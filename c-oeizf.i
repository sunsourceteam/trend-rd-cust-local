for each t-follow:
  delete t-follow.
end.

if g-custno <> 0 and g-takenby <> " " then
  do:
  for each oeehb where oeehb.cono    = g-cono and
                       oeehb.custno  = g-custno and
                       oeehb.takenby = g-takenby and
                       oeehb.stagecd <> 9
                       no-lock:
    /* Exclude if the batch is an OEEBT batch */
    assign alphabatch = no.
    do idx = 1 to 26:
      if oeehb.batchnm begins x-alpha[idx] then
        do:
        assign alphabatch = yes.
        leave.
      end.
    end.
    if alphabatch = yes then next.
    find notes where 
         notes.cono = g-cono and
         notes.notestype = "FU" and
         notes.primarykey = oeehb.batchnm and
         notes.secondarykey = "Initial"
         no-lock no-error.
    find s-follow where
         s-follow.batchnm = oeehb.batchnm and
         s-follow.takenby = g-takenby
         no-error.
    if avail s-follow then next.
    create s-follow.
    assign s-follow.batchnm    = oeehb.batchnm
           s-follow.quamount   = oeehb.totinvamt
           s-follow.qupromdt   = oeehb.promisedt
           s-follow.quenterdt  = oeehb.enterdt
           s-follow.qucanceldt = oeehb.canceldt
           s-follow.FUinitdt   = if avail notes then notes.transdt else ?
           s-follow.FUreminddt = if avail notes then notes.user8   else ?.
  end. /* each oeehb */
end. /* g-custno <> 0 and g-takenby <> " " */
if g-custno <> 0 and g-takenby = " " then
  do:
  for each oeehb where oeehb.cono    = g-cono and
                       oeehb.custno  = g-custno and
                       oeehb.stagecd <> 9
                       no-lock:
    /* Exclude if the batch is an OEEBT batch */
    assign alphabatch = no.
    do idx = 1 to 26:
      if oeehb.batchnm begins x-alpha[idx] then
        do:
        assign alphabatch = yes.
        leave.
      end.
    end.
    if alphabatch = yes then next.
    find notes where 
         notes.cono = g-cono and
         notes.notestype = "FU" and
         notes.primarykey = oeehb.batchnm and
         notes.secondarykey = "Initial"
         no-lock no-error.
    find s-follow where
         s-follow.batchnm = oeehb.batchnm and
         s-follow.takenby = g-takenby
         no-error.
    if avail s-follow then next.
    create s-follow.
    assign s-follow.batchnm    = oeehb.batchnm
           s-follow.quamount   = oeehb.totinvamt
           s-follow.qupromdt   = oeehb.promisedt
           s-follow.quenterdt  = oeehb.enterdt
           s-follow.qucanceldt = oeehb.canceldt
           s-follow.FUinitdt   = if avail notes then notes.transdt else ?
           s-follow.FUreminddt = if avail notes then notes.user8   else ?.
  end. /* each oeehb */
end. /* g-custno <> 0 and g-takenby = " " */
if g-custno = 0 and g-takenby <> " " then
  do:
  for each oeehb where oeehb.cono    = g-cono and
                       oeehb.takenby = g-takenby and
                       oeehb.stagecd <> 9
                       no-lock:
    /* Exclude if the batch is an OEEBT batch */
    assign alphabatch = no.
    do idx = 1 to 26:
      if oeehb.batchnm begins x-alpha[idx] then
        do:
        assign alphabatch = yes.
        leave.
      end.
    end.
    if alphabatch = yes then next.
    find notes where 
         notes.cono = g-cono and
         notes.notestype = "FU" and
         notes.primarykey = oeehb.batchnm and
         notes.secondarykey = "Initial"
         no-lock no-error.
    find s-follow where
         s-follow.batchnm = oeehb.batchnm and
         s-follow.takenby = g-takenby
         no-error.
    if avail s-follow then next.
    create s-follow.
    assign s-follow.batchnm    = oeehb.batchnm
           s-follow.quamount   = oeehb.totinvamt
           s-follow.qupromdt   = oeehb.promisedt
           s-follow.quenterdt  = oeehb.enterdt
           s-follow.qucanceldt = oeehb.canceldt
           s-follow.FUinitdt   = if avail notes then notes.transdt else ?
           s-follow.FUreminddt = if avail notes then notes.user8   else ?.
  end. /* each oeehb */
end. /* g-custno = 0 and g-takenby <> " " */
    
