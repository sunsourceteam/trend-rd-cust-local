/* TBXR F6 CodeGen End of Includes that are CodeGen Generated  */ 
&if defined(user_optiontype) = 2 &then
if not
can-do("B,D,L,P,Q,V,W",substring(p-optiontype,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries B,D,L,P,Q,V,W "
     substring(p-optiontype,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries B,D,L,P,Q,V,W "
     substring(p-optiontype,v-inx,1).
&endif
 
&if defined(user_registertype) = 2 &then
if not
can-do("B,D,L,P,Q,V,W",substring(p-register,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries B,D,L,P,Q,V,W "
     substring(p-register,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries B,D,L,P,Q,V,W "
     substring(p-register,v-inx,1).
&endif
 
&if defined(user_DWvarheaders1) = 2 &then
if v-lookup[v-inx4] = "B" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Buyer".
  assign export_rec = export_rec + v-del + "BuyerName".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "StkOutDate".
  end. 
else  
if v-lookup[v-inx4] = "L" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "ProdLine".
  end. 
else  
if v-lookup[v-inx4] = "P" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Prod".
  assign export_rec = export_rec + v-del + "ProdDescrip".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "Q" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "QtyOnHand".
  end. 
else  
if v-lookup[v-inx4] = "V" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "VendNbr".
  assign export_rec = export_rec + v-del + "VendName".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Warehouse".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_exportvarheaders1) = 2 &then
if v-lookup[v-inx4] = "B" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Buyer".
  assign export_rec = export_rec + v-del + "Buyer Name".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "StockOut".
  end. 
else  
if v-lookup[v-inx4] = "L" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "ProdLine".
  end. 
else  
if v-lookup[v-inx4] = "P" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Product".
  assign export_rec = export_rec + v-del + "Product".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "Q" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "QtyOnHand".
  end. 
else  
if v-lookup[v-inx4] = "V" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Vendor".
  assign export_rec = export_rec + v-del + "Vendor Name".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Whse".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_exportvarheaders2) = 2 &then
if v-lookup[v-inx4] = "B" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  assign export_rec = export_rec + v-del + "".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Date".
  end. 
else  
if v-lookup[v-inx4] = "L" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "P" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  assign export_rec = export_rec + v-del + "Description".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "Q" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "V" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  assign export_rec = export_rec + v-del + "".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_loadtokens) = 2 &then
if v-lookup[v-inx] = "B" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(stkout.buyer,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "D" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(stkout.stockoutdt,"99/99/99")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"99/99/99").   
  end.                                              
else                                              
if v-lookup[v-inx] = "L" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(stkout.prodline,"x(8)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(8)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "P" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(stkout.prod,"x(24)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(24)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "Q" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(stkout.srtqtyOnHand,"x(6)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(6)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "V" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(stkout.vendno,"999999999999")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"999999999999").   
  end.                                              
else                                              
if v-lookup[v-inx] = "W" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(stkout.whse,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "X" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(recid(stkout),"999999999999")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"999999999999").   
  end.                                              
else                                              
  assign v-token = " ".
&endif
 
&if defined(user_totaladd) = 2 &then
  assign t-amounts[1] = stkout.totstkout.
  assign t-amounts[2] = stkout.tot0onhand.
  assign t-amounts[3] = stkout.tot0avail.
  assign t-amounts[4] = stkout.totstkitems.
  assign t-amounts[5] = stkout.tot0availonhand.
  assign t-amounts[6] = 0.
  assign t-amounts[7] = 0.
  assign t-amounts[8] = 0.
  assign t-amounts[9] = 0.
  assign t-amounts[10] = stkout.oelines.
&endif
 
&if defined(user_numbertotals) = 2 &then
  if v-subtotalup[v-inx4] = 1 then
    v-val = t-amounts1[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 2 then
    v-val = t-amounts2[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 3 then
    v-val = t-amounts3[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 4 then
    v-val = t-amounts4[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 5 then
    v-val = t-amounts5[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 6 then
    v-val = 
      (t-amounts[1] / t-amounts[4]) * 100.
  else 
  if v-subtotalup[v-inx4] = 7 then
    v-val = 
      (t-amounts[2] / t-amounts[4]) * 100.
  else 
  if v-subtotalup[v-inx4] = 8 then
    v-val = 
      (t-amounts[3] / t-amounts[4]) * 100.
  else 
  if v-subtotalup[v-inx4] = 9 then
    v-val = 
      (t-amounts[5] / t-amounts[4]) * 100.
  else 
  if v-subtotalup[v-inx4] = 10 then
    v-val = t-amounts10[v-inx4].
  else 
    v-val = t-amounts1[v-inx4].
  do inz = 1 to 10:
  if inz = 1 then
    v-regval[inz] = t-amounts1[v-inx4].
  else 
  if inz = 2 then
    v-regval[inz] = t-amounts2[v-inx4].
  else 
  if inz = 3 then
    v-regval[inz] = t-amounts3[v-inx4].
  else 
  if inz = 4 then
    v-regval[inz] = t-amounts4[v-inx4].
  else 
  if inz = 5 then
    v-regval[inz] = t-amounts5[v-inx4].
  else 
  if inz = 6 then
    v-regval[inz] = 
      (t-amounts[1] / t-amounts[4]) * 100.
  else 
  if inz = 7 then
    v-regval[inz] = 
      (t-amounts[2] / t-amounts[4]) * 100.
  else 
  if inz = 8 then
    v-regval[inz] = 
      (t-amounts[3] / t-amounts[4]) * 100.
  else 
  if inz = 9 then
    v-regval[inz] = 
      (t-amounts[5] / t-amounts[4]) * 100.
  else 
  if inz = 10 then
    v-regval[inz] = t-amounts10[v-inx4].
  else 
    assign inz = inz.
  end.
&endif
 
&if defined(user_summaryloadprint) = 2 &then
  if v-lookup[v-inx2] = "B" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-buyer.i}
      end. 
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Buyer - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "D" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "StockOut Date - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "L" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "ProductLine - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "P" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-product.i}
      end. 
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Product - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "Q" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Qty OnHand GT Zero - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "V" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-vendor.i}
      end. 
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Vendor - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "W" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Warehouse - " + h-lookup + " " + h-genlit.
    end.
  else
      v-summary-lit = v-lookup[v-inx2].
&endif
 
&if defined(user_summaryloadexport) = 2 &then
  if v-inx4 > v-inx2  then 
    do:
    if v-inx4 > 1 then          
      export_rec = export_rec + v-del. 
    assign export_rec = export_rec +  " ".
    if v-cells[v-inx4] = "2" then
      assign export_rec = export_rec +  v-del + " ".
    end. 
  else 
  if v-lookup[v-inx4] = "B" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-buyer.i}
      end. 
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "D" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "99/99/99" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "99/99/99" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "L" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(8)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(8)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "P" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-product.i}
      end. 
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(24)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(24)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "Q" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(6)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(6)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "V" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-vendor.i}
      end. 
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "999999999999" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "999999999999" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "W" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
    do:
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    assign export_rec = export_rec + v-lookup[v-inx4].
    end.
&endif
 
/* TBXR F6 CodeGen End of Includes that are CodeGen Generated  */ 
