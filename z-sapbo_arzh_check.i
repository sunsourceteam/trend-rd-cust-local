/* z-capbozcheck

   Validates selection criteria using variable parameters in z-sapbozdef
   
*/   

Procedure zelectioncheck:

  zelection_good = true.
  if zelection_type = "s" and zelection_matrix [1] = true then
    do:
    find zsapbo where zsapbo.selection_type = "s" and
                      zsapbo.selection_char4 = zelection_char4 and
                      zsapbo.selection_cust = 0 no-lock no-error.
    if not avail zsapbo then
      zelection_good = false.
    end.
  else    
  if zelection_type = "c" and zelection_matrix [2] = true then
    do:
    find zsapbo where zsapbo.selection_type = "c" and
                      zsapbo.selection_char4 = "" and
                      zsapbo.selection_cust = zelection_cust no-lock no-error.
    if not avail zsapbo then
      zelection_good = false.
    end.
  else    
  if zelection_type = "p" and zelection_matrix [3] = true then
    do:
    find zsapbo where zsapbo.selection_type = "p" and
                      zsapbo.selection_char4 = zelection_char4 and
                      zsapbo.selection_cust = 0 no-lock no-error.
    if not avail zsapbo then
      zelection_good = false.
    end.
    else    
  if zelection_type = "x" and zelection_matrix [4] = true then
    do:
    find zsapbo where zsapbo.selection_type = "x" and
                      zsapbo.selection_char4 = zelection_char4 and
                      zsapbo.selection_cust = 0 no-lock no-error.
    if avail zsapbo then
      zelection_good = false.
    end.
end.