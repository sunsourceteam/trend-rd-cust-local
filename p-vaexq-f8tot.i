
procedure f8totals: 
do: 
define buffer tdn-vaspsl for vaspsl. 
define buffer btt-icsp for icsp.


{&vaixicom}
if frame-name = "f8-total"        or 
  (program-name(1) begins "vaexd" or   
   program-name(2) begins "vaexd" or   
   program-name(3) begins "vaexd" or   
   program-name(4) begins "vaexd" or   
   program-name(5) begins "vaexd" or   
   program-name(6) begins "vaexd" or   
   program-name(7) begins "vaexd" or   
   program-name(8) begins "vaexd" or   
   program-name(9) begins "vaexd") then
   leave. 
/{&vaixicom}* */

run makequoteno(g-vaquoteno,output x-quoteno).
if v-errfl then 
 leave.

on cursor-up back-tab. 
on cursor-down tab. 

{&vaixicom}


if v-custno <= 0 then do:
 assign v-framestate = "t1".
 put screen row 23 col 1 " Customer# required - please enter.".
 pause 2.
 next.
end.   

if frame-name = "f-mid1" or
   frame-name = "f-top1" then 
 assign q-rowid = rowid(t-lines)
        o-framenm = frame-name.
/{&vaixicom}* */


 
 hide frame f-whses.
 assign s-override = "".
 assign f8-title =
 " F6-ExternalNotes F9-PrintQuote F10-Update TotalInfo Ctrl-n-InternalNotes  ".
 display f8-title with frame f8-total. 
 
 f8loop:
 repeat:     
   assign s-pndinvamt    = 0
          s-pndextrnamt  = 0
          s-pndintrnamt  = 0
          s-pndintrndsp  = 0
          s-pndtotal     = 0
/* TAH004 1/6/11 */    
          s-shopsupplies = 0
/* TAH004 1/6/11 */               
          s-vaprodcost   = 0
          p-detail       = no 
          p-onlydesc     = no
          p-costfl       = no
          p-quotefl      = no 
          s-listprc      = 0
          t-csttdn-h     = 0
          t-csttdn-m     = 0
          t-cstasy-h     = 0
          t-cstasy-m     = 0
          t-csttst-h     = 0
          t-csttst-m     = 0
          f8-listprc     = 0
          f8-sellprc     = 0
          f8-cost        = 0
          f8-vendno      = 0
          f8-leadtm      = " ".
   find first vasp where
              vasp.cono = g-cono         and 
              vasp.shipprod = x-quoteno  and 
              vasp.whse = entry(1,x-quoteno,"-")
              no-lock no-error.
   if avail vasp then do: 
     run vaexq-OEshipped.p (input g-cono,
                            input substring(vasp.user3,1,39), 
                            input int(substring(vasp.user2,1,7)),
                            output v-FlatRate,
                            output v-OEshipped,
                            output v-Released).
    
    assign v-maxlead  = int(string(substring(vasp.user1,1,4),"9999")) 
           s-maxlead  = int(string(substring(vasp.user1,1,4),"9999")) 
           v-whse     = vasp.whse
           /* smaf  */
           p-pct      = if dec(substring(vasp.user1,5,6)) <> 0 then 
                           dec(substring(vasp.user1,5,6))
                        else 
                          055.00 
           s-pndinvamt = dec(substring(vasp.user1,180,11))
           s-override  = if substring(vasp.user1,250,1) = "f" then 
                           "F"
                         else  
                         if substring(vasp.user1,250,1) ne "" then 
                            "*" 
                         else 
                            " "
           f8-listprc  = dec(substring(vasp.user1,30,10))
           f8-sellprc  = dec(substring(vasp.user1,40,10))
           f8-cost     = dec(substring(vasp.user1,50,10))
           f8-vendno   = 
                   dec(string(substring(vasp.user1,60,12),"999999999999"))
           f8-vendno  = dec(string(f8-vendno,"999999999999"))
           f8-leadtm  = substring(vasp.user1,73,4).
           
        assign v-befamt = s-pndinvamt.
    
           
      if f8-listprc = 0 and s-prod ne "" then do: 
       {w-icsw.i s-prod x-whse no-lock}
       if avail icsw then 
        assign f8-listprc = icsw.listprice. 
      end.  
   end. 
   find first vaspsl where 
              vaspsl.cono = g-cono            and 
              vaspsl.shipprod = vasp.shipprod and 
              vaspsl.compprod = "teardown"    and 
              vaspsl.seqno    = 2
              no-lock no-error. 
   if avail vaspsl then do:
    assign t-csttdn-h = (v-esttdn-h * vaspsl.prodcost) 
           t-csttdn-m = ((v-esttdn-m * vaspsl.prodcost) / 60). 
   end.       
   find first vaspsl where 
              vaspsl.cono = g-cono               and 
              vaspsl.shipprod = vasp.shipprod    and 
              vaspsl.compprod = "service repair" and 
              vaspsl.seqno    = 6
              no-lock no-error. 
   if avail vaspsl then do:
    assign t-cstasy-h = (v-estasy-h * vaspsl.prodcost)
           t-cstasy-m = ((v-estasy-m * vaspsl.prodcost) / 60). 
   end. 
   find first vaspsl where 
              vaspsl.cono = g-cono            and 
              vaspsl.shipprod = vasp.shipprod and 
              vaspsl.compprod = "test time"   and 
              vaspsl.seqno    = 6
              no-lock no-error. 
   if avail vaspsl then do: 
    assign t-csttst-h = (v-esttst-h * vaspsl.prodcost)  
             t-csttst-m = ((v-esttst-m * vaspsl.prodcost) / 60). 
   end.        
   assign s-pndintrndsp  = t-csttdn-h + t-csttdn-m + 
                           t-cstasy-h + t-cstasy-m + 
                           t-csttst-h + t-csttst-m. 
   assign s-vaprodcost  = 0
          s-pndextrnamt = 0.
   for each vaspsl where
            vaspsl.cono     = g-cono and 
            vaspsl.shipprod = x-quoteno and 
           (vaspsl.compprod = "teardown"       or 
            vaspsl.compprod = "service repair" or
            vaspsl.compprod = "test time") no-lock: 
           /** update cost gain on laboritems below **/
        assign s-pndextrnamt = s-pndextrnamt + vaspsl.xxde1. 
   end.
   find first vasp where
              vasp.cono = g-cono         and 
              vasp.shipprod = x-quoteno  and 
              vasp.whse = entry(1,x-quoteno,"-")
{&vaexqcom}
              no-lock no-error.
/{&vaexqcom}* */
{&vaixicom}
              exclusive-lock no-error.
/{&vaixicom}* */

   def buffer f8-t-lines for t-lines. 
   for each f8-t-lines where
            f8-t-lines.specnstype ne "l" no-lock:
/* TAH002 */     
     if ( f8-t-lines.prod begins "core-" or 
          f8-t-lines.prodcat = "SCOR" )
/* TAH002 */        
     then do:
      assign s-pndextrnamt = if f8-t-lines.gain > 0 then 
                              s-pndextrnamt + f8-t-lines.gain
                             else
                              s-pndextrnamt. 
      next.
     end.

{&vaexqcom}
          assign s-vaprodcost  = if f8-t-lines.specnstype = "in" and 
                                ( f8-t-lines.prod begins "core-" or
                                  f8-t-lines.prodcat = "SCOR") then 
                               s-vaprodcost
                              else  
                              if f8-t-lines.specnstype = "in" and 
                                 f8-t-lines.seqno = 3 and
                                 (f8-t-lines.prod = "margin improvement" or
                                  f8-t-lines.prod = "Shop Supplies"      or
                                  f8-t-lines.prod = "freight charge"     or
                                  f8-t-lines.prod = 
                                  "Hydraulic Fluid Charge") then
 
                                s-vaprodcost 
                              else  
                              if f8-t-lines.specnstype = "in"  then
                               s-vaprodcost + f8-t-lines.extcost
                              else 
                               s-vaprodcost
              s-listprc     =   
                              if f8-t-lines.specnstype = "in" and 
                                 f8-t-lines.seqno = 3 and
                                 (f8-t-lines.prod ne "margin improvement" or
                                  f8-t-lines.prod ne "Shop Supplies"      or
                                  f8-t-lines.prod ne "freight charge"     or
                                  f8-t-lines.prod ne 
                                  "Hydraulic Fluid Charge") and
                                 
                                 f8-t-lines.specnstype ne "l" then
 
                               s-listprc + f8-t-lines.listprc
                              else  
                              if f8-t-lines.specnstype = "in" and 
                                 f8-t-lines.seqno <> 3 and
                                 f8-t-lines.specnstype ne "l" then
                               s-listprc + f8-t-lines.listprc
                              else 
                               s-listprc
            s-pndextrnamt =  x-costgain.
  
/{&vaexqcom}* */

 
{&vaixicom}
      assign s-vaprodcost  = s-vaprodcost  + f8-t-lines.extcost
             s-pndextrnamt = if f8-t-lines.gain > 0 then 
                               s-pndextrnamt + f8-t-lines.gain
                            else
                               s-pndextrnamt 
            s-listprc     =  if f8-t-lines.specnstype = "l" then 
                              s-listprc 
                             else  
                              s-listprc + f8-t-lines.listprc.
 
/{&vaixicom}* */

     assign nchk = trim(f8-t-lines.leadtm,"0123456789")         
            ichk = length(nchk).                    
     if ichk = 0 and f8-t-lines.specnstype ne "l" and 
       (int(string(f8-t-lines.leadtm,"9999")) > v-maxlead) then  
        assign v-maxlead = int(string(f8-t-lines.leadtm,"9999")).
   end.

   for each vaspsl where
            vaspsl.cono     = g-cono and 
            vaspsl.shipprod = x-quoteno and 
            vaspsl.compprod = "freight charge" and  
            vaspsl.seqno = 3 no-lock: 
           /** update cost gain on laboritems below **/
           assign s-vaprodcost = s-vaprodcost +
                   ( vaspsl.qtyneeded * vaspsl.prodcost). 
   end.   

   for each vaspsl where
            vaspsl.cono     = g-cono and 
            vaspsl.shipprod = x-quoteno and 
            vaspsl.compprod = "Hydraulic Fluid Charge" and  
            vaspsl.seqno = 3 no-lock: 
           /** update cost gain on laboritems below **/
           assign s-vaprodcost = s-vaprodcost +
                   ( vaspsl.qtyneeded * vaspsl.prodcost). 
   end.   
  
   assign s-vaprodcost = round(s-vaprodcost,2)
          s-pndextrnamt = round(s-pndextrnamt,2).
   if v-maxlead >= s-maxlead then 
    assign s-maxlead = v-maxlead.
   {w-icsw.i t-lines.prod v-whse no-lock}
/* TAH004 1/6/11 */     

   if can-find(first f-vaspsl where
                     f-vaspsl.cono     = g-cono and 
                     f-vaspsl.shipprod = x-quoteno and 
                     f-vaspsl.compprod = "shop supplies" and  
                     f-vaspsl.seqno = 3 no-lock) then do:
                     
                     
     assign s-shopsupplies = round((s-vaprodcost + s-pndextrnamt)
                               * s-shopsuppliesmult,2).
   end.
   else
    assign s-shopsupplies = 0. 
/* TAH004 1/6/11 */     
   assign s-vareqshipdt  = today 
          s-varefer      = s-prod
          s-vacustprod   = s-prod
          s-vapromisedt  = today
          s-vareceiptdt  = today 
          s-pndtotal     = s-vaprodcost + s-pndintrndsp + s-pndextrnamt
/* TAH004 1/6/11 */     
                           + s-shopsupplies.
/* TAH004 1/6/11            
          s-trendtot     = s-pndtotal * 1.04.
   TAH004 1/6/11 */
    find btt-icsp where 
         btt-icsp.cono = 1 and
         btt-icsp.prod = vasp.refer no-lock no-error.
                         
    find zl-notes use-index k-notes where                        
         zl-notes.cono         = g-cono and                      
         zl-notes.notestype    = "zz" and                        
         zl-notes.primarykey   = "standard cost markup" and      
         zl-notes.secondarykey = (if avail btt-icsp then
                                     btt-icsp.prodcat 
                                  else
                                    "#(notvalid)#") and         
         zl-notes.pageno       = 1                               
         no-lock no-error.                                       
    if avail zl-notes then do:                    
      assign s-trendtot = s-pndtotal *            
        max((1 + (dec(zl-notes.noteln[1])  / 100)),v-minadder).  
    end.    
    else
      assign  s-trendtot = s-pndtotal * v-minadder.
/* TAH004 1/6/11 */  
   
   def buffer zxt2-sasos for sasos. 
   find first zxt2-sasos where
              zxt2-sasos.cono  = g-cono and
              zxt2-sasos.oper2 = g-operinits and
              zxt2-sasos.menuproc = "zxt2"
              no-lock no-error.

   assign zsdiupdated = no. 
   find zsdivasp use-index k-repairno where
        zsdivasp.cono = g-cono         and 
        zsdivasp.repairno = x-quoteno  and 
        zsdivasp.whse = entry(1,x-quoteno,"-")
        no-lock no-error.
   if avail zsdivasp then  
    assign p-costfl = if zsdivasp.stagecd > 4 then 
                       yes
                      else 
                       no
          p-quotefl = if zsdivasp.stagecd > 5 then 
                       yes
                      else 
                       no.

   assign s-specnstype = " ".
   if  int(substring(vasp.user2,1,7)) <> 0 then do:
     find first vaeh where 
          vaeh.cono = g-cono and
          vaeh.vano =  int(substring(vasp.user2,1,7)) no-lock no-error.
     if avail vaeh then
       assign s-specnstype = vaeh.nonstockty.
   end.       
 
    
   
   
   
   if (substring(vasp.user1,250,1) = "y" or
       substring(vasp.user1,250,1) = "F") or     
      int(substring(vasp.user2,1,7)) <> 0 or
      p-quotefl then do:
     assign s-pndinvamt = dec(substring(vasp.user1,180,11))
            p-pct       =
           round((1 - (s-trendtot / s-pndinvamt)) * 100,2).
   end.
   else do:             /* if not override then compute */
    assign s-pndinvamt = if round((s-trendtot / (100 - p-pct) * 100),2) -
                           (s-trendtot / (100 - p-pct) * 100) <> 0 then
                           round((s-trendtot / (100 - p-pct) * 100),2) 
                         else
                            round((s-trendtot / (100 - p-pct) * 100),2).  
  
    assign s-pndinvamt2 = s-pndinvamt.
  end.  
                                        
   /* start of big loop */
   totloop:
   do while true on endkey undo, leave: 
   put screen row 22 col 1 color message 
"  F6-Top  F7-Lines  F8-TOTALS  F9-SPcNotes  F10-PoComments  Ctrl-p(Prt label) ~    ".
   assign zsdiupdated = no. 
  
   assign d-override = if s-override <> "" then
                         s-override
                       else if zsdivasp.xuser9 <> 
                               dec(substring(vasp.user1,5,6)) and
                                zsdivasp.stagecd > 5 then
                         "q"
                       else if ((zsdivasp.xuser9 <> 55.00 and 
                                int(substring(vasp.user2,1,7)) <> 0) or
                                (zsdivasp.xuser9 <> 55.00 and 
                                p-quotefl) or
                                zsdivasp.stagecd < 6 ) and
                             dec(substring(vasp.user1,5,6)) <> 55.00 then
                         "m"
                        else if dec(substring(vasp.user1,5,6)) = 55.00 then
                         " "
                        else
                         "*".
     
     
     display s-vareqshipdt
             s-vaprodcost
             s-vapromisedt
             s-pndextrnamt    
             s-pndintrndsp 
/* TAH004 1/6/11 */
             s-shopsupplies
/* TAH004 1/6/11 */
             s-pndtotal
             d-override 
             s-trendtot
             s-listprc
             s-maxlead
             p-costfl
             p-quotefl
             p-pct
             s-pndinvamt      
             f8-listprc
             f8-sellprc
             f8-cost
             f8-vendno
             f8-leadtm
             f8-title 
             with frame f8-total. 
    
{&vaexqcom}
      update /* s-maxlead   when not v-inuse and g-secure  > 2 */
          p-quotefl   when (not v-inuse and g-secure  > 2) and 
                           (not p-quotefl)
          p-pct       when not (v-inuse and zxt2-sasos.securcd[6] > 2  and
                           ( dec(substring(vasp.user2,1,7)) = 0 
                              or 
                              (not v-OEshipped and v-Released and
                                v-FlatRate)) )
          
          s-pndinvamt when (avail zxt2-sasos and zxt2-sasos.securcd[6] > 2
                      and not v-inuse and g-secure  > 2 and
                      ( dec(substring(vasp.user2,1,7)) = 0 or
                       ( not v-OEshipped and v-Released and v-Flatrate)
                        )) 
          
          p-prce      when not v-inuse and g-secure  > 2 
          p-onlydesc  when not v-inuse and g-secure  > 2 
          f8-listprc  when not v-inuse and g-secure  > 2 
          f8-sellprc  when not v-inuse and g-secure  > 2 
          f8-cost     when not v-inuse and g-secure  > 2 
          f8-vendno   when not v-inuse and g-secure  > 2 
          f8-leadtm   when not v-inuse and g-secure  > 2 
          go-on(f6,f8,f9,f10,f11,ctrl-f) with frame f8-total
 
/{&vaexqcom}* */

 
{&vaixicom}

    
   update /* s-maxlead */
          p-costfl    when not p-costfl
          p-quotefl   when not p-quotefl
          p-pct when (avail zxt2-sasos and zxt2-sasos.securcd[6] > 2 and
                      (int(substring(vasp.user2,1,7)) = 0 or
                      (not v-OEshipped and v-Released and v-Flatrate) )) 
                      /* quotedfix */
          s-pndinvamt when (avail zxt2-sasos and zxt2-sasos.securcd[6] > 2 and
                      (int(substring(vasp.user2,1,7)) = 0 or 
                      (not v-OEshipped and v-Released and v-Flatrate) )) 
                      /* quotedfix */
          p-prce
          p-onlydesc
          f8-listprc
          f8-sellprc
          f8-cost
          f8-vendno
          f8-leadtm
          go-on(f6,f9,f10,f11) with frame f8-total
/{&vaixicom}* */
      
          editing:
            readkey.

            if lastkey = 404 then leave totloop.
            
            assign new-pct = input p-pct.
            if {k-sdileave.i 
                  &functions  = " can-do('PF1',keylabel(lastkey))    or 
                                  can-do('f9',keylabel(lastkey))     or 
                                  can-do('f11',keylabel(lastkey))    or 
                                  can-do('f10',keylabel(lastkey))    or "
                  &fieldname     = "p-pct"               
                  &completename  = "p-pct"} then do:
           if new-pct <> round(p-pct,2) then do:
              assign
                s-override = " "
                s-pndinvamt    =
                          if round((s-trendtot / (100 - input p-pct)) * 100,2) -
                          (s-trendtot / (100 - input p-pct)) * 100 <> 0 then
                            round((s-trendtot / (100 - input p-pct)) * 100,2) 
                          else
                            round((s-trendtot / (100 - input p-pct)) * 100,2).
              assign s-pndinvamt2   = s-pndinvamt.
              
              assign d-override = if s-override <> "" then
                                   s-override
                                 else if new-pct = 55.00 then
                                   " "
                                 else if new-pct <> 55.00 then
                                   "m"
                                 else
                                   "*".
             display s-pndinvamt d-override with frame f8-total.
            end. /* new-pct */
            else if new-pct = p-pct then do:
               assign s-pndinvamt    = input s-pndinvamt.
               assign s-pndinvamt2   = s-pndinvamt.      
               display s-pndinvamt with frame f8-total.  
            end. /* else do */                         
           end.                                        

           if {k-sdileave.i 
                  &functions  = " can-do('PF1',keylabel(lastkey))    or 
                                  can-do('f9',keylabel(lastkey))     or 
                                  can-do('f11',keylabel(lastkey))    or 
                                  can-do('f10',keylabel(lastkey))    or "
                  &fieldname     = "s-pndinvamt"               
                  &completename  = "s-pndinvamt"} then do:
                  /* (unit cost / new sell price ) * 100 */
             
          
              assign new-pndinvamt = input s-pndinvamt.
             
             if new-pndinvamt <> s-pndinvamt then do: 
              assign p-pct =  
              round((1 - (s-trendtot / input  s-pndinvamt)) * 100,2).

              assign s-pndinvamt2   = input s-pndinvamt.
              if round((s-trendtot / (100 - input p-pct)) * 100,2) =
                 new-pndinvamt then
                assign s-override = " ".
              else
                assign s-override = "*".

              
             assign d-override = if s-override <> "" then
                                   s-override
                                 else if p-pct = 55.00 then
                                   " "
                                 else if p-pct <> 55.00 then
                                   "m"
                                 else
                                   "*".
             display p-pct d-override with frame f8-total.
             end. /* new-pndinvamt */
             else do:
               assign p-pct =  ROUND(input p-pct, 2).     
               assign s-pndinvamt2   = input s-pndinvamt. 
               display p-pct with frame f8-total.         
             end. /* else do */     
            end.        
            apply lastkey.
          end.         

{&vaexqcom}
   if g-secure <= 2  then do:  
    pause. 
    leave.
   end. 
   else  
   if v-inuse and 
    (vasp.xxc4 ne string(g-operinits,"x(4)") +
     " in --> " + g-currproc) then do:   
    assign v-error = " RepairNo is in-use by operator: " + 
                       string(vasp.xxc4,"x(20)").
    run zsdierrx.p(v-error,"yes"). 
    unix silent value("sh /rd/cust/local/UnixPause.sh").    
    message " ". 
    pause 0.
   /* reset cursor movement for the f-lines browse */ 
    on cursor-up cursor-up. 
    on cursor-down cursor-down.
    readkey pause 0.
    return.
   end. 
/{&vaexqcom}* */
                
                
   assign f8-title =
 " F6-ExternalNotes F9-PrintQuote F10-UPDATE TOTALINFO Ctrl-n-InternalNotes  ".
   display f8-title with frame f8-total. 
   
   find first vasp where
              vasp.cono = g-cono         and 
              vasp.shipprod = x-quoteno  and 
              vasp.whse = entry(1,x-quoteno,"-")
              exclusive-lock no-error.
   if avail vasp then do: 
    find first zsdivasp where
                 zsdivasp.cono = g-cono         and 
                 zsdivasp.repairno = x-quoteno  and 
                 zsdivasp.whse = entry(1,x-quoteno,"-")
                 exclusive-lock no-error.
      assign zsdivasp.leadtm1 = string(v-maxlead,"9999").
   end. 
   assign overlay(vasp.user1,1,4)  = if s-maxlead < v-maxlead and
              int(string(substring(vasp.user1,1,4),"9999")) ne s-maxlead then 
                                      string(v-maxlead,"9999")
                                     else  
                                      string(s-maxlead,"9999")
          overlay(vasp.user1,30,10)  = string(f8-listprc,"9999999.99")
          overlay(vasp.user1,40,10)  = string(f8-sellprc,"9999999.99")
          overlay(vasp.user1,50,10)  = string(f8-cost,"9999999.99")
          overlay(vasp.user1,60,12)  = string(f8-vendno,"999999999999")
          overlay(vasp.user1,73,4)   = string(f8-leadtm,"9999")
          overlay(vasp.user1,150,10) = string(s-vaprodcost,"9999999.99")  
          overlay(vasp.user1,160,10) = string(s-pndextrnamt,"999999.99-")  
          overlay(vasp.user1,170,10) = string(s-listprc,"9999999.99").      

    if s-pndinvamt <= 0 then do: 
     assign v-error = "Price cannot be zero " +  
                      "- Warning price is not recalculated .". 
     run zsdi_vaerrx.p(v-error,"yes"). 
     next totloop.
    end. 
   


    run vaexq-flatprice (input g-cono,
                         input substring(vasp.user3,1,39),
                         input vasp.refer,
                         input vasp.whse,   
                         input dec(vasp.user5),
                         input substring(vasp.user4,80,8),
                         output v-pricetype,
                         output v-prefix). 
     if v-prefix <> "" then
        assign zsdi-prctyp = v-pricetype.
     run vaexq-PDSC (input vasp.whse,
                     input substring(vasp.user3,1,39),
                     input vasp.refer,
                     input v-prefix,
                     input dec(vasp.user5),
                     input substring(vasp.user4,80,8),
                     output xxx-price,
                     output zi-pdscrec).

    find pdsc where pdsc.cono = g-cono and
                    pdsc.pdrecno = zi-pdscrec no-lock no-error.
    assign zsdi-pdsclvl  = if avail pdsc then pdsc.levelcd else 0.

    find first zy-sasos where zy-sasos.cono   = g-cono and 
                              zy-sasos.oper2  = g-operinits and 
                              zy-sasos.menuproc = "zxt1" 
    no-lock no-error.                  

    
    if avail zy-sasos then 
       assign zsdi-lvl = zy-sasos.securcd[3]. 
    else  
       assign zsdi-lvl = 1.

    if (v-FlatRate and  
       ((zsdi-lvl = 2 and 
          not can-do("1,2",string(zsdi-pdsclvl,"9")))
        or
         zsdi-lvl = 1 ) and
         xxx-price >  s-pndinvamt) and
        ( ( s-pndinvamt ne dec(substring(vasp.user1,180,11)) ) or
        (dec(string(p-pct,"999.99-")) ne dec(substring(vasp.user1,5,6)) 
        and s-override <> "*" ) ) 
    then do:
       assign v-error = 
       "Price Override Violation or Reg/Dist Margin Limits (ZXT1 = 1/ PDZM)". 
       
       
       run zsdi_vaerrx.p(v-error,"yes"). 
 
       next-prompt s-pndinvamt with frame f8-total.
       next.   
    end.   

    if (v-FlatRate and 
       (zsdi-pdsclvl = 4 or
        zsdi-pdsclvl = 8)) and
      ( ( s-pndinvamt ne dec(substring(vasp.user1,180,11)) ) or
      (dec(string(p-pct,"999.99-")) ne dec(substring(vasp.user1,5,6)) 
       and s-override <> "*" ) ) 
    then do:
    
      find first zy-sasos where zy-sasos.cono   = g-cono and 
                                zy-sasos.oper2  = g-operinits and 
                                zy-sasos.menuproc = "xpds" 
      no-lock no-error.                  
      if avail zy-sasos then 
        assign zsdi-lvl = zy-sasos.securcd[7]. 
      else 
        assign zsdi-lvl = 1.

      assign zsdi-prcdiff =   if xxx-price <>  s-pndinvamt then
                                 xxx-price -  s-pndinvamt
                               else
                                  0
             zsdi-todaypct = 0
             zsdi-chgpct   = 0.  /* margins cannot be counted on at this point
                                  for flatrate pricing */
 
    
      if zsdi-lvl < 5 then do:
       
        run zsdigradient.p(input zsdi-slsrepout,
                           input v-custno, 
                           input v-shipto,
                           input zsdi-todaypct, 
                           input zsdi-chgpct,  
                           input zsdi-lvl,     
                           input vasp.refer, 
                           input vasp.whse,  
                           input zsdi-prctyp,   
                           input (if v-FlatRate and zsdi-pdsclvl = 8 then 
                                    true
                                  else false)  ,
                           /** set to 'n' for non-stock & pdsc lvl = 0 **/
                           input (if zsdi-pdsclvl = 0 and 
                                    s-specnstype ne "n" then 
                                   "n"
                                  else  
                                    s-specnstype),  
                           input "so", 
                           input zsdi-prcdiff,
                           input xxx-price,
                           input-output v-errfl2,
                           input-output v-errmsg).
        
        if v-errfl2 <> 0 then do:
          assign s-pndinvamt =  s-pndinvamt2.
          run zsdi_vaerrx.p(substring(v-errmsg,1,75),"yes").  
          if frame-field = "s-pndinvamt" then
            next-prompt s-pndinvamt with frame f8-total.
          else 
          if frame-field = "f8-leadtm" then
            next-prompt s-pndinvamt with frame f8-total.
          else
            next-prompt s-pndinvamt with frame f8-total.
            next.   
        end.  /* v-errfl2 <> 0 */ 
      end.
    end.
    
    /* smaf */
    find first zy-sasos where zy-sasos.cono   = g-cono and 
                              zy-sasos.oper2  = g-operinits and 
                              zy-sasos.menuproc = "xpds" 
                              no-lock no-error.                  
    if avail zy-sasos then 
      assign zsdi-lvl = zy-sasos.securcd[6]. 
    else  
       assign zsdi-lvl = 1.

    assign zsdi-pdsclvl = zi-pdscrec.
    assign zsdi-chgpct = ((s-pndinvamt - s-trendtot) / s-pndinvamt) * 100.
    assign /* v-slsrepout   = arsc.slsrepout  
              v-custno      = v-custnoa   */
              v-oldmarg     = p-pct /* zsdi-todaypct  */
              v-chgmarg     = zsdi-chgpct     
              v-lvl         = zsdi-lvl      
              v-type        = zsdi-prctyp    
              v-nstyp       = (if zsdi-pdsclvl = 0 and
                                v-specnstype ne "n" then 
                                "n"
                              else
                                v-specnstype)  
              v-oetyp       = "so"   
           /* v-errfl       = "0 */    
              v-errmsg      = "".
   if not v-FlatRate and    
      zxt2-sasos.securcd[6] >= 2 and v-lvl < 5 and frame-field <> "p-pct" then 
   do:
   run zsdiptypchk.p(input v-slsrepout,
                     input v-custno,
                     input v-shipto,
                     input v-oldmarg,
                     input v-chgmarg,
                     input v-lvl,
                     input v-prod,
                     input v-whse,
                     input v-type,
                     input v-nstyp,
                     input "so",
                     input-output v-errfl2,
                     input-output v-errmsg).           
            if v-errfl2 <> 0 then do:
             assign s-pndinvamt =  s-pndinvamt2.
             run zsdi_vaerrx.p(substring(v-errmsg,13,80),"yes").  
             if frame-field = "s-pndinvamt" then
              next-prompt s-pndinvamt with frame f8-total.
             else 
             if frame-field = "f8-leadtm" then
              next-prompt s-pndinvamt with frame f8-total.
             else
              next-prompt s-pndinvamt with frame f8-total.
              next.   
            end.  /* v-errfl2 <> 0 */ 
   end.  /* if lvl < 5 */                
   
    if not v-FlatRate and    
       zxt2-sasos.securcd[6] >= 2 and v-lvl < 5 and frame-field <> 
    "s-pndinvamt" then do:
        assign   v-oldmarg     = zsdi-chgpct 
                 v-chgmarg     = p-pct.     
                 
        run zsdiptypchk.p(input v-slsrepout,
                     input v-custno,
                     input v-shipto,
                     input v-oldmarg,
                     input v-chgmarg,
                     input v-lvl,
                     input v-prod,
                     input v-whse,
                     input v-type,
                     input v-nstyp,
                     input "so",
                     input-output v-errfl2,
                     input-output v-errmsg).           
            if v-errfl2 <> 0 then do:
             assign p-pct = zsdi-chgpct.
             run zsdi_vaerrx.p(substring(v-errmsg,13,80),"yes").  
             next-prompt p-pct with frame f8-total.
             next. /*  totloop.  */
            end.
    end.  /* sasos */
 
   if (dec(string(p-pct,"999.99-")) ne dec(substring(vasp.user1,5,6)) 
     and (s-override <> "*" and s-override <> "F")) or 
    dec(substring(vasp.user1,180,11)) = 0  then do:
    if int(vasp.user7) > 5 then do:  
     message " Customer has already been quoted "  
               skip                       
             "    Continue with price change?   " 
     view-as alert-box question buttons yes-no 
     update s as logical.
     if s then do: 
      find first vasp where
                 vasp.cono = g-cono         and 
                 vasp.shipprod = x-quoteno  and 
                 vasp.whse = entry(1,x-quoteno,"-")
                 exclusive-lock no-error.
             
       find first zsdivasp where
                  zsdivasp.cono = g-cono         and 
                  zsdivasp.repairno = x-quoteno  and 
                  zsdivasp.whse = entry(1,x-quoteno,"-")
                  exclusive-lock no-error.
      
      assign overlay(vasp.user1,5,6) = if p-pct < 0 then
                                         string(0,"999.99")
                                       else
                                         string(p-pct,"999.99").
      assign zsdivasp.xuser9    = dec(substring(vasp.user1,5,6)).
       assign s-pndinvamt = 
                if  round((s-trendtot / (100 - p-pct) * 100),2)
                      - (s-trendtot / (100 - p-pct) * 100) <> 0 then
                  round((s-trendtot / (100 - p-pct) * 100),2) 
                else
                  round((s-trendtot / (100 - p-pct) * 100),2)
             s-pndinvamt2 = s-pndinvamt
             s-pndinvamt = if s-pndinvamt > 9999999 then 
                             9999999
                           else 
                             s-pndinvamt
              overlay(vasp.user1,250,1)  = " "
              overlay(vasp.user1,180,11) = if s-pndinvamt > 0 then 
                                             string(s-pndinvamt,"9999999.99-")
                                           else 
                                             substring(vasp.user1,180,11).
      assign s-override  = if substring(vasp.user1,250,1) = "F" then 
                             "F"
                           else
                           if substring(vasp.user1,250,1) ne "" then 
                            "*" 
                           else 
                            " ".
 
   assign d-override = if s-override <> "" then
                         s-override
                       else if zsdivasp.xuser9 <> 
                               dec(substring(vasp.user1,5,6)) and
                                zsdivasp.stagecd > 5 then
                         "q"
                       else if ((zsdivasp.xuser9 <> 55.00 and 
                                int(substring(vasp.user2,1,7)) <> 0) or
                                (zsdivasp.xuser9 <> 55.00 and 
                                p-quotefl) or
                                zsdivasp.stagecd < 6 ) and
                             dec(substring(vasp.user1,5,6)) <> 55.00 then
                         "m"
                        else if p-pct = 55.00 then
                         " "
                        else
                         "*".
        
      display p-pct d-override s-pndinvamt with frame f8-total. 
     end.
     else do: 
      assign s-vaprodcost  = 0
             s-pndextrnamt = 0
             p-pct         = dec(substring(vasp.user1,5,6))
             p-pct         = if p-pct > 999 then 
                              999
                             else 
                             if p-pct <= 0 then 
                              0 
                             else   
                              p-pct.
      next totloop.                        
     end.
    end. /* if stage is quoted or above */
    
    find first vasp where
               vasp.cono = g-cono         and 
               vasp.shipprod = x-quoteno  and 
               vasp.whse = entry(1,x-quoteno,"-")
               exclusive-lock no-error.
    assign overlay(vasp.user1,5,6) = if p-pct < 0 then
                                       string(0,"999.99")
                                     else
                                       string(p-pct,"999.99").
    assign s-pndinvamt = 
             if  round((s-trendtot / (100 - p-pct) * 100),2)
                   - (s-trendtot / (100 - p-pct) * 100) <> 0 then
               round((s-trendtot / (100 - p-pct) * 100),2) 
             else
               round((s-trendtot / (100 - p-pct) * 100),2)
              
          s-pndinvamt2 = s-pndinvamt
          s-pndinvamt = if s-pndinvamt > 9999999 then 
                           9999999
                        else 
                          s-pndinvamt
          overlay(vasp.user1,250,1)  = " "
          overlay(vasp.user1,180,11) = if s-pndinvamt > 0 then 
                                         string(s-pndinvamt,"9999999.99-")
                                       else 
                                         substring(vasp.user1,180,11).
      assign s-override  = if substring(vasp.user1,250,1) = "F" then 
                             "F"
                           else
                           if substring(vasp.user1,250,1) ne "" then 
                            "*" 
                           else 
                            " ".

      
       
      assign d-override = if s-override <> "" then
                            s-override
                          else if zsdivasp.xuser9 <> 
                                  dec(substring(vasp.user1,5,6)) and
                                   zsdivasp.stagecd > 5 then
                            "q"
                          else if ((zsdivasp.xuser9 <> 55.00 and 
                                   int(substring(vasp.user2,1,7)) <> 0) or
                                   (zsdivasp.xuser9 <> 55.00 and 
                                   p-quotefl) or
                                   zsdivasp.stagecd < 6 ) and
                                dec(substring(vasp.user1,5,6)) <> 55.00 then
                            "m"
                           else if p-pct = 55.00 then
                             " "
                           else
                             "*".
 
      display p-pct d-override s-pndinvamt with frame f8-total. 
   end. 
   else  
   if s-pndinvamt ne dec(substring(vasp.user1,180,11)) and 
      s-pndinvamt > 0 and v-errfl2 = 0 then do: 
    if int(vasp.user7) > 5 then do:  
     message " Customer has already been quoted "  
               skip                       
             "    Continue with price change?   " 
     view-as alert-box question buttons yes-no 
     update s2 as logical.
     if s2 then do: 
      find first vasp where
                 vasp.cono = g-cono         and 
                 vasp.shipprod = x-quoteno  and 
                 vasp.whse = entry(1,x-quoteno,"-")
                 exclusive-lock no-error.
      assign overlay(vasp.user1,180,11) = if s-pndinvamt > 0 then 
                                           string(s-pndinvamt,"9999999.99-")
                                          else  
                                           substring(vasp.user1,180,11)
             
             p-pct = p-pct
             p-pct = if p-pct > 999 then 
                      999
                     else 
                     if p-pct <= 0 then 
                      0 
                     else   
                      p-pct
             overlay(vasp.user1,5,6)   = if p-pct < 0 then
                                           string(0,"999.99")
                                         else
                                           string(p-pct,"999.99")
             overlay(vasp.user1,250,1) =
               if round((s-trendtot / (100 - p-pct)) * 100,2) =
               s-pndinvamt then
                 " "
               else
                 "y".

       find first zsdivasp where
                  zsdivasp.cono = g-cono         and 
                  zsdivasp.repairno = x-quoteno  and 
                  zsdivasp.whse = entry(1,x-quoteno,"-")
                  exclusive-lock no-error.
      
      assign zsdivasp.xuser9    = dec(substring(vasp.user1,5,6)).
       
      
      
      assign s-override  = if substring(vasp.user1,250,1) = "F" then 
                             "F"
                           else
                           if substring(vasp.user1,250,1) ne "" then 
                            "*" 
                           else 
                            " ".
     
 
   assign d-override = if s-override <> "" then
                         s-override
                       else if zsdivasp.xuser9 <> 
                               dec(substring(vasp.user1,5,6)) and
                                zsdivasp.stagecd > 5 then
                         "q"
                       else if ((zsdivasp.xuser9 <> 55.00 and 
                                int(substring(vasp.user2,1,7)) <> 0) or
                                (zsdivasp.xuser9 <> 55.00 and 
                                p-quotefl) or
                                zsdivasp.stagecd < 6 ) and
                             dec(substring(vasp.user1,5,6)) <> 55.00 then
                         "m"
                       else if p-pct = 55.00 then
                         " "
                       else
                         "*".
        
      
      display p-pct d-override s-pndinvamt with frame f8-total. 
     end. 
     else do: 
      assign s-pndinvamt   = dec(substring(vasp.user1,180,11)).
      next totloop.
     end.
    end. /* if stage is quoted or above */
    find first vasp where
               vasp.cono = g-cono         and 
               vasp.shipprod = x-quoteno  and 
               vasp.whse = entry(1,x-quoteno,"-")
               exclusive-lock no-error.
    assign overlay(vasp.user1,180,11) = if s-pndinvamt > 0 then 
                                         string(s-pndinvamt,"9999999.99-")
                                        else  
                                         substring(vasp.user1,180,11)
           p-pct = p-pct
           p-pct = if p-pct > 999 then 
                      999
                   else 
                   if p-pct <= 0 then 
                      0 
                   else   
                      p-pct
           overlay(vasp.user1,5,6)   = if p-pct < 0 then
                                         string(0,"999.99")
                                       else
                                         string(p-pct,"999.99")
           overlay(vasp.user1,250,1) =
               if round((s-trendtot / (100 - p-pct)) * 100,2) =
               s-pndinvamt then
                 " "
               else
                 "y".
          
    
    assign s-override  = if substring(vasp.user1,250,1) = "F" then
                           "F"
                         else    
                         if substring(vasp.user1,250,1) ne "" then 
                          "*" 
                         else 
                          " ".
     
 
   assign d-override = if s-override <> "" then
                         s-override
                       else if zsdivasp.xuser9 <> 
                               dec(substring(vasp.user1,5,6)) and
                                zsdivasp.stagecd > 5 then
                         "q"
                       else if ((zsdivasp.xuser9 <> 55.00 and 
                                int(substring(vasp.user2,1,7)) <> 0) or
                                (zsdivasp.xuser9 <> 55.00 and 
                                p-quotefl) or
                                zsdivasp.stagecd < 6 ) and
                             dec(substring(vasp.user1,5,6)) <> 55.00 then
                         "m"
                       else if p-pct = 55.00 then
                         " "
                       else
                         "*".

    display p-pct d-override s-pndinvamt with frame f8-total. 
   end. 

 /*
   if p-pct <= 0 or p-pct >= 100 then do: 
    assign v-error = "--> Margin Pct = " + string(p-pct,"999.99") + 
                     "% - Warning price is not recalculated .". 
    run zsdi_vaerrx.p(v-error,"yes"). 
    next totloop.
   end. 
*/

   if p-costfl and not p-quotefl then do:
    def var blank-desc as logical no-undo. 
    def buffer p-vaspsl for vaspsl. 
    if zsdivasp.stagecd < 4 then do: 
     assign v-error       = "Cst - Stage not open - cannot close.". 
     run zsdi_vaerrx.p(v-error,"yes"). 
     next-prompt p-costfl with frame f8-total.
     next.
    end.
    assign blank-desc = no.  

    if zsdivasp.stagecd le 4 and v-error = "" then do:
      run vaexq-call-edit.p (input g-cono,
                             input x-quoteno,
                             output v-error).
      if v-error <> "" then do:
        assign p-costfl = false.
        assign v-error = 
          "Zero cost lines exist must be costed to leave Costing".
        run zsdi_vaerrx.p(v-error,"yes"). 
        next-prompt p-costfl with frame f8-total.
        next.
      end.
    end.
    
    if zsdivasp.stagecd le 4 and v-error = "" then do:
      run vaexq-nstk-edit.p (input g-cono,
                             input x-quoteno,
                             output v-error).
      if v-error <> "" then do:
        assign p-costfl = false.
        assign v-error = 
          "All NonStock lines must be valid to leave Costing".
        run zsdi_vaerrx.p(v-error,"yes"). 
        next-prompt p-costfl with frame f8-total.
        next.
      end.
    end.
    for each p-vaspsl use-index k-vaspsl where 
             p-vaspsl.cono = g-cono        and 
             p-vaspsl.shipprod = x-quoteno and 
             p-vaspsl.nonstockty ne "l" 
             no-lock:  
     if p-vaspsl.compprod = "" then 
        assign blank-desc = yes. 
    end.  
    if blank-desc = yes then do: 
     assign v-error = "Blank items-can't leave costing".
     run zsdi_vaerrx.p(v-error,"yes"). 
     next-prompt p-costfl with frame f8-total.
     next.
    end.
    if avail vasp and substring(vasp.user3,50,22) = "" then do:        
     assign v-error = "Repair must have Serial# assigned-can't leave costing".
     run zsdi_vaerrx.p(v-error,"yes"). 
     next-prompt p-costfl with frame f8-total.
     next.
    end. 
    
    
    /*
    {w-icsp.i s-prod} 
    if (s-prod = ""  or not avail icsp) then do: 
     assign v-error = "Valid unit required - please enter.". 
     run zsdi_vaerrx.p(v-error,"yes"). 
     next-prompt p-costfl with frame f8-total.
     next. 
    end.  
    */
    if (v-estasy-h = 0 and v-estasy-m = 0) or 
       (v-esttst-h = 0 and v-esttst-m = 0) then do:
     assign v-error = 
     "Assembly and Test Labor required - please enter .". 
     run zsdi_vaerrx.p(v-error,"yes"). 
     next-prompt p-costfl with frame f8-total.
     next. 
    end. 
    def buffer q-ztmk for ztmk. 
    find last ztmk use-index k-ztmk-sc2 where
              ztmk.cono      = g-cono    and 
              ztmk.ordertype = "sc"      and 
              ztmk.user1     = x-quoteno and 
              ztmk.techid    = "admn"    and 
              ztmk.statuscd  = 0         and  
              ztmk.stagecd   = 4           
              exclusive-lock no-error.
    if avail ztmk then do:
     /***********/
     assign ztmk.techid     = "admn"
            ztmk.operinit   = g-operinit 
            ztmk.statuscd   = 5 
            ztmk.punchoutdt = today
            ztmk.punchouttm = substring(string(time,"hh:mm"),1,2) +
                              substring(string(time,"hh:mm"),4,2) +
                              substring(string(time,"hh:mm:ss"),7,2).
     find first vasp where
                vasp.cono = g-cono         and 
                vasp.shipprod = x-quoteno  and 
                vasp.whse = entry(1,x-quoteno,"-")

                no-lock no-error.
     if avail vasp then do:  
      assign v-emailmsg = " " 
             v-emailmsg = g-vaquoteno + " -" +  
                        " Customer Name: " + trim(v-custname) +
                        " Part Number  : " + trim(vasp.refer) + 
                        " - Moved to ToBeQuoted". 
     end. 
   
     assign v-emailmsg = x-quoteno + "^" +
                         entry(1,x-quoteno,"-") + "^" + v-emailmsg.
/*
     run sendemailmsg.p( 2,"cstout",x-whse,"VA","",
                         v-emailmsg,
                         "","",0,0).
*/     
     assign v-stgint = ztmk.stagecd. 
     run get_tagseqno(output v-jobseqno).
     create q-ztmk. 
     assign q-ztmk.jobseqno  = 1 /* v-jobseqno */
            q-ztmk.cono      = g-cono        
            q-ztmk.ordertype = "sc" 
            q-ztmk.user1     = x-quoteno        
            q-ztmk.stagecd   = 5   
            q-ztmk.statuscd  = 0 
            q-ztmk.techid    = "admn" 
            q-ztmk.punchindt = today
            q-ztmk.punchintm = substring(string(time,"hh:mm"),1,2) +
                               substring(string(time,"hh:mm"),4,2) +
                                   substring(string(time,"hh:mm:ss"),7,2)
            q-ztmk.operinit  = g-operinit  
            q-ztmk.transtm   = if q-ztmk.transtm = "" then 
                                substring(string(time,"hh:mm"),1,2) +
                                substring(string(time,"hh:mm"),4,2) 
                               else 
                                q-ztmk.transtm
            q-ztmk.transdt   = if q-ztmk.transdt = ? then 
                                today
                               else 
                                q-ztmk.transdt.
     release ztmk. 
     release q-ztmk. 
     find first vasp where
                vasp.cono = g-cono         and 
                vasp.shipprod = x-quoteno  and 
                vasp.whse = entry(1,x-quoteno,"-")
                exclusive-lock no-error.
      find first zsdivasp where
                zsdivasp.cono = g-cono         and 
                zsdivasp.repairno = x-quoteno  and 
                zsdivasp.whse = entry(1,x-quoteno,"-")
                exclusive-lock no-error.
     if avail vasp then do: 
      assign zsdivasp.stagecd   = 5 
             zsdivasp.costingdt = today 
             vasp.user7 = 5 
             v-stage    = "ToBe-" + v-stage2[int(vasp.user7)].
      display v-stage with frame f-top1.       
      run sendemailmsg.p( 2,"cstout",x-whse,"VA","",
                          v-emailmsg,
                          "","",0,0).
 
     end.
    end. /* avail ztmk */ 
   end. /* if p-costfl */    
   else 
   if p-quotefl then do:  
    if avail zsdivasp and zsdivasp.stagecd < 5 then do: 
     assign v-error = "ToBe-Qtd - Stage not open - cannot close.". 
     run zsdi_vaerrx.p(v-error,"yes").
     next-prompt p-quotefl with frame f8-total.
     next.
    end.
    def buffer a-ztmk for ztmk. 
    find first ztmk use-index k-ztmk-sc2 where
               ztmk.cono      = g-cono    and 
               ztmk.ordertype = "sc"      and 
               ztmk.user1     = x-quoteno and 
               ztmk.statuscd  = 0         and 
               ztmk.stagecd   = 5         and 
               ztmk.orderno   = 0         and 
               ztmk.ordersuf  = 0          
               exclusive-lock no-error.
     if avail ztmk then do:
      /***********/
      assign ztmk.techid     = "admn"
             ztmk.operinit   = g-operinit
             ztmk.statuscd   = 5 
             ztmk.punchoutdt = today
             ztmk.statuscd   = 5 
             ztmk.punchouttm = substring(string(time,"hh:mm"),1,2) +
                               substring(string(time,"hh:mm"),4,2) +
                               substring(string(time,"hh:mm:ss"),7,2).
      /*
      run sendemailmsg.p( 2,"qtdout","madhgts","VA","",
                          g-vaquoteno,
                          "","",0,0).
      */
      assign v-stgint = ztmk.stagecd. 
      run get_tagseqno(output v-jobseqno).
      create a-ztmk. 
      assign a-ztmk.jobseqno  = 1 /* v-jobseqno */
             a-ztmk.cono      = g-cono        
             a-ztmk.ordertype = "sc" 
             a-ztmk.user1     = x-quoteno        
             a-ztmk.stagecd   = 6   
             a-ztmk.statuscd  = 0 
             a-ztmk.techid    = "admn" 
             a-ztmk.punchindt = today
             a-ztmk.punchintm = substring(string(time,"hh:mm"),1,2) +
                                substring(string(time,"hh:mm"),4,2) +
                                substring(string(time,"hh:mm:ss"),7,2)
             a-ztmk.operinit  = g-operinit  
             a-ztmk.transtm   = if a-ztmk.transtm = "" then 
                                 substring(string(time,"hh:mm"),1,2) +
                                 substring(string(time,"hh:mm"),4,2) 
                                else 
                                 a-ztmk.transtm
             a-ztmk.transdt   = if a-ztmk.transdt = ? then 
                                 today
                                else 
                                 a-ztmk.transdt.
      release ztmk. 
      release q-ztmk. 
      if avail vasp then do:  
       find first vasp where
                  vasp.cono = g-cono         and 
                  vasp.shipprod = x-quoteno  and 
                  vasp.whse = entry(1,x-quoteno,"-")
                  exclusive-lock no-error.
       
       find first zsdivasp where
                  zsdivasp.cono = g-cono         and 
                  zsdivasp.repairno = x-quoteno  and 
                  zsdivasp.whse = entry(1,x-quoteno,"-")
                  exclusive-lock no-error.
        
       assign zsdiupdated        = yes
              zsdivasp.stagecd   = 6 
              zsdivasp.costingdt = today 
              zsdivasp.xuser9    = dec(substring(vasp.user1,5,6))
              vasp.user7 = 6 
              v-stage    = "ToBe-" + v-stage2[int(vasp.user7)].
       display v-stage with frame f-top1.       
      end.
     end. /* avail ztmk */  
   end. /* p-quotefl */  
   if p-pct > 0 then do: 
    display p-pct with frame f8-total.
    /** 306 = f6 -- 311 = f11 **/
    if avail vasp and 
     (substring(vasp.user1,1,4)    ne string(s-maxlead,"9999")           or 
      substring(vasp.user1,5,6)    ne string(p-pct,"999.99")              or 
      substring(vasp.user1,30,10)  ne string(f8-listprc,"9999999.99")    or 
      substring(vasp.user1,40,10)  ne string(f8-sellprc,"9999999.99")    or 
      substring(vasp.user1,50,10)  ne string(f8-cost,"9999999.99")       or 
      substring(vasp.user1,60,12)  ne string(f8-vendno,"999999999999")   or 
      substring(vasp.user1,73,4)   ne string(f8-leadtm,"9999")           or
      substring(vasp.user1,150,10) ne string(s-vaprodcost,"9999999.99")  or
      substring(vasp.user1,160,10) ne string(s-pndextrnamt,"999999.99-") or 
      substring(vasp.user1,170,10) ne string(s-listprc,"9999999.99")) then do: 
      find first vasp where
                 vasp.cono = g-cono         and 
                 vasp.shipprod = x-quoteno  and 
                 vasp.whse = entry(1,x-quoteno,"-")
                 exclusive-lock no-error.
      assign overlay(vasp.user1,1,4)    = string(s-maxlead,"9999")
             overlay(vasp.user1,5,6)    = if p-pct < 0 then
                                            string(0,"999.99")
                                          else   
                                            string(p-pct,"999.99")
             overlay(vasp.user1,30,10)  = string(f8-listprc,"9999999.99")
             overlay(vasp.user1,40,10)  = string(f8-sellprc,"9999999.99")
             overlay(vasp.user1,50,10)  = string(f8-cost,"9999999.99")
             overlay(vasp.user1,60,12)  = string(f8-vendno,"999999999999")
             overlay(vasp.user1,73,4)   = string(f8-leadtm,"9999")  
             overlay(vasp.user1,150,10) = string(s-vaprodcost,"9999999.99")
             overlay(vasp.user1,160,10) = string(s-pndextrnamt,"999999.99-")
             overlay(vasp.user1,170,10) = string(s-listprc,"9999999.99").
      run zsdivasp_f8totals.p(recid(vasp),"c").  
  
       
   assign d-override = if s-override <> "" then
                         s-override
                       else if zsdivasp.xuser9 <> 
                               dec(substring(vasp.user1,5,6)) and
                                zsdivasp.stagecd > 5 then
                         "q"
                       else if ((zsdivasp.xuser9 <> 55.00 and 
                                int(substring(vasp.user2,1,7)) <> 0) or
                                (zsdivasp.xuser9 <> 55.00 and 
                                p-quotefl) or
                                zsdivasp.stagecd < 6 ) and
                             dec(substring(vasp.user1,5,6)) <> 55.00 then
                         "m"
                        else if p-pct = 55.00 then
                         " "
                        else
                         "*".
      
      display s-vareqshipdt
              s-vaprodcost
              s-vapromisedt
              s-pndinvamt      
              s-pndextrnamt   
              s-pndintrndsp    
              s-pndtotal
              d-override 
              s-listprc
              p-pct
              p-costfl 
              p-quotefl
              s-maxlead
              f8-listprc
              f8-sellprc
              f8-cost
              f8-vendno
              f8-leadtm
              with frame f8-total. 
    end. 
    leave totloop. 
   end. 

   end. /* totloop */
   /**** end of f8 update loop ****/
   
   if avail vasp then 
    run zsdivasp_f8totals.p(recid(vasp),"c"). 
   
   if avail vasp then 
    release vasp. 
   if lastkey = 404 or lastkey = 311 then do: 
    if lastkey = 404 then do: 
{&vaixicom}


     assign v-lmode           = "c"
            v-framestate      = "m1"
            v-currentkeyplace = 3.

/{&vaixicom}* */

     pause 0.
    end.
    hide frame f8-total.
    apply x-cursorup to b-lines in frame f-lines.
    put screen row 22 col 1 color message 
"  F6-Top  F7-Lines  F8-Totals  F9-SPcNotes  F10-PoComments  Ctrl-p(Prt label) ~   ".
     leave.
   end.
   if (p-quotefl and zsdiupdated) and 
       not ({k-func6.i} or {k-func9.i}) then do: 
    run f6-global.     
   end.
   else 
   if {k-func6.i} then do: 
    if p-quotefl and zsdiupdated then 
     run f6-global.     
    hide frame f-mid1.
    run f6-comments.
    pause 0.
    if lastkey = 401 then do:
       if avail vasp and 
       (substring(vasp.user1,1,4)    ne string(s-maxlead,"9999")           or 
        substring(vasp.user1,5,6)    ne string(p-pct,"999.99")             or 
        substring(vasp.user1,30,10)  ne string(f8-listprc,"9999999.99")    or 
        substring(vasp.user1,40,10)  ne string(f8-sellprc,"9999999.99")    or 
        substring(vasp.user1,50,10)  ne string(f8-cost,"9999999.99")       or 
        substring(vasp.user1,60,12)  ne string(f8-vendno,"999999999999")   or 
        substring(vasp.user1,73,4)   ne string(f8-leadtm,"9999")           or
        substring(vasp.user1,150,10) ne string(s-vaprodcost,"9999999.99")  or
        substring(vasp.user1,160,10) ne string(s-pndextrnamt,"999999.99-") or 
        substring(vasp.user1,170,10) ne string(s-listprc,"9999999.99"))
        then do:  
        find first vasp where
                   vasp.cono = g-cono         and 
                   vasp.shipprod = x-quoteno  and 
                   vasp.whse = entry(1,x-quoteno,"-")
                   exclusive-lock no-error.
        assign overlay(vasp.user1,1,4)   = string(s-maxlead,"9999")
               overlay(vasp.user1,5,6)   = if p-pct < 0 then                                              string(0,"999.99")
                                           else
                                             string(p-pct,"999.99")
               overlay(vasp.user1,30,10) = string(f8-listprc,"9999999.99")
               overlay(vasp.user1,40,10) = string(f8-sellprc,"9999999.99")
               overlay(vasp.user1,50,10) = string(f8-cost,"9999999.99")
               overlay(vasp.user1,60,12) = string(f8-vendno,"999999999999")
               overlay(vasp.user1,73,4)  = string(f8-leadtm,"9999")  
               overlay(vasp.user1,150,10) = 
                                          string(s-vaprodcost,"9999999.99")
               overlay(vasp.user1,160,10) = 
                                          string(s-pndextrnamt,"999999.99-")
               overlay(vasp.user1,170,10) =                         
                                          string(s-listprc,"9999999.99").
        run zsdivasp_f8totals.p(recid(vasp),"c").  
        release vasp. 
       end. 
    end. 
    assign f8-title =
 " F6-ExternalNotes F9-PrintQuote F10-UPDATE TOTALINFO Ctrl-n-InternalNotes  ".
    display f8-title with frame f8-total. 
    next.
   end.
   else 
   if {k-func9.i} then do:  
 
     find first vasp where
                vasp.cono = g-cono         and 
                vasp.shipprod = x-quoteno  and 
                vasp.whse = entry(1,x-quoteno,"-")
                no-error.
            
     find first zsdivasp where
                zsdivasp.cono = g-cono         and 
                zsdivasp.repairno = x-quoteno  and 
                zsdivasp.whse = entry(1,x-quoteno,"-")
                no-error.
    
    assign f8-title = 
 " F6-ExternalNotes F9-PRINTQUOTE F10-Update TotalInfo Ctrl-n-InternalNotes  ".
    display f8-title with frame f8-total. 
    if p-quotefl and zsdiupdated then 
     run f6-global.     

    assign d-override = if s-override <> "" then
                         s-override
                       else if zsdivasp.xuser9 <> 
                               dec(substring(vasp.user1,5,6)) and
                                zsdivasp.stagecd > 5 then
                         "q"
                       else if ((zsdivasp.xuser9 <> 55.00 and 
                                int(substring(vasp.user2,1,7)) <> 0) or
                                (zsdivasp.xuser9 <> 55.00 and 
                                p-quotefl) or
                                zsdivasp.stagecd < 6 ) and
                             dec(substring(vasp.user1,5,6)) <> 55.00 then
                         "m"
                        else if p-pct = 55.00 then
                         " "
                        else
                         "*".
    
    display s-vareqshipdt
            s-vaprodcost
            s-vapromisedt
            s-pndinvamt      
            s-pndextrnamt   
            s-pndintrndsp    
            s-pndtotal
            d-override 
            s-listprc
            p-pct
            s-maxlead
            f8-listprc
            f8-sellprc
            f8-cost
            f8-vendno
            f8-leadtm
            with frame f8-total. 
     
     {w-sasoo.i g-operinits no-lock}
     if avail sasoo then do: 
      assign s-printernm = sasoo.printernm.
      release sasoo.
     end.   
     prtloop:
     do while true on endkey undo, leave prtloop:
      update s-printernm with frame f-printer2
      editing:
      readkey.
       /* trap f4(cancel) so updates will take on f1 then f4 */ 
       if {k-cancel.i} then do: 
        leave prtloop.
       end. 
       else 
        apply lastkey.  
      end. /* editing */.

      if (s-printernm = "email" or s-printernm = "e-mail" or 
          s-printernm = "fax") then do: 
         assign sendit   = no
                g-vendno = 0.
         def var x-printernm like s-printernm.
         assign x-printernm = s-printernm
                sendtyp     = s-printernm. 
         run vaexq90sdi.p(input "svcq",
                          input-output x-printernm,
                          input-output g-vendno,
                          input-output sendtyp,
                          input-output sendit).
         if not sendit then 
          pause 0.
         else  
         if sendit then do: 
          if sendtyp = "e-mail" or 
             sendtyp = "email" then 
            status default
                "E-mailing repair quote " + g-vaquoteno + " in progress..".
          else 
          if sendtyp = "fax" then 
            status default
                "Faxing repair quote " + g-vaquoteno + " in progress..".
          assign x-printernm = sendtyp. 
          run prt-routine2(input x-printernm,input no).
         end. 
      leave prtloop.
      end.   
      {w-sasp.i s-printernm}
      if not avail sasp then do:  
       assign v-error = "Invalid Printer - Re-enter".
       run zsdi_vaerrx.p(v-error,"yes"). 
       next.
      end. 
      if num-entries(g-vaquoteno,"-") = 2 and 
         (entry(1,g-vaquoteno,"-") = "" or 
          entry(2,g-vaquoteno,"-") = "") then do:
       assign v-error = "Invalid quote No. - Re-enter". 
       hide frame f-printer2.
       hide frame f8-total.
       next.
      end. 
      assign sendit = no. 
      if s-printernm ne "vid" then 
       run prt-routine2(input s-printernm,input no).
      leave prtloop.
     end. /* end do while prtloop  */
     status default "                                                    ".
     pause 0.
     display with frame f-top1.
     assign f8-title =
 " F6-ExternalNotes F9-PrintQuote F10-UPDATE TOTALINFO Ctrl-n-InternalNotes  ".
     display f8-title with frame f8-total. 
     next. 
   end. 
 /* assign p-quotefl = no. */
  if lastkey = 401 then 
   leave f8loop. 
  end. /* repeat */
  if avail vasp then 
   release vasp. 
  display b-lines with frame f-lines.
{&vaixicom}

  assign v-framestate = "m1"
         v-CurrentKeyPlace = 1.

/{&vaixicom}* */


  on cursor-up cursor-up. 
  on cursor-down cursor-down.
{&vaixicom}

  if o-framenm = "f-top1" then do:  
   hide frame f-lines. 
   display fs-1 with frame f-statusline.
  end. 
/{&vaixicom}* */

  
  leave.
end.  
end. 

