/* oectrl_a.i 04/14/00 */
/*h**************************************************************************
  INCLUDE      : oecntl_a.i
  DESCRIPTION  : Include for User Hook p-oeetlu.z99 
  USED ONCE?   : yes (xxlix.i calls oictrl_a.i)
                 Any changes made to oectrl_a.i should be made in oictrl_a.i
  AUTHOR       : das 
  DATE WRITTEN : 04/14/00 
  CHANGES MADE : 
    04/14/00 das; Allow user to create or view CSR comments to buyers when 
                  CNTL-A is pressed.  The Comments appear on the RARR. 
*****************************************************************************/

/* Notes on RARR */
   define buffer rarr-com for com.
 def var k-prodcst     as c format "x(12)". 
 def var k-shipfmno    as c format "x(4)". 
 def var k-reqshipdt   as c format "x(8)". 
 def var k-arpvendno   as c format "x(12)". 
 
/* def shared var g-operinit as c format "x(4)". */
                                                                                
 def var r-shipprod    like oeel.shipprod            no-undo. 
 def var r-arpvendno   like oeel.arpvendno           no-undo. 
 def var r-prodcst     like oeel.prodcost            no-undo. 
 def var r-shipfmno    like oeel.shipfmno            no-undo. 
 def var r-reqshipdt   like oeel.reqshipdt           no-undo. 
 def var r-arpprodline like oeel.arpprodline         no-undo. 
 def var r-shipvia     as c format "x(4)"            no-undo. 
 def var r-whse        like oeel.altwhse             no-undo. 
 def var r-rush        as logical                    no-undo. 
 def var r-comments1   as c format "x(60)"           no-undo.
 def var r-comments2   as c format "x(60)"           no-undo.
 def var r-comments3   as c format "x(60)"           no-undo.
 def var r-comments4   as c format "x(60)"           no-undo. 
 def var r-comments5   as c format "x(60)"           no-undo.
 def var r-comments6   as c format "x(60)"           no-undo. 
 def var r-comments7   as c format "x(60)"           no-undo.
 
form 
   r-shipprod        at 20    label "Product   " 
   skip 
   skip 
   r-arpvendno       at 10    label "ARP Vendor" 
   r-prodcst         at 35    label "Cost      " 
   r-shipfmno        at 10    label "Ship From " 
   r-reqshipdt       at 35    label "Due Date  "
   r-whse            at 10    label "ARP Whse  " 
   r-shipvia         at 35    label "Ship Via  " 
   r-arpprodline     at 10    label "Prod Line " 
   r-rush            at 35    label "Rush      " 
   "Comments  :"      at 10  
   r-comments1       at 3    no-label 
   r-comments2       at 3    no-label 
   r-comments3       at 3    no-label  
   r-comments4       at 3    no-label  
   r-comments5       at 3    no-label  
   r-comments6       at 3    no-label  
   with frame f-rarrnotes overlay side-labels width 65 column 8 row 7 
        title " Custom Order Comments for Purchasing ". 

  find rarr-com use-index k-com where rarr-com.cono = g-cono      and 
                                      rarr-com.comtype    = "puq" and 
                                      rarr-com.orderno    = 
                                          int(oeelb.batchnm)      and
                                      rarr-com.ordersuf   = 0     and
                                      rarr-com.lineno     = oeelb.lineno
                                      no-lock no-error.
  if avail rarr-com then do:
         assign k-arpvendno   = substring(rarr-com.noteln[9],17,12).
         assign k-prodcst     = substring(rarr-com.noteln[10],17,12).
         assign k-shipfmno    = substring(rarr-com.noteln[11],17,4).
         assign k-reqshipdt   = substring(rarr-com.noteln[12],17,8).
         
         assign r-shipprod    = oeelb.shipprod.
         assign r-arpvendno   = decimal(k-arpvendno).
         assign r-prodcst     = decimal(k-prodcst).
         assign r-shipfmno    = integer(k-shipfmno).
         assign r-reqshipdt   = date(k-reqshipdt).
         assign r-arpprodline = substring(rarr-com.noteln[15],17,6).
         assign r-shipvia     = substring(rarr-com.noteln[14],17,4).
         assign r-whse        = substring(rarr-com.noteln[13],17,4).
         assign r-rush        = if substring(rarr-com.noteln[16],17,3) = "yes"
                                   then yes else no.
         assign r-comments1 = rarr-com.noteln[1]
                r-comments2 = rarr-com.noteln[2]
                r-comments3 = rarr-com.noteln[3]
                r-comments4 = rarr-com.noteln[4]
                r-comments5 = rarr-com.noteln[5]
                r-comments6 = rarr-com.noteln[6].
/*                r-comments7 = rarr-com.noteln[7]. */
         
         view frame f-rarrnotes.
         display
         r-shipprod 
         r-arpvendno
         r-prodcst
         r-shipfmno
         r-reqshipdt
         r-shipvia
         r-whse
         r-arpprodline
         r-rush
         r-comments1
         r-comments2
         r-comments3
         r-comments4
         r-comments5
         r-comments6
      with frame f-rarrnotes.
      pause.
      hide frame f-rarrnotes.
  end.
      
/*   if lastkey = 1 then
     do:
     display "Frame" at 1
             "To" at 1
             "Be:" at 1
             "Displayed" at 1 with frame xx overlay.
     o-frame-value = "".
     pause.
     end.        */
