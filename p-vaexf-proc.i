/* *************************************************************************
PROGRAM NAME: p-vaexf-proc.i
PROJECT NO.:
DESCRIPTION:  Internal Procedures for VAEXF
DATE WRITTEN: 06/24/10
AUTHOR      : based on VAEXQ written by Tony Abbate
              Tom Herzog
Modifications:

tahtrain : training bugs



***************************************************************************/

/*-------------------------------------------------------------------------*/
PROCEDURE log_error:
/*-------------------------------------------------------------------------*/
  DO:
    OUTPUT STREAM errout TO "/home/ftoa/vaexq/errorsout" APPEND.
    EXPORT STREAM errout v-error SKIP.
    OUTPUT STREAM errout CLOSE.
    
  END.
END.

/*-------------------------------------------------------------------------*/
PROCEDURE Create_Browse_Lines:
/*-------------------------------------------------------------------------*/
  DO:
    DEFINE INPUT PARAMETER c-found AS LOGICAL NO-UNDO.
    FOR EACH t-lines use-index ix1 EXCLUSIVE-LOCK:
      DELETE t-lines.
    END.
    FOR EACH t-vaspmap EXCLUSIVE-LOCK:
      DELETE t-vaspmap.
    END.
    ASSIGN 
      v-lmode     = "c"
      v-labormins = 0
      v-drawingmins = 0
      v-electricalmins = 0
      v-mechanicalmins = 0
      v-docmins     = 0.
/* tahtrain */
    if x-quoteno = "" then do:
      Message "Create_Browse_lines quote not established".
      pause.
      return.
    end.  
/* tahtrain */
/* Locate Inventory section from Template */    
    find zsdivaspmap where
         zsdivaspmap.cono = g-cono and
         zsdivaspmap.rectype  = "S" and        /* VAES */
         zsdivaspmap.prod     = x-quoteno  and
         zsdivaspmap.typename = "INVENTORY" and
         zsdivaspmap.lineno = 0  no-lock  no-error.
/* REMINDER INDEX NEEDED */         
    if not avail zsdivaspmap then do:
      message "program error no zsdivaspmap".
      pause 10.
      return.       
    end.
    FOR EACH vaspsl USE-INDEX k-vaspsl WHERE
             vaspsl.cono     = g-cono    AND
             vaspsl.shipprod = x-quoteno AND
             vaspsl.whse     = x-whse    AND
             vaspsl.seqno    = zsdivaspmap.seqno      /* Line Items */
      NO-LOCK:
      IF SUBSTRING(vaspsl.user4,1,4) NE "" THEN
        ASSIGN l-whse = SUBSTRING(vaspsl.user4,1,4).
      ELSE
        ASSIGN l-whse = STRING(x-whse,"x(4)").
      {w-icsw.i vaspsl.compprod l-whse NO-LOCK}
      
      IF avail icsw OR vaspsl.arpvendno > 0 THEN
        FIND FIRST apsv WHERE
                   apsv.cono   = g-cono AND
                   apsv.vendno = vaspsl.arpvendno
        NO-LOCK NO-ERROR.
      
      {w-icsp.i vaspsl.compprod NO-LOCK}
      RUN NoteDisplay3(INPUT vaspsl.compprod).

      CREATE t-lines.
      ASSIGN 
        t-lines.leadtm     = string(int(SUBSTRING(vaspsl.user5,1,4)),"9999")
        t-lines.vendno     = vaspsl.arpvendno
        t-lines.vendnm     = IF avail apsv AND
                             vaspsl.compprod NE "" THEN
                               apsv.lookupnm
                             ELSE
                               ""
        t-lines.gain       = vaspsl.xxde1
        t-lines.vaspslid   = RECID(vaspsl)
        t-lines.prod       = vaspsl.compprod
        t-lines.attrfl     = if substring(vaspsl.user3,1,1) = "y" then 
                               yes 
                             else 
                               no
        t-lines.astr1      = v-astr3
        t-lines.prodline   = vaspsl.arpprodline
        t-lines.ZsdiXrefType = substring(vaspsl.user5,5,2)
        t-lines.prodcat    = vaspsl.prodcat
        t-lines.prctype    = IF vaspsl.nonstockty NE "n"
                             AND avail icsw  THEN
                               icsw.pricetype
                             ELSE
                             IF vaspsl.nonstockty = "l" THEN
                               "l"
                             ELSE
                               ""
        t-lines.lineno     = vaspsl.lineno
        t-lines.descrip    = vaspsl.proddesc
        t-lines.prodcost   = vaspsl.prodcost
        t-lines.qty        = vaspsl.qtyneeded
        t-lines.extcost    = t-lines.prodcost * t-lines.qty
        t-lines.listprc    = if vaspsl.nonstockty = "n" then
                               vaspsl.user7
                             else
                             IF avail icsw THEN
                               icsw.listprice
                             ELSE
                               v-nslistprc
        t-lines.unit       = vaspsl.unit
        t-lines.icspstat   = IF avail icsp THEN
                               icsp.statustype
                             ELSE
                               ""
        t-lines.specnstype = IF vaspsl.nonstockty NE "" THEN
                               vaspsl.nonstockty
                             ELSE
                             IF avail icsw AND icsw.statustype = "O" THEN
                               "s"
                             ELSE
                             IF avail icsw AND icsw.statustype NE "O" THEN
                               " "
                             ELSE
                               vaspsl.nonstockty
        t-lines.altwhse    = SUBSTRING(vaspsl.user4,1,4)
        t-lines.whse       = IF SUBSTRING(vaspsl.user4,1,4) NE "" THEN
                               SUBSTRING(vaspsl.user4,1,4)
                             ELSE
                               STRING(vaspsl.whse,"x(4)").
   

/* REMINDER INDEX */

      find first bs-zsdivaspmap where
                 bs-zsdivaspmap.cono = g-cono and
                 bs-zsdivaspmap.rectype     = "SQ"         and    /* VAES */
                 bs-zsdivaspmap.prod        = x-quoteno    and
                 bs-zsdivaspmap.description = "ADDON"      and
                 bs-zsdivaspmap.seqno       = vaspsl.seqno and
                 bs-zsdivaspmap.lineno = 0  no-lock no-error.

      if avail bs-zsdivaspmap then do:
        find first bs-zsdivaspmap where
                 bs-zsdivaspmap.cono = g-cono and
                 bs-zsdivaspmap.rectype     = "SQ"         and    /* VAES */
                 bs-zsdivaspmap.prod        = x-quoteno    and
                 bs-zsdivaspmap.description = "ADDON"      and
                 bs-zsdivaspmap.seqno       = vaspsl.seqno and
                 bs-zsdivaspmap.lineno = 0  no-lock no-error.
        CREATE t-vaspmap.
        BUFFER-COPY bs-zsdivaspmap
         TO t-vaspmap
         ASSIGN
           t-vaspmap.addonamt[2] = t-vaspmap.addonamt[1]
           t-vaspmap.addon[2] = t-vaspmap.addon[1]
           t-vaspmap.addon[4] = t-vaspmap.addon[3].

      end.
      ELSE
/* Possible sync problem with map rebuild from header */    
      if not avail bs-zsdivaspmap then do:
/* Create addon structure for the line item     */
        find first bs-zsdivaspmap where
                   bs-zsdivaspmap.cono = g-cono            and
                   bs-zsdivaspmap.rectype     = "S"        and      /* VAES */
                   bs-zsdivaspmap.prod        = x-quoteno  and
                   bs-zsdivaspmap.seqno       = vaspsl.seqno and
                   bs-zsdivaspmap.lineno = 0  no-lock no-error.
          CREATE bs2-zsdivaspmap.
          BUFFER-COPY bs-zsdivaspmap
           EXCEPT bs-zsdivaspmap.rectype
                  bs-zsdivaspmap.description
                  bs-zsdivaspmap.seqno 
                  bs-zsdivaspmap.typename
  
          TO bs2-zsdivaspmap
             assign bs2-zsdivaspmap.rectype  = "SQ"
                    bs2-zsdivaspmap.description = "ADDON"
                    bs2-zsdivaspmap.seqno    = vaspsl.seqno
                    bs2-zsdivaspmap.typename = "SectionAddon"
                    bs2-zsdivaspmap.addonamt[2] = 0
                    bs2-zsdivaspmap.addon[2] = 0
                    bs2-zsdivaspmap.addon[4] = 0
                    bs2-zsdivaspmap.user3    = 
                       string(bs-zsdivaspmap.seqno,"9999").
          CREATE t-vaspmap.
          BUFFER-COPY bs-zsdivaspmap
           EXCEPT bs-zsdivaspmap.rectype
                  bs-zsdivaspmap.description
                  bs-zsdivaspmap.seqno 
                  bs-zsdivaspmap.typename
  
          TO t-vaspmap
             assign t-vaspmap.rectype  = "SQ"
                    t-vaspmap.description = "ADDON"
                    t-vaspmap.seqno    = vaspsl.seqno
                    t-vaspmap.typename = "SectionAddon"
                    t-vaspmap.addonamt[2] = 0
                    t-vaspmap.addon[2] = 0
                    t-vaspmap.addon[4] = 0
                    t-vaspmap.user3    = string(bs-zsdivaspmap.seqno,"9999").
      end.
 
      find first bl-zsdivaspmap where
                 bl-zsdivaspmap.cono = g-cono and
                 bl-zsdivaspmap.rectype     = "LQ" and        /* VAES */
                 bl-zsdivaspmap.prod        = x-quoteno  and
                 bl-zsdivaspmap.description = "ADDON" and
                 bl-zsdivaspmap.seqno  = vaspsl.seqno and
                 bl-zsdivaspmap.lineno = vaspsl.lineno  no-lock no-error.
  
      if avail bl-zsdivaspmap then do:
/* Create addon structure for the line item     */
        for each bs-zsdivaspmap where
                 bs-zsdivaspmap.cono = g-cono and
                 bs-zsdivaspmap.rectype     = "SQ"       and         /* VAES */
                 bs-zsdivaspmap.prod        = x-quoteno  and
                 bs-zsdivaspmap.description = "ADDON"    and
                 bs-zsdivaspmap.seqno       = vaspsl.seqno and
                 bs-zsdivaspmap.lineno = 0  no-lock:
 
          for each bl-zsdivaspmap where
                   bl-zsdivaspmap.cono        = g-cono and
                   bl-zsdivaspmap.rectype     = "LQ" and        /* VAESl */
                   bl-zsdivaspmap.prod        = x-quoteno  and
                   bl-zsdivaspmap.seqno       = bs-zsdivaspmap.seqno  and
                   bl-zsdivaspmap.lineno      = vaspsl.lineno  no-lock:
           CREATE t-vaspmap.
            BUFFER-COPY bl-zsdivaspmap
           TO t-vaspmap
             ASSIGN
               t-vaspmap.qty         = vaspsl.qtyneeded
               t-vaspmap.addonamt[2] = t-vaspmap.addonamt[1]
               t-vaspmap.addon[2]    = t-vaspmap.addon[1]
               t-vaspmap.addon[3]    = vaspsl.qtyneeded
               t-vaspmap.addon[4]    = t-vaspmap.addon[3]. /* Qty */

          end.
        end.
      end.
      ELSE
/* Possible sync problem with map rebuild from header */    
      if not avail bl-zsdivaspmap then do:
  
/* Create addon structure for the line item     */
        for each bs-zsdivaspmap where
                 bs-zsdivaspmap.cono = g-cono and
                 bs-zsdivaspmap.rectype     = "S" and        /* VAES */
                 bs-zsdivaspmap.prod        = x-quoteno  and
                 bs-zsdivaspmap.description = "ADDON" and
                 bs-zsdivaspmap.lineno = 0  no-lock:
  
          for each bl-zsdivaspmap where
                   bl-zsdivaspmap.cono = g-cono and
                   bl-zsdivaspmap.rectype     = "L" and        /* VAESl */
                   bl-zsdivaspmap.prod        = x-quoteno  and
                   bl-zsdivaspmap.seqno       = bs-zsdivaspmap.seqno  no-lock:
            CREATE t-vaspmap.
            BUFFER-COPY bl-zsdivaspmap
             EXCEPT bl-zsdivaspmap.rectype
                    bl-zsdivaspmap.seqno
                    bl-zsdivaspmap.lineno  
            TO t-vaspmap
             assign t-vaspmap.rectype  = "LQ"
                    t-vaspmap.seqno    = vaspsl.seqno
                    t-vaspmap.lineno   = vaspsl.lineno
                    t-vaspmap.addonamt[2] = 0
                    t-vaspmap.addon[2]    = 0
                    t-vaspmap.addon[4]    = 0
                    t-vaspmap.addon[3]    = vaspsl.qtyneeded
                    t-vaspmap.qty        = vaspsl.qtyneeded
                    t-vaspmap.user3      = string(bs-zsdivaspmap.seqno,"9999")
                    t-vaspmap.user4   = string(bl-zsdivaspmap.lineno,"9999").
 
          end.
        end.
      end.

    END.
    
    IF (CAN-DO("f7",KEYLABEL(LASTKEY))     OR
        CAN-DO("f8",KEYLABEL(LASTKEY))     OR
        CAN-DO("f9",KEYLABEL(LASTKEY))     OR
        CAN-DO("f10",KEYLABEL(LASTKEY))    OR
        CAN-DO("ctrl-d",KEYLABEL(LASTKEY)) OR
        CAN-DO("ctrl-o",KEYLABEL(LASTKEY))) THEN
      ASSIGN v-lmode = "c".
    
    IF c-found = YES THEN DO:
      ASSIGN o-lineno = 0.
      OPEN QUERY q-lines FOR EACH t-lines use-index ix1.
      b-lines:REFRESHABLE IN FRAME f-lines = FALSE.
      DISPLAY b-lines WITH FRAME f-lines.
      b-lines:REFRESHABLE IN FRAME f-lines  = TRUE.
      ENABLE b-lines WITH FRAME f-lines.
    END.
  END.
END.


/*-------------------------------------------------------------------------*/
PROCEDURE Create-Vaspmap:
/*-------------------------------------------------------------------------*/
 
  DO:
    DEFINE INPUT PARAMETER ip-mode   AS CHAR        NO-UNDO.
    DEFINE INPUT PARAMETER ip-lineno as integer     no-undo.
    DEFINE INPUT PARAMETER ip-prod   like icsp.prod no-undo.
    DEFINE INPUT PARAMETER ip-whse   like icsw.whse no-undo.
    DEFINE INPUT PARAMETER ip-qty    like vaspsl.qtyneeded no-undo.
    DEFINE INPUT PARAMETER ip-nslistprc  like icsw.listprice no-undo.
    DEFINE INPUT PARAMETER ip-specnstype  like vaspsl.nonstockty no-undo.



    DEFINE BUFFER ib-icsp for icsp. 
    ASSIGN 
      v-labormins = 0
      v-drawingmins = 0
      v-docmins     = 0
      v-electricalmins = 0
      v-mechanicalmins = 0.

/* Locate Inventory section from Template */    
    find zsdivaspmap where
         zsdivaspmap.cono = g-cono and
         zsdivaspmap.rectype  = "S" and        /* VAES */
         zsdivaspmap.prod     = x-quoteno  and
         zsdivaspmap.typename = "INVENTORY" and
         zsdivaspmap.lineno = 0  no-lock  no-error.
/* REMINDER INDEX NEEDED */         
    if not avail zsdivaspmap then do:
      message "program error no zsdivaspmap".
      pause 10.
      return.       
    end.

/* If there are not database updates then just clear the 
   uncommitted line and re-add.
*/  
    if avail zsdivaspmap and (ip-mode = "repl" or
                             ip-mode = "new") then do:
      for each t-vaspmap where
               t-vaspmap.cono = g-cono and
               t-vaspmap.rectype  = "LQ" and        /* VAES */
               t-vaspmap.prod     = x-quoteno  and
               t-vaspmap.seqno    = zsdivaspmap.seqno and
               t-vaspmap.lineno   = ip-lineno :
           delete t-vaspmap.
      end.
    end.
 
/* REMINDER INDEX */

    find first t-vaspmap where
               t-vaspmap.cono = g-cono and
               t-vaspmap.rectype     = "SQ"         and    /* VAES */
               t-vaspmap.prod        = x-quoteno    and
               t-vaspmap.description = "ADDON"      and
               t-vaspmap.seqno       = zsdivaspmap.seqno and
               t-vaspmap.lineno = 0  no-lock no-error.

/* Possible sync problem with map rebuild from header */    
    if not avail t-vaspmap then do:
/* Create addon structure for the line item     */
      find first bs-zsdivaspmap where
                 bs-zsdivaspmap.cono = g-cono            and
                 bs-zsdivaspmap.rectype     = "S"        and      /* VAES */
                 bs-zsdivaspmap.prod        = x-quoteno  and
                 bs-zsdivaspmap.seqno       = zsdivaspmap.seqno    and
                 bs-zsdivaspmap.lineno = 0  no-lock no-error.

        CREATE t-vaspmap.
        BUFFER-COPY bs-zsdivaspmap
         EXCEPT bs-zsdivaspmap.rectype
                bs-zsdivaspmap.description
                bs-zsdivaspmap.seqno 
                bs-zsdivaspmap.typename
         TO t-vaspmap
           assign t-vaspmap.rectype  = "SQ"
                  t-vaspmap.description = "ADDON"
                  t-vaspmap.seqno    = zsdivaspmap.seqno
                  t-vaspmap.typename = "SectionAddon"
                  t-vaspmap.user3   = string(bs-zsdivaspmap.seqno,"9999")
                  t-vaspmap.user4   = string(0,"9999").
    end.
 
    find first t-vaspmap where
               t-vaspmap.cono = g-cono and
               t-vaspmap.rectype     = "LQ" and        /* VAES */
               t-vaspmap.prod        = x-quoteno  and
               t-vaspmap.description = "ADDON" and
               t-vaspmap.seqno       = zsdivaspmap.seqno and
               t-vaspmap.lineno      = ip-lineno  no-lock no-error.

    if not avail t-vaspmap then do:

/* Create addon structure for the line item     */
      for each bs-zsdivaspmap where
               bs-zsdivaspmap.cono = g-cono and
               bs-zsdivaspmap.rectype     = "S"        and         /* VAES */
               bs-zsdivaspmap.prod        = x-quoteno  and
               bs-zsdivaspmap.description = "ADDON"    and
               bs-zsdivaspmap.lineno = 0  no-lock:
        for each bl-zsdivaspmap where
                 bl-zsdivaspmap.cono = g-cono and
                 bl-zsdivaspmap.rectype     = "L" and              /* VAESl */
                 bl-zsdivaspmap.prod        = x-quoteno  and
                 bl-zsdivaspmap.seqno       = bs-zsdivaspmap.seqno  no-lock:
          CREATE t-vaspmap.
          BUFFER-COPY bl-zsdivaspmap
            EXCEPT bl-zsdivaspmap.rectype
                   bl-zsdivaspmap.seqno
                   bl-zsdivaspmap.description
                   bl-zsdivaspmap.lineno  
          TO t-vaspmap
           assign t-vaspmap.rectype  = "LQ"
                  t-vaspmap.seqno    = zsdivaspmap.seqno
                  t-vaspmap.description = "ADDON"
                  t-vaspmap.lineno   = ip-lineno
                  t-vaspmap.addon[2] = 0
                  t-vaspmap.addonamt[2] = 0
                  t-vaspmap.addon[3] = ip-qty
                  t-vaspmap.qty = ip-qty
                  t-vaspmap.user3   = string(bs-zsdivaspmap.seqno,"9999")
                  t-vaspmap.user4   = string(bl-zsdivaspmap.lineno,"9999").
          if ip-prod <> "" then do:
  
            run vaexf_labor.p (input "a",
                               output v-labormins,
                               output v-docmins,
                               output v-drawingmins,
                               output v-electricalmins,
                               output v-mechanicalmins).
            
            find ib-icsp where 
                 ib-icsp.cono = g-cono and
                 ib-icsp.prod = ip-prod no-lock no-error.
            IF AVAIL ib-icsp THEN DO:  
              if num-entries(ib-icsp.user21,"~011") > 0 then
                v-labormins = dec(entry(1,ib-icsp.user21,"~011")).    
              if num-entries(ib-icsp.user21,"~011") > 1 then
                v-docmins   = dec(entry(2,ib-icsp.user21,"~011")).
              if num-entries(ib-icsp.user21,"~011") > 2 then
                v-drawingmins = dec(entry(3,ib-icsp.user21,"~011")).
              if num-entries(ib-icsp.user21,"~011") > 3 then
                v-electricalmins = dec(entry(4,ib-icsp.user21,"~011")).
              if num-entries(ib-icsp.user21,"~011") > 4 then
                v-Mechanicalmins = dec(entry(5,ib-icsp.user21,"~011")).
               
            END.
/*  
            ELSE
              ASSIGN 
                v-labormins = 0
                v-docmins   = 0
                v-drawingmins = 0
                v-electricalmins = 0
                v-mechanicalmins = 0.
*/                     
            assign t-vaspmap.user10 = ip-prod.  
            if t-vaspmap.typename = "Labor" and v-labormins <> 0 then
              assign t-vaspmap.addon[1] = v-labormins.
            if t-vaspmap.typename = "Documentation" and v-docmins <> 0 then
              assign t-vaspmap.addon[1] = v-docmins.
            if t-vaspmap.typename = "Drawing Fee" and v-drawingmins <> 0 then
              assign t-vaspmap.addon[1] = v-drawingmins.
            if t-vaspmap.typename = "Electrical" and
                v-electricalmins <> 0 then
              assign t-vaspmap.addon[1] = v-electricalmins.

            if t-vaspmap.typename = "Mechanical" 
               and v-mechanicalmins <> 0 then
              assign t-vaspmap.addon[1] = v-mechanicalmins.


            if t-vaspmap.typename = "Rebate"  then do:
              run VaspMap-Rebate (input ip-mode,
                                  input ip-lineno,
                                  input ip-prod,
                                  input ip-whse,
                                  output v-rebateamt,
                                  input ip-nslistprc).
          
              assign t-vaspmap.addon[1] = v-rebateamt.
            end.
          end.
        end. /* For each "L" */
      end.   /* For each "S" */

      RUN VaspMap-Update (input ip-mode,
                          input zsdivaspmap.seqno,
                          input ip-lineno,
                          input ip-qty).

    end.  /* Not Avail */
    else do: /* updt or lost */

      for each   t-vaspmap where
                 t-vaspmap.cono = g-cono and
                 t-vaspmap.rectype     = "LQ" and        /* VAES */
                 t-vaspmap.prod        = x-quoteno  and
                 t-vaspmap.description = "ADDON" and
                 t-vaspmap.seqno       = zsdivaspmap.seqno and
                 t-vaspmap.lineno      = ip-lineno :
        if ip-prod <> "" and
          (t-vaspmap.user10 = "" or
           ip-prod <> t-vaspmap.user10) then do:
       
            
          run vaexf_labor.p (input "a",
                             output v-labormins,
                             output v-docmins,
                             output v-drawingmins,
                             output v-electricalmins,
                             output v-mechanicalmins).
 
          
          find ib-icsp where 
               ib-icsp.cono = g-cono and
               ib-icsp.prod = ip-prod no-lock no-error.
          IF AVAIL ib-icsp THEN DO:  
            if num-entries(ib-icsp.user21,"~011") > 0 then
              v-labormins = dec(entry(1,ib-icsp.user21,"~011")).    
            if num-entries(ib-icsp.user21,"~011") > 1 then
              v-docmins   = dec(entry(2,ib-icsp.user21,"~011")).
            if num-entries(ib-icsp.user21,"~011") > 2 then
              v-drawingmins = dec(entry(3,ib-icsp.user21,"~011")).
            if num-entries(ib-icsp.user21,"~011") > 3 then
              v-electricalmins   = dec(entry(4,ib-icsp.user21,"~011")).
            if num-entries(ib-icsp.user21,"~011") > 4 then
              v-mechanicalmins = dec(entry(5,ib-icsp.user21,"~011")).
              
          END.
/*
          ELSE
            ASSIGN 
              v-labormins = 0
              v-docmins   = 0
              v-drawingmins = 0
              v-electricalmins = 0
              v-mechanicalmins = 0.
*/
        assign t-vaspmap.user10 = ip-prod.  
        if t-vaspmap.typename = "Labor" and v-labormins <> 0 and
           t-vaspmap.overridety = " " then
          assign t-vaspmap.addon[1] = v-labormins.
        if t-vaspmap.typename = "Documentation" and v-docmins <> 0 and
          t-vaspmap.overridety = " " then
          assign t-vaspmap.addon[1] = v-docmins.
        if t-vaspmap.typename = "Drawing Fee" and v-drawingmins <> 0 and
           t-vaspmap.overridety = " " then
          assign t-vaspmap.addon[1] = v-drawingmins.
        if t-vaspmap.typename = "Electrical" and v-electricalmins <> 0 and
           t-vaspmap.overridety = " " then
          assign t-vaspmap.addon[1] = v-electricalmins.
        if t-vaspmap.typename = "Mechanical" and v-mechanicalmins <> 0 and
           t-vaspmap.overridety = " " then
          assign t-vaspmap.addon[1] = v-mechanicalmins.
          
        if t-vaspmap.typename = "Rebate" then do:
          run VaspMap-Rebate (input ip-mode,
                              input ip-lineno,
                              input ip-prod,
                              input ip-whse,
                              output v-rebateamt,
                              input ip-nslistprc).
          assign t-vaspmap.addon[1] = v-rebateamt.
        end.
        
      end.

    end.
    RUN VaspMap-Update (input ip-mode,
                        input zsdivaspmap.seqno,
                        input ip-lineno,
                        input ip-qty).
  end.
END.
END.


/*-------------------------------------------------------------------------*/
PROCEDURE VaspMap-Rebate:
/*-------------------------------------------------------------------------*/
  DEFINE INPUT PARAMETER  ip-mode   AS CHAR                       NO-UNDO.
  DEFINE INPUT PARAMETER  ip-lineno as integer                    no-undo.
  DEFINE INPUT PARAMETER  ip-prod   like icsp.prod                no-undo.
  DEFINE INPUT PARAMETER  ip-whse   like icsw.whse                no-undo.
  DEFINE OUTPUT PARAMETER ip-rebamt as de format ">>>>>>9.9999-"  no-undo.
  DEFINE input  PARAMETER ip-listprice like icsw.listprice        no-undo.




  DEFINE VAR iw-rebamt     as de format ">>>>>>9.9999"       no-undo.
  DEFINE VAR iw-totrebate  as de format ">>>>>>9.9999"       no-undo.
  DEFINE VAR iw-vicrebate  as de format ">>>>>>9.9999"       no-undo.
  DEFINE VAR iw-netrebate  as de format ">>>>>>9.9999"       no-undo.
  DEFINE VAR iw-listprice  as de format ">>>>>>9.9999"       no-undo.
  DEFINE VAR iw-vendno     as de format "999999999999"       no-undo.
  DEFINE VAR iw-arpwhse    as c  format "x(4)"               no-undo.
  DEFINE VAR iw-valid      as logical                        no-undo.
  DEFINE VAR iw-rebtype    like icsw.rebatety                no-undo.
  DEFINE VAR iw-surcharge  like icsw.datccost                no-undo.


  DEFINE BUFFER ib-icsw  for icsw.
  DEFINE BUFFER ib2-icsw for icsw.
  DEFINE BUFFER PUpct    for notes.

  assign iw-vendno = 0.
  find ib-icsw use-index k-icsw where 
       ib-icsw.cono = g-cono   and
       ib-icsw.whse = ip-whse and
       ib-icsw.prod = ip-prod
       no-lock no-error.
  if avail ib-icsw then do:
    if ib-icsw.arpvendno <> 0 then
      assign iw-vendno    = ib-icsw.arpvendno
             iw-listprice = ib-icsw.listprice
             iw-rebtype   = ib-icsw.rebatety
             iw-surcharge = ib-icsw.datccost.
    else do:
      find ib2-icsw use-index k-icsw where 
           ib2-icsw.cono = g-cono    and
           ib2-icsw.whse = ib-icsw.arpwhse and
           ib2-icsw.prod = ip-prod
           no-lock no-error.
      if avail ib2-icsw then
        assign iw-vendno    = ib2-icsw.arpvendno
               iw-listprice = ib2-icsw.listprice
               iw-rebtype   = ib2-icsw.rebatety
               iw-surcharge = ib2-icsw.datccost.
    end.  /* else */
  end. /* avail icsw */
  else
    assign iw-vendno = v-vendno
           iw-listprice = ip-listprice.
  
  
  assign iw-valid  = no.
  run zsdichkpdsc.p(input        g-cono,
                    input        iw-vendno,
                    input        v-custno,
                    input        v-shipto,
                    input        "POWER UNIT",
                    input-output iw-valid).
  
  if iw-valid = yes then 
    do:
    find PUpct where PUpct.cono = g-cono and
                     PUpct.notestype = "ZZ" and
                     PUpct.primarykey = "Eaton PowerUnit %"
                     no-lock no-error.
    if iw-rebtype = "V" then 
      assign ip-rebamt = if avail PUpct then 
                        iw-listprice * (dec(substr(PUpct.noteln[1],5,4)) / 100)
                         else 0.
    else 
    if iw-rebtype = "C" then
      assign ip-rebamt = if avail PUpct then
                        iw-listprice * (dec(substr(PUpct.noteln[2],5,4)) / 100)
                         else 0.
    else 
    if iw-rebtype = "F" then
      assign ip-rebamt = if avail PUpct then
                        iw-listprice * (dec(substr(PUpct.noteln[3],5,4)) / 100)
                         else 0.
    else
    if iw-rebtype = "D" then
      assign ip-rebamt = if avail PUpct then
                        iw-listprice * (dec(substr(PUpct.noteln[4],5,4)) / 100)
                         else 0.
    else

      assign ip-rebamt = 0.
  end.
  else
    assign ip-rebamt = 0.
END.  



/*-------------------------------------------------------------------------*/
PROCEDURE Recursive-update:
/*-------------------------------------------------------------------------*/
  DEFINE input parameter ip-recid as recid no-undo.  

  DEFINE buffer b-t-totals      for t-totals.
  DEFINE buffer ib2-t-totals    for t-totals.
  DEFINE BUFFER ib-vaspsl       for vaspsl.
  DEFINE BUFFER ib2-vaspsl      for vaspsl.
 
  DEFINE BUFFER ib-vasps        for vasps.
  DEFINE BUFFER ib-t-vaspmap    for t-vaspmap.
  DEFINE BUFFER ib-zsdivaspmap  for zsdivaspmap.
  DEFINE BUFFER ipb-zsdivaspmap for zsdivaspmap. 
  DEFINE BUFFER ib-zsdivasp     for zsdivasp. 
     

  find b-t-totals where recid( b-t-totals) = ip-recid no-lock no-error.
  if not avail b-t-totals then
    return.
  
  FIND ib-zsdivasp WHERE                                             
       ib-zsdivasp.cono     = g-cono         AND                     
       ib-zsdivasp.rectype  = "fb"           AND                     
       ib-zsdivasp.repairno = x-quoteno                              
       no-lock  NO-ERROR.                                                       
  
  find ib-zsdivaspmap where
       ib-zsdivaspmap.cono = g-cono            and
       ib-zsdivaspmap.rectype     = "L"        and        /* Template */
       ib-zsdivaspmap.prod        = x-quoteno  and
       ib-zsdivaspmap.seqno       = b-t-totals.seqno and
       ib-zsdivaspmap.lineno      = b-t-totals.lineno  no-error. 
  if avail ib-zsdivaspmap then do:
    assign ib-zsdivaspmap.addon[1] = b-t-totals.addon[1].
  /* Locate Inventory section from Template */                         
    find ipb-zsdivaspmap where                                         
         ipb-zsdivaspmap.cono = g-cono and                             
         ipb-zsdivaspmap.rectype  = "S" and        /* VAES */          
         ipb-zsdivaspmap.prod     = x-quoteno  and                     
         ipb-zsdivaspmap.typename = "INVENTORY" and                    
         ipb-zsdivaspmap.lineno = 0  no-lock  no-error.                
 /* REMINDER INDEX NEEDED */                                            
    if not avail ipb-zsdivaspmap then do:                              
      message "program error no ipb-zsdivaspmap".                      
      pause 10.                                                        
      return.                                                          
    end.                                                               
                                                                                    for each ib-t-vaspmap where                            
             ib-t-vaspmap.cono = g-cono      and           
             ib-t-vaspmap.rectype     = "LQ" and           
             ib-t-vaspmap.prod        = x-quoteno  and     
             ib-t-vaspmap.seqno       = ipb-zsdivaspmap.seqno and               
             ib-t-vaspmap.typename    = b-t-totals.typename:         
   

      if ib-t-vaspmap.overridety <> "y" then do:
                                  /* don't fix overrided value */
                                  /* Don't override the already set manually */         assign ib-t-vaspmap.addon[1] = b-t-totals.addon[1].                               find ib-vaspsl where 
             ib-vaspsl.cono = g-cono                   and    
             ib-vaspsl.shipprod = x-quoteno            and    
             ib-vaspsl.whse     = ib-zsdivasp.whse     and      
             ib-vaspsl.seqno    = ib-t-vaspmap.seqno   and      
             ib-vaspsl.lineno   = ib-t-vaspmap.lineno  and    
             ib-vaspsl.nonstockty <> "l"                      
             no-lock no-error.                                 
        if not avail ib-vaspsl then                                            
          next.  /* blank like in the buffer or lost buisness */                
 
        RUN VaspMap-Update (input "updt",
                            input ib-t-vaspmap.seqno,
                            input ib-t-vaspmap.lineno,
                            input ib-vaspsl.qtyneeded).
 
        RUN ZsdiVaspMap-update (input "c",
                                input recid(ib-vaspsl)).
 
    
/*    Fix up b-totals query for the totals screen if there */          
        find ib-vasps where 
             ib-vasps.cono     = g-cono                   and
             ib-vasps.shipprod = x-quoteno                and
             ib-vasps.whse     = ib-zsdivasp.whse         and
             ib-vasps.seqno    = int(ib-t-vaspmap.user3) 
             no-lock no-error.
       
        if avail ib-vasps then
          find ib2-vaspsl where
               ib2-vaspsl.cono = g-cono                      and
               ib2-vaspsl.shipprod = x-quoteno               and
               ib2-vaspsl.whse     = ib-zsdivasp.whse        and
               ib2-vaspsl.seqno    = int(ib-t-vaspmap.user3) and
               ib2-vaspsl.lineno   = int(ib-t-vaspmap.user4)
               no-lock no-error.
        if avail ib2-vaspsl and avail ib-vasps then do:
          find first ib2-t-totals use-index k2 where
                     ib2-t-totals.seqno    = ib2-vaspsl.seqno         and
                     ib2-t-totals.sctntype = caps(ib-vasps.sctntype)   and
                     ib2-t-totals.lineno   = ib2-vaspsl.lineno        and
                     ib2-t-totals.product  = caps(ib2-vaspsl.compprod) no-error.
      
          if avail ib2-t-totals and ib-vasps.sctntype = "in" then do:
            assign ib2-t-totals.dvalue = 
                 string(ib2-vaspsl.qtyneeded,">>>>>>9.99-") + " " +
                 string(ib2-vaspsl.qtyneeded * 
                        ib2-vaspsl.prodcost,">>>>>>9.99-").
          end.
        end.
      end.
    end.
  end.  
END. /* Procedure   */

/*-------------------------------------------------------------------------*/
PROCEDURE VaspMap-update:
/*-------------------------------------------------------------------------*/
  DEFINE INPUT PARAMETER ip-mode   AS CHAR NO-UNDO.
  DEFINE INPUT PARAMETER ip-seqno  as integer no-undo.
  DEFINE INPUT PARAMETER ip-lineno as integer no-undo.
  DEFINE INPUT PARAMETER ip-qty    like vaspsl.qtyneeded no-undo.

  
 
  DEFINE BUFFER ib-vaspsl     for vaspsl.
  DEFINE BUFFER ib-vasps      for vasps.
  DEFINE BUFFER ib-t-vaspmap  for t-vaspmap.


 
  if ip-mode = "new" or ip-mode = "repl" then do:
    
    for each ib-t-vaspmap where
             ib-t-vaspmap.cono = g-cono            and
             ib-t-vaspmap.rectype     = "LQ"       and        /* VAESl */
             ib-t-vaspmap.prod        = x-quoteno  and
             ib-t-vaspmap.seqno       = ip-seqno   and
             ib-t-vaspmap.lineno      = ip-lineno:

      find ib-vaspsl where ib-vaspsl.cono = g-cono                   and
                           ib-vaspsl.shipprod = x-quoteno            and
                           ib-vaspsl.whse     = x-whse              and
                           ib-vaspsl.seqno    = int(ib-t-vaspmap.user3) and
                           ib-vaspsl.lineno   = int(ib-t-vaspmap.user4)
                           no-lock no-error.
      if ip-mode = "lost" then ip-qty = 0. /* Lost Business */ 
      find ib-vasps where ib-vasps.cono = g-cono                   and
                          ib-vasps.shipprod = x-quoteno            and
                          ib-vasps.whse     = x-whse               and
                          ib-vasps.seqno    = int(ib-t-vaspmap.user3) 
                          no-lock no-error.
      if avail ib-vaspsl and avail ib-vasps then do:
        if ib-vasps.sctntype     = "it" then do:
          assign ib-t-vaspmap.addonamt[1] =  ip-qty *
              (((ib-t-vaspmap.addon[1] * 60) / 3600) * ib-vaspsl.prodcost)
                ib-t-vaspmap.qty = ip-qty
                ib-t-vaspmap.addon[3] = ip-qty. 
        end.
        else if ib-vasps.sctntype     = "in" then do:
          if ib-t-vaspmap.addonty = true then   /* percent */
            assign ib-t-vaspmap.addonamt[1] = ip-qty *
                 ( v-prodcost * ib-t-vaspmap.addon[1])
                   ib-t-vaspmap.qty = ip-qty
                   ib-t-vaspmap.addon[3] = ip-qty. 
          else         
            assign ib-t-vaspmap.addonamt[1] =
                   ib-t-vaspmap.addon[1] * ip-qty
                   ib-t-vaspmap.qty = ip-qty
                   ib-t-vaspmap.addon[3] = ip-qty. 

        end.  
        else if ib-vasps.sctntype     = "ii" then do:
          assign ib-t-vaspmap.addonamt[1] = 
                 ib-t-vaspmap.addon[1] * ip-qty
                 ib-t-vaspmap.qty = ip-qty
                 ib-t-vaspmap.addon[3] = ip-qty. 
        end.
        else do:
        end.
      
      end.
    end.
  end.   /* New Record of replacement(not yet in database) */        
  else
  if ip-mode = "updt" or ip-mode = "lost" then do:
    for each ib-t-vaspmap where
             ib-t-vaspmap.cono = g-cono            and
             ib-t-vaspmap.rectype     = "LQ"       and        /* VAESl */
             ib-t-vaspmap.prod        = x-quoteno  and
             ib-t-vaspmap.seqno       = ip-seqno   and
             ib-t-vaspmap.lineno      = ip-lineno:

      find ib-vaspsl where ib-vaspsl.cono = g-cono                   and
                           ib-vaspsl.shipprod = x-quoteno            and
                           ib-vaspsl.whse     = x-whse               and
                           ib-vaspsl.seqno    = int(ib-t-vaspmap.user3) and
                           ib-vaspsl.lineno   = int(ib-t-vaspmap.user4)
                           no-lock no-error.
       
      find ib-vasps where ib-vasps.cono = g-cono                   and
                          ib-vasps.shipprod = x-quoteno            and
                          ib-vasps.whse      = x-whse               and
                          ib-vasps.seqno    = int(ib-t-vaspmap.user3) 
                          no-lock no-error.
      if ip-mode = "lost" then ip-qty = 0. /* Lost Business */ 

      if avail ib-vaspsl and avail ib-vasps then do:
        if ib-vasps.sctntype     = "it" then do:
          assign ib-t-vaspmap.addonamt[1] =  ip-qty *
                (((ib-t-vaspmap.addon[1] * 60) / 3600) * ib-vaspsl.prodcost)
                ib-t-vaspmap.qty = ip-qty
                ib-t-vaspmap.addon[3] = ip-qty. 
        end.
        else if ib-vasps.sctntype     = "in" then do:
          if ib-t-vaspmap.addonty = true then   /* percent */
            assign ib-t-vaspmap.addonamt[1] = ip-qty *
                 ( v-prodcost * ib-t-vaspmap.addon[1])
                   ib-t-vaspmap.qty = ip-qty
                   ib-t-vaspmap.addon[3] = ip-qty. 
          else         
            assign ib-t-vaspmap.addonamt[1] =
                   ib-t-vaspmap.addon[1] * ip-qty
                   ib-t-vaspmap.qty = ip-qty
                   ib-t-vaspmap.addon[3] = ip-qty. 
   
        end.  
        else if ib-vasps.sctntype     = "ii" then do:
          assign ib-t-vaspmap.addonamt[1] = 
                 ib-t-vaspmap.addon[1] * ip-qty
                 ib-t-vaspmap.qty = ip-qty
                 ib-t-vaspmap.addon[3] = ip-qty. 
         end.
        else do:
        end.
       
      end.
    end.
  end.   /* New Record */        

end. 

/*-------------------------------------------------------------------------*/
PROCEDURE ZsdiVaspMap-update:
/*-------------------------------------------------------------------------*/
  DEFINE INPUT PARAMETER ip-mode   AS CHAR NO-UNDO.
  DEFINE INPUT PARAMETER ip-recid as recid no-undo.

  
 
  DEFINE BUFFER ib-vaspsl for vaspsl.
  DEFINE BUFFER ibr-vaspsl for vaspsl.  /* r buffer to referenced section */
  DEFINE BUFFER ibr-vasps for vasps.
  DEFINE BUFFER ib-zsdivaspmap  for zsdivaspmap.
  DEFINE BUFFER ib-zsdivasp     for zsdivasp.
  DEFINE BUFFER ib-t-vaspmap    for t-vaspmap.


  find ib-vaspsl where recid(ib-vaspsl) = ip-recid no-error.
  
  if not avail ib-vaspsl then do:
    message "ZSDIVASPMAP error VAEXF 3033". pause.
  end.  
 
  if ip-mode = "a" or 
     ip-mode = "c" then do: 
      
    find first bs-zsdivaspmap where
               bs-zsdivaspmap.cono       = g-cono and
               bs-zsdivaspmap.rectype    = "SQ"         and    /* VAES */
               bs-zsdivaspmap.prod       = x-quoteno    and
               bs-zsdivaspmap.description = "ADDON"      and
               bs-zsdivaspmap.seqno       = ib-vaspsl.seqno and
               bs-zsdivaspmap.lineno = 0  no-lock no-error.
/* Possible sync problem with map rebuild from header */    
    if not avail bs-zsdivaspmap then do:
/* Create addon structure for the line item     */
      find first ib-t-vaspmap where
                 ib-t-vaspmap.cono = g-cono            and
                 ib-t-vaspmap.rectype     = "SQ"       and      /* VAES */
                 ib-t-vaspmap.prod        = x-quoteno  and
                 ib-t-vaspmap.seqno       = ib-vaspsl.seqno   and
                 ib-t-vaspmap.lineno = 0  no-lock no-error.
        CREATE bs-zsdivaspmap.
       
        BUFFER-COPY ib-t-vaspmap
         EXCEPT ib-t-vaspmap.qty
         TO bs-zsdivaspmap.
       
    end.
  end.
  
/* Section for the addons SQ created */  
  
  if ip-mode = "a" or 
     ip-mode = "c" then do: 
    FIND FIRST ib-zsdivasp WHERE
               ib-zsdivasp.cono     = g-cono AND
               ib-zsdivasp.rectype  = "fb"   AND
               ib-zsdivasp.repairno = x-quoteno
    EXCLUSIVE-LOCK NO-ERROR.
     
    for each ib-t-vaspmap use-index k-add  where
             ib-t-vaspmap.cono = g-cono            and
             ib-t-vaspmap.rectype     = "LQ"       and        /* VAESl */
             ib-t-vaspmap.prod        = ib-vaspsl.shipprod and
             ib-t-vaspmap.seqno       = ib-vaspsl.seqno   and
             ib-t-vaspmap.lineno      = ib-vaspsl.lineno:
      find ib-zsdivaspmap use-index k-mapty where 
           ib-zsdivaspmap.cono        = g-cono and
           ib-zsdivaspmap.rectype     = "LQ" and
           ib-zsdivaspmap.prod        = ib-t-vaspmap.prod   and
           ib-zsdivaspmap.seqno       = ib-t-vaspmap.seqno   and
           ib-zsdivaspmap.lineno      = ib-t-vaspmap.lineno  and
           ib-zsdivaspmap.typename    = ib-t-vaspmap.typename no-error.
      if not avail ib-zsdivaspmap then do:     
        CREATE ib-zsdivaspmap.
        BUFFER-COPY ib-t-vaspmap
            EXCEPT ib-t-vaspmap.qty   
          TO ib-zsdivaspmap.
        release ib-zsdivaspmap.
        find ib-zsdivaspmap use-index k-mapty where 
             ib-zsdivaspmap.cono        = g-cono and
             ib-zsdivaspmap.rectype     = "LQ" and
             ib-zsdivaspmap.prod        = ib-t-vaspmap.prod   and
             ib-zsdivaspmap.seqno       = ib-t-vaspmap.seqno   and
             ib-zsdivaspmap.lineno      = ib-t-vaspmap.lineno  and
             ib-zsdivaspmap.typename    = ib-t-vaspmap.typename no-error.
      end.
      /* finding the addon section and line */
      
      find ibr-vaspsl where ibr-vaspsl.cono = g-cono                   and
                            ibr-vaspsl.shipprod = x-quoteno            and
                            ibr-vaspsl.whse     = ib-zsdivasp.whse     and
                            ibr-vaspsl.seqno    = int(ib-t-vaspmap.user3) and
                            ibr-vaspsl.lineno   = int(ib-t-vaspmap.user4)
                            no-error.
       
      find ibr-vasps where ibr-vasps.cono = g-cono                   and
                           ibr-vasps.shipprod = x-quoteno            and
                           ibr-vasps.whse     = ib-zsdivasp.whse     and
                           ibr-vasps.seqno    = int(ib-t-vaspmap.user3) 
                           no-lock no-error.
      if avail ibr-vaspsl and avail ibr-vasps then do:
        if ibr-vasps.sctntype     = "it" then do:
          assign /* Clear old VASPSL VALUE From TOTALS */
                 ib-zsdivasp.repairtotl = ib-zsdivasp.repairtotl -
                   ((ibr-vaspsl.timeelapsed / 3600) * ibr-vaspsl.prodcost)
                 ib-zsdivasp.Labortotl = ib-zsdivasp.Labortotl -
                   ((ibr-vaspsl.timeelapsed / 3600) * ibr-vaspsl.prodcost)
          
                 ib-zsdivaspmap.addon[1]    = ib-t-vaspmap.addon[1]
                 ib-zsdivaspmap.addonamt[1] =  ib-t-vaspmap.qty *
                  (((ib-t-vaspmap.addon[1] * 60) / 3600) 
                     * ibr-vaspsl.prodcost)
                 ib-t-vaspmap.addonamt[1] =  ib-t-vaspmap.qty *
                  (((ib-t-vaspmap.addon[1] * 60) / 3600) 
                     * ibr-vaspsl.prodcost)
                 ibr-vaspsl.timeelapsed = ibr-vaspsl.timeelapsed +
                   (((ib-t-vaspmap.addon[1] * 60) ) * ib-t-vaspmap.addon[3]) -
                   (((ib-t-vaspmap.addon[2] * 60) ) * ib-t-vaspmap.addon[4])

                /* Add new VASPSL VALUE From TOTALS */

                 ib-zsdivasp.repairtotl = ib-zsdivasp.repairtotl +
                   ((ibr-vaspsl.timeelapsed / 3600) * ibr-vaspsl.prodcost)
                 ib-zsdivasp.Labortotl = ib-zsdivasp.Labortotl +
                   ((ibr-vaspsl.timeelapsed / 3600) * ibr-vaspsl.prodcost)

                 ib-t-vaspmap.addon[2] = ib-t-vaspmap.addon[1]
                 ib-t-vaspmap.addon[4] = ib-t-vaspmap.addon[3]  /* Qty */
                 ib-t-vaspmap.addonamt[2] = ib-t-vaspmap.addonamt[1].
                        
        end.
        else if ibr-vasps.sctntype     = "in" then do:
           if ib-t-vaspmap.addonty = true then   /* percent */
             assign /* Clear old VASPSL VALUE to TOTALS */
 
                    ib-zsdivasp.repairtotl = ib-zsdivasp.repairtotl -
                      (ibr-vaspsl.qtyneeded * ibr-vaspsl.prodcost)
                    ib-zsdivasp.Intangtotl = ib-zsdivasp.Intangtotl -
                      (ibr-vaspsl.qtyneeded * ibr-vaspsl.prodcost)
             
                    ib-zsdivaspmap.addonty = ib-t-vaspmap.addonty
                    ib-zsdivaspmap.addon[1]    = ib-t-vaspmap.addon[1]
                    ib-zsdivaspmap.addonamt[1] = ib-t-vaspmap.qty *
                      (ib-vaspsl.prodcost * ib-t-vaspmap.addon[1])
                    ib-t-vaspmap.addonamt[1] = ib-t-vaspmap.qty *
                      (ib-vaspsl.prodcost * ib-t-vaspmap.addon[1])
                    ibr-vaspsl.qtyneeded = ibr-vaspsl.qtyneeded +
                      (ib-t-vaspmap.addonamt[1] - ib-t-vaspmap.addonamt[2])

                /* Add New VASPSL VALUE to TOTALS */

                    ib-zsdivasp.repairtotl = ib-zsdivasp.repairtotl +
                      (ibr-vaspsl.qtyneeded * ibr-vaspsl.prodcost)
                    ib-zsdivasp.Intangtotl = ib-zsdivasp.Intangtotl +
                      (ibr-vaspsl.qtyneeded * ibr-vaspsl.prodcost)

                    ib-t-vaspmap.addon[2] = ib-t-vaspmap.addon[1]
                    ib-t-vaspmap.addon[4] = ib-t-vaspmap.addon[3] /* qty */
                    ib-t-vaspmap.addonamt[2] = ib-t-vaspmap.addonamt[1].                    else    
             assign 
                    
                /* Clear Old VASPSL VALUE From TOTALS */

                    ib-zsdivasp.repairtotl = ib-zsdivasp.repairtotl -
                      (ibr-vaspsl.qtyneeded * ibr-vaspsl.prodcost)
                    ib-zsdivasp.Intangtotl = ib-zsdivasp.Intangtotl -
                      (ibr-vaspsl.qtyneeded * ibr-vaspsl.prodcost)

             
                    ib-zsdivaspmap.addonty = ib-t-vaspmap.addonty
                    ib-zsdivaspmap.addon[1]    = ib-t-vaspmap.addon[1]
                    ib-zsdivaspmap.addonamt[1] = 
                      round(ib-t-vaspmap.qty * ib-t-vaspmap.addon[1],2)
                    ib-t-vaspmap.addonamt[1] =  
                      round(ib-t-vaspmap.qty * ib-t-vaspmap.addon[1],2)
                    ibr-vaspsl.qtyneeded = ibr-vaspsl.qtyneeded +
                      (ib-t-vaspmap.addonamt[1] - ib-t-vaspmap.addonamt[2])


                /* Add New VASPSL VALUE to TOTALS */

                    ib-zsdivasp.repairtotl = ib-zsdivasp.repairtotl +
                      (ibr-vaspsl.qtyneeded * ibr-vaspsl.prodcost)
                    ib-zsdivasp.Intangtotl = ib-zsdivasp.Intangtotl +
                      (ibr-vaspsl.qtyneeded * ibr-vaspsl.prodcost)

                    ib-t-vaspmap.addon[2] = ib-t-vaspmap.addon[1]
                    ib-t-vaspmap.addon[4] = ib-t-vaspmap.addon[3] /* Qty */
                    ib-t-vaspmap.addonamt[2] = ib-t-vaspmap.addonamt[1].                  
        end.  
        else if ibr-vasps.sctntype     = "ii" then do:
          assign   /* Clear Old VASPSL VALUE Fromo TOTALS */                                  ib-zsdivasp.repairtotl = ib-zsdivasp.repairtotl -
                   (ibr-vaspsl.qtyneeded * ibr-vaspsl.prodcost)
                 ib-zsdivasp.user8 = ib-zsdivasp.user8 -
                   (ibr-vaspsl.qtyneeded * ibr-vaspsl.prodcost)
          
                 ib-zsdivaspmap.addonty = ib-t-vaspmap.addonty
                 ib-zsdivaspmap.addon[1]    = ib-t-vaspmap.addon[1]
                 ib-zsdivaspmap.addonamt[1] = 
                   round(ib-t-vaspmap.qty * ib-t-vaspmap.addon[1],2)
                 ib-t-vaspmap.addonamt[1] = 
                   round(ib-t-vaspmap.qty * ib-t-vaspmap.addon[1],2)
                 ibr-vaspsl.qtyneeded = ibr-vaspsl.qtyneeded +
                    (ib-t-vaspmap.addonamt[1] - ib-t-vaspmap.addonamt[2])

               /* Add New VASPSL VALUE To TOTALS */                            
                 ib-zsdivasp.repairtotl = ib-zsdivasp.repairtotl +
                   (ibr-vaspsl.qtyneeded * ibr-vaspsl.prodcost)
                 ib-zsdivasp.user8 = ib-zsdivasp.user8 +
                   (ibr-vaspsl.qtyneeded * ibr-vaspsl.prodcost)

                 ib-t-vaspmap.addon[2] = ib-t-vaspmap.addon[1]
                 ib-t-vaspmap.addon[4] = ib-t-vaspmap.addon[3]  /* Qty */
                 ib-t-vaspmap.addonamt[2] = ib-t-vaspmap.addonamt[1].               
        end.
        else do:
        end.
       
      end.
    end.          /* For each t-vaspmap for "LQ" */
  end.           /* Add or change */

end. 

/*-------------------------------------------------------------------------*/
PROCEDURE create-cancel:
/*-------------------------------------------------------------------------*/
  DO:
    FOR EACH ztmk USE-INDEX k-ztmk-sc2 WHERE
             ztmk.cono      = g-cono    AND
             ztmk.ordertype = "fb"      AND
             ztmk.user1     = x-quoteno AND
             ztmk.statuscd  = 0
      EXCLUSIVE-LOCK:
      ASSIGN 
        ztmk.operinit   = g-operinits
        ztmk.punchoutdt = TODAY
        ztmk.statuscd   = 5
        ztmk.punchouttm = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                          SUBSTRING(STRING(TIME,"hh:mm"),4,2) +
                          SUBSTRING(STRING(TIME,"hh:mm:ss"),7,2).
    END.
    CREATE ztmk.
    ASSIGN 
      ztmk.jobseqno  = 0
      ztmk.cono      = g-cono
      ztmk.ordertype = "fb"
      ztmk.user1     = x-quoteno
      ztmk.stagecd   = v-cancelled
      ztmk.statuscd  = 0
      ztmk.techid    = g-operinits
      ztmk.punchindt = TODAY
      ztmk.punchintm = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                       SUBSTRING(STRING(TIME,"hh:mm"),4,2) +
                       SUBSTRING(STRING(TIME,"hh:mm:ss"),7,2)
      ztmk.operinit  = g-operinit
      ztmk.transtm   = IF ztmk.transtm = "" THEN
                         SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                         SUBSTRING(STRING(TIME,"hh:mm"),4,2)
                       ELSE
                         ztmk.transtm
      ztmk.transdt   = IF ztmk.transdt = ? THEN
                         TODAY
                       ELSE
                         ztmk.transdt.
    ASSIGN 
      ztmk.operinit   = g-operinits
      ztmk.punchoutdt = TODAY
      ztmk.statuscd   = 5
      ztmk.punchouttm = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                        SUBSTRING(STRING(TIME,"hh:mm"),4,2) +
                        SUBSTRING(STRING(TIME,"hh:mm:ss"),7,2).
  END.
END.

/*-------------------------------------------------------------------------*/
PROCEDURE cust-comments:
/*-------------------------------------------------------------------------*/
  DO:
    PUT SCREEN ROW 21 COL 1
    "                                                                                 ".
    ASSIGN v-idx2 = 1.
    /*
    assign v-comments[v-idx2] = "--------------- Internal Notes --------------"
    v-idx2 = v-idx2 + 1.
    for each notes
    where notes.cono         = g-cono     and
    notes.notestype    = "fa"       and
    notes.primarykey   = x-quoteno  and
    notes.secondarykey = x-whse     and
    notes.requirefl    = yes        and
    notes.pageno       = 2
    no-lock:
    if avail notes then
    do:
    do v-idx = 1 to 16:
    if notes.noteln[v-idx] ne "" then
    do:
    assign v-comments[v-idx2] = notes.noteln[v-idx].
    v-idx2 = v-idx2 + 1.
    end.
    end.
    end.
    end.
    assign v-comments[v-idx2] = "--------------- Customer Notes --------------"
    v-idx2 = v-idx2 + 1.
    for each notes
    where notes.cono         = g-cono     and
    notes.notestype    = "fa"       and
    notes.primarykey   = x-quoteno  and
    notes.secondarykey = x-whse     and
    notes.requirefl    = no         and
    notes.pageno       = 1
    no-lock:
    if avail notes then
    do:
    do v-idx = 1 to 16:
    if notes.noteln[v-idx] ne "" then
    do:
    assign v-comments[v-idx2] = notes.noteln[v-idx]
    v-idx2 = v-idx2 + 1.
    end.
    end.
    end.
    end.
    */
    
    ASSIGN v-idx2 = 1.
    FOR EACH notes
      WHERE notes.cono         = g-cono     AND
            notes.notestype    = "in"       AND
            notes.primarykey   = x-quoteno  AND
            notes.secondarykey = x-whse
      NO-LOCK:
      IF avail notes THEN DO:
        DO v-idx = 1 TO 16:
          IF notes.noteln[v-idx] NE "" THEN DO:
            ASSIGN v-comments4[v-idx2] = notes.noteln[v-idx]
            v-idx2 = v-idx2 + 1.
          END.
        END.
      END.
    END.
    DISPLAY v-comments4 WITH FRAME f-reqcom.
    MESSAGE "".
    STATUS DEFAULT "Press any key to continue.".
    UNIX SILENT VALUE("sh /rd/cust/local/UnixPause.sh").
    MESSAGE "".
    STATUS DEFAULT "".
    ASSIGN v-comments4 = "".
  END.
END.

/*
/*-----------------------------------------------------------------------*/
PROCEDURE MakeQuotePrc:
/*-----------------------------------------------------------------------*/
  DEF BUFFER p-t-lines FOR t-lines.
  DEF BUFFER p-vasp FOR vasp.
  DEF BUFFER p-zsdivasp FOR zsdivasp.

  FIND FIRST p-vasp WHERE
             p-vasp.cono     = g-cono AND
             p-vasp.shipprod = x-quoteno
  EXCLUSIVE-LOCK NO-ERROR.
  FIND FIRST p-zsdivasp WHERE
             p-zsdivasp.cono     = g-cono AND
             p-zsdivasp.rectype  = "fb"   AND
             p-zsdivasp.repairno = x-quoteno
  EXCLUSIVE-LOCK NO-ERROR.
   
  
  
  
  IF avail p-vasp THEN
    ASSIGN p-pct = p-zsdivasp.margpct.
  FIND FIRST vaspsl WHERE
             vaspsl.cono = g-cono         AND
             vaspsl.shipprod = x-quoteno  AND
             vaspsl.whse     = p-zsdivasp.whse and
             vaspsl.compprod = "teardown" AND
             vaspsl.seqno    = 2
  NO-LOCK NO-ERROR.
  IF avail vaspsl THEN
  DO:
    ASSIGN 
      t-cstfab-h = (v-estfab-h * vaspsl.prodcost)
      t-cstfab-m = ((v-estfab-m * vaspsl.prodcost) / 60).
  END.
  FIND FIRST vaspsl WHERE
             vaspsl.cono = g-cono               AND
             vaspsl.shipprod = x-quoteno        AND
             vaspsl.whse     = p-zsdivasp.whse  and
             vaspsl.compprod = "service repair" AND
             vaspsl.seqno    = 6
  NO-LOCK NO-ERROR.
  IF avail vaspsl THEN
  DO:
    ASSIGN 
      t-cstdoc-h = (v-estdoc-h * vaspsl.prodcost)
      t-cstdoc-m = ((v-estdoc-m * vaspsl.prodcost) / 60).
  END.
  FIND FIRST vaspsl WHERE
             vaspsl.cono = g-cono          AND
             vaspsl.shipprod = x-quoteno   AND
             vaspsl.compprod = "test time" AND
             vaspsl.seqno    = 6
  NO-LOCK NO-ERROR.
  IF avail vaspsl THEN
  DO:
    ASSIGN 
      t-cstcad-h = (v-estcad-h * vaspsl.prodcost)
      t-cstcad-m = ((v-estcad-m * vaspsl.prodcost) / 60).
  END.
  ASSIGN 
    s-pndintrndsp  = t-cstfab-h + t-cstfab-m +
    t-cstdoc-h + t-cstdoc-m +
    t-cstcad-h + t-cstcad-m
    s-FabTotalCost     = s-pndintrndsp
    s-trendtot     = s-FabTotalCost * 1.04.
  
  /** add in the seal kit and tag cost if available **/
  DEFINE BUFFER s-vaspsl FOR vaspsl.
  FOR EACH s-vaspsl WHERE
           s-vaspsl.cono = g-cono          AND
           s-vaspsl.shipprod = x-quoteno   AND
           s-vaspsl.seqno = 5 NO-LOCK:
    ASSIGN s-trendtot = s-trendtot +
      (((s-vaspsl.prodcost + s-vaspsl.xxde1) * s-vaspsl.qtyneeded) * 1.04).
  END.
  ASSIGN 
    s-FabTotalPrice    = 
       if round(((s-trendtot / (100 - p-pct)) * 100),2) -
          ((s-trendtot / (100 - p-pct) * 100)) <> 0 then
         round(((s-trendtot / (100 - p-pct)) * 100),2) 
         + .01 
       else
        round(((s-trendtot / (100 - p-pct)) * 100),2)
    s-FabTotalPrice    = IF s-FabTotalPrice > 9999999 THEN
                       9999999
                     ELSE
                       s-FabTotalPrice.
  IF avail p-vasp THEN
    ASSIGN SUBSTRING(p-vasp.user1,180,11) = IF s-FabTotalPrice > 0 THEN
                                              STRING(s-FabTotalPrice,"9999999.99-")
                                            ELSE
                                              "".
END.
*/

/*-----------------------------------------------------------------------*/
PROCEDURE MakeQuoteNo:
/*-----------------------------------------------------------------------*/
  DEFINE INPUT  PARAMETER ip-quoteno   AS CHARACTER NO-UNDO.
  DEFINE OUTPUT PARAMETER ip-DBquoteno AS CHARACTER NO-UNDO.
  DEFINE VAR z AS CHAR FORMAT "x(07)"               NO-UNDO.
  DEFINE VAR Y AS INT                               NO-UNDO.
  DEFINE VAR xy AS INT                              NO-UNDO.
  
  ASSIGN v-errfl = NO.
  IF ip-quoteno = "" THEN DO:
    ASSIGN ip-dbquoteno = "".
    RETURN.
  END.
  
  IF NUM-ENTRIES(ip-quoteno,"-") <> 2 THEN DO:
    ASSIGN 
      ip-DBquoteno = ip-quoteno
      v-errfl = YES
      v-error =
        "Invalid FabNo. --> " + ip-quoteno +
        " - Please enter format 'YYMM-(number)  '".
    RETURN.
  END.
  
  ASSIGN z = TRIM(ENTRY(2,ip-quoteno,"-"),"0123456789").
  ASSIGN Y = LENGTH(z).
  
  IF Y NE 0 THEN DO:
    ASSIGN 
      ip-DBquoteno = ip-quoteno
      v-errfl = YES
      v-error =
        "Invalid FabNo. --> " + ip-quoteno +
        " - Please enter format 'YYMM-(number)  '".
    RETURN.
  END.
  
  ASSIGN ip-DBquoteno  = ENTRY (1,ip-quoteno,"-") + "-" +
                         STRING(INT(ENTRY(2,ip-quoteno,"-")),"9999999").
END.

PROCEDURE header-chg:
  DO:
    /** using repeat to try to end exclusive-lock transaction **/
    headerloop:
    REPEAT:
      IF g-fabquoteno NE o-vaquoteno THEN DO:
        RUN makequoteno(o-vaquoteno,OUTPUT x-quoteno).
        RUN vaexfinuse.p(INPUT x-quoteno,
                         INPUT g-operinits,
                         INPUT "c",
                         INPUT-OUTPUT v-inuse).
      END.
      RUN makequoteno(g-fabquoteno,OUTPUT x-quoteno).
      FIND FIRST vasp WHERE
                 vasp.cono     = g-cono AND
                 vasp.shipprod = x-quoteno
      NO-LOCK NO-ERROR.
/*
      IF avail vasp AND
        (vasp.xxc4 NE "" AND
        vasp.xxc4 = STRING(g-operinits,"x(4)") + " in --> " + g-currproc) THEN
        LEAVE.
*/      
      IF v-errfl AND g-fabquoteno NE "" THEN DO:
        ASSIGN v-error =
          "Invalid FabNo. --> " + g-fabquoteno +
          " - Please enter format 'YYMM-(number)  '".
        RUN zsdi_vaerrx.p(v-error,"yes").
        LEAVE.
      END.
      /* do not display f6-top info for already converted Fab's */
      IF  avail vasp and DEC(SUBSTRING(vasp.user2,1,7)) > 0 THEN DO:
        ASSIGN v-errfl = YES.
      END.
      IF v-errfl OR g-fabquoteno = "" THEN DO:
        RUN makequoteno(o-vaquoteno,OUTPUT x-quoteno).
        ASSIGN 
          zsdiupdated = NO
          v-found    = NO
          v-custno   = 0
          v-takenby  = /* IF v-takenby = "" THEN */
                         g-operinits
                      /*  ELSE
                          v-takenby */
          v-custnoa  = ""
          v-vano     = ""
          s-prod     = ""
          v-astr-top = ""
          v-model    = ""
          v-stage    = ""
          o-custno   = 0
          v-custpo   = ""
          v-shipto   = ""
          v-custname = "".
        RUN DisplayTop1.
      END.
      ELSE DO:
        FIND FIRST vasp USE-INDEX k-vasp WHERE
                   vasp.cono     = g-cono    AND
                   vasp.shipprod = x-quoteno 
        NO-LOCK NO-ERROR.

        find zsdivasp where 
             zsdivasp.cono = g-cono and
             zsdivasp.rectype = "fb" and
             zsdivasp.repairno = vasp.shipprod 
        no-lock no-error.

        /** if user has update security log them into fab here **/
        IF avail vasp AND g-secure > 2 THEN DO:
          RUN vaexfinuse.p(INPUT x-quoteno,
                           INPUT g-operinits,
                           INPUT "x",
                           INPUT-OUTPUT v-inuse).
          
          IF (v-inuse AND g-operinits <> SUBSTRING(vasp.xxc4,1,4)) THEN DO:
            ASSIGN 
              v-errfl = YES
              v-error = "FabQu is in-use by operator: " +
                        STRING(vasp.xxc4,"x(20)").
            PUT SCREEN ROW 23 COL 1 "FabQu is in-use by operator: " +
              STRING(vasp.xxc4,"x(20)").
            LEAVE.
          END.
          ELSE
          IF v-inuse AND g-operinits = SUBSTRING(vasp.xxc4,1,4) THEN
            ASSIGN g-operinits = g-operinits.
          ELSE
          IF avail vasp AND g-secure > 2 THEN DO:
            RUN vaexfinuse.p(INPUT x-quoteno,
                             INPUT g-operinits,
                             INPUT "a",
                             INPUT-OUTPUT v-inuse).
            FIND FIRST vasp USE-INDEX k-vasp WHERE
                       vasp.cono     = g-cono    AND
                       vasp.shipprod = x-quoteno 
            NO-LOCK NO-ERROR.
          END.
        END. /* if avail vasp and g-secure > 2 */
        IF avail vasp THEN DO:
          FIND zsdivasp  WHERE
               zsdivasp.cono     = g-cono    AND
               zsdivasp.rectype  = "fb"      and
               zsdivasp.repairno = x-quoteno 
          no-lock no-error.
        END.     
           
        IF avail vasp and avail zsdivasp THEN DO:
          if vasp.user7 = 0 then
            find first t-stages no-lock no-error.
          else do:
            find first t-stages where 
                       t-stages.stage = int(vasp.user7)
            no-lock no-error.
            if not avail t-stages then
              find first t-stages where 
                         t-stages.stage >= int(vasp.user7)
              no-lock no-error.
          end.    
          ASSIGN 
            o-vaquoteno = g-fabquoteno
            h-vaquoteno = g-fabquoteno
            s-maxlead   = int(zsdivasp.leadtm1)
            p-pct       = zsdivasp.margpct
            v-lmode     = "c"
            v-vano      = vasp.user2
            v-revno     = zsdivasp.xuser1
            v-stage     = t-stages.shortname
            v-tagdt     = zsdivasp.enterdt
            v-takenby   = if zsdivasp.takenby <> "" then
                            zsdivasp.takenby
                          else
                            g-operinits
            v-custno    = zsdivasp.custno
            o-custno    = zsdivasp.custno
            v-custnoa   = STRING(zsdivasp.custno)
            v-vano      = vasp.user2
            v-found     = YES
            s-prod      = zsdivasp.partno
            o-partno    = zsdivasp.partno
            v-model     = zsdivasp.mfgname 
            v-serial    = " "
            v-custpo    = zsdivasp.custpo
            v-shipto    = zsdivasp.shipto.
          ASSIGN v-astr-top = "".
          ASSIGN v-astr-top = v-astr3.
          DISPLAY v-astr-top WITH FRAME f-top1.
          ASSIGN v-comments = " ".
          FIND FIRST notes
          WHERE notes.cono         = g-cono     AND
                notes.notestype    = "in"       AND
                notes.primarykey   = x-quoteno  AND
                notes.secondarykey = x-whse
          NO-LOCK NO-ERROR.
          IF avail notes THEN DO:
            DO v-idx = 1 TO 16:
              ASSIGN v-comments[v-idx] = notes.noteln[v-idx].
            END.
          END.
          {w-arsc.i v-custno NO-LOCK}
          IF avail arsc THEN
            ASSIGN v-custname = arsc.NAME.
          RUN DisplayTop1.
        END. /* avail vasp */
      END. /* else do */
      LEAVE headerloop.
    END. /* repeat */
  END.
END. /* end procedure */

/* -------------------------------------------------------------------------  */
PROCEDURE Makenew:
/* -------------------------------------------------------------------------  */
  DEFINE INPUT PARAMETER n-type AS CHAR FORMAT "x" NO-UNDO.
  
  DEF VAR mystring AS CHAR NO-UNDO.
  DEF VAR myidx    AS INT NO-UNDO.
  DEF VAR prod-hit AS l NO-UNDO.
  
  IF n-type = "m" THEN DO:
  END.
  ELSE
  IF n-type = "p" THEN DO:
    ASSIGN prod-hit = NO.
    RUN vaexf-refer.p (INPUT "Fabrication Template",
                       INPUT v-model,
                       INPUT " ",
                       INPUT " ",
                       INPUT " ",
                       INPUT " ",
                       INPUT " ",
                       INPUT "CopyFolders",
                       INPUT "VA",
                       INPUT-OUTPUT s-recid,
                       INPUT-OUTPUT ix-keyno).
  END.
END.


/* -------------------------------------------------------------------------  */
PROCEDURE NoteDisplay:
/* -------------------------------------------------------------------------  */
  DO:
    DEF VAR v-hit AS LOGICAL NO-UNDO.
    DEF VAR v-idx AS INT     NO-UNDO.
    
    ASSIGN  
      v-astr2 = ""
      v-hit   = NO.
    DISPLAY v-astr2 WITH FRAME f-mid1.
    FIND FIRST notes USE-INDEX k-notes WHERE
               notes.cono         = g-cono  AND
               notes.notestype    = "p"     AND
               notes.primarykey   = v-prod  AND
               notes.secondarykey = ""
    NO-LOCK NO-ERROR.
    IF avail notes THEN DO:
      DO v-idx = 1 TO 16:
        IF notes.noteln[v-idx] NE " " THEN DO:
          ASSIGN v-hit = YES.
          LEAVE.
        END.
      END.
    END.
    IF v-hit AND avail notes AND notes.requirefl = YES THEN DO:
      ASSIGN v-astr2 = "!".
      DISPLAY v-astr2 WITH FRAME f-mid1.
      RUN noted.p(TRUE,"p",v-prod,"").
    END.
    ELSE
    IF v-hit AND avail notes AND notes.requirefl = NO THEN DO:
      ASSIGN v-astr2 = "*".
      DISPLAY v-astr2 WITH FRAME f-mid1.
    END.
    ON cursor-up cursor-up.
    ON cursor-down cursor-down.
  END.
END.

/* -------------------------------------------------------------------------  */
PROCEDURE NoteDisplay2:
/* -------------------------------------------------------------------------  */
  DO:
    DEF VAR v-hit AS LOGICAL NO-UNDO.
    DEF VAR v-idx AS INT     NO-UNDO.
    
    ASSIGN 
      v-astr-top = ""
      v-hit      = NO.
    DISPLAY v-astr-top WITH FRAME f-top1.
    FIND FIRST notes USE-INDEX k-notes WHERE
               notes.cono         = g-cono  AND
               notes.notestype    = "p"     AND
               notes.primarykey   = INPUT s-prod AND
               notes.secondarykey = ""
    NO-LOCK NO-ERROR.
    IF avail notes THEN DO:
      DO v-idx = 1 TO 16:
        IF notes.noteln[v-idx] NE " " THEN DO:
          ASSIGN v-hit = YES.
          LEAVE.
        END.
      END.
    END.
    IF v-hit AND avail notes AND notes.requirefl = YES THEN DO:
      ASSIGN v-astr-top = "!".
      DISPLAY v-astr-top WITH FRAME f-top1.
      RUN noted.p(TRUE,"p",s-prod,"").
    END.
    ELSE
    IF v-hit AND avail notes AND notes.requirefl = NO THEN DO:
      ASSIGN v-astr-top = "*".
      DISPLAY v-astr-top WITH FRAME f-top1.
      IF KEYLABEL(LASTKEY) = "F20" THEN
      RUN noted.p(TRUE,"p",s-prod,"").
    END.
  END.
END.

/* -------------------------------------------------------------------------  */
PROCEDURE NoteDisplay3:
/* -------------------------------------------------------------------------  */
  DO:
    DEF INPUT PARAMETER x-prod AS CHAR FORMAT "x(24)"  NO-UNDO.
    
    DEF VAR v-hit AS LOGICAL NO-UNDO.
    DEF VAR v-idx AS INT     NO-UNDO.
    
    ASSIGN  
      v-astr3 = " "
      v-hit   = NO.
    FIND FIRST notes USE-INDEX k-notes WHERE
               notes.cono         = g-cono  AND
               notes.notestype    = "p"     AND
               notes.primarykey   = x-prod  AND
               notes.secondarykey = ""
    NO-LOCK NO-ERROR.
    IF avail notes THEN DO:
      DO v-idx = 1 TO 16:
        IF notes.noteln[v-idx] NE " " THEN DO:
          ASSIGN v-hit = YES.
          LEAVE.
        END.
      END.
    END.
    IF v-hit AND avail notes AND notes.requirefl = YES THEN DO:
      ASSIGN v-astr3 = "!".
    END.
    ELSE
    IF v-hit AND avail notes AND notes.requirefl = NO THEN DO:
      ASSIGN v-astr3 = "*".
    END.
    
  END.
END.

/* -------------------------------------------------------------------------  */
PROCEDURE ItemDelete:
/* -------------------------------------------------------------------------  */
  DEFINE INPUT PARAMETER id-recid AS RECID             no-undo.
  DEFINE INPUT PARAMETER ip-lineno like t-lines.lineno no-undo.
  DEFINE INPUT PARAMETER ip-qty like vaspsl.qtyneeded  no-undo.
  DEFINE INPUT PARAMETER ip-prod like t-lines.prod     no-undo.
  DEFINE INPUT PARAMETER ip-type as char               no-undo.
/* Ip-type is used for a delete 'd' 
                   and a change of product 'c'
*/                   

  define buffer ib-t-vaspmap   for t-vaspmap.
  define buffer ib-zsdivaspmap for zsdivaspmap.
  define buffer ib-vaspsl      for vaspsl.
  define buffer ib-zsdivasp    for zsdivasp.

  
  itemdel:
  DO TRANSACTION:
    FIND vaspsl WHERE RECID(vaspsl) = id-recid
    EXCLUSIVE-LOCK NO-ERROR.

    IF avail vaspsl THEN DO:
/*
      IF vaspsl.compprod BEGINS "core-" THEN DO:
        DEF BUFFER ii-vaspsl FOR vaspsl.
        DEF BUFFER ii-vasps FOR vasps.
        FIND FIRST ii-vaspsl WHERE
                   ii-vaspsl.cono     = g-cono    AND
                   ii-vaspsl.shipprod = x-quoteno AND
                   ii-vaspsl.whse     = STRING(x-whse,"x(4)") AND
                   ii-vaspsl.seqno    = 4 AND
                   ii-vaspsl.lineno   = 1
        EXCLUSIVE-LOCK NO-ERROR.
        IF avail ii-vaspsl THEN
          DELETE ii-vaspsl.
        FIND FIRST ii-vasps WHERE
                   ii-vasps.cono     = g-cono    AND
                   ii-vasps.shipprod = x-quoteno AND
                   ii-vasps.whse     = STRING(x-whse,"x(4)") AND
                   ii-vasps.seqno    = 4         AND
                   ii-vasps.sctntype = "ii"
        EXCLUSIVE-LOCK.
        IF avail ii-vasps THEN
          DELETE ii-vasps.
      END.

      ASSIGN 
        SUBSTRING(vaspsl.user4,1,4) = "lost"
        vaspsl.nonstockty = "l"
        vaspsl.user6      = vaspsl.prodcost
        vaspsl.prodcost   = 0
        vaspsl.xxde1      = 0
        vaspsl.xxde2      = 0.
*/   

        run zsdiCatalogTrace.p
           (input "Delete",  /* Audit, Inquire, Delete */
            input g-cono,
            input  vaspsl.compprod, 
            input vaspsl.whse,
            input "VAEXF",
            input (if vaspsl.nonstockty = "n" then "n" else "s"),
            input int(entry(2,x-quoteno,"-")),
            input int(entry(1,x-quoteno,"-")),
            input vaspsl.seqno,
            input vaspsl.lineno,
            input g-operinits,
            output v-zsdireturncd).


      RUN VaspMap-Update (input "lost",
                          input vaspsl.seqno,
                          input ip-lineno,
                          input ip-qty).
 
      RUN ZSDIVaspMap-update (input "c",
                              input recid(vaspsl)).
      if ip-type = "d" then do:
       
            
        find first rarr-com use-index k-com where 
                   rarr-com.cono     = g-cono      and
                   rarr-com.comtype  = vaspsl.shipprod and  /* Quote No */
                   rarr-com.orderno  = 0           and
                   rarr-com.ordersuf = 0           and
                   rarr-com.lineno   = vaspsl.lineno 
                   no-error.
        if avail rarr-com then do:
          delete rarr-com.
        end.
        
        for each ib-t-vaspmap where
                 ib-t-vaspmap.cono = g-cono            and
                 ib-t-vaspmap.rectype     = "LQ"       and        /* VAESl */
                 ib-t-vaspmap.prod        = x-quoteno  and
                 ib-t-vaspmap.seqno       = vaspsl.seqno   and
                 ib-t-vaspmap.lineno      = vaspsl.lineno:                                     delete ib-t-vaspmap.
        end.    
    
        for each ib-zsdivaspmap where
                 ib-zsdivaspmap.cono = g-cono            and
                 ib-zsdivaspmap.rectype     = "LQ"       and        /* VAESl */
                 ib-zsdivaspmap.prod        = x-quoteno  and
                 ib-zsdivaspmap.seqno       = vaspsl.seqno   and
                 ib-zsdivaspmap.lineno      = vaspsl.lineno:
            delete ib-zsdivaspmap.
        end.    

        FIND FIRST ib-zsdivasp WHERE
                   ib-zsdivasp.cono = g-cono         AND
                   ib-zsdivasp.rectype = "fb"        AND
                   ib-zsdivasp.repairno = x-quoteno  
        NO-ERROR.
                                                                              
        find ib-vaspsl where recid(ib-vaspsl) = recid(vaspsl).    
        assign ib-zsdivasp.invcost = ib-zsdivasp.invcost - 
               (ib-vaspsl.prodcost * ib-vaspsl.qtyneeded).
   
        delete ib-vaspsl.
      end. /* Delete type */
    
    
    
    END.
  END.
  RELEASE vaspsl.
END.

/* -------------------------------------------------------------------------  */
PROCEDURE Total_TagValue:
/* -------------------------------------------------------------------------  */

/*  
  ASSIGN 
    s-FabTotalPrice    = 0
    s-pndextrnamt  = 0
    s-pndintrnamt  = 0
    s-pndintrndsp  = 0
    s-FabTotalCost     = 0
    s-vaprodcost   = 0
    t-cstfab-h     = 0
    t-cstfab-m     = 0
    t-cstdoc-h     = 0
    t-cstdoc-m     = 0
    t-cstcad-h     = 0
    t-cstcad-m     = 0.
 
  FIND FIRST vasp WHERE
             vasp.cono = g-cono         AND
             vasp.shipprod = x-quoteno  
  NO-LOCK NO-ERROR.

  FIND zsdivasp WHERE
       zsdivasp.cono = g-cono         AND
       zsdivasp.rectype = "fb"        AND
       zsdivasp.repairno = x-quoteno  
  NO-LOCK NO-ERROR.
 
  
  IF avail vasp and avail zsdivasp THEN DO:
    ASSIGN s-maxlead  = INT(zsdivasp.leadtm1).
    ASSIGN 
      p-pct      = IF zsdivasp.margpct > 0 THEN
                     zsdivasp.margpct
                   ELSE
                     035.00 
     s-FabTotalPrice = zsdivasp.repairtotl. 
  END.
  ELSE
    LEAVE.
  
  FIND FIRST vasp WHERE
             vasp.cono = g-cono         AND
             vasp.shipprod = x-quoteno  
  NO-LOCK NO-ERROR.
  DEF BUFFER f8-t-lines FOR t-lines.
  FOR EACH f8-t-lines use-index ix1 NO-LOCK:
    IF f8-t-lines.prod BEGINS "core-" THEN DO:
      ASSIGN 
        s-pndextrnamt = IF f8-t-lines.gain > 0 THEN
                          s-pndextrnamt + f8-t-lines.gain
                        ELSE
                          s-pndextrnamt.
      NEXT.
    END.
    ASSIGN 
      s-vaprodcost  = s-vaprodcost  + f8-t-lines.extcost
      s-pndextrnamt = IF f8-t-lines.gain > 0 THEN
                        s-pndextrnamt + f8-t-lines.gain
                      ELSE
                        s-pndextrnamt.
  END.
  IF v-maxlead >= s-maxlead THEN
    ASSIGN 
      s-maxlead  = v-maxlead
      s-FabTotalCost = s-vaprodcost + s-pndintrndsp + s-pndextrnamt
      s-trendtot = s-FabTotalCost * 1.04.
  
  IF avail vasp
   AND zsdivasp.override = "y" THEN
    LEAVE.
  ELSE
  updtloop:
  DO WHILE TRUE ON ENDKEY UNDO, LEAVE:
    FIND FIRST vasp WHERE
               vasp.cono = g-cono         AND
               vasp.shipprod = x-quoteno  
    EXCLUSIVE-LOCK NO-ERROR.
    FIND zsdivasp WHERE
         zsdivasp.cono = g-cono         AND
         zsdivasp.rectype = "fb"        AND
         zsdivasp.repairno = x-quoteno  
    EXCLUSIVE-LOCK NO-ERROR.

    ASSIGN 
      s-FabTotalPrice = 
        if round(((s-trendtot / (100 - p-pct)) * 100),2) -
        ((s-trendtot / (100 - p-pct) * 100)) <> 0 then
          round(((s-trendtot / (100 - p-pct)) * 100),2) 
          + .01 
        else
          round((s-trendtot / (100 - p-pct)) * 100),2)
      zsdivasp.leadtm1           = STRING(s-maxlead,"9999").
    LEAVE updtloop.
  END.
  RELEASE vasp.
  RELEASE zsdivasp.
*/
END. /* procedure */

/* ------------------------------------------------------------------------- */
PROCEDURE add-descrip:
  /* ------------------------------------------------------------------------- */
  DO:
    DEFINE INPUT PARAMETER ad-desc LIKE v-descrip NO-UNDO.
    DEFINE BUFFER z-icsec  FOR icsec.
    DEFINE BUFFER l-icsec  FOR icsec.
    DEFINE BUFFER x-icsec  FOR icsec.
    DEFINE BUFFER n-icsec  FOR icsec.
    DEFINE BUFFER om-icsec FOR icsec.
    
    DEFINE VAR h-user3 AS CHAR FORMAT "x(24)" NO-UNDO.
    
    /* if descrip already exists - do not make another */
    FIND FIRST x-icsec USE-INDEX k-icsec WHERE
               x-icsec.cono    = g-cono             AND
               x-icsec.altprod =  TRIM(ad-desc," ") AND
               x-icsec.rectype = "zx"               AND
               x-icsec.prod    = RIGHT-TRIM(v-model," ") + "^" +
                                 RIGHT-TRIM(s-prod," ")
    NO-LOCK NO-ERROR.
    IF avail x-icsec THEN DO:
      LEAVE.
    END.
    ELSE DO:
      /*** &&& is there a crossref model specific master descrip avail --
      if not create the master description here     **/
      FIND FIRST om-icsec USE-INDEX k-icsec WHERE
                 om-icsec.cono    = g-cono  AND
                 om-icsec.rectype = "zx"    AND
                 om-icsec.prod    = v-model AND
                 om-icsec.altprod =  TRIM(ad-desc," ")
      NO-LOCK NO-ERROR.
      IF NOT avail om-icsec THEN DO:
        FIND LAST l-icsec USE-INDEX k-keyno WHERE
                  l-icsec.cono = g-cono AND
                  l-icsec.rectype = "zx"
        NO-LOCK NO-ERROR.
        CREATE z-icsec.
        ASSIGN 
          z-icsec.cono = g-cono
          z-icsec.altprod = TRIM(ad-desc," ")
          z-icsec.prod    = TRIM(v-model," ")
          z-icsec.rectype = "zx"
          z-icsec.keyno   = IF avail l-icsec THEN
                              l-icsec.keyno + 1
                            ELSE
                              1
          z-icsec.operinit  = g-operinits
          z-icsec.transdt   = TODAY
          z-icsec.lastchgdt = TODAY.
      END.
    END.
    
    FIND LAST l-icsec USE-INDEX k-keyno WHERE
              l-icsec.cono = g-cono AND
              l-icsec.rectype = "zx"
    NO-LOCK NO-ERROR.
    CREATE z-icsec.
    ASSIGN 
      z-icsec.cono = g-cono
      z-icsec.altprod = TRIM(ad-desc," ")
      z-icsec.prod    = TRIM(v-model," ") + "^" + TRIM(s-prod)
      z-icsec.user3   = TRIM(s-prod," ")
      z-icsec.rectype = "zx"
      z-icsec.keyno   = IF avail l-icsec THEN
                          l-icsec.keyno + 1
                        ELSE
                          1
      z-icsec.operinit  = g-operinits
      z-icsec.transdt   = TODAY
      z-icsec.lastchgdt = TODAY.
  END.
END.


/* ------------------------------------------------------------------------- */
PROCEDURE Create_ICSW:
/* -------------------------------------------------------------------------*/
  DEF INPUT        PARAMETER ix-whse  LIKE icsw.whse                  NO-UNDO.
  DEF INPUT        PARAMETER ix-prod  LIKE icsw.prod                  NO-UNDO.
  DEF INPUT-OUTPUT PARAMETER cpyflg   AS LOGICAL                      NO-UNDO.
  
  DEF BUFFER t-icsw FOR icsw.
  DEF BUFFER n-icsw FOR icsw.
  DEF BUFFER x-icsl for icsl.
  DEF BUFFER x-icsd for icsd.
  DEF BUFFER x-icsp for icsp.
  
  def var    ix-vendno like apsv.vendno no-undo.
  def var    ix-prodcat  like icsp.prodcat no-undo.
  ASSIGN cpyflg = NO.
  FOR EACH icsd WHERE icsd.cono    = g-cono AND
           icsd.salesfl = YES    AND
           icsd.custno  = 0
    NO-LOCK:
    FIND FIRST t-icsw WHERE
               t-icsw.cono        = g-cono  AND
               t-icsw.prod        = ix-prod AND
               t-icsw.statustype <> "X"
    NO-LOCK NO-ERROR.
    IF NOT avail t-icsw THEN
    FIND FIRST t-icsw WHERE 
               t-icsw.cono        = g-cono  AND
               t-icsw.prod        = ix-prod AND
               t-icsw.statustype  = "X"
    NO-LOCK NO-ERROR.
    
    IF avail t-icsw THEN
    DO TRANSACTION:
      ASSIGN cpyflg  = YES.
      CREATE n-icsw.
      BUFFER-COPY t-icsw 
      EXCEPT 
        t-icsw.cono
        t-icsw.whse
        t-icsw.qtyonhand
        t-icsw.qtyreservd
        t-icsw.qtycommit
        t-icsw.qtybo
        t-icsw.qtyintrans
        t-icsw.qtyunavail
        t-icsw.qtyonorder
        t-icsw.qtyrcvd
        t-icsw.qtyreqrcv
        t-icsw.qtyreqshp
        t-icsw.qtydemand
        t-icsw.lastsodt
        t-icsw.availsodt
        t-icsw.nodaysso
        t-icsw.leadtmprio
        t-icsw.leadtmlast
        t-icsw.lastltdt
        t-icsw.priorltdt
        t-icsw.lastinvdt
        t-icsw.lastrcptdt
        t-icsw.lastcntdt
        t-icsw.lastpowtdt
        t-icsw.rpt852dt
        t-icsw.priceupddt
        t-icsw.issueunytd
        t-icsw.retinunytd
        t-icsw.retouunytd
        t-icsw.rcptunytd
        t-icsw.qtydemand
        t-icsw.enterdt
        t-icsw.linept
        t-icsw.orderpt
        t-icsw.frozentype
        t-icsw.safeallamt
        t-icsw.safeallpct
        t-icsw.usagerate
        t-icsw.ordqtyin
        t-icsw.ordqtyout
        t-icsw.frozenmmyy
        t-icsw.frozenmos
        t-icsw.binloc1
        t-icsw.binloc2
      TO n-icsw
      ASSIGN 
        n-icsw.cono       = g-cono
        n-icsw.whse       = ix-whse
        n-icsw.statustype = "O"
        n-icsw.leadtmavg  = 30
        n-icsw.frozenmmyy  = STRING(MONTH(TODAY),"99") +
                           SUBSTRING(STRING(YEAR(TODAY),"9999"),3,2)
        n-icsw.frozenmos   = 6
        n-icsw.binloc1     =  "New Part"
        n-icsw.enterdt     = TODAY
        n-icsw.frozentype  = "N".


      assign ix-vendno = n-icsw.arpvendno. 
     /* assign proper vendor and discounts */
      if n-icsw.arpvendno = 70500019  or 
         n-icsw.arpvendno = 222000019 or
         n-icsw.arpvendno = 9853210   or
         n-icsw.arpvendno = 222095000 or    
         n-icsw.arpvendno = 80054265  or
         n-icsw.arpvendno = 61950800  or
         n-icsw.arpvendno = 61950850  then 
        do:
        if n-icsw.whse = "SBUF" then
          do:
          if n-icsw.arpvendno = 80054265 then
            assign n-icsw.arpvendno = 61950850.
          else
            assign n-icsw.arpvendno = 61950800.
          assign n-icsw.replcost = ROUND(icsw.listprice * .43,2).
        end. /* SBUF */
        else
          do:
          assign n-icsw.stndcost = n-icsw.listprice * .40.
          find x-icsd where x-icsd.cono = g-cono and
                            x-icsd.whse = ix-whse
                            no-lock no-error.
          if avail x-icsd and (x-icsd.branchmgr = "Pab" or
                              (x-icsd.branchmgr = "Sun" and
                              (x-icsd.whse = "DROS" or
                               x-icsd.whse = "SROS"))) then
            do:
            assign n-icsw.replcost = if n-icsw.pricetype = "VICC" then
                                       ROUND((n-icsw.listprice * .39) * 1.10,2)
                                     else
                                       ROUND(n-icsw.listprice * .39,2).
            if n-icsw.arpvendno = 70500019 then
              assign n-icsw.arpvendno = 222000019.
            if n-icsw.arpvendno = 9853210 then
              assign n-icsw.arpvendno = 222095000.
          end. /* avail x-icsd */
          else 
            do:
            assign n-icsw.replcost = if n-icsw.pricetype = "VICC" then
                                       ROUND((n-icsw.listprice * .41) * 1.10,2)
                                     else
                                       ROUND(n-icsw.listprice * .41,2).
            if n-icsw.arpvendno = 222000019 then
              assign n-icsw.arpvendno = 70500019.
            if n-icsw.arpvendno = 222095000 then
              assign n-icsw.arpvendno = 9853210.
          end. /* not avail x-icsd */
        end. /* not SBUF */ 
        assign ix-vendno = n-icsw.arpvendno. 
        find x-icsp where x-icsp.cono = g-cono and
                          x-icsp.prod = n-icsw.prod
                          no-lock no-error.
        if avail x-icsp then
          assign ix-prodcat = x-icsp.prodcat.
        else
          assign ix-prodcat = "".
          
        find notes use-index k-notes where
             notes.cono         = g-cono and
             notes.notestype    = "zz" and
             notes.primarykey   = "standard cost markup" and
             notes.secondarykey = ix-prodcat and
             notes.pageno       = 1                           
             no-lock no-error.
        if not avail notes then
          find notes use-index k-notes where
               notes.cono         = g-cono and
               notes.notestype    = "zz" and
               notes.primarykey   = "standard cost markup" and
               notes.secondarykey = "" and
               notes.pageno       = 1
               no-lock no-error.
        if avail notes and dec(notes.noteln[1]) > 0 then
          assign n-icsw.stndcost   = n-icsw.stndcost *
                                   (1 + (dec(notes.noteln[1]) / 100)).
        else
          assign n-icsw.stndcost   = n-icsw.replcost.
      end.
 
      
      find icsl where icsl.cono   = g-cono and
                      icsl.whse     = t-icsw.whse and 
                      icsl.vendno   = t-icsw.arpvendno and
                      icsl.prodline = t-icsw.prodline no-lock no-error.
     
      if avail icsl then do:
      
        find x-icsl where x-icsl.cono     = g-cono and
                          x-icsl.whse     = n-icsw.whse and 
                          x-icsl.vendno   = ix-vendno and
                          x-icsl.prodline = n-icsw.prodline no-lock no-error.
        if not avail x-icsl then do:
          create x-icsl.
          buffer-copy icsl except icsl.cono
                                  icsl.whse
          to x-icsl
           assign x-icsl.cono = g-cono
                  x-icsl.whse = n-icsw.whse.
                
        /* assign proper vendor */
          if t-icsw.arpvendno = 70500019  or 
             t-icsw.arpvendno = 222000019 or
             t-icsw.arpvendno = 9853210   or    
             t-icsw.arpvendno = 222095000 or
             t-icsw.arpvendno = 80054265 then
            do: 
            if t-icsw.whse = "SBUF" then
              do:
              if t-icsw.arpvendno = 80054265 then
                assign t-icsw.arpvendno = 61950850.
              else
                assign t-icsw.arpvendno = 61950800.
            end.
            else
              do:
              find x-icsd where x-icsd.cono = g-cono and 
                                x-icsd.whse = n-icsw.whse 
                                no-lock no-error.
              if avail x-icsd and (x-icsd.branchmgr = "Pab" or
                                  (x-icsd.branchmgr = "Sun" and
                                  (x-icsd.whse = "DROS" or
                                   x-icsd.whse = "SROS"))) then 
                do:   
                if t-icsw.arpvendno = 70500019 then
                  assign x-icsl.vendno = 222000019.
                if t-icsw.arpvendno = 9853210 then
                  assign x-icsl.vendno = 222095000.
              end.
              else 
                do:
                if t-icsw.arpvendno = 222000019 then
                  assign x-icsl.vendno = 70500019.
                if t-icsw.arpvendno = 222095000 then
                  assign x-icsl.vendno = 9853210.
              end.    
            end. /* not SBUF */
          end.      
        end.
      end.
      find icswu where 
           icswu.cono = g-cono and
           icswu.prod = ix-prod and
           icswu.whse = ix-whse no-lock no-error.
      if not avail icswu then do:
        CREATE icswu.
        ASSIGN 
          icswu.cono        = g-cono
          icswu.prod        = ix-prod
          icswu.whse        = ix-whse.
        {t-all.i icswu}
        RELEASE icswu.
      end.
      LEAVE.
    END. /* if avail t-icsw */
  END. /* for each icsd */
  
END. /* Procedure Create_ICSW */


/* -------------------------------------------------------------------------  */
PROCEDURE Bottom2:
/* -------------------------------------------------------------------------  */
  def var x-framefrom as char no-undo.
  
  RUN makequoteno(g-fabquoteno,OUTPUT x-quoteno).
  IF v-errfl THEN DO:
    ASSIGN v-framestate = "t1".
    LEAVE.
  END.

/*  
  if frame-name = "f-top1" then do:
    run WhatPrompt.
    x-framefrom = "t1".
    
    ASSIGN v-framestate = "t1".
    FIND first OptionMoves WHERE
               OptionMoves.procedure  = v-framestate  NO-LOCK NO-ERROR.
      if avail OptionMoves then
        assign v-framestate = OptionMoves.PROCEDURE
               v-currentkeyplace = optionmoves.optionindex.
  end.
  else
  if frame-name = "f-mid1" then do:
    ASSIGN v-framestate = "m1".
    x-framefrom = "m1".
    FIND first OptionMoves WHERE
               OptionMoves.procedure  = v-framestate  NO-LOCK NO-ERROR.
      if avail OptionMoves then
        assign v-framestate = OptionMoves.PROCEDURE
               v-currentkeyplace = optionmoves.optionindex.
  end.
*/
  IF FRAME-NAME <> "f-mid1" THEN DO:
    PUT SCREEN ROW 22 COL 1 COLOR MESSAGE
    "  F6-Top  F7-Lines             F9-SPCNOTES                                       ".
  END.
  ELSE DO:
    PUT SCREEN ROW 22 COL 1 COLOR MESSAGE
    "  F6-Top  F7-Extended            F9-SPCNOTES                                       ".
  END.
  
  
  
  ASSIGN v-repositionfl = TRUE.
  
  DEFINE VAR ix AS INTEGER     NO-UNDO.
  DEFINE VAR del-fl AS LOGICAL NO-UNDO.
  DEFINE VAR fillit AS INT     NO-UNDO.
  ASSIGN v-comments  = ""
  v-comments2 = ""
  v-comments3 = "".
  FOR EACH notes
    WHERE notes.cono         = g-cono     AND
          notes.notestype    = "in"       AND
          notes.primarykey   = x-quoteno  
    NO-LOCK:
    DO v-idx = 1 TO 16:
      ASSIGN v-comments[v-idx] = notes.noteln[v-idx].
    END.
  END.
  FOR EACH notes
    WHERE notes.cono         = g-cono     AND
          notes.notestype    = "fa"       AND
          notes.primarykey   = x-quoteno  AND
          notes.requirefl    = NO         AND
          notes.pageno       = 1
    NO-LOCK:
    DO v-idx = 1 TO 16:
      ASSIGN v-comments3[v-idx] = notes.noteln[v-idx].
    END.
  END.
  
  Bottom2Loop:
  DO WHILE TRUE ON ENDKEY UNDO, LEAVE Bottom2Loop:
    
    IF v-repositionfl THEN DO:
      DO v-repositionix = 16 TO 1 BY -1:
        IF v-comments[v-repositionix] <> "" THEN DO:
          IF v-repositionix NE 16 THEN
          ASSIGN v-repositionix = v-repositionix + 1.
          LEAVE.
        END.
      END.
      IF v-repositionix = 0 THEN
      ASSIGN v-repositionix = 1.
      NEXT-PROMPT v-comments[v-repositionix] WITH FRAME f-bot2.
    END.
    
    IF LASTKEY = 312 AND v-comments[v-repositionix] = "" THEN
    ASSIGN v-repositionfl = FALSE.
    
    UPDATE 
      v-comments[1]
      v-comments[2]
      v-comments[3]
      v-comments[4]
      v-comments[5]
      v-comments[6]
      v-comments[7]
      v-comments[8]
      v-comments[9]
      v-comments[10]
      v-comments[11]
      v-comments[12]
      v-comments[13]
      v-comments[14]
      v-comments[15]
      v-comments[16]
    GO-ON ({k-vaexqon.i})
    WITH FRAME f-bot2
    EDITING: 
      IF v-repositionfl THEN DO:
        ASSIGN v-repositionfl = FALSE.
        IF v-repositionix = 1 THEN
          APPLY "right-end" TO v-comments[1] IN FRAME f-bot2.
        ELSE
        IF v-repositionix = 2 THEN
          APPLY "right-end" TO v-comments[2] IN FRAME f-bot2.
        ELSE
        IF v-repositionix = 3 THEN
          APPLY "right-end" TO v-comments[3] IN FRAME f-bot2.
        ELSE
        IF v-repositionix = 4 THEN
          APPLY "right-end" TO v-comments[4] IN FRAME f-bot2.
        ELSE
        IF v-repositionix = 5 THEN
          APPLY "right-end" TO v-comments[5] IN FRAME f-bot2.
        ELSE
        IF v-repositionix = 6 THEN
          APPLY "right-end" TO v-comments[6] IN FRAME f-bot2.
        ELSE
        IF v-repositionix = 7 THEN
          APPLY "right-end" TO v-comments[7] IN FRAME f-bot2.
        ELSE
        IF v-repositionix = 8 THEN
          APPLY "right-end" TO v-comments[8] IN FRAME f-bot2.
        ELSE
        IF v-repositionix = 9 THEN
          APPLY "right-end" TO v-comments[9] IN FRAME f-bot2.
        ELSE
        IF v-repositionix = 10 THEN
          APPLY "right-end" TO v-comments[10] IN FRAME f-bot2.
        ELSE
        IF v-repositionix = 11 THEN
          APPLY "right-end" TO v-comments[11] IN FRAME f-bot2.
        ELSE
        IF v-repositionix = 12 THEN
          APPLY "right-end" TO v-comments[12] IN FRAME f-bot2.
        ELSE
        IF v-repositionix = 13 THEN
          APPLY "right-end" TO v-comments[13] IN FRAME f-bot2.
        ELSE
        IF v-repositionix = 14 THEN
          APPLY "right-end" TO v-comments[14] IN FRAME f-bot2.
        ELSE
        IF v-repositionix = 15 THEN
          APPLY "right-end" TO v-comments[15] IN FRAME f-bot2.
        ELSE
        IF v-repositionix = 16 THEN
          APPLY "right-end" TO v-comments[16] IN FRAME f-bot2.
      END.
      READKEY.
      IF {k-func12.i} THEN DO:
        DO v-idx = 1 TO 16:
          ASSIGN v-comments[v-idx] = INPUT v-comments[v-idx].
        END.
        ASSIGN v-fidx2    = v-repositionix.
        APPLY LASTKEY.
        ASSIGN v-repositionfl = TRUE.
        NEXT bottom2loop.
      END.
      IF {k-jump.i} OR {k-cancel.i} OR
         KEYLABEL(LASTKEY) = "f6" or
         KEYLABEL(LASTKEY) = "f7" then
         DO:
        APPLY LASTKEY.
        RUN FindNextFrame (INPUT LASTKEY,
                           INPUT-OUTPUT v-CurrentKeyPlace,
                           INPUT-OUTPUT v-framestate).
        DISPLAY fs-1 WITH FRAME f-statusline.
        if {k-jump.i} OR {k-cancel.i} then
          LEAVE Bottom2Loop.
      END.
      APPLY LASTKEY.
    END. /* bottom2-loop */
    
    IF {k-accept.i} or KEYLABEL(LASTKEY) = "f6" or
         KEYLABEL(LASTKEY) = "f7"  THEN DO:
      DEFINE BUFFER b-vasp FOR vasp.
      FIND FIRST b-vasp USE-INDEX k-vasp WHERE
                 b-vasp.cono     = g-cono    AND
                 b-vasp.shipprod = x-quoteno 
      EXCLUSIVE-LOCK NO-ERROR.
      IF NOT avail b-vasp THEN
      LEAVE.
      FIND FIRST notes
      WHERE notes.cono         = g-cono     AND
            notes.notestype    = "in"       AND
            notes.primarykey   = x-quoteno  
      EXCLUSIVE-LOCK NO-ERROR.
      IF NOT avail notes THEN DO:
        CREATE notes.
        ASSIGN 
          b-vasp.notesfl     = IF avail b-vasp THEN
                                 "!"
                               ELSE
                                 ""
          notes.cono         = g-cono
          notes.notestype    = "in"
          notes.primarykey   = x-quoteno
          notes.printfl      = YES
          notes.pageno       = 1.
        DO v-idx = 1 TO 16:
          ASSIGN notes.noteln[v-idx] = v-comments[v-idx].
        END.
      END.
      ELSE
      DO:
        DO v-idx = 1 TO 16:
          ASSIGN notes.noteln[v-idx] = v-comments[v-idx].
        END.
      END.
      IF avail notes THEN DO:
        ASSIGN del-fl = YES.
        DO ix = 1 TO 16:
          IF notes.noteln[ix] NE "" THEN
          DO:
            ASSIGN del-fl = NO.
          END.
        END.
        IF del-fl = YES AND avail b-vasp THEN DO:
          ASSIGN b-vasp.notesfl  = "".
          DELETE notes.
        END.
      END.
      IF avail b-vasp THEN
        RELEASE b-vasp.
      IF avail notes THEN
        RELEASE notes.
      LEAVE Bottom2Loop.
    END.
  END.  /* Bottom2Loop */
  DEFINE BUFFER b-vasps FOR vasps.
  DEFINE VAR mv-idx AS INT NO-UNDO.
  ASSIGN mv-idx = 1.
  DO v-idx = 1 TO 16:
    IF v-comments[v-idx] NE "" THEN
    DO:
      ASSIGN v-comments2[v-idx] = v-comments[v-idx].
    END.
  END.
  FIND FIRST b-vasps USE-INDEX k-vasp WHERE
             b-vasps.cono     = g-cono    AND
             b-vasps.shipprod = x-quoteno AND
             b-vasps.whse     = x-whse    AND
             b-vasps.seqno    = 1         AND
             b-vasps.sctntype = "sp"
  EXCLUSIVE-LOCK NO-ERROR.
  IF avail b-vasps THEN
  DO:
    ASSIGN b-vasps.specdata = "".
    /*    assign substring(b-vasps.specdata,mv-idx,59) =
    "------------------ Specification Notes----------------------"  + "~012"
    assign mv-idx = mv-idx + 61.   */
    DO v-idx = 1 TO 16:
      IF v-comments2[v-idx] NE "" THEN
      DO:
        ASSIGN 
          fillit = LENGTH(v-comments2[v-idx])
          v-comments2[v-idx] = v-comments2[v-idx] + FILL(" ",60 - fillit)
          SUBSTRING(b-vasps.specdata,mv-idx,59) =
                    v-comments2[v-idx] + "~012"
          mv-idx = mv-idx + 61.
      END.
    END.
    /*
    assign substring(b-vasps.specdata,mv-idx,59) =
    "---------------- Printable External Notes ------------------"  + "~012"
    mv-idx = mv-idx + 61.
    do v-idx = 1 to 16:
    if v-comments3[v-idx] ne "" then
    do:
    assign fillit = length(v-comments3[v-idx])
    v-comments3[v-idx] =
    v-comments3[v-idx] + fill(" ",60 - fillit)
    substring(b-vasps.specdata,mv-idx,59) =
    v-comments3[v-idx] + "~012"
    mv-idx = mv-idx + 61.
    end.
    end.
    */
  END.


  IF KEYLABEL(LASTKEY) = "f6"  THEN
    ASSIGN v-framestate = "t1".
  ELSE
  IF KEYLABEL(LASTKEY) = "f7"  THEN
    ASSIGN v-framestate = "m1".
/*  
  ELSE
  IF x-framefrom = "t1" THEN
    ASSIGN v-framestate = "t1".
  ELSE
  IF x-framefrom = "m1"  THEN
    ASSIGN v-framestate = "m1".
  ELSE
    ASSIGN v-framestate = "t1".

  
  IF v-framestate = "t1" THEN
    DISPLAY fs-1 WITH FRAME f-statusline.
  ELSE
    DISPLAY fs-2 WITH FRAME f-statusline2.
  HIDE FRAME f-bot2.
*/


  FIND first OptionMoves WHERE
             OptionMoves.procedure  = v-framestate  NO-LOCK NO-ERROR.
  if avail OptionMoves then
    assign v-framestate = OptionMoves.PROCEDURE
           v-currentkeyplace = optionmoves.optionindex.
/*   ELSE  DO:    */

  RUN FindNextFrame (INPUT LASTKEY,
                     INPUT-OUTPUT v-CurrentKeyPlace,
                     INPUT-OUTPUT v-framestate).
/*  END.        */

  if {k-cancel.i} then do:
    readkey pause 0.
  end.  
  
  
  IF {k-jump.i} /* OR {k-cancel.i} */ THEN DO:
    RUN FindNextFrame (INPUT LASTKEY,
                       INPUT-OUTPUT v-CurrentKeyPlace,
                       INPUT-OUTPUT v-framestate).
  END.
END.

/* -------------------------------------------------------------------------  */
PROCEDURE TopCustlookup:
  /* -------------------------------------------------------------------------  */
  ASSIGN 
    v-topcustlu   = ""
    v-browsekeyed = FALSE.
  DISPLAY v-topcustlu WITH FRAME f-toplu.
  DISPLAY v-custlulit WITH FRAME f-custlu.
  TopCustLoop:
  DO WHILE TRUE ON ENDKEY UNDO, LEAVE TopCustLoop:
    UPDATE v-topcustlu
    GO-ON ({k-vaexqon.i})
    WITH FRAME f-toplu
    EDITING:
      READKEY.
      
      IF CAN-DO("f6,f7,f9,f11",KEYLABEL(LASTKEY)) THEN
      DO:
        CLOSE QUERY q-custlu.
        ON cursor-up back-tab.
        ON cursor-down tab.
        ASSIGN v-custbrowseopen = FALSE.
        DISPLAY "" @ v-custlulit WITH FRAME f-custlu.
        HIDE FRAME f-custlu.
        HIDE FRAME f-zcustinfo.
        APPLY LASTKEY.
        LEAVE TopCustLoop.
      END.
      IF FRAME-FIELD = "v-topcustlu" THEN
      DO:
        IF NOT CAN-DO("9,13,502,508,507",STRING(LASTKEY)) THEN
        ASSIGN v-custbrowsekeyed = FALSE.
        IF (NOT CAN-DO("9,21,13,501,502,503,504,507,508",STRING(LASTKEY)) AND
            KEYFUNCTION(LASTKEY) NE "go" AND
            KEYFUNCTION(LASTKEY) NE "end-error") THEN
        DO:
          APPLY LASTKEY.
          IF FRAME-FIELD <> "v-topcustlu" THEN
            NEXT.
          ASSIGN 
            v-topcustlu = INPUT v-topcustlu
            v-custbrowsekeyed = FALSE.
          
          /* keystroke logic done here key by key */
          IF funcflag = NO THEN
          DO:
            RUN DoCustLu (INPUT v-topcustlu,
                          INPUT "normal",
                          INPUT "f-toplu" ).
          END.
          ELSE
            ASSIGN funcflag = NO.
          ASSIGN v-custbrowsekeyed = FALSE.
          NEXT.
        END.
        ELSE IF (LASTKEY = 501 OR
                 LASTKEY = 502 OR
                 LASTKEY = 507 OR
                 LASTKEY = 508) AND v-custbrowseopen = TRUE THEN
        DO:
          RUN MoveCustLu (INPUT LASTKEY,
                          INPUT "f-toplu").
          ASSIGN v-custbrowsekeyed = TRUE.
          NEXT.
        END.
        ELSE
        IF (LASTKEY = 13 OR KEYFUNCTION(LASTKEY) = "go")
          AND NOT avail arsc THEN
        DO:
          ASSIGN v-topcustlu = INPUT v-topcustlu.
          DISPLAY  
            v-custname
            v-custnoa
          WITH FRAME f-top1.
          ASSIGN v-custbrowsekeyed = FALSE.
          HIDE FRAME f-custlu.
          HIDE FRAME f-zcustinfo.
        END.
        ELSE
        IF (LASTKEY = 13 OR KEYFUNCTION(LASTKEY) = "go")
        AND v-custbrowsekeyed AND avail arsc THEN
        DO:
          ASSIGN 
            v-custlu    = arsc.lookupnm
            v-topcustlu = arsc.lookupnm
            v-custno    = arsc.custno
            v-custnoa   = STRING(arsc.custno)
            v-custname  = arsc.NAME.
          DISPLAY 
            v-custname
            v-custnoa
            v-takenby
          WITH FRAME f-top1.
          DISPLAY "" @ v-custlulit WITH FRAME f-custlu.
          ASSIGN v-custbrowsekeyed = FALSE.
          HIDE FRAME f-toplu.
          HIDE FRAME f-custlu.
          HIDE FRAME f-zcustinfo.
          APPLY LASTKEY.
          LEAVE.
        END.
        ELSE
        ASSIGN v-custbrowsekeyed = FALSE.
      END. /* not can-do */
      APPLY LASTKEY.
    END. /* editing */
    IF KEYFUNCTION(LASTKEY) = "go" OR
    LASTKEY = 13 THEN
    DO:
      CLOSE QUERY q-custlu.
      ASSIGN 
        v-custbrowseopen  = FALSE
        v-custbrowsekeyed = FALSE.
      ON cursor-up back-tab.
      ON cursor-down tab.
      HIDE FRAME f-toplu.
      HIDE FRAME f-custlu.
      HIDE FRAME f-zcustinfo.
    END.
    IF {k-accept.i} OR LASTKEY = 13 THEN DO:
      HIDE FRAME f-toplu.
      LEAVE TopCustLoop.
    END.
  END.   /* TopCustLoop */
  
  {k-sdinavigation.i &jmpcode = "return." }
  
END.

/* -------------------------------------------------------------------------  */
PROCEDURE DocustLu:
  /* -------------------------------------------------------------------------  */
  
  DEFINE INPUT PARAMETER ix-custlu    AS CHARACTER FORMAT "x(15)" NO-UNDO.
  DEFINE INPUT PARAMETER ix-runtype   AS CHARACTER                NO-UNDO.
  DEFINE INPUT PARAMETER ix-callframe AS CHARACTER                NO-UNDO.
  ASSIGN 
    v-custnum = REPLACE(ix-custlu,"0","")
    v-custnum = REPLACE(v-custnum," ","")
    v-custnum = REPLACE(v-custnum,"1","")
    v-custnum = REPLACE(v-custnum,"2","")
    v-custnum = REPLACE(v-custnum,"3","")
    v-custnum = REPLACE(v-custnum,"4","")
    v-custnum = REPLACE(v-custnum,"5","")
    v-custnum = REPLACE(v-custnum,"6","")
    v-custnum = REPLACE(v-custnum,"7","")
    v-custnum = REPLACE(v-custnum,"8","")
    v-custnum = REPLACE(v-custnum,"9","").
  
  HIDE FRAME f-custlu.
  
  IF LENGTH(v-custnum) = 0 AND ix-runtype = "normal" AND
    v-custlutype = g-custlutype AND ix-custlu <> "" THEN
  DO:
    FOR EACH b-w-cust:
      DELETE b-w-cust.
    END.
    IF CAN-FIND(FIRST arsc USE-INDEX k-arsc-stat WHERE
                      arsc.cono = g-cono AND
                      arsc.custno GE DEC(ix-custlu) AND
                      arsc.statustype = YES NO-LOCK) THEN DO:
      CREATE b-w-cust.
      ASSIGN b-w-cust.custno = ?. /*trick query so they all can be the same*/
    END.
    OPEN QUERY q-custlu FOR EACH b-w-cust NO-LOCK,
      EACH arsc OUTER-JOIN USE-INDEX k-arsc-stat WHERE
           arsc.cono = g-cono AND
           arsc.custno GE DEC(ix-custlu) AND
           arsc.statustype = YES NO-LOCK.
    ASSIGN v-custlulit =
 " Shift/F7-LOOKUPNM  Shift/F8-Phone  Shift/F9-Keywds  Shift/F10-City/St/Zip ".
    DISPLAY v-custlulit WITH FRAME f-custlu.
  END.
  ELSE
  IF v-custlutype = "l" THEN
  DO:
    FOR EACH b-w-cust:
      DELETE b-w-cust.
    END.
    IF CAN-FIND(FIRST arsc USE-INDEX k-lkup-stat WHERE
                      arsc.cono = g-cono AND
                      arsc.lookupnm BEGINS ix-custlu AND
                      arsc.statustype = YES NO-LOCK) THEN DO:
      CREATE b-w-cust.
      ASSIGN b-w-cust.custno = ?. /*trick query so they all can be the same*/
    END.
    OPEN QUERY q-custlu FOR EACH b-w-cust NO-LOCK,
      EACH arsc OUTER-JOIN USE-INDEX k-lkup-stat WHERE
           arsc.cono = g-cono AND
           arsc.lookupnm BEGINS ix-custlu AND
           arsc.statustype = YES NO-LOCK.
    ASSIGN v-custlulit =
 " Shift/F7-LOOKUPNM  Shift/F8-Phone  Shift/F9-Keywds  Shift/F10-City/St/Zip ".
    DISPLAY v-custlulit WITH FRAME f-custlu.
  END.
  ELSE
  IF v-custlutype = "p" THEN DO:
    IF ix-runtype = "function" THEN DO:
      FOR EACH b-w-cust:
        DELETE b-w-cust.
      END.
      Phoneloop:
      DO WHILE TRUE ON ENDKEY UNDO Phoneloop, LEAVE Phoneloop:
        UPDATE
          v-lphone
        GO-ON (f17,f18,f19,f20,f6,f7,f8,f9 )
        WITH FRAME f-arphone.
        IF v-lphone = "" AND {k-accept.i} THEN DO:
          ASSIGN v-error = "This is a Required Field".
          RUN zsdi_vaerrx.p(v-error,"yes").
          NEXT-PROMPT v-lphone WITH FRAME f-arphone.
          NEXT Phoneloop.
        END.
        IF CAN-FIND(FIRST arsc USE-INDEX k-phone-stat WHERE
                          arsc.cono = g-cono AND
                          arsc.phoneno BEGINS v-lphone   AND
                          arsc.lookupnm BEGINS ix-custlu AND
                          arsc.statustype = YES NO-LOCK) THEN DO:
          CREATE b-w-cust.
          ASSIGN b-w-cust.custno = ?. /*trick query they  can be the same*/
        END.
        HIDE FRAME f-arphone.
        IF {k-accept.i} OR {k-cancel.i} THEN
        LEAVE Phoneloop.
        IF {k-jump.i} THEN DO:
          RETURN.
        END.
        IF KEYFUNCTION(LASTKEY) = "f17" OR
           KEYFUNCTION(LASTKEY) = "f18" OR
           KEYFUNCTION(LASTKEY) = "f19" OR
           KEYFUNCTION(LASTKEY) = "f20" OR
           KEYFUNCTION(LASTKEY) = "f6" OR
           KEYFUNCTION(LASTKEY) = "f7" OR
           KEYFUNCTION(LASTKEY) = "f8" OR
           KEYFUNCTION(LASTKEY) = "f9" THEN DO:
          RETURN.
        END.
      END.         /* PhoneLoop */
      HIDE FRAME f-arphone.
      OPEN QUERY q-custlu FOR EACH b-w-cust NO-LOCK,
        EACH arsc OUTER-JOIN USE-INDEX k-phone-stat
        WHERE arsc.cono = g-cono AND
              arsc.phoneno BEGINS v-lphone   AND
              arsc.lookupnm BEGINS ix-custlu AND
              arsc.statustype = YES NO-LOCK.
      ASSIGN v-custlulit =
 " Shift/F7-LookupNm  Shift/F8-PHONE  Shift/F9-Keywds  Shift/F10-City/St/Zip ".
    END.
    ELSE
    IF ix-runtype = "normal" THEN DO:
      HIDE FRAME f-arphone.
      OPEN QUERY q-custlu FOR EACH b-w-cust NO-LOCK,
        EACH arsc OUTER-JOIN USE-INDEX k-phone-stat
        WHERE arsc.cono = g-cono AND
              arsc.phoneno BEGINS v-lphone   AND
              arsc.lookupnm BEGINS ix-custlu AND
              arsc.statustype = YES NO-LOCK.
      ASSIGN v-custlulit =
 " Shift/F7-LookupNm  Shift/F8-PHONE  Shift/F9-Keywds  Shift/F10-City/St/Zip ".
    END.
  END.
  ELSE
  IF v-custlutype = "z" THEN
  DO:
    FOR EACH b-w-cust:
      DELETE b-w-cust.
    END.
    IF ix-runtype = "function" THEN
    DO:
      ziploop:
      DO WHILE TRUE ON ENDKEY UNDO ziploop, LEAVE ziploop:
        UPDATE
          v-lcity
          v-lstate
          v-lzipcd
        GO-ON (f17,f18,f19,f20,f6,f7,f8,f9 )
        WITH FRAME f-arzipcd.
        
        IF v-lcity <> "" AND v-lstate = "" AND {k-accept.i} THEN
        DO:
          ASSIGN v-error = "This is a Required Field".
          RUN zsdi_vaerrx.p(v-error,"yes").
          NEXT-PROMPT v-lstate WITH FRAME f-arzipcd.
          NEXT ziploop.
        END.
        HIDE FRAME f-arzipcd.
        IF {k-accept.i} OR {k-cancel.i} THEN
        LEAVE ziploop.
        IF {k-jump.i} THEN DO:
          RETURN.
        END.
        IF KEYFUNCTION(LASTKEY) = "f17" OR
           KEYFUNCTION(LASTKEY) = "f18" OR
           KEYFUNCTION(LASTKEY) = "f19" OR
           KEYFUNCTION(LASTKEY) = "f20" OR
           KEYFUNCTION(LASTKEY) = "f6" OR
           KEYFUNCTION(LASTKEY) = "f7" OR
           KEYFUNCTION(LASTKEY) = "f8" OR
           KEYFUNCTION(LASTKEY) = "f9" THEN DO:
          RETURN.
        END.
      END.         /* ZipLoop */
      HIDE FRAME f-arzipcd.
      IF v-lcity = "" AND v-lstate = "" AND v-lzipcd <> "" AND
        CAN-FIND(FIRST arsc USE-INDEX k-zipcd-stat  WHERE
                       arsc.cono = g-cono AND
                       arsc.zipcd BEGINS v-lzipcd     AND
                       arsc.lookupnm BEGINS ix-custlu AND
                       arsc.statustype = YES NO-LOCK) THEN
      DO:
        CREATE b-w-cust.
        ASSIGN b-w-cust.custno = ?. /*trick query they can be the same*/
      END.
      ELSE
      IF CAN-FIND(FIRST arsc USE-INDEX k-addr-stat WHERE
                        arsc.cono = g-cono AND
                        arsc.state = v-lstate AND
                        arsc.city  BEGINS v-lcity AND
                        arsc.lookupnm BEGINS ix-custlu AND
                        arsc.statustype = YES NO-LOCK) THEN
      DO:
        CREATE b-w-cust.
        ASSIGN b-w-cust.custno = ?. /*trick query they can be the same*/
      END.
      IF v-lcity = "" AND v-lstate = "" AND v-lzipcd <> "" THEN
      DO:
        OPEN QUERY q-custlu FOR EACH b-w-cust  NO-LOCK,
        
          EACH arsc OUTER-JOIN USE-INDEX k-zipcd-stat
          WHERE arsc.cono = g-cono AND
                arsc.zipcd BEGINS v-lzipcd     AND
                arsc.lookupnm BEGINS ix-custlu AND
                arsc.statustype = YES NO-LOCK.
        ASSIGN v-custlulit =
 " Shift/F7-LookupNm  Shift/F8-Phone  Shift/F9-Keywds  Shift/F10-City/St/ZIP ".
        DISPLAY v-custlulit WITH FRAME f-custlu.
      END.
      ELSE
      DO:
        OPEN QUERY q-custlu FOR EACH b-w-cust  NO-LOCK,
        
          EACH arsc OUTER-JOIN USE-INDEX k-addr-stat
          WHERE arsc.cono = g-cono AND
                arsc.state = v-lstate AND
                arsc.city  BEGINS v-lcity AND
                arsc.lookupnm BEGINS ix-custlu AND
                arsc.statustype = YES NO-LOCK.
        ASSIGN v-custlulit =
 " Shift/F7-LookupNm  Shift/F8-Phone  Shift/F9-Keywds  Shift/F10-CITY/ST/Zip ".
      END.
    END.
    ELSE
    IF ix-runtype = "normal" THEN
    DO:
      IF v-lcity = "" AND v-lstate = "" AND v-lzipcd <> "" AND
        CAN-FIND(FIRST arsc USE-INDEX k-zipcd-stat WHERE
                       arsc.cono = g-cono AND
                       arsc.zipcd BEGINS v-lzipcd     AND
                       arsc.lookupnm BEGINS ix-custlu AND
                       arsc.statustype = YES NO-LOCK) THEN
      DO:
        CREATE b-w-cust.
        ASSIGN b-w-cust.custno = ?. /*trick query they can be the same*/
      END.
      ELSE
      IF CAN-FIND(FIRST arsc USE-INDEX k-addr-stat WHERE
                        arsc.cono = g-cono AND
                        arsc.state = v-lstate AND
                        arsc.city  BEGINS v-lcity AND
                        arsc.lookupnm BEGINS ix-custlu AND
                        arsc.statustype = YES NO-LOCK) THEN
      DO:
        CREATE b-w-cust.
        ASSIGN b-w-cust.custno = ?. /*trick query they can be the same*/
      END.
      IF v-lcity = "" AND v-lstate = "" AND v-lzipcd <> "" THEN
      DO:
        OPEN QUERY q-custlu FOR EACH b-w-cust  NO-LOCK,
        
          EACH arsc OUTER-JOIN
          USE-INDEX k-zipcd-stat 
          WHERE arsc.cono = g-cono AND
                arsc.zipcd BEGINS v-lzipcd AND
                arsc.lookupnm BEGINS ix-custlu AND
                arsc.statustype = YES NO-LOCK.
        ASSIGN v-custlulit =
 " Shift/F7-LookupNm  Shift/F8-Phone  Shift/F9-Keywds  Shift/F10-City/St/ZIP ".
        DISPLAY v-custlulit WITH FRAME f-custlu.
      END.
      ELSE
      DO:
        OPEN QUERY q-custlu FOR EACH b-w-cust  NO-LOCK,
        EACH arsc OUTER-JOIN
          USE-INDEX k-addr-stat 
          WHERE arsc.cono = g-cono AND
                arsc.state = v-lstate AND
                arsc.city  BEGINS v-lcity AND
                arsc.lookupnm BEGINS ix-custlu AND
                arsc.statustype = YES NO-LOCK.
        ASSIGN v-custlulit =
  " Shift/F7-LookupNm  Shift/F8-Phone  Shift/F9-Keywds  Shift/F10-CITY/ST/Zip ".
      END.
    END.
  END.  /* if z */
  ELSE
  IF v-custlutype = "k" THEN
  DO:
    IF CAN-FIND(FIRST arsc USE-INDEX k-lkup-stat WHERE
                      arsc.cono = g-cono             AND
                      arsc.lookupnm BEGINS ix-custlu AND
                      arsc.statustype = YES NO-LOCK) THEN
    DO:
      CREATE b-w-cust.
      ASSIGN b-w-cust.custno = ?. /*trick query so they all can be the same*/
    END.
    IF ix-runtype = "function" THEN
    DO:
      RUN zsdivacustkwd.p.
      OPEN QUERY q-custlu FOR EACH b-w-cust  NO-LOCK,
      
        EACH arsc USE-INDEX k-arsc-stat
        WHERE arsc.cono = g-cono AND
              arsc.custno = b-w-cust.custno AND
              arsc.lookupnm BEGINS ix-custlu AND
              arsc.statustype = YES NO-LOCK.
      ASSIGN v-custlulit =
" Shift/F7-Lookupnm  Shift/F8-Phone  Shift/F9-KEYWDS  Shift/F10-City/St/Zip ".
      DISPLAY v-custlulit WITH FRAME f-custlu.
      ASSIGN v-error = " Retrieving customer records - please wait..".
      RUN zsdi_vaerrx.p(v-error,"yes").
      PAUSE 1.
    END.
    ELSE
    IF ix-runtype = "normal" THEN
    DO:
      OPEN QUERY q-custlu FOR EACH b-w-cust  NO-LOCK,
        EACH arsc USE-INDEX k-arsc-stat 
        WHERE arsc.cono = g-cono AND
              arsc.custno = b-w-cust.custno AND
              arsc.lookupnm BEGINS ix-custlu AND
              arsc.statustype = YES NO-LOCK.
      ASSIGN v-custlulit =
" Shift/F7-Lookupnm  Shift/F8-Phone  Shift/F9-KEYWDS  Shift/F10-City/St/Zip ".
      DISPLAY v-custlulit WITH FRAME f-custlu.
    END.
  END. /* type x */
  
  ASSIGN v-custbrowseopen = TRUE.
  
  IF ix-callframe = "f-top1" THEN
    ASSIGN 
      v-custbrowserow = 6
      lu-row = 16.
  IF avail arsc THEN
    RUN bottom_custva.p (INPUT arsc.custno,
                         INPUT " ").
  DISPLAY b-custlu v-custlulit WITH FRAME f-custlu.
  
  IF ix-callframe = "f-top1" THEN
    APPLY "focus" TO v-custnoa IN FRAME f-top1.
  PUT SCREEN ROW 23 COL 1 v-error.
  
END.

/* -------------------------------------------------------------------------  */
PROCEDURE MovecustLu:
  /* -------------------------------------------------------------------------  */
  
  DEFINE INPUT PARAMETER ix-lastkey AS INTEGER                 NO-UNDO.
  DEFINE INPUT PARAMETER ix-callframe AS CHARACTER             NO-UNDO.
  
  ENABLE b-custlu WITH FRAME f-custlu.
  APPLY LASTKEY TO b-custlu IN FRAME f-custlu.
  IF ix-callframe = "f-top1" THEN
    DISPLAY v-custlulit WITH FRAME f-custlu.
  IF LASTKEY = 502 AND v-custbrowsekeyed = FALSE THEN DO:
    APPLY x-cursorup TO b-custlu IN FRAME f-custlu.
  END.
  IF avail arsc THEN
    RUN bottom_custva.p (INPUT arsc.custno,
                         INPUT " ").
  IF ix-callframe = "f-top1" THEN DO:
    APPLY "focus" TO v-custnoa IN FRAME f-top1.
  END.
  
END.

/* -------------------------------------------------------------------------  */
PROCEDURE DoShiptoLu:
  /* -------------------------------------------------------------------------  */
  
  DEFINE INPUT PARAMETER ix-shiptolu  AS CHARACTER FORMAT "x(8)"  NO-UNDO.
  DEFINE INPUT PARAMETER ix-runtype   AS CHARACTER                NO-UNDO.
  DEFINE INPUT PARAMETER ix-callframe AS CHARACTER                NO-UNDO.
  
  HIDE FRAME f-shiptolu.
  IF v-Shiptolutype = "l" THEN
  DO:
    FOR EACH b-w-shipto:
      DELETE b-w-shipto.
    END.
    IF CAN-FIND(FIRST arss WHERE
                      arss.cono = g-cono AND
                      arss.custno = v-custno AND
                      arss.shipto BEGINS ix-Shiptolu NO-LOCK) THEN
    DO:
      CREATE b-w-shipto.
      ASSIGN 
        b-w-shipto.custno = ?
        b-w-shipto.shipto = "".
    END.
    
    OPEN QUERY q-shiptolu FOR EACH b-w-shipto NO-LOCK,
      EACH arss OUTER-JOIN 
      WHERE arss.cono = g-cono AND
            arss.custno = v-custno AND
            arss.shipto BEGINS ix-Shiptolu NO-LOCK,
      EACH arsc 
      WHERE arsc.cono = 1 AND
            arsc.custno = arss.custno NO-LOCK.
    ASSIGN v-shiptolulit =
    " Shift/F7-LOOKUPNM  Shift/F8-Phone                  Shift/F10-City/St/Zip ".
  END.
  ELSE
  IF v-shiptolutype = "p" THEN
  DO:
    FOR EACH b-w-shipto:
      DELETE b-w-shipto.
    END.
    IF ix-runtype = "function" THEN
    DO:
      Phoneloop:
      DO WHILE TRUE ON ENDKEY UNDO Phoneloop, LEAVE Phoneloop:
        UPDATE
          v-lphone
        GO-ON (f17,f18,f19,f20,f6,f7,f8,f9 )
        WITH FRAME f-arphone.
        
        IF v-lphone = "" AND {k-accept.i} THEN
        DO:
          ASSIGN v-error = "This is a Required Field".
          RUN zsdi_vaerrx.p(v-error,"yes").
          NEXT-PROMPT v-lphone WITH FRAME f-arphone.
          NEXT Phoneloop.
        END.
        HIDE FRAME f-arphone.
        IF {k-accept.i} OR {k-cancel.i} THEN
          LEAVE Phoneloop.
        IF {k-jump.i} THEN
        DO:
          RETURN.
        END.
        IF KEYFUNCTION(LASTKEY) = "f17" OR
           KEYFUNCTION(LASTKEY) = "f18" OR
           KEYFUNCTION(LASTKEY) = "f19" OR
           KEYFUNCTION(LASTKEY) = "f20" OR
           KEYFUNCTION(LASTKEY) = "f6" OR
           KEYFUNCTION(LASTKEY) = "f7" OR
           KEYFUNCTION(LASTKEY) = "f8" OR
           KEYFUNCTION(LASTKEY) = "f9" THEN
        DO:
          RETURN.
        END.
      END.         /* PhoneLoop */
      IF CAN-FIND(FIRST arss USE-INDEX k-phone WHERE
                        arss.cono = g-cono AND
                        arss.custno = v-custno AND
                        arss.phoneno BEGINS v-lphone AND
                        arss.shipto BEGINS ix-Shiptolu NO-LOCK) THEN
      DO:
        CREATE b-w-shipto.
        ASSIGN 
          b-w-shipto.custno = ?
          b-w-shipto.shipto = "".
      END.
      OPEN QUERY q-shiptolu FOR EACH b-w-shipto NO-LOCK,
        EACH arss OUTER-JOIN USE-INDEX k-phone 
        WHERE arss.cono = g-cono AND
              arss.custno = v-custno AND
              arss.phoneno BEGINS v-lphone AND
              arss.shipto BEGINS ix-Shiptolu NO-LOCK,
        EACH arsc WHERE arsc.cono = 1 AND
             arsc.custno = arss.custno NO-LOCK.
      ASSIGN v-shiptolulit =
      " Shift/F7-Lookupnm  Shift/F8-PHONE                  Shift/F10-City/St/Zip ".
    END.
  END.
  ELSE
  IF v-shiptolutype = "z" THEN
  DO:
    FOR EACH b-w-shipto:
      DELETE b-w-shipto.
    END.
    IF ix-runtype = "function" THEN
    DO:
      ziploop:
      DO WHILE TRUE ON ENDKEY UNDO ziploop, LEAVE ziploop:
        UPDATE
          v-lcity
          v-lstate
          v-lzipcd
        GO-ON (f17,f18,f19,f20,f6,f7,f8,f9 )
        WITH FRAME f-arzipcd.
        
        IF v-lcity <> "" AND v-lstate = "" AND {k-accept.i} THEN
        DO:
          ASSIGN v-error = "This is a Required Field".
          RUN zsdi_vaerrx.p(v-error,"yes").
          NEXT-PROMPT v-lstate WITH FRAME f-arzipcd.
          NEXT ziploop.
        END.
        HIDE FRAME f-arzipcd.
        IF {k-accept.i} OR {k-cancel.i} THEN
          LEAVE ziploop.
        IF {k-jump.i} THEN
        DO:
          RETURN.
        END.
        IF KEYFUNCTION(LASTKEY) = "f17" OR
           KEYFUNCTION(LASTKEY) = "f18" OR
           KEYFUNCTION(LASTKEY) = "f19" OR
           KEYFUNCTION(LASTKEY) = "f20" OR
           KEYFUNCTION(LASTKEY) = "f6" OR
           KEYFUNCTION(LASTKEY) = "f7" OR
           KEYFUNCTION(LASTKEY) = "f8" OR
           KEYFUNCTION(LASTKEY) = "f9" THEN
        DO:
          RETURN.
        END.
      END.         /* ZipLoop */
      
      IF v-lcity = "" AND v-lstate = "" AND v-lzipcd <> "" AND
        CAN-FIND(FIRST arss USE-INDEX k-zipcd WHERE
                       arss.cono = g-cono AND
                       arss.custno = v-custno AND
                       arss.zipcd  BEGINS v-lzipcd AND
                       arss.shipto BEGINS ix-Shiptolu NO-LOCK) THEN
      DO:
        CREATE b-w-shipto.
        ASSIGN
          b-w-shipto.custno = ?
          b-w-shipto.shipto = "".
      END.
      ELSE
      IF CAN-FIND(FIRST arss USE-INDEX k-addr WHERE
                        arss.cono = g-cono AND
                        arss.custno = v-custno AND
                        arss.state = v-lstate AND
                        arss.city  BEGINS v-lcity AND
                        arss.shipto BEGINS ix-Shiptolu NO-LOCK) THEN
      DO:
        CREATE b-w-shipto.
        ASSIGN 
          b-w-shipto.custno = ?
          b-w-shipto.shipto = "".
      END.
      
      IF v-lcity = "" AND v-lstate = "" AND v-lzipcd <> "" THEN
      DO:
        OPEN QUERY q-shiptolu FOR EACH b-w-shipto  NO-LOCK,
          EACH arss OUTER-JOIN USE-INDEX k-zipcd 
          WHERE arss.cono = g-cono AND
                arss.custno = v-custno AND
                arss.zipcd BEGINS v-lzipcd AND
                arss.shipto BEGINS ix-Shiptolu NO-LOCK,
          EACH arsc WHERE arsc.cono = 1 AND
               arsc.custno = arss.custno NO-LOCK.
        ASSIGN v-shiptolulit =
        " Shift/F7-Lookupnm  Shift/F8-Phone                  Shift/F10-City/St/ZIP ".
      END.
      ELSE
      DO:
        OPEN QUERY q-shiptolu FOR EACH b-w-shipto  NO-LOCK,
          EACH arss OUTER-JOIN USE-INDEX k-addr 
          WHERE arss.cono = g-cono AND
                arss.custno = v-custno AND
                arss.state = v-lstate AND
                arss.city  BEGINS v-lcity AND
                arss.shipto BEGINS ix-Shiptolu NO-LOCK,
          EACH arsc WHERE arsc.cono = 1 AND
               arsc.custno = arss.custno NO-LOCK.
        ASSIGN v-shiptolulit =
        " Shift/F7-Lookupnm  Shift/F8-Phone                  Shift/F10-CITY/ST/Zip ".
      END.
    END.
    ELSE
    IF ix-runtype = "normal" THEN
    DO:
      IF v-lcity = "" AND v-lstate = "" AND v-lzipcd <> "" AND
        CAN-FIND(FIRST arss USE-INDEX k-zipcd WHERE
                       arss.cono = g-cono AND
                       arss.custno = v-custno AND
                       arss.zipcd  BEGINS v-lzipcd AND
                       arss.shipto BEGINS ix-Shiptolu NO-LOCK) THEN
      DO:
        CREATE b-w-shipto.
        ASSIGN 
          b-w-shipto.custno = ?
          b-w-shipto.shipto = "".
      END.
      ELSE
      IF CAN-FIND(FIRST arss USE-INDEX k-addr WHERE
                        arss.cono = g-cono AND
                        arss.custno = v-custno AND
                        arss.state = v-lstate AND
                        arss.city  BEGINS v-lcity AND
                        arss.shipto BEGINS ix-Shiptolu NO-LOCK) THEN
      DO:
        CREATE b-w-shipto.
        ASSIGN 
          b-w-shipto.custno = ?
          b-w-shipto.shipto = "".
      END.
      
      IF v-lcity = "" AND v-lstate = "" AND v-lzipcd <> "" THEN
      DO:
        OPEN QUERY q-shiptolu FOR EACH b-w-shipto  NO-LOCK,
          EACH arss OUTER-JOIN USE-INDEX k-zipcd 
          WHERE arss.cono = g-cono AND
                arss.custno = v-custno AND
                arss.zipcd BEGINS v-lzipcd AND
                arss.shipto BEGINS ix-Shiptolu NO-LOCK,
          EACH arsc WHERE arsc.cono = 1 AND
               arsc.custno = arss.custno NO-LOCK.
        ASSIGN v-shiptolulit =
        " Shift/F7-Lookupnm  Shift/F8-Phone                  Shift/F10-City/St/ZIP ".
      END.
      ELSE
      DO:
        OPEN QUERY q-shiptolu FOR EACH b-w-shipto  NO-LOCK,
          EACH arss OUTER-JOIN USE-INDEX k-addr 
          WHERE arss.cono = g-cono AND
                arss.custno = v-custno AND
                arss.state = v-lstate AND
                arss.city  BEGINS v-lcity AND
                arss.shipto BEGINS ix-Shiptolu NO-LOCK,
          EACH arsc WHERE arsc.cono = 1 AND
               arsc.custno = arss.custno NO-LOCK.
        ASSIGN v-shiptolulit =
        " Shift/F7-Lookupnm  Shift/F8-Phone                  Shift/F10-CITY/ST/Zip ".
      END.
    END.
  END.  /* if z */
  
  ASSIGN v-shiptobrowseopen = TRUE.
  
  IF ix-callframe = "f-top1" THEN
  ASSIGN 
    v-custbrowserow = 6
    lu-row = 16.
  IF avail arss THEN
    RUN bottom_custva.p (INPUT arsc.custno,
                         INPUT arss.shipto).
  DISPLAY b-shiptolu v-shiptolulit WITH FRAME f-shiptolu.
END.

/* -------------------------------------------------------------------------  */
PROCEDURE MoveshiptoLu:
  /* -------------------------------------------------------------------------  */
  
  DEFINE INPUT PARAMETER ix-lastkey AS INTEGER                 NO-UNDO.
  DEFINE INPUT PARAMETER ix-callframe AS CHARACTER             NO-UNDO.
  
  ENABLE b-shiptolu WITH FRAME f-shiptolu.
  IF ix-callframe = "f-top1" THEN
    DISPLAY v-shiptolulit WITH FRAME f-shiptolu.
  APPLY LASTKEY TO b-shiptolu IN FRAME f-shiptolu.
  IF LASTKEY = 502 AND  v-shiptobrowsekeyed = FALSE THEN DO:
    APPLY x-cursorup TO b-shiptolu IN FRAME f-shiptolu.
  END.
  APPLY "focus" TO b-shiptolu IN FRAME f-shiptolu.
  IF avail arss THEN
    RUN bottom_custva.p (INPUT arsc.custno,
                         INPUT arss.shipto).
  APPLY "focus" TO v-shipto IN FRAME f-top1.
END.

/* --------------------&&&--------------------------------------------------  */
PROCEDURE DoProdLu:
  /* -------------------------------------------------------------------------  */
  DEFINE INPUT PARAMETER ix-Prod    AS CHARACTER FORMAT "x(24)" NO-UNDO.
  DEFINE INPUT PARAMETER ix-runtype AS CHARACTER                NO-UNDO.
  
  HIDE FRAME f-prodlu.
  IF v-prodlutype = "l" THEN
  DO:
    FOR EACH b-w-prod:
      DELETE b-w-prod.
    END.
    IF CAN-FIND(FIRST icsp USE-INDEX k-lkup WHERE
                      icsp.cono = g-cono AND
                      icsp.lookupnm BEGINS ix-prod AND
                      icsp.statustype <> "I" NO-LOCK) THEN
    DO:
      CREATE b-w-prod.
      ASSIGN b-w-prod.prod = ".". /* trick query so they all can be the same */
    END.
    OPEN QUERY q-prodlu FOR EACH b-w-prod  NO-LOCK,
    
    FIRST bq-icsw WHERE bq-icsw.cono = g-cono NO-LOCK,
      
      EACH icsp OUTER-JOIN USE-INDEX k-lkup
      WHERE icsp.cono = g-cono           AND
            icsp.lookupnm BEGINS ix-prod AND
            icsp.statustype <> "I" NO-LOCK.
    ASSIGN v-prodlulit =
  "SHIFT  /F7-LOOKUPNM   /F8-Product   /F9-Keywds   /F10-Vendor    /F3-Catalog".
    DISPLAY v-prodlulit WITH FRAME f-prodlu.
  END. /* if lutype = l  */
  ELSE
  IF v-prodlutype = "p" THEN
  DO:
    FOR EACH b-w-prod:
      DELETE b-w-prod.
    END.
    IF CAN-FIND(FIRST icsp USE-INDEX k-icsp WHERE
                      icsp.cono = g-cono AND
                      icsp.prod BEGINS ix-prod AND
                      icsp.statustype <> "I" NO-LOCK) THEN
    DO:
      CREATE b-w-prod.
      ASSIGN b-w-prod.prod = ".". /* trick query so they all can be the same */
    END.
    OPEN QUERY q-prodlu FOR EACH b-w-prod NO-LOCK,
    FIRST bq-icsw WHERE bq-icsw.cono = g-cono NO-LOCK,
      EACH icsp OUTER-JOIN USE-INDEX k-icsp 
      WHERE icsp.cono = g-cono       AND
            icsp.prod BEGINS ix-prod AND
            icsp.statustype <> "I" NO-LOCK.
    ASSIGN v-prodlulit =
 "SHIFT  /F7-LookUpNm   /F8-PRODUCT   /F9-Keywds   /F10-Vendor    /F3-Catalog".
    DISPLAY v-prodlulit WITH FRAME f-prodlu.
  END.  /* if lutype = p */
  ELSE
  IF v-prodlutype = "k" THEN
  DO:
    IF ix-runtype = "function" THEN
    DO:
      RUN zsdiprodkwd.p.
      OPEN QUERY q-prodlu FOR EACH b-w-prod  NO-LOCK,
        FIRST bq-icsw WHERE bq-icsw.cono = g-cono NO-LOCK,
        EACH icsp WHERE
             icsp.cono     = g-cono        AND
             icsp.lookupnm = b-w-prod.prod AND
             icsp.statustype <> "I" NO-LOCK.
      /*       icsp.prod = b-w-prod.prod no-lock. */
    END.
    ELSE
    IF ix-runtype = "normal" THEN
    DO:
      OPEN QUERY q-prodlu 
        FOR EACH b-w-prod WHERE
          b-w-prod.prod BEGINS ix-prod NO-LOCK,
          FIRST bq-icsw WHERE bq-icsw.cono = g-cono NO-LOCK,
          EACH icsp WHERE 
               icsp.cono     = g-cono        AND
               icsp.lookupnm = b-w-prod.prod AND
               icsp.statustype <> "I" NO-LOCK.
      /*       icsp.prod = b-w-prod.prod no-lock. */
    END.
    ASSIGN v-prodlulit =
 "SHIFT  /F7-LookUpNm   /F8-Product   /F9-KEYWDS   /F10-Vendor    /F3-Catalog".
    DISPLAY v-prodlulit WITH FRAME f-prodlu.
  END. /* if function */
  ELSE
  IF v-prodlutype = "v" THEN
  DO:
    IF ix-runtype = "function" THEN
    DO:
      HIDE FRAME f-lines.
      vendloop:
      DO WHILE TRUE ON ENDKEY UNDO vendloop, LEAVE vendloop:
        UPDATE v2-whse
        v2-vendno
        GO-ON (f17,f18,f19,f20,f6,f7,f8,f9 )
        WITH FRAME f-icvend.
        HIDE FRAME f-icvend.
        IF {k-accept.i} THEN DO:
          IF v2-whse NE "" AND
            NOT CAN-FIND(icsd WHERE icsd.cono = g-cono AND
            icsd.whse = v2-whse NO-LOCK) THEN DO:
            ASSIGN v-error = "Warehouse Not Set Up in Warehouse Setup - ICSD".
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT.
          END.
          IF v2-vendno > 0 AND
            NOT CAN-FIND(apsv WHERE apsv.cono = g-cono AND
            apsv.vendno = v2-vendno NO-LOCK) THEN DO:
            ASSIGN v-error =
              "Vendor # Not Set Up in Vendor Setup - APSV (4301)".
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT.
          END.
        END.
        IF {k-accept.i} OR {k-cancel.i} THEN
        DO:
          LEAVE vendloop.
        END.
        IF {k-cancel.i} OR {k-jump.i} THEN
        DO:
          RETURN.
        END.
        IF KEYFUNCTION(LASTKEY) = "f17" OR
           KEYFUNCTION(LASTKEY) = "f18" OR
           KEYFUNCTION(LASTKEY) = "f19" OR
           KEYFUNCTION(LASTKEY) = "f20" OR
           KEYFUNCTION(LASTKEY) = "f6" OR
           KEYFUNCTION(LASTKEY) = "f7" OR
           KEYFUNCTION(LASTKEY) = "f8" OR
           KEYFUNCTION(LASTKEY) = "f9" THEN
        DO:
          HIDE FRAME f-icvend.
          RETURN.
        END.
      END. /** do while  ***/
      
      HIDE FRAME f-icvend.
      
      FOR EACH b-w-prod:
        DELETE b-w-prod.
      END.
      
      IF CAN-FIND(FIRST bq-icsw USE-INDEX k-vendor WHERE
                        bq-icsw.cono = g-cono         AND
                        bq-icsw.whse      = v2-whse    AND
                        bq-icsw.arpvendno = v2-vendno NO-LOCK) THEN
      DO:
        CREATE b-w-prod.
        ASSIGN b-w-prod.prod = ".". /*trick query so they all can be the same */
      END.
      OPEN QUERY q-prodlu 
        FOR EACH b-w-prod WHERE
                 b-w-prod.prod = "." NO-LOCK,
        EACH bq-icsw OUTER-JOIN USE-INDEX k-vendor 
        WHERE bq-icsw.cono      = g-cono AND
              bq-icsw.whse      = v2-whse AND
              bq-icsw.arpvendno = v2-vendno NO-LOCK,
        EACH icsp WHERE
             icsp.cono = g-cono AND
             icsp.statustype <> "I" AND
              icsp.lookupnm = bq-icsw.prod NO-LOCK.
      /*       icsp.prod = bq-icsw.prod no-lock.  */
      ASSIGN v-prodlulit =
  "SHIFT  /F7-LookUpNm   /F8-Product   /F9-Keywds   /F10-VENDOR    /F3-Catalog".
      DISPLAY v-prodlulit WITH FRAME f-prodlu.
      IF NOT {k-cancel.i} THEN DO:
        ASSIGN v-error =
          " Retrieving " + STRING(v2-vendno,"zzzzzz999999")
          + " records - please wait..".
        RUN zsdi_vaerrx.p(v-error,"yes").
        PAUSE 1.
        ASSIGN funcflag = TRUE.
      END.
    END. /* if function  */
    ELSE
    IF ix-runtype = "normal" THEN
    DO:
      IF CAN-FIND(FIRST bq-icsw USE-INDEX k-whse  WHERE
                        bq-icsw.cono = g-cono         AND
                        bq-icsw.whse = v2-whse         AND
                        bq-icsw.prod BEGINS ix-prod   AND
                        bq-icsw.arpvendno = v2-vendno NO-LOCK) THEN
      DO:
        CREATE b-w-prod.
        ASSIGN b-w-prod.prod = ".". /*trick query so they all can be the same */
      END.
      OPEN QUERY q-prodlu 
        FOR EACH b-w-prod NO-LOCK,
        EACH bq-icsw OUTER-JOIN USE-INDEX k-whse 
        WHERE bq-icsw.cono = g-cono AND
              bq-icsw.whse = v2-whse AND
              bq-icsw.prod BEGINS ix-prod AND
              bq-icsw.arpvendno = v2-vendno NO-LOCK,
        EACH icsp WHERE
             icsp.cono = g-cono AND
             icsp.statustype <> "I" AND
             icsp.prod = bq-icsw.prod AND
             icsp.lookupnm BEGINS ix-prod NO-LOCK.
      /**         icsp.prod begins ix-prod no-lock. */
      ASSIGN v-prodlulit =
 "SHIFT  /F7-LookUpNm   /F8-Product   /F9-Keywds   /F10-VENDOR    /F3-Catalog".
      DISPLAY v-prodlulit WITH FRAME f-prodlu.
    END. /* else if normal */
    
    ASSIGN v-prodlulit =
 "SHIFT  /F7-LOOKUPNM   /F8-Product   /F9-Keywds   /F10-VENDOR    /F3-Catalog".
    DISPLAY v-prodlulit WITH FRAME f-prodlu.
    
  END. /* if lutype = v then */
  
  ASSIGN v-prodbrowseopen = TRUE.
  
  DISPLAY b-prodlu v-prodlulit WITH FRAME f-prodlu.
  
  RUN bottom_prodva (INPUT x-whse,
  INPUT ix-prod).
  
  APPLY " focus" TO v-prod IN FRAME f-mid1.
  HIDE FRAME f-icvend.
END.

/* -------------------------------------------------------------------------  */
PROCEDURE MoveProdLu:
  /* -------------------------------------------------------------------------  */
  
  DEFINE INPUT PARAMETER ix-lastkey AS INTEGER                 NO-UNDO.
  
  DISPLAY v-prodlulit WITH FRAME f-prodlu.
  ENABLE b-prodlu WITH FRAME f-prodlu.
  APPLY LASTKEY TO b-prodlu IN FRAME f-prodlu.
  
  IF LASTKEY = 502 AND  v-prodbrowsekeyed = FALSE THEN DO:
    APPLY x-cursorup TO b-prodlu IN FRAME f-prodlu.
  END.
  
  IF avail icsp THEN DO:
    RUN bottom_prodva (INPUT x-whse,
                       INPUT icsp.prod).
  END.
  
  APPLY "focus" TO v-prod IN FRAME f-mid1.
  
END.


/* --------------------&&&--------------------------------------------------  */
PROCEDURE DoProdLus:
  /* -------------------------------------------------------------------------  */
  DEFINE INPUT PARAMETER ix-Prod    AS CHARACTER FORMAT "x(24)" NO-UNDO.
  DEFINE INPUT PARAMETER ix-runtype AS CHARACTER                NO-UNDO.
  
  HIDE FRAME f-prodlu.
  IF v-prodlutype = "l" THEN
  DO:
    FOR EACH b-w-prod:
      DELETE b-w-prod.
    END.
    IF CAN-FIND(FIRST icsp USE-INDEX k-lkup WHERE
                      icsp.cono = g-cono AND
                      icsp.lookupnm BEGINS ix-prod AND
                      icsp.statustype <> "I" NO-LOCK) THEN
    DO:
      CREATE b-w-prod.
      ASSIGN b-w-prod.prod = ".". /* trick query so they all can be the same */
    END.
    OPEN QUERY q-prodlu FOR EACH b-w-prod  NO-LOCK,
    
    FIRST bq-icsw WHERE bq-icsw.cono = g-cono NO-LOCK,
    
      EACH icsp OUTER-JOIN USE-INDEX k-lkup WHERE
           icsp.cono = g-cono           AND
           icsp.lookupnm BEGINS ix-prod AND
           icsp.statustype <> "I" NO-LOCK.
    ASSIGN v-prodlulit =
 "SHIFT  /F7-LOOKUPNM   /F8-Product   /F9-Keywds   /F10-Vendor    /F3-Catalog".
    DISPLAY v-prodlulit WITH FRAME f-prodlu.
  END. /* if lutype = l  */
  ELSE
  IF v-prodlutype = "p" THEN
  DO:
    FOR EACH b-w-prod:
      DELETE b-w-prod.
    END.
    IF CAN-FIND(FIRST icsp USE-INDEX k-icsp WHERE
                      icsp.cono = g-cono AND
                      icsp.prod BEGINS ix-prod AND
                      icsp.statustype <> "I" NO-LOCK) THEN
    DO:
      CREATE b-w-prod.
      ASSIGN b-w-prod.prod = ".". /* trick query so they all can be the same */
    END.
    OPEN QUERY q-prodlu FOR EACH b-w-prod NO-LOCK,
      FIRST bq-icsw WHERE bq-icsw.cono = g-cono NO-LOCK,
      EACH icsp OUTER-JOIN USE-INDEX k-icsp 
      WHERE icsp.cono = g-cono       AND
            icsp.prod BEGINS ix-prod AND
            icsp.statustype <> "I" NO-LOCK.
    ASSIGN v-prodlulit =
 "SHIFT  /F7-LOOKUPNM   /F8-PRODUCT   /F9-Keywds   /F10-Vendor    /F3-Catalog".
    DISPLAY v-prodlulit WITH FRAME f-prodlu.
  END.  /* if lutype = p */
  ELSE
  IF v-prodlutype = "k" THEN
  DO:
    IF ix-runtype = "function" THEN
    DO:
      RUN zsdiprodkwd.p.
      OPEN QUERY q-prodlu FOR EACH b-w-prod  NO-LOCK,
        FIRST bq-icsw WHERE bq-icsw.cono = g-cono NO-LOCK,
        EACH icsp WHERE
             icsp.cono     = g-cono        AND
             icsp.lookupnm = b-w-prod.prod AND
             icsp.statustype <> "I" NO-LOCK.
      /*       icsp.prod = b-w-prod.prod no-lock. */
    END.
    ELSE
    IF ix-runtype = "normal" THEN
    DO:
      OPEN QUERY q-prodlu FOR EACH b-w-prod 
        WHERE b-w-prod.prod BEGINS ix-prod NO-LOCK,
        FIRST bq-icsw 
          WHERE bq-icsw.cono = g-cono NO-LOCK,
        EACH icsp WHERE
             icsp.cono     = g-cono        AND
             icsp.lookupnm = b-w-prod.prod AND
             icsp.statustype <> "I" NO-LOCK.
      /*       icsp.prod = b-w-prod.prod no-lock. */
    END.
    ASSIGN v-prodlulit =
 "SHIFT  /F7-LOOKUPNM   /F8-Product   /F9-KEYWDS   /F10-Vendor    /F3-Catalog".
    DISPLAY v-prodlulit WITH FRAME f-prodlu.
  END. /* if function */
  ELSE
  IF v-prodlutype = "v" THEN
  DO:
    IF ix-runtype = "function" THEN
    DO:
      HIDE FRAME f-lines.
      vendloop:
      DO WHILE TRUE ON ENDKEY UNDO vendloop, LEAVE vendloop:
        UPDATE v2-whse
          v2-vendno
        GO-ON (f17,f18,f19,f20,f6,f7,f8,f9 )
        WITH FRAME f-icvend.
        HIDE FRAME f-icvend.
        IF {k-accept.i} THEN DO:
          IF v2-whse NE "" AND
              NOT CAN-FIND(icsd WHERE icsd.cono = g-cono AND
              icsd.whse = v2-whse NO-LOCK) THEN DO:
            ASSIGN v-error = "Warehouse Not Set Up in Warehouse Setup - ICSD".
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT.
          END.
          IF v2-vendno > 0 AND
              NOT CAN-FIND(apsv WHERE apsv.cono = g-cono AND
              apsv.vendno = v2-vendno NO-LOCK) THEN DO:
            ASSIGN v-error =
              "Vendor # Not Set Up in Vendor Setup - APSV (4301)".
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT.
          END.
        END.
        IF {k-accept.i} OR {k-cancel.i} THEN
        DO:
          LEAVE vendloop.
        END.
        IF {k-cancel.i} OR {k-jump.i} THEN
        DO:
          RETURN.
        END.
        IF KEYFUNCTION(LASTKEY) = "f17" OR
           KEYFUNCTION(LASTKEY) = "f18" OR
           KEYFUNCTION(LASTKEY) = "f19" OR
           KEYFUNCTION(LASTKEY) = "f20" OR
           KEYFUNCTION(LASTKEY) = "f6" OR
           KEYFUNCTION(LASTKEY) = "f7" OR
           KEYFUNCTION(LASTKEY) = "f8" OR
           KEYFUNCTION(LASTKEY) = "f9" THEN
        DO:
          HIDE FRAME f-icvend.
          RETURN.
        END.
      END. /** do while  ***/
      
      HIDE FRAME f-icvend.
      
      FOR EACH b-w-prod:
        DELETE b-w-prod.
      END.
      
      IF CAN-FIND(FIRST bq-icsw USE-INDEX k-vendor WHERE
                        bq-icsw.cono = g-cono         AND
                        bq-icsw.whse      = v2-whse    AND
                        bq-icsw.arpvendno = v2-vendno NO-LOCK) THEN
      DO:
        CREATE b-w-prod.
        ASSIGN b-w-prod.prod = ".". /*trick query so they all can be the same */
      END.
      OPEN QUERY q-prodlu FOR EACH b-w-prod 
        WHERE b-w-prod.prod = "." NO-LOCK,
        EACH bq-icsw OUTER-JOIN USE-INDEX k-vendor 
        WHERE bq-icsw.cono      = g-cono AND
              bq-icsw.whse      = v2-whse AND
              bq-icsw.arpvendno = v2-vendno NO-LOCK,
        EACH icsp WHERE
             icsp.cono = g-cono AND
             icsp.statustype <> "I" AND
             icsp.lookupnm = bq-icsw.prod NO-LOCK.
      /*        icsp.prod = bq-icsw.prod no-lock.  */
      ASSIGN v-prodlulit =
 "SHIFT  /F7-LOOKUPNM   /F8-Product   /F9-Keywds   /F10-VENDOR    /F3-Catalog".
      DISPLAY v-prodlulit WITH FRAME f-prodlu.
      IF NOT {k-cancel.i} THEN DO:
        ASSIGN v-error =
          " Retrieving " + STRING(v2-vendno,"zzzzzz999999")
          + " records - please wait..".
        RUN zsdi_vaerrx.p(v-error,"yes").
        PAUSE 1.
        ASSIGN funcflag = TRUE.
      END.
    END. /* if function  */
    ELSE
    IF ix-runtype = "normal" THEN
    DO:
      IF CAN-FIND(FIRST bq-icsw USE-INDEX k-whse  WHERE
                        bq-icsw.cono = g-cono         AND
                        bq-icsw.whse = v2-whse         AND
                        bq-icsw.prod BEGINS ix-prod   AND
                        bq-icsw.arpvendno = v2-vendno NO-LOCK) THEN
      DO:
        CREATE b-w-prod.
        ASSIGN b-w-prod.prod = ".". /*trick query so they all can be the same */
      END.
      OPEN QUERY q-prodlu FOR EACH b-w-prod NO-LOCK,
        EACH bq-icsw OUTER-JOIN USE-INDEX k-whse 
        WHERE bq-icsw.cono = g-cono AND
              bq-icsw.whse = v2-whse AND
              bq-icsw.prod BEGINS ix-prod AND
              bq-icsw.arpvendno = v2-vendno NO-LOCK,
        EACH icsp WHERE
             icsp.cono = g-cono AND
             icsp.statustype <> "I" AND
             icsp.prod = bq-icsw.prod AND
             icsp.lookupnm BEGINS ix-prod NO-LOCK.
      /**         icsp.prod begins ix-prod no-lock. */
      ASSIGN v-prodlulit =
 "SHIFT  /F7-LOOKUPNM   /F8-Product   /F9-Keywds   /F10-VENDOR    /F3-Catalog".
      DISPLAY v-prodlulit WITH FRAME f-prodlu.
    END. /* else if normal */
    
    ASSIGN v-prodlulit =
 "SHIFT  /F7-LOOKUPNM   /F8-Product   /F9-Keywds   /F10-VENDOR    /F3-Catalog".
    DISPLAY v-prodlulit WITH FRAME f-prodlu.
    
  END. /* if lutype = v then */
  
  ASSIGN v-prodbrowseopent = TRUE.
  
  DISPLAY b-prodlu v-prodlulit WITH FRAME f-prodlu.
  
  RUN bottom_prodva (INPUT x-whse,
                     INPUT ix-prod).
  
  APPLY " focus" TO s-prod IN FRAME f-top1.
  HIDE FRAME f-icvend.
END.


/* -------------------------------------------------------------------------  */
PROCEDURE MoveProdLus:
  /* -------------------------------------------------------------------------  */
  
  DEFINE INPUT PARAMETER ix-lastkey AS INTEGER                 NO-UNDO.
  
  DISPLAY v-prodlulit WITH FRAME f-prodlu.
  ENABLE b-prodlu WITH FRAME f-prodlu.
  APPLY LASTKEY TO b-prodlu IN FRAME f-prodlu.
  
  
  IF LASTKEY = 502 AND  v-prodbrowsekeyedt = FALSE THEN
  DO:
    APPLY x-cursorup TO b-prodlu IN FRAME f-prodlu.
  END.
  IF avail icsp THEN
  DO:
    RUN bottom_prodva (INPUT x-whse,
    INPUT icsp.prod).
  END.
  APPLY "focus" TO s-prod IN FRAME f-top1.
  
END.

/* -------------------------------------------------------------------------  */
PROCEDURE DoLineLu:
  /* -------------------------------------------------------------------------  */
  
  OPEN QUERY q-lines FOR EACH t-lines use-index ix1 .
  ASSIGN v-linebrowseopen = TRUE.
  DISPLAY b-lines WITH FRAME f-lines.
  APPLY "focus" TO v-lineno IN FRAME f-mid1.
END.

/* -------------------------------------------------------------------------  */
PROCEDURE MoveLineLu:
  /* -------------------------------------------------------------------------  */
  
  DEFINE INPUT PARAMETER ix-lastkey AS INTEGER                 NO-UNDO.
  ENABLE b-lines WITH FRAME f-lines.
  b-lines:REFRESHABLE = FALSE.
  APPLY LASTKEY TO b-lines IN FRAME f-lines.
  
  IF LASTKEY = 502 AND v-linebrowsekeyed = FALSE THEN
  DO:
    APPLY x-cursorup TO b-lines IN FRAME f-lines.
  END.
  b-lines:REFRESHABLE = TRUE.
  b-lines:REFRESH().
  
  APPLY "focus" TO v-lineno IN FRAME f-mid1.
END.


/* -------------------------------------------------------------------------  */
PROCEDURE MoveMfgLu:
/* -------------------------------------------------------------------------  */
  DEFINE INPUT PARAMETER ix-lastkey AS INTEGER                 NO-UNDO.
  
  ON cursor-up cursor-up.
  ON cursor-down cursor-down.
  ENABLE b-models WITH FRAME f-models.
  APPLY LASTKEY TO b-models IN FRAME f-models.
  IF LASTKEY = 502 AND v-mfgbrowsekeyed = FALSE THEN DO:
    APPLY x-cursorup TO b-models IN FRAME f-models.
  END.
  APPLY "focus" TO v-model IN FRAME f-top1.
  
END.

/* --------------------&&&--------------------------------------------------  */
PROCEDURE DoMfgLu:
/* -------------------------------------------------------------------------  */
  DEFINE INPUT PARAMETER ix-model   AS CHARACTER FORMAT "x(24)" NO-UNDO.
  DEFINE INPUT PARAMETER ix-runtype AS CHARACTER                NO-UNDO.

  ASSIGN v-modelsbuildfl = TRUE.
  
  EMPTY TEMP-TABLE models.
  
  OPEN QUERY q-models
  FOR EACH zsdirefty WHERE zsdirefty.cono = g-cono AND
           zsdirefty.rectype = "va" and
           zsdirefty.reference <> "Fabrication Template" and
           zsdirefty.reference <> "" NO-LOCK.
    ASSIGN  
      v-mfgbrowseopen = TRUE
      v-modelsbuildfl = FALSE.
    DISPLAY b-models WITH FRAME f-models. 
    APPLY "focus" TO v-model IN FRAME f-top1.
    
  END.
  
  
/* -------------------------------------------------------------------------  */
  PROCEDURE ProdPricing:
/* -------------------------------------------------------------------------  */
    
    DEF INPUT param ix-displaylinefl AS LOGICAL                         NO-UNDO.
    DEF INPUT param ix-PDSCfl        AS LOGICAL                         NO-UNDO.
    DEF INPUT param ix-PDSCrec       LIKE pdsc.pdrecno                  NO-UNDO.
    DEF INPUT param ix-whse          LIKE icsd.whse                     NO-UNDO.
    DEF INPUT param ix-qty           LIKE oeel.qtyord                   NO-UNDO.
    
    DEF INPUT param ix-custno        LIKE oeel.custno                   NO-UNDO.
    DEF INPUT param ix-prod          LIKE oeel.shipprod                 NO-UNDO.
    DEF INPUT-OUTPUT param ix-price   AS DEC                            NO-UNDO.
    DEF INPUT-OUTPUT param ix-gpmarg  AS DEC                            NO-UNDO.
    DEF INPUT-OUTPUT param ix-level   AS INT                            NO-UNDO.
    DEF INPUT-OUTPUT param ix-rebate  AS DEC                            NO-UNDO.
    DEF INPUT-OUTPUT param ix-cost    LIKE oeel.prodcost                NO-UNDO.
    DEF INPUT-OUTPUT param ix-listprice AS DEC                          NO-UNDO.
    DEF INPUT-OUTPUT param ix-disc    AS DEC                            NO-UNDO.
    DEF INPUT-OUTPUT param ix-pdscrecno LIKE oeel.pdrecno               NO-UNDO.
    DEF INPUT-OUTPUT param ix-qtybrkty  AS CHAR FORMAT "x(1)"           NO-UNDO.
    
    DEF BUFFER ix-pdsc FOR pdsc.
    DEF VAR hld-whse LIKE  icsw.whse NO-UNDO.
    DEF VAR ix-totnet      LIKE vaesl.netamt                            NO-UNDO.
    DEF VAR ix-totcost     LIKE vaesl.netamt                            NO-UNDO.
    DEF VAR ix-totmargin   LIKE vaesl.netamt                            NO-UNDO.
    DEF VAR ix-marpct      LIKE vaesl.netamt                            NO-UNDO.
    DEF VAR ix-icspecrecno LIKE oeel.pdrecno                            NO-UNDO.
    ASSIGN 
      ix-price       = 0
      ix-gpmarg      = 0
      ix-level       = 0
      ix-rebate      = 0
      ix-cost        = 0
      ix-listprice   = 0
      ix-disc        = 0
      ix-pdscrecno   = 0
      ix-icspecrecno = 0
      ix-qtybrkty    = ""
      hld-whse       = x-whse.
    
    {w-icsw.i v-prod x-whse NO-LOCK}
    
    IF NOT avail icsw THEN DO:
      ASSIGN copy-okfl = NO.
      /** copy from main whse - if not at default whse  **/
      
      FIND FIRST icsw WHERE
                 icsw.cono = 90     AND
                 icsw.prod = v-prod AND
                 icsw.whse = "main" NO-LOCK NO-ERROR.
      IF NOT avail icsw THEN DO:
        /** copy from a whse - if not at default whse  **/
        RUN Create_ICSW(INPUT x-whse,INPUT v-prod,INPUT-OUTPUT copy-okfl).
      END.
      ELSE DO:
        RUN zsdimaincpy.p(INPUT g-cono,
                          INPUT " ",
                          /*** to whse = x-whse ***/
                          INPUT x-whse,
                          INPUT v-prod,
                          INPUT v-lineno,
                          INPUT-OUTPUT copy-okfl).
      END.
      {w-icsw.i v-prod x-whse NO-LOCK}
      
      IF copy-okfl THEN DO:
        ASSIGN 
          whse-okfl = YES
          v-whse = IF avail t-lines AND
                    (t-lines.whse NE x-whse) AND
                    (t-lines.prod = v-prod)  THEN
                      t-lines.whse
                   ELSE
                   IF ctrlw-whse NE "" AND
                    ctrlw-whse NE x-whse THEN
                     ctrlw-whse
                   ELSE
                   IF v-altwhse NE ""  AND
                     v-altwhse NE x-whse THEN
                     v-altwhse
                   ELSE
                     x-whse
          v-astr1   = IF (ctrlw-whse NE "" AND
                        ctrlw-whse NE x-whse) OR
                        (v-altwhse NE "" AND
                        v-altwhse NE x-whse)  THEN
                        "*"
                      ELSE
                        "".
      END.
    END.
    ELSE
    ASSIGN 
      whse-okfl = YES
      v-whse = IF avail t-lines AND
                (t-lines.whse NE x-whse) AND
                (t-lines.prod = v-prod)  THEN
                 t-lines.whse
               ELSE
               IF ctrlw-whse NE "" AND
                ctrlw-whse NE x-whse THEN
                 ctrlw-whse
               ELSE
               IF v-altwhse NE "" AND
                v-altwhse NE x-whse THEN
                 v-altwhse
               ELSE
                 x-whse
      v-astr1   = IF (ctrlw-whse NE "" AND
                    ctrlw-whse NE x-whse) OR
                    (v-altwhse NE "" AND
                    v-altwhse NE x-whse)  THEN
                    "*"
                  ELSE
                    "".
    
    RUN zsdiquprice.p ( INPUT ix-PDSCfl,
                        INPUT ix-PDSCrec,
                        INPUT x-whse,             /* TAH WHSE ix-whse, */
                        INPUT ix-qty,
                        INPUT ix-custno,
                        INPUT ix-prod,
                        INPUT-OUTPUT ix-price,
                        INPUT-OUTPUT ix-gpmarg,
                        INPUT-OUTPUT ix-level,
                        INPUT-OUTPUT ix-rebate,
                        INPUT-OUTPUT ix-cost,
                        INPUT-OUTPUT ix-listprice,
                        INPUT-OUTPUT ix-disc,
                        INPUT-OUTPUT ix-pdscrecno,
                        INPUT-OUTPUT ix-totnet,
                        INPUT-OUTPUT ix-totcost,
                        INPUT-OUTPUT ix-totmargin,
                        INPUT-OUTPUT ix-marpct,
                        INPUT-OUTPUT ix-icspecrecno).
    
    IF ix-pdscrecno <> ?  AND ix-pdscrecno <> 0 THEN
    FIND ix-pdsc WHERE ix-pdsc.cono = g-cono AND
         ix-pdsc.pdrecno = ix-pdscrecno NO-LOCK NO-ERROR.
    
    ASSIGN 
      v-sellprc   = ix-price
      v-gp        = (ix-gpmarg / ROUND(ix-price,2)) * 100.
/* tahtrain
    {w-icsw.i v-prod v-whse NO-LOCK "vl-"}  /* TAH WHSE */
   tah train replaced below */
   
   {w-icsw.i v-prod x-whse NO-LOCK "vl-"}
   
    IF avail vl-icsw THEN DO:
      FIND FIRST apsv WHERE
                 apsv.cono   = g-cono AND
                 apsv.vendno = vl-icsw.arpvendno
      NO-LOCK NO-ERROR.
      IF (avail t-lines AND t-lines.altwhse NE x-whse)  THEN DO:
        DEFINE BUFFER lt-icsw FOR icsw.
/* tahtrain 
        {w-icsw.i v-prod t-lines.altwhse NO-LOCK "lt-"}
   tahtrain  below instead */
        {w-icsw.i v-prod x-whse NO-LOCK "lt-"}
        IF avail lt-icsw THEN 
            ASSIGN v-leadtm =  STRING(INT(lt-icsw.leadtmavg),"9999").
      END.
      ELSE
      IF avail vl-icsw THEN DO:
        ASSIGN v-leadtm = STRING(INT(vl-icsw.leadtmavg),"9999").
      END.
    
      ASSIGN 
        v-vendno  = IF avail icsw THEN
                      icsw.arpvendno
                    ELSE
                      0
        v-vendnm  = IF avail apsv THEN
                      apsv.lookupnm
                    ELSE
                      " "
        v-prctype = icsw.pricetype
        ix-cost   = IF icsw.avgcost > 0 THEN
                      icsw.avgcost
                    ELSE
                      0.
      IF icsw.statustype = "O" THEN
        ASSIGN v-specnstype = "s".
      ELSE
        ASSIGN v-specnstype = " ".
      DISPLAY 
        v-specnstype 
        v-qty
        v-vendno 
        v-vendnm
        v-leadtm 
      WITH FRAME f-mid1.
    END.
    ELSE
      ASSIGN 
        v-vendno = 0
        v-vendnm = "".
    IF ix-displaylinefl THEN
      RUN DisplayMid1.
  END.
  
/* -------------------------------------------------------------------------  */
  PROCEDURE NonStock_Popup:
/* -------------------------------------------------------------------------  */
    DEF BUFFER n-t-lines FOR t-lines.
    DEF BUFFER n-icsp FOR icsp.
    DEF VAR    v-errorfl     as logical        init false no-undo.
    
    
    
    HIDE FRAME f-whses.
    HIDE FRAME f-prodlu.
        
    FIND t-lines WHERE t-lines.lineno = v-lineno EXCLUSIVE-LOCK NO-ERROR.
    
    IF v-prod = "" THEN DO:
      ON cursor-up cursor-up.
      ON cursor-down cursor-down.
      LEAVE.
    END.
    
    ON cursor-up back-tab.
    ON cursor-down tab.
    
    ASSIGN 
      v-nslistprc =  IF avail t-lines AND
                    t-lines.prod = v-prod THEN
                     t-lines.listprc
                   ELSE
                     v-nslistprc
      v-descrip =  IF avail t-lines AND
                    t-lines.prod = v-prod THEN
                     t-lines.descrip
                   ELSE
                     v-descrip
      v-prodcat  = IF avail t-lines AND
                    t-lines.prod = v-prod THEN
                     t-lines.prodcat
                   ELSE
                     v-prodcat
      v-prodline = IF avail t-lines AND
                    t-lines.prod = v-prod THEN
                      v-prodline
                    ELSE
                      v-prodline
      
      v-whse = IF avail t-lines AND t-lines.altwhse NE "" THEN
                 t-lines.altwhse
               ELSE
               IF avail t-lines AND t-lines.whse NE ""  THEN
                 t-lines.whse
               ELSE
                 x-whse.

      find first rarr-com use-index k-com where 
             rarr-com.cono     = g-cono      and
             rarr-com.comtype  = x-quoteno and
             rarr-com.orderno  = 0           and
             rarr-com.ordersuf = 0           and
             rarr-com.lineno   = v-lineno 
             no-lock no-error.
      if avail rarr-com then do:
        ASSIGN  v-nvendorname   = Rarr-com.noteln[1]
                v-ncontactname  = Rarr-com.noteln[2]
                v-nquoteno      = Rarr-com.noteln[3]
                v-nphoneno      = Rarr-com.noteln[4].
      end.
      else do:
        ASSIGN  v-nvendorname   = " "
                v-ncontactname  = " "
                v-nquoteno      = " "
                v-nphoneno      = " ".

      end.
 
/* Catalog */  
      ASSIGN v-errorcat = "Default".
                   
      FIND icsc WHERE icsc.catalog = v-prod NO-LOCK NO-ERROR.
      
      IF AVAIL icsc and catalogfl and
        (v-lmode = "a" and not avail t-lines or
          (avail t-lines and t-lines.prod = "" )) THEN DO:
        assign
          s-ZsdiXrefType = "GN"
          v-errorcat = ""
          v-nslistprc =  IF icsc.listprice <> 0 THEN
                           icsc.listprice
                         ELSE
                         IF avail t-lines AND
                         t-lines.prod = v-prod THEN
                           t-lines.listprc
                         ELSE
                           icsc.listprice
        
          v-descrip =  if icsc.descrip[1] <> "" then
                          icsc.descrip[1]
                        else  
                        if avail t-lines and 
                          t-lines.prod = v-prod then 
                        t-lines.descrip
                      else 
                        icsc.descrip[1]    
      
          v-vendno =  if icsc.vendno <> 0 then
                         icsc.vendno
                       else  
                       if avail t-lines and 
                       t-lines.prod = v-prod then
                        t-lines.vendno
                      else if v-vendno = 0 then
                        icsc.vendno
                      else
                        0           
          v-prodcost  =  if icsc.prodcost <> 0 then 
                          icsc.prodcost
                        else  
                        if avail t-lines and
                         t-lines.prod = v-prod then
                          t-lines.prodcost
                        else if v-prodcost = 0 then
                          icsc.prodcost
                        else
                          0
                     
          v-prodcat  =  if icsc.prodcat <> "" then 
                          icsc.prodcat
                        else  
                        if avail t-lines and 
                          t-lines.prod = v-prod then 
                         t-lines.prodcat
                       else if v-prodcat = "" then 
                         icsc.prodcat    
                       else
                         v-prodcat
          v-prodline =  if icsc.prodline <> "" then
                          icsc.prodline
                        else
                        if avail t-lines and 
                        t-lines.prod = v-prod then 
                         v-prodline 
                       else if v-prodline = "" then
                         icsc.prodline
                       else  
                         ""    
          v-leadtm   =  if icsc.user6 <> 0 then
                          string(icsc.user6)
                        else  
                        if avail t-lines then 
                         t-lines.leadtm 
                       else if v-leadtm = "" then
                         string(icsc.user6)
                       else
                         "".
        if v-vendno = 0 then do:  
          assign v-errorcat = " Vendor Number is required.".
        end.
        else do:  
          {w-apsv.i v-vendno no-lock}
          if avail apsv then do: 
            if apsv.statustype = no then do: 
              assign v-errorcat = "Vendor is Inactive".
            end.
          end.
          else if v-vendno ne 0 then do:
            assign v-errorcat = "Vendor # Not Set Up in Vendor Setup - APSVR".
          end.                                                                  
        end. 
 
        if v-prodcat = "" then do: 
          assign v-errorcat = " Product Category is required.".
        end.
        else do:                             
          {w-sasta.i C v-prodcat no-lock}
          if not avail sasta then do:  
            assign v-errorcat =
                 "Product Category Not Set Up in System Table - SASTT". 
          end. 
          if v-whse = "" then do:
            assign v-errorcat = " Warehouse is required.".
          end.  
          else do:
             {w-icsd.i v-whse no-lock} 
             if not avail icsd then do: 
              assign v-errorcat =  " Invalid Warehouse.".
             end. 
          end.  
        end.
        if v-prodline ne "" then do: 
          {w-icsl.i v-whse v-vendno v-prodline no-lock}
           if not avail icsl then do:
             assign v-errorcat = " ProdLine notfound - by vendor/by whse.".
           end. 
        end.
        else do:
          assign v-errorcat = " ProdLine is required.".
        end. 
        if v-prodcost = 0 then do: 
          assign v-errorcat = "Product Cost is required.".
        end.
        if v-descrip = " " then do: 
          assign v-errorcat = "(xxxx) Description is required.".
        end.

        
        if v-errorcat = "" then do:
          if avail t-lines then do:
            assign t-lines.leadtm   = left-trim(v-leadtm,"0")
                   t-lines.leadtm   = left-trim(v-leadtm," ")
                   t-lines.icspdesc = v-descrip2.
            FIND FIRST vasp WHERE
                       vasp.cono = g-cono         AND
                       vasp.shipprod = x-quoteno  
            EXCLUSIVE-LOCK NO-ERROR.

            FIND zsdivasp WHERE
                 zsdivasp.cono      = g-cono         AND
                 zsdivasp.rectype   = "fb"        AND 
                 zsdivasp.repairno  = x-quoteno  
            EXCLUSIVE-LOCK NO-ERROR.
       
            find first rarr-com use-index k-com where 
                       rarr-com.cono     = g-cono      and
                       rarr-com.comtype  = x-quoteno and
                       rarr-com.orderno  = 0           and
                       rarr-com.ordersuf = 0           and
                       rarr-com.lineno   = v-lineno 
                       no-error.
            if avail rarr-com then do:
              ASSIGN  Rarr-com.noteln[1] = v-nvendorname 
                      Rarr-com.noteln[2] = v-ncontactname 
                      Rarr-com.noteln[3] = v-nquoteno 
                      Rarr-com.noteln[4] = v-nphoneno
                      Rarr-com.printfl   = v-errorfl. 
            end.   
            else do:
              create rarr-com.
              assign rarr-com.cono     = g-cono      
                     rarr-com.comtype  = x-quoteno 
                     rarr-com.orderno  = 0         
                     rarr-com.ordersuf = 0         
                     rarr-com.lineno   = v-lineno. 
              ASSIGN  Rarr-com.noteln[1] = v-nvendorname 
                      Rarr-com.noteln[2] = v-ncontactname 
                      Rarr-com.noteln[3] =  v-nquoteno 
                      Rarr-com.noteln[4] = v-nphoneno
                      Rarr-com.printfl   = v-errorfl. 
                      
            end.
            IF avail vasp and avail zsdivasp THEN DO:
              if int (v-leadtm) > int( zsdivasp.leadtm1) then
                ASSIGN zsdivasp.leadtm1  = STRING(v-leadtm,"9999").
            END.
          END.
          assign o-prod = v-prod.
          return.
        END.
      END.
  

/* Catalog */                     
                     
     
    
    
    NstkLoop:
    DO WHILE TRUE ON ENDKEY UNDO, LEAVE.
      DISPLAY
        v-prod
        v-vendno
        v-prodcat
        v-prodcost
        v-nslistprc
        v-leadtm
        v-prodline
        v-whse
        v-vendno
        v-descrip 
      
        v-nvendorname   
        v-ncontactname  
        v-nquoteno      
        v-nphoneno      


      WITH FRAME f-midnstk.
      UPDATE
        v-descrip 
        v-vendno
        v-prodline
        v-prodcat
        v-whse
        v-prodcost
        v-nslistprc
        v-leadtm

      
        v-nvendorname   
        v-ncontactname  
        v-nquoteno      
        v-nphoneno      

 
      
      WITH FRAME f-midnstk
      EDITING:
        READKEY.
        ASSIGN 
          v-whse       = INPUT v-whse
          v-leadtm     = LEFT-TRIM(v-leadtm,"0")
          v-leadtm     = LEFT-TRIM(v-leadtm," ")
          v-specnstype = "n"
          v-descrip    = INPUT v-descrip.
        
        IF FRAME-FIELD = "v-prodcat" AND INPUT v-prodcat NE "" AND
          v-descrip2 = "" THEN DO:
          {w-sasta.i C INPUT v-prodcat NO-LOCK}
          ASSIGN v-descrip = IF (avail sasta AND v-descrip = "") THEN
                                sasta.descrip
                              ELSE
                                v-descrip.
          DISPLAY v-descrip WITH FRAME f-midnstk.
        END.
        APPLY LASTKEY.
      END. /** end of editing loop. **/
      
      IF {k-cancel.i} OR {k-jump.i} THEN DO:
        LEAVE Nstkloop.
      END.
      
      IF {k-accept.i} THEN DO:

        IF v-vendno = 0 THEN DO:
          ASSIGN v-errorfl = true.
/*
          ASSIGN v-error = " Vendor Number is required.".
          RUN zsdi_vaerrx.p(v-error,"yes").
          NEXT-PROMPT v-vendno WITH FRAME f-midnstk.
          NEXT.
*/
        END.
        ELSE DO:
          {w-apsv.i v-vendno NO-LOCK}
          IF avail apsv THEN DO:
            ASSIGN v-vendnm = apsv.lookupnm.
            IF apsv.statustype = NO THEN DO:
              ASSIGN v-error = "Vendor is Inactive".
              RUN zsdi_vaerrx.p(v-error,"yes").
              NEXT-PROMPT v-vendno WITH FRAME f-midnstk.
              NEXT.
            END.
          END.
          ELSE IF v-vendno NE 0 THEN DO:
            ASSIGN v-error = "Vendor # Not Set Up in Vendor Setup - APSVR".
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT-PROMPT v-vendno WITH FRAME f-midnstk.
            NEXT.
          END.
        END.
        ASSIGN g-vendno = v-vendno.
        IF v-prodcat = "" THEN DO:
          ASSIGN v-errorfl = true.
/*
          ASSIGN v-error = " Product Category is required.".
          RUN zsdi_vaerrx.p(v-error,"yes").
          NEXT-PROMPT v-prodcat WITH FRAME f-midnstk.
          NEXT.
*/
        END.
        ELSE DO:
          {w-sasta.i C v-prodcat NO-LOCK}
          ASSIGN v-descrip = IF avail sasta AND v-descrip = ""  THEN
                                sasta.descrip
                              ELSE
                                v-descrip.
          DISPLAY v-descrip WITH FRAME f-midnstk.
          IF NOT avail sasta THEN DO:
            ASSIGN v-error =
            "Product Category Not Set Up in System Table - SASTT".
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT-PROMPT v-prodcat WITH FRAME f-midnstk.
            NEXT.
          END.
        END.
        IF v-whse = "" THEN DO:
          ASSIGN v-errorfl = true.

/*
          ASSIGN v-error = " Warehouse is required.".
          RUN zsdi_vaerrx.p(v-error,"yes").
          NEXT-PROMPT v-whse WITH FRAME f-midnstk.
          NEXT.
*/
        END.
        ELSE DO:
          {w-icsd.i v-whse NO-LOCK}
          IF NOT avail icsd THEN DO:
            ASSIGN v-error =  " Invalid Warehouse.".
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT-PROMPT v-whse WITH FRAME f-midnstk.
            NEXT.
          END.
        END.
        IF v-prodline NE "" THEN DO:
          {w-icsl.i v-whse v-vendno v-prodline NO-LOCK}
          IF NOT avail icsl THEN DO:
            ASSIGN v-error = " ProdLine notfound - by vendor/by whse.".
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT-PROMPT v-prodline WITH FRAME f-midnstk.
            NEXT.
          END.
        END.
        ELSE DO:
          ASSIGN v-errorfl = true.

/*
          ASSIGN v-error = " ProdLine is required.".
          RUN zsdi_vaerrx.p(v-error,"yes").
          NEXT-PROMPT v-prodline WITH FRAME f-midnstk.
          NEXT.
*/
        END.
        IF v-prodcost = 0 THEN DO:
          ASSIGN v-error = "Product Cost is required.".
          RUN zsdi_vaerrx.p(v-error,"yes").
          NEXT-PROMPT v-prodcost WITH FRAME f-midnstk.
          NEXT.
        END.
/*        
        IF v-nslistprc = 0 THEN DO:
          ASSIGN v-errorfl = true.
/*
          ASSIGN v-error = "List Price is required.".
          RUN zsdi_vaerrx.p(v-error,"yes").
          NEXT-PROMPT v-nslistprc WITH FRAME f-midnstk.
          NEXT.
*/
        END.
 */
        IF v-descrip = " " THEN DO:
          ASSIGN v-error = "(xxxx) Description is required.".
          RUN zsdi_vaerrx.p(v-error,"yes").
          NEXT-PROMPT v-descrip WITH FRAME f-midnstk.
          NEXT.
        END.
        ASSIGN o-prod   = v-prod
        o-whse   = v-whse.
        IF avail t-lines THEN DO:
          ASSIGN 
            t-lines.leadtm   = string(int(v-leadtm),"9999")
            t-lines.descrip  = v-descrip
            t-lines.gdesc    = ""
            o-leadtm        = v-leadtm.
        END.
        FIND FIRST vasp WHERE
                   vasp.cono = g-cono         AND
                   vasp.shipprod = x-quoteno  
        EXCLUSIVE-LOCK NO-ERROR.

        FIND zsdivasp WHERE
             zsdivasp.cono      = g-cono         AND
             zsdivasp.rectype   = "fb"        AND 
             zsdivasp.repairno  = x-quoteno  
        EXCLUSIVE-LOCK NO-ERROR.

        find first rarr-com use-index k-com where 
                   rarr-com.cono     = g-cono      and
                   rarr-com.comtype  = x-quoteno and
                   rarr-com.orderno  = 0           and
                   rarr-com.ordersuf = 0           and
                   rarr-com.lineno   = v-lineno 
                   no-error.
        if avail rarr-com then do:
          ASSIGN  Rarr-com.noteln[1] = v-nvendorname 
                  Rarr-com.noteln[2] = v-ncontactname 
                  Rarr-com.noteln[3] = v-nquoteno 
                  Rarr-com.noteln[4] = v-nphoneno
                  Rarr-com.printfl   = v-errorfl. 
        end.
        else do:
          create rarr-com.
          assign rarr-com.cono     = g-cono      
                 rarr-com.comtype  = x-quoteno 
                 rarr-com.orderno  = 0         
                 rarr-com.ordersuf = 0         
                 rarr-com.lineno   = v-lineno. 
          ASSIGN  Rarr-com.noteln[1] = v-nvendorname 
                  Rarr-com.noteln[2] = v-ncontactname 
                  Rarr-com.noteln[3] =  v-nquoteno 
                  Rarr-com.noteln[4] = v-nphoneno
                  Rarr-com.printfl   = v-errorfl. 
                   
        end.
 
        
        
        IF avail vasp and avail zsdivasp THEN DO:
          if int (v-leadtm) > int( zsdivasp.leadtm1) then
            ASSIGN zsdivasp.leadtm1  = STRING(v-leadtm,"9999").
        END.
        LEAVE NstkLoop.
      END.
    END.
    HIDE FRAME f-midnstk.
    ON cursor-up cursor-up.
    ON cursor-down cursor-down.
  END.
  
/* -------------------------------------------------------------------------  */
  PROCEDURE ExtendedLine_Popup:
/* -------------------------------------------------------------------------  */
    DEF BUFFER ex-t-lines FOR t-lines.
    DEF BUFFER ex-icsp FOR icsp.
    DEF BUFFER ex-icsw FOR icsw.
    HIDE FRAME f-whses.
    HIDE FRAME f-prodlu.

    assign g-whse = x-whse.

    FIND ex-t-lines WHERE ex-t-lines.lineno = v-lineno 
       EXCLUSIVE-LOCK NO-ERROR.
    
    IF v-prod = "" THEN DO:
      ON cursor-up cursor-up.
      ON cursor-down cursor-down.
      LEAVE.
    END.
    
    ON cursor-up back-tab.
    ON cursor-down tab.
    assign v-arpwhse = "".
    FIND FIRST ex-icsp WHERE
               ex-icsp.cono = g-cono     AND
               ex-icsp.prod = v-prod 
               NO-LOCK NO-ERROR.
    FIND FIRST ex-icsw WHERE
               ex-icsw.cono = g-cono     AND
               ex-icsw.prod = v-prod AND
               ex-icsw.whse = x-whse 
               NO-LOCK NO-ERROR.

    IF avail ex-icsp AND NOT avail ex-icsw THEN DO:
      FIND FIRST ex-icsw WHERE
                 ex-icsw.cono = 90 AND
                 ex-icsw.prod = v-prod AND
                 ex-icsw.whse = "main"
                 NO-LOCK NO-ERROR.
    END.
   
    IF AVAIL ex-icsw THEN DO:
      ASSIGN 
         v-arpwhse =  ex-icsw.arpwhse
         v-prodline =  IF avail ex-icsw and v-prodline = " " THEN
                          ex-icsw.prodline
                        ELSE
                          v-prodline
         v-vendno   = IF avail ex-icsw AND v-vendno = 0 THEN
                         ex-icsw.arpvendno
                       ELSE
                         v-vendno.
      IF avail ex-icsw and v-vendno > 0 THEN DO:
        FIND FIRST apsv WHERE
                   apsv.cono   = g-cono AND
                   apsv.vendno = v-vendno
                  NO-LOCK NO-ERROR.
        ASSIGN v-vendnm = IF avail apsv THEN
                            apsv.lookupnm
                          ELSE
                            " ".
        IF avail ex-icsw THEN DO:
          ASSIGN v-leadtm = STRING(INT(ex-icsw.leadtmavg),"9999").
        END.
      END.           
    END.
    ELSE  DO: 
    IF v-vendno > 0 THEN DO:
        FIND FIRST apsv WHERE
                   apsv.cono   = g-cono AND
                   apsv.vendno = v-vendno
        NO-LOCK NO-ERROR.
        ASSIGN v-vendnm = IF avail apsv THEN
                            apsv.lookupnm
                          ELSE
                            " ".
      END.                           
    END. 

    ASSIGN 
       v-leadtm     = LEFT-TRIM(v-leadtm,"0")
       v-leadtm     = LEFT-TRIM(v-leadtm," ").
 
    
    ExtndLoop:
    DO WHILE TRUE ON ENDKEY UNDO, LEAVE.
      DISPLAY
        v-specnstype 
        v-prod
        v-vendno
        v-vendnm
        v-prodcat
        v-prodcost
        v-leadtm
        v-prodline
        v-arpwhse
        v-vendno
        v-descrip
        
      WITH FRAME f-midextend.

      run displayAddons.
      
      PUT SCREEN ROW 18 COL 13 COLOR MESSAGE
    "  F6-ChgAddon                                          ". 
      UPDATE
    /*  v-descrip    */
    /*    v-vendno   */
    /*    v-prodline */
        v-prodcat
    /*  v-whse   */
        v-prodcost
    /*  v-leadtm  */
        b-extend
      WITH FRAME f-midextend 
      EDITING:
        READKEY.
        ASSIGN 
          v-leadtm     = LEFT-TRIM(v-leadtm,"0")
          v-leadtm     = LEFT-TRIM(v-leadtm," ").
        
        IF FRAME-FIELD = "v-prodcat" AND INPUT v-prodcat NE "" AND
          v-descrip = "" THEN DO:
          {w-sasta.i C INPUT v-prodcat NO-LOCK}
          ASSIGN v-descrip = IF (avail sasta AND v-descrip = "") THEN
                                sasta.descrip
                              ELSE
                                v-descrip.
          DISPLAY v-descrip WITH FRAME f-midextend.
        END.
      
/* tahtrain */
      
        IF ({k-sdileave.i &fieldname = "v-prodcost"
                          &completename = "v-prodcost"})  and
          (v-prodcost  NE input v-prodcost)  THEN DO:
          assign v-prodcost = input v-prodcost.
          run Create-Vaspmap (input if (avail ex-t-lines) then 
                                    "updt"
                                  else
                                    "new",
                            input v-lineno,
                            input v-prod,
                            input x-whse,
                            input v-qty,
                            input v-nslistprc,
                            input v-specnstype).
          b-extend:refresh().
          DISPLAY
            b-extend
          WITH FRAME f-midextend. 

        END.
 /* tahtrain */ 
      
 
        
        
        
        
        
        APPLY LASTKEY.
      END. /** end of editing loop. **/
      
      IF {k-cancel.i} OR {k-jump.i} THEN DO:
        LEAVE ExtndLoop.
      END.
      
      
      IF {k-accept.i} THEN DO:
        IF v-vendno = 0 THEN DO:
          ASSIGN v-error = " Vendor Number is required.".
          IF v-specnstype <> "n" then DO:
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT-PROMPT v-vendno WITH FRAME f-midextend.
            NEXT.
          END.
        END.
        ELSE DO:
          {w-apsv.i v-vendno NO-LOCK}
          IF avail apsv THEN DO:
            ASSIGN v-vendnm = apsv.lookupnm.
            IF apsv.statustype = NO THEN DO:
              ASSIGN v-error = "Vendor is Inactive".
              RUN zsdi_vaerrx.p(v-error,"yes").
              NEXT-PROMPT v-vendno WITH FRAME f-midextend.
              NEXT.
            END.
          END.
          ELSE IF v-vendno NE 0 THEN DO:
            ASSIGN v-error = "Vendor # Not Set Up in Vendor Setup - APSVR".
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT-PROMPT v-vendno WITH FRAME f-midextend.
            NEXT.
          END.
        END.
        ASSIGN g-vendno = v-vendno.
        IF v-prodcat = "" THEN DO:
          ASSIGN v-error = " Product Category is required.".
          IF v-specnstype <> "n" then DO:
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT-PROMPT v-prodcat WITH FRAME f-midextend. 
            NEXT.
          END.
        END.
        ELSE DO:
          {w-sasta.i C v-prodcat NO-LOCK}
          ASSIGN v-descrip = IF avail sasta AND v-descrip = ""  THEN
                                sasta.descrip
                              ELSE
                                v-descrip.
          DISPLAY v-descrip WITH FRAME f-midextend.
          IF NOT avail sasta THEN DO:
            ASSIGN v-error =
            "Product Category Not Set Up in System Table - SASTT".
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT-PROMPT v-prodcat WITH FRAME f-midextend.
            NEXT.
          END.
        END.
        IF v-prodline NE "" THEN DO:
          {w-icsl.i x-whse v-vendno v-prodline NO-LOCK}
          IF NOT avail icsl THEN DO:
            ASSIGN v-error = " ProdLine notfound - by vendor/by whse.".
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT-PROMPT v-prodline WITH FRAME f-midextend.
            NEXT.
          END.
        END.
        ELSE DO:
          ASSIGN v-error = " ProdLine is required.".
          IF v-specnstype <> "n" then DO:
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT-PROMPT v-prodline WITH FRAME f-midextend.
            NEXT.
          END.
        END.
        IF v-prodcost = 0 THEN DO:
          ASSIGN v-error = "Product Cost is required.".
          RUN zsdi_vaerrx.p(v-error,"yes").
          NEXT-PROMPT v-prodcost WITH FRAME f-midextend.
          NEXT.
        END.
        IF v-descrip = " " THEN DO:
          ASSIGN v-error = "(xxxx) Description is required.".
          RUN zsdi_vaerrx.p(v-error,"yes").
          NEXT-PROMPT v-descrip WITH FRAME f-midextend.
          NEXT.
        END.
        ASSIGN o-prod   = v-prod
        o-whse   = x-whse.
        IF avail ex-t-lines THEN DO:
          ASSIGN 
            ex-t-lines.leadtm   = string(int(v-leadtm),"9999")
            ex-t-lines.descrip  = v-descrip
            ex-t-lines.gdesc    = ""
            o-leadtm        = string(int(v-leadtm),"9999").
        END.
        FIND FIRST vasp WHERE
                   vasp.cono = g-cono         AND
                   vasp.shipprod = x-quoteno  
        EXCLUSIVE-LOCK NO-ERROR.

        FIND zsdivasp WHERE
             zsdivasp.cono      = g-cono         AND
             zsdivasp.rectype   = "fb"        AND 
             zsdivasp.repairno  = x-quoteno  
        EXCLUSIVE-LOCK NO-ERROR.
        
        IF avail vasp and avail zsdivasp THEN DO:
          if int (v-leadtm) > int( zsdivasp.leadtm1) then
            ASSIGN zsdivasp.leadtm1  = STRING(int(v-leadtm),"9999").
          if v-specnstype = "n" and v-error <> "" then DO:
            find first rarr-com use-index k-com where 
                       rarr-com.cono     = g-cono      and
                       rarr-com.comtype  = x-quoteno and
                       rarr-com.orderno  = 0           and
                       rarr-com.ordersuf = 0           and
                       rarr-com.lineno   = v-lineno 
                       no-error.
            if avail rarr-com then do:
              ASSIGN 
                  Rarr-com.printfl   = if v-error <> "" then true else false. 
            end.
          END.
        END.
        LEAVE ExtndLoop.
      END.
    END.
    HIDE FRAME f-midextend. 
    ON cursor-up cursor-up.
    ON cursor-down cursor-down.
  END.

  
/* -------------------------------------------------------------------------  */
  PROCEDURE ExtendedLine_Popup_Analyze:
/* -------------------------------------------------------------------------  */
    DEF BUFFER ex-t-lines FOR t-lines.
    DEF BUFFER ex-icsp FOR icsp.
    DEF BUFFER ex-icsw FOR icsw.
    HIDE FRAME f-whses.
    HIDE FRAME f-prodlu.

    assign g-whse = x-whse.

    FIND ex-t-lines WHERE ex-t-lines.lineno = v-lineno 
       EXCLUSIVE-LOCK NO-ERROR.
    
    IF v-prod = "" THEN DO:
      ON cursor-up cursor-up.
      ON cursor-down cursor-down.
      LEAVE.
    END.
    
    ON cursor-up back-tab.
    ON cursor-down tab.
    assign v-arpwhse = "".
    FIND FIRST ex-icsp WHERE
               ex-icsp.cono = g-cono     AND
               ex-icsp.prod = v-prod 
               NO-LOCK NO-ERROR.
    FIND FIRST ex-icsw WHERE
               ex-icsw.cono = g-cono     AND
               ex-icsw.prod = v-prod AND
               ex-icsw.whse = x-whse 
               NO-LOCK NO-ERROR.

    IF avail ex-icsp AND NOT avail ex-icsw THEN DO:
      FIND FIRST ex-icsw WHERE
                 ex-icsw.cono = 90 AND
                 ex-icsw.prod = v-prod AND
                 ex-icsw.whse = "main"
                 NO-LOCK NO-ERROR.
    END.
   
    IF AVAIL ex-icsw THEN DO:
      ASSIGN 
         v-arpwhse =  ex-icsw.arpwhse
         v-prodline =  IF avail ex-icsw and v-prodline = " " THEN
                          ex-icsw.prodline
                        ELSE
                          v-prodline
         v-vendno   = IF avail ex-icsw AND v-vendno = 0 THEN
                         ex-icsw.arpvendno
                       ELSE
                         v-vendno.
      IF avail ex-icsw and v-vendno > 0 THEN DO:
        FIND FIRST apsv WHERE
                   apsv.cono   = g-cono AND
                   apsv.vendno = v-vendno
                  NO-LOCK NO-ERROR.
        ASSIGN v-vendnm = IF avail apsv THEN
                            apsv.lookupnm
                          ELSE
                            " ".
        IF avail ex-icsw THEN DO:
          ASSIGN v-leadtm = STRING(INT(ex-icsw.leadtmavg),"9999").
        END.
      END.           
    END.
    ELSE  DO: 
    IF v-vendno > 0 THEN DO:
        FIND FIRST apsv WHERE
                   apsv.cono   = g-cono AND
                   apsv.vendno = v-vendno
        NO-LOCK NO-ERROR.
        ASSIGN v-vendnm = IF avail apsv THEN
                            apsv.lookupnm
                          ELSE
                            " ".
      END.                           
    END. 

    ASSIGN 
       v-leadtm     = LEFT-TRIM(v-leadtm,"0")
       v-leadtm     = LEFT-TRIM(v-leadtm," ").
 
    
    ExtndLoop:
    DO WHILE TRUE ON ENDKEY UNDO, LEAVE.
      DISPLAY
        v-specnstype 
        v-prod
        v-vendno
        v-vendnm
        v-prodcat
        v-prodcost
        v-leadtm
        v-prodline
        v-arpwhse
        v-vendno
        v-descrip
        
      WITH FRAME f-Aextend.

      run displayAddonsAnalyze.
      
      PUT SCREEN ROW 18 COL 13 COLOR MESSAGE
    "  F6-ChgAddon                                          ". 
      UPDATE
    /*  v-descrip    */
    /*    v-vendno   */
    /*    v-prodline */
        v-prodcat
    /*  v-whse   */
        v-prodcost
    /*  v-leadtm  */
        b-Aextend
      WITH FRAME f-Aextend 
      EDITING:
        READKEY.
        ASSIGN 
          v-leadtm     = LEFT-TRIM(v-leadtm,"0")
          v-leadtm     = LEFT-TRIM(v-leadtm," ").
        
        IF FRAME-FIELD = "v-prodcat" AND INPUT v-prodcat NE "" AND
          v-descrip = "" THEN DO:
          {w-sasta.i C INPUT v-prodcat NO-LOCK}
          ASSIGN v-descrip = IF (avail sasta AND v-descrip = "") THEN
                                sasta.descrip
                              ELSE
                                v-descrip.
          
          DISPLAY v-descrip WITH FRAME f-Aextend.
        END.
      
/* tahtrain */
      
        IF ({k-sdileave.i &fieldname = "v-prodcost"
                          &completename = "v-prodcost"})  and
          (v-prodcost  NE input v-prodcost)  THEN DO:
          assign v-prodcost = input v-prodcost.
          run Create-Vaspmap (input if (avail ex-t-lines) then 
                                    "updt"
                                  else
                                    "new",
                            input v-lineno,
                            input v-prod,
                            input x-whse,
                            input v-qty,
                            input v-nslistprc,
                            input v-specnstype).
          b-Aextend:refresh().
          DISPLAY
            b-Aextend
          WITH FRAME f-Aextend. 

        END.
 /* tahtrain */ 
        
        APPLY LASTKEY.
      END. /** end of editing loop. **/
      
      IF {k-cancel.i} OR {k-jump.i} THEN DO:
         b-Aextend:REFRESH() in frame f-Aextend.
         LEAVE ExtndLoop.
      END.
      
      
      IF {k-accept.i} THEN DO:
        IF v-vendno = 0 THEN DO:
          /*
          ASSIGN v-error = " Vendor Number is required.".
          IF v-specnstype <> "n" then DO:
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT-PROMPT v-vendno WITH FRAME f-Aextend.
            NEXT.
          END.
         */
        END.
        ELSE DO:
          {w-apsv.i v-vendno NO-LOCK}
          IF avail apsv THEN DO:
            ASSIGN v-vendnm = apsv.lookupnm.
            IF apsv.statustype = NO THEN DO:
              ASSIGN v-error = "Vendor is Inactive".
              RUN zsdi_vaerrx.p(v-error,"yes").
              NEXT-PROMPT v-vendno WITH FRAME f-Aextend.
              NEXT.
            END.
          END.
          ELSE IF v-vendno NE 0 THEN DO:
            ASSIGN v-error = "Vendor # Not Set Up in Vendor Setup - APSVR".
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT-PROMPT v-vendno WITH FRAME f-Aextend.
            NEXT.
          END.
        END.
        ASSIGN g-vendno = v-vendno.
        IF v-prodcat = "" THEN DO:
         /*
          ASSIGN v-error = " Product Category is required.".
          IF v-specnstype <> "n" then DO:
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT-PROMPT v-prodcat WITH FRAME f-Aextend. 
            NEXT.
          END.
        */
        END.
        ELSE DO:
          {w-sasta.i C v-prodcat NO-LOCK}
          ASSIGN v-descrip = IF avail sasta AND v-descrip = ""  THEN
                                sasta.descrip
                              ELSE
                                v-descrip.
          DISPLAY v-descrip WITH FRAME f-Aextend.
          IF NOT avail sasta THEN DO:
            ASSIGN v-error =
            "Product Category Not Set Up in System Table - SASTT".
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT-PROMPT v-prodcat WITH FRAME f-Aextend.
            NEXT.
          END.
        END.
        IF v-prodline NE "" THEN DO:
          /*
          {w-icsl.i x-whse v-vendno v-prodline NO-LOCK}
          IF NOT avail icsl THEN DO:
            ASSIGN v-error = " ProdLine notfound - by vendor/by whse.".
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT-PROMPT v-prodline WITH FRAME f-Aextend.
            NEXT.
          END.
         */
        END.
        ELSE DO:
          ASSIGN v-error = " ProdLine is required.".
          IF v-specnstype <> "n" then DO:
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT-PROMPT v-prodline WITH FRAME f-Aextend.
            NEXT.
          END.
        END.
        IF v-prodcost = 0 THEN DO:
          ASSIGN v-error = "Product Cost is required.".
          RUN zsdi_vaerrx.p(v-error,"yes").
          NEXT-PROMPT v-prodcost WITH FRAME f-Aextend.
          NEXT.
        END.
        IF v-descrip = " " THEN DO:
          /*
          ASSIGN v-error = "(xxxx) Description is required.".
          RUN zsdi_vaerrx.p(v-error,"yes").
          NEXT-PROMPT v-descrip WITH FRAME f-Aextend.
          NEXT.
         */ 
        END.
        ASSIGN o-prod   = v-prod
        o-whse   = x-whse.
        IF avail ex-t-lines THEN DO:
          ASSIGN 
            ex-t-lines.leadtm   = string(int(v-leadtm),"9999")
            ex-t-lines.descrip  = v-descrip
            ex-t-lines.gdesc    = ""
            o-leadtm        = string(int(v-leadtm),"9999").
        END.
        FIND FIRST vasp WHERE
                   vasp.cono = g-cono         AND
                   vasp.shipprod = x-quoteno  
        EXCLUSIVE-LOCK NO-ERROR.

        FIND zsdivasp WHERE
             zsdivasp.cono      = g-cono         AND
             zsdivasp.rectype   = "fb"        AND 
             zsdivasp.repairno  = x-quoteno  
        EXCLUSIVE-LOCK NO-ERROR.
        
        IF avail vasp and avail zsdivasp THEN DO:
          if int (v-leadtm) > int( zsdivasp.leadtm1) then
            ASSIGN zsdivasp.leadtm1  = STRING(int(v-leadtm),"9999").
          if v-specnstype = "n" and v-error <> "" then DO:
            find first rarr-com use-index k-com where 
                       rarr-com.cono     = g-cono      and
                       rarr-com.comtype  = x-quoteno and
                       rarr-com.orderno  = 0           and
                       rarr-com.ordersuf = 0           and
                       rarr-com.lineno   = v-lineno 
                       no-error.
            if avail rarr-com then do:
              ASSIGN 
                  Rarr-com.printfl   = if v-error <> "" then true else false. 
            end.
          END.
        END.
      

        IF avail ex-t-lines then do:
          FIND FIRST zsdivasp WHERE
                     zsdivasp.cono = g-cono         AND
                     zsdivasp.rectype = "fb"        AND
                     zsdivasp.repairno = x-quoteno  
          EXCLUSIVE-LOCK NO-ERROR.
          FIND vaspsl WHERE RECID(vaspsl) = ex-t-lines.vaspslid
          EXCLUSIVE-LOCK NO-ERROR.
 
          assign zsdivasp.invcost = zsdivasp.invcost - 
                 (vaspsl.prodcost * vaspsl.qtyneeded).
 
          assign
            vaspsl.prodcost     = IF v-specnstype = "l" THEN
                                    0
                                  ELSE
                                    v-prodcost
            ex-t-lines.prodcost   =  IF v-specnstype = "l" THEN
                                       0
                                     ELSE
                                       v-prodcost
            ex-t-lines.extcost    = IF ex-t-lines.specnstype = "l" THEN
                                      0
                                    ELSE
                                      ex-t-lines.prodcost * ex-t-lines.qty.
   
        
          
          assign zsdivasp.invcost = zsdivasp.invcost + 
                 (vaspsl.prodcost * vaspsl.qtyneeded).
          Release vaspsl.  
          run ZsdiVaspMap-update (input "c",
                                 input ex-t-lines.vaspslid).
          run Retotal_Analyze (input ex-t-lines.lineno).
        END.
      END.  
      LEAVE ExtndLoop.
      
    END.
    HIDE FRAME f-Aextend. 

/*    
    if {k-cancel.i} or {k-jump.i} or {k-accept.i} then do:

      find zsdivasp where 
           zsdivasp.cono = g-cono and
           zsdivasp.rectype = "fb" and
           zsdivasp.repairno = x-quoteno no-lock no-error.
      find icsp where icsp.cono =  g-cono and
                         icsp.prod = zsdivasp.partno no-lock no-error.
      if avail icsp then
        find notes use-index k-notes where                        
             notes.cono         = g-cono and                      
             notes.notestype    = "zz" and                        
             notes.primarykey   = "standard cost markup" and      
             notes.secondarykey = icsp.prodcat and         
             notes.pageno       = 1                               
        no-lock no-error.                                       
      if not avail icsp or
        (avail icsp and not avail notes) then
        find notes use-index k-notes where                      
             notes.cono         = g-cono and                    
             notes.notestype    = "zz" and                      
             notes.primarykey   = "standard cost markup" and    
             notes.secondarykey = "" and                        
             notes.pageno       = 1                             
        no-lock no-error.                                     
      if avail notes then 
     /* calculate the cost gain for nonstock or v-prodcost override  */      
        assign iv-addon =  (1 + (dec(notes.noteln[1])  / 100)).   
      else 
        assign iv-addon = 0.                                                  
  
     
      ASSIGN 
          v-maxlead  = 0
          s-maxlead  = INT(zsdivasp.leadtm1)
          p-pct      = IF zsdivasp.margpct > 0 OR
                          zsdivasp.margpct <> 0  THEN
                        zsdivasp.margpct
                       ELSE
                         035.00
          o-pct         = p-pct
          v-Intangables     = zsdivasp.Intangtotl
          v-invcost     = zsdivasp.invcost
          v-Laborcost     = zsdivasp.Labortotl
          v-rebate      = zsdivasp.user8 * -1
          s-FabTotalCost    = v-Intangables + v-invcost + v-Laborcost + v-rebate
          s-trendtot    = s-FabTotalCost * iv-addon
          s-override  = IF zsdivasp.override NE "" THEN
                          "*"
                        ELSE
                          " "
          s-FabTotalPrice  = 
            if zsdivasp.override ne "" then
              zsdivasp.sellprc
            else
            if round(((s-trendtot / (100 - p-pct)) * 100),2) -
               ((s-trendtot / (100 - p-pct) * 100)) <> 0 then
              round(((s-trendtot / (100 - p-pct)) * 100),2) 
               + .01 
            else
              round(((s-trendtot / (100 - p-pct)) * 100),2)
                           
          s-vareqshipdt = zsdivasp.user4
          s-vapromisedt = zsdivasp.user5.
/*     
      b-totals:refresh () in frame f8-total.
*/ 
      DISPLAY 
            s-vareqshipdt
            s-vapromisedt
            v-invcost
            v-Intangables
            v-Laborcost
            v-rebate
            s-FabTotalCost
            s-override
            s-trendtot
            s-maxlead
            p-assigned
            p-approved
            p-pct
            s-FabTotalPrice
            v-pipematerial
            v-pipestyle
          
          WITH FRAME f-ranalyze.
          b-ranalyze:refresh () in frame f-ranalyze.
      apply "entry" to  b-ranalyze in frame f-ranalyze .
   
      leave.
    
    end.
*/


    ON cursor-up cursor-up.
    ON cursor-down cursor-down.
  END.

/* -------------------------------------------------------------------------  */
  PROCEDURE displayAddonsAnalyze:
/* -------------------------------------------------------------------------  */
  on cursor-up cursor-up.
  on cursor-down cursor-down.

  open query q-Aextend for each t-vaspmap where
                               t-vaspmap.cono = g-cono   and
                               t-vaspmap.rectype     = "LQ"            and
                               t-vaspmap.prod        = x-quoteno       and
 /* REMINDER */                t-vaspmap.seqno       = 5               and
                               t-vaspmap.lineno = v-lineno.

  enable b-Aextend with frame f-Aextend.
/*  display x-functions b-title with frame f-midextend. */
  apply "ENTRY" to b-Aextend in frame f-Aextend.

  on cursor-up back-tab.
  on cursor-down tab.
end.


  

/* -------------------------------------------------------------------------  */
  PROCEDURE displayTotalsAddons:
/* -------------------------------------------------------------------------  */
  on cursor-up cursor-up.
  on cursor-down cursor-down.
  for each t-totals:
    delete t-totals.
  end.  
  for each bs-zsdivaspmap where
           bs-zsdivaspmap.cono = g-cono and
           bs-zsdivaspmap.rectype     = "S" and        /* VAES */
           bs-zsdivaspmap.prod        = x-quoteno  and
           bs-zsdivaspmap.description = "ADDON" and
           bs-zsdivaspmap.lineno = 0  no-lock:
  
    for each bl-zsdivaspmap where
             bl-zsdivaspmap.cono = g-cono and
             bl-zsdivaspmap.rectype     = "L" and        /* VAESl */
             bl-zsdivaspmap.prod        = x-quoteno  and
             bl-zsdivaspmap.seqno       = bs-zsdivaspmap.seqno  no-lock:
      find vasps where 
           vasps.cono = g-cono and
           vasps.shipprod = x-quoteno and
           vasps.whse     = x-whse and
           vasps.seqno    = bs-zsdivaspmap.seqno no-lock no-error.
       find vaspsl where 
            vaspsl.cono = g-cono and
            vaspsl.shipprod = x-quoteno and
            vaspsl.whse     = x-whse and
            vaspsl.seqno    = bs-zsdivaspmap.seqno and
            vaspsl.lineno   = bl-zsdivaspmap.lineno no-lock no-error.
      create t-totals.
      assign  t-totals.seqno    = vaspsl.seqno
              t-totals.sctntype = caps(vasps.sctntype)
              t-totals.lineno   = vaspsl.lineno
              t-totals.typename = bl-zsdivaspmap.typename
              t-totals.addonty  = bl-zsdivaspmap.addonty
              t-totals.addon[1] = bl-zsdivaspmap.addon[1]
              t-totals.product  = caps(vaspsl.compprod).
      
      if vasps.sctntype = "in" then do:
        assign t-totals.dvalue = 
               string(vaspsl.qtyneeded,">>>>>>9.99-") + " " +
               string(vaspsl.qtyneeded * vaspsl.prodcost,">>>>>>9.99-").
      end.
      else
      if vasps.sctntype = "II" then do:
        assign t-totals.dvalue = 
        string((vaspsl.qtyneeded * -1),">>>>>>9.99-") + " " +
        string(vaspsl.qtyneeded * vaspsl.prodcost * -1,">>>>>>9.99-").
      end.                                                                           else
      if vasps.sctntype = "IT" then do:
        assign v-hours = truncate(vaspsl.timeelapsed / 3600,0)
               v-minutes =  (vaspsl.timeelapsed mod 3600) / 60.
                
        assign t-totals.dvalue =
          string (v-hours,">>>>>>9") + ":" +
          string (v-minutes,"99") + "  " +
          string( (v-hours + (v-minutes / 60)) * vaspsl.prodcost,">>>>>>9.99-").
      end.                                                                                  

    end.
  end.
  open query q-totals for each t-totals. 

  enable b-totals with frame f8-total.
  apply "ENTRY" to b-totals in frame f8-total.

  on cursor-up back-tab.
  on cursor-down tab.
end.

  
/* -------------------------------------------------------------------------  */
  PROCEDURE refreshTotalsAddons:
/* -------------------------------------------------------------------------  */
  on cursor-up cursor-up.
  on cursor-down cursor-down.
  close query q-totals. 

  for each t-totals:
    delete t-totals.
  end.  
  for each bs-zsdivaspmap where
           bs-zsdivaspmap.cono = g-cono and
           bs-zsdivaspmap.rectype     = "S" and        /* VAES */
           bs-zsdivaspmap.prod        = x-quoteno  and
           bs-zsdivaspmap.description = "ADDON" and
           bs-zsdivaspmap.lineno = 0  no-lock:
  
    for each bl-zsdivaspmap where
             bl-zsdivaspmap.cono = g-cono and
             bl-zsdivaspmap.rectype     = "L" and        /* VAESl */
             bl-zsdivaspmap.prod        = x-quoteno  and
             bl-zsdivaspmap.seqno       = bs-zsdivaspmap.seqno  no-lock:
      find vasps where 
           vasps.cono = g-cono and
           vasps.shipprod = x-quoteno and
           vasps.whse     = x-whse and
           vasps.seqno    = bs-zsdivaspmap.seqno no-lock no-error.
       find vaspsl where 
           vaspsl.cono = g-cono and
           vaspsl.shipprod = x-quoteno and
           vaspsl.whse     = x-whse and
           vaspsl.seqno    = bs-zsdivaspmap.seqno and
           vaspsl.lineno   = bl-zsdivaspmap.lineno no-lock no-error.
      create t-totals.
      assign  t-totals.seqno    = vaspsl.seqno
              t-totals.sctntype = caps(vasps.sctntype)
              t-totals.lineno   = vaspsl.lineno
              t-totals.addon[1] = bl-zsdivaspmap.addon[1]
              t-totals.product  = caps(vaspsl.compprod).
      
      if vasps.sctntype = "in" then do:
        assign t-totals.dvalue = 
               string(vaspsl.qtyneeded,">>>>>>9.99-") + " " +
               string(vaspsl.qtyneeded * vaspsl.prodcost,">>>>>>9.99-").
      end.
      else
      if vasps.sctntype = "II" then do:
        assign t-totals.dvalue = 
        string((vaspsl.qtyneeded * -1),">>>>>>9.99-") + " " +
        string(vaspsl.qtyneeded * vaspsl.prodcost * -1,">>>>>>9.99-").
      end.                                                                     ~      else
      if vasps.sctntype = "IT" then do:
        assign v-hours = truncate(vaspsl.timeelapsed / 3600,0)
            
               v-minutes =  (vaspsl.timeelapsed mod 3600) / 60.
                
        assign t-totals.dvalue =
          string (v-hours,">>>>>>9") + ":" +            
          string (v-minutes,"99") + "  " +
          string( (v-hours + (v-minutes / 60)) * vaspsl.prodcost,">>>>>>9.99-").
      end.                                                                     
    end.
  end.
  open query q-totals for each t-totals. 
  display b-totals with frame f8-total.
  on cursor-up back-tab.
  on cursor-down tab.
end.


/* -------------------------------------------------------------------------  */
  PROCEDURE displayAddons:
/* -------------------------------------------------------------------------  */
  on cursor-up cursor-up.
  on cursor-down cursor-down.

  open query q-extend for each t-vaspmap where
                               t-vaspmap.cono = g-cono   and
                               t-vaspmap.rectype     = "LQ"            and                                      t-vaspmap.prod        = x-quoteno       and
 /* REMINDER */                t-vaspmap.seqno       = 5               and
                               t-vaspmap.lineno = v-lineno.

  enable b-extend with frame f-midextend.
/*  display x-functions b-title with frame f-midextend. */
  apply "ENTRY" to b-extend in frame f-midextend.

  on cursor-up back-tab.
  on cursor-down tab.
end.



/* -------------------------------------------------------------------------  */
  PROCEDURE displayReverseddons:
/* -------------------------------------------------------------------------  */
  Define INPUT PARAMETER ip-product like vaspsl.compprod no-undo.

  define buffer ipb-zsdivaspmap for zsdivaspmap.
  
  on cursor-up cursor-up.
  on cursor-down cursor-down.

  /* Locate Inventory section from Template */    
    find ipb-zsdivaspmap where
         ipb-zsdivaspmap.cono = g-cono and
         ipb-zsdivaspmap.rectype  = "S" and        /* VAES */
         ipb-zsdivaspmap.prod     = x-quoteno  and
         ipb-zsdivaspmap.typename = "INVENTORY" and
         ipb-zsdivaspmap.lineno = 0  no-lock  no-error.
/* REMINDER INDEX NEEDED */         
    if not avail ipb-zsdivaspmap then do:
      message "program error no ipb-zsdivaspmap".
      pause 10.
      return.       
    end.
 
  open query q-revextend for each t-vaspmap where
                                  t-vaspmap.cono = g-cono      and
                                  t-vaspmap.rectype     = "LQ" and   
                                  t-vaspmap.prod        = x-quoteno  and
 /* REMINDER */                   t-vaspmap.seqno       = 
                                     ipb-zsdivaspmap.seqno and
                                  t-vaspmap.typename   = ip-product,
                             first bex-vaspsl where 
                                   bex-vaspsl.cono = g-cono and
                                   bex-vaspsl.shipprod = x-quoteno and
                                   bex-vaspsl.whse     = x-whse and
                                   bex-vaspsl.seqno    = t-vaspmap.seqno and
                                   bex-vaspsl.lineno   = t-vaspmap.lineno.
  if not avail t-vaspmap then do:
  b-revextend:TITLE IN FRAME f-revextend =
"All Line Items Included in '" + " " + "' Addon".
" Sctn Ty   Ln Product                         Qty    Extended          ".
  end.
  else do:

  b-revextend:TITLE IN FRAME f-revextend =
"All Line Items Included in '" + t-vaspmap.typename + "' Addon".
" Sctn Ty   Ln Product                         Qty    Extended          ".
  end.
  enable b-revextend with frame f-revextend.
/*  display x-functions b-title with frame f-midextend. */
  apply "ENTRY" to b-revextend in frame f-revextend.
  on cursor-up back-tab.
  on cursor-down tab.
end.

  
/* -------------------------------------------------------------------------  */
  PROCEDURE Check_product_ID:
/* -------------------------------------------------------------------------  */
    DEFINE INPUT-OUTPUT PARAMETER ix-prod LIKE icsp.prod               NO-UNDO.
    
    v-xref      = "cxbpmigt".
    ON cursor-up tab.
    ON cursor-down back-tab.
    
    IF (v-specnstype = "N" OR
       v-specnstype = "L" ) AND 
       avail t-lines AND
       t-lines.prod = v-prod THEN
      ASSIGN v-type = " ".
    ELSE  
      RUN crossref.p (v-prod,
                      x-whse,      /* tahtrain   was v-whse */
                      v-xref,
                      v-custno,
                      13,
                      5,
                      OUTPUT ix-prod,
                      OUTPUT v-whse,
                      OUTPUT v-type,
                      OUTPUT v-cqty,
                      OUTPUT v-cunit).


/* Catalog */
    ASSIGN 
      g-whse = x-whse
      catalogfl = false
      v-crprod  = ix-prod
      v-crwhse  = v-whse
      v-crtype  = v-type
      v-crqty   = v-cqty
      v-crunit  = v-cunit.

    IF ix-prod = "" THEN
     ASSIGN
       ix-prod   = v-prod
       v-prctype = v-type.
    ELSE
    IF ix-prod <> "" AND v-type = "G" THEN          
      ASSIGN
        v-prod = ix-prod
        catalogfl = IF v-type = "g" THEN true ELSE false.
    IF catalogfl AND v-catconvertfl THEN DO:
      ASSIGN
        sh-prod = s-prod
        s-prod  = v-prod.

      RUN vaexqlx.p ("G",no).
      ASSIGN
        v-prod = s-prod
        s-prod = sh-prod
        s-ZsdiXrefType = "GS".
  
    END.
/* Catalog */   
  
    IF v-prodbrowseopen = TRUE THEN DO:
      ON cursor-up cursor-up.
      ON cursor-down cursor-down.
    END.
  END.
  
/*-------------------------------------------------------------------------  */
  PROCEDURE Whse_Selection:
/* -------------------------------------------------------------------------  */
    IF avail icsp AND v-prodbrowseopen = TRUE AND
        v-prodbrowsekeyed = TRUE THEN
      ASSIGN ctrlw-whse = "".
    DO WITH FRAME f-whses:
      DO WHILE TRUE ON ENDKEY UNDO, LEAVE.
        NEXT-PROMPT s-whse [1].
        CHOOSE FIELD s-whse COLOR INPUT  NO-ERROR.
        COLOR DISPLAY normal s-whse WITH FRAME f-whses.
        DISPLAY s-whse WITH FRAME f-whses.
        HIDE MESSAGE NO-PAUSE.
        IF KEYFUNCTION(LASTKEY) = "return" OR
           KEYFUNCTION(LASTKEY)  = "go" THEN DO:
          IF FRAME-VALUE <> "" THEN DO:
            ASSIGN v-whse = FRAME-VALUE.
            FIND icsw WHERE icsw.cono = g-cono AND     
                 icsw.whse = v-whse AND
                 icsw.prod = IF avail icsp THEN
                               icsp.prod
                             ELSE
                               v-prod
            NO-LOCK NO-ERROR.
            IF avail icsw THEN DO:
              ASSIGN s-netavail = 0.
              {w-icsp.i icsw.prod NO-LOCK}
              IF avail icsp THEN DO:
                {p-netavl.i}
              END.
/* tahtrain
              ASSIGN
                v-prodcost = icsw.avgcost
                v-leadtm   = STRING(INT(icsw.leadtmavg),"9999").
              IF icsw.statustype = "O" THEN
                ASSIGN v-specnstype = "s".
              ELSE
                ASSIGN v-specnstype = " ".
tahtrain */
              /* create comment for vaesl before vaesl creation */
            end.  /* avail icsw */
            DISPLAY v-specnstype v-whse WITH FRAME f-mid1.
            NEXT-PROMPT s-whse [1].
            v-applygo = TRUE.
            LEAVE.
          end. /* frame value */
        END. /* return or go  */
        IF (FRAME-LINE = 1 AND {k-scrlup.i}) OR {k-prevpg.i} THEN DO:
            {p-quvafill.i &direction = PREV}
        END.
        IF (FRAME-LINE = 12 AND {k-scrldn.i}) OR {k-nextpg.i} THEN DO:
            {p-quvafill.i &direction = NEXT}
        END.
      END.
      ASSIGN ctrlw-whse = IF v-whse = x-whse THEN
                            ""
                          ELSE
                            v-whse.
      IF {k-cancel.i} THEN DO:
        ASSIGN v-applygo = FALSE.
        COLOR DISPLAY normal s-whse WITH FRAME f-whses.
        DISPLAY s-whse WITH FRAME f-whses.
        NEXT-PROMPT s-whse [1].
      END.
    END.
  END.
  
/* -------------------------------------------------------------------------  */
  PROCEDURE FindNextFrame:
/* -------------------------------------------------------------------------  */
    DEF INPUT        PARAMETER ix-lastkey          AS INTEGER         NO-UNDO.
    DEF INPUT-OUTPUT PARAMETER ix-currentposition  AS INTEGER         NO-UNDO.
    DEF INPUT-OUTPUT PARAMETER ix-newposition      AS CHAR            NO-UNDO.
    
    IF {k-cancel.i "ix-"} THEN
    DO:
      ASSIGN ix-currentposition = (IF ix-currentposition - 2 < 0 THEN
                                     -1
                                   ELSE
                                     ix-currentposition - 2).
    END.

    IF v-ManualFrameJump  THEN
    DO: /*
      If they jumped frames bring them back into the place they
      jumped from within the operator settings.
      */
      ASSIGN 
        ix-currentposition = (IF ix-currentposition - 1 < 0 THEN
                                0
                              ELSE
                                ix-currentposition - 1)
        v-ManualFrameJump = FALSE.
    END.
    
    IF ix-currentposition = -1 THEN
    DO:
      
      ASSIGN ix-currentposition  = 1.
      
      /*
      assign ix-newposition      = "xx"
      ix-currentposition  = 0.
      return.
      */
    END.
    DO v-inx = (ix-currentposition + 1) TO 4 BY 1:     
      FIND OptionMoves WHERE
           OptionMoves.OptionIndex  = v-inx NO-LOCK NO-ERROR.
      IF avail OptionMoves THEN do:
      LEAVE.
      end.
    END.
    IF NOT avail OptionMoves THEN
    DO v-inx = 1 TO 4 BY 1:
      FIND OptionMoves WHERE
           OptionMoves.OptionIndex  = v-inx NO-LOCK NO-ERROR.
      IF avail OptionMoves THEN
      LEAVE.
    END.
    
    IF NOT avail optionmoves THEN
    DO:
      PAUSE 0.
      LEAVE.
    END.
    
    ASSIGN 
      ix-newposition      = OptionMoves.PROCEDURE
      ix-currentposition  = OptionMoves.OptionIndex.
    IF ix-newposition = "" THEN
    ASSIGN 
      ix-newposition      = "b1"
      ix-currentposition  = 1.
  END.
  
/* -------------------------------------------------------------------------  */
  PROCEDURE OperatorSettings:
/* -------------------------------------------------------------------------  */
    FORM
      "   Movement 1:"  AT 1
      v-xKeyMovement1   AT 16 no-label
      "   Movement 2:"  AT 1
      v-xKeyMovement2   AT 16 no-label
      "   Movement 3:"  AT 1
      v-xKeyMovement3   AT 16 no-label
      "   Movement 4:"  AT 1
      v-xKeyMovement4   AT 16 no-label
      "TagBarPrinter:"  AT 1
      s-printernm       AT 16 no-label
      HELP "  Barcode Tag Printer"
      "TabingF6-Top :"  AT 1
      fldpos1           AT 16 no-label
      HELP "  Top field positioning -> 1=FabNo, 2=Cust#"
      "TabingF7-line:"  AT 1
      fldpos2           AT 16 no-label
      HELP "  Line entry positioning -> 1=LineNo, 2=Product, 3=QtyOrd"
      "Quote Printer:"  AT 1
      q-printernm       AT 16 no-label
      HELP "  Quote form printer "
      "  LineAdvance:"  AT 1
      nextln            AT 16 no-label
      HELP "  Line maintenance advance -> 'Chg' (Change Mode) or space"
      "      Tech ID:"  AT 1
      idcode            AT 16 no-label
      HELP "  Autoload TechId in Vaext(staging)"
      "  DefaultWhse:"  AT 1
      d-whse            AT 16 no-label
      HELP "  Default whse for Vaexq(entry) & Vaext(staging)"
      
    WITH FRAME f-settings CENTERED SIDE-LABELS ROW 6.
      
    DEFINE BUFFER ix-notes FOR notes.
      
    FIND ix-notes WHERE ix-notes.cono = g-cono AND
         ix-notes.notestype = "zz" AND
         ix-notes.primarykey = "OneScreenFab" AND
         ix-notes.secondarykey = g-operinits NO-ERROR.
      IF avail ix-notes THEN DO:
        ASSIGN
          v-xKeyMovement1 = ix-notes.noteln[1]
          v-xKeyMovement2 = ix-notes.noteln[2]
          v-xKeyMovement3 = ix-notes.noteln[3]
          v-xKeyMovement4 = ix-notes.noteln[4]
          s-printernm     = ix-notes.noteln[5]
          fldpos1         = DEC(ix-notes.noteln[6])
          fldpos2         = DEC(ix-notes.noteln[7])
          q-printernm     = ix-notes.noteln[8]
          nextln          = ix-notes.noteln[9]
          idcode          = ix-notes.noteln[10]
          d-whse          = ix-notes.noteln[11].
        IF fldpos1 = 02 AND
          NOT CAN-DO('ctrl-o',KEYLABEL(LASTKEY)) THEN
          ASSIGN g-fabquoteno = "".
      END.
      
      opermain:
      DO WHILE TRUE ON ENDKEY UNDO, LEAVE opermain:
        DISPLAY
          v-xKeyMovement1
          v-xKeyMovement2
          v-xKeyMovement3
          v-xKeyMovement4
          s-printernm
          fldpos1
          fldpos2
          q-printernm
          nextln
          idcode
          d-whse
        WITH FRAME f-settings.
        
        UPDATE
          v-xKeyMovement1
          VALIDATE (CAN-DO ("F6", v-xKeyMovement1) OR
          v-xKeyMovement1 = "",
          "Screen values F6 or blank are only valid values                                    ")
          HELP "F6 Screen for movement in first motion"
          v-xKeyMovement2
          VALIDATE (CAN-DO ("F6,F7,F9,EX", v-xKeyMovement2) OR
          v-xKeyMovement2 = "",
          "Screen values F6,F7,F9,EX or blank are only valid values                           ")
          HELP "F6,F7,F9,EX Screens for movement order"
          v-xKeyMovement3
          VALIDATE (CAN-DO ("F6,F7,F9,EX", v-xKeyMovement3) OR
          v-xKeyMovement3 = "",
          "Screen values F6,F7,F9,EX or blank are only valid values                           ")
          HELP "F6,F7,F9,EX Screens for movement order"
          v-xKeyMovement4
          VALIDATE (CAN-DO ("F6,F7,F9,EX", v-xKeyMovement4) OR
          v-xKeyMovement4 = "",
          "Screen values F6,F7,F9,EX or blank are only valid values                           ")
          HELP "F6,F7,F9,EX Screens for movement order"
          s-printernm
          fldpos1
          fldpos2
          q-printernm
          nextln
          idcode
          d-whse
        WITH FRAME f-settings
        TITLE "Tag Entry Control Options".
                
        ASSIGN 
          s-printernm
          q-printernm
          fldpos1
          fldpos2
          nextln
          idcode
          d-whse.
                
       IF s-printernm NE "" THEN DO:
         {w-sasp.i s-printernm NO-LOCK}
         IF NOT avail sasp OR
           (avail sasp AND (NOT sasp.pcommand BEGINS "sh")) THEN DO:
           ASSIGN v-error = " Invalid Printer -- please select a label printer".
           RUN zsdi_vaerrx.p(v-error,"yes").
           NEXT.
         END.
       END.
                
       IF {k-accept.i} THEN DO TRANSACTION:
         FIND ix-notes WHERE ix-notes.cono = g-cono AND
              ix-notes.notestype = "zz" AND
              ix-notes.primarykey = "OneScreenFab" AND
              ix-notes.secondarykey = g-operinits NO-ERROR.
         IF NOT avail ix-notes THEN DO:
           CREATE ix-notes.
           ASSIGN 
             ix-notes.cono = g-cono
             ix-notes.notestype = "zz"
             ix-notes.primarykey = "OneScreenFab"
             ix-notes.secondarykey = g-operinits.
         END.
         ASSIGN 
           ix-notes.noteln[1]  = v-xKeyMovement1
           ix-notes.noteln[2]  = v-xKeyMovement2
           ix-notes.noteln[3]  = v-xKeyMovement3
           ix-notes.noteln[4]  = v-xKeyMovement4
           ix-notes.noteln[5]  = s-printernm
           ix-notes.noteln[6]  = STRING(fldpos1,"9")
           ix-notes.noteln[7]  = STRING(fldpos2,"9")
           ix-notes.noteln[8]  = q-printernm
           ix-notes.noteln[9]  = nextln
           ix-notes.noteln[10] = idcode.
           ix-notes.noteln[11] = d-whse.
                  
         ASSIGN v-inx2 = 0.
                  
         FOR EACH OptionMoves:
           DELETE OptionMoves.
         END.
                  
         DO v-inx = 1 TO 4:
           IF ix-notes.noteln[v-inx] <> "" THEN
           DO:
             ASSIGN v-inx2 = v-inx2 + 1.
             CREATE OptionMoves.
             ASSIGN 
               OptionMoves.OptionIndex  = v-inx2.
             IF ix-notes.noteln[v-inx] = "f6" THEN
               ASSIGN 
                 OptionMoves.PROCEDURE    = "t1"
                 OptionMoves.Procname     = "Top 1".
             ELSE
             IF ix-notes.noteln[v-inx] = "f7" THEN
               ASSIGN 
                 OptionMoves.PROCEDURE    = "m1"
                 OptionMoves.Procname     = "Middle 1".
             ELSE
             IF ix-notes.noteln[v-inx] = "f9" THEN
               ASSIGN 
                 OptionMoves.PROCEDURE    = "b2"
                 OptionMoves.Procname     = "Bottom 2".
             ELSE
             IF ix-notes.noteln[v-inx] = "EX" THEN
               ASSIGN 
                 OptionMoves.PROCEDURE    = "Ex"
                 OptionMoves.Procname     = "New Order".
                 
           END.
         END. /* end do */
         IF avail ix-notes THEN RELEASE ix-notes.
           ASSIGN v-error = "".
         RUN zsdi_vaerrx.p(v-error,"yes").
         LEAVE opermain.
       END.  /* do while true */
     END.
     ASSIGN v-error = "".
     RUN zsdi_vaerrx.p(v-error,"yes").
     HIDE FRAME f-settings.
   END.
            
            
  PROCEDURE prt-routine2:
    DEF INPUT PARAMETER ip-printernm LIKE s-printernm.
    DEF INPUT PARAMETER ip-notes     AS LOGICAL.
    /*d Create Fake Report Name */
    DO WHILE TRUE:
      ASSIGN v-reportnm = STRING(TIME) + STRING(RANDOM(1,999)).
      IF NOT CAN-FIND(r-sapb WHERE
                      r-sapb.cono       = g-cono AND
                      r-sapb.reportnm   = v-reportnm NO-LOCK)
      THEN LEAVE.
    END.
    CREATE r-sapb.
    /*tb 21702 09/13/96 mes; Removed assign of b-sapb.oper2 */
    RUN makequoteno(INPUT g-fabquoteno,OUTPUT x-quoteno).
    ASSIGN 
      r-sapb.cono         = g-cono
      r-sapb.reportnm     = v-reportnm
      r-sapb.storefl      = NO
      r-sapb.currproc     = "vaepv"
      r-sapb.printoptfl   = NO
      r-sapb.rpttitle     = "Vasp Quote Fab Print"
      r-sapb.startdta     = STRING(g-today,"999999")
      r-sapb.printernm    = ip-printernm
      r-sapb.priority     = 1
      r-sapb.operinit     = g-operinits
      r-sapb.starttm      = TIME
      r-sapb.delfl        = YES
      r-sapb.inusecd      = "y"
      r-sapb.runnowfl     = YES
      r-sapb.rangebeg[2]  = x-quoteno
      r-sapb.rangeend[2]  = x-quoteno
      r-sapb.optvalue[1]  = x-whse
      r-sapb.optvalue[2]  = "yes"
      r-sapb.optvalue[3]  = "no"
      r-sapb.user1        = IF v-retain THEN
                              "yes"
                            ELSE
                              "no"
      r-sapb.optvalue[4]  = IF p-detail THEN
                              "yes"
                            ELSE
                              "no"
      r-sapb.optvalue[5]  = IF p-prce = YES THEN
                              "yes"
                            ELSE
                              "no"
      r-sapb.optvalue[6]  = IF p-onlydesc = YES THEN
                              "yes"
                            ELSE
                              "no"
      r-sapb.optvalue[7]  = IF ip-notes = YES THEN
                              "yes"
                            ELSE
                              "no".
    IF ip-printernm = "fax" OR ip-printernm = "e-mail" OR
      ip-printernm = "email" THEN DO:
      DO v-idx = 1 TO 10:
        ASSIGN r-sapb.faxcom[v-idx] = v-faxcom[v-idx].
      END.
      ASSIGN 
        r-sapb.faxto1     = v-faxto1
        r-sapb.faxto2     = v-faxto2
        r-sapb.faxfrom    = v-faxfrom
        r-sapb.faxphoneno = IF ip-printernm = "fax" THEN
                              v-faxphoneno
                            ELSE
                              "em:" + zt-emailaddr.
    END.
    CREATE sapbv.
    ASSIGN 
      sapbv.cono       = g-cono
      sapbv.reportnm   = r-sapb.reportnm
      sapbv.selecttype = "p"
      sapbv.apinvno    = x-quoteno
      sapbv.vendno     = v-custno
      sapbv.seqno      = 1
      sapbv.transtype  = ""
      sapbv.user1      = "vaexq".
    RELEASE sapbv.
    FIND sapb WHERE RECID(sapb) = RECID(r-sapb) EXCLUSIVE-LOCK NO-ERROR.
    {w-sassr.i ""vaepv"" NO-LOCK}
    IF avail sassr THEN DO:
      ASSIGN 
        v-reportid = sassr.reportid
        v-openprfl = sassr.openprfl
        v-openprno = sassr.openprno
        v-sassrid  = RECID(sassr)
        v-sapbid   = RECID(r-sapb)
        v-reportnm = r-sapb.reportnm
        v-batfl    = NO.
      /*
      run rptctl2.p("vaepv.p",
      no,
      0,
      no,
      0,
      no).
      */
      RUN vaepv.p.
      IF sendit THEN
        ASSIGN sendit = NO.
    END. /* avail sassr */
    IF avail r-sapb THEN
      DELETE r-sapb.
    IF avail sapbv THEN
      DELETE sapbv.
    PAUSE 0.
    LEAVE.
  END.
  
  PROCEDURE f6-global:
    HIDE FRAME f-statuline.
    HIDE FRAME f-statuline2.
    ASSIGN v-notelit = "Internal".
    DEFINE VAR ix AS INTEGER      NO-UNDO.
    DEFINE VAR del-fl AS LOGICAL  NO-UNDO.
    DEFINE VAR fillit AS INT      NO-UNDO.
    DEFINE VAR notechk AS LOGICAL NO-UNDO.
    ASSIGN
      v-repositionfl  = TRUE
      v-comments      = ""
      v-comments2     = ""
      v-comments3     = ""
      notechk         = NO.
    HIDE FRAME f-whses.
    FOR EACH notes
      WHERE notes.cono         = g-cono     AND
            notes.notestype    = "fa"       AND
            notes.primarykey   = x-quoteno  AND
            notes.secondarykey = x-whse     AND
            notes.requirefl    = YES        AND
            notes.pageno       = 2
      NO-LOCK:
      DO v-idx = 1 TO 16:
        ASSIGN v-comments[v-idx] = notes.noteln[v-idx].
      END.
    END.
    DISPLAY v-notelit v-comments WITH FRAME f6-com.
    ASSIGN f8-title =
" F6-ExternalNotes F9-PrintQuote F10-Update TotalInfo Ctrl-n-INTERNALNOTES  ".
    DISPLAY f8-title WITH FRAME f8-total.
    
    f6-globloop:
    DO WHILE TRUE ON ENDKEY UNDO, LEAVE f6-globloop:
      IF v-repositionfl THEN DO:
        DO v-repositionix = 16 TO 1 BY -1:
          IF v-comments[v-repositionix] <> "" THEN DO:
            IF v-repositionix NE 16 THEN
              ASSIGN v-repositionix = v-repositionix + 1.
            LEAVE.
          END.
        END.
        IF v-repositionix = 0 THEN
          ASSIGN v-repositionix = 1.
        NEXT-PROMPT v-comments[v-repositionix] WITH FRAME f6-com.
      END.
      UPDATE v-comments
      GO-ON (f4) WITH 5 DOWN FRAME f6-com
      EDITING:
        IF v-repositionfl THEN DO:
          ASSIGN v-repositionfl = FALSE.
          IF v-repositionix = 1 THEN
            APPLY "right-end" TO v-comments[1] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 2 THEN
            APPLY "right-end" TO v-comments[2] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 3 THEN
            APPLY "right-end" TO v-comments[3] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 4 THEN
            APPLY "right-end" TO v-comments[4] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 5 THEN
            APPLY "right-end" TO v-comments[5] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 6 THEN
            APPLY "right-end" TO v-comments[6] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 7 THEN
            APPLY "right-end" TO v-comments[7] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 8 THEN
            APPLY "right-end" TO v-comments[8] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 9 THEN
            APPLY "right-end" TO v-comments[9] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 10 THEN
            APPLY "right-end" TO v-comments[10] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 11 THEN
            APPLY "right-end" TO v-comments[11] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 12 THEN
            APPLY "right-end" TO v-comments[12] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 13 THEN
            APPLY "right-end" TO v-comments[13] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 14 THEN
            APPLY "right-end" TO v-comments[14] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 15 THEN
            APPLY "right-end" TO v-comments[15] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 16 THEN
            APPLY "right-end" TO v-comments[16] IN FRAME f6-com.
        END.
        READKEY.
        IF {k-jump.i} OR {k-cancel.i} THEN DO:
          APPLY LASTKEY.
          LEAVE f6-globloop.
        END.
        APPLY LASTKEY.
      END.
      
      DO v-idx = 1 TO 16:
        IF v-comments[v-idx] NE "" THEN
          ASSIGN notechk = YES.
      END.
      
      IF {k-accept.i} AND notechk THEN DO:
        DEFINE BUFFER b-vasp FOR vasp.
        DEFINE BUFFER b-zsdivasp FOR zsdivasp.

        FIND FIRST b-vasp USE-INDEX k-vasp WHERE
                   b-vasp.cono     = g-cono    AND
                   b-vasp.shipprod = x-quoteno 
        EXCLUSIVE-LOCK NO-ERROR.

        FIND b-zsdivasp WHERE
             b-zsdivasp.cono     = g-cono    AND
             b-zsdivasp.rectype  = "fb"      AND
             b-zsdivasp.repairno = x-quoteno 
        EXCLUSIVE-LOCK NO-ERROR.
        
        FIND FIRST notes
        WHERE notes.cono         = g-cono     AND
              notes.notestype    = "fa"       AND
              notes.primarykey   = x-quoteno  AND
              notes.secondarykey = x-whse     AND
              notes.requirefl    = YES        AND
              notes.pageno       = 2
        EXCLUSIVE-LOCK NO-ERROR.
        IF avail b-vasp AND avail notes THEN
          ASSIGN b-vasp.notesfl = "!".
        IF NOT avail notes THEN
        DO:
          CREATE notes.
          ASSIGN 
            notes.cono         = g-cono
            notes.notestype    = "fa"
            notes.primarykey   = x-quoteno
            notes.secondarykey = x-whse
            notes.printfl      = YES
            notes.requirefl    = YES
            notes.pageno       = 2
            b-vasp.notesfl     = "!".
          DO v-idx = 1 TO 16:
            ASSIGN notes.noteln[v-idx] = INPUT v-comments[v-idx].
          END.
          RELEASE notes.
        END.
        ELSE DO:
          DO v-idx = 1 TO 16:
            ASSIGN notes.noteln[v-idx] = INPUT v-comments[v-idx].
          END.
        END.
        IF avail notes THEN DO:
          ASSIGN del-fl = YES.
          DO ix = 1 TO 16:
            IF notes.noteln[ix] NE "" THEN
            DO:
              ASSIGN del-fl = NO.
            END.
          END.
          IF del-fl = YES AND avail b-vasp THEN
          DO:
            ASSIGN b-vasp.notesfl  = "".
            DELETE notes.
          END.
        END.
        IF avail b-vasp THEN
          RELEASE b-vasp.
        IF avail notes THEN
          RELEASE notes.
        LEAVE f6-globloop.
      END.
      LEAVE f6-globloop.
    END.
    HIDE FRAME f6-com.
    ASSIGN f8-title =
" F6-ExternalNotes F9-PrintQuote F10-UPDATE TOTALINFO Ctrl-n-InternalNotes  ".
    DISPLAY f8-title WITH FRAME f8-total.
    IF avail notes THEN
      RELEASE notes.
    LEAVE.
  END.
  
  PROCEDURE f6-comments:
    DEFINE VAR ix AS INTEGER     NO-UNDO.
    DEFINE VAR del-fl AS LOGICAL NO-UNDO.
    DEFINE VAR fillit AS INT     NO-UNDO.
    ASSIGN
      v-repositionfl  = TRUE
      v-comments      = ""
      v-comments2     = ""
      v-comments3     = ""
      v-notelit       = "External".
    DISPLAY v-notelit WITH FRAME f6-com.
    ASSIGN f8-title =
" F6-ExtNotes F7-UpdtIntang F9-PrtQuote F10-Updt TotalInfo Ctrl-n-INTNOTES  ".
    DISPLAY f8-title WITH FRAME f8-total.
    
    f6-globloop:
    DO WHILE TRUE ON ENDKEY UNDO, LEAVE f6-globloop:
      IF v-repositionfl THEN DO:
        DO v-repositionix = 16 TO 1 BY -1:
          IF v-comments[v-repositionix] <> "" THEN DO:
            IF v-repositionix NE 16 THEN
              ASSIGN v-repositionix = v-repositionix + 1.
            LEAVE.
          END.
        END.
        IF v-repositionix = 0 THEN
          ASSIGN v-repositionix = 1.
        NEXT-PROMPT v-comments[v-repositionix] WITH FRAME f6-com.
      END.
      UPDATE v-comments
      GO-ON (f4) WITH 5 DOWN FRAME f6-com
      EDITING:
        IF v-repositionfl THEN DO:
          ASSIGN v-repositionfl = FALSE.
          IF v-repositionix = 1 THEN
            APPLY "right-end" TO v-comments[1] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 2 THEN
            APPLY "right-end" TO v-comments[2] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 3 THEN
            APPLY "right-end" TO v-comments[3] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 4 THEN
            APPLY "right-end" TO v-comments[4] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 5 THEN
            APPLY "right-end" TO v-comments[5] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 6 THEN
            APPLY "right-end" TO v-comments[6] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 7 THEN
            APPLY "right-end" TO v-comments[7] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 8 THEN
            APPLY "right-end" TO v-comments[8] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 9 THEN
            APPLY "right-end" TO v-comments[9] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 10 THEN
            APPLY "right-end" TO v-comments[10] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 11 THEN
            APPLY "right-end" TO v-comments[11] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 12 THEN
            APPLY "right-end" TO v-comments[12] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 13 THEN
            APPLY "right-end" TO v-comments[13] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 14 THEN
            APPLY "right-end" TO v-comments[14] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 15 THEN
            APPLY "right-end" TO v-comments[15] IN FRAME f6-com.
          ELSE
          IF v-repositionix = 16 THEN
            APPLY "right-end" TO v-comments[16] IN FRAME f6-com.
        END.
        READKEY.
        IF {k-jump.i} OR {k-cancel.i} THEN DO:
          APPLY LASTKEY.
          LEAVE f6-globloop.
        END.
        APPLY LASTKEY.
      END.
      
      DO v-idx = 1 TO 16:
        IF v-comments[v-idx] NE "" THEN
          ASSIGN notechk = YES.
      END.
      
      IF {k-accept.i} AND notechk THEN DO:
        DEFINE BUFFER b-vasp FOR vasp.
        FIND FIRST b-vasp USE-INDEX k-vasp WHERE
                   b-vasp.cono     = g-cono    AND
                   b-vasp.shipprod = x-quoteno 
        EXCLUSIVE-LOCK NO-ERROR.
        FIND FIRST notes
        WHERE notes.cono         = g-cono     AND
              notes.notestype    = "fa"       AND
              notes.primarykey   = x-quoteno  AND
              notes.secondarykey = x-whse     AND
              notes.requirefl    = YES        AND
              notes.pageno       = 2
        EXCLUSIVE-LOCK NO-ERROR.
        IF avail b-vasp AND avail notes THEN
          ASSIGN b-vasp.notesfl = "!".
        IF NOT avail notes THEN
        DO:
          CREATE notes.
          ASSIGN
            notes.cono         = g-cono
            notes.notestype    = "fa"
            notes.primarykey   = x-quoteno
            notes.secondarykey = x-whse
            notes.printfl      = YES
            notes.requirefl    = YES
            notes.pageno       = 2
            b-vasp.notesfl     = "!".
          DO v-idx = 1 TO 16:
            ASSIGN notes.noteln[v-idx] = INPUT v-comments[v-idx].
          END.
          RELEASE notes.
        END.
        ELSE DO:
          DO v-idx = 1 TO 16:
            ASSIGN notes.noteln[v-idx] = INPUT v-comments[v-idx].
          END.
        END.
        IF avail notes THEN DO:
          ASSIGN del-fl = YES.
          DO ix = 1 TO 16:
            IF notes.noteln[ix] NE "" THEN
            DO:
              ASSIGN del-fl = NO.
            END.
          END.
          IF del-fl = YES AND avail b-vasp THEN
          DO:
            ASSIGN b-vasp.notesfl  = "".
            DELETE notes.
          END.
        END.
        IF avail b-vasp THEN
          RELEASE b-vasp.
        IF avail notes THEN
          RELEASE notes.
        LEAVE f6-globloop.
      END.
      LEAVE f6-globloop.
    END.
    HIDE FRAME f6-com.
    ASSIGN f8-title =
" F6-ExtNotes F7-UpdtIntang F9-PrtQuote F10-UPDT TOTALINFO Ctrl-n-IntNotes  ".
    DISPLAY f8-title WITH FRAME f8-total.
    IF avail notes THEN
      RELEASE notes.
    LEAVE.
  END.
  
  PROCEDURE get_tagseqno:
    DO:
      
      DEFINE OUTPUT param s-jobseqno AS  DEC FORMAT "zzzzzz9.99-"  NO-UNDO.
      DEFINE BUFFER gj-ztmk FOR ztmk.
      /*
      find last gj-ztmk use-index k-seqnot where
      gj-ztmk.cono      = g-cono    and
      gj-ztmk.ordertype = "fb"      and
      gj-ztmk.techid    = v-takenby and
      gj-ztmk.statuscd  = 0         and
      gj-ztmk.stagecd   > 0         and
      gj-ztmk.jobseqno  > 0
      no-lock no-error.
      if avail gj-ztmk then
      do:
      assign s-jobseqno = gj-ztmk.jobseqno.
      end.
      else
      do:
      find last gj-ztmk use-index k-seqnot where
      gj-ztmk.cono      = g-cono    and
      gj-ztmk.ordertype = "fb"      and
      gj-ztmk.techid    = v-takenby and
      gj-ztmk.jobseqno  > 0
      no-lock no-error.
      if avail gj-ztmk then
      do:
      assign s-jobseqno = gj-ztmk.jobseqno + 1.
      end.
      else
      do:
      assign s-jobseqno = 1.
      end.
      end.
      release gj-ztmk.
      leave.
      */
    END.
  END PROCEDURE.
/* ------------------------------------------------------------------------ */
  PROCEDURE f8totals:
/* ------------------------------------------------------------------------ */
    DEFINE BUFFER zxt2-sasos for sasos. 
    DEFINE BUFFER zy-sasos for sasos. 
    DEFINE BUFFER bp-notes for notes. 
    DEFINE BUFFER bp-icsp for icsp. 
    DEFINE VAR ix-lastkey as integer no-undo.
    DEFINE VAR v-inbrowsefl as logical init false no-undo.

    IF FRAME-NAME = "f8-total"        OR
      (PROGRAM-NAME(1) BEGINS "vaexd" OR
      PROGRAM-NAME(2) BEGINS "vaexd" OR
      PROGRAM-NAME(3) BEGINS "vaexd" OR
      PROGRAM-NAME(4) BEGINS "vaexd" OR
      PROGRAM-NAME(5) BEGINS "vaexd" OR
      PROGRAM-NAME(6) BEGINS "vaexd" OR
      PROGRAM-NAME(7) BEGINS "vaexd" OR
      PROGRAM-NAME(8) BEGINS "vaexd" OR
      PROGRAM-NAME(9) BEGINS "vaexd") THEN
      LEAVE.
    assign v-errmsg = "".
    find first zxt2-sasos where
               zxt2-sasos.cono  = g-cono and
               zxt2-sasos.oper2 = g-operinits and
               zxt2-sasos.menuproc = "zxt2"
               no-lock no-error.
 
    RUN makequoteno(g-fabquoteno,OUTPUT x-quoteno).
      
    IF v-errfl THEN
      LEAVE.
      
    ON cursor-up back-tab.
    ON cursor-down tab.
    
    IF v-custno <= 0 THEN DO:
      ASSIGN v-framestate = "t1".
      PUT SCREEN ROW 23 COL 1 " Customer# required - please enter.".
      PAUSE 2.
      NEXT.
    END.
      
    IF FRAME-NAME = "f-mid1" OR
      FRAME-NAME = "f-top1" THEN
      ASSIGN 
        q-rowid = ROWID(t-lines)
        o-framenm = FRAME-NAME.
  
    HIDE FRAME f-whses.
    ASSIGN s-override = "".
    ASSIGN f8-title =
" F6-ExtNotes F9-PrtQuote                                                    ".
  
    DISPLAY f8-title WITH FRAME f8-total.
/*  */
      ASSIGN 
        p-detail       = NO
        p-onlydesc     = NO
        p-assigned     = NO
        p-approved     = NO
        v-Intangables      = 0
        v-invcost      = 0
        v-Laborcost      = 0.
      FIND FIRST vasp WHERE
                 vasp.cono = g-cono         AND
                 vasp.shipprod = x-quoteno 
      NO-LOCK NO-ERROR.

      IF avail vasp THEN DO:
        find zsdivasp where 
             zsdivasp.cono = g-cono and
             zsdivasp.rectype = "fb" and
             zsdivasp.repairno = vasp.shipprod no-lock no-error.
        find bp-icsp where bp-icsp.cono =  g-cono and
                           bp-icsp.prod = zsdivasp.partno no-lock no-error.
        if avail bp-icsp then
          find bp-notes use-index k-notes where                        
               bp-notes.cono         = g-cono and                      
               bp-notes.notestype    = "zz" and                        
               bp-notes.primarykey   = "standard cost markup" and      
               bp-notes.secondarykey = bp-icsp.prodcat and         
               bp-notes.pageno       = 1                               
          no-lock no-error.                                       
        if not avail bp-icsp or
          (avail bp-icsp and not avail bp-notes) then
          find bp-notes use-index k-notes where                      
               bp-notes.cono         = g-cono and                    
               bp-notes.notestype    = "zz" and                      
               bp-notes.primarykey   = "standard cost markup" and    
               bp-notes.secondarykey = "" and                        
               bp-notes.pageno       = 1                             
          no-lock no-error.                                     
        if avail bp-notes then 
       /* calculate the cost gain for nonstock or v-prodcost override  */      
          assign iv-addon =  (1 + (dec(bp-notes.noteln[1])  / 100)).   
        else 
          assign iv-addon = 0.                                                  
     
        ASSIGN 
          v-maxlead  = 0
          s-maxlead  = INT(zsdivasp.leadtm1)
          p-pct      = IF zsdivasp.margpct > 0 OR
                          zsdivasp.margpct <> 0  THEN
                         zsdivasp.margpct
                       ELSE
                         035.00
          o-pct         = p-pct
          v-Intangables     = zsdivasp.Intangtotl
          v-invcost     = zsdivasp.invcost
          v-Laborcost     = zsdivasp.Labortotl
          v-rebate      = zsdivasp.user8 * -1
          s-FabTotalCost    = v-Intangables + v-invcost + v-Laborcost + v-rebate
          s-trendtot    = s-FabTotalCost * iv-addon
          s-override  = IF zsdivasp.override NE "" THEN
                          "*"
                        ELSE
                          " "
          s-FabTotalPrice  = 
                         if zsdivasp.override ne "" then
                            zsdivasp.sellprc
                         else
                         if round(((s-trendtot / (100 - p-pct)) * 100),2) -
                            ((s-trendtot / (100 - p-pct) * 100)) <> 0 then
                            round(((s-trendtot / (100 - p-pct)) * 100),2) 
                              + .01 
                         else
                            round(((s-trendtot / (100 - p-pct)) * 100),2)
          s-vareqshipdt = zsdivasp.user4
          s-vapromisedt = zsdivasp.user5.
       
      END. /* Avail zsdivasp */
 
/* */      
    f8loop:
    REPEAT:
/*
      ASSIGN 
        p-detail       = NO
        p-onlydesc     = NO
        p-assigned     = NO
        p-approved     = NO
        v-Intangables      = 0
        v-invcost      = 0
        v-Laborcost      = 0.
      FIND FIRST vasp WHERE
                 vasp.cono = g-cono         AND
                 vasp.shipprod = x-quoteno 
      NO-LOCK NO-ERROR.

      IF avail vasp THEN DO:
        find zsdivasp where 
             zsdivasp.cono = g-cono and
             zsdivasp.rectype = "fb" and
             zsdivasp.repairno = vasp.shipprod no-lock no-error.
        find bp-icsp where bp-icsp.cono =  g-cono and
                           bp-icsp.prod = zsdivasp.partno no-lock no-error.
        if avail bp-icsp then
          find bp-notes use-index k-notes where                        
               bp-notes.cono         = g-cono and                      
               bp-notes.notestype    = "zz" and                        
               bp-notes.primarykey   = "standard cost markup" and      
               bp-notes.secondarykey = bp-icsp.prodcat and         
               bp-notes.pageno       = 1                               
          no-lock no-error.                                       
        if not avail bp-icsp or
          (avail bp-icsp and not avail bp-notes) then
          find bp-notes use-index k-notes where                      
               bp-notes.cono         = g-cono and                    
               bp-notes.notestype    = "zz" and                      
               bp-notes.primarykey   = "standard cost markup" and    
               bp-notes.secondarykey = "" and                        
               bp-notes.pageno       = 1                             
          no-lock no-error.                                     
        if avail bp-notes then 
       /* calculate the cost gain for nonstock or v-prodcost override  */      
          assign iv-addon =  (1 + (dec(bp-notes.noteln[1])  / 100)).   
        else 
          assign iv-addon = 0.                                                  
     
        ASSIGN 
          v-maxlead  = 0
          s-maxlead  = INT(zsdivasp.leadtm1)
          p-pct      = IF zsdivasp.margpct > 0 OR
                          zsdivasp.margpct <> 0  THEN
                         zsdivasp.margpct
                       ELSE
                         035.00
          o-pct         = p-pct
          v-Intangables     = zsdivasp.Intangtotl
          v-invcost     = zsdivasp.invcost
          v-Laborcost     = zsdivasp.Labortotl
          v-rebate      = zsdivasp.user8 * -1
          s-FabTotalCost    = v-Intangables + v-invcost + v-Laborcost + v-rebate
          s-trendtot    = s-FabTotalCost * iv-addon
          s-override  = IF zsdivasp.override NE "" THEN
                          "*"
                        ELSE
                          " "
          s-FabTotalPrice  = if zsdivasp.override ne "" then
                           zsdivasp.sellprc
                         else
                         if round(((s-trendtot / (100 - p-pct)) * 100),2) -
                            ((s-trendtot / (100 - p-pct)) * 100) <> 0 then
                            round(((s-trendtot / (100 - p-pct)) * 100),2) 
                             + .01   
                         else
                            round(((s-trendtot / (100 - p-pct)) * 100),2)                  s-vareqshipdt = zsdivasp.user4
          s-vapromisedt = zsdivasp.user5.
       
      END. /* Avail zsdivasp */
        
*/     
      run Build-Material-Browse. 
        
      FIND FIRST vasp WHERE
                 vasp.cono = g-cono         AND
                 vasp.shipprod = x-quoteno 
      NO-ERROR.

      FIND zsdivasp WHERE
           zsdivasp.cono     = g-cono         AND
           zsdivasp.rectype  = "fb"           AND
           zsdivasp.repairno = x-quoteno  
      NO-ERROR.
        /* start of big loop */
      assign v-inbrowsefl = false
             v-errfl2 = 0.
      totloop:
      DO WHILE TRUE ON ENDKEY UNDO, LEAVE:
/*
        PUT SCREEN ROW 22 COL 1 COLOR MESSAGE
"  F6-Top  F7-Lines  F8-TOTALS  F9-SPcNotes                                   ".
*/

        if v-errfl2 <> 0 then do:
          run zsdi_vaerrx.p(substring(v-errmsg,13,80),"yes").  
          next-prompt p-pct with frame f8-total.
        end. 
        


        ASSIGN zsdiupdated = NO.
        FIND zsdivasp WHERE
             zsdivasp.cono = g-cono         AND
             zsdivasp.rectype = "fb"        AND
             zsdivasp.repairno = x-quoteno 
        NO-LOCK NO-ERROR.
        IF avail zsdivasp THEN DO:
          find first t-stages where
                     t-stages.shortname = "QAP" no-lock no-error. 


          IF zsdivasp.stagecd ge t-stages.stage then do:
            assign p-assigned = yes
                   p-approved = yes.
          END.
          ELSE 
          IF zsdivasp.stagecd < t-stages.stage then DO:
            find first t-stages where
                       t-stages.shortname = "ASN" no-lock no-error. 
            IF zsdivasp.stagecd = t-stages.stage then DO:
              assign p-assigned = yes
                     p-approved = no.
            END.
          END.
          ELSE  
            ASSIGN 
              p-assigned  = NO
              p-approved  = NO.
        END.

        DISPLAY 
          s-vareqshipdt
          s-vapromisedt
          v-invcost
          v-Intangables
          v-Laborcost
          v-rebate
          s-FabTotalCost
          s-override
          s-trendtot
          s-maxlead
          p-assigned
          p-approved
          p-pct
          s-FabTotalPrice
          v-pipematerial
          v-pipestyle
        WITH FRAME f8-total.

        run  DisplayTotalsAddons.
        if v-errmsg <> "" then do:
          run zsdi_vaerrx.p(substring(v-errmsg,13,80),"yes").  
          assign v-errmsg = "".
        end.  

        if v-inbrowsefl = true then do:
          next-prompt b-totals with frame f8-total.
          apply "entry" to b-totals in frame f8-total.
          assign v-inbrowsefl = false.
        end.
        
        UPDATE 
          s-vareqshipdt
          s-vapromisedt
          s-maxlead when 1 = 2
          
          p-assigned       WHEN (NOT p-assigned and g-secure > 3) or 
                              (p-assigned and g-secure ge 4 and
                               zsdivasp.stagecd < 60)
          
                               
          p-approved       WHEN (NOT p-approved and g-secure > 3) or
                             (p-approved and g-secure ge 4 and
                              zsdivasp.stagecd < 60)
          
          p-pct       WHEN (avail zxt2-sasos and zxt2-sasos.securcd[6] > 2)
          s-FabTotalPrice WHEN (avail zxt2-sasos AND zxt2-sasos.securcd[6] > 2)
          p-prce
          p-onlydesc
          v-pipematerial
          v-pipestyle
          b-totals
        GO-ON(f6,f7,f9,f10,f11) WITH FRAME f8-total
        EDITING:
          readkey pause 600.
/*
          if {k-cancel.i} and s-FabTotalPrice = input s-FabTotalPrice and
            v-errfl2 <> 0 then do:
            run zsdi_vaerrx.p(substring(v-errmsg,13,80),"yes").  
            next-prompt p-pct with frame f8-total.
            readkey pause 0.
            next totloop.
          end. 
*/                  
          if lastkey = -1 then do:
/*
            if v-errfl2 <> 0 then do:
              run zsdi_vaerrx.p(substring(v-errmsg,13,80),"yes").  
              next-prompt p-pct with frame f8-total.
              next totloop.
            end.  
            else do:
*/
              hide message no-pause.
/* Function Timed Out, Any Changes Not Saved */
              run err.p(1010).
              undo totloop, leave f8loop.
/*            end.     */
          end.
          if frame-field = "b-totals" then do:
            if keylabel(lastkey) = "f6" or 
               keylabel(lastkey) = "f7" or
               keylabel(lastkey) = "f8" then do:
              apply lastkey.
              assign v-inbrowsefl = true.
              next totloop.
            end.
          end.
          else
            assign v-inbrowsefl = false.
          
          IF {k-sdileave.i 
              &functions  = " can-do('PF1',keylabel(lastkey))    or 
                              can-do('f9',keylabel(lastkey))     or 
                              can-do('f11',keylabel(lastkey))    or 
                              can-do('f10',keylabel(lastkey))    or "
              &fieldname     = "p-assigned"               
              &completename  = "p-assigned"} then do:
            if p-assigned <> input p-assigned then do:
              find first t-stages where
                         t-stages.stage = zsdivasp.stagecd no-lock no-error. 
              if input p-assigned = true then do:
                run vaexf-stage.p (input g-cono,
                                   input x-quoteno,
                                   input t-stages.shortname,
                                   input "ASN",
                                   input g-operinits,
                                   input g-secure,
                                   output v-newstage,
                                   output v-stagenm,
                                   output v-stgerror,
                                   output v-stgmsg).
              end.
              else do:
                run vaexf-stage.p (input g-cono,
                                   input x-quoteno,
                                   input t-stages.shortname,
                                   input "RFQ",
                                   input g-operinits,
                                   input g-secure,
                                   output v-newstage,
                                   output v-stagenm,
                                   output v-stgerror,
                                   output v-stgmsg).
              end.
               
              
              
              IF v-stgerror <> 0 then do:
                message v-stgmsg.
                next.
              END.
              else  do:
                FIND zsdivasp WHERE
                     zsdivasp.cono     = g-cono         AND
                     zsdivasp.rectype  = "fb"           AND
                     zsdivasp.repairno = x-quoteno  
                NO-ERROR.
        
                assign zsdivasp.stagecd = v-newstage
                       vasp.user7       = v-newstage.

                find first t-stages where
                           t-stages.stage = zsdivasp.stagecd no-lock no-error. 
                assign v-stage = t-stages.shortname.
                run displayTop1.

              END.
            END.
          END.   

          IF {k-sdileave.i 
              &functions  = " can-do('PF1',keylabel(lastkey))    or 
                              can-do('f9',keylabel(lastkey))     or 
                              can-do('f11',keylabel(lastkey))    or 
                              can-do('f10',keylabel(lastkey))    or "
              &fieldname     = "p-approved"               
              &completename  = "p-approved"} then do:
            if p-approved <> input p-approved then do:
              find first t-stages where
                         t-stages.stage = zsdivasp.stagecd no-lock no-error. 
              if input p-approved = true then do:
                run vaexf-stage.p (input g-cono,
                                   input x-quoteno,
                                   input t-stages.shortname,
                                   input "QAP",
                                   input g-operinits,
                                   input g-secure,
                                   output v-newstage,
                                   output v-stagenm,
                                   output v-stgerror,
                                   output v-stgmsg).
              end.
              else do:
                run vaexf-stage.p (input g-cono,
                                   input x-quoteno,
                                   input t-stages.shortname,
                                   input "ASN",
                                   input g-operinits,
                                   input g-secure,
                                   output v-newstage,
                                   output v-stagenm,
                                   output v-stgerror,
                                   output v-stgmsg).
              end.
               
              
              
              IF v-stgerror <> 0 then do:
                message v-stgmsg.
                next.
              END.
              else  do:
                FIND zsdivasp WHERE
                     zsdivasp.cono     = g-cono         AND
                     zsdivasp.rectype  = "fb"           AND
                     zsdivasp.repairno = x-quoteno  
                NO-ERROR.
        
                assign zsdivasp.stagecd = v-newstage
                       vasp.user7       = v-newstage.

                find first t-stages where
                           t-stages.stage = zsdivasp.stagecd no-lock no-error. 
                assign v-stage = t-stages.shortname.
                run displayTop1.

              END.
            END.
          END.   
              
          if {k-sdileave.i 
              &functions  = " can-do('PF1',keylabel(lastkey))    or 
                              can-do('f9',keylabel(lastkey))     or 
                              can-do('f11',keylabel(lastkey))    or 
                              can-do('f10',keylabel(lastkey))    or "
              &fieldname     = "p-pct"               
              &completename  = "p-pct"} then do:
            if input p-pct <> o-pct then
              assign p-pct = input p-pct
                     s-override = " "
                     s-FabTotalPrice    = 
                         if round((s-trendtot / (100 - input p-pct)) * 100,2) -
                            (s-trendtot / (100 - input p-pct)) * 100 <> 0 then
                           round((s-trendtot / (100 - input p-pct)) * 100,2) 
                            + .01   
                         else
                           round((s-trendtot / (100 - input p-pct)) * 100,2).
            assign o-pct = input p-pct.
            display s-override s-FabTotalPrice with frame f8-total.
            
          END.
 

          if {k-sdileave.i 
              &functions  = " can-do('PF1',keylabel(lastkey))    or 
                              can-do('f9',keylabel(lastkey))     or 
                              can-do('f11',keylabel(lastkey))    or 
                              can-do('f10',keylabel(lastkey))    or "
              &fieldname     = "s-FabTotalPrice"               
              &completename  = "s-FabTotalPrice"} then do:
          
                  /* (unit cost / new sell price ) * 100 */

            s-FabTotalPricex = if  round(((s-trendtot / (100 - p-pct))
                                         * 100),2) 
                              - ((s-trendtot / (100 - p-pct)) * 100) <> 0 then
                               round(((s-trendtot / (100 - p-pct)) * 100),2) 
                                + .01   
                           else
                               round((s-trendtot / (100 - p-pct)) * 100,2).
 
            
            if s-FabTotalPricex <>
            input s-FabTotalPrice then do:
               assign p-pct =  
                  round(((1 - (s-trendtot / input s-FabTotalPrice)) * 100),2).
               

              if 35.00 =    /* changed price back to the default */
                  round(((1 - (s-trendtot / input s-FabTotalPrice)) * 100),2)
              then
                 assign s-override = " ".     
              else
                 assign s-override = "*".
            end.
            
            assign s-FabTotalPrice = input s-FabTotalPrice.
            display s-override p-pct s-FabTotalPrice with frame f8-total.
                  
          END.
          assign v-selectedfl = false. 
          IF {k-func12.i} and frame-field = "v-pipematerial" then do:
            assign v-pipematerial.
            run vaexfPipeLu.p ( input "material",
                                input-output v-pipematerial,
                                output v-selectedfl,
                                output ix-lastkey).
            apply "entry" to v-pipematerial in frame f8-total.

            display v-pipematerial with frame f8-total.
/* tahtrain */
            if {k-cancel.i "ix-"} or {k-jump.i "ix-"} then do:
              next-prompt  v-pipematerial.
              next.
            end.
/* tahtrain */            
            next-prompt v-pipestyle with frame f8-total.
          END.
          
          IF {k-func12.i} and frame-field = "v-pipestyle" then do:
            assign v-pipestyle.
            run vaexfPipeLu.p ( input "Style",
                                input-output v-pipestyle,
                                output v-selectedfl,
                                output ix-lastkey).
             apply "entry" to v-pipestyle in frame f8-total.
             display v-pipestyle with frame f8-total.
/* tahtrain */
             if {k-cancel.i "ix-"} or {k-jump.i "ix-"} then do:
               next-prompt v-pipestyle.
               next.
             end.
/* tahtrain */            
             
             next-prompt b-totals with frame f8-total.
          END.
         
         
          
          
          if ({k-after.i} and 
              (frame-field = "v-pipestyle" or
               frame-field = "v-pipematerial")) or
             (v-selectedfl and 
              (frame-field = "v-pipestyle" or
               frame-field = "v-pipematerial")) or
             ({k-sdileave.i              
               &fieldname     = "v-pipestyle" 
               &completename  = "v-pipestyle"} or
              {k-sdileave.i              
               &fieldname     = "v-pipematerial" 
               &completename  = "v-pipematerial"} ) then do:

            FIND first zsdivaspmap where
                       zsdivaspmap.cono = g-cono and
                       zsdivaspmap.rectype = "GO" and
                                   /* Addon Global Options */ 
                       zsdivaspmap.prod    = x-quoteno and
                       zsdivaspmap.seqno   = 0 and
                       zsdivaspmap.lineno  = 0 and
                       zsdivaspmap.typename = "Piping Material" and
                       zsdivaspmap.description = v-pipematerial 
            NO-LOCK NO-ERROR.
            
            IF not avail zsdivaspmap then do:
              assign v-error = "Invalid selection for Piping Material.".
              run zsdi_vaerrx.p(v-error,"yes").
              next-prompt v-pipematerial with frame f8-total.
              next.
            END.
            
            FIND first zsdivaspmap where
                       zsdivaspmap.cono = g-cono and
                       zsdivaspmap.rectype = "GO" and
                                 /* Addon Global Options */ 
                       zsdivaspmap.prod    = x-quoteno and
                       zsdivaspmap.seqno   = 0 and
                       zsdivaspmap.lineno  = 0 and
                       zsdivaspmap.typename = "Piping Style" and
                       zsdivaspmap.description = v-pipestyle 
            NO-LOCK NO-ERROR.

            IF not avail zsdivaspmap then do:
              assign v-error = "Invalid selection for Piping Style.".
              run zsdi_vaerrx.p(v-error,"yes").
              next-prompt v-pipematerial with frame f8-total.
              next.
            END.
             
            run evaluate-pipe-change (input v-pipematerial,
                                      input v-pipestyle,
                                      output v-changefl).
            IF v-changefl then do:               
              b-totals:refresh().
/* exercise to keep hilite off browse. */
              disable b-totals with frame f8-total.
              enable  b-totals with frame f8-total.
/* reacquire new values */
              FIND zsdivasp WHERE
                   zsdivasp.cono = g-cono         AND
                   zsdivasp.rectype = "fb"        AND
                   zsdivasp.repairno = x-quoteno 
              NO-LOCK NO-ERROR.


              assign
                p-pct         = input p-pct
                v-Intangables = zsdivasp.Intangtotl
                v-invcost     = zsdivasp.invcost
                v-Laborcost   = zsdivasp.Labortotl
                v-rebate      = zsdivasp.user8 * -1
                s-FabTotalCost    = v-Intangables + v-invcost + 
                                v-Laborcost + v-rebate
                s-trendtot    = s-FabTotalCost * iv-addon
                s-override  = IF zsdivasp.override NE "" THEN
                                "*"
                              ELSE
                                " "
                s-FabTotalPrice  = 
                  if zsdivasp.override ne "" then
                    zsdivasp.sellprc
                  else
                  if  round(((s-trendtot / (100 - p-pct)) * 100),2) 
                      - ((s-trendtot / (100 - p-pct) * 100)) <> 0  then
                    round(((s-trendtot / (100 - p-pct)) * 100),2)
                     + .01    
                  else
                    round(((s-trendtot / (100 - p-pct)) * 100),2).
              DISPLAY 
                s-vareqshipdt
                s-vapromisedt
                v-invcost
                v-Intangables
                v-Laborcost
                v-rebate
                s-FabTotalCost
                s-override
                s-trendtot
                s-maxlead
                p-assigned
                p-approved
                p-pct
                s-FabTotalPrice
                v-pipematerial
                v-pipestyle
              WITH FRAME f8-total.

            if frame-field = "v-pipestyle" then
              apply "entry" to b-totals in frame f8-total.
 
            
            END.
          END.
          apply lastkey.
        END. /* Editing Loop */

        ASSIGN f8-title =
" F6-ExtNotes F9-PrtQuote                                                     ".
       
        DISPLAY f8-title WITH FRAME f8-total.
          
        FIND FIRST vasp WHERE
                   vasp.cono = g-cono         AND
                   vasp.shipprod = x-quoteno  
        NO-ERROR.

        FIND  zsdivasp WHERE
              zsdivasp.cono = g-cono         AND
              zsdivasp.rectype = "fb"        AND
              zsdivasp.repairno = x-quoteno  
        NO-ERROR.

        FIND first zy-sasos where 
                   zy-sasos.cono   = g-cono and 
                   zy-sasos.oper2  = g-operinits and 
                   zy-sasos.menuproc = "xpds" 
        NO-LOCK NO-ERROR.
                           
        IF avail zy-sasos then 
          assign zsdi-lvl = zy-sasos.securcd[6]. 
        else  
          assign zsdi-lvl = 1.
      
        assign zsdi-pdsclvl = zi-pdscrec.
        assign zsdi-chgpct = round(((s-FabTotalPrice - s-trendtot) 
                               / s-FabTotalPrice) * 100,2).
        assign 
          v-oldmarg     = p-pct /* zsdi-todaypct  */
          v-chgmarg     = zsdi-chgpct     
          v-lvl         = zsdi-lvl      
          v-type        = zsdi-prctyp    
          v-nstyp       = (if zsdi-pdsclvl = 0 then 
                             "n"
                           else
                             " " )  
          v-oetyp       = "so"   
          v-errmsg      = "".
       
        IF /* tahtrain */  avail zxt2-sasos and
          zxt2-sasos.securcd[6] >= 2 and v-lvl < 5  then do:
           run zsdiptypchk.p(input v-slsrepout,
                            input v-custno,
                            input v-shipto,
                            input v-oldmarg,
                            input v-chgmarg,
                            input v-lvl,
                            input v-prod,
                            input x-whse,     /* tahtrain was v-whse */
                            input v-type,
                            input v-nstyp,
                            input "so",
                            input-output v-errfl2,
                            input-output v-errmsg).           
          IF v-errfl2 <> 0 then do:
            assign s-FabTotalPrice =  if zsdivasp.override ne "" then
                                    zsdivasp.sellprc
                                  else
                                 if  
                                 round(((s-trendtot / (100 - p-pct)) * 100),2) -
                                   ((s-trendtot / (100 - p-pct) * 100)) <> 0 
                                 then
                                  round(((s-trendtot / (100 - p-pct)) * 100),2)
                                    +  .01 
                                 else
                                   round(((s-trendtot / (100 - p-pct)) 
                                         * 100),2).

            run zsdi_vaerrx.p(substring(v-errmsg,13,80),"yes").  
            if frame-field = "s-FabTotalPrice" then
              next-prompt s-FabTotalPrice with frame f8-total.
            else 
            if frame-field = "f8-leadtm" then
              next-prompt s-FabTotalPrice with frame f8-total.
            else
              next-prompt s-FabTotalPrice with frame f8-total.
            next.   
          END.  /* v-errfl2 <> 0 */ 

        END.  /* if lvl < 5 */                
   
        IF /* tahtrain */ avail zxt2-sasos and
           zxt2-sasos.securcd[6] >= 2 and v-lvl < 5 then do:
          assign   v-oldmarg     = zsdi-chgpct 
                   v-chgmarg     = p-pct.     
          run zsdiptypchk.p(input v-slsrepout,
                            input v-custno,
                            input v-shipto,
                            input v-oldmarg,
                            input v-chgmarg,
                            input v-lvl,
                            input v-prod,
                            input x-whse,        /* tahtrain was v-whse */
                            input v-type,
                            input v-nstyp,
                            input "so",
                            input-output v-errfl2,
                            input-output v-errmsg).           
          IF v-errfl2 <> 0 then do:
            assign p-pct = zsdi-chgpct.
            run zsdi_vaerrx.p(substring(v-errmsg,13,80),"yes").  
            next-prompt p-pct with frame f8-total.
            next. /*  totloop.  */
          END.
        END.  /* sasos */
/*
        IF s-FabTotalPrice <= 0 then do:
          assign v-error = "Price cannot be zero " +
                           "- Warning price is not recalculated .".
          run zsdi_vaerrx.p(v-error,"yes").
          next totloop.
        END.
*/

        IF (zsdivasp.user4 <> s-vareqshipdt or
            zsdivasp.user5 <> s-vapromisedt or
            zsdivasp.margpct    NE p-pct    or
            zsdivasp.override <> s-override or
            zsdivasp.sellprc <> s-FabTotalPrice  ) then do:
          ASSIGN 
            zsdivasp.user4   = s-vareqshipdt 
            zsdivasp.user5   = s-vapromisedt 
            zsdivasp.margpct  = p-pct
            zsdivasp.override = s-override 
            zsdivasp.sellprc = s-FabTotalPrice.
        END.      
        
        leave.
      END. /* totloop */

      IF avail vasp THEN
        RELEASE vasp.
   
      IF LASTKEY = 404 OR LASTKEY = 311 THEN DO:
        IF LASTKEY = 404 THEN DO:
          ASSIGN 
            v-lmode           = "c"
            /*
            v-framestate      = "m1"
            v-currentkeyplace = 3 */ .
        END.
  
        HIDE FRAME f8-total.
        VIEW FRAME f-top1.
        DISPLAY b-lines WITH FRAME f-lines.
        APPLY x-cursorup TO b-lines IN FRAME f-lines. 
        PUT SCREEN ROW 22 COL 1 COLOR MESSAGE
"  F6-Top  F7-Lines  F8-Totals  F9-SPcNotes                                   ".
        LEAVE.
      END.
      ELSE
      IF {k-func6.i} THEN DO:
        IF p-approved AND zsdiupdated THEN
          RUN f6-global.

        HIDE FRAME f-mid1.
        RUN f6-comments.
        PAUSE 0.
        IF LASTKEY = 401 THEN DO:
          IF avail vasp AND
           (zsdivasp.user4 <> s-vareqshipdt or
            zsdivasp.user5 <> s-vapromisedt or
            zsdivasp.margpct    NE p-pct OR
            zsdivasp.cost       NE s-vaprodcost) then do:
         
            FIND FIRST vasp WHERE
                       vasp.cono = g-cono         AND
                       vasp.shipprod = x-quoteno  
            NO-ERROR.

            FIND zsdivasp WHERE
                 zsdivasp.cono = g-cono         AND
                 zsdivasp.rectype = "fb"        AND
                 zsdivasp.repairno = x-quoteno  
            NO-ERROR.
   
            ASSIGN 
              zsdivasp.user4 = s-vareqshipdt 
              zsdivasp.user5 = s-vapromisedt 
              zsdivasp.margpct           = p-pct.
            RELEASE vasp.
          END.
        END.
    
        ASSIGN f8-title =
" F6-ExtNotes F9-PrtQuote                                                    ".
 
        DISPLAY f8-title WITH FRAME f8-total.
        NEXT.
      END.
      ELSE
      IF {k-func9.i} THEN DO:
        ASSIGN f8-title =
" F6-ExtNotes F9-PRTQUOTE                                                    ".
        DISPLAY f8-title WITH FRAME f8-total.
        IF p-approved AND zsdiupdated THEN
          RUN f6-global.
        DISPLAY 
          s-vareqshipdt
          s-vapromisedt
          v-invcost
          v-Intangables
          v-Laborcost
          v-rebate
          s-FabTotalCost
          s-override
          p-pct
          s-maxlead
        WITH FRAME f8-total.

        {w-sasoo.i g-operinits NO-LOCK}
        IF avail sasoo THEN DO:
          ASSIGN s-printernm = sasoo.printernm.
          RELEASE sasoo.
        END.
    
        prtloop:
        DO WHILE TRUE ON ENDKEY UNDO, LEAVE prtloop:
          UPDATE s-printernm WITH FRAME f-printer2
          EDITING:
            READKEY.
/* trap f4(cancel) so updates will take on f1 then f4 */
            IF {k-cancel.i} THEN DO:
              LEAVE prtloop.
            END.
            ELSE
              APPLY LASTKEY.
          END. /* editing */.
            
          IF (s-printernm = "email" OR s-printernm = "e-mail" OR
          s-printernm = "fax") THEN DO:
            ASSIGN 
              sendit   = NO
              g-vendno = 0.
            DEF VAR x-printernm LIKE s-printernm.
            ASSIGN 
              x-printernm = s-printernm
              sendtyp     = s-printernm.
            RUN vaexf90sdi.p(INPUT "svcq",
                             INPUT-OUTPUT x-printernm,
                             INPUT-OUTPUT g-vendno,
                             INPUT-OUTPUT sendtyp,
                             INPUT-OUTPUT sendit).
            IF NOT sendit THEN
              PAUSE 0.
            ELSE
            IF sendit THEN DO:
              IF sendtyp = "e-mail" OR
                 sendtyp = "email" THEN
                STATUS DEFAULT
                 "E-mailing Fab quote " + g-fabquoteno + " in progress..".
              ELSE
              IF sendtyp = "fax" THEN
                STATUS DEFAULT
                 "Faxing Fab quote " + g-fabquoteno + " in progress..".
  
              ASSIGN x-printernm = sendtyp.
              RUN prt-routine2(INPUT x-printernm,INPUT NO).
            END.
            LEAVE prtloop.
          END.

          {w-sasp.i s-printernm}
          IF NOT avail sasp THEN DO:
            ASSIGN v-error = "Invalid Printer - Re-enter".
            RUN zsdi_vaerrx.p(v-error,"yes").
            NEXT.
          END.
          IF NUM-ENTRIES(g-fabquoteno,"-") = 2 AND
          (ENTRY(1,g-fabquoteno,"-") = "" OR
          ENTRY(2,g-fabquoteno,"-") = "") THEN DO:
            ASSIGN v-error = "Invalid quote No. - Re-enter".
            HIDE FRAME f-printer2.
            HIDE FRAME f8-total.
            NEXT.
          END.
 
          ASSIGN sendit = NO.
            
          IF s-printernm NE "vid" THEN
            RUN prt-routine2(INPUT s-printernm,INPUT NO).
 
          LEAVE prtloop.
        END. /* end do while prtloop  */

        STATUS DEFAULT "                                                    ".
        PAUSE 0.
        DISPLAY WITH FRAME f-top1.
        ASSIGN f8-title =
" F6-ExtNotes F9-PrtQuote                                                    ".
        DISPLAY f8-title WITH FRAME f8-total.
        NEXT.
      END. /* F9 */
      ELSE do:
        
      END.
        
      IF LASTKEY = 401 THEN do:
        LEAVE f8loop.
      END.
    END. /* repeat */

    IF avail vasp THEN
      RELEASE vasp.

    DISPLAY b-lines WITH FRAME f-lines.
    /*
    ASSIGN 
      v-framestate = "m1"
      v-CurrentKeyPlace = 1.
    */
    
    readkey pause 0.

    ON cursor-up cursor-up.
    ON cursor-down cursor-down.
    HIDE FRAME f8-total.
    VIEW FRAME f-top1.
    IF o-framenm = "f-top1" THEN DO:
      HIDE FRAME f-lines.
      DISPLAY fs-1 WITH FRAME f-statusline.
    END.
    LEAVE.
  END. /* Procedure F8Totals */



/*------------------------------------------------------------------------ */   
PROCEDURE Build-Material-Browse:
/*------------------------------------------------------------------------ */   

  for each t-pipemat:
    delete t-pipemat.
  end.   

  for each t-pipestyle:
    delete t-pipestyle.
  end.   
   
  FOR EACH zsdivaspmap where
           zsdivaspmap.cono    = g-cono and
           zsdivaspmap.rectype = "GO" and        /* GLobal Options */
           zsdivaspmap.prod    = x-quoteno and
           zsdivaspmap.seqno   = 0 and
           zsdivaspmap.lineno  = 0 and
           zsdivaspmap.typename = "Piping Material" no-lock:

    create t-pipemat.
    assign t-pipemat.typename     = zsdivaspmap.typename
           t-pipemat.pdescription  = zsdivaspmap.description
           t-pipemat.cdefault     = zsdivaspmap.user1
           t-pipemat.odefault     = zsdivaspmap.user1
           t-pipemat.zrecid       = recid(zsdivaspmap).
     
      
    if zsdivaspmap.user1 = "yes" then do:
      assign v-pipematerial = zsdivaspmap.description.
    end.
  end.    
        
  FOR EACH zsdivaspmap where
           zsdivaspmap.cono    = g-cono and
           zsdivaspmap.rectype = "GO" and        /* GLobal Options */
           zsdivaspmap.prod    = x-quoteno and
           zsdivaspmap.seqno   = 0 and
           zsdivaspmap.lineno  = 0 and
           zsdivaspmap.typename = "Piping Style" no-lock:
    create t-pipestyle.
    assign t-pipestyle.typename      = zsdivaspmap.typename
           t-pipestyle.pdescription  = zsdivaspmap.description
           t-pipestyle.cdefault      = zsdivaspmap.user1
           t-pipestyle.odefault      = zsdivaspmap.user1
           t-pipestyle.zrecid        = recid(zsdivaspmap).
      
      
    if zsdivaspmap.user1 = "yes" then do:
      assign v-pipestyle = zsdivaspmap.description.
    end.
  end.    
             
END. /* Procedure */


/* --------------------&&&-------------------------------------------------- */
PROCEDURE DoPipeMatLu:
/* ------------------------------------------------------------------------- */
  DEFINE INPUT PARAMETER ix-pipemat AS CHARACTER FORMAT "x(24)" NO-UNDO.
  
  OPEN QUERY q-pipemat  FOR EACH t-pipemat NO-LOCK.
  ASSIGN  
    v-pipematbrowseopen = TRUE.
  DISPLAY b-pipemat WITH FRAME f-pipemat.
  
  ON cursor-up cursor-up.
  ON cursor-down cursor-down.
  ENABLE b-pipemat WITH FRAME f-pipemat.
  
  wait-for window-close of current-window.
  pause.
  on cursor-up back-tab.
  on cursor-down tab.
  close query q-pipemat.

   
  END.
 

/* -------------------------------------------------------------------------  */
PROCEDURE MovePipeMat:
/* -------------------------------------------------------------------------  */
  DEFINE INPUT PARAMETER ix-lastkey AS INTEGER                 NO-UNDO.
  
  ON cursor-up cursor-up.
  ON cursor-down cursor-down.
  ENABLE b-pipemat WITH FRAME f-pipemat.
  APPLY LASTKEY TO b-pipemat IN FRAME f-pipemat.
  IF LASTKEY = 502 AND v-pipematbrowsekeyed = FALSE THEN DO:
    APPLY x-cursorup TO b-pipemat IN FRAME f-pipemat.
  END.
  APPLY "focus" TO v-pipematerial IN FRAME f8-total.
  
END.

/*------------------------------------------------------------------------ */   Procedure Evaluate-Pipe-Change: 
/*------------------------------------------------------------------------ */    define input  parameter ip-pipematerial as char format "x(15)" no-undo.
  define input  parameter ip-pipestyle    as char format "x(15)"  no-undo. 
  define output parameter ip-changed    as logical               no-undo.    

  define var iv-pipingStyle       as dec  format ">>>9.999"   no-undo.
  define var iv-pipingStylety     as logical no-undo.
  define var iv-pipingMaterial    as dec format ">>>9.999"     no-undo.
  define var iv-pipingMaterialty  as logical no-undo.
  define var iv-change            as logical no-undo.
  define var iv-newaddon          as dec format ">>>9.999"     no-undo.



  define buffer b-igl-zsdivaspmap for zsdivaspmap.
  define buffer b-igo-zsdivaspmap for zsdivaspmap.  
  define buffer b-is-zsdivaspmap for zsdivaspmap.  
  define buffer b-il-zsdivaspmap for zsdivaspmap.  
  define buffer b-i-vaspsl       for vaspsl.  
  define buffer b-i-vasps        for vasps.  

  define buffer b-it-vaspmap     for t-vaspmap.  

  find last bl-t-lines use-index ix1 no-lock no-error.
  
  


  
  assign iv-change = false 
         ip-changed = false  /* before updating make sure a change occured */
         iv-newaddon = 0.
  find first b-igo-zsdivaspmap where                                       
             b-igo-zsdivaspmap.cono = g-cono and                                              b-igo-zsdivaspmap.rectype = "GO" and  /* Addon Global Options */ 
             b-igo-zsdivaspmap.prod    = x-quoteno and
             b-igo-zsdivaspmap.seqno   = 0 and
             b-igo-zsdivaspmap.lineno  = 0 and
             b-igo-zsdivaspmap.typename = "Piping Material" and
             b-igo-zsdivaspmap.description = ip-pipematerial and

             b-igo-zsdivaspmap.user1    =  "Yes" no-lock no-error.
  if avail b-igo-zsdivaspmap then do:
  end.
  else do:
    assign iv-change = true.

    find first b-igo-zsdivaspmap where                                       
               b-igo-zsdivaspmap.cono = g-cono and                                              b-igo-zsdivaspmap.rectype = "GO" and  /* Addon Global Options */                b-igo-zsdivaspmap.prod    = x-quoteno and
               b-igo-zsdivaspmap.seqno   = 0 and
               b-igo-zsdivaspmap.lineno  = 0 and
               b-igo-zsdivaspmap.typename = "Piping Material" and
               b-igo-zsdivaspmap.user1  = "yes" no-error.

    if avail b-igo-zsdivaspmap then do:
      assign b-igo-zsdivaspmap.user1 = "No".

       find first b-igo-zsdivaspmap where                                       
                  b-igo-zsdivaspmap.cono = g-cono and                                              b-igo-zsdivaspmap.rectype = "GO" and  
                  /* Addon Global Options */  
                  b-igo-zsdivaspmap.prod    = x-quoteno and
                  b-igo-zsdivaspmap.seqno   = 0 and
                  b-igo-zsdivaspmap.lineno  = 0 and
                  b-igo-zsdivaspmap.typename = "Piping Material" and
                  b-igo-zsdivaspmap.description = ip-pipematerial and
                  b-igo-zsdivaspmap.user1    =  "No"  no-error.
       if avail b-igo-zsdivaspmap then do:
         assign b-igo-zsdivaspmap.user1 = "Yes".
       end.  
    end.
  end.
 
  
  find first b-igo-zsdivaspmap where                                       
             b-igo-zsdivaspmap.cono = g-cono and                                            b-igo-zsdivaspmap.rectype = "GO" and  /* Addon Global Options */ 
             b-igo-zsdivaspmap.prod    = x-quoteno and
             b-igo-zsdivaspmap.seqno   = 0 and
             b-igo-zsdivaspmap.lineno  = 0 and
             b-igo-zsdivaspmap.typename = "Piping Style" and
             b-igo-zsdivaspmap.description = ip-pipestyle and

             b-igo-zsdivaspmap.user1    =  "Yes" no-lock no-error.
  if avail b-igo-zsdivaspmap then do:
  end.
  else do:
    assign iv-change = true.

    find first b-igo-zsdivaspmap where                                       
               b-igo-zsdivaspmap.cono = g-cono and                                            b-igo-zsdivaspmap.rectype = "GO" and  
                 /* Addon Global Options */ 
               b-igo-zsdivaspmap.prod    = x-quoteno and
               b-igo-zsdivaspmap.seqno   = 0 and
               b-igo-zsdivaspmap.lineno  = 0 and
               b-igo-zsdivaspmap.typename = "Piping Style" and
               b-igo-zsdivaspmap.user1  = "yes" no-error.

    if avail b-igo-zsdivaspmap then do:
      assign b-igo-zsdivaspmap.user1 = "No".

       find first b-igo-zsdivaspmap where                                       
                  b-igo-zsdivaspmap.cono = g-cono and                                            b-igo-zsdivaspmap.rectype = "GO" and  
                  /* Addon Global Options */  
                  b-igo-zsdivaspmap.prod    = x-quoteno and
                  b-igo-zsdivaspmap.seqno   = 0 and
                  b-igo-zsdivaspmap.lineno  = 0 and
                  b-igo-zsdivaspmap.typename = "Piping Style" and
                  b-igo-zsdivaspmap.description = ip-pipestyle and
                  b-igo-zsdivaspmap.user1    =  "No"  no-error.
       if avail b-igo-zsdivaspmap then do:
         assign b-igo-zsdivaspmap.user1 = "Yes".
       end.  
    end.
  end.

  if iv-change = true then do:
  
    if avail bl-t-lines and bl-t-lines.lineno > 20 then
      message "Recalculating line item Addons.....".
    
    find first b-igo-zsdivaspmap where
         b-igo-zsdivaspmap.cono = g-cono and
         b-igo-zsdivaspmap.rectype = "GO" and     /* Addon Global Options */
         b-igo-zsdivaspmap.prod    = x-quoteno and
         b-igo-zsdivaspmap.seqno   = 0 and
         b-igo-zsdivaspmap.lineno  = 0 and
         b-igo-zsdivaspmap.typename = "Piping Material" and
         b-igo-zsdivaspmap.user1    = "yes"    no-lock no-error.
    if not avail b-igo-zsdivaspmap then
      assign iv-pipingmaterial = 1
             iv-pipingmaterialty = true.
    else  do:
      assign iv-pipingmaterial = b-igo-zsdivaspmap.addon[1]
             iv-pipingmaterialty = b-igo-zsdivaspmap.addonty.
    end.
    find first b-igo-zsdivaspmap where
         b-igo-zsdivaspmap.cono = g-cono and
         b-igo-zsdivaspmap.rectype = "GO" and     /* Addon Global Options */
         b-igo-zsdivaspmap.prod    = x-quoteno and
         b-igo-zsdivaspmap.seqno   = 0 and
         b-igo-zsdivaspmap.lineno  = 0 and
         b-igo-zsdivaspmap.typename = "Piping Style" and
         b-igo-zsdivaspmap.user1    = "yes"    no-lock no-error.
    if not avail b-igo-zsdivaspmap then
      assign iv-pipingStyle = 1
             iv-pipingStylety = true.
    else do:
      assign iv-pipingStyle = b-igo-zsdivaspmap.addon[1]
             iv-pipingStylety = b-igo-zsdivaspmap.addonty.
    end.         
  
    find b-is-zsdivaspmap where
         b-is-zsdivaspmap.cono = g-cono and
         b-is-zsdivaspmap.rectype = "S" and        /* VAES */
         b-is-zsdivaspmap.prod   = x-quoteno and
         b-is-zsdivaspmap.description = "ADDON" and
         b-is-zsdivaspmap.typename    = 
                  "Intangables" no-lock  no-error.
    if avail b-is-zsdivaspmap then do:
      find b-il-zsdivaspmap where
           b-il-zsdivaspmap.cono = g-cono and
           b-il-zsdivaspmap.rectype = "L" and        /* VAESL */
           b-il-zsdivaspmap.prod   = x-quoteno and
           b-il-zsdivaspmap.typename    = "Fittings"  no-error.
      if avail b-il-zsdivaspmap then do:
        if iv-pipingStylety then do:
          
          assign b-il-zsdivaspmap.addon[1] = 
                 iv-pipingstyle * iv-pipingmaterial
                 b-il-zsdivaspmap.addonty = iv-pipingstylety 
                iv-newaddon = 
                 iv-pipingstyle * iv-pipingmaterial
                 b-il-zsdivaspmap.addonty = iv-pipingstylety. 
        end.
        else do:    
          assign b-il-zsdivaspmap.addon[1] = 
                   iv-pipingstyle + iv-pipingmaterial
                   b-il-zsdivaspmap.addonty = iv-pipingstylety 
                 iv-newaddon = 
                   iv-pipingstyle + iv-pipingmaterial
                   b-il-zsdivaspmap.addonty = iv-pipingstylety. 
  
         end.
      end.
    end.        

  /* update the Temp table then the Database */
    
/* Locate Inventory section from Template */    
    find b-igo-zsdivaspmap where
         b-igo-zsdivaspmap.cono = g-cono and
         b-igo-zsdivaspmap.rectype  = "S" and        /* VAES */
         b-igo-zsdivaspmap.prod     = x-quoteno  and
         b-igo-zsdivaspmap.typename = "INVENTORY" and
         b-igo-zsdivaspmap.lineno = 0  no-lock  no-error.
/* REMINDER INDEX NEEDED */         
    for each b-it-vaspmap where
         b-it-vaspmap.cono = g-cono and
         b-it-vaspmap.rectype = "LQ" and        /* VAESL */
         b-it-vaspmap.prod   = x-quoteno and
         b-it-vaspmap.typename    = "Fittings" and
         b-it-vaspmap.seqno       = b-igo-zsdivaspmap.seqno and
         b-it-vaspmap.overridety <> "y": /* don't fix overrided value */
      /* Don't override the already set manually */
      if b-it-vaspmap.overridety = "" then
        assign b-it-vaspmap.addon[1] = iv-newaddon.
      find b-i-vaspsl where b-i-vaspsl.cono = g-cono                   and
                            b-i-vaspsl.shipprod = x-quoteno            and
                            b-i-vaspsl.whse     = x-whse             and
                            b-i-vaspsl.seqno    = b-it-vaspmap.seqno and
                            b-i-vaspsl.lineno   = b-it-vaspmap.lineno  and
                            b-i-vaspsl.nonstockty <> "l"
                            no-lock no-error.
      if not avail b-i-vaspsl then 
        next.  /* blank like in the buffer or lost buisness */

      RUN VaspMap-Update (input "updt",
                          input b-it-vaspmap.seqno,
                          input b-it-vaspmap.lineno,
                          input b-i-vaspsl.qtyneeded).
 
      
      run ZsdiVaspMap-update (input "c",
                              input recid(b-i-vaspsl)).
 
    
/*    Fix up b-totals query for the totals screen if there */          
      find b-i-vasps where b-i-vasps.cono = g-cono                   and
                           b-i-vasps.shipprod = x-quoteno            and
                           b-i-vasps.whse     = x-whse               and
                           b-i-vasps.seqno    = int(b-it-vaspmap.user3) 
                           no-lock no-error.
       
      if avail b-i-vasps then
        find b-i-vaspsl where b-i-vaspsl.cono = g-cono                   and
                              b-i-vaspsl.shipprod = x-quoteno            and
                              b-i-vaspsl.whse     = x-whse               and
                              b-i-vaspsl.seqno    = int(b-it-vaspmap.user3) and
                              b-i-vaspsl.lineno   = int(b-it-vaspmap.user4)
                              no-lock no-error.
      if avail b-i-vaspsl and avail b-i-vasps then do:
        find first t-totals use-index k2 where
                   t-totals.seqno    = b-i-vaspsl.seqno         and
                   t-totals.sctntype = caps(b-i-vasps.sctntype)   and
                   t-totals.lineno   = b-i-vaspsl.lineno        and
                   t-totals.product  = caps(b-i-vaspsl.compprod) no-error.
      
        if avail t-totals and b-i-vasps.sctntype = "in" then do:
          assign t-totals.dvalue = 
               string(b-i-vaspsl.qtyneeded,">>>>>>9.99-") + " " +
               string(b-i-vaspsl.qtyneeded * b-i-vaspsl.prodcost,">>>>>>9.99-").

                  
           FIND  b-igl-zsdivaspmap where
                 b-igl-zsdivaspmap.cono = g-cono and
                 b-igl-zsdivaspmap.rectype     = "L" and        /* VAESl */
                 b-igl-zsdivaspmap.prod        = x-quoteno  and
                 b-igl-zsdivaspmap.typename    = t-totals.typename and
                 b-igl-zsdivaspmap.seqno       = t-totals.seqno  NO-LOCK.
           IF AVAIL b-igl-zsdivaspmap THEN DO:
             assign t-totals.addonty  = b-igl-zsdivaspmap.addonty
                    t-totals.addon[1] = b-igl-zsdivaspmap.addon[1].
           END.

       
       
       
         assign ip-changed = iv-change.      
        
        end.
      end.
    end.
  
  end. /* changed */


end.
/*------------------------------------------------------------------------ */   
  PROCEDURE CustomerChange:
/*------------------------------------------------------------------------ */   
    DEFINE INPUT-OUTPUT PARAMETER ip-chgfl AS LOGICAL FORMAT "yes/no" NO-UNDO.
    ASSIGN ip-chgfl = NO.
    chgcust:
    DO ON ENDKEY UNDO chgcust, LEAVE chgcust:
      UPDATE ip-chgfl LABEL
      "You are changing the customer on this quote.  Do you want to continue?"
      WITH FRAME f-chg SIDE-LABELS ROW 4 CENTERED OVERLAY.
      IF KEYFUNCTION(LASTKEY) = "GO" OR
         KEYFUNCTION(LASTKEY) = "RETURN" THEN DO:
        HIDE FRAME f-chg NO-PAUSE.
        LEAVE chgcust.
      END.
    END. /* chgcust */
    IF ip-chgfl THEN DO:
      FIND b-arsc WHERE
           b-arsc.cono = g-cono AND
           b-arsc.custno = o-custno NO-LOCK NO-ERROR.
      ASSIGN 
        v-custnoa   = STRING(v-custno)
        o-custno    = v-custno
        v-custname  = arsc.NAME
        v-custpo    = /* if v-custpo = "" then
                        arsc.custpo
                      else */
                        v-custpo
        
        promptpos   = 5
        leavefl     = NO
        v-framestate = "t1".
      DISPLAY 
        v-custnoa
        v-custname
        v-custpo
      WITH FRAME f-top1.
        
    END.
    ELSE DO:
      FIND b-arsc WHERE
        b-arsc.cono = g-cono AND
        b-arsc.custno = o-custno NO-LOCK NO-ERROR.
      ASSIGN 
        v-custnoa   = STRING(o-custno)
        v-custno    = o-custno
        v-custname  = arsc.NAME
        promptpos   = 4
        leavefl     = NO
        v-framestate = "t1".
      DISPLAY 
        v-custnoa
        v-custname
      WITH FRAME f-top1.
    END.
  END.
  
PROCEDURE ProductChange:
  DEFINE INPUT-OUTPUT PARAMETER ip-chgfl AS LOGICAL FORMAT "yes/no" NO-UNDO.
  
  DEFINE BUFFER bx-vasp for vasp.
  DEFINE BUFFER bx-zsdivasp for zsdivasp.
  
  DEF VAR ix-whse-okfl AS LOGICAL NO-UNDO.
  
  ASSIGN ip-chgfl = NO.
  chgprod:
  DO ON ENDKEY UNDO chgprod, LEAVE chgprod:
    UPDATE ip-chgfl LABEL
    "You are changing the Part# on this quote.  Do you want to continue?"
      WITH FRAME f-chg SIDE-LABELS ROW 6 CENTERED OVERLAY.
      IF KEYFUNCTION(LASTKEY) = "GO" OR
      KEYFUNCTION(LASTKEY) = "RETURN" THEN DO:
        HIDE FRAME f-chg NO-PAUSE.
        LEAVE chgprod.
      END.
    END. /* chgprod */
    
    IF ip-chgfl THEN DO:
      ASSIGN o-partno    = s-prod
      promptpos   = 10
      leavefl     = NO
      v-framestate = "t1".
      
      {w-icsw.i s-prod x-whse NO-LOCK}
      
      IF NOT avail icsw THEN DO:
        ASSIGN ix-whse-okfl = NO.
        /** copy from main whse - if not at default whse  **/
        
        FIND FIRST icsw WHERE
                   icsw.cono = 90     AND
                   icsw.prod = s-prod AND
                   icsw.whse = "main" NO-LOCK NO-ERROR.
        IF NOT avail icsw THEN DO:
          /** copy from a whse - if not at default whse  **/
          ASSIGN ix-whse-okfl = NO.
          RUN Create_ICSW(INPUT x-whse,INPUT s-prod,INPUT-OUTPUT ix-whse-okfl).
        END.
        ELSE DO:
          RUN zsdimaincpy.p(INPUT g-cono,
          INPUT " ",
          /*** to whse = x-whse ***/
          INPUT x-whse,
          INPUT s-prod,
          INPUT 0,
          INPUT-OUTPUT ix-whse-okfl).
        END.
      END.
      {w-icsw.i s-prod x-whse NO-LOCK}
      DISPLAY s-prod WITH FRAME f-top1.
/* tahtrain  */   
      
    FIND FIRST bx-zsdivasp USE-INDEX  k-repairno  WHERE
               bx-zsdivasp.cono     = g-cono  AND
               bx-zsdivasp.rectype  = "fb"    AND
               bx-zsdivasp.repairno = x-quoteno
    NO-ERROR.
    FIND FIRST bx-vasp USE-INDEX k-vasp WHERE
               bx-vasp.cono     = g-cono  AND
               bx-vasp.shipprod = x-quoteno   
    NO-ERROR.

    if avail bx-vasp and avail bx-zsdivasp then
      assign bx-vasp.refer = s-prod
             bx-zsdivasp.partno = bx-vasp.refer.
             
/*  tahtrain */    
    
    
    END.
    ELSE DO:
      ASSIGN s-prod    = o-partno
      promptpos   = 10
      leavefl     = NO
      v-framestate = "t1".
      DISPLAY s-prod WITH FRAME f-top1.
      HIDE FRAME f-chg.
    END.
    ASSIGN v-astr-top = "".
    RUN NoteDisplay3(INPUT s-prod).
    ASSIGN v-astr-top = v-astr3.
    DISPLAY v-astr-top WITH FRAME f-top1.
  END.
  
  /* Cut up Top1 into procedures to save segment space */
/*------------------------------------------------------------------------ */     PROCEDURE sProductcheck:
/*------------------------------------------------------------------------ */       DEF VAR ix-whse-okfl AS LOGICAL NO-UNDO.
    {w-icsw.i s-prod x-whse NO-LOCK}
    
    IF NOT avail icsw THEN DO:
      ASSIGN ix-whse-okfl = NO.
      /** copy from main whse - if not at default whse  **/
      
      FIND FIRST icsw WHERE
                 icsw.cono = 90     AND
                 icsw.prod = s-prod AND
                 icsw.whse = "main" NO-LOCK NO-ERROR.
      IF NOT avail icsw THEN DO:
        /** copy from a whse - if not at default whse  **/
        ASSIGN ix-whse-okfl = NO.
        RUN Create_ICSW(INPUT x-whse,INPUT s-prod,INPUT-OUTPUT ix-whse-okfl).
      END.
      ELSE DO:
        RUN zsdimaincpy.p(INPUT g-cono,
                          INPUT " ",
                          /*** to whse = x-whse ***/
                          INPUT x-whse,
                          INPUT s-prod,
                          INPUT 0,
                          INPUT-OUTPUT ix-whse-okfl).
      END.
    END.
    {w-icsw.i s-prod x-whse NO-LOCK}
  END.
/*------------------------------------------------------------------------ */     PROCEDURE DisplayTop1:
/*------------------------------------------------------------------------ */       DISPLAY  
      x-whse
      v-revno 
      v-stage
      v-tagdt
      v-custnoa
      v-custpo
      v-binloc
      v-custname
      v-takenby
      v-shipto
      v-model
      s-prod
      v-astr-top
      s-LastFabNo 

    WITH FRAME f-top1.
  END.
/*------------------------------------------------------------------------ */     PROCEDURE DisplayMid1:
/*------------------------------------------------------------------------ */       
    define buffer ib-icsw for icsw.
    define buffer iv-icsw for icsw.
 
    find iv-icsw where iv-icsw.cono = g-cono and
                       iv-icsw.whse = v-whse and
                       iv-icsw.prod = v-prod no-lock no-error.
     
    find ib-icsw where ib-icsw.cono = g-cono and
                       ib-icsw.whse = x-whse and
                       ib-icsw.prod = v-prod no-lock no-error.
    if avail ib-icsw then 
      assign v-replcst = ib-icsw.replcost.
    else                        
      assign v-replcst = 0.
    
    DISPLAY 
      v-linelit   FORMAT "x(78)"
      v-lineno
      v-specnstype
      v-prod
      v-attrfl
      v-astr2
      v-qty
      IF v-whse NE x-whse THEN
        v-whse
      ELSE
        "" @ v-whse
      v-prodcost
      v-replcst
      v-descrip
      v-leadtm
      v-vendno
      v-vendnm
      IF v-whse NE "" AND
         v-whse NE x-whse THEN
        v-astr1
      ELSE
        "" @ v-astr1
    WITH FRAME f-mid1.
/* tahtrain
    if avail iv-icsw and 
/*      OEEXQ 
        ix-qtyavail = 0 and (ix-avgltdt < (TODAY - 183) or ix-avgltdt = ?) 
*/
      (iv-icsw.avgltdt < (today - 183) or iv-icsw.avgltdt = ?) then
tahtrain replaced with below */
         
    if avail ib-icsw and 
      (ib-icsw.avgltdt < (today - 183) or ib-icsw.avgltdt = ?) then
         v-leadtm:dcolor = 2.
      else
         v-leadtm:dcolor = 0.
  END.
/*------------------------------------------------------------------------ */     PROCEDURE WhatPrompt:
/*------------------------------------------------------------------------ */       IF FRAME-FIELD = "g-Fabquoteno" THEN
      ASSIGN promptpos = 1.
    ELSE
    IF FRAME-FIELD = "x-whse" THEN
      ASSIGN promptpos = 2.
    ELSE
    IF FRAME-FIELD = "v-takenby" THEN
      ASSIGN promptpos = 3.
    ELSE
    IF FRAME-FIELD = "v-custnoa" THEN
      ASSIGN promptpos = 4.
    ELSE
    IF FRAME-FIELD = "v-shipto" THEN
      ASSIGN promptpos = 5.
    ELSE
    IF FRAME-FIELD = "v-custpo" THEN
      ASSIGN promptpos = 6.
    ELSE
    IF FRAME-FIELD = "v-model" THEN
      ASSIGN promptpos = 7.
    ELSE
    IF FRAME-FIELD = "s-prod" THEN
      ASSIGN promptpos = 8.
  END.

/* ------------------------------------------------------------------------- */
Procedure Refresh_Quote:
/* ------------------------------------------------------------------------- */
  run Build_Refresh.
  assign s-sysrefresh = "Y".
  find first rfresh no-error.
  if avail rfresh then do:
    display with frame f-new-refresh.
    open query q-rfresh for each rfresh.
    display b-rfresh with frame f-rfresh.
    on cursor-up cursor-up.
    on cursor-down cursor-down.
    F8-Refresh:
      do while true on endkey undo F8-Refresh, leave f8-Refresh:
      update  b-rfresh with frame f-rfresh
      editing:
        readkey.

        if lastkey = 404 then do:
          hide frame f-new-refresh no-pause.
          hide frame f-new-rfresh no-pause.
          leave F8-Refresh.
        end.
      apply lastkey.
      end. /* editing */
    leave.
    end. /* F8-Refresh */
    close query q-rfresh.
    hide frame f-new-refresh no-pause.
    hide frame f-rfresh no-pause.
  end. /* if avail rfresh */
  hide frame f-rfresh no-pause.

  on cursor-up back-tab.
  on cursor-down tab.
   
  assign v-prod = ""
         g-prod = "".


end. /* Procedure Refresh_Quote */


/* ------------------------------------------------------------------------- */
Procedure ReCost:
/* ------------------------------------------------------------------------- */
  define input parameter ip-selected as logical no-undo.

  define buffer bp-t-lines for t-lines.
  
  if ip-selected then
    assign v-verifylit = "Update only Selected Line Items, Continue?".
  else
    assign v-verifylit = "Update All Line Items Continue?".

  
  assign s-verify = false.
  
  find first rfresh no-error.
  if avail rfresh then do:
    display v-verifylit s-verify with frame f-refresh.
    F8-Refresh:
      do while true on endkey undo F8-Refresh, leave f8-Refresh:
      update  s-verify with frame f-refresh
      editing:
        readkey.

        if lastkey = 404 then do:
          assign s-verify = false.
          leave.
        end.
      apply lastkey.
      end. /* editing */
    leave.
    end. /* F8-Refresh */
  if s-verify then do:
    if ip-selected then do:
      for each buf-rfresh where buf-rfresh.seltype = "x" no-lock:
        find bp-t-lines where bp-t-lines.lineno = buf-rfresh.lineno.
        if avail bp-t-lines then do:
         /*** update vaspsl  ***/
          FIND vaspsl WHERE RECID(vaspsl) = bp-t-lines.vaspslid
          EXCLUSIVE-LOCK NO-ERROR.
          FIND FIRST vasp WHERE
                     vasp.cono = g-cono         AND
                     vasp.shipprod = x-quoteno  
          EXCLUSIVE-LOCK NO-ERROR.
          FIND FIRST zsdivasp WHERE
                     zsdivasp.cono = g-cono         AND
                     zsdivasp.rectype = "fb"        AND
                     zsdivasp.repairno = x-quoteno  
          EXCLUSIVE-LOCK NO-ERROR.
     
          assign 
            zsdivasp.invcost = zsdivasp.invcost - 
               (vaspsl.prodcost * vaspsl.qtyneeded)
            vaspsl.prodcost     =  buf-rfresh.fut-cost.
         
          assign zsdivasp.invcost = zsdivasp.invcost + 
                 (vaspsl.prodcost * vaspsl.qtyneeded).
          Release vaspsl.  
          run ZsdiVaspMap-update (input "c",
                                  input bp-t-lines.vaspslid).
        end.
      end. /* For each */
    end.   /* If selected */
    else do:
      for each buf-rfresh  no-lock:
        find bp-t-lines where bp-t-lines.lineno = buf-rfresh.lineno.
        if avail bp-t-lines then do:
         /*** update vaspsl  ***/
          FIND vaspsl WHERE RECID(vaspsl) = bp-t-lines.vaspslid
          EXCLUSIVE-LOCK NO-ERROR.
          FIND FIRST vasp WHERE
                     vasp.cono = g-cono         AND
                     vasp.shipprod = x-quoteno  
          EXCLUSIVE-LOCK NO-ERROR.
          FIND FIRST zsdivasp WHERE
                     zsdivasp.cono = g-cono         AND
                     zsdivasp.rectype = "fb"        AND
                     zsdivasp.repairno = x-quoteno  
          EXCLUSIVE-LOCK NO-ERROR.
     
          assign 
            zsdivasp.invcost = zsdivasp.invcost - 
               (vaspsl.prodcost * vaspsl.qtyneeded)
            vaspsl.prodcost     =  buf-rfresh.fut-cost.
         
          assign zsdivasp.invcost = zsdivasp.invcost + 
                 (vaspsl.prodcost * vaspsl.qtyneeded).
          Release vaspsl.  
          run ZsdiVaspMap-update (input "c",
                                  input bp-t-lines.vaspslid).
        end.
      end. /* For each */
    end.   /* else If not selected */
    
  end.
  
  hide frame f-refresh.
  end.



end. /* Procedure ReCost */


/* ------------------------------------------------------------------------- */
Procedure Build_Refresh:
/* ------------------------------------------------------------------------- */

  for each rfresh:
    delete rfresh.
  end.
  for each t-lines where t-lines.seqno = 0 no-lock:
    if t-lines.specnstype <> "n" and t-lines.specnstype <> "l" then
      do:
      assign /* zi-displaylinefl = false */
             v-whse           = t-lines.whse
             v-qty            = t-lines.qty
             v-prod           = t-lines.prod.
    end. /* t-lines.specnstype <> "n" and t-lines.specnstype <> "l" */
    assign s-netavail = 0.
/* tahtrain
    {w-icsw.i t-lines.prod t-lines.whse no-lock}
 tahtrain replaced with below */ 
    {w-icsw.i t-lines.prod x-whse no-lock}
    if avail icsw then 
      do:
      {w-icsp.i icsw.prod no-lock}
      if avail icsp then
        do:
        {p-netavl.i}
        end.
    end.
    create rfresh.
    assign rfresh.seltype    = ""
           rfresh.lineno     = t-lines.lineno
           rfresh.specnstype = t-lines.specnstype
           rfresh.prod       = t-lines.prod
           rfresh.whse       = t-lines.whse
           rfresh.qty        = t-lines.qty
           rfresh.avail-qty  = if t-lines.specnstype = "n" then
                                 t-lines.qty
                               else
                                 s-netavail
           rfresh.cur-cost   = t-lines.prodcost
           rfresh.fut-cost   = if t-lines.specnstype = "n" then
                                 t-lines.prodcost
                               else if avail icsw then
                                 icsw.avgcost
                               else  
                                 t-lines.prodcost
           rfresh.sav-cost   = rfresh.fut-cost.
    
  end. /* for each t-lines */
end. /* Procedure Build_Refresh */


/* ------------------------------------------------------------------------- */
Procedure LineMover:
/* ------------------------------------------------------------------------- */
  DEF INPUT PARAMETER ip-lineno like t-lines.lineno no-undo.
  
  assign hm-linefrom = ip-lineno
         hm-lineto   = 0.
      run zsdierrx.p(
      input
"                                                                                 ",
             input false).
 

  MoverLoop:
  DO WHILE TRUE ON ENDKEY UNDO, LEAVE MoverLoop:
    STATUS DEFAULT "F4 Cancels the Move Process.".
    assign hm-word = if hm-linepos then "efore" else "fter".
    display 
      hm-linefrom 
      hm-linepos
      hm-word
      hm-lineto
    with frame f-linemover. 
    
    update 
      hm-linepos
      hm-lineto
    with frame f-linemover
    editing:  
      readkey.
          
      if {k-cancel.i} or {k-jump.i} then do:
        hide frame f-linemover.
        STATUS DEFAULT "".
        return.
      end.

      if frame-field = "hm-linepos" then do:
        assign hm-linepos = input hm-linepos.
        IF (LASTKEY = 501 OR
            LASTKEY = 502 OR
            LASTKEY = 507 OR
            LASTKEY = 508) THEN DO:
          if hm-linepos then
             assign hm-linepos = false.
          else
             assign hm-linepos = true.
        end.
        if hm-linepos then
          assign hm-word = "efore". 
        else
          assign hm-word = "fter ".
        
        display hm-linepos hm-word with frame f-linemover.
      end.     
      
      
      if frame-field = "hm-lineto" then do:
        IF (LASTKEY = 501 OR
            LASTKEY = 502 OR
            LASTKEY = 507 OR
            LASTKEY = 508) AND v-linebrowseopen = TRUE THEN DO:
          RUN MovelineLu (INPUT LASTKEY).
          ASSIGN v-linebrowsekeyed = TRUE
                 hm-lineto         = if avail t-lines then
                                       t-lines.lineno
                                     else
                                       0.
          display hm-lineto with frame f-linemover.
          NEXT.
        END.
        if {k-after.i} then do:
          apply lastkey.
          leave.
        END.
      END.
    apply lastkey.
    END.
    
    
    if {k-cancel.i} or {k-jump.i} then do:
      hide frame f-linemover.
      STATUS DEFAULT "".
      DISPLAY fs-2 WITH FRAME f-statusline2.
      return.
    end.

    if hm-lineto = hm-linefrom or
       /* Before */
       (hm-linepos and (hm-lineto - 1) = hm-linefrom) or
       (hm-linepos and hm-lineto = 0) or
       /* After */
       (not hm-linepos and hm-lineto = (hm-linefrom - 1)) then do:
      run zsdierrx.p (input "Line movement selected is not valid retry request",
                      input true).
      next.
    end.

    if {k-after.i} then do:
      display with frame f-pausing.
      
      if hm-linefrom < (if hm-linepos then
                          hm-lineto - 1
                        else
                          hm-lineto) then

        run RepositionLinesDown (input (if hm-linepos then
                                          hm-lineto - 1
                                        else
                                           hm-lineto),
                                 input hm-linefrom).
      else
        run RepositionLinesUp (input (if hm-linepos then
                                        hm-lineto - 1
                                      else
                                        hm-lineto),
                               input hm-linefrom).
      hide frame f-pausing. 
    end.  
      
    leave MoverLoop.
  
  end. /* Do MoverLoop*/

  readkey pause 0.  
  hide frame f-linemover.
  STATUS DEFAULT "".
  DISPLAY fs-2 WITH FRAME f-statusline2.
  close QUERY q-lines.
  OPEN QUERY q-lines FOR EACH t-lines use-index ix1.
  b-lines:REFRESHABLE IN FRAME f-lines = FALSE.
  DISPLAY b-lines WITH FRAME f-lines.
  b-lines:REFRESHABLE IN FRAME f-lines  = TRUE.
  
end.    

/* ------------------------------------------------------------------------- */
Procedure RepositionLinesDown:
/* ------------------------------------------------------------------------- */
  DEF INPUT PARAMETER ip-lineto          like t-lines.lineno no-undo.
  DEF INPUT PARAMETER ip-linefrom        like t-lines.lineno no-undo.
  

  
  def buffer ib-t-lines     for t-lines.
  def buffer ib-vaspsl      for vaspsl.
  def buffer ib-zsdivaspmap for zsdivaspmap.
  def buffer ib-t-vaspmap   for t-vaspmap.
 
  def var ib-lineposition   as int no-undo.
  def var ib-lastposition   as int no-undo.

  find last ib-t-lines use-index ix1 no-lock no-error.
  if not avail ib-t-lines then
    return.
  assign ib-lastposition = ib-t-lines.lineno.
  
  find ib-t-lines use-index ix1 where ib-t-lines.lineno = ip-linefrom no-error.
  if not avail ib-t-lines then
    return.

  assign ib-t-lines.lineno = 9999999 no-error.

  find ib-vaspsl where recid(ib-vaspsl) = ib-t-lines.vaspslid no-error.
  if not avail ib-vaspsl then
    leave.

    
  find rarr-com use-index k-com where 
       rarr-com.cono     = g-cono      and
       rarr-com.comtype  = ib-vaspsl.shipprod and  /* Quote No */
       rarr-com.orderno  = 0           and
       rarr-com.ordersuf = 0           and
       rarr-com.lineno   = ib-vaspsl.lineno 
       no-error.
  if avail rarr-com then do:
    assign rarr-com.lineno = ib-t-lines.lineno no-error.
  end.  
    
  for each ib-t-vaspmap use-index k-add where
           ib-t-vaspmap.cono    = ib-vaspsl.cono and
           ib-t-vaspmap.rectype = "L" and
           ib-t-vaspmap.seqno   = ib-vaspsl.seqno and
           ib-t-vaspmap.lineno  = ib-vaspsl.lineno:
    assign ib-t-vaspmap.lineno  = ib-t-lines.lineno no-error.
  end.  
  for each ib-t-vaspmap use-index k-add where
           ib-t-vaspmap.cono    = ib-vaspsl.cono and
           ib-t-vaspmap.rectype = "LQ" and
           ib-t-vaspmap.seqno   = ib-vaspsl.seqno and
           ib-t-vaspmap.lineno  = ib-vaspsl.lineno and
           ib-t-vaspmap.prod    = x-quoteno:   
    assign ib-t-vaspmap.lineno  = ib-t-lines.lineno no-error.
  end.  
       
  for each ib-zsdivaspmap use-index k-mapty where
           ib-zsdivaspmap.cono    = ib-vaspsl.cono and
           ib-zsdivaspmap.rectype = "L" and
           ib-zsdivaspmap.seqno   = ib-vaspsl.seqno and
           ib-zsdivaspmap.lineno  = ib-vaspsl.lineno and
           ib-zsdivaspmap.prod    = x-quoteno:
    
    assign ib-zsdivaspmap.lineno  = ib-t-lines.lineno no-error.
  end.  

  for each ib-zsdivaspmap use-index k-mapty where
           ib-zsdivaspmap.cono    = ib-vaspsl.cono and
           ib-zsdivaspmap.rectype = "LQ" and
           ib-zsdivaspmap.seqno   = ib-vaspsl.seqno and
           ib-zsdivaspmap.lineno  = ib-vaspsl.lineno and
           ib-zsdivaspmap.prod    = x-quoteno:
  
    assign ib-zsdivaspmap.lineno  = ib-t-lines.lineno no-error.
  end.  
 
  assign ib-vaspsl.lineno  = ib-t-lines.lineno no-error.
  

  assign ib-lineposition = ip-linefrom.

  RepositionLoop:
  DO WHILE TRUE ON ENDKEY UNDO, LEAVE RepositionLoop:
    
    find ib-t-lines where ib-t-lines.lineno = ib-lineposition + 1 no-error.
   
    if not avail ib-t-lines then
      leave.
  
    
    find ib-vaspsl where recid(ib-vaspsl) = ib-t-lines.vaspslid no-error.
  
    if not avail ib-vaspsl then
      leave.
    
    find rarr-com use-index k-com where 
         rarr-com.cono     = g-cono      and
         rarr-com.comtype  = ib-vaspsl.shipprod and  /* Quote No */
         rarr-com.orderno  = 0           and
         rarr-com.ordersuf = 0           and
         rarr-com.lineno   = ib-vaspsl.lineno 
         no-error.
    if avail rarr-com then do:
      assign rarr-com.lineno = ib-vaspsl.lineno - 1.
    end.

    for each ib-t-vaspmap use-index k-add where
             ib-t-vaspmap.cono    = ib-vaspsl.cono and
             ib-t-vaspmap.rectype = "L" and
             ib-t-vaspmap.seqno   = ib-vaspsl.seqno and
             ib-t-vaspmap.lineno  = ib-vaspsl.lineno and
             ib-t-vaspmap.prod    = x-quoteno:
      assign ib-t-vaspmap.lineno  = ib-t-vaspmap.lineno - 1.
    end.  

    for each ib-t-vaspmap use-index k-add where
             ib-t-vaspmap.cono    = ib-vaspsl.cono and
             ib-t-vaspmap.rectype = "LQ" and
             ib-t-vaspmap.seqno   = ib-vaspsl.seqno and
             ib-t-vaspmap.lineno  = ib-vaspsl.lineno and
             ib-t-vaspmap.prod    = x-quoteno:
     assign ib-t-vaspmap.lineno  = ib-t-vaspmap.lineno - 1.
     end.  
    
    for each ib-zsdivaspmap use-index k-mapty where
             ib-zsdivaspmap.cono    = ib-vaspsl.cono and
             ib-zsdivaspmap.rectype = "L" and
             ib-zsdivaspmap.seqno   = ib-vaspsl.seqno and
             ib-zsdivaspmap.lineno  = ib-vaspsl.lineno and
             ib-zsdivaspmap.prod    = x-quoteno:
    assign ib-zsdivaspmap.lineno  = ib-zsdivaspmap.lineno - 1.
    end.  

    for each ib-zsdivaspmap use-index k-mapty where
             ib-zsdivaspmap.cono    = ib-vaspsl.cono and
             ib-zsdivaspmap.rectype = "LQ" and
             ib-zsdivaspmap.seqno   = ib-vaspsl.seqno and
             ib-zsdivaspmap.lineno  = ib-vaspsl.lineno and
             ib-zsdivaspmap.prod    = x-quoteno:
    assign ib-zsdivaspmap.lineno  = ib-zsdivaspmap.lineno - 1.
    end.  

    assign ib-t-lines.lineno = ib-t-lines.lineno - 1.
    assign ib-vaspsl.lineno  = ib-t-lines.lineno no-error.
     
    assign ib-lineposition = ib-lineposition + 1.
    
    if ib-lineposition > ib-lastposition or
       ib-lineposition = ip-lineto then
      leave RepositionLoop.
  
  end. /* loop */     


  find ib-t-lines use-index ix1 where ib-t-lines.lineno = 9999999 no-error.
  if not avail ib-t-lines then
    return.

  assign ib-t-lines.lineno = ip-lineto no-error.

  find ib-vaspsl where recid(ib-vaspsl) = ib-t-lines.vaspslid no-error.
  if not avail ib-vaspsl then
    leave.

    
  find  rarr-com use-index k-com where 
        rarr-com.cono     = g-cono      and
        rarr-com.comtype  = ib-vaspsl.shipprod and  /* Quote No */
        rarr-com.orderno  = 0           and
        rarr-com.ordersuf = 0           and
        rarr-com.lineno   = ib-vaspsl.lineno 
        no-error.
  if avail rarr-com then do:
    assign rarr-com.lineno = ib-t-lines.lineno no-error.
  end.  
    
  for each ib-t-vaspmap use-index k-add where
           ib-t-vaspmap.cono    = ib-vaspsl.cono and
           ib-t-vaspmap.rectype = "L" and
           ib-t-vaspmap.seqno   = ib-vaspsl.seqno and
           ib-t-vaspmap.lineno  = ib-vaspsl.lineno and
           ib-t-vaspmap.prod    = x-quoteno:
    assign ib-t-vaspmap.lineno  = ib-t-lines.lineno no-error.
  end.  
  for each ib-t-vaspmap use-index k-add where
           ib-t-vaspmap.cono    = ib-vaspsl.cono and
           ib-t-vaspmap.rectype = "LQ" and
           ib-t-vaspmap.seqno   = ib-vaspsl.seqno and
           ib-t-vaspmap.lineno  = ib-vaspsl.lineno and
           ib-t-vaspmap.prod    = x-quoteno:
    assign ib-t-vaspmap.lineno  = ib-t-lines.lineno no-error.
  end.  
       
  for each ib-zsdivaspmap use-index k-mapty where
           ib-zsdivaspmap.cono    = ib-vaspsl.cono and
           ib-zsdivaspmap.rectype = "L" and
           ib-zsdivaspmap.seqno   = ib-vaspsl.seqno and
           ib-zsdivaspmap.lineno  = ib-vaspsl.lineno and
           ib-zsdivaspmap.prod    = x-quoteno:
    assign ib-zsdivaspmap.lineno  = ib-t-lines.lineno no-error.
  end.  
 
  for each ib-zsdivaspmap use-index k-mapty where
           ib-zsdivaspmap.cono    = ib-vaspsl.cono and
           ib-zsdivaspmap.rectype = "LQ" and
           ib-zsdivaspmap.seqno   = ib-vaspsl.seqno and
           ib-zsdivaspmap.lineno  = ib-vaspsl.lineno and
           ib-zsdivaspmap.prod    = x-quoteno:
    assign ib-zsdivaspmap.lineno  = ib-t-lines.lineno no-error.

  end.  
  assign ib-vaspsl.lineno  = ib-t-lines.lineno no-error.
end.


/* ------------------------------------------------------------------------- */
Procedure RepositionLinesUp:
/* ------------------------------------------------------------------------- */
  DEF INPUT PARAMETER ip-lineto          like t-lines.lineno no-undo.
  DEF INPUT PARAMETER ip-linefrom        like t-lines.lineno no-undo.
  

  
  def buffer ib-t-lines     for t-lines.
  def buffer ib-vaspsl      for vaspsl.
  def buffer ib-zsdivaspmap for zsdivaspmap.
  def buffer ib-t-vaspmap   for t-vaspmap.
 
  def var ib-lineposition   as int no-undo.
  def var ib-lastposition   as int no-undo.

  find last ib-t-lines use-index ix1 no-lock no-error.
  if not avail ib-t-lines then
    return.
  assign ib-lastposition = ib-t-lines.lineno.
  
  find ib-t-lines use-index ix1 where ib-t-lines.lineno = ip-linefrom no-error.
  if not avail ib-t-lines then
    return.

  assign ib-t-lines.lineno = 9999999 no-error.

  find ib-vaspsl where recid(ib-vaspsl) = ib-t-lines.vaspslid no-error.
  if not avail ib-vaspsl then
    leave.

    
  find rarr-com use-index k-com where 
       rarr-com.cono     = g-cono      and
       rarr-com.comtype  = ib-vaspsl.shipprod and  /* Quote No */
       rarr-com.orderno  = 0           and
       rarr-com.ordersuf = 0           and
       rarr-com.lineno   = ib-vaspsl.lineno 
       no-error.
  if avail rarr-com then do:
    assign rarr-com.lineno = ib-t-lines.lineno no-error.
  end.  
    
  for each ib-t-vaspmap use-index k-add where
           ib-t-vaspmap.cono    = ib-vaspsl.cono and
           ib-t-vaspmap.rectype = "L" and
           ib-t-vaspmap.seqno   = ib-vaspsl.seqno and
           ib-t-vaspmap.lineno  = ib-vaspsl.lineno and
           ib-t-vaspmap.prod    = x-quoteno:
    assign ib-t-vaspmap.lineno  = ib-t-lines.lineno no-error.
  end.  
  for each ib-t-vaspmap use-index k-add where
           ib-t-vaspmap.cono    = ib-vaspsl.cono and
           ib-t-vaspmap.rectype = "LQ" and
           ib-t-vaspmap.seqno   = ib-vaspsl.seqno and
           ib-t-vaspmap.lineno  = ib-vaspsl.lineno and
           ib-t-vaspmap.prod    = x-quoteno:
   
    assign ib-t-vaspmap.lineno  = ib-t-lines.lineno no-error.
  end.  
       
  for each ib-zsdivaspmap use-index k-mapty where
           ib-zsdivaspmap.cono    = ib-vaspsl.cono and
           ib-zsdivaspmap.rectype = "L" and
           ib-zsdivaspmap.seqno   = ib-vaspsl.seqno and
           ib-zsdivaspmap.lineno  = ib-vaspsl.lineno and
           ib-zsdivaspmap.prod    = x-quoteno:
    
    assign ib-zsdivaspmap.lineno  = ib-t-lines.lineno no-error.
  end.  

  for each ib-zsdivaspmap use-index k-mapty where
           ib-zsdivaspmap.cono    = ib-vaspsl.cono and
           ib-zsdivaspmap.rectype = "LQ" and
           ib-zsdivaspmap.seqno   = ib-vaspsl.seqno and
           ib-zsdivaspmap.lineno  = ib-vaspsl.lineno and
           ib-zsdivaspmap.prod    = x-quoteno:
  
    assign ib-zsdivaspmap.lineno  = ib-t-lines.lineno no-error.
  end.  
 
  assign ib-vaspsl.lineno  = ib-t-lines.lineno no-error.
  

  assign ib-lineposition = ip-linefrom.

  RepositionLoop:
  DO WHILE TRUE ON ENDKEY UNDO, LEAVE RepositionLoop:
    
    find ib-t-lines where ib-t-lines.lineno = ib-lineposition - 1 no-error.
   
    if not avail ib-t-lines then
      leave.
  
    
    find ib-vaspsl where recid(ib-vaspsl) = ib-t-lines.vaspslid no-error.
  
    if not avail ib-vaspsl then
      leave.
    
    find rarr-com use-index k-com where 
         rarr-com.cono     = g-cono      and
         rarr-com.comtype  = ib-vaspsl.shipprod and  /* Quote No */
         rarr-com.orderno  = 0           and
         rarr-com.ordersuf = 0           and
         rarr-com.lineno   = ib-vaspsl.lineno 
         no-error.
    if avail rarr-com then do:
      assign rarr-com.lineno = ib-vaspsl.lineno + 1.
    end.

    for each ib-t-vaspmap use-index k-add  where
             ib-t-vaspmap.cono    = ib-vaspsl.cono and
             ib-t-vaspmap.rectype = "L" and
             ib-t-vaspmap.seqno   = ib-vaspsl.seqno and
             ib-t-vaspmap.lineno  = ib-vaspsl.lineno and
             ib-t-vaspmap.prod    = x-quoteno:
      assign ib-t-vaspmap.lineno  = ib-t-vaspmap.lineno + 1.
    end.  

    for each ib-t-vaspmap use-index k-add where
             ib-t-vaspmap.cono    = ib-vaspsl.cono and
             ib-t-vaspmap.rectype = "LQ" and
             ib-t-vaspmap.seqno   = ib-vaspsl.seqno and
             ib-t-vaspmap.lineno  = ib-vaspsl.lineno and
             ib-t-vaspmap.prod    = x-quoteno:
     assign ib-t-vaspmap.lineno  = ib-t-vaspmap.lineno + 1.
     end.  
    
    for each ib-zsdivaspmap use-index k-mapty where
             ib-zsdivaspmap.cono    = ib-vaspsl.cono and
             ib-zsdivaspmap.rectype = "L" and
             ib-zsdivaspmap.seqno   = ib-vaspsl.seqno and
             ib-zsdivaspmap.lineno  = ib-vaspsl.lineno and
             ib-zsdivaspmap.prod    = x-quoteno:
    assign ib-zsdivaspmap.lineno  = ib-zsdivaspmap.lineno + 1.
    end.  

    for each ib-zsdivaspmap use-index k-mapty where
             ib-zsdivaspmap.cono    = ib-vaspsl.cono and
             ib-zsdivaspmap.rectype = "LQ" and
             ib-zsdivaspmap.seqno   = ib-vaspsl.seqno and
             ib-zsdivaspmap.lineno  = ib-vaspsl.lineno and
             ib-zsdivaspmap.prod    = x-quoteno:
    assign ib-zsdivaspmap.lineno  = ib-zsdivaspmap.lineno + 1.
    end.  

    assign ib-t-lines.lineno = ib-t-lines.lineno + 1.
    assign ib-vaspsl.lineno  = ib-t-lines.lineno no-error.
    assign ib-lineposition = ib-lineposition - 1.
    
    if ib-lineposition < 1 or
       ib-lineposition le (ip-lineto + 1) then
      leave RepositionLoop.
  end. /* loop */     

  find ib-t-lines use-index ix1 where ib-t-lines.lineno = 9999999 no-error.
  if not avail ib-t-lines then
    return.
  assign ib-t-lines.lineno = ip-lineto + 1 no-error.

  find ib-vaspsl where recid(ib-vaspsl) = ib-t-lines.vaspslid no-error.
  if not avail ib-vaspsl then
    leave.

    
  find  rarr-com use-index k-com where 
        rarr-com.cono     = g-cono      and
        rarr-com.comtype  = ib-vaspsl.shipprod and  /* Quote No */
        rarr-com.orderno  = 0           and
        rarr-com.ordersuf = 0           and
        rarr-com.lineno   = ib-vaspsl.lineno 
        no-error.
  if avail rarr-com then do:
    assign rarr-com.lineno = ib-t-lines.lineno no-error.
  end.  
    
  for each ib-t-vaspmap use-index k-add where
           ib-t-vaspmap.cono    = ib-vaspsl.cono and
           ib-t-vaspmap.rectype = "L" and
           ib-t-vaspmap.seqno   = ib-vaspsl.seqno and
           ib-t-vaspmap.lineno  = ib-vaspsl.lineno and
           ib-t-vaspmap.prod    = x-quoteno:
    assign ib-t-vaspmap.lineno  = ib-t-lines.lineno no-error.
  end.  
  for each ib-t-vaspmap use-index k-add where
           ib-t-vaspmap.cono    = ib-vaspsl.cono and
           ib-t-vaspmap.rectype = "LQ" and
           ib-t-vaspmap.seqno   = ib-vaspsl.seqno and
           ib-t-vaspmap.lineno  = ib-vaspsl.lineno and
           ib-t-vaspmap.prod    = x-quoteno:
    assign ib-t-vaspmap.lineno  = ib-t-lines.lineno no-error.
  end.  
       
  for each ib-zsdivaspmap use-index k-mapty where
           ib-zsdivaspmap.cono    = ib-vaspsl.cono and
           ib-zsdivaspmap.rectype = "L" and
           ib-zsdivaspmap.seqno   = ib-vaspsl.seqno and
           ib-zsdivaspmap.lineno  = ib-vaspsl.lineno and
           ib-zsdivaspmap.prod    = x-quoteno:
    assign ib-zsdivaspmap.lineno  = ib-t-lines.lineno no-error.
  end.  
 
  for each ib-zsdivaspmap use-index k-mapty where
           ib-zsdivaspmap.cono    = ib-vaspsl.cono and
           ib-zsdivaspmap.rectype = "LQ" and
           ib-zsdivaspmap.seqno   = ib-vaspsl.seqno and
           ib-zsdivaspmap.lineno  = ib-vaspsl.lineno and
           ib-zsdivaspmap.prod    = x-quoteno:
    assign ib-zsdivaspmap.lineno  = ib-t-lines.lineno no-error.

  end.  
  assign ib-vaspsl.lineno  = ib-t-lines.lineno no-error.
end.

/* tah-analyze */

/* ------------------------------------------------------------------------- */
Procedure Analyze_Quote:
/* ------------------------------------------------------------------------- */
  run Build_Analyze.
  find first ranalyze no-error.
  if avail ranalyze then do:
    display with frame f-new-analyze.
    open query q-ranalyze for each ranalyze.
    DISPLAY 
      s-vareqshipdt
      s-vapromisedt
      v-invcost
      v-Intangables
      v-Laborcost
      v-rebate
      s-FabTotalCost
      s-override
      s-trendtot
      s-maxlead
      p-assigned
      p-approved
      p-pct
      s-FabTotalPrice
      v-pipematerial
      v-pipestyle
      b-ranalyze
    with frame f-ranalyze.

    on cursor-up cursor-up.
    on cursor-down cursor-down.
    F8-Analyze:
      do while true on endkey undo F8-Analyze, leave f8-Analyze:
      update  b-ranalyze with frame f-ranalyze
      editing:
        readkey.

        if lastkey = 404 then do:
          hide frame f-new-analyze no-pause.
          hide frame f-ranalyze no-pause.
          leave F8-Analyze.
        end.
      apply lastkey.
      end. /* editing */
    leave.
    end. /* F8-Analyze */
    close query q-ranalyze.
    hide frame f-new-analyze no-pause.
    hide frame f-ranalyze no-pause.
  end. /* if avail ranalyze */
  hide frame f-ranalyze no-pause.

  on cursor-up back-tab.
  on cursor-down tab.
   
  assign v-prod = ""
         g-prod = "".
    
  if {k-cancel.i} or {k-jump.i} or {k-accept.i} then do:
    find zsdivasp where 
         zsdivasp.cono = g-cono and
         zsdivasp.rectype = "fb" and
         zsdivasp.repairno = x-quoteno no-lock no-error.

      find icsp where icsp.cono =  g-cono and
                         icsp.prod = zsdivasp.partno no-lock no-error.
      if avail icsp then
        find notes use-index k-notes where                        
             notes.cono         = g-cono and                      
             notes.notestype    = "zz" and                        
             notes.primarykey   = "standard cost markup" and      
             notes.secondarykey = icsp.prodcat and         
             notes.pageno       = 1                               
        no-lock no-error.                                       
      if not avail icsp or
        (avail icsp and not avail notes) then
        find notes use-index k-notes where                      
             notes.cono         = g-cono and                    
             notes.notestype    = "zz" and                      
             notes.primarykey   = "standard cost markup" and    
             notes.secondarykey = "" and                        
             notes.pageno       = 1                             
        no-lock no-error.                                     
      if avail notes then 
     /* calculate the cost gain for nonstock or v-prodcost override  */      
        assign iv-addon =  (1 + (dec(notes.noteln[1])  / 100)).   
      else 
        assign iv-addon = 0.                                                  
      
     
     
    ASSIGN 
        v-maxlead  = 0
        s-maxlead  = INT(zsdivasp.leadtm1)
        p-pct      = IF zsdivasp.margpct > 0 OR
                        zsdivasp.margpct <> 0  THEN
                      zsdivasp.margpct
                     ELSE
                       035.00
        o-pct         = p-pct
        v-Intangables     = zsdivasp.Intangtotl
        v-invcost     = zsdivasp.invcost
        v-Laborcost     = zsdivasp.Labortotl
        v-rebate      = zsdivasp.user8 * -1
        s-FabTotalCost    = v-Intangables + v-invcost + v-Laborcost + v-rebate
        s-trendtot    = s-FabTotalCost * iv-addon
        s-override  = IF zsdivasp.override NE "" THEN
                        "*"
                      ELSE
                        " "
        s-FabTotalPrice  = if zsdivasp.override ne "" then
                              zsdivasp.sellprc
                           else
                           if round(((s-trendtot / (100 - p-pct)) * 100),2) -
                            ((s-trendtot / (100 - p-pct) * 100)) <> 0 then
                            round(((s-trendtot / (100 - p-pct)) * 100),2) 
                              + .01 
                           else
                              round(((s-trendtot / (100 - p-pct)) * 100),2)
                           
        s-vareqshipdt = zsdivasp.user4
        s-vapromisedt = zsdivasp.user5.
    
    b-totals:refresh () in frame f8-total.
 
    DISPLAY 
         s-vareqshipdt
         s-vapromisedt
         v-invcost
         v-Intangables
         v-Laborcost
         v-rebate
         s-FabTotalCost
         s-override
         s-trendtot
         s-maxlead
         p-assigned
         p-approved
         p-pct
         s-FabTotalPrice
         v-pipematerial
         v-pipestyle
       
       WITH FRAME f8-total.
    
  end.
 

end. /* Procedure Analyze_Quote */

/* ------------------------------------------------------------------------- */
Procedure Build_Analyze:
/* ------------------------------------------------------------------------- */

  for each ranalyze:
    delete ranalyze.
  end.
  for each t-lines where t-lines.seqno = 0 no-lock:
    if t-lines.specnstype <> "n" and t-lines.specnstype <> "l" then
      do:
      assign v-qty            = t-lines.qty
             v-prod           = t-lines.prod.
    end. /* t-lines.specnstype <> "n" and t-lines.specnstype <> "l" */
    create ranalyze.
    assign 
           ranalyze.lineno     = t-lines.lineno
           ranalyze.specnstype = t-lines.specnstype
           ranalyze.prod       = t-lines.prod
           ranalyze.qty        = t-lines.qty
           ranalyze.cost       = t-lines.prodcost.
    run Accumulatetype (input "labor Items",
                        input t-lines.lineno,      
                        output ranalyze.Labor-cost).
    run Accumulatetype (input "Intangables",
                        input t-lines.lineno,      
                        output ranalyze.Intang-cost).
    
  end. /* for each t-lines */
end. /* Procedure Build_Analyze */



/* ------------------------------------------------------------------------- */
Procedure Retotal_Analyze:
/* ------------------------------------------------------------------------- */
  Define input parameter ip-lineno as integer no-undo.
  for each t-lines where 
           t-lines.lineno = ip-lineno and
           t-lines.seqno = 0 no-lock:
    if t-lines.specnstype <> "n" and t-lines.specnstype <> "l" then
      do:
      assign v-qty            = t-lines.qty
             v-prod           = t-lines.prod.
    end. /* t-lines.specnstype <> "n" and t-lines.specnstype <> "l" */
    assign 
           ranalyze.cost       = t-lines.prodcost.
           
    run Accumulatetype (input "labor Items",
                        input t-lines.lineno,      
                        output ranalyze.Labor-cost).
    run Accumulatetype (input "Intangables",
                        input t-lines.lineno,      
                        output ranalyze.Intang-cost).
    
  end. /* for each t-lines */
end. /* Procedure Retotal_Analyze */







/* -------------------------------------------------------------------------  */
  PROCEDURE Accumulatetype:
/* -------------------------------------------------------------------------  */
  define input parameter ip-typename as char no-undo.
  define input parameter ip-lineno   as integer no-undo.
  define output parameter ip-amount  as dec no-undo.
  
  define buffer b-zsdivaspmap for zsdivaspmap.
  define buffer bs-zsdivaspmap  for zsdivaspmap. 
  define buffer bsl-zsdivaspmap for zsdivaspmap. 
  define buffer bllt-vaspmap for t-vaspmap. 

  assign ip-amount = 0.
  find b-zsdivaspmap where
       b-zsdivaspmap.cono = g-cono and
       b-zsdivaspmap.rectype  = "S" and        /* VAES */
       b-zsdivaspmap.prod     = x-quoteno  and
       b-zsdivaspmap.typename = "INVENTORY" and
       b-zsdivaspmap.lineno = 0  no-lock  no-error.
  if not avail b-zsdivaspmap then do:
    message "program error no zsdivaspmap".
    pause 10.
    return.       
  end.
  for each bs-zsdivaspmap where
           bs-zsdivaspmap.cono        = g-cono and
           bs-zsdivaspmap.rectype     = "S" and     /* Section for type */
           bs-zsdivaspmap.prod        = x-quoteno and
           bs-zsdivaspmap.typename    = ip-typename,
     each  bsl-zsdivaspmap where
           bsl-zsdivaspmap.cono      = g-cono   and
           bsl-zsdivaspmap.rectype     = "L"  and  
            /* Line in section for type */
           bsl-zsdivaspmap.prod        = x-quoteno  and
           bsl-zsdivaspmap.seqno       = bs-zsdivaspmap.seqno,     
     each bllt-vaspmap where
          bllt-vaspmap.cono = g-cono      and
          bllt-vaspmap.rectype     = "LQ" and  
          /* Inventory Line in section  for type */
          bllt-vaspmap.prod        = x-quoteno  and
          bllt-vaspmap.seqno       = b-zsdivaspmap.seqno and
          bllt-vaspmap.lineno      = ip-lineno and
          bllt-vaspmap.typename    = bsl-zsdivaspmap.typename  no-lock:
      assign ip-amount = ip-amount + round(bllt-vaspmap.addonamt[1],2).  
 
 end.          

assign ip-amount = round(ip-amount,2).
end.


/* tah-analyze */

/* Catalog */
/* -------------------------------------------------------------------------  */
Procedure DoCatLu:
/* -------------------------------------------------------------------------  */
   define input parameter ix-Prod    as character format "x(24)" no-undo.
   define input parameter ix-runtype as character                no-undo.
   /*
   message "g-prodlutype: " g-prodlutype. pause.
   */
   assign g-whse = x-whse 
          v-save = ix-prod
          g-prod = ix-prod.
   
   run icsclu.p.
   
   readkey pause 0.
   assign  v-prod = g-prod.
/*
   if v-prod <> " " then
     assign catalogfl = yes.
*/
end. /* Procedure DoCatLu */

/* Catalog */






