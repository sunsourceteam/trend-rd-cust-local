/* n-oeizl3.i 1.1 01/03/98 */
/* n-oeizl3.i 1.6 09/19/97 */
/*h*****************************************************************************
  SX 55
  INCLUDE      : n-oeizl3.i
  DESCRIPTION  : Find next/prev for OEIZL where v-mode = 3 or 6
  USED ONCE?   : yes
  AUTHOR       : rhl
  DATE WRITTEN :
  CHANGES MADE :
    12/23/91 pap; TB#  5266 JIT/Line Due - reqship to promise dt
    07/21/92 mwb; TB#  7191 Added custno change
    01/29/94 kmw; TB# 11747 Make stage a range
    05/09/95 mtt; TB#  5180 When looking for a product-need kit ln
    03/14/96 tdd; TB# 20713 Inquiry not working properly
    09/19/97 kjb; TB# 14318 Quantity/Value displayed is incorrect when a
        Customer PO# is entered
    08/08/02 lcb; TB# e14281 User Hooks
*******************************************************************************/

/*e*****************************************************************************
     Selection Criteria for this procedure:
       v-mode ==> 3:
               Product: <blank>
             Customer#: <blank> (0)
               CustPO#: entered on the screen
       v-mode ==> 6:
               Product: entered on the screen
             Customer#: <blank> (0)
               CustPO#: entered on the screen
*******************************************************************************/
do:
confirm = yes.

if ("{1}" = "FIRST" and s-directionfl )  then
  assign v-xmode = "C".
else
if ("{1}" = "LAST" and not s-directionfl )  then
  assign v-xmode = "O".



if v-xmode = "C" then
  do:

find {1} oeehb use-index k-custpo where
         oeehb.cono     = g-cono   and
         oeehb.custpo begins s-custpo and
         (s-custno     = {c-empty.i} or oeehb.custno    = s-custno)  and
         (s-shipto     = ""   or oeehb.shipto    = s-shipto)  and
         (s-takenby    = ""   or oeehb.takenby   = s-takenby) and
         (s-transtype  = ""   or s-transtype    = "QU" /* oeehb.transtype */) 
         and
          (s-transtype  <> "bo")  and 
          
          ((oeehb.stagecd <> 1 and oeehb.stagecd >= integer(s-stagecdl)) or
           (oeehb.stagecd = 1 and integer(s-stagecdl) = 0)) and
          (oeehb.stagecd <= integer(s-stagecdh)) and

         (s-promisedt  = ?    or oeehb.promisedt >= s-promisedt)   and
         (s-whse = "" or  oeehb.whse = s-whse) and
         oeehb.sourcepros = "ComQu" and
         oeehb.seqno = 2
         {oeizl.z99 &user_find_oeizl = "*"
                    &file            = "oeehb"}
no-lock no-error.

/*tb 6507 05/07/92 dea; added check for DO lines */
if avail oeehb and (v-mode = 6 or s-doonlyfl = yes) then do:

    /*tb 5180 05/09/95 mtt; When looking for a product-need kit ln */
    confirm = no.
    if s-doonlyfl = yes or
        (s-doonlyfl = no and
         v-mode = 6 and
         s-kitty ne "k")
    then do:

        find first oeelb use-index k-oeelb where
                   oeelb.cono     = g-cono and
                   oeelb.batchnm  = oeehb.batchnm and
                   oeelb.seqno    = oeehb.seqno and
                 ((oeelb.shipprod = s-prod and v-mode = 6 and s-doonlyfl = no) 
                   or
                  (oeelb.botype = "d" and s-doonlyfl = yes and v-mode ne 6) or
                  (oeelb.shipprod = s-prod and oeelb.botype = "d"))
        no-lock no-error.
        if avail oeelb then confirm = yes.

    end.

    /*tb 5180 05/09/95 mtt; When looking for a product-need kit ln */
    if v-mode = 6 and confirm = no and can-do("k,b",s-kitty) then do:

        find first oeelk use-index k-prod where
                   oeelk.cono       = g-cono and
                   oeelk.whse       = oeehb.whse and
                   oeelk.shipprod   = s-prod and
                   oeelk.statustype = "a" and
                   oeelk.orderno    = int(oeehb.batchnm)  and
                   oeelk.ordersuf   = 00 and
                   oeelk.ordertype  = "b"
        no-lock no-error.

        if avail oeelk then confirm = yes.
        else do:
            find first oeelk use-index k-prod where
                       oeelk.cono       = g-cono and
                       oeelk.whse       = oeehb.whse and
                       oeelk.shipprod   = s-prod and
                       oeelk.statustype = "i" and
                       oeelk.orderno    = int(oeehb.batchnm)  and
                       oeelk.ordersuf   = 00 and
                       oeelk.ordertype  = "b"
            no-lock no-error.
            if avail oeelk then confirm = yes.
        end.

        if confirm = yes and avail oeelk then do:
            find oeelb use-index k-oeel where
                 oeelb.cono     = g-cono and
                 oeelb.batchnm  = string(oeelk.orderno,">>>>>>>9") and
                 oeelb.seqno    = 2 and
                 oeelb.lineno   = oeelk.lineno and
                 (s-doonlyfl = no or oeelb.botype = "d")
            no-lock no-error.
            if not avail oeelb then confirm = no.
        end.

    end. /* if v-mode = 6 ... */

    /*tb 14318 09/19/97 kjb; Quantity/Value not correct when a Customer PO# is
        entered.  If a product was entered in OEIZL (mode = 6) and that prod
        was found on a line on the order (confirm = yes) then run the internal
        procedure to calculate the qty/value of the lines with that product on
        this order. */
    if confirm = yes and v-mode = 6 then do:
/*
        run tot-order-lines in this-procedure (buffer oeeh,
                                               output v-netamt).

        /*d Set a flag so that the display include will know to use the value
            just calculated instead of the value off the order header record */
*/  
        v-uselnfl = yes.

    end. /* if confirm = yes and v-mode = 6 */

end. /* if avail oeeh ... */

 
if avail oeehb then
  find zxinq where zxinq.present = true  no-lock no-error.
else
  find zxinq where zxinq.present = false no-lock no-error.


end.   

if v-xmode = "C" and (not avail oeehb) and
   ("{1}" = "first" or "{1}" = "next" ) then
  do:
  assign v-xmode = "O"
         confirm = yes.
  find first oeeh use-index k-custpo where
         oeeh.cono     = g-cono   and
         oeeh.custpo = "" and
         oeeh.custno = 0 no-lock no-error.

   
  end.

if v-xmode = "O" then
do:

find {1} oeeh use-index k-custpo where
         oeeh.cono     = g-cono   and
         oeeh.custpo begins s-custpo and
         (s-custno     = {c-empty.i} or oeeh.custno    = s-custno)  and
         (s-shipto     = ""   or oeeh.shipto    = s-shipto)  and
         (s-takenby    = ""   or oeeh.takenby   = s-takenby) and
         (s-transtype  = ""   or s-transtype    = oeeh.transtype or
          (s-transtype  = "bo" and oeeh.borelfl)) and

         (oeeh.stagecd >= integer(s-stagecdl)) and
         (oeeh.stagecd <= integer(s-stagecdh)) and

         (s-promisedt  = ?    or oeeh.promisedt >= s-promisedt)   and
         (s-whse = "" or  oeeh.whse = s-whse)

         {oeizl.z99 &user_find_oeizl = "*"
                    &file            = "oeeh"}
no-lock no-error.

/*tb 6507 05/07/92 dea; added check for DO lines */
if avail oeeh and (v-mode = 6 or s-doonlyfl = yes) then do:

    /*tb 5180 05/09/95 mtt; When looking for a product-need kit ln */
    confirm = no.
    if s-doonlyfl = yes or
        (s-doonlyfl = no and
         v-mode = 6 and
         s-kitty ne "k")
    then do:

        find first oeel use-index k-oeel where
                   oeel.cono     = g-cono and
                   oeel.orderno  = oeeh.orderno and
                   oeel.ordersuf = oeeh.ordersuf and
                 ((oeel.shipprod = s-prod and v-mode = 6 and s-doonlyfl = no) or
                  (oeel.botype = "d" and s-doonlyfl = yes and v-mode ne 6) or
                  (oeel.shipprod = s-prod and oeel.botype = "d"))
        no-lock no-error.
        if avail oeel then confirm = yes.
    end.

    /*tb 5180 05/09/95 mtt; When looking for a product-need kit ln */
    if v-mode = 6 and confirm = no and can-do("k,b",s-kitty) then do:

        find first oeelk use-index k-prod where
                   oeelk.cono       = g-cono and
                   oeelk.whse       = oeeh.whse and
                   oeelk.shipprod   = s-prod and
                   oeelk.statustype = "a" and
                   oeelk.orderno    = oeeh.orderno and
                   oeelk.ordersuf   = oeeh.ordersuf and
                   oeelk.ordertype  = "o"
        no-lock no-error.

        if avail oeelk then confirm = yes.
        else do:
            find first oeelk use-index k-prod where
                       oeelk.cono       = g-cono and
                       oeelk.whse       = oeeh.whse and
                       oeelk.shipprod   = s-prod and
                       oeelk.statustype = "i" and
                       oeelk.orderno    = oeeh.orderno and
                       oeelk.ordersuf   = oeeh.ordersuf and
                       oeelk.ordertype  = "o"
            no-lock no-error.
            if avail oeelk then confirm = yes.
        end.

        if confirm = yes and avail oeelk then do:
            find oeel use-index k-oeel where
                 oeel.cono     = g-cono and
                 oeel.orderno  = oeelk.orderno and
                 oeel.ordersuf = oeelk.ordersuf and
                 oeel.lineno   = oeelk.lineno and
                 (s-doonlyfl = no or oeel.botype = "d")
            no-lock no-error.
            if not avail oeel then confirm = no.
        end.

    end. /* if v-mode = 6 ... */

    /*tb 14318 09/19/97 kjb; Quantity/Value not correct when a Customer PO# is
        entered.  If a product was entered in OEIZL (mode = 6) and that prod
        was found on a line on the order (confirm = yes) then run the internal
        procedure to calculate the qty/value of the lines with that product on
        this order. */
    if confirm = yes and v-mode = 6 then do:

        run tot-order-lines in this-procedure (buffer oeeh,
                                               output v-netamt).

        /*d Set a flag so that the display include will know to use the value
            just calculated instead of the value off the order header record */
        v-uselnfl = yes.

    end. /* if confirm = yes and v-mode = 6 */

end. /* if avail oeeh ... */

if avail oeeh then
  find zxinq where zxinq.present = true  no-lock no-error.
else
  find zxinq where zxinq.present = false no-lock no-error.
end.

if v-xmode = "o" and (not avail oeeh) and
   ("{1}" = "Prev" or "{1}" = "Last" ) then
  do:
  assign v-xmode = "C"
         confirm = yes.


find last oeehb use-index k-custpo where
         oeehb.cono     = g-cono   and
         oeehb.custpo begins s-custpo and
         (s-custno     = {c-empty.i} or oeehb.custno    = s-custno)  and
         (s-shipto     = ""   or oeehb.shipto    = s-shipto)  and
         (s-takenby    = ""   or oeehb.takenby   = s-takenby) and
         (s-transtype  = ""   or s-transtype    = "QU" /* oeehb.transtype */) and
          (s-transtype  <> "bo")  and 
          ((oeehb.stagecd <> 1 and oeehb.stagecd >= integer(s-stagecdl)) or
           (oeehb.stagecd = 1 and integer(s-stagecdl) = 0)) and
          (oeehb.stagecd <= integer(s-stagecdh)) and

         (s-promisedt  = ?    or oeehb.promisedt >= s-promisedt)   and
         (s-whse = "" or  oeehb.whse = s-whse) and
         oeehb.sourcepros = "ComQu" and
         oeehb.seqno = 2
         {oeizl.z99 &user_find_oeizl = "*"
                    &file            = "oeehb"}
no-lock no-error.

/*tb 6507 05/07/92 dea; added check for DO lines */
if avail oeehb and (v-mode = 6 or s-doonlyfl = yes) then do:

    /*tb 5180 05/09/95 mtt; When looking for a product-need kit ln */
    confirm = no.
    if s-doonlyfl = yes or
        (s-doonlyfl = no and
         v-mode = 6 and
         s-kitty ne "k")
    then do:

        find first oeelb use-index k-oeelb where
                   oeelb.cono     = g-cono and
                   oeelb.batchnm  = oeehb.batchnm and
                   oeelb.seqno    = oeehb.seqno and
                 ((oeelb.shipprod = s-prod and v-mode = 6 and s-doonlyfl = no) 
                   or
                  (oeelb.botype = "d" and s-doonlyfl = yes and v-mode ne 6) or
                  (oeelb.shipprod = s-prod and oeelb.botype = "d"))
        no-lock no-error.
        if avail oeelb then confirm = yes.

    end.

    /*tb 5180 05/09/95 mtt; When looking for a product-need kit ln */
    if v-mode = 6 and confirm = no and can-do("k,b",s-kitty) then do:

        find first oeelk use-index k-prod where
                   oeelk.cono       = g-cono and
                   oeelk.whse       = oeehb.whse and
                   oeelk.shipprod   = s-prod and
                   oeelk.statustype = "a" and
                   oeelk.orderno    = int(oeehb.batchnm)  and
                   oeelk.ordersuf   = 00 and
                   oeelk.ordertype  = "b"
        no-lock no-error.

        if avail oeelk then confirm = yes.
        else do:
            find first oeelk use-index k-prod where
                       oeelk.cono       = g-cono and
                       oeelk.whse       = oeehb.whse and
                       oeelk.shipprod   = s-prod and
                       oeelk.statustype = "i" and
                       oeelk.orderno    = int(oeehb.batchnm)  and
                       oeelk.ordersuf   = 00 and
                       oeelk.ordertype  = "b"
            no-lock no-error.
            if avail oeelk then confirm = yes.
        end.

        if confirm = yes and avail oeelk then do:
            find oeelb use-index k-oeel where
                 oeelb.cono     = g-cono and
                 oeelb.batchnm  = string(oeelk.orderno,">>>>>>>9") and
                 oeelb.seqno    = 2 and
                 oeelb.lineno   = oeelk.lineno and
                 (s-doonlyfl = no or oeelb.botype = "d")
            no-lock no-error.
            if not avail oeelb then confirm = no.
        end.

    end. /* if v-mode = 6 ... */

    /*tb 14318 09/19/97 kjb; Quantity/Value not correct when a Customer PO# is
        entered.  If a product was entered in OEIZL (mode = 6) and that prod
        was found on a line on the order (confirm = yes) then run the internal
        procedure to calculate the qty/value of the lines with that product on
        this order. */
    if confirm = yes and v-mode = 6 then do:
/*
        run tot-order-lines in this-procedure (buffer oeeh,
                                               output v-netamt).

        /*d Set a flag so that the display include will know to use the value
            just calculated instead of the value off the order header record */
*/  
        v-uselnfl = yes.

    end. /* if confirm = yes and v-mode = 6 */

end. /* if avail oeeh ... */

 
if confirm = true and avail oeehb then
  find zxinq where zxinq.present = true  no-lock no-error.
else
  find zxinq where zxinq.present = false no-lock no-error.


end.   

end.
