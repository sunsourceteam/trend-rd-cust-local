/*******************************************************************************
  PROCEDURE      : x-hhep.i - (Handheld only) 
  DESCRIPTION    : 
  AUTHOR         : Sunsource
  DATE WRITTEN   : 04/01/07
  CHANGES MADE   : 
*******************************************************************************/

/{&bXX_title}*/
 display v-titlelit with frame f-xx.
/{&bXX_title}*/

/{&temptable}*/
/{&temptable}*/

/{&user_vars}*/

/*d Set title for line frame, and number of iterations in line frame */
assign
  v-length =   1
  v-scrollfl = yes.

form 
   v-whse                at 1   label "Whse      "
   {f-helpRF.i}
   v-direction           at 24  format "x(1)" no-label
   v-putawayinit         at 1   label "Operator  "
   v-receiptid           at 1   label "Receipt Id"
   {f-helpRF.i}
   v-bintpeB            at 1   label "Bin Type" 
   v-bintpeE            at 19  no-label 
   with frame f-hhep-h width 26 row 1 side-labels overlay
   title "Putaway Entry".   

/**
form 
   "Whse:"               at 1  
   v-whse                at 7 
   "Operator:"           at 12 
   v-putawayinit         at 22 
   /**
   "Receipt Id:"         at 1  dcolor 1
   "  "                  at 12 dcolor 1   
   v-receiptid           at 14 dcolor 1
   **/
   "Part#"               at 1  dcolor 2
   v-shipprod            at 2      
      Help "Verify Product Putaway"
   zsdiput.shipprod      at 2
   "Description"         at 1 dcolor 2
   zsdiput.proddesc1     at 2
   zsdiput.proddesc2     at 2
   "Putaway Qty:"        at 1  dcolor 2
   " "                   at 13 
   zsdiput.stkqtyputaway at 14 
   v-binloc              at 14 
         Help "Verify Bin Putaway"
   "Binloc1:    "        at 1  dcolor 2
   zsdiput.binloc1       at 14
   "Binloc2:    "        at 1  dcolor 2
   zsdiput.binloc2       at 14
   " "                   at 1
   x-spce          at 26
   with frame f-hhep width 26 row 1 no-box scroll 1
   no-hide no-labels overlay v-length down.   
**/

form 
   "Whse:"               at 1  
   v-whse                at 7 
   "Operator:"           at 12 
   v-putawayinit         at 22 
   /**
   "Receipt Id:"         at 1  dcolor 1
   "  "                  at 12 dcolor 1   
   v-receiptid           at 14 dcolor 1
   **/
   /* " "                   at 1 */
   v-direction           at 26 format "x(1)" no-label 
   "Part#"               at 1  dcolor 2
   v-shipprod            at 2      
      Help "Verify Product Putaway"
   t-shipprod      at 2
   "Description"         at 1 dcolor 2
   t-proddesc1     at 2
   t-proddesc2     at 2
 /* "Putaway Qty:"        at 1  dcolor 2 */
   "Qty Expect:"         at 1  dcolor 2
   " "                   at 13 
   t-stkqtyputaway at 14 
   "Qty Putaway:"           at 1
   t-stkqty              at 14
   v-binloc              at 14       
         Help "Verify Bin Putaway"
   "Binloc1:    "        at 1  dcolor 2
   t-binloc1       at 14
   "Binloc2:    "        at 1  dcolor 2
   t-binloc2       at 14
   " "                   at 1
   x-spce          at 26
   with frame f-hhep width 26 row 1 no-box scroll 1
   no-hide no-labels overlay v-length down.   

form 
   " Status:    "        at 1  dcolor 2
   v-listlit             at 14
   "Binloc1:    "        at 1  dcolor 2
   t-binloc1       at 14
   "Binloc2:    "        at 1  dcolor 2
   t-binloc2       at 14
   with frame f-hhepb width 26 row 10 no-box 
   no-hide no-labels overlay.   


/{&user_vars}*/

/{&B-zz_browse_events}*/
on ctrl-r of frame f-hhep anywhere  do:

  if v-direction = "F" then
    assign v-direction = "R". 
  else
    assign v-direction = "F".
  display v-direction with frame f-hhep.  
end. 
   
on ctrl-r of frame f-hhep-h anywhere  do:

  if v-direction = "F" then
    assign v-direction = "R". 
  else
    assign v-direction = "F".
  display v-direction with frame f-hhep-h.  
end.     
/{&B-zz_browse_events}*/

/{&user_popup_proc}*/
/{&user_popup_proc}*/

/{&user_display1}*/
/{&user_display1}*/

/{&user_display2}*/
  put screen row 13 column 1 color messages "F6Ex F7 Bin2 F8X.i F9Srch ".
/{&user_display2}*/


/{&user_display3}*/
  put screen row 13 column 1 color messages "                          ".
/{&user_display3}*/


/{&user_Query}*/ 
  /*o Process XXEX screen */  
  {x-xxexcustom3.i &file         = "tsum-put"
                    /* "t-put" "zsdiput"   tmp-zsdi summary */
                  &status       = "If not v-bufferedsearch then"
                  &frame        = "f-hhep"             
                  &find         = "hhep.lfi" /* "hhep.lfi"  temp table name */                   &beforechoose = "if v-scrollfl and
                                      not v-scrollmode 
                                    then v-scrollfl = false."
                                     

                  &snchoosecond = "and not v-searchfl" 
                  &field        = "x-spce" 
                  &internalfield  = "null" 
                  &go-on        = 'ctrl-o'      
                  &sneditsecond = " or v-searchfl "
                  &searcher     = "hhep.sch" 
                  &searchat     = "at 1"
                  &usefile      = "hhep.lfu" 
                  &display      = "hhep.lds"  /* need to display tmp or find */                  &delete       = "hhep.del"
                  &delfl        = "*" 
                  &keymove      = "hhep.key"
                  &gofile       = "hhep.gof"
                  &edit         = "hhep.led"}          
                  
/{&user_Query}*/ 

/{&user_aftmain1}*/ 
if {k-jump.i} then do:
  run RFjump.
end.
/{&user_aftmain1}*/ 

/{&user_procedures}*/ 
procedure RFjump:
  run zsdijumprf.p.
end.
/{&user_procedures}*/ 


/{&RcptSelect}*/

/* There is a better, one line way to delete temp-tables, EMPTY */
empty temp-table tmp-rcpt.
empty temp-table tsum-put.
empty temp-table t-put.

/* F11 Re-entry */

find first zsdiput where zsdiput.cono = g-cono and     
                         zsdiput.user2 = g-operinits 
                         no-lock no-error.               
if avail zsdiput and num-entries(zsdiput.xuser3,"~011") ge 5 then do:
  assign v-whse        = entry(1,zsdiput.xuser3,"~011")
         v-direction   = entry(2,zsdiput.xuser3,"~011")
         v-putawayinit = entry(3,zsdiput.xuser3,"~011")
         v-bintpeB    = entry(4,zsdiput.xuser3,"~011")  
         v-bintpeE    = entry(5,zsdiput.xuser3,"~011"). 

  assign g-whse = v-whse.                              

  for each zsdiput use-index k-userid
                        where zsdiput.cono = g-cono and     
                              zsdiput.user2 = g-operinits and
                              zsdiput.whse  = v-whse no-lock
                              break by zsdiput.receiptid:
    if first-of(zsdiput.receiptid) then do:    
      find tmp-rcpt where tmp-rcpt.receiptid = zsdiput.receiptid
            no-lock no-error.
      if not avail tmp-rcpt then do:
        create tmp-rcpt.
        tmp-rcpt.receiptid = zsdiput.receiptid.
      end.
    end.
  end.

  find first tmp-rcpt no-lock no-error. 
  if avail tmp-rcpt then do:            
    v-receiptid = tmp-rcpt.receiptid.   
    loaded-sw = true.
    g-whse = v-whse.
  end.  
  assign v-reload = true.
  run setTempRecords. 
  return.
  end.   
else do: 
  v-receiptid = 0.
  display   
    v-whse
    v-direction            
    v-putawayinit   
    v-receiptid     
    v-bintpeB    
    v-bintpeE   
  with frame f-hhep-h.
  
  run displaytmprcpt("hhep").
  assign     v-completescroll = false
             v-scrollfl = true
             v-scrollmode = false.
  
  
  /**  This already is called repeatedly until loaded-sw = true. **/
  assign h-binsort = "".
  update v-whse /* when v-whse = "" */  when not can-find(first tmp-rcpt)
    v-receiptid
    v-bintpeB
    v-bintpeE
  with frame f-hhep-h
  editing:
    readkey.
    if frame-field = "v-whse"
      and ({k-accept.i} or {k-return.i} or keylabel(lastkey) = "cursor-down")
    then do:
      if v-whse ne "" and can-find(first tmp-rcpt) then do:
        message "Warehouse set".
        display v-whse with frame f-hhep-h.
        next-prompt v-receiptid with frame f-hhep-h.
        next.
      end.
      if can-find(first icsd where icsd.cono = g-cono and icsd.whse =
        input v-whse) then
        assign v-whse = input v-whse.
      else do:
        message "Invalid Warehouse" v-whse.
        next-prompt v-whse with frame f-hhep-h.
        next. 
      end.
      message "".
    end.
    if frame-field = "v-bintpeE" and keylabel(lastkey) = "cursor-down"
    then do:
      v-bintpeE = input v-bintpeE.
      leave. /* leave update else goes to v-whse */
    end.
    if frame-field = "v-receiptid"
      and ({k-accept.i} or {k-return.i} or keylabel(lastkey) = "cursor-down")
    then do:
      /* nothing found or some kind of error */
      find first zsdiput use-index wrcptix where
        zsdiput.cono = g-cono and
        zsdiput.whse = input v-whse and
        zsdiput.receiptid = input v-receiptid  and
        zsdiput.completefl = no and 
        (zsdiput.user2 = "" or zsdiput.user2 = g-operinits) no-lock no-error.
      
      if not avail zsdiput then do:
        message "No items found for receipt " input v-receiptid.
        errFlag = true.
      end.
  
      if errFlag then do:
        errFlag = false.
        release zsdiput.
        next-prompt v-receiptid with frame f-hhep-h.
        next.
      end.
      v-receiptid = input v-receiptid.
      message "".
      
      /* assign loaded-sw = true */
      g-whse = v-whse.                              
      find tmp-rcpt where tmp-rcpt.receiptid = v-receiptid no-lock no-error.
      if not avail tmp-rcpt then do:
        create tmp-rcpt.
        tmp-rcpt.receiptid = v-receiptid.
        run displaytmprcpt("hhep"). 
      end.
      if not ({k-accept.i} or keylabel(lastkey) = "cursor-down")
      then do:
        next-prompt v-receiptid with frame f-hhep-h.
        next. 
      end.
    end. /* if frame-field = v-receiptid */
    apply lastkey.
  end. /* edit loop */

  find first tmp-rcpt no-lock no-error. 
  if avail tmp-rcpt then do:            
    v-receiptid = tmp-rcpt.receiptid.   
    loaded-sw = true.
    g-whse = v-whse.
  end.  
  else do:
    bell.
    message "No Receipts found".
  end.
end. /* Else not F11 */
/* per Tami 04/13/2007
  Multiple Receipts
  order by BinLocation1 for all items */

/{&RcptSelect}*/



/{&user_f9proc}*/
readkey pause 0.
/{&user_f9proc}*/

/{&user_f10proc}*/
/{&user_f10proc}*/

/{&user_errormsg}*/
/{&user_errormsg}*/

/{&user_clearmsg}*/
/{&user_clearmsg}*/

/{&user_headdisplay}*/
/{&user_headdisplay}*/

/{&user_reqnotes}*/
/{&user_reqnotes}*/

/{&b-zz_Browse}*/ 
/{&b-zz_Browse}*/ 

/{&srch_Query}*/ 
/{&srch_Query}*/ 
