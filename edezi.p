
{p-rptbeg.i}

define stream strm.


define var v-fields               as char extent 50 no-undo.
define var p-dateb                as date           no-undo init 12/04/2015.
define var p-datee                as date           no-undo init 12/04/2015.
define var p-exceptfl             as logical        no-undo.
define var v-dateinc              as date           no-undo init 12/04/2015.

define var v-total                like oeeh.totlineamt no-undo.
define var v-ftotal               like oeeh.totlineamt no-undo.
define var v-ftotalT              as dec               no-undo.
define var v-ftax                 as dec               no-undo.
define var v-ftaxT                as dec               no-undo.
  

define temp-table t-invoices
  field orderno          like oeeh.orderno
  field ordref           as char
  field ordernoT         like oeeh.orderno
  field ordersufT        like oeeh.ordersuf
  field custno           like arsc.custno
  field currency         as char format "x(1)"
  field invdt            as date
  field total            as dec
  field totalT           as dec
  field lines            as int
  field linesT           as int
  field frtin            as dec
  field frtinT           as dec
  field frtout           as dec
  field frtoutT          as dec
  field handle           as dec
  field handleT          as dec
  field expedite         as dec
  field expediteT        as dec
  field tax              as dec
  field taxT             as dec
  field matched          as logical
index ix1
      orderno
index ix2
      ordernoT
      ordersufT.



form header
    "Order #"           at 4
    "Inv Date"          at 13
    "Cust #"            at 28
    "Name"              at 40
    "Inv Amt"           at 76
    "Inv Amt"           at 87
    "Tax Amt"           at 100
    "Tax Amt"           at 113
with frame f-hdr1 no-box no-labels width 132 page-top.

form header
    "  JT   "           at 76
    "  SXE  "           at 87
    "  JT   "           at 100
    "  SXE  "           at 113
with frame f-hdr2 no-box no-labels width 132 page-top.


form 
    t-invoices.orderno  at 1
    "-"                 at 9
    t-invoices.ordersuf at 10
    t-invoices.invdt    at 13
    t-invoices.currency at 22
    
    t-invoices.custno   at 27
    arsc.name           at 40
    v-total             at 71
    t-invoices.totalT   at 84
    t-invoices.tax      at 97
    t-invoices.taxT     at 110
with frame f-dtl no-box no-labels width 132.

form 
    "Final Total"        at 1
    v-ftotal             at 71
    v-ftotalT            at 84
    v-ftax               at 97
    v-ftaxT              at 110
with frame f-totl no-box no-labels width 132.
















if sapb.rangebeg[1] ne "" then
  do:
  v-datein = sapb.rangebeg[1].
  {p-rptdt.i}
  
  if string(v-dateout) = v-lowdt then
    p-dateb = 01/01/1900.
  else  
    p-dateb = v-dateout.
  end.
else
  p-dateb = 01/01/1900.

if sapb.rangeend[1] ne "" then
  do:
  v-datein = sapb.rangeend[1].
  {p-rptdt.i}
  if string(v-dateout) = v-highdt then
    p-datee = 12/31/2049.
  else 
   p-datee = v-dateout.
  end.
else
  p-datee = 12/31/2049.
assign p-exceptfl = if sapb.optvalue[1] = "yes" then
                      true
                    else
                      false.

do v-dateinc = p-dateb to p-datee:

  if search("/edi/aiprod/PARAGON/ftp850/backup/InvoiceRegister-"
    + string(year(v-dateinc),"9999") + "-" +
      string(month(v-dateinc),"99") + "-" +
      string(day(v-dateinc),"99") + "-" +
      string(year(v-dateinc),"9999") + "-" +
      string(month(v-dateinc),"99") + "-" +
      string(day(v-dateinc),"99") + ".csv") = ? then do:
    
    display   "/edi/aiprod/PARAGON/ftp850/backup/InvoiceRegister-"
    + string(year(v-dateinc),"9999") + "-" +
      string(month(v-dateinc),"99") + "-" +
      string(day(v-dateinc),"99") + "-"  +
      string(year(v-dateinc),"9999") + "-" +
      string(month(v-dateinc),"99") + "-" +
      string(day(v-dateinc),"99") +  ".csv not found".
    next.
  end.  

   

  
  input stream strm from 
  value("/edi/aiprod/PARAGON/ftp850/backup/InvoiceRegister-"
   + string(year(v-dateinc),"9999") + "-" +
     string(month(v-dateinc),"99") + "-" +
     string(day(v-dateinc),"99") + "-"  +
     string(year(v-dateinc),"9999") + "-" +
     string(month(v-dateinc),"99") + "-" +
     string(day(v-dateinc),"99") +  ".csv").


  import stream strm delimiter "," v-fields.


  repeat:
    assign v-fields = "".
    import stream strm delimiter "," v-fields.

    assign v-date = date (substr(v-fields[2],6,2) + "/" +
                          substr(v-fields[2],9,2) + "/" +
                          substr(v-fields[2],1,4)).
    create t-invoices.
    assign
      t-invoices.ordref           = v-fields[1]
      t-invoices.orderno          = int(entry(1,v-fields[1],"-"))
      t-invoices.ordernoT         = 0
      t-invoices.ordersufT        = 0
      t-invoices.invdt            = v-date
      t-invoices.total            = dec(v-fields[15])
      t-invoices.totalT           = 0
 
      t-invoices.lines            = int(v-fields[17])
      t-invoices.linesT           = 0
      t-invoices.frtin            = dec(v-fields[18])
      t-invoices.frtinT           = 0
      t-invoices.frtout           = dec(v-fields[19])
      t-invoices.frtoutT           = 0
 
      t-invoices.handle           = dec(v-fields[20])
      t-invoices.handleT          = 0
      t-invoices.expedite         = dec(v-fields[21])
      t-invoices.expediteT        = 0
      t-invoices.tax              = dec(v-fields[22])
      t-invoices.taxT             = 0
      t-invoices.matched          = false.
  end.

  input stream strm close.
end.

for each oeeh where 
         oeeh.cono = g-cono  and
         oeeh.invoicedt ge p-dateb and
         oeeh.invoicedt le p-datee no-lock:

  find first t-invoices where
             t-invoices.orderno = oeeh.orderno and
             t-invoices.matched = false  and
             t-invoices.ordref = oeeh.refer no-error.

  if not avail t-invoices then
    find first t-invoices where
               t-invoices.orderno = oeeh.orderno and
               t-invoices.matched = false  
               no-error.

  
  
  
  
  if avail t-invoices then do:
    assign
      t-invoices.custno           = oeeh.custno
      t-invoices.currency         = substring(oeeh.user5,1,1)
      t-invoices.ordernoT         = oeeh.orderno
      t-invoices.ordersufT        = oeeh.ordersuf
      t-invoices.totalT           = oeeh.totlineamt
      t-invoices.linesT           = 0
      t-invoices.frtinT           = 0
      t-invoices.frtoutT          = 0
      t-invoices.handleT          = 0
      t-invoices.expediteT        = 0
      t-invoices.taxT             = (oeeh.taxamt [1] + oeeh.taxamt [2] +
                                     oeeh.taxamt [3] + oeeh.taxamt [4])
      t-invoices.matched          = true.


    for each addon where 
             addon.cono = g-cono and
             addon.ordertype = "oe" and
             addon.orderno   = oeeh.orderno and
             addon.ordersuf  = oeeh.ordersuf no-lock:
      if addon.addonno = 1 then 
        assign t-invoices.frtinT = t-invoices.frtinT + 
                                   (addon.addonnet *
                                     if oeeh.transtype = "rm" then
                                       -1
                                     else
                                       1).

      if addon.addonno = 2 then 
        assign t-invoices.frtoutT = t-invoices.frtoutT + 
                                   (addon.addonnet *
                                     if oeeh.transtype = "rm" then
                                       -1
                                     else
                                       1).

    end.



    if oeeh.divno = 40 and oeeh.user5 = "C" and 
       oeeh.currencyty <> "" then do:
      find first sastc where sastc.cono = g-cono and                                           sastc.currencyty = (if oeeh.currencyty = "" and         
                                        oeeh.user5 = "c" and             
                                       oeeh.divno = 40 then             
                                       "CN"                             
                                     else                                
                                       oeeh.currencyty) no-lock no-error.
      if avail sastc then do:    
        assign t-invoices.taxT  =  round( t-invoices.taxT *                                                        (if oeeh.user6 <> 0 then                                                           oeeh.user6
                                     else                                
                                       round((1  / sastc.purchexrate),5)),2)

              t-invoices.totalT  =  round( t-invoices.totalT *                                                        (if oeeh.user6 <> 0 then                                                           oeeh.user6
                                     else                                
                                       round((1  / sastc.purchexrate),5)),2).
      

              t-invoices.frtinT  =  round( t-invoices.frtinT *    
                                   (if oeeh.user6 <> 0 then                                                           oeeh.user6
                                    else                                
                                      round((1  / sastc.purchexrate),5)),2).
      
 
              t-invoices.frtoutT  =  round( t-invoices.frtoutT *                                                        (if oeeh.user6 <> 0 then                                                          oeeh.user6
                                        else                                
                                          round((1  / sastc.purchexrate),5)),2).
      
       
      end.

   end.                                                     
                  

    
      
      
  end.    
    
end.    

view frame f-hdr1. 
view frame f-hdr2. 



for each t-invoices where t-invoices.matched :
  find arsc where 
       arsc.cono = g-cono and 
       arsc.custno = t-invoices.custno no-lock no-error.            

  assign v-total =     (t-invoices.total +
                        t-invoices.handle + t-invoices.expedite).  
 
  assign 
    v-ftotal   = v-ftotal + v-total
    v-ftotalT  =  v-ftotalT + t-invoices.totalT
    v-ftax     =  v-ftax    + t-invoices.tax
    v-ftaxT    =  v-ftaxT   + t-invoices.taxt.
 

  if t-invoices.tax <> t-invoices.taxT or
       (t-invoices.total +
        t-invoices.handle + t-invoices.expedite)  <> t-invoices.totalT and
      p-exceptfl = true then do:
  end.
  else if p-exceptfl then
    next.
     

  assign v-total =     (t-invoices.total +
                        t-invoices.handle + t-invoices.expedite).  
  display 
    t-invoices.orderno  
    t-invoices.ordersuf
    t-invoices.invdt   
    t-invoices.currency
    t-invoices.custno
    arsc.name  
    v-total   
    t-invoices.totalT   
    t-invoices.tax     
    t-invoices.taxT  
  with frame f-dtl.
  down with frame f-dtl.

end.

display 
    v-ftotal    
    v-ftotalT  
    v-ftax   
    v-ftaxT  
with frame f-totl.


