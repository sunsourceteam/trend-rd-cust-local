/* version 5.5 */
/* p-oeepa9.i */
/*h*****************************************************************************
  INCLUDE      : p-oeepa9.i
  DESCRIPTION  : OEEPA/OEEPAF main print logic
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    10/14/91 mwb; TB# 4762  Change page#, order #, shipped date and ship via
        for pre-printed forms
    04/03/92 pap; TB# 6219  Line DO  add DO line indicator adjusted qty
        backordered
    06/06/92 mwb; TB# 6889  Replaced oeeh.payamt with oeeh.tendamt.
    06/16/92 mkb; TB# 5929  Full amount tendered, add write off amount to
        downpayment
    06/20/92 mwb; TB# 5413  If the header as a write off amount set - add it
        to the down payment display (BO rounding problems when down
        payment is applied).
    08/05/92 pap; TB# 7245  Do not print the backordered qty or the shipped
        qty on an acknowledgement
    08/12/92 pap;           REVERSAL of TB# 7245 BY DESIGN MEETING
    11/11/92 mms; TB# 8561  Display product surcharge label from ICAO
    12/07/92 ntl; TB# 9041  Canadian Tax Changes (QST)
    03/18/93 jlc; TB# 7245  Items with a quantity shipped of 0 are printing a
        quantity shipped and a quantity backordered.
    04/07/93 jlc; TB# 8890  Drop Ship orders should print Drop Ship for
        the Ship Point.
    04/08/93 jlc; TB# 10155 Print customer product.
    04/08/93 jlc; TB# 9727  Don't print core chg and restock amt for a
        non-print line.
    04/10/93 jlc; TB# 7405  Print DROP for the qty ship on drop lines.
    06/07/93 jlc;           Changes to tb# 7245 per Design Committee.
    02/21/94 dww; TB# 13890 File name not specified causing error
    06/23/95 ajw; TB# 18575 Nonstock/Special Kit Component Project (G1)
    08/17/95 tdd; TB# 17771 Notes print twice when reference component
    11/01/95 gp;  TB# 18091 Expand UPC Number (T24)
    11/06/95 gp;  TB# 18091 Expand UPC Number (T24)
    02/27/97 jkp; TB#  7241 (5.1) Spec8.0 Special Price Costing.  Added call
        of speccost.gva.  Added find of icss record.  Added call to
        speccost.gas to determine prccostper.  Replaced icsp.prccostper with
        v-prccostper.
    10/22/98 jgc; TB# 9731 Expand Non-stock description to 2 lines.
    12/23/98 cm;  TB# 14835 Allow notes specific by suffix
    05/13/99 lbr; TB# 5366  Added shipto notes
    10/12/99 cad; TB# e2761 Tally/Mix Project
    03/29/00 rgm; TB# 3-2  Add RxServer Interface to Standard
    05/08/00 rgm; TB# A9   Fix for RxServer Faxing issue
    02/09/01 jd1; TB# 7852 Add Multicurrency Text
    11/05/01 ajw; TB# e10078 OE Usability - Print OEELC Comments.
    11/05/01 ajw; TB# e10078 OE Usability - Honor Header Print Price Flag.
    01/15/02 lbr; TB# e3207 Added param to oeepp1n
    02/08/02 dls; TB# e11973 OE Usability - New Subtotal logic from oeecl
    03/15/02 sbr; TB# e7966 Customer part # xref not printing
    04/01/02 mwb; TB# e3207 Added component catalog notes display.
    04/24/02 des; TB# e12903 OE Usability: BMat Project Updates
    08/08/02 des; TB# e11863 Specify where notes print by page
    10/10/02 jbt; TB# e11863 Call generic notes printing program.
    11/12/02 kjb; TB# e15321 Modify the Tally print logic - Replace the
        existing tally print logic with new logic found in tallyprt.ldi
    12/06/02 dls; TB# e13523 Additonal Addons in OE
    03/25/03 jkp; TB# e16626 Add option to select Ordered or Shipped Quantity
        in extended lines.
    04/14/03 jkp; TB# e16626 Correct use of t-addon file.
    04/15/03 jkp; TB# e16626 Add :u where appropriate for changes
    04/17/03 lcb; TB# t14244 8Digit Ord No Project
    06/02/03 jkp; TB# e16626 Correct downpayment calculation
    08/11/03 rgm; TB# e18072 add Language code to RxServer output as needed
    04/23/04 bpa; TB# e17505 Add ICSEC.ADDPRTINFO to Customer Prod line
    12/05/04 bp ; TB# e21580 Add user hooks.
    08/18/05 bp ; TB# e22915 Add user hooks.
    02/17/06 bp ; TB# e23917 Add user hooks.
    02/23/06 bpa; TB# e22953 ADDPRTINFO is being pulled from ship prod. Should
        be pulled from requested prod
    02/28/06 bp ; TB# e23917 Add user hooks.
    03/14/06 sbr; TB# e23020 Demand Ack not honoring print QTY ORD
    03/30/06 ejk; TB# e23741 Canadian taxes wrong when option #9 is Yes.
    02/16/07 jhb; TB# e25103 Fixed amounts and totals for orders with zero
        total qty shipped when option set to not use ordered quantities only
    11/01/07 rgm; ERP-27446  Addons not calculated fro some orders
    03/18/08 sbr; ERP-20750  Restocking amount not correct when option #9 is yes
    03/18/08 emc; ERP-32718  Quotes not printing totals
    03/18/08 sbr; ERP-35932  Option 9 = yes not working if there are addons
    04/11/08 sbr; ERP-35921  No Canadian taxes or addons when option #9 is no
    04/22/08 sbr; ERP-38202  Canadian tax wrong when partial ship & option 9 yes
    06/10/09 das; NonReturn/NonCancelable
    06/12/09 das; Allow Tendered Invoices to Print
******************************************************************************/

/*tb  7241 (5.1) 02/27/97 jkp; Added call to speccost.gva. */
{speccost.gva}

def new shared var v-idoeeh as recid                         no-undo.
{g-oeetts.i "new"}
{addon-tt.i &new = "new"
            &shared = "shared"}

def var v-outinvamt  like oeeh.totinvamt                      no-undo.
def var v-outinvord  like oeeh.totinvamt                      no-undo.
def var v-gsttaxamt  like oeeh.taxamt[4]                      no-undo.
def var v-psttaxamt  like oeeh.taxamt[1]                      no-undo.
def var v-qtyship    as   decimal                             no-undo.
def var s-tariffmsg    as c format "x(72)"                    no-undo.
/* das - NonReturn */
def var s-asterisk     as c format "x(4)"                     no-undo.
def var s-asteriskdtla as c format "x(105)"                   no-undo.
def var s-asteriskdtlb as c format "x(125)"                   no-undo.
def var s-asteriskdtlc as c format "x(125)"                   no-undo.
def var s-buysignature as c format "x(80)"                    no-undo.
def var s-sunsignature as c format "x(80)"                    no-undo.


/* smaf - BL info */
def var s-ordernbr like oeeh.orderno                          no-undo.
def var s-literal  as char format "x(10)"                     no-undo.
def shared var v-blprt as logical                             no-undo.

def var v-1019fl as logical          no-undo.

form
 s-message1 as char format "x(56)"  at 20
 s-message2 as char format "x(50)"  at 76
 skip(1)
with frame f-messages no-box no-labels width 132 no-underline.


form
 s-title1   as char format "x(10)"   at 13 label "Order#"
 s-titles2  as char format "x(24)"   at 30 label "Product"
 s-title3   as char format "x(8)"    at 60 label "Quantity"
 s-title4   as char format "x(20)"   at 81 label "Release Date"
with frame f-titles no-box no-labels width 132 no-underline. 


form 
 s-reloeno   as char format "x(10)"    at 13   label "Order#"
 s-relprod   as char format "x(24)"    at 30   label "Product"
 s-relqty    as i format "zzzzz9"      at 60   label "Quantity"
 s-reldate   as date                   at 81   label "Release Date"
with frame f-reldetail no-box no-labels width 132 no-underline.
 
/* smaf - form */


/* smaf -  tax  */

form
 s-taxtitle as char format "x(15)"   at 82  
 s-vartax   as char format "x(15)"   at 118 
with frame f-vartax no-box no-labels width 132 no-underline.

 
 
 

def var v-addonloadfl as log format "yes/no"                  no-undo.
/*tb  5929 06/18/92 mkb */
def buffer            b-oeeh   for oeeh.
/* SX55 */
def var zx-handle     as logical no-undo.
def var sdi-frthdlamt as dec     no-undo.
def var sdi-ParishTax as dec     no-undo.
def var cod-freight   as de format ">>>,>>9.99" no-undo.
def var skip-sig      as logical init no        no-undo.
def var w-date        like oeeh.enterdt         no-undo.

/*tb 3-2 03/29/00 rgm; Rxserver interface display vars for rxserver */
form header  /* main title lines  */
    skip(3)
    s-lit1aa as c format "x(10)"         at 2
    s-lit1a  as c format "x(50)"         at 12
    s-cancel as c format "x(25)"         at 64
    s-lit1b  as c format "x(37)"         at 93
    s-lit2a  as c format "x(53)"         at 2
    /* SX55
    b-sasc.upcvno                        at 95
    oeeh.invoicedt                       at 108
    */
    /* SX55 */
    oeeh.orderno                         at 122
    "-"                                  at 130
    oeeh.ordersuf                        at 131
    s-lit3a as c format "x(09)"          at 2
    oeeh.custno                          at 12
    v-currency                           at 25  /*tb 19426 02/09/01 jd1 */
    s-lit3b as c format "x(39)"          at 94
    /* SX55 */
    s-litattn as c format "X(30)"        at 2  /*SI02*/
    oeeh.enterdt                         at 94
    oeeh.custpo                          at 104
    page-num - v-pageno format "zzz9"    at 129
    skip(1)
    s-lit6a as c format "x(8)"           at 2
    s-billname like arsc.name            at 12
    s-lit6b as c format "x(18)"          at 48
    /* SX55
    b-sasc.conm                          at 68
    */
    s-remitnm as c format "x(24)"        at 68   /* ITB# 2 */
    s-billaddr1 like arsc.name           at 12
    /* SX55
    b-sasc.addr[1]                       at 68
    */
    s-lit6bb as c format "x(6)"          at 48   /* ITB# 2 */
    s-remit-addr1 as c format "x(24)"    at 68   /* ITB# 2 */ 
    s-billaddr2 like arsc.name           at 12
    /* SX55
    b-sasc.addr[2]                       at 68
    */
    s-remit-addr2 as c format "x(24)"    at 68   /* ITB# 2 */
    s-lit8a as c format "x(9)"           at 106
    s-pstlicense as c format "x(11)"     at 116
    s-billcity as c format "x(35)"       at 12
    s-corrcity as c format "x(35)"       at 68
    s-lit9a as c format "x(9)"           at 106
    s-gstregno as c format "x(10)"       at 116
    /* have to comment out for SDI SX55 frame to work. 
    /*tb A9 05/08/00 rgm; Rxserver interface change when appropriate */
    &IF DEFINED(rxserver) = 0 &THEN
        skip(1)
    &ELSE
        s-rxtext1                        at 1
    &ENDIF
    */
    /* SX55 */
    /*SI02 beg*/
    /*skip(1)*/
    s-litbfax as c format "X(30)"        at 12
    /*SI02 end*/

    s-lit11a as c format "x(8)"          at 2
    s-shiptonm like oeeh.shiptonm        at 12
    s-lit11b as c format "x(12)"         at 50
    s-shiptoaddr[1]                      at 12
    oeeh.shipinstr                       at 50
    s-shiptoaddr[2]                      at 12
    s-lit12a as c format "x(10)"         at 50
    s-lit12b as c format "x(3)"          at 82
    s-lit12c as c format "x(7)"          at 104
    s-lit12d as c format "x(5)"          at 117
    s-shipcity as c format "x(35)"       at 12
    s-whsedesc as c format "x(30)"       at 50
    s-shipvia as c format "x(12)"        at 82
    s-cod as c format "x(6)"             at 95
    s-shipdt like oeeh.shipdt            at 104
    s-terms as c format "x(12)"          at 117
    /* have to comment out for SDI SX55 frame to work. 
    &IF DEFINED(rxserver) = 0 &THEN
        skip(1)
    &ELSE
        s-rxhead1                        at 1            /*line 16*/
    &ENDIF
    */
    /* SX55 */
    /*SI02 beg*/
    /*skip(1)*/
    s-litsfax as c format "X(30)"        at 12
    /*SI02 end*/
    
    s-rpttitle like sapb.rpttitle        at 1 /*skip(1)*/
    skip(3)                                    /* das - NonReturn - added */
    s-asterisk                           at 24 /* skip(2) and s-asterisk  */
with frame f-head no-box no-labels
           &IF DEFINED(rxserv_width) = 0 &THEN
               width 132
           &ELSE
               width {&rxserv_width}
           &ENDIF
           page-top.

form
    s-tariffmsg                          at  3
    skip(1)                        
    s-asteriskdtla                       at  3
    s-asteriskdtlb                       at  3
    s-asteriskdtlc                       at  3
    skip(1)
    s-buysignature                       at 50
    skip(1)
    s-sunsignature                       at 50
with frame f-footnote no-box no-labels
           &IF DEFINED(rxserv_width) = 0 &THEN
               width 132
           &ELSE
               width {&rxserv_width}
           &ENDIF.



/*tb 3-2 03/29/00 rgm; Rxserver interface display vars for rxserver */
/*e these have to be defined outside frame because they
    get assigned in a-oeepa1.i
*/
def var s-lit14a as c format "x(75)"        no-undo.
def var s-lit14b as c format "x(53)"        no-undo.
def var s-lit15a as c format "x(75)"        no-undo.
def var s-lit15b as c format "x(53)"        no-undo.
def var s-lit16a as c format "x(78)"        no-undo.
def var s-lit16b as c format "x(53)"        no-undo.

/*tb 3-2 03/29/00 rgm; Rxserver interface display vars for rxserver */
form header  /* detail line title */
    &IF DEFINED(rxserver) = 0 &THEN
        s-lit14a                             at 1
        s-lit14b                             at 76
        s-lit15a                             at 1
        s-lit15b                             at 76
        s-lit16a                             at 1
        s-lit16b                             at 79
    &ELSE
        s-rxhead2                            at  1       /*line 19*/
        s-rxhead3                            at  1       /*line 20*/
        s-rxhead4                            at  1       /*line 21*/
    &ENDIF
with frame f-headl no-box no-labels
           &IF DEFINED(rxserv_width) = 0 &THEN
               width 132
           &ELSE
               width {&rxserv_width}
           &ENDIF
           page-top.

form  /* detail line totals  */
    skip(1)
    v-linecnt         format "zz9"       at 1
    s-lit40a     as c format "x(11)"     at 5
    s-lit40c     as c format "x(17)"     at 44
    s-totqtyshp  as c format "x(13)"     at 62
    /* SX55 
    s-lit40d     as c format "x(18)"     at 97
    */
    s-lit40d     as c format "x(18)"     at 82
    s-totlineamt as c format "x(13)"     at 117
with frame f-tot1 no-box no-labels no-underline width 132.

form  /* all extra costs display  */
    /* SX55
    s-title2 as c format "x(18)"           at 97
    */
    s-canlit as c   format "x(60)"         at 15    /*si02*/
    s-title2 as c format "x(33)"           at 82
    s-amt2   as dec format "zzzzzzzz9.99-" at 117
with frame f-tot2 no-box no-labels width 132 no-underline down.

/* SX55*/
form  /* all extra costs display  */
    s-canlit  format "x(60)"               at 15    /*si02*/
    s-title2  format "x(33)"               at 82
with frame f-tot2a no-box no-labels width 132 no-underline down.

/* SX55 
form  /* total invoice amount */
    s-invtitle as c format "x(18)"         at 97
    s-totinvamt like oeeh.totinvamt        at 117
with frame f-tot3 no-box no-labels no-underline width 132.
*/

form  /* invoice total*/
    s-currencyty as c format "x(51)"     at 24      /*si02*/
    s-invtitle as c format "x(18)"       at 82
    s-totinvamt like oeeh.totinvamt      at 117
with frame f-tot3 no-box no-labels no-underline width 132.

/*tb 3-2 03/29/00 rgm; Rxserver interface display vars for rxserver */
form header  /* footter at last page */
    s-lit48a as c format "x(9)"          at 1
    /* SX55 */
    s-CSR-title  as c format "x(25)"         at 20
    s-contact    as c format "x(30)"         at 46
    s-contlit-ph as c format "x(4)"          at 82
    s-contact-ph as c format "(xxx)xxx-xxxx" at 87
    s-contlit-fx as c format "x(4)"          at 102
    s-contact-fx as c format "(xxx)xxx-xxxx" at 107
    &IF DEFINED(rxserver) <> 0 &THEN
        s-rxfoot1                        at 132
    &ENDIF                                    
with frame f-tot4 no-box no-labels
           &IF DEFINED(rxserv_width) = 0 &THEN
               width 132
           &ELSE
               width {&rxserv_width}
           &ENDIF
           page-bottom.

/*tb# 5929 06/18/92 mkb */
form
    "Full Amount Tendered For All Orders:"  at 5
    oeeh.tottendamt                         at 42
    "*** Back Order/Release Exists ***"     at 5
with frame f-tot5 no-box no-labels width 132.


assign
    v-pageno    = page-number - 1
    v-subnetamt = 0
    o-prtord    = p-prtord
    p-prtord    = if (can-do("fo,qu,st,bl":u,oeeh.transtype) or
                      can-do("s,t":u,oeeh.orderdisp)) then yes
                  else if oeeh.stagecd < 4 then p-prtord
                  else no.

    {a-oeepa9.i s-}

if p-prtord then
    {p-oeord.i "oeeh."}    /*** v-totlineamt ***/
else
    {p-oeord.i "oeeh." "/*"}    /*** v-totlineamt ***/

/* SX55 */  
{w-arss.i oeeh.custno oeeh.shipto no-lock}  /*SI02*/

 if oeeh.stagecd >= 0 and oeeh.stagecd <= 2 and
    oeeh.termstype <> "cod" then do: 
  assign sdi-frthdlamt = 0
         sdi-ParishTax = 0.   
  for each addon use-index k-seqno where
           addon.cono      = g-cono         and
           addon.ordertype = "oe":u         and
           addon.orderno   = oeeh.orderno   and
           addon.ordersuf  = oeeh.ordersuf
           no-lock:
    /** SX55 sdi specific no frt/hdl added to invoice total  **/
    if addon.addonno  <= 7  and 
       addon.addonno  <> 12 and
       addon.addonno  <> 13 and
       addon.addonnet <> 0 then 
       assign sdi-frthdlamt = sdi-frthdlamt + addon.addonnet.   
    /* das - 10/02/13 - Parish Tax */
    if (addon.addonno = 12 or addon.addonno = 13) and 
      addon.addonnet <> 0 and oeeh.whse = "DBAT" then
      assign sdi-ParishTax = sdi-ParishTax + addon.addonnet.
  end. 
 end. 

/*d if option to print ordered only is yes and order has not been invoiced
    then get the ordered amounts */
if p-prtord then do:

    /*d build temp-table t-addon for existing addons for the order. Needed
        in oeettot.p*/

    {oe-t-addon.i &ordertype = ""oe""
                  &orderno   = oeeh.orderno
                  &ordersuf  = oeeh.ordersuf
                  &lock      = "no-lock"}
    
    v-idoeeh = recid(oeeh).

    run oeettot.p("o":u, output v-outinvamt, output v-outinvord).

    {p-oeord.i "oeeh."}

    /* if this is true, then addons were not loaded from oeettot as
       no recalculate logic was run */
    if oeeh.totlineord = v-totlineamt then
        v-addonloadfl = no.
    else
        v-addonloadfl = yes.
end. /* if p-prtord then do */

if oeeh.transtype = "qu" then
  assign s-lit40c = "Qty Quoted Total ".
else
  assign s-lit40c = "Qty Ordered Total".

{w-arss.i oeeh.custno oeeh.shipto no-lock}  /*SI02*/

/*tb 5413 06/20/92 mwb */
assign
    s-billname   = if v-invtofl then oeeh.shiptonm
                   else arsc.name
    s-billaddr1  = if v-invtofl then oeeh.shiptoaddr[1]
                   else arsc.addr[1]
    s-billaddr2  = if v-invtofl then oeeh.shiptoaddr[2]
                   else arsc.addr[2]
    s-billcity   = if v-invtofl then
                       oeeh.shiptocity + ", " + oeeh.shiptost +
                       " " + oeeh.shiptozip
                   else arsc.city + ", " + arsc.state + " " + arsc.zipcd
    /* SX55 */
    /*SI02*/
   s-litbfax    = if oeeh.divno = 40 and
                   length(arsc.faxphoneno)  > 1 then "Fax: " +
                   string(arsc.faxphoneno,"(xxx)xxx-xxxx xxxxx") else ""
/*    s-corrcity   = b-sasc.city + ", " + b-sasc.state + " " + b-sasc.zipcd */
/*    s-cod        = if oeeh.codfl = no then "" else "C.O.D." */
    s-shipdt     = if oeeh.shipdt = ? then oeeh.pickeddt else oeeh.shipdt.

if p-prtord then
assign
    s-totlineamt = if oeeh.lumpbillfl then
                       string(oeeh.lumpbillamt,"zzzzzzzz9.99-")
                   else if oeeh.transtype = "rm":u then
                       string((s-ordlineamt * -1),"zzzzzzzz9.99-")
                   else string(s-ordlineamt,"zzzzzzzz9.99-")
    s-taxes      = s-ordtaxamt + sdi-ParishTax
    s-dwnpmtamt  = if oeeh.transtype = "cs" then oeeh.tendamt
                   else if oeeh.dwnpmttype then oeeh.dwnpmtamt
                   else (oeeh.dwnpmtamt / 100) * oeeh.totinvamt
    v-gsttaxamt  = s-ordgsttaxamt
    v-psttaxamt  = s-ordpsttaxamt.

else
assign
    s-totlineamt = if oeeh.lumpbillfl then
                      string(oeeh.lumpbillamt,"zzzzzzzz9.99-")
                   else if oeeh.transtype = "rm":u then
                      string((oeeh.totlineamt * -1),"zzzzzzzz9.99-")
                   else string(v-totlineamt,"zzzzzzzz9.99-")
    s-taxes      = if oeeh.totqtyshp <> 0 then
                      oeeh.taxamt[1] + oeeh.taxamt[2] +
                      oeeh.taxamt[3] + oeeh.taxamt[4] + sdi-ParishTax
                   else 0
    s-dwnpmtamt  = if oeeh.transtype = "cs" then oeeh.tendamt
                   else if oeeh.dwnpmttype then oeeh.dwnpmtamt
                   else (oeeh.dwnpmtamt / 100) * oeeh.totinvamt
    v-gsttaxamt  = oeeh.taxamt[4]
    v-psttaxamt  = oeeh.taxamt[1].

assign
    s-dwnpmtamt  = s-dwnpmtamt + oeeh.writeoffamt
    v-linecnt    = 0
    s-shiptonm      = oeeh.shiptonm
    s-shiptoaddr[1] = oeeh.shiptoaddr[1]
    s-shiptoaddr[2] = oeeh.shiptoaddr[2]
    s-shipcity      = oeeh.shiptocity + ", " + oeeh.shiptost +
                      " " + oeeh.shiptozip
    /* SX55 */
    /*SI02*/
    s-litsfax    = if oeeh.divno = 40 and avail arss and 
                   length(arss.faxphoneno) > 1 then
                   "Fax: " + string(arss.faxphoneno,"(xxx)xxx-xxxx xxxxx") else
                   if oeeh.divno = 40 and 
                   not avail arss and 
                   s-billname = s-shiptonm and
                   s-billaddr1 = s-shiptoaddr[1] and
                   s-billaddr2 = s-shiptoaddr[2] and
                   s-billcity = s-shipcity and
                   length(arsc.faxphoneno) > 1 then
                   "Fax: " + string(arsc.faxphone,"(xxx)xxx-xxxx xxxxx") 
                   else ""
    s-gstregno      = if g-country = "ca" then b-sasc.fedtaxid else ""
    s-pstlicense    = if g-country = "ca" then oeeh.pstlicenseno
                      else "".

/* {p-oeadfp.i}  /* load addon descriptions */ */

{w-sasta.i "S" oeeh.shipvia no-lock}

{w-icsd.i oeeh.whse no-lock}

/* SX55 */
/* ITB# 2 */
assign s-corrcity = icsd.city + ", " + icsd.state + " " + icsd.zipcd
       s-remitnm = icsd.name
       s-remit-addr1 = icsd.addr[1]
       s-remit-addr2 = icsd.addr[2].

/*tb 8890 04/07/93 jlc; DO's print Drop Ship for Ship Point */
assign
    s-shipvia  = if avail sasta then sasta.descrip else oeeh.shipvia
    s-whsedesc = if oeeh.transtype = "do" then "** Drop Ship **"
                 else if avail icsd then icsd.name
                 else oeeh.whse.

{w-sasta.i "t" oeeh.termstype no-lock}

if oeeh.langcd <> "" then do:
    {w-sals.i oeeh.langcd "t" oeeh.termstype no-lock}
end.

assign
    s-terms    = if oeeh.langcd <> "" and avail sals then sals.descrip[1]
                 else if avail sasta then sasta.descrip
                 else oeeh.termstype
    s-rpttitle = if p-promofl then sapb.rpttitle else "".

/*tb 3-2 03/29/00 rgm; Rxserver interface display vars for rxserver */
/*tb A9 05/08/00 rgm; Rxserver interface change when appropriate */
&IF DEFINED(rxserver) <> 0 &THEN
    {w-sasc.i no-lock}
    if avail oeeh then do:
        {w-sasgs.i oeeh.statecd no-lock}
        {w-arsc.i oeeh.custno no-lock}
    end.

    assign
        s-rxhead1 = ""
        overlay(s-rxhead1,1,8)   = string(g-today)
        overlay(s-rxhead1,10,5)  = string(time,"hh:mm")
        overlay(s-rxhead1,16,4)  = oeeh.takenby
        overlay(s-rxhead1,20,4)  = oeeh.slsrepin
        overlay(s-rxhead1,24,4)  = oeeh.slsrepout
        overlay(s-rxhead1,28,10) = oeeh.placedby
        overlay(s-rxhead1,38,24) = oeeh.custpo
        overlay(s-rxhead1,62,24) = oeeh.refer
        overlay(s-rxhead1,86,8)  = if oeeh.enterdt <> ?
                                   then string(oeeh.enterdt)
                                   else ""
        overlay(s-rxhead1,94,8)  = if oeeh.pickeddt <> ?
                                   then string(oeeh.pickeddt)
                                   else ""
        overlay(s-rxhead1,102,8) = if oeeh.shipdt <> ?
                                   then string(oeeh.shipdt)
                                   else ""
        overlay(s-rxhead1,110,20) = "Date/Time Printed"
        overlay(s-rxhead1,130,10) = "Taken By"
        overlay(s-rxhead1,140,10) = "Slsrep In"
        overlay(s-rxhead1,150,10) = "Slsrep Out"
        overlay(s-rxhead1,160,10) = "Placed By"
        overlay(s-rxhead1,170,20) = "Customer PO#"
        overlay(s-rxhead1,190,10) = "Reference"
        overlay(s-rxhead1,200,10) = "Entered"
        overlay(s-rxhead1,210,10) = "Picked"
        overlay(s-rxhead1,220,10) = "Shipped"
        overlay(s-rxhead1,230,2) = if avail oeeh then oeeh.langcd
                                    else ""
        overlay(s-rxhead1,232,15) = if avail sasc then sasc.fedtaxid
                                    else ""
        overlay(s-rxhead1,247,15) = if avail sasgs then sasgs.taxid
                                    else ""
        overlay(s-rxhead1,262,2) = if avail arsc then arsc.currencyty
                                    else ""

        s-rxtext1                 = s-lit14a
        overlay(s-rxtext1,79)     = s-lit14b
        overlay(s-rxtext1,133)    = s-lit15a
        overlay(s-rxtext1,211)    = s-lit15b.

&ENDIF
assign s-tariffmsg   = "Any applicable surcharge not mentioned above "  +
                       "will be applied at shipping".

assign s-asterisk = "(**)".
assign s-asteriskdtla = 
 "**  Products noted with an (**) next to the line item are non-cancellable" +
 " and non-returnable.".
assign s-asteriskdtlb = 
 "    All other products are subject to cancellation or restock charges." +
 "    All sales are subject to the".
assign s-asteriskdtlc =
 "    SunSource Terms and Conditions of Sale &" +
 " Warranty available at www.sun-source.com/terms-and-conditions.pdf.".

assign skip-sig = no
       w-date   = ?.
if avail arsc then
  assign w-date = date(substr(arsc.user10,10,8)).
if w-date >= TODAY then
  assign skip-sig = yes.

if oeeh.transtype <> "QU" and skip-sig = no then
  assign s-buysignature =
"Authorized Signature of Purchaser _________________________ Date ___________"
         s-sunsignature =
"Authorized Signature of SunSource _________________________ Date ___________".
else
  assign s-buysignature = " "
         s-sunsignature = " ".

/*tb 3-2 03/29/00 rgm; Rxserver interface hook for extra vars */
{oeepa1.z99 &before_headview = "*"}

view frame f-head.

/*tb 3-2 03/29/00 rgm; Rxserver interface display vars for rxserver */
&IF DEFINED(rxserver) = 0 &THEN
    if oeeh.transtype = "ra" then hide frame f-headl.
    else view frame f-headl.
&ELSE
    view frame f-headl.
&ENDIF

view frame f-tot4.


/* print the customer notes  */
if arsc.notesfl ne "" then
    run notesprt.p(g-cono,"c",string(oeeh.custno),"","oeepa",8,2).

/*tb 5366 05/12/99 lbr; Added shipto notes flag */
if avail arss and arss.notesfl <> "" then
    run notesprt.p(g-cono,"cs",string(oeeh.custno),oeeh.shipto,"oeepa",8,2).

/* print the order notes */
if oeeh.notesfl ne "" then
    run notesprt.p(g-cono,"o",string(oeeh.orderno),string(oeeh.ordersuf),
                     "oeepa",8,2).

{oeepa1.z99 &before_line = "*"}

/*  a receipt on acct has no lines */
if oeeh.transtype <> "ra" then do:

    /****** print external comments and subtotals for line #0 *****/
    {oeelc.gpr &orderno      = "oeeh.orderno"
               &ordersuf     = "oeeh.ordersuf"
               &lineno       = "0"
               &linetype     = 'e'
               &linetype1    = 't'
               &displayname  = "@ s-descrip"
               &framename    = "f-comments"
               &subdispname  = "@ s-subtotdesc"
               &subframename = "f-oeelsub"}

    v-totqtyshp = 0.

    for each oeel use-index k-oeel where
        oeel.cono        = g-cono       and
        oeel.orderno     = oeeh.orderno and
        oeel.ordersuf    = oeeh.ordersuf and
        oeel.specnstype <> "l"
    no-lock:

        /*tb 18091 11/01/95 gp;  Find Product section of UPC number */
        if oeel.arpvendno <> 0 then
            {icsv.gfi "'u'" oeel.shipprod oeel.arpvendno no}
        else
            {icsv.gfi "'u'" oeel.shipprod oeel.vendno no}

        /*tb  7241 (5.1) 02/27/97 jkp; Added find of icss record based on
             oeel because the current product is needed. */
        {icss.gfi
            &prod        = oeel.shipprod
            &icspecrecno = oeel.icspecrecno
            &lock        = "no"}

        /*tb  7241 (5.1) 02/27/97 jkp; Added call to speccost.gas to
             determine v-prccostper. */
        assign
            {speccost.gas
                &com = "/*"}
            v-linecnt = v-linecnt + 1
            v-netord  = if p-prtord then oeel.netord
                        else oeel.netamt
            v-qtyord  = if p-prtord then oeel.qtyord
                        else oeel.qtyship.

        /* SX55 */
        /* SunFix0927 */
        if oeel.botype = "d" and v-qtyord = 0 and
           oeeh.stagecd < 2 and 
           (not can-do("fo,qu,st,bl",oeeh.transtype))  and
           (not  can-do("s,t",oeeh.orderdisp))  and
           oeeh.boexistsfl = no  then
          v-qtyord  = oeel.qtyord.

        /* SunFix0927 */
        
        if oeel.specnstype ne "n" then do:
            {w-icsw.i oeel.shipprod oeel.whse no-lock}
            {w-icsp.i oeel.shipprod no-lock}

            if oeeh.langcd <> "" then
                {w-sals.i oeeh.langcd "p" oeel.shipprod no-lock}

        end.
            else
            /* tb e3207 Added catalog read for notes */
            {w-icsc.i oeel.shipprod no-lock} .

        clear frame f-oeel.

        /*tb 1517 01/24/91 mwb; ship comp, tag&hold always uses ordered */
        /*tb 1043 01/26/91 mwb; BO stg 1 */
        /* mms 01/03/92 JIT print quantity to be backordered */
        /*tb 6219 04/03/92 pap; Line DO - incorporate 'd' botype */
        /*tb 7245 03/18/93 jlc; Printing a quantity shipped when the item was
            completely backordered */
        /*tb 7405 04/10/93 jlc; Print DROP for the qty ship on DOs */
        /*tb 7245 06/07/93 jlc; Per design committee, if p-oeordl.i then
            s-qtyship, s-qtybo should be blank */
        /*tb 18091 11/01/95 gp; Assign product section of UPC number */
        /*tb  7241 (5.1) 02/27/97 jkp; Changed assign of s-prcunit to use
             v-prccostper instead of icsp.prccostper. */
        /*tb 25103 02/16/07 jhb Moved p-oeordl.i checks because orders with
            zero total qty shipped were causing incorrect amounts to display */
        assign
            s-qtyord   = if oeel.returnfl then
                             if substring
                                 (string(oeel.qtyord,"9999999.99-"),9,2) = "00"
                                 then string((oeel.qtyord * -1),"zzzzzz9-")
                             else string((oeel.qtyord * -1),"zzzzzz9.99-")
                         else if substring(string(oeel.qtyord,"9999999.99-")
                             ,9,2) = "00"
                             then string(oeel.qtyord,"zzzzzz9-")
                         else string(oeel.qtyord,"zzzzzz9.99-")
            s-qtyship  = if p-prtord then s-qtyord
                         else if oeel.returnfl then
                             if substring
                                 (string(oeel.qtyship,"9999999.99-"),9,2) = "00"
                                 then string((oeel.qtyship * -1),"zzzzzz9-")
                             else string(oeel.qtyship * -1,"zzzzzz9.99-")
                         else if substring
                             (string(oeel.qtyship,"9999999.99-"),9,2) = "00"
                             then string(oeel.qtyship,"zzzzzz9-")
                         else string(oeel.qtyship,"zzzzzz9.99-")
            v-qtyship  = decimal(s-qtyship)
            v-qtybo    = if p-prtord then 0 else
                         if oeel.botype ne "n":u then
                             dec(s-qtyord) - dec(s-qtyship)
                         else 0
            s-qtybo    = if p-prtord then "" else
                         if substring(string(v-qtybo,"9999999.99-"),9,2) = "00"
                             then string(v-qtybo,"zzzzzz9-")
                         else string(v-qtybo,"zzzzzz9.99-")
            s-lineno   = string(oeel.lineno,"zz9")
            s-price    = if oeel.corechgty = "r" then
                             string(oeel.corecharge,"zzzzzz9.99-")
                         else if substring
                             (string(oeel.price,"9999999.99999"),11,3) = "000"
                             then string(oeel.price,"zzzzzz9.99-")
                         else string(oeel.price,"zzzzzz9.99999-")
            s-discpct  = if substring
                             (string(oeel.discpct,"9999999.99999"),11,3) = "000"
                             then string(oeel.discpct,"zzzzzz9.99-")
                         else string(oeel.discpct,"zzzzzz9.99999-")
            s-netamt   = if oeel.corechgty = "r" then
                             string((oeel.corecharge *
                                 if p-prtord then oeel.qtyord else
                                 oeel.qtyship),
                                 "zzzzzzzz9.99-")
                         else string(v-netord,"zzzzzzzz9.99-")
            s-sign     = if oeel.returnfl and oeel.printpricefl = yes and
                            oeeh.printpricefl = yes then "-"
                         else ""
            v-subnetamt = v-subnetamt +
                          if oeel.returnfl then (v-netord * -1)
                          else v-netord
            v-totqtyshp = if oeel.returnfl then
                              if can-do("rm":u,oeeh.transtype) then
                                  v-totqtyshp - v-qtyord
                              else v-totqtyshp
                          else if oeeh.stagecd < 3 and oeel.botype = "d" then
                              v-totqtyshp
                          else v-totqtyshp +
                              (if p-prtord then v-qtyord else v-qtyship)
            s-prcunit   = if v-prccostper <> "" then
                              v-prccostper
                          else oeel.unit
            s-upcpno    = if avail icsv and avail b-sasc
                                        and oeel.corechgty <> "r" then
                              {upcno.gas &section  = "4"
                                         &sasc     = "b-sasc."
                                         &icsv     = "icsv."
                                         &comdelim = "/*"}
                          else "00000"
            /* das - NonReturn */
            s-lasterisk = if substr(oeel.user10,1,1) = "n" then 
                               "**"
                          else
                               "  ".
                               
/*            message "after non-return". pause.  */

                               
        /* SX55 */
        /*si02 begin -- logic for Canadian Currency sdi06501*/
        if substr(oeeh.user5,1,1) = "c" or
           (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then do: 
           if oeeh.user6 <> 0 then do:
              assign 
                 s-price     = if oeel.corechgty = "r" 
                               then string(dec(s-price) * oeeh.user6,
                                           "zzzzzz9.99-") 
                               else if oeel.price * 100 = int(oeel.price * 100) 
                                    then string(dec(s-price) * oeeh.user6, 
                                                "zzzzzz9.99-")
                                    else string(
                                    round(dec(s-price) * oeeh.user6,2),
                                                "zzzzzz9.99-")
                 s-netamt    = string(dec(s-netamt) * oeeh.user6,
                                      "zzzzzzzz9.99-")
                 v-subnetamt = v-subnetamt * oeeh.user6.
           end.

           s-cantot    = s-cantot + 
                         (dec(s-netamt) * (if oeel.returnfl then -1 else 1)). 
        end.
        /*end si02*/                                  
        
        /* Blank BO Qty when Use Ord Qty Only is true or p-oeordl.i is true
           Blank Ship Qty when Use Ord Qty Only is false and p-oeordl.i is true
           Orders with only backordered lines & nothing shipped will print nulls
           for the Backordered & Shipped columns per enhancement TB# e23696 */
        assign
            s-qtybo    = if p-prtord = true or
                           {p-oeordl.i "oeel." "oeeh."} then ""
                         else s-qtybo
            s-qtyship  = if p-prtord = false and
                           {p-oeordl.i "oeel." "oeeh."} then ""
/*                         else s-qtyship. */
/* New TAH */            else
                         if p-prtord = true and
                           {p-oeordl.i "oeel." "oeeh."} then ""
                         else
                         if p-prtord = true then 
                          (if oeel.returnfl then
                             if substring
                              (string(oeel.qtyship,"9999999.99-"),9,2) = "00"
                               then string((oeel.qtyship * -1),"zzzzzz9-")
                            else string(oeel.qtyship * -1,"zzzzzz9.99-")
                           else if substring
                                (string(oeel.qtyship,"9999999.99-"),9,2) = "00"
                           then string(oeel.qtyship,"zzzzzz9-")
                           else string(oeel.qtyship,"zzzzzz9.99-"))
                         else s-qtyship


                                         
            v-qtybo    =  if oeel.botype ne "n" then
                             dec(s-qtyord) - dec(s-qtyship)
                          else 0
           
            s-qtybo    = if substring(string(v-qtybo,"9999999.99-"),9,2) = "00"
                             then string(v-qtybo,"zzzzzz9-")
                         else string(v-qtybo,"zzzzzz9.99-")
            
            s-qtybo    = if {p-oeordl.i "oeel." "oeeh."} or
                         dec(s-qtybo) = 0 then ""
                         else s-qtybo.
 
/* New TAH */



        /*tb 7405 04/10/93 jlc; Print DROP for the qty ship on DOs */
        assign
            s-descrip = ""
            s-qtyship = if oeeh.stagecd < 3 and oeel.botype = "d" then "  DROP"
                        else s-qtyship
            s-qtybo   = if oeeh.stagecd < 3 and oeel.botype = "d" then ""
                        else s-qtybo.
                        

        /*tb 3-2 03/29/00 rgm; Rxserver interface hook for extra vars */
        {oeepa1.z99 &before_linedisplay = "*"}

        /*tb 7245 08/05/92 pap; Do not display qty backordered or qty shipped */
        /* REVERSAL OF TB 7245 - 08/12/92 DESIGN MEETING */
        /*tb 18091 11/01/95 gp;  Change display of UPC number */
        /*tb 3-2 03/29/00 rgm; Rxserver interface display vars for rxserver */

        /* SX55 */
        /* ITB# 2        Suppress discounts from acknowledgments */
        if s-discpct ne "" then do:
           assign s-price = string(round
             (oeel.price * ((100.00 - oeel.discpct) / 100) *
                        /*si02 added "if substr..."*/
                           (if (substr(oeeh.user5,1,1) = "c"
                                and oeeh.user6 <> 0 ) or
                                (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                            then oeeh.user6
                                else 1),2),"zzzzzz9.99-")
                  s-discpct = "".
           end.
        x-shipdt = date(substring(oeel.user3,11,10)).
        if avail icsw then
          do: 
          run zsdiweekendh (input-output x-shipdt,
                            input-output v-1019fl,
                            input        oeeh.whse,
                            input        g-cono,
                            input        oeeh.divno).
        end. /* avail icsw */ 
        
         if oeeh.stagecd < 3 then
          if s-qtybo = "" then
             s-qtybo = string(0,"zzzzzz9-").
 

        display
            s-lineno
            oeel.shipprod
            oeel.unit
            s-qtyord
            s-qtybo
            s-qtyship
            s-prcunit
   /* SX55 */
   /*       s-upcpno */
            s-price         when oeeh.printpricefl = yes and
                                 oeel.printpricefl = yes
   /*         s-discpct       when oeeh.printpricefl = yes and
                                 oeel.printpricefl = yes
   */
            x-shipdt
            s-netamt        when oeeh.printpricefl = yes and
                                 oeel.printpricefl = yes
            s-sign
            s-lasterisk
            &IF DEFINED(rxserver) &THEN
                s-rxline1
            &ENDIF
            with frame f-oeel.
        /*tb 9731 10/22/98 jgc; Expand Non-stock description. Added
            oeel.proddesc2 to display. */
        if oeel.specnstype = "n" then do:
            if oeel.proddesc ne "" then
                display
                    oeel.proddesc + " " + oeel.proddesc2 @ s-descrip
                with frame f-oeel.
        end.        /** non-stock **/

        else if not avail icsp or not avail icsw then
            if oeeh.langcd = "" or not avail sals then
                display v-invalid @ s-descrip with frame f-oeel.
            else display
                     sals.descrip[1] + " " + sals.descrip[2] @ s-descrip
                 with frame f-oeel.
        else if oeeh.langcd = "" or not avail sals then
            display
                icsp.descrip[1] + " " + icsp.descrip[2] @ s-descrip
            with frame f-oeel.
        else display
                 sals.descrip[1] + " " + sals.descrip[2] @ s-descrip
             with frame f-oeel.
        down with frame f-oeel.
         
        /*tb 3-2 03/29/00 rgm; Rxserver interface hook for extra vars */
        {oeepa1.z99 &before_xref = "*"}

        v-reqtitle = if      oeel.xrefprodty = "c" then "   Customer Prod:"
                     else if oeel.xrefprodty = "i" then "Interchange Prod:"
                     else if oeel.xrefprodty = "p" then " Superseded Prod:"
                     else if oeel.xrefprodty = "s" then " Substitute Prod:"
                     else if oeel.xrefprodty = "u" then "    Upgrade Prod:"
                     else "".

        if can-do("c,i,p,s,u",oeel.xrefprodty) and oeel.reqprod <> "" then do:
            /*tb 10155 04/09/93 jlc; print 3rd tier product */
            s-prod  =   oeel.reqprod.
            if oeel.xrefprodty = "c":u then do:
                find first icsec use-index k-custno where
                        icsec.cono      = g-cono        and
                        icsec.rectype   = "c"           and
                        icsec.custno    = oeeh.custno   and
                        icsec.prod      = oeel.reqprod no-lock no-error.

                if avail icsec then
                    s-prod = s-prod + " " + icsec.addprtinfo.
            end.
            display
                s-prod
                v-reqtitle
            with frame f-oeelc.
            down with frame f-oeelc.
        end.

        {oeepa1.z99 &after_xref = "*"}

        /*tb 10155 04/09/93 jlc; print 3rd tier customer product */
        /*tb 10155 01/03/94 mwb; made changes for cust product display */
        if oeel.xrefprodty ne "c" then do:
            s-prod = if oeel.reqprod ne "" then oeel.reqprod
                     else oeel.shipprod.

            {n-icseca.i "first" s-prod ""c"" oeeh.custno no-lock}

            if not avail icsec and oeel.reqprod <> "" then
                {n-icseca.i "first" oeel.shipprod ""c"" oeeh.custno no-lock}

            if avail icsec then do:
               assign
                   s-prod     = icsec.prod
                   v-reqtitle = "   Customer Prod:".

               display
                   s-prod
                   v-reqtitle
               with frame f-oeelc.
            end.

        end. /* if oeel.xrefprodty ne "c" */

        if oeel.corechgty = "r" then
            display with frame f-oeelr.

       /* SX55 */
       /* SurCharge */

        if oeel.datccost <> 0 and v-qtyord <> 0 and
            oeel.printpricefl = yes
        then do:
            clear frame f-oeel1.
            display "Tariff Surcharge" @ oeel.shipprod
                    /*si03 added "* if substr..." for canadian currency*/
                    string(oeel.datccost *
                           (if (substr(oeeh.user5,1,1) = "c" and 
                               oeeh.user6 <> 0 ) or
                             (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then
                              oeeh.user6
                             else 1) * 1 /*v-qtyord*/,"zzzzzz9.99-")
                       @ s-price
                    /*si03 added "* if substr..." for canadian currency*/
                    string(oeel.datccost *
                            (if (substr(oeeh.user5,1,1) = "c" and 
                                oeeh.user6 <> 0) or
                             (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then
                               oeeh.user6 
                             else 1) * v-qtyord ,"zzzzzzzz9.99-")
                       @ s-netamt
                    "-" when oeel.returnfl     @ s-sign
                    " " when not oeel.returnfl @ s-sign   
            with frame f-oeel1.
            down with frame f-oeel1.
        if substr(oeeh.user5,1,1) = "c" or
        (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then 
           do: 
           assign s-cantot = s-cantot + 
                      ( (oeel.datccost *
                          (if (substr(oeeh.user5,1,1) = "c" and 
                               oeeh.user6 <> 0) or
                            (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then
                             oeeh.user6 
                           else 
                             1)
                          * v-qtyord ) *
                       ( if oeel.returnfl  then -1 else 1)).
           end.            
        end.
    /* SurCharge */


  /*   message "before core". pause.  */
  
    /**************** display core charge **************/
        /*tb 9727 04/08/93 jlc; don't print core chg when print flag = no */
        if oeel.corecharge <> 0 and oeel.corechgty <> "r" and
           oeel.printpricefl = yes and oeeh.printpricefl = yes

        then do:
            clear frame f-oeel1.

            display
                  "CORE CHARGE"                           @ oeel.shipprod
                  /* SX55 */
                  /*si03 added "* if substr..." for canadian currency*/
                    string(oeel.corecharge * 
                    if (substr(oeeh.user5,1,1) = "c" and oeeh.user6 <> 0) or
                       (oeeh.currencyty = "lb" and oeeh.user6 <> 0) 
                    then oeeh.user6 else 1,"zzzzzz9.99-") 
                       @ s-price
                    /*si03 added "* if substr..." for canadian currency*/
                    string(oeel.corecharge * 
                    if (substr(oeeh.user5,1,1) = "c" and oeeh.user6 <> 0) or
                       (oeeh.currencyty = "lb" and oeeh.user6 <> 0) 
                    then oeeh.user6 else 1 * v-qtyord,"zzzzzzzz9.99-") 
                       @ s-netamt
                    "-" when oeel.returnfl     @ s-sign
                    " " when not oeel.returnfl @ s-sign
            /*
                "CORE CHARGE"                           @ oeel.shipprod
                string(oeel.corecharge,"zzzzzz9.99-")   @ s-price
                string(oeel.corecharge *
                    (if oeel.returnfl then -1 else 1) *
                    v-qtyord,"zzzzzzzz9.99-")           @ s-netamt
            */
            with frame f-oeel1.
            down with frame f-oeel1.
        end.

    /**************** display restock amount **************/
        /*tb 9727 04/08/93 jlc; don't print restock when print flag = no */
        if oeel.restockamt <> 0 and oeel.printpricefl = yes and
            oeeh.printpricefl = yes
        then do:
            clear frame f-oeel1.
            /* SX55 */
            /*si02 added "* if substr..." for canadian currency*/
            s-restockamt = if oeel.restockfl = yes then oeel.restockamt *
                            if (substr(oeeh.user5,1,1) = "c" 
                                and oeeh.user6 <> 0 ) or
                             (oeeh.currencyty = "lb" and oeeh.user6 <> 0)     
                            then oeeh.user6
                            else 1
                           else round(oeel.netamt *
                            if (substr(oeeh.user5,1,1) = "c" 
                                and oeeh.user6 <> 0) or
                               (oeeh.currencyty = "lb" and oeeh.user6 <> 0)    
                                then oeeh.user6
                            else 1 * (oeel.restockamt / 100),2).
            /* 
            s-restockamt = if oeel.restockfl = yes then
                               oeel.restockamt
                           else if p-prtord then
                               round(oeel.netord * (oeel.restockamt / 100),2)
                           else
                               round(oeel.netamt * (oeel.restockamt / 100),2).
            */
            display
                "RESTOCK AMT" @ oeel.shipprod
                string(s-restockamt,"zzzzzzzz9.99-") @ s-netamt
            with frame f-oeel1.
            down with frame f-oeel1.
        end.

    /**************** display GSA Scheduled Item **************/
        if avail arss then
          do:
          if oeel.pdrecno <> 0 and arss.pricetype = "GOVT" and
            can-find(pdsc where pdsc.cono = g-cono and
                                pdsc.pdrecno = oeel.pdrecno and
                                pdsc.levelcd = 3 and
                                pdsc.startdt <= oeel.enterdt and
                               (pdsc.enddt = ? or
                                pdsc.enddt >= oeel.enterdt)) then
            do:
            clear frame f-oeell.
            display "GSA SCHEDLUED ITEM" @ oeel.shipprod
              with frame f-oeell.
            down with frame f-oeell.
          end. /* GSA product */
        end. /* avail arss */
        else
          do:
          if oeel.pdrecno <> 0 and arsc.pricetype = "GOVT" and
            can-find(pdsc where pdsc.cono = g-cono and
                                pdsc.pdrecno = oeel.pdrecno and
                                pdsc.levelcd = 3 and
                                pdsc.startdt <= oeel.enterdt and
                               (pdsc.enddt = ? or
                                pdsc.enddt >= oeel.enterdt)) then
            do:
            clear frame f-oeel1.
            display "GSA SCHEDLUED ITEM"   @ oeel.shipprod
              with frame f-oeel1.
            down with frame f-oeel1.
          end. /* GSA product */
        end. /* avail arsc */

     
    /**************** display direct order indicator **************/
        /*tb 6219 4/3/92 pap; Line DO - add line DO indicator     **/
        if oeel.botype = "d" and oeeh.transtype ne 'DO' then
            display with frame f-oeeldo.

        /*tb 3-2 03/29/00 rgm; Rxserver interface hook for extra vars */
        {oeepa1.z99 &before_prodnotes = "*"}

    /*************  print the product notes  ************/
    if oeel.specnstype <> "n" and avail icsp and icsp.notesfl ne "" then
     run notesprt.p(g-cono,"p",icsp.prod,"","oeepa",8,1).

    /*tb e3207   print the catalog notes  ************/
    if oeel.specnstype = "n" and avail icsc and icsc.notesfl ne "" then
     run notesprt.p(1,"g",icsc.catalog,"","oeepa",8,1).

    /**************  line item comments  ****************/
    if oeel.commentfl = yes then do:
     {w-com.i ""oe"" oeel.orderno oeel.ordersuf oeel.lineno yes no-lock}
     if avail com and com.printfl2 then do i = 1 to 16:
      if com.noteln[i] ne "" then do:
       display com.noteln[i] with frame f-com.
       down with frame f-com.
      end.
     end.
     {w-com.i ""oe"" oeel.orderno oeel.ordersuf oeel.lineno no no-lock}
     if avail com and com.printfl2 then do i = 1 to 16:
      if com.noteln[i] ne "" then do:
       display com.noteln[i] with frame f-com.
       down with frame f-com.
      end.
     end.
    end. /* if oeel.commentfl = yes */
    
    /* smaf - Tariff Information */
    run zsditariffprt (g-cono,oeel.shipprod,oeel.whse,"oeepa",8).

    /* TB# e11973 02/11/02 Moved oeelc.gpr  */

    /*tb e2761 10/12/99 cad; Tally/mix project */
    /********************  explode tally/mix  *********************/
    /*tb e15321 11/12/02 kjb; Replaced old tally print logic with the new
      logic contained in tallyprt.ldi */
      {tallyprt.ldi &orderno   = oeel.orderno
                    &ordersuf  = oeel.ordersuf
                    &lineno    = oeel.lineno
                    &ordertype = "'O'"
                    &bundleid  = "''"}

    /********************  explode kits  *********************/
        if oeel.kitfl = yes and

            ((avail icsp and icsp.exponinvfl and oeel.specnstype <> "n") or
              oeel.specnstype = "n") and

            can-find(first oeelk use-index k-oeelk where
                           oeelk.cono      = oeel.cono     and
                           oeelk.ordertype = "o"           and
                           oeelk.orderno   = oeel.orderno  and
                           oeelk.ordersuf  = oeel.ordersuf and
                           oeelk.lineno    = oeel.lineno) then

            /*tb 2277 9/11/91 kmw; kit enh TB 2277 */
            /* was ex lock; kmw 9-12-91*/
            for each oeelk use-index k-oeelk where
                oeelk.cono      = oeel.cono     and
                oeelk.ordertype = "o"           and
                oeelk.orderno   = oeel.orderno  and
                oeelk.ordersuf  = oeel.ordersuf and
                oeelk.lineno    = oeel.lineno   and
                oeelk.printfl   = yes
            no-lock:

                /*tb 2277 9/11/91 kmw; kit enhancement */
                if oeelk.comptype = "r" then do:
                    display oeelk.instructions with frame f-refer.
                    down with frame f-refer.
                end.        /** comptype = 'r' **/

                else do:

                    /*tb 18575 06/23/95 ajw; Nonstock/Special Kit Component.*/
                    if oeelk.specnstype <> "n" then do:
                        /*was ex lock*/
                        {w-icsw.i oeelk.shipprod oeel.whse no-lock}
                        {w-icsp.i oeelk.shipprod no-lock}
                    end.

                    /*tb 18091 11/01/95 gp;  Assign product section
                        of UPC number */
                    if oeelk.specnstype = "n" or oeelk.arpvendno <> 0 then
                        {icsv.gfi "'u'" oeelk.shipprod oeelk.arpvendno no}
                    else if avail icsw then
                        {icsv.gfi "'u'" oeelk.shipprod icsw.arpvendno no}

                    assign
                        s-upcpno    = if avail icsv and avail sasc then
                                          {upcno.gas &section  = "4"
                                                     &sasc     = "b-sasc."
                                                     &icsv     = "icsv."
                                                     &comdelim = "/*"}
                                       else "00000".

    /********************  print the component  ***********************/
                    clear frame f-oeel.

                    /*tb 1/24/91 mwb; ship comp, tag&hold always uses ordered */
                    /*tb 2277 9/11/91 kmw; stkqtyord --> qtyord */
                    assign
                        s-qtyord  = if oeel.returnfl then
                                        if substring
                                        (string(oeelk.qtyord,"9999999.99-"),9,2)
                                        = "00"
                                        then
                                        string((oeelk.qtyord * -1),"zzzzzz9-")
                                        else
                                        string(oeelk.qtyord * -1,"zzzzzz9.99-")
                                    else if substring
                                        (string(oeelk.qtyord,"9999999.99-"),9,2)
                                        = "00"
                                        then string(oeelk.qtyord,"zzzzzz9-")
                                    else string(oeelk.qtyord,"zzzzzz9.99-")
                        s-qtyship  = if oeel.returnfl then
                                         if substring(string
                                             (oeelk.qtyship,"9999999.99-"),9,2)
                                             = "00"
                                             then string(
                                             (oeelk.qtyship * -1),"zzzzzz9-")
                                         else string
                                             (oeelk.qtyship * -1,"zzzzzz9.99-")
                                     else if substring(string
                                         (oeelk.qtyship,"9999999.99-"),9,2)
                                         = "00"
                                         then string(oeelk.qtyship,"zzzzzz9-")
                                     else string(oeelk.qtyship,"zzzzzz9.99-")

                        s-qtyship  = if can-do("s,t",oeeh.orderdisp) then
                                         s-qtyord
                                     else s-qtyship.

                    
                    {oeepa1.z99 &before_bodkitcomp = "*"}

                    /*tb 7245 08/05/92 pap; Do not display shipped qty*/
                    /* REVERSAL OF TB 7245 08/12/92 DESIGN MEETING    */
                    /*tb 18575 06/23/95 ajw; Nonstock/Special Kit Comp*/
                    /*tb 18091 11/01/95 gp;  Change display of UPC number */
                    display
                        oeelk.shipprod   @ oeel.shipprod
                        s-qtyord

                        s-qtyship
                        s-upcpno
                        oeelk.unit       @ oeel.unit
                        "Kit"            @ s-lineno
                    with frame f-oeel.

                    /*tb 18575 06/23/95 ajw; Nonstock/Special Kit Component.*/
                    /*tb 9731 10/22/98 jgc; Expand non-stock description.
                        Added oeelk.proddesc2 to display. */
                    if oeelk.specnstype = "n" then
                        display oeelk.proddesc + " " + oeelk.proddesc2
                            @ s-descrip with frame f-oeel.
                    else if avail icsp and avail icsw then
                        display
                            icsp.descrip[1] + " " + icsp.descrip[2] @ s-descrip
                        with frame f-oeel.
                    else display v-invalid @ s-descrip with frame f-oeel.
                    down with frame f-oeel.

                /**************    print the product notes  *******************/
                    /*tb 17771 08/17/95 tdd; Notes print twice when reference
                        component */
                    if oeelk.specnstype ne "n":u and
                       avail icsp and icsp.notesfl ne "" then
                        run notesprt.p(g-cono,"p",icsp.prod,"","oeepa",8,1).

                    else if oeelk.specnstype = "n":u
                    then do:
                        {icsc.gfi &prod = "oeelk.shipprod"
                                  &lock = "no"}
                        if avail icsc and icsc.notesfl ne "" then
                            run notesprt.p(1,"g",icsc.catalog,"","oeepa",8,1).
                    end. /* non-stock catalog notes */

                end. /* else do */

            end. /* for each oeelk */


  /*   message "before subtotals". pause.  */

/* smaf - Printing BL Information on ACK */

  if (oeeh.transtype  = "bl" or oeeh.transtype = "br") and
      v-blprt then do:
      run "print_bl".
  end.

/* smaf - Printing BL Information on ACK */

  if sapb.user3 = "yes" then do:
  if (oeeh.transtype  = "bl" or oeeh.transtype = "br") then do:  
      run "print_bl".
  end.
 end. 
                                                  
     /************** print external comments and subtotals *******************/
    /* TB# e11973 New Subtotal logic */
    {oeelc.gpr &orderno      = "oeel.orderno"
               &ordersuf     = "oeel.ordersuf"
               &lineno       = "oeel.lineno"
               &linetype     = 'e'
               &linetype1    = 't'
               &displayname  = "@ s-descrip"
               &framename    = "f-comments"
               &subdispname  = "@ s-subtotdesc"
               &subframename = "f-oeelsub"}

    end. /* for each oeel */

end. /* if oeeh.transtype <> "ra" */

/************************ order totals ********************/
s-totqtyshp = if substring(string(v-totqtyshp,"999999999.99-"),11,2) = "00" then
                  string(v-totqtyshp,"zzzzzzzz9-")
              else string(v-totqtyshp,"zzzzzzzz9.99-").
              
  /* smaf - Printing BL Information on ACK 

  if (oeeh.transtype  = "bl" or oeeh.transtype = "br") and
      v-blprt then do:
          run "print_bl".
  end.
*/  
  /* smaf - Printing BL Information on ACK */


{oeepa1.z99 &before_totals = "*"}

if oeeh.transtype <> "ra" then do:
    clear frame f-tot1.

    {oeepa1.z99 &before_subtotaldisplay = "*"}

    /*tb 7245 08/05/92 pap; Eliminate shipped qty from display  */
    /* REVERSAL OF TB 7245 08/12/92 BY DESIGN MEETING           */

    /* SX55 */
    /*si02 begin*/
    if (substr(oeeh.user5,1,1) = "c") or
     (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then 
    display
        v-linecnt
        s-lit40a

        s-lit40c            /*    ***** Qty Shipped Total *****     */
        s-totqtyshp

        s-lit40d
        s-cantot @ s-totlineamt
    with frame f-tot1.
    else
    /*SI02 end*/
    display
        v-linecnt
        s-lit40a

        s-lit40c            /*    ***** Qty Shipped Total *****     */
        s-totqtyshp

        s-lit40d
        s-totlineamt
    with frame f-tot1.

    {oeepa1.z99 &after_subtotaldisplay = "*"}

    /*d Only do this when printing ordered totals*/
    if p-prtord = yes then
    do:
        if s-ordwodiscamt <> 0 then do:

            clear frame f-tot2.

            assign
                s-title2 = s-lit41a
                /* SX55 */
                s-amt2   = s-ordwodiscamt *
                       (if oeeh.transtype = "rm":u then -1 else 1) *
                       /*si02 added "if substr..."*/
                       (if (substr(oeeh.user5,1,1) = "c"
                            and oeeh.user6 <> 0) or
                        (oeeh.currencyty = "lb" and oeeh.user6 <> 0)       
                        then oeeh.user6
                            else 1)
               s-cantot = s-cantot + s-amt2.    /*si02*/
               /*
               s-amt2   = s-ordwodiscamt *
                       (if oeeh.transtype = "rm":u then -1 else 1).
               */
            display
                s-title2
                s-amt2
            with frame f-tot2.
            down with frame f-tot2.

        end. /* if s-ordwodiscamt <> 0 then do */

        if s-ordspecdiscamt <> 0 then do:

            clear frame f-tot2.

            assign
                s-title2 = s-lit42a
                /* SX55 */
                s-amt2   = s-ordspecdiscamt *
                          (if oeeh.transtype = "rm":u then -1 else 1) *
                       /*si02 added "if substr..."*/
                       (if (substr(oeeh.user5,1,1) = "c"
                            and oeeh.user6 <> 0) or
                        (oeeh.currencyty = "lb" and oeeh.user6 <> 0)      
                        then oeeh.user6
                            else 1)
                s-cantot = s-cantot + s-amt2.    /*si02*/
            /*                        `
                s-amt2   = s-ordspecdiscamt *
                           (if oeeh.transtype = "rm":u then -1 else 1).
            */
            display
                s-title2
                s-amt2
            with frame f-tot2.
            down with frame f-tot2.

        end. /* if s-ordspecdiscamt <> 0 then do */

        if s-ordcorechg <> 0 then do:

            clear frame f-tot2.

            assign
                s-title2 = s-lit43a
                /* SX55 */
                s-amt2   = s-ordcorechg *
                           (if oeeh.transtype = "rm":u then -1 else 1) *
                       /*si02 added "if substr..."*/
                       (if (substr(oeeh.user5,1,1) = "c"
                            and oeeh.user6 <> 0) or
                         (oeeh.currencyty = "lb" and oeeh.user6 <> 0)    
                        then oeeh.user6
                            else 1)
                s-cantot = s-cantot + s-amt2.    /*si02*/
                /*                
                s-amt2   = s-ordcorechg *
                           (if oeeh.transtype = "rm":u then -1 else 1).
                */
            display
                s-title2
                s-amt2
            with frame f-tot2.
            down with frame f-tot2.

        end. /* if s-ordcorechg <> 0 then do */

        if s-ordrestkamt <> 0 then do:

            clear frame f-tot2.

            assign
               s-title2 = s-lit53a
               /* SX55 */
               s-amt2   = s-ordrestkamt *
                           (if oeeh.transtype = "rm":u then -1 else 1) *
                       /*si02 added "if substr..."*/
                       (if (substr(oeeh.user5,1,1) = "c"
                            and oeeh.user6 <> 0 ) or
                           (oeeh.currencyty = "lb" and oeeh.user6 <> 0)                                   then oeeh.user6
                        else 1)
               s-cantot = s-cantot + s-amt2.    /*si02*/
               /*
               s-amt2   = s-ordrestkamt *
                           (if oeeh.transtype = "rm":u then -1 else 1).
               */
            display
                s-title2
                s-amt2
            with frame f-tot2.
            down with frame f-tot2.

        end. /* if s-ordrestkamt <> 0 then do */

       /* SX55 
       /*  ITB# 2 */
       assign s-amt2 = 0
              zx-handle = false.
        do i = 1 to 4:
         assign s-amt2 = s-amt2 + oeeh.addonnet[i].
         if oeeh.addonno[i] = 5 and
          oeeh.addonnet[i] <> 0 then
          assign zx-handle = true. 
        end.
       */ 
 
        /* SX55 */ 
        assign s-amt2 = 0
            zx-handle = false.
        for each t-addon use-index k-seqno where
            t-addon.cono      = g-cono         and
            t-addon.ordertype = "oe":u         and
            t-addon.orderno   = oeeh.orderno   and
            t-addon.ordersuf  = oeeh.ordersuf
            exclusive-lock:

            t-addon.addonnet = if t-addon.addonno <> 0 and
                                  v-addonloadfl = yes then
                                   s-ordaddonnet[t-addon.addonno]
                               else
                                   t-addon.addonnet.

            if t-addon.addonnet <> 0 then do:
                /* SX55 */
                assign zx-handle = true. 
                {p-oeadfp.i "t-"}
                clear frame f-tot2.

                assign
                    s-title2    = t-addon.addondesc
                    s-amt2      = s-amt2 + (t-addon.addonnet *
                                 (if oeeh.transtype = "rm":u then -1 else 1)).

                {oeepa1.z99 &before_addondisplay = "*"}
/* 
                display
                    s-title2
                    s-amt2
                with frame f-tot2.
                down with frame f-tot2.
*/
                {oeepa1.z99 &after_addondisplay = "*"}

            end.

        end. /* for each t-addon */

    /* SX55 */    
    if s-amt2 = 0 and
       oeeh.stagecd >= 0 and oeeh.stagecd <= 2 and
       oeeh.termstype <> "cod" and
       (zx-handle = true or
        oeeh.inbndfrtfl = true or
        oeeh.outbndfrtfl = true) then do:
      assign s-title2 = "Frt/Hndlg to be added at shipping"
             s-amt2   = 0.
      display
          s-title2
      with frame f-tot2a.
      down with frame f-tot2a.
    end.
    else
    if s-amt2 ne 0 then do:
     clear frame f-tot2.
     if oeeh.stagecd >= 0 and oeeh.stagecd <= 2 and
        oeeh.termstype <> "cod" then do:
      assign s-title2 = "Frt/Hndlg to be added at shipping"
             s-amt2   = 0.
      display
         s-title2
      with frame f-tot2a.
      down with frame f-tot2a.
     end.
     else do: 
      assign s-title2 = "Frt & Hndlg"
             s-amt2 = s-amt2 * (if oeeh.transtype = "rm" then -1 else 1)
               /*si02 added "if substr..."*/
               * (if (substr(oeeh.user5,1,1) = "c"
                    and oeeh.user6 <> 0 ) or
                 (oeeh.currencyty = "lb" and oeeh.user6 <> 0)               
                  then oeeh.user6
                    else 1)
             s-cantot = s-cantot + s-amt2.    /*si02*/
      assign cod-freight = s-amt2.
      display
         s-title2
         s-amt2
      with frame f-tot2.
      down with frame f-tot2.
     end.
    end.
        
   /*** SX55 do not print the surcharge cost in the totals   ****
        /*tb 8561 11/11/92 mms */
        if s-orddatccost <> 0 and v-icdatclabel <> "" then
        do:

            clear frame f-tot2.

            assign
                s-title2 = s-lit44a
                s-amt2   = s-orddatccost *
                           (if oeeh.transtype = "rm":u then -1 else 1) *
                            /* SX55 */
                            /*si02 added "if substr..."*/
                            (if (substr(oeeh.user5,1,1) = "c"
                              and oeeh.user6 <> 0) or
                              (oeeh.currencyty = "lb" and oeeh.user6 <> 0)                                    then oeeh.user6
                             else 1)
                s-cantot = s-cantot + s-amt2.    /*si02*/
            display
                s-title2
                s-amt2
            with frame f-tot2.
            down with frame f-tot2.

        end. /*if s-orddatccost <> 0 and v-icdatclable <> "" then do */
    *********** SX55 ****/
    
    end.  /* if p-prtord = yes then do*/

    else

    /* Only do this when not printing ordered totals*/
    if p-prtord = no then
    do:

        if oeeh.wodiscamt <> 0 then do:
            clear frame f-tot2.

            assign
                s-title2 = s-lit41a
                /* SX55 */
                s-amt2   = oeeh.wodiscamt *
                       (if oeeh.transtype = "rm":u then -1 else 1) *
                       /*si02 added "if substr..."*/
                       (if (substr(oeeh.user5,1,1) = "c"
                            and oeeh.user6 <> 0) or
                         (oeeh.currencyty = "lb" and oeeh.user6 <> 0)       
                         then oeeh.user6
                        else 1)
                s-cantot = s-cantot + s-amt2.    /*si02*/
               /* 
                s-amt2   = oeeh.wodiscamt *
                           (if oeeh.transtype = "rm":u then -1 else 1).
               */
            display
                s-title2
                s-amt2
            with frame f-tot2.
            down with frame f-tot2.
        end.

        if oeeh.specdiscamt <> 0 then do:
            clear frame f-tot2.

            assign
                s-title2 = s-lit42a
                s-amt2   = oeeh.specdiscamt *
                           (if oeeh.transtype = "rm":u then -1 else 1) *
                           /* SX55 */
                           /*si02 added "if substr..."*/
                           (if (substr(oeeh.user5,1,1) = "c"
                              and oeeh.user6 <> 0) or
                             (oeeh.currencyty = "lb" and oeeh.user6 <> 0)   
                              then oeeh.user6
                            else 1)
                s-cantot = s-cantot + s-amt2.    /*si02*/
            display
                s-title2
                s-amt2
            with frame f-tot2.
            down with frame f-tot2.
        end.

        if oeeh.totcorechg <> 0 then do:
            clear frame f-tot2.

            assign
                s-title2 = s-lit43a
                s-amt2   = oeeh.totcorechg *
                           (if oeeh.transtype = "rm":u then -1 else 1) *
                           /* SX55 */
                           /*si02 added "if substr..."*/
                            (if (substr(oeeh.user5,1,1) = "c"
                              and oeeh.user6 <> 0) or
                             (oeeh.currencyty = "lb" and oeeh.user6 <> 0)   
                              then oeeh.user6
                             else 1)
                s-cantot = s-cantot + s-amt2.    /*si02*/
            display
                s-title2
                s-amt2
            with frame f-tot2.
            down with frame f-tot2.
        end.

        if oeeh.totrestkamt <> 0 then do:
            clear frame f-tot2.

            assign
                s-title2 = s-lit53a
                s-amt2   = oeeh.totrestkamt *
                           (if oeeh.transtype = "rm":u then -1 else 1) *
                           /* SX55 */
                           /*si02 added "if substr..."*/
                            (if (substr(oeeh.user5,1,1) = "c"
                              and oeeh.user6 <> 0 ) or
                              (oeeh.currencyty = "lb" and oeeh.user6 <> 0)     
                               then oeeh.user6
                             else 1)
                s-cantot = s-cantot + s-amt2.    /*si02*/
            display
                s-title2
                s-amt2
            with frame f-tot2.
            down with frame f-tot2.
        end.

        /* SX55 */ 
        assign s-amt2        = 0
               zx-handle     = false.

        for each addon use-index k-seqno where
            addon.cono      = g-cono         and
            addon.ordertype = "oe":u         and
            addon.orderno   = oeeh.orderno   and
            addon.ordersuf  = oeeh.ordersuf
            no-lock:
         assign s-amt2 = s-amt2 + addon.addonnet.
         
         if addon.addonno = 5 and
            addon.addonnet <> 0 then
          assign zx-handle = true. 

            if addon.addonnet <> 0 then do:
                {p-oeadfp.i}
                clear frame f-tot2.

                assign
                    s-title2 = s-addondesc
                    s-amt2   = s-amt2 *
                               (if oeeh.transtype = "rm":u then -1 else 1).
/*
                display
                    s-title2
                    s-amt2
                with frame f-tot2.
                down with frame f-tot2.
*/
            end.

        end. /* for each addon */
        
        if s-amt2 = 0 and
         oeeh.stagecd >= 0 and oeeh.stagecd <= 2 and
         oeeh.termstype <> "cod" and
         (zx-handle = true or
          oeeh.inbndfrtfl = true or
          oeeh.outbndfrtfl = true) then do:
         assign s-title2 = "Frt/Hndlg to be added at shipping"
               s-amt2   = 0.
         display
             s-title2
         with frame f-tot2a.
         down with frame f-tot2a.
        end.
        else
        if s-amt2 ne 0 then do:
         clear frame f-tot2.
         if oeeh.stagecd >= 0 and oeeh.stagecd <= 2 and 
            oeeh.termstype <> "cod" then do: 
           assign s-title2 = "Frt/Hndlg to be added at shipping"
                  s-amt2   = 0.
           display
             s-title2
           with frame f-tot2a.
           down with frame f-tot2a.
         end.
         else do:
          assign s-title2 = "Frt & Hndlg"
                 s-amt2 = s-amt2 * (if oeeh.transtype = "rm" then -1 else 1)
                   /*si02 added "if substr..."*/
                  * (if (substr(oeeh.user5,1,1) = "c"
                        and oeeh.user6 <> 0 ) or
                      (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                    then oeeh.user6
                        else 1)
                 s-cantot = s-cantot + s-amt2.    /*si02*/
          assign cod-freight = s-amt2.
          display
             s-title2
             s-amt2
          with frame f-tot2.
          down with frame f-tot2.
         end.
        end.
        
        /***** SX55 - no surcharge print in totals for SDI-custom ******
        /*tb 8561 11/11/92 mms */
        if oeeh.totdatccost <> 0 and v-icdatclabel <> "" then do:
            clear frame f-tot2.

            assign
                s-title2 = s-lit44a
                s-amt2   = oeeh.totdatccost *
                           (if oeeh.transtype = "rm":u then -1 else 1) *
                            /* SX55 */
                            /*si02 added "if substr..."*/
                           (if substr(oeeh.user5,1,1) = "c" and oeeh.user6 <> 0                               then oeeh.user6
                            else 1)
                s-cantot = s-cantot + s-amt2.    /*si02*/

             
            display
                s-title2
                s-amt2
            with frame f-tot2.
            down with frame f-tot2.
        end.
        *********************************************/

    end. /* else if p-prtord = no then do */

    if g-country = "ca" 
       /* SX55 */
       or substr(oeeh.user5,1,1) = "C"  /*SI02*/ then do:

        /* print gst tax amount */
        if v-gsttaxamt <> 0 then do:
            clear frame f-tot2.

            assign
                s-title2 = s-lit49a
                s-amt2   = v-gsttaxamt *
                           (if oeeh.transtype = "rm":u then -1 else 1) *
                            /* SX55 */
                            /*si02 added "if substr..."*/
                           (if substr(oeeh.user5,1,1) = "c"
                                and oeeh.user6 <> 0 
                            then oeeh.user6
                                else 1)
                s-cantot = s-cantot + s-amt2.    /*si02*/

            display
                s-title2
                s-amt2
            with frame f-tot2.
            down with frame f-tot2.
        end.

        /* print pst tax amount */
        if v-psttaxamt <> 0 then do:
            clear frame f-tot2.

            assign
                s-title2 = s-lit50a
                s-amt2   = v-psttaxamt *
                           (if oeeh.transtype = "rm":u then -1 else 1) *
                           /* SX55 */
                           /*si02 added "if substr..."*/
                           (if (substr(oeeh.user5,1,1) = "c"
                                and oeeh.user6 <> 0) or
                            (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                            then oeeh.user6
                                else 1)
                s-cantot = s-cantot + s-amt2.    /*si02*/
            display
                s-title2
                s-amt2
            with frame f-tot2.
            down with frame f-tot2.
        end.

    end. /* if g-country = "ca" */

    else if s-taxes <> 0 then do:
        clear frame f-tot2.

        assign
            s-title2 = s-lit45a
            s-amt2   = s-taxes *
                       (if oeeh.transtype = "rm":u then -1 else 1) *
                       /* SX55 */
                       /*si02 added "if substr..."*/
                          (if (substr(oeeh.user5,1,1) = "c" 
                                and oeeh.user6 <> 0) or
                            (oeeh.currencyty = "lb" and oeeh.user6 <> 0)  
                             then oeeh.user6
                           else 1)
            s-cantax1 = if ({zzcandiv.zvl arsc.divno} and     
                                  substr(oeeh.user5,1,1) <> "c") 
                                
                               then oeeh.taxamt[4] * 
                                    (if oeeh.user6 <> 0             
                                     then oeeh.user6                
                                     else 1)                        
                               else 0                               
            s-cantax2 = if ({zzcandiv.zvl arsc.divno} and
                                  substr(oeeh.user5,1,1) <> "c") 
                           
                               then oeeh.taxamt[1] *
                                    (if oeeh.user6 <> 0
                                     then oeeh.user6
                                     else 1)
                               else 0
            s-canlit = if {zzcandiv.zvl arsc.divno} and     
                                 substr(oeeh.user5,1,1) <> "c"     
                              then "CANADIAN TAX AMOUNT: G.S.T.:" + 
                                   string(s-cantax1,"zzzzz9.99-") + 
                                   "  P.S.T.:" + 
                                   string(s-cantax2,"zzzzz9.99-")
                              else ""     
            s-cantot = s-cantot + s-amt2.    
            /*si02*/

        display
            /* SX55 */
            s-canlit  /*SI02*/
            s-title2
            s-amt2
        with frame f-tot2.
        down with frame f-tot2.
    end.

    if s-dwnpmtamt <> 0 then do:
        clear frame f-tot2.

        assign
            s-title2 = if can-do("cs",oeeh.transtype) then s-lit46b
                       else s-lit46a
            /* SX55 */
            s-amt2   = s-dwnpmtamt *
                      (if oeeh.transtype = "rm":u then -1 else 1) *
                       /*si02 added "if substr..."*/
                      (if (substr(oeeh.user5,1,1) = "c"
                           and oeeh.user6 <> 0) or
                          (oeeh.currencyty = "lb" and oeeh.user6 <> 0)         
                         then oeeh.user6
                       else 1)
            s-cantot = s-cantot + s-amt2.    /*si02*/
            /*
            s-amt2   = s-dwnpmtamt *
                       (if oeeh.transtype = "rm":u then -1 else 1).
            */
        display
            s-title2
            s-amt2
        with frame f-tot2.
        down with frame f-tot2.
    end.

    
    /* smaf  */
    
    if oeeh.taxablefl = yes then
       assign s-taxtitle = "Tax Status:"
              s-vartax = "TAXABLE".
    else
       assign  s-taxtitle = "Tax Status:"

              s-vartax = "NON-TAXABLE ".

    if oeeh.whse <> "DBAT" then
      do:
      display 
        s-taxtitle 
        s-vartax
      with frame f-vartax.
      down with frame f-vartax.
    end.

end. /* if oeeh.transtype <> "ra" */

{oeepa1.z99 &before_totinv = "*"}

clear frame f-tot3.

/*tb 5413 06/20/92 mwb; added the writeoffamt to totinvamt */
assign
    s-totinvamt =
      if oeeh.transtype = "ra" then oeeh.tendamt
    /* SX55 - sdi-frthdlamt - subtracted out if OE stage code picked or less */
      else if p-prtord then
           (s-ordtotalamt - oeeh.tendamt - oeeh.writeoffamt - sdi-frthdlamt) *
             (if oeeh.transtype = "rm":u then -1 else 1)
      else if oeeh.totqtyshp <> 0 then
           (oeeh.totinvamt - oeeh.tendamt - oeeh.writeoffamt - sdi-frthdlamt) *
             (if oeeh.transtype = "rm":u then -1 else 1)
      else 0
    s-invtitle  = if oeeh.transtype = "ra" then s-lit47b else s-lit47a.
    /* SX55 */
    /*si02 added s-currencyty*/
    s-currencyty = if {zzcandiv.zvl arsc.divno} or
                     (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then 
                            if substr(oeeh.user5,1,1) = "c" then
                       "**** ACKNOWLEDGEMENT REFLECTS CANADIAN CURRENCY ***"
                            else 
                         if (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then  
                       "**** ACKNOWLEDGEMENT REFLECTS ENGLISH POUNDS  ***"
                            else
                       "  **** ACKNOWLEDGEMENT REFLECTS U.S. CURRENCY ****"
                          else "".
    s-totinvamt = if (substr(oeeh.user5,1,1) = "c") or
                     (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                         then s-cantot
                         else s-totinvamt.   /*si02*/
    
    /* don't include freight and handling */
    if oeeh.stagecd >= 0 and 
       oeeh.stagecd <= 2 and
       s-totinvamt  <>  0 then do:  /* das - changed s-totinvamt > 0 to <> 0 */
         if (substr(oeeh.user5,1,1) = "c") or
            (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then
            s-totinvamt = s-cantot.
          /* SX55 
          do i = 1 to 4:
            assign s-totinvamt = s-totinvamt - oeeh.addonnet[i].
          end.
          */
       if s-totinvamt < 0 then
        assign s-totinvamt = 0.
       /*
       if oeeh.termstype = "cod" then
        assign s-totinvamt = s-totinvamt + cod-freight.
       */
    end.

display
    /* SX55 */
    s-currencyty  /*Si02*/
    s-invtitle
    s-totinvamt
with frame f-tot3.

/*tb# 5929 06/18/92 mkb */
if oeeh.tottendamt ne 0 then do:
   clear frame f-tot5.

   if oeeh.boexistsfl = no and
       (oeeh.orderdisp = "j"  or oeeh.transtype = "bl")
   then do:
       find first b-oeeh use-index k-oeeh where
           b-oeeh.cono       = oeeh.cono         and
           b-oeeh.orderno    = oeeh.orderno      and
           b-oeeh.ordersuf   > 0
       no-lock no-error.

       if avail b-oeeh then
           display oeeh.tottendamt *
                /* SX55 */
                /*SI02 beg*/
                (if (substr(oeeh.user5,1,1) = "c" 
                   and oeeh.user6 <> 0) or
                  (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                   then oeeh.user6 
                 else 1) /*SI02 end*/
           with frame f-tot5.
   end.
   else if oeeh.boexists = yes then
       display oeeh.tottendamt *
           /* SX55 */
           /*SI02 beg*/
           (if (substr(oeeh.user5,1,1) = "c" 
             and oeeh.user6 <> 0) or
             (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
              then oeeh.user6 
            else 1) /*SI02 end*/
        with frame f-tot5.
end.

{oeepa1.z99 &after_ordertots = "*"}

/* das - NonReturn */
  display s-tariffmsg
          s-asteriskdtla
          s-asteriskdtlb
          s-asteriskdtlc
          s-buysignature
          s-sunsignature
  with frame f-footnote.
  down with frame f-footnote.

assign
    p-prtord = o-prtord
    s-lit48a = "Last Page"
    /* SX55 */
    s-cantot = 0.   /*si02*/

/* SX55 */
find first oimsp where oimsp.person = oeeh.takenby no-lock no-error.
if avail(oimsp) then
   assign s-CSR-title  = "Customer Service Contact:"
          s-contact    = oimsp.name
          s-contlit-ph = "TEL:"
          s-contact-ph = oimsp.phoneno
          s-contlit-fx = "FAX:"
          s-contact-fx = oimsp.faxphoneno.
else
   assign s-CSR-title  = ""
          s-contact    = ""
          s-contlit-ph = ""
          s-contact-ph = ""
          s-contlit-fx = ""
          s-contact-fx = "".
 /* message "end". pause. */
