/* b-oeepp1.i 1.1 01/03/98 */
/* b-oeepp1.i 1.6 03/21/94 */
/*h****************************************************************************
  INCLUDE      : b-oeepp1.i
  DESCRIPTION  : Pick Ticket Print format 1 - NEMA
  USED ONCE?   : no
  AUTHOR       : R&D
  DATE WRITTEN :
  CHANGES MADE :
    02/21/94 dww; TB# 13890 File name not specified causing error
    03/14/94 dww; TB# 15141 Allow decimals in Conversion factor
    03/09/95 mtt; TB# 13417 Bulk lot records display before product
    11/01/95 gp;  TB# 18091 Expand UPC Number (T20)
    06/10/96 jms; TB# 21254 (10A) Develop Value Add Module
                    Placed the define statements at the beginning of this
                    include, added parameter &sapbfile
    09/13/96 dww; TB# 21702 Trend 8.0 Enhancement - remove oper2
    09/18/96 mwb; TB# 19484 (B7) Warehouse Logistics; Added 'down frame' for
                    f-oeel2.i (set in m-oeepp1.i for Counter Sale Bin display.
******************************************************************************/

def var a-lit1a         as c    format "x(43)"              no-undo.
def var a-bulk          as c    format "x(22)"              no-undo.
def var a-lit1b         as c    format "x(37)"              no-undo.
def var a-filltype      as c    format "x(33)"              no-undo.
def var a-lit3a         as c    format "x(7)"               no-undo.
def var a-lit3b         as c    format "x(39)"              no-undo.
def var a-lit6a         as c    format "x(8)"               no-undo.
def var a-lit6b         as c    format "x(18)"              no-undo.
def var a-lit11a        as c    format "x(8)"               no-undo.
def var a-lit11b        as c    format "x(12)"              no-undo.
def var a-lit11c        as c    format "x(12)"              no-undo.
def var a-lit12a        as c    format "x(10)"              no-undo.
def var a-lit12b        as c    format "x(3)"               no-undo.
def var a-lit12e        as c    format "x(7)"               no-undo.
def var a-lit12c        as c    format "x(7)"               no-undo.
def var a-lit12d        as c    format "x(5)"               no-undo.
def var a-whsedesc      as c    format "x(30)"              no-undo.
def var a-lit14a        as c    format "x(78)"              no-undo.
def var a-lit14b        as c    format "x(49)"              no-undo.
def var a-lit15a        as c    format "x(78)"              no-undo.
def var a-lit15b        as c    format "x(48)"              no-undo.
def var a-lit16a        as c    format "x(78)"              no-undo.
def var a-lit16b        as c    format "x(50)"              no-undo.
def var v-linecntx      as c    format "x(3)"               no-undo.
def var a-lit40a        as c    format "x(11)"              no-undo.
def var a-lit40b        as c    format "x(26)"              no-undo.
def var a-lit40c        as c    format "x(18)"              no-undo.
def var a-totqtyshpx    as c    format "x(11)"              no-undo.
def var a-lit40d        as c    format "x(22)"              no-undo.
def var a-lit41a        as c    format "x(78)"              no-undo.
def var a-lit41b        as c    format "x(10)"              no-undo.
def var a-lit42a        as c    format "x(9)"               no-undo.
def var a-lit42b        as c    format "x(33)"              no-undo.

/*o********* Print the bulk pick ticket   *************************/
if sasc.oebulkpickfl and can-do("oeepp,oeepb,wtep,vaepp",g-ourproc)
then do while true:

    /*d****** If only one pick ticket exists, bypass the bulk ticket   *****/
    assign
        i = 0
        v-pageno = page-number - 1.

    for each {&sapbfile} where
        {&sapbfile}.cono     = g-cono and
        {&sapbfile}.reportnm = sapb.reportnm
    no-lock:
        i = i + 1.
        if i > 1 then leave.
    end.

    if i < 2 then leave.

    /*d Form for the bulk pick ticket */
    form header
    skip(1)
    a-lit1a                               at 2
    a-bulk                                at 53
    a-lit1b                               at 93
    a-filltype                            at 53
    sasc.upcvno                           at 95
    a-lit3a                               at 4
    a-lit3b                               at 94
    page-num - v-pageno format "zzz9"     at 127
    skip(1)
    a-lit6a                               at 2
    a-lit6b                               at 48
    skip(4)
    a-lit11a                              at 2
    a-lit11b                              at 50
    a-lit11c                              at 81
    skip(1)
    a-lit12a                              at 50
    a-lit12b                              at 81
    a-lit12e                              at 100
    a-lit12c                              at 110
    a-lit12d                              at 120
    a-whsedesc                            at 50
    skip(1)
    a-lit14a                              at 1
    a-lit14b                              at 79
    a-lit15a                              at 1
    a-lit15b                              at 79
    a-lit16a                              at 1
    a-lit16b                              at 79
    with frame f-ahead no-box no-labels width 132 page-top.

form header
    skip(1)
    v-linecntx                        at 1
    a-lit40a                          at 5
    a-lit40b                          at 20
    a-lit40c                          at 58
    a-totqtyshpx                      at 77
    a-lit40d                          at 91
    a-lit41a                          at 1
    a-lit41b                          at 79
    a-lit42a                          at 1
    a-lit42b                          at 95
with frame f-atot no-box no-labels width 132 page-bottom.

    /*d Assign the literal variables for the labels */
    {a-oeepp1.i a- {&flag}}

    /*d Override the few labels specific to the bulk pick ticket */
    assign a-filltype  = if sapb.currproc ne "oeeb" then "" else
                         "Filled From Received Merchandise"
           a-lit42a    = "Continued"
           a-bulk      = "** BULK PICK TICKET **".

    view frame f-ahead.
    view frame f-atot.

    /*o Read each OEPICK record for the bulk ticket */
    for each oepick where
        oepick.cono     = g-cono    and
        oepick.oper2    = ""        and
        oepick.reportnm = sapb.reportnm
    no-lock
    by if sasc.oepickordty = "r" then oepick.binloc
       else oepick.prod:

        if v-whse = "" then
            v-whse = oepick.whse.
        if oepick.whse ne v-whse then do:
            assign v-whse   = oepick.whse
                   a-lit42a = "Last Page".
            page.
            assign
                v-linecnt    = 0
                a-totqtyshp  = 0
                v-pageno     = page-number - 1
                v-linecntx   = ""
                a-totqtyshpx = ""
                a-lit42a     = "Continued".
        end.

        /*tb 4616 10/30/91 mwb; Added unit to qtyship display **/
        if not can-do("s,l",oepick.serlottype) then do:
            assign v-linecnt    = v-linecnt + 1
                   a-totqtyshp  = a-totqtyshp +
                   /*tb# 5397 11/14/92 jbt; stop return lines from being
                                            included in qtyshipped total on the
                                            bulk pick ticket. */
                            (if oepick.qtyship > 0 then oepick.qtyship else 0).
                            /* SX30 SunSource */
            {w-icsp.i oepick.prod no-lock}
            if avail icsp then
                {w-icsw.i oepick.prod oepick.whse no-lock}

            /*d Print the product line */
            /*tb 13417 03/09/95 mtt; Bulk lot records display before product */
            /*tb 18091 11/01/95 gp;  Change UPC number display */
            /*e The oepick record for the oeel line item will contain the
                oeel unit, however, it will first be prefixed with a blank,
                so it can sort above the lot/serial oepick records. */
            display
                oepick.prod                    @ {&line}.shipprod
                oepick.qtyship                 @ s-qtyship /* SX30 SunSource */
                substring(oepick.serlottype,2) @ {&line}.unit

                s-upcpno
                oepick.binloc format "xx/xx/xxx/xxx" @ s-binloc
            with frame f-oeel.

            if avail icsw and icsw.binloc2 ne "" then
                    display icsw.binloc2 @ s-binloc2 with frame f-oeel.

            if avail icsp then
                display icsp.descrip[1] @ s-descrip with frame f-oeel.

            down with frame f-oeel.  /*tb 2519 03/14/91 mjm; fixed for 2.1 */

            /*tb 19484 09/18/96 mwb; added the down with frame f-oeel2 line and
                    then do, with the end */
            if avail icsp and icsp.descrip[2] ne "" then do:
                display icsp.descrip[2] @ s-descrip3 with frame f-oeel2.
                down with frame f-oeel2.
            end.
/* SX32 UPGRADE
            /*tb 15141 03/14/94 dww; Allow decimals in Conversion factor */
            /*d Print the product notes  */
            if avail icsp and icsp.notesfl ne ""
                then run oeepp1n.p(g-cono,"p",icsp.prod,"").
*/
/* SX32 UPGRADE */

            if avail icsp and icsp.notesfl ne "" then
              run notesprt.p(g-cono,"p",icsp.prod,"","oeepp",8,1).
 
            /* Print the catalog notes */
              {w-icsc.i oepick.prod no-lock}
              if avail icsc and icsc.notesfl ne "" then
               run notesprt.p(1,"g",icsc.catalog,"","oeepp",8,1).
/* SX32 UPGRADE */
        end.

        /*d Display serial/lot records */
        else if oepick.serlottype = "s" then do:
            display oepick.serlotno @ s-serialno[1]
                    with frame f-serial.
            down with frame f-serial.
        end.
        else if oepick.serlottype = "l" then do:
            display oepick.serlotno @ s-lot1
                    oepick.qtyship  @ s-qty1    /* SX30 SunSource */
                    "Lot #:"        @ s-lot1d
                    "Qty:"          @ s-qty1d
                    with frame f-lot.
            down with frame f-lot.
        end.

    end.
    assign
        v-linecntx   = string(v-linecnt,"zz9")
        a-totqtyshpx = string(a-totqtyshp,"zzzzzzzz9.99-")
        a-lit42a     = "Last Page".
    page.
    hide frame f-ahead.
    hide frame f-atot.
    leave.
end.


