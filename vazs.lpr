/*zkprp.lpr 1.1  05/19/00*/
/*h****************************************************************************
  INCLUDE      : zkprp.lpr
  DESCRIPTION  : Kit Production - Production Schedule Report
  USED ONCE?   : yes
  AUTHOR       : SD
  DATE WRITTEN : 06/02/00  
  CHANGES MADE :
   06/02/00  SD: 206890 Original development of new custom report - Print logic
******************************************************************************/
assign
    s-whsetotcnt = 0
    s-whsetothrs = 0
    s-whsetotval = 0
    s-techtotcnt = 0
    s-techtothrs = 0
    s-techtotcnt-gtl = 0
    s-techtothrs-gtl = 0
    s-techtotval = 0
    s-totalcnt   = 0
    s-totalhrs   = 0
    s-totalval   = 0
    s-scheddt    = ?.
/*
if p-schedonly = yes then
   for each t-vaeh  use-index k-whsetech 
       where t-vaeh.technician = "":
       message "b4delete-lpr" t-vaeh.wono t-vaeh.wosuf
                          t-vaeh.technician. pause.
       delete t-vaeh.
   end.    
*/
if p-whsesrtfl = yes and p-showtechfl = yes then do:
    for each t-vaeh no-lock use-index k-schedule 
       where t-vaeh.technician >= b-tech and
             t-vaeh.technician <= e-tech and
           ((t-vaeh.scheddt le p-reportthru  or 
             t-vaeh.scheddt = ?) and
             t-vaeh.blddt ge b-requestdt and
             t-vaeh.blddt le e-requestdt) 
       break by t-vaeh.whse 
             by t-vaeh.technician 
             by t-vaeh.scheddt
             by t-vaeh.requestdt3 
             by t-vaeh.seqno:

        if p-bldable then 
        do:  
           if t-vaeh.stkqtyship = t-vaeh.stkqtyord then 
              assign p-bldable = p-bldable. 
           else 
              next.  
        end. 
 
        if first-of(t-vaeh.technician) then 
           page. 

        {vazs.las}
        {vazs.ldi}   

        if last-of(t-vaeh.technician) then 
        do:
            display s-techlabel
                    s-techtotcnt
                    s-techtothrs
                    s-scheddt 
               /* s-techtotval  when p-showtotfl klt */
                    with frame f-vazse.
            assign s-techtotcnt-gtl = s-techtotcnt-gtl + s-techtotcnt 
                   s-techtothrs-gtl = s-techtothrs-gtl + s-techtothrs.
            display s-techlabel
                    s-techtotcnt-gtl
                    s-techtothrs-gtl
                    with frame f-vazs-gtl.
             assign s-techtotcnt = 0
                    s-techtothrs = 0
                    s-techtotcnt-gtl = 0
                    s-techtothrs-gtl = 0
                    s-techtotval = 0.
        end. /* last-of tech */
        else 
        if last-of(t-vaeh.scheddt) then 
        do:
            display s-techlabel
                    s-techtotcnt
                    s-techtothrs
                    s-scheddt
               /* s-techtotval  when p-showtotfl klt */
                    with frame f-vazse.
            assign s-techtotcnt-gtl = s-techtotcnt-gtl + s-techtotcnt 
                   s-techtothrs-gtl = s-techtothrs-gtl + s-techtothrs
                   s-techtotcnt = 0
                   s-techtothrs = 0
                   s-techtotval = 0.
        end. /* last-of tech */
         
        if last-of(t-vaeh.whse) then
        do:
            display s-whselabel
                    s-whsetotcnt
                    s-whsetothrs
                    with frame f-vazsw.
            assign s-whsetotcnt = 0
                   s-whsetothrs = 0
                   s-whsetotval = 0.
        end. /* last-of whse */

    end. /* for each t-vaeh */
end. /* yes,yes */

if p-whsesrtfl = no and p-showtechfl = yes then do:
    for each t-vaeh no-lock use-index k-technician 
       where t-vaeh.technician >= b-tech and
             t-vaeh.technician <= e-tech and
           ((t-vaeh.scheddt le p-reportthru  or 
             t-vaeh.scheddt = ?) and
             t-vaeh.blddt ge b-requestdt and
             t-vaeh.blddt le e-requestdt) 
          break by t-vaeh.technician
                by t-vaeh.scheddt
                by t-vaeh.requestdt3
                by t-vaeh.seqno:

        if p-bldable then 
        do:  
           if t-vaeh.stkqtyship = t-vaeh.stkqtyord then 
              assign p-bldable = p-bldable. 
           else 
              next.  
        end. 

        if first-of(t-vaeh.technician) or 
           first-of(t-vaeh.scheddt) then page.

        {vazs.las}
        {vazs.ldi}

        if last-of(t-vaeh.technician) then 
        do:
            display s-techlabel
                    s-techtotcnt
                    s-techtothrs
                    s-scheddt
                    with frame f-vazse.
            assign s-techtotcnt-gtl = s-techtotcnt-gtl + s-techtotcnt 
                   s-techtothrs-gtl = s-techtothrs-gtl + s-techtothrs.
            display s-techlabel
                    s-techtotcnt-gtl
                    s-techtothrs-gtl
                    with frame f-vazs-gtl.
            assign  s-techtotcnt = 0
                    s-techtothrs = 0
                    s-techtotval = 0
                    s-techtotcnt-gtl = 0
                    s-techtothrs-gtl = 0.
        end. /* last-of tech */
        else 
        if last-of(t-vaeh.scheddt) then 
        do:
            display s-techlabel
                    s-techtotcnt
                    s-techtothrs
                    s-scheddt
                    with frame f-vazse.
            assign s-techtotcnt-gtl = s-techtotcnt-gtl + s-techtotcnt 
                   s-techtothrs-gtl = s-techtothrs-gtl + s-techtothrs
                   s-techtotcnt = 0
                   s-techtothrs = 0
                   s-techtotval = 0.
        end. /* last-of tech */
    end. /* for each t-vaeh */                    
end. /* no,yes */                        

if p-whsesrtfl = yes and p-showtechfl = no then do:
    for each t-vaeh no-lock use-index k-whse 
       where t-vaeh.technician >= b-tech and
             t-vaeh.technician <= e-tech and
           ((t-vaeh.scheddt le p-reportthru  or 
             t-vaeh.scheddt = ?) and
             t-vaeh.blddt ge b-requestdt and
             t-vaeh.blddt le e-requestdt) 
       break by t-vaeh.whse 
             by t-vaeh.seqno
             by t-vaeh.requestdt:
  
        if p-bldable then 
        do:  
           if t-vaeh.stkqtyship = t-vaeh.stkqtyord then 
              assign p-bldable = p-bldable. 
           else 
              next.  
        end. 

        if first-of(t-vaeh.whse) then page.

        {vazs.las}
        {vazs.ldi}  

        if last-of(t-vaeh.whse) then do:
            display s-whselabel
                    s-whsetotcnt       
                    s-whsetothrs       
             /*   s-whsetotval  when p-showtotfl      klt */
                    with frame f-vazsw.    
                                               
            assign s-whsetotcnt = 0   
                   s-whsetothrs = 0   
                   s-whsetotval = 0.  
        end. /* last-of whse */
                                                            
    end. /* for each t-vaeh */                        
end. /* yes,no */             

if p-whsesrtfl = no and p-showtechfl = no and 
   p-srtkprod  = no then 
  do:
    for each t-vaeh use-index k-seqno 
       where t-vaeh.technician >= b-tech and
             t-vaeh.technician <= e-tech and
           ((t-vaeh.scheddt le p-reportthru  or 
             t-vaeh.scheddt = ?) and
             t-vaeh.blddt ge b-requestdt and
             t-vaeh.blddt le e-requestdt) 
       no-lock:
  
        if p-bldable then 
        do:  
           if t-vaeh.stkqtyship = t-vaeh.stkqtyord then 
              assign p-bldable = p-bldable. 
           else 
              next.  
        end. 

       {vazs.las}
       {vazs.ldi}

  end. /* for each t-vaeh */
end. /* no,no */   
            
if p-whsesrtfl = no and p-showtechfl = no and 
   p-srtkprod  = yes then 
  do:
    for each t-vaeh use-index k-kitprod 
       where t-vaeh.technician >= b-tech and
             t-vaeh.technician <= e-tech and
           ((t-vaeh.scheddt le p-reportthru  or 
             t-vaeh.scheddt = ?) and
             t-vaeh.blddt ge b-requestdt and
             t-vaeh.blddt le e-requestdt) 
             no-lock:

        if p-bldable then 
        do:  
           if t-vaeh.stkqtyship = t-vaeh.stkqtyord then 
              assign p-bldable = p-bldable. 
           else 
              next.  
        end. 
      
       {vazs.las}
       {vazs.ldi}
        
  end. /* for each t-vaeh */
end. /* no,no */                
            
