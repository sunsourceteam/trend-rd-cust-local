form    /* Canadian Check Form */
   skip (3)
   p-cancheckdt            at 62
   skip (2)
   p-checkno               at 44
   v-checkamt              at 59
   skip(1)
   v-textamt               at 9
   v-textamt2              at 9
   skip(3)
   v-addr[1]               at 11
   v-addr[2]               at 11
   v-addr[3]               at 11
   v-addr[4]               at 11
   skip (4)
with frame f-cancheck no-labels width 80 no-box.