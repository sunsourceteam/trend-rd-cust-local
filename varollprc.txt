/* varollprc.p 1.0 10/26/01 */
/*h*****************************************************************************
  PROCEDURE    : varollprc.p
  DESCRIPTION  : Used to process the Rebates logic in 'non-update' mode when
                 the user requests to roll-up prices from the VA components,
                 and the conditions aren't met for excuting the Rebates logic
                 in normal 'update' mode.
  AUTHOR       : sah
  DATE WRITTEN : 10/26/01
  CHANGES MADE :
   07/01/02 doc; TB# e12218 Calculate Pricing from VA Tie/Arp to OE.
******************************************************************************/

def input  param pv-rebtieproc      as char                 no-undo.
def input  param pv-ourproc         like sassm.currproc     no-undo.
def input  param pv-orderno         like oeeh.orderno       no-undo.
def input  param pv-ordersuf        like oeeh.ordersuf      no-undo.
def input  param pv-lineno          like oeel.lineno        no-undo.
def output param pv-rebtiefl        as logi                 no-undo.

def new shared buffer b-vaesl       for vaesl.
def new shared buffer b-vaspsl      for vaspsl.
def new shared buffer b-oeelk       for oeelk.
def new shared buffer b-oeeh        for oeeh.
def new shared buffer b-oeel        for oeel.
def new shared buffer b-icsp        for icsp.
def new shared buffer b-icsw        for icsw.
def new shared buffer b-icsc        for icsc.
def new shared buffer b-icss        for icss.

{g-all.i}
{g-oe.i}
{g-ar.i}
{g-ic.i}
{g-oee99.i}
{g-oeet.i      "shared" " " "/*"}
{g-oeeth.i     "shared"}
{g-oeetx.i     "shared"}
{g-oeetlo.i    "new shared"}
{g-oeetlr.i    "new shared"}
{speccost.gva  "new shared"}
{rspcvars.gva  "new shared"}
{rebties.lva   "new shared"}
{rebnet.gva    "new shared"}
{varollprc.gva "shared"}
{g-margin.i}

/* from {rebcalc.gva} */
def new shared var v-idpdsr         as recid                            no-undo.
def new shared var v-rebatecd       like pdsr.rebatecd                  no-undo.
def new shared var v-cprod          like icsp.prod                      no-undo.
def new shared var v-cprodrebty     like icsw.rebatety                  no-undo.
def new shared var v-cpricetype     like icsw.pricetype                 no-undo.
def new shared var v-cprodline      like icsw.prodline                  no-undo.
def new shared var v-cprodcat       like icsp.prodcat                   no-undo.
def new shared var v-cvendno        like apsv.vendno                    no-undo.
def new shared var v-crebsubty      like icsw.rebsubty                  no-undo.
def new shared var v-ccustno        like arsc.custno                    no-undo.
def new shared var v-cshipto        like arss.shipto                    no-undo.
def new shared var v-ccustrebty     like arsc.rebatety                  no-undo.
def new shared var v-cwhse          like icsd.whse                      no-undo.
def new shared var v-cunit          like pder.unit                      no-undo.
def new shared var v-cnetamt        like pder.netamt                    no-undo.
def new shared var v-cqtyneeded     as deci                             no-undo.
def new shared var v-cqtyord        like oeel.qtyord                    no-undo.
def new shared var v-cnetord        like oeel.netord                    no-undo.
def new shared var v-cspecnstype    like oeel.specnstype                no-undo.
def new shared var v-cdisctype      like oeel.disctype                  no-undo.
def new shared var v-cdiscpct       like oeel.discpct                   no-undo.
def new shared var v-cdiscamt       like oeel.discamt                   no-undo.
def new shared var v-botype         like oeel.botype                    no-undo.
def new shared var v-oeorderty      like oeel.ordertype                 no-undo.
def new shared var v-poorderty      like poelo.ordertype                no-undo.
def new shared var v-arpvendno      like oeel.arpvendno                 no-undo.
def new shared var v-creturnfl      like oeel.returnfl                  no-undo.
def new shared var v-specnstype     like oeel.specnstype                no-undo.
def new shared var v-orderno        like oeel.orderno                   no-undo.
def new shared var v-ordersuf       like oeel.ordersuf                  no-undo.
def new shared var v-lineno         like oeel.lineno                    no-undo.
def new shared var v-seqno          like oeelk.seqno                    no-undo.
def new shared var v-avgcost        like icsw.avgcost                   no-undo.
def new shared var v-lastcost       like icsw.lastcost                  no-undo.
def new shared var v-replcost       like icsw.replcost                  no-undo.
def new shared var v-stndcost       like icsw.stndcost                  no-undo.
def new shared var v-rebatecost     like icsw.rebatecost                no-undo.
def new shared var v-lastcostfor    like icsw.lastcostfor               no-undo.
def new shared var v-actcost        like oeel.prodcost                  no-undo.
def new shared var v-statustype     like pder.statustype                no-undo.
def new shared var v-qtyship        like pder.qtyship                   no-undo.
def new shared var v-pdsrrefer      like pdsr.refer                     no-undo.
def new shared var v-caprebfl       like pdsr.caprebfl init yes         no-undo.
def new shared var g-unitconv       like oeel.unitconv                  no-undo.
def new shared var v-frzrebty       like pder.frzrebty                  no-undo.
def new shared var v-lockedfl       as logical init no                  no-undo.
def new shared var v-cprice         like oeel.price                     no-undo.
def new shared var v-rebcost        as dec format ">>>>>>9.999999"      no-undo.
def new shared var v-rebcostto      as dec format ">>>>>>9.999999"      no-undo.
def new shared var v-allpricefl     as logical init no                  no-undo.
def     shared var s-frzrebty       like pder.frzrebty                  no-undo.
def     shared var s-crbrecno       like pder.rebrecno                  no-undo.
def     shared var s-vrbrecno       like pder.rebrecno                  no-undo.
def     shared var s-creblvcd       like pdsr.levelcd                   no-undo.
def     shared var s-vreblvcd       like pdsr.levelcd                   no-undo.
def new shared var v-kitprod        like oeel.shipprod                  no-undo.
def new shared var v-kqtyship       like oeel.qtyship                   no-undo.
def new shared var v-kstkqtyship    like oeel.stkqtyship                no-undo.
def     shared var v-rebamt         like poelc.cost                     no-undo.
def new shared var v-rebatechg      like poelc.cost                     no-undo.
def new shared var v-undofl         as logical init no                  no-undo.
def new shared var v-rebateamt      like poelc.cost                     no-undo.
def new shared var v-rebateach      like poelc.cost                     no-undo.
def     shared var g-postdt         like sasj.postdt                    no-undo.
def     shared var g-vendno         like apsv.vendno                    no-undo.
def new shared var v-updatefl       as logical                          no-undo.

/* from {oeetlrb.lva} */
def            var o-csunperstk     like icss.csunperstk  initial 1     no-undo.
def            var o-specconv       as integer            initial 1     no-undo.
def            var o-prccostper     like icss.prccostper                no-undo.
def            var o-speccostty     like icss.speccostty                no-undo.
def            var o-icspecrecno    like icss.icspecrecno               no-undo.
def            var level            as int initial 1                    no-undo.
def            var v-recfound       as logical initial no               no-undo.

/* from {g-oeetl.i} */
def new shared var s-botype        like oeel.botype initial "n"       no-undo.
def new shared var s-chrgqty       like oeel.chrgqty                  no-undo.
def new shared var s-commtype      like oeel.commtype                 no-undo.
def new shared var s-jobno         like oeel.jobno                    no-undo.
def new shared var s-kitfl         as logical initial "no"            no-undo.
def new shared var s-lineno        like oeel.lineno                   no-undo.
def new shared var s-netamt        like oeel.netamt                   no-undo.
def new shared var s-ordertype     like oeel.ordertype                no-undo.
def new shared var s-prod          like oeel.shipprod                 no-undo.
def new shared var s-prodcat       like oeel.prodcat                  no-undo.
def new shared var v-idicsp        as recid                           no-undo.
def new shared var s-returnfl      like oeel.returnfl                 no-undo.
def new shared var s-qtyord        like oeel.qtyord                   no-undo.
def new shared var s-qtyship       as dec format "zzzzzz9.99"         no-undo.
def new shared var s-slsrepin      like oeel.slsrepin                 no-undo.
def new shared var s-slsrepout     like oeel.slsrepout                no-undo.
def new shared var s-specnstype    like oeel.specnstype               no-undo.
def new shared var s-termspct      like oeel.termspct                 no-undo.
def new shared var s-unit          like oeel.unit                     no-undo.
def new shared var v-conv          as de                              no-undo.
def new shared var v-costoverfl    as logical init no                 no-undo.
def new shared var v-errfl         as c format "x"                    no-undo.
def new shared var v-linefl        like oeeh.linefl                   no-undo.
def new shared var v-maint-l       as c format "x"                    no-undo.
def new shared var v-manprice      as logical                         no-undo.
def new shared var v-prodcost      as dec format ">>>>>>9.999999"     no-undo.
def new shared var v-stkqtyord     like oeel.stkqtyord                no-undo.
def new shared var v-stkqtyship    like oeel.stkqtyship               no-undo.
def new shared var v-stkunit       like icsp.unitstock                no-undo.
def new shared var s-prodline      like oeel.prodline                 no-undo.
def new shared var s-prodcost      like oeel.prodcost                 no-undo.
def new shared var o-rebamt        like pder.rebateamt                no-undo.
def new shared var s-vendno        like oeel.vendno                   no-undo.

def new shared var s-vvendno       like oeel.vendno                   no-undo.
def new shared var s-qtyneeded     as deci                            no-undo.
def new shared var s-stkqtyord     like oeel.stkqtyord                no-undo.
def new shared var s-stkqtyship    like oeel.stkqtyship               no-undo.
def new shared var v-lastlevel     as int                             no-undo.

def new shared var v-vendrebord    like poelc.cost                    no-undo.
def new shared var v-vaqtyneeded   as deci                            no-undo.

def            var v-idoeelk       as recid                           no-undo.
def            var v-idicsw        as recid                           no-undo.

{w-oeeh.i pv-orderno pv-ordersuf no-lock b-}
{w-oeel.i pv-orderno pv-ordersuf pv-lineno no-lock b-}

if avail b-oeeh and avail b-oeel then do for vaelo, vasp:

    assign
        g-orderno   = b-oeel.orderno
        g-ordersuf  = b-oeel.ordersuf
        s-lineno    = b-oeel.lineno
        pv-rebtiefl = yes.

    /*d Calculate Cost/Price Rollups based on tied VA order (VA may be
        tied either directly to the OE line, or via a WT). */
    if can-do("vaerb,vatrb":u,pv-rebtieproc) then do:

        /*e VA Order is tied directly to the OE line. */
        if pv-rebtieproc = "vaerb":u then
            find first vaelo use-index k-order where
                vaelo.cono          =  g-cono           and
                vaelo.ordertype     =  "o":u            and
                vaelo.orderaltno    =  b-oeel.orderno   and
                vaelo.orderaltsuf   =  b-oeel.ordersuf  and
                vaelo.linealtno     =  b-oeel.lineno    and
                vaelo.seqaltno      =  0
            no-lock no-error.

        /*e VA Order is tied to a WT, that's tied to the OE line. */
        else do:

            find first wtelo use-index k-order where
                wtelo.wtcono        =  g-cono           and
                wtelo.ordertype     =  "o":u            and
                wtelo.orderaltno    =  b-oeel.orderno   and
                wtelo.orderaltsuf   =  b-oeel.ordersuf  and
                wtelo.linealtno     =  b-oeel.lineno    and
                wtelo.seqaltno      =  0
            no-lock no-error.

            if avail wtelo then
                find first vaelo use-index k-order where
                    vaelo.cono          =  g-cono       and
                    vaelo.ordertype     =  "t":u        and
                    vaelo.orderaltno    =  wtelo.wtno   and
                    vaelo.orderaltsuf   =  wtelo.wtsuf  and
                    vaelo.linealtno     =  wtelo.lineno and
                    vaelo.seqaltno      =  0
                no-lock no-error.

            pv-rebtiefl = if avail vaelo then yes
                          else no.

        end. /* pv-rebtieproc = "vatrb" */

        /*e We only want to perform the VA Cost/Price Rollup logic
            for the WT-tied case if a VAELO tie is found. */
        if pv-rebtiefl = yes then do:

            /*e The g-ourproc value is set to 'vaerb' in either tie case,
                since the VA Cost/Price Rollup logic is the same from this
                point on. */
            {rebties.lpr
                &bufoeeh        = "b-"
                &bufoeel        = "b-"
                &asgnoeel       = "*"
                &compder        = "/*"
                &entrymode      = "/*"
                &inqmode        = "*"
                &foreach        = "if avail vaelo then
                                   for each b-vaesl use-index k-vaesl where
                                       b-vaesl.cono     = g-cono       and
                                       b-vaesl.vano     = vaelo.vano   and
                                       b-vaesl.vasuf    = vaelo.vasuf"

                &file           = "b-vaesl"
                &sctntype       = "b-vaesl.sctntype"
                &priceclty      = """"
                &pricecd        = "0"
                &returnfl       = "if b-vaesl.sctntype = 'ii':u then
                                       not sv-s-returnfl
                                   else sv-s-returnfl"
                &botype         = "if b-vaesl.directfl = yes then 'd':u
                                   else sv-s-botype"
                &ordertype      = "b-vaesl.orderalttype"
                &specnstype     = "b-vaesl.nonstockty"
                &prod           = "b-vaesl.shipprod"
                &whse           = "b-vaesl.whse"
                &conv           = "b-vaesl.unitconv"
                &qtyneeded      = "v-vaqtyneeded"
                &icspecrecno    = "b-vaesl.icspecrecno"
                &pdcost         = "{valbrcst.las &file = b-vaesl}"
                &ourproc        = "vaerb"
                }

        end. /* pv-rebtiefl = yes */

    end. /* pv-rebtieproc = "vaerb", "vatrb" */

    /*d Calculate Cost/Price Rollups based on ARP of VA. */
    else if pv-rebtieproc = "vasrb":u then do:

        find vasp use-index k-vasp where
            vasp.cono       = g-cono            and
            vasp.shipprod   = b-oeel.shipprod   and
            vasp.whse       = b-oeel.whse
        no-lock no-error.

        if not avail vasp then
        find vasp use-index k-vasp where
            vasp.cono       = g-cono            and
            vasp.shipprod   = b-oeel.shipprod   and
            vasp.whse       = ""
        no-lock no-error.

        {rebties.lpr
            &bufoeeh        = "b-"
            &bufoeel        = "b-"
            &asgnoeel       = "*"
            &compder        = "/*"
            &entrymode      = "/*"
            &inqmode        = "*"
            &foreach        =
               "if avail vasp then
                for each b-vaspsl no-lock use-index k-vaspsl where
                    b-vaspsl.cono       = g-cono        and
                    b-vaspsl.shipprod   = vasp.shipprod and
                    b-vaspsl.whse       = vasp.whse,
                first vasps use-index k-vasps where
                    vasps.cono      = g-cono            and
                    vasps.shipprod  = vasp.shipprod     and
                    vasps.whse      = vasp.whse         and
                    vasps.seqno     = b-vaspsl.seqno"
            &file           = "b-vaspsl"
            &sctntype       = "vasps.sctntype"
            &priceclty      = """"
            &pricecd        = "0"
            &returnfl       = "if vasps.sctntype = 'ii':u then
                                   not sv-s-returnfl
                               else sv-s-returnfl"
            &botype         = "if b-vaspsl.directfl = yes then 'd':u
                               else sv-s-botype"
            &ordertype      = """"
            &specnstype     = "b-vaspsl.nonstockty"
            &prod           = "b-vaspsl.compprod"
            &whse           = "if can-do('it,is',vasps.sctntype)
                                   then vasps.intrwhse
                               else sv-g-whse"
            &conv           = "b-vaspsl.unitconv"
            &qtyneeded      = "v-vaqtyneeded"
            &icspecrecno    = "?"
            &pdcost         = "{valbrcst.las &file = vasps}"
            &ourproc        = "vasrb"
            }

    end. /* pv-rebtieproc = "vasrb" */

end. /* avail b-oeeh and avail b-oeel */


