{p-rptbeg.i} 

page.
def shared var g-whse     as char                     no-undo.
def var s-runnbr          as integer                  no-undo.
def var s-whse            as c format "x(4)"          no-undo. 
def var s-flag            as c                        no-undo.

    assign s-runnbr = int(sapb.optvalue[2])
           s-whse = sapb.optvalue[1].
           
  find first notes where notes.cono = 1 and
                   notes.notestype = "zz" and
                   notes.primarykey = "hhec" + "-" + s-whse + "-" + 
                   string(s-runnbr) /* and
                   notes.secondarykey = listq.prod  */
                   no-lock no-error. 
  
  if avail notes then do:

     display  "Temporary List for Cycle Count  " s-runnbr no-labels s-whse                 no-labels.
                       
  for each notes where notes.cono = 1 and
                  notes.notestype = "zz" and
                  notes.primarykey = "hhec" + "-" + s-whse + "-" + 
                  string(s-runnbr) /* and
                  notes.secondarykey = listq.prod  */
                  exclusive-lock: 
  

                  display
                  skip
                  notes.noteln[1]  format "x(15)" label "Bin Location" at 1
                  notes.noteln[2]  format "x(24)" label "Product" at 16
                  notes.noteln[3]  format "x(10)" label "Qty." at 41
                  notes.noteln[4]  format "x(24)" label "Comments" at 52 .
  end.
end.                  
