/** p-oeepbpr.i **/
/* 08/15/2007 dkt */
procedure processIcer:
/* This goes through all the ICER records for
   a given warehouse and processinit. Of course,
   this needs to be done before the ICER records
   get deleted in TREND, and after they have been
   created.
*/

/* dkt 08/02/2007 - Mark not for resale items as complete */

define input parameter i-whse like icer.whse.
define input parameter i-init like icer.processinit.
define input parameter i-ordertype as char.

define buffer b-icer for icer.
define buffer b-hdr  for zsdiput.
define buffer b-poel for poel.
define buffer b-wtel for wtel.


def var v-jrnlno    as dec no-undo.
def var v-jrnlproc  as char no-undo.
def var v-receiptId as char no-undo.
/* def var v-order     as char no-undo. */
def var s-qtyalloc  like icer.qtyalloc  no-undo.
def var s-qtypicked like icer.qtypicked no-undo.
def var s-qtyrcvd   like icer.qtyrcvd   no-undo.
/* def var s-qtyputaway as dec initial 0 no-undo. */
def var t-transcnt  as dec initial 0 no-undo.

def var currputrec  as dec initial 0 no-undo.
/*o Find each ICER record */

if icsdAutoPut <> "Y" then
  return.

for each b-icer use-index k-icer where
  b-icer.cono        = g-cono      and
  b-icer.whse        = i-whse      and
/*  (icer.ordertype  >= ""         and icer.ordertype <= "a") and  */
  b-icer.processinit = i-init  and
  (b-icer.qtyrcvd    <> 0  or b-icer.qtypicked <> 0)
no-lock break by b-icer.prod 
/* New 10/2/07 */
              by b-icer.pono descending :
/* New 10/2/07 */
  
/**
display b-icer.ordertype
 b-icer.pono          
 b-icer.posuf         
 b-icer.lineno
 b-icer.user3.        
**/

  if first-of(b-icer.prod) then do:
    find last b-hdr use-index putin where 
              b-hdr.cono = g-cono no-lock no-error.
    if avail b-hdr then 
       currputrec = b-hdr.putrecno + 1.
    
    if num-entries(b-icer.user3, "|") ge 3 then
      v-jrnlno = decimal(entry(1, b-icer.user3, "|")).
    if num-entries(b-icer.user3, "|") ge 3 then
      v-jrnlproc = entry(2, b-icer.user3, "|").       
    if num-entries(b-icer.user3, "|") ge 3 then
      v-receiptId = entry(3, b-icer.user3, "|").      
    /* v-order = entry(4, b-icer.user3, "|"). */
    
    if num-entries(b-icer.user3, "|") ge 6 and
      v-jrnlproc begins "wt" then
      assign v-hwtno  = int(entry(5, b-icer.user3, "|")) 
             v-hwtsuf = int(entry(6, b-icer.user3, "|")). 

      
    end.
  
  /*d Keep running total for same products */
  if i-ordertype = "PO" then do:
    find b-poel where b-poel.cono = g-cono and
     b-poel.pono = b-icer.pono and b-poel.posuf = b-icer.posuf   and
     b-poel.whse = b-icer.whse  and b-poel.lineno = b-icer.lineno and
     b-poel.shipprod = b-icer.prod no-lock no-error.
  end.   
  else
  if i-ordertype = "WT" then
    find b-wtel where b-wtel.cono = g-cono and
     b-wtel.wtno = v-hwtno and b-wtel.wtsuf = v-hwtsuf   and
     b-wtel.shiptowhse = b-icer.whse  and b-wtel.lineno = b-icer.lineno and
     b-wtel.shipprod = b-icer.prod no-lock no-error.
  
  
  if (i-ordertype = "PO" and (avail b-poel and 
    b-poel.nonstockty ne "r")) or
     (i-ordertype = "WT" and avail b-wtel) then
    assign
    s-qtyalloc  = s-qtyalloc  + b-icer.qtyalloc
    s-qtypicked = s-qtypicked + b-icer.qtypicked
    s-qtyrcvd   = s-qtyrcvd   + b-icer.qtyrcvd.
    /* s-qtyputaway = s-qtyrcvd - qtypicked */
/* New 10/2/07 */
  else
  if ((not avail b-poel and i-ordertype = "PO") or
      (not avail b-wtel and i-ordertype = "WT")) and
       b-icer.pono   = 0   and
       int(v-receiptId )  <> 0  then
    assign
     s-qtyalloc  = s-qtyalloc  + b-icer.qtyalloc
     s-qtypicked = s-qtypicked + b-icer.qtypicked
     s-qtyrcvd   = s-qtyrcvd   + b-icer.qtyrcvd.
     /* s-qtyputaway = s-qtyrcvd - qtypicked */

  
  t-transcnt = t-transcnt + 1.
  run createPutRecords(input b-icer.whse, 
                       input b-icer.prod,
                       input v-jrnlno, 
                       input v-receiptId,
                       input i-ordertype, 
                       input recid(b-icer), input currputrec,
                       input if avail(b-poel) then b-poel.nonstockty
                             else if avail b-wtel then
                              b-wtel.nonstockty
                             else "").
  /* can it go off icer or do I have to look at oeelk and wtel? */

  if last-of(b-icer.prod) then do:
    run createPutRecHeader(input b-icer.whse, b-icer.prod,
      v-jrnlno, v-receiptId,
      i-ordertype, s-qtyalloc, s-qtypicked, s-qtyrcvd,
      b-icer.operinit, input recid(b-icer), currputrec).
    assign
      s-qtyalloc  = 0
      s-qtypicked = 0
      s-qtyrcvd   = 0.
  end.
end.
end procedure. /* processIcer */
/** end oeepbpr.p **/

procedure createPutRecHeader:
/* create zsdiput */
def input parameter i-whse like oeel.whse no-undo.
def input parameter i-prod like oeel.shipprod no-undo.
def input parameter i-jrnlno as dec  no-undo.
def input parameter i-rcptid as char no-undo.
def input parameter i-order  as char no-undo.
def input parameter i-alloc  as dec  no-undo.
def input parameter i-picked as dec  no-undo.
def input parameter i-rcvd   as dec  no-undo.
def input parameter i-user   as char no-undo.
def input parameter i-recid  as recid no-undo.
def input parameter i-putrec as dec  no-undo.
def buffer b-hdr for zsdiput.
def buffer b-icsp for icsp.
def buffer b-icsw for icsw.
def buffer b-wmsbp for wmsbp.
def buffer b-wmsb  for wmsb.
def buffer b-zzbin for zzbin.
def buffer b-dtl   for zsdiputdtl.
def buffer b-poel  for poel.
def buffer b-icer  for icer.

def var currBin like wmsb.binloc no-undo.
def var cprior  like wmsb.priority initial 10 no-undo.
def var zzFlag  as   logical  initial no no-undo.
def var bin1    like wmsb.binloc no-undo.
def var bin2    like wmsb.binloc no-undo.

create b-hdr.
/*
assign               */
b-hdr.cono = g-cono.
b-hdr.whse = i-whse. 
b-hdr.putrecno = integer(i-putrec).
b-hdr.user1 = i-order. 
b-hdr.jrnlno = i-jrnlno.
b-hdr.shipprod = i-prod.
b-hdr.receiptid = integer(i-rcptid).
b-hdr.receiverinit = i-user.
b-hdr.stkqtyrcv = i-rcvd.
b-hdr.stkqtyalloc = i-alloc.
b-hdr.stkqtypick = i-picked.
b-hdr.stkqtyputaway = if (i-rcvd - i-picked) < 0 then
                         0 
                      else 
                         i-rcvd - i-picked.
/* dt01 July 2008  should be                         
b-hdr.stkqtyputaway = i-rcvd - i-alloc. 
*/
if b-hdr.stkqtyputaway le 0 then 
  assign b-hdr.completefl = yes
         b-hdr.recordtype = "noqty".
b-hdr.operinit = g-operinit. 
b-hdr.receiptdt = TODAY.
b-hdr.xuser5 = STRING(zx-l).
{t-all.i "b-hdr"}
find first b-icsp where b-icsp.cono = g-cono and
b-icsp.prod = i-prod no-lock no-error.
if avail b-icsp then do:
  b-hdr.proddesc1 = b-icsp.descrip[1].
  b-hdr.proddesc2 = b-icsp.descrip[2].
  find first b-icsw where b-icsw.cono = g-cono and
  b-icsw.prod = i-prod and b-icsw.whse = i-whse no-lock no-error.
  if avail b-icsw then do:
/* INSPECT */
    assign v-inspectitem = false
           v-inspectbin  = "INSP999".
    if avail b-icsw and b-icsw.wmrestrict = "insp" then do:
      assign v-inspectitem = true.
      find first wmsbp where 
                 wmsbp.cono = g-cono + 500 and
                 wmsbp.whse = b-icsw.whse and
                 wmsbp.prod = b-icsw.prod and
                 wmsbp.binloc begins "INSP" 
                 no-lock no-error.
       if avail wmsbp then
         assign v-inspectbin = wmsbp.binloc.
       else do:
         find first zzbin where
                    zzbin.cono = g-cono + 500 and
                    zzbin.whse = b-icsw.whse and 
                    zzbin.binloc begins "INSP" and
                    zzbin.prod = b-icsw.prod no-lock no-error.
         if avail zzbin then 
           assign v-inspectbin = zzbin.binloc.
         else do:
           for each wmsb where 
                    wmsb.cono = g-cono + 500 and
                    wmsb.whse = b-icsw.whse and
                    wmsb.binloc begins "INSP"  and
                    wmsb.statuscode = ""  and 
                    not (can-find (first zzbin where
                                         zzbin.cono = g-cono + 500 and
                                         zzbin.whse = b-icsw.whse and 
                                         zzbin.binloc = wmsb.binloc 
                                         no-lock ))
                    no-lock:
             assign v-inspectbin = wmsb.binloc.
             leave.
           end.
         end.   
      end.
    end.
/* INSPECT */    
    
    bin1 = if v-inspectitem then v-inspectbin else b-icsw.binloc1.
    bin2 = if v-inspectitem then " " else b-icsw.binloc2.
    assign b-hdr.stkqtyputaway = if v-inspectitem and v-inspectbin <> " " then
                                   b-hdr.stkqtyrcv 
                                 else
                                   b-hdr.stkqtyputaway.

/* INSPECT */
    b-hdr.vendno = b-icsw.arpvendno.
    currBin = bin1.
    if currBin = "" or currBin = "New Part"
    and b-icsw.binloc2 ne "" and b-icsw.binloc2 ne "New Part"
      then currBin = b-icsw.binloc2.
  end.
/* INSPECT */
if b-hdr.completefl = yes and
   b-hdr.recordtype = "noqty" and 
   b-hdr.stkqtyputaway > 0 then do:
   assign b-hdr.completefl = no and
          b-hdr.recordtype = "". 
end.   
/* INSPECT */
end.
else do:
  release b-icsw.
  /* Non Stock? */
   find first b-icer where recid(b-icer) = i-recid no-lock no-error.
  
   if i-order = "PO" then do:
    find b-poel where b-poel.cono = g-cono and
         b-poel.pono = b-icer.pono and b-poel.posuf = b-icer.posuf   and
         b-poel.whse = b-icer.whse  and b-poel.lineno = b-icer.lineno and
         b-poel.shipprod = b-icer.prod no-lock no-error.
    if avail(b-poel) then do:
      b-hdr.proddesc1 = b-poel.proddesc.                           
      b-hdr.proddesc2 = b-poel.proddesc2.                           
      b-hdr.vendno = b-poel.vendno.
    /* check for not for resale line */
      if b-poel.nonstockty = "r" then 
        assign b-hdr.completefl = yes
               b-hdr.recordtype = "nfrs".
    end.                             
  end.
  find first b-wmsbp where b-wmsbp.cono = g-cono + 500 and
  b-wmsbp.whse = b-icer.whse and b-wmsbp.prod = b-icer.prod no-lock no-error.
  if avail b-wmsbp then do:
    /* b-hdr.binloc1 = b-wmsbp.binloc. */
    bin1 = b-wmsbp.binloc.
    find next b-wmsbp where b-wmsbp.cono = g-cono + 500 and
    b-wmsbp.whse = b-icer.whse and b-wmsbp.prod = b-icer.prod no-lock no-error.
    if avail b-wmsbp then /* b-hdr.binloc2 = b-wmsbp.binloc. */
      bin2 = b-wmsbp.binloc.
  end.
  currBin = "".                                    
end.  

if currBin = "New Part" then currBin = "".
if bin1 = "New Part" then bin1 = "".
if bin2 = "New Part" then bin2 = "".

/* why this? */
if not avail b-icsw then do:
  for each b-wmsbp 
  where b-wmsbp.cono = g-cono + 500 and
  b-wmsbp.prod = i-prod and b-wmsbp.whse = i-whse no-lock:
    find first b-wmsb 
    where b-wmsb.cono = b-wmsbp.cono and
    b-wmsb.whse = b-wmsbp.whse and b-wmsb.binloc = b-wmsbp.binloc
    no-lock no-error.
    if avail b-wmsb then do:
      if b-wmsb.priority = 9 then zzFlag = yes.
      if b-wmsb.priority = 1 then bin1 = b-wmsb.binloc.
      if b-wmsb.priority = 2 then bin2 = b-wmsb.binloc.
      if b-wmsb.priority < cprior then do:
        currbin = b-wmsb.binloc.
        /* b-hdr.locationid = b-wmsb.building. */
        cprior = b-wmsb.priority.
      end.
    end.  
  end.
end.
find first b-wmsb where b-wmsb.cono = g-cono + 500 and
b-wmsb.whse = i-whse and b-wmsb.binloc = currbin no-lock no-error.
if avail b-wmsb then do:
  b-hdr.locationid = b-wmsb.building.
  b-hdr.zzbinfl    = zzFlag.
  b-hdr.binloc1    = bin1.
  b-hdr.binloc2    = bin2.
  for each b-dtl use-index dtlidix where 
   b-dtl.cono = b-hdr.cono and
   b-dtl.putrecno = b-hdr.putrecno and 
   b-dtl.whse     = b-hdr.whse and
   /* b-dtl.locationid = b-hdr.locationid and */
   b-dtl.receiptid = b-hdr.receiptid and
   b-dtl.shipprod  = b-hdr.shipprod :
    b-dtl.binloc1 = bin1.
    b-dtl.binloc2 = bin2.
    b-dtl.locationid = b-hdr.locationid.
    b-dtl.zzbinfl = b-hdr.zzbinfl.
  end.
end.

end procedure. /* createPutRecHeader */

/** procedure 1 **/
procedure createPutRecords:
def input parameter i-whse like oeel.whse no-undo.
def input parameter i-prod like oeel.shipprod no-undo.
def input parameter i-jrnlno as dec  no-undo.
def input parameter i-rcptid as char no-undo.
def input parameter i-order  as char no-undo.
def input parameter i-recid  as recid no-undo.
def input parameter i-putrec as dec  no-undo.
def input parameter i-nonstkty as char no-undo.
def buffer b-zpd for zsdiputdtl.
def buffer b-icer for icer.
/* INSPECT */
def buffer b-icsw for icsw.
/* INSPECT */


find first b-icer where recid(b-icer) = i-recid.

create b-zpd.
assign
b-zpd.cono         = g-cono.
b-zpd.putrecno     = integer(i-putrec).
b-zpd.ordertype    = i-order. /* b-icer.ordertype. */
/* this is true only if ordertype is PO */
/* b-zpd.user1        = i-order. */
b-zpd.orderno      = b-icer.pono.
b-zpd.ordersuf     = b-icer.posuf.
b-zpd.lineno       = b-icer.lineno.
/* b-zpd.seqno        = */


/* INSPECT */
assign v-inspectitem = false
       v-inspectbin  = "INSP999".

find first b-icsw where 
           b-icsw.cono = g-cono and
           b-icsw.prod = i-prod and 
           b-icsw.whse = i-whse no-lock no-error.
if avail b-icsw and b-icsw.wmrestrict = "insp" then do:
  assign v-inspectitem = true.
  find first wmsbp where 
             wmsbp.cono = g-cono + 500 and
             wmsbp.whse = b-icsw.whse and
             wmsbp.prod = b-icsw.prod and
             wmsbp.binloc begins "INSP" 
             no-lock no-error.
   if avail wmsbp then
     assign v-inspectbin = wmsbp.binloc.
   else do:
     find first zzbin where
                zzbin.cono = g-cono + 500 and
                zzbin.whse = b-icsw.whse and 
                zzbin.binloc begins "INSP" and
                zzbin.prod = b-icsw.prod no-lock no-error.
     if avail zzbin then 
       assign v-inspectbin = zzbin.binloc.
     else do:
       for each wmsb where 
                wmsb.cono = g-cono + 500 and
                wmsb.whse = b-icsw.whse and
                wmsb.binloc begins "INSP"  and
                wmsb.statuscode = ""  and 
                not (can-find (first zzbin where
                                     zzbin.cono = g-cono + 500 and
                                     zzbin.whse = b-icsw.whse and 
                                     zzbin.binloc = wmsb.binloc 
                                     no-lock ))
                 no-lock:
         assign v-inspectbin = wmsb.binloc.
         leave.
       end.
     end.   
  end.
end.
assign b-zpd.stkqtyputaway = if v-inspectitem and v-inspectbin <> " " then
                               b-zpd.stkqtyrcv 
                             else
                             if (b-icer.qtyrcvd - b-icer.qtypicked) < 0 then
                               0
                             else
                               b-icer.qtyrcvd - b-icer.qtypicked. 
/* INSPECT */
 






/* 
assign                                 */
b-zpd.whse         = b-icer.whse.
b-zpd.receiptid    = integer(i-rcptid).
b-zpd.jrnlno       = i-jrnlno.
b-zpd.shipprod     = b-icer.prod.
b-zpd.stkqtyrcv    = b-icer.qtyrcvd.
b-zpd.stkqtyalloc  = b-icer.qtyalloc.
b-zpd.stkqtypick   = b-icer.qtypicked.
/*
b-zpd.stkqtyputaway = if (b-icer.qtyrcvd - b-icer.qtypicked) < 0 then
                        0
                      else
                       b-icer.qtyrcvd - b-icer.qtypicked. 
*/
if b-zpd.stkqtyputaway = 0 then do:
   assign b-zpd.completefl = yes
          b-zpd.recordtype = "noqty".
end.
if i-nonstkty = "r" then do:
   assign b-zpd.completefl = yes
          b-zpd.recordtype = "nrfs".
end.

/* 
b-zpd.locationid   =
b-zpd.binloc1      =
b-zpd.binloc2      =
b-zpd.zzbinfl      =
b-zpd.extype       =
b-zpd.exstatustype =
b-zpd.completefl   =          . */
{t-all.i "b-zpd"}
end procedure.
 
 
/* These are examples of other types of receiveing/transfer */
/**************
find first oeeh where oeeh.cono = g-cono and
  oeeh.pono = 

oeelfmrcvsloop:
for each oeel use-index k-fill where
    oeel.cono        =  g-cono    and
    oeel.statustype  =  "a":U     and
    oeel.whse        =  i-whse    and
    oeel.invoicedt   =  ?         and
    oeel.shipprod    =  i-prod    and
    oeel.qtyfmrcvs   <> 0
no-lock:
  do for oeeh:
    {w-oeeh.i oeel.orderno oeel.ordersuf no-lock}
    if not avail oeeh or
      (avail oeeh and oeeh.stagecd > 2)
      then next oeelfmrcvsloop.
  end. /* do for oeeh */
  find first b-zpd use-index k-icer where
    b-zpd.cono      = oeel.cono      and
    b-zpd.whse      = oeel.whse      and
    b-zpd.shipprod  = oeel.shipprod  and
    b-zpd.ordertype = "OE":U         
  no-lock no-error.
  if not available b-zpd then do:
    create b-zpd.
    b-zpd.cono      = oeel.cono.
    b-zpd.whse      = oeel.whse.
    b-zpd.shipprod  = oeel.shipprod.
    b-zpd.ordertype = "OE":U.
  end.
  assign
    b-zpd.orderno      = oeel.orderno
    b-zpd.ordersuf     = oeel.ordersuf
    b-zpd.lineno       = oeel.lineno
    b-zpd.seqno        = 0.
end. /* for each oeel */

/*d Check kit line items for non-zero quantity from receiver items */
oeelkfmrcvsloop:
for each oeelk use-index k-prod where
  oeelk.cono        =  {&cono}   and
  oeelk.whse        =  {&whse}   and
  oeelk.shipprod    =  {&prod}   and
  oeelk.statustype  =  "a":u     and
  oeelk.qtyfmrcvs   <> 0 no-lock:
  do for oeeh:
    {w-oeeh.i oeelk.orderno oeelk.ordersuf no-lock}
    if not avail oeeh or
      (avail oeeh and oeeh.stagecd > 2)
      then next oeelkfmrcvsloop.
  end. /* do for oeeh */
  find first t-porii use-index k-porii where
    t-porii.ordertype = "OE":U          and
    t-porii.orderno   = oeelk.orderno   and
    t-porii.ordersuf  = oeelk.ordersuf  and
    t-porii.lineno    = oeelk.lineno    and
    t-porii.seqno     = oeelk.seqno
    no-lock no-error.
  if not available t-porii then do:
    {w-oeel.i oeelk.orderno oeelk.ordersuf oeelk.lineno no-lock}
    create t-porii.
    assign
      t-porii.prod         = oeelk.shipprod
      t-porii.orderno      = oeelk.orderno
      t-porii.ordersuf     = oeelk.ordersuf
      t-porii.lineno       = oeelk.lineno
      t-porii.seqno        = oeelk.seqno
      t-porii.whse         = oeelk.whse
      t-porii.notesfl      = " "
      t-porii.transtype    = if avail oeel then oeel.transtype else " "
      t-porii.qtyord       = oeelk.stkqtyord
      t-porii.qtyship      = oeelk.stkqtyship
      t-porii.qtyfmrcvs    = oeelk.qtyfmrcvs
      t-porii.ordertype    = "OE":U.
  end. /* if not available */
end. /* for each oeelk */

/*d Check WT line items for non-zero quantity from receiver items */
wtelfmrcvsloop:
for each b-wtel use-index k-fill where
  b-wtel.cono        =  {&cono}   and
  b-wtel.statustype  =  "a":u     and
  b-wtel.shipfmwhse  =  {&whse}   and
  b-wtel.shipprod    =  {&prod}   and
  b-wtel.qtyfmrcvs   <> 0
no-lock:
  do for b-wteh:
    {w-wteh.i b-wtel.wtno b-wtel.wtsuf no-lock b-}
    if not avail b-wteh or
      (avail b-wteh and b-wteh.stagecd > 2)
      then next wtelfmrcvsloop.
  end. /* do for b-wteh */

  find first t-porii use-index k-porii where
    t-porii.ordertype = "WT":U        and
    t-porii.orderno   = b-wtel.wtno   and
    t-porii.ordersuf  = b-wtel.wtsuf  and
    t-porii.lineno    = b-wtel.lineno and
    t-porii.seqno     = 0
  no-lock no-error.

  if not available t-porii then do:
    create t-porii.
    assign
      t-porii.prod         = b-wtel.shipprod
      t-porii.orderno      = b-wtel.wtno
      t-porii.ordersuf     = b-wtel.wtsuf
      t-porii.lineno       = b-wtel.lineno
      t-porii.seqno        = 0
      t-porii.whse         = b-wtel.shipfmwhse
      t-porii.notesfl      = " "
      t-porii.transtype    = b-wtel.transtype
      t-porii.qtyord       = b-wtel.stkqtyord
      t-porii.qtyship      = b-wtel.stkqtyship
      t-porii.qtyfmrcvs    = b-wtel.qtyfmrcvs
      t-porii.ordertype    = "WT":U.
  end. /* if not available */
end. /* for each b-wtel */
***************/
/** end proc 1  createPutRecords */



/*h*******************************************************************
  INCLUDE      : porii.lpr
/*d Check OE line items for non-zero quantity from receiver items */
oeelfmrcvsloop:
for each oeel use-index k-fill where
    oeel.cono        =  {&cono}   and
    oeel.statustype  =  "a":U     and
    oeel.whse        =  {&whse}   and
    oeel.invoicedt   =  ?         and
    oeel.shipprod    =  {&prod}   and
    oeel.qtyfmrcvs   <> 0
no-lock:
  do for oeeh:
    {w-oeeh.i oeel.orderno oeel.ordersuf no-lock}
    if not avail oeeh or
      (avail oeeh and oeeh.stagecd > 2)
      then next oeelfmrcvsloop.
  end. /* do for oeeh */
  find first t-porii use-index k-porii where
    t-porii.ordertype = "OE":U         and
    t-porii.orderno   = oeel.orderno   and
    t-porii.ordersuf  = oeel.ordersuf  and
    t-porii.lineno    = oeel.lineno    and
    t-porii.seqno     = 0
  no-lock no-error.
  if not available t-porii then do:
    create t-porii.
    assign
      t-porii.prod         = oeel.shipprod
      t-porii.orderno      = oeel.orderno
      t-porii.ordersuf     = oeel.ordersuf
      t-porii.lineno       = oeel.lineno
      t-porii.seqno        = 0
      t-porii.whse         = oeel.whse
      t-porii.notesfl      = " "
      t-porii.transtype    = oeel.transtype
      t-porii.qtyord       = oeel.stkqtyord
      t-porii.qtyship      = oeel.stkqtyship
      t-porii.qtyfmrcvs    = oeel.qtyfmrcvs
      t-porii.ordertype    = "OE":U.
  end. /* if not available */
end. /* for each oeel */

/*d Check kit line items for non-zero quantity from receiver items */
oeelkfmrcvsloop:
for each oeelk use-index k-prod where
  oeelk.cono        =  {&cono}   and
  oeelk.whse        =  {&whse}   and
  oeelk.shipprod    =  {&prod}   and
  oeelk.statustype  =  "a":u     and
  oeelk.qtyfmrcvs   <> 0 no-lock:
  do for oeeh:
    {w-oeeh.i oeelk.orderno oeelk.ordersuf no-lock}
    if not avail oeeh or
      (avail oeeh and oeeh.stagecd > 2)
      then next oeelkfmrcvsloop.
  end. /* do for oeeh */
  find first t-porii use-index k-porii where
    t-porii.ordertype = "OE":U          and
    t-porii.orderno   = oeelk.orderno   and
    t-porii.ordersuf  = oeelk.ordersuf  and
    t-porii.lineno    = oeelk.lineno    and
    t-porii.seqno     = oeelk.seqno
    no-lock no-error.
  if not available t-porii then do:
    {w-oeel.i oeelk.orderno oeelk.ordersuf oeelk.lineno no-lock}
    create t-porii.
    assign
      t-porii.prod         = oeelk.shipprod
      t-porii.orderno      = oeelk.orderno
      t-porii.ordersuf     = oeelk.ordersuf
      t-porii.lineno       = oeelk.lineno
      t-porii.seqno        = oeelk.seqno
      t-porii.whse         = oeelk.whse
      t-porii.notesfl      = " "
      t-porii.transtype    = if avail oeel then oeel.transtype else " "
      t-porii.qtyord       = oeelk.stkqtyord
      t-porii.qtyship      = oeelk.stkqtyship
      t-porii.qtyfmrcvs    = oeelk.qtyfmrcvs
      t-porii.ordertype    = "OE":U.
  end. /* if not available */
end. /* for each oeelk */

/*d Check WT line items for non-zero quantity from receiver items */
wtelfmrcvsloop:
for each b-wtel use-index k-fill where
  b-wtel.cono        =  {&cono}   and
  b-wtel.statustype  =  "a":u     and
  b-wtel.shipfmwhse  =  {&whse}   and
  b-wtel.shipprod    =  {&prod}   and
  b-wtel.qtyfmrcvs   <> 0
no-lock:
  do for b-wteh:
    {w-wteh.i b-wtel.wtno b-wtel.wtsuf no-lock b-}
    if not avail b-wteh or
      (avail b-wteh and b-wteh.stagecd > 2)
      then next wtelfmrcvsloop.
  end. /* do for b-wteh */

  find first t-porii use-index k-porii where
    t-porii.ordertype = "WT":U        and
    t-porii.orderno   = b-wtel.wtno   and
    t-porii.ordersuf  = b-wtel.wtsuf  and
    t-porii.lineno    = b-wtel.lineno and
    t-porii.seqno     = 0
  no-lock no-error.

  if not available t-porii then do:
    create t-porii.
    assign
      t-porii.prod         = b-wtel.shipprod
      t-porii.orderno      = b-wtel.wtno
      t-porii.ordersuf     = b-wtel.wtsuf
      t-porii.lineno       = b-wtel.lineno
      t-porii.seqno        = 0
      t-porii.whse         = b-wtel.shipfmwhse
      t-porii.notesfl      = " "
      t-porii.transtype    = b-wtel.transtype
      t-porii.qtyord       = b-wtel.stkqtyord
      t-porii.qtyship      = b-wtel.stkqtyship
      t-porii.qtyfmrcvs    = b-wtel.qtyfmrcvs
      t-porii.ordertype    = "WT":U.
  end. /* if not available */
end. /* for each b-wtel */  ****************/



