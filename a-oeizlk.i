/*h*****************************************************************************
  SX 55
  INCLUDE      : a-xoeizl.i
  DESCRIPTION  : Detail inquiry
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
*******************************************************************************/

if g-oecustno = {c-empty.i} and ({k-func6.i} or {k-func7.i}) then do:
    {p-oeizl.i}
    if  {k-func6.i} then do:
        assign
            g-modulenm = "".
            g-title    = "Commission Sales Order Inquiry".
        if g-operinit begins "das" then
          do:
          message "here 1" "v-xmode:" v-xmode. pause.
        end.
        run oeizs.p.
    end.
    /*
    else if {k-func7.i} then do:
        g-title = "Order Entry Inquiry - Confirmation".
        run oeic.p.
    end.
    */
    /* return to calling function to jump. smb 6-18-92*/
    put screen row 21 col 3 color message
 " F6-Detail                     F8-Customer    F9-Prod Avail  F10-Incoming   ".
    if {k-jump.i} or {k-select.i} or not
        (g-modulenm = "" or g-modulenm = "pa") then leave main.
    hide message no-pause.
    next.
end.
/*tb 11811 04/28/95 smb; Changed substring for line to 13*/
else if {k-func19.i} and (v-mode = 2 or v-mode = 4) then do:
    if oeelb.commentfl = yes then do:
        frame-value = substring(string(frame-value),13,3).
        run comd.p.
        frame-value = o-frame-value.
        put screen row 21 col 3 color message
 " F6-Detail      F7-Confirm     F8-Customer    F9-Prod Avail  F10-Incoming   ".
        if {k-jump.i} or {k-select.i} or not
        (g-modulenm = "" or g-modulenm = "pa") then leave main.
        hide message no-pause.
        next.
    end.
end.

