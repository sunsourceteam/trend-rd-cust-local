/* SX 55 */
/* poetle.p 1.2 09/01/98 */
/*h*****************************************************************************
  INCLUDE      : poetle.p
  DESCRIPTION  : po extended line item
  USED ONCE?   :
  AUTHOR       : kmw
  DATE WRITTEN :
  CHANGES MADE :
    08/18/95 kr ; TB# 18990 Add user hook &user_editaft
    01/22/97 bp ; TB# 22397 Added user hooks &user_custvar, &user_befupdate,
                            and &user_resetvar
    01/24/97 mtt; TB# 22457 Develop Work Flow functionality.
    05/17/97 jl;  TB# 5594 (4E) Unavailable Tracking.  Added update of
        s-reaunavty to allow update on unavailable reason. Also added
        valmsg.gas , v-reaserrfl, and oc-reasunavty.
    08/28/98 mwb; TB# 25262 Added TWL specific edits to link the PO/RM
        lines to a specific unavailable reason and edit all the current
        orders and ICSOU balances so not over allocated or cause Inventory
        out of balance with TWL and Trend.  Added wlwhse.gva and v-reaserrno.
    05/10/99 bm;  TB# 18548 Added expshipdt, reqshipdt
    06/17/99 gkd; TB# t26314 Add nonstockty parameter to poetluq.p call
    12/03/99 abe; TB# t27013 Add user hook &user_after_updt
*******************************************************************************/
{g-all.i}
{g-ic.i}

/* from g-poet.i */
{g-po.i}

/*tb 22457 01/24/97 mtt; Develop Work Flow functionality */
{g-poetl.i "shared"}

/*tb 25262 08/28/98 mwb; Added wlwhse.gva, wlunreas.lva, input v-reaserrno */
{wlwhse.gva "shared"}
{wlunreas.lva}

def input parameter v-reaserrfl     as logical           no-undo.
def input parameter v-reaserrno     as int               no-undo.

def new shared var  s-pono          like poeh.pono       no-undo.
def new shared var  s-posuf         like poeh.posuf      no-undo.
def new shared var  oc-duedt        like poel.duedt      no-undo.
def new shared var  oc-reqshipdt    like poel.reqshipdt  no-undo.
def new shared var  oc-expshipdt    like poel.expshipdt  no-undo.

/*u User Hook*/
{poetle.z99 &user_custvar = "*"}

{valmsg.gas &string = "2100,4027"}


/* poetle.p specific */
/*e These variable are defined to reset variable if cancel
     is hit since these variables are no-undo */
def            var oc-weight        like poel.weight          no-undo.
def            var oc-cubes         like poel.cubes           no-undo.
def            var oc-totlnwt       like poel.weight          no-undo.
def            var oc-totlncubes    like poel.cubes           no-undo.
def            var oc-reasunavty    like poel.reasunavty      no-undo.

{f-poetle.i} title "Extend".
pause 0 before-hide.
assign
    oc-duedt      = s-duedt
    oc-reqshipdt  = s-reqshipdt
    oc-expshipdt  = s-expshipdt
    oc-weight     = s-weight
    oc-cubes      = s-cubes
    oc-totlnwt    = s-totlnwt
    oc-totlncubes = s-totlncubes
    oc-reasunavty = s-reasunavty.

/*u User Hook*/
{poetle.z99 &user_befupdate = "*"}

main:
do while true with frame f-poetle on endkey undo main, leave main:

    display
        s-stkqtyord
        s-stkunit
        s-reqshipdt
        s-expshipdt
        s-weight
        s-totlnwt
        s-cubes
        s-totlncubes.

    if v-reaserrfl = yes then do:

        /*d Required field or Unavailable Reason on in SASTT */
        /*tb 25262 08/28/98 mwb; Added v-reaserrno edits for display */
        if s-reasunavty = "" then run err.p(2100).
        else if v-reaserrno ne 0 then run err.p(v-reaserrno).
        else run err.p (4027).

        next-prompt s-reasunavty.

    end.

    /*o Allow entry of the extended screen fields */
    update
        s-duedt
        /*
        s-reqshipdt
        s-expshipdt
        */
        s-weight                                when s-nonstockty <> ""
        s-cubes                                 when s-nonstockty <> ""
        s-reasunavty
         validate
             ({v-sasta.i L s-reasunavty "/*"},
              if input s-reasunavty = "" then ({valmsg.gme 2100})
              else {valmsg.gme 4027})
             {f-help.i}                         when g-potype = "rm"
        editing:
        readkey.
        if {k-after.i} then do:

            /*tb 18990 08/18/95 kr; Add user hook &user_editaft */
            {poetle.z99 &user_editaft = "*"}

            /*d Calculate and display the total line weight and cube */
            if frame-field = "s-weight" then do:
                s-totlnwt    = input s-weight * s-qtyord.
                display s-totlnwt.
            end.

            if frame-field = "s-cubes" then do:
                s-totlncubes = input s-cubes  * s-qtyord.
                display s-totlncubes.
            end.

        end.
        apply lastkey.
    end.
    /* tb# 27013  User Hook */
    {poetle.z99 &user_after_updt = "*"}

    /*tb 25262 08/28/98 mwb; Added edit logic for ICSOU validation */
    if v-wlwhsefl = yes and g-potype = "rm" then do:

        /*tb t26314 06/17/99 gkd; Add s-nonstockty to params */
        /*d Edits Current PO's, ICSOU, and ICSW Unavail Qty */
        run poetluq.p (g-cono,
                       g-whse,
                       s-prod,
                       s-reasunavty,
                       s-stkqtyord,
                       g-pono,
                       g-posuf,
                       s-lineno,
                       s-nonstockty,
                       output v-reaserrno).

        if v-reaserrno ne 0 then do:
            v-reaserrfl = yes.
            next main.
        end.

    end. /* v-wlwhsefl */

    /*tb 22457 01/24/97 mtt; Develop Work Flow functionality */
    assign
         s-pono    = g-pono
         s-posuf   = g-posuf.
    {tqtrig.gpr &triggernm = ""poetle""
                &tqpostdt  = "g-today"
                &tqperiod  = "0"
                &tqfiles   = """"
                &tqrecids  = """"}

    leave main.
end.

/*d If cancel pressed, reset all no-undo variabes */
if {k-cancel.i} then do:
    assign
        s-duedt      = oc-duedt
        s-reqshipdt  = oc-reqshipdt
        s-expshipdt  = oc-expshipdt
        s-weight     = oc-weight
        s-cubes      = oc-cubes
        s-totlnwt    = oc-totlnwt
        s-totlncubes = oc-totlncubes
        s-reasunavty = oc-reasunavty.

    /*u User Hook*/
    {poetle.z99 &user_resetvar = "*"}
end.
hide frame f-poetle no-pause.
hide message no-pause.

