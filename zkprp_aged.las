/*zkprp_aged.las 1.1  05/19/00*/
/*h****************************************************************************
  INCLUDE      : zkprp_aged.las
  DESCRIPTION  : Kit Production - Production Schedule Report
  USED ONCE?   : yes (4 times in zkprp.lpr)
  AUTHOR       : SD
  DATE WRITTEN : 06/12/00  
  CHANGES MADE :
   06/12/00  SD: 206890 Original development of new custom report - assignments
******************************************************************************/
       
assign
    s-seqno       = t-kprp.seqno
    s-wono        = string(t-kprp.wono,"9999999")
    s-wosuf       = string(t-kprp.wosuf,"99")
    s-custname    = t-kprp.custname
    s-custpo      = t-kprp.custpo
    s-whse        = t-kprp.whse
    s-promisedt   = if p-srtprom = "p" then 
                       t-kprp.promisedt3 
                    else   
                       t-kprp.promisedt
    s-technician  = t-kprp.technician
    s-rate        = t-kprp.rate
    s-week        = t-kprp.assgnwk
    s-hours       = t-kprp.hours
    s-units       = t-kprp.stkqtyord
    s-qtybld      = t-kprp.stkqtyship
    s-whselabel   = "      Whse " + t-kprp.whse + " Total:"
    s-techlabel   = "Technician " + t-kprp.technician + " Total:"
    s-techgtlbl   = "     Grand " + t-kprp.technician + " Total:"
    s-totprodcost = t-kprp.totprodcost                                             s-whsetotcnt  = s-whsetotcnt + 1 
    s-whsetothrs  = s-whsetothrs + t-kprp.hours 
    s-whsetotval  = s-whsetotval + t-kprp.totprodcost  
    s-techtotcnt  = s-techtotcnt + 1                  
    s-techtothrs  = s-techtothrs + t-kprp.hours       
    s-techtotval  = s-techtotval + t-kprp.totprodcost  
    s-totalcnt    = s-totalcnt   + 1                   
    s-totalhrs    = s-totalhrs   + t-kprp.hours        
    s-totalval    = s-totalval   + t-kprp.totprodcost 
    s-kitprod     = t-kprp.shipprod
    s-orderaltno  = t-kprp.orderaltno
    s-orderaltsuf = t-kprp.orderaltsuf
    s-ordertype   = t-kprp.ordertype
    s-linealtno   = t-kprp.linealtno
    s-stage       = if t-kprp.stagecd = 1 then
                      "Opn"
                    else
                    if t-kprp.stagecd = 2 then
                      "Prt"
                    else
                    if t-kprp.stagecd = 3 then
                      "Blt"
                    else
                      "Can".

