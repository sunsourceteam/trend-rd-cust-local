/*************************************************************************
  Programe Name: icxn.p
  Comments     : A valid zsdibarcd record must be present with appropriate
                 information to match the customer, label name and printer  
                 If label or printer is invalid an error is returned. 
*************************************************************************/

{g-all.i}
{g-po.i}
{g-wt.i}
{g-oe.i}
def var ctr   as int                         no-undo.
def var v-custno    like oeeh.custno         no-undo. 
def var v-type      as char   format "x(4)"  no-undo. 
def var v-whse      like oeeh.whse           no-undo. 
def var v-lblnm     as char format "x(16)"   no-undo. 
def var v-datanm    as char format "x(16)"   no-undo. 
def var v-printernm like sasp.printernm      no-undo. 
def var v-copies    as int  format "zz9"     no-undo. 
def var v-apptype   as char format "x(6)"    no-undo. 
def var v-style     as char format "x(6)"    no-undo. 
def var v-recid     as recid                 no-undo.
def var v-row       as int                   no-undo.
def var v-inq       as int                   no-undo.
def var v-rowid     as rowid                 no-undo.

def var x-inx as integer                     no-undo.
def var v-orderno   like oeeh.orderno        no-undo.
def var v-ordersuf  like oeeh.ordersuf       no-undo.
def var v-oeline    like oeel.lineno         no-undo.
def var v-lineno    as char format "x(7)"    no-undo.
def var v-error     as char format "x(70)"   no-undo.
def var v-custpo   as char format "x(10)"    no-undo.
def var v-supplier as char format "x(5)"     no-undo.
def var v-filename as c format "x(30)"       no-undo.
def var v-tilda as c format "x" init "~~"    no-undo.
def var v-custprod like  poel.shipprod       no-undo.
def var lk-prod    like  poel.shipprod       no-undo.
def var v-printername as c                   no-undo.
def var v-labelname as c                     no-undo.
def var v-ordnbr    as char format "x(10)"   no-undo.
def var v-whsechk   as char format "x(4)"    no-undo.
def var v-uom       as char format "x(4)"    no-undo.
def var v-rev       as char format "x(2)"    no-undo.
def var v-poline    as char format "xxx"     no-undo.
def var v-shipdt like oeel.reqshipdt         no-undo.
def var v-addr2 as char format "x(30)"       no-undo.
def var v-addr3 as char format "x(30)"       no-undo.
def var v-countrycd as c    format "x(10)"   no-undo.
def var v-valid-add     as logical               no-undo.
def var v-outputfl  as logical               no-undo.
def var v-quantity  like poel.qtyrcv         no-undo. 
def var x-quantity  like poel.qtyrcv         no-undo. 
def var p-qty       as int                   no-undo. 
def var p-idx       as int                   no-undo. 
def var x-labels    as integer               no-undo.
def var z-labels    as integer               no-undo.
def var v-icswbinloc1 as char format "x(10)" no-undo.
def var v-icswbinloc2 as char format "x(10)" no-undo.
def var x-functions   as char format "x(78)" dcolor 2 no-undo.  


define temp-table barcd no-undo 
   field btype      as char format "x(4)"
   field custno     as dec  format ">>>>>>>99999" 
   field whseprt    as char format "x(4)" 
   field printernm  as char format "x(12)" 
   field apptype    as char format "x(08)" 
   field lblnm      as char format "x(16)"
   field datanm     as char format "x(16)"
   field nbrcopies  as int  format ">>9"
   field style      as char format "x(6)" 
   field brecid     as recid 
   index bn-idx
     btype
     custno
     whseprt
     apptype. 
    
def buffer x-barcd     for barcd. 
def buffer d-zsdibarcd for zsdibarcd. 

define query q-barcd for barcd scrolling.
define browse b-barcd query q-barcd 
  display (string(barcd.btype,"x(4)") + " " +  
           string(int(barcd.custno),">>>>>>>>>>>>") + " " +                    
           string(barcd.printernm,"x(12)") + " "  + 
           string(barcd.lblnm, "x(16)") + " " +  
           string(barcd.apptype,"x(8)") + " " +  
           string(barcd.whseprt,"x(4)") + " " + 
           string(int(barcd.nbrcopies),"zz9") + "    " + 
           string(barcd.style,"x(6)")) format "x(74)" 
  with 17 down centered overlay no-hide no-labels
  title
"  Type      Custno  PrinterName   LabelName     AppType PrtWhse Copies Style  " .              

assign x-functions = 
"             F7-add             F8-change            F9-delete              ". 

define frame f-barcd 
 b-barcd      at row 2 col 1 
 x-functions  at 2 
 with width 80 overlay no-hide no-labels no-box. 

form
  "Product        : " at 1 
  icsp.prod       at 18 no-label
  "Cross Ref      : " at 1 
  v-custprod      at 18 no-label
  "Qty shipped    :" at 1
  v-quantity      at 18 no-label
  x-labels        at 1 label "Labels to print"
  "Qty printed on label" at 1
  p-qty           at 22 no-label 
  with frame f-zpop overlay side-labels width 45 column 20 row 8
       title " Custom Item Inventory Label Print ".
form
  "Search For: " at 1  
   lk-prod at 13
  with frame f-search  no-labels overlay row 7 column 22.

form
   v-type      at 5 label "Type          "
   v-custno    at 5 label "CustomerNo    "
   v-lblnm     at 5 label "LabelName     "
   v-datanm    at 5 label "TxDataName    "
   v-printernm at 5 label "PrinterName   " 
   {f-help.i}
   v-apptype   at 5 label "ApplicationTyp"
   v-whse      at 5 label "WhseToPrt     " 
   v-copies    at 5 label "Copies        "  
   v-style     at 5 label "StyleOfLabel  "
   with frame f-barcd-add width 40 column 20 row 5 side-labels overlay 
        title " Add Barcode Master Record ".   
    
form
   v-type      at 5 label "Type          "
   v-custno    at 5 label "CustomerNo    "
   v-lblnm     at 5 label "LabelName     "
   v-datanm    at 5 label "TxDataName    "
   v-printernm at 5 label "PrinterName   " 
   {f-help.i}
   v-apptype   at 5 label "ApplicationTyp"
   v-whse      at 5 label "WhseToPrt     " 
   v-copies    at 5 label "Copies        "  
   v-style     at 5 label "StyleOfLabel  "
   with frame f-barcd-chg width 40 column 20 row 5 side-labels overlay 
        title  " Customer Barcode Maintenance ".   

def buffer zxt2-sasos for sasos. 
find first zxt2-sasos where
           zxt2-sasos.cono  = g-cono and
           zxt2-sasos.oper2 = g-operinits and
           zxt2-sasos.menuproc = "zxt2"
           no-lock no-error.

on any-key of b-barcd do: 
 if {k-cancel.i} or {k-jump.i} then do: 
  hide frame f-barcd.   
  hide frame f-search.  
  apply "window-close" to current-window. 
  on cursor-up back-tab. 
  on cursor-up tab. 
 end.
 if lastkey = 13 or lastkey = 401 then do: 
  apply "window-close" to current-window. 
  hide frame f-barcd.
  leave.
 end.
 else 
 if {k-cancel.i} or {k-jump.i} then do: 
  hide frame f-barcd.
  apply "window-close" to current-window. 
  on cursor-up back-tab. 
  on cursor-up tab. 
 end.
 if {k-func7.i} or {k-func8.i} or {k-func9.i} then do: 
  if not avail zxt2-sasos  or 
    (avail zxt2-sasos and zxt2-sasos.securcd[7] < 3) then do:
   assign v-error = "--> Invalid security - Please contact supervisor". 
   run zsdierrx.p(v-error,"yes"). 
   leave.  
  end.
 end. 
 if {k-func7.i} then do: 
  assign v-row = b-barcd:focused-row. 
  assign v-rowid  = rowid(barcd). 
  run add-item. 
  find first x-barcd no-lock no-error.
  if avail x-barcd then do: 
   close query q-barcd. 
   open query q-barcd preselect each barcd. 
   b-barcd:refresh() in frame f-barcd.
   display b-barcd with frame f-barcd.
   enable b-barcd with frame f-barcd. 
   apply "entry" to b-barcd in frame f-barcd. 
   apply "focus" to b-barcd in frame f-barcd. 
   on cursor-up cursor-up. 
   on cursor-down cursor-down. 
  end. 
  hide frame f-barcd-add.
  assign v-row = b-barcd:focused-row.
 end.
 else 
 if {k-func8.i} then do: 
  if not avail zxt2-sasos  or 
   (avail zxt2-sasos and zxt2-sasos.securcd[7] < 3) then do:
   assign v-error = "--> Invalid security - Please contact supervisor". 
   run zsdierrx.p(v-error,"yes"). 
  end.
  hide frame f-barcd-add. 
  assign v-row = b-barcd:focused-row. 
  run change-item. 
  hide frame f-barcd-chg.
  b-barcd:refresh() in frame f-barcd.
 end. /* if f8 */   
 else
 if {k-func9.i} then do: 
  if not avail zxt2-sasos  or 
   (avail zxt2-sasos and zxt2-sasos.securcd[7] < 4) then do:
   assign v-error = "--> Invalid security - Please contact supervisor". 
   run zsdierrx.p(v-error,"yes"). 
  end.
  message " Delete Barcode Master Record? "
  view-as alert-box question buttons yes-no
  update rd as logical.
  if not rd then 
   assign rd = rd.
  else  
  if rd then do: 
   find d-zsdibarcd where recid(d-zsdibarcd) = barcd.brecid
    exclusive-lock. 
   if avail d-zsdibarcd then 
    delete d-zsdibarcd. 
   b-barcd:delete-current-row().
   next.
  end. 
 end. /* if f9 */
end. 

main: 
repeat:
/* do while true on endkey undo, leave main.  */ 

  for each barcd exclusive-lock: 
   delete barcd. 
  end. 
  
  for each zsdibarcd use-index  k-custno 
     where zsdibarcd.cono   = g-cono no-lock:
     if zsdibarcd.type <> " " then do: 
      create barcd. 
      assign barcd.btype     = zsdibarcd.type  
             barcd.custno    = zsdibarcd.custno  
             barcd.printernm = zsdibarcd.printernm[1]
             barcd.lblnm     = zsdibarcd.labelnm 
             barcd.datanm    = zsdibarcd.txdatanm 
             barcd.whseprt   = zsdibarcd.whse 
             barcd.nbrcopies = zsdibarcd.copies 
             barcd.apptype   = zsdibarcd.apptype
             barcd.style     = zsdibarcd.style
             barcd.brecid    = recid(zsdibarcd). 
     end.
  end.  /* for each */
 
 open query q-barcd preselect each barcd. 
 enable b-barcd with frame f-barcd. 
 apply "entry" to b-barcd in frame f-barcd. 
 apply "focus" to b-barcd in frame f-barcd. 
 display b-barcd x-functions with frame f-barcd. 
 on cursor-up cursor-up. 
 on cursor-down cursor-down. 
 wait-for "window-close" of current-window. 

end. /* repeat  */

/**** procedure Area begins here  ****/ 

procedure add-item:

find first x-barcd no-lock no-error.

addupdt: 
do while true on endkey undo, leave addupdt:  
 update v-type     
        v-custno   
        v-lblnm    
        v-datanm 
        v-printernm
        v-apptype  
        v-whse     
        v-copies   
        v-style 
        with frame f-barcd-add.
 
 if input v-type      = "" and 
    input v-custno    = 0  and 
    input v-lblnm     = "" and 
    input v-printernm = "" and 
    input v-apptype   = "" and 
    input v-whse      = "" then do:  
  assign v-error = "--> Blank Add - BarCode Label Not Added.. ". 
  run zsdierrx.p(v-error,"yes"). 
 end.
 
 assign v-type      = input v-type  
        v-custno    = input v-custno  
        v-printernm = input v-printernm 
        v-lblnm     = input v-lblnm  
        v-whse      = input v-whse 
        v-copies    = if input v-copies = 0 then 
                       1 
                      else  
                       input v-copies 
        v-apptype   = input v-apptype
        v-style     = input v-style
        v-datanm    = input v-datanm. 
  
 if v-printernm ne "" then do: 
  find first sasp where  
             sasp.printernm = v-printernm
             no-lock no-error. 
  if not avail sasp /* or not (sasp.pcommand begins "sh") */ then do:  
   assign v-error = "Printer is not setup in (sasp)..".
   run zsdierrx.p(v-error,"yes"). 
   next-prompt v-printernm with frame f-barcd-add.
   next. 
  end. 
 end. 
 
 find first zsdibarcd use-index  k-custno 
      where zsdibarcd.cono         = g-cono      and 
            zsdibarcd.custno       = v-custno    and 
            zsdibarcd.labelnm      = v-lblnm     and 
            zsdibarcd.printernm[1] = v-printernm and 
            zsdibarcd.apptype      = v-apptype  
            no-lock no-error.  
 if avail zsdibarcd and 
    v-custno    = zsdibarcd.custno       and 
    v-lblnm     = zsdibarcd.labelnm      and 
    v-printernm = zsdibarcd.printernm[1] and 
    v-whse      = zsdibarcd.whse         and 
    v-apptype   = zsdibarcd.apptype      then do: 
  assign v-error = "--> Invalid Add - Customer Label Record Exists.. ". 
  run zsdierrx.p(v-error,"yes").
  leave. 
 end.

 if not avail zsdibarcd then do:       
  create zsdibarcd. 
  assign zsdibarcd.cono      = 01 
         zsdibarcd.style     = "new"
         zsdibarcd.type      = v-type  
         zsdibarcd.custno    = v-custno  
         zsdibarcd.printernm[1] = v-printernm 
         zsdibarcd.labelnm   = v-lblnm 
         zsdibarcd.txdatanm  = v-datanm    
         zsdibarcd.whse      = v-whse 
         zsdibarcd.copies    = v-copies
         zsdibarcd.transdt   = today
         zsdibarcd.transtm   = substring(string(time,"hh:mm"),1,2) +     
                               substring(string(time,"hh:mm"),4,2)       
         zsdibarcd.operinit  = g-operinit 
         zsdibarcd.apptype   = v-apptype
         zsdibarcd.style     = v-style.
         
  create barcd. 
  assign barcd.btype     = v-type  
         barcd.custno    = v-custno  
         barcd.printernm = v-printernm 
         barcd.lblnm     = v-lblnm  
         barcd.whseprt   = v-whse 
         barcd.nbrcopies = v-copies 
         barcd.apptype   = v-apptype
         barcd.style     = v-style
         barcd.datanm    = v-datanm 
         barcd.brecid    = recid(zsdibarcd). 
 
 end. 
 if keylabel(lastkey) = "PF1" then 
  leave addupdt.
end. /* while true */ 
return.  
end. 

procedure change-item:
 def buffer x-zsdibarcd for zsdibarcd. 

 find first zsdibarcd use-index  k-custno 
      where zsdibarcd.cono         = g-cono          and 
            zsdibarcd.custno       = barcd.custno    and 
            zsdibarcd.labelnm      = barcd.lblnm     and 
            zsdibarcd.printernm[1] = barcd.printernm and 
            zsdibarcd.apptype      = barcd.apptyp  
            exclusive-lock no-error.  
 if avail zsdibarcd then do:  
  assign v-custno    = zsdibarcd.custno        
         v-lblnm     = zsdibarcd.labelnm 
         v-datanm    = zsdibarcd.txdatanm        
         v-printernm = zsdibarcd.printernm[1]  
         v-whse      = zsdibarcd.whse   
         v-copies    = zsdibarcd.copies
         v-apptype   = zsdibarcd.apptype 
         v-type      = zsdibarcd.type
         v-style     = zsdibarcd.style.
         
  display v-type v-custno v-lblnm with frame f-barcd-chg. 
 end.  
 chgupdt: 
 do while true on endkey undo, leave chgupdt:  
 update /*
        v-type     
        v-custno   
        */
        v-lblnm    
        v-datanm
        v-printernm
        v-apptype  
        v-whse     
        v-copies   
        v-style
        with frame f-barcd-chg.
 
 assign v-printernm = input v-printernm 
       /* 
        v-type      = input v-type  
        v-custno    = input v-custno  
       */
        v-lblnm     = input v-lblnm  
        v-whse      = input v-whse 
        v-copies    = if input v-copies = 0 then 
                       1 
                      else  
                       input v-copies 
        v-apptype   = input v-apptype
        v-style     = input v-style
        v-datanm    = input v-datanm. 
 
 
 if v-printernm ne "" then do: 
  find first sasp where  
             sasp.printernm = v-printernm
             no-lock no-error. 
  if not avail sasp /* or not (sasp.pcommand begins "sh") */ then do: 
   assign v-error = "Printer is not setup in (sasp)..".
   run zsdierrx.p(v-error,"yes"). 
   next-prompt v-printernm with frame f-barcd-chg.
   next. 
  end. 
 end. 
 
 find first x-zsdibarcd use-index  k-custno 
      where x-zsdibarcd.cono         = g-cono      and 
            x-zsdibarcd.custno       = v-custno    and 
            x-zsdibarcd.labelnm      = v-lblnm     and 
            x-zsdibarcd.printernm[1] = v-printernm and  
            x-zsdibarcd.type         = v-type      and 
            x-zsdibarcd.whse         = v-whse      and 
            x-zsdibarcd.apptype      = v-apptype   and 
            x-zsdibarcd.style        = v-style 
            no-lock no-error.  
/*
 if avail x-zsdibarcd and 
    v-custno    = x-zsdibarcd.custno       and 
    v-type      = x-zsdibarcd.type         and 
    v-lblnm     = x-zsdibarcd.labelnm      and 
    v-printernm = x-zsdibarcd.printernm[1] and 
    v-whse      = x-zsdibarcd.whse         and 
    v-style     = x-zsdibarcd.style        and then do: 
  assign v-error = "--> Invalid Add - Customer Label Record Exists.. ". 
  run zsdierrx.p(v-error,"yes").
  next.  
 end.
*/

 if avail zsdibarcd then do:       
  assign barcd.btype     = v-type  
         barcd.custno    = v-custno  
         barcd.printernm = v-printernm 
         barcd.lblnm     = v-lblnm  
         barcd.whseprt   = v-whse 
         barcd.nbrcopies = v-copies 
         barcd.apptype   = v-apptype
         barcd.style     = v-style
         barcd.datanm    = v-datanm. 
  b-barcd:refresh() in frame f-barcd.
  assign zsdibarcd.style     = "new"
         zsdibarcd.type      = v-type  
         zsdibarcd.custno    = v-custno  
         zsdibarcd.printernm[1] = v-printernm 
         zsdibarcd.labelnm   = v-lblnm 
         zsdibarcd.txdatanm  = v-datanm 
         zsdibarcd.whse      = v-whse 
         zsdibarcd.copies    = v-copies
         zsdibarcd.transdt   = today
         zsdibarcd.transtm   = substring(string(time,"hh:mm"),1,2) +     
                               substring(string(time,"hh:mm"),4,2)       
         zsdibarcd.operinit  = g-operinit 
         zsdibarcd.apptype   = v-apptype
         zsdibarcd.style     = v-style.
 end. 
 if keylabel(lastkey) = "PF1" then 
  leave chgupdt.
end. /* while true */
return.
end.



/*

 assign v-uom      = oeel.unit.  
 if v-filename = "" then do: 
  nameloop: 
  do while true:
   v-filename = "/usr/tmp/" + string(time) + string(random(1,999)).
   if search(v-filename) = ? then
    leave nameloop.
  end.
  output stream bcd to value(v-filename).
  assign v-outputfl = true.
  put stream bcd unformatted
   "/L=" 
   v-labelname
   " "
   "/P="
   v-printername 
   chr(13)
   chr(10).

 end.                       
                  
 assign v-valid-add = no. 
 run grepoflabel.p(input v-labelname,
                   input c-custno,
                   input "pick",                   
                   input-output v-valid-add). 
 
 if not v-valid-add or v-labelname = "" then do: 
  run zsdierrx.p(input "Label NotFound for Customer.",true).     
  leave.
 end.
 
 if v-outputfl = true then do: 
  output stream bcd  close.
  unix silent value("sh /rd/unibar/ibprint.sh " + v-filename).
  unix silent value ("rm " + v-filename).
 end.

*/ 

 


