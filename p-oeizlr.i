/* p-oeizlr.i 1.1 01/03/98 */
/* p-oeizlr.i 1.2 09/22/97 */
/*h*****************************************************************************
  SX 55
  INCLUDE      : p-oeizlr.i
  DESCRIPTION  : For All Line Item Finds - Loads Report record to display
                     in Order# Order.
  USED ONCE?   : no
  AUTHOR       : mwb
  DATE WRITTEN : 12/09/93
  CHANGES MADE :
    12/09/93 mwb; TB# 13733; random order# order.
    09/22/97 kjb; TB# 22361 Quantity/Value displayed is incorrect when 'B' is
        entered for the order type - added new parameter &comtot
*******************************************************************************/
/*e
    Parameters:
        &com        Comments out the assignment of OEEL order line data in the
                    REPORT record.
        &comtot     Comments out the call of the internal procedure and
                    assignment of data based upon the order lines.
*******************************************************************************/

if confirm = yes then do:

    create report.

    assign
        report.cono      = g-cono
        report.oper2     = g-operinit
        report.reportnm  = g-ourproc
        report.c20       = "B" +
                          substring(string(int(oeehb.batchnm),">>>>>>>9"),1,8) 
                           + "-" + "00"      
                           {&com} +
                           string(oeelb.lineno,"999")
                           /{&com}* */
        report.i8        = int(oeehb.batchnm)
        report.i2        = 00
        {&com}
        report.i3        = oeelb.lineno /{&com* */.

    /*tb 22361 09/22/97 kjb; Call internal procedure tot-order-lines to
        calculate the qty/value of the lines with the product entered in the
        OEIZL banner.  Load that value and a flag on the REPORT record. */
    {&comtot}
    /*
    run tot-order-lines in this-procedure (buffer oeeh,
                                           output report.de9d2s).

    /*d Set a flag so that the display include will know to use the value
        just calculated instead of the value off the order header record */
    */
    report.l = yes.
    /{&comtot}* */

end. /* if confirm = yes */

