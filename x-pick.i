/*******************************************************************************
  PROCEDURE      : x-pick.i based on x-putaway.i
  DESCRIPTION    : 
  AUTHOR         : Sunsource
  DATE WRITTEN   : 04/01/07
  CHANGES MADE   :
                  03/12/08 dkt va section

*******************************************************************************/

{g-xxex.i "new shared"}
{g-ic.i  "new"}
{g-po.i  "new"} 
/* {g-conv.i} */
{g-jump.i}
{speccost.gva "new shared"}

{g-oeet.i "new shared"}
{g-oeetx.i "new shared"}
{g-oeetb.i "new shared"}

{g-oeetl.i "new shared"}
{g-oeetlo.i "new shared"}
{g-oeetlr.i "new shared"}
{g-oeetlx.i "new shared"}
{g-oeetcr.i "new"}
{g-oee99.i}

define var ordType as char no-undo.


def buffer b2-icsw for icsw.
def var hx-recid as recid no-undo.
def        var v-noassn     like oeel.nosnlots       no-undo.
def new shared var s-bofl   as logical no-undo              .
def new  shared var g-screenpos   as integer   init 1                  no-undo. 
def             var h-origqty     like oeel.nosnlots                    no-undo.
def             var h-proofqty    like oeel.nosnlots                    no-undo.
def             var h-ordqty      like oeel.nosnlots                    no-undo.
def             var h-firsttime   as logical  init true                 no-undo.
def var h-recid as recid no-undo.
def var v-kitconv     like oeel.unitconv                      no-undo.
def var nothing as char no-undo. /* for output I don't want */
def var updBinFlag as logical no-undo.
def var v-grayqtyfl   as logical no-undo init false.

/* report includes */
{g-rpt.i}        
{g-rptctl.i new} 
def var v-reload            as logical no-undo.
def var v-reloadID          as recid   no-undo.

def var v-reportid          like sassr.reportid   no-undo. 
def var v-openprfl          like sassr.openprfl  no-undo.   
def var v-openprno          like sassr.openprno   no-undo. 
def var v-scrollmode        as logical     no-undo.
def var v-completescroll    as logical     no-undo.
def var v-position          as integer no-undo.
def var v-offset            as integer no-undo.
def var v-move              as integer no-undo.
def var v-reposition        as logical no-undo.

def var v-ordernum          as char format "x(12)" no-undo.
def var v-ordersuf          as int no-undo.
def var v-singlefl          as logical             no-undo init false.
def var v-horderno          like oeeh.orderno      no-undo.
def var v-hordersuf         like oeeh.ordersuf     no-undo.


def var dotSpot             as int no-undo.

def var v-prompt       as c                  no-undo.   
def var prompt-sw      as l                  no-undo.   
def var prodLoop       as logical initial yes no-undo.
def var binLoop        as logical initial yes no-undo.
def var advance        as logical initial yes no-undo.

def var o-frameval     as c                  no-undo.
def var v-emptyl       as l                  no-undo.
def var zx-okfl        as logical            no-undo.
def var zx-error       as integer            no-undo.
def var zx-suff        like poeh.posuf       no-undo.
def var v-keys         as c                  no-undo.
def var v-key          as c                  no-undo.
def var v-clearfl      as log                no-undo.
def var z-lastkey      as i                  no-undo.
def var z-loadcharfl   as log                no-undo.
def var v-start        as char format "x(24)"  no-undo.
def var v-editorfl     as logical            no-undo.
def var v-searchfl     as logical            no-undo.
def var v-srchfnd      as logical            no-undo.
def var x-spce         as c    format "x"    no-undo. 

define var v-titlelit   as char format "x(26)"          no-undo.

define var h as handle.
define var p-rowid as rowid.             
def var cRcptId as dec no-undo.
def var iter as int no-undo.

def var rcpt as char format "x(15)" extent 3 no-undo.

def var v-errorno    as i                 no-undo.
def var v-openinit   like poeh.openinit   no-undo.
/* While not currently used for Pick, it could be
   used in a large warehouse to divide the picking better */
def var v-bintypeB   as char format "x(6)" no-undo. 
def var v-bintypeE   as char format "x(6)" no-undo. 

def var v-selecttype as char format "x(1)" no-undo. /* kits? */
def var v-picktype as char format "x(1)" no-undo.
def var v-orderno  as int  format ">>>>>>>>>>>9" no-undo.
def var v-vasection as int format ">9" no-undo.

def var v-putawayinit like poeh.openinit      no-undo.  
/* def var v-receiptid   like zsdiput.receiptid  no-undo. */
/* def var v-whse        like zsdiput.whse       no-undo. */

def var v-shipprod    like poel.shipprod      no-undo.  
def var v-binloc      as c format "xx/xx/xxx/xxx"  no-undo.  
def var h-shipprod    like poel.shipprod      no-undo.  
def var h-binloc      as c format "xx/xx/xxx/xxx"  no-undo.
def var h-qtypicked   as decimal                   no-undo.  
def var h-CompleteThis as logical                  no-undo.
def var h-message     as c format "x(26)" no-undo.  

def var v-actioncode  as c no-undo.
def var v-extype      as c format "x(2)" no-undo.
def var v-editnochhoose as logical no-undo init no.
def var loaded-sw    as logical              no-undo. 
def var zx-l         as logical              no-undo. 
def var zx-x         as integer              no-undo. 

def var srch-flg  as logical              no-undo. 
def var v-notsrch as logical              no-undo. 
def var v-okfl as logical                 no-undo. 

/* def buffer b-zsdiput for zsdiput. */

def var  t-shipprod       like zsdiput.shipprod no-undo.
def var  t-proddesc1      like zsdiput.proddesc1 no-undo.
def var  t-proddesc2      like zsdiput.proddesc2 no-undo.
def var  t-stkqtyputaway  /*as char format "x(8)" */ as dec format ">>>>9.99" 
    no-undo.
def var  t-binloc1        like zsdiput.binloc1 no-undo.
def var  t-binloc2        like zsdiput.binloc2 no-undo.

def var v-qtypicked       like zsdipckdtl.realqtypicked no-undo.
def var t-orderno         as char format "x(12)" no-undo.
def var t-lineno          like zsdipckdtl.lineno     no-undo.
def var t-batchno         like zsdipckdtl.pickid no-undo.
def var l-batchno         as character format "x(10)"  no-undo.

def var t-seqno           like zsdipckdtl.pickseq no-undo.
def var t-type            like zsdipckdtl.recordtype no-undo.

def var lockPickType as logical initial no no-undo.
def var lockedPick   as char initial "" no-undo.

def var  w-binloc1        like zsdiput.binloc1 no-undo.
def var  w-binloc2        like zsdiput.binloc2 no-undo.
def var  h-binsort        as character         no-undo.

def var  w-errorlit       as char format "x(24)" no-undo.
def var  v-listlit        as char format "x(10)" no-undo.
def var inSearch          as char format "x(24)" no-undo.
define var idcnt as int initial 0 no-undo.
/* def var putno as int initial 0 no-undo. */
def var doneFlag as logical initial no no-undo.
/* used for F6 exception check for generateReport in .led and .lfu */

def var who as char extent 10 no-undo.
def var unixCmd as char no-undo.      
def var errFlag as logical initial no no-undo.

def var exceptCode as char no-undo.
/* define new shared var v-sapbid as  recid     no-undo. */

/* The list of receipt ids we are looking at */
define temp-table tmp-rcpt no-undo
  field receiptid like zsdipckdtl.pickid
  field receiptsuf like oeeh.ordersuf
  field section   like vaes.seqno
  field ordertype as character
  index k-rcpt
    receiptid receiptsuf section.

/* The product/qty list of current items
   This way can go from summary to detail */
/* don't NEED for pick, but could be used in batch mode */
/**
define temp-table  no-undo                     
  field prod like zsdiput.shipprod     
  field qty  like zsdiput.stkqtyputaway
  field binloc  like zsdiput.binloc1
  field binloc1 like zsdiput.binloc1
  field binloc2 like zsdiput.binloc2
  field zone   like zsdiput.locationid
  field completefl as logical 
  field split      as logical 
  field extype   like zsdiput.extype
/*  field putno as int */
  field binsort as char
  index k-tsum
    prod
  index k-bin
    binloc1
  index k-tbin
    binsort
    zone
    binloc1 
    prod.
**/

define temp-table t-binlist no-undo
  field prod    like icsw.prod
  field whse    like icsw.whse
  field binloc  like icsw.binloc1
  field binnum  as integer
  field binused as logical
index ix-prod
      prod
      whse
      binloc
      binnum
index ix-binnum
      prod
      whse
      binnum
      binloc.
  
form 
  rcpt[1]
  rcpt[2]
  rcpt[3]
with frame f-rcpt row 8 width 26 no-hide no-labels.

define buffer b-trcpt for tmp-rcpt.
define query q-trcpt for b-trcpt scrolling. 
define browse b-idlist query q-trcpt
  display b-trcpt.receiptid
  with size-chars 24 by 8 down                  
centered overlay no-hide no-labels.             
form
  b-idlist     at 1
with frame f-idlist row 3 no-hide overlay title "Receipt Ids" no-labels.

/* an updateable copy of zsdiput */
/* for pick this should be the main table and almost an exact copy
   of zsdipckdtl */
define temp-table t-put no-undo
  field whse like zsdiput.whse
  field receiptid like zsdiput.receiptid
  field orderno   like zsdipckdtl.orderno
  field suf        like zsdipckdtl.ordersuf
  field lineno    like zsdipckdtl.lineno
  field seqno     like oeelk.seqno
  field ordertype like zsdipckdtl.ordertype
  field prod like zsdiput.shipprod
  field bin  like zsdiput.putawaybin
  field binloc1 like zsdiput.binloc1
  field binloc2 like zsdiput.binloc2
  field binsort as char
  field qtytopick  like zsdiput.stkqtyputaway
  field qtypicked  like zsdipckdtl.realqtypicked
  field totqtyprod like zsdipckdtl.realqtypicked
  field totcntprod like zsdipckdtl.realqtypicked
  field oper like zsdiput.operinit
  field extype   like zsdiput.extype
  field completefl like zsdiput.completefl
  field transdt  like zsdiput.transdt
  field transtm  like zsdiput.transtm
  field zone     like zsdiput.locationid
  field split    as logical
  field secureqty as logical
  field idno     as int
  field proddesc1 as char
  field proddesc2 as char
  field recordtype as char
  field batchno   like zsdipckdtl.pickid
  field batchseq  like zsdipckdtl.pickseq
  field desc1     as char format "x(24)"
  field desc2     as char format "x(24)"
  field vendprod  like icsw.vendprod
  field vendno    like icsw.arpvendno
  field stkqtyship like zsdipckdtl.stkqtyship
/*  field putno    as int */
  field zput     as recid
/*  index k-tmpput
    whse zone bin prod idno
  index k-idno
    idno
  */
  index k-binprod
    binloc1 prod   
  index k-prod
    prod
  index k-bin
    zone
    binsort
    binloc1 
    batchseq 
    prod
    orderno
  index k-binx
    completefl
    split
    binsort
    binloc1 
    batchseq 
    prod
    orderno
 
  index k-ord
    orderno
    suf.

define buffer f-t-put for t-put.
define var v-lastpopprod as character no-undo.

define buffer b-tprod for t-put.
define buffer t2-put for t-put.
define query q-tprod for b-tprod scrolling.
define browse b-prodlist query q-tprod
display
   b-tprod.prod
   b-tprod.binloc1 format "x(24)"
with size-chars 26 by 7 down
centered overlay no-hide no-labels no-box . /* no-row-markers. */
/* def var inSearch as char format "x(24)" no-undo. */
def var thisLabel as char format "x(24)" no-undo.
form
  thisLabel at 1 format "x(24)" dcolor 2
  inSearch  at 1
  b-prodlist at 1                    
with frame f-prodlist no-box no-hide overlay no-labels.

form 
   " Status:    "        at 1  dcolor 2
   v-listlit             at 14
   "Binloc1:    "        at 1  dcolor 2
   t-binloc1       at 14
   "Binloc2:    "        at 1  dcolor 2
   t-binloc2       at 14
   with frame f-hhepb width 26 row 10 no-box 
   no-hide no-labels overlay.   

/* old
define buffer b-tprod for t-put.
define query q-tprod for b-tprod scrolling.
define browse b-prodlist query q-tprod
display
   b-tprod.prod
with size-chars 26 by 8 down
centered overlay no-hide no-labels no-box . /* no-row-markers. */
form
  "       Prod Search        " dcolor 2
  b-prodlist at 1                    
with frame f-prodlist no-box no-hide overlay no-labels.
*/

define buffer b-tbin for t-put.
define query q-tbin for b-tbin scrolling.
define browse b-binlist query q-tbin
display
  b-tbin.binloc1
with size-chars 24 by 10 down
centered overlay no-hide no-labels. /* no-box no-row-markers. */
form
  b-binlist at 1                    
with frame f-binlist row 1 no-hide overlay title "Bins" no-labels.

/* for xx-excustom &file */
define buffer b-t-put for t-put.

{{&appname} &temptable = "*" }
{{&appname} &user_vars = "*" }
{{&appname} &B-zz_browse_events = "*" }
{{&appname} &user_ctrlproc = "*"}

assign v-prompt = "R". 

main1: 
/* do while true on endkey undo main1, leave main1:       */
repeat:
  do transaction:
  pause 0 before-hide.        
  {{&appname} &user_display1 = "*" }
  {{&appname} &user_display3 = "*" }

  if {k-jump.i} then do: 
    clear frame f-hhet all no-pause.  
    run unlockZsdi.
    leave main1.  
  end.

  {w-sasoo.i g-operinits no-lock}  
  assign v-putawayinit = g-operinits
         v-whse        = g-whse.
  if v-whse = "" then v-whse = sasoo.whse.       
  /* This loops the whole frame until the flags are set
     and allows jump or cancel to quickly exit the program */
  do while not loaded-sw: 
    if v-prompt = "R" then do:
      view frame f-hhet-h.
      run RcptSelect.
      if loaded-sw then next main1.
    end.
    else
    if v-prompt = "o" then do:
      view frame f-hhet-h.
      run Options_Select.
      assign v-prompt = "R".
    end.

    if {k-jump.i} or {k-cancel.i} then do: 
      run unlockZsdi.
      clear frame f-hhet   all no-pause.  
      clear frame f-hhet-h all no-pause.  
      leave main1.  
    end.
    next main1.

  
  
  
  
  end.
  end. /*    transaction. */
  assign g-lkupfl  = yes.
  main:
  repeat       on endkey undo main, leave main
               on error undo main, leave main: 
    if {k-cancel.i} or {k-jump.i} then do:
      leave main.
    end.
    if not can-find(first t-put no-lock) then do:
      assign loaded-sw = false
             v-prompt = "R".
      next main1.
    end.  

    {{&appname} &user_Query = "*"}
    {{&appname} &user_display2 = "*" }
    leave main.
  end. /* main  */

  if {k-cancel.i} then do: 
    run unlockZsdi.
    clear frame f-hhet   all no-pause.  
    hide frame f-hhet.
    assign loaded-sw = false
           v-prompt = "R". 
   {{&appname} &user_display3 = "*" }
   end.

  if {k-jump.i} then do: 
    run unlockZsdi.
    clear frame f-hhet   all no-pause.  
    hide frame f-hhet.
  end.


end. /* do while main1 */


if {k-cancel.i} then do: 
  run unlockZsdi.
  clear frame f-hhet   all no-pause.  
  hide frame f-hhet.
  assign loaded-sw = false
         v-prompt = "R". 
 {{&appname} &user_display3 = "*" }
end.

if {k-jump.i} then do: 
  run unlockZsdi.
  clear frame f-hhet   all no-pause.  
  hide frame f-hhet.
end.



{{&appname} &user_aftmain1 = "*"}

procedure OptionsPage:
find first notes use-index k-notes
  where notes.cono         = g-cono and 
        notes.notestype    = "zz" and
        notes.primarykey   = "hher-" + g-operinits and
        notes.secondarykey = " "
        exclusive-lock no-error.
{w-sasoo.i g-operinits no-lock}

if {k-cancel.i} then 
  readkey pause 0.
hide frame f-opts.
release notes.
hide frame f-opts.
end procedure.

procedure RcptSelect:
{ {&appname} &RcptSelect = "*" }

run setTempRecords(input v-picktype, v-selecttype). 

{{&appname} &user_display2 = "*" }
end procedure. 

/*
Procedure RFLoop: 
assign zx-l = no
       zx-x = 1.
repeat while program-name(zx-x) <> ?:         
  if program-name(zx-x) = "hher.p" then do:                                    
    zx-l = yes.                              
    leave.                             
  end.                                      
  assign zx-x = zx-x + 1.                     
end.                                          
end procedure.
*/

{ {&appname} &user_procedures = "*" }

{p-hhet.i}
