/* pdec.lfo 1.1 01/03/98 */
/* pdec.lfo 1.1 07/27/96 */
/*h****************************************************************************
  INCLUDE      : pdec.lfo
  DESCRIPTION  : Price/Discount Entry - Change Prices and Costs - forms
  USED ONCE?   : yes
  AUTHOR       : mms
  DATE WRITTEN : 07/27/96
  CHANGES MADE :
    11/18/94 djp; TB# 14212 Allow list price to change by % of others & cleanup
    02/27/95 fef; TB# 17199 change error message for v-listtype from 5888
        to 6045.
    07/27/96 mms; TB# 11306 Added new; Pulled over changes in form from tbs
        14212 and 17199; these forms were originally defined in pdec.p
    12/15/03 emc; TB# e11337 Not pulling products in product # order
******************************************************************************/

/*d Main entry screen */
form
    s-whse                          colon 11
        validate({v-icsd.i s-whse},
        {valmsg.gme 4601})
        help "If Blank, All Warehouses Will Be Affected                   [L]"
    s-vendno                        colon 27
        validate({v-apsv.i s-vendno},
        {valmsg.gme 4301})
        help "Display Products for this Vendor/Whse Only, Zero for All    [L]"
    apsv.lookupnm                   at 45       no-label

    g-prod                          colon 11    {f-help.i}
        validate({v-icsp.i g-prod "/*"},
        {valmsg.gme 4600})
    icsp.notesfl                       at 38    no-label
    icsp.lookupnm                      at 45    no-label
    v-pricetxt                         at 40    no-label

    v-effectdt                      colon 11    {f-help.i}
    skip (1)

    "Change List/Base/Price Type"      at 4
    "Change Replacement/Standard Cost" at 42
    "---------------------------"      at 4
    "--------------------------------" at 42

    v-basetype                      colon 18
        help "N)ew $, D)$ Chg, P)% Chg, % of I)List, R)epl, S)tnd, or L)ast"
        validate(
        if v-basetype = "" then true else can-do("i,n,d,p,r,s,l",v-basetype),
        {valmsg.gme 5888})
    v-baseprice                        at 22 no-label
        help "Amount or Percent - Leave Blank for No Change"
    v-repltype                      colon 55
        help "N)ew $, D)$ Chg, P)% Chg, % of I)List, B)ase, S)tnd, or L)ast"
        validate(
        if v-repltype = "" then true else can-do("i,n,d,p,b,s,l",v-repltype),
        {valmsg.gme 5889})
    v-replcost                         at 59 no-label
        help "Amount or Percent - Leave Blank for No Change"
    v-listtype                      colon 18
        help "N)ew $, D)$ Chg, P)% Chg, % of S)tnd, L)ast, R)epl, or B)ase"
        validate(
        if v-listtype = "" then true else can-do("n,d,p,s,l,r,b",v-listtype),
        "Must be N, D, P, S, L, R, B, or Blank (6045)")
    v-listprice                        at 22 no-label
        help "Amount or Percent - Leave Blank for No Change"
    v-repldt                        colon 55 label "Last Change"
    v-basedt                        colon 18 label "Last Change"
    v-stndtype                      colon 55
        help "N)ew $, D)$ Chg, P)% Chg, % of I)List, R)epl, B)ase, or L)ast"
        validate(
        if v-stndtype = "" then true else can-do("i,n,d,p,r,b,l",v-stndtype),
        "Must be I, N, D, P, R, B, L, or Blank (5890)")
    v-stndcost                         at 59 no-label
        help "Amount or Percent - Leave Blank for No Change"
    v-pricetype                     colon 18
        help "Product Price Type - Leave Blank for No Change              [L]"
        validate({v-sasta.i "K" v-pricetype},
        "Product Price Type Not Set Up in System Table - SASTT (4006)")
    s-ptype                            at 24 no-label
    v-stnddt                        colon 55 label "Last Change"

    skip (1)

    v-pround                        colon 18
        validate(can-do("u,d,n,,",v-pround),
        "Must be U, D, or N (3658)")
        help "U)p, D)own, N)earest"

    v-ptarget
        validate(v-ptarget ne 0,"This is a Required Field (2100)")
        help "1=100 2=10 3=1 4=.1 5=.01 6=.001 7=.0001 8=.00001 9=User"

    v-pexactrnd
        help "Target for Type 9"
    skip (1)
    v-chgicsw        at 4    label "New ICSW Status  "
    v-leadtmavg      at 4    label "Lead Time Average"
    v-frozenlt       at 4    label "Frozen Lead Time "
    v-vendprod       at 4    label "Vendor Product   "
    v-famtype        at 4    label "ICSW Family Group"
    skip (2)

with frame f-pdsf side-labels overlay row 1 no-hide width 80 title g-title.

/*d F6 Current price, cost, and date overlay inquiry screen */
form

    icsw.baseprice              colon 12 label "Base Price"
    v-priceupddt                   at 31 no-label
    icsw.listprice              colon 12 label "List Price"
    icsw.priceupddt                at 31 no-label
    icsw.replcost               colon 12 label "Repl Cost"
    v-th[1]                        at 27 no-label
    icsw.replcostdt                at 31 no-label
    icsw.stndcost               colon 12 label "Stnd Cost"
    v-th[2]                        at 27 no-label
    icsw.stndcostdt                at 31 no-label
    icsw.lastcost               colon 12 label "Last Cost"
    v-th[3]                        at 27 no-label

with frame f-disp centered row 9 side-labels title "Current Price & Costs"
overlay.


