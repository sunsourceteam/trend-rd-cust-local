/* c-c12.i 1.1 01/03/98 */
/* c-c12.i 1.2 7/23/92 */
/*h***************************************************************************
  INCLUDE               : c-c12.i
  DESCRIPTION           : Used for assignment of report.c12 customer # -
                          depending upon if custno is numeric or alpha
  USED ONLY ONCE?       :
  AUTHOR                : pap
  DATE WRITTEN          : 07/13/92
  CHANGES MADE AND DATE :
                *******************************************************************************/
/* &custno  = customer number field
   This include concatenates as many spaces as needed to fill the 12-character
      field of customer number                                                   */


     string(fill(" ", 12 - length(string({&custno}))) + string({&custno}))



