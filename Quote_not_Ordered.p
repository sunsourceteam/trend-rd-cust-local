def var x-command  as c format "x(80)"  no-undo.
def var copyfile   as c format "x(40)"  no-undo.
def var filedt     as c format "x(8)"   no-undo.
def var filedd     as c format "x(2)"   no-undo.
def var filemm     as c format "x(2)"   no-undo.
def var fileyy     as c format "x(2)"   no-undo.
def var ic-purchdt like icet.postdt     no-undo.
def var ic-invdt   like icet.postdt     no-undo.
def var inact_fl   as logical.  
def var count      as i format ">>>,>>9" no-undo.

def buffer i-icsw  for icsw. 
def buffer f-oeehb for oeehb.
def buffer f-oeelb for oeelb.
def stream INACAUD.

assign filedt = string(today,"99/99/99").

assign filedd = substr(filedt,1,2)
       filemm = substr(filedt,4,2)
       fileyy = substr(filedt,7,2).


assign copyfile = "/usr/tmp/quoted_not_ordered." + string(filedd,"99") +
                                                    string(filemm,"99") + 
                                                    string(fileyy,"99").  


output stream INACAUD to "/usr/tmp/quoted_not_ordered.aud".
for each oeehb use-index k-oeehb where oeehb.cono = 1 and
                                       oeehb.sourcepros = "Quote" and
                                       oeehb.canceldt = TODAY - 183 and  
                                       oeehb.stagecd = 9   
                                       NO-LOCK:
  for each oeelb use-index k-oeelb where oeelb.cono    = oeehb.cono and
                                         oeelb.batchnm = oeehb.batchnm and
                                         oeelb.seqno   = oeehb.seqno and
                                         oeelb.specnstype = "l"   
                                         NO-LOCK:   
    find first oeel where oeel.cono     = oeelb.cono and
                          oeel.shipprod = oeelb.shipprod 
                          no-lock no-error.
    if not avail oeel then
      do:
      {icsp_icsw_inac.i}
    end.
                          
  
  end. /* each oeelb */
end.
output close.

assign x-command = "cp /usr/tmp/quoted_not_ordered.aud " + copyfile.
unix silent value (x-command).      


display count.





