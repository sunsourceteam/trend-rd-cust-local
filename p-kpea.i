/* SX 55 */
/* p-kpea.i 1.3 3/24/93 */
/*h*****************************************************************************
  INCLUDE      : p-kpea.i
  DESCRIPTION  : Process function keys for xxix routine in KPEA
  USED ONCE?   : yes
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    03/24/93 rs;  TB#  6232 Press GO goes to jump box
    06/16/95 gp;  TB# 18611 Make changes for KP Work Order Demand (G1)
    06/21/95 mtt; TB# 18611 Make changes for KP Work Order Demand (X1)
*******************************************************************************/

/*d Display available function keys */
put screen row 21 col 3 color message
  "  F6-Accept   F7-Cancel   F8-Print   F9-All   F10-Update   F12-Mecoh-Update".
/*
 " F6-Accept      F7-Cancel      F8-Print       F9-All         F10-Update     ".
*/

/*tb 18611 06/16/95 gp; Add suffix */
/*d Process the accept (F6) and cancel (F7) */
/*tb 18611 06/21/95 mtt; Make changes for KP Work Order Demand */
if {k-func6.i} or {k-func7.i} or {k-return.i} then do for b-kpet:
    /*d Find the work order */
    /*tb 18611 06/21/95 mtt; Make changes for KP Work Order Demand */
    find b-kpet where recid(b-kpet) = v-recid[frame-line(f-kpea)]
    no-lock no-error.

    /*tb 18611 06/16/95 gp; Add suffix */
    /*d Load the global variables */
    if avail b-kpet then
        assign
              g-wono   = b-kpet.wono
              g-wosuf  = b-kpet.wosuf
              g-prod   = b-kpet.shipprod
              g-whse   = b-kpet.whse
              v-qty    = b-kpet.stkqtyord
              v-errfl  = ""
              v-idkpet = recid(b-kpet).

    else if not avail b-kpet then do:
        run err.p(1106).
        next.
    end.
end.

/*d If return is pressed, maintain the record */
if {k-return.i} then do for b-oeeh, b-oeel:
    run kpear.p.
/*d Display message to manually update OE lines and WT lines tied to the
     work order */
               
    do for b-kpet:
                  
    /* Find KPET record */
      find b-kpet where recid(b-kpet) = v-recid[frame-line(f-kpea)]
        no-lock no-error.
        
      if avail b-kpet then
   /*d Determine if work order is tied to a WT line and an OE line */
        run tiefind.p(input (substr(g-ourproc,1,2) + b-kpet.ordertype),
                      input 0,
                      input 0,
                      input 0,
                      input b-kpet.orderaltno,
                      input b-kpet.orderaltsuf,
                      input b-kpet.linealtno,
                      output iOrderNo1,
                      output iOrderSuf1,
                      output iLineNo1,
                      output rOrderID1,
                      output iOrderNo2,
                      output iOrderSuf2,
                      output iLineNo2,
                      output rOrderID2).

    /*d Display manual update message if work order is tied to both a
                    WT line and OE line */
      if iOrderNo1 <> 0 and iOrderNo2 <> 0 then
        run err.p(6597).
                             
    end.            /* do for B-KPET */
                                            
    
    
    put screen row 21 col 3 color message
 "  F6-Accept   F7-Cancel   F8-Print   F9-All   F10-Update   F12-Mecoh-Update".

/*
 " F6-Accept      F7-Cancel      F8-Print       F9-All         F10-Update     ".
*/
    if frame-line(f-kpea) ne v-length then down 1 with frame f-kpea.
    if {k-jump.i} then leave main.
    next dsply.
end.

/* Process an F6-Accept */
if {k-func6.i} then do for b-oeeh, b-oeel:
    run kpeaa.p.
    if frame-line(f-kpea) ne v-length then down 1 with frame f-kpea.
    next dsply.
end.

/*d Process an F7-Cancel */
else if {k-func7.i} then do:
    run kpeae.p.
    if frame-line(f-kpea) ne v-length then down 1 with frame f-kpea.
    next dsply.
end.

/*d Process the F8-Print */
else if {k-func8.i} then do for b-kpet:
    find b-kpet where recid(b-kpet) = v-recid[frame-line(f-kpea)]
    no-lock no-error.
    assign
         g-wono   = if avail b-kpet then b-kpet.wono else g-wono
         g-wosuf  = if avail b-kpet then b-kpet.wosuf else g-wosuf.

    run kpetg.p("D").
    view frame f-kpeas.
    view frame f-kpea.
    put screen row 21 col 3 color message
 "  F6-Accept   F7-Cancel   F8-Print   F9-All   F10-Update   F12-Mecoh-Update".

/*
 " F6-Accept      F7-Cancel      F8-Print       F9-All         F10-Update     ".
*/
    if {k-jump.i} then leave main.
    if frame-line(f-kpea) ne v-length then down 1 with frame f-kpea.
    next dsply.
end.

/*d Perform F9-All Processing */
else if {k-func9.i} then do for b-oeeh, b-oeel:
    v-frameline = frame-line(f-kpea).
    run kpeac.p.
    if v-frameline ne 1 then up (v-frameline - 1) with frame f-kpea.
    /*e Repaint Screen with Proper Status */
    do i = 1 to v-length:
        if v-recid[i] ne 0 then do for b-kpet:
            find b-kpet where recid(b-kpet) = v-recid[i]
                no-lock no-error.
            if avail b-kpet then
                display b-kpet.statuscd @ s-statuscd
                with frame f-kpea.
        end.
        if frame-line(f-kpea) ne v-length then down 1 with frame f-kpea.
    end.
    if frame-line(f-kpea) ne 1 then up (frame-line(f-kpea) - 1)
        with frame f-kpea.
    next dsply.
end.

/*d process the F10-Update */

else if {k-func10.i} then do:
    status default.
    {p-kpeaut.i}
    return.
end.
else if {k-select.i} then
    next main.
