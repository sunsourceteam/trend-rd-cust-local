/*  p-fabstg.i    */

for each zsastz where
         zsastz.cono = g-cono and
         zsastz.codeiden = "FabStages" and
         zsastz.labelfl = false no-lock:
  
  create t-stages.
  assign         
    t-stages.stage        = int(zsastz.primarykey)
    t-stages.description  = zsastz.codeval[1]
    t-stages.nextstg      = int(zsastz.codeval[2])
    t-stages.upstg        = int(zsastz.codeval[3])
    t-stages.dwnstg       = int(zsastz.codeval[4])
    t-stages.shortname    = zsastz.codeval[5]
    t-stages.privupstg     = int(zsastz.codeval[6])
    t-stages.privdwnstg    = int(zsastz.codeval[7]).
end. 
