/* This is a copy of zsdioeiolb.p - Currency Pop-up */

define input parameter ix-cono     like oeeh.cono     no-undo.
define input parameter ix-type     as character       no-undo.
define input parameter ix-orderno  like oeeh.orderno  no-undo.
define input parameter ix-ordersuf like oeeh.ordersuf no-undo.
define input parameter ix-lineno   like oeel.lineno   no-undo.

define input parameter ix-prod     like oeel.shipprod   no-undo.
define input-output parameter ix-price    like oeel.price      no-undo.
define input parameter ix-discamt  like oeel.discamt    no-undo.
define input parameter ix-disctype like oeel.disctype   no-undo.
define input parameter ix-prodcost like oeel.prodcost   no-undo.
define input parameter ix-returnfl like oeel.returnfl   no-undo.
define input parameter ix-netamt   like oeel.netamt     no-undo.
define input parameter ix-datccost like oeel.netamt     no-undo.
define input parameter ix-qtyord   like oeel.netamt     no-undo.
define input parameter ix-qtyship  like oeel.netamt     no-undo.

{g-all.i}

def var s-addondesc as char format "x(14)" extent 6 no-undo.
def var s-addonamt as decimal format "z,zzz,zz9.99999-" extent 6 no-undo.
def var s-totals   as decimal format "z,zzz,zz9.99999-" extent 8 no-undo.
def var x-discty  as char format "x" no-undo.
def var x-prod    as char format "x(24)" no-undo.
def var x-cdesc   as char format "x(24)" no-undo.
def var x-qty     like oeel.qtyord       no-undo.
def var x-sur     like oeel.price        no-undo.

form
  "Exchange Rate:"  at 3
  s-totals[1]       at 21  format "zz,zzz9.999999-"
  skip(1)
  "Total Ordered:"  at 3
  s-totals[2]       at 19  format "z,zzz,zz9.99-"
  "Total Shipped:"  at 3
  s-totals[3]       at 19  format "z,zzz,zz9.99-"
  "Total Invoice:"  at 3
  s-totals[4]       at 19  format "z,zzz,zz9.99-"
  skip(1)
  "          Tax:"  at 3
  s-totals[5]       at 19  format "z,zzz,zz9.99-"
  s-addondesc[6]    at row 8 column 18 right-aligned  
  s-addonamt[6]     at 19  format "z,zzz,zz9.99-"
  s-addondesc[1]    at row 9 column 18 right-aligned  
  s-addonamt[1]     at 19  format "z,zzz,zz9.99-"
  s-addondesc[2]    at row 10 column 18 right-aligned  
  s-addonamt[2]     at 19  format "z,zzz,zz9.99-"
  s-addondesc[3]    at row 11 column 18 right-aligned  
  s-addonamt[3]     at 19  format "z,zzz,zz9.99-"
  s-addondesc[4]    at row 12 column 18 right-aligned  
  s-addonamt[4]     at 19  format "z,zzz,zz9.99-"
  s-addondesc[5]    at row 13 column 18 right-aligned  
  s-addonamt[5]     at 19  format "z,zzz,zz9.99-"
  "     Currency:"  at 3
  x-cdesc           at 19  
  
   with frame f-pounds overlay centered row 5 no-labels
  title " Currency Conversion ".


form
  "Exchange Rate:"  at 3
  s-totals[1]       at 21  format "zz,zzz9.999999-"
  "         Cost:"  at 3
  s-totals[2]       at 19
  "        Price:"  at 3
  s-totals[3]       at 19  
  "   Sur Charge:"  at 3
  s-totals[6]        at 19 
  "     Discount:"  at 3 
  s-totals[4]       at 19 space (0) x-discty
  skip(1)
  "   Net Amount:"  at 3
  s-totals[5]       at 19 format "z,zzz,zz9.99-"
  "Net Sur Charge:"  at 3
  s-totals[7]        at 19 format "z,zzz,zz9.99-"

  "Tot Net Amount:"  at 3
  s-totals[8]        at 19 format "z,zzz,zz9.99-"
  skip(1)  
  
  "         Line:"  at 3
  ix-lineno         at 19
  "      Product:"  at 3
  x-prod            at 19               
  "     Currency:"  at 3
  x-cdesc           at 19  
  
   with frame f-poundsl overlay centered row 6 no-labels
  title " Currency Conversion ".
                 
 if ix-type = "h" then
   run header_popup.
 else                   
 if ix-type = "l" then
   run line_popup.
 else
 if ix-type = "u" then
   run line_popup_u.
   
procedure header_popup:
 /**********/
 find oeehb where oeehb.cono        = ix-cono and
                   oeehb.batchnm    = string(ix-orderno,">>>>>>>9") and
                   oeehb.seqno      = 1 and
                   oeehb.sourcepros = "Quote"
                   no-lock no-error.
 find first sastc where sastc.cono = ix-cono and
                        sastc.currencyty = oeehb.countrycd no-lock no-error.
 
 if avail sastc then
   do:
   assign x-cdesc = sastc.descrip.
   if oeehb.totdatccost <> 0 then
       assign s-addondesc[6] = "Sur Charge Amt:"
              s-addonamt[6]  = round(oeeh.totdatccost ,2).
   
   assign s-totals[1] = if oeehb.user6 <> 0 then
                           oeehb.user6
                        else
                           round(1 / sastc.purchexrate,5)
          s-totals[2] = round(oeehb.totlineamt * s-totals[1]
                         * (if oeehb.transtype = "rm" then
                              -1
                            else
                               1),2)
          s-totals[3] = round(oeehb.totlineamt * s-totals[1]
                         * (if oeehb.transtype = "rm" then
                              -1
                            else
                              1),2)
          s-totals[4] = round(oeehb.totinvamt * s-totals[1]
                          * (if oeehb.transtype = "rm" then
                              -1
                            else
                              1),2)

          s-totals[5] = round((oeehb.taxamt[1] +
                         oeehb.taxamt[2] +
                         oeehb.taxamt[3] +
                         oeehb.taxamt[4]) * s-totals[1]
                        * (if oeehb.transtype = "rm" then
                              -1
                            else
                              1),2).  

   
   do i = 1 to 4:
     if oeehb.addonamt[i] <> 0 then 
       do:
       {w-sastn.i "a" oeehb.addonno[i] no-lock}
       if oeehb.langcd <> "" then 
         do:
         {w-sals.i oeehb.langcd "o" string(oeehb.addonno[i]) no-lock}
         end.
       assign s-addonamt[i] = round(oeehb.addonamt[i] * s-totals[1]
                          * (if oeehb.transtype = "rm" then
                              -1
                            else
                              1),2)

       
       s-addondesc[i] = if oeehb.langcd <> "" and avail sals then
                           right-trim(sals.descrip[1]) + ":"
                        else if avail sastn then 
                           right-trim(sastn.descrip) + ":"
                        else 
                           right-trim(string(oeehb.addonno[i])) + ":".
       end.
   end.
   assign s-addonamt[5] = round((s-addonamt[5] * s-totals[1]),2) 
          s-addonamt[6] = round((s-addonamt[6] * s-totals[1]),2). 

  end.

  display  
    s-totals[1]
    s-totals[2]
    s-totals[3]
    s-totals[4]
    s-totals[5]
    s-addondesc[6]
    s-addonamt[6]   when s-addonamt[6] <> 0
    s-addondesc[1]
    s-addonamt[1]  when s-addonamt[1] <> 0
    s-addondesc[2]
    s-addonamt[2]  when s-addonamt[2] <> 0
    s-addondesc[3]
    s-addonamt[3]   when s-addonamt[3] <> 0
    s-addondesc[4]
    s-addonamt[4]   when s-addonamt[4] <> 0
    s-addondesc[5]
    s-addonamt[5]   when s-addonamt[5] <> 0
    x-cdesc
    
   with frame f-pounds.

status default "Hit any key to continue".

readkey.
if {k-cancel.i} then
  do:
  readkey pause 0.
  end.
 

hide frame f-pounds.
/*********/
end.  

procedure line_popup:
 find oeehb where oeehb.cono        = ix-cono and
                   oeehb.batchnm    = string(ix-orderno,">>>>>>>9") and
                   oeehb.seqno      = 1 and
                   oeehb.sourcepros = "Quote"
                   no-lock no-error.
 find first oeelb where oeelb.cono    = ix-cono and
                        oeelb.batchnm = string(ix-orderno,">>>>>>>9") and
                        oeelb.seqno   = 1 and
                        oeelb.lineno  = ix-lineno 
                        no-lock no-error.
 if avail oeelb and g-currproc begins "oei" and (ix-prodcost = 0 or
                                                 ix-price = 0) then
  do:
  assign ix-price    = if ix-price    = 0   then oeelb.price
                                            else ix-price
         ix-discamt  = if ix-discamt  = 0   then dec(substr(oeelb.user4,34,6))
                                            else ix-discamt
         ix-disctype = no
         ix-prodcost = if ix-prodcost = 0   then oeelb.prodcost
                                            else ix-prodcost
         ix-returnfl = no
         ix-netamt   = if ix-netamt   = 0   then oeelb.netamt
                                            else ix-netamt
         ix-datccost = if ix-datccost = 0   then oeelb.datccost
                                            else ix-datccost
         ix-qtyord   = if ix-qtyord   = 0   then oeelb.qtyord
                                            else ix-qtyord
         ix-qtyship  = if ix-qtyship  = 0   then oeelb.qtyord
                                            else ix-qtyord.
 end.

 find first sastc where sastc.cono = ix-cono and
                        sastc.currencyty = oeehb.countrycd 
                        no-lock no-error.
 if avail sastc then
   do:
   assign x-cdesc = sastc.descrip
          x-prod = oeelb.shipprod.
   
   assign x-discty = if ix-disctype = false then
                       "%"
                     else
                       "$" .
   assign s-totals[1] = if oeehb.user6 <> 0 then
                           oeehb.user6
                        else
                           round(1 / sastc.purchexrate,5).
   assign s-totals[2] = ix-prodcost * s-totals[1]
                         * (if ix-returnfl then
                              -1
                            else
                               1).
   assign s-totals[3] = ix-price * s-totals[1]
                         * (if ix-returnfl then
                              -1
                            else
                              1).
   assign s-totals[4] = if x-discty = "%" then
                          ix-discamt
                        else
                          ix-discamt * s-totals[1].
   assign s-totals[5] = round(ix-netamt * s-totals[1]
                        * (if ix-returnfl then
                              -1
                            else
                              1),2).  
   
   /*
   if not(can-do("fo,qu,st,bl",oeeh.transtype)  or
          can-do("s,t",oeeh.orderdisp)          or
         (oeeh.stagecd    < 2                  and
          oeeh.boexistsfl = no                 and
          oeeh.totqtyshp  = 0                  and
          oeeh.totqtyret  = 0                  and
          oeel.qtyship    = 0)) then 
       assign x-qty = oeel.qtyship.  
   else
   */
       assign x-qty = ix-qtyord.
       
   
   assign x-sur = ix-datccost.
   if ix-datccost = 0 then
     assign x-sur = 0.
   else
   if x-sur > ix-datccost then    
     assign x-sur = ix-datccost.
   assign s-totals[6] =                         
                       ((x-sur ) 
                       * (if ix-returnfl = true then
                           -1
                         else
                            1)) * s-totals[1].
   
   assign s-totals[7] =                         
                       round(((x-sur * x-qty) 
                       * (if ix-returnfl = true then
                           -1
                         else
                            1)) * s-totals[1],2).
       
   end.
 assign s-totals[8]   = s-totals[7] + s-totals[5].



  display  
    x-discty
    s-totals[1]
    s-totals[2]
    s-totals[3]
    s-totals[4]
    s-totals[5]
    s-totals[6]
    s-totals[7]
    s-totals[8]

    
    ix-lineno
    x-prod
    x-cdesc 
   with frame f-poundsl.



status default "Hit any key to continue".

readkey.
if {k-cancel.i} then
  do:
  readkey pause 0.
  end.
 
hide frame f-poundsl.

end.  





procedure line_popup_u:
 find oeehb where oeehb.cono     = ix-cono and
                  oeehb.batchnm  = string(ix-orderno,">>>>>>>9") and
                  oeehb.seqno    = 1 and
                  oeehb.sourcepros = "Quote"
                  no-lock no-error.
 find first oeelb where oeelb.cono   = ix-cono and
                       oeelb.batchnm = string(ix-orderno,">>>>>>>9") and
                       oeelb.seqno   = 1 and
                       oeelb.lineno  = ix-lineno no-lock no-error.
 
 find first sastc where sastc.cono = ix-cono and
                        sastc.currencyty = oeehb.countrycd no-lock no-error.
 if avail sastc then
   do:
   assign x-cdesc = sastc.descrip
  
          x-prod   = ix-prod
          x-discty = if ix-disctype = false then
                       "%"
                     else
                       "$" 
          s-totals[1] = if oeehb.user6 <> 0 then
                           oeehb.user6
                        else
                           round(1 / sastc.purchexrate,5)
          s-totals[2] = ix-prodcost * s-totals[1]
                         * (if ix-returnfl then
                              -1
                            else
                               1)
          s-totals[3] = ix-price * s-totals[1]
                         * (if ix-returnfl then
                              -1
                            else
                              1)
          s-totals[4] = if x-discty = "%" then
                          ix-discamt
                        else
                          ix-discamt * s-totals[1]
          s-totals[5] = round(ix-netamt * s-totals[1]
                        * (if ix-returnfl then
                              -1
                            else
                              1),2).  

   /*
   if not(can-do("fo,qu,st,bl",oeeh.transtype)  or
          can-do("s,t",oeeh.orderdisp)          or
         (oeeh.stagecd    < 2                  and
          oeeh.boexistsfl = no                 and
          oeeh.totqtyshp  = 0                  and
          oeeh.totqtyret  = 0                  and
          ix-qtyship    = 0)) then 
       assign x-qty = ix-qtyship.  
   else
   */
       assign x-qty = ix-qtyord.
       
   
   assign x-sur = ix-datccost.
   if ix-datccost = 0 then
     assign x-sur = 0.
   else
   if x-sur > ix-datccost then    
     assign x-sur = ix-datccost.
   assign s-totals[6] =                         
                       ((x-sur ) 
                       * (if ix-returnfl = true then
                           -1
                         else
                            1)) * s-totals[1].
   
   assign s-totals[7] =                         
                       round(((x-sur * x-qty) 
                       * (if ix-returnfl = true then
                           -1
                         else
                            1)) * s-totals[1],2).
       
   end.
 assign s-totals[8]   = s-totals[7] + s-totals[5].


   

prcloop:
do while true :  
  display  
    x-discty
    s-totals[1]
    s-totals[2]
    s-totals[3]
    s-totals[4]
    s-totals[5]
    s-totals[6]
    s-totals[7]
    s-totals[8]
    ix-lineno
    x-prod
    x-cdesc 
  with frame f-poundsl.
update s-totals[3] with frame f-poundsl
  editing:
    readkey.
    if {k-cancel.i} then leave prcloop. 
    if {k-accept.i} or {k-return.i} then
      do:
      apply lastkey.
      leave.
      end.
    apply lastkey.
  end.    
if {k-accept.i} or {k-return.i} then
  do:
  assign ix-price = s-totals[3] / s-totals[1].
  leave prcloop.
  end.
end. 

hide frame f-poundsl.
end.  


