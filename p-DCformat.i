procedure FormatOutput:
  define input parameter        ip-locator as integer no-undo.
  define input parameter        ip-token   as char    no-undo.
  define input-output parameter ip-rec     as char    no-undo.
 
  Define Var iv-command         as char               no-undo.
  Define Var iv-fmtText         as char               no-undo.
   
 
  if ip-locator = 1 then do:
    find last t-formats where t-formats.locator = ip-locator no-lock no-error.
    if avail t-formats then
      assign ip-rec = fill(" ",t-formats.fldend).
  end.
  
  find t-formats where t-formats.locator = ip-locator no-lock no-error.

  if not avail t-formats then do:
    message "invalid location " + string (ip-locator).
    pause.
  end.

 assign iv-fmtText = {p-DCstring.i &exp1    = "t-formats.fldexpression1"
                                   &source = "ip-token"
                                   &format  = "t-formats.fldexpression2"}

 if error-status:error or 
    error-status:num-messages <> 0 then do:
   message " Rec #" v-reccount
           " Field " ip-locator
           " Len " t-formats.length
           " Begin " t-formats.fldbegin 
           t-formats.fldexpression1
           "|" + ip-token + "|"
           t-formats.fldexpression2
          t-formats.name
          error-status:get-message  (1).
   pause.       
 end.



 overlay (ip-rec,t-formats.fldbegin,t-formats.length) =
   iv-fmtText.

end.