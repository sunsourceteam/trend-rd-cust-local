/*EN-2839*/ 
 for each LEAD:
   delete LEAD.
 end.
 assign lt-leadtm     = 0
        lt-count      = 0   
        lt-module     = " "
        lt-frozendays = 0.    
 assign lt-module = if icsw.arptype = "V" then "po" else
                    if icsw.arptype = "W" then "wt" else
                    if icsw.arptype = "F" then "va" else
                    if icsw.arptype = "K" then "kp" else " ".
 assign lt-frozendays = icsw.frozenltty.
 if lt-module = " " then return.                   
 LEAD-LOOP: 
 for each lt-icet use-index k-postdt
                  where lt-icet.cono = g-cono      and
                        lt-icet.whse = icsw.whse   and
                        lt-icet.prod = icsw.prod   and
                        lt-icet.transtype = "re"   and   
                        lt-icet.module = lt-module 
                        no-lock:
   if lt-count = 5 then leave LEAD-LOOP.  
   if lt-icet.module = "po" then
     do:
     find lt-poeh where 
          lt-poeh.cono       = g-cono and
          lt-poeh.pono       = lt-icet.orderno and
          lt-poeh.posuf      = lt-icet.ordersuf and 
          lt-poeh.vendno     = icsw.arpvendno    
          no-lock no-error.
     if avail lt-poeh then
       do:
       find LEAD where LEAD.powtkpva-nbr = lt-poeh.pono  and
                       LEAD.sufx         = lt-poeh.posuf and
                       LEAD.receiptdt    = lt-poeh.receiptdt
                       no-error.
       if avail LEAD then next LEAD-LOOP.  
       create LEAD.
       assign lt-count = lt-count + 1.
       assign 
          LEAD.powtkpva-nbr = lt-poeh.pono 
          LEAD.sufx         = lt-poeh.posuf 
          LEAD.orderdt      = lt-poeh.orderdt   
          LEAD.expshipdt    = lt-poeh.expshipdt
          LEAD.receiptdt    = lt-poeh.receiptdt
          LEAD.leadtime     = 
                            if (lt-poeh.receiptdt - lt-poeh.orderdt) < 1 then 
                              1
                            else 
                              (lt-poeh.receiptdt - lt-poeh.orderdt)
          LEAD.includefl    = 
                            if lt-icet.postdt >= TODAY - 183 and 
                               lt-poeh.ignoreltfl = no and
                              (icsw.xxda4 = ? or
                               lt-icet.postdt > icsw.xxda4) then
                              "*" 
                            else 
                              "".
     end. /* avail lt-poeh */           
   end. /* module = "po" */
   if lt-icet.module = "wt" then
     do:
     find lt-wteh where 
          lt-wteh.cono       = g-cono and
          lt-wteh.wtno       = lt-icet.orderno and
          lt-wteh.wtsuf      = lt-icet.ordersuf and
          lt-wteh.shiptowhse = icsw.whse and
          lt-wteh.shipfmwhse = icsw.arpwhse
          no-lock no-error.
     if avail lt-wteh then
       do:     
       find LEAD where LEAD.powtkpva-nbr = lt-wteh.wtno  and
                       LEAD.sufx         = lt-wteh.wtsuf and
                       LEAD.receiptdt    = lt-wteh.receiptdt
                       no-error.
       if avail LEAD then next LEAD-LOOP.
       create LEAD. 
       assign lt-count = lt-count + 1.
       assign 
        LEAD.powtkpva-nbr = lt-wteh.wtno
        LEAD.sufx         = lt-wteh.wtsuf
        LEAD.orderdt      = lt-wteh.orderdt 
        LEAD.expshipdt    = lt-wteh.reqshipdt
        LEAD.receiptdt    = lt-wteh.receiptdt
        LEAD.leadtime     = if (lt-wteh.receiptdt - lt-wteh.orderdt) < 1 then 
                              1
                            else 
                              (lt-wteh.receiptdt - lt-wteh.orderdt)
        LEAD.includefl    = 
                          if lt-icet.postdt >= TODAY - 183 and
                             lt-wteh.ignoreltfl = no and 
                            (icsw.xxda4 = ? or
                             lt-icet.postdt > icsw.xxda4) then
                            "*" 
                          else 
                            "".
     end. /* avail lt-wteh */
   end. /* module = "wt" */     
   if lt-icet.module = "kp" then
     do:
     find lt-kpet where lt-kpet.cono     = g-cono and
                        lt-kpet.shipprod = lt-icet.prod and
                        lt-kpet.whse     = lt-icet.whse and 
                        lt-kpet.wono     = lt-icet.orderno and
                        lt-kpet.wosuf    = lt-icet.ordersuf
                        no-lock no-error.  
     if avail lt-kpet then
       do:  
       find LEAD where LEAD.powtkpva-nbr = lt-kpet.wono  and
                       LEAD.sufx           = lt-kpet.wosuf and
                       LEAD.receiptdt      = lt-icet.postdt
                       no-error.
       if avail LEAD then next LEAD-LOOP.   
       if lt-kpet.ordertype <> " " then
         do:
         if lt-kpet.ordertype = "o" then
           find oeeh where oeeh.cono = g-cono and
                           oeeh.orderno = lt-kpet.orderaltno and
                           oeeh.ordersuf = lt-kpet.orderaltsuf
                           no-lock no-error.
         if lt-kpet.ordertype = "t" then
           find wteh where wteh.cono = g-cono and
                           wteh.wtno = lt-kpet.orderaltno and
                           wteh.wtsuf = lt-kpet.orderaltsuf
                           no-lock no-error.
       end.
       create LEAD.   
       assign lt-count = lt-count + 1.
       assign 
          LEAD.powtkpva-nbr = lt-kpet.wono
          LEAD.sufx         = lt-kpet.wosuf
          LEAD.orderdt      = lt-kpet.enterdt   
          LEAD.expshipdt    = if avail oeeh then oeeh.promisedt
                              else if avail wteh then wteh.reqshipdt
                              else lt-kpet.enterdt + icsw.leadtmavg + 7
          LEAD.receiptdt    = lt-icet.postdt
          LEAD.leadtime     = 
                            if (lt-icet.postdt - lt-kpet.enterdt) < 1 then 
                              1
                            else 
                              (lt-icet.postdt - lt-kpet.enterdt)
          LEAD.includefl    = 
                            if lt-icet.postdt >= TODAY - 183 and
                              (icsw.xxda4 = ? or
                               lt-icet.postdt > icsw.xxda4) then 
                              "*" 
                            else 
                              "".
     end. /* avail kpet */
   end. /* module = "kp" */
   if lt-icet.module = "va" then
     do:
     find lt-vaeh where lt-vaeh.cono      = g-cono and
                        lt-vaeh.shipprod  = lt-icet.prod and
                        lt-vaeh.whse      = lt-icet.whse and
                        lt-vaeh.vano      = lt-icet.orderno and
                        lt-vaeh.vasuf     = lt-icet.ordersuf
                        no-lock no-error.
     if avail lt-vaeh then
       do:
       find LEAD where LEAD.powtkpva-nbr = lt-vaeh.vano  and
                       LEAD.sufx         = lt-vaeh.vasuf and
                       LEAD.receiptdt    = lt-icet.postdt
                       no-error.
       if avail LEAD then next LEAD-LOOP.
       create LEAD.   
       assign lt-count = lt-count + 1.
       assign 
          LEAD.powtkpva-nbr = lt-vaeh.vano
          LEAD.sufx         = lt-vaeh.vasuf
          LEAD.orderdt      = lt-vaeh.enterdt
          LEAD.expshipdt    = lt-vaeh.reqshipdt
          LEAD.receiptdt    = lt-icet.postdt
          LEAD.leadtime     = 
                            if (lt-icet.postdt - lt-vaeh.enterdt) < 1 then 
                              1
                            else 
                              (lt-icet.postdt - lt-vaeh.enterdt)
          LEAD.includefl    = 
                            if lt-icet.postdt >= TODAY - 183 and
                              (icsw.xxda4 = ? or
                               lt-icet.postdt > icsw.xxda4) then
                              "*" 
                            else 
                              "".
     end. /* avail lt-vaeh */
   end. /* module = "va" */
 end. /* each lt-icet */
 assign lt-count  = 0
        lt-leadtm = 0
        lt-LEADcount = 0.  
 for each LEAD no-lock break by LEAD.receiptdt descending:  
   assign lt-count = lt-count + 1.
   if LEAD.includefl <> "" then
     assign lt-leadtm = lt-leadtm + LEAD.leadtime
            lt-LEADcount = lt-LEADcount  + 1.
   if lt-count = 1 then
     assign s-orderdt-1   = LEAD.orderdt
            s-rcptdt-1    = LEAD.receiptdt
            s-expshpdt-1  = LEAD.expshipdt
            s-actLT-1     = LEAD.leadtime
            s-rcptfl-1    = LEAD.includefl. 
   if lt-count = 2 then
     assign s-orderdt-2   = LEAD.orderdt
            s-rcptdt-2    = LEAD.receiptdt
            s-expshpdt-2  = LEAD.expshipdt
            s-actLT-2     = LEAD.leadtime
            s-rcptfl-2    = LEAD.includefl. 
   if lt-count = 3 then
     assign s-orderdt-3   = LEAD.orderdt
            s-rcptdt-3    = LEAD.receiptdt
            s-expshpdt-3  = LEAD.expshipdt
            s-actLT-3     = LEAD.leadtime
            s-rcptfl-3    = LEAD.includefl. 
   if lt-count = 4 then
     assign s-orderdt-4   = LEAD.orderdt
            s-rcptdt-4    = LEAD.receiptdt
            s-expshpdt-4  = LEAD.expshipdt
            s-actLT-4     = LEAD.leadtime
            s-rcptfl-4    = LEAD.includefl. 
   if lt-count = 5 then
     assign s-orderdt-5   = LEAD.orderdt
            s-rcptdt-5    = LEAD.receiptdt
            s-expshpdt-5  = LEAD.expshipdt
            s-actLT-5     = LEAD.leadtime
            s-rcptfl-5    = LEAD.includefl. 

 end. /* for each LEAD */   
 if lt-LEADcount > 0 then
   assign lt-leadtm = lt-leadtm / lt-LEADcount.
 else
   assign lt-leadtm = 0.
 /*  
 if lt-leadtm = 0 then
   assign lt-leadtm = icsw.leadtmavg.   
 */
 /*EN-2839*/    
 
