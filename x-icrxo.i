/{&user_temptable}*/
/*   Variables from screen */
def new shared var b-buyer       like icsl.buyer                  no-undo.
def new shared var e-buyer       like icsl.buyer                  no-undo.
def new shared var b-buygroup    like oimsp.responsible           no-undo.
def new shared var e-buygroup    like oimsp.responsible           no-undo.
def new shared var b-whse        like icsd.whse                   no-undo.
def new shared var e-whse        like icsd.whse                   no-undo.
def new shared var b-vend        like apsv.vendno                 no-undo.
def new shared var e-vend        like apsv.vendno                 no-undo.
def new shared var b-pline       like icsw.prodline               no-undo.
def new shared var e-pline       like icsw.prodline               no-undo.
def new shared var b-pcat        like icsp.prodcat                no-undo.
def new shared var e-pcat        like icsp.prodcat                no-undo.
def new shared var b-vendorid    as c format "x(24)"              no-undo.
def new shared var e-vendorid    as c format "x(24)"              no-undo.
def new shared var b-vparent     as c format "x(24)"              no-undo.
def new shared var e-vparent     as c format "x(24)"              no-undo.
def new shared var b-prod        like icsp.prod                   no-undo.
def new shared var e-prod        like icsp.prod                   no-undo.
def new shared var b-class       like icsw.class                  no-undo.
def new shared var e-class       like icsw.class                  no-undo.
def new shared var b-stockoutdt  like zicswso.stockoutdt          no-undo.
def new shared var e-stockoutdt  like zicswso.stockoutdt          no-undo.
def new shared var b-daysstkout  like zicswso.daysstkout          no-undo.
def new shared var e-daysstkout  like zicswso.daysstkout          no-undo.

def var p-format      as i format "99"                 no-undo.
def var p-list        as logical                       no-undo.
def var p-stockouttp  as c format "x(1)"               no-undo.
def var p-nbrstkout   like zicswso.daysstkout          no-undo.
def var p-detailprt   as c format "x(1)"               no-undo.
def var p-outside     as logical                       no-undo.
/*def var p-export    as c format "x(24)"              no-undo.*/
def var p-totals      as logical                       no-undo.
def var p-regval      as c                             no-undo.

def var x-rangebeg    as c format "x(8)"               no-undo.
def var x-rangeend    as c format "x(8)"               no-undo.


{zsapboydef.i}
{zsapboycheck.i}

def var s-buyer       like icsl.buyer                  no-undo.
def var s-vname       as c format "x(19)"              no-undo.
def var s-vendno      like icsw.arpvendno              no-undo.
def var s-arp         like icsw.arptype                no-undo.
def var s-qtyonhand   as i format ">>>>9-"             no-undo.
def var s-qtyavail    as i format ">>>>9-"             no-undo.
def var s-qtyonord    as i format ">>>>9-"             no-undo.
def var s-qtybo       as i format ">>>>9-"             no-undo.
def var s-usage       as i format ">>>>9-"             no-undo.
def var s-stockoutfrq as i format ">>9"                no-undo.
def var s-orderno     like oeeh.orderno                no-undo.
def var s-ordersuf    like oeeh.ordersuf               no-undo.
def var s-lineno      like oeel.lineno                 no-undo.
def var s-transtype   like oeel.transtype              no-undo.
def var s-ordertype   like oeel.ordertype              no-undo.
def var idx           as   i format "99"               no-undo.
def var x-usage       like icswu.normusage[1]          no-undo.
def var x-totstkitems as   i format ">>>>>9"           no-undo.
def var x-totstkout   as   i format ">>>>9"            no-undo.
def var x-tot0onhand  as   i format ">>>>9"            no-undo.
def var x-tot0avail   as   i format ">>>>9"            no-undo.
def var x-tot0availonhand as i format ">>>>9"          no-undo.
def var s-stockoutpct as  de format ">>>9.99"          no-undo.
def var s-Zonhandpct  as  de format ">>>9.99"          no-undo.
def var s-Zavailpct   as  de format ">>>9.99"          no-undo.
def var s-Zavailonhandpct as de format ">>>9.99"       no-undo.
def var s-blank       as  c  format "x(177)" init " "  no-undo.
def var printdetail   as  logical            init no   no-undo.
def var ex-expecteddt as  c  format "x(8)"             no-undo.
def var ex-stockoutdt as  c  format "x(8)"             no-undo.
def var ex-lastrcptdt as  c  format "x(8)"             no-undo.
def var real-detail   as  logical                      no-undo.
def var v-vendorid    as  c  format "x(24)"            no-undo.
def var v-parentcode  as  c  format "x(24)"            no-undo.
def var v-technology  as  c  format "x(24)"            no-undo.
             

def new shared var g-whse        like icsw.whse                   no-undo.
def new shared var g-prod        like icsw.prod                   no-undo.

def stream dave.
output stream dave to "/usr/tmp/davestream".


define {&sharetype} temp-table stkout no-undo
  field whse        like zicswso.whse
  field prod        like zicswso.prod
  field statustype  like zicswso.statustype
  field stkouttype  like zicswso.stkouttype
  field inactivetp  like zicswso.inactivetp
  field stockoutdt  like zicswso.stockoutdt
  field expecteddt  like zicswso.expecteddt
  field lastrcptdt  like zicswso.lastrcptdt
  field daysstkout  as i format ">>>9"
  field stockoutfrq as i format ">>9"
  field qtyonhand   as i format ">>>>9-"
  field qtyavail    as i format ">>>>9-"
  field qtyonorder  as i format ">>>>9-"
  field qtybo       as i format ">>>>9-"
  field ordcalcty   like zicswso.ordcalcty
  field orderpt     as i format ">>>>9"
  field linept      as i format ">>>>9"
  field frozentype  like zicswso.frozentype
  field class       like zicswso.class
  field arptype     like zicswso.arptype
  field vendno      like zicswso.arpvendno
  field vname       as c format "x(19)"
  field buyer       like icsl.buyer
  field buygroup    like oimsp.responsible
  field prodline    like icsl.prodline
  field prodcat     like icsp.prodcat
  field vendorid    as c format "x(24)"
  field parentid    as c format "x(24)"
  field usage       as i format ">>>>9-"
  field totstkitems as i format ">>>>>9"   /* total number of stocked items */
  field totstkout   as i format ">>>>9"    /* total stock outs */
  field tot0onhand  as i format ">>>>9"    /* total Zero qty on hand */
  field tot0avail   as i format ">>>>9"    /* total Zero qty available */
  field tot0availonhand as i format ">>>>9" /* total 0 avail with qty onhand */
  field srtqtyonhand as c format "x(6)"
  
  index k-date
        whse
        prod
        stockoutdt
        stkouttype
  
  index k-prod
        whse
        prod
        statustype
        stkouttype
        
  index k-status
        statustype
        prod
        whse.
def buffer b-stkout for stkout.
def new shared temp-table dmddetail  no-undo
  FIELD prod           like icsw.prod
  FIELD whse           like icsw.whse
  FIELD ordno          like oeeh.orderno
  FIELD ordsuf         like oeeh.ordersuf
  FIELD tieord         as integer format ">>>>>>>"
  FIELD tiesuf         as integer format ">>" 
  FIELD tielineno      like oeel.lineno
  FIELD tietype        as character format "x"
  FIELD tieseq         like oeelk.seqno
  FIELD transtype      like oeel.transtype
  FIELD ordertype      as   character
  FIELD ordersign      as   character format "xxx"
  FIELD lineno         as   integer   format ">>>"
  FIELD seqno          as   integer   format ">>>"
  FIELD stagecd        as   integer   format "9"
  FIELD holdcd         as   character format "x"
  FIELD nonstockty     as   character format "x"
  FIELD zdesc          as   character format "x(15)"
  FIELD notestype      as   character format "x"
  FIELD comtype        as   character format "xx"
  FIELD entdt          as   date
  FIELD reqdt          as   date
  FIELD promdt         as   date
  FIELD ackncode       as   character format "x(1)"
  FIELD ackndate       as   date
  FIELD acknnbr        as   character format "x(35)"
  FIELD ackntrack      as   character format "x(35)"
  FIELD qtyord         as   decimal   format ">>>>>>>>>-"
  FIELD poqtyord       as   decimal   format ">>>>>>>>>-"
  FIELD qtyresv        as   decimal   format ">>>>>>>>>-"
  FIELD qtycommit      as   decimal   format ">>>>>>>>>-"
  FIELD poqtyrcvd      as   decimal   format ">>>>>>>>>-"
  FIELD blcode         as   integer
/* Just TBYL change */
  FIELD unitcost    as decimal format ">>>>>>9.9999-"
  FIELD unitprice   as decimal format ">>>>>>9.9999-"
  INDEX reqinx
        prod
        blcode
        reqdt
        ordno
        ordsuf
        lineno
        seqno
      
  INDEX whse
        prod
        blcode
        promdt
        ordno
        ordsuf
        lineno
        seqno.

  def buffer b-zicswso for zicswso.
/{&user_temptable}*/

/{&user_drpttable}*/
/* Use to add fields to the report table */
    FIELD rptrecid      as recid


/{&user_drpttable}*/



/{&user_forms}*/
def buffer xx-drpt for drpt.

form header
  "~015 " at 1
  with frame fx-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: ICRXO"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 168
  page-number(xpcd)    at 173 format ">>>9"
  "~015"               at 177
         "Daily StockOut Report" at 70
         "~015"              at 177
   with frame fx-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.

form header
  "~015 " at 1
  with frame f-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: ICRXO"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 168
  page-number          at 173 format ">>>9"
  "~015"              at 177
  "Daily StockOut Report" at 70
  "~015"              at 177
   with frame f-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.

form header 
   "~015"                    at 177
  with frame f-trn3 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.

form
  v-final                at 1
  v-astrik               at 2   format "x(5)"
  v-summary-lit2         at 7   format "x(27)"
  v-amt1                 at 34  format ">>>,>>>,>>9-"
  v-amt2                 at 47  format ">>>,>>>,>>9-"
  v-amt3                 at 60  format ">>>,>>>,>>9-"
  v-amt4                 at 73  format ">>>,>>>,>>9-" 
  v-amt5                 at 86  format ">>>,>>>,>>9-"
  v-amt6                 at 99  format ">>>,>>>,>>9-"
  "~015"                 at 178
with frame f-tot width 178
 no-box no-labels.  
 

form
  skip(2)
  "Totals"                              at   5
  skip(1)                                
  "Total Stock Items"                   at   1
  t-amounts[4]                          at  47
  "Total Stock Outs"                    at   1
  t-amounts[1]                          at  47
  skip(1)                                  
  "Stock Out Percentage"                at   1
  s-stockoutpct                         at  50
  skip(2)
  "Total Zero OnHand Stock Outs"        at   1
  t-amounts[2]                          at  47
  "Zero OnHand Percentage"              at   1
  s-Zonhandpct                          at  50
  skip(1)
  "Total Zero Avail Stock Outs"         at   1
  t-amounts[3]                          at  47
  "Zero Avail Percentage"               at   1
  s-Zavailpct                           at  50
  skip(1)
  "Total Zero Avail with Qty On Hand"   at   1
  t-amounts[5]                          at  47
  "Zero Avail w/Qty On Hand Pct"        at   1
  s-Zavailonhandpct                     at  50
  with frame f-totals width 178 no-box no-labels.
  


form 
  "~015"           at 1
with frame f-skip width 178 no-box no-labels.  

form
  "Buyer"         at   1
  "Whse"          at   7
  "Suplier"       at  17
  "Supplier"      at  34
  "Product"       at  53
  "Qty On"        at  70
  "Qty"           at  79
  "Qty On"        at  84
  "Qty"           at  92
  "Expectd"       at  98
  "Cur"           at 107
  "Ord"           at 112
  "Ord"           at 117
  "Line"          at 122
  "ARP"           at 127
  "Frz"           at 131
  "Inv"           at 136
  "6 Mo"          at 141
  "StockOut"      at 148
  "#Day"          at 157
  "SO Last"       at 162
  "Date of"       at 170

  "Name"          at  18 
  "Number"        at  35
  "Hand"          at  71
  "Avail"         at  78
  "Order"         at  84
  "BO/Dmd"        at  91
  "Ship Dt"       at  98
  "Stat"          at 107
  "Mtd"           at 112
  "Pnt"           at 117
  "Pnt"           at 123
  "Ctl"           at 131
  "Cls"           at 136
  "Usage"         at 141
  "Date"          at 150
  "Out"           at 158
  "12 Mo"         at 163
  "Last Rcp"      at 170
                                    
  "====="                     at   1
  "===="                      at   7
  "==================="       at  12
  "============"              at  32
  "========================"  at  45
  "======"                    at  70
  "======"                    at  77
  "======"                    at  84
  "======"                    at  91
  "========"                  at  98
  "===="                      at 107
  "==="                       at 112
  "===="                      at 116
  "===="                      at 122
  "==="                       at 127
  "==="                       at 131
  "==="                       at 136
  "====="                     at 141
  "========"                  at 148
  "===="                      at 157
  "======="                   at 162
  "========"                  at 170
with frame f-Hstockout width 178 no-box no-labels page-top.

form
  stkout.buyer                at   2
  stkout.whse                 at   7
  stkout.vname                at  12
  stkout.vendno               at  32
  stkout.prod                 at  45
  stkout.qtyonhand            at  70
  stkout.qtyavail             at  77
  stkout.qtyonord             at  84
  stkout.qtybo                at  91
  stkout.expecteddt           at  98
  stkout.statustype           at 108
  stkout.ordcalcty            at 113
  stkout.orderpt              at 116
  stkout.linept               at 122
  stkout.arptype              at 128
  stkout.frozentype           at 131
  stkout.class                at 137
  stkout.usage                at 141
  stkout.stockoutdt           at 148
  stkout.daysstkout           at 157
  stkout.stockoutfrq          at 164
  stkout.lastrcptdt           at 170
with frame f-Rstockout width 178 no-box no-labels.

form
  "Order:"                    at   7
  s-orderno                   at  14
  "-"                         at  21
  s-ordersuf                  at  22
  "Line:"                     at  26
  s-lineno                    at  32
  "Transtype:"                at  37
  s-transtype                 at  48
  "Ordertype:"                at  52
  s-ordertype                 at  63
with frame f-Dstockout width 178 no-box no-labels.

form 
  s-blank                     at    1
with frame f-blank width 178 no-box no-labels.
/{&user_forms}*/


/{&user_extractrun}*/

 run sapb_vars.
 run formatoptions.
 run extract_data.

/{&user_extractrun}*/

/{&user_exportstatDWheaders}*/
      /* no spaces in header names */
      assign export_rec = export_rec + v-del +
                        "Buyer"         + v-del +
                        "Whse"          + v-del +
                        "SupplierName"  + v-del +
                        "SupplierNbr"   + v-del +
                        "Product"       + v-del + 
                        "QtyOnHand"     + v-del +
                        "QtyAvail"      + v-del +
                        "QtyOnOrder"    + v-del +
                        "QtyBO"         + v-del +
                        "ExpShpDt"      + v-del +
                        "CurStat"       + v-del +
                        "OrdMtd"        + v-del +
                        "OrdPt"         + v-del +
                        "LnPt"          + v-del +
                        "ARP"           + v-del +
                        "Frz Ctl"       + v-del +
                        "Inv Clas"      + v-del +
                        "6 Mo Usg"      + v-del +
                        "StkOut Dt"     + v-del +
                        "#Days Out"     + v-del + 
                        "SO-Last 12 Mo" + v-del +
                        "Dt Last Rcpt"  + v-del +
                        "StockOut Cnt"  + v-del +
                        "0 Onhand Cnt"  + v-del +
                        "0 Avail Cnt"   + v-del +
                        " "             + v-del +
                        " ".


/{&user_exportstatDWheaders}*/

/{&user_exportstatheaders}*/
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  /*
  assign export_rec = export_rec + v-del +
                      "Period 1" + v-del +
                      "Period 2" + v-del   +
                      "Period 3" + v-del   +
                      "Period 4" + v-del   +
                      "Period 5" + v-del   +
                      "Period 6 " + v-del   +
                      " " + v-del +
                      " ".
  */
  if p-exportl and not p-exportDWl then
    assign export_rec = export_rec      + v-del +
                        "Qty OnHand"    + v-del + 
                        "Qty Avail"     + v-del +
                        "Qty OnOrder"   + v-del +
                        "Qty BO"        + v-del +
                        "Exp ShpDt"     + v-del +
                        "Cur Stat"      + v-del +
                        "Ord Mtd"       + v-del +
                        "Ord Pt"        + v-del +
                        "Ln Pt"         + v-del +
                        "ARP"           + v-del +
                        "Frz Ctl"       + v-del +
                        "Inv Clas"      + v-del +
                        "6 Mo Usg"      + v-del +
                        "StkOut Dt"     + v-del +
                        "#Days Out"     + v-del + 
                        "SO-Last 12 Mo" + v-del +
                        "Dt Last Rcpt"  + v-del +
                        "StockOut Cnt"  + v-del +
                        "0 Onhand Cnt"  + v-del +
                        "0 Avail Cnt"   + v-del +
                        " "             + v-del +
                        " ".

/{&user_exportstatheaders}*/


/{&user_foreach}*/
for each stkout no-lock:
/{&user_foreach}*/


/{&user_drptassign}*/
 /* If you add fields to the dprt table this is where you assign them */
 drpt.rptrecid = recid(stkout)
/{&user_drptassign}*/

/{&user_B4endloop}*/
  /* this is before the end of the for each loop to feed the data BUILDREC */
/{&user_B4endloop}*/





/{&user_viewheadframes}*/
/* Since frames are unique to each program you need to put your
   views and hides here with your names */

if not p-exportl then
  do:
  if not x-stream then
    do:
    view frame f-Hstockout.
  end.
  else
    do:
    hide stream xpcd frame f-trn1.
    hide stream xpcd frame fx-trn2.
    hide stream xpcd frame f-trn3.
    hide stream xpcd frame f-trn4.
    hide stream xpcd frame f-trn5.
  end.
end. /* not p-exportl */
else
  view frame f-Hstockout.
  


if not p-exportl  then
  do:
  if not x-stream then
    do:
    page.
    /*
    view frame f-trn1.
    view frame f-trn2.
    view frame f-trn3.
    */
    view frame f-Hstockout.
  end.
  else  
    do:
    page stream xpcd.
    view stream xpcd frame f-trn1.
    view stream xpcd frame fx-trn2.
    view stream xpcd frame f-trn3.
  end.
end. /* not p-exportl */

/{&user_viewheadframes}*/

/{&user_drptloop}*/
/{&user_drptloop}*/


/{&user_detailprint}*/
   /* Comment this out if you are not using detail 'X' option  */
   
   if p-totals = no then
     do:     
   
     if p-detail = "d" or (p-detail = "s" and p-export = " ") and
       entry(u-entitys,drpt.sortmystery,v-delim) <> 
       fill("~~",length(entry(u-entitys,drpt.sortmystery,v-delim))) then do: 
      
       if  p-exportl and not p-exportDWl then  do:
         v-inx2 = u-entitys. 
         run Detail_export.
       end.
       else if p-exportDWl then  do:
         v-inx2 = u-entitys. 
         run Detail_export.
       end.
       else do:
         v-inx2 = u-entitys. 
           /* next line would be your detail print procedure */
         run LinePrint (input drpt.rptrecid).
       

         if p-detail = "d" and real-detail = yes then
           do:
           v-inx2 = u-entitys. 
           find stkout where recid(stkout) = drpt.rptrecid no-lock no-error.
           if avail stkout then
             do:
             assign g-whse = stkout.whse
                    g-prod = stkout.prod.
             for each dmddetail:
               delete dmddetail.
             end.
             run icrxodetail.p.
             assign printdetail = no.
             for each dmddetail where dmddetail.whse = stkout.whse and
                                      dmddetail.prod = stkout.prod and
                                      dmddetail.ordersign = "OUT"
                                      no-lock:
               assign printdetail = yes.
               display dmddetail.ordno     @ s-orderno
                       dmddetail.ordsuf    @ s-ordersuf
                       dmddetail.lineno    @ s-lineno
                       dmddetail.transtype @ s-transtype
                       dmddetail.ordertype @ s-ordertype
               with frame f-Dstockout.
               down with frame f-Dstockout.
             end. /* each dmddetail */
             if printdetail = yes then
               do:
               display s-blank with frame f-blank.
               down with frame f-blank.
               assign printdetail = no.
             end. /* printdetail = yes */
           end. /* avail stkout */
         end. /* p-detail */
       end. /* else */
     end. /* if p-detail and other stuff */
   
   end. /* p-totals = no */
   
/{&user_detailprint}*/

/{&user_finalprint}*/

if not p-exportl then
  do:
  if not x-stream then 
    do:
    display 
     v-final
     v-astrik
     v-summary-lit2
   /* PTD */
     t-amounts1[u-entitys + 1]   @ v-amt1
     t-amounts3[u-entitys + 1]   @ v-amt2
    with frame f-tot.
  
    down with frame f-tot.
  end. /* not x-stream */
  else
    do:
    display stream xpcd
      v-final
      v-astrik
      v-summary-lit2
      t-amounts1[u-entitys + 1]   @ v-amt1
      t-amounts3[u-entitys + 1]   @ v-amt2
    with frame f-tot.
    down stream xpcd with frame f-tot.
  end.
end. /* not p-exportl */
else
  do:
  assign export_rec =  v-astrik.
  do v-inx4 = 1 to u-entitys:
    if v-totalup[v-inx4] = "n" then
       next.
    if v-inx4 = 1 then
      v-summary-lit2 = "Final".
    else
      v-summary-lit2 = "".

    if v-cells[v-inx4]= "2" then
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

      assign export_rec = export_rec + v-summary-lit2 + v-del + " ".
     
      end.
    else
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec +  v-summary-lit2.
      end.
  end.
  assign export_rec = export_rec + v-del +
      string(t-amounts1[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts2[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts3[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts4[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts5[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts6[u-entitys + 1],"->>>>>>>>9").
    
  assign export_rec = export_rec + chr(13).    
  put stream expt unformatted export_rec.
end.
/******
assign t-amounts[1] = 0
       t-amounts[2] = 0
       t-amounts[3] = 0.
for each b-stkout no-lock:                                  
  assign t-amounts[1] = t-amounts[1] + b-stkout.totstkout
         t-amounts[2] = t-amounts[2] + b-stkout.tot0onhand
         t-amounts[3] = t-amounts[3] + b-stkout.tot0avail
         t-amounts[5] = t-amounts[5] + b-stkout.tot0availonhand.
end.                                                        
******/
/**
if t-amounts[4] > 0 then
  assign s-stockoutpct     = (t-amounts[1] / t-amounts[4]) * 100
         s-Zonhandpct      = (t-amounts[2] / t-amounts[4]) * 100
         s-Zavailpct       = (t-amounts[3] / t-amounts[4]) * 100
         s-Zavailonhandpct = (t-amounts[5] / t-amounts[4]) * 100.
**/


if t-amounts[4] > 0 then
  assign s-stockoutpct     = (x-totstkout       / t-amounts[4]) * 100
         s-Zonhandpct      = (x-tot0onhand      / t-amounts[4]) * 100
         s-Zavailpct       = (x-tot0avail       / t-amounts[4]) * 100
         s-Zavailonhandpct = (x-tot0availonhand / t-amounts[4]) * 100.
         


/*****
display t-amounts[4]
        t-amounts[1]
        s-stockoutpct
        t-amounts[2]
        s-Zonhandpct
        t-amounts[3]
        s-Zavailpct
        t-amounts[5]
        s-Zavailonhandpct
with frame f-totals.
down with frame f-totals.
*****/
display t-amounts[4]
        x-totstkout       @ t-amounts[1]
        s-stockoutpct
        x-tot0onhand      @ t-amounts[2]
        s-Zonhandpct
        x-tot0avail       @ t-amounts[3]
        s-Zavailpct
        x-tot0availonhand @ t-amounts[5]
        s-Zavailonhandpct
with frame f-totals.
down with frame f-totals.
        

/{&user_finalprint}*/


/{&user_summaryframeprint}*/
 /***
    if not x-stream then
      do:
      display 
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        t-amounts3[v-inx2]   @ v-amt3
        t-amounts4[v-inx2]   @ v-amt4
        t-amounts5[v-inx2]   @ v-amt5
        t-amounts6[v-inx2]   @ v-amt6
        with frame f-tot.
      down with frame f-tot.
      end.
    else
      do:
      display stream xpcd
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        t-amounts3[v-inx2]   @ v-amt3
        t-amounts4[v-inx2]   @ v-amt4
        t-amounts5[v-inx2]   @ v-amt5
        t-amounts6[v-inx2]   @ v-amt6
        with frame f-tot.
      down stream xpcd with frame f-tot.
      end.
    ***/
/{&user_summaryframeprint}*/



/{&user_summaryputexport}*/

 
 assign export_rec = v-astrik + v-del + export_rec.
 
 if p-exportl and not p-exportDWl then 
   do:
   find stkout where recid(stkout) = drpt.rptrecid no-lock no-error.
   if stkout.expecteddt = ? then
     assign ex-expecteddt = "        ".
   else
     assign ex-expecteddt = string(stkout.expecteddt,"99/99/99").
   if stkout.stockoutdt = ? then
     assign ex-stockoutdt = "        ".
   else
     assign ex-stockoutdt = string(stkout.stockoutdt,"99/99/99").
   if stkout.lastrcptdt = ? then
     assign ex-lastrcptdt = "        ".
   else
     assign ex-lastrcptdt = string(stkout.lastrcptdt,"99/99/99").
   assign export_rec = export_rec                     + v-del +
                 string(stkout.qtyonhand,">>>>9-")    + v-del +
                 string(stkout.qtyavail,">>>>9-")     + v-del +
                 string(stkout.qtyonord,">>>>9-")     + v-del +
                 string(stkout.qtybo,">>>>9-")        + v-del +
               /*string(stkout.expecteddt,"99/99/99") + v-del +*/
                        ex-expecteddt                 + v-del +
                        stkout.statustype             + v-del +
                        stkout.ordcalcty              + v-del +
                 string(stkout.orderpt,">>>>9")       + v-del +
                 string(stkout.linept,">>>>9")        + v-del +
                        stkout.arptype                + v-del +
                        stkout.frozentype             + v-del +
                 string(stkout.class,">9")            + v-del +
                 string(stkout.usage,">>>>9-")        + v-del +
               /*string(stkout.stockoutdt,"99/99/99") + v-del +*/
                        ex-stockoutdt                 + v-del +
                 string(stkout.daysstkout,">>>>9")    + v-del +
                 string(stkout.stockoutfrq,">>9")     + v-del +           
               /*string(stkout.lastrcptdt,"99/99/99") + v-del +*/
                        ex-lastrcptdt                 + v-del +
               /*string(stkout.totstkitems,">>>>>9")  + v-del +*/
                 string(stkout.totstkout,">>>>>9")    + v-del +
                 string(stkout.tot0onhand,">>>>9")    + v-del +
                 string(stkout.tot0avail,">>>>9")     + v-del +
                 string(stkout.tot0availonhand,">>>>9").
   
 end.
 
 if p-exportDWl then 
   do:
   find stkout where recid(stkout) = drpt.rptrecid no-lock no-error.
   if stkout.expecteddt = ? then
     assign ex-expecteddt = "        ".
   else
     assign ex-expecteddt = string(stkout.expecteddt,"99/99/99").
   if stkout.stockoutdt = ? then
     assign ex-stockoutdt = "        ".
   else
     assign ex-stockoutdt = string(stkout.stockoutdt,"99/99/99").
   if stkout.lastrcptdt = ? then
     assign ex-lastrcptdt = "        ".
   else
     assign ex-lastrcptdt = string(stkout.lastrcptdt,"99/99/99").
   assign export_rec = export_rec                    + v-del +
                          stkout.buyer                  + v-del +
                          stkout.whse                   + v-del +
                          stkout.vname                  + v-del +
                   string(stkout.vendno,">>>>>>>>>>>9") + v-del +
                          stkout.prod                   + v-del +
                   string(stkout.qtyonhand,">>>>9-")    + v-del +
                   string(stkout.qtyavail,">>>>9-")     + v-del +
                   string(stkout.qtyonord,">>>>9-")     + v-del +
                   string(stkout.qtybo,">>>>9-")        + v-del +
                 /*string(stkout.expecteddt,"99/99/99") + v-del +*/
                          ex-expecteddt                 + v-del +
                          stkout.statustype             + v-del +
                          stkout.ordcalcty              + v-del +
                   string(stkout.orderpt,">>>>9")        + v-del +
                   string(stkout.linept,">>>>9")         + v-del +
                          stkout.arptype                + v-del +
                          stkout.frozentype             + v-del +
                   string(stkout.class,">9")            + v-del +
                   string(stkout.usage,">>>>9-")        + v-del +
                 /*string(stkout.stockoutdt,"99/99/99") + v-del +*/
                          ex-stockoutdt                 + v-del +
                   string(stkout.daysstkout,">>>>9")    + v-del +
                   string(stkout.stockoutfrq,">>9")     + v-del +           
                 /*string(stkout.lastrcptdt,"99/99/99") + v-del +*/
                          ex-lastrcptdt                 + v-del +
                 /*string(stkout.totstkitems,">>>>>9")  + v-del +*/
                   string(stkout.totstkout,">>>>>9")    + v-del +
                   string(stkout.tot0onhand,">>>>9")    + v-del +
                   string(stkout.tot0avail,">>>>9")     + v-del +
                   string(stkout.tot0availonhand,">>>>9"). 
  end. 
  
/{&user_summaryputexport}*/

/{&user_detailputexport}*/
 
 find stkout where recid(stkout) = drpt.rptrecid no-lock no-error.
 assign export_rec = v-astrik + v-del + export_rec. 
 
 if stkout.expecteddt = ? then
   assign ex-expecteddt = "        ".
 else
   assign ex-expecteddt = string(stkout.expecteddt,"99/99/99").
 if stkout.stockoutdt = ? then
   assign ex-stockoutdt = "        ".
 else
   assign ex-stockoutdt = string(stkout.stockoutdt,"99/99/99").
 if stkout.lastrcptdt = ? then
   assign ex-lastrcptdt = "        ".
 else
   assign ex-lastrcptdt = string(stkout.lastrcptdt,"99/99/99").

 if p-exportl and not p-exportDWl then do:

   assign export_rec = export_rec                      + v-del +
                  string(stkout.qtyonhand,">>>>9-")    + v-del +
                  string(stkout.qtyavail,">>>>9-")     + v-del +
                  string(stkout.qtyonord,">>>>9-")     + v-del +
                  string(stkout.qtybo,">>>>9-")        + v-del +
                /*string(stkout.expecteddt,"99/99/99") + v-del +*/
                         ex-expecteddt                 + v-del +
                         stkout.statustype             + v-del +
                         stkout.ordcalcty              + v-del +
                  string(stkout.orderpt,">>>>9")        + v-del +
                  string(stkout.linept,">>>>9")         + v-del +
                         stkout.arptype                + v-del +
                         stkout.frozentype             + v-del +
                  string(stkout.class,">9")            + v-del +
                  string(stkout.usage,">>>>9-")        + v-del +
                /*string(stkout.stockoutdt,"99/99/99") + v-del +*/
                         ex-stockoutdt                 + v-del +
                  string(stkout.daysstkout,">>>>9")    + v-del +
                  string(stkout.stockoutfrq,">>9")     + v-del +           
                /*string(stkout.lastrcptdt,"99/99/99") + v-del +*/
                         ex-lastrcptdt                 + v-del +
                /*string(stkout.totstkitems,">>>>>9")  + v-del +*/
                  string(stkout.totstkout,">>>>>9")    + v-del +
                  string(stkout.tot0onhand,">>>>9")    + v-del +
                  string(stkout.tot0avail,">>>>9")     + v-del +
                  string(stkout.tot0availonhand,">>>>9"). 
    
 end.
 else if  p-exportDWl then do:

   assign export_rec = export_rec                    + v-del +
                           stkout.buyer                  + v-del +
                           stkout.whse                   + v-del +
                           stkout.vname                  + v-del +
                    string(stkout.vendno,">>>>>>>>>>>9") + v-del +
                           stkout.prod                   + v-del +
                    string(stkout.qtyonhand,">>>>9-")    + v-del +
                    string(stkout.qtyavail,">>>>9-")     + v-del +
                    string(stkout.qtyonord,">>>>9-")     + v-del +
                    string(stkout.qtybo,">>>>9-")        + v-del +
                  /*string(stkout.expecteddt,"99/99/99") + v-del +*/
                           ex-expecteddt                 + v-del +
                           stkout.statustype             + v-del +
                           stkout.ordcalcty              + v-del +
                    string(stkout.orderpt,">>>>9")        + v-del +
                    string(stkout.linept,">>>>9")         + v-del +
                           stkout.arptype                + v-del +
                           stkout.frozentype             + v-del +
                    string(stkout.class,">9")            + v-del +
                    string(stkout.usage,">>>>9-")        + v-del +
                  /*string(stkout.stockoutdt,"99/99/99") + v-del +*/
                           ex-stockoutdt                 + v-del +
                    string(stkout.daysstkout,">>>>9")    + v-del +
                    string(stkout.stockoutfrq,">>9")     + v-del +           
                  /*string(stkout.lastrcptdt,"99/99/99") + v-del +*/
                           ex-lastrcptdt                 + v-del +
                  /*string(stkout.totstkitems,">>>>>9")  + v-del +*/
                    string(stkout.totstkout,">>>>>9")    + v-del +
                    string(stkout.tot0onhand,">>>>9")    + v-del +
                    string(stkout.tot0avail,">>>>9")     + v-del +
                    string(stkout.tot0availonhand,">>>>9"). 
 end.
 
/{&user_detailputexport}*/



/{&user_procedures}*/

procedure sapb_vars:

  assign b-buyer      = sapb.rangebeg[1]
         e-buyer      = sapb.rangeend[1]
         b-buygroup   = sapb.rangebeg[2]
         e-buygroup   = sapb.rangeend[2]
         b-whse       = sapb.rangebeg[3]
         e-whse       = sapb.rangeend[3]
         b-vend       = dec(sapb.rangebeg[4])
         e-vend       = dec(sapb.rangeend[4])
         b-pline      = sapb.rangebeg[5]
         e-pline      = sapb.rangeend[5]
         b-pcat    = sapb.rangebeg[6]
         e-pcat    = sapb.rangeend[6]
         b-vendorid   = sapb.rangebeg[7]
         e-vendorid   = sapb.rangeend[7]
         b-vparent    = sapb.rangebeg[8]
         e-vparent    = sapb.rangeend[8]
         b-prod       = sapb.rangebeg[9]
         e-prod       = sapb.rangeend[9]
         b-class      = int(substr(sapb.rangebeg[10],1,2))
         e-class      = int(substr(sapb.rangeend[10],1,2))
         /*
         b-stockoutdt = sapb.rangebeg[11]
         e-stockoutdt = sapb.rangeend[11]
         */
         b-daysstkout = int(substr(sapb.rangebeg[12],1,2))
         e-daysstkout = int(substr(sapb.rangeend[12],1,2)).
                     
  assign p-format     = int(sapb.optvalue[1])
         p-list       = if sapb.optvalue[2] = "yes" then yes else no
         p-stockouttp  = sapb.optvalue[3]
         p-nbrstkout  = int(sapb.optvalue[5])
         p-detailprt  = sapb.optvalue[6]
         p-outside    = if sapb.optvalue[7] = "yes" then yes else no
         p-export     = sapb.optvalue[8]
         p-totals     = if sapb.optvalue[9] = "yes" then yes else no.
         
  
  /* this code is to handle a timing issue */ 
  if substr(sapb.rangebeg[11],1,2) = "**" and 
     substr(sapb.rangeend[11],1,2) = "**" and
    string(TIME,"HH:MM") >= "00:00" and string(TIME,"HH:MM") <= "05:00" then
    assign x-rangebeg = string(TODAY - 1)
           x-rangeend = string(TODAY - 1).
  else
    assign x-rangebeg = sapb.rangebeg[11]
           x-rangeend = sapb.rangeend[11].
           
  if x-rangebeg ne "" then
    do:
    v-datein = x-rangebeg.
    {p-rptdt.i}
    if string(v-dateout) = v-lowdt then
       b-stockoutdt = 01/01/1950.
    else
       b-stockoutdt = v-dateout.
  end.
  else
    b-stockoutdt = 01/01/1950.
  if x-rangeend ne "" then
    do:
    v-datein = x-rangeend.
    {p-rptdt.i}
    if string(v-dateout) = v-lowdt then
       e-stockoutdt = 12/31/2049.
    else
       e-stockoutdt = v-dateout.
  end.
  else
    e-stockoutdt = 12/31/2049.
    
  if e-buyer = "" then
    assign b-buyer = "" 
           e-buyer = "~~~~".
  if e-buygroup = "" then
    assign b-buygroup = ""
           e-buygroup = "~~~~~~~~~~~~~~~~~~~~~~~~".
  if e-whse = "" then
    assign b-whse = ""
           e-whse = "~~~~".
  if e-vend = 0 then
    assign b-vend = 1
           e-vend = 999999999999.
  if e-pline = "" then
    assign b-pline = ""
           e-pline = "~~~~~~".
  if e-pcat = "" then
    assign b-pcat = ""
           e-pcat = "~~~~".
  if e-vendorid = "" then
    assign b-vendorid = ""
           e-vendorid = "~~~~~~~~~~~~~~~~~~~~~~~~".
  if e-vparent = "" then
    assign b-vparent = ""
           e-vparent = "~~~~~~~~~~~~~~~~~~~~~~~~".
  if e-prod = "" then
    assign b-prod = ""
           e-prod = "~~~~~~~~~~~~~~~~~~~~~~~~".
  if e-class = 0 then
    assign b-class = 0
           e-class = 99.
  if e-daysstkout = 0 then
    assign b-daysstkout = -999999
           e-daysstkout = 999999.
  
  if p-detailprt = "d" then
    assign p-detail = "d"
           real-detail = yes.
  else
    assign p-detail = "d"         /* das - 7/21/2008 */
           real-detail = no.
  
  assign zzl_begcat   = b-pcat
         zzl_endcat   = e-pcat
         zzl_Prodbeg  = b-prod
         zzl_Prodend  = e-prod
         zzl_begbuy   = b-buyer
         zzl_endbuy   = e-buyer
         zzl_begvend  = b-vend
         zzl_endvend  = e-vend
         zzl_begwhse  = b-whse
         zzl_endwhse  = e-whse
         zzl_begvname = b-vparent
         zzl_endvname = e-vparent
         zzl_begvpid  = b-vendorid
         zzl_endvpid  = e-vendorid
         zel_begcat   = b-pcat
         zel_endcat   = e-pcat
         zel_Prodbeg  = b-prod
         zel_Prodend  = e-prod
         zel_begbuy   = b-buyer
         zel_endbuy   = e-buyer
         zel_begvend  = b-vend
         zel_endvend  = e-vend
         zel_begwhse  = b-whse
         zel_endwhse  = e-whse
         zel_begvname = b-vparent
         zel_endvname = e-vparent
         zel_begvpid  = b-vendorid
         zel_endvpid  = e-vendorid.
  if p-list then
    do:
    {zsapboyload.i}
  end. /* if p-list */
  
  assign zelection_matrix [4] = (if zelection_matrix[4] > 1 then /* Buyer */
                                  zelection_matrix[4]
                                 else   
                                   1)
         zelection_matrix [7] = (if zelection_matrix[7] > 1 then  /* Vend */
                                  zelection_matrix[7]
                                 else   
                                  1)
         zelection_matrix [6] = (if zelection_matrix[6] > 1 then  /* Whse */
                                  zelection_matrix[6]
                                 else   
                                  1)
         zelection_matrix [3] = (if zelection_matrix[3] > 1 then  /* Cat  */
                                 zelection_matrix[3]
                              else   
                                 1)
         zelection_matrix [8] = (if zelection_matrix[8] > 1 then  /* VName */
                                 zelection_matrix[8]
                              else   
                                 1)
         zelection_matrix [9] = (if zelection_matrix[9] > 1 then  /* VPArent */
                                 zelection_matrix[9]
                              else   
                                 1)
         zelection_matrix [11] = (if zelection_matrix[11] > 1 then /* Product */
                                 zelection_matrix[11]
                              else   
                                 1).
    
  assign b-buyer      = zel_begbuy
         e-buyer      = zel_endbuy
         b-whse       = zel_begwhse
         e-whse       = zel_endwhse
         b-vend       = zel_begvend
         e-vend       = zel_endvend
         /*
         b-pline      =
         e-pline      =
         */
         b-pcat       = zel_begcat
         e-pcat       = zel_endcat
         b-vendorid   = zel_begvpid
         e-vendorid   = zel_endvpid
         b-vparent    = zel_begvname
         e-vparent    = zel_endvname
         b-prod       = zel_Prodbeg
         e-prod       = zel_Prodend.
                                              
end. /* sapb_vars procedure */


/* ------------------------------------------------------------------------- */
procedure formatoptions:
/* ------------------------------------------------------------------------- */

   
   
  assign p-portrait = false.
  /* if false then the report will print in LANDSCAPE 178 character */
  if sapb.optvalue[1] > "0" and sapb.optvalue[1] ne "99" then 
    do:
    p-optiontype = " ".
    
    if p-export <> " " then
       assign p-detail = "d"
              p-detailprt = "d".

    find notes where notes.cono = g-cono and
                     notes.notestype = "zz" and
                     notes.primarykey = "icrxo" and  /* tbxr program */
                     notes.secondarykey = string(p-format) no-lock no-error.
    if not avail notes then
      do:
      display "Format is not valid cannot process request".
      assign p-optiontype = "r"
             p-sorttype   = ">,"
             p-totaltype  = "l"
             p-summcounts = "a".
      return.
      end.               
   
    assign p-optiontype = notes.noteln[1]
           p-sorttype   = notes.noteln[2]
           p-totaltype  = notes.noteln[3]
           p-summcounts = notes.noteln[4]
         /* p-export     = "    " */
           p-register   = notes.noteln[5]
           p-registerex = notes.noteln[6].
    end.     
  else
  if sapb.optvalue[1] = "99" then 
    do:
    assign p-register   = ""
           p-registerex = "".
    run reportopts(input sapb.user5, 
                   input-output p-optiontype,
                   input-output p-sorttype,  
                   input-output p-totaltype,
                   input-output p-summcounts,
                   input-output p-register,
                   input-output p-regval).

    /* Product has to be chosen to print more detailed output */ 
    if p-export = " " then
      do:
      if index(p-optiontype,"p",1) = 0 then
        assign p-optiontype = p-optiontype + ",p"
               p-sorttype   = p-sorttype   + ",>"
               p-totaltype  = p-totaltype  + ",I"
               p-summcounts = p-summcounts + ",a".
    end.
    else
      do:
      if p-export <> " " then
        assign p-detail    = "d"
               p-detailprt = "d".
      
      if index(p-optiontype,"b",1) = 0 then
        assign p-optiontype = p-optiontype + ",b"
               p-sorttype   = p-sorttype   + ",>"
               p-totaltype  = p-totaltype  + ",i"
               p-summcounts = p-summcounts + ",a".
      if index(p-optiontype,"w",1) = 0 then
        assign p-optiontype = p-optiontype + ",w"
               p-sorttype   = p-sorttype   + ",>"
               p-totaltype  = p-totaltype  + ",i"
               p-summcounts = p-summcounts + ",a".
      if index(p-optiontype,"v",1) = 0 then
        assign p-optiontype = p-optiontype + ",v"
               p-sorttype   = p-sorttype   + ",>"
               p-totaltype  = p-totaltype  + ",i"
               p-summcounts = p-summcounts + ",a".
      if index(p-optiontype,"p",1) = 0 then
        assign p-optiontype = p-optiontype + ",p"
               p-sorttype   = p-sorttype   + ",>"
               p-totaltype  = p-totaltype  + ",i"
               p-summcounts = p-summcounts + ",a".
      /*
      end.
      */
    end.
  end.
 end.
   
/********************* 
procedure extract_data:
  assign x-totstkitems     = 0
         x-totstkout       = 0
         x-tot0onhand      = 0
         x-tot0avail       = 0
         x-tot0availonhand = 0.
  
  for each icsd where icsd.cono      = g-cono and
                      icsd.salesfl   = yes and
                      icsd.custno    = 0   and
                      icsd.whse     >= b-whse and
                      icsd.whse     <= e-whse 
                      no-lock:
    if icsd.whse = "acct" then next.
    if icsd.whse = "ap-1" then next.
    if icsd.whse = "cspo" then next.
    if icsd.whse = "cimt" then next.
    for each icsw where icsw.cono        = g-cono     and
                        icsw.statustype  = "S"   and
                        icsw.whse        = icsd.whse  and
                        icsw.prod        >= b-prod     and
                        icsw.prod        <= e-prod     and
                        icsw.class       >= b-class    and
                        icsw.class       <= e-class    and
                        icsw.arpvendno   >= b-vend   and
                        icsw.arpvendno   <= e-vend   and
                        icsw.prodline    >= b-pline and
                        icsw.prodline    <= e-pline
                        no-lock:
      find icsp where icsp.cono = g-cono and
                      icsp.prod = icsw.prod and
                      icsp.statustype = "A" and
                      icsp.kittype <> "B"
                      no-lock no-error.
      if not avail icsp then next.
      if icsp.prodcat < b-pcat and icsp.prodcat > e-pcat then next.
      find icsl where icsl.cono     = g-cono and
                      icsl.whse     = icsw.whse and
                      icsl.vendno   = icsw.arpvendno and
                      icsl.prodline = icsw.prodline and
                      icsl.buyer   >= b-buyer and
                      icsl.buyer   <= e-buyer
                      no-lock no-error.
      if not avail icsl then next.
      find oimsp where oimsp.person       = icsl.buyer and
                       oimsp.responsible >= b-buygroup and
                       oimsp.responsible <= e-buygroup
                       no-lock no-error.
      if not avail oimsp then next.
      find notes where notes.cono = 1 and 
                       notes.notestype = "zz" and
                       notes.primarykey  = "apsva" and
                       notes.secondarykey = icsp.prodcat 
                       no-lock no-error. 
      if avail notes then
        do:
        if notes.noteln[1] < b-vendorid or
           notes.noteln[1] > e-vendorid then
          next.
        if notes.noteln[3] < b-vparent or
           notes.noteln[3] > e-vparent then
          next.
      end. /* avail notes */
      
      assign x-totstkitems = x-totstkitems + 1.
    end. /* each icsw */

    for each zicswso use-index index1 where 
                           zicswso.whse        = icsd.whse and
                           zicswso.prod       >= b-prod    and
                           zicswso.prod       <= e-prod    and
                           zicswso.arpvendno  >= b-vend    and
                           zicswso.arpvendno  <= e-vend    and
                         /*zicswso.statustype  = "A"       and*/
                         ((p-outside = no and
                           zicswso.stockoutdt >= b-stockoutdt and
                           zicswso.stockoutdt <= e-stockoutdt) or
                          (p-outside = yes and zicswso.statustype = "A" and
                           zicswso.stockoutdt >= 01/01/1950 and
                           zicswso.stockoutdt <= 12/31/2049)) and
                           zicswso.class      >= b-class and
                           zicswso.class      <= e-class and
                           zicswso.daysstkout >= b-daysstkout and
                           zicswso.daysstkout <= e-daysstkout
                           no-lock:
      if p-stockouttp = "O" and zicswso.stkouttype <> "O" then next.
      if p-stockouttp = "A" and zicswso.stkouttype <> "A" then next.
      
      find icsw where icsw.cono = g-cono and
                      icsw.whse = zicswso.whse and
                      icsw.prod = zicswso.prod and
                      icsw.prodline >= b-pline and
                      icsw.prodline <= e-pline
                      no-lock no-error.
      if not avail icsw then next.
      
      find icsp where icsp.cono = g-cono and
                      icsp.prod = zicswso.prod and
                      icsp.statustype = "A" and
                      icsp.kittype <> "B" and
                      icsp.prodcat >= b-pcat and
                      icsp.prodcat <= e-pcat
                      no-lock no-error.
      if not avail icsp then next.
      
      find icsl where icsl.cono = g-cono and
                      icsl.whse = zicswso.whse and
                      icsl.vendno = icsw.arpvendno and
                      icsl.prodline = icsw.prodline and
                      icsl.buyer >= b-buyer and
                      icsl.buyer <= e-buyer
                      no-lock no-error.
      if not avail icsl then next.
      find oimsp where oimsp.person       = icsl.buyer and
                       oimsp.responsible >= b-buygroup and
                       oimsp.responsible <= e-buygroup
                       no-lock no-error.
      if not avail oimsp then next.
      find notes where notes.cono = 1 and 
                       notes.notestype = "zz" and
                       notes.primarykey  = "apsva" and
                       notes.secondarykey = icsp.prodcat 
                       no-lock no-error. 
      if avail notes then
        do:
        if notes.noteln[1] < b-vendorid or
           notes.noteln[1] > e-vendorid then
          next.
        if notes.noteln[3] < b-vparent or
           notes.noteln[3] > e-vparent then
          next.
      end.
      
      assign s-stockoutfrq = 0.
      if p-stockouttp = "B" then
        do:
        for each b-zicswso where 
                 b-zicswso.whse        = zicswso.whse and
                 b-zicswso.prod        = zicswso.prod and
                (b-zicswso.qtyavail    = 0 or
                 b-zicswso.qtyonhand   = 0) and
                 b-zicswso.stockoutdt >= TODAY - 365
                 no-lock:
          assign s-stockoutfrq = s-stockoutfrq + 1.
        end. /* each b-zicswso */
      end. /* p-stockouttp = "B" */
      else
        do:
        for each b-zicswso where 
                 b-zicswso.whse        = zicswso.whse and
                 b-zicswso.prod        = zicswso.prod and
                 b-zicswso.stockoutdt >= TODAY - 365  and
                 b-zicswso.stkouttype  = p-stockouttp 
                 no-lock:
          assign s-stockoutfrq = s-stockoutfrq + 1.
        end. /* each b-zicswso */
      end. /* p-stockouttp is specific */
      if p-nbrstkout > 0 and s-stockoutfrq < p-nbrstkout then next.
      
      /********
      if p-stockouttp = "B" then
        do:
        find stkout where stkout.whse = zicswso.whse and
                          stkout.prod = zicswso.prod and
                          stkout.qtyonhand = 0 and
                          stkout.qtyavail  = 0
                          no-lock no-error.
        if avail stkout then next.
      end.
      *********/
      
      if p-stockouttp = "B" then
        do:
        if zicswso.stkouttype = "O" then
          do:
          find stkout where stkout.whse = zicswso.whse and
                            stkout.prod = zicswso.prod and
                            stkout.stkouttype = "A"
                            no-lock no-error.
          if avail stkout then next.
          else
            do:
            /*** should this logic be removed? ***/
            find b-zicswso where b-zicswso.whse = zicswso.whse and
                                 b-zicswso.prod = zicswso.prod and
                                 b-zicswso.statustype = "A" and
                                 b-zicswso.stkouttype = "A" and
                                 b-zicswso.stockoutdt < zicswso.stockoutdt
                                 no-lock no-error.
            if avail b-zicswso then next.
          end.
        end. /* zicswso.stkouttype */
      end. /* p-stockouttp = "B" */
      
      assign x-usage = 0.
      find icswu where icswu.cono = g-cono and
                       icswu.whse = icsw.whse and
                       icswu.prod = icsw.prod
                       no-lock no-error.
      if avail icswu then
        do:
        do idx = 1 to 6:
          assign x-usage = x-usage + icswu.normusage[idx].
        end.
      end. /* avail icswu */
      
      find apsv where apsv.cono = g-cono and
                      apsv.vendno = icsw.arpvendno
                      no-lock no-error.

      create stkout.
      assign stkout.whse        = zicswso.whse
             stkout.prod        = zicswso.prod
             stkout.statustype  = icsw.statustype
             stkout.stkouttype  = zicswso.stkouttype
             stkout.inactivetp  = zicswso.inactivetp
             stkout.stockoutdt  = zicswso.stockoutdt
             stkout.expecteddt  = zicswso.expecteddt
             stkout.lastrcptdt  = zicswso.lastrcptdt
             stkout.daysstkout  = zicswso.daysstkout
             stkout.stockoutfrq = s-stockoutfrq
             stkout.qtyonhand   = int(zicswso.qtyonhand)
             stkout.qtyavail    = int(zicswso.qtyavail)
             stkout.qtyonorder  = int(zicswso.qtyonorder)
             stkout.qtybo       = int(zicswso.qtybo)
             stkout.ordcalcty   = zicswso.ordcalcty
             stkout.orderpt     = zicswso.orderpt
             stkout.linept      = zicswso.linept
             stkout.frozentype  = zicswso.frozentype
             stkout.class       = zicswso.class
             stkout.arptype     = zicswso.arptype
             stkout.vendno      = zicswso.arpvendno
             stkout.vname       = if avail apsv then substr(apsv.name,1,19)
                                  else " "
             stkout.buyer       = icsl.buyer
             stkout.buygroup    = oimsp.responsible
             stkout.prodline    = icsl.prodline
             stkout.prodcat     = icsp.prodcat
             stkout.vendorid    = if avail notes then notes.noteln[1]
                                  else " "
             stkout.parentid    = if avail notes then notes.noteln[3]
                                  else " "
             stkout.usage       = int(x-usage)
             stkout.totstkitems = x-totstkitems
             stkout.totstkout   = 1
             stkout.tot0onhand  = if stkout.qtyonhand    <= 0 then 1 else 0
             stkout.tot0avail   = if stkout.qtyavail     <= 0 then 1 else 0
             stkout.tot0availonhand = if stkout.qtyavail <= 0 and
                                         stkout.qtyonhand > 0 then 1 else 0.
      
      assign x-totstkout = x-totstkout + 1.
      if stkout.qtyonhand <= 0 then
        assign x-tot0onhand = x-tot0onhand + 1.
      if stkout.qtyavail  <= 0 then
        assign x-tot0avail  = x-tot0avail + 1.
      if stkout.qtyavail  <= 0 and stkout.qtyonhand > 0 then
        assign x-tot0availonhand = x-tot0availonhand + 1.
      assign stkout.srtqtyonhand = string(stkout.qtyonhand,">>>>9-").
    end. /* each zicswso */
  end. /* each icsd */

end. /* procedure extract_data */
********************************/

procedure extract_data:
  
  assign x-totstkitems     = 0
         x-totstkout       = 0
         x-tot0onhand      = 0
         x-tot0avail       = 0
         x-tot0availonhand = 0.
  
  for each icsd where icsd.cono      = g-cono and
                      icsd.salesfl   = yes and
                      icsd.custno    = 0   and
                      icsd.whse     >= b-whse and
                      icsd.whse     <= e-whse 
                      no-lock:
    if icsd.whse = "acct" then next.
    if icsd.whse = "ap-1" then next.
    if icsd.whse = "cspo" then next.
    if icsd.whse = "cimt" then next.
    
    for each icsw where icsw.cono        = g-cono     and
                        icsw.statustype  = "S"   and
                        icsw.whse        = icsd.whse  and
                        icsw.prod        >= b-prod     and
                        icsw.prod        <= e-prod     and
                        icsw.class       >= b-class    and
                        icsw.class       <= e-class    and
                        icsw.arpvendno   >= b-vend   and
                        icsw.arpvendno   <= e-vend   and
                        icsw.prodline    >= b-pline and
                        icsw.prodline    <= e-pline
                        no-lock:
      find icsp where icsp.cono = g-cono and
                      icsp.prod = icsw.prod and
                      icsp.statustype = "A" and
                      icsp.kittype <> "B"
                      no-lock no-error.
      if not avail icsp then next.
      if icsp.prodcat < b-pcat and icsp.prodcat > e-pcat then next.
      find icsl where icsl.cono     = g-cono and
                      icsl.whse     = icsw.whse and
                      icsl.vendno   = icsw.arpvendno and
                      icsl.prodline = icsw.prodline and
                      icsl.buyer   >= b-buyer and
                      icsl.buyer   <= e-buyer
                      no-lock no-error.
      if not avail icsl then next.
      find oimsp where oimsp.person       = icsl.buyer and
                       oimsp.responsible >= b-buygroup and
                       oimsp.responsible <= e-buygroup
                       no-lock no-error.
      if not avail oimsp then next.
      find notes where notes.cono = 1 and 
                       notes.notestype = "zz" and
                       notes.primarykey  = "apsva" and
                       notes.secondarykey = icsp.prodcat 
                       no-lock no-error. 
      
      if avail notes then
        do:
        if notes.noteln[1] < b-vendorid or
           notes.noteln[1] > e-vendorid then
          next.
        if notes.noteln[3] < b-vparent or
           notes.noteln[3] > e-vparent then
          next.
      end. /* avail notes */
      
      if not avail notes then
        do:
        v-parentcode = "".
        v-vendorid = substring(icsp.prodcat,1,3).
        if substring(icsp.prodcat,1,1) = "a" then 
          v-technology = "Automation".
        else if substring(icsp.prodcat,1,1) = "f" then
          v-technology = "Fabrication". 
        else if substring(icsp.prodcat,1,1) = "h" then 
          v-technology = "Hydraulic".   
        else if substring(icsp.prodcat,1,1) = "p" then 
          v-technology =  "Pneumatic".   
        else if substring(icsp.prodcat,1,1) = "s" then 
          v-technology =  "Service".     
        else if substring(icsp.prodcat,1,1) = "f" then 
          v-technology =  "Filtration".  
        else if substring(icsp.prodcat,1,1) = "c" then 
          v-technology =  "Connectors".  
        else if substring(icsp.prodcat,1,1) = "m" then 
          v-technology =  "Mobile".      
        else if substring(icsp.prodcat,1,1) = "n" then 
          v-technology =  "None".        
      end. /* not avail notes */
      else
        assign v-vendorid   = notes.noteln[1]
               v-technology = notes.noteln[2]
               v-parentcode = notes.noteln[3].
      
      if p-list = true then
        do:
        assign zelection_type = "n".
               zelection_char4 = v-vendorid.
               zelection_cust = 0.
        run zelectioncheck.
        if zelection_good = false then
          next.
      
        assign zelection_type = "e".
               zelection_char4 = v-parentcode.
               zelection_cust = 0.
        run zelectioncheck.
        if zelection_good = false then
          next.
      
        assign zelection_type = "v"
               zelection_vend = icsw.arpvendno
               zelection_char4 = " ".
        run zelectioncheck.
        if zelection_good = false then
          next.
          
        assign zelection_type = "u"
               zelection_char4 = icsw.prod.
        run zelectioncheck.
        if zelection_good = false then
          next.
    
        assign zelection_type = "p"
               zelection_char4 = icsp.prodcat
               zelection_cust = 0.
        run zelectioncheck.
        if zelection_good = false then
          next.
        assign zelection_type = "b"
               zelection_char4 = icsl.buyer
               zelection_cust = 0.
        run zelectioncheck.
        if zelection_good = false then
          next.
        assign zelection_type = "w"
               zelection_char4 =  icsd.whse
               zelection_cust = 0.
        run zelectioncheck.
        if zelection_good = false then
          next.
      end. /* plist */  
        assign x-totstkitems = x-totstkitems + 1.
    end. /* each icsw */

    for each zicswso use-index index1 where 
                           zicswso.whse        = icsd.whse and
                           zicswso.prod       >= b-prod    and
                           zicswso.prod       <= e-prod    and
                           zicswso.arpvendno  >= b-vend    and
                           zicswso.arpvendno  <= e-vend    and
                         /*zicswso.statustype  = "A"       and*/
                         ((p-outside = no and
                           zicswso.stockoutdt >= b-stockoutdt and
                           zicswso.stockoutdt <= e-stockoutdt) or
                          (p-outside = yes and zicswso.statustype = "A" and
                           zicswso.stockoutdt >= 01/01/1950 and
                           zicswso.stockoutdt <= 12/31/2049)) and
                           zicswso.class      >= b-class and
                           zicswso.class      <= e-class and
                           zicswso.daysstkout >= b-daysstkout and
                           zicswso.daysstkout <= e-daysstkout
                           no-lock:
      if p-stockouttp = "O" and zicswso.stkouttype <> "O" then next.
      if p-stockouttp = "A" and zicswso.stkouttype <> "A" then next.
      
      find icsw where icsw.cono = g-cono and
                      icsw.whse = zicswso.whse and
                      icsw.prod = zicswso.prod and
                      icsw.prodline >= b-pline and
                      icsw.prodline <= e-pline
                      no-lock no-error.
      if not avail icsw then next.
      
      find icsp where icsp.cono = g-cono and
                      icsp.prod = zicswso.prod and
                      icsp.statustype = "A" and
                      icsp.kittype <> "B" and
                      icsp.prodcat >= b-pcat and
                      icsp.prodcat <= e-pcat
                      no-lock no-error.
      if not avail icsp then next.
      
      find icsl where icsl.cono = g-cono and
                      icsl.whse = zicswso.whse and
                      icsl.vendno = icsw.arpvendno and
                      icsl.prodline = icsw.prodline and
                      icsl.buyer >= b-buyer and
                      icsl.buyer <= e-buyer
                      no-lock no-error.
      if not avail icsl then next.
      find oimsp where oimsp.person       = icsl.buyer and
                       oimsp.responsible >= b-buygroup and
                       oimsp.responsible <= e-buygroup
                       no-lock no-error.
      if not avail oimsp then next.
      find notes where notes.cono = 1 and 
                       notes.notestype = "zz" and
                       notes.primarykey  = "apsva" and
                       notes.secondarykey = icsp.prodcat 
                       no-lock no-error. 
      if avail notes then
        do:
        if notes.noteln[1] < b-vendorid or
           notes.noteln[1] > e-vendorid then
          next.
        if notes.noteln[3] < b-vparent or
           notes.noteln[3] > e-vparent then
          next.
      end.
      
      if not avail notes then
        do:
        v-parentcode = "".
        v-vendorid = substring(icsp.prodcat,1,3).
        if substring(icsp.prodcat,1,1) = "a" then 
          v-technology = "Automation".
        else if substring(icsp.prodcat,1,1) = "f" then
          v-technology = "Fabrication". 
        else if substring(icsp.prodcat,1,1) = "h" then 
          v-technology = "Hydraulic".   
        else if substring(icsp.prodcat,1,1) = "p" then 
          v-technology =  "Pneumatic".   
        else if substring(icsp.prodcat,1,1) = "s" then 
          v-technology =  "Service".     
        else if substring(icsp.prodcat,1,1) = "f" then 
          v-technology =  "Filtration".  
        else if substring(icsp.prodcat,1,1) = "c" then 
          v-technology =  "Connectors".  
        else if substring(icsp.prodcat,1,1) = "m" then 
          v-technology =  "Mobile".      
        else if substring(icsp.prodcat,1,1) = "n" then 
          v-technology =  "None".        
      end. /* not avail notes */
      else
        assign v-vendorid   = notes.noteln[1]
               v-technology = notes.noteln[2]
               v-parentcode = notes.noteln[3].
      
      if p-list = true then
        do:
        assign zelection_type = "n".
               zelection_char4 = v-vendorid.
               zelection_cust = 0.
        run zelectioncheck.
        if zelection_good = false then
          next.
      
        assign zelection_type = "e".
               zelection_char4 = v-parentcode.
               zelection_cust = 0.
        run zelectioncheck.
        if zelection_good = false then
          next.
      
        assign zelection_type = "v"
               zelection_vend = icsw.arpvendno
               zelection_char4 = " ".
        run zelectioncheck.
        if zelection_good = false then
          next.
          
        assign zelection_type = "u"
               zelection_char4 = icsw.prod.
        run zelectioncheck.
        if zelection_good = false then
          next.
    
        assign zelection_type = "p"
               zelection_char4 = icsp.prodcat
               zelection_cust = 0.
        run zelectioncheck.
        if zelection_good = false then
          next.
        assign zelection_type = "b"
               zelection_char4 = icsl.buyer
               zelection_cust = 0.
        run zelectioncheck.
        if zelection_good = false then
          next.
        assign zelection_type = "w"
               zelection_char4 =  icsd.whse
               zelection_cust = 0.
        run zelectioncheck.
        if zelection_good = false then
          next.
        
        assign x-totstkitems = x-totstkitems + 1.
      end. /* plist */

      assign s-stockoutfrq = 0.
      if p-stockouttp = "B" then
        do:
        for each b-zicswso where 
                 b-zicswso.whse        = zicswso.whse and
                 b-zicswso.prod        = zicswso.prod and
                (b-zicswso.qtyavail    = 0 or
                 b-zicswso.qtyonhand   = 0) and
                 b-zicswso.stockoutdt >= TODAY - 365
                 no-lock:
          assign s-stockoutfrq = s-stockoutfrq + 1.
        end. /* each b-zicswso */
      end. /* p-stockouttp = "B" */
      else
        do:
        for each b-zicswso where 
                 b-zicswso.whse        = zicswso.whse and
                 b-zicswso.prod        = zicswso.prod and
                 b-zicswso.stockoutdt >= TODAY - 365  and
                 b-zicswso.stkouttype  = p-stockouttp 
                 no-lock:
          assign s-stockoutfrq = s-stockoutfrq + 1.
        end. /* each b-zicswso */
      end. /* p-stockouttp is specific */
      if p-nbrstkout > 0 and s-stockoutfrq < p-nbrstkout then next.
      
      /********
      if p-stockouttp = "B" then
        do:
        find stkout where stkout.whse = zicswso.whse and
                          stkout.prod = zicswso.prod and
                          stkout.qtyonhand = 0 and
                          stkout.qtyavail  = 0
                          no-lock no-error.
        if avail stkout then next.
      end.
      *********/
      if p-stockouttp = "B" then
        do:
        if zicswso.stkouttype = "O" then
          do:
          find stkout where stkout.whse = zicswso.whse and
                            stkout.prod = zicswso.prod and
                            stkout.stkouttype = "A"
                            no-lock no-error.
          if avail stkout then next.
          else
            do:
            find b-zicswso where b-zicswso.whse = zicswso.whse and
                                 b-zicswso.prod = zicswso.prod and
                                 b-zicswso.statustype = "A" and
                                 b-zicswso.stkouttype = "A" and
                                 b-zicswso.stockoutdt < zicswso.stockoutdt
                                 no-lock no-error.
            if avail b-zicswso then next.
          end.
        end. /* zicswso.stkouttype */
      end. /* p-stockouttp = "B" */
      
      assign x-usage = 0.
      find icswu where icswu.cono = g-cono and
                       icswu.whse = icsw.whse and
                       icswu.prod = icsw.prod
                       no-lock no-error.
      if avail icswu then
        do:
        do idx = 1 to 6:
          assign x-usage = x-usage + icswu.normusage[idx].
        end.
      end. /* avail icswu */
      
      find apsv where apsv.cono = g-cono and
                      apsv.vendno = icsw.arpvendno
                      no-lock no-error.

      create stkout.
      assign stkout.whse        = zicswso.whse
             stkout.prod        = zicswso.prod
             stkout.statustype  = icsw.statustype
             stkout.stkouttype  = zicswso.stkouttype
             stkout.inactivetp  = zicswso.inactivetp
             stkout.stockoutdt  = zicswso.stockoutdt
             stkout.expecteddt  = zicswso.expecteddt
             stkout.lastrcptdt  = zicswso.lastrcptdt
             stkout.daysstkout  = zicswso.daysstkout
             stkout.stockoutfrq = s-stockoutfrq
             stkout.qtyonhand   = int(zicswso.qtyonhand)
             stkout.qtyavail    = int(zicswso.qtyavail)
             stkout.qtyonorder  = int(zicswso.qtyonorder)
             stkout.qtybo       = int(zicswso.qtybo)
             stkout.ordcalcty   = zicswso.ordcalcty
             stkout.orderpt     = zicswso.orderpt
             stkout.linept      = zicswso.linept
             stkout.frozentype  = zicswso.frozentype
             stkout.class       = zicswso.class
             stkout.arptype     = zicswso.arptype
             stkout.vendno      = zicswso.arpvendno
             stkout.vname       = if avail apsv then substr(apsv.name,1,19)
                                  else " "
             stkout.buyer       = icsl.buyer
             stkout.buygroup    = oimsp.responsible
             stkout.prodline    = icsl.prodline
             stkout.prodcat     = icsp.prodcat
             stkout.vendorid    = if avail notes then notes.noteln[1]
                                  else " "
             stkout.parentid    = if avail notes then notes.noteln[3]
                                  else " "
             stkout.usage       = int(x-usage)
             stkout.totstkitems = x-totstkitems
             stkout.totstkout   = 1
             stkout.tot0onhand  = if stkout.qtyonhand    <= 0 then 1 else 0
             stkout.tot0avail   = if stkout.qtyavail     <= 0 then 1 else 0
             stkout.tot0availonhand = if stkout.qtyavail <= 0 and
                                         stkout.qtyonhand > 0 then 1 else 0.
      
      assign x-totstkout = x-totstkout + 1.
      if stkout.qtyonhand <= 0 then
        assign x-tot0onhand = x-tot0onhand + 1.
      if stkout.qtyavail  <= 0 then
        assign x-tot0avail  = x-tot0avail + 1.
      if stkout.qtyavail  <= 0 and stkout.qtyonhand > 0 then
        assign x-tot0availonhand = x-tot0availonhand + 1.
      assign stkout.srtqtyonhand = string(stkout.qtyonhand,">>>>9-").
    end. /* each zicswso */
  end. /* each icsd */
end. /* extract_data procedure */

Procedure LinePrint:
  define input parameter s-recid as recid no-undo.
  
  find stkout where recid(stkout) = s-recid no-lock no-error.
  if avail stkout then
    display stkout.buyer
            stkout.whse
            stkout.vname
            stkout.vendno
            stkout.prod
            stkout.qtyonhand
            stkout.qtyavail
            stkout.qtyonord
            stkout.qtybo
            stkout.expecteddt
            stkout.statustype
            stkout.ordcalcty
            stkout.orderpt
            stkout.linept
            stkout.arptype
            stkout.frozentype
            stkout.class
            stkout.usage
            stkout.stockoutdt
            stkout.daysstkout
            stkout.stockoutfrq
            stkout.lastrcptdt
    with frame f-Rstockout.
    down with frame f-Rstockout.
end /* LinePrint */

/***
Procedure look-for-record:
  find zicswso where zicswso.whse        = icsd.whse and
                     zicswso.prod       >= b-prod    and
                     zicswso.prod       <= e-prod    and
                     zicswso.arpvendno  >= b-vend    and
                     zicswso.arpvendno  <= e-vend    and
                     zicswso.stkouttype = "A"        and
                    ((p-outside = no and
                     zicswso.stockoutdt >= b-stockoutdt and
                     zicswso.stockoutdt <= e-stockoutdt) or
                    (p-outside = yes and zicswso.statustype = "A" and
                     zicswso.stockoutdt >= 01/01/1950 and
                     zicswso.stockoutdt <= 12/31/2049)) and
                     zicswso.class      >= b-class and
                     zicswso.class      <= e-class and
                     zicswso.daysstkout >= b-daysstkout and
                     zicswso.daysstkout <= e-daysstkout
                     no-lock:
  if avail zicswso then
    do:
    find icsw where icsw.cono = g-cono and
                    icsw.whse = zicswso.whse and
                    icsw.prod = zicswso.prod and
                    icsw.prodline >= b-pline and
                    icsw.prodline <= e-pline
                    no-lock no-error.
    if not avail icsw then next.
      
    find icsp where icsp.cono = g-cono and
                    icsp.prod = zicswso.prod and
                    icsp.statustype = "A" and
                    icsp.kittype <> "B" and
                    icsp.prodcat >= b-pcat and
                    icsp.prodcat <= e-pcat
                    no-lock no-error.
    if not avail icsp then next.
      
    find icsl where icsl.cono = g-cono and
                    icsl.whse = zicswso.whse and
                    icsl.vendno = icsw.arpvendno and
                    icsl.prodline = icsw.prodline and
                    icsl.buyer >= b-buyer and
                    icsl.buyer <= e-buyer
                    no-lock no-error.
    if not avail icsl then next.
    find oimsp where oimsp.person       = icsl.buyer and
                     oimsp.responsible >= b-buygroup and
                     oimsp.responsible <= e-buygroup
                     no-lock no-error.
    if not avail oimsp then next.
    find notes where notes.cono = 1 and 
                     notes.notestype = "zz" and
                     notes.primarykey  = "apsva" and
                     notes.secondarykey = icsp.prodcat 
                     no-lock no-error. 
    if avail notes then
      do:
      if notes.noteln[1] < b-vendorid or
         notes.noteln[1] > e-vendorid then
        next.
      if notes.noteln[3] < b-vparent or
         notes.noteln[3] > e-vparent then
        next.
    end.
  end. /* avail zicswso */
end. /* look-for-record. */
***/  
/{&user_procedures}*/



/* TBXR F6 CodeGen Includes from here down are CodeGen Generated  */ 



/{&user_optiontype}*/
/{&user_optiontype}*/
   

/{&user_registertype}*/
/{&user_registertype}*/
 

/{&user_exportvarheaders1}*/
/{&user_exportvarheaders1}*/


/{&user_exportvarheaders2}*/
/{&user_exportvarheaders2}*/

/{&user_DWvarheaders1}*/
/{&user_DWvarheaders1}*/

/{&user_loadtokens}*/
/{&user_loadtokens}*/


/{&user_totaladd}*/
/{&user_totaladd}*/


/{&user_numbertotals}*/
/{&user_numbertotals}*/



/{&user_summaryloadprint}*/
/{&user_summaryloadprint}*/



/{&user_summaryloadexport}*/
/{&user_summaryloadexport}*/



/* TBXR F6 CodeGen End of Includes that are CodeGen Generated  */ 






