/* insaex3.lpr 1.2 09/15/98 */
/*h****************************************************************************
  INCLUDE      : insaex3.lpr
  DESCRIPTION  : Fields to extract during insaex3.p
  USED ONCE?   :
  AUTHOR       : drm
  DATE WRITTEN : 04/21/97
  CHANGES MADE :
    04/21/97 drm; TB# 23488 New include.
    07/17/97 jkp; TB# 23488 Added comments and added vendor and customer 
        rebates.
    07/18/97 jkp; TB# 23528 Added str parameter to vendor rebate, customer 
        rebate, number of lines, class, and error id fields.
    08/12/97 mms; TB# 23488 Added buffer logic for oeeh.
    08/16/97 jkp; TB# 23605 Added parameter &case to pass the case selected.
        Changed all numbered parameters to named parameters.
    10/29/97 jkp; TB# 23938 Do not allow question marks to be output.
    10/29/97 jkp; TB# 23996 Convert rebates to negative if the line is a 
        return.
    10/29/97 jkp; TB# 24010 Change the order type to consider the line 
        information instead of just using the oeeh.ordertype.
    12/16/97 jkp; TB# 24271 Changed backlog to oeel.stkqtyord for quantity.
    09/05/98 jkp; TB# 25288 Added sales data to be output, but include too
        large so created insaex3s.lpr just for sales.  If changes are made 
        here, they may also affect insaex3s.lpr.
    08/19/99 jkp; TB# 24005 Round/Trim all fields larger than the data
        warehouse field back to the data warehouse size.  This is due to SQL 
        7.0 tight typing.
    11/23/99 jkp; TB# 26929 Changed the output for v-ordertype by removing the 
        override of RM type based on oeel.returnfl.  This is all handled in 
        insaex3.p.
******************************************************************************/

/*o All fields must be put out as character fields.  So if a field is numeric
    or is a date, it must use the "str" parameter to make it a character 
    field.  For those fields that truly are a character field, i.e., they 
    don't need the "str" parameter, they must have parameter 4 in front 
    of them to get them in the correct case.  In addition, all signed fields
    must have the sign on the left.  It is imperative that no question marks
    are output, so every field must be checked to make sure it is not a 
    question mark, even though that would be corrupt data except for a date
    field.  This is because the analyzer process cannot handle question marks
    and it is SQL code, so it cannot even give a graceful error - it just
    aborts. Some of these checks are made in insaex3.p and some are made here.*/
    
/* Passing parameters definition *********************************************
   {&type} Type of gateway being produced:
           i = invoices
           b = bookings
           a = backlog
           s = sales
   {&buffoeel} Buffer prefix for oeel can be either b- or b2-
   {&buffoeeh} Buffer prefix for oeeh can be either b- or b2-
   {&case}     Selection of the case sensitivity.  This is passed by a group
               of if then else statements and is set to either "caps", "lc", 
               or " " depending on how the user answered the p-casety option 
               on the SASSR.
                    
   Sample include calls might be:
       {insaex3.lpr 
           &type     = "b" 
           &buffoeel = "b-" 
           &buffoeeh = "b-" 
           &case     = "caps"}     
           (Will create a bookings gateway, using b-oeel and b-oeeh and will
            output all character fields in all upper case letters)
       {insaex3.lpr 
           &type     = "i" 
           &buffoeel = "b2-" 
           &buffoeeh = "b2-" 
           &case     = "lc"}
           (Will create an invoices gateway, using b2-oeel and b2-oeeh and 
            will output all character fields in all lower case letters)
******************************************************************************/

if (zxhd.ordno <> ? and zxhd.ordsuf <> ?) then
    string(zxhd.ordno,">>>>>>>9") +
        "-" +  
        string(zxhd.ordsuf,"99")
else v-null
v-d

(if zxhd.invoicedt <> ? then 
   string(zxhd.invoicedt)
 else string(today))
 
v-d 

string(v-fper,"99")

v-d

string(v-fyear,"9999")

v-d

(if zxhd.enterdt <> ? then 
   string(zxhd.enterdt)
 else string(today))
 
v-d 

(if zxhd.reqshipdt <> ? then 
   string(zxhd.reqshipdt)
 else string(today))
 
v-d 

(if zxhd.promisedt <> ? then 
   string(zxhd.promisedt)
 else string(today))
 
v-d 

(if zxhd.shipdt <> ? then 
   string(zxhd.shipdt)
 else string(today))
 
v-d 

{&case}(zxhd.entity)

v-d

{&case}(zxhd.transtype)

v-d

if zxhd.whse <> ? then
    {&case}(zxhd.whse)
else v-null

v-d

/*tb 23938 10/29/97 jkp; Added check for question mark. */
/*e Taken By */
if zxhd.takenby <> "" then
    {&case}(zxhd.takenby)
else {&case}(zxhd.slsrepin)

v-d

/*tb 23938 10/29/97 jkp; Added check for question mark. */
/*e Taken By */
if zxhd.slsrepin <> "" then
    {&case}(zxhd.slsrepin)
else {&case}(v-null)

v-d

/*tb 23938 10/29/97 jkp; Added check for question mark. */
/*e Taken By */
if zxhd.slsrepout <> "" then
    {&case}(zxhd.slsrepout)
else {&case}(v-null)

v-d


/*tb 23938 10/29/97 jkp; Added check for question mark. */
/*e Customer ID */
if zxhd.custno <> ? then
    string(zxhd.custno)
else string(v-zero)    
v-d

/*tb 23938 10/29/97 jkp; Added check for question mark. */
/*e Ship To ID */
if zxhd.shipto <> ? then
    {&case}(substring(zxhd.shipto,1,8))
else v-null
v-d

{&case}(zxhd.name)

v-d

{&case}(string(zxhd.shiptoaddr1,"x(30)"))

v-d

{&case}(string(zxhd.shiptoaddr2,"x(30)"))

v-d

{&case}(string(zxhd.shiptocity,"x(20)"))

v-d

{&case}(string(zxhd.shiptost,"x(2)"))

v-d

if zxhd.countrycd <> "" then
  {&case}(zxhd.countrycd)
else
  v-null
  
v-d

if zxhd.shipvia <> ? then
    {&case}(substring(zxhd.shipvia,1,4))
else v-null
v-d

/*tb 23938 10/29/97 jkp; Added check for question mark. */
/*e Terms Type */
if zxhd.termstype <> ? then
    {&case}(substring(zxhd.termstype,1,4))
else v-null
v-d

if zxhd.custpo <> ? then
    {&case}(zxhd.custpo)
else v-null

v-d


if zxhd.invamt <> ? then
    string(zxhd.invamt)
else string(v-zero)    
v-d

if zxhd.ordamt <> ? then
    string(zxhd.ordamt)
else string(v-zero)    
v-d

if zxhd.ordcst <> ? then
    string(zxhd.ordcst)
else string(v-zero)    
v-d


if zxhd.tendamt <> ? then
    string(zxhd.tendamt)
else string(v-zero)    
v-d


if zxhd.nolines <> ? then
    string(zxhd.nolines)
else string(v-zero)    
v-d


if zxhd.frtin <> ? then
    string(zxhd.frtin)
else string(v-zero)    
v-d

if zxhd.frtout <> ? then
    string(zxhd.frtout)
else string(v-zero)    
v-d

if zxhd.handle <> ? then
    string(zxhd.handle)
else string(v-zero)    
v-d

if zxhd.fuelsur <> ? then
    string(zxhd.fuelsur)
else string(v-zero)    
v-d

if zxhd.exped <> ? then
    string(zxhd.exped)
else string(v-zero)    
v-d

if zxhd.restk <> ? then
    string(zxhd.restk)
else string(v-zero)    
v-d
{&case}(zxhd.completefl)
v-d
/*e SX User 1 */
{&case}(zxhd.crreasonty)
v-d
/*e SX User 2 */
{&case}(if zxhd.pmfl then "yes" else "no")
v-d
/*e SX User 3 */
{&case}(string(zxhd.actfrt))
v-d
/*e SX User 4 */
{&case}(string(zxhd.frtcode))
v-d
/*e SX User 5 */
{&case}(v-null)

v-d
{&case}(zxhd.shiptozip)
v-d
if zxhd.tax <> ? then
  string(zxhd.tax)
else
  string(v-zero)
v-d
if zxhd.gst <> ? then
  string(zxhd.gst)
else
  string(v-zero)

v-d
if zxhd.pst <> ? then
  string(zxhd.pst)
else
  string(v-zero)

v-d

if weekday(zxhd.invoicedt) = 1 then
   zxhd.invoicedt - 2
else
if weekday(zxhd.invoicedt) = 7 then   
   zxhd.invoicedt - 1
else
   zxhd.invoicedt  
  
v-d

(if zxhd.ofrtin  then "YES" else "NO")
v-d

(if zxhd.ofrtout then "YES" else "NO")
v-d

(if zxhd.ohandle then "YES" else "NO")
v-d

(if zxhd.cfrtin then "YES" else "NO")

v-d

(if zxhd.cfrtout then "YES" else "NO")

v-d

(if zxhd.chandle then "YES" else "NO")

v-d
  string(zx-ssur)
v-d
  string(zx-sur)
v-d
  string(zxhd.divno,">>9")
v-d
  {&case}(string(zxhd.allcostcd,"x(2)"))
v-d
if zxhd.ccfee <> ? then
    string(zxhd.ccfee)
else string(v-zero)    
v-d
v-null
v-d
string(zxhd.parentrep,"x(4)")
v-d
string(zxhd.shiptoname,"x(30)")
v-d
{&case} ( substring(string(zxhd.shipinstr,"x(30)"),1,30))
v-d
{&case} ( substring(zxhd.sourcepros,1,4) )
v-d
 ( if  zxhd.inoverdt <> ? then  
     string(zxhd.inoverdt) 
   else
     string(01/01/50) )
v-d
 ( if  zxhd.outoverdt <> ? then  
     string(zxhd.outoverdt) 
   else
     string(01/01/50) )
v-d
{&case} ( substring ( zxhd.inby,1,4) )
v-d
{&case} ( substring ( zxhd.outby,1,4) ) 
v-d
{&case} ( substring ( zxhd.inreason,1,4) )
v-d
{&case} ( substring ( zxhd.inreasonlit,1,40) )
v-d
{&case} ( substring ( zxhd.outreason,1,4) )
v-d
{&case} ( substring ( zxhd.outreasonlit,1,40) ) 
v-d
{&case} (string(zxhd.inbcommoe)) 
v-d
{&case} (string(zxhd.outcommoe)) 
v-d
{&case} (string(zxhd.inbcommxq)) 
v-d
{&case} (string(zxhd.outcommxq)) 
v-d
{&case} (string(zxhd.exceptcdoe))
v-d       
{&case} (string(zxhd.exceptcddtoe))
v-d  
{&case} (string(zxhd.exceptcdxq))   
v-d  
{&case} (string(zxhd.exceptcddtxq))   

CHR(13) CHR(10).


