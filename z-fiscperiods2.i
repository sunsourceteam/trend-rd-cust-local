/* Range finder for fiscal periods */

Define {1} shared Temp-Table zfisc no-undo
Field zbegin as date
Field zend   as date
field zfiscdt as date format "99/99/9999"
Field zperfisc as integer
Field zpersasc as logical
Field zcnt    as integer

Index fisc-inx zperfisc
Index fisc-dt  zfiscdt
Index fisc-fm  zbegin
               zend.
Define {1} shared var z-log as logical init false no-undo.
Define {1} shared var z-cntr as integer no-undo.
Define {1} shared var z-datein1 as date no-undo.
Define {1} shared var z-paramdate as date no-undo.
Define {1} shared var z-paramfisc as int  no-undo.
Define {1} shared var z-fiscper as int no-undo.
Define {1} shared var z-datein2 as date no-undo.
Define {1} shared var z-inx as int no-undo.
Define {1} shared buffer zx-sasc for sasc.
find zx-sasc where zx-sasc.cono = g-cono no-lock.

do z-inx = 1 to 12:
  create zfisc.
  if z-inx = 1 then
    assign zfisc.zbegin = date(01,01,year(zx-sasc.glenddt[z-inx])).
  else
    assign zfisc.zbegin = zx-sasc.glenddt[z-inx - 1] + 1.
  assign zfisc.zend = zx-sasc.glenddt[z-inx]
         zfisc.zpersasc = true
         zfisc.zperfisc = (z-inx * 100) +
           int(substring(string(year(zx-sasc.glenddt[1]),"9999"),3,2))
         zfisc.zfiscdt = date(z-inx,01,
               (year(zx-sasc.glenddt[1]))).
           
end.

for each sasj where sasj.cono = g-cono and sasj.currproc = "oeepi"
                                       and smmergedfl = yes
                                       no-lock :


  find zfisc where zfisc.zperfisc = sasj.perfisc no-lock no-error.
  if avail zfisc and zfisc.zpersasc = true then
    next.
  if not avail zfisc then
    do:

      message sasj.postdt
             sasj.postdt
            sasj.perfisc.
    pause.  
    create zfisc.
    assign zfisc.zbegin = sasj.postdt
           zfisc.zend =   sasj.postdt
           zfisc.zpersasc = false
           zfisc.zperfisc = sasj.perfisc
           zfisc.zfiscdt =
           date(int(substring(string(zfisc.zperfisc,"9999"),1,2)),
           01,
           ( if int(substring(string(zfisc.zperfisc,"9999"),3,2)) < 50 then
                int(substring(string(zfisc.zperfisc,"9999"),3,2)) + 2000
             else                
                int(substring(string(zfisc.zperfisc,"9999"),3,2)) + 1900)).
    end.
  else
    do:
    if zfisc.zbegin > sasj.postdt then
      assign zfisc.zbegin = sasj.postdt.
    if zfisc.zend < sasj.postdt then
      assign zfisc.zend = sasj.postdt.
    end.
end.
assign z-cntr = 0.
for each zfisc use-index fisc-dt:
  assign z-cntr = z-cntr + 1.
  assign zfisc.zcnt = z-cntr.
end.  

procedure getbegindate:
  find zfisc use-index fisc-dt
                       where zfisc.zfiscdt =
                          date(month(z-paramdate),
                               01,
                               year(z-paramdate)) no-lock no-error.
  if avail zfisc then
    z-paramdate = zfisc.zbegin.

  if not avail zfisc and z-log then
    do:
    message z-paramdate z-paramfisc "getbegindate".
    pause.
    end.
end.

procedure getenddate:
  find zfisc use-index fisc-dt
                       where zfisc.zfiscdt =
                          date(month(z-paramdate),
                               01,
                               year(z-paramdate)) no-lock no-error.
                             
  if not avail zfisc and z-log then
    do:
    message z-paramdate z-paramfisc "getenddate".
    pause.
    end.
  if avail zfisc then
    z-paramdate = zfisc.zend.
end.

procedure getfiscdate:
  find zfisc use-index fisc-dt
                       where zfisc.zfiscdt =
                          date(month(z-paramdate),
                               01,
                               year(z-paramdate)) no-lock no-error.
                             
  if not avail zfisc and z-log then
    do:
    message z-paramdate z-paramfisc "getfiscdate".
    pause.
    end.
  if avail zfisc then
    z-paramdate = zfisc.zfiscdt.
end.

procedure getfiscper:
  find zfisc use-index fisc-inx
                       where zfisc.zperfisc =
                             z-paramfisc
                             no-lock no-error.

  if not avail zfisc and z-log then
    do:
    message z-paramdate z-paramfisc "getfiscper".
    pause.
    end.
                             
  if not avail zfisc then
     
     z-paramfisc = 9999.

end.


procedure getfiscfmdate:
  find zfisc use-index fisc-fm
                       where zfisc.zbegin le z-paramdate and
                             zfisc.zend ge z-paramdate
                             no-lock no-error.
                             
  if not avail zfisc and z-log then
    do:
    message z-paramdate z-paramfisc "getfiscfmdate".
    pause.
    end.
  if not avail zfisc then
     z-paramfisc = 9999.
  else
     z-paramfisc = zfisc.zperfisc.
end.


