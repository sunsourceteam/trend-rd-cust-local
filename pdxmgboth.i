/*****************************************************************************
 
 pdxmgsump.i  - General Price Matrix Reporting - By Product Price Type Summary
                This include is called by pdxmg.p.
                Any changes made to pdxmgsump.i may apply to pdxmgdetp.i,
                pdxmgsumc.i or pdxmgdetc.i.
 
*****************************************************************************/ 


  assign i = MONTH(p-date).

  for each matrix use-index k-both   where matrix.yr = YEAR(p-date)
                                       /*
                                       and matrix.region <> ""
                                       and matrix.district <> ""
                                       */
                                       /*
                                       and matrix.i-ytd-amt <> 0
                                       */
                                           no-lock
      break by matrix.region
            by (if p-region = yes then matrix.blank-sort
                else matrix.district)
            by matrix.i-ytd-amt   descending
            by matrix.pprice-type
            by matrix.cprice-type
            by matrix.pct-rate:

      if matrix.cprice-type <> "0001" then do:
      assign s-mo-isales     = s-mo-isales + 
                          (matrix.i-amount[i] - matrix.i-discount[i])
             s-mo-imrgsales  = s-mo-imrgsales +
                          (matrix.i-amount[i] - matrix.i-discount[i]) -
                           matrix.i-cost[i]
                           
             s-mo-msales     = s-mo-msales + matrix.m-amount[i]
             s-mo-mmrgsales  = s-mo-mmrgsales +
                          (matrix.m-amount[i] - matrix.m-cost[i]).
                          
                           
      do y = 1 to i:
         assign s-ytd-isales    = s-ytd-isales +
                          (matrix.i-amount[y] - matrix.i-discount[y])
                s-ytd-imrgsales = s-ytd-imrgsales +
                          (matrix.i-amount[y] - matrix.i-discount[y]) -
                           matrix.i-cost[y]
                           
                s-ytd-msales    = s-ytd-msales + matrix.m-amount[y]
                s-ytd-mmrgsales = s-ytd-mmrgsales +
                          (matrix.m-amount[y] - matrix.m-cost[y])
                          
                s-lmo-isales    = s-lmo-isales +
                      (matrix.i-amount[y + 12] - matrix.i-discount[y + 12])
                s-lmo-imrgsales    = s-lmo-imrgsales +
                      (matrix.i-amount[y + 12] - matrix.i-discount[y + 12]) -
                       matrix.i-cost[y + 12]
                        
                s-lmo-msales    = s-lmo-msales +  matrix.m-amount[y + 12]
                s-lmo-mmrgsales = s-lmo-mmrgsales +
                       (matrix.m-amount[y + 12] - matrix.m-cost[y + 12]).

      end.
      end.  /* matrix.cprice-type <> 0001 */

      if (first-of(matrix.pprice-type)) then do:
         if print-count < p-count then do:

            assign s-mo-isales     = s-mo-isales +
                                (matrix.i-amount[i] - matrix.i-discount[i]).
                           
            do y = 1 to i:
               assign s-ytd-isales    = s-ytd-isales +
                                (matrix.i-amount[y] - matrix.i-discount[y]).
                      s-lmo-isales    = s-lmo-isales +
                         (matrix.i-amount[y + 12] - matrix.i-discount[y + 12]).
            end.

            assign s-cprice    = matrix.cprice-type
                   s-pprice    = matrix.pprice-type
                   substring(s-rgn,1,1) = matrix.region
                   substring(s-rgn,2,3) = matrix.district
                   s-pct       = matrix.pct-mult
                   s-price-desc = "".
            
            find sasta use-index k-sasta where sasta.cono = 1
                                        and sasta.codeiden = "k"
                                        and sasta.codeval  = matrix.pprice-type
                                            no-lock no-error.
            if avail sasta then
               assign s-price-desc = substring(sasta.descrip,1,15).
            else
               assign s-price-desc = "description n/a".

            display s-pprice
                    s-price-desc
                    s-mo-isales
                    s-lmo-isales
                    s-ytd-isales
                    with frame f-detail.
            down with frame f-detail.
            assign print-count  = print-count + 1
                   s-mo-isales  = 0
                   s-lmo-isales = 0
                   s-ytd-isales = 0.
            
         end.
      end.

      
      if (last-of(matrix.cprice-type)) and matrix.cprice-type <> "0001" then do:
         if s-mo-isales <> 0 then
            assign s-mo-imarg   = (s-mo-imrgsales  / s-mo-isales) * 100.
         if s-mo-msales <> 0 then
            assign s-mo-mmarg   = (s-mo-mmrgsales  / s-mo-msales) * 100.
         if s-lmo-isales <> 0 then
            assign s-lmo-imarg  = (s-lmo-imrgsales / s-lmo-isales) * 100.
         if s-lmo-msales <> 0 then
            assign s-lmo-mmarg  = (s-lmo-mmrgsales / s-lmo-msales) * 100.
         if s-ytd-isales <> 0 then
            assign s-ytd-imarg  = (s-ytd-imrgsales / s-ytd-isales) * 100.
         if s-ytd-msales <> 0 then
            assign s-ytd-mmarg  = (s-ytd-mmrgsales / s-ytd-msales) * 100.
         /*
         if g-operinit = "das" then do:
            display "i" matrix.region matrix.district matrix.cprice-type
                    matrix.pprice-type.
         end.
         */
         assign s-cprice    = matrix.cprice-type
                s-pprice    = matrix.pprice-type
                substring(s-rgn,1,1) = matrix.region
                substring(s-rgn,2,3) = matrix.district
                s-pct       = matrix.pct-mult.
         
         if print-count < p-count and p-region = no then do:
            display s-pprice
                    s-cprice
                    s-pct
                    s-rgn
                    s-mo-isales
                    s-mo-mmarg
                    s-mo-imarg
                    s-lmo-isales
                    s-lmo-mmarg
                    s-lmo-imarg
                    s-ytd-isales
                    s-ytd-mmarg
                    s-ytd-imarg
                    with frame f-detail.
            down with frame f-detail.
         end.
         else do:
            if print-count < p-count and p-region = yes then do:
               display s-pprice
                       s-cprice
                       s-pct
                       s-mo-isales
                       s-mo-mmarg
                       s-mo-imarg
                       s-lmo-isales
                       s-lmo-mmarg
                       s-lmo-imarg
                       s-ytd-isales
                       s-ytd-mmarg
                       s-ytd-imarg
                       with frame f-detail.
               down with frame f-detail.
            end.
         end.
                      
         
         if print-count = p-count then
            assign print-count = print-count + 1.
      
      
/* Accumulate All Other Bucket */
         if print-count > p-count then do:
           if matrix.cprice-type <> "0001" then do:
            assign ao-mo-isales      = ao-mo-isales      + s-mo-isales
                   ao-mo-imrgsales   = ao-mo-imrgsales   + s-mo-imrgsales
                   ao-lmo-isales     = ao-lmo-isales     + s-lmo-isales
                   ao-lmo-imrgsales  = ao-lmo-imrgsales  + s-lmo-imrgsales
                   ao-ytd-isales     = ao-ytd-isales     + s-ytd-isales
                   ao-ytd-imrgsales  = ao-ytd-imrgsales  + s-ytd-imrgsales
            
                   ao-mo-msales      = ao-mo-msales      + s-mo-msales
                   ao-mo-mmrgsales   = ao-mo-mmrgsales   + s-mo-mmrgsales
                   ao-lmo-msales     = ao-lmo-msales     + s-lmo-msales
                   ao-lmo-mmrgsales  = ao-lmo-mmrgsales  + s-lmo-mmrgsales
                   ao-ytd-msales     = ao-ytd-msales     + s-ytd-msales
                   ao-ytd-mmrgsales  = ao-ytd-mmrgsales  + s-ytd-mmrgsales.
           end.
         end.


/* Accumlate District Totals */
         if matrix.cprice-type <> "0001" then do:
         assign s-mo-tisales     = s-mo-tisales     + s-mo-isales
                s-mo-timrgsales  = s-mo-timrgsales  + s-mo-imrgsales
                s-lmo-tisales    = s-lmo-tisales    + s-lmo-isales
                s-lmo-timrgsales = s-lmo-timrgsales + s-lmo-imrgsales
                s-ytd-tisales    = s-ytd-tisales    + s-ytd-isales
                s-ytd-timrgsales = s-ytd-timrgsales + s-ytd-imrgsales

                s-mo-tmsales     = s-mo-tmsales     + s-mo-msales
                s-mo-tmmrgsales  = s-mo-tmmrgsales  + s-mo-mmrgsales
                s-lmo-tmsales    = s-lmo-tmsales    + s-lmo-msales
                s-lmo-tmmrgsales = s-lmo-tmmrgsales + s-lmo-mmrgsales
                s-ytd-tmsales    = s-ytd-tmsales    + s-ytd-msales
                s-ytd-tmmrgsales = s-ytd-tmmrgsales + s-ytd-mmrgsales
            
/* Accumlate Regional Totals */                                    
                r-mo-tisales     = r-mo-tisales     + s-mo-isales
                r-mo-timrgsales  = r-mo-timrgsales  + s-mo-imrgsales
                r-lmo-tisales    = r-lmo-tisales    + s-lmo-isales
                r-lmo-timrgsales = r-lmo-timrgsales + s-lmo-imrgsales
                r-ytd-tisales    = r-ytd-tisales    + s-ytd-isales
                r-ytd-timrgsales = r-ytd-timrgsales + s-ytd-imrgsales
         
                r-mo-tmsales     = r-mo-tmsales     + s-mo-msales
                r-mo-tmmrgsales  = r-mo-tmmrgsales  + s-mo-mmrgsales
                r-lmo-tmsales    = r-lmo-tmsales    + s-lmo-msales
                r-lmo-tmmrgsales = r-lmo-tmmrgsales + s-lmo-mmrgsales
                r-ytd-tmsales    = r-ytd-tmsales    + s-ytd-msales
                r-ytd-tmmrgsales = r-ytd-tmmrgsales + s-ytd-mmrgsales
            
/* Accumulate Grand Totals */                                      
                g-mo-tisales     = g-mo-tisales  + s-mo-isales
                g-mo-timrgsales  = g-mo-timrgsales  + s-mo-imrgsales
                g-lmo-tisales    = g-lmo-tisales + s-lmo-isales
                g-lmo-timrgsales = g-lmo-timrgsales + s-lmo-imrgsales
                g-ytd-tisales    = g-ytd-tisales + s-ytd-isales
                g-ytd-timrgsales = g-ytd-timrgsales + s-ytd-imrgsales

                g-mo-tmsales     = g-mo-tmsales     + s-mo-msales
                g-mo-tmmrgsales  = g-mo-tmmrgsales  + s-mo-mmrgsales
                g-lmo-tmsales    = g-lmo-tmsales    + s-lmo-msales
                g-lmo-tmmrgsales = g-lmo-tmmrgsales + s-lmo-mmrgsales
                g-ytd-tmsales    = g-ytd-tmsales    + s-ytd-msales
                g-ytd-tmmrgsales = g-ytd-tmmrgsales + s-ytd-mmrgsales.
         end.


         assign s-mo-isales     = 0
                s-mo-msales     = 0
                s-mo-imrgsales  = 0
                s-mo-mmrgsales  = 0
                s-mo-mmarg      = 0
                s-mo-imarg      = 0
                s-lmo-isales    = 0
                s-lmo-msales    = 0
                s-lmo-mmarg     = 0
                s-lmo-imarg     = 0
                s-lmo-mmrgsales = 0
                s-lmo-imrgsales = 0
                s-ytd-isales    = 0
                s-ytd-msales    = 0
                s-ytd-mmarg     = 0
                s-ytd-imarg     = 0
                s-ytd-mmrgsales = 0
                s-ytd-imrgsales = 0.
      end.
      
      if (last-of(if p-region = yes then matrix.blank-sort
                        else matrix.district)) then do:
                        
          if print-count >= p-count then do:    /* print All Other line */
          
             assign print-count      = 0
                    s-mo-isales      = ao-mo-isales
                    s-lmo-isales     = ao-lmo-isales
                    s-ytd-isales     = ao-ytd-isales.
               
             if ao-mo-isales <> 0 then
                assign s-mo-imarg   = (ao-mo-imrgsales  / ao-mo-isales) * 100.
             if ao-mo-msales <> 0 then
                assign s-mo-mmarg   = (ao-mo-mmrgsales  / ao-mo-msales) * 100.
             if ao-lmo-isales <> 0 then
                assign s-lmo-imarg  = (ao-lmo-imrgsales / ao-lmo-isales) * 100.
             if ao-lmo-msales <> 0 then 
                assign s-lmo-mmarg  = (ao-lmo-mmrgsales / ao-lmo-msales) * 100.
             if ao-ytd-isales <> 0 then 
                assign s-ytd-imarg  = (ao-ytd-imrgsales / ao-ytd-isales) * 100.
             if ao-ytd-msales <> 0 then 
                assign s-ytd-mmarg  = (ao-ytd-mmrgsales / ao-ytd-msales) * 100.
                
             assign s-price-desc = "All Other Types".
             display s-price-desc
                     s-mo-isales      
                     s-mo-mmarg
                     s-mo-imarg
                     s-lmo-isales
                     s-lmo-mmarg
                     s-lmo-imarg
                     s-ytd-isales
                     s-ytd-mmarg
                     s-ytd-imarg
                  with frame f-detail.
                  down with frame f-detail.
                  down with frame f-detail.

             assign s-mo-isales  = 0
                    s-lmo-isales = 0
                    s-ytd-isales = 0
                    s-mo-imarg   = 0
                    s-mo-mmarg   = 0
                    s-lmo-imarg  = 0
                    s-lmo-mmarg  = 0
                    s-ytd-imarg  = 0
                    s-ytd-mmarg  = 0
                    
                    ao-mo-isales      = 0
                    ao-mo-imrgsales   = 0
                    ao-lmo-isales     = 0
                    ao-lmo-imrgsales  = 0
                    ao-ytd-isales     = 0
                    ao-ytd-imrgsales  = 0
                    ao-mo-msales      = 0
                    ao-mo-mmrgsales   = 0
                    ao-lmo-msales     = 0
                    ao-lmo-mmrgsales  = 0
                    ao-ytd-msales     = 0
                    ao-ytd-mmrgsales  = 0.
                  
          end.                        
                        
          if s-mo-tisales <> 0 then
             assign s-mo-timarg   = (s-mo-timrgsales  / s-mo-tisales) * 100.
          if s-mo-tmsales <> 0 then
             assign s-mo-tmmarg   = (s-mo-tmmrgsales  / s-mo-tmsales) * 100.
          if s-lmo-tisales <> 0 then
             assign s-lmo-timarg  = (s-lmo-timrgsales / s-lmo-tisales) * 100.
          if s-lmo-tmsales <> 0 then
             assign s-lmo-tmmarg  = (s-lmo-tmmrgsales / s-lmo-tmsales) * 100.
          if s-ytd-tisales <> 0 then
             assign s-ytd-timarg  = (s-ytd-timrgsales / s-ytd-tisales) * 100.
          if s-ytd-tmsales <> 0 then
             assign s-ytd-tmmarg  = (s-ytd-tmmrgsales / s-ytd-tmsales) * 100.

          assign substring(s-rgn,1,1)  = matrix.region
                 substring(s-rgn,2,3)  = matrix.district.
         
          display s-rgn
                  s-mo-tisales
                  s-mo-tmmarg
                  s-mo-timarg
                  s-lmo-tisales
                  s-lmo-tmmarg
                  s-lmo-timarg
                  s-ytd-tisales
                  s-ytd-tmmarg
                  s-ytd-timarg
                with frame f-district.
                down with frame f-district.
                                                   
          assign s-mo-tisales       = 0
                 s-mo-tmsales       = 0
                 s-mo-tmmarg        = 0 
                 s-mo-timarg        = 0 
                 s-mo-tmmrgsales    = 0
                 s-mo-timrgsales    = 0
                 s-lmo-tisales      = 0
                 s-lmo-tmsales      = 0
                 s-lmo-tmmarg       = 0 
                 s-lmo-timarg       = 0
                 s-lmo-timrgsales   = 0
                 s-lmo-tmmrgsales   = 0
                 s-ytd-tisales      = 0 
                 s-ytd-tmsales      = 0
                 s-ytd-tmmarg       = 0 
                 s-ytd-timarg       = 0
                 s-ytd-tmmrgsales   = 0
                 s-ytd-timrgsales   = 0.
          page.       
      end.
      
      if (last-of(matrix.region)) then do:
          if r-mo-tisales <> 0 then
             assign r-mo-timarg   = (r-mo-timrgsales  / r-mo-tisales) * 100. 
          if r-mo-tmsales <> 0 then
             assign r-mo-tmmarg   = (r-mo-tmmrgsales  / r-mo-tmsales) * 100. 
          if r-lmo-tisales <> 0 then
             assign r-lmo-timarg  = (r-lmo-timrgsales / r-lmo-tisales) * 100.
          if r-lmo-tmsales <> 0 then                                         
             assign r-lmo-tmmarg  = (r-lmo-tmmrgsales / r-lmo-tmsales) * 100.
          if r-ytd-tisales <> 0 then                                         
             assign r-ytd-timarg  = (r-ytd-timrgsales / r-ytd-tisales) * 100.
          if r-ytd-tmsales <> 0 then                                         
             assign r-ytd-tmmarg  = (r-ytd-tmmrgsales / r-ytd-tmsales) * 100.
             
          if matrix.region = "M" then
             assign r-rgn = "MOBIL".
          if matrix.region = "F" then
             assign r-rgn = "FILT ".
          if matrix.region = "P" then
             assign r-rgn = "PABCO".
          if matrix.region = "S" then
             assign r-rgn = "SOUTH".
          if matrix.region = "R" then
             assign r-rgn = "SRVC ".
          if matrix.region = "N" then
             assign r-rgn = "NORTH".
             
          display r-rgn
                  r-mo-tisales
                  r-mo-tmmarg
                  r-mo-timarg
                  r-lmo-tisales
                  r-lmo-tmmarg
                  r-lmo-timarg
                  r-ytd-tisales
                  r-ytd-tmmarg
                  r-ytd-timarg
              with frame f-region.
         down with frame f-region.

         assign r-mo-tisales       = 0 
                r-mo-tmsales       = 0 
                r-mo-tmmarg        = 0 
                r-mo-timarg        = 0 
                r-mo-tmmrgsales    = 0 
                r-mo-timrgsales    = 0 
                r-lmo-tisales      = 0 
                r-lmo-tmsales      = 0 
                r-lmo-tmmarg       = 0 
                r-lmo-timarg       = 0 
                r-lmo-timrgsales   = 0 
                r-lmo-tmmrgsales   = 0 
                r-ytd-tisales      = 0 
                r-ytd-tmsales      = 0 
                r-ytd-tmmarg       = 0 
                r-ytd-timarg       = 0 
                r-ytd-tmmrgsales   = 0 
                r-ytd-timrgsales   = 0.
         page.
      end.

   end.
   
   if g-mo-tisales <> 0 then 
      assign g-mo-timarg   = (g-mo-timrgsales  / g-mo-tisales) * 100.     
   if g-mo-tmsales <> 0 then                                              
      assign g-mo-tmmarg   = (g-mo-tmmrgsales  / g-mo-tmsales) * 100.     
   if g-lmo-tisales <> 0 then                                             
      assign g-lmo-timarg  = (g-lmo-timrgsales / g-lmo-tisales) * 100.    
   if g-lmo-tmsales <> 0 then                                             
      assign g-lmo-tmmarg  = (g-lmo-tmmrgsales / g-lmo-tmsales) * 100.    
   if g-ytd-tisales <> 0 then                                             
      assign g-ytd-timarg  = (g-ytd-timrgsales / g-ytd-tisales) * 100.    
   if g-ytd-tmsales <> 0 then                                             
      assign g-ytd-tmmarg  = (g-ytd-tmmrgsales / g-ytd-tmsales) * 100.    

   page.
   
   display g-mo-tisales
           g-mo-tmmarg
           g-mo-timarg
           g-lmo-tisales
           g-lmo-tmmarg
           g-lmo-timarg
           g-ytd-tisales
           g-ytd-tmmarg
           g-ytd-timarg
           with frame f-grand.
   
