/* SX 55 ???
   f-oere.i 1.1 01/03/98 */
/* f-oere.i 1.7 08/25/97 */
/*h*****************************************************************************
  INCLUDE      : f-oere.i
  DESCRIPTION  : Frames for OERE (OE Exception Report)
  USED ONCE?   : Yes (oered.p)
  AUTHOR       : mwb
  DATE WRITTEN : 09/12/90
  CHANGES MADE :
    12/07/92 ntl; TB#  9041 Canadian tax change
    01/25/93 mms; TB#  9644
    06/26/95 ajw; TB# 18575 Nonstock/Special Kit Component Project (G3)
    10/11/95 mcd; TB# 18812-E5a 7.0 Rebates Enhancement (E5a) -
        Display figures in consideration of rebates on the Exception Report
        Added v-rebatety
    03/21/96 kr;  TB# 10246 Variables defined in form, moved out of form
    02/19/97 jl;  TB# 7241 (4.1) Special Price Costing. Changed s-speccostty to
        v-speccostty.
    08/25/97 kjb; TB# 22385 OERR totals do not include rebates
*******************************************************************************/

{&com}

/*tb 12406 03/21/96 kr; Variables defined in form */
def            var s-psttaxamt      as dec format "zzzzzzz9.99-"       no-undo.
def            var v-ordname        as c format "x(18)"                no-undo.
def            var v-sales          as de format ">>>>>>>>>9.99-"      no-undo.
def            var v-cstgds         as de format ">>>>>>>>>9.99-"      no-undo.
def            var v-gross          as de format ">>>>>>>>>9.99-"      no-undo.
def            var v-marg           as de format ">>>>>>>>>9.99-"      no-undo.
def            var v-charge         as de format ">>>>>>>>>9.99-"      no-undo.
def            var v-down           as de format ">>>>>>>>>9.99-"      no-undo.
def            var v-totord         as de format ">>>>>>>>>9.99-"      no-undo.

/* title line for order line */
form  
    "Order #/"                                      at  2
    "Tkn"                                           at  37
    "Whse/"                                         at  41
    "SlsIn/"                                        at  47
    "----- Dates -----"                             at  54
    s-taxhdg1                                       at  86
    "Net Sale/"                                     at  99 
    
    skip

/*  "Order #"                                       at   3 */
    "CSR"                                           at   3
    "Customer #"                                    at  13
    "Ty Stg Ap  By"                                 at  27
    "SlsOut"                                        at  47
    "Entered  Req Ship"                             at  54
    "Addon Amt"                                     at  72
    s-taxhdg2                                       at  86
    "Tot Ord Val"                                   at  96
    "Margin"                                        at 115
    "Margin %"                                      at 123 

    skip
with frame f-oerea no-box no-labels page-top width 132.

/* line for order line */
form  
    oeeh.orderno                                    at   1
    "-"                                             at   8
    oeeh.ordersuf                                   at   9
    oeeh.notesfl                                    at  11
    v-cust                                          at  13
    oeeh.transtype                                  at  27
    s-stagecd                                       at  30
    oeeh.approvty                                   at  34
    oeeh.takenby                                    at  37
    oeeh.whse                                       at  42
    oeeh.slsrepin                                   at  48
    oeeh.enterdt                                    at  54
    oeeh.reqshipdt                                  at  63
    s-addon                                         at  72
    s-tax                                           at  82
    s-netsale                                       at  96
    s-margin                                        at 110
    s-margpct                                       at 123
    v-rebatety                                      at 131
    s-slstype                                       at   3
    s-rushfl                                        at   7
    s-lookupnm                                      at  13
    oeeh.slsrepout                                  at  48
    v-auth                                          at  60
    oeeh.wtauth                                     at  75
    s-psttaxamt                                     at  83          
    oeeh.totlineord                                 at  96
with frame f-oereb no-box no-labels down width 132.

/* title line for totals by order type */
/*tb 22385 08/25/97 kjb; Modified column label frame to allow for rebate
    indicator */
form 
    skip(1)

    "Cost Of"                                       at  47 

    skip

    "Tot By Order Type:"                            at   1
    "Count"                                         at  22
    "Net Sales"                                     at  31
    "Goods Sold"                                    at  45
    "Margin"                                        at  64
    "Margin %"                                      at  77
    "COD Charge"                                    at  91
    "Downpayments"                                  at 104
    "Tot Ord Val"                                   at 120 
    
    skip

    "----------------------------------------"      at  20
    "----------------------------------------"      at  60
    "---------------------------------"             at 100
with frame f-oeref no-box no-labels width 132.

/* detail line for totals by order so,do,fo,st,cs,rm,cr,bl,br,qu */
/*tb 22385 08/25/97 kjb; Modified totals detail frame to allow for rebate
    indicator */
form  
    v-ordname                                       at   1
    v-count                                         at  21
    v-sales                                         at  28
    v-cstgds                                        at  43
    v-gross                                         at  58
    v-marg                                          at  73
    v-rebatefl                                      at  87
    v-charge                                        at  89
    v-down                                          at 104
    v-totord                                        at 119
with frame f-oeref1 no-box no-labels down width 132.

/* grand total line for all order types */
/*tb 22385 08/25/97 kjb; Modified grand total line frame to allow for rebate
    indicator */
form 
    "------- --------------"                        at  20
    "-------------- -------------- --------------"  at  43
    "-------------- -------------- --------------"  at  89 

    skip

    t-tocount                                       at  20
    t-tosales                                       at  27
    t-tocstgds                                      at  42
    t-togross                                       at  57
    t-tomarg                                        at  72
    t-torebatefl                                    at  87
    t-tocharge                                      at  88
    t-todown                                        at 103
    t-tototord                                      at 118 

    skip(1)

    "Lost Business Lines:"                          at   1
    t-lostcnt                                       at  21
    t-lostnet                                       at  27
    t-lostord                                       at 118
    
    skip(1)
    
    s-csrfield                                      at 1
    s-drilldown                                     at 19
    s-ddvalue                                       at 25
with frame f-oeref2 no-box no-labels width 132.

/{&com}* */

/* title for line items if in slsrep or prod order */
form 
    "Order #/"                                      at   3
    "Customer #/"                                   at  13
    "Type/"                                         at  28
    "Product/"                                      at  38
    "Qty Ord/"                                      at  66
    "Sls In/"                                       at  75
    "Price-Calc/"                                   at  84
    "Unit/"                                         at 100
    "CSR"                                           at   3
    "TknBy"                                         at   7
    "Name"                                          at  13
    "Stg"                                           at  28
    "Ln# N"                                         at  32
    "Description"                                   at  38
    "Qty Ship"                                      at  65
    "Sls Out"                                       at  75
    "WO/Spec Amt"                                   at  83
    "Cost"                                          at 100
    "Discount-Lev"                                  at 106
    "Margin %"                                      at 120
    "BO?"                                           at 130 

    skip
with frame f-oereg no-box no-labels page-top width 132.

/* detail line for line items if in slsrep or prod order */
form  
    oeel.orderno                                    at   1
    "-"                                             at   8
    oeel.ordersuf                                   at   9
    oeeh.notesfl                                    at  11
    v-cust                                          at  13
    oeel.transtype                                  at  29
    oeel.lineno                                     at  32
    oeel.specnstype                                 at  36
    v-prod                                          at  38
    oeel.qtyord                                     at  64
    s-retqtyord                                     at  74
    oeel.slsrepin                                   at  76
    oeel.price                                      at  80
    oeel.priceclty                                  at  93
    v-prccostper                                    at  95
    oeel.unit                                       at 100
    s-discamt                                       at 105
    s-disccd                                        at 118
    s-marginpct                                     at 120
    v-rebatety                                      at 128
    s-backorder                                     at 130
    s-slstype                                       at   3
    oeeh.takenby                                    at   7
    s-lookupnm                                      at  13
    s-stagecd                                       at  29              
    oeel.rushfl                                     at  33
    s-descrip                                       at  38
    oeel.qtyship                                    at  64
    s-retqtyship                                    at  74
    oeel.slsrepout                                  at  76
    v-mardiscoth                                    at  80
    oeel.prodcost                                   at  95
    v-speccostty                                    at 108
    v-coretype                                      at 111
    v-corecharge                                    at 122
with frame f-oereh no-box no-labels down width 132.

/*tb 18575 06/26/95 ajw; Nonstock/Special Kit Component.  New form for OEELK's*/
form /*e Title for Kit Component line items if in slsrep or prod order */
    "Order #/"                                      at   3
    "Customer #/"                                   at  13
    "Type/"                                         at  28
    "Kit Product/"                                  at  45
    "Component/"                                    at  71
    "Qty Ord/"                                      at  97
    "Price/"                                        at 114
    "Unit/"                                         at 125
    "Tkn By"                                        at   4
    "Name"                                          at  13
    "Stg"                                           at  28
    "Ln# Seq#  N"                                   at  33
    "Description"                                   at  45
    "Description"                                   at  71
    "Qty Ship"                                      at  97
    "Cost"                                          at 115
with frame f-oerekh no-box no-labels page-top width 132.

/*tb 18575 06/26/95 ajw; Nonstock/Special Kit Component.  New form for OEELK's*/
/* Detail line for Kit Component lines if in slsrep or prod order */
form  
    oeelk.orderno                                   at   1
    "-"                                             at   8
    oeelk.ordersuf                                  at   9
    oeeh.notesfl                                    at  11
    v-cust                                          at  13
    oeel.transtype                                  at  29
    oeelk.lineno                                    at  32
    oeelk.seqno                                     at  38
    oeelk.specnstype                                at  43
    v-prod                                          at  45
    v-cprod                                         at  71
    oeelk.qtyord                                    at  96
    oeelk.price                                     at 110
    oeelk.unit                                      at 125
    oeeh.takenby                                    at   6
    s-lookupnm                                      at  13
    s-stagecd                                       at  29
    s-descrip                                       at  45
    s-cdescrip                                      at  71
    oeelk.qtyship                                   at  96
    oeelk.prodcost                                  at 110
with frame f-oerekl no-box no-labels down width 132.

form
    oeel.promisedt                                  colon 21    label "Promised"
    oeeh.shipdt                                     colon 45    label "Shipped"
with frame f-oerej no-box side-labels width 132.

{&com}
/* Addtional line if Floor Plan order */
form 
    v-fpcustno                              colon 22    label "Invoiced To Cust"
    s-fplookup                              at    38 no-label
with frame f-oerefp no-box side-labels width 132.
/{&com}* */

