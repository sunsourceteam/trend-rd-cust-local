                        
  if z-icsp.prodcat < zzl_begcat or
     z-icsp.prodcat > zzl_endcat then
     next dataloop.

  assign 
    t-whse = z-icsw.whse
    t-prod = z-icsw.prod
    t-buyer = ""
    t-prodline = z-icsw.prodline
    t-vendor = z-icsw.arpvend
    t-search = 0.
  
 
 
  if z-icsp.kittype = "p" and z-icsw.arpvend = 0 and z-icsw.prodline = "" then
    do:
    find first kpsk where kpsk.cono = g-cono and
                          kpsk.prod = z-icsw.prod no-lock no-error.
    if avail kpsk then
      do:                    
      find first w-icsw where w-icsw.cono = g-cono and
                              w-icsw.prod = z-icsw.prod no-lock no-error.
      if avail w-icsw then
        assign 
          t-whse = w-icsw.whse
          t-prod = w-icsw.prod
          t-buyer = ""
          t-prodline = w-icsw.prodline
          t-vendor = w-icsw.arpvend
          t-search = 0.
      end.
    end.
    
  
  {p-zfindbuygne2.i
     t-whse
     t-prod
     t-buyer
     t-vendor
     t-prodline
     t-search
     }
  
  
  if t-buyer = "" then
     t-buyer = substring(z-icsd.user3,15,4).
  
  if t-prodline ge b-pline and
     t-prodline le e-pline then
     t-search = 0.
  else
     next dataloop.
  
  run zdmdwhse (input z-icsw.whse,input z-icsw.prod).
  
  create buyicsw.
  assign
    buyicsw.buyer        = t-buyer
    buyicsw.prod         = z-icsw.prod
    buyicsw.whse         = z-icsw.whse
    buyicsw.statustype   = z-icsw.statustype
    buyicsw.kittype      = z-icsp.kittype
    buyicsw.um           = z-icsw.unitbuy
    buyicsw.vendor       = t-vendor
    buyicsw.prodline     = t-prodline
    buyicsw.qtyavbl      = (z-icsw.qtyonhand
                            - z-icsw.qtyreservd 
                            - z-icsw.qtycommit) 
    buyicsw.onhand       = z-icsw.qtyonhand
    buyicsw.qtycommit    = z-icsw.qtycommit
    buyicsw.qtyreserved  = z-icsw.qtyreservd
    buyicsw.oo           = z-icsw.qtyonorder
    buyicsw.co           = z-icsw.qtybo
    buyicsw.ytdissue     = z-icsw.issueunytd
    buyicsw.ptdissue     = z-icsw.usagerate
    buyicsw.leadtime     = z-icsw.leadtmavg
    buyicsw.min          = z-icsw.orderpt
    buyicsw.ordqty       = z-icsw.ordqtyin
    buyicsw.max          = z-icsw.linept
    buyicsw.replcst      = z-icsw.replcost
    buyicsw.method       = z-icsw.ordcalcty
    buyicsw.avgcost      = z-icsw.avgcost
    buyicsw.listprc      = z-icsw.listprice
    buyicsw.zdescr       = z-icsp.descrip[1].
    do v-inxv = 1 to 7:
      buyicsw.whsestuff[v-inxv]   = x-whseqty[v-inxv].
    end.
 