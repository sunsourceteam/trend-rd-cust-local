


procedure introduce_stats:

if v-testmode then
   v-printdir = "/mod20b/workfiles/".
assign v-process = false.





if search (v-printdir + "tbxd" + string(i-turnmo,"99") + ".smk") <> ? then
  do:
  v-process = true.

  output stream zprd to 
     value(v-printdir + "tbxd" + string(i-turnmo,"99") + ".sxk").  input stream xprd from 
     value(v-printdir + "tbxd" + string(i-turnmo,"99") + ".smk").


  repeat:
    readkey stream xprd.
    if lastkey = -2 then leave.
    if lastkey = -1 then next.
    if lastkey = 0 then
      put stream zprd control NULL.
    if lastkey = 13 then
      put stream zprd control chr(13) chr(10).
    else
      put stream zprd control chr(lastkey).

  end.    
input  stream xprd close.
output stream zprd close.

os-command value("chmod 666 " +
  v-printdir + "tbxd" + string(i-turnmo,"99") + ".smk").
os-command
value("cp " + 
       v-printdir + "tbxd" + string(i-turnmo,"99") + ".smk" + " " +
       v-printdir + "tbxd" + string(i-turnmo,"99") + ".smk.bk").       
os-command 
   value("rm " + 
         v-printdir + "tbxd" + string(i-turnmo,"99") + ".smk").
end.


if v-process = true then
  do:
  input stream xprd from value
    (v-printdir + "tbxd" + string(i-turnmo,"99") + ".sxk").
        

  assign j-x = 0.

  repeat:

    assign 
      v-region     = ""
      v-district   = ""
      v-pcat       = ""
      v-whse       = ""
      v-vend       = 0
      v-cust       = 0
      v-prod       = ""
      v-ytd        = 0
      v-lytd       = 0
      v-ytdxsls    = 0
      v-lytdxsls   = 0
      v-ytdxmgn    = 0
      v-lytdxmgn   = 0
      v-skip       = ""
      v-cust       = 0
      v-shipto     = ""
      v-lsttrans   = ""
      v-lstsale    = 0.
    if j-x = 0 then 
      do:
      import stream xprd delimiter "~011"
        v-skip.
      import stream xprd delimiter "~011"
        v-skip.
      j-x = 2.
      end.
    
    import stream xprd delimiter "~011"
     v-skip
     v-region
     v-district
     v-whse
     v-cust
     v-skip
     v-shipto
     v-pcat
     v-skip
     v-prod
     v-skip    /* Month 1 Sales             */
     v-skip    /* Month 1 Margin            */
     v-skip    /* Month 2 Sales             */
     v-skip    /* Month 2 Margin            */
     v-skip    /* Month 3 Sales             */
     v-skip    /* Month 3 Margin            */
     v-ytdxsls
     v-ytdxmgn  /* Current YTD margin        */
     v-lytdxsls /* Previous YTD Sales        */
     v-lytdxmgn /* Previous YTD Margin       */
     v-skip     /* Variance Sales            */
     v-skip     /* Variance Difference %     */
     v-skip     /* Variance Margin           */
     v-skip     /* LYTD Sales                */
     v-skip    /* LYTD Margin                */
     v-skip    /* Backlog                    */
     v-skip    /* Backlog Margin             */
     v-lstsale
     v-lsttrans.

   assign v-ytd  = v-ytdxsls - v-ytdxmgn
          v-lytd = v-lytdxsls - v-lytdxmgn.

   j-x = j-x + 1.
   if j-x < 3 or v-region = "final" then
     next.    /* Get past the Excel Header portion of the file */

      
   if v-region = "" or
      substring(v-region,1,1) = "0" or 
      substring(v-region,1,1) = "z" or
      substring(v-region,1,1) = "x" or 
      v-district = "n500" then
     next.
 
   if length(v-prod) > 25 then
     next.
   
   find spprd where
        spprd.whse   = v-whse and
        spprd.custno = v-cust and
        spprd.shipto = v-shipto and
        spprd.prod = v-prod and
        spprd.region = v-region and
        spprd.district = substring(v-district,2,3) no-lock no-error.
   if avail spprd then
     do:
     assign spprd.sales  = spprd.sales /* + v-ytd  */      + v-lytd
            spprd.xsales = spprd.xsales + /* v-ytdxsls + */  v-lytdxsls
            spprd.xmgn  = spprd.xmgn + /* v-ytdxmgn + */     v-lytdxmgn.

     end.
   else
     do:
     create spprd.
     assign spprd.custno = v-cust
            spprd.whse   = v-whse
            spprd.xused  = false
            spprd.shipto = v-shipto
            spprd.region = v-region
            spprd.prodcat = v-pcat
            spprd.vendno  = 0
            spprd.district = substring(v-district,2,3)
            spprd.prod = v-prod
            spprd.sales = /* v-ytd + */      v-lytd
            spprd.xsales = /* v-ytdxsls + */ v-lytdxsls
            spprd.xmgn = /* v-ytdxmgn +  */  v-lytdxmgn
            spprd.lstsale  = v-lstsale
            spprd.lsttrans = v-lsttrans
            spprd.xpct = 0.
     end.
     
   find t-spprd where
        t-spprd.whse = v-whse and
        t-spprd.prod = v-prod no-lock no-error.
   if avail t-spprd then
     do:
     assign t-spprd.sales = t-spprd.sales /* + v-ytd */ + v-lytd.
     end.
   else
     do:
     create t-spprd.
     assign
            t-spprd.prod = v-whse 
            t-spprd.prod  = v-prod
            t-spprd.sales = /* v-ytd +  */  v-lytd.
     end.
   end.   
   
   input stream xprd close.   
 
   end.

if v-process = true then
  do:
  output  stream xprd to 
     value (v-printdir + "tbxdprods" + string(i-turnmo,"99") + ".sxk").
  os-command value("chmod 666 " + 
                   v-printdir + "tbxd" + string(i-turnmo,"99") + ".sxk").


  for each spprd no-lock:
    export stream xprd spprd.
  end.  
  
 output stream xprd close.
 end.
 if v-process = false and 
    search (v-printdir + "tbetr" + string(i-turnmo,"99") + ".sxk") <> ? then
  do:
  input stream xprd from value
     (v-printdir + "tbetr" + string(i-turnmo,"99") + ".sxk").
  repeat:
  create wuyers.
  import stream xprd wuyers except wqobso wasofdt .

   
  assign
     wuyers.wqobso = 
        wuyers.wq60   + 
        wuyers.wqg60  + 
        wuyers.wqns12 + 
        wuyers.wqns24
     wuyers.wasofd = ?.

  
  
  
  if p-currentinfo then  
      do:
      h-prodline = "".
      h-buyer = "".
      h-searchlevel = 0.
      if h-buyer = "" then
        do:
        {p-zfindbuygne2.i
               wuyers.wwhse
               wuyers.wprod
               h-buyer
               h-vendor
               h-prodline
               h-searchlevel}
               
        end.
           
      if h-vendor = 0 then
        run vendor_lookup(input wuyers.wwhse,
                          input wuyers.wprod).
      if h-prodline = ""  then
        assign
          h-prodline = "misc".

  
      find sasta where sasta.cono = g-cono
                   and sasta.codeiden = "B"
                   and sasta.codeval = h-buyer
        no-lock no-error.

      assign d-name = if avail sasta then 
                        sasta.descrip 
                      else 
                        "BuyerNotFound".
  
      find oimsp where oimsp.person = h-buyer no-lock no-error.
      if avail oimsp then
        h-person = oimsp.responsible.
      else   
        h-person = "NTFND".

      assign
         wuyers.wname            = d-name
         wuyers.wpline           = h-prodline
         wuyers.wdept            = h-person
         wuyers.wbuyer           = h-buyer
         wuyers.wvendor          = h-vendor.
        
      end.    
  
  run createranges.
  end.
  input stream xprd close.

  end.
 else
 if v-process = false then
   do:
   v-process = true.
   input stream xprd from value
     (v-printdir + "tbxdprods" + string(i-turnmo,"99") + ".sxk").
   repeat:
     create spprd.
     import stream xprd spprd.
   end. 


   for each spprd no-lock:
     find t-spprd where
          t-spprd.whse = spprd.whse and
          t-spprd.prod = spprd.prod no-lock no-error.
     if avail t-spprd then
       do:
       assign t-spprd.sales = t-spprd.sales + spprd.sales.
       end.
     else
       do:
       create t-spprd.
       assign
              t-spprd.whse  = spprd.whse
              t-spprd.prod  = spprd.prod
              t-spprd.sales = spprd.sales.
       end.
   end.   
   end.

 end.



