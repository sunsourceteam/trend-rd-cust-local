/* poehbzz.sch */

if v-bufferedsearch then do:
  assign sr-prod  = if substring(v-start,1,2) = "P%" then
                      substring(v-start,3,(length(v-start) - 2))
                    else
                      v-start.

  if v-vend-accum <> 0 then do:
    assign v-prodsrcha = sr-prod.
    /* UPC added code */
    run improvise_product ( input v-vend-accum,
                            input v-vend-accum-edi,
                            input "Product", 
                            input-output v-prodsrcha).
    /* UPC added code */
    if not can-find(first tpoehb where 
                    tpoehb.prod begins sr-prod no-lock) and
       not can-find(first tpoehb where 
                    tpoehb.vprod begins sr-prod no-lock) and
       not can-find(first tpoehb where 
                    tpoehb.prod begins v-prodsrcha no-lock) then
          
      run vendor_special_information (input v-vend-accum,
         /* SDI DANFOSS 11/14/05    */
                                      input v-vend-accum-edi,
         /* SDI DANFOSS 11/14/05    */
                                      input "Product",
                                      input-output sr-prod).
  end.
         
        
  assign v-srchfnd  = false
         v-bufferedsearch = false.
        
  find first srch where srch.prod begins sr-prod no-lock no-error.
        
  if avail srch then do:  
    hide frame f-zzx.
    hide frame f-zpoinfo2.
    assign confirm = true
        v-searchfl = false
          v-nextfl = true
          v-curpos = 1
          v-recid  = 0.
    clear frame f-zzx all no-pause.
    input clear.
    readkey pause 0.
    next top.
  end.
  else do:
    hide frame f-zzx. 
    hide frame f-zpoinfo2.
    clear frame f-zzx all no-pause.
    input clear.
    readkey pause 0.
    next dsply.
  end.
end.
