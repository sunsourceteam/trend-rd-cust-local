/* a-oeepa1.i 1.2 04/09/98 */
/*h*****************************************************************************
  INCLUDE      : a-oeepa1.i
  DESCRIPTION  :
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    04/03/92 pap; TB# 6219 Line DO - add direct order handling
    11/11/92 mms; TB# 8561 Display product surcharge label from ICAO
    04/09/98 bm;  TB# 24800 Canadian tax change. Chgd lit49a to GST/HST
*******************************************************************************/

if v-headingfl = yes then assign
    /* SX55 */
    /*SI02*/
    {1}litattn = if oeeh.divno = 40 then "    Attn: " + oeeh.placedby else ""  
    /*SI02 end*/

    {1}cancel = if oeeh.stagecd = 9 then "*** C A N C E L L E D ***"
                else ""
    /* SX55
    {1}lit1b  = "UPC Vendor   Invoice Date     Order #"
    */
    {1}lit1b  = "                              Quote #  "  /* (ITB# 1) */
    {1}lit2a  = if can-do("ra,rm":u,oeeh.transtype) then
                    "===================================================="
                else if oeeh.transtype = "cr":u then
                    "============================================="
                else ""
    {1}lit3a  = "  Cust #:"
    {1}lit3b  = "PO Date   PO #                   Page #"
    {1}lit6a  = "Bill To:"
    {1}lit6b  = "Correspondence To:"
    /* Sx55 */
    {1}lit6bb = "(ONLY)"          
    {1}lit8a  = if g-country = "ca" then "PST Lic#:" else ""
    {1}lit9a  = if g-country = "ca" then "GST Reg#:" else ""
    {1}lit11a = "Ship To:"
    {1}lit11b = "Instructions"
    {1}lit12a = "Ship Point"
    {1}lit12b = "Via"
    /* SX55 */
    /*  {1}lit12c = "Shipped"        (ITB# 1)  */
    {1}lit12c = "To Ship By"    /*  (ITB# 1)  */
    {1}lit12d = "Terms"
    {1}lit14a =
"    Product                   UPC      Quantity     Quantity     Quantity     "
    {1}lit14b =
"Qty.     Unit       Price     Discount         Amount    "
    {1}lit15a =
"Ln# And Description          Item#     Ordered        B.O.       Shipped      "
    {1}lit15b =
" UM      Price       UM      Multiplier        (Net)     "
    {1}lit16a =
"------------------------------------------------------------------------------"
    {1}lit16b =
"-------------------------------------------------------".

assign
    {1}lit1aa = if v-headingfl = yes then "Document: " else " "
    {1}lit1a  = if oeeh.transtype = "rm":u then
                    "Order Acknowledgement - Return Merchandise"
                else if oeeh.transtype = "cr":u then
                    "Order Acknowledgement - Correction"
                else if oeeh.transtype = "ra":u then
                    "Order Acknowledgement - Received On Account"
                else if oeeh.transtype = "qu":u then
                    "Order Acknowledgement - Quote Order"
                else "Order Acknowledgement"
     {1}lit1a = if oeeh.transtype = "bl" then
                    "Order Acknowledgement - Blanket Order"
                else if oeeh.transtype = "do":u then
                    "Order Acknowledgement - Direct Order"
                else if oeeh.transtype = "fo":u then
                    "Order Acknowledgement - Future Order"
                else if oeeh.transtype = "st":u then
                    "Order Acknowledgement - Standing Order"
                else if oeeh.transtype = "br":u and oeeh.stagecd <> 0 then
                    "Order Acknowledgement - Blanket Release"
                else if oeeh.transtype = "br":u and oeeh.stagecd = 0 then
                    "Order Acknowledgement - Blanket Release (Entered)"
                else {1}lit1a
    {1}lit40a = "Lines Total"
    {1}lit40c = "Qty Shipped Total"
    {1}lit40d = if can-do("bl,br",oeeh.transtype) and
                    oeeh.lumpbillfl = yes then
                "Amount Billed     " else
                "Total             "
    {1}lit41a = "Order Discount    "
    {1}lit42a = "Other Discount    "
    {1}lit43a = "Core Charge       "
    {1}lit44a = v-icdatclabel
    {1}lit45a = "Taxes             "
    {1}lit46a = "Downpayment       "
    {1}lit46b = "Payment           "
    {1}lit47a = "Order Total       "
    {1}lit47b = "Payment           "
    {1}lit48a = "Continued"
    {1}lit49a = "G.S.T./H.S.T.     "
    {1}lit50a = "P.S.T.            "
    {1}lit53a = "Restock Amount    ".



