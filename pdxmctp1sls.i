/*****************************************************************************
   
 pdxmctp1sls.i  - Contract Price Matrix Reporting - 
                  This include is called by pdxmc.p.
                  Sorts by Salesrep
                  Prints the report
                
*****************************************************************************/
  assign wk-ytd-sales  = 0
         wk-pytd-sales = 0.
           
  
  for each matrix use-index k-type1  where matrix.customer <> 0 and
                                           /*
                                           matrix.product <> "" and
                                           */
                                           matrix.ytd-sales <> 0
                                           no-lock
                                              
      break by matrix.region
            /*
            by (if substring(e-region,2,3) = "999" then matrix.blank-sort
                else matrix.district)
            */
            by matrix.salesrep
            by matrix.ytd-sales descending
            by matrix.customer
            by matrix.ship-to
            by matrix.product:

      /*
      if matrix.c-sales = 0 or
         matrix.i-sales = 0 then next.
      */
      
      assign i-mgn = 0
             c-mgn = 0
             g-mgn = 0.
      
      if (last-of(matrix.region)) then do:
         assign counter = 0.
         PAGE.
      end.
      
      /*
      if (first-of(if substring(e-region,2,3) = "999" then matrix.blank-sort
          else matrix.district)) then do:
      */
      if (first-of(matrix.salesrep)) then do:
          if not first-page then PAGE.
          else
             assign first-page = no.
       end.

      if (matrix.i-sales - matrix.i-disc) <> 0 then
         assign i-mgn =                 /*  (net-sales - cost) / net-sales)  */
            (((matrix.i-sales - matrix.i-disc) - matrix.i-cost) 
                                     / (matrix.i-sales - matrix.i-disc)) * 100

                w-tot-isales = w-tot-isales + (matrix.i-sales - matrix.i-disc)
                w-isales     = w-isales + (matrix.i-sales - matrix.i-disc)
                w-icost      = w-icost  + matrix.i-cost
                w-tot-icost  = w-tot-icost  + matrix.i-cost.

      else
         assign i-mgn = 0.
      
        if (matrix.c-sales - matrix.c-disc) <> 0 then
         assign c-mgn =     /* c-sales already has discount considered */
                (matrix.c-sales - matrix.c-cost) / matrix.c-sales * 100

                w-tot-csales = w-tot-csales + matrix.c-sales
                w-csales     = w-csales + matrix.c-sales
                w-ccost      = w-ccost  + matrix.c-cost
                w-tot-ccost  = w-tot-ccost  + matrix.c-cost.

      else
         assign c-mgn = 0.
      
      if (matrix.g-sales - matrix.g-disc) <> 0 then
         assign g-mgn =       /* g-sales already has discount considered */
                (matrix.g-sales - matrix.g-cost) / matrix.g-sales * 100

                w-tot-gsales = w-tot-gsales + matrix.g-sales
                w-gsales     = w-gsales + matrix.g-sales
                w-gcost      = w-gcost  + matrix.g-cost
                w-tot-gcost  = w-tot-gcost  + matrix.g-cost.

      else
         assign g-mgn = 0.
         
      if (last-of(matrix.customer)) then
          assign wk-ytd-sales  = wk-ytd-sales +  matrix.ytd-sales
                 wk-pytd-sales = wk-pytd-sales + matrix.pytd-sales.
                 
      if (last-of(matrix.product)) then
          assign wk-prod-ytd-sales = wk-prod-ytd-sales + matrix.prod-ytd-sales.
           
/*
      if matrix.customer <> wk-customer or counter > 32 then do:
         assign counter = 1.
         assign wk-customer = matrix.customer.
*/
         display matrix.customer
                 matrix.ship-to
                 matrix.name
                 matrix.salesrep
                 matrix.cprice-type
                 matrix.ytd-sales
                 matrix.pytd-sales
                 matrix.prod-ytd-sales
                 matrix.product
                 matrix.pprice-type
                 matrix.mult-prc
                 matrix.c-percent
                 matrix.pd-record
                 matrix.exp-date
                 matrix.upd-date
                 i-mgn
                 c-mgn
                 g-mgn
                 with frame f-type1-detail.
                 down with frame f-type1-detail.
/*
      end.
      else do:
         display matrix.prod-ytd-sales
                 matrix.product
                 matrix.pprice-type
                 matrix.mult-prc
                 matrix.c-percent
                 matrix.pd-record
                 matrix.exp-date
                 matrix.upd-date
                 i-mgn
                 c-mgn
                 g-mgn
                 with frame f-type1-detail.     
                 down with frame f-type1-detail.
                 assign counter = counter + 1.
      end.
*/
      
      /*
      if (last-of(if substring(e-region,2,3) = "999" then matrix.blank-sort
                  else matrix.district)) then do:
      */
      if (last-of(salesrep)) then do:

         assign  wk-rgn   = matrix.salesrep
                 wk-i-mrg = (w-isales - w-icost) / w-isales * 100
                 wk-c-mrg = (w-csales - w-ccost) / w-csales * 100
                 wk-g-mrg = (w-gsales - w-gcost) / w-gsales * 100.


         display wk-rgn
                 wk-ytd-sales
                 wk-pytd-sales
                 wk-prod-ytd-sales
                 wk-i-mrg
                 wk-c-mrg
                 wk-g-mrg
                 with frame f-1-totals.
                 down with frame f-1-totals.
                 
         assign  wk-grand-ytd-sales  = wk-grand-ytd-sales  + wk-ytd-sales
                 wk-grand-pytd-sales = wk-grand-pytd-sales + wk-pytd-sales
                 wk-grand-prod-ytd-sales = wk-grand-prod-ytd-sales +
                                           wk-prod-ytd-sales.
         assign  wk-ytd-sales = 0
                 wk-pytd-sales = 0
                 wk-prod-ytd-sales = 0
                 wk-i-mrg = 0
                 wk-c-mrg = 0
                 wk-g-mrg = 0
                 w-isales = 0
                 w-csales = 0
                 w-gsales = 0
                 w-icost  = 0
                 w-ccost  = 0
                 w-gcost  = 0
                 wk-rgn    = ""
                 counter = 0.    
         page.
      end.
      
      
      
  end.   /* for each matrix */    

  assign wk-i-mrg = ((w-tot-isales - w-tot-icost) / w-tot-isales) * 100
         wk-c-mrg = ((w-tot-csales - w-tot-ccost) / w-tot-csales) * 100
         wk-g-mrg = ((w-tot-gsales - w-tot-gcost) / w-tot-gsales) * 100.


  display wk-grand-ytd-sales
          wk-grand-pytd-sales
          wk-grand-prod-ytd-sales
          wk-i-mrg
          wk-c-mrg
          wk-g-mrg
          with frame f-1-grand.
          down with frame f-1-grand.
