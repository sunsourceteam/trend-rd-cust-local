/*******************************************************************************
  PROCEDURE     : p-can-arroll.i
  DESCRIPTION   : A/R Standard Statement Print Include for
                : Canadian Exchange Rate Calculations.
  AUTHOR        : SunSource TA.
  DATE WRITTEN  : 01/31/01
  CHANGES MADE  : 
******************************************************************************/                                   
find b-sasc where b-sasc.cono = g-cono no-lock no-error.

do i = 1 to 4:
   assign s-perfirstdt[i] = if i = 1 then
                               today
                            else 
                               s-perfirstdt[i - 1] - (b-sasc.arperdays[i] + 1)
          s-perlastdt[i]  = if i = 1 then
                               today + (b-sasc.arperdays[1])
                            else                            
                               s-perfirstdt[i - 1] - 1.
 /*  message s-perfirstdt[i]  " " s-perlastdt[i].
     pause.   */
end.

/***** standard transactions       *************/
for each aret where
    aret.cono   = g-cono    and 
    aret.custno = {&custno} and 
    aret.statustype = true  and
    aret.transcd    = 11    and 
    aret.sourcecd = 0 use-index k-invno no-lock:

    find first oeeh where oeeh.cono = g-cono and 
                                 oeeh.orderno = aret.invno and 
                                 oeeh.ordersuf = aret.invsuf
                                 no-lock no-error.
    if avail oeeh then 
      do:
      if oeeh.user6 ne 0 then 
        assign v-exrate = oeeh.user6.
      else    
        assign v-exrate = 1.00.
    end.
    else
      assign v-exrate = 1.
 
 /*  assign 
        s-count = s-count + 1
        {&aret}period = if not aret.disputefl then 
                           1
                        else 
                           aret.period.
        if not aret.disputefl then 
           do i = 1 to 4:
              if aret.duedt < s-perfirstdt[i] then
                 {&aret}period = i + 1.
           end.
        assign  {&aret}period = if {&aret}period = 1 and aret.duedt >
                               s-perlastdt[1] and not aret.disputefl then
                                 0
                               else
                                 {&aret}period 
        if {&aret}period ne 0 then             
           s-periodbal[{&aret}period] = s-periodbal[{&aret}period] +
           (aret.amount * v-exrate).
        if {&aret}period ge 2 and aret.disputefl then 
 */
        if sapb.optvalue[8]  = "yes" then 
           assign s-futinvbal = s-futinvbal +
                             if aret.period = 0 then 
                               ((aret.amount - aret.paymtamt) * v-exrate)
                             else
                                0.
        if aret.period ne 0 then 
           do:
            v-dispbal[aret.period] = v-dispbal[aret.period] + 
                            ((aret.amount - aret.paymtamt) * v-exrate).
            s-totalbal = s-totalbal + 
                            ((aret.amount - aret.paymtamt) * v-exrate).
           end.
   
end. /***** for each ****/
/*
if g-operinit = "das1" then
  do:
  message "in p-can-arroll.i 1" s-totalbal. pause.
end.
*/

/****** misc credit records  **********/
/* das 05/21/14 - Added a check for option 16 to Supress Misc Credits */
if x-nocred = no then
  do:
  for each aret where
    aret.cono   = g-cono    and 
    aret.custno = {&custno} and 
    aret.statustype = true  and
    aret.transcd    = 5      and 
    aret.sourcecd = 0 use-index k-invno no-lock:

    find first oeeh where oeeh.cono = g-cono and 
                                 oeeh.orderno = aret.invno and 
                                 oeeh.ordersuf = aret.invsuf
                                 no-lock no-error.
    if avail oeeh then 
      do:
      if oeeh.user6 ne 0 then 
        assign v-exrate = oeeh.user6.
      else    
        assign v-exrate = 1.00.
    end.
    else
      assign v-exrate = 1.
        
    if sapb.optvalue[8]  = "yes" then 
       assign s-futinvbal = s-futinvbal +
                         if aret.period = 0 then 
                            ((aret.amount - aret.paymtamt) * v-exrate)
                         else
                             0.
    if aret.period ne 0 then
       do:
         v-dispbal[aret.period] = v-dispbal[aret.period] + 
                 ((aret.amount - aret.paymtamt) * v-exrate).
         s-totalbal = s-totalbal + 
                            ((aret.amount - aret.paymtamt) * v-exrate).
       end.
  end. /**** for each ****/        
end. /* Option 16 is set to NO (reflect misc credits and unapplied cash) */
/*
if g-operinit = "das1" then
  do:
  message "in p-can-arroll.i 2" s-totalbal. pause.
end.
*/

/****** unapplied cash records  **********/
/* das 05/21/14 - Added a check for option 16 to Supress Unapplied Caxh */
if x-nocred = no then
  do:
  for each aret where
    aret.cono   = g-cono    and 
    aret.custno = {&custno} and 
    aret.statustype = true  and
    aret.transcd    = 3      and 
    aret.sourcecd = 0 use-index k-invno no-lock:

    find first oeeh where oeeh.cono = g-cono and 
                                 oeeh.orderno = aret.invno and 
                                 oeeh.ordersuf = aret.invsuf
                                 no-lock no-error.
    if avail oeeh then 
      do:
      if oeeh.user6 ne 0 then 
        assign v-exrate = oeeh.user6.
      else    
        assign v-exrate = 1.00.
    end.
    else
      assign v-exrate = 1.

    assign s-uncashbal = s-uncashbal +
                         if aret.period = 0 then 
                           ((aret.amount - aret.paymtamt) * v-exrate)
                         else
                           0.
    if aret.period ne 0 then
       do:
         v-dispbal[aret.period] = v-dispbal[aret.period] + 
                 ((aret.amount - aret.paymtamt) * v-exrate).
       end.

  end. /**** for each unapplied cash ****/        
  assign s-totalbal = s-totalbal + s-uncashbal.
end. /* Option 16 set to NO (reflect misc credits and unapplied cash) */
/*
if g-operinit = "das" then
  do:
  message "total fut " s-futinvbal "total uncash" s-uncashbal.
  pause.
end.
*/

/* das 05/21/14 - this is already done in p-ares5.i and 
                  option values are not getting checked */
/*s-totalbal  = s-totalbal + s-futinvbal + s-uncashbal.*/

do i = 1 to 5:
   assign 
        s-totalold = s-totalold + {&file}.periodbal[i] 
        s-totalnew = s-totalnew + s-periodbal[i].
  /*      {&updt}
        {&file}.periodbal[i] = s-periodbal[i]. */
end.
/*
assign s-totalold = s-totalold + {&file}.futinvbal
       s-totalnew = s-totalnew + s-futinvbal
       {&file}.futinvbal = s-futinvbal
       {&file}.lastagedt = {&date}.
*/       



