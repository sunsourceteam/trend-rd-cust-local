/*h*****************************************************************************
  PROCEDURE      : vaetg
  DESCRIPTION    : VA print parameters
  AUTHOR         : jms
  DATE WRITTEN   : 08/13/96
  CHANGES MADE   :
    08/13/96 jms; TB# 21254 (4A) Develop Value Add Module
    11/27/96 bao; TB# 21254 (21B) Use format "99" for string command when
        assigning sapbv.apinvno to g-vasuf
    12/10/96 jms; TB# 21254 (4A) Develop Value Add Module - add validation for
        seqno for internal print
    04/15/98 cm;  TB# 22068 SAPB record not always deleted
    08/24/98 bm;  TB# 24812 Add Rush Functionality type "IN" Lines.
        Use sapbv.apinvno positions 1 - 7 for vano instead sapbv.vendno
        Use sapbv.apinvno positions 9 - 10 for vasuf instead sapbv.apinvno
    11/26/02 rgm; TB# e15146 Modifications for rxserver print options
    11/13/03 blw; TB# L759   Integrate VA with TWL; Add auto-print for TWL
    04/05/04 bp ; TB# e19471 Add user hooks.
    10/26/05 cm;  TB# e23236 VA GUI migration
*******************************************************************************/

/* A-Auto Print D-Display P-Pick */
def input param v-type              as c format "x(1)"              no-undo.
/* A-Auto Pick Ticket (y/n)      */
def input param v-pick              as c format "x(1)"              no-undo.

{g-all.i}
{g-rptctl.i "new"}
{g-ic.i}
{g-oe.i}
{g-va.i}
{g-print.i}
{va.gva}

def            var s-pickprintfl    as logical                      no-undo.
def            var s-intrnprintfl   as logical                      no-undo.
def            var s-printernmpck   as c format "x(10)"             no-undo.
def            var s-printernmintrn as c format "x(10)"             no-undo.
def            var s-pickseqno      as i format "zz9"               no-undo.
def            var s-intrnseqno     as i format "zz9"               no-undo.
def            var v-pckokfl        as l                            no-undo.
def            var v-intokfl        as l                            no-undo.
def            var s-printer        as c format "x(10)"             no-undo.
def            var v-intptype       like sasp.ptype                 no-undo.
def            var v-extent         as i                            no-undo.
def            var v-custno         like oeeh.custno                no-undo.
def            var v-shipto         like oeeh.shipto                no-undo.
def            var v-sctntype       like vaes.sctntype              no-undo.
def            var v-sapbvid        as recid                        no-undo.
def            var cReportPrefix    as c                            no-undo.

def buffer b-vaeh       for vaeh.
def buffer b-sapb       for sapb.

/*to correct VSIFAX problem */
{valmsg.gas &string = "4035,4052"}

{vaetg.lfo}

/*o Initialize all the global F10 print variables */
assign
    s-pickprintfl     = no
    s-printernmpck    = ""
    s-intrnprintfl    = no
    s-printernmintrn  = "".

/*u User Hook */
{vaetg.z99 &user_def_vars = "*"}

pause 0 before-hide.

/*e So proper fields are prompted on error */
next-prompt s-pickprintfl with frame f-vaetg.

if v-type = "D"
then

/*e for proper scoping */
main:
do for vaeh, sasp, sassr, arsc, arss, sasc while true with frame f-vaetg
    on endkey undo main, leave main on error undo main, leave main:

    view frame f-vaetg.

    /*d Check to see if the operator has security to print the internal
        processing information */
    assign
        v-pckokfl = no
        v-intokfl = no.

    /*e Check security on F10 Print */
    do for sassm, sasos:

        {w-sassm.i ""vaepp"" no-lock}
        if avail sassm then do:
            if sassm.securpos = 0 then v-pckokfl = yes.
            else do:
                {w-sasos.i g-operinits sassm.menuproc no-lock}

                if avail sasos and sasos.securcd[sassm.securpos] > 2
                    then v-pckokfl = yes.
            end.

        end.

        {w-sassm.i ""vaepi"" no-lock}
        if avail sassm then do:
            if sassm.securpos = 0 then v-intokfl = yes.
            else do:
                {w-sasos.i g-operinits sassm.menuproc no-lock}

                if avail sasos and sasos.securcd[sassm.securpos] > 2
                    then v-intokfl = yes.
            end.

        end.

    end.

    if v-pckokfl = no then do:

        s-pickprintfl = no.

        display
            s-pickprintfl
            s-pickseqno
            s-printernmpck
        with frame f-vaetg.
    end.

    if v-intokfl = no then do:

        s-intrnprintfl = no.

        display
            s-intrnprintfl
            s-intrnseqno
            s-printernmintrn
        with frame f-vaetg.
    end. /* if v-intokfl = no */
    /*e Allow *whse* in printer device */
    /*o Allow entry of the order to print and which document(s) to print */
    update
        g-vano
        g-vasuf
        s-pickprintfl     when v-pckokfl
        s-pickseqno       when v-pckokfl
        s-printernmpck    when v-pckokfl
        s-intrnprintfl    when v-intokfl
        s-intrnseqno      when v-intokfl
        s-printernmintrn  when v-intokfl
    with frame f-vaetg
    editing:

        readkey.
        if {k-after.i} then do:
            assign
                g-vano          = input g-vano
                g-vasuf         = input g-vasuf
                s-pickprintfl   = input s-pickprintfl
                s-intrnprintfl  = input s-intrnprintfl.

            if (frame-field = "s-pickprintfl" and s-pickprintfl = yes) or
               (frame-field = "s-intrnprintfl" and s-intrnprintfl = yes)
            then do:

                {vaeh.gfi &vano  = "g-vano"
                          &vasuf = "g-vasuf"
                          &lock  = "no"}

                if avail vaeh then do for vaes:
                    if frame-field = "s-pickprintfl" and s-pickprintfl = yes
                    then do:

                        find first vaes use-index k-vaes where
                                   vaes.cono       = g-cono     and
                                   vaes.vano       = g-vano     and
                                   vaes.vasuf      = g-vasuf    and
                                   vaes.sctntype   = "in"       and
                                   vaes.completefl = no
                        no-lock no-error.

                        if avail vaes then do:
                            s-pickseqno = vaes.seqno.
                            display
                                s-pickseqno
                            with frame f-vaetg.
                        end.
                    end.  /* if frame field = s-pickprintfl  */

                    if frame-field = "s-intrnprintfl" and s-intrnprintfl = yes
                    then do:

                        find first vaes use-index k-vaes where
                                   vaes.cono       = g-cono     and
                                   vaes.vano       = g-vano     and
                                   vaes.vasuf      = g-vasuf    and
                                   vaes.sctntype   = "it"       and
                                   vaes.completefl = no
                        no-lock no-error.

                        if avail vaes then do:
                            s-intrnseqno = vaes.seqno.
                            display
                                s-intrnseqno
                            with frame f-vaetg.
                        end.
                    end.  /* if frame field = s-intrnprintfl  */
                end.   /* if avail vaeh  */
            end.    /* if frame field ... */
        end. /* if k-after */
        apply lastkey.
    end.  /* editing loop */

    /*d Validate the pick ticket printer */
    /*e Check if pick ticket print flag is set to yes */
    /*e Allow *whse* in printer device */
    if s-printernmpck ne "" and s-pickprintfl = yes and
       s-printernmpck ne "*whse*"
    then do:
        {w-sasp.i s-printernmpck no-lock}
        if not avail sasp then do:
            run err.p(4035).
            next-prompt s-printernmpck.
            next.
        end.

        {rxcheck.lpr
            &sasp     = sasp
            &currproc = """vaepp"""
            &prompt_clause = s-printernmpck
            &printernm = s-printernmpck
            &display_clause = "display s-printernmpck." }

        if can-do("v",sasp.ptype) then do:
            run err.p(5000).
            next-prompt s-printernmpck.
            next.
        end.

        if can-do("f",sasp.ptype) then do:
            run err.p(1134).
            next-prompt s-printernmpck.
            next.
        end.

    end.    /* Pick ticket printer validation */

    /*d Validate the internal processing printer */
    /*e Check if internal print flag is set to yes */
    if s-printernmintrn ne "" and s-intrnprintfl = yes
    then do:
        {w-sasp.i s-printernmintrn no-lock}
        if not avail sasp
        then do:
            run err.p(4035).
            next-prompt s-printernmintrn.
            next.
        end.

        if sasp.ptype = "v"
        then do:
            run err.p(5000).
            next-prompt s-printernmintrn.
            next.
        end.

        if can-do("f",sasp.ptype) then do:
            run err.p(1134).
            next-prompt s-printernmintrn.
            next.
        end.

        v-intptype = sasp.ptype.
    end.    /* Internal processing printer name validation */

    /*o Demand print an order if it is requested */
    if g-vano > 0
    then do:
        {vaeh.gfi
            &vano     = "g-vano"
            &vasuf    = "g-vasuf"
            &lock     = "no"}

        /*d Make sure the order is valid */
        if not avail vaeh then do:
            run err.p(4849).
            next-prompt g-vano.
            next.
        end.

        /*e User Include to vaetg.p */
        {vaetg.z99 &user_befprt = "*"}

        /*o Only print the order if it is not in use by anyone */
        if vaeh.openinit ne "" then do:
            message "Order in Use by " vaeh.openinit + "(5608)".
            bell.
            next-prompt g-vano.
            next.
        end.

        /*d Don't allow printing a pick ticket for a shipped order.  Also
            check to ensure the type of order and stage is valid for picking,
            and that the order has been approved */
        if s-pickprintfl = yes then do for vaes:
            {vaes.gfi &vano     = "g-vano"
                      &vasuf    = "g-vasuf"
                      &seqno    = "s-pickseqno"
                      &lock     = "no"}

            if not avail vaes then do:
                run err.p(4850).
                next-prompt s-pickseqno.
                next.
            end.

            i = if vaeh.statustype = false then 5815
                else
                if vaes.completefl = yes then 4858
                else
                if vaes.sctntype ne "in" then 4859
                else
                if vaeh.transtype = "qu" then 4857
                else
                if vaeh.approvty ne "y" then 5846
                else 0.

            if i <> 0
            then do:
                run err.p(i).
                next-prompt g-vano.
                next.
            end.

        end.    /* if printpickfl is set */

        /*d Don't allow printing the internal processing of a VA shipped order.
            Also check to ensure the type of order and stage is valid,
            and that the order has been approved */
        if s-intrnprintfl then do for vaes:
            {vaes.gfi &vano     = "g-vano"
                      &vasuf    = "g-vasuf"
                      &seqno    = "s-intrnseqno"
                      &lock     = "no"}

            if not avail vaes then do:
                run err.p(4850).
                next-prompt s-intrnseqno.
                next.
            end.

            i = if vaeh.statustype = false then 5815
                else
                if vaes.completefl = yes then 4858
                else
                if vaes.sctntype ne "it" then 4859
                else
                if vaeh.transtype = "qu" then 4857
                else
                if vaeh.approvty ne "y" then 5846
                else 0.

            if i <> 0
            then do:
                run err.p(i).
                next-prompt g-vano.
                next.
            end.

        end.    /* if intrnpickfl is set */

    end. /* if order number is greater than zero */

    /*o Set the global printing variables from screen entry */
    assign
        g-pickprintfl     = s-pickprintfl
        g-printernmpck    = s-printernmpck
        g-pickseqno       = s-pickseqno
        g-intrnprintfl    = s-intrnprintfl
        g-printernmintrn  = s-printernmintrn
        g-intrnseqno      = s-intrnseqno.

    if g-vano = 0 or (g-pickprintfl = no and g-intrnprintfl = no)
    then do:
        hide frame f-vaetg no-pause.
        return.
    end.

    leave main.
end.    /* main */

if not {k-jump.i} then hide frame f-vaetg no-pause.

/*o If an order number has not been entered, return to the calling program */
if g-vano = 0 or {k-endkey.i} or {k-jump.i} then return.

cReportPrefix = "".

{vaetg.lpr &runtype        = "v-type"
           &runpick        = "v-pick"
           &vano           = "g-vano"
           &vasuf          = "g-vasuf"
           &report-prefix  = cReportPrefix
           &whseprinter    = "s-printer"
           &pickprintfl    = "g-pickprintfl"
           &printernmpck   = "g-printernmpck"
           &pickseqno      = "g-pickseqno"
           &intrnprintfl   = "g-intrnprintfl"
           &printernmintrn = "g-printernmintrn"
           &pcksapbassigns = "assign sapb.optvalue[5] = 'yes'."
           &intrnseqno     = "g-intrnseqno"}