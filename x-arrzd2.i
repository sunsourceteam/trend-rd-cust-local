/{&user_temptable}*/
define var xx-slsrepout    like smsn.slsrep  no-undo.
define var xx-slsrepin     like smsn.slsrep  no-undo.
define var p-detailprt     as logical        no-undo.
define var p-format        as integer        no-undo.
define var r_regdist_begin like smsn.mgr     no-undo.
define var r_regdist_end   like smsn.mgr     no-undo.
define var r_repin_begin   like smsn.slsrep  no-undo.
define var r_repin_end     like smsn.slsrep  no-undo.
define var r_tknby_begin   like smsn.slsrep  no-undo.
define var r_tknby_end     like smsn.slsrep  no-undo.
define var r_repo_begin    like smsn.slsrep  no-undo.
define var r_repo_end      like smsn.slsrep  no-undo.
define var r_repgp_begin   like smsn.slstype no-undo.
define var r_repgp_end     like smsn.slstype no-undo.
define var r_invdate_begin like oeeh.invoicedt no-undo.
define var r_invdate_end   like oeeh.invoicedt no-undo.
define var r_crmgr_begin   like arsc.creditmgr no-undo.
define var r_crmgr_end     like arsc.creditmgr no-undo.
define var w-custno        like arsc.custno no-undo.
define var w-custnm        like arsc.name   no-undo.
define var w-irep          like smsn.slsrep no-undo.
define var w-iname         like smsn.name   no-undo.
define var w-orep          like smsn.slsrep no-undo.
define var w-oname         like smsn.name   no-undo.
define var w-regdist       like smsn.mgr    no-undo.
define var w-igroup        like smsn.slstype no-undo.
define var w-crmgr         like arsc.creditmgr no-undo.
define var w-r-amt  as decimal format ">>>,>>>,>>9.99-" no-undo.
define var w-t-amt  as decimal format ">>>,>>>,>>9.99-" no-undo.
define var cnt as integer no-undo.
define buffer i-smsn for smsn.
define buffer o-smsn for smsn.
define temp-table zdi
  field zcustno     like arsc.custno
  field zshipto     like arss.shipto
  field zcustnm     like arsc.name
  field zinvno      like aret.invno
  field zinvsuf     like aret.invsuf
  field zinvdt      like aret.invdt
  field zrefer      like aret.refer
  field zcreditmgr  like arsc.creditmgr
  field zregdist    like smsn.mgr
  field zdivno      like arsc.divno
  field zgroup      like smsn.slstype
  field zirep       like smsn.slsrep
  field zirepname   like smsn.name
  field zorep       like smsn.slsrep
  field zorepname   like smsn.name
  field ztakenby    like oeeh.takenby
  field zamt        like aret.amount
  index zdi-inx1
                 zregdist
                 zcustno
                 zshipto
                 zinvdt
                 zinvno
                 zinvsuf.
/{&user_temptable}*/
 
/{&user_drpttable}*/
    FIELD arrecid        AS recid   
/{&user_drpttable}*/
 
/{&user_forms}*/
form header
  "~015 " at 1
  with frame fx-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: ARRZD"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 168
  page-number(xpcd)    at 173 format ">>>9"
  "~015"               at 177
  "Accounts Recievable Disputed Invoice Report" at 65
   "~015"              at 177
   with frame fx-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header
  "~015 " at 1
  with frame f-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: ARRZD"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 168
  page-number          at 173 format ">>>9"
  "~015"              at 177
  "Accounts Recievable Disputed Invoice Report" at 65
  "~015"              at 177
   with frame f-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header 
   "~015"                    at 177
  with frame f-trn3 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header 
   "~015"                    at 177
  with frame f-trn4 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header
  "~015"                           at 1
  "Customer"                       at 5
  "ShipTo"                         at 14
  "Customer Name"                  at 29
  "Inv Nbr"                        at 58
  "Inv Date"                       at 66
  "Amount"                         at 81
  "Reference"                      at 89
  "CrMgr"                          at 104
  "------Inside SlsRep------"      at 110
  "-----Outside SlsRep------"      at 136
  "~015"                           at 178
  "~015"                           at 1
  with frame f-trn5 down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.
 form
   zdi.zcustno     at 1
   zdi.zshipto     at 14
   zdi.zcustnm     at 23
   zdi.zinvno      at 54
   "-"             at 61
   zdi.zinvsuf     at 62
   zdi.zinvdt      at 65
   zdi.zamt        at 74
   zdi.zrefer      at 88  format "x(15)"
   zdi.zcreditmgr  at 104  format "x(4)"
   zdi.zirep       at 109 format "x(4)"
   zdi.zirepname   at 114 format "x(20)"
   zdi.zorep       at 135 format "x(4)"
   zdi.zorepname   at 140 format "x(20)"
   "~015"          at 178 
    with frame rpt1 down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.
form
    v-summary-lit2        at 1  format "x(22)"
    zdi.zamt              at 74
    "~015"                at 178
 with frame f-tot width 178
       no-box no-labels.  
form 
  "~015"           at 1
with frame f-skip width 178 no-box no-labels.  
form /* Customer total line */
    "----------"          at 25
    "-----------"         at 36
    "------------"        at 48
    "-------------"       at 61
    "------------"        at 75
    "------------"        at 88
    "------------"        at 101
    "------------"        at 114
    "------------"        at 127
    "------------"        at 140
    "------------"        at 153
    "------------"        at 167
    "~015"                at 179
  with frame f-totuline no-box no-labels down width 180.
/{&user_forms}*/
 
/{&user_extractrun}*/
 run sapb_vars.
 run extract_data.
/{&user_extractrun}*/
 
/{&user_optiontype}*/
  if not
  can-do("R,D,V,C,M,T,I,O,S",substring(p-optiontype,v-inx,1))
   then
    if not x-stream then 
       display 
         "Invalid option selection valid entries R,D,V,C,M,T,I,O,S"
              substring(p-optiontype,v-inx,1).
     else
       display stream xpcd
         "Invalid option selection valid entries R,D,V,C,M,T,I,O,S"
              substring(p-optiontype,v-inx,1).
   
if not
can-do("C,D,I,M,O,R,T,V",substring(p-optiontype,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries C,D,I,M,O,R,T,V "
     substring(p-optiontype,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries C,D,I,M,O,R,T,V "
     substring(p-optiontype,v-inx,1).
/{&user_optiontype}*/
 
/{&user_registertype}*/
  if not
  can-do("R,D,V,C,M,I,O,T,S",substring(p-register,v-inx,1))
   then
    if not x-stream then 
       display 
         "Invalid option selection valid entries R,D,V,C,M,T,I,O,S"
              substring(p-register,v-inx,1).
     else
       display stream xpcd
         "Invalid option selection valid entries R,D,V,C,M,T,I,O,S"
              substring(p-register,v-inx,1).
 
if not
can-do("C,D,I,M,O,R,T,V",substring(p-register,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries C,D,I,M,O,R,T,V "
     substring(p-register,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries C,D,I,M,O,R,T,V "
     substring(p-register,v-inx,1).
/{&user_registertype}*/
 
/{&user_exportvarheaders1}*/
   if v-lookup[v-inx4] = "R" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Region".
    
    end. 
   else 
   if v-lookup[v-inx4] = "I" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
    assign export_rec = export_rec +  "InsideRep".
    assign export_rec = export_rec + v-del + "InsideRep".
    
    end. 
   else 
   if v-lookup[v-inx4] = "O" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
    assign export_rec = export_rec +  "OutSideRep".
    assign export_rec = export_rec + v-del + "OutSideRep".
    end. 
   else 
   if v-lookup[v-inx4] = "T" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "TakenBy".
    
    end. 
   else 
   if v-lookup[v-inx4] = "D" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "District".
    
    end. 
   else 
   if v-lookup[v-inx4] = "S" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "ShipTo".
    
    end. 
 
   else
   if v-lookup[v-inx4] = "C" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Customer".
     assign export_rec = export_rec + v-del + "Customer".
/* Customer has two columns the sales rep + name */
    
    end. 
   else 
   if v-lookup[v-inx4] = "V" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "DivNo".
    
    end. 
   else
   if v-lookup[v-inx4] = "M" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "CreditMgr".
     assign export_rec = export_rec + v-del + "CreditMgr".
/* Credit Mgr has two columns the sales rep + name */
    
    end. 
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "I" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "M" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "O" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "V" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
/{&user_exportvarheaders1}*/
 
/{&user_exportvarheaders2}*/
   if v-lookup[v-inx4] = "R" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  " ".
    
    end. 
   else 
   if v-lookup[v-inx4] = "T" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  " ".
    
    end. 
   else 
   if v-lookup[v-inx4] = "S" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  " ".
    
    end. 
 
   else 
   if v-lookup[v-inx4] = "I" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
    assign export_rec = export_rec +  " ".
    assign export_rec = export_rec + v-del + "Name".
    end. 
   else 
   if v-lookup[v-inx4] = "O" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
    assign export_rec = export_rec +  " ".
    assign export_rec = export_rec + v-del + "Name".
    end. 
   else 
   if v-lookup[v-inx4] = "D" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  " ".
    
    end. 
   else
   if v-lookup[v-inx4] = "C" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  " ".
     assign export_rec = export_rec + v-del + "Name".
/* Customer has two columns the sales rep + name */
    
    end. 
   else 
   if v-lookup[v-inx4] = "V" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  " ".
    
    end. 
   else
   if v-lookup[v-inx4] = "M" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  " ".
     assign export_rec = export_rec + v-del + "Name".
/* Credit Mgr has two columns the sales rep + name */
    
    end. 
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "I" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "M" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "O" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "V" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
/{&user_exportvarheaders2}*/
 
/{&user_exportstatheaders}*/
  assign export_rec = export_rec + v-del +
                      "Disputed Amount" + v-del +
                      " " + v-del +
                      " ".
 
/{&user_exportstatheaders}*/
 
/{&user_foreach}*/
for each zdi no-lock:
/{&user_foreach}*/
 
/{&user_loadtokens}*/
 if v-lookup[v-inx] = "t" then
   do:
   if v-inx > 1 then
     assign v-mystery = v-mystery + v-delim
            v-sortmystery = v-sortmystery + v-delim.
  
   assign v-token =
          caps(string(zdi.ztakenby,"x(4)")).
   assign v-mystery = v-mystery + v-token.
   if v-sortup[v-inx] = "<" then
     run ascii_make_it_not (input-output v-token).
   assign  v-sortmystery = v-sortmystery +
                           string(v-token,"x(4)").
   end.
 else
 
 if v-lookup[v-inx] = "i" then
   do:
   if v-inx > 1 then
     assign v-mystery = v-mystery + v-delim
            v-sortmystery = v-sortmystery + v-delim.
  
   assign v-token =
          caps(string(zdi.zirep,"x(4)")).
   assign v-mystery = v-mystery + v-token.
   if v-sortup[v-inx] = "<" then
     run ascii_make_it_not (input-output v-token).
   assign  v-sortmystery = v-sortmystery +
                           string(v-token,"x(4)").
   end.
  else
 
 if v-lookup[v-inx] = "s" then
   do:
   if v-inx > 1 then
     assign v-mystery = v-mystery + v-delim
            v-sortmystery = v-sortmystery + v-delim.
  
   assign v-token =
          caps(string(zdi.zshipto,"x(8)")).
   assign v-mystery = v-mystery + v-token.
   if v-sortup[v-inx] = "<" then
     run ascii_make_it_not (input-output v-token).
   assign  v-sortmystery = v-sortmystery +
                           string(v-token,"x(8)").
   end.
  else
 
 if v-lookup[v-inx] = "o" then
   do:
   if v-inx > 1 then
     assign v-mystery = v-mystery + v-delim
            v-sortmystery = v-sortmystery + v-delim.
  
   assign v-token =
          caps(string(zdi.zorep,"x(4)")).
   assign v-mystery = v-mystery + v-token.
   if v-sortup[v-inx] = "<" then
     run ascii_make_it_not (input-output v-token).
   assign  v-sortmystery = v-sortmystery +
                           string(v-token,"x(4)").
   end.
 else
 if v-lookup[v-inx] = "r" then
   do:
   if v-inx > 1 then
     assign v-mystery = v-mystery + v-delim
            v-sortmystery = v-sortmystery + v-delim.
  
   assign v-token =
          caps(string(substring(zdi.zregdist,1,1),"x(1)")).
   assign v-mystery = v-mystery + v-token.
   if v-sortup[v-inx] = "<" then
     run ascii_make_it_not (input-output v-token).
   assign  v-sortmystery = v-sortmystery +
                           string(v-token,"x(1)").
   end.
  else
  if v-lookup[v-inx] = "d" then
    do:
    if v-inx > 1 then
      assign v-mystery = v-mystery + v-delim
             v-sortmystery = v-sortmystery + v-delim.
    assign v-token =
           caps(string(zdi.zregdist,"x(4)")).
    assign v-mystery = v-mystery + v-token.
    if v-sortup[v-inx] = "<" then
      run ascii_make_it_not (input-output v-token).
    assign v-sortmystery = v-sortmystery +
                           string(v-token,"x(4)").
    end.
  else
  if v-lookup[v-inx] = "v" then
    do:
    if v-inx > 1 then
      assign v-mystery = v-mystery + v-delim
             v-sortmystery = v-sortmystery + v-delim.
    assign v-token =
             caps(string(zdi.zdivno,"999")).
    assign v-mystery = v-mystery + v-token.
    if v-sortup[v-inx] = "<" then
      run ascii_make_it_not (input-output v-token).
    assign v-sortmystery = v-sortmystery +
                           string(v-token,"x(3)").
    end.
  else
  if v-lookup[v-inx] = "c" then
    do:
    if v-inx > 1 then                   
      assign v-mystery = v-mystery + v-delim
             v-sortmystery = v-sortmystery + v-delim.
    
    v-token = string( string(zdi.zcustno,"999999999999")).
    v-mystery = v-mystery + v-token.
    if v-sortup[v-inx] = "<" then
      run ascii_make_it_not (input-output v-token).
    v-sortmystery = v-sortmystery +
                    string(v-token,"x(12)").
    end.
  else
  if v-lookup[v-inx] = "m" then
    do:
    if v-inx > 1 then
      assign v-mystery = v-mystery + v-delim
             v-sortmystery = v-sortmystery + v-delim.
    assign v-token =
           caps(string(zdi.zcreditmgr,"x(4)")).
    assign v-mystery = v-mystery + v-token.
    if v-sortup[v-inx] = "<" then
      run ascii_make_it_not (input-output v-token).
    assign v-sortmystery = v-sortmystery +
                           string(v-token,"x(4)").
    end.
                
  else  
  if v-lookup[v-inx] = "x" then
    do:
    if v-inx > 1 then
      assign v-mystery = v-mystery + v-delim
             v-sortmystery = v-sortmystery + v-delim.
    assign v-token =  string(recid(zdi),"999999999999").
    assign v-mystery = v-mystery + v-token.
    if v-sortup[v-inx] = "<" then
      run ascii_make_it_not (input-output v-token).
    assign v-sortmystery = v-sortmystery +
                           v-token.
    end.
if v-lookup[v-inx] = "C" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(,"")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"").   
  end.                                              
else                                              
if v-lookup[v-inx] = "D" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(,"")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"").   
  end.                                              
else                                              
if v-lookup[v-inx] = "I" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(,"")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"").   
  end.                                              
else                                              
if v-lookup[v-inx] = "M" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(,"")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"").   
  end.                                              
else                                              
if v-lookup[v-inx] = "O" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(,"")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"").   
  end.                                              
else                                              
if v-lookup[v-inx] = "R" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(,"")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"").   
  end.                                              
else                                              
if v-lookup[v-inx] = "T" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(,"")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"").   
  end.                                              
else                                              
if v-lookup[v-inx] = "V" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(,"")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"").   
  end.                                              
else                                              
  assign v-token = " ".
/{&user_loadtokens}*/
 
/{&user_totaladd}*/
 assign t-amounts[1]    = zdi.zamt.
  assign t-amounts[1] = .
/{&user_totaladd}*/
 
/{&user_drptassign}*/
   drpt.arrecid = recid(zdi)
/{&user_drptassign}*/
 
/{&user_viewheadframes}*/
if not p-exportl then
  do:
  if not x-stream then
    do:
    hide frame f-trn1.
    hide frame f-trn2.
    hide frame f-trn3.
    hide frame f-trn4.
    hide frame f-trn5.                  
    end.
  else
   do:
    hide stream xpcd frame f-trn1.
    hide stream xpcd frame fx-trn2.
    hide stream xpcd frame f-trn3.
    hide stream xpcd frame f-trn4.
    hide stream xpcd frame f-trn5.
    end.
  
  end.
if not p-exportl  then
  do:
  if not x-stream then
    do:
    page.
    view frame f-trn1.
    view frame f-trn2.
    view frame f-trn3.
    view frame f-trn4.
    view frame f-trn5.
    end.
  else  
    do:
    page stream xpcd.
    view stream xpcd frame f-trn1.
    view stream xpcd frame fx-trn2.
    view stream xpcd frame f-trn3.
    view stream xpcd frame f-trn4.
    view stream xpcd frame f-trn5.
    end.
   
  end.
/{&user_viewheadframes}*/
 
/{&user_drptloop}*/
/{&user_drptloop}*/
 
/{&user_detailprint}*/
       
   if p-detailprt and 
      entry(u-entitys,drpt.sortmystery,v-delim) <> 
      fill("~~",length(entry(u-entitys,drpt.sortmystery,v-delim))) then 
     do:
     run print-dtl (input drpt.arrecid).
     end.
/{&user_detailprint}*/
 
/{&user_finalprint}*/
                    
if not p-exportl then
 do:
 if not x-stream then 
   do:
   display with frame f-totequalline.
   display
     v-summary-lit2              @ v-summary-lit2
     t-amounts1[u-entitys + 1]   @ zdi.zamt
     with frame f-tot.
   down with frame f-tot.
   end.
 else
   do:
   display stream xpcd with frame f-totequalline.
   display stream xpcd
     v-summary-lit2               @ v-summary-lit2
     t-amounts1[u-entitys + 1]   @ zdi.zamt
    with frame f-tot.
  
   down stream xpcd with frame f-tot.
   end.
      
 end.
else
  do:
  assign export_rec =  v-astrik.
  do v-inx4 = 1 to u-entitys:
    if v-totalup[v-inx4] = "n" then
       next.
    if v-inx4 = 1 then
      v-summary-lit2 = "Final".
    else
      v-summary-lit2 = "".
    if can-do("c,m,i,o",v-lookup[v-inx4]) then
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec + v-summary-lit2 + v-del + " ".
     
      end.
    else
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec +  v-summary-lit2.
      end.
  end.
  assign export_rec = export_rec + v-del +
      string(t-amounts1[u-entitys + 1],"->>>>>>>>9").
    
  assign export_rec = export_rec + chr(13).    
  put stream expt unformatted export_rec.
  end.
/{&user_finalprint}*/
 
/{&user_numbertotals}*/
         /* Here the amount     */  if v-subtotalup[v-inx4] = 1 then
         /* that is to be       */    assign v-val = t-amounts1[v-inx4]. 
         /* accumulated for the */  else
         /* subtotal sort is    */  if v-subtotalup[v-inx4] = 2 then
         /* determined by param */    assign v-val = t-amounts2[v-inx4].
         /* this gives power to */  else
         /* sort on any amount  */  if v-subtotalup[v-inx4] = 3 then
         /* on the report at    */    assign v-val = t-amounts3[v-inx4].
         /* any level.          */  else
                                    if v-subtotalup[v-inx4] = 4 then
                                      assign v-val = t-amounts4[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 5 then
                                      assign v-val = t-amounts5[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 6 then
                                      assign v-val =  t-amounts6[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 7 then
                                      assign v-val = t-amounts7[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 8 then
                                      assign v-val = t-amounts8[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 9 then
                                      assign v-val = t-amounts9[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 10 then
                                      assign v-val = t-amounts10[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 11 then
                                      assign v-val = t-amounts11[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 12 then
                                      assign v-val = t-amounts12[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 13 then
                                      assign v-val = t-amounts13[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 14 then
                                      assign v-val = t-amounts14[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 15 then
                                      assign v-val = t-amounts15[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 16 then
                                      assign v-val = t-amounts16[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 17 then
                                      assign v-val = t-amounts17[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 18 then
                                      assign v-val = t-amounts18[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 19 then
                                      assign v-val = t-amounts19[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 20 then
                                      assign v-val = t-amounts20[v-inx4].
                                    else
                                      v-val = t-amounts1[v-inx4].              
                                    do inz = 1 to 20:
         /* The registers need  */  if inz = 1 then
         /* to be updated for   */    v-regval[inz] = t-amounts1[v-inx4]. 
         /* accumulation of the */  else
         /* totals that can be  */  if inz = 2 then
         /* controlled by the   */    v-regval[inz] = t-amounts2[v-inx4].                 /* parameters          */  else
         /*                     */  if inz = 3 then
         /*                     */    v-regval[inz] = t-amounts3[v-inx4].                 /*                     */  else
                                    if inz = 4 then
                                      v-regval[inz] = t-amounts4[v-inx4].
                                    else
                                    if inz = 5 then
                                      v-regval[inz] = t-amounts5[v-inx4].                                            else
                                    if inz = 6 then
                                      v-regval[inz] = t-amounts6[v-inx4].                                            else
                                    if inz = 7 then
                                      v-regval[inz] = t-amounts7[v-inx4].                                            else
                                    if inz = 8 then
                                      v-regval[inz] = t-amounts8[v-inx4].                                            else
                                    if inz = 9 then
                                      v-regval[inz] = t-amounts9[v-inx4].                                            else
                                    if inz = 10 then
                                      v-regval[inz] = t-amounts10[v-inx4].                                           else
                                    if inz = 11 then
                                      v-regval[inz] = t-amounts11[v-inx4].                                          else
                                    if inz = 12 then
                                      v-regval[inz] = t-amounts12[v-inx4].                                          else
                                    if inz = 13 then
                                      v-regval[inz] = t-amounts13[v-inx4].                                          else
                                    if inz = 14 then
                                      v-regval[inz] = t-amounts14[v-inx4].                                          else
                                    if inz = 15 then
                                      v-regval[inz] = t-amounts15[v-inx4].                                          else
                                    if inz = 16 then
                                      v-regval[inz] = t-amounts16[v-inx4].                                          else
                                    if inz = 17 then
                                      v-regval[inz] = t-amounts17[v-inx4].                                          else
                                    if inz = 18 then
                                      v-regval[inz] = t-amounts18[v-inx4].                                          else
                                    if inz = 19 then
                                      v-regval[inz] = t-amounts19[v-inx4].                                          else
                                    if inz = 20 then
                                      v-regval[inz] = t-amounts20[v-inx4].                                         end.
 
  if v-subtotalup[v-inx4] = 1 then
    v-val = t-amounts1[v-inx4].
  else 
    v-val = t-amounts1[v-inx4].
  do inz = 1 to 1:
  if inz = 1 then
    v-regval[inz] = t-amounts1[v-inx4].
  else 
    assign inz = inz.
  end.
/{&user_numbertotals}*/
 
/{&user_summaryloadprint}*/
  if v-lookup[v-inx2] = "V" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = u-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "DivNo - " + h-lookup + " " + h-genlit.
     
   end. 
 else
 if v-lookup[v-inx2] = "R" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = u-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "Region - " + h-lookup + " " + h-genlit.
     
   end. 
  else
 if v-lookup[v-inx2] = "s" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = u-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "Shipto - " + h-lookup + " " + h-genlit.
     
   end. 
  else
 if v-lookup[v-inx2] = "T" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = u-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "TakenBy - " + h-lookup + " " + h-genlit.
     
   end. 
 else
 if v-lookup[v-inx2] = "D" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = u-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "District - " + h-lookup + " " + h-genlit.
     
   end. 
 else
 if v-lookup[v-inx2] = "o" then
    do:
    find smsn where smsn.cono = 1 and
                    smsn.slsrep = h-lookup no-lock no-error.
     
    if avail smsn then
      h-genlit = smsn.name.
    else
      h-genlit = "No Name Available".
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
      assign v-summary-lit2 = h-lookup.
    else
      assign v-summary-lit2 = "RepOut -" +
                             h-lookup + " " + h-genlit.
    end.                          
  else
  if v-lookup[v-inx2] = "i" then
    do:
    find smsn where smsn.cono = 1 and
                    smsn.slsrep = h-lookup no-lock no-error.
     
    if avail smsn then
      h-genlit = smsn.name.
    else
      h-genlit = "No Name Available".
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
      assign v-summary-lit2 = h-lookup.
    else
      assign v-summary-lit2 = "RepIn -" +
                             h-lookup + " " + h-genlit.
    end.                          
   else
   if v-lookup[v-inx2] = "C" then
     do:
     if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       do:
       assign h-lookup = "All Others"
              h-genlit = "".
       if v-inx2 = u-entitys then
         assign v-summary-lit2 = 
                             substring(h-genlit,1,9) + " " +
                             h-lookup. 
       else
         assign v-summary-lit2 = "Cust -  " +
                             substring(h-genlit,1,9) + " " +
                             h-lookup. 
       end.
     else
       do:
       find arsc where arsc.cono = g-cono and 
                       arsc.custno = dec(h-lookup) no-lock no-error.
       if avail arsc then
         h-genlit = arsc.lookupnm.                      
       if v-inx2 = u-entitys then
         assign v-summary-lit2 = 
                             substring(h-genlit,1,9) + " " +
                             string(dec(h-lookup),">>>>>>>>>>>9"). 
       else
         assign v-summary-lit2 = "Cust -  " +
                             substring(h-genlit,1,9) + " " +
                             string(dec(h-lookup),">>>>>>>>>>>9"). 
       end.
     end.
 else
 if v-lookup[v-inx2] = "M" then
   do:
   find oimsp where
        oimsp.person = h-lookup no-lock no-error.
   assign h-lookup = h-lookup + " " +
                       (if avail oimsp then oimsp.name
                        else "No OIMSP Found").
    
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others"
            h-genlit = "".
   if v-inx2 = u-entitys then
     assign v-summary-lit2 = h-lookup.
   else
     assign v-summary-lit2 = "CMgr- " +
                            h-lookup.
   end.
 else
   v-summary-lit = v-lookup[v-inx2].
  if v-lookup[v-inx2] = "C" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Customer Number - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "D" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "District - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "I" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Inside Sales Rep - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "M" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Credit Manager - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "O" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Outside Sales Rep - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "R" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Region - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "T" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Taken By - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "V" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Division Number - " + h-lookup + " " + h-genlit.
    end.
  else
      v-summary-lit = v-lookup[v-inx2].
/{&user_summaryloadprint}*/
 
/{&user_summaryframeprint}*/
  
     if not x-stream then
      do:
      display 
       v-summary-lit2       @ v-summary-lit2
       t-amounts1[v-inx2]   @ zdi.zamt
  
        with frame f-tot.
      down with frame f-tot.
      end.
    else
      do:
      display stream xpcd
        v-summary-lit2       @ v-summary-lit2
        t-amounts12[v-inx2]   @ zdi.zamt
 
        with frame f-tot.
      down stream xpcd with frame f-tot.
      end.
/{&user_summaryframeprint}*/
 
/{&user_summaryloadexport}*/
  if v-inx4 > v-inx2  then
    do:
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    assign export_rec = export_rec +  " ".
/* This is necessary to insert a column for 2 column entries
   for example the sales rep name is in addition to the sales rep
   they are put in their own spread sheet column we have to account for
   that here.
*/   
    if can-do("c,m",substring(v-lookup[v-inx4],1,1)) then
      assign export_rec = export_rec +  v-del + " ".
    end. 
  else
  if v-lookup[v-inx4] = "R" then
     do:
     if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others".
     if v-inx4 > 1 then 
        export_rec = export_rec + v-del.
     assign export_rec = export_rec +  h-lookup.
     
     end.
  else
  if v-lookup[v-inx4] = "s" then
     do:
     if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others".
     if v-inx4 > 1 then 
        export_rec = export_rec + v-del.
     assign export_rec = export_rec +  h-lookup.
     
     end.       
  else
  if v-lookup[v-inx4] = "T" then
     do:
     if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others".
     if v-inx4 > 1 then 
        export_rec = export_rec + v-del.
     assign export_rec = export_rec +  h-lookup.
     
     end. 
  else
  if v-lookup[v-inx4] = "V" then
     do:
     if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others".
     if v-inx4 > 1 then 
        export_rec = export_rec + v-del.
     assign export_rec = export_rec +  h-lookup.
     
     end. 
     
  else
  if v-lookup[v-inx4] = "D" then
     do:
     if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others".
     if v-inx4 > 1 then 
        export_rec = export_rec + v-del.
     assign export_rec = export_rec +  h-lookup.
     
     end. 
  else
  if v-lookup[v-inx4] = "o" then
    do:
    find smsn where smsn.cono = 1 and
                    smsn.slsrep = h-lookup no-lock no-error.
     
    if avail smsn then
      h-genlit = smsn.name.
    else
      h-genlit = "No Name Available".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
      if v-inx4 > 1 then 
        export_rec = export_rec + v-del.
     assign export_rec = export_rec + h-lookup + v-del + h-genlit.
     
    end.                          
  else
   if v-lookup[v-inx4] = "i" then
    do:
    find smsn where smsn.cono = 1 and
                    smsn.slsrep = h-lookup no-lock no-error.
     
    if avail smsn then
      h-genlit = smsn.name.
    else
      h-genlit = "No Name Available".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
      if v-inx4 > 1 then 
        export_rec = export_rec + v-del.
     assign export_rec = export_rec + h-lookup + v-del + h-genlit.
     
    end.                          
  else
  if v-lookup[v-inx4] = "C" then
     do:
     find arsc where arsc.cono = g-cono and 
                     arsc.custno = dec(h-lookup) no-lock no-error.
     if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
     else
     if avail arsc then
       h-genlit = arsc.lookupnm.                      
     
     if v-inx4 > 1 then
       export_rec = export_rec + v-del.         
     assign export_rec = export_rec + h-lookup + 
                               v-del
                             + h-genlit.
     end.
  else
  if v-lookup[v-inx4] = "M" then
    do:
    find oimsp where
         oimsp.person = h-lookup no-lock no-error.
     assign h-genlit =
                       (if avail oimsp then oimsp.name
                        else "No OIMSP Found").
 
    
    if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
   
    assign export_rec = export_rec + h-lookup + v-del + h-genlit.
 
    end.
  else
    do:
    if v-inx4 > 1 then 
        export_rec = export_rec + v-del.
     assign export_rec = export_rec + v-lookup[v-inx4].
    end.
  if v-inx4 > v-inx2  then 
    do:
    if v-inx4 > 1 then          
      export_rec = export_rec + v-del. 
    assign export_rec = export_rec +  " ".
    if v-cells[v-inx4] = "2" then
      assign export_rec = export_rec +  v-del + " ".
    end. 
  else 
  if v-lookup[v-inx4] = "C" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "D" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "I" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "M" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "O" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "R" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "T" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "V" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
    do:
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    assign export_rec = export_rec + v-lookup[v-inx4].
    end.
/{&user_summaryloadexport}*/
 
/{&user_summaryputexport}*/
  
/* PTD */
    assign export_rec = v-astrik + v-del + export_rec.
    assign export_rec = export_rec + v-del +
      string(t-amounts1[v-inx2],"->>>>>>>>9").
      
/{&user_summaryputexport}*/
 
/{&user_procedures}*/
procedure sapb_vars:
if sapb.rangebeg[1] = "" then
   r_regdist_begin = "    ".
else   
   r_regdist_begin = sapb.rangebeg[1].
if sapb.rangeend[1] = "" then
   r_regdist_end = "````".
else   
   r_regdist_end = sapb.rangeend[1].
if sapb.rangebeg[2] = "" then
   r_repin_begin = "    ".
else   
   r_repin_begin = sapb.rangebeg[2].
if sapb.rangeend[2] = "" then
   r_repin_end = "````".
else   
   r_repin_end = sapb.rangeend[2].
if sapb.rangebeg[3] = "" then
   r_repo_begin = "    ".
else   
   r_repo_begin = sapb.rangebeg[3].
if sapb.rangeend[3] = "" then
   r_repo_end = "````".
else   
   r_repo_end = sapb.rangeend[3].
if sapb.rangebeg[4] = "" then
   r_repgp_begin = "    ".
else   
   r_repgp_begin = sapb.rangebeg[4].
if sapb.rangeend[4] = "" then
   r_repgp_end = "````".
else   
   r_repgp_end = sapb.rangeend[4].
if sapb.rangebeg[5] ne "" then
    do:
    v-datein = sapb.rangebeg[5].
    {p-rptdt.i}
    if string(v-dateout) = v-lowdt then
      r_invdate_begin = 01/01/1900.
    else  
      r_invdate_begin = v-dateout.
    end.
else
  r_invdate_begin = 01/01/1900.
if sapb.rangeend[5] ne "" then
    do:
    v-datein = sapb.rangeend[5].
    {p-rptdt.i}
    if string(v-dateout) = v-highdt then
      r_invdate_end = 12/31/2049.
    else
      r_invdate_end = v-dateout.
    end.
else
  r_invdate_end = 12/31/2049.
if sapb.rangebeg[6] = "" then
   r_crmgr_begin = "    ".
else   
   r_crmgr_begin = sapb.rangebeg[6].
if sapb.rangeend[6] = "" then
   r_crmgr_end = "````".
else   
   r_crmgr_end = sapb.rangeend[6].
if sapb.rangebeg[7] = "" then
   r_tknby_begin = "    ".
else   
   r_tknby_begin = sapb.rangebeg[7].
if sapb.rangeend[7] = "" then
   r_tknby_end = "````".
else   
   r_tknby_end = sapb.rangeend[7].
if sapb.optvalue[2] = "d" then
   p-detail = "d".
else
   p-detail = "s".
assign p-detailprt = if p-detail = "s" then no else yes.   
       p-format = int(sapb.optvalue[1]).
   run formatoptions.
end.
/* ------------------------------------------------------------------------- */
procedure formatoptions:
/* ------------------------------------------------------------------------- */
   
   if p-format = 99 then
     do:
     run reportopts
        (input         sapb.user5, 
         input-output  p-optiontype,  
         input-output  p-sorttype,   
         input-output  p-totaltype,  
         input-output  p-summcounts,   
         input-output  p-register,    
         input-output  p-registerex).
     end.
   else
     do:
     find notes where notes.cono = g-cono and
                      notes.notestype = "zz" and
                      notes.primarykey = "arrzd" and
                      notes.secondarykey = string(p-format) no-lock no-error.
     if not avail notes then
       do:
       display "Format is not valid cannot process request".
       assign p-optiontype = "r,d"
              p-sorttype   = ">,>"
              p-totaltype  = "l,l"
              p-summcounts = "a".
       return.
       end.               
   
       assign p-optiontype = notes.noteln[1]
              p-sorttype   = notes.noteln[2]
              p-totaltype  = notes.noteln[3]
              p-summcounts = notes.noteln[4]
              p-export     = "    ".
       assign p-register   = ""
              p-registerex = "".
       end.
   end.
   
   
   
   
procedure extract_data:
  for each aret 
              where aret.cono = g-cono and 
                    aret.statustype = true and 
                    aret.disputefl = true and
                    aret.invdt ge r_invdate_begin and
                    aret.invdt le r_invdate_end no-lock:
                    
    find arsc where arsc.cono = g-cono and
                    arsc.custno = aret.custno no-lock no-error.
    if not avail arsc then
      assign w-custnm = "No Master Found"
             w-crmgr  = "a   "
             w-custno = aret.custno.
   
    else
      assign w-custno = arsc.custno
             w-crmgr  = arsc.creditmgr
             w-custnm = arsc.name.
    assign xx-slsrepout = arsc.slsrepout
           xx-slsrepin  = arsc.slsrepin.
    
    if aret.shipto <> "" then
      do:
      find arss where arss.cono = g-cono and
                      arss.custno = aret.custno and
                      arss.shipto = aret.shipto no-lock no-error.
      if avail arss then
        assign xx-slsrepout = arss.slsrepout
               xx-slsrepin  = arss.slsrepin.
      end.
    find o-smsn where o-smsn.cono = g-cono and 
                      o-smsn.slsrep = xx-slsrepout no-lock no-error.
 
    if not avail o-smsn then
      assign w-orep   = xx-slsrepout
             w-regdist = "A000"
             w-oname  = "No Master Found".
   
    else
      assign w-orep   = o-smsn.slsrep
             w-regdist = o-smsn.mgr
             w-oname  = o-smsn.name.
  
  
  
  
    find i-smsn where i-smsn.cono = g-cono and 
                      i-smsn.slsrep = xx-slsrepin no-lock no-error.
    if not avail i-smsn then
      assign w-irep   = xx-slsrepout
             w-regdist = "A00"
             w-iname  = "No Master Found".
   
    else
      assign w-irep   = i-smsn.slsrep
             w-igroup = i-smsn.slstype
             w-iname  = i-smsn.name.
                    
    if w-regdist < r_regdist_begin or
       w-regdist > r_regdist_end then
       next.
                     
    if w-igroup < r_repgp_begin or
       w-igroup > r_repgp_end then
       next.
    find first oeeh where oeeh.cono = g-cono and
                          oeeh.orderno = aret.invno and
                          oeeh.ordersuf = aret.invsuf  no-lock no-error.
    if avail oeeh  then 
      do:
      if oeeh.takenby  < r_tknby_begin or
         oeeh.takenby  > r_tknby_end then
         next.                  
      end.
    else
    if "   "  < r_tknby_begin or
       "   "  > r_tknby_end then
      next.                  
    if w-irep < r_repin_begin or
       w-irep > r_repin_end or
       w-orep < r_repo_begin or
       w-orep > r_repo_end or
       w-crmgr < r_crmgr_begin or
       w-crmgr > r_crmgr_end then
       next.
     
    create zdi.
    assign
      zdi.zcustno      = w-custno
      zdi.zshipto      = aret.shipto
      zdi.zcustnm      = w-custnm
      zdi.zdivno       = if avail arsc then arsc.divno else 030
      zdi.zinvno       = aret.invno
      zdi.zinvsuf      = aret.invsuf
      zdi.zinvdt       = aret.invdt
      zdi.zrefer       = aret.refer
      zdi.zcreditmgr   = w-crmgr
      zdi.zregdist     = w-regdist
      zdi.zgroup       = w-igroup
      zdi.zirep        = w-irep
      zdi.zirepname    = w-iname
      zdi.zorep        = w-orep
      zdi.zamt         = aret.amount
      zdi.zorepname    = w-oname
      zdi.ztakenby     = if avail oeeh then
                           oeeh.takenby 
                         else
                           " ".
  end.     
end.
 
procedure print-dtl:  
 define input parameter z-recid as recid.
   
 find zdi where recid(zdi) = z-recid.  
 display 
      zdi.zcustno
      zdi.zshipto
      zdi.zcustnm
      zdi.zinvno
      zdi.zinvsuf
      zdi.zinvdt
      zdi.zamt
      zdi.zrefer
      zdi.zcreditmgr
      zdi.zirep
      zdi.zirepname
      zdi.zorep
      zdi.zorepname  with frame rpt1.
  down with frame rpt1.
end.   
  
/{&user_procedures}*/
 
