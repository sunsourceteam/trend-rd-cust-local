/*  g-fabstg.i    */

define temp-table t-stages
  field stage as integer
  field description as character
  field nextstg as integer
  field upstg  as integer
  field dwnstg as integer
  field shortname as character format "x(3)"
  field privupstg  as integer
  field privdwnstg as integer

index k-stg
  stage
index k-stgnm
  shortname.


 
 
