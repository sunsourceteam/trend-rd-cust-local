/* oeetkx.p 1.2 04/10/98 */
/* oeetkx.p 1.1 7/17/95 */
/*h*****************************************************************************
  PROCEDURE      : oeetkx
  DESCRIPTION    : Determine if eXpected required options, keywords, or
                   nonstock components have not been entered.
  AUTHOR         : ajw
  DATE WRITTEN   : 07/18/95
  CHANGES MADE   :
   07/18/95 ajw; TB# 18575 Nonstock/Special Kit Component Project (A17)  Code
        split from p-oeetk.i.
   04/10/98 mtt; TB# 24049 PV: Common code use for GUI interface
            *******************************************************************************/
def output param ip-nsreqfl      as l                            no-undo.
def output param ip-errfl        as c                            no-undo.
def output param ip-prodcost     as dec                          no-undo.


{g-all.i}
{g-oeetkp.i}
{g-oeetk.i}
{g-oeetlk.i}
{oeetkmn.gva}
define buffer p-notes for notes.
define buffer d-notes for notes.

define buffer b-CallCost for notes.
define var    v-SdiCallCost like icsw.stndcost no-undo.

 
find b-CallCost where 
     b-CallCost.Cono = g-cono and
     b-CallCost.NotesType = "ZZ" and
     b-CallCost.Primarykey = "SdiCallCost" no-lock no-error.
if avail b-CallCost then
  v-SdiCallCost = dec(b-CallCost.noteln[1]).
else
  v-SdiCallCost = -9871234.987653.  


if k-ordtype = "o" then do:
  for each   oeelk use-index k-oeelk where
             oeelk.cono      = g-cono        and
             oeelk.ordertype = k-ordtype     and
             oeelk.orderno   = k-orderno     and
             oeelk.ordersuf  = k-ordersuf    and
             oeelk.lineno    = k-lineno      and
            ((oeelk.shipprod ne ""               and
              oeelk.comptype ne "r")             or
             (oeelk.comptype = "r"               and
              oeelk.instructions ne ""))   no-lock:
/*
    message "Comps" k-lineno oeelk.seqno oeelk.prodcost v-SdiCallCost . pause.
*/    
    
    {w-icsp.i oeelk.shipprod no-lock}
    {w-icsw.i oeelk.shipprod v-whse no-lock}

     find first pder where pder.cono = g-cono and
                           pder.orderno = oeelk.orderno and
                           pder.ordersuf = oeelk.ordersuf and
                           pder.lineno   = oeelk.lineno and
                           pder.seqno    = oeelk.seqno no-lock no-error.

       
       
     find first p-notes where p-notes.cono         = g-cono and
                              p-notes.notestype    = "zz" and
                              p-notes.primarykey   = 
                                  "standard cost markup" and
                              p-notes.secondarykey = oeelk.prodcat  and
                              p-notes.pageno       = 1
                              no-lock no-error.
     find first d-notes where d-notes.cono         = g-cono and
                              d-notes.notestype    = "zz" and
                              d-notes.primarykey   = 
                                  "standard cost markup" and
                              d-notes.secondarykey = " " and
                              d-notes.pageno       = 1
                              no-lock no-error.

     find first com  where com.cono = g-cono     and 
                                     com.comtype  = "kt" +
                                          string(oeelk.seqno,"999") and
                                     com.orderno  = oeelk.orderno  and
                                     com.ordersuf = oeelk.ordersuf and
                                     com.lineno   = oeelk.lineno   and
                                     com.printfl  = no
                                     no-lock no-error.

     if oeelk.prodcost = 0 and avail icsw then do:
       if icsw.stndcost = v-SdiCallCost then do:
          assign 
            ip-errfl = "9960"
            ip-nsreqfl = true.
          return.
       end.   
     end.
     
     if oeelk.prodcost = v-SdiCallCost then do:
        assign 
          ip-errfl = "9960"
          ip-nsreqfl = true.
        return.
     end.   
     if avail pder then
       assign ip-prodcost = ip-prodcost + 
         ((if oeelk.orderaltno = 0 and not avail com then
            oeelk.prodcost 
          else
          if oeelk.orderaltno <> 0 and oeelk.glcost <> 0 and avail com  then
            oeelk.glcost *
           (if avail p-notes then
              (1 + (dec(p-notes.noteln[1]) / 100))
            else
            if avail d-notes then
              (1 + (dec(d-notes.noteln[1]) / 100))
            else
              1)
          else
          if oeelk.orderaltno = 0 and avail com and 
            dec( substring(com.noteln[10],17,12)) <> 0 and
            dec( substring(com.noteln[10],17,12)) <> oeelk.prodcost then
            dec( substring(com.noteln[10],17,12))
            *
            (if avail p-notes then
              (1 + (dec(p-notes.noteln[1]) / 100))
            else
            if avail d-notes then
              (1 + (dec(d-notes.noteln[1]) / 100))
            else
              1)
          else
            oeelk.prodcost)
      
           * oeelk.qtyneeded) - 
              (if avail pder then 
                 pder.rebateamt * oeelk.qtyneeded
               else
                 0).
    
  end. /* For each */
end.


