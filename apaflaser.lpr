/*h****************************************************************************
  INCLUDE      : apaflaser.lpr
  DESCRIPTION  : 1099 processing include for laser printers
  USED ONCE?   : no
  AUTHOR       : lcb
  DATE WRITTEN : 12/18/02
  CHANGES MADE :
    12/18/02 lcb; TB# e11886 Created new.
    01/15/03 lcb; TB# e16084 Add payer phone number.
    05/05/04 kjb; TB# e18963 Extra page and incorrect message printing
    08/30/05 gwk; TB# e21598 Account number now required
******************************************************************************/
    {&comassign}
     assign 
         a-amount[a-loop]        = if p-printyear = "c":u then apsv.paymtytd
                                   else apsv.paymtly
         a-name[a-loop]          = if avail sastn and sastn.name <> ""
                                   and sastn.fedtaxid <> "" then sastn.name
                                   else sasc.name
         a-phoneno[a-loop]       = if avail sastn and sastn.name <> ""
                                   and sastn.fedtaxid <> "" then sasc.phoneno
                                   else sasc.phoneno
         a-addr1[a-loop]         = if avail sastn and sastn.name <> ""
                                   and sastn.fedtaxid <> "" then sastn.addr[1]                                     else sasc.addr[1]
         a-addr2[a-loop]         = if avail sastn and sastn.name <> ""
                                   and sastn.fedtaxid <> "" then sastn.addr[2]
                                   else sasc.addr[2]
         a-city[a-loop]          = if avail sastn and sastn.name <> ""
                                   and sastn.fedtaxid <> "" then sastn.city 
                                   else sasc.city
         a-state[a-loop]         = if avail sastn and sastn.name <> ""
                                   and sastn.fedtaxid <> "" then sastn.state
                                   else sasc.state
         a-zip[a-loop]           = if avail sastn and sastn.name <> ""
                                   and sastn.fedtaxid <> "" then sastn.zipcd
                                   else sasc.zipcd
         a-fedtaxid[a-loop]      = if avail sastn and sastn.name <> ""
                                   and sastn.fedtaxid <> "" then sastn.fedtaxid                                    else sasc.fedtaxid
         a-recipnm[a-loop]       = if apsv.ap1099nm = "" then apsv.name
                                   else apsv.ap1099nm
         a-recipfedtaxid[a-loop] = apsv.fedtaxid
         a-recipaddr1[a-loop]    = apsv.addr[1]
         a-recipaddr2[a-loop]    = apsv.addr[2]
         a-recipcity[a-loop]     = apsv.city
         a-recipstate[a-loop]    = apsv.state
         a-recipzip[a-loop]      = apsv.zip
         a-fed1099box[a-loop]    = apsv.fed1099box
         
         a-vendno[a-loop]        = apsv.vendno
            
         a-box1[a-loop]          = if a-fed1099box[a-loop] = 1 
                                       then a-amount[a-loop] else 0   
         a-box2[a-loop]          = if a-fed1099box[a-loop] = 2
                                       then a-amount[a-loop] else 0   
         a-box3[a-loop]          = if a-fed1099box[a-loop] = 3 
                                       then a-amount[a-loop] else 0   
         a-box4[a-loop]          = if a-fed1099box[a-loop] = 4 
                                       then a-amount[a-loop] else 0   
         a-box5[a-loop]          = if a-fed1099box[a-loop] = 5 
                                       then a-amount[a-loop] else 0   
         a-box6[a-loop]          = if a-fed1099box[a-loop] = 6 
                                       then a-amount[a-loop] else 0   
         a-box7[a-loop]          = if a-fed1099box[a-loop] = 7 
                                       then a-amount[a-loop] else 0   
         a-box8[a-loop]          = if a-fed1099box[a-loop] = 8 
                                       then a-amount[a-loop] else 0   
         a-checkfl[a-loop]       = if a-fed1099box[a-loop] = 9 
                                       then "X":u else ""
         a-box10[a-loop]         = if a-fed1099box[a-loop] = 10 
                                       then a-amount[a-loop] else 0   
         a-box13[a-loop]         = if a-fed1099box[a-loop] = 13 
                                       then a-amount[a-loop] else 0   
         a-box14[a-loop]         = if a-fed1099box[a-loop] = 14 
                                       then a-amount[a-loop] else 0   
         a-box15[a-loop]         = if a-fed1099box[a-loop] = 15 
                                       then a-amount[a-loop] else 0   
         a-box16[a-loop]         = if a-fed1099box[a-loop] = 16 
                                       then a-amount[a-loop] else 0   
         a-box18[a-loop]         = if a-fed1099box[a-loop] = 18 
                                       then a-amount[a-loop] else 0   
         a-payerno[a-loop]       = if a-fed1099box[a-loop] = 17 
                                       then a-payerno[a-loop] else "".

    if a-loop = 1 then do:
        assign
            a-loop     = 2
            t-transcnt = t-transcnt + 1.
        next apsvloop. 
    end.  /*if a-loop */  
    /{&comassign}* */
     
    /* display [1] vars */
    display 
        a-void[1]       when p-printtype = "v":u
        a-correct[1]    when p-printtype = "c":u
        a-phoneno[1]
        a-name[1]
        a-addr1[1]
        a-addr2[1]
        a-city[1]
        a-state[1]
        a-zip[1]
        a-fedtaxid[1]
        a-recipfedtaxid[1]
        a-recipnm[1]
        a-recipaddr1[1] when a-recipaddr2[1] = ""  @ a-recipaddr2[1] 
        a-recipaddr1[1] when a-recipaddr2[1] ne "" @ a-recipaddr1[1]
        a-recipaddr2[1] when a-recipaddr2[1] ne "" @ a-recipaddr2[1]
        a-recipcity[1]
        a-recipstate[1]
        a-recipzip[1]

        a-box1[1]       when a-fed1099box[1] = 1
        a-box2[1]       when a-fed1099box[1] = 2
        a-box3[1]       when a-fed1099box[1] = 3
        a-box4[1]       when a-fed1099box[1] = 4
        a-box5[1]       when a-fed1099box[1] = 5
        a-box6[1]       when a-fed1099box[1] = 6 
        a-box7[1]       when a-fed1099box[1] = 7
        a-box8[1]       when a-fed1099box[1] = 8
        a-checkfl[1]    when a-fed1099box[1] = 9
        a-box10[1]      when a-fed1099box[1] = 10
        a-payerno[1]    when a-fed1099box[1] = 17
        
        a-vendno[1]
        
        a-box13[1]      when a-fed1099box[1] = 13
        a-box14[1]      when a-fed1099box[1] = 14
        a-box15[1]      when a-fed1099box[1] = 15
        a-box16[1]      when a-fed1099box[1] = 16
        a-box18[1]      when a-fed1099box[1] = 18

        /* display [2] vars */
        {&com2nd}
        a-void[2]       when p-printtype = "v":u
        a-correct[2]    when p-printtype = "c":u
        a-phoneno[2]
        a-name[2]
        a-addr1[2]
        a-addr2[2]
        a-city[2]
        a-state[2]
        a-zip[2]
        a-fedtaxid[2]
        a-recipfedtaxid[2]
        a-recipnm[2]
        a-recipaddr1[2] when a-recipaddr2[2] = ""  @ a-recipaddr2[2]
        a-recipaddr1[2] when a-recipaddr2[2] ne "" @ a-recipaddr1[2]
        a-recipaddr2[2] when a-recipaddr2[2] ne "" @ a-recipaddr2[2]
        a-recipcity[2]
        a-recipstate[2]
        a-recipzip[2]

        a-box1[2]       when a-fed1099box[2] = 1
        a-box2[2]       when a-fed1099box[2] = 2
        a-box3[2]       when a-fed1099box[2] = 3
        a-box4[2]       when a-fed1099box[2] = 4
        a-box5[2]       when a-fed1099box[2] = 5
        a-box6[2]       when a-fed1099box[2] = 6
        a-box7[2]       when a-fed1099box[2] = 7
        a-box8[2]       when a-fed1099box[2] = 8
        a-checkfl[2]    when a-fed1099box[2] = 9
        a-box10[2]      when a-fed1099box[2] = 10
        a-payerno[2]    when a-fed1099box[2] = 17
        
        a-vendno[2]
        
        a-box13[2]      when a-fed1099box[2] = 13
        a-box14[2]      when a-fed1099box[2] = 14
        a-box15[2]      when a-fed1099box[2] = 15
        a-box16[2]      when a-fed1099box[2] = 16
        a-box18[2]      when a-fed1099box[2] = 18

       /{&com2nd}* */
    with frame f-apaf6a.
    down with frame f-apaf6a.
    page.

    a-loop = 1.
