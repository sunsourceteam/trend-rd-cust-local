/* SX 55 */
/* f-smsn.i 1.1 01/03/98 */
/* f-smsn.i 1.1 2/16/92 */
/*  f-smsn.i  */
/*
    02/13/02 lbr; TB#e12226 Added ability to enter salesman email
    10/29/02 mtt; TB# e15115 Set up events and triggers
    09/09/04 bpa; TB# e20045 Add Sync To CRM flag using smsn.xxl1
***********************************************************************/

    s-slsrep like smsn.slsrep colon 13
    {f-help.i}
    smsn.name        colon 13
    smsn.mgr         colon 67
      help "X999 format"
    smsn.addr[1]     colon 13  label "Address"
    smsn.slstype     colon 67 label "Salesrep Group"
    smsn.addr[2]        at 15  no-label
    smsn.commfl      colon 67
    smsn.city        colon 13  label "City,St,Zip"
    smsn.state          at 36  no-label
    smsn.zipcd          at 39  no-label
    smsn.commtype    colon 67  label "Commission Type"
    smsn.slstitle    colon 13
    smsn.phonesuf    colon 67
    smsn.phoneno     colon 13
    smsn.geocd       colon 67
    smsn.email       colon 13  label "Email"
    skip(1)
    smsn.lettercd[1] colon 18 label "Std Paragraphs"
    smsn.lettercd[2]    at 25 no-label
    smsn.lettercd[3]    at 30 no-label
    smsn.lettercd[4]    at 35 no-label
    smsn.lettercd[5]    at 40 no-label
    smsn.lettercd[6]    at 45 no-label
    smsn.site           colon 60
    smsn.letterdir      colon 18
    smsn.sysname        colon 60

    smsn.securefl       colon 18  /* was after autocmfl      TB #2798 */
    smsn.oper2          colon 18 label "Operator"
        {f-help.i}
        validate({v-sasoo.i smsn.oper2},
            "Operator Not Set Up in Operator Setup - SASOO (4008)")
    smsn.modphoneno     colon 60
    smsn.beglastdt      colon 60 label "Last Transfer"
    smsn.endlastdt      colon 60 label "thru"
    smsn.begprosno      colon 18
    smsn.synccrmfl      colon 60 label "Sync To CRM"
    smsn.endprosno      colon 18
    /* dkt */
    smsn.user1 format "x(4)" colon 60 label "Parent Sales Rep"
    {f-help.i}
with frame f-smsn side-labels row 1 width 80 overlay
