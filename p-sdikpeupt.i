
/* p-sdikpeupt.i 1.1 01/03/98 */
/* p-sdikpeupt.i 1.3 10/9/92 */
/*h*****************************************************************************
  INCLUDE      : p-sdikpeupt.i
  DESCRIPTION  : Used for KP entry to update KPET record and ICSW balances
  USED ONCE?   : no
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    06/06/95 gp;  TB# 18611 Make changes for KP Work Order Demand (D1)
    08/17/95 gp;  TB# 19096 Kit's ICSW on order quantity driven negative.
    08/18/95 mtt; TB# 19096 Kit's ICSW on order quantity driven negative.
    10/30/99 bp ; TB 26575 Add logic to set the SPC variables.
*******************************************************************************/

/*d Update Qty-Commit for components  - Assemble */
/*d        Qty-on-ord for kit         - Assemble */
/*d        Qty-reserved for kit       - Disassemble */
/*d        Qty-on-ord for components  - Disassemble */

/*tb 19096 08/17/95 gp;  Replace order qty assignment with a call to
    an include */
find b3-icsw where recid(b3-icsw) = v-idicsw exclusive-lock no-error.
{w-icsp.i kpet.shipprod no-lock}
if avail b3-icsw and avail icsp and icsp.statustype <> "l" then do:
    /*tb 19096 08/18/95 mtt;  Kit's ICSW on order quantity driven negative */
 /**^^^**/
    {kpearuo.lup &icswbuf       = "b3-"
                 &oldbono       = "kpet.bono"
                 &oldstkqtyord  = "o-oldstkord"
                 &oldstkqtyship = "o-oldstkship"
                 &newstkqtyord  = "kpet.stkqtyord"
                 &newstkqtyship = "kpet.stkqtyship"}
/* not done here 
    if s-qtyordk < 0 and kpet.stagecd = 1 then
        assign
             b3-icsw.qtyreservd = b3-icsw.qtyreservd + ({1} * (-1)).
    else
    if s-qtyordk < 0 and kpet.stagecd > 1 then
        assign
             b3-icsw.qtycommit = b3-icsw.qtycommit + ({1} * (-1)).
*/
/*^^^*/
   {t-all.i b3-icsw} 
end.


