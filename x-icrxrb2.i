

/{&user_temptable}*/

define temp-table xxwhse no-undo

field whse    like icsw.whse
field ztype   as character format "x(1)"
field ordno   like wteh.wtno
field ordsuf  like wteh.wtsuf 
field lineno  like wtel.lineno
field shipprod like wtel.shipprod
field qtyrcv  like poel.qtyrcv
field rcvdt   as date
field zoperinit like poeh.rcvoperinit

index xx
      zoperinit
      ordno
      ordsuf
      lineno.


def  var ln-receiptdt-f   like wteh.receiptdt     no-undo.
def  var ln-receiptdt-t   like wteh.receiptdt     no-undo.


/{&user_temptable}*/


/{&user_drpttable}*/
    FIELD xxrecid        AS recid   

/{&user_drpttable}*/




/{&user_forms}*/
form header
  " " at 1
  with frame fx-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: ICRXR"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 121
  page-number(xpcd)    at 127 format ">>>9"
   "Receiving Statistics Report" at 35
  " "              at 1
   with frame fx-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 132 page-top.

form header
  " " at 1
  with frame f-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: ICRXR"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 121
  page-number         at 127 format ">>>9"
  "Receiving Statistics Report" at 35
  " "                 at 1
   with frame f-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 132 page-top.

form header 
   " "                    at 1
  with frame f-trn3 down NO-BOX NO-LABELS no-underline
            WIDTH 132 page-top.


form header
"Whse"     at 1
"Module"   at 6
"Ord#"     at 15
"Line"     at 27
"Product"  at 32
"Rcv Qty"  at 57
"Receipt"  at 69
"Rcv Init" at 78
"----"      at 1
"------"    at 6
"----------" at 15
"----"     at 27
"-----------------------"  at 32
"----------"  at 57
"--------"  at 69
"--------" at 78

 with frame f-detailhd down NO-BOX NO-LABELS no-underline
           WIDTH 132 page-top.



form
  xxwhse.whse      at 1
  xxwhse.ztype     at 6
  xxwhse.ordno     at 15
  xxwhse.ordsuf    at 23
  xxwhse.lineno    at 26
  xxwhse.shipprod  at 32
  xxwhse.qtyrcv    at 57
  xxwhse.rcvdt     at 69
  xxwhse.zoperinit at 78
with frame f-detail width 132
 no-box no-labels.  



form header
"Tot Qty Rcvd"     at 54
"Count"   at 91
"-------------"    at 54
"-------------"    at 91

 with frame f-summaryhd down NO-BOX NO-LABELS no-underline
           WIDTH 132 page-top.




form
  v-final                at 1
  v-astrik               at 2   format "x(5)"
  v-summary-lit2         at 7   format "x(27)"
  v-amt2                 at 53  format ">>>,>>>,>>9.99-"
  "Count"                at 87
  v-amt1                 at 93  format ">>>,>>>,>>9-"
with frame f-tot width 132
 no-box no-labels.  

form 
  "  "           at 1
with frame f-skip width 132 no-box no-labels.  

assign p-portrait = true.
/{&user_forms}*/


/{&user_extractrun}*/

 run sapb_vars.
 run extract_data.
/{&user_extractrun}*/



/{&user_optiontype}*/

  if not
  can-do("O,M,W,I,D",substring(p-optiontype,v-inx,1))
   then
    if not x-stream then 
       display 
         "Invalid option selection valid entries O,M,W,I,D"
              substring(p-optiontype,v-inx,1).
     else
       display stream xpcd
         "Invalid option selection valid entries O,M,W,I,D"
              substring(p-optiontype,v-inx,1).
/{&user_optiontype}*/
   

/{&user_registertype}*/

  if not
  can-do("O,M,D,I,W",substring(p-register,v-inx,1))
   then
    if not x-stream then 
       display 
         "Invalid option selection valid entries O,M,W,I,D"
              substring(p-register,v-inx,1).
     else
       display stream xpcd
         "Invalid option selection valid entries O,M,W,I,D"
              substring(p-register,v-inx,1).
/{&user_registertype}*/
 
/{&user_exportvarheaders1}*/

   if v-lookup[v-inx4] = "O" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Order".
    
    end. 
   else 
   if v-lookup[v-inx4] = "M" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Module".
    
    end. 
   else
   if v-lookup[v-inx4] = "W" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Whse".
    
    end. 
   else
   if v-lookup[v-inx4] = "I" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "OperatorId".
    
    end. 
   else
   if v-lookup[v-inx4] = "D" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Date".
    
    end. 



/{&user_exportvarheaders1}*/

/{&user_exportvarheaders2}*/

   if v-lookup[v-inx4] = "O" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "   ".
    
    end. 
   else 
   if v-lookup[v-inx4] = "m" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "   ".
    
    end. 
   else
   if v-lookup[v-inx4] = "W" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "  ".
    
    end. 
   else 
   if v-lookup[v-inx4] = "I" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "   ".
    
    end. 
   else
   if v-lookup[v-inx4] = "D" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "  ".
    
    end. 



/{&user_exportvarheaders2}*/


/{&user_exportstatheaders}*/
  /*
  assign export_rec = export_rec + v-del +
                      "Period 1" + v-del +
                      "Period 2" + v-del   +
                      "Period 3" + v-del   +
                      "Period 4" + v-del   +
                      "Period 5" + v-del   +
                      "Period 6 " + v-del   +
                      " " + v-del +
                      " ".
  */
/{&user_exportstatheaders}*/

 

/{&user_foreach}*/
for each xxwhse no-lock:
/{&user_foreach}*/


/{&user_loadtokens}*/
 if v-lookup[v-inx] = "o" then
   do:
   if v-inx > 1 then
     assign v-mystery = v-mystery + v-delim
            v-sortmystery = v-sortmystery + v-delim.
  
   assign v-token =
          caps(string(string(xxwhse.ordno,"9999999") + "-" +
                      string(xxwhse.ordsuf,"99") +
                      string(xxwhse.lineno,"999"),"x(20)")).
   assign v-mystery = v-mystery + v-token.
   if v-sortup[v-inx] = "<" then
     run ascii_make_it_not (input-output v-token).
   assign  v-sortmystery = v-sortmystery +
                           string(v-token,"x(20)").
   end.
  else
  if v-lookup[v-inx] = "m" then
    do:
    if v-inx > 1 then
      assign v-mystery = v-mystery + v-delim
             v-sortmystery = v-sortmystery + v-delim.
    assign v-token =
           caps(string(xxwhse.ztype,"x(1)")).
    assign v-mystery = v-mystery + v-token.
    if v-sortup[v-inx] = "<" then
      run ascii_make_it_not (input-output v-token).
    assign v-sortmystery = v-sortmystery +
                           string(v-token,"x(1)").
    end.
  else
  if v-lookup[v-inx] = "w" then
    do:
    if v-inx > 1 then
      assign v-mystery = v-mystery + v-delim
             v-sortmystery = v-sortmystery + v-delim.
    assign v-token =
             caps(string(xxwhse.whse,"x(4)")).
    assign v-mystery = v-mystery + v-token.
    if v-sortup[v-inx] = "<" then
      run ascii_make_it_not (input-output v-token).
    assign v-sortmystery = v-sortmystery +
                           string(v-token,"x(4)").
    end.
     else
  if v-lookup[v-inx] = "i" then
    do:
    if v-inx > 1 then
      assign v-mystery = v-mystery + v-delim
             v-sortmystery = v-sortmystery + v-delim.
    assign v-token =
             caps(string(xxwhse.zoperinit,"x(4)")).
    assign v-mystery = v-mystery + v-token.
    if v-sortup[v-inx] = "<" then
      run ascii_make_it_not (input-output v-token).
    assign v-sortmystery = v-sortmystery +
                           string(v-token,"x(4)").
    end.
  else
  if v-lookup[v-inx] = "d" then
    do:
    if v-inx > 1 then
      assign v-mystery = v-mystery + v-delim
             v-sortmystery = v-sortmystery + v-delim.
    assign v-token =
             caps(string(xxwhse.rcvdt,"99/99/9999")).
    assign v-mystery = v-mystery + v-token.
    if v-sortup[v-inx] = "<" then
      run ascii_make_it_not (input-output v-token).
    assign v-sortmystery = v-sortmystery +
                           string(v-token,"x(10)").
    end.
 
  else  
  if v-lookup[v-inx] = "x" then
    do:
    if v-inx > 1 then
      assign v-mystery = v-mystery + v-delim
             v-sortmystery = v-sortmystery + v-delim.
    assign v-token =  string(recid(xxwhse),"999999999999").
    assign v-mystery = v-mystery + v-token.
    if v-sortup[v-inx] = "<" then
      run ascii_make_it_not (input-output v-token).
    assign v-sortmystery = v-sortmystery +
                           v-token.
    end.
/{&user_loadtokens}*/


/{&user_totaladd}*/
 assign t-amounts[1] = 1.
        t-amounts[2] = xxwhse.qtyrcv.

/{&user_totaladd}*/

/{&user_drptassign}*/
   drpt.xxrecid = recid(xxwhse)

/{&user_drptassign}*/


/{&user_viewheadframes}*/
if not p-exportl then
  do:
  if not x-stream then
    do:
    hide frame f-trn1.
    hide frame f-trn2.
    hide frame f-trn3.
    hide frame f-trn4.
    hide frame f-trn5.
    end.
  else
   do:
    hide stream xpcd frame f-trn1.
    hide stream xpcd frame fx-trn2.
    hide stream xpcd frame f-trn3.
    hide stream xpcd frame f-trn4.
    hide stream xpcd frame f-trn5.
    end.
  
  end.


if not p-exportl  then
  do:
  if not x-stream then
    do:
    page.
    view frame f-trn1.
    view frame f-trn2.
    view frame f-trn3.
    if p-detail = "d" then
      view frame f-detailhd.
    else
      view frame f-summaryhd.
    
    end.
  else  
    do:
    page stream xpcd.
    view stream xpcd frame f-trn1.
    view stream xpcd frame fx-trn2.
    view stream xpcd frame f-trn3.
    if p-detail = "d" then
      view stream xpcd frame f-detailhd.
    else
      view stream xpcd frame f-summaryhd.
    end.
   
  end.

/{&user_viewheadframes}*/

/{&user_drptloop}*/
/{&user_drptloop}*/


/{&user_detailprint}*/

/** if the register entity(in this case "c") doesn't meet the 
    option criteria(see formatoptions procedure) the entity is 
    filled in with ~~~~. The ~~~~ will not create detail here. **/        
/**------------------------------------------------------------**/
  if p-detail = "d" and 
     entry((u-entitys - 1),drpt.sortmystery,v-delim) <> 
     fill("~~",length(entry((u-entitys - 1),drpt.sortmystery,v-delim)))  then 
   do:
   run print_detail (drpt.xxrecid).
   end.

/{&user_detailprint}*/

/{&user_finalprint}*/
if not p-exportl then
 do:
 if not x-stream then 
   do:
   display 
    v-final
    v-astrik
    v-summary-lit2
/* PTD */
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts2[u-entitys + 1]   @ v-amt2
    with frame f-tot.
  
   down with frame f-tot.
   end.
 else
   do:
   display stream xpcd
    v-final
    v-astrik
    v-summary-lit2
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts2[u-entitys + 1]   @ v-amt2
    with frame f-tot.
  
   down stream xpcd with frame f-tot.
   end.
      
 end.
else
  do:
  assign export_rec =  v-astrik.
  do v-inx4 = 1 to u-entitys:
    if v-inx4 = 1 then
      v-summary-lit2 = "Final".
    else
      v-summary-lit2 = "".

    if can-do("s",v-lookup[v-inx4]) then
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

      assign export_rec = export_rec + v-summary-lit2 + v-del + " ".
     
      end.
    else
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec +  v-summary-lit2.
      end.
  end.
  assign export_rec = export_rec + v-del +
      string(t-amounts1[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts2[u-entitys + 1],"->>>>>>>>9").
    
  assign export_rec = export_rec + chr(13).    
  put stream expt unformatted export_rec.
  end.
/{&user_finalprint}*/



/{&user_numbertotals}*/

         /* Here the amount     */  if v-subtotalup[v-inx4] = 1 then
         /* that is to be       */    assign v-val = t-amounts1[v-inx4]. 
         /* accumulated for the */  else
         /* subtotal sort is    */  if v-subtotalup[v-inx4] = 2 then
         /* determined by param */    assign v-val = t-amounts2[v-inx4].
         /* this gives power to */  else
         /* sort on any amount  */  if v-subtotalup[v-inx4] = 3 then
         /* on the report at    */    assign v-val = t-amounts3[v-inx4].
         /* any level.          */  else
                                    if v-subtotalup[v-inx4] = 4 then
                                      assign v-val = t-amounts4[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 5 then
                                      assign v-val = t-amounts5[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 6 then
                                      assign v-val =  t-amounts6[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 7 then
                                      assign v-val = t-amounts7[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 8 then
                                      assign v-val = t-amounts8[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 9 then
                                      assign v-val = t-amounts9[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 10 then
                                      assign v-val = t-amounts10[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 11 then
                                      assign v-val = t-amounts11[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 12 then
                                      assign v-val = t-amounts12[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 13 then
                                      assign v-val = t-amounts13[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 14 then
                                      assign v-val = t-amounts14[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 15 then
                                      assign v-val = t-amounts15[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 16 then
                                      assign v-val = t-amounts16[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 17 then
                                      assign v-val = t-amounts17[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 18 then
                                      assign v-val = t-amounts18[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 19 then
                                      assign v-val = t-amounts19[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 20 then
                                      assign v-val = t-amounts20[v-inx4].
                                    else
                                      v-val = t-amounts1[v-inx4].              

                                    do inz = 1 to 20:
         /* The registers need  */  if inz = 1 then
         /* to be updated for   */    v-regval[inz] = t-amounts1[v-inx4]. 
         /* accumulation of the */  else
         /* totals that can be  */  if inz = 2 then
        /* controlled by the   */    v-regval[inz] = t-amounts2[v-inx4].                /* parameters          */  else
        /*                     */  if inz = 3 then
        /*                     */    v-regval[inz] = t-amounts3[v-inx4].                /*                     */  else
                                    if inz = 4 then
                                      v-regval[inz] = t-amounts4[v-inx4].
                                    else
                                   if inz = 5 then
                                    v-regval[inz] = t-amounts5[v-inx4].  
                                   else
                                    if inz = 6 then
                                      v-regval[inz] = t-amounts6[v-inx4].                                            else
                                    if inz = 7 then
                                      v-regval[inz] = t-amounts7[v-inx4].                                            else
                                    if inz = 8 then
                                     v-regval[inz] = t-amounts8[v-inx4].                                           else
                                    if inz = 9 then
                                      v-regval[inz] = t-amounts9[v-inx4].                                          else
                                    if inz = 10 then
                                      v-regval[inz] = t-amounts10[v-inx4].                                         else
                                    if inz = 11 then
                                      v-regval[inz] = t-amounts11[v-inx4].                                        else
                                    if inz = 12 then
                                      v-regval[inz] = t-amounts12[v-inx4].                                        else
                                 if inz = 13 then
                                    v-regval[inz] = t-amounts13[v-inx4].                                          else
                                 if inz = 14 then                                                                   v-regval[inz] = t-amounts14[v-inx4].                                          else
                                 if inz = 15 then
                                    v-regval[inz] = t-amounts15[v-inx4].                                          else
                                 if inz = 16 then
                                    v-regval[inz] = t-amounts16[v-inx4].                                          else
                                 if inz = 17 then
                                    v-regval[inz] = t-amounts17[v-inx4].                                          else
                                 if inz = 18 then
                                    v-regval[inz] = t-amounts18[v-inx4].                                          else
                                 if inz = 19 then
                                    v-regval[inz] = t-amounts19[v-inx4].                                          else
                                 if inz = 20 then
                                    v-regval[inz] = t-amounts20[v-inx4].                                         end.

 
/{&user_numbertotals}*/




/{&user_summaryloadprint}*/
 if v-lookup[v-inx2] = "O" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = u-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "Order - " + h-lookup + " " + h-genlit.
     
   end. 
 else
 if v-lookup[v-inx2] = "m" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = u-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "Module  - " + h-lookup + " " + h-genlit.
     
   end. 
 else
 if v-lookup[v-inx2] = "w" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = u-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "Whse - " + h-lookup + " " + h-genlit.
     
   end. 
 else
 if v-lookup[v-inx2] = "i" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = u-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "OperatorId  - " + h-lookup + " " + h-genlit.
     
   end. 
 else
 if v-lookup[v-inx2] = "d" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = u-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "Date - " + h-lookup + " " + h-genlit.
     
   end. 
   
 else
   v-summary-lit = v-lookup[v-inx2].
/{&user_summaryloadprint}*/

/{&user_summaryframeprint}*/
  
    if not x-stream then
      do:
      display 
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        with frame f-tot.
      down with frame f-tot.
      end.
    else
      do:
      display stream xpcd
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        with frame f-tot.
      down stream xpcd with frame f-tot.
      end.
/{&user_summaryframeprint}*/

/{&user_summaryloadexport}*/

  if v-inx4 > v-inx2  then
    do:
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    assign export_rec = export_rec +  " ".
/* This is necessary to insert a column for 2 column entries
   for example the sales rep name is in addition to the sales rep
   they are put in their own spread sheet column we have to account for
   that here.
*/   
    if can-do("s",substring(v-lookup[v-inx4],1,1)) then
      assign export_rec = export_rec +  v-del + " ".
    end. 
  else
  if v-lookup[v-inx4] = "o" then
     do:
     if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others".
     if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  h-lookup.
     
     end. 
  else
  if v-lookup[v-inx4] = "m" then
     do:
     if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others".
     if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  h-lookup.
     
     end. 
  else
  if v-lookup[v-inx4] = "w" then
     do:
     if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others".
     if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  h-lookup.
     
     end. 
  else
  if v-lookup[v-inx4] = "i" then
     do:
     if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others".
     if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  h-lookup.
     
     end. 
  else
  if v-lookup[v-inx4] = "d" then
     do:
     if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others".
     if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  h-lookup.
     
     end. 
  else
    do:
    if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + v-lookup[v-inx4].
    end.
/{&user_summaryloadexport}*/

/{&user_summaryputexport}*/

 
/* PTD */
    assign export_rec = v-astrik + v-del + export_rec.
    assign export_rec = export_rec + v-del +
      string(t-amounts1[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts2[v-inx2],"->>>>>>>>9").
      
/{&user_summaryputexport}*/


/{&user_procedures}*/


procedure sapb_vars:
 run formatoptions.
 assign  p-detail     = sapb.optvalue[2]
         p-export     = " ".
       
assign v-datein    = sapb.rangebeg[2].
       {p-rptdt.i}
       if string(v-dateout) = v-lowdt then
          ln-receiptdt-f = today.
       else
          ln-receiptdt-f = v-dateout.
assign v-datein    = sapb.rangeend[2].
       {p-rptdt.i}
       if string(v-dateout) = v-lowdt then
          ln-receiptdt-t = today.
       else
          ln-receiptdt-t = v-dateout.




end.


/**-------------------------------------------------------------------------**/
procedure formatoptions: 
/**-------------------------------------------------------------------------**/

 if sapb.optvalue[1] > "0" and sapb.optvalue[1] ne "99" then 
 do:
  p-optiontype = " ".
  find notes where
       notes.cono = g-cono        and 
       notes.notestype = "zz"     and
       notes.primarykey = "icrxr" and
       notes.secondarykey = sapb.optvalue[1]
       no-lock no-error.
  if not avail notes then
   do:
    message "Invalid Sort Format Type for ICRXR " " --> " sapb.optvalue[1].
    pause 6.
    return.
   end.
  else
   do:
    p-summcounts = "a,a,a,a,a,a".
    overlay(p-optiontype,1,(length(notes.noteln[1]))) = notes.noteln[1].
    overlay(p-sorttype,1,  (length(notes.noteln[2]))) = notes.noteln[2].
    overlay(p-totaltype,1, (length(notes.noteln[3]))) = notes.noteln[3].
    overlay(p-summcounts,1,(length(notes.noteln[4]))) = notes.noteln[4].
   end.
 end. /* do sapb.optvalue[1] */
 else 
 if sapb.optvalue[1] = "99" then 
   do:
   run reportopts(input sapb.user5, 
                   input-output p-optiontype,
                   input-output p-sorttype,  
                   input-output p-totaltype,
                   input-output p-summcounts,
                   input-output p-register,
                   input-output  p-registerex).
   
   end.

end procedure. /* procedure formatoptions */

procedure extract_data:

if sapb.optvalue[4] = "b" or 
   sapb.optvalue[4] = "p" then
FOR EACH poeh use-index k-poeh WHERE 
        poeh.cono        = g-cono 
    AND poeh.whse        >= sapb.rangebeg[1] 
    AND poeh.whse        <= sapb.rangeend[1] 
    and poeh.receiptdt   >= ln-receiptdt-f  
    and poeh.receiptdt   <= ln-receiptdt-t  
    AND poeh.transtype   = "po" 
    AND poeh.stagecd     >= 5 
    AND poeh.stagecd     < 9 
    NO-LOCK,
  EACH poel  WHERE 
        poel.cono   = g-cono 
    and poel.pono = poeh.pono
    and poel.posuf = poeh.posuf
    AND poel.qtyrcv > 0 
    NO-LOCK:
     
    create xxwhse.
    assign xxwhse.whse     = poel.whse
           xxwhse.ztype    = "P"
           xxwhse.ordno    = poel.pono
           xxwhse.ordsuf   = poel.posuf
           xxwhse.lineno   = poel.lineno
           xxwhse.shipprod = poel.shipprod
           xxwhse.qtyrcv   = poel.qtyrcv
           xxwhse.rcvdt    = poeh.receiptdt
           xxwhse.zoperinit = poeh.rcvoperinit.
  
    end.

if sapb.optvalue[4] = "b" or 
   sapb.optvalue[4] = "w" then
FOR EACH wteh WHERE 
        wteh.cono        = g-cono 
    AND wteh.shiptowhse  >= sapb.rangebeg[1] 
    AND wteh.shiptowhse  <= sapb.rangeend[1]
    and wteh.receiptdt   >= ln-receiptdt-f
    and wteh.receiptdt   <= ln-receiptdt-t
    AND wteh.transtype   = "wt" 
    AND (wteh.stagecd = 5 OR wteh.stagecd = 6) 
    NO-LOCK,
  EACH wtel WHERE 
        (wteh.cono   = wtel.cono 
    and wteh.wtno    = wtel.wtno 
    and wteh.wtsuf   = wtel.wtsuf) 
    AND (wtel.cono  = g-cono 
    AND wtel.qtyrcv <> 0) 
  NO-LOCK:

     
    create xxwhse.
    assign xxwhse.whse     = wteh.shiptowhse
           xxwhse.ztype    = "w"
           xxwhse.ordno    = wteh.wtno
           xxwhse.ordsuf   = wteh.wtsuf
           xxwhse.lineno   = wtel.lineno
           xxwhse.shipprod = wtel.shipprod
           xxwhse.qtyrcv   = wtel.qtyrcv
           xxwhse.rcvdt    = wteh.receiptdt
           xxwhse.zoperinit = wteh.rcvoperinit.
  
    end.
end.

  
procedure print_detail:
define input parameter xxrecid as recid no-undo.
  
find xxwhse where recid(xxwhse)  = xxrecid no-lock no-error.
  
if not x-stream then
  do:
  display 
    xxwhse.whse    
    xxwhse.ztype   
    xxwhse.ordno   
    xxwhse.ordsuf   
    xxwhse.lineno   
    xxwhse.shipprod 
    xxwhse.qtyrcv 
    xxwhse.rcvdt   
    xxwhse.zoperinit 
  with frame f-detail width 178
    no-box no-labels.  

  down with frame f-detail.  
  end.
else
  do:
  display stream xpcd
    xxwhse.whse    
    xxwhse.ztype   
    xxwhse.ordno   
    xxwhse.ordsuf   
    xxwhse.lineno   
    xxwhse.shipprod 
    xxwhse.qtyrcv 
    xxwhse.rcvdt   
    xxwhse.zoperinit 
  with frame f-detail width 178
    no-box no-labels.  

  down stream xpcd with frame f-detail.  
  end.
end.  
/{&user_procedures}*/




