/***************************************************************************
 PROGRAM NAME: pozu.p
  PROJECT NO.: 
  DESCRIPTION: This program gives the user the capability
               to maintain their Vickers rebate records. This will add OE's 
               from a previous month. 

  DATE WRITTEN: 08/20/01.
  AUTHOR      : SunSource TA.  
***************************************************************************/

{g-all.i}
{g-oe.i}

def stream rebate.
def var v-orderno  like oeeh.orderno        no-undo.
def var v-ordersuf like oeeh.ordersuf       no-undo.
def var v-oeline   like oeel.lineno         no-undo.
def var v-filename as c format "x(42)"      no-undo.
def var v-tilda as c format "x" init "~~"   no-undo.
def var v-rundate  like oeeh.invoicedt      no-Undo.
def var v-custprod like  oeel.shipprod      no-undo.
def var s-printernm like sasp.printernm     no-undo.
def var p-orderno   like g-orderno           no-undo.
def var p-ordersuf  like g-ordersuf           no-undo.
def var g-orderno2 like g-orderno           no-undo.
def var g-orderno3 like g-orderno           no-undo.
def var g-orderno4 like g-orderno           no-undo.
def var g-orderno5 like g-orderno           no-undo.
def var g-ordersuf2 like g-ordersuf           no-undo.
def var g-ordersuf3 like g-ordersuf           no-undo.
def var g-ordersuf4 like g-ordersuf           no-undo.
def var g-ordersuf5 like g-ordersuf           no-undo.
define variable vicrec    as character format "x(251)" no-undo.
define variable rfilename as character format "x(24)" no-undo.
define variable ofilename as character format "x(60)" no-undo.
define variable co as integer no-undo.
define variable d_prodline like oeel.prodline no-undo.
def var d_shipprod as c format "x(15)" no-undo.
def var d_shipped as decimal format "99999999" no-undo.
def var d_price as decimal format "9999999.99" no-undo.
def var d_name as c format "x(20)" no-undo.
def var sic as c format "x(4)" no-undo.
def var c_no as c format "x(8)" no-undo.
def var d_line as i format 999 no-undo.
def var d_um as c format "x(4)" no-undo.
def var yyyy as c format "x(4)" no-undo.
def var creditdebit as c format "x(1)" no-undo.
def var bypass_sw as logical no-undo.
def var pwunt     as logical no-undo.
def new shared var p-list    as logical no-undo.
def var company as c format "x(3)" no-undo.
def var ordernum as char format "x(12)" no-undo.
def var x as i no-undo.
def var y as int  no-undo.         



form
   skip(2) 
   "Order Number   : "   at 20
   g-orderno  at 37  no-label 
   space(0) "-" space(0)
/* sx5.5 */
   g-ordersuf at 46 no-label
  "Order Number   : "   at 20
   g-orderno2  at 37  no-label 
   space(0) "-" space(0)
/*sx5.5 */
   g-ordersuf2 at 46 no-label
  "Order Number   : "   at 20
   g-orderno3  at 37  no-label 
   space(0) "-" space(0)
   g-ordersuf3 at 46 no-label
  "Order Number   : "   at 20
   g-orderno4  at 37  no-label 
   space(0) "-" space(0)
   g-ordersuf4 at 46 no-label
  "Order Number   : "   at 20
   g-orderno5  at 37  no-label 
   space(0) "-" space(0)
   g-ordersuf5 at 46 no-label
   "Update Filename: " at 20
   v-filename at 37 no-label
   "Run Date       : "   at 20
   v-rundate  at 37 no-label 
   skip(2) 
with frame f-pozu title "Vicker Rebate File Update".  


on f10 anywhere 
  do: 
     run zsdirpthlp.p.
  end.

mainzx:
do while true:

update g-orderno
       g-ordersuf
       g-orderno2
       g-ordersuf2
       g-orderno3
       g-ordersuf3
       g-orderno4
       g-ordersuf4
       g-orderno5
       g-ordersuf5
       v-filename
       v-rundate
 with frame f-pozu.

assign pwunt = no.
if v-rundate = ? then 
   do: 
      message "Run Date must be greater than 0 - pls correct and rerun".
      pause.
      leave.
   end.
/*
if (substring(v-filename,1,8) ne "00016700" and 
    substring(v-filename,1,8) ne "00164900" and 
    substring(v-filename,1,8) ne "00654900" and
    substring(v-filename,1,8) ne "00717400" and
    substring(v-filename,1,8) ne "000eaton") or 
   (substring(v-filename,1,9) ne "n00016700" and 
    substring(v-filename,1,9) ne "n00164900" and 
    substring(v-filename,1,9) ne "n00654900" and
    substring(v-filename,1,9) ne "n00717400" and
    substring(v-filename,1,9) ne "n000eaton")  
    then 
  do:
   message "File name must begin with 00016700 or 00164900".
   message "                       or 00654900 or 000eaton".
   pause.
   leave.  
  end.

if substring(v-filename,10,3) ne "air" and 
   substring(v-filename,10,3) ne "war" and 
   substring(v-filename,10,3) ne "adr" and 
   substring(v-filename,10,3) ne "nor" and
   substring(v-filename,10,3) ne "reb" then 
  do:
   message "File name must end with .air or .war or .nor or .reb ".
   pause.
   leave.  
  end.
*/
co = 1.
assign ofilename = "/usr/tmp/" + v-filename.
output stream rebate to value(ofilename) append.
if (substring(v-filename,1,8) = "000eaton" or
    substring(v-filename,1,9) = "n000eaton") then
   c_no = "72130503".
else    
if substring(v-filename,1,1) = "n" then 
   c_no = substring(v-filename,2,9).
else    
   c_no = substring(v-filename,1,8).

repeat y = 1 to 5:
 do: 
    if y = 1 then
     if g-orderno ne 0 then 
       do: 
       assign p-orderno  = g-orderno
              p-ordersuf = g-ordersuf.
       find oeeh where oeeh.cono = co  and
            oeeh.orderno  = p-orderno  and 
            oeeh.ordersuf = p-ordersuf and 
            oeeh.stagecd > 3 no-lock no-error.
            if not avail oeeh then 
               do: 
                message "Invalid Order - " p-orderno p-ordersuf " skipped". 
                pause.
                next.
               end.
        end.
     else
        next.
    else
    if y = 2 then
     if g-orderno2 ne 0 then 
       do: 
       assign p-orderno  = g-orderno2
              p-ordersuf = g-ordersuf2.
        find oeeh where oeeh.cono = co  and
            oeeh.orderno  = p-orderno  and 
            oeeh.ordersuf = p-ordersuf and 
            oeeh.stagecd > 3 no-lock no-error.
            if not avail oeeh then 
               do: 
                message "Invalid Order - " p-orderno p-ordersuf " skipped". 
                pause.
                next.
               end.
       end.
      else
        next.
    else
    if y = 3 then
     if g-orderno3 ne 0 then 
       do: 
        assign p-orderno  = g-orderno3
               p-ordersuf = g-ordersuf3.
         find oeeh where oeeh.cono = co  and
            oeeh.orderno  = p-orderno  and 
            oeeh.ordersuf = p-ordersuf and 
            oeeh.stagecd > 3 no-lock no-error.
            if not avail oeeh then 
               do: 
                message "Invalid Order - " p-orderno p-ordersuf " skipped". 
                pause.
                next.
               end.
       end.
     else 
       next.
    else
    if y = 4 then
     if g-orderno4 ne 0 then 
       do: 
        assign p-orderno  = g-orderno4
               p-ordersuf = g-ordersuf4.
        find oeeh where oeeh.cono = co  and
            oeeh.orderno  = p-orderno  and 
            oeeh.ordersuf = p-ordersuf and 
            oeeh.stagecd > 3 no-lock no-error.
            if not avail oeeh then 
               do: 
                message "Invalid Order - " p-orderno p-ordersuf " skipped". 
                pause.
                next.
               end.
       end.
     else 
        next.
    else
    if y = 5 then
     if g-orderno5 ne 0 then 
       do: 
        assign p-orderno  = g-orderno5
               p-ordersuf = g-ordersuf5.
        find oeeh where oeeh.cono = co  and
            oeeh.orderno  = p-orderno  and 
            oeeh.ordersuf = p-ordersuf and 
            oeeh.stagecd > 3 no-lock no-error.
            if not avail oeeh then 
               do: 
                message "Invalid Order - " p-orderno p-ordersuf " skipped". 
                pause.
                next.
               end.
       end.
    else    
       next.
       
oeeh-lookup:
for each oeeh where oeeh.cono = co  and
         oeeh.orderno  = p-orderno  and 
         oeeh.ordersuf = p-ordersuf and 
         oeeh.stagecd > 3   
         no-lock.  

assign company = "".         
find first arsc where arsc.cono = 1 and arsc.custno = oeeh.custno
     no-lock no-error.
if avail(arsc) then
    assign sic = string(arsc.siccd[1]).
           substring(vicrec,180,4) = sic.
 oeel-lookup:
 for each oeel where oeel.cono = co and oeel.orderno = oeeh.orderno and
          oeel.ordersuf = oeeh.ordersuf no-lock.
         
 if oeel.qtyship = 0 then
    next oeel-lookup.

 assign d_prodline = oeel.prodline 
        d_line = oeel.lineno
        d_um = oeel.unit
        d_shipprod = oeel.shipprod.

 if oeel.specnstype = "n" and
    d_shipprod = "000000" then
    d_shipprod = substring(oeel.proddesc,1,15).
       
 if (oeel.arpvendno <> 0 and
    (oeel.arpvendno = 70500019 or 
     oeel.arpvendno = 9793750  or
     oeel.arpvendno = 9853210  or
     oeel.arpvendno = 859000)) or  
    (oeel.arpvendno = 0 and
    (oeel.vendno = 70500019 or 
     oeel.vendno = 9793750  or
     oeel.vendno = 9853210  or
     oeel.vendno = 859000))  then  
     assign d_prodline = "VIC000".
       
 if oeel.specnstype ne "n" then do: 
     find first icsw where icsw.cono = co and icsw.whse = oeel.whse and
                icsw.prod = oeel.shipprod no-lock no-error.
     if avail(icsw) then do:
        d_prodline = if d_prodline = "vic000" then 
                        d_prodline
                     else
                        icsw.prodline.               
        d_shipprod = if icsw.vendprod ne "" then 
                        substring(vendprod,1,15)
                     else
                        substring(oeel.shipprod,1,15).
        end.
     end.

   if d_prodline begins "VIC" and 
       oeel.qtyship > 0 then do:
    find first smsn where smsn.cono = co and smsn.slsrep = oeel.slsrepout
         no-lock no-error.                         
    if available(smsn) then do:
       assign d_name = substring(smsn.name,1,20).
       end. 
    assign d_price = if oeel.discpct = 0 then oeel.price
                     else ((100 - oeel.discpct) / 100) * oeel.price
           d_shipped = oeel.qtyship
           creditdebit = if oeel.returnfl = yes then "-"
                       else "+".
    run move_common_info.
    end.
 
 if oeel.kitfl = yes then          
   do: 
    oeelk-lookup:
    for each oeelk where oeelk.cono = co     and
             oeelk.ordertype = "o"           and
             oeelk.orderno   = oeeh.orderno  and
             oeelk.ordersuf  = oeeh.ordersuf and 
             oeelk.lineno    = oeel.lineno  
         no-lock.
         
   if oeelk.qtyship eq 0 then
      next oeelk-lookup.
   if oeelk.statustype ne "i" then
      next oeelk-lookup.

   assign d_prodline = oeelk.arpprodline      
          d_um = oeelk.unit
          d_shipprod = oeelk.shipprod.
          
    if (oeelk.arpvendno <> 0 and
       (oeelk.arpvendno = 9793750 or
        oeelk.arpvendno = 9853210 or
        oeelk.arpvendno = 859000))
/*     or 
       (oeelk.arpvendno = 0 and
       (oeel.vendno = 9793750 or
        oeel.vendno = 9853210 or
        oeel.vendno = 859000))
*/
      then  
        assign d_prodline = "VIC000".

    if oeelk.specnstype = "n" and
      d_shipprod = "000000" then
      d_shipprod = substring(oeelk.proddesc,1,15).

   if oeelk.specnstype ne "n" then
     do:         
       find first icsw where icsw.cono = co and icsw.whse = oeelk.whse and
                  icsw.prod = oeelk.shipprod no-lock no-error.
       find icsp where
            icsp.cono = co and 
            icsp.prod = icsw.prod
            no-lock no-error.
       if avail(icsw) then
          assign d_prodline = if d_prodline = "vic000" then 
                                 d_prodline
                              else
                                 icsw.prodline.              
                 d_shipprod = if icsw.vendprod ne " " then
                                 substring(vendprod,1,15)
                              else 
                              if oeelk.specnstype ne "n" then  
                                 substring(icsp.descrip[1],1,15)
                              else 
                                 substring(oeelk.proddesc,1,15). 
     end.                      
    
    if d_prodline begins "VIC" and 
        oeelk.qtyship > 0 then do:
      find first smsn where smsn.cono = co and smsn.slsrep = oeel.slsrepout
        no-lock no-error.
      if available(smsn) then do:
         assign d_name = substring(smsn.name,1,20).
         end. 

      assign d_price = if oeel.discpct = 0 then oeelk.price
                       else ((100 - oeel.discpct) / 100) * oeelk.price
             d_shipped = oeelk.qtyship
             creditdebit = "+".
      if oeelk.specnstype ne "N" then
         run PDSC_pricing.   
      run move_common_info.
      end.
    end.  /* loop back to oeelk */  
   end. /* if kitfl = yes */
 end. /* for each oeel */
end. /* do for each oeeh */
end. /* inner do for do y */
output stream rebate close.
end. /* do while true mainx */


PROCEDURE PDSC_pricing:

if oeel.kitrollty = "b" or
   oeel.kitrollty = "p" then 
      leave.

find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 1 and pdsc.custno = oeeh.custno and
                pdsc.prod = oeelk.shipprod and oeelk.whse = oeeh.whse and
                pdsc.startdt le oeeh.invoicedt and
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
                
if not avail pdsc then do:
   find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 2 and pdsc.custno = oeeh.custno and
                pdsc.prod = "p-" + icsw.pricetype and
                pdsc.startdt le oeeh.invoicedt and 
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
   if not avail pdsc then do:              
   find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 2 and pdsc.custno = oeeh.custno and
                pdsc.prod = "l-" + string(icsw.arpvendno,"999999999999") +
                             icsw.prodline and
                pdsc.startdt le oeeh.invoicedt and 
                pdsc.whse = oeeh.whse and
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
   if not avail pdsc then do:
        find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 4 and pdsc.custtype = arsc.pricetype and
                pdsc.prod = icsw.pricetype and pdsc.whse = oeeh.whse and
                pdsc.startdt le oeeh.invoicedt and
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
        if not avail pdsc then do:
           find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 4 and pdsc.custtype = arsc.pricetype and
                pdsc.prod = icsw.pricetype and
                pdsc.startdt le oeeh.invoicedt and
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
     end.    
     end.        
     end.
     end.
     
if not avail pdsc then
   leave.
assign d_price = icsw.listprice.   
repeat x = 1 to 9:
if pdsc.qtybrk[1] = 0  then do:
   if pdsc.prctype = false then 
      d_price = ((100 - pdsc.prcdisc[1]) / 100) * d_price.
   else
      d_price = pdsc.prcmult[1].
   leave.
   end.         
       
if x < 9 then
if oeelk.qtyship lt pdsc.qtybrk[x] then do:
   if pdsc.prctype = false then 
      d_price = ((100 - pdsc.prcdisc[x]) / 100) * d_price.
   else
      d_price = pdsc.prcmult[x].
   leave.         
   end.            
end.
END PROCEDURE.

PROCEDURE move_common_info:

if (d_prodline begins "VIC") then 
  do:
  assign
  substring(vicrec,1,8)     = c_no
  substring(vicrec,9,9)     = substring(string(oeeh.custno,"ZZZZZZZZZZZ9"),4,9)
  substring(vicrec,18,30)   = arsc.name
  substring(vicrec,48,30)   = oeeh.shiptoaddr[1]
  substring(vicrec,78,30)   = oeeh.shiptoaddr[2]
  substring(vicrec,108,30)  = "                              "
  substring(vicrec,138,20)  = substring(oeeh.shiptocity,1,20) 
  substring(vicrec,158,3)   = oeeh.shiptost
  substring(vicrec,161,10)  = oeeh.shiptozip
  substring(vicrec,171,3)   = "US"
  substring(vicrec,174,2)   = substring(string(v-rundate),1,2) 
  substring(vicrec,176,4)   = string(year(v-rundate))
  substring(vicrec,180,4) = sic 
  substring(vicrec,184,15)  = d_shipprod
  substring(vicrec,199,8) = if oeeh.transtype = "RM" or
                               creditdebit = "-" then
                               string(d_shipped * -1, "9999999-")
                            else string(d_shipped, "99999999")
  substring(vicrec,207,20)  = d_name
  substring(vicrec,227,10) = string(d_price, "9999999.99-") 
  substring(vicrec,237,15) = string(oeeh.orderno) +
                             "-" + string(oeeh.ordersuf).    
  put stream rebate vicrec at 1 skip.
  end.
vicrec = "".
END PROCEDURE.
end.

