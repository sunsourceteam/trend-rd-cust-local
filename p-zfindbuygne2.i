/* --------------------------------------------------------------------
   Include :     p-zfindbuygne.i
   Description:  Determine buyer to be displayed
                
                 Parameter {1} is warehouse
                 Parameter {2} is item
                 parameter {3} is buyer returned
                 parameter {4} is vendor returned
                 parameter {5} is prodline returned
                 parameter {6} is search order returned
   Date Written: 4/20/99
   Changes Made:


----------------------------------------------------------------------- */
/*
def var z-peckinx as integer.
def var z-peckmax as integer init 9.
def var z-searchlevel as integer no-undo.

def var z-peckinglist as character format "x(4)" extent 9 init
  ["dros",
   "dren",
   "dhou",
   "dmcp",
   "datl",
   "decp",
   "dscp",
   "dwcp",
   "dxcp"].


define var xzbuyer like poeh.buyer         no-undo.
define var xzvend  like icsl.vendno        no-undo.
define var xzprodline like icsl.prodline   no-undo.

define var xz-whse like icsw.whse no-undo.
define buffer xzicsl for icsl.
define buffer xzicsw for icsw.
define buffer xzicsw2 for icsw.
  */
assign xzbuyer = "" 
       xzvend = 0
       xzprodline = ""
       z-searchlevel = 0.
    
find xzicsw where xzicsw.cono = g-cono and
                  xzicsw.whse = {1} and
                  xzicsw.prod = {2} no-lock no-error.
       
if avail xzicsw then
  assign {4} = xzicsw.arpvendno
         {5} = xzicsw.prodline.
    
    
find first xzicsl where xzicsl.cono     = g-cono
                    and xzicsl.whse     = {1}
                    and xzicsl.vendno   = {4}
                    and xzicsl.prodline = {5}             no-lock
                                                          no-error. 
if avail xzicsl then
  do:
  assign xzbuyer    = xzicsl.buyer
         xzvend     = xzicsl.vendno
         xzprodline = xzicsl.prodline
         z-searchlevel = 1.
  end.
else
  do:
  find first xzicsl where xzicsl.cono     = g-cono
                      and xzicsl.whse     = {1}
                      and xzicsl.vendno =  {4}                 no-lock
                                                               no-error.
      if avail xzicsl then  
        assign xzbuyer = xzicsl.buyer
               xzvend     = xzicsl.vendno
               xzprodline = xzicsl.prodline
               z-searchlevel = 2.
      else
        do:
        find first xzicsl where xzicsl.cono     = g-cono
                           and xzicsl.whse     = {1}
                           and xzicsl.prodline = {5} no-lock                                                    no-error.
        if avail xzicsl then  
          assign xzbuyer = xzicsl.buyer
                  xzvend     = xzicsl.vendno
                  xzprodline = xzicsl.prodline
                  z-searchlevel = 3.
        else 
          do:
          if avail xzicsw then
            do:
            if xzicsw.prodline = "" then
              if xzicsw.arptype = "w" then
                do:
                find xzicsw2 where xzicsw2.cono = g-cono and
                                   xzicsw2.whse = xzicsw.arpwhse and
                                   xzicsw2.prod = xzicsw.prod no-lock no-error.
                if avail xzicsw2 then                   
                  do:
                  assign xz-whse = xzicsw2.whse.
                  {p-zfindlinegne.i
                     xz-whse}.
                  end.
                end.
              if xzbuyer = "" then
                do z-peckinx = 1 to z-peckmax:
                  {p-zfindlinegne.i
                     z-peckinglist[z-peckinx]}.
                if xzbuyer <> "" then
                  do:
                  assign z-searchlevel = 7 + z-peckinx.
                  leave.
                  end.
               end.     
            end.
          end.
        end.     
      end.
if ({6} > z-searchlevel or ({6} = 0 and z-searchlevel <> 0)) and
     xzbuyer <> "" or xzvend <> 0 or xzprodline <> "" then
  assign {3} = xzbuyer
         {4} = xzvend
         {5} = xzprodline
         {6} = z-searchlevel.

