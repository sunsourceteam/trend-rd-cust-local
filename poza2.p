
/***************************************************************************
 PROGRAM NAME: poza2.p
      PROJECT: 99-16
  DESCRIPTION: This program builds the temp files for the poza programs
 DATE WRITTEN: 03/13/00
       AUTHOR: Sir Carl E. Graul Esq.
     MODIFIED: 

***************************************************************************/

{g-all.i}
{g-inq.i}
{g-ic.i}
{g-po.i}
{g-wt.i}
{g-oe.i}

{sts-poza1.i "shared"}

def var wt-no as char format "x(12)" no-undo.
def var wt-name as char format "x(25)" no-undo.
def var wt-out as char format "x(35)" no-undo.
def var ws-notesfl as char format "x(1)" no-undo.
def var ws-type as char format "x(2)" no-undo.
def var ws-unuseable as decimal format "-zzzzzzzzz9.99" no-undo.
def var ws-suboh as decimal format "-zzzzzzzzz9.99" no-undo.
def var ws-tiedto as char format "x" no-undo.

def var ho-taltno like poelo.orderaltno no-undo.
def var ho-taltsuf like poelo.orderaltsuf no-undo.
def var ho-qtyoh like ws-suboh no-undo.
def var ho-whse like icsw.whse no-undo.

def var i-x as integer init 0 no-undo.

/* switches */
def var sw-prod-found as logical init no no-undo.
def var sw-1-whse as logical init no no-undo.
def var sw-open-only as logical init yes no-undo.

def temp-table t-oh no-undo
    field whse like icsw.whse
    field oh like icsw.qtyonhand
    index k-oh
        oh descending
        whse.

if q-whse <> "" then
    assign sw-1-whse = yes.
assign sw-open-only = q-open.

run "160-clear-tmp".
run "10-initialize".
if sts-select = 2 or sts-select = 3 then do:
    run "130-qty".
    run "170-top3-l".
    run "180-top3-a".
end. /* if sts-select = 2 or sts-select = 3 */
if sts-select = 2 then do:
    if lookup(q-ptb,"B,P,b,p") > 0 then
        run "220-select2".
    if lookup(q-ptb,"B,T,b,t") > 0 then    
        run "110-load-wt".
end. /* if sts-delect = 2 */
if sts-select = 1  then do:
    assign q-ptb = "B".
    run "210-select1".
end. /* if sts-select = 1 */
if sts-select = 3 then do:
    assign q-ptb = "B".
    run "230-select3".
end. /* sts-select = 3 */


/* ############################ SUBROUTINES ########################## */

procedure 10-initialize:
assign to-qtyreservd = 0
       to-qtycommit = 0
       to-qtyonhand = 0
       to-avail = 0
       ws-unuseable = 0.
do i-x = 1 to 3:
    assign to-whse[i-x] =  ""
           to-qtyoh[i-x] = 0.
end. /* do i-x = 1 to 3 */
end procedure. /* 10-initialize */

procedure 100-load-po:
    assign ws-type = "P".
    assign ws-tiedto = "".
    find poeh where    
         poeh.cono = poel.cono
        and poeh.pono = poel.pono
        and poeh.posuf = poel.posuf
        no-lock no-wait no-error.
    if not available(poeh) then
        next.
    if poeh.stagecd = 0 then
        next.
    if sw-open-only = yes and poeh.stagecd > 4 then 
        next.
    if sw-1-whse = yes and q-whse <> poel.whse then
        next.
    if lookup(poeh.notesfl,"!,*") = 0 then
        assign ws-notesfl = " ".
    else
        assign ws-notesfl = poeh.notesfl.
    if poeh.transtype = "do" then 
        assign ws-type = ws-type + "D".
    assign ho-taltno = 0
           ho-taltsuf = 0.
    
    run "150-get-poelo".
    
    create t-ack1.
    assign t-ack1.tcono      = poel.cono
           t-ack1.ent-dt     = poel.enterdt
           t-ack1.due-dt     = poel.duedt
           t-ack1.fst-ack-dt = poel.reqshipdt
           t-ack1.exp-dt     = poel.expshipdt
           t-ack1.t-type     = ws-type
           t-ack1.ack-no     = string(poel.pono) + "-" + 
                                string(poel.posuf,"99")
           t-ack1.vend-no    = string(poel.vendno, ">>>>>>>>>>>>9")
           t-ack1.whse       = poel.whse
           t-ack1.ackcd      = substring(poel.user1,1,1)
           t-ack1.ackuser    = substring(poel.user1,4,8)
           t-ack1.ack-dt     = poel.user9
           t-ack1.qty        = poel.stkqtyord
           t-ack1.tied-out   = wt-out
           t-ack1.notesfl    = ws-notesfl
           t-ack1.trkno      = substring(poel.user2,1,35)
           t-ack1.ackno      = substring(poel.user2,38,35)
           t-ack1.tno        = poel.pono
           t-ack1.tsuf       = poel.posuf
           t-ack1.taltno     = ho-taltno
           t-ack1.taltsuf    = ho-taltsuf
           t-ack1.lineno     = poel.lineno
           t-ack1.prod       = poel.shipprod
           t-ack1.stagecd    = poeh.stagecd
           t-ack1.tiedto     = ws-tiedto.

 /*   message  "po tbl " t-ack1.tno t-ack1.tsuf.  
      message  "wt tbl " t-ack1.taltno t-ack1.taltsuf.
      pause.  
      message  "type "   t-ack1.t-type " " t-ack1.tiedto.
      pause.   */
      
    if poel.commentfl = yes then
           assign t-ack1.commfl = "c".
    else
           assign t-ack1.commfl = " ".
    assign t-ack1.ack-no-x = t-ack1.commfl + t-ack1.ack-no + t-ack1.notesfl.    
    if wt-out <> "" then
        assign t-ack1.tied = "T".
end procedure. /* 100-load-po */

procedure 110-load-wt:
for each icsd where 
         icsd.cono = g-cono
         no-lock:
    for each wtel where
             wtel.cono = icsd.cono
         and wtel.shiptowhse = icsd.whse
         and wtel.shipprod = q-prod 
         no-lock:
    assign ws-type = "T".
    assign ws-tiedto = "".
    assign ho-taltno = 0
           ho-taltsuf = 0.
     if wtel.ordertype = "w" then
       do:
         case wtel.ordertype:
              when "w" then 
                     assign wt-name = "Tied Work Order:"
                     ws-tiedto = "W".
         end case. /* case wtelo.ordertype */
         assign wt-out = trim(wt-name) + string(wtel.orderaltno).
         assign ho-taltno = wtel.orderaltno.
         assign ho-taltsuf = 0.
       end.  
    
    find wteh where
         wteh.cono = wtel.cono
        and wteh.wtno = wtel.wtno
        and wteh.wtsuf = wtel.wtsuf
        no-lock no-wait no-error.
    if not available (wteh) then
        next.
    if wteh.stagecd < 1 then
        next.
    if sw-open-only = yes and wteh.stagecd > 4 then
        next.
    if sw-1-whse = yes and q-whse <> wtel.shiptowhse then
        next.
    if wteh.transtype = "do" then
        assign ws-type = ws-type + "D".

    assign ws-notesfl = wteh.notesfl.
    
    if wtel.ordertype ne "w" then
       run "120-get-wtelo".
    
    create t-ack1.
    assign t-ack1.tcono      = wtel.cono
           t-ack1.ent-dt     = wtel.enterdt
           t-ack1.due-dt     = wtel.duedt
           t-ack1.fst-ack-dt = ?
           t-ack1.exp-dt     = ?
           t-ack1.t-type     = ws-type
           t-ack1.ack-no     = string(wtel.wtno) + "-" + 
                                string(wtel.wtsuf,"99")
           t-ack1.vend-no    = wtel.shipfmwhse
           t-ack1.whse       = wtel.shiptowhse
           t-ack1.ackcd      = ""
           t-ack1.ackuser    = ""
           t-ack1.ack-dt     = ? 
           t-ack1.qty        = wtel.qtyord
           t-ack1.tied-out   = wt-out
           t-ack1.notesfl    = ws-notesfl
           t-ack1.tno        = wtel.wtno
           t-ack1.tsuf       = wtel.wtsuf
           t-ack1.taltno     = ho-taltno
           t-ack1.taltsuf    = ho-taltsuf
           t-ack1.stagecd    = wteh.stagecd
           t-ack1.lineno     = wtel.lineno
           t-ack1.prod       = wtel.shipprod
           t-ack1.tiedto     = ws-tiedto.
    if wtel.commentfl = yes then
           assign t-ack1.commfl = "c".
    else
           assign t-ack1.commfl = " ".
    assign t-ack1.ack-no-x = t-ack1.commfl + t-ack1.ack-no + t-ack1.notesfl.
        if wt-out <> "" then
        assign t-ack1.tied = "T".
    end. /* for each trlines.wtel */
end. /* for each trstat.icad where icad.cono = 1 */
end procedure. /* 110-load-wt */

procedure 120-get-wtelo:
assign wt-no = "".
assign wt-name = "".
assign wt-out = "".
find wtelo where
     wtelo.cono = wtel.cono
    and wtelo.wtno = wtel.wtno
    and wtelo.wtsuf = wtel.wtsuf
    and wtelo.lineno = wtel.lineno
        no-lock no-wait no-error.
if not available (wtelo) then
    return.
assign wt-no = string(wtelo.orderaltno,">>>>>>>") + "-" +
            string(wtelo.orderaltsuf,"99").
assign ho-taltno = wtelo.orderaltno
       ho-taltsuf = wtelo.orderaltsuf.
            
case wtelo.ordertype:
    when "p" then 
            assign wt-name = "Tied Purchase Order:"
                   ws-tiedto = "P".
    when "o" then 
             assign wt-name = "Tied Customer Order:"
                    ws-tiedto = "O".
    when "f" then 
             assign wt-name = "Tied Value Added Order:"
                    ws-tiedto = "F".
    when "w" then 
              assign wt-name = "Tied Work Order:"
                     ws-tiedto = "W".
    when "t" then 
              assign wt-name = "Tied Transfer:"
                     ws-tiedto = "T".
end case. /* case wtelo.ordertype */
assign wt-out = trim(wt-name) + wt-no.
end procedure. /* 120-get-wtelo */
 
procedure 130-qty:
for each icsd where icsd.cono = g-cono no-lock:
    find icsw where
            icsw.cono = icsd.cono
        and icsw.whse = icsd.whse
        and icsw.prod = q-prod
        no-lock no-error.
     if not available(icsw) then
        next.
     assign to-qtyreservd = to-qtyreservd + icsw.qtyreservd.
     assign to-qtycommit = to-qtycommit + icsw.qtycommit.
     assign to-qtyonhand = to-qtyonhand + icsw.qtyonhand.
     assign ws-unuseable = ws-unuseable + icsw.qtycommit + icsw.qtyreservd.
end. /* for each icsd where icsd.cono = 1 */
assign to-avail = to-qtyonhand - (to-qtyreservd + to-qtycommit).
end procedure. /* 130-qty */

procedure 150-get-poelo:
assign wt-no = "".
assign wt-name = "".
assign wt-out = "".
find first poelo where
           poelo.cono = poel.cono
    and poelo.pono = poel.pono
    and poelo.posuf = poel.posuf
    and poelo.lineno = poel.lineno
        no-lock no-error.
if not available (poelo) then
    return.
assign wt-no = string(poelo.orderaltno,">>>>>>>") + "-" +
            string(poelo.orderaltsuf,"99").
assign ho-taltno = poelo.orderaltno
       ho-taltsuf = poelo.orderaltsuf.

case poelo.ordertype:
    when "p" then 
            assign wt-name = "Tied Purchase Order:"
                   ws-tiedto = "P".
    when "o" then 
            assign wt-name = "Tied Customer Order:"
                   ws-tiedto = "O".
    when "f" then 
            assign wt-name = "Tied Value Added Order:"
                   ws-tiedto = "F".
    when "w" then 
            assign wt-name = "Tied Work Order:"
                    ws-tiedto = "W".
    when "t" then 
            assign wt-name = "Tied Transfer:"
                   ws-tiedto = "T".
end case. /* case poelo.ordertype */
assign wt-out = trim(wt-name) + wt-no.
end procedure. /* 150-get-poelo */

procedure 160-clear-tmp:
for each t-ack1 
        exclusive-lock:
    delete t-ack1.
end. /* for each t-ack1 */
end procedure. /* 160-clear-tmp */

procedure 170-top3-l:
for each icsd where icsd.cono = g-cono no-lock:
    find icsw where
            icsw.cono = icsd.cono
        and icsw.whse = icsd.whse
        and icsw.prod = q-prod
        no-lock no-error.
     if not available(icsw) then
        next.
    assign ws-suboh =  icsw.qtyonhand - ws-unuseable.
    assign ws-unuseable = ws-unuseable - icsw.qtyonhand.    
    if ws-unuseable < 0 then
        assign ws-unuseable = 0.
    if ws-suboh < 0 then
        assign ws-suboh = 0.
    create t-oh.
    assign t-oh.whse = icsw.whse
           t-oh.oh = ws-suboh. 
end. /* for each icsd  */
end procedure. /* 170-top3-l */

procedure 180-top3-a:
assign i-x = 1.
for each t-oh 
        use-index k-oh no-lock:
    if t-oh.oh = 0 then 
        next.
    assign to-whse[i-x] = t-oh.whse
           to-qtyoh[i-x] = t-oh.oh
           i-x = i-x + 1.
    if i-x > 3 then 
        leave.
end. /* for each t-oh */
end procedure. /* 180-top3-a */

procedure 210-select1:
for each poel where 
         poel.cono = g-cono
        and poel.pono = q-pono
        and poel.posuf = q-posuf
        no-lock:
    run "100-load-po".
end. /* for each trlines.poel */
end procedure. /* 210-select1 */

procedure 220-select2:
for each  poel where 
          poel.cono = g-cono
      and poel.shipprod = q-prod
      no-lock:
    run "100-load-po".
end. /* for each trlines.poel */
end. /* 220-select2 */


procedure 230-select3:
for each poel where 
         poel.cono = g-cono
        and poel.pono = q-pono
        and poel.posuf = q-posuf
        and poel.shipprod = q-prod
        no-lock:
    run "100-load-po".
end. /* for each trlines.poel */
end procedure. /* 230-select3 */

