/***************************************************************************
 PROGRAM NAME: vaszp.p

 DATE WRITTEN: 04/13/14.
 AUTHOR      : tah 
***************************************************************************/

{g-all.i}
{g-ar.i}
{g-ap.i}
{va.gva}

def var fhit        as logical init no                no-undo.
def var freal       as logical init no                no-undo.
def var endsw       as logical init no                no-undo.
def var scrn_lns    as integer                        no-undo.
def var q-recid     as recid                          no-undo.
def var v-status    as logical init no                no-undo.
def var brwse-nbr   as integer                        no-undo.
def var h-rowid     as rowid                          no-undo.
def var p-rowid     as int                            no-undo.
def var x-functions as char format "x(70)"            no-undo.

def var v-model     as char format "x(40)"            no-undo.
def var v-modelh    as char format "x(40)"            no-undo.

def var v-prefix    as char format "x(24)"            no-undo.
def var v-plength   as int                            no-undo.
def var v-prodcat   like icsp.prodcat                 no-undo.
def var v-pricetype like icsw.pricetype               no-undo.
def var v-prodcatD   as char format "x(24)"           no-undo.
def var v-pricetypeD as char format "x(24)"           no-undo.
def var v-price      like icsw.listprice              no-undo.
def var v-bypassfl   as logical     format "Y/N"      no-undo.




def var x-cursorup        as i format "999" init 501     no-undo.   

def buffer b-notes for notes.

define temp-table t-vaszp no-undo 
    field model     as char format "x(40)"
    field prefix    as char format "x(24)"
    field plength   as int
    field prodcat   like icsp.prodcat
    field pricetype like icsw.pricetype
    field price     like icsw.listprice
    field bypassfl  as logical format "Y/N"
index k-ix
      model
      plength
      prefix.

define query  q-vaszp for t-vaszp scrolling.
define browse b-vaszp query q-vaszp
      display 
              string(t-vaszp.prefix,"x(24)") + " " +
              string(t-vaszp.prodcat,"x(4)") + " " +
              string(t-vaszp.pricetype,"x(4)") + "   " +      
              string(t-vaszp.price,">>>>>>9.99999") format "x(77)"                              
      with 12 down width 76 centered overlay no-hide no-labels
title 
"  Prefix                    Pcat Prtyp   FlatPrice                          ". 

form
    v-model at 5 label "Model"     

/*    b-vaszp    at row 5 col 2     */
/*    x-functions at row 19 col 5    no-label dcolor 2   */
    
with frame f-vaszp width 80 row 1 centered
     overlay title g-title side-labels.   
form
    v-modelh at 5 label "Model"     
    b-vaszp    at row 5 col 2  
    x-functions at row 19 col 5 no-label dcolor 2  
    
with frame f-vaszpB width 80 row 1 centered
     overlay title g-title side-labels.   


form 
    v-modelh      colon 12 label "Model    "         
    v-prefix     colon 12 label "Prefix   "  
    v-prodcat    colon 12 label "Prodcat  "
    help "Valid Product Category                                      [L]"  
    v-prodcatD    at 20    no-label
    v-pricetype  colon 12 label "Pricetype"
    help "PROD for product specific (ex. r8 series items)or Pricetype [L]"           
    v-pricetypeD at 20    no-label
    v-price      colon 12 label "FltPrc"
    v-bypassfl   colon 12 label "Bypass Ovr?"
    with frame f-vaszp2 width 55 row 09 col 14
      overlay title "Add Fixed Rate Master " side-labels. 


form 
    v-modelh      colon 12 label "Model    "         
    v-prefix     colon 12 label "Prefix   "  
    v-prodcat    colon 12 label "Prodcat  "
    help "Valid Product Category                                      [L]"  
    v-prodcatD   at 20    no-label
    v-pricetype  colon 12 label "Pricetype"
    help "PROD for product specific (ex. r8 series items)or Pricetype [L]"           v-pricetypeD at 20    no-label
    v-price      colon 12 label "FltPrc"
    v-bypassfl   colon 12 label "Bypass Ovr?"
  
    with frame f-vaszp3 width 55 row 09 col 14
       overlay title "Change Fixed Rate Master " side-labels. 


define buffer bw-icsec   for icsec.
define buffer bzz-icsec  for icsec. 
define buffer bzf-icsec  for icsec. 
define var    v-modelsbuildfl as logical no-undo.
define var    v-mfgbrowseopen as logical no-undo.
define var    v-mfgbrowsekeyed as logical no-undo.
define var    lk-prod         as char format "x(40)" no-undo. 


define new shared temp-table models no-undo 
    field name     as char format "x(39)" 
    field unitdt   as date  
    field descrip  as char format "x(30)" 
    index k-modix
          name.

define query q-models for bzz-icsec scrolling.

define browse b-models query q-models 
  display bzz-icsec.prod
          bzz-icsec.lastchgdt
  with 10 down centered overlay no-hide no-labels
   title                                                                   
 "   MFG Descriptions                      Last MaintDt    ". 
form 
  b-models at row 1 col 1
  with frame f-models width 80 row 6 col 15 overlay no-hide no-labels no-box.   

form
  "Search For: " at 1  
   lk-prod at 13
  with frame f-search  no-labels overlay row 9 column 22.


on leave of v-model in frame f-vaszp
do:
  close query q-models.
  assign v-mfgbrowseopen = false.
  hide frame f-models. 
  on cursor-up back-tab.    /**&&**/
  on cursor-down tab.
end.


on entry of v-model in frame f-vaszp
do:
  on cursor-up cursor-up.   /**&&**/
  on cursor-down cursor-down.
  assign v-mfgbrowsekeyed = false
         v-mfgbrowseopen  = false.
  if v-model <> " " and v-model <> "?" then do:  
   run DoMfgLu (input v-model,
                input "normal").
   assign v-mfgbrowsekeyed = true.
  end. 
  if v-mfgbrowseopen = false then do: 
   on cursor-up back-tab.
   on cursor-down tab.
  end.
end.


on any-key of b-models do: 
 if {k-cancel.i} or {k-jump.i} then do: 
   hide frame f-models.
   hide frame f-search.
   display v-model with frame f-vaszp.
   apply "entry" to v-model in frame f-vaszp.
   apply "window-close" to current-window.
   on cursor-up back-tab.
   on cursor-down tab.
 end.   
 if ({k-return.i} or {k-accept.i}) and avail bzz-icsec then do: 
   assign v-model = bzz-icsec.prod.
   display v-model with frame f-vaszp.
   hide frame f-models.
   apply "entry" to v-model in frame f-vaszp.
   apply "window-close" to current-window. 
   on cursor-up back-tab.
   on cursor-down tab.
 end.
end.



on any-key of b-vaszp do:
  do transaction:
    freal = false.
   if not {k-cancel.i} then 
      display 
        v-modelh
        b-vaszp 
      with frame f-vaszpB.

    if {k-cancel.i} or keylabel(lastkey) = "PF1" or
      keylabel(lastkey) = "F11" or 
      keylabel(lastkey) = "return" then do:
      run "clear-browse".
       
      if keylabel(lastkey) = "f11" then
        endsw = yes.
      else
        apply "entry" to v-modelh.
     
      apply "window-close" to current-window. 
    end.
   else
   if {k-func7.i} and g-secure > 2 then do: 
     assign 
       v-prefix    = " "
       v-prodcat   = ""
       v-pricetype = ""
       v-price     = 0
       v-plength   = 0
       v-bypassfl  = no.
     amain:
     do while true :  
      display 
        v-modelh
        v-prefix
        v-prodcat
        v-pricetype
        v-price
        v-bypassfl
 
       with frame f-vaszp2. 
       
       update 
         v-prefix
         v-prodcat 
         v-pricetype
         v-price
         v-bypassfl
       with frame f-vaszp2.
  
       if {k-cancel.i} then  do: 
         apply "window-close" to current-window. 
         leave amain.
       end.
       else 
       if {k-accept.i} or {k-return.i} then 
         if v-prefix = "" or v-prodcat = " " or v-pricetype = " " then do:
           message "All Fields are required".
           next amain.
         end.
      
       if v-prefix <> "" and v-prodcat <> " " and v-pricetype <> " " then do:
          find notes where
               notes.cono         = g-cono  and 
               notes.notestype    = "<p"    and
               notes.primarykey   = v-modelh and
               notes.secondarykey = v-prefix 
          no-error.
          if avail notes then do:
            message "Record already exists ".
            next amain.
          end.

          {sasta.gfi "k" "v-pricetype" no}
          if not avail sasta and v-pricetype <> "PROD" then do:
            run err.p (4006).
            next amain.
          end.   
          else do:
            assign v-pricetypeD = if v-pricetype = "PROD" then
                                    "Product Specific"
                                  else  
                                     sasta.descrip.
            display v-pricetypeD with frame f-vaszp2.
          end.
         
          {sasta.gfi "c" "v-prodcat" no}
          if not avail sasta  then do:
            run err.p (4020).
            next amain.
          end.   
          else do:
            assign v-pricetypeD = sasta.descrip.
            display v-pricetypeD with frame f-vaszp2.
          end.

          find notes where
               notes.cono         = g-cono  and 
               notes.notestype    = "<p"    and
               notes.primarykey   = v-modelh and
               notes.secondarykey = v-prefix 
          no-error.
          
          if not avail notes then do:
            create notes.
            assign
               notes.cono         = g-cono  
               notes.notestype    = "<p"    
               notes.primarykey   = v-modelh 
               notes.secondarykey = v-prefix
               notes.noteln[1]    = v-prodcat
               notes.noteln[2]    = v-pricetype 
               notes.noteln[3]    = string(v-price)
               notes.xxl1         = v-bypassfl.
          end.

          create t-vaszp.
          assign 

            t-vaszp.model      = v-modelh 
            t-vaszp.prefix     = v-prefix
            t-vaszp.prodcat    = v-prodcat
            t-vaszp.pricetype  = v-pricetype
            t-vaszp.plength    = length(v-prefix)
            t-vaszp.price      = v-price
            t-vaszp.bypassfl   = v-bypassfl. 
          if not fhit then do:
            p-rowid = 1. 
            fhit = yes.
          end. 
          else    
            assign p-rowid = current-result-row("q-vaszp").                  

         
          if v-prefix <> " " and 
             v-prodcat <> " " and 
             v-pricetype <> " " then do:
            if p-rowid < 13 and p-rowid > 1 then 
              p-rowid = p-rowid + 1.
            close query q-vaszp.
            open query q-vaszp preselect each t-vaszp use-index k-ix.
            enable  b-vaszp with frame f-vaszpB. 
            v-status = self:select-row(p-rowid) no-error.
            get current q-vaszp.
            display b-vaszp with frame f-vaszpB.
            leave amain.                          
          end.                
        end.
      end. 
    end.
    else if {k-func8.i} and g-secure > 2 then do: 
      if fhit = no then do: 
        run err.p(1016).
        leave.
      end.   

      assign 
        v-prefix    =  t-vaszp.prefix     
        v-prodcat   =  t-vaszp.prodcat    
        v-pricetype =  t-vaszp.pricetype  
        v-plength   =  t-vaszp.plength
        v-price     =  t-vaszp.price
        v-bypassfl  =  t-vaszp.bypassfl. 
 

          {sasta.gfi "k" "v-pricetype" no}
          if not avail sasta and v-pricetype <> "PROD" then do:
          end.   
          else
            assign v-pricetypeD = if v-pricetype = "PROD" then
                                    "Product Specific"
                                  else
                                    sasta.descrip .
          
          {sasta.gfi "c" "v-prodcat" no}
          if not avail sasta then do:
          end.   
          else
            assign v-prodcatD = sasta.descrip .
     
     cmain:
     do while true :  
 

      display 
        v-modelh
        v-prefix
        v-prodcatD
        v-prodcat
        v-pricetype
        v-pricetypeD
        v-price
        v-bypassfl
        
      with frame f-vaszp3.

      update v-prodcat
             v-pricetype
             v-price
             v-bypassfl
      with frame f-vaszp3
        editing:
          readkey.       
          if {k-cancel.i} then do: 
            apply "window-close" to current-window. 
            leave.
          end.

          if {k-accept.i} or {k-return.i} then do:
            self:select-focused-row().
            assign v-prodcat       = input v-prodcat
                   v-price         = input v-price
                   v-pricetype     = input v-pricetype
                   v-bypassfl      = input v-bypassfl.

        
            find notes where
                 notes.cono         = g-cono     and
                 notes.notestype    = "<p"       and
                 notes.primarykey   = v-modelh   and
                 notes.secondarykey = v-prefix   no-error.

            {sasta.gfi "k" "v-pricetype" no}
            if not avail sasta and v-pricetype <> "PROD" then do:
              run err.p (4006).
              next .
            end.   
            else do:
              assign v-pricetypeD = if v-pricetype = "PROD" then
                                      "Product Specific"
                                    else
                                      sasta.descrip .
              display v-pricetypeD with frame f-vaszp3.
            end.
            {sasta.gfi "c" "v-prodcat" no}
            if not avail sasta then do:
              run err.p (4020).
              next .
            end.   
            else do:
              assign v-prodcatD =  sasta.descrip .
              display v-prodcatD with frame f-vaszp3.
            end.
 
        
            assign
               notes.noteln[1]    = v-prodcat
               notes.noteln[2]    = v-pricetype 
               notes.noteln[3]    = string(v-price)
               notes.xxl1         = v-bypassfl.
            assign
               t-vaszp.prodcat   = v-prodcat
               t-vaszp.pricetype = v-pricetype
               t-vaszp.price     = v-price
               t-vaszp.bypassfl  = v-bypassfl.
            self:refresh().
          end.
        apply lastkey.

       
        
        end. /*** editing **/

        display b-vaszp with frame f-vaszpB.
        leave cmain.
      end.                
 
      
      
      end. /** k-func8.i **/
    else    
    if {k-func9.i} and g-secure > 2 then do: 
        
      assign 
        v-prefix    =  t-vaszp.prefix     
        v-prodcat   =  t-vaszp.prodcat    
        v-pricetype =  t-vaszp.pricetype  
        v-plength   =  t-vaszp.plength
        v-bypassfl  =  t-vaszp.bypassfl. 
 
      find notes where
           notes.cono         = g-cono     and
           notes.notestype    = "<p"       and
           notes.primarykey   = v-modelh   and
           notes.secondarykey = v-prefix   no-error.
 
      if not avail notes then do:
        run err.p(1016).
        leave.      
      end.
      if avail notes then do:
        message "Are you sure you want to Delete ?" 
                 skip 
                 "Prefix ? " + t-vaszp.prefix
        view-as alert-box question buttons yes-no
        update z as logical.
        if z then do: 
          delete t-vaszp.
          delete notes.
          v-status = self:delete-selected-row(1). 
          scrn_lns = self:num-entries.
          if scrn_lns = 0 then do: 
            /* apply "window-close" to current-window.   */
          end.
          else do:
            self:select-focused-row().
            assign p-rowid = current-result-row("q-vaszp").                  
          end.
        end. /*** if z **/
        else 
          next.
      end. /** if avail notes **/     
    end. /** k-func9.i **/
  end. /* do transaction */

end. /*** big do any-key **/

/****** Main Block ****************/


assign endsw = no.
main:             
do while endsw = false on endkey undo, next main:
disable b-vaszp with frame f-vaszpB.
hide frame f-models.
hide frame f-vaszpB.
 

endsw = no.
fhit = no.
on cursor-up back-tab.
on cursor-down tab.
 
status input off.
status default
 "Arrow Up/Down -  F7 - F8 - F9  or F4-Cancel"
   in window current-window.

if g-secure > 2 then 
  do:
  assign x-functions = 
    " F7-Add   F8-Change   F9-Delete                          ".
  end.
else  
  do:
  assign x-functions =  
    "                                                         ".
  end.
/* display x-functions with frame f-vaszp. */

umain:
do while true:   

  hide frame f-vaszpB.
  hide frame f-vaszp2.
  hide frame f-vaszp3.
  
  update                     
    v-model
  with frame f-vaszp
    editloop: 
    editing:
      readkey.  

      if {k-cancel.i} or {k-jump.i} then  do:
        run "clear-browse".
        hide frame f-models.
        apply "window-close" to current-window.
        hide frame f-vaszpB.
        leave main.
      end.


      if {k-sdileave.i 
       &functions  = " can-do('f7',keylabel(lastkey))     or 
                       can-do('f8',keylabel(lastkey))     or
                       can-do('f9',keylabel(lastkey))     or 
                       can-do('f10',keylabel(lastkey))    or
                       can-do('ctrl-d',keylabel(lastkey)) or
                       can-do('ctrl-o',keylabel(lastkey)) or 
                       can-do('ctrl-p',keylabel(lastkey)) or "           
       &fieldname     = "v-model"               
       &completename  = "v-model"} then do:      
        assign v-model = input v-model.
        hide frame f-models.
        if (keylabel(lastkey) = "cursor-left") /* or 
            lastkey = 501)*/  and   
           v-model:cursor-offset = 1 then do:          
           next-prompt v-model with frame f-vaszp. 
           next.  
        end. 
      end.

      if frame-field = "v-model" and 
       (can-do('f7',keylabel(lastkey))     or 
        can-do('f8',keylabel(lastkey))     or
        can-do('f9',keylabel(lastkey))     or 
        can-do('f10',keylabel(lastkey))    or
        can-do('ctrl-d',keylabel(lastkey)) or
        can-do('ctrl-o',keylabel(lastkey)) or 
        can-do('ctrl-p',keylabel(lastkey))) then do: 
        assign v-model = input v-model. 
      end.
      else 
      if frame-field = "v-model" then do: 
        hide frame f-models.
       if not can-do("9,13,401,404,501,502,503,504,508,507",string(lastkey)) 
        then 
        assign v-mfgbrowsekeyed = false.
       if (not can-do("9,21,13,501,502,503,504,507,508",string(lastkey)) and
           keyfunction(lastkey) ne "go" and
           keyfunction(lastkey) ne "end-error") then do: 
       /* user has keyed in non-movement key */
         apply lastkey.
         if frame-field <> "v-model" then
           next editloop.
         
         assign v-model.
         assign v-model = input v-model. 
         
         run DoMfgLu (input v-model,
                      input "normal").
         assign v-mfgbrowsekeyed = false.
         
         next editloop.
       end.
       else 
       if ((keylabel(lastkey) = "cursor-left" or 
            lastkey = 501)   and 
          v-model:cursor-offset = 1) and
          v-mfgbrowsekeyed = false then do:           
          on cursor-up back-tab.   
          on cursor-down tab.
          assign v-mfgbrowseopen = false.
          hide frame f-models.
          next-prompt v-model with frame f-vaszp.
          apply "entry" to v-model in frame f-vaszp.
          next.
       end.
       else if (lastkey = 501 or
                lastkey = 502 or
                lastkey = 507 or
                lastkey = 508) and v-mfgbrowseopen = true then do: 
           run MoveMfgLu (input lastkey).
          assign v-mfgbrowsekeyed = true.
          next.
       end.
       else 
       if keyfunction(lastkey) = "end-error" and 
          v-mfgbrowsekeyed    = true then do: 
          on cursor-up back-tab.   
          on cursor-down tab.
          hide frame f-models.
          next.
       end.
       else 
       if (lastkey = 13 or keyfunction(lastkey) = "go")  
           and not v-mfgbrowsekeyed  then do:  
         assign v-model = input v-model.
         display v-model with frame f-vaszp.
         assign v-mfgbrowsekeyed = false.
         close query q-models.
         on cursor-up back-tab.   
         on cursor-down tab.
         hide frame f-models.
       end.
       else
       if (lastkey = 13 or keyfunction(lastkey) = "go")
           and v-mfgbrowsekeyed and avail bzz-icsec then do: 
         assign v-model = bzz-icsec.prod.
         display v-model with frame f-vaszp.
         assign v-mfgbrowsekeyed = false.
         close query q-models.
         on cursor-up back-tab.    /**&&**/
         on cursor-down tab.
         hide frame f-models.
         end.
       else do:
         assign v-mfgbrowsekeyed = false.
         hide frame f-models.
        end.
  
      end.
    
      apply lastkey.
 
    
    end.

  assign v-modelh = v-model.
/*
  if x-security < 1 or
     x-security > 5 then do:
    message "Security level must be > 0 and < 5".
    bell.
    next umain.
  end.
*/
  leave umain.
end.
hide frame f-models.
run "extract-recs".

on cursor-up cursor-up.
on cursor-down cursor-down.
open query q-vaszp preselect each t-vaszp use-index k-ix.
display 
  v-modelh 
  b-vaszp 
  x-functions 
  with frame f-vaszpB.
enable b-vaszp with frame f-vaszpB.
wait-for window-close of current-window.
on cursor-up back-tab.
on cursor-down tab.
close query q-vaszp.
hide frame f-vaszpB.
hide frame f-vaszp2.
hide frame f-vaszp3.





end. /****** end do while loop******/

status default. 
 {j-all.i "frame f-vaszp"} 

/***** procedure section *******/

procedure extract-recs:
  fhit = no.
  run "clear-browse".  
        
  for each notes where
           notes.cono         = g-cono     and
           notes.notestype    = "<p"       and
           notes.primarykey   = v-model    no-lock:
    create t-vaszp.
    assign 
      fhit    = yes
      t-vaszp.model      = notes.primarykey 
      t-vaszp.prefix     = notes.secondarykey
      t-vaszp.prodcat    = notes.noteln[1]
      t-vaszp.pricetype  = notes.noteln[2]
      t-vaszp.price      = dec (notes.noteln[3])
      t-vaszp.plength    = length(notes.secondarykey)
      t-vaszp.bypassfl   = notes.xxl1. 
  end. /*** end of for each  *****/
end.

procedure clear-browse:
  close query q-vaszp.
  for each t-vaszp: 
    delete t-vaszp.
  end.
  display  b-vaszp  with frame f-vaszpB.
end. /**** end procedure ***/


/* -------------------------------------------------------------------------  */
 Procedure MoveMfgLu:
/* -------------------------------------------------------------------------  */
  define input parameter ix-lastkey as integer                 no-undo.

  on cursor-up cursor-up. 
  on cursor-down cursor-down.
  enable b-models with frame f-models.
  apply lastkey to b-models in frame f-models.
  if lastkey = 502 and v-mfgbrowsekeyed = false then do:    
     apply x-cursorup to b-models in frame f-models.      
  end. 
  apply "focus" to v-model in frame f-vaszp.

end.                               

/* --------------------&&&--------------------------------------------------  */
Procedure DoMfgLu:
/* -------------------------------------------------------------------------  */
  define input parameter ix-model   as character format "x(24)" no-undo.
  define input parameter ix-runtype as character                no-undo.

  assign v-modelsbuildfl = true.

  empty temp-table models.
  open query q-models  
    for each bzz-icsec use-index k-icsec where  
             bzz-icsec.cono = g-cono  and 
             bzz-icsec.rectype = "ZM" and 
             bzz-icsec.prod begins ix-model and 
             bzz-icsec.prod <> ""
             no-lock.

  assign  v-mfgbrowseopen = true.
          v-modelsbuildfl = false.
  display b-models with frame f-models.
  apply "focus" to v-model in frame f-vaszp. 


end.                               





  
 

