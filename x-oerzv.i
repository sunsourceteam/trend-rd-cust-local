/{&user_temptable}*/

/*   Variables from screen */
def  var b-ovrdate        as date format "99/99/9999"      no-undo.
def  var e-ovrdate        as date format "99/99/9999"      no-undo.
def  var b-region         as c    format "x(1)"            no-undo.
def  var e-region         as c    format "x(1)"            no-undo.
def  var b-district       as c    format "x(3)"            no-undo.
def  var e-district       as c    format "x(3)"            no-undo.
def  var b-takenby        as c    format "x(4)"            no-undo.
def  var e-takenby        as c    format "x(4)"            no-undo.
def  var b-slsrep         as c    format "x(4)"            no-undo.
def  var e-slsrep         as c    format "x(4)"            no-undo.

def  var p-format         as integer                       no-undo.
def  var p-ovrtype        as c    format "x(1)"            no-undo.

def var p-regval    as c                             no-undo.
def var s-regdist   as c    format "x(4)"            no-undo.
def var s-name      as c    format "x(20)"           no-undo.
def var s-slsrep    as c    format "x(4)"            no-undo.
def var s-takenby   as c    format "x(25)"           no-undo.
def var s-order     as c    format "x(10)"           no-undo.
def var s-transtype as c    format "x(2)"            no-undo.
def var s-pline     as c    format "x(6)"            no-undo.
def var s-prod      as c    format "x(24)"           no-undo.
def var s-proddesc  as c    format "x(24)"           no-undo.
def var s-qtyord    as i    format ">>>>>>>>9"       no-undo.
def var s-stndcost  as de   format ">>>>>9.99"       no-undo.
def var s-overcost  as de   format ">>>>>9.99"       no-undo.
def var s-comment1  as c    format "x(60)"           no-undo.
def var s-comment2  as c    format "x(60)"           no-undo.
def var s-comment3  as c    format "x(60)"           no-undo.
def var s-comment4  as c    format "x(60)"           no-undo.
def var s-comment5  as c    format "x(60)"           no-undo.
def var s-comment6  as c    format "x(60)"           no-undo.
def var s-comment7  as c    format "x(60)"           no-undo.
def var w-name      as c    format "x(20)"           no-undo.
def var w-custno    like    arsc.custno              no-undo.
def var w-shipto    like    arss.shipto              no-undo.
def var com-cnt     as i    format "99"              no-undo.
def var s-space     as c    format "x(1)"            no-undo.
def var x-name      as c    format "x(20)"           no-undo.
def var s-custno    like    arsc.custno              no-undo.
def var s-shipto    like    arss.shipto              no-undo.
def var x-comment1  as c                             no-undo.
def var x-comment2  as c                             no-undo.
def var w-noteln1   like    com.noteln[1]            no-undo.
def var w-noteln2   like    com.noteln[2]            no-undo.
def var w-noteln3   like    com.noteln[3]            no-undo.
def var w-noteln4   like    com.noteln[4]            no-undo.
def var w-noteln5   like    com.noteln[5]            no-undo.
def var w-noteln6   like    com.noteln[6]            no-undo.
def var w-noteln7   like    com.noteln[7]            no-undo.


def temp-table ovr no-undo
  FIELD regdist     as c    format "x(4)"
  FIELD custno      like    arsc.custno     /**/
  FIELD shipto      like    arss.shipto     /**/
  FIELD name        as c    format "x(20)"
  FIELD slsrep      as c    format "x(4)"
  FIELD takenby     as c    format "x(25)"
  FIELD order       as c    format "x(10)"
  FIELD transtype   as c    format "x(2)"
  FIELD pline       as c    format "x(6)"
  FIELD prod        as c    format "x(24)"
  FIELD proddesc    as c    format "x(24)"
  FIELD qtyord      as i    format "->>>>>>>>9"
  FIELD stndcost    as de   format "->>>>>>9.99"
  FIELD overcost    as de   format "->>>>>>9.99"
  FIELD enterdt     as date format "99/99/99"
  FIELD ovrtype     as c    format "x(1)"
  FIELD comment1    as c    format "x(60)"
  FIELD comment2    as c    format "x(60)"
  FIELD comment3    as c    format "x(60)"
  FIELD comment4    as c    format "x(60)"
  FIELD comment5    as c    format "x(60)"
  FIELD comment6    as c    format "x(60)"
  FIELD comment7    as c    format "x(60)"
  INDEX k-rgn
        regdist
        takenby
        name
  INDEX k-takenby
        takenby
        name
        regdist
  INDEX k-name
        name
        regdist
        takenby.
           
/{&user_temptable}*/
 
/{&user_drpttable}*/
/* Use to add fields to the report table */
    FIELD ovrrecid        AS recid   
/{&user_drpttable}*/
 
/{&user_forms}*/

form
  "Rgn/"             at   1
  "Acct"             at  29
  "Prod"             at  77

  "Dist"             at   1
  "Customer Name"    at   7
  "Mgr"              at  29
  "Taken By"         at  35
  "Order"            at  62
  "Tp"               at  74
  "Line"             at  77
  "Product"          at  85
  "Description"      at 111
  "Qty Ord"          at 137 
  "Stnd Cost"        at 148
  "Ovr Cost"         at 159
  
  "----"                       at   1
  "--------------------"       at   7
  "----"                       at  29
  "-------------------------"  at  35
  "---------"                  at  62
  "--"                         at  74
  "------"                     at  77
  "------------------------"   at  85
  "------------------------"   at 111
  "---------"                  at 137 
  "---------"                  at 148
  "---------"                  at 159
  
  with frame f-header no-underline no-box no-labels page-top down width 178.

form
  s-regdist          at   1
  s-name             at   7
  s-slsrep           at  29
  s-takenby          at  35
  s-order            at  62
  s-transtype        at  74
  s-pline            at  77
  s-prod             at  85
  s-proddesc         at 111
  s-qtyord           at 137
  s-stndcost         at 148
  s-overcost         at 159
  "CTRL-A Comments:" at  90
  s-comment1         at 108
  
 with frame f-detail no-underline no-box no-labels down width 178.
 
form
  s-comment2          at 108
 with frame f-com2 no-underline no-box no-labels down width 178.

form
  s-comment3          at 108
 with frame f-com3 no-underline no-box no-labels down width 178.

form
  s-comment4          at 108
 with frame f-com4 no-underline no-box no-labels down width 178.

form
  s-comment5          at 108
 with frame f-com5 no-underline no-box no-labels down width 178.

form
  s-comment6          at 108
 with frame f-com6 no-underline no-box no-labels down width 178.

form
  s-comment7          at 108
 with frame f-com7 no-underline no-box no-labels down width 178.

form
  s-space             at 1
 with frame f-space no-underline no-box no-labels down width 178.
 
/{&user_forms}*/

 
/{&user_extractrun}*/

if sapb.rangebeg[1] = "" then
  b-ovrdate = today.
else do:
  v-datein = sapb.rangebeg[1].
  {p-rptdt.i}
  if string(v-dateout) = v-lowdt then
     b-ovrdate  = today.
  else
     b-ovrdate  = v-dateout.
end.

if sapb.rangeend[1] = "" then
   e-ovrdate  = today.
else do:
   v-datein = sapb.rangeend[1].
   {p-rptdt.i}
   if string(v-dateout) = v-lowdt then
      e-ovrdate = today.
   else
      e-ovrdate = v-dateout.
end.
                    
assign b-region   = sapb.rangebeg[2]
       e-region   = sapb.rangeend[2]
       b-district = sapb.rangebeg[3]
       e-district = sapb.rangeend[3]
       b-takenby  = sapb.rangebeg[4]
       e-takenby  = sapb.rangeend[4]
       b-slsrep   = sapb.rangebeg[5]
       e-slsrep   = sapb.rangeend[5]
       
       p-format   = int(sapb.optvalue[1])
       p-export   = sapb.optvalue[2].
       p-ovrtype  = sapb.optvalue[3].
assign p-detail   = "D".

/* USED FOR TESTING
def var g-cono as i format 9999.
assign g-cono = 1.
assign b-ovrdate  = 06/01/05 
       e-ovrdate  = 06/30/05 
       b-region   = "N"
       e-region   = "N"
       b-district = "400"
       e-district = "400"
       b-takenby  = "AAAA"
       e-takenby  = "ZZZZ"
       b-slsrep   = "0000"
       e-slsrep   = "9999"
       p-sort     = "D".
*/

run formatoptions.

for each oeel
       fields(cono
              transtype
              statustype
              specnstype
              enterdt
              transtype
              user5
              user4
              orderno
              ordersuf
              lineno
              qtyord
              prodcost
              glcost
              costoverfl
              whse
              vendno
              arpvendno
              custno
              slsrepout
              shipprod)
 
       use-index k-oeel     where oeel.cono = 1 and
                                  oeel.ordersuf = 0 no-lock:
  if oeel.slsrepout >= "0982" and
     oeel.slsrepout <= "0999" then next.

  if  oeel.statustype <> "C" and
     (oeel.specnstype <> "L" and
      oeel.specnstype <> "N") and
      /*
      oeel.enterdt >= b-ovrdate and
      oeel.enterdt <= e-ovrdate and
      */
      oeel.transtype <> "RM" and
      substring(oeel.user5,1,1) <> " " and
      oeel.ordersuf = 0  then do:
    find oeeh where oeeh.cono     = oeel.cono and
                    oeeh.orderno  = oeel.orderno and
                    oeeh.ordersuf = oeel.ordersuf
                    no-lock no-error.
    if not avail oeeh then next.
    
    find pder where pder.cono     = g-cono + 500 and
                    pder.orderno  = oeel.orderno and
                    pder.ordersuf = oeel.ordersuf and
                    pder.lineno   = oeel.lineno
                    no-lock no-error.
    if avail pder then next.
    
    assign w-noteln1 = ""
           w-noteln2 = ""
           w-noteln3 = ""
           w-noteln4 = ""
           w-noteln5 = ""
           w-noteln6 = ""
           w-noteln7 = "".
           
    find com use-index k-com where com.cono = g-cono and
                                   com.comtype = "pu" and
                                   com.orderno = oeel.orderno and
                                   com.ordersuf = oeel.ordersuf and
                                   com.lineno = oeel.lineno and
                                   com.transdt >= b-ovrdate and
                                   com.transdt <= e-ovrdate
                                   no-lock no-error.
    if not avail com and substring(oeel.user5,1,1) = "" then next.
    if not avail com and 
       (oeel.enterdt < b-ovrdate or oeel.enterdt > e-ovrdate) then next.

    if not avail com and oeeh.transtype = "DO" then
       assign w-noteln1 = "'DO' ORDER TYPE".
    if not avail com and oeeh.transtype <> "DO" then
       assign w-noteln1 = "THIS OE DOES NOT CONTAIN COMMENTS".

    if not avail com then
       find com use-index k-com where com.cono = g-cono and
                                   com.comtype = "pu" and
                                   com.orderno = oeel.orderno and
                                   com.ordersuf = oeel.ordersuf and
                                   com.lineno = oeel.lineno
                                   no-lock no-error.
    
    if avail com then
    assign w-noteln1 = com.noteln[1]
           w-noteln2 = com.noteln[2]
           w-noteln3 = com.noteln[3]
           w-noteln4 = com.noteln[4]
           w-noteln5 = com.noteln[5]
           w-noteln6 = com.noteln[6]
           w-noteln7 = com.noteln[7].

    assign x-name = "".
    find first oimsp where oimsp.person = oeeh.takenby no-lock no-error.
    if avail oimsp then
       assign x-name = substr(oimsp.name,1,20).
    else
       assign x-name = "N/A".
       
    if oeeh.shipto = "" then do:
       find arsc use-index k-arsc where arsc.cono = g-cono and
                                        arsc.custno = oeeh.custno and
                                        arsc.slsrepout >= b-slsrep and
                                        arsc.slsrepin  <= e-slsrep
                                        no-lock no-error.
       if not avail arsc then next.
       assign w-name      = substring(arsc.name,1,20)
              w-custno    = arsc.custno
              w-shipto    = "".
    end.
    else do:
       find arss use-index k-arss where arss.cono = g-cono and
                                        arss.custno = oeeh.custno and
                                        arss.shipto = oeeh.shipto and
                                        arss.slsrepout >= b-slsrep  and
                                        arss.slsrepout <= e-slsrep
                                        no-lock no-error.
       if not avail arss then next.
       assign w-name      = substring(arss.name,1,20)
              w-custno    = arss.custno
              w-shipto    = arss.shipto.
       find arsc use-index k-arsc where arsc.cono = g-cono and
                                        arsc.custno = arss.custno
                                        no-lock
                                        no-error.
       if not avail arsc then next.
    end.

    find icsp use-index k-icsp where icsp.cono = g-cono and
                                     icsp.prod = oeel.shipprod and
                                     icsp.statustype <> "L"
                                     no-lock no-error.
    if not avail icsp then next.
         
    find smsn use-index k-smsn where smsn.cono      = g-cono and
                                     smsn.slsrep    = oeel.slsrepout and
                              substr(smsn.mgr,1,1) >= b-region       and
                              substr(smsn.mgr,1,1) <= e-region       and
                              substr(smsn.mgr,2,3) >= b-district     and
                              substr(smsn.mgr,2,3) <= e-district
                                     no-lock no-error.
    if not avail smsn then next.
    find icsw where icsw.cono = g-cono and
                    icsw.whse = oeel.whse and
                    icsw.prod = oeel.shipprod
                    no-lock no-error.
    if not avail icsw then next.
    
    if icsw.stndcost = oeel.prodcost then next.
        
    find icsp where icsp.cono = g-cono and
                    icsp.prod = icsw.prod
                    no-lock no-error.
    if not avail icsp then next.
    
    create ovr.
    assign ovr.regdist   = CAPS(smsn.mgr)
           ovr.custno    = w-custno
           ovr.shipto    = w-shipto
           ovr.name      = w-name
           ovr.slsrep    = smsn.slsrep
           ovr.takenby   = CAPS(oeeh.takenby) + " " + CAPS(x-name)
           ovr.order     = string(oeeh.orderno,"9999999") + "-" + 
                           string(oeeh.ordersuf,"99")
           ovr.transtype = oeeh.transtype
           ovr.pline     = CAPS(icsw.prodline)
           ovr.prod      = replace(oeel.shipprod,"""","'")
           ovr.proddesc  = icsp.descrip[1]
           ovr.qtyord    = oeel.qtyord
           ovr.stndcost  = icsw.stndcost
           ovr.overcost  = oeel.prodcost
           ovr.enterdt   = oeel.enterdt
           ovr.ovrtype   = if icsw.stndcost > oeel.prodcost then "D" else "U"
           ovr.comment1  = w-noteln1
           ovr.comment2  = w-noteln2
           ovr.comment3  = w-noteln3
           ovr.comment4  = w-noteln4
           ovr.comment5  = w-noteln5
           ovr.comment6  = w-noteln6
           ovr.comment7  = w-noteln7.
    assign ovr.prod      = CAPS(ovr.prod).
    if ovr.stndcost >  999999.99 then assign ovr.stndcost =  999999.99.
    if ovr.stndcost < -999999.99 then assign ovr.stndcost = -999999.99.
  end. /* each oeeh */
end. /* each oeel */

/{&user_extractrun}*/

/{&user_exportstatDWheaders}*/
  
  assign export_rec = "Region"        + v-del +
                      "District"      + v-del +
                      "CustNo"        + v-del +
                      "ShipTo"        + v-del +
                      "CustName"      + v-del +
                      "AM"            + v-del +
                      "TknBy"         + v-del +
                      "TknByName"     + v-del +
                      "OrderNo"       + v-del +
                      "Transtype"     + v-del +
                      "ProdLine"      + v-del +
                      "Product"       + v-del +
                      "ProdDescript"  + v-del +
                      "QtyOrd"        + v-del +
                      "StndCost"      + v-del +
                      "OverCost"      + v-del +
                      "Comment1"      + v-del +
                      "Comment2"      + v-del +
                      "EnterDate".
 
/{&user_exportstatDWheaders}*/
 
 
/{&user_exportstatheaders}*/
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  assign export_rec = " ".
  assign export_rec = "RGN/DIST"      + v-del +
                      "CUSOTMER NAME" + v-del +
                      "ACCT MGR"      + v-del +
                      "TKN BY"        + v-del +
                      "TKN BY NAME"   + v-del +
                      "ORDER"         + v-del +
                      "TRANSTYPE"     + v-del +
                      "PROD LINE"     + v-del +
                      "PRODUCT"       + v-del +
                      "DESCRIPTION"   + v-del +
                      "QTY ORD"       + v-del +
                      "STND COST"     + v-del +
                      "OVR COST"      + v-del +
                      "ENTER DATE"    + v-del +
                      "COMMENTS1"     + v-del +
                      "COMMENTS2".
/{&user_exportstatheaders}*/
 
/{&user_foreach}*/
for each ovr no-lock:
   if p-ovrtype = "D" and ovr.ovrtype = "U" then next.
   if p-ovrtype = "U" and ovr.ovrtype = "D" then next.
/{&user_foreach}*/
 
/{&user_drptassign}*/
 /* If you add fields to the dprt table this is where you assign them */
   drpt.ovrrecid = recid(ovr) 
/{&user_drptassign}*/

/{&user_B4endloop}*/
/{&user_B4endloop}*/

 
/{&user_viewheadframes}*/
/* Since frames are unique to each program you need to put your
   views and hides here with your names */

if not p-exportl then do:
  if not x-stream then do:
    hide frame f-header.
  end.
  else do:
    hide stream xpcd frame f-header.
  end.
end.
if not p-exportl then do:
  if not x-stream then do:
    page.
    view frame f-header.
  end.
  else do:
    page stream xpcd.
    view stream xpcd frame f-header.
  end.
end.
  
/{&user_viewheadframes}*/
 
/{&user_drptloop}*/
/{&user_drptloop}*/
 
/{&user_detailprint}*/
   /* Comment this out if you are not using detail 'X' option  */
   find ovr where recid(ovr) = drpt.ovrrecid no-lock no-error.
   assign export_rec = ""
          x-comment1 = ""
          x-comment2 = "".
   if p-exportDWl then do:
      if ovr.comment1 <> "" then
          assign x-comment1 = x-comment1 + ovr.comment1 + " ".
      if ovr.comment2 <> "" then
          assign x-comment1 = x-comment1 + ovr.comment2 + " ".
      if ovr.comment3 <> "" then
          assign x-comment1 = x-comment1 + ovr.comment3 + " ".
      if ovr.comment4 <> "" then
          assign x-comment1 = x-comment1 + ovr.comment4 + " ".
      if ovr.comment5 <> "" then
          assign x-comment2 = x-comment2 + ovr.comment5 + " ".
      if ovr.comment6 <> "" then
          assign x-comment2 = x-comment2 + ovr.comment6 + " ".
      if ovr.comment7 <> "" then
          assign x-comment2 = x-comment2 + ovr.comment7.

      assign export_rec = substr(ovr.regdist,1,1)           + v-del +
                          substr(ovr.regdist,2,3)           + v-del +
                          string(ovr.custno,"999999999999") + v-del +
                          ovr.shipto                        + v-del +
                          ovr.name                          + v-del +
                          ovr.slsrep                        + v-del +
                          substr(ovr.takenby,1,4)           + v-del +
                          substr(ovr.takenby,6,20)          + v-del +
                          ovr.order                         + v-del +
                          ovr.transtype                     + v-del +
                          ovr.pline                         + v-del +
                          ovr.prod                          + v-del +
                          ovr.proddesc                      + v-del +
                          string(ovr.qtyord,">>>>>>>9")     + v-del +
                          string(ovr.stndcost,"->>>>>9.99") + v-del +
                          string(ovr.overcost,"->>>>>9.99") + v-del +
                          x-comment1                        + v-del +
                          x-comment2                        + v-del +
                          string(ovr.enterdt,"99/99/99").
          
      assign export_rec = export_rec + chr(13) + chr(10).
      put stream expt unformatted export_rec.
      assign export_rec = ""
             x-comment1 = ""
             x-comment2 = "".
   end.
   else do:
      if p-exportl then do:
         if ovr.comment1 <> "" then
             assign x-comment1 = x-comment1 + ovr.comment1 + " ".
         if ovr.comment2 <> "" then
             assign x-comment1 = x-comment1 + ovr.comment2 + " ".
         if ovr.comment3 <> "" then
             assign x-comment1 = x-comment1 + ovr.comment3 + " ".
         if ovr.comment4 <> "" then
             assign x-comment1 = x-comment1 + ovr.comment4 + " ".
         if ovr.comment5 <> "" then
             assign x-comment2 = x-comment2 + ovr.comment5 + " ".
         if ovr.comment6 <> "" then
             assign x-comment2 = x-comment2 + ovr.comment6 + " ".
         if ovr.comment7 <> "" then
             assign x-comment2 = x-comment2 + ovr.comment7.
   
         assign export_rec = ovr.regdist           + v-del +
                             ovr.name                          + v-del +
                             ovr.slsrep                        + v-del +
                             substr(ovr.takenby,1,4)           + v-del +
                             substr(ovr.takenby,6,20)          + v-del +
                             ovr.order                         + v-del +
                             ovr.transtype                     + v-del +
                             ovr.pline                         + v-del +
                             ovr.prod                          + v-del +
                             ovr.proddesc                      + v-del +
                             string(ovr.qtyord,"->>>>>>>>9")   + v-del +
                             string(ovr.stndcost,"->>>>>9.99") + v-del +
                             string(ovr.overcost,"->>>>>9.99") + v-del +
                             string(ovr.enterdt,"99/99/99")    + v-del +
                             x-comment1                        + v-del +
                             x-comment2.
          
         assign export_rec = export_rec + chr(13) + chr(10).
         put stream expt unformatted export_rec.
         assign export_rec = ""
                x-comment1 = ""
                x-comment2 = "".
      end.
      else do:
         assign com-cnt = 0.
         assign s-regdist   = ovr.regdist  
                s-name      = ovr.name
                s-slsrep    = ovr.slsrep
                s-takenby   = ovr.takenby
                s-order     = ovr.order
                s-transtype = ovr.transtype
                s-pline     = ovr.pline
                s-prod      = ovr.prod
                s-proddesc  = ovr.proddesc
                s-qtyord    = ovr.qtyord
                s-stndcost  = ovr.stndcost
                s-overcost  = ovr.overcost
                s-comment1  = ovr.comment1
                s-comment2  = ovr.comment2
                s-comment3  = ovr.comment3
                s-comment4  = ovr.comment4
                s-comment5  = ovr.comment5
                s-comment6  = ovr.comment6
                s-comment7  = ovr.comment7.
   
         if s-comment1 <> "" then assign com-cnt = com-cnt + 1.
         if s-comment2 <> "" then assign com-cnt = com-cnt + 1.
         if s-comment3 <> "" then assign com-cnt = com-cnt + 1.
         if s-comment4 <> "" then assign com-cnt = com-cnt + 1.
         if s-comment5 <> "" then assign com-cnt = com-cnt + 1.
         if s-comment6 <> "" then assign com-cnt = com-cnt + 1.
         if s-comment7 <> "" then assign com-cnt = com-cnt + 1.
         
         if LINE-COUNTER + com-cnt > PAGE-SIZE then do:
            page.
         end.
   
         display s-regdist  
                 s-name     
                 s-slsrep    
                 s-takenby  
                 s-order    
                 s-transtype
                 s-pline    
                 s-prod     
                 s-proddesc   
                 s-qtyord    
                 s-stndcost  
                 s-overcost  
                 s-comment1  
         with frame f-detail.
         down with frame f-detail.
    
         if s-comment2 <> " " then do:
            display s-comment2 with frame f-com2.
            down with frame f-com2.
         end.
         if s-comment3 <> " " then do:
            display s-comment3 with frame f-com3.
            down with frame f-com3.
         end.
         if s-comment4 <> " " then do:
            display s-comment4 with frame f-com4.
            down with frame f-com4.
         end.
         if s-comment5 <> " " then do:
            display s-comment5 with frame f-com5.
            down with frame f-com5.
         end.
         if s-comment6 <> " " then do:
            display s-comment6 with frame f-com6.
            down with frame f-com6.
         end.
         if s-comment7 <> " " then do:
            display s-comment7 with frame f-com7.
            down with frame f-com7.
         end.
   
         display s-space with frame f-space.
         down with frame f-space.
   
         assign s-comment1 = ""
                s-comment2 = ""
                s-comment3 = ""
                s-comment4 = ""
                s-comment5 = ""
                s-comment6 = ""
                s-comment7 = "".
      end.
   end.
/{&user_detailprint}*/
 
/{&user_finalprint}*/
/{&user_finalprint}*/
 
/{&user_summaryframeprint}*/
/{&user_summaryframeprint}*/
 
/{&user_summaryputexport}*/
/{&user_summaryputexport}*/
 
/{&user_detailputexport}*/
/{&user_detailputexport}*/


/{&user_procedures}*/
 procedure formatoptions:
    assign p-portrait = false.
    /* if false then the report will print in LANDSCAPE 178 character */
  
    if sapb.optvalue[1] > "0" and sapb.optvalue[1] ne "99" then do:
       p-optiontype = " ".
       find notes where notes.cono = g-cono and
                        notes.notestype = "zz" and
                        notes.primarykey = "oerzv" and  /* tbxr program */
                        notes.secondarykey = string(p-format) no-lock no-error.
       if not avail notes then do:
         display "Format is not valid cannot process request".
         assign p-optiontype = "R"
                p-sorttype   = ">,"
                p-totaltype  = "N"
                p-summcounts = "A".
         return.
       end.               
   
       assign p-optiontype = notes.noteln[1]
              p-sorttype   = notes.noteln[2]
              p-totaltype  = notes.noteln[3]
              p-summcounts = notes.noteln[4]
              /*
              p-export     = "    "
              */
              p-register   = notes.noteln[5]
              p-registerex = notes.noteln[6].
    end.     
    else
       if sapb.optvalue[1] = "99" then do:
          assign p-register   = ""
                 p-registerex = "".
          run reportopts(input sapb.user5, 
                         input-output p-optiontype,
                         input-output p-sorttype,  
                         input-output p-totaltype,
                         input-output p-summcounts,
                         input-output p-register,
                         input-output p-regval).
       end.
 end.

/{&user_procedures}*/
 
/* TBXR F6 CodeGen Includes from here down are CodeGen Generated  */ 
/{&user_optiontype}*/
/{&user_optiontype}*/
 
/{&user_registertype}*/
/{&user_registertype}*/
 
/{&user_DWvarheaders1}*/
/{&user_DWvarheaders1}*/
 
/{&user_exportvarheaders1}*/
/{&user_exportvarheaders1}*/
 
/{&user_exportvarheaders2}*/
/{&user_exportvarheaders2}*/
 
/{&user_loadtokens}*/
/{&user_loadtokens}*/
 
/{&user_totaladd}*/
   t-amounts[1]
   t-amounts[2]
   t-amounts[3]
   t-amounts[4]
   t-amounts[5]
   t-amounts[6]
   t-amounts[7]
   t-amounts[8]
   t-amounts[9]
   t-amounts[10]
   t-amounts[11]
   t-amounts[12]
   t-amounts[13]
   t-amounts[14]
   t-amounts[15]

/{&user_totaladd}*/
 
/{&user_numbertotals}*/
/{&user_numbertotals}*/
 
/{&user_summaryloadprint}*/
/{&user_summaryloadprint}*/
 
/{&user_summaryloadexport}*/
/{&user_summaryloadexport}*/
 
 
/* TBXR F6 CodeGen End of Includes that are CodeGen Generated  */ 
