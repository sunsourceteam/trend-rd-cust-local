/* g-edepp.i 1.2 10/21/98 */
/*h****************************************************************************
  INCLUDE      : g-edepp.i
  DESCRIPTION  : EDI-Incoming Purchase Order
  USED ONCE?   : no
  AUTHOR       : mjm
  DATE WRITTEN : 02/26/95
  CHANGES MADE :
    02/11/93 jlc; TB# 9812  Rewrite to use a flat file.
    04/27/93 jlc; TB# 11116 Add user fields
    06/25/93 jlc; TB# 12056 Add variables for custom processing
    10/10/93 jlc; TB# 13285 Add po issue date; added s-poissuedt, p-poissuedt
    10/14/93 jlc; TB# 13336 add v-oecustno for code split.
    10/29/93 jlc; TB# 13528 Alpha custno cross reference.
    02/22/94 jlc; TB# 15205 Add edih/edil recid for 855 document.
    04/21/94 jlc; TB# 15187 EDI Data Collision
    10/05/94 jlc; TB# 16732 edeppa.p code split.
    10/13/94 jlc; TB# 15969 Add confirmation purchase order; added v-errmsg
    09/12/95 kjb; TB# 14340 EDI does not support JIT orders; added p-jitfl,
             added new error message to v-errmsg and incremented its extent
    09/20/95 kjb; TB# 18342 Allow customer defined terms; added p-termsfl,
             s-termstype.
    10/02/95 kr ; TB# 12915 Allow customer to accept incomming EDI stock prices.
    10/03/95 kjb; TB# 19238 Allow incomplete orders into OE Batch; added
             p-invalidfl.  Removed variables that are local to one program.
    02/24/96 jlc; TB# 20591R9999 Add a warning message for an unknown shipto.
    06/25/96 jkp; TB# 21061 Added g-mess variable.
    11/25/96 jkp; TB# 21889 Allow optional processing of 840 (Request for
             Quote) data; added p-process840.
    05/29/97 gp;  TB# 17827 Assign batch name from the flat file
    06/05/97 gp;  TB# 19219 Direct update to live Order Entry.
             Add p-runbatchfl, s-printernm, s-backfl, s-autooe and temp table
             t-oeehb.
    10/21/98 kjb; TB#  9731 Add a second nonstock description line.  Added
             s-proddesc2
    03/23/99 sbr; TB# 25206 Warning for superseded product
    10/27/00 bpa; TB# e5604 Add variable for email funtionality.
    12/02/02 ns;  TB# e12955 Add variable for EDI Line #
    10/27/03 blr; TB# e16106 Add error message for implied & core products
    12/08/03 blr; TB# e16106 Add error message for return reman products.
    03/22/04 rgm; TB# e11010 Allow EDEPP/OEEBU to be run to GUI View
    04/06/04 mwb; TB# e19978 Modified EDI Price message to include calculated
             price value.  So two can be compared.
    09/23/05 mwb; TB# e20589 Removed the 'shared' buffers - moved to specific
             programs that require them. Record locking issues with oeebu.
        ******************************************************************************/

/*tb 20591 02/24/96 jlc; Unknown Shipto in flat file */
def {1} shared var g-ffshiptofl    as logical                  no-undo.
def {1} shared var g-confordno     like oeeh.orderno           no-undo.
def {1} shared var g-confbatchnm   like oeehb.batchnm          no-undo.
def {1} shared var g-confirmok     as l initial "true"         no-undo.
def {1} shared var g-mess          as c format "x(78)"         no-undo.
def {1} shared var o-partner       as c format "x(15)"         no-undo.
def {1} shared var p-batchnm       like sabs.batchnm           no-undo.
def {1} shared var p-catfl         as logical                  no-undo.
def {1} shared var p-nsfl          as logical                  no-undo.
def {1} shared var p-pricefl       as logical                  no-undo.
def {1} shared var p-updfl         as logical                  no-undo.
def {1} shared var p-printfl       as l                        no-undo.
def {1} shared var p-edidir        as c format "x(24)"         no-undo.
def {1} shared var p-jitfl         as l                        no-undo.
def {1} shared var p-termsfl       as l                        no-undo.
def {1} shared var p-invalidfl     as l                        no-undo.
def {1} shared var p-process840    as l                        no-undo.
def {1} shared var p-runbatchfl    as l                        no-undo.
def {1} shared var s-custno        like arsc.custno            no-undo.
def {1} shared var s-custnoin      as c format "x(12)"         no-undo.
def {1} shared var s-custnm        like arsc.name              no-undo.
def {1} shared var s-shipto        like arsc.shipto            no-undo.
def {1} shared var s-shiptoph      like arsc.phoneno           no-undo.
def {1} shared var s-shiptoduns    like arsc.dunsno            no-undo.
def {1} shared var s-shiptonm      like arsc.name              no-undo.
def {1} shared var s-shiptoaddr    like arsc.addr              no-undo.
def {1} shared var s-shiptocity    like arsc.city              no-undo.
def {1} shared var s-shiptostate   like arsc.state             no-undo.
def {1} shared var s-shiptozipcd   like arsc.zipcd             no-undo.
def {1} shared var s-whse          like oeel.whse              no-undo.
def {1} shared var s-shipfmph      like arsc.phoneno           no-undo.
def {1} shared var s-shipfmnm      like arsc.name              no-undo.
def {1} shared var s-shipfmaddr    like arsc.addr              no-undo.
def {1} shared var s-shipfmcity    like arsc.city              no-undo.
def {1} shared var s-shipfmstate   like arsc.state             no-undo.
def {1} shared var s-shipfmzipcd   like arsc.zipcd             no-undo.
def {1} shared var s-termstype     like sasta.codeval          no-undo.
def {1} shared var s-custpo        like oeeh.custpo            no-undo.
def {1} shared var s-canceldt      as c format "x(8)"          no-undo.
def {1} shared var s-reqshipdt     as c format "x(8)"          no-undo.
def {1} shared var s-promisedt     as c format "x(8)"          no-undo.
def {1} shared var s-poissuedt     as c format "x(8)"          no-undo.
def {1} shared var s-transtype     like oeel.transtype         no-undo.
def {1} shared var s-ourprod       like oeel.shipprod          no-undo.
def {1} shared var s-custprod      like oeel.shipprod          no-undo.
def {1} shared var s-intchngprod   like oeel.shipprod          no-undo.
def {1} shared var s-lineno        as c format "x(6)"          no-undo.
def {1} shared var s-qtyord        as c format "x(13)"         no-undo.
def {1} shared var s-unit          like oeel.unit              no-undo.
def {1} shared var s-price         as c format "x(14)"         no-undo.
def {1} shared var s-proddesc      like oeel.proddesc          no-undo.
def {1} shared var s-proddesc2     like oeel.proddesc2         no-undo.
def {1} shared var s-descrip       like oeel.proddesc          no-undo.
def {1} shared var s-refer         like oeeh.refer             no-undo.
def {1} shared var s-shipinstr     like oeeh.shipinstr         no-undo.
def {1} shared var s-actiontype    as c                        no-undo.
def {1} shared var s-buyer         as c                        no-undo.
def {1} shared var s-linecnt       as c                        no-undo.
def {1} shared var s-hduser1       as c format "x(80)"         no-undo.
def {1} shared var s-hduser2       as c format "x(8)"          no-undo.
def {1} shared var s-hduser3       as c format "x(8)"          no-undo.
def {1} shared var s-batchnm       like oeehb.batchnm          no-undo.
def {1} shared var s-printernm     like sapb.printernm         no-undo.
def {1} shared var s-edilineno     as c format "x(11)"         no-undo.
def {1} shared var s-faxto1        like sapb.faxto1            no-undo.
def {1} shared var s-shipviaJT     as c format "x(24)"         no-undo.
def {1} shared var s-trackingno    as c format "x(24)"         no-undo.



/*tb e5604 10/27/00 bpa; Variable for email funtionality */
def {1} shared var s-faxphoneno    like sapb.faxphoneno        no-undo.
def {1} shared var s-backfl        like sapb.backfl            no-undo.
def {1} shared var s-autooe        like oeehb.batchnm          no-undo.
def {1} shared var v-rectype       as c format "x(6)"          no-undo.
def {1} shared var v-OK            as l                        no-undo.
def {1} shared var v-virgin        as l                        no-undo.
def {1} shared var v-shipprod      like oeel.shipprod          no-undo.
def {1} shared var v-recid         as recid                    no-undo.
def {1} shared var v-edihid        as recid                    no-undo.
def {1} shared var v-edilid        as recid                    no-undo.
def {1} shared var v-oeehbid       as recid                    no-undo.
def {1} shared var v-oeelbid       as recid                    no-undo.
def {1} shared var v-rptid         as recid                    no-undo.
def {1} shared var v-unit          as c                        no-undo.
def {1} shared var v-seqno         as i                        no-undo.
def {1} shared var v-setno         as c                        no-undo.
def {1} shared var v-partner       as c format "x(15)"         no-undo.
def {1} shared var v-linecnt       as i format "zz9"           no-undo.
def {1} shared var v-uline         as c format "x(132)"        no-undo.
def {1} shared var v-errtype       as i                        no-undo.
def {1} shared var v-edepp99fl     as l                        no-undo.
def {1} shared var v-oereqdays     like sasc.oereqdays         no-undo.
def {1} shared var v-data          as c format "x(600)"        no-undo.

/*tb 25206 03/23/99 sbr; Increased extent to 12 and initialized the 12th extent
    of v-errmsg to "CAUTION: Superseded Product". */
    def {1} shared var v-errmsg        as c format "x(30)"         no-undo
                                            extent 15 initial
                                              /*123456789012345678901234567890*/
  ["Invalid Trading Partner",
    "Invalid Batch Name",
    "Invalid Unit of Measure",
    "CAUTION: Catalog Product",
    "CAUTION: Non Stock Product",
    "Invalid Product",
    "Existing Trend Order Not Found",
    "Batch EDI Data Already Exists",
    "Live EDI Data Already Exists",
    "Req Ship Date > Promise Date",
    "EDI Price <> ",
    "CAUTION: Superseded Product",
    "Implied Core Prod Not Allowed",
    "Dirty Core Returns Not Allowed",
    "Reman Returns Not Allowed"
  ].
/* v-user is a field reserved for custom programming needs */
def {1} shared var v-user          as c format "x(120)"        no-undo.

def {1} shared temp-table t-oeehb no-undo
    field cono      like sasc.cono
    field batchnm   like oeehb.batchnm
index k-oeehb is primary unique
  cono        ascending
  batchnm     ascending.

def {1} shared temp-table t-reptadd no-undo
    field rptrecid     as recid
    field discamt      as dec
    field prodcost     as dec
    field currencyty   as char
    field Pcurrencyty   as char
    field slsrep       as char
    field prodcat      as char format "x(4)"
    field laborfl      as logical
    field nontaxtype   like oeelb.nontaxtype 
    field taxablefl    like oeelb.taxablefl  
 
index raix 
  rptrecid.

def {1} shared var s-taxtype         as c                        no-undo.

