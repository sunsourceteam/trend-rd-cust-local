/* vaepv1nt.lpr 1.1 01/03/98 */
/******************************************************************************
  INCLUDE      : vaepv1nt.lpr
  DESCRIPTION  : Notes for Internal Processing Print
  USED ONCE?   : 
  AUTHOR       : 
  DATE WRITTEN : 
  CHANGES MADE :
******************************************************************************/
/*  message "internal notes". pause.  */

do with frame f-notes: 
for each notes where
    notes.cono         = g-cono          and
    notes.notestype    = {&notestype}    and
    notes.primarykey   = {&primarykey}   and
    notes.secondarykey = {&secondarykey} and
    notes.printfl      = yes             and 
    notes.pageno       = 1
    no-lock:
    j = 0.
    do i = 1 to 16:
       if notes.noteln[i] <> " " then do:
         assign s-noteln =  notes.noteln[i].
         display s-noteln with frame f-com3.
         down with frame f-com3.
       end.  
    end.

 /*   
    message "i" i. pause.
    do i = 1 to j with frame f-com3:
       display notes.noteln[i] with frame f-com3.
    end.   
*/
   end. /* for each notes */
end.  


