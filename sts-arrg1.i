/***************************************************************************
 PROGRAM NAME: sts-arrg1.i
  PROJECT NO.: 00-35
  DESCRIPTION: This is the include for arrg.p, arrg1.p, & arrg2.p 
 DATE WRITTEN: 04/11/00
       AUTHOR: Sir Carl E. Graul Esq.
     MODIFIED: 09/26/00 added code for project 00-55

***************************************************************************/


/*   Variables Used To Create Trend Like Header */
DEF {1} VAR rpt-dt      as date format "99/99/9999" no-undo.
DEF {1} VAR rpt-dow     as char format "x(3)"   no-undo. 
DEF {1} VAR rpt-time    as char format "x(5)"  no-undo.
DEF {1} VAR rpt-cono    like oeeh.cono         no-undo.
DEF {1} VAR rpt-user    as char format "x(08)" no-undo.
DEF {1} VAR x-title     as char format "x(177)" no-undo.
DEF {1} VAR x-page      as integer format ">>>9" no-undo.
DEF {1} VAR dow-lst     as character format "x(30)"
           init "Sun,Mon,Tue,Wed,Thu,Fri,Sat"                 no-undo.

/*   Variables from screen */
def {1} var b-creditmgr      like arsc.creditmgr              no-undo.
def {1} var e-creditmgr      like arsc.creditmgr              no-undo.
def {1} var b-region         as character format "x(1)"       no-undo.
def {1} var e-region         as character format "x(1)"       no-undo.  
def {1} var b-district       as character format "x(3)"       no-undo.
def {1} var e-district       as character format "x(3)"       no-undo.
def {1} var b-slsrep         like smsn.slsrep                 no-undo.
def {1} var e-slsrep         like smsn.slsrep                 no-undo.
def {1} var b-divno          as integer format ">>9"          no-undo.
def {1} var e-divno          as integer format ">>9"          no-undo.
def {1} var p-agecredit      as character                     no-undo.
def {1} var p-minbal         as decimal                       no-undo.
def {1} var p-mindays        as integer                       no-undo.
def {1} var p-detail         as character                     no-undo.
def {1} var p-div            as logical                       no-undo.
def {1} var p-sum            as logical                       no-undo.
def {1} var p-prtby          as char format "x(1)"            no-undo.
def {1} var p-dispute        as logical                       no-undo.
def {1} var p-debit          as logical                       no-undo.
/* Foot Hill Mod */
def {1} var p-foothill       as logical                       no-undo.
/* Foot Hill Mod */


/* ############################ end of program ######################### */ 

