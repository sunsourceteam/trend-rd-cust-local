/*
 SX 55
 
 This is a knock off of a-oeetlm it handles exchange different on the cost */
/* a-oeetlm.i 1.1 01/03/98 */
/* a-oeetlm.i 1.1 01/02/98 */
/*  a-oeetlm.i 1.11 7/19/93  */
/*h*****************************************************************************
  INCLUDE      : a-oeetlm.i
  DESCRIPTION  : Margin calculation for OE line items
  USED ONCE?   : no
  AUTHOR       : mwb
  DATE WRITTEN :
  CHANGES MADE :
    11/17/92 mwb; TB# 8595 Changed margin pct to use a rounded cost value so
        it is consistant with the net amount calc
    12/03/92 mwb; TB# 4535 Take into consideration the customer and vendor
        rebates.  Drives off the SMAO option to subtract the rebates or not
    03/19/93 kmw; TB# 10080 Margin pricing; REMOVING THE ROUNDING "UNFIXES"
        TB 8595. Discussed in the design meeting and agreed upon
    04/14/93 kmw; TB# 10940 Load v-sm variables from SASC
    06/06/93 dww; TB# 10126 Special costing product pdsc pricing Force Bid Prep
        to use csunperstk calculations
    07/08/93 mwb; TB# 12121 Added edit to margin pct setting of 100% for price
        and cost being equal (pct = 0% not 100%)
    07/19/93 dww; TB# 12302 Bid Prep Inquiries change 2 includes limit scope of
        g-ourproc to "bpet" and "bpes"
    09/22/95 gdf; TB# 18812 7.0 Rebates Enhancement (C1a). The variables
        v-custrebamt and v-vendrebamt must now be defined in the calling
        program.  A new parameter &oerebty must now be passed to a-oeetlm.i.
    10/11/95 mcd; TB# 18812-E5a 7.0 Rebates Enhancement (E5a) -
        Display figures in consideration of rebates on the Exception Report.
        Code clean up.
    10/08/96 tdd; TB# 20663 Removed parens from around p-oeordl.i since they
        were added in the include itself
    03/06/97 frb; TB# 7241  Special price costing - replace all occurances
        of "&icsp" with "&icss"
    06/09/99 gwk; TB# 25844 do not omit special price calculations when line
        is lost business.
*******************************************************************************/

/*e****************************************************************************
    IF THIS CHANGES IN ANY WAY - TELL SOFTWARE INTEGRATION
       - Daily Sales Reports (Desiree Ortiz)
*******************************************************************************/
/*tb 18812 09/22/95 gdf; The vendor and customer rebate will no longer be stored
           on the oeel record.  These fields will be calculated from the
           pder records into v-custrebamt and v-vendrebamt. */

/*tb 18812 09/22/95 gdf; Added run of oeetlmrb.p which will calculate the
    v-custrebamt and v-vendrebamt. This section should be commented out
        when running from batch (&com = "/ *") */

/*tb 18812 10/06/95 mcd; Rebate Enhancement 7.0 (E5-a) Code clean up*/

/*d v-marqty    =   the qty to use in the margin calculation based on the
                     stage/disposition of the order

    v-marnet    =   the real net of the line after subtracting the
                    prorated special discount if after invoicing

    v-marcost   =   the cost used for margin, taking into account a
                    charge qty if entered, and special costing factors.
                    Multiply by the conversion factor to get to a per line cost,
                    checking to see if the operator is in Bid Prep
                    (catalog items are treated like stock items)

    v-marprice =    the margin price, taking into account a
                    charge qty if entered, and any special costing factor
                    that exists

   v-mardisc   =    the margin discount, based on % or $, whichever specified.
                    If the discount is a $ discount, consider the charge qty
                    and special costing factor if set

   v-marnetdiv =    the line net to 5 decimals for the margin % calculation

   s-marginamt =    the margin set to price - discount - cost

   s-marginpct =    the margin by net or cost based on OEAO options */

/*e Rebates should only be included in margin and cost calculations if the
    operator has the appropiate security (sasoo.oerebty = i). If a-oeetlm.i
    is being run from a function where rebates should not be considered,
    then set &oerebty = "n" in the calling program. */

{&sasc}
do for sasc:
    {w-sasc.i no-lock}
    assign v-smcustrebfl = sasc.smcustrebfl
           v-smvendrebfl = sasc.smvendrebfl.
/{&sasc}* */

v-marqty    = {&com2}
              if not {p-oeordl.i "{&oeel}" "{&oeeh}"} then
              {&oeel}qtyship else
              /{&com2}* */
              {&oeel}qtyord.

{&rebcom}
if {&oerebty} = "i" then
    run  oeetlmrb.p({&oeeh}orderno,
                   {&oeeh}ordersuf,
                   {&oeel}lineno,
                   0,   /* SX 55 */
                   {&oeeh}stagecd,
                   v-marqty,
                   output v-custrebamt,
                   output v-vendrebamt).
/{&rebcom}* */

/*tb 25844 06/09/99 gwk; do not omit lost business lines from special price
    calculations */
assign
    v-marnet    = {&com2}
                  if {p-oeordl.i "{&oeel}" "{&oeeh}"} then 
                    ( {&oeel}netord * v-exchrate) - v-mardiscoth
                  else
                  /{&com2}* */
                  ( {&oeel}netamt * v-exchrate) - v-mardiscoth

    v-marcost   = ({&oeel}prodcost * v-exchrate) *
                      (if {&oeel}chrgqty <> 0 then {&oeel}chrgqty / v-marqty
                       else if ({&oeel}specnstype = "n" or
                                {&icss}speccostty = "") and
                               not can-do("bpet,bpes",substring(g-ourproc,1,4))
                                        then 1
                    else ({&icss}csunperstk /
                        (if {&icss}speccostty = "t" then 1000
                        else if {&icss}speccostty = "h" then 100
                        else 1) * v-conv))
    v-marprice  = {&oeel}price *
                    (if v-exchrate <> 0 then v-exchrate else 1)
                    *  (if {&oeel}chrgqty <> 0 then {&oeel}chrgqty / v-marqty
                        else if ({&oeel}specnstype = "n" or
                                 {&icss}speccostty = "") and
                                not can-do("bpet,bpes",substring(g-ourproc,1,4))
                                        then 1
                        else ({&icss}csunperstk * v-conv))

    v-mardisc   = if not {&oeel}disctype then
                      v-marprice * ({&oeel}discpct / 100)
                  else ({&oeel}discamt * v-exchrate) *
                       (if {&oeel}chrgqty <> 0 then {&oeel}chrgqty / v-marqty
                        else if ({&oeel}specnstype = "n" or
                                 {&icss}speccostty = "") and
                                not can-do("bpet,bpes",substring(g-ourproc,1,4))
                                        then 1
                        else ({&icss}csunperstk * v-conv))

    v-marnetdiv = (v-marprice * v-marqty) - (v-mardisc  * v-marqty) -
                   v-mardiscoth

    s-marginamt = ((v-marprice * v-marqty) -
                        (if v-smcustrebfl = yes then v-custrebamt
                         else 0)) -
                   (v-mardisc  * v-marqty) - v-mardiscoth -
                ((v-marcost  * v-marqty) -
                        (if v-smvendrebfl = yes then v-vendrebamt
                         else 0))

    s-marginpct = if v-oecostsale then
                      if v-marnetdiv = 0 then 0
                      else s-marginamt / (v-marnetdiv -
                                     (if v-smcustrebfl = yes
                                          then v-custrebamt else 0)) * 100
                else if (v-marqty * v-marcost) = 0 then
                      if v-marcost = v-marprice then 0 else 100
                else s-marginamt / ((v-marqty * v-marcost) -
                                     (if v-smvendrebfl = yes
                                          then v-vendrebamt else 0)) * 100

    /*d Flip values for a return line */
    s-marginamt = if {&oeel}returnfl then s-marginamt * -1
                  else s-marginamt
    {&com}
    v-marqty    = if {&oeel}returnfl then v-marqty * -1
                  else v-marqty
    v-marnet    = if {&oeel}returnfl then v-marnet * -1
                  else v-marnet
    v-marcost   = if {&oeel}returnfl then v-marcost * -1
                  else v-marcost
    /{&com}* */
.

{&sasc}
end.
/{&sasc}* */





