/* g-oere.i 1.1 01/03/98 */
/* g-oere.i 1.12 08/25/97 */
/*h*****************************************************************************
  INCLUDE      : g-oere.i
  DESCRIPTION  : Global variables used for exception processing
  USED ONCE?   : no
  AUTHOR       : mwb
  DATE WRITTEN : 10/12/90
  CHANGES MADE :
    06/27/91 mwb; TB# 3670  Added return flag to qty's
    10/30/91 mwb; TB# 4882  Totals for lost business, bo on lines
    12/09/92 mwb; TB# 4535  Added v-rebatety, v-smvendrebfl, v-smcustrebfl for
        new rebates calc included in margin calculation
    01/07/92 pap; TB# 5225  JIT - modified margin variable format
    01/15/92 pap; TB# 5225  JIT - added jit flag variable
    05/14/92 dea; TB# 6472  Added s-dolinefl for line DO
    03/19/93 kmw; TB# 10080 Margin pricing - change format of s-marginamt
    10/28/93 kmw; TB# 11758 Improve Unit Conversion Precision
    11/08/93 mwb; TB# 13565 Changed all order totals to new, shared variables
    03/22/95 kjb; TB# 17997 OERE.P oversized.  Moved the variables that had to
        be shared into this routine.
    06/26/95 ajw; TB# 18575 Nonstock/Special Kit Component Project (G3)
    08/30/95 dww; TB# 17521 Performance in OERE.  Removed reference to
        v-oeehid.
    10/11/95 mcd; TB# 18812-E5a 7.0 Rebates Enhancement (E5a) -
        Display figures in consideration of rebates on the Exception Report
        Added  {1} var s-frzrebty and {1} var v-oerebty.
    02/19/97 jl;  TB# 7241 (4.1) Special Price Costing. Removed s-speccostty
        and s-csunperstk.
    08/25/97 kjb; TB# 22385 OERR totals do not include rebates - modified the
        format of t-lostcnt from "<<<<<<9" to "<<<<<9", added v-rebatefl,
        t-torebatefl, t-tocustreb, t-tovendreb, t-rebfl, t-custreb and
        t-vendreb
*******************************************************************************/

{g-margin.i}
{g-conv.i}

def     var b-orderno      like oeeh.orderno                            no-undo.
def     var e-orderno      like oeeh.orderno                            no-undo.
def     var b-enterdt      like oeeh.enterdt                            no-undo.
def     var e-enterdt      like oeeh.enterdt                            no-undo.
def {1} var b-slsgrp       as c format "x"                              no-undo.
def {1} var w-slstype      as c format "x(4)"                           no-undo.
def {1} var p-printtot     as logical initial no                        no-undo.
def     var s-taxhdg1      as c format "x(8)"                           no-undo.
def     var s-taxhdg2      as c format "x(7)"                           no-undo.
def     var s-lookupnm     as c format "x(15)" initial ""               no-undo.
def     var s-fplookup     as c format "x(15)" initial ""               no-undo.
def     var s-disccd       as c format "x"                              no-undo.
def     var s-jitflag      as c format "x(3)"  initial ""               no-undo.
def     var s-margin       as decimal format "zzzzzzz9.99-" initial 0   no-undo.
def     var s-marginamt    as dec format "zzzzz9.99999-"                no-undo.
def     var s-margpct      as decimal format "zzz9.99-" initial 0       no-undo.
def     var s-marginpct    as decimal format "zzz9.99-" initial 0       no-undo.
def     var s-netamt       like oeel.netamt                             no-undo.
def     var s-cost         like oeeh.totlineamt                         no-undo.
def     var s-discamt      like oeel.discamt                            no-undo.
def     var s-dolinefl     as c format "x"                              no-undo.
def {1} var v-oecostsale   like sasc.oecostsale                         no-undo.
def     var v-totlineamt   like oeeh.totlineamt initial 0               no-undo.
def     var v-totcost      like oeeh.totcost    initial 0               no-undo.
def     var v-coretype     as c format "x(10)" initial ""               no-undo.
def     var v-corecharge   as c format "x(9)"  initial ""               no-undo.
def {1} var v-count        as integer format ">>>>>9" initial 0         no-undo.
def     var v-prod         as c format "x(25)" initial ""               no-undo.
def     var v-cust         as c format "x(13)" initial ""               no-undo.
def     var v-fpcustno     as c format "x(13)" initial ""               no-undo.
def     var s-descrip      as c format "x(24)" initial ""               no-undo.
def     var s-netsale      as decimal format "zzzzzzzz9.99-" initial 0  no-undo.
def     var s-addon        as decimal format "zzzz9.99-"     initial 0  no-undo.
def     var s-tax          as decimal format "zzzzzzzz9.99-" initial 0  no-undo.
def     var s-stagecd      as c format "x(3)" initial ""                no-undo.
def     var v-label        as c format "x(80)" initial ""               no-undo.
def     var v-auth         as c format "x(14)" initial "Authorization:" no-undo.
def     var s-retqtyord    as c format "x(1)" initial ""                no-undo.
def     var s-retqtyship   as c format "x(1)" initial ""                no-undo.

/******************** total var for order types ***************************/
def     var t-ordtype      as c format "x(20)" extent 10 initial
    ["Stock Order","Direct Order",
     "Counter Sales","Return Merchandise","Corrections",
     "Blanket Order","Blanket Release","Future Order","Quote",
     "Standing Order"] no-undo.

def {1} var t-sales   as de format ">>>>>>>>>9.99-" extent 10 initial 0 no-undo.
def {1} var t-cstgds  as de format ">>>>>>>>>9.99-" extent 10 initial 0 no-undo.
def {1} var t-gross   as de format ">>>>>>>>>9.99-" extent 10 initial 0 no-undo.
def {1} var t-charge  as de format ">>>>>>>>>9.99-" extent 10 initial 0 no-undo.
def {1} var t-down    as de format ">>>>>>>>>9.99-" extent 10 initial 0 no-undo.
def {1} var t-totord  as de format ">>>>>>>>>9.99-" extent 10 initial 0 no-undo.
def {1} var t-count   as i  format ">>>>>9"         extent 10 initial 0 no-undo.
def {1} var t-lines   as i  format ">>>>>>9"        extent 10 initial 0 no-undo.

def var z-sales      as de format ">>>>>>>>>9.99-" extent 10 initial 0 no-undo. 
def var z-cstgds     as de format  ">>>>>>>>9.99-" extent 10 initial 0 no-undo. 
def var z-custreb    as de format ">>>>>>>>>9.99-" extent 10 initial 0 no-undo. 
def var z-vendreb    as de format ">>>>>>>>>9.99-" extent 10 initial 0 no-undo. 
def var z-charge     as de format  ">>>>>>>>9.99-" extent 10 initial 0 no-undo. 
def var z-down       as de format  ">>>>>>>>9.99-" extent 10 initial 0 no-undo. 
def var z-totord     as de format ">>>>>>>>>9.99-" extent 10 initial 0 no-undo. 
def var z-count      as de format ">>>>>>9"        extent 10 initial 0 no-undo. 
def var z-lines      as de format ">>>>>>9"        extent 10 initial 0 no-undo. 

def var z-lostcnt              as i  format ">>>>>9"          initial 0 no-undo.
def var z-lostnet              as i  format ">>>>>>>>>>9.99-" initial 0 no-undo.
def var z-lostord              as i  format ">>>>>>>>>>9.99-" initial 0 no-undo.

def {1} var t-tosales      as de format ">>>>>>>>>>9.99-"     initial 0 no-undo.
def {1} var t-tocstgds     as de format ">>>>>>>>>>9.99-"     initial 0 no-undo.
def {1} var t-togross      as de format ">>>>>>>>>>9.99-"     initial 0 no-undo.
def {1} var t-tomarg       as de format ">>>>>>>>>>9.99-"     initial 0 no-undo.
def {1} var t-tocharge     as de format ">>>>>>>>>>9.99-"     initial 0 no-undo.
def {1} var t-todown       as de format ">>>>>>>>>>9.99-"     initial 0 no-undo.
def {1} var t-tototord     as de format ">>>>>>>>>>9.99-"     initial 0 no-undo.
def {1} var t-tocount      as i  format ">>>>>>9"             initial 0 no-undo.
def {1} var t-lostcnt      as i  format ">>>>>9"              initial 0 no-undo.
def {1} var t-lostnet      as de format ">>>>>>>>>>9.99-"     initial 0 no-undo.
def {1} var t-lostord      as de format ">>>>>>>>>>9.99-"     initial 0 no-undo.
def {1} var t-tolines      as i  format ">>>>>>>9"            initial 0 no-undo.
def {1} var s-backorder    as logical                        initial no no-undo.
def     var v-rebatety     as c format "x"                              no-undo.

def {1} var v-oper2        like sasc.operinit  initial ""               no-undo.
def {1} var v-report       like sapb.reportnm initial ""                no-undo.
def {1} var p-printprice   as logical                                   no-undo.
def {1} var p-printprom    as logical                                   no-undo.
def {1} var p-printcost    as logical                                   no-undo.
def {1} var p-printnet     as logical                                   no-undo.
def {1} var p-printsminmax as logical                                   no-undo.
def {1} var p-printsret    as logical                                   no-undo.
def {1} var p-printqtya    as logical                                   no-undo.
def {1} var p-printqtyo    as logical                                   no-undo.
def {1} var p-printstock   as logical                                   no-undo.
def {1} var p-printcatfl   as logical                                   no-undo.
def {1} var p-printnoties  as logical                                   no-undo.
def {1} var v-qtyshipfl    as logical                                   no-undo.
def {1} var v-smcustrebfl  like sasc.smcustrebfl                        no-undo.
def {1} var v-smvendrebfl  like sasc.smvendrebfl                        no-undo.

def {1} var s-frzrebty     like pder.frzrebty                           no-undo.
def {1} var v-oerebty      like sasoo.oerebty                           no-undo.

/*tb 18575 06/26/95 ajw; Nonstock/Special Kit Component.*/
def     var v-cprod        like oeelk.shipprod                          no-undo.
def     var s-cdescrip     like oeelk.proddesc                          no-undo.

def {1} var t-rebfl      as c  format "x"              extent 10        no-undo.
def {1} var t-vendreb    as de format ">>>>>>>>>9.99-" extent 10 init 0 no-undo.
def {1} var t-custreb    as de format ">>>>>>>>>9.99-" extent 10 init 0 no-undo.
def     var t-tovendreb  as de format ">>>>>>>>>9.99-"           init 0 no-undo.
def     var t-tocustreb  as de format ">>>>>>>>>9.99-"           init 0 no-undo.
def     var t-torebatefl as c format "x"                                no-undo.
def     var v-rebatefl   as c format "x"                                no-undo.

def     var s-slstype    as c format "x(3)"             no-undo.
def {1} var p-drilldown  as c format "x"                no-undo.
/* das - begin */                                                               
define {1} TEMP-TABLE tsls no-undo
       FIELD grptype       as c format "x(3)"
       FIELD csr           as c format "x(3)"
       FIELD rep           as c format "x(4)"
       FIELD g-sales       as de format ">>>>>>>>>9.99-" extent 10 initial 0
       FIELD g-cstgds      as de format ">>>>>>>>>9.99-" extent 10 initial 0
       FIELD g-gross       as de format ">>>>>>>>>9.99-" extent 10 initial 0
       FIELD g-charge      as de format ">>>>>>>>>9.99-" extent 10 initial 0
       FIELD g-down        as de format ">>>>>>>>>9.99-" extent 10 initial 0
       FIELD g-totord      as de format ">>>>>>>>>9.99-" extent 10 initial 0
       FIELD g-count       as i  format ">>>>>9"         extent 10 initial 0
       FIELD g-lines       as i  format ">>>>>>9"        extent 10 initial 0
  index k-prime is unique primary
       grptype ascending.
/*       csr     ascending.  */
/* das - end */


