/* input fields for apza.p - ACH; Terms Update */
def {1} var i-ap-tech    as c                    no-undo.    
def {1} var i-vendno     as c                    no-undo. /*key field*/ 
def {1} var i-name       as c                    no-undo.   
def {1} var i-invly      as c                    no-undo.   
def {1} var i-cur-Vtp    like apsv.vendtype      no-undo.
def {1} var i-cur-terms  like apsv.termstype     no-undo.
def {1} var i-disc-terms as c                    no-undo.
def {1} var i-ACH-accept as c                    no-undo.
def {1} var i-com        as c                    no-undo.
def {1} var i-Vtype      like apsv.vendtype      no-undo.  /*key field*/
def {1} var i-Vterms     like apsv.termstype     no-undo.  /*key field*/
def {1} var i-email      like apsv.email         no-undo.  /*key field*/
def {1} var i-bankacct   like apsv.vendbankacct  no-undo.  /*key field*/
def {1} var i-routing    like apsv.vendbanktrno  no-undo.  /*key field*/
def {1} var i-cono       as c format "x(4)"      no-undo.  /*key field*/    
def {1} var i-bankno     like apsv.bankno        no-undo.  /*key field*/
def {1} var p-fileaud    as c format "x(50)"     no-undo.
def {1} var v-count      as i format ">,>>9"     no-undo.
def {1} var ap-count     as i format ">>,>>9"    no-undo.
def {1} var po-count     as i format ">>,>>9"    no-undo.
    