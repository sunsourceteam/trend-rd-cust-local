/* k-sdileave.i
 
 Include to determine in the Editor Loop if the field is being exited. 

 Parameters {&functions} - function butons that would lead to leaving field
            {&fieldname} - name to match to field-name attribute
            {&completename} - entire name of field for use in cursor-offset
                              command

*/

    frame-field = "{&fieldname}" and
     ( ({k-after.i} or {k-accept.i}) or
      {&functions}                          
     ( frame-field = "{&fieldname}"  and                                    
     ( ({&completename}:cursor-offset = {&completename}:width-chars and
        keylabel(lastkey) = "CURSOR-RIGHT") or                       
     ({&completename}:cursor-offset = 1 AND 
        keylabel(lastkey) = "CURSOR-LEFT") )   ) )                                       
