/* f-oeepp8.i 04/23/97 */
/*si**** Custom ****************************************************************
  CLIENT  : 80281 SDI
  JOB     : SDI008 Custom Print Packing List
  AUTHOR  : jsb
  DATE    : 04/23/97
  VERSION : 7.0.003
  PURPOSE : Run Packing List from PMEP and OEES
  CHANGES :
    SI01 04/23/97 jsb; Original Changes                      
    si02 08/29/97 jsb; SDI00801
*******************************************************************************/
/* f-oeepp1.i 1.3 9/8/93 */
/*Copyright 1990 by R&D Systems, Inc.*/
/*h*****************************************************************************
  INCLUDE      : f-oeepp1.i
  DESCRIPTION  : forms and form variable definitions for oe & wt pick tickets
  USED ONCE?   : no (wtep1.p oeepp1.p)
  AUTHOR       : rhl
  DATE WRITTEN : 11/04/89
  CHANGES MADE : 10/14/91 mwb; TB# 4762  - Changed page# to fit pre-printed form
                 11/11/92 mms; TB# 8669  - Rush - Sort rush orders to top
                 09/07/93 mms; TB# 12619 - Display cod message if wt tied to cod
                               order; added s-codmsg not used in oe
                 11/10/98 dbf; ITB# 3    - Remove "Correspondence To" info from
                               pick ticket print.
*******************************************************************************/

    form header
    /* dbf itb# 3 beg
    sasc.conm                            at 25    /*si02*/
    sasc.addr[1]                         at 25    /*si02*/
    sasc.addr[2]                         at 25    /*si02*/
    s-corrcity as c format "x(35)"       at 25    /*si02*/
    dbf itb# 3 end */
    /*    skip(1)                                       si02*/
    s-lit1a as c format "x(10)"          at 2
    s-doctype as c format "x(12)"        at 12    /*si01 form was 33*/
    s-reprint as c format "x(36)"        at 24    /*si01 was 53 si02*/
    s-lit1b as c format "x(10)"          at 70  /*si01 form was 22 was 93 si02*/
    s-rush as c format "x(12)"           at 12
    s-codmsg as c format "x(11)"         at 25
    s-filltype as c format "x(33)"       at 37    /*si01 was 53 si02*/
/*    sasc.upcvno                          at 95    si01*/
    {&head}.{&pref}no                    at 71    /*si01 was 106 si02*/
    "-"                                  at 78    /*si01 was 113 si02*/
    {&head}.{&pref}suf                   at 79    /*si01 was 114 si02*/
    s-lit3a as c format "x(7)"           at 4
    {&com}
    {&head}.custno                       at 12
    /{&com}*  */
    s-orderdisp as c format "x(21)"      at 33    /*si01 was 53*/
    s-lit3b as c format "x(25)"          at 56    /*si01 was 94 si02*/
    {&orderdt}                           at 56    /*si01 was 94*/

    {&com}
    {&head}.custpo format "x(10)"        at 66    /*si01 was 104*/
    /{&com}*  */

    page-num - v-pageno format "zzz9"    at 76    /*si01 was 129 si02*/
    s-pricedesc as c format "x(48)"      at 2
    s-lit6a as c format "x(1)"           at 2     /*si01 form was 8*/
    s-billname like arsc.name            at 4     /*si01 was 12*/
    "F"                                  at 40    /*si02*/
    s-fmname as c format "x(30)"         at 42    /*si02*/
    s-lit6b as c format "x(1)"           at 2     /*si01 form was 18 at 48*/
/*    sasc.conm                            at 68    si01*/
    s-billaddr1    as c format "x(30)"   at 4     /*si01 was 12*/
    "R"                                  at 40    /*si02*/
    s-fmaddr1 as c format "x(30)"        at 42    /*si02*/
/*    sasc.addr[1]                         at 68    si01*/
    s-lit6c as c format "x"              at 2     /*si01*/
    s-billaddr2    as c format "x(30)"   at 4     /*si01 was 12*/
    "O"                                  at 40    /*si02*/
    s-fmaddr2 as c format "x(30)"        at 42    /*si02*/
/*    sasc.addr[2]                         at 68    si01*/
    s-lit6d as c format "x"              at 2     /*si01*/
    s-billcity as c format "x(35)"       at 4     /*si01 was 12*/
    "M"                                  at 40    /*si02*/
    s-fmcity as c format "x(35)"         at 42    /*si02*/
/*    s-corrcity as c format "x(35)"       at 68    si01*/
    skip(1)
    s-lit11a as c format "x(1)"          at 2     /*si01 form was 8*/
    s-shiptonm like {&head}.shiptonm     at 4     /*si01 was 12*/
    s-lit11b as c format "x(12)"         at 40    /*si01 was 50*/
/*    s-lit11c as c format "x(12)"         at 71    /*si01 was 81*/ si02*/
    s-lit12e as c format "x(7)"          at 70    /*si01 si02*/ 
    s-lit11d as c format "x"             at 2     /*si01*/
    s-shiptoaddr[1]                      at 4     /*si01 was 12*/
    {&head}.shipinstr                    at 40    /*si01 was 50*/
/*    s-stagearea like {&head}.stagearea   at 71    /*si01 was 81*/  si02*/
    {&head}.reqshipdt                    at 70    /*si01 si02*/
    s-lit11e as c format "x"             at 2     /*si01*/
    s-shiptoaddr[2]                      at 4     /*si01 was 12*/
/*    s-lit12a as c format "x(10)"         at 40    /*si01 was 50*/ si02*/
    s-lit12b as c format "x(3)"          at 40    /*si01 was 81 si02*/
/*    s-lit12e as c format "x(7)"          at 100   si01*/
    s-lit12c as c format "x(7)"          at 70    /*si01 was 110 si02*/
/*    s-lit12d as c format "x(5)"          at 120   si01*/
    s-lit11f as c format "x"             at 2     /*si01*/
    s-shipcity as c format "x(35)"       at 4     /*si01 was 12*/
/*    s-whsedesc as c format "x(30)"       at 40    /*si01 was 50*/ si02*/
    s-shipvia as c format "x(12)"        at 40    /*si01 was 81 si02*/
    s-cod as c format "x(6)"             at 57    /*si01 was 94 si02*/
/*    {&head}.reqshipdt                    at 100   si01*/
    {&pickdt}                            at 70    /*si01 was 110 si02*/
/*    s-terms as c format "x(12)"          at 120   si01*/
    skip(1)
    s-lit14a as c format "x(62)"         at 1     /*si02*/
    s-lit14b as c format "x(18)"         at 63    /*si01 was 49 si02*/  
    s-lit15a as c format "x(62)"         at 1     /*si02*/
    s-lit15b as c format "x(18)"         at 63    /*si01 was 48 si02*/
    s-lit16a as c format "x(78)"         at 1
/*    s-lit16b as c format "x(18)"         at 79    /*si01 was 50*/ si02*/
    with frame f-head no-box no-labels width 80 page-top. /*si02 was 132*/

form header
    skip(1)
    s-linecntx as c format "x(3)"    at 1
    s-lit40a as c format "x(11)"     at 5
    s-lit40b as c format "x(26)"     at 17   /*si02 was 20*/
    v-noprintx as c format "x(3)"    at 44   /*si02 was 47*/
    s-lit40c as c format "x(18)"     at 48   /*si02 was 58*/
    s-totqtyshp  as c format "x(13)" at 67   /*si02 was 77*/
/*    s-lit40d as c format "x(22)"     at 91
    s-totlineamt as c format "x(13)" at 115   si01*/
    s-lit41a as c format "x(78)"     at 1
/*    s-lit41b as c format "x(10)"     at 79  si02*/
/*    s-totcubes  as c format "x(13)"  at 42
    s-totweight as c format "x(13)"  at 56      si01*/
    oeeh.pickedtm                    at 48    /*si01*/
    oeeh.takenby                     at 63    /*si01*/
    s-lit42a as c format "x(9)"      at 1
/*    s-lit42b as c format "x(33)"     at 95    si01*/
with frame f-tot no-box no-labels width 80 page-bottom.  /*si02 was 132*/
