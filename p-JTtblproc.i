
{g-INTformat.i &formatfile = "{&formatfile}" }  
{g-INTxref.i &xreffile = "{&xreffile}" }  


define stream cin.

define var g-cono      as int                           no-undo init 98.

define var v-reccount  as int                           no-undo.
define var v-input     as char                          no-undo.
define var v-numfields as int                           no-undo.
define var v-extent    as int                           no-undo.
define var v-tablename as char                          no-undo.
define var x-name      as char                          no-undo.
define var x-value     as char                          no-undo.

define var tfhdl as handle.   


define temp-table tablexref no-undo
  field sxetable   as char
  field sxefield   as char
  field EDIfield   as char
index ixt
  sxetable
  sxefield
index ixf
  sxetable
  EDIfield.

find last t-formats use-index k-locat no-lock no-error.
if not avail t-formats then 
  leave.
assign v-numfields = t-formats.locator 
       v-tablename = "{&tblname}" .

run XrefImport.

/* -- This will be an SAPB parameter EDI like -- */

input stream cin from "/home/tomh/FordConv/FORDarsc".

/* 
   Edit data to field definitions to ensure it is valid to the 
   data format.
   --
   Need to handle foreign data that does not fit. Not sure if it will happen
   but if it does allow for it in mapping. 
   --
*/   

repeat:
  assign v-reccount = v-reccount + 1.
  import stream cin unformatted v-input.
  run Interpret-Input (input v-tablename,
                       input v-input,
                       input v-numfields).

  if v-reccount > 100 then 
    leave.

end.

/*  
   This reads the first record of the master file and makes sure the fields
   are compatible with the data base schema names.

*/ 
for each t-data no-lock:
  /* see if this can be handled with soft setups if needed 
  if (t-data.name matches "*XnaX*") then /* XnaX are manually handled */
    next.
  */
  run Analyze_table (input t-data.tblname,
                     input t-data.name,
                     input t-data.extent).
  if t-data.recno > 1 then    /* just need one record to determine */
    leave.
end.                     




/* Edit data against the database schema as well as edit
   supporting tables such as SASTT SMSN ... ect 
*/
  run Edit_Data_Fields.

/* Updating the data base this update is assuming a CREATE other logic will
   be performed to handle changes
 
*/
/*
  run Create_Data_Records.

*/


procedure Edit_Data_Fields:

  define var thdl  as handle.
  define var tfhdl as handle.
  
  
  for each t-record ,
  
      each   t-data use-index i-create
       where t-data.tblname = t-record.tblname and
             t-data.recno   = t-record.recno
         
                            break by t-data.tblname
                                  by t-data.recno
                                  by t-data.locator:
  
    for each _file where _file._file-number > 0 AND
             _file._file-name = t-data.tblname no-lock:
      for each _field of _file where 
               _field._field-name = t-data.name  no-lock:
        if _field._Extent > 0 then do:
          if _field._Data-Type = "integer" then
            int(t-data.fieldval) no-error.
          else
          if _field._Data-Type = "character" then
            string(t-data.fieldval) no-error.
          
          else
          if _field._Data-Type = "decimal" then
            dec(t-data.fieldval) no-error.
           
          else
          if _field._Data-Type = "date" then
            date(t-data.fieldval) no-error.
          
          else
          
          if _field._Data-Type = "logical" then do:
             if t-data.fieldval <> "" then
                logical(t-data.fieldval,_field._Format ) no-error. 
          end.       
          
        end.
        else do:
          if _field._Data-Type = "integer" then
            int(t-data.fieldval) no-error.
          else
          if _field._Data-Type = "character" then
            string( t-data.fieldval) no-error.
          else
          if _field._Data-Type = "decimal" then
            dec(t-data.fieldval) no-error.
          else
          if _field._Data-Type = "date" then
            date(t-data.fieldval) no-error.
          else
          if _field._Data-Type = "logical" then
            if t-data.fieldval <> "" then 
              logical(t-data.fieldval,_field._Format ) no-error.
         
        end.

        if error-status:error then do:
          assign t-data.errorcd  = "E"
                 t-record.errorcd = "E"
                 t-data.errorlit = "Data value does not conform to database".
      
        end.         

        if t-record.errorcd <> "E" then do:
          run  Other_supporting_Edits (input  t-record.tblname,
                                       input  _field._field-name,
                                       input  t-data.fieldval,
                                       input-output t-data.errorcd,
                                       input-output t-record.errorcd,
                                       input-output t-data.errorlit).
        end.
  
  
      end.
    end.
  end.
end.




procedure Create_Data_Records:

  define var thdl  as handle.
  define var tfhdl as handle.



  
    for each t-data use-index i-create no-lock break by t-data.tblname
                                  by t-data.recno
                                  by t-data.locator TRANSACTION:
  
      if t-data.recno > 1 then
        leave.
       
      if first-of (t-data.recno) then do:
        create buffer thdl for table t-data.tblname.
        thdl:buffer-create().

        tfhdl = thdl:buffer-field ("cono").

        assign thdl:BUFFER-FIELD("cono"):buffer-value
               = int(g-cono).

      end.
      for each _file where _file._file-number > 0 AND
               _file._file-name = t-data.tblname no-lock:
        for each _field of _file where 
                 _field._field-name = t-data.name  no-lock:
          tfhdl = thdl:buffer-field (_field._field-name). 

          
          if tfhdl:extent > 0 then do:
            if _field._Data-Type = "integer" then
              assign thdl:BUFFER-FIELD(_field._field-name):buffer-value
               (t-data.extent) = int(t-data.fieldval).
            else
            if _field._Data-Type = "character" then
              assign thdl:BUFFER-FIELD(_field._field-name):buffer-value
               (t-data.extent) = t-data.fieldval.
            
            else
            if _field._Data-Type = "decimal" then
             assign thdl:BUFFER-FIELD(_field._field-name):buffer-value
                (t-data.extent) = dec(t-data.fieldval).
            
            else
            if _field._Data-Type = "date" then
              assign thdl:BUFFER-FIELD(_field._field-name):buffer-value
               (t-data.extent) = date(t-data.fieldval).
            
            else
            
            if _field._Data-Type = "logical" then do:
               if t-data.fieldval <> "" then
                 assign thdl:BUFFER-FIELD(_field._field-name):buffer-value
                  (t-data.extent) = logical(t-data.fieldval,_field._Format ).            end.      
          
          end.
          else do:
            /*
            message _field._Data-Type t-data.name  t-data.fieldval. pause.
            */
            if _field._Data-Type = "integer" then
              assign thdl:BUFFER-FIELD(_field._field-name):buffer-value
                = int(t-data.fieldval).
            else
            if _field._Data-Type = "character" then
              assign thdl:BUFFER-FIELD(_field._field-name):buffer-value
                = t-data.fieldval.
            else
            if _field._Data-Type = "decimal" then
             assign thdl:BUFFER-FIELD(_field._field-name):buffer-value
                 = dec(t-data.fieldval).
            else
            if _field._Data-Type = "date" then
              assign thdl:BUFFER-FIELD(_field._field-name):buffer-value
               = date(t-data.fieldval).
            else
            if _field._Data-Type = "logical" then
              if t-data.fieldval <> "" then 
                assign thdl:BUFFER-FIELD(_field._field-name):buffer-value
                  = logical(t-data.fieldval,_field._Format ).
           
          end.
  
        end.
      end.
  
    end.
end.



procedure Analyze_table:
  define input parameter ip-tablename as char no-undo.
  define input parameter ip-fieldname as char no-undo.
  define input parameter ip-extent    as int no-undo.


  find _file use-index _file-name where
       _file._file-name = ip-tablename no-lock no-error.
  if not avail _file then do:
    message "table name invalid " + ip-tablename.
    pause.
    return.
  end.    

  find _field use-index _file/field where 
       _field._file-recid = recid(_file) and
       _field._field-name = ip-fieldname  no-lock no-error.
  if not avail _field then do: 
    message "field name invalid " + ip-tablename + " " + ip-fieldname.
    pause.
    return.
  end.    
  if _field._Extent <> 0 and 
    ( _field._Extent < ip-extent or
      ip-extent = 0 ) then do: 
    message "field extent invalid " + ip-tablename + " " + 
      ip-fieldname + " " + string(ip-extent,">>>9" ) + 
      string (_field._extent,">>>9" ).
    pause.
    return.
  end.    


end.


{p-INTformat.i}  

