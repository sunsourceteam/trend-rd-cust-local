/*****************************************************************************
Creates a file with all the APEC checking information. When APEC runs to print
checks for vendors, it also creates the file CPCvenrp.txt to send to BofA
SunSource 03/11/2019
*****************************************************************************/
{p-rptbeg.i}
{g-apec.i new}    

def new shared var threshold-amt as i format ">>>>>" init 50000 no-undo.
def new shared var overthreshold as logical  init no            no-undo.    



/* HEADER RECORD */
def var h-file-type      as c  format "x(6)"  init "BULKCSV"    no-undo. /* 0*/
def var h-company        as c  format "x(17)"  
                               init "STS OPERATING INC"         no-undo. /* 1*/
def var h-company-id     as c  format "x(9)"  init "SUNSOURCE"  no-undo. /* 2*/
def var h-EFT-key        as c  format "x(4)"  init "C201"       no-undo. /* 3*/
def var h-creation-dt    as c  format "x(8)"                    no-undo. /* 4*/
def var h-filename       as c  format "x(27)"                   no-undo. /* 5*/
def var h-TP-indicator   as c  format "x(4)"  init "PROD"       no-undo. /* 6*/



/* TRANSACTION RECORD */
def var t-record-type    as c format "x(3)"   init "TRN"        no-undo. /* 0*/
def var t-trans-type     as c format "x(5)"   init "CHECK"      no-undo. /* 1*/
def var t-alt-trans-type as c format "x(3)"   init ""           no-undo. /* 2*/
def var t-company-id     as c format "x(1)"   init ""           no-undo. /* 3*/
def var t-branch-cd      as c format "x(4)"   init ""           no-undo. /* 4*/
def var t-orig-acct      as c format "x(10)"  init ""           no-undo. /* 5*/
def var t-ABA-nbr        as c format "x(9)"                     no-undo. /* 6*/
def var t-orig-acct-cur  as c format "x(3)"   init ""           no-undo. /* 7*/
def var t-pay-col-ind    as c format "x(3)"   init "PAY"        no-undo. /* 8*/
def var t-trans-hand-cd  as c format "x(1)"   init "N"          no-undo. /* 9*/
def var t-post-ind       as c format "x(1)"   init ""           no-undo. /*10*/
def var t-consol-ref     as c format "x(19)"  init ""           no-undo. /*11*/
def var t-priority-ind   as c format "x(1)"   init ""           no-undo. /*12*/
def var t-trans-ref      as c format "x(16)"                    no-undo. /*13*/
def var t-mail-handl-cd  as c format "x(2)"   init "US"         no-undo. /*14*/
def var t-oparty-name    as c format "x(30)"  
                              init "STS OPERATING INC"          no-undo. /*15*/
def var t-oparty-id      as c format "x(1)"   init ""           no-undo. /*16*/
def var t-oparty-addr1   as c format "x(30)"                               
                              init "2301 WINDSOR COURT"         no-undo. /*17*/
def var t-oparty-addr2   as c format "x(30)"                    no-undo. /*18*/
def var t-oparty-city    as c format "x(30)"  init "ADDISON"    no-undo. /*19*/
def var t-oparty-state   as c format "x(2)"   init "IL"         no-undo. /*20*/
def var t-oparty-zip     as c format "x(11)"  init "60101"      no-undo. /*21*/
def var t-oparty-country as c format "x(2)"   init "US"         no-undo. /*22*/
def var t-rparty-name    as c format "x(30)"                    no-undo. /*23*/
def var t-rparty-id      as c format "x(12)"                    no-undo. /*24*/
def var t-rparty-addr1   as c format "x(30)"                    no-undo. /*25*/
def var t-rparty-addr2   as c format "x(30)"                    no-undo. /*26*/
def var t-rparty-city    as c format "x(30)"                    no-undo. /*27*/
def var t-rparty-state   as c format "x(2)"                     no-undo. /*28*/
def var t-rparty-zip     as c format "x(11)"                    no-undo. /*29*/
def var t-rparty-country as c format "x(2)"                     no-undo. /*30*/
def var t-eff-entry-dt   as c format "x(8)"                     no-undo. /*31*/
def var t-trans-descr    as c format "x(10)"  init "CHCKPAYMNT" no-undo. /*32*/
def var t-trans-cur-cd   as c format "x(3)"   init ""           no-undo. /*33*/
def var t-trans-amount   as de format ">>>>>>>>9.99"            no-undo. /*34*/
def var t-charg-ind      as c format "x(3)"   init ""           no-undo. /*35*/
def var t-check-nbr      as i format ">>>>>>>>>9"               no-undo. /*36*/
def var t-rcv-bank-name  as c format "x(33)"                    no-undo. /*37*/
def var t-rcv-acct-ty-id as c format "x(5)"   init ""           no-undo. /*38*/
def var t-rcv-ID-sort-cd as c format "x(12)"                    no-undo. /*39*/
def var t-rcv-SWIFT-addr as c format "x(8)"                     no-undo. /*40*/
def var t-rcv-acct-nbr   as c format "x(35)"                    no-undo. /*41*/
def var t-rcv-branch-nbr as c format "x(12)"                    no-undo. /*42*/
def var t-rcv-city-name  as c format "x(30)"                    no-undo. /*43*/
def var t-rcv-ISO-cntry  as c format "x(2)"   init "US"         no-undo. /*44*/
def var t-field45        as c format "x(1)"   init ""           no-undo. /*45*/
def var t-field46        as c format "x(1)"   init ""           no-undo. /*46*/
def var t-field47        as c format "x(1)"   init ""           no-undo. /*47*/
def var t-field48        as c format "x(1)"   init ""           no-undo. /*48*/
def var t-field49        as c format "x(1)"   init ""           no-undo. /*49*/
def var t-field50        as c format "x(1)"   init ""           no-undo. /*50*/
def var t-field51        as c format "x(1)"   init ""           no-undo. /*51*/
def var t-field52        as c format "x(1)"   init ""           no-undo. /*52*/

def var t-field53        as c format "x(1)"   init ""           no-undo. /*53*/
def var t-field54        as c format "x(1)"   init ""           no-undo. /*54*/
def var t-field55        as c format "x(1)"   init ""           no-undo. /*55*/
def var t-field56        as c format "x(1)"   init ""           no-undo. /*56*/
def var t-field57        as c format "x(1)"   init ""           no-undo. /*57*/
def var t-field58        as c format "x(1)"   init ""           no-undo. /*58*/
def var t-field59        as c format "x(1)"   init ""           no-undo. /*59*/
def var t-field60        as c format "x(1)"   init ""           no-undo. /*60*/
def var t-field61        as c format "x(1)"   init ""           no-undo. /*61*/
def var t-field62        as c format "x(1)"   init ""           no-undo. /*62*/
def var t-field63        as c format "x(1)"   init ""           no-undo. /*63*/
def var t-field64        as c format "x(1)"   init ""           no-undo. /*64*/
def var t-field65        as c format "x(1)"   init ""           no-undo. /*65*/
def var t-field66        as c format "x(1)"   init ""           no-undo. /*66*/
def var t-field67        as c format "x(1)"   init ""           no-undo. /*67*/
def var t-field68        as c format "x(1)"   init ""           no-undo. /*68*/
def var t-field69        as c format "x(1)"   init ""           no-undo. /*69*/
def var t-field70        as c format "x(1)"   init ""           no-undo. /*70*/
def var t-field71        as c format "x(1)"   init ""           no-undo. /*71*/
def var t-field72        as c format "x(1)"   init ""           no-undo. /*72*/
def var t-field73        as c format "x(1)"   init ""           no-undo. /*73*/
def var t-field74        as c format "x(1)"   init ""           no-undo. /*74*/
    
    
/* REMMITANCE (Unstructured */
/**** NOT NEEDED
def var ru-record-type   as c format "x(3)"   init "RDF"        no-undo.
def var ru-remit-type    as c format "x(3)"   init "TM"         no-undo.
def var ru-handle-dtl    as c format "x(22)"   
                                init "PAYMENT FROM SUNSOURCE"   no-undo.
****/   

/* REMITTANCE DETAIL RECORD */
def var r-record-type    as c format "x(3)"   init "RDS"       no-undo. /* 0*/
def var r-remit-type     as c format "x(3)"   init "INV"       no-undo. /* 1*/
def var r-item-ref       as c format "x(15)"                   no-undo. /* 2*/
def var r-item-date      as c format "x(8)"                    no-undo. /* 3*/
def var r-amount-paid    as de format ">>>>>>9.99"             no-undo. /* 4*/
def var r-orig-amount    as de format ">>>>>>9.99"             no-undo. /* 5*/
def var r-add-ref        as c format "x(1)"   init ""          no-undo. /* 6*/
def var r-response-cd    as c format "x(1)"   init ""          no-undo. /* 7*/
def var r-adj-cd         as c format "x(1)"   init "PO"        no-undo. /* 8*/
def var r-disc-amt       as de format ">>>>>>9.99"             no-undo. /* 9*/
def var r-adj-note       as c format "x(1)"   init ""          no-undo. /*10*/
def var r-addl-txt       as c format "x(1)"   init ""          no-undo. /*11*/

/* FOOTER */
def var f-record-type    as c format "x(14)"  
                                      init "BULKCSVTRAILER"    no-undo.
def var f-trans-count    as c format "x(10)"                   no-undo.
def var f-hash-tot       as c format "x(15)"                   no-undo.

def var o-comma          as c format "x(1)"  init ","          no-undo. 
def var o-dq             as c format "x(1)"  init '"'          no-undo.
def stream cpc.
def var o_doctype           as c    format "x". 
def var o_file              as c    format "x(50)". 
def buffer b-apet for apet.
def buffer c-apet for apet. 


def temp-table rptpay no-undo
  FIELD vendno  like apsv.vendno
  FIELD name    like apsv.name
  FIELD amount  like apet.amount     
  FIELD checkno like apet.checkno
  FIELD tiedfl  as   c format "x(3)"    
  FIELD notesfl as   c format "x(1)"
  INDEX k-over
        vendno
        checkno
        amount DESCENDING.
def temp-table remit no-undo
  FIELD vendno    like apsv.vendno
  FIELD checkno   like apet.checkno
  FIELD amount    like apet.amount   
  FIELD apinvno   like apet.apinvno
  FIELD reference like apet.refer
  INDEX k-remit
        vendno
        checkno
        apinvno.



def var x-today             as date format "99/99/9999".
def var x-time              as c    format "x(5)".
def var x-page              as i    format ">>>9".
def var x-line              as i    format "999".
def var x-page-size         as i    format "99"   init 50  no-undo.
def var x-day               as c    format "x(2)"          no-undo.
def var x-month             as c    format "x(2)"          no-undo. 
def var x-year              as c    format "x(2)"          no-undo. 
def var x-wyear             as c    format "x(4)"          no-undo.
def var x-wtime             as c    format "x(8)"          no-undo. 
def var x-ftime             as c    format "x(6)"          no-undo.
def var x-blank             as c                           no-undo. 
def var x-total             as de   format ">>>>>>>>9.99"  no-undo.

def frame  f-header 
  "~015"  at 1
  "Date:" at 1
  x-today format "99/99/99" at   7
  "at "                     at  16
  x-time                    at  19
  "~015"
  "Company:"                at   1
  g-cono                    at  10
  g-operinit                at  60
  "Page:"                   at  69
  x-page                    at  74 format ">>>9"
  "~015"                    at  78
  "Function: apec"          at   1
  "Check Payments"          at  32
  "~015"                    at  78
  "~015"                    at  78
  "Vendor#"                 at   1
  "Vendor Name"             at  17  
  "Check Amt"               at  50  
  "Check Nbr"               at  63
  "Tied?"                   at  73
  "~015"                    at  78
  "---------------"         at   1
  "------------------------------" at 17
  "-----------"             at  49  
  "-----------"             at  61
  "-----"                   at  73
  "~015"                    at  78
with down stream-io width 80 no-labels no-box no-underline.

define frame f-detail
   rptpay.vendno            at   4
   rptpay.name              at  17
   rptpay.amount            at  49 format ">>>>>>>9.99-"  
   rptpay.checkno           at  61 format ">>>>>>>>>9"      
   rptpay.tiedfl            at  74  
   rptpay.notesfl           at  77
   "~015"                   at  78
with down stream-io width 78 no-labels no-box no-underline.
define frame f-remithdr
  x-blank                    at   1
  "Invoice"                  at   7
  "Tied PO"                  at  24 
  "Amount"                   at  53
  "~015"                     at  78
  "---------------"          at   7
  "------------------------" at  24
  "----------"               at  53
  "~015"                     at  78
with down stream-io width 78 no-labels no-box no-underline.
define frame f-remit
   remit.apinvno            at   7
   remit.reference          at  24 
   remit.amount             at  53 format ">>>>>>>9.99-"     
   "~015"                   at  78
with down stream-io width 80 no-labels no-box no-underline.    
define frame f-blank
   x-blank                  at   1  
   "~015"                   at  78
with down stream-io width 80 no-labels no-box no-underline.
define frame f-total
   x-blank                  at   1
   "Total for Journal"      at  17
   p-jrnlno                 at  35
   x-total                  at  48      
with down stream-io width 80 no-labels no-box no-underline.
  
/*EMAIL Variables*/
def var cMailSubject  as character format "x(40)"  no-undo.
def var cMailMessage  as character format "x(130)" no-undo.
def var cMailto       as character format "x(40)"  no-undo.
def var cType         as character                 no-undo.
def var iCounter      as integer                   no-undo.
def var currUser      as character format "x(10)"  no-undo.
def var senderAddress as character                 no-undo.
def var v-emailfile   as character                 no-undo.
def var v-rmfile      as character                 no-undo.




assign x-month    = string(month(TODAY),"99").      
assign x-day      = string(day(TODAY),"99").
assign x-wyear    = string(year(TODAY),"9999").
assign x-year     = substr(x-wyear,3,2).   
assign x-wtime    = string(time,"HH:MM:SS").    
assign x-ftime    = substr(x-wtime,1,2) + 
                    substr(x-wtime,4,2) +   
                    substr(x-wtime,7,2).
assign x-time     = string(time,"HH:MM").

assign o_file = "/usr/tmp/bank/ss_prod_CPC_" + x-month + 
                                               x-year  + "_" +   
                                               x-ftime +
                                               ".csv".      


output stream cpc to value(o_file).

define variable destfilename as character format "x(40)".
define variable outputfilename as character format "x(40)".
define variable destfilecounter as character format "x(40)".

define variable flsuff  as character.
define variable flsuff2 as character.


substring(flsuff,1,2,"character") =
  substring(string(today),1,2,"character").
substring(flsuff,3,2,"character") = 
  substring(string(today),4,2,"character").

substring(flsuff,5,1,"character") = ".".

substring(flsuff,6,2,"character") = 
  substring(string(time,"HH:MM:SS"),1,2,"character").

substring(flsuff,8,2,"character") =
  substring(string(time,"HH:MM:SS"),4,2,"character").
substring(flsuff,10,2,"character") = 
  substring(string(time,"HH:MM:SS"),7,2,"character").

substring(destfilename,1,14,"character") = "/usr/tmp/CPCba".
substring(destfilename,15,11,"character") = flsuff.
substring(destfilecounter,1,19,"character") = "/usr/tmp/CPCcounter".

/************** Fields are defined in g-apec.i
def new shared buffer b-apsv for apsv.
def            var b-vendno      like apet.vendno       no-undo.
def            var e-vendno      like apet.vendno       no-undo.
def            var b-checkno     like apet.checkno      no-undo.
def            var e-checkno     like apet.checkno      no-undo.
def            var v-lastcktoprt like apet.checkno      no-undo.
def            var v-cknotest    like apet.checkno      no-undo.
def new shared var p-jrnlno      like apet.jrnlno       no-undo.
def new shared var p-reprintfl   as logical             no-undo.
def new shared var p-checkno     like apet.checkno      no-undo.
def new shared var p-preprintfl  as logical             no-undo.
def new shared var p-checkdt     like apet.paymtdt      no-undo.
def new shared var p-numprintfl  as logical             no-undo.
def new shared var p-bankno      like apsv.bankno       no-undo.
def new shared var p-order       as c                   no-undo.
def new shared var p-remittype   as c                   no-undo.
def new shared var p-zerofl      as l                   no-undo.
def new shared var p-edifl       as l                   no-undo.
def new shared var p-traceno     like apet.checkno      no-undo.    
def new shared var v-hadingfl    as logical             no-undo.
def new shared var v-headingfl   as logical             no-undo.
def            var v-checkfmt    as i                   no-undo.
def new shared var v-cono        like sapb.cono         no-undo.
def new shared var v-lastcheckno like cret.checkno      no-undo.
def new shared var g-outputty    like report.outputty   no-undo.
def new shared var g-faxfl       like report.faxfl      no-undo.
def buffer b-apemf for apemf.
def new shared stream remit.
def new shared var v-fpstreamfl  as l no-undo.
def            var s-sertitle    as c format "x(6)"     no-undo.
def            var v-firstfl     as l                   no-undo.
def            var v-firstsnfl   as l                   no-undo.
def            var v-count       as decimal             no-undo.
def            var v-serialno    as c format "x(20)"    no-undo. 
def            var v-sercount    as i                   no-undo.
def            var s-serial1     as c format "x(20)"    no-undo.
def            var s-serial2     as c format "x(20)"    no-undo.
def            var s-serial3     as c format "x(20)"    no-undo.  
def            var s-error       as c format "x(80)"    no-undo.

form s-error at 1
  with frame f-error no-box width 80 no-labels down.
**************/  

def new shared var p-manual      as c format "x(3)"     no-undo.    


assign b-vendno = decimal(sapb.rangebeg[1]).
assign e-vendno = decimal(sapb.rangeend[1]).
assign p-jrnlno = integer(sapb.optvalue[1]).
assign p-bankno = integer(sapb.optvalue[6]).    
assign p-manual = sapb.optvalue[16].


/*WORK FIELDS*/
def var w_counter       as i    format "9999999".
def var ed_counter      as i    format "9999999".
def var w_total         as i    format "9999999999".

{w-crsb.i p-bankno no-lock}

/*
find crsb where crsb.cono = g-cono and
                crsb.bankno = p-bankno
                no-lock no-error.
*/
if avail crsb then
  assign t-orig-acct     = if crsb.bankno <> 15 and crsb.bankno <> 16 and
                              crsb.bankno <> 17 and crsb.bankno <> 20 then 
                             string(substr(crsb.bankacct,10,10))
                           else
                             string(substr(crsb.bankacct,1,9))
         t-ABA-nbr       = replace(crsb.transrouteno,"=","")
         t-orig-acct-cur = if crsb.bankno = 5 then
                             "USD"
                           else
                             "CAD"  
         t-trans-cur-cd  = t-orig-acct-cur.

else
  return.
        
/*CREATE HEADER RECORD*/
assign h-creation-dt = string(year(TODAY),"9999") + 
                       string(month(TODAY),"99") +
                       string(day(TODAY),"99").
assign h-filename = substr(o_file,15,27).
/* EXPORT HEADER RECORD */
put stream cpc UNFORMATTED   
   h-file-type     o-comma
   h-company       o-comma
   h-company-id    o-comma
   h-EFT-key       o-comma
   h-creation-dt   o-comma
   h-filename      o-comma   
   h-TP-indicator  
  CHR(13)
  CHR(10).

/*CREATE DETAIL RECORD*/    

for each apet use-index k-jrnl where apet.cono      = g-cono and    
                                     apet.jrnlno  = p-jrnlno and
                                     apet.vendno >= b-vendno and
                                     apet.vendno <= e-vendno and  
                                     apet.bankno  = p-bankno and  
                                     apet.transcd = 07
                                     no-lock:
  if apet.manaddrfl then
    find apemm use-index k-apemm where
         apemm.cono = apet.cono and
         apemm.jrnlno = apet.pidjrnlno and
         apemm.setno = apet.pidsetno
         no-lock no-error.
  find apsv use-index k-apsv where   apsv.cono     = g-cono and
                                     apsv.vendno   = apet.vendno and
                                     apsv.bankno   = p-bankno and
                                    (apsv.vendtype <> "edc" and
                                     apsv.vendtype <> "eds" and
                                     apsv.vendtype <> "ddc" and
                                     apsv.vendtype <> "dds" and
                                     apsv.vendtype <> "sdc" and
                                     apsv.vendtype <> "sds" and
                                     apsv.vendtype <> "xdc" and
                                     apsv.vendtype <> "adc" and
                                     apsv.vendtype <> "udc" and
                                     apsv.vendtype <> "ldc" and
                                     apsv.vendtype <> "lds" and
                                     apsv.vendtype <> "pdc" and
                                     apsv.vendtype <> "sua" and
                                     apsv.vendtype <> "gdc" and
                                     apsv.vendtype <> "gds")
                                     no-lock no-error.
  if avail apsv then 
    do:
    assign t-mail-handl-cd = if apsv.countrycd = "US" or
                                apsv.countrycd = " " then
                               "US"
                             else
                               "FM".
    assign t-rparty-name    = if avail apemm and apet.manaddrfl = yes then
                                CAPS(apemm.name) else CAPS(apsv.name).   
    assign t-rparty-id      = string(apsv.vendno).
    assign t-rparty-addr1   = CAPS(apsv.addr[1]).
    assign t-rparty-addr2   = CAPS(apsv.addr[2]).
    assign t-rparty-city    = CAPS(apsv.city).
    assign t-rparty-state   = CAPS(apsv.state).
    assign t-rparty-zip     = apsv.zipcd.
    assign t-rparty-country = if apsv.countrycd = "" then "US" else
                                 CAPS(apsv.countrycd).
    assign t-eff-entry-dt   = string(YEAR(apet.transdt),"9999") +
                              string(MONTH(apet.transdt),"99")  +
                              string(DAY(apet.transdt),"99").
    assign t-trans-amount   = apet.amount.  
                                    /* APET rec has actual $ amt for */
                                    /* CAD or USD. The exchange rate */
                                    /* makes calcs to USD amount.    */
    assign t-check-nbr      = apet.checkno.
    assign f-hash-tot       = string(int(f-hash-tot) + (apet.amount * 100)). 
    /* Remove any possible commas */
    assign t-rparty-name    = REPLACE(t-rparty-name,",","").
    assign t-rparty-addr1   = REPLACE(t-rparty-addr1,",","").
    assign t-rparty-addr2   = REPLACE(t-rparty-addr2,",","").
    assign t-rparty-city    = REPLACE(t-rparty-city,",","").
    assign t-rparty-state   = REPLACE(t-rparty-state,",","").
    if int(t-trans-amount) > 0 then  
      do:
      if apet.amount > threshold-amt then
        assign overthreshold = yes.
      find rptpay where rptpay.vendno = apet.vendno and
                        rptpay.checkno = apet.checkno 
                        no-error. 
      if not avail rptpay then
        do:
        create rptpay.
        assign rptpay.vendno = apsv.vendno    
               rptpay.name   = apsv.name
               rptpay.amount = apet.amount
               rptpay.checkno = apet.checkno.
      end.
      assign t-trans-ref = "PYMT" + string(apet.checkno).
      assign f-trans-count = string(int(f-trans-count) + 1).
      put stream cpc UNFORMATTED    
          t-record-type       o-comma
          t-trans-type        o-comma
          t-alt-trans-type    o-comma
          t-company-id        o-comma
          t-branch-cd         o-comma
          t-orig-acct         o-comma
          t-ABA-nbr           o-comma
          t-orig-acct-cur     o-comma
          t-pay-col-ind       o-comma
          t-trans-hand-cd     o-comma
          t-post-ind          o-comma
          t-consol-ref        o-comma
          t-priority-ind      o-comma
          t-trans-ref         o-comma
          t-mail-handl-cd     o-comma
          t-oparty-name       o-comma
          t-oparty-id         o-comma
          t-oparty-addr1      o-comma
          t-oparty-addr2      o-comma
          t-oparty-city       o-comma
          t-oparty-state      o-comma
          t-oparty-zip        o-comma
          t-oparty-country    o-comma
          t-rparty-name       o-comma
          t-rparty-id         o-comma
          t-rparty-addr1      o-comma
          t-rparty-addr2      o-comma
          t-rparty-city       o-comma
          t-rparty-state      o-comma
          t-rparty-zip        o-comma
          t-rparty-country    o-comma
          t-eff-entry-dt      o-comma
          t-trans-descr       o-comma
          t-trans-cur-cd      o-comma
          left-trim(string(t-trans-amount,">>>>>>>>9.99"))   o-comma   
          t-charg-ind         o-comma
          t-check-nbr        
        CHR(13)
        CHR(10).
      
    
      /* REMITTANCE DETAIL */
      for each b-apet where b-apet.cono    = apet.cono and
                            b-apet.jrnlno  = apet.jrnlno and
                            b-apet.vendno  = apet.vendno and
                            b-apet.checkno = apet.checkno and
                            b-apet.transcd = 03               /* INVOICE */
                            no-lock:
        assign r-item-ref    = REPLACE(b-apet.apinvno,",",";").
        assign r-item-date   = string(YEAR(b-apet.invdt),"9999") + 
                               string(MONTH(b-apet.invdt),"99")  +
                               string(DAY(b-apet.invdt),"99").
        assign r-amount-paid = b-apet.paymtamt.
        assign r-orig-amount = b-apet.amount.
        /*assign r-add-ref     = b-apet.refer.*/
        assign r-disc-amt    = b-apet.discamt.
        assign r-adj-note = "REF " + TRIM(string(b-apet.refer)).    
        assign r-adj-note = replace(r-adj-note,",",";").
        
        find remit where remit.vendno  = b-apet.vendno and
                         remit.checkno = b-apet.checkno and
                         remit.refer   = b-apet.apinvno
                         no-error.
        if not avail remit then
          do:
          create remit.
          assign remit.vendno    = b-apet.vendno
                 remit.checkno   = b-apet.checkno
                 remit.amount    = b-apet.amount
                 remit.apinvno   = b-apet.apinvno
                 remit.reference = b-apet.refer.
        end. /* not avail remit */
        find first poeh where poeh.cono = g-cono and
                              poeh.pono = int(b-apet.refer) and
                              poeh.vendno = apet.vendno
                              no-lock no-error.
        find rptpay where rptpay.vendno  = apet.vendno and
                          rptpay.checkno = apet.checkno
                          no-error.  
        if avail rptpay then
          assign rptpay.tiedfl = if avail poeh and rptpay.tiedfl = " " then 
                                   "yes"  
                                 else 
                                   if not avail poeh then
                                     "no"
                                   else 
                                     rptpay.tiedfl
                 rptpay.notesfl = if rptpay.notesfl = " " then 
                                    b-apet.notesfl
                                  else
                                    rptpay.notesfl.
                                     
        put stream cpc UNFORMATTED
            r-record-type                 o-comma
            r-remit-type                  o-comma
            right-trim(r-item-ref)        o-comma
            r-item-date                   o-comma
            left-trim(string(r-amount-paid,"->>>>>>>9.99"))    o-comma
            left-trim(string(r-orig-amount,"->>>>>>>9.99"))    o-comma
            right-trim(r-add-ref)         o-comma
            r-response-cd                 o-comma
            r-adj-cd                      o-comma
            left-trim(string(r-disc-amt,"->>>>>>>9.99"))       o-comma
            r-adj-note           /*         o-comma
            r-addl-txt           */           
          CHR(13)
          CHR(10).
      end. /* each b-apet */

      /* REMITTANCE DETAIL - FIND MISC CREDITS */
      for each c-apet where c-apet.cono      = apet.cono and
                            c-apet.pidjrnlno = apet.jrnlno and
                            c-apet.vendno    = apet.vendno and
                            c-apet.checkno   = apet.checkno and
                            c-apet.transcd   = 05     /* CREDITS */
                            no-lock:
        assign r-item-ref    = c-apet.apinvno.
        assign r-item-date   = string(YEAR(c-apet.invdt),"9999") + 
                               string(MONTH(c-apet.invdt),"99")  +
                               string(DAY(c-apet.invdt),"99").
        assign r-amount-paid = c-apet.paymtamt.
        assign r-orig-amount = c-apet.amount.
        assign r-add-ref     = c-apet.refer.
        assign r-disc-amt    = c-apet.discamt.    
        assign r-adj-note    = "REF " + TRIM(string(c-apet.apinvno)).   
        assign r-adj-note    = replace(r-adj-note,",",";").
        find remit where remit.vendno  = c-apet.vendno and
                         remit.checkno = c-apet.checkno and
                         remit.refer   = c-apet.apinvno
                         no-error.
        if not avail remit then
          do:
          create remit.
          assign remit.vendno    = c-apet.vendno
                 remit.checkno   = c-apet.checkno
                 remit.amount    = c-apet.amount    
                 remit.apinvno   = c-apet.apinvno
                 remit.reference = c-apet.refer.
        end. /* not avail remit */
        find first poeh where poeh.cono = g-cono and    
                              poeh.pono = int(c-apet.refer) and
                              poeh.vendno = apet.vendno
                              no-lock no-error.
        find rptpay where rptpay.vendno  = apet.vendno and
                          rptpay.checkno = apet.checkno
                          no-error.  
        if avail rptpay then
          assign rptpay.tiedfl = if avail poeh and rptpay.tiedfl = " " then 
                                   "yes"  
                                 else 
                                   rptpay.tiedfl.
        else
          assign rptpay.tiedfl = "no".

        put stream cpc UNFORMATTED
            r-record-type                 o-comma
            r-remit-type                  o-comma
            right-trim(r-item-ref)        o-comma
            r-item-date                   o-comma
            left-trim(string(r-amount-paid,"->>>>>>>9.99"))    o-comma
            left-trim(string(r-orig-amount,"->>>>>>>9.99"))    o-comma
            right-trim(r-add-ref)         o-comma
            r-response-cd                 o-comma
            r-adj-cd                      o-comma
            left-trim(string(r-disc-amt,"->>>>>>>9.99"))       o-comma
            r-adj-note                    o-comma
            r-addl-txt                    
          CHR(13)
          CHR(10).
      end. /* each c-apet */
    end. /* if d_check_amt > 0 */
  end. /* avail apsv */
end. /* each apet */        

put stream cpc UNFORMATTED 
    f-record-type    o-comma
    f-trans-count    o-comma
    f-hash-tot     
  CHR(13)
  CHR(10).
       
output stream cpc close.

if opsys = "unix" then
  do:
  os-copy value(o_file) value(destfilename).
  
end.
output stream cpc close.

 
def var payfile as c format "x(45)"  no-undo.
assign payfile = "/usr/tmp/ck-payments_" + x-month + x-day + x-year + ".txt".
output to value(payfile).
  /* put date into YYMMDD format */
  assign x-today    = TODAY.
  assign x-time     = string(time,"HH:MM").
  assign x-page = 1.
  assign x-line = 4.
  display
     x-today     x-time     g-cono     g-operinit     x-page
      with frame f-header.
  for each rptpay no-lock break by rptpay.tiedfl
                                by rptpay.amount DESCENDING:
    assign x-total = x-total + rptpay.amount.
    display rptpay.vendno 
            rptpay.name
            rptpay.amount
            rptpay.checkno  
            rptpay.tiedfl
            rptpay.notesfl
            with frame f-detail.
    down with frame f-detail.
    assign x-line = x-line + 1.
    if (x-line + 4) > x-page-size then
      do:
      run headers.
    end.
    
    /***** Separate process if Detailed Remit is requested
    display x-blank with frame f-remithdr.
    assign x-line = x-line + 1.
    down with frame f-remithdr.
    for each remit where remit.vendno  = rptpay.vendno  and
                         remit.checkno = rptpay.checkno 
                         no-lock:
      display remit.apinvno
              remit.apinvno
              remit.reference
              remit.amount
      with frame f-remit.
      down with frame f-remit.  
      assign x-line = x-line + 1.
    end.
    display x-blank with frame f-blank.
    down with frame f-blank.    
    assign x-line = x-line + 1. 
    ******/
  end.
  display x-blank with frame f-blank.
  display x-blank
          p-jrnlno
          x-total   
          with frame f-total.
  down with frame f-total.
  output close.
  
  /* EMAIL the Check Print File Results */
  assign cMailSubject = if p-bankno = 15 then
                          "APEC-Bank of America Canada " + 
                                               "MANUAL Check Print Results"
                        else
                          "APEC-Bank of America MANUAL Check Print Results" 
         cMailto      = "ltews@sunsrce.com"     + " " + 
                        "roleksiak@sunsrce.com" + " " +
                        "gyocheva@sunsrce.com"  + " " + 
                        "yradomski@sunsrce.com" + " " +     
                        "ebitri@sunsrce.com"    + " " + 
                        "pabraham@sunsrce.com"  + " " +
                        "dsivak@sunsrce.com"
         cMailmessage = "The attached report contains MANUAL check " +
                        "payments.  Please review and contact AP to " +     
                        "release for payment. " 
         v-emailfile  = payfile.
             
        /* send out rptpay.txt via email */
        unix silent value ("sh /rd/cust/local/jailtinv.sh -b " + 
                           "-s " + "'" + cMailsubject + "' " +   
                           "-l " + "'" + cMailMessage + "' " +   
                           "-n '" + g-operinit + "' " +          
                           "'" + cMailto + "'" + " " +        
                           "'" + v-emailfile + "'").

PROCEDURE headers:
  page.
  assign x-page = x-page + 1.
  assign x-line = 4.
  display   
    x-today     x-time     g-cono     g-operinit     x-page
    with frame f-header.
end.  /* procedure */
