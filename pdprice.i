def new shared var  pd-whse       like icsw.whse                no-undo.
def new shared var  pd-prod       like icsw.prod                no-undo.
def new shared var  pd-cust       like arsc.custno              no-undo.
def new shared var  pd-shipto     like arss.shipto              no-undo.
def new shared var  pd-totnet     as dec format ">>>,>>9.99-"   no-undo.
def new shared var  pd-totcost    as dec format ">>>,>>9.99-"   no-undo.
def new shared var  pd-vendrebamt as dec format ">>>,>>9.99-"   no-undo.
def new shared var  pd-margin     as dec format ">>>,>>9.99-"   no-undo.
def new shared var  pd-margpct    as dec format ">>>,>>9.99-"   no-undo.
def new shared var  pd-netunit    as dec format ">>>,>>9.99-"   no-undo.
def new shared var  pd-pdrecno    like pdsc.pdrecno             no-undo.
def new shared var  pd-levelcd    like pdsc.levelcd             no-undo.
def new shared var  pd-price      as dec format ">>>,>>9.99-"   no-undo.
