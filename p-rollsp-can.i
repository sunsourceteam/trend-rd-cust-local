/*******************************************************************************
  PROCEDURE     : p-rollsp-can.i
  DESCRIPTION   : A/R Standard Statement Print Include for Exchange Rate Calc.
  AUTHOR        : SunSource TA.
  DATE WRITTEN  : 01/31/01
  CHANGES MADE  : 
*******************************************************************************/
assign v-tempmc = 0
       v-tempsc = 0 
       v-tempfut = 0
       v-tempbal = 0.

do i =  1 to 4:
   assign s-perfirstdt[i] = if i = 1 then 
                              {&lastdt}
                            else
                              s-perfirstdt[i - 1] - ({&b-}sasc.arperdays[i] + 1)
          s-perlastdt[i]  = if i = 1 then
                              {&lastdt} + {&b-}sasc.arperdays[1]
                            else 
                              s-perfirstdt[i - 1] - 1.
end. /****end do ***/                              

if {&code} = "m" or {&code} = "b" then
   do:
      do j = 3 to 5 by 2:
         loop1:
         for each aret where aret.cono = g-cono and 
                             aret.custno = {&custno} and 
                             aret.transcd = j and 
                             aret.statustype = true no-lock:
           find first oeeh where oeeh.cono = g-cono and 
                                 oeeh.orderno = aret.invno and 
                                 oeeh.ordersuf = aret.invsuf
                                 no-lock no-error.
           if avail oeeh then 
               do:
               message oeeh.orderno oeeh.ordersuf " " oeeh.user6.
               pause.
               end.
             
             do i = {&startp} to 4.
                if {&cond1} aret.duedt ne ? then 
                  if aret.duedt > s-perlastdt[1] then 
                     do:             
                        v-tempfut = v-tempfut + 
                         (aret.amount * (if avail oeeh then oeeh.user6
                                          else 1)).
                        next loop1.
                     end.             
                  else                 
                  if aret.duedt >= s-perfirstdt[i] and
                     aret.duedt <= s-perlastdt[i]  then         
                     do:          
                        v-tempmc[i] = v-tempmc[i] + 
                          (aret.amount * (if avail oeeh then oeeh.user6
                                          else 1)).
                        next loop1.
                     end.         
                  else 
                  if aret.duedt < s-perfirstdt[4] then 
                     do:
                        v-tempmc[5] = v-tempmc[5] +
                         (aret.amount * (if avail oeeh then oeeh.user6
                                          else 1)).
                        next loop1.
                     end.
                  else
                     next.
                else      
                if aret.invdt >= s-perfirstdt[i] and
                   aret.invdt <= s-perlastdt[i]  then         
                   do:          
                      v-tempmc[i] = v-tempmc[i] + 
                       (aret.amount * (if avail oeeh then oeeh.user6
                                          else 1)).
                      next loop1.
                   end.         
                else 
                if aret.invdt < s-perfirstdt[4] then 
                   do:
                      v-tempmc[5] = v-tempmc[5] + 
                       (aret.amount * (if avail oeeh then oeeh.user6
                                          else 1)).
                      next loop1.
                   end.
                else
                if aret.invdt > s-perlastdt[1] then
                   do:
                      v-tempfut = v-tempfut -
                       (aret.amount * (if avail oeeh then oeeh.user6
                                          else 1)).
                      next loop1.
                   end.
                else      
                   next.
             end. /*** end do ****/
          end. /***** for each aret *****/
      end. /*** do j   ****/
   end.  /**** first do  ****/
if {&code} = "s" or {&code} = "b" then
   do:
   loop:
         for each aret where aret.cono = g-cono and 
                             aret.custno = {&custno} and 
                             aret.transcd = 11 and 
                             aret.sourcecd = 1 and 
                             aret.statustype = true no-lock:
             find first oeeh where oeeh.cono = g-cono and 
                                   oeeh.orderno = aret.invno and 
                                   oeeh.ordersuf = aret.invsuf
                                   no-lock no-error.
              if avail oeeh then 
               do:
               message oeeh.orderno oeeh.ordersuf " " oeeh.user6.
               pause.
               end.
 
             do i = {&startp} to 4:
                if {&cond1} aret.duedt ne ? then 
                  if aret.duedt > s-perlastdt[1] then 
                     do:             
                        v-tempfut = v-tempfut +
                         (aret.amount * (if avail oeeh then oeeh.user6
                                          else 1)).
                        next loop.
                     end.             
                  else                 
                  if aret.duedt >= s-perfirstdt[i] and
                     aret.duedt <= s-perlastdt[i]  then         
                     do:          
                        v-tempsc[i] = v-tempsc[i] + 
                         (aret.amount * (if avail oeeh then oeeh.user6
                                          else 1)).
                        next loop.

                     end.
                  else 
                  if aret.duedt < s-perfirstdt[4] then 
                     do:
                        v-tempsc[5] = v-tempsc[5] + 
                         (aret.amount * (if avail oeeh then oeeh.user6
                                          else 1)).
                        next loop.
                     end.
                  else
                     next.
                else      
                if aret.invdt >= s-perfirstdt[i] and
                   aret.invdt <= s-perlastdt[i]  then         
                   do:          
                      v-tempsc[i] = v-tempsc[i] + 
                       (aret.amount * (if avail oeeh then oeeh.user6
                                          else 1)).
                      next loop.
                   end.         
                else 
                if aret.invdt < s-perfirstdt[4] then 
                   do:
                      v-tempsc[5] = v-tempsc[5] + 
                       (aret.amount * (if avail oeeh then oeeh.user6
                                          else 1)).
                      next loop.
                   end.
                else
                if aret.invdt > s-perlastdt[1] then
                   do:
                      v-tempfut = v-tempfut + 
                       (aret.amount * (if avail oeeh then oeeh.user6
                                          else 1)).
                      next loop.
                   end.
                else      
                   next.
             end. /*** end do ****/
          end. /***** for each aret *****/
   end.  /**** first do  ****/
if {&code} = "b" then 
   do i = {&startp} to 5:
      if {&need} = "tot" then 
         v-tempboth[i] = v-tempmc[i] - v-tempsc[i].
      else          
         v-tempboth[i] = v-tempsc[i] + v-tempmc[i].
   end.
if {&need} = "tot" then 
   do i = {&startp} to 5:
      v-tempbal = if {&code} = "b" then
                     v-tempbal + v-tempboth[i]
                  else
                  if {&code} = "m" then 
                     v-tempbal + v-tempmc[i]
                  else     
                  if {&code} = "s" then 
                     v-tempbal + v-tempsc[i] 
                  else 
                     0.
   end.                  
                  
