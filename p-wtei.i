/* SX 55 */
/* p-wtei.i 1.3 10/15/98 */
/*h*****************************************************************************
  INCLUDE      : p-wtei.i
  DESCRIPTION  : Line processing for WT receiving
  USED ONCE?   : yes
  AUTHOR       : rhl
  DATE WRITTEN : 08/20/90
  CHANGES MADE :
    10/07/92 mms; TB#  8152 WM WT interface
    02/22/93 rhl; TB#  7729 Charge Quantity field in Extended screen
    02/23/93 rhl; TB# 10134 Keep the weight logic consistent with PO
    05/28/93 mms; TB# 11634 poei weight and cube logic removed from tb 10134
    06/15/93 mms; TB# 11820 Display entire unavailable desc on f7 extend screen
    07/20/93 tdd; TB# 12317 Allow <f7> extend on nonstocks
    11/05/93 mwb; TB# 13520 Carry unitconv to 10 decimals
    04/27/95 mtt; TB# 16822 <F10> Whse Mgr not changed to all caps
    09/07/95 tdd; TB# 16933 Can't receive OAN into unavailable
    03/14/97 mms; TB#  7241 Spec8.0 Special Price Costing; added logic to allow
        for loading if special costing information off the WT line.  The line
        information will be stored in v- variables to be used in serial and lot
        processing.
    05/19/97 jl;  TB#  5594 (4I) Unavailable Tracking. Added validation to
        poei.reasunavty so it is a required field if a unavailable qty is
        entered on the F7 - Extend screen
    04/27/98 ama; TB# 22762 Add user hook v-mode = "oel" to wtei99.p
    10/13/98 kjb; TB#  9731 Add a second nonstock description.
    09/09/99 abe; TB# 26612 Added user hook &user_extend_edit, user_edit
    01/08/01 lbr; TB# e5104 Added var passed to icets.p.
    02/13/02 bpa; TB# e7123 Verify changes in PO/WT receiving
    06/12/02 TB# e13850 bp ; Add user hooks.
    08/11/03 emc; TB# e17624 Ser/Lot qty to allocate incorrect
    01/24/05 bpa; TB# e21560 Change to quantity recieved on F7 extend screen
        was not being saved to line.
    TB# e21560 Rolling out change made above. GUI was incorrect,
        so this change reflecting GUI functionality is causing problems in CHUI
        now. The change will be rethought and done in another TB.
    03/09/05 bpa; TB# e21560 Remove access to update Stock Quantity Received.
        Only allow update of Quantity Recieved on Line
   10/16/2007 dkt added 3 hooks                                            
      {p-wtei.z99 &user_update_wteil = "*"}                                  
      {p-wtei.z99 &print_labels = "*"}                                       
      {p-wtei.z99 &user_before_lineedit = "*"}                               
      (this could be in wtei99.p("oel") if v-wtei99fl would equal true.)     
 

*******************************************************************************/
/*tb e7123 02/11/02 bpa; Added definition for sasoo.verrcvchgfl value */
def            buffer b-sasoo for sasoo.
def            var v-verrcvchgfl  like sasoo.verrcvchgfl               no-undo.
def            var v-rcvverfl     as log                               no-undo.
def            var v-firstpass    as log                 init "yes"    no-undo.
def            var v-firstfl      as log                 init "yes"    no-undo.


{p-wtei.z99 &user_before_lineedit = "*"}    /* SX 55 */

/*d Assign old variables for change edits */
assign
    g-prod     = poei.shipprod
    o-qtyrcv   = poei.qtyrcv
    o-unit     = poei.unit
    v-firstfl  = yes.

/*d Find the WTEL line */
{w-wtel.i poei.pono poei.posuf poei.lineno no-lock}

if not avail wtel then do:
    message g-cono poei.pono poei.posuf poei.lineno. pause.
    run err.p(9999).
    next.
end.

if can-do("s,l",poei.serlotty) then
    {w-wteh.i poei.pono poei.posuf exclusive-lock}

{w-icsp.i poei.shipprod no-lock}

/* tb e7123 02/11/02 bpa; Set variable with value from sasoo.verrcvchgfl */
{w-sasoo.i g-operinit no-lock b-}
if avail b-sasoo then
    v-verrcvchgfl = b-sasoo.verrcvchgfl.

details:
do for icsw while true on endkey undo updt, next updt
              on error  undo details, next details:
    hide frame f-wteie.
    hide frame f-poeis.

    /*d Allow entry of the qty received */
    update
        poei.qtyrcv
       {p-wtei.z99 &user_update = "*"}   /* SX 55 */
     
    go-on(f7 f9 f10) with frame f-wteil editing:
        readkey.

        {p-wtei.z99 &user_top_editing = "*"}

        if {k-after.i} or {k-func.i} then do:
            /*tb e7123 02/12/02 bpa; Determine if we need to track a change
            being made to the line */

            if (input poei.qtyrcv <> wtel.qtyship and
                v-rcvverfl = no and v-firstpass = yes and
                v-verrcvchgfl) then
              assign
                  v-firstpass = no
                  v-rcvverfl  = yes.
            assign
                o-qtyrcv       = input poei.qtyrcv
                o-qty          = if poei.stkqtyrcv = 0       and
                                    v-wtqtyrcvfl   = yes     and
                                    wtel.transtype <> "do":u and
                                    v-firstfl
                                 then wtel.stkqtyship
                                 else poei.stkqtyrcv
                v-firstfl      = no
                poei.stkqtyrcv = input poei.qtyrcv *
                                   {unitconv.gas &decconv = poei.conv}
                poei.nosnlots  = if can-do("s,l",poei.serlotty) then
                                     poei.nosnlots - o-qty + poei.stkqtyrcv
                                 else 0.
             if can-do("s,l",poei.serlotty) then
                wteh.nosnlotsi  = wteh.nosnlotsi - o-qty + poei.stkqtyrcv.
             if poei.stkqtyrcv > wtel.stkqtyord and not {k-func.i} then do:
                run err.p(8649).
                pause 3 no-message.
            end.

            /*u User Hook*/
            {p-wtei.z99 &user_edit = "*"}

        end.

        apply lastkey.
    end.

    {p-wtei.z99 &after_update = "*"}  /* SX 55 */

    /*u user hook */
    if v-wtei99fl = yes then run wtei99.p("oel").
    /*tb e7123 02/12/02 bpa; If change to line made, and we are verifying
        change, then show warning and re-enter line */
    if v-rcvverfl = yes then do:
        v-rcvverfl = no.
        run warninga.p("Discrepancy From WT. Please Verify Changes Made").
        next details.
    end.
    v-firstpass = yes.
    /*d*** If F7 is pressed, access the Extended screen ****/
    /*tb 11820 06/15/93 mms; Changed s-reasunavty to s-unavdesc to display
      entire unavailable reason */
    /*tb 12317 07/20/93 tdd; Allow <f7> extend screen on nonstocks */
    if {k-func7.i} then
    xtend:
    do with frame f-wteie on endkey undo xtend, next details:
        /*tb 16822 04/27/95 mtt; <F10> Whse Mgr not changed to all caps */
        put screen row 21 column 19 color messages "F7-EXTEND".

        {w-icsw.i poei.shipprod g-whse no-lock}

        o-qty = poei.stkqtyrcv.

        /*d Display information on the extended screen */
        display
            wtel.qtyord
            wtel.unit
            wtel.stkqtyord
            "" @ icsp.unitstock
            "" @ s-unitstock
            "" @ poei.reasunavty
            "" @ s-unavdesc
            poei.qtyunavail
            poei.qtyassign
            poei.stkqtyrcv.

        /*tb 9731 10/13/98 kjb; Added proddesc2 for second nonstock
            description */
        if poei.nonstockty ne "n" then
            display
                icsw.binloc1
                icsw.binloc2
                icsp.unitstock when not can-do("l,s",poei.serlotty)
                icsp.descrip[1] + " " + icsp.descrip[2] @ s-desc.
        else
            display
                wtel.proddesc + " " + wtel.proddesc2 @ s-desc.

        {w-sasta.i L poei.reasunavty no-lock}

        s-unavdesc = if avail sasta then sasta.descrip
                     else if poei.reasunavty ne "" then v-invalid
                     else "".

        display s-unavdesc.

        /*d Allow entry of the stock qty received */
        /*tb 12317 07/20/93 tdd; Don't allow <f7> extend update on nonstocks */
        /*tb 16933 09/07/95 tdd; Can't receive OAN into unavailable */
        /*tb 5594 (4I) 05/19/97 jl; Added validation to poei.reasunavty */
        update
            poei.qtyunavail validate
                (poei.qtyunavail le poei.stkqtyrcv,
                "Unavailable Must not be Greater than Received (5938)")
                when (not can-do("s,l",poei.serlotty)) and
                  not can-do("do",g-wttype) and wtel.prodinrcvfl = yes

            poei.reasunavty
                validate({v-sasta.i L poei.reasunavty "/*"} or
                            (input frame f-wteie poei.qtyunavail = 0 and
                             poei.reasunavty = ""),
                         (if input frame f-wteie poei.reasunavty = ""
                              then ({valmsg.gme 2100})
                          else {valmsg.gme 4027}))
                when (not can-do("s,l",poei.serlotty)) and
                  not can-do("do",g-wttype) and wtel.prodinrcvfl = yes
        editing:
            readkey.
            /*u User Hook*/
            {p-wtei.z99 &user_extend_edit = "*"}

            if {k-after.i} then do:

                if input poei.reasunavty ne o-reasunavty then do:
                    o-reasunavty = input poei.reasunavty.

                    {w-sasta.i L o-reasunavty no-lock}

                    s-unavdesc = if avail sasta then sasta.descrip
                                 else if o-reasunavty ne "" then v-invalid
                                 else "".

                    display s-unavdesc.
                end.
            end.

            /*tb 16822 04/27/95 mtt; <F10> Whse Mgr not changed to all caps */
            if {k-cancel.i} or {k-jump.i} then do:
              /*tb 16822 04/27/95 mtt; <F10> Whse Mgr not changed to all caps */
                put screen row 21 column 19 color messages "F7-Extend".
            end.

            apply lastkey.
        end.

        if poei.stkqtyrcv > wtel.stkqtyord then do:
            run err.p(8649).
            /*tb 16822 04/27/95 mtt; <F10> Whse Mgr not changed to all caps */
            {pause.gsc 3}
        end.

        poei.nosnlots  = if can-do("s,l",poei.serlotty) then
                              poei.nosnlots - o-qty + poei.stkqtyrcv
                         else 0.

        if can-do("s,l",poei.serlotty) then
            wteh.nosnlotsi  = wteh.nosnlotsi - o-qty + poei.stkqtyrcv.

        /*d Need pause if the product is nonstock since no update allowed */
        /*tb 16822 04/27/95 mtt; <F10> Whse Mgr not changed to all caps */
        if poei.nonstockty = "n" then do:
            {pause.gsc}
        end.

        clear frame f-wteie.
        hide frame f-wteie.

        /*tb 16822 04/27/95 mtt; <F10> Whse Mgr not changed to all caps */
        put screen row 21 column 19 color messages "F7-Extend".
        next.
    end.

    /*d*** If F9 pressed, allow Serial/Lot entry ****/
    if {k-func9.i} and can-do("s,l",poei.serlottype) then do:
        /*tb 16822 04/27/95 mtt; <F10> Whse Mgr not changed to all caps */
        put screen row 21 column 48 color messages "F9-SERIAL/LOT".

        /*d Find and load special price costing information off the WT line
            for use in the serial and lot processing below. */
        {icss.gfi
            &prod        = wtel.shipprod
            &icspecrecno = wtel.icspecrecno
            &lock        = "no"}

        assign
            {speccost.gas &com = "/*"}
            o-qty        = poei.stkqtyrcv
            o-nosnlotsi  = poei.nosnlots
            v-nosnlotsi  = poei.nosnlots
            s-lineno     = poei.lineno
            v-errfl      = ""
            v-qtyunavail = poei.qtyunavail
            v-conv       = /*tb 13520 11/05/93 mwb; conv to 10 dec */
                           {unitconv.gas &decconv = poei.conv}
            v-eachprice = if poei.eachfl = yes then poei.price / v-conv
                          else (poei.price * v-conv) / poei.stkqtyrcv
            v-allprice  = if poei.eachfl = no then poei.price
                          else (poei.price / v-conv) * poei.stkqtyrcv.

        status default.

        /*d Serial screen */
        if poei.serlottype = "s" then
            /*tb e5104 Added "" param before output */
            run icets.p (wteh.shiptowhse,   /* Ship To Whse */
                         poei.shipprod,     /* Product      */
                         "wt",              /* Whse Trans   */
                         g-wtno,            /* WT #         */
                         g-wtsuf,           /* WT Suf #     */
                         poei.lineno,       /* WT Line #    */
                         0,                 /* Seq # (0)    */
                         true,              /* Return Flag-True for Receiving */
                         poei.stkqtyrcv,    /* */
                         v-nosnlotsi,
                         wtel.stkqtyord,
                         v-eachprice,
                         g-wttype,
                         "",
                         output poei.stkqtyrcv,
                         output v-errfl,
                         input-output v-qtyunavail).

        /*d Load screen */
        else if poei.serlottype = "l" then
            run icetl.p(g-whse,
                        poei.shipprod,
                        "wt",
                        g-wtno,
                        g-wtsuf,
                        poei.lineno,
                        0,
                        true,
                        poei.stkqtyrcv,
                        v-nosnlotsi,
                        wtel.stkqtyord,
                        v-eachprice,
                        g-wttype,
                        output poei.stkqtyrcv,
                        output v-errfl,
                        input-output v-qtyunavail).

        status default kblabel("return") + " to Receive Quantity "
            + kblabel("go") + " to Finish".

        put screen row 21 column 3 color messages
            "                F7-Extend                   " +
            (if can-do("s,l",poei.serlottype) then
            " F9-Serial/Lot " else "               ") +
            /*tb 8152 10/07/92 mms */
            if poei.wmfl and not can-do("do",g-wttype) then
            " F10-Whse Mgr  " else "               ".

        if v-errfl = "n" and poei.cancelfl = no then poei.cancelfl = yes.

        if v-errfl = "s" then do:
            /*tb 13520 11/05/93 mwb; conv to 10 dec */
            assign
                poei.qtyrcv    = round(poei.stkqtyrcv /
                                     {unitconv.gas &decconv = poei.conv},2)
                poei.nosnlots  = poei.stkqtyrcv
                o-nosnlotsi     = o-nosnlotsi - (o-qty - poei.stkqtyrcv)
                wteh.nosnlotsi  = wteh.nosnlotsi - (o-qty - poei.stkqtyrcv).

            display poei.qtyrcv.
        end.

        /*d Recalculate the number of serial/lot records left to allocate */

        if poei.serlottype = "s" then do:
            {p-nosn.i &seqno     = 0
                      &ordertype = ""t""
                      &orderno   = g-wtno
                      &ordersuf  = g-wtsuf
                      &usecode   =  *
                      &com       = "/*"}

            v-nosnlotsi = round(poei.stkqtyrcv - v-noassn,0).
        end.

        else if poei.serlottype = "l" then do:
            {p-nolots.i &seqno     = 0
                       &ordertype = ""t""
                       &orderno   = g-wtno
                       &ordersuf  = g-wtsuf
                       &usecode   =  *
                       &com       = "/*"}

            /*tb 7410 08/12/92 mwb; round to two places not 0 */
            v-nosnlotsi = round(poei.stkqtyrcv - v-noassn,2).
        end.

        assign
            poei.nosnlots    = v-nosnlotsi
            poei.qtyunavail  = v-qtyunavail
            wteh.nosnlotsi   = wteh.nosnlotsi - (o-nosnlotsi - poei.nosnlots).
        next.
    end.

    /*tb 8152 10/07/92 mms */
    /*o**** Process the F10 function key for access to WM bin information */
    else if {k-func10.i} and poei.wmfl and not can-do("do",g-wttype)
    then do:
        /*tb 16822 04/27/95 mtt; <F10> Whse Mgr not changed to all caps */
        put screen row 21 column 63 color messages "F10-WHSE MGR".

        run poeiwm.p(v-poeiid,"t",0).

        /*tb 16822 04/27/95 mtt; <F10> Whse Mgr not changed to all caps */
        put screen row 21 column 63 color messages "F10-Whse Mgr".
    end.

    leave.
end.


