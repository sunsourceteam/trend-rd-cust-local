
/* apec9a.p 1.1 09/16/98 */
/*h****************************************************************************
  PROCEDURE      : apec9a
  DESCRIPTION    : Standard Check Print Format 1
  AUTHOR         : cm
  DATE WRITTEN   : 09/08/98
  CHANGES MADE   :
******************************************************************************/

/*d Define shared buffers for use in split program */
def     shared buffer report for report.


def var p-nonneg     as c  format "x(3)"             no-undo.   
def var p-manual     as c  format "x(3)"             no-undo.
def var p-cancheckdt as c  format "x(8)"             no-undo.
def var p-checkamt   as de format "$***,***,**9.99-" no-undo.
def     shared var emailcnt as i format "99"                no-undo.
def     shared var z-vendno like apsv.vendno                no-undo.
def            var sdi-cono like apsv.cono                  no-undo.
def var x-micrspace1   as c  format "x(1)" init " "         no-undo.
def var x-micrspace2   as c  format "x(2)" init "  "        no-undo.
def var x-micrspace3   as c  format "x(3)" init "   "       no-undo.   
def var x-micrspace4   as c  format "x(4)" init "    "      no-undo.
def var x-micrspace5   as c  format "x(5)" init "     "     no-undo.
def var x-micrspace6   as c  format "x(6)" init "      "    no-undo.   
def var x-micrspace7   as c  format "x(7)" init "       "   no-undo.    
def var x-micrspace8   as c  format "x(8)" init "        "  no-undo.
def var x-micrspace9   as c  format "x(8)" init "         " no-undo.
def var x-filler       as c                init " "         no-undo.

{g-all.i}
{g-rptctl.i}

{lasersdi.lva new}                                          /*si01*/

def var v-micrposABA as c no-undo.
def var v-micrposRTE as c no-undo.
def var v-bankmicrABA as c no-undo.
def var v-bankmicrRTE as c no-undo.

define buffer edd-sapb for sapb.

{g-apec.i}

{apec1.lva}

{apecedi.gva}                                               

put control "~033(10U~033(s1p12v0s3b16901T".

find edd-sapb where recid(edd-sapb) = v-sapbid no-lock no-error.
if avail edd-sapb then
   assign p-nonneg = edd-sapb.optvalue[15]
          p-manual = edd-sapb.optvalue[16].
   
/*SI01-Begin*/
/* si01; set up check stuff
   NOTE: routing# must be in pos 1-9 in crsb.bankacct, remainder is acct# */
{w-crsb.i p-bankno no-lock}

assign
    v-alttray  = if v-deftray = v-upper then v-manual else v-upper
    v-bankmicr = if not avail crsb then "**** ERROR ****"
                 else
                 if crsb.bankno <> 91 and crsb.bankno <> 95 and
                    crsb.bankno <> 93 and crsb.bankno <> 41 and 
                    crsb.bankno <> 51 and crsb.bankno <> 15 and
                    crsb.bankno <> 16 and crsb.bankno <> 17 and
                    crsb.bankno <> 20 then
                   ": ;"   + (substr(crsb.bankacct,1,9)) +
                   ";   " + trim(substr(crsb.bankacct,10,11)) +
                   ":"
                 else
                   ": ;"  + substr(crsb.transrouteno,1,9) +
                   ";   " + substr(crsb.bankacct,1,12) +
                   ":"
    v-altform  = v-esc + "&f8y4X"
    v-chkpos1  = v-esc + "&a5040v0H"
    v-micrfont = v-esc + "(2K" + v-esc + "(s0p8.00h10.0v0s0b0T"
    v-micrpos = if crsb.bankno = 22 then
                  v-esc + "&a7360v1040H"
                else
                if crsb.bankno = 91 or crsb.bankno = 95 or 
                   crsb.bankno = 93 or
                   ((crsb.bankno = 41 or crsb.bankno = 51) and g-cono = 80)
                  then 
                  v-esc + "&a7400v1040H" /*->if canadian, drop the micrline*/
                else
                if crsb.bankno =  5 or crsb.bankno = 15 or crsb.bankno = 16 or
                   crsb.bankno = 17 or crsb.bankno = 20 then
                  v-esc + "&a7400v920H"
                else
                  /* down a line-was &a7340v1040H */
                  v-esc + "&a7400v1040H" /*->moved the starting point of the*/
                                           /* micrline from 1040H to 900H */
    v-chkfont  = v-goth15 + v-10cpi.
if p-bankno = 5 then
  do:
  assign v-micrposABA = v-esc + "&a7400v1820H".   /*vertical is controlled  */
  assign v-micrposRTE = v-esc + "&a7400v3085H".   /*by the # before the "v" */
end.                                              /*horizontal is controlled*/
if p-bankno = 15 or                               /*by the # after the "v"  */
   p-bankno = 16 or p-bankno = 17 or p-bankno = 20 then
  do:
  assign v-micrpos    = v-esc + "&a7400v750H".
  assign v-micrposABA = v-esc + "&a7400v1825H".
  assign v-micrposRTE = v-esc + "&a7400v3170H". 
end.

if (p-bankno = 5  or p-bankno = 44 or 
    p-bankno = 53 or p-bankno = 15) and
  avail crsb then  
  do:   
  if p-nonneg = "no" then   
    do:
    if p-bankno = 15 or p-bankno = 16 or p-bankno = 17 or p-bankno = 20 then
      assign v-bankmicr = ":" + v-micrposABA + ";" +
                          substr(crsb.transrouteno,1,9) + "; " +
                          v-micrposRTE + substr(crsb.bankacct,1,12) + ":". 
    else    
      assign v-bankmicr = ":" + v-micrposABA + ";" +
                          substr(crsb.transrouteno,1,9) + "; " +
                          v-micrposRTE + substr(crsb.bankacct,10,11) + ":". 
  end.
  else
    assign v-bankmicr = " ".
end.

    
if p-nonneg = "no" then
  do:
  assign
    v-rxform   = if not avail crsb then
                   v-esc + "&f6y4X"
                 else if p-bankno = 93 or p-bankno = 15 then
                   v-esc + "&f60y4X"
                 else if (p-bankno = 41 or p-bankno = 51) and g-cono = 80 then
                   v-esc + "&f56y4X"
                 else
                   v-esc + "&f6y4X".
end.
/*SI01-End*/
/** das - emailing */
if p-nonneg = "yes" then
  do:
  find apsv where apsv.cono = g-cono and
                  apsv.vendno = z-vendno
                  no-lock no-error.
  if p-bankno = 93 or p-bankno = 15 or p-bankno = 16 or p-bankno = 17 then
    assign v-rxform = v-esc + "&f61y4X".
  else
    assign v-rxform = v-esc + "&f7y4X".
end.
  {f-apec9.i}                                                 /*si01*/
  {f-apec9can.i}
  {f-apec9dom.i}
  {p-apec9.i}                                                 /*si01*/
/*if p-nonneg = "no" then   /* das - emailing */*/
  put control v-reset.                                        /*si01*/

