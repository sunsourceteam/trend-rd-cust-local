/*h*****************************************************************************
  PROCEDURE      : zsdireservechko.p
  DESCRIPTION    : Delayed Reservation Processing 
  AUTHOR         : tah
  DATE WRITTEN   : 11/23/09
  CHANGES MADE   :
*******************************************************************************/

/*d Define input and output parameters */
define input parameter ip-orderno   as int  no-undo.
define input parameter ip-ordersuf  as int  no-undo.
define input parameter ip-reqshipdt as date  no-undo.


{g-all.i}
{g-ic.i}
{g-jump.i}
{nxtrend.gpp}
{g-oeet.i "shared"}

def shared  var s-maint       as logical format "add/chg" no-undo.
def shared  var v-headfl      as logical            no-undo.
def shared  var s-crreasonty  like oeeh.crreasonty  no-undo.
def shared  var v-shiptofl    like sasoo.shiptofl   no-undo.
def shared  var v-credpostfl  like sasoo.credpostfl no-undo.
{g-oeetb.i "shared" "/*"}
{g-oeeth.i "shared"}
{g-oeetf.i "shared"}
{g-oeetl.i "new shared"}
{g-oeetlo.i "new shared"}
{g-oeetcr.i "new"}
{g-oeetlr.i "new shared"}
{g-oeetlx.i "new shared"}
{speccost.gva "new shared"}
{g-oeetk.i "new" "/*"}


/*e All these are defined as shared in oeeslu.p */
def new shared buffer b2-icsw   for icsw.
def new shared var v-actqty     like pdsc.actqty                    no-undo.
def new shared var v-qtyyymm    like pdsc.qtyyymm                   no-undo.
def new shared var v-lastlevel  as integer                          no-undo.
def new shared var v-qtybo      like icsw.qtybo                     no-undo.
def new shared var v-qtyonhand  like icsw.qtyonhand                 no-undo.
def new shared var v-qtyreservd like icsw.qtyreservd                no-undo.
def new shared var v-qtycommit  like icsw.qtycommit                 no-undo.
def new shared var v-qtyonorder like icsw.qtyonorder                no-undo.
def new shared var v-display    as logical initial "no":U           no-undo.
def new shared var v-noassn     like oeel.nosnlots                  no-undo.
def new shared var s-bofl       as logical                          no-undo.
def new shared var v-commitfl   as l init no                        no-undo.
def new shared var s-origsuf    like oeel.ordersuf                  no-undo.

/*d Other local variables */
def var o-idoeeh                as recid                            no-undo.
def var o-orderno               like oeeh.orderno                   no-undo.
def var o-ordersuf              like oeeh.ordersuf                  no-undo.
def var o-maint                 as l                                no-undo.
def var o-custno                like oeeh.custno                    no-undo.
def var o-oetype                like oeeh.transtype                 no-undo.
def var o-shipto                like oeeh.shipto                    no-undo.
def var o-idicsp                as recid                            no-undo.
def var o-idicsw1               as recid                            no-undo.
def var o-idicsw2               as recid                            no-undo.
def var o-svcost                as dec format ">>>>>>9.999999"      no-undo.
def var v-convqty               like oeelk.stkqtyship               no-undo.
def var v-conv2                 as de                               no-undo.
def var lQtyChanged             as logical                          no-undo.
def var lHoldDelayFl            as logical                          no-undo.
def var lBodFl                  as logical init no                  no-undo.
def var iUpdTypeCnt             as int                              no-undo.
def var cUpdType                like oeeh.updtype                   no-undo.

/*d Variables for Cross Reference Checks for Kit Components */
def new shared var v-xreferrfl   as logi                            no-undo.

/*o**************************** Main Processing*******************************/
/*d Save off shared variables to restore at end */
assign
    o-idicsp   = v-idicsp
    o-idicsw1  = v-idicsw1
    o-idicsw2  = v-idicsw2
    o-svcost   = v-prodcost
    o-idoeeh   = v-idoeeh
    o-orderno  = g-orderno
    o-ordersuf = g-ordersuf
    o-maint    = s-maint
    o-custno   = g-custno
    o-oetype   = g-oetype
    o-shipto   = g-shipto.


run oeebti.p.

/*e Since this is a report function it is possible that the AO/SASOO variables
    being set in oeebti.p are not the original new shared set from login time
    lig.p).  Rather, the oeebti.p program could have been initializing another
    set of shared variables defined in other function(s) running this
    program.  Want to force the g-oeloadedfl to be no, so if the operator
    accesses OEET, they will be forced to re-load the AO/SASOO variables. */
find     b-oeeh  where
         b-oeeh.cono       = g-cono      and
         b-oeeh.orderno    = ip-orderno  and
         b-oeeh.ordersuf   = ip-ordersuf and
         b-oeeh.invoicedt  = ?           and
         b-oeeh.stagecd   <> 9           and 
         b-oeeh.stagecd   <> 0
    no-lock no-error.
if b-oeeh.stagecd > 1 or not avail b-oeeh then
  return.
assign lQtyChanged = no.

ProcessLines:
for each b-oeel use-index k-oeel where
         b-oeel.cono         = g-cono          and
         b-oeel.orderno      = b-oeeh.orderno  and
         b-oeel.ordersuf     = b-oeeh.ordersuf and
         b-oeel.statustype   = "a":U           and
        ( b-oeel.specnstype   = ""              or
          b-oeel.kitfl = true                   or
          (b-oeel.ordertype = "w" and
           b-oeel.orderaltno <> 0))                 and
         b-oeel.bono         = 0              
  no-lock:

  if b-oeel.ordertype = "w" and
     b-oeel.orderaltno <> 0 then do:
     find first kpet where kpet.cono  = g-cono and
                           kpet.wono  = b-oeel.orderaltno and
                           kpet.stagecd < 3 no-lock no-error.
     if avail kpet then    
       run zsdireservechkkp (input kpet.wono,
                             input kpet.wosuf,
                             input (if ip-reqshipdt = ? then
                                      today
                                    else
                                      ip-reqshipdt)).
    next.
  end.
  else
  if b-oeel.orderaltno <> 0 then do:
    next.
  end.  




      /*d Local processing */
/*d Load product and product warehouse records */
  {icsp.gfi
      &prod = b-oeel.shipprod
      &b    = "b-"
      &lock = "no"}

  {icsw.gfi
      &prod = b-oeel.shipprod
      &whse = b-oeel.whse
      &b    = "b-"
      &lock = "no"}

  if (not avail b-icsw or not avail b-icsp)
     and not b-oeel.kitfl then next ProcessLines.
  if avail b-icsw and avail b-icsp then do:
/*d Load quantity available for this product */
    {p-netavl.i "/*" "b-"}
  end.
/*d Determine if we can continue to delay reservation or the quantity
    to reserve if we cannot */
/*tb e6098 09/13/00 gp; Set kit param to no if wt bod */
  lBodFl       = if b-oeel.specnstype <> "n" and
                     avail b-icsp and
                     b-icsp.kittype = "b":u and
                     b-icsp.bodtransferty = "t":u
                         then no
                 else b-oeel.kitfl.
  /* Add back in the qty already reserved */
  assign s-netavail = s-netavail + b-oeel.stkqtyship.
         o-stkqtyship = b-oeel.stkqtyship.
  if b-oeel.kitfl then do:
  
    for each oeelk use-index k-oeelk where
             oeelk.cono      = g-cono    and
             oeelk.ordertype = "o":u     and
             oeelk.orderno   = b-oeel.orderno    and
             oeelk.ordersuf  = b-oeel.ordersuf   and
             oeelk.lineno    = b-oeel.lineno                          
    exclusive-lock:
 
      assign
           oeelk.prevprod    = oeelk.shipprod 
           oeelk.prevqtyship = oeelk.stkqtyship
           oeelk.prevspecns  = oeelk.specnstype
           oeelk.prevqtyrsv  = oeelk.qtyreservd
           oeelk.prevqtyord  = oeelk.stkqtyord
           oeelk.prevunit    = oeelk.unit
           oeelk.prevconv    = oeelk.conv.

      if  oeelk.comptype   <> "r":u and
          oeelk.prevqtyord >= 0     and
          oeelk.specnstype ne "n":u then do:
        {w-icsp.i oeelk.shipprod no-lock b-}
        {w-icsw.i oeelk.shipprod g-whse exclusive-lock b-}
      
      
        if avail b-icsw and (b-icsw.statustype <> "s" or
           oeelk.specnstype <> "") then
          next.           
                    
      /*d**************** ADJUST ICSW BUCKETS *****************/
      /*d Subtracts out the old quantities */
        
        v-display = no.

        {p-oeetlw.i &oeel   = "b-oeel."
                    &oeeh   = "b-oeeh."
                    &sign   = "-"
                    &qty    = "oeelk.prevqtyship"
                    &qtyrsv = "oeelk.prevqtyrsv"
                    &comp   = "or b-oeel.specnstype = 'n'"
                    &boqty  = "if can-do('s,t',b-oeeh.orderdisp) then
                                 (0 - oeelk.prevqtyrsv)
                               else if (b-oeel.bono <> 0)
                                   then 0
                               else( 0 - oeelk.prevqtyrsv)"
                    &display-comment-out = "yes"
                    &icsw-comment-out    = "yes"
                    &so-comment-out      = "yes"
                    &botype-comment-out  = "yes"}
      assign oeelk.stkqtyship = 0
             oeelk.qtyreservd = 0.
      end.    /* if all conditions are met to update */
    end.
  end.

  if b-oeel.kitfl then do:
  
    for each oeelk use-index k-oeelk where
             oeelk.cono      = g-cono    and
             oeelk.ordertype = "o":u     and
             oeelk.orderno   = b-oeel.orderno    and
             oeelk.ordersuf  = b-oeel.ordersuf   and
             oeelk.lineno    = b-oeel.lineno                          
    exclusive-lock:
 
      assign
           oeelk.prevprod    = oeelk.shipprod 
           oeelk.prevqtyship = oeelk.stkqtyship
           oeelk.prevspecns  = oeelk.specnstype
           oeelk.prevqtyrsv  = oeelk.qtyreservd
           oeelk.prevqtyord  = oeelk.stkqtyord
           oeelk.prevunit    = oeelk.unit
           oeelk.prevconv    = oeelk.conv.

    end.
  end.
/*d Load product and product warehouse records */
  {icsp.gfi
      &prod = b-oeel.shipprod
      &b    = "b-"
      &lock = "no"}

  {icsw.gfi
      &prod = b-oeel.shipprod
      &whse = b-oeel.whse
      &b    = "b-"
      &lock = "no"}

  
  run netavail.p(input (if avail b-icsw then
                           recid(b-icsw)
                        else
                           ?),
                   input ip-reqshipdt,
                   input b-oeel.orderno,
                   input b-oeel.ordersuf,
                   input b-oeel.lineno,
                   input (b-oeel.bono > 0),
                   input lBodFl,
                   input-output s-netavail,
                   input-output v-buildqty,
                   input-output v-maxbuildqty,
                   output v-delayresrvfl).
  lHoldDelayFl = v-delayresrvfl.
  /*
  if g-operinits = "tah" then do:
    message b-oeel.lineno v-buildqty.
    pause.
  end.
  */  
/*d If the line is ready for reservation, is a BOD kit, or a Tally then
    continue processing.  BOD kit lines will always be processed even if they
    continue to delay (qtyship = 0) so that oeetltk.p will update
    the components properly. */
/*  if  b-oeel.kitfl = yes or b-oeel.tallyfl then do:         */

    /*d Set previous component values for processing in oeetltk.p for
        all components */
/*
    if b-oeel.kitfl = yes then do:

        for each oeelk use-index k-oeelk where
            oeelk.cono      = g-cono          and
            oeelk.ordertype = "O":U           and
            oeelk.orderno   = b-oeel.orderno  and
            oeelk.ordersuf  = b-oeel.ordersuf and
            oeelk.lineno    = b-oeel.lineno
        exclusive-lock:

            assign
                oeelk.prevprod    = oeelk.shipprod
                oeelk.prevqtyship = oeelk.stkqtyship
                oeelk.prevspecns  = oeelk.specnstype
                oeelk.prevqtyrsv  = oeelk.qtyreservd
                oeelk.prevqtyord  = oeelk.stkqtyord
                oeelk.prevunit    = oeelk.unit
                oeelk.prevconv    = oeelk.conv.

        end. /* for each oeelk */

    end. /* if b-oeel.kitfl = yes */
*/
    /* Set previous component values for processing in oeetltm.p for
       all components */
    if b-oeel.tallyfl = yes then do:

        for each oeelm use-index k-oeelm where
            oeelm.cono      = g-cono          and
            oeelm.ordertype = "O":U           and
            oeelm.orderno   = b-oeel.orderno  and
            oeelm.ordersuf  = b-oeel.ordersuf and
            oeelm.lineno    = b-oeel.lineno
        exclusive-lock:

            assign
                oeelm.prevprod    = oeelm.shipprod
                oeelm.prevqtyship = oeelm.stkqtyship
                oeelm.prevqtyord  = oeelm.stkqtyord
                oeelm.prevunit    = oeelm.unit
                oeelm.prevconv    = oeelm.conv.

        end. /* for each oeelm */

    end. /* if b-oeel.tallyfl = yes */

    assign
        lQtyChanged  = yes.
    if avail b-icsw and avail b-icsp then
      assign  
        v-resetstgfl = no
        g-prod       = b-icsp.prod
        s-notesfl    = b-icsp.notesfl
        s-proddesc   = b-icsp.descrip[1] + " " + b-icsp.descrip[2]
        v-qtyonhand  = if can-do("b,m":U,b-icsp.kittype) then 0
                       else if b-icsp.statustype = "l":U then 0
                       else b-icsw.qtyonhand
        v-qtycommit  = if can-do("b,m":U,b-icsp.kittype) then 0
                       else if b-icsp.statustype = "l":U then 0
                       else b-icsw.qtycommit
        v-qtyreservd = if can-do("b,m":U,b-icsp.kittype) then 0
                       else if b-icsp.statustype = "l":U then 0
                       else b-icsw.qtyreservd
        v-qtyonorder = if can-do("b,m":U,b-icsp.kittype) then 0
                       else if b-icsp.statustype = "l":U then 0
                       else b-icsw.qtyonorder
        v-qtybo      = if can-do("b,m":U,b-icsp.kittype) then 0
                       else if b-icsp.statustype = "l":U then 0
                       else b-icsw.qtybo
        v-idicsp     = recid(b-icsp)
        v-idicsw1    = recid(b-icsw)
        v-idicsw2    = recid(b-icsw).

    {a-oeeslv.i &b = "b-"}

    /*d Assign other variables needed for oeeslu.p */
  
    assign
        v-delayresrvfl = lHoldDelayFl
        v-conv         = if b-oeel.specnstype = "n":U then 1
                         else {unitconv.gas &decconv = b-oeel.unitconv}
        v-conv2        = if v-conv < 1 then 1 else v-conv
        v-stkqtyship   = if (avail b-icsp and b-icsp.kittype = "b":U) or
                             b-oeel.kitfl then
                             if v-delayresrvfl then 0
                             else min(b-oeel.stkqtyord,
                                      truncate(v-buildqty / v-conv,0))
                         else if avail b-icsp and b-icsp.kittype = "m":U then
                            b-oeel.stkqtyord
                         else
                             if s-netavail < 0 then 0
                         else
                             min(b-oeel.stkqtyord,s-netavail)
        s-qtyship      = v-stkqtyship / v-conv
        s-netavail     = s-netavail / v-conv
        s-netavail     = truncate(s-netavail,2)
        s-onorder      = v-qtyonorder / v-conv
        s-backorder    = v-qtybo / v-conv
        s-returnty     = ""
        g-orderno      = b-oeeh.orderno
        g-ordersuf     = b-oeeh.ordersuf
        g-oetype       = b-oeeh.transtype
        g-custno       = b-oeeh.custno
        s-origsuf      = g-ordersuf
        s-custpo       = b-oeeh.custpo
        g-shipto       = b-oeeh.shipto
        v-display      = no
        v-qtychg       = yes
        v-idoeeh       = recid(b-oeeh)
        v-retotalfl    = yes
        v-nosnlots     = if avail b-icsw and can-do("s,l":U,b-icsw.serlottype) 
                           then v-stkqtyship
                         else 0
        s-chrgqty      = b-oeel.chrgqty.

    /*d Set special price/cost data */
    run chngspc.p(input b-oeel.icspecrecno,
                  input b-oeel.shipprod,
                  input if b-oeel.specnstype = "n":U then yes
                      else no,
                  output v-csunperstk,
                  output v-icspecrecno,
                  output v-prccostper,
                  output v-specconv,
                  output v-speccostty,
                  output vc-csunperstk,
                  output vc-icspecrecno,
                  output vc-prccostper,
                  output vc-specconv,
                  output vc-speccostty,
                  output v-spcconvertfl).
    /*d Update the files */
    assign v-display = false.
    run oeeslu.p.

/*   end. /* kitfl or tallyfl */ */
end. /* for each b-oeel */

    /*d If any of the delayed lines/components have beed reserved we need
        to force a retotal of the header and run the backorder logic */
if lQtyChanged then do:
  assign
    v-display   = no
    v-totlinefl = yes
    g-ourproc   = "oeebu":U.

        /*d Reset order totals based on new qtys on line */
    run oeetbot.p.
/* don't know
      /* Create/Adjust Any Back Orders */
    run oeetbo.p.
*/
    assign
      g-ourproc   = "oeet":U
      v-totlinefl = no.
end. /* if lQtyChanged */


/*d Reset the recid and globals for OEEH to the current order */
assign
    v-idoeeh   = o-idoeeh
    g-orderno  = o-orderno
    g-ordersuf = o-ordersuf
    s-maint    = o-maint
    g-custno   = o-custno
    g-oetype   = o-oetype
    g-shipto   = o-shipto
    v-idicsp   = o-idicsp
    v-idicsw1  = o-idicsw1
    v-idicsw2  = o-idicsw2
    v-prodcost = o-svcost.
