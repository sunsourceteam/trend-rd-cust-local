/* ------------------------------------------------------------------------
   Program id  :  p-icizp-sel.i
   Author      :  tah
   Create Date :  03/12/14
-------------------------------------------------------------------------- */

(if {&type} = "f" then
  ({&b}icsw.qtyonorder -
   ({&b}icsw.qtybo + {&b}icsw.qtydemand))        
 else
 if {&type} = "a" then
   ({&b}icsw.qtyonhand - {&b}icsw.qtycommit - {&b}icsw.qtyreserv)
 else
 if {&type} = "o" then
   ({&b}icsw.qtyonhand)
 else
   ({&b}icsw.qtyonhand - {&b}icsw.qtycommit - {&b}icsw.qtyreserv) )
