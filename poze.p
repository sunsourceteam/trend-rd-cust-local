/*  poze.p - Expedite Report
    Proactive report to list products that are in danger of stock out
    before they can be replenished.
    - list only if unrecvd po/wt exists.
    - list only if avail cannot cover (demand - cushion) before next po/wt.
    - summary list of po's to expedite w/ name & phone
    10/jun/98 gb - add oe# for tied lines, non stocks if oe promisedt <
        po due date.
    15/jun/98 gb - make whse not required, add option for buyer, fix tie disp
    26/jun/98 gb - show inside rep name & promisedt for tied orders; page
        break before nonstocks; remove source column
    28/sep/98 gb - create form for fax to vendor, page break on vendor.
        mark poel.user8 with date reported, don't redisplay unless date
        is blank or at least 7 days old.
    *** vendor form moved to pozc ****
    7/oct/98 gb - add code for products not on order.    
    10/oct/98 gb- add option for # days not on order; promisedt.
    8/feb/99 gb - add region range
*/

{p-rptbeg.i}

    /** input variables **/

define buffer zz-icsd for icsd.

def var b-vend  like apsv.vendno        no-undo.
def var e-vend  like apsv.vendno        no-undo.
def var b-whse  as c  format "x(4)"     no-undo.
def var e-whse  as c  format "x(4)"     no-undo.
def var b-regn  as c  format "x(4)"     no-undo.
def var e-regn  as c  format "x(4)"     no-undo.
def var b-buyr  as c  format "x(4)"     no-undo.
def var e-buyr  as c  format "x(4)"     no-undo.
def var i-cush  as de format ">>9"      no-undo.  /* cushion days */
def var i-sort  as c  format "x"        no-undo.
def var i-incl  as lo                   no-undo.  /* incl prods not on ord */
def var i-only  as lo                   no-undo.  /* only prods not on ord */
def var i-days  as i  format ">>9"      no-undo.  /* # days not on ord */
def var n-vend  like icsw.arpvendno     no-undo.
def var n-line  like icsw.prodline      no-undo.
def var n-arp   as c  format "x"        no-undo.

    /** display variables **/
def var n-avail as de format ">>>>>9-"  no-undo.
def var n-bo    as de format ">>>>>9-"  no-undo.
def var d-avail as de format ">>>9-"    no-undo.  /* days avail */
def var d-next  as de format ">>>9-"    no-undo.  /* days to next rcpt */
def var n-powt  as c  format "x(10)"    no-undo.  /* po/wt # */
def var n-ddt   as da format "99/99/99" no-undo.  /* due dt */
def var n-qty   as de format ">>>>>9"   no-undo.  /* qty due */
def var n-rep   as c  format "x(12)"    no-undo.  /* inside rep */
def var n-pmdt  as da format "99/99/99" no-undo.  /* oe promise dt */
def var n-oe    as c  format "x(10)"    no-undo.  /* oe # */
def var n-name  as c  format "x(30)"    no-undo.
def var n-phone like  arsc.phoneno      no-undo.
def var n-lt    as de format ">>9"      no-undo.
def var n-stg   as i  format "9"        no-undo.
def var v-usg   as de format ">>>>>9.9999-" no-undo.
def var n-tie   as c  format "x(10)"    no-undo.
def var n-dt    as da format "99/99/99" no-undo.
def var n-list  as lo                   no-undo.
def var t-ord   as i  format ">>>>>>9"  no-undo.
def var t-suf   as i  format "99"       no-undo.
def var t-line  as i  format ">>>9"     no-undo.
def var b-nm    as c  format "x(24)"    no-undo.
def var v-phn   like apsv.phoneno       no-undo.
def var lv-tkr  as character format "x(4)" no-undo.
def var lv-rep  as character format "x(4)" no-undo.



form
    skip(1)
    apsv.vendno     label "Name"        space(2)
    apsv.name       no-label            space(5)
    apsv.expednm    label "Expedite"    space(3)
    apsv.exphoneno  no-label
    skip(1)
with frame f-vend side-labels no-box width 132.
form 
    "Days"      at 50
    "Days to"   at 55
    skip
    "Whse"      at 1
    "Product"   at 6
    "Avl"       at 32
    "BO"        at 40
    "LT"        at 46
    " Avl"      at 50
    " Rcpt"     at 56
    "PO/WT"     at 65
    "Stg"       at 73
    "Due Dt"    at 79
    "Due Qty"   at 89
    "Tie"       at 97
    "Promise"   at 108
    "Rep"       at 117
    skip
    "--------------------------------------------------" at 2
    "--------------------------------------------------------" at 52
with frame f-lbl no-box width 132.

form
    icsw.whse   at 1
    icsw.prod   at 6 format "x(22)"
    n-avail     at 29
    n-bo        at 36
    n-lt        at 45
    d-avail     at 49
    d-next      at 55
    n-powt      at 64
    n-stg       at 74
    n-ddt       at 78
    n-qty       at 88
    n-tie       at 97
    n-pmdt      at 108
    n-rep       at 117
with frame f-prod down no-box no-labels width 132.
form
    d-next      at 55
    n-powt      at 64
    n-stg       at 74
    n-ddt       at 78
    n-qty       at 88
    n-tie       at 97
    n-pmdt      at 108
    n-rep       at 117
with frame f-src down no-box no-labels width 132.
form
    "--------------------------------------------------" at 2
    "--------------------------------------------------------" at 52
with frame f-line no-box no-labels width 132.

form header
    "Vend"    at 1
    "Line"    at 14
    "Arp"     at 19
    "NS"      at 23
    "Product" at 26
    "Ty"      at 51
    "Order"   at 54
    "Ln"      at 65
    "Sq"      at 70
    "Hld"     at 73
    "Appr/Ent" at 77
    "PromDt"  at 86
    "Tkn"     at 96
    "Qty"     at 104
    "Cost"    at 111
    "Buyer"   at 117
    "Whse"    at 123
    "SRep"    at 128
with frame f-hd2 width 132 page-top /* SX 55 */ no-box.
form
    report.de12d0   at 1
    report.c12      at 14 format "x(6)"
    report.c13      at 21 format "x"
    report.c2-2     at 23 format "x"
    report.c24      at 26
    report.c2-3     at 51
    report.i7       at 54
    space(0) "-" space(0)
    report.i2   format "99"       
    report.i4       at 65 format ">>9"
    report.i3       at 69
    report.c1       at 74
    report.dt-2     at 77
    report.dt       at 86
    lv-tkr          at 95 format "x(4)"
    report.de9d2s   at 100 format ">>>>>>9"
    report.de12d5-2 at 108 format ">>>>9.999"
    report.outputty at 118 format "x(4)"
    report.c2       at 123 format "x(4)"
    lv-rep          at 128 format "x(4)"
with frame f-noord down width 132 no-labels /* SX 55 */ no-box.
    

assign b-vend = dec(sapb.rangebeg[1])
       e-vend = dec(sapb.rangeend[1])
       b-whse = sapb.rangebeg[2]
       e-whse = sapb.rangeend[2]
       b-buyr = sapb.rangebeg[3]
       e-buyr = sapb.rangeend[3]
       b-regn = sapb.rangebeg[4]
       e-regn = sapb.rangeend[4]
       i-cush = dec(sapb.optvalue[1])
       i-sort = sapb.optvalue[2]   /* sort by po or prod */
       i-incl = if sapb.optvalue[3] = "yes" then true else false
       i-only = if sapb.optvalue[4] = "yes" then true else false
       i-days = int(sapb.optvalue[5]).

if i-only = false then do:
for each icsl where icsl.cono = g-cono 
    and (icsl.whse >= b-whse and icsl.whse <= e-whse)
    and (icsl.vendno >= b-vend and icsl.vendno <= e-vend)
    and (icsl.buyer >= b-buyr and icsl.buyer <= e-buyr) no-lock,
    /*
  first icsd where icsd.cono = g-cono 
    and (icsd.region >= b-regn and icsd.region <= e-regn) no-lock,
    */
  each icsw use-index k-vendor where icsw.cono = g-cono
    and icsw.whse = icsl.whse
    and icsw.arpvendno = icsl.vendno
    and icsw.qtyonorder > 0 
    no-lock break by icsl.buyer by icsl.vendno by icsl.whse by icsw.prod:

    if first-of(icsl.vendno) then do:
        find apsv where apsv.cono = g-cono and apsv.vendno = icsl.vendno
            no-lock no-error.
        if not avail apsv then next.
        display apsv.vendno apsv.name apsv.expednm apsv.exphoneno
            with frame f-vend.
        display with frame f-lbl.
       /* display with frame f-line. */
    end.
    assign v-usg    = if icsw.usagerate = 0 then 0    /* # sold per day */
                      else (icsw.usagerate / 28)
           n-avail  = icsw.qtyonhand - icsw.qtyreservd - icsw.qtycommit
           d-avail  = if n-avail = 0 then 0           /* # days avail */
                      else if v-usg = 0 then 0
                      else (n-avail / v-usg)
           d-avail  = d-avail - i-cush                /* subtr cushion days */
           n-bo     = icsw.qtybo
           n-lt     = icsw.leadtmavg
           t-ord    = 0
           t-suf    = 0
           t-line   = 0
           n-rep    = ""
           n-pmdt   = ?.

/* don't have enough to cover until next rcpt */
    if (d-avail - icsw.leadtmavg)  <= 0 then do:

        for each poel use-index k-type where poel.cono = g-cono
            and poel.statustype = "A" and poel.transtype = "PO"
            and poel.whse = icsw.whse and poel.shipprod = icsw.prod
        no-lock,
        first poeh use-index k-poeh
           where poeh.cono = g-cono and poeh.pono = poel.pono
            and poeh.posuf = poel.posuf and poeh.stagecd > 0
            and poeh.stagecd < 5
            no-lock break by poel.shipprod by poel.duedt:

            assign
            n-powt = string(string(poel.pono) + "-" + string(poel.posuf))
            n-ddt  = poel.duedt
            n-qty  = poel.qtyord
            n-stg  = poeh.stagecd
            d-next = poel.duedt - today.

            find first poelo use-index k-poelo where poelo.cono = g-cono
                and poelo.pono = poel.pono and poelo.posuf = poel.posuf
            no-lock no-error.
            assign n-tie = if avail poelo then string(poelo.orderaltno) +
                              "-" + string(poelo.orderaltsuf,"99")
                           else ""
                   t-ord = if avail poelo then poelo.orderaltno else 0
                   t-suf = if avail poelo then poelo.orderaltsuf else 0
                   t-line = if avail poelo then poelo.linealtno else 0.

   /* find tied orders to get rep & promisedt */
            if t-ord > 0 then do:
                find oeeh where oeeh.cono = g-cono and oeeh.orderno = t-ord
                    and oeeh.ordersuf = t-suf no-lock no-error.
                assign n-rep = if avail oeeh then oeeh.slsrepin else ""
                       n-pmdt = if avail oeeh then oeeh.promisedt else ?.
                if n-rep <> "" then do:
                    find smsn where smsn.cono = g-cono
                        and smsn.slsrep = n-rep no-lock no-error.
                        n-rep = if avail smsn then smsn.name else n-rep.
                end.        
            end.
                       
  /* build summary list of po's to expedite for this vendor */
            find first report use-index k-cust where report.cono = g-cono
                and report.oper2 = g-operinit
                and report.reportnm = sapb.reportnm
                and report.de12d0 = icsw.arpvendno
                and report.outputty = "T"
                and report.c12 = n-powt
            exclusive-lock no-error.
            if not avail report then do:
                create report.
                assign
                report.cono = g-cono
                report.oper2 = g-operinit
                report.reportnm = sapb.reportnm
                report.de12d0 = icsw.arpvendno
                report.c12 = n-powt
                report.outputty = "T"
                report.dt = poeh.duedt.
            end.
            if poeh.vendno <> icsw.arpvendno then do:
                find apsv where apsv.cono = g-cono
                    and apsv.vendno = poeh.vendno no-lock no-error.
                if not avail apsv then next.
                else assign report.c13 = string(poeh.vendno)
                            report.c15 = apsv.lookupnm
                            report.c20 = apsv.expednm
                            report.c20-2 = apsv.exphoneno.
            end.

        if first-of(poel.shipprod) then do:
            display icsw.whse icsw.prod n-avail n-bo n-lt d-avail d-next
                n-powt n-stg n-ddt n-qty
                n-tie n-pmdt n-rep with frame f-prod down.
            down with frame f-prod.
        end.
        else display d-next n-powt n-stg n-ddt n-qty n-tie n-pmdt n-rep
            with frame f-src down.
        down with frame f-src.
      end.
    end.

    if last-of(icsl.vendno) then do:
        page.
        display with frame f-line.
        display skip(1) "Non Stocks" skip (1)
            with no-box no-labels frame a1 width 132.

        ns:
        for each oeel use-index k-specns where oeel.cono = g-cono
            and oeel.statustype = "A" and oeel.specnstype = "N"
            and oeel.arpvendno = icsl.vendno
            and oeel.qtyship < oeel.qtyord
            and oeel.invoicedt = ?
            and oeel.ordertype <> "F"
            no-lock,
          first oeeh where oeeh.cono = g-cono and oeeh.orderno = oeel.orderno
            and oeeh.ordersuf = oeel.ordersuf and oeeh.stagecd = 1
            no-lock,
          first smsn where smsn.cono = g-cono and smsn.slsrep = oeeh.slsrepin
            no-lock:
            n-list = false.

            if oeel.ordertype = "P" then do:
                find first poelo use-index k-order where poelo.cono = g-cono
                    and poelo.ordertype = "O" 
                    and poelo.orderaltno = oeel.orderno
                    and poelo.orderaltsuf = oeel.ordersuf
                    and poelo.linealtno = oeel.lineno
                no-lock no-error.
                if not avail poelo then 
                    assign n-list = true n-dt = ? n-powt = "Bad Tie".
                else do:
                    find poeh where poeh.cono = g-cono 
                        and poeh.pono = poelo.pono and poeh.posuf = poelo.posuf
                        and poeh.stagecd < 5 no-lock no-error.
                    if not avail poeh then next ns.  /* already received */    
                    else if poeh.duedt > oeeh.promisedt then 
                        assign n-list = true
                               n-dt   = poeh.duedt
                               n-powt = string(poeh.pono) + "-" +
                                        string(poeh.posuf,"99").
                end.
            end.
            else if oeel.ordertype = "T" then do:
                find wtelo use-index k-order where wtelo.cono = g-cono
                    and wtelo.ordertype = "O" 
                    and wtelo.orderaltno = oeel.orderno
                    and wtelo.orderaltsuf = oeel.ordersuf
                    and wtelo.linealtno = oeel.lineno
                no-lock no-error.
                if not avail wtelo then 
                    assign n-list = true n-dt = ? n-powt = "Bad Tie".
                else do:
                    find wteh where wteh.cono = g-cono 
                        and wteh.wtno = wtelo.wtno and wteh.wtsuf = wtelo.wtsuf
                        and wteh.stagecd < 5 no-lock no-error.
                    if not avail wteh then next ns.  /* already received */    
                    else if wteh.duedt > oeeh.promisedt then 
                        assign n-list = true
                               n-dt   = wteh.duedt
                               n-powt = string(wteh.wtno) + "-" +
                                        string(wteh.wtsuf,"99").
                end.
            end.
            else assign n-powt = "No Tie"
                        n-list = true
                        n-dt = ?.

            if n-list = true then do:
                display 
                    oeeh.orderno label "OE #" at 1 
                    space(0) "-" space(0)
                    oeeh.ordersuf no-label 
                    oeel.lineno label "Ln" at 12
                    oeel.shipprod label "Product" at 18
                    oeeh.promisedt label "Promise" at 45
                    smsn.name format "x(20)" label "Rep" at 55
                    n-powt label "PO/WT" at 76
                    n-dt label "Due"  at 88
                    with frame f-non down width 132.
                down with frame f-non.
                                  
    /* build summary list of po's to expedite for this vendor */
                find first report use-index k-cust where report.cono = g-cono
                    and report.oper2 = g-operinit
                    and report.reportnm = sapb.reportnm
                    and report.de12d0 = oeel.arpvendno
                    and report.outputty = "T"
                    and report.c12 = n-powt
                exclusive-lock no-error.
                if not avail report then do:
                    create report.
                    assign
                    report.cono = g-cono
                    report.oper2 = g-operinit
                    report.reportnm = sapb.reportnm
                    report.de12d0 = oeel.arpvendno
                    report.c12 = n-powt
                    report.outputty = "T"
                    report.dt = n-dt.
                end.
                
            end.                    
        end.    

        display with frame f-line.
        display skip(2) "Summary of PO's to Expedite" skip(1)
            with no-box no-labels frame a width 132.
        for each report use-index k-cust where report.cono = g-cono
            and report.oper2 = g-operinit
            and report.reportnm = sapb.reportnm
            and report.outputty = "T"
            and report.de12d0 = icsw.arpvendno
            exclusive-lock break by report.c15 by report.dt:

            display report.c12      label "PO #"
                    report.dt       label "Due Dt"
                    report.c15      label "Vend"
                    report.c20      label "Expedite"
                    report.c20-2    no-label
            with frame b width 132 down.
            down with frame b.

            delete report validate(true,"").
        end.
        page.
    end.
end.
end.  /* list prods in danger of stockout */

if i-incl = true then do:  /** show products not on order **/
view frame f-hd2.
page.

order:
for each zz-icsd where zz-icsd.cono = g-cono
      and (zz-icsd.whse >= b-whse and zz-icsd.whse <= e-whse) no-lock,
      
    each oeel use-index k-fill where oeel.cono = g-cono
    and oeel.statustype = "A"
    and oeel.invoicedt = ? 
   /*  and (oeel.whse >= b-whse and oeel.whse <= e-whse) */
    and oeel.whse = zz-icsd.whse
    and oeel.specnstype <> "L"
    and can-do("SO,CS,DO",oeel.transtype)
    and oeel.qtyord > oeel.qtyship 
    and oeel.botype <> "N"
    and oeel.ordertype <> "f"
    no-lock,
  first oeeh where oeeh.cono = g-cono and oeeh.orderno = oeel.orderno
    and oeeh.ordersuf = oeel.ordersuf and oeeh.stagecd = 1
    no-lock:
    
    find oeehc use-index k-oeehc where oeehc.cono = g-cono 
        and oeehc.orderno = oeeh.orderno and oeehc.ordersuf = oeeh.ordersuf
    no-lock no-error.
    assign n-dt = if avail oeehc then oeehc.approvedt else oeel.enterdt.

    /* skip lines not X days old */
    if n-dt > (today - i-days) then next order.

    if oeel.kitfl = true then do:
        kit:
        for each oeelk use-index k-oeelk where oeelk.cono = g-cono
            and oeelk.ordertype = "O" and oeelk.orderno = oeel.orderno
            and oeelk.ordersuf = oeel.ordersuf 
            and oeelk.lineno = oeel.lineno
            and oeelk.comptype = "C"
            and oeelk.qtyord > oeelk.qtyreservd
            and oeelk.orderaltno = 0
        no-lock:
            assign n-vend = oeelk.arpvendno
                   n-line = oeelk.arpprodline.

            if oeelk.specnstype = "N" then do:
                find first poelo use-index k-order 
                    where poelo.cono = g-cono
                    and poelo.ordertype = "O" 
                    and poelo.orderaltno = oeelk.orderno
                    and poelo.orderaltsuf = oeelk.ordersuf
                    and poelo.linealtno = oeelk.lineno
                    and poelo.seqaltno = oeelk.seqno
                no-lock no-error.
                if avail poelo then next kit.
                else do:
                    find first wtelo use-index k-order 
                        where wtelo.cono = g-cono
                        and wtelo.ordertype = "O" 
                        and wtelo.orderaltno = oeelk.orderno
                        and wtelo.orderaltsuf = oeelk.ordersuf
                        and wtelo.linealtno = oeelk.lineno
                        and wtelo.seqaltno = oeelk.seqno
                    no-lock no-error.
                    if avail wtelo then next kit.
                end.
            end.
            else do:  /* stock & specials */
                find icsp where icsp.cono = g-cono 
                    and icsp.prod = oeelk.shipprod 
                    and icsp.statustype <> "L" no-lock no-error.
                if not avail icsp then next kit.
                find icsw use-index k-icsw where icsw.cono = g-cono
                    and icsw.prod = oeelk.shipprod
                    and icsw.whse = oeelk.whse no-lock no-error.
                if not avail icsw then next kit.
                if oeelk.specnstype = "" and (icsw.qtyonhand - 
                    icsw.qtyreservd - icsw.qtycommit - icsw.qtybo +
                    icsw.qtyonorder) >= 0
                    then next kit.
                    
                else do:
                    find first poelo use-index k-order 
                        where poelo.cono = g-cono
                        and poelo.ordertype = "O" 
                        and poelo.orderaltno = oeelk.orderno
                        and poelo.orderaltsuf = oeelk.ordersuf
                        and poelo.linealtno = oeelk.lineno
                        and poelo.seqaltno = oeelk.seqno
                    no-lock no-error.
                    if avail poelo then next kit.
                    else do:
                        find first wtelo use-index k-order 
                            where wtelo.cono = g-cono
                            and wtelo.ordertype = "O" 
                            and wtelo.orderaltno = oeelk.orderno
                            and wtelo.orderaltsuf = oeelk.ordersuf
                            and wtelo.linealtno = oeelk.lineno
                            and wtelo.seqaltno = oeelk.seqno
                        no-lock no-error.
                        if avail wtelo then next kit.
                    end.
                end.
                
                assign 
                    n-arp  = icsw.arptype 
                    n-vend = icsw.arpvendno
                    n-line = icsw.prodline.
            end.

            find first icsl where icsl.cono = g-cono 
                and icsl.whse = oeel.whse
                and icsl.vendno = n-vend
                and icsl.prodline = (if n-line = "" then icsl.prodline
                                     else n-line)
                and (icsl.buyer >= b-buyr and icsl.buyer <= e-buyr)
            no-lock no-error.

            find first report use-index k-c24 where report.cono = g-cono
                and report.oper2 = sapb.operinit
                and report.reportnm = sapb.reportnm
                and report.c24 = oeelk.shipprod
                and report.c2 = oeel.whse
                and report.i7 = oeelk.orderno
                and report.i2 = oeelk.ordersuf
                and report.i4 = oeelk.lineno
                and report.i3 = oeelk.seqno
            exclusive-lock no-error.
            if not avail report then do:
                create report.
                assign 
                report.cono = g-cono
                report.oper2 = sapb.operinit
                report.reportnm = sapb.reportnm
                report.c24 = oeelk.shipprod
                report.c2 = oeel.whse
                report.i7 = oeelk.orderno
                report.i2 = oeelk.ordersuf
                report.i4 = oeelk.lineno
                report.i3 = oeelk.seqno.
                assign
                report.c13 = n-arp
                report.de12d0 = n-vend
                report.c12 = n-line
                report.c4 = oeelk.arpwhse
                report.de9d2s = (oeelk.qtyord - oeelk.qtyreservd -
                                 oeelk.qtyship)
                report.de12d5-2 = oeelk.prodcost
                report.l = oeel.kitfl.
                assign
                report.dt = if oeel.promisedt <> ? then oeel.promisedt
                            else if oeel.reqshipdt <> ? then oeel.reqshipdt
                            else oeeh.promisedt
                report.dt-2 = n-dt  /* approve or enter date */
                report.c1 = oeeh.approvty
                substring(report.c6,1,4) = oeeh.takenby
                substring(report.c6,5,4) = oeel.slsrepout
                report.de12d5 = oeeh.custno
                report.c2-2 = ""
                report.c2-3 = oeeh.transtype
                report.outputty = if avail icsl then icsl.buyer else "".
            end.
        end.
    end.  /* kits */
    else do:
        assign n-vend = if oeel.arpvendno = 0 then oeel.vendno
                        else oeel.arpvendno         
               n-line = if oeel.arpprodline = "" then oeel.prodline
                        else oeel.arpprodline.
 
        if oeel.specnstype <> "N" then do:
            find icsp where icsp.cono = g-cono 
                and icsp.prod = oeel.shipprod 
                and icsp.statustype <> "L" no-lock no-error.
            if not avail icsp then next order.
            find icsw use-index k-icsw where icsw.cono = g-cono
                and icsw.prod = oeel.shipprod
                and icsw.whse = oeel.whse no-lock no-error.
            assign 
                n-arp  = if avail icsw then icsw.arptype else ""
                n-vend = if avail icsw then icsw.arpvendno
                         else n-vend
                n-line = if avail icsw then icsw.prodline
                         else n-line.
        end.
        
        find first icsl where icsl.cono = g-cono 
            and icsl.whse = oeel.whse
            and icsl.vendno = n-vend
            and icsl.prodline = (if n-line <> "" then n-line
                                 else icsl.prodline)
            and (icsl.buyer >= b-buyr and icsl.buyer <= e-buyr)
        no-lock no-error.

        if oeel.specnstype = "" then do:  /* stock prod - check if ordered */
            find icsw use-index k-icsw where icsw.cono = g-cono 
                and icsw.prod = oeel.shipprod and icsw.whse = oeel.whse
                and (icsw.qtyonhand - icsw.qtyreservd - icsw.qtycommit +
                     icsw.qtyonorder) >= icsw.qtybo no-lock no-error.
            if avail icsw then next order.
        end.
        else do:
            if oeel.ordertype = "P" then do:
                find first poelo use-index k-order where poelo.cono = g-cono
                    and poelo.ordertype = "O" 
                    and poelo.orderaltno = oeel.orderno
                    and poelo.orderaltsuf = oeel.ordersuf
                    and poelo.linealtno = oeel.lineno
                no-lock no-error.
                if avail poelo then next order.
            end.
            else if oeel.ordertype = "T" then do:
                find wtelo use-index k-order where wtelo.cono = g-cono
                    and wtelo.ordertype = "O" 
                    and wtelo.orderaltno = oeel.orderno
                    and wtelo.orderaltsuf = oeel.ordersuf
                    and wtelo.linealtno = oeel.lineno
                no-lock no-error.
                if avail wtelo then next order.
            end.
        end.

        find first report use-index k-c24 where report.cono = g-cono
            and report.oper2 = sapb.operinit
            and report.reportnm = sapb.reportnm
            and report.c24 = oeel.shipprod
            and report.c2 = oeel.whse
            and report.i7 = oeel.orderno
            and report.i2 = oeel.ordersuf
            and report.i4 = oeel.lineno
            and report.i3 = 0
        exclusive-lock no-error.
        if not avail report then do:
            create report.
            assign 
            report.cono = g-cono
            report.oper2 = sapb.operinit
            report.reportnm = sapb.reportnm
            report.c24 = oeel.shipprod
            report.c2 = oeel.whse
            report.i7 = oeel.orderno
            report.i2 = oeel.ordersuf
            report.i4 = oeel.lineno
            report.i3 = 0.
            assign
            report.c13 = n-arp
            report.de12d0 = n-vend
            report.c12 = n-line
            report.c4 = ""  /* no arpwhse on oeel */
            report.de9d2s = (oeel.qtyord - oeel.qtyship)
            report.de12d5-2 = oeel.prodcost
            report.l = oeel.kitfl.
            assign
            substring(report.c6,1,4) = oeeh.takenby
            substring(report.c6,5,4) = oeel.slsrepout
            report.dt-2 = n-dt
            report.dt = if oeel.promisedt <> ? then oeel.promisedt
                        else if oeel.reqshipdt <> ? then oeel.reqshipdt
                        else oeeh.promisedt
            report.c1 = oeeh.approvty
            report.de12d5 = oeeh.custno
            report.c2-2 = oeel.specnstype
            report.c2-3 = oeeh.transtype
            report.outputty = if avail icsl then icsl.buyer else "".
        end.
    end.
end.  /* lines not on order */

for each report use-index k-cust where report.cono = g-cono
    and report.oper2 = sapb.operinit
    and report.reportnm = sapb.reportnm
    exclusive-lock 
    break by report.outputty by report.de12d0 by report.c12 by report.c24:
    assign lv-tkr = substring(report.c6,1,4)
           lv-rep = substring(report.c6,5,6).
    display report.de12d0 report.c12 report.c13 report.c2-2 
        report.c24 report.c2-3 report.i7 report.i2 report.i4 report.i3 
        report.c1 report.dt-2 report.dt lv-tkr 
        report.de9d2s report.de12d5-2 report.outputty report.c2
        lv-rep
    with frame f-noord down.
    down with frame f-noord.
    
    delete report validate(true,"").
end.
end.


