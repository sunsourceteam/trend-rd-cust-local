/***************************************************************************
 icrxv.p
 This program produces a VA Inventory In Put Away Report
 modified from /home/njew/rep00117.p
 Marjorie Fialek   02/01/03

***************************************************************************/

{p-rptbeg.i} 

def var ln-whse-f           as c    format "x(4)"         no-undo.
def var ln-whse-t           as c    format "x(4)"         no-undo.
def var ln-receiptdt-f      as date no-undo.
def var ln-receiptdt-t      as date no-undo.
def var ln-vano-f           as de   format ">>>>>>9"      no-undo.
def var ln-vano-t           as de   format ">>>>>>9"      no-undo.
def var ln-seqno-f          as de   format ">>9"          no-undo. 
def var ln-seqno-t          as de   format ">>9"          no-undo.

def var w-whse           as c    format "x(4)"          no-undo.
def var w-receiptdt      as date no-undo.
def var w-vano           as i    format "zzzzzz9"       no-undo.
def var w-vasuf          as i    format "99"            no-undo.
def var w-stagecd        as i    format "9"             no-undo.
def var w-sctntype       as c    format "x(3)"          no-undo.
def var w-avgcost        as de   format ">>>>9.99"      no-undo.
def var w-prod           as c    format "x(24)"         no-undo.
def var w-stkqtyord      as de   format ">>>>9.99"      no-undo. 
def var w-stkqtyship     as de   format ">>>>9.99"      no-undo.
def var w-prodcost       as de   format ">>>>9.99"      no-undo.
def var w-binloc1        as c    format "xx/xx/xxx/xxx" no-undo.
def var w-status         as c    format "1"             no-undo.

Form
   "Whse"             at 1
   "Receipt dt."      at 7
   "VA #"             at 21
   "Suf"              at 28
   "Stg"              at 34
   "Sec"              at 40
   "Product"          at 45
   "Stkqtyprod"       at 70
   "Stkqtyship"       at 84
   "Prod Cost"        at 98
   "Bin Loc."         at 112
   "Status"           at 125
with frame f-header width 132 no-labels no-box page-top no-underline.

Form 
   w-whse         at 1       
   w-receiptdt    at 7     
   w-vano         at 19    
   w-vasuf        at 28      
   w-stagecd      at 34     
   w-sctntype     at 40      
   w-prod         at 45      
   w-stkqtyord    at 70      
   w-stkqtyship   at 83      
   w-prodcost     at 97     
   w-binloc1      at 112    
   w-status       at 125 
 with down frame f-detail width 132 no-labels no-box no-underline.

/* put control "~033~046~1541~117".    /* landscape format */ */

assign ln-whse-f        = sapb.rangebeg[1]
       ln-whse-t        = sapb.rangeend[1] 
       v-datein         = sapb.rangebeg[2].
       {p-rptdt.i}
assign ln-receiptdt-f   = v-dateout
       v-datein         = sapb.rangeend[2].
       {p-rptdt.i}
       assign ln-receiptdt-t = v-dateout.
       
assign ln-vano-f        = integer(substring(sapb.rangebeg[3],1,7))
       ln-vano-t        = integer(substring(sapb.rangeend[3],1,7)) 
       ln-seqno-f       = integer(substring(sapb.rangebeg[4],1,3))
       ln-seqno-t       = integer(substring(sapb.rangeend[4],1,3)).

/*
display sapb.rangebeg[1]
        sapb.rangeend[1]
        sapb.rangebeg[2]
        sapb.rangeend[2]
        sapb.rangebeg[3]
        sapb.rangeend[3]
        sapb.rangebeg[4]
        sapb.rangeend[4]
        ln-whse-f     
        ln-whse-t     
        ln-receiptdt-f
        ln-receiptdt-t
        ln-vano-f     
        ln-vano-t     
        ln-seqno-f    
        ln-seqno-t.   
*/        
       
View frame f-header.

if ln-vano-f <> 0 and ln-vano-t <> 0 then 
do:

    if ln-seqno-f = 0 then
    do:  
      message " Sequence # is required with VA # ".
      pause.
      return.
    end.
   
    if ln-seqno-t = 999 then
    do:  
      message " Sequence # is required with VA # ".
      pause.
      return.
    end.
    

for each vaeh use-index k-vaeh WHERE  vaeh.cono = 1 and
                                      vaeh.vano = int(ln-vano-f) and
                                      vaeh.vano = int(ln-vano-t) 
                                      no-lock: 
 
 
 for each vaesl use-index k-vaesl WHERE vaesl.cono  =  1 and
                                        vaesl.vano  = vaeh.vano and
                                        vaesl.seqno >= ln-seqno-f and
                                        vaesl.seqno <= ln-seqno-t and
                                        vaesl.sctntype = "ii" and
                                        vaesl.qtyship >= 1
                                        no-lock:

   for each icsw use-index k-whse WHERE icsw.cono = 1 and
                                        icsw.whse = vaesl.whse and
                                        icsw.prod = vaesl.shipprod 
                                        no-lock
                                        break
                                        by vaeh.whse
                                        by icsw.binloc1:
        

   assign w-whse         = vaesl.whse  
          w-receiptdt    = vaeh.receiptdt  
          w-vano         = vaeh.vano  
          w-vasuf        = vaeh.vasuf  
          w-stagecd      = vaeh.stagecd  
          w-sctntype     = vaesl.sctntype  
          w-prod         = icsw.prod 
          w-status       = icsw.statustype 
          w-stkqtyord    = vaesl.stkqtyord   
          w-stkqtyship   = vaesl.stkqtyship  
          w-prodcost     = vaesl.prodcost  
          w-binloc1      = icsw.binloc1.
            
   run va_print.         

end.
end.
end.
end.

else
do:

    if ln-receiptdt-f = 01/01/50 then
    do:  
      message "Receipt date is required with whse".
      pause.
      return.
    end.
    
    if ln-receiptdt-t = 12/31/49 then
    do:  
      message "Receipt date is required with whse".
      pause.
      return.
    end.
   

for each vaeh use-index k-vaeh WHERE vaeh.cono = 1 and
                                     vaeh.whse >= ln-whse-f and
                                     vaeh.whse <= ln-whse-t and
                                    (vaeh.stagecd >= 3 and
                                     vaeh.stagecd <= 7)
                                     no-lock: 

 for each vaes use-index k-vaes WHERE vaes.cono = 1 and
                                      vaes.vano = vaeh.vano and
                                      vaes.vasuf = vaeh.vasuf and
                                      vaes.sctntype = "ii" and
                                      vaes.completefl = yes
                                      no-lock:

 for each vaesl use-index k-vaesl WHERE vaesl.cono  =  1 and
                                        vaesl.vano  = vaes.vano and
                                        vaesl.vasuf = vaes.vasuf and
                                        vaesl.seqno = vaes.seqno and
                                        vaesl.shipprod <> "REBATE" and
                                        vaesl.qtyship > 0
                                        no-lock:
                                        
  for each sasj use-index k-inqd WHERE sasj.cono = 1 and
                                       sasj.jrnlno = vaes.jrnlno and
                                       sasj.opendt >= ln-receiptdt-f and
                                       sasj.opendt <= ln-receiptdt-t
                                       no-lock:
                                         
     
   for each icsw use-index k-whse WHERE icsw.cono = 1 and
                                        icsw.whse = vaeh.whse and
                                        icsw.prod = vaesl.shipprod 
                                        no-lock
                                        break
                                        by vaeh.whse
                                        by icsw.binloc1:
    
      assign w-whse      = vaeh.whse  
          w-receiptdt    = sasj.opendt  
          w-vano         = vaeh.vano  
          w-vasuf        = vaeh.vasuf  
          w-stagecd      = vaeh.stagecd  
          w-sctntype     = vaesl.sctntype  
          w-prod         = icsw.prod  
          w-status       = icsw.statustype
          w-stkqtyord    = vaesl.stkqtyord   
          w-stkqtyship   = vaesl.stkqtyship  
          w-prodcost     = vaesl.prodcost  
          w-binloc1      = icsw.binloc1.  

     run va_print.

end.
end.
end.
end.
end.
end.


procedure va_print:

     display
          w-whse        
          w-receiptdt    
          w-vano           
          w-vasuf          
          w-stagecd        
          w-sctntype       
          w-prod  
          w-stkqtyord       
          w-stkqtyship     
          w-prodcost       
          w-binloc1        
          w-status
       with frame f-detail.    
   down with frame f-detail.

end procedure.




