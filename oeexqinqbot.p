{g-all.i}
def     shared var v-quoteno         as integer format ">>>>>>>9"   no-undo.
def            var v-internalfl      as logical                     no-undo.
def            var j-inx             as integer                     no-undo.
def            var v-comments        as char format "x(60)"  
                                     view-as editor inner-chars 60
                                     inner-lines 6
                                     scrollbar-vertical.

def            var v-allfl           as logical    init no          no-undo.
def            var v-ackfl           as logical    init yes         no-undo.
def            var v-pckfl           as logical    init no          no-undo.
def            var v-advfl           as logical    init no          no-undo.
def            var v-invfl           as logical    init no          no-undo.

form
  v-comments at 13
  "INternal"     at column  1 row 1
  "Quote Notes:" at column  1 row 2
  with frame f-bot2i no-labels no-box overlay row 13.

form
  "On All:"       at 13
  v-allfl         at 21
  "On Ack:"       at 25
  v-ackfl         at 33 
  "On Pck:"       at 37
  v-pckfl         at 45
  "On Adv:"       at 49
  v-advfl         at 57
  "On Inv:"       at 61
  v-invfl         at 69
  v-comments at 13
  "EXternal"     at column  1 row 1 
  "Quote Notes:" at column  1 row 2
  with frame f-bot2e no-labels no-box overlay row 12.

 
assign v-internalfl = yes.

  /*
  on cursor-up tab.  
  on cursor-down cursor-down.
  */
  
  Bottom2Loop:
  
  do while true on endkey undo, leave Bottom2Loop:
  
    if v-internalfl then
      do:
      assign v-comments = "".
      for each notes where notes.cono = g-cono and
        notes.notestype = "ba"                 and
        notes.primarykey   = string(v-quoteno,">>>>>>>9") and
        notes.secondarykey = "I"
        no-lock:
        do j-inx = 1 to 16:
          assign v-comments =   v-comments  + 
                               (if notes.noteln[j-inx] <> "" then
                                   replace(notes.noteln[j-inx],chr(10),"") 
                                   + chr(10)  /* Editor block each line has a 
                                                line feed at the end   %TAH */
                                else
                                   "").
        end.                           
      end.
    
      display v-comments
        with frame f-bot2i.
        
      readkey.

      if keylabel(lastkey) = "PF4" then
        do:
        hide frame f-bot2i no-pause.
        hide frame f-bot2e no-pause.
        leave Bottom2Loop.
      end.
     
      if keylabel(lastkey) = "F10" then
        do:
        readkey pause 0.
        assign v-internalfl = no
               v-comments = "".
        hide frame f-bot2i no-pause.
        next Bottom2Loop.
      end.
    
      apply lastkey.
    end. /* internal */
    else
      do:
      assign v-comments = "".
      for each notes where notes.cono = g-cono and
        notes.notestype = "ba"                 and
        notes.primarykey   = string(v-quoteno,">>>>>>>9") and
        notes.secondarykey = "E"     
        no-lock:
        do j-inx = 1 to 16:
          assign v-comments =  v-comments + 
                               (if notes.noteln[j-inx] <> "" then
                                   replace(notes.noteln[j-inx],chr(10),"") 
                                   + chr(10)  /* Editor block each line has a 
                                                line feed at the end   %TAH */
                                else
                                   "").
        end.    
        
        assign v-allfl =  notes.printfl 
               v-ackfl =  notes.printfl2
               v-pckfl =  notes.printfl3
               v-advfl =  notes.printfl4
               v-invfl =  notes.printfl5.
      end.
      display  v-comments
               v-allfl 
               v-ackfl
               v-pckfl
               v-advfl 
               v-invfl
               with frame f-bot2e.
      readkey.
        
      if keylabel(lastkey) = "PF4" then
        do:
        hide frame f-bot2i no-pause.
        hide frame f-bot2e no-pause.
        leave Bottom2Loop.
      end.

      if frame-field = "v-allfl" and input v-allfl = yes then
        do:
        assign v-allfl = yes
               v-ackfl = yes
               v-pckfl = yes
               v-advfl = yes
               v-invfl = yes.
        display v-allfl 
                v-ackfl 
                v-pckfl 
                v-advfl 
                v-invfl 
                with frame f-bot2e.
      end.
      
      if keylabel(lastkey) = "F10" then
        do:
        readkey pause 0.
        assign v-internalfl = yes
               v-comments = ""
               v-allfl    = no
               v-ackfl    = yes
               v-pckfl    = no
               v-advfl    = no
               v-invfl    = no.
        hide frame f-bot2e.
        next Bottom2Loop.
      end.
      apply lastkey.  
    end. /* external */
  end.  /* Bottom2Loop */
  
hide frame f-bot2i   no-pause.
hide frame f-bot2e   no-pause.
return.
