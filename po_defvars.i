/******************************************************************************
 po_defvars_.i
 Include for PO lookups. To fill in the bottom of the screen with 
  additional PO information.
  
*****************************************************************************/

/* SDI custom loookup variables */
def buffer z-apsv for apsv.
def buffer z-poeh for poeh.

def {1} shared var bl-recid        as recid                            no-undo.
def {1} shared var a-hlddt         like poeh.reqshipdt                 no-undo.
def {1} shared var a-hldprod       like poel.shipprod                  no-undo.
def {1} shared var a-qtyallo       like poel.qtyrcv format ">>>>>9.99" no-undo.
def {1} shared var ws-qtyallo      like poel.qtyrcv format ">>>>>9.99" no-undo.
def {1} shared var zu-shipto        as char format "x(04)"       no-undo.
def {1} shared var zu-pono          like poeh.pono               no-undo.
def {1} shared var lu-pono          as char format "x(10)" dcolor 3 no-undo.
def {1} shared var lu-qtyrcv        like poel.qtyrcv             no-undo.
def {1} shared var lu-qtyord        like poel.qtyord             no-undo.
def {1} shared var lu-qtyrcvc       as char                      no-undo.
def {1} shared var lu-qtyordc       as char                      no-undo.
def {1} shared var lu-shipid        like poehb.shipmentid        no-undo.
def {1} shared var lu-lineno        like poel.lineno    dcolor 3 no-undo.
def {1} shared var lu-vendno        like apsv.vendno    dcolor 3 no-undo.
def {1} shared var lu-vendnoc       as char dcolor 3             no-undo.
def {1} shared var lu-name          as c format "x(30)" dcolor 3 no-undo.
def {1} shared var lu-expshipdt     like poeh.reqshipdt          no-undo.
def {1} shared var lu-restaddr      as c format "x(60)"          no-undo.
def {1} shared var lu-city          as c format "x(20)"          no-undo.
def {1} shared var lu-state         like arsc.state              no-undo.
def {1} shared var lu-repout        like arsc.slsrepout          no-undo.
def {1} shared var lu-repnm1        as c format "x(30)"          no-undo.
def {1} shared var lu-repnm2        as c format "x(30)"          no-undo.
def {1} shared var lu-buyer         like poeh.buyer              no-undo.
def {1} shared var lu-stagecd       as char format "xxxxx"       no-undo.
def {1} shared var lu-stagecd2 as c format "x(3)"   extent 9    
   init ["Ord", "Prt", "Ack", "Pre", "Rcv", "Cst", "Cls", "  ", "Can"].
def {1} shared var lu-transtype     like poeh.transtype          no-undo.
def {1} shared var lu-ordaltno      like poeh.orderaltno         no-undo.
def {1} shared var lu-ordaltsuf     like poeh.orderaltsuf        no-undo.
def {1} shared var lu-bl-shipdt     like poeh.reqshipdt          no-undo.
def {1} shared var lu-borelfl       as char format "xxx"         no-undo.


