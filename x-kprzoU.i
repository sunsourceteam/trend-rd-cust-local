&if defined(user_temptable) = 2 &then
def var p-detailprt                 as logical                  no-undo. 
def var oExport                     as logical                    no-undo.  
def  var p-regval                   as c                        no-undo. 
def var b-kitprod  like icsp.prod       no-undo.
def var e-kitprod  like icsp.prod       no-undo.
def var b-district like smsn.mgr        no-undo.
def var e-district like smsn.mgr        no-undo.
def var b-salesout like oeeh.slsrepout  no-undo.
def var e-salesout like oeeh.slsrepout  no-undo.
def var b-salesin  like oeeh.slsrepin   no-undo.
def var e-salesin  like oeeh.slsrepin   no-undo.
def var b-takenby  like oeeh.takenby    no-undo.
def var e-takenby  like oeeh.takenby    no-undo.
def var b-cust     like oeeh.custno     no-undo.
def var e-cust     like oeeh.custno     no-undo.
def var b-createddt as date format "99/99/99"    no-undo.
def var e-createddt as date format "99/99/99"    no-undo.
/* def var p-export   as char format "x(24)" no-undo.  */
def var p-format   as int                 no-undo.
def var v-mgn      as decimal  no-undo.
def var v-mgn2     as decimal  no-undo.
define temp-table kpztemp no-undo
  field mgr       like smsn.mgr
  field name      like arsc.name
  field takenby   like oeeh.takenby
  field orderno   like oeeh.orderno
  field ordersuf  like oeeh.ordersuf
  field lineno    like oeel.lineno
  field prod      like icsp.prod
  field qtyord    like oeel.qtyord
  field price     like oeel.price
  field prodcost  like oeel.prodcost
  field margin1   as dec
  field wstndcost like zsdikpz.wstndcost
  field margin2   as dec
  field slsrepin  like oeeh.slsrepin
  field sortdtl   as char format "x(36)"
  field usedwhse  like icsw.whse
  field createddt as date format "99/99/99"
index k-prod
      prod
      orderno.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_drpttable) = 2 &then
/* Use to add fields to the report table */
   field kpztempid as recid
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_forms) = 2 &then
form 
"Dist"            at 1 
"Cust Name"       at 6             
"Takenby"         at 36            
"Order#"          at 44
"Suf"             at 51           
"Ln#"             at 57            
"Product"         at 66            
"Qty"             at 94            
"Curr Price"      at 103 
"Curr Cost"       at 115                                        
"Curr Mgn%"       at 127           
"New Cost"        at 145           
"New Mgn%"        at 159          
"RepIn"           at 36                                        
with down frame f-titles
no-box no-labels width 178.
form 
kpztemp.mgr          at 1       label "Dist"
kpztemp.name         at 6       label "Cust Name"      
kpztemp.takenby      at 36      label "Taken"      
kpztemp.orderno      at 42      label "Order#"      
kpztemp.ordersuf     at 52      label "Suf"
kpztemp.lineno       at 56      label "Ln#"      
kpztemp.prod         at 66      label "Product"      
kpztemp.qtyord       at 90      label "Qty"      
kpztemp.price        at 100     label "Curr Price"       
kpztemp.prodcost     at 113     label "Curr Cost"
kpztemp.margin1      at 127     label "Curr Mgn%"        
kpztemp.wstndcost    at 142     label "New Cost"      
kpztemp.margin2      at 157     label "New Mgn%" 
kpztemp.usedwhse     at 170     label "UsedWhse"
kpztemp.createddt    at 170     label "Roll Dt"       
kpztemp.slsrepin     at 36      label "RepIn"
with down frame f-orderdtl
no-box no-labels width 178.   
form
  v-final                at 1
  v-astrik               at 2   format "x(5)"
  v-summary-lit2         at 7   format "x(27)"
  v-amt1           at 34  format ">>>,>>>,>>9-"
  v-amt2           at 47  format ">>>,>>>,>>9-"
  v-amt3           at 60  format ">>>,>>>,>>9-"
  v-amt4           at 73  format ">>>,>>>,>>9-" 
  v-amt5           at 86  format ">>>,>>>,>>9-"
  v-amt6           at 99  format ">>>,>>>,>>9-"
  "~015"           at 178
with frame f-tot width 178
 no-box no-labels.  
form 
  "~015"           at 1
with frame f-skip width 178 no-box no-labels.  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_extractrun) = 2 &then
 run sapb_vars.
 run extract_data.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_exportstatheaders) = 2 &then
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  
  put stream expt unformatted " " + 
  v-del + v-del + v-del + v-del + v-del + v-del + v-del +  
  v-del                                                    
  v-del                                                    
  v-del                                                    
  v-del                                                    
  v-del                                                    
  chr(13).                                                 
  assign export_rec = 
         export_rec      +
      /* " "             + v-del +   */
         "Dist"          + v-del +
         "Cust Name"     + v-del +
         "Taken"         + v-del +
         "RepIn"         + v-del +
         "Order#"        + v-del +
         "Suf"           + v-del +
         "Ln#"           + v-del +
         "Product"       + v-del +
         "Qty"           + v-del +
         "Curr Price"    + v-del +
         "Curr Cost"     + v-del +
         "Curr Mgn%"     + v-del +
         "New Cost"      + v-del +
         "New Mgn%"      + v-del +
         "UsedWhse"      + v-del +
         "Roll Dt".       
                   
  
/*     assign export_rec = export_rec + " ".      */
                      " ".
                      
  if p-detailprt then 
     assign export_rec = export_rec + v-del.                 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_foreach) = 2 &then
for each kpztemp no-lock:
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_drptassign) = 2 &then
 /* If you add fields to the dprt table this is where you assign them */
 drpt.kpztempid = recid(kpztemp)
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_viewheadframes) = 2 &then
/* Since frames are unique to each program you need to put your
   views and hides here with your names */
if not p-exportl then
  do:
  if not x-stream then
    do:
    hide frame f-trn1.
    hide frame f-trn2.
    hide frame f-trn3.
    hide frame f-trn4.
    hide frame f-trn5.
    end.
  else
   do:
    hide stream xpcd frame f-trn1.
    hide stream xpcd frame fx-trn2.
    hide stream xpcd frame f-trn3.
    hide stream xpcd frame f-trn4.
    hide stream xpcd frame f-trn5.
    end.
  end.
if not p-exportl  then
  do:
  if not x-stream then
    do:
    page.
    view frame f-trn1.
    view frame f-trn2.
    view frame f-trn3.
    end.
  else  
    do:
    page stream xpcd.
    view stream xpcd frame f-trn1.
    view stream xpcd frame fx-trn2.
    view stream xpcd frame f-trn3.
    end.
   
  end.
  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_drptloop) = 2 &then
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_detailprint) = 2 &then
   /* Comment this out if you are not using detail 'X' option  */
   
   if p-detailprt and 
      entry(u-entitys,drpt.sortmystery,v-delim) <> 
      fill("~~",length(entry(u-entitys,drpt.sortmystery,v-delim))) then 
     do:
       /* this code skips the all other lines for printing detail */
       /* next line would be your detail print procedure */
     if p-exportl then
       run print-dtl-export(input drpt.kpztempid).
     else   
       run print-dtl(input drpt.kpztempid).
     end.  
  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_finalprint) = 2 &then
  
if not p-exportl then
 do:
 if not x-stream then 
   do:
   display 
    v-final
    v-astrik
    v-summary-lit2
/* PTD */
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts3[u-entitys + 1]   @ v-amt2
    with frame f-tot.
  
   down with frame f-tot.
   end.
 else
   do:
   display stream xpcd
    v-final
    v-astrik
    v-summary-lit2
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts3[u-entitys + 1]   @ v-amt2
    with frame f-tot.
  
   down stream xpcd with frame f-tot.
   end.
      
 end.
else
  do:
  assign export_rec =  v-astrik.
  do v-inx4 = 1 to u-entitys:
    if v-totalup[v-inx4] = "n" then
       next.
    if v-inx4 = 1 then
      v-summary-lit2 = "Final".
    else
      v-summary-lit2 = "".
    if v-cells[v-inx4]= "2" then
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec + v-summary-lit2 + v-del + " ".
     
      end.
    else
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec +  v-summary-lit2.
      end.
  end.
  assign export_rec = export_rec + v-del +
      string(t-amounts1[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts2[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts3[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts4[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts5[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts6[u-entitys + 1],"->>>>>>>>9").
    
  assign export_rec = export_rec + chr(13).    
  put stream expt unformatted export_rec.
  end.
    
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_summaryframeprint) = 2 &then
  
    if not x-stream then
      do:
      display 
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        t-amounts3[v-inx2]   @ v-amt3
        t-amounts4[v-inx2]   @ v-amt4
        t-amounts5[v-inx2]   @ v-amt5
        t-amounts6[v-inx2]   @ v-amt6
        with frame f-tot.
      down with frame f-tot.
      end.
    else
      do:
      display stream xpcd
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        t-amounts3[v-inx2]   @ v-amt3
        t-amounts4[v-inx2]   @ v-amt4
        t-amounts5[v-inx2]   @ v-amt5
        t-amounts6[v-inx2]   @ v-amt6
        with frame f-tot.
      down stream xpcd with frame f-tot.
      end.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_summaryputexport) = 2 &then
 
/* PTD */
    assign export_rec = v-astrik + v-del + export_rec.
    assign export_rec = export_rec + v-del +
      string(t-amounts1[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts2[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts3[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts4[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts5[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts6[v-inx2],"->>>>>>>>9").
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_procedures) = 2 &then
procedure sapb_vars:
  assign
       b-kitprod    = sapb.rangebeg[1]
       e-kitprod    = sapb.rangeend[1]
       b-district   = sapb.rangebeg[2]
       e-district   = sapb.rangeend[2]
       b-salesout   = sapb.rangebeg[3]
       e-salesout   = sapb.rangeend[3]
       b-salesin    = sapb.rangebeg[4]
       e-salesin    = sapb.rangeend[4]
       b-takenby    = sapb.rangebeg[5]
       e-takenby    = sapb.rangeend[5]
       b-cust       = dec(sapb.rangebeg[6])
       e-cust       = dec(sapb.rangeend[6])
       b-createddt  = date(sapb.rangebeg[7])
       e-createddt  = date(sapb.rangeend[7])
       
       p-format     = int(sapb.optvalue[1])
       p-export     = sapb.optvalue[2].
  
  assign p-detailprt = yes
         p-detail    = "D"
         p-portrait  = yes.
  run formatoptions.
  
  
  
end.
/* ------------------------------------------------------------------------- */
procedure formatoptions:
/* ------------------------------------------------------------------------- */
   
   
  assign p-portrait = false.
  /* if false then the report will print in LANDSCAPE 178 character */
  
  if sapb.optvalue[1] > "0" and sapb.optvalue[1] ne "99" then do:
    p-optiontype = " ".
    find notes where notes.cono = g-cono and
                     notes.notestype = "zz" and
                     notes.primarykey = "kprzo" and  /* tbxr program */
                     notes.secondarykey = string(p-format) no-lock no-error.
    if not avail notes then do:
      display "Format is not valid cannot process request".
      assign p-optiontype = "r"
             p-sorttype   = ">,"
             p-totaltype  = "l"
             p-summcounts = "a".
      return.
    end.               
   
    assign p-optiontype = notes.noteln[1]
           p-sorttype   = notes.noteln[2]
           p-totaltype  = notes.noteln[3]
           p-summcounts = notes.noteln[4]
           p-export     = "    "
           p-register   = notes.noteln[5]
           p-registerex = notes.noteln[6].
  end.     
  else
  if sapb.optvalue[1] = "99" then do:
    assign p-register   = ""
           p-registerex = "".
    run reportopts(input sapb.user5, 
                   input-output p-optiontype,
                   input-output p-sorttype,  
                   input-output p-totaltype,
                   input-output p-summcounts,
                   input-output p-register,
                   input-output p-regval).
  end.
end.
   
 
procedure extract_data:
assign v-mgn = 0
       v-mgn2 = 0.
for each zsdikpz use-index k-zsdikpz where zsdikpz.cono = g-cono and
                                          (zsdikpz.kitprod >= b-kitprod  and
                                           zsdikpz.kitprod <= e-kitprod) and
                                          (zsdikpz.createddt >= b-createddt and
                                           zsdikpz.createddt <= e-createddt) 
                                           no-lock:
  for each oeel use-index k-prod where oeel.cono = g-cono and
                                       oeel.shipprod = zsdikpz.kitprod and
                                      (oeel.custno >= b-cust and
                                       oeel.custno <= e-cust) and
                                       oeel.whse = zsdikpz.whse and  
                                      (oeel.slsrepout >= b-salesout  and
                                       oeel.slsrepout <= e-salesout) and
                                      (oeel.slsrepin  >= b-salesin   and
                                       oeel.slsrepin  <= e-salesin) 
                                       no-lock:
                
    for each oeeh use-index k-oeeh where oeeh.cono = oeel.cono and
                                         oeeh.orderno = oeel.orderno   and
                                         oeeh.ordersuf = oeel.ordersuf and   
                                        (oeeh.takenby   >= b-takenby   and
                                         oeeh.takenby   <= e-takenby)  and
                                         oeeh.stagecd   <= 1           and
                                        (oeeh.transtype <> "BL"        and
                                         oeeh.transtype <> "QU")
                                         no-lock:
         
         
     for each smsn use-index k-smsn where smsn.cono = g-cono and
                                          smsn.slsrep = oeel.slsrepout and
                                         (smsn.mgr >= b-district and
                                          smsn.mgr <= e-district)
                                          no-lock:
                                          
      for each arsc where arsc.cono = g-cono and
                            arsc.custno = oeeh.custno
                            no-lock:
              
              /*
              display "prod" oeel.shipprod
               "price" oeel.price "cost" oeel.prodcost 
               "zsdi" zsdikpz.wstndcost
               "whse" zsdikpz.whse
               "oeeh" oeeh.orderno oeeh.ordersuf.
              */
              
       assign v-mgn  = ((oeel.price - oeel.prodcost) / oeel.price) * 100.
       assign v-mgn2 = ((oeel.price - zsdikpz.wstndcost) / oeel.price) * 100.
   
       
       
       create kpztemp.
       assign /* kpztemp.sortdtl     = string(oeel.shipprod) +
                                    string(oeeh.orderno) + 
                                    string(oeeh.ordersuf)    */
              kpztemp.mgr         = smsn.mgr      
              kpztemp.name        = arsc.name             
              kpztemp.takenby     = oeeh.takenby            
              kpztemp.orderno     = oeeh.orderno            
              kpztemp.ordersuf    = oeeh.ordersuf
              kpztemp.lineno      = oeel.lineno            
              kpztemp.prod        = oeel.shipprod            
              kpztemp.qtyord      = oeel.qtyord            
              kpztemp.price       = oeel.price            
              kpztemp.prodcost    = oeel.prodcost
              kpztemp.margin1     = v-mgn             
              kpztemp.wstndcost   = zsdikpz.wstndcost           
              kpztemp.margin2     = v-mgn2             
              kpztemp.slsrepin    = oeeh.slsrepin
              kpztemp.usedwhse    = zsdikpz.usedwhse
              kpztemp.createddt   = zsdikpz.createddt.
   
      end.  /* create */
   end.
  end.
 end.
end.
end.  
/*--------------------------------------------------------------------------*/
procedure print-dtl-export:
/*--------------------------------------------------------------------------*/
define input parameter ip-recid as recid no-undo.
 find kpztemp where recid(kpztemp) = ip-recid no-lock no-error.
 
 put stream expt unformatted
     "  "
     v-del
     v-del
     kpztemp.mgr          
     v-del
     kpztemp.name                      
     v-del
     kpztemp.takenby                  
     v-del
     kpztemp.slsrepin     
     v-del
     kpztemp.orderno                  
     v-del
     kpztemp.ordersuf     
     v-del
     kpztemp.lineno                   
     v-del
     kpztemp.prod                 
     v-del
     kpztemp.qtyord                   
     v-del
     kpztemp.price                    
     v-del
     kpztemp.prodcost     
     v-del
     kpztemp.margin1                   
     v-del
     kpztemp.wstndcost               
     v-del
     kpztemp.margin2                   
     v-del
     kpztemp.usedwhse
     v-del
     kpztemp.createddt
     chr(13).
end.  /* procedure print-dtl-export */
 
/* THE SECOND COMMENT WAS MISSING THE SLASH AND THE SCREWY ERROR CAME FROM
   THAT tah 
*/   
 
/*---------------------------------------------------------------------------*/
procedure print-dtl:
/*----------------------------------------------------------------------------*/
define input parameter ip-recid as recid no-undo.
 find kpztemp where recid(kpztemp) = ip-recid no-lock no-error.
display
kpztemp.mgr          
kpztemp.name                      
kpztemp.takenby                  
kpztemp.orderno                  
kpztemp.ordersuf     
kpztemp.lineno                   
kpztemp.prod                 
kpztemp.qtyord                   
kpztemp.price                    
kpztemp.prodcost     
kpztemp.margin1                   
kpztemp.wstndcost               
kpztemp.margin2                   
kpztemp.slsrepin     
kpztemp.usedwhse
kpztemp.createddt
with frame f-orderdtl.
down with frame f-orderdtl.
   
end.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
