/* NOTE - When compiling zsdioble.p (Quote Inquiry; Extended Line Info)
          comment out the "find t-lines" */

def buffer z-sasos  for sasos.  
def buffer z-oeelb  for oeelb.  
def buffer u-oeelb  for oeelb.
def var zx-promisedt  as date format "99/99/99"     no-undo.
def var zx-expshipdt  as date format "99/99/99"     no-undo.
def var zx-jitbl      as char format "x" init "n"   no-undo.
def var zx-lineupdt   as char format "x" init "n"   no-undo.
def var hzx-promisedt as date format "99/99/99"     no-undo.
def var hzx-expshipdt as date format "99/99/99"     no-undo.

/* EN-1808 */
def var var-inputpd       like oeeh.promisedt       no-undo.                   
def var v-1019fl          as logical  init  no      no-undo.
def var h-shipdt          like oeeh.promisedt       no-undo.
form
   zx-promisedt  colon 13 label "Promise  Dt"
   zx-expshipdt  colon 13 label "Exp/Ship Dt"
   zx-lineupdt   colon 23 label "Update all lines y/n?"
   with frame f-oeexq-dates row 8 col 28 width 30
              overlay side-labels title "Quote Extended Date Entry".    

find first z-sasos where
           z-sasos.cono  = g-cono    and
           z-sasos.oper2 = g-operinits and
           z-sasos.menuproc = "zxt1"
           no-lock no-error.
if avail z-sasos and z-sasos.securcd[7] > 2 then
  do:
  find z-oeelb where 
       z-oeelb.cono = g-cono and
       z-oeelb.batchnm = oeehb.batchnm and 
       z-oeelb.seqno   = oeehb.seqno and
       z-oeelb.lineno = v-lineno
       no-lock no-error.
  if avail z-oeelb then
    assign zx-promisedt = z-oeelb.promisedt
           zx-expshipdt = z-oeelb.reqshipdt
           zx-lineupdt  = "n"
           hzx-promisedt = z-oeelb.promisedt
           hzx-expshipdt = z-oeelb.reqshipdt.
  else
    assign zx-promisedt = v-shipdt
           zx-expshipdt = v-shipdt
           zx-lineupdt  = "n"
           hzx-promisedt = v-shipdt
           hzx-expshipdt = v-shipdt.
  if oeehb.stagecd = 0 and v-specnstype <> "L" then
    do:
    display zx-promisedt
            zx-expshipdt
            zx-lineupdt
            with frame f-oeexq-dates.
    dates-updt:
    do while true with frame f-oeexq-dates on endkey undo dates-updt,
                                                     leave dates-updt:
      update zx-promisedt
             zx-expshipdt
             zx-lineupdt
             with frame f-oeexq-dates
      editing:
        readkey.
        if {k-cancel.i} or {k-jump.i} then
          do:
          hide frame f-oeexq-dates.
          leave dates-updt.
        end.
        if {k-after.i} and frame-field = "zx-lineupdt" then
          do:
          if input zx-lineupdt <> "N" and
             input zx-lineupdt <> "y" then
            do:
            message "You must enter a 'n' or 'y' in this field.".
            next-prompt zx-lineupdt with frame f-oeexq-dates.
            next.
          end.  
        end.
        if {k-after.i} and (frame-field = "zx-promisedt" or 
                           /* frame-field = "zx-expshipdt" or */
                            frame-field = "zx-lineupdt") then  
        do:   

          if (input zx-promisedt < today and 
              input zx-promisedt <> 09/09/99) /*  or 
             (input zx-expshipdt < today and 
              input zx-expshipdt <> 09/09/99) */ 
          then 
          do:
       message "Check Promise Date. Can't be less than today. Please correct".
            next-prompt zx-promisedt with frame f-oeexq-dates.
            next.
          end.
          /* EN-1808 */
          
          else if input zx-promisedt <> "" or 
                  input zx-expshipdt <> "" 
          then do: 
        /**    assign zx-expshipdt = input zx-promisedt.  **/

            assign var-inputpd  = input zx-promisedt
                   zx-promisedt = var-inputpd
                   v-1019fl     = no.
            run zsdiweekendh (input-output var-inputpd,
                              input-output v-1019fl,     
                              input        oeehb.whse,
                              input        g-cono,
                              input        oeehb.divno).
            if v-1019fl = yes then
              do:
              assign zx-promisedt = var-inputpd
                     zx-expshipdt = var-inputpd. 
              display zx-promisedt zx-expshipdt with frame f-oeexq-dates.
              assign v-1019fl = no.
              run err.p(1019).
              if frame-field = "zx-promisedt" then
                next-prompt zx-expshipdt with frame f-oeexq-dates.
              if frame-field = "zx-expshipdt" then
                next-prompt zx-lineupdt with frame f-oeexq-dates. 
              next.
            end.
            if frame-field = "zx-expshipdt" then
              do:
              assign var-inputpd  = input zx-expshipdt
                     zx-expshipdt = var-inputpd
                     v-1019fl     = no.
              run zsdiweekendh (input-output zx-expshipdt,
                                input-output v-1019fl,
                                input        oeehb.whse,
                                input        g-cono,
                                input        oeehb.divno).
              display zx-expshipdt with frame f-oeexq-dates.
              if v-1019fl = yes then
                do:
                assign h-shipdt = zx-expshipdt.
                run err.p(1019).
                next-prompt zx-lineupdt with frame f-oeexq-dates.
                next.
              end.
            end.
          end.
          else 
            assign zx-promisedt = input zx-promisedt
                   zx-expshipdt = input zx-expshipdt.          

            
          assign manualdtfl = yes.
         
          
          /* smaf */
          assign zx-promisedt
                 zx-expshipdt.
          /* check if promisedt was changed and expshipdt wasn't,
             default epshipdt = promisedt. */
          if zx-promisedt <> hzx-promisedt and
             zx-expshipdt  = hzx-expshipdt then
            assign zx-expshipdt = zx-promisedt.
          assign v-shipdt    = zx-promisedt
                 v-reqshipdt = zx-expshipdt.
          if input zx-lineupdt = "y" then
            do:   
            for each u-oeelb where 
                     u-oeelb.cono = g-cono and
                     u-oeelb.batchnm = oeehb.batchnm and
                     u-oeelb.seqno   = oeehb.seqno:
              overlay(u-oeelb.user12,1,8)   = 
                                  string(u-oeelb.promisedt,"99/99/99").
              overlay(u-oeelb.user12,10,4)  = 
                            string(zx-promisedt - TODAY,"9999").
              assign u-oeelb.promisedt = zx-promisedt
                     u-oeelb.reqshipdt = zx-expshipdt
                     u-oeelb.xxda1     = zx-promisedt
              substr(u-oeelb.user4,26,8) = string(zx-promisedt,"99/99/99").
                    
              /* NOTE-Comment out find t-lines when compiling zsdioble.p */   
              /**/
              find t-lines where t-lines.lineno = u-oeelb.lineno no-error.
              if avail t-lines then
                assign t-lines.shipdt = zx-promisedt. 
              /**/
            end. /* each u-oeelb */
          end. /* zx-lineupdt = y */
          else
            do:
            find u-oeelb where 
                 u-oeelb.cono = g-cono and
                 u-oeelb.batchnm = oeehb.batchnm and
                 u-oeelb.seqno   = oeehb.seqno   and
                 u-oeelb.lineno  = v-lineno
                 no-error.
            if avail u-oeelb then   
              do:
              overlay(u-oeelb.user12,1,8) =
                                      string(u-oeelb.promisedt,"99/99/99").
              overlay(u-oeelb.user12,10,4)  =   
                            string(zx-promisedt - TODAY,"9999").
              assign u-oeelb.promisedt = zx-promisedt
                     u-oeelb.reqshipdt = zx-expshipdt
                     u-oeelb.xxda1     = zx-promisedt
              substr(u-oeelb.user4,26,8) = string(zx-promisedt,"99/99/99"). 
            end. /* avail u-oeelb */
          end.
        end. /* k-after */
        apply lastkey.
      end. /* update */
      leave dates-updt.
    end. /* do while */
  end. /* stagecd <> 0 and specnstype <> "L" */     
  else
    do:
    message "Quote and/or Line must be active". 
    pause.  
  end.
end. /* security level */       
else  
  do:
  message "You Currently do not have security to change line dates".
  pause.
end.
hide frame f-oeexq-dates.
