/* oeepp1h.las */
/* oeepp1h.las 1.2 10/19/98 */

/*h****************************************************************************
  INCLUDE      : oeepp1h.las
  DESCRIPTION  : assign variables to display on the pick ticket header
  USED ONCE?   :
  AUTHOR       : jms
  DATE WRITTEN : 06/11/96
  CHANGES MADE :
    10/02/2007 dkt need a hook - print address on "w" oeeh.orderdisp
    
    06/11/96 jms; TB# 21254 (10A) Develop Value Add Module
    08/06/96 jms; TB# 21254 (10A) Develop Value Add Module
             change s-billcity to blank if no ship to file
    10/08/98 sbr; TB# 25363 Option 9 Ship Complete Handling error
    02/09/99 sbr; TB# 25776 Pick ticket print says pick ticket
    03/29/00 rgm; TB# 3-2  Add RxServer Interface to Standard
    05/08/00 rgm; TB# A9   Fix for RxServer Faxing issue
    06/10/02 rgm; TB#e13459 Correct RxServer issue with ? in g-today field.
******************************************************************************/
/*e     Parameters:
           &termstype  = The field to be read for the terms
           &head       = The file to be read for header information
           &section    = Header file for "OE" or Section file for "VA"
           &orderdisp  = The field or variable for the order disposition
           &totqtyshp  = The field or variable for the total quantity shipped
           &totqtyord  = The field or variable for the total quantity ordered
           &norushln   = The field or variable for the # of rush line items
           &prtpricefl = The field or variable for the print price flag
           &codfl      = The field or variable for the C.O.D. flag
           &lumpbillfl = The field or variable for the lump billing flag
           &shiptonm   = Ship to name or destination name
           &arsc       = The bill to file name
           &shiptofile = The file for ship to information
           &shiptost   = Ship to state
           &shiptozip  = Ship to zip code

*******************************************************************************/

        {sasta.gfi "t" {&termstype} no}
        {w-icsd.i {&head}.whse no-lock}

        /*d Set variables for display on the pick ticket header; determine if
            ship, ship comp,tag and hold order has been comp filled */
        /*tb 25363 10/08/98 sbr; Changed the assignment of s-reprint to check
            for v-loopcnt = 2 before {&section}.pickcnt < 2 instead of after. */
        /*tb 25776 02/09/99 sbr; Changed the assignment of s-doctype to check
            for "sapb.currproc = "vaepp"" instead of "g-ourproc = "vaepp"". */
        assign
            v-completefl = if can-do("s,t",{&orderdisp}) and
                               {&totqtyshp} <> {&totqtyord}
                               then no
                           else if {&orderdisp} = "s" and
                               p-packty = "b" and
                               v-loopcnt <> v-printcnt then no
                           else yes
            s-whsedesc   = if avail icsd then icsd.name
                           else {&head}.whse
            s-billname   = if v-invtofl then {&shiptonm}
                           else {&arsc}name
            s-billaddr1  = if v-invtofl then {&shiptofile}addr[1]
                           else {&arsc}addr[1]
            s-billaddr2  = if v-invtofl then {&shiptofile}addr[2]
                           else {&arsc}addr[2]
            s-billcity   = if v-invtofl then
                               {&shiptofile}city + (if {&shiptost} = "" and
                               {&shiptozip} = "" then ""
                               else ", " + {&shiptost} + " " + {&shiptozip})
                           else {&arsc}city + (if {&arsc}state = "" and
                               {&arsc}zipcd = "" then ""
                               else ", " + {&arsc}state + " " + {&arsc}zipcd)
            s-corrcity   = sasc.city + ", " + sasc.state + " " + sasc.zipcd
            s-doctype    = if sapb.currproc = "vaepp" then
                               "Value Add Pick Ticket"
                           else "Pick Ticket/Packing List"
            s-stagearea  = if can-do("t,w,s",{&orderdisp}) and
                               {&section}.stagearea = "" then "********"
                           else {&section}.stagearea
            s-reprint    = if v-loopcnt = 2 then "*** PACKING SLIP ***"
                           else if {&section}.pickcnt < 2 then ""
                           else "*** R E P R I N T " +
                               (if p-reprintfl = yes then  "(Changes Only) "
                                else "") + "***"
            s-rush       = if {&norushln} <> 0 then
                                "*** RUSH ***" else ""
            s-filltype   = if g-ourproc <> "oeepb" then ""
                           else "Filled From Received Merchandise"
            s-pricedesc  = if {&prtpricefl} = no then ""
                           else
                            "** DO NOT PAY **** DO NOT PAY **** DO NOT PAY **"
            s-orderdisp  = if {&head}.transtype = "rm" then
                            "*** RETURN MERCH. ***"
                           else if {&head}.transtype = "cr" then
                               "*** CORRECTION ***"
                           else if  {&orderdisp} = "s" then
                               "*** SHIP COMPLETE ***"
                           else if {&orderdisp} = "t" then
                               "*** TAG AND HOLD ***"
                           else if {&orderdisp} = "w" then
                               "*** WILL CALL ***"
                           else if {&orderdisp} = "j" then
                               "*** JUST IN TIME ***"
                           else ""
            s-cod        = if {&codfl} = no then "" else "C.O.D."
            s-lit40d     = if can-do("bl,br",{&head}.transtype) and
                               {&lumpbillfl} = yes then
                               "Amount Billed ________"
                           else "Total __________ _____"
            v-linecnt    = 0
            v-noprint    = 0
            s-linecntx   = ""
            v-noprintx   = ""
            a-totqtyshp  = 0
            a-totcubes   = 0
            a-totweight  = 0
            a-totlineamt = 0
            s-totqtyshp  = ""
            s-totlineamt = ""
            s-totcubes   = ""
            s-totweight  = ""
            s-terms      = if avail sasta then sasta.descrip else {&termstype}
            v-{&head}id     = recid({&head}).
      
        /** need a hook dkt 10/02/2007 **/
        /* if (can-do("t,w",{&orderdisp}) or */
        if (({&orderdisp} = "t") or 
            ({&orderdisp} = "s" and v-completefl = no)) then
            assign
                s-shiptonm      = ""
                s-shiptoaddr    = ""
                s-shipcity      = "".

        else   
            assign
                s-shiptonm      = {&shiptonm}
                s-shiptoaddr[1] = {&shiptofile}addr[1]
                s-shiptoaddr[2] = {&shiptofile}addr[2]
                s-shipcity      = {&shiptofile}city + ", " + {&shiptost} +
                                    " " + {&shiptozip}.

        {sasta.gfi "S" {&section}.shipvia no}
        s-shipvia = if avail sasta then sasta.descrip
                    else {&section}.shipvia.

        /*tb 3-2 03/29/00 rgm; Rxserver interface display vars for rxserver */
        /*tb A9 05/08/00 rgm; Rxserver interface change when appropriate */
        &IF DEFINED(rxserver) <> 0 &THEN
            if g-today = ? then g-today = today.
            assign
                s-rxhead1 = ""
                overlay(s-rxhead1,1,8)   = string(g-today)
                overlay(s-rxhead1,10,5)  = string(time,"hh:mm")
                overlay(s-rxhead1,110,20) = "Date/Time Printed".
             &if "{&head}" = "oeeh" &then
                assign
                    overlay(s-rxhead1,16,4)  = {&head}.takenby
                    overlay(s-rxhead1,20,4)  = {&head}.slsrepin
                    overlay(s-rxhead1,24,4)  = {&head}.slsrepout
                    overlay(s-rxhead1,28,10) = {&head}.placedby
                    overlay(s-rxhead1,38,24) = {&head}.custpo
                    overlay(s-rxhead1,62,24) = {&head}.refer
                    overlay(s-rxhead1,86,8)  = if {&head}.enterdt <> ?
                                               then string({&head}.enterdt)
                                               else ""
                    overlay(s-rxhead1,94,8)  = if {&head}.pickeddt <> ?
                                               then string({&head}.pickeddt)
                                               else ""
                    overlay(s-rxhead1,102,8) = if {&head}.shipdt <> ?
                                               then string({&head}.shipdt)
                                               else ""
                    overlay(s-rxhead1,110,20) = "Date/Time Printed"
                    overlay(s-rxhead1,130,10) = "Taken By"
                    overlay(s-rxhead1,140,10) = "Slsrep In"
                    overlay(s-rxhead1,150,10) = "Slsrep Out"
                    overlay(s-rxhead1,160,10) = "Placed By"
                    overlay(s-rxhead1,170,20) = "Customer PO#"
                    overlay(s-rxhead1,190,10) = "Reference"
                    overlay(s-rxhead1,200,10) = "Entered"
                    overlay(s-rxhead1,210,10) = "Picked"
                    overlay(s-rxhead1,220,10) = "Shipped".
            &endif
            assign
                s-rxtext1               = s-lit14a
                overlay(s-rxtext1,79)   = s-lit14b
                overlay(s-rxtext1,133)  = s-lit15a
                overlay(s-rxtext1,211)  = s-lit15b.
        &ENDIF
        {oeepp9.z99 &before_headview = "*"
                    &termstype  = "{&termstype}"
                    &head       = "{&head}"
                    &section    = "{&section}"
                    &orderdisp  = "{&orderdisp}"
                    &totqtyshp  = "{&totqtyshp}"
                    &totqtyord  = "{&totqtyord}"
                    &norushln   = "{&norushln}"
                    &prtpricefl = "{&prtpricefl}"
                    &codfl      = "{&codfl}"
                    &lumpbillfl = "{&lumpbillfl}"
                    &arsc       = "{&arsc}"
                    &shiptonm   = "{&shiptonm}"
                    &shiptofile = "{&shiptofile}"
                    &shiptost   = "{&shiptost}"
                    &shiptozip  = "{&shiptozip}" }

        view frame f-head.
        view frame f-tot.
