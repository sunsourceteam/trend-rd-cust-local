/*h****************************************************************************
  INCLUDE      : oeepiel.lpr
  DESCRIPTION  : Processing logic used by both oeepie and oeepae to prepare
                 line variables for producing the following documents.  It is
                 used in both CHUI and GUI:
                 1) 810 - EDI Invoice
                 2) 843 - EDI Response for Price
                 3) XML Enabled documents for Invoice and Order Acknowledgement
  USED ONCE?   : yes
  AUTHOR       : ajw
  DATE WRITTEN : 04/12/00
  CHANGES MADE :
    04/12/00 ajw; TB# e4931 Buildnet Enable code reuse.
    12/02/02 ns;  TB# e12955 Additional field EDI Line #, Customer Product
    05/01/03 lbr; TB# e17245 Added more vars for eBill
    05/05/03 lbr; TB# e17235 Changed format for speccostunit
    05/05/03 lbr; TB# e17268 Added spec price units for stocking and selling
        from icsp. Looked up icsp.unitstock and icsp.unitsell in icseu.
    07/15/03 rgm; TB# e17950 add unitconv from oeel for unit conversion changes
    07/24/03 sbr; TB# e16932 840 inbound 855 outbound indicate changes made
    10/07/03 jlc; TB# e13983 Add more edih/edil data
    07/12/04 emc; TB# e20448 Add Serial#'s to EDI Invoice for eBill
    10/11/04 sbr; TB# e21036 If RM line, netamount wrong (EDI only)
    10/19/05 sbr; TB# e23400 EDI 810 invoice flat file missing lines
    11/21/05 rghm;TB# e23592 Add call to upcno.gco for Vendor/prod upc number
******************************************************************************/
    /*NOTE:  The references to the EDIH and EDIL EDI files are only used for
             EDI Processing.  If this include is called for the XML format,
             this logic is commented out throughout this include file.*/

    /* ALREADY HAVE THE EDIL RECORD 
    /* Remove from &edicom - this is needed to properly assign s-custprod and
       the new s-edilineno field */
    {edil.gfi &batchnm  = "VERMEER"
              &seqno    = oeel.orderno
              &lineno   = oeel.lineno
              &lock     = no}
    */
    
    /*Ability to comment out the EDI files processing.*/
    {&edicom}
    /*tb 15205 02/23/94 jlc; process edi855 document. */
    /*d when processing the 855, flag 'important' line changes. */
    s-lnacktype  = v-2spaces.

    if v-prt855fl then do:
        s-lnacktype =   if (avail edil and edil.price ne oeel.price) or
                           (avail edil and edil.qtyord ne oeel.qtyord)
                            /* or not avail edil */ then
                                "IC"
                        else "IA".
    end.
    /{&edicom}* */

    /*d Check for proper acknowledgement type */
    {oeepiel.z99 &acknowledge_check = "*"}

    if oeel.specnstype ne "n" then do:
        find icsw where
             icsw.cono = oeel.cono     and
             icsw.prod = oeel.shipprod and
             icsw.whse = oeel.whse no-lock no-error.
        {w-icsp.i oeel.shipprod no-lock}
        if oeeh.langcd <> "" then
          {w-sals.i oeeh.langcd "p" oeel.shipprod no-lock}
    end.
    {w-sasta.i "u" oeel.unit no-lock}

    /* tb e17268 05/05/03 lbr; Added icseu lookup */
    {w-icseu.i oeel.shipprod icsp.unitsell}
    s-ediunitsell = if avail icseu then                                             string(icseu.unitconv,"999999.99999")
                       else "000000.00000".

    {w-icseu.i oeel.shipprod icsp.unitstock}
    s-ediunitstock = if avail icseu then                                              string(icseu.unitconv,"999999.99999")
                        else "000000.00000".
    /* rgm; TB e17950 added oeel.unitconv assign */
    s-ediunitconv = if avail oeel then
        string(oeel.unitconv,"999999.99999")
                        else "000000.00000".


    /*tb 11064 04/24/93 jlc; Add the quantity ordered (s-qtyord) */
    /*tb 11064 04/24/93 jlc; Add the quantity ordered (s-totqtyord) */
    /*tb 11078 04/26/93 jlc; Add user fields (s-itemuser1 - s-itemuser10) */
    /*tb 13659 11/11/93 jlc; Add requested product (s-custprod) */
    /*tb 13286 10/10/93 jlc; Add edi backorder code (s-bofl) */
    /*tb 13515 10/28/93 jlc; Add unit discount (s-unitdisc) */
    /*tb 18581 05/31/95 jkp; Add special cost fields (s-specuom) */
    /*tb 18488 06/01/95 jkp; Add oeel.xrefprodty field (s-xrefprodty) */
    /*tb 20599 07/25/96 jkp; Add oeel tax fields (s-taxablefl, s-taxablety,
         s-taxamount1, s-taxamount2, s-taxamount3, s-taxamount4, and
         s-taxgroup) */
    /*tb 20987 08/19/96 jkp; Add line level restocking charges
         (s-lnrestock and s-lnrestockamt) */
    /*tb 24619 03/27/98 jlc; Add promisedt and reqshipdt to item record. */
    /*tb 24730 03/27/98 jlc; Add special non-stock type to item record. */
    /*tb 24477 03/27/98 jlc; User field 1 not printing to item record. */
    /*tb 24671 05/14/98 sbr; Changed assignments of s-taxamount1, s-taxamount2,
         s-taxamount3, and s-taxamount4 to include five decimal places */
    /*tb 9731  10/21/98 jgc; Expand non-stock description.  Added
        oeel.proddesc2 to s-descrip assignment. */
    /*tb e13983 10/07/03 jlc; Add more edih/edil data */
    assign
        v-oeelid       = recid(oeel)
        v-linecnt      = v-linecnt + 1
        s-linecnt      = string(v-linecnt,"999999")
        s-qtyship      = if oeel.returnfl then
                             string(oeel.qtyship * -1,"-999999.99")
                         else
                             string(oeel.qtyship,"-999999.99")
        s-qtyord       = if oeel.returnfl then
                             string(oeel.qtyord * -1,"-999999.99")
                         else       
                             string(oeel.qtyord,"-999999.99")
        s-totqtyshp    = string(dec(s-totqtyshp) + dec(s-qtyship),"-9999999.99")
        s-totqtyshp    = string(dec(s-totqtyshp),"-9999999.99")
        s-totqtyord    = string(dec(s-totqtyord) + dec(s-qtyord),"-9999999.99")
        s-totqtyord    = string(dec(s-totqtyord),"-9999999.99")
        s-lineno       = string(oeel.lineno,"9999")
        s-edilineno    = if available edil and edil.edilineno > '' then
                              string(edil.edilineno, "x(11)")
                         else fill(' ',11)
        s-price        = if oeel.corechgty = "r" then
                             string(oeel.corecharge,"-9999999.99999")
                         else
                             string(oeel.price,"-9999999.99999")
        s-discpct      = if oeel.discpct ne 0 then
                             string(oeel.discpct,"-9999999.99999")
                         else
                             v-14spaces
        s-unit         = if avail sasta then
                             string(sasta.unitediuom,"xx")
                         else
                             string("EA","xx")
        s-descrip      = if oeel.specnstype = "n" then
                             string((oeel.proddesc + " " +
                                oeel.proddesc2),"x(24)")
                         else
                             if not avail icsp or not avail icsw then
                                 if oeeh.langcd = "" or not avail sals then
                                     string("Invalid","x(24)")
                         else
                             string((sals.descrip[1] + " " +
                                 sals.descrip[2]),"x(24)")
                         else
                             if oeeh.langcd = "" or not avail sals then
                                 string((icsp.descrip[1] + " " +
                                     icsp.descrip[2]),"x(24)")
                         else
                             string((sals.descrip[1] + " " +
                                 sals.descrip[2]),"x(24)")
        /* tb e17245 lbr; added netamt and netord */
        s-edinetamt     = if oeel.returnfl then
                              string(oeel.netamt * -1,"-999999999.99")
                          else
                              string(oeel.netamt,"-999999999.99")
        s-edinetord     = if oeel.returnfl then
                              string(oeel.netord * -1,"-999999999.99")
                          else
                              string(oeel.netord,"-999999999.99")

        s-itemuser1    = if avail icsp then
                             string(icsp.user1, "x(8)")
                         else
                             v-8spaces
        s-itemuser2    = if avail icsp then
                             string(icsp.user2, "x(8)")
                         else
                             v-8spaces
        s-itemuser3    = if avail icsw then
                             string(icsw.user1, "x(8)")
                         else
                             v-8spaces
        s-itemuser4    = if avail icsw then
                             string(icsw.user2, "x(8)")
                         else
                             v-8spaces
        s-itemuser5    = string(substring(oeel.user1,1,8),"x(8)").


    /*Ability to Comment Out EDI files*/
    {&edicom}
    assign
        s-itemuser6    = if avail edil then
                             string(edil.user1, "x(8)")
                         else
                             v-8spaces
        s-itemuser7    = if avail edil then
                             string(edil.user2, "x(8)")
                         else
                             v-8spaces
        s-itemuser8    = if avail edil then
                             string(edil.user3, "x(8)")
                         else
                             v-8spaces
        s-edilprice    = if avail edil then
                             string(edil.price,"-9999999.99999")
                         else
                             fill(" ",14)
        s-edilproddesc = if avail edil then
                             string(edil.proddesc,"x(24)")
                         else
                             fill(" ",24)
        s-edilproddesc2 = if avail edil then
                             string(edil.proddesc2,"x(24)")
                          else
                             fill(" ",24)
        s-edilqtyord    = if avail edil then
                             string(edil.qtyord,"-9999999.99")
                          else
                             fill(" ",11)
        s-edilreqprod   = if avail edil then
                             string(edil.reqprod,"x(24)")
                          else
                             fill(" ",24)
        s-edilshipprod  = if avail edil then
                             string(edil.shipprod,"x(24)")
                          else
                             fill(" ",24)
        s-ediltaxablety = if avail edil then
                             string(edil.taxablety,"x(8)")
                          else
                             fill(" ",8)
        s-edilxrefprod  = if avail edil then
                             string(edil.xrefprod,"x(1)")
                          else
                             fill(" ",1).
    /{&edicom}* */

    assign
        s-itemuser9    = v-8spaces
        s-itemuser10   = v-8spaces
        s-shipprod     = string(oeel.shipprod,"x(24)")
        s-custprod     = if available edil and edil.custprod > '' then
                              string(edil.custprod, "x(24)")
                         else string(oeel.reqprod,"x(24)")
        /* v-icsecfl   = no */
        s-bofl         = string(oeel.botype,"x")
        s-unitdisc     = if oeel.discamt ne 0 then
                             string(oeel.discamt,"9999999.99999")
                         else
                             v-13spaces
        s-specuom      = v-2spaces
        s-xrefprodty   = string(oeel.xrefprodty,"x(1)")
        s-taxablefl    = if oeel.taxablefl = yes then
                             string("y","x(1)")
                         else string("n","x(1)")
        s-taxablety    = string(oeel.taxablety,"x(1)")
        s-taxamount1   = string(oeel.taxamount[1],"-9999999.99999")
        s-taxamount2   = string(oeel.taxamount[2],"-9999999.99999")
        s-taxamount3   = string(oeel.taxamount[3],"-9999999.99999")
        s-taxamount4   = string(oeel.taxamount[4],"-9999999.99999")
        s-taxgroup     = string(oeel.taxgroup,"9")
        s-lnrestock    = if oeel.restockfl = yes then            /* $ */
                             oeel.restockamt
                         else                                    /* % */
                             round(oeel.netamt * (oeel.restockamt / 100),2)
        s-lnrestockamt = string(s-lnrestock,"999999999.99")
        s-lnpromisedt  = if oeel.promisedt ne ? then
                             string(oeel.promisedt,"99/99/99")
                         else v-8spaces
        s-lnreqshipdt  = if oeel.reqshipdt ne ? then
                             string(oeel.reqshipdt,"99/99/99")
                         else v-8spaces
        s-specnstype   = string(oeel.specnstype,"x(1)")
        s-serlottype   = if avail icsw and icsw.serlottype <> "" then
                             icsw.serlottype
                         else
                             v-1spaces.

    /*tb 24765 05/28/98 kjb; Add the UPC number to the EDI 810 out file */
    do for icsv:

        {icsv.gfi "'u'" oeel.shipprod oeel.vendno no}

        assign
            s-upcno1      = if avail icsv then
                                string(icsv.section1,"999999999999")
                            else v-12spaces
            s-upcno2      = if avail icsv then
                                string(icsv.section2,"999999999999")
                            else v-12spaces
            s-upcno3      = if avail icsv then
                                string(icsv.section3,"999999999999")
                            else v-12spaces
            s-upcno4      = if avail icsv then
                                string(icsv.section4,"999999999999")
                            else v-12spaces
            s-upcno5      = if avail icsv then
                                string(icsv.section5,"999999999999")
                            else v-12spaces
            s-upcno6      = if avail icsv then
                                string(icsv.section6,"999999999999")
                            else v-12spaces.

            if avail icsv then do:
                assign

                    {upcno.gco &sectionupc = "*"
                               &sasc       = "v-"
                               &icsv       = "icsv."
                               &upc        = "s-upcno"}.

                s-upcno = fill(" ", 24 - length(s-upcno)) + trim(s-upcno).

            end.
            else do:
                assign
                    s-upcno = fill(" ", 24).
            end.

    end. /* do for icsv */

    /*tb 17353 03/21/95 jlc; Read the cross reference file and
       automatically plug the customer product if one does not
       already exist in s-custprod */
    /*tb 20659 03/06/96 jkp; Removed "oeel.specnstype ne "n" and" from the if
       statement, so ICSEC will also be checked for customer products for
       non-stocks */
    if s-custprod = "" then
    do:
        /* check cross-ref for customer product */
        find first icsec use-index k-altprod where
            icsec.cono = g-cono  and
            icsec.rectype = "c"  and
            icsec.altprod = s-shipprod  and
            icsec.custno = oeeh.custno
        no-lock no-error.
        if avail icsec then
            assign
            s-custprod = string(icsec.prod,"x(24)").
    end.

    /*o Get the Product/Service Identifier that is stored in the icsp
        Product Description field */

    /*tb 13659 11/11/93 jlc; Add requested product   ***********
    v-prod  =   if v-icsecfl = yes then                        *
                    s-custprod                                 *
                else                                           *
                    s-shipprod.                                *
    ***********************************************************/

    {w-icsp.i s-shipprod no-lock}

    /*tb  7241 (5.1) 03/02/97 jkp; Added find of icss record. */
    {icss.gfi
        &prod        = oeel.shipprod
        &icspecrecno = oeel.icspecrecno
        &lock        = "no"}

    /*tb 18581 05/31/95 jkp; Add special cost fields */
    /*tb  7241 (5.1) 03/02/97 jkp; Changed the use of icsp to the use
         of icss for the find of the sasta record. */
    if avail icss and icss.prccostper ne "" then do:
        {w-sasta.i "u" icss.prccostper no-lock}
        s-specuom       =   if avail sasta then
                                string(sasta.unitediuom,"xx")
                            else
                                string("zz","xx").
    end.

    /*tb  7241 (5.1) 03/02/97 jkp; Changed the use of icsp to the use
         of icss for the assign of the s-speccostty and s-speccostunit
         variables. */
    assign
        s-prodcd        =   if avail icsp then
                                string(icsp.edicd,"xx")
                            else
                                v-2spaces
        s-speccostty    =   if avail icss then
                                string(icss.speccostty,"x")
                            else
                                v-1spaces
        /* tb e17235 changed format from 7.5 to 6.6 */
        s-speccostunit  =   if avail icss then
                                string(icss.csunperstk,"-999999.999999")
                            else
                                v-14spaces
        /* TB e17268 added qty uom and price uom, I took the edi prc unit from
            oeepi1l.p*/
        s-ediprcunit    = if avail icss and icss.prccostper <> ""
                              then string(icss.prccostper,"x(4)")
                          else string(oeel.unit,"x(4)")
        s-ediqtyunit    = string(oeel.unit,"x(4)").

    /*d User hook to alter data stored in variables for the flat file */
    {oeepiel.z99 &user_variables = "*"}

    /*o Write an acknowledgement record to the 855 flat file */
    /*tb 15205 02/23/94 jlc; process the edi 855 flat file */

    if v-prt855fl and v-oeepie99fl then
    do:
        g-errfl = no.
        run oeepie99.p("ab").
        if g-errfl then next.
    end.