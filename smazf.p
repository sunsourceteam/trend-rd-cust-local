{g-all.i}
{g-cm.i}
{g-setup.i}

def var v-slsrep        like g-slsrep           no-undo.
def var v-type          as character format "xx".
/* def var v-key           as character format "x(30)".  */

def var v-year          as integer   format "9999".
def var v-directory     as character format "x" no-undo.
def var neg             as character format "x" extent 12 no-undo.

form {f-smazf.i} title g-title.

assign v-slsrep = g-slsrep 
       v-type   = "SR".
main:

do for zsmsq while true on endkey undo main, leave main:
  {p-setup.i f-smazf}
  disable smsn.name with frame f-smazf.
  update /* v-type 
           validate (can-do("sr",v-type),"Valid Type(s) are 'SR'") */
        /* v-key   */
         s-slsrep
         v-year     
         with frame f-smazf.
  assign
    g-slsrep = s-slsrep.
   
  hide message no-pause.
 
  find smsn where smsn.cono = g-cono and 
                  smsn.slsrep = g-slsrep no-lock no-error.
  if avail smsn then
    display smsn.name with frame f-smazf.
    
  
  if g-secure = 2 then
    do:
    {w-zsmsq.i v-year v-type s-slsrep no-lock}
    if not avail zsmsq then
      do:
      run err.p (1108).
      next main.
      end.
    else
      do:
      display {u-zsmsq.i} with frame f-smazf.
      run err.p (1107).
      pause.
      end.
    end.
  else
   do transaction on error undo main, next main:
   {w-zsmsq.i v-year v-type s-slsrep exclusive-lock}
   if not avail zsmsq then
     do:
     {p-addrec.i}
     if {k-create.i} then
       do:
       create zsmsq.
       assign zsmsq.cono = g-cono
              zsmsq.year = v-year
              zsmsq.divno = 0
              zsmsq.quotaty = v-type
              zsmsq.primarykey = s-slsrep.


       end.
     else
       do:
       next.
       end.
     end.
     updt:
     do on endkey undo main, next main:
       update {u-zsmsq.i} {g-del.i} with frame f-smazf.
     end.
        
     if {k-delete.i} then
       do:
       {p-delete.i} 
         do:
         delete zsmsq.
         end.
       end.
   end.
 end.
{j-all.i "frame f-smazf"}.         
       
       
 
