
/{&user_temptable}*/

/*d Preprocessor Directives */
{porbx.lpp}

{g-porzc.i new}
{porzc.lva}

{zsapboydef.i}

/*d Temp table containing the Whses that are selected */
def temp-table t-whselist no-undo
    field whse      like icsd.whse
    index k-whse is primary unique
        whse        ascending.

def var v-transtype     like poeh.transtype     no-undo.
def var gota-hit as logical                     no-undo.
def var ordtyp          like poelo.ordertype    no-undo.
def var vendsw   as logical                     no-undo.
def var d-entitys       as integer initial 0   no-undo.
def var xinx            as integer initial 0   no-undo.
def var prtcntr         as integer             no-undo.
def var firstfl       as logical    init yes     no-undo.
def var sassr-pg      as int                     no-undo.
def var page-length   as int                     no-undo.
def var p-regval   as char                      no-undo.
def buffer z-oimsp for oimsp.
define new shared var p-n-codes as logical init no no-undo.
define new shared var p-excl-r  as char format "x"          no-undo.
define new shared var p-excl-x  as char format "x"          no-undo.
define new shared var p-excl-n  as char format "x"          no-undo.
define new shared var p-excl-s  as char format "x"          no-undo.
define new shared var p-valids  as char format "x(30)"      no-undo.
define new shared var p-delim   as char format "x" init "," no-undo.
define new shared var idx1      as integer initial 0   no-undo.
define new shared var p-blnk-buyer as logical               no-undo.

/{&user_temptable}*/

/{&user_drpttable}*/
   field zqrecid   as recid
   FIELD porecid   as recid
   FIELD buyer     like poeh.buyer
   FIELD vendno    like poeh.vendno
/{&user_drpttable}*/

/{&user_forms}*/

{f-porzc.i}

form header
  "~015 " at 1
  with frame fx-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: porzc"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 168
  page-number(xpcd)    at 173 format ">>>9"
  "~015"               at 177
         "Po Exepdite Report" at 71
         "~015"              at 177
   with frame fx-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.

form header
  "~015 " at 1
  with frame f-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: porzc"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 168
  page-number          at 173 format ">>>9"
  "~015"              at 177
  "Po Expedite Report" at 72
  "~015"              at 177
   with frame f-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.

form header 
   "~015"                    at 177
  with frame f-trn3 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.

form
  v-final                at 1
  v-astrik               at 2   format "x(5)"
  v-summary-lit2         at 7   format "x(27)"
  "Total Cost of Items " at 38 
  v-amt1                 at 58  format ">,>>>,>>9-"
  "Total Po Lines "      at 69
  v-amt2                 at 84  format ">>>,>>9-"
  "~015"           at 178
with frame f-tot width 178
 no-box no-labels.  

form 
  "~015"           at 1
with frame f-skip width 178 no-box no-labels.  

form header
  " Note: ""p"" after ""Exp Ship Dt"" denotes a partial receipt " at 1 
  "~015"           at 170
  with frame f-legend width 178 no-label no-box page-bottom.
  
/{&user_forms}*/

/{&user_extractrun}*/
 run sapb_vars.
 run formatoptions.
 run extract_data.
 if p-faxopt = "f" or p-faxopt = "p" then 
 do: 
    if p-faxopt = "f" then
    do:
        message "Fax Option Chosen - Faxing Po's now...". 
        run porzc_b.p(input p-faxopt).
    end.
    else
    if p-faxopt = "p" then  
    do: 
        message "Print Option Chosen - Printing Po's on printer now...". 
        run porzc_b.p(input p-faxopt).
    end.
    else 
    do:
        message "Report Option Chosen - Expediting Report Printing...".  
    end.
 end.
 if p-faxopt ne "r" then 
 do:
    return.
 end.

/{&user_extractrun}*/

/{&user_optiontype}*/

  if 
  not    /* The option codes are application specific */
  can-do("B,V,W,D,O,P,F",substring(p-optiontype,v-inx,1))
   then
    if not x-stream then 
    display 
 "Invalid option selection valid entries B,V,W,D,O,P,F"
          substring(p-optiontype,v-inx,1).
 else
    display stream xpcd
 "Invalid option selection valid entries B,V,W,D,O,P,F"
          substring(p-optiontype,v-inx,1).
          
/{&user_optiontype}*/

/{&user_registertype}*/

  if not
  can-do("B,V,W,D,O,P,F",substring(p-register,v-inx,1))
   then
    if not x-stream then 
       display 
         "Invalid option selection valid entries B,V,W,D,O,P,F"
              substring(p-register,v-inx,1).
     else
       display stream xpcd
         "Invalid option selection valid entries B,V,W,D,O,P,F"
              substring(p-register,v-inx,1).

/{&user_registertype}*/

/{&user_exportvarheaders1}*/

   if v-lookup[v-inx4] = "W" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Warehouse".
    
    end. 
   else 
   if v-lookup[v-inx4] = "C" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Customer".
     assign export_rec = export_rec + v-del + "Customer".
/* Customer has two columns the sales rep + name */
    
    end. 
   else 
   if v-lookup[v-inx4] = "B" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Oe Inv/WT Ship Date".
    end. 
   else
   if v-lookup[v-inx4] = "S" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Sales Region".
    end. 
   else
   if v-lookup[v-inx4] = "D" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "District".
    end. 
   else
   if v-lookup[v-inx4] = "G" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Sales Rep Group".
    end. 

   else
   if v-lookup[v-inx4] = "I" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Sales Rep In".
     assign export_rec = export_rec + v-del + "Sales Rep In".
    end. 

   else
   if v-lookup[v-inx4] = "O" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Sales Rep Out".
     assign export_rec = export_rec + v-del + "Sales Rep Out".
    end. 

   else
   if v-lookup[v-inx4] = "P" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Product Category".
    end. 
   else 
   if v-lookup[v-inx4] = "T" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "TakenBy".
    end. 

/{&user_exportvarheaders1}*/

/{&user_exportvarheaders2}*/
/{&user_exportvarheaders2}*/

/{&user_exportstatheaders}*/
/{&user_exportstatheaders}*/


/{&user_foreach}*/
 for each t-porzc no-lock:
/{&user_foreach}*/

/{&user_loadtokens}*/

     if v-lookup[v-inx] = "B" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
       assign v-token =
                       caps(string(t-porzc.buyer,"x(4)")).
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(4)").
       end.
     else
      
     if v-lookup[v-inx] = "W" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
       assign v-token =
                       caps(string(t-porzc.whse,"x(4)")).
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(4)").
       end.
     else
     if v-lookup[v-inx] = "o" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
       assign v-token =
                       caps(string(t-porzc.pono,"9999999")
                                   + "-" +
                            string(t-porzc.posuf,"99")
                                   + " " +
                            string(t-porzc.lineno,"999"))
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(14)").
       end.
     else
     if v-lookup[v-inx] = "d" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
       assign v-token =
                       caps(string(year(t-porzc.duedt),"9999")
                               +
                            string(month(t-porzc.duedt),"99")
                               + 
                            string(day(t-porzc.duedt),"99"))
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(8)").
       end.
     else
     if v-lookup[v-inx] = "p" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
       assign v-token = caps(string(t-porzc.shipprod,"x(24)")).
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(24)").
       end.
     else 
     if v-lookup[v-inx] = "V" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
       assign v-token = caps(string(t-porzc.vendno,"999999999999"))
                
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" and
          v-summup[v-inx] <> "#" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(12)").
       end.
     else 
     if v-lookup[v-inx] = "F" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
       assign v-token = caps(string(t-porzc.shipfmno,"9999"))
                
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" and
          v-summup[v-inx] <> "#" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(12)").
       end.
     else  
     if v-lookup[v-inx] = "x" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
         v-token =  string(recid(t-porzc),"999999999999").
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       v-token.
       end.
  
/{&user_loadtokens}*/

/{&user_totaladd}*/

  assign t-amounts[1] = t-porzc.lnprc
         t-amounts[2] = t-porzc.totlns.
                                  
/{&user_totaladd}*/

/{&user_drptassign}*/
   drpt.zqrecid = recid(t-porzc)
   drpt.vendno  = t-porzc.vendno
   drpt.buyer   = t-porzc.buyer
/{&user_drptassign}*/

/{&user_viewheadframes}*/

if not p-exportl then
  do:
  if not x-stream then
    do:
    hide frame f-trn1.
    hide frame f-trn2.
    hide frame f-trn3.
    hide frame f-trn4.
    hide frame f-trn5.
    end.
  else
   do:
    hide stream xpcd frame f-trn1.
    hide stream xpcd frame fx-trn2.
    hide stream xpcd frame f-trn3.
    hide stream xpcd frame f-trn4.
    hide stream xpcd frame f-trn5.
    end.
  end.

if not p-exportl  then
  do:
  if not x-stream then
    do:
    page.
    view frame f-trn1.
    view frame f-trn2.
    view frame f-trn3.
    view frame f-legend.
    end.
  else  
    do:
    page stream xpcd.
    view stream xpcd frame f-trn1.
    view stream xpcd frame fx-trn2.
    view stream xpcd frame f-trn3.
    end.
  end.

/{&user_viewheadframes}*/

/{&user_drptloop}*/
   /*  buyer print  */
   form header
       "~015"              at 1
       s-buybrk            at 3
       "Buyer :"           at 4
       s-buyer             at 12
       "Name:"             at 39
       s-buyernm           at 45
       s-buyerph           at 70
       s-buyerfx           at 89
       "~015"              at 159
       "~015"              at 159
    with frame f-porzc2c no-box no-labels page-top width 175.
    view frame f-porzc2c.
   /*  column headings for product detail lines  */
   form header
       "Product/"              at 4
       "Ship To"               at 28
       "Unit"                  at 37
       "Net Avail"             at 47
       "Qty Ord"               at 61
       "PO Date"               at 72
       "PO #"                  at 89
       "Exp Ship Dt"           at 99
       "Notes / Comments "     at 128
       "~015"                  at 159
       "Description"           at 7
       "Due Date"              at 72
       "Rush/Type"             at 87
       "Last Ack Dt"           at 99
       "~015"                  at 159
"-------------------------------------------------------------------------------
-------------------------------------------------------------------------------~-------------------"           at 1 
       "~015"                  at 159
   with frame f-porzc4 no-box no-labels page-top width 200.
   vendsw = no.
   if v-holdsortmystery = "xxxxxxxxxxxxxx" then
    do:
     assign  v-holdsortmystery = drpt.sortmystery.
     run buyer_header(input drpt.buyer).
    end.
  
/{&user_drptloop}*/

/{&user_detailprint}*/
  /** if not = all others only - print only items we choose to see */

  find t-porzc where recid(t-porzc) = drpt.zqrecid
       no-lock no-error.

  /* application specific */
 /* to refresh variables in header- the form needs to be defined in the loop
    that is printing it */
 if not p-exportl then
  do:
    if founddata then
       view frame f-porzc4.
    run detail_print (input drpt.zqrecid).
  end.
 assign founddata = true.
 hide frame f-porzc4.
/{&user_detailprint}*/

/{&user_finalprint}*/
display with frame f-blank.
v-summary-lit2 = " Final".
v-summary-val = " ".
/* v-summary-amount = t-amounts[u-entitys + 1]. */
if not p-exportl then
 do:
 if not x-stream then 
   do:
   display 
    v-final
    v-astrik
    v-summary-lit2
/* PTD */
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts2[u-entitys + 1]   @ v-amt2
/*
    t-amounts5[u-entitys + 1]   @ v-amt3
    t-amounts7[u-entitys + 1]   @ v-amt4
    v-per1 @ v-amt5
/* Obsolete */  
    t-amounts3[u-entitys + 1]   @ v-amt6
    t-amounts4[u-entitys + 1]   @ v-amt7
    t-amounts6[u-entitys + 1]   @ v-amt8
    t-amounts8[u-entitys + 1]   @ v-amt9
    t-amounts10[u-entitys + 1]   @ v-amt10
    t-amounts12[u-entitys + 1]   @ v-amt11
    t-amounts14[u-entitys + 1]   @ v-amt12
*/  
    with frame f-tot.
  
   down with frame f-tot.
   end.
 else
   do:
   display stream xpcd
    v-final
    v-astrik
    v-summary-lit2
/* PTD */
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts2[u-entitys + 1]   @ v-amt2
/*
    t-amounts5[u-entitys + 1]   @ v-amt3
    t-amounts7[u-entitys + 1]   @ v-amt4
    v-per1 @ v-amt5
/* Obsolete */  
    t-amounts3[u-entitys + 1]   @ v-amt6
    t-amounts4[u-entitys + 1]   @ v-amt7
    t-amounts6[u-entitys + 1]   @ v-amt8
    t-amounts8[u-entitys + 1]   @ v-amt9
    t-amounts10[u-entitys + 1]   @ v-amt10
    t-amounts12[u-entitys + 1]   @ v-amt11
    t-amounts14[u-entitys + 1]   @ v-amt12
*/
    with frame f-tot.
   down stream xpcd with frame f-tot.
   end.
 end.
/{&user_finalprint}*/

/{&user_numbertotals}*/

         /* Here the amount     */  if v-subtotalup[v-inx4] = 1 then
         /* that is to be       */   assign v-val = t-amounts1[v-inx4].
         /* accumulated for the */   
                                    else
         /* subtotal sort is    */  if v-subtotalup[v-inx4] = 2 then
                                      assign v-val = t-amounts2[v-inx4].
         /* determined by param */  
         /* this gives power to */  else
         /* sort on any amount  */  if v-subtotalup[v-inx4] = 3 then
         /* on the report at    */    assign v-val = t-amounts3[v-inx4].
         /* any level.          */  else
                                    if v-subtotalup[v-inx4] = 4 then
                                      assign v-val = t-amounts4[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 5 then
                                      assign v-val = t-amounts5[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 6 then
                                      assign v-val =  t-amounts6[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 7 then
                                      assign v-val = t-amounts7[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 8 then
                                      assign v-val = t-amounts8[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 9 then
                                      assign v-val = t-amounts9[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 10 then
                                      assign v-val = t-amounts10[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 11 then
                                      assign v-val = t-amounts11[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 12 then
                                      assign v-val = t-amounts12[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 13 then
                                      assign v-val = t-amounts13[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 14 then
                                      assign v-val = t-amounts14[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 15 then
                                      assign v-val = t-amounts15[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 16 then
                                      assign v-val = t-amounts16[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 17 then
                                      assign v-val = t-amounts17[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 18 then
                                      assign v-val = t-amounts18[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 19 then
                                      assign v-val = t-amounts19[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 20 then
                                      assign v-val = t-amounts20[v-inx4].
                                    else
                                      v-val = t-amounts1[v-inx4].              

                                    do inz = 1 to 20:
         /* The registers need  */  if inz = 1 then
         /* to be updated for   */    v-regval[inz] = t-amounts1[v-inx4].     
         /* accumulation of the */  else
         /* totals that can be  */  if inz = 2 then
                                      v-regval[inz] = t-amounts2[v-inx4].      
         /* controlled by the   */         
         /* parameters          */  else
         /*                     */  if inz = 3 then
         /*                     */    v-regval[inz] = t-amounts3[v-inx4].      
         /*                     */  else
                                    if inz = 4 then
                                      v-regval[inz] = t-amounts4[v-inx4].
                                    else
                                    if inz = 5 then
                                      v-regval[inz] = t-amounts5[v-inx4].   
                                    else
                                    if inz = 6 then
                                      v-regval[inz] = t-amounts6[v-inx4].    
                                    else
                                    if inz = 7 then
                                      v-regval[inz] = t-amounts7[v-inx4]. 
                                    else
                                    if inz = 8 then
                                      v-regval[inz] = t-amounts8[v-inx4].      
                                    else
                                    if inz = 9 then
                                      v-regval[inz] = t-amounts9[v-inx4].      
                                    else
                                    if inz = 10 then
                                      v-regval[inz] = t-amounts10[v-inx4].     
                                    else
                                    if inz = 11 then
                                      v-regval[inz] = t-amounts11[v-inx4].     
                                    else
                                    if inz = 12 then
                                      v-regval[inz] = t-amounts12[v-inx4].     
                                    else
                                    if inz = 13 then
                                      v-regval[inz] = t-amounts13[v-inx4].     
                                    else
                                    if inz = 14 then
                                      v-regval[inz] = t-amounts14[v-inx4].     
                                    else
                                    if inz = 15 then
                                      v-regval[inz] = t-amounts15[v-inx4].     
                                    else
                                    if inz = 16 then
                                      v-regval[inz] = t-amounts16[v-inx4].     
                                    else
                                    if inz = 17 then
                                      v-regval[inz] = t-amounts17[v-inx4].     
                                    else
                                    if inz = 18 then
                                      v-regval[inz] = t-amounts18[v-inx4].     
                                    else
                                    if inz = 19 then
                                      v-regval[inz] = t-amounts19[v-inx4].     
                                    else
                                    if inz = 20 then
                                      v-regval[inz] = t-amounts20[v-inx4].     
                                    end.

 
/{&user_numbertotals}*/

/{&user_summaryloadprint}*/

  assign d-entitys = if p-detail = "d" then
                       u-entitys - 1 
                     else 
                       u-entitys.
  
  h-genlit = "".
  v-summary-lit2 = "".

  if v-lookup[v-inx2] = "w" then
     do:
     if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others".
      if v-inx2 = u-entitys then
       assign v-summary-lit2 = h-lookup + " " + h-genlit.
     else  
       assign v-summary-lit2 = "Whse - " + h-lookup + " " + h-genlit.
     
     end. 
  else
  if v-lookup[v-inx2] = "v" then
    do:
    find apsv where apsv.cono = g-cono
         and apsv.vendno = dec(h-lookup) no-lock no-error.
     assign h-lookup = string(dec(h-lookup),">>>>>>>>>>>9") + " " +

                       (if avail apsv then apsv.lookupnm
                        else "No APSV Found").
    
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
       assign v-summary-lit2 = h-lookup.
    else
      assign v-summary-lit2 = " V# - " +
                            h-lookup.
    hide frame f-porzc4.
    display with frame f-skip.
    end.
  else
  if v-lookup[v-inx2] = "f" then
    do:
    if int(h-lookup) > 0 then 
     do:
      find apss where apss.cono = g-cono
           and apss.vendno   = drpt.vendno 
           and apss.shipfmno = int(h-lookup) no-lock no-error.
     
      assign h-lookup =  string(int(h-lookup),"zzz9") + " " +
                         (if avail apss then 
                             ""  
                          else
                             "No APSS Found").
      end.
     else
      assign h-lookup =  string(int(h-lookup),"zzz9").
           
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
       assign v-summary-lit2 = h-lookup.
    else
      assign v-summary-lit2 = " Ship From# - " +
                            h-lookup.
    display with frame f-skip.
    end.
  else
  if v-lookup[v-inx2] = "D" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
      assign v-summary-lit2 = h-lookup.
    else
      assign v-summary-lit2 = " Date - " +
                            h-lookup.
    end.
  else
  if v-lookup[v-inx2] = "b" then
    do:
       find sasta use-index k-sasta where sasta.cono = g-cono
         and sasta.codeiden = "B"
         and sasta.codeval = h-lookup no-lock no-error.
     assign h-lookup = h-lookup + " " +
                       (if avail sasta then sasta.descrip
                        else "No BUYER Descrip").
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
       assign v-summary-lit2 = h-lookup.
    else
      assign v-summary-lit2 = "Buyer - " +
                            h-lookup.
    firstfl = yes.
    run buyer_header(input entry(v-inx2,v-mystery,v-delim)).
    end.
  else
  if v-lookup[v-inx2] = "p" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
      assign v-summary-lit2 = h-lookup.
    else
      assign v-summary-lit2 = " Product - " +
                            h-lookup.
    end.
  else
  if v-lookup[v-inx2] = "o" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
      assign v-summary-lit2 = h-lookup.
    else
      assign v-summary-lit2 = " Order - " +
                            h-lookup.
    end.
  
  else
     v-summary-lit = v-lookup[v-inx2].
  
  if firstbreak and v-totalup[v-inx2] <> "s" then
    do:
    firstbreak = false.
    end.
  
  /* application specific */
  if v-lookup[v-inx] = "b" then 
    do: 
     assign firstfl = yes.
     hide frame f-porzc4.
    end.
  else 
  /* application specific */
  if v-lookup[v-inx] = "v" or
     v-lookup[v-inx] = "f" then 
     assign vendsw = yes.

 /{&user_summaryloadprint}*/

/{&user_summaryframeprint}*/
  if v-totalup[v-inx2] <> "s" then
    do:
/* assign percents  */

    if not x-stream then
      do:
      display 
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
/*
        t-amounts5[v-inx2]   @ v-amt3
        t-amounts7[v-inx2]   @ v-amt4
        v-per1 @ v-amt5
    /* Obsolete */  
        t-amounts3[v-inx2]   @ v-amt6
        t-amounts4[v-inx2]   @ v-amt7
        t-amounts6[v-inx2]   @ v-amt8
        t-amounts8[v-inx2]   @ v-amt9
        t-amounts10[v-inx2]   @ v-amt10
        t-amounts12[v-inx2]   @ v-amt11
        t-amounts14[v-inx2]  @ v-amt12
*/
        with frame f-tot.
      down with frame f-tot.
      end.
    else
      do:
      display stream xpcd
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
/*
        t-amounts5[v-inx2]   @ v-amt3
        t-amounts7[v-inx2]   @ v-amt4
        v-per1 @ v-amt5
    /* Obsolete */  
        t-amounts3[v-inx2]   @ v-amt6
        t-amounts4[v-inx2]   @ v-amt7
        t-amounts6[v-inx2]   @ v-amt8
        t-amounts8[v-inx2]   @ v-amt9
        t-amounts10[v-inx2]   @ v-amt10
        t-amounts12[v-inx2]   @ v-amt11
        t-amounts14[v-inx2]  @ v-amt12
*/
        with frame f-tot.
      down stream xpcd with frame f-tot.
      end.
      
    end.
      
 if v-totalup[v-inx2] = "p" then
   if not x-stream then
     page.
   else
     page stream xpcd.
     
 if v-totalup[v-inx2] = "l" then
   if not x-stream then
     display with frame f-skip.
   else
     display stream xpcd with frame f-skip.

 /{&user_summaryframeprint}*/

/{&user_summaryloadexport}*/
/{&user_summaryloadexport}*/

/{&user_summaryputexport}*/
/{&user_summaryputexport}*/

/{&user_procedures}*/

/**-----------------Begin Procedure Section-------------------------------**/
procedure sapb_vars:
/**-----------------------------------------------------------------------**/

/* LOAD PARAMETERS FROM SAPB */
assign
    b-buyer       = substring(sapb.rangebeg[1],1,4)
    e-buyer       = substring(sapb.rangeend[1],1,4)
    b-vendno      = decimal(sapb.rangebeg[2])
    e-vendno      = decimal(sapb.rangeend[2])
    b-whse        = sapb.rangebeg[3]
    e-whse        = sapb.rangeend[3]
    b-prodline    = substring(sapb.rangebeg[4],1,6)
    e-prodline    = substring(sapb.rangeend[4],1,6)
    b-lnduedt     = date(sapb.rangebeg[5])
    e-lnduedt     = date(sapb.rangeend[5])
    b-ordtype     = sapb.rangebeg[6]
    e-ordtype     = sapb.rangeend[6]
    b-oimsp       = sapb.rangebeg[7]
    e-oimsp       = sapb.rangeend[7]
    b-poentdt     = date(sapb.rangebeg[8])
    e-poentdt     = date(sapb.rangeend[8])
    b-product     = sapb.rangebeg[9]
    e-product     = sapb.rangeend[9]
    p-latedays    = decimal(sapb.optvalue[1])
    p-dueorexp    = sapb.optvalue[2]
    p-notltbo     = if sapb.optvalue[3] = "yes" then yes else no
    p-onlybo      = if sapb.optvalue[4] = "yes" then yes else no
    p-prtbos      = if sapb.optvalue[5] = "yes" then yes else no
    p-rushfl      = if sapb.optvalue[6] = "yes" then yes else no
    p-sortby      = sapb.optvalue[7]
    p-faxopt      = sapb.optvalue[10] 
    p-list        = if sapb.optvalue[11] = "yes" then true else false.

assign p-excl-n  = sapb.optvalue[12]
       p-excl-r  = sapb.optvalue[13]
       p-excl-x  = sapb.optvalue[14]
       p-excl-s  = sapb.optvalue[15]
       p-blnk-buyer = if sapb.optvalue[16] = "yes" then 
                         true 
                      else 
                         false.               

assign p-valids = "".
if p-excl-n = "i" then 
   assign p-valids = p-valids + "n, ,".
else     
   assign p-valids = p-valids + "~011" + "," + "~011" + ",".
if p-excl-r = "i" then 
   assign p-valids = p-valids + "r,". 
else 
   assign p-valids = p-valids + "~011" + ",". 
if p-excl-x = "i" then 
   assign p-valids = p-valids + "x,". 
else 
   assign p-valids = p-valids + "~011" + ",". 
if p-excl-s = "i" then 
   assign p-valids = p-valids + "s,".
else 
   assign p-valids = p-valids + "~011" + ",". 
/* below always include "a"  types */
assign p-valids = p-valids + "a,".    
 
assign zzl_Prodbeg  = b-product
       zzl_Prodend  = e-product
       zzl_begbuy   = b-buyer
       zzl_endbuy   = e-buyer
       zzl_begvend  = b-vendno
       zzl_endvend  = e-vendno
       zzl_begwhse  = b-whse
       zzl_endwhse  = e-whse.
assign zel_Prodbeg  = b-product
       zel_Prodend  = e-product
       zel_begbuy   = b-buyer
       zel_endbuy   = e-buyer
       zel_begvend  = b-vendno
       zel_endvend  = e-vendno
       zel_begwhse  = b-whse
       zel_endwhse  = e-whse.
if p-list then
 do:
  assign zel_Prodbeg  = b-product
         zel_Prodend  = e-product
         zel_begbuy   = b-buyer
         zel_endbuy   = e-buyer
         zel_begvend  = b-vendno
         zel_endvend  = e-vendno
         zel_begwhse  = b-whse
         zel_endwhse  = e-whse.
  {zsapboyload.i}

 end.

 assign zelection_matrix [4] = (if zelection_matrix[4] > 1 then /* Buyer */
                                  zelection_matrix[4]
                                 else   
                                   1)
         zelection_matrix [7] = (if zelection_matrix[7] > 1 then  /* Vend */
                                  zelection_matrix[7]
                                 else   
                                  1)
         zelection_matrix [6] = (if zelection_matrix[6] > 1 then  /* Whse */
                                  zelection_matrix[6]
                                 else   
                                  1)
         zelection_matrix [11] = (if zelection_matrix[11] > 1 then /* Product */
                                 zelection_matrix[11]
                              else   
                                 1).

/*** if always printing detail then p-detail must be a "d" ***/
p-detail = "d".
if b-whse = "" then 
   b-whse = "   ".
if e-whse = "" then 
   e-whse = "~~~~".

/* ------ Begin of Parameter Handling and initialization -------- */

find first sassr where
           sassr.currproc = "porzc" and 
           sassr.reportid = "porzc"
     no-lock no-error.
 if avail sassr then 
   assign sassr-pg = (sassr.pglength - 8).

if sapb.rangebeg[5] ne "" then
    do:
    v-datein = sapb.rangebeg[5].
    {p-rptdt.i}
    if string(v-dateout) = v-lowdt then
      b-lnduedt = 01/01/1900.
    else  
      b-lnduedt = v-dateout.
    end.
else
  b-lnduedt = 01/01/1900.

if sapb.rangeend[5] ne "" then
    do:
    v-datein = sapb.rangeend[5].
    {p-rptdt.i}
    if string(v-dateout) = v-highdt then
      e-lnduedt = 12/31/2049.
    else
      e-lnduedt = v-dateout.
    end.
else
  e-lnduedt = 12/31/2049.

if sapb.rangebeg[8] ne "" then
    do:
    v-datein = sapb.rangebeg[8].
    {p-rptdt.i}
    if string(v-dateout) = v-lowdt then
      b-poentdt = 01/01/1900.
    else  
      b-poentdt = v-dateout.
    end.
else
  b-poentdt = 01/01/1900.

if sapb.rangeend[8] ne "" then
    do:
    v-datein = sapb.rangeend[8].
    {p-rptdt.i}
    if string(v-dateout) = v-highdt then
      e-poentdt = 12/31/2049.
    else
      e-poentdt = v-dateout.
    end.
else
  e-poentdt = 12/31/2049.

end. /* end of sapb_vars procedure */

/**-------------------------------------------------------------------------**/
procedure formatoptions: 
/**-------------------------------------------------------------------------**/

 if sapb.optvalue[7] > "0" and sapb.optvalue[7] ne "99" then 
 do:
  p-optiontype = " ".
  find notes where
       notes.cono = g-cono        and 
       notes.notestype = "zz"     and
       notes.primarykey = "porzc" and
       notes.secondarykey = sapb.optvalue[7]
       no-lock no-error.
  if not avail notes then
   do:
    message "Invalid Sort Format Type for porzc " " --> " sapb.optvalue[7].
    pause 6.
    return.
   end.
  else
   do:
    p-summcounts = "a,a,a,a,a,a".
    overlay(p-optiontype,1,(length(notes.noteln[1]))) = notes.noteln[1].
    overlay(p-sorttype,1,  (length(notes.noteln[2]))) = notes.noteln[2].
    overlay(p-totaltype,1, (length(notes.noteln[3]))) = notes.noteln[3].
    overlay(p-summcounts,1,(length(notes.noteln[4]))) = notes.noteln[4].
   end.
 end. /* do sapb.optvalue[7] */
 else 
 if sapb.optvalue[7] = "99" then 
   do:
    run reportopts(input sapb.user5, 
                   input-output p-optiontype,
                   input-output p-sorttype,  
                   input-output p-totaltype,
                   input-output p-summcounts,
                   input-output p-register,
                   input-output p-regval).
   end.

end procedure. /* procedure formatoptions */
/**-------------------------------------------------------------------------**/
procedure extract_data: 
/**-------------------------------------------------------------------------**/

/*e Create the list of selected Whse records based on the Whse and Region
    Ranges entered */
run create-whse-list.

/* BEGIN MERGE LOGIC */

/*d If only one Vendor selected for Report */
  if zel_begvend = zel_endvend then 
   do: 
    /*d Run thru Open POs for a single Vendor */
    poehloop1:
    for each poeh use-index k-vendno where
        poeh.cono      = g-cono      and
        poeh.vendno    = zel_begvend and 
       (poeh.stagecd > 1 and poeh.stagecd < 5)                         and
       (can-do("po,ac,do,br",poeh.transtype))                          and 
        poeh.transtype >= b-ordtype  and poeh.transtype <= e-ordtype   and
        poeh.buyer     >= zel_begbuy  and poeh.buyer    <= zel_endbuy  and
        poeh.whse      >= zel_begwhse and poeh.whse     <= zel_endwhse and 
        poeh.enterdt   >= b-poentdt   and poeh.enterdt  <= e-poentdt         
        no-lock:

        /*d Discard BR POs that have not been released */
         if poeh.transtype = "br" and poeh.stagecd = 0 then next poehloop1.
 
         assign zelection_type = "v"
                zelection_vend = poeh.vendno
                zelection_char4 = "".
         run zelectioncheck.
         if zelection_good = false then
            next poehloop1.
         assign zelection_type = "b"
                zelection_char4 = poeh.buyer
                zelection_cust = 0.
         run zelectioncheck.
         if zelection_good = false then
            next poehloop1.
         assign zelection_type = "w"
                zelection_char4 = poeh.whse
                zelection_cust = 0.
         run zelectioncheck.
         if zelection_good = false then
            next poehloop1.

        /*  run create-rush-records only - opt 6  */
        assign s-rushlit = " ".
        if p-rushfl = yes and poeh.rushfl = yes then 
           assign s-rushlit = "Rush/".
        else 
        if p-rushfl = yes and poeh.rushfl = no then 
           next.

        if b-oimsp  ne "" or e-oimsp ne "" then 
         do:
            find first oimsp where
                       oimsp.person    = poeh.buyer  
                      no-lock no-error.
            if avail oimsp then
              do:
              find first oimsp where
                         oimsp.person    = poeh.buyer and 
                         oimsp.responsible >= b-oimsp and 
                         oimsp.responsible <= e-oimsp
                         no-lock no-error.
              if not avail oimsp then      
                 next.
              end.
         end.         
         run process-poel.
    end. /* for each poeh, 1 Vendor */                  
  end. /* if b-vendno = e-vendno */
 else 
 if can-find(t-whselist use-index k-whse where
             t-whselist.whse ne "") then 
  do: 
    /*d Run thru Open POs for a whse and Vendor selection */
    poehloop2:
    for each poeh use-index k-whsevendno where
        poeh.cono      = g-cono     and
        can-find(t-whselist use-index k-whse where
                 t-whselist.whse = poeh.whse) and
        poeh.vendno >= zel_begvend and poeh.vendno <= zel_endvend    and
       (poeh.stagecd > 1 and poeh.stagecd < 5)                       and
       (can-do("po,ac,do,br",poeh.transtype))                        and 
        poeh.transtype >= b-ordtype and poeh.transtype <= e-ordtype  and
        poeh.buyer     >= zel_begbuy and poeh.buyer    <= zel_endbuy and
        poeh.enterdt   >= b-poentdt  and poeh.enterdt  <= e-poentdt         
        no-lock:

        assign zelection_type = "v"
               zelection_vend = poeh.vendno
               zelection_char4 = "".
        run zelectioncheck.
        if zelection_good = false then
           next poehloop2.
        assign zelection_type = "b"
               zelection_char4 = poeh.buyer
               zelection_cust = 0.
        run zelectioncheck.
        if zelection_good = false then
           next poehloop2.
        
        /*d Discard BR POs that have not been released */
          if poeh.transtype = "br" and poeh.stagecd = 0 then next poehloop2.

        /*  run create-rush-records only - opt 6  */
        assign s-rushlit = "  ".
        if p-rushfl = yes and poeh.rushfl = yes then 
          assign s-rushlit = "Rush/".
        else 
        if p-rushfl = yes and poeh.rushfl = no then 
           next.

        if b-oimsp  ne "" or e-oimsp ne "" then 
         do:
            find first oimsp where
                       oimsp.person    = poeh.buyer  
                      no-lock no-error.
            if avail oimsp then
              do:
              find first oimsp where 
                         oimsp.person    = poeh.buyer and 
                         oimsp.responsible >= b-oimsp and 
                         oimsp.responsible <= e-oimsp
                         no-lock no-error.
              if not avail oimsp then      
                 next.
              end.
         end.         
         run process-poel.
    end. /* for each poeh, 1 Vendor */
  end. /* if b-vendno = e-vendno */
 else
  do:
    /*d Loop thru the types of POs that appear on this report */
    do i = 1 to 4:
        v-transtype = if i = 1 then "PO"
                      else
                      if i = 2 then "AC"
                      else
                      if i = 3 then "DO"
                      else "BR".
        /*tb 16474 05/12/95 tdd; Rush orders print even though option = no */
        poehloop3:
        for each poeh use-index k-convert where
            poeh.cono      = g-cono      and
            poeh.transtype = v-transtype and
            poeh.transtype >= b-ordtype  and poeh.transtype <= e-ordtype   and
           (poeh.stagecd > 1 and poeh.stagecd < 5)                         and
            poeh.vendno   >= zel_begvend and poeh.vendno    <= zel_endvend and
            poeh.buyer    >= zel_begbuy  and poeh.buyer     <= zel_endbuy  and
            poeh.whse     >= zel_begwhse and poeh.whse      <= zel_endwhse and 
            poeh.enterdt  >= b-poentdt   and poeh.enterdt   <= e-poentdt       
            no-lock:

            /*d Discard BR POs that have not been released */
            if v-transtype = "br" and poeh.stagecd = 0 then next poehloop3.

            assign zelection_type = "v"
                   zelection_vend = poeh.vendno
                   zelection_char4 = "".
            run zelectioncheck.
            if zelection_good = false then
               next poehloop3.
            assign zelection_type = "b"
                   zelection_char4 = poeh.buyer
                   zelection_cust = 0.
            run zelectioncheck.
            if zelection_good = false then
               next poehloop3.
            assign zelection_type = "w"
                   zelection_char4 = poeh.whse
                   zelection_cust = 0.
            run zelectioncheck.
            if zelection_good = false then
               next poehloop3.
            
            /*  run create-rush-records only - opt 6  */
            assign s-rushlit = " ".
            if p-rushfl = yes and poeh.rushfl = yes then 
               assign s-rushlit = "Rush/".
            else 
            if p-rushfl = yes and poeh.rushfl = no then 
               next.

            if b-oimsp ne "" or e-oimsp ne "" then 
             do:
                find first oimsp where
                           oimsp.person    = poeh.buyer  
                           no-lock no-error.
                if avail oimsp then
                  do:
                  find first oimsp where 
                             oimsp.person    = poeh.buyer and 
                             oimsp.responsible >= b-oimsp and 
                             oimsp.responsible <= e-oimsp
                             no-lock no-error.
                  if not avail oimsp then      
                    next.
                  end.
             end.   
             run process-poel.
        end. /* for each poeh */
    end. /* do i = 1 to 3 */
  end. /* else do */

find last t-porzc use-index k-porbx no-lock no-error.

end procedure.

/*d Create a Temp Table containing the ICSD records based on the Ranges
    entered */
procedure create-whse-list:
    {ipbegin.gpr &proc-name = "create-whse-list"}
    /*d Roll thru all ICSD records and build a Temp Table of ICSD Whses that
        have been selected via the Ranges of this report */
    for each icsd use-index k-icsd where
        icsd.cono   =  g-cono      and
        icsd.whse   >= zel_begwhse and icsd.whse   <= zel_endwhse  
    no-lock:

        assign zelection_type = "w"
               zelection_char4 = icsd.whse
               zelection_cust = 0.
        run zelectioncheck.
        if zelection_good = false then
           next.
        
        create t-whselist.
        t-whselist.whse = icsd.whse.
    end. /* for each icsd */
    {ipend.gpr &proc-name = "create-whse-list"}
end procedure. /* create-whse-list */

/*d Process the POEL records and create any Temp-Table records */
procedure process-poel:

    {ipbegin.gpr &proc-name = "process-poel"}
    def var s-netavail      like icsw.qtyonhand     no-undo.
    def buffer b-icsw  for icsw.
    def buffer b-poel  for poel.

    poelloop:
    for each poel use-index k-poel where
        poel.cono  = g-cono          and
        poel.pono  = poeh.pono       and
        poel.posuf = poeh.posuf      and 
        poel.shipprod >= zel_prodbeg and 
        poel.shipprod <= zel_prodend and 
       (poel.duedt >= b-lnduedt      and 
        poel.duedt <= e-lnduedt)     and 
       (poel.prodline >= b-prodline  and 
        poel.prodline <= e-prodline) and 
        poel.nonstockty ne "l" and 
        poel.statustype = "a"

        no-lock:

       assign zelection_type = "u"
              zelection_char4 = poel.shipprod
              zelection_cust = 0.
       run zelectioncheck.
       if zelection_good = false then
          next.

       if poel.nonstockty = "" then
        do:                    
         {w-icsw.i poel.shipprod poel.whse no-lock}      
         if not avail icsw then next poelloop.           
         s-netavail = icsw.qtyonhand  - icsw.qtyreservd - icsw.qtycommit.

       end. 
       else 
       if poel.nonstockty = "n" then 
          s-netavail = 0.
      
       if poel.nonstockty = "l" then  
          next.
       
       if p-blnk-buyer then 
          if poeh.buyer  = "" then 
             assign gota-hit = yes.
          else 
             next.
       else 
       if not p-blnk-buyer then 
       do:
          assign gota-hit = no
                 idx1     = 1.
          do idx1 = 1 to 6:
             if entry(idx1,p-valids,p-delim) = substring(poel.user1,1,1) then 
                assign gota-hit = yes. 
          end.
          if gota-hit = no then 
             next.
       end. 

      /** checking for icsw B/O situaiton - option 4 
          this option ignores option 1 - days late check **/
      
      if p-onlybo or p-notltbo then 
      do:
       if poel.nonstockty = "n" or
        (avail icsw and                              
        (icsw.qtybo ne 0 or icsw.qtydemand ne 0)) then 
        do: 
          run create-detail(s-netavail).
          next.
        end. 
       else 
       if p-onlybo then
        next.
      end.  

      daysdiff = 0. 
      if p-dueorexp = "d" then 
       do:
         assign daysdiff = poel.duedt - today. 
         
         if daysdiff < 0 and ((daysdiff * -1) >= p-latedays) then 
          /* option 3  checks icsw b/o qty's in conjunction with opt 1 */ 
            do:
             run create-detail(s-netavail).
             next.
            end.
         else 
           next.
       end.
      else  
      if p-dueorexp = "e" then 
       do:
         assign daysdiff = poel.expshipdt - today.
         if daysdiff < 0 and ((daysdiff * -1) >= p-latedays) then 
          /* option 3 checks icsw b/o qty's in conjunction with opt 1 */ 
            do:
             run create-detail(s-netavail).
             next.
            end.
         else 
           next.
       end.

    end. /* for each poel */
end procedure. /* process-poel */

/* Create a Temp Table for any POEL exceptions */
procedure create-detail:

    def input parameter s-netavail      like icsw.qtyonhand no-undo.
    create t-porzc.
    assign
        t-porzc.atyp        = substring(poel.user1,1,1)
        t-porzc.ordtyp      = ordtyp 
        t-porzc.condition   = " "
        t-porzc.buyer       = poel.buyer
        t-porzc.orderdt     = poeh.orderdt
        t-porzc.duedt       = poel.duedt
        t-porzc.expshipdt   = poel.expshipdt 
        t-porzc.lastackdt   = poel.user9
        t-porzc.pono        = poel.pono
        t-porzc.posuf       = poel.posuf
        t-porzc.lineno      = poel.lineno
        t-porzc.shipprod    = poel.shipprod
        t-porzc.whse        = poel.whse
        t-porzc.vendno      = poeh.vendno
        t-porzc.shipfmno    = poel.shipfmno
        t-porzc.netavail    = s-netavail
        t-porzc.stkqtyord   = poel.stkqtyord
        t-porzc.notesfl     = poeh.notesfl
        t-porzc.transtype   = poel.transtype
        t-porzc.nonstockty  = poel.nonstockty
        t-porzc.lnprc       = poel.qtyord * poel.price
        t-porzc.totlns      = 1 
        t-porzc.rushfl      = poeh.rushfl.
    
end procedure. /* create-detail */

procedure buyer_header:
def input parameter hv-buyer as char format "x(4)" no-undo.
   {sasta.gfi "b" hv-buyer no}
   assign  s-buyernm = if avail sasta then sasta.descrip
                       else if hv-buyer = "" then "None Assigned"
                       else v-invalid
           s-buybrk  = "*"
           s-buyer   = hv-buyer.
   firstfl = yes.

find first z-oimsp where
           z-oimsp.person = hv-buyer
     no-lock no-error.
if avail z-oimsp then
 do:
   assign 
   s-buyerph = "TEL:(" + substring(z-oimsp.phoneno,1,3) + ")" +
                         substring(z-oimsp.phoneno,4,3) + "-" + 
                         substring(z-oimsp.phoneno,7,4)
   s-buyerfx = "FAX:(" + substring(z-oimsp.faxphoneno,1,3) + ")" +
                         substring(z-oimsp.faxphoneno,4,3) + "-" +
                         substring(z-oimsp.faxphoneno,7,4).
 end.
else 
  assign s-buyerph = " "
         s-buyerfx = " ".

end procedure.

procedure detail_print:
define input parameter tblrecid as recid.

do: 
   find t-porzc where 
        recid(t-porzc) = tblrecid 
        no-lock no-error.

   if firstfl then 
   do:
    assign vendsw = yes
           firstfl = no.
   end.

   /*d Ship From */
   if vendsw = yes then 
    do:
    {w-apsv.i t-porzc.vendno no-lock}
    if t-porzc.shipfmno > 0 then
       {w-apss.i t-porzc.vendno integer(t-porzc.shipfmno) no-lock}
    assign
          s-vndbrk     = /* if hld-vendno ne t-porzc.vendno then "*" else */
                          ""
          s-shipfm     = int(string(t-porzc.shipfmno))
          s-vendno     = string(t-porzc.vendno)  
                         + (if avail apsv then apsv.notesfl
                            else "")
          s-vendnm     = if t-porzc.shipfmno = 0 then
                            if avail apsv then apsv.name
                            else v-invalid
                            else if avail apss then apss.name
                            else v-invalid
          s-expednm    = if t-porzc.shipfmno = 0 then
                            if avail apsv then apsv.expednm
                            else ""
                            else if avail apss then apss.expednm
                            else ""
          s-addr1      = if t-porzc.shipfmno = 0 then
                            if avail apsv then apsv.addr[1]
                            else ""
                            else if avail apss then apss.addr[1]
                            else ""
          s-exphoneno  = if t-porzc.shipfmno = 0 then
                            if avail apsv then apsv.exphoneno
                            else ""
                            else if avail apss then apss.exphoneno
                            else ""
          s-addr2      = if t-porzc.shipfmno = 0 then
                            if avail apsv then apsv.addr[2]
                            else ""
                            else if avail apss then apss.addr[2]
                            else ""
          s-faxphoneno = if t-porzc.shipfmno = 0 then
                            if avail apsv then apsv.faxphoneno
                            else ""
                            else if avail apss then apss.faxphoneno
                            else ""
          s-city       = if t-porzc.shipfmno = 0 then
                            if avail apsv then apsv.city
                            else ""
                            else if avail apss then apss.city
                            else ""
          s-state      = if t-porzc.shipfmno = 0 then
                            if avail apsv then apsv.state
                            else ""
                            else if avail apss then apss.state
                            else ""
          s-zipcd      = if t-porzc.shipfmno = 0 then
                            if avail apsv then apsv.zipcd
                            else ""
                            else if avail apss then apss.zipcd
                            else "".
     hide frame f-porzc4.
     if line-counter >= sassr-pg then 
       page.
     display  s-vndbrk
              s-vendno
              s-shipfm
              s-vendnm
              s-expednm
              s-addr1
              s-exphoneno
              s-addr2
              s-faxphoneno
              s-city
              s-state
              s-zipcd
              with frame f-porzc3.
              s-vndbrk = "".
     hide frame f-porzc3.
    line-counter = line-counter + 7.
    end.
    
    /*d Print appropriate detail lines for conditions "A" thru "D" */
     if t-porzc.nonstockty = "" then
        {w-icsp.i t-porzc.shipprod no-lock}
     else
        {w-poel.i t-porzc.pono t-porzc.posuf t-porzc.lineno no-lock}

     assign s-prod      = t-porzc.shipprod +
                         (if avail icsp then icsp.notesfl
                          else "")
            s-descrip   = if avail icsp then
                             icsp.descrip[1] + " " + icsp.descrip[2]
                          else if t-porzc.nonstockty = "" then v-invalid
                          else if avail poel then
                               poel.proddesc   + " " + poel.proddesc2
                          else ""
            s-unitstock = if avail icsp then icsp.unitstock
                          else if t-porzc.nonstockty = "" then v-invalid
                          else if avail poel then poel.unit
                          else ""
            s-netavail  = t-porzc.netavail
            s-qtyord    = t-porzc.stkqtyord
            s-dash      = "-"
            s-rushlit   = if t-porzc.rushfl = yes then 
                           "Rush/" 
                          else 
                           "     "
            s-partialfl = ""
            v-brbono    = 0.

      if t-porzc.posuf > 0 then
       do:
         if t-porzc.transtype <> "br" then
          do:
           {w-poel.i t-porzc.pono 0 t-porzc.lineno no-lock}
           if avail poel then
              s-partialfl = if poel.stkqtyord > t-porzc.stkqtyord
                               then "p"
                            else "".
           end.
         else
          do:
            {w-poeh.i t-porzc.pono t-porzc.posuf no-lock}
        /*  if not avail poeh then next reportblk. */
            assign s-partialfl = if poeh.borelfl = yes then "p"
                                      else ""
                   v-brbono    = poeh.brbono.
          end.
       end. /* if t-porbx.posuf > 0 */

        display s-prod
                t-porzc.whse
                s-descrip
                s-unitstock
                t-porzc.reservety
                s-netavail when t-porzc.nonstockty = ""  
                s-qtyord
                t-porzc.orderdt
                t-porzc.pono
                s-dash
                t-porzc.posuf
                t-porzc.notesfl
                t-porzc.expshipdt
                t-porzc.transtype
                t-porzc.lastackdt
                s-descrip
                s-partialfl
                t-porzc.duedt
                t-porzc.atyp
                s-rushlit
                with frame f-porzc6.
                down with frame f-porzc6.
   line-counter = line-counter + 1.
   s-buybrk = "".

   /** print oe/wt/va/wo type backorders if selected option 5 **/
   if p-prtbos then 
   do:
      validprt = no.
      run check_for_bos.
      if validprt = yes then 
       do:
         display with frame f-skip.
         line-counter = line-counter + 2.
       end.
   end. /*  if  p-prtbos  option 5 */
end.
end.

procedure check_for_bos:
do:
 /** check for a valid oe/wt/va/wo B/O situation existing  **/
 if p-prtbos then 
  do:
     for each   poelo use-index k-poelo where
                poelo.cono        = g-cono       and
                poelo.pono        = t-porzc.pono and
                poelo.lineno      = t-porzc.lineno  
                no-lock:
       assign dp-notesfl  = poelo.ordertype.
       if poelo.ordertype = "o" then 
           assign proc-orderno = poelo.orderaltno.
       else
       if poelo.ordertype = "t" then
        do:
          {w-wteh.i poelo.orderaltno poelo.orderaltsuf no-lock}
          {w-wtel.i poelo.orderaltno
                    poelo.orderaltsuf
                    poelo.linealtno no-lock}
          assign dp-reqshipdt = wteh.reqshipdt
                 dp-promisedt = wteh.duedt.
          find last  wtelo use-index k-wtelo where
                     wtelo.cono        = g-cono           and
                     wtelo.wtno        = poelo.orderaltno and
                     wtelo.lineno      = poelo.linealtno
                     no-lock no-error.
          if avail wtelo then 
           do:
             {w-oeeh.i wtelo.orderaltno wtelo.orderaltsuf no-lock} 
             {w-oeel.i wtelo.orderaltno wtelo.orderaltsuf
                       wtelo.linealtno no-lock}
             if avail oeeh or avail oeel then 
                {w-arsc.i oeel.custno no-lock}
           end.
           if avail wtelo and wtelo.ordertype = "f" then 
            do: 
               run drill_deeper.
            end.
           else 
           if avail wtelo and wtelo.ordertype = "o" then 
           do:
            if avail oeel and avail arsc then           
             do:
              assign s-custno     = string(oeel.custno) +
                                   (if avail arsc then arsc.notesfl
                                    else "")    
                     s-custnm     = if avail arsc then
                                     arsc.lookupnm
                                    else 
                                     v-invalid
                     dp-reqshipdt = if avail oeel and 
                                      oeel.reqshipdt ne ? then 
                                      oeel.reqshipdt 
                                    else    
                                      oeeh.reqshipdt 
                     dp-promisedt = if avail oeel and 
                                      oeel.promisedt ne ? then 
                                      oeel.promisedt 
                                    else
                                      oeeh.promisedt. 
              end.
            else 
              assign s-custno = "" 
                     s-custnm = "".
           end.
           assign dp-orderno   = wtel.wtno
                  dp-ordersuf  = wtel.wtsuf
                  dp-lineno    = wtel.lineno
                  dp-qtyord    = wtel.qtyord.
        end.
       else
       if poelo.ordertype = "f" and 
          poelo.linealtno ne 0  then
        do:
          {vaeh.gfi  &vano   = "poelo.orderaltno"
                     &vasuf  = "poelo.orderaltsuf" &lock  = no}
          {vaesl.gfi &vano   = "poelo.orderaltno"             
                     &vasuf  = "poelo.orderaltsuf" 
                     &seqno  = "poelo.seqaltno"
                     &lineno = "poelo.linealtno"
                     &lock   = "no"}
          assign dp-reqshipdt = vaeh.reqshipdt 
                 dp-promisedt = vaeh.promisedt.
          find last  vaelo use-index k-vaelo where
                     vaelo.cono        = g-cono           and
                     vaelo.vano        = poelo.orderaltno 
                     no-lock no-error.
          if avail vaelo then 
           do:
             {w-oeeh.i vaelo.orderaltno vaelo.orderaltsuf no-lock}
             {w-oeel.i vaelo.orderaltno vaelo.orderaltsuf 
                       vaelo.linealtno no-lock}
             if avail oeeh or avail oeel then 
              {w-arsc.i oeeh.custno no-lock}
           end.
          if avail vaelo and vaelo.ordertype = "o" then 
           do:
            if avail oeeh and avail arsc then           
              assign s-custno     = string(oeeh.custno) +
                                   (if avail arsc then arsc.notesfl
                                    else "")    
                     s-custnm     = if avail arsc then arsc.lookupnm
                                    else v-invalid
                     dp-reqshipdt = if avail oeel and 
                                      oeel.reqshipdt ne ? then 
                                      oeel.reqshipdt
                                     else 
                                       oeeh.reqshipdt 
                     dp-promisedt = if avail oeel and 
                                      oeel.promisedt ne ? then 
                                      oeel.promisedt
                                    else 
                                      oeeh.promisedt.
               
            else 
              assign s-custno = "" 
                     s-custnm = "".
           end.
           assign dp-orderno   = vaesl.vano
                  dp-ordersuf  = vaesl.vasuf
                  dp-lineno    = vaesl.lineno
                  dp-qtyord    = vaesl.qtyord.
        end.
       else
       if poelo.ordertype = "w" then 
         do:
           find last  kpet where
                      kpet.cono = 01 and 
                      kpet.wono = poelo.orderaltno  
                      no-lock no-error.
           if avail kpet then 
            do:
             {w-oeel.i kpet.orderaltno kpet.orderaltsuf
                       kpet.linealtno no-lock}
             {w-oeeh.i kpet.orderaltno kpet.orderaltsuf no-lock}
             {w-arsc.i oeel.custno no-lock}
             if avail oeel and avail arsc then           
              assign s-custno     = string(oeel.custno) +
                                   (if avail arsc then arsc.notesfl
                                    else "")    
                     s-custnm     = if avail arsc then arsc.lookupnm
                                    else v-invalid
                     dp-reqshipdt = if oeel.reqshipdt ne ? and avail oeel then 
                                       oeel.reqshipdt 
                                    else 
                                       oeeh.reqshipdt
                     dp-promisedt = if oeel.promisedt ne ? and avail oeel then 
                                       oeel.promisedt 
                                    else 
                                       oeeh.promisedt.
             else 
              assign s-custno = "" 
                     s-custnm = "".
              assign dp-orderno   = kpet.wono
                     dp-ordersuf  = 0
                     dp-lineno    = kpet.linealtno
                     dp-qtyord    = kpet.qtyord.
            end.
         end.
       else 
        leave.  

       if poelo.ordertype = "o" and poelo.seqaltno = 0 then 
        for each oeel use-index k-fill where
                 oeel.cono          = g-cono             and
                 oeel.orderno       = proc-orderno       and 
                 oeel.ordersuf      ge 0                  and 
                 oeel.statustype    = "a"                and
                 oeel.whse          = t-porzc.whse       and
                 oeel.invoicedt     = ?                  and
                 oeel.shipprod      = t-porzc.shipprod   and
                 oeel.bono          = 0                  and
                 oeel.kitfl         = no                 and
                 oeel.tallyfl       = no                 and
                 oeel.returnfl      = no                 and
                 can-do("so,br",oeel.transtype)          and
                 oeel.altwhse       = ""                 and
                 oeel.qtyship       < oeel.qtyord        and
                (oeel.specnstype = "s" or oeel.specnstype = "n")
                 no-lock:    
           {w-arsc.i oeel.custno no-lock}
           {w-oeeh.i oeel.orderno oeel.ordersuf no-lock}
           assign s-custno     = string(oeel.custno) +
                                 (if avail arsc then arsc.notesfl
                                  else "")
                  s-custnm     = if avail arsc then arsc.lookupnm
                                 else v-invalid
                  dp-orderno   = oeel.orderno 
                  dp-ordersuf  = oeel.ordersuf 
                  dp-lineno    = oeel.lineno            
                  dp-qtyord    = oeel.qtyord
                  dp-reqshipdt = if oeel.reqshipdt ne ? and avail oeel then 
                                    oeel.reqshipdt
                                 else 
                                    oeeh.reqshipdt
                  dp-promisedt = if oeel.promisedt ne ? and avail oeel then 
                                    oeel.promisedt
                                 else 
                                    oeeh.promisedt. 
           if validprt = no then 
            do:
               display with frame f-porzc5.
               validprt = yes.
            end. 
            display s-custno
                   s-custnm
                   dp-orderno
                   dp-ordersuf
                   dp-notesfl
                   dp-lineno
                   dp-qtyord
                   dp-reqshipdt
                   dp-promisedt                                    
                   with frame f-porzc7.
                   down with frame f-porzc7.
           assign s-custno = "" 
                  s-custnm = "".
        end. /* for each oeel */  
       else 
       if poelo.seqaltno > 0 then 
        do:
         for each  oeelk where 
                   oeelk.cono      = g-cono             and 
                   oeelk.ordertype = (if poelo.ordertype = "o" then 
                                         poelo.ordertype  
                                      else 
                                         "w")           and  
                   oeelk.orderno   = poelo.orderaltno   and    
                   oeelk.ordersuf  ge 0                  and 
                   oeelk.lineno    = poelo.linealtno    and 
                   oeelk.seqno     = poelo.seqaltno     and
                  (oeelk.specnstype = "s" or oeelk.specnstype = "n")
                   no-lock:
         {w-oeeh.i oeelk.orderno oeelk.ordersuf no-lock}
         {w-oeel.i oeelk.orderno oeelk.ordersuf oeelk.lineno no-lock}
         if avail oeeh and oeeh.invoicedt = ? then 
          do:
           {w-arsc.i oeeh.custno no-lock}
           assign s-custno     = string(oeeh.custno) +
                                 (if avail arsc then arsc.notesfl
                                  else "")
                  s-custnm     = if avail arsc then arsc.lookupnm
                                 else v-invalid
                  dp-orderno   = oeelk.orderno 
                  dp-ordersuf  = oeelk.ordersuf 
                  dp-lineno    = oeelk.lineno            
                  dp-qtyord    = oeelk.qtyord
                  dp-reqshipdt = if oeel.reqshipdt ne ? and avail oeel then 
                                    oeel.reqshipdt
                                 else 
                                    oeeh.reqshipdt
                  dp-promisedt = if oeel.promisedt ne ? and avail oeel then 
                                    oeel.promisedt
                                 else 
                                    oeeh.promisedt.
           if validprt = no then 
            do:
               display with frame f-porzc5.
               validprt = yes.
            end. 
            display s-custno
                    s-custnm
                    dp-orderno
                    dp-ordersuf
                    dp-notesfl
                    dp-lineno
                    dp-qtyord
                    dp-reqshipdt
                    dp-promisedt                                    
                    with frame f-porzc7.
                    down with frame f-porzc7.
           assign s-custno = "" 
                  s-custnm = "".
          end. 
        end. /* for each */
       end. /* poelo-seqno > 0 */.

     if poelo.ordertype ne "o" then 
      do:
        if validprt = no then 
         do:
            display with frame f-porzc5.
            validprt = yes.
         end. 
        display s-custno
                s-custnm
                dp-orderno
                dp-ordersuf
                dp-notesfl
                dp-lineno
                dp-qtyord
                dp-reqshipdt
                dp-promisedt                                    
                with frame f-porzc7.
               down with frame f-porzc7.
        assign s-custno = "" 
               s-custnm = "".
      end.
  end. /* for each poelo */
 end.  /* if p-onlybo   */
end. /* do */
end. /* procedure */

procedure drill_deeper:
   {vaeh.gfi  &vano   = "wtelo.orderaltno"
              &vasuf  = "wtelo.orderaltsuf" &lock  = no}
   {vaesl.gfi &vano   = "wtelo.orderaltno"             
              &vasuf  = "wtelo.orderaltsuf" 
              &seqno  = "wtelo.seqaltno"
              &lineno = "wtelo.linealtno"
              &lock   = "no"}
   find last  vaelo use-index k-vaelo where
              vaelo.cono        = g-cono           and
              vaelo.vano        = wtelo.orderaltno 
              no-lock no-error.
   if avail vaelo then 
    do:
      {w-oeeh.i vaelo.orderaltno vaelo.orderaltsuf no-lock}
      {w-oeel.i vaelo.orderaltno vaelo.orderaltsuf
                vaelo.linealtno no-lock}
      if avail oeeh or avail oeel then 
       {w-arsc.i oeeh.custno no-lock}
    end.
   if avail vaelo and vaelo.ordertype = "o" then 
    do:
      if avail oeeh and avail arsc then           
       assign s-custno     = string(oeeh.custno) +
                            (if avail arsc then arsc.notesfl
                             else "")    
              s-custnm     = if avail arsc then arsc.lookupnm
                             else v-invalid
              dp-reqshipdt = if oeel.reqshipdt ne ? and avail oeel then 
                               oeel.reqshipdt 
                             else 
                               oeeh.reqshipdt
              dp-promisedt = if oeel.promisedt ne ? and avail oeel then 
                                oeel.promisedt 
                             else 
                                oeeh.promisedt.
      else 
       assign s-custno = "" 
              s-custnm = "".
    end.
end. 

{zsapboycheck.i}

/{&user_procedures}*/

