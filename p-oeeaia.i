/* p-oeeaia.i 1.3 06/08/98 */
/*h*****************************************************************************  INCLUDE      : p-oeeaia.i
  DESCRIPTION  : Fields in "Inv" record for edi 810, 843, and 855.
  USED ONCE?   : no
  AUTHOR       : jlc
  DATE WRITTEN : 02/23/94
  CHANGES MADE :
    03/28/96 jlc; TB# 20843 Add the Trend order type.
    03/27/98 jlc; TB# 21395 Add placedby and ship instructions to inv record.
    05/29/98 kjb; TB# 24971 Add the whse to the EDI 810 out file, s-whse
    03/25/00 jlc; TB# 4666  Output Order Disposition
    05/01/03 lbr; TB# e17245 Added dunsno var s-conodunsno
*******************************************************************************/
"Inv   " +
        caps(s-invdt)        +
        caps(s-invno)        +
        caps(s-invsuf)       +
        caps(s-custpo)       +
        caps(s-invtype)      +
        caps(s-refer)        +
        caps(s-corecharge)   +
        caps(s-datccost)     +
        caps(s-downpmtamt)   +
        caps(s-othdiscamt)   +
        caps(s-restockamt)   +
        caps(s-taxamt)       +
        caps(s-gsttaxamt)    +
        caps(s-psttaxamt)    +
        caps(s-wodiscamt)    +
        caps(s-partner)      +
        caps(s-byid)         +
        caps(s-deptno)       +
        caps(s-orderdisp)    +
        caps(s-event)        +
        caps(s-vendno2)      +
        caps(s-canceldt)     +
        caps(s-shipdt)       +
        caps(s-promisedt)    +
        caps(s-reqshipdt)    +
        caps(s-shipvia)      +
        caps(s-hduser1)      +
        caps(s-poissuedt)    +
        caps(s-enterdt)      +
        caps(s-termsdiscamt) +
        caps(s-pkgid)        +
        caps(s-acktype)      +
        caps(s-todaysdt)     +
        caps(s-hduser2)      +
        caps(s-hduser3)      +
        caps(s-hduser4)      +
        caps(s-ordtype)      +
        caps(s-shipinstr)    +
        caps(s-placedby)     +
        caps(s-whse)      /* +
        caps(s-conodunsno)*/