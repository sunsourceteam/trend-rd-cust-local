/* n-zpoque.i */
/* ------------------------------------------------------------------------
  Program Name :  N-ZPOQUE.I

  Description  :  Next /  Previous / First find module for POEZQ using
                  xxixx.i

  Author       :  Thomas Herzog


------------------------------------------------------------------------- */

confirm = true.


if z-refresh then
  do:
  z-refresh = false.

  find  poel where recid(poel) = z-poelrecid no-lock no-error.
  if avail poel then
    do:
    assign g-pono = poel.pono
           g-posuf = poel.posuf
           g-polineno = poel.lineno.
    end.      
  find  zidc where recid(zidc) = z-zidcrecid no-lock no-error.
  if avail zidc then
    do:
    find  poeh 
            where poeh.cono = g-cono and
                    poeh.pono = zidc.docnumber and
                   poeh.posuf = 0 no-lock no-error.
    end.          
  end.
else                         
  do:
  if "{1}" = "next" or "{1}" = "prev" then
    do:
    find {1}   poel use-index k-poel
                   where poel.cono = g-cono and
                         poel.pono = g-pono and
                         poel.posuf = 0 no-lock no-error.
   
    if avail poel then
      do:
      assign g-pono = poel.pono
             g-posuf = poel.posuf
             g-polineno = poel.lineno.
      find       poeh 
           where poeh.cono = g-cono and
                 poeh.pono = g-pono and
                 poeh.posuf = 0 no-lock no-error.
      if poeh.stagecd ge 2 then
        do:
        g-pono = 0.
        confirm = false.
        end.
      else
        do:
        if i = 1  then
          find first zidc use-index docinx1
                          where zidc.cono        = g-cono and
                                zidc.docttype    = "PQ" and
                                zidc.docnumber   = poel.pono
                                no-lock no-error.
       
        if "{1}" = "prev" then
          assign z-poelrecid = recid(poel)
                 z-zidcrecid = recid(zidc)
                 confirm = true.
        end.
      end.
    end.  
                         
                        
  if not avail poel or g-pono = 0 or "{1}" = "first" then
    do:
    find {1} zidc  use-index docinx6
                where zidc.cono       = g-cono and
                      zidc.docttype   = "pq" and
                      zidc.docpartner = s-buyer and
                      zidc.docvendno  ge b-vendno and
                      zidc.docuser1   = "Inbatch"      
                      no-lock no-error.

     if avail zidc then
       find  poeh 
             where poeh.cono = g-cono and
                   poeh.pono = zidc.docnumber and
                   poeh.posuf = 0 and
                   poeh.stagecd < 2 no-lock no-error.
     if not avail zidc or not avail poeh then
      do:
      confirm = false.
      end.
    else
      do:
      /*
      find  poeh 
                   where poeh.cono = g-cono and
                         poeh.pono = zidc.docnumber and
                         poeh.posuf = 0 and
                         poeh.stagecd < 2 no-lock no-error.
      */        
      if "{1}" = "prev" then       
        find last  poel use-index k-poel
                   where poel.cono = g-cono and
                         poel.pono = zidc.docnumber and
                         poel.posuf = 0 no-lock no-error.
      else  
        find first   poel use-index k-poel
                   where poel.cono = g-cono and
                         poel.pono = zidc.docnumber and
                         poel.posuf = 0 no-lock no-error.
      if avail poel then
        do:
        assign g-pono = poel.pono
               g-posuf = poel.posuf
               g-polineno = poel.lineno
               confirm = true.

        if poeh.stagecd ge 2 then
          do:
          g-pono = 0.
          confirm = false.
          end.
        else
          do:
          if "{1}" = "first" or "{1}" = "prev" then
            assign z-poelrecid = recid(poel)
                   z-zidcrecid = recid(zidc).
          end.
        end.
    
    
      if not avail poel then
        do:
        g-pono = 0.
        confirm = false.
        end.
      end.
    end.
  end.
if i = 1 and "{1}" = "next" then
  assign z-poelrecid = recid(poel)
         z-zidcrecid = recid(zidc).
 
       
