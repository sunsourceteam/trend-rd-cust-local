
/* f-oees.i 1.1 01/03/98 */
/* f-oees.i 1.2 03/30/93 */
/*h*****************************************************************************
  INCLUDE      : f-oees.i
  DESCRIPTION  : Form for oees.p
  USED ONCE?   : No (oees.p, oeesi.p & oeesl.p)
  AUTHOR       :
  DATE WRITTEN : 02/17/93
  CHANGES MADE :
    02/17/96 rhl; TB#  5868 Suffix lookup indicator is missing [L]
    03/20/06 kr;  TB# 12406 Variables defined in form, moved out of form
    05/10/99 lbr; TB# 5366  Added shipto notes
    05/31/03 lcb; TB# t14244 8Digit Ord No Project
    08/12/03 mms; TB# e17778 8 Digit OrderNo-moved notes flag to right one.
*******************************************************************************/


form
    "Order#    Type   Customer#    Ship To  Whse  PO" at  3  
    arsc.lookupnm                                     at 63
    g-orderno                                         at  1
        {f-help.i}
    "-"                                               at  9
    g-ordersuf                                        at  10
        {f-help.i}
    oeeh.notesfl                                      at 12
    g-oetype                                          at 14
    g-custno                                          at 19
    arsc.notesfl                                      at 31
    g-shipto                                          at 33
    arss.notesfl                                      at 41
    g-whse                                            at 42
    s-custpo                                          at 48
    s-blbofl                                          at 71
    s-stagecd                                         at 74
    skip(1)   
    "Outbound Freight:"                               at 1
    s-outfrt                                          at 24
    "# of Pkg:"                                       at 1  
    s-nopackages                                      at 24  
    "Tracker #1:"                                     at 1
    s-tracker1                                        at 24
    "Tracker #2:"                                     at 1
    s-tracker2                                        at 24
    "Tracker #3:"                                     at 1
    s-tracker3                                        at 24
    "Shipvia:"                                        at 1
     s-shipvia                                        at 24
     {f-help.i}
    "Inbound Freight:"                                at 1
     s-infrt                                          at 24
    "Inbnd Frt Req:"                                  at 1
     s-inbfrtfl                                       at 24
    "Outbnd Frt Req:"                                 at 1
     s-outfrtfl                                       at 24
    "Handling"                                        at 1
     s-handling                                       at 24
    "Ship Instructions:"                              at 1
    s-shipinstr                                       at 24  
    "Order Total:"                                    at 1
    s-invamt                                          at 24
    "Ship Dt:"                                        at 1  
    s-shipdt                                          at 24 
    "Ship Late Reason:"                               at 1
    exceptCode                                        at 24
    with frame f-oeezs title g-title row 1 width 80 no-labels side-labels 
    overlay.
