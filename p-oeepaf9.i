/* p-oeepa1.i 1.20 02/21/94 */
/*h*****************************************************************************
  INCLUDE      : p-oeepa1.i
  DESCRIPTION  : OEEPA/OEEPAF main print logic
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    10/14/91 mwb; TB# 4762  Change page#, order #, shipped date and ship via
        for pre-printed forms
    04/03/92 pap; TB# 6219  Line DO  add DO line indicator adjusted qty
        backordered
    06/06/92 mwb; TB# 6889  Replaced oeeh.payamt with oeeh.tendamt.
    06/16/92 mkb; TB# 5929  Full amount tendered, add write off amount to
        downpayment
    06/20/92 mwb; TB# 5413  If the header as a write off amount set - add it
        to the down payment display (BO rounding problems when down
        payment is applied).
    08/05/92 pap; TB# 7245  Do not print the backordered qty or the shipped
        qty on an acknowledgement
    08/12/92 pap;           REVERSAL of TB# 7245 BY DESIGN MEETING
    11/11/92 mms; TB# 8561  Display product surcharge label from ICAO
    12/07/92 ntl; TB# 9041  Canadian Tax Changes (QST)
    03/18/93 jlc; TB# 7245  Items with a quantity shipped of 0 are printing a
        quantity shipped and a quantity backordered.
    04/07/93 jlc; TB# 8890  Drop Ship orders should print Drop Ship for
        the Ship Point.
    04/08/93 jlc; TB# 10155 Print customer product.
    04/08/93 jlc; TB# 9727  Don't print core chg and restock amt for a
        non-print line.
    04/10/93 jlc; TB# 7405  Print DROP for the qty ship on drop lines.
    06/07/93 jlc;           Changes to tb# 7245 per Design Committee.
    02/21/94 dww; TB# 13890 File name not specified causing error
    06/23/95 ajw; TB# 18575 Nonstock/Special Kit Component Project (G1)
    08/17/95 tdd; TB# 17771 Notes print twice when reference component
    11/01/95 gp;  TB# 18091 Expand UPC Number (T24)
    11/06/95 gp;  TB# 18091 Expand UPC Number (T24)
    02/27/97 jkp; TB#  7241 (5.1) Spec8.0 Special Price Costing.  Added call
        of speccost.gva.  Added find of icss record.  Added call to 
        speccost.gas to determine prccostper.  Replaced icsp.prccostper with 
        v-prccostper.
    04/17/97 JPH; ITB# 1    Remove upc number, qty b/o, qty shpd, upc vendor
                            number, change orderno to invoice number.
    12/01/97 JPH; ITB# 2    Add icsd information to remittance portion.          ******************************************************************************/

/*tb  7241 (5.1) 02/27/97 jkp; Added call to speccost.gva. */
{speccost.gva}

/*tb  5929 06/18/92 mkb */
def buffer            b-oeeh   for oeeh.
def var zx-handle     as logical no-undo.


form header  /* main title lines  */
    skip(2)
    s-lit1aa as c format "x(10)"         at 2
    s-lit1a  as c format "x(50)"         at 12
    s-cancel as c format "x(25)"         at 64
    s-lit1b  as c format "x(37)"         at 93
    s-lit2a  as c format "x(53)"         at 2
/*    b-sasc.upcvno                        at 95    ITB# 1 */
/*    oeeh.invoicedt                       at 108   ITB# 1 */
    oeeh.orderno                         at 123
    "-"                                  at 130
    oeeh.ordersuf                        at 131
    s-lit3a as c format "x(09)"          at 2
    oeeh.custno                          at 12
    s-lit3b as c format "x(39)"          at 94
    oeeh.enterdt                         at 94
    oeeh.custpo                          at 104
    page-num - v-pageno format "zzz9"    at 129
    skip(1)
    s-lit6a as c format "x(8)"           at 2
    s-billname like arsc.name            at 12
    s-lit6b as c format "x(18)"          at 48
/*      b-sasc.conm                          at 68  ITB# 2 */
    s-remitnm as c format "x(24)"        at 68   /* ITB# 2 */
    s-billaddr1 like arsc.name           at 12
/*    b-sasc.addr[1]                       at 68    ITB# 2 */
    s-lit6bb as c format "x(6)"          at 48   /* ITB# 2 */
    s-remit-addr1 as c format "x(24)"    at 68   /* ITB# 2 */ 
    s-billaddr2 like arsc.name           at 12
/*    b-sasc.addr[2]                       at 68    ITB# 2 */
    s-remit-addr2 as c format "x(24)"    at 68   /* ITB# 2 */
    s-lit8a as c format "x(9)"           at 106
    s-pstlicense as c format "x(11)"     at 116
    s-billcity as c format "x(35)"       at 12
    s-corrcity as c format "x(35)"       at 68
    s-lit9a as c format "x(9)"           at 106
    s-gstregno as c format "x(10)"       at 116
    skip(1)
    s-lit11a as c format "x(8)"          at 2
    s-shiptonm like oeeh.shiptonm        at 12
    s-lit11b as c format "x(12)"         at 50
    s-shiptoaddr[1]                      at 12
    oeeh.shipinstr                       at 50
    s-shiptoaddr[2]                      at 12
    s-lit12a as c format "x(10)"         at 50
    s-lit12b as c format "x(3)"          at 82
    s-lit12c as c format "x(7)"          at 104
    s-lit12d as c format "x(5)"          at 117
    s-shipcity as c format "x(35)"       at 12
    s-whsedesc as c format "x(30)"       at 50
    s-shipvia as c format "x(12)"        at 82
    s-cod as c format "x(6)"             at 95
    s-shipdt like oeeh.shipdt            at 104
    s-terms as c format "x(12)"          at 117
    skip(1)
    s-rpttitle like sapb.rpttitle        at 1 skip(1)
with frame f-head no-box no-labels width 132 page-top.

form header  /* detail line title */
    s-lit14a as c format "x(75)"         at 1
    s-lit14b as c format "x(53)"         at 76
    s-lit15a as c format "x(75)"         at 1
    s-lit15b as c format "x(53)"         at 76
/*
    s-lit16a as c format "x(78)"         at 1
    s-lit16b as c format "x(53)"         at 79
*/
with frame f-headl no-box no-labels width 132 page-top.

def var    s-lit16a as c format "x(78)".
def var    s-lit16b as c format "x(53)".


form  /* detail line totals  */
    skip(1)
    v-linecnt         format "zz9"       at 1
    s-lit40a     as c format "x(11)"     at 5
    s-lit40c     as c format "x(17)"     at 21   
    s-totqtyshp  as c format "x(13)"     at 40
    s-lit40d     as c format "x(18)"     at 82
    s-totlineamt as c format "x(13)"     at 117
with frame f-tot1 no-box no-labels no-underline width 132.


form  /* all extra costs display  */
    s-canlit as c   format "x(60)"         at 15    /*si02*/
    s-title2 as c format "x(33)"           at 82
    s-amt2   as dec format "zzzzzzzz9.99-" at 117
with frame f-tot2 no-box no-labels width 132 no-underline down.


form  /* all extra costs display  */
    s-canlit  format "x(60)"               at 15    /*si02*/
    s-title2  format "x(33)"               at 82
with frame f-tot2a no-box no-labels width 132 no-underline down.


form  /* invoice total*/
    s-currencyty as c format "x(51)"     at 24      /*si02*/
    s-invtitle as c format "x(18)"       at 82
    s-totinvamt like oeeh.totinvamt      at 117
with frame f-tot3 no-box no-labels no-underline width 132.



/* smaf */

form
 s-taxtitle as char format "x(15)"   at 82  
 s-vartax   as char format "x(15)"   at 118
with frame f-vartax no-box no-labels width 132 no-underline.


form header  /* footter at last page */
    s-lit48a as c format "x(9)"          at 1
    s-CSR-title  as c format "x(25)"         at 20 
    s-contact    as c format "x(30)"         at 46 
    s-contlit-ph as c format "x(4)"          at 82 
    s-contact-ph as c format "(xxx)xxx-xxxx" at 87 
    s-contlit-fx as c format "x(4)"          at 102
    s-contact-fx as c format "(xxx)xxx-xxxx" at 107
with frame f-tot4 no-box no-labels width 132 page-bottom.

/*tb# 5929 06/18/92 mkb */
form
    "Full Amount Tendered For All Orders:"  at 5
    oeeh.tottendamt                         at 42
    "*** Back Order/Release Exists ***"     at 5
with frame f-tot5 no-box no-labels width 132.

assign
    v-pageno    = page-number - 1
    v-subnetamt = 0.

/*    {a-oeepa9.i s-} */
    {a-oeepaf9.i s-} 


    {p-oeord.i "oeeh."}    /*** v-totlineamt ***/

if oeeh.transtype = "qu" then  
   s-lit1a = "Quote".

if oeeh.transtype = "qu" then
  assign s-lit40c = "Qty Quoted Total ".
else
  assign s-lit40c = "Qty Ordered Total".



/*tb 5413 06/20/92 mwb */
assign
    s-billname   = if v-invtofl then oeeh.shiptonm
                   else arsc.name
    s-billaddr1  = if v-invtofl then oeeh.shiptoaddr[1]
                   else arsc.addr[1]
    s-billaddr2  = if v-invtofl then oeeh.shiptoaddr[2]
                   else arsc.addr[2]
    s-billcity   = if v-invtofl then
                       oeeh.shiptocity + ", " + oeeh.shiptost +
                       " " + oeeh.shiptozip
                   else arsc.city + ", " + arsc.state + " " + arsc.zipcd
/*    s-corrcity   = b-sasc.city + ", " + b-sasc.state + " " + b-sasc.zipcd */
/*    s-cod        = if oeeh.codfl = no then "" else "C.O.D." */
    s-shipdt     = if oeeh.shipdt = ? then oeeh.pickeddt else oeeh.shipdt
    s-totlineamt = if oeeh.lumpbillfl then
                       string(oeeh.lumpbillamt,"zzzzzzzz9.99-")
                   else if oeeh.transtype = "rm" then
                       string((oeeh.totlineamt * -1),"zzzzzzzz9.99-")
                   else string(v-totlineamt,"zzzzzzzz9.99-")
    s-taxes      = oeeh.taxamt[1] + oeeh.taxamt[2] +
                   oeeh.taxamt[3] + oeeh.taxamt[4]
    s-dwnpmtamt  = if oeeh.transtype = "cs" then oeeh.tendamt
                   else if oeeh.dwnpmttype then oeeh.dwnpmtamt
                   else (oeeh.dwnpmtamt / 100) * oeeh.totinvamt
    s-dwnpmtamt  = s-dwnpmtamt + oeeh.writeoffamt
    v-linecnt    = 0
    s-shiptonm      = oeeh.shiptonm
    s-shiptoaddr[1] = oeeh.shiptoaddr[1]
    s-shiptoaddr[2] = oeeh.shiptoaddr[2]
    s-shipcity      = oeeh.shiptocity + ", " + oeeh.shiptost +
                      " " + oeeh.shiptozip
    s-gstregno      = if g-country = "ca" then b-sasc.fedtaxid else ""
    s-pstlicense    = if g-country = "ca" then oeeh.pstlicenseno
                      else "".

{p-oeadfp.i}  /* load addon descriptions */

{w-sasta.i "S" oeeh.shipvia no-lock}

{w-icsd.i oeeh.whse no-lock}

/* ITB# 2 */
assign s-corrcity = icsd.city + ", " + icsd.state + " " + icsd.zipcd
       s-remitnm = icsd.name
       s-remit-addr1 = icsd.addr[1]
       s-remit-addr2 = icsd.addr[2].

/*tb 8890 04/07/93 jlc; DO's print Drop Ship for Ship Point */
assign
    s-shipvia  = if avail sasta then sasta.descrip else oeeh.shipvia
    s-whsedesc = if oeeh.transtype = "do" then "** Drop Ship **"
                 else if avail icsd then icsd.name
                 else oeeh.whse.

{w-sasta.i "t" oeeh.termstype no-lock}

if oeeh.langcd <> "" then do:
    {w-sals.i oeeh.langcd "t" oeeh.termstype no-lock}
end.

assign
    s-terms    = if oeeh.langcd <> "" and avail sals then sals.descrip[1]
                 else if avail sasta then sasta.descrip
                 else oeeh.termstype
    s-rpttitle = if p-promofl then sapb.rpttitle else "".

view frame f-head.
if oeeh.transtype = "ra" then hide frame f-headl.
else view frame f-headl.
view frame f-tot4.
/* SX32
/*    print the customer notes  */
if arsc.notesfl ne "" then do:
    for each notes where
        notes.cono         = g-cono    and
        notes.notestype    = "c"       and
        notes.primarykey   = string(oeeh.custno) and
        notes.secondarykey = ""
    no-lock:

        if notes.printfl = yes then do i = 1 to 16:
            /*tb 13890 02/21/94 dww; File name not specified causing error */
            if notes.noteln[i] ne "" then do:
                display notes.noteln[i] with frame f-notes.
                down with frame f-notes.
            end.

        end.

    end.

end. /* if arsc.notesfl ne "" */

/* print the order notes */
if oeeh.notesfl ne "" then do:
    for each notes where
        notes.cono         = g-cono    and
        notes.notestype    = "o"       and
        notes.primarykey   = string(oeeh.orderno) and
        notes.secondarykey = ""
    no-lock:

        if notes.printfl = yes then do i = 1 to 16:
            /*tb 13890 02/21/94 dww; File name not specified causing error */
            if notes.noteln[i] ne "" then do:
                display notes.noteln[i] with frame f-notes.
                down with frame f-notes.
            end.

        end.

    end.

end. /* if oeeh.notesfl ne "" */
SX32 */

/*    print the customer notes  */
if arsc.notesfl ne "" then
  run notesprt.p(g-cono,"c",string(oeeh.custno),"","oeepa",8,2).
    
    /*tb 5366 05/12/99 lbr; Added shipto notes flag */
if avail arss and arss.notesfl <> "" then
  run notesprt.p(g-cono,"cs",string(oeeh.custno),oeeh.shipto,"oeepa",8,2).

/* print the order notes */
if oeeh.notesfl ne "" then
  run notesprt.p(g-cono,"o",string(oeeh.orderno),string(oeeh.ordersuf),
                 "oeepa",8,2).




/*  a receipt on acct has no lines */
if oeeh.transtype <> "ra" then do:

    v-totqtyshp = 0.

    for each oeel use-index k-oeel where
        oeel.cono        = g-cono       and
        oeel.orderno     = oeeh.orderno and
        oeel.ordersuf    = oeeh.ordersuf and
        oeel.specnstype <> "l"
    no-lock:

        /*tb 18091 11/01/95 gp;  Find Product section of UPC number */
        if oeel.arpvendno <> 0 then
            {icsv.gfi "'u'" oeel.shipprod oeel.arpvendno no}
        else
            {icsv.gfi "'u'" oeel.shipprod oeel.vendno no}

        /*tb  7241 (5.1) 02/27/97 jkp; Added find of icss record based on
             oeel because the current product is needed. */
        {icss.gfi 
            &prod        = oeel.shipprod
            &icspecrecno = oeel.icspecrecno
            &lock        = "no"}

        /*tb  7241 (5.1) 02/27/97 jkp; Added call to speccost.gas to 
             determine v-prccostper. */
        assign
            {speccost.gas
                &com = "/*"}
            v-linecnt = v-linecnt + 1
            v-netord  = if {p-oeordl.i "oeel." "oeeh."} then oeel.netord
                        else oeel.netamt
            v-qtyord  = if {p-oeordl.i "oeel." "oeeh."} then oeel.qtyord
                        else oeel.qtyship.
/* SunFix0927 */
        if oeel.botype = "d" and v-qtyord = 0 and
           oeeh.stagecd < 2 and 
           (not can-do("fo,qu,st,bl",oeeh.transtype))  and
           (not  can-do("s,t",oeeh.orderdisp))  and
           oeeh.boexistsfl = no  then
          v-qtyord  = oeel.qtyord.

/* SunFix0927 */
 




        if oeel.specnstype ne "n" then do:
            {w-icsw.i oeel.shipprod oeel.whse no-lock}
            {w-icsp.i oeel.shipprod no-lock}

            if oeeh.langcd <> "" then
                {w-sals.i oeeh.langcd "p" oeel.shipprod no-lock}

        end.
       else
            /* Added catalog read for product notes display */
                        {w-icsc.i oeel.shipprod no-lock} .
                        
 
        clear frame f-oeel.

        /*tb 1517 01/24/91 mwb; ship comp, tag&hold always uses ordered */
        /*tb 1043 01/26/91 mwb; BO stg 1 */
        /* mms 01/03/92 JIT print quantity to be backordered */
        /*tb 6219 04/03/92 pap; Line DO - incorporate 'd' botype */
        /*tb 7245 03/18/93 jlc; Printing a quantity shipped when the item was
            completely backordered */
        /*tb 7405 04/10/93 jlc; Print DROP for the qty ship on DOs */
        /*tb 7245 06/07/93 jlc; Per design committee, if p-oeordl.i then
            s-qtyship, s-qtybo should be blank */
        /*tb 18091 11/01/95 gp; Assign product section of UPC number */
        /*tb  7241 (5.1) 02/27/97 jkp; Changed assign of s-prcunit to use
             v-prccostper instead of icsp.prccostper. */
        assign
            s-qtyship  = if oeel.returnfl then
                             if substring
                                 (string(oeel.qtyship,"9999999.99-"),9,2) = "00"
                                 then string((oeel.qtyship * -1),"zzzzzz9-")
                             else string(oeel.qtyship * -1,"zzzzzz9.99-")
                         else if substring
                             (string(oeel.qtyship,"9999999.99-"),9,2) = "00"
                             then string(oeel.qtyship,"zzzzzz9-")
                         else string(oeel.qtyship,"zzzzzz9.99-")
            s-qtyord   = if oeel.returnfl then
                             if substring
                                 (string(oeel.qtyord,"9999999.99-"),9,2) = "00"
                                 then string((oeel.qtyord * -1),"zzzzzz9-")
                             else string((oeel.qtyord * -1),"zzzzzz9.99-")
                         else if substring(string(oeel.qtyord,"9999999.99-")
                             ,9,2) = "00"
                             then string(oeel.qtyord,"zzzzzz9-")
                         else string(oeel.qtyord,"zzzzzz9.99-")
             
            s-qtyship  = if {p-oeordl.i "oeel." "oeeh."} then ""
                         else s-qtyship
            v-qtybo    = if oeel.botype ne "n" then
                             dec(s-qtyord) - dec(s-qtyship)
                         else 0
            s-qtybo    = if substring(string(v-qtybo,"9999999.99-"),9,2) = "00"
                             then string(v-qtybo,"zzzzzz9-")
                         else string(v-qtybo,"zzzzzz9.99-")
            s-qtybo    = if {p-oeordl.i "oeel." "oeeh."} then ""
                         else s-qtybo
            s-lineno   = string(oeel.lineno,"zz9")
            s-price    = if oeel.corechgty = "r" then
                             string(oeel.corecharge,"zzzzzz9.99-")
                         else if substring
                             (string(oeel.price,"9999999.99999"),11,3) = "000"
                             then string(oeel.price,"zzzzzz9.99-")
                         else string(oeel.price,"zzzzzz9.99999-")
            s-discpct  = if substring
                             (string(oeel.discpct,"9999999.99999"),11,3) = "000"
                             then string(oeel.discpct,"zzzzzz9.99-")
                         else string(oeel.discpct,"zzzzzz9.99999-")
            s-netamt   = if oeel.corechgty = "r" then
                             string((oeel.corecharge * oeel.qtyship),
                                 "zzzzzzzz9.99-")
                         else string(v-netord,"zzzzzzzz9.99-")
            s-sign     = if oeel.returnfl and oeel.printpricefl = yes then "-"
                         else ""
            v-subnetamt = v-subnetamt +
                          if oeel.returnfl then (v-netord * -1)
                          else v-netord
            v-totqtyshp = if oeel.returnfl then
                              if can-do("rm",oeeh.transtype) then
                                  v-totqtyshp - v-qtyord
                              else v-totqtyshp
                          else if oeeh.stagecd < 3 and oeel.botype = "d" then
                              v-totqtyshp
                          else v-totqtyshp + v-qtyord
            s-prcunit   = if v-prccostper <> "" then
                              v-prccostper
                          else oeel.unit
            s-upcpno    = if avail icsv and avail b-sasc
                                        and oeel.corechgty <> "r" then
                              {upcno.gas &section  = "4"
                                         &sasc     = "b-sasc."
                                         &icsv     = "icsv."
                                         &comdelim = "/*"}
                          else "00000".

/* new canada stuff */


        /*si02 begin -- logic for Canadian Currency sdi06501*/
        if (substr(oeeh.user5,1,1) = "c") or
         (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then do: 
           if oeeh.user6 <> 0 then do:
              assign 
                 s-price     = if oeel.corechgty = "r" 
                               then string(dec(s-price) * oeeh.user6,
                                           "zzzzzz9.99-") 
                               else if oeel.price * 100 = int(oeel.price * 100) 
                                    then string(dec(s-price) * oeeh.user6, 
                                                "zzzzzz9.99-")
                                    else string(
                                    round(dec(s-price) * oeeh.user6,2),
                                                "zzzzzz9.99-")
                 s-netamt    = string(dec(s-netamt) * oeeh.user6,
                                      "zzzzzzzz9.99-")
                 v-subnetamt = v-subnetamt * oeeh.user6.
           end.

           s-cantot    = s-cantot + 
                         (dec(s-netamt) * (if oeel.returnfl then -1 else 1)). 
        end.
        /*end si02*/                                  

/* new canada stuff */



        
        
        
        
        /*tb 7405 04/10/93 jlc; Print DROP for the qty ship on DOs */
        assign
            s-descrip = ""
            s-qtyship = if oeeh.stagecd < 3 and oeel.botype = "d" then "  DROP"
                        else s-qtyship
            s-qtybo   = if oeeh.stagecd < 3 and oeel.botype = "d" then ""
                        else s-qtybo.

        /*tb 7245 08/05/92 pap; Do not display qty backordered or qty shipped */
        /* REVERSAL OF TB 7245 - 08/12/92 DESIGN MEETING */
        /*tb 18091 11/01/95 gp;  Change display of UPC number */

/* ITB# 2        Suppress discounts from acknowledgments 
        if s-discpct ne "" then do:
           assign s-price = string
             (oeel.price * ((100.00 - oeel.discpct) / 100),"zzzzzz9.99999-")
                  s-discpct = "".
           end.
   new canada stuff */


/* ITB# 2        Suppress discounts from acknowledgments */
        if s-discpct ne "" then do:
           assign s-price = string(round
             (oeel.price * ((100.00 - oeel.discpct) / 100) *
                        /*si02 added "if substr..."*/
                           (if (substr(oeeh.user5,1,1) = "c"
                                and oeeh.user6 <> 0) or
                             (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
    
                            then oeeh.user6
                                else 1),2),"zzzzzz9.99-")
                  s-discpct = "".
           end.
/* new canada stuff */

        x-shipdt = date(substring(oeel.user3,11,10)).
                   
        display
            s-lineno
            oeel.shipprod
            oeel.unit
            s-qtyord
            s-qtybo
            s-qtyship

            s-prcunit
/*            s-upcpno   ITB # 1 */
            s-price         when oeel.printpricefl = yes
/*           s-discpct       when oeel.printpricefl = yes      */
            x-shipdt
            s-netamt        when oeel.printpricefl = yes
            s-sign          with frame f-oeel.
/* Old
                    
        display
            s-lineno
            oeel.shipprod
            oeel.unit
            s-qtyord
            s-qtybo
            s-qtyship

            s-prcunit
/*            s-upcpno   ITB # 1 */
            s-price         when oeel.printpricefl = yes
            s-discpct       when oeel.printpricefl = yes
            s-netamt        when oeel.printpricefl = yes
            s-sign          with frame f-oeel.
*/
        if oeel.specnstype = "n" then do:
            if oeel.proddesc ne "" then
                display
                    oeel.proddesc @ s-descrip
                with frame f-oeel.
        end.        /** non-stock **/

        else if not avail icsp or not avail icsw then
            if oeeh.langcd = "" or not avail sals then
                display v-invalid @ s-descrip with frame f-oeel.
            else display
                     sals.descrip[1] + " " + sals.descrip[2] @ s-descrip
                 with frame f-oeel.
        else if oeeh.langcd = "" or not avail sals then
            display
                icsp.descrip[1] + " " + icsp.descrip[2] @ s-descrip
            with frame f-oeel.
        else display
                 sals.descrip[1] + " " + sals.descrip[2] @ s-descrip
             with frame f-oeel.
        down with frame f-oeel.

        v-reqtitle = if      oeel.xrefprodty = "c" then "   Customer Prod:"
                     else if oeel.xrefprodty = "i" then "Interchange Prod:"
                     else if oeel.xrefprodty = "p" then " Superseded Prod:"
                     else if oeel.xrefprodty = "s" then " Substitute Prod:"
                     else if oeel.xrefprodty = "u" then "    Upgrade Prod:"
                     else "".

        if can-do("c,i,p,s,u",oeel.xrefprodty) and oeel.reqprod <> "" then do:
            /*tb 10155 04/09/93 jlc; print 3rd tier product */
            s-prod  =   oeel.reqprod.

            display
                s-prod
                v-reqtitle
            with frame f-oeelc.
            down with frame f-oeelc.
        end.

        /*tb 10155 04/09/93 jlc; print 3rd tier customer product */
        /*tb 10155 01/03/94 mwb; made changes for cust product display */
        if oeel.xrefprodty ne "c" then do:
            s-prod = if oeel.reqprod ne "" then oeel.reqprod
                     else oeel.shipprod.

            {n-icseca.i "first" s-prod ""c"" oeeh.custno no-lock}

            if avail icsec then do:
               assign
                   s-prod     = icsec.prod
                   v-reqtitle = "   Customer Prod:".

               display
                   s-prod
                   v-reqtitle
               with frame f-oeelc.
            end.

        end. /* if oeel.xrefprodty ne "c" */

        if oeel.corechgty = "r" then
            display with frame f-oeelr.

  


/* SurCharge */

        if oeel.datccost <> 0 and v-qtyord <> 0 and
            oeel.printpricefl = yes
        then do:
            clear frame f-oeel1.
            display "Tariff Surcharge" @ oeel.shipprod
                    /*si03 added "* if substr..." for canadian currency*/
                    string(oeel.datccost *
                           (if (substr(oeeh.user5,1,1) = "c" and 
                               oeeh.user6 <> 0) or
                             (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then
                              oeeh.user6
                             else 1) * v-qtyord,"zzzzzz9.99-")
                       @ s-price
                    /*si03 added "* if substr..." for canadian currency*/
                    string(oeel.datccost *
                            (if (substr(oeeh.user5,1,1) = "c" and 
                                oeeh.user6 <> 0) or
                              (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then
                               oeeh.user6 
                             else 1) * v-qtyord ,"zzzzzzzz9.99-")
                       @ s-netamt
                    "-" when oeel.returnfl     @ s-sign
                    " " when not oeel.returnfl @ s-sign   
            with frame f-oeel1.
            down with frame f-oeel1.
        if (substr(oeeh.user5,1,1) = "c") or
         (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then 
           do: 
           assign s-cantot = s-cantot + 
                      ( (oeel.datccost *
                          (if (substr(oeeh.user5,1,1) = "c" and 
                               oeeh.user6 <> 0) or
                            (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                             then
                             oeeh.user6 
                           else 
                             1)
                          * v-qtyord ) *
                       ( if oeel.returnfl  then -1 else 1)).
           end.            
        end.


/* SurCharge */



    
    /**************** display core charge **************/
        /*tb 9727 04/08/93 jlc; don't print core chg when print flag = no */
        if oeel.corecharge <> 0 and oeel.corechgty <> "r" and
           oeel.printpricefl = yes
        then do:
            clear frame f-oeel1.

            display
                "CORE CHARGE"                           @ oeel.shipprod
                    /*si03 added "* if substr..." for canadian currency*/
                    string(oeel.corecharge * 
                    if (substr(oeeh.user5,1,1) = "c" and oeeh.user6 <> 0) or
                       (oeeh.currencyty = "lb" and oeeh.user6 <> 0)

                    then oeeh.user6 else 1,"zzzzzz9.99-") 
                       @ s-price
                    /*si03 added "* if substr..." for canadian currency*/
                    string(oeel.corecharge * 
                    if (substr(oeeh.user5,1,1) = "c" and oeeh.user6 <> 0) or
                       (oeeh.currencyty = "lb" and oeeh.user6 <> 0)

                    then oeeh.user6 else 1 * v-qtyord,"zzzzzzzz9.99-") 
                       @ s-netamt
                    "-" when oeel.returnfl     @ s-sign
                    " " when not oeel.returnfl @ s-sign
             with frame f-oeel1.
             down with frame f-oeel1.
        end.

    /**************** display restock amount **************/
        /*tb 9727 04/08/93 jlc; don't print restock when print flag = no */
        if oeel.restockamt <> 0 and oeel.printpricefl = yes
        then do:
            clear frame f-oeel1.


            /*si02 added "* if substr..." for canadian currency*/
            s-restockamt = if oeel.restockfl = yes then oeel.restockamt *
                            if (substr(oeeh.user5,1,1) = "c" 
                               and oeeh.user6 <> 0) or
                               (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                                
                            then oeeh.user6
                            else 1
                           else round(oeel.netamt *
                            if (substr(oeeh.user5,1,1) = "c" 
                               and oeeh.user6 <> 0) or
                             (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
  
                            then oeeh.user6
                            else 1 * (oeel.restockamt / 100),2).
          display
                "RESTOCK AMT" @ oeel.shipprod
                string(s-restockamt,"zzzzzzzz9.99-") @ s-netamt
            with frame f-oeel1.
            down with frame f-oeel1.
        end.


    /**************** display direct order indicator **************/
        /*tb 6219 4/3/92 pap; Line DO - add line DO indicator     **/
        if oeel.botype = "d" and oeeh.transtype ne 'DO' then
            display with frame f-oeeldo.

/* SX32 
    /*************  print the product notes  ************/
        if oeel.specnstype <> "n" and avail icsp and icsp.notesfl ne ""
        then do:
            for each notes where
                notes.cono = g-cono         and
                notes.notestype = "p"       and
                notes.primarykey = icsp.prod and
                notes.secondarykey = ""
            no-lock:

                if notes.printfl = yes then do i = 1 to 16:
                    /*tb 13890 02/21/94 dww; File name not specified causing
                        error */
                    if notes.noteln[i] ne "" then do:
                        display notes.noteln[i] with frame f-notes.
                        down with frame f-notes.
                    end.

                end.

            end.

        end. /* if oeel.specnstype <> "n" and avail icsp and ... */
SX32 */
/*************  print the product notes  ************/
     if oeel.specnstype <> "n" and avail icsp and icsp.notesfl ne "" then
       run notesprt.p(g-cono,"p",icsp.prod,"","oeepa",8,1).
                    
/********** Print the catalog product notes  ************/
     if oeel.specnstype = "n" and avail icsc and icsc.notesfl ne "" then
       run notesprt.p(1,"g",icsc.catalog,"","oeepa",8,1).
                     
 
    /**************  line item comments  ****************/
        if oeel.commentfl = yes then do:
            {w-com.i ""oe"" oeel.orderno oeel.ordersuf oeel.lineno yes no-lock}

            if avail com and com.printfl2 then do i = 1 to 16:
                if com.noteln[i] ne "" then do:
                    display com.noteln[i] with frame f-com.
                    down with frame f-com.
                end.

            end.

            {w-com.i ""oe"" oeel.orderno oeel.ordersuf oeel.lineno no no-lock}

            if avail com and com.printfl2 then do i = 1 to 16:
                if com.noteln[i] ne "" then do:
                    display com.noteln[i] with frame f-com.
                    down with frame f-com.
                end.

            end.

        end. /* if oeel.commentfl = yes */

    /********************  explode kits  *********************/
        if oeel.kitfl = yes and

            ((avail icsp and icsp.exponinvfl and oeel.specnstype <> "n") or
              oeel.specnstype = "n") and

            can-find(first oeelk use-index k-oeelk where
                           oeelk.cono      = oeel.cono     and
                           oeelk.ordertype = "o"           and
                           oeelk.orderno   = oeel.orderno  and
                           oeelk.ordersuf  = oeel.ordersuf and
                           oeelk.lineno    = oeel.lineno) then

            /*tb 2277 9/11/91 kmw; kit enh TB 2277 */
            /* was ex lock; kmw 9-12-91*/
            for each oeelk use-index k-oeelk where
                oeelk.cono      = oeel.cono     and
                oeelk.ordertype = "o"           and
                oeelk.orderno   = oeel.orderno  and
                oeelk.ordersuf  = oeel.ordersuf and
                oeelk.lineno    = oeel.lineno   and
                oeelk.printfl   = yes
            no-lock:

                /*tb 2277 9/11/91 kmw; kit enhancement */
                if oeelk.comptype = "r" then do:
                    display oeelk.instructions with frame f-refer.
                    down with frame f-refer.
                end.        /** comptype = 'r' **/

                else do:

                    /*tb 18575 06/23/95 ajw; Nonstock/Special Kit Component.*/
                    if oeelk.specnstype <> "n" then do:
                        /*was ex lock*/
                        {w-icsw.i oeelk.shipprod oeel.whse no-lock}
                        {w-icsp.i oeelk.shipprod no-lock}
                    end.
                   else
            /* Added catalog read for product notes display */
                        {w-icsc.i oeelk.shipprod no-lock} .
                     /*tb 18091 11/01/95 gp;  Assign product section
                        of UPC number */
                    if oeelk.specnstype = "n" or oeelk.arpvendno <> 0 then
                        {icsv.gfi "'u'" oeelk.shipprod oeelk.arpvendno no}
                    else if avail icsw then
                        {icsv.gfi "'u'" oeelk.shipprod icsw.arpvendno no}

                    assign
                        s-upcpno    = if avail icsv and avail sasc then
                                          {upcno.gas &section  = "4"
                                                     &sasc     = "b-sasc."
                                                     &icsv     = "icsv."
                                                     &comdelim = "/*"}
                                       else "00000".

    /********************  print the component  ***********************/
                    clear frame f-oeel.

                    /*tb 1/24/91 mwb; ship comp, tag&hold always uses ordered */
                    /*tb 2277 9/11/91 kmw; stkqtyord --> qtyord */
                    assign
                        s-qtyord  = if oeel.returnfl then
                                        if substring
                                        (string(oeelk.qtyord,"9999999.99-"),9,2)
                                        = "00"
                                        then
                                        string((oeelk.qtyord * -1),"zzzzzz9-")
                                        else
                                        string(oeelk.qtyord * -1,"zzzzzz9.99-")
                                    else if substring
                                        (string(oeelk.qtyord,"9999999.99-"),9,2)
                                        = "00"
                                        then string(oeelk.qtyord,"zzzzzz9-")
                                    else string(oeelk.qtyord,"zzzzzz9.99-")
                        s-qtyship  = if oeel.returnfl then
                                         if substring(string
                                             (oeelk.qtyship,"9999999.99-"),9,2)
                                             = "00"
                                             then string(
                                             (oeelk.qtyship * -1),"zzzzzz9-")
                                         else string
                                             (oeelk.qtyship * -1,"zzzzzz9.99-")
                                     else if substring(string
                                         (oeelk.qtyship,"9999999.99-"),9,2)
                                         = "00"
                                         then string(oeelk.qtyship,"zzzzzz9-")
                                     else string(oeelk.qtyship,"zzzzzz9.99-")

                        s-qtyship  = if can-do("s,t",oeeh.orderdisp) then
                                         s-qtyord
                                     else s-qtyship.

                    /*tb 7245 08/05/92 pap; Do not display shipped qty*/
                    /* REVERSAL OF TB 7245 08/12/92 DESIGN MEETING    */
                    /*tb 18575 06/23/95 ajw; Nonstock/Special Kit Comp*/
                    /*tb 18091 11/01/95 gp;  Change display of UPC number */
                    display
                        oeelk.shipprod   @ oeel.shipprod
                        s-qtyord

                        s-qtyship
                        s-upcpno
                        oeelk.unit       @ oeel.unit
                        "Kit"            @ s-lineno
                    with frame f-oeel.

                    /*tb 18575 06/23/95 ajw; Nonstock/Special Kit Component.*/
                    if oeelk.specnstype = "n" then
                        display oeelk.proddesc @ s-descrip with frame f-oeel.
                    else if avail icsp and avail icsw then
                        display
                            icsp.descrip[1] + " " + icsp.descrip[2] @ s-descrip
                        with frame f-oeel.
                    else display v-invalid @ s-descrip with frame f-oeel.
                    down with frame f-oeel.
/* SX32
                /**************    print the product notes  *******************/
                    /*tb 17771 08/17/95 tdd; Notes print twice when reference
                        component */
                    if avail icsp and icsp.notesfl ne "" then do:
                        for each notes where
                            notes.cono         = g-cono    and
                            notes.notestype    = "p"       and
                            notes.primarykey   = icsp.prod and
                            notes.secondarykey = ""
                        no-lock:

                            if notes.printfl = yes then do i = 1 to 16:
                                /*tb 13890 02/21/94 dww; File name not specified
                                    causing error */
                                if notes.noteln[i] ne "" then do:
                                    display notes.noteln[i] with frame f-notes.
                                    down with frame f-notes.
                                end.

                            end.

                        end.

                    end. /* if avail icsp and icsp.notesfl ne "" */
SX32 */
 /**************    print the product notes  *******************/
 /*tb 17771 08/17/95 tdd; Notes print twice when reference
                                             component */
                if oeelk.specnstype ne "n":u and
                   avail icsp and icsp.notesfl ne "" then
                   run notesprt.p(g-cono,"p",icsp.prod,"","oeepa",8,1).
 
                else if oeelk.specnstype = "n":u
                then do:
                    {icsc.gfi &prod = "oeelk.shipprod"
                     &lock = "no"}
                if avail icsc and icsc.notesfl ne "" then
                  run notesprt.p(1,"g",icsc.catalog,"","oeepa",8,1).
                end. /* non-stock catalog notes */
 
 

                end. /* else do */

            end. /* for each oeelk */

    /****************** subtotals **********************/
        if oeel.subtotalfl then do:
            assign
                v-subnetdisp = string(v-subnetamt,"zzzzzzzz9.99-")
                v-subnetamt  = 0.

            display v-subnetdisp with frame f-oeelsub.
        end.

    end. /* for each oeel */





end. /* if oeeh.transtype <> "ra" */

/************************ order totals ********************/
s-totqtyshp = if substring(string(v-totqtyshp,"999999999.99-"),11,2) = "00" then
                  string(v-totqtyshp,"zzzzzzzz9-")
              else string(v-totqtyshp,"zzzzzzzz9.99-").




if oeeh.transtype <> "ra" then do:
    clear frame f-tot1.

    /*tb 7245 08/05/92 pap; Eliminate shipped qty from display  */
    /* REVERSAL OF TB 7245 08/12/92 BY DESIGN MEETING           */
    
    /*si02 begin*/
    if (substr(oeeh.user5,1,1) = "c") or
      (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then 
    display
        v-linecnt
        s-lit40a

        s-lit40c            /*    ***** Qty Shipped Total *****     */
        s-totqtyshp

        s-lit40d
        s-cantot @ s-totlineamt
    with frame f-tot1.
    else
    /*SI02 end*/
    display
        v-linecnt
        s-lit40a

        s-lit40c            /*    ***** Qty Shipped Total *****     */
        s-totqtyshp

        s-lit40d
        s-totlineamt
    with frame f-tot1.



    if oeeh.wodiscamt <> 0 then do:
        clear frame f-tot2.

        assign
            s-title2 = s-lit41a
            s-amt2   = oeeh.wodiscamt *
                       (if oeeh.transtype = "rm" then -1 else 1) *
                       /*si02 added "if substr..."*/
                       (if (substr(oeeh.user5,1,1) = "c"
                            and oeeh.user6 <> 0 ) or
                         (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
   
                        then oeeh.user6
                            else 1)
            s-cantot = s-cantot + s-amt2.    /*si02*/

        display
            s-title2
            s-amt2
        with frame f-tot2.
        down with frame f-tot2.
    end.

    if oeeh.specdiscamt <> 0 then do:
        clear frame f-tot2.

        assign
            s-title2 = s-lit42a
            s-amt2   = oeeh.specdiscamt *
                       (if oeeh.transtype = "rm" then -1 else 1) *
                       /*si02 added "if substr..."*/
                       (if (substr(oeeh.user5,1,1) = "c"
                            and oeeh.user6 <> 0) or
                           (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
  
                        then oeeh.user6
                            else 1)
            s-cantot = s-cantot + s-amt2.    /*si02*/

        display
            s-title2
            s-amt2
        with frame f-tot2.
        down with frame f-tot2.
    end.

    if oeeh.totcorechg <> 0 then do:
        clear frame f-tot2.

        assign
            s-title2 = s-lit43a
            s-amt2   = oeeh.totcorechg *
                       (if oeeh.transtype = "rm" then -1 else 1) *
                       /*si02 added "if substr..."*/
                       (if (substr(oeeh.user5,1,1) = "c"
                            and oeeh.user6 <> 0) or
                          (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
  
                        then oeeh.user6
                            else 1)
            s-cantot = s-cantot + s-amt2.    /*si02*/

        display
            s-title2
            s-amt2
        with frame f-tot2.
        down with frame f-tot2.
    end.

    if oeeh.totrestkamt <> 0 then do:
        clear frame f-tot2.

        assign
            s-title2 = s-lit53a
            s-amt2   = oeeh.totrestkamt *
                       (if oeeh.transtype = "rm" then -1 else 1) *
                       /*si02 added "if substr..."*/
                       (if (substr(oeeh.user5,1,1) = "c"
                            and oeeh.user6 <> 0) or
                         (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
    
                        then oeeh.user6
                            else 1)
            s-cantot = s-cantot + s-amt2.    /*si02*/



        display
            s-title2
            s-amt2
        with frame f-tot2.
        down with frame f-tot2.
    end.

    /*  ITB# 2 */
 
    assign s-amt2 = 0
           zx-handle = false.
    do i = 1 to 4:
       assign s-amt2 = s-amt2 + oeeh.addonnet[i].
       if oeeh.addonno[i] = 5 and
          oeeh.addonnet[i] <> 0 then
         assign zx-handle = true. 
    end.

    if s-amt2 = 0 and
       oeeh.stagecd >= 0 and oeeh.stagecd <= 2 and
       oeeh.termstype <> "cod" and
       (zx-handle = true or
        oeeh.inbndfrtfl = true or
        oeeh.outbndfrtfl = true) then
      do:
      assign s-title2 = "Frt/Hndlg to be added at shipping"
             s-amt2   = 0.
      display
          s-title2
      with frame f-tot2a.
      down with frame f-tot2a.
      end.
    else
    if s-amt2 ne 0 then do:
       clear frame f-tot2.
       if oeeh.stagecd >= 0 and oeeh.stagecd <= 2 and
          oeeh.termstype <> "cod" then
         do:
         assign s-title2 = "Frt/Hndlg to be added at shipping"
                s-amt2   = 0.
         display
           s-title2
          with frame f-tot2a.
          down with frame f-tot2.
          end.
       else
         do:
         assign s-title2 = "Frt & Hndlg"
                s-amt2 = s-amt2 * (if oeeh.transtype = "rm" then -1 else 1)
                  /*si02 added "if substr..."*/
                  * (if (substr(oeeh.user5,1,1) = "c"
                       and oeeh.user6 <> 0) or
                     (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
   
                   then oeeh.user6
                       else 1)
                s-cantot = s-cantot + s-amt2.    /*si02*/
        display
           s-title2
           s-amt2
        with frame f-tot2.
        down with frame f-tot2.
        end.
    end.

/* SurCharge   ---------------------------------------------------------
    /*tb 8561 11/11/92 mms */
    if oeeh.totdatccost <> 0 and v-icdatclabel <> "" then do:
        clear frame f-tot2.

        assign
            s-title2 = s-lit44a
            s-amt2   = oeeh.totdatccost *
                       (if oeeh.transtype = "rm" then -1 else 1) *
                       /*si02 added "if substr..."*/
                       (if (substr(oeeh.user5,1,1) = "c"
                            and oeeh.user6 <> 0) or
                         (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
    
                        then oeeh.user6
                            else 1)
            s-cantot = s-cantot + s-amt2.    /*si02*/


        display
            s-title2
            s-amt2
        with frame f-tot2.
        down with frame f-tot2.
    end.
*/
    if g-country = "ca" 
       or substr(oeeh.user5,1,1) = "C"  /*SI02*/ then do:
        /* print gst tax amount */
        if oeeh.taxamt[4] <> 0 then do:
            clear frame f-tot2.

            assign
                s-title2 = s-lit49a
                s-amt2   = oeeh.taxamt[4] *
                           (if oeeh.transtype = "rm" then -1 else 1) *
                           /*si02 added "if substr..."*/
                           (if substr(oeeh.user5,1,1) = "c"
                                and oeeh.user6 <> 0 
                            then oeeh.user6
                                else 1)
                s-cantot = s-cantot + s-amt2.    /*si02*/
                           

            display
                s-title2
                s-amt2
            with frame f-tot2.
            down with frame f-tot2.
        end.

        /* print pst tax amount */
        if oeeh.taxamt[1] <> 0 then do:
            clear frame f-tot2.

            assign
                s-title2 = s-lit50a
                s-amt2   = oeeh.taxamt[1] *
                           (if oeeh.transtype = "rm" then -1 else 1) *
                           /*si02 added "if substr..."*/
                           (if substr(oeeh.user5,1,1) = "c"
                                and oeeh.user6 <> 0 
                            then oeeh.user6
                                else 1)
                s-cantot = s-cantot + s-amt2.    /*si02*/


            display
                s-title2
                s-amt2
            with frame f-tot2.
            down with frame f-tot2.
        end.

    end. /* if g-country = "ca" */

    else if s-taxes <> 0 then do:
        clear frame f-tot2.

        assign
            s-title2 = s-lit45a
            s-amt2   = s-taxes *
                       (if oeeh.transtype = "rm" then -1 else 1) *
                       /*si02 added "if substr..."*/
                          (if (substr(oeeh.user5,1,1) = "c" 
                               and oeeh.user6 <> 0) or
                            (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
    
                            then oeeh.user6
                            else 1)
            s-cantax1 = if {zzcandiv.zvl arsc.divno} and     
                                  substr(oeeh.user5,1,1) <> "c"     
                               then oeeh.taxamt[4] * 
                                    (if oeeh.user6 <> 0             
                                     then oeeh.user6                
                                     else 1)                        
                               else 0                               
            s-cantax2 = if {zzcandiv.zvl arsc.divno} and
                                  substr(oeeh.user5,1,1) <> "c"
                               then oeeh.taxamt[1] *
                                    (if oeeh.user6 <> 0
                                     then oeeh.user6
                                     else 1)
                               else 0
            s-canlit = if {zzcandiv.zvl arsc.divno} and     
                                 substr(oeeh.user5,1,1) <> "c"     
                              then "CANADIAN TAX AMOUNT: G.S.T.:" + 
                                   string(s-cantax1,"zzzzz9.99-") + 
                                   "  P.S.T.:" + 
                                   string(s-cantax2,"zzzzz9.99-")
                              else ""     
            s-cantot = s-cantot + s-amt2.    
            /*si02*/
        display
            s-canlit  /*SI02*/
            s-title2
            s-amt2
        with frame f-tot2.
        down with frame f-tot2.
    end.

    if s-dwnpmtamt <> 0 then do:
        clear frame f-tot2.

        assign
            s-title2 = if can-do("cs",oeeh.transtype) then s-lit46b
                       else s-lit46a
            s-amt2   = s-dwnpmtamt *
                       (if oeeh.transtype = "rm" then -1 else 1) *
                       /*si02 added "if substr..."*/
                      (if (substr(oeeh.user5,1,1) = "c"
                            and oeeh.user6 <> 0) or
                        (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
     
                        then oeeh.user6
                            else 1)
            s-cantot = s-cantot + s-amt2.    /*si02*/

        display
            s-title2
            s-amt2
        with frame f-tot2.
        down with frame f-tot2.
    end.


    
    /* smaf  */
    
    if oeeh.taxablefl = yes then
       assign s-taxtitle = "Tax Status:"
              s-vartax = "TAXABLE".
    else
       assign s-taxtitle = "Tax Status:"
              s-vartax = "NON-TAXABLE".
    
    display 
      s-taxtitle 
      s-vartax
    with frame f-vartax.
    down with frame f-vartax.


end. /* if oeeh.transtype <> "ra" */

clear frame f-tot3.

/*tb 5413 06/20/92 mwb; added the writeoffamt to totinvamt */
assign
    s-totinvamt = if oeeh.transtype = "ra" then oeeh.tendamt
                  else (oeeh.totinvamt - oeeh.tendamt - oeeh.writeoffamt) *
                      (if oeeh.transtype = "rm" then -1 else 1)
    s-invtitle  = if oeeh.transtype = "ra" then s-lit47b else s-lit47a
    /*si02 added s-currencyty*/
    s-currencyty = if ({zzcandiv.zvl arsc.divno}) or
                      (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                      then 
                            if substr(oeeh.user5,1,1) = "c" then
                       "**** ACKNOWLEDGEMENT REFLECTS CANADIAN CURRENCY ***"
                           else
                           if (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then

                       "**** ACKNOWLEDGEMENT REFLECTS ENGLISH POUNDS  ***"

                            else 
                       "  **** ACKNOWLEDGEMENT REFLECTS U.S. CURRENCY ****"
                          else "".
    s-totinvamt = if (substr(oeeh.user5,1,1) = "c") or
                     (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then 
                       s-cantot
                         else s-totinvamt.   /*si02*/
    
    /* don't include freight and handling */
    if oeeh.stagecd >= 0 and 
       oeeh.stagecd <= 2 and
       s-totinvamt  >  0 then do:
         if (substr(oeeh.user5,1,1) = "c") or
                      (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then
            s-totinvamt = s-cantot.
         else              
           do i = 1 to 4:
             assign s-totinvamt = s-totinvamt - oeeh.addonnet[i].
           end.
       if s-totinvamt < 0 then
          assign s-totinvamt = 0.
    end.

display
    s-currencyty  /*Si02*/
    s-invtitle
    s-totinvamt
with frame f-tot3.

/*tb# 5929 06/18/92 mkb */
if oeeh.tottendamt ne 0 then do:
   clear frame f-tot5.

   if oeeh.boexistsfl = no and
       (oeeh.orderdisp = "j"  or oeeh.transtype = "bl")
   then do:
       find first b-oeeh use-index k-oeeh where
           b-oeeh.cono       = oeeh.cono         and
           b-oeeh.orderno    = oeeh.orderno      and
           b-oeeh.ordersuf   > 0
       no-lock no-error.

       if avail b-oeeh then
           display oeeh.tottendamt 
                  /*SI02 beg*/
                  * (if (substr(oeeh.user5,1,1) = "c" 
                     and oeeh.user6 <> 0) or
                    (oeeh.currencyty = "lb" and oeeh.user6 <> 0)

                     then oeeh.user6 
                      else 1) /*SI02 end*/
           with frame f-tot5.
   end.

   else if oeeh.boexists = yes then
       display oeeh.tottendamt 
                  /*SI02 beg*/
                  * (if (substr(oeeh.user5,1,1) = "c" 
                     and oeeh.user6 <> 0 ) or
                     (oeeh.currencyty = "lb" and oeeh.user6 <> 0)

                     then oeeh.user6 
                      else 1) /*SI02 end*/
       with frame f-tot5.
end.

assign
    s-lit48a = "Last Page"
    s-cantot = 0.   /*si02*/
    
find first oimsp where oimsp.person = oeeh.takenby no-lock no-error.
if avail(oimsp) then
   assign s-CSR-title  = "Customer Service Contact:"
          s-contact    = oimsp.name
          s-contlit-ph = "TEL:"
          s-contact-ph = oimsp.phoneno
          s-contlit-fx = "FAX:"
          s-contact-fx = oimsp.faxphoneno.
else
   assign s-CSR-title  = ""
          s-contact    = ""
          s-contlit-ph = ""
          s-contact-ph = ""
          s-contlit-fx = ""
          s-contact-fx = "".
 
 
