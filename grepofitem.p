define input parameter zx-prod like icsp.prod                          no-undo.
define input parameter zx-hash as char                                 no-undo.
define input-output parameter zx-valid as logical                      no-undo.


define var x-result     as char      format "x(24)"             no-undo.
define var x-hashlen    as integer                              no-undo.
define var x-prodlen    as integer                              no-undo.
define var x-prod       as character format "x(24)"             no-undo.
define var x-hash       as character format "x(24)"             no-undo.
define var x-deadend    as character format "x(2)"              no-undo.
define var x-anchor     as logical                              no-undo.

assign x-hashlen = length(zx-hash)
       x-prodlen = length(zx-prod)
       x-anchor = true.

if x-hashlen > 1 and substring(zx-hash,x-hashlen,1) = "%" then
  assign x-hash = substring(zx-hash,1,x-hashlen - 1)  
         x-prod = substring(zx-prod,1,x-prodlen)
         x-deadend = "  ". 
else
if x-hashlen > 3 and substring(zx-hash,x-hashlen - 2,3) = "[*]" then
  assign x-hash = substring(zx-hash,1,x-hashlen - 3)  
         x-prod = substring(zx-prod,1,x-prodlen)
         x-deadend = "  ". 
else
if x-hashlen > 2 and substring(zx-hash,x-hashlen,1) = "$" then
  assign x-hash = substring(zx-hash,1,x-hashlen)  
         x-prod = substring(zx-prod,1,x-prodlen)
         x-anchor = false
         x-deadend = "  ". 
         
else
  assign x-hash = zx-hash
         x-prod = zx-prod
         x-deadend = "-x".

if x-prodlen > 20 then
   x-prodlen = 20.

input through value ( "echo " + "`" + 'print ' + 
                      substring(x-prod,1,x-prodlen) + 
                     ' | grep -i ' +
                     x-deadend + ' ' +
                     (if x-anchor then
                        "^"
                      else
                        "")
                      + x-hash + "`" ) no-echo.


import unformatted x-result.
input close.

if x-result = substring(x-prod,1,x-prodlen) then
  zx-valid = true.
else
  zx-valid = false.
