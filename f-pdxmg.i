/****************************************************************************
*                                                                           *
* f-pdxm.i - General Matrix Margin Report Forms                             *
*                                                                           *
****************************************************************************/


form 
"Prod"                  at 1
"Cust"                  at 23
"<--------"             at 72
x-mo-1                  at 82
"-------->"             at 95
"<---------"            at 104
x-mo-1ytd               at 115
"YTD"                   at 125
"--------->"            at 129
"<--------"             at 140
x-mo-2                  at 150
"YA"                    at 160
"YTD"                   at 163
"------->"              at 167
"Price"                 at 1
"Price"                 at 7
"Price"                 at 23
"Matrix"                at 29
"Reg"                   at 37
"Customer"              at 42
"Ship"                  at 55
"Matrix"                at 85 
"Invoiced"              at 94
"Matrix"                at 120
"Invoiced"              at 130
"Matrix"                at 156
"Invoiced"              at 166
"Type"                  at 1
"Description"           at 7
"Type"                  at 23
"Multplr"               at 29
"Dist"                  at 37
"Number"                at 42
"To"                    at 55
"Name"                  at 60
"Sales"                 at 77
"Margin%"               at 85
"Margin%"               at 95
"Sales"                 at 112
"Margin%"               at 120
"Margin%"               at 131
"Sales"                 at 148
"Margin%"               at 156
"Margin%"               at 167
"-----"                 at 1
"---------------"       at 7
"-----"                 at 23
"-------"               at 29
"----"                  at 37
"------------"          at 42
"----"                  at 55
"----------"            at 60
"----------"            at 72
"---------"             at 83
"----------"            at 93
"-------------"         at 104
"---------"             at 119 
"---------"             at 130
"-------------"         at 140
"---------"             at 155
"---------"             at 166
with frame f-header no-underline no-box no-labels page-top down width 178.

form
s-pprice                at 1
s-price-desc            at 7
s-cprice                at 23
s-pct                   at 29
s-rgn                   at 37
s-custno                at 42
s-shipto                at 55
s-name                  at 60
s-mo-isales             at 71
s-mo-mmarg              at 83
s-mo-imarg              at 94
s-ytd-isales            at 106
s-ytd-mmarg             at 119
s-ytd-imarg             at 130
s-lmo-isales            at 142
s-lmo-mmarg             at 155
s-lmo-imarg             at 166
with frame f-detail no-underline no-box no-labels down width 178.

form
skip (1)
"Totals for:"           at 1
s-rgn                   at 29
s-mo-tisales            at 70
s-mo-tmmarg             at 83
s-mo-timarg             at 94
s-ytd-tisales           at 106
s-ytd-tmmarg            at 119
s-ytd-timarg            at 130
s-lmo-tisales           at 142
s-lmo-tmmarg            at 155
s-lmo-timarg            at 166
with frame f-district no-underline no-box no-labels down width 178.

form                           
skip (1)                       
"Totals for:"           at 1   
r-rgn                   at 29  
r-mo-tisales            at 70  
r-mo-tmmarg             at 83  
r-mo-timarg             at 94  
r-ytd-tisales           at 106  
r-ytd-tmmarg            at 119 
r-ytd-timarg            at 130 
r-lmo-tisales           at 142 
r-lmo-tmmarg            at 155 
r-lmo-timarg            at 166 
with frame f-region no-underline no-box no-labels down width 178.


form
"Grand Totals:"         at 38
g-mo-tisales            at 70 
g-mo-tmmarg             at 83 
g-mo-timarg             at 94 
g-ytd-tisales           at 106
g-ytd-tmmarg            at 119 
g-ytd-timarg            at 130 
g-lmo-tisales           at 142
g-lmo-tmmarg            at 155
g-lmo-timarg            at 166
with frame f-grand no-underline no-box no-labels down width 178.


form
"Cust"                  at 1
"Prod"                  at 7
"Price"                 at 1
"Price"                 at 7
"Reg"                   at 14
"Customer"              at 20
"Customer"              at 43
"Order"                 at 67
"Order"                 at 77
"Invoice"               at 84
"Sales"                 at 97
"Matrix"                at 110
"Invoiced"              at 121
"Type"                  at 1
"Type"                  at 7
"Dist"                  at 14
"Number"                at 20
"Ship To"               at 33
"Name"                  at 43
"Number"                at 67
"Line"                  at 77
"Date"                  at 84
"Amount"                at 97
"Margin%"               at 110
"Margin%"               at 121
"Product"               at 131
"-----"                 at 1
"-----"                 at 7
"----"                  at 14
"------------"          at 20
"--------"              at 33
"--------------------"  at 43
"----------"            at 65
"-----"                 at 77
"--------"              at 84
"------------"          at 94
"---------"             at 108
"---------"             at 120
"---------------------" at 131
with frame f-dd-header no-underline no-box no-labels page-top down width 178.


form
s-cprice                at 1  
s-pprice                at 7  
s-rgn                   at 14 
s-custno                at 20
s-shipto                at 33
s-name                  at 43
s-order                 at 65
s-line                  at 78
s-date                  at 84
s-isales                at 96 
s-mmarg                 at 109 
s-imarg                 at 121
oeel.shipprod           at 131
with frame f-dd-detail no-underline no-box no-labels down width 178.

form
skip(1)
"Totals for"            at 43
w-year                  at 54
s-ytd-isales            at 94
s-ytd-mmarg             at 108
s-ytd-imarg             at 120
skip(1)
with frame f-dd-total no-underline no-box no-labels down width 178.


form 
w-errmsg                at 5 label "Error"
with frame f-error side-labels no-box.

       
