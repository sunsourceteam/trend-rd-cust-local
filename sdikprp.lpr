/*zkprp.lpr 1.1  05/19/00*/
/*h****************************************************************************
  INCLUDE      : zkprp.lpr
  DESCRIPTION  : Kit Production - Production Schedule Report
  USED ONCE?   : yes
  AUTHOR       : SD
  DATE WRITTEN : 06/02/00  
  CHANGES MADE :
   06/02/00  SD: 206890 Original development of new custom report - Print logic
******************************************************************************/
assign
    s-whsetotcnt = 0
    s-whsetothrs = 0
    s-whsetotval = 0
    s-techtotcnt = 0
    s-techtothrs = 0
    s-techtotcnt-gtl = 0
    s-techtothrs-gtl = 0
    s-techtotval = 0
    s-totalcnt   = 0
    s-totalhrs   = 0
    s-totalval   = 0
    s-scheddt    = ?.

if p-schedonly = yes then
   for each t-kprp  use-index k-whsetech 
       where t-kprp.technician = "":
       delete t-kprp.
   end.    
    

if p-whsesrtfl = yes and p-showtechfl = yes then do:
    for each t-kprp no-lock use-index k-schedule 
       where t-kprp.technician >= b-tech and
             t-kprp.technician <= e-tech and

             ((t-kprp.scheddt le p-reportthru  or 
               t-kprp.scheddt = ?) and
               t-kprp.blddt ge b-requestdt and
               t-kprp.blddt le e-requestdt) and
             
             ((( t-kprp.stkqtyord le t-kprp.stkqtyship or
                 t-kprp.technician = "tdns") and p-bldable) or
                 not p-bldable)
       break by t-kprp.whse 
             by t-kprp.technician 
             by t-kprp.scheddt
             by t-kprp.requestdt3 
             by t-kprp.seqno:

        /* 06/29/00 yl; print the whse total on the last tech's page;
                            so page break on the first-of (tech) */
        if first-of(t-kprp.technician) then 
           page. 

        {sdikprp.las}
        {sdikprp.ldi}   

        if p-printmatfl then do:
            {zkprpcomp.i}
        end.

        if last-of(t-kprp.technician) then 
        do:
            display s-techlabel
                    s-techtotcnt
                    s-techtothrs
                    s-scheddt 
               /* s-techtotval  when p-showtotfl klt */
                    with frame f-kprpe.
            assign s-techtotcnt-gtl = s-techtotcnt-gtl + s-techtotcnt 
                   s-techtothrs-gtl = s-techtothrs-gtl + s-techtothrs.
            display s-techlabel
                    s-techtotcnt-gtl
                    s-techtothrs-gtl
                    with frame f-kprp-gtl.
             assign s-techtotcnt = 0
                    s-techtothrs = 0
                    s-techtotcnt-gtl = 0
                    s-techtothrs-gtl = 0
                    s-techtotval = 0.
        end. /* last-of tech */
        else 
        if last-of(t-kprp.scheddt) then 
        do:
            display s-techlabel
                    s-techtotcnt
                    s-techtothrs
                    s-scheddt
               /* s-techtotval  when p-showtotfl klt */
                    with frame f-kprpe.
            assign s-techtotcnt-gtl = s-techtotcnt-gtl + s-techtotcnt 
                   s-techtothrs-gtl = s-techtothrs-gtl + s-techtothrs
                   s-techtotcnt = 0
                   s-techtothrs = 0
                   s-techtotval = 0.
        end. /* last-of tech */
         
        if last-of(t-kprp.whse) then
        do:
            display s-whselabel
                    s-whsetotcnt
                    s-whsetothrs
                    with frame f-kprpw.
            assign s-whsetotcnt = 0
                   s-whsetothrs = 0
                   s-whsetotval = 0.
        end. /* last-of whse */

    end. /* for each t-kprp */
end. /* yes,yes */

if p-whsesrtfl = no and p-showtechfl = yes then do:
    for each t-kprp no-lock use-index k-technician 
           where t-kprp.technician >= b-tech and
                 t-kprp.technician <= e-tech and
             
             ((t-kprp.scheddt le p-reportthru  or 
               t-kprp.scheddt = ?) and
               t-kprp.blddt ge b-requestdt and
               t-kprp.blddt le e-requestdt) and
              
             ((( t-kprp.stkqtyord le t-kprp.stkqtyship and
                 t-kprp.technician <> "tdns") and p-bldable) or
                 not p-bldable)
                  

          break by t-kprp.technician
                by t-kprp.scheddt
                by t-kprp.requestdt3
                by t-kprp.seqno:

        /* 06/29/00 yl; print the whse total on the last tech's page;
                        so page break on the first-of (tech) */
      
        if first-of(t-kprp.technician) or 
           first-of(t-kprp.scheddt) then page.

        {sdikprp.las}
        {sdikprp.ldi}
        
        if p-printmatfl then do:
            {zkprpcomp.i}
        end.
        
        if last-of(t-kprp.technician) then 
        do:
            display s-techlabel
                    s-techtotcnt
                    s-techtothrs
                    s-scheddt
                    with frame f-kprpe.
            assign s-techtotcnt-gtl = s-techtotcnt-gtl + s-techtotcnt 
                   s-techtothrs-gtl = s-techtothrs-gtl + s-techtothrs.
            display s-techlabel
                    s-techtotcnt-gtl
                    s-techtothrs-gtl
                    with frame f-kprp-gtl.
            assign  s-techtotcnt = 0
                    s-techtothrs = 0
                    s-techtotval = 0
                    s-techtotcnt-gtl = 0
                    s-techtothrs-gtl = 0.
        end. /* last-of tech */
        else 
        if last-of(t-kprp.scheddt) then 
        do:
            display s-techlabel
                    s-techtotcnt
                    s-techtothrs
                    s-scheddt
                    with frame f-kprpe.
            assign s-techtotcnt-gtl = s-techtotcnt-gtl + s-techtotcnt 
                   s-techtothrs-gtl = s-techtothrs-gtl + s-techtothrs
                   s-techtotcnt = 0
                   s-techtothrs = 0
                   s-techtotval = 0.
        end. /* last-of tech */
 
    end. /* for each t-kprp */                    
end. /* no,yes */                        

if p-whsesrtfl = yes and p-showtechfl = no then do:
    for each t-kprp no-lock use-index k-whse 
       where t-kprp.technician >= b-tech and
             t-kprp.technician <= e-tech and

             ((t-kprp.scheddt le p-reportthru  or 
               t-kprp.scheddt = ?) and
               t-kprp.blddt ge b-requestdt and
               t-kprp.blddt le e-requestdt) and
  
             ((( t-kprp.stkqtyord le t-kprp.stkqtyship or
                 t-kprp.technician = "tdns") and p-bldable) or
                 not p-bldable)
                  
       break by t-kprp.whse 
             by t-kprp.seqno
             by t-kprp.requestdt:

        /* 06/29/00 yl; print the grand total on the last whse's page;
                        so page break on the first-of (whse) */
        if first-of(t-kprp.whse) then page.

        {sdikprp.las}
        {sdikprp.ldi}  
        
        if p-printmatfl then do:
            {zkprpcomp.i}           
        end.
        
        if last-of(t-kprp.whse) then do:
            display s-whselabel
                    s-whsetotcnt       
                    s-whsetothrs       
             /*   s-whsetotval  when p-showtotfl      klt */
                    with frame f-kprpw.    
                                               
            assign s-whsetotcnt = 0   
                   s-whsetothrs = 0   
                   s-whsetotval = 0.  
        end. /* last-of whse */
                                                            
    end. /* for each t-kprp */                        
end. /* yes,no */             

if p-whsesrtfl = no and p-showtechfl = no and 
   p-srtkprod  = no then 
  do:
    for each t-kprp use-index k-seqno 
       where t-kprp.technician >= b-tech and
             t-kprp.technician <= e-tech and

             ((t-kprp.scheddt le p-reportthru  or 
               t-kprp.scheddt = ?) and
               t-kprp.blddt ge b-requestdt and
               t-kprp.blddt le e-requestdt) and
  
             ((( t-kprp.stkqtyord le t-kprp.stkqtyship or
                 t-kprp.technician = "tdns") and p-bldable) or
                 not p-bldable)
                  
     no-lock:
        {sdikprp.las}
        {sdikprp.ldi}
        
        if p-printmatfl then do:
            {zkprpcomp.i}
        end.
        
  end. /* for each t-kprp */
end. /* no,no */   
            
if p-whsesrtfl = no and p-showtechfl = no and 
   p-srtkprod  = yes then 
  do:
    for each t-kprp use-index k-kitprod 
       where t-kprp.technician >= b-tech and
             t-kprp.technician <= e-tech and

             ((t-kprp.scheddt le p-reportthru  or 
               t-kprp.scheddt = ?) and
               t-kprp.blddt ge b-requestdt and
               t-kprp.blddt le e-requestdt) and
  
             ((( t-kprp.stkqtyord le t-kprp.stkqtyship or
                 t-kprp.technician = "tdns") and p-bldable) or
                 not p-bldable)
                  
     no-lock:
        {sdikprp.las}
        {sdikprp.ldi}
        
        if p-printmatfl then do:
            {zkprpcomp.i}
        end.
        
  end. /* for each t-kprp */
end. /* no,no */                
            
