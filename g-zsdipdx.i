def {1}  var  pd-whse       like icsw.whse                no-undo.
def {1}  var  pd-prod       like icsw.prod                no-undo.
def {1}  var  pd-cust       like arsc.custno              no-undo.
def {1}  var  pd-shipto     like arss.shipto              no-undo.
def {1}  var  pd-totnet     as dec format ">>>,>>9.99-"   no-undo.
def {1}  var  pd-totcost    as dec format ">>>,>>9.99-"   no-undo.
def {1}  var  pd-vendrebamt as dec format ">>>,>>9.99-"   no-undo.
def {1}  var  pd-margin     as dec format ">>>,>>9.99-"   no-undo.
def {1}  var  pd-margpct    as dec format ">>>,>>9.99-"   no-undo.
def {1}  var  pd-netunit    as dec format ">>>,>>9.99-"   no-undo.
def {1}  var  pd-pdrecno    like pdsc.pdrecno             no-undo.
def {1}  var  pd-levelcd    like pdsc.levelcd             no-undo.
def {1}  var  pd-price      as dec format ">>>,>>9.99-"   no-undo.
