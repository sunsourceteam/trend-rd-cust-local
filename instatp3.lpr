/* instatp3.lpr 1.1 01/03/98 */
/* instatp3.lpr 1.1 08/16/97 */
/*h****************************************************************************
  INCLUDE      : instatp3.lpr
  DESCRIPTION  : Static Extract - Product Gateway - Product and Product
                     Warehouse Only (P-ic types 1 and 2)
  USED ONCE?   :
  AUTHOR       : jkp
  DATE WRITTEN : 08/16/97
  CHANGES MADE :
    08/16/97 jkp; TB# 23605 New Include.  Code taken from instatx3.p to allow
        the passing of the case parameter.
    11/06/97 jkp; TB# 23938 Do not allow question marks to be output.    
******************************************************************************/

/*o All fields must be put out as character fields.  So if a field is numeric
    or is a date, it must use the "string" parameter to make it a character 
    field.  For those fields that truly are a character field, i.e., they 
    don't need the "string" parameter, they must have parameter 4 in front 
    of them to get them in the correct case.  In addition, all signed fields
    must have the sign on the left.  It is imperative that no question marks
    are output, so every field must be checked to make sure it is not a
    question mark, even though that would be corrupt data except for a date
    field.  This is because the analyzer process cannot handle question marks
    and it is SQL code, so it cannot even give a graceful error - it just
    aborts.  Some of these checks are made in instatx3.p and some are made 
    here. */

/* Passing parameters definition *********************************************
   {&type}     Selection of what type of product gateway to output.  This is
               controlled by the p-ic SASSR Option as listed below:
               (Not used in this include:
                    3 = Product Category  (instatc3.lpr)
                    4 = Product Line  (instatl3.lpr)
                    5 = Product Line Warehouse  (instatl3.lpr))
               1 = Product
               2 = Product Warehouse
   {&case}     Selection of the case sensitivity.  This is passed by a group
               of if then else statements and is set to either "caps", "lc", 
               or " " depending on how the user answered the p-casety option 
               on the SASSR.
                    
   Sample include calls might be:
       {insaexp3.lpr 
           &type     = "1"
           &case     = "caps"}     
           (Will output in the Product format and all character fields will
            be in all upper case letters)
       {insaexp3.lpr 
           &type     = "2"
           &case     = "lc"}
           (Will output in the Product Warehouse format and all character 
            fields be in all lower case letters)
       {insaexp3.lpr 
           &type     = "1" 
           &case     = " "}
           (Will output in the Product format and all character fields
            will be output, without changing the case)
******************************************************************************/

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Product ID */
(if "{&type}" = "1" then
    (if icsp.prod <> ? then
        {&case}(icsp.prod)   
     else v-null)
 else if icsw.prod <> ? then
     {&case}(icsw.prod)
 else v-null)
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Product Name */ 
(if "{&type}" = "1" then
    (if icsp.lookupnm <> ? then
        {&case}(icsp.lookupnm)
     else v-null)
 else if v-look <> ? then
     {&case}(v-look)
 else v-null)
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Product Description */
(if "{&type}" = "1" then
    (if icsp.descrip[1] <> ? then
        {&case}(substring(icsp.descrip[1],1,24))            
     else v-null)
 else if v-desc[1] <> ? then
     {&case}(substring(v-desc[1],1,24)) 
 else v-null)
v-del 

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Product Line */
if v-prodline <> ? then
    {&case}(substring(v-prodline,1,6))
else v-null
v-del 

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Product Line Name */
if v-linedesc <> ? then
    {&case}(substring(v-linedesc,1,40)) 
else v-null
v-del 

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Buyer */
if v-buyer <> ? then
    {&case}(v-buyer)
else v-null
v-del 

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Product Category ID */
(if "{&type}" = "1" then
    (if icsp.prodcat <> ? then
        {&case}(icsp.prodcat)
     else v-null)
 else if v-cat <> ? then
     {&case}(v-cat)
 else v-null)
v-del 

/*e Price Type */
(if "{&type}" = "1" then
  {&case}(v-null)
 else 
  {&case}(substring(icsw.pricetype,1,4)))
v-del 

/*e Product Class */
{&case}(v-null)
v-del 

/*e Product Family Group */
{&case}(v-null)
v-del 

/*e Seasonal */
{&case}(v-null)
v-del 

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Kit Type */
(if "{&type}" = "1" then
    (if icsp.kittype <> ? then
        {&case}(icsp.kittype)
     else v-null)
 else if v-kittype <> ? then
     {&case}(v-kittype)
 else v-null)
v-del 

/*e Stock Status */
(if "{&type}" = "1" then
   {&case}(v-null)
 else
   {&case} (string(icsw.statustype)))
v-del 

/*e ARP Vendor */
(if "{&type}" = "1" then
   {&case}(v-null)
 else
   {&case} (string(icsw.arpvendno)))
v-del 
  
/*e Rebate Type */
(if "{&type}" = "1" then
   {&case}(v-null)
 else
   {&case} (string(icsw.rebatety)))

v-del 

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Type 1 = User 1 
    Type 2 = Product Warehouse User 1 */
(if "{&type}" = "1" then
    (if icsp.user1 <> ? then
        {&case}(icsp.user1)
     else v-null)
 else if icsw.user1 <> ? then
     {&case}(icsw.user1)
 else v-null)
v-del 

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Type 1 = User 2 
    Type 2 = Product Warehouse User 2 */
(if "{&type}" = "1" then
    (if icsp.user2 <> ? then
        {&case}(icsp.user2)
     else v-null)
 else if icsw.user2 <> ? then
     {&case}(icsw.user2)
 else v-null)
v-del 

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Type 1 = Product Warehouse User 1 
    Type 2 = User 1 */
if v-user1 <> ? then
    {&case}(v-user1)
else v-null
v-del 

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Type 1 = Product Warehouse User 2 
    Type 2 = User 2 */
if v-user2 <> ? then
    {&case}(v-user2)
else v-user2
v-del 
                                                 
/*e SX User 1 */

(if "{&type}" = "1" then
   {&case}(v-null)
 else
 if icsw.avgcost > 1000000 then
   {&case} ("999999.9999")
 else  
   {&case} (string(icsw.avgcost)))
v-del

/*e SX User 2 */
(if "{&type}" = "1" then
   {&case}(v-null)
 else
 if icsw.replcost > 1000000 then
   {&case} ("999999.9999")
 else  
   {&case} (string(icsw.replcost)))
v-del

/*e SX User 3 */
(if "{&type}" = "1" then
   {&case}(v-null)
 else
 if icsw.stndcost > 1000000 then
   {&case} ("999999.9999")
 else  
   {&case} (string(icsw.stndcost)))
v-del

/*e SX User 4 */
(if "{&type}" = "1" then
   {&case}(v-null)
 else
   {&case} (string(icsw.arptype)))
v-del
 
/*e SX User 5 */
(if "{&type}" = "1" then
   {&case}(v-null)
 else
   {&case} (string(icsw.arpwhse)))
v-del

/*e SX User 6 */
(if "{&type}" = "1" then
   {&case}(v-null)
 else
   {&case}(substring(string(icsw.listprice),1,12)))
v-del

/*e SX User 7 */
{&case}(icsp.slgroup)
v-del

/*e SX User 8 */
{&case}(icsp.statustype)
v-del

/*e SX User 9 */
(if "{&type}" = "2" then 
  
   substring( string(icsw.qtyonhand),1,12)
 else
   string(v-zero))
v-del
 
/*e SX User 10 */
(if "{&type}" = "2" then 
   string((icsw.qtyonhand - icsw.qtyreserv - icsw.qtycommit))
 else
   string(v-zero))

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Type 2 Only = Warehouse */
(if "{&type}" = "2" then 
    v-del 
 else "")
(if "{&type}" = "2" and icsw.whse <> ? then
    {&case}(icsw.whse)
 else "")

(if "{&type}" = "2" then 
    v-del 
 else "")
(if "{&type}" = "2" and icsw.whse <> ? then
    {&case}(string(icsw.binloc1,"xx/xx/xxx/xxx"))
 else "")
(if "{&type}" = "2" then 
    v-del 
 else "")
(if "{&type}" = "2" and icsw.whse <> ? then
    {&case}(string(icsw.binloc2,"xx/xx/xxx/xxx"))
 else "")                      










chr(13) chr(10).



