 

/* zsdi-margin.i 1.1 01/03/98 */
/* zsdi-margin.i 1.2 3/23/93 */
/*h*****************************************************************************
  INCLUDE      : zsdi-margin.i
  DESCRIPTION  : for oeel margin calc
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    03/19/93 kmw; TB# 10080 Margin pricing - added v-marnetdiv
*******************************************************************************/

def var vsdi-marcost                 as dec format "zzzzzz9.99999-"   no-undo.
def var vsdi-mardisc                 like oeel.discamt init 0         no-undo.
def var vsdi-mardiscoth              like oeel.discamtoth             no-undo.
def var vsdi-marnet                  like oeel.netamt                 no-undo.
def var vsdi-marnetdiv               as dec format "zzzzzzzz9.99999-" no-undo.
def var vsdi-marprice                like oeel.price                  no-undo.
def var vsdi-marqty                  as dec format ">>>>>>9.99-"      no-undo.
def var vsdi-exchrate                as dec                           no-undo.
def var vsdi-custrebamt              as dec format ">>>>9.99-"        no-undo.
def var vsdi-vendrebamt              as dec format ">>>>9.99-"        no-undo.
def var ssdi-marginamt               like oeel.netamt                 no-undo.
def var ssdi-marginpct               like oeel.discpct                no-undo.
def var vsdi-oecostsale              like sasc.oecostsale             no-undo.
def var vsdi-smcustrebfl             like sasc.smcustrebfl            no-undo.
def var vsdi-smvendrebfl             like sasc.smcustrebfl            no-undo.



