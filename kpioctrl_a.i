/* oectrl_a.i 04/14/00 */
/*h**************************************************************************
  INCLUDE      : kpiocntl_a.i
  DESCRIPTION  : Include for vaiosl.p 
  USED ONCE?   : yes (x-xxex.i calls vaioctrl_a.i)
  AUTHOR       : das 
  DATE WRITTEN : 10/02/02 
  CHANGES MADE : 
    04/14/00 das; Allow user to create or view CSR comments to buyers when 
                  CNTL-A is pressed.  The Comments appear on the RARR. 
*****************************************************************************/
/* MOVED TO oeiolk.z99
/* Notes on RARR */
   define buffer rarr-com for com.
 def var k-prodcst     as c format "x(12)". 
 def var k-shipfmno    as c format "x(4)". 
 def var k-reqshipdt   as c format "x(8)". 
 def var k-arpvendno   as c format "x(12)". 
 
/* def shared var g-operinit as c format "x(4)". */
                                                                                
 def var r-shipprod    like vaesl.shipprod           no-undo. 
 def var r-arpvendno   like vaesl.arpvendno          no-undo. 
 def var r-prodcst     like vaesl.prodcost           no-undo. 
 def var r-shipfmno    as i format "9999"            no-undo. 
 def var r-reqshipdt   as date format "99/99/99"     no-undo. 
 def var r-arpprodline like vaesl.arpprodline        no-undo. 
 def var r-shipvia     as c format "x(4)"            no-undo. 
 def var r-whse        as c format "x(4)"            no-undo. 
 def var r-rush        as logical                    no-undo. 
 def var r-comments1   as c format "x(60)"           no-undo.
 def var r-comments2   as c format "x(60)"           no-undo.
 def var r-comments3   as c format "x(60)"           no-undo.
 def var r-comments4   as c format "x(60)"           no-undo. 
 def var r-comments5   as c format "x(60)"           no-undo.
 def var r-comments6   as c format "x(60)"           no-undo. 
 def var r-comments7   as c format "x(60)"           no-undo.

form 
   r-shipprod        at 20    label "Product   " 
   skip 
   skip 
   r-arpvendno       at 10    label "ARP Vendor" 
   r-prodcst         at 35    label "Cost      " 
   r-shipfmno        at 10    label "Ship From " 
   r-reqshipdt       at 35    label "Due Date  "
   r-whse            at 10    label "ARP Whse  " 
   r-shipvia         at 35    label "Ship Via  " 
   r-arpprodline     at 10    label "Prod Line " 
   r-rush            at 35    label "Rush      " 
   "Comments  :"      at 10  
   r-comments1       at 3    no-label 
   r-comments2       at 3    no-label 
   r-comments3       at 3    no-label  
   r-comments4       at 3    no-label  
   r-comments5       at 3    no-label  
   r-comments6       at 3    no-label  
   with frame f-rarrnotes overlay side-labels width 65 column 8 row 7 
        title " Custom Order Comments for Purchasing ". 

define buffer z-oeelk for oeelk.
*/

find z-oeelk where recid(z-oeelk) = v-recid[frame-line(f-oeetk)]
                                    no-lock no-error.
if avail z-oeelk then do:
  if g-callproc begins "oei" then
    find rarr-com use-index k-com where 
         rarr-com.cono    = 1    and 
         rarr-com.comtype = "kt" + string(z-oeelk.seqno,"999") and
         rarr-com.orderno  = z-oeelk.orderno  and
         rarr-com.ordersuf = z-oeelk.ordersuf and
         rarr-com.lineno   = z-oeelk.lineno
         /*
         rarr-com.lineno   = int(frame-line)
         */
         no-lock no-error.
  else
    find rarr-com use-index k-com where 
         rarr-com.cono    = 1    and 
         rarr-com.comtype = "ktq" + string(z-oeelk.seqno,"999") and
         rarr-com.orderno  = z-oeelk.orderno  and
         rarr-com.ordersuf = z-oeelk.ordersuf and
         rarr-com.lineno   = z-oeelk.lineno
         /*
         rarr-com.lineno   = int(frame-line)
         */
         no-lock no-error.
  if avail rarr-com then do:
         assign k-arpvendno   = substring(rarr-com.noteln[9],17,12).
         assign k-prodcst     = substring(rarr-com.noteln[10],17,12).
         assign k-shipfmno    = substring(rarr-com.noteln[11],17,4).
         assign k-reqshipdt   = substring(rarr-com.noteln[12],17,8).
         
         assign r-shipprod    = z-oeelk.shipprod.
         assign r-arpvendno   = decimal(k-arpvendno).
         assign r-prodcst     = decimal(k-prodcst).
         assign r-shipfmno    = integer(k-shipfmno).
         assign r-reqshipdt   = date(k-reqshipdt).
         assign r-arpprodline = substring(rarr-com.noteln[15],17,6).
         assign r-shipvia     = substring(rarr-com.noteln[14],17,4).
         assign r-whse        = substring(rarr-com.noteln[13],17,4).
         assign r-rush        = if substring(rarr-com.noteln[16],17,3) = "yes"
                                   then yes else no.
         assign r-comments1 = rarr-com.noteln[1]
                r-comments2 = rarr-com.noteln[2]
                r-comments3 = rarr-com.noteln[3]
                r-comments4 = rarr-com.noteln[4]
                r-comments5 = rarr-com.noteln[5]
                r-comments6 = rarr-com.noteln[6].
/*                r-comments7 = rarr-com.noteln[7]. */
         
         view frame f-rarrnotes.
         display
         r-shipprod 
         r-arpvendno
         r-prodcst
         r-shipfmno
         r-reqshipdt
         r-shipvia
         r-whse
         r-arpprodline
         r-rush
         r-comments1
         r-comments2
         r-comments3
         r-comments4
         r-comments5
         r-comments6
      with frame f-rarrnotes.
      pause.
      hide frame f-rarrnotes no-pause.
      /*view frame f-blank.
      display x-blank with frame f-blank.
      hide frame f-blank no-pause.*/
  end.
end.      
