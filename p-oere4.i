/* p-oere4.i 1.1 01/03/98 */
/* p-oere4.i 1.4 08/25/97 */
/*h*****************************************************************************
  INCLUDE      : p-oere4.i
  DESCRIPTION  : Display the totals by order type for OERE
  USED ONCE?   : yes (oered.p)
  AUTHOR       : mwb
  DATE WRITTEN : 09/13/90
  CHANGES MADE :
    01/06/92 pap; TB# Split from p-oerr4.i due to differing frames
    04/13/95 kjb; TB# 18251 BL and BR included in grand totals
    08/30/95 dww; TB# 17521 Performance in OERE.  Added &sasc parameter to
        replace direct references to SASC
    08/25/97 kjb; TB# 22385 OERR totals do not include rebates
*******************************************************************************/
/*o If changes are made in this routine, please make sure to review p-oerr4.i
    and p-oerr5.i to determine if the changes are required there also. */

page.
display with frame f-oeref.
v-count = 0.

do i = 1 to 10:

    /*tb 22385 08/25/97 kjb; Set the rebate indicator if the margin for the 
        current order type was calculated with a rebate.  Also modifed v-gross
        to be the margin calculated with rebates instead of v-sales - v-cstgds.
        Now calculate v-marg with v-gross instead of v-sales - v-cstgds.  If
        calculating by net, then subtract out customer rebates from the total
        sale amount.  If calculating by cost, subtract out vendor rebates from
        the total cost.*/
    assign
        v-sales    = t-sales[i]
        v-cstgds   = t-cstgds[i]
        v-gross    = (t-sales[i] - t-custreb[i]) - (t-cstgds[i] - t-vendreb[i])
        v-charge   = t-charge[i]
        v-down     = t-down[i]
        v-totord   = t-totord[i]
        v-count    = t-count[i]
        v-marg     = if {&sasc}oecostsale then
                         if (v-sales - t-custreb[i]) <> 0 then
                             (v-gross / (v-sales - t-custreb[i])) * 100
                         else 0
                     else if (v-cstgds - t-vendreb[i]) <> 0 then
                         (v-gross / (v-cstgds - t-vendreb[i])) * 100
                     else 100
        v-ordname  = t-ordtype[i]
        v-rebatefl = t-rebfl[i].

    /*tb 18251 04/13/95 kjb; Do not increment the totals with BL totals */
    /*tb 22385 08/25/97 kjb; Added t-torebatefl which will be set if any of
        the margins were calculated with a rebate.  Also added t-tocustreb
        and t-tovendreb to accumulate the total rebate values. */
    if t-ordtype[i] ne "Blanket Order" then
        assign
            t-tosales    = t-tosales   + t-sales[i]
            t-tocstgds   = t-tocstgds  + t-cstgds[i]
            t-togross    = t-togross   + v-gross
            t-tocharge   = t-tocharge  + t-charge[i]
            t-todown     = t-todown    + t-down[i]
            t-tocount    = t-tocount   + t-count[i]
            t-tototord   = t-tototord  + t-totord[i]
            t-tocustreb  = t-tocustreb + t-custreb[i]
            t-tovendreb  = t-tovendreb + t-vendreb[i]
            t-torebatefl = if v-rebatefl = "r" then "r"
                           else t-torebatefl.

    /*tb 22385 08/25/97 kjb; Added v-rebatefl to the display */
    display
        v-ordname
        v-count
        v-sales
        v-cstgds when g-seecostfl = yes
        v-gross
        v-marg
        v-rebatefl
        v-charge
        v-down
        v-totord
    with frame f-oeref1.
    down with frame f-oeref1.

    /*tb 22385 08/25/97 kjb; Reset the rebate indicator for the next order 
        type. */
    assign
        v-ordname  = ""
        v-sales    = 0
        v-cstgds   = 0
        v-gross    = 0
        v-marg     = 0
        v-charge   = 0
        v-down     = 0
        v-count    = 0
        v-totord   = 0
        v-rebatefl = "".

end. /* do i = 1 to 10 */

/*tb 22385 08/25/97 kjb; Calculate the total margin % using t-togross instead
    of t-tosales - t-tocstgds so that rebates are included.  If calculating by
    net, then subtract out customer rebates from the total sale amount.  If
    calculating by cost, subtract out vendor rebates from the total cost.*/
assign
    t-tomarg = if {&sasc}oecostsale then
                   if (t-tosales - t-tocustreb) <> 0 then
                       (t-togross / (t-tosales - t-tocustreb)) * 100 
                   else 0
               else if (t-tocstgds - t-tovendreb) <> 0 then
                       (t-togross / (t-tocstgds - t-tovendreb)) * 100 
               else 100.

/*tb 22385 08/25/97 kjb; Added t-torebatefl to the display */
display
    t-tosales
    t-tocount
    t-tocstgds when g-seecostfl = yes
    t-togross
    t-tomarg
    t-torebatefl
    t-tocharge
    t-todown
    t-tototord
    t-lostcnt
    t-lostnet
    t-lostord
    
    s-csrfield
    s-drilldown
    s-ddvalue
with frame f-oeref2.

/*tb 22385 08/25/97 kjb; Display the legend frame so that the user will know
    what the little 'r' in the totals section means */
display with frame f-legend.


