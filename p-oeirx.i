/* p-oeirx.i 1.2 09/09/98 */
/* p-oeirx.i 1.26 04/18/94 */
/*h*****************************************************************************
  INCLUDE      : p-oeirx.i
  DESCRIPTION  : Selection logic for credit release
  USED ONCE?   : yes
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    10/08/91 rhl; TB# 4320 Update transdt/tm
    12/02/91 mwb; TB# 4878 Always set the global orderno off frame sitting on
    06/06/92 mwb; TB# 6889 Replaced oeeh.payamt with oeeh.tendamt
    07/22/92 jlc; TB# 7191 Alpha customer number change
    10/06/92 mjm; TB# 8144 Added a user call
    01/29/93 rhl; TB# 5055 Approval display not updated
    01/29/93 rhl; TB# 6942 AR Order Balance not updated if Cod changed
    02/19/93 rhl; TB# 7286 Sales order status "D" overridden by OE
    03/03/93 sdc; TB# 10245 Dnet Control Path
    03/18/93 rhl; TB# 10361 Allows order COD terms change after ship
    04/15/93 rhl; TB# 10900 Unable to auto-print zero-ship orders
    05/06/93 rhl; TB# 11248 Add communication between OEET and OEIR
    06/15/93 mwb; TB# 11712 Added security edit before approve order
    11/16/93 mwb; TB# 13670 OEAO days forward not looking at ICSD sat, sun or
        weekends for auto print calculation.
    12/02/93 mwb; TB# 13455 If approve order, was losing pointer on next page -
        was going to first record.
    01/11/94 mwb; TB# 14107 Cross locking on b-oeeh - added oeeh do for
    01/21/94 mwb; TB# 14402 Need to reset security after running external
        functions that have different security
    02/18/94 dww; TB# 14709 Function keys disappear & look wrong
    02/20/94 dww; TB# 14111 No OEEH record is available (91)
    03/07/94 dww; TB# 15057 Can't use F20 Notes in OEIR lines
    03/07/94 dww; TB# 14402 Cannot access approval field - security problem
    04/18/94 dww; TB# 15372 F20 Notes locks user
    06/28/96 bmf; TB# 21423 Add user include (&user_beg)
    07/12/96 vjp; TB# 21525 Add user include (&user_updt)
    12/19/96 bao; TB# 21254 (21c) Change vaeh.approvty when oeeh tied to va ord
    07/01/97 mtt; TB# 23133 Develop Credit Control Center Feature
    09/01/98 mtt; TB# 24049 PV: Common code use for GUI interface
    02/19/01 ajw; TB# e7495 Credit Cards in Standard
    04/01/03 mms; TB# t14244 8Digit Ord No Project - Relayed to accommodate the
        new size.
    09/16/03 doc; TB# e18306 F6 processing not pulling ship to credit info
*******************************************************************************/

assign  g-orderno  = integer(substring(frame-value,1,8))
        g-ordersuf = integer(substring(frame-value,10,2))
        o-secure   = g-secure.

/*u User Hook */
{p-oeirx.z99 &user_beg = "*"}

if {k-func20.i} then do:
    /* Apply a keystroke of "HELP (F20)" to force applhelp.p to take over */
    apply 320.

    /* Clear input buffer */
    input clear.
    readkey pause 0.
end.

{k-lkfunc.i}
if i > 0 then do:
    if i = 5 then do:
        put screen row 21 col 64 color message "F10-PRINT".

        run oeetg.p("D","n").

        put screen row 21 col 3 color message g-bottom.
    end.

    else do for oeeh:
        {w-oeeh.i g-orderno g-ordersuf no-lock}
        if not avail oeeh then next.

        assign
            g-custno = oeeh.custno
            g-shipto = oeeh.shipto.

        run jumpp.p(i).
        put screen row 21 col 3 color message g-bottom.
    end.

    g-secure = o-secure.

    next.
end.

g-secure = o-secure.

if ({k-right.i} or {k-return.i}) and g-secure > 2 then do:

    do for oeeh, b-oeeh transaction:

        {w-oeeh.i g-orderno g-ordersuf no-lock}

        if not avail oeeh then next.

        {e-dnctrl.i &file = "oeeh"
                    &next = "next"}
        assign
            v-idoeeh   = recid(oeeh)
            o-approvty = oeeh.approvty
            g-custno   = oeeh.custno.

        /*u User Hook */
        {p-oeirx.z99 &user_updt = "*"}

        if v-holdoverfl = yes then do:
            l-updt:
            do while true on endkey undo, leave:
                prompt-for
                    oeeh.approvty when oeeh.approvty <> "f"
              help "(Y)es, (N)o, (C)hange Terms & Approve, or Any Char for Hold"
                with frame f-oeir.
                if o-approvty ne input oeeh.approvty then do:

                    if input oeeh.approvty = "c" then run oeira.p.
                    if {k-cancel.i} then do:
                        display o-approvty @ oeeh.approvty.
                        leave l-updt.
                    end.

                    /*If the User has changed the Approval code to Approved,
                      then perform the Credit Card logic*/
                    if input oeeh.approvty = "y":u then do:

                        {ccoeir.lpr &MainFrame  = "f-oeir"
                                    &CCFrame    = "f-oeir-ccwarn"
                                    &OldValue   = "o-approvty"
                                    &NewValue   = "oeeh.approvty"
                                    &NextBlock  = "l-updt"}
                    end.

                    /*tb 24049 09/01/98 mtt; PV: Common code use for GUI
                      interface */
                    {oeir.lup &orderno       = "g-orderno"
                              &ordersuf      = "g-ordersuf"
                              &approvty      = "input oeeh.approvty"
                              &oeehnotfnd    = "next."}

                    if avail b-oeeh and b-oeeh.approvty = "y" and
                    (v-length = frame-line or frame-line = 1) then do:
                        if v-length = frame-line then
                            v-recid[v-length] = v-recid[v-length - 1].
                        if frame-line = 1 then v-recid[1] = v-recid[2].
                    end.

                end.

                leave l-updt.
            end.

        end. /* end hold over     */

    end.     /* end do for b-oeeh */

    if frame-line ne frame-down then down with frame f-oeir.
    next dsply.
end.
