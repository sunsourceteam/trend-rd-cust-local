/*h****************************************************************************
  INCLUDE      : poetg.lpr
  DESCRIPTION  : Purchase Order Print Screen - Processing Include
  USED ONCE?   : yes
  AUTHOR       : mtt
  DATE WRITTEN : 05/01/98
  CHANGES MADE :
    05/01/98 mtt; TB# 24049 PV: Common code use for GUI interface
    03/04/99 jba; PVTB#1730 skip delete SAPB if no SASP and (sapb.backfl=yes),
                            to allow print to file/email in GUI,
    05/05/99 jl;  TB# 17865 Expanded tag updated for faxing
    01/10/00 sbr; TB# 26772 F10 Fax - PO Print, Fax # not used
    01/11/00 sbr; TB# 26768 F10 - Fax # default not using APSE
    01/19/00 mjg; TB# e2785 Demand print not working in GUI.
    07/11/00 mjg; TB# e5637 Some print options in GUI not working.
    11/06/01 lsl; TB# e11093 Moved the ck of sapb.filefl to allow F10
                  demand email to pass through the email where appropriate
                  code if doc format = 3.
     02/04/02 rgm; TB# e11699 Correction to Email Demand RxServer print.
     06/24/02 rgm; TB# e13550 modify to work with F10 prints and custom code
******************************************************************************/

def            var v-pdoctypefl      as l                            no-undo.
def            var v-prxserverfl     as l                            no-undo.

/*o************* Print the PO **********************************/
/*tb 14171 05/17/95 mtt; Lock table overflow problem in F10 */
if {&poprintfl} = yes
then
do: /* for poeh, b-sapb, sapb, sasp, sassr, sasc, apsv,
       apss, sapbo transaction: */

    {w-poeh.i {&pono} {&posuf} no-lock}

    /*d Decide if a Document can be altered by checking Location
       against the StageCd */
    {&comdnet}
    {e-dnctrl.i &file = "poeh"
                &next = "pause. return"}
    /{&comdnet}* */

    /*d Find the stored report for the PO print */
    /*tb 21702-6 09/12/96 kr; Remove sapb.oper2 from Trend */
    find b-sapb use-index k-sapb where
         b-sapb.cono     = g-cono              and
         b-sapb.reportnm = ("@po" + poeh.whse + "alsdfajfjkl")
    no-lock no-error.

    /*tb#e2785 01/27/00 mjg; Demand print not working correctly in GUI */
    /*d Assign random report name */
    {p-rptnm.i v-reportnm {&report-prefix}}

    /*o Create the PO print */
    create sapb.
    /*tb 21702-6 09/12/96 kr; Remove sapb.oper2 from Trend */
    assign
        sapb.currproc   = "poepp"
        sapb.cono       = g-cono
        sapb.reportnm   = v-reportnm
        sapb.printoptfl = no
        sapb.priority   = 9
        sapb.printernm  = {&printernmpo}.


    if not avail b-sapb then
        assign
            sapb.optvalue[2] = string(TODAY,"99/99/99")
            sapb.optvalue[4] = "no"
            sapb.optvalue[3] = "0"
            sapb.optvalue[5] = if zx-Changes then "yes" else "no"
            sapb.optvalue[6] = if zx-prodPrint then "yes" else "no"
            sapb.optvalue[7] = "p"
            sapb.optvalue[8] = if zx-E-Fax then "yes" else "no"
            sapb.optvalue[9] = if zx-EDI then "yes" else "no".
    else do i = 1 to 20:

        sapb.optvalue[i] = b-sapb.optvalue[i].

    end. /* else do i = 1 to 20 */



    assign
        sapb.optvalue[4] = "no"
        sapb.optvalue[1] = "yes".

    /*tb e2785 01/19/00  mjg; Demand print not working in GUI */
    {&posapbassigns}

    /* tb e11093 11/09/01 lsl; begin - added a check for format 3 docs
    (RxServer) and the email where appropriate doc type.  This sets the
    sapb.filefl = no, so that email where appropriate output is used
    instead of the email-report code in rptctl.p */
    /* TB e11699 02/04/01 rgm; We should not require "email where
    appropriate" flag when doing a demand print */
    if {&printernmpo} = 'E-MAIL':u then
    do:
        if not avail poeh then do:
            run err{&errorno}.p(4606).
            {&comui}
            {pause.gsc}
            /{&comui}* */
            {&errorexit}
            return.
        end.
        {w-apsv.i poeh.vendno no-lock}

        v-pdoctypefl = yes.

        /* set the format type */
        run rxformck.p (input "po":u,
                        output v-prxserverfl).
        assign
            sapb.filefl      = if v-pdoctypefl and v-prxserverfl then no
                               else yes
            sapb.optvalue[8] = if v-pdoctypefl and v-prxserverfl then "yes"
                               else sapb.optvalue[8].
    end.

    {t-all.i sapb}

   /*tb e5637 07/11/00 mjg; Print options not working in GUI */
    if "{&printernmpo}" <> "E-Mail" then do:

        {w-sasp.i {&printernmpo} no-lock}

       /*tb 22068 04/15/98 cm; Delete temporary SAPB record before return.  Added
         error message and pause. */
      /*tb  1730 03/04/99 jba; no SASP necessary if GUI (background) */
      if ( not avail sasp )
      and sapb.backfl<>yes         /* TB 1730 */
      then do:

          {delete.gde sapb}
          run err{&errorno}.p(4035).
          {&comui}
          {pause.gsc}
          /{&comui}* */
          {&errorexit}

      end. /* if not avail sasp */
    end. /* if valid printer */

    {w-sassr.i ""poepp"" no-lock}

    /*tb 22068 04/15/98 cm; Delete temporary SAPB record before return.  Added
        error message and pause. */
    if not avail sassr then do:

        {delete.gde sapb}
        run err{&errorno}.p(4004).
        {&comui}
        {pause.gsc}
        /{&comui}* */
        {&errorexit}

    end. /* if not avail sassr */

    /*tb e2785 01/19/00  mjg; Demand print not working in GUI */
    /*tb 19803 12/18/95 sbl(CGA); Force background if set in SASSR */
    /*tb 19803 01/15/96 vlp(CGA); Create sched record if background */
    if sassr.backfl = yes or sapb.backfl = yes then do:

        assign
            {&poprintfl}  = no
            v-sapbid      = ?
            sapb.startdt  = today
            sapb.starttm  = time
            sapb.delfl    = yes
            sapb.backfl   = yes.

        /*d Creates RSES records from SAPB records */
        {&comrssched}
        {rssched.gpr}
        /{&comrssched}* */
        {&reportsched}

    end. /* else if sapb.backfl = yes */

    /*d  Add long distance code  */
    if avail sasp and sasp.ptype = "f" then do:

        if v-faxphoneno = "" then do:

            {w-sasc.i no-lock}
            {w-apsv.i poeh.vendno no-lock}

            if not avail apsv then next.

            if poeh.shipfmno <> 0 then
                {w-apss.i poeh.vendno poeh.shipfmno no-lock}

                /*tb 26768 01/11/00 sbr; Added "and apss.epoto = false" to the
                    if condition. */
                if poeh.shipfmno <> 0 and avail apss and apss.epoto = false
                then do:

                    /*d Fax defaults */
                    {p-fxapdf.i &file   = "apss."
                                &extent = 1}

                end. /* if poeh.shipfmno <> 0 */

                else do:

                    /*d Fax defaults */
                    {p-fxapdf.i &file   = "apsv."
                                &extent = 1}

                end. /* else do */

            /*tb 24781 04/06/98 bm; Force Fax# screen if GO pressed and no fax#
                       Moved here from above, following w-apss.i */
            if v-faxphoneno = "" then do:

                /*tb 16974 11/08/94 rs; Need > 2 comment lines */
                /*tb 17288 06/16/95 dww; Unix Error in On-Line Doc. */
                /*d Fax parameter input */
                {&comfax}
                {p-fxparm.i &proc = "poetg"
                            &pre  = "v-"}

                if {k-endkey.i} then do:

                    delete sapb.
                    g-errfl = no.
                    /*d Fax Canceled, No Document Printed */
                    run warning.p(6339).
                    return.

                end. /* if k-endkey.i */
                /{&comfax}* */

                {&pofaxprocessing}

            end. /* Second, if v-faxphoneno = "" */

        end. /* if v-faxphoneno = "" */

        /*tb 17865 05/05/99 jl; Added &faxtag1 and &faxtag2 */
        /*d Assign fax into to sapb */
        {p-fxasgn.i &option  = 8
                    &faxtag1 = "sapb.faxto1"
                    &faxtag2 = "'PO: ' + string({&pono}) + '-' +
                                string({&posuf},'99')"}

    end. /* if avail sasp */

    {w-sapb.i g-cono v-reportnm no-lock}

    if not avail sapb then do:
        run err{&errorno}.p(4036).
        {&comui}
        {pause.gsc}
        /{&comui}* */
        {&errorexit}
    end. /* not avail sapb */

    /*o Create the SAPBO record to list the PO to print */
    create sapbo.
    /*tb 21702-6 09/12/96 kr; Remove sapbo.oper2 from Trend */
    /*tb 26772 01/10/00 sbr; Removed " and sapb.faxphoneno = "" " from the
        if clause within the assign statement for sapbo.outputty. */
    assign
        sapbo.orderno   = {&pono}
        sapbo.ordersuf  = {&posuf}
        sapbo.reportnm  = v-reportnm
        sapbo.cono      = g-cono
        sapbo.reprintfl = if (poeh.printfl = yes and poeh.stagecd > 1)
                          then yes else no
        sapbo.route     = ""
        sapbo.outputty  = if avail sasp   /* TB#1730 */
                          and sasp.ptype = "f"
                          then "f" else "p".
    {t-all.i sapbo}

    assign
        v-sapbid  = recid(sapb)
        v-sassrid = recid(sassr).

end. /* if {&poprintfl} */

/*tb 14171 05/17/95 mtt; Lock table overflow problem in F10 */
/*o Run POEPP.P */
{&compoepp}
if {&poprintfl}
then
    /*d Purchase Order Processing */
  do:
   assign zx-ourproc = g-ourproc.
   assign g-ourproc  = "poepp".
   run poepp.p.
   assign g-ourproc  = zx-ourproc.
  end.


/{&compoepp}* */

{&comsapbdelete}
if {&poprintfl} then do: /* for sapb, sapbo transaction: */

    {w-sapb.i g-cono v-reportnm exclusive-lock}

    if avail sapb then delete sapb.

    {w-sapbo.i g-cono v-reportnm {&pono} {&posuf} exclusive-lock}

    if avail sapbo then delete sapbo.

end. /* if {&poprintfl} */
/{&comsapbdelete}* */

