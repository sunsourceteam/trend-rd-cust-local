/**************************************************************************/
/*                                  ICRZI.P                               */  
/* This program will generate a list of products set up in a whse for a   */
/* specific vendor.                                                       */
/* Marjorie Fialek 01/10/03                                               */  
/**************************************************************************/

{p-rptbeg.i}

def var s-whse           as c   format "x(4)"         no-undo.
def var w-whse           as c   format "x(4)"         no-undo.
def var w-product        as c   format "x(24)"        no-undo.
def var w-status         as c   format "x(1)"         no-undo.
def var w-vendno         as de  format ">>>>>9999999" no-undo.
def var w-prodline       as c   format "x(6)"         no-undo.
def var w-listprice      as de  format ">>>>9.99"     no-undo.
def var w-avgcost        as de  format ">>>>9.99"     no-undo.
def var w-onhand         as de  format ">>>>9.99"     no-undo.
def var w-netavail       as de  format ">>>>9.99"     no-undo. 
def var w-qtyonorder     as de  format ">>>>9.99"     no-undo.
def var w-qtyreservd     as de  format ">>>>9.99"     no-undo.
def var w-qtycommit      as de  format ">>>>9.99"     no-undo.
def var w-usagerate      as de  format ">>>>9.99"     no-undo.
def var w-descrip        as char format "x(24)"       no-undo.



def var b-vendor         as de  format "999999999999" no-undo.
def var e-vendor         as de  format "999999999999" no-undo.
def var b-prodline       as c   format "x(6)"          no-undo.
def var e-prodline       as c   format "x(6)"          no-undo.
def var b-whse           as c   format "x(4)"          no-undo.
def var e-whse           as c   format "x(4)"          no-undo.

def var o-onhand         as logical                    no-undo init no.
def var o-reorder        as logical                    no-undo init yes.
def var o-list           as logical                    no-undo init no.


Form
   "Whse "            at 1
   "Product"          at 7
   "Description"      at 35
   "Status"           at 60
   "Vendor"           at 70
   "Prod Line"        at 83
   "List Price"       at 93
   "Avg Cost"         at 104
   "On Hand"          at 114
   "Net Avail"        at 123
   "On Order"         at 133
   "Avg Usage"        at 143
with frame f-header width 178 no-labels no-box page-top no-underline.

Form 
   w-whse             at 1       
   w-product          at 7     
   w-descrip          at 35
   w-status           at 60    
   w-vendno           at 66      
   w-prodline         at 83     
   w-listprice        at 93      
   w-avgcost          at 104      
   w-onhand           at 113      
   w-netavail         at 123      
   w-qtyonorder       at 133     
   w-usagerate        at 143     
 with down frame f-detail width 178 no-labels no-box no-underline.


put control "~033~046~1541~117".    /* landscape format */


assign b-whse     = sapb.rangebeg[1].
assign e-whse     = sapb.rangeend[1].
assign b-vendor   = dec(sapb.rangebeg[2]).
assign e-vendor   = dec(sapb.rangeend[2]).
assign b-prodline = sapb.rangebeg[3].
assign e-prodline = sapb.rangeend[3].


if e-whse     = "" then assign e-whse   = "````".
if e-vendor   = 0 then assign e-vendor = 999999999999.
if e-prodline = "" then assign e-prodline = "``````".


assign o-onhand   = if sapb.optvalue[1] = "yes" then yes else no.
assign o-reorder  = if sapb.optvalue[2] = "yes" then yes else no.
assign o-list     = if sapb.optvalue[3] = "yes" then yes else no.

View frame f-header.

if o-list =  yes then 
do:

   for each sapbv where sapbv.cono     = g-cono and
                        sapbv.reportnm = sapb.reportnm 
                     /* sapbv.vendno   = 0 and
                        sapbv.type     = yes and
                        sapbv.seqno    = 0  */
                        exclusive-lock:

   for each icsd use-index k-icsd where icsd.cono     = g-cono and
                                        icsd.whse     = sapbv.apinvno and
                                        icsd.salesfl  = yes
                                        no-lock:

   for each icsw where icsw.cono      = g-cono and
                       icsw.whse      = icsd.whse and
                       icsw.arpvendno ge b-vendor and
                       icsw.arpvendno le e-vendor and
                       icsw.prodline  ge b-prodline and
                       icsw.prodline  le e-prodline
                       no-lock:
  
    find icsp where icsp.cono = g-cono and
                    icsp.prod = icsw.prod no-lock no-error.
    
    assign w-whse          = icsw.whse
           w-product       = icsw.prod
           w-descrip       = icsp.descrip[1]
           w-status        = icsw.statustype
           w-vendno        = icsw.arpvendno
           w-prodline      = icsw.prodline
           w-listprice     = icsw.listprice
           w-avgcost       = icsw.avgcost
           w-onhand        = icsw.qtyonhand
           w-netavail      = icsw.qtyonhand - (icsw.qtyreservd + icsw.qtycommit)
           w-qtyonorder    = icsw.qtyonorder
           w-usagerate     = icsw.usagerate.
            
        if o-onhand = yes and w-onhand = 0 then
           next.
        
        if o-reorder = no and w-status = "x" then
           next.
       
        run "print-list".   
   end.
  end.
 end. 
end.

else     /* if o-list = no then */
do:           
 
 for each icsd use-index k-icsd where icsd.cono      = g-cono and
                                        icsd.whse    ge b-whse and
                                        icsd.whse    le e-whse and
                                        icsd.salesfl =  yes
                                        no-lock:

 for each icsw use-index k-vendor where icsw.cono       = g-cono and
                                        icsw.whse       = icsd.whse and
                                        icsw.arpvendno  ge b-vendor and
                                        icsw.arpvendno  le e-vendor and
                                        icsw.prodline   ge b-prodline and
                                        icsw.prodline   le e-prodline 
                                        exclusive-lock:
                                        
   find icsp where icsp.cono = g-cono and
                   icsp.prod = icsw.prod no-lock no-error.
    
   assign w-whse          = icsw.whse
          w-product       = icsw.prod
          w-descrip       = icsp.descrip[1]
          w-status        = icsw.statustype
          w-vendno        = icsw.arpvendno
          w-prodline      = icsw.prodline
          w-listprice     = icsw.listprice
          w-avgcost       = icsw.avgcost
          w-onhand        = icsw.qtyonhand
          w-netavail      = icsw.qtyonhand - (icsw.qtyreservd + icsw.qtycommit)
          w-qtyonorder    = icsw.qtyonorder
          w-usagerate     = icsw.usagerate.
            
     if o-onhand = yes and w-onhand = 0 then
         next.
        
     if o-reorder = no and w-status = "x" then
        next.
     
     run "print-list".   
    
  end.
 end. 
end.



/*
if sapb.delfl = true then
         delete sapbv.                      

if ({k-cancel.i} or {k-jump.i}) and v-reportnm = ""
  then
  for each sapbv use-index k-sapbv where
                 sapbv.cono      = g-cono and
                 sapbv.reportnm  = sapb.reportnm
                 exclusive-lock:
                 
             delete sapbv.
  end.
end. 
*/ 


procedure print-list:

   display  w-whse
            w-product  
            w-descrip
            w-status
            w-vendno
            w-prodline
            w-listprice
            w-avgcost
            w-onhand
            w-netavail 
            w-qtyonorder
            w-usagerate
        with frame f-detail.    
       down with frame f-detail.

end procedure.

