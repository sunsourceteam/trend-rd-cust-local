/* insaex3.lpr 1.2 09/15/98 */
/* insaex3.lpr 1.1 04/21/97 */
/*h****************************************************************************
  INCLUDE      : insaex3.lpr
  DESCRIPTION  : Fields to extract during insaex3.p
  USED ONCE?   :
  AUTHOR       : drm
  DATE WRITTEN : 04/21/97
  CHANGES MADE :
    04/21/97 drm; TB# 23488 New include.
    07/17/97 jkp; TB# 23488 Added comments and added vendor and customer
        rebates.
    07/18/97 jkp; TB# 23528 Added string parameter to vendor rebate, customer
        rebate, number of lines, class, and error id fields.
    08/12/97 mms; TB# 23488 Added buffer logic for oeeh.
    08/16/97 jkp; TB# 23605 Added parameter &case to pass the case selected.
        Changed all numbered parameters to named parameters.
    10/29/97 jkp; TB# 23938 Do not allow question marks to be output.
    10/29/97 jkp; TB# 23996 Convert rebates to negative if the line is a
        return.
    10/29/97 jkp; TB# 24010 Change the order type to consider the line
        information instead of just using the oeeh.ordertype.
    12/16/97 jkp; TB# 24271 Changed backlog to oeel.stkqtyord for quantity.
    09/05/98 jkp; TB# 25288 Added sales data to be output, but include too
        large so created insaex3s.lpr just for sales.  If changes are made
        here, they may also affect insaex3s.lpr.
    08/19/99 jkp; TB# 24005 Round/Trim all fields larger than the data
        warehouse field back to the data warehouse size.  This is due to SQL
        7.0 tight typing.
    11/23/99 jkp; TB# 26929 Changed the output for v-ordertype by removing the
        override of RM type based on oeel.returnfl.  This is all handled in
        insaex3.p.
    06/13/01 lsl; fix SX.i 4.0 extracts to include older data streams
       removed inline if statements to reduce overall size of code
       to below 4095
    07/17/01 lsl; changed call for include insaex3.lpr to pass conditional
        preprocessors so that correct fields are output for sainv, sablog, and
        sabook.exp files.
    09/12/01 lsl; change chr(13) chr(10) eol characters to be just chr(10)
******************************************************************************/

/*e Record Status */
/* v-quote */
(v-null)
/* v-quote */
v-del
/* v-quote */
(invdte)
/* v-quote */
v-del
/* v-quote */
/*e Entity ID */
(v-entity)
/* v-quote */
v-del
/*e Sales */
string(v-net)
v-del
/*e Cost */
(cost)
v-del
/*e Discount */
string(v-disc)
v-del
(qty)
v-del
/*e Number of Billing Lines (counter) */
string(v-nolines)
v-del
/*e Vendor Rebate Amount */
(vrebamt)
v-del
/*e Customer Rebate Amount */
(crebamt)
v-del
/* v-quote */
/*e Order Number */
(ordnum)
/* v-quote */
v-del
/* v-quote */
/*e Outside Sales Rep */
(oslsrep)
/* v-quote */
v-del
/* v-quote */
(islsrep)
/* v-quote */
v-del
/* v-quote */
(tknby)
/* v-quote */
v-del
(custid)
v-del
/* v-quote */
(shipto)
/* v-quote */
v-del
/* v-quote */
(prodid)
/* v-quote */
v-del
(vendid)
v-del
/* v-quote */
(prodln)
/* v-quote */
v-del
/* v-quote */
/*e Buyer */
(v-buyer)
/* v-quote */
v-del
/* v-quote */
(pricetype)
/* v-quote */
v-del
/* v-quote */
/*e Product Family Group */
(v-famgrp)
/* v-quote */
v-del
/* v-quote */
/*e Product Class */
string(v-class)
/* v-quote */
v-del
/*  v-quote */
/*e Order Fill Type */
(v-ordertype)
/* v-quote */
v-del
/* v-quote */
(shipvia)
/* v-quote */
v-del
/* v-quote */
/*e Payment Type */
(v-paytype)
/* v-quote */
v-del
/* v-quote */
(termtype)
/* v-quote */
v-del
/* v-quote */
(v-null)
/* v-quote */
v-del
/* v-quote */
(ordstype)
/* v-quote */
v-del
/* v-quote */
/*e Kit Code */
(v-kittype)
/* v-quote */
v-del
/* v-quote */
(whseid)
/* v-quote */
v-del
/* v-quote */
(huser1)
/* v-quote */
v-del
/* v-quote */
(huser2)
/* v-quote */
v-del
/* v-quote */
(luser1)
/* v-quote */
v-del
/* v-quote */
(string(xz-lineuser2,">>>9"))
/* v-quote */
v-del
/*e Error ID */
string(v-zero)
v-del
/* v-quote */
(prodcat)
/* v-quote */
v-del
&if "{&v-type}" = "inv" &then
/* v-quote */
(orddte)
/* v-quote */
v-del
&endif
/* v-quote */
(reqdte)
/* v-quote */
v-del
/* v-quote */
(promdte)
/* v-quote */
v-del
&if "{&v-type}" = "blog" or "{&v-type}" = "inv" &then
/* v-quote */
(shpdte)
/* v-quote */
v-del
&endif
/* v-quote */
/*e SX User 1 */
caps(xz-lastlin)
/* v-quote */
v-del
/* v-quote */
/*e SX User 2 */
caps(xz-transtype)
/* v-quote */
v-del
/*e GLCost   */
string(xz-GLcost,"->>>>>>>>>9.99")
v-del
/* v-quote */
"        "
/* v-quote */
v-del
/* v-quote */
caps(xz-reasoncd)
/* v-quote */
v-del
string(xz-divno,">>9")
v-del
/* v-quote */
caps(string(xz-credty,"x"))
/* v-quote */
v-del
/* v-quote */ 

caps(string(xz-allcost,"x(2)"))

v-del   
caps(string(v-Initiative,"x(30)"))

 
 /* v-quote */
CHR(13) CHR(10).
