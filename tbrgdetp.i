/*****************************************************************************
   
 tbrgdetp.i  -  
                
*****************************************************************************/

  for each slsrec use-index k-cdetail 
     where slsrec.yr = YEAR(p-date)
       no-lock
      break by slsrec.region
            by slsrec.district
            by slsrec.salesrep
            by slsrec.customer:
     
      assign s-mo-isales     = s-mo-isales + slsrec.i-amount[w]
             s-mo-icost      = s-mo-icost  + slsrec.i-cost[w]
             s-mo-imarg      = s-mo-imarg  +
                       (slsrec.i-amount[w] - slsrec.i-cost[w])
             s-mo-glmarg     = s-mo-glmarg +
                       (slsrec.m-amount[w] - slsrec.m-cost[w]).

      assign s-b4-isales     = s-b4-isales + slsrec.i-amount[x] 
             s-b4-icost      = s-b4-icost  + slsrec.i-cost[x]
             s-b4-imarg      = s-b4-imarg  +
                  (slsrec.i-amount[x]  - slsrec.i-cost[x])
             s-b4-glmarg     = s-b4-glmarg +
                  (slsrec.m-amount[x]  - slsrec.m-cost[x]). 
/*                             
      if p-optmarg ne 0 then 
        do: 
           m-optamt = (s-mo-glmarg - s-mo-imarg).
           if m-optamt < 0 then 
              m-optamt * -1.
         if m-optamt < p-optmarg then 
            next.
        end.    
      if p-optpct ne 0 then 
        do: 
           s-mo-ipct   = (if s-mo-isales = 0 then 
                            (s-mo-imarg  / 1)  * 100
                          else
                            (s-mo-imarg  / s-mo-isales)  * 100).
           s-mo-glpct  = (if s-mo-isales = 0 then
                            (s-mo-glmarg  / 1) * 100
                          else  
                            (s-mo-glmarg  / s-mo-isales) * 100).
           m-optpct = (s-mo-glpct - s-mo-ipct).
           if m-optpct < 0 then 
              m-optpct = m-optpct * -1.
           if m-optpct <  p-optpct then 
              next.
        end.
*/
      
       do y = 1 to w:
        do:
         assign s-ytd-isales    = s-ytd-isales + slsrec.i-amount[y]  
                s-ytd-imarg = s-ytd-imarg +
                      (slsrec.i-amount[y] - slsrec.i-cost[y])
                s-ytd-icost    = s-ytd-icost  + slsrec.i-cost[y]
                s-ytd-glmarg   = s-ytd-glmarg +
                      (slsrec.m-amount[y] - slsrec.m-cost[y])
                s-ytd-glcost   = cs-ytd-glcost  + slsrec.m-cost[y].
         assign cs-ytd-isales  = cs-ytd-isales + slsrec.i-amount[y]  
                cs-ytd-imarg   = cs-ytd-imarg +
                      (slsrec.i-amount[y] - slsrec.i-cost[y])
                cs-ytd-icost    = cs-ytd-icost  + slsrec.i-cost[y]
                cs-ytd-glmarg   = cs-ytd-glmarg +
                      (slsrec.m-amount[y] - slsrec.m-cost[y])
                cs-ytd-glcost   = cs-ytd-glcost  + slsrec.m-cost[y].
        end.
       end.
    
    /* customer accums */ 
      assign cs-mo-isales     = cs-mo-isales + slsrec.i-amount[w]
             cs-mo-icost      = cs-mo-icost  + slsrec.i-cost[w]
             cs-mo-imarg      = cs-mo-imarg  +
                       (slsrec.i-amount[w] - slsrec.i-cost[w])
             cs-mo-glmarg     = cs-mo-glmarg +
                       (slsrec.m-amount[w] - slsrec.m-cost[w]).

      assign cs-b4-isales     = cs-b4-isales + slsrec.i-amount[x] 
             cs-b4-icost      = cs-b4-icost  + slsrec.i-cost[x]
             cs-b4-imarg      = cs-b4-imarg  +
                  (slsrec.i-amount[x]  - slsrec.i-cost[x])
             cs-b4-glmarg     = cs-b4-glmarg +
                  (slsrec.m-amount[x]  - slsrec.m-cost[x]).            
   if ((last-of(slsrec.customer))and p-sort = "c") then 
      do: 
       ord-ctr = 1.
       if ord-ctr = 1 then 
         do:
           assign substring(s-rgn,1,1) = slsrec.region
                  substring(s-rgn,2,3) = slsrec.district
                  s-custno    = string(slsrec.salesrep)
                  s-name      = slsrec.name.
           find arsc where
                arsc.cono = g-cono and
                arsc.custno = slsrec.customer
                no-lock no-error.
             if avail arsc then 
                assign cs-custno = arsc.custno
                       cs-name   = arsc.name.         
             else 
                assign cs-custno = arsc.custno 
                       cs-name   = "*No Customer Name Avlbl*".
           if hdr-sw = 0 then 
             do: 
               display s-rgn
                       s-custno
                       s-name
                       with frame f-ss-head.
               display with frame f-cd-header.
               hdr-sw = 1.
             end.
          display cs-custno 
                  cs-name 
                  with frame f-cc-head.
             down with frame f-cc-head.
         end.
      
       if ((p-drlln = "o" or p-drlln = "l") and 
       can-find(first custord use-index k-ordprt  where
                custord.region   = slsrec.region   and 
                custord.district = slsrec.district and 
                custord.salesrep = slsrec.salesrep and 
                custord.customer = slsrec.customer and
                custord.whse    <> " "             and 
               (custord.invdt >= p-date and custord.invdt <= z-date))) then
           run "prt-detail".
       else 
          ord-ctr = ord-ctr + 1.
      
       assign cs-mo-ipct    = (if cs-mo-isales = 0 then
                                 (cs-mo-imarg / 1) * 100  
                               else 
                                 (cs-mo-imarg / cs-mo-isales) * 100)  
              cs-mo-glpct   = (if cs-mo-isales = 0 then 
                                 (cs-mo-glmarg / 1)   * 100
                               else
                                 (cs-mo-glmarg / cs-mo-isales) * 100)
              cs-b4-ipct    = (if cs-b4-isales = 0 then 
                                 (cs-b4-imarg  / 1)   * 100
                               else   
                                 (cs-b4-imarg  / cs-b4-isales) * 100)
              cs-b4-glpct   = (if cs-b4-isales = 0 then 
                                 (cs-b4-glmarg / 1)   * 100
                               else   
                                 (cs-b4-glmarg / cs-b4-isales) * 100)
              cs-ytd-ipct   = (if cs-ytd-isales = 0 then 
                                 (cs-ytd-imarg  / 1) * 100
                               else 
                                 (cs-ytd-imarg  / cs-ytd-isales) * 100)
              cs-ytd-glpct  = (if cs-ytd-isales = 0 then 
                                 (cs-ytd-glmarg / 1) * 100
                               else  
                                 (cs-ytd-glmarg / cs-ytd-isales) * 100).
       if can-find(first custord use-index k-ordprt where
                custord.region   = slsrec.region   and 
                custord.district = slsrec.district and 
                custord.salesrep = slsrec.salesrep and 
                custord.customer = slsrec.customer and 
                custord.whse    <> " "             and 
               (custord.invdt >= p-date and custord.invdt <= z-date)) then
         do:
           if p-drlln = "o" or p-drlln = "l" then
              display with frame f-spaces.
         end.
       if cs-mo-ipct > 999 then 
          cs-mo-ipct = 999.
       if cs-mo-glpct > 999 then 
          cs-mo-glpct = 999.
       if cs-b4-ipct > 999 then 
          cs-b4-ipct = 999.
       if cs-b4-glpct > 999 then 
          cs-b4-glpct = 999.
       if cs-ytd-ipct > 999 then 
          cs-ytd-ipct = 999.
       if cs-ytd-glpct > 999 then 
          cs-ytd-glpct = 999.
       
       if cs-mo-ipct < (999 * -1) then 
          cs-mo-ipct = 999 * -1.
       if cs-mo-glpct < (999 * -1) then 
          cs-mo-glpct = 999 * -1.
       if cs-b4-ipct < (999 * -1) then 
          cs-b4-ipct = 999 * -1.
       if cs-b4-glpct < (999 * -1) then 
          cs-b4-glpct = 999 * -1.
       if cs-ytd-ipct < (999 * -1) then 
          cs-ytd-ipct = 999 * -1.
       if cs-ytd-glpct < (999 * -1) then 
          cs-ytd-glpct = 999 * -1.
       
       
       display  cs-mo-isales
                cs-mo-imarg
                cs-mo-ipct
                cs-mo-glmarg
                cs-mo-glpct
                cs-b4-isales
                cs-b4-imarg
                cs-b4-ipct
                cs-b4-glmarg
                cs-b4-glpct
                cs-ytd-isales
                cs-ytd-imarg
                cs-ytd-ipct
                cs-ytd-glmarg
                cs-ytd-glpct
                with frame f-dd-detail.
           down with frame f-dd-detail.
       if p-drlln = "o" or p-drlln = "l" then 
          display with frame f-spaces.

       assign cs-mo-isales  = 0
              cs-mo-icost   = 0
              cs-mo-imarg   = 0
              cs-mo-glmarg  = 0
              cs-b4-isales  = 0
              cs-b4-icost   = 0
              cs-b4-imarg   = 0
              cs-b4-glmarg  = 0
              cs-ytd-isales = 0
              cs-ytd-icost  = 0
              cs-ytd-glcost = 0
              cs-ytd-imarg  = 0
              cs-ytd-glmarg = 0.
     end.
      
   if (last-of(slsrec.salesrep)) then
     do:
      if s-mo-isales <> 0 then
       assign s-mo-ipct    = (if s-mo-isales = 0 then
                                (s-mo-imarg / 1) * 100  
                              else 
                                (s-mo-imarg / s-mo-isales) * 100)  
              s-mo-glpct   = (if s-mo-isales = 0 then 
                                (s-mo-glmarg / 1)   * 100
                               else
                                (s-mo-glmarg / s-mo-isales) * 100).
      if s-b4-isales <> 0 then
       assign s-b4-ipct    = (if s-b4-isales = 0 then 
                                (s-b4-imarg  / 1)   * 100
                               else   
                                (s-b4-imarg  / s-b4-isales) * 100)
              s-b4-glpct   = (if s-b4-isales = 0 then 
                                (s-b4-glmarg / 1)   * 100
                               else   
                                (s-b4-glmarg / s-b4-isales) * 100).
      if s-ytd-isales <> 0 then
       assign s-ytd-ipct   = (if s-ytd-isales = 0  then 
                                (s-ytd-imarg  / 1) * 100
                               else 
                                (s-ytd-imarg  / s-ytd-isales) * 100)
              s-ytd-glpct  = (if s-ytd-isales = 0 then 
                                (s-ytd-glmarg / 1) * 100
                               else  
                                (s-ytd-glmarg / s-ytd-isales) * 100).
         if s-mo-ipct > 999 then 
            s-mo-ipct = 999.
         if s-mo-glpct > 999 then 
            s-mo-glpct = 999.
         if s-b4-ipct > 999 then 
            s-b4-ipct = 999.
         if s-b4-glpct > 999 then 
            s-b4-glpct = 999.
         if s-ytd-ipct > 999 then 
            s-ytd-ipct = 999.
         if s-ytd-glpct > 999 then 
            s-ytd-glpct = 999.
         
         if s-mo-ipct < (999 * -1) then 
            s-mo-ipct = 999 * -1.
         if s-mo-glpct < (999 * -1) then 
            s-mo-glpct = 999 * -1.
         if s-b4-ipct < (999 * -1) then 
            s-b4-ipct = 999 * -1.
         if s-b4-glpct < (999 * -1) then 
            s-b4-glpct = 999 * -1.
         if s-ytd-ipct < (999 * -1) then 
            s-ytd-ipct = 999 * -1.
         if s-ytd-glpct < (999 * -1) then 
            s-ytd-glpct = 999 * -1.
         
         assign substring(s-rgn,1,1) = slsrec.region
              substring(s-rgn,2,3) = slsrec.district
              s-custno    = string(slsrec.salesrep)
              s-name      = slsrec.name
              hdr-sw      = 0.

         if p-sort = "s" or p-sort = "c" then
           do:
             display s-rgn
                     s-custno
                     s-name
                     s-mo-isales
                     s-mo-imarg
                     s-mo-ipct
                     s-mo-glmarg
                     s-mo-glpct
                     s-b4-isales
                     s-b4-imarg
                     s-b4-ipct
                     s-b4-glmarg
                     s-b4-glpct
                     s-ytd-isales
                     s-ytd-imarg
                     s-ytd-ipct
                     s-ytd-glmarg
                     s-ytd-glpct
                     with frame f-detail.
                     down with frame f-detail.
           end.
/* Accumlate District Totals */
         assign  d-mo-tisales     = d-mo-tisales    + s-mo-isales
                 d-mo-timarg      = d-mo-timarg     + s-mo-imarg
                 d-mo-tglmarg     = d-mo-tglmarg    + s-mo-glmarg
                 d-mo-ticost      = d-mo-ticost     + s-mo-icost
                 d-b4-tisales     = d-b4-tisales    + s-b4-isales
                 d-b4-timarg      = d-b4-timarg     + s-b4-imarg
                 d-b4-tglmarg     = d-b4-tglmarg    + s-b4-glmarg
                 d-b4-ticost      = d-b4-ticost     + s-b4-icost
                 d-ytd-tisales    = d-ytd-tisales   + s-ytd-isales
                 d-ytd-ticost     = d-ytd-ticost    + s-ytd-icost
                 d-ytd-tglmarg    = d-ytd-tglmarg   + s-ytd-glmarg
                 d-ytd-timarg     = d-ytd-timarg    + s-ytd-imarg.
         
         
/* Accumlate Regional Totals */                                    
         assign  r-mo-tisales     = r-mo-tisales    + s-mo-isales
                 r-mo-timarg      = r-mo-timarg     + s-mo-imarg
                 r-mo-tglmarg     = r-mo-tglmarg    + s-mo-glmarg
                 r-mo-ticost      = r-mo-ticost     + s-mo-icost
                 r-b4-tisales     = r-b4-tisales    + s-b4-isales
                 r-b4-timarg      = r-b4-timarg     + s-b4-imarg
                 r-b4-tglmarg     = r-b4-tglmarg    + s-b4-glmarg
                 r-b4-ticost      = r-b4-ticost     + s-b4-icost
                 r-ytd-tisales    = r-ytd-tisales   + s-ytd-isales
                 r-ytd-ticost     = r-ytd-ticost    + s-ytd-icost
                 r-ytd-tglmarg    = r-ytd-tglmarg   + s-ytd-glmarg
                 r-ytd-timarg     = r-ytd-timarg    + s-ytd-imarg.
            
/* Accumulate Grand Totals */                                    
  
         assign  g-mo-tisales     = g-mo-tisales    + s-mo-isales
                 g-mo-timarg      = g-mo-timarg     + s-mo-imarg
                 g-mo-tglmarg     = g-mo-tglmarg    + s-mo-glmarg
                 g-mo-ticost      = g-mo-ticost     + s-mo-icost
                 g-b4-tisales     = g-b4-tisales    + s-b4-isales
                 g-b4-timarg      = g-b4-timarg     + s-b4-imarg
                 g-b4-tglmarg     = g-b4-tglmarg    + s-b4-glmarg
                 g-b4-ticost      = g-b4-ticost     + s-b4-icost
                 g-ytd-tisales    = g-ytd-tisales   + s-ytd-isales
                 g-ytd-ticost     = g-ytd-ticost    + s-ytd-icost
                 g-ytd-tglmarg    = g-ytd-tglmarg   + s-ytd-glmarg
                 g-ytd-timarg     = g-ytd-timarg    + s-ytd-imarg.

/* zero out lowest break accums   */      
         assign s-mo-isales     = 0
                s-mo-imarg      = 0
                s-mo-icost      = 0
                s-mo-ipct       = 0
                s-mo-glmarg     = 0
                s-mo-glpct      = 0
                s-mo-glcost     = 0
                s-b4-isales     = 0
                s-b4-icost      = 0
                s-b4-imarg      = 0
                s-b4-ipct       = 0
                s-b4-glcost     = 0
                s-b4-glmarg     = 0
                s-b4-glpct      = 0
                s-ytd-isales    = 0
                s-ytd-icost     = 0
                s-ytd-imarg     = 0
                s-ytd-glmarg    = 0
                s-ytd-ipct      = 0
                s-ytd-glpct     = 0.

         assign cs-mo-isales  = 0
                cs-mo-icost   = 0
                cs-mo-imarg   = 0
                cs-mo-glmarg  = 0
                cs-b4-isales  = 0
                cs-b4-icost   = 0
                cs-b4-imarg   = 0
                cs-b4-glmarg  = 0
                cs-ytd-isales = 0
                cs-ytd-icost  = 0
                cs-ytd-glcost = 0
                cs-ytd-imarg  = 0
                cs-ytd-glmarg = 0.
     end.
      
   if (last-of(slsrec.district)) then
     do:
       assign d-mo-tipct    = (if d-mo-tisales = 0 then
                                 (d-mo-timarg / 1) * 100  
                               else 
                                 (d-mo-timarg / d-mo-tisales) * 100)  
              d-mo-tglpct   = (if d-mo-tisales = 0 then 
                                 (d-mo-tglmarg / 1)   * 100
                               else
                                 (d-mo-tglmarg / d-mo-tisales) * 100)
              d-b4-tipct    = (if d-b4-tisales = 0 then 
                                 (d-b4-timarg  / 1)   * 100
                               else   
                                 (d-b4-timarg  / d-b4-tisales) * 100)
              d-b4-tglpct   = (if d-b4-tisales = 0 then 
                                 (d-b4-tglmarg / 1)   * 100
                               else  
                                 (d-b4-tglmarg / d-b4-tisales) * 100)
              d-ytd-tipct   = (if d-ytd-tisales = 0  then 
                                 (d-ytd-timarg  / 1) * 100
                               else 
                                 (d-ytd-timarg  / d-ytd-tisales) * 100)
              d-ytd-tglpct  = (if d-ytd-tisales = 0 then 
                                 (d-ytd-tglmarg / 1) * 100
                               else  
                                 (d-ytd-tglmarg / d-ytd-tisales) * 100).
          if d-mo-tipct > 999 then 
             d-mo-tipct = 999.
          if d-mo-tglpct > 999 then 
             d-mo-tglpct = 999.
          if d-b4-tipct > 999 then 
             d-b4-tipct = 999.
          if d-b4-tglpct > 999 then 
             d-b4-tglpct = 999.
          if d-ytd-tipct > 999 then 
             d-ytd-tipct = 999.
          if d-ytd-tglpct > 999 then 
             d-ytd-tglpct = 999.

          if d-mo-tipct < (999 * -1) then 
             d-mo-tipct = 999 * -1.
          if d-mo-tglpct < (999 * -1) then 
             d-mo-tglpct = 999 * -1.
          if d-b4-tipct < (999 * -1) then 
             d-b4-tipct = 999 * -1.
          if d-b4-tglpct < (999 * -1) then 
             d-b4-tglpct = 999 * -1.
          if d-ytd-tipct < (999 * -1) then 
             d-ytd-tipct = 999 * -1.
          if d-ytd-tglpct < (999 * -1) then 
             d-ytd-tglpct = 999 * -1.
           
           if p-sort = "d" or p-sort = "s" or 
              p-sort = "c" then
            do:
              assign r-rgn = substring(s-rgn,2,3).
              display r-rgn
                      d-mo-tisales
                      d-mo-timarg
                      d-mo-tipct
                      d-mo-tglmarg
                      d-mo-tglpct
                      d-b4-tisales
                      d-b4-timarg
                      d-b4-tipct
                      d-b4-tglmarg
                      d-b4-tglpct
                      d-ytd-tisales
                      d-ytd-timarg
                      d-ytd-tipct
                      d-ytd-tglmarg
                      d-ytd-tglpct
                      with frame f-district.  
                      down with frame f-district.
           end.
         assign     d-mo-tisales  = 0
                    d-mo-tipct    = 0
                    d-mo-timarg   = 0
                    d-mo-ticost   = 0
                    d-mo-tglmarg  = 0
                    d-mo-tglpct   = 0
                    d-b4-tisales  = 0
                    d-b4-tipct    = 0
                    d-b4-timarg   = 0
                    d-b4-tglmarg  = 0
                    d-b4-tglpct   = 0
                    d-ytd-tisales = 0
                    d-ytd-tipct   = 0
                    d-ytd-tglpct  = 0
                    d-ytd-tglmarg = 0
                    d-ytd-timarg  = 0.
     end.
      
   if (last-of(slsrec.region)) then
     do:
       assign r-mo-tipct    = (if r-mo-tisales = 0 then
                                 (r-mo-timarg / 1) * 100  
                               else 
                                 (r-mo-timarg / r-mo-tisales) * 100)  
              r-mo-tglpct   = (if r-mo-tisales = 0 then 
                                 (r-mo-tglmarg / 1)   * 100
                               else
                                 (r-mo-tglmarg / r-mo-tisales) * 100)
              r-b4-tipct    = (if r-b4-tisales = 0 then 
                                 (r-b4-timarg  / 1)   * 100
                               else   
                                 (r-b4-timarg  / r-b4-tisales) * 100)
              r-b4-tglpct   = (if r-b4-tisales = 0 then 
                                 (r-b4-tglmarg / 1)   * 100
                               else  
                                 (r-b4-tglmarg / r-b4-tisales) * 100)
              r-ytd-tipct   = (if r-ytd-tisales = 0  then 
                                 (r-ytd-timarg  / 1) * 100
                               else 
                                 (r-ytd-timarg  / r-ytd-tisales) * 100)
              r-ytd-tglpct  = (if r-ytd-tisales = 0 then 
                                 (r-ytd-tglmarg / 1) * 100
                               else  
                                 (r-ytd-tglmarg / r-ytd-tisales) * 100).
          if r-mo-tipct > 999 then 
             r-mo-tipct = 999.
          if r-mo-tglpct > 999 then 
             r-mo-tglpct = 999.
          if r-b4-tipct > 999 then 
             r-b4-tipct = 999.
          if r-b4-tglpct > 999 then 
             r-b4-tglpct = 999.
          if r-ytd-tipct > 999 then 
             r-ytd-tipct = 999.
          if r-ytd-tglpct > 999 then 
             r-ytd-tglpct = 999.
          
          if r-mo-tipct < (999 * -1) then 
             r-mo-tipct = 999 * -1.
          if r-mo-tglpct < (999 * -1) then 
             r-mo-tglpct = 999 * -1.
          if r-b4-tipct < (999 * -1) then 
             r-b4-tipct = 999 * -1.
          if r-b4-tglpct < (999 * -1) then 
             r-b4-tglpct = 999 * -1.
          if r-ytd-tipct < (999 * -1) then 
             r-ytd-tipct = 999 * -1.
          if r-ytd-tglpct < (999 * -1) then 
             r-ytd-tglpct = 999 * -1.

          if slsrec.region = "f" then
             assign r-lit = "FABRICATION".
          if slsrec.region = "n" then
             assign r-lit = "NORTH".
          if slsrec.region = "m" then
             assign r-lit = "MOBILE OEM ".
          if slsrec.region = "w" then
             assign r-lit = "WEST ".
          if slsrec.region = "e" then
            assign r-lit = "EAST ".
          if slsrec.region = "r" then
             assign r-lit = "SERVICE ".
          if slsrec.region = "s" then
             assign r-lit = "SOUTH WEST".
          if slsrec.region = "P" then
             assign r-lit = "PABCO ".
    
             
          if p-sort = "d" or p-sort = "r" or 
             p-sort = "s" or p-sort = "c" then
           do:
             assign r-rgn = substring(s-rgn,1,1).
             display r-lit
                     r-rgn
                     r-mo-tisales
                     r-mo-timarg
                     r-mo-tipct
                     r-mo-tglmarg
                     r-mo-tglpct
                     r-b4-tisales
                     r-b4-timarg
                     r-b4-tipct
                     r-b4-tglmarg
                     r-b4-tglpct
                     r-ytd-tisales
                     r-ytd-timarg
                     r-ytd-tipct
                     r-ytd-tglmarg
                     r-ytd-tglpct
                     with frame f-region.
                down with frame f-region.
           end.
         assign     r-mo-tisales  = 0
                    r-mo-tipct    = 0
                    r-mo-timarg   = 0
                    r-mo-ticost   = 0
                    r-mo-tglmarg  = 0
                    r-mo-tglpct   = 0
                    r-b4-tisales  = 0
                    r-b4-tipct    = 0
                    r-b4-timarg   = 0
                    r-b4-tglmarg  = 0
                    r-b4-tglpct   = 0
                    r-ytd-tisales = 0
                    r-ytd-tipct   = 0
                    r-ytd-tglpct  = 0
                    r-ytd-tglmarg = 0
                    r-ytd-timarg  = 0.

     end.
   end. /* end of for each */
    
 /*  grand total print */

       assign g-mo-tipct    = (if g-mo-tisales = 0 then
                                 (g-mo-timarg / 1) * 100  
                               else 
                                 (g-mo-timarg / g-mo-tisales) * 100)  
              g-mo-tglpct   = (if g-mo-tisales = 0 then 
                                 (g-mo-tglmarg / 1)   * 100
                               else
                                 (g-mo-tglmarg / g-mo-tisales) * 100)
              g-b4-tipct    = (if g-b4-tisales = 0 then 
                                 (g-b4-timarg  / 1)   * 100
                               else   
                                 (g-b4-timarg  / g-b4-tisales) * 100)
              g-b4-tglpct   = (if g-b4-tisales = 0 then 
                                 (g-b4-tglmarg / 1)   * 100
                               else  
                                 (g-b4-tglmarg / g-b4-tisales) * 100)
              g-ytd-tipct   = (if g-ytd-tisales = 0  then 
                                 (g-ytd-timarg  / 1) * 100
                               else 
                                 (g-ytd-timarg  / g-ytd-tisales) * 100)
              g-ytd-tglpct  = (if g-ytd-tisales = 0 then 
                                 (g-ytd-tglmarg / 1) * 100
                               else  
                                 (g-ytd-tglmarg / g-ytd-tisales) * 100).

    
          if g-mo-tipct > 999 then 
             g-mo-tipct = 999.
          if g-mo-tglpct > 999 then 
             g-mo-tglpct = 999.
          if g-b4-tipct > 999 then 
             g-b4-tipct = 999.
          if g-b4-tglpct > 999 then 
             g-b4-tglpct = 999.
          if g-ytd-tipct > 999 then 
             g-ytd-tipct = 999.
          if g-ytd-tglpct > 999 then 
             g-ytd-tglpct = 999.
          
          display g-mo-tisales
                  g-mo-timarg
                  g-mo-tipct
                  g-mo-tglmarg
                  g-mo-tglpct
                  g-b4-tisales
                  g-b4-timarg
                  g-b4-tipct
                  g-b4-tglmarg
                  g-b4-tglpct
                  g-ytd-tisales
                  g-ytd-timarg
                  g-ytd-tipct
                  g-ytd-tglmarg
                  g-ytd-tglpct
              with frame f-grand.  
              down with frame f-grand.
 
procedure prt-detail:
do:
   if hdr-sw = 1 then 
     do:
        hdr-sw = 2.
        if p-drlln = "o" or p-drlln = "l" then 
         do: 
          display with frame f-od-header.
         end.
        if p-drlln = "l" then 
          display with frame f-ld-header.
     end.
    
   for each custord use-index k-ordprt where 
            custord.region   = slsrec.region   and 
            custord.district = slsrec.district and 
            custord.salesrep = slsrec.salesrep and 
            custord.customer = slsrec.customer and 
            custord.whse    <> " "             and 
           (custord.invdt >= p-date and custord.invdt <= z-date)
            no-lock: 

   if avail custord then 
     do:
        assign cs-custno = (if ord-ctr = 1 then 
                               custord.customer
                            else 
                               0) 
               cs-oname  = "----------->"
               cs-order  = (string(int(custord.ordnbr),"9999999") + "-" +
                            string(int(custord.ordsuf),"99"))     
               cs-total  = custord.totord
               cs-date   = custord.invdt.
        if ord-ctr = 1 then 
          do: 
             assign cs-ordhold = custord.ordnbr + custord.ordsuf.
             display  cs-order
                      cs-date
                      cs-total 
                      with frame f-oo-head.
                 down with frame f-oo-head. 
          end.
        if (cs-ordhold ne (custord.ordnbr + custord.ordsuf))
            and (p-drlln = "o" or p-drlln = "l") then
          do:  
            assign cs-ordhold = (custord.ordnbr + custord.ordsuf).
            display  cs-order
                     cs-date
                     cs-total 
                     with frame f-oo-head.
                down with frame f-oo-head. 
          end.
        assign print-count = print-count + 1.
        ord-ctr = ord-ctr + 1.
        if p-drlln  = "l" then 
          do:
           find oeeh use-index k-oeeh where
                oeeh.cono     = g-cono         and
                oeeh.orderno  = custord.ordnbr and 
                oeeh.ordersuf = custord.ordsuf  
                no-lock no-error.
           find oeel use-index k-oeel where
                oeel.cono     = g-cono and 
                oeel.orderno  = custord.ordnbr and 
                oeel.ordersuf = custord.ordsuf and 
                oeel.lineno   = custord.line   and 
                oeel.shipprod = custord.prod
                no-lock no-error.
           if avail oeel then 
             do:
                assign s-lineno   = oeel.lineno
                       s-prod     = oeel.shipprod
                       s-qtyship  = oeel.qtyship
                       s-stndcost = custord.cost
                       s-glcost   = custord.glcost
                       s-lnprice  = (if oeeh.transtype = "rm" then 
                                        oeel.netamt * -1 
                                     else 
                                        oeel.netamt)
                       s-oemarg   = custord.oemarg
                       s-gllnmarg = custord.glmarg
                       s-user5 = substring(oeel.user5,1,1)
                       s-orderaltno =  oeel.orderaltno.
                assign glhld-1pct = (if s-lnprice = 0 then 
                                       (s-oemarg / 1) * 100
                                     else
                                       (s-oemarg / s-lnprice) * 100)
                       glhld-2pct = (if s-lnprice = 0 then 
                                       (s-gllnmarg / 1) * 100
                                     else  
                                       (s-gllnmarg / s-lnprice) * 100)
                       glhld-var = ( if s-glcost = 0 then 
                                        100
                                     else   
                           ((s-stndcost - s-glcost) / s-glcost) * 100).
                if glhld-1pct < -999 or glhld-1pct > 999 then 
                   assign glhld-1pct = 999.   
                if glhld-2pct < -999 or glhld-2pct > 999 then 
                   assign glhld-2pct = 999.   
                if glhld-var < -999 or glhld-var > 999 then 
                   assign glhld-1pct = 999.   
                display s-lineno custord.blanksort s-prod s-qtyship
                        s-stndcost s-lnprice s-glcost
                        s-oemarg s-gllnmarg glhld-1pct glhld-2pct
                        glhld-var oeel.whse oeel.ordertype s-orderaltno
                        s-user5
                        with frame f-lndetail.
                        down with frame f-lndetail.
             end.        
          end. /* end do */
     end. 
   end. /* for each    */
 end. /* do */
end. /* procedure */
   
   

