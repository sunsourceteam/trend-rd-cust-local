/* f-ares9.i 1.9 10/07/97 */
/*h*****************************************************************************
  INCLUDE               : f-ares9.i
  DESCRIPTION           : Forms for SunSource AR statements, format 9
  USED ONLY ONCE?       :
  AUTHOR                :
  DATE WRITTEN          :
  CHANGES MADE AND DATE : 07/13/92 pap; TB# 7191 - Customer # change
                              Use report.c12 instead of report.de12d0
                              for customer #
                          06/24/93 jrg; TB# 11968 Changed variables in f-bottom
                              to change period balances to contain SVs
                              and Credits when the Special Transaction
                              flag is set.
                          11/17/93 bj; TB# 13551 Check summary last 30 days
                              incorrect
                          10/07/97 jph; Headings Change    
*******************************************************************************/

form header
    skip (4)             /* 10/07/97 Heading change to drop 1 line */
    h-callmsg           at 4
    h-custprt           at 4
    h-custno            at 66
    /*tb 7191 07/13/92 pap; Customer # change - use c12 instead of de12d0
    report.de12d0       at 68                                               */
    report.c12          at 68

    h-stmtdt            at 66
    p-stmtdt            at 69
    v-addr[1]           at 11
    h-totdue            at 66
    v-addr[2]           at 11
    s-totalbal          at 68
    v-addr[3]           at 11
    v-addr[4]           at 11
    skip (3)
    h-remitto           at 4 
    h-coname            at 11
    h-coaddr[1]         at 11
    h-amtpaid           at 66
    h-coaddr[2]         at 11
    h-amtpaidln         at 62
    h-cocitystzip       at 11
    h-remit1            at 55
    h-remit2            at 55
    v-msg               at 1
    skip (1)
    h-stmtdt2           at 1
    h-custno2           at 19
    p-stmtdt2           at 4
    /*tb 7191 07/13/92 pap; Customer # change - use c12 instead of de12d0
    v-de12d0            at 16                                               */
    v-c12               at 16
    skip (1)
    h-headingln         at 1
    h-uline             at 1
with frame f-top width 80 no-labels no-box overlay page-top no-underline.

form
    b-report.dt           at 1
    b-report.dt-2         at 10
    b-report.c2           at 20
    b-report.c6           at 24
    v-invoice             at 31
    v-charge              at 55 format "zzzzzzzz9.99-"
    v-credit              at 68 format "zzzzzzzz9.99-"
with frame f-detail width 80 no-labels no-box overlay.

form
    v-custpolit           at 31
    v-custpo              at 36
with frame f-custpo width 80 no-labels no-box overlay.


form
    "Last Statement -"    at 1
    v-lastdt           colon 45 label "Date"
    v-lastbal          colon 66 format "zzzzzzzz9.99-" label "Balance"
    skip (1)
with frame f-fwdbal width 80 side-labels no-box.

form
    report.c24          at 10
with frame f-refer width 80 no-labels no-box overlay.

form
    skip (1)
    v-totbal           colon 66 format "zzzzzzzz9.99-" label "Balance Due"
with frame f-tot width 80 side-labels no-box overlay.

form header
    skip (1)
/*tb 13551 11/17/93 bj; Check summary last 30 days incorrect */
    "Check Summary Since Last Statement:"      at 1
    "Chk Date"            at 1
    "Check #"             at 31
    "  Amount"            at 68
    "--------"            at 1
    "--------"            at 31
    "--------"            at 68
with frame f-chksumm width 80 no-labels no-box overlay page-top no-underline.

/* tb 11968 06/24/93 jrg; Changed variables in f-bottom to change period
                          balances to contain SVs and Credits when the Special
                          Transaction flag is set. */
form header
    v-continued         at 10
    skip (1)
    h-footerln1         at 1
    s-periodbal[1]   at 3
    s-periodbal[2]   at 18
    s-periodbal[3]   at 33
    s-periodbal[4]   at 48
    s-periodbal[5]   at 63
    skip (1)
    h-footerln2         at 1
 /* arsc.servchgbal     at 3       chg 10/07/97  remove*/
    report.de9d2s-3     at 18
 /* s-futbal            at 33       chg 10/07/97
    arsc.ordbal         at 48           10/07/97
    arsc.servchgytd     at 63           10/07/97   */
with frame f-bottom width 80 no-labels no-box overlay page-bottom no-underline.

form
    b-report.c13       colon 11 label "** Ship To"
    v-subtot           colon 66 format "zzzzzzzz9.99-"
                                label "Outstanding Charges"
with frame f-subbot width 80 side-labels no-box overlay.

form
    skip (1)
    b-report.c13       colon 11 label "** Ship To"
with frame f-subtop width 80 side-labels no-box overlay.

/********* original f-bottom frame *********
form header
    v-continued         at 10
    skip (1)
    h-footerln1         at 1
    arsc.periodbal[1]   at 3
    arsc.periodbal[2]   at 18
    arsc.periodbal[3]   at 33
    arsc.periodbal[4]   at 48
    arsc.periodbal[5]   at 63
    skip (1)
    h-footerln2         at 1
    arsc.servchgbal     at 3
    report.de9d2s-3     at 18       /* arsc.misccrbal      at 18 */
    arsc.futinvbal      at 33
    arsc.ordbal         at 48
    arsc.servchgytd     at 63
with frame f-bottom width 80 no-labels no-box overlay page-bottom no-underline.
********************************************/
