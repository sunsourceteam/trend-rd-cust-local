/* d-zpoque.i */
/* ------------------------------------------------------------------------
  Program Name :  D-ZPOQUE.I

  Description  :  Display module for POEZQ using xxixx.i

  Author       :  Thomas Herzog


------------------------------------------------------------------------- */









z-ponoline = substring(zidc.docdisp,1,1) +
             string(g-pono,">>>>>>9") + "-" + string(g-posuf,"99") + 
             string(poeh.notesfl,"x") +
             string(g-polineno,">>9") +
             (if poel.commentfl then "*" else " ").


if not avail poel then 
   assign z-shipprod = "".
else
  do:
  assign z-shipprod = poel.shipprod. 
  assign z-rush = if poeh.rushfl then "y" else "n".
  
  find icsw use-index k-whse where icsw.cono = g-cono and
                icsw.whse = poel.whse and
                icsw.prod = poel.shipprod no-lock no-error.
  assign z-avail = 0 
         z-coavail = 0.
  if avail icsw then
    z-avail = icsw.qtyonhand - icsw.qtyreservd - icsw.qtycommit.

  for each icsw use-index k-icsw 
                where icsw.cono = g-cono and
                      icsw.prod = poel.shipprod no-lock:
    z-coavail = z-coavail +
      (icsw.qtyonhand - icsw.qtyreservd - icsw.qtycommit).
  end.
end.                 
                


display 
  z-ponoline
  poel.nonstockty
  z-shipprod
  poel.vendno
  poel.qtyord
  z-coavail
  poel.price
  poel.transtype
  z-rush
  with frame f-poquel.

