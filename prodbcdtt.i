/* prodbcdtt.i 
   to put the temp-prod record in the outstream
*/

/* export stream bcd delimiter "~~" temp-prod chr(13) chr(10). */

put stream bcd unformatted
temp-prod.lblname v-tilda
temp-prod.descrip1 v-tilda 
temp-prod.descrip2 v-tilda 
temp-prod.shipprod v-tilda 
temp-prod.binloc1 v-tilda 
temp-prod.binloc2 v-tilda 
temp-prod.vendprod v-tilda 
temp-prod.section1 v-tilda 
temp-prod.section2 v-tilda 
temp-prod.section3 v-tilda 
temp-prod.section4 v-tilda 
temp-prod.section5 v-tilda 
temp-prod.section6 v-tilda 
temp-prod.pono v-tilda 
temp-prod.posuf v-tilda 
temp-prod.lineno v-tilda 
temp-prod.weight v-tilda 
temp-prod.cubes v-tilda 
temp-prod.vendno v-tilda 
temp-prod.vendnm v-tilda 
temp-prod.serialno v-tilda 
temp-prod.user1 v-tilda 
temp-prod.user2 v-tilda 
temp-prod.user3 v-tilda 
temp-prod.user4 v-tilda 
temp-prod.user5 v-tilda 
temp-prod.user6 v-tilda 
temp-prod.user7 v-tilda 
temp-prod.user8 v-tilda 
temp-prod.user9 v-tilda 
temp-prod.user10 v-tilda 
temp-prod.reprint v-tilda 
temp-prod.prepo1 v-tilda 
temp-prod.fullpono v-tilda 
temp-prod.tagno v-tilda
temp-prod.qtyrcv v-tilda 
/* put stream bcd unformatted */
   chr(13)
   chr(10).
