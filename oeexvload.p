
{g-all.i}
{g-inq.i}
{g-ic.i}

def     shared var g-batchnm   like edih.batchnm              no-undo.
def new shared var g-po        as c format "x(10)"            no-undo.
def new shared var g-seqno     like edih.seqno                no-undo.

def        var w-docno       as c format "x(11)"              no-undo.
def        var w-currproc    like sassm.currproc              no-undo.
def        var w-ourproc     like sassm.currproc              no-undo.
def        var s-verify      as logical                       no-undo.
def        var blank-line    as c format "x(78)"               no-undo.
def        var blank-line1   as c format "x(78)"               no-undo.
def        var blank-line3   as c format "x(78)"               no-undo.
def        var blank-line4   as c format "x(78)"               no-undo.
def        var blank-line5   as c format "x(78)"               no-undo.
def        var blank-line6   as c format "x(78)"               no-undo.
def        var blank-line7   as c format "x(78)"               no-undo.
def        var blank-line8   as c format "x(78)"               no-undo.
def        var blank-line9   as c format "x(78)"               no-undo.
def        var blank-line10  as c format "x(78)"               no-undo.
def        var blank-line11  as c format "x(78)"               no-undo.
def        var blank-line12  as c format "x(78)"               no-undo.
def        var blank-line13  as c format "x(78)"               no-undo.
def        var blank-line14  as c format "x(78)"               no-undo.
def        var blank-line15  as c format "x(78)"               no-undo.
def        var blank-line16  as c format "x(78)"               no-undo.
def        var blank-line17  as c format "x(78)"               no-undo.
def        var blank-line18  as c format "x(78)"               no-undo.
def        var blank-line19  as c format "x(78)"               no-undo.
def        var blank-line20  as c format "x(78)"               no-undo.
def        var blank-line21  as c format "x(78)"               no-undo.

def shared var q-line        as i      init 1                 no-undo.

define var p-cost as  logical init yes   no-undo.
define var v-cost as  de                 no-undo.
def    var s-prodcat  like icsp.prodcat  no-undo.
def    var s-update   as c format "x(1)" no-undo.
def    var completefl as logical         no-undo.
def    var upd-count  as i format "999"  no-undo.
define new shared buffer b-edil for edil.
define new shared buffer b2-edil for edil.
define new shared buffer b-edih for edih.
define new shared buffer b2-edih for edih.

def new shared var v-smwodiscfl  like sasc.smwodiscfl initial no no-undo.
def new shared var v-oecostsale  like sasc.oecostsale initial no no-undo.
def new shared var v-smcustrebfl like sasc.smcustrebfl initial no no-undo.
def new shared var v-smvendrebfl like sasc.smvendrebfl initial no no-undo.

{speccost.gva "new shared"}
{rebnet.gva "new shared"}

define shared temp-table t-VERM
  field batchnm          like edih.batchnm
  field seqno            like edih.seqno
  field bstatus          like edih.refer
  field custpo           as c format "x(10)"
  field baccept          as c format "x(1)"
  /***
  field custno           like oeel.custno
  field shipto           like oeel.shipto
  field lineno           like edil.lineno
  field shipprod         like edil.shipprod
  field promisedt        like oeel.promisedt
  field expshipdt        like oeel.reqshipdt
  field price            like oeel.price
  ***/
index batchnm
      seqno
      custpo.
      
def        buffer w-VERM for t-VERM.
def shared buffer s-VERM for t-VERM.

def query q-VERM for s-VERM scrolling.

def browse b-VERM query q-VERM
  display
    s-VERM.baccept      column-label "Accept"     
    s-VERM.custpo       column-label "VERMEER PO"
  with no-box size-chars 78 by 16 down.
  
def frame f-body
  b-VERM
  with row 4 no-underline overlay no-labels width 80 .

def frame f-update
   "Are you sure you want to tag this PO complete?" at 5
   s-verify         at 58 no-label
   with overlay side-labels width 68 column 8 row 10.
   
def frame blank-screen
   blank-line    at 1 no-label
   blank-line1   at 1 no-label
   blank-line3   at 1 no-label
   blank-line4   at 1 no-label
   blank-line5   at 1 no-label
   blank-line6   at 1 no-label
   blank-line7   at 1 no-label
   blank-line8   at 1 no-label
   blank-line9   at 1 no-label
   blank-line10  at 1 no-label
   blank-line11  at 1 no-label
   blank-line12  at 1 no-label
   blank-line13  at 1 no-label
   blank-line14  at 1 no-label
   blank-line15  at 1 no-label
   with overlay side-labels width 80 column 1 row 4.

  def frame blank-screen2
   blank-line    at 1 no-label
   blank-line1   at 1 no-label
   blank-line3   at 1 no-label
   blank-line4   at 1 no-label
   blank-line5   at 1 no-label
   blank-line6   at 1 no-label
   blank-line7   at 1 no-label
   blank-line8   at 1 no-label
   blank-line9   at 1 no-label
   blank-line10  at 1 no-label
   blank-line11  at 1 no-label
   blank-line12  at 1 no-label
   blank-line13  at 1 no-label
   blank-line14  at 1 no-label
   blank-line15  at 1 no-label
   blank-line16  at 1 no-label
   blank-line17  at 1 no-label
   blank-line18  at 1 no-label
   blank-line19  at 1 no-label
   blank-line20  at 1 no-label
   blank-line21  at 1 no-label
   with overlay side-labels width 80 column 1 row 4.
 

on any-key of b-VERM do:

   if {k-cancel.i} or lastkey = 401 then 
     do:
     /* Close the querry and leave OEEXV */
     input clear.
     /*
     readkey pause 0.
     */
     hide frame f-body.
     close query q-VERM.
     apply "window-close" to frame f-body.
     leave.
   end.  
   
   
   if {k-func7.i} then 
     do:
     display blank-line
             blank-line1
             blank-line3 
             blank-line4 
             blank-line5 
             blank-line6 
             blank-line7 
             blank-line8 
             blank-line9 
             blank-line10
             blank-line11
             blank-line12
             blank-line13
             blank-line14
             blank-line15
     with frame blank-screen.
   
     assign q-line = integer(current-result-row("q-VERM")).
   
     assign g-po      = s-VERM.custpo
            g-batchnm = s-VERM.batchnm
            g-seqno   = s-VERM.seqno.
     
     run oeexvdtl.p.
     
     /* 
     if q-line > 1 then 
       do:
       reposition q-VERM to row q-line.
       display b-VERM with frame f-body.
     end.
     */ 
     self:select-focused-row().
     self:refresh().
             
     put screen row 21 col 3
     color message
  "          F7-Edit PO  F8-Accept   F9-Reject                               ".

   end. /* func7 */
   
   if {k-func8.i} then 
     do:
     if avail s-VERM then
       do:
       assign upd-count = 0.
       for each b-edih where b-edih.cono    = g-cono + 500 and
                             b-edih.batchnm = s-VERM.batchnm and
                             b-edih.seqno   = s-VERM.seqno   and
                             b-edih.doctype = "855":
         assign completefl = yes.
         for each b-edil where b-edil.cono = b-edih.cono and
                               b-edil.batchnm = b-edih.batchnm and
                               b-edil.seqno   = b-edih.seqno
                               no-lock:
           if b-edil.xxc15 <> "UPDATED" then
             assign completefl = no.
           else
             assign upd-count = upd-count + 1.
         end.
         if completefl = yes or upd-count > 0 then
           assign b-edih.refer    = "ACCEPT"
                  s-VERM.baccept  = "X"
                  b-edih.transdt  = TODAY
                  b-edih.operinit = g-operinit
                  b-edih.transtm  = substr(string(TIME,"HH:MM"),1,2) +
                                    substr(string(TIME,"HH:MM"),4,2). 
       end. /* for each b-edih */
     end. /* avail s-VERM */
     if s-VERM.baccept = "X" then
       display s-VERM.baccept with browse b-VERM.
     if completefl = no and upd-count > 0 then
       message 
       "Not All lines have been maintained.  PLEASE F7-Edit to confirm".
     else
       if completefl = no then
         message
        "Cannot Accept.  Lines on the PO have not been maintained".
   end. /* func8 */
   
   if {k-func9.i} then 
     do:
     if avail s-VERM then
       do:
       for each b-edih where b-edih.cono    = g-cono + 500 and
                             b-edih.batchnm = s-VERM.batchnm and
                             b-edih.seqno   = s-VERM.seqno   and
                             b-edih.doctype = "855":
         assign b-edih.refer = "REJECT"
                s-VERM.baccept = "R"
                b-edih.operinit = g-operinit
                b-edih.transdt  = TODAY
                b-edih.transtm  = substr(string(TIME,"HH:MM"),1,2) +
                                  substr(string(TIME,"HH:MM"),4,2).
       end. /* for each b-edih */
     end. /* avail s-VERM */
     if s-VERM.baccept = "R" then                
       display s-VERM.baccept with browse b-VERM.
   end. /* func9 */
end.

if lastkey = 401 then leave.

on cursor-up cursor-up.
on cursor-down cursor-down.

put screen row 21 col 3
   color message
  "          F7-Edit PO  F8-Accept   F9-Reject                              ".

main:

  do while true on endkey undo main, leave main:

     if {k-cancel.i} or lastkey = 401 then
         leave main.
     
     open query q-VERM for each s-VERM no-prefetch.
     apply "ENTRY" to b-VERM in frame f-body.
     enable b-VERM with frame f-body.
     
     put screen row 21 col 3
        color message
  "          F7-Edit PO  F8-Accept   F9-Reject                              ".


     wait-for window-close of current-window.
     
     close query q-VERM.
     input clear.
     on cursor-up back-tab.
     on cursor-down tab.
     
end.
