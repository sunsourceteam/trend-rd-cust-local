
define {1} shared stream custin.
input stream custin from "/rd/cust/local/repovrfile.csv".

define var inx as integer format "99999" init 0 no-undo.
define var c_cust  as c format "x(12)"      no-undo.
define var c_shipto like arss.shipto        no-undo.
define var c_slsmn as c format "x(4)"       no-undo. 
define var c_tech  as c format "x"          no-undo.
define var p_parm as logical                no-undo.

define {1} shared var h_cust  as c format "x(12)"       no-undo.
define {1} shared var h_slsrepin as c format "x(4)"     no-undo. 
define {1} shared var h_shipto like arss.shipto         no-undo. 
define {1} shared var h_techtype as char format "x"     no-undo.
define {1} shared var h_tech     as char format "x"     no-undo.



define {1} shared temp-table cst no-undo
    field cst_cono      like arsc.cono
    field cst_shipto    like arss.shipto
    field cst_cust      like arsc.custno
    field cst_slsrepin  like arsc.slsrepin
    field cst_slsrepout like arsc.slsrepout
    field cst_tech      as character format "x"
    index cst_cono
          cst_cust
          cst_shipto 
          cst_slsrepin
          cst_tech.

/****** main Block *********/


if "{1}" = "new" then 
   do: 
    input stream custin from "/rd/cust/local/repovrfile.csv".
    run load_temp_tbl. 
    input stream custin close.
   end.
