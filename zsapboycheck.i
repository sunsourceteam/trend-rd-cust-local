/* zsapboycheck

   Validates selection criteria using variable parameters in zsapboydef
   
*/   

Procedure zelectioncheck:

  zelection_good = true.
  if zelection_type = "s" and zelection_matrix [1] <> 0  then
    do:
    find zsapbo where zsapbo.selection_type = "s" and
                      zsapbo.selection_char4 = zelection_char4 and
                      zsapbo.selection_cust = 0 no-lock no-error.
    if zelection_char4 ge zzl_begrep and 
       zelection_char4 le zzl_endrep then
      do: 
      if not avail zsapbo and zelection_matrix [1] > 1 then
        zelection_good = false.
      else
      if avail zsapbo and zsapbo.selection_mode = "x" then
        zelection_good = false.
      end.
    if zelection_char4 < zzl_begrep or 
       zelection_char4 > zzl_endrep or
       zelection_mode = "i" then
      do:
      if avail zsapbo and zsapbo.selection_mode <> "i" then
        zelection_good = false.
      else
      if not avail zsapbo then
        zelection_good = false.
      end.
     
    end.
  else    
  if zelection_type = "c" and zelection_matrix [2] <> 0 then
    do:
    find zsapbo where zsapbo.selection_type = "c" and
                      zsapbo.selection_char4 = "" and
                      zsapbo.selection_cust = zelection_cust no-lock no-error.
     
     if zelection_cust ge zzl_begcust and 
        zelection_cust le zzl_endcust then
       do:
       if not avail zsapbo and zelection_matrix[2] > 1 then
         zelection_good = false.
       else
       if avail zsapbo and zsapbo.selection_mode = "x" then
         zelection_good = false.
       end.
       
    if zelection_cust < zzl_begcust or 
       zelection_cust > zzl_endcust or
       zelection_mode = "i" then
      do:
      if avail zsapbo and zsapbo.selection_mode <> "i" then
        zelection_good = false.
      else
      if not avail zsapbo then
        zelection_good = false.
      end.
 
    
    end.
  else    
  if zelection_type = "p" and zelection_matrix [3] <> 0 then
    do:
    find zsapbo where zsapbo.selection_type = "p" and
                      zsapbo.selection_char4 = zelection_char4 and
                      zsapbo.selection_cust = 0 no-lock no-error.
    if zelection_char4 ge zzl_begcat and 
       zelection_char4 le zzl_endcat then
      do:
      if not avail zsapbo and zelection_matrix[3] > 1 then
        zelection_good = false.
      else
      if avail zsapbo and zsapbo.selection_mode = "x" then
        zelection_good = false.
      end. 
       
    if zelection_char4 < zzl_begcat or 
       zelection_char4 > zzl_endcat or
       zelection_mode = "i" then
      do:
      if avail zsapbo and zsapbo.selection_mode <> "i" then
        zelection_good = false.
      else
      if not avail zsapbo then
        zelection_good = false.
      end.
    end.
  else    
  if zelection_type = "b" and zelection_matrix [4] <> 0 then
    do:
    find zsapbo where zsapbo.selection_type = "b" and
                      zsapbo.selection_char4 = zelection_char4 and
                      zsapbo.selection_cust = 0 no-lock no-error.
    if zelection_char4 ge zzl_begbuy and 
       zelection_char4 le zzl_endbuy then
      do:
      if not avail zsapbo and zelection_matrix [4] > 1 then
        zelection_good = false.
      else
      if avail zsapbo and zsapbo.selection_mode = "x" then
        zelection_good = false.
      end.
       
    if zelection_char4 < zzl_begbuy or 
       zelection_char4 > zzl_endbuy or
       zelection_mode = "i" then
      do:
      if avail zsapbo and zsapbo.selection_mode <> "i" then
        zelection_good = false.
      else
      if not avail zsapbo then
        zelection_good = false.
      end.
 
     
    end.
  else
  if zelection_type = "t" and zelection_matrix [5] <> 0 then
    do:
    find zsapbo where zsapbo.selection_type = "t" and
                      zsapbo.selection_char4 = zelection_char4 and
                      zsapbo.selection_cust = 0 no-lock no-error.
    if zelection_char4 ge zzl_begtype and 
       zelection_char4 le zzl_endtype then
      do: 
      if not avail zsapbo and zelection_matrix [5] > 1 then
        zelection_good = false.
      else
      if avail zsapbo and zsapbo.selection_mode = "x" then
        zelection_good = false.
      end.
       
    if zelection_char4 < zzl_begtype or 
       zelection_char4 > zzl_endtype or
       zelection_mode = "i" then
      do:
      if avail zsapbo and zsapbo.selection_mode <> "i" then
        zelection_good = false.
      else
      if not avail zsapbo then
        zelection_good = false.
      end.
    end.
    
    
  else
  if zelection_type = "w" and zelection_matrix [6] <> 0 then
    do:
    find zsapbo where zsapbo.selection_type = "w" and
                      zsapbo.selection_char4 = zelection_char4 and
                      zsapbo.selection_cust = 0 no-lock no-error.
    if zelection_char4 ge zzl_begwhse and 
       zelection_char4 le zzl_endwhse then
      do: 
      if not avail zsapbo and zelection_matrix [6] > 1 then
        zelection_good = false.
      else
      if avail zsapbo and zsapbo.selection_mode = "x" then
        zelection_good = false.
      end.
       
    if zelection_char4 < zzl_begwhse or 
       zelection_char4 > zzl_endwhse or
       zelection_mode = "i" then
      do:
      if avail zsapbo and zsapbo.selection_mode <> "i" then
        zelection_good = false.
      else
      if not avail zsapbo then
        zelection_good = false.
      end.
 
     
    end.
  else
  if zelection_type = "v" and zelection_matrix [7] <> 0 then
    do:
    find zsapbo where zsapbo.selection_type = "v" and
                      zsapbo.selection_char4 = "" and
                      zsapbo.selection_cust = zelection_vend no-lock no-error.
     
     if zelection_vend ge zzl_begvend and 
        zelection_vend le zzl_endvend then
       do:
       if not avail zsapbo and zelection_matrix[7] > 1 then
         zelection_good = false.
       else
       if avail zsapbo and zsapbo.selection_mode = "x" then
         zelection_good = false.
       end.
       
    if zelection_vend < zzl_begvend or 
       zelection_vend > zzl_endvend or
       zelection_mode = "i" then
      do:
      if avail zsapbo and zsapbo.selection_mode <> "i" then
        zelection_good = false.
      else
      if not avail zsapbo then
        zelection_good = false.
      end.
 
    
    end.
  else
  if zelection_type = "n" and zelection_matrix [8] <> 0 then
    do:
    find zsapbo where zsapbo.selection_type = "n" and
                      zsapbo.selection_char4 = zelection_char4 and
                      zsapbo.selection_cust = 0 no-lock no-error.
     
     if zelection_char4 ge zzl_begvname and 
        zelection_char4 le zzl_endvname then
       do:
       if not avail zsapbo and zelection_matrix[8] > 1 then
         zelection_good = false.
       else
       if avail zsapbo and zsapbo.selection_mode = "x" then
         zelection_good = false.
       end.
       
    if zelection_char4 < zzl_begvname or 
       zelection_char4 > zzl_endvname or
       zelection_mode = "i" then
      do:
      if avail zsapbo and zsapbo.selection_mode <> "i" then
        zelection_good = false.
      else
      if not avail zsapbo then
        zelection_good = false.
      end.
 
    
    end.
  else
  if zelection_type = "e" and zelection_matrix [9] <> 0 then
    do:
    find zsapbo where zsapbo.selection_type = "e" and
                      zsapbo.selection_char4 = zelection_char4 and
                      zsapbo.selection_cust = 0 no-lock no-error.
     
     if zelection_char4 ge zzl_begvpid and 
        zelection_char4 le zzl_endvpid then
       do:
       if not avail zsapbo and zelection_matrix[9] > 1 then
         zelection_good = false.
       else
       if avail zsapbo and zsapbo.selection_mode = "x" then
         zelection_good = false.
       end.
       
    if zelection_char4 < zzl_begvpid or 
       zelection_char4 > zzl_endvpid or
       zelection_mode = "i" then
      do:
      if avail zsapbo and zsapbo.selection_mode <> "i" then
        zelection_good = false.
      else
      if not avail zsapbo then
        zelection_good = false.
      end.
 
    
    end.
  else
  if zelection_type = "G" and zelection_matrix [10] <> 0 then
    do:
    find zsapbo where zsapbo.selection_type = "g" and
                      zsapbo.selection_char4 = zelection_char4 and
                      zsapbo.selection_cust = 0 no-lock no-error.
     
     if zelection_char4 ge zzl_techbeg and 
        zelection_char4 le zzl_techend then
       do:
       if not avail zsapbo and zelection_matrix[10] > 1 then
         zelection_good = false.
       else
       if avail zsapbo and zsapbo.selection_mode = "x" then
         zelection_good = false.
       end.
       
    if zelection_char4 < zzl_techbeg or 
       zelection_char4 > zzl_techend or
       zelection_mode = "i" then
      do:
      if avail zsapbo and zsapbo.selection_mode <> "i" then
        zelection_good = false.
      else
      if not avail zsapbo then
        zelection_good = false.
      end.
 
    
    end.
  else
  if zelection_type = "U" and zelection_matrix [11] <> 0 then
    do:
    find zsapbo where zsapbo.selection_type = "u" and
                      zsapbo.selection_char4 = zelection_char4 and
                      zsapbo.selection_cust = 0 no-lock no-error.
     
     if zelection_char4 ge zzl_Prodbeg and 
        zelection_char4 le zzl_Prodend then
       do:
       if not avail zsapbo and zelection_matrix[11] > 1 then
         zelection_good = false.
       else
       if avail zsapbo and zsapbo.selection_mode = "x" then
         zelection_good = false.
       end.
       
    if zelection_char4 < zzl_Prodbeg or 
       zelection_char4 > zzl_Prodend or
       zelection_mode = "i" then
      do:
      if avail zsapbo and zsapbo.selection_mode <> "i" then
        zelection_good = false.
      else
      if not avail zsapbo then
        zelection_good = false.
      end.
    
    end.
  else
  if zelection_type = "h" and zelection_matrix [12] <> 0  then
    do:
    find zsapbo where zsapbo.selection_type = "h" and
                      zsapbo.selection_char4 = zelection_char4 and
                      zsapbo.selection_cust = 0 no-lock no-error.
    if zelection_char4 ge zzl_begshipto and 
       zelection_char4 le zzl_endshipto then
      do: 
      if not avail zsapbo and zelection_matrix [12] > 1 then
        zelection_good = false.
      else
      if avail zsapbo and zsapbo.selection_mode = "x" then
        zelection_good = false.
      end.
    if zelection_char4 < zzl_begshipto or 
       zelection_char4 > zzl_endshipto or
       zelection_mode = "i" then
      do:
      if avail zsapbo and zsapbo.selection_mode <> "i" then
        zelection_good = false.
      else
      if not avail zsapbo then
        zelection_good = false.
      end.
     
  end.
  else    
  if zelection_type = "l" and zelection_matrix [13] <> 0 then
    do:
    find zsapbo where zsapbo.selection_type = "l" and
                      zsapbo.selection_char4 = zelection_char4 and
                      zsapbo.selection_cust = 0 no-lock no-error.
    if zelection_char4 ge zzl_beglbus and 
       zelection_char4 le zzl_endlbus then
      do:
      if not avail zsapbo and zelection_matrix [13] > 1 then
        zelection_good = false.
      else
      if avail zsapbo and zsapbo.selection_mode = "x" then
        zelection_good = false.
      end.
       
    if zelection_char4 < zzl_beglbus or 
       zelection_char4 > zzl_endlbus or
       zelection_mode = "i" then
      do:
      if avail zsapbo and zsapbo.selection_mode <> "i" then
        zelection_good = false.
      else
      if not avail zsapbo then
        zelection_good = false.
      end.
 
     
    end.
  else    
  if zelection_type = "k" and zelection_matrix [14] <> 0 then
    do:
    find zsapbo where zsapbo.selection_type = "k" and
                      zsapbo.selection_char4 = zelection_char4 and
                      zsapbo.selection_cust = 0 no-lock no-error.
    if zelection_char4 ge zzl_begtakenby and 
       zelection_char4 le zzl_endtakenby then
      do:
      if not avail zsapbo and zelection_matrix [14] > 1 then
        zelection_good = false.
      else
      if avail zsapbo and zsapbo.selection_mode = "x" then
        zelection_good = false.
      end.
       
    if zelection_char4 < zzl_begtakenby or 
       zelection_char4 > zzl_endtakenby or
       zelection_mode = "i" then
      do:
      if avail zsapbo and zsapbo.selection_mode <> "i" then
        zelection_good = false.
      else
      if not avail zsapbo then
        zelection_good = false.
      end.
 
     
  end.
  /* */
  else    
  if zelection_type = "bt" and zelection_matrix [15] <> 0 then
    do:
    find zsapbo where zsapbo.selection_type = "bt" and
                      zsapbo.selection_char4 = "" and
                      zsapbo.selection_cust = zelection_cust no-lock no-error.
    if zelection_cust ge zzl_begbatch and 
       zelection_cust le zzl_endbatch then
      do:
      if not avail zsapbo and zelection_matrix [15] > 1 then
        zelection_good = false.
      else
      if avail zsapbo and zsapbo.selection_mode = "x" then
        zelection_good = false.
      end.
       
    if zelection_cust < zzl_begbatch or 
       zelection_cust > zzl_endbatch or
       zelection_mode = "i" then
      do:
      if avail zsapbo and zsapbo.selection_mode <> "i" then
        zelection_good = false.
      else
      if not avail zsapbo then
        zelection_good = false.
      end.
 
     
  end.
   else    
  if zelection_type = "ex" and zelection_matrix [16] <> 0 then
    do:
    find zsapbo where zsapbo.selection_type = "ex" and
                      zsapbo.selection_char4 = zelection_char4 and
                      zsapbo.selection_cust = 0 no-lock no-error.
    if zelection_char4 ge zzl_begexcept and 
       zelection_char4 le zzl_endexcept then
      do:
      if not avail zsapbo and zelection_matrix [16] > 1 then
        zelection_good = false.
      else
      if avail zsapbo and zsapbo.selection_mode = "x" then
        zelection_good = false.
      end.
       
    if zelection_char4 < zzl_begexcept or 
       zelection_char4 > zzl_endexcept or
       zelection_mode = "i" then
      do:
      if avail zsapbo and zsapbo.selection_mode <> "i" then
        zelection_good = false.
      else
      if not avail zsapbo then
        zelection_good = false.
      end.
 
     
  end.
   else    
  if zelection_type = "o" and zelection_matrix [17] <> 0 then
    do:
    find zsapbo where zsapbo.selection_type = "o" and
                      zsapbo.selection_char4 = "" and
                      zsapbo.selection_cust = zelection_cust no-lock no-error.
    if zelection_cust ge zzl_begordno and 
       zelection_cust le zzl_endordno then
      do:
      if not avail zsapbo and zelection_matrix [17] > 1 then
        zelection_good = false.
      else
      if avail zsapbo and zsapbo.selection_mode = "x" then
        zelection_good = false.
      end.
       
    if zelection_cust < zzl_begordno or 
       zelection_cust > zzl_endordno or
       zelection_mode = "i" then
      do:
      if avail zsapbo and zsapbo.selection_mode <> "i" then
        zelection_good = false.
      else
      if not avail zsapbo then
        zelection_good = false.
      end.
 
     
  end.
  else
  if zelection_type = "j" and zelection_matrix [18] <> 0  then
    do:
    find zsapbo where zsapbo.selection_type = "j" and
                      zsapbo.selection_char4 = zelection_char4 and
                      zsapbo.selection_cust = 0 no-lock no-error.
    if zelection_char4 ge zzl_Ibegrep and 
       zelection_char4 le zzl_Iendrep then
      do: 
      if not avail zsapbo and zelection_matrix [18] > 1 then
        zelection_good = false.
      else
      if avail zsapbo and zsapbo.selection_mode = "x" then
        zelection_good = false.
      end.
    if zelection_char4 < zzl_Ibegrep or 
       zelection_char4 > zzl_Iendrep or
       zelection_mode = "i" then
      do:
      if avail zsapbo and zsapbo.selection_mode <> "i" then
        zelection_good = false.
      else
      if not avail zsapbo then
        zelection_good = false.
      end.
     
    end.
           
  /* */         
end.