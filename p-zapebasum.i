
/{&summarize_data}*/

  find sumz where sumz.zbuyer = substring (report.c4,1,4) and
                  sumz.zvend  = report.de12d0 no-lock no-error.
  if not avail sumz then
    do:
    create sumz.
    assign sumz.zvend = report.de12d0
           sumz.zbuyer = substring (report.c4,1,4).

    {w-sasta.i "b" substring(report.c4,1,4) no-lock}
    if avail sasta then
      x-buyername = sasta.descrip.
    else  
      x-buyername = "Not Found".
    sumz.zbuyername = x-buyername.    
    end.
  assign sumz.znet = sumz.znet + s-ponet
         sumz.zaddon = sumz.zaddon + b-report.de9d2s-2
         sumz.zdiff = sumz.zdiff + s-invdiff
         sumz.zinv  = sumz.zinv + s-invnet.
          
/{&summarize_data}*/




/{&print_summary}*/
  hide frame f-top.
  page.
  if p-summarytype = "x" or 
     p-summarytype = "b" then
    do:
    if p-summarytype = "b" then
      view frame f-brtot. 
    else
      view frame f-bvtot.
    
    for each sumz  no-lock
       break by sumz.zbuyer
             by sumz.zvend:
      accumulate sumz.zinv (SUB-TOTAL by        
                                sumz.zbuyer
                                )
                 sumz.znet (SUB-TOTAL by        
                                sumz.zbuyer
                                )
                 sumz.zaddon (SUB-TOTAL by        
                               sumz.zbuyer
                                )
                 sumz.zdiff (SUB-TOTAL by        
                                sumz.zbuyer
                                )
                 sumz.zdiff (total)
                 sumz.zaddon(total)
                 sumz.znet  (total)
                 sumz.zinv  (total).

      if p-summarytype = "x" then
        do:
         find xzapsv where xzapsv.cono = g-cono and 
                         xzapsv.vendno = sumz.zvend no-lock no-error.
           
         display
               sumz.zbuyer 
               sumz.zbuyername
               sumz.zvend
               xzapsv.name
               sumz.znet
               sumz.zinv
               sumz.zaddon
               sumz.zdiff
         with frame f-bvdet.
         down with frame f-bvdet.
         end.
      
      if last-of (sumz.zbuyer) and p-summarytype <>  "x" then
         do:
         
         display 
                sumz.zbuyer
                sumz.zbuyername
                accum sub-total by sumz.zbuyer sumz.znet @ sumz.znet
                accum sub-total by sumz.zbuyer sumz.zinv @ sumz.zinv
                accum sub-total by sumz.zbuyer sumz.zdiff @ sumz.zdiff
                accum sub-total by sumz.zbuyer sumz.zaddon @ sumz.zaddon
         
         with frame f-brdet.
         down with frame f-brdet.
         end.
         
          
      if last-of (sumz.zbuyer) and p-summarytype =  "x" then
         do:
         display 
                sumz.zbuyer
                sumz.zbuyername
                accum sub-total by sumz.zbuyer sumz.znet @ sumz.znet
                accum sub-total by sumz.zbuyer sumz.zinv @ sumz.zinv
                accum sub-total by sumz.zbuyer sumz.zdiff @ sumz.zdiff
                accum sub-total by sumz.zbuyer sumz.zaddon @ sumz.zaddon
          skip(1)
         with frame f-brdet.
         
         end.
   
      end.
      down with frame f-brdet.
      display   
                " " @ sumz.zbuyer
                accum total sumz.znet  @ sumz.znet
                accum total sumz.zinv  @ sumz.zinv
                accum total sumz.zdiff @ sumz.zdiff
                accum total sumz.zaddon @ sumz.zaddon
       with frame f-brdet.
      down with frame f-brdet.
    end.
  else
    if p-summarytype = "v" then
      do:
      view frame f-vrtot.
      
      for each sumz  no-lock
         break 
               by sumz.zvend:
        accumulate sumz.zinv (SUB-TOTAL by        
                                  sumz.zvend
                                  )
                   sumz.znet (SUB-TOTAL by        
                                  sumz.zvend
                                  )
                   sumz.zaddon (SUB-TOTAL by        
                                 sumz.zvend
                                  )
                   sumz.zdiff (SUB-TOTAL by        
                                  sumz.zvend
                                  )
                   sumz.zdiff (total)
                   sumz.zaddon(total)
                   sumz.znet  (total)
                   sumz.zinv  (total).

        if last-of (sumz.zvend) then
           do:
           find xzapsv where xzapsv.cono = g-cono and 
                           xzapsv.vendno = sumz.zvend no-lock no-error.
           
           display 
                  sumz.zvend
                  xzapsv.name
                  accum sub-total by sumz.zvend sumz.znet  @ sumz.znet
                  accum sub-total by sumz.zvend sumz.zinv  @ sumz.zinv
                  accum sub-total by sumz.zvend sumz.zdiff @ sumz.zdiff
                  accum sub-total by sumz.zvend sumz.zaddon  @ sumz.zaddon
           with frame f-vrdet.
           down with frame f-vrdet.
           end.
        end.     
        down with frame f-brdet.
        display 
                " " @ sumz.zbuyer
                accum total sumz.znet @ sumz.znet 
                accum total sumz.zinv @ sumz.zinv
                accum total sumz.zdiff @ sumz.zdiff
                accum total sumz.zaddon @ sumz.zaddon
                with frame f-brdet.
        down with frame f-brdet.
    end.
 




/{&print_summary}*/


