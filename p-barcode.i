procedure scan-bccode:
/* When barcode is read/scanned. Process data through here */
  define input parameter inData like icsp.prod no-undo.
  define output parameter outCode like icsp.prod no-undo.
  /* check for asterisk, space, and single quote ' */
  inData = replace(inData,"?0"," ").
  inData = replace(inData,"?2","*").
  inData = replace(inData,"?3","'").
  /* putting ? back in MUST be done last */
  inData = replace(inData,"?1","?").
  outCode = inData.
end procedure.

procedure set-bccode:
/* When a barcode is about to be printed, barcode width needs to be twice
   character width to replace characters */
  define input parameter inData like icsp.prod no-undo.
  define output parameter outCode like icsp.prod no-undo.
  def var w-asterisk as char initial "*" format "x(1)" no-undo.
  /* check for asterisk, space, and single quote ' */
  inData = right-trim(inData).
  inData = replace(inData,"?","?1").
  inData = replace(inData," ","?0").
  inData = replace(inData,"*","?2").
  inData = replace(inData,"'","?3").
  outCode = w-asterisk + caps(inData) + w-asterisk.
end procedure.

procedure checkString:
  define input parameter inData as char no-undo.
  define output parameter badData as logical no-undo.
  define thisVal as int no-undo.
  define iter as int no-undo.
  badData = no.
  do iter = 1 to length(inData):
    thisVal = substring(inData,iter,1).
    if not ((thisVal >= 48 and thisVal <= 57) or /* 0 - 9 */
       (thisVal >= 65 and thisVal <= 90) or      /* A - Z */
       (thisVal >= 97 and thisVal <= 122)) then  /* a - z */
      badData = yes. 
  end.
end procdure.  
  
