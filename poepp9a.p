/* poepp9a.p */
/*****************************************************************************
  VERSION 5.5    : poepp print - SDI specific - portrait(poepp9a.p) 
  PROCEDURE      : poepp9a.p 
  DESCRIPTION    : Purchase Order Entry PO Print Format 1 Print
  AUTHOR         : cm
  DATE WRITTEN   : 08/31/98
  CHANGES MADE   :
    11/19/98 cm;  TB# 19427 Make WL variables shared
    03/29/00 rgm; TB# 3-2  Add RxServer Interface to Standard
    04/24/01 kjb; TB# e8618 When faxing through RxServer, the footer frame
        was appearing in the body of the document
    09/29/02 n-jk; TB# e8651 Core - Show Core Allocation Detail
    10/07/02  al; TB# x74 Change PO 'Invoice To' information - PTX
    11/12/02 kjb; TB# e15321 Modify the Tally print logic - Added tallyprt.lfo
        which contains the new variable and form definitions for the new tally
        print logic
    04/23/07 mms; TB# e20597 Backout changes for-Add PO Chained Discounts
******************************************************************************/
{p-rptbeg.i}

/* tb# x74 - Add nxtrend.gpp, PTX variables and library */
{nxtrend.gpp}
&IF "{&NXTREND-APP}" = "PTX" &THEN
  {m-ptxflags.i}
  {po-ptx-invoiceto.lva}
&ENDIF

/*tb 19427 11/19/98 cm; Make WL variables shared. */
{wlwhse.gva "shared"}

/*d temp table defines */
{wlptemp.gva &shared = "shared"}

def input param    v-faxfl       as logical                         no-undo.
/*
def     shared var v-headingfl   as logical                         no-undo.
*/
def            var v-processty   as char format "x(3)"              no-undo.

/*tb e8651 09/29/02 n-jk; Core - Show Core Allocation Detail */
def            var v-price       like poel.price                    no-undo.

/* Sx55 */
def     shared var v-blprint     as logical                         no-undo. 
def            var v-laserend    as char                            no-undo.
def            var v-loopcnt     as int                             no-undo.
def            var s-apcustno    like apss.apcustno                 no-undo.
def            buffer z-poel     for poel. 
/** SX55 - need these variables for the p-oeepp1.i in poepp1vp.lpr **/
{oeepp6.z99 &define_frames = "*"}   

def     shared buffer sapbo      for sapbo.
def     shared buffer poeh       for poeh.
def     shared buffer apsv       for apsv.
def     shared buffer b-sasc     for sasc.
def            buffer b-icsd     for icsd.
/*tb e8651 09/29/02 n-jk; Core - Show Core Allocation Detail */
def            buffer b-poel     for poel.

def var v-whse        like icsw.whse       no-undo. 
def var v-qtyship     like oeel.qtyship    no-undo.
def var s-prod        like icsw.prod       no-undo.


/*tb 3-2 03/29/00 rgm; Rxserver interface hook for extra vars */
{poepp1.z99 &define_rxvars = "*"}

{rxserv3.lva "new shared"}

/* SX55 */
{f-poepp9.isx}  

{poepp1va.lva}
{poepp1va.lfo}
{tallyprt.lfo &startpos = 5}
if poeh.shipfmno ne 0 then
    {w-apss.i poeh.vendno poeh.shipfmno no-lock}

if not avail apsv then
  {w-apsv.i poeh.vendno no-lock}
if avail apsv and substr(apsv.user11,1,4) <> "    " then
  do:
  find icsd where icsd.cono = g-cono and
                  icsd.whse = substr(apsv.user11,1,4) no-lock no-error.
  if not avail icsd then
    if poeh.billtowhse ne "" then
      {w-icsd.i poeh.billtowhse no-lock}
end.
else
  if poeh.billtowhse ne "" then
    {w-icsd.i poeh.billtowhse no-lock}

/*tb 14171 05/17/95 mtt; Lock table overflow problem in F10 */
find b-sasc where b-sasc.cono = g-cono no-lock no-error.

/* SX55 *****************************************/
/* Set the flag to indicate if headings should be printed or if a preprinted
   form is used */
{w-sasp.i sapb.printernm no-lock}
assign v-headingfl = no. 
/** SX55 always set to no 
v-headingfl = if avail sasp and sasp.ptype = "f" then yes
                    else b-sasc.popoheadfl.
*/ 
/* SX55 */
{lasersdi.lva new}

if sapb.printernm = "vid"
then assign v-tovid = yes
            v-tof   = v-esc + "&a0h40V".

/* si01: PO is always type 1 (1y part of code) */
/**v-rxform = v-esc + "&f1y4X".   das - replaced with zsdigetlogo.p **/
/* check if printer has ALL LOGO Flash */
if avail sasp and sasp.user5[10] = 0 then
  run zsdigetlogo.p (input "PO  ",
                     input v-esc,
                     input recid(poeh),
                     input no).
else
  assign v-rxform = v-esc + "&f1y4X".

if not v-tovid and not v-faxfl then 
   put control v-reset + v-lineterm + v-deftray + v-noperf +
               v-rxform /* + v-tof si02*/  + v-6lpi + v-goth17.
/* SX55 *****************************************/

/* Print the # of POs requested for the vendor */
do v-index = 1 to if v-faxfl = yes then 1 else apsv.nopocopies:
   
   /* SX55 - print po logic - returns to poepp9.p  */
   assign v-tof = v-tof + "~012" + "~015".
   {p-poepp6.i}
    page.
    /*tb e8618 04/24/01 kjb; When faxing through RxServer, the footer is
        appearing in the body of the PO ticket.  Need to set as 'paged' so
        that formating is done correctly. */
    &IF DEFINED(rxserv_format) = 0 &THEN
    if v-faxfl = yes then do:
        hide frame f-tot4.
        hide frame f-head.
        hide frame f-headl.
    end.
    else
    &ENDIF
    page.

end.

/* SX55 **********************************************/
/** On BL type PO's- print the release information  **/
procedure print_releases:
do: 
   if poeh.transtype = "br" then 
   find first z-poel use-index k-poel where 
              z-poel.cono      = poel.cono     and 
              z-poel.pono      = poel.pono     and 
              z-poel.posuf     = poel.posuf    and 
              z-poel.lineno    = poel.lineno   and 
              z-poel.shipprod  = poel.shipprod and 
              z-poel.transtype = "br" 
              no-lock no-error.
   else 
   find first z-poel use-index k-poel where 
              z-poel.cono      = poel.cono     and 
              z-poel.pono      = poel.pono     and 
              z-poel.lineno    = poel.lineno   and 
              z-poel.shipprod  = poel.shipprod and 
              z-poel.transtype = "br" 
              no-lock no-error.
   if avail z-poel then 
    do:
      display s-lit52a s-lit52b s-lit52c with frame f-relmsg1.
    end.
   else 
      leave.
   for each z-poel use-index k-poel where 
            z-poel.cono      = poeh.cono     and 
            z-poel.pono      = poeh.pono     and 
            z-poel.lineno    = poel.lineno   and 
            z-poel.shipprod  = poel.shipprod and 
            z-poel.transtype = "br" 
            no-lock:
   
   assign s-relpono = string(z-poel.pono,"z999999") + "-" + 
                      string(z-poel.posuf,"99")
          s-relprod = z-poel.shipprod
          s-relqty  = dec(z-poel.qtyord)
          s-reldate = z-poel.duedt.            

   if poeh.transtype = "br" and z-poel.posuf < poeh.posuf then 
    next. 
   else    
    display s-relpono s-relprod
            s-relqty  s-reldate 
            with frame f-reldet1.
            down with frame f-reldet1.

   end. /* for each z-poel */
   assign s-spaces = " ".
   display s-spaces with frame f-spaces.
   leave.
end. /* big do */
end. /* procedure */



