/*****************************************************************************
   
 pdxmsslsr.i  - Sales Price Matrix Print Include
                
*****************************************************************************/

procedure sort-by-slsrep:
  
  for each matrix use-index k-salesrep /* where matrix.i-ytd-amt <> 0 */ 
     no-lock
                                              
     break by matrix.salesrep
           by matrix.customer
           by matrix.ship-to
           by matrix.i-ytd-amt descending
           by matrix.pprice-type
           by matrix.product:
          
     assign s-isales    = s-isales + matrix.i-amount
            s-imrgsales = s-imrgsales + (matrix.i-amount - matrix.i-cost)
            s-csales    = s-csales + matrix.c-amount
            s-cmrgsales = s-cmrgsales + (matrix.c-amount - matrix.c-cost)
            s-fsales    = s-fsales + matrix.f-amount
            s-fmrgsales = s-fmrgsales + (matrix.f-amount - matrix.f-cost).
            
     assign s-bsales    = s-bsales + matrix.i-amount   /* BEFORE */
            s-bmrgsales = s-bmrgsales + (matrix.i-amount - matrix.i-cost)
            s-asales    = s-asales + matrix.i-amount   /* AFTER  */
            s-amrgsales = s-amrgsales + (matrix.f-amount - matrix.f-cost).
            
     assign s-dbsales    = s-dbsales + matrix.i-amount   /* BEFORE */       
            s-dbmrgsales = s-dbmrgsales + (matrix.i-amount - matrix.i-cost) 
            s-dasales    = s-dasales + matrix.i-amount   /* AFTER  */       
            s-damrgsales = s-damrgsales + (matrix.f-amount - matrix.f-cost).
        
                          
     if s-isales <> 0 then
        assign s-imarg = (s-imrgsales / s-isales) * 100.
     if s-csales <> 0 then
        assign s-cmarg = (s-cmrgsales / s-csales) * 100.
     if s-fsales <> 0 then
        assign s-fmarg = (s-fmrgsales / s-fsales) * 100.
     
     if p-print = yes then do:
        export delimiter ","
                matrix.salesrep     
                matrix.customer         
                matrix.ship-to
                matrix.name     
                matrix.cprice-type  
                matrix.pprice-type
                matrix.product
                matrix.i-ytd-amt
                matrix.i-netprc
                matrix.i-listprc
                matrix.i-discmult
                matrix.i-pdtype
                s-imarg
                matrix.c-netprc
                s-cmarg
                matrix.pd-level
                matrix.f-netprc
                matrix.f-discmult
                matrix.f-pdtype
                s-fmarg
                matrix.f-level
                matrix.pd-recno     
                matrix.usage00 
                matrix.usage01 
                matrix.usage02
                matrix.pd-trndt
                matrix.pd-enddt.
     end.            
     else do:   
     if p-infile = "" then do:
        display matrix.salesrep     
                matrix.customer         
                matrix.ship-to
                matrix.name     
                matrix.cprice-type  
                matrix.pprice-type
                matrix.product
                matrix.i-ytd-amt
                matrix.i-netprc
                matrix.i-listprc
                matrix.i-discmult
                matrix.i-pdtype
                s-imarg
                matrix.c-netprc
                s-cmarg
                matrix.pd-level
                matrix.f-netprc
                matrix.f-discmult
                matrix.f-pdtype
                s-fmarg
                matrix.f-level
                matrix.pd-recno     
        with frame f-detail.
        down with frame f-detail.
     end.
     else do:
        if p-update = no then do:
           find inp use-index k-input where inp.z-slsrep = matrix.salesrep and
                                            inp.z-cust   = matrix.customer and
                                            inp.z-shipto = matrix.ship-to  and
                                            inp.z-prod   = matrix.product
                                            no-error.
           /*
           input from value(p-infile).
           repeat:
              import delimiter ","
                 i-slsrep    i-cust      i-shipto i-name   i-ctype   i-pptype
                 i-prod      i-prodsales i-lastsell
                 i-asell     i-adiscmult i-amrg
                 i-csell                 i-cmrg   i-cpdlvl
                 i-fsell     i-fdiscmult i-fmrg   i-fpdlvl
                 i-pdrecno   i-change    i-factor i-chdate i-enddate.
               if i-slsrep = matrix.salesrep and 
                  i-cust   = matrix.customer and
                  i-shipto = matrix.ship-to  and
                  i-prod   = matrix.product then 
                  leave.
           end.
           input close.
           */
           if inp.z-cpdlvl > 1 then do:
              assign inp.z-fpdlvl  = 1
                     inp.z-pdrecno = 0.
           end.
           assign s-vs1   = ((f-netprc / inp.z-lastsell) - 1) * 100
                  s-vs2   = ((f-netprc / matrix.i-netprc) - 1) * 100.
           output stream dryrun to value(p-infile + "_dr") append.
           export stream dryrun delimiter ","
                 matrix.salesrep
                 matrix.customer
                 matrix.ship-to
                 matrix.name
                 matrix.pprice-type
                 matrix.product
                 inp.z-lastsell
                 inp.z-asell
                 inp.z-adiscmult
                 inp.z-amrg
                 matrix.i-netprc
                 matrix.i-listprc
                 matrix.i-discmult
                 s-imarg
                 inp.z-fsell
                 inp.z-fdiscmult
                 inp.z-fmrg
                 inp.z-fpdlvl
                 inp.z-pdrecno
                 matrix.f-netprc
                 matrix.f-discmult
                 s-fmarg
                 s-vs1
                 s-vs2
                 matrix.change
                 matrix.change-amt
                 
                 matrix.if-inact-amt 
                 matrix.if-inact-cost 
                 matrix.if-inact-pd   
                 matrix.if-inact-pdtp 
                 matrix.new-enddt.
            output stream dryrun close.
        end.
        else do:
           display matrix.salesrep   
                   matrix.customer   
                   matrix.ship-to
                   matrix.name       
                   matrix.pprice-type
                   matrix.product    
                   matrix.i-netprc
                   matrix.i-listprc
                   matrix.i-discmult
                   matrix.i-pdtype
                   s-imarg
                   matrix.c-netprc   
                   s-cmarg           
                   matrix.pd-level   
                   matrix.f-netprc  
                   matrix.f-discmult
                   matrix.f-pdtype
                   s-fmarg           
                   matrix.f-level    
                   matrix.pd-recno
                   matrix.change
                   matrix.change-amt
           with frame f-detail-a.
           down with frame f-detail-a.
           /*
           if p-update = yes and matrix.change = "Y" then do:
              run pd_create.p(i-cust,
                              matrix.pd-recno,
                              matrix.product,
                              matrix.pprice-type,
                              matrix.pd-level,
                              matrix.change-amt,
                              matrix.f-level).
           end.
           */
        end.
     end.                            
     end.
     assign s-isales    = 0
            s-csales    = 0
            s-fsales    = 0
            s-imrgsales = 0
            s-cmrgsales = 0
            s-fmrgsales = 0
            s-imarg     = 0
            s-cmarg     = 0
            s-fmarg     = 0.
            
    if last-of(matrix.salesrep) then do:
       if s-bsales <> 0 then                              
          assign s-bmarg = (s-bmrgsales / s-bsales) * 100.
       if s-asales <> 0 then                              
          assign s-amarg = (s-amrgsales / s-asales) * 100.


       if p-infile <> "" then do:
          if p-update = no then do:
             output stream dryrun to value(p-infile + "_dr") append.
             export stream dryrun delimiter ","
                matrix.salesrep
                ""  /* customer #  */
                ""  /* ship to     */
                ""  /* cust name   */
                ""  /* cust type   */
                "TOTAL MARGINS:"  /* part number */
                ""  /* last sell   */
                ""  /* list price  */
                ""  /* disc mult   */
                ""  /* old inv mrg */
                ""  /* last sell   */
                ""  /* list price  */
                ""  /* disc mult   */
                s-bmarg
                ""  /* net sell    */
                ""  /* disc mult   */
                ""  /* old fut mrg */
                ""  /* net sell    */
                ""  /* disc mult   */
                ""  /* pd level    */
                ""  /* pd recno    */
                s-amarg.
             output stream dryrun close.
          end.
          else do:
             display matrix.salesrep
                     s-bmarg
                     s-amarg
                     with frame f-margtot.
                     down with frame f-margtot.
          end.
       end.
       page.
       
       
       
       assign s-bmarg     = 0
              s-amarg     = 0
              s-bmrgsales = 0
              s-amrgsales = 0
              s-bsales    = 0
              s-asales    = 0.
    end.

  end.
   
  page.

end. /* procedure */

