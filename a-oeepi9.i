/*sx5.5*/
/* a-oeepi9.i 5/27/97 */
/*si**** Custom ****************************************************************
CLIENT  : 80281 SDI
JOB     : sdi009 Custom invoice print
AUTHOR  : msk
VERSION : 7
CHANGES :
    si01 5/27/97 msk; 80281/sdi009; initial changes
        - remove UPC title
        - remove corr/remit titles

*******************************************************************************/

/* a-oeepi1.i 1.4 4/21/93 */
/*h*****************************************************************************
  INCLUDE      : a-oeepi1.i
  DESCRIPTION  : Assign label for invoice
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    11/11/92 mms; TB# 8561 Display product surcharge label from ICAO
    04/21/93 jlc; TB# 11023 Display Direct Order on DO's
*******************************************************************************/

if v-headingfl then assign
/*    {1}lit1b  = "UPC Vendor   Invoice Date       Order #"         si01 */
    {1}lit1b  = "             Invoice Date       Order #"
    {1}lit2a  = if can-do("ra,rm",oeeh.transtype) then
                    "========================================="
                else ""
    {1}lit3a  = "  Cust #:"
    {1}lit3b  = "PO Date   PO #                   Page #"
    {1}lit6a  = "Ship To:"
/*    {1}lit6b  = "Correspondence To:"                              si01 */
    {1}lit8a  = if g-country = "ca" then "PST Lic#:" else ""
    {1}lit9a  = if g-country = "ca" then "GST Reg#:" else ""
    {1}lit11a = "Bill To:"
    {1}lit11b = "Instructions"
    {1}lit12a = "Ship Point"
    {1}lit12b = "Via"
    {1}lit12c = "Shipped"
    {1}lit12d = "Terms"
    {1}lit14a =
"    Product                   UPC      Quantity     Quantity     Quantity     "
    {1}lit14b =
"Qty.     Unit       Price     Discount         Amount    "
    {1}lit15a =
"Ln# And Description          Item#     Ordered        B.O.       Shipped      "
    {1}lit15b =
" UM      Price       UM      Multiplier        (Net)     "
    {1}lit16a =
"------------------------------------------------------------------------------"
    {1}lit16b =
"----------------------------------------------------------".

assign
    {1}lit1aa = if v-headingfl = yes then "Document: " else ""
    {1}lit1a  = if oeeh.transtype = "rm" then
                    "Invoice - Return Merchandise"
                else if oeeh.transtype = "cr" then
                    "Invoice - Correction"
                else if oeeh.transtype = "ra" then
                    "Invoice - Receipt - DO NOT PAY"
                else "Invoice"
    {1}lit1a  = if      oeeh.transtype = "fo" then {1}lit1a + " - Future Order"
                else if oeeh.transtype = "st" then {1}lit1a +
                    " - Standing Order"
                else if oeeh.transtype = "qu" then {1}lit1a + " - Quote Order"
                else if oeeh.transtype = "bl" then {1}lit1a + " - Blanket Order"
                else if oeeh.transtype = "br" and oeeh.stagecd = 0 then
                    {1}lit1a + " - Blanket Release (Entered)"
                else if oeeh.transtype = "br" then {1}lit1a +
                     " - Blanket Release"
                /*tb 11023 04/21/93 jlc; Display Direct Order on DO's */
                else if oeeh.transtype = "do" then {1}lit1a +
                    " - Direct Order"
                else {1}lit1a
    {1}lithd1 = "                         "
    /******** das
    {1}lithd2 = "TEL:"
    {1}lithd3 = "FAX:"
    ************/
    {1}lithd2 = "Sales  Inquiries:"
    {1}lithd3 = "Credit Inquiries:"
    {1}lit40a = "Lines Total"
    {1}lit40c = "Qty Shipped Total"
    {1}lit40d = "Total             "
    {1}lit40d = if can-do("bl,br",oeeh.transtype) and
                    oeeh.lumpbillfl = yes then
                "Amount Billed     " else
                "Total             "
    {1}lit41a = "Order Discount    "
    {1}lit42a = "Other Discount    "
    {1}lit43a = "Core Charge       "
    {1}lit44a = v-icdatclabel
    {1}lit45a = "Taxes             "
    {1}lit46a = "Downpayment       "
    {1}lit46b = "Payment           "
    {1}lit47a = "Invoice Total     "
    {1}lit47b = "Payment           "
    {1}lit48a = "Continued"
    {1}lit48b = "Cash Discount"
    {1}lit48c = "If Paid By"
    {1}lit48d = "If Paid Within Terms"
    {1}lit49a = "CREDIT - DO NOT PAY"
    {1}lit50a = "==================="
    {1}lit51a = "G.S.T.            "
    {1}lit52a = "P.S.T.            "
    {1}lit53a = "Restock Amount    ".
