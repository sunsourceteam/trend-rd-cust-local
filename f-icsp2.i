/* Init: smaf  Date: 05/05/09  Location:  /home/smaf/oeiolereturns  Proj:    
  Display NR/NC code in F10 buyer icsp.  
*/


/* f-icsp.i 1.1 01/03/98 */
/*h*****************************************************************************
  INCLUDE         : f-icsp.i
  DESCRIPTION     : Inventory Control - Setup Products - Form include
  USED ONLY ONCE? : no
  AUTHOR          : rhl
  WRITTEN         : 07/03/89
  CHANGES MADE    :
    03/15/93 mtt; tb# 10366 changed help message for "List on Invoice" to
        indicate bid proposals
    10/08/93 dww; TB# 13281 Add "Supplier Group" to ICSP
    10/18/93 rhl; TB# 13364 Add the edi product/service identifi
    11/15/93 dww; TB# 13669 Take ICSP defaults from ICSC
    01/10/95 dww; TB# 17474 Forms misalignment in Progress V7.
    06/05/95 ajw; TB# 18575; Nonstock/Special Kit Component Project (E4)
        Added the Nonstock Kit Component Required Flag to the screen.
    10/26/95 dww; TB# 18091 Expand UPC Number. Remove UPC #.
    02/19/97 jl;  TB# 7241 (4.3) Special Price Costing. Change csunperstk,
        speccostty, and prccostper from icsp to s-. Added labels.
    08/24/99 bp ; TB# 26575 SPC Enhancement.  Here, add lookup to
        s-speccostty field.
    06/28/00 doc; TB# e5642; WT BOD kit transfer project
        Added ICSP.bodtransferty and ICSP.tiedcompprt
*******************************************************************************/
form
    icsp.prod           colon 17
    icsp.descrip[1]     colon 17    label "Description"
    icsp.descrip[2]     at 47    no-label
    icsp.lookupnm       colon 17
    icsp.seqno          at 35       no-label help "Sequence #"
    icsp.bolclass       colon 64
    icsp.statustype     colon 17
    icsp.weight         colon 64
    icsp.cubes          colon 64    label "Cube"
    o-nrnc              colon 17    label " NR/NC:  "
    /* skip(1) */
    icsp.prodcat        colon 17
    s-prodcat           at 24    no-label
    icsp.lifocat        colon 64    label "Lifo Cat"
       help "Lifo Category                                               [L]"
    icsp.unitstock      colon 17    label "Units- Stocking"
    icsp.priceonty      colon 64
    icsp.unitsell       colon 17    label "Selling"
    s-selldesc          at 24    no-label
    icsp.termsdiscfl    colon 64    label "Terms Disc"
    icsp.termspct       at 70       no-label
    "%"                 at 76
    icsp.unitcnt        colon 17    label "Counting"
    s-cntdesc           at 24    no-label
    icsp.sellmult       colon 64
    skip(1)
    icsp.kittype        colon 17
    icsp.bodtransferty  colon 40    label "Allow BOD Transfer"
    s-speccostty        colon 64    label "Special Price/Cost"
        help "                                                           [L]"
    icsp.kitrollty      colon 17    label "OE/BP Kit Rollup"
    s-csunperstk        colon 64    label "Price/Cost Units Per Stk Unit"
    icsp.exponinvfl     colon 17    label "Component Opts"
        help "Yes to List the Components on the Invoice and Bid Proposal"
    icsp.kitnsreqfl     at 23       no-label
       help "Yes to Require at Least One Nonstock Comp to be Entered (OE/BP)"
    icsp.tiedcompprt    at 27       no-label
    s-prccostper        colon 64    label "Special Price/Cost Unit"
    skip (1)
    icsp.msdsfl         colon 17
    icsp.msdssheetno    colon 29    label "Sheet#"
    icsp.msdschgdt      at 42    no-label help "Date of Last MSDS Change"
    icsp.corecharge     colon 64
    icsp.edicd          colon 17
    icsp.warrlength     colon 64
    icsp.warrtype       at 71    no-label
    icsp.slgroup        colon 17
    icsp.user1          colon 64
    icsp.pbseqno        colon 17    label "Price Book Seq#"
    icsp.user2          colon 64
    with frame f-icsp2 side-labels row 1 width 80 overlay no-hide
         title "Custom ICSP Update".


