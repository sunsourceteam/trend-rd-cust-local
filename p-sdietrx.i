define stream zprd.

define stream xprd.


procedure introduce_stats:


   v-printdir = "/mod20b/workfiles/".
assign v-process = false.





if search ("/usr/tmp/tbxddw" + ".smk") <> ? then
  do:
  v-process = true.

  output stream zprd to 
     value(v-printdir + "tbxddw" + ".sxk").  
     input stream  xprd from 
     value("/usr/tmp/tbxddw" + ".smk").

    
    put stream zprd control
     "Tot"
     "~011"
     "Region"
     "~011"
     "District"
     "~011"
     "Whse"
     "~011"
     "Cust"
     "~011"
     "CustName"
     "~011"
     "Shipto"
     "~011"
     "Pcat"
     "~011"
     "PcatDescrip"
     "~011"
     "Prod"
     "~011"
     "MTDsls1" /* Month 1 Sales             */
     "~011"
     "MTDmgn1" /* Month 1 Margin            */
     "~011"
     "MTDsls2" /* Month 2 Sales             */
     "~011"
     "MTDmgn2" /* Month 2 Margin            */
     "~011"
     "MTDsls3" /* Month 3 Sales             */
     "~011"
     "MTDmgn3" /* Month 3 Margin            */
     "~011"
     "YTDxsls"
     "~011"
     "YTDxmgn"  /* Current YTD margin        */
     "~011"
     "LYTDxsls" /* Previous YTD Sales        */
     "~011"
     "LYTDxmgn" /* Previous YTD Margin       */
     "~011"
     "Varsls"   /* Variance Sales            */
     "~011"
     "Vardiff"  /* Variance Difference %     */
     "~011"
     "Varmgn"   /* Variance Margin           */
     "~011"
     "LYTDsls" /* LYTD Sales                */
     "~011"
     "LYTDmgn" /* LYTD Margin                */
     "~011"
     "Blog"    /* Backlog                    */
     "~011"
     "Blogmgn" /* Backlog Margin             */
     "~011"
     "Llstsale"
     "~011"
     "Lsttrans"
     chr(13) chr(10).


  repeat:
    readkey stream xprd.
    if lastkey = -2 then leave.
    if lastkey = -1 then next.
    if lastkey = 0 then
      put stream zprd control NULL.
    if lastkey = 13 then
      put stream zprd control chr(13) chr(10).
    else
      put stream zprd control chr(lastkey).

  end.    
input  stream xprd close.
output stream zprd close.
/*
os-command value("chmod 666 " +
  v-printdir + "tbxdxx" + ".smk").
os-command
value("cp " + 
       v-printdir + "tbxdxx" + ".smk" + " " +
       v-printdir + "tbxdxx" + ".smk.bk").       
os-command 
   value("rm " + 
         v-printdir + "tbxdxx" + ".smk").
*/
end.

/*
if v-process = true then
  do:
  input stream xprd from value
    (v-printdir + "tbxdxx" + ".sxk").
        

  assign j-x = 0.

  repeat:

    assign 
      v-region     = ""
      v-district   = ""
      v-pcat       = ""
      v-whse       = ""
      v-vend       = 0
      v-cust       = 0
      v-prod       = ""
      v-ytd        = 0
      v-lytd       = 0
      v-ytdxsls    = 0
      v-lytdxsls   = 0
      v-ytdxmgn    = 0
      v-lytdxmgn   = 0
      v-skip       = ""
      v-cust       = 0
      v-shipto     = ""
      v-lsttrans   = ""
      v-lstsale    = 0.
    if j-x = 0 then 
      do:
      import stream xprd delimiter "~011"
        v-skip.
      import stream xprd delimiter "~011"
        v-skip.
      j-x = 2.
      end.
    
    import stream xprd delimiter "~011"
     v-skip
     v-region
     v-district
     v-whse
     v-cust
     v-skip
     v-shipto
     v-pcat
     v-skip
     v-prod
     v-skip    /* Month 1 Sales             */
     v-skip    /* Month 1 Margin            */
     v-skip    /* Month 2 Sales             */
     v-skip    /* Month 2 Margin            */
     v-skip    /* Month 3 Sales             */
     v-skip    /* Month 3 Margin            */
     v-ytdxsls
     v-ytdxmgn  /* Current YTD margin        */
     v-lytdxsls /* Previous YTD Sales        */
     v-lytdxmgn /* Previous YTD Margin       */
     v-skip     /* Variance Sales            */
     v-skip     /* Variance Difference %     */
     v-skip     /* Variance Margin           */
     v-skip     /* LYTD Sales                */
     v-skip    /* LYTD Margin                */
     v-skip    /* Backlog                    */
     v-skip    /* Backlog Margin             */
     v-lstsale
     v-lsttrans.

   assign v-ytd  = v-ytdxsls - v-ytdxmgn
          v-lytd = v-lytdxsls - v-lytdxmgn.

   j-x = j-x + 1.
   if j-x < 3 or v-region = "final" then
     next.    /* Get past the Excel Header portion of the file */

      
   if v-region = "" or
      substring(v-region,1,1) = "0" or 
      substring(v-region,1,1) = "z" or
      substring(v-region,1,1) = "x" or 
      v-district = "n500" then
     next.
 
   if length(v-prod) > 25 then
     next.
   
   find spprd where
        spprd.whse   = v-whse and
        spprd.custno = v-cust and
        spprd.shipto = v-shipto and
        spprd.prod = v-prod and
        spprd.region = v-region and
        spprd.district = substring(v-district,2,3) no-lock no-error.
   if avail spprd then
     do:
     assign spprd.sales  = spprd.sales /* + v-ytd  */      + v-lytd
            spprd.xsales = spprd.xsales + /* v-ytdxsls + */  v-lytdxsls
            spprd.xmgn  = spprd.xmgn + /* v-ytdxmgn + */     v-lytdxmgn.

     end.
   else
     do:
     create spprd.
     assign spprd.custno = v-cust
            spprd.whse   = v-whse
            spprd.xused  = false
            spprd.shipto = v-shipto
            spprd.region = v-region
            spprd.prodcat = v-pcat
            spprd.vendno  = 0
            spprd.district = substring(v-district,2,3)
            spprd.prod = v-prod
            spprd.sales = /* v-ytd + */      v-lytd
            spprd.xsales = /* v-ytdxsls + */ v-lytdxsls
            spprd.xmgn = /* v-ytdxmgn +  */  v-lytdxmgn
            spprd.lstsale  = v-lstsale
            spprd.lsttrans = v-lsttrans
            spprd.xpct = 0.
     end.
     
   find t-spprd where
        t-spprd.whse = v-whse and
        t-spprd.prod = v-prod no-lock no-error.
   if avail t-spprd then
     do:
     assign t-spprd.sales = t-spprd.sales /* + v-ytd */ + v-lytd.
     end.
   else
     do:
     create t-spprd.
     assign
            t-spprd.prod = v-whse 
            t-spprd.prod  = v-prod
            t-spprd.sales = /* v-ytd +  */  v-lytd.
     end.
   end.   
   
   input stream xprd close.   
 
   end.

if v-process = true then
  do:
  output  stream xprd to 
     value (v-printdir + "tbxdprodsxx" + ".sxk").
  os-command value("chmod 666 " + 
                   v-printdir + "tbxdxx" + ".sxk").


  for each spprd no-lock:
    export stream xprd spprd.
  end.  
  
 output stream xprd close.
 end.

 if v-process = false and 
    search (v-printdir + "tbetrxx" + ".sxk") <> ? then
  do:
  input stream xprd from value
     (v-printdir + "tbetrxx" + ".sxk").
  repeat:
  create wuyers.
  import stream xprd wuyers.
  run createranges.
  end.
  input stream xprd close.
  end.
 else
 if v-process = false then
   do:
   v-process = true.
   input stream xprd from value
     (v-printdir + "tbxdprodsxx" + ".sxk").
   repeat:
     create spprd.
     import stream xprd spprd.
   end. 


   for each spprd no-lock:
     find t-spprd where
          t-spprd.whse = spprd.whse and
          t-spprd.prod = spprd.prod no-lock no-error.
     if avail t-spprd then
       do:
       assign t-spprd.sales = t-spprd.sales + spprd.sales.
       end.
     else
       do:
       create t-spprd.
       assign
              t-spprd.whse  = spprd.whse
              t-spprd.prod  = spprd.prod
              t-spprd.sales = spprd.sales.
       end.
   end.   
   end.
*/
 end.



