/*EN-2839*/ 
 def var lt-module like icet.module    no-undo.
 def var w-lt      like icsw.leadtmavg no-undo.
 for each LEAD:
   delete LEAD.
 end.
 assign lt-leadtm = 0
        lt-count  = 0   
        lt-module = " ".
if avail icsw then
 do:
 assign lt-module = if icsw.arptype = "V" then "po" else
                    if icsw.arptype = "W" then "wt" else
                    if icsw.arptype = "F" then "va" else
                    if icsw.arptype = "K" then "kp" else " ".
 if lt-module = " " then return.                    
 LEAD-LOOP:
 for each lt-icet where lt-icet.cono = g-cono      and
                        lt-icet.whse = icsw.whse   and
                        lt-icet.prod = icsw.prod   and
                        lt-icet.transtype = "re"   and   
                        lt-icet.module = lt-module and
                        lt-icet.postdt >= today - 183 and
                       (icsw.xxda4 = ? or
                        lt-icet.postdt > icsw.xxda4)
                        no-lock:
   if lt-count = 5 then leave LEAD-LOOP.
   if lt-icet.module = "po" then
     do:
     find lt-poeh where 
          lt-poeh.cono       = g-cono and
          lt-poeh.pono       = lt-icet.orderno and
          lt-poeh.posuf      = lt-icet.ordersuf and
          lt-poeh.vendno     = icsw.arpvendno and
          lt-poeh.ignoreltfl = no
          no-lock no-error.
     if avail lt-poeh then
       do:
       find LEAD where LEAD.powtkpva-nbr = lt-poeh.pono  and
                       LEAD.sufx         = lt-poeh.posuf and
                       LEAD.receiptdt    = lt-poeh.receiptdt
                       no-error.
       if avail LEAD then next LEAD-LOOP.  
       create LEAD.
       assign lt-count = lt-count + 1.
       assign 
          LEAD.powtkpva-nbr = lt-poeh.pono 
          LEAD.sufx         = lt-poeh.posuf
          LEAD.receiptdt    = lt-poeh.receiptdt
          LEAD.leadtime     = 
                            if (lt-poeh.receiptdt - lt-poeh.orderdt) < 1 then 
                              1
                            else 
                              (lt-poeh.receiptdt - lt-poeh.orderdt).   
     end. /* avail lt-poeh */           
   end. /* module = "po" */
   if lt-icet.module = "wt" then
     do:
     find lt-wteh where 
          lt-wteh.cono       = g-cono and
          lt-wteh.wtno       = lt-icet.orderno and
          lt-wteh.wtsuf      = lt-icet.ordersuf and
          lt-wteh.shiptowhse = icsw.whse and
          lt-wteh.shipfmwhse = icsw.arpwhse and
          lt-wteh.ignoreltfl = no
          no-lock no-error.
     if avail lt-wteh then
       do: 
       find LEAD where LEAD.powtkpva-nbr = lt-wteh.wtno  and
                       LEAD.sufx         = lt-wteh.wtsuf and
                       LEAD.receiptdt    = lt-wteh.receiptdt 
                       no-error.
       if avail LEAD then next LEAD-LOOP.
       create LEAD. 
       assign lt-count = lt-count + 1.
       assign 
         LEAD.powtkpva-nbr = lt-wteh.wtno
         LEAD.sufx         = lt-wteh.wtsuf
         LEAD.receiptdt    = lt-wteh.receiptdt
         LEAD.leadtime     = 
                if (lt-wteh.receiptdt - lt-wteh.orderdt) < 1 then 
                  1 
                else 
                  (lt-wteh.receiptdt - lt-wteh.orderdt).
     end. /* avail lt-wteh */
   end. /* module = "wt" */     
   if lt-icet.module = "kp" then
     do:
     find lt-kpet where lt-kpet.cono     = g-cono and
                        lt-kpet.shipprod = lt-icet.prod and
                        lt-kpet.whse     = lt-icet.whse and 
                        lt-kpet.wono     = lt-icet.orderno and
                        lt-kpet.wosuf    = lt-icet.ordersuf
                        no-lock no-error.  
     if avail lt-kpet then
       do:
       find LEAD where LEAD.powtkpva-nbr = lt-kpet.wono  and
                       LEAD.sufx           = lt-kpet.wosuf and
                       LEAD.receiptdt      = lt-icet.postdt
                       no-error.
       if avail LEAD then next LEAD-LOOP.
       create LEAD.   
       assign lt-count = lt-count + 1.
       assign 
          LEAD.powtkpva-nbr = lt-kpet.wono
          LEAD.sufx         = lt-kpet.wosuf
          LEAD.receiptdt    = lt-icet.postdt
          LEAD.leadtime     = 
                            if (lt-icet.postdt - lt-kpet.enterdt) < 1 then 1
                            else (lt-icet.postdt - lt-kpet.enterdt). 
     end. /* avail kpet */
   end. /* module = "kp" */
   if lt-icet.module = "va" then
     do:
     find lt-vaeh where lt-vaeh.cono      = g-cono and
                        lt-vaeh.shipprod  = lt-icet.prod and
                        lt-vaeh.whse      = lt-icet.whse and
                        lt-vaeh.vano      = lt-icet.orderno and
                        lt-vaeh.vasuf     = lt-icet.ordersuf
                        no-lock no-error.
     if avail lt-vaeh then
       do:
       find LEAD where LEAD.powtkpva-nbr = lt-vaeh.vano  and
                       LEAD.sufx         = lt-vaeh.vasuf and
                       LEAD.receiptdt    = lt-icet.postdt
                       no-error.
       if avail LEAD then next LEAD-LOOP.
       create LEAD.   
       assign lt-count = lt-count + 1.
       assign 
          LEAD.powtkpva-nbr = lt-vaeh.vano
          LEAD.sufx         = lt-vaeh.vasuf
          LEAD.receiptdt    = lt-icet.postdt
          LEAD.leadtime     = 
                            if (lt-icet.postdt - lt-vaeh.enterdt) < 1 then 
                              1
                            else 
                              (lt-icet.postdt - lt-vaeh.enterdt). 
     end. /* avail lt-vaeh */
   end. /* module = "va" */
 end. /* each lt-icet */
 assign lt-count  = 0
        lt-leadtm = 0.
 for each LEAD no-lock:
   assign lt-count  = lt-count + 1
          lt-leadtm = lt-leadtm + LEAD.leadtime.
 end.  
 find lt-icsw where lt-icsw.cono = icsw.cono and
                    lt-icsw.whse = icsw.whse and
                    lt-icsw.prod = icsw.prod and
                    lt-icsw.frozenltty <> 999
                    no-error.
 if avail lt-icsw then
   do:
   assign w-lt = if lt-count > 0 then
                   ROUND(lt-leadtm / lt-count,0)
                 else
                   lt-leadtm.
   if w-lt > lt-icsw.frozenltty and w-lt > 0 then
     do:
     assign lt-icsw.leadtmprio = lt-icsw.leadtmlast
            lt-icsw.priorltdt  = lt-icsw.lastltdt.
     assign lt-icsw.leadtmlast = lt-icsw.leadtmavg
            lt-icsw.lastltdt   = lt-icsw.avgltdt.
     assign lt-icsw.leadtmavg  = w-lt
            lt-icsw.avgltdt    = today.
   end.
 end. /* avail lt-icsw */
end. /* avail icsw */
 /*EN-2839*/    
 
