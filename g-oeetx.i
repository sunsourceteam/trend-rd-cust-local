/* SX 55
   g-oeetx.i 1.2 11/04/98 */
/*h*****************************************************************************
  INCLUDE      : g-oeetx.i
  DESCRIPTION  : variables used by oeetl - lines
  USED ONCE?   : no
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    08/01/91 mms;           Add Warehouse Manager functionality
    11/03/92 mms; TB#  8566 Add Rush functionality
    11/08/92 kmw; TB#  8625 Add product line/category pricing
    05/26/93 mtt; TB# 11003 Bid Prep Release project
    09/01/93 mwb; TB# 12807 Added sasc.icpartialfl for oe/wt/po.
    01/27/94 kmw; TB# 13570 Add SW user hooks
    09/18/95 wpp; TB# 15593 Change Cost Override option to a type.
    09/22/95 gdf; TB# 18812 7.0 Rebates Enhancement (C1a)  Added
        v-oevrebcalcty, v-oefrzrebty, and v-oerebty variable definitions.
    10/09/98 gfk; TB# 11961 9.0 OE Shipped Quantity Override Security.
        Added v-oeqtyshipty definition
    12/07/98 mtt; TB# 24049 PV: Common code use for GUI interface (mtt)
    05/06/99 jbs; TB# 19724 Changes for sm/gl cost split.
    05/13/99 kjb; TB# 19724 Moved definitions of v-icsmcost and v-icincaddgl
        inside the parameter 2 block so that they can be commented out with
        similar variables.
    10/31/00 kjb; TB# e4280 Add the ability to do customer pricing based upon
        the ICSW Rebate Type; added v-pdlevelrt and increased v-pdsecure extent
        from 44 to 50
    12/18/00 gp;  TB# e6350 Option to enter commission type on Non-Stock
    10/11/01 usb; TB# e10577 Toggle Margin/Cost Display - BMAT OE Usability Proj
    08/20/02 bm;  TB# e13643 Make promo window optional in OE
        Added v-promoprcdflt, v-promoprcwin
    08/27/02 bm;  TB# e12821 Add ability to turn off non-stock defaulting.
        Added v-useprevnsfl
    11/30/02 bm;  TB# t19491 Add ability to enter requested ship date
        and promised date at the line level for non-jit orders.
*******************************************************************************/

/*  Begin SMRZQ  */
def {1} var v-lpromcd  as char format "x(1)" no-undo.
def {1} var v-lpromdt as date no-undo.
def {1} var v-lexpdt  as date no-undo.

/*  End   SMRZQ  */




def {1} var v-sufl         as logical              no-undo.
def {1} var v-bpifl        as logical              no-undo.
def {1} var v-idoeeh0      as recid                no-undo.
def {1} var v-pdsecure     as logical extent 50    no-undo.
def {1} var v-termspct     like oeeh.termspct      no-undo.
def {1} var v-termslinefl  like oeeh.termslinefl   no-undo.

def {1} var v-jobno        like oeeh.jobno         no-undo.
def {1} var v-slsrepin     like oeeh.slsrepin      no-undo.
def {1} var v-slsrepout    like oeeh.slsrepout     no-undo.

def {1} var v-oetietype    like sasoo.oetietype    no-undo.
def {1} var v-oelineno     like sasoo.oelineno     no-undo.
def {1} var v-wtapprwhse   like sasoo.wtapprwhse   no-undo.
def {1} var v-catconvertfl like sasoo.catconvertfl no-undo.
def {1} var g-oeoptionfl   like sasoo.oeoptionfl   no-undo.
def {1} var v-oecostoverty like sasoo.oecostoverty no-undo.
def {1} var v-oeqtyshipty  like sasoo.oeqtyshipty  no-undo.
def {1} var g-oecostfl     like sasoo.seecostfl    no-undo.

def {1} var v-iccatstockfl like sasc.iccatstockfl  no-undo.
def {1} var v-oeforcefl    like sasc.oeforcefl     no-undo.
def {1} var v-oecostsale   like sasc.oecostsale    no-undo.
def {1} var v-oealtwhsefl  like sasc.oealtwhsefl   no-undo.
def {1} var v-oesplabel    like sasc.oesplabel     no-undo.
def {1} var v-csbofl       like sasc.csbofl        no-undo.
def {1} var v-pdlevelfl    like sasc.pdlevelfl     no-undo.
def {1} var v-pdlevelpl    like sasc.pdlevelpl     no-undo.
def {1} var v-pdlevelpc    like sasc.pdlevelpc     no-undo.
def {1} var v-pdlevelrt    like sasc.pdlevelrt     no-undo.
def {1} var v-pdjobfl      like sasc.pdjobfl       no-undo.
def {1} var v-pdpromofl    like sasc.pdpromofl     no-undo.
def {1} var v-oepofl       like sasc.oepofl        no-undo.
def {1} var v-oewtfl       like sasc.oewtfl        no-undo.
def {1} var v-icoptionfl   like sasc.icoptionfl    no-undo.
def {1} var v-iccosttype   like sasc.iccosttype    no-undo.
def {1} var v-icincaddcp   like sasc.icincaddcp    no-undo.
def {1} var v-iccstprdfl   like sasc.iccstprdfl    no-undo.
def {1} var v-oelinefl     like sasc.oelinefl      no-undo.
def {1} var v-wmsboefl     like sasc.wmsboefl      no-undo.
def {1} var v-wmpprimfl    like sasc.wmpprimfl     no-undo.
def {1} var v-wtleadtm     like sasc.wtleadtm      no-undo.
def {1} var v-smcustrebfl  like sasc.smcustrebfl   no-undo.
def {1} var v-icpartialfl  like sasc.icpartialfl   no-undo.

def {1} var v-modkpfl      like sasa.modkpfl       no-undo.
def {1} var v-modswfl      like sasa.modswfl       no-undo.

def {1} var v-pricetype    like arsc.pricetype     no-undo.
def {1} var v-pricecd      like arsc.pricecd       no-undo.
def {1} var v-disccd       like arsc.disccd        no-undo.
def {1} var v-custprodfl   like arsc.custprodfl    no-undo.
def {1} var v-salesterr    like arsc.salesterr     no-undo.

def {1} var v-reqshipdt    like oeeh.reqshipdt     no-undo.
def {1} var v-promisedt    like oeeh.promisedt     no-undo.

def {1} var v-icdatclabel  as c format "x(5)"      no-undo.
def {1} var v-icdatcty     like sasc.icdatcty      no-undo.
def {1} var v-ardatcty     like arsc.ardatcty      no-undo.
def {1} var v-ardatccost   like arsc.ardatccost    no-undo.
def {1} var v-icdatccost   like sasc.icdatccost    no-undo.

def {1} var v-bphistoryfl  like sasc.bphistoryfl   no-undo.

def {1} var v-oevrebcalcty like sasc.oevrebcalcty  no-undo.
def {1} var v-oefrzrebty   like sasc.oefrzrebty    no-undo.
def {1} var v-oerebty      like sasoo.oerebty      no-undo.

def {1} var v-lndtentfl    like oeao.lndtentfl     no-undo.
def {1} var v-promoprcdflt like oeao.promoprcdflt  no-undo.
def {1} var v-promoprcwin  like sasoo.promoprcwin  no-undo.
def {1} var v-useprevnsfl  like sasoo.useprevnsfl  no-undo.

{2}
def {1} var v-icsmcost     like sasc.icsmcost      no-undo.
def {1} var v-icincaddgl   like sasc.icincaddgl    no-undo.
def {1} var v-icglcost     like sasc.icglcost      no-undo.
def {1} var v-icincaddsm   like sasc.icincaddsm    no-undo.
def {1} var v-icfifofl     like sasc.icfifofl      no-undo.
def {1} var v-modwmfl      like sasa.modwmfl       no-undo.
def {1} var v-smvendrebfl  like sasc.smvendrebfl   no-undo.
def {1} var v-borelfl      like oeeh.borelfl       no-undo.
def {1} var v-nonstockfl   like sasoo.nonstockfl   no-undo.
def {1} var v-intercofl    like sasc.intercofl     no-undo.
def {1} var v-pocostfl     like sasc.pocostfl      no-undo.
def {1} var v-pdvenqtyfl   like sasc.pdvenqtyfl    no-undo.
def {1} var v-poqtyrcvfl   like sasc.poqtyrcvfl    no-undo.
def {1} var v-icnsprodcat  like sasc.icnsprodcat   no-undo.
def {1} var v-icsnpofl     like sasc.icsnpofl      no-undo.
def {1} var v-subfl        like arsc.subfl         no-undo.
def {1} var v-nscommfl     like sasoo.nscommfl     no-undo.
/{2}* */