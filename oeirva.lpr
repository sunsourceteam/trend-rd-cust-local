
/* oeirva.lpr 1.2 03/25/98 */
/*h****************************************************************************
  INCLUDE      : oeirva.lpr
  DESCRIPTION  : Order Entry Inquiry Credit Release - Approve Tied VA Orders
  USED ONCE?   : no
  AUTHOR       : mtt
  DATE WRITTEN : 07/01/97
  CHANGES MADE :
    12/19/96 bao; TB# 21254 (21c) Change vaeh.approvty when oeeh tied to va ord
    07/01/97 mtt; TB# 23133 Develop Credit Control Center Feature
    03/17/98 gp;  TB# 24593 Update approve field on VA order.
        Add stage edit.
******************************************************************************/

/*tb 21254-21c 12/19/96 bao; Determine if oeeh is tied to a
  vaeh record and if so, change the vaeh.approvty.    */

for each vaelo use-index k-order where
         vaelo.cono        = g-cono       and
         vaelo.ordertype   = "o"          and
         vaelo.orderaltno  = g-orderno    and
         vaelo.orderaltsuf = g-ordersuf
         no-lock:
    do for vaeh:
        {vaeh.gfi &vano  = vaelo.vano
                  &vasuf = vaelo.vasuf
                  &lock  = exclusive}
        if avail vaeh                  and
            /** sync up the va header to oe header regardless,
                if OE margin hold or Oe credit hold scenario **/ 
            vaeh.stagecd  < 5        /* and
            vaeh.approvty = o-approvty  and
            vaeh.openinit = ""       */
        then do:
            assign
                 vaeh.approvty = {&approvty}.
            {t-all.i vaeh}
        end.
    end. /* for exclusive-lock vaeh */
end. /* for each vaelo */
