/* oexpp */

/* message "running oexpp". pause.   */

define     shared temp-table xlines no-undo

  field orderno like oeeh.orderno
  field ordersuf like oeeh.ordersuf
  field shipfrom as character format "x(4)"
  field lineno like oeel.lineno
  field shipprod like oeel.shipprod
  field qtyord like oeel.qtyord
  field printq like oeel.qtyord
  field printl as logical
index 
  xf
  orderno
  ordersuf
  lineno.


{p-rptbeg.i}
{g-ic.i}
{g-oeet.i "shared"}
{g-oeeth.i "shared"}
{g-oeetx.i "shared"}
{g-oeepwm.i "new"}
{g-oeepp.i "new"}
{g-oee99.i}
{oerah.gva "new"}
{wlwhse.gva "new shared"}

def new shared var v-commitfl     as logical init yes     no-undo.
def new shared var v-nondocnt     as integer initial 0    no-undo.
def new shared var v-oepctfillty  like sasc.oepctfillty   no-undo.
def new shared var v-oepctfillqty like sasc.oepctfillqty  no-undo.
def new shared var s-origsuf      like oeel.ordersuf      no-undo.

/*tb 20406 02/01/96 mtt; Not using PDAO pricing by Whse */
{g-jump.i}

/*tb 23693 11/18/97  aa; variables fro new options: */
def var p-lblprinternm     like    sapb.printernm      no-undo.
def var p-lblprintopt      as      char                no-undo.

/*d Workfile to keep divisional balances for Div Acct; needs to be shared for
  back orders created with tender amounts transferred and only needed if
  oeepp runs stand alone */
/*tb 23507 09/25/98 cm; Use new include to define w-div */
{gldiv.gtt &new = "new shared"}

def var v-forward     like icsd.jitdays    init 0  no-undo.
def var v-odate       like oeeh.promisedt          no-undo.
def var v-sat         as i                 init 0  no-undo.
def var v-sun         as i                 init 0  no-undo.
def shared var        s-printernm  like sasp.printernm     no-undo.
hide message no-pause.
pause 0 before-hide.

/*o Load variables from SASA and SASC to carry across programs */
/*tb 19484 08/20/96 gp; Check if the WL module is purchased.  The
    include wlsasa.gas assigns v-modwlfl. */
/*tb 21702 09/13/96 kjb; Remove references to SAPB oper2 in the code.  Assign
    a null value to v-oper2 since sapb.oper2 was never loaded. */
assign
    v-oper2        = ""
    v-wmpprimfl    = if avail sasc then sasc.wmpprimfl else no
    v-wmsboefl     = if avail sasc then sasc.wmsboefl else yes
    v-oepctfillty  = if avail sasc then sasc.oepctfillty else "d"
    v-oepctfillqty = if avail sasc then sasc.oepctfillqty else 0
    {wlsasa.gas}.
run oeebti.p.

/*tb 20406 02/01/96 mtt; Not using PDAO pricing by Whse */
/*e Since this program can be run from other function(s) (POEI/WTEI), its
    possible that the AO/SASOO variables being set in oeebti.p are not
    the original new shared set from login time (lig.p).  Rather, the
    oeebti.p program could have been initializing another set of
    shared variables defined in the other function(s) running this
    program.  Want to force the g-oeloadedfl to be no, so if the operator
    accesses OEET, they will be forced to re-load the AO/SASOO variables.
    This was a problem at a user site where the OEET function was not
    doing the proper pricing by whse, since the v-pdwhsefl was not
    set properly (oeebti.p was not loading the log in variable, rather
    it was loading a variable defined as new shared in POEI). */

g-oeloadedfl = no.

/*o Load ranges and option values from SAPB */

/*d There is no option value 9 that needs to be loaded.  It is part of 10 */
assign b-orderno   = 1
       e-orderno   = 9999999
       b-custno    = 1
       e-custno    = 999999999999
       b-route     = " "
       e-route     = "```````"
       b-takenby   = " "
       e-takenby   = "````"
       b-shipviaty = " "
       e-shipviaty = "````"

       p-whse      = " "
       p-listfl    = yes
       p-days      = 0
       v-datein    = "**/**/**".
{p-rptdt.i}

find oeeh where recid(oeeh) = v-idoeeh no-lock no-error.
find sasp where sasp.printernm = s-printernm
  no-lock no-error.
/*tb #6764 11/30/00 lbr; Added p-printdropshipfl */
assign
    p-promisedt     = v-dateout
    o-promisedt     = p-promisedt
    p-rushfl        = no
    p-reprintfl     = yes 
    p-sortty        = "o"
    p-commitfl      = no
    p-packty        = "b"
    p-lblprinternm  = " "
    p-lblprintopt   = " "
    p-printdropshipfl = yes.


    /*tb#e2785 01/27/00 mjg; Demand print not working correctly in GUI */          /*d Assign random report name */                                            
     {p-rptnm.i v-reportnm " "}                                     
                                                                         
 
     create sapb.                                                                
 /*tb 21702-6 09/12/96 kr; Remove sapb.oper2 from Trend */                   
     assign                                                                          sapb.currproc   = "oexpp"
      sapb.cono       = g-cono
      sapb.reportnm   = v-reportnm
      sapb.printoptfl = no
      sapb.priority   = 9
      sapb.printernm  = s-printernm.                                       
    find first
         sassr where sassr.currproc = sapb.currproc no-lock no-error.
    
    
    assign v-sapbid = recid(sapb)
           v-sassrid = recid(sassr)
           v-saspid = recid(sasp).
                                                                                    /*d Create the SAPBO list for the order to be processed */
    create sapbo.
    assign sapbo.orderno   = oeeh.orderno
           sapbo.ordersuf  = oeeh.ordersuf
           sapbo.reportnm  = v-reportnm
           sapbo.cono      = g-cono
           sapbo.outputty  = if avail sasp      /* TB# 1730 */
                               and sasp.ptype = "f" then "f" else "p"
           sapbo.reprintfl = if (oeeh.printpckfl = yes and oeeh.stagecd > 1)
                                  then yes else no
           sapbo.route     = "".
                                                 
    {t-all.i sapbo}
                 

/*o Print the pick ticket based on the format requested */
/*tb 21702 09/13/96 kjb; Remove references to SAPBO, SAPB oper2 in the code */
if  can-find(first b-sapbo use-index k-sapbo where
                   b-sapbo.cono     = g-cono  and
                   b-sapbo.reportnm = sapb.reportnm)
then do:
 /*   message "are we here?". pause.    */
    run rptctl2.p("oexpp" + string(sasc.oeprtfrmt[2],"9") + ".p",
                  yes,
                  0,
                  yes,
                  0,
                  yes).

end.


hide message no-pause.


