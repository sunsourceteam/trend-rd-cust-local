def input  parameter ip-email like sapb.user3 no-undo.
def output parameter ip-errfl as logical      no-undo.

def stream i-chk.
def var v-length    as integer no-undo.
def var v-entries   as integer no-undo.
def var v-index     as integer no-undo.
def var v-mailer    as char    no-undo.
def var v-email     as char    no-undo.
def var v-emailaddr as char    no-undo.



assign v-mailer = right-trim(ip-email).
assign v-length = length(v-mailer).
assign v-mailer = replace(v-mailer," ",";").
assign v-entries = num-entries(v-mailer,";").

do v-index = 1 to v-entries:
  assign v-email = entry(v-index,v-mailer,";").
  repeat:
    input stream i-chk through  
      value("grep " + v-email + " /etc/aliases | cut -d: -f2").
    import  stream i-chk unformatted v-emailaddr no-error.
    leave.
  end.
  input  stream i-chk close. 

  if right-trim(left-trim(v-emailaddr)) <> right-trim(left-trim(v-email)) 
  then do:
    assign ip-errfl = true.
    message "Invalid internal e-mail address - " + v-email.
/*    pause. */
    return.
  end.
  else
   assign ip-errfl = false.
end. 

