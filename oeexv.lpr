put screen row 21 col 3
    color message
 "          F7-EDIT PO    F8-Add/Copy Line     F9-Delete Line               ".

if s-lineno <> int(frame-value) and avail edih then
  do:
  assign s-lineno = int(frame-value). 
  find edil where edil.cono = g-cono + 500 and
                  edil.batchnm = g-batchnm and
                  edil.seqno   = g-seqno and  
                  edil.lineno  = s-lineno
                  no-lock no-error.
  if avail edil then
    assign s-price     = edil.price
           s-promisedt = edil.xxda1
           s-reqshipdt = edil.xxda2.
end.
assign g-lineno = s-lineno.

main:
do while true on endkey undo, leave:
  update s-shipto
         s-qtyord
         s-price
         s-promisedt
         s-reqshipdt
         with frame f-oeexvl.
                             
  /*EN-2727_Fix*/
  if frame-field = "s-shipto" and
    can-do("9,13,306,307,308,309,310,401,501,502,503,504",string(lastkey))
    then
    do:                       
    if input s-shipto = "" then
      do:
      message "ShipTo is Required". 
      next-prompt s-shipto with frame f-oeexvl.
      next.
    end.                      
    find first arss where arss.cono = g-cono and
                          arss.custno >= 13307161 and
                          arss.custno <= 13307174 and
                          arss.shipto  = input s-shipto
                          no-lock no-error.
    if not avail arss then
      do:
      message "ShipTo Not Found". 
      next-prompt s-shipto with frame f-oeexvl.
      next.
    end. /* not avail arss */   
  end. /* shipto was entered */
  /**/                             
  if lastkey = 401 or
     frame-field = "s-reqshipdt" and lastkey = 32 then
    do:
    assign linefoundfl = no.
    for each b-edil where b-edil.cono = g-cono + 500 and
                          b-edil.batchnm = g-batchnm and
                          b-edil.seqno   = g-seqno and
                          b-edil.lineno  = s-lineno:
      assign x-recid      = recid(b-edil).
      assign b-edil.price  = input s-price
             b-edil.xxda1  = input s-promisedt
             b-edil.xxda2  = input s-reqshipdt.
      if s-shipto <> "" then
        do:
        oeehloop1:
        for each oeeh where oeeh.cono       = g-cono and
                            oeeh.custpo     = g-po and    
                            oeeh.transtype <> "BL" and  
                            oeeh.shipto     = s-shipto and
                           (oeeh.enterdt  >= TODAY - 730 or
                           (oeeh.transtype = "BR" and oeeh.stagecd = 1 and
                            oeeh.enterdt  >= TODAY - 1460)) and
                            oeeh.custno    >= 13307161 and
                            oeeh.custno    <= 13307174
                            no-lock:
          find first oeel where oeel.cono          = g-cono and
                                oeel.orderno       = oeeh.orderno and
                                oeel.ordersuf      = oeeh.ordersuf and
                                oeel.reqprod       = b-edil.reqprod and
                                oeel.qtyord        = s-qtyord and
                                oeel.price         = b-edil.price and
                    date(substr(oeel.user3,1,10))  = b-edil.xxda1 and
                    date(substr(oeel.user3,11,10)) = b-edil.xxda2 and
                                oeel.specnstype   <> "L"
                                no-lock no-error.
          if not avail oeel then
            do:
            find first oeel where oeel.cono          = g-cono and
                                oeel.orderno       = oeeh.orderno and
                                oeel.ordersuf      = oeeh.ordersuf and
                                oeel.qtyord        = s-qtyord and
                                oeel.price         = b-edil.price and
                    date(substr(oeel.user3,1,10))  = b-edil.xxda1 and
                    date(substr(oeel.user3,11,10)) = b-edil.xxda2 and
                                oeel.specnstype   <> "L"
                                no-lock no-error.

          end. /* not avail oeel */
          if avail oeel then
            do:
            assign g-orderno  = oeeh.orderno
                   g-ordersuf = oeeh.ordersuf.
                 /*  s-qtyord   = oeel.qtyord.  */
            run V-oeepie1.p.
            find b2-edil where b2-edil.cono    = g-cono + 500 and
                               b2-edil.batchnm = g-batchnm and
                               b2-edil.seqno   = g-seqno and
                               b2-edil.lineno  = s-lineno  
                               no-error.
            if avail b2-edil and b2-edil.xxc15 = "UPDATED" then
              do:
              assign s-complete = "*"
                     linefoundfl = yes
                     b2-edil.qtyord = s-qtyord.
              display s-complete 
                      s-qtyord
                      with frame f-oeexvl.

              /* Update oeeh.refer with the Vermeer Order Revision/PO info */
              for each b-oeeh where b-oeeh.cono     = g-cono and
                                    b-oeeh.orderno  = oeeh.orderno and
                                    b-oeeh.ordersuf = oeeh.ordersuf and   
                                    b-oeeh.shipto   = oeeh.shipto and
                                    b-oeeh.custpo   = g-po:
                find edih where edih.cono    = b2-edil.cono and
                                edih.batchnm = b2-edil.batchnm and
                                edih.seqno   = b2-edil.seqno
                                no-lock no-error.
                if avail edih then
                  assign b-oeeh.refer = substr(edih.user1,58,24).
              end.
              /**********************/
              if (v-length = frame-line or frame-line = 1) then do:
                if v-length = frame-line then
                  v-recid[v-length] = v-recid[v-length - 1].
                if frame-line = 1 then v-recid[1] = v-recid[2].
              end.
              /**********************/
              leave oeehloop1.
            end. /* avail b2-edil */
          end. /* avail oeel */
        end. /* each oeeh */        
      end. /* s-shipto <> "" */
      else
        do: 
        oeehloop2:
        for each oeeh where oeeh.cono       = g-cono and
                            oeeh.custpo     = g-po and    
                            oeeh.shipto     = s-shipto and
                            oeeh.transtype <> "BL" and
                           (oeeh.enterdt  >= TODAY - 730 or
                           (oeeh.transtype = "BR" and oeeh.stagecd = 1 and
                            oeeh.enterdt  >= TODAY - 1460)) and
                            oeeh.custno    >= 13307161 and
                            oeeh.custno    <= 13307174
                            no-lock:
          find first oeel where oeel.cono          = g-cono and
                                oeel.orderno       = oeeh.orderno and
                                oeel.ordersuf      = oeeh.ordersuf and
                                oeel.reqprod       = b-edil.reqprod and
                                oeel.qtyord        = s-qtyord and
                                oeel.price         = b-edil.price and
                    date(substr(oeel.user3,1,10))  = b-edil.xxda1 and
                    date(substr(oeel.user3,11,10)) = b-edil.xxda2 and
                                oeel.specnstype   <> "L"
                                no-lock no-error.
          if not avail oeel then
            do:
            find first oeel where oeel.cono          = g-cono and
                                oeel.orderno       = oeeh.orderno and
                                oeel.ordersuf      = oeeh.ordersuf and
                                oeel.qtyord        = s-qtyord and
                                oeel.price         = b-edil.price and
                    date(substr(oeel.user3,1,10))  = b-edil.xxda1 and
                    date(substr(oeel.user3,11,10)) = b-edil.xxda2 and
                                oeel.specnstype   <> "L"
                                no-lock no-error.

          end. /* not avail oeel */
          if avail oeel then
            do:
            assign g-orderno  = oeeh.orderno
                   g-ordersuf = oeeh.ordersuf
                   s-qtyord   = oeel.qtyord.
            run V-oeepie1.p.
            find b2-edil where b2-edil.cono    = g-cono + 500 and
                               b2-edil.batchnm = g-batchnm and
                               b2-edil.seqno   = g-seqno and
                               b2-edil.lineno  = s-lineno  
                               no-error.
            if avail b2-edil and b2-edil.xxc15 = "UPDATED" then
              do:
              assign s-complete = "*"
                     linefoundfl = yes
                     b2-edil.qtyord = s-qtyord.
              display s-complete 
                      s-qtyord
                      with frame f-oeexvl.

              /* Update oeeh.refer with the Vermeer Order Revision/PO info */
              for each b-oeeh where b-oeeh.cono     = g-cono and
                                    b-oeeh.orderno  = oeeh.orderno and
                                    b-oeeh.ordersuf = oeeh.ordersuf and   
                                    b-oeeh.shipto   = oeeh.shipto and
                                    b-oeeh.custpo   = g-po:
                find edih where edih.cono    = b2-edil.cono and
                                edih.batchnm = b2-edil.batchnm and
                                edih.seqno   = b2-edil.seqno
                                no-lock no-error.
                if avail edih then
                  assign b-oeeh.refer = substr(edih.user1,58,24).
              end.
              /**********************/
              if (v-length = frame-line or frame-line = 1) then do:
                if v-length = frame-line then
                  v-recid[v-length] = v-recid[v-length - 1].
                if frame-line = 1 then v-recid[1] = v-recid[2].
              end.
              /**********************/
              leave oeehloop2.
            end. /* avail b2-edil */
          end. /* avail oeel */
        end. /* each oeeh */        
      end. /* else ship-to exists */
    end. /* each b-edil */
    if linefoundfl = no then    
      do:
      message
"OE Line not found. ShipTo, Prod, Price, Qty, PromDt and Exp/ShipDt must match".
      next-prompt s-lineno with frame f-oeexvl.
      next.
    end. /* line not found */
  end. /* lastkey */  
  
  if lastkey = 401 then
    leave main.
end. /* do while */
