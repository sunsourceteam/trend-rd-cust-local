/*h*****************************************************************************
  INCLUDE      : p-oeetl.i
  DESCRIPTION  : OE line item entry
  USED ONCE?   : Yes (oeetl.p
  AUTHOR       : kmw
  DATE WRITTEN :
  CHANGES MADE :
    01/01/90 mwb; TB#   848
    02/06/91 mwb; TB#  1878 Back order on types s and t
    02/08/91 mwb; TB#  1769 Default to credit
    02/13/91 mwb; TB#  1345 Back order on type CS - not suspended
    05/20/91 mwb; TB#  3065 Cannot delete BL line if BRs in entered stage
    06/20/91 mwb; TB#  3520 Qtyship of 0 and pickup on the way
    07/31/91 kmw;           For kit enhancements; require option/keyword
    08/09/91 mwb; TB#  4178 Set to no for serial/lot products
    08/16/91 kmw;           Added "do while true"s around each pause
    09/06/91 mwb; TB#  3483 Error for 0 cost on non-stock
    09/25/91 mwb;           Don't allow cost, price, disc to be entered
    10/10/91 kmw;           Clear for a wo; give not msg
    10/09/91 mwb; TB#  4660 Delete must have 5 security
    11/01/91 mms;           Added v-oepricefl logic
    11/20/91 mms; TB#  5057 JIT, made error run common logic
    04/08/92 mjq; TB#  6222 Line DO - changed to s-botype from g-oetype
    06/09/92 mjq; TB#  5948 Line DO - Don't allow ties to open PO/WT
    06/30/92 mjq; TB#  7125 If NS in OE whse and stocked and speccost in WT
        whse - don't allow
    09/09/92 mms; TB#  7796 Allow vendor return tie set in po to remain
    11/08/92 kmw; TB#  8625 Add product line/category pricing
    11/21/92 mms; TB#  8566 Rush - Add datccost update logic, rushdesc
    12/04/92 mwb; TB#  9012 g-all.i variables pulled in, split code (oeetlmar.p)
    12/21/92 mwb; TB#  9012 Added oeebtlma.p and curproc edit for batch.
    12/31/92 mwb; TB#  6657 Removed the error 5805 for back order edits
    02/16/93 kmw; TB#  5460 Line level security based on header security
    02/19/93 kmw; TB#  5995 Qty/Unit default for customer product
    03/03/93 kmw; TB# 10233 Must manually adjust PO/WT - no message
    03/03/93 kmw; TB# 10244 If cancel pressed from a message, don't cancel trans
    03/03/93 kmw; TB#  9421 Progress error **no wteh record avail
    03/08/93 kmw; TB# 10299 If tie removed, give message
    03/17/93 kmw; TB#  8212 Return window at line does not display
    04/22/93 kmw; TB# 10713 Press spacebar to continue on non-stock
    04/24/93 rhl; TB# 11076 Define OE Line globals at login
    05/04/93 kmw; TB# 10785 Catalog DO product
    05/08/93 kmw; TB# 11271 Performance with display of previous lines
    05/17/93 kmw; TB# 11370 Progress error received w/tie
    05/18/93 kmw; TB# 11473 Performance - call of margin calc
    05/24/93 kmw; TB# 11526 Performance- Eliminate snlots.p call
    07/15/93 mwb; TB# 12173 Edits for delete of drop ship lines to PO/WT stages
    08/09/93 mwb; TB# 12497 Allow debit (CR) on drop ship lines
    09/07/93 mms; TB# 12619 Only allow transfer tie if on DO COD
    09/27/93 mwb; TB# 12861 Stop 'cs' drop ship line from BO edits
    10/26/93 mwb; TB# 13428 Allow comments in OE Batch.
    11/01/93 mwb; TB# 13462 Edit do not reorder to not allow BO.
    11/22/93 kmw; TB# 13739 No b-oeeh record available
    12/01/93 mwb; TB# 13771 PO/WT print message inconsistant.
    04/07/94 mwb; TB# 14575 Comments not deleting when line deletes
    09/27/94 gp;  TB# 16659 Del/canc DO lines tied to a WT
    04/07/95 tdd; TB# 16540 DO variables not retained
    04/24/95 tdd; TB# 16448 CR credit allowed when Post Credits = no
    04/25/95 tdd; TB# 15500 Can cancel OE tied to shipped WT DO
    05/03/95 kjb; TB# 16664 Drop ship line tie check for cancel/delete
    05/11/95 dww; TB#  6023 Add "Require Physical Count" popup to Counter Sales
        Orders when the Qty Ship has changed
    06/30/95 mtt; TB# 18611 Make changes for KP Work Order Demand (Y1)
    07/17/95 gp;  TB# 18611 Make changes for KP Work Order Demand (C3)
    07/24/95 gp;  TB# 18611 Make changes for KP Work Order Demand (C3)
        Added "for kpet" prior to kp-edit.
    08/25/95 dww; TB# 19052 OEELK's deleted if in Chg Mode & cancel
    09/25/95 mcd; TB# 19329 Added User Include
        {p-oeetl.z99  &user_afterupdt = "*"}
    09/26/95 tdd; TB# 16861 PO/WT tie prevents Lost Business
    02/06/96 tdd; TB# 19638 Cancelling a back ordered DO line
    02/08/96 kr;  TB#  2103 Code clean up
    03/18/96 mwb; TB# 18982 Carry g-wono globally.
    08/29/96 jms; TB# 21254 (17A) Develop Value Add Module
    09/10/96 mtt; TB# 21254 (17C) Develop Value Add Module
    10/17/96 tdd; TB# 20813 Can't cancel OEBO when qty on PO changed
    11/21/96 kr;  TB# 22186 Using wrong variable in condition statement
    12/09/96 rh;  TB# 22204 Added user hook (&user_defvars)
    02/19/97 mtt; TB# 22607 Allow different Whse for VA order
    03/03/97 mms; TB#  7241 (5.1) Spec8.0 Special price costing
        speccost.gva must be new shared in order to carry information to header
        totaling routines originated from this program; added b2-icss buffer.
    05/05/97 rh;  TB# 22710 Add user include &user_afterframe
    07/29/97 gp;  TB# 23574 Need to access TWL order for changes.  Add
        wlwhse.gva and s-wlwhsechgfl.
    03/16/98 mtt; TB# 24049 PV: Common code use for GUI interface
    03/16/98 bp ; TB# 24701 Added user hook &user_aftvarinit
    04/22/98 kjb; TB# 24806 ICSW in use Progress error when accessing the same
        product in VAET
    06/11/98 sbr; TB# 24982 Order pad, F8, not all lines added
    08/24/98 bm;  TB# 24812 Add Rush Functionality type "IN" Lines.
        Added rushfl as 2nd last param when run vaetcret
    10/05/98 kjb; TB#  9731 Add a second nonstock description.
    12/28/98 bp ; TB# 25701 Add user include &bef_linedel
    03/10/99 gp;  TB# 25971 Add lock status to oeel
    04/14/99 kmo; TB# 26065 added user hook &user_001
    04/30/99 sbr; TB# 26154 Add user hook &user_delete to p-oeetl.i
    06/02/99 mwb; TB# 26253 Added Tendering to Batch Processing
    09/15/99 mjg; TB# t15256 Limit line number.
    10/16/99 bp ; TB# 26575 Add variables to permit rebate calculations with
        the SPC information from the line.
    10/31/00 pcs; TB# 6947  Add user hook &user_aftlotchk
    10/31/00 kjb; TB# e4280 Add the ability to do customer pricing based upon
        the ICSW Rebate Type; added v-pdlevelrt
    02/12/01 lcg; TB# e7852 AR Multicurrency changes
    05/05/01 bp ; TB# e7192 Add additional user hook.
    05/10/01 bp ; TB# e7192 Add user hooks.
    06/12/01 lcg; TB# e9298 Currencyty under OE Sales banner should display
        OE currencty when in Chg mode
    06/17/01 ajw; TB# e9298 Fixed compile issue in Batch with recent change.
    08/22/01 gp;  TB# e9923 Stocked BOD kit entered without components
    08/28/01 mt;  TB# e10078  ; OE usability project phase 1.
    09/19/01 usb; TB# e10576  ; Add Price Origin Code
    10/11/01 usb; TB# e10577 Ext Ln Format Margin entry - BMAT OE Usability Proj
    10/24/01 usb; TB# e10578 OEET Ext Ln Format Totals - BMAT OE Usability Proj
    10/30/01 usb; tb# e10577 margin% format reduced from 4 decimal places to 2
    11/08/01 mt;  TB# e10078  ; OE Usability - Correct initials & "u's"
    01/08/02 blr; TB# e11627 OE Usability - Changed call to p-oeetlz.i
         removed the quotes around &com.
    01/09/02 dls; TB# e11633 Added oeelc records for subtotal lines
    02/01/02 tmg; TB# e11171 Change wtbod kit from order entry
    03/04/02 doc; TB# e12167 Calculate Rebates on VA Orders and Prebuilt Kit
        Components, when tied to an OE order (Roll from TB# e8223)
    03/05/02 doc; TB# e12218 Calculate Pricing from VA Tie/Arp to OE
        (Roll of TB# e8225).
    04/03/02 dls; TB# e13043 Fixed error with no b-oeel record on int/ext/sub lines
    04/24/02 des; TB# e12903 OE Usability: BMat Project Updates
    04/25/02 mwb; TB# e8868 Added edit to prevent the deletion of line comments
        for lost business lines.
    02/24/02 spo; TB# e16268 Added criteria to allow some changes to TWL orders
    05/13/03 doc; TB# e17219 Added Value Add nonstock markup processing
    09/10/03 blr; TB# t11498 Cross Reference Functionality for Kit Components -
        added new sasse message when unable to ship qty order for kit product
        that has components
    11/04/03 cm;  TB# e18958 Force ship partial kit BO hangs SX
    11/11/03 ns;  TB# e17893 override ao serial setting at the icsw level
    12/17/03 cm;  TB# e13515 Source DNR/Superceded products from alternate whse
    01/21/04 bp ; TB# e19059 Add user hooks.
    01/28/04 cm;  TB# e11091 VA tie not created from OE.  Block tie to existing
        VA order.
    07/07/04 bp ; TB# e11708 Require authorization to cancel/delete a line
        with ties.
    07/08/04 cm;  TB# e5438 Force Serial/Lot for components
    07/19/04 bp ; TB# e11708 CR changes.
    08/12/04 bp ; TB# e20941 Add other tie types to e11708 processing.
    01/27/05 sbr; TB# e21382 New event for cancel/delete tied lines/ords
    12/23/08 emc; MSF 110894 F11 on BOD WT doesn't put components back on WO
*******************************************************************************/
/*e************************** special calls for custom mods ********************
  oeetl99.p  call - "bt" - beginning of transation state
                    "el" - end of line item after updates done
  oeetla99.p call - "lv" - from oeetla for level pricing
  oeetlu99.p call - "pe" - stocked product entered and all variables set
                    "pr" - product check after ICSP and cross ref not found.
                    "pc" - after price and cost set (p-oeetlm.i)
                    "in" - after all screen values are initialized (p-oeetlh.i)
                    "au" - after oeetlu run (screen updating done)
  oeetlf99.p call - if F10/F8 pressed on blank product and v-oelabel ne ""
  oeetlt99.p call - "bt" - beginning of transaction state
                    "et" - end of transaction state
*******************************************************************************/

{g-oe.i}
{g-oeetlo.i "new shared"}
{g-oeetlr.i "new shared"}
{g-oeetlx.i "new shared"}
{g-oeetl.i  "new shared" {&com}}
{g-oepowt.i "new"}
{g-oedopw.i}
{g-oeetcr.i "new"}
{speccost.gva "new shared"}
{g-ic.i}

/*tb e7852 07/28/00 lcg; A/R Multicurrency global variables */
{arexchg.gva}

/*tb 26575 10/16/99 bp ; Add rebate SPC variables.*/
{rspcvars.gva "new shared"}

/*tb 18982 03/18/96 mwb; 'new' was clearing g-wono after each line added.  *
    the g-kp.i include is not even needed. Loaded 'new' in lig.p,          *
    rptmgr.p, and other KP functions                                       *
{g-kp.i     "new"}                                                         *
****************************************************************************/

{g-margin.i}

{wlwhse.gva "shared"}
def     shared var s-wlwhsechgfl as logical                            no-undo.

def            var v-deletecomfl as logical                            no-undo.

/* from {g-all.i} */
def     shared var g-add         as logical                            no-undo.
def     shared var g-cono        as integer format ">>>9"              no-undo.
def     shared var g-operinit    as c format "x(4)"                    no-undo.
def     shared var g-ourproc     like sassm.currproc                   no-undo.
def     shared var g-secure      as i format "9"                       no-undo.

{c-oecust.i}

/*d LEAVE "i" AND "j" IN YOUR CODE IF YOU PULL THIS INCLUDE IN; THEY CAN BE
    MISTAKEN FOR FIELDS AND YOU MAY NOT SEE A COMPILE ERROR */
def            var i            as int                                 no-undo.
def            var j            as int                                 no-undo.
def            var confirm      as logical                             no-undo.
def            var v-authpick   as log format "yes/no"                 no-undo.
def            var v-authreqfl  as log format "yes/no"                 no-undo.
/* from {g-oeet.i} */
def     shared var g-oephonefl   like sasoo.oephonefl                  no-undo.
def     shared var g-oeoptionfl  like sasoo.oeoptionfl                 no-undo.
def     shared var g-oecostfl    like sasoo.seecostfl                  no-undo.
def     shared var v-lastkey     as i                                  no-undo.
def     shared var s-lostbusty   as c format "x(2)"                    no-undo.
def     shared var v-idoeeh      as recid                              no-undo.
def     shared var v-credpostfl  like sasoo.credpostfl                 no-undo.
def     shared var v-jobno       like oeeh.jobno                       no-undo.
def     shared var v-icsnpofl    like sasc.icsnpofl                    no-undo.
def     shared var v-nontaxtype  like arsc.nontaxtype                  no-undo.
def     shared var v-oelineno    like sasoo.oelineno                   no-undo.
def     shared var v-oecostsale  like sasc.oecostsale                  no-undo.
def     shared var v-oeendfl     as logical                            no-undo.
def     shared var v-oeforcefl   like sasc.oeforcefl                   no-undo.
def     shared var v-oeinterfl   as logical                            no-undo.
def     shared var v-oespfl      as logical                            no-undo.
def     shared var v-oesplabel   like sasc.oesplabel                   no-undo.
def     shared var v-oepricefl   like sasoo.oepricefl                  no-undo.
def     shared var v-oeextlfl    as logical                            no-undo.
def     shared var v-orderdisp   like oeeh.orderdisp                   no-undo.
def     shared var v-pdlevelfl   like sasc.pdlevelfl                   no-undo.
def     shared var v-pdlevelpl   like sasc.pdlevelpl                   no-undo.
def     shared var v-pdlevelpc   like sasc.pdlevelpc                   no-undo.
def     shared var v-pdlevelrt   like sasc.pdlevelrt                   no-undo.
def     shared var v-pdpromofl   like sasc.pdpromofl                   no-undo.
def     shared var v-slsrepin    like oeeh.slsrepin                    no-undo.
def     shared var v-slsrepout   like oeeh.slsrepout                   no-undo.
def     shared var v-icoptionfl  like sasc.icoptionfl                  no-undo.
def     shared var v-csbofl      like sasc.csbofl                      no-undo.
def     shared var v-bofl        as c format "x(1)"                    no-undo.
def     shared var v-termsoverfl like sasoo.termsoverfl                no-undo.

def     shared var s-crreasonty  like oeeh.crreasonty                  no-undo.
def     shared var s-lumpbillfl  like oeeh.lumpbillfl                  no-undo.
def     shared var s-lumppricefl like oeeh.lumppricefl                 no-undo.

def new shared var v-firstwarnfl as logical      init no               no-undo.
def new shared var v-datcauth    as logical      init no               no-undo.
def new shared var v-onlyfl      as logical      init no               no-undo.
def new shared var v-lastlevel   as integer                            no-undo.
def new shared var v-qtybo       like icsw.qtybo                       no-undo.
def new shared var v-qtyonhand   like icsw.qtyonhand                   no-undo.
def new shared var v-qtyreservd  like icsw.qtyreservd                  no-undo.
def new shared var v-qtycommit   like icsw.qtycommit                   no-undo.
def new shared var v-qtyonorder  like icsw.qtyonorder                  no-undo.
def new shared var v-countfl     as log     init no                    no-undo.
def new shared var g-vanotemp    like vaeh.vano                        no-undo.
def new shared var g-vasuftemp   like vaeh.vasuf                       no-undo.
def new shared var lSuperWhseUseFl as log        init no               no-undo.
def            var v-display     as logical                            no-undo.
def            var v-nexttype    as c format "x" init ""               no-undo.
def            var v-noassn      like oeel.nosnlots                    no-undo.
def            var v-totlineamt  like oeeh.totlineamt                  no-undo.
def            var v-errorno     as i                                  no-undo.
def            var o-rmordertype like oeel.ordertype                   no-undo.
def            var v-rebtiefl    as logi                               no-undo.
def            var v-whsesnpofl  as logical                            no-undo.

/*d Variables for Cross Reference Checks for Kit Components */
def new shared var v-xreferrfl   as logi                               no-undo.

/*tb 22607 02/19/97 mtt; Allow different Whse for VA order */
def var o-ordertype              like oeel.ordertype                   no-undo.
def var o-whse                   like icsd.whse                        no-undo.
{g-wt.i}

/*tb 24982 06/11/98 sbr; Added orderpad.gtt and orderpad.gva */
{orderpad.gtt}
{orderpad.gva}

{g-secure.i}
{varollprc.gva "new shared"}

def buffer b2-icsw               for icsw.  
    
/* EATON PREMIUM */
def buffer b3-icsw               for icsw.
def buffer b3-oeel               for oeel.
def var var-eaton     as logical no-undo.
def var o-optprod     as char    no-undo.
def var var-kitfl     as logical no-undo.
/* def shared var var-eatonline    like oeel.lineno    no-undo.   */



def buffer b2-icss               for icss.
def new shared frame f-oeetl.
def new shared frame f-oeetlt.
def new shared frame f-oeetltx.

/*e Add user include*/
{p-oeetl.z99 &user_afterframe = "*"}

/*e User Hook */
{p-oeetl.z99 &user_defvars = "*"}

{f-oeetl.i}.
{f-oeetlt.i}

{varollprc.gfo}

/*d extended line format has Margin instead of Discount - chg col heading */
if v-oeextlfl then
    s-discamt:label in frame f-oeetl = "Margin".

view frame f-oeetl.
down 4 with frame f-oeetl.

pause 0 before-hide.

s-returnfl = if can-do("cr,rm",g-oetype) then yes else no.

/*o Display previous lines on screen */
find b-oeeh where recid(b-oeeh) = v-idoeeh no-lock no-error.

/*tb e7852 07/28/00 lcg; A/R Multicurrency assign exchange rate */
/*e Get the ARSC file since b-oeeh not the same FILE depending if
    called by oeetl.p or oeebtl.p (= oeehb file in the latter) */
{w-arsc.i b-oeeh.custno no-lock}
{&com}
v-exchrate = b-oeeh.salesexrate.
/{&com}* */

{&find}

/*d oeetli.p/oeebtli.p  - OE line item entry - display of entered lines in
                     transaction area */
{&com}
if b-oeeh.nextlineno > 1 then
/{&com}*   */
    if g-ourproc <> "oeebt" then run oeetli.p.
    else run oeebtli.p.

assign
    v-nextlineno = g-oelineno
    v-display    = yes
    {&lineno}
    g-ourproc    = if g-ourproc = "oeebt" then "oeebt" else "oeetl".

/*o Begin line entry */
main:
do for b-icsp, b-icsw, b-oeel while true with frame f-oeetl
        on endkey undo main, next main
        on error  undo main, next main:

    /*tb 6023 05/11/95 dww; Add "Require Physical Count" popup on CS orders */
    v-countfl = no.

    updt:
    do while true with frame f-oeetl on endkey undo updt, leave updt
                                     on error  undo updt, leave updt:

        if v-errfl = "z" then
            find b-oeeh where recid(b-oeeh) = v-idoeeh no-lock no-error.

        hide frame del no-pause.

        /*d Display available function keys */
        if g-ourproc <> "oeebt" then
            put screen row 21 column 3 color messages
 " F6-Header      F7-Extend      F8-Addon/Tot   F9-Tendering   F10-Options    ".

        else put screen row 21 column 3 color messages
 " F6-Header      F7-Extend      F8-Addons      F9-Tendering                  ".

        if avail b-oeeh and g-ourproc <> "oeebt" and v-exchrate ne 1
        then put screen color message row 4 column 54
                        " CURRENCY: "
                        {&com} + caps(b-oeeh.currencyty) /{&com}* */ +
                        " ".

        /*o Initialize all shared variables */
        if not v-func then do:

            {&com3}
            /*d Initialize variables for oeetl line items */
            {p-oeetlh.i}
            
            /{&com3}* */

            assign
                g-vanotemp  = 0
                g-vasuftemp = 0.

             /*tb 24982 06/11/98 sbr; Added code to reset g-orderpadfl to "yes"
                if records still exist within the temp table t-orderpad. */
            find first t-orderpad no-lock no-error.
            if avail t-orderpad and g-orderpadfl = no then
                g-orderpadfl = yes.


            /*d OE line item entry - initialize variables */
            if g-ourproc <> "oeebt" then
                run oeetlh.p.

            /* User Include */
            {p-oeetl.z99 &user_aftvarinit = "*"}

            /*tb t15256 09/15/99 mjg Limit line number */
            /*d New Line Not Allowed; Max Line Number on Orders is 998 */
            if v-nextlineno > {&MAX_OE_LINE} then do:
                run err.p(6522).
            end.

            display
                s-commentfl
                s-notesfl
                s-returnfl
                s-proddesc
                s-taxablety
                s-rushdesc.

            if v-oepricefl <> "n" then
                display
                    s-price
                    s-discamt       when v-oeextlfl = no
                    s-disctype      when v-oeextlfl = no
                    s-netamt
                    s-qtybreakty
                    s-speccost.

            /* extended line format has margin% & price origin code */
            if v-oeextlfl then
                display
                    s-marginpct     format "zzz9.99-"
                                    when g-oecostfl
                                        @ s-discamt
                    "%"             when g-oecostfl
                                        @ s-disctype
                    s-priceorigcd.

        end. /* if not v-func */

        /*d Process optional products if they exist for the last product
            entered */
       
        {&com}
        
        if v-icoptionfl and g-oeoptionfl and not v-func and v-optprod <> ""
        then do:

    
/* Eaton Premium */

        find first b3-oeel where b3-oeel.cono = g-cono and            
                                 b3-oeel.orderno = g-orderno and
                                 b3-oeel.ordersuf = g-ordersuf and
                                 b3-oeel.lineno = (v-nextlineno - 1) and    
                                 b3-oeel.shipprod = v-optprod
                                 no-lock no-error.

         
        if avail b3-oeel then do:
         
         /* COMMENT OUT KITS 
          
         if b3-oeel.kitfl = yes then do:    
          
             
             assign var-kitfl = yes.
            
            for each oeelk where oeelk.cono = g-cono and
                              oeelk.ordertype = "o" and
                              oeelk.comptype = "C" and
                              oeelk.orderno = b3-oeel.orderno and
                              oeelk.ordersuf = b3-oeel.ordersuf and
                              oeelk.lineno = b3-oeel.lineno
                              no-lock:
                              
            
            find first b3-icsw where b3-icsw.cono = g-cono and 
                                 b3-icsw.prod = oeelk.shipprod and
                                 b3-icsw.whse = oeelk.whse /* and
                                 b3-icsw.arpvendno = oeelk.arpvendno */
                                 no-lock no-error.
                           
            if avail b3-icsw then do:
              
             
             find first pdsr use-index k-contractno where pdsr.cono = g-cono and
                                        pdsr.vendno = oeelk.arpvendno  and
                                       (pdsr.enddt >= today or 
                                        pdsr.enddt = ?)
                                        no-lock no-error.
                                 
              if avail pdsr then do:
                 
                   
                 find first notes where notes.cono = g-cono and
                            notes.notestype = "zz" and
                            notes.primarykey = "eaton-premium" and
                            notes.secondarykey = b3-icsw.pricetype 
                            no-lock no-error.

                  if avail notes then do: 

                      assign var-eaton = yes 
                             o-optprod = v-optprod
                             v-optprod = "EATON PREMIUM PRODUCT".
                  end.   /* avail notes */
                         
                  /*
                  else                                            
                  if not avail notes then do:
                     /* message "no notes". pause.  */
                     next.
                  end.
                  */    
                end. /* avail pdsr */
                                
                /*
                else 
                
                if not avail pdsr then do:
                   next. 
                end. /* not avail pdsr */
                */   

            end. /* b3-icsw */
           end. /* for each  */

       
       if can-find(first icsec use-index k-icsec where
                              icsec.cono    = g-cono and
                              icsec.rectype = "o"    and
                              icsec.prod    = v-optprod)   
            then do:

                run oeetlo.p.
                
                if var-eaton then do:
                   assign var-eaton = no 
                          v-optprod = "".
                end.
                
   
         end.  /* added */
      
       
       end.   /* kits */
       
       else  
       
       FINISHED KITS COMMENT OUT */
       
       
       
       
       if b3-oeel.kitfl = no then do:    

            
            find first b3-icsw where b3-icsw.cono = g-cono and 
                                 b3-icsw.prod = v-optprod and
                                 b3-icsw.whse = g-whse /* and
                                 b3-icsw.arpvendno = b-oeel.vendno  */
                                 no-lock no-error.
                           
            if avail b3-icsw then do:
              
             
             /*
             find first pdsr use-index k-contractno where pdsr.cono = g-cono and
                                        pdsr.vendno = b3-icsw.arpvendno  and
                                        pdsr.custno = b3-oeel.custno and
                                       (pdsr.enddt >= today or 
                                        pdsr.enddt = ?)
                                        no-lock no-error.
                                 
                if avail pdsr then do:
                  message "avail pdsr and rec"  pdsr.rebrecno
                  "vend" b3-icsw.arpvendno "cust" b3-oeel.custno. pause.
             */

        
                 find first pder where pder.cono = g-cono and
                                      pder.orderno = b3-oeel.orderno and
                                      pder.ordersuf = b3-oeel.ordersuf and
                                      pder.lineno = b3-oeel.lineno
                                      no-lock no-error.
                 if avail pder then do:                     

                 find first notes where notes.cono = g-cono and
                            notes.notestype = "zz" and
                            notes.primarykey = "eaton-premium" and
                            notes.secondarykey = b3-icsw.pricetype 
                            no-lock no-error.

                  if avail notes then do:   
                   
                   assign var-eaton = yes 
                          o-optprod = v-optprod
                          v-optprod = "EATON PREMIUM PRODUCT".
                  end. /* notes */

                  /*
                  else                                            
                  if not avail notes then do:
                     if g-operinit begins "das" or 
                        g-operinit = "smaf" then
                       do:
                       message "caught in a loop 1". pause.
                     end.
                     message "no notes". pause.  
                     next.
                  end.
                  */
                end. /* avail pdsr */
                
                /*                   
                else 
                if not avail pdsr then do:
                   /* message "not avail pdsr". pause.  */
                   if g-operinit begins "das" or
                      g-operinit = "smaf" then
                     do:
                     message "caught in a loop 2". pause.
                   end.
                   next.
                end. /* not avail pdsr */
                */
                
           end.  /*  b3-icsw */
           
            if can-find(first icsec use-index k-icsec where
                              icsec.cono    = g-cono and
                              icsec.rectype = "o"    and
                              icsec.prod    = v-optprod)   
            then do:

                /*d OE line items - optional products screen */
                
                run oeetlo.p.
                
                if var-eaton then do:

                   assign var-eaton = no 
                          v-optprod = "".
                end.
                
       
       end. /* not kit */      
       
       end. /* b3-oeel */
       
       else 
         do:
         run oeetlo. 
         end.

                 
      /*d OE line item entry - display of entered lines in
                
        transaction area */
     
                run oeetli.p.

                v-optprod = "".

                /*d Oeetl end */
                /*d 01/08/02 - TB# e11627 - removed quotes*/

                {p-oeetlz.i {&com}}

                next main.

            end. /* if can-find(icsec) */

            v-optprod = "".

        end. /* if v-icoptionfl = yes and .... */

        /{&com}* */

        {p-oeetl.z99 &bef_oeetlu = "*"}

        /*o Update fields for the line item */
        if g-ourproc <> "oeebt" then run oeetlu.p.
        else run oeebtlu.p.

        /*u User Custom Run */
        if v-oeinterfl then run oeetlu99.p("au").

        /*d If F19 pressed, allow comment entry/maintenance */
        if {k-func19.i} then do:

            if s-prod = "" then next updt.

            /*d Comments entry */
            run com.p("oe",g-orderno,g-ordersuf,s-lineno, output v-commentfl).

            assign
                s-commentfl = if v-commentfl then "c" else ""
                v-func      = yes
                v-donefl    = no.

            display s-commentfl.

            next-prompt s-qtyord.
            next updt.

        end. /* if k-func19.i */

        /*d Set variables for a return line */
        /*d 22186 11/21/96 kr; Using wrong variable in condition statement,
            replaced s-crreasonty with s-returnty */
        if can-do("rm,cr,cs,so",g-oetype) and s-returnfl and
            s-returnty <> "v" then
            assign
                s-botype     = "n"
                s-ordertype  = ""
                s-orderaltno = 0.

        status input.

        /*d Check for invalid field entry on a core return */
        if v-corechgfl and g-ourproc <> "oeebt" then do:

            if s-price <> 0  or s-discamt <> 0 or s-disctype <> no or
                s-unit <> "EACH"
            then do:

                /*d Price, Disc, Disc Type, and Unit Can't be Changed on a
                    Core Chg Return */
                run err.p(5788).

                v-func = yes.

                next updt.

            end. /* if s-price <> 0 or .... */

        end. /* if v-corechgfl and .... */

        /*d Force the operator to press GO other screens (like com, serial,
            lot, kit...) have been accessed */
        if {k-cancel.i} or {k-jump.i} or {k-func13.i} then

            if v-maint-l = "c" and not v-donefl then do:

                /*d You Must Press <GO> on This Line Item */
                run err.p(5837).

                v-func = yes.

                next updt.

            end. /* if v-maint-l = c and .... */

            else undo updt, leave updt.
        else if {k-cancel.i} or {k-jump.i} or {k-func13.i} then
                undo updt, leave updt.

        /*o If F17 pressed, process a return */
         else if  not can-do("e,i,t":u,s-specnstype) and
                (({k-func17.i} and
                 (v-maint-l = "a" or (v-maint-l = "c" and s-returnfl = yes))) or
                 (v-maint-l = "a" and g-oetype = "rm" and
                  v-corechgfl = no and s-prod <> "" and
                ({k-accept.i} or {k-func6.i} or {k-func8.i} or {k-func9.i} or
                ({k-return.i} ))))
        then do:

            pause 0 before-hide.

            v-errfl = "".

            /*tb 23574 08/05/97 gp; Need to access TWL order for changes */
            if v-wlwhsefl     = yes and
                s-wlwhsechgfl = no then
            do:
                run err.p(6555).
                s-prod = "".
                v-func = yes.
                next updt.
            end. /* end if v-wlwhsefl ... */

            if not {c-chan.i} and v-onlyfl = no then do:

                /*d oeetl17.p/oeebtl17.p - oe line item entry - Return
                    processing whse f17 key Pressed */
                if g-ourproc <> "oeebt" then run oeetl17.p.
                else                         run oeebtl17.p.

                /*e User Hook */
                {p-oeetl.z99 &aft_func17 = "*"}

                g-ourproc = if g-ourproc = "oeebt" then "oeebt" else "oeetl".

            end. /* if not c-chan.i and v-onlyfl = no */

            else v-func = yes.

            if v-errfl = "s" or v-errfl = "z" then do:

                /*d Oeetl end */
                /*d TB# e11627 01/08/02 Removed Quotes */
                {p-oeetlz.i {&com}}

                if v-errfl = "z" then undo updt, next updt.
                else next updt.

            end. /* if v-errfl = s or v-errfl = z */

            else next updt.

        end. /* if ({k-func17.i} ... */

        else if {k-func17.i} and v-maint-l = "c" and s-returnfl ne yes then do:

            /*d Cannot Enter the Return Screen (F17) on a Sales Line */
            run err.p(6088).

            v-func = yes.

            next updt.

        end. /* else if k-func17.i and .... */

        if s-prod = "" then do:

            /*tb 15256 10/15/99 mjg; if line > MAX leave */
            if s-lineno > {&MAX_OE_LINE} then leave main.

            if {k-func7.i}           then next updt.
            else if {k-delete.i}     then next updt.
            else if not {k-func10.i} then leave main.

        end. /* if s-prod = "" */

        /*d Calculate the nosnlots for product and kits */
        if s-prod <> "" then do:

            /*d Check to see if SN/Lot check needs to be done */
            {p-snlots.i}

            /*u User Hook*/
            {p-oeetl.z99 &user_aftlotchk = "*"}

            /*o Delete the line if requested */
            if {k-delete.i} and v-maint-l = "a" then do:

                do for b2-icsw:
                    {w-icsw.i s-prod s-whse no-lock b2-}
                    if available b2-icsw then
                        v-whsesnpofl={icsnpo.gcn &file=b2-icsw
                                                 &aosnpofl=v-icsnpofl}.
                    else v-whsesnpofl = v-icsnpofl.
                end.

                /*d Remove related files (COM, ICSES, ICETL, OEELK) if cancel
                    or delete during oe line item entry, icet, or wt shipping */
                run oeetlz.p("o",
                             g-orderno,
                             g-ordersuf,
                             yes,
                             s-returnfl,

                             yes,
                             yes,
                             (if s-returnfl = no and v-whsesnpofl = no then true
                             else false),
                             "").

                s-prod = "".

                next updt.

            end. /* if k-delete.i and v-maint-l = a */

            else if {k-delete.i} then do:

                /*tb 26154 04/30/99 sbr; Added user hook &user_delete */
                /*u User Hook */
                {oeetl.z99 &user_delete = "*"}

                if ((g-secure    < 4) or (g-secure < 5 and
                    ((b-oeeh.stagecd >= 2) and
                    ((b-oeeh.orderdisp <> "j") or
                     (b-oeeh.orderdisp =  "j" and avail
                      b-oeel and b-oeel.jitpickfl)))))
                then do:

                    /*d Security Violation - Delete Not Allowed */
                    run err.p(1109).

                    s-prod = "".

                    next updt.

                end. /* if g-secure < 4 or .... */

                {&com}

                /*d Require Authorization of a tied line.  This check
                    is not required in batch because the tie has not
                    been created.*/
                if avail b-oeel and
                   b-oeel.ordertype  ne '' and
                   b-oeel.orderaltno >  0  and
                   b-oeel.statustype =  'a':u
                then do:

                    v-authreqfl = no.

                    if s-ordertype = 'p':u then do for poel:

                        find last poel use-index k-poel where
                                  poel.cono       =  g-cono       and
                                  poel.pono       =  s-orderaltno and
                                  poel.lineno     =  v-linealtno  and
                                  poel.statustype ne 'c':u
                        no-lock no-error.

                        if avail poel then do for poeh:

                            {w-poeh.i poel.pono poel.posuf no-lock}

                            if avail poeh           and
                                poeh.stagecd ge 2   and
                                poeh.stagecd ne 9
                            then
                                v-authreqfl = yes.

                        end. /*if avail poel*/

                    end. /* if s-ordertype = 'p' */
                    else if s-ordertype = 't':u then do for wtel:

                        find last wtel use-index k-wtel where
                                  wtel.wtno       =  s-orderaltno and
                                  wtel.lineno     =  v-linealtno  and
                                  wtel.statustype ne 'c':u        and
                                  wtel.cono       =  g-cono
                        no-lock no-error.

                        if avail wtel then do for wteh:

                            {w-wteh.i wtel.wtno wtel.wtsuf no-lock}

                            if avail wteh               and
                                wteh.stagecd ge 2 and
                                wteh.stagecd ne 9
                            then v-authreqfl = yes.

                            /*d Also check to see if the WTEL is tied
                                to a PO in printed stage.*/
                            if avail wteh              and
                                v-authreqfl    = no    and
                                wtel.ordertype = 'p'   and
                                wtel.linealtno > 0
                            then do for poel:

                                find last poel use-index k-poel where
                                      poel.cono       =  g-cono          and
                                      poel.pono       =  wtel.orderaltno and
                                      poel.lineno     =  wtel.linealtno  and
                                      poel.statustype ne 'c':u
                                no-lock no-error.

                                if avail poel then do for poeh:

                                    {w-poeh.i poel.pono poel.posuf no-lock}

                                    if avail poeh           and
                                        poeh.stagecd ge 2   and
                                        poeh.stagecd ne 9
                                    then
                                        v-authreqfl = yes.

                                end. /*if avail poel*/

                            end. /*if avail wteh*/

                        end. /*if avail wtel*/

                    end. /*if v-authreqfl*/
                    else if s-ordertype = "f":u then do for vaeh:

                        find last vaeh use-index k-vaeh where
                            vaeh.vano   = s-orderaltno and
                            vaeh.cono   = g-cono
                        no-lock no-error.

                        /*d If the VAEH has not been printed and is
                            not cancelled, allow the delete.*/
                        if avail vaeh           and
                            vaeh.stagecd  ge 3  and
                            vaeh.stagecd  <  9
                        then v-authreqfl = yes.

                    end. /* if oeel.ordertype = "f" */
                    else if s-ordertype = 'w':u then do for kpet:

                        find last kpet use-index k-kpet where
                                kpet.cono = g-cono and
                                kpet.wono = s-orderaltno
                        no-lock no-error.

                        /*d If the work order has not been printed and is
                            not cancelled, allow the delete.*/
                        if avail kpet           and
                            kpet.stagecd  ge 2  and
                            kpet.stagecd  <  9
                        then v-authreqfl = yes.

                    end. /*else if oeel.ordertype */

                    if v-authreqfl = yes then do:

                        v-authpick = no.

                        run authchk.p (g-cono,
                                       g-operinit,
                                       'oeet':U,
                                       'lines':U,
                                       'ordertype':U,
                                       '':u,
                                       '':u,
                                       output v-authpick).

                        if v-authpick = no then do:

                            /*d Authorizaton Violation - Delete Not Allowed */
                            run err.p(1187).

                            s-prod = "".

                            next updt.

                       end. /*if v-authpick*/

                    end. /*if v-authreqfl*/

                end. /*if oeel.ordertype*/

                /{&com}* */

                /*tb 23574 07/29/97 gp; Need to access TWL order for changes */
                if v-wlwhsefl     = yes and
                    s-wlwhsechgfl = no  then
                do:
                    run err.p(6555).
                    s-prod = "".
                    next updt.
                end. /* end if v-wlwhsefl ... */

                /*tb 19638 02/06/96 tdd; Cancelling a back ordered DO line */
                if s-botype = "d" and s-orderaltno <> 0
                    {&com}
                    and avail b-oeel
                    /{&com}* */
                then do:

                    v-errfl = "".

                    /*tb 16664 05/03/95 kjb; Drop ship line tie check for
                        cancel/delete.  Error not given if OE suffix doesn't
                        match PO suffix */
                    if s-ordertype = "p" then do for poel:

                        find last poel use-index k-poel where
                                  poel.cono   = g-cono       and
                                  poel.pono   = s-orderaltno and
                                  poel.lineno = v-linealtno
                        no-lock no-error.

                        if avail poel then do for poeh:

                            {w-poeh.i poel.pono poel.posuf no-lock}

                            /*tb 16861 09/26/95 tdd; PO/WT tie prevents Lost
                                Business */
                            /*tb 20813 10/17/96 tdd; Do not allow line to be
                                set to lost business if the PO has been
                                received and the OE line has a qty ship */
                            if (avail poeh and
                                (poeh.stagecd <= 4 or poeh.stagecd = 9))

                                {&com}
                                or
                                (b-oeel.bono ne 0 and b-oeel.stkqtyship = 0)
                                /{&com}* */

                                then i = i.

                            else if
                                {&com}
                                b-oeel.stkqtyship <> 0 or
                                /{&com}* */

                                (avail poeh and poeh.stagecd >= 5 and
                                 poel.botype = "y" and
                                 poel.stkqtyord > poel.stkqtyrcv and
                                 poel.stkqtyrcv <> 0) then

                                 v-errfl = "y".

                        end. /* if avail poel */

                    end. /* if s-ordertype = p */

                    /*tb 16664 05/03/95 kjb; Drop ship line tie check for
                        cancel/delete.  Error not given if OE suffix doesn't
                        match WT suffix.  This fix takes the place of the fix
                        for TB# 15500. */
                    /*e The WT error checking is different from the PO error
                        checking.  The wteh.stagecd does not need to be checked
                        because the wtel.statustype is set to "s" when the WT
                        is shipped.  The statustype is "a" only for stages 0,
                        1 and 2. */
                    if s-ordertype = "t" then do for wtel:

                        find last wtel use-index k-wtel where
                                  wtel.wtno   = s-orderaltno and
                                  wtel.lineno = v-linealtno  and
                                  wtel.cono   = g-cono
                        no-lock no-error.

                        /*tb 16861 09/26/95 tdd; PO/WT tie prevents Lost
                            Business */
                        /*tb 20813 10/17/96 tdd; Do not allow line to be
                            set to lost business if the WT has been
                            received and the OE line has a qty ship */
                        if avail wtel then do for wteh:

                            {w-wteh.i wtel.wtno wtel.wtsuf no-lock}

                            if (avail wteh and
                                (wteh.stagecd <= 5 or wteh.stagecd = 9))

                                {&com}
                                or
                                (b-oeel.bono ne 0 and b-oeel.stkqtyship = 0)
                                /{&com}* */

                                then i = i.

                            else if
                                {&com}
                                b-oeel.stkqtyship <> 0 or
                                /{&com}* */

                                (avail wteh and wteh.stagecd >= 6 and
                                 wtel.bofl = yes and
                                 wtel.stkqtyord > wtel.stkqtyrcv and
                                 wtel.stkqtyrcv <> 0) then

                                v-errfl = "y".

                        end. /* if avail wtel */

                    end. /* if s-ordertype = t */

                    if v-errfl = "y" then do:

                        assign
                            v-errfl = "n"
                            s-prod  = "".

                        /*d Cannot Delete/Cancel; Tied to WT/PO That Has Been                             Shipped/Received */
                        run err.p(6319).

                        next updt.

                    end. /* if v-errfl = y */

                end. /* if s-botype = "d" */

                /* User Include */
                {p-oeetl.z99 &bef_linedel = "*"}

                confirm = no.

                pause 0 before-hide.

                update
                    confirm label "Delete?" auto-return
                with frame del side-labels row 1 column 67 overlay.

                 /*tb 26065 4/14/99 kmo; added user hook to p-oeetl.i*/
                {p-oeetl.z99 &user_001 = "*"}

                hide frame del.
                pause before-hide.

                if confirm then do:

                    /*tb 25971 03/10/99 gp; Add lock status to oeel*/
                    if g-oetype = "bl" then do:

                        for each oeeh use-index k-oeeh no-lock where

                                 oeeh.cono     = g-cono    and
                                 oeeh.orderno  = g-orderno and
                                 oeeh.ordersuf > 0         and
                                 oeeh.stagecd  = 0,

                            first oeel use-index k-oeel no-lock where
                                  oeel.cono     = g-cono        and
                                  oeel.orderno  = g-orderno     and
                                  oeel.ordersuf = oeeh.ordersuf and
                                  oeel.lineno   = s-lineno      and
                                  oeel.qtyord  <> 0:

                            /*d Finds stage 0 BR's with a qty released
                                off of the BL **/
                            run err.p(6021).

                            s-prod = "".

                            next updt.

                        end. /* for each oeeh */

                    end. /* if g-oetype = "bl" */

                    v-maint-l = "d".

                    /*tb # 10078 08/28/01 mt; add logic for oeelc lines */
                    {&com}
                    if can-do("e,i,t":u,s-specnstype) then
                    do:
                        {w-oeelc.i
                             g-orderno
                             g-ordersuf
                             s-lineno
                             s-specnstype
                             integer(substring(s-rushdesc,3,2))
                         exclusive-lock}

                        if avail oeelc then
                            delete oeelc.

                        if s-specnstype = "t":u then do:

                            {w-oeel.i g-orderno
                                      g-ordersuf
                                      s-lineno
                                      exclusive-lock b-}
                            if avail b-oeel then
                                b-oeel.subtotalfl = false.

                        end. /* s-specnstype = "t":u */

                    end.
                    else
                    /{&com}* */
                        /*d oeetld.p/oeebtld.p - OE delete line items */
                        if g-ourproc <> "oeebt":u then run oeetld.p(yes,no).
                        else run oeebtld.p(yes,no).

                    {p-oeetl.z99 &after_delete = "*"}

                    /*d Oeelt end */
                    /*d TB# e11627 01/08/02 Removed Quotes */
                    {p-oeetlz.i {&com}}

                    /*d calc & display line totals for extended line format */
                    if v-oeextlfl then
                        run oeetltx.p(input yes).   /* yes - display */

                    next updt.

                end. /* if confirm */

                else do:

                    find b-oeeh where recid(b-oeeh) = v-idoeeh no-lock no-error.

                    /*d Oeetl end */
                    /* TB# e11627 01/08/02 Removed Quotes */
                    {p-oeetlz.i {&com}}

                    next updt.

                end. /* else do */

            end. /* if {k-delete.i} */

            /*tb 24049 03/16/98 mtt; PV: Common code use for GUI interface */
            {oeetlau.led &returnfl        = "s-returnfl"
                         &returnty        = "s-returnty"
                         &disctype        = "s-disctype"
                         &discamt         = "s-discamt"
                         &price           = "s-price"
                         &unit            = "s-unit"
                         &specnstype      = "s-specnstype"
                         &ordertype       = "s-ordertype"
                         &orderaltno      = "s-orderaltno"
                         &vendno          = "s-vendno"
                         &vvendno         = "s-vvendno"
                         &powtintfl       = "v-powtintfl"
                         &arpwhse         = "s-whse"
                         &wwhse           = "s-wwhse"
                         &botype          = "s-botype"
                         &qtyship         = "s-qtyship"
                         &scrnprodcost    = "s-prodcost"
                         &corechgfl       = "v-corechgfl"
                         &orgstkqtyship   = "o-stkqtyship"
                         &stkqtyship      = "v-stkqtyship"
                         &retorderno      = "s-retorderno"
                         &retordersuf     = "s-retordersuf"
                         &retlineno       = "s-retlineno"
                         &qtyunavail      = "s-qtyunavail"
                         &warrexchgfl     = "s-warrexchgfl"
                         &pressedfunc7    = "~{k-func7.i~}"
                         &pressedfunc10   = "~{k-func10.i~}"}

        end. /* if s-prod not = "" */

        /*o Functions keys; if function key or CS and Serial/Lot or NS-Kit */
        assign
            v-firstforce  = if g-ourproc = "oeebt":u then no
                            else v-firstforce
            v-firstforcek = if g-ourproc = "oeebt":u then no
                            else v-firstforcek.

        /*tb# 10078 08/28/01 mt; added check for "standard" oeel line */
        if not can-do("e,i,t":u,s-specnstype) and
           {k-func.i}                                                 or
           (g-oetype = "cs" and v-firstcsfl and s-qtyship < s-qtyord) or
           (v-nosnlots < 0)                                           or
           (v-nosnlotsk < 0)                                          or
           (v-wmqtyassn < 0)                                          or
           (v-wmqtyassnk < 0)                                         or
           (g-oetype = "cs" and not can-do("n,l",s-specnstype)
                            and v-nosnlots > 0)                       or
           (g-oetype = "cs" and not can-do("n,l",s-specnstype)
                              and v-wmqtyassn > 0)                      or
           (k-reqfl)                                                  or
           (v-firstforce = yes and not can-do("n,l",s-specnstype) and
                            not (can-do("st,fo,bl,qu,do,cr,ra",g-oetype)
                                 or s-botype = "d")
                            and v-nosnlots > 0 and v-oeforcefl)       or
           (g-oetype = "cs" and v-nosnlotsk > 0)                      or
           (g-oetype = "cs" and v-wmqtyassnk > 0)                     or
           (v-firstforcek = yes and
                            not (can-do("st,fo,bl,qu,do,cr,ra",g-oetype)
                                 or s-botype = "d")
                            and v-nosnlotsk > 0 and v-oeforcefl)      or
           (((s-kitfl = yes and v-bodtransferty <> "t":u) or
             (s-kitfl = yes and v-bodtransferty  = "t":u and
            (v-maint-l = "a":u or v-wtboderrfl = no)))
            and not {v-oeelk.i "o" g-orderno g-ordersuf s-lineno "0" "/*"})
        then do:

            if not {k-func6.i} and not {k-func8.i} and not {k-func9.i} then do:

                /*d oeetlfu.p/oeebtlfu.p - oe line item entry - function keys                     in oeetl */
                if g-ourproc <> "oeebt" then run oeetlfu.p(output v-nexttype).
                else run oeebtlfu.p(output v-nexttype).

                if v-nexttype = "l" then do:

                    assign
                        g-oelineno = v-nextlineno
                        g-ourproc  = if g-ourproc = "oeebt" then "oeebt"
                                     else "oeet".

                    hide frame f-oeetl no-pause.
                    /*d hide totals for extended line format */
                    if v-oeextlfl then
                        hide frame f-oeetltx no-pause.
                    else
                        /*d hide regular line totals */
                        hide frame f-oeetlt no-pause.

                    return. /* on order suspend */

                end. /* if v-nexttype = l */

                else if v-nexttype = "m" then next main.
                else if v-nexttype = "u" then next updt.

                /*d End of function key operation */
                {p-oeetlf.i}

            end. /* if not k-func6.i and .... */

        end. /* if {k-func.i} */

        /*tb 24049 03/19/98 mtt; PV: Common code use for GUI interface */
        /*tb 10078 08/28/01 mt; only run include code if "standard" oeel */
        if not can-do("e,i,t":u,s-specnstype) then
        do:
            {oeetlafk.led &botype          = "s-botype"
                          &qtyship         = "s-qtyship"
                          &returnfl        = "s-returnfl"
                          &firsttimefl     = "v-firsttime"
                          &specnstype      = "s-specnstype"
                          &firstwarnfl     = "v-firstwarnfl"
                          &marginprog      = "oeetlmar.p"}
        end.

        {&com}

        /*tb 24049 03/19/98 mtt; PV: Common code use for GUI interface */
        /*tb 10078 08/28/01 mt; only run include code if "standard" oeel */
        if not can-do("e,i,t":u,s-specnstype) then
        do:
            {oeetltie.led &ordertype       = "s-ordertype"
                          &orderaltno      = "s-orderaltno"
                          &maint-l         = "v-maint-l"
                          &botype          = "s-botype"
                          &specnstype      = "s-specnstype"
                          &prod            = "s-prod"
                          &icspecrecno     = "v-icspecrecno"
                          &wcono           = "s-wcono"
                          &wwhse           = "s-wwhse"
                          &bodtransferty   = "v-bodtransferty"}
        end.

        /{&com}* */

        v-onlyfl = no.

        leave updt.

    end. /* updt: */

    /*u User Hook */
    {p-oeetl.z99 &user_afterupdt = "*"}

    /*tb 19052 08/25/95 dww; OEELK's deleted if in Chg Mode & cancel */
    /*tb 10078 08/28/01 mt; only if "standard" oeel record */
    /*o If cancel is pressed, delete records and cancel the line */
    if ({k-cancel.i} or {k-func13.i} or {k-jump.i} or s-specnstype = "l") and
       v-maint-l = "a" and not can-do("e,i,t":u,s-specnstype)
    then do:
        /* Keep comment if lost business and line exists */
        v-deletecomfl = if s-specnstype = "l":u and
                            not {k-cancel.i} and
                            not {k-func13.i} and
                            not {k-jump.i} then no
                         else yes.

        do for b2-icsw:
            {w-icsw.i s-prod s-whse no-lock b2-}
            if available b2-icsw then
                v-whsesnpofl={icsnpo.gcn &file=b2-icsw &aosnpofl=v-icsnpofl}.
            else v-whsesnpofl = v-icsnpofl.
        end.

        run oeetlz.p("o",
                     g-orderno,
                     g-ordersuf,
                     v-deletecomfl,
                     s-returnfl,
                     yes,
                     yes,
                     (if s-returnfl = no and v-whsesnpofl = no then true
                      else false),
                     "").
    end.

    if ({k-cancel.i} or {k-func13.i}) then do:

        /*d Oeetl end */
        /*d TB# e11627 01/08/02 Removed quotes */
        {p-oeetlz.i {&com}}

        assign
            v-func     = no
            v-onlyfl   = no
            s-rushdesc = "".

        /*u User Include */
        {p-oeetl.z99 &aft_p-oeetlz = "*"}

        hide message no-pause.

        next main.

    end. /* if k-cancel.i or k-func13.i */

    else if {k-jump.i} then leave main.

    /*o Process the line item */
    v-func = no.

    {&com}
    /*tb 10078 08/28/01 mt; only if "standard" oeel record */
    if not can-do("l,e,i,t":u,s-specnstype) and
    /{&com}* */

        {&com}
        ((avail b-icsp and b-icsp.statustype <> "l") or not avail b-icsp) and
        (not (can-do("st,fo,bl,qu,do,cr,rm,cs,ra",g-oetype) or s-botype = "d")
        or (g-oetype = "cs" and b-oeeh.ordersuf = 0 and v-csbofl = yes and
            s-botype <> "d")) and
        not s-returnfl
    then do:

        if s-qtyship < s-qtyord and
              ((s-qtyship <> o-totqtyship) or
               (s-qtyord  <> o-totqtyord ) or
               (s-unit    <> o-unitchg   ) or
               (s-botype  <> o-botype    ))
        then do:

            if s-botype = "y" then do:

                /*d Error checking */
                {p-oeetea.i}

            end. /* if s-botype = y */

            /*d Error checking */
            {p-oeeteb.i}

            readkey pause 0.

        end. /* if s-botype = y */

    end. /* end of if s-specnstype <> l */

    /{&com}* */

    if s-specnstype = "l" then do:

        s-commentfl = "".

        display s-commentfl.

    end. /* if s-specnstype = l */

    /*tb 22607 02/19/97 mtt; Allow different Whse for VA order */
    {&com}

    assign
        o-ordertype    = s-ordertype
        v-vacreatewtfl = if s-ordertype = "f" and
                            v-vawhse ne g-whse and
                            v-vawhse ne "" and
                            s-orderaltno = 0
                         then
                             yes
                         else
                             no
        s-ordertype    = if v-vacreatewtfl = yes
                         then
                             "t"
                         else
                             s-ordertype
        s-whse         = if v-vacreatewtfl = yes
                         then
                             v-vawhse
                         else
                             s-whse
        v-powtnew      = if v-vacreatewtfl = yes
                         then
                             yes

                         else
                             v-powtnew.

    if v-vacreatewtfl = yes then run oeetvawt.p.


    /*tb 23574 07/29/97 gp; Need to access TWL order for changes */
    /*tb 16268 02/24/03 spo; don't allow ord qty or um to be modified either */
    if v-wlwhsefl = yes and s-wlwhsechgfl = no then
    do:
        /*tb 10078 08/28/01 mt; only if "standard" oeel record */
        if v-maint-l = "a" or
            s-specnstype = "l" or
            (v-maint-l = "c" and
             avail b-oeel    and
             not can-do("l,e,i":u,s-specnstype)   and
             ((v-stkqtyship <> b-oeel.stkqtyship) or
              (s-qtyord     <> o-totqtyord)       or
              (s-unit       <> o-unitchg)         or
              (s-prod <> b-oeel.shipprod))) then
        do:
            s-prod = "".
            run err.p(6555).
            undo main, next main.
        end. /* end if v-maint-l ... */

    end. /* end if v-wlwhsefl ... */

    /{&com}* */

    /*d oeetlt.p/oeebtlt.p - order entry line items - transaction state after
        user interface complete */
    if g-ourproc <> "oeebt" then
        run oeetlt.p (yes,yes,yes).
    else
        run oeebtlt.p(yes,yes,yes).

    /*tb # 10078 10/09/01 mt; add logic to display seq # for oeelc lines */
    if g-ourproc <> "oeebt" and v-maint-l = "a" and
       can-do("e,i":u,s-specnstype) then
        display
            s-rushdesc
        with frame f-oeetl.

    /*tb 24806 04/22/98 kjb; The b-icsw and the b-oeel records are down graded
        to share locks when returning from oeetlt.p.  Need to release the
        records and re-find them no-lock. */
    release b-icsw.
    release b-oeel.

    find b-icsw where recid(b-icsw) = v-idicsw1 no-lock no-error.
    find b-oeel where recid(b-oeel) = v-idoeel  no-lock no-error.
    find b-oeeh where recid(b-oeeh) = v-idoeeh  no-lock no-error.

    /* TB# e13043 OE useability */
    if not avail b-oeel then do:
        scroll.
        next main.
    end.

    if g-oetype = "qu" and s-lostbusty <> "" then
        display b-oeel.specnstype @ s-specnstype with frame f-oeetl.

    /*u User Include */
    {p-oeetl.z99 &user_aftupdt = "*"}

    /*tb 21254 08/29/96 jms; Develop Value Add Module */
    /*d Value Add - Create Value Add Tra nsaction */
    /*tb 22607 02/19/97 mtt; Allow different Whse for VA order */

    {&com}
    /*d OE line is tied to a VA Order. */
    if o-ordertype = "f" then do:

        o-whse = g-whse.

        /* Check to see if multiple ties exist for this VA order and give
           warning. */
        if s-orderaltno <> 0 and
            can-find(vaelo use-index k-vaelo where
                vaelo.cono = g-cono       and
                vaelo.vano = s-orderaltno and
               (vaelo.ordertype   <> "o":u           or
                vaelo.orderaltno  <> b-oeel.orderno  or
                vaelo.linealtno   <> b-oeel.lineno))
        then do:
            /* Multiple Ties Exist; Use Caution When Changing VA Order */
            run err.p(8727).
            {pause.gsc}
        end.

        /*tb 24812 08/24/98 bm; added b-oeel.rushfl as 2nd last param */
        /*tb 9731 10/05/98 kjb; Added a new parameter for the second
            nonstock description - 14th in list */
        run vaetcret.p(g-vanotemp,
                       g-vasuftemp,
                       b-oeel.shipprod,
                       if v-vacreatewtfl = yes
                       then
                           v-vawhse
                       else
                           g-whse,
                       b-oeel.specnstype,
                       s-qtyord - s-qtyship,
                       b-oeel.unit,
                       if v-vacreatewtfl = yes
                       then
                           "y"
                       else
                           b-oeel.botype,
                       b-oeeh.approvty,
                       if v-orderdisp = "j" then b-oeel.reqshipdt
                           else b-oeeh.reqshipdt,
                       if v-orderdisp = "j" then b-oeel.promisedt
                           else b-oeeh.promisedt,
                       if v-vacreatewtfl = yes
                       then
                           0
                       else
                           s-orderaltno,
                       b-oeel.vendno,
                       b-oeel.arpprodline,
                       b-oeel.proddesc,
                       b-oeel.proddesc2,
                       b-oeel.prodcat,
                       if v-vacreatewtfl = yes
                       then
                           "t"
                       else
                           "o",
                       if v-vacreatewtfl = yes
                       then
                           g-wtno
                       else
                           g-orderno,
                       if v-vacreatewtfl = yes
                       then
                           g-wtsuf
                       else
                           g-ordersuf,
                       if v-vacreatewtfl = yes
                       then
                           b-oeel.linealtno
                       else
                           b-oeel.lineno,
                       0,
                       b-oeel.rushfl,
                       yes).
        g-whse = o-whse.

    end. /* o-ordertype = "f" */
/*d Ask the user if they want to calculate VA rollup costs/prices if this
        OE line is tied to a VA Order (either directly or via a WT), or the
        product has an ARP of VA. */
    /*e NOTE: If the user selects to calculate VA rollup costs/prices, the
        calculations are done in the rebties.p routine, then presented to the
        user afterwards in the varollprc.gpr logic (see below).  This
        separation of presentation vs. calculation logic was intentional so
        that the rebties.p logic could be used as-is in the GUI Appserver
        environment. */
    if o-ordertype          = "f":u or
        b-oeel.ordertype    = "t":u or
       (avail b-icsw
        and
        b-icsw.arptype      = "f":u)
    then do:

        /* Pop VA Rollup screen if user has security to view or enter prices.
           NOTE: Design decision NOT to pop this screen if the user ONLY has
           cost security since the main pupose of this feature is to roll
           PRICES from the VA components.  If this restriction is loosened,
           an addtional 'or g-seecostfl = yes' could be added here. */
        if can-do("e,v":u,v-oepricefl) then do:

            assign
                s-varollfl          = no
                s-vamarkupfl        = no
                s-vaextmarkup       = 0
                s-vaintmarkup       = 0
                s-vanonstkmarkup    = 0
                v-rebtiefl          = yes
                s-varolltitle       = "VA Tie/Arp Rollup for " +
                                      b-oeel.shipprod
                s-vacostprctitle    = "VA Tie/Arp Cost and Price Rollup " +
                                      "for " + b-oeel.shipprod.

            /*e When this OE line is tied to a WT, we need to determine if the
                WT is tied to a VA Order in order to proceed. */
            if o-ordertype          ne "f":u and
                b-oeel.ordertype    =  "t":u
            then do for wtelo:

                find first wtelo use-index k-order where
                    wtelo.wtcono        =  g-cono           and
                    wtelo.ordertype     =  "o":u            and
                    wtelo.orderaltno    =  b-oeel.orderno   and
                    wtelo.orderaltsuf   =  b-oeel.ordersuf  and
                    wtelo.linealtno     =  b-oeel.lineno    and
                    wtelo.seqaltno      =  0
                no-lock no-error.

                v-rebtiefl = if avail wtelo                 and
                    can-find(first vaelo use-index k-order where
                        vaelo.cono          =  g-cono       and
                        vaelo.ordertype     =  "t":u        and
                        vaelo.orderaltno    =  wtelo.wtno   and
                        vaelo.orderaltsuf   =  wtelo.wtsuf  and
                        vaelo.linealtno     =  wtelo.lineno and
                        vaelo.seqaltno      =  0)
                    then yes
                    else no.

            end. /* o-ordetype ne "f", b-oeel.ordertype = "t" */

            if v-rebtiefl = yes then
            VAroll:
            do while true on endkey undo VAroll, leave VAroll:

                update
                    s-varollfl
                    s-vamarkupfl        when v-oepricefl = "e":u
                    s-vaextmarkup       when v-oepricefl = "e":u
                    s-vaintmarkup       when v-oepricefl = "e":u
                    s-vanonstkmarkup    when v-oepricefl = "e":u
                with frame f-varoll.

                hide frame f-varoll no-pause.
                leave VAroll.

            end. /* VAroll */

        end. /* can-do("e,v":u,v-oepricefl) */

    end. /* OE line is tied/ARP'd to VA */

    /*d Calculate rebates based VA order either tied directly to the OE,
        or via a WT (when the VA is being newly created, above). */
    /*tb e8225 Moved this logic lower so that we can pop the above VA Rollup
        prompt AFTER the VA Order screens are accessed (in vaetcret.p) but
        BEFORE the rebties.p logic is done. */
    if o-ordertype = "f" then
        run rebties.p(input  if v-vacreatewtfl = no then "vaerb":u
                             else "vatrb":u,
                      input  g-ourproc,
                      input  b-oeel.orderno,
                      input  b-oeel.ordersuf,
                      input  b-oeel.lineno,
                      output v-rebtiefl).

    /*d Calculate rebates based on VA order tied to the OE via a WT. */
    else if b-oeel.ordertype = "t":u then
        run rebties.p(input  "vatrb":u,
                      input  g-ourproc,
                      input  b-oeel.orderno,
                      input  b-oeel.ordersuf,
                      input  b-oeel.lineno,
                      output v-rebtiefl).

    /*d Calculate rebates based on ARP of VA. */
    else if avail b-icsw and b-icsw.arptype = "f":u then
        run rebties.p(input  "vasrb":u,
                      input  g-ourproc,
                      input  b-oeel.orderno,
                      input  b-oeel.ordersuf,
                      input  b-oeel.lineno,
                      output v-rebtiefl).

    /*d Calculate rebates based on tied KP work order. */
    else if b-oeel.ordertype = "w":u then
        run rebties.p(input  "kperb":u,
                      input  g-ourproc,
                      input  b-oeel.orderno,
                      input  b-oeel.ordersuf,
                      input  b-oeel.lineno,
                      output v-rebtiefl).

    /*d Calculate rebates based on ARP of KP. */
    else if avail b-icsw and b-icsw.arptype = "k":u then
        run rebties.p(input  "kpsrb":u,
                      input  g-ourproc,
                      input  b-oeel.orderno,
                      input  b-oeel.ordersuf,
                      input  b-oeel.lineno,
                      output v-rebtiefl).
/*d If the user wanted to calculate VA rollup costs/prices, then present
        them with the VA Rollup screen if this OE line is tied to a VA Order
        (either directly or via a WT), or the product has an ARP of VA. */
    if o-ordertype          = "f":u or
       (b-oeel.ordertype    = "t":u
        and
        v-rebtiefl          = yes)  or
       (avail b-icsw
        and
        b-icsw.arptype      = "f":u)
    then do:

        /*d Pop VA Rollup Cost/Price screen and allow user to override the
            price on the OE line (depending on their security). */
        {varollprc.gpr &displayfl = yes}

    end. /* OE line is tied/ARP'd to VA */

    /{&com}* */

    scroll.

    /*u User Custom Run */
    if v-oeendfl then run oeetl99.p("el").

    if {k-func6.i} or {k-func8.i} or {k-func9.i} then leave main.

end. /* main: */

assign
    g-oelineno = v-nextlineno
    g-ourproc  = if g-ourproc = "oeebt" then "oeebt" else "oeet".

if not {k-jump.i} then do:

    hide frame f-oeetl no-pause.
    /*d hide totals for extended line format */
    if v-oeextlfl then
        hide frame f-oeetltx no-pause.
    else
        /*d head regular line totals */
        hide frame f-oeetl      no-pause.
        hide frame f-oeetlt     no-pause.
        hide frame f-varoll     no-pause.
        hide frame f-vacostprc  no-pause.

end. /* if not k-jump.i */

