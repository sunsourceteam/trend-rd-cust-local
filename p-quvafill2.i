assign v-oneinthedirection = false.

if "{&direction}" = "prev" then do: 
 find t-whseinfo where t-whseinfo.whse = va-whse[1] no-lock no-error.
 if not avail t-whseinfo then
  next label1.
 assign v-idx = 15.
end.  
if "{&direction}" = "next" then do: 
 find t-whseinfo where t-whseinfo.whse = va-whse[15] no-lock no-error.
 if not avail t-whseinfo then
  next label1.
 assign v-idx = 1.
end.  

do v-idx2 = 1 to 15:    
 find {&direction} t-whseinfo no-lock no-error.
 if avail t-whseinfo and v-oneinthedirection = false then do: 
  assign v-oneinthedirection = true.
 end.
 else
 if not avail t-whseinfo and v-oneinthedirection = false then do: 
  leave.
 end.
    
 if "{&direction}" = "prev" and avail t-whseinfo then do:
  if t-whseinfo.zavail > 999 then
   assign disp-avail = "***".
  else
  if t-whseinfo.zavail < 0 then
   assign disp-avail = "***".
  else
   assign disp-avail = string(t-whseinfo.zavail,"zz9").

  if t-whseinfo.zoo > 999 then
   assign disp-oo = "***".
  else
  if t-whseinfo.zoo < -999 then
   assign disp-oo = "***".
  else
   assign disp-oo = string(t-whseinfo.zoo,"zz9-").
     
  assign hv-avail[v-idx] = disp-avail + "/" + disp-oo.
  assign va-whse [v-idx] = t-whseinfo.whse
                   v-idx = v-idx - 1.
  
 end.
 if "{&direction}" = "next" and avail t-whseinfo then do:
  if t-whseinfo.zavail > 999 then
   assign disp-avail = "***".
  else
  if t-whseinfo.zavail < 0 then
   assign disp-avail = "***".
  else
   assign disp-avail = string(t-whseinfo.zavail,"zz9").

  if t-whseinfo.zoo > 999 then
   assign disp-oo = "***".
  else
  if t-whseinfo.zoo < -999 then
   assign disp-oo = "***".
  else
   assign disp-oo = string(t-whseinfo.zoo,"zz9-").
     
  assign hv-avail[v-idx] = disp-avail + "/" + disp-oo.
  assign va-whse [v-idx] =  t-whseinfo.whse
                   v-idx = v-idx + 1.

 end.
 if "{&direction}" = "prev" and not avail t-whseinfo then
  assign va-whse [v-idx] =  "    "
         hv-avail[v-idx] = "       "
         v-idx = v-idx - 1.
 if "{&direction}" = "next" and not avail t-whseinfo then
  assign va-whse [v-idx] =  "    "         
         hv-avail[v-idx] =  "       " 
         v-idx = v-idx + 1.
             
end.           

display va-whse[1]   va-whse[2]   va-whse[3]   va-whse[4]   va-whse[5] 
        va-whse[6]   va-whse[7]   va-whse[8]   va-whse[9]   va-whse[10]     
        va-whse[11]  va-whse[12]  va-whse[13]  va-whse[14]  va-whse[15]        
        hv-avail[1]  hv-avail[2]  hv-avail[3]  hv-avail[4]  hv-avail[5]
        hv-avail[6]  hv-avail[7]  hv-avail[8]  hv-avail[9]  hv-avail[10] 
        hv-avail[11] hv-avail[12] hv-avail[13] hv-avail[14] hv-avail[15] 
        with frame f-whses1.
