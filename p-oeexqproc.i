/******************************************************************************
  INCLUDE       : p-oeexqproc.i
  DESCRIPTION   : Custom Quote Entry - Main Entry Include
  USED ONCE?    : No (oeexq.p, oeexqinq.p)
  AUTHOR        : SunSource
  DATE WRITTEN  : 01/01/07
  CHANGES MADE  : 01/01/07 tah; fix drop of last character
                  06/10/09 das; NonReturn/NonCancelable
                  
******************************************************************************/


/* -------------------------------------------------------------------------  */
Procedure Edit_v-qty:
/* -------------------------------------------------------------------------  */
  
  if can-do("501,504",string(lastkey)) then
    assign typeover = yes.
  else
    assign typeover = no.
  if v-lmode = "c" and not avail oeelb then
    find oeelb where oeelb.cono = g-cono and
         oeelb.batchnm = string(w-batchnm,">>>>>>>9") and
         oeelb.lineno = v-lineno
         no-lock no-error.
  if v-lmode = "a" and v-slsrepovr = no then
    do:
    if v-slsrepout = "" then
      do:
      assign v-slsrepout = if avail arss then arss.slsrepout
                           else if avail arsc then arsc.slsrepout
                                else v-slsrepout.
      if v-slsrepout = "" then
        assign v-slsrepout = o-slsrepout.
    end. /* v-slsrepout = "" */
    if v-slsrepin = "" then
      do:
      v-slsrepin  = if avail arss then arss.slsrepin
                    else if avail arsc then arsc.slsrepin
                         else v-slsrepin.
    end.

    run xsdioetlt.p (input-output v-slsrepout,
                     input v-prodcat,
                     input v-custno,
                     input v-shipto).

  end. /* v-lmode = "a" and v-slsrepovr = no */
  if ((v-lmode = "a" and avail icsp) or
     (v-hqty <> v-qty and avail icsp)) then 
    do:
    assign /*v-qty = input v-qty*/
           v-sellmultfl = if icsp.sellmult > 0 then true else false.
           v-qty = if icsp.sellmult > 0 then
                     icsp.sellmult * (truncate(v-qty / icsp.sellmult,0) +
                     if (v-qty / icsp.sellmult) -
                       truncate(v-qty / icsp.sellmult,0) > 0 then 1        
                     else 0)
                   else v-qty.
                 
    /*display v-qty with frame f-mid1.*/
    if lastkey = 13 or lastkey = 401 or lastkey = 9 then
      do:
      assign x-whse       = v-whse
             x-specnstype = v-specnstype.
/* TAH 02-08-11 Tami does not want this to pop again just on the product
                selection
                
      run XRef_Check (input v-whse,
                      input v-seqno,
                      input-output v-qty,
                      input-output v-prod).
      if lastkey = 404 then 
        do:
        readkey pause 0.
        assign cancel-xref = yes
               w-key = 13.
        apply w-key.
      end.
   TAH 02-08-11 */
      assign v-whse = x-whse
             v-specnstype = x-specnstype.
       
    end. /* if lastkey = 13 or lastkey = 401 or lastkey = 9 */
    display v-prod with frame f-mid1.
          
    run Get-Whse-Info (input v-prod,
                       input v-whse,
                       input v-qty,
                       input-output v-shipdt,
                       input-output v-specnstype,
                       input-output v-ptype,
                       input-output v-foundfl,
                       input-output v-statustype,
                       input-output v-prodline,
                       input-output v-rarrnotesfl).

    display v-specnstype v-shipdt with frame f-mid1.
    
    assign zi-displaylinefl = true
           h-sellprc = v-sellprc.
    if (avail oeelb and oeelb.priceoverfl = no) or (v-lmode = "a" and
                                                    manual-change = no) then
      do:
      /*assign zi-PDSCfl = yes.*/
      run oeexqpricing.p.   
      if v-rebatefl <> " " and h-rebfl = " " then
        assign h-rebfl = v-rebatefl.
      /*assign zi-PDSCfl = no.*/
    end.
    if zi-displaylinefl then
      do:
      assign disp-sellprc = v-sellprc
             v-sellprc = round(v-sellprc,3).
      display   
        v-lineno     
        v-specnstype
        v-prod  
        v-qty     
        v-whse
        v-sellprc    
        v-matrixed
        v-shipdt
        v-NR
        v-gp          when v-dispGP = yes
        /*v-ptype*/
        v-listprc     when v-dispGP = yes
        v-disc        when v-dispGP = yes
        v-kitfl  
        v-lcomment
      with frame f-mid1.
      assign v-sellprc = disp-sellprc.
    end.
  end. /* if v-lmode = "a" and avail icsp */
        
  if v-lmode = "c" then 
    do:
    if h-sellprc <> v-sellprc and
       h-sellprc <> 0 and
       oeelb.priceoverfl = no then
      do:
      find oeelb where
           oeelb.cono    = g-cono and
           oeelb.batchnm = string(w-batchnm,">>>>>>>9") and
           oeelb.lineno  = v-lineno
           no-error.
      if avail oeelb then
        do:
        assign repricefl = yes.
        run Update_OEELB.
        assign repricefl = no.
      end.
    end. /* if h-sellprc <> v-sellprc */
    run Quote-Total.

  end. /* v-lmode = "c" */
  assign v-totcost    = if avail icsp and icsp.kittype = "b" then
                          v-prodcost * input v-qty  
                        else
                          (v-prodcost - v-sparebamt) * input v-qty
         v-totnet     = v-sellprc  * input v-qty
         v-totmarg    = v-totnet - v-totcost
         v-margpct    = if v-totnet > 0 then
                          ((v-totnet - v-totcost) / v-totnet) * 100
                        else
                          0.
        
  if lastkey = -1 then
    do:
    readkey pause 0.  /* reset lastkey */
    next-prompt v-whse with frame f-mid1.
    next.
  end.
end. /* procedure Edit_v-qty */

/* -------------------------------------------------------------------------  */
Procedure Edit_v-sellprc:
/* -------------------------------------------------------------------------  */
  /* if v-surcharge was changed in F8-Extend, save it */

  if v-chgsrchgfl = yes then
    assign h-surcharge = v-surcharge.
  assign v-surcharge = 0.
  run zsdidatccalc.p (input v-whse,
                      input v-prod,
                      input v-custno,
                      input v-sellprc,
                      input-output v-surcharge).
  /* if v-surcharge was changed in F8-Extend, put it back */
  if v-chgsrchgfl = yes then
    assign v-surcharge = h-surcharge.

  find first icss where icss.cono = g-cono and
                        icss.prod = v-prod and
                        icss.statusfl = yes
                        no-lock no-error.
  if avail icss then
    assign v-speccostty = icss.speccostty
           v-csunperstk = icss.csunperstk.
  else
    assign v-speccostty = " "
           v-csunperstk = 0.
  /*** das - 06/19/09 - moved lower in code so margin info gets populated
  if zsdiconfirm = no or v-sellprc >= 0 then
    do:
    run oeexqauth.p.
  end.
  if v-errfl = "b" then return.
  ***/
  if avail icsp and icsp.kittype = "b" then 
    do:
    run Roll_Kit_Cost-Price (input "c",
                             input-output v-sellprc,
                             input-output v-kitcost,
                             input-output v-gp).
    /*assign v-prodcost = v-kitcost.*/
  end.  
  else
    assign x-gp = if round(v-sellprc,3) <> 0 then
                    ((v-sellprc - round(v-prodcost,3)) / v-sellprc) * 100
                  else 0
           v-gp = if x-gp > 999999.99 then 999.99
                  else if x-gp < -999999.99 then -999.99
                       else x-gp.

  assign /*v-sellprc  = input v-sellprc*/
         /* why re-calculate margin% when it's done in above procedure */
         x-gp       = /*if round(v-sellprc,3) <> 0 then
                        ((v-sellprc - round(v-prodcost,3)) / v-sellprc) * 100
                      else 0*/
                      v-gp
         v-gp       = if x-gp > 999999.99 then 999.99
                      else if x-gp < -999999.99 then -999.99
                           else x-gp
         v-margpct  = v-gp
         zi-margpct = v-gp
         v-totmarg  = v-sellprc - round(v-prodcost,3)   /* das - 061909 */
         v-disc     = if v-sellprc > v-listprc or v-listprc = 0 then
                        0
                      else 
                        ((v-listprc - v-sellprc) / v-listprc) * 100.
  if zi-totnet <> (v-sellprc * v-qty) then
    assign zi-totnet = (v-sellprc * v-qty).
    
 
  if zsdiconfirm = no or v-sellprc >= 0 then
    do:
    if auth-sellprc <> v-sellprc and v-disc > 0 and manual-change = no then
      assign s-price = if auth-sellprc <> 0 then auth-sellprc else s-price.
    run oeexqauth.p.
    assign s-price = v-sellprc.
  end.
  if v-errfl = "b" then return.
  
end. /* procedure Edit _v-sellprc */


/* -------------------------------------------------------------------------  */
Procedure Continue_Middle1_Process:
/* -------------------------------------------------------------------------  */
  run oeexqSauerAPR.p (input "P"). 
  if v-whse = " " then
    do:
    run err.p(4601).
    next-prompt v-whse with frame f-mid1.
    next.
  end.
  if v-kitfl = yes then
    do:
    if v-specnstype = "n" then
      do:
      assign hdr-whse = if avail oeehb then oeehb.whse else " ".
      run Check_NonStock_BODKits(input v-quoteno,    
                                 input v-lineno,
                                 input hdr-whse,
                                 input-output v-whse,
                                 output errorfl).
      if errorfl = yes then
        do:
        assign errorfl = no.
        message
        "Banner Whse " + hdr-whse +
        " and Kit Whse must match.  Enter separate quote for this Kit".
        next-Prompt v-whse with frame f-midl.
        next.
      end.
    end. /* if v-specnstype = "n" */
    else
      do:
      run oeexqckkitty.p(input v-prod,
                         input v-quoteno,
                         input v-lineno,
                         input-output v-whse,
                         output errorfl).
      if errorfl = yes then
        do:
        message
        "Banner Whse " + CAPS(oeehb.whse) +
        " and Kit Whse must match.  Enter separate quote for this Kit".
        next-prompt v-whse with frame f-mid1.
        next.
      end.
    end. /* v-specnstype <> "n" */
  end. /* v-kitfl = yes */

  assign s-prod       = v-prod
         s-qtyord     = v-qty
         s-chrgqty    = v-qty
         s-price      = /*input*/ v-sellprc
         s-specnstype = v-specnstype
         s-discamt    = 0 /*v-disc*/
         s-disctype   = no
         s-prodcost   = v-prodcost + zi-rebate
         s-returnfl   = no
         s-pricetype  = v-ptype
         s-prodcat    = v-prodcat
         v-rebamt     = v-sparebamt.
  find first icss where icss.cono = g-cono and
                        icss.prod = v-prod and
                        icss.statusfl = yes
                        no-lock no-error.
  if avail icss then
    assign v-speccostty = icss.speccostty
           v-csunperstk = icss.csunperstk.
  else
    assign v-speccostty = " "
           v-csunperstk = 0.
   
  if zsdiconfirm = no or v-sellprc >= 0 then
    do:
    if auth-sellprc <> v-sellprc and v-disc > 0 and manual-change = no then
      assign s-price = if auth-sellprc <> 0 then auth-sellprc else s-price.
    run oeexqauth.p.
    assign s-price = v-sellprc.
  end.
  if v-errfl = "b" then return.
    /****
    do:
    if v-sellprc >= 0 then
      do:
      run oeexqauth.p.
    end.
  end.
  if v-errfl = "b" then 
    do:
    next-prompt v-sellprc with frame f-mid1.
    assign v-errfl = " ".
    next.
  end.
  ****/
end. /* procedure Continue_Middle1_Process */



/* -------------------------------------------------------------------------  */
Procedure Check_product_ID:
/* -------------------------------------------------------------------------  */
  define input        parameter ix-type  as character                  no-undo.
  define input-output parameter ix-prod  like icsp.prod                no-undo.
  define input-output parameter ix-hprod like icsp.prod                no-undo.
  /*
  v-xprod      = "cpsoiub". /*"cxbpmigt".*/ /*if v-custprodfl then  "cxbpmigt"
                                                 else "xbpmigt". */
  */ 
  on cursor-up cursor-up. /*tab.*/
  on cursor-down cursor-down. /*back-tab.*/

  assign ix-hprod = ix-prod.

  if v-whse = "" then
    do:
    run Find_Whse (input x-prodtype).
  end.

  /* catalog */
  if v-lmode = "a" then
    run crossref.p (v-prod,
                    v-whse,
                    ix-type,
                    v-custno,
                    6,
                    5,
                    output ix-prod,
                    output v-whse,
                    output v-type,
                    output v-cqty,
                    output v-cunit).
  
  if v-whse = "" and x-whse <> "" then
    assign v-whse = x-whse.
  
  if ix-prod = "" then
    ix-prod = ix-hprod.
     
  if ix-prod <> ix-hprod then
  /* das - 07/05/07 - If avail icsp and an xref exists, use the xref
    do:
    if avail icsp and icsp.prod = ix-hprod then
      assign newprod = no
             ix-prod = ix-hprod.
    else
      assign newprod = yes.
  end.
  */
    assign newprod = yes.
    
  /* Catalog */           
  assign g-whse = x-whse
         catalogfl = false
         v-crprod  = ix-prod
         v-crwhse  = v-whse
         v-crtype  = v-type
         v-crqty   = v-cqty
         v-crunit  = v-cunit.
    
  if ix-prod = "" then 
     assign ix-prod   = v-prod.
            /*v-prctype = v-type.*/
  else
  if ix-prod <> "" and v-type = "G" then
    assign  v-prod = ix-prod
            catalogfl = if v-type = "g" then true else false.

  if catalogfl then 
    do:
    assign /*sh-prod = s-prod*/
           s-prod  = v-prod.
    if v-lmode = "a" then
      do:
      run vaexqlx.p ("G",no).  /* catalog cross reference options and access */
      assign v-prod = s-prod.
             /*s-prod = sh-prod.*/
      
      find u-icsp where u-icsp.cono = g-cono and
                        u-icsp.prod = v-prod
                        no-lock no-error.
      run zsdiCatalogTrace.p
         (input "Audit",  /* Audit, Inquire, Delete */
          input g-cono,
          input v-prod,
          input v-whse,
          input "OEEXQ",
          input (if avail u-icsp then "s" else "n"),
          input v-quoteno,
          input 0,
          input v-lineno,
          input v-seqno,
          input g-operinits,
          output v-zsdireturncd).
      assign cataddfl = if avail u-icsp then "gs" else "gn".
      /*run Catalog_Audit.*/
    end.       
  end.
/* Catalog */

  on cursor-up cursor-up.
  on cursor-down cursor-down.


end. /* Check_product_ID */
  

/* -------------------------------------------------------------------------  */
Procedure Whse_Selection:
/* -------------------------------------------------------------------------  */
  on cursor-up cursor-up.    
  on cursor-down cursor-down.

  if avail icsp and v-prodbrowseopen = true /*and
    v-prodbrowsekeyed = true*/ then
    do with frame f-whses:
      do while true on endkey undo, leave.
      next-prompt s-whse [1].
      
      
      choose field s-whse color input  no-error.
      
        color display normal s-whse with frame f-whses.
        display s-whse with frame f-whses.

        hide message no-pause.
              
        if keyfunction(lastkey) = "return" or
           keyfunction(lastkey)  = "go" then
           if frame-value <> "" then
             do:
             assign v-whse = frame-value.
             run Get-Whse-Info (input v-prod,
                                input v-whse,
                                input v-qty,
                                input-output v-shipdt,
                                input-output v-specnstype,
                                input-output v-ptype,
                                input-output v-foundfl,
                                input-output v-statustype,
                                input-output v-prodline,
                                input-output v-rarrnotesfl).
             display v-specnstype 
                     v-whse 
                     v-shipdt
                     with frame f-mid1.
             next-prompt s-whse [1].
             v-applygo = true.
             leave.
           end.
       
        if (frame-line = 1 and {k-scrlup.i}) or
           {k-prevpg.i} then
          do:
          {p-qufill.i &direction = prev}
          end.
        if (frame-line = 12 and {k-scrldn.i}) or
           {k-nextpg.i} then
          do:
          {p-qufill.i &direction = next}
          end.
        end.   
      if {k-cancel.i} then
        do:
        do i = 1 to 12:
          if s-whse[i] = frame-value then
            do:
            color display normal s-whse[i] with frame f-whses.
            display s-whse[i] with frame f-whses.
          end.
        end.
        next-prompt s-whse [1].
        v-applygo = false.     
        end.
    end.
    on cursor-up back-tab.
    on cursor-down tab.
  end.

/* -------------------------------------------------------------------------  */
Procedure FindNextFrame:
/* -------------------------------------------------------------------------  */
  def input        parameter ix-lastkey          as integer         no-undo.
  def input-output parameter ix-currentposition  as integer         no-undo.
  def input-output parameter ix-newposition      as char            no-undo.
  
  if {k-cancel.i "ix-"} and not v-ManualFrameJump then
    do:
    if avail OptionMoves and v-quoteno > 0 then
      find Opts where Opts.OptionIndex = OptionMoves.OptionIndex + 2
                      no-lock no-error.
    if not avail Opts then
      do:
      assign ix-currentposition = (if ix-currentposition - 2 < 0 then
                                     -1
                                   else
                                     ix-currentposition - 2).
      find first oeelb where oeelb.cono = g-cono and
                             oeelb.batchnm = string(v-quoteno,">>>>>>>9")
                             no-lock no-error.
      if not avail oeelb then 
        do:
        if ix-currentposition < 0 then
           assign ix-newposition      = "xx"
                  ix-currentposition  = 0.
        if v-quoteno > 0 then 
          do:
          find first oeelb where oeelb.cono = g-cono and
                           oeelb.batchnm = string(v-quoteno,">>>>>>>9")
                           no-lock no-error.
          if not avail oeelb then 
            do:
            find b-oeehb where 
                 b-oeehb.cono = g-cono and  
                 b-oeehb.batchnm = string(v-quoteno,">>>>>>>") and
                 b-oeehb.seqno   = 1
                 exclusive-lock no-error.          
            if avail b-oeehb then 
              do:
              assign v-delfl = yes.
              delmode:
              do on endkey undo delmode, leave delmode:
                update v-delfl label
   "The Quote Just Entered Does Not Have Line Items, Do You Want to Delete It"
                with frame f-del side-labels row 5 centered overlay.
   
                if v-delfl = yes then 
                  do:
                  delete b-oeehb.
                  run Clear-Frames.  
                  run Initialize-All.
                  leave.
                end.
              end.
            end. /* avail b-oeehb */
          end.  /* not avail oeehb */
          run Clear-Frames.  
          run Initialize-All.
          leave.
        end. /* v-quoteno > 0 */
        return.                          
      end. /* not avail oeehb */
    end. /* not avail Opts */
    else do:
      if Opts.OptionIndex < 5 then
        readkey pause 0.
    end.
  end. /* cancel and not manual jump */
  if v-ManualFrameJump  then 
    do: /*
          If they jumped frames bring them back into the place they
          jumped from within the operator settings.
        */   

    assign ix-currentposition = (if ix-currentposition - 1 < 0 then
                                  0
                                else
                                  ix-currentposition - 1)
           v-ManualFrameJump = false. 
    if v-framestate = "B2" then
      readkey pause 0. /* due to leaving quote when F4 was pressed on notes */
  end.

  if ix-currentposition = -1 then
    do:
    
    assign ix-newposition      = "xx"
           ix-currentposition  = 0. 
    return.
    end.  


  do v-inx = (ix-currentposition + 1) to 5 by 1:
    find OptionMoves where
         OptionMoves.OptionIndex  = v-inx no-lock no-error.
    if avail OptionMoves then
      leave.
  end.    
  if not avail OptionMoves then 
    do v-inx = 1 to 5 by 1:
      find OptionMoves where
           OptionMoves.OptionIndex  = v-inx no-lock no-error.
      if avail OptionMoves then
        leave.
    end.    
  
  assign ix-newposition      = OptionMoves.Procedure
         ix-currentposition  = OptionMoves.OptionIndex. 
         
  if ix-newposition = "d1" then do:
     assign ix-newposition = "xx"
            ix-currentposition = 0.
     return.
  end.
         
  if ix-newposition = "" then
    assign ix-newposition      = "t1"
           ix-currentposition  = 1. 

end. /* Procedure FindNextFrame */


/* -------------------------------------------------------------------------  */
Procedure LoadQuote:
/* -------------------------------------------------------------------------  */
 def input-output parameter errorfl  as logical      no-undo.

   for each t-lines:
     delete t-lines.
   end.
   assign v-qmode   = "c"
          v-hmode   = "c".
   assign x-quoteno = string(w-batchnm,">>>>>>>9").
   {w-oeehb.i x-quoteno 1 no-lock}
   if not avail oeehb then 
     do:
     assign errorfl = yes.
     message "Quote "  x-quoteno " is not in Quote Entry.". pause.
     return.
   end.
   
   if avail oeehb and oeehb.stagecd = 9 and inquiryfl = no then
     do:
     assign inquiryfl = yes.
     run oeexqinq.p.
     hide frame f-cnv no-pause.
     run Clear-Frames.
     run Initialize-All.
     if g-callproc = "zsdiob"     or g-callproc = "oeexf" or
        g-callproc begins "oeilo" or g-callproc = "oeizf" then
       return.
     assign inquiryfl     = no
            errorfl       = yes
            v-quoteno     = 0
            w-batchnm     = 0
          /* g-orderno     = 0 */
            v-shiptonm    = "" 
            v-shiptoaddr1 = "" 
            v-shiptoaddr2 = "" 
            v-shiptocity  = "" 
            v-shiptost    = "" 
            v-shiptozip   = "" 
            v-shiptogeocd =  0
            v-custpo      = "".
     /*release oeehb.*/
     
     display blankline1
             blankline2
             blankline3
             blankline4
             blankline5
             blankline6
             blankline7
             blankline8
             blankline9
             blankline10
             blankline11
             blankline12
             blankline13
             blankline14
             blankline15
             blankline16
             blankline17
     with frame f-blank.
     display v-quoteno with frame f-top1.
     return.
   end.

   {w-arss.i oeehb.custno oeehb.shipto no-lock}
   {w-arsc.i oeehb.custno no-lock}
   if avail arss then
     {w-smsn.i arss.slsrepout no-lock}
   else
     {w-smsn.i arsc.slsrepout no-lock}

   assign v-mode      = "c"
          v-custno    = oeehb.custno
          v-custno    = oeehb.custno
          v-hcustno   = oeehb.custno
          v-shipto    = oeehb.shipto
          v-custlu    = arsc.lookupnm
          v-hcustlu   = arsc.lookupnm
          v-custname  = if avail arss then
                          substr(arss.name,1,25)
                        else
                          substr(arsc.name,1,25)
          v-currency  = if arsc.currencyty = "CN" then
                          "CURRENCY: CN"
                        else ""
          v-terms     = if avail arss then arss.termstype
                        else arsc.termstype
          v-arnotefl  = if avail arss then arss.notesfl else arsc.notesfl
          v-takenby   = oeehb.takenby
          v-canceldt  = oeehb.canceldt
          v-quotetot  = oeehb.totinvamt
          v-quotemgn% = oeehb.user7
          v-contact   = substr(oeehb.user3,1,20)
          v-phone     = substr(oeehb.user3,21,12)
          v-email     = substr(oeehb.user3,33,20)
          v-custpo    = oeehb.custpo
          v-slsrepout = if avail arss then arss.slsrepout else arsc.slsrepout
          v-slsrepin  = if avail arss then arss.slsrepin  else arsc.slsrepin
          v-notefl    = oeehb.notesfl
          v-shiptonm    = oeehb.shiptonm
          v-shiptoaddr1 = oeehb.shiptoaddr[1]
          v-shiptoaddr2 = oeehb.shiptoaddr[2]
          v-shiptocity  = oeehb.shiptocity
          v-shiptost    = oeehb.shiptost
          v-shiptozip   = oeehb.shiptozip
          v-shiptogeocd = oeehb.geocd.
          
   find zsdiarsc where zsdiarsc.cono = g-cono and
                       zsdiarsc.custno = arsc.custno and
                       zsdiarsc.statustype = yes
                       no-lock no-error.
   if avail zsdiarsc then
     do:
     v-custno:dcolor in frame f-top1 = 2.
   end.
   else
     do:
     v-custno:dcolor in frame f-top1 = 0.
   end.
   
   /* das - 04/16/2013 - Competitor Focus Logic */
   find first notes where
              notes.cono = g-cono          and
              notes.notestype    = "zz"    and
              notes.primarykey   = "slsmx" and
              notes.secondarykey = string(arsc.custno,"99999999999") and
              notes.printfl2     = yes
              no-lock no-error.
   if avail notes then
     do:
     assign v-compfocus = "CF".
     v-compfocus:dcolor in frame f-top1 = 2.
   end.
   else
     do:
     assign v-compfocus = "  ".
     v-compfocus:dcolor in frame f-top1 = 0.
   end.

/* %toms% */
   if not v-conversion then
/* %toms% */
    
   display v-quoteno
           v-notefl
           v-custno 
           v-arnotefl
           v-shipto  
           v-custlu  
           v-custname
           v-compfocus
           v-takenby 
           v-contact 
           v-phone   
         /*  v-email   */
           v-custpo
           v-currency
           v-terms
           v-canceldt
           v-quotetot
           v-quotemgn%
           with frame f-Top1.
/*   disable v-quoteno with frame f-Top1.*/
   
   for each oeelb where oeelb.cono    = g-cono and
                        oeelb.batchnm = oeehb.batchnm and
                        oeelb.seqno   = oeehb.seqno
                        no-lock:
     find t-lines where t-lines.lineno = oeelb.lineno and
                        t-lines.seqno  = oeelb.seqno  
                        no-lock no-error.
     if avail t-lines then next.
     create t-lines.    
     assign t-lines.lineno     = oeelb.lineno
            t-lines.seqno      = 0
            t-lines.specnstype = oeelb.specnstype
            t-lines.prod       = oeelb.shipprod
            t-lines.descrip    = oeelb.proddesc
            t-lines.descrip2   = oeelb.proddesc2
            t-lines.xprod      = substr(oeelb.user5,30,24)
            t-lines.sellprc    = oeelb.price
            t-lines.listprc    = if oeelb.specnstype = "n" then
                                   oeelb.price
                                 else 0
            t-lines.listfl     = substr(oeelb.user4,25,1)
            t-lines.qty        = oeelb.qtyord
            t-lines.kqty       = 0
            t-lines.vendno     = oeelb.vendno
            t-lines.slsrepout  = oeelb.slsrepout
            t-lines.slsrepin   = oeelb.slsrepin
            t-lines.prodcat    = oeelb.prodcat
            t-lines.prodcost   = oeelb.prodcost
            t-lines.surcharge  = oeelb.datccost
            t-lines.leadtm     = oeelb.leadtm
            t-lines.pricetype  = oeelb.pricetype
            t-lines.prodline   = oeelb.prodline
/* %toms2% */
            t-lines.convertfl  = if v-conversion then 
                                    if substr(oeelb.user5,2,3) = "yes" then
                                      yes 
                                    else
                                      no
                                 else
                                    no
            t-lines.kitfl      = if substr(oeelb.user5,1,1) = "y" then yes
                                 else no
            t-lines.whse       = substr(oeelb.user4,1,4)
            t-lines.gp         = dec(substr(oeelb.user4,5,7))
            t-lines.listprc    = dec(substr(oeelb.user4,12,11))
            t-lines.shipdt     = if substr(oeelb.user4,26,8) = "?       " then
                                    ?
                                 else
                                    date(substr(oeelb.user4,26,8))
            t-lines.NR         = substr(oeelb.user10,1,1)
            t-lines.discpct    = dec(substr(oeelb.user4,34,7))
            t-lines.disctype   = if substr(oeelb.user4,40,1) = "%" then no
                                 else yes
            t-lines.qtybrkty   = substr(oeelb.user4,41,1)
            t-lines.pdscrecno  = int(substr(oeelb.user4,42,8))
            t-lines.pdtype     = int(substr(oeelb.user4,50,1))
            t-lines.pdamt      = dec(substr(oeelb.user4,51,10))
            t-lines.pd$md      = substr(oeelb.user4,61,1)
            t-lines.totnet     = /*dec(substr(oeelb.user4,62,11))*/
                                 oeelb.price * oeelb.qtyord
            t-lines.lcomment   = substr(oeelb.user4,74,1)
            t-lines.totcost    = /*dec(substr(oeelb.user3,44,11))*/
                                 (oeelb.prodcost - oeelb.user6) * oeelb.qtyord
            t-lines.rebatefl   = substr(oeelb.user3,55,1)
            t-lines.sparebamt  = oeelb.user6
            t-lines.totmarg    = /*dec(substr(oeelb.user3,56,11))*/
                                 (oeelb.price - (oeelb.prodcost + oeelb.user6)) 
                                          * oeelb.qtyord
            t-lines.margpct    = /*dec(substr(oeelb.user3,67,11)).*/
                                 if oeelb.price > 0 then
                                   (t-lines.totmarg / oeelb.price) * 100
                          /*
                          ((oeelb.price - oeelb.prodcost) / oeelb.price) * 100
                          */
                                 else 0
            t-lines.kitrollty  = substr(oeelb.user3,78,1).
     find icsp where icsp.cono = 1 and
                     icsp.prod = oeelb.shipprod
                     no-lock no-error.
     if avail icsp then
      assign t-lines.icnotefl = icsp.notesfl.
     assign v-surcharge        = oeelb.datccost 
            v-sparebamt        = oeelb.user6.

     if t-lines.kitfl = yes then do:
        for each oeelk where oeelk.cono = g-cono and
                         oeelk.ordertype = "b" and
                         oeelk.orderno   = v-quoteno and
                         oeelk.ordersuf  = 00 and
                         oeelk.lineno    = oeelb.lineno
                         no-lock:
          find t-lines where t-lines.lineno = oeelk.lineno and
                             t-lines.seqno  = oeelk.seqno
                             no-lock no-error.
          if avail t-lines then next.
          create t-lines.
          assign
            t-lines.lineno     =  oeelk.lineno
            t-lines.seqno      =  oeelk.seqno
            t-lines.prod       =  oeelk.shipprod
            t-lines.descrip    =  oeelk.refer
            t-lines.xprod      =  substr(oeelk.user5,5,24)
            t-lines.whse       =  oeelk.whse
/* %toms2% */
            t-lines.convertfl  = if v-conversion then 
                                    if substr(oeelk.user3,2,3) = "yes" then
                                      yes 
                                    else
                                      no
                                 else
                                    no
            t-lines.leadtm     =  int(oeelk.user7)
            t-lines.vendno     =  oeelk.arpvendno
            t-lines.slsrepout  =  oeelb.slsrepout
            t-lines.slsrepin   =  oeelb.slsrepin
            t-lines.prodcat    =  oeelk.prodcat
            t-lines.pricetype  =  oeelk.pricetype
            t-lines.listprc    =  if oeelk.specnstype = "n" then oeelk.price 
                                  else 0
            t-lines.sellprc    =  oeelk.price
            t-lines.prodcost   =  oeelk.prodcost
            t-lines.surcharge  =  0
            t-lines.gp         =  0
            t-lines.discpct    =  0
            t-lines.disctype   =  no
            t-lines.kqty       =  oeelk.qtyord
            t-lines.shipdt     =  ?
            t-lines.NR         =  " "
            t-lines.kitfl      =  no

            t-lines.specnstype = oeelk.specnstype
            t-lines.pdscrecno  = oeelk.pdrecno
            t-lines.lcomment   = substr(oeelk.user4,74,1)
            
            t-lines.prodline   = oeelk.arpprodline
            t-lines.gp         = dec(substr(oeelk.user4,5,7))
            t-lines.qtybrkty   = substr(oeelk.user4,41,1)
            t-lines.pdscrecno  = oeelk.pdrecno
            t-lines.pdtype     = int(substr(oeelk.user4,50,1))
            t-lines.pdamt      = dec(substr(oeelk.user4,51,10))
            t-lines.pd$md      = substr(oeelk.user4,61,1)
            t-lines.totnet     = /*substr(oeelk.user4,62,11).*/
                                 oeelk.price * oeelk.qtyord
            t-lines.totcost    = /*substr(oeelk.user3,44,11).*/
                                 (oeelk.prodcost - oeelk.user6) * oeelk.qtyord
            t-lines.rebatefl   = substr(oeelk.user3,55,1)
            t-lines.totmarg    = (oeelk.price - oeelk.prodcost) * oeelk.qtyord
                                 /*substr(oeelk.user3,56,11).*/
            t-lines.margpct    = /*substr(oeelk.user3,67,11).*/
                                 if oeelk.price > 0 then
                        ((oeelk.price - oeelk.prodcost) / oeelk.price) * 100
                                 else 0
            t-lines.sparebamt  = oeelk.user6.
          find icsp where icsp.cono = g-cono and
                          icsp.prod = oeelk.shipprod
                          no-lock no-error.
          if avail icsp then
            assign t-lines.icnotefl = icsp.notesfl.
        end.
     end.
   end.
   assign v-quoteloadfl = yes
          g-orderno = v-quoteno
          g-custno  = v-custno
          g-shipto  = v-shipto.

   if v-arnotefl = "!" and (not v-conversion and v-chgfl = no) and
     repricefl = no then
     do:
     run noted.p(yes,"c",string(v-custno),"").
     readkey pause 0.
   end.
 
end. /* Procedure LoadQuote */

/* ------------------------------------------------------------------------- */
Procedure Load-Defaults:
/* ------------------------------------------------------------------------- */
 find arsc where arsc.cono = g-cono and 
                 arsc.custno = oeehb.custno no-lock no-error.
 if oeehb.shipto <> "" then 
   find arss where arss.cono = g-cono and 
                   arss.custno = oeehb.custno and
                   arss.shipto = oeehb.shipto no-lock no-error.
 assign oeehb.nontaxtype = if avail arss then
                             arss.nontaxtype
                           else
                             arsc.nontaxtype.
 assign oeehb.taxablefl   = if avail arss and arss.taxablety <> "n" then
                              yes
                            else
                              if avail arsc and arsc.taxablety <> "n" then
                                yes
                              else
                                no.                      
  
 if oeehb.xxl12 = no then  /* Check if ShipTo Addr was overridden on popup */
   do:
   assign v-shiptostatecd = if avail arss then arss.state 
                            else
                              if avail arsc then arsc.state 
                              else " ".
           
   assign oeehb.statecd       = if avail arss then 
                                  arss.statecd
                                else
                                  if avail arsc then 
                                    arsc.statecd
                                  else " "
          oeehb.shiptonm       = if avail arss then arss.name 
                                 else
                                   if avail arsc then arsc.name 
                                   else " "
          oeehb.shiptoaddr[1] = if avail arss then arss.addr[1]
                                else
                                  if avail arsc then arsc.addr[1] 
                                  else " "
          oeehb.shiptoaddr[2] = if avail arss then arss.addr[2] 
                                else
                                  if avail arsc then arsc.addr[2] 
                                  else " "
          oeehb.shiptocity    = if avail arss then arss.city 
                                else
                                  if avail arsc then arsc.city 
                                  else " "
          oeehb.shiptost      = if avail arss then arss.state 
                                else
                                  if avail arsc then arsc.state 
                                  else " "
          oeehb.shiptozip     = if avail arss then arss.zip 
                                else
                                  if avail arsc then arsc.zip 
                                  else " "
          oeehb.geocd         = if avail arss then arss.geocd 
                                else
                                  if avail arsc then arsc.geocd 
                                  else 0.
 end. /* ShipTo Address was overridden on Popup xxl12 = yes */
 assign oeehb.outbndfrtfl   = if avail arss then arss.outbndfrtfl
                              else 
                                if avail arsc then arsc.outbndfrtfl
                                else oeehb.outbndfrtfl
        oeehb.inbndfrtfl    = if avail arss then arss.inbndfrtfl
                              else if avail arsc then arsc.inbndfrtfl
                                   else oeehb.inbndfrtfl
        oeehb.shipviaty     = if avail arss then arss.shipviaty
                              else if avail arsc then arsc.shipviaty
                                   else oeehb.shipviaty
        o-InBndFrtfl        = oeehb.inbndfrtfl
        o-OutBndFrtfl       = oeehb.outbndfrtfl
        oeehb.orderdisp     = if avail arss then arss.orderdisp
                              else
                                if avail arsc then arsc.orderdisp
                                else oeehb.orderdisp.
 run oeexqupdt.p.
 run Create_Addons.
end. /* Procedure Load-Defaults */


/* ------------------------------------------------------------------------- */
Procedure RePrice:
/* ------------------------------------------------------------------------- */
  
  if not avail oeehb then
    find oeehb where oeehb.cono = g-cono and
                     oeehb.batchnm = string(w-batchnm,">>>>>>>9") and
                     oeehb.seqno   = 1
                     no-lock no-error.
  for each oeelb where
           oeelb.cono        = g-cono and
           oeelb.batchnm     = string(w-batchnm,">>>>>>>9"):
      
     if inquiryfl = yes then
       run Load_Variables.
         
      assign v-qty        = oeelb.qtyord
             v-prod       = oeelb.shipprod
             v-specnstype = oeelb.specnstype
             v-whse       = substr(oeelb.user4,1,4)
             v-lineno     = oeelb.lineno.
             
      /*
      if v-whse = "" then
         run Find_Whse(input x-prodtype).
      */
   
      assign repricefl = yes.
      if oeelb.specnstype <> "n" then
        do:
        assign zi-displaylinefl = false.
        if s-pctrefresh > 0 then
          do:
          find rfresh where 
               rfresh.lineno = oeelb.lineno and
               rfresh.prod   = oeelb.shipprod and
               rfresh.seltype = "X" 
               no-lock no-error.
          if not avail rfresh then next.
          run oeexqpricing.p.       
          assign v-sellprc = rfresh.fut-sell
                 v-totnet  = v-sellprc * v-qty
                 x-gp      = ((v-sellprc - v-prodcost) / v-sellprc) * 100
                 v-gp       = if x-gp > 999999.99 then 999.99
                              else if x-gp < -999999.99 then -999.99
                                   else x-gp
                 v-margpct = v-gp
                 h-rebfl   = if v-rebatefl <> " " and h-rebfl = " " then 
                               v-rebatefl
                             else 
                               h-rebfl.
        end.
        else 
          do:
          run oeexqpricing.p.   
          if v-rebatefl <> " " and h-rebfl = " " then
            assign h-rebfl = v-rebatefl.
        end. /* else */
        if v-slsrepout = "" then
          do:
          if avail arsc then
            assign v-slsrepout = arsc.slsrepout.
          if avail arss then
            assign v-slsrepout = arss.slsrepout.
          if v-slsrepout = "" then
            assign v-slsrepout = o-slsrepout.
        end.

        if v-slsrepin = "" then
          do:
          if avail arss then
            assign v-slsrepin = arss.slsrepin.
          if avail arsc then
            assign v-slsrepin = arsc.slsrepin.
          if v-slsrepin = "" then
            assign v-slsrepin = o-slsrepin.
        end.

        run xsdioetlt.p (input-output v-slsrepout,
                         input v-prodcat,
                         input v-custno,
                         input v-shipto).
        find icsw where icsw.cono = g-cono and
                        icsw.whse = v-whse and
                        icsw.prod = v-prod
                        no-lock no-error.
        find icsd where icsd.cono = g-cono and 
                        icsd.whse = v-whse 
                        no-lock no-error.
        if avail icsw and v-shipdt = ? then
          run Assign_LineDate (input v-lmode,
                               input repricefl,
                               input v-whse,
                               input x-qtyavail,
                               input v-qty,
                               input icsw.prod,
                               input icsw.statustype,
                               input icsw.qtyreserv,
                               input icsw.qtycommit,
                               input icsw.avgltdt,
                               input icsw.leadtmavg,
                               input icsd.enddaycut,    
                               input oeehb.divno,
                               input-output v-shipdt).
        run Update_OEELB.
      end. /* oeelb.specnstype <> "n" */
      else 
        do:
        overlay(oeelb.user4,1,4) = v-whse.
        assign oeelb.xxc3 = v-whse.
      end.
      overlay(oeelb.user12,1,15) = " ".
      run Initialize-Line.
      run LoadQuote (input-output errorfl).
   end.
   assign repricefl = no.
end. /* procedure RePrice */

/* ------------------------------------------------------------------------- */
Procedure ReDate:
/* ------------------------------------------------------------------------- */
  
  /*if not avail oeehb then */
    find oeehb where oeehb.cono = g-cono and
                     oeehb.batchnm = string(w-batchnm,">>>>>>>9") and
                     oeehb.seqno   = 1
                     /*no-lock.*/  no-error. 
  for each oeelb where
           oeelb.cono        = g-cono and
           oeelb.batchnm     = string(w-batchnm,">>>>>>>9"):
    find dtrfresh where 
         dtrfresh.lineno = oeelb.lineno and
         dtrfresh.prod   = oeelb.shipprod and
         dtrfresh.seltype = "X" 
         no-lock no-error.
    if not avail dtrfresh then next.
    assign dt-refreshfl = yes.
    assign oeelb.promisedt   = dtrfresh.fut-date
           oeelb.reqshipdt   = dtrfresh.fut-date
           oeelb.xxda1       = dtrfresh.fut-date
           /*oeehb.promisedt = dtrfresh.fut-date
           oeehb.reqshipdt   = dtrfresh.fut-date*/
           v-promisedt       = dtrfresh.fut-date.
    overlay(oeelb.user12,1,8)  = string(dtrfresh.fut-date,"99/99/99").
    overlay(oeelb.user12,10,4) = string((dtrfresh.fut-date - TODAY),"9999").
    overlay(oeelb.user4,26,8)  = string(dtrfresh.fut-date,"99/99/99").
    find t-lines where t-lines.lineno = oeelb.lineno  and
                       t-lines.seqno  = 0 no-error.
    if avail t-lines then
      assign t-lines.shipdt = dtrfresh.fut-date.
  end.
end. /* procedure ReDate */



/* ------------------------------------------------------------------------- */
Procedure Update_OEEHB:
/* ------------------------------------------------------------------------- */
  if not avail arss and oeehb.shipto <> ""  then
    find arss where arss.cono   = g-cono and
                    arss.custno = oeehb.custno and
                    arss.shipto = oeehb.shipto
                    no-lock no-error.
    
  if not avail arsc then
    find arsc where arsc.cono   = g-cono and
                    arsc.custno = oeehb.custno 
                    no-lock no-error.
  assign  oeehb.custno        = arsc.custno
          oeehb.takenby       = v-takenby
          oeehb.shipto        = v-shipto
          oeehb.shiptonm      = if v-shiptonm <> "" then v-shiptonm
                                else oeehb.shiptonm
          oeehb.shiptoaddr[1] = if v-shiptoaddr1 <> "" then v-shiptoaddr1
                                else oeehb.shiptoaddr[1]
          oeehb.shiptoaddr[2] = if v-shiptoaddr2 <> "" then v-shiptoaddr2
                                else oeehb.shiptoaddr[2]
          oeehb.shiptocity    = if v-shiptocity <> "" then CAPS(v-shiptocity)
                                else oeehb.shiptocity
          oeehb.shiptost      = if v-shiptost <> "" then CAPS(v-shiptost)
                                else oeehb.shiptost
          oeehb.shiptozip     = if v-shiptozip <> "" then CAPS(v-shiptozip)
                                else oeehb.shiptozip
          oeehb.geocd         = if v-shiptogeocd <> 0 then v-shiptogeocd
                                else oeehb.geocd
          oeehb.statecd       = if avail arss then arss.statecd
                                else arsc.statecd
          oeehb.xxl12         = if oeehb.xxl12 = no then w-shipchgfl
                                else oeehb.xxl12
          oeehb.whse          = if avail arss and v-shipto <> "" 
                                  then arss.whse
                                else arsc.whse
          oeehb.shipviaty     = if avail arss and v-shipto <> ""
                                  then arss.shipviaty
                                else arsc.shipviaty
          oeehb.countrycd     = if oeehb.countrycd <> "" then oeehb.countrycd
                                  else 
                                  if arsc.divno = 40 then "CN"
                                    else arsc.currencyty
          oeehb.custpo        = v-custpo
          oeehb.orderdisp     = if avail arss and v-shipto <> ""
                                  then arss.orderdisp
                                else arsc.orderdisp
          oeehb.termstype     = if avail arss and v-shipto <> ""
                                  then arss.termstype
                                else arsc.termstype
          oeehb.shipinstr     = if avail arss and v-shipto <> "" 
                                  then arss.shipinstr
                                else arsc.shipinstr
          oeehb.slsrepout     = if avail arss and v-shipto <> ""
                                  then arss.slsrepout
                                else if avail arsc then arsc.slsrepout
                                     else v-slsrepout
          oeehb.slsrepin      = if avail arss and v-shipto <> ""
                                  then arss.slsrepin
                                else if avail arsc then arsc.slsrepin
                                     else v-takenby
          oeehb.canceldt      = v-canceldt
          oeehb.user8         = if refreshfl = yes then TODAY else oeehb.user8
          oeehb.placedby      = v-contact
          oeehb.inbndfrtfl    = if avail arss and v-shipto <> ""
                                  then arss.inbndfrtfl
                                else
                                  if avail arsc then arsc.inbndfrtfl
                                  else oeehb.inbndfrtfl
          oeehb.outbndfrtfl   = if avail arss and v-shipto <> ""
                                  then arss.outbndfrtfl
                                else
                                  if avail arsc then arsc.outbndfrtfl
                                  else oeehb.outbndfrtfl
          /*oeehb.totinvamt     =  v-quotetot*/
          v-hcustno           = oeehb.custno. 
  overlay(oeehb.user3,1,20)  = string(v-contact,"x(20)").
  overlay(oeehb.user3,21,12) = string(v-phone,"x(12)").
  overlay(oeehb.user3,33,20) = string(v-email,"x(20)").
        

  find first notes use-index k-notes where
                 notes.cono     = g-cono and               
                 notes.notestype = "ba"                 and
                 notes.primarykey   = string(v-quoteno) and
                 notes.secondarykey = string(1)            
                 no-lock no-error.
  if avail notes then do:
    if notes.requirefl = yes then
      assign oeehb.notesfl = "!".
    else
      assign oeehb.notesfl = "*".
  end.
  find sastc where sastc.cono = g-cono and
                   sastc.currencyty = arsc.currencyty
                   no-lock no-error.
  if avail sastc then
    assign oeehb.user6 = if oeehb.user6 <> 0 then
                           oeehb.user6
                         else
                         if sastc.purchexrate > 0 then
                           1 / sastc.purchexrate
                         else
                           0.
  else
    assign oeehb.user6 = 0.
  run Create_Addons.
{t-all.i "oeehb"}
end. /* Procedure Update_OEEHB */

/* ------------------------------------------------------------------------- */
Procedure Update_OEELB:
/* ------------------------------------------------------------------------- */
  /* Losing sales rep in and out and getting operinits every so often
     so I'm going to refresh the buffers here.  TAH
  */   
  if avail oeehb then 
    do:
    find arsc where arsc.cono = oeehb.cono and
                    arsc.custno = oeehb.custno no-lock no-error. 
  
    if oeehb.shipto <> "" then
      find arss where arss.cono = oeehb.cono and
                      arss.custno = oeehb.custno and
                      arss.shipto  = oeehb.shipto 
                      no-lock no-error.     
    if avail oeelb then
      do:
      run xsdioetlt.p (input-output v-slsrepout,
                       input v-prodcat,
                       input oeehb.custno,
                       input oeehb.shipto).
    end. /* if avail oeelb */
  end. /* if avail oeehb */
  
  run Get_Tax_Flag (input-output v-taxablefl,
                    input-output v-nontaxtype).
    
  if repricefl = yes and oeelb.specnstype <> "n" then 
    do:
    assign oeelb.enterdt     = TODAY
           oeelb.reqshipdt   = if v-reqshipdt <> ? then v-reqshipdt
                               else 
                               if v-shipdt <> ? then v-shipdt
                               else
                               oeehb.reqshipdt
           oeelb.promisedt   = if v-shipdt <> ? then v-shipdt
                               else oeehb.reqshipdt
           oeelb.xxda1       = if manualdtfl = yes then v-shipdt 
                               else oeelb.xxda1
           oeelb.shipprod    = v-prod
           oeelb.price       = v-sellprc 
           oeelb.datccost    = v-surcharge
           oeelb.user6       = if v-sparebamt <> 0 then v-sparebamt
                               else oeelb.user6
           oeelb.priceoverfl = if oeelb.priceoverfl = yes then
                                 oeelb.priceoverfl else
                               if v-sellprc <> orig-price then
                                 yes else no
           oeelb.pricetype   = if v-specnstype = "n" then v-ptype
                               else 
                                 if avail icsw then icsw.pricetype
                                 else ""
           oeelb.specnstype  = if v-specnstype = "" then " " else
                                 v-specnstype
           oeelb.usagefl     = if v-specnstype = "n" and v-kitfl = no then no 
                               else if avail icsp and icsp.statustype = "l"
                               then no
                               else yes
           oeelb.rushfl      = v-rushfl
           oeelb.costoverfl  = if oeelb.costoverfl = no then v-costoverfl
                               else oeelb.costoverfl
           oeelb.qtyord      = dec(v-qty)
           oeelb.reqprod     = v-reqprod
           oeelb.xrefprodty  = if v-xrefty <> "" then 
                                 v-xrefty 
                               else if v-reqprod <> "" then "c"
                               else oeelb.xrefprodty
           oeelb.unit        = if avail icsp and icsp.unitsell <> "" then
                                 icsp.unitsell 
                               else
                               if avail icsp and icsp.unitstock <> "" then
                                 icsp.unitstock
                               else if v-cunit <> "" then
                                 v-cunit
                               else 
                                 "EACH"
           oeelb.leadtm      = v-leadtm
           oeelb.commentfl   = if can-find
                              (first com use-index k-com where
                              com.cono     = g-cono       
                          and com.comtype  = "ob" + string(w-batchnm,">>>>>>>9")
                          and com.lineno = v-lineno
                              no-lock)
                              then yes else no
           oeelb.icspecrecno = if zi-icspecrecno = 0 then 1 else zi-icspecrecno
           oeelb.prodcat     = if v-prodcat <> "" then v-prodcat
                               else if avail icsp then icsp.prodcat
                               else " "
           oeelb.prodline    = if v-prodline <> " " then v-prodline
                               else if avail icsw then icsw.prodline
                               else " "
           oeelb.taxgroup    = if v-conversion then
                                 oeelb.taxgroup
                               else  
                               if avail icsw then 
                                 icsw.taxgroup 
                               else 
                                 1
           oeelb.gststatus   = if avail icsw then icsw.gststatus else true
           oeelb.netamt      = v-totnet
           oeelb.user1       = if oeelb.costoverfl = yes and 
                               ((over-cost >= 0 and v-specnstype = "n" and
                                v-kitfl = false) or over-cost > 0) then
                                 "y" + string(over-cost)
                               else
                                 if oeelb.costoverfl = no and v-specnstype = "n"
                                   and v-kitfl = false
                                   then "y" + string(over-cost)
                                 else
                                   oeelb.user1
           oeelb.xxc3        = v-whse.

    overlay(oeelb.user4,1,4)   = v-whse.
    overlay(oeelb.user4,5,7)   = string(v-gp,">>9.99-").
    overlay(oeelb.user4,12,13) = string(v-listprc,">>>>>>9.99999").
    overlay(oeelb.user4,25,1)  = v-listfl.
    overlay(oeelb.user4,26,8)  = if v-shipdt = ? then "?       "
                                 else string(v-shipdt,"99/99/99").
    overlay(oeelb.user4,34,7)  = string(v-disc,">>9.99-").
    /*overlay(oeelb.user4,40,1) = if v-disctype = no then "%" else "$".*/
    overlay(oeelb.user4,41,1)  = zi-qtybrkty.
    overlay(oeelb.user4,42,8)  = string(v-pdrecord,">>>>>>>9").
    overlay(oeelb.user4,50,1)  = string(v-pdtype,"9").
    overlay(oeelb.user4,51,10) = string(v-pdamt,">>>>9.9999").
    overlay(oeelb.user4,61,1)  = v-pd$md.
    /* was >>>,>>9.99- */
    overlay(oeelb.user4,62,11) = string(v-totnet,">>>>>>9.99-").
    /*overlay(oeelb.user4,74,1)  = v-lcomment.*/ /*don't updt on reprice*/
    overlay(oeelb.user4,75,4)  = v-pdbased.
    /* was >>>,>>9.99- */
    overlay(oeelb.user3,44,11) = string(v-totcost,">>>>>>9.99-").
    overlay(oeelb.user3,55,1)  = if v-rebatefl <> " " then v-rebatefl
                                 else substr(oeelb.user3,55,1).
    /* was >>>,>>9.99- */
    overlay(oeelb.user3,56,11) = string(v-totmarg,">>>>>>9.99-").
    overlay(oeelb.user3,67,11) = string(v-margpct,">>>>>>9.99-").
    overlay(oeelb.user3,78,1)  = v-kitrollty.
    overlay(oeelb.user5,30,24) = v-xprod.
    /*  Don't update user5. We don't know what the value of v-kitfl is 
    assign  overlay(oeelb.user5,1,1) = if v-kitfl = yes then "y" else " ".
    */
    overlay(oeelb.user10,1,1)  = v-NR.  

    /* EN-437-Override LT Days */   
    if OD-date <> ? then
      do:
      overlay(oeelb.user12,10,4) = if v-OLTDays = 0 and   /*Ovr LT Days*/
                                      int(substr(oeelb.user12,10,4)) > 0 then
                                     substr(oeelb.user12,10,4)
                                   else   
                                     string(v-OLTDays,"->>9").
     if substr(oeelb.user12,1,8) = " " then 
       overlay(oeelb.user12,1,8)  = string(OD-date,"99/99/99"). /*OriginalDate*/
    end.
    assign  oeelb.ordertype    = if not v-conversion then
                                   " "
                                 else
                                   oeelb.ordertype.
            /*                   
            v-oeelbid         = recid(oeelb).
            */
            /*
            oeelb.slsrepin    = if v-conversion then
                                  oeelb.slsrepin
                                else  
                                if avail arss and v-shipto <> ""
                                  then arss.slsrepin
                                else if avail arsc then arsc.slsrepin
                                else g-operinit
            */
   assign
            oeelb.slsrepin    = if v-slsrepin = "" then oeelb.slsrepin
                                else v-slsrepin
            oeelb.slsrepout   = if v-slsrepout = "" then oeelb.slsrepout
                                else v-slsrepout
            oeelb.nontaxtype  = if v-conversion then
                                  oeelb.nontaxtype
                                else  
                                  v-nontaxtype.
    /*tb 24060 09/13/99 kjb; Load additional information if processing
       a catalog product */

    
    if not avail icsw then
      find first sc-notes where 
                 sc-notes.cono         = g-cono and
                 sc-notes.notestype    = "ZZ" and
                 sc-notes.primarykey   = "standard cost markup" and
                 sc-notes.secondarykey = oeelb.prodcat and
                 sc-notes.pageno          = 1
                 no-lock no-error.
    if not avail sc-notes then
      find sc-notes use-index k-notes where 
           sc-notes.cono         = g-cono and
           sc-notes.notestype    = "zz" and
           sc-notes.primarykey   = "standard cost markup" and
           sc-notes.secondarykey = "" and
           sc-notes.pageno       = 1
           no-lock no-error.

    assign    oeelb.botype      = if v-conversion then
                                    oeelb.botype
                                  else  
                                    if v-botype = "" then
                                      if v-5603fl = yes then "n"
                                      else
                                        if avail arsc and arsc.bofl = yes
                                          then "y"
                                        else "n"
                                    else
                                      /* das - 06/23/09 */
                                      if avail arsc and arsc.bofl = yes and
                                        v-kitfl = yes then "y"
                                      else v-botype
              oeelb.user2       = if v-glcost <> 0 then 
                                    string(v-glcost,">>>>9.99")
                                  else if avail icsw then 
                                         string(icsw.avgcost,">>>>9.99")
                                       else string(0,">>>>9.99")
              oeelb.prodcost    = if v-prodcost <> 0 then v-prodcost 
                                  else
                                    if avail icsw then 
                                       icsw.stndcost
                                    else if avail sc-notes then 
                                         ((1 + (dec(sc-notes.noteln[1]) / 100))
                                            * v-prodcost)
                                       else
                                        0
              oeelb.vendno      = if v-conversion then
                                     oeelb.vendno
                                  else  
                                  if avail icsw then icsw.arpvendno
                                  else if v-vendno <> 0 then v-vendno
                                  else 0
              oeelb.arpvendno    = if oeelb.specnstype <> " " then oeelb.vendno
                                   else oeelb.arpvendno
              oeelb.taxablefl   = if v-conversion then
                                     oeelb.taxablefl
                                   else
                                     v-taxablefl
              oeelb.printpricefl = yes
              oeelb.subtotalfl   = no
              oeelb.proddesc2    = if avail icsp and icsp.descrip[2] <> ""
                                     then icsp.descrip[2]
                                   else
                                     if v-descrip2 <> "" then v-descrip2
                                     else oeelb.proddesc2.
    if substr(oeelb.user1,1,2) = "y0" and dec(oeelb.user2) > 0 then
      overlay(oeelb.user1,2,7) = left-trim(oeelb.user2).
      
    {t-all.i "oeelb"}
    /* fix misc issues with xrefprodty */
    if (oeelb.xrefprodty = " " or oeelb.xrefprodty = ""
                               or oeelb.xrefprodty = "g") and 
      oeelb.reqprod <> " " then
      assign oeelb.xrefprodty = "c".
  end. /* repricefl = yes and oeelb.specnstype <> "n" */
  else
   do:  
   assign  oeelb.cono        = g-cono
           oeelb.batchnm     = oeehb.batchnm
           oeelb.seqno       = oeehb.seqno
           oeelb.user7       = oeehb.custno 
           oeelb.enterdt     = TODAY
           oeelb.reqshipdt   = if v-reqshipdt <> ? then v-reqshipdt
                               else
                               if v-shipdt <> ? then v-shipdt
                               else 
                               oeehb.reqshipdt
           oeelb.promisedt   = if v-shipdt <> ? then v-shipdt
                               else oeehb.reqshipdt
           oeelb.xxda1       = if manualdtfl = yes then v-shipdt 
                               else oeelb.xxda1
           /*oeelb.lineno      = integer(v-lineno)*/
           oeelb.shipprod    = v-prod
           oeelb.proddesc    = if avail icsp then icsp.descrip[1]
                               else 
                                  if v-descrip <> "" then v-descrip
                                  else ""
           oeelb.proddesc2   = if avail icsp and icsp.descrip[2] <> "" 
                                 then icsp.descrip[2]
                               else if v-descrip2 <> "" then v-descrip2
                                    else oeelb.proddesc2
           oeelb.price       = if manual-change = yes then v-sellprc
                               else oeelb.price
           oeelb.datccost    = v-surcharge  
           oeelb.user6       = if v-sparebamt <> 0 then v-sparebamt
                               else oeelb.user6
           oeelb.priceoverfl = if oeelb.priceoverfl = yes then
                                 oeelb.priceoverfl else
                               if v-sellprc <> orig-price then
                                 yes else no
           oeelb.pricetype   = if v-specnstype = "n" then v-ptype
                               else 
                                  if avail icsw then icsw.pricetype
                                  else ""
           oeelb.specnstype  = if v-specnstype = "" then " " else
                                  v-specnstype
           oeelb.usagefl     = if v-specnstype = "n" and v-kitfl = no then no 
                               else if avail icsp and icsp.statustype = "l"
                                       then no
                                    else yes
           oeelb.rushfl      = v-rushfl
           oeelb.costoverfl  = if oeelb.costoverfl = no then v-costoverfl
                               else oeelb.costoverfl
           oeelb.qtyord      = dec(v-qty)
           oeelb.reqprod     = v-reqprod
           oeelb.xrefprodty  = if v-xrefty <> "" then
                                 v-xrefty
                               else if v-reqprod <> "" then "c"
                               else oeelb.xrefprodty
           oeelb.unit        = if avail icsp and icsp.unitsell <> "" then
                                  icsp.unitsell 
                               else
                               if avail icsp and icsp.unitstock <> "" then
                                  icsp.unitstock
                               else if v-cunit <> "" then
                                  v-cunit
                               else
                                  "EACH"
           oeelb.leadtm      = v-leadtm
           oeelb.commentfl   = if can-find
                           (first com use-index k-com where
                            com.cono     = g-cono         and
                            com.comtype  = "ob" + string(w-batchnm,">>>>>>>9")
                            and com.lineno = v-lineno
                            no-lock)
                            then yes else no
           oeelb.prodcat     = if v-prodcat <> "" then v-prodcat
                               else if avail icsp then icsp.prodcat
                               else " "
           oeelb.prodline    = if v-prodline <> " " then v-prodline
                               else if avail icsw then icsw.prodline
                               else " "
           oeelb.taxgroup    = if v-conversion then
                                 oeelb.taxgroup
                               else
                               if avail icsw then icsw.taxgroup else 1
           oeelb.gststatus   = if avail icsw then icsw.gststatus else true
           oeelb.netamt      = v-totnet
           oeelb.user1       = if oeelb.costoverfl = yes and 
                               ((over-cost >= 0 and v-specnstype = "n" and
                                v-kitfl = false) or over-cost > 0) then
                                 "y" + string(over-cost)
                               else
                                 if oeelb.costoverfl = no and v-specnstype = "n"
                                   and v-kitfl = false
                                   then "y" + string(over-cost)
                                 else
                                   oeelb.user1
           oeelb.xxc3         = v-whse.
   overlay(oeelb.user4,1,4)   = v-whse.
   overlay(oeelb.user4,5,7)   = if v-gp <> ? then string(v-gp,">>9.99-")
                                else string(0,">>9.99-").
   overlay(oeelb.user4,12,13) = string(v-listprc,">>>>>>9.99999").
   overlay(oeelb.user4,25,1)  = v-listfl.
   overlay(oeelb.user4,26,8)  = if v-shipdt = ? then "?       "
                                else string(v-shipdt,"99/99/99").
   overlay(oeelb.user4,34,7)  = string(v-disc,">>9.99-").
   /*overlay(oeelb.user4,40,1)  = if v-disctype = no then "%" else "$".*/
   overlay(oeelb.user4,41,1)  = zi-qtybrkty.
   overlay(oeelb.user4,42,8)  = string(v-pdrecord,">>>>>>>9").
   overlay(oeelb.user4,50,1)  = string(v-pdtype,"9").
   overlay(oeelb.user4,51,10) = string(v-pdamt,">>>>9.9999").
   overlay(oeelb.user4,61,1)  = v-pd$md.
   /* was >>>,>>9.99- */
   overlay(oeelb.user4,62,11) = string(v-totnet,">>>>>>9.99-").
   overlay(oeelb.user4,74,1)  = v-lcomment.
   overlay(oeelb.user4,75,4)  = v-pdbased.
   overlay(oeelb.user5,30,24) = v-xprod.
   /* was >>>,>>9.99- */
   overlay(oeelb.user3,44,11) = string(v-totcost,">>>>>>9.99-").
   overlay(oeelb.user3,55,1)  = if v-rebatefl <> " " then v-rebatefl
                                else substr(oeelb.user3,55,1).
   /* was >>>,>>9.99- */
   overlay(oeelb.user3,56,11) = string(v-totmarg,">>>>>>9.99-").
   overlay(oeelb.user3,67,11) = string(v-margpct,">>>>>>9.99-").
   overlay(oeelb.user3,78,1)  = if v-kitfl = yes then v-kitrollty else
                                                      substr(oeelb.user3,78,1).
                                              

/*   assign  overlay(oeelb.user5,1,1) = if v-kitfl = yes then "y" else " ". */
    overlay(oeelb.user10,1,1)  = v-NR.   
    /* EN-437-Override LT Days */   
    if OD-date <> ? then
     do:
     overlay(oeelb.user12,10,4) = if v-OLTDays = 0 and   /*Ovr LT Days*/
                                    int(substr(oeelb.user12,10,4)) > 0 then
                                    substr(oeelb.user12,10,4)
                                  else
                                    string(v-OLTDays,"->>9").
     if substr(oeelb.user12,1,8) = " " then 
      overlay(oeelb.user12,1,8) = string(OD-date,"99/99/99"). /*Original Date*/
    end. /* OD-date <> ? */ 
   /* hold the override info if alread overridden */
   overlay(oeelb.user21,1,4)    = if substr(oeelb.user21,1,4) = " " then
                                    zhold-operinit
                                  else
                                    substr(oeelb.user21,1,4).
   overlay(oeelb.user21,6,13)   = if substr(oeelb.user21,6,13) = " " or
                                    dec(substr(oeelb.user21,6,13)) = 0 then
                                    string(zhold-orig-price,">>>>>>9.99999")
                                  else
                                    substr(oeelb.user21,6,13).
   overlay(oeelb.user21,20,13)  = if substr(oeelb.user21,20,13) = " " or
                                    dec(substr(oeelb.user21,20,13)) = 0 then
                                    string(zhold-ovrd-price,">>>>>>9.99999")
                                  else
                                    substr(oeelb.user21,20,13).
   overlay(oeelb.user21,36,8)   = if substr(oeelb.user21,36,8) = " " then
                                    string(TODAY,"99/99/99")
                                  else
                                    substr(oeelb.user21,36,8).
   overlay(oeelb.user21,46,4)   = if substr(oeelb.user21,46,4) = " " then
                                    g-operinits
                                  else
                                    substr(oeelb.user21,46,4).
   assign  oeelb.ordertype    =  if not v-conversion then 
                                   " "
                                 else
                                   oeelb.ordertype.
           /*
           v-oeelbid         = recid(oeelb).
           */
   assign  oeelb.slsrepin    = if v-conversion then
                                 oeelb.slsrepin
                               else 
                                 if v-slsrepin = "" then oeelb.slsrepin
                                 else v-slsrepin
                               /*
                               if avail arss and v-shipto <> ""
                                 then arss.slsrepin
                               else if avail arsc then arsc.slsrepin
                               else g-operinit
                               */
           oeelb.slsrepout   = if v-conversion then
                                 oeelb.slsrepout
                               else  
                               if v-slsrepout = "" then oeelb.slsrepout
                               else v-slsrepout                    
           oeelb.nontaxtype  = if v-conversion then
                                 oeelb.nontaxtype
                               else  
                                 v-nontaxtype.
   /*tb 24060 09/13/99 kjb; Load additional information if processing
        a catalog product */
   
   /*
   if p-catfl and v-icscfl then do for sasc:
                   {sasc.gfi &lock = "no"}
   */
      
      if not avail icsw then
         find first sc-notes where 
                    sc-notes.cono         = g-cono and
                    sc-notes.notestype    = "ZZ" and
                    sc-notes.primarykey   = "standard cost markup" and
                    sc-notes.secondarykey = oeelb.prodcat and
                    notes.pageno          = 1
                    no-lock no-error.
         if not avail sc-notes then
           find sc-notes use-index k-notes where 
                sc-notes.cono         = g-cono and
                sc-notes.notestype    = "zz" and
                sc-notes.primarykey   = "standard cost markup" and
                sc-notes.secondarykey = "" and
                sc-notes.pageno       = 1
                no-lock no-error.
   assign    oeelb.botype       = if v-conversion then 
                                    oeelb.botype
                                  else  
                                  if v-botype = "" then
                                    if v-5603fl = yes then "n"
                                    else
                                      if avail arsc and arsc.bofl = yes
                                        then "y"
                                      else "n"
                                  else
                                    /* das - 06/23/09 */
                                    if avail arsc and arsc.bofl = yes and
                                      v-kitfl = yes then "y"
                                    else
                                      v-botype
             oeelb.user2       = if v-glcost <> 0 then 
                                   string(v-glcost,">>>>9.99")
                                  else if avail icsw then 
                                         string(icsw.avgcost,">>>>9.99")
                                       else string(0,">>>>9.99")
             oeelb.prodcost     = if v-prodcost <> 0 then v-prodcost 
                                  else
                                    if avail icsw then 
                                       icsw.stndcost
                                    else if avail sc-notes then 
                                         ((1 + (dec(sc-notes.noteln[1]) / 100))
                                            * v-prodcost)
                                       else
                                        0
             oeelb.vendno       = if v-conversion then
                                    oeelb.vendno
                                  else
                                  if v-vendno <> 0 then v-vendno
                                  else
                                  if avail icsw and 
                                           icsw.prod = oeelb.shipprod then
                                    icsw.arpvendno
                                  else 0
             oeelb.arpvendno    = if oeelb.specnstype <> " " then oeelb.vendno
                                  else oeelb.arpvendno
             oeelb.taxablefl    = if v-conversion then
                                    oeelb.taxablefl
                                  else  
                                    v-taxablefl
             oeelb.printpricefl = yes
             oeelb.subtotalfl   = no.
   if substr(oeelb.user1,1,2) = "y0" and dec(oeelb.user2) > 0 then
      overlay(oeelb.user1,2,7) = left-trim(oeelb.user2).
   
   if substr(oeelb.user21,60,10) = " " then
     do:
     find icsc where icsc.catalog = oeelb.shipprod no-lock no-error.
     if avail icsc then
       do:
       find icsp where icsp.cono     = g-cono and
                       icsp.prod     = oeelb.shipprod and
                       icsp.enterdt  = today and
                       icsp.operinit = g-operinit
                       no-lock no-error.
       if avail icsp then      
         /* user used catalog and created inventory */
         overlay(oeelb.user21,60,10) = "CAT & ICSP". 
       else
         do:
         find icsp where icsp.cono = g-cono and
                         icsp.prod = oeelb.shipprod
                         no-lock no-error.
         if avail icsp then    
           /* user used existing product */
           overlay(oeelb.user21,60,10) = "ICSP EXIST".
         else
           /* user used catalog */
           overlay(oeelb.user21,60,10) = "CAT NONSTK". 
       end.
       overlay(oeelb.user21,70,8) = string(today,"99/99/99").
     end. /* avail icsc */
   end. /* substr(oeelb.user21,60,10) = " " */
   /*
   end. /* if p-catfl and v-icscfl */
   */
   {t-all.i "oeelb"}
   /* fix misc issues with xrefprodty */
   if (oeelb.xrefprodty = " " or oeelb.xrefprodty = ""
                              or oeelb.xrefprodty = "g") and
     oeelb.reqprod <> " " then
     assign oeelb.xrefprodty = "c".
  end.
  
  /* update t-lines */
  find t-lines where t-lines.lineno = oeelb.lineno  and
                     t-lines.seqno  = 0 no-error.      
  assign  t-lines.whse       = substr(oeelb.user4,1,4)
          t-lines.vendno     = oeelb.vendno
          t-lines.slsrepout  = oeelb.slsrepout
          t-lines.slsrepin   = oeelb.slsrepin
          t-lines.prodcat    = oeelb.prodcat
          t-lines.prodline   = oeelb.prodline
          t-lines.listprc    = dec(substr(oeelb.user4,12,13))
          t-lines.listfl     = substr(oeelb.user4,25,1)
          t-lines.pricetype  = oeelb.pricetype
          t-lines.sellprc    = oeelb.price
          t-lines.prodcost   = oeelb.prodcost
          t-lines.glcost     = dec(oeelb.user2)
          t-lines.surcharge  = oeelb.datccost
          t-lines.sparebamt  = oeelb.user6
          t-lines.rebatefl   = substr(oeelb.user3,55,1)
          t-lines.leadtm     = oeelb.leadtm
          t-lines.gp         = dec(substr(oeelb.user4,5,7))
          t-lines.discpct    = dec(substr(oeelb.user4,34,7))
          t-lines.qty        = oeelb.qtyord
          t-lines.shipdt     = /*oeelb.reqshipdt*/ oeelb.promisedt
          t-lines.NR         = substr(oeelb.user10,1,1)
          t-lines.kitfl      = if substr(oeelb.user5,1,1) = "y" then yes 
                               else no
          t-lines.specnstype = oeelb.specnstype
          t-lines.pdscrecno  = int(substr(oeelb.user4,42,8))    
          t-lines.qtybrkty   = substr(oeelb.user4,41,1)
          t-lines.pdtype     = int(substr(oeelb.user4,50,1))
          t-lines.pdamt      = dec(substr(oeelb.user4,51,10))
          t-lines.pd$md      = substr(oeelb.user4,61,1)
          t-lines.pdbased    = substr(oeelb.user4,75,4)
          t-lines.totnet     = dec(substr(oeelb.user4,62,11))
          t-lines.totcost    = dec(substr(oeelb.user3,44,11))
          t-lines.rebatefl   = substr(oeelb.user3,55,1)
          t-lines.totmarg    = dec(substr(oeelb.user3,56,11))
          t-lines.margpct    = dec(substr(oeelb.user3,67,11)).
   /* update t-lines */
   
end. /* procedure Update-OEELB */


/* ------------------------------------------------------------------------- */
Procedure Quote-Total:
/* ------------------------------------------------------------------------- */
   assign v-quotetot  = 0
          v-mgn%cost  = 0
          v-mgn%net   = 0
          v-quotemgn% = 0.
   for each t-oeelb where t-oeelb.cono    = g-cono and
                          t-oeelb.batchnm = string(w-batchnm,">>>>>>>9") and
                          t-oeelb.seqno   = oeehb.seqno and
                          t-oeelb.specnstype <> "L"
                          no-lock:
     assign v-quotetot = v-quotetot + 
                          (ROUND(t-oeelb.price,2) * t-oeelb.qtyord) + 
                          (ROUND(t-oeelb.datccost,2) * t-oeelb.qtyord)
            v-mgn%net  = v-mgn%net  + ROUND(t-oeelb.price,2) * t-oeelb.qtyord
            v-mgn%cost = 
              v-mgn%cost + ((ROUND(t-oeelb.prodcost,2) - ROUND(t-oeelb.user6,2))
                         * t-oeelb.qtyord).
   end. 
   if v-mgn%net > 0 then
      assign v-quotemgn% = 
             ROUND(((v-mgn%net - v-mgn%cost) / v-mgn%net) * 100,3).
   else
      assign v-quotemgn% = v-mgn%net - v-mgn%cost.
   if v-quotemgn% >  999999.99 then assign v-quotemgn% =  999999.99.
   if v-quotemgn% < -999999.99 then assign v-quotemgn% = -999999.99.
/* %toms% */

   if not v-conversion then
/* %toms% */

   display v-quotetot 
           v-quotemgn%
           with frame f-top1.
   if v-quotetot <> 0 then do transaction:
     find oeehb where oeehb.cono = g-cono and
                      oeehb.batchnm = string(w-batchnm,">>>>>>>9") and 
                      oeehb.sourcepros = "Quote"
                      no-error.

     if avail oeehb then
       do:
       if oeehb.whse <> " " then
         do:
          /* %Tom 08/03 make sure oeehb is recent */
         assign  d-whse              = oeehb.whse
                 oeehb.shiptonm      = if v-shiptonm <> "" then
                                        v-shiptonm 
                                      else
                                        oeehb.shiptonm
                 oeehb.shiptoaddr[1] = if v-shiptoaddr1 <> "" then
                                         v-shiptoaddr1
                                       else
                                         oeehb.shiptoaddr[1]
                 oeehb.shiptoaddr[2] = if v-shiptoaddr2 <> "" then
                                         v-shiptoaddr2 
                                       else
                                         oeehb.shiptoaddr[2]
                 oeehb.shiptocity    = if v-shiptocity <> "" then
                                         v-shiptocity
                                       else
                                         oeehb.shiptocity
                 oeehb.shiptozip     = if v-shiptozip <> "" then
                                         v-shiptozip
                                       else
                                         oeehb.shiptozip
                 oeehb.geocd         = if v-shiptogeocd <> 0 then
                                         v-shiptogeocd 
                                       else
                                         oeehb.geocd
                v-shiptonm    = if v-shiptonm    = " " then oeehb.shiptonm
                                  else v-shiptonm
                v-shiptoaddr1 = if v-shiptoaddr1 = " " then 
                                  oeehb.shiptoaddr[1]
                                else v-shiptoaddr1
                v-shiptoaddr2 = if v-shiptoaddr2 = " " then 
                                  oeehb.shiptoaddr[2]
                                else v-shiptoaddr2
                v-shiptocity  = if v-shiptocity = " " then 
                                  CAPS(oeehb.shiptocity)
                                else v-shiptocity
                v-shiptost    = if v-shiptost    = " " then 
                                  CAPS(oeehb.shiptost)
                                else v-shiptost
                v-shiptozip   = if v-shiptozip   = " " then 
                                  oeehb.shiptozip
                                else v-shiptozip
                v-shiptogeocd = if v-shiptogeocd = 0 then
                                  oeehb.geocd
                                 else v-shiptogeocd.
          /*
          if not v-conversion then
            run oeebtd.p("oeehb.",recid(oeehb),?,yes,v-shipfl).
          else
            run oeebtd.p("oeehb.",recid(oeehb),?,no,v-shipfl).
          */  
         /* oeebtd.p is changing the shipto info. Change back if needed */
        
         /* %Tom 08/03 make sure oeehb buffer is the new one  */
         
         /****** das - 08/22/08
         find oeehb where oeehb.cono = g-cono and
                          oeehb.batchnm = string(w-batchnm,">>>>>>>9") and 
                          oeehb.sourcepros = "Quote"
                          no-error.
/* the warehouse was getting zapped as well */
           
         if oeehb.whse <> d-whse then
           assign oeehb.whse          = d-whse
                  v-whse              = d-whse.
         assign oeehb.approvty = "y".
         if oeehb.shiptonm      <> v-shiptonm then
           assign oeehb.shiptonm      = v-shiptonm.
         if oeehb.shiptoaddr[1] <> v-shiptoaddr1 then
           assign oeehb.shiptoaddr[1] = v-shiptoaddr1.
         if oeehb.shiptoaddr[2] <> v-shiptoaddr2 then
           assign oeehb.shiptoaddr[2] = v-shiptoaddr2.
         if oeehb.shiptocity    <> v-shiptocity then
           assign oeehb.shiptocity    = v-shiptocity.
         if oeehb.shiptozip     <> v-shiptozip then
           assign oeehb.shiptozip     = v-shiptozip.
         if oeehb.shiptost      <> v-shiptost  then
           assign oeehb.shiptost      = v-shiptost.

           
         if oeehb.geocd         <> v-shiptogeocd then
           assign oeehb.geocd         = v-shiptogeocd.
         *********/
         assign v-idoeeh  = recid(oeehb)
                v-idoeeh0 = ?
                g-secure  = v-secure
                /*g-whse    = oeehb.whse*/
                g-custno  = oeehb.custno
                g-orderno = int(w-batchnm).
         run oeebti.p.
       end.

       assign oeehb.totinvamt = v-quotetot
              oeehb.user7     = v-quotemgn%.
    end.
  end.
end. /* procedure Quote-Total */


/* ------------------------------------------------------------------------- */
Procedure Find_Whse: 
/* ------------------------------------------------------------------------- */
  def input        parameter x-prodtype  as c format "x(4)"      no-undo.
/* once the warehouse is establised it should stick
   so this is defining the buffer to check that out  %TAH */
  def buffer fw-oeehb for oeehb.
  find fw-oeehb where fw-oeehb.cono = g-cono and
                      fw-oeehb.batchnm = string(w-batchnm,">>>>>>>9") and 
                      fw-oeehb.sourcepros = "Quote"
                      no-lock no-error.

  if avail fw-oeehb then do:
    find icsd where icsd.cono    = g-cono and
                    icsd.whse    = fw-oeehb.whse and
                    icsd.salesfl = yes
                    no-lock no-error.
    if avail icsd then
      do:
      assign v-whse = if x-prodtype = "comp" then h-whse else fw-oeehb.whse
             g-whse = if x-prodtype = "comp" then h-whse else fw-oeehb.whse
             v-specnstype = if v-specnstype <> "n" then " " else v-specnstype
             v-statustype = " ".
      run Get-Whse-Info (input v-prod,
                         input v-whse,
                         input v-qty,
                         input-output v-shipdt,
                         input-output v-specnstype,
                         input-output v-ptype,
                         input-output v-foundfl,
                         input-output v-statustype,
                         input-output v-prodline,
                         input-output v-rarrnotesfl).
    end. /* if avail icsd */
    
/* once the warehouse is establised it should stick
   so this is defining the buffer to check that out  %TAH */

    if x-prodtype <> "comp" and fw-oeehb.whse <> "" and
       v-whse = "" then
      do:
      assign v-whse  = fw-oeehb.whse
             g-whse  = fw-oeehb.whse
             v-specnstype = if v-specnstype <> "n" then " " else v-specnstype
             v-statustype = " ".
      run Get-Whse-Info (input v-prod,
                         input v-whse,
                         input v-qty,
                         input-output v-shipdt,
                         input-output v-specnstype,
                         input-output v-ptype,
                         input-output v-foundfl,
                         input-output v-statustype,
                         input-output v-prodline,
                         input-output v-rarrnotesfl).

    end. /*if x-prodtype <> "comp" and fw-oeehb.whse <> "" */
    else if v-whse = " " then
      do:
      for each icsw where 
               icsw.cono = g-cono and
               icsw.prod = v-prod and
        substr(icsw.whse,1,1) <> "c" and
               /*icsw.statustype <> "x" and  PER TAMI, IGNORE STATUS */
               icsw.qtyonhand - (icsw.qtycommit + icsw.qtyreservd) > 0
               no-lock:
        find icsd where icsd.cono    = g-cono and
                        icsd.whse    = icsw.whse and
                        icsd.salesfl = yes and
                        icsd.custno  = 0
                        no-lock no-error.
        if avail icsd then 
          do:
          assign v-whse = if x-prodtype = "comp" then h-whse else icsd.whse
                 g-whse = if x-prodtype = "comp" then h-whse else icsd.whse
                 v-specnstype = if icsw.statustype = "o" and 
                                  v-specnstype <> "n" then "s"
                                else v-specnstype
                 x-qtyavail = 
                       icsw.qtyonhand - icsw.qtyreserv - icsw.qtycommit
                 v-vendno = icsw.arpvendno
                 v-ptype  = icsw.pricetype.
          if v-shipdt = ? then
            run Assign_LineDate (input v-lmode,
                                 input repricefl,
                                 input v-whse,
                                 input x-qtyavail,
                                 input v-qty,
                                 input icsw.prod,
                                 input icsw.statustype,
                                 input icsw.qtyreserv,
                                 input icsw.qtycommit,
                                 input icsw.avgltdt,
                                 input icsw.leadtmavg,
                                 input icsd.enddaycut,  
                                 input fw-oeehb.divno,
                                 input-output v-shipdt).
         
          assign hold-linedt = v-shipdt.
          leave. /* for each icsw */
        end. /* avail icsd */
        else
          next.
      end. /* each icsw */
      if v-whse = "" then
        do:
        for each icsw where 
                 icsw.cono        = g-cono and
                 icsw.prod        = v-prod and
          substr(icsw.whse,1,1)  <> "c" 
                 /*and icsw.statustype <> "x"   PER TAMI, IGNORE STATUS */
                 no-lock:
          find icsd where icsd.cono    = g-cono and
                          icsd.whse    = icsw.whse and
                          icsd.salesfl = yes and
                          icsd.custno  = 0
                          no-lock no-error.
          if avail icsd then 
            do:
            assign v-whse = if x-prodtype = "comp" then h-whse else icsd.whse
                   g-whse = if x-prodtype = "comp" then h-whse else icsd.whse
                   v-specnstype = if icsw.statustype = "o" and 
                                    v-specnstype <> "n" then "s"
                                  else v-specnstype
                   x-qtyavail = 
                         icsw.qtyonhand - icsw.qtyreserv - icsw.qtycommit
                   v-vendno = icsw.arpvendno
                   v-ptype  = icsw.pricetype.
            if v-shipdt = ? then
              run Assign_LineDate (input v-lmode,
                                   input repricefl,
                                   input v-whse,
                                   input x-qtyavail,
                                   input v-qty,
                                   input icsw.prod,
                                   input icsw.statustype,
                                   input icsw.qtyreserv,
                                   input icsw.qtycommit,
                                   input icsw.avgltdt,
                                   input icsw.leadtmavg,
                                   input icsd.enddaycut,    
                                   input fw-oeehb.divno,
                                   input-output v-shipdt).
         
            assign hold-linedt = v-shipdt.
            leave. /* for each icsw */
          end. /* avail icsd */
          else
            next.
        end. /* each icsw */
      end. /* if whse = "" */
    end. /* if v-whse = "" */
  end. /* if avail fw-oeehb */
  assign o-whse = v-whse.
  display v-whse with frame f-mid1.
  if v-statustype = "x" and v-5603fl = no then 
    do:
    find first icsec where icsec.cono = g-cono and     
                           icsec.rectype = "c" and     
                           icsec.custno  = oeehb.custno and
                           icsec.prod = v-prod
                           no-lock
                           no-error.
    if not avail icsec then
      do:
      if v-specnstype = "n" and v-kitfl = yes then
        assign v-5603fl = yes.
      else
        do:
        run warning.p(5603).     
        assign v-5603fl = yes.
      end.
    end. /* not avail icsec */
    else
      message "Customer Cross Reference will be Selected". pause 2.
  end. /* if v-statustype = "x" and v-5603fl = no */
end. /* Procedure Find_Whse */


/* ------------------------------------------------------------------------- */
Procedure Reassign_Lines:
/* ------------------------------------------------------------------------- */
  for each oeelb where oeelb.cono    = g-cono and
                       oeelb.batchnm = string(w-batchnm,">>>>>>>9")
                       no-lock:
     for each oeelk where oeelk.cono      = g-cono and
                          oeelk.orderno   = v-quoteno and
                          oeelk.ordersuf  = 0 and
                          oeelk.lineno    = oeelb.lineno and
                          oeelk.ordertype = "b":
       assign oeelk.custno = v-custno
              oeelk.shipto = v-shipto.
     end.
  end.
end. /* procedure Reassign_Lines */

/* ------------------------------------------------------------------------- */
Procedure Assign-Update-Quote:
/* ------------------------------------------------------------------------- */
  assign v-hmode = "c".
  do transaction:
   if v-quoteno = 0 then do:
      if not avail arsc then
         {w-arsc.i v-custno no-lock}
      if v-shipto <> "" then
        {w-arss.i v-custno v-shipto no-lock}

      find last L-oeehb where L-oeehb.cono = g-cono and
                              L-oeehb.sourcepros = "Quote" and
                              L-oeehb.seqno = 1
                              no-lock no-error.
      if avail L-oeehb then do:
         assign w-batchnm = int(L-oeehb.batchnm) + 1.
      end.
      else
         assign w-batchnm = 1.
      find sasc where sasc.cono = g-cono no-lock no-error.

      create oeehb.
      {oeehb_create.i}
      run Create_Addons.
      release L-oeehb no-error.
      if oeehb.whse <> " " then
        do:
        find icsd where icsd.cono = g-cono and
                        icsd.whse = oeehb.whse and
                        icsd.salesfl = yes
                        no-lock no-error.
        if not avail icsd then
          assign oeehb.whse = " ".
      end.
      if oeehb.shipto <> "" then 
         assign v-shipfl = yes.
      else 
         assign v-shipfl = no.
      
      /******* das - 08/22/08
      if oeehb.whse <> " " then 
        do:
        assign v-idoeehb = recid(oeehb)
               d-whse    = oeehb.whse
               d-divno   = oeehb.divno.
        if not v-conversion then
          do:
          run oeebtd.p("oeehb.",recid(oeehb),?,yes,v-shipfl).
          assign oeehb.stagecd = 0.  /* oeebtd.p is changing stagecd to 1 */
        end.
        else
          run oeebtd.p("oeehb.",recid(oeehb),?,no,v-shipfl).
        
        /* oeebtd.p is changing the shipto info. Change back if needed */
        
        find oeehb where recid(oeehb) = v-idoeehb no-error.

/* The warehouse was getting zapped as well */
        
        if oeehb.whse          <> d-whse then
          assign oeehb.whse          = d-whse
                 v-whse              = d-whse.
        assign oeehb.approvty = "y"  /* quotes are always approved */
               oeehb.divno    = d-divno.
        if oeehb.shiptonm      <> v-shiptonm then
          assign oeehb.shiptonm      = v-shiptonm.
        if oeehb.shiptoaddr[1] <> v-shiptoaddr1 then
          assign oeehb.shiptoaddr[1] = v-shiptoaddr1.
        if oeehb.shiptoaddr[2] <> v-shiptoaddr2 then
          assign oeehb.shiptoaddr[2] = v-shiptoaddr2.
        if oeehb.shiptocity    <> v-shiptocity then
          assign oeehb.shiptocity    = v-shiptocity.
        if oeehb.shiptozip     <> v-shiptozip then
          assign oeehb.shiptozip     = v-shiptozip.
        if oeehb.shiptost      <> v-shiptost  then
          assign oeehb.shiptost      = v-shiptost.
 
        if oeehb.geocd         <> v-shiptogeocd then
          assign oeehb.geocd         = v-shiptogeocd.
      end. /* if oeehb.whse <> " " */
      ********/
      find first notes use-index k-notes where
                 notes.cono     = g-cono and               
                 notes.notestype = "ba"                 and
                 notes.primarykey   = string(v-quoteno) and
                 notes.secondarykey = string(1)            
                 no-lock no-error.
      if avail notes then do:
        if notes.requirefl = yes then
          assign oeehb.notesfl = "!".
        else
          assign oeehb.notesfl = "*".
      end.
      assign v-idoeeh  = recid(oeehb)
             v-idoeeh0 = ?
             g-secure  = v-secure
             g-whse    = oeehb.whse
             g-custno  = oeehb.custno
             g-orderno = int(w-batchnm).
      run oeebti.p.

      assign v-quoteno = int(oeehb.batchnm)
             v-hcustno = oeehb.custno.
      if v-shiptonm <> " " then
         assign oeehb.shiptonm    = v-shiptonm.
      if v-shiptoaddr1 <> " " or (v-shiptonm <> "" and v-shiptoaddr1 = "") then
         assign oeehb.shiptoaddr[1] = v-shiptoaddr1.
      if v-shiptoaddr2 <> " " or (v-shiptonm <> "" and v-shiptoaddr2 = "") then
         assign oeehb.shiptoaddr[2] = v-shiptoaddr2.
      if v-shiptocity <> " " then
         assign oeehb.shiptocity  = CAPS(v-shiptocity).
      if v-shiptost <> " " then
         assign oeehb.shiptost = CAPS(v-shiptost).
      if v-shiptozip <> " " then
         assign oeehb.shiptozip = CAPS(v-shiptozip).
      if v-shiptogeocd <> 0 then
         assign oeehb.geocd = v-shiptogeocd.
   end. /* v-quoteno > 0 */
   else do:  /* update oeehb */
      if v-hcustno = v-custno then do:
         find oeehb where oeehb.cono = g-cono and   
                          oeehb.batchnm = string(w-batchnm,">>>>>>>9") and
                          oeehb.sourcepros = "Quote"
                          no-error.
         if avail oeehb then do:
/* Warehouse is getting zapped when there is not a customer change %TAH */
  
           assign d-whse = oeehb.whse.
           run Update_OEEHB.
           if oeehb.whse = "" then
              assign oeehb.whse = d-whse.
         end.
      end.
      else do:
         /* customer changed - reprice lines */
         find oeehb where oeehb.cono = g-cono and   
                          oeehb.batchnm = string(w-batchnm,">>>>>>>9") and
                          oeehb.sourcepros = "Quote"
                               no-error.
         if avail oeehb then 
           do:
            assign v-hcustlu = v-custlu
                   v-custlu = arsc.lookupnm 
                   v-custno = arsc.custno
                   g-custno = arsc.custno
                   v-custname = substr(arsc.name,1,25)
                   v-slsrepout = arsc.slsrepout
                   o-slsrepout = arsc.slsrepout
                   v-slsrepin  = arsc.slsrepin
                   v-arnotefl  = arsc.notesfl
                   v-shipto    = ""
                   v-contact   = ""
                   v-phone     = ""
                   v-custpo    = arsc.custpo
                   v-notefl    = ""
                   v-canceldt  = today + 60.
/* %toms% */
           if not v-conversion then
/* %toms% */


            display  v-notefl
                     v-custname
                     v-custno
                     v-arnotefl
                     v-compfocus
                     v-takenby
                     v-shipto
                     v-contact
                     v-phone
                     v-custpo
                     v-canceldt
                     v-quotetot
                     v-quotemgn%
                     v-custlu 
            with frame f-top1.
            run Update_OEEHB.
         end.
         run RePrice.
         run Quote-total.
      end. /* Customer changed. Reprice lines */
    end. /* update OEEHB */
  end. /* transaction */
end. /* Procedure Assign-Update-Quote */


/* ------------------------------------------------------------------------- */
Procedure Get-Whse-Info:
/* ------------------------------------------------------------------------- */
  def input        parameter v-prod        like oeel.shipprod    no-undo.
  def input        parameter v-whse        like oeel.whse        no-undo.
  def input        parameter v-qty         like oeel.qtyord      no-undo.
  def input-output parameter v-shipdt      like oeel.promisedt   no-undo.
  def input-output parameter v-specnstype  like oeel.specnstype  no-undo.
  def input-output parameter v-ptype       like icsw.pricetype   no-undo.
  def input-output parameter v-foundfl     as logical            no-undo.
  def input-output parameter v-statustype  like icsw.statustype  no-undo.
  def input-output parameter v-prodline    like icsw.prodline    no-undo.
  def input-output parameter v-rarrnotesfl as logical            no-undo.

  find t-oeehb where 
       t-oeehb.cono       = g-cono and   
       t-oeehb.batchnm    = string(w-batchnm,">>>>>>>9") and
       t-oeehb.seqno      = 1 and
       t-oeehb.sourcepros = "Quote"
       no-lock no-error.

  if avail t-oeehb and t-oeehb.whse <> " "  and v-rarrnotesfl <> yes and
     v-whse = t-oeehb.whse and
     (lastkey = 9 or lastkey = 13 or lastkey = 401) then
    do:
    {w-icsw.i v-prod t-oeehb.whse no-lock "t-"}
    if avail t-icsw then
      do:
      assign v-specnstype = if t-icsw.statustype = "o" and v-specnstype <> "n"
                              then "s"
                            else 
                              if v-specnstype <> "n" and v-specnstype <> "s"
                                then ""
                              else v-specnstype.
    end.
    else do:
      find b-icsp where b-icsp.cono = g-cono and
                        b-icsp.prod = v-prod and
                        b-icsp.kittype = "B"
                        no-lock no-error.
      if not avail b-icsp then
        do:
        run zsdimaincpy.p (input g-cono,
                           input " ",
                           input t-oeehb.whse,
                           input v-prod,
                           input v-lineno,
                           input-output zsdi-confirm).
        if not can-find (first icsw where 
                               icsw.cono = g-cono and
                               icsw.whse = v-whse and
                               icsw.prod = v-prod no-lock) then
          do:
          /*
          run Create_ICSW (input v-whse,
                           input v-prod).
          */
          run oeexqICSW.p.
/* Callmod */                                                                  
          if not can-find (first icsw where
                                 icsw.cono = g-cono and
                                 icsw.whse = v-whse and
                                 icsw.prod = v-prod no-lock) then 
            do:
            /*
            message 
            "Product/Warehouse Not Set Up in Warehouse Products - ICSW (4602)".
            */
            leave.
          end.
/* Callmod */                                                         
        end.
      end. /* not avail b-icsp - kittype of "B" */
    end. /* icsw not avail */
    {w-icsw.i v-prod t-oeehb.whse no-lock "t-"}
    if avail t-icsw then
      do:
      assign v-specnstype = if t-icsw.statustype = "o" and v-specnstype <> "n"
                              then "s"
                            else 
                              if v-specnstype <> "n" and v-specnstype <> "s"
                                then ""
                              else v-specnstype.
    end.
  end.
  else
    do:
    if v-rarrnotesfl <> yes then
      do:
      {w-icsw.i v-prod v-whse no-lock "t-"}
      if avail t-icsw then
        do:
        assign v-specnstype = if t-icsw.statustype = "o" and 
                                v-specnstype <> "n" then "s"
                              else 
                                if v-specnstype <> "n" and v-specnstype <> "s"
                                  then "" 
                                else v-specnstype.
      end.
    end.
  end.

  v-foundfl = no.
  {w-icsw.i v-prod v-whse no-lock}
  if avail icsw then 
    do:
    assign v-foundfl    = yes
           x-qtyavail   = icsw.qtyonhand - icsw.qtyreserv - icsw.qtycommit
           v-ptype      = icsw.pricetype
           v-prodline   = icsw.prodline
           v-leadtm     = icsw.leadtmavg
           v-statustype = icsw.statustype
           v-vendno     = icsw.arpvendno.
    find icsd where icsd.cono = g-cono and icsd.whse = v-whse no-lock no-error.
    if v-shipdt = ? or (v-shipdt <> ? and v-whse <> orig-whse and
                        not v-conversion) then
      run Assign_LineDate (input v-lmode,
                           input repricefl,
                           input v-whse,
                           input x-qtyavail,
                           input v-qty,
                           input icsw.prod,
                           input icsw.statustype,
                           input icsw.qtyreserv,
                           input icsw.qtycommit,
                           input icsw.avgltdt,
                           input icsw.leadtmavg,
                           input icsd.enddaycut,    
                           input t-oeehb.divno,
                           input-output v-shipdt).
    assign hold-linedt = v-shipdt.
    {w-icsp.i v-prod no-lock}
    if avail icsp then
      assign v-prodcat = if v-prodcat = " " then
                           icsp.prodcat
                         else
                           v-prodcat.
  end. /* if avail icsw */
  else
     assign v-foundfl = no.
  if v-statustype = "x" and v-5603fl = no then 
    do:
    find first icsec where icsec.cono = g-cono and     
                           icsec.rectype = "c" and     
                           icsec.custno  = oeehb.custno and
                           icsec.prod = v-prod
                           no-lock
                           no-error.
    if not avail icsec then
      do:
      if v-specnstype = "n" and v-kitfl = yes then
        assign v-5603fl = yes.
      else
        do:
        run warning.p(5603).     
        assign v-5603fl = yes.
      end.
    end. /* not avail icsec */
    else
      message "Customer Cross Reference will be Selected". pause 2.
  end.  /* status is x */
end. /* procedure Get-Whse-Info */


/* ------------------------------------------------------------------------- */
Procedure Refresh_NEW_Quote:
/* ------------------------------------------------------------------------- */
  run Build_Refresh.
  assign s-sysrefresh = "Y".
  find first rfresh no-error.
  display with frame f-blankline.
  if avail rfresh then
    do: 
    display with frame f-blankline.
    open query q-rfresh for each rfresh.
    display b-rfresh with frame f-rfresh.
    /*EN-437*/
    display with frame f-refreshdates.
    on cursor-up cursor-up.
    on cursor-down cursor-down.
    F8-Refresh:
      do while true on endkey undo F8-Refresh, leave f8-Refresh:
      display s-pctrefresh with frame f-new-refresh.
      update  s-pctrefresh with frame f-new-refresh
      editing:
        readkey.
        apply lastkey.
        /*
        if lastkey = 307 then /* F7-Refresh Dates */  
          do:
          hide frame f-new-refresh.
          run Build_Date_Refresh.
          run F7-Refresh-Dates.
        end.
        */
        if lastkey = 404 or lastkey = 307 then 
          do:
          hide frame f-new-refresh no-pause.
          hide frame f-new-rfresh no-pause.
          leave F8-Refresh.
        end.
        
        if can-do("501,502,507,508",string(lastkey)) then
          do:
          run MoveRefresh (input lastkey).
        end.

        if lastkey = 13 and s-pctrefresh > 0 then
          do:
          if rfresh.seltype = "X" then
            assign rfresh.seltype  = ""
                   rfresh.fut-sell = rfresh.sav-sell.
          else
            assign rfresh.seltype = "X"
                   rfresh.fut-sell = 
                          rfresh.cur-sell * (1 + (s-pctrefresh / 100)).
          /*
          display b-rfresh with frame f-rfresh.
          next. 
          */
          apply lastkey.
        end.
        
        v-rowid = rowid(rfresh).
        open query q-rfresh for each rfresh.
        display b-rfresh with frame f-rfresh.
        b-rfresh:refreshable = false.
        reposition q-rfresh to rowid(v-rowid).
        /*get next q-rfresh.*/
        b-rfresh:select-row (1).
        b-rfresh:refreshable = true.
        b-rfresh:refresh().
         
        /*if can-do("9,13,401",string(lastkey)) then */
        if can-do("401",string(lastkey)) then
          do:
          run RePrice.
          run Quote-Total.
          close query q-rfresh.
          hide frame f-new-rfresh no-pause.
          hide frame f-new-refresh no-pause. 
          assign v-canceldt = TODAY + 60
                 refreshfl  = yes.
          display v-canceldt with frame f-top1.
          leave F8-Refresh.
        end. /* lastkey is RETURN or GO */
      end. /* editing */
    end. /* F8-Refresh */
    close query q-rfresh.
    hide frame f-new-refresh no-pause.
    hide frame f-rfresh no-pause.
  end. /* if avail rfresh */
  
  if lastkey = 307 then /* F7-Refresh Dates */  
    do: 
    hide frame f-new-refresh.
    run Build_Date_Refresh. 
    run F7-Refresh-Dates.
  end.
    
  
  
  hide frame f-rfresh no-pause. 
  assign v-prod = ""
         g-prod = "".
end. /* Procedure Refresh_NEW_Quote */

/* ------------------------------------------------------------------------- */
Procedure Refresh_Quote:
/* ------------------------------------------------------------------------- */
  run Build_Refresh.
  find first rfresh no-lock no-error.
  if avail rfresh then
    do:
    open query q-rfresh for each rfresh.
    display b-rfresh with frame f-rfresh.
    on cursor-up cursor-up.
    on cursor-down cursor-down.
    F8-Refresh:
      do while true on endkey undo F8-Refresh, leave f8-Refresh:
      display s-verify with frame f-refresh.
      update s-verify with frame f-refresh
      editing:
        readkey.
        apply lastkey.
        /*if lastkey =  */
        if lastkey = 404 then 
          do:
          hide frame f-refresh no-pause.
          hide frame f-rfresh no-pause.
          leave F8-Refresh.
        end.
        if can-do("501,502,507,508",string(lastkey)) then
          do:
          run MoveRefresh (input lastkey).
        end.
        v-rowid = rowid(rfresh).
        open query q-rfresh for each rfresh.
        display b-rfresh with frame f-rfresh.
        b-rfresh:refreshable = false.
        reposition q-rfresh to rowid(v-rowid).
        /*get next q-rfresh.*/
        b-rfresh:select-row (1).
        b-rfresh:refreshable = true.
        b-rfresh:refresh().
         
        if can-do("9,13,401",string(lastkey)) then 
          do:
          if input s-verify = yes then 
            do:
            run RePrice.
            run Quote-Total.
            close query q-rfresh.
            hide frame f-rfresh no-pause.
            hide frame f-refresh no-pause. 
            assign v-canceldt = TODAY + 60
                   refreshfl  = yes.
            display v-canceldt with frame f-top1.
            leave F8-Refresh.
          end.
          else 
            do:
            close query q-rfresh.
            hide frame f-refresh no-pause.
            hide frame f-rfresh no-pause.
            leave f8-Refresh.
          end.
        end. /* lastkey is RETURN or GO */
      end. /* editing */
    end. /* F8-Refresh */
  end. /* if avail rfresh */
  hide frame f-rfresh no-pause.
end. /* Procedure Refresh_Quote */




/* ------------------------------------------------------------------------- */
Procedure Build_Refresh:
/* ------------------------------------------------------------------------- */

  for each rfresh:
    delete rfresh.
  end.
  for each t-lines where t-lines.seqno = 0 no-lock:
    if t-lines.specnstype <> "n" and t-lines.specnstype <> "l" then
      do:
      assign zi-displaylinefl = false
             v-whse           = t-lines.whse
             v-qty            = t-lines.qty
            /* v-custno         = g-custno*/
             v-prod           = t-lines.prod.
      run oeexqpricing.p.   
      if v-rebatefl <> " " and h-rebfl = " " then
        assign h-rebfl = v-rebatefl.
    end. /* t-lines.specnstype <> "n" and t-lines.specnstype <> "l" */
    assign s-netavail = 0.
    {w-icsw.i t-lines.prod t-lines.whse no-lock}
    if avail icsw then 
      do:
      {w-icsp.i icsw.prod no-lock}
      if avail icsp then
        do:
        {p-netavl.i}
        end.
    end.
    create rfresh.
    assign rfresh.seltype    = ""
           rfresh.lineno     = t-lines.lineno
           rfresh.specnstype = t-lines.specnstype
           rfresh.prod       = t-lines.prod
           rfresh.whse       = t-lines.whse
           rfresh.qty        = t-lines.qty
           rfresh.avail-qty  = if t-lines.specnstype = "n" then
                                 t-lines.qty
                               else
                                 s-netavail
           rfresh.cur-sell   = t-lines.sellprc
           rfresh.fut-sell   = if t-lines.specnstype = "n" then
                                 t-lines.sellprc
                               else
                                 v-sellprc
           rfresh.sav-sell   = fut-sell.
    
  end. /* for each t-lines */
end. /* Procedure Build_Refresh */


/* ------------------------------------------------------------------------- */ 
Procedure MoveRefresh: 
/* ------------------------------------------------------------------------- */
  define input parameter ix-lastkey as integer   no-undo.
  enable b-rfresh with frame f-rfresh.
  apply ix-lastkey to b-rfresh in frame f-rfresh.
end.


/* ------------------------------------------------------------------------- */ 
Procedure MoveRefreshDt: 
/* ------------------------------------------------------------------------- */
  define input parameter ix-lastkey as integer   no-undo.
  enable b-dtrfresh with frame f-dtrfresh.
  apply ix-lastkey to b-dtrfresh in frame f-dtrfresh.   
end.


/* ------------------------------------------------------------------------- */
Procedure Setup-Options:
/* ------------------------------------------------------------------------- */
  assign v-prodlutype = g-prodlutype
         v-custlutype = g-custlutype.

  find first OptionMoves no-lock no-error.
  if not avail OptionMoves then 
    do:
    find notes where notes.cono = g-cono and 
                     notes.notestype = "zz" and
                     notes.primarykey = "OneScreenOE" and
                     notes.secondarykey = g-operinits no-error.
    if not avail notes or (avail notes and notes.noteln[1] = " " and
                                           notes.noteln[2] = " " and
                                           notes.noteln[3] = " " and
                                           notes.noteln[4] = " ") then
      do:
      create OptionMoves.
      assign OptionMoves.OptionIndex  = 3
             OptionMoves.Procedure    = "b2"
             OptionMoves.Procname     = "Bottom 2".
     
      create OptionMoves.
      assign OptionMoves.OptionIndex  = 2
             OptionMoves.Procedure    = "m1"
             OptionMoves.Procname     = "Middle 1".
 
      create OptionMoves.
      assign OptionMoves.OptionIndex  = 1
             OptionMoves.Procedure    = "t1"
             OptionMoves.Procname     = "Top 1".
      create OptionMoves.
      assign OptionMoves.OptionIndex  = 5
             OptionMoves.Procedure    = "d1"
             OptionMoves.Procname     = "dummy".
    end.
    else
      do:
      assign v-xKeyMovement1 = notes.noteln[1]
             v-xKeyMovement2 = notes.noteln[2] 
             v-xKeyMovement3 = notes.noteln[3] 
             v-xKeyMovement4 = notes.noteln[4]
             v-inx2 = 0.
      do v-inx = 1 to 4: 
      if notes.noteln[v-inx] <> "" then
        do:
        assign v-inx2 = v-inx2 + 1.
        create OptionMoves.
        assign OptionMoves.OptionIndex  = v-inx2.
        if notes.noteln[v-inx] = "f6" then
          assign OptionMoves.Procedure    = "t1"
                 OptionMoves.Procname     = "Top 1".
        else
        if notes.noteln[v-inx] = "f7" then
          assign OptionMoves.Procedure    = "m1"
                 OptionMoves.Procname     = "Middle 1".
        else
        if notes.noteln[v-inx] = "f10" then
          assign OptionMoves.Procedure    = "b2"
                 OptionMoves.Procname     = "Bottom 2".
      end. /* end if */
    end. /* end do 1 to 4 */
    create OptionMoves.
    assign OptionMoves.OptionIndex  = 5
           OptionMoves.Procedure    = "d1"
           OptionMoves.Procname     = "dummy".
  end. /* else */
end. /* if OptionMoves not avail */

assign v-linebrowseopen = false.  

if v-jumpfl = yes then  
  assign v-framestate = "m1"
         v-CurrentKeyPlace = 2.
else
  assign v-CurrentKeyPlace = 0.
run FindNextFrame (input v-NormalMove,   /* Don't want lastkey to effect */
                   input-output v-CurrentKeyPlace ,
                   input-output v-framestate).

if v-framestate = "" then
  assign v-framestate = "t1"
         v-CurrentKeyPlace = 1.
  
assign v-takenby    = g-operinit
       v-lmode      = "a"  /* "a" for add mode on line "c" for change */
       v-qmode      = "a".
       
v-compfocus:dcolor in frame f-top1 = 0.
v-custno:dcolor in frame f-top1 = 0.
on cursor-up back-tab.
on cursor-down tab.

end. /* Procedure Setup-Options */


/* ------------------------------------------------------------------------- */
Procedure Clear-Frames:
/* ------------------------------------------------------------------------- */
  hide frame f-del no-pause.       
  hide frame f-top1 no-pause.      
  hide frame f-bot2i no-pause.      
  hide frame f-bot2e no-pause.
  hide frame f-statusline no-pause.
  hide frame f-Shiptolu no-pause.  
  hide frame f-zcustinfo no-pause. 
  hide frame f-prodlu no-pause.    
  hide frame f-whses no-pause.     
  hide frame f-lines no-pause.     
  hide frame f-custlu no-pause.    
  hide frame f-mid1 no-pause.
  hide frame f-midk no-pause.
  hide frame f-extend no-pause.
  hide frame f-cnv no-pause.
  hide frame f-statusline1 no-pause.
  hide frame f-statusline2 no-pause.
  hide frame f-statuslinex no-pause.
  hide frame f-refresh no-pause.
  hide frame f-new-refresh no-pause.
  hide frame f-FOlock no-pause.
  hide message no-pause.
end. /* Procedure Clear-Frames */


/* ------------------------------------------------------------------------- */ 
Procedure Initialize-All: 
/* ------------------------------------------------------------------------- */
  
  /* das - deadly embrace logic */
  find oeehb where 
       oeehb.cono = g-cono and  
       oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
       oeehb.sourcepros = "Quote"
       no-error.          
  if avail oeehb then
    do:
    assign oeehb.inprocessfl = no
           oeehb.user2       = " ".
    release oeehb.
  end.
  
  {p-CatalogSwap2.i &prodX = v-prod}
  assign 
     /*g-orderno     = 0
       g-ordersuf    = 0
       g-custno      = 0
       g-shipto      = ""
       g-whse        = ""*/
       
       g-shipto      = ""            /* das - 061909 */
       v-mode        = ""
       w-batchnm     = 0
       v-quoteno     = 0
       v-notefl      = " "
       v-custno      = 0
       v-hcustlu     = " "
       v-hcustno     = 0
       v-arnotefl    = " "
       v-custname    = " "
   /*  v-takenby   */
       v-compfocus   = " "
       v-custlu      = ""
       v-shipto      = ""
       v-contact     = ""
       v-phone       = ""
       v-email       = ""
       v-lphone      = ""
       v-custpo      = ""
       v-canceldt    = ?
       v-currency    = ""
       v-terms       = ""
       v-quotetot    = 0
       v-quotemgn%   = 0

       v-kittingl    = false
       v-specnstype  = " "
       v-prod        = ""
       v-reqprod     = ""
       v-icnotefl    = ""       /**das**/
       v-qty         = 1
       v-whse        = ""
       v-kitfl       = no
       v-sellprc     = 0
       v-matrixed    = " "
       v-shipdt      = ?
       v-reqshipdt   = ?
       v-NR          = " "
       v-gp          = 0
       v-descrip     = ""
       v-descrip2    = ""
       v-xprod       = ""
       v-listprc     = 0
       v-listfl      = "n"
       v-prodcost    = 0
       v-glcost      = 0
       h-prodcost    = 0
       v-stndcost    = 0
       v-disc        = 0
       v-vendno     = 0  
       v-prodcat    = " "
       v-lcomment    = " "
       v-sparebamt  = 0
       v-totcost     = 0
       v-totmarg     = 0
       v-margpct     = 0
       v-pdrecord    = 0
       v-pdtype      = 0
       v-pdamt       = 0
       v-pd$md       = ""
       v-pdbased     = " "
       v-totnet      = 0
       v-ptype       = " "
       v-prodline    = " "
       v-quoteloadfl = no
       v-comments    = " "
       v-shiptonm    = " "
       v-shiptoaddr1 = " "
       v-shiptoaddr2 = " "
       v-shiptocity  = " "
       v-shiptost    = " "
       v-shiptozip   = " "
       v-shiptogeocd = 0
       v-rarrnotesfl = no
       v-5603fl      = no
       v-statustype  = " "
       /*v-slsrepout   = " "*/
       F11-proc      = " "
       cancel-xref   = no
       newprod       = no
       v-xreffl      = yes
       v-xrefty      = ""
       v-loadbrowseline  = no
       v-linebrowsekeyed = no
       v-costoverfl  = no
       v-delmode     = yes
       v-delkmode    = no
       v-lostbusty   = " "
       v-rushfl      = no
       s-mprice      = 0
       s-mcostplus   = 0
       s-mmargin     = 0
       s-mdiscamt    = 0
       s-mdiscpct    = 0
       s-discfl      = no
       h-whse        = ""
       zsdiconfirm   = no
       v-ctrlaovr    = no
       v-hmode       = "a"
       s-mprice      = 0  
       s-mcostplus   = 0  
       s-mmargin     = 0  
       s-mdiscamt    = 0  
       s-mdiscpct    = 0  
       s-discfl      = no
       apr-error     = no
       cataddfl      = "  "
       manual-change = no
       zhold-operinit = " "
       zhold-orig-price = 0
       zhold-ovrd-price = 0
       catalogfl     = false
       v-1019fl      = no
       OD-date       = ?
       v-OLTDays     = 0.       
  for each t-lines:
    delete t-lines.
  end.
end. /* procedure Initialize-All */

/* ------------------------------------------------------------------------- */ 
Procedure Initialize-Line: 
/* ------------------------------------------------------------------------- */
for each zidc where 
         zidc.cono      = g-cono and
         zidc.docnumber = int(v-custno) and
         zidc.docdesc   = v-prod and
         zidc.docttype  = "Puffing" and
         zidc.docuser1 <> " " and
         zidc.docdate   = today:
  delete zidc.
end.
          
{p-CatalogSwap2.i &prodX = v-prod}
assign v-lineno      = /* %toms% */ if not v-conversion then
                                      0
                                    else
                                      v-lineno 
       v-specnstype  = " "
       v-prod        = ""
       v-reqprod     = ""
       v-icnotefl    = ""
       v-qty         = 1
       v-whse        = ""
       /* das - Must check v-kitfl so process continues for kit processing */
       v-kittingl    = if v-kitfl = no then no else v-kittingl
       v-kitfl       = no
       v-sellprc     = 0
       h-sellprc     = 0
       auth-sellprc  = 0
       manual-change = no
       v-matrixed    = " "
       v-shipdt      = ?
       v-reqshipdt   = ?
       v-NR          = " "
       v-gp          = 0
       v-descrip     = " "
       v-descrip2    = " "
       v-xprod       = " "
       v-listprc     = 0
       v-listfl      = "n"
       v-prodcost    = 0
       h-prodcost    = 0
       v-glcost      = 0
       v-stndcost    = 0
       v-vendno      = 0
       v-disc        = 0
       v-prodcat     = " "
       v-lcomment    = " "
       v-sparebamt   = 0
       v-totcost     = 0
       v-totmarg     = 0
       v-margpct     = 0
       v-pdrecord    = 0
       v-pdtype      = 0
       v-pdamt       = 0
       v-pd$md       = ""
       v-pdbased     = " "
       v-totnet      = 0
       v-ptype       = " "
       v-prodline    = " "
       v-leadtm      = 0
       v-rarrnotesfl = no
       v-5603fl      = no
       v-statustype  = " "
       cancel-xref   = no
       newprod       = no
       v-xreffl      = yes
       v-xrefty      = ""
       v-costoverfl  = no
       v-delmode     = yes
       v-delkmode    = no
       v-lostbusty   = " "
       v-surcharge   = 0
       h-surcharge   = 0
       v-chgsrchgfl  = no
       v-taxablefl   = no
       v-slsrepovr   = no
       s-mprice      = 0  
       s-mcostplus   = 0  
       s-mmargin     = 0  
       s-mdiscamt    = 0  
       s-mdiscpct    = 0  
       s-discfl      = no
       zsdiconfirm   = no
       v-ctrlaovr    = no
       ns-descrip    = ""  
       ns-descrip2   = ""
       ns-vendno     =  0  
       ns-prodline   = ""  
       ns-prodcat    = ""  
       ns-listprc    =  0  
       ns-listfl     = "n" 
       ns-prodcost   =  0  
       ns-leadtm     = 30  
       ns-ptype      = ""  
       ns-kitfl      = no  
       /*
       ns-kitrollty  = "C"
       */
       ns-kitrollty  = " "
       orig-whse     = ""
       orig-cost     = 0
       over-cost     = 0
       v-slsrepout   = ""
       v-slsrepin    = ""
       v-rushfl      = no
       s-mprice      = 0  
       s-mcostplus   = 0  
       s-mmargin     = 0  
       s-mdiscamt    = 0  
       s-mdiscpct    = 0  
       s-discfl      = no
       apr-error     = no
       h-rebfl       = " "
       cataddfl      = "  "
       zhold-operinit   = " "
       zhold-orig-price = 0
       zhold-ovrd-price = 0
       catalogfl     = false
       v-1019fl      = no
       OD-date       = ?
       v-OLTDays     = 0.    

find first sasos where sasos.cono = g-cono and
                       sasos.oper2 = g-operinits and
                       sasos.menuproc = "zxt1"
                       no-lock no-error.
if avail sasos and sasos.securcd[16] > 1 then
  assign v-NRline = true.
else
  assign v-NRline = false.
       
end. /* procedure Initialize-Line */


/* ------------------------------------------------------------------------- */ 
Procedure XRef_Check: 
/* ------------------------------------------------------------------------- */
  /* Check for Customer Part Number, Supercedes and Substitutes */
  def input        parameter ix-whse  like icsw.whse                  no-undo.
  def input        parameter ix-seqno like oeel.lineno                no-undo.
  def input-output parameter ix-qty   like icsw.qtyonhand             no-undo.
  def input-output parameter ix-prod  like icsw.prod                  no-undo.
  /**/
  {w-icsp.i ix-prod no-lock}
  /*per Jennifer 11/21/18 */
  /*if avail icsp and icsp.kittype = "B" then return.*/
  
  {w-icsw.i ix-prod ix-whse no-lock}

  if avail icsw then
    assign v-avail = (icsw.qtyonhand - icsw.qtyreservd - icsw.qtycommit)
           v-hprod = ix-prod.
  else
    assign v-avail = 0
           v-hprod = ix-prod.
  /**/
  /*
  find icsp where icsp.cono = g-cono and
                  icsp.prod = ix-prod
                  no-lock no-error.
  */
  assign xtype = "c"
         v-hprod = ix-prod.
         /*v-xreffl = no.*/
  run Check_product_ID(input xtype,
                       input-output ix-prod,
                       input-output v-hprod).
  if ix-prod <> v-hprod then
    do:
    assign v-reqprod = v-hprod
           v-xprod   = v-hprod
           v-xrefty  = xtype
           ix-qty = v-cqty
           v-qty = v-cqty.
    {w-icsp.i ix-prod no-lock}
    {w-icsw.i ix-prod ix-whse no-lock}
    if avail icsw then
      assign v-avail = (icsw.qtyonhand - icsw.qtyreservd - icsw.qtycommit)
             v-prod  = ix-prod.  /* we found the xref, use it */
  end.
  if ix-qty > v-avail and cancel-xref = no and v-xreffl = yes then
    do:
    find first icsec where icsec.cono = g-cono and
               icsec.rectype = "p" and
               icsec.prod = v-prod
               no-lock no-error.
    if avail icsec then
      do:
      if v-avail > 0 then
        do:
        xrefmode:
        do on endkey undo xrefmode, leave xrefmode:
          update v-xreffl label
          "Product Superseded; Do You Want to Choose From Replacement Products"
          with frame f-xref side-labels row 10 centered overlay.
      
          if v-xreffl = yes then 
            do:
            hide frame f-xref no-pause.
            assign xtype = "p"
                   v-hprod = ix-prod.
            run Check_product_ID(input xtype,
                                 input-output ix-prod,
                                 input-output v-hprod).
            if v-hprod <> ix-prod then
              do:
              {w-icsp.i ix-prod no-lock}
              if avail icsp and icsp.statustype <> "I" then
                do:
                  assign v-prod = ix-prod
                         /*ix-qty = 1*/
                         ix-qty = if v-cqty <> 0 then v-cqty else
                         if icsp.sellmult > 0 then
                         icsp.sellmult * (truncate(ix-qty / icsp.sellmult,0) +
                           if (ix-qty / icsp.sellmult) -
                            truncate(ix-qty / icsp.sellmult,0) > 0
                            then 1
                          else 0)
                          else ix-qty
                         v-reqprod = v-hprod
                         v-xrefty  = xtype.
                {w-icsw.i ix-prod ix-whse no-lock}
                if avail icsw then
                  assign v-avail = 
                          (icsw.qtyonhand - icsw.qtyreservd - icsw.qtycommit)
                         v-hprod = ix-prod
                         v-reqprod = v-hprod
                         v-xrefty  = xtype.
                else
                  assign v-avail = 0
                         v-hprod = ix-prod
                         v-reqprod = v-hprod
                         v-xrefty  = xtype.
                if ix-seqno > 0 then
                  display v-prod with frame f-midk.
                else
                  display v-prod with frame f-mid1.
                message "Cross Reference Product Selected For Use".
                pause.
              end. /* avail icsp and not inactive */
              else
                assign ix-prod = v-hprod.
            end. /* cross reference found */
      
            if ix-qty > v-avail then
              do:                   
              assign xtype = "s"
                     v-hprod = ix-prod.
              run Check_product_ID(input xtype,
                                   input-output ix-prod,
                                   input-output v-hprod).
              if v-hprod <> ix-prod then
                do:
                {w-icsp.i ix-prod no-lock}
                if avail icsp then
                  assign /*ix-qty = 1*/
                         ix-qty = if v-cqty <> 0 then v-cqty else
                          if icsp.sellmult > 0 then
                          icsp.sellmult * (truncate(ix-qty / icsp.sellmult,0) +
                          if (ix-qty / icsp.sellmult) -
                           truncate(ix-qty / icsp.sellmult,0) > 0
                           then 1
                          else 0)
                          else ix-qty
                         v-reqprod = v-hprod
                         v-xrefty  = xtype.
              end.
            end.
          end. /* v-xreffl = yes */
        end. /* xrefmode */
      end. /* v-avail 0 - don't display pop-up if qtyavail is <= 0 */
      else
        do:
        assign xtype   = "p"
               v-hprod = ix-prod.
        run Check_product_ID(input xtype,
                             input-output ix-prod,
                             input-output v-hprod).
        if v-hprod <> ix-prod then
          do:
          {w-icsp.i ix-prod no-lock}
          if avail icsp and icsp.statustype <> "I" then
            do:
            assign v-prod = ix-prod
                   /*ix-qty = 1*/   
                   ix-qty = if v-cqty <> 0 then v-cqty else
                       if icsp.sellmult > 0 then
                       icsp.sellmult * (truncate(ix-qty / icsp.sellmult,0) +
                       if (ix-qty / icsp.sellmult) -
                        truncate(ix-qty / icsp.sellmult,0) > 0
                        then 1
                       else 0)
                       else ix-qty
                   v-reqprod = v-hprod
                   v-xrefty  = xtype.

            {w-icsw.i ix-prod ix-whse no-lock}

            if avail icsw then
              assign v-avail = 
                          (icsw.qtyonhand - icsw.qtyreservd - icsw.qtycommit)
                     v-hprod = ix-prod.
            else
              assign v-avail = 0
                     v-hprod = ix-prod.
            if ix-seqno > 0 then
              display v-prod with frame f-midk.
            else
              display v-prod with frame f-mid1.
            message "Cross Reference Product Selected For Use".
            pause.
          end. /* if avail icsp and not inactive */
          else
            assign ix-prod = v-hprod.
        end. /* cross reference found */
        if ix-qty > v-avail then
          do:                   
          assign xtype = "s"
                 v-hprod = ix-prod.
          run Check_product_ID(input xtype,
                               input-output ix-prod,
                               input-output v-hprod).
          if v-hprod <> ix-prod then
            do:
            {w-icsp.i ix-prod no-lock}
            if avail icsp then
               assign /*ix-qty = 1*/
                      ix-qty = if v-cqty <> 0 then v-cqty else
                         if icsp.sellmult > 0 then
                         icsp.sellmult * (truncate(ix-qty / icsp.sellmult,0) +
                         if (ix-qty / icsp.sellmult) -
                          truncate(ix-qty / icsp.sellmult,0) > 0
                          then 1
                         else 0)
                         else ix-qty
                      v-reqprod = v-hprod
                      v-xrefty  = xtype.
          end.
        end.
      end. /* qtyavail is = 0 */
    end. /* Supersede exists */
  end. /* qty > available */

  if ix-qty > v-avail and cancel-xref = no then
    do:
    assign xtype = "s"
           v-hprod = ix-prod.
           /*v-xreffl = yes.*/
    run Check_product_ID(input xtype,
                         input-output ix-prod,
                         input-output v-hprod).
    if v-hprod <> ix-prod then
      do:
      if avail icsp then
        assign /*ix-qty = 1*/
               ix-qty = if v-cqty <> 0 then v-cqty
                          else if icsp.sellmult > 0 then
                          icsp.sellmult * (truncate(ix-qty / icsp.sellmult,0) +
                          if (ix-qty / icsp.sellmult) -
                            truncate(ix-qty / icsp.sellmult,0) > 0
                            then 1
                          else 0)
                          else ix-qty
               v-reqprod = v-hprod
               v-xrefty  = xtype.
    end.
  end.
  if v-hprod = ix-prod and not avail icsp then
    do:
    assign xtype = "m"
           v-hprod = ix-prod.
    run Check_product_ID(input xtype,
                         input-output ix-prod,
                         input-output v-hprod).
    if v-hprod <> ix-prod then
      do:
      message "Crossed By: Vendor Product".
      assign v-reqprod = v-hprod
             v-xrefty  = xtype.
    end.
  end.
  if v-hprod = ix-prod and not avail icsp then
    do:
    assign xtype = "g"
           v-hprod = ix-prod.
    run Check_product_ID(input xtype,
                         input-output ix-prod,
                         input-output v-hprod).
    assign v-xrefty = xtype
           xtype    = " ".
    
    /****
    if v-hprod <> ix-prod then
      do:
      message "Crossed By: Vendor Product".
      assign v-reqprod = v-hprod
             v-xrefty  = xtype.
    end.
    ****/
  end.

  hide frame f-xref no-pause.
  
end. /* procedure XRef_Check */

/* ------------------------------------------------------------------------- */ 
   Procedure XRef_Check_for_Options:
/* ------------------------------------------------------------------------- */
  /* Check for Options */
  def input        parameter ix-whse  like icsw.whse                  no-undo.
  def input        parameter ix-seqno like oeel.lineno                no-undo.
  def input-output parameter ix-qty   like icsw.qtyonhand             no-undo.
  def input-output parameter ix-prod  like icsw.prod                  no-undo.
  run Initialize-Line.

  
  assign xtype = "o"
         v-hprod = ix-prod
         g-ourproc = "oeetl"
         /*v-xreffl = yes.*/
         x-prod = ""
         x-qty  = 0
         x-unit = "".
         v-optprod = ix-prod.   

 /*
   hide frame f-zcustinfo no-pause.
   hide frame f-prodlu no-pause.
   hide frame f-whses no-pause.
   hide frame f-lines no-pause.
   hide frame f-custlu no-pause.
   hide frame f-mid1 no-pause.
   hide frame f-midk no-pause.
   hide frame f-extend no-pause.
   */


/*  
  run oeexqo.p(input v-quoteno,
               input v-seqno,
               input v-lineno,
               input v-kitfl,
               input v-whse).
*/


/* EATON PREMIUM */
    
define buffer b3-icsw for icsw.     
define var o-optprod     as char    no-undo.

find first b-oeehb where b-oeehb.cono = g-cono and
           b-oeehb.batchnm = string(v-quoteno,">>>>>>>9")
           no-lock no-error.
  
if avail b-oeehb then
do:

  find last oeelb where oeelb.cono = g-cono and
                        oeelb.batchnm = b-oeehb.batchnm and
                        oeelb.seqno   = b-oeehb.seqno   
                   /*   oeelb.user6 <> 0        */
                        no-lock no-error.
    
  if avail oeelb then do:  

      assign v-lineno    = oeelb.lineno /* + 1  */
             v-whse      = substr(oeelb.user4,1,4)
             v-slsrepin  = oeelb.slsrepin
             v-slsrepout = oeelb.slsrepout
             v-seqno     = oeelb.seqno.
   

      /* message "oeelb.user5"   substring(oeelb.user5,1,1). pause. */
       
       
       if substr(oeelb.user5,1,1) = "y" then 
          assign var-kitfl = yes. 
       else 
          assign var-kitfl = no. 
    

/*  COMMENT OUT KITS 

     if var-kitfl = yes  then do: 
        
        for each oeelk where oeelk.cono = g-cono and
                              oeelk.ordertype = "b" and
                              oeelk.comptype = "C" and
                              oeelk.orderno   = v-quoteno and
                              oeelk.ordersuf  = 00 and
                              oeelk.lineno    = oeelb.lineno
                              no-lock:
                              
            
            find first b3-icsw where b3-icsw.cono = g-cono and 
                                 b3-icsw.prod = oeelk.shipprod and
                                 b3-icsw.whse = oeelk.whse  
                                 no-lock no-error.
                           
            if avail b3-icsw then do:
                 
             find first pdsr use-index k-contractno where pdsr.cono = g-cono and
                                        pdsr.vendno =   b3-icsw.arpvendno 
                                                     /* oeelk.arpvendno */ and
                                       (pdsr.enddt >= today or 
                                        pdsr.enddt = ?)
                                        no-lock no-error.
                                 
              if avail pdsr then do:
                   
                   
                 find first notes where notes.cono = g-cono and
                            notes.notestype = "zz" and
                            notes.primarykey = "eaton-premium" and
                            notes.secondarykey = b3-icsw.pricetype 
                            no-lock no-error.

                  if avail notes then do: 
                     
                      assign var-eaton = yes 
                             o-optprod = v-optprod
                             v-optprod = "EATON PREMIUM PRODUCT".
                  
                   end.   /* avail notes */
                        
              end. /* avail pdsr */
            
            end. /* b3-icsw */
         end. /* for each */    
        
         /*
         if can-find(first icsec use-index k-icsec where
                              icsec.cono    = g-cono and
                              icsec.rectype = "o"    and
                              icsec.prod    = v-optprod)   
            then do:

                message "v-optprod" v-optprod
                        "are we at this point for kits". pause.

         */

                    
                    run oeexqo.p(input v-quoteno,
                                 input v-seqno,
                                 input v-lineno,
                                 input var-kitfl,
                                 input v-whse,
                                 output var-premiumpr).
                    if var-eaton then do:
                        
                      assign var-eaton = no 
                             v-optprod = "".
                    end.    
                    if lastkey = 404 then
                      assign var-eaton        = no
                             var-premiumpr    = 0.

                
        end.   /* kits */
       
       ELSE do:

        FINISH COMMENT OUT KITS */
        
          
             
            find first b3-icsw where b3-icsw.cono = g-cono and 
                                 b3-icsw.prod = v-optprod and
                                 b3-icsw.whse = g-whse 
                                 no-lock no-error.
                           
            if avail b3-icsw then do:
             /*  
             find first pdsr use-index k-contractno where pdsr.cono = g-cono and
                                        pdsr.vendno = b3-icsw.arpvendno  and
                                       (pdsr.enddt >= today or 
                                        pdsr.enddt = ?)
                                        no-lock no-error.
                                 
              if avail pdsr then do:

              */    
                 find first notes where notes.cono = g-cono and
                            notes.notestype = "zz" and
                            notes.primarykey = "eaton-premium" and
                            notes.secondarykey = b3-icsw.pricetype 
                            no-lock no-error.

                  if avail notes then do:   
                    
                    if oeelb.user6  <> 0 then do:
                     assign var-eaton = yes 
                            o-optprod = v-optprod
                            v-optprod = "EATON PREMIUM PRODUCT".    
                    end. /* user6 */        
                  end. /* notes */

          /*    end. /* avail pdsr */     */
                                   

           end.  /*  b3-icsw */    
           
            run oeexqo.p(input v-quoteno,
                           input v-seqno,
                           input v-lineno,
                           input var-kitfl,
                           input v-whse,
                           output var-premiumpr).
                if var-eaton then do:
                      assign var-eaton = no 
                             v-optprod = "".
                end.
                if lastkey = 404 then
                  assign var-eaton        = no
                         var-premiumpr    = 0.
                
                   
     /*   end. /* not kit */        */
     
    end. /* oeelb */

       
       
       
      do p-inx = 1 to 7:
      if x-prod[p-inx] <> "" then
        do:
        assign v-lineno = v-lineno + 1.
        assign v-prod = x-prod[p-inx]
               v-qty  = x-qty[p-inx]
               v-unit = x-unit[p-inx]
               v-whse = if v-whse = "" then g-whse else v-whse
               zi-displaylinefl = false.
        run oeexqpricing.p. 
        if v-rebatefl <> " " and h-rebfl = " " then
          assign h-rebfl = v-rebatefl.
        if v-shipdt = ? then
          do:
          /* das- did not have icsd when v-whse was changed on the line */
          if not avail icsd then  
            find icsd where icsd.cono = g-cono and
                            icsd.whse = v-whse
                            no-lock no-error.
          run Assign_LineDate (input v-lmode,
                               input repricefl,
                               input v-whse,
                               input x-qtyavail,
                               input v-qty,
                               input icsw.prod,
                               input icsw.statustype,
                               input icsw.qtyreserv,
                               input icsw.qtycommit,
                               input icsw.avgltdt,
                               input icsw.leadtmavg,
                               input icsd.enddaycut,    
                               input b-oeehb.divno,
                               input-output v-shipdt).
        end. /* if v-shipdt = ? */
     
        find t-lines where t-lines.lineno = v-lineno      and
                           t-lines.seqno  = 0 no-error.
        if not avail t-lines then
          do:
          if v-rebatefl = " " and h-rebfl <> " " then
            assign v-rebatefl = h-rebfl.
          create t-lines.
          assign  t-lines.lineno     = v-lineno
                  t-lines.seqno      = 0
                  t-lines.prod       = v-prod
                  t-lines.icnotefl   = v-icnotefl
                  t-lines.descrip    = v-descrip
                  t-lines.descrip2   = v-descrip2
                  t-lines.xprod      = v-xprod
                  t-lines.whse       = v-whse
                  t-lines.vendno     = v-vendno
                  t-lines.slsrepout  = v-slsrepout
                  t-lines.slsrepin   = v-slsrepin
                  t-lines.prodcat    = v-prodcat
                  t-lines.prodline   = v-prodline
                  t-lines.listprc    = v-listprc
                  t-lines.listfl     = v-listfl
/* %toms2% */                                                              
                t-lines.convertfl  = if v-conversion then t-lines.convertfl 
                                     else no
/* %toms2% */
                t-lines.pricetype  = if v-ptype <> " " then v-ptype
                                     else t-lines.pricetype
                t-lines.sellprc    = v-sellprc
                t-lines.prodcost   = v-prodcost
                t-lines.glcost     = v-glcost
                t-lines.surcharge  = v-surcharge
                t-lines.sparebamt  = v-sparebamt
                t-lines.leadtm     = v-leadtm
                t-lines.gp         = if v-gp = ? then 0 else v-gp
                t-lines.discpct    = v-disc
              /*t-lines.disctype   = v-disctype*/
                t-lines.qty        = v-qty
                t-lines.kqty       = v-kqty
                t-lines.shipdt     = v-shipdt
                t-lines.NR         = v-NR
                t-lines.kitfl      = v-kitfl
                t-lines.specnstype = v-specnstype
                t-lines.pdscrecno  = zi-pdscrecno    
                t-lines.qtybrkty   = zi-qtybrkty
                t-lines.pdtype     = zi-level
                t-lines.pdamt      = v-pdamt
                t-lines.pd$md      = v-pd$md
                t-lines.pdbased    = v-pdbased
                t-lines.totnet     = /*zi-totnet*/
                                     v-sellprc * v-qty
                t-lines.lcomment   = v-lcomment
                t-lines.totcost    = /*zi-totcost*/
                                     v-prodcost * v-qty
                t-lines.rebatefl   = if v-rebatefl <> " " then v-rebatefl
                                     else t-lines.rebatefl
                t-lines.totmarg    = /*zi-totmargin*/
                               (v-sellprc - (v-prodcost + v-sparebamt)) * v-qty
                t-lines.margpct    = /*zi-margpct.*/               
                                     if v-sellprc <> 0 then
                                   ((v-sellprc - v-prodcost) / v-sellprc) * 100
                                     else 0.
        end. /* not avail t-lines */
        create oeelb.    
        run Create_OEELB.
      end. /* if x-prod[p-inx] <> "" */
    end. /* do p-inx = 1 to 7 */
    run Quote-Total.
  end. /* if avail b-oeehb */
end. /* Procedure XRef_Check_for_Options */

/* ------------------------------------------------------------------------- */ 
Procedure Roll_Kit_Cost-Price: 
/* ------------------------------------------------------------------------- */
  def input        parameter v-kitrollty as c format "x(1)"          no-undo.
  def input-output parameter v-sellprc   like oeelb.price            no-undo.
  def input-output parameter v-kitcost   like oeelb.prodcost         no-undo.
  def input-output parameter v-gp        as decimal format ">>9.99-" no-undo.

  assign v-kitcost   = 0
         v-kitprice  = 0
         v-kitdate   = 09/09/99
         comp-leadtm = 0.
  for each oeelk where
           oeelk.cono       = g-cono     and
           oeelk.ordertype  = "b"        and
           oeelk.orderno    = v-quoteno  and
           oeelk.ordersuf   = 0          and
           oeelk.lineno     = v-lineno /*and
           oeelk.comptype   = "c"        and
           oeelk.statustype = "a"        and
           oeelk.transtype  = "QU"     */
           no-lock:
    assign v-kitcost  = v-kitcost + (oeelk.prodcost * oeelk.qtyord)
           v-kitprice = if v-kitrollty = "B" then
                          v-kitprice + (oeelk.price * oeelk.qtyord)
                        else
                          0
           v-kitdate = if oeelk.user8 > v-kitdate 
                         then oeelk.user8 
                       else v-kitdate
           comp-leadtm = if comp-leadtm < int(oeelk.user7) then
                           int(oeelk.user7)
                         else
                           comp-leadtm
           v-kitrebamt = oeelk.user6 * oeelk.qtyord.
  end.
  if v-kitcost > 0 or v-kitprice > 0 then
    do transaction:
    find t-oeelb where
         t-oeelb.cono        = g-cono and
         t-oeelb.batchnm     = string(w-batchnm,">>>>>>>9") and
         t-oeelb.lineno      = v-lineno and
         t-oeelb.seqno       = 1
         no-error.
    if avail t-oeelb then
      do:
      assign t-oeelb.prodcost  = v-kitcost
             t-oeelb.promisedt = if t-oeelb.xxda1 = ? then v-kitdate
                                 else t-oeelb.promisedt             
             t-oeelb.netamt    = if v-kitrollty = "B" then v-kitprice
                                 else t-oeelb.netamt
             t-oeelb.price     = if v-kitrollty = "B" then
                                   if t-oeelb.qtyord <> 0 then
                                      v-kitprice / t-oeelb.qtyord
                                   else 0
                                 else
                                   t-oeelb.price
             v-sellprc         = if v-kitrollty = "B" then t-oeelb.price
                                 else v-sellprc
             t-oeelb.leadtm    = comp-leadtm
             t-oeelb.user6     = v-kitrebamt.
      overlay(t-oeelb.user4,26,8)  = if t-oeelb.promisedt = ? then "?       "
                                  else string(t-oeelb.promisedt,"99/99/99").
      /* was >>>,>>9.99- */
      overlay(t-oeelb.user3,44,11) = string(v-kitcost,">>>>>>9.99-").
      overlay(t-oeelb.user3,56,11) = 
          string(((t-oeelb.price - v-kitcost) * t-oeelb.qtyord),">>>>>>9.99-").
      if t-oeelb.price <> 0 then
        do:
        assign x-gp = ((t-oeelb.price - (v-kitcost - v-kitrebamt)) 
                                          / t-oeelb.price) * 100
               x-gp = if x-gp > 999999.99 then 999.99
                      else if x-gp < -999999.99 then -999.99
                           else x-gp.
      end.
      /*
      overlay(t-oeelb.user3,67,11) = if t-oeelb.price <> 0 then
          string((((t-oeelb.price - v-kitcost) / t-oeelb.price) * 100)
                                                       ,">>>,>>9.99-") 
                                     else string(0,">>>,>>9.99-").
      */
      /* was >>>,>>9.99- */
      overlay(t-oeelb.user3,67,11) = string(x-gp,">>>>>>9.99-").
      assign v-gp = dec(substr(t-oeelb.user3,67,11)).
      overlay(t-oeelb.user4,5,7)   = if v-gp <> ? then string(v-gp,">>9.99-")
                                     else string(0,">>9.99-").
    end.
    find xt-lines where xt-lines.lineno = v-lineno  and
                        xt-lines.seqno  = 0
                        no-lock no-error.
    if avail xt-lines then
      do:   
      assign xt-lines.gp = v-gp
             xt-lines.prodcost = v-kitcost
             xt-lines.shipdt   = if avail t-oeelb and t-oeelb.xxda1 = ? then
                                   t-oeelb.promisedt 
                                 else v-kitdate
             xt-lines.totcost  = v-kitcost * xt-lines.qty
             xt-lines.totnet   = if v-kitrollty = "B" then t-oeelb.netamt
                                 else xt-lines.totnet
             xt-lines.sellprc  = if v-kitrollty = "B" then t-oeelb.price 
                                 else xt-lines.sellprc
             xt-lines.totmarg  = round(xt-lines.totnet - xt-lines.totcost,2)
             xt-lines.margpct  = if round(xt-lines.totmarg,2) <> 0 then
                          round(((xt-lines.totmarg / xt-lines.totnet) * 100),2)
                                 else 0.
    end. /* avail xt-lines */
  end. /* v-kitcost > 0 */
end. /* procedure Roll_Kit_Cost-Price */


/* ------------------------------------------------------------------------- */ 
Procedure Line_Browse: 
/* ------------------------------------------------------------------------- */

  find first t-lines no-lock no-error.
  if avail t-lines then
    do:
    v-rowid = rowid(t-lines). 
    open query q-lines for each t-lines.
    assign v-linebrowseopen = true.
    display b-lines with frame f-lines.
    b-lines:refreshable = false.
       
    if not v-conversion then
      do:
      enable b-lines with frame f-lines.
      apply "end" to b-lines in frame f-lines.
      b-lines:select-row (b-lines:num-iterations).
    end.
    
    b-lines:refreshable = true.
    b-lines:refresh(). 
  end.
  else
    open query q-lines for each t-lines.
end. /* procedure Line_Browse */


/********
/* ------------------------------------------------------------------------- */ 
Procedure Delete_Routine: 
/* ------------------------------------------------------------------------- */

end.
********/

/* ------------------------------------------------------------------------- */ 
Procedure Create_OEELB: 
/* ------------------------------------------------------------------------- */
  if avail oeehb then 
    do:
    find arsc where arsc.cono = oeehb.cono and
                    arsc.custno = oeehb.custno no-lock no-error. 
  
    if oeehb.shipto <> "" then
      find arss where arss.cono = oeehb.cono and
                      arss.custno = oeehb.custno and
                      arss.shipto  = oeehb.shipto no-lock no-error. 
  end.
  
  run Get_Tax_Flag (input-output v-taxablefl,
                    input-output v-nontaxtype).
  assign oeelb.cono        = g-cono
         oeelb.user7       = oeehb.custno 
         oeelb.batchnm     = oeehb.batchnm
         oeelb.seqno       = oeehb.seqno
         oeelb.enterdt     = TODAY
         oeelb.reqshipdt   = if v-reqshipdt <> ? then v-reqshipdt
                             else
                             if v-shipdt <> ? then v-shipdt
                             else 
                             oeehb.reqshipdt
         oeelb.promisedt   = if v-shipdt <> ? then v-shipdt
                                else oeehb.reqshipdt
         oeelb.xxda1       = if manualdtfl = yes then v-shipdt 
                             else oeelb.xxda1
         oeelb.lineno      = integer(v-lineno)
         oeelb.shipprod    = v-prod
         oeelb.proddesc    = if avail icsp then icsp.descrip[1]
                             else 
                                if v-descrip <> "" then v-descrip
                                else ""
         oeelb.proddesc2   = if avail icsp and icsp.descrip[2] <> ""
                               then icsp.descrip[2]
                             else
                               if v-descrip2 <> "" then v-descrip2
                               else oeelb.proddesc2
         oeelb.price       = v-sellprc
         oeelb.datccost    = v-surcharge
         oeelb.user6       = if v-sparebamt <> 0 then v-sparebamt
                             else oeelb.user6
         oeelb.priceoverfl = if v-sellprc <> orig-price then yes else no
         oeelb.pricetype   = if v-specnstype = "n" then v-ptype
                             else 
                                if avail icsw then icsw.pricetype
                                else ""
         oeelb.specnstype  = if v-specnstype = "" then 
                               " "
                             else
                               v-specnstype
         oeelb.usagefl     = if v-specnstype = "n" and v-kitfl = no then no 
                                else if avail icsp and icsp.statustype = "l"
                                        then no
                                     else yes
         oeelb.rushfl      = v-rushfl
         oeelb.costoverfl  = v-costoverfl
         oeelb.qtyord      = dec(v-qty)
         oeelb.reqprod     = v-reqprod
         oeelb.xrefprodty  = if v-xrefty <> "" then 
                               v-xrefty 
                             else if v-reqprod <> "" then "c"
                             else oeelb.xrefprodty
         oeelb.unit        = if avail icsp and icsp.unitsell <> "" then
                               icsp.unitsell 
                             else
                             if avail icsp and icsp.unitstock <> "" then
                                icsp.unitstock
                             else if v-cunit <> "" then
                                v-cunit
                             else 
                                "EACH"
         oeelb.leadtm      = v-leadtm
         oeelb.commentfl   = if can-find
                         (first com use-index k-com where
                         com.cono     = g-cono         and
                         com.comtype  = "ob" + string(w-batchnm,">>>>>>>9") and
                         com.lineno   = v-lineno
                         no-lock)
                         then yes else no
         oeelb.icspecrecno = if zi-icspecrecno = 0 then 1 else zi-icspecrecno
         oeelb.prodcat     = if v-prodcat <> "" then v-prodcat
                             else if avail icsp then icsp.prodcat
                             else " "
         oeelb.prodline    = if avail icsw then icsw.prodline
                             else v-prodline
         oeelb.taxgroup    = if avail icsw then icsw.taxgroup else 1
         oeelb.gststatus   = if avail icsw then icsw.gststatus else true
         oeelb.netamt      = zi-totnet
         oeelb.ordertype   = " ".
  assign oeelb.slsrepin    = if v-slsrepin = "" then
                               if avail arss and v-shipto <> ""
                                 then arss.slsrepin
                               else if avail arsc then arsc.slsrepin
                                    else v-slsrepin
                             else
                               v-slsrepin
         oeelb.slsrepout   = v-slsrepout
         oeelb.nontaxtype  = v-nontaxtype
         oeelb.user1       = if v-costoverfl = yes and 
                               ((over-cost >= 0 and v-specnstype = "n" and
                               v-kitfl = false) or over-cost > 0) then
                               "y" + string(over-cost)
                             else
                               if v-costoverfl = no and v-specnstype = "n"
                                 and v-kitfl = false
                                 then "y" + string(over-cost)
                               else
                                 oeelb.user1
         oeelb.xxc3          = v-whse.
  overlay(oeelb.user5,30,24) = v-xprod.
  overlay(oeelb.user4,1,4)   = v-whse.
  overlay(oeelb.user4,5,7)   = string(v-gp,">>9.99-").
  overlay(oeelb.user4,12,13) = string(v-listprc,">>>>>>9.99999").
  overlay(oeelb.user4,25,1)  = v-listfl.
  overlay(oeelb.user4,26,8)  = if v-shipdt = ? then "?       "
                              else string(v-shipdt,"99/99/99").
  overlay(oeelb.user4,34,7)  = string(v-disc,">>9.99-").
  /*overlay(oeelb.user4,40,1)  = if v-disctype = no then "%" else "$".*/
  overlay(oeelb.user4,41,1)  = zi-qtybrkty.
  overlay(oeelb.user4,42,8)  = string(zi-pdscrecno,">>>>>>>9").
  overlay(oeelb.user4,50,1)  = string(zi-level,"9").
  overlay(oeelb.user4,51,10) = string(v-pdamt,">>>>9.9999").
  overlay(oeelb.user4,61,1)  = v-pd$md.
  overlay(oeelb.user4,62,11) = string(zi-totnet,">>>>>>9.99-").
  overlay(oeelb.user4,74,1)  = v-lcomment.
  overlay(oeelb.user3,44,11) = string(zi-totcost,">>>>>>9.99-").
  overlay(oeelb.user3,55,1)  = if v-rebatefl <> " " then v-rebatefl
                               else substr(oeelb.user3,55,1).
  overlay(oeelb.user3,56,11) = string(zi-totmargin,">>>>>>9.99-").
  overlay(oeelb.user3,67,11) = string(zi-margpct,">>>>>>9.99-").
  overlay(oeelb.user3,78,1)  = v-kitrollty.
  overlay(oeelb.user5,1,1)   = if v-kitfl = yes then "y" else " ".
  overlay(oeelb.user10,1,1)  = v-NR.
  /* EN-437-Override LT Days */
  if OD-date <> ? then
   do:
   overlay(oeelb.user12,10,4) = if v-OLTDays = 0 and   /*Ovr LT Days*/
                                  int(substr(oeelb.user12,10,4)) > 0 then
                                  substr(oeelb.user12,10,4)
                                else
                                  string(v-OLTDays,"->>9").
   if substr(oeelb.user12,1,8) = " " then 
     overlay(oeelb.user12,1,8)  = string(OD-date,"99/99/99"). /*Original Date*/
  end. /* OD-date <> ? */
  /* hold the override info if alread overridden */
  overlay(oeelb.user21,1,4)    = if substr(oeelb.user21,1,4) = " " then
                                   zhold-operinit
                                 else
                                   substr(oeelb.user21,1,4).
  overlay(oeelb.user21,6,13)   = if substr(oeelb.user21,6,13) = " " or
                                    dec(substr(oeelb.user21,6,13)) = 0 then
                                   string(zhold-orig-price,">>>>>>9.99999")
                                 else
                                   substr(oeelb.user21,6,13).
  overlay(oeelb.user21,20,13)  = if substr(oeelb.user21,20,13) = " " or
                                    dec(substr(oeelb.user21,20,13)) = 0 then
                                   string(zhold-ovrd-price,">>>>>>9.99999")
                                 else
                                   substr(oeelb.user21,20,13).
  overlay(oeelb.user21,36,8)   = string(TODAY,"99/99/99").
  overlay(oeelb.user21,46,4)   = g-operinits.
  if substr(oeelb.user5,1,1) = "y" and oeelb.specnstype = "n" then
    assign oeelb.usagefl = yes.
  find first sc-notes where 
             sc-notes.cono         = g-cono and
             sc-notes.notestype    = "ZZ" and
             sc-notes.primarykey   = "standard cost markup" and
             sc-notes.secondarykey = oeelb.prodcat and
             notes.pageno          = 1
             no-lock no-error.
  assign  oeelb.botype       = if v-botype = "" then
                                 if v-5603fl = yes then "n"
                                 else
                                   if avail arsc and arsc.bofl = yes
                                     then "y"
                                   else "n"
                               else
                                 /* das - 06/23/09 */
                                 if avail arsc and arsc.bofl = yes and
                                    v-kitfl = yes then "y"
                                 else v-botype
          oeelb.user2       = if v-glcost <> 0 then 
                                string(v-glcost,">>>>9.99")
                              else if avail icsw then 
                                     string(icsw.avgcost,">>>>9.99")
                                   else string(0,">>>>9.99")
          oeelb.prodcost     = if v-prodcost <> 0 then v-prodcost 
                                  else
                                    if avail icsw then 
                                       icsw.stndcost
                                    else if avail sc-notes then 
                                         ((1 + (dec(sc-notes.noteln[1]) / 100))
                                            * v-prodcost)
                                       else
                                        0
          /*oeelb.costoverfl   = if oeelb.prodcost = 0 then no
                               else yes*/
          oeelb.vendno       = if avail icsw then icsw.arpvendno
                               else if v-vendno <> 0 then v-vendno
                               else 0
          oeelb.arpvendno    = if oeelb.specnstype <> " " then oeelb.vendno
                               else oeelb.arpvendno
          oeelb.taxablefl    = v-taxablefl
          oeelb.printpricefl = yes
          oeelb.subtotalfl   = no
          oeelb.user11       = cataddfl.
  if substr(oeelb.user1,1,2) = "y0" and dec(oeelb.user2) > 0 then
    overlay(oeelb.user1,2,7) = left-trim(oeelb.user2).

  find icsc where icsc.catalog = oeelb.shipprod no-lock no-error.
  if avail icsc then
    do:
    find icsp where icsp.cono     = g-cono and
                    icsp.prod     = oeelb.shipprod and
                    icsp.enterdt  = today and
                    icsp.operinit = g-operinit
                    no-lock no-error.
    if avail icsp then      
      /* user used catalog and created inventory */
      overlay(oeelb.user21,60,10) = "CAT & ICSP". 
    else
      do:
      find icsp where icsp.cono = g-cono and
                      icsp.prod = oeelb.shipprod
                      no-lock no-error.
      if avail icsp then    
        /* user used existing product */
        overlay(oeelb.user21,60,10) = "ICSP EXIST".
      else
        /* user used catalog */
        overlay(oeelb.user21,60,10) = "CAT NONSTK". 
    end.
    overlay(oeelb.user21,70,8) = string(today,"99/99/99").
  end. /* avail icsc */

  {t-all.i "oeelb"}
  /* fix misc issues with xrefprodty */
  if (oeelb.xrefprodty = " " or oeelb.xrefprodty = "" 
                             or oeelb.xrefprodty = "g") and 
    oeelb.reqprod <> " " then
    assign oeelb.xrefprodty = "c".
end. /* Procedure Create_OEELB */


/* ------------------------------------------------------------------------- */ 
Procedure Create_OEELK: 
/* ------------------------------------------------------------------------- */
  if not avail oeehb then
    find oeehb where oeehb.cono = g-cono and
                     oeehb.batchnm = string(w-batchnm,">>>>>>>9") and 
                     oeehb.sourcepros = "Quote"
                     no-error.
  if avail oeehb and oeehb.whse <> v-whse then
    find icsw where icsw.cono = g-cono and
                    icsw.prod = v-prod and
                    icsw.whse = oeehb.whse
                    no-lock no-error.
  else
    find icsw where icsw.cono = g-cono and
                    icsw.prod = v-prod and
                    icsw.whse = v-whse
                    no-lock no-error.

  find icsp where icsp.cono = g-cono and
                  icsp.prod = v-prod
                  no-lock no-error.
  assign oeelk.cono       = g-cono
         oeelk.ordertype  = "b"
         oeelk.orderno    = v-quoteno
         oeelk.ordersuf   = 0
         oeelk.lineno     = v-lineno
         oeelk.seqno      = v-seqno
         oeelk.statustype = "a"
         oeelk.transtype  = "QU"
         oeelk.shipprod     = v-prod
         oeelk.qtyneeded    = v-kqty
         oeelk.qtyord       = v-kqty
         oeelk.unit         = if avail icsp and icsp.unitstock <> "" then
                                icsp.unitstock
                              else
                              if avail icsp and icsp.unitsell <> "" then
                                icsp.unitsell
                              else
                                "EACH"
         oeelk.custno       = v-custno
         oeelk.whse         = if avail oeehb then oeehb.whse else v-whse
         oeelk.arpwhse      = if avail icsw and icsw.arptype = "w" then
                                icsw.arpwhse else ""
         oeelk.arpvendno    = if avail icsw then icsw.arpvendno else v-vendno 
         oeelk.arpprodline  = if avail icsw then icsw.prodline else  v-prodline
         oeelk.pricetype    = if avail icsw then icsw.pricetype else v-ptype
         oeelk.user7        = if avail icsw then dec(icsw.leadtmavg)
                              else dec(v-leadtm)
         oeelk.comptype     = "c"
         oeelk.printfl      = no
         oeelk.pricefl      = no
         oeelk.price        = if v-specnstype = "n" then v-listprc 
                              else v-sellprc
         oeelk.prodcost     = v-prodcost
         oeelk.glcost       = if avail icsw then icsw.avgcost else v-glcost
         /*
         oeelk.glcost       = if avail icsw then icsw.replcost else v-prodcost
         */
         oeelk.prodcat      = if v-prodcat <> " " then v-prodcat else
                              if avail icsp then icsp.prodcat else v-prodcat
         oeelk.refer        = v-descrip
         oeelk.specnstype   = v-specnstype
         oeelk.pdrecno      = zi-pdscrecno
         /*
         oeelk.pricefl      = no
         */
         oeelk.stkqtyord    = 0
         oeelk.stkqtyship   = 0
         oeelk.reqfl        = no
         oeelk.user6        = v-sparebamt
         oeelk.user8        = v-shipdt
         oeelk.user11       = cataddfl.
  overlay(oeelk.user4,1,4)   = if avail oeehb then oeehb.whse else v-whse.
  overlay(oeelk.user4,5,7)   = string(v-gp,">>9.99-").
  overlay(oeelk.user4,41,1)  = zi-qtybrkty.
  overlay(oeelk.user4,42,8)  = string(zi-pdscrecno,">>>>>>>9").
  overlay(oeelk.user4,50,1)  = string(zi-level,"9").
  overlay(oeelk.user4,51,10) = string(v-pdamt,">>>>9.9999").
  overlay(oeelk.user4,61,1)  = v-pd$md.
  overlay(oeelk.user4,62,11) = string(zi-totnet,">>>>>>9.99-").
  overlay(oeelk.user4,74,1)  = v-lcomment.
  overlay(oeelk.user3,44,11) = string(zi-totcost,">>>>>>9.99-").
  overlay(oeelk.user3,55,1)  = v-rebatefl.
  overlay(oeelk.user3,56,11) = string(zi-totmargin,">>>>>>9.99-").
  overlay(oeelk.user3,67,11) = string(zi-margpct,">>>>>>9.99-").    
  {t-all.i oeelk}
end. /* Procedure Create_OEELK */


/* ------------------------------------------------------------------------- */ 
Procedure Update_OEELK: 
/* ------------------------------------------------------------------------- */
  if not avail oeehb then
    find oeehb where oeehb.cono = g-cono and
                     oeehb.batchnm = string(w-batchnm,">>>>>>>9") and 
                     oeehb.sourcepros = "Quote"
                     no-error.
  if avail oeehb and oeehb.whse <> v-whse then
    find icsw where icsw.cono = g-cono and
                    icsw.prod = v-prod and
                    icsw.whse = oeehb.whse
                    no-lock no-error.
  else
    find icsw where icsw.cono = g-cono and
                    icsw.prod = v-prod and
                    icsw.whse = v-whse
                    no-lock no-error.

  find icsp where icsp.cono = g-cono and
                  icsp.prod = v-prod
                  no-lock no-error.

  assign oeelk.shipprod     = v-prod
         oeelk.qtyneeded    = v-kqty
         oeelk.qtyord       = v-kqty
         oeelk.unit         = if v-conversion then
                                oeelk.unit
                              else if avail icsp then icsp.unitstock else ""
         oeelk.custno       = if v-conversion then
                                oeelk.custno
                              else v-custno
         oeelk.whse         = if v-conversion then
                                oeelk.whse
                              else   
                              if avail oeehb then oeehb.whse else v-whse
         oeelk.arpwhse      = if v-conversion then
                                oeelk.arpwhse
                              else  
                              if avail icsw and icsw.arptype = "w" then
                                icsw.arpwhse else ""
         oeelk.arpvendno    = if v-conversion then
                                oeelk.arpvendno
                              else  
                              if avail icsw then icsw.arpvendno else v-vendno
         oeelk.arpprodline  = if v-conversion then
                                oeelk.arpprodline
                              else  
                              if avail icsw then icsw.prodline else v-prodline
         oeelk.pricetype    = if avail icsw then icsw.pricetype 
                              else v-ptype
         oeelk.user7        = if v-conversion then
                                oeelk.user7
                              else
                              if avail icsw then dec(icsw.leadtmavg)
                              else dec(v-leadtm)
         oeelk.comptype     = "c"
         oeelk.printfl      = no
         oeelk.pricefl      = no
         oeelk.price        = if v-conversion then
                                oeelk.price
                              else   
                              if v-specnstype = "n" then v-listprc 
                              else v-sellprc
         oeelk.prodcost     = v-prodcost
         /*
         oeelk.glcost       = if avail icsw then icsw.replcost else v-prodcost
         */
         oeelk.glcost       = if avail icsw then icsw.avgcost else v-glcost
         oeelk.prodcat      = if v-prodcat <> " " then v-prodcat else
                              if avail icsp then icsp.prodcat else v-prodcat
         oeelk.refer        = v-descrip
         oeelk.specnstype   = v-specnstype
         oeelk.pdrecno      = if v-conversion then
                                oeelk.pdrecno
                              else
                                zi-pdscrecno
         oeelk.unit         = if oeelk.unit <> "" then
                                oeelk.unit
                              else  
                              if avail icsp and icsp.unitstock <> "" then
                                icsp.unitstock
                              else
                              if avail icsp and icsp.unitsell <> "" then
                                icsp.unitsell
                              else
                                "EACH"

         oeelk.stkqtyord    = 0
         oeelk.stkqtyship   = 0
         oeelk.reqfl        = no
         oeelk.variablefl   = yes
         oeelk.user6        = v-sparebamt
         oeelk.user8        = v-shipdt.

  overlay(oeelk.user5,5,24) = v-xprod.
  overlay(oeelk.user4,1,4)   = v-whse.
  overlay(oeelk.user4,5,7)   =  string(v-gp,">>9.99-").
  overlay(oeelk.user4,41,1)  = zi-qtybrkty.
  overlay(oeelk.user4,42,8)  = string(zi-pdscrecno,">>>>>>>9").
  overlay(oeelk.user4,50,1)  = string(zi-level,"9").
  overlay(oeelk.user4,51,10) = string(v-pdamt,">>>>9.9999").
  overlay(oeelk.user4,61,1)  = v-pd$md.
  overlay(oeelk.user4,62,11) = string(zi-totnet,">>>>>>9.99-").
  overlay(oeelk.user4,74,1)  = v-lcomment.
  overlay(oeelk.user3,44,11) = string(zi-totcost,">>>>>>9.99-").
  overlay(oeelk.user3,55,1)  = v-rebatefl.
  overlay(oeelk.user3,56,11) = string(zi-totmargin,">>>>>>9.99-").
  overlay(oeelk.user3,67,11) = string(zi-margpct,">>>>>>9.99-").

  {t-all.i oeelk}
end. /* Procedure Update_OEELK */


/* ------------------------------------------------------------------------- */ 
Procedure Create_OEELK_BOD: 
/* ------------------------------------------------------------------------- */
  def input        parameter ix-batchnm    as i format ">>>>>>>9" no-undo.
  def input        parameter ix-prod       like icsp.prod         no-undo.
  def input        parameter ix-whse       like icsw.whse         no-undo.
  def input        parameter ix-quoteno    as i format ">>>>>>>9" no-undo.
  def input        parameter ix-lineno     like oeel.lineno       no-undo.
  def input-output parameter ix-xprod      like icsw.whse         no-undo.
  def input-output parameter v-kitlineno   as integer             no-undo.
                          
  if not avail oeehb then
    find oeehb where oeehb.cono = g-cono and
                     oeehb.batchnm = string(ix-batchnm,">>>>>>>9") and 
                     oeehb.sourcepros = "Quote"
                     no-error.
  
  assign kpsk-line = 0.
  for each kpsk where kpsk.cono = g-cono and
                      kpsk.prod = ix-prod and
                      kpsk.comptype = "c"
                      no-lock:
    assign kpsk-line = kpsk-line + 1.  /* keep track of seqno */
    find icsp where icsp.cono = g-cono and
                    icsp.statustype <> "I" and
                    icsp.prod = kpsk.comprod
                    no-lock no-error.
    if not avail icsp then
      do:
      message "Component " + 
              string(kpsk.seqno,">>9") + 
              " product " + 
              kpsk.comprod 
              " is inactive."
              " Work with Buyer to Re-Activate".
      pause.
    end.
    if avail icsp then
      do:
      assign h-prod = v-prod
             v-prod = icsp.prod.
      run oeexqSauerAPR.p (input "P").
      /*
      run SauerAPRcheck.p (input icsp.prodcat,
                           input v-shiptost,
                           input v-shiptozip,
                           input-output apr-error,
                           input-output apr-msg).
      */
      assign v-prod = h-prod.
    end.
    find icsw where icsw.cono = g-cono and
                    icsw.prod = kpsk.comprod and
                    icsw.whse = ix-whse
                    no-lock no-error.
    if not avail icsw then 
      do:
      find icsw where icsw.cono = g-cono and
                      icsw.prod = kpsk.comprod and
                      icsw.statustype <> "x"
                      no-lock no-error.
    end.
    if avail icsw then 
      do:
      /*
      assign xcount = 0.
      for each icsec where icsec.cono = g-cono and     
                           icsec.rectype = "c" and     
                           icsec.custno  = oeehb.custno and
                           icsec.altprod = icsw.prod
                           no-lock:                    
        assign xcount  = xcount + 1                   
        ix-xprod = substr(icsec.prod,1,22).
      end.                                             
      if xcount = 0 then                               
        assign ix-xprod = "No Xrefs Exist".            
      if xcount > 1 then                               
        assign ix-xprod = "Multiple Xrefs Exist".      
      */
 
      find first icsec where icsec.cono = g-cono and
                 icsec.rectype = "c" and
                 icsec.custno  = v-custno and
                 icsec.altprod = v-prod
                 no-lock no-error.
      if avail icsec then
        do:
        assign v-xprod = icsec.prod.
      end.
      else
        assign v-xprod = " ".
    end.
    
    find oeelk where oeelk.cono      = g-cono and
                     oeelk.ordertype = "b" and
                     oeelk.orderno   = ix-quoteno and
                     oeelk.ordersuf  = 0 and
                     oeelk.lineno    = ix-lineno and
                     oeelk.seqno     = /*kpsk.seqno*/ kpsk-line
                     no-lock no-error.
    if not avail oeelk then 
      do:
      /* das - 06/26/09 - added rebate info here */
      if avail icsw then
        do:
        assign hc-whse     = v-whse
               hc-prod     = v-prod
               hc-prodcost = v-prodcost
               hc-vendno   = v-vendno
               v-whse      = icsw.whse
               v-prod      = icsw.prod
               v-vendno    = icsw.arpvendno
               v-sparebamt = 0.
        run oeexqpricing.p.     
        if v-rebatefl <> " " and h-rebfl = " " then
          assign h-rebfl = v-rebatefl.
      end.
      create oeelk.
      assign oeelk.cono         = g-cono
             oeelk.ordertype    = "b"
             oeelk.orderno      = ix-quoteno
             oeelk.ordersuf     = 0
             oeelk.lineno       = ix-lineno
             oeelk.seqno        = /*kpsk.seqno*/ kpsk-line
             oeelk.statustype   = "a"
             oeelk.transtype    = "QU"
             oeelk.shipprod     = kpsk.comprod
             oeelk.qtyneeded    = kpsk.qtyneeded
             oeelk.qtyord       = kpsk.qtyneeded
             oeelk.stkqtyord    = kpsk.qtyneeded
             oeelk.unit         = kpsk.unit
             oeelk.custno       = oeehb.custno
             oeelk.whse         = ix-whse
             oeelk.arpwhse      = if avail icsw and icsw.arptype = "w" then
                                    icsw.arpwhse else ""
             oeelk.pricetype    = if avail icsw then icsw.pricetype else " "
             oeelk.comptype     = kpsk.comptype
             oeelk.printfl      = no
             oeelk.pricefl      = kpsk.pricefl
          /* oeelk.price        = v-sellprc    */
             oeelk.refer        = kpsk.refer
          /* oeelk.specnstype   = v-specnstype */
          /* oeelk.pdrecno      = zi-pdscrecno */
             oeelk.stkqtyord    = 0
             oeelk.stkqtyship   = 0
             oeelk.proddesc2    = ix-xprod
             oeelk.prodcat      = if avail icsp then icsp.prodcat else " "
             oeelk.prodcost     = v-prodcost
             oeelk.user6        = v-sparebamt
             oeelk.reqfl        = no
             oeelk.variablefl   = yes.  
      overlay(oeelk.user4,1,4)  = ix-whse.
        
      assign v-whse     = hc-whse
             v-prod     = hc-prod
             v-prodcost = hc-prodcost
             v-vendno   = hc-vendno.
      
      {t-all.i oeelk}
      /*****
      find t-lines where t-lines.lineno = ix-lineno and
                         t-lines.seqno  = 0 and
                         t-lines.prod   = ix-prod
                         no-error.
      if not avail t-lines then
        do:
        create t-lines.
        assign t-lines.lineno  = ix-lineno
               t-lines.seqno   = 0
               t-lines.prod    = ix-prod
               t-lines.kitfl   = yes.
      end.
      ******/
      find t-lines where t-lines.lineno = oeelk.lineno and
                         t-lines.seqno  = oeelk.seqno  
                         no-lock no-error.
      if not avail t-lines then
        do: 
        create t-lines.
        assign t-lines.lineno     =  oeelk.lineno
               t-lines.seqno      =  oeelk.seqno
               t-lines.prod       =  oeelk.shipprod
               t-lines.slsrepout  =  oeehb.slsrepout
               t-lines.slsrepin   =  oeehb.slsrepin
               t-lines.prodcost   =  oeelk.prodcost
               t-lines.sparebamt  =  oeelk.user6
               t-lines.descrip    =  oeelk.refer
               t-lines.xprod      =  substr(oeelk.user5,5,24)
               t-lines.whse       =  oeelk.whse
               t-lines.listprc    =  0
               t-lines.sellprc    =  oeelk.price
               t-lines.gp         =  0
               t-lines.discpct    =  0
               t-lines.kqty       =  oeelk.qtyord
               t-lines.shipdt     =  ?
               t-lines.kitfl      =  no
               t-lines.specnstype = oeelk.specnstype
               t-lines.pdscrecno  = oeelk.pdrecno
               t-lines.prodcat    = oeelk.prodcat
               t-lines.icnotefl   = if avail icsp then icsp.notesfl 
                                    else " "
               v-kitlineno        = oeelk.lineno.
               
      end. /* not avail t-lines */
    end. /* not avail oeelk */
  end. /* each kpsk */
  run Roll_Kit_Cost-Price (input "c",
                           input-output v-sellprc,
                           input-output v-kitcost,
                           input-output v-gp).
  assign v-prodcost = v-kitcost.
  for each t-lines where t-lines.lineno = v-lineno and
                         t-lines.seqno  = 0:
    assign t-lines.gp       = v-gp
           t-lines.prodcost = v-kitcost.
  end.
end. /* procedure Create_OEELK_BOD */


/* ------------------------------------------------------------------------- */ 
Procedure notes-create: 
/* ------------------------------------------------------------------------- */

  assign n-inx = 1     /* noteln index    */
         c-inx = 1     /* v-comment index */
         p-inx = 1     /* page index      */
         n-IE  = if v-internalfl = yes then 
                   "I"
                 else 
                   "E".

  if v-quoteno > 0 then do:
  find notes where notes.cono = g-cono and
                   notes.notestype = "ba" and
                   notes.primarykey   = string(v-quoteno,">>>>>>>9") and
                   notes.secondarykey = n-IE and
                   notes.pageno = p-inx
                   no-error.
  if not avail notes then 
    do:
    create notes.
    assign notes.cono = g-cono
           notes.notestype = "ba"
           notes.primarykey   = string(v-quoteno,">>>>>>>9")
           notes.secondarykey = n-IE
           notes.pageno = p-inx
           notes.printfl  = if n-IE = "I" then no else v-allfl
           notes.printfl2 = if n-IE = "I" then no else v-ackfl
           notes.printfl3 = if n-IE = "I" then no else v-pckfl
           notes.printfl4 = if n-IE = "I" then no else v-advfl
           notes.printfl5 = if n-IE = "I" then no else v-invfl
           notes.operinit = g-operinits
           notes.transdt  = TODAY
           notes.transtm  = string(TIME,"HH:MM").
  end.

  if n-IE = "i" then
    h-chandle =  v-comments:handle in frame f-bot2i. 
  else  
    h-chandle = v-comments:handle in frame f-bot2e.

  assign v-cstartPos  = 1
         j-inx        = h-chandle:num-lines.
  do i-inx = 1 to j-inx:                          /* 99:  %TAH */
    if n-inx > 16 then 
      do:
      assign n-inx = 1
             p-inx = p-inx + 1.
      find notes where notes.cono = g-cono and
                       notes.notestype = "ba" and
                       notes.primarykey   = string(v-quoteno,">>>>>>>9") and
                       notes.secondarykey = n-IE and
                       notes.pageno = p-inx
                       no-error.
      if not avail notes then 
        do:
        create notes.
        assign notes.cono = g-cono
               notes.notestype = "ba"
               notes.primarykey   = string(v-quoteno,">>>>>>>9")
               notes.secondarykey = n-IE
               notes.pageno = p-inx
               notes.printfl  = if n-IE = "I" then no else v-allfl
               notes.printfl2 = if n-IE = "I" then no else v-ackfl
               notes.printfl3 = if n-IE = "I" then no else v-pckfl
               notes.printfl4 = if n-IE = "I" then no else v-advfl
               notes.printfl5 = if n-IE = "I" then no else v-invfl
               notes.operinit = g-operinits
               notes.transdt  = TODAY
               notes.transtm  = string(TIME,"HH:MM").
      end.
    end.
  
    if i-inx > j-inx then
      leave.                /* no more lines */
    else 
      do:
   /* Code to read each line how it sits in the EDITOR widget */
      assign v-coffset = if i-inx = j-inx then
                         h-chandle:length
                      else
                         h-chandle:convert-to-offset ( i-inx + 1, 1 )
           
             v-dummyfl = if i-inx = j-inx and j-inx = 1 then
                           h-chandle:set-selection (v-cstartPos, v-coffset )
                         else
                           h-chandle:set-selection (v-cstartPos, v-coffset - 1)
/* TAH 01/17/07                           
            v-dummyfl = h-chandle:set-selection ( v-cstartPos, v-coffset - 1 )
*/
           v-commentline = h-chandle:selection-text
           v-cstartPos = v-coffset
           v-dummyfl = h-chandle:clear-selection(). 
           /* Above line not necessary - hides selection bar */
      assign notes.noteln[n-inx] = replace(v-commentline,chr(10),"")
             c-inx = c-inx + 1
             n-inx = n-inx + 1. 
    end.

    if v-quoteno > 0 then
    run oeexqinqnote.p (input g-cono,
                   input string(v-quoteno,">>>>>>>9")).
  end.

  /* 
  OEEXQ is holding the notes record and locking other users out.  This
  FOR EACH transaction eleviates this from happening.
  */
  for each notes where notes.cono = g-cono and
                       notes.notestype = "ba" and
                       notes.primarykey   = string(v-quoteno,">>>>>>>9") and
                       notes.secondarykey = n-IE and
                       notes.pageno = p-inx:
  end.
  end. /* v-quoteno > 0 */
end. /* Procedure notes-create */

/************
/* ------------------------------------------------------------------------- */ 
Procedure Create_ICSW: 
/* ------------------------------------------------------------------------- */
  def input        parameter ix-whse  like icsw.whse                  no-undo.
  def input        parameter ix-prod  like icsw.prod                  no-undo.

  for each icsd where icsd.cono    = g-cono and 
                      icsd.salesfl = yes    and
                      icsd.custno  = 0
                      no-lock:
    find first t-icsw where t-icsw.cono        = g-cono  and
                            t-icsw.prod        = ix-prod and
                            t-icsw.statustype <> "X"
                            no-lock no-error.
    if avail t-icsw then
      do transaction:
      create icsw.
      buffer-copy t-icsw except t-icsw.cono
                                t-icsw.whse
                                t-icsw.qtyonhand
                                t-icsw.qtyreservd
                                t-icsw.qtycommit
                                t-icsw.qtybo
                                t-icsw.qtyintrans
                                t-icsw.qtyunavail
                                t-icsw.qtyonorder
                                t-icsw.qtyrcvd
                                t-icsw.qtyreqrcv
                                t-icsw.qtyreqshp
                                t-icsw.qtydemand
                                t-icsw.lastsodt
                                t-icsw.availsodt
                                t-icsw.nodaysso
                                t-icsw.leadtmprio
                                t-icsw.leadtmlast
                                t-icsw.lastltdt
                                t-icsw.priorltdt
                                t-icsw.lastinvdt
                                t-icsw.lastrcptdt
                                t-icsw.lastcntdt
                                t-icsw.lastpowtdt
                                t-icsw.rpt852dt
                                t-icsw.priceupddt
                                t-icsw.issueunytd
                                t-icsw.retinunytd
                                t-icsw.retouunytd
                                t-icsw.rcptunytd
                                t-icsw.qtydemand
                                t-icsw.enterdt
                                t-icsw.linept
                                t-icsw.orderpt
                                t-icsw.frozentype
                                t-icsw.safeallamt
                                t-icsw.safeallpct
                                t-icsw.usagerate
                                t-icsw.ordqtyin
                                t-icsw.ordqtyout
                                t-icsw.frozenmmyy
                                t-icsw.frozenmos
                                t-icsw.binloc1
                                t-icsw.binloc2
      to icsw
      assign icsw.cono       = g-cono
             icsw.whse       = ix-whse
             icsw.statustype = "O"
             icsw.leadtmavg  = 30
             icsw.frozenmmyy  = string(month(today),"99") +
                                substr(string(year(today),"9999"),3,2)
             icsw.frozenmos   = 6
             icsw.binloc1     =  "New Part"
             icsw.enterdt     = today
             icsw.frozentype  = "N".
                  
      create icswu.                       
      assign icswu.cono        = g-cono
             icswu.prod        = ix-prod
             icswu.whse        = ix-whse
             g-prod = ix-prod
             g-whse = ix-whse.
      {t-all.i icswu}
      release icswu.
      /* Create a Partial Product Record (Product on the Fly) */
      find icsl where icsl.cono = g-cono and
                      icsl.whse = t-icsw.whse and
                      icsl.vendno = t-icsw.arpvendno and
                      icsl.prodline = t-icsw.prodline
                      no-lock no-error.
      if avail icsl then
        do:
        find zidc use-index docinx4 where
             zidc.cono       = g-cono and
             zidc.docstatus  = 02     and
             zidc.docttype   = "OTF" and
             zidc.doctype    = icsl.buyer and
             substr(zidc.docpartner,1,4)  = icsw.whse and
             substr(zidc.docpartner,5,24) = icsw.prod
             no-lock no-error.
        if not avail zidc then
          do:
          find icsp where icsp.cono = g-cono and
                          icsp.prod = v-prod
                          no-lock no-error.
          if not avail icsp then leave.
          assign substr(w-docno,1,7) = string(w-batchnm,">>>>>>>9")
                 substr(w-docno,8,2) = "00".
          create zidc.
          assign zidc.cono       = g-cono
                 zidc.docnumber  = int(w-docno)
                 zidc.docdate    = TODAY
                 substr(zidc.docpartner,1,4)  = icsw.whse
                 substr(zidc.docpartner,5,24) = icsw.prod   
                 substr(zidc.docdesc,1,24) = icsp.descrip[1]
                 substr(zidc.docdesc,27,4) = g-operinit     
                 zidc.docstatus  = 02
                 zidc.docdisp    = ""                                
                 zidc.docstnbr   = 0                                 
                 zidc.docfadate  = if icsp.enterdt = TODAY and
                                      icsp.operinit = g-operinit then
                                                           TODAY
                                   else
                                      ?
                 zidc.doctype    = icsl.buyer
                 zidc.docgsnbr   = v-lineno
                 zidc.docttype   = "OTF"
                 substr(zidc.docuser1,1,6)  = icsl.prodline
                 substr(zidc.docuser1,7,24) = icsp.descrip[1]
                 substr(zidc.docuser2,1,15) = "              "
                 substr(zidc.docuser2,17,4) = icsp.prodcat
                 substr(zidc.docuser3,1,24) = icsw.prod
                 substr(zidc.docuser3,27,4) = icsw.pricetype
                 substr(zidc.docuser4,1,4)  = icsw.whse
                 substr(zidc.docuser4,6,12) = string(icsl.vendno)
                 zidc.docvendno  = icsw.replcost
                 zidc.docstnbr   = icsw.leadtmavg.
          find b-icsl where b-icsl.cono = g-cono and
                            b-icsl.whse = icsw.whse and
                            b-icsl.vendno = icsw.arpvendno and
                            b-icsl.prodline = icsw.prodline
                            no-lock no-error.
          if not avail b-icsl then
            assign zidc.docdisp = "n".  /* icsl is new or missing */
          else
            assign zidc.docdisp = "".
          release zidc.
        end. /* not avail zidc */
      end. /* avail icsl */
      leave.
    end. /* if avail t-icsw */
  end. /* for each icsd */

end. /* Procedure Create_ICSW */
************/

/* ------------------------------------------------------------------------- */ 
Procedure Assign_LineDate:
/* ------------------------------------------------------------------------- */
  def input        parameter  ix-lmode         as   character        no-undo.
  def input        parameter  ix-repricefl     as   logical          no-undo.
  def input        parameter  ix-whse          like icsw.whse        no-undo.
  def input        parameter  ix-qtyavail      like icsw.qtyonhand   no-undo.
  def input        parameter  ix-qty           like icsw.qtyonhand   no-undo.
  def input        parameter  ix-prod          like icsw.prod        no-undo.
  def input        parameter  ix-statustype    like icsw.statustype  no-undo.
  def input        parameter  ix-qtyreserv     like icsw.qtyreserv   no-undo.
  def input        parameter  ix-qtycommit     like icsw.qtycommit   no-undo.
  def input        parameter  ix-avgltdt       like icsw.avgltdt     no-undo.
  def input        parameter  ix-leadtmavg     like icsw.leadtmavg   no-undo.
  def input        parameter  ix-enddaycut     like icsd.enddaycut   no-undo.
  def input        parameter  ix-divno         like oeehb.divno      no-undo.
  def input-output parameter  ix-shipdt        like oeel.promisedt   no-undo.

  if not avail icsp then
    find icsp where icsp.cono = g-cono and
                    icsp.prod = ix-prod
                    no-lock no-error.
  if ix-shipdt = ? or (ix-shipdt <> ? and ix-whse <> orig-whse) and
     manualdtfl = no and v-1019fl = no then    
    do:
    if ix-qtyavail = 0 and (ix-avgltdt < (TODAY - 183) or ix-avgltdt = ?) 
      then
      assign ix-shipdt = if ix-shipdt = ? then 09/09/99
                        else ix-shipdt.
    else
    if ix-leadtmavg > 0 then
      assign ix-shipdt = TODAY + ix-leadtmavg.
    else
      assign ix-shipdt = if avail icsp and icsp.kittype = "b" then 09/09/99
                         else 
                           if not avail icsp and v-kitfl = yes then 09/09/99
                           else TODAY + 30.
      /*
      assign ix-shipdt = if avail icsp and ix-leadtmavg = 0 then 09/09/99
                         else TODAY + 30.
      */
  end. 
  
  if ix-shipdt <> 09/09/99 and (ix-lmode = "a" or ix-repricefl = yes) and
     manualdtfl = no and v-1019fl = no then
    do:
    if ix-shipdt = ? or ix-shipdt = today then
      assign ix-shipdt = 
      if ((int(substr(string(time,"hh:mm:ss"),1,2)) * 100) +
           int(substr(string(time,"hh:mm:ss"),4,2))) >
         ((truncate(truncate(ix-enddaycut / 60 ,0) / 60,0) * 100) +
          (truncate(ix-enddaycut / 60 ,0)) mod 60) then
        ix-shipdt + 1
      else
        ix-shipdt.    
    else
      assign ix-shipdt = /*today*/ ix-shipdt.
    if v-1019fl = no then   /* avoid duplicate warning messages displayed */
      do:
      run zsdiweekendh (input-output ix-shipdt,
                        input-output v-1019fl,
                        input        ix-whse,
                        input        g-cono,
                        input        oeehb.divno).
      if v-1019fl = yes then
        do:
        assign h-shipdt = ix-shipdt
               v-shipdt = ix-shipdt.
        display v-shipdt with frame f-mid1.
        run err.p(1019).
      end.
    end. /* v-1019fl = no */
  end. /* ix-shipdt */
  
end. /* Procedure Assign_LineDate */


/* ------------------------------------------------------------------------- */ 
Procedure Check_ARSS:
/* ------------------------------------------------------------------------- */
  def input        parameter  ix-custno        like arss.custno      no-undo.
  def input        parameter  ix-shipto        like arss.shipto      no-undo.
  def input-output parameter  ix-5966fl        as   logical          no-undo.

  assign ix-5966fl = no.
  find arss where arss.cono = g-cono and
                  arss.custno = v-custno and
                  arss.shipto = v-shipto
                  no-lock no-error.
  if avail arss and arss.statustype = no then
    assign ix-5966fl = yes.
  
end. /* Check_ARSS */

/* ------------------------------------------------------------------------- */
Procedure OTF_Routine:
/* ------------------------------------------------------------------------- */
  def input        parameter  v-prod          like icsw.prod        no-undo.
  def input        parameter  v-whse          like icsw.whse        no-undo. 
  def input        parameter  v-lineno        like oeel.lineno      no-undo.
  def input        parameter  x-prodtype      as   c format "x(4)"  no-undo.
  
  if v-whse = "" then
    do:
    run Find_Whse (input x-prodtype).
  end.
  assign otf-create = no
         otf-kit    = no.
  assign g-whse     = v-whse
         g-oelineno = v-lineno.
  if g-whse = "" then
    do:
    find oeehb where oeehb.cono = g-cono and
                     oeehb.batchnm = string(w-batchnm,">>>>>>>9") and 
                     oeehb.sourcepros = "Quote"
                     no-lock no-error.
    if avail oeehb then
      assign g-whse = oeehb.whse.
  end.

  if ({v-icsw.i v-prod v-whse}) = false then 
    do:
    assign g-prod     = v-prod.
           /*
           g-currproc = "oeexq"
           g-callproc = "oeexq"
           g-ourproc  = "oeexq".
           */
    if ({v-icsp.i v-prod}) = false then 
      do:
      assign g-secure = 4.
      run icsp.p.
      run icspupdt.p(g-prod,v-lineno).
      assign w-loadefaults = no.
    end.
    else
      assign w-loadefaults = yes.
    /* verify icsp was created */
    if ({v-icsp.i v-prod}) = true then
      do:
      assign otf-error = no.
      run otficsp.p(input v-prod,
                    output otf-error).
      if otf-error = yes then 
        do:
        run err.p(4735).
        next-prompt v-prod.
        next.
      end.
      assign otf-create = yes.
      assign w-currproc = g-currproc
             w-ourproc  = g-ourproc
             w-lineno   = g-oelineno
             g-secure = 4
             g-currproc = "icsw"
             g-oelineno = w-lineno.
      run icsw.p.
      assign g-oelineno = w-lineno
             g-currproc = w-currproc
             g-ourproc  = w-ourproc.

      run zsdimaincpy.p (input g-cono,
                         input "x",
                         input v-whse,
                         input v-prod,
                         input v-lineno,
                         input-output zsdi-confirm).           
    end. /* icsp as verified */
  end. /* icsw does not exist */
  run Find_Whse (input x-prodtype).

  display blankline1
          blankline2
          blankline3
          blankline4
          blankline5
          blankline6
          blankline7
          blankline8
          blankline9
          blankline10
          blankline11
          blankline12
          blankline13
          blankline14
          blankline15
          blankline16
          blankline17
  with frame f-blank.
end. /* Procedure OTF_Routine */

/* ------------------------------------------------------------------------- */
Procedure Tag_SASOO:
/* ------------------------------------------------------------------------- */
  def input        parameter w-batchnm    as i format ">>>>>>>9" no-undo.
  def input        parameter ix-location  as c format "x(7)"     no-undo.
  for each b-sasoo where b-sasoo.cono   = g-cono and
                         b-sasoo.oper2  = g-operinit:
    assign b-sasoo.user1 = if w-batchnm <> 0 then 
                             string(w-batchnm,">>>>>>>9")
                           else " ".
           b-sasoo.user2 = ix-location.
  end.
end. /* Procedure Tag_SASOO */

/********** moved to oeexqckkitty.p
/* ------------------------------------------------------------------------- */
Procedure Check_KitType:
/* ------------------------------------------------------------------------- */
  def input        parameter v-prod    like icsp.prod          no-undo.
  def input        parameter v-quoteno as i format ">>>>>>>9"  no-undo.
  def input        parameter v-lineno  like oeel.lineno        no-undo.
  def input-output parameter v-whse    like icsw.whse          no-undo.
  def output       parameter errorfl   as   logical            no-undo.

  if not avail oeehb then
  find oeehb where 
             oeehb.cono = g-cono and
             oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
             oeehb.seqno   = 1 and
             oeehb.sourcepros = "Quote"
             no-lock no-error.
  if avail oeehb then
    assign h-whse = oeehb.whse.
  assign errorfl = no.
  /*if not avail icsp then*/
  find icsp where icsp.cono = g-cono and
                  icsp.prod = v-prod
                  no-lock no-error.
  if avail icsp and icsp.kittype = "B" then
    do:
    /* Check if Banner Whse was already set by a previous line */
    assign kitcount = 0.
    for each b-oeelb where 
             b-oeelb.cono = g-cono and
             b-oeelb.batchnm = string(v-quoteno,">>>>>>>9") and
             b-oeelb.seqno   = 1
             no-lock:
      find b-icsp where b-icsp.cono = g-cono and
                        b-icsp.prod = b-oeelb.shipprod and
                        b-icsp.kittype = "B"
                        no-lock no-error.
      if avail b-icsp and (substr(b-oeelb.user4,1,4) <> h-whse)   /*or 
                           (substr(b-oeelb.user3,1,4) <> v-whse)) */ then
        assign kitcount = kitcount + 1.
    end. /* for each */
    if kitcount >= 1 then
      do:
      find oeehb where 
           oeehb.cono = g-cono and
           oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
           oeehb.seqno   = 1 and
           oeehb.sourcepros = "Quote"
           no-lock no-error.
      if avail oeehb /*and (not can-find(icsw where icsw.cono = g-cono and
                                                    icsw.prod = v-prod and
                                                    icsw.whse = oeehb.whse
                                                    no-lock))*/ then
        assign v-whse  = oeehb.whse
               errorfl = yes.
    end. /* kitcount >= 1 */
    else
      do:
      find icsw where icsw.cono = g-cono and
                      icsw.whse = v-whse and
                      icsw.prod = v-prod
                      no-lock no-error.
      if avail icsw then
        do:
        for each oeehb where 
                 oeehb.cono = g-cono and
                 oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
                 oeehb.seqno   = 1 and
                 oeehb.sourcepros = "Quote":
          assign oeehb.whse = v-whse
                 h-whse   = v-whse.
        end.
        for each oeelk where oeelk.cono = g-cono and
                 oeelk.ordertype = "b" and
                 oeelk.orderno   = v-quoteno and
                 oeelk.ordersuf  = 00 and
                 oeelk.lineno    = v-lineno:
          assign oeelk.whse = v-whse
                 oeelk.arpwhse = if icsw.arptype = "w" then icsw.arpwhse 
                                 else "".
          overlay(oeelk.user4,1,4) = v-whse.
        end. /* each oeelk */
      end. /* avail icsw */
    end. /* kitcount < 1 */
    /*
    else
      do:
      for each icsw where icsw.cono = g-cono and
                          icsw.prod = v-prod and
                          substr(icsw.whse,1,1) <> "c" and
                          icsw.statustype <> "x"
                          no-lock:
        find icsd where icsd.cono    = g-cono and
                        icsd.salesfl = yes and
                        icsd.whse    = icsw.whse and
                        icsd.custno  = 0
                        no-lock no-error.
        if avail icsd then
          do:
          assign v-whse = icsw.whse.
          for each oeehb where
                   oeehb.cono       = g-cono and
                   oeehb.batchnm    = string(v-quoteno,">>>>>>>9") and
                   oeehb.seqno      = 1 and
                   oeehb.sourcepros = "Quote":
            assign oeehb.whse = v-whse
                   h-whse   = v-whse.
          end.
          for each oeelk where oeelk.cono = g-cono and
                   oeelk.ordertype = "b" and
                   oeelk.orderno   = v-quoteno and
                   oeelk.ordersuf  = 00 and
                   oeelk.lineno    = v-lineno:
            assign oeelk.whse = v-whse
                   oeelk.arpwhse = if icsw.arptype = "w" then icsw.arpwhse
                                   else "".
            overlay(oeelk.user4,1,4) = v-whse.
          end.
          leave.
        end. /* avail icsd */
      end. /* for each icsw */
    end. /* whse wasn't manually changed */
    */
  end. /* avail icsp and icsp.kittype = "B" */
end. /* Procedure Check_KitType */
**************/

/* ------------------------------------------------------------------------- */
Procedure Check_Nonstock_BODKits:
/* ------------------------------------------------------------------------- */
  def input        parameter v-quoteno as i format ">>>>>>>9"  no-undo.
  def input        parameter v-lineno  like oeel.lineno        no-undo.
  def input        parameter h-whse  like icsw.whse          no-undo.
  def input-output parameter v-whse    like icsw.whse          no-undo.
  def output       parameter errorfl   as   logical            no-undo.

  find oeehb where 
             oeehb.cono = g-cono and
             oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
             oeehb.seqno   = 1 and
             oeehb.sourcepros = "Quote"
             no-lock no-error.
  assign h-whse = oeehb.whse.
  assign errorfl = no.
  if v-whse <> h-whse and h-whse <> " " then
    do:
    /* Check if Banner Whse was already set by a previous line */
    assign kitcount = 0.
    for each b-oeelb where 
             b-oeelb.cono = g-cono and
             b-oeelb.batchnm = string(v-quoteno,">>>>>>>9") and
             b-oeelb.seqno   = 1 and
             b-oeelb.lineno <> v-lineno and
      substr(b-oeelb.user5,1,1) = "y"
             no-lock:
      assign kitcount = kitcount + 1.
    end. /* for each b-oeelb */
    if kitcount >= 1 then
      do:
      find oeehb where 
           oeehb.cono = g-cono and
           oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
           oeehb.seqno   = 1 and
           oeehb.sourcepros = "Quote"
           no-lock no-error.
      if avail oeehb /* and (not can-find(icsw where icsw.cono = g-cono and
                                                  icsw.prod = v-prod and
                                                  icsw.whse = oeehb.whse
                                                  no-lock)) */ then
        assign v-whse  = oeehb.whse
               errorfl = yes.
    end. /* if kitcount >= 1 */
    else
      do:
      for each oeehb where
               oeehb.cono       = g-cono and
               oeehb.batchnm    = string(v-quoteno,">>>>>>>9") and
               oeehb.seqno      = 1 and
               oeehb.sourcepros = "Quote":
        assign oeehb.whse = v-whse.
      end.
    end. /* kitcount = 0 */
  end. /* v-whse <> h-whse and h-whse <> " " */
end. /* Procedure Check_Nonstock_BODKits */


/* ------------------------------------------------------------------------- */
Procedure Handle_Currency:
/* ------------------------------------------------------------------------- */
  def input         parameter v-quoteno as i format ">>>>>>>9"      no-undo.
  def input         parameter v-currencytype as c format "x(2)"     no-undo.
  def input-output  parameter v-currencyty   as c format "x(1)"     no-undo.
  def var                     x-currencyty   as c format "x(1)"     no-undo.
  form 
    x-currencyty colon  29 label "(C)anadian or (U)S Currency?"
    with frame f-CNcurr overlay side-labels row 5 centered.
    
  if v-currencytype = "CN" then
    do:
    display x-currencyty with frame f-CNcurr.
    
    CNcurrency:
    do with frame f-CNcurr on endkey undo, leave CNcurrency:
      update  x-currencyty 
      with frame f-CNcurr
      editing:
        readkey.
      if can-do("13,401,404",string(lastkey)) and 
         input x-currencyty <> "C" and
         input x-currencyty <> "U" then
        do:
        message "Enter C or U".
        next-prompt x-currencyty with frame f-CNcurr.
        next.
      end.
      apply lastkey.
      if (input x-currencyty = "C" or input x-currencyty = "U") and
        can-do("13,401",string(lastkey)) then
        do:
        assign x-currencyty = input x-currencyty.
        find b-oeehb where 
             b-oeehb.cono    = g-cono and
             b-oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
             b-oeehb.seqno   = 1 and
             b-oeehb.sourcepros = "Quote"
             no-error.
        if avail b-oeehb then
          do:
          assign substr(b-oeehb.user5,1,1) = x-currencyty.
          if x-currencyty = "c" then
            do:
            assign b-oeehb.divno = 40.
            /****** MOVED TO oeehb_create.i
            *find sastc where sastc.cono = g-cono and
            *                 sastc.currencyty = "CN"
            *                 no-lock no-error.
            *if avail sastc then
            *  assign b-oeehb.user6 = if sastc.purchexrate > 0 then
            *                           1 / sastc.purchexrate
            *                         else
            *                           1.
            *else
            *  assign b-oeehb.user6 = 1.
            ******/
          end. /* if x-currencyty = "c" */
          /*****
          *else
          *  assign b-oeehb.user6 = 1.
          *****/
        end. /* if avail b-oeehb */
        leave.
      end. /* x-currencyty = "C" or "U" */
    end. /* editing */
    end. /* do while */
    hide frame f-CNcurr.
  end. /* v-currencytype = "CN" */
  /*******
  else
    do:
    if v-currencytype = "LB" then
      do:
      find b-oeehb where 
           b-oeehb.cono    = g-cono and
           b-oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
           b-oeehb.seqno   = 1 and
           b-oeehb.sourcepros = "Quote"
           no-error.
      if avail b-oeehb then
        do:
        find sastc where sastc.cono = g-cono and
                         sastc.currencyty = "LB"
                         no-lock no-error.
        if avail sastc then 
          assign b-oeehb.user6 = if sastc.purchexrate > 0 then
                                   1 / sastc.purchexrate
                                 else
                                   1.
        else
          assign b-oeehb.user6 = 1.
      end. /* if avail b-oeehb */
    end. /* if v-currencytype = "LB" */
  end. /* else */
  *******/
end. /* Procedure Handle_Currency */


/* ------------------------------------------------------------------------- */
Procedure Process_Contact:
/* ------------------------------------------------------------------------- */
  def input parameter v-custno like arsc.custno    no-undo.
  def input parameter v-shipto like arss.shipto    no-undo.
  def input parameter v-phone  as c format "x(12)" no-undo.

  find first notes where 
                   notes.cono         = g-cono and 
                   notes.notestype    = "zz" and
                   notes.primarykey   = "slsmx" and
                   notes.secondarykey = 
                      string(v-custno,"99999999999") +
                             v-shipto
                   no-error. 
  if avail notes then 
    do:
    v-inx = num-entries(notes.user3,"^").
    if v-inx > 0 then
      do:
      do i = 1 to v-inx:
        if v-contact = entry(i,notes.user3,"^") then
          do:
          assign entry(i,notes.user5,"^") = v-phone.
          leave.
        end.
      end. /* i = 1 to v-inx */
    end. /* v-inx > 0 */
  end. /* if avail notes */
end. /* procedure Process_Contact */

/* ------------------------------------------------------------------------- */
Procedure Get_Tax_Flag:
/* ------------------------------------------------------------------------- */
  def input-output parameter v-taxablefl  as logical          no-undo.
  def input-output parameter v-nontaxtype as c format "x(2)"  no-undo.
  
  /*if not avail icsw then*/
    find icsw where icsw.cono = g-cono and
                    icsw.whse = v-whse and
                    icsw.prod = v-prod
                    no-lock no-error.
  if avail icsw then
    do:
    if icsw.taxablety = "Y" then
      assign v-taxablefl  = yes
             v-nontaxtype = "".
    if icsw.taxablety = "N" then
      assign v-taxablefl  = no
             v-nontaxtype = icsw.nontaxtype.
    if icsw.taxablety = "V" then
      do:
      if avail oeehb then 
        assign v-taxablefl = oeehb.taxablefl
               v-nontaxtype = oeehb.nontaxtype.
      else         
      if avail arss then
        do:
        if arss.taxablety = "y" then 
          assign v-taxablefl  = yes
                 v-nontaxtype = "".
        else 
          assign v-taxablefl  = no
                 v-nontaxtype = arss.nontaxtype.
      end. /* avail arss */
      else
        do:
        if avail arsc then
          do:
          if arsc.taxablety = "y" then 
            assign v-taxablefl  = yes
                   v-nontaxtype = "".
          else 
            assign v-taxablefl  = no
                   v-nontaxtype = arsc.nontaxtype.
        end. /* avail arsc */
        else
          assign v-taxablefl  = yes
                 v-nontaxtype = "".
      end. /* else */
    end.
    if avail arss then
      do:
      if arss.taxablety = "N" then 
          assign v-taxablefl  = no
                 v-nontaxtype = arss.nontaxtype.
    end. /* avail arss */
    else
      do:
      if avail arsc then
        do:
        if arsc.taxablety = "N" then 
            assign v-taxablefl  = no
                   v-nontaxtype = arsc.nontaxtype.
      end. /* avail arsc */
    end.
  end. /* avail icsw */
  else /* not avail icsw */
    do:

    if avail oeehb then 
      assign v-taxablefl = oeehb.taxablefl
             v-nontaxtype = oeehb.nontaxtype.
    else
    if avail arss then
      do:
      if arss.taxablety = "y" then 
        assign v-taxablefl  = yes
               v-nontaxtype = "".
      else 
        assign v-taxablefl  = no
               v-nontaxtype = arss.nontaxtype.
    end. /* avail arss */
    else
      do:
      if avail arsc then
        do:
        if arsc.taxablety = "y" then 
          assign v-taxablefl  = yes
                 v-nontaxtype = "".
        else 
          assign v-taxablefl  = no 
                 v-nontaxtype = arsc.nontaxtype.
      end. /* avail arsc */
      else
        assign v-taxablefl  = yes
               v-nontaxtype = "".
    end. /* else check arsc */
  end. /* not avail icsw */

  if v-taxablefl = yes then
    do:
    /**
    if avail oeehb then do:
     /* this should be taken care of from the header determination */
    end.
    else
    **/
    if avail arss then
      do:
      if arss.taxablety = "n" then
        assign v-taxablefl  = no
               v-nontaxtype = arss.nontaxtype.
    end.
    else
      if avail arsc and arsc.taxablety = "n" then
        assign v-taxablefl  = no
               v-nontaxtype = arsc.nontaxtype.
  end.      

end. /* procedure Get_Tax_Flag */

/* ------------------------------------------------------------------------- */
Procedure Load_Variables:
/* ------------------------------------------------------------------------- */
  
  assign v-enterdt      = oeelb.enterdt
         v-shipdt       = /* oeelb.reqshipdt */ oeelb.promisedt
         v-NR           = substr(oeelb.user10,1,1)
         v-xxda1        = oeelb.xxda1
         v-prod         = oeelb.shipprod
         v-sellprc      = oeelb.price
         v-surcharge    = oeelb.datccost
         v-sparebamt    = oeelb.user6
         v-priceoverfl  = oeelb.priceoverfl
         v-ptype        = oeelb.pricetype
         v-specnstype   = oeelb.specnstype
         v-usagefl      = oeelb.usagefl
         v-rushfl       = oeelb.rushfl
         v-costoverfl   = oeelb.costoverfl
         orig-cost      = dec(substr(oeelb.user1,2,7))
         over-cost      = dec(substr(oeelb.user1,2,7))
         v-qty          = oeelb.qtyord 
         v-reqprod      = oeelb.reqprod 
         v-xrefty       = oeelb.xrefprodty
         v-unit         = oeelb.unit
         v-leadtm       = oeelb.leadtm 
         v-commentfl    = oeelb.commentfl
         v-icspecrecno  = oeelb.icspecrecno
         v-prodcat      = oeelb.prodcat
         v-prodline     = oeelb.prodline
         v-taxgroup     = oeelb.taxgroup
         v-gststatus    = oeelb.gststatus
         v-totnet       = oeelb.netamt
         v-whse         = substr(oeelb.user4,1,4)
         v-gp           = dec(substr(oeelb.user4,5,7))
         v-listprc      = dec(substr(oeelb.user4,12,13))         
         v-listfl       = substr(oeelb.user4,25,1)
       /*v-shipdt       = date(string(substr(oeelb.user4,26,8),"99/99/99"))*/
         v-disc         = dec(substr(oeelb.user4,34,7))
         v-disctype     = if substr(oeelb.user4,40,1) = "%" then no else yes
         zi-qtybrkty    = substr(oeelb.user4,41,1)
         v-pdrecord     = int(substr(oeelb.user4,42,8))
         v-pdtype       = int(substr(oeelb.user4,50,1))
         v-pdamt        = dec(substr(oeelb.user4,51,10))
         v-pd$md        = substr(oeelb.user4,61,1)
         v-totnet       = dec(substr(oeelb.user4,62,11))         
         v-lcomment     = substr(oeelb.user4,74,1)
         v-pdbased      = substr(oeelb.user4,75,4)
         v-totcost      = dec(substr(oeelb.user3,44,11))         
         v-rebatefl     = substr(oeelb.user3,55,1)
         v-totmarg      = dec(substr(oeelb.user3,56,11))         
         v-margpct      = dec(substr(oeelb.user3,67,11))         
         v-kitrollty    = substr(oeelb.user3,78,1)
         v-kitfl        = if substr(oeelb.user5,1,1) = "y" then yes else no
         v-ordertype    = oeelb.ordertype
         v-slsrepin     = oeelb.slsrepin
         v-slsrepout    = oeelb.slsrepout
         v-nontaxtype   = oeelb.nontaxtype
         v-botype       = oeelb.botype
         v-prodcost     = oeelb.prodcost
         v-glcost       = dec(oeelb.user2)
         v-vendno       = oeelb.vendno
         v-taxablefl    = oeelb.taxablefl
         v-xprod        = substr(oeelb.user5,30,24)
         v-descrip2     = oeelb.proddesc2
         v-OLTDays      = int(substr(oeelb.user12,10,4)).
  find icsp where icsp.cono = g-cono and
                  icsp.prod = v-prod
                  no-lock no-error.
  if avail icsp then 
    assign v-icnotefl = icsp.notesfl.
  else
    assign v-icnotefl = "".
end. /* Load_Variables */


/* ------------------------------------------------------------------------- */
Procedure OEELK_BOD_Create:
/* ------------------------------------------------------------------------- */
  assign h-prod = v-prod.
  run Create_OEELK_BOD (input w-batchnm,
                        input v-prod,
                        input v-whse,
                        input v-quoteno,
                        input v-lineno,
                        input-output v-xprod,
                        input-output v-kitlineno).
  /* loosing v-prod and icsp prod when creating OEELK BOD */
     
  assign v-prod = h-prod.
  run Roll_Kit_Cost-Price (input "c",
                           input-output v-sellprc,
                           input-output v-kitcost,
                           input-output v-gp).
  
  find icsp where icsp.cono = g-cono and
                              icsp.prod = v-prod
                              no-lock no-error.
  assign v-prodcost  = v-kitcost
                       v-kitfl     = yes
                       v-kitlineno = v-lineno
                       v-kittingl  = yes
                       v-linebrowseopen = false.
end. /* Procedure OEELK_BOD_Create */

/* ------------------------------------------------------------------------- */
Procedure Convert_Option:
/* ------------------------------------------------------------------------- */
/* TAH FG */
    
  form
     v-cnvfl  at 1 label
                  "Would you like to convert this quote to an order?"
     v-commissionmsg at 1 no-label  format "x(49)"
     v-commissionfl at 52 no-label
    
  with frame f-cnv side-labels row 5 centered overlay.

  assign v-checkingfl = true.
/* TAH FG */ 
  
  /* EN-438 OEEXQ Update LIne Promise Dates on Ship Complete Quotes */
  if avail arsc and arsc.orderdisp = "S" then
    do transaction:
    find u-oeehb where 
         u-oeehb.cono = g-cono and  
         u-oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
         u-oeehb.seqno      = 1 and
         u-oeehb.sourcepros = "Quote" and
         u-oeehb.stagecd <> 9
         no-error.
    if avail u-oeehb then 
      do:
      assign w-date = ?.
      for each oeelb where oeelb.cono    = g-cono and
                           oeelb.batchnm = u-oeehb.batchnm and
                           oeelb.seqno   = u-oeehb.seqno and
                           oeelb.specnstype <> "L"
                           no-lock: 
        if w-date = ? or w-date < oeelb.promisedt then
          assign w-date = oeelb.promisedt.
      end. /* for each oeelb */   
      for each u-oeelb where u-oeelb.cono      = g-cono and
                             u-oeelb.batchnm   = u-oeehb.batchnm and
                             u-oeelb.seqno     = u-oeehb.seqno and
                             u-oeelb.specnstype <> "L":
        assign u-oeelb.promisedt = w-date.
      end.
      for each t-lines:
        assign t-lines.shipdt = w-date.
      end.
    end. /* avail u-oeehb */
  end. /* orderdisp = "S" */
   
  
  
  /*** das - removed only to process at the completion of the line 
  run oeexqSauerAPR.p (input "L").
  hide message no-pause.
  if zsdiconfirm = no then
    do:
    message color normal apr-msg.
    bell.
    next-prompt v-prod with frame f-mid1.
    next.
  end. /* zsdiconfirm = no */
  ***/
  assign v-nextmainfl = no.
  /* %toms% 3 */
  assign x-backed-quote-up = false.
  /* %toms% 3 */
  do transaction:
    find oeehb where oeehb.cono = g-cono and  
                     oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
                     oeehb.seqno      = 1 and
                     oeehb.sourcepros = "Quote" and
                     oeehb.stagecd <> 9
                     no-error.
    if avail oeehb then 
      do:
/* TAH FG */
      assign v-commissionfl = if substring(oeehb.user15,1,1) = "Y" then
                                true
                              else
                                false.
/* TAH FG */

      assign oeehb.promisedt = TODAY
             oeehb.totdatccost = if lastkey = 401 then 0 else oeehb.totdatccost
             g-whse          = oeehb.whse.
      for each t-lines no-lock:
        assign oeehb.totdatccost = if lastkey = 401 then
               oeehb.totdatccost + (t-lines.surcharge * t-lines.qty) 
                                  else oeehb.totdatccost.
        if t-lines.shipdt > oeehb.promisedt then
          assign oeehb.promisedt = t-lines.shipdt.
      end.
      assign same-whse = yes
             d-whse    = " ".
      find first oeelb where oeelb.cono = g-cono and
                             oeelb.batchnm = oeehb.batchnm and
                             oeelb.seqno   = oeehb.seqno
                             no-lock no-error.
      if avail oeelb then
        assign d-whse = substr(oeelb.user4,1,4).
      for each oeelb where oeelb.cono    = g-cono and
                           oeelb.batchnm = oeehb.batchnm and
                           oeelb.seqno   = oeehb.seqno
                           no-lock:
        if d-whse <> substr(oeelb.user4,1,4) then
          assign same-whse = no.
      end.
      if same-whse = yes and oeehb.whse <> d-whse then
        assign oeehb.whse = d-whse.
      /*
      if lastkey = 401 then
        do:
        */
        if (oeehb.countrycd = "CN" or oeehb.countrycd = "LB") and
           substr(oeehb.user5,1,1) = " " then
          do:
          run Handle_Currency (input v-quoteno,
                               input oeehb.countrycd,
                               input-output v-currencyty).
        end. /* oeehb.countrycd = "CN" or "LB" */
      /*
      end. /* lastkey = 401 */
      */
    end.
    else
      do:
      run Clear-Frames.
      run Initialize-All.
      run Setup-Options.
      display v-quoteno  v-notefl   v-custno   v-arnotefl v-custname 
              v-takenby  v-custlu   v-shipto   v-contact  v-phone 
              v-custpo   v-currency v-terms    v-compfocus
              v-canceldt v-quotetot v-quotemgn%
      with frame f-top1.
      release oeehb no-error.
      release oeelb no-error.
      release notes no-error.
      /*
      next main.
      */
      assign v-nextmainfl = yes.
    end.
  end. /* transaction */
  cnvquote:
  do while v-checkingfl on endkey undo cnvquote, leave cnvquote:
/* TAH FG */
    if can-find( first sasos where 
                       sasos.cono = g-cono and
                       sasos.oper2 = g-operinits and
                       sasos.menuproc = "zxt3" and 
                       sasos.securcd[1] > 1 no-lock) then
      assign v-commissionmsg = 
        "Would you like to this to be a  commission order?".                        else 
      assign v-commissionmsg = " ". 
    
    display v-commissionmsg with frame f-cnv.
/* TAH FG */
    
    update v-cnvfl label
    "Would you like to convert this quote to an order?"
/* TAH FG */
           v-commissionfl at 52 no-label
            when  can-find( first sasos where 
                                  sasos.cono = g-cono and
                                  sasos.oper2 = g-operinits and
                                  sasos.menuproc = "zxt3" and 
                                  sasos.securcd[1] > 1 no-lock)                 /* TAH FG */    
    
    with frame f-cnv side-labels row 5 centered overlay.
 /* %toms% */

    if v-cnvfl = yes and v-commissionfl = yes then do:
      message "Commission Quotes cannot be converted".
      next cnvquote.
    end.  
    else if v-commissionfl = yes then do:
      do transaction:
        find oeehb where oeehb.cono = g-cono and  
                         oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
                         oeehb.seqno      = 1 and
                         oeehb.sourcepros = "Quote" no-error.
        if avail oeehb then do:
          overlay(oeehb.user15,1,1) = "Y".
        end.
      end.
    end.
    else if v-commissionfl = no then do:
      do transaction:
        find oeehb where oeehb.cono = g-cono and  
                         oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
                         oeehb.seqno      = 1 and
                         oeehb.sourcepros = "Quote" no-error.
        if avail oeehb then do:
          overlay(oeehb.user15,1,1) = "N".
        end.
      end.
    end.



    assign v-checkingfl = false.




    if v-cnvfl = yes then 
      do:
      hide frame f-cnv no-pause.
      find first b-oeehb where b-oeehb.cono = g-cono and  
                 b-oeehb.batchnm = string(v-quoteno,">>>>>>>9")
                 no-lock no-error.    
      if avail b-oeehb then 
        do:
        assign ip-update = true.
        run Conversion (input recid(b-oeehb)).
        if ip-update then 
          do:
          run Check_Quote_Restore.
          run Check_Quote_Mortality.
          if can-find(first oeeh where 
                            oeeh.cono      = g-cono and
                            oeeh.orderno   = g-orderno and
                            oeeh.ordersuf  = 0 and
                            oeeh.enterdt   = TODAY and
                            oeeh.approvty <> "y" no-lock) then
            do:
            find first poelo where poelo.cono = g-cono and
                                   poelo.ordertype = "o" and
                                   poelo.orderaltno = g-orderno and
                                   poelo.orderaltsuf = 0
                                   no-lock no-error.
            if avail poelo then
              assign tied-po = poelo.pono.
            else
              assign tied-po = 0.
            find first wtelo where wtelo.wtcono = g-cono and
                                   wtelo.ordertype = "o" and
                                   wtelo.orderaltno = g-orderno and
                                   wtelo.orderaltsuf = 0
                                   no-lock no-error.
            if avail wtelo then
              assign tied-wt = wtelo.wtno.
            else
              assign tied-wt = 0.
            if tied-po = 0 and tied-wt = 0 then
              message "Customer Order " + string(ip-orderno) + " created". 
            if tied-po > 0 then
              message "Customer Order " + string(ip-orderno) + " created. " +
                      "Purchase Order " + string(tied-po) + " created.".
            if tied-wt > 0 then
              message "Customer Order " + string(ip-orderno) + " created. " +
                      "WHSE Transfer " + string(tied-wt) + " created.".
            run warning.p(8624).
            if s-oelockfl = yes then
              run Set_OE_Lock (input ip-orderno).
          end. /* if can-find first */
          else 
            if can-find(first oeeh where
                              oeeh.cono     = g-cono and
                              oeeh.orderno  = g-orderno and
                              oeeh.ordersuf = 0 and
                              oeeh.enterdt  = TODAY no-lock) then
              do:
              find first poelo where poelo.cono = g-cono and
                                     poelo.ordertype = "o" and
                                     poelo.orderaltno = g-orderno and
                                     poelo.orderaltsuf = 0
                                     no-lock no-error.
              if avail poelo then
                assign tied-po = poelo.pono.
              else
                assign tied-po = 0.
              find first wtelo where wtelo.wtcono = g-cono and
                                     wtelo.ordertype = "o" and
                                     wtelo.orderaltno = g-orderno and
                                     wtelo.orderaltsuf = 0
                                     no-lock no-error.
              if avail wtelo then
                assign tied-wt = wtelo.wtno.
              else
                assign tied-wt = 0.
              if tied-po = 0 and tied-wt = 0 then
                message "Customer Order " + string(ip-orderno) + " created". 
              if tied-po > 0 then
                message "Customer Order " + string(ip-orderno) + " created. " +
                        "Purchase Order " + string(tied-po) + " created.".
              if tied-wt > 0 then
                message "Customer Order " + string(ip-orderno) + " created. " +
                        "WHSE Transfer " + string(tied-wt) + " created.".
              pause.
              if s-oelockfl = yes then
                run Set_OE_Lock (input ip-orderno).
            end. /* if can find first */
          run Clear-Frames.
          /*if v-faxpopup and errorfl = no then*/
          if v-faxpopup then
            do:
            if errorfl = yes then
              run oeexqg.p.
            else
              run oeexqgo.p.
          end.
          leave cnvquote.
          hide frame f-cnv no-pause.
        end. /* if ip-update */
        else 
          do:
          run Check_Quote_Restore.
          run Clear-Frames.
          if v-faxpopup and errorfl = no then
            run oeexqg.p.
          hide frame f-cnv no-pause.
          leave cnvquote.
        end.
      end. /* avail oeehb */
      else  
        do:
        run Check_Quote_Restore.
        hide frame f-cnv no-pause.
        assign v-blanks = string(" ","x(76)").         
        put screen row 22 col 3 v-blanks.
        run Clear-Frames.
        /*if v-faxpopup and errorfl = no then*/
        if v-faxpopup then
          do:
          if errorfl = yes then
            run oeexqg.p.
          else
            run oeexqgo.p.
        end.
        hide frame f-cnv no-pause.
        leave cnvquote.
      end. /* not avail oeehb */
    end. /* v-cnvfl = yes */
    else
      do:
      if lastkey = 404 then
        do:
        if v-faxpopup and errorfl = no then
          run oeexqg.p.
      end.
    end.
  end. /* cnvquote */
  run Check_Quote_Restore.
  hide frame f-cnv no-pause.
end. /* Procedure Convert_Option */
 
 
/* ------------------------------------------------------------------------- */
Procedure Create_Addons:
/* ------------------------------------------------------------------------- */
  do transaction:
  for each addon where addon.cono = g-cono and
                       addon.ordertype = "oe*" + oeehb.batchnm and
                       addon.orderno   = 0 and
                       addon.ordersuf = oeehb.seqno:
    delete addon.
  end.
  end.
  
  release addon.
  if oeehb.shipto <> "" then
    find a-arss where a-arss.cono   = g-cono and
                      a-arss.custno = oeehb.custno and
                      a-arss.shipto = oeehb.shipto
                      no-lock no-error.
  else
    find a-arsc where a-arsc.cono   = g-cono and
                      a-arsc.custno = oeehb.custno
                      no-lock no-error.
  
  /***************
  if avail arss then
    export stream addon_aud delimiter ","
      oeehb.batchnm oeehb.custno oeehb.shipto a-arss.custno a-arss.shipto
      a-arss.addonnum[1] a-arss.addonnum[2] a-arss.addonnum[3].
  else
    export stream addon_aud delimiter ","
      oeehb.batchnm oeehb.custno oeehb.shipto a-arsc.custno " "
      a-arsc.addonnum[1] a-arsc.addonnum[2] a-arsc.addonnum[3].
  ***************/
     
  do transaction:
  do i = 1 to 8:
    assign x-addonno = if avail a-arss then a-arss.addonnum[i]
                       else a-arsc.addonnum[i].
    if x-addonno > 0 or i le 4 then
      do:
      create addon.
      assign addon.cono = g-cono
             addon.ordertype = "oe*" + oeehb.batchnm
             addon.orderno   = 0
             addon.ordersuf  = oeehb.seqno
             addon.seqno     = i
             addon.addonno   = x-addonno 
             j-inx           = addon.addonno.
          
      
      find sastn where sastn.cono = g-cono and
                       sastn.codeiden = "a" and
                       sastn.codeval  = addon.addonno
                       no-lock no-error.
      if avail sastn then
        assign addon.addontype = sastn.addontype
               addon.addonamt  = if sastn.addontype = yes then 0 
                                 else sastn.addonamt.
      else
        assign addon.addontype = yes
               addon.addonamt  = 0.
      if g-country = "ca":u then 
        do:
        {w-sasga.i ""s"" 0 0 oeehb.statecd """" """" """" x-addonno no-lock}
        if avail sasga then
          addon.addtaxgroup = sasga.addontaxgroup.
      end.
      else 
        do:
        {w-sasga.i  ""m"" 2 1 oeehb.statecd """" """" """" x-addonno no-lock}
        if avail sasga then
          addon.addtaxgroup = sasga.addontaxgroup.
      end.
    end. /* x-addonno > 0 or i le 4 */
  end. /* i = 1 to 8 */
  end.
  release addon.
end. /* Procedure Create_Addons */

/* ------------------------------------------------------------------------- */
Procedure Set_OE_Lock:
/* ------------------------------------------------------------------------- */
  define input        parameter ip-orderno like oeeh.orderno       no-undo.
  for each oeeh where oeeh.cono = g-cono and
                    oeeh.orderno = ip-orderno and
                    oeeh.ordersuf = 0 and
                    oeeh.transtype = "FO":
    if s-oelockfl = yes then
      assign oeeh.lockfl = yes.
  end.
end. /* Procedure Set_OE_Lock */

/* ------------------------------------------------------------------------- */
Procedure Catalog_Audit:
/* ------------------------------------------------------------------------- */
  if s-yn = yes then
    do:
    if not avail icsp then
      do:
      find icsp where icsp.cono = g-cono and
                      icsp.prod = v-prod
                      no-lock no-error.
      if avail icsp then
        do:
        find icsw where icsw.cono = g-cono and
                        icsw.whse = v-whse and
                        icsw.prod = v-prod no-lock no-error.
        if avail icsw then
          do:
          find first icsl where icsl.cono     = g-cono and
                                icsl.whse     = icsw.whse and
                                icsl.vendno   = icsw.arpvendno and
                                icsl.prodline = icsw.prodline
                                no-lock no-error.
        end. /* avail icsw */
      end. /* avail icsp */
    end. /* not avail icsp */
  end. /* s-yn = yes */

  assign x-catlineno = v-lineno * 1000.
  if x-prodtype = "comp" then assign x-catlineno = x-catlineno + v-seqno.
  
  find first zidc use-index docinx4 where
             zidc.cono       = g-cono and
             zidc.docstatus  = (if s-yn = yes then 50 else 51) and
             zidc.docttype   = "CAT" and
             zidc.doctype    = "oeexq" and
             substring(zidc.docpartner,1,4)  = v-whse and
             substring(zidc.docpartner,5,24) = v-prod and
             zidc.docgsnbr   = x-catlineno
             no-lock no-error.
  if not avail zidc then 
    do:
    create zidc.
    assign zidc.cono       = g-cono
           zidc.docnumber  = v-quoteno
           zidc.docdate    = TODAY
           substring(zidc.docpartner,1,4)  = if avail icsw then icsw.whse
                                             else v-whse
           substring(zidc.docpartner,5,24) = if avail icsw then icsw.prod
                                             else v-prod
           substring(zidc.docdesc,1,24) = if avail icsp then icsp.descrip[1]
                                          else 
                                          if avail icsc then icsc.descrip[1]
                                          else v-descrip
           substring(zidc.docdesc,27,4) = g-operinit
           zidc.docstatus  = if avail icsp then 51 else 50
           zidc.docdisp    = ""
           zidc.docstnbr   = 0
           zidc.docfadate  = ?
           zidc.doctype    = "OEEXQ"
           zidc.docgsnbr   = x-catlineno
           zidc.docttype   = "CAT"
           substring(zidc.docuser1,1,6) = if avail icsw then icsw.prodline
                                          else if avail icsc then icsc.prodline
                                          else "      "
           substring(zidc.docuser1,7,24) = if avail icsp then icsp.descrip[1]
                                           else if avail icsc then 
                                             icsc.descrip[1]
                                           else v-descrip
           substring(zidc.docuser2,1,15) = "               "
           substring(zidc.docuser2,17,4) = if avail icsp then icsp.prodcat
                                           else v-prodcat
           substring(zidc.docuser3,1,24) = if avail icsw then icsw.prod
                                           else v-prod
           substring(zidc.docuser3,27,4) = if avail icsw then icsw.pricetype
                                           else v-ptype
           substring(zidc.docuser4,1,4)  = if avail icsw then icsw.whse
                                           else v-whse
           substring(zidc.docuser4,6,12) = if avail icsw then 
                                             string(icsw.arpvendno)
                                           else
                                           if avail icsc then
                                             string(icsc.vendno)
                                           else
                                             string(v-vendno)
           zidc.docvendno  = if avail icsw then icsw.replcost
                             else if avail icsc then icsc.prodcost
                             else v-prodcost
           zidc.docstnbr   = if avail icsw then icsw.leadtmavg
                             else if avail icsc then int(icsc.user6) else 30
           zidc.docdisp = "".
  end. /* not avail zidc */
end. /* Procedure Catalog_Audit */

/* ------------------------------------------------------------------------- */
Procedure Update_ShipTo_Info:
/* ------------------------------------------------------------------------- */
  if avail arss then
    do:
    for each t-oeehb where 
             t-oeehb.cono = g-cono and  
             t-oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
             t-oeehb.seqno      = 1 and
             t-oeehb.sourcepros = "Quote":
      if v-mode = "C" or v-chgfl = yes then
        do:
        assign t-oeehb.shipto        = arss.shipto
               t-oeehb.orderdisp     = arss.orderdisp
               t-oeehb.shiptonm      = if v-shiptonm <> "" then
                                         v-shiptonm else arss.name
               t-oeehb.shiptoaddr[1] = if v-shiptoaddr1 <> "" then
                                         v-shiptoaddr1 else arss.addr[1]
               t-oeehb.shiptoaddr[2] = if v-shiptoaddr2 <> "" then
                                         v-shiptoaddr2 else arss.addr[2]
               t-oeehb.shiptocity    = if v-shiptocity <> "" then
                                         v-shiptocity else arss.city
               t-oeehb.shiptost      = if v-shiptost <> "" then
                                         v-shiptost else arss.state
               t-oeehb.shiptozip     = if v-shiptozip <> "" then
                                         v-shiptozip else arss.zip
               t-oeehb.geocd         = if v-shiptogeocd <> 0 then
                                         v-shiptogeocd else arss.geocd
               t-oeehb.slsrepout     = arss.slsrepout
               t-oeehb.slsrepin      = arss.slsrepin 
               t-oeehb.codfl         = if arss.termstype = "COD" then 
                                         yes else no
               t-oeehb.outbndfrtfl   = arss.outbndfrtfl /*das-03/02/09*/
               t-oeehb.inbndfrtfl    = arss.inbndfrtfl  /*das-03/02/09*/
               t-oeehb.taxablefl     = if arss.taxablety = "n" then no
                                     else yes.
        for each t-oeelb where t-oeelb.cono = g-cono and
                               t-oeelb.batchnm = t-oeehb.batchnm and
                               t-oeelb.seqno   = t-oeehb.seqno:
          assign t-oeelb.slsrepout  = arss.slsrepout
                 t-oeelb.slsrepin     = arss.slsrepin.
        end. /* each oeelb */
        assign v-shiptonm    = if v-shiptonm = "" then arss.name
                               else v-shiptonm
               v-shiptoaddr1 = if v-shiptoaddr1 = "" then arss.addr[1]
                               else v-shiptoaddr1
               v-shiptoaddr2 = if v-shiptoaddr2 = "" then arss.addr[2]
                               else v-shiptoaddr2
               v-shiptocity  = if v-shiptocity = "" then arss.city
                               else v-shiptocity
               v-shiptost    = if v-shiptost = "" then arss.state
                               else v-shiptost
               v-shiptozip   = if v-shiptozip = "" then arss.zip
                               else v-shiptozip
               v-shiptogeocd = if v-shiptogeocd = 0 then arss.geocd
                               else v-shiptogeocd
               g-shipto      = arss.shipto
               v-slsrepout   = arss.slsrepout
               o-slsrepout   = arss.slsrepout
               v-slsrepin    = arss.slsrepin
               v-taxablefl   = if arss.taxablety = "n" 
                               then no else yes.
      end. /* if mode = "c" or chgfl = yes */
    end. /* each oeehb */
  end. /* avail arss */
end. /* Procedure Update_ShipTo_Info */
/***************************************************************************/
Procedure F7-Refresh-Dates:
/***************************************************************************/
  /*EN-437*/
  find first dtrfresh no-error.
  if avail dtrfresh then
    do:
    display with frame f-Changedates.   
    message color normal
       "<Enter> to mark with 'X'; F8 to OverRide date and assign OLT".
    open query q-dtrfresh for each dtrfresh.
    display b-dtrfresh with frame f-dtrfresh.
    on cursor-up cursor-up.
    on cursor-down cursor-down.
    F7-Refresh:
    do while true on endkey undo F7-Refresh, leave f7-Refresh:
      readkey.
      if lastkey = 404 then 
        do:
        hide frame f-dtrfresh no-pause.
        hide frame f-Changedates no-pause.
        leave F7-Refresh.
      end.
      if can-do("501,502,507,508",string(lastkey)) then
        do:
        run MoveRefreshDt (input lastkey).      
      end.
      if lastkey = 13 then
        do:
        if dtrfresh.seltype = "X" then
          assign dtrfresh.seltype  = "".
        else
          assign dtrfresh.seltype = "X".
        /*apply lastkey.    */
      end.
      if {k-func8.i} then
        do:
        display dtrfresh.lineno 
                dtrfresh.fut-date with frame f-ChangeFutureDt.
        update dtrfresh.fut-date with frame f-ChangeFuturedt.
        if lastkey = 13 then
          do:
          assign dtrfresh.fut-date = input dtrfresh.fut-date.
          assign dtrfresh.seltype  = "X".
          assign dtrfresh.OLT      = dtrfresh.fut-date - TODAY.
          hide frame f-ChangeFuturedt.
          display with frame f-blankline2.
        end.
      end. /*func8*/
      v-rowid = rowid(dtrfresh).
      open query q-dtrfresh for each dtrfresh.    
      display b-dtrfresh with frame f-dtrfresh.
      b-dtrfresh:refreshable = false.
      reposition q-dtrfresh to rowid(v-rowid).
      /*get next q-dtrfresh.*/
      b-dtrfresh:select-row (1).
      b-dtrfresh:refreshable = true.
      b-dtrfresh:refresh().
      
      if can-do("401",string(lastkey)) then
        do:
        run ReDate.
        close query q-dtrfresh.
        hide frame f-dtrfresh no-pause.
        hide frame f-new-refresh no-pause. 
        leave F7-Refresh.
      end. /* lastkey is RETURN or GO */    
    end. /* F7-Refresh */
    close query q-dtrfresh.
    hide frame f-dtrfresh no-pause.
  /*end. /* editing */  */
  end. /* if avail dtrfresh */
  hide frame f-dtrfresh no-pause. 

end. /* Procedure F7-Refresh-Dates */

/* ------------------------------------------------------------------------- */
Procedure Build_Date_Refresh:
/* ------------------------------------------------------------------------- */

  for each dtrfresh:
    delete dtrfresh.
  end.
  for each t-lines where t-lines.seqno = 0 no-lock:
    if t-lines.specnstype <> "l" then
      do:
      assign zi-displaylinefl = false
             v-whse           = t-lines.whse
             v-qty            = t-lines.qty
            /* v-custno         = g-custno*/
             v-prod           = t-lines.prod.
    end. /* t-lines.specnstype <> "n" and t-lines.specnstype <> "l" */
    assign s-netavail = 0.
    {w-icsw.i t-lines.prod t-lines.whse no-lock}
    if avail icsw then 
      do:
      {w-icsp.i icsw.prod no-lock}
      if avail icsp then
        do:
        {p-netavl.i}
        end.
    end.
    /*if not avail oeelb then*/
    find oeelb where oeelb.cono    = g-cono and
                     oeelb.batchnm = string(w-batchnm,">>>>>>>9") and
                     oeelb.seqno   = 1   and
                     oeelb.lineno  = t-lines.lineno
                     no-lock no-error.    
    create dtrfresh.
    assign dtrfresh.seltype    = ""
           dtrfresh.lineno     = t-lines.lineno
           dtrfresh.specnstype = t-lines.specnstype
           dtrfresh.prod       = t-lines.prod
           dtrfresh.whse       = t-lines.whse
           dtrfresh.qtyord     = t-lines.qty
           dtrfresh.qtyrsv     = s-netavail
           dtrfresh.cur-date   = t-lines.shipdt.
    if dtrfresh.cur-date = 09/09/99 then
      assign dtrfresh.fut-date   = TODAY + 
                                     if avail oeelb and
                                       int(substr(oeelb.user12,10,4)) > 0 then
                                       int(substr(oeelb.user12,10,4))
                                     else
                                       t-lines.leadtm.
    else
      assign dtrfresh.fut-date   = /*t-lines.shipdt*/ TODAY + 
                                     if avail oeelb and
                                       int(substr(oeelb.user12,10,4)) > 0 then
                                       int(substr(oeelb.user12,10,4))
                                     else
                                       t-lines.leadtm.
    if dtrfresh.fut-date < dtrfresh.cur-date then
      assign dtrfresh.fut-date = dtrfresh.cur-date.
    
    if avail oeelb then
      assign dtrfresh.OLT = int(substr(oeelb.user12,10,4)). 
    
  end. /* for each t-lines */
end. /* Procedure Build_Date_Refresh */


