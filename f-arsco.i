/*h*****************************************************************************
  INCLUDE      : f-arsco.i
  DESCRIPTION  : Customer Setup F7-Order Screen - Frame
  USED ONCE?   : no
  AUTHOR       : rnd
  DATE WRITTEN :
  CHANGES MADE :
    11/02/92 mms; TB# 8551  Rearranged screen and added new fields
    07/20/93 jrg; TB# 10584 Added title.
    07/06/95 emc; TB# 18812 7.0 Vendor Rebate Enhancement (a2a)
                      Add rebate type and rearrange screen.  Move screen
                      variable defines into arsco.gva
    11/19/97 jec; TB# 23693  IB add field for shipping label format
    05/11/99 cm;  TB# 5366   Added shipto notes
    08/24/99 bp ; TB# 26575  Add SPC default at the ARSC/ARSS level.  Here, add
        the spcdefaultty field and move all subsequent fields down one row.
    03/27/00 mwb; TB# e4651  Changed the SPC Default Descriptions.
    09/07/01 usb; TB# 10117  Add price-by custno - BMAT OE Usabilit Project
    12/11/01 dls; TB# e11565 Added label for pdcustno
    07/16/02 mwb; TB# e13245 Modified 'ccno' for encryption
    11/21/02 spo; TB# e13755 Fixed 'Freight In' label
    11/14/02 dls; TB# e13523 Additonal Addons.
    02/10/03 spo; TB# e12279 Require valid Shipto
    04/27/04 sbr; TB# e16217 Shipto is required
    05/20/05 rghm;TB# e22547 Correct Addons label
    08/02/05 kjb; TB# e17682 Added new consolidated invoice functionality
    08/25/05 ds;  TB# e23044 Changed label names for consitancy
    08/29/05 kjb; TB# e23031 Modify the behavior of the Last Consolidated
        Invoice Date to better match the design document
*******************************************************************************/

arsc.shipviaty      colon 17
    s-shipviaty        at 24 no-label
    arsc.shipreqfl  colon 64
arsc.shipinstr      colon 17
arsc.shipto
    validate
        (arsc.shipto = ''
          or (can-find(first arss where
                             arss.cono       = g-cono               and
                             arss.custno     = decimal(arsc.custno) and
                             arss.shipto     = arsc.shipto          and
                             arss.statustype = yes ))
          ,"Customer/Ship To Not Set Up in Ship To Setup or is Inactive")
                    colon 64
    arss.notesfl       at 74 no-label
arsc.poreqfl        colon 17 label "Customer PO#"
arsc.custpo            at 23 no-label
    arsc.bofl       colon 64
    arsc.subfl               label "Subs"
arsc.slsrepout      colon 17
    arsc.slsrepin   colon 27
    arsc.tendqtyfl  colon 64
arsc.mediacd        colon 17 label "Paymt and Card#"
    s-ccno                   no-label
    arsc.ccexp               no-label
    arsc.orderdisp  colon 64
skip (1)

/* TB26575 08/26/99 bp ; Add the spcdefaultty */
arsc.spcdefaultty   colon 17 label "OE SPC Default"
    help "Enter (I)CSP-Product, (O)verride-OE, or (N)ot Special"
    arsc.pricetype  colon 32
    s-pricetype        at 39 no-label
    arsc.pricecd    colon 64 label "Level"
    arsc.disccd        at 68 no-label
    arsc.wodisccd      at 70 no-label
arsc.rebatety       colon 17
    s-rebatedesc       at 28 no-label
arsc.pdcustno       colon 64 label "PD Customer#"
    help "Enter Associated Pricing Customer Number if Applicable      [L]"
    validate({v-arsc.i pdcustno},
             "Customer Not Set Up in Customer Setup - ARSC (4303)")
arsc.minord         colon 17 label "Min/Max Order"
arsc.maxord            at 30 no-label
s-title1               at 53 no-label
arsc.ardatcty          at 66 no-label
arsc.noinvcopy      colon 17
    arsc.pickprno   colon 35
arsc.pickprtfl      colon 45 label "Price"
    arsc.salesmgrfl colon 64
arsc.consolinvty    format "X" colon 17 label "Consol - Type"
   validate(if can-do(",c,s,p,o":u,input arsc.consolinvty) then true else false,
             "Must be <Blank>, C, S, P or O (4168)")
             help "<Blank>, (C)ustomer, (S)hipto, Customer (P)O or (O)rder"
arsc.consolformat   format "X"    at 21 label "Fmt"
    validate(if can-do(",p,o":u,input arsc.consolformat) then true else false,
             "Must be P or O (4166)")
             help "(P)roduct Order or (O)rder Order"
arsc.consolterms    format "x(4)" at 29 label "Terms"
arsc.consolinterval format "x(2)" at 41 label "Interval"
    validate(if can-do(",da,su,mo,tu,we,th,fr,sa,sm,mt,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31":u,input arsc.consolinterval)
    then true else false,
             "Must be DA,SU,MO,TU,WE,TH,FR,SA,SM,MT or 1-31 (4167)")
help "(DA)ily,SU,MO,TU,WE,TH,FR,SA,(SM)SemiMonth,(MT)Month,Days 1-31"
s-title2               at 56 no-label
arsc.ardatccost        at 66 no-label
arsc.lastconsoldt   colon 17 label "Last Consol"
    validate(if input arsc.lastconsoldt  = ? or
                input arsc.lastconsoldt <= today then true else false,
             "Date Must Be Less Than Or Equal To Today (2056)")
arsc.nextconsoldt   colon 40 label "Next Consol"
    arsc.salesterr  colon 64
arsc.route          colon 17
    arsc.whse       colon 64
arsc.addonnum[1]    colon 17 label "Addons"
    help "Default Addon 1 - Used For Freight In                       [L]"
    arsc.addonnum[2]          no-label
    help "Default Addon 2 - Used For Freight Out                      [L]"
    arsc.addonnum[3]          no-label
    help "Default Addon 3                                             [L]"
    arsc.addonnum[4]          no-label
    help "Default Addon 4                                             [L]"
    arsc.addonnum[5]          no-label
    help "Default Addon 5                                             [L]"
    arsc.addonnum[6]          no-label
    help "Default Addon 6                                             [L]"
    arsc.addonnum[7]          no-label
    help "Default Addon 7                                             [L]"
    arsc.addonnum[8]          no-label
    help "Default Addon 8                                             [L]"
s-dummy                at 45 no-label
arsc.fpcustno       colon 64
    help "Enter Customer Number for Finance Company if Applicable     [L]"
b-arsc.notesfl         at 78 no-label
arsc.inbndfrtfl     colon 17
    arsc.outbndfrtfl colon 29
    arsc.user1      colon 64 format "x(10)"
    arsc.shiplbl    colon 17 label "IB Ship Label"           /* TB 23693 */
    arsc.user2      colon 64 format "x(10)"

with frame f-arsco side-labels title "Ordering Information"
    row 4 column 1 overlay width 80 no-hide
