/****************************************************************************
*                                                                           *
* f-pdanalysis.i - Sales Analysis Matrix Margin Report Forms                *
*                                                                           *
****************************************************************************/

/* Analysis Form */
form 
"---------------- ACTUAL -----------------"  at  71
"--CURRENT MATRIX INFO--"                    at 113
"----------- FUTURE -----------"             at 137
"Prod"                              at  33
"Product"                           at  63
"Actual"                            at  71
"Actual"                            at  81
"Actual"                            at  91
"Actual"                            at 102
"Future"                            at 137
"Price/"                            at 147
"Future"                            at 158

"Sls"                               at   1
"Customer"                          at  19
"Cust"                              at  28
"Prc"                               at  33
"YTD"                               at  63
"Last Sell"                         at  71
"List"                              at  81
"Discount/"                         at  91
"Invoiced"                          at 102
"Net Sell"                          at 113
"Matrix"                            at 123
"PD"                                at 134
"Net Sell"                          at 137
"Discount/"                         at 147
"Matrix"                            at 158
"PD"                                at 168
"PD"                                at 171

"Rep"                               at   1
"Customer #"                        at   6
"Name"                              at  19
"Type"                              at  28
"Type"                              at  33
"Part Number"                       at  38
"Sales"                             at  63
"Price"                             at  71
"Price"                             at  81
"Multiplier"                        at  91
"Margin %"                          at 102
"Price"                             at 113
"Margin %"                          at 123
"Lv"                                at 134
"Price"                             at 137
"Multiplier"                        at 147
"Margin %"                          at 158
"Lv"                                at 168
"Record#"                           at 171

"----"                              at   1
"------------"                      at   6
"--------"                          at  19
"----"                              at  28
"----"                              at  33
"------------------------"          at  38
"-------"                           at  63
"---------"                         at  71
"---------"                         at  81
"----------"                        at  91
"----------"                        at 102
"---------"                         at 113
"----------"                        at 123
"--"                                at 134
"---------"                         at 137
"----------"                        at 147
"---------"                         at 158
"--"                                at 168
"-------"                           at 171
with frame f-header no-underline no-box no-labels page-top down width 178.


form
matrix.salesrep               at   1
matrix.customer               at   6
matrix.name                   at  19
matrix.cprice-type            at  28
matrix.pprice-type            at  33
matrix.product                at  38
matrix.i-ytd-amt              at  63
matrix.i-netprc               at  71
matrix.i-listprc              at  81
matrix.i-discmult             at  91
matrix.i-pdtype               at 101 
s-imarg                       at 102
matrix.c-netprc               at 113
s-cmarg                       at 123
matrix.pd-level               at 135
matrix.f-netprc               at 137
matrix.f-discmult             at 147
matrix.f-pdtype               at 157
s-fmarg                       at 158
matrix.f-level                at 169
matrix.pd-recno               at 171
with frame f-detail no-underline no-box no-labels down width 178.


form 
"---------------- ACTUAL -----------------"  at  58
"--CURRENT MATRIX INFO--"                    at 100
"----------- FUTURE -----------"             at 124
"Prod"                              at  28
"Actual"                            at  58
"Actual"                            at  68
"Actual"                            at  78
"Actual"                            at  89
"Future"                            at 124
"Price/"                            at 134
"Future"                            at 145

"Sls"                               at   1
"Customer"                          at  19
"Prc"                               at  28
"Last Sell"                         at  58
"List"                              at  68
"Discount/"                         at  78
"Invoiced"                          at  89
"Net Sell"                          at 100
"Matrix"                            at 110
"PD"                                at 121
"Net Sell"                          at 124
"Discount/"                         at 134
"Matrix"                            at 145
"PD"                                at 155
"PD"                                at 158
"Change"                            at 168

"Rep"                               at   1
"Customer #"                        at   6
"Name"                              at  19
"Type"                              at  28
"Part Number"                       at  33
"Price"                             at  58
"Price"                             at  68
"Multiplier"                        at  78
"Margin %"                          at  89
"Price"                             at 100
"Margin %"                          at 110
"Lv"                                at 121
"Price"                             at 124
"Multiplier"                        at 134
"Margin %"                          at 145
"Lv"                                at 155
"Record#"                           at 158
"Amount"                            at 168

"----"                              at   1
"------------"                      at   6
"--------"                          at  19
"----"                              at  28
"------------------------"          at  33
"---------"                         at  58
"---------"                         at  68
"----------"                        at  78
"----------"                        at  89
"---------"                         at 100
"----------"                        at 110
"--"                                at 121
"---------"                         at 124
"----------"                        at 134
"---------"                         at 145
"--"                                at 155
"-------"                           at 158
"-"                                 at 166
"--------"                          at 168
with frame f-header-a no-underline no-box no-labels page-top down width 178.


form
matrix.salesrep               at   1
matrix.customer               at   6
matrix.name                   at  19
matrix.pprice-type            at  28
matrix.product                at  33
matrix.i-netprc               at  58
matrix.i-listprc              at  68
matrix.i-discmult             at  78
matrix.i-pdtype               at  88 
s-imarg                       at  89
matrix.c-netprc               at 100
s-cmarg                       at 110
matrix.pd-level               at 121
matrix.f-netprc               at 124
matrix.f-discmult             at 134
matrix.f-pdtype               at 144
s-fmarg                       at 145
matrix.f-level                at 155
matrix.pd-recno               at 158
matrix.change                 at 166
matrix.change-amt             at 168
with frame f-detail-a no-underline no-box no-labels down width 178.

form
skip(1)
matrix.salesrep               at   1
"Before/After Margins:"       at  58
s-bmarg                       at  89
s-amarg                       at 145
with frame f-margtot no-underline no-box no-labels down width 178.
/*
form                                                           
skip(1)                                                        
"** Actual, Current and Future Margins include a 3% overcost **"      at 5
with frame f-trailer no-box no-labels page-bottom down width 178.
*/     
     
