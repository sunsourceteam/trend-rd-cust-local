
assign  oeehb.batchnm       = string(w-batchnm,">>>>>>>9")
        oeehb.seqno         = 1
        oeehb.cono          = g-cono
        oeehb.transtype     = "QU"
        oeehb.stagecd       = 0
        oeehb.inprocessfl   = yes 
        oeehb.user2         = g-operinit
        oeehb.custno        = arsc.custno
        oeehb.divno         = arsc.divno
        oeehb.shipto        = v-shipto
        oeehb.takenby       = v-takenby
        oeehb.statecd       = if avail arss then arss.statecd else arsc.statecd
        oeehb.countrycd     = if oeehb.countrycd <> "" then oeehb.countrycd
                                  else 
                                  if arsc.divno = 40 then "CN"
                                    else arsc.currencyty
        oeehb.pricecd       = if avail arss and v-shipto <> " "
                                then arss.pricecd
                              else arsc.pricecd
        oeehb.whse          = if avail arss and v-shipto <> " " 
                                then arss.whse
                              else arsc.whse
        oeehb.shipviaty     = if avail arss and v-shipto <> " "
                                then arss.shipviaty
                              else arsc.shipviaty
        oeehb.outbndfrtfl   = if avail arss and v-shipto <> " "
                                then arss.outbndfrtfl
                              else
                                arsc.outbndfrtfl
        oeehb.inbndfrtfl    = if avail arss and v-shipto <> " "
                                then arss.inbndfrtfl
                              else
                                arsc.inbndfrtfl
        oeehb.addonno[1]    = if avail arss and v-shipto <> " "
                                then arss.addonno[1]
                              else
                                arsc.addonno[1]
        oeehb.addonno[2]    = if avail arss and v-shipto <> " "
                                then arss.addonno[2]
                              else
                                arsc.addonno[2]
        oeehb.addonno[3]    = if avail arss and v-shipto <> " "
                                then arss.addonno[3]
                              else
                                arsc.addonno[3]
        oeehb.addonno[4]    = if avail arss and v-shipto <> " "
                                then arss.addonno[4]
                              else
                                arsc.addonno[4]
        oeehb.orderdisp     = if avail arss and v-shipto <> " "
                                then arss.orderdisp
                              else
                                arsc.orderdisp
        oeehb.bofl          = if avail arss and v-shipto <> " "
                                then arss.bofl
                              else
                                arsc.bofl
        oeehb.subfl         = if avail arss and v-shipto <> " "
                                then arss.subfl
                              else
                                arsc.subfl
        oeehb.slsrepout     = if avail arss and v-shipto <> " "
                                then arss.slsrepout
                              else arsc.slsrepout
        oeehb.slsrepin      = if avail arss and v-shipto <> " "
                                then arss.slsrepin
                              else arsc.slsrepin
        oeehb.custpo        = v-custpo
        oeehb.orderdisp     = if avail arss and v-shipto <> " " 
                                then arss.orderdisp
                              else arsc.orderdisp
        oeehb.termstype     = if avail arss and v-shipto <> " "
                                then arss.termstype
                              else arsc.termstype
        oeehb.approvty      = /*sasc.oeapprty*/ "y"
        oeehb.shipinstr     = if avail arss and v-shipto <> " "
                                then arss.shipinstr
                              else arsc.shipinstr
        oeehb.codfl         = if avail arss and arss.termstype = "COD" 
                                then yes     
                              else
                                if avail arsc and arsc.termstype = "COD" 
                                  then yes     
                                else no                              
        oeehb.placedby      = v-contact
        oeehb.canceldt      = v-canceldt
        oeehb.reqshipdt     = TODAY
        oeehb.poissdt       = ?
        oeehb.enterdt       = g-today
        oeehb.sourcepros    = "Quote"
        substr(oeehb.user3,1,20) = string(v-contact,"x(20)")
        substr(oeehb.user3,21,12) = string(v-phone,"x(12)")
        substr(oeehb.user3,33,20) = string(v-email,"x(20)")
        substr(oeehb.user5,1,1)   = v-currencyty
        hdr-whse            = oeehb.whse
        oeehb.nontaxtype    = if avail arss then arss.nontaxtype
                              else arsc.nontaxtype
        oeehb.taxablefl     = if avail arss and arss.taxablety = "n" then
                                no
                              else 
                                if avail arss and arss.taxablety = "y" then
                                  yes
                                else
                                  if avail arsc and arsc.taxablety = "n" then
                                    no
                                  else
                                    if avail arss and arsc.taxablety = "y" then
                                      yes
                                    else
                                      yes.
        find sastc where sastc.cono = g-cono and
                         sastc.currencyty = arsc.currencyty
                         no-lock no-error.
        if avail sastc then
          assign oeehb.user6 = if sastc.purchexrate > 0 then
                                 1 / sastc.purchexrate
                               else
                                 0.
        else
          assign oeehb.user6 = 0.
        assign
          oeehb.shiptonm      = if v-shiptonm <> " " then v-shiptonm
                                else oeehb.shiptonm
          oeehb.shiptoaddr[1] = if v-shiptoaddr1 <> " " then v-shiptoaddr1
                                else oeehb.shiptoaddr[1]
          oeehb.shiptoaddr[2] = if v-shiptoaddr2 <> " " then v-shiptoaddr2
                                else oeehb.shiptoaddr[2]
          oeehb.shiptocity    = if v-shiptocity <> " " then CAPS(v-shiptocity)
                                else oeehb.shiptocity
          oeehb.shiptost      = if v-shiptost <> " " then CAPS(v-shiptost)
                                else oeehb.shiptost
          oeehb.shiptozip     = if v-shiptozip <> " " then CAPS(v-shiptozip)
                                else oeehb.shiptozip
          oeehb.geocd         = if v-shiptogeocd <> 0 then v-shiptogeocd
                                else oeehb.geocd
          oeehb.xxl12         = w-shipchgfl.
{t-all.i "oeehb"}
                           

