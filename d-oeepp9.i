/*h*****************************************************************************
  PROCEDURE     : d-oeepp9.i based on 1
  DESCRIPTION   : Used in oeepp1wm.p, oeepp1.p, oeepp1ws.p, vaepp1.p,
                    vaepp1wm.p and vaepp1ws.p`
  AUTHOR        : R&D
  DATE WRITTEN  :
  CHANGES MADE  :
    05/28/91 mwb; TB# 3228  MSDS info not on reprint
    09/11/91 kmw; TB# 2277  Kit enhancements
    10/30/91 mwb; TB# 4616  Qtyord as neg if return
    07/07/92 mms; TB# 7176  Honor linefl on list
    07/29/92 mkb; TB# 7191  Alpha custno changes
    09/09/92 mms; TB# 5113  Don't assn wm on pickup on way
    04/19/93 jlc; TB# 10995 Display customer product info
    06/07/93 kmw; TB# 11519 Qty ship & qty bo is not right
    06/08/93 kmw; TB# 11690 MSDS message appears twice
    07/06/93 kmw; TB# 11717 Print Pick Ticket flag
    07/06/93 mwb; TB# 11604 Component customer product.
    10/10/93 jlc; tb# 13103 EDI customer product wrong.
    10/29/93 kmw; TB# 11758 Set unit conv to 10 dec
    11/08/93 mwb; TB# 13520 Carry unitconv to 10 dec.
    11/16/93 mwb; TB# 13627 Change icsw exclusive to no-lock
    12/01/93 mwb; TB# 13754 oeepp1 oversize.
    02/21/94 dww; TB# 13890 File name not specified causing error
    03/14/94 dww; TB# 15141 Allow decimals in Conversion factor
    11/20/94 mms; TB# 14261 Added Ship Complete packing slip logic
    04/27/95 mtt; TB# 13356 Serial/Lot # doesn't print on PO/RM
    06/22/95 ajw; TB# 18575 Nonstock/Special Kit Component Project (G1)
    11/01/95 gp;  TB# 18091 Expand UPC Number (T20)
    11/06/95 gp;  TB# 18091 Expand UPC Number (T20)
    06/26/96 jms; TB# 21254 (10A) Develop Value Add Module
        Added parameter &lineno to use a numeric variable
    08/20/96 gp;  TB# 19484 (B2a) Warehouse Logistics.  Add creation
        of WLEL records for lines and WLELK records for BOD kit components.
    09/09/96 gp;  TB# 19484 (B2a) Warehouse Logistics.  Add MSDS logic.
    09/16/96 mwb; TB# 19484 (B7) Whse Logistics. Added the CS edit per line.
        Counter Sale orders are excluded from oeepp, moved to oeepi.
    10/03/96 des; TB# 19484 (B1a) Warehouse Logistics Do not send type "r"
        components.  Only send type "c".
    10/30/96 ajw; TB# 19484 (B13) Warehouse Logistics - Added ShipmentID
    12/26/96 mwb; TB# 22380 Whse Logistics.  If the Primary Counter bin
        location is blank in ICSW and the order is a Counter Sale, then
        display a blank 'Bin Loc:' field so the operator has room to write
        in the location to pick from the Location inquiry in TWL.
    12/29/96 jms; TB# 21254 (10A) Develop Value Add Module - changed
        wmet.ordertype from "o" to {&ordertype} and
        wmet.seqno     from 0   to {&seqno}.
    01/31/97 jl;  TB# 22491 Whse Logistic. If line is tied to AC/PO don't send
    02/10/97 mwb; TB# 22557 Whse Logistics. No longer sending the Negative
        Components as Receipt records - passed as PCK records. Sending all
        the components with the kit to avoid double receipts.
    07/17/97 mwb; TB# 23455 Add edit against Order Stage to block the creation
        of a download to TWL if the order is in stage 3 or greater.  Customers
        are using PMEP F10-Print once the order is shipped as a packing slip
        to place in the last box.  The downloads were erroring out (unable to
        update TWL order - not open).
    12/01/97 mwb; TB# 24253 Added edit to block tag and hold orders.  Need to
        change the order disposition to get the order downloaded.
    09/10/98 gkd; TB# 25120 Removed tag and hold order block because order
        disposition changed now.
    10/21/98 jgc; TB# 9731 Expand non-stock description to 2 lines.
    10/12/99 ama; TB# e2761 Tally Project - Add printing of oeelm records for
        Mix-Tally Kits
    10/21/99 ama; TB# e2761 Tally Project - Comment out the oeel.tallyfl check
        so vaepp1.p will compile
    10/26/99 ama; TB# e2761 Tally Project - Add memomixfl check for memo disp
        will be different than other mix kits display
    11/01/99 ama; TB# e2761 Tally Project - Changing "Mix" to "Tly"
    11/09/99 ama; TB# e2761 Tally Project - changes in memo tally print
    03/07/00 cm;  TB# e4369 Memo tally components display with "** Invalid **"
    03/29/00 rgm; TB# 3-2  Add RxServer Interface to Standard
    06/07/00 rcl; TB#e5244 Add Bin Location to nonstocks
    06/22/00 hpl; TB#e5061 Add new Quantity from receivers for products and
        build on demand kits and provide logic to be commented out by value add
        module.
    07/11/00 cm;  TB# e5061 CR changes for hpl
    07/17/00 smi; TB# e5537 WT BOD Project - Added WO component print
        variables
    07/25/00 des; TB# e5061 Inventory Allocation - Adjust qtyfmrcvs display on
        a reprint
    09/20/00 mms; TB# e6508 WT BOD Project - Print components on BOD kit
        fabricated in same whse.
    09/21/00 mms; TB# e6508 WT BOD Project - Add logic to support VA tie type.
        Added new &orderalttype param.
    11/29/00 lbr; TB# e6764 Added Indication of alternate drop ship source
    01/22/01 mms; TB# e7644 WT BOD Project - Pass all required params to
        kitcompprt1.i and replace calls to wocompprt.p with kitcompprt1.i.
    11/05/01 ajw; TB# e10078 OE Usability - Print OEELC Comments.
    11/19/01 sbr; TB# e11166 "?" appears in Unit Price
    11/21/01 ajw; TB# e11305 Print External Comments on Pick Ticket too.
    01/16/02 ajw; TB# e11791 Print Multiple Bin Locations (ICSWB data).
    01/15/02 lbr; TB# e3207 Added param to oeepp1n
    02/08/02 dls; TB# e11976 OE Usability - New subtotal logic from oeelc
    02/27/02 kjb; TB# e11381 Do not create WL records if the line on the OE
        order is a DO line and the new option to print DO lines is set to yes
    03/15/02 sbr; TB# e7966 Customer part # xref not printing
    10/10/02 jbt; TB# e11863 Call generic notes printing program.
    11/12/02 kjb; TB# e15321 Modify the Tally print logic - Replace the
        existing tally print logic with new logic found in tallyprt.ldi
    11/22/02 gkd; TB# L963 Send all lines to TWL when printing changes only
    02/27/03 jkp; TB# e16544 Print assigned serial numbers for bod kit
        components.
    04/22/03 blw; TB# L1211 - Added si mod wl009 & wl010 to standard
    05/15/03 blw; TB# L1211 - Display s-qtybo when printing.
    11/12/03 blw; TB# L759  - Integrate VA with TWL; Create WLIT records.
    11/14/03 blw; TB# L759  - Integrate VA with TWL; Print Carton Info.
    03/31/04 bm;  TB# e13915 Picktickets don't print foreign language
    04/23/04 bpa; TB# e17505 Add ICSEC.ADDPRTINFO to Customer Prod line
    05/21/04 jhl; TB L1379 Enhanced Counter Sales Processing
    07/08/04 blw; TB# L1525  Add UOM conversion and unit to TWL carton print.
    08/12/04 ns;  TB# e20511 Add display of scrap return message
    01/18/05 rgm; TB# e21353 Subtotal lines not displaying on PickTicket
    03/07/05 blw; TB# L1692 - Add sequence for printing cartons on kits.
    03/18/05 blw; TB# L1678 - Add truck pallet printing support.
    03/22/05 blw; TB# L1692 - VA external lines should have a sequence of zero.
    09/06/05 rgm; TB# e23059 Don't force blank line for Scrap Return Message
    10/10/05 ds;  TB# e23301 Bin locations will no print in proper format
    01/06/06 bp ; TB# e23820 Add user hooks.
    02/17/06 jab; TB# e23526 Add Internal Comment option
    02/23/06 bpa; TB# e22953 ADDPRTINFO is being pulled from ship prod. Should
        be pulled from requested prod
    02/12/07 bp ; TB# e25927 Add user hooks.
    11/14/08 dkt change the oeepp1.z99 to oeepp9.z99
                 fix internal comments to pick ticket only
    12/15/08 dkt display s-binloc2                 
    01/27/09 dt01 sortoeelk
    01/28/09 dkt reset s-binloc2 in loop to null
    01/29/09 dt01 changed "o" (&line}.shipprod to oeelk.shipprod
    02/02/09 dt01 change find wmsb/wmsbp for binloc priority findfirstbin
    02/10/09 dt01 no bins for labor items
    03/17/09 tah  fix kit component print
*******************************************************************************/
/*e     Parameters:
           &returnfl     = Return flag
           &head         = Header information file
           &section      = Header file for "OE" or Section file for "VA"
           &line         = Line item file
           &cubes        = The number of cube units for the entire line item
           &weight       = Weight of the entire line item
           &ourproc      = The process that calls this program
           &specnstype   = Special or non-stock type
           &altwhse      = Alternate warehouse
           &vendno       = Vendor number
           &kitfl        = Kit flag
           &botype       = Back order type
           &prevqtyship  = Previous quantity shipped
           &netamt       = Net amount - the fully extended price of the line
           &prtpricefl   = Print price flag
           &bono         = Back order number
           &orderalttype = Order tie type specifying what will fill this line
           &orderno      = Order number
           &ordersuf     = Order suffix
           &custno       = Customer number
           &comxref      = Do not use for "oe", comment out for "va" - no xref
           &corechgty    = Core charge type
           &combo        = Do not use for "oe", comment out for "va" - no BO's
           &qtybo        = Quantity back ordered
           &orderdisp    = Order disposition
           &ordertype    = "o" for order entry, "f" for Value Add - specifies
                           whether OE or VA is running this code
           &prefix       = "order" for order entry, "va" for value add
           &seqno        = "0" zero for order entry, "vaesl.seqno" for "va"
           &stkqtyshp    = Stock quantity shipped
           &nosnlots     = Number of serial or lots
           &comtype      = Comment type "oe" for "oe", v-comtype for "va"
           &comlineno    = Comment line number
           &shipto       = Ship to
*******************************************************************************/

/*o Accumulate the totals for the bottom of the pick ticket */
/*tb# e6764 Don't include do's from alternate sources in totals */
if {&botype} <> "d" then
assign
    v-linecnt    = v-linecnt + 1
    a-totcubes   = a-totcubes + if {&returnfl} then 0 else {&cubes}
    a-totweight  = a-totweight + if {&returnfl} then 0 else {&weight}
    a-totlineamt = a-totlineamt + ({&netamt} * if {&returnfl} then -1 else 1).

/*d 1. Skip the print of this line if 0 qty ship lines are suppressed.
    2. For Ship Complete, Tag and Hold orders, only print new/changed items if
       the order is not complete.
    3. If a pick ticket reprint is requested and this line has not been changed
       since the last pick, skip it */

/* Create a WLEL record for each line */
/* All lines need to go to TWL - even if printing changes only */
if v-wlwhsefl = yes and
        ((v-oelinefl = no and {&line}.stkqtyship = 0) or
        ({&line}.printpckfl = no and p-reprintfl))
then do:
        v-wlupdtype = "WL-Change":u.
        v-noprint = v-noprint + 1.
end.
else do:
        v-wlupdtype = "".
end.

if  v-wlupdtype <> "WL-change":u
        AND
        (

          ((v-oelinefl = no and {&line}.stkqtyship = 0) or
           ({&line}.printpckfl = no and p-reprintfl))

          or

          (can-do("s,t",{&orderdisp}) and
           {&section}.pickcnt > 1             and
           v-completefl = no            and
           p-reprintfl = yes            and
           ({&line}.printpckfl = no or
            {&line}.stkqtyship = 0)        and
           g-ourproc = "{&ourproc}")

          or

          (v-reprintfl     = yes and
           p-reprintfl     = yes and
           {&line}.printpckfl = no)

        )
then do:
        v-noprint = v-noprint + 1.
        next.
end.

/** del dkt 12/16/2008
/*o Load line variables */
if {&specnstype} ne "n" then do:
    {w-icsw.i {&line}.shipprod
        "if {&altwhse} ne '' then {&altwhse} else {&line}.whse" no-lock}
    {w-icsp.i {&line}.shipprod no-lock}
end.
else
end of del **/
    /* tb e3207 added read of catalog so we can print notes */
    {w-icsc.i {&line}.shipprod no-lock}.

/*tb 18091 11/01/95 gp;  Find product section of UPC number */
if {&line}.arpvendno <> 0 then
    {icsv.gfi "'u'" {&line}.shipprod {&line}.arpvendno no}
else
    {icsv.gfi "'u'" {&line}.shipprod {&vendno} no}

/*tb 18091 11/01/95 gp;  Assign product section of UPC Number */
/*tb e6764 12/18/00 lbr; Changed a-totqtyshp to exclude botype = d from
    totals */


/* dkt 11/19/2008 */
/** if {&specnstype} = "n" then do:              del 2008 **/


  /* dt01 02/02/09 
  at this point has last wmsbp, go figure, must refind */
  
  wmsbpid = ?.
  wmsbid  = ?.
  
  run findfirstbin.p(input g-cono, input {&whse}, input {&line}.shipprod,
                     output wmsbid, output wmsbpid).

  if wmsbid ne ? and wmsbpid ne ? then do:
    find wmsbp where recid(wmsbp) = wmsbpid no-lock no-error.
    find wmsb  where recid(wmsb)  = wmsbid  no-lock no-error.
  end.
  /* dt01 02/02/09 */

  if not avail (wmsb) or not avail (wmsbp) then do:
    release wmsb.
    release wmsbp.
  end.

  {w-icsw.i {&line}.shipprod                                         
     "if {&altwhse} ne '' then {&altwhse} else {&line}.whse" no-lock}
  {w-icsp.i {&line}.shipprod no-lock}       
              
/** del 2008
 end.                                                                 
**/

assign
    v-wmfl      = if {&specnstype} ne "n" and avail icsw and not {&kitfl}
                      and {&botype} <> "p"

                      {&coms}
                      and w-binorder.w-wmid <> 0
                      /{&coms}* */

                      then icsw.wmfl
                  else no

    {&comt}
    a-totqtyshp = if {&botype} <> "d" then
                    (a-totqtyshp + if ({&returnfl}
                                       or v-wlupdtype = "WL-Change":u)
                    then 0 else {&line}.qtyship)
                    else a-totqtyshp
    /{&comt}* */

    v-conv      = {unitconv.gas &decconv = {&line}.unitconv}
    s-qtybo     = {&qtybo}
    s-qtyship   = {&line}.qtyship -
                      ((if p-reprintfl = yes then {&prevqtyship} else 0)
                        / v-conv)
    s-qtyship   = if {&returnfl} = yes then s-qtyship * -1
                  else if s-qtyship < 0 then 0
                  else s-qtyship
    v-qtyship   = s-qtyship
    s-qtyord    = if {&returnfl} = yes then {&line}.qtyord * -1
                  else {&line}.qtyord
    s-lineno    = string({&line}.lineno,"zz9")
    s-aster     = if v-completefl = yes    and {&line}.printpckfl = yes and
                      can-do("s,t",{&orderdisp}) then "*"
                  else ""
    s-netamt    = if {&returnfl} = yes then {&netamt} * -1
                  else {&netamt}

    /* dkt 11/19/2008 */
                     /* dt01 02/10/09 */
    s-binloc    = if (avail (icsp) and icsp.statustype = "L") then ""
                  else if {&botype} = "p" then "Pick Up"
                  else if {&botype} = "y" and avail wmsbp then
                    string(wmsbp.binloc,"xx/xx/xxx/xxx") /* dkt 12/16/2008 */
                  else if {&altwhse} ne "" then "Whse: " + {&altwhse}
                  else if avail icsw then string(icsw.binloc1,"xx/xx/xxx/xxx")
                  else if avail wmsb then string(wmsb.binloc,"xx/xx/xxx/xxx")
                  else if {&kitfl} = yes then "" 
                  else if {&specnstype} = "n" then "Non Stock"
                  else ""                                                          /* dkt 01/28/2009 */
    s-binloc2   = ""

                   
    s-upcpno    = if avail icsv then
                      {upcno.gas &section  = "4"
                                 &sasc     = "v-"
                                 &icsv     = "icsv."
                                 &comdelim = "/*"}
                  else "00000"

    &if "{&line}" = "oeel":u &then
    s-scrapmsg  = if {&line}.returnty = "c":u then "SCRAP RETURN/CREDIT"
                  else "".
    &else
    s-scrapmsg  = "".
    &endif

    &if "{&comoeout}" <> "out":u &then
    v-subnetamt = v-subnetamt + s-netamt.
    &endif /* {&comoeout} */

    /*tb 3-2 03/29/00 rgm; Rxserver interface extra variable code */
    /*tb e11166 11/19/01 sbr; Modified the assign statement for s-rxprice to
        include a check for s-qtyship equal to zero. */
    &IF DEFINED(rxserver) <> 0 &THEN
        s-rxline1 = "".
        if {&prtpricefl} = yes then
        assign
            s-rxprice  = if s-qtyship <> 0 then
                             s-netamt / s-qtyship
                         else 0
            s-rxline1  = &if "{&head}" = "oeeh" &then
                             if oeel.corechgty = "r"
                             then string(oeel.corecharge,"zzzzzz9.99-")
                             else
                         &endif
                         if s-rxprice * 100 = integer(s-rxprice * 100)
                              then string(s-rxprice,"zzzzzz9.99-")
                              else string(s-rxprice,"zzzzzz9.99999-").
    &ENDIF


/*d Back ordered qty not used in Value Add */
{&combo}
/*d Set the back ordered qty */
if {&bono} > 0 then do:

    find b-{&line} use-index k-{&line} where
        b-{&line}.cono        = g-cono         and
        b-{&orderno}          = {&orderno}     and
        b-{&ordersuf}         = {&bono}        and
        b-{&line}.lineno      = {&line}.lineno and
        b-{&line}.statustype ne "c" no-lock no-error.

    if avail b-{&line} then s-qtybo = b-{&line}.qtyord.

end. /* set s-qtybo */
/{&combo}* */

/* Display changed lines only on report when printing changes only
   even if printing all lines to TWL */
if not v-wlupdtype = "WL-Change":u then do:

  /*tb 3-2 03/29/00 rgm; Rxserver interface hook for extra variables */
  {oeepp9.z99 &before_linedisplay = "*"
            &comr        = "{&comr}"
            &coms        = "{&coms}"
            &returnfl    = "{&returnfl}"
            &cubes       = "{&cubes}"
            &weight      = "{&weight}"
            &head        = "{&head}"
            &line        = "{&line}"
            &section     = "{&section}"
            &ourproc     = "{&ourproc}"
            &specnstype  = "{&specnstype}"
            &altwhse     = "{&altwhse}"
            &vendno      = "{&vendno}"
            &kitfl       = "{&kitfl}"
            &botype      = "{&botype}"
            &orderdisp   = "{&orderdisp}"
            &prevqtyship = "{&prevqtyship}"
            &netamt      = "{&netamt}"
            &prtpricefl  = "{&prtpricefl}"
            &bono        = "{&bono}"
            &orderno     = "{&orderno}"
            &ordersuf    = "{&ordersuf}"
            &custno      = "{&custno}"
            &corechgty   = "{&corechgty}"
            &qtybo       = "{&qtybo}"
            &ordertype   = "{&ordertype}"
            &prefix      = "{&prefix}"
            &seqno       = "{&seqno}"
            &stkqtyshp   = "{&stkqtyshp}"
            &nosnlots    = "{&nosnlots}"
            &comtype     = "{&comtype}"
            &comlineno   = "{&comlineno}"
            &shipto      = "{&shipto}"
            &twlstgedit  = "{&twlstgedit}" }

  /*d Display the line if WM has not been purchased or is not used */
  /*tb 18091 11/01/95 gp;  Change UPC number display */
  /*tb 3-2 03/29/00 rgm; Rxserver interface added s-rxline vars for rxserver */
  if not v-wmfl or not v-modwmfl then do:

    /* Comment out the OE specific logic when coming from areas such  as
       VA that might cause compile issues or where the fields are module
       specific. */
    &if "{&comoeout}" <> "out":u &then
    if {&LINE}.qtyfmrcvs <> 0 then
        s-qtyfmrcvs = "  Qty From Receivers:     " +
                        (if {&LINE}.qtyfmrcvs > s-qtyship then
                            string(s-qtyship,">>>>>>9.99")
                         else
                            string({&LINE}.qtyfmrcvs,">>>>>>9.99")).
    else
        s-qtyfmrcvs = "".
    &endif /* {&comoeout} */

    /* dkt 11/19/2008 */
    if icsdAutoPck = "y" and       
       icsdAutoXdock = "Y" then do:
       
      find first zsdipckdtl use-index k-ordix where 
               zsdipckdtl.cono = g-cono              and
               zsdipckdtl.ordertype = "{&ordertype}" and
               zsdipckdtl.whse = {&whse}             and 
               zsdipckdtl.orderno = {&orderno}       and
               zsdipckdtl.ordersuf = {&ordersuf}     and
               zsdipckdtl.lineno = {&line}.lineno    and
               zsdipckdtl.seqno  = integer({&seqno})          and
               zsdipckdtl.binloc1 = "RECEIVING" no-lock no-error.
      if not avail zsdipckdtl then
        find first zsdipckdtl use-index k-ordix where 
               zsdipckdtl.cono = g-cono              and
               zsdipckdtl.ordertype = "{&ordertype}" and
               zsdipckdtl.whse = {&whse}             and 
               zsdipckdtl.orderno = {&orderno}       and
               zsdipckdtl.ordersuf = {&ordersuf}     and
               zsdipckdtl.lineno = {&line}.lineno    and
               zsdipckdtl.seqno  = integer({&seqno})        
               no-lock no-error.
       
      if avail zsdipckdtl then do:
        s-binloc2 = zsdipckdtl.binloc2.
        if zsdipckdtl.binloc1 = "RECEIVING" then
          s-binloc = zsdipckdtl.binloc1.
        else
          s-binloc = string(zsdipckdtl.binloc1,"xx/xx/xxx/xxx").
      end.
    
      if {&kitfl} = yes {&kitparent} then  /* tah 92208 */
        s-binloc = "".  
    
    end. /* if autoFlags */

    if v-loopcnt = 3 and x-receipt then
      display
        s-lineno
        s-aster
        {&line}.shipprod
        {&line}.unit
        s-qtyord
        s-qtybo
        s-qtyship
        s-upcpno
        s-binloc
        s-binloc2 when s-binloc2 ne s-binloc /* dkt */
        s-netamt when {&prtpricefl} = yes
        string(s-qtyship,">>>>>9.99") @ s-underline
        &IF DEFINED(rxserver) <> 0 &THEN
            s-rxline1
        &ENDIF
        &if "{&comoeout}" <> "out":u &then
        s-qtyfmrcvs @ s-descrip2
        &endif
      with frame f-oeel.
    else
    display
        s-lineno
        s-aster
        {&line}.shipprod
        {&line}.unit
        s-qtyord
        s-qtybo
        s-qtyship
        s-upcpno
        s-binloc
        s-binloc2 when s-binloc2 ne s-binloc /* dkt */
        s-netamt when {&prtpricefl} = yes
        "____________ _____" @ s-underline
        &IF DEFINED(rxserver) <> 0 &THEN
            s-rxline1
        &ENDIF
        &if "{&comoeout}" <> "out":u &then
        s-qtyfmrcvs @ s-descrip2
        &endif
    with frame f-oeel.


    /*tb 9731 10/21/98 jgc; Expand non-stock description.  Added second
        display line for proddesc2. */
    if {&specnstype} = "n" then do:

        if {&line}.proddesc ne "" then display {&line}.proddesc @ s-descrip
            with frame f-oeel.
        if {&line}.proddesc2 ne "" then do:
            display {&line}.proddesc2 @ s-descrip3 with frame f-oeel2.
            down with frame f-oeel2.
        end.
    end.
    else do:
        if avail icsw and {&altwhse} ne "" then
            display icsw.binloc1 @ s-binloc2 with frame f-oeel.

        else if avail icsw and icsw.binloc2 ne "" then
            display icsw.binloc2 @ s-binloc2 with frame f-oeel.

        if not avail icsp or not avail icsw then
            display v-invalid @ s-descrip with frame f-oeel.
        else do:

            {&comva}
            {w-sals.i oeeh.langcd "P" {&line}.shipprod no-lock}
            /{&comva}* */
            if avail sals then
                display sals.descrip[1] @ s-descrip with frame f-oeel.
            else
                display icsp.descrip[1] @ s-descrip with frame f-oeel.
        end.
    end.

    {oeepp9.z99 &b4_down_f-oeel = "*"}

    down with frame f-oeel.

    if avail icsp and icsp.descrip[2] ne "" and {&specnstype} ne "n" then do:

        if avail sals and sals.descrip[2] ne "" then
            display sals.descrip[2] @ s-descrip3 with frame f-oeel2.
        else
            display icsp.descrip[2] @ s-descrip3 with frame f-oeel2.

        if avail icsw and {&altwhse} ne "" then
            display icsw.binloc2 @ s-binloc3 with frame f-oeel2.

        /*If we don't have any additional Bin Locations, make sure to force
          a line-feed. If we do have additional bin location(s), then print
          the first one with the Desc #2*/
        if not can-find (first icswb where
            icswb.cono = g-cono and
            icswb.prod = {&line}.shipprod and
            icswb.whse = {&line}.whse no-lock) then

            /*tb 19484 09/16/96 mwb; added the down with frame line */
              down with frame f-oeel2.
    end.

    /*Print any additional Bin Locations*/
    if {&specnstype} ne "n":u then do:
        for each icswb where
            icswb.cono  = g-cono and
            icswb.prod  = {&line}.shipprod and
            icswb.whse  = {&line}.whse no-lock:

            display icswb.binloc @ s-binloc3 with frame f-oeel2.
            down with frame f-oeel2.
        end.
    end.

    /*tb 19484 09/16/96 mwb; added displaying of the counter bin location
        for WL.  Can setup a product with a primary counter bin or show room
        location.  Needs to display with the other bins for Primary Picking */
    if v-wlwhsefl = yes and {&head}.transtype = "cs"
    then do for wlicsw:

        {wlicsw.gfi &prod = "{&line}.shipprod"
                    &whse = "{&line}.whse"
                    &lock = "no"}

        /*d Counter Bin Location */
        if avail wlicsw and wlicsw.bincntr ne "" then
            display "            Counter Bin:" @ s-descrip3
                    wlicsw.bincntr @ s-binloc3 with frame f-oeel2.


        /*tb 22380 12/26/96 mwb; added blank location display */
        else
            display "            Counter Bin:" @ s-descrip3
                    "_____________" @ s-binloc3 with frame f-oeel2.

        down with frame f-oeel2.

    end.
    /*tb 19484 09/16/96 mwb; end of Counter Bin display */
    s-lastfl = yes.
  end.    /* not WM */

  /*d Process for a WM product */
  else do:
    {&coms}
    assign
        s-netfl   = w-binorder.w-netfl
        s-lastfl  = w-binorder.w-lastfl
        s-qtyship = if avail wmet then wmet.qtyactual / v-conv else 0
        s-qtyship = if {&returnfl} = yes then s-qtyship * -1
                    else if s-qtyship < 0 then 0
                    else s-qtyship
        v-qtyship = if avail wmet then wmet.qtyactual / v-conv else 0
        s-binloc  = if avail wmet then string(wmet.binloc,"xx/xx/xxx/xxx")
                    else "".

    /{&coms}* */

    {&comt}
    assign
        s-netfl   = yes
        s-lastfl  = yes
        s-qtyship = 0
        v-frameln = 1
        s-binloc  = "Whse Mgr".
    /{&comt}* */

    /*o Display the line item */
    /*tb 18091 11/01/95 gp;  Change UPC number assignment */
    if v-loopcnt = 3 and x-receipt then
      display
        s-lineno
        s-aster
        {&line}.shipprod
        s-upcpno
        s-binloc
        s-qtyord
        s-qtybo
        {&line}.unit
        string(s-qtyship,">>>>>9.99") @ s-underline
        &IF DEFINED(rxserver) <> 0 &THEN
            s-rxline1
        &ENDIF

        {&comb}
        s-qtyship
        "Whse Mgr" @ s-binloc2
        /{&comb}* */

        {&comt}
        "" when s-qtyord <> s-qtybo @ s-qtyship
        0 when s-qtyord = s-qtybo @ s-qtyship
        /{&comt}* */
      with frame f-oeel.
    else    
      display
        s-lineno
        s-aster
        {&line}.shipprod
        s-upcpno
        s-binloc
        s-qtyord
        s-qtybo
        {&line}.unit
        "____________ _____" @ s-underline
        &IF DEFINED(rxserver) <> 0 &THEN
            s-rxline1
        &ENDIF

        {&comb}
        s-qtyship
        "Whse Mgr" @ s-binloc2
        /{&comb}* */

        {&comt}
        "" when s-qtyord <> s-qtybo @ s-qtyship
        0 when s-qtyord = s-qtybo @ s-qtyship
        /{&comt}* */
    with frame f-oeel.

    {&comb}
    assign
        s-wminfo  = if {&altwhse} ne "" then "Whse: " + {&altwhse} +
                        " (of " + string({&line}.qtyship) + " Total)"
                    else "(of " + string({&line}.qtyship) + " Total)".

    display
        icsp.descrip[1] @ s-descrip
        s-wminfo @ s-descrip2
    with frame f-oeel.
    down with frame f-oeel.
    /{&comb}* */

    /*o Display the WM bins to pick from */
    /*tb 21254 12/29/96 jms; Develop Value Add Module - changed ordertype to
        {&ordertype} from "o" */
    {&comt}
    for each wmet use-index k-ord where
        wmet.cono      = {&line}.cono     and
        wmet.ordertype = "{&ordertype}"   and
        wmet.orderno   = {&orderno}       and
        wmet.ordersuf  = {&ordersuf}      and
        wmet.lineno    = {&line}.lineno   and
        wmet.seqno     = {&seqno}
    no-lock:

      if v-frameln = 1 then do:

            assign
                s-wmqtyship = wmet.qtyactual / v-conv
                s-wmqtyship = if {&returnfl} = yes then s-wmqtyship * -1
                              else if s-wmqtyship < 0 then 0
                              else s-wmqtyship
                s-descrip2  = if {&altwhse} ne "" then "Whse: " +
                                  {&altwhse} + "              " +
                                  string(s-wmqtyship,">>>>>>>>9.99-")
                              else "                        " +
                                  string(s-wmqtyship,">>>>>>>>9.99-")
                s-binloc2   = wmet.binloc.

            display
                icsp.descrip[1] @ s-descrip
                s-binloc2 s-descrip2
            with frame f-oeel.
                    down with frame f-oeel.

            v-frameln = v-frameln + 1.

      end. /* if frameln = 1 */

      else do:

            assign
                s-wmqtyship = wmet.qtyactual / v-conv
                s-wmqtyship = if {&returnfl} = yes then s-wmqtyship * -1
                              else if s-wmqtyship < 0 then 0
                              else s-wmqtyship
                s-binlocwm  = wmet.binloc.

            display
                s-binlocwm
                s-wmqtyship
            with frame f-oewm.
            down with frame f-oewm.

      end. /* not frameln 1 */

    end. /* for each wmet */

    /*tb 9731 10/21/98 jgc; Expand non-stock description.  Added second
        display line for proddesc2. */
    if {&line}.stkqtyship = 0 or {&specnstype} = "n" then do:

        display
            {&line}.proddesc when {&specnstype} = "n" @ s-descrip
            icsp.descrip[1] when {&specnstype} = "" @ s-descrip
        with frame f-oeel.
        down with frame f-oeel.
        if {&line}.proddesc2 ne "" then do:
            display {&line}.proddesc2 @ s-descrip3 with frame f-oeel2.
            down with frame f-oeel2.
        end.

    end. /* stkqtyship = 0, specnstype = n */
    /{&comt}* */

  end. /* process for wm product */

  if s-scrapmsg <> "" then do:
        display s-scrapmsg @ s-extracomment with frame f-comments.
        down with frame f-comments.
  end.

  /*tb 3-2 03/29/00 rgm; Rxserver interface hooks for extra varaibles */
  {oeepp9.z99 &before_xref = "*"
            &comr        = "{&comr}"
            &coms        = "{&coms}"
            &returnfl    = "{&returnfl}"
            &cubes       = "{&cubes}"
            &weight      = "{&weight}"
            &head        = "{&head}"
            &line        = "{&line}"
            &section     = "{&section}"
            &ourproc     = "{&ourproc}"
            &specnstype  = "{&specnstype}"
            &altwhse     = "{&altwhse}"
            &vendno      = "{&vendno}"
            &kitfl       = "{&kitfl}"
            &botype      = "{&botype}"
            &orderdisp   = "{&orderdisp}"
            &prevqtyship = "{&prevqtyship}"
            &netamt      = "{&netamt}"
            &prtpricefl  = "{&prtpricefl}"
            &bono        = "{&bono}"
            &orderno     = "{&orderno}"
            &ordersuf    = "{&ordersuf}"
            &custno      = "{&custno}"
            &corechgty   = "{&corechgty}"
            &qtybo       = "{&qtybo}"
            &ordertype   = "{&ordertype}"
            &prefix      = "{&prefix}"
            &seqno       = "{&seqno}"
            &stkqtyshp   = "{&stkqtyshp}"
            &nosnlots    = "{&nosnlots}"
            &comtype     = "{&comtype}"
            &comlineno   = "{&comlineno}"
            &shipto      = "{&shipto}"
            &twlstgedit  = "{&twlstgedit}" }


  /*06/07/96 jms; TB# 21254 Develop Value Add Module
            Value Add does not have cross reference capability */
  {&comxref}

  v-reqtitle = if oeel.xrefprodty = "c" then "   Customer Prod:"
               else if oeel.xrefprodty = "i" then "Interchange Prod:"
               else if oeel.xrefprodty = "p" then " Superseded Prod:"
               else if oeel.xrefprodty = "s" then " Substitute Prod:"
               else if oeel.xrefprodty = "u" then "    Upgrade Prod:"
               else "".

  if can-do("c,i,p,s,u",oeel.xrefprodty) and oeel.reqprod <> ""
  then do:
    s-prod  =   oeel.reqprod.

    find first icsec use-index k-custno where
            icsec.cono      = g-cono    and
            icsec.rectype   = "c"       and
            icsec.custno    = {&custno} and
            icsec.prod      = oeel.reqprod no-lock no-error.

    if avail icsec then
        s-prod = s-prod + " " + icsec.addprtinfo.

    display
        s-prod
        v-reqtitle
    with frame f-oeelc.
    down with frame f-oeelc.

  end. /* cross ref prod display */

  if oeel.xrefprodty ne "c" then do:
    s-prod = if oeel.reqprod ne "" then oeel.reqprod
             else {&line}.shipprod.

    {n-icseca.i "first" s-prod ""c"" {&custno} no-lock}

    if not avail icsec and oeel.reqprod <> "" then
        {n-icseca.i "first" {&line}.shipprod ""c"" {&custno} no-lock}

    if avail icsec then do:
        assign
            s-prod     = icsec.prod
            v-reqtitle = "   Customer Prod:".

      if v-wlupdtype <> "WL-Change":u then do:
            display
                s-prod
                v-reqtitle
            with frame f-oeelc.
      end.
    end.
  end. /* xrefprodty display */

  /{&comxref}* */

  if {&corechgty} = "r" then display with frame f-oeelr.

  /*tb 3-2 03/29/00 rgm; Rxserver interface hook for extra varaibles */
  {oeepp9.z99 &before_prodnotes = "*"
            &comr        = "{&comr}"
            &coms        = "{&coms}"
            &returnfl    = "{&returnfl}"
            &cubes       = "{&cubes}"
            &weight      = "{&weight}"
            &head        = "{&head}"
            &line        = "{&line}"
            &section     = "{&section}"
            &ourproc     = "{&ourproc}"
            &specnstype  = "{&specnstype}"
            &altwhse     = "{&altwhse}"
            &vendno      = "{&vendno}"
            &kitfl       = "{&kitfl}"
            &botype      = "{&botype}"
            &orderdisp   = "{&orderdisp}"
            &prevqtyship = "{&prevqtyship}"
            &netamt      = "{&netamt}"
            &prtpricefl  = "{&prtpricefl}"
            &bono        = "{&bono}"
            &orderno     = "{&orderno}"
            &ordersuf    = "{&ordersuf}"
            &custno      = "{&custno}"
            &corechgty   = "{&corechgty}"
            &qtybo       = "{&qtybo}"
            &ordertype   = "{&ordertype}"
            &prefix      = "{&prefix}"
            &seqno       = "{&seqno}"
            &stkqtyshp   = "{&stkqtyshp}"
            &nosnlots    = "{&nosnlots}"
            &comtype     = "{&comtype}"
            &comlineno   = "{&comlineno}"
            &shipto      = "{&shipto}"
            &twlstgedit  = "{&twlstgedit}" }

  /*tb L1211 04/22/03 blw; Added si mod wl009 & wl010 to standard */
  if v-modwlfl and v-wlwhsefl and p-cartonfl and
      can-find(first t-cartons where
                  t-cartons.orderno  = {&orderno}    and
                  t-cartons.ordersuf = {&ordersuf}   and
                  t-cartons.vaordseq = {&seqno}      and
                  t-cartons.lineno   = {&comlineno}  and
                  t-cartons.lineseq  = if "{&section}" = "vaes":U then
                                           0
                                       else
                                           {&seqno})
  then do:

      /* Clear the display vars */
      do v-numprint = 1 to 2:
          assign
              s-cartons[v-numprint]   = ""
              s-cartonqty[v-numprint] = ""
              s-lineuom[v-numprint]   = ""
              s-cartonlbl[v-numprint] = ""
              s-qtylbl[v-numprint]    = "".
      end. /* do v-printnum = 1 to 2 */

      v-numprint = 1.

      for each t-cartons where
          t-cartons.orderno   = {&orderno}   and
          t-cartons.ordersuf  = {&ordersuf}  and
          t-cartons.vaordseq  = {&seqno}     and
          t-cartons.lineno    = {&comlineno} and
          t-cartons.lineseq   = if "{&section}" = "vaes":U then
                                    0
                                else
                                    {&seqno}
      no-lock
      break by t-cartons.orderno
            by t-cartons.ordersuf
            by t-cartons.vaordseq
            by t-cartons.lineno
            by t-cartons.lineseq
            by t-cartons.cartonid:

          if last-of(t-cartons.cartonid)
          then do:
              assign
                  s-cartons[v-numprint]   = t-cartons.cartonid
                  s-cartonqty[v-numprint] =
                      string(t-cartons.cartonqty,"zzzzzz9.99")
                  s-lineuom[v-numprint]   = t-cartons.lineuom
                  s-cartonlbl[v-numprint] =
                      (if t-cartons.cartonid begins "C":U then
                           "Carton #:"
                       else
                           "Pallet #:")
                  s-qtylbl[v-numprint]    = "Qty:"
                  v-numprint              = v-numprint + 1.
          end. /* last of t-cartons.cartonid */

          if v-numprint > 2 or last-of(t-cartons.lineno)
          then do:
              display
                  s-cartonlbl[1]
                  s-cartons[1]
                  s-qtylbl[1]
                  s-cartonqty[1]
                  s-lineuom[1]
                  s-cartonlbl[2]
                  s-cartons[2]
                  s-qtylbl[2]
                  s-cartonqty[2]
                  s-lineuom[2]
              with frame f-cartonline.
              down with frame f-cartonline.

              /* Reset the vars */
              do v-numprint = 1 to 2:
                  assign
                      s-cartons[v-numprint]   = ""
                      s-cartonqty[v-numprint] = ""
                      s-lineuom[v-numprint]   = ""
                      s-cartonlbl[v-numprint] = ""
                      s-qtylbl[v-numprint]    = "".
              end. /* do v-numprint = 1 to 2 */

              v-numprint = 1.

          end. /* v-numprint = 2 or (last-of(t-cartons.orderno) and ... */

      end. /* for each t-cartons */

  end. /* v-modwlfl, v-wlwhsefl, p-cartonfl and can-find(first t-cartons) */


  /*tb e6764 11/27/00 lbr; Added indication of alternate source */
  if can-do("so,cs",{&head}.transtype) and {&botype} = "d" and
     p-printdropshipfl
  then do:
    display "**** Will be Drop Shipped From an Alternate Source"
    with frame f-dropship.
  end. /* if can-do transtype */

  {oeepp9.z99 &before_notesprt = "*"
            &comr        = "{&comr}"
            &coms        = "{&coms}"
            &returnfl    = "{&returnfl}"
            &cubes       = "{&cubes}"
            &weight      = "{&weight}"
            &head        = "{&head}"
            &line        = "{&line}"
            &section     = "{&section}"
            &ourproc     = "{&ourproc}"
            &specnstype  = "{&specnstype}"
            &altwhse     = "{&altwhse}"
            &vendno      = "{&vendno}"
            &kitfl       = "{&kitfl}"
            &botype      = "{&botype}"
            &orderdisp   = "{&orderdisp}"
            &prevqtyship = "{&prevqtyship}"
            &netamt      = "{&netamt}"
            &prtpricefl  = "{&prtpricefl}"
            &bono        = "{&bono}"
            &orderno     = "{&orderno}"
            &ordersuf    = "{&ordersuf}"
            &custno      = "{&custno}"
            &corechgty   = "{&corechgty}"
            &qtybo       = "{&qtybo}"
            &ordertype   = "{&ordertype}"
            &prefix      = "{&prefix}"
            &seqno       = "{&seqno}"
            &stkqtyshp   = "{&stkqtyshp}"
            &nosnlots    = "{&nosnlots}"
            &comtype     = "{&comtype}"
            &comlineno   = "{&comlineno}"
            &shipto      = "{&shipto}"
            &twlstgedit  = "{&twlstgedit}" }

  if v-loopcnt > 1 and v-oelinefl = true and "{&line}" = "oeel" then do:
     date(substring({&line}.user3,1,10)) no-error.
     if error-status:error then do:
     end.
     else do:
       run oeepp-promdt.p (input "       Promise Date: " +
                     string(date(substring({&line}.user3,1,10)),"99/99/99")). 
     end.
  end.           
  run zsditariffprt (g-cono,{&line}.shipprod,{&line}.whse,"oeepp",8).

  /*o Print the product notes  */
  /* e3207 Added g-cono to call */
  if {&specnstype} ne "n" and avail icsp and icsp.notesfl ne "" then
    run notesprt.p(g-cono,"p",icsp.prod,"","oeepp",8,1).

  /*o tb e3207 Print the product notes for catalog cono = 1 for catalogs*/
  if {&specnstype} = "n" and avail icsc and icsc.notesfl ne "" then
    run notesprt.p(1,"g",icsc.catalog,"","oeepp",8,1).

  /*o Print the serial/lot records */
  /*tb 13356 04/27/95 mtt; Serial/Lot # doesn't print on PO/RM */
  {p-oeepp1.i &type       = "{&ordertype}"
              &line       = "{&line}"
              &pref       = "{&prefix}"
              &seq        = {&seqno}
              &stkqtyship = "{&stkqtyshp}"
              &snlot      = "{&nosnlots}"}

  /*o Print the line item comments  */
  if {&line}.commentfl = yes then do:

    {w-com.i "{&comtype}" {&orderno} {&ordersuf} {&comlineno} yes no-lock}

    if avail com then do i = 1 to 16:

        if com.noteln[i] ne "" then do:

            display com.noteln[i] with frame f-com.
            down with frame f-com.

        end.

    end.

  end. /* print line item comments */

  /*Only comment in the logic if this is from OE (not from VA).*/
  &if "{&comoeout}" <> "out":u &then
        /*Display Internal and external Line Comments*/
        /* TB# e11976 02/11/02 dls; New Subtotal and comment logic */
        /* dkt SX_55 */
        if p-prtintcom = no or v-loopcnt > 1 then do:
            {oeelc.gpr &orderno      = "oeel.orderno"
                       &ordersuf     = "oeel.ordersuf"
                       &lineno       = "oeel.lineno"
                       &linetype     = 'e'
                       &linetype1    = 't'
                       &displayname  = "@ s-extracomment"
                       &framename    = "f-comments"
                       &subdispname  = "@ v-desc"
                       &subframename = "f-oeelsub"}
         end.
         else do:
            {oeelc.gpr &orderno      = "oeel.orderno"
                       &ordersuf     = "oeel.ordersuf"
                       &lineno       = "oeel.lineno"
                       &linetype     = 'e'
                       &linetype1    = 't'
                       &linetype2    = "or oeelc.linetype = 'i'"
                       &displayname  = "@ s-extracomment"
                       &framename    = "f-comments"
                       &subdispname  = "@ v-desc"
                       &subframename = "f-oeelsub"}

         end.
  &endif /* {&comoeout} */

  /*06/26/96 jms; TB# 21254 Develop Value Add Module
    MSDS sheets not used for VA since the customer is own company */
  {&commsds}

  /*o Display an MSDS sheet notice for an MSDS product */
  /*tb 19484 09/09/96 gp; The value in v-msds is not blank when the next
    product is a non-hazardous product.  Add else statement. */
  if v-icmsdsprt = yes and (avail icsp and icsp.msdsfl = yes) then do:

    run oeeppu.p("m",
                 {&custno},
                 {&shipto},
                 {&line}.shipprod,
                 icsp.msdschgdt,
                 output v-printfl,
                 0,
                 0,
                 0,
                 0).

    clear frame f-msds.

    if v-printfl = yes then do:

        v-msds = if icsp.msdssheetno ne "" then "** Include MSDS Sheet " +
                     icsp.msdssheetno + " For This Product With This Order **"
                 else
                    "** Include MSDS Sheet For This Product With This Order **".

        display v-msds with frame f-msds.

    end.

  end.    /* MSDS */

  else v-msds = "".

  /{&commsds}* */


end. /* not v-wlupdtype */

/*tb 19484 08/20/96 gp;  Create a WLEL record for each line */
/*tb 19484 09/09/96 gp;  Add MSDS logic for WL whses */
/*tb 19484 09/16/96 mwb; Added the CS edit per line.  No need to write the data
    to WL for a Counter Sale.  The interface moved to oeepi for a Stk Move out
    of oh-hand in WL. */
/*tb 22491 01/31/97 jl;  Added check for botype. If line attached to AC/PO then
    don't send line to TWL. Added Compile time parameter to only compile in
    if pick ticket is an OE pick Ticket. */
/*tb 23455 07/17/97 mwb; Added the stage edit for oeeh.stagecd < 3 */
/*tb 24253 12/01/97 mwb; Added the tag and hold edit */
/*tb 25120 09/10/98 gkd; Removed tag and hold edit */
/*tb e11381 02/27/02 kjb; Added a new condition to prevent the WL line records
    from being created if the current line on the order is a DO line.  Changed
    the condition from only checking the oeel.botype for an AC/PO to checking
    for an AC/PO and a DO/PO. */
if v-wlwhsefl = yes /*tb L1379 ** and {&head}.transtype ne "cs" */
    {&twlstgedit}
    &if "{&line}" = "oeel" &then
        and not can-do("p,d",{&line}.botype)
    &endif
then do:

    if v-msds ne "" then
        v-msds = if icsp.msdssheetno ne "" then icsp.msdssheetno
                 else "Incl MSDS".

    /*tb 19484 10/30/96 ajw; (B13) Warehouse Logistics - Shipmentid*/
    &if "{&section}" = "oeeh" &then
    {wlpproc.gpr &invoutcond    = "{&head}.transtype <> ""rm"""
                 &wlsetno       = v-wlsetno
                 &whse          = {&head}.whse
                 &ordertype     = ""O""
                 &orderno       = {&orderno}
                 &ordersuf      = {&ordersuf}
                 &lineno        = {&comlineno}
                 &seqno         = 0
                 &msdssheetno   = v-msds
                 &updtype       = ""A""
                 &shipmentid    = """"}
    &else
    {wlpproc.gpr &invoutcond    = YES
                 &wlsetno       = v-wlsetno
                 &whse          = {&head}.whse
                 &ordertype     = ""f""
                 &orderno       = {&orderno}
                 &ordersuf      = {&ordersuf}
                 &lineno        = {&comlineno}
                 &seqno         = {&seqno}
                 &msdssheetno   = v-msds
                 &updtype       = ""A""
                 &shipmentid    = """"}
    &endif
end. /* if v-wlwhsefl = yes */

v-whse = if {&altwhse} ne "" then {&altwhse} else {&line}.whse.

/* 06/14/96 jms; TB# 21254 Develop Value Add Module */
/* BOD kits are not used in the Value Add module
   Explode BOD/non-stock kits and print all components */

if {&kitfl} = yes then do:

  /* dt01 sortoeelk */
/*  03/17/09 tah 
  if icsdAutoPck = "Y" then do:
    03/17/09 tah */
  /*If BOD Transfer kit then lookup the Work Order and
      Print the Components from it.*/
  if {&specnstype}      <> "n":u and
        icsp.kittype       = "b":u and
        icsp.bodtransferty = "t":u and
        can-do("o,b":u,icsp.tiedcompprt) and
        "{&ordertype}" = "o":u then do:

        /*Find Work Order Attached to the Transfer Kit*/
        run tiefind.p (input ({&comtype} + {&orderalttype}),
                       input {&orderno},
                       input {&ordersuf},
                       input {&comlineno},
                       input {&line}.orderaltno,
                       input 0,
                       input  {&line}.linealtno,

                       output iOrderno1,
                       output iOrdersuf1,
                       output iLineno1 ,
                       output rOrderID1,
                       output iOrderno2,
                       output iOrdersuf2,
                       output iLineno2,
                       output rOrderID2).

    /*Print the Components from the Work Order*/
    if string(rorderID2) <> "" then do for kpet:

            find kpet where rowid(kpet) = rOrderID2 no-lock no-error.
            assign
                o-whse = v-whse
                v-whse = if avail kpet then kpet.whse else v-whse.

            &scoped-define comforkpout out

      if avail kpet then
      for each oeelk no-lock where
                oeelk.cono      = g-cono     and
                oeelk.ordertype = "w":u      and
                oeelk.orderno   = kpet.wono  and
                oeelk.ordersuf  = kpet.wosuf and
      oeelk.lineno    = 0:

        /* was kitcompprt1.i dkt 11/19/2008 */
                    
        /* dkt 11/19/2008 */
/*    03/17/09 tah 
        if {&specnstype} = "n" then do:                                    
      03/17/09 tah          */  
          /* dt01 02/03/09
          for each wmsbp where wmsbp.cono = 500 + g-cono and                 
          wmsbp.whse = {&whse} and wmsbp.prod = oeelk.shipprod no-lock,    
          each wmsb where wmsb.cono = wmsbp.cono and wmsb.whse = wmsbp.whse  
          and wmsb.binloc = wmsbp.binloc no-lock break by wmsb.priority:
            leave.
          end.
          */
          
          run findfirstbin.p(input g-cono, input {&whse},
                             input oeelk.shipprod,
                             output wmsbid, output wmsbpid).

          if wmsbid ne ? and wmsbpid ne ? then do:
            find wmsbp where recid(wmsbp) = wmsbpid no-lock no-error.
            find wmsb  where recid(wmsb)  = wmsbid  no-lock no-error.
          end.                                                       
  
          {w-icsw.i {&line}.shipprod                                         
            "if {&altwhse} ne '' then {&altwhse} else {&line}.whse" no-lock}
          {w-icsp.i {&line}.shipprod no-lock}                                

     /* dt01 01/27/2009 sortoeelk */
          if avail wmsb and wmsbid <> ? then
            assign substring(oeelk.user4,65,13) =       
                   string(wmsb.binloc,"xx/xx/xxx/xxx").
          else if avail icsw then
            assign substring(oeelk.user4,65,13) =       
                   string(icsw.binloc1,"xx/xx/xxx/xxx").
/*  03/17/09 tah 
          end.                                                                 
    03/17/09 tah     */       
      end.
      for each oeelk use-index k-oeelk where
        oeelk.cono      = {&line}.cono  and
        oeelk.ordertype = "w"           and
        oeelk.orderno   = kpet.wono     and
        oeelk.ordersuf  = kpet.wosuf    
      no-lock
      break by substring(oeelk.user4,65,13):     
    /* dt01 sortoeelk */
      
        {kitcompprt9.i
                    &line       = "{&line}"
                    &returnfl   = "{&returnfl}"
                    &prefix     = "{&prefix}"
                    &head       = "{&head}"
                    &nosnlots   = "{&nosnlots}"
                    &custno     = "{&custno}"
                    &twlstgedit = " and true = false "
                    &ordertype  = "w"
                    &wono       = kpet.wono
                    &wosuf      = kpet.wosuf
                    &lineno     = 0
                  
                    &whse = "{&whse}"
                    
            &comr        = "{&comr}"
            &coms        = "{&coms}"
            &cubes       = "{&cubes}"
            &weight      = "{&weight}"
            &section     = "{&section}"
            &ourproc     = "{&ourproc}"
            &specnstype  = "{&specnstype}"
            &altwhse     = "{&altwhse}"
            &vendno      = "{&vendno}"
            &kitfl       = "{&kitfl}"
            &botype      = "{&botype}"
            &orderdisp   = "{&orderdisp}"
            &prevqtyship = "{&prevqtyship}"
            &netamt      = "{&netamt}"
            &prtpricefl  = "{&prtpricefl}"
            &bono        = "{&bono}"
            &orderno     = "{&orderno}"
            &ordersuf    = "{&ordersuf}"
            &custno      = "{&custno}"
            &corechgty   = "{&corechgty}"
            &qtybo       = "{&qtybo}"
            &prefix      = "{&prefix}"
            &seqno       = "oeelk.seqno"
            &stkqtyshp   = "{&stkqtyshp}"
            &comtype     = "{&comtype}"
            &comlineno   = "{&comlineno}"
            &shipto      = "{&shipto}"
        }

      end. /* if avail kpet then */
      assign v-whse = o-whse.
    end. /* if string(rorderID2) <> "" */
  end. /* WO component Print*/
  else
    if can-find(first oeelk use-index k-oeelk where
        oeelk.cono      = {&line}.cono     and
        oeelk.ordertype = "o"              and
        oeelk.orderno   = {&orderno}       and
        oeelk.ordersuf  = {&ordersuf}      and
        oeelk.lineno    = {&line}.lineno) then
    for each oeelk use-index k-oeelk where
        oeelk.cono      = {&line}.cono and
        oeelk.ordertype = "o"          and
        oeelk.orderno   = {&orderno}   and
        oeelk.ordersuf  = {&ordersuf}  and
        oeelk.lineno    = {&line}.lineno /* no-lock */:

        &scoped-define comforkpout in

        /* dkt 11/19/2008 */
          /*
          for each wmsbp where wmsbp.cono = 500 + g-cono and                 
          wmsbp.whse = {&whse} and wmsbp.prod = oeelk.shipprod no-lock,    
          each wmsb where wmsb.cono = wmsbp.cono and wmsb.whse = wmsbp.whse  
          and wmsb.binloc = wmsbp.binloc no-lock break by wmsb.priority:
            leave.
          end.
          */
          
          run findfirstbin.p(input g-cono, input {&whse},
                             input oeelk.shipprod,
                             output wmsbid, output wmsbpid).

          if wmsbid ne ? and wmsbpid ne ? then do:
            find wmsbp where recid(wmsbp) = wmsbpid no-lock no-error.
            find wmsb  where recid(wmsb)  = wmsbid  no-lock no-error.
          end.                                                       
 
          {w-icsw.i oeelk.shipprod                                         
            "if {&altwhse} ne '' then {&altwhse} else {&line}.whse" no-lock}
          {w-icsp.i oeelk.shipprod no-lock}                                

          /* dt01 01/27/2009 sortoeelk */
          if avail wmsb and wmsbid <> ? then
            assign substring(oeelk.user4,65,13) =       
                   string(wmsb.binloc,"xx/xx/xxx/xxx").
          else if avail icsw then
            assign substring(oeelk.user4,65,13) =       
                   string(icsw.binloc1,"xx/xx/xxx/xxx").
      end.
  
      for each oeelk use-index k-oeelk where
        oeelk.cono      = {&line}.cono  and
        oeelk.ordertype = "o"           and
        oeelk.orderno   = {&orderno}    and
        oeelk.ordersuf  = {&ordersuf}   and
        oeelk.lineno    = {&line}.lineno 
      no-lock
      break by substring(oeelk.user4,65,13):     
      /* dt01 sortoeelk */

        {kitcompprt9.i &line       = "{&line}"
                       &returnfl   = "{&returnfl}"
                       &prefix     = "{&prefix}"
                       &head       = "{&head}"
                       &nosnlots   = "{&nosnlots}"
                       &custno     = "{&custno}"
                       &twlstgedit = "{&twlstgedit}"
                       &ordertype  = "o"
                       &wono       = 0
                       &wosuf      = 0
                       &lineno     = 0
                    
                       &whse = "{&whse}"
            &comr        = "{&comr}"
            &coms        = "{&coms}"
            &cubes       = "{&cubes}"
            &weight      = "{&weight}"
            &section     = "{&section}"
            &ourproc     = "{&ourproc}"
            &specnstype  = "{&specnstype}"
            &altwhse     = "{&altwhse}"
            &vendno      = "{&vendno}"
            &kitfl       = "{&kitfl}"
            &botype      = "{&botype}"
            &orderdisp   = "{&orderdisp}"
            &prevqtyship = "{&prevqtyship}"
            &netamt      = "{&netamt}"
            &prtpricefl  = "{&prtpricefl}"
            &bono        = "{&bono}"
            &orderno     = "{&orderno}"
            &ordersuf    = "{&ordersuf}"
            &custno      = "{&custno}"
            &corechgty   = "{&corechgty}"
            &qtybo       = "{&qtybo}"
            &prefix      = "{&prefix}"
            &seqno       = "oeelk.seqno"
            &stkqtyshp   = "{&stkqtyshp}"
            &comtype     = "{&comtype}"
            &comlineno   = "{&comlineno}"
            &shipto      = "{&shipto}"
            &twlstgedit  = "{&twlstgedit}" 
                       }

  end.  /* can-find first oeelk... */
  if mecoh-fl = yes then
    display mecoh-line1
            mecoh-line2
            mecoh-line3 with frame f-mecoh.
/*  03/17/09 tah 
  end. /* if icsdAutoPck  sortoeelk */
  else do:

  /* dt01 sortoeelk 
  if not icsdAutoPck then do: */

  /*If BOD Transfer kit then lookup the Work Order and
      Print the Components from it.*/
  if {&specnstype}      <> "n":u and
        icsp.kittype       = "b":u and
        icsp.bodtransferty = "t":u and
        can-do("o,b":u,icsp.tiedcompprt) and
        "{&ordertype}" = "o":u then do:

        /*Find Work Order Attached to the Transfer Kit*/
        run tiefind.p (input ({&comtype} + {&orderalttype}),
                       input {&orderno},
                       input {&ordersuf},
                       input {&comlineno},
                       input {&line}.orderaltno,
                       input 0,
                       input  {&line}.linealtno,

                       output iOrderno1,
                       output iOrdersuf1,
                       output iLineno1 ,
                       output rOrderID1,
                       output iOrderno2,
                       output iOrdersuf2,
                       output iLineno2,
                       output rOrderID2).

    /*Print the Components from the Work Order*/
    if string(rorderID2) <> "" then do for kpet:

            find kpet where rowid(kpet) = rOrderID2 no-lock no-error.
            assign
                o-whse = v-whse
                v-whse = if avail kpet then kpet.whse else v-whse.

            &scoped-define comforkpout out

      if avail kpet then
      for each oeelk no-lock where
                oeelk.cono      = g-cono     and
                oeelk.ordertype = "o":u      and
                oeelk.orderno   = kpet.wono  and
                oeelk.ordersuf  = kpet.wosuf and
      oeelk.lineno    = 0:

        /* was kitcompprt1.i dkt 11/19/2008 */
                
        /* dkt 11/19/2008 */
        if {&specnstype} = "n" then do:                                      
          /*
          for each wmsbp where wmsbp.cono = 500 + g-cono and                 
          wmsbp.whse = {&whse} and wmsbp.prod = oeelk.shipprod no-lock,    
          each wmsb where wmsb.cono = wmsbp.cono and wmsb.whse = wmsbp.whse  
          and wmsb.binloc = wmsbp.binloc no-lock break by wmsb.priority:
            leave.
          end.
          */
          
          run findfirstbin.p(input g-cono, input {&whse},
                             input oeelk.shipprod,
                             output wmsbid, output wmsbpid).

          if wmsbid ne ? and wmsbpid ne ? then do:
            find wmsbp where recid(wmsbp) = wmsbpid no-lock no-error.
            find wmsb  where recid(wmsb)  = wmsbid  no-lock no-error.
          end.                                                       

          /* 01/29/2009 dt01 change {&line}.shipprod to oeelk.shipprod */
          {w-icsw.i oeelk.shipprod                                         
            "if {&altwhse} ne '' then {&altwhse} else {&line}.whse" no-lock}
          {w-icsp.i {&line}.shipprod no-lock}                                

        end.                                                                 
      
        {kitcompprt9.i
                    &line       = "{&line}"
                    &returnfl   = "{&returnfl}"
                    &prefix     = "{&prefix}"
                    &head       = "{&head}"
                    &nosnlots   = "{&nosnlots}"
                    &custno     = "{&custno}"
                    &twlstgedit = " and true = false "
                    &ordertype  = "w"
                    &wono       = kpet.wono
                    &wosuf      = kpet.wosuf
                    &lineno     = 0
                  
                    &whse = "{&whse}"
                    
            &comr        = "{&comr}"
            &coms        = "{&coms}"
            &cubes       = "{&cubes}"
            &weight      = "{&weight}"
            &section     = "{&section}"
            &ourproc     = "{&ourproc}"
            &specnstype  = "{&specnstype}"
            &altwhse     = "{&altwhse}"
            &vendno      = "{&vendno}"
            &kitfl       = "{&kitfl}"
            &botype      = "{&botype}"
            &orderdisp   = "{&orderdisp}"
            &prevqtyship = "{&prevqtyship}"
            &netamt      = "{&netamt}"
            &prtpricefl  = "{&prtpricefl}"
            &bono        = "{&bono}"
            &orderno     = "{&orderno}"
            &ordersuf    = "{&ordersuf}"
            &custno      = "{&custno}"
            &corechgty   = "{&corechgty}"
            &qtybo       = "{&qtybo}"
            &prefix      = "{&prefix}"
            &seqno       = "oeelk.seqno"
            &stkqtyshp   = "{&stkqtyshp}"
            &comtype     = "{&comtype}"
            &comlineno   = "{&comlineno}"
            &shipto      = "{&shipto}"
        }

      end. /* if avail kpet then */
      assign v-whse = o-whse.
    end. /* if string(rorderID2) <> "" */
  end. /* WO component Print*/
  else
    if can-find(first oeelk use-index k-oeelk where
        oeelk.cono      = {&line}.cono     and
        oeelk.ordertype = "o"              and
        oeelk.orderno   = {&orderno}       and
        oeelk.ordersuf  = {&ordersuf}      and
        oeelk.lineno    = {&line}.lineno) then
    for each oeelk use-index k-oeelk where
        oeelk.cono      = {&line}.cono and
        oeelk.ordertype = "o"          and
        oeelk.orderno   = {&orderno}   and
        oeelk.ordersuf  = {&ordersuf}  and
        oeelk.lineno    = {&line}.lineno no-lock:

        &scoped-define comforkpout in

        {kitcompprt9.i &line       = "{&line}"
                       &returnfl   = "{&returnfl}"
                       &prefix     = "{&prefix}"
                       &head       = "{&head}"
                       &nosnlots   = "{&nosnlots}"
                       &custno     = "{&custno}"
                       &twlstgedit = "{&twlstgedit}"
                       &ordertype  = "o"
                       &wono       = 0
                       &wosuf      = 0
                       &lineno     = 0
                    
                       &whse = "{&whse}"
            &comr        = "{&comr}"
            &coms        = "{&coms}"
            &cubes       = "{&cubes}"
            &weight      = "{&weight}"
            &section     = "{&section}"
            &ourproc     = "{&ourproc}"
            &specnstype  = "{&specnstype}"
            &altwhse     = "{&altwhse}"
            &vendno      = "{&vendno}"
            &kitfl       = "{&kitfl}"
            &botype      = "{&botype}"
            &orderdisp   = "{&orderdisp}"
            &prevqtyship = "{&prevqtyship}"
            &netamt      = "{&netamt}"
            &prtpricefl  = "{&prtpricefl}"
            &bono        = "{&bono}"
            &orderno     = "{&orderno}"
            &ordersuf    = "{&ordersuf}"
            &custno      = "{&custno}"
            &corechgty   = "{&corechgty}"
            &qtybo       = "{&qtybo}"
            &prefix      = "{&prefix}"
            &seqno       = "oeelk.seqno"
            &stkqtyshp   = "{&stkqtyshp}"
            &comtype     = "{&comtype}"
            &comlineno   = "{&comlineno}"
            &shipto      = "{&shipto}"
            &twlstgedit  = "{&twlstgedit}" 
                       }

  end.  /* can-find first oeelk... */
   
  end.  /* not icsdAutoPck */
  03/17/09 tah  */
end. /* if kitfl = true*/

/*o Explode Mix kits and print all components */
else do:

if not v-wlupdtype = "WL-Change":u then do:

    /*tb e15321 11/12/02 kjb; Replaced old tally print logic with the new
        logic contained in tallyprt.ldi */
    {tallyprt.ldi &orderno   = "{&orderno}"
                  &ordersuf  = "{&ordersuf}"
                  &lineno    = "{&line}.lineno"
                  &ordertype = "'O'"
                  &bundleid  = "''"}
end. /* if not v-wlupdtype = "WL-Change":u then do: */

end.

/*o Reset the line item print flag */
if {&line}.printpckfl = yes and s-lastfl then
    run oeeppu.p("{&ordertype}",
                 {c-empty.i},
                 " ",
                 " ",
                 g-today,
                 output v-printfl,
                 {&orderno},
                 {&ordersuf},
                 {&line}.lineno,
                 {&seqno}).
