/* TBXR F6 CodeGen End of Includes that are CodeGen Generated  */ 
&if defined(user_optiontype) = 2 &then
if not
can-do("B,C,D,E,I,L,M,N,O,R,T",substring(p-optiontype,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries B,C,D,E,I,L,M,N,O,R,T "
     substring(p-optiontype,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries B,C,D,E,I,L,M,N,O,R,T "
     substring(p-optiontype,v-inx,1).
&endif
 
&if defined(user_registertype) = 2 &then
if not
can-do("B,C,D,E,I,L,M,N,O,R,T",substring(p-register,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries B,C,D,E,I,L,M,N,O,R,T "
     substring(p-register,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries B,C,D,E,I,L,M,N,O,R,T "
     substring(p-register,v-inx,1).
&endif
 
&if defined(user_DWvarheaders1) = 2 &then
if v-lookup[v-inx4] = "B" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Branch".
  end. 
else  
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "CustName".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "District".
  end. 
else  
if v-lookup[v-inx4] = "E" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "EnterDt".
  end. 
else  
if v-lookup[v-inx4] = "I" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "SlsRepIn".
  assign export_rec = export_rec + v-del + "SlsRepInName".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "L" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "LostbusinessCd".
  end. 
else  
if v-lookup[v-inx4] = "M" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "CommissionType".
  end. 
else  
if v-lookup[v-inx4] = "N" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "CancelDt".
  end. 
else  
if v-lookup[v-inx4] = "O" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "SlsRepOut".
  assign export_rec = export_rec + v-del + "SlsRepOutName".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Region".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "TakenBy".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_exportvarheaders1) = 2 &then
if v-lookup[v-inx4] = "B" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Branch".
  end. 
else  
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Customer".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "District".
  end. 
else  
if v-lookup[v-inx4] = "E" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Enter".
  end. 
else  
if v-lookup[v-inx4] = "I" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "SlsRepIn".
  assign export_rec = export_rec + v-del + "SlsRepIn".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "L" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "LostBusiness".
  end. 
else  
if v-lookup[v-inx4] = "M" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "N" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Cancel".
  end. 
else  
if v-lookup[v-inx4] = "O" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "SlsRepOut".
  assign export_rec = export_rec + v-del + "SlsRepOut".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Region".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "TakenBy".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_exportvarheaders2) = 2 &then
if v-lookup[v-inx4] = "B" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Name".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "E" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Date".
  end. 
else  
if v-lookup[v-inx4] = "I" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  assign export_rec = export_rec + v-del + "Name".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "L" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "ReasonCode".
  end. 
else  
if v-lookup[v-inx4] = "M" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "N" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Date".
  end. 
else  
if v-lookup[v-inx4] = "O" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  assign export_rec = export_rec + v-del + "Name".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_loadtokens) = 2 &then
if v-lookup[v-inx] = "B" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.Branch,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "C" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.custname,"x(15)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(15)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "D" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.district,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "E" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.enterdt,"99/99/99")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"99/99/99").   
  end.                                              
else                                              
if v-lookup[v-inx] = "I" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.slsrepin,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "L" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.lostbusinesscd,"x(2)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(2)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "M" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.Commissiontype,"x(15)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(15)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "N" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.Canceldt,"99/99/99")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"99/99/99").   
  end.                                              
else                                              
if v-lookup[v-inx] = "O" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.slsrepout,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "R" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.region,"x(1)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(1)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "T" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.Takenby,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "X" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.batchnm,"x(8)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(8)").   
  end.                                              
else                                              
  assign v-token = " ".
&endif
 
&if defined(user_totaladd) = 2 &then
  assign t-amounts[1] = repTable.sumPrice.
  assign t-amounts[2] = repTable.sumCost.
  assign t-amounts[3] = repTable.cntConvert.
  assign t-amounts[4] = repTable.cntCancel.
  assign t-amounts[5] = 0.
  assign t-amounts[6] = repTable.cntLine.
  assign t-amounts[7] = repTable.cntOrder.
  assign t-amounts[8] = repTable.QsumPrice.
  assign t-amounts[9] = repTable.QsumCost.
  assign t-amounts[10] = 0.
  assign t-amounts[11] = repTable.CvtSumPrice.
  assign t-amounts[12] = repTable.CvtSumCost.
  assign t-amounts[13] = 0.
  assign t-amounts[14] = repTable.CanSumPrice.
  assign t-amounts[15] = repTable.CanSumCost.
  assign t-amounts[16] = 0.
  assign t-amounts[17] = repTable.OpnSumPrice.
  assign t-amounts[18] = repTable.OpnSumCost.
  assign t-amounts[19] = 0.
  assign t-amounts[20] = reptable.cntLost.
  assign t-amounts[21] = reptable.cntOpen.
  assign t-amounts[22] = 0.
  assign t-amounts[23] = 0.
  assign t-amounts[24] = 0.
  assign t-amounts[25] = repTable.cntopen2.
  assign t-amounts[26] = repTable.cntOrderOEET.
  assign t-amounts[27] = repTable.sumpriceOEET.
  assign t-amounts[28] = reptable.sumCostOEET.
  assign t-amounts[29] = 0.
  assign t-amounts[30] = 0.
  assign t-amounts[31] = repTable.sumpriceQUOEET.
  assign t-amounts[32] = reptable.sumCostQUOEET.
  assign t-amounts[33] = 0.
  assign t-amounts[34] = repTable.cntOrderQUOEET.
  assign t-amounts[35] = repTable.cntOBCquote.
  assign t-amounts[36] = 0.
  assign t-amounts[37] = repTable.OBCSumPrice.
  assign t-amounts[38] = repTable.OBCSumCost.
  assign t-amounts[39] = 0.
  assign t-amounts[40] = repTable.cntNOBCquote.
  assign t-amounts[41] = 0.
  assign t-amounts[42] = repTable.NOBCSumPrice.
  assign t-amounts[43] = repTable.NOBCSumCost.
  assign t-amounts[44] = 0.
&endif
 
&if defined(user_numbertotals) = 2 &then
  if v-subtotalup[v-inx4] = 1 then
    v-val = t-amounts1[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 2 then
    v-val = t-amounts2[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 3 then
    v-val = t-amounts3[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 4 then
    v-val = t-amounts4[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 5 then
    v-val = 
      {x-oerzq-fmt5.i "1" "2"}
  else 
  if v-subtotalup[v-inx4] = 6 then
    v-val = t-amounts6[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 7 then
    v-val = t-amounts7[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 8 then
    v-val = t-amounts8[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 9 then
    v-val = t-amounts9[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 10 then
    v-val = 
      {x-oerzq-fmt5.i "8" "9"}
  else 
  if v-subtotalup[v-inx4] = 11 then
    v-val = t-amounts11[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 12 then
    v-val = t-amounts12[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 13 then
    v-val = 
      {x-oerzq-fmt5.i "11" "12"}
  else 
  if v-subtotalup[v-inx4] = 14 then
    v-val = t-amounts14[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 15 then
    v-val = t-amounts15[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 16 then
    v-val = 
      {x-oerzq-fmt5.i "14" "15"}
  else 
  if v-subtotalup[v-inx4] = 17 then
    v-val = t-amounts17[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 18 then
    v-val = t-amounts18[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 19 then
    v-val = 
      {x-oerzq-fmt5.i "17" "18"}
  else 
  if v-subtotalup[v-inx4] = 20 then
    v-val = t-amounts20[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 21 then
    v-val = t-amounts21[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 22 then
    v-val = 
      {x-oerzq-fmt20.i "7" "3"}
  else 
  if v-subtotalup[v-inx4] = 23 then
    v-val = 
      {x-oerzq-fmt20.i "7" "20"}
  else 
  if v-subtotalup[v-inx4] = 24 then
    v-val = 
      {x-oerzq-fmt20.i "7" "25"}
  else 
  if v-subtotalup[v-inx4] = 25 then
    v-val = t-amounts25[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 26 then
    v-val = t-amounts26[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 27 then
    v-val = t-amounts27[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 28 then
    v-val = t-amounts28[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 29 then
    v-val = 
      {x-oerzq-fmt5.i "27" "28"}
  else 
  if v-subtotalup[v-inx4] = 30 then
    v-val = 
      {x-oerzq-fmt20.i "7" "28"}
  else 
  if v-subtotalup[v-inx4] = 31 then
    v-val = t-amounts31[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 32 then
    v-val = t-amounts32[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 33 then
    v-val = 
      {x-oerzq-fmt5.i "31" "32"}
  else 
  if v-subtotalup[v-inx4] = 34 then
    v-val = t-amounts34[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 35 then
    v-val = t-amounts35[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 36 then
    v-val = 
      {x-oerzq-fmt20.i "7" "35"}
  else 
  if v-subtotalup[v-inx4] = 37 then
    v-val = t-amounts37[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 38 then
    v-val = t-amounts38[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 39 then
    v-val = 
      {x-oerzq-fmt5.i "37" "38"}
  else 
  if v-subtotalup[v-inx4] = 40 then
    v-val = t-amounts40[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 41 then
    v-val = 
      {x-oerzq-fmt20.i "7" "40"}
  else 
  if v-subtotalup[v-inx4] = 42 then
    v-val = t-amounts42[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 43 then
    v-val = t-amounts43[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 44 then
    v-val = 
      {x-oerzq-fmt5.i "42" "43"}
  else 
    v-val = t-amounts1[v-inx4].
  do inz = 1 to 44:
  if inz = 1 then
    v-regval[inz] = t-amounts1[v-inx4].
  else 
  if inz = 2 then
    v-regval[inz] = t-amounts2[v-inx4].
  else 
  if inz = 3 then
    v-regval[inz] = t-amounts3[v-inx4].
  else 
  if inz = 4 then
    v-regval[inz] = t-amounts4[v-inx4].
  else 
  if inz = 5 then
    v-regval[inz] = 
      {x-oerzq-fmt5.i "1" "2"}
  else 
  if inz = 6 then
    v-regval[inz] = t-amounts6[v-inx4].
  else 
  if inz = 7 then
    v-regval[inz] = t-amounts7[v-inx4].
  else 
  if inz = 8 then
    v-regval[inz] = t-amounts8[v-inx4].
  else 
  if inz = 9 then
    v-regval[inz] = t-amounts9[v-inx4].
  else 
  if inz = 10 then
    v-regval[inz] = 
      {x-oerzq-fmt5.i "8" "9"}
  else 
  if inz = 11 then
    v-regval[inz] = t-amounts11[v-inx4].
  else 
  if inz = 12 then
    v-regval[inz] = t-amounts12[v-inx4].
  else 
  if inz = 13 then
    v-regval[inz] = 
      {x-oerzq-fmt5.i "11" "12"}
  else 
  if inz = 14 then
    v-regval[inz] = t-amounts14[v-inx4].
  else 
  if inz = 15 then
    v-regval[inz] = t-amounts15[v-inx4].
  else 
  if inz = 16 then
    v-regval[inz] = 
      {x-oerzq-fmt5.i "14" "15"}
  else 
  if inz = 17 then
    v-regval[inz] = t-amounts17[v-inx4].
  else 
  if inz = 18 then
    v-regval[inz] = t-amounts18[v-inx4].
  else 
  if inz = 19 then
    v-regval[inz] = 
      {x-oerzq-fmt5.i "17" "18"}
  else 
  if inz = 20 then
    v-regval[inz] = t-amounts20[v-inx4].
  else 
  if inz = 21 then
    v-regval[inz] = t-amounts21[v-inx4].
  else 
  if inz = 22 then
    v-regval[inz] = 
      {x-oerzq-fmt20.i "7" "3"}
  else 
  if inz = 23 then
    v-regval[inz] = 
      {x-oerzq-fmt20.i "7" "20"}
  else 
  if inz = 24 then
    v-regval[inz] = 
      {x-oerzq-fmt20.i "7" "25"}
  else 
  if inz = 25 then
    v-regval[inz] = t-amounts25[v-inx4].
  else 
  if inz = 26 then
    v-regval[inz] = t-amounts26[v-inx4].
  else 
  if inz = 27 then
    v-regval[inz] = t-amounts27[v-inx4].
  else 
  if inz = 28 then
    v-regval[inz] = t-amounts28[v-inx4].
  else 
  if inz = 29 then
    v-regval[inz] = 
      {x-oerzq-fmt5.i "27" "28"}
  else 
  if inz = 30 then
    v-regval[inz] = 
      {x-oerzq-fmt20.i "7" "28"}
  else 
  if inz = 31 then
    v-regval[inz] = t-amounts31[v-inx4].
  else 
  if inz = 32 then
    v-regval[inz] = t-amounts32[v-inx4].
  else 
  if inz = 33 then
    v-regval[inz] = 
      {x-oerzq-fmt5.i "31" "32"}
  else 
  if inz = 34 then
    v-regval[inz] = t-amounts34[v-inx4].
  else 
  if inz = 35 then
    v-regval[inz] = t-amounts35[v-inx4].
  else 
  if inz = 36 then
    v-regval[inz] = 
      {x-oerzq-fmt20.i "7" "35"}
  else 
  if inz = 37 then
    v-regval[inz] = t-amounts37[v-inx4].
  else 
  if inz = 38 then
    v-regval[inz] = t-amounts38[v-inx4].
  else 
  if inz = 39 then
    v-regval[inz] = 
      {x-oerzq-fmt5.i "37" "38"}
  else 
  if inz = 40 then
    v-regval[inz] = t-amounts40[v-inx4].
  else 
  if inz = 41 then
    v-regval[inz] = 
      {x-oerzq-fmt20.i "7" "40"}
  else 
  if inz = 42 then
    v-regval[inz] = t-amounts42[v-inx4].
  else 
  if inz = 43 then
    v-regval[inz] = t-amounts43[v-inx4].
  else 
  if inz = 44 then
    v-regval[inz] = 
      {x-oerzq-fmt5.i "42" "43"}
  else 
    assign inz = inz.
  end.
&endif
 
&if defined(user_summaryloadprint) = 2 &then
  if v-lookup[v-inx2] = "B" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Branch - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "C" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Customer Name - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "D" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "District - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "E" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Entered Date - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "I" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-smsn.i}
      end. 
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Sales Rep In - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "L" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Lost Business Reason - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "M" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Commission Type - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "N" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Canceled Date - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "O" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-smsn.i}
      end. 
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Sales Rep Out - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "R" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Region - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "T" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Taken By - " + h-lookup + " " + h-genlit.
    end.
  else
      v-summary-lit = v-lookup[v-inx2].
&endif
 
&if defined(user_summaryloadexport) = 2 &then
  if v-inx4 > v-inx2  then 
    do:
    if v-inx4 > 1 then          
      export_rec = export_rec + v-del. 
    assign export_rec = export_rec +  " ".
    if v-cells[v-inx4] = "2" then
      assign export_rec = export_rec +  v-del + " ".
    end. 
  else 
  if v-lookup[v-inx4] = "B" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "C" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(15)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(15)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "D" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "E" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "99/99/99" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "99/99/99" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "I" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-smsn.i}
      end. 
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "L" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(2)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(2)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "M" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(15)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(15)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "N" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "99/99/99" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "99/99/99" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "O" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-smsn.i}
      end. 
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "R" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(1)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(1)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "T" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
    do:
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    assign export_rec = export_rec + v-lookup[v-inx4].
    end.
&endif
 
/* TBXR F6 CodeGen End of Includes that are CodeGen Generated  */ 
