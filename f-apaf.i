/* f-apaf.i 1.1 01/03/98 */
/* f-apaf.i 1.3 10/22/93 */
/*h*****************************************************************************
  INCLUDE      : f-apaf.i
  DESCRIPTION  : 1099 format layouts for 1 - 10 formats
  USED ONCE?   : yes
  AUTHOR       : mwb
  DATE WRITTEN :
  CHANGES MADE : 
    10/22/93 kmw; TB# 12955 1099 Name Needed in APSV - replace apsv.name with
        s-recipnm
    01/31/94 bj ; TB# 17718 Print Both Lines of Vendor Address
    01/16/97 nlp; TB# 22339 Address and Zip codes printing outside of boxes
    07/08/01 gwk; TB# e8953 New format for 1099-misc form
    01/23/02 gwk; TB# e12003 Incorrect lineup when vendor address has 2 lines
    12/23/02 lcb; TB# e11886 print 1099 on laser printers
    01/15/03 lcb; TB# e16084 Add Payer phone number
    01/09/04 kjb; TB# e19347 Phone number overwriting box label - moved phone
        number below address
    08/30/05 gwk; TB# e21598 Account number now required    
    09/16/05 gwk; TB# e21598 Adjust alignment
*******************************************************************************/

form /* style 1 - 1099-A */
    v-void              at 32
    v-correct           at 40 skip(2)
    s-name              at 5
    s-addr[1]           at 5
    s-addr[2]           at 5
    s-city              at 5
    s-state             at 26
    s-zip               at 29 skip(1)
    s-fedtaxid          at 5
    apsv.fedtaxid       at 22               s-date  at 40  s-box2  at 54 skip(2)
    s-recipnm           at 5                s-box3  at 40  s-box4  at 54 skip(1)
    apsv.addr[1]        at 5                s-yesfl at 58  s-nofl  at 62 skip(1)
    apsv.city           at 5
    apsv.state          at 26
    apsv.zip            at 29               s-descrip1 at 40
                                            s-descrip2 at 40
                                            s-descrip3 at 40 skip(4)
with frame f-apaf1 no-box down no-labels width 80.

form /* style 2 - 1099-B */
    v-void              at 32
    v-correct           at 40 skip(1)
                                            s-date          at 40
    s-name              at 5
    s-addr[1]           at 5                s-number        at 40
    s-addr[2]           at 5                               s-nofl  at 57
    s-city              at 5
    s-state             at 26
    s-zip               at 29               s-box2  at 40  s-yesfl at 57 skip(1)
    s-fedtaxid          at 5
    apsv.fedtaxid       at 22               s-box3  at 40  s-box4  at 54 skip(1)
                                            s-descrip1 at 40
    s-recipnm           at 5  skip(1)
    apsv.addr[1]        at 5  skip          s-box6  at 40  s-box7  at 54
    apsv.city           at 5
    apsv.state          at 26
    apsv.zip            at 29 skip(1)       s-box8  at 40  s-box9  at 54 skip(4)
with frame f-apaf2 no-box down no-labels width 80.

form /* style 3 - 1099-DIV */
    v-void              at 32
    v-correct           at 40 skip(2)
    s-name              at 5                s-box1a         at 40
    s-addr[1]           at 5
    s-addr[2]           at 5
    s-city              at 5
    s-state             at 26
    s-zip               at 29               s-box1b         at 40 skip(1)
    s-fedtaxid          at 5
    apsv.fedtaxid       at 22               s-box1c at 40  s-box1d at 54 skip(2)
    s-recipnm           at 5                s-box1e at 40  s-box2  at 54 skip(1)
    apsv.addr[1]        at 5              s-box3  at 40  s-possess at 54 skip(1)
    apsv.city           at 5
    apsv.state          at 26
    apsv.zip            at 29 skip(1)       s-box5  at 40  s-box6  at 54 skip(4)
with frame f-apaf3 no-box down no-labels width 80.

form /* style 4 - 1099-G */
    v-void              at 32
    v-correct           at 40 skip(2)
    s-name              at 5              s-box1  at 40
    s-addr[1]           at 5
    s-addr[2]           at 5
    s-city              at 5
    s-state             at 26
    s-zip               at 29             s-box2  at 40 skip(1)
    s-fedtaxid          at 5
    apsv.fedtaxid       at 22             s-year  at 40  s-box4  at 54 skip(2)
    s-recipnm           at 5              s-box5  at 40  s-box6  at 54 skip(1)
    apsv.addr[1]        at 5              s-box7  at 40  s-checkfl at 65 skip(1)
    apsv.city           at 5
    apsv.state          at 26
    apsv.zip            at 29 skip(6)
with frame f-apaf4 no-box down no-labels width 80.

form /* style 5 - 1099-INT */
    v-void              at 32
    v-correct           at 40 skip(1)       s-number   at 40
    s-name              at 5
    s-addr[1]           at 5
    s-addr[2]           at 5
    s-city              at 5
    s-state             at 26
    s-zip               at 29 skip(1)
    s-fedtaxid          at 5
    apsv.fedtaxid       at 22               s-box1  at 40  skip(2)
    s-recipnm           at 5                s-box2  at 40  s-box3  at 54 skip(1)
    apsv.addr[1]        at 5                s-box4  at 40  skip(1)
    apsv.city           at 5
    apsv.state          at 26
    apsv.zip            at 29 skip(1)
                                          s-box5  at 40  s-possess at 54 skip(4)
with frame f-apaf5 no-box down no-labels width 80.

/*tb 17718 01/31/95 bj; Print Both Vendor Address Lines */
/*tb 22339 01/16/97 nlp; Address and Zip codes printing outside of boxes */
/*tb e8953 07/09/01 gwk; New format for year 2001 */

/***********************OLD FORMAT YEAR 2000 and PRIOR YEARS *****************
form /* style 6 - 1099-MISC */
    v-void              at 32
    v-correct           at 40 skip(1)       s-box1          at 39
    s-name              at 5
    s-addr[1]           at 5                s-box2          at 39
    s-addr[2]           at 5
    s-city              at 5
    s-state             at 26
    s-zip               at 29               s-box3          at 39 skip(1)
    s-fedtaxid          at 5
    apsv.fedtaxid       at 22               s-box4  at 39  s-box5  at 53 skip(1)
    s-recipnm           at 5                s-box6  at 39  s-box7  at 53
    apsv.addr[1]        at 5  skip(1)
    apsv.addr[2]        at 5              s-box8  at 39  s-checkfl at 65 skip(1)
    apsv.city           at 5
    apsv.state          at 25
    apsv.zip            at 28               s-box10 at 39  s-box11 at 53 skip(1)
                                            s-payerno at 39 skip(4)
with frame f-apaf6 no-box down no-labels width 80.
********************** END OF OLD FORMAT FOR YEAR 2000 AND PRIOR YEARS *****/

form /* style 6 - 1099-MISC */

    /* line 1 */
    v-void              at 20
    v-correct           at 29    skip(2)  

    /* line 5 */
    s-name              at 1     s-box1          at 39
    
    /* line 6 */
    s-addr[1]           at 1               
        
    /* line 7 */
    s-addr[2]           at 1              s-box2          at 39

    
    /* line 8 */
    s-city              at 1
    s-state             at 22
    s-zip               at 25
    
    /* line 9 */                          
    s-phoneno           at 1              skip
    
    /* line 10 */
                                          s-box3 at 43  s-box4 at 60 skip(2)

    /* line 13 */    
    s-fedtaxid          at 1
    apsv.fedtaxid       at 21             s-box5 at 43  s-box6 at 60 skip(3)
    
    /* line 17 */
    s-recipnm           at 1              skip
                                          s-box7 at 43  s-box8 at 60 skip(1)
    
    /* line 20 */
    apsv.addr[1]        at 1               
      
    /* line 21 */
    apsv.addr[2]        at 1              s-checkfl at 57 s-box10 at 60 skip(1)
    
    /* line 23 */
    apsv.city           at 1
    apsv.state          at 21
    apsv.zip            at 27             skip(2)
    
    /* line 26 */
    apsv.vendno         at 1              s-box13 at 43  s-box14 at 60 skip(1)
                                              
    /* line 28 */                                              
    s-box15             at 1              s-box16 at 43 s-payerno at 58 
                                          s-box18 at 69  skip(5)
with frame f-apaf6 no-box down no-labels width 80.

/* 1099-misc Laser Printer stock forms */
form /* style 6 - 1099-MISC laser printer forms */                             
                      
    /* line 1 */                                                               
    a-void[1]              at 26                                               
    a-correct[1]           at 36      skip(2)
        
    /* line 4 */                                                               
    a-name[1]              at 5      a-box1[1] at 47                           
               
           
    /* line 5 */                                                               
    a-addr1[1]             at 5                                 
             
    /* line 6 */                                                                
    a-addr2[1]             at 5               
                                                                               
    /* line 7 */                                                                
    a-city[1]              at 5              
    a-state[1]             at 26   
    a-zip[1]               at 29      a-box2[1] at 47
        
    /* line 8 */
    a-phoneno[1]           at 5      skip(1)
                                                
    /* line 10 */                                                               
                                      a-box3[1] at 47  a-box4[1] at 64 skip(3)
                                      
    /* line 14 */           
    a-fedtaxid[1]           at 5      
    a-recipfedtaxid[1]      at 25     a-box5[1] at 47  a-box6[1] at 64 skip(2)
       
    /* line 17 */                                                               
    a-recipnm[1]            at 5       skip(1)
   
    /* line 19 */
                                       a-box7[1] at 47  a-box8[1] at 64 skip(1)
    /* line 21 */                                                               
    a-recipaddr1[1]         at 5                   

    /* line 22 */   
    a-recipaddr2[1]         at  5     a-checkfl[1] at 59 a-box10[1]  at  64
                                              skip(1)

     /* line 24 */  
     a-recipcity[1]          at 5                                              
    a-recipstate[1]         at 25                                    
    a-recipzip[1]           at 31       skip(3)               
     
    /* line 28 */                                                               
   a-vendno[1]             at 5      a-box13[1] at 47  a-box14[1] at 64 skip(1)
                                        
    /* line 30 */                                                      
    a-box15[1]               at 5     a-box16[1] at 47 a-payerno[1] at 64
                                      a-box18[1] at 79 skip(7)
     
/*second half of laser form */                                       

    a-void[2]              at 26                                               
    a-correct[2]           at 36      skip(2)                                  
               
    /* line 4 */                                                                
    a-name[2]              at 5       a-box1[2] at 47                          
            
   /*line 5 */                                                                
   a-addr1[2]              at 5                                                
                
   /* line 6 */                                                                
   a-addr2[2]              at 5                    
   
   /* line 7 */                                                                
   a-city[2]               at 5                                                 
   a-state[2]              at 26                                                
   a-zip[2]                at 29      a-box2[2] at 47
                                                                               
    /* line 8 */              
    a-phoneno              at 5       skip(1)

   /* line 10 */                                                               
                                      a-box3[2] at 47  a-box4[2] at 64 skip(3)
   /* line 14 */                                                              
   a-fedtaxid[2]          at 5                                                 
   a-recipfedtaxid[2]     at 25       a-box5[2] at 47  a-box6[2] at 64 skip(2)
   
    /* line 17 */                                                              
    a-recipnm[2]          at 5        skip(1)                                  
           
   /* line 19 */                                             
                                      a-box7[2] at 47  a-box8[2] at 64 skip(1)

   /* line 21 */                                                               
    a-recipaddr1[2]        at 5                                                
           
   /* line 22 */                                                               
    a-recipaddr2[2]        at 5      a-checkfl[2] at 59 a-box10[2] at 64 skip(1)
   
   /* line 24 */                                                               
    a-recipcity[2]         at 5                                                
    a-recipstate[2]        at 25                                               
    a-recipzip[2]          at 31              skip(2)
            
    /* line 27 */                     
    a-vendno[2]            at 5      a-box13[2] at 47  a-box14[2] at 64 skip(1)

    /* line 29 */                                    
    a-box15[2]             at 5      a-box16[2] at 47 a-payerno[2] at 64       
                                     a-box18[2] at 79                  
with frame f-apaf6a no-box down no-labels width 132.
 
form /* style 7 - 1099-OID */
    v-void              at 32
    v-correct           at 40 skip(2)
    s-name              at 5                s-box1          at 40
    s-addr[1]           at 5
    s-addr[2]           at 5
    s-city              at 5
    s-state             at 26
    s-zip               at 29               s-box2          at 40 skip(1)
    s-fedtaxid          at 5
    apsv.fedtaxid       at 22               s-box3  at 40  s-box4  at 54 skip(1)
                                            s-descrip1 at 40
    s-recipnm           at 5                s-descrip2 at 40
                                            s-descrip3 at 40
    apsv.addr[1]        at 5                s-descrip4 at 40
                                            s-descrip5 at 40
    apsv.city           at 5
    apsv.state          at 26
    apsv.zip            at 29               s-descrip6 at 40 skip(6)
with frame f-apaf7 no-box down no-labels width 80.

form /* style 8 - 1099-PATR */
    v-void              at 32
    v-correct           at 40 skip(1)       s-box1  at 40
    s-name              at 5
    s-addr[1]           at 5                s-box2  at 40
    s-addr[2]           at 5
    s-city              at 5
    s-state             at 26
    s-zip               at 29               s-box3  at 40  skip(1)
    s-fedtaxid          at 5
    apsv.fedtaxid       at 22               s-box4  at 40  skip(2)
    s-recipnm           at 5                s-box5  at 40  skip(1)
    apsv.addr[1]        at 5                s-box6  at 40  s-box7  at 54 skip(1)
    apsv.city           at 5
    apsv.state          at 26
    apsv.zip            at 29               s-box8  at 40  s-box9  at 54 skip(6)
with frame f-apaf8 no-box down no-labels width 80.

form /* style 9 - 1099-R */
    v-void              at 32
    v-correct           at 40 skip(1)       s-box1          at 40
    s-name              at 5
    s-addr[1]           at 5                s-box2          at 40
    s-addr[2]           at 5
    s-city              at 5
    s-state             at 26
    s-zip               at 29 skip(1)
    s-fedtaxid          at 5
    apsv.fedtaxid       at 22           s-box3  at 40  s-box4  at 54 skip(2)
    s-recipnm           at 5            s-box5  at 40  s-box6  at 54 skip(1)
    apsv.addr[1]        at 5            s-code  at 39  s-checkfl at 50 skip(1)
    apsv.city           at 5
    apsv.state          at 26
    apsv.zip            at 29             s-other at 40  s-pctoth at 49
                                                         s-pct    at 62 skip(1)
                                          s-box10 at 40  s-box11  at 54 skip(4)
with frame f-apaf9 no-box down no-labels width 80.

form /* style 10 - 1099-S */
    v-void              at 32
    v-correct           at 40 skip(2)
    s-name              at 5
    s-addr[1]           at 5
    s-addr[2]           at 5
    s-city              at 5
    s-state             at 26
    s-zip               at 29 skip(1)
    s-fedtaxid          at 5
    apsv.fedtaxid       at 22               s-date  at 40  s-box2  at 54 skip(1)
                                            s-descrip1 at 40
    s-recipnm           at 5                s-descrip2 at 40
                                            s-descrip3 at 40
    apsv.addr[1]        at 5                s-descrip4 at 40
                                            s-descrip5 at 40
    apsv.city           at 5
    apsv.state          at 26
    apsv.zip            at 29               s-descrip6 at 40 skip(1)
                                            s-checkfl  at 65 skip(4)
with frame f-apaf10 no-box down no-labels width 80.

