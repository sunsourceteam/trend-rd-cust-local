/* p-oere.i 1.2 09/15/98 */
/*h*****************************************************************************
  INCLUDE      : p-oere.i
  DESCRIPTION  : PO/WT Ties exception edits
  USED ONCE?   : no
  AUTHOR       : mwb
  DATE WRITTEN : 01/13/94
  CHANGES MADE :
    01/13/94 mwb; TB# 14143; rewrote the ties exceptions.
    06/26/95 ajw; TB# 18575 Nonstock/Special Kit Component Project (G3)  Added
        the &tiefile parameter to allow include to be used for OEEL or OEELK
    06/29/95 mtt; TB# 18611 Make changes for KP Work Order Demand (Y1)
    12/10/97 rh;  TB# 18683 Exception incorrect OE tie to PO/BR
    09/15/98 sbr; TB# 25172 Backorders, "Built but order not filled"
*******************************************************************************/

/*e Used in OERE for the PO and WT ties edits, to print on the exception
    report.
        {&file}  = poeh or wteh or kpet
        {&line}  = poel or wtel or kpet
        {&mod}   = module - po or wt or kp
        {&wtcom} = cono not used on WT's
        {&stg}   = break point stage edits for po/wt/wo.
                   stagecd - PO - stg4 = pre rcv
                                - stg5 = rcv stg
                             WT - stg4 = pre rcv
                                - stg5 = exp stg
                                - stg6 = rcv stg
                             WO - stg3 = built stg
        {&tiefile} = the oe file (oeel or oeelk) for tie data
*******************************************************************************/

                if {&tiefile}.orderaltno = 0
                    then v-errorno = 1.
                else if oeel.bono = 0 then
                do i = 0 to 99:
                    find {&line} use-index k-{&line} where
                         {&line}.cono{&2}   = {&tiefile}.cono       and
                         {&line}.{&mod}no   = {&tiefile}.orderaltno and
                         {&line}.{&mod}suf  = i
                         {&kpcom}
                         and {&line}.lineno     = {&tiefile}.linealtno
                        /{&kpcom}* */
                    no-lock no-error.
                    if avail {&line} then do:
                        {&kpcom}
                        find {&file} use-index k-{&file} where
                             {&file}.cono{&2}  = {&tiefile}.cono       and
                             {&file}.{&mod}no  = {&tiefile}.orderaltno and
                             {&file}.{&mod}suf = i no-lock no-error.
                        if not avail {&file} then leave.
                        /{&kpcom}* */

                        /*tb 18683 12/10/97 rh; Exception incorrect OE
                            tied to PO/BR.*/
                        /*tb 25172 09/15/98 sbr; Added check for oeeh.transtype
                            equal to "bl" or "br". */
                        if avail {&file} and {&file}.stagecd >= {&stg} then
                            if oeeh.transtype = "bl" or oeeh.transtype = "br" 
                                then leave. 
                            else next.
                        else if avail {&file} and {&file}.stagecd < {&stg} then
                            if oeeh.transtype = "bl" or oeeh.transtype = "br"
                                then next.
                            else leave.
                    end.
                    else leave.
                end.
                assign
                       v-errorno = if v-errorno ne 0 then v-errorno
                                   else if not avail {&line} and i = 0 and
                                       oeel.bono = 0 then 2
                                   else if oeel.bono ne 0 then 0
                                   else if avail {&file} then
                                       if {&file}.stagecd < {&stg} and
                                          {&tiefile}.qtyship = {&tiefile}.qtyord
                                          then 6
                                       else if {&file}.stagecd = 9 then 3
                                       else if {&file}.stagecd >= {&stg} then
                                           if {&tiefile}.qtyship ne
                                              {&tiefile}.qtyord then 4
                                           else if oeel.botype = "d" and
                                               oeeh.stagecd < 3 then 5
                                           else 0
                                      else 0
                                   else if avail {&line} and not avail {&file}
                                       then 2
                                   else 0
                       v-type    = "{&type}"
                       v-suf     = if avail {&file} then
                                       string({&file}.{&mod}suf,"99")
                                   else "".

