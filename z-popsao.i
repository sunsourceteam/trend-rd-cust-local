/* z-popsao.i 1.1 6/12/92 */
/*h*****************************************************************************
  INCLUDE              : z-popsao.i
  DESCRIPTION          : Custom Assignments for Pop-Up Window on Setups
  AUTHOR               : enp
  DATE LAST MODIFIED   : 04/22/92
  CHANGES MADE AND DATE:
*******************************************************************************/

on ctrl-o anywhere
   do:
     if frame-name  = "f-sasoo" then
        run "zsdisopop".
   end.   
