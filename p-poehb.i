procedure checkbinused:
/* If yes then that item is being received and thus should already
   be assigned to that bin in someone's receipts */
define input parameter inWhse like poehb.whse.
define input parameter inPono like poehb.pono.
define input parameter inProd like poelb.shipprod.
define input parameter inBin  like wmsbp.binloc.
define output parameter outInUse as char.
define buffer bf-poehb for poehb.
define buffer bf-poelb for poelb.
define buffer bf-wmsbp for wmsbp.

outinuse = "".

find first bf-wmsbp where bf-wmsbp.cono = 500 + g-cono
and bf-wmsbp.whse = inWhse and bf-wmsbp.prod = inProd
and bf-wmsbp.binloc = inBin no-lock no-error.
if avail bf-wmsbp then do:
  outinuse = "WMSBP".
  return.
end.

for each bf-poehb where bf-poehb.cono = g-cono
and bf-poehb.whse = inWhse /* tpoehb.whse */
and bf-poehb.pono ne inPono /* tpoehb.pono */ no-lock:
        
  for each bf-poelb where bf-poelb.cono = g-cono 
  and bf-poelb.shipprod = inProd /* tpoehb.prod */
  and bf-poelb.pono = bf-poehb.pono
  and bf-poelb.posuf = bf-poehb.posuf 
  no-lock:
          
    if (substring(bf-poelb.user5,1,6) = "inb856" and
      substring(bf-poelb.user5,7,1) = "y")  or
      (substring(bf-poelb.user5,1,6) <> "inb856" and
      bf-poelb.qtyrcv > 0) then do:
      outinuse = "POELB".
      return.             
    end.
  end. /* bf-poelb */
  /* if errorFlag then leave. */
end. /* bf-poehb */
end procedure.        