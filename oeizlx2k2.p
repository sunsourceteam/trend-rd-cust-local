/*h*****************************************************************************
  SX 55
  PROCEDURE      : oeizl2k2
  DESCRIPTION    : Lookup Orders By Line Items
  AUTHOR         : mtt
  DATE WRITTEN   : 05/11/95
  CHANGES MADE   :
    05/11/95 mtt; TB#  5180 When looking for a product-need kit ln
    09/19/97 kjb; TB# 14318 Quantity/Value displayed is incorrect when a
        Customer PO# is entered - added v-netamt and v-uselnfl
    05/29/98 cm;  TB# 24295 Incorrect qty/value on components. Need to consider
        quantity needed per kit.
    03/31/99 lbr; TB# 20955 Added ability to display orders in accending = yes          or decending = no
*******************************************************************************/

/*e*****************************************************************************
     Selection Criteria for this procedure:
       v-mode ==> 2:
               Product: entered on the screen
    Kit Component Type: "k" (Kit Components Only)
             Customer#: <blank> (0)
               CustPO#: <blank>
                  Whse: <blank>
*******************************************************************************/

{g-all.i}
{g-ar.i}
{g-ic.i}
{g-oe.i}
{g-inq.i}
{oeizl2k2.z99 &user_defvar = "*"}
def var v-netamt    like oeel.netamt                no-undo.
def var v-uselnfl   as logical      init no         no-undo.

{f-oeizl.i}
def var v-xmode     as character    init "C"        no-undo.
/* TAH 082707 */
def var v-ninesfl   as logical      init false      no-undo.
/* TAH 082707 */


def var s-dtranstype like oeeh.transtype            no-undo.
def var s-dstagecd   like oeeh.stagecd              no-undo.
def var s-dpromisedt like oeeh.promisedt            no-undo.
def var s-dinvoicedt like oeeh.invoicedt            no-undo.
def var s-dtakenby   like oeeh.takenby              no-undo.

def temp-table zxinq no-undo
  field present  as logical.
  
create zxinq.
assign zxinq.present = true.  
  
 

/*tb 24295 05/29/98 cm; Incorrect qty/value on components. Include IP. */
/*d Internal procedures used to calculate totals */
{oeizltot.gip}

/*tb 11811 04/28/95 smb; Changed substring for line to be 13*/
/*tb 20955 04/01/99 lbr; Added direction flag */
{oeizl2k2.z99 &user_befmain = "*"}
main:
do while true with frame f-oeizl on endkey undo main, leave main:
    if {k-cancel.i} or {k-jump.i} then return.
    repeat:
        {xxilx.i &find      = "xoeizl2k2.lfi"
                 &frame     = "f-oeizl"
                 &file      = "zxinq"
                 &field     = "s-order"
                 &display   = "d-xoeizl.i"
                 &where     = "p-xoeizl.i"
                 &usecode   =  *
                 &usefile   = "a-xoeizl.i"
                 &global    = "assign g-orderno =
                               integer(substring(frame-value,1,8))
                               g-ordersuf =
                               integer(substring(frame-value,10,2))"
                 &direction = s-directionfl}.
    end.
    leave.
end.

