/* zsdif-oeepa6.i 1.6 04/16/93 */
/*h*****************************************************************************
  INCLUDE      : zsdif-oeepa6.i
  DESCRIPTION  : Forms & defines for oeepa1 and oeepaf1
  USED ONCE?   : No (oeepa1.p & oeepaf1.p)
  AUTHOR       :
  DATE WRITTEN : 04/03/92
  CHANGES MADE :
    04/03/92 pap; TB#  6219 Line DO - add DO line indicator
    11/11/92 mms; TB#  8561 Display product surcharge label from ICAO
    04/08/93 jlc; TB# 10155 Display customer products.
    04/13/93 jlc; TB#  7245 Incorrect backorder qty.
    11/01/95 gp;  TB# 18091 Expand UPC Number (T24).  Add s-upcpno.
    03/20/96 kr;  TB# 12406 Variables defined in form, moved to calling programs
    04/17/97 JPH; ITB# 1    Remove upc item, qty b/o qty shpd from detail.
    06/10/09 das; NonReturn/NonCancelable
    06/12/09 das; Allow Tendered Invoices to Print
*******************************************************************************/

{f-blank.i}

/* detail line and prod descrip */
form  
    s-lineno                             at   1
    oeelb.shipprod                       at   5
/*    s-upcpno                             at  30 */  /*ITB# 1 */
    s-qtyord                             at  42
    s-qtybo                              at  53
    s-qtyship                            at  65
    oeelb.unit                           at  78
    s-price                              at  84
    s-prcunit                            at 100
/*  s-discpct                            at 102        */
    x-shipdt                             at 108
    x-astriks                            at 116        /*das*/
    s-netamt                             at 117
    s-sign                               at 130 
    s-lasterisk                          at 131
    skip
    s-descrip                            at   5
    s-leadtmlit                          at 108
    s-leadtm                             at 116
with frame f-oeelb no-box no-labels no-underline down width 132.

/* form to display core charge */
form  
    oeelb.shipprod                       at   5
    s-price                              at  84
    s-netamt                             at 117
with frame f-oeel1 no-box no-labels no-underline down width 132.

form
    v-reqtitle                           at   5
    s-prod                               at  23
with frame f-oeelc no-box no-labels width 132.

form
    "** CORE RETURN **"                  at   8
with frame f-oeelr no-box side-labels width 132.

form
    "** DIRECT ORDER **"                 at   8
with frame f-oeeldo no-box side-labels width 132.

form
    "Kit"                                at   1
    oeelk.instructions                   at   5
with frame f-refer no-box no-labels width 132.

form
with frame f-com no-box no-labels col 8 width 80.

form
with frame f-notes2 no-box no-labels col 8 width 80.

form
  s-noteln   at 1
with frame f-notes no-box no-labels width 200.
    
form
    "============="                      at 117
    "Subtotal:"                          at 107
    v-subnetdisp                         at 117 

    skip(1)
with frame f-oeelsub no-box no-labels no-underline width 132.
