/* -------------------------------------------------------------------------
   Include Name : po-SdiDebug.i
   Description  : This include contains logic in ZsdiDebug that determines
                  settings used for a given application as well as supplies
                  logic to commit the log to it's appropriate file
   ------------------------------------------------------------------------- */ 
Procedure ZsdiDebug:
  Define Input Parameter ip-cono      as integer           no-undo.
  Define Input Parameter ip-objectid  as char              no-undo.

  find first zsastz where 
             zsastz.cono       = ip-cono and
             zsastz.labelfl    = true    and
             zsastz.codeiden   = "SdiDebug"  no-lock no-error.
  if not avail zsastz then do: /* Debugging not set up */
    assign v-setup-debugfl = false.
    return.
  end.

/* Check and set up default settings in case there are not any
   setups 
*/   

  assign
   v-setup-logfilename      =  "apilog.lxg"
   v-setup-loglevel         =  "None" 
   v-setup-log-dir          = "/rd/cust/local/API"
   v-setup-logmaxfilesize   =  0   
   v-setup-debugfl          =  false.
  

  find first zsastz where 
             zsastz.cono       = ip-cono    and
             zsastz.labelfl    = false      and
             zsastz.codeiden   = "SdiDebug" and
             zsastz.primarykey = "Default" no-lock no-error.
  if not avail zsastz then do: /* Debugging not set up */
    assign v-setup-debugfl = false.
    return.
  end.
  else 
  if avail zsastz then do: 
    assign    
      v-setup-log-dir           =  (if zsastz.codeval[4] = "" then
                                     v-setup-log-dir
                                   else
                                      zsastz.codeval[4])
      v-setup-logmaxfilesize   = (if int(zsastz.codeval[3]) = 0 then
                                    0    /* unlimited size */
                                  else
                                    int(zsastz.codeval[3]))
      v-setup-logfilename      =  (if zsastz.codeval[2] = "" then
                                     "apilog.lxg"
                                   else
                                      zsastz.codeval[2])
      v-setup-objectname       =  ip-objectid
      v-setup-loglevel         =  if zsastz.codeval[1] = "" then
                                    "None"
                                  else
                                    zsastz.codeval[1].
    if v-setup-loglevel = "none" then
      assign v-setup-debugfl = false.
    else
      assign v-setup-debugfl = true.
  end. /* zsastz default debugging options */


  find first zsastz where 
             zsastz.cono       = ip-cono and
             zsastz.labelfl    = false    and
             zsastz.codeiden   = "SdiDebug" and
             zsastz.primarykey = ip-objectid no-lock no-error.
  if avail zsastz then do:  /* Move debug settings to variables */
    assign    
      v-setup-log-dir          =  (if zsastz.codeval[4] = "" then
                                     v-setup-log-dir
                                   else
                                      zsastz.codeval[4])
      v-setup-logmaxfilesize   = (if int(zsastz.codeval[3]) = 0 then
                                    0    /* unlimited size */
                                  else
                                    int(zsastz.codeval[3]))
      v-setup-logfilename      =  (if zsastz.codeval[2] = "" then
                                     "apilog.lxg"
                                   else
                                     zsastz.codeval[2])
      v-setup-objectname       =  zsastz.primarykey
      v-setup-loglevel         =  if zsastz.codeval[1] = "" then
                                    "None"
                                  else
                                    zsastz.codeval[1].
  end. /* zsastz specific logging setup for application */                       
  assign v-setup-logfilefull = v-setup-log-dir + v-setup-logfilename.
    
  if v-setup-loglevel = "none" then
    assign v-setup-debugfl = false.
  else
    assign v-setup-debugfl = true.
              
end.  /*  Procedure ZsdiDebug  */
 

Procedure ZsdiLogWrite:
  Define Input Parameter ip-loglevel as char no-undo.
  Define Input Parameter ip-logtext  as char no-undo.
 
  if not v-setup-debugfl then 
    return.
  if ip-loglevel = "None" then
    return.
  else
  if ip-loglevel = "Maximum" and 
     v-setup-loglevel = "Minimum" then
    return.

  if search(v-setup-logfilefull) = ? then do:
    output stream Debugmylog to value(v-setup-logfilefull).
    os-command value("chmod 666 " + v-setup-logfilefull).
  end.
  else do:  /* file exists so check the size to make sure it 
               does not exceed the max */
    if v-setup-logmaxfilesize = 0 then do:
      output stream Debugmylog to value(v-setup-logfilefull) append.
    end.
    else do:
      FILE-INFO:FILE-NAME = v-setup-logfilefull.
      if FILE-INFO:FILE-SIZE > v-setup-logmaxfilesize then do:
        output stream Debugmylog to value(v-setup-logfilefull).
        os-command silent value("chmod 666 " + v-setup-logfilefull).
      end.
      else
        output stream Debugmylog to value(v-setup-logfilefull) append.
    end.
  end.  

  
  put stream Debugmylog unformatted
    string(today,"99/99/99") 
    " "
    string(time,"hh:mm:ss")
    " "
    v-setup-objectname
    " - "
    ip-logtext
    skip.
    
  output stream Debugmylog close.
end.



