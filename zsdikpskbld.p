/* zsdikpskbld.p 1.1 01/02/98 */
/* zsdikpskbld.p 1.12 02/09/95 */
/*h*****************************************************************************
  PROCEDURE    : zsdikpskbld.p
  DESCRIPTION  : KP Setup Kits
  AUTHOR       : SunSource
  DATE WRITTEN : 09/09/04
  CHANGES MADE :
*******************************************************************************/

/*d From g-all.i */
def {1} shared var g-add          as logical                           no-undo.
def {1} shared var g-cono         as integer format ">>>9"             no-undo.
def {1} shared var g-errfl        as logical                           no-undo.
def {1} shared var g-lkupfl       as logical                           no-undo.
def {1} shared var g-secondsel    as c format "x(5)"                   no-undo.
def {1} shared var g-operinit     as c format "x(4)"                   no-undo.
def {1} shared var g-secure       as i format "9"                      no-undo.
def {1} shared var g-title        like sassm.frametitle                no-undo.
def {1} shared var g-inqfl        as logical                           no-undo.
def {1} shared var g-operinits    like g-operinit                      no-undo.
def {1} shared var g-triggerfl    as logical                           no-undo.


/*tb 25582 04/26/99 gwk; {nav.gva} to define g-currproc for use by t-all.i*/
{nav.gva}

def            var confirm        as logical                           no-undo.
def            var i              as i                                 no-undo.
def            var j              as i                                 no-undo.
def            var v-invalid      as c format "x(13)"
                                  initial "** Invalid **"              no-undo.
{g-ic.i}
{g-setup.i}
{applhelp.gva}

def buffer b-icsp  for icsp.
def buffer b2-icsp for icsp.
def buffer b2-icsw for icsw.

def            var v-kitprodtype  like icsp.prodtype initial "s"       no-undo.
def            var ox-type        as int                               no-undo.
def            var vx-parentnm    as char  format "x(30)"              no-undo.
def            var v-errfl        as c format "x"                      no-undo.
def            var v-seqno        like kpsk.seqno                      no-undo.
def            var o-prod         like kpsk.prod                       no-undo.
def            var o-comprod      like kpsk.comprod                    no-undo.
def            var o-refer        like kpsk.refer                      no-undo.
def            var o-kittype      like icsp.kittype                    no-undo.
def            var o-comptype     like kpsk.comptype                   no-undo.
def            var o-unit         like kpsk.unit                       no-undo.
def            var s-comptype     like kpsk.comptype                   no-undo.
def            var s-comprod      as c format "x(24)"                  no-undo.
def            var s-label        as c format "x(13)"                  no-undo.
def new shared var s-keyword      as c format "x(8)" extent 5          no-undo.
def            var s-desc         as c format "x(24)"                  no-undo.
def            var s-lookupnm2    like icsp.lookupnm                   no-undo.
def            var s-notesfl2     like icsp.notesfl                    no-undo.
def            var s-unit         like kpsk.unit                       no-undo.
def new shared var v-kpskrecid    as recid                             no-undo.
def new shared var v-newrecfl     as l                                 no-undo.
def new shared var v-errorfl      as l                                 no-undo.
def            var v-kconv        as de                                no-undo.
def            var v-prod         like icsp.prod                       no-undo.

/*tb 12406 03/20/96 kr; Variable defined in form f-kpskr.i */
def            var s-instructions as char format "x(50)"               no-undo.

/*tb 12406 03/20/96 kr; Variables defined in form f-kpskh.i */
def            var s-notesfl1     like icsp.notesfl                    no-undo.
def            var s-lookupnm1    like icsp.lookupnm                   no-undo.
def            var s-seqno        like kpsk.seqno                      no-undo.


/*d Save off global product */
v-prod = g-prod.

define shared temp-table vx
  field parent      as character
  field whse        like icsd.whse
  field part        like icsp.prod
  field vseqno      as integer 
  field vendprod    like icsw.vendprod
  field icspfl      as logical
  field type        as integer format "99"
  field vtitle      as character 
  field qty         as decimal format "zzzzz9.99"
  field descr       as character 
  field zcode       as character format "xx" 
  field recnbr      as integer 
index v-inx
        type
        part
        vseqno
        zcode 
        whse.

main:
do while true on endkey undo main, leave main
              on error  undo main, leave main:
 for each vx exclusive-lock:

    assign
        vx-parentnm  = vx.parent
        ox-type      = vx.type
        s-keyword    = ""
        s-seqno      = vseqno
        s-label      = ""
        g-prod       = vx.part
        g-whse       = vx.whse
        v-errfl      = ""
        g-errfl      = no
        s-comprod    = "".

    /* Check for error conditions */
    {w-icsp.i g-prod no-lock}
    
    if avail icsp then
     do:
        assign s-notesfl1  = icsp.notesfl
               s-lookupnm1 = icsp.lookupnm
               s-unit      = icsp.unitstock
               s-desc      = icsp.descrip[1] 
               v-kconv     = 1
               confirm     = yes.

         if icsp.kittype = "p" and vx.type = 1 then 
           next.
          
         /* Must be a Kit product -> icsp.kittype = 'p' or 'b' */
         /*tb 17387 02/09/95 kjb; If the product is not a kit, give a
          warning but allow the user to continue. */
            if icsp.kittype ne "" and vx.type = 2 then
             do:
                g-errfl = yes.
                message "Warning: Product Set Up as Kit in ICSP - ".
                message "Item - " vx.part. 
                pause. 
                next.
             end. /* end of if icsp.kittype ne  "p" */

      {q-kpeti.i
        &prod    = icsp.prod
        &unit    = icsp.unitstock
        &comment = "/*"}

      /* v-errfl = e -> build on demand kit - skip because valid */
        if v-errfl = "e" then v-errfl = "".
        if v-errfl <> "" or g-errfl = yes then
         do:
            {p-kpserr.i}
             if v-errfl = "f" then 
                message "Component not setup in ICSW".
             message "Item - " vx.part vx.whse. 
             pause.
             v-errfl = "".
             next.
         end. /* end of if v-errfl <> "" */

     end. /* avail icsp */
     else 
     do:
         g-errfl = yes.
         message "Warning: Product Not Set Up in ICSP - KPSK not created - ".
         message "Item - " vx.part. 
         pause. 
         next.
     end.

     {w-kpsk.i g-prod s-seqno no-lock}

     {w-icsw.i vx.part vx.whse}

      if avail kpsk and s-seqno > 0 then 
       do: 
          next.
       end.

       if s-seqno = 0 then
        do:
            {p-kpsnxt.i &file   = "kpsk"
                        &field1 = "prod"
                        &field2 = "vx.parent"}
        end. /* end of if s-seqno = 0 */

        {w-kpsk.i vx.parent s-seqno exclusive-lock}
         v-newrecfl = no.

        if not avail kpsk then
         do:
           create kpsk.
           /* Clear out fields when component types change */
           if vx.type ne ox-type then
            do:
              assign kpsk.compfl      = no
                     kpsk.comprod     = ""
                     kpsk.kitrollty   = ""
                     kpsk.pricefl     = no
                     kpsk.printfl     = no
                     kpsk.qtyfl       = no
                     kpsk.qtyneeded   = 1
                     kpsk.refer       = ""
                     kpsk.reqfl       = no
                     kpsk.requestprod = ""
                     kpsk.subfl       = no
                     kpsk.sublistfl   = no
                     kpsk.unit        = ""
                     kpsk.variablefl  = no.
            end. /* end of if o-comptype <> kpsk.comptype */

            assign  v-newrecfl     = yes
                    kpsk.transtm   = substring(string(time,"hh:mm"),1,2) +
                                     substring(string(time,"hh:mm"),4,2)
                    kpsk.transdt   = today 
                    kpsk.operinit  = g-operinit
                    kpsk.kitrollty = ""
                    kpsk.cono      = g-cono
                    kpsk.prod      = vx.parent
                    kpsk.comprod   = if vx.type = 2 then 
                                        vx.part
                                     else
                                        ""
                    kpsk.seqno     = s-seqno
                    vx.vseqno      = s-seqno
                    kpsk.user5     = vx.descr
                    kpsk.qtyneeded = vx.qty
                    kpsk.pricefl   = no
                    kpsk.printfl   = no
                    kpsk.serlotfl  = if avail icsw and 
                                              icsw.serlottype ne "" then 
                                        yes 
                                     else 
                                        no        
                    kpsk.compfl    = if vx.type = 1 then 
                                        no 
                                     else 
                                        yes.
        end. /* end of not avail kpsk */
        else
           next.

        assign
            v-kpskrecid  = recid(kpsk)
            s-comprod    = kpsk.comprod
            o-refer      = kpsk.refer
            o-kittype    = if avail icsp then icsp.kittype else o-kittype
            s-comptype   = "c" 
            o-comptype   = kpsk.comptype
            s-keyword[1] = substring(kpsk.comprod,1,8)
            s-keyword[2] = substring(kpsk.comprod,12,8)
            s-keyword[3] = substring(kpsk.comprod,23,8)
            s-keyword[4] = substring(kpsk.comprod,34,8)
            s-keyword[5] = substring(kpsk.comprod,45,8)
            s-notesfl2   = ""
            s-lookupnm2  = ""
            o-comprod    = kpsk.comprod
            o-unit       = ?
            v-prod       = g-prod.
        confirm = yes.
        {p-unit.i &unitconvfl = icsp.unitconvfl
                  &unitstock  = icsp.unitstock
                  &unit       = s-unit
                  &desc       = s-desc
                  &conv       = v-kconv
                  &confirm    = confirm
                  &prod       = icsp.prod}
         if not confirm then
          do:
            message "Units Not Set Up in Unit Table - ICSEU or SASTT - 4026 ". 
            pause 0. 
            next.
          end. /* end of if not confirm */

        assign
            kpsk.comptype = "c"
            kpsk.unit     = icsp.unitstock.

 end. /* for each vx */
 leave main.
end. /* end of main */

message "BOM(kpsk) - Kit build complete for " + vx-parentnm.
pause 2. 
return.


