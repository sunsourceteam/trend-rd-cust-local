/*h*****************************************************************************
  INCLUDE      : d-oeizs.i
  DESCRIPTION  : Commission Batch display line 
  USED ONCE?   : yes
  AUTHOR       :
  DATE WRITTEN : 04/02/15
  CHANGES MADE :
*******************************************************************************/

if oeelb.specnstype ne "n" then
    {w-icsp.i oeelb.shipprod no-lock}

/*tb 7241  (4.2) 02/24/97 jl;  Special Price Costing. Added icss.gfi */
{icss.gfi
    &prod        = oeelb.shipprod
    &icspecrecno = oeelb.icspecrecno
    &lock        = "no"}
/*
find pdsc use-index k-pdrecno where
    pdsc.cono     = oeelb.cono and
    pdsc.pdrecno  = oeelb.pdrecno
no-lock no-error.
*/

assign {speccost.gas &com = "/*"}
       s-speccost   = v-prccostper
       s-commentfl  = if oeelb.commentfl = yes then "c"
                      else ""
       s-jitflag    = if oeehb.orderdisp = "j" then
                          if oeehb.stagecd <= 1 then
                              if  oeehb.reqshipdt = oeelb.reqshipdt and
                                  oeehb.promisedt = oeelb.promisedt then ""
                              else "j"
                          else if oeelb.jitpickfl = yes then ""
                          else if  oeehb.reqshipdt = oeelb.reqshipdt and
                                   oeehb.promisedt = oeelb.promisedt then ""
                          else "j"
                      else ""
       s-lineno     = oeelb.lineno
       s-specnstype = oeelb.specnstype
       s-prod       = oeelb.shipprod
       s-notesfl    = if not can-do("n,l",oeelb.specnstype) then
                          (if avail icsp then icsp.notesfl else "")
                      else ""
       s-qtyord     = oeelb.qtyord
       s-returnfl   = oeelb.returnfl
       s-return     = /* if oeelb.qtyreturn <> 0 then "r" else */ ""
       s-unit       = oeelb.unit
       s-price      = oeelb.price
       s-discamt    = /* if oeelb.disctype then */ oeelb.discamt
                     /*  else oeelb.discpct  */
       s-disctype   = oeelb.disctype
       s-qtybreakty = /* if avail pdsc and pdsc.qtybreakty <> "" then "q"
                         else */ ""
       s-proddesc   = if can-do("n,l",oeelb.specnstype) then oeelb.proddesc +
                        " "  + oeelb.proddesc2 
                      else if ( /* oeelb.altwhse ne "" or */
                                oeelb.xrefprodty ne "")
                                and avail icsp then
                          icsp.descrip[1] + " " +
                      /*
                          (if oeelb.altwhse ne "" then
                              "(From: " + oeelb.altwhse + ")"
                       */
                        
                           (if oeelb.xrefprodty = "s" then "(Substitute)"
                            else if oeelb.xrefprodty = "u" then "(Upgrade)"
                            else if oeelb.xrefprodty = "p" then "(Supersede)"
                            else icsp.descrip[2])

                      else if avail icsp then
                          icsp.descrip[1] + " " + icsp.descrip[2]
                      else ""
       s-rushdesc   = if oeelb.rushfl then "Rush" else ""
       /*tb e7852 07/28/00 lcg; AR Multicurrency apply excchange rate */
       v-mardiscoth = /* (oeelb.discamtoth + oeelb.wodiscamt) * v-exchrate */ 0
       v-mardisc    = 0
       v-marcost    = 0
       v-marnet     = 0
       v-marprice   = 0
       v-marqty     = 0
       s-marginamt  = 0
       s-marginpct  = 0
       v-conv       = {unitconv.gas &decconv = 1}.

assign v-exchrate   = 1.

assign
    s-discpct = s-discamt
    s-netord  = s-netamt.
   {a-oeetlm.i &oeel    = "s-"
               &oeeh    = "oeehb."
               &icss    = "v-"
               &com     = "/*"
               &com2    = "/*"
               &rebcom  = "/*"
               &sasc    = "/{&blank}*"}

assign
    s-netamt    = (v-marnet + v-mardiscoth) * (if oeelb.returnfl then -1 else 1)
    s-linedo    = if oeelb.botype = "d" then "d" else ""
    v-exchrate  = 1. /* oeehb.salesexrate. */

run oeizsl99.p("bld").  /* bld = before line display */

display
    s-commentfl
    s-lineno
    s-specnstype
    s-prod
    s-notesfl
    s-qtyord
    s-returnfl
    s-unit
    s-price
    s-discamt
    s-disctype
    s-jitflag
    s-proddesc
    s-netamt
    s-speccost
    s-qtybreakty
    s-return
    s-rushdesc
    s-linedo
with frame f-oeiol.



