/* tiac01.p based on tials.p
 dkt 10/24/2006 created
 made so that WY will not be taxable even if the customer is taxable.
 Updates the taxtable by running tax025 After the import.
 tax025 reads the parm025 file which has 5112 = 51 WY, 1 state, 2 no tax

 I believe this replaced tirunx in the TIAC sassm menu 
*/

/* Taxware step load */
/* report values, required for all reports */
{p-rptbeg.i}

find sapb where recid(sapb) = v-sapbid no-lock no-error.
pause 0 before-hide.                                                    

/* Constants */
def var develPath as char initial "/usr/tmp/stepdevelopment/" no-undo.
def var prodPath  as char initial "/usr/tmp/stepproduction/"  no-undo.
def var necFile  as char extent 3
  initial ["stepprod", "steprate", "steptec"] no-undo. 

/* Variables */
def var whichDb as char no-undo.
def var currPathname as char no-undo.
def var indxFile as integer no-undo.
def var indxDb as integer no-undo.
def var maxNo  as integer initial 10 no-undo.
def var dName as char extent 10 no-undo.
def var errFlag as logical no-undo.
def var fnd as char no-undo.
def var v-executable as char no-undo. 
def var s-file as char no-undo.       
  

errFlag = no.
do indxDb = 1 to maxNo:
  dName[indxDb] = ldbname(indxDb).
end.
do indxDb = 2 to maxNo:
  if dName[indxDb] ne ? then errFlag = yes.
end.
if dName[1] = "?" then errFlag = yes.
if errFlag then do:
  message "More than one database connected!".
  leave.
end.
else do:
  if dName[1] = "devnxt" then whichDb = "Test/Development Database:".
  else if dName[1] = "nxt" then whichDb = "Production System Database:".
  else whichDb = "UNKNOWN Database:".
  message "Connected to" whichDb dName[1].
end.
if dName[1] = "devnxt" then do:
  v-executable =  "/devel/rd/taxware/salestax/tax025".
  s-file = "/usr/tmp/tiacOut.txt".
  unix silent value(v-executable) value(" > " + s-file).
end.
else if dName[1] = "nxt" then do:
  v-executable =  "/rd/taxware/salestax/tax025".   
  s-file = "/usr/tmp/tiacOut.txt".                       
  unix silent value(v-executable) value(" > " + s-file).
end.
else do:
  message "Connected to UNKNOWN database:" dName[1].
  errFlag = yes.
end.

