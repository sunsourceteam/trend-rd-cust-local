/* SX 55
   f-oeetg.i 1.1 01/03/98 */
/* f-oeetg.i 1.2 01/10/95 */
/*h*****************************************************************************

  INCLUDE      : f-oeetg.i
  DESCRIPTION  : OE form for Order Force Print
  USED ONCE?   : yes (oeetg.p)
  AUTHOR       : rhl
  DATE WRITTEN : 08/12/89
  CHANGES MADE :
    01/10/95 dww; TB# 17221 Position of suffix is wrong.
    04/10/03 lcb; TB# t14244 8Digit Ord No Project
    05/06/04 rgm; TB# e19893 Allow demand print to use EDI conditionally
*******************************************************************************/

form
    g-orderno                      colon 27
      {f-help.i}
    "-"                               at 37
    g-ordersuf                        at 38 no-label
      {f-help.i}
    skip(1)
    s-ackprintfl                   colon 27 label "Acknowledgement"
    s-printernmack                 colon 41 label "Printer"
       validate(if input s-ackprintfl = yes and s-printernmack = "" then false
                 else if s-printernmack = "e-mail" then true
                else {v-sasp.i s-printernmack},
                      {valmsg.gme 4035})
      {f-help.i}
    s-pickprintfl                  colon 27 label "Pick Ticket"
    s-printernmpck                 colon 41 label "Printer"
        validate(if input s-pickprintfl = yes and s-printernmpck = "" then false
                 else if s-printernmpck = "*whse*" then true
                 else {v-sasp.i s-printernmpck},
                       {valmsg.gme 4035})
      {f-help.i}
   s-invprintfl                   colon 27 label "Invoice"
   s-printernminv                 colon 41 label "Printer"
       validate(if input s-invprintfl = yes and s-printernminv = "" then false
                else if s-printernminv = "e-mail" then true
                else {v-sasp.i s-printernminv},
                      {valmsg.gme 4035})
      {f-help.i}
   s-rcptprintfl                  colon 27 label "Receipt"
   s-printernmrcpt                colon 41 label "Printer"
       validate (if not input s-rcptprintfl then true
                 else {v-sasp.i s-printernmrcpt "/*"},
                       {valmsg.gme 4035})
      {f-help.i}
   skip(1)
   s-promomsg                     colon 27 label "Promotional Message"
   "^"                               at 68
   s-autoprintfl                  colon 27 label "Automatic Print"
   s-whereappfl                   colon 27 label "Use Where Appropriate"
  with frame f-oeetg row 5 width 80 side-labels overlay
        title " Document Print ".
