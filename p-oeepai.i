/*se5.5*/
/*h****************************************************************************
  INCLUDE      : p-oeepai.i
  DESCRIPTION  : Main processing logic used by both oeepie and oeepae to
                 produce outbound edi flat files for the 810 and 843 documents.
  USED ONCE?   : no
  AUTHOR       : jlc
  DATE WRITTEN : 04/24/93
  CHANGES MADE :
    04/26/93 jlc; TB# 11078 Add user fields.
    04/27/93 jlc: TB# 11105 ? in invoice date.
    05/18/93 jlc; TB# 11393/11394 Use the edi terms basis codes from sasta
        and sals.
    05/20/93 jlc; TB# 11496 Increase dunning field size.
    05/28/93 jlc; TB# 11622 Add dates - cancel, ship, request.
    09/07/93 rs ; TB# 12932 Trend Doc Mgr for Rel 5.2
    09/23/93 jlc; TB# 13116 Check module purchased flag.
    10/09/93 jlc; TB# 13107 Add order notes.
    10/10/93 jlc; TB# 13285 Add po issue date.
    10/10/93 jlc; TB# 13283 Add enter date.
    10/28/93 jlc; TB# 13517 Add terms discount amount.
    12/11/93 jlc; TB# 13635 Add Trend Ship To.
    12/11/93 jlc; TB# 13636 Add Customer number.
    12/11/93 jlc; TB# 13640 Add Package ID.
    02/16/94 jlc; TB# 14778 Always display bill-to record.
    02/16/94 jlc; TB# 14779 Always plug customer record with customer (arsc)
        data.
    02/21/94 dww; TB# 13890 File name not specified causing error
    02/23/94 jlc; TB# 15205 Add edi 855 capability.
    03/19/94 jlc; TB# 15164 No sastn available error.
    05/03/95 jlc; TB# 18404 User hook error in &user_vaiables.
    08/24/95 jkp; TB# 18444 Add inside salesrep and phone number to customer
        record.  Also separated assign statements by flat file record.
    08/25/95 jkp; TB# 18254 Always output discduedt and netduedt regardless
        of the sasta.disctype and sasta.duetype, but these fields will be used
        to calculate the dates for some types.
    10/02/95 kjb; TB# 19391 The 'total# lines' is not being printed on the
        'totals' record when there are no lines on the OE order.
    03/06/96 jkp; TB# 20659 The Line Count and Total Quantity Ordered are
        not being zeroed out for the 'total' record.  Corrected.
    03/28/96 jlc; TB# 20843 Add Trend order type
    06/25/96 jkp; TB# 21222 Change check for module purchased to use
        modcheck.gpr include, so message is displayed.
    09/13/96 dww; TB# 21702 Trend 8.0 Enhancement - remove oper2
    03/27/98 jlc; TB# 21395 Add placedby and ship instructions to inv record.
    05/29/98 kjb; TB# 24971 Add the whse to the EDI 810 out file
    12/23/98 cm;  TB# 14835 Allow notes specific by suffix
    03/25/00 jlc; TB# 4666 Output Order Disposition
    04/12/00 ajw; TB# e4931 Buildnet Enable code reuse.
    08/08/02 des; TB# e11863 Specify where notes print by page
******************************************************************************/
{nxtrend.gpp}

/*tb 18254 08/28/95 jkp; Added shared buffer in order to use sasta.gfi */
def new shared buffer b-sasta for sasta.

/*d user hook at top of processing */
{p-oeepai.z99 &top_main = "*"}

/*tb 12932 09/07/93 rs; Trend Doc Mgr for Rel 5.2 */
find sassr where recid(sassr) = v-sassrid no-lock no-error.
if avail sassr then v-coldfl = sassr.coldfl.

/*tb 13116 09/24/93 jlc; Check module purchased flag */
/*tb 21222 06/25/96 jkp; Change to modcheck.gpr include so standard message
     is displayed. Do not need the sasa (&sasacom) read because p-rptbeg.i
     is being used and it already reads sasa.  Do not want pause (&com). */
{modcheck.gpr
    &module  = "ed"
    &sasacom = "/*"
    &com     = "/*"}

/*o Read through each SAPBO record that should be "EDI"d */
for each sapbo no-lock use-index k-outputty where
    sapbo.cono     = g-cono         and
    sapbo.reportnm = sapb.reportnm  and
    sapbo.outputty = "e",
first oeeh no-lock use-index k-oeeh where
    oeeh.cono      = g-cono         and
    oeeh.orderno   = sapbo.orderno  and
    oeeh.ordersuf  = sapbo.ordersuf and
    (g-ourproc ne "oeepi" or oeeh.stagecd ge 4),
first arsc use-index k-arsc where
    arsc.cono      = g-cono         and
    arsc.custno    = oeeh.custno
no-lock:

    {p-oeepai.z99 &top_for_each = "*"}
    
    /*All processing for preparing variables*/
    {p-oeepai.lpr}

    if v-OK then do:

        /*d Print the Header Record */
        /*tb 11078 04/26/93 jlc; Add user fields */
        /*tb 13283 10/10/93 jlc; Add enter date */
        /*tb 13285 10/10/93 jlc; Add po issue date */
        /*tb 13640 12/11/93 jlc; Add package id */
        if v-prt810fl then
            put stream edi810 unformatted
                {p-oeeaia.i}.

        if v-prt843fl then
            put stream edi843 unformatted
                {p-oeeaia.i}.

        if v-prt855fl then
            put stream edi855 unformatted
                {p-oeeaia.i}.

        /*u oeepie99.p ("ha"); After standard header data is written.  Custom
                              fields may be added to the record here */
        if v-oeepie99fl then run oeepie99.p("ha").

        /*d  use the skip to finish this record */

        if v-prt810fl then
            put stream edi810 unformatted skip.
        if v-prt843fl then
            put stream edi843 unformatted skip.
        if v-prt855fl then
            put stream edi855 unformatted skip.


        /*d Print the order notes  */
        /*tb 14835 12/23/98 cm; Allow notes specific by suffix */
        /*d First loop will print specific notes, second will print global
            notes. */
        if oeeh.notesfl ne "" then do j = 1 to 2:
            for each notes where
                 notes.cono         = g-cono    and
                 notes.notestype    = "o"       and
                 notes.primarykey   = string(oeeh.orderno) and
                 notes.secondarykey = (if j = 1 then string(oeeh.ordersuf)
                                       else "")
            no-lock:
                if notes.printfl = true or
                    (((v-prt843fl = true or v-prt855fl = true) and
                      notes.printfl2 = yes) or
                     (v-prt810fl and notes.printfl5 = yes))
                then do i = 1 to 16:
                    /*tb 13890 02/21/94 dww; File name not specified
                         causing error */
                    if notes.noteln[i] ne "" then do:

                    /*u oeepie99.p("nb"); Before Notes EDI print */
                    if v-oeepie99fl then
                        run oeepie99.p("nb").

                        s-note = string(notes.noteln[i],"x(60)").

                        if v-prt810fl then
                            put stream edi810 unformatted
                                {p-oeeaib.i}.

                        if v-prt843fl then
                            put stream edi843 unformatted
                                {p-oeeaib.i}.

                        if v-prt855fl then
                            put stream edi855 unformatted
                                {p-oeeaib.i}.

                    /*u oeepie99.p("na"); Before Notes EDI print */
                    if v-oeepie99fl then
                        run oeepie99.p("na").

                        if v-prt810fl then
                            put stream edi810 unformatted skip.
                        if v-prt843fl then
                            put stream edi843 unformatted skip.
                        if v-prt855fl then
                            put stream edi855 unformatted skip.

                    end.
                end.
            end.
        end.

        /*d Print the Customer Record */

        /*u oeepie99.p("cb"); Before Customer EDI print */
        if v-oeepie99fl then
            run oeepie99.p("cb").

        /*tb 11078 04/26/93 jlc; Add user fields */
        /*tb 13636 12/11/93 jlc; Add customer number */
        if s-custnm <> "" then do:
            if v-prt810fl then
                put stream edi810 unformatted
                    {p-oeeaic.i}.

            if v-prt843fl then
                put stream edi843 unformatted
                    {p-oeeaic.i}.

            if v-prt855fl then
                put stream edi855 unformatted
                    {p-oeeaic.i}.

            /*u oeepie99.p ("ca"); After standard customer data is written.
                              Custom fields may be added to the record here */
            if v-oeepie99fl then run oeepie99.p("ca").

            /*d use the skip to finish this record */

            if v-prt810fl then
                put stream edi810 unformatted skip.
            if v-prt843fl then
                put stream edi843 unformatted skip.
            if v-prt855fl then
                put stream edi855 unformatted skip.

        end. /* s-custnm <> "" */

        /*d Print the Ship To Record */

        /*u oeepie99.p("sb"); Before Ship To EDI print */
        if v-oeepie99fl then
            run oeepie99.p("sb").

        /*tb 11078 04/26/93 jlc; Add user fields */
        /*tb 13635 12/11/93 jlc; Add ship to */
        if s-shiptonm <> "" then do:
            if v-prt810fl then
                put stream edi810 unformatted
                    {p-oeeaid.i}.

            if v-prt843fl then
                put stream edi843 unformatted
                    {p-oeeaid.i}.

            if v-prt855fl then
                put stream edi855 unformatted
                    {p-oeeaid.i}.

        /*u oeepie99.p ("sa"); After standard Ship To data is written.  Custom
                              fields may be added to the record here */
        if v-oeepie99fl then run oeepie99.p("sa").

        /*d use the skip to finish this record */

        if v-prt810fl then
            put stream edi810 unformatted skip.
        if v-prt843fl then
            put stream edi843 unformatted skip.
        if v-prt855fl then
            put stream edi855 unformatted skip.

        end. /* s-shiptonm <> "" */

        /*d Print the Bill To Record */

        /*u oeepie99.p("bb"); Before Bill To EDI print */
        if v-oeepie99fl then
            run oeepie99.p("bb").

        /*tb 11078 04/26/93 jlc; Add user fields */
        if s-invname <> "" then do:
            if v-prt810fl then
                put stream edi810 unformatted
                    {p-oeeaie.i}.

            if v-prt843fl then
                put stream edi843 unformatted
                    {p-oeeaie.i}.

            if v-prt855fl then
                put stream edi855 unformatted
                    {p-oeeaie.i}.

        /*u oeepie99.p ("ba"); After standard Bill To data is written.  Custom
                              fields may be added to the record here */
        if v-oeepie99fl then run oeepie99.p("ba").

        /*d use the skip to finish this record */

        if v-prt810fl then
            put stream edi810 unformatted skip.
        if v-prt843fl then
            put stream edi843 unformatted skip.
        if v-prt855fl then
            put stream edi855 unformatted skip.

        end.  /* s-invname <> "" */

        /*d Print the Remit Record */

        /*u oeepie99.p("rb"); Before Remit EDI print */
        if v-oeepie99fl then
            run oeepie99.p("rb").

        if s-remitnm <> "" then do:
            if v-prt810fl then
                put stream edi810 unformatted
                    {p-oeeaif.i}.

            if v-prt843fl then
                put stream edi843 unformatted
                    {p-oeeaif.i}.

            if v-prt855fl then
                put stream edi855 unformatted
                    {p-oeeaif.i}.

        /*u oeepie99.p ("ra"); After standard Remit data is written.  Custom
                              fields may be added to the record here */
        if v-oeepie99fl then run oeepie99.p("ra").

        /*d use the skip to finish this record */

        if v-prt810fl then
            put stream edi810 unformatted skip.
        if v-prt843fl then
            put stream edi843 unformatted skip.
        if v-prt855fl then
            put stream edi855 unformatted skip.

        end. /* s-remitnm <> "" */

        /*d Print the Terms Record */

        /*u oeepie99.p("tb"); Before Terms EDI print */
        if v-oeepie99fl then
            run oeepie99.p("tb").

        if v-prt810fl then
            put stream edi810 unformatted
                {p-oeeaig.i}.

        if v-prt843fl then
            put stream edi843 unformatted
                {p-oeeaig.i}.

        if v-prt855fl then
            put stream edi855 unformatted
                {p-oeeaig.i}.

        /*u oeepie99.p ("ta"); After standard terms data is written.  Custom
                              fields may be added to the record here */
        if v-oeepie99fl then run oeepie99.p("ta").

        /*d Use the skip to end this record */

        if v-prt810fl then
            put stream edi810 unformatted skip.
        if v-prt843fl then
            put stream edi843 unformatted skip.
        if v-prt855fl then
            put stream edi855 unformatted skip.



        /*d Print the Addon Record(s) */

       /*u oeepie99.p("ab"); Before Addon(s) EDI print */
        if v-oeepie99fl then
            run oeepie99.p("ab").

        for each addon use-index k-seqno where
                 addon.cono      = g-cono        and
                 addon.ordertype = "oe"          and
                 addon.orderno   = oeeh.orderno  and
                 addon.ordersuf  = oeeh.ordersuf
                 no-lock:
                 {p-oeadfp.i}

          assign
            s-addonamt    = s-addonamt +
                           (if addon.addonnet <> 0 then
                              if oeeh.transtype = "rm" then
                                string((addon.addonnet * -1),"-99999999.99")
                              else
                                 string(addon.addonnet,"-99999999.99")
                            else string(0,"-99999999.99"))
            s-addondesc   = string(s-addondesc,"x(36)").

            if addon.addonnet <> 0 then do:
              {w-sastn.i "a" addon.addonno no-lock}
              
              /*tb 15164 jlc 03/19/94; No sastn available error */
              assign
                s-addsvccd = if avail sastn and sastn.zipcd ne "" then
                               string(substr(sastn.zipcd,1,5),"x(5)")
                             else
                               v-5spaces
                s-addchgcd = if avail sastn and sastn.zipcd ne "" then
                               string(substr(sastn.zipcd,6,5),"x(5)")
                             else
                               v-5spaces.
            end. /* if addon.addonnet */
            if dec(s-addonamt) <> 0 then
              do:
              s-addallchg     =   if dec(s-addonamt) < 0 then "a"
                                  else "c".
              if v-prt810fl then
                put stream edi810 unformatted
                    {p-oeeaih.i}.
                        
              if v-prt843fl then
                put stream edi843 unformatted
                    {p-oeeaih.i}.
                        
              if v-prt855fl then
                put stream edi855 unformatted
                    {p-oeeaih.i}.
                        
              /*u oeepie99.p ("aa"); After standard header data is written.
                            Custom fields may be added to the record here */
              if v-oeepie99fl then run oeepie99.p("aa").

              /*d use the skip to end this record */
              if v-prt810fl then
                put stream edi810 unformatted skip.
              if v-prt843fl then
                put stream edi843 unformatted skip.
              if v-prt855fl then
                put stream edi855 unformatted skip.

            end.  /* if s-addonamt > 0 */
            assign s-addonamt = "".  /*das - sx5.5*/
        end. /* each addon */

        /*o Calculate and print the 810 EDI Line Item Data */
        run oeepiel.p.

        /*tb 19391 10/02/95 kjb; No line total is being printed if no lines
            exist */

        s-linecnt = if int(s-linecnt) = 0 then "000000"
                    else s-linecnt.

        /*d Print the Totals Record */

        /*u oeepie99.p("ob"); Before Totals EDI print */
        if v-oeepie99fl then
            run oeepie99.p("ob").

        if v-prt810fl then
            put stream edi810 unformatted
                {p-oeeaii.i}.

        if v-prt843fl then
            put stream edi843 unformatted
                {p-oeeaii.i}.

        if v-prt855fl then
            put stream edi855 unformatted
                {p-oeeaii.i}.

        /*u oeepie99.p ("oa"); After standard header data is written.  Custom
                              fields may be added to the record here */
        if v-oeepie99fl then run oeepie99.p("oa").

        /*d use the skip to end this record */
        if v-prt810fl then
            put stream edi810 unformatted skip.
        if v-prt843fl then
            put stream edi843 unformatted skip.
        if v-prt855fl then
            put stream edi855 unformatted skip.

        {p-oeepai.z99 &bottom_v_ok = "*"}
    end.  /* end v-OK */


    /*d If a hardcopy of the document is requested, reset the SAPBO record
        to print on the printer */
    if (sasc.oifaxhardfl[3] and g-ourproc <> "oeepa") or
       (sasc.oifaxhardfl[2] and g-ourproc = "oeepa") or
        not(v-OK) then do for b-sapbo transaction:

        find b-sapbo where recid(b-sapbo) = v-sapboid exclusive-lock no-error.

        if not avail b-sapbo then next.
        assign
            b-sapbo.reprintfl = if v-OK then yes else b-sapbo.reprintfl
            b-sapbo.outputty  = "P".
    end.

    /*tb 12932 09/07/93 rs; Trend Doc Mgr for Rel 5.2 */
    else  if v-coldfl then do for b-sapbo transaction:
        find b-sapbo where recid(b-sapbo) = v-sapboid exclusive-lock no-error.

        if avail b-sapbo then
           b-sapbo.outputty  = "x".
    end.
end.

/* user hook at bottom of p-oeepai */
{p-oeepai.z99 &bottom_main = "*"}