/******************************************************************************
  PROGRAM      : vasza.p
  DESCRIPTION  : Global Folder / Maintenance Browse Processor 
  USED ONCE?   : Yes
  AUTHOR       : SunSource
  DATE WRITTEN : 07/07/10
  CHANGES MADE :
*******************************************************************************/

define var                   ix-puType as char format "x(24)" no-undo.      
define var                   ix-Whse   as char format "x(4)"  no-undo.      
define var                   ix-prod  like icsp.prod          no-undo.      
define var                   ix-desc  like icsp.prod          no-undo.      
define var                   ixo-desc like icsp.prod          no-undo.      
define var                   ix-recid as recid                no-undo.      
define var                   ix-keylabel as char              no-undo.      
define var                   ix-keyno    as dec               no-undo.      



                                                                            
def stream debug.
output stream debug to "/usr/tmp/debug.lxg" append.                                                                            

{g-all.i} 
{g-ic.i}
{fabref.i }

define buffer b-zsdirefty for zsdirefty. 

def var s-netavail like icsw.qtyonhand                no-undo.
def var s-recid    as   recid                         no-undo.

def new shared var errfl as logical                   no-undo.
def new shared var  sa-rowid as recid extent 5        no-undo.
def var w-color     as int dcolor 3                   no-undo.
def var v-conv      like icsw.unitstnd                no-undo.
def var fhit        as logical init no                no-undo.
def var r           as logical init no                no-undo.
def var onetime     as logical init no                no-undo.
def var h-lvl       as int  format "9"                no-undo.
def var endsw       as logical init no                no-undo.
def var v-returning as logical init no                no-undo.
def var v-exitbrowse as logical init no                no-undo.

def var scrn-lns    as integer                        no-undo.
def var whsefl      as logical                        no-undo.
def var q-rowid     as rowid                          no-undo.
def var m-recid     as recid                          no-undo.
def var prev-rowid  as rowid                          no-undo.
def var lv-popup    as logical init no                no-undo.
def var v-status    as logical init no                no-undo.
def var v-notdone   as logical init yes               no-undo.
def var v-tierdn    as logical init no                no-undo.
def var x           as integer                        no-undo.
def var brwse-nbr   as int                            no-undo.
def var b-nbr   as dec                                no-undo.
def var first-rowid as rowid                          no-undo.
def var p-rowid     as int                            no-undo.
def var dd          as character                      no-undo.
def var len         as integer                        no-undo.
def var xinx        as integer                        no-undo.
def var winx        as integer                        no-undo.
def var nonxt       as logical                        no-undo.
def var cnt         as integer                        no-undo.
def var zy-cnt      as integer                        no-undo.
def var v-delim     as c format "x(05)" init "~011"   no-undo.
def var v-spc       as c format "x(05)"               no-undo.
def new shared var  b-altdesc as c format "x(30)"     no-undo.    
def var h-fullprod  as c format "x(60)"               no-undo.
def var h-foldnm    as c format "x(60)"               no-undo.
def var h-foldxrec  as c format "x(60)"               no-undo.
def var h-prod      as c format "x(24)"               no-undo.
def var h-b-prod    as c format "x(24)"               no-undo.
def var h-b-crossref as c format "x(24)"              no-undo.
def var h-b-parentkey as dec                          no-undo.
def var h-ttype     as c format "x(24)"               no-undo.
def var br-prod     as c format "x(60)"               no-undo.
def var sr-prod     as c format "x(24)"               no-undo.
def var s-prod      like icsp.prod                    no-undo.
def var v-descrip   as char format "x(24)"            no-undo.
def var hr-prod     as c format "x(60)"               no-undo.
def var h-name      as c format "x(10)"               no-undo.
def var s-key       as c format "x(60)"               no-undo.
def var s-words     as c format "x(24)" extent 5      no-undo.
def var s-puType    as c format "x(24)"               no-undo.
def var s-whse      as c format "x(04)" extent 12 dcolor 3 no-undo.
def var h-avail     as c format "x(07)" extent 12 dcolor 3 no-undo.


def var l-whse      as c format "x(04)" no-undo extent 12 initial  
   ["datl","decp","dros","dmcp","chry","ddal",
    "delp","dxcp","dhou","dndn","fmec","fgr "].
def var s-avail     as c format "x(07)" extent 12 dcolor 3 no-undo.
def var l-vendno    as c format "x(12)" dcolor 3      no-undo.
def var l-vendnm    like apsv.name      dcolor 3      no-undo.
def var pv-errormess      as c format "x(72)"         no-undo.
def var v-error           as c format "x(72)"         no-undo.
def var disp-avail  as c format "x(3)"                no-undo.
def var disp-oo     as c format "x(3)"                no-undo.
 
def buffer b-saindex for saindex.
def buffer cc-icsp   for icsp.

define temp-table parts no-undo 
    field ttype     as c    format "x(01)"
    field tier      as c    format "x"
    field foldnm    as c    format "x(22)"
    field foldnmno  as decimal
    field xrecid    as recid 
    field xkeyno    as decimal
    field prod      as c    format "x(30)"
    field sprod     as c    format "x(30)"
    field fullprod  as c    format "x(120)"
    field cref      as c    format "x(22)" 
    field typfl     as c    format "x(02)"
    field clrfl     as c    format "x(01)"
    field sname     as c    format "x(60)"
    field zdescrip  as c    format "x(30)"
    field altdesc   as c    format "x(30)"
    field zavail    as c    format "x(08)" dcolor 3
    
    index k-parts
          sname 
          tier
          typfl  
          prod
    index k-prod
          fullprod  
          sprod
          sname 
          tier.

define query  q-parts for parts scrolling.
define browse b-parts query q-parts 
      display parts.prod
              parts.zdescrip
              parts.zavail  
      with 9 down centered overlay no-hide no-labels no-box.

def buffer zz-parts  for parts.
def buffer zy-parts  for parts.
def buffer zyp-parts  for parts.


def new shared temp-table t-whseinfo no-undo
  field whse   like icsw.whse
  field widx   as int 
  field zavail like icsw.qtyunavail
  field zoo    like icsw.qtyonorder
 index fkx
   widx   descending
   zavail descending
   whse ascending.



form 
"    Product                        Description                    Avail   "     at 1 
"    ------------------------------ ------------------------------ --------"    at 1 
    b-parts at row 3 col 3  
   with frame f-parts width 80 row 9 centered overlay 
        no-hide  title "Fabrication Description Lookup " side-labels.   
form 
 v-descrip at 2
 {f-help.i}
with frame f-descrip no-labels overlay row 8 no-box.  


assign b-parts:selectable = true. 

form 
 "Searching For: "  sr-prod 
  with frame f-lookprod width 45 row 7 centered
  overlay title "Folder Product Search" no-labels.   

form 
 "Add Product:" at 1 g-prod  
 "Confirm Add?" at 1 confirm
 with frame f-newprod width 42 row 7 centered
 overlay title " Additional Product Entry " no-labels.   

form 
 "ICSP Product :" at 1 cc-icsp.prod  
 "Labor Minutes:" at 1 cc-icsp.user7
  with frame f-laborupdt width 42 row 7 centered
  overlay title " Update Labor Minutes " no-labels.   

form 
 "ICSP Product   :" at 1 zsdirefer.prod  
 "Alt/Description:" at 1 b-altdesc            
  with frame f-altdesc width 50 row 7 centered
  overlay title " Update Alt/Description " no-labels.   

/*

define shared frame f-whses
   "Ctrl-W to Select Whse" at 3 dcolor 2
   "Vendor:"     at 24
   l-vendno      at 31
   "Name:"       at 44
   l-vendnm      at 49
"  Whse Avl/OnO Whse Avl/OnO Whse Avl/OnO Whse Avl/OnO Whse Avl/OnO Whse Avl/On~O"
    at 1  
   " " at 1
   s-whse[1]  h-avail[1]  s-whse[2]  h-avail[2] 
   s-whse[3]  h-avail[3]  s-whse[4]  h-avail[4] 
   s-whse[5]  h-avail[5]  s-whse[6]  h-avail[6] 
   " " at 1
   s-whse[7]  h-avail[7]  s-whse[8]  h-avail[8] 
   s-whse[9]  h-avail[9]  s-whse[10] h-avail[10] 
   s-whse[11] h-avail[11] s-whse[12] h-avail[12] 
 
   with width 80 row 17 centered no-box overlay no-labels.

*/


form 
"--------------------Product Availablity by Warehouse---------------------------"  
   "  Vendor No: " l-vendno " Name: " l-vendnm
   "  Alternate Description: " at 1 b-altdesc
/*
"  Whse  Qtyavl Whse  Qtyavl Whse  Qtyavl Whse  Qtyavl Whse  Qtyavl Whse  Qtyavl"    at 1  
   " " at 1
*/
"  Whse Avl/OnO Whse Avl/OnO Whse Avl/OnO Whse Avl/OnO Whse Avl/OnO Whse Avl/OnO"
    at 1  
   " " at 1
   s-whse[1]  h-avail[1]  s-whse[2]  h-avail[2] 
   s-whse[3]  h-avail[3]  s-whse[4]  h-avail[4] 
   s-whse[5]  h-avail[5]  s-whse[6]  h-avail[6] 
   " " at 1
   s-whse[7]  h-avail[7]  s-whse[8]  h-avail[8] 
   s-whse[9]  h-avail[9]  s-whse[10] h-avail[10] 
   s-whse[11] h-avail[11] s-whse[12] h-avail[12] 
 
/*   
   s-whse[1]  s-avail[1]  s-whse[2]  s-avail[2] 
   s-whse[3]  s-avail[3]  s-whse[4]  s-avail[4] 
   s-whse[5]  s-avail[5]  s-whse[6]  s-avail[6] 
   " " at 1
   s-whse[7]  s-avail[7]  s-whse[8]  s-avail[8] 
   s-whse[9]  s-avail[9]  s-whse[10] s-avail[10] 
   s-whse[11] s-avail[11] s-whse[12] s-avail[12] 
*/ 
   with frame f-whses width 80 row 2 centered no-box
   overlay no-labels.


form /* Assign Words */
  s-puType  colon 15 label "Fab Type      "
    help "Please Enter Power Unit type"
  s-words[1]  colon 15 label "Folder Level 1"
    help "Please Enter Folder Name"
  s-words[2]  colon 15 label "Folder Level 2"
    help "Please Enter Folder Name"
  s-words[3]  colon 15 label "Folder Level 3"
    help "Please Enter Folder Name"
  s-words[4]  colon 15 label "Folder Level 4"
    help "Please Enter Folder Name"
  s-words[5]  colon 15 label "Folder Level 5"
    help "Please Enter Folder Name"
  with frame f-words row 6 col 2 side-labels title
  " Additional Folder Entry "
  overlay centered.

form
  ix-puType     at 10 label "Unit Type"
   {f-help.i}

with frame f-header width 80 row 1 side-labels title
  "Attrubute Entry and Maintenance".

DEF VAR v-mfgbrowsekeyed    AS LOGICAL                   NO-UNDO.
DEF VAR v-mfgbrowseopen    AS LOGICAL                   NO-UNDO.
DEF VAR x-cursorup        AS i FORMAT "999" INIT 501     NO-UNDO.


DEFINE NEW SHARED TEMP-TABLE models NO-UNDO
    FIELD NAME     AS CHAR FORMAT "x(39)"
    FIELD unitdt   AS DATE
    FIELD descrip  AS CHAR FORMAT "x(30)"
INDEX k-modix
      NAME.
DEFINE BUFFER bw-icsec   FOR icsec.
DEFINE BUFFER bzz-icsec  FOR icsec.
DEFINE BUFFER bzf-icsec  FOR icsec.
DEFINE VAR    v-modelsbuildfl AS LOGICAL NO-UNDO.

DEFINE QUERY q-models FOR zsdirefty SCROLLING.

DEFINE BROWSE b-models QUERY q-models
DISPLAY 
  zsdirefty.reference
  zsdirefty.transdt
WITH 14 DOWN CENTERED OVERLAY NO-HIDE NO-LABELS
TITLE
"   Fab Descriptions                      Last MaintDt    ".

DEFINE FRAME
  f-models
  b-models AT ROW 1 COL 1
WITH WIDTH 80 ROW 6 COL 15 OVERLAY NO-HIDE NO-LABELS NO-BOX.

def var s-formend       as c format "x"                   no-undo.

def buffer bp-zsdirefer for zsdirefer.
def buffer bk-zsdirefer for zsdirefer.
                                    
pv-errormess = "Invalid Input - Preceding Folder Levels Cannot Be Blank".      

/***** trigger section *****/



ON f12 OF ix-puType IN FRAME f-header DO:
  ON cursor-up cursor-up.   /**&&**/
  ON cursor-down cursor-down.
  ASSIGN v-mfgbrowsekeyed = FALSE
  v-mfgbrowseopen  = FALSE.
/*  IF ix-puType <> " " THEN DO: */
      RUN DoMfgLu (INPUT ix-puType,
                   INPUT "normal").
/*   END.                        */
  IF v-mfgbrowseopen = FALSE THEN DO:
    ON cursor-up back-tab.
    ON cursor-down tab.
  END.
END.
  
ON any-key OF b-models DO:
  IF {k-cancel.i} OR {k-jump.i} THEN DO:
    HIDE FRAME f-models.
    HIDE FRAME f-search.
    DISPLAY ix-puType WITH FRAME f-header.
    APPLY "entry" TO ix-puType IN FRAME f-header.
    APPLY "window-close" TO CURRENT-WINDOW.
    ON cursor-up back-tab.
    ON cursor-down tab.
  END.
  IF ({k-return.i} OR {k-accept.i}) AND avail zsdirefty THEN DO:
    ASSIGN ix-puType = zsdirefty.reference.
    DISPLAY ix-puType WITH FRAME f-header.
    HIDE FRAME f-models.
    APPLY "entry" TO ix-puType IN FRAME f-header.
    APPLY "window-close" TO CURRENT-WINDOW.
    ON cursor-up back-tab.
    ON cursor-down tab.
  END.
END.
  
ON LEAVE OF ix-puType IN FRAME f-header DO:
  CLOSE QUERY q-models.
  ASSIGN v-mfgbrowseopen = FALSE.
  HIDE FRAME f-models.
  ON cursor-up back-tab.    /**&&**/
  ON cursor-down tab.
END.

on value-changed of b-parts do: 
 
  STATUS DEFAULT
    "Space Bar opens folder (+) or closes folder (-), F4 to end".
 
  if b-parts:new-row in frame f-parts then 
  leave. 
 assign s-whse   = "" 
        s-avail  = ""
        l-vendno = ""
        l-vendnm = "".
 if not can-do("+,-",parts.ttype) then do: 
  find first zsdirefer where 
       recid(zsdirefer) = parts.xrecid
       no-lock no-error.
  if avail zsdirefer then do: 
   assign  b-altdesc = zsdirefer.altdesc.
   display b-altdesc with frame f-whses.
  end.
  run fillin-whses.
 end.
 else 
 hide frame f-whses.
 /*
 display s-whse[1]  s-whse[2]  s-whse[3]  s-whse[4]  s-whse[5]  s-whse[6]
         s-avail[1] s-avail[2] s-avail[3] s-avail[4] s-avail[5] s-avail[6]
         s-whse[7]  s-whse[8]  s-whse[9]  s-whse[10] s-whse[11]  s-whse[12]
         s-avail[7] s-avail[8] s-avail[9] s-avail[10] s-avail[11] s-avail[12]
         l-vendno l-vendnm 
         with frame f-whses.
 */
end.

on F6 of b-parts do: 
 assign v-tierdn = yes
        h-lvl  = if parts.tier = "1" and parts.ttype = "+" and 
                    parts.prod = "+"  then 
                  1
                 else  
                  int(string(parts.tier,"9")) + 1.
 if g-secure <= 3 then 
  leave.
 do xinx = 1 to 5: 
  s-words[xinx] = "".
 end.   
 assign s-key = ""
        v-returning = true.
 do xinx = 1 to (h-lvl - 1): 
  if xinx > 1 then 
     s-key = s-key + v-delim.
  assign 
    s-words[xinx] =
     (if can-do("",parts.ttype) then 
         string(xinx,"9") + "-" + entry(xinx,parts.fullprod,v-delim)
      else 
         entry(xinx,parts.fullprod,v-delim))
    s-key         = s-key + entry(xinx,parts.fullprod,v-delim).
 end.
 do xinx = (h-lvl - 1) to 5:
    assign s-key = s-key + v-delim.
 end.
 if parts.prod     = "<F7 Deletes Unit>"  then
   s-words[1] = "".

 if can-do("+",parts.ttype) then do: 
  if s-puType   ne "" then next-prompt s-words[1] with frame f-words.
  if s-words[1] ne "" then next-prompt s-words[2] with frame f-words.
  if s-words[2] ne "" then next-prompt s-words[3] with frame f-words.
  if s-words[3] ne "" then next-prompt s-words[4] with frame f-words.
  if s-words[4] ne "" then next-prompt s-words[5] with frame f-words.
 end.

 assign confirm = yes.
/*** only creating product recs on a + or null parts.ttype ***/
 if can-do("",parts.ttype)  or  
    can-do("-",parts.ttype) then 
  updt1:
  do while true on endkey undo, leave: 
   update g-prod  
          confirm auto-return
          go-on(f1) 
          with frame f-newprod.
   errfl = no.
  if confirm = yes and input g-prod ne "" then do:  
   assign sr-prod = input g-prod. 
   if sr-prod ne "" then do:  
    run vaexf-refer.p (input s-puType,
                       input entry(1,s-key,v-delim),
                       input entry(2,s-key,v-delim),
                       input entry(3,s-key,v-delim),
                       input entry(4,s-key,v-delim),
                       input entry(5,s-key,v-delim),            
                       input sr-prod,
                       input "ADD",
                       input "VA",
                       input-output s-recid,
                       input-output ix-keyno).
    run reset-brws("down").
   /*** building prior records to cover prev screen back on a + type level ***/
    run repaint-brws("up"). /* just load records don't reset */
    if errfl = no then do:
     hide frame f-newprod.
     leave updt1.
    end.
    else do: 
     pause 4. 
    end.
   end. /* sr-prod ne nulls  */  
  end. /* do while true */ 
  hide frame f-newprod.
  end. /* can-do "" - */
  else 
  if can-do("+",parts.ttype) then do: 
    self:refreshable = no.
    display s-putype   
            s-words[1] 
            s-words[2] 
            s-words[3] 
            s-words[4] 
            s-words[5] 
            with frame f-words.
   updt2:
   do while true on endkey undo, leave: 
    update s-putype   when s-puType   = ""
           s-words[1] when s-words[2] = ""
           s-words[2] when s-words[3] = ""
           s-words[3] when s-words[4] = ""
           s-words[4] when s-words[5] = ""
           s-words[5] 
           go-on(f1)  with frame f-words
    editing:
     readkey.
     if {k-cancel.i} then do: 
      leave.    
      hide frame f-words. 
     end.
     apply lastkey.
    end. /* editing loop */ 
    if s-puType   ne "" then next-prompt s-words[1] with frame f-words.
    if s-words[1] ne "" then next-prompt s-words[2] with frame f-words.
    if s-words[2] ne "" then next-prompt s-words[3] with frame f-words.
    if s-words[3] ne "" then next-prompt s-words[4] with frame f-words.
    if s-words[4] ne "" then next-prompt s-words[5] with frame f-words.
    if {k-cancel.i} then
     leave.
    if s-words[1] = "" and
      (s-words[2] = "" or s-words[3] = "" or 
       s-words[4] = "" or s-words[5] = "") then do: 
     run zsdierrx.p(pv-errormess,"yes"). 
     next.
    end. 
    else 
         
    if s-words[1] = "" and
      (s-words[2] ne "" or s-words[3] ne "" or 
       s-words[4] ne "" or s-words[5] ne "") then do: 
     run zsdierrx.p(pv-errormess,"yes"). 
     next.
    end. 
    else 
    if s-words[2] ne "" and
      (s-words[1] = "") then do: 
     run zsdierrx.p(pv-errormess,"yes"). 
     next.
    end.     
    else 
    if s-words[3] ne "" and
      (s-words[1] = "" or s-words[2] = "") then do: 
     run zsdierrx.p(pv-errormess,"yes"). 
     next.
    end.     
    else 
    if s-words[4] ne "" and
      (s-words[1] = "" or s-words[2] = "" or s-words[3] = "") then do: 
     run zsdierrx.p(pv-errormess,"yes"). 
     next.
    end.     
    else 
    if s-words[5] ne "" and
      (s-words[1] = "" or s-words[2] = "" or s-words[3] = "" or 
       s-words[4] = "") then do: 
     run zsdierrx.p(pv-errormess,"yes"). 
     next.
    end.     
    assign sr-prod = ""
           sa-rowid[1]  = ?
           sa-rowid[2]  = ?
           sa-rowid[3]  = ?
           sa-rowid[4]  = ?
           sa-rowid[5]  = ?.
    run vaexf-refer.p (input s-puType,
                       input s-words[1],
                       input s-words[2],
                       input s-words[3],
                       input s-words[4],
                       input s-words[5],
                       input sr-prod,
                       input "Add",
                       input "VA",
                       input-output s-recid,
                       input-output ix-keyno).
    run add-create-brws.
    reposition q-parts to rowid(q-rowid).
    get current q-parts.
    self:refreshable = true.
    leave updt2.
  end. /** do while true **/
  hide frame f-words.
 end. /* can-do- +,-  */
 hide frame f-newprod.
 hide frame f-words.
 on cursor-up cursor-up.
 on cursor-down cursor-down.
end.

on row-display of b-parts do: 
 if parts.clrfl = "c" then do: 
  assign parts.prod:dcolor in browse b-parts = 3.
 end.
end.     

on any-key of b-parts do:

  STATUS DEFAULT
    "Space Bar opens folder (+) or closes folder (-), F4 to end".
  
 assign v-returning = false.
 self:select-row (int(self:focused-row)).  
 assign v-notdone = yes
        b-nbr     = self:focused-row
        h-lvl     = int(string(parts.tier,"9")) + 1.
 assign q-rowid = rowid(parts).       

 if (not can-do("1,4,13,32,501,502,503,504,507,508",string(lastkey)) and            keyfunction(lastkey) ne "go" and                                  
     keyfunction(lastkey) ne "end-error") and 
     not {k-func7.i}  then do:                     
  apply lastkey.                                                     
  assign ix-prod    = ix-prod
         ix-desc    = ixo-desc 
         v-notdone  = false
        v-returning = true.
  apply "window-close" to current-window.
  return.
 end.    
 /*
 if {k-func7.i} and can-do("",parts.ttype) then do:
  find zsdirefer where
       recid(zsdirefer) = parts.xrecid
       no-error.
  if avail zsdirefer then do: 
   assign  b-altdesc = zsdirefer.altdesc.
   display zsdirefer.prod b-altdesc with frame f-altdesc.
   update b-altdesc
      with frame f-altdesc.
   assign zsdirefer.altdesc = input b-altdesc
          b-altdesc         = input b-altdesc.
    display b-altdesc with frame f-whses.
  end.
  hide frame f-altdesc.
 end.
 else 
*/
 if {k-func7.i} and g-secure > 4 then do: 
  assign v-returning = true.
  self:refreshable = false.
  assign m-recid = recid(parts)
         q-rowid = rowid(parts).
  {p-delete.i}
  /* if confirm then do */
  do: 


   if parts.prod     = "<F7 Deletes Unit>"   then do:

     find zsdirefty where
          zsdirefty.cono      = g-cono and
          zsdirefty.rectype   = "va"    and
          zsdirefty.reference = s-puType  no-error.
     if avail zsdirefty then
       delete zsdirefty.
    APPLY "window-close" TO CURRENT-WINDOW.
    assign  v-returning = false
            v-exitbrowse = true. 
    
   end.       
      
   if can-do("",parts.ttype) and confirm then 
    run del-brwse-rec.
   else do: 
   /* saving rowid for a + or part.ttype record for repositioning */
    run del-brwse-rec.
    reposition q-parts to rowid(q-rowid).
   end.
   assign v-tierdn = yes.
   self:refreshable = true.
   self:refresh().
   leave.  
  end. /* if confirm  */
  else 
   next.
 end.   /* do k-delete.i  */     
 else 
 if {k-nextpg.i} or (({k-down.i} and b-nbr = 9) and
    (can-do("",parts.ttype) or can-do("-",parts.ttype))) then do: 
  assign v-returning = true.
  v-status = self:select-row(min(9,self:num-iterations)).
  assign h-lvl = int(string(parts.tier,"9")) + 1.
  if can-do("",parts.ttype) or can-do("-",parts.ttype) then do: 
   if v-status then do:  
    assign sr-prod = parts.sprod.  /* Same as a search    */
    run reset-brws("down").
   end.
  end.
 end. /* if down or next page */
 else
 if {k-prevpg.i} or ({k-up.i} and b-nbr = 1 and
     can-do("",parts.ttype)) then do: 
  assign v-returning = true.
  v-status = self:select-row(1). 
  assign h-lvl = int(string(parts.tier,"9")) + 1.
  if can-do("",parts.ttype) then do: 
   if v-status then do: 
    run reset-brws("up").
    v-status = self:select-row(1).
   end.
  end.
  end. /* if down or next page */
  else
  if {k-func12.i} and 
     (can-do("",parts.ttype) or can-do("-",parts.ttype)) then do: 
   assign v-returning = true.
   update sr-prod 
          with frame f-lookprod.
   if sr-prod ne "" then do: 
    run reset-brws("down").
    /*** building prior records to cover prev screen back on a
         + type level ***/
    run repaint-brws("up"). /* just load records don't reset */
    hide frame f-lookprod.
   end.  
   on cursor-up cursor-up.
   on cursor-down cursor-down.
  end.
  else 
  if ({k-accept.i} or {k-return.i}) /* and can-do("",parts.ttype) */ then do: 
   assign ix-prod   = parts.sprod
          ix-desc   = replace (parts.fullprod,"~011","+")
          ix-keyno  = parts.xkeyno
          ix-recid  = recid(parts)
        v-returning = false.
   apply "window-close" to current-window.
   return.
     
/*
   assign v-returning = true.
   assign v-notdone    = no.
   find cc-icsp where cc-icsp.cono = g-cono and 
        cc-icsp.prod = parts.sprod
        exclusive-lock no-error.
   if avail cc-icsp then 
   do:       
    display cc-icsp.prod with frame f-laborupdt.
    update cc-icsp.user7
    with frame f-laborupdt.
   end.
   hide frame f-laborupdt.
 */
  end.  
  else 
  if /* {k-accept.i} or {k-return.i} */
    lastkey = 32 or {k-cancel.i} then do: 
   assign v-returning = true.
   self:select-focused-row(). 
   self:refreshable = false.
   if {k-cancel.i} and can-do("",parts.ttype) then do: 
    find first zy-parts use-index k-parts where 
               zy-parts.tier     = string((h-lvl - 1),"9") and 
               zy-parts.fullprod = parts.fullprod  and                 
               zy-parts.ttype    = "-"    
               exclusive-lock no-error.
    self:select-focused-row().
    if avail zy-parts then 
    assign substring(zy-parts.prod,index(zy-parts.prod,"-",1),1) = "+"  
           zy-parts.ttype = "+"
           q-rowid        = rowid(zy-parts)
           v-tierdn       = yes.
    for each zy-parts use-index k-prod  where 
             zy-parts.fullprod = parts.fullprod and 
             zy-parts.typfl = string((h-lvl - 1),"9") + "9"
        exclusive-lock:
        delete zy-parts.
    end. 
    self:refreshable = true.
   end.
   else 
   if ({k-cancel.i} and can-do("+,-",parts.ttype)) then do:
    assign ix-prod    = ix-prod
           ix-desc    = ixo-desc 
           v-notdone  = no.
          v-returning = true.
    hide frame f-parts no-pause.
    hide frame f-whses no-pause.
    apply "window-close" to current-window.
    return.
    /* just returning for now untill specs are more clear here
    assign v-returning = false. 
    run clear-browse.
    hide frame f-parts no-pause.
    hide frame f-whses no-pause.
    self:refreshable = true.
    v-notdone = true.
    leave. 
    */
   end.
   else  
   if can-do("+,-",parts.ttype) and
      not ({k-nextpg.i} or {k-down.i} or
           {k-prevpg.i} or {k-up.i}) then do: 
    assign v-returning = true.
    assign q-rowid = rowid(parts)          
           self:refreshable = false.
    if parts.ttype = "+" then do: 
     find zsdirefer where 
          recid(zsdirefer) = parts.xrecid
      no-error.
      if avail zsdirefer then do: 
       assign substring(parts.prod,index(parts.prod,"+",1),1) = "-"
              parts.ttype = "-".
       run bld-tier-recs.
      end.
      else 
       assign substring(parts.prod,index(parts.prod,"+",1),1) = "-"
              parts.ttype = "-".
    end.
    else do: 
     assign substring(parts.prod,index(parts.prod,"-",1),1) = "+"
            parts.ttype = "+"
            v-tierdn = yes.
     for each zy-parts use-index k-prod  where 
              zy-parts.fullprod = parts.fullprod and 
              zy-parts.typfl = string((h-lvl - 1),"9") + "9"
              exclusive-lock:
      delete zy-parts.
     end.          
    end. /* else do */
    self:refreshable = true.
   end.  /*  can-do("+,-",parts.ttype)  */
   else do:
    assign v-returning = true.
    self:select-focused-row().
    assign q-rowid = rowid(parts)          
    self:refreshable = true.
    return.
   end.
   reposition q-parts to rowid(q-rowid).
   get current q-parts.
   v-status = b-parts:select-focused-row().  /* make sure it's in focus */
   self:refreshable = true.
 end. /* k-accept.i or k-return.i or k-cancel.i */
end. /* on any-key */

procedure del-brwse-rec: 
do transaction:
 find first zy-parts where
      recid(zy-parts) = m-recid
      exclusive-lock.
 if avail zy-parts then do:  
  if can-do("+,-",zy-parts.ttype) then do:
   assign h-fullprod = zy-parts.fullprod
          h-foldnm   = zy-parts.foldnm.
  end.
 end.
 else 
  leave.
 if can-do("",zy-parts.ttype) then do: 
  find first zsdirefer where
      recid(zsdirefer) = parts.xrecid  no-error.
  if avail zsdirefer then do: 
   delete zsdirefer.
   if avail zy-parts then do: 
    get current q-parts exclusive-lock no-wait.
    delete zy-parts.

    find first zy-parts no-lock no-error.
    if not avail zy-parts then do:
      create zy-parts.

      assign zy-parts.ttype    = "+"
             zy-parts.tier     = string(1,"9")
             zy-parts.foldnm   =   "1" + v-delim
             zy-parts.clrfl    = "c"
             zy-parts.prod     = "<F7 Deletes Unit>"   
             zy-parts.fullprod = "<F7 Deletes Unit>"   
             zy-parts.sname    = "<F7 Deletes Unit>"   
             zy-parts.typfl    = string(1,"9") 
             zy-parts.xrecid   = ?
             zy-parts.xkeyno   = 1
             h-name         = " ".
    end.
    
    v-status = self:delete-selected-rows().
    get current q-parts.
    q-rowid = rowid(zy-parts).
   end.
  end.
 end.
 else 
 if can-do("-,+",zy-parts.ttype) then do:
  find first zsdirefer where
      recid(zsdirefer) = zy-parts.xrecid  no-error.
  if avail zsdirefer then do: 
   message " Delete folder(s) and all contents?"  
           skip
           "  Note: All folders and contents of "  
           skip
           "  this level will also be deleted.  "  
           skip
          entry(zsdirefer.tier,zsdirefer.fullpath,"~011")  
   view-as alert-box question buttons yes-no
   update  r as logical.
  end.
  if r then do: 
   if avail zsdirefer then do:  
    do xinx = 1 to 5: 
      s-words[xinx] = "".
    end.   
    assign s-key = "".
    do xinx = 1 to (h-lvl - 1): 
     if xinx > 1 then 
      s-key = s-key + v-delim.
      assign s-words[xinx] =
             (if can-do("",zy-parts.ttype) then 
               string(xinx,"9") + "-" + entry(xinx,zy-parts.fullprod,v-delim)
              else 
               entry(xinx,zy-parts.fullprod,v-delim))
             s-key = s-key + entry(xinx,zy-parts.fullprod,v-delim).
    end.
    do xinx = (h-lvl - 1) to 5:
     assign s-key = s-key + v-delim.
    end.
    assign sr-prod = "".
    run vaexf-refer.p (input s-puType,
                       input s-words[1],
                       input s-words[2],
                       input s-words[3],
                       input s-words[4],
                       input s-words[5],
                       input sr-prod,
                       input "Delete",
                       input "VA",
                       input-output s-recid,
                       input-output ix-keyno).
    /** delete folder from browse and inner product contents **/
    for each zy-parts use-index k-prod  where 
             zy-parts.fullprod begins h-fullprod and 
             zy-parts.typfl    ge string((h-lvl - 1),"9")
             exclusive-lock:
      find first zsdirefer where
           recid(zsdirefer) = zy-parts.xrecid  no-error.
      if avail zsdirefer then do: 
       delete zsdirefer.
      end. 
      for each zyp-parts use-index k-prod  where 
               zyp-parts.fullprod begins h-fullprod and 
               zyp-parts.typfl    = string((h-lvl - 1),"9") + "9"
          exclusive-lock:
        delete zyp-parts.
      end.
      delete zy-parts.
    end.

    find first zy-parts no-lock no-error.
    if not avail zy-parts then do:
      create zy-parts.

      assign zy-parts.ttype    = "+"
             zy-parts.tier     = string(1,"9")
             zy-parts.foldnm   =   "1" + v-delim
             zy-parts.clrfl    = "c"
             zy-parts.prod     = "<F7 Deletes Unit>"   
             zy-parts.fullprod = "<F7 Deletes Unit>"   
             zy-parts.sname    = "<F7 Deletes Unit>"   
             zy-parts.typfl    = string(1,"9") 
             zy-parts.xrecid   = ?
             zy-parts.xkeyno   = 1
             h-name         = " ".
    q-rowid = rowid(zy-parts).
             
    end.
 
   end.  /* if avail zsdirefer **/
  end. /* alert box message   */
  assign v-tierdn = yes.
 end. /* can-do + -  */
end.
end procedure.

procedure reset-brws:
 define input parameter up-down as char no-undo.
 /*
 find first zsdirefer where
            zsdirefer.cono       = g-cono and 
            zsdirefer.rectype    = "va" and 
            zsdirefer.fullpath   = parts.foldnm and
            zsdirefer.prod       ge (if up-down = "up" then
                                    parts.sprod
                                 else   
                                    sr-prod)   
            no-lock no-error.
*/
 find zsdirefer where
  recid(zsdirefer) = s-recid no-lock no-error.
 if not avail zsdirefer then do: 
  find last zsdirefer  where 
            zsdirefer.cono     = g-cono       and 
            zsdirefer.rectype    = "va"         and 
            zsdirefer.fullpath = parts.foldnm and 
            zsdirefer.prod     le sr-prod      
            no-lock no-error.
 end.
 
 if not avail zsdirefer then 
  leave.
  
 find first zy-parts where zy-parts.ttype = ""  and 
            zy-parts.sprod  = zsdirefer.prod    and
            zy-parts.foldnm = zsdirefer.fullpath
            no-lock no-error.
 if not avail zy-parts then do: 
  assign v-spc = "". 
  do xinx = 1 to (h-lvl - 1) :
    assign v-spc = v-spc + "~040".
  end.   
  
  create zy-parts.
  assign zy-parts.ttype     = ""
         zy-parts.tier      = string(h-lvl - 1,"9")
         zy-parts.foldnm    = zsdirefer.fullpath 
         zy-parts.sname     = parts.sname 
         zy-parts.clrfl     = "" 
         zy-parts.prod      = v-spc + zy-parts.ttype + zsdirefer.fentry 
         zy-parts.sprod     = zsdirefer.prod
         zy-parts.fullprod  = parts.fullprod
         zy-parts.typfl     = string((h-lvl - 1),"9") + "9"
         zy-parts.xrecid    = recid(zsdirefer)
         zy-parts.xkeyno    = zsdirefer.keyno.
  {w-icsp.i zsdirefer.prod no-lock}
  if avail icsp then do: 
   find first icsw use-index k-whse where
              icsw.cono = g-cono  and 
              icsw.whse = ix-whse  and 
              icsw.prod = zsdirefer.prod
              no-lock no-error.
   if avail icsw then do: 
    s-netavail = 0.
    {p-netavl.i}
    assign zy-parts.zavail = string(int(s-netavail),"zzzzzzz9").        
   end.
   else 
    assign zy-parts.zavail = "     N/A".
   if icsp.statustype = "l" then 
    assign zy-parts.zavail = "   Labor".
   else 
   if can-do("b,p",icsp.kittype) then 
    assign zy-parts.zavail = "     KIT".
   assign zy-parts.zdescrip = icsp.descrip[1] + icsp.descrip[2].
   end.
 end. /* if not avail zy-parts */
   
 assign h-b-prod      = zsdirefer.prod
        h-b-crossref  = zy-parts.foldnm
        h-b-parentkey = zsdirefer.parentkeyno 
        q-rowid = rowid(zy-parts).
 run repaint-brws(up-down).
 if up-down  = "up" then do: 
  if {k-prevpg.i} then do:
   get current q-parts.
  end.
  else 
  if {k-up.i} then do: 
   get prev q-parts.  
   reposition q-parts backward 1. 
  end.  
  else  
   reposition q-parts to rowid(q-rowid). 
 end.
 else
 if up-down  = "down" then do: 
  if {k-nextpg.i} then do:  
   get current q-parts.
  end.
  else 
  if {k-down.i} then 
   get next q-parts.
  else  
   reposition q-parts to rowid(q-rowid). 
 end.
 else  
  reposition q-parts to rowid(q-rowid). 
 enable b-parts with frame f-parts.
 display b-parts with frame f-parts.
end procedure.   
   
procedure bld-tier-recs:
 s-key = "".
 do xinx = 1 to 5: 
    s-words[xinx] = "".
 end.   
 do xinx = 1 to (h-lvl - 1): 
  if xinx > 1 then 
     s-key = s-key + v-delim.
  assign 
    s-words[xinx] = string(xinx,"9") + "-" + entry(xinx,parts.fullprod,v-delim)
    s-key         = s-key + string(xinx,"9") + "-"
                    + entry(xinx,parts.fullprod,v-delim).
 end.
 do xinx = (h-lvl - 1) to 5:
    assign s-key = s-key + v-delim.
 end.

 x = 0.
 find bp-zsdirefer where 
      recid(bp-zsdirefer) = parts.xrecid  no-lock no-error.

 find first zsdirefer where
            zsdirefer.cono           = g-cono and
            zsdirefer.rectype        = "va"   and
            zsdirefer.typekeyno      = zsdirefty.keyno and
            zsdirefer.parentkeyno    = bp-zsdirefer.keyno and
            zsdirefer.prod           <> "" no-lock no-error.
 if not avail zsdirefer then
  find zy-parts where 
       recid(zy-parts) = recid(parts) no-lock no-error.
 else do:
 cr:
  for  each zsdirefer where
            zsdirefer.cono           = g-cono and
            zsdirefer.rectype        = "va"   and
            zsdirefer.typekeyno      = zsdirefty.keyno and
            zsdirefer.parentkeyno    = bp-zsdirefer.keyno and
            zsdirefer.prod           <> "" no-lock:

   assign v-spc = "". 
   do xinx = 1 to (h-lvl - 1) :
      assign v-spc = v-spc + "~040".
   end.   
   x = x + 1. 
   if x > 26 then 
     leave cr.      
   create zy-parts.
   assign zy-parts.ttype     = ""
          zy-parts.tier      = string(h-lvl - 1,"9")
          zy-parts.foldnm    = zsdirefer.fullpath 
          zy-parts.sname     = parts.sname 
          zy-parts.clrfl     = ""
          zy-parts.prod      = v-spc + zy-parts.ttype + zsdirefer.fentry
          zy-parts.sprod     = zsdirefer.prod
          zy-parts.fullprod  = parts.fullprod
          zy-parts.typfl     = string((h-lvl - 1),"9") + "9"
          zy-parts.xrecid    = recid(zsdirefer)
          zy-parts.xkeyno    = zsdirefer.keyno.

   {w-icsp.i zsdirefer.prod no-lock}
   if not avail icsp then 
      assign zy-parts.zdescrip = 
             "** invalid icsp - notfound **".
   if avail icsp then do: 
    find first icsw use-index k-whse where
               icsw.cono = g-cono  and 
               icsw.whse = ix-whse  and 
               icsw.prod = zsdirefer.prod
               no-lock no-error.
    if avail icsw then do: 
     s-netavail = 0.
     {p-netavl.i}
     assign zy-parts.zavail = string(int(s-netavail),"zzzzzzz9").        
    end.
    else 
     assign zy-parts.zavail = "     N/A".
    if icsp.statustype = "l" then 
     assign zy-parts.zavail = "   Labor".
    else 
    if can-do("b,p",icsp.kittype) then 
     assign zy-parts.zavail = "     KIT".
    assign zy-parts.zdescrip = icsp.descrip[1] + icsp.descrip[2].
   end.
  end. /* cr look */
 end.
 reposition q-parts to rowid(rowid(zy-parts)).  
end procedure. 

procedure repaint-brws:
  define input parameter up-down as char no-undo.
  x = 0.
  if up-down = "up" then do: 
   run repaint-brws-back.
   leave.
  end.
  rb:
  for each zsdirefer where
      zsdirefer.cono        = g-cono       and
      zsdirefer.rectype     = "va"         and
      zsdirefer.prod        > h-b-prod     and 
      zsdirefer.prod       <> ""           and 
      zsdirefer.fullpath    = h-b-crossref and 
      zsdirefer.parentkeyno = h-b-parentkey     
      no-lock:
   
   x = x + 1. 
   if x > 26 then 
     leave rb. 
   find first zy-parts where 
              zy-parts.ttype = ""  and 
              zy-parts.sprod  = zsdirefer.prod      and
              zy-parts.foldnm = zsdirefer.fullpath
              no-lock no-error.
   if avail zy-parts then do: 
    next.        
   end.
   assign v-spc = "". 
   do xinx = 1 to (h-lvl - 1) :
      assign v-spc = v-spc + "~040".
   end.   
   create zy-parts.
   assign zy-parts.ttype     = ""
          zy-parts.tier      = string(h-lvl - 1,"9")
          zy-parts.foldnm    = zsdirefer.fullpath 
          zy-parts.sname     = parts.sname 
          zy-parts.prod      = v-spc + zy-parts.ttype + zsdirefer.fentry 
          zy-parts.sprod     = zsdirefer.prod
          zy-parts.clrfl     = ""
          zy-parts.fullprod  = parts.fullprod
          zy-parts.typfl     = string((h-lvl - 1),"9") + "9"
          zy-parts.xrecid    = recid(zsdirefer)
          zy-parts.xkeyno    = zsdirefer.keyno.

   {w-icsp.i zsdirefer.prod no-lock}
   if avail icsp then do: 
    find first icsw use-index k-whse where
               icsw.cono = g-cono  and 
               icsw.whse = ix-whse  and 
               icsw.prod = zsdirefer.prod
               no-lock no-error.
    if avail icsw then do: 
     s-netavail = 0.
     {p-netavl.i}
     assign zy-parts.zavail = string(int(s-netavail),"zzzzzzz9").        
    end.
    else 
     assign zy-parts.zavail = "     N/A".
    if icsp.statustype = "l" then 
     assign zy-parts.zavail = "   Labor".
    else 
    if can-do("b,p",icsp.kittype) then 
     assign zy-parts.zavail = "     KIT".
    assign zy-parts.zdescrip = icsp.descrip[1] + icsp.descrip[2].
   end.
  end. /* cr look */
end procedure.

procedure repaint-brws-back:
  x = 0.
  rb:
  repeat: 
  find prev zsdirefer where
      zsdirefer.cono        = g-cono       and
      zsdirefer.rectype     = "va"         and
      zsdirefer.prod        le h-b-prod    and 
      zsdirefer.prod        <> ""          and 
      zsdirefer.fullpath    = h-b-crossref and 
      zsdirefer.parentkeyno = h-b-parentkey       
       no-lock no-error.
   x = x + 1. 
   if x > 13  or not avail zsdirefer then 
    leave rb. 

   find first zy-parts where 
              zy-parts.ttype = "" and 
              zy-parts.sprod  = zsdirefer.prod and
              zy-parts.foldnm = zsdirefer.fullpath
              no-lock no-error.
   if avail zy-parts then do:
    next.        
   end.
   assign v-spc = "". 
   do xinx = 1 to (h-lvl - 1) :
      assign v-spc = v-spc + "~040".
   end.   

   create zy-parts.
   assign zy-parts.ttype     = ""
          zy-parts.tier      = string(h-lvl - 1,"9")
          zy-parts.foldnm    = zsdirefer.fullpath 
          zy-parts.sname     = parts.sname 
          zy-parts.clrfl     = ""
          zy-parts.prod      = v-spc + zy-parts.ttype + zsdirefer.fentry 
          zy-parts.sprod     = zsdirefer.prod
          zy-parts.fullprod  = parts.fullprod
          zy-parts.typfl     = string((h-lvl - 1),"9") + "9"
          zy-parts.xrecid    = recid(zsdirefer)
          zy-parts.xkeyno    = zsdirefer.keyno.

   {w-icsp.i zsdirefer.prod no-lock}
   if avail icsp then do:  
    find first icsw use-index k-whse where
               icsw.cono = g-cono  and 
               icsw.whse = ix-whse  and 
               icsw.prod = zsdirefer.prod
               no-lock no-error.
    if avail icsw then do: 
     s-netavail = 0.
     {p-netavl.i}
     assign zy-parts.zavail = string(int(s-netavail),"zzzzzzz9").        
    end.
    else 
     assign zy-parts.zavail = "     N/A".
    if icsp.statustype = "l" then 
     assign zy-parts.zavail = "   Labor".
    else 
    if can-do("b,p",icsp.kittype) then 
     assign zy-parts.zavail = "     KIT".
    assign zy-parts.zdescrip = icsp.descrip[1] + icsp.descrip[2].
   end.
  end. /* cr look */
end procedure.

procedure create-brws:

 for each zsdirefer where
       zsdirefer.cono      = g-cono and
       zsdirefer.rectype   = "va"    and
       zsdirefer.typekeyno = zsdirefty.keyno and
       zsdirefer.prod = "" 
       no-lock :
   
   zy-cnt = zy-cnt + 1.                    
   assign v-spc = "". 
   do xinx = 1 to zsdirefer.tier:
      assign v-spc = v-spc + "~040".
   end.   

   s-key = "".
   do xinx = 1 to 5: 
     s-words[xinx] = "".
   end.   
   do xinx = 1 to zsdirefer.tier: 
    if xinx > 1 then 
     s-key = s-key + v-delim.
    assign s-words[xinx] = string(xinx,"9") + "-" + 
                            entry(xinx,zsdirefer.fullpath,v-delim)
            s-key = s-key + string(xinx,"9") + "-"
                    + entry(xinx,zsdirefer.fullpath,v-delim).
   end.
   do xinx = zsdirefer.tier to 5:
    assign s-key = s-key + v-delim.
   end.
   create parts.
   assign parts.ttype    = "+"
          parts.tier     = string(zsdirefer.tier,"9")
          parts.foldnm   =   s-key
          parts.clrfl    = "c"
          parts.prod     = v-spc + parts.ttype +
                           entry(zsdirefer.tier,zsdirefer.fullpath,"~011")   
          parts.fullprod = zsdirefer.fullpath
          parts.sname    = zsdirefer.fullpath
          parts.typfl    = string(zsdirefer.tier,"9") 
          parts.xrecid   = recid(zsdirefer)
          parts.xkeyno   = zsdirefer.keyno
          h-name         = zsdirefer.prod.
 end. /* for each */

 find first parts no-lock no-error.
 if not avail parts then do:
   create parts.

   assign parts.ttype    = "+"
          parts.tier     = string(1,"9")
          parts.foldnm   =   "1" + v-delim
          parts.clrfl    = "c"
          parts.prod     = "<F7 Deletes Unit>"   
          parts.fullprod = "<F7 Deletes Unit>"   
          parts.sname    = "<F7 Deletes Unit>"   
          parts.typfl    = string(1,"9") 
          parts.xrecid   = ?
          parts.xkeyno   = 1
          h-name         = " ".
 end.
 




end procedure.

procedure add-create-brws:
 do winx = 1 to 5:
  if sa-rowid[winx] = ? then
    return.
   export stream debug delimiter "~011"
         "Index"
         winx. 
   find zsdirefer where 
        recid(zsdirefer) = sa-rowid[winx] and
        zsdirefer.prod = ""
        no-lock no-error.
   if avail zsdirefer then do:
    assign v-spc = "". 
    do xinx = 1 to zsdirefer.tier:
      assign v-spc = v-spc + "~040".
    end.   
    s-key = "".
    do xinx = 1 to zsdirefer.tier: 
     if xinx > 1 then 
      s-key = s-key + v-delim.
      assign s-key = s-key + string(xinx,"9") + "-" + zsdirefer.reference.
    end.
    do xinx = zsdirefer.tier to 5:
      assign s-key = s-key + v-delim.
    end.
    export stream debug delimiter "~011"
     "Before add"
      zsdirefer.fullpath
      zsdirefer.tier
      zsdirefer.fentry. 

    find first parts where
              parts.fullprod = "<F7 Deletes Unit>" no-error.   
    if avail parts then do:
      delete parts.
    end.
    
    find parts use-index k-parts where
         parts.sname = zsdirefer.fullpath and
         parts.tier  = string(zsdirefer.tier,"9") and
         parts.typfl = string(zsdirefer.tier,"9") and
        (parts.prod  = v-spc + "+"  + zsdirefer.fentry   or
         parts.prod  = v-spc + "-" + zsdirefer.fentry ) no-lock no-error.

    if not avail parts then do:
     create parts.
     assign parts.ttype    = "+"
     parts.tier     = string(zsdirefer.tier,"9")
     parts.foldnm   = s-key
     parts.clrfl    = "c"
     parts.prod     = v-spc + parts.ttype + zsdirefer.fentry  
     parts.fullprod = zsdirefer.fullpath 
     parts.sname    = zsdirefer.fullpath
     parts.typfl    = string(zsdirefer.tier,"9")
     parts.xrecid   = recid(zsdirefer)
     parts.xkeyno   = zsdirefer.keyno. 
     export stream debug delimiter "~011"
      "After add"
      zsdirefer.fullpath
      zsdirefer.tier
      zsdirefer.fentry. 
    end.
   q-rowid = rowid(parts).
  end. /* find rowid saindex */
  else do: 
   assign v-error = "Cannot Add - Folder Already Exists At This Level".
   run zsdierrx.p(v-error,"yes").
  end.    
 end. /* do winx */

end procedure. 

procedure fillin-whses.

 
  for each t-whseinfo:
    delete t-whseinfo.
  end.  


 assign winx     = 0
        s-whse   = ""
        h-avail  = ""
        s-avail  = ""
        l-vendno = ""
        l-vendnm = ""
        whsefl   = false.

  for each icsd where 
      icsd.cono = g-cono and 
      icsd.salesfl = yes and
      icsd.custno  = 0   and
      not can-do("c",substring(icsd.whse,1,1))
      no-lock:
 
  find icsw use-index k-whse where
       icsw.cono = g-cono    and 
       icsw.whse = icsd.whse and 
       icsw.prod = parts.sprod
       no-lock no-error.
  if avail icsw then do:      
   find apsv where apsv.cono = g-cono and 
                   apsv.vendno = icsw.arpvendno
                   no-lock no-error.
   if avail apsv and not whsefl then 
    assign l-vendno  = string(dec(icsw.arpvendno),"zzzzzzzzzzzz") 
           l-vendnm  = apsv.name
           whsefl    = if ix-whse = icsw.whse then 
                        true
                       else whsefl.
      s-netavail = 0.
      {w-icsp.i icsw.prod no-lock}
      if avail icsp then 
      do:
       {p-netavl.i}
       create t-whseinfo.
       assign t-whseinfo.zavail = s-netavail
              t-whseinfo.zoo    = icsw.qtyonorder
              t-whseinfo.whse   = icsw.whse
              t-whseinfo.widx   = if icsw.whse = ix-whse then 
                                   1
                                  else 
                                   0.
       find apsv where apsv.cono = g-cono and 
                       apsv.vendno = icsw.arpvendno
                       no-lock no-error.
       if avail apsv and not whsefl then 
        assign l-vendno  = string(dec(icsw.arpvendno),"zzzzzzzzzzzz") 
               l-vendnm  = apsv.name
               whsefl    = if ix-whse = icsw.whse then 
                             true
                           else whsefl.
 
/*  
    s-netavail = 0.
    {w-icsp.i icsw.prod no-lock}
    if avail icsp then do: 
     {p-netavl.i}
     if int(s-netavail) >= 0 then do: 
      winx = winx + 1.
      if winx > 12 then 
       leave.
      else do:    
       find apsv where apsv.cono = g-cono and 
                       apsv.vendno = icsw.arpvendno
                       no-lock no-error.
       if avail apsv and not whsefl then 
        assign l-vendno  = string(dec(icsw.arpvendno),"zzzzzzzzzzzz") 
               l-vendnm  = apsv.name
               whsefl    = if ix-whse = icsw.whse then 
                            true
                           else whsefl.
       assign s-whse[winx]  = icsw.whse.
              s-avail[winx] = string(int(s-netavail),"zzzzzz9").
      end.
     end. /* if netavail > 0 */
   */
    end. /* if avail icsp */
  end. /* avail icsw */
end. /* for each */

/*

display s-whse[1]  s-whse[2]  s-whse[3]  s-whse[4]  s-whse[5]  s-whse[6]
        s-avail[1] s-avail[2] s-avail[3] s-avail[4] s-avail[5] s-avail[6]
        s-whse[7]  s-whse[8]  s-whse[9]  s-whse[10] s-whse[11]  s-whse[12]
        s-avail[7] s-avail[8] s-avail[9] s-avail[10] s-avail[11] s-avail[12]
        l-vendno l-vendnm
        with frame f-whses.
*/

for each t-whseinfo no-lock:
  assign winx = winx + 1.
  if winx > 12 then
    leave.
  
  assign s-whse[winx] =  t-whseinfo.whse.
  if t-whseinfo.zavail > 999 then
     assign disp-avail = "***".
  else
  if t-whseinfo.zavail < 0 then
     assign disp-avail = "***".
  else
     assign disp-avail = string(t-whseinfo.zavail,"zz9").

  if t-whseinfo.zoo > 999 then
     assign disp-oo = "***".
  else
  if t-whseinfo.zoo < 0 then
     assign disp-oo = "***".
  else
     assign disp-oo = string(t-whseinfo.zoo,"zz9").
     
  assign h-avail[winx] = disp-avail + "/" + disp-oo.
        /*
         h-avail[winx] = string(t-whseinfo.zavail,"zz9") + "/" +
                         string(t-whseinfo.zoo,"zz9").
        */
end.

next-prompt s-whse with frame f-whses.

display s-whse[1]  s-whse[2]  s-whse[3]  s-whse[4]  s-whse[5]  s-whse[6]
        h-avail[1] h-avail[2] h-avail[3] h-avail[4] h-avail[5] h-avail[6]
        s-whse[7]  s-whse[8]  s-whse[9]  s-whse[10] s-whse[11]  s-whse[12]
        h-avail[7] h-avail[8] h-avail[9] h-avail[10] h-avail[11] h-avail[12]
        l-vendno l-vendnm
        with frame f-whses.


end procedure.

procedure clear-browse:
  for each parts no-lock:
      delete parts.
  end.
end procedure.  

procedure first-zy-create: do: 
 onetime = true.
 open query q-parts for each parts use-index k-parts indexed-reposition.
 updt2:
 do while onetime on endkey undo,leave: 
  display s-putype with frame f-words.
  update s-putype   /* when s-puType   = "" */
         s-words[1] when s-words[2] = ""
         s-words[2] when s-words[3] = ""
         s-words[3] when s-words[4] = ""
         s-words[4] when s-words[5] = ""
         s-words[5] 
         go-on(f1)  with frame f-words
   editing:
    readkey.
    if {k-cancel.i} then do: 
     hide frame f-words. 
     leave.    
    end. 
    apply lastkey.
   end.
    if s-puType   ne "" then next-prompt s-words[1] with frame f-words.
    if s-words[1] ne "" then next-prompt s-words[2] with frame f-words.
    if s-words[2] ne "" then next-prompt s-words[3] with frame f-words.
    if s-words[3] ne "" then next-prompt s-words[4] with frame f-words.
    if s-words[4] ne "" then next-prompt s-words[5] with frame f-words.
    if {k-cancel.i} then
     leave.    
    if s-words[1] = "" and
      (s-words[2] = "" or s-words[3] = "" or 
       s-words[4] = "" or s-words[5] = "") then do:
     run zsdierrx.p(pv-errormess,"yes"). 
     next.
    end. 
    else 

    if s-words[1] = "" and
      (s-words[2] ne "" or s-words[3] ne "" or 
       s-words[4] ne "" or s-words[5] ne "") then do:
     run zsdierrx.p(pv-errormess,"yes"). 
     next.
    end. 
    else 
    if s-words[2] ne "" and
      (s-words[1] = "") then do:
     run zsdierrx.p(pv-errormess,"yes"). 
     next.
    end.     
    else 
    if s-words[3] ne "" and
      (s-words[1] = "" or s-words[2] = "") then do: 
     run zsdierrx.p(pv-errormess,"yes"). 
     next.
    end.     
    else 
    if s-words[4] ne "" and
      (s-words[1] = "" or s-words[2] = "" or s-words[3] = "") then do: 
     run zsdierrx.p(pv-errormess,"yes"). 
     next.
    end.     
    else 
    if s-words[5] ne "" and
      (s-words[1] = "" or s-words[2] = "" or s-words[3] = "" or 
       s-words[4] = "") then do:
     run zsdierrx.p(pv-errormess,"yes"). 
     next.
    end.     
    assign sr-prod = ""
           sa-rowid[1]  = ?
           sa-rowid[2]  = ?
           sa-rowid[3]  = ?
           sa-rowid[4]  = ?
           sa-rowid[5]  = ?.
    if {k-after.i} then 
     hide frame f-words.
    run vaexf-refer.p (input s-puType,
                       input s-words[1],
                       input s-words[2],
                       input s-words[3],
                       input s-words[4],
                       input s-words[5],
                       input sr-prod,
                       input "ADD",
                       input "VA",
                       input-output s-recid,
                       input-output ix-keyno).
     
    run add-create-brws.
    onetime = false.
    close query q-parts.
    leave updt2.
 end. /* do while true */ 
end. 
end procedure. 



/* -------------------------------------------------------------------------  */
PROCEDURE MoveMfgLu:
/* -------------------------------------------------------------------------  */
  DEFINE INPUT PARAMETER ix-lastkey AS INTEGER                 NO-UNDO.
  
  ON cursor-up cursor-up.
  ON cursor-down cursor-down.
  ENABLE b-models WITH FRAME f-models.
  APPLY LASTKEY TO b-models IN FRAME f-models.
  IF LASTKEY = 502 AND v-mfgbrowsekeyed = FALSE THEN DO:
    APPLY x-cursorup TO b-models IN FRAME f-models.
  END.
  APPLY "focus" TO ix-puType IN FRAME f-header.

END PROCEDURE.

/* --------------------&&&--------------------------------------------------  */
PROCEDURE DoMfgLu:
/* -------------------------------------------------------------------------  */
  DEFINE INPUT PARAMETER ix-model   AS CHARACTER FORMAT "x(24)" NO-UNDO.
  DEFINE INPUT PARAMETER ix-runtype AS CHARACTER                NO-UNDO.

  ASSIGN v-modelsbuildfl = TRUE.

  EMPTY TEMP-TABLE models.
  
  OPEN QUERY q-models
  FOR EACH zsdirefty WHERE zsdirefty.cono = g-cono AND
           zsdirefty.rectype = "va" NO-LOCK.
    ASSIGN  
      v-mfgbrowseopen = TRUE
      v-modelsbuildfl = FALSE.
    DISPLAY b-models WITH FRAME f-models. 
    APPLY "focus" TO ix-puType IN FRAME f-header.
    
  END PROCEDURE.
 


main:
do while v-notdone on endkey undo,leave main
                   on error undo, leave main:
    
  disable b-parts with frame f-parts.
  /* view frame f-whses. */
  on cursor-up cursor-up.
  on cursor-down cursor-down.
 
  UpdateLoop:
  do while v-notdone TRANSACTION on endkey undo,leave UpdateLoop
                                 on error undo, leave UpdateLoop:
    display ix-puType with frame f-header.
    update ix-puType with frame f-header
      editing:
        readkey.
        if {k-cancel.i} and frame-field = "ix-puType" and
            (v-mfgbrowsekeyed = true or 
             v-mfgbrowseopen = true) THEN DO:
          readkey pause 0.
          CLOSE QUERY q-models.
        
          hide frame f-models.
          if v-mfgbrowsekeyed = false then do:
            assign v-mfgbrowseopen = FALSE
                   v-mfgbrowsekeyed = FALSE
                   v-modelsbuildfl = FALSE.
            LEAVE MAIN.
          end.
          APPLY "entry" TO ix-puType IN FRAME f-header.
          NEXT-PROMPT ix-puType WITH FRAME f-header.

          assign v-mfgbrowseopen = FALSE
                 v-mfgbrowsekeyed = FALSE
                 v-modelsbuildfl = FALSE.
          next.
        end.
        else 
        
        if {k-jump.i} or {k-cancel.i} then do:
          HIDE FRAME f-models.
          leave main.
        end.

      
        IF FRAME-FIELD = "ix-puType" AND
          (CAN-DO('f7',KEYLABEL(LASTKEY))     OR
           CAN-DO('f8',KEYLABEL(LASTKEY))     OR
           CAN-DO('f9',KEYLABEL(LASTKEY))     OR
           CAN-DO('f10',KEYLABEL(LASTKEY))) THEN DO:
          ASSIGN ix-puType = INPUT ix-puType.
        END.
        ELSE
        IF FRAME-FIELD = "ix-puType" and
           v-mfgbrowseopen = true THEN DO:
          HIDE FRAME f-models.
          IF NOT CAN-DO("9,13,401,404,501,502,503,504,508,507",STRING(LASTKEY))
          THEN
            ASSIGN v-mfgbrowsekeyed = FALSE.
          IF (NOT CAN-DO("9,21,13,404,501,502,503,504,507,508",STRING(LASTKEY))
             AND
              KEYFUNCTION(LASTKEY) NE "go" AND
              KEYFUNCTION(LASTKEY) NE "end-error") THEN DO:
            /* user has keyed in non-movement key */
            APPLY LASTKEY.
            IF FRAME-FIELD <> "ix-puType" THEN
              NEXT.
            ASSIGN ix-puType = INPUT ix-puType.
            RUN DoMfgLu (INPUT ix-puType,
                         INPUT "normal").
            ASSIGN v-mfgbrowsekeyed = FALSE.
            NEXT.
          END.
          ELSE IF (LASTKEY = 501 OR
                   LASTKEY = 502 OR
                   LASTKEY = 507 OR
                   LASTKEY = 508) AND v-mfgbrowseopen = TRUE THEN DO:
            RUN MoveMfgLu (INPUT LASTKEY).
            ASSIGN v-mfgbrowsekeyed = TRUE.
            NEXT.
          END.
          ELSE
          IF KEYFUNCTION(LASTKEY) = "end-error" AND
             v-mfgbrowsekeyed    = TRUE THEN DO:
            ON cursor-up back-tab.
            ON cursor-down tab.
            HIDE FRAME f-models.
            NEXT.
          END.
          ELSE
          IF KEYFUNCTION(LASTKEY) = "end-error" AND
             v-mfgbrowsekeyed    = false THEN DO:
            ON cursor-up back-tab.
            ON cursor-down tab.
            HIDE FRAME f-models.
            LEAVE MAIN.
          END.
 
          
          
          ELSE
          IF (LASTKEY = 13 OR KEYFUNCTION(LASTKEY) = "go")
             AND NOT v-mfgbrowsekeyed  THEN DO:
            ASSIGN ix-puType  = INPUT ix-puType.
            display ix-puType with frame f-header.
            assign v-mfgbrowseopen = FALSE
                   v-mfgbrowsekeyed = FALSE
                   v-modelsbuildfl = FALSE.
            CLOSE QUERY q-models.
            ON cursor-up back-tab.
            ON cursor-down tab.
            HIDE FRAME f-models.
          END.
          ELSE
          IF (LASTKEY = 13 OR KEYFUNCTION(LASTKEY) = "go")
            AND v-mfgbrowsekeyed AND avail zsdirefty THEN DO:
            ASSIGN ix-puType = zsdirefty.reference.
            DISPLAY ix-puType  WITH FRAME f-header.
            assign v-mfgbrowseopen = FALSE
                   v-mfgbrowsekeyed = FALSE
                   v-modelsbuildfl = FALSE.
            CLOSE QUERY q-models.
            ON cursor-up back-tab.    /**&&**/
            ON cursor-down tab.
            HIDE FRAME f-models.
          END.
          ELSE DO:
            ASSIGN v-mfgbrowsekeyed = FALSE.
            HIDE FRAME f-models.
          END.
        END.
        
        apply lastkey.
      end.  
     assign v-mfgbrowseopen = FALSE
            v-mfgbrowsekeyed = FALSE
            v-modelsbuildfl = FALSE.
     if ix-puType = "" then do:
      message "Unit Type is Required".
      next UpdateLoop.
    end.  
    assign s-puType = ix-puType.
    find zsdirefty where
         zsdirefty.cono      = g-cono and
         zsdirefty.rectype   = "va"    and
         zsdirefty.reference = s-puType no-lock no-error.


    if avail zsdirefty then
      leave UpdateLoop.
    else
    if not avail zsdirefty then do:
      {p-addrec.i}
               
      if {k-create.i} then do :
         find last b-zsdirefty use-index k-keyno where
                   b-zsdirefty.cono      = g-cono and
                   b-zsdirefty.rectype   = "va"  no-lock no-error.
         create zsdirefty.           
         assign zsdirefty.cono       = g-cono 
                zsdirefty.rectype   = "va"  
                zsdirefty.reference = s-puType
                zsdirefty.keyno     = if avail b-zsdirefty then
                                        b-zsdirefty.keyno + 1
                                      else
                                        1. 
      end.
    
    end.
/*    
    s-puType = "Generic".
    find zsdirefty where
       zsdirefty.cono      = g-cono and
       zsdirefty.rectype   = "va"    and
       zsdirefty.reference = s-puType no-lock no-error.
  end.
  if not avail zsdirefty then do:
   run vaexf-refer.p (input s-puType,
                      input " ",
                      input " ",
                      input " ",
                      input " ",
                      input " ",
                      input "",
                      input "ADD",
                      input "VA",
                      input-output s-recid,
                      input-output ix-keyno).
  end.                                     

  if not v-tierdn then do: 
   find first zsdirefer where 
              zsdirefer.cono = g-cono      and 
              zsdirefer.rectype = "va"     and
              zsdirefer.typekeyno = zsdirefty.keyno
              no-lock no-error.
   if not avail zsdirefer then do: 
    run first-zy-create.
    if {k-cancel.i} then do: 
     return.
    end. 
   end. 
   else               
*/
  end. /* Update Loop */

  find zsdirefty where
       zsdirefty.cono      = g-cono and
       zsdirefty.rectype   = "va"    and
       zsdirefty.reference = s-puType no-lock no-error.
  
  for each    parts : 
    delete parts.
  end.
  assign v-exitbrowse = false. 
  run create-brws.
  open query q-parts for each parts use-index k-parts indexed-reposition. 
  enable all with frame f-parts.
  apply "entry" to b-parts. 
  STATUS input
    "Space Bar opens folder (+) or closes folder (-), F4 to end".
  
  assign v-returning = false.
  ksubtran:
  do while v-notdone TRANSACTION on endkey undo, leave ksubtran
                                 on error undo,  leave ksubtran:
    on cursor-up cursor-up.
    on cursor-down cursor-down.
    assign v-returning = false.

    STATUS DEFAULT
    "Space Bar opens folder (+) or closes folder (-), F4 to end".
    
    
    
    put screen col 2 row 21 color message 
"              F6-Add    F7-Delete                                             ".
    wait-for PF4 ,endkey ,  
             window-close of current-window.

    STATUS DEFAULT
    "                                                             ".
    put screen col 2 row 21   
"                                                                             ".
 
/*
    if {k-after.i} and not v-returning then do:
      hide all.
      leave main.
    end.
*/ 
    
    if {k-jump.i} then do: 
      hide all.
      leave main.
    end.
    else
    if ({k-cancel.i} and not v-returning) or
       (v-exitbrowse and not v-returning) then do: 
      v-returning = false. 
      
      on cursor-up back-tab.
      on cursor-down tab.
      close query q-parts.

      hide frame f-parts.
      readkey pause 0.
      next main.
    end.
    else
    if {k-cancel.i} and v-returning then do: 
      v-returning = false. 
      v-notdone = true.

      on cursor-up back-tab.
      on cursor-down tab.
      close query q-parts.
      hide frame f-parts.
      
      readkey pause 0.
      next main.
    end.

  end. /* Ksubtran */
end.  /* Main */

on cursor-up back-tab.
on cursor-down tab.
close query q-parts.
hide frame f-parts.
hide frame f-whses.
hide frame f-newprod.
hide frame f-lookprod.
status default.

