/* message "running browse". pause.  */

{g-all.i}
{g-ic.i}
{g-oe.i}
{g-kp.i}
{g-wt.i}
{g-po.i}
{va.gva}


define input parameter ip-whse    as char no-undo.
define input parameter ip-custno  like arsc.custno no-undo.



def var v-QryDesc as char              no-undo.
def var v-QryCat  as char              no-undo.
def var v-QryVen  as char              no-undo.
def var v-QryWhse as char              no-undo.
def var v-QryProd as char              no-undo.

def var v-entries as int               no-undo.
def var v-inx     as int               no-undo.
def var        v-myframe   as character format "x(5)" no-undo.
def var v-custno like arsc.custno no-undo.      
def var v-row as int no-undo.                   
def var v-down as int no-undo.                  
def var out-whse like icsw.whse no-undo.        
def var out-prod like icsw.prod no-undo.        
def var out-type as char  no-undo.              
def var out-qty as dec  no-undo.                
def var out-unit as char  no-undo.              
def var        x-lookup    as char no-undo.
def var        x-lookup2   as char no-undo.

define query dmdH-q for oeeh,arsc. /* scrolling. */

define browse dmdh-b query dmdh-q 
  display  
  oeeh.orderno      column-label "OrderNo"  space (1)
  oeeh.enterdt      column-label "EnterDt"
  oeeh.takenby      column-label "TakenBy"
  oeeh.custno       column-label "Custno" 
  arsc.name         column-label "Name"
 
 
 with no-box size-chars 78 by 15 down.

define frame  f-qH
  dmdH-b
  with  no-underline overlay width 80 row 5.

on any-key of dmdH-b do:
  if {k-cancel.i} then do:
    leave.
  end.
  
  if {k-func6.i} then do:
     
      v-myframe = "oeio".
      run titlemorph.
       
      assign
        g-orderno = oeeh.orderno
        g-ordersuf = oeeh.ordersuf.
      run oeio.p.
      
      v-myframe = "oeizw".
      run titlemorph.
      
     
      on cursor-up cursor-up.
      on cursor-down cursor-down.
      put screen row 21 col 3 
       color message
   " F6 - OIEO                                                             ".

  end.
end.


/* All browses are here because of buffer conficts caused by 
   order by 
*/   
   
main:

do while true on endkey undo main, leave main:
  
 
 on cursor-up cursor-up.
 on cursor-down cursor-down.

  
 assign x-lookup = "([sxcono_" +                            
                    string(g-cono) + "]". 
      
 assign x-lookup = x-lookup + "&[sxstagecd_0}".    
 assign x-lookup = x-lookup + "&[sxtranstype_QU]".

 if ip-whse <> "" then
   assign x-lookup = x-lookup + "&[sxwhse_" + 
                     right-trim(left-trim(ip-whse," ")," ") + "]".
 if ip-custno <> 0 then
   assign x-lookup = x-lookup + "&[sxcustno_" + 
                     right-trim(left-trim(string(ip-custno)," ")," ") + "]".


 
 
 assign x-lookup = x-lookup + ")".     
 open query dmdH-q 
   for each oeeh where
            oeeh.keyindex contains x-lookup and
            oeeh.sourcepros = "WEB"
     no-lock,
     first arsc where arsc.cono = g-cono and
                      arsc.custno = oeeh.custno.
 enable dmdH-b with frame f-qH.
 
 put screen row 21 col 3 
   color message
 " F6 - OEIO                                                                  ".
 
 wait-for window-close of current-window.

 close query dmdH-q.
    
end. /* Main */
 
on cursor-up back-tab.
on cursor-down tab.

hide frame  f-qH.

readkey pause 0.



procedure titlemorph:

find sassm where sassm.currproc = v-myframe no-lock no-error.
if avail sassm then
  g-title = sassm.frametitle.
end.  

