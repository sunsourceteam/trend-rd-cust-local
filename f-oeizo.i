
/* SX 55
  h*****************************************************************************
  INCLUDE      : f-oeizo.i - based off of f-oeetle.i
  DESCRIPTION  : Quote Line Items Extended Screen
  USED ONCE?   : no
  AUTHOR       : kmw
  DATE WRITTEN : 08/04/89
  CHANGES MADE :
    11/03/92 mms; TB#  8566 Rush - added variables & rearranged screen
    12/09/92 mwb; TB#  4535 added v-rebatenet and v-rebatecost for "r" display
    04/12/93 kmw; TB# 10378 Allow entry of LT for non-stocks
    04/21/93 jlc; TB# 11023 Incorrect help messages
    05/05/93 kmw; TB# 11227 Need help prompt for PO/WT on F7 extend
    07/22/93 kmw; TB# 12198 Display more info on Extended Screen
    10/20/93 kmw; TB# 13273 JIT date window display - move to right side
    09/22/95 gdf; TB# 18812 7.0 Rebates Enhancement (C1a) Added v-frzreblabel
        and v-frzrebty and rearranged form
    12/12/95 kr ; TB# 19904 No [L] help indicator on Alt Order#
    08/21/96 jms; TB# 21254 (17A)Develop Value Add Module - Enlarge frame and
        add VA to PO/WT/WO#
    06/19/00 maa; TB# e5061 Inventory Allocation Project
    02/12/01 lcg; TB# e7852 AR Multicurrency changes
    03/14/01 lcg; TB# 8198 Multicurrency correction. Changed v-excost and v-cost
        position. Added Domestic to Net column label.
    05/28/01 lcg; TB# e9067 Domestic labels should only display
        when using a foreign customer
    06/12/01 lcg; TB# e9298 OEIO Extend Screen "Domestic Cost" is misaligned
    10/11/01 usb; TB# e10577 Ext Ln Format Discount entry-BMAT OE Usability Proj
    10/17/03 kjb; TB# e17500 Modify format of net cost and margin percent to
        prevent ???? displaying
    04/19/04 cm;  TB# e9464 Add standard pack
    06/27/05 mwb; TB# L1685 Added wlpicktype for counter sale dropping.
*******************************************************************************/

form
  s-qtyship                                 colon 13
  s-usagefl                                 colon 43
        help
        "Enter 'No' to not Record Usage; 'Yes' Follows Usage Rules"
  s-unitstnd                                colon 58 label "Stnd Pack"
  s-botype                                  colon 13
    /*tb e5061 06/19/00 maa; Add reservation delay message to frame */
  s-delaymsg                                at    16 format "x(8)" no-label
  v-pdatelit                                at    25 no-label    
  s-jobno                                   colon 43
  lblWLPickType                             at    54 no-label
  s-wlpicktype                              at    62 no-label
    help
      "C)ounter Zone, W)hse Pick, or Blank (Counter and Whse)"
  s-ordertype                               colon 13    label "PO/WT/WO/VA#"
  s-orderaltno                              at    17 no-label
    help
        "The PO/WT/WO/VA# This Order Interfaces to Based on the Type [L]"
  v-pshpdt                                  at    25 no-label    
  s-prodcat                                 colon 43
     {f-help.i}
  s-taxablefl                               colon 13
  s-nontaxtype                              at    19 no-label
     help
         "Reason the Customer/Product is Not Taxable                 [L]"
  s-taxgroup                                at    22 no-label
     help
         "Tax Group (1 - 5; 1 for Standard Tax or 1 for Vertex)      [L]"
  s-slsrepout                               colon 43    label "Salesrep"
     help
         "Outside Sales Rep                                          [L]"
  s-slsrepin                                at    50 no-label
     help
         "Inside Sales Rep                                           [L]"
  s-rushfl                                  colon 13
     help
         "If 'Yes', Then Line Will be Treated as Rush Order"
  s-corecharge                              colon 43
  v-icdatclabel                             at     9 no-label
  s-datccost                                at    15 no-label
  s-termspct                                colon 43    label "Terms Disc"
  v-frzreblabel                             at     3 no-label
  s-frzrebty                                at    15 no-label
    help
        "Recalculate Rebate Through (E)nt, (O)rd, or (I)nv"
        validate(can-do("e,o,i",s-frzrebty),
        "Must be E, O, or I. (6421)")
  v-cost                                    at    27 no-label
  s-prodcost                                at    42 no-label
  
  /*****
  s-returnflc                               at    55 no-label
  s-ht                                      at    56 no-label
  */
  v-ratelabel                               at     5 no-label
  v-exchrate                                at    15 no-label
  v-excost                                  at    36 no-label 
  v-exprodcost                              at    43 no-label  
  /*
  v-excost                                  at    1  no-label
  v-exprodcost                              at    15 no-label
  v-ratelabel                               at    31 no-label
  v-exchrate                                at    43 no-label

  s-printpricefl                            colon 13    label
      "Print Opt" help "Print Prices on the Order Entry Prints"
  s-subtotalfl                              at    19 no-label
    help
        "Subtotal Prices After This Line on the Order Entry Prints"
  s-jitbl                                   colon 29   label   
    "LineBL?"                                                
  s-leadtm                                  colon 43    
  v-altwhse                                 at     4 no-label
  s-altwhse                                 at    14 no-label
  v-jittitle                                at    32 no-label
  v-reqshipdt                               at    42 no-label
  v-promisedt                               at    51 no-label
  v-lostbus                                 at     4 no-label
  v-disc                                    at    40 no-label
  s-discamt                                 at    42 no-label
  s-disctype                                at    55 no-label
  ******/
  /*v-returnable                              at    59 no-label     */
  s-leadtm                        colon  8 label "LeadTm"
  s-OLTDays                       colon 24 Label "OvrLT" 
  v-jittitle                                at    33 no-label
  v-promisedt                               at    43 no-label
  v-reqshipdt                               at    52 no-label
  v-pdtype                        colon  8 label "PDType"
  v-pdrecord                      colon 24 label "Rec#"     
  v-pdamt    format "zzzz9.9999"  colon 41 label "PDSetup"  
  v-pd$md                         at    53 no-label 
  x-qty                           colon  8 label "QtyBrk"
  x-price                         colon 24 label "Price"
  x-totnet                        colon 41 label "Net"
  x-date                          at    53 no-label
  
  /*v-pdbased                       at    58 no-label         */
  
  v-ldatelit                                at    34 no-label   
  v-lpromdt                                 at    44 no-label   
  v-lexpdt                                  at    53 no-label   

  /*"Domestic Net          Cost         Margin" at     3 */
  s-domlabel                                at     3 no-label
  s-netamt format "zzzzzzzz9.99-"           at     4 no-label
      v-rebatenet                           at    17 no-label
  s-cost   format "zzzzzzzz9.99-"           at    18 no-label
  s-returnfl                                at    31 no-label
  v-rebatecost                              at    32 no-label
  v-spacost                                 at    33 no-label
  s-marginamt                               at    35 no-label
  s-marginpct                               at    49 no-label
  "%"                                       at    57
with frame f-oeetle row /* 4 */ 2 side-labels overlay centered

  
