def input        parameter v-prod    like icsp.prod           no-undo.
def input        parameter v-quoteno as i format ">>>>>>>9"   no-undo.
def input        parameter v-lineno  like oeel.lineno         no-undo.
def input-output parameter v-whse    like icsw.whse           no-undo.
def output       parameter errorfl   as   logical             no-undo.

def     shared var g-cono            like oeehb.cono          no-undo.
def     shared var g-operinit        like oeehb.operinit      no-undo.


def            var kitcount          as i format "999"        no-undo.
def            var h-whse            like icsw.whse           no-undo.
def            var zsdi-confirm      as logical    init no    no-undo.

def buffer b-oeelb for oeelb.
def buffer b-icsp  for icsp.

find oeehb where oeehb.cono = g-cono and
                 oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
                 oeehb.seqno   = 1 and
                 oeehb.sourcepros = "Quote"
                 no-lock no-error.
if avail oeehb then
  assign h-whse = oeehb.whse.
assign errorfl = no.
  
find icsp where icsp.cono = g-cono and
                icsp.prod = v-prod
                no-lock no-error.
if avail icsp and icsp.kittype = "B" then
  do:
  /* Check if Banner Whse was already set by a previous line */
  assign kitcount = 0.
  for each b-oeelb where 
           b-oeelb.cono = g-cono and
           b-oeelb.batchnm = string(v-quoteno,">>>>>>>9") and
           b-oeelb.seqno   = 1
           no-lock:
    find b-icsp where b-icsp.cono = g-cono and
                      b-icsp.prod = b-oeelb.shipprod and
                      b-icsp.kittype = "B"
                      no-lock no-error.
    if avail b-icsp and (substr(b-oeelb.user4,1,4) <> h-whse)   /*or 
                         (substr(b-oeelb.user3,1,4) <> v-whse)) */ then
      assign kitcount = kitcount + 1.
  end. /* for each b-oeelb */
  if kitcount >= 1 then
    do:
    find oeehb where 
         oeehb.cono = g-cono and
         oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
         oeehb.seqno   = 1 and
         oeehb.sourcepros = "Quote"
         no-lock no-error.
    if avail oeehb /*and (not can-find(icsw where icsw.cono = g-cono and
                                                  icsw.prod = v-prod and
                                                  icsw.whse = oeehb.whse
                                                  no-lock))*/ then
      assign v-whse  = oeehb.whse
             errorfl = yes.
  end. /* kitcount >= 1 */
  else
    do:
    find icsw where icsw.cono = g-cono and
                    icsw.whse = v-whse and
                    icsw.prod = v-prod
                    no-lock no-error.
    if avail icsw then
      do:
      for each oeehb where 
               oeehb.cono = g-cono and
               oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
               oeehb.seqno   = 1 and
               oeehb.sourcepros = "Quote":
        assign oeehb.whse = v-whse
               h-whse     = v-whse.
      end.
      for each oeelk where oeelk.cono = g-cono and
               oeelk.ordertype = "b" and
               oeelk.orderno   = v-quoteno and
               oeelk.ordersuf  = 00 and
               oeelk.lineno    = v-lineno:
        find icsw where icsw.cono = g-cono and
                        icsw.whse = v-whse and
                        icsw.prod = oeelk.shipprod
                        no-lock no-error.
        assign oeelk.whse     = v-whse
               oeelk.arpwhse  = if icsw.arptype = "w" then icsw.arpwhse 
                                else ""
               oeelk.price    = if avail icsw then icsw.listprice 
                                else oeelk.price
               oeelk.prodcost = if avail icsw then 
                                  icsw.stndcost /*- oeelk.user6 */ /*das*/
                                else 
                                  oeelk.prodcost.
        overlay(oeelk.user4,1,4) = v-whse.
        
      end. /* each oeelk */
    end. /* avail icsw */
  end. /* kitcount < 1 */
end. /* avail icsp and icsp.kittype = "B" */
else
  do:
  find b-oeelb where b-oeelb.cono = g-cono and
                     b-oeelb.batchnm    = string(v-quoteno,">>>>>>>9") and
                     b-oeelb.seqno      = 1 and
                     b-oeelb.lineno     = v-lineno and
                     b-oeelb.specnstype = "n"
                     no-lock no-error.
  if avail b-oeelb then
    do:
    for each oeelk where oeelk.cono = g-cono and
             oeelk.ordertype = "b" and
             oeelk.orderno   = v-quoteno and
             oeelk.ordersuf  = 00 and
             oeelk.lineno    = v-lineno:
      find icsw where icsw.cono = g-cono and
                      icsw.whse = v-whse and
                      icsw.prod = oeelk.shipprod
                      no-lock no-error.
      if not avail icsw then
        do:
        run zsdimaincpy.p (input g-cono,
                           input " ",
                           input v-whse,
                           input oeelk.shipprod,
                           input v-lineno,
                           input-output zsdi-confirm).
        find first icsw where icsw.cono = g-cono and
                              icsw.whse = v-whse and
                              icsw.prod = oeelk.shipprod 
                              no-lock no-error.
        if not avail icsw then
          do:
          run oeexqICSW.p.
        end.
      end. /* not avail icsw */
      assign oeelk.whse = v-whse
             oeelk.arpwhse = if icsw.arptype = "w" then icsw.arpwhse 
                             else "".
      overlay(oeelk.user4,1,4) = v-whse.
    end. /* each oeelk */
  end. /* avail b-oeelb */
end. /* else */

