message "x-prodsrch.i". pause.

/* x-prodsrch.i 
*/
/* dkt */
def var qtyLabel like poelb.user7 no-undo.
def var cQty as char no-undo.      
def var extraData as char no-undo. 

/* REMOVED THE COMMNENT OUT */
define  shared var trustfl     as logical init no no-undo. 

def var v-printLblFlag as logical label "Print" no-undo.

define input-output parameter ioScanProd as char no-undo.


define input-output parameter sc-recid as recid no-undo. 
{g-xxex.i }
{g-ic.i  "new"}
{g-po.i  "new"}
{g-conv.i}
{g-jump.i}

def var v-keys         as c                  no-undo.
def var v-key          as c                  no-undo.
def var v-clearfl      as log                no-undo.
def var z-lastkey      as i                  no-undo.
def var z-loadcharfl   as log                no-undo.
def var v-start        as char format "x(24)"  no-undo.
def var v-editorfl     as logical            no-undo.
def var v-searchfl     as logical            no-undo.
def var v-srchfnd      as logical            no-undo.
def   shared var toggle-sw as logical   no-undo. 
def var zx-l as logical                   no-undo. 
def var zx-x as int                       no-undo.
def var srch-flg  as logical              no-undo. 
def var v-notsrch as logical              no-undo. 
def var sr-prod as character format "x(24)" no-undo. 
define var v-prodsrch as character format "x(24)"      no-undo.
define var v-prodsrcha as character format "x(24)"      no-undo.
define var v-prodsrchb as character format "x(24)"      no-undo.

define shared temp-table tpoehb no-undo 
    field pono       like poeh.pono 
    field posuf      like poeh.posuf
    field prod       like poel.shipprod
    field lineno     like poel.lineno format ">>>"
    field linecnt    as integer
    field qtyord     like poel.qtyord format ">>>>>>9.99"
    field qtyrcv     like poel.qtyrcv format ">>>>>>9.99"
    field pqtyrcv    like poel.qtyord format ">>>>>>9.99"
    field pstkqtyrcv like poel.qtyrcv format ">>>>>>9.99"
    field binloc     like icsw.binloc1
    field statustype like icsw.statustype 
    field whse       like poeh.whse
    field unit       like poel.unit
    field type       as integer format "9"    
    field lncd       as integer format "9"    
    field descrip    like icsp.descrip[1] 
    field vname      as char format "x(25)"
    field vendno     like icsw.arpvendno
    field vprod      like icsw.prod 
    field leadtm     like icsw.leadtmavg
    field nonstockty like poel.nonstockty
    field serlottype like icsw.serlottype
    field price      like poel.price
    field nosnlots   like poel.nosnlots
    field qtyunavail like poel.qtyunavail
    field reasunavty like poel.reasunavty
    field poelbid    as recid 
    field botyp      as char format "x"
    field slotty     as char format "x"
    field potyp      as char format "xx"
    field shipmentid like poehb.shipmentid
    field flglbl     as logical format "y/n"
    field cancelfl   as logical format "y/n"
    field notesfl1   as char format "x"
    field comfl      as l    format "c/ "
    field prodsrch   as character format "x(24)"
    field expshpdt   like poeh.reqshipdt
/* SDI DANFOSS 11/14/05    */
    field vendedi    like apsv.edipartner
/* SDI DANFOSS 11/14/05    */
    index k-temp1
          pono
          posuf
          lineno
          prod
    index k-temp2
          pono 
          posuf
          vprod
          lineno
    index k-temp3
          pono 
          posuf
          qtyrcv descending
    index k-temp4
          pono 
          posuf
          prodsrch
          lineno.

define    shared temp-table srch no-undo 
    field pono     as char format "x(10)" 
    field prod     like poel.shipprod
    field vprod    like poel.shipprod
    field lineno   like poel.lineno format ">>>"
    field ctype    as character format "x(1)"
    field srecid   as recid
    field type     as char format "x"
    field qtyord   as dec  format ">>>>>9.99"
    field qtyrcv   as dec  format ">>>>>9.99"
    field shipid   like poehb.shipmentid
    field expshpdt like poeh.reqshipdt
    index k-srch1
          prod
          expshpdt
          qtyord descending
          lineno ascending
    index k-srch2
          vprod 
          expshpdt
          qtyord descending
          lineno ascending.

define buffer b-srch for srch.

define temp-table sid no-undo 
    field pono     as char format "x(10)" 
    field posuf    like poeh.posuf
    field prod     like poel.shipprod
    field lineno   like poel.lineno format ">>>"
    field whse     like poeh.whse
    field unit     like poel.unit
    field type     as integer format "9"    
    field lncd     as integer format "9"    
    field poelbid  as recid 
    field botyp    as char format "x"
    field shipid   as char format "x(20)"
    field openinit like poehb.openinit
    field marked   as char format "x" 
    field tpoehbid as recid 
    field vendno   like icsw.arpvendno
/* SDI DANFOSS 11/14/05    */
    field vendedi  like apsv.edipartner
/* SDI DANFOSS 11/14/05    */


    index k-sid1
          pono
          posuf
          lineno
          prod
    index k-sid2
          marked
          pono
          posuf
          lineno
          prod.



/* SDI DANFOSS 11/14/05    */

define buffer z1-apsv for apsv.

/* SDI DANFOSS 11/14/05    */

define buffer b-sid  for sid.
define buffer b2-sid for sid.
def var sid-recno  as recid no-undo. 
def var sid-recrow as rowid no-undo. 

define temp-table plist no-undo 
    field pono     as char format "x(10)" 
    field posuf    like poeh.posuf
    field prod     like poel.shipprod
    field lineno   like poel.lineno format ">>>"
    field whse     like poeh.whse
    field unit     like poel.unit
    field type     as char format "xx"    
    field botyp    as char format "x"
    field shipid   as char format "x(20)"
    field openinit like poehb.openinit
    field marked   as character format "x"
    index k-plist
          pono
          whse
    index k-plist2
          marked
          pono
          whse.

define var v-titlelit   as char format "x(26)"          no-undo.

define var h as handle.
define var p-rowid as rowid.             
def var v-pono like poeh.pono             no-undo.
def var z-pono like poeh.pono             no-undo.
def var z-recid as recid                  no-undo.
def var v-shipid as char format "x(24)"   no-undo.
def var v-msg2   as char format "x(78)"   no-undo.
define var s-ibprinter  like sapb.printernm             no-undo.
define var o-ibprinter  like sapb.printernm             no-undo.
define var s-pckprinter like sapb.printernm             no-undo.
define var o-pckprinter like sapb.printernm             no-undo.
def  shared var v-saso-whse     like sasoo.whse no-undo.
def  shared var x-saso-whsegrp  as character    no-undo.
def  shared var x-f10-whse      like sasoo.whse   no-undo.
def  shared var v-vend-accum    like apsv.vendno  no-undo.
def  shared var v-vend-accum-edi    like apsv.edipartner  no-undo.

{po_defvars.i } 

{ {&appname} &user_vars = "*" }

mainlooper:
do while true on endkey undo mainlooper, leave mainlooper
              on error undo mainlooper,  leave mainlooper: 
  { {&appname} &srch_Query = "*" }

  if {k-cancel.i} or {k-jump.i} then do:
    assign sc-recid = ?.
  end.

  hide frame f-zzx.
  hide frame f-lookprod.
  hide frame f-lookprod2. 
  hide frame f-zpoinfo2.
  leave MainLooper.          
end. /* MainLooper */

{ {&appname} &user_procedures = "*" }

Procedure RFLoop: 
assign zx-l = no
       zx-x = 1.
repeat while program-name(zx-x) <> ?:         
  if program-name(zx-x) = "hher.p" then do:                                          zx-l = yes.                              
    leave.                             
  end.                                      
  assign zx-x = zx-x + 1.                     
end.                                          
end procedure.

/* UPC added code */
procedure improvise_product:
define input         parameter ipx-vendor  like apsv.vendno no-undo.
define input         parameter ipx-vendedi like apsv.edipartner no-undo.
define input         parameter ipx-field  as character     no-undo. 
define input-output parameter v-prodsrch as char format "x(24)" no-undo.

/* UPC MOD */
def buffer sv-apsv for apsv.

if ipx-vendor <> 0 then do:
  find sv-apsv where 
       sv-apsv.cono = g-cono and
       sv-apsv.vendno = ipx-vendor no-lock no-error.
end.
else do:
  find sv-apsv where 
       sv-apsv.cono = g-cono and
       sv-apsv.edipartner = ipx-vendedi no-lock no-error.
end.

if avail sv-apsv then do:
  dec(v-prodsrch) no-error.
  if error-status:error then do: 
    /* only integers can be upc numbers */
  end.
  else do:
    find icsv use-index k-produpc where 
         icsv.cono = g-cono and 
         icsv.vendno = sv-apsv.vendno and
         icsv.section3 = sv-apsv.vendno and
         icsv.section4 = dec(v-prodsrch) no-lock no-error.
    if avail icsv then do:
      assign v-prodsrch = icsv.prod.   
      return.
    end.
  end.
end.
/* ipx-field = product */
/* UPC MOD */


  v-prodsrch = replace (v-prodsrch," ","").
/*  v-prodsrch = replace (v-prodsrch,"s","5").
    v-prodsrch = replace (v-prodsrch,"o","0").
    v-prodsrch = replace (v-prodsrch,"i","1").
    v-prodsrch = replace (v-prodsrch,"l","1").
    v-prodsrch = replace (v-prodsrch,"z","2").
    v-prodsrch = replace (v-prodsrch,"v","u"). 
*/
  v-prodsrch = replace (v-prodsrch,"-","").
  v-prodsrch = replace (v-prodsrch,"/","").
  v-prodsrch = replace (v-prodsrch,"*","").
  v-prodsrch = replace (v-prodsrch,'"',"").
  v-prodsrch = replace (v-prodsrch,"x","").
  v-prodsrch = replace (v-prodsrch,"~~","").
  v-prodsrch = replace (v-prodsrch,"'","").
  v-prodsrch = replace (v-prodsrch,"(","").
  v-prodsrch = replace (v-prodsrch,")","").
  v-prodsrch = replace (v-prodsrch,"`","").
  v-prodsrch = replace (v-prodsrch,".","").
  v-prodsrch = replace (v-prodsrch,"#","").
  v-prodsrch = replace (v-prodsrch,"<","").
  v-prodsrch = replace (v-prodsrch,">","").
  v-prodsrch = replace (v-prodsrch,"~\","").
end procedure. 
/* UPC added code */


procedure vendor_special_information:
 
define input         parameter ipx-vendor  like apsv.vendno no-undo.
define input         parameter ipx-vendedi like apsv.edipartner no-undo.
define input         parameter ipx-field  as character     no-undo. 
define input-output  parameter ipx-fieldaval as character  no-undo.

/* ipx-field = product */

/* UPC MOD */
def buffer sv-apsv for apsv.

if ipx-vendor <> 0 then do:
  find sv-apsv where 
       sv-apsv.cono = g-cono and
       sv-apsv.vendno = ipx-vendor no-lock no-error.
end.
else do:
  find sv-apsv where 
       sv-apsv.cono = g-cono and
       sv-apsv.edipartner = ipx-vendedi no-lock no-error.
end.

if avail sv-apsv then do:
  dec(ipx-fieldaval) no-error.
  if error-status:error then do: 
    /* only integers can be upc numbers */
  end.
  else do:
     find icsv use-index k-produpc where 
         icsv.cono = g-cono and 
         icsv.section4 = dec(ioScanProd) and
         icsv.section3 = sv-apsv.vendno and
         icsv.vendno = sv-apsv.vendno 
         no-lock no-error.
    
    if avail icsv then do:
      assign ipx-fieldaval = icsv.prod.   
      return.
    end.
  end.
end.
/* ipx-field = product */
/* UPC MOD */


  find zsastz where zsastz.cono = g-cono and
                    zsastz.labelfl = false and
                    zsastz.codeiden = "VendorChars" and
                    zsastz.primarykey = 
                                  ( if ipx-vendedi <> "" then
                                       ipx-vendedi
                                    else   
                                      string(ipx-vendor)) no-lock no-error.
  if not avail zsastz then return.                    

  if avail zsastz then do:     


   def var new-fieldaval like ipx-fieldaval. 
  
     assign new-fieldaval = ipx-fieldaval. 
        /*  message "new-fieldaval" new-fieldaval.   */
  
  
    if ipx-field = "Product" and 
       substring(ipx-fieldaval,1,1) = substring(zsastz.codeval[7],1,1) then do:

      /*  message "found 7". pause.   */
    
      if ipx-fieldaval begins zsastz.codeval[7] then do:
        ipx-fieldaval = substring(ipx-fieldaval, length(zsastz.codeval[7]) + 1,
          length(ipx-fieldaval) - length(zsastz.codeval[7])).       
      
           /* message "the7" ipx-fieldaval. pause. */ 
      end.           
  end.
  
  else
  
  /*
  message "checkign"  substring(ipx-fieldaval,1,1)
                      substring(zsastz.codeval[8],1,2). pause.
  message "length" length(zsastz.codeval[8]) + 1   
          "senlen" length(ipx-fieldaval) - length(zsastz.codeval[8]). pause.
  */                    
                      
  if substring(ipx-fieldaval,1,2) = substring(zsastz.codeval[8],1,2) then 

/*  if ipx-fieldaval begins substring(zsastz.codeval[8],1,2) then  */

    assign 
    ipx-fieldaval = substring(ipx-fieldaval, length(zsastz.codeval[8]) + 1,
                    length(ipx-fieldaval) - length(zsastz.codeval[8])).       
 
        /*   message "ipx-fieldaval #8" ipx-fieldaval. pause.    */
        
        find first icsp where icsp.cono = g-cono and
                              icsp.prod = ipx-fieldaval
                              no-lock no-error.
                              
        if avail icsp then 
            message "". 
            /*
            message "found it". pause.   
            */
                              
        if not avail icsp then do:
            /*  message "notfound". pause.   */
           assign ipx-fieldaval =  new-fieldaval. 
            /*  message "new" new-fieldaval. pause.  */ 
           
          /* message "running3" ipx-field. pause.     */
        assign ipx-fieldaval = new-fieldaval. 
         /* message "after assign" ipx-fieldaval. pause.  */

        
  if substring(ipx-fieldaval,1,1) = substring(zsastz.codeval[3],1,1) then
     /* message "at 3??". pause.   */
     if ipx-fieldaval begins zsastz.codeval[3] then 
        ipx-fieldaval = substring(ipx-fieldaval, length(zsastz.codeval[3]) + 1,
          length(ipx-fieldaval) - length(zsastz.codeval[3])).       
 
        /*   message "ipx-fieldaval #3" ipx-fieldaval. pause.       */
         
        
 end. /* else do */
        
                
 end.               
                 
  
  if ipx-field = "PackSlip" and zsastz.codeval[5] <> "" then do:
    if substring(ipx-fieldaval,1,1) = substring(zsastz.codeval[5],1,1) then
      assign ipx-fieldaval = 
        substring(ipx-fieldaval,2,length(ipx-fieldaval) - 1).
    if substring(ipx-fieldaval,1,1) = substring(zsastz.codeval[5],2,1) and
      substring(zsastz.codeval[5],2,1) <> "" then
      assign ipx-fieldaval = 
        substring(ipx-fieldaval,2,length(ipx-fieldaval) - 1).
  end.      
end procedure. 




