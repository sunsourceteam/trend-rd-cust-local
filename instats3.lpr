/* instats3.lpr 1.1 01/03/98 */
/* instats3.lpr 1.1 08/16/97 */
/*h****************************************************************************
  INCLUDE      : instats3.lpr
  DESCRIPTION  : Static Extract - Sales Rep Gateway
  USED ONCE?   :
  AUTHOR       : jkp
  DATE WRITTEN : 08/16/97
  CHANGES MADE :
    08/16/97 jkp; TB# 23605 New Include.  Code taken from instatx3.p to allow
        the passing of the case parameter.
    11/06/97 jkp; TB# 23938 Do not allow question marks to be output.
    08/19/99 jkp; TB# 24005 Round/Trim all fields larger than the data
        warehouse field back to the data warehouse size.  This is due to SQL 
        7.0 tight typing.
******************************************************************************/

/*o All fields must be put out as character fields.  So if a field is numeric
    or is a date, it must use the "string" parameter to make it a character 
    field.  For those fields that truly are a character field, i.e., they 
    don't need the "string" parameter, they must have parameter 4 in front 
    of them to get them in the correct case.  In addition, all signed fields
    must have the sign on the left.  It is imperative that no question marks
    are output, so every field must be checked to make sure it is not a
    question mark, even though that would be corrupt data except for a date
    field.  This is because the analyzer process cannot handle question marks
    and it is SQL code, so it cannot even give a graceful error - it just
    aborts.  Some of these checks are made in instatx3.p and some are made 
    here. */

/* Passing parameters definition *********************************************
   {&case}     Selection of the case sensitivity.  This is passed by a group
               of if then else statements and is set to either "caps", "lc", 
               or " " depending on how the user answered the p-casety option 
               on the SASSR.
                    
   Sample include calls might be:
       {insaexs3.lpr 
           &case     = "caps"}     
           (Will output all character fields in all upper case letters)
       {insaexs3.lpr 
           &case     = "lc"}
           (Will output all character fields in all lower case letters)
       {insaexs3.lpr 
           &case     = " "}
           (Will output all character fields, without changing the case)
******************************************************************************/

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Sales Rep ID */
if smsn.slsrep <> ? then
    {&case}(smsn.slsrep)
else v-null
v-del
            
/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Sales Rep Parent ID */
if smsn.user1 <> ? and
   smsn.user1 <> "" then
    {&case}(smsn.user1) 
else
if smsn.slsrep <> ? and
   smsn.slsrep <> "" then
    {&case}(smsn.slsrep) 
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Sales Rep Name */
if smsn.name <> ? then
    {&case}(smsn.name) 
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*tb 24005 08/19/99 jkp; Trimmed addr[2] field to 29 characters to match data
     warehouse. */
/*e Sales Rep Address */
if smsn.addr[1] <> ? and smsn.addr[2] <> ? then
    {&case}(smsn.addr[1]) + " " + {&case}(substring(smsn.addr[2],1,29))
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Sales Rep City */
if smsn.city <> ? then
    {&case}(smsn.city)
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Sales Rep State */
if smsn.state <> ? then
    {&case}(smsn.state)
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Sales Rep Zip Code */
if smsn.zipcd <> ? then
    {&case}(smsn.zipcd)
else v-null
v-del

/*e Sales Rep Country */
{&case}("USA")
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Sales Rep Phone Number */
if smsn.phoneno <> ? then
    {&case}(smsn.phoneno)
else v-null
v-del

/*e Sales Rep Fax Number */
{&case}(v-null) 
v-del

/*e Territory */
{&case}(v-null) 
v-del

/*e Region */
{&case}(v-null) 
v-del

/*e Group Name */
{&case}(v-null)
v-del
           
/*e Site */
if smsn.site <> "" then
   {&case}(smsn.site)
else
   {&case}(v-null)
v-del
            
/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Sales Rep Type */
if smsn.slstype <> ? then
    {&case}(smsn.slstype)
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Manager */
(if smsn.mgr <> ? then
   if smsn.cono = 80 and substring(smsn.mgr,1,1) <> "I" then
     "I999"
   else  
     {&case}(smsn.mgr)
 else v-null)
v-del
             
/*e SX User 1 */
{&case}(if smsn.cono = 80 and substring(smsn.mgr,1,1) <> "I" then
           "I"
        else substring(smsn.mgr,1,1)   )
v-del

/*e SX User 2 */
{&case}(if smsn.cono = 80 and substring(smsn.mgr,1,1) <> "I" then
          "999"
        else
          substring(smsn.mgr,2,3)) 
v-del

/*e SX User 3 */
{&case}(string(smsn.sysname,"x(4)")) 
v-del

/*e SX User 4 */
{&case}(string(smsn.commtype,"x(4)")) 
v-del

/*e SX User 5 */
{&case}(v-null) 
v-del 
           
/*e SX User 6 */
{&case}(v-null) 
v-del

/*e SX User 7 */
{&case}(v-null) 
v-del

/*e SX User 8 */
{&case}(v-null) 
v-del

/*e SX User 9 */
string(v-zero) 
v-del

/*e SX User 10 */
string(v-zero) 
v-del

/*e Sales Rep E-Mail */
{&case}(v-null) 
           
CHR(13) CHR(10).
  

