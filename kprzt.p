
/* kprzt.p  7/16/2005 */
/*****************************************************************************
  PROCEDURE      : kprzt
  DESCRIPTION    : Kit Production - Work Order Activity Log Report 
  AUTHOR         : Sunsource
  DATE WRITTEN   : 7/16/05
  CHANGES MADE   :
*******************************************************************************/

{p-rptbeg.i} 
{g-kpstg2.i}

def new shared var w-month          as c    format "x(9)"       no-undo.
def new shared var w-date           as c    format "x(10)"      no-undo.
def new shared var work-date        as date format "99/99/9999" no-undo.
def new shared var b1-date          as date format "99/99/9999" no-undo.
def new shared var e1-date          as date format "99/99/9999" no-undo.
def new shared var ze1-date         as date format "99/99/9999" no-undo.
def new shared var b2-date          as date format "99/99/9999" no-undo.
def new shared var e2-date          as date format "99/99/9999" no-undo.
def new shared var beg-date         as date format "99/99/9999" no-undo.
def new shared var leapyr           as de   format "9999.99"    no-undo.
def new shared var leaptst          as c    format "x(7)"       no-undo.
def var p-perend                    as int                      no-undo.
def new shared var b-invdate        as date                     no-undo.
def new shared var e-invdate        as date                     no-undo.
def var b-techid                    as char format "x(4)"       no-undo. 
def var e-techid                    as char format "x(4)"       no-undo. 
def var b-wono                      like kpet.wono              no-undo.
def var e-wono                      like kpet.wono              no-undo.
def var b-whse                      like kpet.whse              no-undo.
def var e-whse                      like kpet.whse              no-undo.
def var l-date                      as date                     no-undo.
def var s-stageprt                  as c format "x(3)"          no-undo.
def var s-stageprt2                 as c format "x(3)"          no-undo.
def var d-wononbr                   as c format "x(10)"         no-undo.
def var d-wononbr2                  as c format "x(10)"         no-undo.
def var v-stagetext                 as c extent 10              no-undo initial
["","Ord","Prt","Blt"," "," "," "," "," ","Can"].
def var v-stgint                    as int format "99"          no-undo.
def var m-stgint                    as int format "99"          no-undo.
def var inx                         as integer                  no-undo.
def var inx2                        as integer                  no-undo.
def var t-ordqty                    as decimal                  no-undo.
def var t-sordqty                   as decimal                  no-undo.
def var t-tordqty                   as decimal                  no-undo.
def var t-wordqty                   as decimal                  no-undo.
def var t-gordqty                   as decimal                  no-undo.
def var t-dordqty                   as decimal                  no-undo.
def var t-qty                       as decimal                  no-undo.
def var t-tmout                     as int                      no-undo.
def var t-tmin                      as int                      no-undo.
def var t-time                      as decimal                  no-undo.
def var t-hrs                       as decimal                  no-undo. 
def var t-hrs-left                  as decimal                  no-undo. 
def var t-min                       as decimal                  no-undo.
def var prt-time                    as char format "999:99"     no-undo.
def var dtl-tstg-time               as char format "999:99"     no-undo.
def var dtl-tech-time               as char format "999:99"     no-undo.
def var dtl-date-time               as char format "999:99"     no-undo.
def var dtl-twono-time              as char format "999:99"     no-undo.
def var dtl-tgtl-time               as char format "999:99"     no-undo.
def var t-days                      as decimal                  no-undo.
def var t-days-prt                  as decimal                  no-undo.
def var t-days-tot                  as decimal                  no-undo.
def var dtl-time                    as decimal                  no-undo.
def var tstg-time                   as decimal                  no-undo.
def var tech-time                   as decimal                  no-undo.
def var date-time                   as decimal                  no-undo.
def var twono-time                  as decimal                  no-undo.
def var tgtl-time                   as decimal                  no-undo.
def var s-title                     as char format "x(79)"      no-undo.
def var v-name                      as char format "x(23)"      no-undo.
def var stg-flg                     as l                        no-undo.
def var date-flg                    as l                        no-undo.
def var detl-flg                    as l                        no-undo.
def var qhd-flg                     as l                        no-undo.
def var q-opn                       as l                        no-undo.
def var s-dateprt                   as date                     no-undo.
def var p-spaces                    as c format "x" init " "    no-undo.
 
assign b-techid = sapb.rangebeg[1] 
       e-techid = sapb.rangeend[1]
       b-whse   = sapb.rangebeg[2]
       e-whse   = sapb.rangebeg[2]
       b-wono   = int(substring(sapb.rangebeg[4],1,7))
       e-wono   = int(substring(sapb.rangeend[4],1,7))
       stg-flg  = if sapb.optvalue[1] = "yes" then yes 
                  else no
       date-flg = if sapb.optvalue[2] = "yes" then yes 
                  else no
       detl-flg = if sapb.optvalue[3] = "yes" then yes 
                  else no
       qhd-flg  = if sapb.optvalue[4] = "yes" then yes 
                  else no
       q-opn    = if sapb.optvalue[5] = "yes" then yes 
                  else no.

if e-whse = " " then     
   assign e-whse = "zzzz".
if e-wono = 0 then 
   assign e-wono = 9999999.
   
if sapb.rangebeg[3] ne "" then
    do:
    v-datein = sapb.rangebeg[3].
    {p-rptdt.i}
    if string(v-dateout) = v-lowdt then
      b-invdate = 01/01/1900.
    else  
      b-invdate = v-dateout.
    end.
else
  b-invdate = 01/01/1900.

if sapb.rangeend[3] ne "" then
    do:
    v-datein = sapb.rangeend[3].
    {p-rptdt.i}
    if string(v-dateout) = v-highdt then
      e-invdate = 12/31/2049.
    else
      e-invdate = v-dateout.
    end.
else
  e-invdate = 12/31/2049.

form header 
    s-title                 at 1 
with frame f-hdr1 width 80 no-box no-labels page-top.

assign s-title =
  "Tech    Wo No     Stg   Indate     Intime   Outdate   Outtime  Hrs:Min".

form header 
   "Begin: "            at 51
   b-invdate            at 58 format "99/99/9999"
   "End: "              at 70
   e-invdate            at 75 format "99/99/9999"
with frame f-dthdr width 130 no-box no-labels page-top.

define frame f-spaces 
   p-spaces             at 1 
   with down width 130 no-box no-labels.

define frame f-spaces2 
   p-spaces             at 1 
   with down width 130 no-box no-labels.

define frame f-detail 
   ztmk.techid          at 1
   d-wononbr            at 6 
   s-stageprt           at 19 
   ztmk.punchindt       at 25
   ztmk.punchintm       at 35 
   ztmk.punchoutdt      at 45 
   ztmk.punchouttm      at 55 format "xx:xx:xx"
   prt-time             at 64 format "999:99"
   with down width 130 no-box no-labels.

 define frame f-detail2 
   ztmk.techid          at 1
   d-wononbr            at 6 
   s-stageprt           at 19 
   ztmk.punchindt       at 25
   ztmk.punchintm       at 35 
   ztmk.punchoutdt      at 45 
   ztmk.punchouttm      at 55 format "xx:xx:xx"
   prt-time             at 64 format "999:99"
   with down width 130 no-box no-labels.
 
define frame f-stgtot 
   "Total Stage"        at 7 
    s-stageprt          at 19
    t-sordqty           at 24 
    dtl-tstg-time       at 35
    "<--Hrs:Min"        at 46 
    skip(1) 
    with width 80 no-box no-labels.

define frame f-stgtot2
   "Total Stage"        at 7 
    s-stageprt          at 19
    t-sordqty           at 24 
    dtl-tstg-time       at 35
    "<--Hrs:Min"        at 46 
    skip(1) 
    with width 80 no-box no-labels.

define frame f-wonotot 
    d-wononbr           at 6
    " - Totl"           at 16
    t-wordqty           at 24 
    dtl-twono-time      at 35
    "<--Hrs:Min - "     at 43 
    v-name              at 56
    with width 85 no-box no-labels.

define frame f-wonotot2 
    d-wononbr           at 6
    " - Totl"           at 16
    t-wordqty           at 24 
    dtl-twono-time      at 35
    "<--Hrs:Min - "     at 43 
    v-name              at 56
    with width 85 no-box no-labels.

define frame f-techtot 
    v-name              at 1 
    t-tordqty           at 24
    dtl-tech-time       at 35
    "<--Hrs:Min - "     at 43 
    ztmk.techid         at 56
    skip(1) 
    with width 85 no-box no-labels.

define frame f-techtot2 
    v-name              at 1 
    t-tordqty           at 24
    dtl-tech-time       at 35
    "<--Hrs:Min - "     at 43
    ztmk.techid         at 56
    skip(1) 
    with width 85 no-box no-labels.

define frame f-datetot 
   "Date Total"         at 1 
    s-dateprt           at 12
    "Days-->"           at 23 
    t-days-prt          at 31 
    dtl-date-time       at 35
    "<--Hrs:Min"        at 43 
    skip(1) 
    "-------------------------------------------------------------------------"
    with width 80 no-box no-labels.

define frame f-gtltot 
   "Final Total"        at 7 
    "Days-->"           at 23 
    t-days-prt          at 31 
    dtl-tgtl-time       at 35
    "<--Hrs:Min"        at 43 
    skip(1) 
    with width 80 no-box no-labels.

view frame f-dthdr.  

if qhd-flg then 
do: 
   run qhd-psh. 
   leave.
end.   

if date-flg then 
do l-date = b-invdate to e-invdate: 
   view frame f-hdr1. 
   for each ztmk use-index k-techin where
            ztmk.cono      = g-cono       and 
          ((ztmk.punchindt >= b-invdate   and 
            ztmk.punchindt <= e-invdate)  and 
            ztmk.punchoutdt ne ?)         and 
           (ztmk.techid    >= b-techid    and 
            ztmk.techid    <= e-techid)   and 
           (ztmk.orderno   >= b-wono      and 
            ztmk.orderno   <= e-wono)     and 
            ztmk.statuscd   = 5            
            no-lock              
            break by ztmk.cono 
                  by ztmk.punchindt
                  by ztmk.techid
                  by ztmk.orderno 
                  by ztmk.ordersuf
                  by ztmk.stagecd: 
       find first kpet where
                  kpet.cono  = ztmk.cono    and 
                  kpet.wono  = ztmk.orderno and 
                  kpet.wosuf = ztmk.ordersuf 
                  no-lock no-error.
       if avail kpet and 
         (kpet.whse < b-whse or 
          kpet.whse > e-whse) then 
          next.
       if qhd-flg and 
         (ztmk.stagecd ne 7 and ztmk.stagecd ne 8) then 
          next.         
       if (ztmk.stagecd ne 1 and 
           ztmk.stagecd ne 2 and 
           ztmk.stagecd ne 7 and 
           ztmk.stagecd ne 8 and not qhd-flg) or  
         ((ztmk.stagecd = 7  or 
           ztmk.stagecd = 8) and qhd-flg) then 
          assign t-days      =  ztmk.punchoutdt - ztmk.punchindt  
                 t-tmout     = (dec(substring(ztmk.punchouttm,1,2)) * 60) +
                                dec(substring(ztmk.punchouttm,3,2))
                 t-tmin      = (dec(substring(ztmk.punchintm,1,2)) * 60) + 
                                dec(substring(ztmk.punchintm,3,2))
                 t-time      = if t-days = 0 then 
                                  t-tmout - t-tmin
                               else 
                                 (t-tmout - t-tmin) + (t-days + (24 * 60)).
       assign t-ordqty    = t-ordqty  + 1
              t-dordqty   = t-dordqty + 1
              t-tordqty   = t-tordqty + 1
              t-sordqty   = t-sordqty + 1
              t-wordqty   = t-wordqty + 1
              t-gordqty   = t-gordqty + 1.
       do inx2  = 1 to 11: 
       if inx2 = ztmk.stagecd then 
        do:  
            assign s-stageprt = v-stage2[inx2].
            leave.
        end.
       end.
       assign d-wononbr = string(ztmk.orderno,"9999999") + "-" + 
                          string(ztmk.ordersuf,"99")
              dtl-time  = if (ztmk.stagecd = 1  or 
                              ztmk.stagecd = 2) or  
                            ((ztmk.stagecd = 7  or 
                              ztmk.stagecd = 8) and not qhd-flg) then 
                             0 
                          else 
                              (t-time / 60).             
       assign tstg-time = tstg-time + dtl-time.
       if dtl-time > 0 then                     
          assign t-hrs = t-hrs + truncate(dtl-time / 1,0)
                 t-min = t-min + (60 * (dtl-time - truncate(dtl-time / 1,0))). 
       else 
          assign t-hrs = 0 
                 t-min = 0.
       assign substring(prt-time,1,3) = string(t-hrs,"zz9")
              substring(prt-time,4,2) = string(t-min,"99")
              dtl-time = 0 
              t-hrs    = 0 
              t-min    = 0.
       if detl-flg then 
          display ztmk.techid d-wononbr s-stageprt
                  ztmk.punchindt  ztmk.punchintm 
                  ztmk.punchoutdt ztmk.punchouttm 
                  prt-time 
                  with frame f-detail.
                  down with frame f-detail.
       if last-of(ztmk.stagecd) then 
       do: 
          assign twono-time = twono-time + tstg-time. 
          if tstg-time > 0 then 
             assign t-hrs = t-hrs + truncate(tstg-time / 1,0)
                    t-min = t-min + 
                          (60 * (tstg-time - truncate(tstg-time / 1,0))).
          else 
             assign t-hrs = 0 
                    t-min = 0. 
          assign substring(dtl-tstg-time,1,3) = string(t-hrs,"zz9")
                 substring(dtl-tstg-time,4,2) = string(t-min,"99").
          if stg-flg = yes then 
             display s-stageprt t-sordqty dtl-tstg-time 
                     with frame f-stgtot. 
          assign t-sordqty   = 0 
                 tstg-time   = 0
                 t-ordqty    = 0
                 t-hrs       = 0 
                 t-min       = 0.  
       end. 
       if last-of(ztmk.ordersuf) then 
       do: 
          assign tech-time  = tech-time + twono-time. 
          if twono-time > 0 then 
             assign t-hrs = t-hrs + truncate(twono-time / 1,0)
                    t-min = t-min + 
                          (60 * (twono-time - truncate(twono-time / 1,0))).
          else 
             assign t-hrs = 0 
                    t-min = 0.       
          assign substring(dtl-twono-time,1,3) = string(t-hrs,"zz9")
                 substring(dtl-twono-time,4,2) = string(t-min,"99").
          {w-smsn.i ztmk.techid no-lock}
           v-name = if avail smsn then
                       smsn.name
                    else 
                       "Tech Name Notfound".
          display d-wononbr t-wordqty dtl-twono-time v-name
                  with frame f-wonotot. 
          assign t-wordqty   = 0 
                 twono-time  = 0
                 t-hrs       = 0 
                 t-min       = 0.  
          if detl-flg then 
             display p-spaces with frame f-spaces. 
       end. 
       if last-of(ztmk.techid) then 
       do: 
          assign date-time = date-time + tech-time.
          if tech-time > 0 then 
             assign t-hrs = t-hrs + truncate(tech-time / 1,0)
                    t-min = t-min + 
                          (60 * (tech-time - truncate(tech-time / 1,0))).
          else 
             assign t-hrs = 0              
                    t-min = 0.
          assign substring(dtl-tech-time,1,3) = string(t-hrs,"zz9")
                 substring(dtl-tech-time,4,2) = string(t-min,"99").
          {w-smsn.i ztmk.techid no-lock}
           v-name = if avail smsn then
                       smsn.name
                    else 
                       "Tech Name Notfound".
          if not detl-flg then 
             display p-spaces with frame f-spaces. 
          display t-tordqty dtl-tech-time v-name ztmk.techid
                  with frame f-techtot. 
          assign t-tordqty   = 0 
                 tech-time   = 0
                 t-hrs       = 0 
                 t-min       = 0.  
       end. 
       if last-of(ztmk.punchindt) then 
       do: 
          assign tgtl-time = tgtl-time + date-time. 
          if date-time > 0 then 
             assign t-hrs = t-hrs + truncate(date-time / 1,0)
                    t-min = t-min + 
                           (60 * (date-time - truncate(date-time / 1,0))).
          else 
              assign t-hrs = 0 
                     t-min = 0.
          assign t-days-tot = t-hrs 
                 t-days-prt = truncate(t-days-tot / 8, 0)
                 t-hrs-left = truncate(t-days-tot - (t-days-prt * 8),0).
          assign substring(dtl-date-time,1,3) =
                           string(t-hrs-left,"z99") 
                 substring(dtl-date-time,4,2) = string(t-min,"99")
                 s-dateprt   = ztmk.punchindt. 
          display t-days-prt format "zz9" dtl-date-time s-dateprt 
                  with frame f-datetot. 
          assign t-dordqty   = 0 
                 date-time   = 0
                 t-hrs       = 0 
                 t-min       = 0.
       end. 
   end.  
 if tgtl-time > 0 then 
    assign t-hrs = t-hrs + truncate(tgtl-time / 1,0)
           t-min = t-min + 
                      (60 * (tgtl-time - truncate(tgtl-time / 1,0))).
 else 
    assign t-hrs = 0 
           t-min = 0. 
 assign t-days-tot = t-hrs 
        t-days-prt = truncate(t-days-tot / 8, 0)
        t-hrs-left = truncate(t-days-tot - (t-days-prt * 8),0).
 assign substring(dtl-tgtl-time,1,3) = string(t-hrs-left,"z99") 
        substring(dtl-tgtl-time,4,2) = string(t-min,"99").
 display t-days-prt format "zz9" dtl-tgtl-time with frame f-gtltot. 
 leave.
end. /* do l-date */
else  
do l-date = b-invdate to e-invdate: 
   view frame f-hdr1. 
   for each ztmk use-index k-techin where
            ztmk.cono      = g-cono       and 
          ((ztmk.punchindt >= b-invdate   and 
            ztmk.punchindt <= e-invdate)  and 
            ztmk.punchoutdt ne ?)         and 
           (ztmk.techid    >= b-techid    and 
            ztmk.techid    <= e-techid)   and 
           (ztmk.orderno   >= b-wono      and 
            ztmk.orderno   <= e-wono)     and 
            ztmk.statuscd   = 5 
            no-lock
            break by ztmk.cono 
                  by ztmk.techid
                  by ztmk.orderno 
                  by ztmk.ordersuf
                  by ztmk.stagecd
                  by ztmk.punchindt:

       find first kpet where
                  kpet.cono  = ztmk.cono    and 
                  kpet.wono  = ztmk.orderno and 
                  kpet.wosuf = ztmk.ordersuf 
                  no-lock no-error.
       if avail kpet and 
         (kpet.whse < b-whse or 
          kpet.whse > e-whse) then 
          next.        
        
       if qhd-flg and 
         (ztmk.stagecd ne 7 and ztmk.stagecd ne 8) then 
          next.         
       if (ztmk.stagecd ne 1 and 
           ztmk.stagecd ne 2 and 
           ztmk.stagecd ne 7 and 
           ztmk.stagecd ne 8 and not qhd-flg) or  
         ((ztmk.stagecd = 7  or 
           ztmk.stagecd = 8) and qhd-flg) then 
          assign t-days      =  ztmk.punchoutdt - ztmk.punchindt  
                 t-tmout     = (dec(substring(ztmk.punchouttm,1,2)) * 60) +
                                dec(substring(ztmk.punchouttm,3,2)) 
                 t-tmin      = (dec(substring(ztmk.punchintm,1,2)) * 60) + 
                                dec(substring(ztmk.punchintm,3,2))
                 t-time      = if t-days = 0 then 
                                  t-tmout - t-tmin
                               else 
                               (t-tmout - t-tmin) + (t-days + (24 * 60)).  
       assign t-ordqty    = t-ordqty  + 1
              t-dordqty   = t-dordqty + 1
              t-tordqty   = t-tordqty + 1
              t-sordqty   = t-sordqty + 1
              t-wordqty   = t-wordqty + 1
              t-gordqty   = t-gordqty + 1.
       do inx2  = 1 to 11: 
        if inx2 = ztmk.stagecd then 
         do:  
             assign s-stageprt = v-stage2[inx2].
             leave.
         end.
       end.
       assign d-wononbr = string(ztmk.orderno,"9999999") + "-" + 
                          string(ztmk.ordersuf,"99")
              dtl-time  = if (ztmk.stagecd = 1  or 
                              ztmk.stagecd = 2) or  
                            ((ztmk.stagecd = 7  or 
                              ztmk.stagecd = 8) and not qhd-flg) then 
                             0 
                          else 
                              (t-time / 60).   
       assign tstg-time = tstg-time + dtl-time.
       if dtl-time > 0 then                     
          assign t-hrs = t-hrs + truncate(dtl-time / 1,0)
                 t-min = t-min +
                       (60 * (dtl-time - truncate(dtl-time / 1,0))). 
       else 
          assign t-hrs = 0 
                 t-min = 0.
       assign substring(prt-time,1,3) = string(t-hrs,"zz9")
              substring(prt-time,4,2) = string(t-min,"99")
              dtl-time = 0
              t-hrs    = 0 
              t-min    = 0.
       if detl-flg then 
          display ztmk.techid d-wononbr s-stageprt
                  ztmk.punchindt  ztmk.punchintm 
                  ztmk.punchoutdt ztmk.punchouttm  
                  prt-time 
                  with frame f-detail2.
                  down with frame f-detail2.
       if last-of(ztmk.stagecd) then 
       do: 
          assign twono-time = twono-time + tstg-time. 
          if tstg-time > 0 then 
             assign t-hrs = t-hrs + truncate(tstg-time / 1,0)
                    t-min = t-min + 
                          (60 * (tstg-time - truncate(tstg-time / 1,0))).
          else 
             assign t-hrs = 0 
                    t-min = 0. 
          assign substring(dtl-tstg-time,1,3) = string(t-hrs,"zz9")
                 substring(dtl-tstg-time,4,2) = string(t-min,"99").
          if stg-flg = yes then 
             display s-stageprt t-sordqty dtl-tstg-time
                     with frame f-stgtot2. 
          assign t-sordqty   = 0 
                 tstg-time   = 0
                 t-ordqty    = 0
                 t-hrs       = 0 
                 t-min       = 0.
       end. 
       if last-of(ztmk.ordersuf) then 
       do: 
          assign tech-time = tech-time + twono-time.
          if twono-time > 0 then 
             assign t-hrs = t-hrs + truncate(twono-time / 1,0)
                    t-min = t-min + 
                          (60 * (twono-time - truncate(twono-time / 1,0))).
          else 
             assign t-hrs = 0 
                    t-min = 0.       
          assign substring(dtl-twono-time,1,3) = string(t-hrs,"zz9")
                 substring(dtl-twono-time,4,2) = string(t-min,"99").
          {w-smsn.i ztmk.techid no-lock}
           v-name = if avail smsn then
                       smsn.name
                    else 
                       "Tech Name Notfound".
          display d-wononbr t-wordqty dtl-twono-time v-name
                  with frame f-wonotot2. 
          assign t-wordqty   = 0 
                 twono-time  = 0
                 t-hrs       = 0 
                 t-min       = 0.
          if detl-flg then 
             display p-spaces with frame f-spaces2. 
       end. 
       if last-of(ztmk.techid) then 
       do: 
          tgtl-time = tgtl-time + tech-time.
          if tech-time > 0 then 
             assign t-hrs = t-hrs + truncate(tech-time / 1,0)
                    t-min = t-min + 
                          (60 * (tech-time - truncate(tech-time / 1,0))).
          else 
             assign t-hrs = 0              
                    t-min = 0.
          assign substring(dtl-tech-time,1,3) = string(t-hrs,"zz9")
                 substring(dtl-tech-time,4,2) = string(t-min,"99").
          {w-smsn.i ztmk.techid no-lock}
           v-name = if avail smsn then
                       smsn.name
                    else 
                       "Tech Name Notfound".
          if not detl-flg then 
             display p-spaces with frame f-spaces2. 
          display t-tordqty dtl-tech-time v-name ztmk.techid
                  with frame f-techtot2. 
          assign t-tordqty   = 0 
                 tech-time   = 0
                 t-hrs       = 0
                 t-min       = 0.
       end.
   end.  /* for each ztmk */
 if tgtl-time > 0 then 
    assign t-hrs = t-hrs + truncate(tgtl-time / 1,0)
           t-min = t-min + 
                      (60 * (tgtl-time - truncate(tgtl-time / 1,0))).
 else 
    assign t-hrs = 0 
           t-min = 0. 
 assign t-days-tot = t-hrs 
        t-days-prt = truncate(t-days-tot / 8, 0)
        t-hrs-left = truncate(t-days-tot - (t-days-prt * 8),0).
 assign substring(dtl-tgtl-time,1,3) = string(t-hrs-left,"z99") 
        substring(dtl-tgtl-time,4,2) = string(t-min,"99").
 display t-days-prt format "zz9" dtl-tgtl-time with frame f-gtltot. 
 leave.
end. /* do l-date */

procedure qhd-psh: 
do l-date = b-invdate to e-invdate: 
   view frame f-hdr1. 
   for each ztmk use-index k-techin where
            ztmk.cono      = g-cono       and 
           (ztmk.punchindt >= b-invdate   and 
            ztmk.punchindt <= e-invdate)  and 
           (ztmk.techid    >= b-techid    and 
            ztmk.techid    <= e-techid)   and 
           (ztmk.orderno   >= b-wono      and 
            ztmk.orderno   <= e-wono)     and 
            ztmk.statuscd   = (if q-opn then 
                                1
                               else 
                                5)        and   
           (ztmk.stagecd    = 7 or 
            ztmk.stagecd    = 8)
            no-lock              
            break by ztmk.cono 
                  by ztmk.punchindt
                  by ztmk.techid
                  by ztmk.orderno 
                  by ztmk.ordersuf
                  by ztmk.stagecd: 
       find first kpet where
                  kpet.cono  = ztmk.cono    and 
                  kpet.wono  = ztmk.orderno and 
                  kpet.wosuf = ztmk.ordersuf 
                  no-lock no-error.
       if avail kpet and 
         (kpet.whse < b-whse or 
          kpet.whse > e-whse) then 
          next.
       if qhd-flg and 
         (ztmk.stagecd ne 7 and ztmk.stagecd ne 8) then 
          next.         
       if (ztmk.stagecd ne 1 and 
           ztmk.stagecd ne 2 and 
           ztmk.stagecd ne 7 and 
           ztmk.stagecd ne 8 and not qhd-flg) or  
         ((ztmk.stagecd = 7  or 
           ztmk.stagecd = 8) and qhd-flg) then 
          assign t-days      =  ztmk.punchoutdt - ztmk.punchindt  
                 t-tmout     = (dec(substring(ztmk.punchouttm,1,2)) * 60) +
                                dec(substring(ztmk.punchouttm,3,2))
                 t-tmin      = (dec(substring(ztmk.punchintm,1,2)) * 60) + 
                                dec(substring(ztmk.punchintm,3,2))
                 t-time      = if t-days = 0 then 
                                  t-tmout - t-tmin
                               else 
                                 (t-tmout - t-tmin) + (t-days + (24 * 60)).
       assign t-ordqty    = t-ordqty  + 1
              t-dordqty   = t-dordqty + 1
              t-tordqty   = t-tordqty + 1
              t-sordqty   = t-sordqty + 1
              t-wordqty   = t-wordqty + 1
              t-gordqty   = t-gordqty + 1.
       do inx2  = 1 to 11: 
        if inx2 = ztmk.stagecd then do:  
         assign s-stageprt = v-stage2[inx2].
         leave.
        end.
       end.
       assign d-wononbr = string(ztmk.orderno,"9999999") + "-" + 
                          string(ztmk.ordersuf,"99")
              dtl-time  = if (ztmk.stagecd = 1  or 
                              ztmk.stagecd = 2) or  
                            ((ztmk.stagecd = 7  or 
                              ztmk.stagecd = 8) and not qhd-flg) then 
                             0 
                          else 
                              (t-time / 60).             
       assign tstg-time = tstg-time + dtl-time.
       if dtl-time > 0 then                     
          assign t-hrs = t-hrs + truncate(dtl-time / 1,0)
                 t-min = t-min + (60 * (dtl-time - truncate(dtl-time / 1,0))). 
       else 
          assign t-hrs = 0 
                 t-min = 0.
       assign substring(prt-time,1,3) = string(t-hrs,"zz9")
              substring(prt-time,4,2) = string(t-min,"99")
              dtl-time = 0 
              t-hrs    = 0 
              t-min    = 0.
       if detl-flg then 
          display ztmk.techid d-wononbr s-stageprt
                  ztmk.punchindt  ztmk.punchintm 
                  ztmk.punchoutdt ztmk.punchouttm 
                  prt-time 
                  with frame f-detail.
                  down with frame f-detail.
       if last-of(ztmk.stagecd) then 
       do: 
          assign twono-time = twono-time + tstg-time. 
          if tstg-time > 0 then 
             assign t-hrs = t-hrs + truncate(tstg-time / 1,0)
                    t-min = t-min + 
                          (60 * (tstg-time - truncate(tstg-time / 1,0))).
          else 
             assign t-hrs = 0 
                    t-min = 0. 
          assign substring(dtl-tstg-time,1,3) = string(t-hrs,"zz9")
                 substring(dtl-tstg-time,4,2) = string(t-min,"99").
          if stg-flg = yes then 
             display s-stageprt t-sordqty dtl-tstg-time 
                     with frame f-stgtot. 
          assign t-sordqty   = 0 
                 tstg-time   = 0
                 t-ordqty    = 0
                 t-hrs       = 0 
                 t-min       = 0.  
       end. 
       if last-of(ztmk.ordersuf) then 
       do: 
          assign tech-time  = tech-time + twono-time. 
          if twono-time > 0 then 
             assign t-hrs = t-hrs + truncate(twono-time / 1,0)
                    t-min = t-min + 
                          (60 * (twono-time - truncate(twono-time / 1,0))).
          else 
             assign t-hrs = 0 
                    t-min = 0.       
          assign substring(dtl-twono-time,1,3) = string(t-hrs,"zz9")
                 substring(dtl-twono-time,4,2) = string(t-min,"99").
          {w-smsn.i ztmk.techid no-lock}
           v-name = if avail smsn then
                       smsn.name
                    else 
                       "Tech Name Notfound".
          display d-wononbr t-wordqty dtl-twono-time v-name
                  with frame f-wonotot. 
          assign t-wordqty   = 0 
                 twono-time  = 0
                 t-hrs       = 0 
                 t-min       = 0.  
          if detl-flg then 
             display p-spaces with frame f-spaces. 
       end. 
       if last-of(ztmk.techid) then 
       do: 
          assign date-time = date-time + tech-time.
          if tech-time > 0 then 
             assign t-hrs = t-hrs + truncate(tech-time / 1,0)
                    t-min = t-min + 
                          (60 * (tech-time - truncate(tech-time / 1,0))).
          else 
             assign t-hrs = 0              
                    t-min = 0.
          assign substring(dtl-tech-time,1,3) = string(t-hrs,"zz9")
                 substring(dtl-tech-time,4,2) = string(t-min,"99").
          {w-smsn.i ztmk.techid no-lock}
           v-name = if avail smsn then
                       smsn.name
                    else 
                       "Tech Name Notfound".
          if not detl-flg then 
             display p-spaces with frame f-spaces. 
          display t-tordqty dtl-tech-time v-name ztmk.techid
                  with frame f-techtot. 
          assign t-tordqty   = 0 
                 tech-time   = 0
                 t-hrs       = 0 
                 t-min       = 0.  
       end. 
       if last-of(ztmk.punchindt) then 
       do: 
          assign tgtl-time = tgtl-time + date-time. 
          if date-time > 0 then 
             assign t-hrs = t-hrs + truncate(date-time / 1,0)
                    t-min = t-min + 
                           (60 * (date-time - truncate(date-time / 1,0))).
          else 
              assign t-hrs = 0 
                     t-min = 0.
          assign t-days-tot = t-hrs 
                 t-days-prt = truncate(t-days-tot / 8, 0)
                 t-hrs-left = truncate(t-days-tot - (t-days-prt * 8),0).
          assign substring(dtl-date-time,1,3) =
                           string(t-hrs-left,"z99") 
                 substring(dtl-date-time,4,2) = string(t-min,"99")
                 s-dateprt   = ztmk.punchindt. 
          display t-days-prt format "zz9" dtl-date-time s-dateprt 
                  with frame f-datetot. 
          assign t-dordqty   = 0 
                 date-time   = 0
                 t-hrs       = 0 
                 t-min       = 0.
       end. 
   end.  
 if tgtl-time > 0 then 
    assign t-hrs = t-hrs + truncate(tgtl-time / 1,0)
           t-min = t-min + 
                      (60 * (tgtl-time - truncate(tgtl-time / 1,0))).
 else 
    assign t-hrs = 0 
           t-min = 0. 
 assign t-days-tot = t-hrs 
        t-days-prt = truncate(t-days-tot / 8, 0)
        t-hrs-left = truncate(t-days-tot - (t-days-prt * 8),0).
 assign substring(dtl-tgtl-time,1,3) = string(t-hrs-left,"z99") 
        substring(dtl-tgtl-time,4,2) = string(t-min,"99").
 display t-days-prt format "zz9" dtl-tgtl-time with frame f-gtltot. 
 leave.
end.
end.

