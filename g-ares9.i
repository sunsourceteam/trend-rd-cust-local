/* g-ares9.i 1.6 6/25/93 */
/*h*****************************************************************************
  INCLUDE               : g-ares9.i
  DESCRIPTION           : All variables and forms for ARES*1.p
  USED ONLY ONCE?       :
  AUTHOR                :
  DATE WRITTEN          :
  CHANGES MADE AND DATE : 07/13/92 pap; TB# 7191 - Customer # change
                                        Use report.c12 instead of report.de12d0
                                        for customer #
                          tb 11968 06/24/93 jrg; Added variables for the period
                                        balances to contain SVs and Credits when
                                        the Special Transaction flag is set.
*******************************************************************************/
{g-ares.i}

def buffer  b-report    for report.

def var v-addr          as c format "x(35)"     no-undo extent 4.
def var v-continued     as c format "x(24)"     no-undo.
def var v-credit        like aret.amount        no-undo.
def var v-charge        like aret.amount        no-undo.
def var p-stmtdt2       like p-stmtdt           no-undo.

/*tb 7191 07/13/92 pap; Customer # change - use c12 instead of de12d0 for cust
def var v-de12d0        like report.de12d0      no-undo.                      */
def var v-c12           like report.c12         no-undo.

def var v-totbal        like arsc.futbal        no-undo.
def var v-invoice       as c format "x(15)"     no-undo.
def var v-subtot        like v-totbal           no-undo.
def var v-faxfl         as logical              no-undo.

/********** All Heading Variables *********************/
def var h-remitto       as c format "x(9)"      no-undo.
def var h-custprt       as c format "x(20)"     no-undo.
def var h-caninv        as c format "x(15)"     no-undo.
def var h-callmsg       as c format "x(60)"     no-undo.
def var h-coname        like sasc.name          no-undo.
def var h-coaddr        like sasc.addr          no-undo.
def var h-cocitystzip   as c format "x(35)"     no-undo.
def var h-totdue        as c format "x(9)"      no-undo.
def var h-custno        as c format "x(11)"     no-undo.
def var h-custno2       as c format "x(11)"     no-undo.
def var h-stmtdt        as c format "x(14)"     no-undo.
def var h-stmtdt2       as c format "x(14)"     no-undo.
def var h-amtpaid       as c format "x(11)"     no-undo.
def var h-amtpaidln     as c format "x(18)"     no-undo.
def var h-remit1        as c format "x(26)"     no-undo.
def var h-remit2        as c format "x(26)"     no-undo.
def var h-duedt         as c format "x(8)"      no-undo.
def var h-headingln     as c format "x(80)"     no-undo.
def var h-uline         as c format "x(80)"     no-undo.
def var h-footerln1     as c format "x(80)"     no-undo.
def var h-footerln2     as c format "x(80)"     no-undo.

/*tb 11968 06/24/93 jrg; Added variables for the period balances to contain SVs
                and Credits when the Special Transaction flag is set. */
def var h-footerln2b    as c format "x(80)"     no-undo.
def var h-footerln2s    as c format "x(80)"     no-undo.
def var h-footerln2m    as c format "x(80)"     no-undo.
def var h-footerln2n    as c format "x(80)"     no-undo.
def var s-periodbal     like arsc.periodbal     no-undo.
def var s-servchgbal    like arsc.servchgbal                     no-undo.
def var s-misccrbal     like arsc.misccrbal                      no-undo.
def var v-tempbal       like aret.amount                         no-undo.
def var v-tempmc        like aret.amount     extent 5 initial 0  no-undo.
def var v-tempsc        like aret.amount     extent 5 initial 0  no-undo.
def var v-tempboth      like aret.amount     extent 5 initial 0  no-undo.
def var v-tempfut       like aret.amount                         no-undo.
def var s-futbal        like arsc.futinvbal                      no-undo.
def var s-perlastdt     as date extent 5 format "99/99/99"       no-undo.
def var s-perfirstdt    as date extent 5 format "99/99/99"       no-undo.
