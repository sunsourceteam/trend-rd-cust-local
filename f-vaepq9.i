/* f-oeepa1.i 1.6 04/16/93 */
/*h*****************************************************************************
  INCLUDE      : f-oeepa1.i
  DESCRIPTION  : Forms & defines for oeepa1 and oeepaf1
  USED ONCE?   : No (oeepa1.p & oeepaf1.p)
  AUTHOR       :
  DATE WRITTEN : 04/03/92
  CHANGES MADE :
    04/03/92 pap; TB#  6219 Line DO - add DO line indicator
    11/11/92 mms; TB#  8561 Display product surcharge label from ICAO
    04/08/93 jlc; TB# 10155 Display customer products.
    04/13/93 jlc; TB#  7245 Incorrect backorder qty.
    11/01/95 gp;  TB# 18091 Expand UPC Number (T24).  Add s-upcpno.
    03/20/96 kr;  TB# 12406 Variables defined in form, moved to calling programs
    04/17/97 JPH; ITB# 1    Remove upc item, qty b/o qty shpd from detail.
*******************************************************************************/

{f-blank.i}

/* detail line and prod descrip */
form  
    s-lineno                             at   1
    oeel.shipprod                        at   5
    s-qtyord                             at  38
    s-qtybo                              at  51
    s-qtyship                            at  64
    oeel.unit                            at  76
    s-price                              at  80
    s-prcunit                            at  96
    s-discpct                            at 102
    s-netamt                             at 117
    s-sign                               at 130 
    skip
    s-descrip                            at   8
with frame f-oeel no-box no-labels no-underline down width 132.

form  
    s-lineno                             at   1
    v-shipprod                           at   5
    s-qtyord                             at  42
/*    s-qtybo                              at  51 */
    s-qtyship                            at  64
    v-unit                               at  79
    s-price                              at  87
    s-prcunit                            at 101
/*    s-discpct                            at 102 */
    s-netamt                             at 119
    s-sign                               at 132 
    skip
    s-descrip                            at   8
with frame f-vaesl no-box no-labels no-underline down width 134.

form 
     s-lnlineno       at 1 
     s-lnshipprod     at 5 
     s-lncostty       at 32 
     s-lnhours        at 44 
     s-lncolon        at 46 
     s-lnminutes      at 47
     s-lnprodcost     at 87
     s-lnnetamt       at 120 
     s-lnproddesc     at 8 
with frame f-vaepq1it down no-box width 132 no-labels.     
     
/* form to display core charge */
form  
    oeel.shipprod                        at   5
    s-price                              at  81
    s-netamt                             at 117
with frame f-oeel1 no-box no-labels no-underline down width 132.

form
    v-reqtitle                           at   5
    s-prod                               at  23
with frame f-oeelc no-box no-labels width 132.

form
    "** CORE RETURN **"                  at   8
with frame f-oeelr no-box side-labels width 132.

form
    "** DIRECT ORDER **"                 at   8
with frame f-oeeldo no-box side-labels width 132.

form
    "Kit"                                at   1
    oeelk.instructions                   at   5
with frame f-refer no-box no-labels width 132.

form
    s-comdata                           at 8
with frame f-com2 no-box no-labels down width 152.

form
    s-comdata                           at 5
with frame f-com3 no-box no-labels down width 152.

form
with frame f-notes2 no-box no-labels col 8 width 132.

form
    "============="                      at 117
    "Subtotal:"                          at 107
    v-subnetdisp                         at 117 

    skip(1)
with frame f-oeelsub no-box no-labels no-underline width 132.
