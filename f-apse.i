/* f-apse.i 1.1 01/03/98 */
/* f-apse.i 1.4 05/26/93 */
/*h*****************************************************************************
  INCLUDE      : f-apse.i
  DESCRIPTION  : AP Setup Electronic Data (EDI, FAX) form include
  USED ONCE?   : yes
  AUTHOR       : enp
  DATE WRITTEN : 09/06/90
  CHANGES MADE :
  11/12/92 mtt; TB#  8695 Changed the screen to allow the user to maintain
          the 2 new fields equotetype (contained in APSV and APSS) and equoteto
          (contained only in APSS).
  05/14/93 jlc; TB# 11383 No validation on versions.
  09/25/95 mms; TB# 18812 7.0 Rebates Enhancement (F2a); Added s-erebtype
          and s-erebto to screen display.
  10/04/95 kr ; TB# 19460 Add variable apsv.updtprice to form statement.
  11/19/96 jkp; TB# 21705 Add Payments section (s-epmttype and s-epmtto)
          and Payment & Bank section (apsv.paymentty, apsv.vendbanktrno, and
          apsv.vendbankacct) to screen display. Moved Update Price and
          Adv Ctrl # lower on the screen.
  02/10/97 jkp; TB# 21705 Correct typo on apsv.paymentty field for help.
  09/23/02  aa; TB# e13553 Added XML output for vendors. If Epotype=X then
          send XmlOutput (via SxXmlSpinner) to Commerce Connect.
  02/03/03  aa; TB# e16130 Separate EDI directory (APSE) for eBuy vendors
  10/23/03 rgm; TB# e17182 Email where appropriate without CAM
  05/04/07 des; TB# e26319 EDI 845 Vendor Base Code Defaults
        *******************************************************************************/

  g-vendno            colon 13    label "Vendor #"    {f-help.i}
    validate({v-apsv.i g-vendno "/*"},
        "Vendor Not Set Up in Vendor Setup - APSV (4301)")
  apsv.notesfl           at 27    no-label
  apsv.name              at 29    no-label

  g-shipfmno          colon 13    label "Ship From"   {f-help.i}
    validate({v-apss.i "input g-vendno" g-shipfmno},
        "Vend/Ship From Not Set Up in Ship From Setup - APSS (4302")
  apss.name              at 29    no-label
  "Send By   Send To"    at 20
  "-------   -------"    at 20
  "Purchase Orders:"     to 19
  s-epotype              at 23   no-label
    help "(F)ax, (E)di, (X)ml, E(M)ail or Blank"
    validate(if s-epotype = "" then true else can-do("f,e,x,m",s-epotype),
             "Must be F, E, M, X, or Blank (3750)")
  s-epoto                at 30   no-label
    help "(V)endor or (S)hipFm"
  v-text[1]              at 40   no-label
  "Quotes:"              to 19
  s-equotetype           at 23   no-label
    help "(F)ax, E(M)ail or Blank"
    validate(if s-equotetype = "" then true else can-do("f,m",s-equotetype),
             "Must be F, M or Blank (3615)")
  s-equoteto             at 30   no-label
    help "(V)endor or (S)hipFm"
  v-text[2]              at 40   no-label
  "Rebates:"             to 19
  s-erebtype             at 23   no-label
    help "(E)di, or Blank"
    validate(can-do(",e",s-erebtype), "Must be E, or Blank (4336)")
  s-erebto               at 30   no-label
  "Payments:"            to 19
  s-epmttype             at 23   no-label
    help "(E)di, or Blank"
    validate(can-do(",e",s-epmttype), "Must be E, or Blank (4336)")
  s-epmtto               at 30   no-label
  skip (1)
  "EDI -"                 at 1
  s-edipartner        colon 19    label "Partner Code"
    validate(if input s-epotype ne "e" then true
             else s-edipartner ne "",
               "This is a Required Field (2100)")
    help "EDI Partner Name as Defined in Your EDI Software (REQ)"
  "Payment & Bank -"     to 55
  s-edipover          colon 19    label "PO Version"
    help "ANSI Version 002002, 002003, 002040, etc."
  apsv.paymentty      colon 54    label "Type"
    help "ANSI X12 Payment Method Code, Element 591"

  s-edi846ver         colon 19    label "EDI Frt Over"
/*  
    help "ANSI Version 002002, 002003, etc."
*/
    help "Override EDI Frt if not blank EDI Frt% is used"
  apsv.vendbanktrno   colon 54  label "Transit Routing #"
    help "Enter the Transit Routing Number for the Vendor's Bank"
  apsv.vendbankacct   colon 54  label "Account #"
    help "Enter the Bank Account Number for the Vendor"
  s-edinetwork        colon 19    label "EDI User 1"
  s-ecommercety       colon 54    label "eCommerce Type"
    help "Enter 'eBuy' for eBuy vendors or blank for other vendors"
  s-edipartaddr       colon 19    label "EDI User 2"
  apsv.updtprice      colon 54    label "Update Price"
  s-ediyouraddr       colon 19    label "EDI User 3"
  s-edi846no          colon 54    label "EDI Frt % "
     help "Integer used as % of Frt to be paid, 0 defaults to 74."
  "EDI 845 -" at 1
  apsv.edi845netfmty  format "x(2)" colon 19 label "Net Base Fm"
    help "B)s,L)st,P)rc,A)vg,Las(T),R)p,S)t,R(E)b,LstF(O),Act(C)st or ''"
    validate(can-do(",":u + cRebateCostTy-pd,apsv.edi845netfmty),
                    {valmsg.gme 6419})
  apsv.edi845netdwnto format "x(2)" colon 36 label "Net Down To"
    help "F)l,B)s,L)st,P)rc,A)v,Las(T),R)p,S)t,R(E)b,LstF(O),C)st or ''"
    validate(can-do(",":u + cRebDownToTy-pd,apsv.edi845netdwnto),
                   {valmsg.gme 6420})
  apsv.edi845pcttoty  format "x(2)" colon 54 label "Pct Base On"
    help "B)s,L)st,P)rc,A)vg,Las(T),R)p,S)t,R(E)b,LstF(O),Act(C)st or ''"
    validate(can-do(",":u + cRebateCostTy-pd,apsv.edi845pcttoty),
                   {valmsg.gme 6419})
  skip(1)

with frame f-apse side-labels row 1 width 80 overlay no-hide
