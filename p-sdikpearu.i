
/* p-sdikpearu.i 1.5 10/29/93 */
/*h*****************************************************************************
  INCLUDE      : p-sdikpearu.i
  DESCRIPTION  : KPEA/KPET ICSW/OEELK update for kit components
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    10/28/93 kmw; TB# 11758 Set unit conv to 10 dec
    06/06/95 gp;  TB# 18611 Make changes for KP Work Order Demand (D1)
    06/23/95 mtt; TB# 18611 Make changes for KP Work Order Demand (W1)
    08/08/95 mtt; TB# 18611 Make changes for KP Work Order Demand (W1)
    03/15/97 mms; TB#  7241 (5.1) Spec8.0 Special Price Costing;  Added b-icss
        and speccost logic.
    02/13/01 mms; TB# e6817 Only update the oeelk cost when the cost over has
        not been set.
    06/01/01 bpa; TB# e8862 PB Kit rework
    08/11/03 bp ; TB# e17796 Add user hooks.
*******************************************************************************/

/*e

  Optional Parameters:
    &icspbuf      = The name of the buffer to be used to read the ICSP record
                    for each component product.
    &icswbuf      = The name of the buffer to be used to read the ICSW record
                    for each component product.
    &comprevprod  = This parameter can be used to comment out the previous
                    product processing.

  Expected Variables:
    s-diff = the qty to be added or subtracted from qtyreservd.

  Expected Buffers:
    b-oeelk       = Each OEELK record should be read with the b-oeelk buffer
    b-icss        = This buffer is found from the icsp special price costing
                    information
*******************************************************************************/

if b-oeelk.comptype <> "r" then
do:
    /*tb 18611 06/06/95 gp; Change n-qty to k-stkqtyord and k-stkqtyship.
         Change assignments of qtyship. */
    /********* Update the qtys on the OEELK record ***********************/
        /**^^^**/
        assign
             b-oeelk.qtyreservd  = if "{&procsign}" = "+" then   
                                      b-oeelk.qtyreservd + s-diff
                                   else           
                                      b-oeelk.qtyreservd - s-diff
             /*** update qtyship from new reserve ***/
             b-oeelk.qtyship     = b-oeelk.qtyreservd * b-oeelk.qtyneeded
             b-oeelk.stkqtyship  = b-oeelk.qtyreservd * b-oeelk.qtyneeded *
                                    {unitconv.gas &decconv = b-oeelk.conv}.
        {t-all.i b-oeelk}   
       
         export stream msg1 delimiter ","  
               "{&literal}" b-oeelk.orderno b-oeelk.ordersuf  
                      b-oeelk.shipprod
                      b-oeelk.prevqtyrsv "{&procsign}" s-diff 
                      "=" b-oeelk.qtyreservd
                      "kprpreq= " t-kprp.requestdt
                      "compreq="  t-comp.requestdt
                      "stkqtyship = " b-oeelk.stkqtyship.         
          /*
            message  "{&literal}" b-oeelk.orderno b-oeelk.ordersuf  
                      b-oeelk.shipprod
                      b-oeelk.prevqtyrsv "{&procsign}" s-diff 
                      "=" b-oeelk.qtyreservd
                      "kprpreq= " t-kprp.requestdt
                      "compreq="  t-comp.requestdt
                      "stkqtyship = " b-oeelk.stkqtyship. pause.
          */
end.



