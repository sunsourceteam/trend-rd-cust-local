/****************************************************************************
*                                                                           *
* f-pdxs.i - Sales Matrix Margin Report Forms                               *
*                                                                           *
****************************************************************************/

/* Form 1 (South Version) with General Matrix % */
form 
"Prod"                     at  32
"Part Number/"             at  37
"Customer"                 at  70
"Product/"                 at  82
"Contract"                 at  92
"Matrix"                   at 121
"Margin %"                 at 134

"Ship"                     at  14
"Cust"                     at  19
"Sls"                      at  27
"Prc"                      at  32
"Product Price Type"       at  37
"Customer"                 at  60
"Prior Yr"                 at  70
"Prc Type"                 at  82
"Discount/"                at  92
"Invoiced"                 at 108
"Contract"                 at 121
"Based On"                 at 134
"PD"                       at 147
"PD"                       at 155
"PD Last"                  at 170 

"Customer #"               at   1
"To"                       at  14
"Name"                     at  19
"Rep"                      at  27
"Type"                     at  32
"Description"              at  37
"YTD Sales"                at  60
"Sales(Full)"              at  70
"YTD Sales"                at  82
"Net Price"                at  92
"Margin %"                 at 108
"Margin %"                 at 121
"Gen'l Matrx"              at 134
"Record#"                  at 147
"Level"                    at 155
"Exp Date"                 at 161
"Updated"                  at 170
"------------"             at   1
"----"                     at  14
"-------"                  at  19
"----"                     at  27
"----"                     at  32
"------------------------" at  37
"---------"                at  60
"-----------"              at  70
"---------"                at  82
"---------"                at  92
"-----------"              at 108
"-----------"              at 121
"-----------"              at 134
"-------"                  at 147
"-----"                    at 155
"--------"                 at 161
"-------"                  at 170
with frame f-1g-hdr no-underline no-box no-labels page-top down width 178.


form
matrix.customer             at   1
matrix.ship-to              at  14
matrix.name                 at  19
matrix.salesrep             at  27
matrix.pprice-type          at  32
matrix.proddesc             at  34
matrix.ytdsales             at  60
matrix.last-ytdsales        at  70
matrix.i-ytd-amt            at  82
matrix.pct-mult             at  92
matrix.type                 at 100
matrix.pd-disctp            at 102
s-imarg                     at 109
s-mmarg                     at 122
s-gmarg                     at 135
matrix.pd-recno             at 147
matrix.pd-level             at 155
matrix.pd-enddt             at 161
matrix.pd-trandt            at 170
with frame f-1g-detail  no-underline no-box no-labels down width 178.

form
"Prod"                      at  32
"Part Number/"              at  38
"Customer"                  at  76
"Product/"                  at  89
"Contract"                  at 100
"Matrix"                    at 131

"Ship"                      at  14
"Cust"                      at  19
"Sls"                       at  27
"Prc"                       at  32
"Product Price Type"        at  38
"Customer"                  at  64
"Prior Yr"                  at  76
"Prc Type"                  at  89
"Discount/"                 at 100
"Invoiced"                  at 118
"Contract"                  at 131
"PD"                        at 144
"PD"                        at 153
"PD Last"                   at 170

"Customer #"                at   1
"To"                        at  14
"Name"                      at  19
"Rep"                       at  27
"Type"                      at  32
"Description"               at  38
"YTD Sales"                 at  64
"Sales(Full)"               at  76
"YTD Sales"                 at  89
"Net Price"                 at 100
"Margin %"                  at 118
"Margin %"                  at 131
"Record#"                   at 144
"Level"                     at 153
"Exp Date"                  at 160
"Updated"                   at 170

"------------"              at   1
"----"                      at  14
"-------"                   at  19
"----"                      at  27
"----"                      at  32
"------------------------"  at  38
"----------"                at  64
"-----------"               at  76
"---------"                 at  89
"---------"                 at 100
"-----------"               at 118
"-----------"               at 131
"-------"                   at 144
"-----"                     at 153
"--------"                  at 160
"--------"                  at 170
with frame f-1-hdr no-underline no-box no-labels page-top down width 178.


form
matrix.customer             at   1
matrix.ship-to              at  14
matrix.name                 at  19
matrix.salesrep             at  27
matrix.pprice-type          at  32
matrix.proddesc             at  38
matrix.ytdsales             at  64
matrix.last-ytdsales        at  76
matrix.i-ytd-amt            at  89
matrix.pct-mult             at 100
matrix.type                 at 108
matrix.pd-disctp            at 111
s-imarg                     at 119
s-mmarg                     at 132
matrix.pd-recno             at 144
matrix.pd-level             at 153
matrix.pd-enddt             at 160
matrix.pd-trandt            at 170
with frame f-1-detail  no-underline no-box no-labels down width 178.
                            
form                                                               
skip (1)
"Customer Totals:"          at  55
s-ytd-isales                at  89
s-ytd-imarg                 at 119
s-ytd-mmarg                 at 132                                 
with frame f-1-subtot no-underline no-box no-labels down width 178.

form                                                               
skip (1)
"Customer Totals:"          at  55
s-ytd-isales                at  80
s-ytd-imarg                 at 108                                 
s-ytd-mmarg                 at 121                                 
s-ytd-gmarg                 at 134                                 
with frame f-1g-subtot no-underline no-box no-labels down width 178.

form                                                                 
skip (1)
"Salesrep Totals:"           at  55
s-ytd-tisales                at  89
s-ytd-timarg                 at 119                                   
s-ytd-tmmarg                 at 132                                   
with frame f-1-salesrep no-underline no-box no-labels down width 178. 

form                                                                 
skip(1)
"Salesrep Totals:"           at  55
s-ytd-tisales                at  80
s-ytd-timarg                 at 108                                   
s-ytd-tmmarg                 at 121                                   
s-ytd-tgmarg                 at 134                                   
with frame f-1g-salesrep no-underline no-box no-labels down width 178.



form                                                                 
skip (1)
"District Totals:"           at  55
s-ytd-tisales                at  89
s-ytd-timarg                 at 119                                   
s-ytd-tmmarg                 at 132                                   
with frame f-1-district no-underline no-box no-labels down width 178. 

form                                                                 
skip(1)
"District Totals:"           at  55
s-ytd-tisales                at  80
s-ytd-timarg                 at 108                                   
s-ytd-tmmarg                 at 121                                   
s-ytd-tgmarg                 at 134                                   
with frame f-1g-district no-underline no-box no-labels down width 178.

form                                                                   
skip (1)
"Region Totals:"             at  65                                    
r-ytd-tisales                at  87                                    
r-ytd-timarg                 at 119                                    
r-ytd-tmmarg                 at 132                                    
with frame f-1-region no-underline no-box no-labels down width 178. 
                                                                       
form    
skip (1)                                       
"Region Totals:"             at  65
r-ytd-tisales                at  89                                    
r-ytd-timarg                 at 108                                    
r-ytd-tmmarg                 at 121                                    
r-ytd-tgmarg                 at 134                                    
with frame f-1g-region no-underline no-box no-labels down width 178.

form                                                                 
"Grand Totals:"              at  68                                  
g-ytd-tisales                at  87                                  
g-ytd-timarg                 at 118                                  
g-ytd-tmmarg                 at 131                                  
with frame f-1-grand no-underline no-box no-labels down width 178. 
                                                                     
form
"Grand Totals:"              at  68
g-ytd-tisales                at  89                                  
g-ytd-timarg                 at 108                                  
g-ytd-tmmarg                 at 121                                  
g-ytd-tgmarg                 at 134                                  
with frame f-1g-grand no-underline no-box no-labels down width 178.

form                                      
w-errmsg                at 5 label "Error"
with frame f-error side-labels no-box.    

form
skip(1)
"Legend:"                                                at 5
"Price - Value in preceding field is customer price"     at 15
"D%L - Value in preceding field is discount off of list" at 70
"P%L - Price = Value in preceding field * List"          at 130
with frame f-dd-trailer1 no-box no-labels page-bottom
     down width 178.
     
