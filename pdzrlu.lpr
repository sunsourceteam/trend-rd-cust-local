/* pdsrlu.lpr 1.1 01/03/98 */
/* pdsrlu.lpr 1.4 09/09/97 */
/*h*****************************************************************************
  INCLUDE : pdsrlu.lpr (used to be w-prlu.i)
  DESCRIPTION : Date assignment and pdsr find.
  USED ONCE? : yes
  AUTHOR : enp
  DATE WRITTEN : 2/16/93
  CHANGES MADE :
    11/01/94 jtk; TB# 16979 added level code to pdsr index; changed name
    02/27/95 gdf; TB# 17373 Int'l dates not calculated correctly.
    07/21/95 gdf; TB# 18812 7.0 Rebates Enhancement (A5a/b) Changed to use
             pdsr.gfi rather than w-pdsr.i.
    09/09/97 mms; TB# 22023 Year 2000 compliance; removed references to
             century.
*******************************************************************************/

/*tb 17373 02/27/95 gdf; Made 1=v-ms & 4=v-ds for International Dates */
v-date = date(integer(substring(o-frame-value,v-ms,2)),
              integer(substring(o-frame-value,v-ds,2)),
              {y2kfour.gca  substring(o-frame-value,7,2)}).

/*tb 18812 07/21/95 gdf; Changed w-pdsr.i to pdsr.gfi */
{pdzr.gfi codeid
          levelkey
          vendno
          rebsubty
          custno
          shipto
          custrebatety
          whse
          v-date no-lock}



