/* a-poepp6.i 1.1 01/02/98 */
/******************************************************************************
  VERSION 5.5  : called from lansdcape(poepp6a.p) , portrait(poepp9a.p) ,
               : fax(poeppf9.p) , e-mail(poeppm1.p) SDI specific programs
  INCLUDE      : a-poepp6.i
  DESCRIPTION  : Set headings for PO print if preprinted form is not used
  USED ONCE?   : Yes (p-poepp1.i)
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    03/18/93 rs ; TB# 10264 Price U/M displays ICSP Special Cost
    10/11/95 kr ; TB# 19526 Move POEP to new Processing Menu
*******************************************************************************/

/*d "Hard code" the headings for the PO print if a preprinted form is not
    used for the PO print */
/*e Set the "literal" variables for headings based on the line the heading
    prints on the PO */

if v-headingfl then assign
    {1}lit1a  = if poeh.transtype = "rm" then
    "Document: RETURN MERCHANDISE"
else if poeh.transtype = "qu" then
    "Document: REQUEST FOR QUOTE"
else "Document: Purchase Order"
    {1}lit1a  = if poeh.transtype = "bl" then {1}lit1a + " - Blanket Order"
                else if poeh.transtype = "br" then {1}lit1a +
                     " - Blanket Release"
                else if poeh.transtype = "do" then {1}lit1a + " - D/S"
                else {1}lit1a
/*    {1}lit1a  = if poeh.confirmfl = yes then {1}lit1a + " - CONFIRMING"
                else {1}lit1a */
    {1}lit1b  = "UPC Vendor  P.O. Date      PO #    Page#"
    {1}lit2a  = if can-do("qu,rm",poeh.transtype) then
    "============================"
else ""
    {1}lit3a  = "  Vend #:"
    {1}lit6a  = "Seller:"
    {1}lit6b  = "Sold To:"
    {1}lit6c  = "Invoice To:"
    {1}lit11a = "Ship To:"
    {1}lit11b = "Instructions"
    {1}lit11c = "Resale #"
    {1}lit11d = "Reference"
    {1}lit12b = "Via"
    {1}lit12c = "Due Date"
    {1}lit12d = "Freight Terms"
    {1}lit12e = "Payment Terms"
    {1}lit12f = "Schedule"
    {1}lit14a =
"Line         Product                                   UPC    Quantity      Qt"
    {1}lit14b =
"y      Unit      Price       Amount       Due"
    {1}lit15a =
" #       And Description                              Item#    Ordered      U/"
    {1}lit15b =
"M      Price      U/M        (Net)        Date  "
    {1}lit16a =
"------------------------------------------------------------------------------"
    {1}lit16b =
"--------------------------------------------------"
    {1}lit40a = "Lines Total"
    {1}lit40b = "Qty Shipped Total:"
    {1}lit40c = "Purchase Total:   "
    {1}lit48b = "Confirming:"
    {1}lit48c = "Buyer:"
    {1}lit48d = "Signature:"
    {1}lit50a = "==========".

else
    assign
{1}lit1a  = if poeh.transtype = "rm" then
"          RETURN MERCHANDISE"
    else if poeh.transtype = "qu" then
"          REQUEST FOR QUOTE"
    else "          Purchase Order"
{1}lit1a  = if poeh.transtype = "bl" then {1}lit1a + " - Blanket Order"
    else if poeh.transtype = "br" then {1}lit1a +
 " - Blanket Release"
    /* SX55 */
    else if poeh.transtype = "do" then {1}lit1a + " - D/S"
    else {1}lit1a
/*        {1}lit1a  = if poeh.confirmfl = yes then {1}lit1a + " - CONFIRMING"
                    else {1}lit1a */
{1}lit6b  = "        "
{1}lit40a = "Lines Total"
{1}lit40b = "Qty Shipped Total:"
{1}lit40c = "Purchase Total:   ".

assign
    {1}lit41a = "Order Discount:   "
    /* SX55 */
        {1}comphd1 = "                         "  
/* DBF IT #1 beg */   
    {1}comphd2 = ""
    {1}comphd3 = ""
    {1}comphd4 = ""
/* DBF IT #1 end */
    {1}lit48a = "Continued".

/* SX55 */
assign 
  {1}lit49  = " "
  {1}lit49a = "** PLEASE CONFIRM PRICING AND SHIPPING DATE WITHIN 24 HOURS**".
   
if poeh.transtype = "po" then do:
   assign {1}lit49 =
         "** SHIP UPS COLLECT (WITHOUT DECLARED VALUE) OR >150# LTL COLLECT BASED ON ROUTING LETTER **".
   assign {1}lit51a = " ".
   if poeh.whse = "DROS" then
      assign {1}lit51a = 
                    "** PLEASE DO NOT SHIP WITH STYROFOAM PACKING PEANUTS **".
   if poeh.whse = "DMCP" or poeh.whse = "DBUS" or poeh.whse = "SMIN" then
      assign {1}lit51a = 
                    "** PLEASE DO NOT SHIP PRODUCT PACKED IN PEANUTS **".
end.

if poeh.transtype = "bl" or poeh.transtype = "br" then 
 do:
    assign {1}lit52a = 
    "*****  Blanket releases exist for the following quantities and release dates  *****"
           {1}lit52b = 
    "**** The following is for informational purposes only - See PO releases below  ****"
           {1}lit52c = 
    "PO Number        Product                     Qty to Release       Release Date".
 end.



