/* f-aptus.i 1.1 01/03/98 */
/* f-aptus.i 1.3 10/22/93 */
/*h*****************************************************************************
  INCLUDE      : f-aptus.i
  DESCRIPTION  : AP Vendor Tax Screen
  USED ONCE?   : yes
  AUTHOR       : enp
  DATE WRITTEN :
  CHANGES MADE :
    07/20/93 jrg; TB# 10584 Added title
    10/22/93 kmw; TB# 12955 1099 Name Needed in APSV
    07/02/01 gwk; TB# e8953 New format for 1009-Misc form
*******************************************************************************/
skip (1)
apsv.fed1099no  colon 19
apsv.fed1099box colon 19
    help "Area on the 1099 Miscellaneous Form, 1 - 18"
    validate (apsv.fed1099box < 19,"Must be 1 Through 18 (3157)")
apsv.fedtaxid   colon 19
/*tb 12955 10/22/93 kmw; 1099 Name Needed in APSV */
apsv.ap1099nm   colon 19
apsv.user24   format "x(1)" label "Business Type"   colon 19                                                 
    help "(I)Indvidual, (C)Corporation, (P)Partnership, (L)LLC"          
with frame f-apsvt side-labels title "Tax Information"
    row 5 width 80 overlay
