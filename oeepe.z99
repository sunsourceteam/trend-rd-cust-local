/* oeepe.z99 1.1 01/03/98 */
/*h*****************************************************************************
  INCLUDE      : oeepe.z99
  DESCRIPTION  : User include for oeepe.p (EDI ASN).
  USED ONCE?   : yes
  AUTHOR       : jlc
  DATE WRITTEN : 11/19/94
  CHANGES MADE :

    Mode           Description
    -------------  -----------------------------------------------------------
    &top_main                 Placed at top of processsing logic in oeepe.p.
    &top_foreach              Placed at top of the for each loop
    &asn_grouping             Placed in the logic specific to grouping the
                              customers/ship to's in the edi flat file.
    &asn_before               Placed immediately before the ASN    record
                              fields are written to the flat file.
    &asn_after                Placed immediately after the ASN     record
                              fields are written to the flat file.
    &header_before            Placed immediately before the HEADER record
                              fields are written to the flat file.
    &header_after             Placed immediately after the HEADER  record
                              fields are written to the flat file.
    &custom_before            Placed immediately before the CUSTOM record
                              fields are written to the flat file.
    &custom_after             Placed immediately after the CUSTOM  record
                              fields are written to the flat file.
    &shipto_before            Placed immediately before the SHIPTO record
                              fields are written to the flat file.
    &shipto_after             Placed immediately after the SHIPTO  record
                              fields are written to the flat file.
    &notes_before             Placed immediately before the NOTES  record
                              fields are written to the flat file.
    &notes_after              Placed immediately after the NOTES   record
                              fields are written to the flat file.
    &item_before              Placed immediately before the ITEM   record
                              fields are written to the flat file.
    &item_after               Placed immediately after the ITEM    record
                              fields are written to the flat file.
    &serial_before            Placed immediately before the SERIAL record
                              fields are written to the flat file.
    &serial_after             Placed immediately after the SERIAL  record
                              fields are written to the flat file.
    &lot_before               Placed immediately before the LOT    record
                              fields are written to the flat file.
    &lot_after                Placed immediately after the LOT     record
                              fields are written to the flat file.
    &lncomm_before            Placed immediately before the LNCOMM record
                              fields are written to the flat file.
    &lncomm_after             Placed immediately after the LNCOMM  record
                              fields are written to the flat file.
    &ordtot_before            Placed immediately before the ORDTOT record
                              fields are written to the flat file.
    &ordtot_after             Placed immediately after the ORDTOT  record
                              fields are written to the flat file.
    &oeeh_update              Placed after the order has completed all
                              processing for this ASN.
    &bottom_main              Placed after the main processing building the
                              ASN flat files.
    &sapbo_loop_top_foreach 
    &sapb_loop_asn_grouping 
    
******************************************************************************/
/{&top_main}*/
def stream oeepeWTF.
output stream oeepeWTF to "/usr/tmp/ASN_mystery" APPEND.
def var wk-user       as c  format "x(78)".
def var wk-addon-amt  as de format "999999.99".
/* SX 5.5
def var s-edilineno      as c format "x(11)"         no-undo.
*/    
    
define var drepid as recid no-undo.


define var e-shipdt as date no-undo.
define var b-shipdt as date no-undo.
define var p-list   as logical no-undo.
define var v-removeit as logical no-undo.
define var var_find as logical init no no-undo.

define variable var_stagechg as logical init no  no-undo.
define buffer z-oeeh for oeeh.
define buffer o-oeeh for oeeh.
define buffer z-addon for addon.
define buffer o-addon for addon.
define var v-fc-amt as dec no-undo.
define var v-hd-amt as dec no-undo.
define var x-entry  as int format ">>>9" no-undo.
define buffer p-oeehp for oeehp.
def var x-oeehuser4 as c   format "x(78)" no-undo.
def var pkgcount    as i   format "999"   no-undo.

assign v-datein = sapb.rangeend[5].
{p-rptdt.i}
assign e-shipdt = v-dateout. 

assign v-datein = sapb.rangebeg[5].
{p-rptdt.i}
assign b-shipdt = v-dateout.
 
assign p-list = if sapb.optvalue[4] = "yes" then yes else no.
/{&top_main}*/

/{&top_foreach}*/
 export stream oeepeWTF delimiter ","
   string(TODAY,"99/99/99") oeeh.orderno oeeh.ordersuf oeeh.custno oeeh.shipto
   oeeh.stagecd oeeh.shipdt oeeh.shiptm sapb.reportnm g-operinit
   substr(oeeh.user2,5,3) substr(oeeh.user13,5,25).
/{&top_foreach}*/


/{&asn_grouping}*/
  assign drepid = recid(report)
         v-removeit = false.

  if oeeh.shipdt < b-shipdt or
     oeeh.shipdt > e-shipdt then
    assign v-removeit = true.

  if not v-removeit and p-list then
    do:
    find first sapbo where sapbo.cono = sapb.cono and
                           sapbo.reportnm = sapb.reportnm and
                           sapbo.orderno = oeeh.orderno and
                           sapbo.ordersuf = oeeh.ordersuf no-lock no-error.

    if not avail sapbo then
      v-removeit = true.
  end.
 
  /* das - 04/07/10 - Removed since cat wants all ASNs to go through *
  if not v-removeit and not p-list and oeeh.custno = 233912 and 
       substring(oeeh.user2,1,3) <> "862" then
    v-removeit = true.
  ****/
  if not v-removeit and not p-list and oeeh.custno = 233912 and 
       substring(oeeh.user2,5,3) = "ASN" then
    v-removeit = true.
  if not v-removeit and not p-list and oeeh.custno = 233912 and 
       substring(oeeh.user3,1,6) = "CONSIG" 
    then 
    do:
    v-removeit = true.
    find b-oeeh use-index k-oeeh where b-oeeh.cono     = g-cono 
                                   and b-oeeh.orderno  = oeeh.orderno 
                                   and b-oeeh.ordersuf = oeeh.ordersuf
                                   no-error.
    if avail b-oeeh then 
      assign substring(b-oeeh.user2,9,6) = "CONSIG".
  end.         

  assign var_find = no.
  if oeeh.custno = 13301743 or oeeh.custno = 13301741 then 
  do:
    assign var_find = no.
    do i = 1 to 22:
      if substring(oeeh.custpo,i,1)           = "B" and
         substring(oeeh.custpo,i + 1,1)       = "O" then 
        do:
        assign var_find = yes.
        leave.
      end.
    end.
    do i = 1 to 22:    
      if substring(oeeh.custpo,i,1)           = "P" and
         substring(oeeh.custpo,i + 1,1)       = "M" then 
        do:
        assign var_find = yes.
        leave.
      end.
    end.
    do i = 1 to 22:   
      if substring(oeeh.custpo,i,1)           = "B" and
         substring(oeeh.custpo,i + 1,1)       = "V" then 
        do:
        assign var_find = yes.
        leave.
      end.
    end.
    do i = 1 to 22:  
      if substring(oeeh.custpo,i,1)           = "V" and
         substring(oeeh.custpo,i + 1,1)       = "D" then 
        do:
        assign var_find = yes.
        leave.
      end.       
    end.
  end.  /* oeeh.custno = 13301743 or oeeh.custno = 13301741 */

  if var_find = yes then 
     assign v-removeit = false.

  if v-removeit then  
    do:
    find report where recid(report) = drepid exclusive-lock. 
    delete report.
  end.  
/{&asn_grouping}*/

/{&asn_before}*/
/{&asn_before}*/

/{&asn_after}*/
/{&asn_after}*/

/{&header_before}*/
  /* begin das - 02/02/00 - Caterpillar */
  if oeeh.custno = 233912 and substring(oeeh.user2,1,3) = "862" then do:
/* SX 55 */
    assign v-fc-amt = 0
           v-hd-amt = 0.

    for each z-addon use-index k-seqno where
             z-addon.cono      = g-cono       and
             z-addon.ordertype = "oe" and
             z-addon.orderno   = oeeh.orderno   and
             z-addon.ordersuf  = oeeh.ordersuf no-lock:
      if z-addon.addonno = 1 or z-addon.addonno = 2 then  
        v-fc-amt = v-fc-amt + z-addon.addonamt.
      if z-addon.addonno ge 3  then  
        v-hd-amt = v-hd-amt + z-addon.addonamt.
    end.        
    if v-fc-amt <> 0 
    /* oeeh.addonno[1] > 0 or oeeh.addonno[2] > 0  */ then 
      do:
      assign wk-user     = "FC"
             s-oeehuser1 = string(wk-user,"x(78)")
             wk-addon-amt = v-fc-amt /* oeeh.addonamt[1] + oeeh.addonamt[2] */
             wk-user      = string(wk-addon-amt,"zzzz9.99")
             s-oeehuser2 = string(wk-user,"x(78)").
    end.
    if v-hd-amt <> 0 then
/*    if oeeh.addonno[3] > 0 or oeeh.addonno[4] > 0 then  */
      do:
      assign wk-user     = "HD"
             s-oeehuser3 = string(wk-user,"x(78)")
             wk-addon-amt = v-hd-amt /* oeeh.addonamt[3] + oeeh.addonamt[4] */
             wk-user      = string(wk-addon-amt,"zzzz9.99")
             s-oeehuser4 = string(wk-user,"x(78)").
    end.
    assign s-oeehuser10 = string(oeeh.user3,"x(80)").   /* CAT Misc info */
    assign s-oeehuser11 = string(substring(oeeh.user2,1,3),"x(8)"). /* 862 */
  end. /* oeeh.custno = 233912 and substring(oeeh.user2,1,3) = "862" */
  /* end   das - 02/02/00 */

  if oeeh.custno = 13301743 or oeeh.custno = 13301741 then  
    do:
    if v-fc-amt <> 0 or v-hd-amt <> 0 then
    /*
    if oeeh.addonno[1] > 0 or oeeh.addonno[2] > 0 or oeeh.addonno[3] > 0 or
       oeeh.addonno[4] > 0 then
    */
      assign s-oeehuser5 = string(v-fc-amt + v-hd-amt).
                      /*   string(oeeh.addonamt[1] + oeeh.addonamt[2] + 
                           oeeh.addonamt[3] + oeeh.addonamt[4]). */
  end.                                                               
    
  /*
  if avail oeeh and oeeh.custno <> 233912 then
    do:
  */
    find first oeehp where 
                     oeehp.cono = oeeh.cono and
                     oeehp.orderty = "o" and
                     oeehp.orderno = oeeh.orderno  and
                     oeehp.ordersuf = oeeh.ordersuf
                     no-lock no-error.
    if avail oeehp then
      do:   
      assign pkgcount = 0.
      for each p-oeehp where p-oeehp.cono     = oeeh.cono and
                             p-oeehp.orderty  = "o" and
                             p-oeehp.orderno  = oeeh.orderno and
                             p-oeehp.ordersuf = oeeh.ordersuf
                             no-lock:
        assign pkgcount = pkgcount + 1. 
      end.
      if pkgcount = 0 then
        assign pkgcount = 1.
      /*overlay(x-oeehuser4,1,30) = oeehp.trackerno.       */
      /*overlay(x-oeehuser4,35,3) = string(pkgcount,"999").*/
      assign s-oeehuser4 = string(oeehp.trackerno, "x(78)").
      /*assign s-oeehuser4 = string(x-oeehuser4,"x(78)").  */
    end.
    else
      assign s-oeehuser4 = string(" ", "x(78)"). 
  /*            
  end.                      
  */
/{&header_before}*/

/{&header_after}*/
/{&header_after}*/

/{&custom_before}*/
/{&custom_before}*/

/{&custom_after}*/
/{&custom_after}*/

/{&shipto_before}*/
/* aron - 02/05/16 - Caterpillar ship-to code for SP20 shipping label
                     which appears on the label's 4th address line */
 if oeeh.custno = 233912 and oeeh.user11 <> "" then
   assign s-shiptoerpid = oeeh.user11.
/{&shipto_before}*/

/{&shipto_after}*/
/{&shipto_after}*/

/{&notes_before}*/
/{&notes_before}*/

/{&notes_after}*/
/{&notes_after}*/

/{&item_before}*/
  /* aron - 02/10/16 - Caterpillar EC # */
  overlay(s-oeeluser12,1,2) = substr(oeel.user10,10,2).
  /* begin das - 02/02/00 - Caterpillar */
  assign s-oeeluser6 = string(oeel.price - oeel.discamt,"zzzzzzzz9.99999-").
  assign s-oeeluser3 = "".
  if oeel.reqprod <> "" then
     assign s-custprod = string(oeel.reqprod,"x(24)").
 /* end   das - 02/02/00 */
 /* das - 01/13/10 - add Country of Origin in user11 */
  find icsp where icsp.cono = g-cono and
                  icsp.prod = oeel.shipprod
                  no-lock no-error.
  /* 3rd entry in user3, tab delimited */
  if avail icsp and icsp.user3 <> " " then
    do:
    assign x-entry = num-entries(icsp.user3,"~011").
    if x-entry >= 3 then
      do:
      overlay(s-oeeluser11,1,2) = entry(3,icsp.user3,"~011").
      if substr(s-oeeluser11,1,2) = "CN" then
        assign overlay(s-oeeluser11,1,2) = "CA".
    end.
  end.
  if s-oeeluser11 = "" then
    do:
    if oeel.arpvendno <> 0 then
      do:
      /*assign s-oeeluser11 = "".*/
      find apsv where apsv.cono = g-cono and
                      apsv.vendno = oeel.arpvendno
                      no-lock no-error.
      if avail apsv then
        do:
        if apsv.countrycd = " " then
          overlay(s-oeeluser11,1,2) = "US".
        else
          overlay(s-oeeluser11,1,2) = apsv.countrycd.
      end.
      if s-oeeluser11 = " " then
        overlay(s-oeeluser11,1,2) = "US".
      if s-oeeluser11 = "CN" then
        overlay(s-oeeluser11,1,2) = "CA".
    end. /* oeel.arpvendno <> 0 */
    else
      overlay(s-oeeluser11,1,2) = "US".
  end. /* s-oeeluser11 = "" */  
  if s-oeeluser2 = " " and pkgcount > 0 then
    overlay(s-oeeluser2,1,3) = string(pkgcount,">>9").     
    
/{&item_before}*/

/{&item_after}*/
/{&item_after}*/

/{&serial_before}*/
/{&serial_before}*/

/{&serial_after}*/
/{&serial_after}*/

/{&lot_before}*/
/{&lot_before}*/

/{&lot_after}*/
/{&lot_after}*/

/{&lncomm_before}*/
/{&lncomm_before}*/

/{&_lncommafter}*/
/{&lncomm_after}*/

/{&ordtot_before}*/
/{&ordtot_before}*/

/{&ordtot_after}*/
/{&ordtot_after}*/
  

/{&oeeh_update}*/
 do for b-oeeh transaction:
   find b-oeeh where recid(b-oeeh) = recid(oeeh) /*v-oeehid*/ 
                                     exclusive-lock no-error.
   if avail b-oeeh then 
     assign substr(b-oeeh.user2,5,3) = "ASN".
     
   if avail b-oeeh and avail sapb and sapb.reportnm = "cat8561" then
     assign substr(b-oeeh.user13,5,3) = "ASN"
            substr(b-oeeh.user13,9,4) = g-operinit
            substr(b-oeeh.user13,14,8) = string(TODAY,"99/99/99")
            substr(b-oeeh.user13,23,5) = string(TIME,"HH:MM").
 end.    
/{&oeeh_update}*/

/{&bottom_main}*/

for each sapbo where sapbo.cono = sapb.cono and
                     sapbo.reportnm = sapb.reportnm 
                     exclusive-lock:
  delete sapbo.
end.
/{&bottom_main}*/
                
/{&sapbo_loop_top_foreach}*/ 
/*  USED FOR SENDING ASNs IN STAGE 5 */
/*
assign var_stagechg = no.

{w-oeeh.i sapbo.orderno sapbo.ordersuf exclusive-lock z-}
   if avail z-oeeh and z-oeeh.custno = 233912 then
   do:
      assign z-oeeh.stagecd = 4.
      assign var_stagechg = yes.
   end.
*/
/{&sapbo_loop_top_foreach}*/ 

/{&sapb_loop_asn_grouping}*/

/* USED FOR SENDING ASNs IN STAGE 5  */
/* Removed code 11/12/04 */
/*
{w-oeeh.i sapbo.orderno sapbo.ordersuf exclusive-lock o-}
   if avail o-oeeh and o-oeeh.custno = 233912 and var_stagechg = yes then
   do:
      assign o-oeeh.stagecd = 5.
      assign var_stagechg = no.
   end.
*/   
/{&sapb_loop_asn_grouping}*/



 
