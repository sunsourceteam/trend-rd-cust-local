/*h****************************************************************************
  INCLUDE      : p-oeett.i
  DESCRIPTION  : OE Tendering - Function key processing
  USED ONCE?   : yes
  AUTHOR       : NxT
  DATE WRITTEN : 04/09/93
  CHANGES MADE :
    04/09/93 rhl; TB# 9507 Can tender on ordered, but BO flag = no
    02/13/01 cm;  TB# e7495 Credit Cards.  Brought the SI Modification into
        standard.  Also did some code cleanup.
    08/05/02 mwb; TB# e13246 Added masking to credit card fields.
    02/11/04 bm;  TB# e19230 Added Warning message when reverse tendering.
    10/06/05 emc; TB# e21853 Multi tender types cursor position wrong.
    05/10/06 rgm; TB# e20089 CC Swipe Addition
    05/16/06 sbr; TB# e18853 Over tender allowed
    06/19/06 lac; TB# e24709 CC-Stuck in pmt # field when tendering - added
        check that cc not blank before performing cc number field edits prior
        to GO.
    08/08/06 sbr; TB# e24942 Tender with media not CC CC# in GLET
    11/30/06 ejk; TB# e24261 Downpayment amount wrong on fully tendered 00 when
        01 is unshipped and v-tendqtyfl is changed to Shipped.
    03/19/07 ejk; TB# e25608 When payment types default in and cursor defaults
        to the pay amount field, pressing down-arrow/up-arrow causes errors.
******************************************************************************/

/*d Allow entry of tendering fields when return, GO, or a function key is
    pressed */
if {k-after.i} or {k-func.i} then do:

    /*d Validate the media codes and display their descriptions */
    if frame-field = "x-mediacd":U then do:
        assign
            j            = frame-index
            g-applhelpty = input x-mediacd[j].

        if input x-mediacd[j] ne o-mediacd[j] then do:
            assign
                v-diffmediafl = no
                o-mediacd[j]  = input x-mediacd[j].

            if o-mediacd[j] = 0 then
                display "" @ s-mediacd[j].
            else do:
                {w-sastn.i P o-mediacd[j] no-lock}
                if avail sastn then do:
                    display sastn.descrip @ s-mediacd[j].

                    if sastn.addtaxfl = no then
                        display "" @ v-media[j].

                    if lCCInstalled then do:
                        {oeett.gas &array = j}

                        g-applhelpty = substr(string(sastn.codeval,"9999"),
                                           3,1).

                        next-prompt v-sdiTenderfl /* s-payamt[j] */ 
                           with frame f-oeett.
                        next loop.
                    end. /* if lCCInstalled */

                end. /* if avail sastn */
            end. /* else do */

        end. /* if input x-mediacd[j] ne o-mediacd[j] */

    end. /* if frame-field = "x-mediacd" */

    /*d If the down payment amount/% has changed, calculate the amount due,
        calculate the down payment amount, and set the first payment
        amount to the amount of the down payment */
    {1}
    if input v-dwnpmtamt ne o-dwnpmtamt or
        input v-dwnpmttype ne o-dwnpmttype
    then do:
        assign
            v-tendamt    = s-paid
            o-dwnpmtamt  = input v-dwnpmtamt
            o-dwnpmttype = input v-dwnpmttype
            s-dwnpmtamt  = if input v-dwnpmttype then
                               input v-dwnpmtamt
                           else if input v-tendqtyfl = no then
                               (s-shptotalamt * input v-dwnpmtamt) / 100
                           else
                               (s-ordtotalamt * input v-dwnpmtamt) / 100
            s-amtdue     = if input v-tendqtyfl = no then
                               s-shptotalamt - v-tendamt - s-dwnpmtamt
                           else
                               s-ordtotalamt - v-tendamt - s-dwnpmtamt
            lEditFl      = no.

        display s-amtdue s-dwnpmtamt.

        if s-maint = yes then do:
            s-payamt[1] = s-dwnpmtamt.
            display s-payamt[1].
        end. /* if s-maint = yes */
        else do:
            j = 0.
            do i = 1 to 3:
                if s-payamt[i] ne 0 then
                    assign
                        k = i
                        j = j + 1.
            end. /* do i = 1 to 3 */

            if j = 0 then
                assign
                    j = 1
                    k = 1.

            if j = 1 then do:
                s-payamt[k] = s-dwnpmtamt.
                display s-payamt[k].
            end. /* if j = 1 */
        end. /* else do */

    end. /* if input v-dwnpmtamt ne o-dwnpmtamt */
    /{1}*  */

    /*d Added during the tendering enhancement. If the order or ship default
        is toggled, need to reset the downpayment and amount due and redisplay
        the highlighted totals */
    if input v-tendqtyfl <> v-tendqtyfl then do:
        assign v-tendqtyfl.

        if v-tendqtyfl = no then do:
            color display messages v-shplabel s-shptotalamt with frame f-oeett.
            display v-shplabel s-shptotalamt with frame f-oeett.
            color display normal v-ordlabel s-ordtotalamt with frame f-oeett.
            display v-ordlabel s-ordtotalamt with frame f-oeett.
        end. /* if input v-tendqtyfl <> v-tendqtyfl */
        else do:
            color display messages v-ordlabel s-ordtotalamt with frame f-oeett.
            display v-ordlabel s-ordtotalamt with frame f-oeett.
            color display normal v-shplabel s-shptotalamt with frame f-oeett.
            display v-shplabel s-shptotalamt with frame f-oeett.
        end. /* else do */

         /* if the order was previously tendered to ordered (in excess of the
            shipped amount) we should honor that instead of defaulting a
            refund screen */
         if v-tendqtyfl = no and
            oeeh.tendamt + v-ordbotendamt > s-shptotalamt then
            assign v-tendqtyfl = yes.

        /*tb 9507 04/09/93 rhl; Can tender on ordered, but BO flag = no */
        assign
            v-tendamt    = s-paid
            s-dwnpmtamt  = if v-tendqtyfl = no then
                               if input v-dwnpmttype then
                                   s-shptotalamt - v-tendamt
                               else
                                   ((s-shptotalamt * input v-dwnpmtamt) / 100)
                                       - v-tendamt
                           else if input v-dwnpmttype then
                               s-ordtotalamt - v-tendamt
                           else
                               ((s-ordtotalamt * input v-dwnpmtamt) / 100) -
                                   - v-tendamt
            v-dwnpmtamt  = if input v-dwnpmttype then
                               s-dwnpmtamt
                           else
                               v-dwnpmtamt
            s-amtdue     = if v-tendqtyfl = no then
                               s-shptotalamt - s-dwnpmtamt - v-tendamt
                           else
                               s-ordtotalamt - s-dwnpmtamt - v-tendamt
            v-amtdue     = if v-tendqtyfl = no then
                               v-amtdueshp
                           else
                               v-amtdueord
            s-payamt[1]  = s-dwnpmtamt
            o-dwnpmtamt  = v-dwnpmtamt
            o-dwnpmttype = v-dwnpmttype
            lEditFl      = no.

        display s-amtdue v-dwnpmtamt s-dwnpmtamt s-payamt[1]
        with frame f-oeett.

    end. /* if input v-tendqtyfl <> v-tendqtyfl */

    /* Must carry an unmasked card number for validation and GL/AR posting. */
    if frame-field = "v-media":u then
        assign
            j = if j eq 0
                then if frame-index ne 0
                     then frame-index
                     else 1
                else j
           v-unmaskedcardno[j] = if input v-media[j] ne v-media[j] then
                                      input v-media[j]
                                   else v-unmaskedcardno[j].

    /* Credit Card Processing */
    if lCCInstalled then do:
        /* Credit card number entered */
        if frame-field = "v-media":U then do:

            j = frame-index.

            if lCreditCdFl[j] and input v-media[j] <> cOrigMedia[j] and
                input s-payamt[j] <> 0 and input v-media[j] <> ''
            then do:

                {zsdicc-oeett.led &media   = v-unmaskedcardno[j]
                              &cardno  = v-media[j]
                              &mediacd = x-mediacd[j]
                              &amount  = s-payamt[j]
                              &label   = loop
                              &frame   = f-oeett
                              &array   = "[j]"
                              &i       = j}

            end. /* if lCreditCdFl[j] */

        end. /* if frame-field = "v-media" */

        /* If GO was pressed or RETURN from the last field, then perform credit
           card edits for any changes made. */
        if {k-select.i} or
             ({k-return.i} and frame-field = "v-media":U and frame-index = 3)
        then do j = 1 to 3:
            if lCreditCdFl[j] and input s-payamt[j] <> 0 and
                (not lEditFl[j] or input v-media[j] <> cOrigMedia[j])
            then do:
                 {zsdicc-oeett.led &media   = v-unmaskedcardno[j]
                              &cardno  = v-media[j]
                              &mediacd = x-mediacd[j]
                              &amount  = s-payamt[j]
                              &label   = loop
                              &frame   = f-oeett
                              &array   = "[j]"
                              &i       = j}
 
             end. /* if lCreditCdFl[j] */

        end. /* if {k-select.i} */

    end. /* if lCCInstalled */

end. /* if {k-after.i} or {k-func.i} */
