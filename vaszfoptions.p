/***************************************************************************
  PROGRAM NAME: vaszfoptions.p
  PROJECT NO.: Fab
  DESCRIPTION: 
                Set up Fab BOM structure for updating intangables and 
                labor items within the VASP that will hold the Fab Quote
  DATE WRITTEN: 02/09/10.
  AUTHOR      : TAH.  
***************************************************************************/

{g-all.i}
{g-ar.i}
{g-ap.i}
{va.gva}

define shared var v-template as char  format "x(24)" no-undo.
define shared var v-descrip  as char  format "x(24)" no-undo.
def var v-matrixid    as char  format "x(24)"  no-undo.
define var v-prod     as char  format "x(24)"  no-undo.
define var v-sctnno   as int   format ">>9"    no-undo.
define var v-adder    as dec   format "999.99" no-undo.
define var v-closewindow as logical            no-undo.
define var v-addonamt as dec   format "zzzz9.999" no-undo.
define var v-addontype as logical format "%/$"  no-undo.
define var v-typename  as char    format "x(24)" no-undo.
define var v-default   as logical                 no-undo.
define var v-description as char format "x(24)"  no-undo.
def var p-rowid          as integer              no-undo.
def var q-rowid          as rowid                no-undo.
def var v-status         as logical             no-undo.
def var z                as logical              no-undo.
define var v-rowint    as integer              no-undo.
define var scrn_lns    as integer              no-undo.

define var v-returncd  as logical              no-undo.

define shared temp-table t-fab no-undo 
    Field template  as char format "x(24)"
    Field prod      as char format "x(24)"
    Field sctntype  as char format "x(2)"
    Field seqno     as integer format "zz9"
    Field lineno    as integer format "zz9"
    Field type      as char format "x(2)"
    Field shortnm   as char format "x(15)"
    Field addonty   as logical format "%/$"
    Field addonamt  as decimal format "zzzz9.999"    
    Field frecid    as recid
    
    index k-itemlk
          seqno
          lineno
          prod
     index k-recid
           frecid.



define temp-table t-options no-undo
 field typename       as char format "x(24)"
 field description    as char format "x(24)"
 field odefault        as logical 
 index tyinx
    typename
    description.
    

define query  q-options for t-options scrolling.
define browse b-options query q-options
      display (if t-options.odefault = true then
                 "*"
               else
                 " ")
              t-options.typename
              t-options.description
      with 13 down centered overlay no-hide no-labels
title 
" Option       Description   ". 

form 
   skip(1)
   v-template  colon 23 label "Template "
   b-options   at row 5 col 6
   {f-help.i}
   with frame f-options width 80 row 1 centered
     overlay title g-title side-labels.   

form 
    v-typename    colon 16 label "          Type"
    v-description colon 16 label "   Description"
    v-addonamt           colon 16 label "         Amount"   
    v-addontype          at 28  no-label
    v-default            colon 16 label "Default Value"
    with frame f-addoptions width 55 row 09 col 14
    overlay title "Add Option Type " side-labels. 
form 
    v-typename    colon 16 label "          Type"
    v-description colon 16 label "   Description"
    v-addonamt           colon 16 label "         Amount"   
    v-addontype          at 28 no-label
    v-default            colon 16 label "Default Value"
    with frame f-Changeoptions width 55 row 09 col 14
    overlay title "Add Option Type " side-labels. 


   

on any-key of b-options do:

    if {k-accept.i} then do:
       run "clear-browse".
       on cursor-up cursor-up.
       on cursor-down cursor-down.
       apply "window-close" to current-window. 
    end.
 
    
    do transaction:
    if g-secure > 2 then do:
      put screen row 20 col 13 color message   
      "   F6-Add F7-Change F8-Delete                            ".
      pause 0.
    end.
    else do:
      put screen row 20 col 13 color message   
      "                                                         ".
      pause 0.
    end.
    if {k-func6.i} and g-secure > 2 then do:
      Addloop:
      do while true on endkey undo, leave AddLoop:
        update 
          v-typename 
          v-description      
          v-addonamt
          v-addontype
          v-default
        with frame f-addoptions
        editing:
          readkey.       
          if {k-cancel.i} then do: 
            on cursor-up cursor-up.
            on cursor-down cursor-down.
            hide frame f-addoptions. 
            leave.
          end.
          if {k-accept.i} or ({k-return.i} and 
             frame-field = "v-default") 
          then do:
            assign v-typename
                   v-description
                   v-addonamt
                   v-addontype
                   v-default.
            find vasp where 
                 vasp.cono = g-cono and
                 vasp.shipprod = v-template no-lock no-error.
             if (frame-field = "v-default") then do:         
              find first zsdivaspmap where
                   zsdivaspmap.cono = g-cono and
                   zsdivaspmap.rectype = "GO" and     /* Addon Global Options */
                   zsdivaspmap.prod    = vasp.shipprod and
                   zsdivaspmap.seqno   = 0 and
                   zsdivaspmap.lineno  = 0 and
                   zsdivaspmap.typename = v-typename and
                   zsdivaspmap.user1    = "Yes" no-lock no-error.
              if avail zsdivaspmap and v-default = true then do:
                message "Only one Global option of this type can be DEFAULT".
                next.
              end.  
            end.
            find zsdivaspmap where
                 zsdivaspmap.cono = g-cono and
                 zsdivaspmap.rectype = "GO" and     /* Addon Global Options */
                 zsdivaspmap.prod    = vasp.shipprod and
                 zsdivaspmap.seqno   = 0 and
                 zsdivaspmap.lineno  = 0 and
                 zsdivaspmap.typename = v-typename and
                 zsdivaspmap.description = v-description
                 no-lock no-error.
       
            create t-options.
            assign t-options.typename = v-typename
                   t-options.description = v-description
                   t-options.odefault     = v-default.
        
            if not avail zsdivaspmap then do:
              create  zsdivaspmap.
              assign
                zsdivaspmap.cono = g-cono 
                zsdivaspmap.rectype = "GO"    /* Addon Global Options */
                zsdivaspmap.prod    = vasp.shipprod 
                zsdivaspmap.seqno   = 0 
                zsdivaspmap.lineno  = 0
                zsdivaspmap.typename = v-typename
                zsdivaspmap.description = v-description
                zsdivaspmap.addon[1]  = v-addonamt
                zsdivaspmap.addonty   = v-addontype
                zsdivaspmap.user1     = if v-default = true then
                                           "Yes"
                                        else
                                           "No".
            end.
          end.
        apply lastkey.
        end. /* Editing */
   
       leave AddLoop.
       end. /* AddLoop */
       run vaszfgopts(input v-typename,
                      input vasp.shipprod).

       find first t-options where t-options.typename = "<No Options Created>"
               no-error.
       if avail t-options then do:
         delete t-options.
         assign p-rowid = ?.
       end.
       else
         assign p-rowid = current-result-row("q-options").                  
    
       
       close query q-options.
       open query q-options preselect each t-options use-index tyinx.
       enable b-options with frame f-options.
       if p-rowid <> ? then do:
         v-status = self:select-row(p-rowid).
         get current q-options.
       end.  
       display b-options with frame f-options.
    end.
    else
    if {k-func7.i} and g-secure > 2 then do:
       assign v-typename = t-options.typename
              v-description = t-options.description
              v-default     = t-options.odefault.
              
        find zsdivaspmap where
             zsdivaspmap.cono = g-cono and
             zsdivaspmap.rectype = "GO" and  /* Addon Global Options */
             zsdivaspmap.prod    = vasp.shipprod and
             zsdivaspmap.seqno   = 0 and
             zsdivaspmap.lineno  = 0 and 
             zsdivaspmap.typename = v-typename and
             zsdivaspmap.description = v-description
             no-lock no-error.

       assign v-addonamt = zsdivaspmap.addon[1]
              v-addontype = zsdivaspmap.addonty
              v-default    = if zsdivaspmap.user1 = "yes" then
                                true
                             else
                                false.
              
       
       display 
         v-typename      
         v-description     
         v-addonamt
         v-addontype
         v-default
       with frame f-Changeoptions.
                
       Changeloop:
       do while true on endkey undo, leave ChangeLoop:
         update        
           v-typename      when 1 = 2
           v-description   when 1 = 2   
           v-addonamt
           v-addontype
           v-default
         with frame f-Changeoptions
         editing:
           readkey.       
           if {k-cancel.i} then do: 
             on cursor-up cursor-up.
             on cursor-down cursor-down.
             hide frame f-Changeoptions.
             leave.
           end.
           if {k-accept.i} or ({k-return.i} and 
              frame-field = "v-default") 
           then do:
             assign v-typename
                    v-description
                    v-addonamt
                    v-addontype
                    v-default.
             find vasp where 
                  vasp.cono = g-cono and
                  vasp.shipprod = v-template no-lock no-error.
             if (frame-field = "v-default") then do:         
               find first zsdivaspmap where
                    zsdivaspmap.cono = g-cono and
                    zsdivaspmap.rectype = "GO" and   /* Addon Global Options */                     zsdivaspmap.prod    = vasp.shipprod and
                    zsdivaspmap.seqno   = 0 and
                    zsdivaspmap.lineno  = 0 and
                    zsdivaspmap.typename = v-typename and
                    zsdivaspmap.user1    = "Yes" and
                    zsdivaspmap.description <> v-description no-lock no-error.

               if avail zsdivaspmap and v-default = true then do:
                 message "Only one Global option of this type can be DEFAULT".
                 next.
               end.  
             end.
         

           
             find zsdivaspmap where
                  zsdivaspmap.cono = g-cono and
                  zsdivaspmap.rectype = "GO" and  /* Addon Global Options */
                  zsdivaspmap.prod    = vasp.shipprod and
                  zsdivaspmap.seqno   = 0 and
                  zsdivaspmap.lineno  = 0 and 
                  zsdivaspmap.typename = v-typename and
                  zsdivaspmap.description = v-description
                  no-error.

             if avail zsdivaspmap then do:
               assign zsdivaspmap.addon[1]  = v-addonamt
                      zsdivaspmap.addonty   = v-addontype
                      zsdivaspmap.user1     = if v-default = true then
                                                "Yes"
                                              else
                                                "No".

                assign t-options.odefault     = v-default.

             end.
         end.
          apply lastkey.
         end. /* Editing */
       leave Changeloop.
       end. /* ChangeLoop */  
         run vaszfgopts(input v-typename,
                        input vasp.shipprod).
         self:refresh().
         display b-options with frame f-options.
    end.
    else
    if {k-func8.i} and g-secure > 2 then do: 
      assign v-typename = t-options.typename
              v-description = t-options.description.
              
        find zsdivaspmap where
             zsdivaspmap.cono = g-cono and
             zsdivaspmap.rectype = "GO" and /* Addon Global Options */
             zsdivaspmap.prod    = vasp.shipprod and
             zsdivaspmap.seqno   = 0 and
             zsdivaspmap.lineno  = 0 and 
             zsdivaspmap.typename = v-typename and
             zsdivaspmap.description = v-description
             no-lock no-error.

       assign v-addonamt = zsdivaspmap.addon[1]
              v-addontype = zsdivaspmap.addonty
                            v-default    = if zsdivaspmap.user1 = "yes" then
                                true
                             else
                                false.
      if not avail zsdivaspmap then 
        do:
          run err.p(1016).
          leave.      
        end.
      if avail zsdivaspmap then do:
        Deleteloop:
        do while true on endkey undo, leave DeleteLoop:
          message "Are you sure you want to Delete ?" 
                  skip 
                  "Option ? " + zsdivaspmap.description
          view-as alert-box question buttons yes-no
          update z as logical.
            if z then do: 
              delete t-options.
              
              find zsdivaspmap where
                   zsdivaspmap.cono = g-cono and
                   zsdivaspmap.rectype = "GO" and /* Addon Global Options */
                   zsdivaspmap.prod    = vasp.shipprod and
                   zsdivaspmap.seqno   = 0 and
                   zsdivaspmap.lineno  = 0 and 
                   zsdivaspmap.typename = v-typename and
                   zsdivaspmap.description = v-description
                   no-error.
             if avail zsdivaspmap then do:
               delete zsdivaspmap.
             end.  

              v-status = self:delete-selected-row(1). 
              scrn_lns = self:num-entries.
              if scrn_lns = 0 then do: 
                apply "window-close" to current-window. 
              end.
              self:refresh().
              display b-options with frame f-options.
              /*
              self:select-focused-row().
              assign p-rowid = current-result-row("q-optionspdsc").                           */
            end. /*** if z **/
        leave DeleteLoop.
        end. /* DeleteLoop */
      end.      
  
    run vaszfgopts(input v-typename,
                   input vasp.shipprod).
 
    end. /** k-func8.i **/
 


    if not {k-cancel.i} then 
      display  b-options with frame f-options.
 /*
    if {k-cancel.i} then do:
       run "clear-browse".
       apply "window-close" to current-window. 
    end.
 */
  end. /* do transaction */
end.  /*** any-key **/

/****** Main Block ****************/
                    
main:             
do while true on endkey undo, leave main:

  disable b-options with frame f-options.
  on cursor-up back-tab.
  on cursor-down tab.
 
  status input off.
  status default
   "Arrow Up/Down  - f6 - F7 - F8  or F4-Cancel"
     in window current-window.
  display                      
    v-template
  with frame f-options.
  display v-template with frame f-options.
  run "extract-recs".

  on cursor-up cursor-up.
  on cursor-down cursor-down.
  open query q-options preselect each t-options use-index tyinx.
  enable b-options with frame f-options.
  if g-secure > 2 then do:
    put screen row 20 col 13 color message   
    "   F6-Add  F7-Change F8-Delete                          ".
    pause 0.
  end.
  else do:
    put screen row 20 col 13 color message   
    "                                                         ".
    pause 0.
  end.
  apply "entry" to b-options in frame f-options.
  display b-options with frame f-options.
  wait-for window-close of current-window.
  on cursor-up back-tab.
  on cursor-down tab.
  close query q-options.
  leave.

end. /****** end do while loop******/

on cursor-up back-tab.
on cursor-down tab.
 
status default. 
{j-all.i "frame f-fab}

/***** procedure section *******/
procedure extract-recs:
  run "clear-browse".  


  find vasp where vasp.cono = 1 and 
                  vasp.shipprod = v-template no-lock no-error.

  
  for each zsdivaspmap where
           zsdivaspmap.cono = g-cono and
           zsdivaspmap.rectype = "GO" and  /* Addon Global Options */
           zsdivaspmap.prod    = vasp.shipprod and
           zsdivaspmap.seqno = 0 and
           zsdivaspmap.lineno = 0 no-lock:
    find t-options where 
         t-options.typename = zsdivaspmap.typename and
         t-options.description = zsdivaspmap.description
         no-lock no-error.
    if not avail t-options then do:
      create t-options.
      assign t-options.typename    = zsdivaspmap.typename
             t-options.description = zsdivaspmap.description
             t-options.odefault     = if zsdivaspmap.user1 = "yes" then
                                        true
                                      else
                                        false.
    end.        
  end.
  find first t-options no-lock no-error.
  if not avail t-options then do:
      create t-options.
      assign t-options.typename = "<No Options Created>"
             t-options.description = " "
             t-options.odefault = false.
  end.
end.
  
procedure clear-browse.
  close query q-options.
  for each t-options:
    delete t-options.
  end.  
  
  display  b-options  with frame f-options.
  apply "entry" to v-template in frame f-options.
end. /**** end procedure ***/

 
  
 

