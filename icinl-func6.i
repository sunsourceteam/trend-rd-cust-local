if v-recid[frame-line(f-icinl)] ne 0 then 
  do:
     find icenl where recid(icenl) = 
          v-recid[frame-line(f-icinl)] no-lock no-error.

     if {k-func6.i} then 
       do:
       if icenl.ordtype = "f" then
          do:
             assign g-title = "Value Add - Inquiry - Orders"
                    g-vano  = icenl.orderno
                    g-vasuf = icenl.ordersuf.
             run "vaio.p".
          end. 
       else
       if icenl.ordtype = "t" then
          do: 
             assign g-title = "Warehouse Transfer Inquiry - Order"
                    g-wtno  = icenl.orderno
                    g-wtsuf = icenl.ordersuf.
             run "wtio.p".
          end.
       else
       if icenl.ordtype = "p"  or icenl.ordtype = "a" then
          do: 
             assign g-title = "Purchase Order Inquiry - Order"
                    g-pono  = icenl.orderno
                    g-posuf = icenl.ordersuf.
             run "poio.p".
          end.
       else
       if icenl.ordtype = "o" then
          do:
             assign g-title    = "Order Entry Inquiry - Order"
                    g-orderno  = icenl.orderno
                    g-ordersuf = icenl.ordersuf.
             run "oeio.p".
          end.
       else
       if icenl.ordtype = "w" then
          do: 
             assign g-title = "Kit Production Inquiry - Orders" 
                    g-wono  = icenl.orderno
                    g-wosuf = icenl.ordersuf.
             run "kpio.p".
          end.
          put screen row 21 col 3 color message 
  "F6-Order Detail                                                            ".        end.
  end.
