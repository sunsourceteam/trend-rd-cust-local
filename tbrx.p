{p-rptbeg.i}


def new shared var p-begdt as date no-undo.
def new shared var p-enddt as date no-undo.



if sapb.rangebeg[1] = "" then
  p-begdt = today.
else
    do:
    v-datein = sapb.rangebeg[1].
    {p-rptdt.i}
    if string(v-dateout) = v-lowdt then
      p-begdt = date(v-lowdt).
    else  
      p-begdt = v-dateout.
    end.


if sapb.rangeend[1] = "" then
  p-enddt = today.
else
    do:
    v-datein = sapb.rangeend[1].
    {p-rptdt.i}
    if string(v-dateout) = v-highdt then
      p-enddt = date(v-highdt).
    else  
      p-enddt = v-dateout.
    end.


run tbrx99.p.
