/* SX 55 */
/*h*****************************************************************************
  INCLUDE      : p-aric.i
  DESCRIPTION  : A/R Inquiry Customers - Processing Include
  USED ONCE?   : yes
  AUTHOR       : r&d
  DATE WRITTEN :
  CHANGES MADE :
    11/06/91 mms; TB# 4948 JIT - change logic for orderdisp
    08/10/92 smb; TB# 7386 - age special transaction types
    09/08/92 smb; TB# 7779 - added ability to show sc's and mc's as future bal
    05/14/93 jrg; TB# 10573 added arsc.uncashbal.
    06/07/93 jrg; TB# ????? Do not highlight cod balance anymore.
             Show Future Balance as a negitive if it is negitive.
    08/02/94 bj; TB# 15927 UC not included in Credit Hold Calc
    05/29/97 mtt; TB# 23133 Develop Credit Control Center Feature
    02/09/98 enp; TB# 21198 Fixed problem with past period balances
    05/07/99 prb; PVTB# 2050 Ripped out the logic that calculates the message,
             in order to use it for GUI SX.E
    07/27/00 ct;  TB# e7852 A/R Multicurrency changes
    10/23/01 ajw; TB# e6997 Added Shipto Credit Limit
    01/08/02 mtt; TB# e10426 Add route info to Cust & Shipto Tabs
    01/11/02 mtt; TB# e10430 Add contact info to ARIC Customer tab
    07/16/03 sbr; TB# e16028 Ship to credit info
    08/05/03 sbr; TB# e18106 Shipto credit check: Hold per from cust
    09/11/03 emc; TB# e18269 Credit Check for Cust uses ARSS
    01/28/05 mwb; TB# e20669 Credit check totals by customer or shipto.
        Modified include to run for customer and shipto's with credit limit
        to find all credit limits on hold when running 'total exposure'.
    02/16/05 mwb; TB# e22009 Fix 'credit hold period x' message to display
        shipto. Confusion when shipto hold period overrides customer hold per.
    10/03/05 ds; TB# e23148 Display the ship to terms if the user enters a ship to.
*******************************************************************************/

hide message no-pause.

assign
    s-ordbal        = 0
    v-openbal       = 0
    s-phoneno       = {1}.phoneno     s-name      = {1}.name
    s-faxphoneno    = {1}.faxphoneno  s-pricetype = {1}.pricetype
    s-pricecd       = {1}.pricecd     s-addr[1]   = {1}.addr[1]
    s-disccd        = {1}.disccd      s-addr[2]   = {1}.addr[2]
    s-wodisccd      = {1}.wodisccd
    s-city          = {1}.city        s-state     = {1}.state
    s-zipcd         = {1}.zipcd       s-whse      = {1}.whse
    s-shipinstr     = {1}.shipinstr
    s-inbndfrtfl    = {1}.inbndfrtfl  s-outbndfrtfl = {1}.outbndfrtfl
    s-slsrep[1]     = {1}.slsrepin    s-slsrep[2]   = {1}.slsrepout
    s-taxablety     =  if {1}.taxablety = "y" then "Yes" else
                       if {1}.taxablety = "v" then "Variable" else "No"
    s-route         = {1}.route
    s-pocontctnm    = {1}.pocontctnm
    s-pophoneno     = {1}.pophoneno

    /*tb e7852 07/28/00 ct; A/R Multicurrency Assignments */
    s-currencyty    = arsc.currencyty
    s-shiptocredlimlab = if g-shipto ne "" and avail arss
                            then "Shipto Credit Limit:" else ""
    s-shiptocredlim = if g-shipto ne "" and
                         avail arss then string(arss.credlim) else ""
    s-message       = if arsc.currencyty <> ""
                      then "Domestic" else ""
    s-displayfl     = if g-shipto = "" then s-displayfl
                      else if avail arss and arss.credlim = 0 then yes
                      else no
    s-displaytxt    = if g-shipto ne "" and avail arss and arss.credlim ne 0
                          then "Ship To Credit"
                      else if s-displayfl = yes then "Customer Credit"
                      else "Total Exposure"
    v-futjobbal     = 0
    t-totjobbal     = 0
    v-jobscbal      = 0
    v-jobmcbal      = 0
    v-jobucbal      = 0
    v-jobcdbal      = 0
    v-jobbal        = 0.

/* Period Dates 1-5 */
{arscbdt.las}

/* Total Balance Labels - misc credit, service charge, etc. */
{arscblb.las}

/* s-totalbal calculated per stored values */
if g-shipto ne "" and avail arss and arss.credlim <> 0
then do:

    /* s-totalbal */
    {arbal.lca &file = "arss"
               &pre  = "job"}

    /* Ship To Fields */
    {aricj.las}

    assign
        v-openbal   = arss.jobordbal
        s-ordbal    = v-openbal
        t-totjobbal = t-totjobbal + v-futjobbal + v-openbal.
end.

else do:

    /* Customer */
    if s-displayfl = yes then do:
        {arbal.lca &pre = "cust"}
    end.
    /* Total Exposure */
    else do:
        {arbal.lca}
    end.

    /* das-12/04/2014 - included the assignment of s-credlim */
    assign
        v-openbal   = if s-displayfl = yes then arsc.custordbal
                      else arsc.ordbal
        s-ordbal    = v-openbal
        v-totalbal  = s-totalbal + s-ordbal
        t-totjobbal = v-totalbal
        s-credlim   = arsc.credlim.
end.
assign
    s-credrem  = if s-credlim <> 0 then
                     if t-totjobbal < 0 then s-credlim
                     else s-credlim - t-totjobbal
                 else 0.

/* Credit and on Hold Messages */
{2}
{aricmsg1.lpr}
/{2}* */

/* If the Ship to credit limit is loaded, then use the shipto hold period
    since driving off the ship to balances. If the credit limit is zero and
    the hold period is set on the ship to, then need to edit the ship to
    period balances based on the ship to hold code.  No shipt to credit
    limit or hold code, then use the customer hold code. */
assign
    v-holdshipfl = if g-shipto ne "" and avail arss and arss.credlim = 0 and
                       arss.holdpercd <> 0 then yes
                   else no
    v-holdpercd  = if g-shipto ne "" and avail arss and
                       (arss.credlim ne 0 or arss.holdpercd <> 0)
                            then arss.holdpercd
                   else if s-displayfl = yes then arsc.holdpercd
                   else 0
    v-holdperbal = 0.

/* Set Hold Period balances - overrides current balances for edits. */
if v-holdpercd ne 0 and v-holdshipfl = yes
then do:

    /* Rolls the Service Charge and Misc Credits into proper period
       balances if the Special AO option is set to roll */
    if arss.credlim = 0 then do:
        {arscbrl.lpr &file = "arss"
                     &pre  = "job"}
    end.

    /* Load into override fields for use in aricmsg2.lpr */
    do i = 1 to 5:
        assign
            v-holdperbal[i] = s-periodbals[i].
    end.
end.

/* Roll the balanaces and carry the temporary values via AO settings. All
   based on stored file values now. */
if g-shipto ne "" and avail arss and arss.credlim ne 0 then do:

    {arscbrl.lpr
         &periodbalassign = "if i >= v-holdpercd
                             then
                                 v-tempbal = v-tempbal + v-tempmc[i]."
         &tempbalassign   = "if sasc.arrollcd = ""m""
                             then
                                 v-tempbal = v-tempbal * -1."
         &finalassign     = "assign
                                 v-tempbal    = arss.jobmisccrbal
                                 s-futbal     = arss.jobfutinvbal."
         &comfutbal       = "/*"
         &file            = "arss"
         &pre             = "job"}
end.
else do:

    if s-displayfl = yes then do:

        {arscbrl.lpr
             &periodbalassign = "if i >= v-holdpercd
                                     then
                                     v-tempbal = v-tempbal + v-tempmc[i]."
             &tempbalassign   = "if sasc.arrollcd = ""m""
                                 then
                                     v-tempbal = v-tempbal * -1."
             &finalassign     = "assign
                                     v-tempbal    = arsc.custmisccrbal
                                     s-futbal     = arsc.custfutinvbal."
             &comfutbal       = "/*"
             &pre             = "cust"}
    end.
    else do:
        {arscbrl.lpr
             &periodbalassign = "if i >= v-holdpercd
                                     then
                                     v-tempbal = v-tempbal + v-tempmc[i]."
             &tempbalassign   = "if sasc.arrollcd = ""m""
                                 then
                                     v-tempbal = v-tempbal * -1."
             &finalassign     = "assign
                                     v-tempbal    = arsc.misccrbal
                                     s-futbal     = arsc.futinvbal."
             &comfutbal       = "/*"}

    end.
end.

/*tb 15927 07/27/94 bj; Include UC in v-tempbal */
/*tb 21198 02/09/98 enp; Changed to calculate off of screen balances
     to eliminate the hassles with rolling in misc credits, etc.  Also
     made a change to substract unapplied cash ONLY if you are not already
     rolling unapplied cash into the period balances. */

/* Total Exposure does not do the credit limit edit - must be individual */
if g-shipto ne "" or
  (g-shipto  = "" and s-displayfl = yes)
then do:

    /* Credit Limit and Hold Credit messages */
    {aricmsg2.lpr}

    /* Flag the Message with the 'Ship To' if required for clarity */
    cCredMsg = "".
    if s-msgtext[1] ne "" and g-shipto ne "" and avail arss and
        ((arss.holdpercd <> 0 and
            index(s-msgtext[1],"ON CREDIT HOLD HldPer") ne 0) or
         (arss.credlim ne 0 and
            index(s-msgtext[1],"ON CREDIT HOLD") ne 0)        or
         index(s-msgtext[1],"OVER JOB SALES LIMIT") ne 0      or
         index(s-msgtext[1],"NO LIEN PRELIM FILED") ne 0)
    then
        assign
            cCredMsg     = "ShpTo " + g-shipto + " " + s-msgtext[1]
            s-msgtext[1] = "".

    /* Total Exposure points to the record with the Credit Message */
    if lDisplayCredFl = no and s-msgtext[1] ne "" then
       cCredMsg = "Customer-" + s-msgtext[1].
end.

if s-msgtext[1] = "" and cCredMsg ne "" then
    s-msgtext[1] = cCredMsg.

if s-msgtext[1] ne "" then color display message s-msgtext[1]
        with frame f-aric.
else color display normal s-msgtext[1] with frame f-aric.

if index(s-msgtext[1],"ON CREDIT HOLD!") ne 0 then do:
    color display message s-credrem with frame f-aric.
    bell.
end.
else color display normal s-credrem with frame f-aric.


/******************** Descriptions ******************************/
{2}
if can-do("t,s,w,j",{1}.orderdisp) then
    s-disptext = if {1}.orderdisp = "t" then "TAG AND HOLD"
                 else if {1}.orderdisp = "s" then "SHIP COMPLETE"
                 else if {1}.orderdisp = "w" then "WILL CALL"
                 else if {1}.orderdisp = "j" then "JUST IN TIME"
                 else "".
else s-disptext = "".


    {w-sasta.i "t" arsc.termstype no-lock}
    assign s-terms   = if arsc.termstype = "" then "" else if avail sasta then
                       sasta.descrip else v-invalid.
if g-shipto <> "" then do:
    {w-sasta.i "t" arss.termstype no-lock}
    assign s-terms   = if arss.termstype = "" then "" else if avail sasta then
                   sasta.descrip else v-invalid.
end.
{w-sasta.i "s" {1}.shipviaty no-lock}
assign s-shipvia = if {1}.shipviaty  = "" then "" else if avail sasta then
                   sasta.descrip else v-invalid.


/* das - dso */
assign s-dsodays = dec(substring(arsc.user5,70,7)).

assign s-TandCdt = substr(arsc.user10,10,8).

/**************** Inactive Record *********************/
if not arsc.statustype then run err.p(8654).
color display message s-totbal[v-ttpos] with frame f-aric.

/{2}* */
