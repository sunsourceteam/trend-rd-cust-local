/* vaext.p 1.1 2/16/2005 */
/*****************************************************************************
  PROCEDURE      : vaext
  DESCRIPTION    : Value Add Production - Work Order Activity Maintenance 
  AUTHOR         : SunSource
  DATE WRITTEN   : 2/16/05
  CHANGES MADE   : 001-ta - multiple techs logged into repair at the same time.
   
*******************************************************************************/

{p-rptbeg.i}
{g-fabstg.i}
{g-ic.i}
{g-kp.i}
{g-inqpd.i}
{va.gva "new"}
define var  g-forcelufl as logical no-undo.
define var  g-luvalue as char format "x(24)" no-undo.
def var v-myframe     as c                no-undo.
/* Local Variables */
def var x-cursorup        as i format "999" init 501   no-undo.   
def var hh-input          as char format "x(12)"       no-undo.
def var bc-barcode        as char format "x(6)"        no-undo.
def var bc-stgcode        as char format "x(5)"        no-undo.
def var v-alpha           as char                      no-undo. 
def var v-bypassforcost   as logical no-undo.
def var v-question        as logical no-undo.
def var v-resetvasp       as logical no-undo.
def var w-asterisk        as char format "x" init "*"  no-undo.
def var s-seqno           like oeelk.seqno             no-undo.
def var s-overamt         as c format "x(20)"  init "" no-undo.
def var v-cnt             as i                 init 0  no-undo.
def var v-jobseqno        as  dec format "~zzzzzz9.99-" no-undo.
def var v-emailmsg        as char format "x(80)"       no-undo.
def var v-printed         as l                         no-undo.
def var v-error           as char format "x(78)"       no-undo.
def var v-errfl           as l                         no-undo.
def var v-inuse           as l                         no-undo.
def var q-start           as l                         no-undo.
def var s-error           as char format "x(78)"       no-undo.
def var s-vano            as int format "9999999"      no-undo.
def var s-dash            as c format "x" init "-"     no-undo.
def var s-vasuf           as int format "99"           no-undo.
def var v-wono            as int format "9999999"      no-undo.
def var v-wosuf           as int format "99"           no-undo.
def var v-stgint          as int format "99"           no-undo.
def var v-indate          as date                      no-undo.
def var v-intime          as char format "xx:xx:xx"    no-undo.
def var v-outdate         as date                      no-undo.
def var v-outtime         as char format "xx:xx:xx"    no-undo.
def var v-lendays         as integer                   no-undo.
def var v-lenhr           as integer                   no-undo.
def var v-lenmin          as integer                   no-undo.
def var v-lensec          as integer                   no-undo.
def var my-prompt         as integer                   no-undo.
def var myrecid           as recid                     no-undo.
def var m-stgint          as int format "99"           no-undo.
def var curr-stg          as int format "99"           no-undo.
def var v-multiple        as int                       no-undo.
def var q-pistons         as l                         no-undo.
def var q-blocks          as l                         no-undo.
def var q-other           as l                         no-undo.
def var qty-pistons       as int format "zz9"          no-undo. 
def var qty-blocks        as int format "zz9"          no-undo. 
def var qty-other         as int format "zz9"          no-undo. 
def var q-otherdesc       as char format "x(30)"       no-undo. 
def var f9-sw             as l                         no-undo.
def var f93               as l                         no-undo.
def var cost-sw           as l                         no-undo.
def var updtfl            as l                         no-undo.
def var g-technician      like swsst.technician        no-undo.
def var tech-hit          as logical                   no-undo.
def var user-ws           as c format "x(78)"          no-undo.
def var lst-tech          like g-technician            no-undo.  
def var s-printernm       as c format "x(10)"          no-undo.
def var s-printbars       as logical                   no-undo. 
def var s-stagetext       as c format "x(3)"           no-undo.
def var s-stagetext2      as c format "x(3)"           no-undo.
def var s-stageprt        as c format "x(8)" dcolor 2  no-undo.
def var v-openlit         as c format "x(4)" dcolor 2  no-undo.
def var d-vanonbr         as c format "x(12)"          no-undo.
def var d-custno          as dec format "zzzzzzz99999" no-undo.
def var d-custnm          as c format "x(24)"          no-undo.
def var d-prod            as c format "x(24)"          no-undo.
def var d-loc             as c format "x(4)"           no-undo.
def var d-tech            as c format "x(4)"           no-undo.
def var v-stagetext       as c extent 10               no-undo initial
["","Ord","Prt","Blt"," "," "," "," "," ","Can"].
def var s-prodk           like kpet.shipprod           no-undo.
def var s-stkqtyordk      as dec format "zzzzzz9.99-"  no-undo.
def var s-unitk           like kpet.unit               no-undo.
def var s-postdt          like kpet.enterdt            no-undo.
def var s-proddesc1       as c format "x(24)"          no-undo.
def var s-proddesc2       as c format "x(24)"          no-undo.
def var s-binlocx         like wmet.binloc             no-undo.
def var s-wstkqtyord      as dec format "zzzzzz9.99-"  no-undo.
def var s-refer           like kpet.refer              no-undo.
def var s-notesfl         like kpet.notesfl            no-undo.
def var s-qtyordk         like kpet.qtyord             no-undo.
def var s-qtyshipk        like kpet.qtyship            no-undo.
def var s-stkqtyshipk     like kpet.stkqtyship         no-undo.
def var s-stkunitk        like kpet.unit               no-undo.
def var s-stkunitk2       like kpet.unit               no-undo.
def var x-tagno           as c format "x(12)"          no-undo.
def var x-whse            as c format "x(4)"           no-undo.
def var s-tech            as c format "x(23)"          no-undo.
def var s-tech2           as c format "x(4)"           no-undo.
def var s-whse2           as c format "x(4)"           no-undo.
def var s-avail           as c format "x(3)"           no-undo.
def var s-binloc2         as c format "x(13)"          no-undo.
def var s-title           as c format "x(80)"          no-undo.
def var s-binlock2        as c format "x(13)"          no-undo.
def var s-qtyneeded       like oeelk.qtyneeded         no-undo.
def var s-qtyship         like oeelk.qtyship           no-undo.
def var s-unit            like oeelk.unit              no-undo.
def var s-stkqtyship      like oeelk.stkqtyship        no-undo.
def var s-techmgrtext     as c format "x(30)"          no-undo.
def var s-techmgrtextk    as c format "x(30)"          no-undo.
def var s-binlocxk        like wmet.binloc             no-undo.
def var s-wstkqtyordk     as dec format "zzzzzz9.99-"  no-undo.
def var s-reprint         as c format "x(21)"          no-undo.
def var s-rpttyp          as c format "x(1)"           no-undo.
def var s-stage           as c format "x(3)"           no-undo.
def var s-stgstat         as c format "x(6)"           no-undo.
def var v-firstfl         as logical                   no-undo.
def var v-invoutfl        as logical                   no-undo.
def var makecopy          as logical                   no-undo.
def var v-idx             as integer                   no-undo.
def var inx               as integer                   no-undo.
def var inx2              as integer                   no-undo.
def var printer-cmd       as char format "x(50)" init "".           
def var os-cmd-string     as char format "x(80)" init "".           
def var v-string          as char format "x(80)" init "".           
def var home-dir          as char format "x(30)" init "".    
def new shared var s-printer   like sasp.printernm    no-undo.     
def new shared var s-directory as char format "x(40)" no-undo.     
def new shared var z-filename  as c format "x(70)"    no-undo.     
def new shared var s-filename  as c format "x(70)"    no-undo.     
def var s-punchindt       as char format "x(8)"       no-undo.
def var s-punchintm       like ztmk.punchintm         no-undo.
def var s-punchotdt       as char format "x(8)"       no-undo.
def var s-punchottm       like ztmk.punchintm         no-undo.
def var t-ordqty          as decimal                  no-undo.
def var t-qty             as decimal                  no-undo.
def var t-tmout           as int                      no-undo.
def var prt-days          as int  format "zzz9"       no-undo.
def var t-tmin            as int                      no-undo.
def var timetot           as c format "x(8)"          no-undo.
def var d-time            as c format "99:99"         no-undo.
def var dtl-time          as decimal                  no-undo.
def var dtl-date-time     as char format "999:99"     no-undo.
def var t-hrs             as decimal                  no-undo. 
def var t-hrs-left        as decimal                  no-undo. 
def var t-min             as decimal                  no-undo.
def var prt-time          as char format "999:99"     no-undo.
def var prt-techid        like ztmk.techid            no-undo.
def new shared var t-time as decimal                  no-undo.
def new shared var t-days as decimal                  no-undo.  
define var v-headingfl    as logical.
define var v-myoutputty   as c format  "x(1)" init "f" no-undo.
define shared var v-tempprt as c format  "x(40)"      no-undo.
define var v-20size       as c format  "x(22)".
define var v-14size       as c format  "x(22)".
define var v-14bold       as c format  "x(22)".
define var v-compress     as c format  "x(22)".
def var h-modulenm as character                       no-undo.
def var h-menusel  as character                       no-undo.
def var h-bottom    like g-bottom                     no-undo.
def var h-inqtype   like sassm.inqtype                no-undo.
def var h-inqtitle  like sassm.inqtitle               no-undo.
def var h-inqproc   like sassm.inqproc                no-undo.
def var h-vaquoteno like g-vaquoteno                  no-undo.
def var o-vaquoteno like g-vaquoteno                  no-undo.
def var x-function  as char format "x(76)"            no-undo.
def var v-comments  as char format "x(60)" extent 16. 
def var v-comments2 as char format "x(60)" extent 16. 
def var v-comments3 as char format "x(60)" extent 16. 
def var fillit      as int                            no-undo.
def var p-pct       as dec format "999.99"            no-undo.
def var v-custno    like oeeh.custno                  no-undo.
def new shared var v-stage    as c format "x(8)"  dcolor 2  no-undo.
def new shared var s-prod     as character format "x(24)"   no-undo.
def new shared var v-esttdn-h as int format "99"      no-undo.
def new shared var v-esttdn-m as int format "99"      no-undo.
def new shared var v-estasy-h as int format "99"      no-undo.
def new shared var v-estasy-m as int format "99"      no-undo.
def new shared var v-esttst-h as int format "99"      no-undo.
def new shared var v-esttst-m as int format "99"      no-undo.
def new shared var v-custpo   like oeeh.custpo        no-undo.
def new shared var v-model    as character format "x(39)" no-undo.
def new shared var v-takenby  like oeeh.takenby       no-undo.
def new shared var v-shipto   like oeeh.shipto        no-undo.
def new shared var v-binloc   as char    format "x(6)"    no-undo.


def var t-csttdn-h as dec                             no-undo.
def var t-csttdn-m as dec                             no-undo.
def var t-cstasy-h as dec                             no-undo.
def var t-cstasy-m as dec                             no-undo.
def var t-csttst-h as dec                             no-undo.
def var t-csttst-m as dec                             no-undo.
def var s-trendtot    like vaeh.pndinvamt             no-undo. 
def var s-vaprodcost  like vaeh.pndinvamt             no-undo. 
def var s-pndtotal    like vaeh.pndinvamt             no-undo. 
def var s-pndinvamt   like vaeh.pndinvamt             no-undo.
def var s-pndextrnamt like vaeh.pndextrnamt           no-undo.
def var s-pndintrnest like vaeh.pndintrnest           no-undo.
def var s-pndintrnamt like vaeh.pndintrnamt           no-undo.
def var s-pndintrndsp like vaeh.pndintrnamt           no-undo.

define buffer v-sasp  for sasp.
define buffer v-ztmk  for ztmk. 
define buffer x-ztmk  for ztmk. 
define buffer fi-ztmk for ztmk. 
define buffer vm-ztmk for ztmk. 

define new shared temp-table t-lines no-undo
  field lineno      like oeel.lineno
  field seqno       like oeelk.seqno
  field specnstype  like oeel.specnstype
  field prod        like oeel.shipprod
  field astr1       as char format "x"
  field prodline    like icsl.prodline
  field descrip     as character format "x(24)"
  field icspdesc    as character format "x(24)"
  field gdesc       as character format "x(50)"
  field prctype     like oeel.pricetype
  field prodcat     like oeel.prodcat 
  field vendno      like apsv.vendno 
  field vendnm      like apsv.name
  field whse        like oeel.whse
  field altwhse     like oeel.whse
  field prodcost    like oeel.prodcost
  field extcost     like oeel.prodcost
  field gain        like oeel.prodcost 
  field listprc     like oeel.price
  field discpct     as dec format ">>9.99"
  field sellprc     like oeel.price
  field gp          as dec format ">>>9.99-"
  field qty         like oeel.qtyord
  field shipdt      as date format "99/99/99"
  field pdscrecno   like oeel.pdrecno
  field qtybrkty    as char format "x(1)"
  field kitfl       as logical format "yes/no"
  field unit        like oeel.unit 
  field icspstat    like icsp.statustype
  field vaspslid    as recid 
  field leadtm      as char format "xxxx"
  field f10leadtm   as char format "xxxx"
  field faxfl       as char format "x" 
  index ix1
    lineno
    seqno.

def var v-repositionfl as logical                     no-undo.    
def var v-repositionix as integer                     no-undo.    
def var v-fidx            as i                        no-undo.
def var v-fidx2           as i                        no-undo.
def var v-failtbl         as char format "x(40)" extent 15 no-undo initial
["Shaft spline wear                      ",
 "Shaft area seal worn                   ",
 "Contamination solid particle           ",
 "Contamination water/chemical           ",
 "Contamination fine particle            ",
 "Lack of lubrication low charge pressure",
 "Lack of lubrication dry start up       ",
 "Normal wear                            ",
 "Over speed                             ",
 "High case pressure                     ",
 "Excessive heat                         ",
 "Misalignment                           ",
 "Slippers pulled                        ",
 "Slippers rolled                        ",
 "Cavitation                             "].

define temp-table fails no-undo 
  field descrip   as char format "x(40)"  
  field nbr       as int
  field marked    as char format "x". 
  
define query q-fails for fails scrolling.
define browse b-fails query q-fails
       display fails.descrip   
               fails.marked 
       with 15 down centered overlay no-hide no-labels
       title "Repair Failure Descriptions". 
define frame f-fails 
  b-fails at row 1 col 1
  with width 50 row 2 col 17 overlay no-hide no-labels no-box.   

define new shared temp-table ltimes no-undo 
    field techid    like g-technician
    field orderno   like s-vano 
    field ordersuf  like s-vasuf
    field whse      like swsst.whse
    field ltm       as dec
    index k-timesidx
          orderno  
          ordersuf.

define new shared temp-table techs no-undo 
    field techid    like swsst.technician
    field whse      like swsst.whse 
    field hrsperwk  like swsst.hrsperwk
    field hrsperday like swsst.hrsperday
    index k-techidx
          whse 
          techid.

define query q-techs for techs scrolling.
define browse b-techs query q-techs
       display techs.techid  
               techs.whse 
               techs.hrsperwk 
               techs.hrsperday 
          with 17 down centered overlay no-hide no-labels
          title "   Tech Whse HrsWeek/HrsDay". 
define frame f-techs
  b-techs at row 1 col 1
  "  Up/down arrow to scroll  " at 1 dcolor 2    
  with width 29 row 1 col 25 overlay no-hide no-labels no-box.   

define new shared temp-table techin no-undo 
    field techid    like swsst.technician
    field orderno   as char format "x(12)" 
    field indate    as date 
    field intime    as char format "x(10)"
    field stage     as char format "xxxxxxxx"
    field custno    like arsc.custno 
    field partno    like icsp.prod
    field whse      like icsd.whse 
    index k-techidx
          whse    ascending
          custno  ascending
          indate  descending
          orderno ascending
          stage   ascending.
          
define query q-techin for techin scrolling.
define browse b-techin query q-techin
       display techin.orderno 
               techin.indate 
               techin.techid 
               techin.stage
               techin.custno 
               techin.partno 
 with 16 down centered overlay no-hide no-labels   
 title
 " RepairNo  CostingDt Lstusr StageCd       Custno       Partno             ". 
define frame f-techin
  b-techin at row 1 col 2
"                           Up/down arrow to scroll                           " 
   at 3 dcolor 2    
  with width 80 row 1 col 1 overlay no-hide no-labels no-box.   

define temp-table stg no-undo 
    field nbrcd   as int 
    field stgcd   as char format "xxxxxxxx" 
    field descrip as char format "x(19)"
    index k-stg 
          nbrcd
          stgcd.  
define query q-stg for stg scrolling.
define browse b-stg query q-stg
       display stg.stgcd 
               stg.descrip
          with 16 down centered no-labels
          title "Stage Codes". 
define frame f-stgcode
  b-stg at row 1 col 1
  "      Up/down arrow to scroll     " dcolor 2 at 1   
  with width 35 row 2 col 24 no-hide no-labels no-box.   

define temp-table lbrstgs no-undo 
    field techid    like swsst.technician
    field orderno   as char format "x(12)" 
    field indate    as date 
    field intime    like ztmk.punchintm 
    field otdate    as date 
    field ottime    like ztmk.punchintm 
    field stage     as char format "xxxx"
    field stgcd     as int
    field xrecid    as recid
    index k-techidx
          stgcd
          orderno 
          indate 
          intime.
define query q-lbrstgs for lbrstgs scrolling.
define browse b-lbrstgs query q-lbrstgs
       display lbrstgs.techid 
               lbrstgs.orderno
               lbrstgs.stage
               lbrstgs.indate 
               lbrstgs.intime 
               lbrstgs.otdate 
               lbrstgs.ottime 
          with 10 down centered overlay no-hide no-labels
    title " TechId RepairNo    Stg  InDate   InTime   OutDate  OutTime   ". 
define frame f-lbrstgs
  b-lbrstgs at row 1 col 9
  with width 80 row 9 col 1 overlay no-hide no-labels no-box.   

{lasersdi.lva new}
      
form 
  " Estimated Labor Time(s) Entry  " at 2 dcolor 2
  "Tdn>"       at 2
  v-esttdn-h   at 7 
  ":"          at 9 
  v-esttdn-m   at 10
  "Asy>"       at 13
  v-estasy-h   at 18 
  ":"          at 20
  v-estasy-m   at 21
  "Tst>"       at 24
  v-esttst-h   at 29
  ":"          at 31
  v-esttst-m   at 32
  skip(1)
  with frame f-labor no-box no-labels overlay row 13 col 20.

form 
  "Non-Genuine Parts?           Quantity?" at 1 dcolor 2
  q-pistons            at 1
  "Pistons y/n?"       at 5 
  qty-pistons          at 36 
  q-blocks             at 1
  "Blocks y/n?"        at 5 
  qty-blocks           at 36 
  q-other              at 1
  q-otherdesc          at 5 
  qty-other            at 36 
  skip(1)
  with frame f-genparts no-box no-labels overlay row 13 col 21.

define frame f-costlst 
 "           Service Repair Tag Ready for Costing" at 1 
 "Tech  RepairNo    Loc       Custno      Name                    Product" at 1
   skip(1) 
   d-tech             at 1
   g-vaquoteno        at 6 
   d-loc              at 19 
   d-custno           at 25 
   d-custnm           at 39
   d-prod             at 65 
   skip(1)
   "Operator:"        at 3 
   g-operinits        at 13
   "PunchIn  Time"    at 19  
   s-punchintm        at 33 
   "PunchIn  Date"    at 19 
   s-punchindt        at 33
   with down width 120 no-box no-labels.
 
form
  v-comments[1]  at 1  
  v-comments[2]  at 1  
  v-comments[3]  at 1  
  v-comments[4]  at 1  
  v-comments[5]  at 1  
  v-comments[6]  at 1  
  v-comments[7]  at 1  
  v-comments[8]  at 1  
  v-comments[9]  at 1  
  v-comments[10] at 1  
  v-comments[11] at 1  
  v-comments[12] at 1  
  v-comments[13] at 1  
  v-comments[14] at 1  
  v-comments[15] at 1
  v-comments[16] at 1  
  with frame f-bot2 no-labels overlay col 11 row 2 width 62
       title "             Repair Specification Notes                     ". 

form
    skip(1)
    "Repair:"                at 19
    g-vaquoteno              at 26 
    {f-help.i}
    "Current Stage:"         at 39
    s-stageprt               at 54 
    "VaNo:"                  at 19
    s-vano                   at 26
    {f-help.i}
    "-"                      at 33
    s-vasuf                  at 34
    "Status:"                at 39
    s-stgstat                at 47 
    s-tech                   at 54 
    "OperId:"                at 19
    g-technician             at 26
    {f-help.i}
    "Days: "                 at 42
    prt-days                 at 48 format "zzz"  
    "Stage:"                 at 19
    s-stagetext2             at 26 format "x(3)"
    {f-help.i}
    "HrsMins: "              at 39 
    prt-time                 at 48
    skip(1)
    "PunchIn  Time"          at 19  
    s-punchintm              at 33 
    "PunchIn  Date"          at 19 
    s-punchindt              at 33
    "PunchOut Time"          at 19  
    s-punchottm              at 33 
    "PunchOut Date"          at 19 
    s-punchotdt              at 33
    skip(8)
with frame f-vaexp width 78 no-labels row 1 col 2 no-hide overlay
     title
"                    Service RepairNo Stage Update                           ".

form 
  x-function               at 1 format "x(76)"  no-label dcolor 2 
with frame f-functions width 78 no-labels row 21 col 3 no-box no-hide.

form
   "Printer    RptTyp"   at 2  
   s-printernm           at 2  no-label
   {f-help.i}
   s-rpttyp              at 13 no-label
   "(R)epairNo/(T)ech"   at 1
   with frame f-printer width 21 row 5 col 31 overlay
        title "Report Printer".   

define frame f-detail 
   prt-techid           at 1
   d-vanonbr            at 6 
   s-stageprt           at 19 
   ztmk.punchindt       at 30
   ztmk.punchintm       at 40 
   ztmk.punchoutdt      at 50 
   ztmk.punchouttm      at 59
   t-days               at 68  format "zzz" 
   prt-time             at 71  
   with down width 80 no-box no-labels.
   
define frame f-labor-detail 
   x-ztmk.techid        at 1
   d-vanonbr            at 6 
   s-stageprt           at 19 format "xxx"
   x-ztmk.punchindt     at 24
   x-ztmk.punchintm     at 34 
   x-ztmk.punchoutdt    at 44 
   x-ztmk.punchouttm    at 53
   with width 64 col 9 row 7 no-labels overlay.

define frame f-labor-stage1 
  "Enter RepairNo pls?" at 1  
  g-vaquoteno           at 5 
  with width 22 col 28 row 9 no-labels overlay.
    
form  header 
    s-title                 at 1 
with frame f-hdr1 down width 80 no-box no-labels.

define frame f-inhide 
   hh-input at 1
with width 20 row 2 col 30 no-box no-labels.   

assign s-title =
  "Tech    Va No     StageCd    Indate     Intime   Outdate  Outtime Days HrsMins".
define frame f-hdr2
    s-title                 at 1 
with down width 80 no-box no-labels.

define frame f-vaext-bar
    "----------------------------------------------"
    "Tech:"                 at 1
    s-tech                  at 8
    "~033(0Y~033(s0p4.69h12.0v0s0b0T"  at 1
    bc-barcode              at 33 
    "~033(10U~033(s0p16.67h8.5v0s0b0T" at 70
    "Whse:"                 at 1  
    s-whse2                 at 6  
with down width 130 no-box no-labels.

define frame f-vaext-bar2
    "-------------------------------------------"
    "Stage Code:"           at 1
    s-stagetext             at 13
    "~033(0Y~033(s0p4.69h12.0v0s0b0T" at 1
    bc-stgcode              at 33 
    "~033(10U~033(s0p16.67h8.5v0s0b0T" at 70
with down width 130 no-box no-labels.
/*
on f12 of v-comments in frame f-bot2
do:
if lastkey  = 404 then do: 
 on cursor-up back-tab.
 on cursor-down tab.
 close query q-fails.
 leave.
end.
if frame-field = "v-comments" and {k-func12.i} then do:  
 for each fails exclusive-lock: 
   delete fails.
 end.  
 do v-fidx = 1 to 15: 
 create fails. 
 assign fails.descrip = v-failtbl[v-fidx]. 
end.  
open query q-fails preselect each fails.
enable  b-fails with frame f-fails. 
apply "entry" to b-fails in frame f-fails.
apply "focus" to b-fails in frame f-fails.
display b-fails with frame f-fails.
on cursor-up cursor-up.
on cursor-down cursor-down.
status default 
   " Up/Down arrow to scroll..".  
wait-for window-close of current-window. 
on cursor-up back-tab.
on cursor-down tab.
end. 
end. 

on any-key of b-fails do: 
  def buffer z-fails for fails.
  status default
   "   Apply 'm' to mark '*' - multiple note descriptions..".   
  if {k-cancel.i} or {k-jump.i} then do: 
    hide frame f-fails.
    apply "entry" to v-comments in frame f-bot2.
    apply "focus" to v-comments in frame f-bot2. 
    apply "window-close" to current-window.
    next.
 end.   
 else 
 if keylabel(lastkey) = "m" and 
  frame-name = "f-fails" then do: 
  if avail fails then do: 
   if fails.marked = "*" then 
    assign fails.marked = " ". 
   else 
    assign fails.marked = "*".
    b-fails:refresh().  
  end. 
 end.
 else 
 if {k-return.i} or {k-accept.i} then do: 
    /* if they key a new descrip and go to f12 to insert others get the first
       open space below **/
    if v-comments[v-fidx2] ne "" then do:   
     do v-idx = 16 to 1 by -1: 
      if v-comments[v-idx] ne "" then do:
       assign v-fidx2 = v-idx + 1.
       leave. 
      end. 
     end. 
    end.
    /***********/
    for each fails no-lock:
    if fails.marked = "*" then do: 
     if v-fidx2 <= 16 then 
      assign v-comments[v-fidx2] = fails.descrip.
     assign v-fidx2 = v-fidx2 + 1.
    end. 
   end. 
   find first z-fails where z-fails.marked = "*" no-lock no-error.  
   if not avail z-fails then do: 
    b-fails:select-focused-row().
    if v-fidx2 <= 16 then 
     assign v-comments[v-fidx2] = fails.descrip.
    else 
    /* find first blank line if all are filled */
    if v-fidx2 >= 16 then do: 
     do v-idx = 16 to 1 by -1: 
       if v-comments[v-idx] = "" then do:
        assign v-comments[v-idx] = fails.descrip.
        leave. 
       end. 
     end. 
    end. 
   end.
   display v-comments with frame f-bot2.  
   hide frame f-fails.
   apply "window-close" to current-window. 
   apply "entry" to v-comments in frame f-bot2.
   apply "focus" to v-comments in frame f-bot2. 
   next.
 end.
end. 

on any-key of b-techs
do:
   assign tech-hit = no.
   if {k-cancel.i} or {k-jump.i} then do: 
    display with frame f-vaexp. 
    hide frame f-techs.
    apply "window-close" to current-window.
   end.   
   else 
   if {k-return.i} or {k-accept.i} then do: 
    assign g-technician = if avail techs then 
                           techs.techid
                          else 
                           g-technician
           tech-hit     = yes.
    hide frame f-techs.
    display g-technician with frame f-vaexp. 
    apply "window-close" to current-window.
    next-prompt s-stagetext2 with frame f-vaexp.
   end.
end.   

on any-key of b-stg
do:
   if {k-cancel.i} or {k-jump.i} then do:
    hide frame f-stgcode.
    apply "window-close" to current-window.
   end.   
   else 
   if {k-return.i} or {k-accept.i} then do: 
    assign s-stagetext2 = stg.stgcd.
    hide frame f-stgcode.
    apply "window-close" to current-window.
    display s-stagetext2 with frame f-vaexp.
    next-prompt s-stagetext2 with frame f-vaexp. 
    next.
   end.
end.   

on f12 of g-vaquoteno in frame f-vaexp  
do: 
   for each techin exclusive-lock: 
       delete techin.
   end.  
   if g-vaquoteno = "" then 
   for each zsdivasp use-index k-custno where 
            zsdivasp.cono     = g-cono and 
            zsdivasp.stagecd < 14 no-lock: 
      if zsdivasp.stagecd = 5 or zsdivasp.stagecd = 6 then 
       find last ztmk use-index k-ztmk-sc1 where
                 ztmk.cono = g-cono and 
                 ztmk.ordertype = "sc" and 
                 ztmk.statuscd = 0     and 
                 ztmk.user1 = zsdivasp.repairno and 
                 ztmk.stagecd = zsdivasp.stagecd  
                 no-lock no-error.
       create techin.
       assign techin.techid    = zsdivasp.lstusr  
              techin.whse      = zsdivasp.whse
              techin.orderno   = entry(1,zsdivasp.repairno,"-") + "-" + 
                       left-trim(entry(2,zsdivasp.repairno,"-"),"0") 
              techin.indate    = zsdivasp.costingdt
              techin.custno    = zsdivasp.custno 
              techin.partno    = zsdivasp.partno 
              techin.stage     = if avail ztmk and ztmk.punchoutdt = ? and 
                                   (zsdivasp.stagecd = 5 or 
                                    zsdivasp.stagecd = 6) then     
                                  "ToBe" + "-" +
                                     v-stage2[zsdivasp.stagecd]
                                 else    
                                 if zsdivasp.stagecd > 0 then 
                                   v-stage2[zsdivasp.stagecd]
                                 else 
                                   "".
   end. /* each zsdivasp  */
   else do: 
    if num-entries(g-vaquoteno,"-") = 2 then do: 
     assign x-whse = entry(1,g-vaquoteno,"-"). 
    end. 
    for each zsdivasp use-index k-custno where 
             zsdivasp.cono     = g-cono and 
             zsdivasp.whse     = x-whse and 
             zsdivasp.stagecd  < 14 no-lock: 
      if zsdivasp.stagecd = 5 or zsdivasp.stagecd = 6 then 
       find last ztmk use-index k-ztmk-sc1 where
                 ztmk.cono = g-cono    and 
                 ztmk.ordertype = "sc" and 
                 ztmk.statuscd = 0     and 
                 ztmk.user1 = zsdivasp.repairno and 
                 ztmk.stagecd = zsdivasp.stagecd  
                 no-lock no-error.
       create techin.
       assign techin.techid    = zsdivasp.lstusr  
              techin.whse      = zsdivasp.whse
              techin.orderno   = entry(1,zsdivasp.repairno,"-") + "-" + 
                       left-trim(entry(2,zsdivasp.repairno,"-"),"0") 
              techin.indate    = zsdivasp.costingdt
              techin.custno    = zsdivasp.custno 
              techin.partno    = zsdivasp.partno 
              techin.stage     = if avail ztmk and ztmk.punchoutdt = ? and 
                                  (zsdivasp.stagecd = 5 or 
                                   zsdivasp.stagecd = 6) then     
                                  "ToBe" + "-" +
                                     v-stage2[zsdivasp.stagecd]
                                 else    
                                 if zsdivasp.stagecd > 0 then 
                                   v-stage2[zsdivasp.stagecd]
                                 else 
                                   "".
     end. /* each zsdivasp */
    end. /* else do */ 
    hide frame f-vaexp.
    open query q-techin preselect each techin.
    enable  b-techin with frame f-techin.
    display b-techin with frame f-techin.
    on cursor-up cursor-up.
    on cursor-down cursor-down.
    wait-for window-close of current-window. 
    on cursor-up back-tab.
    on cursor-down tab.
    hide frame f-techin.
    close query q-techin.
    display x-function with frame f-functions.           
end. 

on any-key of b-techin
do:    
  define buffer x-techin for techin. 
  if {k-cancel.i} or {k-jump.i} then do:
   apply "window-close" to current-window.
   display g-vaquoteno with frame f-vaexp.
   next-prompt g-vaquoteno with frame f-vaexp. 
   next.
  end.   
  else 
  if {k-return.i} or {k-accept.i} then do: 
   find first x-techin no-lock no-error. 
   assign g-vaquoteno = if avail x-techin then 
                         techin.orderno
                        else 
                         g-vaquoteno.
   apply "window-close" to current-window.
   display g-vaquoteno with frame f-vaexp.
   next-prompt g-vaquoteno with frame f-vaexp. 
   next.
  end.
end. 

on any-key of b-lbrstgs
do:    
  define buffer x-lbrstgs for lbrstgs. 
  find x-lbrstgs where recid(x-lbrstgs) = recid(lbrstgs)
      exclusive-lock no-error.
  if lastkey = 404 or not avail x-lbrstgs then do: 
   readkey pause 0.
   hide frame f-lbrstgs. 
   apply "window-close" to current-window.  
   return no-apply. 
  end. 
  if {k-accept.i} or {k-cancel.i} or {k-jump.i} then do:
   hide frame f-lbrstgs. 
   apply "window-close" to current-window.
   display g-vaquoteno with frame f-vaexp.
   next-prompt g-vaquoteno with frame f-vaexp. 
   next.
  end.   
  else 
  if {k-return.i} then do: 
   run makequoteno(input x-lbrstgs.orderno,output x-tagno).
   find last x-ztmk where recid(x-ztmk) = x-lbrstgs.xrecid
        exclusive-lock no-error.
   if avail x-ztmk then do:
    assign s-stageprt = v-stage2[x-ztmk.stagecd]
           d-vanonbr  = x-lbrstgs.orderno.
    display s-stageprt d-vanonbr x-ztmk.techid 
            x-ztmk.punchindt  x-ztmk.punchintm 
            x-ztmk.punchoutdt x-ztmk.punchouttm 
            with frame f-labor-detail.
    ztmkupdt: 
    do while true on endkey undo, leave ztmkupdt:
     update x-ztmk.punchoutdt
            x-ztmk.punchouttm
            with frame f-labor-detail.
     if input x-ztmk.punchoutdt < x-ztmk.punchindt then do: 
      assign v-error = "         PunchOut Date < PunchIn Date.".
      run zsdierrx.p(v-error,"yes"). 
      next-prompt x-ztmk.punchoutdt with frame f-labor-detail.
      next.
     end.
     if input x-ztmk.punchoutdt = x-ztmk.punchindt and 
      input x-ztmk.punchouttm < x-ztmk.punchintm then do: 
      assign v-error = "         PunchOut Time < PunchIn Time.".
      run zsdierrx.p(v-error,"yes"). 
      next-prompt x-ztmk.punchouttm with frame f-labor-detail.
      next.
     end.
     if dec(substring(x-ztmk.punchouttm,1,2)) > 24 then do: 
      assign v-error = "         PunchOut Hours > 24 - Enter <= 24 pls.".
      run zsdierrx.p(v-error,"yes"). 
      next-prompt x-ztmk.punchouttm with frame f-labor-detail.
      next.
     end. 
     if dec(substring(x-ztmk.punchouttm,3,2)) > 60 or 
        dec(substring(x-ztmk.punchouttm,5,2)) > 60 then do: 
      assign v-error =
               "         PunchOut minutes/seconds  > 60 - Enter <= 60 pls.".
      run zsdierrx.p(v-error,"yes"). 
      next-prompt x-ztmk.punchouttm with frame f-labor-detail.
      next.
     end. 
     if x-ztmk.punchoutdt ne ? then do: 
      assign x-lbrstgs.otdate  = x-ztmk.punchoutdt
             x-ztmk.punchoutdt
             x-ztmk.punchouttm 
             x-ztmk.statuscd   = 5.
      if substring(x-ztmk.punchouttm,1,2) = "" then 
       assign substring(x-ztmk.punchouttm,1,2) = "00".
      if substring(x-ztmk.punchouttm,3,2) = "" then 
       assign substring(x-ztmk.punchouttm,3,2) = "00".
      if substring(x-ztmk.punchouttm,5,2) = "" then 
       assign substring(x-ztmk.punchouttm,5,2) = "00".
      assign x-lbrstgs.ottime  = x-ztmk.punchouttm.
      if dec(substring(x-ztmk.punchouttm,1,2)) <= 0 then   
       assign x-lbrstgs.ottime  = substring(string(time,"hh:mm"),1,2) +
                                  substring(string(time,"hh:mm"),4,2) +
                                  substring(string(time,"hh:mm:ss"),7,2)
              x-ztmk.punchouttm = substring(string(time,"hh:mm"),1,2) +
                                  substring(string(time,"hh:mm"),4,2) +
                                  substring(string(time,"hh:mm:ss"),7,2).
     end. /* punchoutdt ne ? */ 
     leave ztmkupdt.
    end. /* do while true */
    hide frame f-labor-detail. 
    b-lbrstgs:refresh().                                       
   end.                                      
  end.
end. 

on entry of g-vaquoteno in frame f-vaexp
do:
 if o-vaquoteno ne g-vaquoteno then do: 
  assign s-punchintm = " " 
         s-punchintm = replace(s-punchintm,":","")
         s-punchindt = " "
         s-punchottm = " " 
         s-punchottm = replace(s-punchottm,":","")
         s-punchotdt = " "
         s-vano      = 0 
         s-vasuf     = 0
         s-stageprt  = ""
         s-tech      = "" 
         prt-days    = 0 
         prt-time    = "".
  display s-stagetext2  s-stageprt s-tech 
          s-stgstat    s-vano s-vasuf prt-days prt-time
          s-punchintm  s-punchindt
          s-punchottm  s-punchotdt with frame f-vaexp. 
 end. 
end. 
  */
on f7 of frame f-vaexp  anywhere
do: 
    assign x-function =
"      F7-JUMP(VAIO)      F8-SPcNotes       F9-Punchout      F10-RptDetail     ".
    display x-function with frame f-functions. 
    v-myframe = "vaio".
    run titlemorph.
    assign
      g-bottom = ""
      h-modulenm = g-modulenm
      h-menusel  = g-menusel
      g-modulenm = "VA"
      g-menusel  = "IO".
    {w-sassm.i "'vaio'" no-lock}
    do i = 1 to 5:
      assign
        h-inqproc[i]    = g-inqproc[i]
        g-inqproc[i]    = sassm.inqproc[i]
        h-inqtype[i]    = g-inqtype[i]
        g-inqtype[i]    =  if sassm.inqtype[i] = "j" then "p" 
                          else  sassm.inqtype[1]
        h-inqtitle[i]   = g-inqtitle[i]
        g-inqtitle[i]   = sassm.inqtitle[i]
        substring(g-bottom,(i * 15) - 12) =
                  if g-inqtitle[i] = "" then ""
                  else
                    (if i = 1 then "F6-" else
                     if i = 2 then "F7-" else
                     if i = 3 then "F8-" else
                     if i = 4 then "F9-" else "F10-")
                   + g-inqtitle[i].
     end.
    assign
      g-vano  = s-vano
      g-vasuf = s-vasuf.
    run vaio.p.
    v-myframe = "vaext".
    run titlemorph.
    do i = 1 to 5:
      assign
        g-inqproc[i]    = h-inqproc[i]
        g-inqtype[i]    = h-inqtype[i]
        g-inqtitle[i]   = h-inqtitle[i].
    end.
    assign
      g-modulenm = h-modulenm
      g-menusel  = h-menusel.

hide all. 
end.   

assign x-function =
"      F7-Jump(VAIO)      F8-SPcNotes       F9-Punchout      F10-RptDetail      ".

assign s-punchintm = " " 
       s-punchintm = replace(s-punchintm,":","")
       s-punchindt = " "
       s-punchottm = " " 
       s-punchottm = replace(s-punchottm,":","")
       s-punchotdt = " "
       v-error     = "" 
       s-vano      = 0 
       s-vasuf     = 0.
display s-punchintm s-punchindt
        s-punchottm s-punchotdt
        with frame f-vaexp.

define buffer ix-notes for notes.
find ix-notes where ix-notes.cono = g-cono and 
                    ix-notes.notestype = "zz" and
                    ix-notes.primarykey = "OneScreenFAB" and
                    ix-notes.secondarykey = g-operinits no-lock no-error.
if avail ix-notes then do: 
 assign g-technician = ix-notes.noteln[10]
        g-vaquoteno  = if g-technician ne "" and
                        ix-notes.noteln[11] ne "" then 
                        ix-notes.noteln[11] + "-"
                       else 
                        g-vaquoteno
        x-whse       = if ix-notes.noteln[11] ne "" then 
                        ix-notes.noteln[11]
                       else 
                        "".
end. 

assign q-start = false. 

{p-fabstg.i}

main:
do transaction while true on endkey undo, next main:
   assign x-function =
"      F7-Jump(VAIO)      F8-SPcNotes       F9-Punchout      F10-RptDetail     ".

  display x-function   with frame f-functions.
  assign s-punchottm = " " 
         s-punchottm = replace(s-punchottm,":","")
         s-punchotdt = " ".
  find first vasp where
             vasp.cono = g-cono       and 
             vasp.shipprod = x-tagno  
             no-lock no-error.
  if avail vasp and 
   (vasp.xxc4 ne "" and
    vasp.xxc4 = string(g-operinits,"x(4)") + " in --> " + g-currproc) then do: 
    run vasp-inuse.p(input x-tagno, 
                     input g-operinits, 
                     input "c",
                     input-output v-inuse).
    release zsdivasp.
    release vasp. 
    next main.
  end. 

  if v-error ne "" then do:
   run zsdierrx.p(v-error,"yes"). 
   assign v-error = "".
  end. 

  display g-technician
          g-vaquoteno
          s-punchintm
          s-punchindt
          s-punchottm
          s-punchotdt
          s-vano 
          s-vasuf
          with frame f-vaexp.
  if my-prompt = 1 then 
   next-prompt g-vaquoteno with frame f-vaexp. 
  else 
  if my-prompt = 2 then 
   next-prompt s-vano with frame f-vaexp. 
  else 
  if my-prompt = 3 then 
   next-prompt g-technician with frame f-vaex. 
  else 
  if my-prompt = 4 then 
   next-prompt s-stagetext2 with frame f-vaexp. 
  update g-vaquoteno
         s-vano       
         s-vasuf 
         g-technician
         s-stagetext2 format "x(3)"
         go-on(f7 f6 f8)
         with frame f-vaexp
 editing: 
  assign x-function =
  "      F7-Jump(VAIO)      F8-SPcNotes       F9-Punchout      F10-RptDetail     ~ ".
  display x-function with frame f-functions.

  if num-entries(g-vaquoteno,"-") = 2 and q-start = false and   
     entry(2,g-vaquoteno,"-") = ""  then do: 
   assign v-idx = length(g-vaquoteno) + 1
          q-start = true.
   g-vaquoteno:cursor-offset = v-idx. 
   apply "esc-cursor-right" to g-vaquoteno in frame f-vaexp.
  end. 
 readkey.
  
  assign v-bypassforcost = false.
   
  display x-function with frame f-functions.               
 
  if {k-cancel.i} or {k-jump.i} then do: 
   hide all no-pause.
   find first vasp where
              vasp.cono = g-cono         and 
              vasp.shipprod = x-tagno  /* and 
              vasp.whse = entry(1,x-tagno,"-") */
              no-lock no-error.
   if avail vasp and 
   (vasp.xxc4 ne "" and
    vasp.xxc4 = string(g-operinits,"x(4)") + " in --> " + g-currproc) then do: 
    run vasp-inuse.p(input x-tagno, 
                     input g-operinits, 
                     input "c",
                     input-output v-inuse).
    
   end. 
   release zsdivasp.
   leave main.
  end.
  
  if keylabel(lastkey) = "~{" then do:
    assign g-vaquoteno = "".
   next-prompt s-vano with frame f-vaexp.
   next.
  end.
  if keylabel(lastkey) = "~~" then do: 
   assign s-stagetext2 = "".
   next-prompt s-stagetext2 with frame f-vaexp.
   next.
  end.
  if keylabel(lastkey) = "^" then do:
   assign g-technician = "".
   next-prompt g-technician with frame f-vaexp.
   next.
  end.

  assign g-vaquoteno   = input g-vaquoteno
         s-vano        = input s-vano  
         s-vasuf       = input s-vasuf 
         g-technician  = input g-technician 
         s-stagetext2  = input s-stagetext2 
         g-vano        = s-vano 
         g-vasuf       = s-vasuf
         f9-sw         = no.
  
  /** sensing leave of s-vano **/
  if ({k-sdileave.i 
       &fieldname     = "s-vano"               
       &completename  = "s-vano"} and s-vano > 0 and g-vaquoteno = "") or 
    (({k-accept.i} or {k-func8.i} or {k-func9.i} or {k-func10.i}) and 
       s-vano > 0 and g-vaquoteno = "") then do: 
   if avail ix-notes and ix-notes.noteln[11] ne "" then 
    assign x-whse = ix-notes.noteln[11]. 
   if s-vano > 0 then do: 
     find first vaeh where
                vaeh.cono  = g-cono  and 
                vaeh.vano  = s-vano  and 
                vaeh.vasuf = s-vasuf and      
                vaeh.user2 ne "" 
                no-lock no-error.
     if avail vaeh then do: 
       assign g-vaquoteno = entry(1,vaeh.user2,"-") + "-" + 
              left-trim(entry(2,vaeh.user2,"-"),"0")
              my-prompt   = 2. 
       run makequoteno(input g-vaquoteno,output x-tagno).
       if v-errfl = yes and g-vaquoteno ne "" then do: 
         assign v-error = "  Invalid RepairNo - re-enter pls ".
         run zsdierrx.p(v-error,"yes"). 
         display g-vaquoteno with frame f-vaexp.
         next-prompt g-vaquoteno with frame f-vaexp.
         next main. 
       end.
       display g-vaquoteno with frame f-vaexp.
       run fetch-info.        
     end. 
   end.
  end. 

  /** sensing leave of g-vaquoteno **/
  if {k-sdileave.i 
      &fieldname     = "g-vaquoteno"               
      &completename  = "g-vaquoteno"} then do:   
   run makequoteno(input g-vaquoteno,output x-tagno).
   if v-errfl = yes then do: 
     run zsdierrx.p(v-error,"yes"). 
     display g-vaquoteno with frame f-vaexp.
     assign my-prompt = 1. 
     next-prompt g-vaquoteno with frame f-vaexp.
     next main. 
   end. 
   find first vasp where
              vasp.cono = g-cono         and 
              vasp.shipprod = x-tagno 
              no-lock no-error.
   if not avail vasp then do: 
     assign v-error = "  Fabrication Quote notfound - " + g-vaquoteno.
     run zsdierrx.p(v-error,"yes"). 
     display g-vaquoteno with frame f-vaexp.
     assign my-prompt = 1. 
     next-prompt g-vaquoteno with frame f-vaexp.
     next main. 
   end. 
   run fetch-info.        
  end. 

  if {k-func8.i} then do: 
   assign x-tagno = "".
   if g-vaquoteno ne "" then do: 
     run makequoteno(input g-vaquoteno,output x-tagno).
     if v-errfl = yes then do:  
       run zsdierrx.p(v-error,"yes"). 
       display g-vaquoteno with frame f-vaexp.
       next-prompt g-vaquoteno with frame f-vaexp.
       next main. 
     end. 
   end.
   if g-vaquoteno = "" and s-vano = 0 then 
    next main. 
   run bottom2. 
   next main.
  end. 
  
  if frame-field = "s-stagetext2" and {k-func12.i} then do: 
     for each stg exclusive-lock: 
         delete stg.
     end.  
     for each t-stages no-lock:
       create stg.  
       assign stg.nbrcd    = t-stages.stage
              stg.stgcd    = t-stages.shortname
              stg.descrip  = t-stages.description. 
     end.
     open query q-stg preselect each stg.
     enable  b-stg with frame f-stgcode.
     display b-stg with frame f-stgcode.
     assign my-prompt = 4.
     on cursor-up cursor-up.
     on cursor-down cursor-down.
     wait-for window-close of current-window. 
     on cursor-up back-tab.
     on cursor-down tab.
     close query q-stg.
     hide frame f-stgcode.
     display s-stagetext2 with frame f-vaexp.
     next-prompt s-stagetext2 with frame f-vaexp.
     next.
  end. 
  else     
  if {k-accept.i} or {k-func9.i} then do:  
    run makequoteno(input g-vaquoteno,output x-tagno).
    if v-errfl = yes then do: 
     run zsdierrx.p(v-error,"yes"). 
     display g-vaquoteno with frame f-vaexp.
     assign my-prompt = 1. 
     next-prompt g-vaquoteno with frame f-vaexp.
     next. 
    end. 
    find first vasp where                              
               vasp.cono = g-cono       and          
               vasp.shipprod = x-tagno  and          
               vasp.whse = entry(1,x-tagno,"-")      
               no-lock no-error.                       
    if avail vasp then do:            
     run vasp-inuse.p(input x-tagno,                 
                      input g-operinits,               
                      input "x",                       
                      input-output v-inuse).           
     if not v-inuse then do:                           
      run vasp-inuse.p(input x-tagno,                
                       input g-operinits,              
                       input "a",                      
                       input-output v-inuse).          
      find first vasp where                              
                 vasp.cono = g-cono       and          
                 vasp.shipprod = x-tagno  and          
                 vasp.whse = entry(1,x-tagno,"-")      
                 no-lock no-error.                       
     end.
     else 
     if v-inuse and                                          
       (vasp.xxc4 ne "" and                                   
        vasp.xxc4 ne string(g-operinits,"x(4)") +             
       " in --> " + g-currproc) then do:                     
      assign v-error = " RepairNo is in-use by operator: " + 
                         string(vasp.xxc4,"x(20)").          
      run zsdierrx.p(v-error,"yes").                         
      assign my-prompt = 1.
      next main.
     end.  
    end.
    assign v-printed    = false 
           s-punchottm  = " " 
           s-punchottm  = replace(s-punchottm,":","")
           s-punchotdt  = " "
           g-vaquoteno  = input g-vaquoteno 
           g-technician = input g-technician 
           s-stagetext2 = input s-stagetext2.
    display s-punchottm s-punchotdt with frame f-vaexp.
    if lastkey = 309 and input s-stagetext2 = ""  then do:  
     run fetch-info.
     display s-stagetext2 with frame f-vaexp. 
    end.     
    if (avail vasp and vasp.user7 = 14) or 
       s-stagetext2 = "can" then do:   
      assign v-error = "  Cancelled repairNo - cannot change. ".
      run zsdierrx.p(v-error,"yes"). 
      assign my-prompt = 1.
      next-prompt s-stagetext2 with frame f-vaexp.
      next main.
    end.  
    if (s-stagetext2 = "tdn" or s-stagetext2 = "asy" or
        s-stagetext2 = "tst") and 
       (g-technician = "" ) then do: 
       assign v-error = "  TechId required - LaborStage ".
       run zsdierrx.p(v-error,"yes"). 
       assign my-prompt = 3.
       next-prompt g-technician with frame f-vaexp.
       next main.
    end.
    assign v-firstfl = no.
    do inx = 1 to 16: 
     if v-stage2[inx] = s-stagetext2 then do:
      assign v-firstfl = yes 
             v-stgint  = inx. 
      leave.
     end.
    end.
    if (not v-firstfl or s-stagetext2 = "") and {k-func9.i} then do:
      assign v-error = if {k-func9.i} and s-stagetext2 = "" then 
                        "  Open labor tech/stage notfound - F9 not applicable" 
                       else
                        "  Open stage notfound - cannot close.".
      run zsdierrx.p(v-error,"yes"). 
      assign my-prompt = 3. 
      next-prompt s-stagetext2 with frame f-vaexp.
      next main.
    end.
    find first swsst use-index k-swsst where
               swsst.cono       = g-cono and
               swsst.technician = g-technician           
               no-lock no-error.
    if ((not avail swsst and g-technician ne "") and g-technician ne "admn")  
      then do: 
     assign v-error = "  Invalid technician - re-enter pls ".
     run zsdierrx.p(v-error,"yes"). 
     assign my-prompt = 3. 
     next-prompt g-technician with frame f-vaexp.
     next main.
    end.
    {vaeh.gfi &vano  = s-vano
              &vasuf = s-vasuf 
              &lock  = "no"}
    if ((v-stgint = 4 and v-stgint ne 15 and v-stgint ne 16 and
        {k-func9.i}) or  
        (v-stgint > 4 and v-stgint ne 15 and v-stgint ne 16 and
        {k-accept.i})) and g-vaquoteno ne "" then do:
     def var blank-desc as logical no-undo. 
     assign blank-desc = no.  
     for each vaspsl use-index k-vaspsl where 
              vaspsl.cono     = g-cono  and 
              vaspsl.shipprod = x-tagno and 
              vaspsl.nonstockty ne "l" 
              no-lock:                                                     
         if vaspsl.compprod = "" then 
            assign blank-desc = yes. 
     end.
     if blank-desc = yes then do:
      assign v-error = "  Blank items-can't leave costing. ".
      assign my-prompt = 4. 
      run zsdierrx.p(v-error,"yes"). 
      next-prompt s-stagetext2 with frame f-vaexp.
      next main.
     end.
     find first vasp where
                vasp.cono = g-cono       and 
                vasp.shipprod = x-tagno  and 
                vasp.whse = entry(1,x-tagno,"-")
                no-lock no-error.
       if avail vasp then 
        assign s-prod = vasp.refer.        
       {w-icsp.i s-prod no-lock}
       if s-prod = "" then do: 
        assign v-error = "  Valid Part# required-pls enter". 
        run zsdierrx.p(v-error,"yes"). 
        assign my-prompt = 1. 
        next-prompt g-vaquoteno with frame f-vaexp.
        next main. 
       end.  
    end.  /*  v-stgint = 4 and */ 
    if v-stgint > 4 and v-stgint ne 15 and v-stgint ne 16 and 
       g-vaquoteno ne "" then do:
     define buffer cs-ztmk for ztmk.
     find first cs-ztmk use-index k-ztmk-sc2 where
                cs-ztmk.cono      = g-cono       and 
                cs-ztmk.ordertype = "sc"         and 
                cs-ztmk.user1     = x-tagno      and 
                cs-ztmk.stagecd   > 4            
                no-lock no-error. 
     if not avail cs-ztmk and v-stgint ne 4 and 
        v-stgint ne 15 and v-stgint ne 16 then do: 
      assign v-error = "  Incomplete costing - invalid transaction.".
      run zsdierrx.p(v-error,"yes"). 
      assign my-prompt = 4. 
      next-prompt s-stagetext2 with frame f-vaexp.
      next main.
     end. 
    end. /* v-stgint > 4 */
    if v-stgint <= 6 and g-vaquoteno ne "" and 
       not {k-func9.i} then do:
     define buffer ca-ztmk for ztmk.
     find first ca-ztmk use-index k-ztmk-sc2 where
                ca-ztmk.cono      = g-cono       and 
                ca-ztmk.ordertype = "sc"         and 
                ca-ztmk.user1     = x-tagno      and 
                ca-ztmk.stagecd   = 6            and 
                ca-ztmk.orderno   > 0  
                no-lock no-error. 
     if avail ca-ztmk and v-stgint <= 6 then do:
      assign v-error =
      "  Repair has been converted - cannot perform backward staging.".
      run zsdierrx.p(v-error,"yes"). 
      assign my-prompt = 1. 
      next-prompt s-stagetext2 with frame f-vaexp.
      next main.
     end. 
    end. /* v-stgint < = 6 */
    /** evaluate order to check if labor stage etc **/
    assign s-error = v-error
           v-error = "".
    run evaluate_staging. 
    if v-error = "" then 
     assign v-error = s-error.
    if v-error ne "" then do:
     run zsdierrx.p(v-error,"yes"). 
     assign my-prompt = 3. 
     next main. 
    end. 
  end. /* accept or f9 editing  */ 
  if {k-func9.i} then do:
   assign f9-sw      = yes 
          s-stageprt = v-stage2[v-stgint].
   
   if v-stgint = 3 or v-stgint = 10 or v-stgint = 11 then do: 
     assign updtfl = no. 
     if v-multiple > 0 then do: 
      message  "   Move " + g-technician + " individual "  
              skip                       
               " labor stage to closed status? "
      view-as alert-box question buttons yes-no 
      update v-question.
     end.
     assign f93 = no. 
     if v-question and v-stgint = 3 and v-multiple = 1 then do: 
       message  "  Move repair to costing stage?  "  
                skip                       
        g-technician + "-> " + s-stageprt + " was last open stage. "
       view-as alert-box question buttons yes-no 
     update f93.
     end.
     if f93 then do:  
      assign cost-sw = yes
             updtfl  = yes. 
      find first vasp where
                 vasp.cono = g-cono       and 
                 vasp.shipprod = x-tagno  and 
                 vasp.whse = entry(1,g-vaquoteno,"-")
                 no-lock no-error.
      if avail vasp and v-multiple = 1 and  
       ((dec(substring(vasp.user4,50,4)) <= 0) or     
        (dec(substring(vasp.user4,54,4)) <= 0) or     
        (dec(substring(vasp.user4,58,4)) <= 0)) then do:  
         assign v-error = "  Labor time(s) not completed ".
         run zsdierrx.p(v-error,"yes"). 
         lbrtst: 
         do while true on endkey undo, leave lbrtst: 
          run labortest(input recid(vasp),
                        input-output updtfl). 
          if lastkey = 404 or updtfl then do: 
           leave lbrtst. 
          end. 
         end. 
         display g-vaquoteno s-stagetext2 with frame f-vaexp. 
         if updtfl = no then do:  
          assign cost-sw = no.
          assign my-prompt = 1. 
          run complete_tagstg(recid(v-ztmk)).
          assign v-error = "  F9-Punchout Completed    ". 
          run zsdierrx.p(v-error,"yes"). 
          next main. 
         end.
      end. /* v-multiple = 1 */
     end. /* if f93 */
     else 
     if not f93 or updtfl = no then 
      assign f9-sw     = if v-stgint = 3 then 
                          no 
                         else 
                          f9-sw
             cost-sw   = no.
     if v-question and cost-sw then do: 
      if v-stgint = 3 then 
       run genuine-audit.
      run complete_tagstg(recid(v-ztmk)).
      assign v-error = "  F9-Punchout Completed    ". 
      run zsdierrx.p(v-error,"yes"). 
      assign my-prompt = 1.
      next main.   
     end.  
     else
     if v-question and not cost-sw then do: 
      run complete_tagstg(recid(v-ztmk)).
      assign v-error = "  F9-Punchout Completed    ". 
      run zsdierrx.p(v-error,"yes"). 
      assign my-prompt = 1.
      next main.   
     end.  
     else do: 
      assign cost-sw = no
             f9-sw   = if v-stgint = 3 then 
                        no
                       else 
                        f9-sw.
      assign s-punchintm = " " 
             s-punchintm = replace(s-punchintm,":","")
             s-punchindt = " "
             s-punchottm = " " 
             s-punchottm = replace(s-punchottm,":","")
             s-punchotdt = " ".
       display s-punchintm s-punchindt
               s-punchottm s-punchotdt with frame f-vaexp.
       assign my-prompt = 1.
       next main.
     end.
   next main.
   end. /* if v-stg = stage 3 or 10 or 11  */
   
   /** punch out open stage regardless if not tdn asy or tst stage  **/
   assign v-error    = "  F9-Punchout Completed    "
          v-question = yes. 
   run complete_tagstg(recid(v-ztmk)).
   run zsdierrx.p(v-error,"yes"). 
   assign my-prompt = 1.
   display s-punchottm s-punchotdt with frame f-vaexp.
   next main.
  end. /* if f9 */
  else 
  if {k-func10.i} then do: 
   if g-vaquoteno ne "" then do:
    run makequoteno(input g-vaquoteno,output x-tagno).
    if v-errfl = yes then do: 
     run zsdierrx.p(v-error,"yes"). 
     display g-vaquoteno with frame f-vaexp.
     next-prompt g-vaquoteno with frame f-vaexp.
     next main. 
    end. 
   end.
   assign s-rpttyp = "".
   run prt-routine.
   assign x-function =
"      F7-Jump(VAIO)      F8-SPcNotes       F9-Punchout      F10-RptDetail     ".
   display x-function with frame f-functions.
   next main.
  end. /** if f10 **/
  if {k-accept.i} and (v-stgint = 3 or v-stgint = 10 or v-stgint = 11) then do:
   def buffer ll-ztmk for ztmk. 
   find last  ll-ztmk use-index k-ztmk-sc1 where
              ll-ztmk.cono      = g-cono       and 
              ll-ztmk.ordertype = "sc"         and 
              ll-ztmk.statuscd  = 0            and  
              ll-ztmk.user1     = x-tagno      and 
              ll-ztmk.techid    = (if (v-stgint = 3 or v-stgint = 10 or 
                                       v-stgint = 11) then 
                                     g-technician 
                                   else    
                                    "admn")   
              no-lock no-error. 
   /* 001-ta */
   if avail ll-ztmk and ll-ztmk.techid ne g-technician and 
     (ll-ztmk.stagecd = 3 or ll-ztmk.stagecd = 10 or
      ll-ztmk.stagecd = 11) then do:   
      assign v-error = "  TechRepair MisMatch " + g-technician + " - "
                                                + ll-ztmk.techid.
      run zsdierrx.p(v-error,"yes"). 
      assign my-prompt = 3. 
      next-prompt g-technician with frame f-vaexp.
      next main.
   end.
  end.  
  if {k-accept.i} and avail ztmk and g-vaquoteno ne ""  then do:
    assign g-technician = input g-technician. 
    find first vasp where
               vasp.cono = g-cono       and 
               vasp.shipprod = x-tagno  and 
               vasp.whse = entry(1,g-vaquoteno,"-")
               no-error.
    if avail vasp and v-stgint = 4 and
     ((dec(substring(vasp.user4,50,4)) <= 0) or     
      (dec(substring(vasp.user4,54,4)) <= 0) or     
      (dec(substring(vasp.user4,58,4)) <= 0)) then do: 
       message  "  Open new costing stage?   "  
               skip                       
                " Is all labor accounted for? "
       view-as alert-box question buttons yes-no 
       update w2 as logical.
       if not w2 then do: 
        assign my-prompt = 4. 
        next main. 
       end.  
       if w2 then do: 
        assign v-multiple = 0.  
        for each vm-ztmk use-index k-ztmk-sc1 where
                 vm-ztmk.cono      = g-cono  and 
                 vm-ztmk.ordertype = "sc"    and 
                 vm-ztmk.statuscd  = 0       and  
                 vm-ztmk.user1     = x-tagno and 
                 vm-ztmk.stagecd   = v-stgint  
                 no-lock:
         assign v-multiple = v-multiple + 1.
        end.         
        if v-multiple > 1 then do: 
         assign v-error = "  Multiple " + s-stageprt + 
                " labor stages open - close all Tech(s) " + s-stageprt + 
                " labor to continue.".
         assign my-prompt = 3. 
         run zsdierrx.p(v-error,"yes"). 
         next-prompt s-stagetext2 with frame f-vaexp.
         next main.
        end.   
        assign updtfl  = yes
               cost-sw = yes.
        assign h-vaquoteno = g-vaquoteno. 
        lbrtst2:
        do while true on endkey undo, leave lbrtst2: 
         run labortest(input recid(vasp), 
                       input-output updtfl). 
        
         if lastkey = 404 then do: 
          assign cost-sw = no.
          leave lbrtst2. 
         end. 
         else 
         if updtfl then  
          assign cost-sw = yes.
         else 
          assign cost-sw = no. 
         leave lbrtst2.
        end. /* do while */ 
        display g-vaquoteno s-stagetext2 with frame f-vaexp. 
        if updtfl = no then do:  
         assign cost-sw = no.
         assign my-prompt = 1. 
         next main. 
        end.
        else do: 
        /** labor has been filled in and we can create a costing stage here **/
         assign v-bypassforcost = true.
        end.
       end.
       else do: 
        bell.
        next-prompt s-stagetext2 with frame f-vaexp.
        next main.
       end. 
    end. /* avail vasp and labor not complete */
    def buffer e-ztmk for ztmk.
    find first e-ztmk use-index k-ztmk-sc2 where
               e-ztmk.cono      = g-cono       and 
               e-ztmk.ordertype = "sc"         and
               e-ztmk.user1     = x-tagno      and 
               e-ztmk.stagecd   = 8            and  
               e-ztmk.statuscd  = 0             
               no-lock no-error.
    if avail e-ztmk then do:
     assign v-error = "  RepairNo 'PIN' Open Stage Found".
     run zsdierrx.p(v-error,"yes"). 
     next-prompt s-stagetext2 with frame f-vaexp. 
     next main.
    end.
    find last ztmk use-index k-ztmk-sc1 where
              ztmk.cono      = g-cono       and 
              ztmk.ordertype = "sc"         and 
              ztmk.statuscd  = 0            and 
              ztmk.user1     = x-tagno      and 
              ztmk.techid    = (if (v-stgint = 3 or v-stgint = 10 or 
                                    v-stgint = 11) then 
                                  g-technician 
                                else    
                                  "admn")   and 
              ztmk.stagecd  ne v-stgint       
              no-lock no-error.
    if not avail ztmk and not {k-func9.i} then do: 
      find last ztmk use-index k-ztmk-sc1 where
                ztmk.cono      = g-cono       and 
                ztmk.ordertype = "sc"         and 
                ztmk.statuscd  = 0            and 
                ztmk.user1     = x-tagno      and 
                ztmk.techid    = (if (v-stgint = 3 or v-stgint = 10 or 
                                      v-stgint = 11) then 
                                     g-technician 
                                  else    
                                    "admn")   and 
                ztmk.stagecd   = v-stgint       
                no-lock no-error.
    end.
    if avail ztmk and ztmk.stagecd ne v-stgint then 
     assign curr-stg = ztmk.stagecd.  
    if not avail ztmk and not {k-func9.i} and v-question then do:
       if v-stgint = 4 then 
        run genuine-audit.
       run get_tagseqno(output v-jobseqno).
       create ztmk. 
       assign ztmk.jobseqno  = v-jobseqno
              ztmk.cono      = g-cono        
              ztmk.ordertype = "sc" 
              ztmk.user1     = x-tagno        
              ztmk.stagecd   = v-stgint   
              ztmk.statuscd  = 0 
              ztmk.orderno   = if v-stgint > 6 then 
                                s-vano 
                               else 
                                0
              ztmk.ordersuf  = 0
              ztmk.techid    = (if (v-stgint = 3 or v-stgint = 10 or 
                                    v-stgint = 11) then 
                                  g-technician 
                                else                                         
                                 "admn")    
              ztmk.punchindt = today
              ztmk.punchintm = substring(string(time,"hh:mm"),1,2) +
                               substring(string(time,"hh:mm"),4,2) +
                               substring(string(time,"hh:mm:ss"),7,2)
              ztmk.operinit  = g-operinit  
              ztmk.transtm   = if ztmk.transtm = "" then 
                                substring(string(time,"hh:mm"),1,2) +
                                substring(string(time,"hh:mm"),4,2) 
                               else 
                                ztmk.transtm
              ztmk.transdt   = if ztmk.transdt = ? then 
                                today
                               else 
                                ztmk.transdt
              s-punchintm      = ztmk.punchintm              
              s-punchindt      = string(ztmk.punchindt,"99/99/99")
              s-punchottm = " " 
              s-punchottm = replace(s-punchottm,":","")
              prt-days    = 0 
              prt-time    = "".
       display s-punchintm s-punchindt 
               s-punchottm s-punchotdt prt-time prt-days 
               with frame f-vaexp.
       find first vasp where
                  vasp.cono = g-cono       and 
                  vasp.shipprod = x-tagno  and 
                  vasp.whse = entry(1,g-vaquoteno,"-")
                  no-error.
       
       find first zsdivasp  use-index k-repairno where
                  zsdivasp.cono = g-cono       and 
                  zsdivasp.repairno = x-tagno  and 
                  zsdivasp.whse = entry(1,g-vaquoteno,"-")
                  no-error.
       
       if avail vasp then do: 
        assign zsdivasp.stagecd   = v-stgint 
               zsdivasp.costingdt = today 
               vasp.user7  = v-stgint 
                 s-stgstat = if avail ztmk and ztmk.punchoutdt = ? then 
                             "open"
                            else 
                             "closed" 
               s-tech     = if avail ztmk and ztmk.techid ne "admn" then 
                             lst-tech 
                            else 
                             "" 
               s-stageprt = if vasp.user7 = 0 then 
                             v-stage2[1]
                            else    
                            if int(vasp.user7) = 5 or int(vasp.user7) = 6 then 
                             "ToBe" + "-" + v-stage2[int(vasp.user7)]
                            else 
                             v-stage2[int(vasp.user7)]. 
        display s-stageprt s-tech s-stgstat with frame f-vaexp. 
        if v-stgint = 4 and not f9-sw and ztmk.statuscd = 0 then do:
         find oimsp where oimsp.person = "costing" 
            no-lock no-error. 
         find first arsc where 
                    arsc.cono = g-cono and
                    arsc.custno = dec(string(vasp.user5,"999999999999")) 
                    no-lock no-error. 
         if avail arsc then 
          assign d-custnm = arsc.name. 
         else 
          assign d-custnm = "".   
          assign d-custno    = dec(string(vasp.user5,"999999999999")) 
                 d-prod      = vasp.refer 
                 d-loc       = vasp.whse
                 d-tech      = g-operinits
                 s-printernm = (if avail oimsp then 
                                 oimsp.miscdata[1]
                                else "")
                 s-punchindt = string(today,"99/99/99")
                 s-punchintm = substring(string(time,"hh:mm"),1,2) +
                               substring(string(time,"hh:mm"),4,2) +
                               substring(string(time,"hh:mm:ss"),7,2). 
                 
          {w-sasp.i s-printernm no-lock} 
          if avail sasp then do: 
           output through value(sasp.pcommand).       
           display d-tech g-vaquoteno d-custno d-custnm 
                   d-prod d-loc g-operinits 
                   s-punchindt s-punchintm with frame f-costlst.   
           output close. 
           assign v-printed = yes.
          end. 
/* TAH Jen */      
          /*
          else do: 
          */
            assign v-emailmsg = 
                   "Cust: " + string (d-custno) + " " +
                   "Repair: " +  g-vaquoteno + " " +
                   "in Costing". 
            assign v-emailmsg = vasp.shipprod + "^" +   
                                 vasp.whse + "^" + v-emailmsg.   

           /*
                 run sendemailmsg.p( 2,"testout",vasp.whse,"VA",v-takenby,
                         v-emailmsg,
                         "","",0,0).
           */
             run sendemailmsg.p( 2,"tdnout",vasp.whse,"VA","",
                                v-emailmsg,
                                "","",
                                ztmk.orderno,ztmk.ordersuf). 
            
    /*       end.  */
        end. /* stage 4 */     
       end. /* avail vasp */
    end.  /* not avail ztmk */
    else do: 
     if avail ztmk and v-stgint ne curr-stg then do:  
      /*** a stage open record already exists ***/
      assign s-stageprt = v-stage2[ztmk.stagecd].
      if v-bypassforcost = false and 
       (v-stgint = 3 or v-stgint = 10 or v-stgint = 11) and 
        avail ztmk and ztmk.techid = g-technician and not {k-func9.i} then do:
        message  s-stageprt + " - Stage already exist-> " + ztmk.techid  
               skip                       
               " close " + s-stageprt + " - open new " + s-stagetext2 + "?" 
        view-as alert-box question buttons yes-no 
        update v-question.
        if not v-question or 
          (v-question and curr-stg = 4 and v-stgint = 3) then  
         assign cost-sw = no.
      end.
      else 
      if v-bypassforcost = false and not {k-func9.i} then do:
        message  s-stageprt + " - Stage already exist."  
               skip                       
               "Close " + s-stageprt + " - open new " + s-stagetext2 + "?" 
        view-as alert-box question buttons yes-no 
        update v-question.
        if not v-question or 
          (v-question and curr-stg = 4 and v-stgint = 3) then  
         assign cost-sw = no.
      end.
      else
      if v-bypassforcost = false and {k-func9.i} then do:
        message  s-stageprt + " - Stage already exist - " + ztmk.techid
               skip       
               "Close currently open stage " + s-stageprt + " ? " 
        view-as alert-box question buttons yes-no 
        update v-question.
        if not v-question then  
         assign cost-sw = no.
      end.
      else
        assign v-question = true.
     end. /* same stage */  
     else 
      assign v-question = true. 
     if v-question and v-stgint ne curr-stg then do:
      if avail ztmk and ztmk.techid ne g-technician and 
       (ztmk.stagecd = 3 or ztmk.stagecd = 10 or
        ztmk.stagecd = 11) then do:   
       assign v-error = "  TechTag MisMatch " + g-technician + " - "
                                              + ll-ztmk.techid.
       run zsdierrx.p(v-error,"yes"). 
       assign my-prompt = 3. 
       next-prompt g-technician with frame f-vaexp.
       next main.
      end.
      /*** complete for new stage if it is already open ***/
      run complete_tagstg(recid(ztmk)).
      find last  ztmk use-index  k-ztmk-sc1 where
                 ztmk.cono      = g-cono       and 
                 ztmk.ordertype = "sc"         and 
                 ztmk.statuscd  = 0            and 
                 ztmk.user1     = x-tagno      and 
                 ztmk.stagecd   = v-stgint     and 
                 ztmk.techid    = (if (v-stgint = 3 or v-stgint = 10 or 
                                       v-stgint = 11) then 
                                     g-technician 
                                   else            
                                    "admn") 
                 no-lock no-error.
      if not avail ztmk then do:  
        if v-stgint = 4 then 
         run genuine-audit.
        run get_tagseqno(output v-jobseqno).
        create ztmk. 
        assign ztmk.jobseqno  = v-jobseqno
               ztmk.cono      = g-cono        
               ztmk.ordertype = "sc" 
               ztmk.user1     = x-tagno       
               ztmk.stagecd   = v-stgint   
               ztmk.statuscd  = 0 
               ztmk.orderno   = if v-stgint > 6 then 
                                 s-vano 
                                else 
                                 0
               ztmk.ordersuf  = 0
               ztmk.techid    = (if (v-stgint = 3 or v-stgint = 10 or 
                                     v-stgint = 11) then 
                                   g-technician 
                                 else            
                                  "admn")   
               ztmk.punchindt = today
               ztmk.punchintm = substring(string(time,"hh:mm"),1,2) +
                                substring(string(time,"hh:mm"),4,2) +
                                substring(string(time,"hh:mm:ss"),7,2)
               ztmk.operinit  = g-operinit  
               ztmk.transtm   = if ztmk.transtm = "" then 
                                 substring(string(time,"hh:mm"),1,2) +
                                 substring(string(time,"hh:mm"),4,2) 
                                else 
                                 ztmk.transtm
               ztmk.transdt   = if ztmk.transdt = ? then 
                                 today
                                else 
                                 ztmk.transdt
                s-punchintm   = ztmk.punchintm              
                s-punchindt   = string(ztmk.punchindt,"99/99/99")
                s-punchottm   = " " 
                s-punchottm   = replace(s-punchottm,":","")
                  prt-days    = 0 
                  prt-time    = "".
       display s-punchintm s-punchindt
               s-punchottm s-punchotdt
               prt-days    prt-time
               with frame f-vaexp.
       find first vasp where
                  vasp.cono = g-cono      and 
                  vasp.shipprod = x-tagno and 
                  vasp.whse = entry(1,x-tagno,"-")
                  no-error.
       
       find first zsdivasp use-index k-repairno where
                  zsdivasp.cono = g-cono       and 
                  zsdivasp.repairno = x-tagno  and 
                  zsdivasp.whse = entry(1,g-vaquoteno,"-")
                  no-error.
       
         if avail vasp then do:  
          assign zsdivasp.stagecd   = v-stgint 
                 zsdivasp.costingdt = today 
                 vasp.user7 = v-stgint
                  s-stgstat = if avail ztmk and ztmk.punchoutdt = ? then 
                               "open"
                              else 
                               "closed" 
                  s-tech    = if avail ztmk and ztmk.techid ne "admn" then 
                               lst-tech  
                              else 
                               "" 
                 s-stageprt = if vasp.user7 = 0 then 
                               v-stage2[1]
                              else    
                              if int(vasp.user7) = 5 or 
                                 int(vasp.user7) = 6 then 
                               "ToBe" + "-" + v-stage2[int(vasp.user7)]
                              else 
                               v-stage2[int(vasp.user7)]. 
          display s-stageprt s-tech s-stgstat with frame f-vaexp 
         end. /* avail vasp  */ 
         if avail vasp and v-stgint = 15 then do:  
          find first arsc where
             arsc.cono   = g-cono and 
             arsc.custno = dec(string(vasp.user5,"999999999999"))
             no-lock no-error.
          assign v-takenby = "".
          find first vaeh where 
                     vaeh.cono  = g-cono and 
                     vaeh.vano  = int(substring(vasp.user2,1,7)) and 
                     vaeh.user2 = x-tagno
                     no-lock no-error.           
           if avail vaeh then do: 
            find first vaelo where 
                       vaelo.cono = g-cono   and 
                       vaelo.ordertype = "o" and 
                       vaelo.vano = vaeh.vano 
                        no-lock no-error. 
             if avail vaelo then do:            
              find first oeeh where 
                         oeeh.cono     = g-cono           and 
                         oeeh.orderno  = vaelo.orderaltno and 
                         oeeh.ordersuf = vaelo.orderaltsuf
                         no-lock no-error.  
              if avail oeeh then   
               assign v-takenby = oeeh.takenby.
             end.
           end. /* avail vaeh */
           if avail arsc then 
            assign d-custnm = arsc.name. 
           else 
            assign d-custnm = "".        
            assign v-emailmsg = " Customer Name: " + trim(d-custnm) +
                                " Part Number  : " + trim(vasp.refer) +
                                " - Closed Stage"
                                 v-emailmsg = vasp.shipprod + "^" +   
                                 vasp.whse + "^" + v-emailmsg.   
           run sendemailmsg.p( 2,"testout",vasp.whse,"VA",v-takenby,
                                 v-emailmsg,
                                "","",0,0).
         end. /* avail vasp and v-stgint = 15  */
      end. /* if not avail ztmk and  */
      if avail vasp then 
        release vasp. 
     end.  /* if v-question  */
     else  do: /* else to if v-question */
     if v-question then   
      run complete_tagstg(recid(ztmk)).
      find first vasp where
                 vasp.cono = g-cono       and 
                 vasp.shipprod = x-tagno  and 
                 vasp.whse = entry(1,g-vaquoteno,"-")
                 no-error.
      
      find first zsdivasp use-index k-repairno where
                 zsdivasp.cono = g-cono       and 
                 zsdivasp.repairno = x-tagno  and 
                 zsdivasp.whse = entry(1,g-vaquoteno,"-")
                 no-error.
      
       /** v-resetvasp only set in evaluate_staging **/
       if avail vasp and v-resetvasp then do: 
        assign zsdivasp.stagecd   = v-stgint 
               zsdivasp.costingdt = today 
               vasp.user7 = v-stgint 
                s-stgstat = if avail ztmk and ztmk.punchoutdt = ? then 
                            "open"
                           else 
                            "closed" 
                s-tech    = if avail ztmk and ztmk.techid ne "admn" then 
                             lst-tech 
                            else 
                             "" 
               s-stageprt = if vasp.user7 = 0 then 
                             v-stage2[1]
                            else    
                            if int(vasp.user7) = 5 or int(vasp.user7) = 6 then 
                             "ToBe" + "-" + v-stage2[int(vasp.user7)]
                            else 
                             v-stage2[int(vasp.user7)]. 
        display s-stageprt s-tech s-stgstat with frame f-vaexp. 
        release vasp.
       end.
       next main.
      end. 
      assign  s-punchintm = ztmk.punchintm              
              s-punchindt = string(ztmk.punchindt,"99/99/99"). 
      display s-punchintm s-punchindt with frame f-vaexp.
    end. /* big else  */
    release ztmk.
    release vasp. 
    if (v-stgint = 3 or v-stgint = 10 or v-stgint = 11) then do: 
     assign lst-tech = "".
     for each fi-ztmk use-index k-ztmk-sc2 where
              fi-ztmk.cono      = g-cono       and 
              fi-ztmk.ordertype = "sc"         and 
              fi-ztmk.user1     = x-tagno      and   
              fi-ztmk.statuscd  = 0            
              no-lock:   
        assign lst-tech = lst-tech + fi-ztmk.tech + ",". 
     end.
     assign s-tech = lst-tech. 
     display s-tech with frame f-vaexp. 
    end. 
    else 
      assign lst-tech = "". 
  next main.
  end. /* k-accept.i and avail ztmk and g-vaquoteno ne ""  */
 apply lastkey. 
 end.  /* ediiting */
display x-function with frame f-functions. 
next main.
end. /* do for  */

procedure free-locks:   
  define buffer fl-vasp for vasp. 
  define buffer fl-zsdivasp for zsdivasp. 
/*
  find first vasp where
             vasp.cono = g-cono       and 
             vasp.shipprod = x-tagno  and 
             vasp.whse = entry(1,x-tagno,"-")
             no-lock no-error.
  if avail vasp and 
   (vasp.xxc4 ne "" and
    vasp.xxc4 = string(g-operinits,"x(4)") +
                   " in --> " + g-currproc) then do: 
    run vasp-inuse.p(input x-tagno, 
                     input g-operinits, 
                     input "c",
                     input-output v-inuse).
    release zsdivasp.
    release vasp. 
  end. 
*/
end. 

procedure labor-outupdt: 
 def buffer zxt2l-sasos for sasos.
 def var lo-whse like arsc.whse no-undo. 
 if num-entries(g-vaquoteno,"-") > 1 then 
  assign lo-whse = entry(1,g-vaquoteno,"-").
 else 
  assign lo-whse = "". 
 find first zxt2l-sasos where
            zxt2l-sasos.cono  = g-cono      and
            zxt2l-sasos.oper2 = g-operinits and
            zxt2l-sasos.menuproc = "zxt2"
            no-lock no-error.
 if not avail zxt2l-sasos or 
 (avail zxt2l-sasos and zxt2l-sasos.securcd[9] <= 4) then 
  leave. 
 for each lbrstgs:
  delete lbrstgs. 
 end. 
 assign v-question = yes
        v-openlit  = "'open'"
        v-openlit  = if lo-whse ne "" then 
                      lo-whse + " " + v-openlit
                     else 
                      v-openlit.
 message " Process only " + v-openlit + " labor stages? "  
           view-as alert-box question buttons yes-no 
 update v-question.
 if v-question then do:  
  for each ztmk use-index k-ztmk-sc1 where
           ztmk.cono      = g-cono       and 
           ztmk.ordertype = "sc"         and 
           ztmk.statuscd  = 0            and  
           ztmk.user1     begins lo-whse and 
          (ztmk.stagecd = 3 or ztmk.stagecd = 10 or
           ztmk.stagecd = 11) and 
           ztmk.punchoutdt = ?                       
           no-lock:
     create lbrstgs.
     assign lbrstgs.techid  = ztmk.techid
            lbrstgs.orderno = entry(1,ztmk.user1,"-") + "-" + 
                         left-trim(entry(2,ztmk.user1,"-"),"0") 
            lbrstgs.indate  = ztmk.punchindt                
            lbrstgs.intime  = ztmk.punchintm    
            lbrstgs.otdate  = ztmk.punchoutdt                
            lbrstgs.ottime  = ztmk.punchouttm    
            lbrstgs.stgcd   = ztmk.stagecd
            lbrstgs.xrecid  = recid(ztmk) 
            lbrstgs.stage   = v-stage2[ztmk.stagecd]. 
  end. /* for each */              
 end.
 else do:  
  update g-vaquoteno with frame f-labor-stage1.
  hide frame f-labor-stage1. 
  if num-entries(g-vaquoteno,"-") = 2 then do: 
   run makequoteno(input g-vaquoteno,output x-tagno).
   for each ztmk use-index k-ztmk-sc2 where
            ztmk.cono      = g-cono     and 
            ztmk.ordertype = "sc"       and 
            ztmk.user1     = x-tagno    and 
           (ztmk.stagecd = 3 or ztmk.stagecd = 10 or ztmk.stagecd = 11)  
            no-lock:
      create lbrstgs.
      assign lbrstgs.techid  = ztmk.techid
             lbrstgs.orderno = entry(1,ztmk.user1,"-") + "-" + 
                          left-trim(entry(2,ztmk.user1,"-"),"0") 
             lbrstgs.indate  = ztmk.punchindt                
             lbrstgs.intime  = ztmk.punchintm    
             lbrstgs.otdate  = ztmk.punchoutdt                
             lbrstgs.ottime  = ztmk.punchouttm    
             lbrstgs.stgcd   = ztmk.stagecd
             lbrstgs.xrecid  = recid(ztmk) 
             lbrstgs.stage   = v-stage2[ztmk.stagecd]. 
   end. /** for each **/    
  end. 
 end. 

 open query q-lbrstgs preselect each lbrstgs.
 enable  b-lbrstgs with frame f-lbrstgs. 
 apply "entry" to b-lbrstgs in frame f-lbrstgs.
 apply "focus" to b-lbrstgs in frame f-lbrstgs.
 display b-lbrstgs with frame f-lbrstgs.
 on cursor-up cursor-up.
 on cursor-down cursor-down.
 wait-for window-close of current-window. 
 hide frame f-lbrstgs. 
end.  

procedure labortest:
do:  
  define buffer c-vasp for vasp. 
  define input parameter sp-recid      as recid.
  define input-output parameter lupdtfl as l no-undo. 
  define var v-tag2 as char format "x(12)"   no-undo. 
  
  find c-vasp where recid(c-vasp) = sp-recid
       no-error.
  if not avail c-vasp then 
     leave.   
 assign lupdtfl = no.  
 mainl:
 do while true on endkey undo, leave mainl:
  if avail c-vasp then 
  assign s-prod     = vasp.refer   
         v-takenby  = substring(c-vasp.user4,120,4)      
         v-model    = substring(c-vasp.user3,1,39)      
         v-custpo   = substring(c-vasp.user4,1,22)       
         v-shipto   = substring(c-vasp.user4,80,8)       
         v-binloc   = substring(c-vasp.user4,110,6)      
         v-custno   = dec(string(c-vasp.user5,"999999999999")) 
         v-esttdn-h = dec(substring(c-vasp.user4,50,2))    
         v-esttdn-m = dec(substring(c-vasp.user4,52,2))    
         v-estasy-h = dec(substring(c-vasp.user4,54,2))  
         v-estasy-m = dec(substring(c-vasp.user4,56,2))  
         v-esttst-h = dec(substring(c-vasp.user4,58,2))  
         v-esttst-m = dec(substring(c-vasp.user4,60,2)).
  update v-esttdn-h 
         v-esttdn-m 
         v-estasy-h 
         v-estasy-m 
         v-esttst-h 
         v-esttst-m 
         go-on(f4)
         with frame f-labor.
         
  if avail c-vasp then do:  
   if substring(c-vasp.user4,50,2) ne string(v-esttdn-h,"99") or
      substring(c-vasp.user4,52,2) ne string(v-esttdn-m,"99") then do:
    find first vaspsl where 
               vaspsl.cono = g-cono and 
               vaspsl.shipprod = c-vasp.shipprod and 
               vaspsl.compprod = "teardown" 
               exclusive-lock no-error. 
    if avail vaspsl then do: 
     assign vaspsl.timeelapsed = vaspsl.qtyneeded *
           ((v-esttdn-h * 60 * 60) + (v-esttdn-m * 60)).
    end. 
    release vaspsl. 
   end.    
   if substring(c-vasp.user4,54,2) ne string(v-estasy-h,"99") or
      substring(c-vasp.user4,56,2) ne string(v-estasy-m,"99") then do: 
    find first vaspsl where 
               vaspsl.cono = g-cono and 
               vaspsl.shipprod = c-vasp.shipprod and 
               vaspsl.compprod = "service repair" 
               exclusive-lock no-error. 
    if avail vaspsl then do:
     assign vaspsl.timeelapsed = vaspsl.qtyneeded *
          ((v-estasy-h * 60 * 60) + (v-estasy-m * 60)).
    end. 
    release vaspsl. 
   end.    
   if substring(c-vasp.user4,58,2) ne string(v-esttst-h,"99") or
      substring(c-vasp.user4,60,2) ne string(v-esttst-m,"99") then do: 
    find first vaspsl where 
               vaspsl.cono = g-cono and 
               vaspsl.shipprod = c-vasp.shipprod and 
               vaspsl.compprod = "test time" 
               exclusive-lock no-error. 
    if avail vaspsl then do:
     assign vaspsl.timeelapsed = vaspsl.qtyneeded *
          ((v-esttst-h * 60 * 60) + (v-esttst-m * 60)).
    end. 
    release vaspsl. 
   end.   
   if v-esttdn-h ne dec(substring(c-vasp.user4,50,2)) or   
      v-esttdn-m ne dec(substring(c-vasp.user4,52,2)) or   
      v-estasy-h ne dec(substring(c-vasp.user4,54,2)) or 
      v-estasy-m ne dec(substring(c-vasp.user4,56,2)) or 
      v-esttst-h ne dec(substring(c-vasp.user4,58,2)) or 
      v-esttst-m ne dec(substring(c-vasp.user4,60,2)) then  
      run vasp501tag.p(input c-vasp.whse, 
                       input v-custno,
                       input "chg", 
                       input v-model,
                       input substring(c-vasp.user3,50,22),     
                       input-output c-vasp.shipprod,
                       output v-tag2).
  end. /* avail c-vasp */    
  if avail c-vasp and 
  ((dec(substring(c-vasp.user4,50,4)) <= 0) or     
   (dec(substring(c-vasp.user4,54,4)) <= 0) or     
   (dec(substring(c-vasp.user4,58,4)) <= 0)) then do: 
    assign v-error = "  Labor time(s) not completed ".
    run zsdierrx.p(v-error,"yes"). 
    next mainl.
  end.
  else do: 
    assign lupdtfl = yes.
    hide frame f-labor.
    run Total_TagValue. 
    leave mainl. 
  end.  
 end. /** do while true **/
 if lastkey = 404 then do: 
   hide frame f-labor. 
   next-prompt s-stagetext2 with frame f-vaexp. 
   next. 
 end.  
 end.
end. 

procedure evaluate_staging:  
do: 
  /** first find on ztmk - this table record is used downstream to 
      determine whether a new stg should be opened on an accept  **/
  assign v-resetvasp = true.
  if g-vaquoteno ne "" then do: 
   find last ztmk use-index k-ztmk-sc1 where
             ztmk.cono      = g-cono     and 
             ztmk.ordertype = "sc"       and 
             ztmk.statuscd  = 0          and  
             ztmk.user1     = x-tagno    and
             ztmk.techid    = (if (v-stgint = 3 or v-stgint = 10 or 
                                   v-stgint = 11) then 
                                 g-technician 
                               else    
                                 "admn") and 
             ztmk.stagecd   ne v-stgint                    
             no-lock no-error.
   if not avail ztmk then  
     find last ztmk use-index k-ztmk-sc1 where
               ztmk.cono      = g-cono     and 
               ztmk.ordertype = "sc"       and 
               ztmk.statuscd  = 0          and  
               ztmk.user1     = x-tagno    and
               ztmk.techid    = (if (v-stgint = 3 or v-stgint = 10 or 
                                     v-stgint = 11) then 
                                   g-technician 
                                 else    
                                   "admn") and 
               ztmk.stagecd   = v-stgint                    
               no-lock no-error.
   /** if input stage is not open find last open stage to close. **/
   if not avail ztmk then  
     find last ztmk use-index k-ztmk-sc1 where
               ztmk.cono      = g-cono     and 
               ztmk.ordertype = "sc"       and 
               ztmk.statuscd  = 0          and  
               ztmk.user1     = x-tagno    
               no-lock no-error.

   if avail ztmk and ztmk.statuscd = 0 and 
     (ztmk.stagecd = 3 or ztmk.stagecd = 10 or ztmk.stagecd = 11) then do: 
    assign v-multiple = 0.  
    for each vm-ztmk use-index k-ztmk-sc1 where
             vm-ztmk.cono      = g-cono  and 
             vm-ztmk.ordertype = "sc"    and 
             vm-ztmk.statuscd  = 0       and  
             vm-ztmk.user1     = x-tagno and 
             vm-ztmk.stagecd   = ztmk.stagecd  
             no-lock:
     assign v-multiple = v-multiple + 1.
    end.            
   end.   
   
   if {k-func9.i} then do: 
    find last  v-ztmk use-index k-ztmk-sc2 where
               v-ztmk.cono      = g-cono       and 
               v-ztmk.ordertype = "sc"         and 
               v-ztmk.user1     = x-tagno      and 
               v-ztmk.techid    = (if (v-stgint = 3 or v-stgint = 10 or 
                                       v-stgint = 11) then 
                                    g-technician 
                                   else    
                                    "admn")  and 
               v-ztmk.statuscd  = 0             
               no-lock no-error. 
     if not avail v-ztmk then do: 
       assign v-error = 
         "  Open labor tech/stage notfound - F9 not applicable".
       run zsdierrx.p(v-error,"yes"). 
       assign my-prompt = 3.
       leave.
     end.
   end.  
   
   if avail ztmk and ztmk.statuscd = 0 and 
      ztmk.stagecd ne v-stgint then do: 
     assign curr-stg  = ztmk.stagecd  
      v-bypassforcost = false.
      /*** a stage open record already exists ***/
      assign s-stageprt = v-stage2[ztmk.stagecd].
      if v-multiple > 1 and not {k-func9.i} then do: 
       assign v-error =
        "  Multiple " + s-stageprt + 
        " labor stages open - close all Tech(s) " + s-stageprt + 
        " labor to continue.".
       run zsdierrx.p(v-error,"yes"). 
       next-prompt s-stagetext2 with frame f-vaexp.
       leave.
      end.   
      if not {k-func9.i} then do: 
       message s-stageprt + " - Stage already exist."  
               skip                       
               "Close " + s-stageprt + " - open new " + s-stagetext2 + "?" 
                view-as alert-box question buttons yes-no 
       update v-question.
       if not v-question or 
         (v-question and curr-stg = 4 and 
          ztmk.stagecd = 4 and ztmk.statuscd = 0) then  
         assign cost-sw = no.
      end.                     
      else  
      if {k-func9.i} and v-multiple <= 1 and curr-stg = v-stgint then do: 
       message s-stageprt + " - Stage already exist - " + ztmk.techid
               skip                       
               "Close currently open stage " + s-stageprt + " ? " 
                view-as alert-box question buttons yes-no 
       update v-question.
      end. 
      if v-question then do: 
       run complete_tagstg(recid(ztmk)).
       find first vasp where
                  vasp.cono = g-cono      and 
                  vasp.shipprod = x-tagno and 
                  vasp.whse = entry(1,x-tagno,"-")
                  no-error.
       
       find first zsdivasp use-index k-repairno where
                  zsdivasp.cono = g-cono       and 
                  zsdivasp.repairno = x-tagno  and 
                  zsdivasp.whse = entry(1,g-vaquoteno,"-")
                  no-error.
       
       if avail vasp then do:  
        assign zsdivasp.stagecd = if avail ztmk then 
                                   ztmk.stagecd 
                                  else 
                                   v-stgint
               zsdivasp.costingdt = today 
               vasp.user7 = if avail ztmk then 
                             ztmk.stagecd 
                            else 
                             v-stgint
                s-stgstat = if avail ztmk and ztmk.punchoutdt = ? then 
                             "open"
                            else 
                             "closed" 
                s-tech    = if avail ztmk and ztmk.techid ne "admn" then 
                             ztmk.techid 
                            else 
                             "" 
               s-stageprt = if vasp.user7 = 0 then 
                             v-stage2[1]
                            else    
                            if int(vasp.user7) = 5 or 
                               int(vasp.user7) = 6 then 
                             "ToBe" + "-" + v-stage2[int(vasp.user7)]
                            else 
                             v-stage2[int(vasp.user7)]. 
         display s-stageprt s-tech s-stgstat with frame f-vaexp. 
       end. /* avail vasp  */ 
      end. /* if v-question  */
      else do:  /* not v-question */
       assign curr-stg     = v-stgint
              v-resetvasp  = false.
      end. 
   end. /* avail ztmk  */   
   else do: 
     if not avail ztmk then 
      find last ztmk use-index k-ztmk-sc2 where
                ztmk.cono      = g-cono     and 
                ztmk.ordertype = "sc"       and 
                ztmk.user1     = x-tagno    
                no-lock no-error.
     if {k-accept.i} then 
      assign cost-sw = if avail ztmk     and 
                         ztmk.stagecd = 4 and 
                         ztmk.statuscd = 0 then 
                         yes 
                       else   
                         no
            curr-stg = 0.
   end. 
 end. /* g-vaquoteno */
 /*** a tech cannot punch out of another tech's labor item - 
  *** not used for now - multiple tech will use one computer 
 if ({k-func9.i} and avail ztmk)  then do:
  find first swsst use-index k-swsst where
             swsst.cono       = g-cono       and
             swsst.technician = g-technician and 
             swsst.whse       = ztmk.whse           
             no-lock no-error.
  if avail ztmk and (ztmk.stagecd = 3 or ztmk.stagecd = 10 or 
                      ztmk.stagecd = 11) then 
   find last ztmk use-index k-ztmk-sc1 where
             ztmk.cono      = g-cono       and 
             ztmk.ordertype = "sc"         and 
             ztmk.statuscd  = 0            and  
             ztmk.user1     = x-tagno      and
             ztmk.stagecd   = v-stgint    and 
             ztmk.techid    = (if (v-stgint = 3 or v-stgint = 10 or 
                                   v-stgint = 11) then 
                                g-technician 
                               else    
                                "admn")   
             no-lock no-error. 
   if avail ztmk and avail then do: 
    assign v-error = "  No TechTag(s) to complete-> " + g-technician.
    run zsdierrx.p(v-error,"yes"). 
    next-prompt s-stagetext2 with frame f-vaexp.
    next.
   end.
 end. /* s-stagetext2 = "" and f9 */
 *************/
end. 
end. /* end procedure */ 
 
procedure fetch-info:  
do:
 def var ttm as char format "xx:xx". 
 def var min-tm as int. 
 def var hrs-tm as int. 
 
 if g-vaquoteno ne "" then do: 
  run makequoteno(input g-vaquoteno,output x-tagno).
  if v-errfl then leave. 
  find first vasp where
             vasp.cono = g-cono       and 
             vasp.shipprod = x-tagno  and 
             vasp.whse = entry(1,x-tagno,"-")
             no-lock no-error.
  if not avail vasp then leave. 
  assign lst-tech    = ""
         s-vano      = if avail vasp then 
                        dec(substring(vasp.user2,1,7))
                       else 
                        0 
         s-vasuf     = if avail vasp then        
                        dec(substring(vasp.user2,9,2))
                       else 
                        0 
         prt-days    = 0 
         s-punchottm = " " 
         s-punchottm = replace(s-punchottm,":","")
         s-punchotdt = " ".
  if avail vasp then do:  
    /* looking for the newest open record - if labor(transtm) must be used
       with this index(sc2) because of the techid */ 
    for each x-ztmk use-index k-ztmk-sc2 where
             x-ztmk.cono      = g-cono       and 
             x-ztmk.ordertype = "sc"         and 
             x-ztmk.user1     = x-tagno      and 
             x-ztmk.statuscd  = 0        no-lock
             break by x-ztmk.transdt descending
                   by x-ztmk.transtm descending:
      assign myrecid = recid(x-ztmk). 
      if lastkey = 309 then do:  
       if x-ztmk.stagecd > 0 then do:
        assign s-stagetext2 = v-stage2[x-ztmk.stagecd].
       end. 
       display s-stagetext2 with frame f-vaexp. 
      end.               
      leave.
    end.             
    find ztmk where recid(ztmk) = myrecid no-lock no-error.          
    if avail ztmk and ztmk.statuscd = 0 and 
     (ztmk.stagecd = 3 or ztmk.stagecd = 10 or ztmk.stagecd = 11) then do: 
     for each fi-ztmk use-index k-ztmk-sc2 where
              fi-ztmk.cono      = g-cono       and 
              fi-ztmk.ordertype = "sc"         and 
              fi-ztmk.user1     = x-tagno      and   
              fi-ztmk.statuscd  = 0            
              no-lock:   
        assign lst-tech = lst-tech + fi-ztmk.tech + ",". 
     end.
   end. 
   else 
    assign lst-tech = "".          
   if not avail ztmk then do: 
    /* looking for the last punched out record  */ 
    for each fi-ztmk use-index k-ztmk-sc2 where
             fi-ztmk.cono      = g-cono       and 
             fi-ztmk.ordertype = "sc"         and 
             fi-ztmk.user1     = x-tagno      no-lock
             break by fi-ztmk.transdt descending
                   by fi-ztmk.transtm descending:
     if fi-ztmk.punchoutdt ne ? then do: 
      assign myrecid = recid(fi-ztmk).
      leave.
     end. 
    end.             
    find ztmk where recid(ztmk) = myrecid no-lock no-error.          
   end.
   if avail ztmk then do: 
    assign v-indate   = ztmk.punchindt 
           v-intime   = ztmk.punchintm 
           v-outdate  = if ztmk.punchoutdt ne ? then 
                         ztmk.punchoutdt 
                        else 
                         today 
           v-outtime  = if ztmk.punchouttm ne "" then 
                         ztmk.punchouttm 
                        else  
                         string(truncate((time modulo 86400) / 3600 ,0),"99")  
                       + string(truncate((time modulo 3600) / 60 ,0),"99")   
                       + string(truncate((time modulo 60) / 1 ,0),"99").  
     run timespan (input  v-indate,
                   input  v-intime,
                   input  v-outdate,
                   input  v-outtime,
                   output v-lendays,
                   output v-lenhr,
                   output v-lenmin,
                   output v-lensec).
    assign prt-days = v-lendays 
           substring(prt-time,1,3) = string(v-lenhr,"zz9")
           substring(prt-time,4,2) = string(v-lenmin,"99").
    display prt-days prt-time
            with frame f-vaexp.
   end. 
   assign s-stgstat = if avail ztmk and ztmk.punchoutdt = ? then 
                       "open"
                      else 
                       "closed" 
          s-tech    = lst-tech  
         s-stageprt = if vasp.user7 = 0 then 
                       v-stage2[1]
                      else    
                      if (int(vasp.user7) = 5 or int(vasp.user7) = 6) and 
                          ztmk.punchoutdt = ? then 
                       "ToBe" + "-" + v-stage2[int(vasp.user7)]
                      else 
                       v-stage2[int(vasp.user7)]
        s-stagetext2 = if input g-vaquoteno ne o-vaquoteno and 
                        vasp.user7 > 0 then 
                        v-stage2[int(vasp.user7)]
                       else 
                        s-stagetext2
        s-punchintm = if avail ztmk then 
                       ztmk.punchintm   
                      else 
                       " "  
        s-punchintm = replace(s-punchintm,":","")    
        s-punchindt = if avail ztmk then 
                       string(ztmk.punchindt,"99/99/99")
                      else 
                       " "
        s-punchottm = if avail ztmk and ztmk.punchoutdt ne ? then 
                       ztmk.punchouttm   
                      else 
                       " "  
        s-punchottm = if avail ztmk and ztmk.punchoutdt ne ? then  
                       s-punchottm
                      else 
                       replace(s-punchottm,":","") 
        s-punchotdt = if avail ztmk and ztmk.punchoutdt ne ? then 
                       string(ztmk.punchoutdt,"99/99/99")
                      else 
                       " "
        o-vaquoteno = input g-vaquoteno.

   display s-stagetext2 s-stageprt s-tech 
           s-stgstat    s-vano s-vasuf 
           s-punchintm  s-punchindt
           s-punchottm  s-punchotdt with frame f-vaexp. 
  end.  
 end.
end. 
end.

/* used to close staging - open costing or close a stage if appropriate */

procedure complete_tagstg: 
do transaction: 
  define input parameter ca-recid as recid. 

  define buffer c-ztmk  for ztmk.
  define buffer ct-ztmk  for ztmk.

  find c-ztmk where recid(c-ztmk) = ca-recid
       exclusive-lock no-error.
  if not avail c-ztmk or not v-question then 
   leave.   
  
  if c-ztmk.stagecd = 5 and f9-sw then do:
   if c-ztmk.statuscd =  0 then do: 
   assign v-error = 
    "  Qtd Stage must be completed in the vaexq or vaixi screen..". 
   run zsdierrx.p(v-error,"yes"). 
   leave. 
   end. 
  end.
  
  define buffer nn-vaeh for vaeh.
  define var wk-punchindt like c-ztmk.punchindt.
  define var wk-punchintm like c-ztmk.punchintm.

  assign t-ordqty     = 0
         t-tmout      = 0 
         t-tmin       = 0 
         t-days       = 0 
         t-time       = 0                                       
         wk-punchindt = ?
         wk-punchintm = "".

  for each ltimes exclusive-lock:
     delete ltimes.
  end.   
  assign v-emailmsg = "".

/*
  /** used in vaext_timer.pc ****/
  {vaeh.gfi &vano  = c-ztmk.orderno 
            &vasuf = c-ztmk.ordersuf  
            &lock  = "no"
            &buf   = "nn-"}

   create ltimes.
   assign ltimes.orderno   = c-ztmk.orderno 
          ltimes.ordersuf  = c-ztmk.ordersuf
          ltimes.techid    = c-ztmk.techid
          ltimes.whse      = if avail nn-vaeh then 
                                nn-vaeh.whse 
                             else 
                                "".
*/

   if c-ztmk.stagecd  = 4 and not f9-sw and 
      c-ztmk.statuscd = 0 and cost-sw then do:
      find oimsp where oimsp.person = "costing" 
         no-lock no-error. 
      find first vasp where 
                 vasp.cono = g-cono and 
                 vasp.shipprod = x-tagno
                 no-lock no-error. 
      find first arsc where 
                 arsc.cono = g-cono and
                 arsc.custno = dec(string(vasp.user5,"999999999999")) 
                 no-lock no-error. 
      if avail arsc then 
       assign d-custnm = arsc.name. 
      else 
       assign d-custnm = "".   
      if avail vasp then do:
       assign d-custno    = dec(string(vasp.user5,"999999999999")) 
              d-prod      = vasp.refer 
              d-loc       = vasp.whse
              d-tech      = g-operinits
              s-printernm = if avail oimsp then 
                             oimsp.miscdata[1]
                            else ""
              s-punchindt = string(today,"99/99/99")
              s-punchintm = substring(string(time,"hh:mm"),1,2) +
                            substring(string(time,"hh:mm"),4,2) +
                            substring(string(time,"hh:mm:ss"),7,2). 
       {w-sasp.i s-printernm no-lock} 
       if avail sasp then do: 
        output through value(sasp.pcommand).       
        display d-tech g-vaquoteno d-custno d-custnm 
                d-prod d-loc g-operinits 
                s-punchindt s-punchintm with frame f-costlst.
        output close. 
       end. 
/* TAH jen */
/*       else do:  */
            assign v-emailmsg = 
                   "Cust: " + string (d-custno) + " " +
                   "Repair: " +  g-vaquoteno + " " +
                   "in Costing". 
            assign v-emailmsg = vasp.shipprod + "^" +   
                                vasp.whse + "^" + v-emailmsg.   

           /*
                 run sendemailmsg.p( 2,"testout",vasp.whse,"VA",v-takenby,
                         v-emailmsg,
                         "","",0,0).
           */
             run sendemailmsg.p( 2,"tdnout",vasp.whse,"VA","",
                                v-emailmsg,
                                "","",
                                c-ztmk.orderno,c-ztmk.ordersuf). 
/*        end.  */
      end. /* avail vasp */  
   end. /* stage 4 */                 
   
   /***********/
   assign c-ztmk.punchoutdt = today
          c-ztmk.operinit   = g-operinit 
          c-ztmk.statuscd   = 5 
          c-ztmk.punchouttm = substring(string(time,"hh:mm"),1,2) +
                              substring(string(time,"hh:mm"),4,2) +
                              substring(string(time,"hh:mm:ss"),7,2).
   
   assign s-punchottm = c-ztmk.punchouttm  
          s-punchotdt = string(c-ztmk.punchoutdt,"99/99/99").
 
   assign t-days   = c-ztmk.punchoutdt - wk-punchindt
          t-ordqty = t-ordqty + 1
          t-tmout  = (dec(substring(c-ztmk.punchouttm,1,2)) * 60) +
                      dec(substring(c-ztmk.punchouttm,3,2))
          t-tmin   = (dec(substring(wk-punchintm,1,2)) * 60) + 
                      dec(substring(wk-punchintm,3,2))
          t-time   =  t-time + (t-tmout - t-tmin).

   if (c-ztmk.stagecd = 11 and f9-sw) then do:   
    assign d-custnm = ""
           lst-tech = "".
    def buffer cl-ztmk for ztmk.  
     find last cl-ztmk use-index k-ztmk-sc2 where
               cl-ztmk.cono      = g-cono   and 
               cl-ztmk.ordertype = "sc"     and 
               cl-ztmk.user1     = x-tagno  and   
               cl-ztmk.statuscd  = 0        and 
               cl-ztmk.stagecd   = 11
               no-lock no-error.    
     if avail cl-ztmk then do: 
      run fetch-info.
      leave.
     end.
    run fetch-info.
    message "Move tested repair to 'cls' stage?. "  
             view-as alert-box question buttons yes-no 
    update s3 as logical.
    if s3 then
     assign s3 = s3. 
    else  
     leave.
   end. 
     
   /* event driven by v-stgint(new stg) or m-stgint(previous stg) **/ 
   if c-ztmk.stagecd = 3 and c-ztmk.statuscd = 5 and 
      f9-sw and cost-sw then do: 
     find oimsp where oimsp.person = "costing" 
          no-lock no-error. 
     find first vasp where 
                vasp.cono = g-cono and 
                vasp.shipprod = x-tagno
                no-lock no-error. 
     find first arsc where 
                arsc.cono = g-cono and
                arsc.custno = dec(string(vasp.user5,"999999999999")) 
                no-lock no-error. 
     if avail arsc then 
      assign d-custnm = arsc.name. 
     else 
      assign d-custnm = "".           
     if avail vasp then do:
      assign d-custno    = dec(string(vasp.user5,"999999999999")) 
             d-prod      = vasp.refer 
             d-loc       = vasp.whse
             d-tech      = c-ztmk.techid
             s-printernm = if avail oimsp then 
                            oimsp.miscdata[1]
                           else ""
             s-punchindt = string(today,"99/99/99")
             s-punchintm = substring(string(time,"hh:mm"),1,2) +
                           substring(string(time,"hh:mm"),4,2) +
                            substring(string(time,"hh:mm:ss"),7,2). 
      {w-sasp.i s-printernm no-lock} 
      if avail sasp then do: 
       output through value(sasp.pcommand).       
       display d-tech g-vaquoteno d-custno d-custnm 
               d-prod d-loc g-operinits 
               s-punchindt s-punchintm with frame f-costlst.     
       output close. 
      end. 
/* TAH jen */
/*       else do:  */
            assign v-emailmsg = 
                   "Cust: " + string (d-custno) + " " +
                   "Repair: " +  g-vaquoteno + " " +
                   "in Costing". 
            assign  v-emailmsg = vasp.shipprod + "^" +   
                                  vasp.whse + "^" + v-emailmsg.   

           /*
                 run sendemailmsg.p( 2,"testout",vasp.whse,"VA",v-takenby,
                         v-emailmsg,
                         "","",0,0).
           */
             run sendemailmsg.p( 2,"tdnout",vasp.whse,"VA","",
                                v-emailmsg,
                                "","",
                                c-ztmk.orderno,c-ztmk.ordersuf). 
          
/*      end.  */
     end. 
   end. /*v-stgint = 3 and f9-sw */  
   if (c-ztmk.stagecd = 3 and c-ztmk.statuscd = 5 and 
       f9-sw and cost-sw) then do:   
    run get_tagseqno(output v-jobseqno).
    create ct-ztmk. 
    assign ct-ztmk.jobseqno  = v-jobseqno
           ct-ztmk.cono      = g-cono        
           ct-ztmk.ordertype = "sc" 
           ct-ztmk.user1     = x-tagno        
           ct-ztmk.stagecd   = 4   
           ct-ztmk.statuscd  = 0 
           ct-ztmk.techid    = "admn" 
           ct-ztmk.punchindt = today
           ct-ztmk.punchintm = substring(string(time,"hh:mm"),1,2) +
                               substring(string(time,"hh:mm"),4,2) +
                               substring(string(time,"hh:mm:ss"),7,2)
           ct-ztmk.operinit  = g-operinit  
           ct-ztmk.transtm   = if ct-ztmk.transtm = "" then 
                                substring(string(time,"hh:mm"),1,2) +
                                substring(string(time,"hh:mm"),4,2) 
                               else 
                                 ct-ztmk.transtm
           ct-ztmk.transdt   = if ct-ztmk.transdt = ? then 
                                today
                                else 
                                 ct-ztmk.transdt
            s-punchintm      = ct-ztmk.punchintm              
            s-punchindt      = string(ct-ztmk.punchindt,"99/99/99").
     display s-punchintm s-punchindt with frame f-vaexp.
     find first vasp where
                vasp.cono = g-cono       and 
                vasp.shipprod = x-tagno  and 
                vasp.whse = entry(1,g-vaquoteno,"-")
                no-error.
     
     find first zsdivasp use-index k-repairno where
                zsdivasp.cono = g-cono       and 
                zsdivasp.repairno = x-tagno  and 
                zsdivasp.whse = entry(1,g-vaquoteno,"-")
                no-error.
     
     if avail vasp then do: 
     /** updating stage directly to costing **/
      assign zsdivasp.stagecd   = 4 
             zsdivasp.costingdt = today 
             vasp.user7 = 4
           s-stagetext2 = v-stage2[int(vasp.user7)]
              s-stgstat = if avail ct-ztmk and ct-ztmk.punchoutdt = ? then 
                           "open"
                          else 
                           "closed" 
             s-tech     = if avail ct-ztmk and ct-ztmk.techid ne "admn" then 
                           ct-ztmk.techid 
                          else 
                           "" 
             s-stageprt = if vasp.user7 = 0 then 
                           v-stage2[1]
                          else    
                          if int(vasp.user7) = 5 or int(vasp.user7) = 6 then 
                           "ToBe" + "-" + v-stage2[int(vasp.user7)]
                          else 
                           v-stage2[int(vasp.user7)]. 
      display s-stageprt s-tech
              s-stgstat  s-stagetext2 with frame f-vaexp. 
      release vasp.
     end.
    end. /* v-stgint = 3 and f9-sw  */


    /** update the vaesl labor rep if the repair has been converted to a va **/ 
    if (c-ztmk.stagecd = 10 and c-ztmk.statuscd = 5 and f9-sw) then do:   
     find first vasp where
                vasp.cono = g-cono       and 
                vasp.shipprod = x-tagno  and 
                vasp.whse = entry(1,g-vaquoteno,"-")
                no-error.
     if avail vasp then 
       /* update vaesl.timeslsrep if the repair has already been converted */
      run timeslsrep(input ca-recid). 
    end. 

    /** all tech's tst stages closed - now create a cls(clsed) stage **/ 
    if (c-ztmk.stagecd = 11 and c-ztmk.statuscd = 5 and f9-sw) then do:   
     run get_tagseqno(output v-jobseqno).
     create ct-ztmk. 
     assign ct-ztmk.jobseqno  = v-jobseqno
            ct-ztmk.cono      = g-cono        
            ct-ztmk.ordertype = "sc" 
            ct-ztmk.user1     = x-tagno        
            ct-ztmk.stagecd   = 15 
            ct-ztmk.orderno   = if v-stgint > 6 then 
                                 s-vano 
                                else 
                                 0
            ct-ztmk.ordersuf  = 0
            ct-ztmk.techid    = if g-technician ne "" then 
                                 g-technician 
                                else 
                                 "admn" 
            ct-ztmk.punchindt = today
            ct-ztmk.punchintm = substring(string(time,"hh:mm"),1,2) +
                                substring(string(time,"hh:mm"),4,2) +
                                substring(string(time,"hh:mm:ss"),7,2)
            ct-ztmk.punchoutdt = today
            ct-ztmk.statuscd   = 5 
            ct-ztmk.punchouttm = substring(string(time,"hh:mm"),1,2) +
                                 substring(string(time,"hh:mm"),4,2) +
                                 substring(string(time,"hh:mm:ss"),7,2)
            ct-ztmk.operinit  = g-operinit  
            ct-ztmk.transtm   = if ct-ztmk.transtm = "" then 
                                 substring(string(time,"hh:mm"),1,2) +
                                 substring(string(time,"hh:mm"),4,2) 
                                else 
                                 ct-ztmk.transtm
            ct-ztmk.transdt   = if ct-ztmk.transdt = ? then 
                                 today
                                else 
                                 ct-ztmk.transdt
            s-punchintm      = ct-ztmk.punchintm              
            s-punchindt      = string(ct-ztmk.punchindt,"99/99/99").
     display s-punchintm s-punchindt with frame f-vaexp.
     find first vasp where
                vasp.cono = g-cono       and 
                vasp.shipprod = x-tagno  and 
                vasp.whse = entry(1,g-vaquoteno,"-")
                no-error.
     
     find first zsdivasp use-index k-repairno where
                zsdivasp.cono = g-cono       and 
                zsdivasp.repairno = x-tagno  and 
                zsdivasp.whse = entry(1,g-vaquoteno,"-")
                no-error.    
     if avail vasp then do: 
      /* update vaesl.timeslsrep if the repair has already been converted */
      run timeslsrep(input ca-recid). 
      /** updating stage directly to closed **/
     assign  zsdivasp.stagecd   = 15 
             zsdivasp.costingdt = today 
             vasp.user7 = 15
              s-stgstat = if avail ct-ztmk and ct-ztmk.punchoutdt = ? then 
                           "open"
                          else 
                           "closed" 
              s-tech    = if avail ct-ztmk and ztmk.techid ne "admn" then 
                           ct-ztmk.techid 
                          else 
                           "" 
             s-stageprt = if vasp.user7 = 0 then 
                           v-stage2[1]
                          else    
                          if int(vasp.user7) = 5 or int(vasp.user7) = 6 then 
                           "ToBe" + "-" + v-stage2[int(vasp.user7)]
                          else 
                           v-stage2[int(vasp.user7)]. 
      display s-stageprt s-tech s-stgstat with frame f-vaexp. 
     end.
     if avail vasp then do:  
       find first arsc where
                  arsc.cono   = g-cono and 
                  arsc.custno = dec(string(vasp.user5,"999999999999"))
       no-lock no-error.
       assign v-takenby = "".
       find first vaeh where 
                  vaeh.cono  = g-cono and 
                  vaeh.vano  = int(substring(vasp.user2,1,7)) and 
                  vaeh.user2 = x-tagno 
                  no-lock no-error.           
       if avail vaeh then do: 
        find first vaelo where 
                   vaelo.cono = g-cono   and 
                   vaelo.ordertype = "o" and 
                   vaelo.vano = vaeh.vano 
                   no-lock no-error. 
        if avail vaelo then do:            
         find first oeeh where 
                    oeeh.cono     = g-cono           and 
                    oeeh.orderno  = vaelo.orderaltno and 
                    oeeh.ordersuf = vaelo.orderaltsuf
                    no-lock no-error.  
         if avail oeeh then   
          assign v-takenby = oeeh.takenby.
        end.
       end. /* avail vaeh */
       if avail arsc then 
        assign d-custnm = arsc.name. 
       else 
       assign d-custnm = "".        
       assign v-emailmsg = " Customer Name: " + trim(d-custnm) +
                           " Part Number  : " + trim(vasp.refer) +
                           " - Closed Stage - " 
              v-emailmsg = if avail vaeh then 
                            v-emailmsg + "VaNo " + string(vaeh.vano,">>>9999")
                           else 
                            v-emailmsg
              v-emailmsg = vasp.shipprod + "^" +   
                           vasp.whse + "^" + v-emailmsg.   
     end. /* avail vasp */
     run sendemailmsg.p( 2,"testout",vasp.whse,"VA",v-takenby,
                         v-emailmsg,
                         "","",0,0).
     release vasp. 
    end.
  release c-ztmk.
  if avail ct-ztmk then 
     release ct-ztmk.
  if avail nn-vaeh then 
     release nn-vaeh.
  assign t-days = if t-days > 0 then 
                   ((t-days * 24) * 60)
                  else 
                    0
         t-time = (t-time / t-ordqty)
         t-time =  t-time + t-days. 
  run fetch-info.
end.
end procedure.

/* if the repair is converted and an 'asy' or 'tst' stage is completed - 
   update the vaesl line with the techid(timeslsrep).  */

procedure timeslsrep: 
define input parameter v-recid as recid. 
define buffer c-ztmk  for ztmk.

find c-ztmk where recid(c-ztmk) = v-recid
     no-lock no-error.
if not avail c-ztmk then 
 leave.   

def buffer c-vaesl for vaesl. 

find first vaeh where 
           vaeh.cono  = g-cono and 
           vaeh.vano  = int(substring(vasp.user2,1,7)) and 
           vaeh.user2 = x-tagno 
           no-lock no-error.           
if avail vaeh then do:     
 if c-ztmk.stagecd = 10 then do: 
  find first c-vaesl use-index k-vaesl where
             c-vaesl.cono   = g-cono     and 
             c-vaesl.vano   = vaeh.vano  and 
             c-vaesl.vasuf  = vaeh.vasuf and 
             c-vaesl.seqno  = 6          and 
             c-vaesl.shipprod = "service repair" 
             exclusive-lock no-error. 
  if avail c-vaesl then do:
   assign c-vaesl.timeslsrep = c-ztmk.techid         
          c-vaesl.operinit   = g-operinit
          c-vaesl.transdt    = today
          c-vaesl.transtm    = substring(string(time,"hh:mm"),1,2) +
                               substring(string(time,"hh:mm"),4,2).
  end.        
 end. 
 else 
 if c-ztmk.stagecd = 11 then do: 
  find first c-vaesl use-index k-vaesl where
             c-vaesl.cono   = g-cono     and 
             c-vaesl.vano   = vaeh.vano  and 
             c-vaesl.vasuf  = vaeh.vasuf and 
             c-vaesl.seqno  = 6          and 
             c-vaesl.shipprod = "test time" 
             exclusive-lock no-error. 
  if avail c-vaesl then do:
   assign c-vaesl.timeslsrep = c-ztmk.techid         
          c-vaesl.operinit   = g-operinit
          c-vaesl.transdt    = today
          c-vaesl.transtm    = substring(string(time,"hh:mm"),1,2) +
                               substring(string(time,"hh:mm"),4,2).
  end.   
 end. 
end. /* avail vaeh */
end. 

procedure genuine-audit: 
 def buffer x-zsdivasp for zsdivasp. 
 find first zsdivasp  use-index k-repairno where
            zsdivasp.cono = g-cono       and 
            zsdivasp.repairno = x-tagno  and 
            zsdivasp.whse = entry(1,g-vaquoteno,"-")
            no-error.          
 if avail zsdivasp and zsdivasp.user1 = "" then 
  assign q-pistons   = no 
         q-blocks    = no 
         q-other     = no 
         qty-pistons = 0
         qty-blocks  = 0 
         qty-other   = 0
         q-otherdesc = "".
 if avail zsdivasp and zsdivasp.user1 ne " " and  
    num-entries(zsdivasp.user1,"^") > 6 then do: 
  assign q-pistons   = if entry(1,zsdivasp.user1,"^") = "yes" then 
                        yes
                       else 
                        no
         qty-pistons = if entry(1,zsdivasp.user1,"^") = "yes" then 
                        int(entry(2,zsdivasp.user1,"^"))
                       else 
                        0
         q-blocks    = if entry(3,zsdivasp.user1,"^") = "yes" then 
                        yes 
                       else 
                        no 
         qty-blocks  = if entry(3,zsdivasp.user1,"^") = "yes" then 
                        int(entry(4,zsdivasp.user1,"^"))
                       else 
                        0
         q-other     = if entry(5,zsdivasp.user1,"^") = "yes" then 
                        yes 
                       else 
                        no
         qty-other   = if entry(5,zsdivasp.user1,"^") = "yes" then 
                        int(entry(6,zsdivasp.user1,"^"))
                       else 
                        0
         q-otherdesc = entry(7,zsdivasp.user1,"^"). 
  display q-pistons qty-pistons q-blocks qty-blocks        
          q-other   qty-other   q-otherdesc with frame f-genparts.        
 end. 
 genuine: 
 do for x-zsdivasp while true on endkey undo, leave genuine:
  update q-pistons
         qty-pistons 
         q-blocks 
         qty-blocks 
         q-other 
         q-otherdesc
         qty-other with frame f-genparts
  editing:
   readkey.
   if {k-cancel.i} then 
    leave genuine.
   apply lastkey. 
  end. 
  find x-zsdivasp where recid(x-zsdivasp) = recid(zsdivasp) no-error.
  assign substring(x-zsdivasp.user1,1,3)   = (if q-pistons and 
                                                 qty-pistons > 0 then 
                                               "yes" 
                                              else 
                                               "no ")
         substring(x-zsdivasp.user1,4,7)   = "^" +
                                           string(int(qty-pistons),"999").
  assign substring(x-zsdivasp.user1,8,11)  = "^" + (if q-blocks and 
                                                      qty-blocks > 0 then 
                                                     "yes" 
                                                    else 
                                                     "no ") 
         substring(x-zsdivasp.user1,12,15) = "^" + 
                                           string(int(qty-blocks),"999").
  assign substring(x-zsdivasp.user1,16,19) = "^" + (if q-other and 
                                                       qty-other > 0  then 
                                                     "yes" 
                                                    else 
                                                     "no ")    
         substring(x-zsdivasp.user1,20,24) = "^" + 
                                           string(int(qty-other),"999") + 
                                            "^"  
         substring(x-zsdivasp.user1,25,55) = q-otherdesc.        
  if q-pistons = yes and qty-pistons = 0 then do: 
   assign v-error = "  Piston Qty Required Pls.  ". 
   run zsdierrx.p(v-error,"yes"). 
   next-prompt qty-pistons with frame f-genparts. 
   next.
  end. 
  if q-blocks = yes and qty-blocks = 0 then do: 
   assign v-error = "  Block Qty Required Pls. ". 
   run zsdierrx.p(v-error,"yes"). 
   next-prompt qty-blocks with frame f-genparts. 
   next.
  end. 
  if q-other = yes and qty-other = 0 then do: 
   assign v-error = "  Other Qty Required Pls. ". 
   run zsdierrx.p(v-error,"yes"). 
   next-prompt qty-other with frame f-genparts. 
   next.
  end. 
   if qty-pistons > 0 and q-pistons = no then do: 
   assign v-error = "  Piston y/n required pls - Qty > 0.". 
   run zsdierrx.p(v-error,"yes"). 
   next-prompt q-pistons with frame f-genparts. 
   next.
  end. 
  if qty-blocks > 0 and q-blocks = no then do: 
   assign v-error = "  Block y/n required pls - Qty > 0.". 
   run zsdierrx.p(v-error,"yes"). 
   next-prompt q-blocks with frame f-genparts. 
   next.
  end. 
  if qty-other > 0 and q-other = no then do: 
   assign v-error = "  Other y/n required pls - Qty > 0.". 
   run zsdierrx.p(v-error,"yes"). 
   next-prompt q-other with frame f-genparts. 
   next.
  end. 
  if keylabel(lastkey) = "pf1" or 
    (keylabel(lastkey) = "return" and frame-field = "qty-other") then do:
   hide frame f-genparts. 
   leave genuine.   
  end. 
 end. 
end. 

procedure close_allstg: 
do transaction: 
  define input parameter ca-stgint as int. 
  define var wk-punchindt like ztmk.punchindt.
  define var wk-punchintm like ztmk.punchintm.
  define buffer ca-ztmk  for ztmk.

  for each ca-ztmk where 
           ca-ztmk.cono = g-cono         and 
           ca-ztmk.ordertype = "sc"      and 
           ca-ztmk.user1     = x-tagno   and       
           ca-ztmk.stagecd   = ca-stgint and 
           ca-ztmk.statuscd  = 0 
           exclusive-lock:
   /***********/
   assign ca-ztmk.punchoutdt = today
          ca-ztmk.operinit   = g-operinit 
          ca-ztmk.statuscd   = 5 
          ca-ztmk.punchouttm = substring(string(time,"hh:mm"),1,2) +
                               substring(string(time,"hh:mm"),4,2) +
                               substring(string(time,"hh:mm:ss"),7,2).
   
   assign t-days   = ca-ztmk.punchoutdt - wk-punchindt
          t-ordqty = t-ordqty + 1
          t-tmout  = (dec(substring(ca-ztmk.punchouttm,1,2)) * 60) +
                      dec(substring(ca-ztmk.punchouttm,3,2))
          t-tmin   = (dec(substring(wk-punchintm,1,2)) * 60) + 
                      dec(substring(wk-punchintm,3,2))
          t-time   =  t-time + (t-tmout - t-tmin).
  
  end. 
  run fetch-info. 
end.
end. 

procedure complete_allstg: 
do transaction: 
  define input parameter ca-recid as recid. 

  define buffer c-ztmk  for ztmk.
  define buffer ct-ztmk  for ztmk.
  
  find c-ztmk where recid(c-ztmk) = ca-recid
       exclusive-lock no-error.
  if not avail c-ztmk or not v-question then 
     leave.   
  
  define buffer nn-vaeh for vaeh.
  define var wk-punchindt like c-ztmk.punchindt.
  define var wk-punchintm like c-ztmk.punchintm.

  assign t-ordqty     = 0
         t-tmout      = 0 
         t-tmin       = 0 
         t-days       = 0 
         t-time       = 0
         wk-punchindt = ?
         wk-punchintm = "".

  for each ltimes exclusive-lock:
     delete ltimes.
  end.   

  if (v-stgint = 11 and f9-sw) then do:   
   message "Move tested repair to 'cls' stage?. "  
   view-as alert-box question buttons yes-no 
   update s3 as logical.
   if s3 then
    assign s3 = s3. 
   else   
    leave.
  end. 

  assign v-emailmsg = "".
/*
  /** used in vaext_timer.pc ****/
  {vaeh.gfi &vano  = c-ztmk.orderno 
            &vasuf = c-ztmk.ordersuf  
            &lock  = "no"
            &buf   = "nn-"}
   create ltimes.
   assign ltimes.orderno  = c-ztmk.orderno 
          ltimes.ordersuf = c-ztmk.ordersuf
          ltimes.techid   = c-ztmk.techid
          ltimes.whse     = if avail nn-vaeh then 
                               nn-vaeh.whse 
                            else 
                               "".
   **********/

   assign c-ztmk.punchoutdt = today
          c-ztmk.statuscd   = 5 
          c-ztmk.operinit   = g-operinits 
          c-ztmk.punchouttm = substring(string(time,"hh:mm"),1,2) +
                              substring(string(time,"hh:mm"),4,2) +
                              substring(string(time,"hh:mm:ss"),7,2).
   assign t-days   = c-ztmk.punchoutdt - wk-punchindt
          t-ordqty = t-ordqty + 1
          t-tmout  = (dec(substring(c-ztmk.punchouttm,1,2)) * 60) +
                      dec(substring(c-ztmk.punchouttm,3,2))
          t-tmin   = (dec(substring(wk-punchintm,1,2)) * 60) + 
                      dec(substring(wk-punchintm,3,2))
          t-time   =  t-time + (t-tmout - t-tmin).

    /* event driven by v-stgint(new stg) or m-stgint(previous stg) **/ 
    if v-stgint = 3 and f9-sw and not v-printed then do:
       find oimsp where oimsp.person = "costing" 
            no-lock no-error. 
       find first vasp where 
                  vasp.cono = g-cono and 
                  vasp.shipprod = x-tagno
                  no-lock no-error.
       find first arsc where 
                  arsc.cono = g-cono and
                  arsc.custno = dec(string(vasp.user5,"999999999999")) 
                  no-lock no-error. 
       if avail arsc then 
        assign d-custnm = arsc.name. 
       else 
       assign d-custnm = "".           
       if avail vasp then do:
       assign d-custno    = dec(string(vasp.user5,"999999999999")) 
              d-prod      = vasp.refer 
              d-loc       = vasp.whse
              d-tech      = c-ztmk.techid
              s-printernm = if avail oimsp then 
                             oimsp.miscdata[1]
                            else "fgrfofflj4"
              s-punchindt = string(today,"99/99/99")
              s-punchintm = substring(string(time,"hh:mm"),1,2) +
                            substring(string(time,"hh:mm"),4,2) +
                            substring(string(time,"hh:mm:ss"),7,2). 
       {w-sasp.i s-printernm no-lock} 
       if avail sasp then do: 
        output through value(sasp.pcommand).       
        display d-tech g-vaquoteno d-custno d-custnm
                d-prod d-loc g-operinits
                s-punchindt s-punchintm with frame f-costlst.
                         
        output close. 
       end. 
/* TAH jen */
/*       else do: */
            assign v-emailmsg = 
                   "Cust: " + string (d-custno) + " " +
                   "Repair: " +  g-vaquoteno + " " +
                   "in Costing". 
            assign v-emailmsg = vasp.shipprod + "^" +   
                                vasp.whse + "^" + v-emailmsg.   

           /*
                 run sendemailmsg.p( 2,"testout",vasp.whse,"VA",v-takenby,
                         v-emailmsg,
                         "","",0,0).
           */
             run sendemailmsg.p( 2,"tdnout",vasp.whse,"VA","",
                                v-emailmsg,
                                "","",
                                c-ztmk.orderno,c-ztmk.ordersuf). 
          
/*        end.  */
      end. 
    end.     
    if (v-stgint = 11 and f9-sw) then do:   
     run get_tagseqno(output v-jobseqno).
     create ct-ztmk. 
     assign ct-ztmk.jobseqno  = v-jobseqno
            ct-ztmk.cono      = g-cono        
            ct-ztmk.ordertype = "sc" 
            ct-ztmk.user1     = x-tagno        
            ct-ztmk.stagecd   = 15 
            ct-ztmk.orderno   = if v-stgint > 6 then 
                                 s-vano 
                                else 
                                 0
            ct-ztmk.ordersuf  = 0
            ct-ztmk.techid    = if g-technician ne "" then 
                                 g-technician 
                                else 
                                 "admn" 
            ct-ztmk.punchindt = today
            ct-ztmk.punchintm = substring(string(time,"hh:mm"),1,2) +
                                substring(string(time,"hh:mm"),4,2) +
                                substring(string(time,"hh:mm:ss"),7,2)
            ct-ztmk.punchoutdt = today
            ct-ztmk.statuscd   = 5 
            ct-ztmk.punchouttm = substring(string(time,"hh:mm"),1,2) +
                                 substring(string(time,"hh:mm"),4,2) +
                                 substring(string(time,"hh:mm:ss"),7,2)
            ct-ztmk.operinit  = g-operinit  
            ct-ztmk.transtm   = if ct-ztmk.transtm = "" then 
                                 substring(string(time,"hh:mm"),1,2) +
                                 substring(string(time,"hh:mm"),4,2) 
                                else 
                                 ct-ztmk.transtm
            ct-ztmk.transdt   = if ct-ztmk.transdt = ? then 
                                 today
                                else 
                                 ct-ztmk.transdt
            s-punchintm      = ct-ztmk.punchintm              
            s-punchindt      = string(ct-ztmk.punchindt,"99/99/99")
            s-punchottm      = "" 
            s-punchottm      = replace(s-punchottm,":",""). 
     display s-punchintm s-punchindt with frame f-vaexp.
     find first vasp where
                vasp.cono = g-cono       and 
                vasp.shipprod = x-tagno  and 
                vasp.whse = entry(1,g-vaquoteno,"-")
                no-error.
     
     find first zsdivasp use-index k-repairno where
                zsdivasp.cono = g-cono       and 
                zsdivasp.repairno = x-tagno  and 
                zsdivasp.whse = entry(1,g-vaquoteno,"-")
                no-error.
     
     if avail vasp then do: 
      /** updating stage directly to closed **/
      assign zsdivasp.stagecd = 15 
             zsdivasp.costingdt = today 
             vasp.user7 = 15
              s-stgstat = if avail ct-ztmk and ct-ztmk.punchoutdt = ? then 
                           "open"
                          else 
                           "closed" 
              s-tech    = if avail ztmk and ztmk.techid ne "admn" then 
                           ztmk.techid 
                          else 
                           "" 
             s-stageprt = if vasp.user7 = 0 then 
                           v-stage2[1]
                          else    
                          if int(vasp.user7) = 5 or int(vasp.user7) = 6 then 
                           "ToBe" + "-" + v-stage2[int(vasp.user7)]
                          else 
                           v-stage2[int(vasp.user7)]. 
      display s-stageprt s-tech s-stgstat with frame f-vaexp. 
     end.
     if avail vasp then do:  
        find first arsc where
                   arsc.cono   = g-cono and 
                   arsc.custno = dec(string(vasp.user5,"999999999999"))
        no-lock no-error.
        assign v-takenby = "".
        find first vaeh where 
                   vaeh.cono  = g-cono and 
                   vaeh.vano  = int(substring(vasp.user2,1,7)) and 
                   vaeh.user2 = x-tagno 
                   no-lock no-error.           
        if avail vaeh then do: 
         find first vaelo where 
                    vaelo.cono = g-cono   and 
                    vaelo.ordertype = "o" and 
                    vaelo.vano = vaeh.vano 
                    no-lock no-error. 
         if avail vaelo then do:            
          find first oeeh where 
                     oeeh.cono     = g-cono           and 
                     oeeh.orderno  = vaelo.orderaltno and 
                     oeeh.ordersuf = vaelo.orderaltsuf
                     no-lock no-error.  
           if avail oeeh then   
            assign v-takenby = oeeh.takenby.
          end. /* avail vaelo */
        end. /* avail vaeh */
       if avail arsc then 
        assign d-custnm = arsc.name. 
       else 
       assign d-custnm = "".        
       assign v-emailmsg = " Customer Name: " + trim(d-custnm)   +
                           " Part Number  : " + trim(vasp.refer) +
                           " - Closed Stage"
              v-emailmsg = vasp.shipprod + "^" +   
                           vasp.whse + "^" + v-emailmsg.   
     end. /* avail vasp */
     run sendemailmsg.p( 2,"testout",vasp.whse,"VA",v-takenby,
                         v-emailmsg,
                         "","",0,0).
     release vasp. 
   end. /* v-stgint 11  */
  release c-ztmk.
  if avail nn-vaeh then 
     release nn-vaeh.
  assign t-days = if t-days > 0 then 
                   ((t-days * 24) * 60)
                  else 
                    0
         t-time = (t-time / t-ordqty)
         t-time =  t-time + t-days. 

/*   run vaext_timer.p. */

end.
end procedure.

procedure get_jobseqno: 
do: 
   define output param s-jobseqno as  dec format "zzzzzz9.99-"  no-undo.
   define buffer gj-ztmk for ztmk.
   find last gj-ztmk use-index k-seqnot where
             gj-ztmk.cono      = g-cono       and 
             gj-ztmk.ordertype = "sc"         and 
             gj-ztmk.techid    = (if (v-stgint = 3 or v-stgint = 10 or 
                                      v-stgint = 11) then 
                                      g-technician 
                                  else                                         
                                      "admn") and  
             gj-ztmk.statuscd  = 0            and  
             gj-ztmk.stagecd   = v-stgint     and  
             gj-ztmk.jobseqno  > 0             
             no-lock no-error.
   if avail gj-ztmk then 
   do:
      assign s-jobseqno = gj-ztmk.jobseqno.     
   end. 
   else do:  
      find last gj-ztmk use-index k-seqnot where
                gj-ztmk.cono      = g-cono       and 
                gj-ztmk.ordertype = "sc"         and 
                gj-ztmk.techid    = (if (v-stgint = 3 or v-stgint = 10 or
                                         v-stgint = 11) then 
                                         g-technician 
                                     else                    
                                         "admn") and 
                gj-ztmk.jobseqno  > 0            
                no-lock no-error.
      if avail gj-ztmk then 
      do: 
         assign s-jobseqno = gj-ztmk.jobseqno + 1.
      end.
      else 
      do:
         assign s-jobseqno = 1.
      end.
   end.
   release gj-ztmk.
   leave.
end. 
end procedure.

procedure get_tagseqno: 
do: 
   define output param s-jobseqno as  dec format "zzzzzz9.99-"  no-undo.
   define buffer gj-ztmk for ztmk.
/*
   find last gj-ztmk use-index k-seqnot where
             gj-ztmk.cono      = g-cono       and 
             gj-ztmk.ordertype = "sc"         and
             gj-ztmk.techid    = (if (v-stgint = 3 or v-stgint = 10 or 
                                      v-stgint = 11) then 
                                      g-technician 
                                  else                                         
                                     "admn")  and  
             gj-ztmk.stagecd   = v-stgint     and  
             gj-ztmk.jobseqno  > 0             
             no-lock no-error.
   if avail gj-ztmk then do:
      assign s-jobseqno = gj-ztmk.jobseqno + 1.     
   end. 
   else do:  
      find last gj-ztmk use-index k-seqnot where
                gj-ztmk.cono      = g-cono       and 
                gj-ztmk.ordertype = "sc"         and
                gj-ztmk.techid    = (if (v-stgint = 3 or v-stgint = 10 or 
                                         v-stgint = 11) then 
                                         g-technician 
                                     else        
                                         "admn") and 
                gj-ztmk.jobseqno  > 0             
                no-lock no-error.
      if avail gj-ztmk then 
      do: 
         assign s-jobseqno = gj-ztmk.jobseqno + 1.
      end.
      else 
      do:
         assign s-jobseqno = 1.
      end.
   end.
   release gj-ztmk.
   leave.
*/
end. 
end procedure.

procedure prt-routine:
do:
   {w-sasoo.i g-operinits no-lock}
   if avail sasoo then do: 
    assign s-printernm = "vid" /* sasoo.printernm */
           s-rpttyp    = "r" .
    release sasoo.
   end.   
   assign x-function =
"      F7-JUMP(VAIO)      F8-SPcNotes       F9-Punchout      F10-RPTDETAIL     ".
   display x-function with frame f-functions. 
   rptlbl:
   repeat:
   update s-printernm 
          s-rpttyp
          with frame f-printer
   editing:
   readkey.
    if {k-cancel.i} then do: 
     hide frame f-printer.
     return. 
    end.
    apply lastkey.
   end.  /* editing */

   assign s-printernm
          s-rpttyp = if s-rpttyp ne "" then 
                        s-rpttyp 
                     else 
                        "". 
   {w-sasp.i s-printernm no-lock}
   if avail sasp then 
      assign printer-cmd = sasp.pcommand.
   if s-rpttyp ne "t" and 
      s-rpttyp ne "r" then do: 
    assign v-error = "   --Invalid Report Type--   ".
    run zsdierrx.p(v-error,"yes"). 
    next-prompt s-rpttyp with frame f-printer.
    next rptlbl.
   end.
   if s-printernm = "" or not avail sasp then do:   
    assign v-error = "   ----Invalid Printer----   ".
    run zsdierrx.p(v-error,"yes"). 
    next rptlbl.
   end.
   else 
   if g-technician = "" and 
      s-rpttyp = "t"    then do: 
    assign v-error = "  Blank Tech Invalid     ".
    run zsdierrx.p(v-error,"yes"). 
    next rptlbl.
   end. 
   else 
   if g-vaquoteno = "" and s-rpttyp = "r" and s-vano <= 0 then do: 
    assign v-error = "  Blank RepirNo Invalid     ".
    run zsdierrx.p(v-error,"yes"). 
    next rptlbl.
   end. 
 
  /*  Randomize disk file name  */
   nameloop: 
   do while true:
      assign v-rxfax    = "/usr/tmp/" + string(time) + string(random(1,999))
             s-filename = substring(v-rxfax,10,8).         
      if search(v-rxfax) = ? then
        leave nameloop.
   end.
   output to value(v-rxfax) paged page-size 54. 
   view frame f-hdr1.

   if s-rpttyp = "r" and g-vaquoteno ne ""  then 
   for each ztmk use-index k-ztmk-sc2 where
            ztmk.cono      = g-cono       and 
            ztmk.ordertype = "sc"         and 
            ztmk.user1     = x-tagno 
            no-lock
            break by ztmk.cono 
                  by ztmk.user1 
                  by ztmk.statuscd 
                  by ztmk.punchoutdt descending
                  by ztmk.punchouttm descending
                  by ztmk.techid
                  by ztmk.stagecd:
    do inx2  = 1 to 16: 
     if inx2 = ztmk.stagecd then do: 
      if (ztmk.stagecd = 5 or 
          ztmk.stagecd = 6) and ztmk.punchoutdt = ? then  
       assign s-stageprt = "ToBe" + "-" + v-stage2[inx2].
      else    
       assign s-stageprt = v-stage2[inx2].
      leave.
     end.
    end.
    assign d-vanonbr = if ztmk.orderno > 0 then 
                        string(ztmk.orderno,"9999999") +  
                        "-" + string(ztmk.ordersuf,"99")
                       else 
                        entry(1,ztmk.user1,"-") + "-" + 
                        left-trim(entry(2,ztmk.user1,"-"),"0"). 
    /* add total time to f10 report */ 
    assign v-indate   = ztmk.punchindt 
           v-intime   = ztmk.punchintm 
           v-outdate  = if ztmk.punchoutdt ne ? then 
                         ztmk.punchoutdt 
                        else 
                         today 
           v-outtime  = if ztmk.punchouttm ne "" then 
                         ztmk.punchouttm 
                        else  
                         string(truncate((time modulo 86400) / 3600 ,0),"99")
                       + string(truncate((time modulo 3600) / 60 ,0),"99")   
                       + string(truncate((time modulo 60) / 1 ,0),"99").  
     run timespan (input  v-indate,
                   input  v-intime,
                   input  v-outdate,
                   input  v-outtime,
                   output v-lendays,
                   output v-lenhr,
                   output v-lenmin,
                   output v-lensec).
     assign t-days   = v-lendays
            substring(prt-time,1,3) = string(v-lenhr,"zz9")
            substring(prt-time,4,2) = string(v-lenmin,"99").
     assign prt-techid = if ztmk.techid = "admn" then 
                          ztmk.operinit
                         else 
                          ztmk.techid.
     display prt-techid d-vanonbr s-stageprt
             ztmk.punchindt ztmk.punchintm 
             ztmk.punchoutdt ztmk.punchouttm 
             t-days
             prt-time
             with frame f-detail.
             down with frame f-detail.
     assign t-tmin  = 0 
            t-tmout = 0.
   end.  /* for each */
/*
   else 
   if s-rpttyp = "r" and s-vano > 0 then 
   for each ztmk use-index k-ztmk where
            ztmk.cono      = g-cono       and 
            ztmk.ordertype = "sc"         and  
            ztmk.orderno   = s-vano       and 
            ztmk.ordersuf  = s-vasuf       
            no-lock
            break by ztmk.cono 
                  by ztmk.orderno 
                  by ztmk.ordersuf
                  by ztmk.statuscd 
                  by ztmk.punchoutdt descending
                  by ztmk.punchouttm descending
                  by ztmk.techid
                  by ztmk.stagecd:
    do inx2  = 1 to 16: 
     if inx2 = ztmk.stagecd then do: 
      if (ztmk.stagecd = 5 or 
          ztmk.stagecd = 6) and ztmk.punchoutdt = ? then  
       assign s-stageprt = "ToBe" + "-" + v-stage2[inx2].
      else    
       assign s-stageprt = v-stage2[inx2].
      leave.
     end.
    end.
    assign d-vanonbr = string(ztmk.orderno,"9999999") + "-" + 
                       string(ztmk.ordersuf,"99").
    display ztmk.techid d-vanonbr s-stageprt
            ztmk.punchindt ztmk.punchintm 
            ztmk.punchoutdt ztmk.punchouttm 
            with frame f-detail.
            down with frame f-detail.
   end.  /* for each */
*/
   if s-rpttyp = "t" then  
   for each ztmk use-index k-techin-sc where
            ztmk.cono      = g-cono       and 
            ztmk.ordertype = "sc"         and
            ztmk.techid    = g-technician  
            no-lock
            break by ztmk.cono 
                  by ztmk.techid
                  by ztmk.statuscd 
                  by ztmk.jobseqno 
                  by ztmk.orderno 
                  by ztmk.ordersuf
                  by ztmk.punchoutdt descending
                  by ztmk.punchouttm descending
                  by ztmk.stagecd:
     do inx2  = 1 to 16: 
      if inx2 = ztmk.stagecd then do: 
       assign s-stageprt = v-stage2[inx2].
       leave.
      end.
     end.
     assign d-vanonbr = if ztmk.orderno > 0 then 
                         string(ztmk.orderno,"9999999") +  
                         "-" + string(ztmk.ordersuf,"99")
                        else 
                         entry(1,ztmk.user1,"-") + "-" + 
                         left-trim(entry(2,ztmk.user1,"-"),"0"). 
    /* add total time to f10 report */ 
    assign v-indate   = ztmk.punchindt 
           v-intime   = ztmk.punchintm 
           v-outdate  = if ztmk.punchoutdt ne ? then 
                         ztmk.punchoutdt 
                        else 
                         today 
           v-outtime  = if ztmk.punchouttm ne "" then 
                         ztmk.punchouttm 
                        else  
                         string(truncate((time modulo 86400) / 3600 ,0),"99")
                       + string(truncate((time modulo 3600) / 60 ,0),"99")   
                       + string(truncate((time modulo 60) / 1 ,0),"99").  
     run timespan (input  v-indate,
                   input  v-intime,
                   input  v-outdate,
                   input  v-outtime,
                   output v-lendays,
                   output v-lenhr,
                   output v-lenmin,
                   output v-lensec).
     assign t-days   = v-lendays
            substring(prt-time,1,3) = string(v-lenhr,"zz9")
            substring(prt-time,4,2) = string(v-lenmin,"99").
    assign prt-techid = if ztmk.techid = "admn" then 
                          ztmk.operinit
                        else 
                          ztmk.techid.
    display prt-techid d-vanonbr s-stageprt
            ztmk.punchindt ztmk.punchintm 
            ztmk.punchoutdt ztmk.punchouttm 
            t-days 
            prt-time
            with frame f-detail.
            down with frame f-detail.
    assign t-tmin  = 0 
           t-tmout = 0.
   end.  /* for each */
   leave rptlbl.
 end. /* repeat */

 if avail sasp and sasp.printernm = "vid" then do:    
  output close.
  assign s-directory = "/usr/tmp/"               
         s-printer   = "vid".                    
   run porzx_vid.p.
 end.                                            
 else                                                                
 if avail sasp and sasp.printernm ne "vid" then do:
  assign v-string = (printer-cmd + " " + v-rxfax).
         unix silent value(v-string).
         status default 
         "Printing in progress..        ".
         pause 1.
 end.
 assign os-cmd-string = "rm " + v-rxfax. 
 os-command value(os-cmd-string). 
 display x-function with frame f-functions.               
end. 
end procedure.

procedure titlemorph:

find first sassm where sassm.currproc = v-myframe no-lock no-error.
if avail sassm then
  g-title = sassm.frametitle.
end.  
         
/*-----------------------------------------------------------------------*/
procedure MakeQuoteNo:
/*-----------------------------------------------------------------------*/
define input  parameter ip-quoteno   as character no-undo.
define output parameter ip-DBquoteno as character no-undo.
define var z as char format "x(07)"               no-undo. 
define var y as int                               no-undo.
assign v-errfl = no.

if num-entries(ip-quoteno,"-") <> 2 then do: 
 assign ip-DBquoteno = ip-quoteno
             v-errfl = yes
             v-error = 
             "  Invalid RepairNo. --> " + ip-quoteno +   
             " - Please enter format 'Whse-7
             '".
 return.
end.

 assign z = trim(entry(2,ip-quoteno,"-"),"0123456789").
 assign y = length(z).

if y ne 0 then do: 
 assign ip-DBquoteno = ip-quoteno
             v-errfl = yes
             v-error = 
             "Invalid RepairNo. --> " + ip-quoteno +   
             " - Please enter format 'Whse-7'".
 return.
end.

assign ip-DBquoteno  = entry (1,ip-quoteno,"-") + "-" +
                      string(int(entry(2,ip-quoteno,"-")),"9999999").
                                                        
end.   
    
/* -------------------------------------------------------------------------  */
Procedure Bottom2:
/* -------------------------------------------------------------------------  */
do: 
  define var ix as integer     no-undo. 
  define var del-fl as logical no-undo. 
  define buffer b-vasp for vasp. 
  assign v-comments = "".  
  display v-comments with frame f-bot2.
  assign x-function =
"      F7-JUMP(VAIO)      F8-SPCNOTES       F9-Punchout      F10-RptDetail     ".
  display x-function with frame f-functions. 

  if g-vaquoteno ne "" then do: 
   run makequoteno(g-vaquoteno,output x-tagno).
   assign g-whse = entry(1,g-vaquoteno,"-"). 
   find first vasp where
              vasp.cono = g-cono       and 
              vasp.shipprod = x-tagno  and 
              vasp.whse = entry(1,x-tagno,"-")
              no-lock no-error.
   if avail vasp then do:            
     run vasp-inuse.p(input x-tagno,                 
                      input g-operinits,               
                      input "x",                       
                      input-output v-inuse).           
     if not v-inuse then do:                           
      run vasp-inuse.p(input x-tagno,                
                       input g-operinits,              
                       input "a",                      
                       input-output v-inuse).          
      find first vasp where                              
                 vasp.cono = g-cono       and          
                 vasp.shipprod = x-tagno  and          
                 vasp.whse = entry(1,x-tagno,"-")      
                 no-lock no-error.                       
     end.
     else 
     if v-inuse and                                          
       (vasp.xxc4 ne "" and                                   
        vasp.xxc4 ne string(g-operinits,"x(4)") +             
       " in --> " + g-currproc) then do:                     
      assign v-error = " RepairNo is in-use by operator: " + 
                         string(vasp.xxc4,"x(20)").          
      run zsdierrx.p(v-error,"yes").                         
      assign my-prompt = 1.
      leave. 
     end.  
   end.
   find first b-vasp use-index k-vasp where 
              b-vasp.cono     = g-cono  and 
              b-vasp.shipprod = x-tagno and  
              b-vasp.whse     = g-whse 
              no-error.
  end.
  if not avail b-vasp then do:  
   find first ztmk use-index k-ztmk where 
              ztmk.cono      = g-cono and 
              ztmk.ordertype = "sc"   and 
              ztmk.orderno   = s-vano and 
              ztmk.ordersuf  = s-vasuf
              no-lock no-error.
   if avail ztmk then do: 
    assign x-tagno = ztmk.user1.
    find first b-vasp use-index k-vasp where 
               b-vasp.cono     = g-cono     and 
               b-vasp.shipprod = (if avail ztmk then 
                                   ztmk.user1
                                  else    
                                   x-tagno) and  
               b-vasp.whse     = g-whse 
               no-error.
    if not avail b-vasp then do: 
     bell.
     assign v-error = "  RepairNo/Vano Notfound      ".
     run zsdierr(v-error,"yes").
     leave.  
    end. 
   end. /* not avail vasp */ 
   else do: 
    bell.
    assign v-error = "  RepairNo/Vano Notfound      ".
    run zsdierrx.p(v-error,"yes"). 
    leave.  
   end. 
  end.
 
  for each notes
   where notes.cono         = g-cono   and 
         notes.notestype    = "in"     and 
         notes.primarykey   = x-tagno  and 
         notes.secondarykey = g-whse
         no-lock:
   do v-idx = 1 to 16:        
      assign v-comments[v-idx] = notes.noteln[v-idx].
   end. 
  end. 
  display v-comments with frame f-bot2.
  assign v-repositionfl = true.

 Bottom2Loop:
 do while true on endkey undo, leave Bottom2Loop:
   if v-repositionfl then do:
    do v-repositionix = 16 to 1 by -1:
      if v-comments[v-repositionix] <> "" then do: 
        if v-repositionix ne 16 then 
         assign v-repositionix = v-repositionix + 1. 
        leave.
      end. 
    end.     
    if v-repositionix = 0 then 
     assign v-repositionix = 1.
    next-prompt v-comments[v-repositionix] with frame f-bot2.
   end.  

   if lastkey = 312 and v-comments[v-repositionix] = "" then 
    assign v-repositionfl = false.
   
   update  v-comments[1]
           v-comments[2]
           v-comments[3]
           v-comments[4]
           v-comments[5]
           v-comments[6]
           v-comments[7]
           v-comments[8]
           v-comments[9]
           v-comments[10]
           v-comments[11]
           v-comments[12]
           v-comments[13]
           v-comments[14]
           v-comments[15]
           v-comments[16]
    with frame f-bot2 
   editing:
   if v-repositionfl then do:
    assign v-repositionfl = false.
    if v-repositionix = 1 then
     apply "right-end" to v-comments[1] in frame f-bot2. 
    else
    if v-repositionix = 2 then
     apply "right-end" to v-comments[2] in frame f-bot2. 
    else
    if v-repositionix = 3 then
     apply "right-end" to v-comments[3] in frame f-bot2. 
    else
    if v-repositionix = 4 then
     apply "right-end" to v-comments[4] in frame f-bot2. 
    else
    if v-repositionix = 5 then
     apply "right-end" to v-comments[5] in frame f-bot2. 
    else
    if v-repositionix = 6 then
     apply "right-end" to v-comments[6] in frame f-bot2. 
    else
    if v-repositionix = 7 then
     apply "right-end" to v-comments[7] in frame f-bot2. 
    else
    if v-repositionix = 8 then
     apply "right-end" to v-comments[8] in frame f-bot2. 
    else
    if v-repositionix = 9 then
     apply "right-end" to v-comments[9] in frame f-bot2. 
    else
    if v-repositionix = 10 then
     apply "right-end" to v-comments[10] in frame f-bot2. 
    else
    if v-repositionix = 11 then
     apply "right-end" to v-comments[11] in frame f-bot2. 
    else
    if v-repositionix = 12 then
     apply "right-end" to v-comments[12] in frame f-bot2. 
    else
    if v-repositionix = 13 then
     apply "right-end" to v-comments[13] in frame f-bot2. 
    else
    if v-repositionix = 14 then
     apply "right-end" to v-comments[14] in frame f-bot2. 
    else
    if v-repositionix = 15 then
     apply "right-end" to v-comments[15] in frame f-bot2. 
    else
    if v-repositionix = 16 then
     apply "right-end" to v-comments[16] in frame f-bot2. 
   end.
 
 readkey.
  if {k-func12.i} then do: 
   do v-idx = 1 to 16:        
      assign v-comments[v-idx] = input v-comments[v-idx].
   end. 
   assign v-fidx2 = v-repositionix.
   apply lastkey.
   assign v-repositionfl = true.
   next bottom2loop.
  end.
  else 
  if {k-jump.i} or {k-cancel.i} then do: 
   apply lastkey.
   leave Bottom2Loop.
  end.
  apply lastkey.  
 end. /* editing */     
  
  find first notes
       where notes.cono         = g-cono  and 
             notes.notestype    = "in"    and 
             notes.primarykey   = x-tagno and 
             notes.secondarykey = g-whse   
             no-error.
  if not avail notes then do: 
   create notes.          
   assign notes.cono         = g-cono
          notes.notestype    = "in"
          notes.primarykey   = x-tagno
          notes.secondarykey = g-whse
          notes.printfl      = yes.
   do v-idx = 1 to 16:        
    assign notes.noteln[v-idx] = v-comments[v-idx].
   end. 
  end. 
  else do: 
   do v-idx = 1 to 16:        
    assign notes.noteln[v-idx] = v-comments[v-idx].
   end. 
  end.  
  if avail notes then do: 
   assign del-fl = yes. 
   do ix = 1 to 16:
    if notes.noteln[ix] ne "" then do: 
     assign del-fl = no.
    end.
   end. 
   if del-fl = yes and avail b-vasp then do: 
    assign b-vasp.notesfl  = "". 
    delete notes.
   end.    
  end. 
  if avail b-vasp then 
   release b-vasp.
  if avail notes then 
   release notes. 
  if avail ztmk then
   release ztmk.
  leave Bottom2Loop.
  end. 
 end.  /* Bottom2Loop */
 hide frame f-bot2.
 define buffer b-vasps for vasps. 
 define var mv-idx as int no-undo. 
 assign mv-idx = 1.
 for each notes
    where notes.cono         = g-cono   and 
          notes.notestype    = "fa"     and 
          notes.primarykey   = x-tagno  and 
          notes.secondarykey = g-whse   and 
          notes.requirefl    = no       and 
          notes.pageno       = 1 
          no-lock:
    do v-idx = 1 to 16:        
       assign v-comments3[v-idx] = notes.noteln[v-idx].
    end. 
  end. 
  do v-idx = 1 to 16:        
   if v-comments[v-idx] ne "" then do:
    assign v-comments2[v-idx] = v-comments[v-idx].    
  end.
 end.
 find first b-vasps use-index k-vasp where 
            b-vasps.cono     = g-cono    and 
            b-vasps.shipprod = x-tagno   and  
            b-vasps.whse     = g-whse    and
            b-vasps.seqno    = 1         and 
            b-vasps.sctntype = "sp"  
            no-error.
 if avail b-vasps then do: 
  assign b-vasps.specdata = "". 
/* 
  assign substring(b-vasps.specdata,mv-idx,59) =
 "------------------ Specification Notes ---------------------"  + "~012"  
   assign mv-idx = mv-idx + 61. */                               
   do v-idx = 1 to 16:        
   if v-comments2[v-idx] ne "" then do: 
    assign fillit = length(v-comments2[v-idx])
           v-comments2[v-idx] =
           v-comments2[v-idx] + fill(" ",60 - fillit)
           substring(b-vasps.specdata,mv-idx,59) =
                     v-comments2[v-idx] + "~012" 
           mv-idx = mv-idx + 61.
   end.
 end. 
/*
 assign substring(b-vasps.specdata,mv-idx,59) =
 "---------------------- External Notes ----------------------"  + "~012" 
        mv-idx = mv-idx + 61.
 do v-idx = 1 to 16:        
  if v-comments3[v-idx] ne "" then do: 
   assign fillit = length(v-comments3[v-idx])
          v-comments3[v-idx] =
          v-comments3[v-idx] + fill(" ",60 - fillit)
          substring(b-vasps.specdata,mv-idx,59) =
                    v-comments3[v-idx] + "~012" 
          mv-idx = mv-idx + 61.
  end.
 end. 
*/ 
 end. /* avail vasp */
  
 if {k-jump.i} or {k-cancel.i} then do: 
  leave. 
 end.
end.


/* -------------------------------------------------------------------------  */
Procedure Total_TagValue:
/* -------------------------------------------------------------------------  */

   assign s-pndinvamt    = 0
          s-pndextrnamt  = 0
          s-pndintrnamt  = 0
          s-pndintrndsp  = 0
          s-pndtotal     = 0
          s-vaprodcost   = 0
          t-csttdn-h     = 0
          t-csttdn-m     = 0
          t-cstasy-h     = 0
          t-cstasy-m     = 0
          t-csttst-h     = 0
          t-csttst-m     = 0.
   find first vasp where
              vasp.cono = g-cono       and 
              vasp.shipprod = x-tagno  and 
              vasp.whse = entry(1,x-tagno,"-")
              no-lock no-error.
   if avail vasp then do: 
     assign p-pct      = if dec(substring(vasp.user1,5,6)) > 0 then  
                           dec(substring(vasp.user1,5,6))
                        else 
                          050.00
           s-pndinvamt = dec(substring(vasp.user1,180,11)).
   end. 
   find first vaspsl where 
              vaspsl.cono = g-cono            and 
              vaspsl.shipprod = vasp.shipprod and 
              vaspsl.compprod = "teardown"    and 
              vaspsl.seqno    = 2
              no-lock no-error. 
   if avail vaspsl then do: 
    assign t-csttdn-h = (v-esttdn-h * vaspsl.prodcost) 
           t-csttdn-m = ((v-esttdn-m * vaspsl.prodcost) / 60). 
   end.       
   find first vaspsl where 
              vaspsl.cono = g-cono               and 
              vaspsl.shipprod = vasp.shipprod    and 
              vaspsl.compprod = "service repair" and 
              vaspsl.seqno    = 6
              no-lock no-error. 
   if avail vaspsl then do: 
    assign t-cstasy-h = (v-estasy-h * vaspsl.prodcost)
           t-cstasy-m = ((v-estasy-m * vaspsl.prodcost) / 60). 
   end. 
   find first vaspsl where 
              vaspsl.cono = g-cono            and 
              vaspsl.shipprod = vasp.shipprod and 
              vaspsl.compprod = "test time"   and 
              vaspsl.seqno    = 6
              no-lock no-error. 
   if avail vaspsl then do: 
    assign t-csttst-h = (v-esttst-h * vaspsl.prodcost)  
           t-csttst-m = ((v-esttst-m * vaspsl.prodcost) / 60). 
   end.        
   assign s-pndintrndsp  = t-csttdn-h + t-csttdn-m + 
                           t-cstasy-h + t-cstasy-m + 
                           t-csttst-h + t-csttst-m. 
   assign s-vaprodcost  = 0
          s-pndextrnamt = 0.
   find first vasp where
              vasp.cono = g-cono       and 
              vasp.shipprod = x-tagno  and 
              vasp.whse = entry(1,x-tagno,"-")
              no-error.
   def buffer t-vaspsl for vaspsl. 
   for each t-vaspsl where 
            t-vaspsl.cono = g-cono      and 
            t-vaspsl.shipprod = x-tagno and 
            t-vaspsl.compprod ne "margin improvement"
            no-lock:
     if t-vaspsl.compprod begins "core-" then do:
      assign s-pndextrnamt = if t-vaspsl.xxde1 > 0 then 
                              s-pndextrnamt + t-vaspsl.xxde1
                             else
                              s-pndextrnamt. 
      next.
     end.
     assign s-vaprodcost  = s-vaprodcost + 
                           (t-vaspsl.prodcost * t-vaspsl.qtyneeded)
                            s-pndextrnamt = if t-vaspsl.xxde1 > 0 then 
                               s-pndextrnamt + t-vaspsl.xxde1
                            else
                               s-pndextrnamt. 
   end. /* t-vaspsl for each */
   assign s-pndtotal = s-vaprodcost + s-pndintrndsp + s-pndextrnamt
          s-trendtot = s-pndtotal * 1.04.
   if substring(vasp.user1,250,1) = "y" then 
    leave.
   else do: 
    assign s-pndinvamt = (s-trendtot / (100 - p-pct) * 100)
           substring(vasp.user1,160,10) = string(s-pndextrnamt,"999999.99-")
           substring(vasp.user1,180,11) = string(s-pndinvamt,"9999999.99-")
           s-pndinvamt = if s-pndinvamt > 9999999 then 
                          9999999
                         else 
                          s-pndinvamt.
   end.
 
end. /* procedure */ 

procedure timespan:    
  define input  parameter ix-indate    as date no-undo.
  define input  parameter ix-intime    as char format "xx:xx:xx" no-undo.
  define input  parameter ix-outdate   as date no-undo.
  define input  parameter ix-outtime   as char format "xx:xx:xx" no-undo.
  define output parameter ix-lendays   as integer no-undo.
  define output parameter ix-lenhr     as integer no-undo.
  define output parameter ix-lenmin    as integer no-undo.
  define output parameter ix-lensec    as integer no-undo.


  def var wx-days as integer no-undo.
  def var wx-tm   as integer no-undo.
  def var wx-itm  as integer no-undo.
  def var wx-otm  as integer no-undo.

  assign wx-days = ix-outdate - ix-indate
         wx-itm  = (dec(substring(ix-intime,1,2)) * 3600) +
                    dec(substring(ix-intime,3,2)) * 60 +       
                    dec(substring(ix-intime,5,2))

         wx-otm  = (dec(substring(ix-outtime,1,2)) * 3600) +
                    dec(substring(ix-outtime,3,2)) * 60 +       
                    dec(substring(ix-outtime,5,2)).
                             
  assign wx-tm   = if wx-days = 0 then                          
                     wx-otm - wx-itm                         
                   else
                     (((wx-days * (24 * 3600)) + wx-otm) - wx-itm).  
  assign ix-lendays = truncate((wx-tm / 86400),0)
         ix-lenhr   = truncate((wx-tm modulo 86400) / 3600 ,0)
         ix-lenmin  = truncate((wx-tm modulo 3600 ) / 60   ,0)
         ix-lensec  = truncate((wx-tm modulo 60)    / 1    ,0).
end.
    
    


 
