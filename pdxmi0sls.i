/* 
   This procedure will go through all the customers to find the contracts that
   do not have any sales for the year
*/

procedure get-0-sales-info:
 for each arsc use-index k-arsc where arsc.cono = g-cono and
                                      arsc.custno     >= b-customer and
                                      arsc.custno     <= e-customer and
                                      arsc.statustype  = yes and
                                      arsc.slsrepout  >= b-salesrep and
                                      arsc.slsrepout  <= e-salesrep 
                                      no-lock:

   find smsn use-index k-smsn where smsn.cono = 1 and
                                    smsn.slsrep = arsc.slsrepout
                                    no-lock no-error.
   if not avail smsn then next.
   if avail smsn then do:
      if smsn.mgr < b-region or
         smsn.mgr > e-region then
         next.
   end.

   for each pdsc use-index k-custprod where pdsc.cono       = g-cono      and
                                            pdsc.statustype = yes         and
                                            pdsc.levelcd    = 1           and
                                            pdsc.custno     = arsc.custno and
                                            pdsc.startdt   <= beg-date    and
                                           (pdsc.enddt     >= e1-date or
                                            pdsc.enddt      = ?)
                                            no-lock:
      
      find first icsw use-index k-icsw where 
                                    icsw.cono        = g-cono    and
                                    icsw.prod        = pdsc.prod and
                                    icsw.statustype <> "X"       and
                                    icsw.pricetype >= b-ptype    and
                                    icsw.pricetype <= e-ptype
                                    no-lock no-error.
      if not avail icsw then next.
      find first matrix use-index k-salesrep where 
                                  matrix.region   = substring(smsn.mgr,1,1) and
                                  matrix.district = substring(smsn.mgr,2,3) and
                                  matrix.salesrep = arsc.slsrepout and
                                  matrix.customer = pdsc.custno and
                                  matrix.product  = pdsc.prod
                                  no-lock
                                  no-error.
      if avail matrix then next.
      else do:
         assign w-description = ""
                w-pricetype   = ""
                w-stndcost    = 0
                w-listprice   = 0
                w-product     = "".
         for each icsd where icsd.cono = g-cono and
                             icsd.salesfl = yes
                             no-lock:
            find first icsw use-index k-icsw where 
                                    icsw.cono        = g-cono    and
                                    icsw.prod        = pdsc.prod and
                                    icsw.statustype <> "X"
                                    no-lock no-error.
            if avail icsw then do:
               assign w-pricetype = icsw.pricetype
                      w-stndcost  = icsw.stndcost
                      w-listprice = icsw.listprice
                      w-product   = icsw.prod.
               find icsp use-index k-icsp where 
                                       icsp.cono = g-cono and
                                       icsp.prod = icsw.prod
                                       no-lock no-error.
               if avail icsp then
                  assign w-description = icsp.descrip[1].
               else
                  assign w-description = "".
               leave.   /* leave for each icsd */
            end.
         end.
         
         assign matrix-price = w-listprice.
         if pdsc.prctype = yes then
            assign matrix-price = pdsc.prcmult[1].
         if pdsc.prctype = no and pdsc.prcmult[1] <> 0 then
            assign matrix-price = w-listprice * (pdsc.prcmult[1] / 100).
         if pdsc.prctype = no and pdsc.prcdisc[1] <> 0 then 
            assign matrix-price = 
                   w-listprice * ((100 - pdsc.prcdisc[1]) / 100).
         
         create matrix.
         assign 
             matrix.region        = substring(smsn.mgr,1,1)
             matrix.district      = substring(smsn.mgr,2,3)
             matrix.salesrep      = arsc.slsrepout
             matrix.customer      = arsc.custno
             matrix.name          = substring(arsc.name,1,8)
             matrix.ytdsales      = arsc.salesytd
             matrix.product       = pdsc.prod
             matrix.cprice-type   = arsc.pricetype
             matrix.pprice-type   = w-pricetype
             matrix.pd-recno      = pdsc.pdrecno
             matrix.pd-level      = pdsc.levelcd
             matrix.pd-enddt      = pdsc.enddt
             matrix.pd-trndt      = pdsc.transdt
             matrix.f-level       = pdsc.levelcd
             matrix.c-netprc      = matrix-price
             matrix.c-amount      = matrix-price
             matrix.c-cost        = w-stndcost
             matrix.f-netprc      = if p-calcfutr = "c" then
                                       matrix.c-netprc * w-prcincr
                                    else
                                       matrix-price * w-prcincr
             matrix.f-amount      = if p-calcfutr = "c" then
                                       matrix.c-netprc * w-prcincr
                                    else
                                       matrix-price * w-prcincr
             matrix.f-cost        = w-stndcost * w-costincr.
          do i = 1 to 3:
             assign z-year = string(YEAR(e1-date)).
             assign r-year = int(substring(z-year,3,2)).
             for each smsew use-index k-smsew where
                      smsew.cono      = g-cono      and
                      smsew.yr        = r-year      and
                      smsew.custno    = arsc.custno and
                      smsew.prod      = w-product
                      no-lock:
                do z = 1 to 12:
                   if i = 3 then
                   assign matrix.usage00 = matrix.usage00 + smsew.qtysold[z].
                   if i = 2 then
                   assign matrix.usage01 = matrix.usage01 + smsew.qtysold[z].
                   if i = 1 then
                   assign matrix.usage02 = matrix.usage02 + smsew.qtysold[z].
                end.
             end.
             assign r-year = r-year - 1.
          end.
      end.
   end.
    
   for each pdsc use-index k-custprod where pdsc.cono   = g-cono and
                                            pdsc.statustype = yes and
                                            pdsc.levelcd    = 2 and
                                            pdsc.custno     = arsc.custno and
                                            pdsc.startdt   <= p-date and
                                           (pdsc.enddt     >= p-date or
                                            pdsc.enddt      = ?)
                                            no-lock:
      if substring(pdsc.prod,1,1) = "p" then do:
      
         if substring(pdsc.prod,3,4) < b-ptype or
            substring(pdsc.prod,3,4) > e-ptype then
            next.
      
         find sasta where sasta.cono = g-cono and
                          sasta.codeiden = "k" and
                          sasta.codeval  = substring(pdsc.prod,3,4)
                          no-lock no-error.
         if avail sasta then
            assign w-description = sasta.descrip.
         else
            assign w-description = "".
         assign w-product = substring(pdsc.prod,3,4).
         run "check-type-2".
      end.
 
      if substring(pdsc.prod,1,1) = "l" then do:
      
         if b-ptype <> "" then next.
         
         find first icsl use-index k-prodline where
                                   icsl.cono     = g-cono and
                                   icsl.whse     = arsc.whse and
                                   icsl.prodline = substring(pdsc.prod,15,6)
                                   no-lock no-error.
         if avail icsl then 
            assign w-description = icsl.descrip.
         else
            assign w-description = "".
         assign w-product = substring(pdsc.prod,15,6).
         run "check-type-2".
      end.
 
      if substring(pdsc.prod,1,1) = "c" then do:
         
         if b-ptype <> "" then next.
      
         find sasta where sasta.cono = g-cono and
                          sasta.codeiden = "c" and
                          sasta.codeval  = substring(pdsc.prod,3,4)
                          no-lock no-error.
         if avail sasta then
            assign w-description = sasta.descrip.
         else
            assign w-description = "".
         assign w-product = substring(pdsc.prod,3,4).
         run "check-type-2".
      end.
   end. /* each pdsc (2) */
 end. /* each arsc */
end. /* procedure */

procedure check-type-2:
  find first matrix use-index k-salesrep where 
                              matrix.region   = substring(smsn.mgr,1,1) and
                              matrix.district = substring(smsn.mgr,2,3) and
                              matrix.salesrep = arsc.slsrepout and
                              matrix.customer = pdsc.custno and
                              matrix.product  = w-description
                              no-lock
                              no-error.
  if avail matrix then next.
  else do:
     create matrix.
     assign matrix.region        = substring(smsn.mgr,1,1)
            matrix.district      = substring(smsn.mgr,2,3)
            matrix.salesrep      = arsc.slsrepout
            matrix.customer      = arsc.custno
            matrix.name          = substring(arsc.name,1,8)
            matrix.ytdsales      = arsc.salesytd
            matrix.product       = w-description
            matrix.cprice-type   = arsc.pricetype
            matrix.pprice-type   = ""
            matrix.pd-recno      = pdsc.pdrecno
            matrix.pd-level      = pdsc.levelcd
            matrix.pd-enddt      = pdsc.enddt
            matrix.pd-trndt      = pdsc.transdt.
    end.
end.
