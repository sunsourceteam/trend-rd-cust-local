/*h*****************************************************************************
  INCLUDE      : u-oeio.i
  DESCRIPTION  : List of fields to display in OEIO
  USED ONCE?   : yes (oeio.p)
  AUTHOR       : rhl
  DATE WRITTEN : 09/20/89
  CHANGES MADE :
    12/16/91 pap; TB#  5244 JIT/Line Due - added promised date
    07/20/93 rhl; TB# 10657 Show Entered Data on Inquiry Screen
    04/29/94 dww; TB# 15542 Display of addonamt[1] needs description
    03/16/95 mtt; TB# 15195 RM total shows as positive vs negative
    12/18/95 kr ; TB# 19791 Can view prices if SASOO Price/Disc = no
    12/17/01 mb : TB# e5239 display shipto as soldto when invtofl
    07/02/04 bpa; TB# e19465 RM Terms Discount should display negative value
    08/02/05 cm;  TB# e17682 Add consolidated invoicing
*******************************************************************************/

    oeeh.currencyty
    oeeh.notesfl
    oeeh.approvty
    oeeh.takenby
    oeeh.transtype
    s-orderdisp
    arsc.custno
    arsc.notesfl
    s-stagecd
    oeeh.whse
    arsc.name
/*tb e5239 display shipto as billto when arss.invtofl. */
    arss.name when avail arss and arss.invtofl @ arsc.name
    oeeh.shiptonm
    arsc.addr
    arss.addr[1] when avail arss and arss.invtofl @ arsc.addr[1]
    arss.addr[2] when avail arss and arss.invtofl @ arsc.addr[2]
    oeeh.shiptoaddr
    arsc.city
    arss.city when avail arss and arss.invtofl @ arsc.city
    oeeh.shiptocity
    arsc.state
    arss.state when avail arss and arss.invtofl @ arsc.state
    oeeh.shiptost
    oeeh.shiptozip
    arsc.zipcd
    arss.zipcd when avail arss and arss.invtofl @ arsc.zipcd
    oeeh.shipinstr
    oeeh.consolorderno
    oeeh.consolinvdt
    oeeh.reqshipdt
    oeeh.custpo
    oeeh.jrnlno2
    s-shipviaty
    oeeh.refer
    oeeh.jobno
    oeeh.promisedt
    oeeh.shipdt
    oeeh.billdt
    oeeh.canceldt
    oeeh.invoicedt
    oeeh.paiddt
    s-termstype
    oeeh.slsrepin
    oeeh.slsrepout
    oeeh.placedby
    k-actfreight      when sasoo.oepricefl <> "n" @ oeeh.actfreight 
    s-addondesc       when sasoo.oepricefl <> "n"
    s-otheraddon      when sasoo.oepricefl <> "n" and
    s-otheraddon    <> 0
    ""                when s-otheraddon     = 0   @   s-otheraddon
    s-addon           when sasoo.oepricefl <> "n"
    ""                when s-addonamt[1]    = 0   @   s-addonamt[1]
    ""                when s-addonamt[2]    = 0   @   s-addonamt[2]
    ""                when s-addonamt[3]    = 0   @   s-addonamt[3]
    ""                when s-addonamt[4]    = 0   @   s-addonamt[4]
    k-addonamt[1]     when sasoo.oepricefl <> "n" and
                           s-addonamt[1]   <> 0   @ s-addonamt[1]
    k-addonamt[2]     when sasoo.oepricefl <> "n" and
                           s-addonamt[2]   <> 0   @ s-addonamt[2]
    k-addonamt[3]     when sasoo.oepricefl <> "n" and
                           s-addonamt[3]   <> 0   @ s-addonamt[3]
    k-addonamt[4]     when sasoo.oepricefl <> "n" and
                           s-addonamt[4]   <> 0   @ s-addonamt[4]     
    k-wodiscamt       when sasoo.oepricefl <> "n" @ oeeh.wodiscamt
    k-specdiscamt     when sasoo.oepricefl <> "n" @ oeeh.specdiscamt
    k-termsdiscamt    when sasoo.oepricefl <> "n" @ s-termsdiscamt
    oeeh.termspct     when sasoo.oepricefl <> "n"
    k-salestax        when sasoo.oepricefl <> "n" @ s-salestax 
    k-totlineord      when sasoo.oepricefl <> "n" @ s-totlineord 
    k-totlineamt      when sasoo.oepricefl <> "n" @ s-totlineamt 
    k-totinvamt       when sasoo.oepricefl <> "n" @ s-totinvamt
    k-payamt          when sasoo.oepricefl <> "n" @ s-payamt  
    s-cod             when sasoo.oepricefl <> "n"
    s-finthru
    oeeh.enterdt
