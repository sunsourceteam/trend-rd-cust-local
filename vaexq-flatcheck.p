define input parameter  ip-cono       like icsw.cono         no-undo.
define input parameter  ip-product    like icsw.prod         no-undo.
define input parameter  ip-proctype   as char                no-undo.

define output parameter ip-prefix     like icsw.prod         no-undo.
define output parameter ip-pricetype  like icsw.pricetype    no-undo.
define output parameter ip-model      as char format "x(40)" no-undo.
define output parameter ip-bypassfl   as logical             no-undo.
    
define var v-len                   as int              no-undo.
define var v-inx                   as int              no-undo.


define temp-table t-flatprice
  field type       as char
  field model      as char
  field prefix     as char
  field length     as integer
  field dprefix     as char
  field dlength     as integer
  field pricetype  like icsw.pricetype
  field prodcat    like icsp.prodcat
  field bypassfl   as logical

index ix1
  length descending
  prefix
index ix2
  length descending
  model
  prefix.


assign ip-prefix = " "
       ip-pricetype = " "
       ip-model  = " "
       ip-bypassfl = no.


for each notes where
         notes.cono         = ip-cono  and 
         notes.notestype    = "<pd"     no-lock:
  
  assign v-len = length(notes.noteln[4]).
  create t-flatprice.
  assign 
    t-flatprice.type   = "Detail"
    t-flatprice.model  = notes.primarykey
    t-flatprice.length = 24
    t-flatprice.prefix = notes.secondarykey
    t-flatprice.dlength = v-len
    t-flatprice.dprefix = notes.noteln[4]
    t-flatprice.prodcat = notes.noteln[1]
    t-flatprice.pricetype = notes.noteln[2]
    t-flatprice.bypassfl  = false.
end.


for each notes where
         notes.cono         = ip-cono  and 
         notes.notestype    = "<p"     no-lock:
  
  assign v-len = length(notes.secondarykey).
  create t-flatprice.
  assign 
    t-flatprice.type   = "prefix"
    t-flatprice.model  = notes.primarykey
    t-flatprice.length = v-len
    t-flatprice.prefix = notes.secondarykey
    t-flatprice.prodcat = notes.noteln[1]
    t-flatprice.pricetype = notes.noteln[2]    
    t-flatprice.bypassfl  = notes.xxl1.

end.
         

find first t-flatprice use-index ix1 where 
  ip-product begins t-flatprice.prefix no-lock no-error.

if avail t-flatprice then do:

  if t-flatprice.pricetype = "PROD" and t-flatprice.type = "prefix" then do:
    find icsp where 
         icsp.cono = ip-cono and
         icsp.prod = ip-product no-lock no-error.
    if avail icsp and icsp.prodcat <> t-flatprice.prodcat then
      return. /* not in right pcat */  

         
  end.

  
  assign          ip-prefix    = t-flatprice.prefix
                  ip-pricetype = t-flatprice.pricetype
                  ip-model     = t-flatprice.model
                  ip-bypassfl  = t-flatprice.bypassfl.   
                      
  if t-flatprice.pricetype = "PROD" and t-flatprice.type = "prefix"
    and ip-proctype  <> "E" then do:
    run vaexq-presel.p (input  ip-cono,
                        input  ip-model,
                        input  ip-product,
                        output ip-prefix,
                        output ip-pricetype ).
  end.
end.  


