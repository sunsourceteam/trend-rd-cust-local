/* p-oeizl2.i 1.1 01/03/98 */
/* p-oeizl2.i 1.2 6/12/92 */
/*h*****************************************************************************
  SX 55
  INCLUDE      : p-oeizl2.i
  DESCRIPTION  : Sets globals for other inquiries
  USED ONCE?   : no
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
   04/28/95 smb; TB# 11811 Changed the substring for the line to 13,3 from 12,3
*******************************************************************************/
assign  g-orderno  = integer(substring(o-frame-value,1,8))
        g-ordersuf = integer(substring(o-frame-value,10,2))
        g-oelineno = integer(substring(o-frame-value,14,3)).

if substring(o-frame-value,9,1) = "-" then        
  do:
  {w-oeel.i g-orderno g-ordersuf g-oelineno no-lock}
  {w-oeeh.i g-orderno g-ordersuf no-lock}

  assign v-xmode = "o".
/* TAH 082707 */
  
   if (avail oeeh and oeeh.stagecd <> 9) or not avail oeeh then
     assign v-ninesfl = false.
   else
     assign v-ninesfl = true.
/* TAH 082707 */


  
  end.
else
if substring(o-frame-value,9,1) = "c" then        
  do:
  find oeelb where oeelb.cono = g-cono and
                   oeelb.batchnm = string(g-orderno,">>>>>>>9") and
                   oeelb.seqno = 2 and 
                   oeelb.lineno = g-oelineno no-lock no-error.
  find oeehb where oeehb.cono = g-cono and
                   oeehb.batchnm = string(g-orderno,">>>>>>>9") and
                   oeehb.seqno = 2 and
                   oeehb.sourcepros = "ComQu" no-lock no-error.
   assign v-xmode = "c".
/* TAH 082707 */
   if (avail oeehb and oeehb.stagecd <> 9) or not avail oeehb then
     assign v-ninesfl = false.
   else
     assign v-ninesfl = true.
/* TAH 082707 */


  end.
                  