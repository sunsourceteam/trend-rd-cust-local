/* p-zsdipck.i  */
/*  **************************************************************************
 
   Pick Pack Detail Creation 

   Created:        06/27/07
   Author:         Tah
   Modifications:

   Parameters the same as d-oeepp1.i to accomodate all modules:

       &returnfl   = "oeel.returnfl"
       &cubes      = "oeel.cubes"
       &weight     = "oeel.weight"
       &head       = "oeeh"
       &line       = "oeel"
       &section    = "oeeh"
       &ourproc    = "oeepp"
       &specnstype = "oeel.specnstype"
       &altwhse    = "oeel.altwhse"
       &vendno     = "oeel.vendno"
       &kitfl      = "x-false"
       &botype     = "oeel.botype"
       &orderdisp  = "oeeh.orderdisp"
       &prevqtyship= "oeel.prevqtyshp"
       &netamt     = "oeel.netamt"
       &prtpricefl = "oeeh.pickprtfl"
       &bono       = "oeel.bono"
       &orderno    = "oeel.orderno"
       &ordersuf   = "oeel.ordersuf"
       &custno     = "oeeh.custno"
       &corechgty  = "oeel.corechgty"
       &qtybo      = "~{oeepp1bo.las~}" 
       &ordertype  = "o"
       &prefix     = "order"
       &seqno      = "0"
       &stkqtyshp  = "oeel.stkqtyship"
       &nosnlots   = "oeel.nosnlots"
       &comtype    = ""oe""
       &comlineno  = "oeel.lineno"
       &shipto     = "oeeh.shipto"
       &twlstgedit = " and oeeh.stagecd < 3 "} 
 


Inits   Date      Description
-----   --------  ----------------------------------------------------------
dkt     02/29/2008 VA - didn't actually change - was already there
dkt     12/15/2008 Changed both finds to match (not eq RECEIVING)

*************************************************************************    */

if icsdAutoPck = "Y" and v-loopcnt = 1 and x-pckdetailfl and 
   ("{&line}" = "oeel" and s-qtyship > 0 or
     "{&line}" = "oeelk" and {&stkqtyshp} > 0 or 
    ("{&line}" <> "oeel"  and "{&line}" <> "oeelk" 
      and {&stkqtyshp} > 0)) then do:

   curroefillqty = 0.

/*
   if g-operinit = "dt01" then
   message "new message p-zsdipck" 
           icsdAutoXdock
           "cono="    g-cono
           "oper2="   g-operinit 
           "reportnm=" sapb.reportnm 
           "ordtype=" "{&ordertype}" 
           "ordno=" {&orderno} 
           "ordsuf=" {&ordersuf} 
           "lineno=" {&line}.lineno 
           "compseq=" "{&line}" {&seqno}
           "v-rpt"   v-reportnm
           .
*/  
/* INSPECT */  
  {w-icsw.i {&line}.shipprod
      "if {&altwhse} ne '' then {&altwhse} else {&line}.whse" no-lock "zx-"}
    
 
  
  
  if icsdAutoXdock = "y" and
    (not avail zx-icsw or 
      (avail zx-icsw and zx-icsw.wmrestrict <> "insp") ) then do:
/* INSPECT */
    for each oefill use-index k-order where 
              oefill.cono = g-cono and
              oefill.oper2     = g-operinit and
              oefill.reportnm  = sapb.reportnm and
              oefill.processty <> "" and
              oefill.ordertype = "{&ordertype}" and
              oefill.orderno   = {&orderno} and
              oefill.ordersuf  = {&ordersuf} and
              oefill.lineno    = {&line}.lineno and
              oefill.seqno = (if "{&line}" = "oeelk" then
                                   {&seqno}
                                 else
                                 if "{&line}" = "vaesl" then  /* dkt Aug2008 */
                                   0
                                 else
                                   0   )           and 
               oefill.compseqno = (if "{&line}" = "oeelk" then
                                   0 
                                 else
                                 if "{&line}" = "vaesl" then  /* dkt Aug2008 */
                                   {&seqno}
                                 else
                                   0) : 

       
       /*
       if g-operinit = "dt01" then do:
         message "p-zsdipck.i " oefill.prod oefill.orderno oefill.reportnm.
       end.
       */
       
       run updateWmsbp.p(input p-whse, "RECEIVING", oefill.qtyalloc,
                         oefill.prod, 500 + g-cono, "RCV").

       curroefillqty = curroefillqty + oefill.qtyalloc.
    end.
  /* now can delete OEFILL or move up if not using accum */

  end. /* if icsdAutoXdock = "y" */
  {w-icsw.i {&line}.shipprod
      "if {&altwhse} ne '' then {&altwhse} else {&line}.whse" no-lock "zx-"}
  {w-icsp.i {&line}.shipprod no-lock "zx-"}


  if avail zx-icsp and zx-icsp.statustype = "L" then do:
    assign x-pckdetailfl = x-pckdetailfl. 
  end.
  else do:
  
    
    create zsdipckdtl.

    if avail zx-icsp then do:
      if avail zx-icsw then do:
        assign
          zsdipckdtl.binloc1       =  if zx-icsw.binloc1 = "new part" then
                                         " "
                                      else
                                        zx-icsw.binloc1 
          zsdipckdtl.binloc2       =  if zx-icsw.binloc2 = "new part" then
                                         " "
                                      else
                                        zx-icsw.binloc2. 
      end.
    end.
    else do:
      /* Non Stock? */
      find first wmsbp where 
               wmsbp.cono = g-cono + 500 and
               wmsbp.whse = {&line}.whse and 
               wmsbp.prod = {&line}.shipprod and 
               wmsbp.binloc ne "RECEIVING" no-lock no-error. 
               /* dkt 12/15/2008 */
      if avail wmsbp then do:
        assign zsdipckdtl.binloc1     =  if wmsbp.binloc = "new part" then
                                           " "
                                         else
                                           wmsbp.binloc. 
        find next wmsbp where 
                wmsbp.cono = g-cono + 500 and
                wmsbp.whse = {&line}.whse and 
                wmsbp.prod = {&line}.shipprod and
                wmsbp.binloc ne "RECEIVING" no-lock no-error.
                /* dkt Sept 2008 */
        if avail wmsbp then 
          assign zsdipckdtl.binloc2     =  if wmsbp.binloc = "new part" then
                                             " "
                                           else
                                             wmsbp.binloc. 
      end.  
    end.

    
    for each   wmsbp where 
               wmsbp.cono = g-cono + 500 and
               wmsbp.whse = {&line}.whse and 
               wmsbp.prod = {&line}.shipprod no-lock:
      find first wmsb where 
                 wmsb.cono = wmsbp.cono and
                 wmsb.whse = wmsbp.whse and 
                 wmsb.binloc = wmsbp.binloc no-lock no-error.
      if avail wmsb then do:
        if wmsb.priority = 9 then    
          zsdipckdtl.zzbinfl      = yes.
      end.  
    end.
  
    find first wmsb where 
               wmsb.cono = g-cono + 500 and
               wmsb.whse = {&line}.whse and 
               wmsb.binloc = zsdipckdtl.binloc1 no-lock no-error.
    if avail wmsb then
      assign    zsdipckdtl.locationid     = wmsb.building.
  
    /* dkt */
   
    /**
   &if not defined(oelkordtype) /* = 2 */ &then
    def var oelkordtype as char no-undo.
   &endif
    if avail oeelk then oelkordtype = oeelk.ordertype.
    else oelkordtype = "o".
    **/
    
    assign
       zsdipckdtl.cono           = g-cono
       zsdipckdtl.ordertype      = if "{&line}" = "oeel" then
                                     "o"
                                   else
                                   if "{&line}" = "oeelk" and
                                      "{&ordertype}" <> "W" then
                                     "o"  /* oeelk.ordertype  */
                                   else
                                   if "{&line}" = "wtel" then
                                     "t"
                                   else  
                                   if "{&line}" begins "va" then
                                     "f"
                                   else
                                   if "{&line}" begins "kp" or
                                      "{&ordertype}" = "W" then
                                     "w"
                                   else
                                     "o"
      
      
       zsdipckdtl.orderno        =  {&orderno} 
       zsdipckdtl.ordersuf       =  {&ordersuf} 
       zsdipckdtl.lineno         =  if "{&line}" begins "va" then
                                      {&seqno}
                                    else  
                                      {&line}.lineno
       zsdipckdtl.seqno          =  if "{&line}" begins "va" then
                                      {&line}.lineno
                                    else
                                      {&seqno}
       zsdipckdtl.whse           =  {&line}.whse
                                      
       zsdipckdtl.pickid         =  b-zsdipckid.pickid 
       zsdipckdtl.pickseq        =  b-zsdipcksq.seqid
                                     
       zsdipckdtl.shipprod       =  {&line}.shipprod   
       zsdipckdtl.stkqtyord      =  {&line}.stkqtyord   
       
       zsdipckdtl.stkqtyship     =  (if "{&line}" = "oeel" then
                                       s-qtyship
                                     else if "{&line}" = "oeelk" then
                                       {&stkqtyshp}
                                     else
                                       {&stkqtyshp})
       
       zsdipckdtl.stkqtybo       =  (if "{&line}" = "oeel" then
                                       s-qtybo
                                     else if "{&line}" = "oeelk" then
                                      x-qtybo
                                    else
                                      0)
       
       zsdipckdtl.stkqtypicked   =  (if "{&line}" = "oeel" then
                                       s-qtyship
                                    else if "{&line}" = "oeelk" then
                                      {&stkqtyshp}
                                    else
                                      {&stkqtyshp})
       
       zsdipckdtl.realqtypicked  = 0
       /*
       zsdipckdtl.locationid     = ""
       zsdipckdtl.binloc1        = ""
       zsdipckdtl.binloc2        = ""
       zsdipckdtl.zzbinfl        = no
       */
       zsdipckdtl.Pickbin        = ""
       zsdipckdtl.extype         = ""
       zsdipckdtl.exstatustype   = ""
       zsdipckdtl.statustype     = "a"
       zsdipckdtl.completefl     = no
       zsdipckdtl.transproc      = g-ourproc
       zsdipckdtl.recordtype     = if "{&line}" = "oeelk" then
                                     "C"
                                   else if x-false = true then
                                     "K"
                                   else
                                     " ".
       if zsdipckdtl.recordtype = "K" then
           zsdipckdtl.completefl = true.
       {t-all.i zsdipckdtl}
       
       /* dkt */
       if curroefillqty > 0 then do:
         if zsdipckdtl.stkqtypicked <= curroefillqty and
            zsdipckdtl.stkqtypicked <> 0 then do:
           assign zsdipckdtl.binloc2 = zsdipckdtl.binloc1
                  zsdipckdtl.binloc1 = "RECEIVING"
                  zsdipckdtl.locationid = " ".
    
           /* dkt Sept 2008 */
           find oefill use-index k-order where oefill.cono = g-cono and
              oefill.oper2     = g-operinit and
              oefill.reportnm  = sapb.reportnm and
              oefill.processty <> "" and
              oefill.ordertype = "{&ordertype}" and
              oefill.orderno   = {&orderno} and
              oefill.ordersuf  = {&ordersuf} and
              oefill.lineno    = {&line}.lineno and
              oefill.compseqno = {&seqno}
           no-error. 

           if avail oefill then do:
            find poelo use-index k-order where poelo.cono = oefill.cono and
              poelo.ordertype = oefill.ordertype and
              poelo.orderaltno = oefill.orderno and
              poelo.orderaltsuf = oefill.ordersuf and
              poelo.linealtno  = oefill.lineno and
              poelo.seqaltno  = oefill.compseqno no-lock no-error.
              
            if avail poelo then do:
              
              find first icer where 
              icer.pono = poelo.pono and
              icer.posuf = poelo.posuf and
              icer.lineno = poelo.lineno and
              icer.cono = poelo.cono and
              icer.whse =    {&line}.whse  and     
              icer.processinit = g-operinit     and
              icer.prod =    {&line}.shipprod /*  and    
              icer.ordertype =  "a"    */
               /**
                                 if "{&line}" = "oeel" then
                                     "o"
                                   else
                                   if "{&line}" = "oeelk" and
                                      "{&ordertype}" <> "W" then
                                     "o"  /* oeelk.ordertype  */
                                   else
                                   if "{&line}" = "wtel" then
                                     "t"
                                   else  
                                   if "{&line}" begins "va" then
                                     "f"
                                   else
                                   if "{&line}" begins "kp" or
                                      "{&ordertype}" = "W" then
                                     "w"
                                   else
                                     "o"
          **/
/*
                                     and
                                     icer.pono = poelo.pono and
                                     icer.posuf = poelo.posuf and
                                     icer.lineno = poelo.lineno.
*/
 share-lock no-error.
           
           /*
           if avail icer then do:
          
             /*
             if g-operinit = "dt01" then 
               message "p-zsdipck.i" icer.qtyrcvd icer.qtyalloc icer.qtypicked.
             */
             
              /*
               if "{&line}" = "vaesl" then do:
                 /* however it does it for oe and wt
                   except they have .prevqtyshp - no i in ship! */
                 icer.qtypicked = icer.qtypicked 
                   + {&line}.stkqtyship - {&line}.prevqtyship.
                   
               end.
               */
           
           /**    
           if icer.qtyrcvd > icer.qtypicked then
             /* if have some to pick or put away */
             if icer.qtyalloc > icer.qtypicked then
               /* and more allocated, so more should be picked */
               icer.qtypicked = icer.qtypicked + 
                 icer.qtyalloc - icer.qtypicked.
           **/
             
            /*
             if g-operinit = "dt01" then 
               message "p-zsdipck.i2" icer.qtyrcvd icer.qtyalloc icer.qtypicked.
            */ 
           end. /* av icer */  */
          end. /* poelo */
         end. /* oefill */
           
           
/**
           if oefill.qtypicked < zsdipckdtl.stkqtypicked then do:
             if g-operinit = "dt01" then do:
               message "p-zsdipck.i" oefill.orderno oefill.ordersuf oefill.compseqno oefill.lineno oefill.prod oefill.processty oefill.qtypicked.
             end.
             
             oefill.qtypicked = zsdipckdtl.stkqtypicked.
         
           end.
**/
         end.
         else if zsdipckdtl.stkqtypicked > curroefillqty then do:

  /*   make one for qty curroefillqty and this is for qty - curroefill      */ 
  
           buffer-copy zsdipckdtl
              except
                zsdipckdtl.locationid
                zsdipckdtl.user2
                zsdipckdtl.binloc1                      
                zsdipckdtl.binloc2             
                zsdipckdtl.stkqtyord
                zsdipckdtl.stkqtyship
                zsdipckdtl.stkqtybo
                zsdipckdtl.stkqtypicked
              to bf-zsdipckdtl
              assign
                bf-zsdipckdtl.locationid =  " "
                bf-zsdipckdtl.user1   = "Copy to receiving"
                bf-zsdipckdtl.user2   =  ""
                bf-zsdipckdtl.binloc1 = "RECEIVING"
                bf-zsdipckdtl.binloc2 = zsdipckdtl.binloc1                      
                bf-zsdipckdtl.stkqtyord  = 0
                bf-zsdipckdtl.stkqtyship = 0
                bf-zsdipckdtl.stkqtybo   = 0
                bf-zsdipckdtl.stkqtypicked = curroefillqty.
                
           assign zsdipckdtl.stkqtypicked = zsdipckdtl.stkqtypicked - 
                  curroefillqty   
                  zsdipckdtl.user1   = "Copy of original receiving".
         end.
       end.
  end. /* else not labor */    
end.   /* icsdAutoPck = "Y" */ 
