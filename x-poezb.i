/*******************************************************************************
  PROCEDURE      : x-poezb.i
  DESCRIPTION    : PO Receiving interface to build poehb & poelb records,
                   for non EDI po's. If a shipping notice(856) is not sent 
                   F6 will build screen. F7 for no EDI types. POEI batch 
                   receiving can then be run by whse by poehb.openinit.
                   F9 will facilitate batch receiving like a poei(whse) run.
  AUTHOR         : Sunsource
  DATE WRITTEN   : 02/25/04
  CHANGES MADE   :
*******************************************************************************/

/{&bXX_title}*/
 b-xx:title in frame f-xx = v-titlelit.
/{&bXX_title}*/

/{&temptable}*/
define new shared temp-table srch no-undo 
    field pono     as char format "x(10)" 
    field prod     like poel.shipprod
    field vprod    like poel.shipprod
    field lineno   like poel.lineno format ">>>"
    field ctype    as character format "x(1)"
    field srecid   as recid
    field type     as char format "x"
    field qtyord   as dec  format ">>>>>9.99"
    field qtyrcv   as dec  format ">>>>>9.99"
    field shipid   like poehb.shipmentid
    field expshpdt like poel.reqshipdt
    index k-srch1
          prod
          expshpdt
          qtyord descending
          lineno ascending
    index k-srch2
          vprod 
          expshpdt
          qtyord descending
          lineno ascending.
/{&temptable}*/

/{&user_vars}*/
def var v-clearl              as l  format "yes/no"   no-undo.
def new shared var zx-brblnbr like poeh.posuf         no-undo.
def var bl-suff               like poeh.posuf         no-undo. 
def var zx-pono               as char format "x(8)"   no-undo.
def var alloc-qty1            like poelb.qtyrcv no-undo.    
def var alloc-qty2            like poelb.qtyrcv no-undo.    
def var alloc-vendno          like apsv.vendno  no-undo.

define new shared temp-table blbr no-undo 
    field pono     as char format "x(10)" 
    field posuf    as char format "xx"
    field srecid   as recid
    field type     as char format "x"
    field brbono   like poeh.brbono
    field borelfl  like poeh.borelfl
    field expshpdt like poel.reqshipdt
    field ldfl     as char format "x"
    index k-blbr1
          posuf.

define query q-blbr for blbr scrolling.
define browse b-blbr query q-blbr
       display blbr.pono  
               blbr.brbono
               blbr.borelfl
               blbr.expshpdt
               blbr.ldfl 
          with 9 down centered overlay no-hide no-labels
          title "    Po No   BoNo Rel ExptShip Ld ". 
define frame f-blbr
  b-blbr at row 1 col 27
  with width 60 row 6 overlay no-labels no-box.   

form 
 "Are You Sure you"         at 1
 "want to leave ?"          at 1
 "Po's with no received"    at 1
 "Qty will be cleared"      at 1
 "from your work buffer."   at 1                     
 v-clearl                   at 8
with frame zx-ql row 8 centered overlay no-labels.

def frame f-zpoinfo
    "Name" lu-name format "x(21)"  
    "VendNo" at 1 lu-vendnoc lu-stagecd 
    "Buyer"  at 1 lu-buyer "ExpShp" lu-expshipdt
    "Id"     at 1 lu-shipid
    with width 80 row 17 col 28 no-box overlay no-labels no-hide.      

define query q-xx for sid scrolling.
define browse b-xx query q-xx
       display sid.pono  
               sid.openinit
               sid.whse
               sid.marked
          with 9 down centered overlay no-hide no-labels
            title v-titlelit. 
define frame f-xx
  b-xx at row 1 col 27
  with width 80 row 6 overlay no-labels no-box.   

on value-changed of b-xx in frame f-xx do: 
  run bottom_vend(input dec(substring(sid.pono,1,7)), 
                  input dec(substring(sid.pono,9,2)),
                  input sid.shipid).     
end.

define query q-yy for sid scrolling.
define browse b-yy query q-yy
       display sid.shipid  
          with 9 down centered overlay no-hide no-labels
            title "ShipmentId List". 

form
  b-yy at row 1 col 27
  with frame f-yy width 80 row 6 overlay no-labels no-box.   

on value-changed of b-yy in frame f-yy do: 
  run bottom_vend(input dec(substring(sid.pono,1,7)), 
                  input dec(substring(sid.pono,9,2)),
                  input sid.shipid).     
end.

define query q-ll for plist scrolling.
define browse b-ll query q-ll
       display plist.pono  
               plist.whse
               plist.openinit
               plist.marked
          with 9 down centered overlay no-hide no-labels
            title "Po Updt List". 

form
  b-ll at row 1 col 27
  with frame f-ll width 80 row 6 overlay no-labels no-box.   

def frame f-zpoinfo2
    "Pono"   at 10 lu-pono format "x(10)" "LineNo" lu-lineno 
    "Buyer"  lu-buyer lu-stagecd
    with width 63 row 18 col 26 overlay no-labels centered.      

form  
  srch.ctype    at 1
  srch.lineno   at 3
  srch.prod     at 7 
  srch.qtyord   at 32
  srch.qtyrcv   at 42
  srch.expshpdt at 53
  with frame f-zzx width 63 row 6     
   scroll 1  centered
   no-hide no-labels overlay v-length down   
          title
      "   Ln    Product                QtyOrd    QtyRcv  ExpShipDt  ". 

form 
   "Vendor:"     at 01
   tpoehb.vendno at 08
   tpoehb.vname  at 21
   " Whse:"      at 63
   tpoehb.whse   at 70
   v-msg         at 1 
   v-msg2        at 1 
   with frame f-poehb-hdr width 80 row 1 centered no-hide 
   no-labels overlay.   

/* Append */

form
"Append Po's ?:"     at 1
v-Accum              at 16
"Pick Printer?:"     at 1
s-pckprinter         at 16
{f-help.i}
"Lbl  Printer?:"     at 1 
s-ibprinter          at 16 
{f-help.i}
"Default Whse: "    at 1  
v-saso-whse         at 16
with frame f-opts width 28 row 8 column 27 no-hide 
   no-labels overlay  title  "Options".   

form
"Append Po's ?:"     at 1
v-Accum              at 16
"Cannot put more than 1" at 1
"PO in buffer. To do so" at 1
"choose Append PO's-YES" at 1

with frame f-accum width 28 row 8 column 27 no-hide 
   no-labels overlay  title  "Append Error".   

form
  srch.prod at 1
  a-qtyallo at row 2 col 8
  with frame f-alloc width 26 row 8 centered overlay no-labels
  title "Product qty to allocate?".   

/* Append */

/*d Set title for line frame, and number of iterations in line frame */
assign
/*
    v-msg   =
"Line   Item                    Unit  QtyOrd PrtLbl  BinLoc       QtyRcv SL/Tie"
*/
    v-msg   =
"Ln Item                     Unit QtyOrd   BinLoc        QtyRcv QtyLbl    SL/Tie"
    v-msg2  = 
"          Description           Vendor/Product          SType  PoNumber Can Ty"
    v-length =   7
    v-scrollfl = yes.

form 
  tpoehb.comfl    at 1
  tpoehb.lineno   format ">9"    at 2 
  tpoehb.prod     format "x(24)" at 5  /* was 6 */
  tpoehb.unit     format "x(4)"  at 30 /* was 33 */
  tpoehb.qtyord   format ">>>>9.99" at 35 /* was 38 */
/*   tpoehb.flglbl   at 49 */
  tpoehb.binloc   format "xx/xx/xxx/xxx" at 44 /* was at 51 */
  tpoehb.qtyrcv   format ">>>>>9.99"  at 59 /* was at 65 */
  qtyLabel        format ">>9"       at 70
  tpoehb.cancelfl at 76
  tpoehb.slotty   at 78
  tpoehb.botyp    at 79
  tpoehb.descrip  at 6 
  tpoehb.vprod    at 34  
  tpoehb.statustype at 59
  tpoehb.pono     at 64 format ">>>>>>9" 
  "-"             at 71
  tpoehb.posuf    at 72 
  tpoehb.notesfl1 at 74
  tpoehb.potyp    at 76
  with frame f-poehb row 6  scroll 1 no-box
  no-hide no-labels overlay v-length down.   

form 
  "Start:"  sr-prod  
  with frame f-lookprod width 34 row 7 centered
  overlay title "Product PoList" no-labels.   

form 
  "Start at: "  v-start format "x(24)"  
  with frame f-lookprod2 width 40 row 7 centered
  overlay title "Product PoList" no-labels.   

form 
  " Po No: " at 2
  v-pono     at 10
/*  "Labels:"  at 2
  v-printLblFlag at 10 */
  with frame l-f7 width 22 row 8 centered no-labels overlay
  title " Load Po No".

form 
  "Shipment Id : " at 2
  v-shipid         at 2 
  with frame l-f6 width 28 row 8 centered no-labels overlay
  title " Load Shipment Id ".
/{&user_vars}*/

/{&B-zz_browse_events}*/
/{&B-zz_browse_events}*/

/{&user_popup_proc}*/
  assign zx-brblnbr = 00.
  if poeh.transtype = "bl" or poeh.transtype = "br" then do:
     hide frame f-poehb.
     for each blbr exclusive-lock:
       delete blbr.
     end.
     define buffer br-poeh for poeh.
     define buffer br-poel for poel.
     for each br-poeh where 
              br-poeh.cono  = g-cono and 
              br-poeh.pono  = g-pono and 
              br-poeh.posuf > 0      and 
             (br-poeh.stagecd = 1 or
              br-poeh.stagecd = 2 or
              br-poeh.stagecd = 3)
              no-lock: 
         find first br-poel of br-poeh no-lock no-error.
         create blbr.
         if can-find (first poehb where 
                            poehb.cono  = (g-cono + 500) and 
                            poehb.pono  =  g-pono and
                            poehb.posuf =  br-poeh.posuf) then
             assign blbr.ldfl = "L". 
         else      
             assign blbr.ldfl = " ".
         assign blbr.pono     = string(br-poeh.pono,">>>>>>9") + "-" + 
                                string(br-poeh.posuf,"99")
                blbr.posuf    = string(br-poeh.posuf,"99")
                blbr.srecid   = recid(br-poeh)
                blbr.expshpdt = if avail br-poel then 
                                   br-poel.expshipdt
                                else    
                                   br-poeh.expshipdt 
                blbr.borelfl  = br-poeh.borelfl
                blbr.brbono   = br-poeh.brbono.
     end.         
  find first blbr no-lock no-error.
  if not avail blbr then 
  do: 
    message "Valid Blanket Release Records Notfound " + string(v-pono).
    pause.
    leave.
  end.
  open query q-blbr for each blbr.
  enable  b-blbr with frame f-blbr.
  display b-blbr with frame f-blbr.
  on cursor-up cursor-up.
  on cursor-down cursor-down.
  wait-for window-close of current-window. 
  on cursor-up back-tab.
  on cursor-down tab.
  close query q-blbr.
 end. 
/{&user_popup_proc}*/

/{&user_display1}*/
    color display input v-msg v-msg2 with frame f-poehb-hdr.     
    display v-msg v-msg2 with frame f-poehb-hdr.                   
/{&user_display1}*/

/{&user_display2}*/
if toggle-sw = no then 
  put screen row 21 column 1 color messages
"  F6-Options     F7-Load Type    F8-VendProd Toggle     F9-PoList   F10-Update  ".
else 
  put screen row 21 column 1 color messages
"  F6-Options     F7-Load Type    F8-VndProd Toggle(on)  F9-PoList   F10-Update  ".
/{&user_display2}*/

/{&user_Query}*/ 
  /*o Process XXEX screen */  
  {x-xxexcustom3.i &file         = "tpoehb"              
                   &frame        = "f-poehb"             
                   &find         = "poehb.lfi"              
                   &snchoosecond = "and not v-searchfl"
                   &field        = "tpoehb.lineno"
                   &internalfield  = "lineno"
                   &sneditsecond = "or v-searchfl"
                   &keys         = "keys v-key"
                   &searcher     = "poehb.sch"
                   &searchat     = "at 1"
                   &go-on        = "ctrl-o"    
                   &usefile      = "poehb.lfu" 
                   &display      = "poehb.lds"            
                   &delete       = "poehb.del"
                   &delfl        = "*" 
                   &keymove      = "poehb.key"
                   &edit         = "poehb.led"
                   
                   &gofile       = "poehb.gof"
                   }
/{&user_Query}*/ 

/{&user_aftmain1}*/ 
/{&user_aftmain1}*/ 

/{&user_procedures}*/ 

procedure blanket-suffix:
do:
   define buffer bl-poeh for poeh. 
   define buffer bo-poeh for poeh. 
   define buffer br-poeh for poeh. 
   assign bl-suff = 00.
   bl-loop:
   for each bl-poeh where 
            bl-poeh.cono  = g-cono and 
            bl-poeh.pono  = v-pono and 
            bl-poeh.posuf = zx-brblnbr
            no-lock: 
     if bl-poeh.stagecd <= 2 and
        bl-poeh.stagecd > 0 then
     do:
       assign bl-suff  = bl-poeh.posuf
              bl-recid = recid(bl-poeh). 
       leave bl-loop.
     end.  
     for each bo-poeh where 
              bo-poeh.cono   = g-cono and 
              bo-poeh.pono   = v-pono and 
              bo-poeh.brbono = bl-poeh.posuf 
              no-lock: 
        if bo-poeh.stagecd <= 3 and
           bo-poeh.stagecd > 0 then 
         do:
            assign bl-suff  = bo-poeh.posuf
                   bl-recid = recid(bo-poeh). 
            return.
         end.  
        else 
         do:
            assign zx-brblnbr = bo-poeh.posuf. 
            next bl-loop.
         end.   
     end. /* inner for each */
   end. /* outer for each */ 
   leave.       
end. /*do*/
end procedure.

procedure bottom_vend:
def input parameter in-pono   like poeh.pono.
def input parameter in-posuf  like poeh.posuf.
def input parameter in-shipid like poehb.shipmentid.

do: 
   find first poeh where
              poeh.cono  = g-cono   and 
              poeh.pono  = in-pono  and 
              poeh.posuf = in-posuf 
              no-lock no-error. 
   find first apsv where 
              apsv.cono   = g-cono and 
              apsv.vendno = poeh.vendno
              no-lock no-error. 
   if avail poeh then 
    do:       
       assign lu-name      = apsv.name
              lu-shipid    = in-shipid
              lu-vendno    = poeh.vendno 
              lu-stagecd   = if poeh.stagecd = 0 then 
                              "("  + "Ent" + ")"
                             else 
                              "(" +  lu-stagecd2[poeh.stagecd] + ")"
              lu-buyer     = poeh.buyer
              lu-transtype = poeh.transtype
              lu-ordaltno  = poeh.orderaltno 
              lu-ordaltsuf = poeh.orderaltsuf
              lu-expshipdt = poeh.expshipdt
              lu-vendnoc   = string(lu-vendno,">>>>>>999999")
              lu-vendnoc   = trim(lu-vendnoc," ").
       display lu-name      
               lu-vendnoc
               lu-stagecd
               lu-buyer 
               lu-expshipdt
               lu-shipid
               with frame f-zpoinfo. 
    end.
end.           
end procedure.           

procedure bottom_srch:
def input parameter in-pono   like poeh.pono.
def input parameter in-posuf  like poeh.posuf.
def buffer s-tpoehb for tpoehb.
do: 
   hide frame f-poehb.
   find tpoehb where recid(tpoehb) = srch.srecid
        no-lock no-error.
   find first poeh where
              poeh.cono  = g-cono   and 
              poeh.pono  = in-pono  and 
              poeh.posuf = in-posuf 
              no-lock no-error. 
   find first apsv where 
              apsv.cono   = g-cono and 
              apsv.vendno = poeh.vendno
              no-lock no-error. 
   if avail poeh then 
    do:           
       assign lu-pono      = string(tpoehb.pono,">>>>>>9") + "-"  +          
                             string(tpoehb.posuf,"99")
              lu-lineno    = tpoehb.lineno
              lu-qtyrcv    = tpoehb.qtyrcv
              lu-qtyord    = tpoehb.qtyord
              lu-name      = apsv.name
              lu-vendno    = poeh.vendno 
              lu-stagecd   = if poeh.stagecd = 0 then 
                              "("  + "Ent" + ")"
                             else 
                              "(" +  lu-stagecd2[poeh.stagecd] + ")"
              lu-buyer     = poeh.buyer
              lu-transtype = poeh.transtype
              lu-ordaltno  = poeh.orderaltno 
              lu-ordaltsuf = poeh.orderaltsuf
              lu-expshipdt = tpoehb.expshpdt
              lu-vendnoc   = string(lu-vendno,">>>>>>999999")
              lu-vendnoc   = trim(lu-vendnoc," ").
       display lu-pono      
               lu-lineno
               lu-stagecd
               lu-buyer 
               with frame f-zpoinfo2. 
    end.
end.           
end procedure.           

procedure alloc-errchk: 
do: 
   def buffer y-srch for srch.
   def buffer y-tpoehb for tpoehb.
   assign alloc-qty1 = 0
          alloc-qty2 = 0.
   for each y-srch where 
            y-srch.prod = a-hldprod 
            no-lock
            break by y-srch.prod:
     find y-tpoehb where recid(y-tpoehb)  = y-srch.srecid 
          no-lock no-error.
     if first-of(y-srch.prod) then 
        assign alloc-vendno = y-tpoehb.vendno.
     if alloc-vendno ne y-tpoehb.vendno then 
        assign alloc-qty2 = alloc-qty2 + 1. 
     assign alloc-qty1 = alloc-qty1 + (y-srch.qtyord - y-srch.qtyrcv).
   end. /* end for each */
end.
end procedure.

procedure allocate-prep: 
do: 
def buffer a-srch   for srch.
def buffer z-srch   for srch.
def buffer a-poelb  for poelb.
def buffer a-poehb  for poehb.
def buffer a-tpoehb for tpoehb.
                                    
for each z-srch where 
         z-srch.prod = a-hldprod no-lock:
    assign a-hlddt = z-srch.expshpdt.
for each a-srch where
         a-srch.prod = z-srch.prod and 
         a-srch.expshpdt = a-hlddt  exclusive-lock
  break by a-srch.prod
        by a-srch.qtyord descending:
  if a-qtyallo <= 0 then 
     leave.
  assign ws-qtyallo = 0.
  find a-tpoehb where recid(a-tpoehb)  = a-srch.srecid 
       exclusive-lock no-error.
  find a-poelb where recid(a-poelb) = a-tpoehb.poelbid
       exclusive-lock no-error.
  if avail a-poelb then 
      find a-poehb where
           a-poehb.cono  = a-poelb.cono and 
           a-poehb.pono  = a-poelb.pono and  
           a-poehb.posuf = a-poelb.posuf
           exclusive-lock no-error.
  if substring(a-poelb.user5,1,6) = "inb856" and 
    (a-tpoehb.qtyord - a-poelb.qtyrcv) = 0   then 
     next.
  assign ws-qtyallo = (a-tpoehb.qtyord - a-poelb.qtyrcv).
  if avail a-tpoehb and avail a-poelb then  
  do: 
     if (a-qtyallo >= (a-tpoehb.qtyord - a-poelb.qtyrcv)) then 
     do:
        assign a-poelb.stkqtyrcv = ((a-poelb.qtyrcv + ws-qtyallo) * 
                                   {unitconv.gas &decconv = a-poelb.unitconv})
               a-poelb.qtyrcv    = a-poelb.qtyrcv  + ws-qtyallo
               a-tpoehb.qtyrcv   = a-tpoehb.qtyrcv + ws-qtyallo
               a-srch.qtyrcv     = a-srch.qtyrcv   + ws-qtyallo
               a-qtyallo         = a-qtyallo - ws-qtyallo
               v-holdline        = 1.
        assign a-srch.ctype = if substring(a-poelb.user5,1,6) = "inb856" and
                                 substring(a-poelb.user5,7,1) = "n" then
                                 "!"
                              else
                              if substring(a-poelb.user5,1,6) = "inb856" and
                                 substring(a-poelb.user5,7,1) = "y" then
                                 "#"
                              else
                              if substring(a-poelb.user5,1,6) <> "inb856" and
                                 a-poelb.qtyrcv <> 0 then
                                 "#"
                              else
                              if substring(a-poelb.user5,1,6) <> "inb856" and
                                 a-poelb.qtyrcv = 0 then
                                 " "
                              else
                                 " ".
        display a-srch.ctype  @ srch.ctype a-srch.qtyrcv @ srch.qtyrcv 
                with frame f-zzx.
        if substring(a-poelb.user5,1,6) = "inb856" then 
           assign substring(a-poelb.user5,7,1) = "y".
        if avail a-poehb then 
           assign a-poehb.updtfl = yes.
        release a-tpoehb.
        release a-srch.
        release a-poelb.
        release a-poehb.  
     end.
  end.
end. /* for each a-srch */
end. /* for each srch */
  leave.
end.
end procedure.

procedure allocate-qtys: 
do: 
def buffer a-srch   for srch.
def buffer a-poelb  for poelb.
def buffer a-poehb  for poehb.
def buffer a-tpoehb for tpoehb.

for each a-srch where
         a-srch.prod = a-hldprod exclusive-lock
  break by a-srch.prod
        by a-srch.expshpdt
        by a-srch.lineno:
  if a-qtyallo <= 0 then 
     leave.
  assign ws-qtyallo = 0.
  find a-tpoehb where recid(a-tpoehb)  = a-srch.srecid 
       exclusive-lock no-error.
  find a-poelb where recid(a-poelb) = a-tpoehb.poelbid
       exclusive-lock no-error.
  if avail a-poelb then 
      find a-poehb where
           a-poehb.cono  = a-poelb.cono and 
           a-poehb.pono  = a-poelb.pono and  
           a-poehb.posuf = a-poelb.posuf
           exclusive-lock no-error.
      
  if substring(a-poelb.user5,1,6) = "inb856" and 
    (a-tpoehb.qtyord - a-poelb.qtyrcv) = 0   then 
     next.
  assign ws-qtyallo = if (a-qtyallo >= (a-tpoehb.qtyord - a-poelb.qtyrcv)) then
                         (a-tpoehb.qtyord - a-poelb.qtyrcv)
                      else 
                         a-qtyallo.
  if avail a-tpoehb and avail a-poelb then 
  do: 
     if ((ws-qtyallo + a-poelb.qtyrcv) <= a-tpoehb.qtyord) then  
     do:
        assign a-poelb.stkqtyrcv = ((a-poelb.qtyrcv + ws-qtyallo) * 
                                   {unitconv.gas &decconv = a-poelb.unitconv})
               a-poelb.qtyrcv    = a-poelb.qtyrcv  + ws-qtyallo
               a-tpoehb.qtyrcv   = a-tpoehb.qtyrcv + ws-qtyallo
               a-srch.qtyrcv     = a-srch.qtyrcv   + ws-qtyallo
               a-qtyallo         = a-qtyallo - ws-qtyallo
               v-holdline        = 1.
        assign a-srch.ctype = if substring(a-poelb.user5,1,6) = "inb856" and
                                 substring(a-poelb.user5,7,1) = "n" then
                                 "!"
                              else
                              if substring(a-poelb.user5,1,6) = "inb856" and
                                 substring(a-poelb.user5,7,1) = "y" then
                                 "#"
                              else
                              if substring(a-poelb.user5,1,6) <> "inb856" and
                                 a-poelb.qtyrcv <> 0 then
                                 "#"
                              else
                              if substring(a-poelb.user5,1,6) <> "inb856" and
                                 a-poelb.qtyrcv = 0 then
                                 " "
                              else
                                 " ".
        display a-srch.ctype  @ srch.ctype a-srch.qtyrcv @ srch.qtyrcv 
                with frame f-zzx.
        if substring(a-poelb.user5,1,6) = "inb856" then 
           assign substring(a-poelb.user5,7,1) = "y".
        if avail a-poehb then 
           assign a-poehb.updtfl = yes.
        release a-tpoehb.
        release a-srch.
        release a-poelb.
        release a-poehb.  
     end.
  end.
end. /* for each srch */
  hide frame f-alloc.
end.
end procedure.

/{&user_procedures}*/ 

/{&user_ctrlproc}*/
on ctrl-o anywhere do:
  if frame-name = "f-poehb" then do:
    find tpoehb where recid(tpoehb) = v-recid[frame-line(f-poehb)]
      no-lock no-error.
    if avail tpoehb then do: 
      assign g-pono  = tpoehb.pono 
             g-posuf = tpoehb.posuf.
      run poio.p.
    end.
  end. 
end.
/{&user_ctrlproc}*/

/{&user_f9proc}*/
run List_POs.
readkey pause 0.
/{&user_f9proc}*/

/{&user_f10proc}*/
  v-okfl = yes.
  define buffer zb1-poehb for poehb.
  do transaction:
    find first zb1-poehb where 
               zb1-poehb.cono     = (g-cono + 500) and 
               zb1-poehb.openinit =  g-operinit    
               no-lock no-error. 
    if avail zb1-poehb then do:
      assign g-whse = zb1-poehb.whse.
      find last poeh where
                poeh.cono = g-cono and 
                poeh.pono = zb1-poehb.pono
                no-lock no-error.
    end.
    else
      assign g-whse  = v-saso-whse.
  end.

  run poebustk.p(input g-operinit, 
                 input v-okfl).
  
  release poeh.
  if substring(v-saso-whse,1,1) = "&" then
    run clear-brwse (x-f10-whse).
  else  
    run clear-brwse("all").
  clear frame f-poehb-hdr.
  color display input v-msg v-msg2 with frame f-poehb-hdr.     
  display v-msg v-msg2 with frame f-poehb-hdr.                   
  view frame f-poehb-hdr.
  if toggle-sw = no then 
  put screen row 21 column 1 color messages
"  F6-Options     F7-Load Type    F8-VendProd Toggle     F9-PoList   F10-Update
  ".
else 
  put screen row 21 column 1 color messages
"  F6-Options     F7-Load type    F8-VndProd Toggle(on)  F9-PoList   F10-Update
  ".
 
/{&user_f10proc}*/

/{&user_errormsg}*/
  run warning.p ({&errnbr}).
/{&user_errormsg}*/


/{&user_clearmsg}*/
  display  0 @ tpoehb.vendno  
         "" @ tpoehb.vname "" @ tpoehb.whse with frame f-poehb-hdr.
/{&user_clearmsg}*/

/{&user_headdisplay}*/
   display tpoehb.whse tpoehb.vendno tpoehb.vname
           with frame f-poehb-hdr. 
/{&user_headdisplay}*/

/{&user_reqnotes}*/
   if poeh.notesfl = "!" then
    do:
      run noted.p(yes,"x",string(poeh.pono),string(poeh.posuf)).
      readkey pause 0.
    end.
/{&user_reqnotes}*/

/{&b-zz_Browse}*/ 
 assign sc-recid = ?.
 run poezbsearch.p ( input-output v-f,
                     input-output sc-recid). 
 if sc-recid <> ? then
   find srch where recid(srch) = sc-recid no-lock no-error.
/{&b-zz_Browse}*/ 

/{&srch_Query}*/ 
v-length = 10.
v-scrollfl = yes.
main:
do while true on endkey undo main, leave main
              on error undo main,  leave main: 
  if {k-cancel.i} or {k-jump.i} then do:
    assign sc-recid = ?.
    leave main.
  end.
  
  /*o Process XXEX screen */  
  {x-xxexcustom3.i &file         = "srch"              
                   &status       = "If not v-bufferedsearch then"
                   &frame        = "f-zzx"             
                   &find         = "poehbzz.lfi"              
                   &snchoosecond = "and not v-searchfl" 
                   &field        = "srch.prod" 
                   &internalfield  = "prod" 
                   &go-on        = '" "'           
                   &sneditsecond = " or v-searchfl "
                   &searcher     = "poehbzz.sch"
                   &searchat     = "at 1"
                   &usefile      = "poehbzz.lfu" 
                   &display      = "poehbzz.lds"            
                   &delfl        = "*" 
                   &keymove      = "poehbzz.key"
                   &gofile       = "poehbzz.goon"
                   &edit         = "poehbzz.led"}          
end.
/{&srch_Query}*/ 
