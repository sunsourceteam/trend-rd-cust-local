
/*  ------------------------------------------------------------------------ */    Procedure Evaluate-Pipe-Change: 
/*  ------------------------------------------------------------------------ */   define input  parameter ip-pipematerial as char format "x(15)"  no-undo.
  define input  parameter ip-pipestyle    as char format "x(15)"  no-undo. 
  define output parameter ip-changed      as logical              no-undo.    

  define var iv-pipingStyle       as dec  format ">>>9.999"       no-undo.
  define var iv-pipingStylety     as logical                      no-undo.
  define var iv-pipingMaterial    as dec format ">>>9.999"        no-undo.
  define var iv-pipingMaterialty  as logical                      no-undo.
  define var iv-change            as logical                      no-undo.
  define var iv-newaddon          as dec format ">>>9.999"        no-undo.




  define buffer b-igo-zsdivaspmap for zsdivaspmap.  
  define buffer b-is-zsdivaspmap for zsdivaspmap.  
  define buffer b-il-zsdivaspmap for zsdivaspmap.  
  define buffer b-i-vaspsl       for vaspsl.  
  define buffer b-i-vasps        for vasps.  

  define buffer b-it-vaspmap     for t-vaspmap.  

  find last bl-t-lines use-index ix1 no-lock no-error.
  
  


  
  assign iv-change = false 
         ip-changed = false  /* before updating make sure a change occured */
         iv-newaddon = 0.
  find first b-igo-zsdivaspmap where                                       
             b-igo-zsdivaspmap.cono = g-cono and                                              b-igo-zsdivaspmap.rectype = "GO" and  /* Addon Global Options */ 
             b-igo-zsdivaspmap.prod    = x-quoteno and
             b-igo-zsdivaspmap.seqno   = 0 and
             b-igo-zsdivaspmap.lineno  = 0 and
             b-igo-zsdivaspmap.typename = "Piping Material" and
             b-igo-zsdivaspmap.description = ip-pipematerial and
             b-igo-zsdivaspmap.user1    =  "Yes" no-lock no-error.
  if avail b-igo-zsdivaspmap then do:
  end.
  else do:
    assign iv-change = true.

    find first b-igo-zsdivaspmap where                                       
               b-igo-zsdivaspmap.cono = g-cono and                             ~                 b-igo-zsdivaspmap.rectype = "GO" and  /* Addon Global Options ~*/ 
               b-igo-zsdivaspmap.prod    = x-quoteno and
               b-igo-zsdivaspmap.seqno   = 0 and
               b-igo-zsdivaspmap.lineno  = 0 and
               b-igo-zsdivaspmap.typename = "Piping Material" and
               b-igo-zsdivaspmap.user1  = "yes" no-error.

    if avail b-igo-zsdivaspmap then do:
      assign b-igo-zsdivaspmap.user1 = "No".

       find first b-igo-zsdivaspmap where                                       
                  b-igo-zsdivaspmap.cono = g-cono and                          ~                    b-igo-zsdivaspmap.rectype = "GO" and  
                  /* Addon Global Options */  
                  b-igo-zsdivaspmap.prod    = x-quoteno and
                  b-igo-zsdivaspmap.seqno   = 0 and
                  b-igo-zsdivaspmap.lineno  = 0 and
                  b-igo-zsdivaspmap.typename = "Piping Material" and
                  b-igo-zsdivaspmap.description = ip-pipematerial and
                  b-igo-zsdivaspmap.user1    =  "No"  no-error.
       if avail b-igo-zsdivaspmap then do:
         assign b-igo-zsdivaspmap.user1 = "Yes".
       end.  
    end.
  end.
 
  
  find first b-igo-zsdivaspmap where                                       
             b-igo-zsdivaspmap.cono = g-cono and                               ~             b-igo-zsdivaspmap.rectype = "GO" and  /* Addon Global Options */ 
             b-igo-zsdivaspmap.prod    = x-quoteno and
             b-igo-zsdivaspmap.seqno   = 0 and
             b-igo-zsdivaspmap.lineno  = 0 and
             b-igo-zsdivaspmap.typename = "Piping Style" and
             b-igo-zsdivaspmap.description = ip-pipestyle and

             b-igo-zsdivaspmap.user1    =  "Yes" no-lock no-error.
  if avail b-igo-zsdivaspmap then do:
  end.
  else do:
    assign iv-change = true.

    find first b-igo-zsdivaspmap where                                       
               b-igo-zsdivaspmap.cono = g-cono and                             ~               b-igo-zsdivaspmap.rectype = "GO" and  
                 /* Addon Global Options */ 
               b-igo-zsdivaspmap.prod    = x-quoteno and
               b-igo-zsdivaspmap.seqno   = 0 and
               b-igo-zsdivaspmap.lineno  = 0 and
               b-igo-zsdivaspmap.typename = "Piping Style" and
               b-igo-zsdivaspmap.user1  = "yes" no-error.

    if avail b-igo-zsdivaspmap then do:
      assign b-igo-zsdivaspmap.user1 = "No".

       find first b-igo-zsdivaspmap where                                       
                  b-igo-zsdivaspmap.cono = g-cono and                          ~                  b-igo-zsdivaspmap.rectype = "GO" and  
                  /* Addon Global Options */  
                  b-igo-zsdivaspmap.prod    = x-quoteno and
                  b-igo-zsdivaspmap.seqno   = 0 and
                  b-igo-zsdivaspmap.lineno  = 0 and
                  b-igo-zsdivaspmap.typename = "Piping Style" and
                  b-igo-zsdivaspmap.description = ip-pipestyle and
                  b-igo-zsdivaspmap.user1    =  "No"  no-error.
       if avail b-igo-zsdivaspmap then do:
         assign b-igo-zsdivaspmap.user1 = "Yes".
       end.  
    end.
  end.

  if iv-change = true then do:
  
    if avail bl-t-lines and bl-t-lines.lineno > 20 then
      message "Recalculating line item Addons.....".
    
    find first b-igo-zsdivaspmap where
         b-igo-zsdivaspmap.cono = g-cono and
         b-igo-zsdivaspmap.rectype = "GO" and     /* Addon Global Options */
         b-igo-zsdivaspmap.prod    = x-quoteno and
         b-igo-zsdivaspmap.seqno   = 0 and
         b-igo-zsdivaspmap.lineno  = 0 and
         b-igo-zsdivaspmap.typename = "Piping Material" and
         b-igo-zsdivaspmap.user1    = "yes"    no-lock no-error.
    if not avail b-igo-zsdivaspmap then
      assign iv-pipingmaterial = 1
             iv-pipingmaterialty = true.
    else  do:
      assign iv-pipingmaterial = b-igo-zsdivaspmap.addon[1]
             iv-pipingmaterialty = b-igo-zsdivaspmap.addonty.
    end.
    find first b-igo-zsdivaspmap where
         b-igo-zsdivaspmap.cono = g-cono and
         b-igo-zsdivaspmap.rectype = "GO" and     /* Addon Global Options */
         b-igo-zsdivaspmap.prod    = x-quoteno and
         b-igo-zsdivaspmap.seqno   = 0 and
         b-igo-zsdivaspmap.lineno  = 0 and
         b-igo-zsdivaspmap.typename = "Piping Style" and
         b-igo-zsdivaspmap.user1    = "yes"    no-lock no-error.
    if not avail b-igo-zsdivaspmap then
      assign iv-pipingStyle = 1
             iv-pipingStylety = true.
    else do:
      assign iv-pipingStyle = b-igo-zsdivaspmap.addon[1]
             iv-pipingStylety = b-igo-zsdivaspmap.addonty.
    end.         
  
    find b-is-zsdivaspmap where
         b-is-zsdivaspmap.cono = g-cono and
         b-is-zsdivaspmap.rectype = "S" and        /* VAES */
         b-is-zsdivaspmap.prod   = x-quoteno and
         b-is-zsdivaspmap.description = "ADDON" and
         b-is-zsdivaspmap.typename    = 
                  "Intangables" no-lock  no-error.
    if avail b-is-zsdivaspmap then do:
      find b-il-zsdivaspmap where
           b-il-zsdivaspmap.cono = g-cono and
           b-il-zsdivaspmap.rectype = "L" and        /* VAESL */
           b-il-zsdivaspmap.prod   = x-quoteno and
           b-il-zsdivaspmap.typename    = "Fittings"  no-error.
      if avail b-il-zsdivaspmap then do:
        if iv-pipingStylety then do:
          assign b-il-zsdivaspmap.addon[1] = 
                 iv-pipingstyle * iv-pipingmaterial
                 b-il-zsdivaspmap.addonty = iv-pipingstylety 
                iv-newaddon = 
                 iv-pipingstyle * iv-pipingmaterial
                 b-il-zsdivaspmap.addonty = iv-pipingstylety. 
        end.
        else do:    
          assign b-il-zsdivaspmap.addon[1] = 
                   iv-pipingstyle + iv-pipingmaterial
                   b-il-zsdivaspmap.addonty = iv-pipingstylety 
                 iv-newaddon = 
                   iv-pipingstyle + iv-pipingmaterial
                   b-il-zsdivaspmap.addonty = iv-pipingstylety. 
  
         end.
      end.
    end.        

  /* update the Temp table then the Database */
    
/* Locate Inventory section from Template */    
    find b-igo-zsdivaspmap where
         b-igo-zsdivaspmap.cono = g-cono and
         b-igo-zsdivaspmap.rectype  = "S" and        /* VAES */
         b-igo-zsdivaspmap.prod     = x-quoteno  and
         b-igo-zsdivaspmap.typename = "INVENTORY" and
         b-igo-zsdivaspmap.lineno = 0  no-lock  no-error.
/* REMINDER INDEX NEEDED */         
    for each b-it-vaspmap where
         b-it-vaspmap.cono = g-cono and
         b-it-vaspmap.rectype = "LQ" and        /* VAESL */
         b-it-vaspmap.prod   = x-quoteno and
         b-it-vaspmap.typename    = "Fittings" and
         b-it-vaspmap.seqno       = b-igo-zsdivaspmap.seqno and
         b-it-vaspmap.overridety <> "y": /* don't fix overrided value */
      /* Don't override the already set manually */
      if b-it-vaspmap.overridety = "" then
        assign b-it-vaspmap.addon[1] = iv-newaddon.
      find b-i-vaspsl where b-i-vaspsl.cono = g-cono                   and
                            b-i-vaspsl.shipprod = x-quoteno            and
                            b-i-vaspsl.whse     = x-whse             and
                            b-i-vaspsl.seqno    = b-it-vaspmap.seqno and
                            b-i-vaspsl.lineno   = b-it-vaspmap.lineno  and
                            b-i-vaspsl.nonstockty <> "l"
                            no-lock no-error.
      if not avail b-i-vaspsl then 
        next.  /* blank like in the buffer or lost buisness */

      RUN VaspMap-Update (input "updt",
                          input b-it-vaspmap.seqno,
                          input b-it-vaspmap.lineno,
                          input b-i-vaspsl.qtyneeded).
 
      
      run ZsdiVaspMap-update (input "c",
                              input recid(b-i-vaspsl)).
 
    
/*    Fix up b-totals query for the totals screen if there */          
      find b-i-vasps where b-i-vasps.cono = g-cono                   and
                           b-i-vasps.shipprod = x-quoteno            and
                           b-i-vasps.whse     = x-whse               and
                           b-i-vasps.seqno    = int(b-it-vaspmap.user3) 
                           no-lock no-error.
       
      if avail b-i-vasps then
        find b-i-vaspsl where b-i-vaspsl.cono = g-cono                   and
                              b-i-vaspsl.shipprod = x-quoteno            and
                              b-i-vaspsl.whse     = x-whse               and
                              b-i-vaspsl.seqno    = int(b-it-vaspmap.user3) and
                              b-i-vaspsl.lineno   = int(b-it-vaspmap.user4)
                              no-lock no-error.
      if avail b-i-vaspsl and avail b-i-vasps then do:
        find first t-totals use-index k2 where
                   t-totals.seqno    = b-i-vaspsl.seqno         and
                   t-totals.sctntype = caps(b-i-vasps.sctntype)   and
                   t-totals.lineno   = b-i-vaspsl.lineno        and
                   t-totals.product  = caps(b-i-vaspsl.compprod) no-error.
      
        if avail t-totals and b-i-vasps.sctntype = "in" then do:
          assign t-totals.dvalue = 
               string(b-i-vaspsl.qtyneeded,">>>>>>9.99-") + " " +
               string(b-i-vaspsl.qtyneeded * b-i-vaspsl.prodcost,">>>>>>9.99-").
       
         assign ip-changed = iv-change.      
        
        end.
      end.
    end.
  
  end. /* changed */


end.
