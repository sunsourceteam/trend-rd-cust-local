/* f-porzc.i 1.1 01/03/98 */
/* f-porzc.i 1.4 01/29/96 */
/*h****************************************************************************
  INCLUDE      : f-porzc.i
  DESCRIPTION  : PO Expedite Report - Forms
  USED ONCE?   : yes (porzc.p)
  AUTHOR       : dww
  DATE WRITTEN : 01/29/96
  CHANGES MADE :
    11/16/92 mms; TB# 8718 Rush - Add whse range & exception Rush POs < stage 5
    06/08/93 enp; TB# 11544 Displays lookupnm instead of descrip[1]
    01/29/96 dww; TB# 14239 Performance when 1 vendor, 1 whse and 1 product
        line used.
    05/01/97 mtt; TB# 20773 Create Activities to Expedite POs
    05/24/00 hpl; TB# e5061 Insert reserve type field to forms for display on
        report.
******************************************************************************/

{f-under.i}
{f-blank.i}

form header
    skip(1)
    '"p"  After "Due Date" Denotes a Partial Receipt'    at 1
with frame f-footer no-box no-labels width 132 page-bottom.

/*  condition print  */
form
    skip(1)
    s-conbrk            at 1
    s-condition         at 2
with frame f-porzc1c no-box no-labels page-top width 132.

/*  condition print  */
form
    skip(1)
    s-conbrk            at 3
    s-condition         at 4
with frame f-porzc1b no-box no-labels page-top width 132.
/*
/*  buyer print  */
form
    "~015"              at 1
    s-buybrk            at 3
    "Buyer :"           at 4
    s-buyer             at 12
    "Name:"             at 39
    s-buyernm           at 45
    "~015"              at 159
    "~015"              at 159
with frame f-porzc2c no-box no-labels width 160.
*/

/*  buyer print  */
form
    "~015"              at 1
    s-buybrk            at 2
    "Buyer:"            at 3
    s-buyer             at 10
    s-buyernm           at 16
    "~015"              at 159
with frame f-porzc2b no-box no-labels  width 160.

/*  vendor print  */
form 
    "~015"                    at 1
    s-vndbrk                  at 3
    "Vendor:"                 at 4
    s-vendno                  at 12
    "Name:"                   at 39
    s-vendnm                  at 45
    "Expedite Contact:"       at 83
    s-expednm                 at 101 
    "~015"                    at 159
    "Ship From: "             at 3
    s-shipfm                  at 14
    "Address:"                at 35
    s-addr1                   at 44
    "         Phone #:"       at 82
    s-exphoneno               at 100
    "~015"                    at 159
    s-addr2                   at 44
    "     Fax Phone #:"       at 82
    s-faxphoneno              at 100
    "~015"                    at 159
    "City,St,Zip:"            at 31
    s-city                    at 44
    s-state                   at 66
    s-zipcd                   at 69
    "~015"                    at 159
    "~015"                    at 159
    "Product/"                at 4
    "Ship To"                 at 28
    "Unit"                    at 37
    "Net Avail"               at 47
    "Qty Ord"                 at 61
    "PO Date"                 at 72
    "PO #"                    at 89
    "Exp Ship Dt"             at 99
    "Notes / Comments "       at 128
    "~015"                    at 159
    "Description"             at 7
    "Due Date"                at 72
    "Ackn"                    at 81  
    "Rush/Type"               at 87
    "Last Ack Dt"             at 99
    "~015"                    at 159
"-------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------"               at 1 
    "~015"                    at 176
with frame f-porzc3 no-box no-labels width 180.

/*  column headings for product detail lines  */
/*
form header
    "Product"               at 4
    "Ship To"               at 28
    "Unit"                  at 37
    "Net Avail"             at 47
    "Qty Ord"               at 61
    "PO Date"               at 72
    "PO #"                  at 90
    "Exp Ship Dt"           at 99
    "Notes / Comments "     at 128
    "~015"                  at 159
    "Description"           at 7
    "Due Date"              at 77
    "Rush/Type"             at 87
    "Last Ack Dt"           at 104
    "Vendor#"               at 118
    "Name"                  at 133
    "Buyer"                 at 133
    "Name"                  at 139
    "~015"                  at 159
    "Ack#"                  at 133
    "Track#"                at 144
    "~015"                  at 165
"-------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------"              at 1 
    "~015"                  at 159
with frame f-porzc4 no-box no-labels page-top width 175.
*/
/*  column headings for customer backorder detail  */
form
    "~015"                  at 158
    "~015"                  at 1
    "Back Orders:"          at 16
    "Customer #"            at 36
    "Name"                  at 51
    "Order # Typ"           at 68
    "Ln #"                  at 81
    "Quantity"              at 90
    "Req Dt"                at 101
    "Line Prom Dt"          at 112
     "~015"                 at 159
    "------------"          at 35
    "---------------"       at 50
    "-----------"           at 67
    "----"                  at 80
    "--------"              at 89
    "--------"              at 100
    "------------"          at 111
    "~015"                  at 159
with frame f-porzc5 no-box no-labels width 180.

/*tb 11544 06/08/93 enp; Displays lookupnm instead of descrip[1] */
/*  detail lines for product print  */

form
    s-prod                     at 4
    t-porzc.whse               at 32
    s-unitstock                at 37
    s-netavail                 at 41
    s-qtyord                   at 55
    t-porzc.orderdt            at 72
    t-porzc.pono               at 86
    s-dash                     at 93
    t-porzc.posuf              at 94
    t-porzc.notesfl            at 96
    t-porzc.expshipdt          at 99
    s-partialfl                at 107
    "                        " at 111
    "~015"                     at 159
    s-descrip                  at 6 
    t-porzc.duedt              at 72
    t-porzc.atyp               at 81 
    s-rushlit                  at 87
    t-porzc.transtype          at 92
    t-porzc.lastackdt          at 99
    t-porzc.vendno             at 116
    t-porzc.vendnm             at 133
    t-porzc.buyer              at 133
    t-porzc.buyername          at 139 
    "~015"                     at 159
    t-porzc.ackno              at 133
    t-porzc.trackno            at 144
with frame f-porzc6 no-box down no-labels width 165.

/*  detail lines for customer backorder print  */
form
    s-custno                at 35
    s-custnm                at 50 format "x(15)"
    dp-orderno              at 67
    "-"                     at 75
    dp-ordersuf             at 76
    dp-notesfl              at 78
    dp-lineno               at 81
    dp-qtyord               at 86
    dp-reqshipdt            at 100
    dp-promisedt            at 111
    "~015"                  at 159
with frame f-porzc7 no-box down no-labels width 180.

/* detail for rush */
form
    t-porzc.orderdt         at 4
    t-porzc.pono            at 13
    "-"                     at 20
    t-porzc.posuf           at 21
    t-porzc.notesfl         at 23
    t-porzc.transtype       at 25
    s-stage                 at 28
    t-porzc.duedt           at 32
    t-porzc.netavail        at 41
    t-porzc.stkqtyord       at 55
with frame f-porzc9 no-box down no-labels width 160.






