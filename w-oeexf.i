/********************** w-oeexf.i *******************************************/
find oeelb use-index k-oeelb where
oeelb.cono      = g-cono    and
oeelb.batchnm   = x-quoteno and 
oeelb.seqno     = x-seqno   and
oeelb.lineno    = integer(o-frame-value) 
no-lock no-error. 
assign v-lineno = integer(o-frame-value)
       v-quoteno = integer(x-quoteno).