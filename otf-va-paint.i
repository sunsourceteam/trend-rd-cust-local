/* Re-Paint Header Info */
      display
            g-vano
            g-vasuf
            s-vanotesfl
            s-vatranstype
            s-vawhse
            s-vanonstockty
            s-vashipprod
            s-vaprdnotesfl
            s-vaqtyord
            s-vaunit
            s-vastatus
            s-vaproddesc
      with frame f-vaet.

/* Re-Paint Line Message */
      assign
      v-msg        =
" Ln# N Product                   Qtot Quantity  Unit Total Qty    Cost/Ext   "
      v-length     = 6.
      
      pause 0 before-hide.
      
      color display input v-msg with frame f-vaetslb.
      display v-msg with frame f-vaetslb.
      
/* Re-Paint Footer */
      {putscr.gsc &color     = "messages"
                  &blankout  = "*"
                  &f6        = "*"
                  &f6text    = "Header"
                  &f7        = "*"
                  &f7text    = "Extend"
                  &f10       = "*"
                  &f10text   = "Options"}
      if can-do("in,ii,it",s-sctntype) then do:
         {putscr.gsc &color     = "messages"
                     &f8        = "*"
                     &f8text    = "Whse Mgr"
                     &f9        = "*"
                     &f9text    = "Serial/Lot"}
      end.
