/* custombcdtt.i 
   to put the record in the outstream
*/
put stream bcd unformatted
  custom.data01 v-tilda           
  custom.data02 v-tilda           
  custom.data03 v-tilda           
  custom.data04 v-tilda           
  custom.data05 v-tilda           
  custom.data06 v-tilda           
  custom.data07 v-tilda           
  custom.data08 v-tilda           
  custom.data09 v-tilda           
  custom.data10 v-tilda           
  custom.data11 v-tilda           
  custom.data12 v-tilda           
   chr(13)
   chr(10).

