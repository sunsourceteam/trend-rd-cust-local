/* Mode Parameter */
DEFINE {1} VAR vp-mode               AS CHAR                        no-undo.

/* Tariff Parameters */
DEFINE {1} VAR vp-CanadianTariffNbr  as char format "x(15)"         no-undo. 
DEFINE {1} VAR vp-USATariffNbr       as char format "x(15)"         no-undo. 
DEFINE {1} VAR vp-CountryOfOrigin    as char format "x(02)"         no-undo.
DEFINE {1} var vp-MaintDt            as date                        no-undo.
DEFINE {1} VAR vp-CustomsExpireDt    as date                        no-undo.
DEFINE {1} VAR vp-NAFTAexpiredt      as date                        no-undo. 
DEFINE {1} VAR vp-flagtr             as char format "x(1)"          no-undo.

/* Schedule B Parameter */
DEFINE {1} VAR vp-ScheduleBInfo      as char format "x(15)"         no-undo.
DEFINE {1} var vp-ScheduleBMaintDt   as date                        no-undo. 

/* RAF parameters */
DEFINE {1} VAR vp-source             as char format "x(3)"          no-undo.
DEFINE {1} VAR vp-sourcedt           as date                        no-undo.


/* MISC Fields */
DEFINE {1} var vp-SurChargePct       as decimal 
                                         format "zzzzzzzz9.99999-"  no-undo.
DEFINE {1} var vp-NR-NC              as char format "x(1)"          no-undo.
DEFINE {1} var vp-UNSPSCcode         as char format "x(8)"          no-undo.
DEFINE {1} var vp-Protected          as char format "x(1)"          no-undo.
DEFINE {1} var vp-core               as char format "x(1)"          no-undo.
DEFINE {1} var vp-sensitivity        as char format "x(6)"          no-undo.
DEFINE {1} var vp-Leadtm             as dec  format ">>>9"          no-undo.
DEFINE {1} var vp-gasandoil          as char format "x(1)"          no-undo.
/* Fabrication Labor Information */

DEFINE {1} VAR vp-Labor              as decimal format ">>>>>9"     no-undo.
DEFINE {1} VAR vp-Documentation      as decimal format ">>>>>9"     no-undo.
DEFINE {1} VAR vp-DrawingFee         as decimal format ">>>>>9"     no-undo.
DEFINE {1} VAR vp-ElectricalLabor    as decimal format ">>>>>9"     no-undo.
DEFINE {1} VAR vp-MEchanicalLabor    as decimal format ">>>>>9"     no-undo.
                
