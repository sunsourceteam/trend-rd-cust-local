/* ibrfloes.lpr 1.5 02/19/98 */
/*h****************************************************************************
  INCLUDE      : ibrfloes.lpr (Replaces u-lnoe.i for RF terminals; Ib project)
  DESCRIPTION  :
  USED ONCE?   : NO; used in ibrfloes.p and ibrfetlo.p
  AUTHOR       :
  DATE WRITTEN : 2/16/92
  CHANGES MADE :
    01/09/98  aa; TB# 23363 Replaces u-lnoe.i (update lot #'s from oe) for RF
                            (IB project)
    02/19/98  aa; TB# 23693 Deleted code and moved all code from ibrfloes.ldi
                here; ibrfloes.ldi not needed any more;
    12/21/99  bp ; TB# 26804 Add logic for SPC Conversions
******************************************************************************/

/* u-lnoe.i 1.1 2/16/92 */

if v-returnfl then do:

    /*tb 23683 begin; commented code; not needed for RF terminals ....
        if v-seqno =  0 and
           s-returnty = "p" then
            put screen row v-frow + 14 column 64 color message "F10-Unavail".       tb 23693 end; comments ... */

    if icsel.closedt ne ? then display "CLOSED" @ icsel.qtyavail
        with frame f-rf-icetlout.  /*tb 23693*/
    s-quantity = if avail icetl then                                     /*mwb*/
                     if icetl.quantity >= v-proofqty and v-proofqty > 0  /*mwb*/
                         then v-proofqty                                 /*mwb*/
                     else if icetl.quantity > v-proofqty then 0          /*mwb*/
                     else icetl.quantity                                 /*mwb*/
                 else if s-selectfl = yes then                           /*mwb*/
                     if s-quantity > v-proofqty and v-proofqty > 0       /*mwb*/
                         then v-proofqty                                 /*mwb*/
                     else if s-quantity > v-proofqty then 0 else s-quantity
                 else v-proofqty.                                        /*mwb*/
             /* if avail icetl then icetl.quantity else v-proofqty. */

    /*tb 23693 02/09/98 aa; Display relevant frame fields for RF */
    /*tb 26804 12/22/99 bp ; Add SPC conversion. */
    display
        s-selectfl
        s-lotno
        s-quantity
        icsel.opendt
        icsel.qtyavail
        (icsel.prodcost * {speccost.gco &curricss = "b-icss."
                                        &histicss = "v-"}) @ icsel.prodcost
        icsel.expiredt
        with frame f-rf-icetlout.

    update s-quantity
        validate(if (not v-returnfl) and
                    s-quantity > v-quantity then false else true,
              "Cannot Allocate Quantity Greater Than Quantity Available (5810)")
              /*tb 23693 Do not go-on(f10) */
        with frame f-rf-icetlout.

    if s-retorderno <> 0 and v-returnfl = yes then do:                   /*mwb*/
        if avail icetl and icetl.orderno = s-retorderno and              /*mwb*/
        icetl.ordersuf = s-retordersuf and                               /*mwb*/
        icetl.quantity < s-quantity then do:                             /*mwb*/
            run ibrferr.p(6062).                               /*mwb*/
            s-quantity = 0.                                              /*mwb*/
            display s-quantity with frame f-rf-icetlout.                /*mwb*/
            next.                                                        /*mwb*/
        end.                                                             /*mwb*/
    end.                                                                 /*mwb*/

    if ({k-func10.i} or s-reasunavty ne "") and v-seqno = 0 and
    s-returnty = "p" then do:                                            /*mwb*/
        l-unavail:
        do while true on endkey undo, leave l-update:
            update s-reasunavty validate({v-sasta.i "L" s-reasunavty},
               "Reason Unavail Not Set Up in System Table Setup - SASTT (4027)")
                   s-qtyunavail validate(s-qtyunavail le s-quantity,
             "Quantity Unavail Cannot be Greater Than Quantity Received (5809)")
            with frame f-return editing:
                readkey.
                if {k-after.i} and frame-field = "s-reasunavty" then do:
                    hide message no-pause.
                    {w-sasta.i "L" "input s-reasunavty" no-lock}
                    if avail sasta then message color normal sasta.descrip.
                end.
                apply lastkey.
            end.
            if s-reasunavty = "" and s-qtyunavail ne 0
                then run ibrferr.p(2100).
            else leave l-unavail.
        end.
        hide message no-pause.
        hide frame f-return no-pause.
    end.
    else if v-returnfl = yes then                                     /* mwb */
        s-qtyunavail = if can-do("v,u",s-returnty) then s-quantity    /* mwb */
                       else 0.                                        /* mwb */

    /*tb 23693 begin; Put Screen not needed for RF Terminale (IB Project)
    .....code commented....
    if v-method = "s" then put screen row v-frow + 14 col 4 color message
"  F6-SELECT      F7-Enter                                                  ".
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
    else put screen row v-frow + 14 col 4 color message
 " F6-Select      F7-ENTER                                                  ".
        tb 23693 End; commendted code........... */

end.
else do:
    assign
        v-quantity = /* if avail icetl then icsel.qtyavail + icetl.quantity
                     else */ icsel.qtyavail
        s-quantity = /* if avail icetl then icetl.quantity else  */
                     minimum(v-proofqty,icsel.qtyavail).

    /*tb 23693 02/09/98 aa; Display relevant frame fields for RF */
    /*tb 26804 12/22/99 bp ; Add SPC conversion. */
    display
        s-selectfl
        s-lotno
        s-quantity
        icsel.opendt
        icsel.qtyavail
        (icsel.prodcost * {speccost.gco &curricss = "b-icss."
                                        &histicss = "v-"}) @ icsel.prodcost
        icsel.expiredt
        with frame f-rf-icetlout.

    update s-quantity with frame f-rf-icetlout.  /*tb 23693 */
end.
