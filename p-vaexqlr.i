/*******************************************************************************
  INCLUDE      : p-oeetlr.i
  DESCRIPTION  : Run the cross reference routine
  USED ONCE?   : no (for oeetlx.p)
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    tb       05/19/92 dby; bypass no v-crprod error message for bid prep
    tb       10/22/92 dby; correct to use g-currproc for bp bypass
    tb       02/08/93 dby; no cross reference message for bid prep
    tb 10087 02/19/93 kmw; Change g-currproc to g-ourproc
    tb 5997  03/18/93 kmw; Keeping track of last requested product
    tb 25440 05/04/99 gwk; display correct message when vendor cross ref prod
    tb e3105 12/06/99 prb; specnstype will not be blanked on customer xref
    tb e3105 12/15/99 bpa; Added Interchange, Auto Price and Barcode to types
    11/09/06 ejk; TB# e22553 Allow use of barcode xref unit
    02/27/08 emc; ERP-20557  Barcode with blank unit, use ICSP unit
*******************************************************************************/

/*d Run the cross reference routine. When a product is found, treat like a
    product change and loop to the top of the update statement with the
    new product. (Options are not run through crossref.p)

    Pass the following information:
       - product
       - whse
       - cross reference type
       - customer
       - screen row position
       - screen column position.
    Pass back the following:
       - cross reference product selected
       - alternate whse if selected
       - cross reference type found
       - normally purchased qty for a customer cross reference
       - normally purchased unit for a customer cross reference */
/*
run crossref.p(s-prod, g-whse, {1}, g-custno, 8, 5,
               output v-crprod,output v-crwhse,output v-crtype,output v-crqty,
               output v-crunit).
*/
/*d Give an error that the product is invalid if no cross reference was found */
if v-crprod = "" then do:
    if {1} <> "suw" and {1} <> "w"   then do:
        /* 5/19/92 dby  don't send error for bid prep, do set error flag */
        /* 10/22/92 dby; adjusted name of currproc to bypass when in bid prep */
        if g-ourproc ne "bpetl" then do:
            if {1} = "p" then do:
               if b-icsp.statustype = "i" then run err.p(5602).
               else run err.p(5600).
            end.
            else do:
                run err.p(4602).
            end.
        end.
        v-errflchar = "p".
    end.
end.

/*d Set cross reference variables if a product is found */
else if s-prod <> v-crprod then do:
    /*tb 5997 03/18/93 kmw; Keeping track of last requested product
    if v-reqprod = "" then v-reqprod = s-prod. */
    assign v-reqprod    = s-prod
           o-prod       = ""
           v-whse       = v-crwhse
           s-prod       = v-crprod
           s-specnstype = if can-do("c,i,t,b",v-crtype) and
                                /*tb e3105 12/15/99 bpa */
                          s-specnstype <> 'n':u /*tb e3105 11/23/99 prb */
                              then s-specnstype
                              else ""     /*tb 3712 12-20-91 kmw */
           v-xrefprodty = if      v-crtype = "z" then "i"
                          else if v-crtype = "y" then "c"
                          else if v-crtype = "w" then "b"
                          else v-crtype
           v-xreffl     = yes
           s-qtyord     = if v-xrefprodty = "c" then v-crqty  else s-qtyord
           s-unit       = if v-xrefprodty = "c":u or
                             (v-xrefprodty = "b":u and
                               g-ourproc = "oeetl":u and
                               v-crunit <> "")
                          then v-crunit else s-unit
           v-errflchar      = "p".
    if v-crtype <> "x" and g-ourproc ne "bpetl" then
        if v-crtype = "m" or v-crtype = "v" then
            message "Crossed By: Vendor Product".
        else
            message "Cross Reference Product Selected For Use".
end.

/*d If the whse doesn't match, an alternate whse was selected */
else if g-whse <> v-crwhse then do:
    assign o-prod       = ""
           v-whse       = v-crwhse
           s-prod       = v-crprod
           v-xrefprodty = v-crtype
           v-xreffl     = yes
           v-errflchar      = "p".

    /*tb 25440 05/13/99 gwk; display correct message when vendor product */
    if v-crtype <> "x" and g-ourproc ne "bpetl" then
        if v-crtype = "m" or v-crtype = "v" then
            message "Crossed By: Vendor Product".
        else
            message "Cross Reference Product Selected For Use".
end.
