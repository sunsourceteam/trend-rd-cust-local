/******************************************************************************
  INCLUDE      : d-oeexf.i
  DESCRIPTION  : Quote display line for OEEXF
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN : 
  CHANGES MADE :
               
                
                
                
*******************************************************************************/

{w-oeehb.i oeelb.batchnm 1 no-lock}

/*tb 19946 12/01/95 tdd; Core Charge not displaying */
assign
  s-descrip  = ""
  s-core     = if oeelb.restockamt <> 0 then
                 "Restock Amt " + string(oeelb.restockamt,">>>>>>>9.99-")
                                +  "$" 
               else if oeelb.corechgty = "y" then
                 "Core Charge " + string(oeelb.corecharge,">>>>>>>>9.99-")
               else if oeelb.corechgty = "r" then
                 "Core Return " + string(oeelb.corecharge,">>>>>>>>9.99-")
               else if substr(oeelb.user5,1,1) = "y" then "Built on Demand Kit"
               else ""
  s-alttxt   = ""
  s-com      = if oeelb.commentfl then "c" else ""
  s-jitflag  = ""
  s-rushdesc = if oeelb.rushfl then "Rush" else ""
  s-alttxt   = if oeelb.xrefprodty = "s" then "(Substitute)"
                 else if oeelb.xrefprodty = "u" then "(Upgrade)"
                 else if oeelb.xrefprodty = "p" then "(Supersede)"
               else ""
  s-netamt   = oeelb.price - oeelb.discamt - oeelb.discamt *
                           (if oeelb.returnfl = yes then -1 else 1)
  s-botype   = if oeelb.botype = "d" then "d" else "".

  display "" @ icsp.notesfl with frame f-oeexfd.

  if oeelb.specnstype ne "n" then 
    do:
    {w-icsp.i oeelb.shipprod no-lock}
    if avail icsp then 
      do:
      s-descrip = icsp.descrip[1].
      display icsp.notesfl with frame f-oeexfd.
    end.
    else s-descrip = v-invalid.
  end.
  else s-descrip = oeelb.proddesc.

  display s-com
          oeelb.lineno
          oeelb.specnstype
          s-botype
          oeelb.shipprod
          oeelb.unit
          s-core
          s-alttxt
          s-rushdesc
          s-jitflag
          s-descrip
          s-netamt when sasoo.oepricefl ne "n"
          oeelb.prodcost
          with frame f-oeexfd.

  /*display "  " @ oeelb.bono with frame f-oeexf.*/

  if oeelb.returnfl = yes then
    display (oeelb.qtyord * -1) @ oeelb.qtyord
          /*  (oeelb.qtyship * -1) @ oeelb.qtyship */
            with frame f-oeexfd.
  else
    display oeelb.qtyord
            /*oeel.qtyship*/
            with frame f-oeexfd.

