#!/bin/ksh
DLC=/progress/dlc73c/;export DLC
PROPATH=/dv/marc/rd/lib/trend.pl:/dv/marc/rd/twl/twl.pl:/dv/marc/rd/exec/
export PROPATH
PROTERMCAP=/dv/marc/rd/opsys/rdtermcap
export PROTERMCAP
TERM=${TERM:-vt220};export TERM
RPTID=$1;export RPTID
JOBCOMPID=$2;export JOBCOMPID
SHELL=/bin/ksh;export SHELL
if [ ! -f /dv/marc/tmp/rptsch.err ]
then
   > /dv/marc/tmp/rptsch.err
fi
chmod 666 /dv/marc/tmp/rptsch.err 2> /dev/null
echo "" >> /dv/marc/tmp/rptsch.err
echo "==============================================" >> /dv/marc/tmp/rptsch.err
echo "Starting Report Scheduler Batch Session" >> /dv/marc/tmp/rptsch.err
echo "Beginning: " `date`  >> /dv/marc/tmp/rptsch.err
exec /progress/dlc73c/bin/_progres -b -p rptrun.p -pf /dv/marc/rd/opsys/rptsch.pf -param "$3" >> /dv/marc/tmp/rptsch.err  2>&1 &
