assign inact_fl   = yes
       ic-invdt   = ?
       ic-purchdt = ?.
/* PROVE PRODUCT SHOULD STAY ACTIVE */
find icsp where icsp.cono = 1 and
                icsp.prod = {&prod} and
                icsp.statustype = "A" 
                no-lock no-error.
if not avail icsp then 
  assign inact_fl = no.
if avail icsp and icsp.prod matches "*template*" then 
  assign inact_fl = no.

if avail icsp and icsp.kittype = "B" then
  assign inact_fl = no.
  
if avail icsp and icsp.kittype = "P" then
  do:
  PROPRIETARY:
  for each icsd where icsd.cono = 1 and
                      icsd.salesfl = yes and
                      icsd.whse > "AZZZ"
                      no-lock:
    find first icsw where icsw.cono   = 1 and
                          icsw.whse   = icsd.whse and
                          icsw.prod   = {&prod} and
                          icsw.statustype <> "X" and
                          icsw.arptype = "K"
                          no-lock no-error.
    if avail icsw then
      do:
      assign inact_fl = no.
      leave PROPRIETARY.
    end.
  end. /* each icsd */
end. /* B-ICSP is PreBuild */

/*Check if Superceded or Substitute Product*/
if inact_fl = yes then
  do:
  find first icsec where icsec.cono = 1 and
                         icsec.rectype = "p" and
                         icsec.altprod = {&prod}
                         no-lock no-error.
  if avail icsec then
    assign inact_fl = no.
end.
if inact_fl = yes then
  do:
  find first icsec where icsec.cono = 1 and
                         icsec.rectype = "s" and
                         icsec.altprod = {&prod}
                         no-lock no-error.
  if avail icsec then 
    assign inact_fl = no.
end.
  
/*Check if the product is a component on a kit*/
if inact_fl = yes then
  do:
  if can-find(first kpsk where
                    kpsk.cono = 1 and
                    kpsk.comptype = "c" and
                    kpsk.comprod = {&prod}
                     no-lock) then
    assign inact_fl = no.   
end.
/*Check if the product is a kit*/
if inact_fl = yes then
  do:
  if can-find(first kpsk where
                    kpsk.cono = 1 and
                    kpsk.prod = icsp.prod
                    no-lock) then
    assign inact_fl = no.
end.

if inact_fl = yes then
  do:
  /* if STOCK Status, do not inactivate */
  find first icsw where icsw.cono = 1 and
                        icsw.whse > "AZZZ" and
                        icsw.prod = {&prod} and 
                        icsw.statustype = "S"
                        no-lock no-error.
  if avail icsw then
    assign inact_fl = no.
end.
if inact_fl = yes then
  do:
  /* if the prod went through a current price increase, do not inactivate */
  find first icsw where icsw.cono = 1 and
                        icsw.whse > "AZZZ" and
                        icsw.prod = {&prod} and 
                        icsw.statustype = "o" and
                        icsw.replcostdt >= TODAY - 1095
                        no-lock no-error.
  if avail icsw then
    assign inact_fl = no.
end.

if inact_fl = yes then
  do:
  /* if prod has a balance, or recent activity do not inactivate */
  for each icsw where icsw.cono = 1 and
                      icsw.whse > "AZZZ" and
                      icsw.prod = {&prod} and    
                      icsw.statustype = "O"
                      no-lock:
    if icsw.qtyonhand  > 0 or
       icsw.qtybo      > 0 or
       icsw.qtycommit  > 0 or
       icsw.qtyintrans > 0 or
       icsw.qtyonorder > 0 or
       icsw.qtyreservd > 0 or
       icsw.qtydemand  > 0 then
      assign inact_fl = no.
    if inact_fl = yes then
      do:
      for each icet where icet.cono = 1 and
                          icet.whse = icsw.whse and
                          icet.prod = icsw.prod and
                         (icet.transtype = "in" or
                          icet.transtype = "re")
                          no-lock:
        if icet.transtype = "re" and (ic-purchdt = ? or
                                      ic-purchdt < icet.postdt) then
          assign ic-purchdt = icet.postdt.
        if icet.transtype = "in" and (ic-invdt = ? or
                                      ic-invdt < icet.postdt) then
          assign ic-invdt = icet.postdt.
      end. /* each icet */
    end. /* inact_fl = yes */
  end. /* icsw */
  /* if activity within 3 years, do not inactivate */
  if ic-invdt   <> ? and ic-invdt   > TODAY - 1095 then    
    assign inact_fl = no.
  if ic-purchdt <> ? and ic-purchdt > TODAY - 1095 then
    assign inact_fl = no.
  if ic-invdt = ? then
    do:
    /* make sure the product was not on a Direct Order */
    find last oeel where oeel.cono = 1 and 
                         oeel.shipprod = {&prod} and
                        (oeel.transtype = "DO" or
                         oeel.transtype = "BL" or
                         oeel.transtype = "QU" or
                         oeel.botype = "d") and
                         oeel.specnstype <> "L" and
                        (oeel.invoicedt > TODAY - 1095 or
                         oeel.invoicedt = ?)
                         no-lock no-error.  
    if avail oeel then
      assign inact_fl = no.
  end.
  if inact_fl = yes then
    do:
    /* make sure the product is not on a current quote */ 
    find first oeelb where oeelb.cono        = 1 and
                           oeelb.shipprod    = {&prod} and
                           oeelb.specnstype <> "L"  and
                           oeelb.reasunavty <> "EQ"
                           no-lock no-error.
    if avail oeelb then
      assign inact_fl = no.
    if inact_fl = yes then
      do:
      /* check if a quote was entered in the past 6 months */
      for each oeelb where oeelb.cono      = 1 and
                           oeelb.shipprod  = {&prod} and
                           oeelb.seqno     = 1
                           no-lock:
        find oeehb where oeehb.cono       = oeelb.cono and    
                         oeehb.batchnm    = oeelb.batchnm and
                         oeehb.seqno      = oeelb.seqno and
                         oeehb.sourcepros = "Quote" and
                         oeehb.canceldt   > TODAY - 183
                         no-lock no-error.
        if avail oeehb then
          assign inact_fl = no. 
      end. /* for each oeelb */
    end. /* inact_fl still yes */
  end. 
end. /* inact_fl = yes */   

if inact_fl = yes then
  do:
  ICSDLOOP:
  for each icsd where icsd.cono = 1 and
                      icsd.salesfl = yes and
                      icsd.whse > "AZZZ"
                      no-lock:
    /* VAEXQ */
    for each vaspsl where vaspsl.cono = 1 and
                          vaspsl.compprod = {&prod} and
                          vaspsl.whse     = icsd.whse
                          no-lock:
      find first vasp where vasp.cono     = vaspsl.cono and
                            vasp.shipprod = vaspsl.shipprod and
                            vasp.whse     = vaspsl.whse
                            no-lock no-error.
      if avail vasp then 
        do: 
        find first zsdivasp use-index k-repairno where
                   zsdivasp.cono     = 1 and
                   zsdivasp.repairno = vasp.shipprod and
                   zsdivasp.whse     = vasp.whse and
                   zsdivasp.stagecd <= 6
                   no-lock no-error.
        if avail zsdivasp then
          do:
          assign inact_fl = no.
          leave ICSDLOOP.
        end.    
      end.
    end.
    /* VA */
    for each vaesl where vaesl.cono = 1 and
                         vaesl.shipprod = {&prod} and
                         vaesl.whse = icsd.whse and
                         vaesl.sctntype = "IN"
                         no-lock:
      if vaesl.completefl = no then
        do:
        assign inact_fl = no.
        leave ICSDLOOP.
      end.
      else
        do:
        find vaeh where vaeh.cono = 1 and
                        vaeh.vano = vaesl.vano and
                        vaeh.vasuf = vaesl.vasuf and
                        vaeh.stagecd <= 5 and
                        vaeh.enterdt > TODAY - 1095
                        no-lock no-error.   
        /* or stagecd < whatever it is for open and quote stage 0 */
        
        if avail vaeh then
          do:
          assign inact_fl = no.
          leave ICSDLOOP.
        end.
      end.
    end.    
    find first vaeh where vaeh.cono = 1 and
                          vaeh.shipprod = {&prod} and
                         (vaeh.enterdt > TODAY - 1095 or
                          vaeh.stagecd <= 5)
                          no-lock no-error.       
    if avail vaeh then
      do:
      assign inact_fl = no.
      leave ICSDLOOP.
    end.

    /* Transfers */
    for each wtel where wtel.cono = 1 and
                        wtel.shipfmwhse = icsd.whse and
                        wtel.shipprod = {&prod} and
                        wtel.enterdt > TODAY - 1095
                        no-lock: 
    /* check for open as well */    
      find wteh where wteh.cono = 1 and
                      wteh.wtno = wtel.wtno and
                      wteh.wtsuf = wtel.wtsuf and
                      wteh.stagecd <= 5
                      no-lock no-error.
      if avail wteh then
        do:
        assign inact_fl = no.
        leave ICSDLOOP.
      end.    
    end.
    /* ordertype O for order ordertype W for workorder */
    for each oeelk where oeelk.cono = 1 and
                         oeelk.whse = icsd.whse and
                         oeelk.shipprod = {&prod}
                         no-lock:
      if oeelk.ordertype = "O" then
        do:
        find oeel where oeel.cono = 1 and
                        oeel.orderno = oeelk.orderno and
                        oeel.ordersuf = oeelk.ordersuf and
                        oeel.lineno = oeelk.lineno and
                       (oeel.enterdt >= TODAY - 1095 or
                        oeel.statustype = " " or
                        oeel.statustype = "A")
                        no-lock no-error. 
   /* check for open as well */  
   /* oeel.statustype ' ' -Quote 'A' Open 'I' invoiced */
      
        if avail oeel then
          do:
          assign inact_fl = no.
          leave ICSDLOOP.
        end.    
      end. /* ordertype = "o" */
      if oeelk.ordertype = "W" then
        do:
        find first kpet where kpet.cono = 1 and
                              kpet.wono     = oeelk.orderno  and
                              kpet.wosuf    = oeelk.ordersuf and
                             (kpet.enterdt >= TODAY - 1095 or
                              kpet.stagecd <= 2)
                              no-lock no-error. 
        if avail kpet then
          do:
          assign inact_fl = no.
          next ICSDLOOP.
        end.
        
      end. /* ordertype = "w" */        
    end. /* each oeelk */
    find first kpet where kpet.cono = 1 and
                          kpet.shipprod = {&prod} and
                         (kpet.enterdt >= TODAY - 1095 or
                          kpet.stagecd <= 2)
                          no-lock no-error. 
    if avail kpet then
      do:
      assign inact_fl = no.
      next ICSDLOOP.
    end.

  end. /* each icsd-ICSDLOOP */
end. /* still inact_fl = yes */


/* Inactivate */
if inact_fl = yes then
  do:
  export stream AUD delimiter "~011"
    "ICSP" icsp.prod "" icsp.statustype icsp.enterdt.    
  find icsp where icsp.cono = 1 and
                  icsp.prod = {&prod}   
                  /* NO-LOCK     *** TESTING ***/
                  no-error. 
  if avail icsp then
    do:
    assign count = count + 1.
    /***** TESTING */
    assign icsp.statustype = "i"    
           icsp.transdt    = TODAY
           icsp.transtm    = substr(string(TIME,"HH:MM"),1,2) +
                             substr(string(TIME,"HH:MM"),4,2)
           icsp.operinit   = "INAC"
           icsp.xxda1      = TODAY.
    /* TESTING ******/
    for each icsd where icsd.cono = 1 and
                      icsd.salesfl = yes and
                      icsd.whse > "AZZZ"
                      no-lock:
      find icsw where icsw.cono = 1 and
                      icsw.whse = icsd.whse and
                      icsw.prod = icsp.prod and 
                   /* icsw.arpvendno = {&vend} and */
                      icsw.statustype <> "X"
                      no-lock no-error.
      if avail icsw then
        do:
        find i-icsw where i-icsw.cono = icsw.cono and
                          i-icsw.whse = icsw.whse and
                          i-icsw.prod = icsw.prod and
                          i-icsw.arpvendno = icsw.arpvendno
                          /* NO-LOCK   *** TESTING ***/
                          no-error.
        export stream AUD delimiter "~011"
          "ICSW" i-icsw.prod i-icsw.whse i-icsw.statustype i-icsw.enterdt.
        /**** TESTING */
        assign i-icsw.statustype = "X"
               i-icsw.transtm    = substr(string(TIME,"HH:MM"),1,2) +
                                   substr(string(TIME,"HH:MM"),4,2)   
               i-icsw.transdt    = TODAY
               i-icsw.operinit   = "INAC"
               i-icsw.xxda1      = TODAY.   
        /* TESTING ******/
      end. /* avail icsw */    
    end. /* each icsd */
  end. /* avail icsp */ 
end. /* inact_fl = yes */       

