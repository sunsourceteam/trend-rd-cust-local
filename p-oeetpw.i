/* p-oeetpw.i 1.2 10/27/98 */
/* p-oeetpw.i 1.18 05/17/96 */
/*h*****************************************************************************
  INCLUDE      : p-oeetpw.i
  DESCRIPTION  : OE PO/WT interface window
  USED ONCE?   : No (oeetpw.p & oeebtpw.p)
  AUTHOR       : mjq
  DATE WRITTEN : 06/08/92
  CHANGES MADE :
    09/11/92 mms; TB#  7813 Added new Pre stagecd
    02/15/93 kmw; TB#  7255 Orders tied to po with inactive vend
    02/19/93 kmw; TB# 10087 Change g-currproc to g-ourproc
    02/26/93 kmw; TB#  9388 Previous vendor for ship from dflt
    03/30/93 kmw; TB# 10355 Add vendor/product line
    04/12/93 kmw; TB# 10378 Allow entry of LT
    04/14/93 kmw; TB# 10934 Remove SASC reads
    05/11/93 kmw; TB# 11327 Cannot enter a whse for a ns do type
    06/16/93 mwb; TB# 11805 Made changes on PO ties to use the ICSW ARP, not
        the header if arptype is 'a'.
    07/22/93 kmw; TB# 12198 Display more info on Extended Screen
    12/14/93 mwb; TB# 13728 Lead Time not being honored if the due date is not
        changed. Loading PO/WT duedt incorrectly.
    04/20/94 mwb; TB# 15399 WT Cono was being overridden from u-oeetle.i (11370)
    03/09/95 kjb; TB# 17421 Added user hook p-oeetpw.z99
    04/07/95 tdd; TB# 16540 DO variables not retained
    05/03/95 smb; TB# 12489 Added misc vendor enhancement
    10/16/95 kr;  TB# 19565 Introduce new ARP Type of "M" (VMI)
    02/08/96 kr;  TB#  2103 Create a PO RM from an OE order
    05/17/96 kr;  TB# 21004 Need user include &user_beginamode
    10/26/98 mtt; TB# 24049 PV: Common code use for GUI interface
    07/01/99 sbr; TB# 26023 Cancel at authorization box process hung
    09/13/99 gfk; TB# 23624 Check security for non-resale vendor
    01/08/02 gwk; TB# e5667 User hooks
    06/13/02 bp ; TB# e13850 Add user hooks.
*******************************************************************************/

/*  made an include for batch use
    &bcom used to comment out logic or code not needed in batch               */

/*e Three call modes are possible:  (not used for batch)
        "h" - (H)eader call (from oeet banner) to load do defaults
        "d" - Load (D)efaults (from oeetl)  when a valid product is entered
                            on a DO order
        "u" - (U)pdate (from F7-Extend screen) manually enter/update info
        "i" - (I)nquiry call (from oeiole)                                  */

{&bcom}
def input parameter v-callmode     as c format "x"                     no-undo.
/{&bcom}* */

def input parameter v-maintmode    as c format "x"                     no-undo.

{g-all.i}
{g-ap.i}

/*  from {g-ic.i} */
def     shared var g-prod          like icsp.prod                      no-undo.
def     shared var g-whse          like icsw.whse                      no-undo.

/*  from {g-po.i} */
def     shared var g-pono          like poeh.pono                      no-undo.
def     shared var g-posuf         like poeh.posuf                     no-undo.

/*  from {g-wt.i} */
def     shared var g-cono2         like sasc.cono                      no-undo.
def     shared var g-wtno          like wteh.wtno                      no-undo.
def     shared var g-wtsuf         like wteh.wtsuf                     no-undo.

/*  from {g-oe.i} */
def     shared var g-orderno       like oeeh.orderno                   no-undo.
def     shared var g-ordersuf      like oeeh.ordersuf                  no-undo.
def     shared var g-oetype        like oeeh.transtype                 no-undo.

/*tb 23624 09/13/99 gfk; Add v-poresalefl. */
def     shared var v-poresalefl    like sasoo.resalefl                 no-undo.

def     shared var v-oetietype     like sasoo.oetietype                no-undo.

/*  from {g-oeet.i} */
def     shared var v-lastkey       as i                                no-undo.
def     shared var v-oepofl        like sasc.oepofl                    no-undo.
def     shared var v-oewtfl        like sasc.oewtfl                    no-undo.
def     shared var v-orderdisp     like oeeh.orderdisp                 no-undo.
def     shared var v-reqshipdt     like oeeh.reqshipdt                 no-undo.
def     shared var v-promisedt     like oeeh.promisedt                 no-undo.
def     shared var v-wtleadtm      like sasc.wtleadtm                  no-undo.

/* from {g-oeetl.i} */
{&bcom}
def     shared buffer b-oeeh       for oeeh.
def     shared buffer b-oeel       for oeel.
/{&bcom}* */
def     shared var s-returnfl      like oeel.returnfl                  no-undo.
def     shared var s-createpofl    as logical                          no-undo.
def     shared var s-botype        like oeel.botype                    no-undo.
def     shared var s-leadtm        like oeel.leadtm                    no-undo.
def     shared var s-lineno        like oeel.lineno                    no-undo.
def     shared var s-orderaltno    like oeel.orderaltno                no-undo.
def     shared var s-ordertype     like oeel.ordertype                 no-undo.
def     shared var s-prod          like oeel.shipprod                  no-undo.
def     shared var s-specnstype    like oeel.specnstype                no-undo.
def     shared var s-vendno        like oeel.vendno                    no-undo.
def     shared var s-whse          like icsw.arpwhse                   no-undo.
def     shared var v-maint-l       as c        format "x"              no-undo.
def            var o-duedt         like poel.duedt                     no-undo.

/* from {g-postg.i} & {g-wtstg.i} if these variables change then change the 2
    g-includes as well ***/
{&bcom}
def            var v-postg         as c format "x(3)" extent 10 initial
    ["Ent","Ord","Prt","Ack","Pre","Rcv","Cst","Cls","","Can"]         no-undo.
def            var v-wtstg         as c format "x(3)" extent 10 initial
    ["Req","Ord","Prt","Shp","Pre","Exp","Rcv","","","Can"]            no-undo.
/{&bcom}* */

/*d DO variables not retained  */
{g-oepowt.i}
{g-oedopw.i}

/*d Define miscellaneous screen variables */
def            var s-typelabel     as c format "x(12)"                 no-undo.
def            var s-typedescrip   as c format "x(35)"                 no-undo.
def            var s-vnotesfl      like apsv.notesfl                   no-undo.
def            var s-vname         like apsv.name                      no-undo.
def            var s-vshipname     like apss.name                      no-undo.
def            var s-vshipdescrip  like sasta.descrip                  no-undo.
def            var s-vexpednm      like apsv.expednm                   no-undo.
def            var s-vphoneno      like apsv.phoneno                   no-undo.
def            var s-vfaxphoneno   like apsv.faxphoneno                no-undo.
def            var s-vorderdt      like poeh.orderdt                   no-undo.
def            var s-wname         like icsd.name                      no-undo.
def            var s-wshipdescrip  like sasta.descrip                  no-undo.

/*d Define variables to store off old values for cancel (defined no-undo) */
def            var oc-ordertype    like oeel.ordertype                 no-undo.
def            var oc-orderaltno   like oeel.orderaltno                no-undo.
def            var oc-vvendno      like apsv.vendno                    no-undo.
def            var oc-vshipfmno    like apss.shipfmno                  no-undo.
def            var oc-vduedt       like poeh.duedt                     no-undo.
def            var oc-vshipviaty   like apsv.shipviaty                 no-undo.
def            var oc-vfobfl       like poeh.fobfl                     no-undo.
def            var oc-vconfirmfl   like poeh.confirmfl                 no-undo.
def            var oc-wcono        like sasc.cono                      no-undo.
def            var oc-wwhse        like icsd.whse                      no-undo.
def            var oc-wduedt       like wteh.duedt                     no-undo.
def            var oc-wshipviaty   like icsd.shipviaty                 no-undo.
def            var oc-powtnew      as logical                          no-undo.

/*d Define other variables */
def            var o-vvendno       like apsv.vendno                    no-undo.
def            var o-vshipfmno     like apss.shipfmno                  no-undo.
def            var o-vshipviaty    like poeh.shipviaty                 no-undo.
def            var o-wwhse         like icsd.whse                      no-undo.
def            var v-updttie       as logical  initial no              no-undo.
def            var v-arptype       as c format "x"                     no-undo.
def            var v-arpshipviaty  like icsd.shipviaty                 no-undo.
def            var v-darptype      like icsd.arptype                   no-undo.
def            var v-darpvendno    like icsd.arpvendno                 no-undo.
def            var v-dshipviaty    like icsd.shipviaty                 no-undo.
def            var v-warptype      like icsw.arptype                   no-undo.
def            var v-warpvendno    like icsw.arpvendno                 no-undo.
def            var v-warpwhse      like icsw.arpwhse                   no-undo.
def            var v-wleadtmavg    like icsw.leadtmavg                 no-undo.
def            var v-wconochg      as logical  initial no              no-undo.
def            var v-pochg         as logical  initial no              no-undo.
def            var v-xferchg       as logical  initial no              no-undo.
def            var v-first         as logical  initial yes             no-undo.
def            var v-leave         as logical  initial no              no-undo.
def            var v-loaderr       as logical  initial no              no-undo.
def            var v-goodpowt      as logical                          no-undo.
def            var v-authreq       as logical  initial no              no-undo.
def            var v-authfirst     as logical  initial yes             no-undo.
def            var v-savekey       as i                                no-undo.

/* cross company xfers will be done on a later release.
def            var o-wcono         like sasc.cono                      no-undo.
def            var s-wcononame     like sasc.name                      no-undo.
*/

{p-oeetpw.c99 &user_defvar = "*"}

{f-oeetpw.i} title if v-callmode = "h" then " Direct Order "
                   else " PO/WT Interface ".

{&bcom}
form
    s-ordertype      colon 13    label "       Type"
with frame typeover overlay no-box column 4 row 6 side-labels.

/*tb 12489 05/02/95 smb; Added misc vendor name, addr, city, st, zip updt*/
form
    s-mvname         colon  9    label "Name"
    s-vaddr[1]       colon  9    label "Address"
    s-vaddr[2]       at    11 no-label
    s-vcity          colon  9    label "City"
    s-vstate         at    32 no-label
    s-vzipcd         at    35 no-label
with frame f-miscvend overlay side-labels row 5 column 30 width 46 title
" Misc Vendor Address ".
/{&bcom}* */

v-savekey = v-lastkey.

/*d Store old variable values in case the cancel key is pressed */
assign
    s-ordertype   = if v-callmode = "h" and v-maintmode = "a" then
                        /*if can-do("p,t",v-oetietype) then v-oetietype
                          else "a"*/
                      "p"
                    else s-ordertype
    s-typelabel   = if v-callmode = "h" then "       Type:"
                    else "PO/Transfer:"
    oc-ordertype  = s-ordertype
    oc-orderaltno = s-orderaltno
    oc-vvendno    = s-vvendno
    oc-vshipfmno  = s-vshipfmno
    oc-vduedt     = s-vduedt
    oc-vconfirmfl = s-vconfirmfl
    oc-vfobfl     = s-vfobfl
    oc-vshipviaty = s-vshipviaty
    oc-wcono      = s-wcono
    oc-wwhse      = s-wwhse
    oc-wduedt     = s-wduedt
    oc-wshipviaty = s-wshipviaty
    oc-powtnew    = v-powtnew
    v-updttie     = no
    s-wcono       = g-cono.

/*o Add mode - Load arp defaults for warehouse and product */
/*tb 24049 10/26/98 mtt; PV: Common code use for GUI interface */
{oeetpwa.las &orderaltno    = "s-orderaltno"
             &whse          = "g-whse"
             &prod          = "s-prod"
             &specnstype    = "s-specnstype"
             &leadtm        = "s-leadtm"}

/*o Main processing loop- for header call or line add mode */
if (v-maintmode = "a" or v-callmode = "h" or
   (s-orderaltno = 0 and can-do("bl,fo,qu,st",g-oetype))) then
main:
do with frame f-oeetpw while true on endkey undo main, leave main
                                  on error  undo main, leave main:

    {p-setup2.i f-oeetpw}

    /*tb 12489 05/02/95 smb; Added misc vendor enhancement.  variable assigns*/
    assign
        s-vname        = ""
        s-vshipname    = ""
        s-vexpednm     = ""
        s-vphoneno     = ""
        s-vfaxphoneno  = ""
        s-vorderdt     = ?
        /* cross company transfers will be handled on a later release
        s-wcononame    = ""                                         */
        s-wname        = ""
        s-vshipdescrip = ""
        s-wshipdescrip = ""
        s-mvname       = ""
        s-vaddr        = ""
        s-vcity        = ""
        s-vstate       = ""
        s-vzipcd       = "".

    {p-oeetpw.z99 &user_afterasgn = "*"}
    
    /*
    if g-operinits = "tah" then
      do:
      message v-powtintfl v-callmode  v-maintmode .  
      pause.
      end.
    */
    
    /*tb 24049 10/26/98 mtt; PV: Common code use for GUI interface */
    {oeetpw.las &ordertype          = "s-ordertype"
                &vvendno            = "s-vvendno"
                &vshipfmno          = "s-vshipfmno"
                &vduedt             = "s-vduedt"
                &vorderdt           = "s-vorderdt"
                &vshipviaty         = "s-vshipviaty"
                &vfobfl             = "s-vfobfl"
                &vconfirmfl         = "s-vconfirmfl"

                &wcono              = "s-wcono"
                &wwhse              = "s-wwhse"
                &wduedt             = "s-wduedt"
                &wshipviaty         = "s-wshipviaty"

                &specnstype         = "s-specnstype"
                &returnfl           = "s-returnfl"
                &scrnvendno         = "s-vendno"
                &arpwhse            = "s-whse"
                &oeel-arpvendno     = "b-oeel.arpvendno"
                &oeel-arpprodline   = "b-oeel.arpprodline"
                &oeel-shipfmno      = "b-oeel.shipfmno"}

    /*tb 26023 07/01/99 sbr; Added code to be executed when the cancel key is
        hit within the oeetpw.las include program. */

    if s-ordertype = "a" then
      assign s-ordertype = "p".

    if {k-cancel.i} then do:
        undo main.
        leave main.
    end.
    
    /**** das
    if g-currproc = "oeexq" then
      dooeel.arpvendno"
      :
      find oeehb where oeehb.cono = g-cono and  
                             oeehb.batchnm = string(g-orderno,">>>>>>>9") and
                             oeehb.seqno      = 1 and
                             oeehb.sourcepros = "Quote"
                             no-lock no-error.
      if avail oeehb and num-entries(oeehb.user4,"^") ge 9 then
        assign s-vvendno    = dec(entry(2,oeehb.user4,"^"))
               s-vshipfmno  = dec(entry(3,oeehb.user4,"^"))
               s-vduedt     = date(entry(5,oeehb.user4,"^")
               s-vfobfl     = if entry(6,oeehb.user4,"^") = "yes" then yes
                              else no
               s-vconfirmfl = if entry(7,oeehb.user4,"^") = "yes" then yes
                              else no.
    end. /* if g-currproc = "oeexq" */
    ***/
    
    /* das - 10/10/2007 */
    if v-maintmode = "a" and g-currproc = "oeexq" and
       s-vvendno = 0 then
      assign s-vvendno = g-vendno.

    if v-callmode <> "d" or v-loaderr then
    do:

        assign
            s-typedescrip = if s-ordertype = "a" then "ARP"
                            else if s-ordertype = "p" then "Vendor PO"
                            else "Whse Transfer".
        display
            s-typelabel
            s-ordertype
            s-typedescrip.

    end. /* end of if v-callmode <> d or .... */

    /*o Add mode or Header call updates */
    a-mode-updt:
    do while true on endkey undo a-mode-updt, leave a-mode-updt
                  on error  undo a-mode-updt, leave a-mode-updt:

        if not (v-callmode = "h" and v-first and v-maintmode <> "c") and
            s-ordertype = "p" then do for apsv, apss, sasta, poeh:

            /*u User Hook */
            {p-oeetpw.z99 &user_beginamode = "*"}

            /*d Load vendor po defaults -- check for existing po and load
                defaults from it or from previously stored product and/or
                warehouse defaults */
            {&bcom}
            {p-posrch.i}
            {w-sasta.i s s-vshipviaty no-lock}
            assign
                s-vshipdescrip = if avail sasta then sasta.descrip
                                 else if s-vshipviaty <> "" then v-invalid
                                 else ""
                o-duedt        = s-vduedt.
            /{&bcom}* */

            if g-ourproc = "oeebt" or g-ourproc = "oeexq" then
            do:

                {w-apsv.i s-vvendno no-lock}
                if s-vshipfmno <> 0 then
                    {w-apss.i s-vvendno s-vshipfmno no-lock}
                assign
                    s-wwhse       = ""
                    s-wduedt      = ?
                    s-wshipviaty  = ""
                    s-vnotesfl    = if avail apsv then apsv.notesfl else ""
                    s-vname       = if avail apsv then apsv.name
                                    else if s-vvendno = 0 then ""
                                    else v-invalid
                    s-vshipname   = if avail apss then apss.name
                                    else if s-vshipfmno = 0 then ""
                                    else v-invalid
                    s-vexpednm    = if      avail apss then apss.expednm
                                    else if avail apsv then apsv.expednm
                                    else ""
                    s-vphoneno    = if avail apss then
                                     if apss.exphoneno <> "" then apss.exphoneno
                                     else apss.phoneno
                                    else if avail apsv then
                                     if apsv.exphoneno <> "" then apsv.exphoneno
                                     else apsv.phoneno
                                    else ""
                    s-vfaxphoneno = if     avail apss then apss.faxphoneno
                                    else if avail apsv then apsv.faxphoneno
                                    else "".

            end. /* end of if g-ourproc = "oeebt" */

        end. /* end of if not (v-callmode = "h" and ... */

        else if not (v-callmode = "h" and v-first and v-maintmode <> "c")
        then do for icsd, sasc, sasta, wteh:

            /*d Load wt defaults -- check for existing wt and load defaults
                from it or from previously stored product and/or warehouse
                defaults */
            {&bcom}
            {p-wtsrch.i}
            {w-sasta.i s s-wshipviaty no-lock}
            assign
                s-wshipdescrip = if avail sasta then sasta.descrip
                                 else if s-wshipviaty <> "" then v-invalid
                                 else ""
                o-duedt        = s-wduedt.
            /{&bcom}* */

            if g-ourproc = "oeebt" or g-ourproc = "oeexq" then
            do:

                {wc-icsd.i s-wcono s-wwhse no-lock}
                assign
                    s-vvendno    = 0
                    s-vshipfmno  = 0
                    s-vduedt     = ?
                    s-vfobfl     = no
                    s-vconfirmfl = no
                    s-vshipviaty = ""
                    s-wname      = if avail icsd then icsd.name
                                   else if s-wwhse <> "" then v-invalid
                                   else "".

            end. /* end of if g-ourproc = oeebt */

        end. /* end of if not v-callmode = h and .... */

        else if s-ordertype = "t" then
            s-wcono = g-cono.

        /*e If called to load defaults and attempting a WT from the same
                warehouse, display error and force update */
        if v-callmode = "d" and s-wwhse = g-whse and s-wcono = g-cono
            and v-first then do:

            assign
                v-first     = no
                v-updttie   = yes
                s-ordertype = ""
                v-loaderr   = yes.

            /*e Can't xfer to same whse */
            run err.p(5885).
            pause.
            next main.

        end. /* end of if v-callmode = d */

        /*d Update vendor po information */
        if (can-do("h,u",v-callmode) or v-loaderr) and s-ordertype = "p"
        then do:

            /*tb 17421 03/09/95 kjb; Added user hook */
            {p-oeetpw.z99 &user_befvend = "*"}

            display
                s-ordertype
                s-typedescrip
                s-vvendno
                s-vnotesfl
                s-vname
                s-vshipfmno
                s-vshipname
                s-vexpednm
                s-vphoneno
                s-vfaxphoneno
                s-vorderdt     when can-do("br,cs,do,so",g-oetype)
                                        and g-ourproc <> "oeebt"
                s-vduedt       when can-do("br,cs,do,so",g-oetype)
                                        and g-ourproc <> "oeebt"
                s-vshipviaty   when can-do("br,cs,do,so",g-oetype)
                                        and g-ourproc <> "oeebt"
                s-vshipdescrip when can-do("br,cs,do,so",g-oetype)
                                        and g-ourproc <> "oeebt"
                s-vfobfl       when can-do("br,cs,do,so",g-oetype)
                                        and g-ourproc <> "oeebt"
                s-vconfirmfl   when can-do("br,cs,do,so",g-oetype)
                                        and g-ourproc <> "oeebt".

            if s-vname = v-invalid then
            do:

                run err.p(4301).
                next-prompt s-vvendno.

            end. /* end of if s-vname */

            else if s-vshipname = v-invalid then
            do:

                run err.p(4302).
                next-prompt s-vshipfmno.

            end. /* end of if s-vshipname */

            if v-leave then leave a-mode-updt.
            else if {k-up.i} then
            do:

                if frame-field = "s-vvendno"
                and can-do("bl,fo,qu,st",g-oetype) then
                    next-prompt s-vshipfmno.
                else if frame-field = "s-vvendno" and v-powtnew then
                    next-prompt s-vconfirmfl.
                else if frame-field = "s-vvendno" then
                    next-prompt s-vduedt.
                else if frame-field = "s-vvendno" then
                    next-prompt s-vvendno.

            end. /* end of if k-up.i */

            else if v-pochg and s-vvendno <> o-vvendno then
                next-prompt s-vshipfmno.
            else if v-pochg and s-vshipfmno <> o-vshipfmno then
                next-prompt s-vduedt.
            else next-prompt s-vvendno.

            assign
                o-vvendno    = if s-vname <> v-invalid then s-vvendno
                               else o-vvendno
                o-vshipfmno  = if s-vshipname <> v-invalid then s-vshipfmno
                               else o-vshipfmno
                o-vshipviaty = if s-vshipdescrip <> v-invalid then s-vshipviaty
                               else o-vshipviaty
                v-pochg      = no
                g-vendno     = s-vvendno.

            /*tb 2103 02/12/96 kr; Create PO RM from an OE order, added the
                s-returnfl = no condition to allow entry if the user is
                not tying to a PO */
    
      
            update
                s-vvendno
                s-vshipfmno
                s-vduedt
                    when can-do("br,cs,do,so",g-oetype)
                        and g-ourproc <> "oeebt" and s-returnfl = no
                s-vshipviaty
                    when v-powtnew
                        and can-do("br,cs,do,so",g-oetype)
                        and g-ourproc <> "oeebt" and s-returnfl = no
                s-vfobfl
                    when v-powtnew
                        and can-do("br,cs,do,so",g-oetype)
                        and g-ourproc <> "oeebt" and s-returnfl = no
                s-vconfirmfl
                    when v-powtnew
                        and can-do("br,cs,do,so",g-oetype)
                        and g-ourproc <> "oeebt" and s-returnfl = no
            editing:

                readkey.
                if {k-accept.i} then v-leave = yes.
                if {k-after.i} then
                do:

                    v-first = no.
                    hide message no-pause.
                    if frame-field = "s-vvendno" then do for apsv, sasta:

                        assign s-vvendno.
                        {w-apsv.i s-vvendno no-lock}

                        if not avail apsv then
                        do:

                            display v-invalid @ s-vname.
                            /*e Vendor not set up */
                            run err.p(4301).
                            next-prompt s-vvendno.
                            next.

                        end. /* end of if not avail apsv */

                        if apsv.statustype = no then
                        do:

                            run err.p(5387).
                            next-prompt s-vvendno.
                            next.

                        end. /* end of if apsv.statustype = no */

                        /*TB23624 gfk; 09/13/99 Check not for resale security */
                        if apsv.resalefl = no and v-poresalefl = no then do:
                               run err.p(1131).
                            next-prompt s-vvendno.
                            next.

                        end. /* end of if apsv.resalefl = no */

                        /*e if the vendor changes, need to go back and search
                                for any existing po for the new vendor tied to
                                this order */
                        else if s-vvendno <> o-vvendno then
                        do:

                            /*tb 12489 05/04/95 smb; Misc Vend info cleared*/
                            if g-ourproc <> "oeebt" then
                            do:

                                assign
                                    v-pochg  = yes
                                    g-vendno = s-vvendno
                                    s-mvname = ""
                                    s-vaddr  = ""
                                    s-vcity  = ""
                                    s-vstate = ""
                                    s-vzipcd = "".
                                next-prompt s-vshipfmno.
                                next a-mode-updt.

                            end. /* end of if g-ourproc <> oeebt */

                            else do:

                                assign
                                    s-vnotesfl    = apsv.notesfl
                                    s-vname       = apsv.name
                                    s-vexpednm    = apsv.expednm
                                    s-vphoneno    = if apsv.exphoneno <> "" then
                                                       apsv.exphoneno
                                                    else apsv.phoneno
                                    s-vfaxphoneno = apsv.faxphoneno
                                    o-vvendno     = s-vvendno
                                    g-vendno      = s-vvendno.

                                display
                                    s-vnotesfl
                                    s-vname
                                    s-vexpednm
                                    s-vphoneno
                                    s-vfaxphoneno.

                            end. /* end of else do */

                        end. /* end of else if s-vvendno <> o-vvendno */

                    end. /* end of s-vvendno */

                    if frame-field = "s-vshipfmno" then do for apss, sasta:

                        assign s-vshipfmno.

                        if s-vshipfmno <> 0 then
                        do:

                            {w-apss.i s-vvendno s-vshipfmno no-lock}
                            if not avail apss then
                            do:

                                display v-invalid @ s-vshipname.
                                /*e Ship from not set up */
                                run err.p(4302).
                                next-prompt s-vshipfmno.
                                next.

                            end. /* end of if not avail apss */

                        end. /* end of if s-vshipfmno <> 0 */

                        /*e If the ship from changes, then need to go back and
                            search for an existing po for the new vendor/ship
                            from combination tied to this order */
                        if s-vshipfmno <> o-vshipfmno then
                        do:

                            if g-ourproc <> "oeebt" then
                            do:

                                assign v-pochg = yes.
                                next-prompt s-vduedt.
                                next a-mode-updt.

                            end. /* end of if g-ourproc <> oeebt */

                            else do:

                                assign
                                    s-vshipname   = apss.name
                                    s-vexpednm    = apss.expednm
                                    s-vphoneno    = if apss.exphoneno <> "" then

                                                       apss.exphoneno
                                                    else apss.phoneno
                                    s-vfaxphoneno = apss.faxphoneno.
                                display
                                    s-vshipname
                                    s-vexpednm
                                    s-vphoneno
                                    s-vfaxphoneno.

                            end. /* end of else do */

                        end. /* end of if s-vshipfmno */

                    end. /* end of if frame-field = s-vshipfmno */

                    if frame-field = "s-vshipviaty" and
                        input s-vshipviaty <> o-vshipviaty then
                    do for sasta:

                        assign s-vshipviaty.

                        {w-sasta.i s s-vshipviaty no-lock}
                        if not avail sasta and s-vshipviaty <> "" then
                        do:

                            assign
                                s-vshipdescrip = v-invalid
                                o-vshipviaty   = ?.
                            display s-vshipdescrip.
                            /*e Ship via not set up */
                            run err.p(4030).
                            next-prompt s-vshipviaty.
                            next.

                        end. /* end of if not avail sasta */

                        assign
                            s-vshipdescrip = if s-vshipviaty = "" then ""
                                             else sasta.descrip.
                        display s-vshipdescrip.

                    end. /* end of if s-vshipviaty */

                    /*tb e5667 01/08/02 gwk; User hooks */
                    {p-oeetpw.z99 &user_aft_shipvia = "*"}


                end. /* end of k-after */

                apply lastkey.

            end. /* end of po information editing loop */

            /*e If <GO> is pressed, insure vendor & shipfrom are valid */
            if ({k-accept.i} and g-ourproc = "oeebt"  and
                frame-field = "s-vvendno"             and
                not {v-apss.i s-vvendno s-vshipfmno}) or
               (s-vname = v-invalid or s-vshipname = v-invalid)
            then
                next a-mode-updt.

        end. /* end of vendor po information */

        /*d Update WT information */
        else if (can-do("h,u",v-callmode) or v-loaderr) and s-ordertype = "t"
        then do:

            /*tb 17421 03/09/95 kjb; Added user hook */
            {p-oeetpw.z99 &user_befwhse = "*"}

            /* cross company transfers will be handled on a later release */
            display
                s-ordertype
                s-typedescrip

                {p-oeetpw.z99 &user_display = "*"}

             /* s-wcono
                s-wcononame */
                s-wwhse
                s-wname
                s-wduedt        when can-do("br,cs,do,so",g-oetype)
                                         and g-ourproc <> "oeebt"
                s-wshipviaty    when can-do("br,cs,do,so",g-oetype)
                                         and g-ourproc <> "oeebt"
                s-wshipdescrip  when can-do("br,cs,do,so",g-oetype)
                                         and g-ourproc <> "oeebt".

            if v-leave then leave a-mode-updt.
            if v-xferchg then
                next-prompt s-wduedt.
            else
                next-prompt s-wwhse.
            assign
                /* cross company transfers will be handled on a later release
                o-wcono = s-wcono                                            */
                o-wwhse = s-wwhse
                v-xferchg = no.

            update
                /* cross company transfers will be handled on a later release
                s-wcono
                    validate ({v-sasc.i s-wcono "/ *"},
                            "Company Not Set Up in Company Setup - SASC (4000)")
                                                                             */
                s-wwhse
                s-wduedt
                    when can-do("br,cs,do,so",g-oetype)
                        and g-ourproc <> "oeebt"
                s-wshipviaty
                    validate ({v-sasta.i s s-wshipviaty},
                           "Ship Via Not Set Up in System Table - SASTT (4030)")
                    when v-powtnew
                        and can-do("br,cs,do,so",g-oetype)
                        and g-ourproc <> "oeebt"
            editing:

                readkey.
                if {k-cancel.i} then /* das 4/23/07 */
                  do:
                  undo main.
                  leave main.
                end.

                if {k-accept.i} then v-leave = yes.
                if {k-after.i} then
                do:

                    v-first = no.
                    hide message no-pause.
                    /* cross company transfers will be handled on a later
                        release
                    if frame-field = "s-wcono"
                    and input s-wcono <> s-wcono then do for sasc:

                        {wc-sasc.i "input s-wcono" no-lock}
                        assign
                            s-wcononame = if not avail sasc
                                              then v-invalid
                                          else if input s-wcono <> g-cono
                                              then sasc.name
                                          else ""
                            s-wcono
                            v-wconochg  = yes.
                        display s-wcononame.

                    end. /* end of s-wcono */               */

                    if frame-field = "s-wwhse" then do for icsd, icsw:

                        /* cross company transfers later
                        if {k-up.i} then
                        do:

                            apply lastkey.
                            next.

                        end. /* end of if k-up.i */                 */

                        /* cross company transfers later */
                        if input s-wwhse <> s-wwhse /* or v-wconochg */
                        or input s-wwhse = "" then
                        do:

                            {wc-icsd.i s-wcono "input s-wwhse" no-lock}
                            assign
                                s-wname = if avail icsd then icsd.name
                                        else v-invalid.
                            display s-wname.

                            if not avail icsd then
                            do:

                                /*e Warehouse not set up */
                                run err.p(4601).
                                next-prompt s-wwhse.
                                next.

                            end. /* end of if not avail icsd */

                        end. /* end of whse/cono change */

                        if input s-wwhse = g-whse
                        /* cross company transfers later
                        and input s-wcono = g-cono */ then
                        do:

                            /*e Can't xfer to same whse */
                            run err.p(5885).
                            next-prompt s-wwhse.
                            next.

                        end. /* end of if input s-wwhse */

                        /*d On all but header calls, need to be sure the
                            product exists in the ship from warehouse
                            (except if NS in both warehouses) and is not a
                            DO only product in the from whse */
                        if v-callmode <> "h" and s-specnstype <> "n" then
                        do:

                            {wc-icsw.i s-wcono s-prod "input s-wwhse" no-lock}
                            if not avail icsw then
                            do:

                                /*e Product not set up in shipfm whse */
                                run err.p(5876).
                                next-prompt s-wwhse.
                                next.

                            end. /* end of if not avail icsw */

                            else if icsw.statustype = "d" then
                            do:

                                /*e Cannot be DO prod on WT type */
                                run err.p(5967).
                                next-prompt s-wwhse.
                                next.

                            end. /* end of else if icsw.statustype = d */

                        end. /* end of if v-callmode <> h */

                        /*e If the warehouse changes, then need to go back
                                and see if a WT from the new warehouse tied to
                                this order already exists */
                        if input s-wwhse <> o-wwhse
                        /* cross company transfers later
                                                     or s-wcono <> o-wcono) */
                        then do:

                            if g-ourproc <> "oeebt" then
                            do:

                                assign
                                    s-wwhse
                                    v-xferchg = yes.
                                next a-mode-updt.

                            end. /* end of if g-ourproc <> oeebt */

                            else do:

                                assign
                                    s-wname = icsd.name.
                                display s-wname.

                            end. /* end of else do */

                        end. /* end of if input s-wwhse */

                    end. /* end of if frame-field = s-wwhse */

                    if frame-field = "s-wshipviaty" and
                        input s-wshipviaty <> s-wshipviaty
                    then do for sasta:

                        {w-sasta.i s "input s-wshipviaty" no-lock}
                        assign
                            s-wshipdescrip = if avail sasta
                                                 then sasta.descrip
                                             else if s-wshipviaty <> ""
                                                 then v-invalid
                                             else "".
                        display s-wshipdescrip.

                    end. /* end of s-wshipviaty */

                end. /* end of k-after */

                apply lastkey.

            end. /* end of wt information editing */

        end. /* end of WT information */

        {p-oeetpw.z99 &user_else = "*"}

        else if v-callmode = "h" and s-ordertype = "a" then
            assign
                s-vvendno   = 0
                s-vshipfmno = 0
                s-wwhse     = "".

        /*d On all but header calls, validate the selected source */
        if v-callmode <> "h" and s-specnstype <> "n" then
        do:

            if s-ordertype = "t" then do for icsw:

                {wc-icsw.i s-wcono s-prod s-wwhse no-lock}
                if not avail icsw then
                do:

                    run err.p(5876).    /* Product not set up in shipfm whse */

                    /*e If called to load defaults, pause for user to read error
                        message before going back and allowing update */
                    if v-callmode = "d" and not v-loaderr then pause.
                    assign
                        v-updttie   = yes
                        s-ordertype = ""
                        v-loaderr   = yes.
                    next main.

                end. /* end of if not avail icsw */

                else if icsw.statustype = "d" then
                do:

                    /*e Cannot be DO prod on WT type */
                    run err.p(5967).

                    /*e If called to load defaults, pause for user to read error
                        message before going back and allowing update */
                    if v-callmode = "d" and not v-loaderr then pause.
                    assign
                        v-updttie   = yes
                        s-ordertype = ""
                        v-loaderr   = yes.
                    next main.

                end. /* end of else if icsw.statustype = d */

            end. /* end of if s-ordertype = t */

            if s-ordertype = "p" then do for apsv:

                if not {v-apsv.i s-vvendno "/*} then
                do:

                    run err.p(5794).

                    /*e If called to load defaults, pause for user to read error
                        message before going back and allowing update */
                    if v-callmode = "d" and not v-loaderr then pause.
                    assign
                        v-updttie   = yes
                        s-ordertype = ""
                        v-loaderr   = yes.
                    next main.

                end. /* end of if not v-apsv.i */

            end. /* end of if s-ordertype = p */

        end. /* end of if v-callmode <> "h" ... */

        leave a-mode-updt.

    end. /* end of a-mode-updt */

    /*e If the user presses cancel and they have updated the tie type,
        go back and update the ordertype again, else leave */
    /*tb 2103 04/16/96 kr; Create a PO RM from an OE order, added
        s-returnfl = no to condition.  A cancel from the F6-Vendor screen will
        take you back to the F17-Return screen */
    if (v-callmode <> "d" or v-loaderr) and {k-cancel.i} and v-updttie
        and s-returnfl = no then do:

        assign
            s-ordertype = if v-callmode = "h" then s-ordertype else ""
            v-first     = if v-callmode = "h" then yes         else no.
        clear frame f-oeetpw.
        next main.

    end. /* end of if v-callmode <> d */

    hide message no-pause.
    pause if {k-cancel.i} then 0
            else 1
        no-message.
    leave main.

end. /* end of if v-maintmode = "a" ** main */

