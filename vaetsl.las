/* SX 55 */
/* vaetsl.las 1.5 10/15/98 */
/*h****************************************************************************
  INCLUDE      : vaetsl.las
  DESCRIPTION  : Value Add Entry Transactions - Line Items - Assign Include
  USED ONCE?   : yes
  AUTHOR       : mtt
  DATE WRITTEN : 07/02/96
  CHANGES MADE :
    07/02/96 mtt; TB# 21254 (2F) Develop Value Add Module
    10/26/96 mtt; TB# 21254 (2F) Develop Value Add Module
    05/08/98 kjb; TB# 22842 Allow catalog product selection
    08/18/98 bm;  TB# 24879 Added Must Count Update to vaetslex.p
        for section type "in" line items.  Added s-countfl.
    08/24/98 bm;  TB# 24812 Add Rush Functionality type "IN" Lines.
        Added s-lnrushfl, s-lnrushdesc, o-lnrushfl
    10/05/98 kjb; TB#  9731 Add a second nonstock description.
    01/28/02 mms; TB# e10302 Added o-lnxxi3 logic.
    04/30/02 mwb; TB# e12382 Rollup from 3.0 for e10302 (xxi3 now boseqno)
    01/19/03 bp ; TB# e16009 Add additional hooks.
    08/14/03 bp ; TB# e18204 Add user hook.
******************************************************************************/

/*e Clear out screen line item variables, so they are in one location.  This
    section is used during "add" mode to initialize the variables. */

{vaetsl.z99 &bef_init = "*"}

/{&initialize}*/

/*tb 9731 10/05/98 kjb; Added proddesc2 for second nonstock description */
assign
     s-lncommentfl     = ""
     s-lnstatustype    = ""

     s-lnnonstockty    = ""
     s-lnshipprod      = ""
     s-lnproddesc      = ""
     s-lnproddesc2     = ""
     s-lnprodnotesfl   = ""
     s-lnwhse          = if can-do("it,is",s-sctntype) then
                            if s-sctnintrwhse ne "" then s-sctnintrwhse
                            else s-vawhse
                         else s-vawhse

     s-lnunit          = "each"
     s-lnunitconv      = 1
     s-lnqtybasetotfl  = if can-do("it,is,ii",s-sctntype) then
                             no
                         else yes
     s-lnqtyneeded     = 1
     s-lnqtyord        = 1
     s-lnstkqtyord     = 1
     s-lnqtyship       = 0
     s-lnstkqtyship    = 0

     s-lnprodcost      = 0
     s-lnnetamt        = 0

     s-lnarpvendno     = 0
     s-lnarpprodline   = ""
     s-lnarpwhse       = ""

     s-lnorderalttype  = ""
     s-lnorderaltno    = 0
     s-lnlinealtno     = 0
     s-lnusagefl       = yes
     s-lnrushfl        = no
     s-lnrushdesc      = ""

     s-lnprodcat       = ""

     s-lnlaborflatrtfl = no
     s-lnlabortype     = ""
     s-lnlaborunits    = 0
     s-lnlabordata     = ""

     s-lntimeactty     = ""
     s-lntimeworkdt    = ?
     s-lntimecomment   = ""
     s-lntimeslsrep    = ""
     s-lntimeslsrepnm  = ""
     s-hours           = 0
     s-minutes         = 0

     s-lndirectfl      = no

     s-lncubes         = 0
     s-lnweight        = 0
     s-lnextcubes      = 0
     s-lnextweight     = 0

     s-lnshpqtyoverfl  = no
     s-lncostoverfl    = no
     s-lnintermprodfl  = no

     s-lnqtyunavail    = 0
     s-lnreasunavty    = ""

     v-dispqtyordfl    = no
     v-icspstatus      = ""
     v-icswstatus      = ""
     v-serlottype      = ""
     v-wmfl            = no

     s-countfl         = no
     s-netavail        = 0
     s-qtyonorder      = 0

     o-lnrushfl        = no
     o-lnnonstockty    = ""
     o-lnshipprod      = ?
     o-lnstkqtyord     = 0
     o-lnstkqtyship    = 0
     o-lnnetamt        = 0
     o-lnunit          = ?
     o-lnunitconv      = 0
     o-lnprodcost      = 0
     o-lntimeactty     = ?
     o-lnqtybasetotfl  = ?
     o-lnstatustype    = no
     o-lndirectfl      = no
     o-lnqtyneeded     = 0
     o-lnboseqno       = 0.

/{&initialize}*/

{vaetsl.z99 &aft_init = "*"}

/{&record-to-screen}*/

do for icsp, icsw, smsn:

    if {&buf}vaesl.nonstockty ne "n" then do:
        {w-icsp.i {&buf}vaesl.shipprod no-lock}
        {w-icsw.i {&buf}vaesl.shipprod {&buf}vaesl.whse no-lock}
    end.

    /*tb 22842 05/08/98 kjb; If a nonstock, attempt to find the ICSC record */
    else do:
        {w-icsc.i {&buf}vaesl.shipprod "no-lock"}
    end.

    /*tb 22842 05/08/98 kjb; When working with a nonstock, attempt to load the
        notes flag from the ICSC record. */
    /*tb 9731 10/05/98 kjb; Added proddesc2 for second nonstock description */
    assign
         s-lnlineno         = {&buf}vaesl.lineno
         s-lncommentfl      = if {&buf}vaesl.commentfl = yes then
                                  "c"
                              else ""
         s-lnstatustype     = if {&buf}vaesl.statustype = false then "I"
                              else ""
         s-lnnonstockty     = {&buf}vaesl.nonstockty
         s-lnshipprod       = {&buf}vaesl.shipprod
         s-lnproddesc       = if s-lnnonstockty = "n" then
                                  {&buf}vaesl.proddesc
                              else
                              if avail icsp then icsp.descrip[1]
                              else v-invalid
         s-lnproddesc2      = if s-lnnonstockty = "n" then
                                  {&buf}vaesl.proddesc2
                              else
                              if avail icsp then icsp.descrip[2]
                              else v-invalid
         s-lnprodnotesfl    = if s-lnnonstockty = "n" then
                                  if avail icsc then icsc.notesfl
                                  else ""
                              else if avail icsp then icsp.notesfl
                              else ""
         s-lnwhse           = {&buf}vaesl.whse

         s-lnunit           = {&buf}vaesl.unit
         s-lnunitconv       = {&buf}vaesl.unitconv
         s-lnqtybasetotfl   = {&buf}vaesl.qtybasetotfl
         s-lnqtyneeded      = {&buf}vaesl.qtyneeded

         s-lnqtyord         = if {&buf}vaesl.sctntype = "ex"
                                  then {&buf}vaesl.laborunits
                              else {&buf}vaesl.qtyord
         s-lnstkqtyord      = {&buf}vaesl.stkqtyord

         s-lnqtyship        = {&buf}vaesl.qtyship
         s-lnstkqtyship     = {&buf}vaesl.stkqtyship

         s-lnprodcost       = {&buf}vaesl.prodcost
         s-lnnetamt         = {&buf}vaesl.netamt

         s-lnarpvendno      = {&buf}vaesl.arpvendno
         s-lnarpprodline    = {&buf}vaesl.arpprodline
         s-lnarpwhse        = {&buf}vaesl.arpwhse

         s-lnorderalttype   = {&buf}vaesl.orderalttype
         s-lnorderaltno     = {&buf}vaesl.orderaltno
         s-lnlinealtno      = {&buf}vaesl.linealtno
         s-lnusagefl        = {&buf}vaesl.usagefl
         s-lnrushfl         = {&buf}vaesl.rushfl
         s-lnrushdesc       = if {&buf}vaesl.rushfl = yes then "r" else ""

         s-lnprodcat        = {&buf}vaesl.prodcat

         s-lnlaborflatrtfl  = if {&buf}vaesl.sctntype = "ex" then
                                  {&buf}vaesl.laborflatrtfl
                              else no
         s-lnlabortype      = if {&buf}vaesl.sctntype = "ex" then
                                  {&buf}vaesl.labortype
                              else ""
         s-lnlaborunits     = if {&buf}vaesl.sctntype = "ex" then
                                  {&buf}vaesl.laborunits
                              else 0

         s-lntimeactty      = {&buf}vaesl.timeactty
         s-lntimeworkdt     = {&buf}vaesl.timeworkdt

         s-lntimecomment    = {&buf}vaesl.timecomment
         s-lntimeslsrep     = {&buf}vaesl.timeslsrep
         s-lntimeslsrepnm   = ""

         v-icspstatus       = if s-lnnonstockty ne "n" and avail icsp
                                  then icsp.statustype
                              else ""

         {vaetslld.las &labordata      = "s-lnlabordata"
                       &icspstatustype = "v-icspstatus"
                       &file           = "{&buf}vaesl"
                       &sctntype       = "{&buf}vaesl.sctntype"
                       &timeworkdt     = "{&buf}vaesl.timeworkdt"}

         s-lndirectfl       = {&buf}vaesl.directfl

         s-lncubes          = {&buf}vaesl.cubes
         s-lnweight         = {&buf}vaesl.weight
         s-lnextcubes       = {&buf}vaesl.extcubes
         s-lnextweight      = {&buf}vaesl.extweight

         s-lnshpqtyoverfl   = {&buf}vaesl.shpqtyoverfl
         s-lncostoverfl     = {&buf}vaesl.costoverfl
         s-lnintermprodfl   = {&buf}vaesl.intermprodfl

         s-lnqtyunavail     = {&buf}vaesl.qtyunavail
         s-lnreasunavty     = {&buf}vaesl.reasunavty.

    assign
         v-icswstatus       = ""
         v-serlottype       = ""
         v-wmfl             = no
         s-countfl         = no

         v-dispqtyordfl     = if can-do("in,ii",{&buf}vaesl.sctntype) then
                                  yes
                              else
                              if {&buf}vaesl.sctntype = "ex" and
                              s-lnlaborflatrtfl = no then
                                  yes
                              else
                              if can-do("it,is",{&buf}vaesl.sctntype) and
                              v-icspstatus ne "l" then
                                  yes
                              else
                                  no

         s-netavail         = 0
         s-qtyonorder       = 0.

    if s-lntimeslsrep ne "" then do:
        {w-smsn.i s-lntimeslsrep no-lock}
        s-lntimeslsrepnm = if avail smsn then smsn.name else "".
    end.

/* SX 55 - not used in sx32 production anymore

    if can-do("it,",{&buf}vaesl.sctntype) and  
      (s-lnshipprod = "service repair" or  
       s-lnshipprod = "test time") then do:   
       if s-lnshipprod = "service repair" then 
        find last ztmk use-index k-ztmk
            where ztmk.cono = g-cono       and 
                  ztmk.ordertype = "sc"    and 
                  ztmk.orderno   = g-vano  and 
                  ztmk.ordersuf  = g-vasuf and 
                  ztmk.stagecd   = 10
                  no-lock no-error.
       else 
       if s-lnshipprod = "test time" then 
        find last ztmk use-index k-ztmk
            where ztmk.cono = g-cono       and 
                  ztmk.ordertype = "sc"    and 
                  ztmk.orderno   = g-vano  and 
                  ztmk.ordersuf  = g-vasuf and 
                  ztmk.stagecd   = 11
                  no-lock no-error.
        if avail ztmk then do: 
         assign s-lntimeslsrep = ztmk.techid.
         {w-smsn.i s-lntimeslsrep no-lock}
         assign s-lntimeslsrepnm = if avail smsn then smsn.name
                                    else "".
        end.           
    end. 

*** SX 55  */

    if {&buf}vaesl.nonstockty ne "n" then do:
        if avail icsw then do:

            {vaetsl.z99 &bef_netavl = "*"}

            /* from p-netavl.i */
            s-netavail = icsw.qtyonhand  -
                         icsw.qtyreservd -
                         icsw.qtycommit.

            {vaetsl.z99 &aft_netavl = "*"}


            assign
                 s-qtyonorder = icsw.qtyonorder
                 s-netavail   = if s-vatranstype = "va" and
                                {&buf}vaesl.statustype = true
                                    then s-netavail + s-lnstkqtyship
                                else s-netavail
                 s-netavail   = s-netavail / s-lnunitconv
                 s-netavail   = truncate(round(s-netavail,3),2)
                 s-qtyonorder = icsw.qtyonorder / s-lnunitconv
                 v-icswstatus = icsw.statustype
                 v-serlottype = icsw.serlottype
                 v-wmfl       = icsw.wmfl.
        end.
        if avail icsp and icsp.statustype = "l" then s-netavail = 0.
    end.

    assign
         o-lnstatustype    = {&buf}vaesl.statustype
         o-lndirectfl      = s-lndirectfl
         o-lnrushfl        = s-lnrushfl
         o-lnnonstockty    = s-lnnonstockty
         o-lnshipprod      = s-lnshipprod
         o-lnqtyship       = s-lnqtyship
         o-lnstkqtyord     = s-lnstkqtyord
         o-lnstkqtyship    = s-lnstkqtyship
         o-lnnetamt        = s-lnnetamt
         o-lnunit          = s-lnunit
         o-lnunitconv      = s-lnunitconv
         o-lnprodcost      = s-lnprodcost
         o-lntimeactty     = s-lntimeactty
         o-lnqtybasetotfl  = s-lnqtybasetotfl
         o-lnqtyneeded     = s-lnqtyneeded
         o-lnboseqno       = {&buf}vaesl.boseqno.

end.

/{&record-to-screen}*/

{vaetsl.z99 &user_bottom_las = "*"}

