/* poerr99.c99 */
/*si**** Custom ****************************************************************
  CLIENT  : 80281 SDI Operating Partners
  JOB     : 
  ANCHOR  : none
  VERSION : 8.0.003
  PURPOSE : created .c99 hook at top and bottom at customer request allowing
  
            *** NXTREND SHOULD NOT EDIT THIS FILE - FOR CLIENT CHANGES ONLY
  CHANGES :
    SI01 04/11/00 gc;  initial create                    
    
  Mode         Description
  -----------  -------------------------------------------------
  &user_top    at top of poerr99.p after def input params
  &user_bottom at bottom of poerr99.p

The input parameters read into poerr99.p are as follows:
    def input parameter v-mode      as c                    no-undo.
    def input parameter v-reportno  like poeral.reportno    no-undo.
    def input parameter v-lineno    like poeral.lineno      no-undo.

*******************************************************************************/

/{&user_top}*/

{poerrdd.ltt}
def buffer z-t-docdet for t-docdet.
def shared var g-cono     like sasc.cono                no-undo.    
def shared var g-currproc like sapb.currproc            no-undo.    
def shared var g-ourproc  like sassm.ourproc            no-undo.    
def shared var g-callproc like sassm.callproc           no-undo.    
def shared var g-postproc like sassm.callproc           no-undo.    
def shared var s-shipprod like icsw.prod                no-undo.  
def shared var p-whse    like icsd.whse                 no-undo.
def        var po-i      as   i format  "99"            no-undo. 
def        var noteline  as   c format "x(60)"          no-undo. 
def        var parm1     as   c format "x(30)"          no-undo. 
def        var parm2     as   c format "x(30)"          no-undo. 
def        var blankline as   c format "x(60)" init ""  no-undo. 


/* smaf  poerr option */

def shared var p-woprint       as logical        no-undo.  


/*** rebate records to be displayed after notes *****/ 
def var z-custno    like arsc.custno                    no-undo.
def var z-name      as char  format "x(24)"             no-undo.
def var z-qty       as int   format "zzzzzz9"           no-undo.
def var z-qty2      as int   format "zzzzzz9"           no-undo.
def var z-rebcost   like pder.rebcost                   no-undo.
/* SX 55 */
def var z-orderno   like oeeh.orderno                   no-undo.
def var z-fullord   as char  format "xxxxxxxxxxx"       no-undo.
def var z-lineno    like oeel.lineno                    no-undo.
def var z-vendno    like apsv.vendno                    no-undo.
def var z-spectype  as char  format  "x"                no-undo.
def var ctr1        as int                              no-undo.
def var ctr2        as int                              no-undo.
        
def buffer ix-icsw for icsw.

def frame f-comment 
  "**"            at 3 
  noteline        at 6 
  with down stream-io no-box no-labels no-underline width 132. 
 
def frame f-parameters 
  "**"            at 3 
  parm1           at 6 
  parm2           at 37 
  with down stream-io no-box no-labels no-underline width 132. 

def frame f-blank 
  blankline       at 1 
  with down stream-io no-box no-labels no-underline width 132. 

form    
    "Cust Nbr"      at 9
    "Name"          at 18
    " Qty Ord"      at 43
    "Qty Exhausted" at 52
    "Rebate Cost"   at 67
    "Oe #"          at 79
    "Line #"        at 90
    "O"             at 97
    " TransDt"      at 99
    "Reference"     at 109
    with frame z-head no-box no-labels width 150.

form  
   z-custno         at 5 
   z-name           at 18
   z-qty            at 44
   z-qty2           at 53
   z-rebcost        at 65
   z-fullord        at 79 
   z-lineno         at 92
   z-spectype       at 97
   pder.transdt     at 99
   pder.refer       at 109
   with frame z-pder no-label
        no-box width 150.
        
/* Purchasing Notes on VA - NonStock (Notes on RARR) */
/******************
if v-mode = "bexn" then do:
   find first poeral where                    
              poeral.cono     = g-cono and    
              poeral.reportno = v-reportno and
              poeral.lineno   = v-lineno      
              no-lock no-error.               
   if avail poeral then do:                                            
      find com where com.cono     = g-cono and
                     com.comtype  = "va" + string(poeral.seqaltno,"999") and
                     com.orderno  = poeral.orderaltno and
                     com.ordersuf = poeral.orderaltsuf and
                     com.lineno   = poeral.linealtno
                     no-lock no-error.
      if avail com then do:
      if com.noteln[8] = 
         "  The following parameters were entered with this note:"
        then do:  
         do po-i = 1 to 16: 
            if po-i <= 8 then do:  
               if com.noteln[po-i] <> "" then do:  
                  assign noteline = com.noteln[po-i].  
                  display noteline with frame f-comment.  
                  down with frame f-comment.  
               end. 
            end.
            else do:  
               assign parm1 = substring(com.noteln[po-i],1,30).
               assign parm2 = substring(com.noteln[po-i + 1],1,30).
               display parm1 parm2 with frame f-parameters.
               down with frame f-parameters.  
               assign po-i = po-i + 1.  
            end.  
         end.  
         display blankline with frame f-blank.  
      end.  
      end.
   end.
end.

if v-mode = "bexn" then do:
   find first poeral where                    
              poeral.cono     = g-cono and    
              poeral.reportno = v-reportno and
              poeral.lineno   = v-lineno      
              no-lock no-error.               
   if avail poeral then do:
      find com where com.cono     = g-cono and
                     com.comtype  = "kt" + string(poeral.seqaltno,"999") and
                     com.orderno  = poeral.orderaltno and
                     com.ordersuf = poeral.orderaltsuf and
                     com.lineno   = poeral.linealtno
                     no-lock no-error.
      if avail com then do:
      if com.noteln[8] = 
         "  The following parameters were entered with this note:"
        then do:  
         do po-i = 1 to 16: 
            if po-i <= 8 then do:  
               if com.noteln[po-i] <> "" then do:  
                  assign noteline = com.noteln[po-i].  
                  display noteline with frame f-comment.  
                  down with frame f-comment.  
               end. 
            end.
            else do:  
               assign parm1 = substring(com.noteln[po-i],1,30).
               assign parm2 = substring(com.noteln[po-i + 1],1,30).
               display parm1 parm2 with frame f-parameters.
               down with frame f-parameters.  
               assign po-i = po-i + 1.  
            end.  
         end.  
         display blankline with frame f-blank.  
      end.
      end.
   end.
end.
*********************/

if v-mode = "bexn" and not avail com then 
  do:
  find com use-index k-com where com.cono     = g-cono     and  
                                 com.comtype  = "xp"       and 
                                 com.orderno  = v-reportno and 
                                 com.lineno   = v-lineno   and
                                 com.noteln[8] begins "  The following"
                                 no-lock no-error.
  if avail com then 
    do:
    if com.noteln[8] = 
      "  The following parameters were entered with this note:" then 
      do:  
      do po-i = 1 to 16: 
        if po-i <= 8 then 
          do:  
          if com.noteln[po-i] <> "" then 
            do:  
            assign noteline = com.noteln[po-i].  
            display noteline with frame f-comment.  
            down with frame f-comment.  
          end. 
        end.
        else 
          do:  
          assign parm1 = substring(com.noteln[po-i],1,30).
          assign parm2 = substring(com.noteln[po-i + 1],1,30).
          display parm1 parm2 with frame f-parameters.
          down with frame f-parameters.  
          assign po-i = po-i + 1.  
        end.  
      end. /* do 1 to 16 */
      display blankline with frame f-blank.  
    end. /* com.noteln[8] has comments */
  end. /* if avail com */
  else
    do:
    /* Check to see if component CTRL-A notes exist */
    find first poeral where                    
               poeral.cono     = g-cono and    
               poeral.reportno = v-reportno and
               poeral.lineno   = v-lineno      
               no-lock no-error.               
    if avail poeral then 
      do:
      find com where com.cono     = g-cono and
                     com.comtype  = "kt" + string(poeral.seqaltno,"999") and
                     com.orderno  = poeral.orderaltno and
                     com.ordersuf = poeral.orderaltsuf and
                     com.lineno   = poeral.linealtno
                     no-lock no-error.
      if avail com then 
        do:
        if com.noteln[8] = 
           "  The following parameters were entered with this note:" then
          do:
          do po-i = 1 to 16: 
            if po-i <= 8 then 
              do:
              if com.noteln[po-i] <> "" then 
                do:  
                assign noteline = com.noteln[po-i].  
                display noteline with frame f-comment.  
                down with frame f-comment.  
              end. 
            end. /* if po-i <= 8 */
            else 
              do:  
              assign parm1 = substring(com.noteln[po-i],1,30).
              assign parm2 = substring(com.noteln[po-i + 1],1,30).
              display parm1 parm2 with frame f-parameters.
              down with frame f-parameters.  
              assign po-i = po-i + 1.  
            end.  
          end. /* do po-i = 1 to 16 */
          display blankline with frame f-blank.  
        end. /* com.noteln[8] has comments */
      end. /* avail com */
    end. /* avail poeral */
  end. /* not avail com - check for component notes */
end. /* if v-mode = "bexn" */

if v-mode = "bexn" or v-mode = "bexr" then
  do:
    
      if v-mode = "bexn" then 
         find first z-t-docdet where 
                    z-t-docdet.reportno = v-reportno and 
                    z-t-docdet.shipprod = s-shipprod and 
                    z-t-docdet.rrarlineno = v-lineno
              no-lock no-error.

     if v-mode = "bexr" then 
     find first poeral where
                poeral.cono     = g-cono and 
                poeral.reportno = v-reportno and 
                poeral.lineno   = v-lineno
                no-lock no-error.
     else 
     if v-mode = "bexn" then 
     find first poeral where
                poeral.cono       = g-cono and 
                poeral.reportno   = v-reportno and 
                poeral.shipprod   = s-shipprod and
                poeral.orderaltno = z-t-docdet.orderno and 
                poeral.lineno     = z-t-docdet.rrarlineno
                no-lock no-error.
      
      if avail poeral then  
        find first icsw use-index k-whse where 
             icsw.cono = g-cono and 
             icsw.whse = poeral.whse and 
             icsw.prod = poeral.shipprod
             no-lock no-error.
      if not avail icsw then 
         leave.

     if icsw.arpvendno ne 0 and icsw.arptype = "v" then 
        assign z-vendno = icsw.arpvendno.

     assign ctr1 = 0
            ctr2 = 0.         
            
     if poeral.orderaltno ne 0 then        
     rebprt:
     for each pder use-index k-pder
        where pder.cono     = g-cono + 500      and  
              pder.orderno  = poeral.orderaltno and 
              pder.ordersuf = 00                and 
              pder.lineno   = poeral.linealtno  and  
              pder.shipprod = icsw.prod         and 
              pder.vendno   = z-vendno          and 
              pder.xxde2[1] > 0 
              no-lock:
        if avail pder then 
          do:
             find first oeeh where 
                        oeeh.cono    = g-cono and 
                        oeeh.orderno = poeral.orderaltno
                        no-lock no-error.
             if avail oeeh and 
               (oeeh.transtype = "rm" or 
                oeeh.transtype = "cr" or 
                oeeh.transtype = "qu") then 
                next rebprt.
             else 
             if not avail oeeh then 
                next rebprt.
             if pder.statustype = "x" then 
                 next rebprt.
             find arsc where
                  arsc.cono   = g-cono and 
                  arsc.custno = pder.custno
                  no-lock no-error.
             if avail arsc then 
                assign z-name = arsc.name. 
             else 
                assign z-name = "No Customer Name Available". 
           
             if pder.seqno = 0 then
               do:
                  find first oeel where 
                             oeel.cono     = g-cono and
                             oeel.orderno  = pder.orderno and
                             oeel.lineno   = pder.lineno 
                             no-lock no-error.
               if not avail oeel then 
                  next rebprt.
               end.

             assign z-custno   = pder.custno
                    z-qty      = pder.xxde2[1]
                    z-qty2     = pder.xxde2[2]
                    z-rebcost  = icsw.stndcost - pder.rebateamt
                    /* SX 55 */
                    z-fullord  = string(pder.orderno,"zz999999") + "-" +  
                                 string(pder.ordersuf,"99") 
                    z-spectype = (if oeel.specnstype= "s" then 
                                  "*" 
                                 else 
                                  " ") 
                    z-lineno   = pder.lineno.
             
             if p-whse ne "" and oeel.whse = p-whse then 
              do:
                ctr1 = ctr1 + 1.
                if ctr1 = 1 then 
                   display with frame z-head.
                display z-custno 
                        z-name
                        z-qty    
                        z-qty2
                        z-rebcost
                        z-fullord
                        z-lineno
                        z-spectype
                        pder.transdt
                        pder.refer
                        with frame z-pder.
                   down with frame z-pder.
              end.
             else
             if poeral.whse = oeel.whse then 
              do:
                ctr1 = ctr1 + 1.
                if ctr1 = 1 then 
                   display with frame z-head.
                display z-custno 
                        z-name
                        z-qty    
                        z-qty2
                        z-rebcost
                        z-fullord
                        z-lineno
                        z-spectype
                        pder.transdt
                        pder.refer
                        with frame z-pder.
                   down with frame z-pder.
              end.
           end.      
     end.   /* for each  */
     else
     rebprt2:
     for each pder  use-index k-contractno  /* speed fix */
        where pder.cono     = g-cono + 500   and  
              pder.statustype = "e"    and
              pder.rebatecd  = "s" and
              pder.shipprod = icsw.prod      and 
              pder.vendno   = z-vendno       and 
              pder.xxde2[1] > 0 
              no-lock:
        if avail pder then 
          do:
             find first oeeh where 
                        oeeh.cono    = g-cono and 
                        oeeh.orderno = pder.orderno
                        no-lock no-error.
             if avail oeeh and 
               (oeeh.transtype = "rm" or 
                oeeh.transtype = "cr" or 
                oeeh.transtype = "qu") then 
                next rebprt2.
             else 
             if not avail oeeh then 
                next rebprt2.
             if pder.statustype = "x" then 
                 next rebprt2.
             find arsc where
                  arsc.cono   = g-cono and 
                  arsc.custno = pder.custno
                  no-lock no-error.
             if avail arsc then 
                assign z-name = arsc.name. 
             else 
                assign z-name = "No Customer Name Available". 
           
             if pder.seqno = 0 then
               do:
               find first oeel where
                          oeel.cono     = g-cono and
                          oeel.orderno  = pder.orderno and
                          oeel.lineno   = pder.lineno 
                          no-lock no-error.
               if not avail oeel then 
                  next.
               end.

             find ix-icsw where ix-icsw.cono = g-cono and 
                                ix-icsw.whse = pder.whse and
                                ix-icsw.prod = pder.shipprod no-lock no-error.
             assign z-custno   = pder.custno
                    z-qty      = pder.xxde2[1]
                    z-qty2     = pder.xxde2[2]
                    z-rebcost  = (if avail ix-icsw then
                                    ix-icsw.stndcost
                                  else
                                    icsw.stndcost) - pder.rebateamt
                    /* SX 55 */
                    z-fullord  = string(pder.orderno,"zz999999") + "-" +  
                                 string(pder.ordersuf,"99") 
                    z-spectype = (if oeel.specnstype= "s" then 
                                  "*" 
                                 else 
                                  " ") 
                    z-lineno   = pder.lineno.
             
             if p-whse ne "" and oeel.whse = p-whse then 
              do:
                ctr2 = ctr2 + 1.
                if ctr2 = 1 then 
                   display with frame z-head.
                display z-custno 
                        z-name
                        z-qty    
                        z-qty2
                        z-rebcost
                        z-fullord
                        z-lineno
                        z-spectype
                        pder.transdt
                        pder.refer
                        with frame z-pder.
                   down with frame z-pder.
              end.
             else 
             if poeral.whse = oeel.whse then 
              do:
                ctr2 = ctr2 + 1.
                if ctr2 = 1 then 
                   display with frame z-head.
                display z-custno 
                        z-name
                        z-qty    
                        z-qty2
                        z-rebcost
                        z-fullord
                        z-lineno
                        z-spectype
                        pder.transdt
                        pder.refer
                        with frame z-pder.
                   down with frame z-pder.
              end.
          end.      
     end.   /* for each  */

     display blankline with frame f-blank.
end. /* if v-mode */

/* smaf */


if p-woprint = yes then do:

/*  message "p-woprint" p-woprint. pause.  */
 
 run kppoerr.p (input icsw.whse,
                input icsw.prod).
        

end. /* p-woprint */

/{&user_top}*/

/{&user_bottom}*/
/{&user_bottom}*/
