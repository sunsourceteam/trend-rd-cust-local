{g-all.i}
define input parameter ip-cono    like poel.cono no-undo.
define input parameter ip-poelid  as recid       no-undo.
define input parameter ip-expdate as date        no-undo.
define var   s-expdtfl            as logical     no-undo.
 
define var zsdiconfirm   as logical     no-undo.
define var zsdileveltxt  as char        no-undo.
define var v-expdate     as date        no-undo.
define var v-lpromdt     as date        no-undo.
define var v-lexpdt      as date        no-undo.
define var v-lexpdth     as date        no-undo. 

/*EN-1808*/     
define var v-1019fl      as logical     no-undo.
find poel where recid(poel) = ip-poelid no-lock no-error.
if not avail poel then 
  return.

for each poelo use-index k-poelo where
         poelo.cono        = poel.cono       and
         poelo.ordertype   = "o"             and
         poelo.pono        = poel.pono       and
/*       poelo.posuf       = poel.posuf   and        */
         poelo.lineno      = poel.lineno     no-lock:
  find oeeh where oeeh.cono = poel.cono and
                  oeeh.orderno = poelo.orderaltno and 
                  oeeh.ordersuf = poelo.orderaltsuf and
                  oeeh.stagecd > 0  and
                  oeeh.stagecd < 3  no-lock no-error.       /* ? */
  if not avail oeeh then 
    next.

  find arsc where 
       arsc.cono = poel.cono and  
       arsc.custno = oeeh.custno no-lock no-error.
  if not avail arsc then 
    next.
  assign s-expdtfl  = if substr(arsc.user20,4,3) = " " then
                        no
                      else
                        logical(substr(arsc.user20,4,3)).
  if s-expdtfl = yes then
    next.
  
  
  
  
  
  find oeel where oeel.cono = poel.cono and
                  oeel.orderno   = poelo.orderaltno  and 
                  oeel.ordersuf  = poelo.orderaltsuf and
                  oeel.lineno    = poelo.linealtno   and 
                  oeel.specnstype <> "l"             and
                  oeel.specnstype <> "c"             and
                  oeel.kitfl = no                    no-error.
  if not avail oeel then
    next.
  assign
    v-lpromdt  = date(substring(oeel.user3,1,10))
    v-lexpdt   = date(substring(oeel.user3,11,10)).


  assign v-expdate = ip-expdate.

  find first zsastz where
             zsastz.cono = poel.cono            and
             zsastz.labelfl = false             and
             zsastz.codeiden = "AcknTransitDays" and
             zsastz.primarykey = (if poel.transtype = "DO" then
                                    "DO"
                                  else
                                    "PO")
  no-lock no-error.              
 
  if avail zsastz then do:
    assign v-expdate = v-expdate + int(zsastz.codeval[1]).
  end.     

  /* replaced with zsdiweekendh below
  run zsdiweekend.p (input-output v-expdate,    /*not used*/
                     input poel.whse,
                     input poel.cono).
  */
  assign v-1019fl = no.
  run zsdiweekendh (input-output v-expdate,
                    input-output v-1019fl,
                    input        oeeh.whse,
                    input        g-cono,
                    input        oeeh.divno).   

  
  if v-expdate < oeeh.reqshipdt then
    assign v-expdate = oeeh.reqshipdt.
    
  if v-expdate = v-lexpdt then
    return.


  run zsdioeetlog.p (input recid(oeeh),
                     input recid(oeel),
                     input oeel.netamt,
                     input oeel.qtyord,
                     input oeel.price,
                     input oeel.discamt,
                     input oeel.disctype,
                     input oeeh.reqshipdt,
                     input g-operinit,
                     input oeel.specnstype,
                     input oeel.lostbusty,
                     input (if oeel.slsrepin = "" then
                              oeeh.slsrepin
                            else
                              oeel.slsrepin),
                     input (if oeel.slsrepout = "" then 
                              oeeh.slsrepout
                            else
                              oeel.slsrepout),
                
                     input "logackn",
                     output zsdiconfirm,
                     output zsdileveltxt,
                     input "ackn",
                     input "Expected Date from Ackn Update",
                     input " ", /* authper  */
                     input "U", 
                     input v-lpromdt, 
                     input v-expdate ). 
  
  overlay(oeel.user3,11,10) = string(v-expdate,"99/99/9999").

end.