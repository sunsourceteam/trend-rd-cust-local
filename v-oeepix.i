/* v-oeepi.i 1.1 01/03/98 */
/* v-oeepi.i 1.7 4/28/93 */
/*h*****************************************************************************
  PROCEDURE            : v-oeepi
  DESCRIPTION          : Order Entry Printing Validation
  USED ONLY ONCE?      : no
  AUTHOR               : mkb
  DATE LAST MODIFIED   : 06/19/92
  CHANGES MADE AND DATE: tb 6889 06/06/92 mwb; use tendamt instead of 3
   payamts
 tb 6756 06/13/92 mjm; added oerd if not list
 tb 6756 06/18/92 mkb; do not check for minimum if
   ourproc = oerd
 tb 2811 06/16/92 mkb; Print unprinted invoices
 tb 4531 06/19/92 mkb; move checks to v-oeepi.i from
   oeepi.p the above tb's explain the reason for
   the checks listed below
 02/04/93 mwb; TB# 9760; added total amount due = 0
       edit to the oerd range print.
 04/28/93 rs; TB# 6404 Negative addon on RM update
  incorrect
*******************************************************************************/
/* message "running this v-include". pause.  */
v-printokfl  =

    if g-ourproc = "oeepi" then
if not avail arsc then no
else if not avail {&b}oeeh then no
else if p-printunp = yes and sapbo.reprintfl = yes then no
/*
else if (( p-shipfl and {&b}oeeh.stagecd < 3) or
 (not p-shipfl and {&b}oeeh.stagecd < 2))
    and not p-selectfl then no
*/    
/*
else if (({&b}oeeh.totinvamt - {&b}oeeh.tendamt) *
    (if can-do("rm,cr,cs,so",{&b}oeeh.transtype) and
    ({&b}oeeh.totinvamt - {&b}oeeh.tendamt) < 0 then  -1 else 1))
    < b-sasc.oemninvamt then no
*/    
else if {&b}oeeh.transtype = "cs" and
    ({&b}oeeh.totinvamt - {&b}oeeh.tendamt) = 0 then no
else yes

    else if g-ourproc = "oerd" and p-selectfl = no then
if not avail {&b}oeeh then no
else if not avail arsc then no
else if p-quote = "q" and {&b}oeeh.transtype <> "qu" then no
else if (({&b}oeeh.totinvamt - {&b}oeeh.tendamt) *
    (if can-do("cr,cs,so",{&b}oeeh.transtype) and
    ({&b}oeeh.totinvamt - {&b}oeeh.tendamt) < 0 then  -1 else 1))
    < b-sasc.oemninvamt then no
else if {&b}oeeh.transtype = "cs" and
    ({&b}oeeh.totinvamt - {&b}oeeh.tendamt) = 0 then no
else yes
    else yes.

