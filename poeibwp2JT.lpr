/* poeibwpa2JT.lpr 1.1 01/06/99 */
/*h****************************************************************************
  INCLUDE      : poeibwpa2JT.lpr
  DESCRIPTION  : PO Batch Receiving Process - Batch Files
  USED ONCE?   : yes
  AUTHOR       : mwb
  DATE WRITTEN : 01/06/99
  CHANGES MADE :
    12/02/2008    dkt createLabel update
    05/02/2006    dkt
    01/06/99 mwb; TB# 24049 PV: Common code use for GUI interface
    04/09/99 sbr; TB# 25722 Open initials not getting set on POEH
******************************************************************************/

/*e Select those PO Batch Receiving POs (poehb records) that have been flagged
    as received (poehb.receivefl = yes), for the processing whse (v-batchwhse)
    that have been flagged for updating (poehb.updtfl = yes).  Join them with
    the corrosponding POEH record, the APSV record, and those PO Batch
    Receiving PO Line Itemsm (poelb records) that have been received.  Join this
    to the POEL records themselfs.  Those compound for each will a poehb, POEH,
    poelb, and each poelb records.  This means that the same POEH and POEL
    record may be read multiple times since there can be multiple poelb records
    for a single POEL record. */

/* dkt change label printing */
def var extraData as char no-undo.
def var xxl3Flag as logical no-undo.
def var cQty as char no-undo.
def buffer b-zzbin for zzbin.
def buffer b-icsw  for icsw.

def var binprior as integer no-undo.
def var delFlag  as logical no-undo.
def var resetZzbinFlag as logical no-undo.
def var assignedcode like wmsb.assigncode no-undo.

if s-ibprinter ne "" /* and NOT ( s-flglbl = no and poelb.xxl3 = no ) */
then
  run PrintLabelPartOne.p.
/* dkt */

btchloop:
for each poehb exclusive-lock no-prefetch use-index
  {&poehbforeach},
  first poeh exclusive-lock use-index k-poeh where
        poeh.cono        = g-cono and
        poeh.pono        = poehb.pono and
        poeh.posuf       = poehb.posuf and
/*      poeh.openinit    = "" and */
        can-do("po,do,br,ac,rm",poeh.transtype) and
        poeh.stagecd     > 0 and
        poeh.stagecd     < 5,

  first apsv no-lock use-index k-apsv where
        apsv.cono        = g-cono and
        apsv.vendno      = poeh.vendno,

  each poelb share-lock use-index k-poelb where
       poelb.cono        = g-cono    and
       poelb.shipmentid  = poehb.shipmentid and
       poelb.pono        = poehb.pono       and
       poelb.posuf       = poehb.posuf      and
       poelb.qtyrcv      ne 0,

  first poel exclusive-lock use-index k-poel where
        poel.cono        = g-cono and
        poel.pono        = poelb.pono and
        poel.posuf       = poelb.posuf and
        poel.lineno      = poelb.lineno and
        poel.statustype  = "a"
break by poeh.cono
      by poeh.pono
      by poeh.posuf
      by poel.lineno:

    /*e Remember: There can be multiple poelb records for a given poel record,
                  so this for each could read the same poel record more than
                  once.  */
  if v-debugfl = yes then do:
    message "poel.pono: " poel.pono
            " suf: " poel.posuf
            " ln#: " poel.lineno.
    pause.
    message "poelb.pono: " poelb.pono
            " suf: " poelb.posuf
            " ln#: " poelb.lineno
            " ID: " poelb.shipmentid
            " rcv: " poelb.qtyrcv.
    pause.
  end.

 
  if s-ibprinter ne "" then do: 
    extraData = g-operinits + "," + trim(STRING(poelb.qtyrcv,">>>>>>>>>9"))
      + "," + poelb.shipmentid.
      
    cQty = trim(string(poelb.user7,">>9")).
    
    if s-flglbl = yes and cQty ne "0" then do:
      run RFLoop.
 
      run createLabel(input s-ibprinter, "rfrcv.lbl", "Product",
                      input "ICSP", "ICSW", "POEI",
                      input poelb.shipprod, g-whse,
                      input poelb.pono, poelb.posuf, poelb.lineno,
                      input extraData,
                      input cQty,
                      input zx-l,
                      input "", input "prod-txdata", input "").
      end.                 
    if s-flglbl then poelb.user7 = 0.
  end. /* if s-ibprinter */
   
  /* fix temporary ZZbins and set product into a bin if not done earlier */
  
  /**
  find first zzbin where zzbin.cono = g-cono + 500 and
  zzbin.prod = poelb.shipprod and zzbin.whse = poehb.whse
  exclusive-lock no-error.
  **/
  find first zzbin where
             zzbin.cono = poelb.cono /* 501 */    and             
             zzbin.whse = poehb.whse and 
             zzbin.prod = poelb.shipprod and
             zzbin.transproc begins "PO"+ STRING(poehb.pono) and
   /*  + "-" + STRING(poehb.posuf) */
             zzbin.binloc ne "" no-error.        

  
  if avail zzbin then do:
    delFlag = no.
    find wmsb where 
         wmsb.cono = g-cono + 500 and 
         wmsb.whse = poehb.whse and
         wmsb.binloc = zzbin.binloc no-error.
    find wmsbp where 
         wmsbp.cono = g-cono + 500 and 
         wmsbp.whse = poehb.whse  and
         wmsbp.binloc = zzbin.binloc and 
         wmsbp.prod = poelb.shipprod  no-lock no-error.
    if avail wmsb then do:
      if avail wmsbp then do:
        delFlag = yes.
/* TAH 022807 done later already and the logic makes this not a good place 
        delete zzbin. /* both tables are set, icsw should be set */
*/        
      end.
      else do:
        /* have bin, and it is empty */
        /* what do I have already, if any? */
        binprior = 0.
        resetZzbinFlag = no.
        find b-icsw where 
             b-icsw.cono = g-cono and
             b-icsw.whse = wmsb.whse and 
             b-icsw.prod = poelb.shipprod no-error.
        if avail b-icsw then do:
          assignedcode = b-icsw.statustype.
          if b-icsw.binloc1 = "" or b-icsw.binloc1 = zzbin.binloc
            or b-icsw.binloc1 = "New Part" then do:
            binprior = 1.
            if b-icsw.binloc1 = "" or b-icsw.binloc1 = "New Part" 
              then b-icsw.binloc1 = zzbin.binloc.
            delFlag = yes.
          end.
          else if b-icsw.binloc2 = "" or b-icsw.binloc2 = zzbin.binloc
            or b-icsw.binloc2 = "New Part" then do:
            binprior = 2.
            if b-icsw.binloc2 = "" or b-icsw.binloc2 = "New Part"
              then b-icsw.binloc2 = zzbin.binloc.
            delFlag = yes.
          end.
          else do:
            binprior = 2.
            delFlag = no.
            resetZzbinFlag = yes.
          end.
        end. /* if avail */
        else do:
          assignedcode = "N".
          resetZzbinFlag = yes.
        end.
        if resetZzbinFlag then do: 
          /* have binloc1 and 2 already
             OR no icsw record */
          delFlag = no.
          find first b-zzbin where 
                     b-zzbin.cono = g-cono and
                     b-zzbin.whse = poehb.whse and 
                     b-zzbin.prod = poelb.shipprod and
                     b-zzbin.binloc = zzbin.binloc no-lock no-error.
          if avail b-zzbin then do:
            delFlag = yes. /* delete the g-cono + 500 version */
          end.
          else do:
            /*thisRecId = recid(b-zzbin).*/
            for each b-zzbin where 
                     b-zzbin.cono = g-cono and
                     b-zzbin.whse = poehb.whse and 
                     b-zzbin.prod = poelb.shipprod no-lock:
              binprior = binprior + 1.
            end.
            /* find a priority and change zzbin to it */
            binprior = binprior + 1.
            if binprior > 2 then binprior = 9.
            /**
            release b-zzbin.
            find first b-zzbin where recid(b-zzbin) = thisRecId
            exclusive-lock no-error.
            **/
            zzbin.cono = g-cono. /*  + 500. */
            wmsb.priority = binprior.
          end.
        end.
        create wmsbp.
        assign wmsbp.cono = g-cono + 500
               wmsbp.prod = poelb.shipprod
               wmsbp.binloc = wmsb.binloc
               wmsbp.whse   = wmsb.whse.
        {t-all.i wmsbp}
        assign        
               wmsbp.transproc = "Poeibwpa2.lpr"
               wmsb.statuscode = "A"  
               wmsb.priority   = binprior   
               wmsb.assigncode = assignedcode.  
        /* end.   */
        if avail b-icsw then do:
          if binprior = 1 then 
            b-icsw.binloc1 = wmsb.binloc.
          if binprior = 2 then 
            b-icsw.binloc2 = wmsb.binloc.
        end.
      end. /* wmsbp not avail */
      if delFlag then
        delete zzbin. /* once you are done copying it to icsw and wmsbp get rid
                         of it, unless converted to priority 3 or more */
      else do:
        /* could it already exist?? If set in ICSZB before update... */
        find first b-zzbin where 
                   b-zzbin.cono = g-cono and         
                   b-zzbin.whse = poehb.whse and 
                   b-zzbin.prod = poelb.shipprod and
                   b-zzbin.binloc = zzbin.binloc no-lock no-error.        
        if not avail b-zzbin then
          zzbin.cono = g-cono.
        else
          delete zzbin. /* don't need a 501 AND a g-cono, just g-cono */
      end.
    end. /* avail wmsb */
    else delete zzbin.
  end. /* avail zzbin */

  
  
  /* Beginning processing of a given purchase order. */
  if first-of(poeh.posuf) then do:
    assign
      g-pono         = poeh.pono
      g-posuf        = poeh.posuf
      g-whse         = poeh.whse
      v-whse         = poeh.whse
      v-totlnwght    = 0
      v-totlncubes   = 0
      v-totqtyrcv    = 0.

    if v-debugfl = yes then do:
      message "in poeibwp.p, first of po#: " poeh.pono " suf: " poeh.posuf.
      pause.
    end.

    if not can-find (first poei use-index k-lineno where
                           poei.cono   = g-cono   and
                           poei.jrnlno = g-jrnlno and
                           poei.pono   = g-pono   and
                           poei.posuf  = g-posuf)
      then do:
            /*e Force variable "Foce entry of qty received during entry"
                to be "yes" (v-poqtyrcvfl).  This is so the qty received will
                be zero, then it can be loaded based on poelb data.  The poeiw.p
                procedure will also zero out some poeh fields.
                Also force variable "Default to 'all' pricing during receiving"
                to be "no" (v-poallfl).  This is so the poei.price will be set
                to be the poel.price in poeiw.p so if any unit changes exist,
                the poei.price can be adjusted.  */
            /*tb 25722 04/09/99 sbr; Added "poeh.openinit = g-operinit" to the
                 assign statement. */
            assign
                 poeh.actionty    = if poehb.actionty ne "" then
                                        poehb.actionty
                                    else poeh.actionty
                 poeh.receiptdt   = poehb.receiptdt  /*  Today    JTjournal */
                 poeh.jrnlno      = g-jrnlno
                 poeh.setno       = g-setno
                 poeh.laststagecd = if poeh.stagecd < 5 then
                                    poeh.stagecd else poeh.laststagecd
                 o-poqtyrcvfl     = v-poqtyrcvfl
                 v-poqtyrcvfl     = yes
                 o-poallfl        = v-poallfl
                 v-poallfl        = no
                 poeh.stagecd     = 5
                 poeh.openinit    = g-operinit.

            {t-all.i poeh}

            if v-debugfl = yes then do:
                message "in poeibwp.p, before poeiw.p run". pause.
                message "poeh.nosnlots: " poeh.nosnlots. pause.
            end.

            run poeiw.p("w").

            if v-debugfl = yes then do:
                message "in poeibwp.p, after poeiw.p run". pause.
                message "poeh.nosnlots: " poeh.nosnlots. pause.
                message "poeh.totqtyrcv: " poeh.totqtyrcv. pause.
            end.

            /*e Re-instate the "force receiving qtys" and "default to 'all'
                pricing" variable to be the original POAO setting. */
            assign
                 v-poqtyrcvfl = o-poqtyrcvfl
                 v-poallfl    = o-poallfl.

            /*e For an "RM" type PO, the poeiw.p program will load the
                qtyrcv to be the qty ordered.  This is not wanted since the
                setting of the qtyrcv needs to be controlled by the POELB
                records.  In this case, the POEI records will be read and
                zeroed out. */
            if poeh.transtype = "rm" then do for b-poei:
                for each b-poei use-index k-lineno where
                         b-poei.cono     = g-cono and
                         b-poei.jrnlno   = g-jrnlno and
                         b-poei.pono     = poeh.pono and
                         b-poei.posuf    = poeh.posuf
                exclusive-lock:
                    assign
                         b-poei.qtyrcv    = 0
                         b-poei.stkqtyrcv = 0
                         b-poei.nosnlots  = 0.
                end.
                assign
                     poeh.totqtyrcv    = 0
                     poeh.totqtyrcvb   = 0
                     poeh.totcubes     = 0
                     poeh.totweight    = 0.
            end.
        end.
    end.

    /*o Beginning processing for a given PO line item */
    if first-of(poel.lineno) then do for icsp, icss:

        /*e Need to examine all poelb records for the PO line item being
            processed to see if all poelb records have the same unit or not. */
        {w-icsp.i poel.shipprod no-lock}

        /*tb 7241 (5.2) 02/18/97 mms; Spec8.0 Changes for PO Module Level;
             Added find and scoping above in ICSS; removed local assignments
             for variables as they are all performed in the include. */
        /*d Base Special Price and Cost information off the ICSS attached to
            the POEL record */
        {icss.gfi
            &prod        = poel.shipprod
            &icspecrecno = poel.icspecrecno
            &lock        = "no"}

        assign
             {speccost.gas &com  = "/*"}
             v-sameunitfl    = yes
             v-sameunitfstfl = yes
             v-sameunit      = ""
             v-unit          = poelb.unit
             v-unitconv      = poelb.unitconv.

        if poel.nonstockty ne "n" then
        unitloop:
        for each b-poelb no-lock use-index k-pono where
                 b-poelb.cono        = g-cono  and
                 b-poelb.pono        = poel.pono      and
                 b-poelb.posuf       = poel.posuf     and
                 b-poelb.lineno      = poel.lineno    and
                 b-poelb.qtyrcv      ne 0,  
            first b-poehb no-lock use-index k-poehb where
                  b-poehb.cono       = g-cono     and
                  b-poehb.shipmentid = b-poelb.shipmentid and
                  b-poehb.pono       = b-poelb.pono       and
                  b-poehb.posuf      = b-poelb.posuf      and
                  {&b-poehbedit}
                  ( (b-poehb.openinit = v-openinit) or
                    (b-poehb.openinit = "poei") ):
           
            if v-sameunitfstfl = yes then
                assign
                     v-sameunitfstfl = no
                     v-sameunit      = b-poelb.unit
                     v-unit          = b-poelb.unit
                     v-unitconv      = b-poelb.unitconv.
            else
            if b-poelb.unit ne v-sameunit then do:
                assign
                     v-sameunitfl = no.
                leave unitloop.
            end.
        end.

        /*e If there are different units across the POELB records for the
            same line#, return to the stocking unit. */
        if v-sameunitfl = no then
            assign
                 v-unit     = if avail icsp then icsp.unitstock else v-unit
                 v-unitconv = 1.
    end.

    /*tb 19528 11/08/95 mtt; Develop PO Batch Receiving Function */
    /*e Update each POEHB record to have the open initials.  Each POEHB
        record for the pono/posuf needs to be updated, although this assignment
        will be done for each POELB record.  This is a little inefficent, but
        there is no break by clause on any POEHB field. */
    assign poehb.openinit = "poei"
           poehb.user4 = "Delete".

    /*o Each poelb record read - read the POEI record to be updated. */
    find first poei use-index k-poei where
               poei.cono       = g-cono and
               poei.jrnlno     = g-jrnlno and
               poei.pono       = poel.pono and
               poei.posuf      = poel.posuf and
               poei.lineno     = poel.lineno
    exclusive-lock no-error.

    /*e There should always be a POEI record for the poelb record being
        processed, since the poelb record would not have been read if there
        was not a corrosponding POEL record and during the first record
        processed for the PO, the poeiw.p program was run to create one POEI
        record for each POEL. */
    if avail poei then do:
        /*e Check for substitution/supersed condition */
        if poelb.shipprod ne poel.shipprod then do for icsp, icsw:

            {w-icsp.i poelb.shipprod no-lock}
            {w-icsw.i poelb.shipprod poeh.whse no-lock}

            assign
                 poei.cancelfl = (if poei.cancelfl = false and
                                     poelb.cancelfl = true then
                                    true
                                  else
                                    false) 
                 poei.shipprod  = poelb.shipprod
                 poei.reqprod   = poelb.reqprod
                 poei.wmfl      = if avail icsw then icsw.wmfl else poei.wmfl
                 poei.notesfl   = if poei.nonstockty = "" then
                                      if avail icsp then icsp.notesfl
                                      else poei.notesfl
                                  else poei.notesfl
                 poei.vendprod  = if poei.nonstockty ne "" then poei.shipprod
                                  else
                                  if avail icsw and icsw.vendprod ne "" then
                                      icsw.vendprod
                                  else poei.shipprod
                 poei.serlottype = if poei.nonstockty = "n" then ""
                                   else
                                   if avail icsw and
                                   ( (icsw.serlottype = "l") or
                                     (icsw.serlottype = "s" and
                                      v-icsnpofl = yes) )
                                       then icsw.serlottype
                                   else ""
                 poel.origcubes  = if avail icsp then icsp.cubes else
                                   poel.origcubes
                 poel.origweight = if avail icsp then icsp.weight else
                                   poel.origweight.
        end.

        /*e Update poei fields for each poelb record - could be multiple poelb
            records for the same poei record. */
        assign
                 poei.cancelfl = (if poei.cancelfl = false and
                                     poelb.cancelfl = true then
                                    true
                                  else
                                    false). 

        if v-sameunitfl = yes then
            assign
                  poei.qtyrcv    = poei.qtyrcv    + poelb.qtyrcv
                  poei.stkqtyrcv = poei.stkqtyrcv + poelb.stkqtyrcv.
        else
            assign
                  poei.stkqtyrcv = poei.stkqtyrcv + poelb.stkqtyrcv
                  poei.qtyrcv    = poei.stkqtyrcv.

        /*e When the poeiw.p program was run to create the POEI records,
            the "v-poqtyrcvfl" was set to yes.  This would have caused the
            POEI records to not have the poei.nosnlots field set at all (would
            have been zero) and the poeiw.p program would have zeroed out the
            poeh.nosnlots field. */
        assign
              poei.qtyunavail = poei.qtyunavail + poelb.qtyunavail
              poei.reasunavty = poelb.reasunavty
              poei.nosnlots   = if can-do("l,s",poei.serlottype) then
                                    poei.nosnlots + poelb.stkqtyrcv
                                else poei.nosnlots
              poeh.nosnlots   = if can-do("l,s",poei.serlottype) then
                                    poeh.nosnlots + poelb.stkqtyrcv
                                else poeh.nosnlots.


        /*o Last of processing for a given PO line item */
        if last-of(poel.lineno) then do:
            assign
                 poel.qtyrcv    = poei.qtyrcv
                 poel.stkqtyrcv = poei.stkqtyrcv
                 poel.weight    = poel.origweight * poel.stkqtyrcv
                 poel.cubes     = poel.origcubes  * poel.stkqtyrcv

                 poei.price     = if v-speccostty ne "" then poei.price
                                  else
                                  if v-unitconv ne poel.unitconv then
                                      poei.price /
                                      {unitconv.gas &decconv = poei.conv} *

                                      v-unitconv
                                  else
                                      poei.price
                 poei.unit      = v-unit
                 poei.conv      = v-unitconv
                 s-lineno       = poei.lineno

                 v-totlnwght  = v-totlnwght + poel.weight
                 v-totlncubes = v-totlncubes + poel.cubes
                 v-totqtyrcv  = v-totqtyrcv + poei.stkqtyrcv
                 o-nosnlots   = if can-do("s,l",poei.serlottype) then
                                    poei.nosnlots
                                else 0.

            if poei.serlottype = "s" then do:
                {p-nosn.i &ordertype = ""p""
                          &orderno   = g-pono
                          &ordersuf  = g-posuf
                          &com       = "/*"}
                poei.nosnlots = round(poei.stkqtyrcv - v-noassn,0).
            end.
            else if poei.serlottype = "l" then do:
                {p-nolots.i &seqno     = 0
                            &ordertype = ""p""
                            &orderno   = g-pono
                            &ordersuf  = g-posuf
                            &com       = "/*"}
                poei.nosnlots = round(poei.stkqtyrcv - v-noassn,2).
            end.

            assign
                poeh.nosnlots  = if can-do("s,l",poei.serlottype) then
                                     poeh.nosnlots -
                                         (o-nosnlots - poei.nosnlots)
                                 else poeh.nosnlots.

            if v-debugfl = yes then do:
                message "before p-poeilc.i". pause.
                message "poeh.totrcvamt: " poeh.totrcvamt. pause.
            end.

            {p-poeilc.i}

            if v-debugfl = yes then do:
                message "last of poel.lineno". pause.
                message "poei.lineno: " poei.lineno. pause.
                message "poeh.totrcvamt: " poeh.totrcvamt. pause.
            end.

        end.
    end.

    /*o Last of processing for a given purchase order */
    if last-of(poeh.posuf) then do:
        if v-debugfl = yes then do:
            message "at last-of poeh.posuf". pause.
            message "poeh.totqtyrcv: " poeh.totqtyrcv. pause.
            message "v-totqtyrcv: " v-totqtyrcv. pause.
        end.

        assign
             poeh.totqtyrcv   = poeh.totqtyrcv + v-totqtyrcv
             poeh.totqtyrcvb  = poeh.totqtyrcv
             poeh.totweight   = poeh.totweight + v-totlnwght
             poeh.totcubes    = poeh.totcubes + v-totlncubes.
    end.
end. /* for each poehb, ... */

if s-ibprinter ne "" /*  and NOT ( s-flglbl = no and poelb.xxl3 = no ) */
then     
  run PrintLabelPartTwo.p.                                                   
  
