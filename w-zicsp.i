/***************************************************************************
 PROGRAM NAME: w-zicsp.i
  PROJECT NO.:
  DESCRIPTION: This include is required for icsp lookups 
 DATE WRITTEN: 06/23/00
       AUTHOR: Sir Carl E. Graul Esq.
     MODIFIED: 

            Notes:
                Parameters
                        {1} = product number
                        {2} = no-lock or exclusive-lock
                        {3} = buffer profix b-

***************************************************************************/


/* ############################## LOGIC ############################## */

find    {3}icsp use-index k-icsp where
        {3}icsp.cono        = g-cono
    and {3}icsp.prod        = {1}
    and {3}icsp.statustype  <> "i"
    {2} no-error.


/* ############################ EOJ STUFF ############################ */


