/* f-smszg.i */
s-vendorid            format "x(24)"  colon 20 label "Vendor ID"
    validate (
              can-find(first b-zsastz where b-zsastz.cono = g-cono and
                                    b-zsastz.labelfl = false and
                                    b-zsastz.codeiden = "Vendor-Reports" and
                                    b-zsastz.primarykey = input s-vendorid and
                                    b-zsastz.secondarykey ge ""),
    "Vendor ID is not set up in SASTZ 'Vendor-Reports'")                         s-label               format "x(24)"  colon 20 label "Locator ID"
s-continue            format "999"    colon 20 label "Record #"
{1}[1]     colon 20 label "GeoCode"
{1}[2]     colon 20 label "GeoCode"
{1}[3]     colon 20 label "GeoCode"
{1}[4]     colon 20 label "GeoCode"
{1}[5]     colon 20 label "GeoCode"
{1}[6]     colon 20 label "GeoCode"
{1}[7]     colon 20 label "GeoCode"
{1}[8]     colon 20 label "GeoCode"
{1}[9]      colon 20 label "GeoCode"
{1}[10]     colon 20 label "GeoCode"
{1}[11]     colon 20 label "GeoCode"
{1}[12]     colon 20 label "GeoCode"
{1}[13]     colon 20 label "GeoCode"
{1}[14]     colon 20 label "GeoCode"
{1}[15]     colon 20 label "GeoCode"
{1}[16]     colon 20 label "GeoCode"
with frame f-smszg side-labels row 1 width 80 overlay
