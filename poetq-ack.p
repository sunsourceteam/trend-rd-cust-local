/***************************************************************************
 PROGRAM NAME: poetq-ack.p
  PROJECT NO.: 99-16
  DESCRIPTION: This program changed the acknowledgement data to default
                for copied p/o's
 DATE WRITTEN: 04/25/00
       AUTHOR: Sir Carl E. Graul Esq.
     MODIFIED: 

***************************************************************************/
{g-all.i}
{g-po.i}

def buffer b-poel for poel.

run "100-chg-back".

/* ***** eoj stuff ***** */


/* ############################ end of program ######################### */ 

procedure 100-chg-back:
for each b-poel where
        b-poel.cono = g-cono
    and b-poel.pono = g-pono
    and b-poel.posuf = g-posuf
    exclusive-lock:
    assign b-poel.user1 = "n"
           b-poel.user2 = ""
           b-poel.user9 = ?.
end. /* for each b-poel */
end procedure. /* 100-chg-back */

