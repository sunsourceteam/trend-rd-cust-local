/*sx5.5*/
/* oeepi9l.p 5/27/97 */
/*si**** Custom ****************************************************************
CLIENT  : 80281 SDI
JOB     : sdi009 Custom invoice print
AUTHOR  : msk
VERSION : 7
CHANGES :
    si01 5/27/97 msk; 80281/sdi009; initial changes
        - remove UPC from detail line
    si02 10/28/97  SV;sdi027; Added TB# 7241 & Upgraded to Trend V8
    si03 03/04/99 pcs;sdi065; added changes for canadian Currency conversion
    si04 04/01/99 gc; sdi065; Fixed problem with credit/returns canada total
    SS01 08/20/15 tah: 11% adder for audit purposes above cost.
*******************************************************************************/

/* oeepi9l.p 1.18 05/18/95 */
/*h*****************************************************************************
  PROCEDURE      : oeepi9l
  DESCRIPTION    : Order Entry Invoice Processing Format 9 - NEMA
  AUTHOR         : kmw
  DATE WRITTEN   : 02/16/90
  CHANGES MADE   :
    01/16/91 mwb; split out p-oeepi1.i for oversize
    12/04/91 pap; TB# 5155 JIT/Line Due
    01/03/92 mms; JIT print bo quantity even if no bo created yet
    04/06/92 pap; TB# 6238 Line DO - add indicator for direct order line items.
        Line DO - handle botype of 'd' for qty backordered
    06/13/92 mjm; TB# 5797 display blank description for nostock items if it is
        blank
    06/13/92 mjm; TB# 5116 change made to print quantity ordered when return
        line and decimal places to display.
    03/30/93 jlc; TB# 7151 If all items on a line were backordered, then qty
        shipped still printed a shipped qty.
    04/08/93 jlc; TB# 9726 Don't print core charge or restock amounts when the
        print price flag for the line is no.
    04/15/93 jlc; TB# 10156 Print customer product.
    06/09/93 jlc; TB# 7151  Additional fix per design committee.
    07/22/93 rs;  TB# 12120 Qty BO and Ship not showing on 's' or
    10/11/93 jlc; TB# 13103 Print customer product for edi orders.
    01/27/94 kmw; TB# 13828 2 hooks are needed in OEEPI1L.P
    02/04/94 mwb; TB# 14667 changed qty loads to use p-oeordl.i again, was hard
        coded due to TB# 12120.
    02/21/94 dww; TB# 13890 File name not specified causing error
    04/27/95 mtt; TB# 13356 Serial/Lot # doesn't print on PO/RM
    05/18/95 dww; TB# 16457 Qty BO'd and Shipped fields blank on Ship Complete
        orders.
    05/18/95 dww; TB# 17641 Suffix 01 Filled DO lines extend price on -00
    06/12/95 mtt; TB# 18575 Make changes for non-stock kit components (I1)
    11/06/95 gp;  TB# 18091 Expand UPC Number (T10).  Add s-upcpno and
        v-icupclength4.
    it01 jph; remove discount from invoioce print.
*******************************************************************************/
{g-wt.i}
{g-all.i}
{g-oeepi.i}

/*tb 7151 04/13/93 jlc; Calculate backorder quantity. */
def var v-qtybo         as de               no-undo.
def var v-price         as de format "999999999.99999" no-undo.    

def shared var v-wtehrecid     as recid                     no-undo.
def shared var v-subnetamt     like oeeh.totinvamt          no-undo.
def shared var v-totqtyshp     like oeeh.totqtyshp          no-undo.
def shared var v-linecnt       as i                         no-undo.

/*tb 10156 04/15/93 jlc; Print customer product */
def var s-prod          as c format "x(70)"             no-undo.
def var s-serialno      as c format "x(22)" extent 5    no-undo.
def var s-sign          as c format "x"                 no-undo.
def var s-descrip       as c format "x(49)" initial ""  no-undo.
def var s-price         as c format "x(13)" initial ""  no-undo.
def var s-netamt        as c format "x(13)" initial ""  no-undo.
def var v-reqtitle      as c format "x(17)" initial ""  no-undo.
def var s-qtyord        as c format "x(11)" initial ""  no-undo.
def var s-qtybo         as c format "x(11)" initial ""  no-undo.
def var s-qtyship       as c format "x(11)" initial ""  no-undo.
def var s-lineno        as c format "x(3)"  initial ""  no-undo.
def var s-prcunit       as c format "x(4)"  initial ""  no-undo.
def var s-discpct       as c format "x(13)" initial ""  no-undo.
def var s-upcpno        as c format "x(5)"              no-undo.
def var v-netord        like oeel.netord initial 0      no-undo.
def var v-qtyord        like oeel.qtyord initial 0      no-undo.
def var s-restockamt    like oeel.restockamt            no-undo.
/*tb 13828 01/27/94 kmw; 2 hooks are needed in OEEPI1L.P */
def var v-cancelfl      as l init no                    no-undo.
def var v-memomixfl     like icsp.memomixfl             no-undo.

/*tb# e11974 02/11/02 dls; New Subtotal logic from oeelc */
def var s-subtotdesc    as c format "x(24)" initial ""  no-undo.

{icsv.gva "new shared"}

def shared var s-cantot like oeeh.totinvamt initial 0 no-undo. /*si03*/

def var v-whse          like oeeh.whse                 no-undo. 
def var v-qtyship       like oeel.qtyship              no-undo.  
def var v-qtyfl         as log format "yes/no"         no-undo.

{f-wtxpp9.i}                            /* si01 */
{oeepp6.z99 &define_frames = "*"}
def var v-loopcnt as integer init 1 no-undo.

/* MAIN LOGIC */
find wteh where recid(wteh) = v-wtehrecid no-lock no-error.
assign s-cantot = 0.
if avail wteh then do:

    for each wtel use-index k-wtel where
        wtel.wtno     = wteh.wtno    and
        wtel.wtsuf    = wteh.wtsuf   and
        wtel.statustype ne "c"
    no-lock:
      if wteh.stagecd ge 2 and wtel.qtyship = 0 then
        next.
        /*d Set the net/qty based on ordered or shipped qtys */
        assign
            v-linecnt = v-linecnt + 1
            v-netord  = if wteh.stagecd < 2 then wtel.netord * 1.129  /* SS01 */
                        else wtel.netamt * 1.129                      /* SS01 */
            v-qtyord  = if wteh.stagecd < 2 then wtel.qtyord
                        else wtel.qtyship.              
                        
        /*o Find the ICSW, ICSP, and SALS records */
        if wtel.nonstockty ne "n" then do:
            {w-icsw.i wtel.shipprod wtel.shipfmwhse no-lock}
            {w-icsp.i wtel.shipprod no-lock}
        end.

        /*sx55*/
        /*tb e2761 10/20/99 kjb; Add Tally functionality to OEEPI - indicate
           whether the product is a memo tally product */
        v-memomixfl = if avail icsp then icsp.memomixfl else no.
        
                /* si02 */
        /*tb 7241 (5.1) 03/02/97 jkp; Added find of icss record from the
            oeel record because the prices/costs are not being recalculated */
        {icss.gfi
            &prod        = wtel.shipprod
            &icspecrecno = wtel.icspecrecno
            &lock        = "no"}
                

        /*tb 18091 11/06/95 gp;  Find Product section of UPC number */
        {icsv.gfi "'u'" wtel.shipprod wtel.vendno no}

        clear frame f-oeel.
        assign
            s-qtyship  = string(wtel.qtyship,"zzzzzz9.99-")
            s-qtyord   = string(wtel.qtyord,"zzzzzz9.99-")
            s-qtyship  = if wteh.stagecd < 2 then ""
                         else s-qtyship
            v-qtybo    = if wtel.bofl ne no then
                            dec(s-qtyord) - dec(s-qtyship)
                         else 0
            s-qtybo    = if v-qtybo = integer(v-qtybo)
                            then string(v-qtybo,"zzzzzz9-")
                         else string(v-qtybo,"zzzzzz9.99-")
            s-qtybo   = if wteh.stagecd < 2 then ""
                        else s-qtybo
            s-lineno   = string(wtel.lineno,"zz9")
/* SS01 */
            s-price    = if (wtel.prodcost * 1.129) * 100 =
                            integer(wtel.prodcost * 100)
                              then string(wtel.prodcost * 1.129,"zzzzzz9.99-")
                         else string(wtel.prodcost * 1.129,"zzzzzz9.99999-")
/* SS01 */
            s-discpct  = string(0,"zzzzzz9.99-")
            s-netamt   = string(v-netord ,"zzzzzzzz9.99-")
            s-sign     =  ""
            v-subnetamt = v-subnetamt + v-netord
            v-totqtyshp  = v-totqtyshp + v-qtyord
/* SX20 Sunsource */          
            s-prcunit    = if avail icss and icss.prccostper <> ""
                               then icss.prccostper
                           else wtel.unit
/* SX20 Sunsource */          

            s-upcpno    = if avail icsv then
                              {upcno.gas &section  = "4"
                                         &sasc     = "v-"
                                         &icsv     = "icsv."
                                         &comdelim = "/*"}
                          else "00000".

        if s-discpct ne "" then do:                          /* beg it01 */
             v-price = wtel.prodcost * 1.129.                 /* SS01 */
             s-price = if v-price * 100 = integer(v-price * 100)
                          then string(v-price,"zzzzzz9.99-")
                       else string(v-price,"zzzzzz9.9999-").
             s-discpct = "".
             end.                                           /* end it01 */
             
        /*tb 3-2 03/29/00 rgm; Rxserver interface hook for extra vars */
        {wtxppl.z99 &before_linedisplay = "*"}
        
        /*o Display the line item */
        /*tb 18091 11/06/95 gp;  Change UPC number assignment */

        display s-lineno
                wtel.shipprod
                wtel.unit
                s-qtyord
                s-qtybo
                s-qtyship
                s-prcunit
                /* s-upcpno                                     si01 */
                s-price         
                s-discpct       
                s-netamt        
                s-sign
        with frame f-oeel.

        /*d Display the second product description line */
        if wtel.nonstockty = "n" then
            display wtel.proddesc @ s-descrip with frame f-oeel.
        else if not avail icsp or not avail icsw then 
           display v-invalid @ s-descrip with frame f-oeel.
        else
           display icsp.descrip[1] + " " + icsp.descrip[2]
                 @ s-descrip with frame f-oeel.
        down with frame f-oeel.

        /*o Display cross reference information */
        v-reqtitle = if wtel.xrefprodty = "c" then "   Customer Prod:"
                     else if wtel.xrefprodty = "i" then "Interchange Prod:"
                     else if wtel.xrefprodty = "p" then " Superseded Prod:"
                     else if wtel.xrefprodty = "s" then " Substitute Prod:"
                     else if wtel.xrefprodty = "u" then "    Upgrade Prod:"
                     else "".
        if can-do("c,i,p,s,u",wtel.xrefprodty) and wtel.reqprod <> ""
        then do:
            s-prod = wtel.reqprod.
            display s-prod v-reqtitle with frame f-oeelc.
            down with frame f-oeelc.
        end.
/*
        if wtel.xrefprodty ne "c" then do:
            s-prod = if wtel.reqprod ne "" then wtel.reqprod
                     else wtel.shipprod.
            {n-icseca.i "first" s-prod ""c"" oeeh.custno no-lock}
            if avail icsec then do:
                assign
                    s-prod = icsec.prod
                    v-reqtitle = "   Customer Prod:".
                display s-prod v-reqtitle with frame f-oeelc.
            end.
        end.
*/


    /*tb 3-2 03/29/00 rgm; Rxserver interface hook for extra vars */
    {wtxppl.z99 &before_prodnotes = "*"}

     run zsditariffprt (g-cono,wtel.shipprod,wtel.shipfmwhse,"oeepi",8).

    /*o*******************  Print the product notes  *********************/
        if wtel.nonstockty <> "n" and avail icsp and icsp.notesfl ne "" then
            for each notes where
                notes.cono         = g-cono    and
                notes.notestype    = "p"       and
                notes.primarykey   = icsp.prod and
                notes.secondarykey = ""        and
                notes.printfl      = yes
            no-lock:
                do i = 1 to 16:
                    /*tb 13890 02/21/94 dww; File name not specified
                         causing error */
                    if notes.noteln[i] ne "" then do:
                        display notes.noteln[i] with frame f-notes.
                        down with frame f-notes.
                    end.
                end.
            end.

        /*o Print the serial/lot records for the line (shared with OEEPP) */
        /*tb 13356 04/27/95 mtt; Serial/Lot # doesn't print on PO/RM */
        {p-oeepp1.i &type       = "t"
                    &line       = "wtel"
                    &pref       = "wt"
                    &seq        = 0
                    &stkqtyship = "wtel.stkqtyship"
                    &snlot      = "wtel.nosnlotso"
                    &specnstype = "wtel.nonstockty"}

    /*o*************Print the line item comments  ************************/
        if wtel.commentfl = yes then do:
            {w-com.i ""wte"" wtel.wtno wtel.wtsuf wtel.lineno yes no-lock}
            if avail com and com.printfl = yes then do i = 1 to 16:
                if com.noteln[i] ne "" then do:
                    display com.noteln[i] with frame f-com.
                    down with frame f-com.
                end.
            end.

            {w-com.i ""wt"" wtel.wtno wtel.wtsuf wtel.lineno yes no-lock}
            if avail com and com.printfl = yes then do i = 1 to 16:
                if com.noteln[i] ne "" then do:
                    display com.noteln[i] with frame f-com.
                    down with frame f-com.
                end.
            end.
        end.

    end. /* for each oeel */
    
end. /* if oeeh found */
/* Copyright 1989,1990,1991,1992,1993,1994 by R & D Systems Company */
/* All Rights Reserved. */
