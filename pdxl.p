/***************************************************************************
 PROGRAM NAME: pdxl.p
  PROJECT NO.: 
  DESCRIPTION: This program creates a rebate file for Copperlogic product      
               lines. Build on demand it components will be included. 

  DATE WRITTEN: 08/20/01.
  AUTHOR      : SunSource.  

Patch    Init Date     Notes
------   ---- -------- -----------------------------------------------------


***************************************************************************/

/* default stream is Air-Draulics */ 

{p-rptbeg.i}
{zsapboydef.i}

{g-pdcalc.i &new = "new"}
{speccost.gva "new shared"}

define var v-whse like icsw.whse no-undo.
define var v-custno like arsc.custno no-undo.
define var v-prod like icsw.prod no-undo.
define var v-qty   like oeel.qtyord no-undo.
define var v-priced like icsw.listprice no-undo.
define var v-orderno like oeel.orderno no-undo.
define var v-ordersuf like oeel.ordersuf no-undo.
define var v-lineno like oeel.lineno no-undo.
define var v-recid as recid no-undo.
define var v-prdprctype like icsw.pricetype no-undo.
define var v-pdrecid like oeel.pdrecno no-undo.
define var v-pddisc  like oeel.discamt no-undo.
define var v-pdlist  like oeel.price no-undo.
define var v-mgr     like smsn.mgr no-undo.



def stream rebate_tmp.
def stream rebate.
def var fmessage          as char      format "x(80)" no-undo.
define variable vicrec    as character format "x(251)" no-undo.
define variable rfilename as character format "x(24)" no-undo.
define variable ofilename as character format "x(60)" no-undo.
define variable tempfile  as character format "x(60)" no-undo.
define variable d_prodline like oeel.prodline no-undo.
define variable d_prodcat  like oeel.prodcat  no-undo.
define new shared var v-tempprt as c format "x(40)" no-undo.
define new shared var s-type    as c format "x(40)" no-undo.
def var d_shipprod as c format "x(24)" no-undo.
def var d_shipped as decimal format "99999999" no-undo.
def var d_price as decimal format "9999999.99" no-undo.

/* fix006 */
def var x_shipprod  as character format "x(24)" no-undo.
def var x_listprice as decimal format "9999999.99" no-undo.
/* fix006 */

def var d_name as c format "x(20)" no-undo.
def var sic as c format "x(4)" no-undo.
def var c_no as c format "x(8)" no-undo.
def var d_line as i format 999 no-undo.
def var d_um as c format "x(4)" no-undo.
def var yyyy as c format "x(4)" no-undo.
def var creditdebit as c format "x(1)"   no-undo.
def var bypass_sw as logical             no-undo.
def var reprun    as logical init "no"   no-undo.
def var repout    like arsc.slsrepout    no-undo.
def var pwrrun    as logical init "no"   no-undo.
def var chlrun    as logical init "no"   no-undo.
def var pdsrhit   as logical init "no"   no-undo.
def var autotype  as logical init "no"   no-undo.
def var prctype   as char format "x(04)" no-undo.
def var vrebloc   as char format "x(10)" no-undo.
def var alias-custno as c format "x(30)" no-undo.
def var sun-custno   like oeeh.custno    no-undo.
def var icswptyp  as char format "x(04)" no-undo.
def var icsdftyp  as char format "x(03)" no-undo.
def var rebpct    as int  format "9"     no-undo.
def new shared var p-list    as logical  no-undo.
def var company as c format "x(3)"       no-undo.
def var ordernum as char format "x(12)"  no-undo.
def var procdt   as date                 no-undo.               
def var x        as i                    no-undo.
def var n-idx    as i                    no-undo.
def var distrec  as logical              no-undo.
def var yes-power as logical             no-undo.
def var b_r_date as char                 no-undo.
def var e_r_date as char                 no-undo.
def new shared var zx-vendno like apsv.vendno no-undo.
def var savedt   like oeeh.invoicedt     no-undo.  
def var savecust like oeeh.custno        no-undo.
def var w-vendno     as de format "999999999999"       no-undo.
def var w-arpwhse    as c  format "x(4)" no-undo.
def var w-valid      as logical          no-undo.
def var w-rebtype    like icsw.rebatety                no-undo.
def var w-rebamt     as de format ">>>>>>9.9999"       no-undo.
def var w-totrebate  as de format ">>>>>>9.9999"       no-undo.
def var w-vicrebate  as de format ">>>>>>9.9999"       no-undo.
def var w-netrebate  as de format ">>>>>>9.9999"       no-undo.
def var w-listprice  as de format ">>>>>>9.9999"       no-undo.
def var t-pderamt    as de format ">>>>>>9.9999"       no-undo.
def var t-expreb     as de format ">>>>>>9.9999"       no-undo.
def var t-status     as char  format "x"               no-undo.
def var tx-cono      like apsv.cono   no-undo.
def var tx-vendno    like apsv.vendno no-undo.
def var tx-custno    like arsc.custno no-undo.
def var tx-prod      like icsp.prod   no-undo.
def var tx-pct       as decimal   no-undo.
def var w-rebamt2    as de format ">>>>>>9.9999"       no-undo.
def var w-rebamt3    as de format ">>>>>>9.9999"       no-undo.
def var t-note       as char format "x(30)"            no-undo. 
 
def buffer b-icsw for icsw.

def temp-table t-err
    field custno like arsc.custno 
    field whse   like icsd.whse
    index k-err
          custno
          whse.

def temp-table xno_tbl
    field xordno   as char format "x(12)" 
    field xlineno  as int  format "999"
    field xno      as int  format "999" 
    index k-xno1 
          xordno 
          xlineno
          xno     ascending. 
 
define frame f-fname
       skip (2)
       fmessage at 1 
       with width 130 no-labels no-box.

define new shared variable begdt as date no-undo.
define new shared variable enddt as date no-undo.

define temp-table t-pcats
  field pcat as char
index ikx
  pcat.

create t-pcats.
assign t-pcats.pcat = "AMOB".

create t-pcats.
assign t-pcats.pcat = "AMOC".  

create t-pcats.
assign t-pcats.pcat = "AMOD". 

create t-pcats.
assign t-pcats.pcat = "AMOE".  

/* designed to run for previous month if run in current month period */

b_r_date = sapb.rangebeg[1].
v-datein = sapb.rangebeg[1].
 {p-rptdt.i}
begdt = v-dateout.
if string(v-dateout) = v-lowdt then 
  do:
   assign begdt = today.
  end.
if substring(b_r_date,1,2) = "**" or substring(v-datein,1,2) = "" then 
   if month(begdt) = month(today) then  
     do:
      assign begdt = ( if day(today) <= 30 then
                          today - 30
                       else
                          today - 31)
             begdt = date(month(begdt),1,year(begdt)).
     end. 

e_r_date = sapb.rangeend[1].
v-datein = sapb.rangeend[1].
 {p-rptdt.i}
enddt = v-dateout.
if string(v-dateout) = v-highdt then 
   assign enddt = today.
if substring(e_r_date,1,2) = "**" or substring(e_r_date,1,2) = "" then  
   if month(enddt) = month(today) then  
     do:
      assign enddt = (date(month(today),1,year(today)) - 1).
     end. 

if begdt = today and enddt = today then 
  do: 
     message "Date fields were left blank - defaulting to today".
     message "Pls use asterisks for previous month date range".
     pause.
     message "Previous months dates will begin with the first day".
     message " and end with the last day of the previous month".
     pause.
     leave.  
  end.

display "Beginning Date Range " begdt no-label.                     
display "Ending Date Range " enddt no-label. 

if sapb.optvalue[1] = "p" then
  pwrrun = yes.

assign rfilename = sapb.optvalue[1]
       ofilename = "/usr/tmp/" + rfilename  
       tempfile  = ofilename + "_tmp".

output stream rebate_tmp to value(tempfile).
output stream rebate     to value(ofilename).
p-list   = if sapb.optvalue[2] = "yes" then 
              true 
           else 
              false.
/** FIX012 **/
assign vrebloc   = sapb.optvalue[3]              
       zx-vendno = 10000033083.
if p-list then 
   do:
     {zsapboyload.i}
     if zelection_matrix[1] = 0 and 
        zelection_matrix[2] = 0 and 
        zelection_matrix[3] = 0 and 
        zelection_matrix[4] = 0 and 
        zelection_matrix[5] = 0 and 
        zelection_matrix[6] = 0 and 
        zelection_matrix[7] = 0 then 
        do:
        display
         "**************POZY****************"  at 1 skip 
         "******Error in List Function******"  at 1 skip  
         "********List Option Chosen********"  at 1 skip 
         "**** No data entered ** POZY *****"  at 1.
        return.
        end.
   end.  

  
 run "power_build".

/* fix007 */  
for each icsd where icsd.cono = g-cono  
    no-lock:
/* fix007 */                                    

do procdt = begdt to enddt: 
oeeh-lookup:
for each oeeh use-index k-invproc
   where oeeh.cono      = g-cono    and 
         oeeh.whse      = icsd.whse and  
         oeeh.invoicedt = procdt    and
         oeeh.stagecd   > 3         and
        (oeeh.transtype ne "rm"     and 
         oeeh.transtype ne "cr"     and 
         oeeh.transtype ne "qu")  
      no-lock.  

if p-list then do: 
 zelection_type  = "c".
 zelection_char4 = " ". 
 zelection_cust  = oeeh.custno. 
 zelection_vend  = 0.
 run zelectioncheck.
 if zelection_good = false then 
   next oeeh-lookup.
end.            

/* extract price type to determine autorun */ 
assign prctype = ""
       repout  = "". /* fix009 */  
find first arsc where arsc.cono = g-cono
       and arsc.custno = oeeh.custno
       no-lock no-error.
if avail(arsc) then
    assign repout  = arsc.slsrepout  /* fix009 */  
           prctype = arsc.pricetype
           sic     = string(arsc.siccd[1]).

if oeeh.shipto ne "" then do: 
find first arss where 
           arss.cono   = g-cono
       and arss.custno = oeeh.custno  
       and arss.shipto = oeeh.shipto
       no-lock no-error.
if avail(arss) then
    assign prctype = arss.pricetype.
    /*       repout  = arss.slsrepout. /* fix009 */   */
end.


for each oeel where
         oeel.cono     = g-cono        and
         oeel.orderno  = oeeh.orderno  and
         oeel.ordersuf = oeeh.ordersuf and 
         oeel.specnstype ne "l"
         no-lock.

if oeel.qtyship = 0 then next.

find first smsn
     where smsn.cono   = oeeh.cono and
           smsn.slsrep = (if repout ne "" then  /* fix009 */  
                             repout 
                          else    
                          if oeel.slsrepout = "" then 
                             oeeh.slsrepout
                          else 
                             oeel.slsrepout)
     no-lock no-error.      

/* fix009 */  


if avail smsn then
  v-mgr = smsn.mgr.
else
  v-mgr = "".
if avail smsn and 
   (smsn.slsrep = "0227" or
    smsn.slsrep = "2128" or
    smsn.slsrep = "2135") then
  v-mgr = "P500".

if avail smsn  then 
    assign d_name = substring(smsn.name,1,20).

assign d_prodline = oeel.prodline 
       d_prodcat  = oeel.prodcat
       d_line     = oeel.lineno
       d_um       = oeel.unit
       d_shipprod = substring(oeel.shipprod,1,24)
       w-arpwhse  = oeel.whse. 

if oeel.specnstype = "n" and
   d_shipprod = "000000" then
       d_shipprod = substring(oeel.proddesc,1,24).

 find first icsw where
            icsw.cono = g-cono    and 
            icsw.whse = w-arpwhse and  
            icsw.prod = oeel.shipprod no-lock no-error.

  /* fix006 */
 assign x_shipprod = oeel.shipprod
        x_listprice = if avail icsw then 
                        icsw.listprice
                      else
                        0.

 if avail icsw and icsw.arpvendno <> 0 then
   w-vendno = icsw.arpvendno.
/*
 if w-vendno <> 10000033083 and oeel.kitfl = false then
   next.
*/




/* fix006 */  
  
  find icsp where
           icsp.cono = g-cono and 
           icsp.prod = oeel.shipprod
           no-lock no-error.

  if avail icsp then 
    assign         
      d_prodcat  = (if can-find(t-pcats where 
                               t-pcats.pcat = d_prodcat no-lock) then 
                      d_prodcat
                   else 
                   if avail icsp and d_prodcat ne "fvib" then 
                     icsp.prodcat
                   else
                     d_prodcat)     
                               
      d_shipprod = (if avail icsw and icsw.vendprod ne "" then 
                      substring(icsw.vendprod,1,24)
                    else 
                    if avail icsp then    
                      substring(oeel.shipprod,1,24)
                    else     
                      d_shipprod).
 
 if oeel.specnstype ne "n" and icsw.whse = "ap-1" then 
    next.
 else 
 if oeel.whse = "ap-1" then 
    next. 

if oeel.ordertype = "f" and oeel.orderaltno <> 0 then 
   next. 
/* FIX002  */

 
if not can-find(t-pcats where t-pcats.pcat = d_prodcat no-lock) and
     oeel.kitfl = false then 
  next.
 
 
 
 t-pderamt = 0.
 find first pder use-index k-pder 
      where pder.cono     = g-cono and 
            pder.orderno  = oeel.orderno and 
            pder.ordersuf = oeel.ordersuf and 
            pder.lineno   = oeel.lineno
            no-lock no-error.
  if avail pder then 
   do:         
    /*tb 7241  (7.1) 03/04/97 jl; Added icss.gfi and speccost.gas*/
    /*tb 24359 03/05/98 gp; Change pder.prod to pder.shipprod */
    /*d Find special price costing information and load information into
        standard speccost variables */

    {icss.gfi
        &prod        = pder.shipprod
        &icspecrecno = pder.icspecrecno
        &lock        = "no"}

    assign
        {speccost.gas
            &com    = "/*"}
        t-pderamt  = round((
                       pder.rebateamt * {speceach.gco} * pder.actstkqty *
                          (if v-speccostty = "" then 1
                           else pder.unitconv
                          )                                             *
                          (if pder.returnfl then -1
                           else 1
                          )),2).
   end.

if oeel.qtyship > 0 and oeel.kitfl ne yes then 
 do:
    assign d_price     = 
                       if oeel.discpct = 0 then
                            oeel.price
                         else
                           ((100 - oeel.discpct) / 100) * oeel.price
           
           d_shipped   = oeel.qtyship
           creditdebit = if oeel.returnfl = yes then
                          "-"
                         else
                          "+".
   run move_rebate_info(input 23,
                        input vrebloc).
 end.

run tied-orders.

/* FIX004  */
if oeel.kitfl = yes then          
/* FIX004  */
 do:
 for each oeelk where oeelk.cono = g-cono and 
          oeelk.ordertype = "o"           and
          oeelk.orderno   = oeeh.orderno  and 
          oeelk.ordersuf  = oeeh.ordersuf and 
          oeelk.lineno    = oeel.lineno   and 
          oeelk.specnstype ne "l"
          no-lock.

 if oeelk.qtyship eq 0 then next.
 if oeelk.statustype ne "i" then next.

 assign d_prodline = oeelk.arpprodline      
        icswptyp   = oeelk.pricetype
        d_prodcat  = oeelk.prodcat
        d_um       = oeelk.unit
        d_shipprod = substring(oeelk.shipprod,1,24)
        w-arpwhse  = oeelk.whse 
        w-vendno   = oeelk.arpvendno.

     find first icsw where
                icsw.cono = g-cono    and 
                icsw.whse = w-arpwhse and  
                icsw.prod = oeelk.shipprod no-lock no-error.

     if avail icsw and icsw.arpvendno <> 0 then
       w-vendno = icsw.arpvendno.

/* fix006 */
     assign x_shipprod = oeelk.shipprod
            x_listprice = if avail icsw then 
                            icsw.listprice
                          else
                            0.
/* fix006 */  
        
        find icsp where
             icsp.cono = g-cono and 
             icsp.prod = oeelk.shipprod
             no-lock no-error.
 
if not can-find(t-pcats where t-pcats.pcat = d_prodcat no-lock) and
     oeel.kitfl = false then 
  next.
 
        
        if avail icsp then 
         assign
         /* FIX004  */
          d_prodcat  = if can-find(t-pcats where t-pcats.pcat = d_prodcat 
                                   no-lock) then 
                          d_prodcat
                       else 
                       if avail icsp then 
                          icsp.prodcat
                       else
                          d_prodcat               
          d_shipprod = (if avail icsw and icsw.vendprod ne "" then 
                           substring(icsw.vendprod,1,24)
                        else 
                        if avail icsp then    
                           oeelk.shipprod
                        else     
                           d_shipprod).
 
 if oeelk.specnstype ne "n" and icsw.whse = "ap-1" then 
    next.
 else 
 if oeelk.whse = "ap-1" then 
    next. 
  
 if oeelk.ordertype = "f" and oeelk.orderaltno <> 0 then 
    next. 
if not can-find(t-pcats where t-pcats.pcat = d_prodcat no-lock) then 
  next.

if avail smsn then
  v-mgr = smsn.mgr.
else
  v-mgr = "".
if avail smsn and 
   (smsn.slsrep = "0227" or
    smsn.slsrep = "2128" or
    smsn.slsrep = "2135") then
  v-mgr = "P500".

 do: /***** temporary for february 2004 ****/
     assign d_price = if oeel.discpct = 0 then
                         oeelk.price
                     else 
                        ((100 - oeel.discpct) / 100) * oeelk.price
           d_shipped = oeelk.qtyship
           creditdebit = "+".
    if oeelk.specnstype ne "N" then
    do: 
     assign v-custno = oeeh.custno
            v-prod   = oeelk.shipprod
            v-whse   = oeeh.whse 
            v-qty    = oeelk.qtyship.
     run zsdipricer4.p (input no,
                        input v-recid,
                        input v-prod,
                        input v-qty,
                        input v-whse,
                        input v-custno,
                        input-output v-pdrecid,
                        input-output v-prdprctype,
                        input-output v-priced,
                        input-output v-pddisc,
                        input-output v-pdlist).
     assign d_price = v-priced.
    end. 
    /*  run PDSC_pricing.    */
 end.
 
 t-pderamt = 0.
 find first pder use-index k-pder 
      where pder.cono     = g-cono        and 
            pder.orderno  = oeel.orderno  and 
            pder.ordersuf = oeel.ordersuf and 
            pder.lineno   = oeel.lineno   and 
            pder.seqno    = oeelk.seqno
            no-lock no-error.
  if avail pder then 
   do:         
    /*tb 7241  (7.1) 03/04/97 jl; Added icss.gfi and speccost.gas*/
    /*tb 24359 03/05/98 gp; Change pder.prod to pder.shipprod */
    /*d Find special price costing information and load information into
        standard speccost variables */
    {icss.gfi
        &prod        = pder.shipprod
        &icspecrecno = pder.icspecrecno
        &lock        = "no"}
    assign
        {speccost.gas
            &com    = "/*"}
        t-pderamt  = round((
                       pder.rebateamt * {speceach.gco} * pder.actstkqty *
                          (if v-speccostty = "" then 1
                           else pder.unitconv
                          )                                             *
                          (if pder.returnfl then -1
                           else 1
                          )),2).
   end.

 if oeelk.specnstype = "n" and
    d_shipprod = "000000" then
    d_shipprod = substring(oeelk.proddesc,1,24).
 
  if oeelk.qtyship > 0 then
  do:
    d_shipped = oeelk.qtyship.
    run move_rebate_info(input 23,
                         input vrebloc).
  end.
 end.  /* loop back to oeelk */  
 end. /*** if kitfl = yes do ***/

end. /* oeel loop end */
end. /* for oeeh loop */
end. /* do procdt     */
end. /* for icsd loop */

assign  fmessage =  "*** PDXL successfully created file " + ofilename.
display fmessage  
        with frame f-fname.

procedure tied-orders: 
/* FIX004  */
 do:
 for each oeelk where oeelk.cono = g-cono    and 
          oeelk.ordertype = "w"              and
          oeelk.orderno   = oeel.orderaltno  and 
          oeelk.ordersuf  = 00               and 
          oeelk.lineno    = 0                and 
          oeelk.specnstype ne "l"
          no-lock.

 if oeelk.qtyship eq 0 then next.
 if oeelk.statustype ne "i" then next.

 assign d_prodline = oeelk.arpprodline      
        icswptyp   = oeelk.pricetype
        d_prodcat  = oeelk.prodcat
        d_um       = oeelk.unit
        d_shipprod = substring(oeelk.shipprod,1,24)
        w-arpwhse  = oeelk.whse 
        w-vendno   = oeelk.arpvendno.

     find first icsw where
                icsw.cono = g-cono    and 
                icsw.whse = w-arpwhse and  
                icsw.prod = oeelk.shipprod no-lock no-error.

     if avail icsw and icsw.arpvendno <> 0 then
       w-vendno = icsw.arpvendno.

/* fix006 */
     assign x_shipprod = oeelk.shipprod
            x_listprice = if avail icsw then 
                            icsw.listprice
                          else
                            0.
/* fix006 */  
        
        find icsp where
             icsp.cono = g-cono and 
             icsp.prod = oeelk.shipprod
             no-lock no-error.
        if avail icsp then 
         assign
         /* FIX004  */
          d_prodcat  = if can-find(t-pcats where t-pcats.pcat = d_prodcat 
                                   no-lock) then 
                          d_prodcat
                       else 
                       if avail icsp then 
                          icsp.prodcat
                       else
                          d_prodcat               
          d_shipprod = (if avail icsw and icsw.vendprod ne "" then 
                           substring(icsw.vendprod,1,24)
                        else 
                        if avail icsp then    
                           oeelk.shipprod
                        else     
                           d_shipprod).
 
 if oeelk.specnstype ne "n" and icsw.whse = "ap-1" then 
    next.
 else 
 if oeelk.whse = "ap-1" then 
    next. 
  
 if oeelk.ordertype = "f" and oeelk.orderaltno <> 0 then
    next. 
 if not can-find(t-pcats where t-pcats.pcat = d_prodcat no-lock) then
    next.
if avail smsn then
  v-mgr = smsn.mgr.
else
  v-mgr = "".
if avail smsn and 
   (smsn.slsrep = "0227" or
    smsn.slsrep = "2128" or
    smsn.slsrep = "2135") then
  v-mgr = "P500".

 do: /***** temporary for february 2004 ****/
     assign d_price = if oeel.discpct = 0 then
                         oeelk.price
                     else 
                        ((100 - oeel.discpct) / 100) * oeelk.price
           d_shipped = oeelk.qtyship
           creditdebit = "+".
    if oeelk.specnstype ne "N" then
    do: 
     assign v-custno = oeeh.custno
            v-prod   = oeelk.shipprod
            v-whse   = oeeh.whse 
            v-qty    = oeelk.qtyship.
     run zsdipricer4.p (input no,
                        input v-recid,
                        input v-prod,
                        input v-qty,
                        input v-whse,
                        input v-custno,
                        input-output v-pdrecid,
                        input-output v-prdprctype,
                        input-output v-priced,
                        input-output v-pddisc,
                        input-output v-pdlist).
     assign d_price = v-priced.
    end. 
    /*   run PDSC_pricing.   */
 end.
 
 t-pderamt = 0.
 find first pder use-index k-pder 
      where pder.cono     = g-cono        and 
            pder.orderno  = oeel.orderno  and 
            pder.ordersuf = oeel.ordersuf and 
            pder.lineno   = oeel.lineno   and 
            pder.seqno    = oeelk.seqno
            no-lock no-error.
  if avail pder then 
   do:         
    /*tb 7241  (7.1) 03/04/97 jl; Added icss.gfi and speccost.gas*/
    /*tb 24359 03/05/98 gp; Change pder.prod to pder.shipprod */
    /*d Find special price costing information and load information into
        standard speccost variables */
    {icss.gfi
        &prod        = pder.shipprod
        &icspecrecno = pder.icspecrecno
        &lock        = "no"}
    assign
        {speccost.gas
            &com    = "/*"}
        t-pderamt  = round((
                       pder.rebateamt * {speceach.gco} * pder.actstkqty *
                          (if v-speccostty = "" then 1
                           else pder.unitconv
                          )                                             *
                          (if pder.returnfl then -1
                           else 1
                          )),2).
   end.

 if oeelk.specnstype = "n" and
    d_shipprod = "000000" then
    d_shipprod = substring(oeelk.proddesc,1,24).
 
 if oeelk.qtyship > 0 then
  do:
    d_shipped = oeelk.qtyship.
    run move_rebate_info(input 23,
                         input vrebloc).
  end.
 end.  /* loop back to oeelk */  
 end. /*** tied orders do ***/
end procedure. 

PROCEDURE move_common_info:
do:
  assign rebpct = 0.

  assign
  substring(vicrec,1,8)    = c_no
  substring(vicrec,9,9)    = 
   (if substring(string(oeeh.custno,"zzzzzzzzzzz9"),1,3) <> "" then
      substring(string(oeeh.custno,"999999999999"),4,9)
    else
      substring(string(oeeh.custno,"zzzzzzzzzzz9"),4,9))
  substring(vicrec,18,30)  = arsc.name
  substring(vicrec,48,30)  = oeeh.shiptoaddr[1]
  substring(vicrec,78,30)  = oeeh.shiptoaddr[2]
  substring(vicrec,108,30) = "                              "
  substring(vicrec,138,20) = substring(oeeh.shiptocity,1,20) 
  substring(vicrec,158,3)  = oeeh.shiptost
  substring(vicrec,161,10) = oeeh.shiptozip
  substring(vicrec,171,3)  = "US"
  overlay(vicrec,174,2)    = string(month(oeeh.invoicedt),"99") 
  overlay(vicrec,176,4)    = string(year(oeeh.invoicedt))
  substring(vicrec,180,4)  = sic 
  substring(vicrec,184,15) = d_shipprod
  substring(vicrec,199,8)  = if oeeh.transtype = "RM" or
                               creditdebit = "-" then
                               string(d_shipped * -1, "9999999-")
                            else string(d_shipped, "99999999")
  substring(vicrec,207,20) = d_name
  substring(vicrec,227,10) = string(d_price, "999999.99-") 
  substring(vicrec,237,15) = string(oeeh.orderno,"9999999") +
                                    "-" + string(oeeh.ordersuf,"99").    
  put stream rebate vicrec at 1 skip.
  export stream rebate_tmp delimiter "," 
         c_no                                           
         (if substring(string(oeeh.custno,"zzzzzzzzzzz9"),1,3) <> "" then
           substring(string(oeeh.custno,"999999999999"),4,9)
          else
           substring(string(oeeh.custno,"zzzzzzzzzzz9"),4,9))
         string(arsc.name,"x(30)")                                          
         string(oeeh.shiptoaddr[1],"x(30)")                                 
         string(oeeh.shiptoaddr[2],"x(30)")                                 
         "                              "                   
         substring(oeeh.shiptocity,1,20)                    
         oeeh.shiptost                                      
         oeeh.shiptozip                                     
         "US"                                               
         string(month(oeeh.invoicedt),"99") +             
         string(year(oeeh.invoicedt))                       
         sic                                                
         string(d_shipprod,"x(15)")                                
         if oeeh.transtype = "RM" or           
            creditdebit = "-" then              
            string(d_shipped * -1, "9999999-")  
         else
            string(d_shipped, "99999999")     
         string(d_name,"x(20)")                                
         string(d_price, "9999999.99-")        
         string(oeeh.orderno,"99999999") + "-" + string(oeeh.ordersuf,"99") 
         string(rebpct,"9")
         string(t-pderamt,"999999.99")
         string(t-expreb,"999999.99")
         string(t-status,"x")
         string(t-note,"x(30)")
         string(oeeh.custno,"zzzzzzzzzzz9").
end.

vicrec = "".
END PROCEDURE.

procedure power_build:
  do:
    FOR EACH vaeh WHERE vaeh.cono = g-cono        and 
         ((year(vaeh.receiptdt)  ge year(begdt)   and
          month(vaeh.receiptdt)  ge month(begdt)) and 
          (year(vaeh.receiptdt)  le year(enddt)   and  
          month(vaeh.receiptdt)  le month(enddt)))
        NO-LOCK,
        EACH vaelo
             WHERE (vaelo.vano = vaeh.vano AND vaelo.vasuf = vaeh.vasuf) and
                   (vaelo.cono = vaeh.cono) and 
                    vaelo.ordertype = "o"
                    NO-LOCK:
        for each vaesl
            WHERE (vaesl.vano = vaeh.vano AND vaesl.vasuf = vaeh.vasuf) and
                  (vaesl.cono = g-cono AND vaesl.sctntype = "in" AND
                   vaesl.statustype = no)
                   NO-LOCK:
     

         if avail vaelo then 
          do: 
            assign w-rebtype = " ". 
            find first oeeh where
                       oeeh.cono = g-cono and 
                       oeeh.orderno   = vaelo.orderaltno and 
                       oeeh.ordersuf >= vaelo.orderaltsuf
                       no-lock no-error.
            find first oeel where
                       oeel.cono     = g-cono and 
                       oeel.orderno  = oeeh.orderno     and 
                       oeel.ordersuf = oeeh.ordersuf    and 
                       oeel.lineno   = vaelo.linealtno  and 
                       oeel.shipprod = vaeh.shipprod
                       no-lock no-error.
            if (not avail oeeh or not avail oeel) or 
               (avail oeel and oeel.specnstype = "l") or
                (avail oeeh and oeeh.stagecd = 9) then 
              next.


            /* extract proper outside rep fix009 */ 
            assign sic     = ""
                   prctype = ""
/*  fix009 */      repout  = "".   
            find first arsc where arsc.cono = g-cono
                   and arsc.custno = oeeh.custno
                   no-lock no-error.
            if avail(arsc) then
               assign repout  = arsc.slsrepout  /* fix009 */  
                      prctype = arsc.pricetype
                      sic     = string(arsc.siccd[1]).
   
            if oeeh.shipto ne "" then 
            do:
               find first arss where 
                          arss.cono   = g-cono
                      and arss.custno = oeeh.custno  
                      and arss.shipto = oeeh.shipto
                      no-lock no-error.
               if avail(arss) then
                  assign prctype = arss.pricetype.
                     /*    repout  = arss.slsrepout. /* fix009 */   */
            end.

            find first smsn
                 where smsn.cono   = oeeh.cono and
                       smsn.slsrep = (if repout ne "" then  /* fix009 */  
                                        repout 
                                      else    
                                      if oeel.slsrepout = "" then 
                                        oeeh.slsrepout
                                      else 
                                        oeel.slsrepout)
                 no-lock no-error.      

   /* fix009 */  
   
           if avail smsn then
             v-mgr = smsn.mgr.
           else
             v-mgr = "".
           if avail smsn and 
            (smsn.slsrep = "0227" or
             smsn.slsrep = "2128" or
             smsn.slsrep = "2135") then
              v-mgr = "P500".
            if avail smsn and icsdftyp = "Pab" then 
            if avail smsn and 
               substring(v-mgr,1,1) begins "p" and 
               v-mgr <> "p500" then 
               assign d_name = substring(smsn.name,1,20).
            else 
               next. 
           if avail smsn and icsdftyp ne "Pab" then 
            if avail smsn and 
              substring(v-mgr,1,1) begins "p" and 
              v-mgr <> "p500" then 
              next.
            else 
             assign d_name = substring(smsn.name,1,20).
  /* fix009 */  

/*  not used 
/* fix007 */  
            find first icsd where icsd.cono = g-cono and 
                       icsd.whse = oeeh.whse 
                       no-lock no-error.
            if avail icsd and            
               substring(icsd.branchmgr,1,3) ne icsdftyp then 
            next.   
/* fix007 */  
 */ 
          end. /* avail vaelo */

           assign d_prodcat = vaesl.prodcat.
           assign w-vendno = 0.
           if vaesl.arpvendno <> 0 then 
              assign w-vendno  = vaesl.arpvendno
                     w-arpwhse = vaesl.whse.
           else
            do:
              if vaesl.arpwhse <> "" then
                 assign w-arpwhse = vaesl.arpwhse.
              else
                 assign w-arpwhse = vaesl.whse.
            end.
         
           find icsw use-index k-icsw where icsw.cono = g-cono   and
                                            icsw.whse = w-arpwhse and
                                            icsw.prod = vaesl.shipprod
                                            no-lock no-error.


           if avail icsw and icsw.arpvendno <> 0 then
              w-vendno = icsw.arpvendno.
      
/* fix006 */
           assign x_shipprod = vaesl.shipprod
                  x_listprice = if avail icsw then 
                                  icsw.listprice
                                else
                                   0.
/* fix006 */  

           if avail icsw then
           do:
             if icsw.arpvendno <> 0 then
                assign w-vendno    = icsw.arpvendno
                       w-listprice = icsw.listprice
                       w-rebtype   = icsw.rebatety.
             else
             do:
                find b-icsw use-index k-icsw where 
                                      b-icsw.cono = g-cono    and
                                      b-icsw.whse = icsw.arpwhse and
                                      b-icsw.prod = vaesl.shipprod
                                      no-lock no-error.
                if avail b-icsw then
                   assign w-vendno    = b-icsw.arpvendno
                          w-listprice = b-icsw.listprice
                          w-rebtype   = b-icsw.rebatety.
             end. /* else */
           end. /* avail icsw */
     
       assign w-valid  = no.
       run zsdichkpdsc.p(input        g-cono,
                         input        zx-vendno,
                         input        oeeh.custno,
                         input        oeeh.shipto,
                         input        "POWER UNIT",
                         input-output w-valid).
        
  assign w-rebamt2 = 0
         w-rebamt3 = 0
         t-pderamt = 0.
  
  if avail icsw then   
   do: 
    {icss.gfi
        &prod        = icsw.prod
        &icspecrecno = vaesl.icspecrecno
        &lock        = "no"}
    assign
        {speccost.gas
            &com    = "/*"}
         w-rebamt2  = round((
                       icsw.listprice * {speceach.gco} * vaesl.stkqtyship *
                          (if v-speccostty = "" then 1
                           else vaesl.unitconv)  *
                          (if vaesl.stkqtyship >= 0 then 1
                           else -1
                          )),2)
         w-rebamt3  = round((
                       (((100 - d_price) / 100) * icsw.listprice)
                        * {speceach.gco} * vaesl.stkqtyship *
                          (if v-speccostty = "" then 1
                           else vaesl.unitconv)  *
                          (if vaesl.stkqtyship >= 0 then 1
                           else -1
                          )),2).
    t-pderamt = w-rebamt2 - w-rebamt3. 

/* FIX005  */    
    if not w-valid or
       (w-rebtype <> "v" and
        w-rebtype <> "f" and
        w-rebtype <> "c") then
      t-pderamt = 0.
/* FIX005  */    
   end. /* avail icsw */
         /*** using icsp descrip below - because the vaesl.proddesc only get 
              populated on a nonstock ty = "n" *********/
         find icsp where
              icsp.cono = g-cono and 
              icsp.prod = vaesl.shipprod
              no-lock no-error.
         if avail icsp then 
          do:
            assign 
               d_prodcat  = if can-find(t-pcats where t-pcats.pcat = d_prodcat
                              no-lock) then 
                               d_prodcat
                            else 
                            if avail icsp then 
                               icsp.prodcat
                            else
                               d_prodcat               
              d_shipprod = (if avail icsw and icsw.vendprod ne "" then 
                               substring(icsw.vendprod,1,24)
                            else 
                            if avail icsp then    
                               vaesl.shipprod
                            else     
                               d_shipprod).
      end.


     if not can-find(t-pcats where t-pcats.pcat = d_prodcat no-lock) then
       next.

     if avail vaesl and vaesl.nonstockty = "n" then 
        assign d_shipprod = vaesl.shipprod.

     if avail vaesl then 
      do:
       assign d_shipped  = vaesl.qtyship.
       if  vaesl.qtyship > 0 then 
          run move_power_rec.

      end.
     end. /* for each vaesl */   
    end.
  end.
end procedure.

procedure move_power_rec:
do: 
  find first oeeh where oeeh.cono = g-cono and 
             oeeh.orderno   = vaelo.orderaltno  and 
             oeeh.ordersuf  = vaelo.orderaltsuf  
             no-lock no-error.
  if avail oeeh and
           oeeh.transtype = "rm" or 
           oeeh.transtype = "cr" or
           oeeh.transtype = "qu" then 
           leave. 

if p-list then 
 do:
   zelection_type  = "c".
   zelection_char4 = " ". 
   zelection_cust  = oeeh.custno. 
   zelection_vend  = 0.
   run zelectioncheck.
   if zelection_good = false then 
      leave.
 end.

/* fix008      */
 run move_rebate_info(input 23,
                      input vrebloc).

/* fix008      */

/*    vrebate_move1 replaces this move 
   if avail oeeh then 
     do: 
      assign
      substring(vicrec,1,8)     = c_no
      substring(vicrec,9,9)     =
       (if substring(string(oeeh.custno,"zzzzzzzzzzz9"),1,3) <> "" then
          substring(string(oeeh.custno,"999999999999"),4,9)
        else
          substring(string(oeeh.custno,"zzzzzzzzzzz9"),4,9))
      substring(vicrec,18,30)   = oeeh.shiptonm
      substring(vicrec,48,30)   = oeeh.shiptoaddr[1]
      substring(vicrec,78,30)   = oeeh.shiptoaddr[2]
      substring(vicrec,108,30)  = "                              "
      substring(vicrec,138,20)  = substring(oeeh.shiptocity,1,20) 
      substring(vicrec,158,3)   = oeeh.shiptost
      substring(vicrec,161,10)  = oeeh.shiptozip
      substring(vicrec,171,3)   = "US"
      overlay(vicrec,174,2)     = string(month(vaeh.receiptdt),"99") 
      overlay(vicrec,176,4)     = string(year(vaeh.receiptdt))
      substring(vicrec,180,4)   = sic 
      substring(vicrec,184,15)  = d_shipprod
      substring(vicrec,199,8)   = string(d_shipped,"99999999")
      substring(vicrec,207,20)  = d_name
      substring(vicrec,227,10)  = string(d_price,"999999.99-") 
      substring(vicrec,237,15)  = string(oeeh.orderno,"9999999") + "-"
                                + string(oeeh.ordersuf,"99").    
      put stream rebate vicrec at 1 skip.
      export stream rebate_tmp delimiter "," 
             c_no                                
             (if substring(string(oeeh.custno,"zzzzzzzzzzz9"),1,3) <> "" then
                substring(string(oeeh.custno,"999999999999"),4,9)
              else
                substring(string(oeeh.custno,"zzzzzzzzzzz9"),4,9))
             string(oeeh.shiptonm,"x(30)")                       
             string(oeeh.shiptoaddr[1],"x(30)")                  
             string(oeeh.shiptoaddr[2],"x(30)")                  
             "                              "    
             substring(oeeh.shiptocity,1,20)     
             oeeh.shiptost                       
             oeeh.shiptozip                      
             "US"                                
             string(month(vaeh.receiptdt),"99") + 
             string(year(vaeh.receiptdt))        
             sic                                 
             string(d_shipprod,"x(15)")                          
             string(d_shipped,"99999999")         
             string(d_name,"x(20)")                                
             string(d_price,"999999.99-")        
             string(oeeh.orderno,"99999999") + "-" +
             string(oeeh.ordersuf,"99")
             string(rebpct,"9")
             string(t-pderamt,"999999.99")
             string(t-expreb,"999999.99")
             string(t-status,"x")
             string(t-note,"x(30)")
             string(oeeh.custno,"zzzzzzzzzzz9").
    end.
  else
   do: 
      assign
      substring(vicrec,1,8)     = c_no
      substring(vicrec,9,9)     = 
         (if substring(string(vaelo.custno,"zzzzzzzzzzz9"),1,3) <> "" then
           substring(string(vaelo.custno,"999999999999"),4,9)
          else
           substring(string(vaelo.custno,"zzzzzzzzzzz9"),4,9))

      substring(vicrec,18,30)   = (if avail arsc then
                                      arsc.name
                                   else "  ")
      substring(vicrec,48,30)   = "oeeh.shiptoaddr1              "
      substring(vicrec,78,30)   = "oeeh.shiptoaddr2              "
      substring(vicrec,108,30)  = "                              "
      substring(vicrec,138,20)  = "(oeeh.shiptocity,1," 
      substring(vicrec,158,3)   = "oee"
      substring(vicrec,161,10)  = "oeeh.shipt"
      substring(vicrec,171,3)   = "US"
      overlay(vicrec,174,2)     = string(month(vaeh.receiptdt),"99") 
      overlay(vicrec,176,4)     = string(year(vaeh.receiptdt))
      substring(vicrec,180,4)   = sic
      substring(vicrec,184,15)  = d_shipprod
      substring(vicrec,199,8)   = string(d_shipped, "99999999")
      substring(vicrec,207,20)  = d_name
      substring(vicrec,227,10)  = string(d_price, "999999.99-") 
      substring(vicrec,237,15)  = string(vaeh.vano,"9999999") + "-"
                                       + string(vaeh.vasuf,"99"). 
      put stream rebate vicrec at 1 skip. 
      export stream rebate_tmp delimiter "," 
              c_no                                      
              (if substring(string(vaelo.custno,"zzzzzzzzzzz9"),1,3) <> "" then
                 substring(string(vaelo.custno,"999999999999"),4,9)
               else
                 substring(string(vaelo.custno,"zzzzzzzzzzz9"),4,9))
              (if avail arsc then                       
                  arsc.name                             
               else "  ")                               
              "oeeh.shiptoaddr1              "          
              "oeeh.shiptoaddr2              "          
              "                              "          
              "(oeeh.shiptocity,1,"                     
      
              "oee"                                     
              "oeeh.shipt"                              
              "US"                                      
              string(month(vaeh.receiptdt),"99") +             
              string(year(vaeh.receiptdt))              
              sic                                       
              string(d_shipprod,"x(15)")                                
              string(d_shipped, "99999999")      
              string(d_name,"x(20)")                                
              string(d_price, "999999.99-")     
              string(vaeh.vano,"9999999") + "-" + 
              string(vaeh.vasuf,"99")
              string(rebpct,"9")
              string(t-pderamt,"999999.99")
              string(t-expreb,"999999.99")
              string(t-status,"x")
              string(t-note,"x(30)")
              string(vaelo.custno,"zzzzzzzzzzz9").
           
   end.
vicrec = " ".
*/
end.
end procedure.  

{vrebate_moveWH}
{zsapboycheck.i}

PROCEDURE PDSC_pricing:

if oeel.kitrollty = "b" or
   oeel.kitrollty = "p" then 
      leave.

find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 1 and pdsc.custno = oeeh.custno and
                pdsc.prod = oeelk.shipprod and oeelk.whse = oeeh.whse and
                pdsc.startdt le oeeh.invoicedt and
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
                
if not avail pdsc then do:
   find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 2 and pdsc.custno = oeeh.custno and
                pdsc.prod = "p-" + icsw.pricetype and
                pdsc.startdt le oeeh.invoicedt and 
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
   if not avail pdsc then do:              
   find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 2 and pdsc.custno = oeeh.custno and
                pdsc.prod = "l-" + string(icsw.arpvendno,"999999999999") +
                             icsw.prodline and
                pdsc.startdt le oeeh.invoicedt and 
                pdsc.whse = oeeh.whse and
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
   if not avail pdsc then do:
        find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 4 and pdsc.custtype = arsc.pricetype and
                pdsc.prod = icsw.pricetype and pdsc.whse = oeeh.whse and
                pdsc.startdt le oeeh.invoicedt and
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
        if not avail pdsc then do:
           find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 4 and pdsc.custtype = arsc.pricetype and
                pdsc.prod = icsw.pricetype and
                pdsc.startdt le oeeh.invoicedt and
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
     end.    
     end.        
     end.
     end.

     
if not avail pdsc then
   leave.
assign d_price = icsw.listprice.   
repeat x = 1 to 9:
if pdsc.qtybrk[1] = 0  then do:
   if pdsc.prctype = false then 
      d_price = ((100 - pdsc.prcdisc[1]) / 100) * d_price.
   else
      d_price = pdsc.prcmult[1].
   leave.
   end.         
       
if x < 9 then
if oeelk.qtyship lt pdsc.qtybrk[x] then do:
   if pdsc.prctype = false then 
      d_price = ((100 - pdsc.prcdisc[x]) / 100) * d_price.
   else
      d_price = pdsc.prcmult[x].
   leave.         
   end.            
end.
END PROCEDURE.

/* FIX013 

procedure prebuilt-untied-orders: 
/* FIX004  */
 do:
 def buffer bx-icsw for icsw.
 def buffer bx-icsp for icsp.


 for each kpsk where 
          kpsk.cono = g-cono and
          kpsk.prod = oeel.shipprod and
          kpsk.comptype = "C" no-lock:

 if (oeel.qtyship * kpsk.qtyneeded) eq 0 then next.

 
 find first bx-icsw where
            bx-icsw.cono = g-cono    and 
            bx-icsw.whse = oeel.whse and  
            bx-icsw.prod = kpsk.comprod  no-lock no-error.

 
 find first bx-icsp where
            bx-icsp.cono = g-cono    and 
            bx-icsp.prod = kpsk.comprod  no-lock no-error.
 
 assign d_prodline = if avail bx-icsw then bx-icsw.prodline else ""      
        icswptyp   = if avail bx-icsw then bx-icsw.pricetype else ""
        d_prodcat  = if avail bx-icsp then bx-icsp.prodcat else ""
        d_um       = kpsk.unit
        d_shipprod = substring(kpsk.comprod,1,24)
        w-arpwhse  = if avail bx-icsw and bx-icsw.arptype = "w" then
                       bx-icsw.arpwhse
                     else  
                       oeel.whse 
        w-vendno   = if avail bx-icsw then bx-icsw.arpvendno else 0.

     find first icsw where
                icsw.cono = g-cono    and 
                icsw.whse = w-arpwhse and  
                icsw.prod = kpsk.comprod no-lock no-error.




/* fix006 */
     assign x_shipprod = kpsk.comprod
            x_listprice = if avail icsw then 
                            icsw.listprice
                          else
                            0.


     if avail icsw then do:
       if icsw.arpvendno <> 0 then
         assign w-vendno    = icsw.arpvendno
                w-listprice = icsw.listprice
                w-rebtype   = icsw.rebatety.
       else do:
         find b-icsw use-index k-icsw where 
              b-icsw.cono = g-cono    and
              b-icsw.whse = icsw.arpwhse and
              b-icsw.prod = kpsk.comprod
              no-lock no-error.
         if avail b-icsw then
           assign w-vendno    = b-icsw.arpvendno
                  w-listprice = b-icsw.listprice
                  w-rebtype   = b-icsw.rebatety.
       end. /* else */
     end. /* avail icsw */
 

/* fix006 */  
        
        find icsp where
             icsp.cono = g-cono and 
             icsp.prod = kpsk.comprod
             no-lock no-error.
        if avail icsp then 
         assign
         /* FIX004  */
          d_prodcat  = if d_prodcat begins "hvi" or 
         /* FIX004  */
                          d_prodcat = "fvia" then 
                          d_prodcat
                       else 
                       if avail icsp then 
                          icsp.prodcat
                       else
                          d_prodcat               
          d_shipprod = (if avail icsw and icsw.vendprod ne "" then 
                           substring(icsw.vendprod,1,24)
                        else 
                        if avail icsp then    
                           kpsk.comprod
                        else     
                           d_shipprod).
 
 if avail icsw and icsw.whse = "ap-1" then 
    next.
 else 
 if oeel.whse = "ap-1" then 
    next. 
/*  
 /*** with prod cat svia or fvia we only want to extract
      repair and replacement items that are non-power units  ***/ 
 /* FIX002  */
 if (d_prodcat begins "hvi" or d_prodcat = "fvia" or
     d_prodcat = "fvim") then 
    next. 
 /* FIX002  */
*/

if avail smsn then
  v-mgr = smsn.mgr.
else
  v-mgr = "".
if avail smsn and 
   (smsn.slsrep = "0227" or
    smsn.slsrep = "2128" or
    smsn.slsrep = "2135") then
  v-mgr = "P500".

 if icsdftyp = "pab" and 
    v-mgr <> "p500" then do:
 assign tx-cono   = g-cono  
        tx-vendno = zx-vendno  
        tx-custno = oeeh.custno 
        tx-prod   = kpsk.comprod.

 /* FIX003  */
 run zsdihashpct.p(input tx-cono,  
                   input tx-vendno,
                   input tx-custno, 
                   input oeeh.shipto,
                   input tx-prod, 
                   input-output tx-pct). 
/*   FIX003 */ 
   
 d_price = 0.

 if tx-pct = 0 /* or autotype */ then   /* FIX001 adds 'or autotype' */
  do:
   assign d_price = (if avail icsw then 
                        icsw.listprice
                     else 
                        0).
   /* fix011 */
     assign v-custno = oeeh.custno
            v-prod   = kpsk.comprod
            v-whse   = oeeh.whse 
            v-qty    = (oeel.qtyship * kpsk.qtyneeded).
     run zsdipricer4.p (input no,
                        input v-recid,
                        input v-prod,
                        input v-qty,
                        input v-whse,
                        input v-custno,
                        input-output v-pdrecid,
                        input-output v-prdprctype,
                        input-output v-priced,
                        input-output v-pddisc,
                        input-output v-pdlist).
     assign d_price = v-priced.

/* fix011 */
   end.
 else 
 if tx-pct > 0 then  
   assign d_price = 
    ((100 - tx-pct) / 100) *  (if avail icsw then  
                                 icsw.listprice   
                               else                
                                 0).     
 end. /* if pab */
 else 
 do: /***** temporary for february 2004 ****/

   assign d_price = 
    ((100 - tx-pct) / 100) *  (if avail icsw then  
                                 icsw.listprice   
                               else                
                                 0).     
 /*
     assign d_price = if oeel.discpct = 0 then
                         oeelk.price
                     else 
                        ((100 - oeel.discpct) / 100) * oeelk.price
 */  assign
           d_shipped = oeel.qtyship * kpsk.qtyneeded
           creditdebit = "+".
     assign v-custno = oeeh.custno
            v-prod   = kpsk.comprod
            v-whse   = oeeh.whse 
            v-qty    = (oeel.qtyship * kpsk.qtyneeded).
     run zsdipricer4.p (input no,
                        input v-recid,
                        input v-prod,
                        input v-qty,
                        input v-whse,
                        input v-custno,
                        input-output v-pdrecid,
                        input-output v-prdprctype,
                        input-output v-priced,
                        input-output v-pddisc,
                        input-output v-pdlist).
     assign d_price = v-priced.
    /*   run PDSC_pricing.   */
 end.
 
 t-pderamt = 0.
     
 if w-rebtype = "V" then
    assign d_price = 15.
 else 
 if w-rebtype = "C" then
    assign d_price = 4.
 else 
 if w-rebtype = "F" then
    assign d_price = 10.
 else   
    assign d_price = 15.




 /* FIX004  */
 if (d_prodcat begins "hvi" or  
     d_prodcat = "fvia" or
     d_prodcat = "fvim") and  
 /* FIX004  */
     (oeel.qtyship * kpsk.qtyneeded) > 0 then
  do:
    d_shipped = (oeel.qtyship * kpsk.qtyneeded).
/* fix008      */
    run move_rebate_info(input 23,
                         input vrebloc).
/* fix008      */
  end.
 end.  /* loop back to oeelk */  
 end. /*** un tied orders do ***/
end procedure. 



procedure prebuilt-comp-untied-orders: 
/* FIX004  */
 do:
 def buffer bx-icsw for icsw.
 def buffer bx-icsp for icsp.


 for each kpsk where 
          kpsk.cono = g-cono and
          kpsk.prod = oeelk.shipprod and
          kpsk.comptype = "C" no-lock:


/*     
    if oeelk.orderno = 3396429 then do:
      message "C" kpsk.prod kpsk.comprod kpsk.seqno oeelk.ordersuf.
      pause.
    end.
*/ 
 
 if (oeelk.qtyship * kpsk.qtyneeded) eq 0 then next.

 
 find first bx-icsw where
            bx-icsw.cono = g-cono    and 
            bx-icsw.whse = oeel.whse and  
            bx-icsw.prod = kpsk.comprod  no-lock no-error.

 
 find first bx-icsp where
            bx-icsp.cono = g-cono    and 
            bx-icsp.prod = kpsk.comprod  no-lock no-error.
 
 assign d_prodline = if avail bx-icsw then bx-icsw.prodline else ""      
        icswptyp   = if avail bx-icsw then bx-icsw.pricetype else ""
        d_prodcat  = if avail bx-icsp then bx-icsp.prodcat else ""
        d_um       = kpsk.unit
        d_shipprod = substring(kpsk.comprod,1,24)
        w-arpwhse  = if avail bx-icsw and bx-icsw.arptype = "w" then
                       bx-icsw.arpwhse
                     else  
                       oeel.whse 
        w-vendno   = if avail bx-icsw then bx-icsw.arpvendno else 0.

     find first icsw where
                icsw.cono = g-cono    and 
                icsw.whse = w-arpwhse and  
                icsw.prod = kpsk.comprod no-lock no-error.




/* fix006 */
     assign x_shipprod = kpsk.comprod
            x_listprice = if avail icsw then 
                            icsw.listprice
                          else
                            0.


     if avail icsw then do:
       if icsw.arpvendno <> 0 then
         assign w-vendno    = icsw.arpvendno
                w-listprice = icsw.listprice
                w-rebtype   = icsw.rebatety.
       else do:
         find b-icsw use-index k-icsw where 
              b-icsw.cono = g-cono    and
              b-icsw.whse = icsw.arpwhse and
              b-icsw.prod = kpsk.comprod
              no-lock no-error.
         if avail b-icsw then
           assign w-vendno    = b-icsw.arpvendno
                  w-listprice = b-icsw.listprice
                  w-rebtype   = b-icsw.rebatety.
       end. /* else */
     end. /* avail icsw */
 



/* fix006 */  
        
        find icsp where
             icsp.cono = g-cono and 
             icsp.prod = kpsk.comprod
             no-lock no-error.
        if avail icsp then 
         assign
         /* FIX004  */
          d_prodcat  = if d_prodcat begins "hvi" or 
         /* FIX004  */
                          d_prodcat = "fvia" then 
                          d_prodcat
                       else 
                       if avail icsp then 
                          icsp.prodcat
                       else
                          d_prodcat               
          d_shipprod = (if avail icsw and icsw.vendprod ne "" then 
                           substring(icsw.vendprod,1,24)
                        else 
                        if avail icsp then    
                           kpsk.comprod
                        else     
                           d_shipprod).
 
 if avail icsw and icsw.whse = "ap-1" then 
    next.
 else 
 if oeel.whse = "ap-1" then 
    next. 
/*  
 /*** with prod cat svia or fvia we only want to extract
      repair and replacement items that are non-power units  ***/ 
 /* FIX002  */
 if (d_prodcat begins "hvi" or d_prodcat = "fvia" or
     d_prodcat = "fvim") then 
    next. 
 /* FIX002  */
*/

if avail smsn then
  v-mgr = smsn.mgr.
else
  v-mgr = "".
if avail smsn and 
   (smsn.slsrep = "0227" or
    smsn.slsrep = "2128" or
    smsn.slsrep = "2135") then
  v-mgr = "P500".

 if icsdftyp = "pab" and 
    v-mgr <> "p500" then do:
 assign tx-cono   = g-cono  
        tx-vendno = zx-vendno  
        tx-custno = oeeh.custno 
        tx-prod   = kpsk.comprod.

 /* FIX003  */
 run zsdihashpct.p(input tx-cono,  
                   input tx-vendno,
                   input tx-custno, 
                   input oeeh.shipto,
                   input tx-prod, 
                   input-output tx-pct). 
/*   FIX003 */ 
   
 d_price = 0.

 if tx-pct = 0 /* or autotype */ then   /* FIX001 adds 'or autotype' */
  do:
   assign d_price = (if avail icsw then 
                        icsw.listprice
                     else 
                        0).
   /* fix011 */
     assign v-custno = oeeh.custno
            v-prod   = kpsk.comprod
            v-whse   = oeeh.whse 
            v-qty    = (oeelk.qtyship * kpsk.qtyneeded).
     run zsdipricer4.p (input no,
                        input v-recid,
                        input v-prod,
                        input v-qty,
                        input v-whse,
                        input v-custno,
                        input-output v-pdrecid,
                        input-output v-prdprctype,
                        input-output v-priced,
                        input-output v-pddisc,
                        input-output v-pdlist).
     assign d_price = v-priced.

/* fix011 */
   end.
 else 
 if tx-pct > 0 then  
   assign d_price = 
    ((100 - tx-pct) / 100) *  (if avail icsw then  
                                 icsw.listprice   
                               else                
                                 0).     
 end. /* if pab */
 else 
 do: /***** temporary for february 2004 ****/

   assign d_price = 
    ((100 - tx-pct) / 100) *  (if avail icsw then  
                                 icsw.listprice   
                               else                
                                 0).     
 /*
     assign d_price = if oeel.discpct = 0 then
                         oeelk.price
                     else 
                        ((100 - oeel.discpct) / 100) * oeelk.price
 */  assign
           d_shipped = oeelk.qtyship * kpsk.qtyneeded
           creditdebit = "+".
     assign v-custno = oeeh.custno
            v-prod   = kpsk.comprod
            v-whse   = oeeh.whse 
            v-qty    = (oeelk.qtyship * kpsk.qtyneeded).
     run zsdipricer4.p (input no,
                        input v-recid,
                        input v-prod,
                        input v-qty,
                        input v-whse,
                        input v-custno,
                        input-output v-pdrecid,
                        input-output v-prdprctype,
                        input-output v-priced,
                        input-output v-pddisc,
                        input-output v-pdlist).
     assign d_price = v-priced.
    /*   run PDSC_pricing.   */
 end.

      
 if w-rebtype = "V" then
    assign d_price = 15.
 else 
 if w-rebtype = "C" then
    assign d_price = 4.
 else 
 if w-rebtype = "F" then
    assign d_price = 10.
 else   
    assign d_price = 15.


 
 t-pderamt = 0.
 /* FIX004  */
 if (d_prodcat begins "hvi" or  
     d_prodcat = "fvia" or
     d_prodcat = "fvim") and  
 /* FIX004  */
     (oeelk.qtyship * kpsk.qtyneeded) > 0 then
  do:
    d_shipped = (oeelk.qtyship * kpsk.qtyneeded).
/* fix008      */
    run move_rebate_info(input 23,
                         input vrebloc).
/* fix008      */
  end.
 end.  /* loop back to oeelk */  
 end. /*** un tied orders do ***/
end procedure. 



 FIX013 */


     
