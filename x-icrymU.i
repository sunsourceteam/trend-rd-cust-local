&if defined(user_temptable) = 2 &then
/* {p-rptbeg.i}  */
{zsapboydef.i}
def buffer b-oeeh for oeeh.
def buffer b2-oeeh for oeeh.
def buffer b-zsastz for zsastz.
def buffer b2-zsastz for zsastz.
def buffer b3-zsastz for zsastz.
def var p-regval        as c        no-undo.
def var b-pickeddt      as date     format "99/99/99"       no-undo.
def var e-pickeddt      as date     format "99/99/99"       no-undo.
def var b-percal        like glet.percal                    no-undo.
def var e-percal        like glet.percal                    no-undo.
def var b-whse          as char     format "x(4)"           no-undo.
def var e-whse          as char     format "x(4)"           no-undo.
def var b-region        as char     format "x(4)"           no-undo.
def var e-region        as char     format "x(4)"           no-undo.
/* def var p-export   as char format "x(24)" no-undo.    */
def var p-format   as int                 no-undo.  
def var p-detailprt as logical            no-undo.
def var pk_good     as logical            no-undo.
def var p-whse     like icsw.whse         no-undo.
def var p-list     as logical             no-undo.
def var var-total       as integer        no-undo.
def var var-total-lt    as integer        no-undo.      
def var var-total2      as integer        no-undo.
def var var-total2-lt   as integer        no-undo.
def var final-tot       as integer        no-undo.
def var final-tot2      as integer        no-undo.
def var total-errors    as integer        no-undo.
def var var-stg2        as integer        no-undo.
def var var-stg2w       as integer        no-undo.
def var final-stg2      as integer        no-undo.
def var var-stg2amt     as decimal        no-undo.
def var var-stg2wamt    as decimal        no-undo.
def var final-stg2amt   as decimal        no-undo.
def var v-weekdays      as integer        no-undo.
def var v-value         as decimal        no-undo.
def var v-gain-loss     as decimal        no-undo.
def var var-1           as decimal        no-undo.
def var var-2           as decimal        no-undo.
def var v-cyclecnt      as integer        no-undo.
def var v-corr          as integer        no-undo.
def var v-sdayship      as decimal        no-undo.
def var v-shipacc       as decimal        no-undo.
def var v-invgl         as decimal        no-undo.
def var v-invacc        as decimal        no-undo.
def var p-year          like glet.yr      no-undo.
define temp-table whsetemp no-undo
  field whse        like icsd.whse
  field name        as char          format "x(16)"
  field region      like icsd.region 
  field ordprt      as integer       format ">>>,>>9"
  field ordnotsd    as integer       format ">>>,>>9"
  field shiperr     as integer       format ">,>>9"
  field invvalue    as decimal       format "->>>,>>>,>99.99"
  field stg2cnt     as integer       format ">>>,>>9"
  field stg2amt     as decimal       format "->>>,>>>,>99.99"
  field gainloss    as decimal       format "->>>,>>>,>99.99"
  field invcnt      as integer       format ">>,>>>,>>9"
  field invcorr     as integer       format ">>,>>9"
  field sdayship    as decimal       format "->>>.99"
  field goal1       as decimal       format "->>>.99"
  field shipacc     as decimal       format "->>>.99"
  field goal2       as decimal       format "->>>.99"
  field invacc      as decimal       format "->>>.99"
  field goal3       as decimal       format "->>>.99"
  field invgl       as decimal       format "->>>.99"
  field goal4       as decimal       format "->>>.99"
  
  index k-whse
        whse.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_drpttable) = 2 &then
/* Use to add fields to the report table */
   field whsetempid as recid
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_forms) = 2 &then
form 
  "Name"              at 1 
  "Region"            at 6
  "Inv $"             at 23 
  "OrdPrt"            at 34  
  "Not Ship"          at 44     
  "Ship"              at 55
  "Inv $"             at 65
  /* new part */
  
  "Inv Items"         at 83
  "Inv"               at 95  
 "Same-Day"           at 101
  "Goal"              at 111
  "Ship"              at 120 
  "Goal"              at 130
  "Cycle"             at 140   
  "Goal"              at 150 
/* "Inv"               at 159 */  
/* "Goal"              at 170  */         
  "Whse"              at 1
  "Stg 2"             at 23
  "Stg2Cnt"           at 34
  "Same Day"          at 44
  "Errors"            at 55
  "Gain/Loss"         at 65
  
  
  "Counted"           at 83
  "Corr"              at 95
  "Ship"              at 101
  "Accur"             at 120
  "Accur"             at 140
/* "Gain/Loss"         at 159  */
   with frame f-titles down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top. 
form 
  "~015"
  whsetemp.whse        at 1 
  whsetemp.region      at 6           
  whsetemp.name        at 1
  whsetemp.invvalue    at 17   
  whsetemp.ordprt      at 34    
  whsetemp.ordnotsd    at 44   
  whsetemp.shiperr     at 55   
  whsetemp.gainloss    at 61
  whsetemp.invcnt      at 78   
  whsetemp.invcorr     at 92   
  whsetemp.sdayship    at 99
  "%"                  at 107
  whsetemp.goal1       at 109
  "%"                  at 117
  whsetemp.shipacc     at 119
  "%"                  at 127
  whsetemp.goal2       at 129
  "%"                  at 137 
  whsetemp.invacc      at 139
  "%"                  at 147
  whsetemp.goal3       at 149
  "%"                  at 157 
  whsetemp.invgl       at 159
  /* "%"               at 167   */
  whsetemp.goal4       at 169
  /* "%"               at 177   */
  whsetemp.stg2amt     at 17
  whsetemp.stg2cnt     at 34
  with frame f-whsedetail down NO-BOX NO-LABELS no-underline
            WIDTH 178. 
form
  v-final                at 1
  v-astrik               at 2   format "x(2)"
  v-summary-lit2         at 4   format "x(27)"
  "~015"
  v-amt4           at 17  format "->>>,>>>,>>9.99"  
  v-amt1           at 34  format ">>>,>>9"
  v-amt2           at 44  format ">>>,>>9"
  v-amt3           at 55  format "->>>9"
  v-amt5           at 61  format "->>>,>>>,>>9.99"
  v-amt6           at 78  format ">>,>>>,>>9"
  v-amt7           at 92  format ">>,>>9"
  v-amt9           at 99  format "->>>.99"
  "%"              at 107   
  v-amt10          at 109 format "->>>.99"
  /* "%"              at 117   */
  v-amt11          at 119 format "->>>.99"
  "%"              at 127  
  v-amt12          at 129 format "->>>.99"
  /* "%"              at 137  */
  v-amt13          at 139 format "->>>.99"
  "%"              at 147      
  v-amt14          at 149 format "->>>.99"
  /* "%"              at 157       */
  v-amt15          at 159 format "->>>.99"
/* "%"              at 167  removed from total */
  v-amt16          at 169 format "->>>.99"
  /* "%"              at 177  */
  "~015"           at 178
  v-amt17          at 17  format "->>>,>>>,>>9.99" 
  v-amt18          at 34  format ">>>,>>9"
with frame f-tot width 178
 no-box no-labels.  
form 
  "~015"           at 1
with frame f-skip width 178 no-box no-labels.  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_extractrun) = 2 &then
 run sapb_vars.
 run extract_data.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_exportstatheaders) = 2 &then
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  
  put stream expt unformatted " " + 
  v-del + v-del + v-del + v-del + v-del + v-del + v-del +
  v-del
  v-del
  v-del
  v-del
/*  v-del  */
  chr(13).
  
  assign export_rec = 
                      export_rec          +
                      "Whse"              + v-del + 
                      "Location"          + v-del +
                      "Region"            + v-del +
                      "Orders Printed"    + v-del +
                      "Not Ship Same Day" + v-del +
                      "Ship Errors"       + v-del +
                      "Curr Inv $"        + v-del +
                      "Inv Gain/Loss"     + v-del +
                      "Inv Counted"       + v-del +
                      "Inv Corrected"     + v-del +
                      "Same-Day Ship"     + v-del +
                      "Goal"              + v-del +
                      "Ship Accuracy"     + v-del + 
                      "Goal"              + v-del +
                      "Cycle Cnt"         + v-del + 
                      "Goal".               
               /*     "Inv Gain/Loss"     + v-del +    */
               /*     "Goal".                          */
                      
    if p-detailprt then 
       assign export_rec = export_rec + v-del.               
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_foreach) = 2 &then
for each whsetemp no-lock:
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_drptassign) = 2 &then
 /* If you add fields to the dprt table this is where you assign them */
 drpt.whsetempid = recid(whsetemp)
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_viewheadframes) = 2 &then
/* Since frames are unique to each program you need to put your
   views and hides here with your names */
if not p-exportl then
  do:
  if not x-stream then
    do:
    hide frame f-titles.
    end.
  else
   do:
    hide stream xpcd frame f-titles.
    end.
  
  end.
if not p-exportl  then
  do:
  if not x-stream then
    do:
    page.
    view frame f-titles.
    end.
  else  
    do:
    page stream xpcd.
    view stream xpcd frame f-titles.
    end.
   
  end.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_drptloop) = 2 &then
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_detailprint) = 2 &then
   /* Comment this out if you are not using detail 'X' option  */
   if p-detailprt and 
      entry(u-entitys,drpt.sortmystery,v-delim) <> 
      fill("~~",length(entry(u-entitys,drpt.sortmystery,v-delim))) then 
     do:
     /* this code skips the all other lines for printing detail */
     /* next line would be your detail print procedure */
     if p-exportl then
        run print-dtl-export(input drpt.whsetempid).
     else   
        run print-dtl(input drpt.whsetempid).
     end.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_finalprint) = 2 &then
if not p-exportl then
 do:
   assign v-per1 = 
                 if t-amounts1[u-entitys + 1] = 0 then                     
                   0                                                
                 else                                               
                  ((t-amounts1[u-entitys + 1] - t-amounts2[u-entitys + 1])
                    / t-amounts1[u-entitys + 1]) * 100      
         v-per2 = 
                 if t-amounts1[u-entitys + 1] = 0 then                     
                   0                                                
                 else                                               
                   ((t-amounts1[u-entitys + 1] - t-amounts3[u-entitys + 1])
                    / t-amounts1[u-entitys + 1]) * 100      
         
         v-per3 = 
                 if t-amounts6[u-entitys + 1] = 0 then                     
                   0                                                
                 else                                               
                   ((t-amounts6[u-entitys + 1] - t-amounts7[u-entitys + 1])
                    / t-amounts6[u-entitys + 1]) * 100      
         
         v-per4 =
                 if t-amounts4[u-entitys + 1] = 0 then                     
                   0                                                
                 else                                               
                   ((t-amounts4[u-entitys + 1] + t-amounts5[u-entitys + 1])
                    / t-amounts4[u-entitys + 1]) * 100
                    
         v-per5 =
                 if t-amounts8[u-entitys + 1] = 0 then                     
                   0                                                
                 else                                               
                   t-amounts10[u-entitys + 1] / t-amounts8[u-entitys + 1].
   
 
 if not x-stream then 
   do:
   display 
    v-final
    v-astrik
    v-summary-lit2   
/* PTD */
    t-amounts4[u-entitys + 1]   @ v-amt4    
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts2[u-entitys + 1]   @ v-amt2
    t-amounts3[u-entitys + 1]   @ v-amt3
    t-amounts5[u-entitys + 1]   @ v-amt5
    t-amounts6[u-entitys + 1]   @ v-amt6
    t-amounts7[u-entitys + 1]   @ v-amt7
    v-per1                      @ v-amt9   /* added */ 
 /* v-per5                      @ v-amt10 */
 /* t-amounts9[u-entitys + 1]   @ v-amt9  */
 /* t-amounts10[u-entitys + 1]  @ v-amt10 */
 /* t-amounts11[u-entitys + 1]  @ v-amt11 */
    v-per2                      @ v-amt11  /* added */
 /* t-amounts12[u-entitys + 1]  @ v-amt12 */
 /* t-amounts13[u-entitys + 1]  @ v-amt13 */
    v-per3                      @ v-amt13  /* added */
 /* t-amounts14[u-entitys + 1]  @ v-amt14 */
 /* t-amounts15[u-entitys + 1]  @ v-amt15 */
 /* v-per4                      @ v-amt15  removed this from total */
 /* t-amounts16[u-entitys + 1]  @ v-amt16 */    
    t-amounts17[u-entitys + 1]  @ v-amt17
    t-amounts18[u-entitys + 1]  @ v-amt18
 
   
   
   with frame f-tot.
   down with frame f-tot.
   end.
 else
   do:
   display stream xpcd
    v-final
    v-astrik
    v-summary-lit2
    t-amounts4[u-entitys + 1]   @ v-amt4    
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts2[u-entitys + 1]   @ v-amt2
    t-amounts3[u-entitys + 1]   @ v-amt3
    t-amounts5[u-entitys + 1]   @ v-amt5
    t-amounts6[u-entitys + 1]   @ v-amt6
    t-amounts7[u-entitys + 1]   @ v-amt7
    v-per1                      @ v-amt9  /* added */
 /* v-per5                      @ v-amt10 */
 /* t-amounts9[u-entitys + 1]   @ v-amt9  */
 /* t-amounts10[u-entitys + 1]  @ v-amt10 */
 /* t-amounts11[u-entitys + 1]  @ v-amt11 */
     v-per2                     @ v-amt11 /* added */
 /* t-amounts12[u-entitys + 1]  @ v-amt12 */
 /* t-amounts13[u-entitys + 1]  @ v-amt13 */
    v-per3                      @ v-amt13 /* added */
 /* t-amounts14[u-entitys + 1]  @ v-amt14 */
 /* t-amounts15[u-entitys + 1]  @ v-amt15 */
 /* v-per4                      @ v-amt15 removed this from total */
 /* t-amounts16[u-entitys + 1]  @ v-amt16 */    
    t-amounts17[u-entitys + 1]  @ v-amt17
    t-amounts18[u-entitys + 1]  @ v-amt18
    with frame f-tot.
  
   down stream xpcd with frame f-tot.
   end.
      
 end.
else
  do:
  assign export_rec =  v-astrik.
  do v-inx4 = 1 to u-entitys:
    if v-totalup[v-inx4] = "n" then
       next.
    if v-inx4 = 1 then
      v-summary-lit2 = "Final".
    else
      v-summary-lit2 = "".
    if v-cells[v-inx4]= "2" then
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec + v-summary-lit2 + v-del + " ".
     
      end.
    else
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec +  v-summary-lit2.
      end.
  end.
     assign v-per1 = 
                 if t-amounts1[u-entitys + 1] = 0 then                     
                   0                                                
                 else                                               
                  ((t-amounts1[u-entitys + 1] - t-amounts2[u-entitys + 1])
                    / t-amounts1[u-entitys + 1]) * 100      
         v-per2 = 
                 if t-amounts1[u-entitys + 1] = 0 then                     
                   0                                                
                 else                                               
                   ((t-amounts1[u-entitys + 1] - t-amounts3[u-entitys + 1])
                    / t-amounts1[u-entitys + 1]) * 100      
         
         v-per3 = 
                 if t-amounts6[u-entitys + 1] = 0 then                     
                   0                                                
                 else                                               
                   ((t-amounts6[u-entitys + 1] - t-amounts7[u-entitys + 1])
                    / t-amounts6[u-entitys + 1]) * 100      
         
         v-per4 =
                 if t-amounts4[u-entitys + 1] = 0 then                     
                   0                                                
                 else                                               
                   ((t-amounts4[u-entitys + 1] + t-amounts5[u-entitys + 1])
                    / t-amounts4[u-entitys + 1]) * 100
 
          v-per5 =
                 if t-amounts8[u-entitys + 1] = 0 then                     
                   0                                                
                 else                                               
                   t-amounts10[u-entitys + 1] / t-amounts8[u-entitys + 1].
   
 
  
  /* added here */
  assign export_rec = export_rec + v-del + v-del + 
      string(t-amounts1[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts2[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts3[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts4[u-entitys + 1],"->>>>>>>>9.99") + v-del +
      string(t-amounts5[u-entitys + 1],"->>>>>>>>9.99") + v-del +
      string(t-amounts6[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts7[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(v-per1,"->>>>9.999") + v-del +
      ""                          + v-del +
      string(v-per2,"->>>>9.999") + v-del +
      ""                          + v-del +
      string(v-per3,"->>>>9.999") + v-del +
      ""                          + v-del +
      string(v-per4,"->>>>9.999").   /* added */
      
      /*
      string(t-amounts9[u-entitys + 1],"->>>.99")    + v-del +
      string(t-amounts10[u-entitys + 1],"->>>.99")    + v-del +
      string(t-amounts11[u-entitys + 1],"->>>.99")    + v-del +
      string(t-amounts12[u-entitys + 1],"->>>.99")    + v-del +
      string(t-amounts13[u-entitys + 1],"->>>.99")    + v-del +
      string(t-amounts14[u-entitys + 1],"->>>.99")    + v-del +
      string(t-amounts15[u-entitys + 1],"->>>.99")    + v-del +
      string(t-amounts16[u-entitys + 1],"->>>.99")    + v-del +
      string(t-amounts17[u-entitys + 1],"->>>.99"]    + v-del + 
      string(t-amounts18[u-entitys + 1],"->>>.99"].  */
    
  assign export_rec = export_rec + chr(13).    
  put stream expt unformatted export_rec.
  end.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_summaryframeprint) = 2 &then
  assign v-per1 = 
                 if t-amounts1[v-inx2] = 0 then                     
                   0                                                
                 else                                               
                  ((t-amounts1[v-inx2] - t-amounts2[v-inx2])
                    / t-amounts1[v-inx2]) * 100      
         v-per2 = 
                 if t-amounts1[v-inx2] = 0 then                     
                   0                                                
                 else                                               
                   ((t-amounts1[v-inx2] - t-amounts3[v-inx2])
                    / t-amounts1[v-inx2]) * 100      
         
         v-per3 = 
                 if t-amounts6[v-inx2] = 0 then                     
                   0                                                
                 else                                               
                   ((t-amounts6[v-inx2] - t-amounts7[v-inx2])
                    / t-amounts6[v-inx2]) * 100      
         
         v-per4 =
                 if t-amounts4[v-inx2] = 0 then                     
                   0                                                
                 else                                               
                   ((t-amounts4[v-inx2] + t-amounts5[v-inx2])
                    / t-amounts4[v-inx2]) * 100
                    
         v-per5 =
                 if t-amounts8[v-inx2] = 0 then                     
                   0                                                
                 else                                               
                   t-amounts10[v-inx2] / t-amounts8[v-inx2].
   
 
 
 
    if not x-stream then
      do:
      display 
        v-final
        v-astrik
        v-summary-lit2
        t-amounts4[v-inx2]   @ v-amt4   
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        t-amounts3[v-inx2]   @ v-amt3
        t-amounts5[v-inx2]   @ v-amt5
        t-amounts6[v-inx2]   @ v-amt6
        t-amounts7[v-inx2]   @ v-amt7
        v-per1               @ v-amt9   
        v-per2               @ v-amt11
        v-per3               @ v-amt13   /* added */
    /*  v-per4               @ v-amt15   removed this from total */
        
   /*   t-amounts9[v-inx2]   @ v-amt9
        t-amounts10[v-inx2]   @ v-amt10
        t-amounts11[v-inx2]   @ v-amt11
        t-amounts12[v-inx2]   @ v-amt12
        t-amounts13[v-inx2]   @ v-amt13
        t-amounts14[v-inx2]   @ v-amt14
        t-amounts15[v-inx2]   @ v-amt15
        t-amounts16[v-inx2]   @ v-amt16   */    
        t-amounts17[v-inx2]   @ v-amt17
        t-amounts18[v-inx2]   @ v-amt18
        with frame f-tot.
      down with frame f-tot.
      end.
    else
      do:
      display stream xpcd
        v-final
        v-astrik
        v-summary-lit2
        t-amounts4[v-inx2]   @ v-amt4   
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        t-amounts3[v-inx2]   @ v-amt3
        t-amounts5[v-inx2]   @ v-amt5
        t-amounts6[v-inx2]   @ v-amt6
        t-amounts7[v-inx2]   @ v-amt7
        v-per1               @ v-amt9
        v-per2               @ v-amt11
        v-per3               @ v-amt13  /* added */
   /*   v-per4               @ v-amt15  removed this from total */
        
   /*   t-amounts9[v-inx2]   @ v-amt9
        t-amounts10[v-inx2]   @ v-amt10
        t-amounts11[v-inx2]   @ v-amt11
        t-amounts12[v-inx2]   @ v-amt12
        t-amounts13[v-inx2]   @ v-amt13
        t-amounts14[v-inx2]   @ v-amt14
        t-amounts15[v-inx2]   @ v-amt15
        t-amounts16[v-inx2]   @ v-amt16  */ 
        t-amounts17[v-inx2]   @ v-amt17
        t-amounts18[v-inx2]   @ v-amt18
 
        with frame f-tot.
      down stream xpcd with frame f-tot.
      end.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_summaryputexport) = 2 &then
  assign v-per1 = 
                 if t-amounts1[v-inx2] = 0 then                     
                   0                                                
                 else                                               
                  ((t-amounts1[v-inx2] - t-amounts2[v-inx2])
                    / t-amounts1[v-inx2]) * 100      
         v-per2 = 
                 if t-amounts1[v-inx2] = 0 then                     
                   0                                                
                 else                                               
                   ((t-amounts1[v-inx2] - t-amounts3[v-inx2])
                    / t-amounts1[v-inx2]) * 100      
         
         v-per3 = 
                 if t-amounts6[v-inx2] = 0 then                     
                   0                                                
                 else                                               
                   ((t-amounts6[v-inx2] - t-amounts7[v-inx2])
                    / t-amounts6[v-inx2]) * 100      
         
         v-per4 =
                 if t-amounts4[v-inx2] = 0 then                     
                   0                                                
                 else                                               
                   ((t-amounts4[v-inx2] + t-amounts5[v-inx2])
                    / t-amounts4[v-inx2]) * 100
                    
         v-per5 =
                 if t-amounts8[v-inx2] = 0 then                     
                   0                                                
                 else                                               
                   t-amounts10[v-inx2] / t-amounts8[v-inx2].
   
 
 
/* PTD */                          
    assign export_rec = v-astrik + v-del + v-del + export_rec.
    assign export_rec = export_rec + v-del +
      string(t-amounts1[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts2[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts3[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts4[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts5[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts6[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts7[v-inx2],"->>>>>>>>9") + v-del +
      string(v-per1,"->>>>9.999") + v-del + 
      ""                          + v-del +
      string(v-per2,"->>>>9.999") + v-del +
      ""                          + v-del + 
      string(v-per3,"->>>>9.999") + v-del +
      ""                          + v-del + 
      string(v-per4,"->>>>9.999").  /* added */
      
      
      
  /*  string(t-amounts9[v-inx2],"->>>.99")    + v-del +
      string(t-amounts10[v-inx2],"->>>.99")    + v-del +
      string(t-amounts11[v-inx2],"->>>.99")    + v-del +
      string(t-amounts12[v-inx2],"->>>.99")    + v-del +
      string(t-amounts13[v-inx2],"->>>.99")    + v-del +
      string(t-amounts14[v-inx2],"->>>.99")    + v-del +
      string(t-amounts15[v-inx2],"->>>.99")    + v-del +
      string(t-amounts16[v-inx2],"->>>.99").  */
 
      
      
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_procedures) = 2 &then
procedure sapb_vars:
  assign
       b-pickeddt       =   date(sapb.rangebeg[1])
       e-pickeddt       =   date(sapb.rangeend[1])
       b-region         =   sapb.rangebeg[2]
       e-region         =   sapb.rangeend[2]
       b-whse           =   sapb.rangebeg[3]
       e-whse           =   sapb.rangeend[3]
       b-percal         =   int(sapb.rangebeg[4])
       e-percal         =   int(sapb.rangeend[4])  
       p-format         =   int(sapb.optvalue[1])
       p-export         =   sapb.optvalue[3]
       /* p-detailprt   =   if sapb.optvalue[2] = "yes" then yes else no */
       /* p-whse        =   sapb.optvalue[3]   */
       p-list           =   if sapb.optvalue[2] = "yes" then yes else no.
       p-year           =   int(sapb.optvalue[4]).
  assign p-detailprt    =   yes
         p-detail       =   "D"
         p-portrait     =   yes.
/*
do i = 1 to 2:
    if i = 1 then
       v-perin      = sapb.rangebeg[6].
    else
       v-perin      = sapb.rangeend[6].
                        
    {p-rptper.i}
         
    if i = 1 then
       b-percal     = v-perout.
    else
       e-percal     = v-perout.
end. /* for i = 1 ... */
*/
       
       
  /* stuff for zsapboycheck.i */
  
  assign  zzl_begwhse = b-whse
          zzl_endwhse = e-whse
          zel_begwhse = b-whse
          zel_endwhse = e-whse.
          
  if p-list then do:
     {zsapboyload.i}                         
     
  end.        
           
/* Zelection_matrix has to have an extent for each selection type              
   right now we have. During the load these booleans are set showing what      
   options have been selected.                                                  
   s - sales rep               ....Index 1                                     
   c - customer number         ....Index 2                                     
   p - product cat             ....Index 3                                     
   b - buyer                   ....Index 4                                     
   t - Order types             ....Index 5                                     
   w - Warehouse               ....Index 6                                     
   v - Vendor number           ....Index 7                                     
   N - Vendor Name             ....Index 8                                      
 PLEASE KEEP THIS DOCUMENTATION UPDATED WHEN CHANGED                           
 */                                                                            
assign zelection_matrix [6] = (if zelection_matrix[6] > 1 then /* Whse  */   
                                 zelection_matrix[6]                         
                               else                                           
                                 1).                                          
                            
  
  run formatoptions.
end.
/* ------------------------------------------------------------------------- */
procedure formatoptions:
/* ------------------------------------------------------------------------- */
   
   
  assign p-portrait = false.
  /* if false then the report will print in LANDSCAPE 178 character */
  
  if sapb.optvalue[1] > "0" and sapb.optvalue[1] ne "99" then 
    do:
    p-optiontype = " ".
    find notes where notes.cono = g-cono and
                     notes.notestype = "zz" and
                     notes.primarykey = "icrym" and  /* tbxr program */
                     notes.secondarykey = string(p-format) no-lock no-error.
    if not avail notes then
      do:
      display "Format is not valid cannot process request".
      assign p-optiontype = "r"
             p-sorttype   = ">,"
             p-totaltype  = "l"
             p-summcounts = "a".
      return.
      end.               
   
    assign p-optiontype = notes.noteln[1]
           p-sorttype   = notes.noteln[2]
           p-totaltype  = notes.noteln[3]
           p-summcounts = notes.noteln[4]
           p-export     = "    "
           p-register   = notes.noteln[5]
           p-registerex = notes.noteln[6].
    end.     
  else
  if sapb.optvalue[1] = "99" then 
    do:
    assign p-register   = ""
           p-registerex = "".
    run reportopts(input sapb.user5, 
                   input-output p-optiontype,
                   input-output p-sorttype,  
                   input-output p-totaltype,
                   input-output p-summcounts,
                   input-output p-register,
                   input-output p-regval).
    end.
 end.
   
 
procedure extract_data:
assign 
v-sdayship = 0               
v-shipacc  = 0               
v-invacc   = 0               
v-invgl    = 0               
final-tot     = 0
final-tot2 = 0
final-stg2 = 0
final-stg2amt = 0
total-errors   = 0
v-value   = 0
v-gain-loss  = 0
v-cyclecnt   = 0
v-corr       = 0
var-1        = 0
var-2        = 0.
for each icsd use-index k-icsd where icsd.cono = g-cono and
                    icsd.salesfl = yes        and
                    icsd.custno = 0           and
                   (icsd.whse    >= b-whse    and
                    icsd.whse    <= e-whse)   and
                    icsd.whse    <> "accf"    and
                    icsd.whse    <> "vend"    and
                    icsd.whse    <> "acct"    and
                    icsd.whse    <> "accw"    and
                    icsd.whse    <> "ap-1"    and
                   (icsd.region  >= b-region  and
                    icsd.region  <= e-region) 
                    no-lock:
                    
 zelection_type = "w".              
 zelection_char4 = icsd.whse.       
 zelection_cust = 0.                
 run zelectioncheck.                
 if zelection_good = false then     
    do:                              
      pk_good = false.                 
      next.            
    end.                             
                 
assign var-total = 0
       var-total-lt = 0
       var-stg2 = 0
       var-stg2amt = 0.        
        
 for each oeeh use-index k-invproc where oeeh.cono = g-cono and
                                         oeeh.whse = icsd.whse and
                                        (oeeh.pickeddt >= b-pickeddt and
                                         oeeh.pickeddt <= e-pickeddt) and
                                        (oeeh.updtype = "M" or
                                         oeeh.updtype = "P") and
                                         oeeh.orderdisp <> "W" and
                                         oeeh.transtype <> "RM" and
                                        (oeeh.shipviaty <> "p/u" and
                                         oeeh.shipviaty <> "SDLV" and
                                         oeeh.shipviaty <> "DLVS" and
                                         oeeh.shipviaty <> "DELV")
                                         no-lock:   
  /*   if oeeh.shipdt = oeeh.pickeddt then   */
          assign var-total = var-total + 1.                 
      
       if oeeh.shipdt > oeeh.pickeddt then
          assign var-total-lt = var-total-lt + 1.
                     
end. /* oeeh */ 
/* stage 2 part */
 for each b2-oeeh use-index k-whsestagecd where b2-oeeh.cono = g-cono and
                                            b2-oeeh.whse = icsd.whse and
                                            b2-oeeh.stagecd = 2 and
                                            b2-oeeh.pickeddt < TODAY 
                                            no-lock:   
       
                 
          run countdays.p ( input TODAY,
                            input b2-oeeh.pickeddt,
                            output v-weekdays).
           /*
           message "b2-oeeh.pickeddt" b2-oeeh.pickeddt
                   "v-weekdays" v-weekdays. pause.                  
           */        
           /* 
           message b2-oeeh.orderno b2-oeeh.pickeddt "v-weekdays" v-weekdays.   
                   pause.
           */ 
           if v-weekdays >= 5 then  /* changed it to 5 becasue we want to count                                        from the picked date.  And not the day                                        after  3/10/16 - 2/17/16 is 5 days not 6                                        days  */                
         
              assign var-stg2 =  var-stg2 + 1      
                     var-stg2amt = var-stg2amt + b2-oeeh.totlineamt.
                     
end. /* b2-oeeh */ 
/* stage 2 part */
assign var-total2 = 0
       var-total2-lt = 0
       final-tot = 0
       final-tot2 =0
       v-sdayship = 0
    /* var-stg2w = 0
       var-stg2wamt = 0   */
       final-stg2 = 0
       final-stg2amt = 0.
              
 
  for each wteh use-index k-fmwhse where wteh.cono = g-cono and
                                         wteh.shipfmwhse = icsd.whse and
                                        (wteh.printeddt >= b-pickeddt and
                                         wteh.printeddt <= e-pickeddt)
                                         no-lock:
                                         
         assign var-total2 = var-total2 + 1.                                  
       
       if wteh.shipdt > wteh.printeddt then 
          assign var-total2-lt = var-total2-lt + 1.
       
  end.  /* wteh */
                     
assign final-tot = var-total + var-total2
       final-tot2 = var-total-lt + var-total2-lt
/* added */
       
       final-stg2 = var-stg2 + var-stg2w
       final-stg2amt = var-stg2amt + var-stg2wamt.
       /*
       message "final-stg2" final-stg2
               "final-stg2amt" final-stg2amt. pause.
       */
find zsastz where zsastz.cono = g-cono and
                  zsastz.codeiden = "SameDayShipping" and
                  zsastz.primarykey =  icsd.whse
                  no-lock no-error.
                  
     if avail zsastz then do:
        v-sdayship = ((final-tot - final-tot2) / final-tot) * 100. 
     end.   /* if avail zsastz */
     else
     next.
assign total-errors = 0
       v-shipacc = 0.
  for each b-oeeh use-index k-invproc where b-oeeh.cono = g-cono and         
                                            b-oeeh.whse = icsd.whse and        
                                           (b-oeeh.invoicedt >= b-pickeddt and  
                                            b-oeeh.invoicedt <= e-pickeddt) and
                                           (b-oeeh.stagecd >= 4 and            
                                            b-oeeh.stagecd <= 5 ) and  
                                           (b-oeeh.transtype  = "RM" or        
                                            b-oeeh.transtype  = "CR")           
                                            no-lock:                           
                                                                               
                                                                      
      for each oeel use-index k-oeel where oeel.cono = g-cono and
                                           oeel.orderno = b-oeeh.orderno and
                                           oeel.ordersuf = b-oeeh.ordersuf and
                                           oeel.specnstype <> "l" and
                                          (oeel.crreasonty  = "sa" or      
                                           oeel.crreasonty = "sd" or        
                                           oeel.crreasonty = "se" or        
                                           oeel.crreasonty = "sh" or        
                                           oeel.crreasonty = "sm" or        
                                           oeel.crreasonty = "so" or        
                                           oeel.crreasonty = "sr" or        
                                       /*  oeel.crreasonty = "st" or  */      
                                           oeel.crreasonty = "sw" or        
                                           oeel.crreasonty = "we") 
                                           no-lock:
                    
           assign total-errors =  total-errors + 1. 
           
      end.  /* oeel */
  end. /* b-oeeh */
find b-zsastz where b-zsastz.cono = g-cono and
                    b-zsastz.codeiden = "ShippingAccuracy" and
                    b-zsastz.primarykey =  icsd.whse
                    no-lock no-error.
                  
     if avail b-zsastz then do:
        v-shipacc = ((final-tot - total-errors) / final-tot) * 100. 
     end.   /* if avail b-zsastz */
     else
     next.
assign v-value = 0
       v-cyclecnt = 0.
for each icsw use-index k-whse where icsw.cono = g-cono and
                                     icsw.whse = icsd.whse 
                                     no-lock:
                    
      
  assign v-value = v-value + icsw.avgcost * (icsw.qtyonhand +              
                                             icsw.qtyunavail + icsw.qtyrcv).    
end.  /* added smaf */
                                                 
for each zsdiicsep use-index  k-zsdiicsepd where zsdiicsep.cono = g-cono and
                                       zsdiicsep.whse = icsd.whse and
                                      (zsdiicsep.transdt >= b-pickeddt and
                                       zsdiicsep.transdt <= e-pickeddt)
                                       no-lock break by zsdiicsep.icswlstcntdt 
                                       by zsdiicsep.prod:
                         
     if not last-of(zsdiicsep.prod) then next.
        assign v-cyclecnt =  v-cyclecnt + 1. 
    
end.  /*  for each */
/* end.  removed smaf  each icsw */
                                          
assign var-1 = 0
       var-2 = 0
       v-gain-loss = 0
       v-invgl = 0.
for each glet use-index k-period where glet.cono = g-cono and
                                       glet.gldivno = icsd.divno and
                                       glet.gldeptno = icsd.gldeptno[3] and
                                       glet.glacctno = 31200 and
                                      (glet.percal  >= b-percal and
                                       glet.percal <=  e-percal) and
                                       glet.yr = p-year
                                   /* (glet.transno = 1 or
                                       glet.transno = 2)  */
                                       no-lock: 
    
    if glet.transcd = 1 then do:
       assign var-1 = var-1 + glet.amount.
    end.   
    else 
    if glet.transcd = 2 then do:
       assign var-2 = var-2 + glet.amount.
       
    end.   
    
       assign v-gain-loss = var-1 + var-2.
       
       
end. /* for each glet */
find b2-zsastz where b2-zsastz.cono = g-cono and
                     b2-zsastz.codeiden = "InvAccuracy" and /* InvGainLoss" */
                     b2-zsastz.primarykey =  icsd.whse
                     no-lock no-error.
                  
     if avail b2-zsastz then do:
        v-invgl = ((v-value + v-gain-loss) / v-value) * 100. 
     end.   /* if avail zsastz */
     else
     next.                               
assign v-corr = 0
  /*     v-cyclecnt = 0   */
       v-invacc = 0.
for each icet use-index k-postdt where icet.cono = g-cono and
                                       icet.whse = icsd.whse and
                                      (icet.postdt >= b-pickeddt and
                                       icet.postdt <= e-pickeddt) and
                                       icet.transtype = "sa" and
                                       icet.transproc = "icepu"
                                       no-lock:
    assign v-corr = v-corr + 1. 
end.
/*
find first zsdiicsep use-index  k-zsdiicsepd where zsdiicsep.cono = g-cono and
                                       zsdiicsep.whse = icsd.whse and
                                       zsdiicsep.prod = icsw.prod and
                                      (zsdiicsep.icswlstcntdt >= b-pickeddt and
                                       zsdiicsep.icswlstcntdt <= e-pickeddt)
                                       no-lock no-error.
                         
   if avail zsdiicsep then 
    
    assign v-cyclecnt =  v-cyclecnt + 1. 
    
 /*    if zsdiicsep.qtycnt <> zsdiicsep.qtyexp then   
       assign v-corr = v-corr + 1. */
/* end.  was for each */
*/
find b3-zsastz where b3-zsastz.cono = g-cono and
                     b3-zsastz.codeiden = "InvGainLoss" and
                     b3-zsastz.primarykey =  icsd.whse
                  no-lock no-error.
                  
     if avail b3-zsastz then do:
        v-invacc = if v-cyclecnt = 0 then 0 else
           ((v-cyclecnt - v-corr) / v-cyclecnt) * 100.
     end.   /* if avail b3-zsastz */
     else
     next.                         
/*                         
display
v-cyclecnt
v-corr
v-invacc.                         
*/
        
        create whsetemp.
        assign 
          whsetemp.whse        =    icsd.whse
          whsetemp.name        =    icsd.city
          whsetemp.region      =    icsd.region
          whsetemp.ordprt      =    final-tot 
          whsetemp.ordnotsd    =    final-tot2
          whsetemp.stg2cnt     =    final-stg2
          whsetemp.stg2amt     =    final-stg2amt
          whsetemp.shiperr     =    total-errors
          whsetemp.invvalue    =    v-value
          whsetemp.gainloss    =    v-gain-loss
          whsetemp.invcnt      =    v-cyclecnt
          whsetemp.invcorr     =    v-corr
          whsetemp.sdayship    =    v-sdayship
          whsetemp.goal1       =   (dec(zsastz.secondarykey) * 100)
          whsetemp.shipacc     =    v-shipacc
          whsetemp.goal2       =   (dec(b-zsastz.secondarykey) * 100)
          whsetemp.invacc      =    v-invacc
          whsetemp.goal3       =   (dec(b2-zsastz.secondarykey) * 100)
          whsetemp.invgl       =    v-invgl
          whsetemp.goal4       =   (dec(b3-zsastz.secondarykey) * 100).
          
        end.   /* create */
    end. /* icsd */
    
    {zsapboycheck.i}                     
    
    
    
/*---------------------------------------------------------------------------*/
procedure print-dtl-export:
/*---------------------------------------------------------------------------*/
define input parameter ip-recid as recid no-undo.
find whsetemp where recid(whsetemp) = ip-recid no-lock no-error.
put stream expt unformatted
    "  "
    v-del
    whsetemp.whse      
    v-del
    whsetemp.name        
    v-del
    whsetemp.region
    v-del
    whsetemp.ordprt     
    v-del
    whsetemp.ordnotsd   
    v-del
    whsetemp.shiperr    
    v-del
    whsetemp.invvalue    
    v-del
    whsetemp.gainloss    
    v-del
    whsetemp.invcnt      
    v-del
    whsetemp.invcorr     
    v-del
    whsetemp.sdayship       
    v-del
    whsetemp.goal1       
    v-del
    whsetemp.shipacc       
    v-del
    whsetemp.goal2       
    v-del
    whsetemp.invacc       
    v-del
    whsetemp.goal3       
/*  v-del
    whsetemp.invgl
    v-del
    whsetemp.goal4       */
    chr(13).
end.  /* procedure print-dtl-export  */
/*---------------------------------------------------------------------------*/
procedure print-dtl:
/*---------------------------------------------------------------------------*/
define input parameter ip-recid as recid no-undo.
find whsetemp where recid(whsetemp) = ip-recid no-lock no-error.
display
  whsetemp.whse        
  whsetemp.name        
  whsetemp.region
  whsetemp.invvalue
  whsetemp.ordprt       
  whsetemp.ordnotsd    
  whsetemp.shiperr     
  whsetemp.gainloss    
  whsetemp.invcnt      
  whsetemp.invcorr     
  whsetemp.sdayship       
  whsetemp.goal1       
  whsetemp.shipacc       
  whsetemp.goal2       
  whsetemp.invacc       
  whsetemp.goal3       
/*  whsetemp.invgl
  whsetemp.goal4       */
 whsetemp.stg2amt
 whsetemp.stg2cnt
  with frame f-whsedetail.
  down with  frame f-whsedetail.
  
  
end. /*  procedure print-dtl  */
  
  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
