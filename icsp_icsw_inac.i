assign inact_fl   = yes
       ic-invdt   = ?
       ic-purchdt = ?.

/* PROVE PRODUCT SHOULD STAY ACTIVE */
find icsp where icsp.cono = 1 and
                icsp.prod = oeelb.shipprod and
                icsp.statustype = "A" 
                no-lock no-error.
if not avail icsp then 
  assign inact_fl = no.
/*
if icsp.prod matches "*CORE CHARGE*" then
  assign inact_fl = no.
if icsp.descrip[1] matches "*CORE CHARGE*" then
  assign inact_fl = no.
if icsp.descrip[2] matches "*CORE CHARGE*" then
  assign inact_fl = no.
*/
if inact_fl = yes then
  do:
  find first icsw where icsw.cono = 1 and
                        icsw.whse > "AZZZ" and
                        icsw.prod = oeelb.shipprod and
                        icsw.statustype = "S"
                        no-lock no-error.
  if avail icsw then
    assign inact_fl = no.
end.
if inact_fl = yes then
  do:
  find first icsw where icsw.cono = 1 and
                        icsw.whse > "AZZZ" and
                        icsw.prod = oeelb.shipprod and
                        icsw.statustype = "o" and
                        icsw.replcostdt >= TODAY - 395
                        no-lock no-error.
  if avail icsw then
    assign inact_fl = no.
end.
/* check if the product is a substitute */
if inact_fl = yes then
  do:
  find first icsec where icsec.cono = 1 and
                         icsec.rectype = "s" and
                         icsec.altprod = oeelb.shipprod
                         no-lock no-error.
  if avail icsec then
    assign inact_fl = no.
end.
if inact_fl = yes then
  do:
  for each icsw where icsw.cono = 1 and
                      icsw.whse > "AZZZ" and
                      icsw.prod = oeelb.shipprod and
                      icsw.statustype = "O"
                      no-lock:
    if icsw.qtyonhand  > 0 or
       icsw.qtybo      > 0 or
       icsw.qtycommit  > 0 or
       icsw.qtyintrans > 0 or
       icsw.qtyonorder > 0 or
       icsw.qtyreservd > 0 or
       icsw.qtydemand  > 0 then
      assign inact_fl = no.
    if inact_fl = yes then
      do:
      /*
      assign ic-purchdt = ?
             ic-invdt   = ?.
      */
      for each icet where icet.cono = 1 and
                          icet.whse = icsw.whse and
                          icet.prod = icsw.prod and
                         (icet.transtype = "in" or
                          icet.transtype = "re")
                          no-lock:
        if icet.transtype = "re" and (ic-purchdt = ? or
                                      ic-purchdt < icet.postdt) then
          assign ic-purchdt = icet.postdt.
        if icet.transtype = "in" and (ic-invdt = ? or
                                      ic-invdt < icet.postdt) then
          assign ic-invdt = icet.postdt.
      end. /* each icet */
    end. /* inact_fl = yes */
  end. /* icsw */
  /* if activity within 5 years, do not inactivate */
  if ic-invdt   <> ? and ic-invdt   > TODAY - 1825 then    
    assign inact_fl = no.
  if ic-purchdt <> ? and ic-purchdt > TODAY - 1825 then
    assign inact_fl = no.
  if ic-invdt = ? then
    do:
    /* make sure the product was not on a Direct Order */
    find last oeel where oeel.cono = 1 and 
                         oeel.shipprod = oeelb.shipprod and
                         oeel.transtype = "DO" and
                         oeel.specnstype <> "L" 
                         no-lock no-error.  
    if avail oeel then
      assign inact_fl = no.
  end.
  if inact_fl = yes then
    do:
    /* make sure the product is not on a quote with a later cancel date */ 
    find first f-oeelb where f-oeelb.cono = 1 and
                             f-oeelb.batchnm <> oeelb.batchnm and
                             f-oeelb.shipprod = oeelb.shipprod and
                             f-oeelb.specnstype <> "L" and
                             f-oeelb.reasunavty   <> "EQ"
                             no-lock no-error.
    if avail f-oeelb then
      assign inact_fl = no.
    if inact_fl = yes then
      do:
      for each f-oeelb where f-oeelb.cono      = 1 and
                             f-oeelb.shipprod  = icsp.prod and
                             f-oeelb.seqno     = 1
                             no-lock:
        find f-oeehb where f-oeehb.cono       = f-oeelb.cono and    
                           f-oeehb.batchnm    = f-oeelb.batchnm and
                           f-oeehb.seqno      = f-oeelb.seqno and
                           f-oeehb.sourcepros = "Quote" and
                           f-oeehb.canceldt   > TODAY - 183
                           no-lock no-error.
        if avail f-oeehb then
          assign inact_fl = no. 
      end. /* for each f-oeelb */
    end. /* inact_fl still yes */
  end. 
end. /* inact_fl = yes */   

if inact_fl = yes then
  do:
  if can-find(first kpsk where 
                    kpsk.cono = 1 and
                    kpsk.comptype = "c" and
                    kpsk.comprod = oeelb.shipprod
                    no-lock) then
   assign inact_fl = no.
end.
                              
     
/* Inactivate */
if inact_fl = yes then
  do:
  find icsp where icsp.cono = oeelb.cono and    
                  icsp.prod = oeelb.shipprod   
                  no-error.     
  if avail icsp then
    do:
    export stream INACAUD delimiter "~011"
    "ICSP" icsp.prod "" icsp.statustype icsp.enterdt.       
    assign count = count + 1.
    assign icsp.statustype = "i"    
           icsp.transdt    = TODAY
           icsp.transtm    = substr(string(TIME,"HH:MM"),1,2) +
                             substr(string(TIME,"HH:MM"),4,2)
           icsp.operinit   = "INAC"
           icsp.xxda1      = TODAY.
    for each icsd where icsd.cono = 1 and
                      icsd.salesfl = yes and
                      icsd.whse > "AZZZ"
                      no-lock:
      find icsw where icsw.cono = 1 and
                      icsw.whse = icsd.whse and
                      icsw.prod = icsp.prod and
                      icsw.statustype <> "X"
                      no-lock no-error.
      if avail icsw then
        do:
        find i-icsw where i-icsw.cono = icsw.cono and
                          i-icsw.whse = icsw.whse and
                          i-icsw.prod = icsw.prod
                          no-error.
        export stream INACAUD delimiter "~011"
          "ICSW" i-icsw.prod i-icsw.whse i-icsw.statustype i-icsw.enterdt.
        assign i-icsw.statustype = "X"
               i-icsw.transtm    = substr(string(TIME,"HH:MM"),1,2) +
                                   substr(string(TIME,"HH:MM"),4,2)   
               i-icsw.transdt    = TODAY
               i-icsw.operinit   = "INAC"
               i-icsw.xxda1      = TODAY.   
      end. /* avail icsw */    
    end. /* each icsd */
  end. /* avail icsp */ 
end. /* inact_fl = no */