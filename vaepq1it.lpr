/****************************************************************************
 Include: vaepq1it.lpr.

****************************************************************************/
/*e

    Variables that must be loaded for this include are:

    Required parameters used in this include are:
     &stitle    - Used to display appropriate section header
     &qthead    - The Returned Quantity heading
     &xdash1    - Extra dashes required to underscore &qthead
     &xdash2    - Extra dashes to mark a space for write-in of returned qty

    Optional parameters used in this include are:
*/

if vaesl.nonstockty ne "n" then 
 {w-icsp.i vaesl.shipprod no-lock}

if v-vafstinvfl = yes then
 do:
    assign
        v-vafstinvfl   = no
        s-vadata       = {&stitle}.
/* printable ----  
    if p-detail then
      do:
      display s-vadata with frame f-vadata.
      down with frame f-vadata.

      assign
         s-vadata                  = ""
         substring(s-vadata,1,35)  = "-----------------------------------"
         substring(s-vadata,36,35) = "-----------------------------------"
         substring(s-vadata,71,56) = 
               "--------------------------------------------------------"
         substring(s-vadata,127,1) = {&xdash1}. 
     display s-vadata with frame f-vadata.
     down with frame f-vadata.
     end.
  */
 end.

/*d Print the notes for the section */
if v-seqhld ne vaes.seqno and vaes.sctntype = "it" then 
do:
  if avail vaes then 
   if vaes.notesfl ne "" then
    do:
      assign v-seqhld  = vaes.seqno
             s-comdata = "Section " + vaes.sctntype +
                         " - " +  "Seq# " + string(vaesl.seqno,"zz9") +
                         " Note(s): ".

/* printable -----
      if p-detail then
        do:
        display s-comdata no-label with frame f-com3.
        down with frame f-com3.
        assign s-comdata = "--------------------------------".
        display s-comdata no-label with frame f-com3.
        down with frame f-com3.
         {vaepi1nt.lpr &notestype    = ""fs""
                       &primarykey   = "string(vaes.vano) + '-' +
                                        string(vaes.vasuf)"
                       &secondarykey = "string(vaes.seqno)"}
        end.
*/
    
    end. /* if vaes.notesfl ne "" for section */
end.

assign s-lncolon      = ":" 
       s-lnlineno     = vaesl.lineno
       s-lnshipprod   = if p-desconly then                        
                          ""                                     
                        else                                      
                        if substring(vaesl.user5,1,24) ne "" then 
                           substring(vaesl.user5,1,24)            
                        else   
                           vaesl.shipprod
       s-lncostty     = " "
                        /*
                        if vaesl.timeactty = "e" then "Estimate"
                        else "  Actual"
                        */
       s-lnhours      = if can-do("it,is",vaesl.sctntype) then 
                         truncate(vaesl.timeelapsed / 3600,0)
                        else 0
       s-lnminutes    = if can-do("it,is",vaesl.sctntype) then 
                           (vaesl.timeelapsed mod 3600) / 60   
                        else 0                                 
       s-lnprodcost   = if p-prtprc then                 
                string((vaesl.prodcost * 1.03) + 
                      ((vaesl.prodcost * 1.03) * p-margpct)
                        ,"zzzzz9.99") 
                        else ""          
       s-lnnetamt     = if p-prtprc then                 
                string((vaesl.netamt * 1.03) + 
                      ((vaesl.netamt * 1.03) * p-margpct),"zzzzz9.99") 
                        else ""  
       s-lnproddesc   = if avail icsp then 
                           icsp.descrip[1]
                        else 
                           v-invalid.
   
   assign ittot = ittot + ((vaesl.netamt * 1.03) + 
                          ((vaesl.netamt * 1.03) * p-margpct)). 
                  
   if substring(vaesl.user5,25,1) = "n" /* and vaesl.sctntype ne "ii"  */ then 
     do:
     assign qa-miscamt    = qa-miscamt + dec (s-lnprodcost)
            qa-misctotamt = qa-misctotamt + dec(s-lnnetamt).
  
     end.
   else
   if substring(vaesl.user5,25,1) ne "n"  /* or vaesl.sctntype = "ii" */ then 
     do: 
     if p-detail then
       do:
       display 0 @ s-lnlineno format "zzz"   
               s-lnshipprod  
               s-lncostty      
               s-lnhours     
               s-lncolon
               s-lnminutes   
               s-lnprodcost  
               s-lnnetamt    
               s-lnproddesc  
       with frame f-vaepq1it.
       end.                      
     end.                 

