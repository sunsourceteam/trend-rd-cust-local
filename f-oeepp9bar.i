/* f-oeepp1.i 1.4 09/08/93 */
/*h*****************************************************************************
  INCLUDE      : f-oeepp1.i
  DESCRIPTION  : Forms and form variable definitions for oe & wt pick tickets
  USED ONCE?   : No (wtep1.p oeepp1.p)
  AUTHOR       : rhl
  DATE WRITTEN : 11/04/89
  CHANGES MADE : 
    10/14/91 mwb; TB#  4762 Changed page# to fit pre-printed form
    11/11/92 mms; TB#  8669 Rush - Sort rush orders to top
    09/07/93 mms; TB# 12619 Display cod message if wt tied to cod order;
        added s-codmsg not used in oe
    03/20/96 kr;  TB# 12406 Variables defined in form, moved out of form 
    06/12/96 jms; TB# 21254 (10A) Develop Value Add Module
        added parameters &section, &shiptonm, &custno, and &custpo
    08/05/96 jms; TB# 21254 (10A) Develop Value Add Module - 
        Cleaned-up lines 19&20
    06/04/98 JPH; ITB# 1  Display Customer PO on DO WT'S.    
    11/04/98 DBF; ITB# 2  Add requesting warehouse info to WT's.
                  sdix01  Use Rxlaser form and Pack/Pick Separation
*******************************************************************************/

/*tb 12406 03/26/96 kr; Variables defined in form f-oeepp1.i */

form header
    v-tof      format "x(12)"   /* sdix01 */
    
/*   "~033(0Y~033(s0p4.69h12.0v0s0b0T" at 1   das - barcode on packing slip */
    v-delta1                          at 1
    bc-orderno                        at 26
/*    "~033(10U~033(s0p17h0s0b4102T"    at 55 */
    v-delta2                          at 55
    skip(1)    
    /* skip(3)  */                   /* sdix01 */
    {&skiper}  

    s-lit1a                           at   2
    s-doctype                         at  12
    s-reprint                         at  53
    s-lit1b                           at  93
    s-rush                            at  12
    s-codmsg                          at  25
    s-filltype                        at  53
    sasc.upcvno                       at  95
    {&com}
    {&head}.{&pref}no                 at 106
    "-"                               at 113
    {&head}.{&pref}suf                at 114
    s-seqno                           at 125
    /{&com}* */
 
    {&zcom2}
    {&head}.{&pref}no                 at 119
    "-"                               at 126
    {&head}.{&pref}suf                at 127
    /{&zcom2}* */
    s-lit3a                           at   4

    {&com}
    {&custno}                     at  12
    "-"                           at  24
    {&shipto}                     at  25
    /{&com}*  */
                                             
    s-orderdisp                       at  53
    s-lit3b                           at  94
    s-pricedesc                       at   2     /* das - moved up a line */
    {&orderdt}                        at  94

    {&com}
    {&custpo}                     at 104
    /{&com}*  */

    page-num - v-pageno format "zzz9" at 129    /* das - barcode */
    v-delta1                            at 1
 /*   "~033(0Y~033(s0p4.69h12.0v0s0b0T" at 1  */
     bc-pono                          at 26
      /*   "~033(10U~033(s0p17h0s0b4102T"    at 50  */
    v-delta2                            at 50
                
/*    s-pricedesc                       at   2 */ /* das - barcode see above */
    s-lit6a                           at   2 
    s-billname                        at  12
    /* s-lit6b                           at  48     */
    x-sascconm                        at 64
    "Shippers Reference:"             at  104
    v-shipref                         at  124
    v-frtin                           at  132
    s-billaddr1                       at  12
    x-sascaddr1                       at  64
    s-billaddr2                       at  12
    x-sascaddr2                       at  64
    s-billcity                        at  12
    x-corrcity                        at  64

/*    skip(1)  */      /* das - barcode - remove space between bill/ship to */
    {&skiper}

/* ITB# 1 */
/*    s-custpolit                      at   95            */
/*    s-custpo                         at   111           */
    s-lit11a                          at   2
    s-shiptonm                        at  12
    s-lit11b                          at  50
    s-lit11c                          at  81
/* dbf beg ITB# 2 */
/*    "Requesting Whse"                 at  95          */
/* dbf end ITB# 2 */
    s-shiptoaddr[1]                   at  12
    {&section}.shipinstr              at  50
    s-stagearea                       at  81
/* dbf beg ITB# 2 */
/*    s-reqwhse                         at  95            */
/*    s-reqname                         at  100           */
/* dbf end ITB# 2 */
    s-shiptoaddr[2]                   at  12
    s-lit12a                          at  50
    s-lit12b                          at  81
    s-lit12e                          at 100
    s-lit12c                          at 110
    s-lit12d                          at 120
    s-shipcity                        at  12
    s-whsedesc                        at  50
    s-shipvia                         at  81
    s-cod                             at  94
    {&section}.reqshipdt              at 100
    {&pickdt}                         at 110
    s-terms                           at 120

    skip(1)

    s-lit14a                          at   1
    s-lit14b                          at  79
    s-lit15a                          at   1
    s-lit15b                          at  79
    s-lit16a                          at   1
    s-lit16b                          at  79
with frame f-head no-box no-labels width 132 page-top.

form header     skip(1)

    s-linecntx                        at   1
    s-lit40a                          at   5
    s-lit40b                          at  20
    v-noprintx                        at  47
    s-lit40c                          at  58
    s-totqtyshp                       at  77
    s-lit40d                          at  91
    s-totlineamt                      at 115
    s-lit41a                          at   1
    s-lit41b                          at  79
    s-totcubes                        at  42
    s-totweight                       at  56
    s-lit42a                          at   1  format "x(15)"
    s-lit42b                          at  95
with frame f-tot no-box no-labels width 132 page-bottom.
