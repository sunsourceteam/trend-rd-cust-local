/* default stream is Air-Draulics */ 
def stream airb.
def stream warren.
def stream norris.
def stream cuno.
def stream waste.
define variable vicrec as character format "x(251)" no-undo.
define variable co as integer no-undo.
define variable d_prodline like oeel.prodline no-undo.
def var d_shipprod as c format "x(15)" no-undo.
def var d_shipped as decimal format "99999999" no-undo.
def var d_price as decimal format "9999999.99" no-undo.
def var d_name as c format "x(20)" no-undo.
def var sic as c format "x(4)" no-undo.
def var c_no as c format "x(8)" no-undo.
def var d_line as i format 999 no-undo.
def var d_um as c format "x(4)" no-undo.
def var yyyy as c format "x(4)" no-undo.
def var creditdebit as c format "x(1)" no-undo.
def var bypass_sw as logical no-undo.
def var company as c format "x(3)" no-undo.
def var ordernum as char format "x(12)" no-undo.
def var x as i no-undo.

output to "/rd/conv/mesg.log".
output stream airb to "/rd/conv/00016700.air".  
output stream warren to "/rd/conv/00164900.war".  
output stream norris to "/rd/conv/00654900.nor".  
output stream cuno to "/rd/conv/cuno.txt".  
    output stream waste to "/rd/conv/T380.txt".  

co = 1.

for each oeeh where oeeh.cono = co and oeeh.stagecd > 3 and
         year(oeeh.invoicedt) = year(today) and
         month(oeeh.invoicedt) = (month(today) - 1) no-lock.  

assign company = "".         

if oeeh.custno = 138572 or oeeh.custno = 214789 or oeeh.custno = 215200 or    
   oeeh.custno = 239200 or oeeh.custno = 155800 or oeeh.custno = 249200 or
   oeeh.custno = 264571 or oeeh.custno = 275200 or oeeh.custno = 284082 or
   oeeh.custno = 288800 or oeeh.custno = 386650 or oeeh.custno = 413030 or  
   oeeh.custno = 239200 then  
      assign c_no = "00164900"
             company = "war".
      
if oeeh.custno = 848126 or oeeh.custno = 181906 or oeeh.custno = 545780 or
   oeeh.custno = 143451 or oeeh.custno = 676021 or oeeh.custno = 170581 or
   oeeh.custno = 191941 or oeeh.custno = 181907 or oeeh.custno = 848127 or
   oeeh.custno = 2204768 or oeeh.custno = 368450 or oeeh.custno = 463989 or   
   oeeh.custno = 467594 or oeeh.custno = 479408 or oeeh.custno = 524071 or   
   oeeh.custno = 545569 or oeeh.custno = 775856 or oeeh.custno = 575244 or
   oeeh.custno = 717113 or oeeh.custno = 748378 or oeeh.custno = 821655 or   
   oeeh.custno = 844835 or oeeh.custno = 850345 or oeeh.custno = 853979 or        oeeh.custno = 885650 or oeeh.custno = 316804 or oeeh.custno = 331363 or
   oeeh.custno = 605586 or oeeh.custno = 656175 or oeeh.custno = 826575 or
   oeeh.custno = 272825 or oeeh.custno = 291370 or oeeh.custno = 307070 or
   oeeh.custno = 410699 or oeeh.custno = 546781 or oeeh.custno = 178651 or
   oeeh.custno = 676021 or oeeh.custno = 272825 or oeeh.custno = 445480 or
   oeeh.custno = 166311 or oeeh.custno = 178576 or oeeh.custno = 206514 or
   oeeh.custno = 240271 or oeeh.custno = 311656 or oeeh.custno = 597500 or
   oeeh.custno = 410700 or oeeh.custno = 457897 or oeeh.custno = 748378 or
   oeeh.custno = 495988 or oeeh.custno = 854105 or
   oeeh.custno = 598711 or oeeh.custno = 598718 or
   oeeh.custno = 882215 or oeeh.custno = 866730 or oeeh.custno = 347462 or
   oeeh.custno = 356395 or oeeh.custno = 805134  then
        assign c_no = "00654900"
               company = "nor".     
      
if oeeh.custno = 102900 or oeeh.custno = 574500 or oeeh.custno = 471250 or
   oeeh.custno = 768625 then
       assign c_no = "00016700"
          company = "air".

if oeeh.custno = 392100 or oeeh.custno = 392101 or oeeh.custno = 392103 or
   oeeh.custno = 392109 or oeeh.custno = 392087 or oeeh.custno = 6366669 or
   oeeh.custno = 392110 or oeeh.custno = 392090 or oeeh.custno = 392106 or
   oeeh.custno = 392111 or oeeh.custno = 392102 or oeeh.custno = 392093 then
      assign company = "wm".
          

find first arsc where arsc.cono = 1 and arsc.custno = oeeh.custno
     no-lock no-error.
if avail(arsc) then
    assign sic = string(arsc.siccd[1]).
           substring(vicrec,180,4) = sic.
for each oeel where oeel.cono = co and oeel.orderno = oeeh.orderno and
         oeel.ordersuf = oeeh.ordersuf no-lock.
         
if oeel.kitfl = yes then next.         
if oeel.qtyship = 0 then next.

assign d_prodline = oeel.prodline 
       d_line = oeel.lineno
       d_um = oeel.unit
       d_shipprod = oeel.shipprod.

if oeel.specnstype = "n" and
   d_shipprod = "000000" then
       d_shipprod = substring(oeel.proddesc,1,15).
       
if oeel.arpvendno = 9793750 or
   oeel.arpvendno = 9853210 or
   oeel.arpvendno = 859000 then 
      assign d_prodline = "VIC000".
       
                    
if oeel.specnstype ne "n" then do: 
    find first icsw where icsw.cono = co and icsw.whse = oeel.whse and
               icsw.prod = oeel.shipprod no-lock no-error.
    if avail(icsw) then do:
       d_prodline = icsw.prodline.               
       d_shipprod = if icsw.vendprod ne "" then substring(vendprod,1,15)
                    else substring(oeel.shipprod,1,15).
       end.
    end.

if (d_prodline begins "VIC" or
    d_prodline begins "AER" or
    d_prodline begins "CUN") and 
    oeel.qtyship > 0 then do:
   find first smsn where smsn.cono = co and smsn.slsrep = oeel.slsrepout
        no-lock no-error.
   if available(smsn) then do:
      assign d_name = substring(smsn.name,1,20).
      end. 
   assign d_price = if oeel.discpct = 0 then oeel.price
                    else ((100 - oeel.discpct) / 100) * oeel.price
          d_shipped = oeel.qtyship
          creditdebit = if oeel.returnfl = yes then "-"
                      else "+".
   run move_common_info.
   end.
end. /* oeel */
  

for each oeelk where oeelk.cono = co and oeelk.ordertype = "o" and
         oeelk.orderno = oeeh.orderno and oeelk.ordersuf = oeeh.ordersuf          no-lock.
if oeelk.qtyship eq 0 then next.
if oeelk.statustype ne "i" then next.

assign d_prodline = oeelk.arpprodline      
       d_um = oeelk.unit
       d_shipprod = oeelk.shipprod.

if oeelk.specnstype = "n" and
   d_shipprod = "000000" then
       d_shipprod = substring(oeelk.proddesc,1,15).

if oeelk.specnstype ne "n" then do:         
    find first icsw where icsw.cono = co and icsw.whse = oeelk.whse and
               icsw.prod = oeelk.shipprod no-lock no-error.
    if avail(icsw) then
       assign d_prodline = icsw.prodline
              d_shipprod = if icsw.vendprod ne "" then substring(vendprod,1,15)
                           else substring(oeelk.shipprod,1,15).
    end.                      

if (d_prodline begins "vic" or
    d_prodline begins "AER" or
    d_prodline begins "cun") and
    oeelk.qtyship > 0 then do:
   find first oeel where oeel.cono = co and oeel.orderno = oeelk.orderno and
              oeel.ordersuf = oeelk.ordersuf and oeel.lineno = oeelk.lineno
              no-lock no-error.
   find first smsn where smsn.cono = co and smsn.slsrep = oeel.slsrepout
        no-lock no-error.
   if available(smsn) then do:
      assign d_name = substring(smsn.name,1,20).
      end. 

   assign d_price = if oeel.discpct = 0 then oeelk.price
                    else ((100 - oeel.discpct) / 100) * oeelk.price
          d_shipped = oeelk.qtyship
          creditdebit = "+".
   if oeelk.specnstype ne "N" then
      run PDSC_pricing.   
          

   run move_common_info.
   end.
end.  /* loop back to oeelk */  

PROCEDURE PDSC_pricing:

if oeel.kitrollty = "b" or
   oeel.kitrollty = "p" then 
      leave.

find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 1 and pdsc.custno = oeeh.custno and
                pdsc.prod = oeelk.shipprod and oeelk.whse = oeeh.whse and
                pdsc.startdt le oeeh.invoicedt and
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
                
if not avail pdsc then do:
   find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 2 and pdsc.custno = oeeh.custno and
                pdsc.prod = "p-" + icsw.pricetype and
                pdsc.startdt le oeeh.invoicedt and 
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
   if not avail pdsc then do:              
   find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 2 and pdsc.custno = oeeh.custno and
                pdsc.prod = "l-" + string(icsw.arpvendno,"999999999999") +
                             icsw.prodline and
                pdsc.startdt le oeeh.invoicedt and 
                pdsc.whse = oeeh.whse and
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
   if not avail pdsc then do:
        find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 4 and pdsc.custtype = arsc.pricetype and
                pdsc.prod = icsw.pricetype and pdsc.whse = oeeh.whse and
                pdsc.startdt le oeeh.invoicedt and
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
        if not avail pdsc then do:
           find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 4 and pdsc.custtype = arsc.pricetype and
                pdsc.prod = icsw.pricetype and
                pdsc.startdt le oeeh.invoicedt and
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
     end.    
     end.        
     end.
     end.

     
if not avail pdsc then
   leave.
assign d_price = icsw.listprice.   
repeat x = 1 to 9:
if pdsc.qtybrk[1] = 0  then do:
   if pdsc.prctype = false then 
      d_price = ((100 - pdsc.prcdisc[1]) / 100) * d_price.
   else
      d_price = pdsc.prcmult[1].
   leave.
   end.         
       
if x < 9 then
if oeelk.qtyship lt pdsc.qtybrk[x] then do:
   if pdsc.prctype = false then 
      d_price = ((100 - pdsc.prcdisc[x]) / 100) * d_price.
   else
      d_price = pdsc.prcmult[x].
   leave.         
   end.            
end.
END PROCEDURE.


PROCEDURE move_common_info:
if can-do("war,air,nor",company) and
   d_prodline begins "vic" then do:
  assign
  substring(vicrec,1,8)     = c_no
  substring(vicrec,9,9)     = string(oeeh.custno)   
  substring(vicrec,18,30)   = arsc.name
  substring(vicrec,48,30)   = oeeh.shiptoaddr[1]
  substring(vicrec,78,30)   = oeeh.shiptoaddr[2]
  substring(vicrec,108,30)  = "                              "
  substring(vicrec,138,20)  = substring(oeeh.shiptocity,1,20) 
  substring(vicrec,158,3)   = oeeh.shiptost
  substring(vicrec,161,10)  = oeeh.shiptozip
  substring(vicrec,171,3)   = "US"
  substring(vicrec,174,2)   = substring(string(oeeh.invoicedt),1,2) 
  substring(vicrec,176,4)   = string(year(oeeh.invoicedt))
  substring(vicrec,180,4) = sic 
  substring(vicrec,184,15)  = d_shipprod
  substring(vicrec,199,8) = if oeeh.transtype = "RM" or
                               creditdebit = "-" then
                               string(d_shipped * -1, "9999999-")
                            else string(d_shipped, "99999999")
  substring(vicrec,207,20)  = d_name
  substring(vicrec,227,10) = string(d_price, "9999999.99-") 
  substring(vicrec,237,15) = string(oeeh.orderno) + "-" + string(oeeh.ordersuf).    
  if company = "war" then
     put stream warren vicrec at 1 skip. 
  if company = "nor" then
     put stream norris vicrec at 1 skip. 
  if company = "air" then
     put stream airb vicrec at 1 skip. 
     
  end.
if company = "" and
   d_prodline begins "CUN" and
   (oeeh.shiptost = "il" or
    oeeh.shiptocity = "gary" or
    oeeh.shiptost = "ia") then do:
  assign
  yyyy = string(year(oeeh.invoicedt))
  substring(vicrec,1,12)    = string(oeeh.custno,"9999999999")
  substring(vicrec,13,18)   = d_shipprod
  substring(vicrec,31,12)   = string(oeeh.orderno) + "-" + string(oeeh.ordersuf)
  substring(vicrec,43,3)    = string(d_line, "999")
  substring(vicrec,46,8)   = string(d_shipped, "99999999") 
  substring(vicrec,54,3)   = "000"
  substring(vicrec,57,1) = creditdebit
  substring(vicrec,58,4)  = d_um
  substring(vicrec,62,2)  = substring(yyyy,3,4)
  substring(vicrec,64,2) = string(month(oeeh.invoicedt), "99")
  substring(vicrec,66,2)   = string(day(oeeh.invoicedt), "99")
  substring(vicrec,68,25)  = oeeh.shiptonm
  substring(vicrec,93,25)  = oeeh.shiptoaddr[1]
  substring(vicrec,118,25) = if oeeh.shiptoaddr[2] ne "" then
                                oeeh.shiptoaddr[2]
                             else  "                         "   
  substring(vicrec,143,15) = oeeh.shiptocity
  substring(vicrec,158,2)  = oeeh.shiptost
  substring(vicrec,160,10) = oeeh.shiptozip
  substring(vicrec,170,25) = "                         "
  substring(vicrec,195,25)  = "USA".

  put stream cuno vicrec format "x(220)" at 1  skip. 
  end.

if company = "wm" and
   d_prodline begins "AER" then do:
   ordernum = string(oeeh.orderno) + "-" + string(oeeh.ordersuf).
   export stream waste delimiter "," 
          "T380" oeeh.shiptonm oeeh.shiptoaddr[1]
          oeeh.shiptoaddr[2] oeeh.shiptocity oeeh.shiptost oeeh.shiptozip
          ordernum ordernum d_shipprod d_shipped d_shipped d_um d_price
          oeeh.enterdt oeeh.reqshipdt oeeh.invoicedt oeeh.shipdt.
   end.       
          
vicrec = "".
END PROCEDURE.
end.


