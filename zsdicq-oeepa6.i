/* zsdip-oeepa6.i */
/*si**** Custom ****************************************************************
  CLIENT  : 80281 SDI Operating Partners, Inc
  JOB     : SDI06501 - Change to show Canadian Dollar Amts
  ANCHOR  : zsdip-oeepa6.i
  VERSION : 8.0.003
  PURPOSE : 
  CHANGES :
    SI01 (Client originally developed order acknowledgement)
    SI02 03/04/00; ucv/usi; SDI06501 - Change to show Canadian Dollar Amts
    06/10/09 das; NonReturn/NonCancelable
    06/12/09 das; Allow Tendered Invoices to Print
******************************************************************************//* p-oeepa1.i 1.20 02/21/94 */
/*h*****************************************************************************
  INCLUDE      : zsdip-oeepa6.i
  DESCRIPTION  : OEEPA/OEEPAF main print logic
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    10/14/91 mwb; TB# 4762  Change page#, order #, shipped date and ship via
        for pre-printed forms
    04/03/92 pap; TB# 6219  Line DO  add DO line indicator adjusted qty
        backordered
    06/06/92 mwb; TB# 6889  Replaced oeehb.payamt with oeehb.tendamt.
    06/16/92 mkb; TB# 5929  Full amount tendered, add write off amount to
        downpayment
    06/20/92 mwb; TB# 5413  If the header as a write off amount set - add it
        to the down payment display (BO rounding problems when down
        payment is applied).
    08/05/92 pap; TB# 7245  Do not print the backordered qty or the shipped
        qty on an acknowledgement
    08/12/92 pap;           REVERSAL of TB# 7245 BY DESIGN MEETING
    11/11/92 mms; TB# 8561  Display product surcharge label from ICAO
    12/07/92 ntl; TB# 9041  Canadian Tax Changes (QST)
    03/18/93 jlc; TB# 7245  Items with a quantity shipped of 0 are printing a
        quantity shipped and a quantity backordered.
    04/07/93 jlc; TB# 8890  Drop Ship orders should print Drop Ship for
        the Ship Point.
    04/08/93 jlc; TB# 10155 Print customer product.
    04/08/93 jlc; TB# 9727  Don't print core chg and restock amt for a
        non-print line.
    04/10/93 jlc; TB# 7405  Print DROP for the qty ship on drop lines.
    06/07/93 jlc;           Changes to tb# 7245 per Design Committee.
    02/21/94 dww; TB# 13890 File name not specified causing error
    06/23/95 ajw; TB# 18575 Nonstock/Special Kit Component Project (G1)
    08/17/95 tdd; TB# 17771 Notes print twice when reference component
    11/01/95 gp;  TB# 18091 Expand UPC Number (T24)
    11/06/95 gp;  TB# 18091 Expand UPC Number (T24)
    02/27/97 jkp; TB#  7241 (5.1) Spec8.0 Special Price Costing.  Added call
        of speccost.gva.  Added find of icss record.  Added call to 
        speccost.gas to determine prccostper.  Replaced icsp.prccostper with 
        v-prccostper.
    04/17/97 JPH; ITB# 1    Remove upc number, qty b/o, qty shpd, upc vendor
                            number, change orderno to invoice number.
    12/01/97 JPH; ITB# 2    Add icsd information to remittance portion.        
 ******************************************************************************/

/*tb  7241 (5.1) 02/27/97 jkp; Added call to speccost.gva. */
{speccost.gva}                                             

/*tb  5929 06/18/92 mkb */
def buffer            b-oeehb   for oeehb.
def buffer            b-icsd    for icsd.
def var zx-handle     as logical                          no-undo.
def var my-string     as char format "x(10)"              no-undo. 
def var shiplit       as char format "x(33)"              no-undo. 
def var s-shipmsg     as char format "x(77)"              no-undo.
def var ix-enddaycut  like icsd.enddaycut                 no-undo.
def var s-tariffmsg    as c format "x(72)"                no-undo.
/* das - NonReturn */                                                
def var s-asterisk     as c format "x(4)"                     no-undo.
def var s-asteriskdtla as c format "x(124)"                   no-undo.
def var s-asteriskdtlb as c format "x(125)"                   no-undo.
def var s-asteriskdtlc as c format "x(125)"                   no-undo.
def var s-buysignature as c format "x(80)"                    no-undo.
def var s-sunsignature as c format "x(80)"                    no-undo.
def var ast-prtfl      as logical                             no-undo.

def var SS-comm        as dec format ">>>>>>.99"              no-undo.
/*assign shiplit = "Frt/Hndlg to be added at shipping".*/
/*assign shiplit = "Frt/Hndlg/Tax to be added at Shipping".*/
assign shiplit = " ".

/*EN-1808*/
def var v-1019fl  as logical no-undo.


form header  /* main title lines  */
    "** COMMISSIONS **"                  at 99
    skip(1)   
    
    s-lit1aa as c format "x(10)"         at 2
    
    s-lit1a  as c format "x(50)"         at 12
   /* x-reset  as c format "x(10)"         at 28*/
    
    
    s-cancel as c format "x(25)"         at 64
    s-lit1b  as c format "x(37)"         at 93
    s-lit2a  as c format "x(53)"         at 2
/*    oeehb.invoicedt                    at 108   ITB# 1 */
    oeehb.batchnm                        at 123
    "c"                                 at 131
/*
    "-"                                  at 130
    oeehb.ordersuf                       at 131
*/
    s-lit3a as c format "x(09)"          at 2
    oeehb.custno                         at 12
    s-lit3b as c format "x(39)"          at 94
    s-litattn as c format "X(30)"        at 2  /*SI02*/
    oeehb.enterdt                        at 94
    oeehb.custpo                         at 104
    page-num - v-pageno format "zzz9"    at 129
    skip(1)
    s-lit6a as c format "x(8)"           at 2
    s-billname like arsc.name            at 12
    s-lit6b as c format "x(18)"          at 48
    s-remitnm as c format "x(24)"        at 68   /* ITB# 2 */
    s-billaddr1 like arsc.name           at 12
    s-lit6bb as c format "x(6)"          at 48   /* ITB# 2 */
    s-remit-addr1 as c format "x(24)"    at 68   /* ITB# 2 */ 
    s-billaddr2 like arsc.name           at 12
    s-remit-addr2 as c format "x(24)"    at 68   /* ITB# 2 */
    s-lit8a as c format "x(9)"           at 106
    s-pstlicense as c format "x(11)"     at 116
    s-billcity as c format "x(35)"       at 12
    s-corrcity as c format "x(35)"       at 68
    s-lit9a as c format "x(9)"           at 106
    s-gstregno as c format "x(10)"       at 116
    /*SI02 beg*/
    /*skip(1)*/
    s-litbfax as c format "X(30)"        at 12
    /*SI02 end*/
    s-lit11a as c format "x(8)"          at 2
    s-shiptonm like oeehb.shiptonm        at 12
    s-lit11b as c format "x(12)"         at 50
    s-shiptoaddr[1]                      at 12
    oeehb.shipinstr                      at 50
    s-shiptoaddr[2]                      at 12
    s-lit12a as c format "x(10)"         at 50
    s-lit12b as c format "x(3)"          at 82
    s-lit12c as c format "x(7)"          at 104
    s-lit12d as c format "x(5)"          at 117
    s-shipcity as c format "x(35)"       at 12
    s-whsedesc as c format "x(30)"       at 50
    s-shipvia as c format "x(12)"        at 82
    s-cod as c format "x(6)"             at 95
    s-shipdt like oeehb.shipdt            at 104
    s-terms as c format "x(12)"          at 117
    /*SI02 beg*/
    /*skip(1)*/
    s-litsfax as c format "X(30)"        at 12
    /*SI02 end*/
    s-rpttitle like sapb.rpttitle        at 1 skip(1)
    /* das - NonReturn */
    /*
    skip(1) 
    s-asterisk                           at 31
    */
with frame f-head no-box no-labels width 132 page-top.

/**/
form
  s-tariffmsg                            at  3
  /*
  skip(1)
  s-asteriskdtla                         at  3
  s-asteriskdtlb                         at  3
  s-asteriskdtlc                         at  3
  /***** don't print signature lines on quote
  skip(1)
  s-buysignature                         at 50
  skip(1)
  s-sunsignature                         at 50
  *****/
  */
with frame f-footnote no-box no-labels width 132.
/**/                      

form header  /* detail line title */
    s-lit14a as c format "x(75)"         at 1
    s-lit14b as c format "x(53)"         at 76
    s-lit15a as c format "x(75)"         at 1
    s-lit15b as c format "x(53)"         at 76
    s-lit16a as c format "x(78)"         at 1
    s-lit16b as c format "x(53)"         at 79
with frame f-headl no-box no-labels width 132 page-top.


form header  /* detail line title */
    s-lit14a          at 1
    s-lit14b          at 76
    s-lit15a          at 1
    s-lit15b          at 76
/*
    s-lit16a as c format "x(78)"         at 1
    s-lit16b as c format "x(53)"         at 79
*/
with frame f-headl-laser no-box no-labels width 132 page-top.

form /* ship date message */  /* das */
    skip(2)
    s-shipmsg                            at 25
    skip(1)
with frame f-shipmsg no-box no-labels no-underline width 132.

form  /* detail line totals  */
    skip(1)
    v-linecnt         format "zz9"       at 1
    s-lit40a     as c format "x(11)"     at 5
    s-lit40c     as c format "x(17)"     at 21   
    s-totqtyshp  as c format "x(13)"     at 40
    s-lit40d     as c format "x(18)"     at 78   /*82*/
    s-totlineamt as c format "x(13)"     at 117
with frame f-tot1 no-box no-labels no-underline width 132.

form  /* all extra costs display  */
    s-canlit as c   format "x(60)"         at 15    /*si02*/
    s-title2 as c format "x(37)"           at 78  /*82*/
    s-amt2   as dec format "zzzzzzzz9.99-" at 117
with frame f-tot2 no-box no-labels width 132 no-underline down.

form  /* all extra costs display  */
    s-canlit  format "x(60)"               at 15    /*si02*/
    s-title2  format "x(37)"               at 78  /*82*/
with frame f-tot2a no-box no-labels width 132 no-underline down.

form  /* invoice total*/
    s-currencyty as c format "x(51)"     at 24      /*si02*/
    s-invtitle as c format "x(18)"       at 78   /*82*/
    s-totinvamt like oeehb.totinvamt      at 117
with frame f-tot3 no-box no-labels no-underline width 132 down.

form header  /* footter at last page */
    s-lit48a as c format "x(9)"              at 1
    s-CSR-title  as c format "x(25)"         at 20
    s-contact    as c format "x(30)"         at 46
    s-contlit-ph as c format "x(4)"          at 82
    s-contact-ph as c format "(xxx)xxx-xxxx" at 87
    s-contlit-fx as c format "x(4)"          at 102
    s-contact-fx as c format "(xxx)xxx-xxxx" at 107
with frame f-tot4 no-box no-labels width 132 page-bottom.

/*tb# 5929 06/18/92 mkb */
form
    "Full Amount Tendered For All Orders:"  at 5
/*    oeehb.tottendamt                        at 42 */
    "*** Back Order/Release Exists ***"     at 5
with frame f-tot5 no-box no-labels width 132.

assign v-pageno    = page-number - 1
       v-subnetamt = 0.

{zsdicq-oeepa9.i s-}

/* {p-oeord.i "oeehb." }  */ /*** v-totlineamt ***/

if oeehb.transtype = "qu" then
  assign s-lit40c = "Qty Quoted Total ".
else
  assign s-lit40c = "Qty Ordered Total".

{w-arss.i oeehb.custno oeehb.shipto no-lock}  /*SI02*/

find first oeelb where oeelb.cono    = g-cono and
                       oeelb.batchnm = oeehb.batchnm and
                       oeelb.seqno   = oeehb.seqno no-lock no-error.
if avail oeelb then
      
   assign s-shipdt = if substring(oeelb.user5,6,8) ne "" then 
                        date(substring(oeelb.user5,6,8))
                      else 
                         s-shipdt.

/*tb 5413 06/20/92 mwb */
assign
    s-billname   = if v-invtofl then oeehb.shiptonm
                   else arsc.name
    s-billaddr1  = if v-invtofl then oeehb.shiptoaddr[1]
                   else arsc.addr[1]
    s-billaddr2  = if v-invtofl then oeehb.shiptoaddr[2]
                   else arsc.addr[2]
    s-billcity   = if v-invtofl then
                       oeehb.shiptocity + ", " + oeehb.shiptost +
                       " " + oeehb.shiptozip
                   else arsc.city + ", " + arsc.state + " " + arsc.zipcd

    /*SI02*/
    s-litbfax    = if oeehb.divno = 40 and
                   length(arsc.faxphoneno)  > 1 then "Fax: " +
                   string(arsc.faxphoneno,"(xxx)xxx-xxxx xxxxx") else ""
    /* s-cod        = if oeehb.codfl = no then "" else "C.O.D." */
    /*
    s-shipdt     = if oeehb.shipdt = ? then oeehb.promisedt else oeehb.shipdt
    */
    s-totlineamt =
                /*   
                   if oeehb.lumpbillfl then
                       string(oeehb.lumpbillamt,"zzzzzzzz9.99-")
                   else
                */  
                   if oeehb.transtype = "rm" then
                       string((oeehb.totlineamt * -1),"zzzzzzzz9.99-")
                   else string(oeehb.totlineamt,"zzzzzzzz9.99-")
    s-taxes      = oeehb.taxamt[1] + oeehb.taxamt[2] +
                   oeehb.taxamt[3] + oeehb.taxamt[4]
    s-dwnpmtamt  = /*
                   if oeehb.transtype = "cs" then oeehb.tendamt
                   else
                   */
                   if oeehb.dwnpmttype then oeehb.dwnpmtamt
                   else (oeehb.dwnpmtamt / 100) * oeehb.totinvamt
    s-dwnpmtamt  = s-dwnpmtamt /*   + oeehb.writeoffamt  */
    v-linecnt    = 0
    s-shiptonm      = oeehb.shiptonm
    s-shiptoaddr[1] = oeehb.shiptoaddr[1]
    s-shiptoaddr[2] = oeehb.shiptoaddr[2]
    s-shipcity      = oeehb.shiptocity + ", " + oeehb.shiptost +
                      " " + oeehb.shiptozip
    /*SI02*/
    s-litsfax    = if oeehb.divno = 40 and avail arss and 
                   length(arss.faxphoneno) > 1 then
                   "Fax: " + string(arss.faxphoneno,"(xxx)xxx-xxxx xxxxx") else
                   if oeehb.divno = 40 and 
                   not avail arss and 
                   s-billname = s-shiptonm and
                   s-billaddr1 = s-shiptoaddr[1] and
                   s-billaddr2 = s-shiptoaddr[2] and
                   s-billcity = s-shipcity and
                   length(arsc.faxphoneno) > 1 then
                   "Fax: " + string(arsc.faxphone,"(xxx)xxx-xxxx xxxxx") 
                   else ""

/*    s-gstregno      = if g-country = "ca" then b-sasc.fedtaxid else "" */
    s-pstlicense    = if g-country = "ca" then oeehb.pstlicenseno
                      else "".

/*  {p-oeadfp.i}   load addon descriptions    */

{w-sasta.i "S" oeehb.shipvia no-lock}

{w-icsd.i oeehb.whse no-lock}

/* ITB# 2 */
assign s-corrcity = icsd.city + ", " + icsd.state + " " + icsd.zipcd
       s-remitnm = icsd.name
       s-remit-addr1 = icsd.addr[1]
       s-remit-addr2 = icsd.addr[2].

/*tb 8890 04/07/93 jlc; DO's print Drop Ship for Ship Point */
assign
    s-shipvia  = if avail sasta then sasta.descrip else oeehb.shipvia
    s-whsedesc = if oeehb.transtype = "do" then "** Drop Ship **"
                 else if avail icsd then icsd.name
                 else oeehb.whse.

{w-sasta.i "t" oeehb.termstype no-lock}

if oeehb.langcd <> "" then do:
    {w-sals.i oeehb.langcd "t" oeehb.termstype no-lock}
end.

assign
    s-terms    = if oeehb.langcd <> "" and avail sals then sals.descrip[1]
                 else if avail sasta then sasta.descrip
                 else oeehb.termstype
    s-rpttitle = if p-promofl then sapb.rpttitle else "".

assign s-tariffmsg   = "Any applicable surcharge not mentioned above "  +
                       "will be applied at shipping".

/* das - NonReturn */
/*****
assign s-asterisk = "(**)".
assign s-asteriskdtla = 
 "**  Products noted with an (**) next to the line item are non-cancellable" +
 " and non-returnable.".
assign s-asteriskdtlb = 
 "    All other products are subject to cancellation or restock charges." +
 "    All sales are subject to the".
assign s-asteriskdtlc =
 "    SunSource Terms and Conditions of Sale &" +
 " Warranty available at www.sun-source.com/terms-and-conditions.pdf.".
****/

/***** don't print signature lines on quote
assign s-buysignature =
"Authorized Signature of Purchaser _________________________ Date ___________".
assign s-sunsignature =
"Authorized Signature of SunSource _________________________ Date ___________".
*****/
view frame f-head.
if oeehb.transtype = "ra" then
  hide frame f-head1.
else  
if (avail x-sasp and x-sasp.user3[1] = 001) then
do:
  hide frame f-headl.
  view frame f-headl-laser.
end.                              
else 
  view frame f-headl.

view frame f-tot4.

/** sx32 
/*    print the customer notes  */
if avail arsc and arsc.notesfl ne "" then
do:
    for each notes where
        notes.cono         = g-cono    and
        notes.notestype    = "c"       and
        notes.primarykey   = string(oeehb.custno) and
        notes.secondarykey = ""
    no-lock:
        if notes.printfl = yes then
        do i = 1 to 16:
            if notes.noteln[i] ne "" then
            do:
                display notes.noteln[i] with frame f-notes.
                down with frame f-notes.
            end.
        end.
    end.
end. /* if arsc.notesfl ne "" */

 sx32 **/


/*    Print the customer notes  */
if arsc.notesfl ne "" then do:
  run notesprt.p(g-cono,"c",string(oeehb.custno),"","oeepa",8,2).
  end.    

/*    Added shipto notes flag   */
if avail arss and arss.notesfl <> "" then do:
  run notesprt.p(g-cono,"cs",string(oeehb.custno),oeehb.shipto,"oeepa",8,2).
  end.

/*    Print the order notes     */
if oeehb.notesfl ne "" then do:
  run notesprt.p(g-cono,"ZBA",oeehb.batchnm," ","oeepa",8,2).
  end.   


/*  a receipt on acct has no lines */
if oeehb.transtype <> "ra" then do:
    v-totqtyshp = 0.
    for each oeelb where oeelb.cono = g-cono and 
             oeelb.batchnm = oeehb.batchnm and
             oeelb.seqno   = oeehb.seqno   and
             oeelb.specnstype <> "l"
             no-lock:

        {w-icsd.i substr(oeelb.user4,1,4) no-lock b-}
        if avail b-icsd then
          assign ix-enddaycut = b-icsd.enddaycut.
        else
          assign ix-enddaycut = 86400.

        /*tb 18091 11/01/95 gp;  Find Product section of UPC number */
        if oeelb.arpvendno <> 0 then
            {icsv.gfi "'u'" oeelb.shipprod oeelb.arpvendno no}
        else
            {icsv.gfi "'u'" oeelb.shipprod oeelb.vendno no}

        /*tb  7241 (5.1) 02/27/97 jkp; Added find of icss record based on
             oeel because the current product is needed. */
        {icss.gfi 
            &prod        = oeelb.shipprod
            &icspecrecno = oeelb.icspecrecno
            &lock        = "no"}

        /*tb  7241 (5.1) 02/27/97 jkp; Added call to speccost.gas to 
             determine v-prccostper. */
        assign
            {speccost.gas &com = "/*"}
            v-linecnt = v-linecnt + 1
            v-netord  = /*
                        if {p-oeordl.i "oeelb." "oeehb."} then oeelb.netord
                        else
                        */
                        oeelb.netamt
            v-qtyord  = /*
                        if {p-oeordl.i "oeelb." "oeehb."} then oeelb.qtyord
                        else
                        */
                        oeelb.qtyord.
/* SunFix0927 
        if oeelb.botype = "d" and v-qtyord = 0 and
           oeehb.stagecd < 2 and 
           (not can-do("fo,qu,st,bl",oeehb.transtype))  and
           (not can-do("s,t",oeehb.orderdisp))  and
           oeehb.boexistsfl = no  then
           v-qtyord  = oeelb.qtyord.

   SunFix0927 */

        if oeelb.specnstype ne "n" then do:
            {w-icsw.i oeelb.shipprod substr(oeelb.user4,1,4) no-lock}
            {w-icsp.i oeelb.shipprod no-lock}
            if oeehb.langcd <> "" then
                {w-sals.i oeehb.langcd "p" oeelb.shipprod no-lock}
          end.
       else
            /* Added catalog read for product notes display */
                        {w-icsc.i oeelb.shipprod no-lock} .

        clear frame f-oeelb.

        assign
            s-qtyship  = if oeelb.returnfl then
                           if substring
                              (string(oeelb.qtyord,"9999999.99-"),9,2) = "00"
                               then string((oeelb.qtyord * -1),"zzzzzz9-")
                           else string(oeelb.qtyord * -1,"zzzzzz9.99-")
                         else if substring
                             (string(oeelb.qtyord,"9999999.99-"),9,2) = "00"
                             then string(oeelb.qtyord,"zzzzzz9-")
                         else string(oeelb.qtyord,"zzzzzz9.99-")
            s-qtyord   = if oeelb.returnfl then
                             if substring
                                 (string(oeelb.qtyord,"9999999.99-"),9,2) = "00"
                                 then string((oeelb.qtyord * -1),"zzzzzz9-")
                             else string((oeelb.qtyord * -1),"zzzzzz9.99-")
                         else if substring(string(oeelb.qtyord,"9999999.99-")
                             ,9,2) = "00"
                             then string(oeelb.qtyord,"zzzzzz9-")
                         else string(oeelb.qtyord,"zzzzzz9.99-")
         
            s-qtyship  = /*
                         if {p-oeordl.i "oeelb." "oeehb."} then ""
                         else
                         */
                         string(oeelb.stkqtyord)
            v-qtybo    = if oeelb.botype ne "n" then
                             dec(s-qtyord) - dec(s-qtyship)
                         else 0
            s-qtybo    = if substring(string(v-qtybo,"9999999.99-"),9,2) = "00"
                             then string(v-qtybo,"zzzzzz9-")
                         else string(v-qtybo,"zzzzzz9.99-")
            s-qtybo    = /*
                         if {p-oeordl.i "oeelb." "oeehb."} then ""
                         else 
                         */
                         s-qtybo
            s-lineno   = string(oeelb.lineno,"zz9")
            s-price    = if oeelb.corechgty = "r" then
                             string(oeelb.corecharge,"zzzzzz9.99-")
                         else if substring
                             (string(oeelb.price,"9999999.99999"),11,3) = "000"
                             then string(oeelb.price,"zzzzzz9.99-")
                         /*else string(oeelb.price,"zzzzzz9.99999-") SI02*/
                         else string(round(oeelb.price,2),"zzzzzz9.99-")
            s-discpct  = /* if substring
                             (string(oeelb.discpct,"9999999.99999"),11,3) =                               "000"
                             then string(oeelb.discpct,"zzzzzz9.99-")
                         else string(oeelb.discpct,"zzzzzz9.99999-")
                         */ ""
            s-netamt   = if oeelb.corechgty = "r" then
                             string((oeelb.corecharge * oeelb.stkqtyord),
                                 "zzzzzzzz9.99-")
                          else string(v-netord,"zzzzzzzz9.99-") 
            s-sign     = if oeelb.returnfl and oeelb.printpricefl = yes then "-"
                         else ""
            v-subnetamt = v-subnetamt +
                          if oeelb.returnfl then (v-netord * -1)
                          else v-netord
            s-totlineamt = string(dec(v-subnetamt),"zzzzzzzz9.99-")
            v-totqtyshp = if oeelb.returnfl then
                              if can-do("rm",oeehb.transtype) then
                                  v-totqtyshp - v-qtyord
                              else v-totqtyshp
                          else if oeehb.stagecd < 3 and oeelb.botype = "d" then
                              v-totqtyshp
                          else v-totqtyshp + v-qtyord
            s-prcunit   = if v-prccostper <> "" then
                              v-prccostper
                          else oeelb.unit
            /* das - NonReturn */
            s-lasterisk = if substr(oeelb.user10,1,1) = "n" then
                             "**"
                          else
                             "  ".

        /*si02 begin -- logic for Canadian Currency sdi06501*/
        /*
        if (substr(oeehb.user5,1,1) = "c") or
           (oeehb.currencyty = "lb" and oeehb.user6 <> 0)
        */
        if oeehb.divno ne 30 and substr(oeehb.user5,1,1) <> "U"
        
        then do: 
           if oeehb.user6 <> 0 then do:
              assign 
                 s-price     = if oeelb.corechgty = "r" 
                               then string(dec(s-price) * oeehb.user6,
                                           "zzzzzz9.99-") 
                               else
                               if oeelb.price * 100 = int(oeelb.price * 100) 
                                    then string(dec(s-price) * oeehb.user6, 
                                                "zzzzzz9.99-")
                                    else string(
                                    round(dec(s-price) * oeehb.user6,2),
                                                "zzzzzz9.99-")
                 s-netamt    = string(dec(s-netamt) * oeehb.user6,
                                      "zzzzzzzz9.99-")
                 v-subnetamt = v-subnetamt * oeehb.user6
                 s-totlineamt = string(dec(v-subnetamt),"zzzzzzzz9.99-").
           end.
           s-cantot    = s-cantot + 
                         (dec(s-netamt) * (if oeelb.returnfl then -1 else 1)). 
        end.
        /*end si02*/                                  

        /*tb 7405 04/10/93 jlc; Print DROP for the qty ship on DOs */
        assign
            s-descrip = ""
            s-qtyship = if oeehb.stagecd < 3 and oeelb.botype = "d" then 
                           "DROP"
                        else s-qtyship
            s-qtybo   = if oeehb.stagecd < 3 and oeelb.botype = "d" then ""
                        else s-qtybo.

        /*tb 7245 08/05/92 pap; Do not display qty backordered or qty shipped */
        /* REVERSAL OF TB 7245 - 08/12/92 DESIGN MEETING */
        /*tb 18091 11/01/95 gp;  Change display of UPC number */

/* ITB# 2        Suppress discounts from acknowledgments */
        if s-discpct ne "" then do:
           assign s-price = string(round
             (oeelb.price * ((100.00 - oeelb.discamt) / 100) *
                        /*si02 added "if substr..."*/
                           (if (substr(oeehb.user5,1,1) = "c"
                                and oeehb.user6 <> 0)
                       /*  or (oeehb.currencyty = "lb" and oeehb.user6 <> 0) */
                            then oeehb.user6
                                else 1),2),"zzzzzz9.99-")
                  s-discpct = "".
           end.
        
        /* if shipdt was manually entered, print the manually entered date */
        if oeelb.xxda1 <> ? then
          assign x-shipdt  = oeelb.xxda1
                 x-astriks = "*".         /*das*/
        else
          do:
          assign x-shipdt  = if avail icsw and oeelb.leadtm = 0 and
                                icsw.leadtmavg > 0 then
                               today + icsw.leadtmavg
                             else
                             if oeelb.leadtm <> 0 then
                               today + oeelb.leadtm
                             else
                             if s-shipdt <> ? then
                                s-shipdt
                             else   
                               today + 14
                 x-astriks = "*".        /*das*/
         
          assign x-shipdt = 
                 if ((int(substring(string(time,"hh:mm:ss"),1,2)) * 100) +
                     int(substring(string(time,"hh:mm:ss"),4,2))) >
                   ((truncate(truncate(ix-enddaycut / 60 ,0) / 60,0) * 100) +
                    (truncate(ix-enddaycut / 60 ,0)) mod 60) then
                   x-shipdt + 1
                 else
                   x-shipdt.
          if avail icsw then
            run zsdiweekendh (input-output x-shipdt,
                              input-output v-1019fl,
                              input        icsw.whse,
                              input        g-cono,
                              input        oeehb.divno). 
        end.
/*        
        assign x-shipdt = if substring(oeelb.user3,11,10) ne "" then 
                             date(substring(oeelb.user3,11,10))
                          else 
                             s-shipdt.
*/                   
        display s-lineno
                oeelb.shipprod  
                oeelb.unit
                s-qtyord
/*              s-qtybo        
                s-qtyship      */
                s-prcunit
/*              s-upcpno   ITB # 1 */
                s-price         
/*              s-discpct       when oeelb.printpricefl = yes      */
                x-shipdt    
                x-astriks      /*das*/ 
                s-netamt        
                s-sign          
                s-lasterisk    /*das - NonReturn */
                with frame f-oeelb.

/* Jen Take out Lead time
        
        if avail icsw and icsw.leadtmavg <> 0 then
           assign s-leadtm = icsw.leadtmavg
                  s-leadtmlit = "LeadTm:".
        else
        if oeelb.specnstype = "n" and oeelb.leadtm <> 0 then
           assign s-leadtm = oeelb.leadtm
                  s-leadtmlit = "LeadTm:".
        else
*/
           assign s-leadtm  = 0
                  s-leadtmlit = "".
        display s-leadtm s-leadtmlit with frame f-oeelb.          
        
        if oeelb.specnstype = "n" then do:
            if oeelb.proddesc ne "" then
                display oeelb.proddesc @ s-descrip
                with frame f-oeelb.
        end.        /** non-stock **/

        else if not avail icsp or not avail icsw then
            if oeehb.langcd = "" or not avail sals then
                display v-invalid @ s-descrip with frame f-oeelb.
            else display
                     sals.descrip[1] + " " + sals.descrip[2] @ s-descrip
                 with frame f-oeelb.
        else if oeehb.langcd = "" or not avail sals then
            display
                icsp.descrip[1] + " " + icsp.descrip[2] @ s-descrip
            with frame f-oeelb.
        else display
                 sals.descrip[1] + " " + sals.descrip[2] @ s-descrip
             with frame f-oeelb.
        down with frame f-oeelb.

        v-reqtitle = if      oeelb.xrefprodty = "c" then "   Customer Prod:"
                     else if oeelb.xrefprodty = "i" then "Interchange Prod:"
                     else if oeelb.xrefprodty = "p" then " Superseded Prod:"
                     else if oeelb.xrefprodty = "s" then " Substitute Prod:"
                     else if oeelb.xrefprodty = "u" then "    Upgrade Prod:"
                     else "".

        if can-do("c,i,p,s,u",oeelb.xrefprodty) and oeelb.reqprod <> "" then do:
            /*tb 10155 04/09/93 jlc; print 3rd tier product */
            s-prod  =   oeelb.reqprod.
            if oeelb.xrefprodty = "c":u then
              do:
              find first icsec use-index k-custno where
                         icsec.cono      = g-cono       and
                         icsec.rectype   = "c"          and
                         icsec.custno    = oeehb.custno and
                         icsec.prod      = oeelb.reqprod no-lock no-error.
              if avail icsec then
                assign s-prod = s-prod + " " + icsec.addprtinfo.
            end.
            display s-prod
                    v-reqtitle
                    with frame f-oeelc.
                    down with frame f-oeelc.
        end.

        /*tb 10155 04/09/93 jlc; print 3rd tier customer product */
        /*tb 10155 01/03/94 mwb; made changes for cust product display */
        if oeelb.xrefprodty ne "c" then do:
            s-prod = if oeelb.reqprod ne "" then oeelb.reqprod
                     else oeelb.shipprod.

            {n-icseca.i "first" s-prod ""c"" oeehb.custno no-lock}

            if avail icsec then do:
               assign
                   s-prod     = icsec.prod + " " + icsec.addprtinfo
                   v-reqtitle = "   Customer Prod:".

               display
                   s-prod
                   v-reqtitle
               with frame f-oeelc.
            end.
            else
              do:
              if oeelb.reqprod <> ""  and
              oeelb.xrefprodty = "c" then
                do:
                assign s-prod     = oeelb.reqprod
                       v-reqtitle = "   Customer Prod:".
                display
                  s-prod
                  v-reqtitle
                with frame f-oeelc.
              end.
            end.

        end. /* if oeelb.xrefprodty ne "c" */

        if oeelb.corechgty = "r" then
            display with frame f-oeelr.

        /* Determine Commision Amounts */
        clear frame f-oeel1.
        assign SS-comm = (oeelb.xxde1 + oeelb.xxde2 + oeelb.xxde3) / 100.
        display "*** Commission Amount ***" @ oeelb.shipprod
       string(SS-comm * (oeelb.price  * (oeelb.termspct / 100)),"zzzzzz9.99-")                                                                  @ s-price
       string(SS-comm * (oeelb.netamt * (oeelb.termspct / 100)),"zzzzzzzz9.99-")
                                                                @ s-netamt
           with frame f-oeel1.
        down with frame f-oeel1.
        
        /***** SurCharge
        if oeelb.datccost <> 0 and /* v-qtyord <> 0 and */   /*das-080408*/
            oeelb.printpricefl = yes
        then do:
            clear frame f-oeel1.
            display "Tariff Surcharge" @ oeelb.shipprod
                    /*si03 added "* if substr..." for canadian currency*/
                    string(oeelb.datccost *
                           (if (substr(oeehb.user5,1,1) = "c" and 
                               oeehb.user6 <> 0)
                      /*  or (oeehb.currencyty = "lb" and oeehb.user6 <> 0) */
                             then
                              oeehb.user6
                             else 1) /* * v-qtyord */   /*das-080408*/
                             ,"zzzzzz9.99-")
                       @ s-price
                    /*si03 added "* if substr..." for canadian currency*/
                    string(oeelb.datccost *
                            (if (substr(oeehb.user5,1,1) = "c" and 
                                oeehb.user6 <> 0) 
                      /*   or (oeehb.currencyty = "lb" and oeehb.user6 <> 0)*/ 
                              then
                               oeehb.user6 
                             else 1) * v-qtyord ,"zzzzzzzz9.99-")
                       @ s-netamt
                    "-" when oeelb.returnfl     @ s-sign
                    " " when not oeelb.returnfl @ s-sign   
            with frame f-oeel1.
            down with frame f-oeel1.
          if (substr(oeehb.user5,1,1) = "c") 
       /*  or (oeehb.currencyty = "lb" and oeehb.user6 <> 0) */ then 
           do: 
           assign s-cantot = s-cantot + 
                      ( (oeelb.datccost *
                          (if (substr(oeehb.user5,1,1) = "c" and 
                               oeehb.user6 <> 0) 
                  /*  or (oeehb.currencyty = "lb" and oeehb.user6 <> 0) */ then
                             oeehb.user6 
                           else 
                             1)
                          * v-qtyord ) *
                       ( if oeelb.returnfl  then -1 else 1)).
          end. /* if canadian */
          /* das - added surcharge to Total line */
          else
            do:
            export stream whatsup delimiter ","
              v-subnetamt s-totlineamt oeelb.datccost.
            assign v-subnetamt = dec(s-totlineamt) + /*v-subnetamt +*/
                                  ((oeelb.datccost * v-qtyord) *
                                   (if oeelb.returnfl then -1 else 1))
                   s-totlineamt = string(v-subnetamt,"zzzzzzzz9.99-").
          end.

        end. /* if oeelb.datccost <> 0 and v-qtyord <> 0 and
                   oeelb.printpricefl = yes */
        SurCharge ******/
    
    /**************** display core charge **************/
        /*tb 9727 04/08/93 jlc; don't print core chg when print flag = no */
        if oeelb.corecharge <> 0 and oeelb.corechgty <> "r" and
           oeelb.printpricefl = yes
        then do:
            clear frame f-oeel1.

            display
                "CORE CHARGE"                           @ oeelb.shipprod
                    /*si03 added "* if substr..." for canadian currency*/
                    string(oeelb.corecharge * 
                    if (substr(oeehb.user5,1,1) = "c" and oeehb.user6 <> 0) 
               /*    or (oeehb.currencyty = "lb" and oeehb.user6 <> 0) */

                    then oeehb.user6 else 1,"zzzzzz9.99-") 
                       @ s-price
                    /*si03 added "* if substr..." for canadian currency*/
                    string(oeelb.corecharge * 
                    if (substr(oeehb.user5,1,1) = "c" and oeehb.user6 <> 0) 
              /*  or (oeehb.currencyty = "lb" and oeehb.user6 <> 0) */
                    then oeehb.user6 else 1 * v-qtyord,"zzzzzzzz9.99-") 
                       @ s-netamt
                    "-" when oeelb.returnfl     @ s-sign
                    " " when not oeelb.returnfl @ s-sign
             with frame f-oeel1.
             down with frame f-oeel1.
        end.

    /**************** display restock amount **************/
        /*tb 9727 04/08/93 jlc; don't print restock when print flag = no */
        if oeelb.restockamt <> 0 and oeelb.printpricefl = yes
        then do:
            clear frame f-oeel1.

            /*si02 added "* if substr..." for canadian currency*/
            s-restockamt = 0.
                      /*
                         if oeelb.restockfl = yes then oeelb.restockamt *
                            if (substr(oeehb.user5,1,1) = "c" 
                               and oeehb.user6 <> 0 )
                    /*   or (oeehb.currencyty = "lb" and oeehb.user6 <> 0) */

                            then oeehb.user6
                            else 1
                           else round(oeelb.netamt *
                            if (substr(oeehb.user5,1,1) = "c" 
                               and oeehb.user6 <> 0)
                   /*    or (oeehb.currencyty = "lb" and oeehb.user6 <> 0) */

                            then oeehb.user6
                            else 1 * (oeelb.restockamt / 100),2).
                   */
          display
                "RESTOCK AMT" @ oeelb.shipprod
                string(s-restockamt,"zzzzzzzz9.99-") @ s-netamt
            with frame f-oeel1.
            down with frame f-oeel1.
        end.
    /**************** display GSA Scheduled Item **************/
        if avail arss then
          do:
          if int(substr(oeelb.user4,42,8)) <> 0 and arss.pricetype = "GOVT" and
            can-find(pdsc where 
                     pdsc.cono = g-cono and
                     pdsc.pdrecno = int(substr(oeelb.user4,42,8)) and
                     pdsc.levelcd = 3 and
                     pdsc.startdt <= oeelb.enterdt and
                    (pdsc.enddt = ? or
                     pdsc.enddt >= oeelb.enterdt)) then
            do:
            clear frame f-oeell.
            display "GSA SCHEDLUED ITEM" @ oeelb.shipprod
              with frame f-oeel1.
            down with frame f-oeel1.
          end. /* GSA product */
        end. /* avail arss */
        else
          do:
          if int(substr(oeelb.user4,42,8)) <> 0 and arsc.pricetype = "GOVT" and
            can-find(pdsc where 
                     pdsc.cono = g-cono and
                     pdsc.pdrecno = int(substr(oeelb.user4,42,8)) and
                     pdsc.levelcd = 3 and
                     pdsc.startdt <= oeelb.enterdt and
                    (pdsc.enddt = ? or
                     pdsc.enddt >= oeelb.enterdt)) then
            do:
            clear frame f-oeel1.
            display "GSA SCHEDLUED ITEM"   @ oeelb.shipprod
              with frame f-oeel1.
            down with frame f-oeel1.
          end. /* GSA product */
        end. /* avail arsc */

    /**************** display direct order indicator **************/
        /*tb 6219 4/3/92 pap; Line DO - add line DO indicator     **/
        if oeelb.botype = "d" and oeehb.transtype ne 'DO' then
            display with frame f-oeeldo.

/* SX32
    /*************  print the product notes  ************/
        if oeelb.specnstype <> "n" and avail icsp and icsp.notesfl ne ""
        then do:
            for each notes where
                notes.cono = g-cono         and
                notes.notestype = "p"       and
                notes.primarykey = icsp.prod and
                notes.secondarykey = ""
            no-lock:

                if notes.printfl = yes then do i = 1 to 16:
                    /*tb 13890 02/21/94 dww; File name not specified causing
                        error */
                    if notes.noteln[i] ne "" then do:
                        display notes.noteln[i] with frame f-notes.
                        down with frame f-notes.
                    end.

                end.

            end.

        end. /* if oeelb.specnstype <> "n" and avail icsp and ... */
SX32 */

     run zsditariffprt (g-cono,oeelb.shipprod,oeehb.whse,"oeepi",8).

/*************  print the product notes  ************/
     if oeelb.specnstype <> "n" and avail icsp and icsp.notesfl ne "" then
       run notesprt.p(g-cono,"p",icsp.prod,"","oeepa",8,1).
                    
/********** Print the catalog product notes  ************/
     if oeelb.specnstype = "n" and avail icsc and icsc.notesfl ne "" then
       run notesprt.p(1,"g",icsc.catalog,"","oeepa",8,1).
                     
    /**************  line item comments  ****************/
        if oeelb.commentfl = yes then do:
         assign my-string = "ob" + string(int(oeelb.batchnm),">>>>>>>9"). 
         {w-com.i my-string 1 00 oeelb.lineno yes no-lock}
         if avail com and com.printfl2 then 
           do i = 1 to 16:
           if com.noteln[i] ne "" then do:
              display com.noteln[i] with frame f-com.
              down with frame f-com.
              end.

           end.

           {w-com.i my-string 1 00 oeelb.lineno no no-lock}

           if avail com and com.printfl2 then 
             do i = 1 to 16:
               if com.noteln[i] ne "" then do:
                 display com.noteln[i] with frame f-com.
                 down with frame f-com.
               end.

             end.

        end. /* if oeelb.commentfl = yes */

    /********************  explode kits  *********************/
        if oeelb.xxL1 /* kitfl */ = yes and

            ((avail icsp and icsp.exponinvfl and oeelb.specnstype <> "n") or
              oeelb.specnstype = "n") and

            can-find(first oeelk use-index k-oeelk where
                           oeelk.cono      = oeelb.cono         and
                           oeelk.ordertype = "b"                and
                           oeelk.orderno   = int(oeelb.batchnm) and
                           oeelk.ordersuf  = 0 and
                           oeelk.lineno    = oeelb.lineno) then

            /*tb 2277 9/11/91 kmw; kit enh TB 2277 */
            /* was ex lock; kmw 9-12-91*/
            for each oeelk use-index k-oeelk where
                oeelk.cono      = oeelb.cono         and
                oeelk.ordertype = "b"                and
                oeelk.orderno   = int(oeelb.batchnm) and
                oeelk.ordersuf  = 0 and
                oeelk.lineno    = oeelb.lineno   and
                oeelk.printfl   = yes
            no-lock:

                /*tb 2277 9/11/91 kmw; kit enhancement */
                if oeelk.comptype = "r" then do:
                    display oeelk.instructions with frame f-refer.
                    down with frame f-refer.
                end.        /** comptype = 'r' **/

                else do:

                    /*tb 18575 06/23/95 ajw; Nonstock/Special Kit Component.*/
                    if oeelk.specnstype <> "n" then do:
                        /*was ex lock*/
                        {w-icsw.i oeelk.shipprod oeehb.whse no-lock}
                        {w-icsp.i oeelk.shipprod no-lock}
                    end.
                    else
                    /* Added catalog read for product notes display */
                       {w-icsc.i oeelk.shipprod no-lock} .
 
                    /*tb 18091 11/01/95 gp;  Assign product section
                        of UPC number */
                    if oeelk.specnstype = "n" or oeelk.arpvendno <> 0 then
                        {icsv.gfi "'u'" oeelk.shipprod oeelk.arpvendno no}
                    else if avail icsw then
                        {icsv.gfi "'u'" oeelk.shipprod icsw.arpvendno no}

                    assign
                        s-upcpno    = "00000".

    /********************  print the component  ***********************/
                    clear frame f-oeelb.

                    /*tb 1/24/91 mwb; ship comp, tag&hold always uses ordered */
                    /*tb 2277 9/11/91 kmw; stkqtyord --> qtyord */
                    assign
                        s-qtyord  = if oeelb.returnfl then
                                        if substring
                                        (string(oeelk.qtyord,"9999999.99-"),9,2)
                                        = "00"
                                        then
                                        string((oeelk.qtyord * -1),"zzzzzz9-")
                                        else
                                        string(oeelk.qtyord * -1,"zzzzzz9.99-")
                                    else if substring
                                        (string(oeelk.qtyord,"9999999.99-"),9,2)
                                        = "00"
                                        then string(oeelk.qtyord,"zzzzzz9-")
                                    else string(oeelk.qtyord,"zzzzzz9.99-")
                        s-qtyship  = if oeelb.returnfl then
                                         if substring(string
                                             (oeelk.qtyship,"9999999.99-"),9,2)
                                             = "00"
                                             then string(
                                             (oeelk.qtyship * -1),"zzzzzz9-")
                                         else string
                                             (oeelk.qtyship * -1,"zzzzzz9.99-")
                                     else if substring(string
                                         (oeelk.qtyship,"9999999.99-"),9,2)
                                         = "00"
                                         then string(oeelk.qtyship,"zzzzzz9-")
                                     else string(oeelk.qtyship,"zzzzzz9.99-")

                        s-qtyship  = if can-do("s,t",oeehb.orderdisp) then
                                         s-qtyord
                                     else s-qtyship.

                    /*tb 7245 08/05/92 pap; Do not display shipped qty*/
                    /* REVERSAL OF TB 7245 08/12/92 DESIGN MEETING    */
                    /*tb 18575 06/23/95 ajw; Nonstock/Special Kit Comp*/
                    /*tb 18091 11/01/95 gp;  Change display of UPC number */
                    display
                        oeelk.shipprod   @ oeelb.shipprod
                        s-qtyord
                        s-qtyship
                        s-upcpno
                        oeelk.unit       @ oeelb.unit
                        "Kit"            @ s-lineno
                    with frame f-oeelb.

                    /*tb 18575 06/23/95 ajw; Nonstock/Special Kit Component.*/
                    if oeelk.specnstype = "n" then
                        display oeelk.proddesc @ s-descrip with frame f-oeelb.
                    else if avail icsp and avail icsw then
                        display
                            icsp.descrip[1] + " " + icsp.descrip[2] @ s-descrip
                        with frame f-oeelb.
                    else display v-invalid @ s-descrip with frame f-oeelb.
                    down with frame f-oeelb.

/* SX32
/**************    print the product notes  *******************/
                    /*tb 17771 08/17/95 tdd; Notes print twice when reference
                        component */
                    if avail icsp and icsp.notesfl ne "" then do:
                        for each notes where
                            notes.cono         = g-cono    and
                            notes.notestype    = "p"       and
                            notes.primarykey   = icsp.prod and
                            notes.secondarykey = ""
                        no-lock:

                            if notes.printfl = yes then do i = 1 to 16:
                                /*tb 13890 02/21/94 dww; File name not specified
                                    causing error */
                                if notes.noteln[i] ne "" then do:
                                    display notes.noteln[i] with frame f-notes.
                                    down with frame f-notes.
                                end.

                            end.

                        end.

                    end. /* if avail icsp and icsp.notesfl ne "" */
SX32 */

            /**************    print the product notes  *******************/
            /*tb 17771 08/17/95 tdd; Notes print twice when reference
                                          component */
            if oeelk.specnstype ne "n":u and
               avail icsp and icsp.notesfl ne "" then
              run notesprt.p(g-cono,"p",icsp.prod,"","oeepa",8,1).
 
            else if oeelk.specnstype = "n":u
            then do:
              {icsc.gfi &prod = "oeelk.shipprod"
                                &lock = "no"}
            if avail icsc and icsc.notesfl ne "" then
              run notesprt.p(1,"g",icsc.catalog,"","oeepa",8,1).
            end. /* non-stock catalog notes */
          end. /* else do */
         end. /* for each oeelk */

    /***********skip subtotals **********************
        if oeelb.subtotalfl then do:
            assign v-subnetdisp = string(v-subnetamt,"zzzzzzzz9.99-")
                   v-subnetamt  = 0. 
            display v-subnetdisp with frame f-oeelsub.
        end.
        /* v-subnetamt was accumlating to a huge amount and blowing up when
           the order was over 250 lines. Inialize v-subnetamt for each line */
        assign v-subnetamt = 0.
    **********end skip ***********/    
    end. /* for each oeel */

end. /* if oeehb.transtype <> "ra" */

if s-amt2 = 0 and        /*das */
  oeehb.stagecd >= 0 and oeehb.stagecd <= 2 /**and
  oeehb.termstype <> "cod" and
  (zx-handle = true or
   oeehb.inbndfrtfl = true or
   oeehb.outbndfrtfl = true)**/ then
  do:
  s-shipmsg = "* Ship Dates subject to inventory availability" +
              " at the time the PO is received".
  display s-shipmsg with frame f-shipmsg.
end.

/************************ order totals ********************/
s-totqtyshp = if substring(string(v-totqtyshp,"999999999.99-"),11,2) = "00" then
                 string(v-totqtyshp,"zzzzzzzz9-")
              else
                 string(v-totqtyshp,"zzzzzzzz9.99-").

if oeehb.transtype <> "ra" then do:
    clear frame f-tot1.

    /*tb 7245 08/05/92 pap; Eliminate shipped qty from display  */
    /* REVERSAL OF TB 7245 08/12/92 BY DESIGN MEETING           */
    
    /*si02 begin*/
    if (substr(oeehb.user5,1,1) = "c") /* or
       (oeehb.currencyty = "lb" and oeehb.user6 <> 0) */ then
    do: 
       display v-linecnt
               s-lit40a
               s-lit40c            /*    ***** Qty Shipped Total *****     */
               s-totqtyshp
               s-lit40d
               s-cantot @ s-totlineamt
               with frame f-tot1.
    end. 
    else
    do: 
       display v-linecnt
               s-lit40a
               s-lit40c            /*    ***** Qty Shipped Total *****     */
               s-totqtyshp
               s-lit40d
               s-totlineamt
               with frame f-tot1.
    end.
    if oeehb.wodiscamt <> 0 then
    do:
        clear frame f-tot2.
        assign
            s-title2 = s-lit41a
            s-amt2   = oeehb.wodiscamt *
                       (if oeehb.transtype = "rm" then -1 else 1) *
                       /*si02 added "if substr..."*/
                       (if (substr(oeehb.user5,1,1) = "c"
                            and oeehb.user6 <> 0) 
               /*   or (oeehb.currencyty = "lb" and oeehb.user6 <> 0) */
                             
                        then oeehb.user6
                            else 1)
            s-cantot = s-cantot + s-amt2.    /*si02*/
        display
            s-title2
            s-amt2
        with frame f-tot2.
        down with frame f-tot2.
    end.

    if oeehb.specdiscamt <> 0 then do:
        clear frame f-tot2.
        assign
            s-title2 = s-lit42a
            s-amt2   = oeehb.specdiscamt *
                       (if oeehb.transtype = "rm" then -1 else 1) *
                       /*si02 added "if substr..."*/
                       (if (substr(oeehb.user5,1,1) = "c"
                            and oeehb.user6 <> 0 )
                /*  or (oeehb.currencyty = "lb" and oeehb.user6 <> 0) */
                        then oeehb.user6
                            else 1)
            s-cantot = s-cantot + s-amt2.    /*si02*/
        display s-title2
                s-amt2
                with frame f-tot2.
                down with frame f-tot2.
    end.

    if oeehb.totcorechg <> 0 then do:
        clear frame f-tot2.
        assign
            s-title2 = s-lit43a
            s-amt2   = oeehb.totcorechg *
                       (if oeehb.transtype = "rm" then -1 else 1) *
                       /*si02 added "if substr..."*/
                       (if (substr(oeehb.user5,1,1) = "c"
                            and oeehb.user6 <> 0)
           /*   or (oeehb.currencyty = "lb" and oeehb.user6 <> 0) */
                             
                        then oeehb.user6
                            else 1)
            s-cantot = s-cantot + s-amt2.    /*si02*/
        display s-title2
                s-amt2
                with frame f-tot2.
                down with frame f-tot2.
    end.

    if oeehb.totrestkamt <> 0 then do:
        clear frame f-tot2.

        assign
            s-title2 = s-lit53a
            s-amt2   = oeehb.totrestkamt *
                       (if oeehb.transtype = "rm" then -1 else 1) *
                       /*si02 added "if substr..."*/
                       (if (substr(oeehb.user5,1,1) = "c"
                            and oeehb.user6 <> 0)
           /*   or (oeehb.currencyty = "lb" and oeehb.user6 <> 0) */
                        then oeehb.user6
                            else 1)
            s-cantot = s-cantot + s-amt2.    /*si02*/
        display s-title2
                s-amt2
                with frame f-tot2.
                down with frame f-tot2.
    end.

    /*  ITB# 2 */
    assign s-amt2 = 0
           zx-handle = false.
    do i = 1 to 4:
       assign s-amt2 = s-amt2 + oeehb.addonamt[i].
       if oeehb.addonno[i] = 5 and
          oeehb.addonamt[i] <> 0 then
         assign zx-handle = true. 
    end.
      
    if s-amt2 = 0 and
       oeehb.stagecd >= 0 and oeehb.stagecd <= 2 and
       oeehb.termstype <> "cod"  /**and
       (zx-handle = true or
        oeehb.inbndfrtfl = true or
        oeehb.outbndfrtfl = true)**/ then
      do:
      /*assign s-title2  = "Frt/Hndlg to be added at shipping"*/
      /*assign s-title2 = "Frt/Hndlg/Tax to be added at Shipping".*/
      assign s-title2  = " "
             s-amt2    = 0.
      display
          s-title2
      with frame f-tot2a.
      down with frame f-tot2a.
      end.
    else
    if s-amt2 ne 0 then do:
       clear frame f-tot2.
       if oeehb.stagecd >= 0 and oeehb.stagecd <= 2 and 
          oeehb.termstype <> "cod" then
         do:
         /*assign s-title2 = "Frt/Hndlg to be added at shipping"*/
         /*assign s-title2 = "Frt/Hndlg/Tax to be added at Shipping".*/
         assign s-title2 = " "
                s-amt2   = 0.
         display
           s-title2
         with frame f-tot2a.
         down with frame f-tot2a.
         end.
       else
         do:
         assign s-title2 = "Frt & Hndlg"
                s-amt2 = s-amt2 * (if oeehb.transtype = "rm" then -1 else 1)
                  /*si02 added "if substr..."*/
                  * (if (substr(oeehb.user5,1,1) = "c"
                       and oeehb.user6 <> 0 )
             /*  or (oeehb.currencyty = "lb" and oeehb.user6 <> 0) */
                   then oeehb.user6
                       else 1)
                s-cantot = s-cantot + s-amt2.    /*si02*/
        display
           s-title2
           s-amt2
        with frame f-tot2.
        down with frame f-tot2.
        end.
    end.

/* SurCharge   ---------------------------------------------------------
    /*tb 8561 11/11/92 mms */
    if oeehb.totdatccost <> 0 and v-icdatclabel <> "" then do:
        clear frame f-tot2.

        assign
            s-title2 = s-lit44a
            s-amt2   = oeehb.totdatccost *
                       (if oeehb.transtype = "rm" then -1 else 1) *
                       /*si02 added "if substr..."*/
                       (if (substr(oeehb.user5,1,1) = "c"
                            and oeehb.user6 <> 0)
              /*   or (oeehb.currencyty = "lb" and oeehb.user6 <> 0) */
                            
                        then oeehb.user6
                            else 1)
            s-cantot = s-cantot + s-amt2.    /*si02*/
        display
            s-title2
            s-amt2
        with frame f-tot2.
        down with frame f-tot2.
    end.
*/
    if g-country = "ca" 
       or substr(oeehb.user5,1,1) = "C"  /*SI02*/ then do:
        /* print gst tax amount */
        if oeehb.taxamt[4] <> 0 then do:
            clear frame f-tot2.

            assign
                s-title2 = s-lit49a
                s-amt2   = oeehb.taxamt[4] *
                           (if oeehb.transtype = "rm" then -1 else 1) *
                           /*si02 added "if substr..."*/
                           (if substr(oeehb.user5,1,1) = "c"
                                and oeehb.user6 <> 0 
                            then oeehb.user6
                                else 1)
                s-cantot = s-cantot + s-amt2.    /*si02*/
            display s-title2
                    s-amt2
                    with frame f-tot2.
                    down with frame f-tot2.
        end.

        /* print pst tax amount */
        if oeehb.taxamt[1] <> 0 then do:
            clear frame f-tot2.
            assign
                s-title2 = s-lit50a
                s-amt2   = oeehb.taxamt[1] *
                           (if oeehb.transtype = "rm" then -1 else 1) *
                           /*si02 added "if substr..."*/
                           (if substr(oeehb.user5,1,1) = "c"
                                and oeehb.user6 <> 0 
                            then oeehb.user6
                                else 1)
                s-cantot = s-cantot + s-amt2.    /*si02*/
            display s-title2
                    s-amt2
                    with frame f-tot2.
                    down with frame f-tot2.
        end.
    end. /* if g-country = "ca" */

    else if s-taxes <> 0 then do:
        clear frame f-tot2.
        assign
            s-title2 = s-lit45a
            s-amt2   = s-taxes *
                       (if oeehb.transtype = "rm" then -1 else 1) *
                       /*si02 added "if substr..."*/
                          (if (substr(oeehb.user5,1,1) = "c" 
                               and oeehb.user6 <> 0 ) 
                 /*     or (oeehb.currencyty = "lb" and oeehb.user6 <> 0)  */
                            then oeehb.user6
                            else 1)
            s-cantax1 = if {zzcandiv.zvl arsc.divno} and     
                                  substr(oeehb.user5,1,1) <> "c"     
                               then oeehb.taxamt[4] * 
                                    (if oeehb.user6 <> 0             
                                     then oeehb.user6                
                                     else 1)                        
                               else 0                               
            s-cantax2 = if {zzcandiv.zvl arsc.divno} and
                                  substr(oeehb.user5,1,1) <> "c"
                               then oeehb.taxamt[1] *
                                    (if oeehb.user6 <> 0
                                     then oeehb.user6
                                     else 1)
                               else 0
            s-canlit = if {zzcandiv.zvl arsc.divno} and     
                                 substr(oeehb.user5,1,1) <> "c"     
                              then "CANADIAN TAX AMOUNT: G.S.T.:" + 
                                   string(s-cantax1,"zzzzz9.99-") + 
                                   "  P.S.T.:" + 
                                   string(s-cantax2,"zzzzz9.99-")
                              else ""     
            s-cantot = s-cantot + s-amt2.    
            /*si02*/
        display s-canlit  /*SI02*/
                s-title2
                s-amt2
                with frame f-tot2.
                down with frame f-tot2.
    end.

    if s-dwnpmtamt <> 0 then do:
        clear frame f-tot2.
        assign
            s-title2 = if can-do("cs",oeehb.transtype) then s-lit46b
                       else s-lit46a
            s-amt2   = s-dwnpmtamt *
                       (if oeehb.transtype = "rm" then -1 else 1) *
                       /*si02 added "if substr..."*/
                      (if (substr(oeehb.user5,1,1) = "c"
                            and oeehb.user6 <> 0)
               /*  or (oeehb.currencyty = "lb" and oeehb.user6 <> 0) */
                             
                        then oeehb.user6
                            else 1)
            s-cantot = s-cantot + s-amt2.    /*si02*/
        display
            s-title2
            s-amt2
        with frame f-tot2.
        down with frame f-tot2.
    end.

end. /* if oeehb.transtype <> "ra" */

clear frame f-tot3.

/*tb 5413 06/20/92 mwb; added the writeoffamt to totinvamt */
assign
    s-totinvamt = if oeehb.transtype = "ra" then  0 /* oeehb.tendamt */
                  else (oeehb.totinvamt /* - oeehb.tendamt
                        - oeehb.writeoffamt */) *
                      (if oeehb.transtype = "rm" then -1 else 1)
    s-invtitle  = if oeehb.transtype = "ra" then s-lit47b else s-lit47a
    /*si02 added s-currencyty*/
    s-currencyty = if {zzcandiv.zvl arsc.divno} or
                      (substr(oeehb.user5,1,1) <> " " and oeehb.user6 <> 0) then
                     if substr(oeehb.user5,1,1) = "c" then
                     "**** QUOTE REFLECTS CANADIAN CURRENCY ***"
                     else
                       if substr(oeehb.user5,1,1) = "l" and 
                         oeehb.user6 <> 0 then
                       "**** QUOTE REFLECTS ENGLISH POUNDS ***"
                       else 
                       "  **** QUOTE REFLECTS U.S. CURRENCY ****"
                    else "".
    s-totinvamt = if substr(oeehb.user5,1,1) = "c" and oeehb.user6 <> 0
                         then s-cantot
                         else s-totinvamt.   /*si02*/
    
    /* don't include freight and handling */
    if oeehb.stagecd >= 0 and 
       oeehb.stagecd <= 2 and
       s-totinvamt  <>  0 then do:  /* das - changed s-totinvamt > 0 to <> 0 */
         if (substr(oeehb.user5,1,1) = "c" and oeehb.user6 <> 0) then
            s-totinvamt = s-cantot.
         else              
           do i = 1 to 4:
             assign s-totinvamt = s-totinvamt - oeehb.addonamt[i].
           end.
       if s-totinvamt < 0 then
          assign s-totinvamt = 0.
    end.
/*
/** frt/handling literal */
display shiplit @ s-title2
with frame f-tot2a.
*/

display
    s-currencyty  /*Si02*/
    s-invtitle
    s-totinvamt
with frame f-tot3.

/*tb# 5929 06/18/92 mkb */
/*
if oeehb.tottendamt ne 0 then do:
   clear frame f-tot5.
   if /* oeehb.boexistsfl = no and */
       (oeehb.orderdisp = "j"  or oeehb.transtype = "bl")
   then do:
       find first b-oeehb use-index k-oeehb where
                  b-oeehb.cono       = oeehb.co       and
                  b-oeehb.batchnm    = oeehb.batchnm  and
                  b-oeehb.seqno      = 2 and
                  b-oeehb.sourcepros = "ComQu"
       no-lock no-error.

       if avail b-oeehb then
           display oeehb.tottendamt 
                  /*SI02 beg*/
                  * (if (substr(oeehb.user5,1,1) = "c" 
                     and oeehb.user6 <> 0)
               /*  or (oeehb.currencyty = "lb" and oeehb.user6 <> 0)  */                                    
                     then oeehb.user6 
                      else 1) /*SI02 end*/
           with frame f-tot5.
   end.

   else if oeehb.boexists = yes then
       display oeehb.tottendamt 
                  /*SI02 beg*/
                  * (if (substr(oeehb.user5,1,1) = "c" 
                     and oeehb.user6 <> 0) or
                    (oeehb.currencyty = "lb" and oeehb.user6 <> 0)                      
                     then oeehb.user6 
                      else 1) /*SI02 end*/
       with frame f-tot5.
end.
*/

display s-tariffmsg with frame f-footnote.
down with frame f-footnote.
/* das - NonReturn */
/*  display s-asteriskdtla
          s-asteriskdtlb
          s-asteriskdtlc
          /**
          s-buysignature
          s-sunsignature
          **/
  with frame f-footnote.
  down with frame f-footnote. */

assign
    s-lit48a = "Last Page"
    s-cantot = 0.   /*si02*/

if p-fax then 
do:
   find first oimsp where oimsp.person = g-operinits no-lock no-error.
   if avail(oimsp) then
      assign s-CSR-title  = "Customer Service Contact: "
             s-contact    = oimsp.name
             s-contlit-ph = "TEL:"
             s-contact-ph = oimsp.phoneno
             s-contlit-fx = "FAX:"
             s-contact-fx = oimsp.faxphoneno.
   else 
   assign s-CSR-title  = ""
          s-contact    = ""
          s-contlit-ph = ""
          s-contact-ph = ""
          s-contlit-fx = ""
          s-contact-fx = "".
end.
else 
do: 
  find first oimsp where oimsp.person = oeehb.takenby no-lock no-error.
  if avail(oimsp) then
     assign s-CSR-title  = "Customer Service Contact:"
            s-contact    = oimsp.name
            s-contlit-ph = "TEL:"
            s-contact-ph = oimsp.phoneno
            s-contlit-fx = "FAX:"
            s-contact-fx = oimsp.faxphoneno.
  else
     assign s-CSR-title  = ""
            s-contact    = ""
            s-contlit-ph = ""
            s-contact-ph = ""
            s-contlit-fx = ""
            s-contact-fx = "".
end.          

if s-CSR-title = "" then 
do: 
   find first oimsp where oimsp.person = g-operinits no-lock no-error.
   if avail(oimsp) then
      assign s-CSR-title  = "Customer Service Contact: "
             s-contact    = oimsp.name
             s-contlit-ph = "TEL:"
             s-contact-ph = oimsp.phoneno
             s-contlit-fx = "FAX:"
             s-contact-fx = oimsp.faxphoneno.
end.


