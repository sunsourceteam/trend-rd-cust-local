/*h****************************************************************************
  INCLUDE      : f-arse2.i
  DESCRIPTION  : AR Setup Electronic Data (EDI, FAX) form include - continued
  USED ONCE?   : yes
  AUTHOR       : wdb
  DATE WRITTEN : 09/29/00
  CHANGES MADE :
    09/29/00 wdb; TB# e5822 ecommwhse
    04/27/05 kjb; TB# e22432 Add edicatprodfl and edinsprodfl so that CHUI only
        users can use catalog products with Storefront
      ******************************************************************************/
skip (1)
"EDI -"             at 3
s-edipartner    colon 19    label "Partner Code"
    validate(if (input s-eacktype ne "e" and input s-einvtype ne "e") or
                 input g-shipto ne ""
             then true
             else s-edipartner ne "",
                "This is a Required Field (2100)")
    help "EDI Partner Name as Defined in Your EDI Software (REQ)"
s-ediackver     colon 19    label "Acknow. Version"
    help "ANSI Version 002002, 002003, 002040, etc."
s-ediinvver     colon 63    label "Invoice Version"
    help "ANSI Version 002002, 002003, 002040, etc."
s-ediordcd      colon 19    label "Order Status Cd"
    help "ANSI X12 Shipment/Order Status Code, Element 368"
s-edicatprodfl  colon 63    label "Accept Catalog Products"
    help "If yes, Will Accept Catalog Products from EDI"
s-edichgcd      colon 19    label "Change Reason Cd"
    help "ANSI X12 Change Reason Code, Element 371"
s-edinsprodfl   colon 63    label "Accept Non Stock Products"
    help "If yes, Will Accept Non Stock Products via EDI"
s-edinetwork    colon 19    label "EDI User 1"
s-edipartaddr   colon 19    label "EDI User 2"
s-ediyouraddr   colon 19    format "x(30)" label "EDI User 3"
with frame f-arse side-labels row 1 width 80 overlay no-hide
