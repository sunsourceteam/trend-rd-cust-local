/*

  Parameter {1}  is the prodcost of the OE line item
  Parameter {2}  Is a logical if an order was found

*/
 
 
 
  if vaeh.stagecd < 9 then
    do:
    find vaelo where vaelo.cono = 1 
                 and vaelo.vano = vaeh.vano
                 and vaelo.vasuf = vaeh.vasuf
                 and vaelo.lineno = 0 no-lock no-error.
    if avail vaelo then
     do:
     if vaelo.ordertype = "o" then
       do:
       if vaelo.seqaltno <> 0 then do:
         find oeel where oeel.cono = vaelo.cono 
                     and oeel.orderno = vaelo.orderaltno
                     and oeel.ordersuf = vaelo.orderaltsuf
                     and oeel.lineno = vaelo.linealtno
                     no-lock no-error.

         find oeelk where oeelk.cono = vaelo.cono 
                     and oeelk.ordertype = "o" 
                     and oeelk.orderno = vaelo.orderaltno
                     and oeelk.ordersuf = vaelo.orderaltsuf
                     and oeelk.lineno = vaelo.linealtno
                     and oeelk.seqno  = vaelo.seqaltno
                   no-lock no-error.
         if avail oeelk then
           assign
             {1} = oeelk.prodcost
             {2} = false.
       end. 
       else do:
         find oeel where oeel.cono = vaelo.cono 
                     and oeel.orderno = vaelo.orderaltno
                     and oeel.ordersuf = vaelo.orderaltsuf
                     and oeel.lineno = vaelo.linealtno
                     no-lock no-error.
         if avail oeel then
           assign
             {1} = oeel.prodcost
             {2} = true.
         end. 
      end.
    else             
    if vaelo.ordertype = "t" then
      do:
      find wtel where wtel.cono = vaelo.cono 
                  and wtel.wtno = vaelo.orderaltno
                  and wtel.wtsuf = vaelo.orderaltsuf
                  and wtel.lineno = vaelo.linealtno
                  no-lock no-error.
      if avail wtel then
        do:
        find wtelo where wtelo.cono = 1 
                and wtelo.wtno = wtel.wtno
                and wtelo.wtsuf = wtel.wtsuf 
                and wtelo.lineno = wtel.lineno no-lock no-error.
        if avail wtelo then
          if wtelo.ordertype = "o" then
            do:
            find oeel where oeel.cono = wtelo.cono 
                        and oeel.orderno = wtelo.orderaltno
                        and oeel.ordersuf = wtelo.orderaltsuf
                        and oeel.lineno = wtelo.linealtno
                        no-lock no-error.
            
            if avail oeel then
              assign
              {1} = oeel.prodcost
              {2} = true. 
            end. 
          
        end.  
      end.                
    end.
  end.