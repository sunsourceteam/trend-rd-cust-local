/* *************************************************************************
PROGRAM NAME: p-vaexf-mid.i
PROJECT NO.:
DESCRIPTION:  Line Item Routines for VAEXF
DATE WRITTEN: 06/24/10
AUTHOR      : based on VAEXQ written by Tony Abbate
              Tom Herzog



Modifications:

tahtrain: training bugs

***************************************************************************/


/* -------------------------------------------------------------------------  */
PROCEDURE Middle1:
/* -------------------------------------------------------------------------  */
  
  RUN makequoteno(g-fabquoteno,OUTPUT x-quoteno).
  IF v-errfl THEN DO:
    ASSIGN v-framestate = "t1".
    LEAVE.
  END.
  IF v-linebrowseopen = FALSE THEN DO:
    OPEN QUERY q-lines FOR EACH t-lines use-index ix1.
    ASSIGN v-linebrowseopen = TRUE.
  END.
    ASSIGN fs-2 =
"  F6-Top  F7-Extended  F8-Totals  F9-SPcNotes  F10-FabLookup                        ".
  DISPLAY fs-2 WITH FRAME f-statusline2.
  ASSIGN
    v-prod      = ""
    v-attrfl    = d-attrfl
    s-ZsdiXrefType = ""
    v-prodline  = ""
    d-prod      = ""
    v-descrip   = ""
    v-descrip2  = ""
    f-descrip   = ""
    z-descrip   = ""
    ctrlw-whse  = ""
    v-altwhse   = ""
    v-astr1     = ""
    v-astr2     = ""
    v-whse      = ""
    v-gp        = 0
    v-qty       = 1
    v-vendno    = 0
    v-vendnm    = ""
    v-prodcat   = ""
    v-sellprc   = 0
    v-Intangables   = 0
    v-prodcost  = 0
    v-specnstype = ""
    v-nslistprc = 0
    v-costgain  = 0
    v-labormins = 0
    o-labormins = 0
    v-leadtm    = ""
    v-prctype   = ""
    v-error     = "".
  DISPLAY b-lines WITH FRAME f-lines.
  HIDE FRAME f-bot1.
  MidLoop:
  DO WHILE TRUE ON ENDKEY UNDO, LEAVE MidLoop:
    IF NOT v-loadbrowseline THEN DO:
      FIND LAST t-lines use-index ix1 NO-LOCK NO-ERROR.
      IF avail t-lines THEN DO:
        DEF BUFFER n-t-lines FOR t-lines.
        FIND n-t-lines WHERE
             n-t-lines.lineno = o-lineno + 1
        NO-LOCK NO-ERROR.
        IF avail n-t-lines AND n-t-lines.specnstype = "l" THEN DO:
          ASSIGN
            o-lineno = o-lineno + 1
            v-lineno = o-lineno + 1.
          DISPLAY v-lineno WITH FRAME f-mid1.
          NEXT.
        END.
        /* v-lineno will be next open line if not ctrl-o(nxt) function used -
        v-lineno can change - we need to find the correct table line here */
        IF avail n-t-lines AND nextln NE "" AND nextln = "Chg" THEN DO:
          ASSIGN v-lineno = (o-lineno + 1).
        END.
        ELSE DO:
          ASSIGN
            v-lineno = (if avail t-lines then 
                          t-lines.lineno + 1
                        else if avail n-t-lines then
                          n-t-lines.lineno + 1
                        else 1  )
            v-specnstype = "".
          find t-lines where t-lines.lineno = v-lineno no-error.
          /* clear t-lines for new line */    
        END.
        IF avail n-t-lines AND nextln NE "" AND nextln = "Chg" THEN DO:
          IF avail n-t-lines AND v-linebrowseopen THEN DO:
            ASSIGN
              v-row = b-lines:FOCUSED-ROW
              v-rowid = ROWID(n-t-lines).
            REPOSITION q-lines TO ROWID(v-rowid).
            b-lines:SET-REPOSITIONED-ROW(v-row).
/*          IF n-t-lines.prod = "" THEN */
              DISPLAY fs-2 WITH FRAME f-statusline2.
/*          ELSE
              DISPLAY fs-3 @ fs-2 WITH FRAME f-statusline2. */
          END.
           ASSIGN
            z-descrip    = n-t-lines.gdesc
            v-lmode      = "c"
            v-attrfl     = n-t-lines.attrfl
            v-seqno      = n-t-lines.seqno
            v-lineno     = n-t-lines.lineno
            v-prod       = n-t-lines.prod
            v-nslistprc  = if n-t-lines.specnstype = "n" then
                             n-t-lines.listprc
                           else
                             0
            d-prod       = n-t-lines.prod
            v-prodline   = n-t-lines.prodline
            s-ZsdiXrefType = n-t-lines.ZsdiXrefType
            v-prodcat    = n-t-lines.prodcat
            v-vendno     = n-t-lines.vendno
            v-vendnm     = n-t-lines.vendnm
            v-prctype    = n-t-lines.prctype
            v-whse       = IF n-t-lines.altwhse NE "" AND
                           n-t-lines.altwhse NE x-whse THEN
                             n-t-lines.altwhse
                           ELSE
                             n-t-lines.whse
            v-prodcost   = n-t-lines.prodcost
            v-descrip    = n-t-lines.descrip
            v-qty        = n-t-lines.qty
            v-specnstype = n-t-lines.specnstype
            v-matrixed   = IF n-t-lines.qtybrkty <> "" THEN
                             n-t-lines.qtybrkty
                           ELSE
                             ""
            v-leadtm     = LEFT-TRIM(n-t-lines.leadtm,"0")
            v-astr1      = IF v-whse NE x-whse AND
                           v-whse NE "" THEN
                             "*"
                           ELSE
                             ""
            v-astr2      = " "
            o-leadtm     = n-t-lines.leadtm
            o-descrip    = IF n-t-lines.gdesc NE "" THEN
                             n-t-lines.gdesc
                           ELSE
                             n-t-lines.descrip
            o-attrfl     = n-t-lines.attrfl
            o-gdesc      = n-t-lines.gdesc
            o-lineno     = n-t-lines.lineno
            o-seqno      = n-t-lines.seqno
            o-prod       = n-t-lines.prod
            o-prctype    = n-t-lines.prctype
            o-prodcat    = n-t-lines.prodcat
            o-vendno     = n-t-lines.vendno
            o-whse       = v-whse
            o-prodcost   = n-t-lines.prodcost
            o-qty        = n-t-lines.qty
            o-specnstype = n-t-lines.specnstype.
        END.
      END. /* avail t-lines */
      ELSE
        ASSIGN v-lineno = 1
               o-attrfl     = d-attrfl
               o-leadtm     = ""
               o-descrip    = ""
               o-gdesc      = ""
               o-lineno     = 1
               o-seqno      = 0
               o-prod       = ""
               o-prctype    = ""
               o-prodcat    = ""
               o-vendno     = 0
               o-whse       = v-whse
               o-prodcost   = 0
               o-qty        = 0
               o-specnstype = ""
               v-lmode      = "a".
    END.
    
    ASSIGN
      v-loadbrowseline = FALSE
      v-astr2 = " ".
    DISPLAY v-astr2 WITH FRAME f-mid1.
    
    /* TAH */
    FIND t-lines WHERE t-lines.lineno = v-lineno NO-ERROR.
    IF not avail t-lines then DO:
      ASSIGN 
        v-labormins = 0
        v-drawingmins = 0
        v-electricalmins = 0
        v-mechanicalmins = 0
        v-docmins     = 0
        o-leadtm     = ""
        o-attrfl     = d-attrfl
        o-descrip    = ""
        o-gdesc      = ""
        o-lineno     = v-lineno
        o-seqno      = 0
        o-prod       = ""
        o-prctype    = ""
        o-prodcat    = ""
        o-vendno     = 0
        o-whse       = v-whse
        o-prodcost   = 0
        o-qty        = 0
        o-specnstype = "".
      run Create-Vaspmap (input "New",
                          input v-lineno,
                          input v-prod,
                          input x-whse,
                          input v-qty,
                          input v-nslistprc,
                          input v-specnstype).
      assign v-lmode      = "a".
    END.  
    
    
    IF (avail t-lines AND t-lines.prod NE "") THEN DO:
      ASSIGN
        v-astr2 = ""
        v-hit   = NO.
      DISPLAY v-astr2 WITH FRAME f-mid1.
      FIND FIRST notes USE-INDEX k-notes WHERE
                 notes.cono         = g-cono       AND
                 notes.notestype    = "p"          AND
                 notes.primarykey   = t-lines.prod AND
                 notes.secondarykey = ""
      NO-LOCK NO-ERROR.
      IF avail notes THEN DO:
        DO v-idx = 1 TO 16:
          IF notes.noteln[v-idx] NE " " THEN DO:
            ASSIGN v-hit = YES.
            LEAVE.
          END.
        END.
      END.
      IF v-hit AND avail notes AND notes.requirefl = YES THEN DO:
        ASSIGN v-astr2 = "!".
      END.
      ELSE
      IF v-hit AND avail notes AND notes.requirefl = NO THEN DO:
        ASSIGN v-astr2 = "*".
      END.
      DISPLAY v-astr2 WITH FRAME f-mid1.
    END.

    ASSIGN  v-linelit = v-linenormlit.

    RUN DisplayMid1.
    
    ASSIGN
      o-descrip    = v-descrip
      o-vendno     = v-vendno
      o-leadtm     = v-leadtm
      o-lineno     = v-lineno
      o-specnstype = v-specnstype
      o-attrfl     = v-attrfl
      o-prod       = v-prod
      o-prctype    = v-prctype
      o-whse       = v-whse
      o-listprc    = v-Intangables
      o-prodcost   = v-prodcost
      o-qty        = v-qty
      o-nslistprc  = v-nslistprc.
    ASSIGN
      v-prodbrowsekeyed = FALSE
      p-brwsfl          = FALSE.

    IF fldpos2 = 1 THEN
      NEXT-PROMPT v-lineno WITH FRAME f-mid1.
    ELSE
    IF fldpos2 = 2 THEN 
      NEXT-PROMPT v-prod WITH FRAME f-mid1.
    ELSE
    IF fldpos2 = 3 THEN
      NEXT-PROMPT v-attrfl WITH FRAME f-mid1.
    ELSE
    IF fldpos2 = 4 THEN
      NEXT-PROMPT v-qty WITH FRAME f-mid1.
    ELSE
    IF fldpos2 = 5 THEN
      NEXT-PROMPT v-prodcost WITH FRAME f-mid1.
    ELSE
    IF fldpos2 = 6 THEN
      NEXT-PROMPT v-descrip WITH FRAME f-mid1.
    ELSE
      NEXT-PROMPT v-prod WITH FRAME f-mid1.
    ASSIGN v-descronentry = FALSE.

    UPDATE v-lineno
HELP "Enter Line number to edit. Ctrl-D Delete Or CTRL-L to Reposition Lines."
      v-prod                                         
      HELP
      "Enter data or press PF4                                       "
      v-attrfl
      v-qty      FORMAT ">>>>>9.99"
      HELP
      "Enter data or press PF4                                        "
      v-prodcost
      HELP
      "Enter data or press PF4 to end.                                "
/*    v-descrip
      HELP
      "Enter data or press PF4 to end.                                "
*/    
    GO-ON ({k-vaexqon.i})
    WITH FRAME f-mid1
    EDITING: 
      v-applygo = FALSE.
      IF leave-fl THEN DO:
        ASSIGN leave-fl = NO.
        IF LASTKEY = 404 or lastkey = 13 THEN DO:
          NEXT-PROMPT v-prod WITH FRAME f-mid1.
          NEXT.
        END.

      END.
      ELSE DO:
        READKEY.
      END.
      
/* Catalog */
      IF KEYLABEL(LASTKEY) = "f13" AND FRAME-FIELD = "v-prod" THEN DO:
        
        CLOSE QUERY q-prodlu.
        ASSIGN
          v-prodbrowseopen = FALSE
          v-prodbrowsekeyed = FALSE.
  
        ASSIGN v-prodlutype = "x".
        RUN DoCatLu (INPUT v-Prod,
                     INPUT (IF KEYLABEL(LASTKEY) = "f12" THEN
                              "normal"
                            ELSE
                              "function") ).
        DISPLAY v-prod WITH FRAME f-mid1.
      
      END.
      /* Catalog */ 
 
 


      IF KEYLABEL(LASTKEY) = "F8" THEN DO:
        IF (FRAME-NAME = "f-mid1" and
            (frame-field = "v-qty" or
             frame-field = "v-prod" or
             frame-field = "v-prodcost" or
             frame-field = "v -descrip")) or v-lmode = "c" then DO:

          
          if frame-field = "v-lineno" then
            assign  fldpos2 = 1.
          if frame-field = "v-prod" then
            assign  fldpos2 = 2.
          if frame-field = "v-attrfl" then
            assign  fldpos2 = 3.
            
          if frame-field = "v-qty" then
            assign  fldpos2 = 4.
          if frame-field = "v-prodcost" then
            assign  fldpos2 = 5.
          if frame-field = "v-descrip" then
            assign  fldpos2 = 6.
          run f8totals.   
/*
          if fldpos2 = 1 then
            APPLY "entry" TO v-lineno   IN FRAME f-mid1.
          if fldpos2 = 2 then
            APPLY "entry" TO v-prod     IN FRAME f-mid1.
          if fldpos2 = 3 then
            APPLY "entry" TO v-qty      IN FRAME f-mid1.
          if fldpos2 = 4 then
            APPLY "entry" TO v-prodcost IN FRAME f-mid1.
          if fldpos2 = 5 then
            APPLY "entry" TO v-descrip  IN FRAME f-mid1.
*/
        ASSIGN
          fldpos2 = 2
          v-prod      = ""
          v-prodline  = ""
          s-ZsdiXrefType = ""
          d-prod      = ""
          v-attrfl    = d-attrfl
          v-astr2     = ""
          v-descrip   = ""
          v-descrip2  = ""
          f-descrip   = ""
          z-descrip   = ""
          ctrlw-whse  = ""
          v-altwhse   = ""
          v-astr1     = ""
          v-astr2     = ""
          v-whse      = ""
          v-gp        = 0
          v-qty       = 1
          v-vendno    = 0
          v-specnstype = ""
          v-vendnm    = ""
          v-prodcat   = ""
          v-sellprc   = 0
          v-Intangables   = 0
          v-prodcost  = 0
          v-costgain  = 0
          v-labormins = 0
          o-labormins = 0
          v-nslistprc = 0
          v-leadtm    = ""
          v-prctype   = ""
          v-error     = "".
        DISPLAY b-lines WITH FRAME f-lines.
        v-leadtm:dcolor = 0.
        v-leadtm = "".
        DISPLAY
          v-lineno
          v-specnstype
          v-prod
          v-attrfl
          v-astr2
          v-qty      FORMAT ">>>>>9.99"
          v-prodcost
          v-descrip
          v-leadtm
        WITH FRAME f-mid1.
          o-lineno = v-lineno - 1.   
          assign v-loadbrowseline = false.
          NEXT midloop.
        END.
      END.


/*      
      IF (FRAME-FIELD = "v-descrip" AND LASTKEY = 401) AND
          fldpos2 = 3 AND v-descronentry  = TRUE AND
          INPUT v-descrip NE "" AND
          ((o-prod NE INPUT v-prod) OR
           (avail t-lines AND t-lines.lineno NE INPUT v-lineno)) THEN DO:
        ASSIGN v-error = "Enter/Confirm qty field pls.".
        RUN zsdi_vaerrx.p(v-error,"yes").
        ASSIGN v-descronentry = FALSE.
        NEXT-PROMPT v-qty WITH FRAME f-mid1.
        NEXT.
      END.
*/      
      IF INPUT v-qty = 0 AND (KEYFUNCTION(LASTKEY) = "go" OR
        (KEYFUNCTION(LASTKEY) = "return" AND FRAME-FIELD = "v-prodcost"))
      THEN DO:
        ASSIGN v-error = "Qty required field - please enter.".
        RUN zsdi_vaerrx.p(v-error,"yes").
        NEXT-PROMPT v-qty WITH FRAME f-mid1.
        NEXT.
      END.
/*
      IF (FRAME-FIELD = "v-descrip" AND LASTKEY = 13 ) AND
          v-descronentry = TRUE  THEN DO:
        IF fldpos2 = 2 THEN DO:
          ASSIGN v-error = "Enter/Confirm qty field pls.".
          RUN zsdi_vaerrx.p(v-error,"yes").
          ASSIGN v-descronentry = FALSE.
          NEXT-PROMPT v-qty WITH FRAME f-mid1.
        END.
        ELSE
        NEXT-PROMPT v-prod WITH FRAME f-mid1.
        ASSIGN v-descronentry = FALSE.
        ON cursor-up back-tab.
        ON cursor-down tab.
        NEXT.
      END.
      IF FRAME-FIELD = "v-descrip" AND
        (KEYLABEL(LASTKEY) = "cursor-left" AND
         INPUT v-descrip:CURSOR-OFFSET = 1) THEN DO:
        APPLY LASTKEY.
        NEXT-PROMPT v-prodcost WITH FRAME f-mid1.
        ON cursor-up back-tab.
        ON cursor-down tab.
        NEXT.
      END.
  
      ASSIGN v-descronentry = FALSE.
*/
      IF nextln = "Chg" AND KEYLABEL(LASTKEY) = "pf4" THEN DO:
        DEF BUFFER f4-t-lines FOR t-lines.
        FIND f4-t-lines WHERE
             f4-t-lines.lineno = INPUT v-lineno
        NO-LOCK NO-ERROR.
        IF avail f4-t-lines THEN DO:
          ASSIGN
            v-framestate = "t1"
            v-found  = NO.
          READKEY PAUSE 0.
          RETURN.
        END.
      END.
      IF LASTKEY = 404 THEN DO:
      /* Clear buffers for a record that was started and
         canceled 
      */
        if not avail t-lines then do:
          
              
          find first rarr-com use-index k-com where 
                     rarr-com.cono     = g-cono      and
                     rarr-com.comtype  = x-quoteno   and  /* Quote No */
                     rarr-com.orderno  = 0           and
                     rarr-com.ordersuf = 0           and
                     rarr-com.lineno   = v-lineno 
                     no-error.
          if avail rarr-com then do:
            delete rarr-com.
          end.
          
          find zsdivaspmap where
               zsdivaspmap.cono = g-cono and
               zsdivaspmap.rectype  = "S" and        /* VAES */
               zsdivaspmap.prod     = x-quoteno  and
               zsdivaspmap.typename = "INVENTORY" and
               zsdivaspmap.lineno = 0  no-lock  no-error.
          if avail zsdivaspmap then do:
            for each t-vaspmap where
                     t-vaspmap.cono = g-cono and
                     t-vaspmap.rectype  = "LQ" and        /* VAES */
                     t-vaspmap.prod     = x-quoteno  and
                     t-vaspmap.seqno    = zsdivaspmap.seqno and
                     t-vaspmap.lineno   = v-lineno :
              delete t-vaspmap.
            end.
          end.
        end.
        ASSIGN
          fldpos2 = 2
          v-prod      = ""
          v-prodline  = ""
          s-ZsdiXrefType = ""
          v-attrfl    = d-attrfl
          d-prod      = ""
          v-astr2     = ""
          v-descrip   = ""
          v-descrip2  = ""
          f-descrip   = ""
          z-descrip   = ""
          ctrlw-whse  = ""
          v-altwhse   = ""
          v-astr1     = ""
          v-astr2     = ""
          v-whse      = ""
          v-gp        = 0
          v-qty       = 1
          v-vendno    = 0
          v-vendnm    = ""
          v-prodcat   = ""
          v-sellprc   = 0
          v-Intangables   = 0
          v-prodcost  = 0
          v-costgain  = 0
          v-labormins = 0
          o-labormins = 0
          v-nslistprc = 0
          v-leadtm    = ""
          v-prctype   = ""
          v-specnstype = " "
          v-error     = "".
        DISPLAY b-lines WITH FRAME f-lines.
        v-leadtm:dcolor = 0.
        DISPLAY
          v-specnstype
          v-lineno
          v-prod
          v-attrfl
          v-astr2
          v-qty      FORMAT ">>>>>9.99"
          v-prodcost
          v-descrip
          v-leadtm
        WITH FRAME f-mid1.
        NEXT MidLoop.
      END.
      
      IF INPUT v-prod NE "" THEN
        DISPLAY fs-3 @ fs-2 WITH FRAME f-statusline2.
      ELSE
        DISPLAY "y" + fs-2 WITH FRAME f-statusline2.

      b-lines:TITLE IN FRAME f-lines =
"   Ln# Whse Product                       Qty       Description          LdTm".
 

      IF (FRAME-FIELD = "v-lineno" AND (keylabel(LASTKEY) = "Ctrl-L")) and
       (not avail t-lines) THEN DO:
        RUN zsdi_vaerrx.p
(input "(M001): Valid lines to reposition are line items that are committed ",
                      input true).
        next.
      end.  

      IF (FRAME-FIELD = "v-lineno" AND (keylabel(LASTKEY) = "Ctrl-L")) and
       avail t-lines and v-linebrowseopen = TRUE THEN DO:
        run LineMover (input t-lines.lineno).
        /*
        IF v-linebrowseopen = TRUE OR LASTKEY = 24 THEN DO:
           b-lines:REFRESH().
        end.
        */
        o-lineno = v-lineno - 1.   
        assign v-loadbrowseline = false.
       
        ASSIGN                                               
          v-prod      = ""
          v-attrfl    = d-attrfl
          v-prodline  = ""
          s-ZsdiXrefType = ""
          d-prod      = ""
          v-descrip   = ""
          v-descrip2  = ""
          f-descrip   = ""
          z-descrip   = ""
          ctrlw-whse  = ""
          v-altwhse   = ""
          v-astr1     = ""
          v-astr2     = ""
          v-whse      = ""
          v-gp        = 0
          v-qty       = 1
          v-vendno    = 0
          v-vendnm    = ""
          v-prodcat   = ""
          v-sellprc   = 0
          v-Intangables   = 0
          v-prodcost  = 0
          v-nslistprc = 0
          v-costgain  = 0
          v-labormins = 0
          o-labormins = 0
          v-leadtm    = ""
          v-prctype   = ""
          v-error     = "".
        
        run DisplayMid1.

        input clear.
        
        NEXT midloop.
      END.
 

/*      
      IF ((FRAME-FIELD = "v-lineno" AND (LASTKEY = 32 OR LASTKEY = 24))) OR
         ((FRAME-FIELD = "v-qty" OR FRAME-FIELD = "v-prodcost") AND
          LASTKEY = 24) THEN DO:
        IF togglefl = YES THEN DO:
          ASSIGN togglefl = NO.
          b-lines:TITLE IN FRAME f-lines =
"   Ln# Whse Product                       Qty       Description          LdTm".
        END.
        ELSE DO:
          ASSIGN togglefl = YES.
          b-lines:TITLE IN FRAME f-lines =
"   Ln# Whse Product                       Qty  Icsp-Description          LdTm".
        END.
        IF v-linebrowseopen = TRUE OR LASTKEY = 24 THEN DO:
          b-lines:REFRESH().
        END.
        APPLY LASTKEY.
        DISPLAY v-lineno v-specnstype WITH FRAME f-mid1.
        NEXT.
      END.
*/      


      IF KEYLABEL(LASTKEY) = "F7" THEN DO:
        IF (FRAME-NAME = "f-mid1" and
            (frame-field = "v-qty" or
             frame-field = "v-prodcost" or
             frame-field = "v-attrfl" or
             frame-field = "v -descrip")) or v-lmode = "c" then DO:

          
          if frame-field = "v-lineno" then
            assign  fldpos2 = 1.
          if frame-field = "v-prod" then
            assign  fldpos2 = 2.
          if frame-field = "v-attrfl" then
            assign  fldpos2 = 3.
          if frame-field = "v-qty" then
            assign  fldpos2 = 4.
          if frame-field = "v-prodcost" then
            assign  fldpos2 = 5.
          if frame-field = "v-descrip" then
            assign  fldpos2 = 6.
/* tahtrain */
          ASSIGN 
            v-lineno    = input v-lineno
            v-prod      = input v-prod
            v-attrfl    = input v-attrfl
            v-qty       = input v-qty
            v-prodcost  = input v-prodcost.
        
         
            run Create-Vaspmap (input if (avail t-lines and v-prod = o-prod)
                                         or (v-prod = o-prod) then
                                         "updt"
                                      else
                                      if avail t-lines and v-prod <> o-prod then
                                         "repl"
                                      else
                                         "new",
                                input v-lineno,
                                input v-prod,
                                input x-whse,
                                input v-qty,
                                input v-nslistprc,
                                input v-specnstype).
 
/* tahtrain */
          run ExtendedLine_Popup.   
          if fldpos2 = 1 then
            APPLY "entry" TO v-lineno   IN FRAME f-mid1.
          if fldpos2 = 2 then
            APPLY "entry" TO v-prod     IN FRAME f-mid1.
          if fldpos2 = 3 then
            APPLY "entry" TO v-attrfl   IN FRAME f-mid1.
          if fldpos2 = 4 then
            APPLY "entry" TO v-qty      IN FRAME f-mid1.
          if fldpos2 = 5 then
            APPLY "entry" TO v-prodcost IN FRAME f-mid1.
          if fldpos2 = 6 then
            APPLY "entry" TO v-descrip  IN FRAME f-mid1.
/* tahtrain */            
          DISPLAY
            v-specnstype
            v-lineno
            v-prod
            v-attrfl
            v-astr2
            v-qty      FORMAT ">>>>>9.99"
            v-prodcost
            v-descrip
            v-leadtm
          WITH FRAME f-mid1.
/* tahtrain */              
          assign  fldpos2 = 2.
          NEXT .
        END.
      END.

      IF CAN-DO("f6,f9",KEYLABEL(LASTKEY)) THEN DO:
        CLOSE QUERY q-prodlu.
        ASSIGN
          v-prodbrowseopen  = FALSE
          v-prodbrowsekeyed = FALSE.
        HIDE FRAME f-prodlu.
        HIDE FRAME f-whses.
        HIDE FRAME f-bot2.
        ON cursor-up back-tab.
        ON cursor-down tab.
        promptpos = 0.
        assign v-ManualFrameJump = true.
        RUN FindNextFrame (INPUT LASTKEY,
                           INPUT-OUTPUT v-CurrentKeyPlace,
                           INPUT-OUTPUT v-framestate).
        assign v-ManualFrameJump = false.
        APPLY LASTKEY.
        LEAVE MidLoop.
      END.
      
      IF (KEYFUNCTION(LASTKEY) = "end-error" OR KEYLABEL(LASTKEY) = "f11")
        OR v-empty  OR
        (INPUT v-prod = "" AND 
        /* INPUT v-descrip = "" AND */
        NOT p-brwsfl AND NOT
        CAN-FIND(FIRST e-t-lines WHERE e-t-lines.lineno = INPUT v-lineno) AND
        (((LASTKEY = 13 AND FRAME-FIELD = "v-prodcost") OR LASTKEY = 401)
        AND FRAME-FIELD NE "v-lineno")) THEN DO:

        RUN FindNextFrame (INPUT LASTKEY,
                           INPUT-OUTPUT v-CurrentKeyPlace,
                           INPUT-OUTPUT v-framestate).
        CLOSE QUERY q-lines.
        ON cursor-up back-tab.
        ON cursor-down tab.
        ASSIGN v-linebrowseopen = FALSE.
        IF avail t-lines THEN
          HIDE FRAME f-lines.
        IF (KEYFUNCTION(LASTKEY) = "end-error" OR
            KEYLABEL(LASTKEY) = "f11")         OR
           ((INPUT v-prod = "" AND 
           /* INPUT v-descrip = "" AND */
           NOT p-brwsfl AND NOT          
           CAN-FIND(FIRST e-t-lines WHERE e-t-lines.lineno = INPUT v-lineno)
           AND LASTKEY = 401))
        THEN
        IF fldpos1 LE 1 AND promptpos LE 1 THEN
         RUN vaexfinuse.p(INPUT x-quoteno,
                          INPUT g-operinits,
                          INPUT "c",
                          INPUT-OUTPUT v-inuse).
/*  leaving middle1 going to main on the f4 lastkey - force control
    then top1 procedure from here via v-framestate =  t1   */
        ASSIGN
          promptpos = 0
/*          v-framestate = "t1" */
          v-tagprt     = YES
          v-found      = NO.
        IF KEYLABEL(LASTKEY) NE "f11" THEN
          READKEY PAUSE 0.
        HIDE FRAME f-mid1.
        HIDE FRAME f-bot2.
        HIDE FRAME f-prodlu.
        HIDE FRAME f-whses.
        RETURN.
      END.

      IF FRAME-FIELD <> "v-lineno" AND
        (LASTKEY = 13 OR KEYFUNCTION(LASTKEY) = "go") THEN DO:
        ASSIGN
/*        v-descrip  = INPUT v-descrip 
          n-descrip  = INPUT v-descrip */
          v-qty      = INPUT v-qty
          v-prodcost = INPUT v-prodcost.
      END.
      /*** 503 right arrow 504 left arrow not included in movement ***/
      IF FRAME-FIELD = "v-lineno" THEN DO:
        IF (NOT CAN-DO("4,13,501,502,507,508",STRING(LASTKEY)) AND
            KEYFUNCTION(LASTKEY) NE "go" AND
            KEYFUNCTION(LASTKEY) NE "end-error") THEN DO:
          APPLY LASTKEY.
          /** up and down arrow(browse) and a key stroke is applyed here -
          no affect to the item entry information line  **/
          IF KEYLABEL(LASTKEY) = "m" AND FRAME-NAME = "f-mid1" THEN DO:
            IF avail t-lines THEN DO:
              IF t-lines.faxfl = "*" THEN
                ASSIGN t-lines.faxfl = " ".
              ELSE
              IF t-lines.specnstype = "l" THEN DO:
                assign
                  v-error = 
                    "Lost business line --> not markable for quote sheet.".
                RUN zsdi_vaerrx.p(v-error,"yes").
              END.
              ELSE
              ASSIGN 
                t-lines.faxfl = "*".
              b-lines:REFRESH().
            END.
            NEXT.
          END.
          IF KEYLABEL(LASTKEY) = "CTRL-A" AND FRAME-NAME = "f-mid1" THEN DO:
            IF avail t-lines AND t-lines.prod = " " THEN DO:
              DEF BUFFER zxt2-sasos FOR sasos.
              FIND FIRST zxt2-sasos WHERE
                         zxt2-sasos.cono  = g-cono AND
                         zxt2-sasos.oper2 = g-operinits AND
                         zxt2-sasos.menuproc = "zxt2"
              NO-LOCK NO-ERROR.
              IF NOT avail zxt2-sasos  OR
                (avail zxt2-sasos AND zxt2-sasos.securcd[5] < 3) THEN DO:
                ASSIGN v-error =
                  "--> Invalid security - Please contact supervisor".
                RUN zsdi_vaerrx.p(v-error,"yes").
                NEXT.
              END.
              IF t-lines.gdesc NE " " THEN DO:
                RUN add-descrip(INPUT t-lines.gdesc).
                b-lines:REFRESH().
                ASSIGN v-error =
"--> New MFG/Unit Description added to list --> " + t-lines.gdesc.
                RUN zsdi_vaerrx.p(v-error,"yes").
              END.
              NEXT.
            END.
            ELSE
            IF avail t-lines AND t-lines.prod NE " " THEN DO:
              ASSIGN v-error =
"  New MFG/Unit Description not added to list - product already assigned.".
              RUN zsdi_vaerrx.p(v-error,"yes").
              NEXT.
            END.
          END.
          IF FRAME-FIELD <> "v-lineno" AND
            (INPUT v-lineno NE v-lineno) THEN DO:
            FIND t-lines WHERE t-lines.lineno = INPUT v-lineno
            EXCLUSIVE-LOCK NO-ERROR.
            IF avail t-lines AND v-linebrowseopen THEN DO:
              ASSIGN 
                v-row = b-lines:FOCUSED-ROW
                v-rowid = ROWID(t-lines).
              REPOSITION q-lines TO ROWID(v-rowid).
              b-lines:SET-REPOSITIONED-ROW(v-row).
              IF t-lines.prod = "" THEN
                DISPLAY fs-2 WITH FRAME f-statusline2.
              ELSE
                DISPLAY fs-3 @ fs-2 WITH FRAME f-statusline2.
            END.
            IF NOT avail t-lines THEN DO:
              ASSIGN 
                v-linebrowsekeyed = FALSE
                v-error = "Invalid LineNo -> "
                  + STRING(INPUT v-lineno,"zz9") +
                  " -- Please enter an available LineNo.".
              RUN zsdi_vaerrx.p(v-error,"yes").
              NEXT-PROMPT v-lineno WITH FRAME f-mid1.
              NEXT.
            END.
            IF avail t-lines THEN
              ASSIGN v-linebrowsekeyed = FALSE.
            IF avail t-lines AND t-lines.specnstype = "l" THEN DO:
              v-error = "Lost Business line --> "
                + STRING(INPUT v-lineno,"zz9") +
                " -- Please enter an updateable LineNo.".
              RUN zsdi_vaerrx.p(v-error,"yes").
              NEXT-PROMPT v-lineno WITH FRAME f-mid1.
              NEXT.
            END.
          END.
          ELSE DO:
            IF FRAME-FIELD <> "v-lineno" THEN
              NEXT.
            IF v-linebrowseopen = FALSE THEN
              RUN DolineLu.
            ELSE DO:
              ENABLE b-lines WITH FRAME f-lines.
              APPLY "focus" TO v-lineno IN FRAME f-mid1.
            END.
            ASSIGN v-linebrowsekeyed = TRUE.
            NEXT.
          END.
        END.
        ELSE
        IF KEYLABEL(LASTKEY) = "Ctrl-D" THEN
        DO:
          DEFINE VAR d-lineno AS CHAR FORMAT "x(4)" DCOLOR 2 NO-UNDO.
          ASSIGN 
            d-lineno = IF avail t-lines THEN
                         STRING(t-lines.lineno,"zzz9")
                       ELSE
                         ""
            d-lineno = LEFT-TRIM(d-lineno,"").
/* 
          MESSAGE " Move LineNo-> " + d-lineno + " to lost business ? "
*/
          MESSAGE " Delete LineNo-> " + d-lineno + " ? "
 
            SKIP
            " Press (F1)-apply update."
            SKIP
          VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO
          UPDATE r AS LOGICAL.
          IF r THEN DO:
            DEF VAR b-row AS ROWID.
            ENABLE b-lines WITH FRAME f-lines.
            APPLY "focus" TO b-lines IN FRAME f-lines.
            FIND FIRST vasp WHERE
                       vasp.cono = g-cono         AND
                       vasp.shipprod = x-quoteno 
            NO-ERROR.
   
            FIND FIRST zsdivasp WHERE
                       zsdivasp.cono = g-cono         AND
                       zsdivasp.rectype = "fb"        AND
                       zsdivasp.repairno = x-quoteno 
            NO-ERROR.
   
             
            
            FIND CURRENT t-lines NO-ERROR.
            IF avail t-lines THEN DO:
              RUN ItemDelete(INPUT t-lines.vaspslid,
                             input t-lines.lineno,
                             input t-lines.qty,
                             input t-lines.prod,
                             input "d").
              RELEASE vasp.
              IF avail t-lines THEN
                delete t-lines.
/*
                ASSIGN 
                    t-lines.specnstype = "l"
                  t-lines.prodcost   = 0
                  t-lines.extcost    = 0
                  t-lines.gain       = 0.
*/

              ASSIGN 
/*                v-specnstype = "l"                */
                v-specnstype = ""              
                v-nslistprc  = 0
                v-prodcost   = 0
                v-prod       = ""
                v-astr2      = ""
                v-descrip    = ""
                v-descrip2   = ""
                v-attrfl     = d-attrfl
                v-vendno     = 0
                v-vendnm     = ""
                v-leadtm     = "".
             v-leadtm:dcolor = 0.
             DISPLAY 
                v-prod
                v-attrfl
                v-specnstype
                "" @ v-whse
                "" @ v-astr1
                v-astr2
                v-prodcost
                v-descrip
/*                v-descrip2        */
                v-leadtm
                v-vendno
                v-vendnm
              WITH FRAME f-mid1.
            
              find first bl-t-lines use-index ix2-lead where
                         bl-t-lines.specnstype <> "l" and
                         bl-t-lines.lineno <> v-lineno no-lock no-error.
              if avail bl-t-lines and avail zsdivasp then do:
                assign zsdivasp.leadtm1 = STRING(int(bl-t-lines.leadtm),"9999").
              end.  
              else
              if avail zsdivasp then do:
                assign zsdivasp.leadtm1 = STRING(0,"9999").
              end.  
 
             
            END.
            FIND FIRST t-lines NO-LOCK NO-ERROR.
            IF avail t-lines THEN DO:
              ENABLE b-lines WITH FRAME f-lines.
                     b-lines:REFRESHABLE = FALSE.
                     b-lines:REFRESH().
                     b-lines:REFRESHABLE = TRUE.
              DISPLAY b-lines WITH FRAME f-lines.
            END.
            ELSE DO:
              HIDE FRAME f-mid1.
              HIDE FRAME f-lines.
              ASSIGN 
                v-framestate      = "t1"
                v-currentkeyplace = 1.
              NEXT midloop.
            END.
            NEXT midloop.
          END. /* if r */
          /* clear the fields the product was not the same as it was */
          ASSIGN 
            v-astr1     = ""
            v-astr2     = ""
            v-descrip   = ""
            v-descrip2  = ""
            v-whse      = ""
            v-altwhse   = ""
            v-attrfl    = d-attrfl
            v-vendno    = 0
            v-vendnm    = ""
            v-prodcat   = ""
            s-ZsdiXrefType = ""
            v-prodline  = ""
            v-prodcost  = 0
            v-nslistprc = 0
            v-leadtm    = ""
            v-prctype   = "".
          RUN DisplayMid1.
          NEXT.
        END. /* not-cando */
        /** 501-up 502-down 507-prevpage 508-nextpage **/
        ELSE IF (LASTKEY = 501 OR
                 LASTKEY = 502 OR
                 LASTKEY = 507 OR
                 LASTKEY = 508) AND v-linebrowseopen = TRUE THEN DO:
          RUN MovelineLu (INPUT LASTKEY).
          ASSIGN v-linebrowsekeyed = TRUE.
          NEXT.
        END.
        ELSE
        IF ((LASTKEY = 13 OR KEYFUNCTION(LASTKEY) = "go" ) AND
            (v-linebrowsekeyed OR v-lineno NE INPUT v-lineno)) THEN DO:
          IF ((LASTKEY = 13 OR KEYFUNCTION(LASTKEY) = "go" ) AND
              (INPUT v-lineno NE v-lineno)) THEN
          FIND t-lines WHERE t-lines.lineno = INPUT v-lineno
          EXCLUSIVE-LOCK NO-ERROR.
          IF avail t-lines THEN
          ASSIGN v-linebrowsekeyed = FALSE.
          ELSE DO:
            FIND LAST t-lines NO-LOCK NO-ERROR.
            IF avail t-lines AND INPUT v-lineno >
              (t-lines.lineno + 1) THEN DO:
              ASSIGN 
                v-linebrowsekeyed = FALSE
                v-error = "Invalid LineNo -> "
                  + STRING(INPUT v-lineno,"zz9") +
                  " -- Please enter an available LineNo.".
              RUN zsdi_vaerrx.p(v-error,"yes").
              NEXT-PROMPT v-lineno WITH FRAME f-mid1.
              NEXT.
            END.
            ELSE
            IF avail t-lines AND INPUT v-lineno = (t-lines.lineno + 1) THEN DO:
              ASSIGN
                v-linebrowsekeyed = FALSE
                v-error = "Repositioned to last LineNo -> "
                  + STRING((INPUT v-lineno - 1),"zz9") +
                  " -- Press F1 to create a new line item.".
              RUN zsdi_vaerrx.p(v-error,"yes").
            END.
          END.
        END.
        ELSE
        ASSIGN v-linebrowsekeyed = FALSE.
        /** moving into v-prod with new lineno or existing line **/
        IF avail t-lines THEN DO:
          IF t-lines.specnstype = "l" THEN DO:
            ASSIGN
              v-error = "Lost Business line --> "
                + STRING(t-lines.lineno,"zz9") +
                " -- Please enter an updateable LineNo.".
            RUN zsdi_vaerrx.p(v-error,"yes").
            FIND t-lines WHERE t-lines.lineno = v-lineno
            EXCLUSIVE-LOCK NO-ERROR.
            NEXT-PROMPT v-lineno WITH FRAME f-mid1.
            NEXT.
          END.
          IF avail t-lines AND v-linebrowseopen THEN DO:
            ASSIGN 
              v-row = b-lines:FOCUSED-ROW
              v-rowid = ROWID(t-lines).
            REPOSITION q-lines TO ROWID(v-rowid).
            b-lines:SET-REPOSITIONED-ROW(v-row).
          END.
          ASSIGN 
            z-descrip    = t-lines.gdesc
            v-lmode      = "c"
            v-seqno      = t-lines.seqno
            v-lineno     = t-lines.lineno
            v-prod       = t-lines.prod
            d-prod       = t-lines.prod
            v-prodline   = t-lines.prodline
            s-ZsdiXrefType = t-lines.ZsdiXrefType
            v-attrfl     = t-lines.attrfl
            v-prodcat    = t-lines.prodcat
            v-vendno     = t-lines.vendno
            v-vendnm     = t-lines.vendnm
            v-prctype    = t-lines.prctype
            v-whse       = IF t-lines.altwhse NE "" AND
                           t-lines.altwhse NE x-whse THEN
                             t-lines.altwhse
                           ELSE
                             t-lines.whse
            v-nslistprc  = if t-lines.specnstype = "n" then
                             t-lines.listprc
                           else
                             0
            v-prodcost   = t-lines.prodcost
            v-descrip    = t-lines.descrip
            v-qty        = t-lines.qty
            v-specnstype = t-lines.specnstype
            v-matrixed   = IF t-lines.qtybrkty <> "" THEN
                             t-lines.qtybrkty
                           ELSE
                             ""
            v-leadtm     = LEFT-TRIM(t-lines.leadtm,"0")
            v-astr1      = IF v-whse NE x-whse THEN
                             "*"
                           ELSE
                             ""
            v-astr2      = t-lines.astr1
            o-nslistprc  = v-nslistprc
            o-leadtm     = t-lines.leadtm
            o-descrip    = t-lines.icspdesc 
            o-lineno     = t-lines.lineno
            o-seqno      = t-lines.seqno
            o-prod       = t-lines.prod
            o-prctype    = t-lines.prctype
            o-attrfl     = t-lines.attrfl
            o-prodcat    = t-lines.prodcat
            o-vendno     = t-lines.vendno
            o-whse       = v-whse
            o-prodcost   = t-lines.prodcost
            o-qty        = t-lines.qty
            o-specnstype = t-lines.specnstype.
        END.
        /** if in v-lineno and go from v-lineno with new lineno or up or down
        in browse and enter is pressed - re-initiate the lookup here
        re-display of work area if t-lines is available  **/
        IF v-prod NE " " THEN DO:
          RUN DoProdLu (INPUT v-prod,
                        INPUT "normal").
        END.
        ELSE
        ASSIGN 
          v-vendnm  = ""
          v-astr2   = "".
        IF v-seqno = 0 THEN DO:
          RUN DisplayMid1.
          NEXT-PROMPT v-prod WITH FRAME f-mid1.
          NEXT.
        END.
      END.  /* frame-field = v-lineno */
      
 /** leadtime display in the item update will display if qty chg or
      ctrl-w different whse is chosen  **/

      IF ({k-sdileave.i &fieldname = "v-qty"
                      &completename = "v-qty"}) and
        (o-qty NE v-qty)  THEN DO:
        run Create-Vaspmap (input if (avail t-lines and input v-prod = o-prod)
                                     or (input v-prod = o-prod ) then
                                    "updt"
                                  else
                                  if avail t-lines and 
                                     input v-prod <> o-prod then
                                    "repl"
                                  else
                                    "new",
                            input v-lineno,
                            input v-prod,
                            input x-whse,
                            input v-qty,
                            input v-nslistprc,
                            input v-specnstype).
      END.
      ELSE
      IF avail t-lines AND (LASTKEY = 13 OR LASTKEY = 9) AND
        (t-lines.qty NE v-qty)  THEN DO:
        /* if line is maintaind the icsw leadtm could have changed, so check it
        again here to determine the live leadtm for item below
        */
        
        DEFINE BUFFER lt-icsw FOR icsw.
        ASSIGN 
          v-whse = IF ctrlw-whse = "" AND v-whse = x-whse THEN
                     x-whse
                   ELSE
                   IF v-whse NE "" AND v-whse NE x-whse THEN
                     v-whse
                   ELSE
                   IF t-lines.altwhse NE "" AND
                     t-lines.altwhse NE x-whse THEN
                     t-lines.altwhse
                   ELSE
                     t-lines.whse.
        IF (v-whse NE "")  OR
           (t-lines.qty NE v-qty) THEN DO:
/* tahtrain
           {w-icsw.i v-prod v-whse NO-LOCK "lt-"}
tahtrain          replaced with below */
          {w-icsw.i v-prod x-whse NO-LOCK "lt-"}
          IF avail lt-icsw THEN 
            ASSIGN
              v-leadtm = STRING(INT(lt-icsw.leadtmavg),"9999").
        END.
      
        run Create-Vaspmap (input if ( avail t-lines and input v-prod = o-prod) 
                                      or (o-prod = input v-prod) then
                                     "updt"
                                  else
                                  if avail t-lines and 
                                     input v-prod <> o-prod then
                                     "repl"
                                  else
                                     "new",
                            input v-lineno,
                            input v-prod,
                            input x-whse,
                            input v-qty,
                            input v-nslistprc,
                            input v-specnstype).
 
      
      
      
      END.

      IF FRAME-FIELD = "v-prod" AND INPUT v-prod NE v-prod THEN DO:
        /* clear the fields the product was not the same as it was */
        ASSIGN 
          v-descrip   = ""
          v-astr1     = ""
          v-specnstype = ""
          v-astr2     = ""
          v-whse      = ""
          v-attrfl    = d-attrfl
          v-altwhse   = ""
          v-vendno    = 0
          v-vendnm    = ""
          v-prodcat   = ""
          v-prodcost  = 0
          v-nslistprc = 0
          v-leadtm    = ""
          v-prctype   = "".
        DISPLAY v-specnstype v-astr2 WITH FRAME f-mid1.
        IF avail t-lines THEN
          ASSIGN 
            t-lines.altwhse = ""
            t-lines.whse = "".
      END.
      IF FRAME-FIELD = "v-prod" THEN DO:
        IF (NOT CAN-DO("9,21,13,501,502,507,508",STRING(LASTKEY)) AND
            KEYFUNCTION(LASTKEY) NE "go" AND
            KEYFUNCTION(LASTKEY) NE "end-error" AND
            KEYLABEL(LASTKEY) NE "Ctrl-W") THEN DO:
          APPLY LASTKEY.
          IF FRAME-FIELD <> "v-prod" THEN DO:
            IF INPUT v-prod <> o-prod THEN DO:
/*          
              
              if avail t-lines THEN DO:
                assign v-error = 
                "Product cannot be changed, can only be put to Lost business".
                RUN zsdi_vaerrx.p(v-error,"yes").
                assign v-prod = o-prod.
                display v-prod with frame f-mid1.
                v-prod:cursor-offset = 1.
                next-prompt v-prod with frame f-mid1.   
                next.
              end.

*/            
            if avail t-lines then
              RUN ItemDelete(INPUT t-lines.vaspslid,
                             input t-lines.lineno,
                             input t-lines.qty,
                             input t-lines.prod,
                             input "c").
  
            run Create-Vaspmap (input "New",
                                input v-lineno,
                                input v-prod,
                                input x-whse,
                                input v-qty,
                                input v-nslistprc,
                                input v-specnstype).
 
              ASSIGN 
                v-prod   = INPUT v-prod
                v-qty    = INPUT v-qty.
              RUN Check_product_ID(INPUT-OUTPUT v-prod).
              /* Customer Part numbers and such */
             
              FIND icsp WHERE icsp.cono = g-cono AND
                   icsp.prod = v-prod NO-LOCK NO-ERROR.
      

              find first zsdirefty where
                         zsdirefty.cono      = g-cono and
                         zsdirefty.rectype   = "va"   and
                         zsdirefty.reference = v-model no-lock no-error.
/* Performance Key Needed */
              if avail zsdirefty then do:                     
                find first zsdirefer use-index k-prod where 
                           zsdirefer.cono        = g-cono     and                                         zsdirefer.rectype     = "va"       and
                           zsdirefer.typekeyno   = zsdirefty.keyno and
                           zsdirefer.prod        = v-prod no-lock no-error.
              
                if avail zsdirefer then
                  assign v-attrfl = no.
              end.
              
              IF avail icsp THEN DO:
                {w-icsw.i v-prod x-whse NO-LOCK}
                IF NOT avail icsp THEN
                  ASSIGN v-specnstype = "n".
                ELSE
                IF avail icsw AND icsw.statustype = "O" THEN
                  ASSIGN v-specnstype = "s".
                ELSE
                IF not avail icsw  THEN
                  ASSIGN v-specnstype = "s".
                ELSE
                   ASSIGN v-specnstype = " ".


              
              
              
              
                RUN NoteDisplay3(INPUT icsp.prod).
                ASSIGN
                  v-Prod     = icsp.prod
                  v-astr2    = v-astr3
                  v-prodcat  = icsp.prodcat
                  v-descrip = icsp.descrip[1].
                DISPLAY 
                  v-specnstype
                  v-prod
                  v-attrfl
                  v-astr2
                  v-descrip
                WITH FRAME f-mid1.
                RUN ProdPricing(  INPUT TRUE,
                                  INPUT zi-PDSCfl,
                                  INPUT zi-PDSCrec,
                                  INPUT x-whse,  /* tahtrain was v-whse */
                                  INPUT v-qty,
                                  INPUT v-custno,
                                  INPUT v-prod,
                                  INPUT-OUTPUT zi-price,
                                  INPUT-OUTPUT zi-gpmarg,
                                  INPUT-OUTPUT zi-level,
                                  INPUT-OUTPUT zi-rebate,
                                  INPUT-OUTPUT zi-cost,
                                  INPUT-OUTPUT zi-listprice,
                                  INPUT-OUTPUT zi-disc,
                                  INPUT-OUTPUT zi-pdscrecno,
                                  INPUT-OUTPUT zi-qtybrkty).
                ASSIGN v-prodcost = zi-cost.
                DISPLAY v-prodcost WITH FRAME f-mid1.
              run Create-Vaspmap (input "new",
                                  input v-lineno,
                                  input v-prod,
                                  input x-whse,
                                  input v-qty,
                                  input v-nslistprc,
                                  input v-specnstype).
              END. /* avail icsp */
              ELSE
              DO:
                /* clear the fields the product was not the same as it was */
                ASSIGN 
                  v-descrip   = if avail t-lines then 
                                  " "
                                else
                                  " "
                  v-astr1     = ""
                  v-specnstype = " "
                  v-astr2     = ""
                  v-attrfl    = d-attrfl
                  v-whse      = ""
                  v-altwhse   = ""
                  v-vendno    = 0
                  v-prodline  =  ""
                  v-vendnm    = ""
                  v-prodcat   = ""
                  v-prodcost  = 0
                  v-nslistprc = 0
                  v-labormins = 0
                  v-leadtm    = ""
                  v-prctype   = "".
                assign v-attrfl = false.
                RUN Nonstock_Popup.
                v-leadtm:dcolor = 0.
                DISPLAY 
                  v-specnstype
                  v-prod
                  v-attrfl
                  v-astr2
                  v-descrip
                  v-leadtm
                WITH FRAME f-mid1.
                
                IF {k-cancel.i} THEN
                DO:
                  assign v-attrfl = if avail t-lines then
                                      t-lines.attrfl
                                    else
                                      true.
                  NEXT-PROMPT v-prod WITH FRAME f-mid1.
                  RUN DoProdlu(INPUT v-prod, INPUT "normal").
                  NEXT.
                END.
                ASSIGN 
                  v-Prod       = INPUT v-prod
                  v-qty        = INPUT v-qty
                  v-specnstype = "n"
                  v-attrfl     = no
                  v-astr1      = IF v-whse NE x-whse AND
                                 v-whse NE "" THEN
                                   "*"
                                 ELSE
                                   "".
                DISPLAY
                  v-specnstype
                  v-prod
                  v-attrfl
                  v-astr2
                  v-qty
                  v-descrip
                  (IF v-whse NE x-whse THEN
                     v-whse
                   ELSE
                     "") @ v-whse
                  v-prodcost
                  LEFT-TRIM(v-leadtm,"0") @ v-leadtm
                  v-vendno
                  v-vendnm
                  (IF v-whse NE x-whse AND
                     v-whse NE "" THEN
                     v-astr1
                  ELSE
                     "") @  v-astr1
                WITH FRAME f-mid1.
                ASSIGN o-prod = v-prod.
                run Create-Vaspmap (input "new",
                                    input v-lineno,
                                    input v-prod,
                                    input x-whse,
                                    input v-qty,
                                    input v-nslistprc,
                                    input v-specnstype).
                                    
                                    /* changed prompt */
 
                NEXT-PROMPT v-attrfl WITH FRAME f-mid1.
                NEXT.
              END. /* do for else */
              ASSIGN o-prod = v-prod.
            END. /* input v-prod ne o-prod */
            RUN NoteDisplay.
            NEXT.
          END. /* frame field ne v-prod */
          ASSIGN v-prod = INPUT v-prod.
          /** Product lookup by keystroke logic **/
          STATUS DEFAULT
          "                                                         ".
          IF NOT funcflag THEN
            RUN DoProdLu (INPUT v-prod,
                          INPUT "normal").
          ELSE
            ASSIGN funcflag = NO.
          NEXT.
        END. /** not can do **/
        ELSE
        IF KEYLABEL(LASTKEY) = "Ctrl-W" AND v-prodbrowseopen = TRUE AND
           NOT (avail t-lines AND t-lines.specnstype = "lost") THEN DO:
          ASSIGN
            ctrlw-whse  = ""
            v-qty       = INPUT v-qty
            v-prodcost  = INPUT v-prodcost
            v-lineno    = INPUT v-lineno.
          RUN Whse_Selection.
          ASSIGN 
            v-whse    = IF ctrlw-whse NE "" AND
                        ctrlw-whse NE x-whse THEN
                          ctrlw-whse
                        ELSE
                          v-whse
            v-astr1   = IF v-whse NE x-whse AND
                        v-whse NE "" THEN
                          "*"
                        ELSE
                          "".
          IF v-applygo = FALSE THEN
            NEXT.
          /* v-prodcost reflects new whse avgcost from whse_selection */
          DISPLAY 
            LEFT-TRIM(v-leadtm,"0") @ v-leadtm
            v-prodcost
            v-whse
            v-qty
            (IF v-whse NE x-whse AND
             v-whse NE "" THEN
               v-astr1
             ELSE
              "") @ v-astr1
          WITH FRAME f-mid1.
        END.
        ELSE
      /** only up arrow movwment (501) lastkey **/
        IF (((KEYLABEL(LASTKEY) = "cursor-left" OR
            LASTKEY = 501)  AND
            v-prod:CURSOR-OFFSET = 1) OR
           (v-prod:CURSOR-OFFSET = v-prod:WIDTH-CHARS AND
           KEYLABEL(LASTKEY) = "CURSOR-RIGHT")) or
          (KEYLABEL(LASTKEY) = "CTRL-U" OR
           KEYLABEL(LASTKEY) = "TAB") AND
           v-prodbrowsekeyed = FALSE  THEN DO:
          ASSIGN v-prodbrowsekeyed = FALSE.
          HIDE FRAME f-prodlu.
          HIDE FRAME f-whses.
          RUN NoteDisplay.
          IF KEYLABEL(LASTKEY) = "CURSOR-RIGHT" or
             KEYLABEL(LASTKEY) = "TAB" THEN DO:
            NEXT-PROMPT v-attrfl WITH FRAME f-mid1.
            APPLY "entry" TO v-attrfl IN FRAME f-mid1.
            IF v-prod = o-prod THEN
              NEXT.
            ELSE
              v-applygo = TRUE.
          END.
          ELSE DO:
            NEXT-PROMPT v-lineno WITH FRAME f-mid1.
            APPLY "entry" TO v-lineno IN FRAME f-mid1.
            IF v-prod = o-prod THEN
              NEXT.
            ELSE
              v-applygo = TRUE.
          END.
        END.
/*
        ELSE
        IF (KEYLABEL(LASTKEY) = "CTRL-U" OR
            KEYLABEL(LASTKEY) = "TAB") AND
            FRAME-FIELD = "v-prod" THEN DO:
          /**^^^^^^^^^^^**/
          pause.
          RUN NoteDisplay.
        END.
*/
        ELSE
        IF (LASTKEY = 501 OR
            LASTKEY = 502 OR
            LASTKEY = 507 OR
            LASTKEY = 508) AND v-prodbrowseopen = TRUE  THEN DO:
          ON cursor-up cursor-up.
          ON cursor-down cursor-down.
          RUN MoveProdLu (INPUT LASTKEY).
          ASSIGN v-prodbrowsekeyed = TRUE.
          NEXT.
        END.
        /* not an else if because of the v-applygo */
        IF (LASTKEY = 13 OR KEYFUNCTION(LASTKEY) = "go" OR v-applygo) THEN DO:
          ON cursor-up back-tab.
          ON cursor-down tab.
/*
          IF avail t-lines AND t-lines.prod NE "" THEN DO:
            /** set up o-labormins in case the product changes **/
            FIND v-icsp WHERE v-icsp.cono = g-cono AND
                 v-icsp.prod = t-lines.prod NO-LOCK NO-ERROR.
            IF avail v-icsp THEN
              ASSIGN o-labormins = v-icsp.user7.
            ELSE
              ASSIGN o-labormins = 0.
          END.
*/          
          IF v-prodbrowsekeyed = FALSE THEN DO:
            IF v-qty = 0 AND (KEYFUNCTION(LASTKEY) = "go" OR
            (KEYFUNCTION(LASTKEY) = "return" AND FRAME-FIELD = "v-prodcost"))
            THEN DO:
              ASSIGN v-error = "Qty required field - please enter.".
              RUN zsdi_vaerrx.p(v-error,"yes").
              NEXT-PROMPT v-qty WITH FRAME f-mid1.
              NEXT.
            END.
            FIND icsp WHERE icsp.cono = g-cono AND
                 icsp.prod = INPUT v-prod NO-LOCK NO-ERROR.
            IF avail icsp THEN DO:
              ASSIGN v-descrip = icsp.descrip[1]
                     v-prodcat = icsp.prodcat.
              DISPLAY v-descrip WITH FRAME f-mid1.
            END.
            IF avail icsp AND icsp.statustype = "i" THEN DO:
              ASSIGN v-error =
                "Product has Inactive Status.  See Buyer to Activate".
              RUN zsdi_vaerrx.p(v-error,"yes").
              NEXT-PROMPT v-qty WITH FRAME f-mid1.
              NEXT.
            END.
            {w-icsw.i v-prod x-whse NO-LOCK}
/* tahtrain added product <> "" check */
            if not avail icsp and INPUT v-prod = "" and 
               lastkey = 13 then DO:
              ASSIGN v-prod = g-prod.
              DISPLAY v-prod WITH FRAME f-mid1.
              NEXT-PROMPT v-qty WITH FRAME f-mid1.
              NEXT.
            END.
            ELSE 
            IF NOT avail icsp and INPUT v-prod <> "" THEN
              ASSIGN v-specnstype = "n".
            ELSE
            IF avail icsw AND icsw.statustype = "O" THEN
              ASSIGN v-specnstype = "s".
            ELSE
            IF not avail icsw  THEN
              ASSIGN v-specnstype = "s".
            ELSE
              ASSIGN v-specnstype = " ".
            /***********************************************************/
            ASSIGN 
              v-prod     = INPUT v-prod
              v-prodcat  = IF avail icsp THEN
                             icsp.prodcat
                           ELSE
                             v-prodcat.
              v-descrip = IF avail icsp THEN
                             icsp.descrip[1]
                           ELSE
                             v-descrip.
          END.
          IF NOT avail icsp THEN DO:
            RUN Check_product_ID(INPUT-OUTPUT v-prod).
            /* Customer Part numbers and such */
            FIND icsp WHERE icsp.cono = g-cono AND
                 icsp.prod = v-prod NO-LOCK NO-ERROR.
            IF avail icsp THEN DO:
              {w-icsw.i v-prod x-whse NO-LOCK}
              IF NOT avail icsp THEN
                ASSIGN v-specnstype = "n".
              ELSE
              IF avail icsw AND icsw.statustype = "O" THEN
                ASSIGN v-specnstype = "s".
              ELSE
              IF not avail icsw  THEN
                ASSIGN v-specnstype = "s".
              else
                assign v-specnstype = " ".
              
              RUN NoteDisplay3(INPUT icsp.prod).
    
              
      

              find first zsdirefty where
                         zsdirefty.cono      = g-cono and
                         zsdirefty.rectype   = "va"   and
                         zsdirefty.reference = v-model no-lock no-error.
/* Performance Key Needed */
              if avail zsdirefty then do:                     
                find first zsdirefer use-index k-prod where 
                           zsdirefer.cono        = g-cono     and                                         zsdirefer.rectype     = "va"       and
                           zsdirefer.typekeyno   = zsdirefty.keyno and
                           zsdirefer.prod        = v-prod no-lock no-error.
              
                if avail zsdirefer then
                  assign v-attrfl = no.
              end.
               
              ASSIGN 
                v-Prod       = icsp.prod
                v-Prodcat    = icsp.prodcat
                v-astr2      = v-astr3
                v-descrip    = icsp.descrip[1].
              DISPLAY
                v-specnstype
                v-prod
                v-attrfl
                v-astr2
                LEFT-TRIM(v-leadtm,"0") @ v-leadtm
                v-descrip
                v-vendno
                v-vendnm
                v-whse
                (IF v-whse NE x-whse AND
                 v-whse NE "" THEN
                   v-astr1
                 ELSE
                   "")  @ v-astr1
              WITH FRAME f-mid1.
            END.
            ELSE DO:
            IF v-prod <> o-prod THEN do:
/*
              if avail t-lines THEN DO:
                assign v-error = 
                "Product cannot be changed, can only be put to Lost business".
                RUN zsdi_vaerrx.p(v-error,"yes").
                assign v-prod = o-prod.
                display v-prod with frame f-mid1.
                v-prod:cursor-offset = 1.
               next-prompt v-prod with frame f-mid1.   
               next.
             end.
*/
            if avail t-lines then 
            RUN ItemDelete(INPUT t-lines.vaspslid,
                           input t-lines.lineno,
                           input t-lines.qty,
                           input t-lines.prod,
                           input "c").
  
            run Create-Vaspmap (input "New",
                                input v-lineno,
                                input v-prod,
                                input x-whse,
                                input v-qty,
                                input v-nslistprc,
                                input v-specnstype).
 
               /* must have a new part number or blank product - or a descrip
              with no part number  */
                ASSIGN
                  v-gp        = 0
                  v-specnstype  = ""
                  v-descrip   = ""
                  v-astr1     = ""
                  v-astr2     = ""
                  v-attrfl    = d-attrfl
                  v-whse      = ""
                  v-prodline  = "" 
                  v-vendno    = 0
                  v-vendnm    = ""
                  v-prodcat   = ""
                  v-prctype   = ""
                  v-pricetype = ""
                  v-prodcost  = 0
                  v-nslistprc = 0
                  v-costgain  = 0
                  v-labormins = 0
                  v-leadtm    = "".
              end.
              assign v-attrfl = false.
              RUN Nonstock_Popup.
              IF {k-cancel.i} THEN DO:
                assign v-attrfl = if avail t-lines then
                                    t-lines.attrfl
                                  else
                                    true.
                NEXT-PROMPT v-prod WITH FRAME f-mid1.
                RUN doprodlu(INPUT v-prod, INPUT "normal").
                NEXT.
              END.
              ASSIGN 
                v-Prod = INPUT v-prod
                v-qty  = INPUT v-qty
                v-specnstype = "n"
                v-astr1      = IF v-whse NE x-whse AND
                               v-whse NE "" THEN
                                 "*"
                               ELSE
                                 "".
              RUN DisplayMid1.
              run Create-Vaspmap (input "new",
                                  input v-lineno,
                                  input v-prod,
                                  input x-whse,
                                  input v-qty,
                                  input v-nslistprc,
                                  input v-specnstype).
                                  
                                  
 
              ON cursor-up back-tab.
              ON cursor-down tab.
              NEXT-PROMPT v-attrfl WITH FRAME f-mid1.
              NEXT.
            END. /* else do if  v-prod ne o-prod */
          END. /* if not avail icsp */
          ELSE DO:
            /** enter is pressed from v-prod - still in edit loop ----
            it can be new line or existing line - with an avail icsp **/
            FIND t-lines WHERE t-lines.lineno = INPUT v-lineno
            EXCLUSIVE-LOCK NO-ERROR.
            ASSIGN 
              v-Prod     = icsp.prod
              v-Prodcat  = icsp.prodcat
              v-descrip  = icsp.descrip[1].
            IF avail t-lines AND t-lines.gdesc NE "" AND
                     t-lines.prod NE v-prod THEN DO:
              ASSIGN 
                t-lines.gdesc = ""
                z-descrip = ""
                f-descrip = "".
            END.
          END.
          IF v-prod <> o-prod AND avail icsp THEN DO:
/*
            if avail t-lines THEN DO:
              assign v-error = 
                 "Product cannot be changed, can only be put to Lost business".
              RUN zsdi_vaerrx.p(v-error,"yes").
             assign v-prod = o-prod.
             display v-prod with frame f-mid1.
             v-prod:cursor-offset = 1.
             next-prompt v-prod with frame f-mid1.   
             next.
           end.

*/
            if avail t-lines then
            RUN ItemDelete(INPUT t-lines.vaspslid,
                           input t-lines.lineno,
                           input t-lines.qty,
                           input t-lines.prod,
                           input "c").
  
            run Create-Vaspmap (input "New",
                                input v-lineno,
                                input v-prod,
                                input x-whse,
                                input v-qty,
                                input v-nslistprc,
                                input v-specnstype).
 
            RUN ProdPricing (INPUT FALSE,
                             INPUT zi-PDSCfl,
                             INPUT zi-PDSCrec,
                             INPUT x-whse,   /* tahtrain was v-whse */
                             INPUT v-qty,
                             INPUT v-custno,
                             INPUT v-prod,
                             INPUT-OUTPUT zi-price,
                             INPUT-OUTPUT zi-gpmarg,
                             INPUT-OUTPUT zi-level,
                             INPUT-OUTPUT zi-rebate,
                             INPUT-OUTPUT zi-cost,
                             INPUT-OUTPUT zi-listprice,
                             INPUT-OUTPUT zi-disc,
                             INPUT-OUTPUT zi-pdscrecno,
                             INPUT-OUTPUT zi-qtybrkty).
            /** product has changed here - we need to use the new prodcost **/
            ASSIGN 
              v-prodcost = zi-cost.
         
            run Create-Vaspmap (input if (avail t-lines and v-prod = o-prod)
                                         or (v-prod = o-prod) then
                                         "updt"
                                      else
                                      if avail t-lines and v-prod <> o-prod then
                                         "repl"
                                      else
                                         "new",
                                input v-lineno,
                                input v-prod,
                                input x-whse,
                                input v-qty,
                                input v-nslistprc,
                                input v-specnstype).
            assign      o-prod   = v-prod
                        v-prodline = "".
                              
               
          END. /* v-prod ne o-prod and avail icsp */

      

          find first zsdirefty where
                     zsdirefty.cono      = g-cono and
                     zsdirefty.rectype   = "va"   and
                     zsdirefty.reference = v-model no-lock no-error.
/* Performance Key Needed */
          if avail zsdirefty then do:                     
            find first zsdirefer use-index k-prod where 
                       zsdirefer.cono        = g-cono     and                                           zsdirefer.rectype     = "va"       and
                       zsdirefer.typekeyno   = zsdirefty.keyno and
                       zsdirefer.prod        = v-prod no-lock no-error.
              
            if avail zsdirefer  then
              assign v-attrfl = no.
          end.
           
          
          
          RUN DisplayMid1.
          ASSIGN v-prodbrowsekeyed = FALSE.
          HIDE FRAME f-prodlu.
          HIDE FRAME f-whses.
        END.
        ELSE
          ASSIGN v-prodbrowsekeyed = FALSE.
      END. /* if frame-field = v-prod */
      
      /** popup required notes auto and if not required identify with '*' **/
      IF (KEYFUNCTION(LASTKEY) = "go" OR
          KEYFUNCTION(LASTKEY) = "return") AND FRAME-FIELD = "v-prod" THEN DO:
        RUN NoteDisplay.
      END.
      
      /** don't allow blank descrip and prod **/
      IF (KEYFUNCTION(LASTKEY) = "go" OR
         (KEYFUNCTION(LASTKEY) = "return" AND FRAME-FIELD = "v-prodcost")) AND
          INPUT v-prod = "" AND INPUT v-descrip = "" THEN DO:
        ASSIGN
          v-error = "Product required field - please enter. ".
        RUN zsdi_vaerrx.p(v-error,"yes").
        ON cursor-up cursor-up.
        ON cursor-down cursor-down.
        NEXT-PROMPT v-prod WITH FRAME f-mid1.
        NEXT.
      END.
      
      APPLY LASTKEY.
      
    END. /* end of edit loop --- last key 13 or go or v-applygo */
    /************************************************************/
    
    IF v-prod NE "" THEN DO:
      ASSIGN v-error = "".
      FIND icsp WHERE icsp.cono = g-cono AND
           icsp.prod = INPUT v-prod NO-LOCK NO-ERROR.
      FIND FIRST icsw WHERE
                 icsw.cono = 90     AND
                 icsw.prod = v-prod AND
                 icsw.whse = "main" 
      NO-LOCK NO-ERROR.
      IF avail icsp AND NOT avail icsw THEN DO:
        FIND FIRST icsw WHERE
                   icsw.cono = g-cono AND
                   icsw.prod = v-prod AND
                   icsw.whse = x-whse
        NO-LOCK NO-ERROR.
        IF NOT avail icsw THEN DO:
          /** copy from a whse - if not at default whse  **/
          ASSIGN whse-okfl = NO.
          RUN Create_ICSW(INPUT x-whse,INPUT v-prod,INPUT-OUTPUT whse-okfl).
        END.
        IF NOT avail icsw AND NOT whse-okfl THEN DO:
          ASSIGN v-error = "Invalid Whse item -> " + v-prod +
            " -- Not available in any whse".
          RUN zsdi_vaerrx.p(v-error,"yes").
          ASSIGN
            v-descrip2  = ""
            v-descrip   = ""
            v-astr1     = ""
            v-astr2     = ""
            v-whse      = ""
            v-vendno    = 0
            v-vendnm    = ""
            v-attrfl    = d-attrfl
            v-prodcat   = ""
            v-prctype   = ""
            v-pricetype = ""
            v-prodcost  = 0
            v-nslistprc = 0
            v-leadtm    = "".
          RUN DisplayMid1.
          ASSIGN v-loadbrowseline = TRUE.
          NEXT-PROMPT v-prod WITH FRAME f-mid1.
          NEXT.
        END.
      END.
    END.

    IF KEYFUNCTION(LASTKEY) = "go" OR
      (FRAME-FIELD = "v-prodcost" AND   
       KEYFUNCTION(LASTKEY) = "return") THEN DO:
      IF v-qty = 0 AND
        (KEYFUNCTION(LASTKEY) = "go" OR
        (KEYFUNCTION(LASTKEY) = "return" AND
         FRAME-FIELD = "v-prodcost")) THEN DO:
        ASSIGN 
          v-error = "Qty required field - please enter."
          v-loadbrowseline = TRUE.
        RUN zsdi_vaerrx.p(v-error,"yes").
        NEXT-PROMPT v-qty WITH FRAME f-mid1.
        NEXT.
      END.
      FIND icsp WHERE icsp.cono = g-cono AND
           icsp.prod = v-prod NO-LOCK NO-ERROR.
      IF avail icsp THEN DO:
        ASSIGN v-descrip = icsp.descrip[1].
        DISPLAY v-descrip WITH FRAME f-mid1.
      END.
      IF avail icsp AND icsp.statustype = "i" THEN DO:
        ASSIGN v-error = "Product has Inactive Status.  See Buyer to Activate".
        RUN zsdi_vaerrx.p(v-error,"yes").
        NEXT-PROMPT v-qty WITH FRAME f-mid1.
        NEXT.
      END.
      FIND t-lines WHERE t-lines.lineno = v-lineno AND
           t-lines.seqno  = 0 NO-LOCK NO-ERROR.
      ASSIGN v-prod = INPUT v-prod.
      /** line is committed here - error checking is done **/
      IF NOT avail t-lines AND v-lineno > 0 THEN DO:
        CREATE t-lines.
         ASSIGN 
           v-labormins = 0
           v-drawingmins = 0
           v-electricalmins = 0
           v-mechanicalmins = 0
           v-docmins     = 0.
        ASSIGN t-lines.lineno = v-lineno.
        IF avail t-lines AND f-descrip NE "" THEN DO:
          ASSIGN t-lines.gdesc = TRIM(f-descrip," ").
        END.
        /** set up o-labormins in case the product changes **/
      END.
      IF v-prod <> o-prod THEN DO:
         RUN Check_product_ID(INPUT-OUTPUT v-prod).
        /* Customer Part numbers and such */
        FIND icsp WHERE icsp.cono = g-cono AND
             icsp.prod = v-prod NO-LOCK NO-ERROR.
        IF avail icsp THEN DO:
          FIND t-lines WHERE t-lines.lineno = INPUT v-lineno
          EXCLUSIVE-LOCK NO-ERROR.
          ASSIGN 
            v-Prod     = icsp.prod
            v-descrip  = icsp.descrip[1].
          IF avail t-lines AND t-lines.gdesc NE "" AND
             t-lines.prod NE v-prod THEN DO:
            ASSIGN 
              t-lines.gdesc = ""
              z-descrip = ""
              f-descrip = "".
          END.
          DISPLAY 
            v-prod
            v-descrip
          WITH FRAME f-mid1.
          RUN ProdPricing ( INPUT TRUE,
                            INPUT zi-PDSCfl,
                            INPUT zi-PDSCrec,
                            INPUT x-whse,     /* tahtrain was v-whse */
                            INPUT v-qty,
                            INPUT v-custno,
                            INPUT v-prod,
                            INPUT-OUTPUT zi-price,
                            INPUT-OUTPUT zi-gpmarg,
                            INPUT-OUTPUT zi-level,
                            INPUT-OUTPUT zi-rebate,
                            INPUT-OUTPUT zi-cost,
                            INPUT-OUTPUT zi-listprice,
                            INPUT-OUTPUT zi-disc,
                            INPUT-OUTPUT zi-pdscrecno,
                            INPUT-OUTPUT zi-qtybrkty).
          ASSIGN 
            o-prod = v-prod
            v-prodcost = zi-cost.
        END. /* avail icsp */
        ASSIGN o-prod   = v-prod.
      END. /* v-prod ne o-prod */
      
      IF v-altwhse NE "" THEN
        ASSIGN l-whse = v-altwhse.
      ELSE
        ASSIGN l-whse = v-whse.
      
      {w-icsw.i v-prod l-whse NO-LOCK "vl-"}  /* TAH WHSE */
      {w-icsw.i v-prod x-whse NO-LOCK}
      {w-icsp.i v-prod NO-LOCK}
      
      ASSIGN v-costgain = 0.
      IF (NOT avail icsp OR
       (avail icsw AND (INPUT v-prodcost NE icsw.avgcost))) THEN DO:
        FIND zl-notes USE-INDEX k-notes WHERE
             zl-notes.cono         = g-cono AND
             zl-notes.notestype    = "zz" AND
             zl-notes.primarykey   = "standard cost markup" AND
             zl-notes.secondarykey = v-prodcat AND
             zl-notes.pageno       = 1
        NO-LOCK NO-ERROR.
        IF NOT avail zl-notes THEN
          FIND zl-notes USE-INDEX k-notes WHERE
               zl-notes.cono         = g-cono AND
               zl-notes.notestype    = "zz" AND
               zl-notes.primarykey   = "standard cost markup" AND
               zl-notes.secondarykey = "" AND
               zl-notes.pageno       = 1
          NO-LOCK NO-ERROR.
        IF avail zl-notes THEN DO:
          /* calculate the cost gain for nonstock or v-prodcost override  */
          ASSIGN v-costgain =
            IF (INPUT v-prodcost * (1 + (DEC(zl-notes.noteln[1])  / 100)))  >
            INPUT v-prodcost THEN
              (((INPUT v-prodcost * (1 + (DEC(zl-notes.noteln[1]) / 100)))
              * v-qty) - (INPUT v-prodcost * v-qty))
            ELSE
              0.
        END.
      END. /** not avail icsp **/
      ELSE
      IF avail icsp THEN DO:
        IF avail icsw AND
          (icsw.stndcost > icsw.avgcost) AND
          v-prodcost = icsw.avgcost THEN DO:
          /** true icsw with no change to line item cost **/
          ASSIGN v-costgain = (icsw.stndcost - icsw.avgcost) * v-qty.
        END.
      END. /* else do */
      
      IF (v-prod = "" AND v-descrip NE "") THEN
          ASSIGN 
          v-whse  = ""
          v-astr1 = ""
          v-astr2 = "".
      IF v-whse = "" THEN
        ASSIGN v-whse = x-whse.
      {w-icsp.i v-prod NO-LOCK}
      RUN NoteDisplay3(INPUT v-prod).
      
      IF avail icsw OR (v-specnstype = "n" AND v-vendno > 0) THEN DO:
        FIND FIRST apsv WHERE
                   apsv.cono   = g-cono AND
                   apsv.vendno = v-vendno
        NO-LOCK NO-ERROR.
/* tahtrain
        IF avail vl-icsw THEN DO:
          ASSIGN v-leadtm = STRING(INT(vl-icsw.leadtmavg),"9999").
        END.
 tah train replaced with below */        

        IF avail icsw THEN DO:
          ASSIGN v-leadtm = STRING(INT(icsw.leadtmavg),"9999").
        END.
         
        ASSIGN v-vendnm = IF avail apsv THEN
                            apsv.lookupnm
                          ELSE
                            " ".
        
        /* always check repair whse for stock or nonstock availability */
        {w-icsw.i v-prod x-whse NO-LOCK}
        IF v-specnstype = "l" THEN
          ASSIGN v-specnstype = v-specnstype.
        ELSE
        IF NOT avail icsp OR (avail icsp AND NOT avail icsw ) THEN
          ASSIGN v-specnstype = "n".
        ELSE
        IF avail icsw AND icsw.statustype = "O" THEN
          ASSIGN v-specnstype = "s".
        ELSE
          "".
      END.
      
      ASSIGN
        t-lines.faxfl      = ""
        t-lines.leadtm     = IF v-leadtm NE "" AND v-prod NE "" THEN
                               string(int(v-leadtm),"9999")
                             ELSE
                               ""
        t-lines.gain       = IF t-lines.specnstype = "l" THEN
                               0
                             ELSE
                             IF v-costgain < 0 THEN
                               0
                             ELSE
                               v-costgain
        t-lines.lineno     = v-lineno
        t-lines.seqno      = 0
        t-lines.attrfl     = v-attrfl
        t-lines.whse       = IF v-whse NE x-whse THEN
                               STRING(v-whse,"x(4)")
                             ELSE
                               STRING(x-whse,"x(4)")
        t-lines.altwhse    = IF v-altwhse NE "" AND
                              v-altwhse = x-whse THEN
                                ""
                              ELSE
                              IF v-altwhse NE "" AND
                              v-altwhse NE x-whse   THEN
                                STRING(v-altwhse,"x(4)")
                              ELSE
                              IF ctrlw-whse NE "" AND
                              ctrlw-whse NE x-whse  THEN
                                STRING(ctrlw-whse,"x(4)")
                              ELSE
                                ""
        t-lines.prod       = v-prod
        t-lines.astr1      = v-astr3
        t-lines.prodline   = v-prodline
        t-lines.ZsdiXrefType = s-ZsdiXrefType
        t-lines.descrip    = IF avail icsp THEN
                               icsp.descrip[1]
                             ELSE
                               v-descrip
        t-lines.prctype    = v-prctype
        t-lines.prodcat    = IF avail icsp THEN
                               icsp.prodcat
                             ELSE
                               v-prodcat
        t-lines.vendno     = v-vendno
        t-lines.vendnm     = IF v-prod NE "" THEN
                               v-vendnm
                             ELSE
                               ""
        t-lines.prodcost   =  IF v-specnstype = "l" THEN
                                0
                              ELSE
                                v-prodcost
        t-lines.qty        = v-qty
        t-lines.extcost    = IF t-lines.specnstype = "l" THEN
                               0
                             ELSE
                               t-lines.prodcost * t-lines.qty
        t-lines.listprc    = if v-specnstype = "n" then
                               v-nslistprc
                             else
                             IF v-sellprc > 0 THEN
                               v-sellprc
                             ELSE
                             IF avail icsw THEN
                               icsw.listprice
                             ELSE
                               v-sellprc
        t-lines.specnstype = v-specnstype
        t-lines.icspstat   = IF avail icsp THEN
                               icsp.statustype
                             ELSE
                               ""
        t-lines.pdscrecno  = zi-pdscrecno
        t-lines.qtybrkty   = zi-qtybrkty.
        
      IF avail t-lines AND t-lines.specnstype = "n"
      AND v-prod NE "" THEN DO:
        FIND FIRST com WHERE
                   com.cono    = g-cono      AND
                   com.comtype = x-quoteno AND
                   com.lineno  = v-lineno
        NO-LOCK NO-ERROR.
        /*
        IF NOT avail com THEN
          RUN f10_popup.
        */
        DISPLAY fs-3 WITH FRAME f-statusline2.
      END.
      
      /*** try to find f10_popup informatione here ***/
      FIND FIRST com WHERE
                 com.cono    = g-cono      AND
                 com.comtype = x-quoteno   AND
                 com.lineno  = v-lineno
      NO-LOCK NO-ERROR.
      IF avail com THEN DO:
        ASSIGN v-comfl = "q".
      END.
      ELSE
        ASSIGN v-comfl = " ".
     
      /** when an item begins with "core-" it will be considered a core
      charge - create an ii section core charge and a vasps section -
      the in and ii items will offset - marg imprv still kept on 'in' item **/
      
      IF avail t-lines AND t-lines.prod BEGINS "core-" THEN DO:
        DEF BUFFER ii-vaspsl FOR vaspsl.
        {w-icsp.i v-prod NO-LOCK}
        ASSIGN v-specnstype = IF avail icsp THEN
                                ""
                              ELSE
                                "n".
        FIND FIRST ii-vaspsl WHERE
                   ii-vaspsl.cono     = g-cono    AND
                   ii-vaspsl.shipprod = x-quoteno AND
                   ii-vaspsl.whse     = zsdivasp.whse AND
                   ii-vaspsl.seqno    = 4 AND
                   ii-vaspsl.lineno   = 1
        EXCLUSIVE-LOCK NO-ERROR.
        IF NOT avail ii-vaspsl THEN DO:
          CREATE vasps.
          ASSIGN 
            vasps.cono         = g-cono
            vasps.shipprod     = x-quoteno
            vasps.whse         = zsdivasp.whse
            vasps.intrwhse     = ""
            vasps.destwhse     = zsdivasp.whse
            vasps.seqno        = 4
            vasps.sctntype     = "ii"
            vasps.sctncode     = zsdivasp.whse
            vasps.specprtfl    = YES
            vasps.specprtty    = "b"
            vasps.desttype     = ""
            vasps.transdt      = TODAY
            vasps.operinit     = g-operinit
            vasps.transtm      = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                                 SUBSTRING(STRING(TIME,"hh:mm"),4,2)
            vasps.transproc    = g-currproc.
          CREATE ii-vaspsl.
          ASSIGN 
            ii-vaspsl.cono         = g-cono
            ii-vaspsl.shipprod     = x-quoteno
            ii-vaspsl.nonstockty   = v-specnstype
            ii-vaspsl.arpvendno    = IF avail t-lines THEN
                                       t-lines.vendno
                                     ELSE
                                       0
            ii-vaspsl.arpprodline  = IF avail t-lines THEN
                                       t-lines.prodline
                                     ELSE
                                     IF avail icsw THEN
                                       icsw.prodline
                                     ELSE
                                       ""
            ii-vaspsl.arpwhse      = ""
            ii-vaspsl.prodcat      = IF avail t-lines THEN
                                       t-lines.prodcat
                                     ELSE
                                     IF avail icsp THEN
                                       icsp.prodcat
                                     ELSE
                                       ""
            ii-vaspsl.compprod     = v-prod
            ii-vaspsl.proddesc     = IF avail t-lines THEN
                                       t-lines.descrip
                                     ELSE
                                       "Core Charge"
            ii-vaspsl.whse         = STRING(x-whse,"x(4)")
                                     SUBSTRING(ii-vaspsl.user5,1,4) = "1"
            ii-vaspsl.lineno       = 1
            ii-vaspsl.operinit     = g-operinits
            ii-vaspsl.unit         = IF avail icsp THEN
                                       icsp.unitstock
                                     ELSE
                                       "ea"
            ii-vaspsl.unitconv     = 1
            ii-vaspsl.qtyneeded    = 1
            ii-vaspsl.prodcost     = v-prodcost
            ii-vaspsl.qtybasetotfl = YES
            ii-vaspsl.usagefl      = YES
            ii-vaspsl.seqno        = 4
            ii-vaspsl.operinit     = g-operinit
            ii-vaspsl.transdt      = TODAY
            ii-vaspsl.transtm      = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                                     SUBSTRING(STRING(TIME,"hh:mm"),4,2)
            ii-vaspsl.transproc    = g-currproc.
        END.
        IF avail ii-vaspsl THEN
          ASSIGN 
            ii-vaspsl.compprod     = v-prod
            ii-vaspsl.qtyneeded    = v-qty
            ii-vaspsl.arpvendno    = IF avail t-lines THEN
                                       t-lines.vendno
                                     ELSE
                                       0
        ii-vaspsl.arpprodline  = IF avail t-lines THEN
                                   t-lines.prodline
                                 ELSE
                                 IF avail icsw THEN
                                   icsw.prodline
                                 ELSE
                                   ""
        ii-vaspsl.prodcat      = IF avail t-lines THEN
                                   t-lines.prodcat
                                 ELSE
                                 IF avail icsp THEN
                                   icsp.prodcat
                                 ELSE
                                   ""
        ii-vaspsl.proddesc     = IF avail t-lines THEN
                                   t-lines.descrip
                                 ELSE
                                   "Core Charge"
        ii-vaspsl.whse         = zsdivasp.whse
        SUBSTRING(ii-vaspsl.user5,1,4) = t-lines.leadtm
        ii-vaspsl.operinit     = g-operinits
        ii-vaspsl.prodcost     = v-prodcost
        ii-vaspsl.operinit     = g-operinit
        ii-vaspsl.transdt      = TODAY
        ii-vaspsl.transtm      = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                                 SUBSTRING(STRING(TIME,"hh:mm"),4,2)
        ii-vaspsl.transproc    = g-currproc.
      END.
      
      /* always check repair whse for stock or nonstock availability */
      {w-icsw.i v-prod x-whse NO-LOCK}
      {w-icsp.i v-prod NO-LOCK}
      IF v-specnstype = "l" THEN
        ASSIGN v-specnstype = v-specnstype.
      ELSE
      IF NOT avail icsp OR (avail icsp AND NOT avail icsw ) THEN
        ASSIGN v-specnstype = "n".
      ELSE
      IF avail icsw AND icsw.statustype = "O" THEN
        ASSIGN v-specnstype = "s".
      ELSE
        "".
      
      /*** update vaspsl  ***/
      FIND vaspsl WHERE RECID(vaspsl) = t-lines.vaspslid
      EXCLUSIVE-LOCK NO-ERROR.
      IF (NOT avail vaspsl AND v-stage = "cst") OR
         (avail vaspsl AND v-stage = "cst" AND
         (vaspsl.compprod  NE v-prod       OR
          vaspsl.qtyneeded NE v-qty        OR
          vaspsl.prodcost  NE v-prodcost   OR
          vaspsl.proddesc  NE v-descrip)) THEN DO:
        FIND FIRST vasp WHERE
                   vasp.cono = g-cono         AND
                   vasp.shipprod = x-quoteno  
        EXCLUSIVE-LOCK NO-ERROR.
        FIND FIRST zsdivasp WHERE
                   zsdivasp.cono = g-cono         AND
                   zsdivasp.rectype = "fb"        AND
                   zsdivasp.repairno = x-quoteno  
        EXCLUSIVE-LOCK NO-ERROR.
        
        ASSIGN zsdivasp.outincd = "In".
        RELEASE vasp.
      END.
/* REMINDER ZSDIVASPMAP for the ICSP */      
      
     run vaexf_labor.p (input "a",
                        output v-labormins,
                        output v-docmins,
                        output v-drawingmins,
                        output v-electricalmins,
                        output v-mechanicalmins).
     IF AVAIL icsp THEN DO:  
      if num-entries(icsp.user21,"~011") > 0 then
        v-labormins = dec(entry(1,icsp.user21,"~011")).    
      if num-entries(icsp.user21,"~011") > 1 then
        v-docmins   = dec(entry(2,icsp.user21,"~011")).
      if num-entries(icsp.user21,"~011") > 2 then
        v-drawingmins = dec(entry(3,icsp.user21,"~011")).
      if num-entries(icsp.user21,"~011") > 3 then
        v-Electricalmins = dec(entry(4,icsp.user21,"~011")).
      if num-entries(icsp.user21,"~011") > 4 then
        v-Mechanicalmins = dec(entry(5,icsp.user21,"~011")).
        
    END.
/*
    ELSE
      ASSIGN 
        v-labormins = 0
        v-docmins   = 0
        v-drawingmins = 0
        v-electricalmins = 0
        v-mechanicalmins = 0.
*/
      /*** sync up the labor to the find f10_popup informatione here ***/
      
      IF avail icsp THEN DO:
        RUN ProdPricing(INPUT TRUE,
                        INPUT zi-PDSCfl,
                        INPUT zi-PDSCrec,
                        INPUT l-whse,
                        INPUT v-qty,
                        INPUT v-custno,
                        INPUT v-prod,
                        INPUT-OUTPUT zi-price,
                        INPUT-OUTPUT zi-gpmarg,
                        INPUT-OUTPUT zi-level,
                        INPUT-OUTPUT zi-rebate,
                        INPUT-OUTPUT zi-cost,
                        INPUT-OUTPUT zi-listprice,
                        INPUT-OUTPUT zi-disc,
                        INPUT-OUTPUT zi-pdscrecno,
                        INPUT-OUTPUT zi-qtybrkty).
      END.
      FIND FIRST zsdivasp WHERE
                 zsdivasp.cono = g-cono         AND
                 zsdivasp.rectype = "fb"        AND
                 zsdivasp.repairno = x-quoteno  
      EXCLUSIVE-LOCK NO-ERROR.
       

 
      
      IF NOT avail vaspsl THEN DO:
        CREATE vaspsl.
        IF avail t-lines THEN
          ASSIGN t-lines.vaspslid   = RECID(vaspsl).
        ASSIGN 
          vaspsl.whse         = zsdivasp.whse
          vaspsl.cono         = g-cono
          vaspsl.shipprod     = x-quoteno
          vaspsl.compprod     = v-prod
          g-prod              = v-prod
          vaspsl.arpwhse      = ""
          vaspsl.arpvendno    = IF v-vendno > 0 THEN
                                  v-vendno
                                ELSE
                                  0
          vaspsl.user7        = t-lines.listprc
          vaspsl.arpprodline  = IF avail t-lines       AND
                                t-lines.gdesc NE "" AND
                                t-lines.prod = ""   AND
                                t-lines.lineno = v-lineno THEN
                                  ""
                                ELSE
                                IF avail icsw THEN
                                  icsw.prodline
                                ELSE
                                  v-prodline
          vaspsl.prodcat      = IF v-prodcat NE "" THEN
                                  v-prodcat
                                ELSE
                                  ""
          vaspsl.lineno       = v-lineno
          vaspsl.commentfl    = IF v-comfl = "q" THEN 
                                  YES
                                ELSE 
                                  NO
          vaspsl.unit         = IF avail icsp THEN
                                  icsp.unitstock
                                ELSE
                                  "ea"
          vaspsl.qtyneeded    = v-qty
          vaspsl.qtybasetotfl = YES
          vaspsl.unitconv     = 1
          vaspsl.usagefl      = IF avail icsp THEN
                                  NO
                                ELSE
                                  YES
          vaspsl.seqno        = 5
          vaspsl.prodcost     = IF v-specnstype = "l" THEN
                                  0
                                ELSE
                                  v-prodcost
          vaspsl.costoverfl   = IF zi-cost NE v-prodcost THEN
                                  YES
                                ELSE
                                  NO
          vaspsl.xxde1        = IF v-specnstype = "l" THEN
                                  0
                                ELSE
                                IF v-costgain < 0 THEN
                                  0
                                ELSE
                                  v-costgain
          substring(vaspsl.user3,1,1) = if v-attrfl then "y" else "n"
          vaspsl.proddesc     = v-descrip
          vaspsl.operinit     = g-operinits
          SUBSTRING(vaspsl.user5,1,4) = LEFT-TRIM(v-leadtm,"0")
          SUBSTRING(vaspsl.user4,1,4) = IF avail t-lines AND
                                        t-lines.whse NE x-whse AND
                                        t-lines.whse NE "" THEN
                                          STRING(t-lines.whse,"x(4)")
                                        ELSE
                                          ""
          vaspsl.nonstockty   = v-specnstype
          vaspsl.transdt      = TODAY
          vaspsl.transtm      = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                                SUBSTRING(STRING(TIME,"hh:mm"),4,2)
          vaspsl.transproc    = g-currproc.
        assign zsdivasp.invcost = zsdivasp.invcost + 
               (vaspsl.prodcost * vaspsl.qtyneeded).
       
        if int(v-leadtm) > int(zsdivasp.leadtm1) then
          zsdivasp.leadtm1           = STRING(v-leadtm,"9999").
        
                
        if s-ZsdiXrefType <> "" then
          run zsdiCatalogTrace.p
            (input "Audit",  /* Audit, Inquire, Delete */
             input g-cono,
             input vaspsl.compprod,
             input vaspsl.whse,
             input "VAEXF",
             input (if vaspsl.nonstockty = "n" then "n" else "s"),
             input int(entry(2,x-quoteno,"-")), 
             input int(entry(1,x-quoteno,"-")),
             input vaspsl.seqno,
             input vaspsl.lineno,
             input g-operinits,
             output v-zsdireturncd).


        
        
        run ZsdiVaspMap-update (input "a",
                                input recid(vaspsl)).


        if v-attrfl  then do:

          find first zsdirefty where
                     zsdirefty.cono      = g-cono and
                     zsdirefty.rectype   = "va"   and
                     zsdirefty.reference = v-model no-lock no-error.
/* Performance Key Needed */
          if avail zsdirefty then do:                     
            find first zsdirefer use-index k-prod where 
                       zsdirefer.cono        = g-cono     and                
                       zsdirefer.rectype     = "va"       and
                       zsdirefer.typekeyno   = zsdirefty.keyno and
                       zsdirefer.prod        = v-prod no-lock no-error.
            if not avail zsdirefer then do:
              RUN vaexf-brwse-p.p
               (INPUT        v-model,
                INPUT        x-whse,
                             /* tahtrain
                             (IF avail t-lines THEN
                                t-lines.whse
                              ELSE
                                v-whse), */
                input        "Update",
                INPUT-OUTPUT v-prod,
                INPUT-OUTPUT f-descrip,
                INPUT-OUTPUT o-descrip,
                INPUT-OUTPUT v-recid,
                INPUT-OUTPUT lk-keylabel,
                INPUT-OUTPUT lk-keyno).
            end.
          end.  
          RUN DisplayTop1.
        end.
              
      
      
      END.
      ELSE
      IF avail vaspsl THEN DO:
        if v-prod <> vaspsl.compprod then do:
           if s-ZsdiXrefType <> "" then do:
             run zsdiCatalogTrace.p
               (input "Audit",  /* Audit, Inquire, Delete */
                input g-cono,
                input v-prod,
                input vaspsl.whse,
                input "VAEXF",
                input (if v-specnstype = "n" then "n" else "s"),
                input int(entry(2,x-quoteno,"-")),
                input int(entry(1,x-quoteno,"-")),
                input vaspsl.seqno,
                input vaspsl.lineno,
                input g-operinits,
                output v-zsdireturncd).
           end.
         end.
         
        assign zsdivasp.invcost = zsdivasp.invcost - 
               (vaspsl.prodcost * vaspsl.qtyneeded).
         
        ASSIGN
          g-prod = v-prod.
        ASSIGN 
          overlay(vaspsl.user5,1,4)
            = LEFT-TRIM(v-leadtm,"0")
          vaspsl.whse         = zsdivasp.whse
          vaspsl.arpwhse      = ""
          overlay(vaspsl.user4,1,4) = IF avail t-lines AND
                                        t-lines.whse NE x-whse AND
                                        t-lines.whse NE "" THEN
                                          STRING(t-lines.whse,"x(4)")
                                        ELSE
                                          ""
          vaspsl.compprod     = v-prod
          vaspsl.qtyneeded    = v-qty
          vaspsl.user7        = v-nslistprc
          vaspsl.prodcost     = IF v-specnstype = "l" THEN
                                  0
                                ELSE
                                  v-prodcost
          vaspsl.commentfl    = IF v-comfl = "q" THEN 
                                  YES
                                ELSE
                                  NO
          vaspsl.costoverfl   = IF zi-cost NE v-prodcost THEN
                                  YES
                                ELSE
                                  NO
          vaspsl.xxde1        = IF v-specnstype = "l" THEN
                                  0
                                ELSE
                                IF v-costgain < 0 THEN
                                  0
                                ELSE
                                  v-costgain
          vaspsl.xxde2        = (vaspsl.qtyneeded * v-labormins) *
          v-fabcost
          vaspsl.proddesc     = v-descrip
          overlay(vaspsl.user3,1,1) = if v-attrfl then "y" else "n"
          overlay(vaspsl.user5,5,2)     = s-ZsdiXrefType
/*          
          SUBSTRING(vaspsl.user3,1,24) = v-descrip2
          OVERLAY(vaspsl.proddesc2,1,50) = v-descrip
*/
          vaspsl.operinit     = g-operinits
          vaspsl.arpvendno    = IF v-vendno NE o-vendno THEN
                                  v-vendno
                                ELSE
                                  o-vendno
          vaspsl.prodcat      = IF v-prodcat NE o-prodcat THEN
                                  v-prodcat
                                ELSE
                                  o-prodcat
          vaspsl.arpprodline  =  IF avail icsw THEN
                                   icsw.prodline
                                 ELSE
                                   v-prodline
          vaspsl.nonstockty   = v-specnstype
          vaspsl.transdt      = TODAY
          vaspsl.transtm      = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                                SUBSTRING(STRING(TIME,"hh:mm"),4,2)
          vaspsl.transproc    = g-currproc.
        find first bl-t-lines use-index ix2-lead where
                   bl-t-lines.specnstype <> "l" no-lock no-error.
        if avail bl-t-lines then do:
          assign zsdivasp.leadtm1     = STRING(int(bl-t-lines.leadtm),"9999").
        end.  
        else
        if avail zsdivasp then do:
          assign zsdivasp.leadtm1     = STRING(0,"9999").
        end.  
         
        assign zsdivasp.invcost = zsdivasp.invcost + 
               (vaspsl.prodcost * vaspsl.qtyneeded).

        if s-ZsdiXrefType <> "" then

          run zsdiCatalogTrace.p
            (input "Audit",  /* Audit, Inquire, Delete */
             input g-cono,
             input vaspsl.compprod,
             input vaspsl.whse,
             input "VAEXF",
             input (if vaspsl.nonstockty = "n" then "n" else "s"),
             input int(entry(2,x-quoteno,"-")),
             input int(entry(1,x-quoteno,"-")),
             input vaspsl.seqno,
             input vaspsl.lineno,
             input g-operinits,
             output v-zsdireturncd).

        
        
        
        Release vaspsl.  
        run ZsdiVaspMap-update (input "c",
                               input t-lines.vaspslid).
       END.
      
      RELEASE vaspsl.
      
      IF v-linebrowseopen = TRUE THEN DO:
        v-row   = b-lines:FOCUSED-ROW.
        v-rowid = ROWID(t-lines).
        CLOSE QUERY q-lines.
        ON cursor-up back-tab.
        ON cursor-down tab.
        ASSIGN v-linebrowseopen = FALSE.
      END.
      OPEN QUERY q-lines FOR EACH t-lines.
      ENABLE b-lines WITH FRAME f-lines.
      ASSIGN v-linebrowseopen = TRUE.
      b-lines:REFRESHABLE = FALSE.
      REPOSITION q-lines TO ROWID(v-rowid).
      b-lines:SET-REPOSITIONED-ROW(v-row).
      b-lines:REFRESHABLE = TRUE.
      ASSIGN 
        v-leadtm    = ""
        v-lineno    = 0
        v-seqno     = 0
        v-prod      = ""
        v-attrfl    = d-attrfl
        s-ZsdiXrefType = "" 
        v-descrip   = ""
        v-descrip2  = ""
        z-descrip   = ""
        f-descrip   = ""
        v-specnstype = ""
        v-prctype   = ""
        g-whse      = x-whse
        ctrlw-whse  = ""
        v-whse      = ""
        v-altwhse   = ""
        v-astr1     = ""
        v-astr2     = ""
        zi-qtybrkty = " "
        v-matrixed  = " "
        v-prodcost  = 0
        v-costgain  = 0
        v-prodcat   = ""
        v-vendno    = 0
        v-vendnm    = ""
        v-qty       = 1
        o-leadtm    = ""
        o-seqno     = 0
        o-attrfl    = d-attrfl
        o-specnstype = ""
        o-prod      = ""
        o-descrip   = ""
        o-gdesc     = ""
        o-prctype   = ""
        o-whse      = ""
        o-prodcost  = 0
        o-labormins = 0
        o-prodcat   = ""
        o-vendno    = 0
        o-qty       = 1.
    END.
 
    
    {k-sdinavigation.i &jmpcode = "return." }
  END.  /* MidLoop */
  
  IF {k-jump.i} /* or {k-cancel.i} */ THEN DO:
    RUN makequoteno(o-vaquoteno,OUTPUT x-quoteno).
    RUN vaexfinuse.p(INPUT x-quoteno,
                     INPUT g-operinits,
                     INPUT "c",
                     INPUT-OUTPUT v-inuse).
  END.
  
  {k-sdinavigation.i &jmpcode = "leave." }
  
  IF {k-jump.i} /* or {k-cancel.i}  */ THEN DO:
    HIDE FRAME f-whses.
    HIDE FRAME f-mid1.
    HIDE FRAME f-bot1.
    HIDE FRAME f-lines.
    HIDE FRAME f-prodlu.
    IF avail t-lines THEN DO:
      CLOSE QUERY q-lines.
      ON cursor-up back-tab.
      ON cursor-down   tab.
      ASSIGN v-linebrowseopen = FALSE.
      HIDE FRAME f-lines.
    END.
    RUN FindNextFrame (INPUT LASTKEY,
                       INPUT-OUTPUT v-CurrentKeyPlace,
                       INPUT-OUTPUT v-framestate).
    LEAVE.
  END.
  ELSE DO:
    ASSIGN 
      v-lmode = "c".
      v-framestate = "m1".
    IF {k-cancel.i} THEN DO:
      READKEY PAUSE 0.
    END.
    
  END.
END.

