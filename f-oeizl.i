/*h*****************************************************************************
  INCLUDE      : f-oeizl.i
  DESCRIPTION  : Form for OEIZL
  USED ONCE?   : No (oeizl.p, oeizl1.p, oeizl2.p, oeizl2b.p, oeizl2k1.p,
                     oeizl2k2.p, oeizl3.p, oeizl4.p, oeizl4b.p, oeizl4k1.p,
                     oeizl4k2.p, oeizl5,p, oeizl6.p, oeizl8.p & oeizlk.p)
  AUTHOR       : rhl
  DATE WRITTEN : 12/23/91
  CHANGES MADE :
    12/23/91 pap; TB#  5266 JIT/Line Due - replace reqship date with promised
        date
    12/26/91 pap; TB#  5266 JIT/Line Due - add jit diff flag if oeeh reqshipdt
        or promisedt differ from oeel dates
    06/04/92 smb; TB#  6867 Operator choice of Qty/Value
    12/21/92 mms; TB#  8566 Added rushfl display
    07/11/93 rhl; TB# 12180 Change F7 Confirmation to a pulldown
    01/29/94 kmw; TB# 11747 Make stage a range
    01/29/94 kmw; TB# 14457 Tag fields that are keyed; put keyed fields on left
        side of selection criteria
    04/28/95 smb; TB# 11811/10234 Added specnstype display in d-oeizl2.i from
        oeizl2 and oeizl4.
    05/09/95 mtt; TB#  5180 When looking for a product-need kit ln
    03/14/96 tdd; TB# 20713 Inquiry not working properly
    03/21/96 kr;  TB# 12406 Variables defined in form, moved out of form
    05/29/98 cm;  TB# 24295 Incorrect qty/value on components. Added
        v-qtyneeded.
    03/31/99 lbr; TB# 20955 Added ability to display orders in accending or 
        decending- added var s-directionfl too
    05/11/99 cm;  TB# 5366 Added shipto notes
    02/12/01 lcg; TB# e7852 AR Multicurrency changes
    08/27/02 emc; TB# e13512 OEIZL Slow Performance
    06/02/03 lcb; TB# t14244 8Digit Ord No Project
    09/30/03 kjb; TB# t15904 Allow the user to control display of the PO#/Job#
        column
    09/29/05 ds; TB# e22714 Increased the lenght of ship to display to 5 char
                 instead of 2.
*******************************************************************************/

def {1} shared var v-msg1        as c format "x(45)" initial
    " Order #    Ln#  Ty St Promised Invoiced By"                      no-undo.
def {1} shared var v-msg2        as c format "x(21)"                   no-undo.
def {1} shared var v-msg3        as c format "x(12)"
    initial " Quantity   "                                             no-undo.
def {1} shared var v-mode        as i format "99"                      no-undo.
def {1} shared var s-crossref    as c                                  no-undo.
def {1} shared var s-transtype   like oeeh.transtype                   no-undo.
def {1} shared var s-stagecdl    as c format "x(1)" init "0"           no-undo.
def {1} shared var s-stagecdh    as c format "x(1)" init "9"           no-undo.
def {1} shared var s-custpo      like oeeh.custpo                      no-undo.
def {1} shared var s-vendno      like oeelb.arpvendno                  no-undo.
def {1} shared var s-takenby     like oeeh.takenby                     no-undo.
def {1} shared var s-reqshipdt   like oeeh.reqshipdt                   no-undo.
def {1} shared var s-promisedt   like oeeh.promisedt                   no-undo.
def {1} shared var s-doonlyfl    as logical initial no                 no-undo.
def {1} shared var s-valuefl     as l format "v/q"                     no-undo.
def {1} shared var s-kitty       as c format "x" init "o"              no-undo.
def {1} shared var s-custno      like arsc.custno                      no-undo.
def {1} shared var s-shipto      like arsc.shipto                      no-undo.
def {1} shared var s-prod        like icsp.prod                        no-undo.
def {1} shared var s-whse        like icsd.whse                        no-undo.
def {1} shared var s-directionfl as log format "a/d" initial yes       no-undo.
def {1} shared var s-custpojobty as c format "x" init "c"              no-undo.

def            var v-fkey        as c format "x(5)" extent 5 initial
    ["","","aric","iciap","oeii"]                                      no-undo.
def            var s-linedo      as c format "x"                       no-undo.
def            var s-jitflag     as c format "x(1)"                    no-undo.
def            var s-rushfl      as c format "x(1)"                    no-undo.
def            var v-prod        like icsp.prod                        no-undo.
def            var v-whse        like icsw.whse                        no-undo.
def            var v-type        as c                                  no-undo.
def            var v-crqty       like icsec.orderqty                   no-undo.
def            var v-crunit      like icsec.unitsell                   no-undo.
def            var o-custno      like arsc.custno                      no-undo.
def            var v-totlineamt  like oeeh.totlineamt                  no-undo.
def            var s-netamt      like oeel.netamt                      no-undo.
def            var o-shipto      like arss.shipto                      no-undo.
def            var o-custpo      like oeeh.custpo                      no-undo.

/*tb 12406 03/21/96 kr; Variables defined in form */
def            var s-order       as c format "x(17)"                   no-undo.
def            var s-suspend     as c format "x"                       no-undo.
def            var s-pocust      as c format "x(18)"                   no-undo.
def            var s-value       as c format "x(13)"                   no-undo.
def            var v-qtyneeded   like oeelk.qtyneeded initial 1        no-undo.
def            var s-usekeyfl    as l                                  no-undo.

def {1} shared frame f-oeizl.
def {1} shared frame f-oeizls.
v-length = 13.

/*tb 11747 01/29/94 kmw; Make stage a range */
/*tb 14457 01/29/94 kmw; Tag fields that are keyed */
/*tb 5180 05/09/95 mtt; When looking for a product-need kit ln */
/*tb 20955 03/31/99 lbr; moved last collum left 1 space s-transtype,s-stagecd
    s-promisedt,s-valuefl, s-stagecdh added s-directionfl */
/*tb e7852 07/28/00 lcg; A/R Multicurrency. Added currencyty */
/*tb t15904 09/30/03 kjb; added s-custpojobty to the frame */
form
    s-custno                  colon 12    label "Customer #"
        help "*** Selections Entered on the Left are Indexed and Fast *** [L]"
    arsc.notesfl              at    26 no-label
    s-shipto                  at    30 no-label
        help "Ship To                                                     [L]"
    arss.notesfl              at    38 no-label
    arsc.lookupnm             at    41 no-label
    arsc.currencyty           at    59 no-label
    s-transtype               colon 67    label "Type"
    s-doonlyfl                at    73 no-label
        help "(Y)es to Show Only Orders with DO Lines"
    s-prod                    colon 12
        {f-help.i}
    icsp.notesfl              at    38 no-label
    s-kitty                   at    39 no-label
    icsp.lookupnm             at    41 no-label
    s-stagecdl                colon 67    label "Stage"
    "to"                      at    71
    s-stagecdh                at    74 no-label
    s-custpo                  colon 12    label "Customer PO"
    s-takenby                 colon 49    label "Taken By"
    s-promisedt               colon 67    label "Promise"
    s-crossref                at    14 no-label  
    s-vendno                  colon 12    label "Vendor #" 
    s-usekeyfl                at    28    label "Key?"
   help "Use GUI Keyword: Fast with Ship To, Taken By, Stage or Promise"
    s-whse                    colon 49    label "Whse"
        {f-help.i}
    s-valuefl                 colon 67    label "Display"
    s-custpojobty             at    72 no-label
        help "(C)ustomer #/Ship To, Customer (P)O, or (J)ob ID"
    s-directionfl             at    75 no-label
        help "(A)scending or (D)escending Display"
    v-msg1                    at     1 no-label
    v-msg2                    at    46 no-label
    v-msg3                    at    67 no-label

    skip(13)

    {f-frmend.i}
with frame f-oeizls side-labels centered no-underline title g-title width 80
overlay.

/*tb 11811 04/28/95 smb; Added 1 to s-order and Sub'ed 1 from s-pocust*/
form
    s-order                   at     1
    oeeh.transtype            at    18
    s-linedo                  at    20
    oeeh.stagecd              at    21
    s-suspend                 at    22
    s-jitflag                 at    23
    oeeh.promisedt            at    24
    s-rushfl                  at    32
    oeeh.invoicedt            at    33
    oeeh.takenby              at    42
    s-pocust                  at    47
    s-value                   at    66
with frame f-oeizl overlay no-labels width 78 column 2 scroll 1
no-box row 8 v-length down no-hide.

pause 0 before-hide.

if {c-chan.i} then
    assign
        v-fkey[1] = "csc"
        v-fkey[2] = "csp"
        v-fkey[3] = "cslp"
        v-fkey[4] = ""
        v-fkey[5] = "".