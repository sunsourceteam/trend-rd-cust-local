/***************************************************************************** 
  z-oeepi-contact.i
  Purpose: Include to retreive contact info that appears at the top 
           of an invoice.
  Used:    oeepi9.p
           oeepif9.p
           oeepim1.p

******************************************************************************/
    
assign x-phone         = " "
       s-cname-phonehd = " "
       s-cemail        = " ".
assign x-company = substr(arsc.user19,1,4).
if x-company = " " then
  assign x-company = "SS".
/* Credit Manager */
find first oimsp where oimsp.person = arsc.creditmgr no-lock no-error.
if avail oimsp and oimsp.phoneno <> " " and oimsp.miscdata[1] <> " " then  
  do:
  assign x-phone = string(oimsp.phoneno,"(xxx)xxx-xxxx/xxxx").   
  assign s-cname-phonehd = right-trim(substr(oimsp.name,1,20)) + 
                                               " " + x-phone. 
  assign s-cemail = if g-cono = 1 then
                      right-trim(oimsp.miscdata[1]) + "@sunsrce.com"
                    else
                      if x-company = "PGON" then
                        right-trim(oimsp.miscdata[1]) + "@paragontech.com" 
                      else
                        right-trim(oimsp.miscdata[1]) + "@pshinc.com".
end.
if x-phone = " " or s-cemail = " " then
  do:
  if g-cono = 1 then
    find first oimsp where oimsp.person = "AR01" no-lock no-error.
  else
    if x-company = "PGON" then
      find first oimsp where oimsp.person = "PARA" no-lock no-error.    
    else
      find first oimsp where oimsp.person = "PERF" no-lock no-error.
  if avail oimsp then  
    do:
    assign x-phone = string(oimsp.phoneno,"(xxx)xxx-xxxx/xxxx").
    assign s-cname-phonehd = right-trim(substr(oimsp.name,1,20)) +
                                                     " " + x-phone. 
    assign s-cemail = if g-cono = 1 then
                        right-trim(oimsp.miscdata[1]) + "@sunsrce.com"
                      else
                        if x-company = "PGON" then
                          right-trim(oimsp.miscdata[1]) + "@paragontech.com"
                        else
                          right-trim(oimsp.miscdata[1]) + "@pshinc.com".
  end.
end.
       
assign x-phone         = " "
       s-sname-phonehd = " "
       s-semail        = " ".
/* Customer Service */
find first oimsp where oimsp.person = oeeh.takenby no-lock no-error.
if not avail oimsp or (avail oimsp and (oimsp.phoneno     = " " or
                                        oimsp.miscdata[1] = " ")) then
  do:
  if g-cono = 1 then
    assign x-phone = "(630)317-2700"
           s-sname-phonehd = "Customer Service" + " " + x-phone
           s-semail = "SunSourceInvoicing@sunsrce.com".
  else
    do:
    find icsd where icsd.cono = g-cono and
                    icsd.whse = oeeh.whse
                    no-lock no-error.
    assign x-phone = if avail icsd and icsd.phoneno <> " " then
                       string(icsd.phoneno,"(xxx)xxx-xxxx/xxxx")
                     else
                       if x-company = "PGON" then
                         "(586)756-9100" 
                       else
                         "(888)628-2800".
    assign s-sname-phonehd = "Customer Service" + " " + x-phone.
    assign s-semail = if avail icsd and icsd.user23 <> " " then
                        icsd.user23
                      else
                        if x-company = "PGON" then
                          "Service@paragontech.com"
                        else
                          "InsideSales@pshinc.com".
  end.
end.
else  
  do:
  assign x-phone = string(oimsp.phoneno,"(xxx)xxx-xxxx/xxxx").
  assign s-sname-phonehd = right-trim(substr(oimsp.name,1,20)) + 
                                                 " " + x-phone.
  assign s-semail = if g-cono = 1 then
                      right-trim(oimsp.miscdata[1]) + "@sunsrce.com"
                    else
                      if x-company = "PGON" then
                        right-trim(oimsp.miscdata[1]) + "paragontech.com"
                      else
                        right-trim(oimsp.miscdata[1]) + "pshinc.com".
end.
if x-phone = " " or s-semail = " " then
  do:
  if g-cono = 1 then
    assign s-sname-phonehd = "Customer Service (630)317-2700"
           s-semail        = "SunSourceInvoicing@sunsrce.com".
  else
    do:
    find icsd where icsd.cono = g-cono and
                    icsd.whse = oeeh.whse
                    no-lock no-error.
    assign x-phone = if avail icsd and icsd.phoneno <> " " then
                       string(icsd.phoneno,"(xxx)xxx-xxxx/xxxx")
                     else
                       if x-company = "PGON" then
                         "(586)756-9100" 
                       else
                         "(630)628-2800/520".
    assign s-sname-phonehd = "Customer Service" + " " + x-phone.
    assign s-semail = if avail icsd and icsd.user23 <> " " then
                        icsd.user23
                      else
                        if x-company = "PGON" then
                          "Service@paragontech.com"
                        else
                          "AccountsReceivable@pshinc.com".
  end.
end.
    