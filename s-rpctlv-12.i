
/* s-rpctlv-12.i 1.3 04/16/98 */
/*h****************************************************************************
  INCLUDE      : s-rpctlv.i
  DESCRIPTION  : Video Output from Report Control
  USED ONCE?   : no (rptctl.p and rptctl2.p)
  AUTHOR       : enp
  DATE WRITTEN : 08/13/91
  CHANGES MADE :
    07/07/94 dww; TB# 16054 O/S Shell accessible from "pg" command
    07/17/96 dww; TB# 20741 Unix Shell accessible from OIPF.  Add this include
        into oipf.p.  Added &filename parameter to control whether or not
        to compile the 'z-filename' assignment in or not.  If the calling
        program has already set the value of 'z-filename', then this code
        will not be needed and will not compile into the program.
    02/05/97 jl;  TB# 22553 Added &skipdelete parameter to control whether or
        not to delete the file after printing.  Added for oipf.p where
        where may want to view file but not delete until later.
    04/07/98 cm;  TB# 24789 Allow vid printing to use mmore command
    04/09/98 dbw; TB# 23648 NT Migration for PVCC
    08/13/02 lcb; TB# e12313 Report Manager overwriting output files
    01/09/04 rgm; TB# e19180 Linux support of less instead of page command
******************************************************************************/

/*tb 16054 07/07/94 dww; O/S Shell accessible from "pg" command */
&if "{&filename}" ne "set" &then
    z-filename = v-printdir + sapb.reportnm + "." + v-jobnm.
&endif


case opsys:
   when "unix" then do:
         if sasp.pcommand = "passthru" then
            z-shellcmd = "cat " + z-filename.

         /*tb 24789 04/07/98 cm; Allow vid printing to use mmore command */
         else if sasp.pcommand = "mmore" then
            z-shellcmd = "mmore " + z-filename.

         else if sasp.pcommand = "linux" then
            z-shellcmd =
                'COLUMNS=132; export COLUMNS;' +
                ' exec /usr/bin/less --chop-long-lines --shift 8 "$@" ' +
                z-filename.
         else do:
           z-shellcmd = "pg -14 -f -c " + z-filename.

           /* Get security for Access to Shell and adjust the Shell Command */
           {shellpg.lpr  &accessfl = z-accessfl
                      &cmd      = z-shellcmd}

         end. /* else do */

         os-command value(z-shellcmd).
      end. /*when "unix"*/

   when "win32" then do:
         if sasp.pcommand = "passthru" then
            z-shellcmd = "type " + z-filename.
         else do:
            z-shellcmd = "more " + z-filename.

            /* Get security for Access to Shell and adjust the Shell Command */
            {shellpg.lpr  &accessfl = z-accessfl
                          &cmd      = z-shellcmd}
         end. /* else do */

         os-command value(z-shellcmd).
      end. /* when "win32" */

   when "vms" then do:
         vms type/page value(z-filename).
         z-filename =  z-filename  + ";" + chr(42).
      end. /* when "vms" */

   when "msdos" or
   when "os2"   or
   when "btos"  then os-command type value(z-filename) | more.

end case.

/* Remove the file from the system */

{&skipdelete}

os-delete value(z-filename).

/{&skipdelete}* */


