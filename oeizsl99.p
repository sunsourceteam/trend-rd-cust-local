/* oeizsl99.p */
Define Input Parameter p-section as character.
def shared var g-cono           like sasc.cono         no-undo.
def shared var g-operinit       as c format "x(4)"     no-undo.
def shared var g-ourproc        like sassm.currproc    no-undo.
def shared var g-orderno        like oeel.orderno      no-undo.
def shared var g-ordersuf       like oeel.ordersuf     no-undo.
def shared var s-commentfl      as c format "x"        no-undo. 
def shared var s-lineno         like oeel.lineno       no-undo.
def shared var s-prod           like oeel.shipprod     no-undo.
def shared var s-proddesc       as c format "x(49)"    no-undo.

def shared var v-batchnm        like oeehb.orderno     no-undo.

def buffer rarr-com for com.
def buffer ic-oeehb  for oeehb.

if p-section = "bld" then do:
/******
/* Notes on RARR */
   find rarr-com use-index k-com where rarr-com.cono       = g-cono     and 
                                       rarr-com.comtype    = "puq"      and 
                                       rarr-com.orderno    = v-batchnm  and
                                       rarr-com.ordersuf   = 0          and 
                                       rarr-com.lineno     = s-lineno   
                                       no-lock no-error.
   if avail rarr-com then do:
      if s-commentfl = "c" then
         assign s-commentfl = "x".
      else
         assign s-commentfl = "p".
   end.
   ******/
end. 
/* Notes on RARR */

/* display customer product xref in OEIO */
   find ic-oeehb where ic-oeehb.cono    = g-cono and
                       ic-oeehb.batchnm =  string(g-orderno,">>>>>>>9") and
                       ic-oeehb.seqno   = 2 no-lock no-error.
   if avail ic-oeehb then do:
      find icsec use-index k-altprod where icsec.cono     = g-cono         and
                                           icsec.altprod  = s-prod         and
                                           icsec.rectype  = "c"            and
                                           icsec.custno   = ic-oeehb.custno 
                                           no-lock no-error.
      if avail icsec then do:
         assign substring(s-proddesc,26,3)  = "x:".
         assign substring(s-proddesc,28,22) = icsec.prod.
      end.
   end.
/* display customer product xref */
