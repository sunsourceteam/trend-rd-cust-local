/* CreateLabel.p */
/* 
   changes: Double-check with zsdibarcd for copies and whether to run "new"             version. 4/2008 dkt

   dkt 11/2005 This is the middle part of an overly complicated way to print
   labels. The reason I made it was to simplify the call to print a given
   label. If you have all the information you need it should print immediately.
   If not, frames pop up for all the necessary information. This part
   may be put inside a loop to append to the file, while part 1 and part 2
   should both be outside the loop.

   dkt 01/19/2006 Have to change to allow range of Bins.
   Actually the loop ended up being included within this createLabel.p program.
   InVal4 is for the last item in the loop. Bins only at the moment, sorry.
   THIS SHOULD BE CHANGED to outside this program, which means the popup fillin    might also have to be separated in a pre-process done in PrintLabelsPartOne.
   PrintPartOne  /* create file */
   optional Loop:
     createLabel./* put data in file */
   PrintPartTwo  /* print and delete file */

   dkt 02/22/2006 Allow non-stock printing by input lblprod.

   PrintLabel part 1 - make stream and file with 1st line 
   CreateLabel.p(record value)
   PrintLabel part 2 - print file as label and delete file

   icsw MUST follow icsp; product, whse
   oeeh MUST follow "*" ; orderno, ordersuf 
   oeel MUST follow oeeh; 

   inPrinterNm - printer name in sasp.
   inLabelNm   - label name in sasp.fieldxxx
   inLabelType - label type, must be from choices defined in this program.
   mTable1,2,3 - inTables for Find statements, may be NULL and leading
     values should be NULL to match multiple inVals
   inVal1-5    - inVal for Find statements to match given mTables
     see setupByType if passing the values in
   extraData   - csv data to dump in EVERY label - such as g-operinit
   numCopies   - number of copies to print
   handheld    = full size or handheld
*/

define input parameter inPrinterNm like sasp.printernm no-undo.
define input parameter inLabelNm   as char no-undo.
define input parameter inLabelType as char no-undo.
define input parameter mTable1     as char no-undo.
define input parameter mTable2     as char no-undo.
define input parameter mTable3     as char no-undo.
define input parameter inVal1      as char no-undo.
define input parameter inVal2      as char no-undo.
define input parameter inVal3      as char no-undo.
define input parameter inVal4      as char no-undo.
define input parameter inVal5      as char no-undo. /* was # copies */
define input parameter extraData   as char no-undo.
define input parameter inNumCopies as char no-undo.
define input parameter inHand      as logical no-undo.
define input parameter fndMethod   as char no-undo. /* num or recid */
define input parameter inDR        as char no-undo. /* DR for BC2000 */
define input parameter inLabelFormat as char no-undo.

/* labelfile = path + filename of datafile */
define shared var labelfile as char no-undo.
define shared stream outStream.
define shared var didOnce as logical no-undo.
define shared var lblCnt  as integer no-undo.
define shared var printErrOut as logical no-undo.
define shared var currNumCopies as integer no-undo.
define shared var currDR as char no-undo.

def shared var g-cono as integer no-undo.
def var validinfo as logical initial no no-undo.
def var exitLoop as logical initial no no-undo.
def var printernm like sasp.printernm format "x(12)" no-undo.

/* actual name of the label to print */
def var labelnm   as char format "x(12)" no-undo.
def var labelty   as char format "x(12)" no-undo.
def var lkTable  as char extent 3 no-undo.
def var preTitle as char extent 5 no-undo.
def var inVal    as char extent 5 format "x(20)" no-undo.
def var updFillExtent as integer initial 5 no-undo.
def var numCopies as integer format ">9" no-undo.

def var dcolorValue as int initial 2 no-undo.

def var zsdiNewFlag as logical initial no no-undo.


find first zsdibarcd where zsdibarcd.cono = g-cono and
zsdibarcd.labelnm = inLabelNm and zsdibarcd.style = "new"
no-lock no-error.

if avail zsdibarcd then do:
  if inNumCopies = "" then inNumCopies = string(zsdibarcd.copies).
  if inDR = "" then inDR = zsdibarcd.txdatanm.
  zsdiNewFlag = yes.
end.

numCopies = integer(inNumCopies).

form                                                            
  "Printer:"  at  1 dcolor dcolorValue    printernm   at 12
  "Label:"    at  1 dcolor dcolorValue    labelnm     at 12                      preTitle[1] at  1     inVal[1]    at 12
  preTitle[2] at  1     inVal[2]    at 12
  preTitle[3] at  1     inVal[3]    at 12
  preTitle[4] at  1     inVal[4]    at 12
  preTitle[5] at  1     inVal[5]    at 12
  "Copies:"   at  1 dcolor dcolorValue    numCopies   at 12
with frame f-printlabel no-labels no-hide overlay width 35 down
title "Print Label".       

form
  "Prn:" at 1 dcolor dcolorValue printernm at 5
  "Lbl:" at 1 dcolor dcolorValue labelnm   at 5
  preTitle[1] at 1
  inVal[1]    at 1
  preTitle[2] at 1
  inVal[2]    at 1
  preTitle[3] at 1
  inVal[3]    at 1
  preTitle[4] at 1
  inVal[4]    at 1
  preTitle[5] at 1
  inVal[5]    at 1
  "Copies:"   at 1 dcolor dcolorValue
  numCopies   at 9
with frame f-hhprintlabel no-labels no-box no-hide overlay width 26.       

define temp-table labeltypes no-undo
  field labelname as char
  field labeltype as char
  index k-lt labeltype labelname.
define query q-lbltype for labeltypes scrolling.
define browse b-labeltype query q-lbltype
  display labeltype format "x(12)" labelname format "x(12)"
  with 8 down width 24 centered overlay no-hide no-labels
  title "Label Types  Label Names".

form
  b-labeltype
  with frame f-browselabels width 26 no-hide overlay.

on any-key of b-labeltype do:
  if {k-cancel.i} or keylabel(lastkey) = "PF11" then do:
    hide frame f-browselabels. 
    close query q-lbltype.
    apply "window-close" to frame f-browselabels. 
  end.
  if keylabel(lastkey) = "RETURN" or keylabel(lastkey) = "PF1" then do:
    labelnm = labeltypes.labelname.
    labelty = labeltypes.labeltype.
    hide frame f-browselabels. 
    close query q-lbltype.
    if inHand then 
      display labelnm with frame f-hhprintlabel.
    else
      display labelnm with frame f-printlabel.
    apply "window-close" to frame f-browselabels. 
  end.
end.

/* Allow for SASP to drive product labels  */                       
if inPrinterNm <> "" and inLabelNm <> "" and                        
   inLabelType = "Product" then do:                                 
     find sasp where sasp.printernm = inPrinterNm no-lock no-error.    
     if avail sasp and inLabelNm = "rfrcv.lbl" and                     
        sasp.prodlbl <> "rfrcv.lbl" and sasp.prodlbl <> "" then do:    
       inLabelNm = sasp.prodlbl.                                     
       labelnm = inLabelNm.                                          
       printernm = inPrinterNm.                                      
     end.                                                              
end.                                                                

run FillLabelTypes.
updateinfo:                                               
do while exitLoop = no on endkey undo, leave updateinfo:  
    /* fill out form with data */
    printernm = inPrinterNm.
    labelnm   = inLabelNm.
    inVal[1] = inVal1. inVal[2] = inVal2. inVal[3] = inVal3.
    inVal[4] = trim(inVal4). inVal[5] = trim(inVal5).
    if inHand then
      display printernm labelnm
        inVal[1] inVal[2] inVal[3] inVal[4] numCopies with frame f-hhprintlabel.
    else if inVal[1] = "" then
      display printernm labelnm
        inVal[1] inVal[2] inVal[3] inVal[4] numCopies with frame f-printlabel.
  
    /* run fillLabelTypes. */
    find labeltypes where labeltypes.labelname = labelnm no-lock no-error.
    if avail labeltypes then do:
      labelty = labeltypes.labeltype.  
      validinfo = yes.
    end.
    lkTable[1] = mTable1.
    lkTable[2] = mTable2.
    lkTable[3] = mTable3.

    if inHand then
      update
        printernm when inPrinterNm = "" 
      with frame f-hhprintlabel 
      editing:
        readkey.
        if {k-cancel.i} then do:
          printErrOut = yes.
          exitLoop = yes.
          validInfo = no.
        end.
        apply lastkey.
      end.  
    else  
      update
        printernm when inPrinterNm = "" 
      with frame f-printlabel
      editing:
        readkey.
        if {k-cancel.i} then do:
          printErrOut = yes.
          exitLoop = yes.
          validInfo = no.
        end.
        apply lastkey.
      end.  
    run FillLabelTypes.                
    if inLabelNm = "" and inLabelType ne "" then do:
      find labeltypes where labeltypes.labelType = inLabelType no-lock no-error.
      if avail labeltypes then do:
        labelnm = labeltypes.labelName.
        if inHand then
          display labelnm with frame f-hhprintlabel.
        else
          display printernm labelnm with frame f-printlabel.
      end.
    end.
    if inHand then
      update labelnm when inLabelNm = "" with frame f-hhprintlabel
      editing:
        readkey.
        if {k-cancel.i} then do:
          printErrOut = yes.
          validInfo = no.
          exitLoop = yes.
        end.
        if {k-func12.i} and frame-field = "labelnm" then do:
          open query q-lbltype preselect each labeltypes.       
          find first labeltypes no-error.
          if avail(labeltypes) then do:
            enable b-labeltype with frame f-browselabels. 
            apply "ENTRY" to b-labeltype in frame f-browselabels.
            wait-for window-close of frame f-browselabels. 
          end.
          else 
            message "No labels found for this printer" view-as alert-box.
        end.
        apply lastkey.
      end.
    else 
    update
      labelnm when inLabelNm = ""  
    with frame f-printlabel
    editing:
      readkey.
      if {k-cancel.i} then do:
        printErrOut = yes.
        validInfo = no.
        exitLoop = yes.
      end.
      if {k-func12.i} and frame-field = "labelnm" then do:
        open query q-lbltype preselect each labeltypes.       
        find first labeltypes no-error.
        if avail(labeltypes) then do:
          enable b-labeltype with frame f-browselabels. 
          apply "ENTRY" to b-labeltype in frame f-browselabels.
          wait-for window-close of frame f-browselabels. 
        end.
        else 
          message "No labels found for this printer" view-as alert-box.
      end.
      apply lastkey.
    end. /* editing */
    find sasp where sasp.printernm = printernm no-lock no-error.
    if not avail(sasp) then do:
      message "Printer " printernm "not found".
      next-prompt printernm.
    end.
    find labeltypes where labeltypes.labelname = labelnm no-lock no-error.
    if not avail(labeltypes) then do:
      message "Label " labelnm " is not associated with printer " printernm.
      inPrinterNm = "".
      inLabelNm = "".
      next-prompt labelnm.
    end. 
    else do:
      labelty = labeltypes.labeltype.
      validinfo = yes.
    end.
    run setUpByType.

  if inHand then
  update 
    inVal[1] when inVal[1] = "" /* preTitle[1] ne ""  = ? */
    inVal[2] when inVal[2] = "" /* preTitle[2] ne "" */
    inVal[3] when inVal[3] = "" /* preTitle[3] ne "" */
    inVal[4] when (inVal[4] = "" and preTitle[4] ne "")
    inVal[5] when (inVal[5] = "" and preTitle[5] ne "")
    numCopies when (numCopies < 1 or numCopies = ?) /* labelnm = ?   ne "" */
  with frame f-hhprintlabel
  editing:
    readkey. 
    if {k-cancel.i} then do:
      printErrOut = yes.
      exitLoop = yes.
      validInfo = no.
    end.  
    apply lastkey. 
  end. 
  
  else
  update 
    inVal[1] when inVal[1] = "" /* preTitle[1] ne ""  = ? */
    inVal[2] when inVal[2] = "" /* preTitle[2] ne "" */
    inVal[3] when inVal[3] = "" /* preTitle[3] ne "" */
    inVal[4] when (inVal[4] = "" and preTitle[4] ne "") 
    inVal[5] when (inVal[5] = "" and preTitle[5] ne "") 
    numCopies when (numCopies < 1 or numCopies = ?) /* labelnm = ?   ne "" */
  with frame f-printlabel
  editing:
    readkey.
    if {k-cancel.i} then do:
      printErrOut = yes.
      exitLoop = yes.
      validInfo = no.
    end.  
    apply lastkey.
  end.
  exitLoop = yes. 
end.


find first zsdibarcd where zsdibarcd.cono = g-cono and
zsdibarcd.labelnm = labelNm and zsdibarcd.style = "new"
no-lock no-error.

if avail zsdibarcd then do: /*  for zsdibarcd while true: */
  zsdiNewFlag = yes.
  if inDR = "" then inDR = zsdibarcd.txdatanm.
end.
zsdiNewFlag = yes.
if validinfo then do:
  if inDR = "" or not zsdiNewFlag then do:
    inDR = "".
    if not didOnce then do:
      output stream outStream to value(labelfile).
      put stream outStream unformatted "/L=" + labelnm + " /P=" + Printernm     
        + " /C=" + string(numCopies) skip.                                
      didOnce = yes.
      /* currNumCopies = numCopies. */
    end.
    else do:
      if currNumCopies ne numCopies then do:
        put stream outStream unformatted "/L=" + labelnm + " /P=" + Printernm 
          + " /C=" + string(numCopies) skip.                                  
        /* currNumCopies = numCopies.  */
      end. 
    end.
  end.
  else do:   /* inDR != "" */
    if not didOnce then do:
      output stream outStream to value(labelfile).
      put stream outStream unformatted "/L=" + labelnm + " /P=" + Printernm     
        + " /DR"+ inDR + " /C=" + string(numCopies) skip.                              didOnce = yes.
      /* currNumCopies = numCopies. */
    end.
    else do:
      if (currNumCopies ne numCopies) or (currDR ne inDR) then do:
        put stream outStream unformatted "/L=" + labelnm + " /P=" + Printernm 
          + " /DR" + inDR + " /C=" + string(numCopies) skip.                            /* currNumCopies = numCopies.  */
      end. 
    end.
  end.
  currNumCopies = numCopies.
  currDR = inDR.
  
  if labelty = "Bin" and inVal[4] ne ""
  then run LooptheRecords.
  else do:
    run FindtheRecords.
    run PrinttheLabel.
  end.
  printErrOut = no.
end.
else
  printErrOut = yes.
hide frame f-printlabel.
hide frame f-hhprintlabel.
/** End Main **/

procedure PrinttheLabel:
 /* export data to file */
 def var csz as char format "x(30)" initial "" no-undo.
 def var proddescrip as char format "x(24)" initial "" no-undo. 
 
 def var arsczipcd   like arsc.zipcd no-undo.     
 def var delptzipchksum as char no-undo.
 def var arscname    like arsc.name no-undo.     
 def var arscaddr     like arsc.addr no-undo.     
      
 def var lblprod like icsp.prod initial "" no-undo.     
 def var lbldescrip   like wmsb.descrip initial "" no-undo.  
 def var lblbinloc    like wmsb.binloc initial "" no-undo.   
 def var lblunitstock like icsp.unitstock initial "" no-undo.
 def var lblunitsell  like icsp.unitsell initial "" no-undo.
 def var lblunitcnt   like icsp.unitcnt initial "" no-undo.
 def var lblweight    like icsp.weight initial "" no-undo.
 def var lblunfbin    as char format "x(12)" initial "" no-undo. 
 def var data         as char extent 50 no-undo.
 def var indx         as integer no-undo.

 case labelty:
   when "AR Mailing" then do:
     csz = trim(arsc.city) + ", " + trim(arsc.state) + " " + trim(arsc.zipcd).
     export stream outStream delimiter "~~" 
     sasp.armaillbl format "x(12)"
     arsc.zipcd     format "x(12)"
     "delptzip + chksum" format "x(14)"
     arsc.name      format "x(30)"
     arsc.addr[1]   format "x(30)"
     arsc.addr[2]   format "x(30)"
     csz            format "x(30)".
     lblCnt = lblCnt + 1.
   end.
   when "CM Mailing" then do:
     csz = trim(cmsn.city) + ", " + trim(cmsn.state) + " " + trim(cmsn.zipcd).
     export stream outStream delimiter "~~"   
     sasp.cmmaillbl format "x(12)"            
     arsc.zipcd     format "x(12)"            
     "delptzip + chksum" format "x(14)"       
     cmsp.name      format "x(30)"            
     cmsn.addr[1]   format "x(30)"            
     cmsn.addr[2]   format "x(30)"            
     csz            format "x(30)"           
     cmsn.name      format "x(30)"
     cmsn.cotitle   format "x(15)"
     cmsn.groupcd   format "x(8)"
     cmsn.comment   format "x(20)".
     lblCnt = lblCnt + 1.
   end.
   when "Bin" then do:  /* changed to reflect wmsb and wmsbp */
     if avail wmsbp then do:
       lblprod = wmsbp.prod.
       find icsp where icsp.cono = g-cono and icsp.prod = wmsbp.prod
         no-lock no-error.
       if avail icsp then do:  
         proddescrip = TRIM( icsp.descrip[1] ).
         proddescrip = proddescrip + " " + icsp.descrip[2]. 
         proddescrip = TRIM( proddescrip ).  
         lblunitstock = icsp.unitstock.
         lblunitsell  = icsp.unitsell.
         lblunitcnt   = icsp.unitcnt.
         lblweight    = icsp.weight.
       end.
     end. /* wmsbp */
     export stream outStream delimiter "~~"
     labelnm     format "x(12)"
     lblprod     format "x(12)"
     wmsb.descrip   format "x(24)"
     proddescrip    format "x(24)"
     string(wmsb.binloc,"xx/xx/xxx/xxx")
     ""
     lblunitstock format "x(4)"
     lblunitsell  format "x(4)"
     lblunitcnt   format "x(4)"
     lblweight    format "x(9)"
     lblprod     format "x(24)"
     wmsb.binloc format "x(13)"
     "".            
     lblCnt = lblCnt + 1.
   end.
   when "Pick" then do:
     /* could use WT instead of OE for com etc. */
     do indx = 1 to 50:
       data[indx] = "".
     end.              
     if avail oeeh then do:
       data[1]  = labelnm.
       data[2]  = STRING(oeeh.orderno,">>>>>>>>9").
       data[3]  = STRING(oeeh.ordersuf,"99").
       data[4]  = STRING(oeeh.custno,">>>>>>>>>>>9").
       data[5]  = oeeh.custpo.
       data[7]  = oeeh.shiptonm.
       data[8]  = oeeh.shiptoaddr[1].
       data[9]  = oeeh.shiptoaddr[2].
       data[10] = oeeh.shiptocity.
       data[11] = oeeh.shiptost.
       data[12] = oeeh.shiptozip.
       data[13] = oeeh.shiptocity + "," + oeeh.shiptost + " " + oeeh.shiptozip.
       data[14] = oeeh.shipviaty.
       data[15] = oeeh.stagearea.
       data[16] = oeeh.user1.
       data[17] = oeeh.user2.
     end.
     if avail oeel then do:
       data[18] = oeel.binloc.
       data[19] = STRING(oeel.weight,">>>>>>>>>9").
       data[20] = STRING(oeel.cubes,">>>>>>>>>9").
       data[21] = oeel.jobno.
       data[22] = STRING(oeel.lineno,">9").
       data[23] = oeel.shipprod.
       data[24] = oeel.reqprod.
       data[25] = oeel.proddesc.
       data[26] = STRING(oeel.qtyord,">>>>>>>>>9").
       data[27] = STRING(oeel.qtyship,">>>>>>>>9").
       data[28] = oeel.unit.
       data[29] = oeel.user1.
       data[30] = oeel.user2.
       data[31] = oeel.user3.
       data[32] = STRING(oeel.user6,">>>>>>>>>9").
       data[33] = STRING(oeel.user8,">>>>>>>>>9").
       /* data[44] = STRING((oeel.qtyship mod s-qtyctn), ">>>>>>>>>9"). */
     end.
     find com where com.cono = g-cono and
     com.comtype = "oe" and
     com.printfl = yes and
     com.orderno = oeel.orderno and
     com.ordersuf = oeel.ordersuf and com.lineno = oeel.lineno
     and com.transproc ne ? no-lock no-error.
     if avail com then do:
       data[34] = com.noteln[1].
       data[35] = com.noteln[2].
       data[36] = com.noteln[3].
       data[37] = com.noteln[4].
       data[38] = com.noteln[5].
       data[39] = com.noteln[6].
       data[40] = com.noteln[7].
       data[41] = com.noteln[8].
       data[42] = com.noteln[9].
       data[43] = com.noteln[10].
     end.
     export stream outStream delimiter "~~"       
       /* sasp.prodlbl */                           
       data[1]  format "x(11)"                      
       data[2]  format "x(7)"                      
       data[3]  format "x(2)"                      
       data[4]  format "x(12)"                      
       data[5]  format "x(12)"                      
       data[6]  format "x(30)"                      
       data[7]  format "x(30)"                      
       data[8]  format "x(30)"                      
       data[9]  format "x(30)"                      
       data[10] format "x(20)"     
       data[11] format "x(2)"     
       data[12] format "x(10)"     
       data[13] format "x(32)"     
       data[14] format "x(4)"      
       data[15] format "x(10)"      
       data[16] format "x(8)"      
       data[17] format "x(8)"     
       data[18] format "x(10)"     
       data[19] format "x(10)"     
       data[20] format "x(10)"     
       data[21] format "x(8)"     
       data[22] format "x(3)"    
       data[23] format "x(24)"
       data[24] format "x(24)"
       data[25] format "x(24)"
       data[26] format "x(10)"    
       data[27] format "x(10)"    
       data[28] format "x(4)"  
       data[29] format "x(78)"  
       data[30] format "x(78)"  
       string(today,"99/99/99") format "x(8)"   
       data[32] format "x(8)"   
       data[33] format "x(60)"  
       data[34] format "x(60)"  
       data[35] format "x(60)" 
       data[36] format "x(60)"    
       data[37] format "x(60)"    
       data[38] format "x(60)"  
       data[39] format "x(60)"  
       data[40] format "x(3)"  
       data[41] format "x(4)"   
       data[42] format "x(18)"   
       data[43] format "x(18)"  
       data[44] format "x(4)".  
     lblcnt = lblcnt + 1.
   end. /* Pick */
   when "Product" then do:
     do indx = 1 to 50:
       data[indx] = "".
     end.
     data[1] = labelnm.
     data[2]  = inVal[1].     
     data[4]  = inVal[1].         
       
     if avail icsp then do:
       data[2]  = icsp.descr[1].   
       data[3]  = icsp.descr[2].
       data[4]  = icsp.prod.
       data[17] = STRING(icsp.weight,">>>>9.99").
       data[18] = STRING(icsp.cubes,">>>>9.99").
       data[22] = icsp.user1.
       data[23] = icsp.user2.
     end.
     else do:
       if avail poel then do:
         data[2] = poel.proddesc.
         data[3] = poel.proddesc2.
         data[4] = poel.shipprod.
       end.
       else do:
         find first icenh use-index k-icenh where icenh.cono = g-cono 
         and icenh.typecd = "N"                                       
         and icenh.whse = inVal[2]                                     
         and icenh.prod = inVal[1]                                     
         no-lock no-error.                                            
                                                                      
         if avail icenh then do:                                      
           data[2] = icenh.descrip[1].                           
           data[3] = icenh.descrip[2].                           
           data[4] = inVal[1].
         end.
       end.
     end.
     data[14] = inVal[3].
     data[15] = inVal[4].
     data[16] = inVal[5].
     if avail poelb then do:
       data[4] = poelb.shipprod.
       data[14] = string(poelb.pono,">>>>>>>>>>9").
       data[15] = STRING(poelb.posuf,"99").        
       data[16] = STRING(poelb.lineno,">>99").     
     end.
     if avail icsw then do:
       data[5]  = icsw.binloc1.
       data[6]  = icsw.binloc2.
       data[7]  = icsw.vendprod.
       data[24] = icsw.user1.
       data[25] = icsw.user2.
     end.      
     
     if avail icsp and avail icsw then 
     find poei where poei.cono = icsw.cono and
     poei.pono = decimal(inVal[3]) and poei.vendprod = icsw.vendprod
     no-lock no-error.
     
     if avail poei then do:
       data[14] = string(poei.pono,">>>>>>>>>>9").
       data[15] = STRING(poei.posuf,"99").
       data[16] = STRING(poei.lineno,">>99").
     end.
     if avail icsw then do:
       if avail poei then do:
         find icsv where icsv.cono = poei.cono and icsw.prod = icsw.prod
         and icsv.vendno = icsw.arpvendno no-lock no-error.
       end.
       if avail icsv then do:
         data[8]  = string(icsv.section1,">>>>>>>>>>>9").
         data[9]  = STRING(icsv.section2,">>>>>>>>>>>9").
         data[10] = STRING(icsv.section3,">>>>>>>>>>>9").
         data[11] = STRING(icsv.section4,">>>>>>>>>>>9").
         data[12] = STRING(icsv.section5,">>>>>>>>>>>9").
         data[13] = STRING(icsv.section6,">>>>>>>>>>>9").
       end.
       data[19] = STRING(icsw.arpvendno,">>>>>>>>>>>9").
       find apsv where apsv.cono = icsw.cono and apsv.vendno = icsw.arpvendno
       no-lock no-error.
       if avail apsv then data[20] = apsv.name.
       find poelo where poelo.cono = poei.cono and poelo.pono = poei.pono
       and poelo.posuf = poei.posuf and poelo.seqno = poei.seqno and 
       poelo.ordertype = "o" no-lock no-error.
       if avail poelo then do:
         find icets where icets.cono = poelo.cono and icets.prod = icsw.prod
         and icets.whse = icsw.whse and icets.orderno = poelo.orderaltno
         and icets.ordersuf = poelo.orderaltsuf and
         icets.lineno = poelo.linealtno no-lock no-error.
         if avail icets then data[21] = icets.serialno.
         else do:
           find icetl where icetl.cono = poelo.cono and icetl.prod = icsw.prod
           and icetl.whse = icsw.whse and icetl.orderno = poelo.orderaltno
           and icetl.ordersuf = poelo.orderaltsuf and
           icetl.lineno = poelo.linealtno no-lock no-error.
             if avail icetl then data[21] = icetl.lotno.
         end.       
       end.
     end. /* avail icsw */
     do indx = 1 to num-entries(extraData):
       data[25 + indx] = entry(indx, extraData).
       if indx = 25 then leave. 
     end.

     export stream outStream delimiter "~~"    
     data[1]  format "x(11)"       
     data[2]  format "x(24)"
     data[3]  format "x(24)"
     data[4]  format "x(24)"
     data[5]  format "x(13)"
     data[6]  format "x(13)"
     data[7]  format "x(24)"  
     data[8]  format "x(12)"  
     data[9]  format "x(12)"  
     data[10] format "x(12)"  
     data[11] format "x(12)"  
     data[12] format "x(12)"  
     data[13] format "x(12)"  
     data[14] format "x(7)"  
     data[15] format "x(2)"  
     data[16] format "x(3)"  
     data[17] format "x(10)"  
     data[18] format "x(10)"  
     data[19] format "x(12)"  
     data[20] format "x(30)"  
     data[21] format "x(20)"  
     data[22] format "x(8)"   
     data[23] format "x(8)"   
     data[24] format "x(8)"   
     data[25] format "x(8)"
     /* extraData */
     data[26] format "x(24)"
     data[27] format "x(24)"
     data[28] format "x(24)"
     data[29] format "x(24)"   
     data[30] format "x(24)"
     string(today,"99/99/99") format "x(8)".   
                    
     lblCnt = lblCnt + 1.
   end.
   otherwise do:
     message "Unkown label type! What is this?". pause.
   end.
 end case.
end procedure.

procedure LooptheRecords:
  if labelty = "Bin" then do:
    if inVal[3] = "" then do:
      for each wmsb where wmsb.cono = g-cono + 500 and wmsb.whse =         
      inVal[1] and wmsb.binloc >= inVal[2] and wmsb.binloc <= inVal[4] 
      no-lock:
        run PrinttheLabel.
      end.
    end.
    else do:
      for each wmsb where wmsb.cono = g-cono + 500 and wmsb.whse =          
      inVal[1] and wmsb.binloc >= inVal[2] and wmsb.binloc <= inVal[4]      
      no-lock:                                                              
        find wmsbp where wmsbp.cono = wmsb.cono and wmsbp.whse = wmsb.whse
        and wmsbp.binloc = wmsb.binloc and wmsbp.prod = inVal[3] no-lock
        no-error.
          if avail wmsbp then run PrinttheLabel.                                       end.                                                                  
    end.
  end.                                                               
end procedure. /* Loop the records */

procedure FindtheRecords:
  def var iter as integer no-undo.
  do iter = 1 to 3:
    if lkTable[iter] ne "" then do:
      case lkTable[iter]:
        when "arsc" then do:
          find arsc where arsc.cono = g-cono and
            arsc.custno = decimal(inVal[iter]) no-lock no-error.
        end.
        when "cmsn" then do:
          find cmsn where cmsn.prosno = decimal(inVal[iter]) no-lock no-error.
          find cmsp where cmsp.prosno = decimal(inVal[iter]) no-lock no-error.
        end.
        when "icsd" then do:
          find icsd where icsd.cono = g-cono and icsd.whse = inVal[iter]
            no-lock no-error.
        end.
        when "icsp" then do:
          find icsp where icsp.cono = g-cono and icsp.prod = inVal[iter]
            no-lock no-error.
        end.
        when "icsw" then do:
          find icsw where icsw.cono = g-cono and icsw.whse = inVal[iter]
          and icsw.prod = inVal[iter - 1] no-lock no-error.
        end.
        when "oeeh" then do:
          find oeeh use-index k-oeeh where oeeh.cono = g-cono and
          oeeh.orderno = integer(inVal[iter - 1])
          and oeeh.ordersuf = integer(inVal[iter]) no-lock no-error.
        end.
        when "oeel" then do:
          find oeel use-index k-oeel where oeel.cono = oeeh.cono
          and oeel.orderno = oeeh.orderno
          and oeel.ordersuf = oeeh.ordersuf
          and oeel.lineno = integer(inVal[iter]) no-lock no-error.
        end.
        when "poei" then do:
          /* is a really big mess of required variables */
          if inVal[3] ne "" and inVal[4] ne "" and inVal[5] ne "" then 
          find poel where poel.cono = g-cono and 
          poel.pono = integer(inVal[3]) and 
          poel.posuf = integer(inVal[4]) and
          poel.lineno = integer(inVal[5])
          no-lock no-error.                      
        end.
        /** Not sure I need this **/
        when "poeb" then do:
          if inVal[3] ne "" and inVal[4] ne "" and inVal[5] ne "" then
          find poelb where poelb.cono = g-cono and
          poelb.pono  = integer(inVal[3]) and
          poelb.posuf = integer(inVal[4]) and
          poelb.lineno = integer(inVal[5])
          no-lock no-error.
        end.
        when "wmsb" then do:
          find wmsb where wmsb.cono = g-cono + 500 and wmsb.whse =   
            inVal[iter - 1] and wmsb.binloc = inVal[iter] no-lock no-error.
        end.
        when "wmsbp" then do:
          find wmsbp where wmsbp.cono = wmsb.cono and wmsbp.whse = wmsb.whse
          and wmsbp.prod = inVal[iter] no-lock no-error.
        end.
      end case.
    end.
  end.
end procedure.


procedure FillLabelTypes:
  /*
  for each labeltypes:
    delete labeltypes.
  end.
  */
  empty temp-table labeltypes.
  if not avail sasp then do:
    find sasp where sasp.printernm = printernm no-lock no-error.       
    if not avail(sasp) then return.                                        
  end.
  if sasp.armaillbl ne "" then do:
    create labeltypes.
    labeltypes.labelname = sasp.armaillbl.  
    labeltypes.labeltype = "AR Mailing". 
  end.
  if sasp.cmmaillbl ne "" then do:                 
    create labeltypes.                          
    labeltypes.labelname = sasp.cmmaillbl.      
    labeltypes.labeltype = "CM Mailing".        
  end.                                          
  if sasp.binloclbl ne "" then do:                 
    create labeltypes.                          
    labeltypes.labelname = sasp.binloclbl.      
    labeltypes.labeltype = "Bin".        
  end.                                          
  if sasp.prodlbl ne "" then do:                 
    create labeltypes.                          
    labeltypes.labelname = sasp.prodlbl.      
    labeltypes.labeltype = "Product".        
  end.                                          
  if sasp.picklbl ne "" then do:                 
    create labeltypes.                          
    labeltypes.labelname = sasp.picklbl.      
    labeltypes.labeltype = "Pick".        
  end.                                          
  /* Carton Marking - use Pick? */            
  if sasp.shiplbl ne "" then do:                 
    create labeltypes.                          
    labeltypes.labelname = sasp.shiplbl.      
    labeltypes.labeltype = "Shipping".        
  end.                                          
end procedure.


procedure setupbyType:
  def var iter as integer no-undo.
  def var displayFlag as logical initial no no-undo.
  preTitle[4] = "".
  preTitle[5] = "".
  case labelty:
    when "AR Mailing" then do:
      lkTable[1] = "arsc".
      lkTable[2] = "".
      lkTable[3] = "".
      preTitle[1] = "Cust:". /* g-custno */
    end.
    when "CM Mailing" then do:
      lkTable[1] = "cmsn".
      lkTable[2] = "arsc".  /* was cmsp */
      lkTable[3] = "".
      preTitle[1] = "Prospct:". /* g-prosno */
      preTitle[2] = "Cust:".
    end.
    when "Bin" then do:
      lkTable[1] = "".    
      lkTable[2] = "wmsb".  /* "icsw". */
      lkTable[3] = "wmsbp". /* "icsp". */
      preTitle[1] = "Whse:".
      preTitle[2] = "Bin:".
      preTitle[3] = "Prod:". /* Prod */
      preTitle[4] = "LastBin:".
    end.
    when "Product" then do:
      lkTable[1] = "icsp".
      lkTable[2] = "icsw".
      lkTable[3] = "poei".
      preTitle[1] = "Prod:".
      preTitle[2] = "Whse:".
      preTitle[3] = "PO:".
      preTitle[4] = "PO Suf:".
      preTitle[5] = "LineNo:".
    end.
    when "AR Mailing" then do:
      lkTable[1] = "".   
      lkTable[2] = "oeeh".
      lkTable[3] = "oeel".
      preTitle[1] = "OrdNo:".
      preTitle[2] = "OrdSuf:".
      preTitle[3] = "LineNo:".
    end.
    when "Pick" then do:
      lkTable[1] = "".           
      lkTable[2] = "oeeh".       
      lkTable[3] = "oeel".       
      preTitle[1] = "OrdNo:". 
      preTitle[2] = "OrdSuf:".
      preTitle[3] = "LineNo:".  
    end.
    when "Shipping" then do:
      lkTable[1] = "icsd".
      lkTable[2] = "".
      lkTable[3] = "oeeh".
      preTitle[1] = "Whse:".
      preTitle[2] = "OrdNo:".
      preTitle[3] = "OrdSuf:".
    end.
    otherwise do:
      message "Label type " labelty " not found".
      pause.
    end.
  end case.
  if inHand then
      display
        preTitle[1] preTitle[2] preTitle[3] preTitle[4] preTitle[5]
        inVal[1] inVal[2] inVal[3] inVal[4] inVal[5]
        numCopies with frame f-hhprintlabel.
  else
     display
       preTitle[1] preTitle[2] preTitle[3] preTitle[4] preTitle[5]
       inVal[1] inVal[2] inVal[3] inVal[4] inVal[5]
       numCopies with frame f-printlabel.
end procedure.

procedure updateLabelName:
if {k-cancel.i} then do:
  printErrOut = yes.
  validInfo = no.
  exitLoop = yes.
end.
if {k-func12.i} and frame-field = "labelnm" then do:
  open query q-lbltype preselect each labeltypes.       
  find first labeltypes no-error.
  if avail(labeltypes) then do:
    enable b-labeltype with frame f-browselabels. 
    apply "ENTRY" to b-labeltype in frame f-browselabels.
    wait-for window-close of frame f-browselabels. 
  end.
  else 
  message "No labels found for this printer" view-as alert-box.
end.
end procedure.


procedure updateVals:
define input parameter inNumCopies as int no-undo.
if {k-cancel.i} then do:
  printErrOut = yes.
  exitLoop = yes.
  validInfo = no.
end.  
if frame-field = "numCopies" and ({k-after.i} or {k-accept.i}) then do:
  if inNumCopies < 1 or inNumCopies = ? then do:
    message "numCopies must be at least 1 to print".
    if inHand then
      next-prompt numCopies with frame f-hhprintlabel.
    else
      next-prompt numCopies with frame f-printlabel.
    exitLoop = no.
    validInfo = no.
  end.
  else do:
    validInfo = yes.
    exitLoop = yes.
  end.
end.
end procedure.
