/*  pdzp.p
    pdsr expiration report - pdsr's expiring with 30 days.
    SPA pricing report worksheet.
*/


{p-rptbeg.i}

def var cntz          as integer.
def var v-i           as integer                    no-undo.
def var n-type        as c   format "x"             no-undo.
def var n-title       as c   format "x(23)"         no-undo.
def var n-name        as c   format "x(20)"         no-undo.
def var n-yr          as i   format "99"            no-undo.
def var n-sls         as de  format ">>>>>>>-"      no-undo.
def var n-ct          as i   format "zzzzzzz99999"  no-undo.
def var n-vend        like pdsr.vendno              no-undo.
def var v-pricetypeb  like arsc.pricetype       no-undo.
def var v-pricetypee  like arsc.pricetype       no-undo.
def var v-prodprctpb  as character format "x(4)" no-undo.
def var v-prodprctpe  as character format "x(4)" no-undo.
def var v-prodprcatpb as character format "x(4)" no-undo.
def var v-prodprcatpe as character format "x(4)" no-undo.
def var v-whsebg      like icsw.whse no-undo.
def var v-whseed      like icsw.whse no-undo.
def var v-vendbg      like icsw.arpvendno no-undo.
def var v-vended      like icsw.arpvendno no-undo.
def var v-custbg      like arsc.custno no-undo.
def var v-custed      like arsc.custno no-undo.
def var v-ptybg       as char no-undo.
def var v-ptyed       as char no-undo.
def var v-prodbg      as char no-undo.
def var v-proded      as char no-undo.
def var n-newsort     as logical no-undo.
def var p-sortcd      as char no-undo.
def var n-qtybrk      as integer format "zzzzzz9" no-undo.
def var n-mult        as character format "x" no-undo.
def var cat-sw        as logical init "no" no-undo. 
def var typ-sw        as logical init "no"  no-undo. 

form header
   "Date: " today "Time: " string(time,"HH:MM:SS")
   "ICSW - Rebate Verification Worksheet" at 75
   "Operator: " at 143
   g-operinits  at 153
   "Page " at 162 page-number format ">>9" 
    skip(1)
    "Whse"             at 1
    "Product"          at 6
    "Vendor Product"   at 31
    "Description"      at 56
    "Prod Cat"         at 77
    "ProdLn"           at 86
    "PrcTyp"           at 93
    "Vendor No"        at 100
    "ArpTy"            at 110
    "ArpWhse"          at 116
    "RebateTyp"        at 124
with frame f-hd width 180 page-top.

form
    icsw.whse         at 1 
    icsw.prod         at 6
    icsw.vendprod     at 31
    icsp.descrip[1]   at 56
    icsp.prodcat      at 81 
    icsw.prodline     at 86
    icsw.pricetype    at 93
    icsw.arpvendno    at 98
    icsw.arptype      at 112
    icsw.arpwhse      at 116
    icsw.rebatety     at 124
with frame f-icsw  width 200 no-box no-labels.

if sapb.rangebeg[1] ne "" then
   v-whsebg = sapb.rangebeg[1].
else 
   v-whsebg = " ".
if sapb.rangeend[1] ne "" then
   v-whseed = sapb.rangeend[1].
else 
   v-whseed = "zzzz".
  
if sapb.rangebeg[2] ne "    " then
  do:
   typ-sw = yes.
   If sapb.rangebeg[2] = "blnk" then
     v-prodprctpb = "    ".
   else
     v-prodprctpb = sapb.rangebeg[2].
  end.
else             
   v-prodprctpb = "    ".
if sapb.rangeend[2] begins "~~~~" then
   v-prodprctpe = "````".
else
  do:
  typ-sw = yes.
  If sapb.rangeend[2] = "blnk" then
     v-prodprctpe = "    ".
  else
     v-prodprctpe = sapb.rangeend[2].
  end.

if sapb.rangebeg[3] ne "    " then
  do:
   cat-sw = yes.
   v-prodprcatpb = sapb.rangebeg[3].
  end. 
else
   v-prodprcatpb = "    ".
if sapb.rangeend[3] begins "~~~~" then
   v-prodprcatpe = "````".
else 
  do: 
   cat-sw = yes.
   v-prodprcatpe = sapb.rangeend[3].
  end. 

 if  v-prodprctpe begins "~~~~~~~~" then
   v-prodprctpe = "````".
 if v-prodprcatpe begins "~~~~~~~~" then
   v-prodprcatpe = "````".

 assign p-sortcd      = sapb.optvalue[1]
        v-ptybg       = sapb.rangebeg[4]
        v-ptyed       = sapb.rangeend[4]
        v-prodbg      = sapb.rangebeg[5]
        v-proded      = sapb.rangeend[5].



put control "~033~046~1541~117".   /* landscape format */
view frame f-hd.

if p-sortcd = "1" or p-sortcd = " " then
  do for icsw: 
   icswloop:
  for each icsd where icsd.cono = g-cono and
             (icsd.whse >= v-whsebg and icsd.whse <= v-whseed) no-lock, 
     each icsw use-index k-whse where
           icsw.cono =  g-cono and 
           icsw.whse = icsd.whse and
           (icsw.prod ge v-prodbg and
            icsw.prod le v-proded) and  
           (icsw.pricety ge v-ptybg and
            icsw.pricety le v-ptyed) and  
          (icsw.rebatety >= v-prodprctpb and 
           icsw.rebatety <= v-prodprctpe)  
           no-lock
       break by icsw.prod
             by icsw.pricety:
     find first icsp where
                icsp.cono = g-cono and 
                icsp.prod = icsw.prod
          no-lock no-error.
      if avail icsp and
        (icsp.prodcat < v-prodprcatpb or 
         icsp.prodcat > v-prodprcatpe) then 
        next.

      display icsw.whse       
              icsw.prod       
              icsw.vendprod   
              icsp.descrip[1] 
              icsp.prodcat    
              icsw.prodline   
              icsw.pricetype  
              icsw.arpvendno  
              icsw.arptype    
              icsw.arpwhse    
              icsw.rebatety
        with frame f-icsw.
            down with frame f-icsw. 
    end. /* for each icsw */      
  leave.
  end. /* do for icsw */
else   
if p-sortcd = "2" then
  do for icsw: 
   icswloop:
  for each icsd where icsd.cono = g-cono and
             (icsd.whse >= v-whsebg and icsd.whse <= v-whseed) no-lock, 
     each icsw use-index k-whse where
           icsw.cono =  g-cono and 
           icsw.whse = icsd.whse and
           (icsw.prod ge v-prodbg and
            icsw.prod le v-proded) and  
           (icsw.pricety ge v-ptybg and
            icsw.pricety le v-ptyed) and  
          (icsw.rebatety >= v-prodprctpb and 
           icsw.rebatety <= v-prodprctpe)  
           no-lock
       break by icsw.whse
             by icsw.prod
             by icsw.pricety:
     find first icsp where
                icsp.cono = g-cono and 
                icsp.prod = icsw.prod
          no-lock no-error.
      if avail icsp and
        (icsp.prodcat < v-prodprcatpb or 
         icsp.prodcat > v-prodprcatpe) then 
        next.

      display icsw.whse       
              icsw.prod       
              icsw.vendprod   
              icsp.descrip[1] 
              icsp.prodcat    
              icsw.prodline   
              icsw.pricetype  
              icsw.arpvendno  
              icsw.arptype    
              icsw.arpwhse    
              icsw.rebatety
        with frame f-icsw.
            down with frame f-icsw. 
    end. /* for each icsw */      
  leave.
  end. /* do for icsw */

