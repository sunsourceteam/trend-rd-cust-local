/***************************************************************************
 PROGRAM NAME: dsi-schroedr.p
  DESCRIPTION: This program will create the DSI file to upload to MNet's
                 Schroeder network. (SCHROEDER DSI EXTRACT)
 DATE WRITTEN:
       AUTHOR: Dave Sivak
     MODIFIED: 10/14/99 - Carl Graul - changed the formatting in the output
                    file
             : 11/12/99 - Carl Graul added "," to .csv records
             : 11/22/99 - Carl Graul - default distributor to SUNSRCE"
                          until mnet is fixed             
***************************************************************************/

def shared var fl-done as logical.
/* def var dmon as c format "x(2)".            MONTH       USED   */
/* def var dday as c format "x(2)".            DAY         FOR    */
/* def var dyr as c format  "x(2)".            YEAR        SCREEN */
/* def var dyear as c format "x(4)".                       INPUT  */
def var wprod as c format "x(25)".
def var quantity as i format "9999".
def var distributor as c format "x(8)".
def var discount as i format "99".
def var price as c format "x(1)".
def var todays_date as date format "99/99/99".
def var wdate as c format "x(8)" init "  /  /  ".
def var assembly as c format "x(11)".
def var count as i format "999".
def var vendornum as i format "999999999999".
def var warehouse as c format "x(4)".
def var distribcode as c format "x(8)".

/* display "Please enter today's date:".                 USED FOR   */
/* set todays_date.                                   SCREEN INPUT  */


def var out-csv as char format "x(60)" no-undo.
def var out-aer as char format "x(60)" no-undo.
assign out-csv = os-getenv("HOME") + "/schroeder.csv".
assign out-aer = os-getenv("HOME") + "/sunsrce.sch".

def stream str1.

output stream str1 to value(out-csv).
run "120-load-csv".
output stream str1 close.

input from value(out-csv).
output stream str1 to value(out-aer).


/* dday = string(day(todays_date),"99").                    USED    */
/* dmon = string(month(todays_date),"99").                  FOR     */
/* dyear = string(year(todays_date),"9999").                SCREEN  */
/* dyr = substring(dyear,3,2).                              INPUT   */
/* substring(wdate,1,2) = dmon.                                     */
/* substring(wdate,4,2) = dday.                                     */
/* substring(wdate,7,2) = dyr.                                      */

assign todays_date = TODAY.

repeat:
  import delimiter ","
     vendornum warehouse distribcode.

  for each icsw use-index k-vendor where icsw.cono = 1 and 
                                         icsw.whse = warehouse and
                                         icsw.arpvendno = vendornum  no-lock.
                     
      quantity = (qtyonhand - qtyreservd - qtycommit) - (linept + ordqtyin).
      if quantity > 0 then do:
         distributor = distribcode.
/* until distributor address is fixed at mnet */
         distributor = "SUNSRCE".         
         
         discount = 28.
         wprod = icsw.prod.
         run "110-write".
       end. /* if quantity > 0 */
   end. /* for each icsw */
end. /* repeat */
input close.
output  stream str1 close.
assign fl-done = yes.
                                           
procedure 110-write:
put stream str1 UNFORMATTED
    trim(wprod) "|" 
    trim(distributor) "|" 
    quantity "|" 
    discount "|"
    trim(price) "|" 
    todays_date
    skip.
end. /* 110-write */


procedure 120-load-csv:
put stream str1 UNFORMATTED 
    "9733060,dros,SUNCHI,"
    skip
    "9771000,dros,SUNCHI,".
end procedure. /* 120-load-csv */

         



