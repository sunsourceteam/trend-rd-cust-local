/* z-popsao.i 1.1 6/12/92 */
/*h*****************************************************************************
  INCLUDE              : z-popsao.i
  DESCRIPTION          : Custom Assignments for Pop-Up Window on Setups
  AUTHOR               : enp
  DATE LAST MODIFIED   : 04/22/92
  CHANGES MADE AND DATE:
*******************************************************************************/
def buffer z-sasoo for sasoo.

form 
    "Allowed Stored Job Access Yes/No? "  at row 2 col 22   
     z-sasoo.xxl4                         at row 2 col 56
     with frame f-sasoo-z no-labels width 80
          title "Extended Security Update". 


on ctrl-o anywhere
   do:
     if v-oper2 ne "" then 
       do:  
        find z-sasoo use-index k-sasoo where
             z-sasoo.cono = g-cono and 
             z-sasoo.oper2 = v-oper2 exclusive-lock no-error.
        if not avail z-sasoo then 
           do: 
             run err.p(1108).
             leave.
           end.
       end.
     else 
       do:
          hide frame f-sasoo.
          display z-sasoo.xxl4 with frame f-sasoo-z.
       end.
     if g-secure ge 3 then
      do:
      sasoo-updt:
       do while true with frame f-sasoo-z on endkey undo sasoo-updt,
                                                   leave sasoo-updt:
         update z-sasoo.xxl4.
         if {k-accept.i} then
            leave sasoo-updt.
       end.
      end.
      hide frame f-sasoo-z.
   end.
