/*h*****************************************************************************
  INCLUDE      : p-vaepq9f.i
  DESCRIPTION  : va quote -  main print logic
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
  ******************************************************************************/

/*tb  7241 (5.1) 02/27/97 jkp; Added call to speccost.gva. */
{speccost.gva}
{addon-tt.i &new = "new"
            &shared = "shared"}

/*tb  5929 06/18/92 mkb */
def buffer            b-oeeh   for oeeh.

form header  /* main title lines  */
    skip(2)
    s-lit1aa as c format "x(10)"         at 2
    s-lit1a  as c format "x(50)"         at 12
    s-cancel as c format "x(25)"         at 64
    s-lit1b  as c format "x(37)"         at 93
    s-lit2a  as c format "x(53)"         at 2
/*    b-sasc.upcvno                        at 95    ITB# 1 */
/*    oeeh.invoicedt                       at 108   ITB# 1 */
    oeeh.orderno                         at 123
    "-"                                  at 130
    oeeh.ordersuf                        at 131
    s-lit3a as c format "x(09)"          at 2
    oeeh.custno                          at 12
    s-lit3b as c format "x(39)"          at 94
    oeeh.enterdt                         at 94
    oeeh.custpo                          at 104
    page-num - v-pageno format "zzz9"    at 129
    skip(1)
    s-lit6a as c format "x(8)"           at 2
    s-billname like arsc.name            at 12
    s-lit6b as c format "x(18)"          at 48
/*      b-sasc.conm                          at 68  ITB# 2 */
    s-remitnm as c format "x(24)"        at 68   /* ITB# 2 */
    s-billaddr1 like arsc.name           at 12
/*    b-sasc.addr[1]                       at 68    ITB# 2 */
    s-lit6bb as c format "x(6)"          at 48   /* ITB# 2 */
    s-remit-addr1 as c format "x(24)"    at 68   /* ITB# 2 */ 
    s-billaddr2 like arsc.name           at 12
/*    b-sasc.addr[2]                       at 68    ITB# 2 */
    s-remit-addr2 as c format "x(24)"    at 68   /* ITB# 2 */
    s-lit8a as c format "x(9)"           at 106
    s-pstlicense as c format "x(11)"     at 116
    s-billcity as c format "x(35)"       at 12
    s-corrcity as c format "x(35)"       at 68
    s-lit9a as c format "x(9)"           at 106
    s-gstregno as c format "x(10)"       at 116
    skip(1)
    s-lit11a as c format "x(8)"          at 2
    s-shiptonm like oeeh.shiptonm        at 12
    s-lit11b as c format "x(12)"         at 50
    s-shiptoaddr[1]                      at 12
    oeeh.shipinstr                       at 50
    s-shiptoaddr[2]                      at 12
    s-lit12a as c format "x(10)"         at 50
    s-lit12b as c format "x(3)"          at 82
    s-lit12c as c format "x(7)"          at 104
    s-lit12d as c format "x(5)"          at 117
    s-shipcity as c format "x(35)"       at 12
    s-whsedesc as c format "x(30)"       at 50
    s-shipvia as c format "x(12)"        at 82
    s-cod as c format "x(6)"             at 95
    s-shipdt like oeeh.shipdt            at 104
    s-terms as c format "x(12)"          at 117
    skip(1)
    s-rpttitle like sapb.rpttitle        at 1 skip(1)
with frame f-head no-box no-labels width 132 page-top.

form header  /* detail line title */
    s-lit14a as c format "x(75)"         at 1
    s-lit14b as c format "x(53)"         at 76
    s-lit15a as c format "x(75)"         at 1
    s-lit15b as c format "x(53)"         at 76
    s-lit16a as c format "x(78)"         at 1
    s-lit16b as c format "x(53)"         at 79
with frame f-headl no-box no-labels width 132 page-top.

form  /* detail line totals  */
    skip(1)
    v-linecnt         format "zz9"       at 1
    s-lit40a     as c format "x(11)"     at 5
    s-lit40c     as c format "x(17)"     at 44
    s-totqtyshp  as c format "x(13)"     at 62
    s-lit40d     as c format "x(18)"     at 82
    s-totlineamt as c format "x(13)"     at 117
with frame f-tot1 no-box no-labels no-underline width 132.

form  /* all extra costs display  */
    s-title2 as c format "x(33)"           at 82
    s-amt2   as dec format "zzzzzzzz9.99-" at 117
with frame f-tot2 no-box no-labels width 132 no-underline down.

form  /* total invoice amount */
    s-invtitle as c format "x(18)"         at 82
    s-totinvamt like oeeh.totinvamt        at 117
with frame f-tot3 no-box no-labels no-underline width 132.

form header  /* footter at last page */
     " "                                      at 1                            
     " "                                      at 1                            
    s-lit48a as c format "x(9)"               at 1
    s-CSR-title  as c format "x(25)"         at 20 
    s-contact    as c format "x(30)"         at 46 
    s-contlit-ph as c format "x(4)"          at 82 
    s-contact-ph as c format "(xxx)xxx-xxxx" at 87 
    s-contlit-fx as c format "x(4)"          at 102
    s-contact-fx as c format "(xxx)xxx-xxxx" at 107
with frame f-tot4 no-box no-labels width 132 page-bottom.

/*tb# 5929 06/18/92 mkb */
form
    "Full Amount Tendered For All Orders:"  at 5
    oeeh.tottendamt                         at 42
    "*** Back Order/Release Exists ***"     at 5
with frame f-tot5 no-box no-labels width 132.

assign
    v-pageno    = page-number - 1
    v-subnetamt = 0.

    {a-vaepq9f.i s-} 

    {p-oeord.i "oeeh."}    /*** v-totlineamt ***/


  {oe-t-addon.i &ordertype = ""oe""
                &orderno   = oeeh.orderno
                &ordersuf  = oeeh.ordersuf
                &lock      = "no-lock"}

if oeeh.transtype = "qu" then  
   s-lit1a = "Quote".

/*tb 5413 06/20/92 mwb */
assign
    s-billname   = if v-invtofl then oeeh.shiptonm
                   else arsc.name
    s-billaddr1  = if v-invtofl then oeeh.shiptoaddr[1]
                   else arsc.addr[1]
    s-billaddr2  = if v-invtofl then oeeh.shiptoaddr[2]
                   else arsc.addr[2]
    s-billcity   = if v-invtofl then
                       oeeh.shiptocity + ", " + oeeh.shiptost +
                       " " + oeeh.shiptozip
                   else arsc.city + ", " + arsc.state + " " + arsc.zipcd
/*    s-corrcity   = b-sasc.city + ", " + b-sasc.state + " " + b-sasc.zipcd */
    s-cod        = if oeeh.codfl = no then "" else "C.O.D."
    s-shipdt     = if oeeh.shipdt = ? then oeeh.pickeddt else oeeh.shipdt
    s-totlineamt = if oeeh.lumpbillfl then
                       string(oeeh.lumpbillamt,"zzzzzzzz9.99-")
                   else if oeeh.transtype = "rm" then
                       string((oeeh.totlineamt * -1),"zzzzzzzz9.99-")
                   else string(v-totlineamt,"zzzzzzzz9.99-")
    s-taxes      = oeeh.taxamt[1] + oeeh.taxamt[2] +
                   oeeh.taxamt[3] + oeeh.taxamt[4]
    s-dwnpmtamt  = if oeeh.transtype = "cs" then oeeh.tendamt
                   else if oeeh.dwnpmttype then oeeh.dwnpmtamt
                   else (oeeh.dwnpmtamt / 100) * oeeh.totinvamt
    s-dwnpmtamt  = s-dwnpmtamt + oeeh.writeoffamt
    v-linecnt    = 0
    s-shiptonm      = oeeh.shiptonm
    s-shiptoaddr[1] = oeeh.shiptoaddr[1]
    s-shiptoaddr[2] = oeeh.shiptoaddr[2]
    s-shipcity      = oeeh.shiptocity + ", " + oeeh.shiptost +
                      " " + oeeh.shiptozip
    s-gstregno      = if g-country = "ca" then b-sasc.fedtaxid else ""
    s-pstlicense    = if g-country = "ca" then oeeh.pstlicenseno
                      else "".
/*
{p-oeadfp.i}  /* load addon descriptions */
*/
{w-sasta.i "S" oeeh.shipvia no-lock}

{w-icsd.i oeeh.whse no-lock}

/* ITB# 2 */
assign s-corrcity = icsd.city + ", " + icsd.state + " " + icsd.zipcd
       s-remitnm = icsd.name
       s-remit-addr1 = icsd.addr[1]
       s-remit-addr2 = icsd.addr[2].

/*tb 8890 04/07/93 jlc; DO's print Drop Ship for Ship Point */
assign
    s-shipvia  = if avail sasta then sasta.descrip else oeeh.shipvia
    s-whsedesc = if oeeh.transtype = "do" then "** Drop Ship **"
                 else if avail icsd then icsd.name
                 else oeeh.whse.

{w-sasta.i "t" oeeh.termstype no-lock}

if oeeh.langcd <> "" then do:
    {w-sals.i oeeh.langcd "t" oeeh.termstype no-lock}
end.

assign
    s-terms    = if oeeh.langcd <> "" and avail sals then sals.descrip[1]
                 else if avail sasta then sasta.descrip
                 else oeeh.termstype
    s-rpttitle = if p-promofl then sapb.rpttitle else "".

view frame f-head.
if oeeh.transtype = "ra" then hide frame f-headl.
else view frame f-headl.

assign s-lit48a = "Continued".
view frame f-tot4.
    
find first oimsp where oimsp.person = oeeh.takenby no-lock no-error.
if avail(oimsp) then                                                
   assign s-CSR-title  = "Customer Service Contact:"                
          s-contact    = oimsp.name                                 
          s-contlit-ph = "TEL:"                                     
          s-contact-ph = oimsp.phoneno                              
          s-contlit-fx = "FAX:"                                     
          s-contact-fx = oimsp.faxphoneno.                          
else                                                                
   assign s-CSR-title  = ""                                         
          s-contact    = ""                                         
          s-contlit-ph = ""                                         
          s-contact-ph = ""                                         
          s-contlit-fx = ""                                         
          s-contact-fx = "".                                        
 
