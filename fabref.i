/*
define stream zout.

define {1} shared temp-table zsdirefty no-undo   
  field cono        like arsc.cono
  field keyno       as decimal
  field rectype     as character
  field reference   like icsp.prod
  field altdesc     as char format "x(20)"
  field operinit    like icsw.operinit
  field transdt     like icsw.transdt
  field transtm     like icsw.transtm
  field transproc   like icsw.transproc
  field user1       like oeel.user1
  field user2       like oeel.user2
  field user3       like oeel.user3
  field user4       like oeel.user4
  field user5       like oeel.user5
  field user6       like oeel.user6
  field user7       like oeel.user7
  field user8       like oeel.user8
  field user9       like oeel.user9 
  index k-keyno
        cono
        rectype
        keyno 
  index k-parent
        cono
        rectype
        reference.


define {1} shared temp-table zsdirefer no-undo   
  field cono        like arsc.cono
  field keyno       as decimal
  field parentkeyno as decimal
  field typekeyno   as decimal
  field rectype     as character
  field reference   like icsp.prod
  field prod        like icsp.prod
  field fentry       like icsp.prod
  field altdesc     as char format "x(20)"
  field tier        as integer 
  field fullpath    as character
  field operinit    like icsw.operinit
  field transdt     like icsw.transdt
  field transtm     like icsw.transtm
  field transproc   like icsw.transproc
  field user1       like oeel.user1
  field user2       like oeel.user2
  field user3       like oeel.user3
  field user4       like oeel.user4
  field user5       like oeel.user5
  field user6       like oeel.user6
  field user7       like oeel.user7
  field user8       like oeel.user8
  field user9       like oeel.user9 
  index k-keyno
        cono
        rectype
        keyno 
  index k-parent
        cono
        rectype
        typekeyno
        parentkeyno 
        tier
        reference
        prod
  index k-prod
        cono
        rectype
        prod.

if "{1}" = "NEW" then do:
  if search("/rd/cust/local/tomh/fabdemo/zsdirefer") <> ? then do:

  input stream zout from "/rd/cust/local/tomh/fabdemo/zsdirefer".
  repeat:
    create zsdirefer.
    import stream zout zsdirefer.
  end.
  end.
  
  input stream zout close.  
  if search ("/rd/cust/local/tomh/fabdemo/zsdirefty") <> ? then do:
  input stream zout from "/rd/cust/local/tomh/fabdemo/zsdirefty".
  repeat:
    create zsdirefty.
    import stream zout zsdirefty.
  end.

  input stream zout close.  
  end.
end.    
*/
                     
/*
procedure Export-Data:

  output stream zout to "/rd/cust/local/tomh/fabdemo/zsdirefer".
  for each zsdirefer where zsdirefer.cono <> 0:
    export stream zout zsdirefer.
  end.
 
  output stream zout close.  

  output stream zout to "/rd/cust/local/tomh/fabdemo/zsdirefty".
  for each zsdirefty where zsdirefty.cono <> 0:
    export stream zout zsdirefty.
  end.
   
  output stream zout close.  

end.  
*/ 


