/* oeepp1.z99 */
/*h*****************************************************************************
  INCLUDE      : oeepp1.z99
  DESCRIPTION  : User Include for pick ticket special print
  USED ONCE?   : yes
  AUTHOR       : pg
  DATE WRITTEN : 03/09/00
  CHANGES MADE :
  03/29/00 rgm; TB# 3-2  Add RxServer Interface to Standard

  Mode                    Description
  ------------------      ---------------------------------------------------
  &define_rxvars          m-oeepp1.i, before rxserver vars defined
  &before_headview        oeepp1h.las, before view of f-head
  &before_linedisplay     d-oeepp1.i, before line detail section
  &before_xref            d-oeepp1.i, before crossref prints
  &before_prodnotes       d-oeepp1.i, before product notes print

*******************************************************************************/

/* note that all of the hooks within this file are bracketed by this
   check to ensure that we are in rxserver mode -- so no code that might
      be added to a custom z99 will be included in standard format 1
*/

&if defined(define_rxvars) = 2 &then
&endif

&if defined(before_headview) = 2 &then
&endif

&if defined(before_linedisplay) = 2 &then
&endif

&if defined(before_xref) = 2 &then
&endif

&if defined(before_prodnotes) = 2 &then
&endif


&if defined(before_comp_prodnotes) = 2 &then
&endif



&if defined(before_complinedisplay) = 2 &then
&endif
