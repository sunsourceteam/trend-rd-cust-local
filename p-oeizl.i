/* p-oeizl.i 1.1 01/03/98 
SX 55 */
/* p-oeizl.i 1.1 2/16/92 */
/*  p-oeizl.i   insert to build global from frame value  */
assign
    g-orderno  = integer(substring(o-frame-value,1,8))
    g-ordersuf = integer(substring(o-frame-value,10,2)).

if substring(o-frame-value,9,1) = "-" then        
   do:
   {w-oeeh.i g-orderno g-ordersuf no-lock}
   assign v-xmode = "O".
/* TAH 082707 */

   if oeeh.stagecd <> 9 then
     assign v-ninesfl = false.
   else
     assign v-ninesfl = true.
/* TAH 082707 */

   end.

else
if substring(o-frame-value,9,1) = "c" then        
   do:
   assign v-xmode = "C".
   find oeehb where oeehb.cono = g-cono and
                    oeehb.batchnm = string(g-orderno,">>>>>>>9") and
                    oeehb.sourcepros = "ComQu" and
                    oeehb.seqno = 2 no-lock no-error.
/* TAH 082707 */
   if oeehb.stagecd <> 9 then
     assign v-ninesfl = false.
   else
     assign v-ninesfl = true.
/* TAH 082707 */

   
   end.


