/*h****************************************************************************
  INCLUDE      : u-aric.i
  DESCRIPTION  : Field list for ARIC
  USED ONCE?   : yes
  AUTHOR       : NxT
  DATE WRITTEN :
  CHANGES MADE :
    08/10/92 smb; TB# 7386 Changed to use variable in the place of periodbal,
       servchgbal and misccrbal for arao option special aging
    09/08/92 smb; TB# 7779 Added ability to show sc's and mc's as future
        invoice
    05/11/93 jrg; TB# 10557 Added fields to the screen
    10/19/93 enp; TB# 13130 Available Credit Does Not Clear
    05/11/99 cm;  TB# 5366  Add Ship to notes
    07/27/00 ct;  TB# e7852 A/R Multicurrency changes
    10/23/01 ajw; TB# e6997 Added Shipto Credit Limit
    07/16/03 sbr; TB# e16028 Ship to credit info
    01/11/05 mwb; TB# e20669 Credit Check - balances off shipto/customer.
******************************************************************************/

g-custno
arsc.notesfl
g-shipto
"" @ arss.notesfl
arss.notesfl when g-shipto <> "" and avail arss
s-phoneno
s-name
s-faxphoneno
s-addr[1]
s-terms
s-addr[2]
arsc.class
s-city
s-state
s-zipcd
arsc.custtype

/*tb e7852 07/28/00 ct; A/R Multicurrency Add Currency */
"  " @ s-currencyty
s-currencyty when s-currencyty <> ""

s-shiptocredlimlab
s-shiptocredlim

s-displaytxt

s-pertext
s-futbal
s-ordbal
s-periodbals[1]
arsc.futbal
s-periodbals[2]
s-periodbals[3]
s-periodbals[4]
arsc.lastpaydt
s-periodbals[5]
arsc.lastpayamt
s-credlim
s-arLim  /* SX55 */
"" @ s-insureamt
s-insureamt when s-insureamt ne 0

"" @ s-insureleft
s-insureleft when s-insureleft ne 0

"" @ s-credrem
s-credrem when s-credlim ne 0
s-msgtext[1]
arsc.salesytd
arsc.returnsytd

/*tb 10557 05/11/93 jrg; Added the following fields to the screen. */
s-totbal
s-totlbl
s-dsodays
s-TandCdt


arsc.avgpaydays
arsc.costytd
arsc.lastsaleamt
arsc.lastsaledt
arsc.laststmtbal
arsc.prstmtbal
/* arsc.downpayamt */
/* arsc.servchgytd */

/*tb e7852 07/28/00 ct; A/R Multicurrency Add Message */
s-message

/* arsc.discytd */
/* arsc.unearnedytd */
