/* xxxxlu.i 1.1 01/03/98 */
/* xxxxlu.i 1.20 09/29/19/94 */
/*h****************************************************************************
  PROCEDURE       : xxxxlu.i
  DESCRIPTION     : Generic Pop Up Lookup
  AUTHOR          : enp
  LAST MODIFIED   : 07/31/89
  CHANGES MADE    :
    08/24/90 -enp Added &com to comment out define stmt
    03/18/93 enp;  TB# 10278 Able to create a product from F12
    04/14/93 beth; TB# 10539 - Implemented F6 to change lookup year in glsalu.p.
        Defined s-yr in g-lu.i
    08/27/93 pjt; TB# 12698 Receive Progress error in lookup
    10/18/93 rhl; TB# 11860 F12 call to a setup from lookup bug
    01/31/94 dww; TB# 13927 Keyboard buffering Keyword lookups
    03/01/94 dww; TB# 15009 Cancelling from LU Setup aborts xxxxlu
    07/29/94 rs;  TB# 16251 Allow backspace when typing description
    08/01/94 dww; TB# 16264 Lookups "jump around" when typing.  Automatically
        popup a frame to allow entry of the text to start at.
    09/19/94 dww; TB# 16596 Progress V7 Buffers Space
    09/29/94 dww; TB# 16691 Add Notes Ability to Lookups
    10/11/94 rs;  TB# 13046 SASTT values description lookup
    05/22/96 klb; TB# 21131 Ensure function keys work under DOS
    06/12/96 dww; TB# 20566 Install code for E-Spot to work thru Intellex
******************************************************************************/

/*e This is the controller for standard lookups.

    Expected variables, includes:
        g-lu.i     - Shared vars for lookup.
        g-xxxxlu.i - Variables to save parameters for return from double
                     lookup.
        v-setup    - Variable to contain name of setup procedure.
        v-start    - Text var to receive typed input from user.

        This controller expects screen frame with the name f-lookup to
        be defined.

    Required parameters:
        &find      - Name of include containining find logic (n- or .lfi).
        &file      - Name of file records are retrieved from.
        &chfield   - Name of field in frame to have choose bar.
        &display   - Names of fields to be displayed.

    Optional parameters:
        &alpha     - Set to "no" if file keyed by numeric field.
        &global    - Logic to be executed on return or GO.
        &com       - Send comment string if v-clearfl already defined.
        &func      - Send asterisk to cause Fkeys to return to calling prog.
        &funcpr    - Special processing for function keys
        &dial      - Send asterisk to enable phone dialing on Ctrl-A (CM only).
        &pict      - Send asterisk to enable picture interface for IC.
        &go-on     - May send keys to be treated as GO from choose.
        &notes     - Send asterisk to enable Notes interface (requires
                        &notesfile to be sent)
        &notesfile - Send Filename of Notes Logic (requires &notes to be sent)
******************************************************************************/

    /*tb 16596 09/19/94 dww; Progress V7 Buffers a Space. Defined z-lastkey. */
    {&com}
    def var v-clearfl    as logical no-undo.
    def var z-loadcharfl as log     no-undo.
    def var z-lastkey    as i       no-undo.

    form skip (21)
    with frame f-z no-box overlay width 80 row 1.
    /{&com}* */

    /*d Initialize **/
    assign
        v-recid = ? 
        g-luvalue = "".

    clear frame f-lookup all.
    status default "Searching ...".
    pause 0.

    /*tb 12177 enp */
    /*tb 13927 01/31/94 dww; Keyboard buffering Keyword lookups-ICSW */
    /*d input clear. */
    do i = 1 to v-length:

        /*d* Find First **/
        if i = 1 then do:
            {{&find} first}.
            if (not avail {&file}) and v-start ne "" then do:
                {{&find} last "/*"}.
            end.
        end.

        /*d* Find Next **/
        else do:
            {{&find} next "/*"}.
        end.

        /*d* Not Avail **/
        if not avail {&file} then do:
            /* End of Display */
            run err.p(9013).
            leave.
        end.

        /*d* Avail **/
        else do:
            display {&display} with frame f-lookup.
            v-recid[frame-line(f-lookup)] = recid({&file}).
            down with frame f-lookup.
            /*tb 12177 enp */
            /*tb 13927 01/31/94 dww; Keyboard buffering Keyword lookups-ICSW */
            /* readkey pause 0.
            if {k-cancel.i} then leave. */
        end. /* record avail */
    end. /* i = 1 to v-length */

    /*d* Return to Top **/
    up (i - 1) with frame f-lookup.
    assign
        v-start     = if index(v-start,"*") = 0 then "" else v-start
        v-key       = ""
        v-clearfl   = yes.

    /*o************************ Main Display **********************************/
    dsply:

    repeat with frame f-lookup on endkey undo main, leave main
                               on error  undo main, leave main:

        /*tb 16264 08/01/94 dww; Lookups "jump around" when typing */
        /*tb 13927 01/31/94 dww; Keyboard buffering Keyword lookups-ICSW */
        /* input clear. */
        status default
            "Chars to Search, Arrows, Prev/Next Page, GO/RETURN to Select".

       {xxxxlu.z99 &b4choose = "*"
                   &file = {&file}
                   &prog = {&prog}} 
        
        /*tb 21131 05/22/96 klb; Add shift-f3 for DOS functionality */
        choose row {&chfield} keys v-key
            go-on(" " f5 f12 f11 f13 shift-f3 ctrl-p {&go-on}) no-error
            with frame f-lookup.
        color display normal {&chfield}.

        hide message no-pause.

        if {k-jump.i} then do:
            bell.
            next.
        end.

        /{&func}*/
            /*tb 13046 10/07/94 rs; SASTT values description lookup */
            {&funcpr}
            if {k-func.i} then return.
        /{&func}*/

        /{&dial}*/
        if {k-ctrla.i} then do:
            run value(v-dial)(frame-value).
            next.
        end.
        /{&dial}*/

        /{&pict}*/
        if {k-ctrlp.i} then do:
            run icpict.p(frame-value).
            next.
        end.
        /{&pict}*/

        /*tb 16691 09/29/94 dww; Add Notes Ability to Lookups */
        /{&notes}*/
            {{&notesfile}}
        /{&notes}*/

        if {k-return.i} and v-start ne "" then next main.
        else if {k-accept.i} or {k-return.i} then do:

            /*tb 12698 08/27/93 pjt; Receive Progress error in lookup */
            /*d Only load if called through f12 naturally */
            if g-forcelufl = no then do:
                {&global}.
            end.

            /*d Always load */
            g-luvalue = frame-value.
            leave main.

        end. /* accept or return */

        /*tb 10539 beth */
        if {k-func6.i} then do:
            confirm = yes.
            next main.
        end.

        /*o****************** Setup From Lookup ****************************/
        else if {k-func5.i} or {k-func12.i} then do:
            /*d Don't Allow Lookup Setup if Already in Lookup Mode */
            /*tb 10278 03/18/93 enp; Able to create a product from F12 F12 */
            if g-lkupfl = yes or g-oecustno ne {c-empty.i}
            then do:
                bell.
                next.
            end.

            if v-setup ne "" then do i = 1 to 2 on endkey undo, next main:
                if i = 2 then do:
                    if v-setup = "icsp" then v-setup = "icsw".
                    else if v-setup = "icseu" then v-setup = "sast".
                    else leave.
                end.

                /* Read SASSM and SASOS to get security (1-5) */
                {p-secure.i v-setup}

                if g-secure > 1 then do:

                    assign g-title    = "LU - " + sassm.frametitle
                           g-currproc = v-setup
                           g-ourproc  = v-setup
                           g-modulenm = substring(v-setup,1,2)
                           g-lkupfl   = yes.

                    pause 0 before-hide.
                    view frame f-z.
                    pause 0 before-hide.
                    status default.

                    /*tb 20566 06/12/96 dww; E-Spot thru Intellex */
                    {ivset.gpr}

                    /*tb 11860 10/18/93 rhl; F12 call to a setup from lookup */
                    run value(lc(sassm.callproc) + ".p").

                    /*tb 20566 06/12/96 dww; E-Spot thru Intellex */
                    v-gginfo = "".
                    {ivset.gpr}

                    /*tb 15009 03/01/94 dww; Cancelling from LU Setup
                         aborts xxxxlu */
                    input clear.
                    readkey pause 0.

                    status default
                "Chars to Search, Arrows, Prev/Next Page, GO/RETURN to Select".

                    hide frame f-z no-pause.
                    {g-xxxxlu.i "g" "sv" "/*"}

                    g-lkupfl = no.

                end. /* g-secure > 1 */

                else
                    /* Security Violation - Operator Access Denied */
                    run err.p(1100).

            end. /* v-setup ne "" */

            pause 0 before-hide.
            next main.
        end. /* setup from lookup */

        /*o************************ Page Up **********************************/
        else if {k-prevpg.i} or {k-scrlup.i} then do:

            status default "Searching ...".

            /*d* Find Top Record **/
            if v-recid[1] ne ? then
                find {&file} where recid({&file}) = v-recid[1] no-lock no-error.

            /*d* No Records Above Top **/
            if v-recid[1] = ? or not avail {&file} then do:

                /* There are No Records Prior to the First Displayed */
                run err.p(9014).

                v-key = "".
                next.
            end.

            /*d* Cycle Through Screen Fill **/
            do i = v-length to 1 by -1:

                {{&find} prev "/*"}

                /*d* Not Available **/
                if not avail {&file} then do:
                    /* End of Display */
                    run err.p(9013).
                    leave.
                end.

                /*d* Available **/
                else do:
                    if i = v-length then do:
                        clear frame f-lookup all no-pause.
                        down (v-length - 1) with frame f-lookup.
                        v-recid = ?.
                    end.

                    display {&display} with frame f-lookup.

                    v-recid[frame-line(f-lookup)] = recid({&file}).

                    if i ne 1 then up with frame f-lookup.

                    /*tb 13927 01/31/94 dww;
                         Keyboard buffering Keyword lookups-ICSW */
                    /* readkey pause 0.
                    if {k-cancel.i} then leave. */

                end. /* record avail */
            end. /* v-length to 1 by -1 */
            if frame-line(f-lookup) ne frame-down(f-lookup) then
            down
                frame-down(f-lookup) - frame-line(f-lookup)
            with frame f-lookup.
        end. /* prev page */

        /*o************************ Next Page ********************************/
        else if {k-nextpg.i} or {k-scrldn.i} then do:

            status default "Searching ...".

            /*d* Find Bottom Record **/
            if v-recid[v-length] ne ? then
                find {&file} where recid({&file}) = v-recid[v-length] no-lock                 no-error.

            if v-recid[v-length] = ? or not avail {&file} then do:
                v-key = "".
                /* There Are No Records After the Last One Displayed */
                run err.p(9015).
                next.
            end.

            /*d* Find & Display Records **/
            do i = 1 to v-length:
                {{&find} next "/*"}

                if not avail {&file} then do:
                    /* End of Display */
                    run err.p(9013).
                    leave.
                end.

                /*d* Available **/
                else do:
                    if i = 1 then do:
                        v-recid = ?.
                        clear frame f-lookup all no-pause.
                    end.

                    display {&display} with frame f-lookup.

                    v-recid[frame-line(f-lookup)] = recid({&file}).

                    if i ne v-length then down with frame f-lookup.

                    /*tb 12177 enp */
                    /*tb 13927 01/31/94 dww; Keyboard buffering
                         Keyword lookups-ICSW */
                    /* readkey pause 0.
                    if {k-cancel.i} then leave. */

                end. /* record avail */
            end. /* i = 1 to v-length */

            if frame-line(f-lookup) ne 1 then
            up
                frame-line(f-lookup) - 1
            with frame f-lookup.

        end. /* next page */

        /*o Load v-start from v-key when user has typed text */
        else do :

            /*tb 16596 09/19/94 dww; Progress V7 Buffers a Space */
            assign
                z-lastkey    = lastkey
                z-loadcharfl = yes.

            do on endkey undo, leave:
                update
                    v-start format "x(20)" label "Start at"
                with frame f-start side-labels overlay centered row 6
                xxeditloop: editing:

                    /* First pass thru the editing loop, load any characters
                        that were typed into the CHOOSE statement into the
                        screen editing buffer */
                    if z-loadcharfl = yes then do:

                        do i = 1 to length(v-key):
                            if "{&alpha}" ne "no" or
                               can-do("0,1,2,3,4,5,6,7,8,9", keylabel(lastkey))
                                    then apply keycode(substr(v-key, i, 1)).
                        end.

                        /*tb 16596 09/19/94 dww; Progress V7 Buffers a Space */
                        /* Apply any trailing space character */
                        if z-lastkey = 32 and "{&alpha}" ne "no"
                            then apply z-lastkey.

                        /* Turn off the flag that indicated this was the
                            first time into the edit loop */
                        z-loadcharfl = no.
                    end.

                    readkey.

                    /* If the Alpha flag is "no", then only numeric characters

                        and edit characters are allowed. */
                    if "{&alpha}" = "no" and
                       (not can-do("0,1,2,3,4,5,6,7,8,9", keylabel(lastkey)) and
                        not {k-cancel.i}                                     and
                        not {k-select.i}                                     and
                        not {k-pf.i}                                         and
                        not {k-return.i}                                     and
                        not {k-backsp.i}                                     and
                        not {k-del.i}                                        and
                        not {k-remove.i}                                     and
                        not {k-cursor.i})
                    then do:
                        bell.
                        next xxeditloop.
                    end.

                    apply lastkey.
                end.

            end.

            input clear.
            readkey pause 0.

            hide frame f-start no-pause.

            next main.
        end. /* text typed */
    end. /* repeat, dsply */



