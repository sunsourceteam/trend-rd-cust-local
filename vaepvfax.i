do: 
  {fixphone.gca {&file}faxphoneno s-faxphoneno {&comvar}}

  if avail sasp then 
  assign v-faxto[1] = {&file}name
         v-faxto[2] = if sasc.oifaxattn[{&extent}] then
                        {&file}slsnm 
                      else 
                        {&file}expednm
         v-rxfaxcmd  = sasp.pcommand + " -n" +
                       (if sasc.oifaxpreno = "1" and 
                         substring(s-faxphoneno,1,1) = "1" then 
                         ""
                        else 
                         sasc.oifaxpreno)  +
                         s-faxphoneno      + sasc.oifaxsufno +
                         " -t TCO="       +
                         "~"" + v-faxto[1] + "~"" +
                         " -t TNM="     +
                         "~"" + v-faxto[2] + "~"" +
                         " -t FNM=Va Quoting"  +
                         " -t tg1=" +
                         "~"" + v-faxto[1] + "~"" +
                         " -t tg2=" +
                         "~"" + {&tag} + "~"".
    
    output /*through value(sasp.pcommand)*/ to  value(v-rxfax) paged.
/*    
    find sassr where recid(sassr) = v-sassrid no-lock no-error.
         if avail sassr and 
            ((v-reportno = 0 and sassr.wide = true) or 
             (v-reportno = 0 and sassr.xwide[v-reportno] = true)) then 
             do: 
                v-faxcmd = chr(15).
                put control v-faxcmd.
             end.   
*/
end.                
             
{&out} = if sasc.oifaxhardfl[{&extent}] then "p" 
         else if v-coldfl then "x" 
         else {&out}.    
             
    
    
    
    
