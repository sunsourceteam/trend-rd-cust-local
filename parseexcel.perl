#!/opt/perl/bin/perl
 use strict;
 use Spreadsheet::ParseExcel;

 
 my $filename = $ARGV[0] ;

 
 
 if( ! -f $filename) {
   usage( "File $filename does not exist") ;
   exit ;
}

 
 
 my $oBook = 
   Spreadsheet::ParseExcel::Workbook->Parse($filename);
 my($iR, $iC, $oWkS, $oWkC , $oWkF );
 foreach my $oWkS (@{$oBook->{Worksheet}}) {
   print "-//-", $oWkS->{Name}, "\n";
   for(my $iR = $oWkS->{MinRow} ; 
     defined $oWkS->{MaxRow} && $iR <= $oWkS->{MaxRow} ; $iR++) {
     for(my $iC = $oWkS->{MinCol} ;
       defined $oWkS->{MaxCol} && $iC <= $oWkS->{MaxCol} ; $iC++) {
         $oWkC = $oWkS->{Cells}[$iR][$iC];
         $oWkF = $oWkS->{Cells}[$iR][$iC]->{Type};
       print $iR , "," , $iC , "," , $oWkC->Value , "," ,
         $oWkF , "\n" if($oWkC);
     }
   }
} 