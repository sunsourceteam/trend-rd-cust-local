/* p-fxvsdif.i             */
/* p-fxardf.i 1.1 10/28/93 */
/*h*****************************************************************************
  INCLUDE      : p-fxapdf.i
  DESCRIPTION  : AP fax defaults
  USED ONCE?   :
  AUTHOR       : rhl
  DATE WRITTEN : 10/28/93
  CHANGES MADE :
                06/26/02 sbr; TB# t18698 FAX: FROM defaults             
                Purchasing/Accounting
*******************************************************************************/
{w-sasc.i no-lock}
{w-apsv.i g-vendno no-lock} 

if v-faxfrom = "" then v-faxfrom = /* "Customer Service" */ "Purchasing".
if v-faxto1  = "" then v-faxto1  = {&file}name.

assign
  v-faxphoneno = if v-faxphoneno = "" then 
                   {&file}faxphoneno
                 else 
                    v-faxphoneno
  v-faxto2     = if v-faxto2 ne "" then 
                   v-faxto2
                 else   
                 if sasc.oifaxattn[{&extent}] then apsv.slsnm
                 else {&file}phoneno.

if v-faxfrom = "" or v-faxfrom = /*"Customer Service"*/ "Purchasing"
then do:
  for first pv_user fields(_user-name) where
            pv_user.cono  = g-cono and
            pv_user.oper2 = g-operinit
            no-lock:
    v-faxfrom = if pv_user._user-name <> "" and
                   pv_user._user-name <> g-operinit then 
                  pv_user._user-name
                else v-faxfrom.
  end.
end.
/*
if v-faxfrom = "Customer Service" then do:
    for first smsn fields(name) where
              smsn.cono   = g-cono and
              smsn.slsrep = g-operinit
              no-lock:
        v-faxfrom = smsn.name.
    end.
end.
*/

find first oimsp where
           oimsp.person = g-operinits 
           no-lock no-error. 
if avail oimsp then       
  if v-faxfrom = "" or v-faxfrom = "Purchasing" then
    assign v-faxfrom = trim(oimsp.name) + " Phone: " + trim(oimsp.phoneno) 
                      + " " + 
                       (if trim(oimsp.miscdata[1]) = "" then
                          "" 
                        else 
                          trim(oimsp.miscdata[1])) + 
                           
                        (if trim(oimsp.miscdata[1]) = "" then
                          "" 
                        else 
                          "@sunsrce.com"). 
