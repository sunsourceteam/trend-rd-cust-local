/* oeellu.lpr 1.0 10/19/01 */
/*******************************************************************************
 PROCEDURE    : oeellu.lpr
 DESCRIPTION  : oeellu.p fill temp table
 AUTHOR       : mt
 DATE WRITTEN : 10/19/01
 CHANGES MADE :
   10/19/01 mt; TB# e10078 Initial create
   11/26/01 mt; TB# e10078 Needed to set t-lu.viewln for all oeelc creates.
   02/14/03 mt; TB# e16226 Issues with line # lookup.
*******************************************************************************/

assign
    v-nxtviewln  = 1
    g-oeelclty   = ""
    g-oeelcseqno = 0.
if g-currproc <> "oeexq" then do:

  for each oeelc use-index k-intseqno where
           oeelc.cono       = g-cono and
           oeelc.orderno    = s-orderno and
           oeelc.ordersuf   = s-ordersuf and
           oeelc.dspllineno = 0
  no-lock:
    create t-lu.
    assign
        t-lu.viewln     = v-nxtviewln
        v-nxtviewln     = v-nxtviewln + 1
        t-lu.key        = string(oeelc.dspllineno,"999") + " " +
                          oeelc.linetype + " " +
                          string(oeelc.seqno,"99")
        t-lu.shipprod   = oeelc.descrip
        t-lu.proddesc   = oeelc.descrip1.
  end. /* for each oeelc */
end.
if g-currproc = "oeexq" then do:
  for each oeelb use-index k-oeelb where
           oeelb.cono     = g-cono and
           oeelb.batchnm  = string(s-orderno,">>>>>>>9")
           no-lock:
    find   oeehb  where
           oeehb.cono     = g-cono and
           oeehb.batchnm  = string(s-orderno,">>>>>>>9")  and
           oeehb.seqno    = 1 no-lock no-error.
  
    find first sastc where sastc.cono = g-cono and
               sastc.currencyty = (if oeehb.countrycd = "" and
                                      oeehb.user5 = "c" and
                                      oeehb.divno = 40 then
                                     "CN"
                                   else
                                     oeehb.countrycd) no-lock no-error.
    assign k-exchange   =  if oeehb.user6 <> 0 then
                              oeehb.user6
                           else if avail sastc then
                              round((1 / sastc.purchexrate),5)    
                           else    
                              1.
     
    if oeelb.specnstype = "n" then s-proddesc = oeelb.proddesc.
    else do for icsp:

        {w-icsp.i oeelb.shipprod no-lock}
        s-proddesc = if avail icsp then icsp.descrip[1]
                     else oeelb.proddesc.

    end. /* do for icsp */
    /*tb e16226 02/14/03 mt; build subtotal in case it's required*/
    v-subnetamt = v-subnetamt +
                  (if oeelb.returnfl = yes then
                       (oeelb.netamt * -1)
                   else
                       oeelb.netamt).
    create t-lu.
    /* tb# e10078 11/26/01 mt; need to set t-lu.viewln.*/
    assign
        t-lu.rushfl      = oeelb.rushfl
        t-lu.key         = string(oeelb.lineno,"999") + " " +
                           oeelb.specnstype
        t-lu.viewln     = v-nxtviewln
        v-nxtviewln     = v-nxtviewln + 1
        t-lu.shipprod    = oeelb.shipprod
        t-lu.qtyord      = oeelb.qtyord
        t-lu.unit        = oeelb.unit
        t-lu.proddesc    = s-proddesc
        t-lu.netamt      = oeelb.price * k-exchange
        t-lu.EXnetamt    = oeelb.netamt * k-exchange
        t-lu.exrate      = k-exchange
        t-lu.returnfl    = oeelb.returnfl.
 
  end.
end.
else do:
  for each oeel use-index k-oeel where
           oeel.cono     = g-cono and
           oeel.orderno  = s-orderno and
           oeel.ordersuf = s-ordersuf no-lock:
    find oeeh where
         oeeh.cono     = g-cono and
         oeeh.orderno  = s-orderno and
         oeeh.ordersuf = s-ordersuf no-lock no-error.    
    
    find first sastc where sastc.cono = g-cono and
               sastc.currencyty = (if oeeh.currencyty = "" and
                                      oeeh.user5 = "c" and
                                      oeeh.divno = 40 then
                                     "CN"
                                   else
                                     oeeh.currencyty) no-lock no-error.
    assign k-exchange   =  if oeeh.user6 <> 0 then
                              oeeh.user6
                           else if avail sastc then
                              round((1 / sastc.purchexrate),5)    
                           else    
                              1.
 
    if oeel.specnstype = "n" then s-proddesc = oeel.proddesc.
    else do for icsp:

        {w-icsp.i oeel.shipprod no-lock}
        s-proddesc = if avail icsp then icsp.descrip[1]
                     else oeel.proddesc.

    end. /* do for icsp */
    /*tb e16226 02/14/03 mt; build subtotal in case it's required*/
    v-subnetamt = v-subnetamt +
                  (if oeel.returnfl = yes then
                       (oeel.netamt * -1)
                   else
                       oeel.netamt).
    create t-lu.
    /* tb# e10078 11/26/01 mt; need to set t-lu.viewln.*/
    assign
        t-lu.rushfl      = oeel.rushfl
        t-lu.key         = string(oeel.lineno,"999") + " " +
                           oeel.specnstype
        t-lu.viewln     = v-nxtviewln
        v-nxtviewln     = v-nxtviewln + 1
        t-lu.shipprod    = oeel.shipprod
        t-lu.qtyord      = oeel.qtyord
        t-lu.unit        = oeel.unit
        t-lu.proddesc    = s-proddesc
        t-lu.netamt      = oeel.price * k-exchange
        t-lu.EXnetamt    = oeel.netord * k-exchange
        t-lu.exrate      = k-exchange
        t-lu.returnfl    = oeel.returnfl.

    /*tb 10078 08/30/01 mt; After displaying the oeel line check for
      any oeelc entries for the line*/
    for each oeelc use-index k-intseqno where
             oeelc.cono       = g-cono and
             oeelc.orderno    = s-orderno and
             oeelc.ordersuf   = s-ordersuf and
             oeelc.dspllineno = oeel.lineno
    no-lock:
        create t-lu.
        /* tb# e10078 11/26/01 mt; need to set t-lu.viewln.*/
        assign
            t-lu.key        = string(oeelc.dspllineno,"999") + " " +
                              oeelc.linetype + " " +
                              string(oeelc.seqno,"99")
            t-lu.viewln     = v-nxtviewln
            v-nxtviewln     = v-nxtviewln + 1
            t-lu.shipprod   = oeelc.descrip
            t-lu.proddesc   = oeelc.descrip1.

        /* tb e16226 02/14/03 mt; if subtotal set netamt*/
        if oeelc.linetype = "t" then
            assign
                t-lu.netamt = v-subnetamt
                v-subnetamt = 0.
    end.

  end. /* for each oeel */
end.

