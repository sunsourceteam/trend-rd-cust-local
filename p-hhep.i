/* p-hhep.i 
   Procedures for hhep.i */

/* put into settemprecords
procedure storeCurrRcpts:
for each tmp-rcpt:
  for each zsdiput use-index k-userid
    where zsdiput.cono = g-cono and                                                  zsdiput.whse = g-whse and 
    zsdiput.receiptid = tmp-rcpt.receiptid and
    zsdiput.user2 = g-operinits:

    assign zsdiput.xuser3 = 
                            g-whse +  "~011" +
                            v-direction + "~011" +
                            v-putawayinit + "~011" +
                            v-bintpeB  +  "~011" + 
                            v-bintpeE  + "~011". 

  end.
end.
end procedure.
**/


procedure unlockZsdi:

if {k-jump.i} then do:
  if avail tsum-put then do:
    for each t-put where 
             t-put.prod = tsum-put.prod and
             t-put.binloc1 = tsum-put.binloc1 no-lock:
      for each zsdiput where zsdiput.cono = g-cono and
               zsdiput.whse = g-whse and 
               zsdiput.shipprod = t-put.prod and  
               zsdiput.receiptid = t-put.receiptid and
               zsdiput.binloc1 = t-put.binloc1  and
               zsdiput.completefl = no: 
       assign zsdiput.xuser2 = "CurrentLine".
      end.     
    end.
  end.
  for each tmp-rcpt:
    for each zsdiput use-index k-userid
                     where zsdiput.cono = g-cono and
                           zsdiput.whse = g-whse and 
                           zsdiput.receiptid = tmp-rcpt.receiptid and
                           zsdiput.user2 = g-operinits:

        assign zsdiput.xuser3 = 
                                g-whse +  "~011" +
                                v-direction + "~011" +
                                v-putawayinit + "~011" +
                                v-bintpeB  +  "~011" + 
                                v-bintpeE  + "~011". 

    end.
  end.
  return.
end.    



for each tmp-rcpt:
  for each zsdiput use-index k-userid 
            where zsdiput.cono = g-cono and
                  zsdiput.whse = g-whse and 
                  zsdiput.receiptid = tmp-rcpt.receiptid and
                  zsdiput.user2 = g-operinits:
    zsdiput.user2 = "".
    zsdiput.xuser3 = "". /* dkt mar 2008 */
  end.
end.

end procedure.

procedure generateReport:
define input parameter whichPrt like sapb.printernm no-undo.
/* def var rptnm as c no-undo. */
def var minRcpt as dec initial 2147483647 no-undo.
def var maxRcpt as dec initial 0 no-undo.
def var minInt as int initial 0 no-undo.
def var maxInt as int initial 2147483647 no-undo.
def var minChar as char initial "" no-undo.
def var maxChar as char initial "~~~~~~~~~~~~~~~~~~~~~~~~" no-undo.
def var minDate as date initial "01/01/50" no-undo.
def var maxDate as date initial "12/31/49" no-undo.

/**
{g-sapb.i new}   
{g-rpt.i}        
{g-rptctl.i new} 
**/
{p-rptnm.i v-reportnm " "}

create sapb.
assign
  sapb.cono = g-cono
  sapb.backfl      = no
  sapb.currproc    = "porzp"  /* "rrcv"  */
  sapb.printoptfl  = no                                    
  sapb.priority    = 9    
  sapb.filefl      = no                                 
  sapb.inusecd     = "y"
  sapb.statustype  = ""
  sapb.demandfl    = no
  sapb.storefl     = no
  sapb.delfl       = yes.                                   

v-sapbid = recid(sapb).

find first sassr where sassr.currproc = sapb.currproc     
  no-lock no-error.                                         
{t-all.i sapb}                                                 
if avail sassr then sapb.rpttitle = sassr.rpttitle.

assign                                               
  v-reportid    = sassr.reportid                     
  v-openprfl    = sassr.openprfl                     
  v-openprno    = sassr.openprno                     
 /*  v-reportnm    = sapb.reportnm  */
  v-batfl       = false.                             
                                                                     
assign v-sapbid  = recid(sapb)                       
       v-sassrid = recid(sassr).                    
       v-saspid  = 0.        
sapb.reportnm = v-reportnm.
for each tmp-rcpt no-lock: /* break by tmp-rcpt.receiptid: */
  create sapbv.
  sapbv.vendno = tmp-rcpt.receiptid.
  sapbv.cono   = g-cono.
  sapbv.reportnm = v-reportnm.
  sapbv.transproc = sapb.currproc.
  sapbv.operinit  = g-operinits.
  {t-all.i sapbv}
  if tmp-rcpt.receiptid > maxRcpt then maxRcpt = tmp-rcpt.receiptid.
  if tmp-rcpt.receiptid < minRcpt then minRcpt = tmp-rcpt.receiptid.
end.  

sapb.printernm = whichPrt.
/* ranges 1 through 8 */
sapb.rangebeg[1] = string(minInt).
sapb.rangeend[1] = string(maxInt).
sapb.rangebeg[2] = string(minInt).
sapb.rangeend[2] = string(maxInt).
sapb.rangebeg[3] = string(minRcpt).
sapb.rangeend[3] = string(maxRcpt).
sapb.rangebeg[4] = string(minDate).
sapb.rangeend[4] = string(maxDate).
sapb.rangebeg[5] = string(minInt).
sapb.rangeend[5] = string(1000000000000).
sapb.rangebeg[6] = string(minChar).
sapb.rangeend[6] = string(maxChar).
sapb.rangebeg[7] = string(minDate).
sapb.rangeend[7] = string(maxDate).
sapb.rangebeg[8] = string(minChar).
sapb.rangeend[8] = string(maxChar).
sapb.optvalue[1] = g-whse.
sapb.optvalue[3] = g-operinits.
sapb.optvalue[6] = "E".
sapb.optvalue[7] = "D".
sapb.optvalue[8] = "yes".
sapb.optvalue[9] = "yes".
run rptctl2.p("porzp.p", yes, 0, yes, 0, yes). /* was rrcv */
/* for some reason not deleting */
find sapb where sapb.cono = g-cono and sapb.reportnm = v-reportnm
   no-error.
if avail sapb then 
  delete sapb.
message "". /* Clear Printing... message */
run unlockZsdi.
end procedure. /* generate report */

procedure assignBin:
/* used in hhep.led */
define input parameter i-prod like icsw.prod no-undo.
define input parameter i-binloc like icsw.binloc1 no-undo.
define input parameter i-whse like icsw.whse no-undo.

def var thisPri as integer initial 0.
def var assignCode as char.
def var tRecId as recid.             
def buffer bf-tsum-put for tsum-put. 

def var cZone as char initial "" no-undo.
/* @zone */
def var iZone as integer initial 0 no-undo.


cZone = "".
/* find empty */
find first wmsb where wmsb.cono = g-cono + 500 and
  wmsb.whse = i-whse and wmsb.binloc = i-binloc and
  wmsb.priority = 0 and wmsb.statuscode = "" no-error.
if not avail wmsb then do:
  /* find already assigned */
  find first wmsbp where wmsbp.cono = g-cono + 500 and
    wmsbp.whse = i-whse and wmsbp.binloc = i-binloc and
    wmsbp.prod = i-prod no-lock no-error.
  if not avail wmsbp then return.
  else
    find first wmsb where wmsb.cono = g-cono + 500 and
      wmsb.whse = i-whse and wmsb.binloc = i-binloc and
      wmsb.priority > 0 and wmsb.statuscode <> ""  no-error.
end.

if avail wmsb
then cZone = wmsb.building.
else do:
  message "Invalid bin".
  return.
end.
tRecId = recid(tsum-put).

/* @zone */
iZone = int(cZone)  no-error.
if error-status:error then
  cZone = cZone.
else
  cZone = string (int(cZone),"99999999").


find first bf-tsum-put where recid(bf-tsum-put) = tRecId
  no-error.
for each t-put where
  /* t-put.whse = g-whse and */
  t-put.prod = i-prod and                 
  t-put.completefl = no and
  (t-put.binloc1 = "" or t-put.binloc1 = "New Part"):                         
  for each zsdiput where zsdiput.cono = g-cono and
    zsdiput.whse = g-whse and 
    zsdiput.shipprod = t-put.prod
    and  zsdiput.receiptid = t-put.receiptid
    and  zsdiput.binloc1 = t-put.binloc1  
  and zsdiput.completefl = no:
  
    for each zsdiputdtl where zsdiputdtl.cono = zsdiput.cono and
    zsdiputdtl.receiptid = zsdiput.receiptid and
    zsdiputdtl.whse = zsdiput.whse and
    zsdiputdtl.shipprod = zsdiput.shipprod and
    zsdiputdtl.binloc1  = zsdiput.binloc1:

      assign zsdiputdtl.binloc1 = i-binloc
             /* zsdiputdtl.locationid = if avail wmsb then
                                        wmsb.building
                                     else
                                        "    ". */
             zsdiputdtl.locationid = cZone.
    end.
    assign zsdiput.binloc1 = i-binloc
           /* zsdiput.locationid = if avail wmsb then
                                   wmsb.building
                                else
                                   "    ". */
           zsdiput.xuser1     = "" 
           zsdiput.locationid = cZone.
  end.
  assign t-put.binloc1 = i-binloc
         /* t-put.zone    = if avail wmsb then
                                   wmsb.building
                                else
                                   "    ". */
         t-put.zone = cZone.
end.
assign bf-tsum-put.binloc  = i-binloc
       bf-tsum-put.binloc1 = i-binloc
       bf-tsum-put.zone    = cZone
       bf-tsum-put.binsort = substring(i-binloc,1,2) + "````````".
       /* if avail wmsb then
                               wmsb.building
                             else
                               "    ". */
release bf-tsum-put.

find first wmsbp where wmsbp.cono = g-cono + 500 and
  wmsbp.whse = i-whse and wmsbp.binloc = i-binloc and
  wmsbp.prod = i-prod no-lock no-error.

if avail wmsbp then return.
find first icsw where icsw.cono = g-cono and icsw.whse = i-whse
  and icsw.prod = i-prod  no-error.
if avail icsw then do:
  assignCode = icsw.statustype.
  if icsw.binloc1 = "" or icsw.binloc1 = "New Part" then do:
    icsw.binloc1 = i-binloc.
    thisPri = 1.
  end.
  else do:
    if icsw.binloc2 = "" or icsw.binloc2 = "New Part" then do:
      icsw.binloc2 = i-binloc.
      thisPri = 2.
    end.
    else thisPri = 9.
  end.
  release icsw. /* dkt 2008 */   
end. /* avail icsw */
else do:
  thisPri = 9.
  assignCode = "N".
end.

create wmsbp.
wmsbp.cono = wmsb.cono.
wmsbp.whse = wmsb.whse.
wmsbp.binloc = wmsb.binloc.
wmsbp.prod   = i-prod.

{t-all.i wmsbp}
 
wmsbp.transproc = "hhep".

/* modify wmsb */
wmsb.statuscode = "A".
wmsb.priority   = thisPri.
wmsb.assigncode = assignCode.
wmsb.transproc  = "hhep".
/* create zzbin? */
if thisPri = 9 then do:
  find first zzbin where zzbin.cono = g-cono and
  zzbin.whse = i-whse and zzbin.prod = i-prod and zzbin.binloc = i-binloc
  no-lock no-error.
  if not avail zzbin then do:
    create zzbin.
    zzbin.cono = g-cono.
    zzbin.prod = i-prod.
    zzbin.whse = i-whse.
    zzbin.binloc = i-binloc.
  end.
end.

/* all in updatezsdiput */
end procedure.

procedure showidlist:
define input  parameter i-idrcpt like tmp-rcpt.receiptid.
define output parameter o-idrcpt like tmp-rcpt.receiptid.

on cursor-up cursor-up.        
on cursor-down cursor-down.    

close query q-trcpt.
open query q-trcpt                                                      
  for each b-trcpt no-lock.
on any-key of b-idlist in frame f-idlist do:
  if {k-cancel.i} or {k-jump.i} then do:
    hide frame f-idlist.
    o-idrcpt = i-idrcpt.
    apply "window-close" to frame f-idlist.
    put screen row 13 column 1 color messages "F6Ex F7 Bin2 F8Qty F9Srch ".
  end.
  else
  if keylabel(lastkey) = "RETURN" or keylabel(lastkey) = "PF1" then do:
    if avail b-trcpt then
      o-idrcpt = b-trcpt.receiptid.
    hide frame f-idlist.
    apply "window-close" to frame f-idlist.
    put screen row 13 column 1 color messages "F6Ex F7 Bin2 F8Qty F9Srch ".
  end.
end. /* any-key */

enable b-idlist with frame f-idlist.
apply "entry" to b-idlist.
display b-idlist with frame f-idlist.
wait-for window-close of frame f-idlist.
on cursor-up back-tab.
on cursor-down tab.
close query q-trcpt.
release b-trcpt.                  
end procedure. /* showidlist */

/* dkt 3/2008 */
procedure partialputaway:
put screen row 13 column 1 color messages "F6Ex F7 Bin2 F8QTY F9Srch ".
p-qtyputaway = 0. /* t-stkqty. */
display t-stkqtyputaway t-stkqty t-binloc1 with frame f-partial.
update p-qtyputaway with frame f-partial
editing:
  readkey.
  if {k-cancel.i} then do:
    hide frame f-partial.
    put screen row 13 column 1 color messages "F6Ex F7 Bin2 F8Qty F9Srch ".
    return.
  end.
  apply lastkey.
end.
tsum-put.qtyrcvd = tsum-put.qtyrcvd + p-qtyputaway.
t-stkqty = tsum-put.qtyrcvd.
hide frame f-partial.

/* f6 logic */

def buffer xi-zsdiput for zsdiput.
def buffer xi-zsdiputdtl for zsdiputdtl.
def buffer xi-t-put for t-put.
def buffer xi-tsum-put for tsum-put.
def var ip-exstatustype as char initial "PP" no-undo.
def var ip-extype as char initial "PP" no-undo.


for each tmp-rcpt no-lock:     /* only work on receiptids loaded */
    for each xi-zsdiput where 
             xi-zsdiput.cono      = g-cono and
             xi-zsdiput.whse      = g-whse and
             xi-zsdiput.receiptid = tmp-rcpt.receiptid and
             xi-zsdiput.shipprod  = t-shipprod and 
             xi-zsdiput.binloc1   = t-binloc1:
      assign xi-zsdiput.exstatustype  = ip-exstatustype
             xi-zsdiput.extype        = ip-extype
             xi-zsdiput.putawayinit   = g-operinits.
      for each xi-zsdiputdtl use-index dtlidix where 
               xi-zsdiputdtl.cono      = g-cono and
               xi-zsdiputdtl.whse      = g-whse and
               xi-zsdiputdtl.receiptid = tmp-rcpt.receiptid and
               xi-zsdiputdtl.shipprod  = t-shipprod and 
               xi-zsdiputdtl.binloc1   = t-binloc1:                    
        assign xi-zsdiputdtl.exstatustype  = ip-exstatustype
               xi-zsdiputdtl.extype        = ip-extype.
               /* xi-zsdiputdtl.putawayinit   = g-operinits. */
        release xi-zsdiputdtl.
      end.
    end.  
 
    for each xi-t-put where 
             xi-t-put.whse      = g-whse and
             xi-t-put.receiptid = tmp-rcpt.receiptid and
             xi-t-put.prod      = t-shipprod and 
             xi-t-put.binloc1   = t-binloc1:
      assign xi-t-put.extype    = ip-exstatustype.
    end.     
  end.    

  find first xi-tsum-put where 
             xi-tsum-put.prod = t-shipprod and
             xi-tsum-put.binloc1 = t-binloc1 no-error.
  if avail xi-tsum-put then
    assign xi-tsum-put.extype = ip-exstatustype. 
/* end f6 logic */

put screen row 13 column 1 color messages "F6Ex F7 Bin2 F8Qty F9Srch ".
end procedure.

procedure displayprodbin:
if avail b-tprod then do:
  t-binloc1 = b-tprod.binloc1.
  t-binloc2 = b-tprod.binloc2.
  assign v-listlit = (if b-tprod.completefl then "Completed"
                      else if b-tprod.extype <> "" then "Exception"
                      else "Open").
end.
else do:
  t-binloc1 = "".
  t-binloc2 = "".
  v-listlit = "".
end.                    
display t-binloc1 t-binloc2 v-listlit with frame f-hhepb.
apply "focus" to inSearch in frame f-prodlist.
end procedure.

/* f9 new showprodlist */
procedure showprodlist:
define output parameter outProduct like icsp.prod no-undo.
define output parameter outBin like icsw.binloc1 no-undo.
outProduct = "".
outBin = "".
inSearch = "".

on cursor-up cursor-up.        
on cursor-down cursor-down.    
put screen row 13 column 1 color messages "Search F6 PROD   F7 Bins  ".

on entry of b-prodlist in frame f-prodlist do:
  run displayprodbin.
end.
on value-changed of b-prodlist in frame f-prodlist do:
  run displayprodbin.
end.

close query q-tprod.
open query q-tprod
  for each b-tprod /* where b-tprod.cono = 1 and
  b-tprod.whse = "test" */ /* use-index k-tsum */ no-lock.
thisLabel = "Product".
enable b-prodlist with frame f-prodlist.
display thisLabel b-prodlist with frame f-prodlist.
apply "entry" to b-prodlist. 
put screen row 13 column 1 color messages "Search F6 PROD   F7 Bins  ". 
update inSearch with frame f-prodlist
editing:
  readkey.
      
  if (lastkey = 501 or
      lastkey = 502 or
      lastkey = 503 or
      lastkey = 504 or
      lastkey = 507 or 
      lastkey = 508) then do:
    enable b-prodlist with frame f-prodlist. 
    apply lastkey to b-prodlist in frame f-prodlist.            
    apply "focus" to inSearch in frame f-prodlist.                
  end.         
  else do:
    if {k-after.i} or {k-accept.i} then do:
      if avail b-tprod then do:
        outProduct = b-tprod.prod.
        outBin = b-tprod.binloc1.
      end.  
      else do: 
        outProduct = input inSearch.
        outBin = "".
      end.  
    end.
    
    if not {k-cancel.i} then apply lastkey. 
    inSearch = input inSearch.
    if {k-func6.i} and thisLabel ne "Product" then do:
      thisLabel = "Product".
      put screen row 13 column 1 color messages "Search F6 PROD   F7 Bins  ".
    end.
    if {k-func7.i} and thisLabel ne "Bin" then do:
      thisLabel = "Bin".
      put screen row 13 column 1 color messages "Search F6 Prod   F7 BINS  ".
    end.
    close query q-tprod.
    if thisLabel = "Bin" then
      open query q-tprod 
      for each b-tprod where b-tprod.binloc1 begins input inSearch no-lock.
    else
      open query q-tprod 
      for each b-tprod where b-tprod.prod begins input inSearch no-lock.
    display thisLabel inSearch b-prodlist with frame f-prodlist.
    run displayprodbin.
  end.
 
  if lastkey = 13 or keyfunction(lastkey) = "go" or {k-cancel.i} then do:
    on cursor-up back-tab.
    on cursor-down tab.
    hide frame f-prodlist.
    hide frame f-hhepb.
    apply "window-close" to frame f-prodlist.
    close query q-tprod.
    release b-tprod.
    put screen row 13 column 1 color messages "F6Ex F7 Bin2 F8Qty F9Srch ".
    return.
  end.
end.

on cursor-up back-tab.
on cursor-down tab.

end procedure.
/* f9 new showprodlist end */
/**
procedure showbinlist:
on cursor-up cursor-up.        
on cursor-down cursor-down.    

put screen row 13 column 1 color messages "Search F6 Prod F7 BINS".

close query q-tbin.      
open query q-tbin
  for each b-tbin use-index k-bin no-lock.
on any-key of b-binlist in frame f-binlist do:
  if {k-func6.i} then do:
    hide frame f-binlist.
    apply "window-close" to frame f-binlist.
    close query q-tbin.
    run showprodlist.
  end. 
  if {k-cancel.i} or {k-jump.i} then do:
    hide frame f-binlist.
    apply "window-close" to frame f-binlist.
  end.
  else
  if keylabel(lastkey) = "RETURN" or keylabel(lastkey) = "PF1" then do:
    find tsum-put where recid(tsum-put) =                    
      recid(b-tbin) no-lock no-error.
    find first zsdiput where
      zsdiput.cono = g-cono     and
      zsdiput.whse = g-whse and
      zsdiput.shipprod = tsum-put.prod and
      zsdiput.binloc1  = tsum-put.binloc1 no-lock no-error.
    assign
      t-shipprod      = b-tbin.prod   
      t-proddesc1     = zsdiput.proddesc1 
      t-proddesc2     = zsdiput.proddesc2 
      t-stkqtyputaway = b-tbin.qty  
      t-binloc1       = b-tbin.binloc1   
      t-binloc2       = b-tbin.binloc2
      xx t-stkqty        = t-stkqtyputaway.  
          
    hide frame f-binlist.
    apply "window-close" to frame f-binlist.
    {hhep.lds}
  end.
end. /* any-key */

enable b-binlist with frame f-binlist.
apply "entry" to b-binlist. 
display b-binlist with frame f-binlist.
wait-for window-close of frame f-binlist.
on cursor-up back-tab.
on cursor-down tab.
close query q-tbin.
release b-tbin.                  
end procedure. /* showprodlist */
**/

procedure updateZsdiput:
/* define input parameter i-recid as recid. */
define input parameter i-prod  like zsdiput.shipprod no-undo.
define input parameter i-qty   as decimal no-undo.
define input parameter i-bin   like zsdiput.binloc1 no-undo.
/* define input parameter i-putno as int no-undo. */
/* define input parameter i-recBin like zsdiput.binloc1 no-undo. */
define /* output parameter */ var o-rptFlag as logical initial no no-undo.

def var cQty like zsdiput.stkqtyputaway no-undo.
def var tQty like zsdiput.stkqtyputaway no-undo.
def var putAllfl as logical no-undo.
def var currputrecno as dec no-undo.

/* def var v-tmprcpt as dec no-undo. */

def buffer bf-tsum-put for tsum-put.
def buffer lp-tsum-put for tsum-put.

find first bf-tsum-put where
           bf-tsum-put.prod = i-prod and
           bf-tsum-put.binloc1 = i-bin /* and
           bf-tsum-put.putno = i-putno */  no-error.

if avail bf-tsum-put then do:

  if bf-tsum-put.qty <> i-qty  and
     bf-tsum-put.extype <> "D" then do:
    /* not full put away */
    /* message "not full putaway for " i-prod. */
    putAllfl = no.
    tQty = i-qty.
    bf-tsum-put.extype = "PP".
  end.
  else do:
    if bf-tsum-put.extype = "PP" then 
      bf-tsum-put.extype = "".
    bf-tsum-put.extype = "". /* clear possible reentry */
    putAllfl = yes.
  end.
  
  if bf-tsum-put.extype = "D" then putAllfl = yes.
  if bf-tsum-put.extype = "PP" then do:
    putAllfl =  bf-tsum-put.split. 
    /*
    message "pp - " bf-tsum-put.split
    bf-tsum-put.prod bf-tsum-put.binloc1. pause 3.
    */
  end.  
  bf-tsum-put.completefl = putAllfl.
  if putAllfl then
    bf-tsum-put.extype = "".
end.
else return.
 
for each tmp-rcpt no-lock:
  for each t-put where 
           t-put.receiptid = tmp-rcpt.receiptid and
           t-put.prod = i-prod and
           t-put.binloc1 = i-bin and
           t-put.completefl = no /*  and
           t-put.putno = bf-tsum-put.putno */ :
    /* this should be only one.... if we don't play with binloc1
       if it splits into two bins, the one zsdiput is now more
       than one and I need the right key... */
    for each zsdiput where zsdiput.cono = g-cono and
             zsdiput.whse = g-whse and 
             zsdiput.shipprod = (if i-prod = "" then zsdiput.shipprod  
                                 else i-prod)  
             and  zsdiput.receiptid = tmp-rcpt.receiptid
             and  zsdiput.binloc1 = t-put.binloc1  
    /* and  zsdiput.locationid = t-put.zone */
            and zsdiput.completefl = no: 
      /*  Do I still need to check zone? Probably! moved above
      if i-zone1 ne "" then if zsdiput.locationid < i-zone1 then next.
      if i-zone2 ne "" then if zsdiput.locationid > i-zone2 then next.
      */
      
      zsdiput.putawaybin = i-bin.
      zsdiput.putawayinit = g-operinits. 
      zsdiput.extype = t-put.extype.
      zsdiput.completefl = putAllfl.
      zsdiput.exstatustype = if t-put.extype <> "PP" or putAllfl then
                               ""
                             else
                               zsdiput.exstatustype.  
      zsdiput.putawaydt = today.
      substring(zsdiput.xuser5,4) = STRING(zx-l).
      /** zsdiput.recordtype = "PO". **/
      t-put.completefl = putAllfl.
      {t-all.i zsdiput}
      /*  t-put.transdt  = zsdiput.transdt
          t-put.transtm  = zsdiput.transtm  */
      find first zsdiputdtl where 
        zsdiputdtl.cono = zsdiput.cono and    
        zsdiputdtl.whse = zsdiput.whse and
        zsdiputdtl.receiptid = zsdiput.receiptid and                    
        zsdiputdtl.shipprod = zsdiput.shipprod and                         
        zsdiputdtl.binloc1  = zsdiput.binloc1 no-error.
      if avail zsdiputdtl then do:
        zsdiputdtl.completefl = putAllfl.
        /* zsdiputdtl.recordtype = "PO". dkt March 2008 */
        zsdiputdtl.extype = t-put.extype.
        zsdiputdtl.exstatustype = if t-put.extype <> "PP" or putAllfl then
                                    ""
                                  else
                                    zsdiput.exstatustype.  
        zsdiputdtl.stkqtyputaway = zsdiput.stkqtyputaway.
        {t-all.i zsdiputdtl}
      end.
      if t-put.extype <> "pp" or putAllfl then t-put.extype = "".
    end.
  end. /*  for each t-put */
end. /* tmp-rcpt */

/* just grabs first in list 05/2008 */
find first tmp-rcpt no-lock no-error.

/* v-tmprcpt = tmp-rcpt.receiptid. */
for each bf-tsum-put where 
         bf-tsum-put.completefl = no and
         bf-tsum-put.prod = i-prod and
         bf-tsum-put.qty ne tsum-put.qtyrcvd:

    /*
    message "!= " bf-tsum-put.qty tsum-put.qtyrcvd i-prod "p-hhep.i updZsdi".
    pause.
    */
    
    /* create zsdiput */
    find zsdiput where zsdiput.cono = 500 + g-cono and
      zsdiput.whse = g-whse and
      zsdiput.shipprod = tsum-put.prod and
      zsdiput.receiptid = tmp-rcpt.receiptid and
      zsdiput.completefl = no
      no-error.
    if not avail zsdiput then do:
      find last zsdiput use-index putin where 
        zsdiput.cono = g-cono + 500 no-lock no-error.
      currputrecno = if avail zsdiput then zsdiput.putrecno else 1.
      create zsdiput.
      assign zsdiput.cono = g-cono + 500
             zsdiput.putrecno = /* something unique  int(recid(zsdiput)) */
                currputrecno + 1
             zsdiput.whse = g-whse
             zsdiput.shipprod = tsum-put.prod
             zsdiput.receiptid = tmp-rcpt.receiptid
             zsdiput.stkqtyalloc = tsum-put.qty  /* all expected */
             zsdiput.recordtype = "HPUT"
             zsdiput.completefl = no.
    end.  
    zsdiput.putawaybin    = i-bin.
    zsdiput.stkqtyputaway = tsum-put.qtyrcvd.    /* all there */
    
    advance = yes.  /* move to next */
end.
  /* then do for each where are equal and delete the zsdiput for cono 501? */
end procedure.

procedure checkReport:
  define output parameter o-rptFlag as logical   no-undo.   
  def var answer as logical format "yes/no" no-undo. 
  def var whichPrinter like sapb.printernm no-undo.
  def buffer bf-tsum-put for tsum-put.

  form
    whichPrinter  at 4
    
  with frame f-prt title "Exception Printer"
       row 4 column 4 overlay. 
  form answer label  "Batch Completed ?" at 2   
    with frame f-complete side-labels row 4 overlay. 
 
  assign o-rptFlag = no.
         answer    = no.

  find first bf-tsum-put where                                   
             bf-tsum-put.completefl = no and                       
             bf-tsum-put.extype = "" no-lock no-error. 
  if not avail bf-tsum-put then 
    find first bf-tsum-put where                                   
               bf-tsum-put.completefl = no and                       
               bf-tsum-put.extype = "PP" no-lock no-error. 
             
  if not avail bf-tsum-put then do:
    /* all complete or exception */
    /* ask if they want to finsh/print report */
    display answer with frame f-complete.
    status input "Arrow up/dn change answer ".
    completeloop:
    do on endkey undo completeloop, leave completeloop:                         
      update answer                 
        with frame f-complete
      editing:
        readkey.
        if lastkey = 501 or lastkey = 502 then do:
          if input answer = true then do:
             assign answer = false.
             display answer with frame f-complete.
             next.
          end.
          else
          if input answer = false then do:
             assign answer = true.
             display answer with frame f-complete.
             next.
          end.
        end.
      apply lastkey.
      end.    
      status input "                          ".
         
      if keyfunction(lastkey) = "GO" or                                 
         keyfunction(lastkey) = "RETURN" then do:                   
        hide frame f-complete no-pause.                                 
        leave completeloop.                            
      end.      
    end. /* completeloop */                                                

  hide frame f-complete.

  if answer = yes then do:
    /* delete the cono 501 for partial putaway */
    for each bf-tsum-put where bf-tsum-put.qty = bf-tsum-put.qtyrcvd no-lock:
      for each zsdiput where zsdiput.cono = g-cono + 500 and
             zsdiput.whse = g-whse and 
             zsdiput.receiptid = tmp-rcpt.receiptid and
             zsdiput.shipprod  = tsum-put.prod and
             (zsdiput.user2 = "" or zsdiput.user2 = g-operinits) and
             zsdiput.recordtype = "HPUT":
        delete zsdiput.
        message "deleted 501 partial receive". pause 3.     
      end.       
    end.  
    
    find first bf-tsum-put where bf-tsum-put.extype ne "" no-lock no-error.
    if not avail bf-tsum-put then do:
      o-rptFlag = yes. /* report would be blank */
      return.
    end.

    {w-sasoo.i g-operinits no-lock}
    if avail sasoo then
      whichPrinter = sasoo.printernm. 
    if whichPrinter = "" or whichPrinter = "vid" then do:
      {w-icsd.i g-whse no-lock}
      if avail icsd then
        whichPrinter = icsd.printernm[1].
    end.
    input clear.
    readkey pause 0.
    display whichPrinter with frame f-prt.
    update  whichPrinter with frame f-prt
    editing:
      readkey.
      if {k-cancel.i} then do:
        whichPrinter = "".
        hide frame f-prt.
        assign  o-rptFlag = no.
        return.
      end.

      if {k-sdileave.i &fieldname     = "whichPrinter"
                       &completename  = "whichPrinter"} then do:
        find first sasp where sasp.printernm = input whichPrinter
                       no-lock no-error.
        if not avail sasp then do:
          message "Invalid Printer".
          apply lastkey.
          next-prompt whichPrinter with frame f-prt.
          next.
        end.
         
      end.
      apply lastkey.
    end.
    hide frame f-prt.
    message "Printing Report...". 
    run generateReport(input whichPrinter).
    message "".                     
    o-rptFlag = yes.
  end.
end.
end procedure.

procedure validateException:
define input  parameter i-tsumRecid as recid no-undo.
define output parameter o-doneFlag  as logical initial no no-undo.

def buffer bf-tsum-put for tsum-put.
def var canswer as logical format "yes/no" no-undo. 
def var cquestion as char format "x(17)" no-undo.
form
 cquestion at 2
 canswer at 20 no-label    
with frame f-clear no-labels row 4 overlay. 
assign o-doneFlag = false.
find first bf-tsum-put where recid(bf-tsum-put) = i-tsumRecid                        no-error. 

if avail bf-tsum-put then do:
  if bf-tsum-put.extype = "" then        
    assign cquestion = "Clear from Batch?".
  else
    assign cquestion = "Clear Exception ?".
end.    

if avail bf-tsum-put then do:
  display cquestion
          canswer with frame f-clear.
  status input "Arrow up/dn change answer ".

  clearloop:
  do on endkey undo clearloop, leave clearloop:                         
    update canswer 
      with frame f-clear
      editing:
        readkey.
        if lastkey = 501 or lastkey = 502 then do:
          if input canswer = true then do:
             assign canswer = false.
             display cquestion canswer with frame f-clear.
             next.
          end.
          else
          if input canswer = false then do:
             assign canswer = true.
             display cquestion canswer with frame f-clear.
             next.
          end.
        end.
      apply lastkey.
      end.    
    
      status input "                          ".
        
      if keyfunction(lastkey) = "GO" or                                 
         keyfunction(lastkey) = "RETURN" then do:                   
        hide frame f-clear no-pause.                                 
        leave clearloop.                            
      end.      
   end. /* clearloop */                                                

  hide frame f-clear.
end.
if canswer = yes then do:
  assign o-doneFlag = true.
  bf-tsum-put.extype = "D".
  bf-tsum-put.completefl = yes.
   
  /* for each tmp-rcpt no-lock: */
  for each t-put where 
  t-put.prod = bf-tsum-put.prod and
  t-put.binloc1 = bf-tsum-put.binloc1 and
  t-put.completefl = no:
    t-put.extype = "D1".
  end.
  run updateZsdiput         
    (input bf-tsum-put.prod,
           bf-tsum-put.qtyrcvd, 
           bf-tsum-put.binloc1).       
/*           bf-tsum-put.putno). */
end.
end procedure.  /* validateException */

procedure setTempRecords:        
def var sumqty like zsdiput.stkqtyputaway initial 0 no-undo.
def var fndCnt as int initial 0 no-undo. 
def var vl-reload as logical no-undo.
/* @zone */
def var iZone as integer initial 0 no-undo.
def var cZone as character initial " " no-undo.

run RFLoop.

assign vl-reload = v-reload
       v-reload  = false.

for each tmp-rcpt no-lock:
  find first tsum-put where 
       tsum-put.prod = zsdiput.shipprod and
       tsum-put.binloc1 = zsdiput.binloc1  no-lock no-error.

  find first t-put where 
             t-put.receiptid = tmp-rcpt.receiptid no-lock no-error.
  if not avail t-put then do:
    for each zsdiput where zsdiput.cono = g-cono and
             zsdiput.whse = g-whse and 
             zsdiput.receiptid = tmp-rcpt.receiptid and
             (zsdiput.user2 = "" or zsdiput.user2 = g-operinits): 
      /* new */
      if locked zsdiput then next.
      if zsdiput.binloc1 ne "" and 
        (v-bintpeB ne "" or v-bintpeE ne "") then do:
        find wmsb where wmsb.cono = 500 + g-cono and wmsb.whse = g-whse
        and wmsb.binloc = zsdiput.binloc1 no-lock no-error.
        if wmsb.bintype < v-bintpeB or wmsb.bintype > v-bintpeE then 
          next.
      end.    
      if zsdiput.user2 ne "" and zsdiput.user2 ne g-operinits then do:
        next.
      end.  
      fndcnt = fndcnt + 1.
      unixCmd = "who -mu".                                          
      input through value(unixCmd).                                 
      import who[1] who[2] who[3] who[4] who[5] who[6] who[7] who[8].
      input close.                                                  
      
      zsdiput.user2 = g-operinits.
      zsdiput.user3 = who[2]. 
      zsdiput.user4 = who[8]. 

        
      assign zsdiput.xuser3 = 
                            g-whse +  "~011" +
                            v-direction + "~011" +
                            v-putawayinit + "~011" +
                            v-bintpeB  +  "~011" + 
                            v-bintpeE  + "~011". 

    
       /* end new */
      if not vl-reload then
        assign zsdiput.exstatustype = "".
      /*
      if i-zone1 ne "" then if zsdiput.locationid < i-zone1 then next.
      if i-zone2 ne "" then if zsdiput.locationid > i-zone2 then next.
      */

/* blank bins are exceptions if bin type ranges are entered. */      
      
      if (v-bintpeB ne "" or v-bintpeE ne "") and zsdiput.binloc1 = ""
        and not vl-reload then do:
        assign zsdiput.exstatustype = "Z1"
               zsdiput.extype       = "E".
        for each zsdiputdtl use-index dtlidix where 
                 zsdiputdtl.cono      = g-cono and
                 zsdiputdtl.whse      = g-whse and
                 zsdiputdtl.receiptid = zsdiput.receiptid and
                 zsdiputdtl.shipprod  = zsdiput.shipprod and 
                 zsdiputdtl.binloc1   = zsdiput.binloc1:                    
          assign zsdiputdtl.exstatustype  = zsdiput.exstatustype
                 zsdiputdtl.extype        = zsdiput.extype.
          release zsdiputdtl.
        end.
      end.          

 /* @zone */
      iZone = int(zsdiput.locationid)  no-error.
      if error-status:error then
        cZone = zsdiput.locationid.
      else
        cZone = string (int(zsdiput.locationid),"99999999").
 
      
      
      idcnt = idcnt + 1. 
      create t-put.
      assign
        t-put.whse = v-whse
        t-put.receiptid = zsdiput.receiptid
        t-put.prod = zsdiput.shipprod
        t-put.bin  = ""
        t-put.binloc1 = zsdiput.binloc1
        t-put.binloc2 = zsdiput.binloc2
        t-put.qty  = zsdiput.stkqtyputaway  
        t-put.oper = g-operinits /* zsdiput.operinit */
        t-put.extype = zsdiput.exstatustype
        t-put.complete = zsdiput.completefl
        t-put.transdt  = zsdiput.transdt
        t-put.transtm  = zsdiput.transtm
        t-put.zone     = if zsdiput.binloc1 = "" or
                            zsdiput.binloc1 = "New Part" then
                           "````"
                         else      /* @zone  zsdiput.locationid */
                            cZone
        t-put.zput     = recid(zsdiput)
        t-put.idno     = idcnt.
/*        t-put.putno    = cPutno. */
      
      /* display idcnt. */
      /** It makes more sense to input receiptid for each product
          otherwise we cannot track which receipts were short  **/

      
      find first tsum-put where 
                 tsum-put.prod = zsdiput.shipprod and
                 tsum-put.binloc1 = zsdiput.binloc1  no-error.
      if avail tsum-put then
        tsum-put.qty = tsum-put.qty + zsdiput.stkqtyputaway.
      else do:
       /* @zone */
        iZone = int(zsdiput.locationid)  no-error.
        if error-status:error then
          cZone = zsdiput.locationid.
        else
          cZone = string (int(zsdiput.locationid),"99999999").
 
        create tsum-put.
        tsum-put.binloc  = "".
        tsum-put.binloc1 = zsdiput.binloc1.
        tsum-put.binloc2 = zsdiput.binloc2.
        tsum-put.zone = if zsdiput.binloc1 = "" or
                            zsdiput.binloc1 = "New Part" then
                           "````"
                         else   /* @zone   zsdiput.locationid */
                           cZone.
        tsum-put.prod = zsdiput.shipprod.
        tsum-put.qty  = zsdiput.stkqtyputaway.
        tsum-put.qtyrcvd = 0.
        tsum-put.completefl = zsdiput.completefl.
        tsum-put.extype = zsdiput.exstatustype.
/*        tsum-put.putno = cPutno. */
        tsum-put.binsort = if (zsdiput.binloc1 = "" or
                             zsdiput.binloc1 = "New Part") and
                             zsdiput.xuser1 <> "" then
                             zsdiput.xuser1
                           else
                           if zsdiput.binloc1 = "" or
                             zsdiput.binloc1 = "New Part" then
                             "``````````"
                           else
                             substring(zsdiput.binloc1,1,2) + "````````".


        if zsdiput.xuser2 = "CurrentLine" then do:
          if not vl-reload then
            assign v-reloadID = ?.
          else
            assign v-reloadID = recid(tsum-put).
         
          assign zsdiput.xuser2 = "".             
        end. 

        assign zsdiput.xuser3 = "".             

        
        find first icsw where icsw.cono = g-cono and icsw.whse = v-whse and
        icsw.prod = zsdiput.shipprod no-lock no-error.
        if avail icsw then do:
          tsum-put.vendprod = icsw.vendprod.
          tsum-put.vendno   = icsw.arpvendno.      
        end.
      end.
    end. /* for each zsdiput */
    for each zsdiput where zsdiput.cono = g-cono + 500 and
             zsdiput.whse = g-whse and 
             zsdiput.receiptid = tmp-rcpt.receiptid and
             (zsdiput.user2 = "" or zsdiput.user2 = g-operinits) and
             zsdiput.recordtype = "HPUT"
             no-lock:
      find first tsum-put where tsum-put.prod = zsdiput.shipprod no-error.
      if avail tsum-put then
        tsum-put.qtyrcvd = zsdiput.stkqtyputaway.         
    end.
  end.
end.
if fndCnt > 0 then loaded-sw = yes.
else do:
  loaded-sw = no.
  message "No valid unopen records found".
end.  
end procedure. /* setTempRecs */

procedure displaytmprcpt:
define input parameter i-htype as c no-undo. 
/** other defs for browse and form in x-putaway.i **/
close query q-tr.
open query q-tr for each b-tr no-lock.
display b-rcpt with frame f-rcpt.
apply "focus" to v-receiptid in frame f-hhep-h.
end procedure. /* displaytmprcpt */

procedure putawaybrowse:
define input parameter i-whse like icer.whse.
define input parameter i-receipt as dec.
define input parameter i-jrnlno  as dec.
define input parameter i-style   as char.
define output parameter o-out    as char.

/* need to make it for tsum-put and not zsdi! 05/27/07 */

/* define buffer b-zsdiput for zsdiput. */
define buffer b-zsdiput for tsum-put.
define query q-put for b-zsdiput scrolling. 
                                               
define browse b-put query q-put          
display /* if i-style = "Prod" then */
        if num-results("q-put") >= 1 then
          STRING(b-zsdiput.prod,"x(24)")
        else
          STRING("No items found", "x(24)")
        /*
        else if i-style = "bin" then
          STRING(b-zsdiput.locationid, "x(24)")   
        else if i-style = "receipt" then
          STRING(b-zsdiput.receiptid, "x(24)")
        else
        STRING(b-zsdiput.whse + " " + STRING(b-zsdiput.putrecno,"x(19)"), "x(24)")     */
        format "x(24)"                          
with size-chars 26 by 9 down                  
centered overlay no-hide no-labels.             
              
form
  b-put
with frame f-put row 1 no-hide no-box overlay no-labels.

on cursor-up cursor-up.        
on cursor-down cursor-down.    

/* should match the criteria in the build t-put */
/*
if integer(i-jrnlno) > 0 then  
  open query q-put
  for each b-zsdiput where b-zsdiput.cono = g-cono and b-zsdiput.whse = i-whse
  and b-zsdiput.jrnlno = integer(i-jrnlno)
  and b-zsdiput.completefl = no and zsdiput.extype = "" no-lock.
else if integer(i-receipt) > 0 then
  open query q-put                                                      
  for each b-zsdiput where b-zsdiput.cono = g-cono and b-zsdiput.whse = i-whse
  and b-zsdiput.receiptid = integer(i-receipt) 
  and b-zsdiput.completefl = no and zsdiput.extype = "" no-lock.               */
open query q-put for each b-zsdiput no-lock.        

on any-key of b-put do:
  if {k-cancel.i} or {k-jump.i} then do:
    hide frame f-put.
    o-out = "".
    apply "window-close" to frame f-put.
  end.
  else
  if keylabel(lastkey) = "RETURN" or keylabel(lastkey) = "PF1" then do:
    if avail b-zsdiput then do:
      case i-style:
        when "Prod" then o-out = b-zsdiput.prod.
      end case.
    end.
    else o-out = "".
    hide frame f-put.
    apply "window-close" to frame f-put.
  end.
end. /* any-key */

enable b-put with frame f-put.
apply "entry" to b-put.
display b-put with frame f-put.
wait-for window-close of frame f-put.
on cursor-up back-tab.
on cursor-down tab.
close query q-put.
release b-zsdiput.                  
end procedure. /* putawaybrowse */

/************** TOMS *************************/

procedure LoadBinList:
  define input parameter ip-cono like icsw.cono no-undo.
  define input parameter ip-prod like icsw.prod no-undo.
  define input parameter ip-whse like icsw.prod no-undo.

  define buffer ix-icsw     for icsw.
  define buffer ix-wmsb     for wmsb.
  define buffer ix-wmsbp    for wmsbp.
  define buffer ix-tsum-put for tsum-put.
  define var    ix-cnt   as integer no-undo.

  find first t-binlist where 
             t-binlist.prod = ip-prod and
             t-binlist.whse = ip-whse no-lock no-error.
  if not avail t-binlist then do:
    for each t-binlist :
      delete t-binlist.
    end.  
    find ix-icsw where 
         ix-icsw.cono = ip-cono and
         ix-icsw.prod = ip-prod and
         ix-icsw.whse = ip-whse no-lock no-error.
    if avail ix-icsw then do:
      /* Bin Location 1 */
      if ix-icsw.binloc1 <> "" and 
         ix-icsw.binloc1 <> "New Part" then do:
        create t-binlist.
        assign t-binlist.whse   = ip-whse
               t-binlist.prod   = ip-prod
               t-binlist.binloc = ix-icsw.binloc1
               t-binlist.binnum = 1
               ix-cnt = 1.
      end.          

      /* Bin Location 2 */
      if ix-icsw.binloc2 <> "" and 
         ix-icsw.binloc2 <> "New Part" then do:
        create t-binlist.
        assign t-binlist.whse   = ip-whse
               t-binlist.prod   = ip-prod
               t-binlist.binloc = ix-icsw.binloc2
               t-binlist.binnum = 2
               ix-cnt = 2.
      end.          
    end. 
   
    for each ix-wmsbp where 
               ix-wmsbp.cono = ip-cono + 500 and
               ix-wmsbp.whse = ip-whse and
               ix-wmsbp.prod = ip-prod no-lock:
      find   ix-wmsb where 
             ix-wmsb.cono = ip-cono + 500 and
             ix-wmsb.whse = ip-whse and
             ix-wmsb.binloc = ix-wmsbp.binloc and
             ix-wmsb.priority = 9 no-lock no-error.
      if avail ix-wmsb then do:
        create t-binlist.
        assign t-binlist.whse   = ip-whse
               t-binlist.prod   = ip-prod
               t-binlist.binloc = ix-wmsbp.binloc
               t-binlist.binnum = ix-cnt + 1
               ix-cnt = ix-cnt + 1.
      end.
    end. /* for each IX-WMSBP  */                            
  end.   /*   if not avail t-binlist  */
  else
    for each t-binlist:
      assign t-binlist.binused = false.
    end.
      
  for each t-binlist:
    find first ix-tsum-put where
               ix-tsum-put.prod = ip-prod and
               ix-tsum-put.binloc1 = t-binlist.binloc no-lock no-error.
    if avail ix-tsum-put then
      assign t-binlist.binused  = true.
  end.                  
end. /* procedure LoadBinList */
   
procedure F6PopUp:
/* choose an exception code */
define output parameter outExceptCode as char no-undo.
                                                                     
def var exReason as char format "x(2)" no-undo.
def var exError as logical no-undo.                      
def var cancelFlag as logical no-undo.
                                                                     
form                                                                 
  exReason label "Exception Code" dcolor 2                           
with frame f-reasoncode at row 6 col 1 side-labels overlay width 24. 
cancelFlag = no.                                      
exError = yes.                                        
do while exError:                                     
  update exReason with frame f-reasoncode             
  editing:                                            
    readkey.                                          
    if {k-accept.i} or {k-return.i} then do:          
      find first zsastz where zsastz.cono = g-cono and
      zsastz.codeiden = "PutawayExceptions" and       
      zsastz.primarykey = input exReason    and       
      zsastz.labelfl = no no-lock no-error.           
      if avail zsastz then                            
        exError = no.                                 
      else                                            
        exError = yes.                                
      apply lastkey.                                  
    end.                                              
    else if ({k-cancel.i} or {k-jump.i}) then do:            
      cancelFlag = yes.                                      
      exError = no.                                          
      leave.                                                 
    end.                                                     
    else if ({k-func10.i} or {k-func12.i}) then do:          
      run reasoncd.p 
       (input g-cono, input "PutawayExceptions",
        input "  Select Exception Code  ", input yes,          
        output exReason). /* choose exception code */
                 
      display exReason with frame f-reasoncode.              
    end.                                                     
    else apply lastkey.                                      
  end. /* edit */                                            
  if exError then message "Invalid Reason".                  
end.                                                         
hide frame f-reasoncode.              
message "".                           
if cancelFlag then exReason = "".     
outExceptCode = exReason.             
end procedure.                        
       
procedure F6Exception:

  define input  parameter ip-cono like icsw.cono no-undo.
  define input  parameter ip-shipprod like icsw.prod   no-undo.
  define input  parameter ip-whse like icsw.prod       no-undo.
  define input  parameter ip-binloc  as character      no-undo.
  define input parameter ip-exstatustype  as char format "x(1)"   no-undo.
  define input  parameter ip-extype        as cha  format "x(2)"   
         no-undo.

  define output parameter ip-errorlit as character format "x(24)" no-undo.
  
  define buffer xi-zsdiput    for zsdiput.
  define buffer xi-zsdiputdtl for zsdiputdtl.
  define buffer xi-t-put      for t-put.
  define buffer xi-tsum-put   for tsum-put.
  define buffer xi-wmsb       for wmsb.

  assign ip-errorlit = "".    
/*
  run hhepreason.p (input-output ip-extype, 
                    input-output ip-errorlit ).
*/
  if ip-extype = "" then                  
  run reasoncd.p (input ip-cono,"PutawayExceptions","Exceptions", yes,
                  output ip-extype).


  /* assign ip-extype   = "E1". */

  for each tmp-rcpt no-lock:     /* only work on receiptids loaded */
    for each xi-zsdiput where 
             xi-zsdiput.cono      = ip-cono and
             xi-zsdiput.whse      = ip-whse and
             xi-zsdiput.receiptid = tmp-rcpt.receiptid and
             xi-zsdiput.shipprod  = ip-shipprod and 
             xi-zsdiput.binloc1   = ip-binloc:
      assign xi-zsdiput.exstatustype  = ip-exstatustype
             xi-zsdiput.extype        = ip-extype
             xi-zsdiput.putawayinit   = g-operinits.
      for each xi-zsdiputdtl use-index dtlidix where 
               xi-zsdiputdtl.cono      = ip-cono and
               xi-zsdiputdtl.whse      = ip-whse and
               xi-zsdiputdtl.receiptid = tmp-rcpt.receiptid and
               xi-zsdiputdtl.shipprod  = ip-shipprod and 
               xi-zsdiputdtl.binloc1   = ip-binloc:                    
        assign xi-zsdiputdtl.exstatustype  = ip-exstatustype
               xi-zsdiputdtl.extype        = ip-extype.
               /* xi-zsdiputdtl.putawayinit   = g-operinits. */
         release xi-zsdiputdtl.
      end.
    end.  
 
    for each xi-t-put where 
             xi-t-put.whse      = ip-whse and
             xi-t-put.receiptid = tmp-rcpt.receiptid and
             xi-t-put.prod      = ip-shipprod and 
             xi-t-put.binloc1   = ip-binloc:
      assign xi-t-put.extype    = ip-exstatustype.
    end.     


  end.    

  find first xi-tsum-put where 
             xi-tsum-put.prod = ip-shipprod and
             xi-tsum-put.binloc1 = ip-binloc no-error.
  if avail xi-tsum-put then
    assign xi-tsum-put.extype = ip-exstatustype. 
end.  /* procedure F6Exception */


procedure F7BinSplit:
/* message "running f7split". pause.  */
        
  define input  parameter ip-cono like icsw.cono no-undo.
  define input  parameter ip-shipprod like icsw.prod   no-undo.
  define input  parameter ip-whse like icsw.prod       no-undo.
  define input  parameter ip-binloc  as character      no-undo.
  define output parameter ip-binloc1 like icsw.binloc1 no-undo.
  define output parameter ip-binloc2 like icsw.binloc1 no-undo.
  define output parameter ip-errorlit as character format "x(24)" no-undo.

  define buffer xn-zsdiput    for zsdiput.
  
  define buffer xi-zsdiput    for zsdiput.
  define buffer xi-zsdiputdtl for zsdiputdtl.
  define buffer xi-t-put      for t-put.
  define buffer xi-tsum-put   for tsum-put.
  define buffer xi-wmsb       for wmsb.

  define buffer x2-zsdiput    for zsdiput.
  define buffer x2-zsdiputdtl for zsdiputdtl.
  define buffer x2-t-put      for t-put.
  define buffer x2-tsum-put   for tsum-put.

  define var    ix-cnt   as integer no-undo.
  define var    ix-currentbinno   as integer no-undo.

  def var madeSplit as logical no-undo.
 
/* @zone */
  def var iZone as integer initial 0 no-undo.
  def var cZone as character initial " " no-undo.

  
  
  
  madeSplit = no.
  h-message = "". /* dkt 5/08 */
  
  assign ix-cnt = 0
         ip-binloc1 = ""    
         ip-binloc2 = ""
         ip-errorlit = "".    

  for each t-binlist use-index ix-binnum where 
           t-binlist.binused = false:
    if ix-cnt = 0 then do:  /* added */
      assign ip-binloc1 = t-binlist.binloc
             ix-currentbinno = ix-cnt + 1
             t-binlist.binused = true.
    end. /* added */
    else
      assign ip-binloc2 = t-binlist.binloc.
    ix-cnt = ix-cnt + 1.
    if ix-cnt ge 2 then
      leave.
  end.    

  find first xi-tsum-put where 
             xi-tsum-put.prod   = ip-shipprod and
             xi-tsum-put.binloc1 = ip-binloc1  no-lock no-error.
  if avail xi-tsum-put then do:
    assign ix-cnt = 0
           ip-errorlit = "Bin/Prod inuse '" 
                         + ip-binloc1
                         + "'"
           ip-binloc1 = ""    
           ip-binloc2 = "".
    return.
  end.
  else do:
    find first xi-tsum-put where 
             xi-tsum-put.prod   = ip-shipprod and
             xi-tsum-put.binloc1 = (if ip-binloc1 = "New Part" then
                                      ""
                                    else
                                      ip-binloc1) 
                                      no-lock no-error.
    if avail xi-tsum-put then do:
      assign ix-cnt = 0
             ip-errorlit = "Bin/Prod inuse '" 
                           + ip-binloc1
                           + "'"
             ip-binloc1 = ""    
             ip-binloc2 = "".
      return.
    end.
  end. 
  
  for each tmp-rcpt no-lock:     /* only work on receiptids loaded */
    for each xi-zsdiput where 
             xi-zsdiput.cono      = ip-cono and
             xi-zsdiput.whse      = ip-whse and
             xi-zsdiput.receiptid = tmp-rcpt.receiptid and
             xi-zsdiput.shipprod  = ip-shipprod and 
             xi-zsdiput.binloc1   = ip-binloc  no-lock:

    if not madeSplit then do:

      find first xi-wmsb where 
                 xi-wmsb.cono = ip-cono and
                 xi-wmsb.whse = ip-whse and 
                 xi-wmsb.binloc = ip-binloc1 no-lock no-error.
       
      find last xn-zsdiput use-index putin
       where xn-zsdiput.cono = g-cono no-lock no-error.
      
      madeSplit = yes.
      
      create x2-zsdiput.
      buffer-copy xi-zsdiput  
        except xi-zsdiput.binloc1
               xi-zsdiput.binloc2
               xi-zsdiput.recordtype
               xi-zsdiput.putrecno
               /* dkt 05/23/07 doubled quantities */
               
               xi-zsdiput.stkqtyrcv
               xi-zsdiput.stkqtyalloc
               xi-zsdiput.stkqtypick
               xi-zsdiput.stkqtyputaway
               
      to x2-zsdiput 
        assign x2-zsdiput.binloc1      = ip-binloc1
               x2-zsdiput.binloc2      = ip-binloc2
               x2-zsdiput.locationid   = if avail xi-wmsb and 
                                            ip-binloc1 <> "" and
                                            ip-binloc1 ne "New Part" then
                                           xi-wmsb.building
                                         else if ip-binloc1 = "" or
                                            ip-binloc1 = "New Part" then
                                           "````" 
                                         else
                                           "    "  
               x2-zsdiput.xuser1       = if xi-zsdiput.binloc1 <> "" and
                                            xi-zsdiput.binloc1 <> "New Part"                                                  and ip-binloc1 = "" then
                                           substring(xi-zsdiput.binloc1,1,2) +
                                               "````````"
                                         else
                                           ""
                                         
               x2-zsdiput.recordtype   = "Bin" +                                                                          string(ix-currentbinno,">>9")
               x2-zsdiput.user2        = g-operinits
               x2-zsdiput.putrecno     = xn-zsdiput.putrecno + 1.
       

   /*   message "Completefl"  x2-zsdiput.completefl. pause.  */
      
      /* for each xi-zsdiputdtl where */
      find first xi-zsdiputdtl where
               xi-zsdiputdtl.cono      = ip-cono and
               xi-zsdiputdtl.whse      = ip-whse and
               xi-zsdiputdtl.receiptid = tmp-rcpt.receiptid and
               xi-zsdiputdtl.shipprod  = ip-shipprod and 
               xi-zsdiputdtl.binloc1   = ip-binloc  and
               xi-zsdiputdtl.putrecno  = xi-zsdiput.putrecno no-lock no-error.
        if not avail xi-zsdiputdtl then do:
          message "Cannot find zsdiputdtl". pause. 
        end.  
        create x2-zsdiputdtl.
        buffer-copy xi-zsdiputdtl 
          except xi-zsdiputdtl.binloc1
                 xi-zsdiputdtl.binloc2
                 xi-zsdiputdtl.recordtype
                 /* dkt 05/23/07 doubled quantities */
                 
                 xi-zsdiputdtl.stkqtyrcv
                 xi-zsdiputdtl.stkqtyalloc
                 xi-zsdiputdtl.stkqtypick
                 xi-zsdiputdtl.stkqtyputaway
                 
        to x2-zsdiputdtl 
          assign x2-zsdiputdtl.binloc1      = x2-zsdiput.binloc1
                 x2-zsdiputdtl.binloc2      = x2-zsdiput.binloc2
                 x2-zsdiputdtl.locationid   = x2-zsdiput.locationid
                      
                 x2-zsdiputdtl.recordtype   = "Bin" +                          
                                              string(ix-currentbinno,">>9")
                 x2-zsdiputdtl.putrecno     = x2-zsdiput.putrecno.
       
      /* end.  xi-zdiputdtl */
  
      find last x2-t-put use-index  k-idno  no-lock no-error.
 /* @zone */
      iZone = int(x2-zsdiput.locationid)  no-error.
      if error-status:error then
        cZone = x2-zsdiput.locationid.
      else
        cZone = string (int(x2-zsdiput.locationid),"99999999").
 
 
      create xi-t-put.
      assign
        xi-t-put.whse         = v-whse
        xi-t-put.receiptid    = x2-zsdiput.receiptid
        xi-t-put.prod         = x2-zsdiput.shipprod
        xi-t-put.bin          = x2-zsdiput.binloc1
        xi-t-put.binloc1      = x2-zsdiput.binloc1
        xi-t-put.binloc2      = x2-zsdiput.binloc2
        xi-t-put.qty          = 0 /* x2-zsdiput.stkqtyputaway  */
        xi-t-put.oper         = g-operinits /* zsdiput.operinit */
        xi-t-put.extype       = x2-zsdiput.extype
        xi-t-put.complete     = x2-zsdiput.completefl
        xi-t-put.transdt      = x2-zsdiput.transdt
        xi-t-put.transtm      = x2-zsdiput.transtm 
        xi-t-put.zone         = cZone /* @zone  x2-zsdiput.locationid */
        xi-t-put.idno         = if avail x2-t-put then
                                   x2-t-put.idno + 1
                                else
                                   1.
 
      find first xi-tsum-put where 
                 xi-tsum-put.prod = zsdiput.shipprod and
                 xi-tsum-put.binloc1 = ip-binloc no-error.
      
      find first x2-tsum-put where 
                 x2-tsum-put.prod = zsdiput.shipprod and
                 x2-tsum-put.binloc1 = x2-zsdiput.binloc1 no-lock no-error.
      if not avail x2-tsum-put then do:           
        find first xi-wmsb where 
                   xi-wmsb.cono = ip-cono and
                   xi-wmsb.whse = ip-whse and 
                   xi-wmsb.binloc = x2-zsdiput.binloc1 no-lock no-error.
 /* @zone */
        cZone =  if avail xi-wmsb and 
                   ip-binloc1 <> "" and
                   ip-binloc1 ne "New Part" then
                   xi-wmsb.building
                  else if ip-binloc1 = "" or
                       ip-binloc1 = "New Part" then
                    "````" 
                  else
                    "    ".
        iZone = int(cZone)  no-error.
        if error-status:error then
          cZone = cZone.
        else
          cZone = string (int(cZone),"99999999").
 
       
        
        create x2-tsum-put.
        assign x2-tsum-put.prod = xi-tsum-put.prod
               x2-tsum-put.qty  = 0 /* xi-tsum-put.qty */
               x2-tsum-put.binloc1 = x2-zsdiput.binloc1
               x2-tsum-put.zone  =  cZone
                            /* @zone      if avail xi-wmsb and 
                                       ip-binloc1 <> "" and
                                       ip-binloc1 ne "New Part" then
                                      xi-wmsb.building
                                    else if ip-binloc1 = "" or
                                         ip-binloc1 = "New Part" then
                                      "````" 
                                    else
                                      "    "  */
               x2-tsum-put.binsort = if (x2-tsum-put.binloc1 <> "" and
                                         x2-tsum-put.binloc1 <> "New Part") 
                                     then
                                       substring(x2-tsum-put.binloc1,1,2) +
                                            "````````"
                                     else
                                     if (x2-tsum-put.binloc1 = "" or
                                         x2-tsum-put.binloc1 = "New Part") and
                                         x2-zsdiput.xuser1 <> ""  then
                                       x2-zsdiput.xuser1 
                                     else
                                       "``````````"
               x2-tsum-put.split      = true 
               x2-tsum-put.completefl = false. 
               xi-tsum-put.split      = true.
      end.
    end.    /* not madesplit */
    end.    /* xi-zsdiput */
  end.
  /*
  message "split .split?" xi-tsum-put.split xi-tsum-put.prod xi-tsum-put.binloc1. pause 3.
*/
  if madeSplit then h-message = "Bin2 record created".
  
  if madeSplit and t-stkqtyputaway ne t-stkqty then do:
    /* x2-zsdiputdtl.stkqtyputaway =  */
    xi-t-put.qty                = t-stkqtyputaway - t-stkqty.
    x2-tsum-put.qty             = t-stkqtyputaway - t-stkqty.
  end.
end procedure.  /* procedure F7BinSplit */


procedure GetBinLocs:
  define input  parameter ip-cono       like icsw.cono    no-undo.
  define input  parameter ip-shipprod   like icsw.prod    no-undo.
  define input  parameter ip-whse like  icsw.prod         no-undo.
  define input  parameter ip-recordcode as character      no-undo.
  define output parameter ip-binloc1    like icsw.binloc1 no-undo.
  define output parameter ip-binloc2    like icsw.binloc1 no-undo.

  define var    ix-currentbinno   as integer no-undo.
  define var    ix-cnt   as integer no-undo.


  run LoadBinList (input ip-cono,
                   input ip-shipprod,
                   input ip-whse).

  if ip-recordcode  = "" then
    assign ix-currentbinno = 1.
  else if ip-recordcode begins "Bin" then
    assign ix-currentbinno = int(substr(ip-recordcode,4,3)).    

  assign ix-cnt = 0
         ip-binloc1 = ""    
         ip-binloc2 = "".    

  for each t-binlist: 
    ix-cnt = ix-cnt + 1.
    if ix-cnt < ix-currentbinno then 
      next.
    if ix-cnt = ix-currentbinno  then
      assign ip-binloc1 = t-binlist.binloc
             t-binlist.binused = true.
    else
      assign ip-binloc2 = t-binlist.binloc.
    if ix-cnt ge (ix-currentbinno + 1) then
      leave.
  end.    
end. 

procedure DetermineProductAction:
define input  parameter ip-shipprod   like icsp.prod no-undo.
define output parameter ip-actioncode as character   no-undo.

def var doFind as logical no-undo.
def buffer ix-tsum-put for tsum-put.

assign ip-actioncode = "".
find ix-tsum-put where recid(ix-tsum-put) = recid(tsum-put) no-lock no-error.

/* message "find in prodaction gets" ix-tsum-put.prod tsum-put.prod. pause. */

if ip-shipprod ne "" then do:
  doFind = no.
  if avail ix-tsum-put then do:
    if ix-tsum-put.prod ne ip-shipprod then 
      assign doFind = yes.
  end.
  else doFind = yes.
end.
else 
  doFind = no.

/* message "prodaction doFind?" doFind. pause. */

if doFind then do:
  /* entered a product */
  find first ix-tsum-put where ix-tsum-put.prod = ip-shipprod and
                               ix-tsum-put.completefl = no and
                               ix-tsum-put.extype     = "" 
                               no-lock no-error.
  if avail ix-tsum-put then
     assign ip-actioncode = "Prodjump".
  
  /* entered a bin */
  if not avail ix-tsum-put then do:
    find first ix-tsum-put where 
               ix-tsum-put.binloc1 = ip-shipprod and
               ix-tsum-put.completefl = no and
               ix-tsum-put.extype     = "" no-lock no-error.
    if avail ix-tsum-put then
      assign ip-actioncode = "Prodjump".
  end. 
  if not avail ix-tsum-put then do:
    find first ix-tsum-put where 
               ix-tsum-put.binloc1 = " " + ip-shipprod and
               ix-tsum-put.completefl = no and
               ix-tsum-put.extype     = "" no-lock no-error.
    if avail ix-tsum-put then
      assign ip-actioncode = "Prodjump".
  end. 

  if not avail ix-tsum-put then do:
    find first ix-tsum-put where 
               ix-tsum-put.binloc2 = ip-shipprod and
               ix-tsum-put.completefl = no and
               ix-tsum-put.extype     = "" no-lock no-error.
    if avail ix-tsum-put then
      assign ip-actioncode = "Prodjump".
  end.    
  if not avail ix-tsum-put then do:
    find first ix-tsum-put where 
               ix-tsum-put.binloc2 = " " + ip-shipprod and
               ix-tsum-put.completefl = no and
               ix-tsum-put.extype     = "" no-lock no-error.
    if avail ix-tsum-put then
      assign ip-actioncode = "Prodjump".
  end.    
                                              
  if avail ix-tsum-put then do: 
    /* dkt */
    find tsum-put where recid(tsum-put) = recid(ix-tsum-put) no-lock
    no-error.
    
    t-shipprod = ix-tsum-put.prod.
    assign                                       
      v-recid    = 0                             
      v-recid[1] = recid(ix-tsum-put)                  
      v-curpos   = 2 
      confirm    = no                            
      v-nextfl   = yes.                          

    for each tmp-rcpt no-lock:     /* only work on receiptids loaded */
      find first zsdiput where 
                 zsdiput.cono        = g-cono and 
                 zsdiput.whse        = v-whse and 
                 zsdiput.receiptid   = tmp-rcpt.receiptid and
                 zsdiput.shipprod    = ix-tsum-put.prod and
                 zsdiput.binloc1     = ix-tsum-put.binloc1 no-lock no-error.
      if avail zsdiput then do:
        /* message "about to leave prodaction with" zsdiput.shipprod. pause. */
        leave.
      end.
    end.              

    /* message "about to display - need to find ix and tsumput, yes?". pause.     */

    assign t-stkqtyputaway = ix-tsum-put.qty
           t-shipprod      = ix-tsum-put.prod
           t-proddesc1     = if avail zsdiput then
                               zsdiput.proddesc1
                             else
                               ""
           t-proddesc2     = if avail zsdiput then
                               zsdiput.proddesc2
                             else
                               ""
      /* t-stkqtyputaway   = ix-tsum-put.stkqtyputaway. */
                                         
           t-stkqty        = ix-tsum-put.qtyrcvd
           t-binloc1       = ix-tsum-put.binloc1
           t-binloc2       = ix-tsum-put.binloc2.
    v-shipprod = ip-shipprod.
    if ix-tsum-put.split and ix-tsum-put.extype ne "" then
      t-stkqty = t-stkqtyputaway.
    else
    if not ix-tsum-put.split and ix-tsum-put.qtyrcvd = 0 then
      t-stkqty = t-stkqtyputaway.
      
    {hhep.lds}  /* Need TSUM-PUT to run this */

    if ip-actioncode = "" then
      assign ip-actioncode = "Bin".
  end. 
  else do:                                   
    confirm = no.                            
    assign ip-actioncode = "ProdErr".
    message "1Re-scan part number".
  end.      
end.
end procedure.


procedure DetermineBinAction:
define input-output  parameter ip-binloc    like icsw.binloc1 no-undo.
define input  parameter ip-shipprod   like icsp.prod no-undo.
define output parameter ip-actioncode as character   no-undo.

def var rptFlag as logical initial no no-undo.

def buffer ix-tsum-put for tsum-put.

find ix-tsum-put where recid(ix-tsum-put) = recid(tsum-put) no-lock no-error.
assign ip-actioncode = "".

if keylabel(lastkey) = "cursor-up" /* and ip-binloc = "" */ then do:
  ip-actioncode = "upKey".
  return.
end.
 
 /*
 message "ip" ip-shipprod "ix" ix-tsum-put.prod
         "ixbin" ix-tsum-put.binloc1. pause.  
 */
 
if ip-shipprod ne ix-tsum-put.prod then do:
  ip-actioncode = "Product".
  /* advance = no. */
  message "2Re-scan Product".
  return.
end.

/* CANNOT run updateZsdi inside of determineBinLocation!!!
   Must move it so that it only happens on cursor-down or F1
   and all info right - that means HHEP.LFU? */

if ip-binloc = ix-tsum-put.binloc1 and 
   ip-binloc ne "" and 
   ip-binloc ne "New Part" and
   ip-shipprod = ix-tsum-put.prod then do:
  /*
  run updateZsdiput
    (input ix-tsum-put.prod, 
           ix-tsum-put.qtyrcvd, 
           ip-binloc).
/*           ix-tsum-put.putno, */
     output rptFlag).  */ 
  ip-actioncode = "good1".
end.
else
if " " + ip-binloc = ix-tsum-put.binloc1 and 
   ip-binloc ne "" and 
   ip-binloc ne "New Part" and
   ip-shipprod = ix-tsum-put.prod then do:
  /*
                  
  run updateZsdiput
    (input ix-tsum-put.prod, 
           ix-tsum-put.qtyrcvd, 
           " " + ip-binloc). 
/*           ix-tsum-put.putno, */
     output rptFlag).     */
  assign ip-binloc = " " + ip-binloc. 
         ip-actioncode = "good2".
end.

else if ip-binloc ne "" and ix-tsum-put.binloc1 ne "" and
  ix-tsum-put.binloc1 ne "New Part"
  then do: 

  /*  message "ip-binloc" ip-binloc "ix-tsum" ix-tsum-put.binloc1. pause.  */
            

    /* give message that this is not the normal bin? */
    find first wmsbp where wmsbp.cono = g-cono + 500 and
             wmsbp.whse = g-whse and
             wmsbp.prod = ip-shipprod and 
             wmsbp.binloc = ip-binloc no-lock no-error.
    if not avail wmsbp then
      find first wmsbp where wmsbp.cono = g-cono + 500 and
                 wmsbp.whse = g-whse and
                 wmsbp.prod = ip-shipprod and 
                 wmsbp.binloc = " " + ip-binloc no-lock no-error.
             
    /* if not the first bin - then what? */
    if avail wmsbp then do:
    /*  message "wmsbp.binloc" wmsbp.binloc
              "wmsbp.prod" wmsbp.prod
              "ip-shipprod" ip-shipprod. pause.   */
      /* message "|" + ip-binloc + "|". pause. */
      if wmsbp.prod ne ip-shipprod then
        message "Bin Error - Product".
      else
      if wmsbp.binloc ne ip-binloc then
        message "Bin Error - Check Bin".
      /* run updateZsdiput(input ix-tsum-put.prod, tsum-put.qty, v-binloc). */
      /* run updateRecords, full qty assigned to this bin */
       ip-actioncode = "BinErr".
    end.
    else do:
      /* message "|" + ip-binloc + "|". pause. */
      message "Bin Error - Invalid".
      ip-actioncode = "BinErr".
    end.
end.
else do:
  if (ix-tsum-put.binloc1 = "" or ix-tsum-put.binloc1 = "New Part") then do:
    /* assign new bin if valid bin */
    /* message "v-whse" v-whse "g-whse" g-whse. pause. */
    if can-find(first wmsb where wmsb.cono = g-cono + 500 and
                        wmsb.whse = v-whse and 
                        wmsb.binloc = ip-binloc and
                        wmsb.priority = 0 and 
                        wmsb.statuscode = "" no-lock)
    or can-find(first wmsbp where wmsbp.cono = g-cono + 500 and
                        wmsbp.whse = v-whse and
                        wmsbp.binloc = ip-binloc and
                        wmsbp.prod = ip-shipprod no-lock)
    then do:
      run assignBin(input ip-shipprod, input ip-binloc, input v-whse).
      /*
      run updateZsdiput
        (input ix-tsum-put.prod, 
               ix-tsum-put.qty,
               ip-binloc).
/*               ix-tsum-put.putno, */
         output rptFlag). */
      ip-actioncode = "good3".
    end.
    else
    if can-find(first wmsb where wmsb.cono = g-cono + 500 and
                        wmsb.whse = v-whse and 
                        wmsb.binloc = " " + ip-binloc and
                        wmsb.priority = 0 and 
                        wmsb.statuscode = "" no-lock)
    or can-find(first wmsbp where wmsbp.cono = g-cono + 500 and
                        wmsbp.whse = v-whse and
                        wmsbp.binloc = " " + ip-binloc and
                        wmsbp.prod = ip-shipprod no-lock)
    then do:
      run assignBin(input ip-shipprod, input " " + ip-binloc, input v-whse).
      /*
      run updateZsdiput
        (input ix-tsum-put.prod, 
               ix-tsum-put.qty,
               " " + ip-binloc)
/*               ix-tsum-put.putno, */
         output rptFlag).
      assign ip-binloc = " " + ip-binloc. */
      ip-actioncode = "good4".
    end.
    else do:
      assign ip-actioncode = "BinErr".
      if can-find(first wmsb where wmsb.cono = g-cono + 500 and
                        wmsb.whse = v-whse and 
                        wmsb.binloc = " " + ip-binloc  no-lock) or
         can-find(first wmsb where wmsb.cono = g-cono + 500 and
                        wmsb.whse = v-whse and 
                        wmsb.binloc = ip-binloc no-lock) then do:
       
        
        
        if can-find(first wmsb where wmsb.cono = g-cono + 500 and
                        wmsb.whse = v-whse and 
                        wmsb.binloc = " " + ip-binloc  no-lock) then
         assign ip-binloc = " " + ip-binloc.
        
        message "Invalid Empty Bin".
        bell.
      end.
      else do:                      
        message "Invalid Bin".
        bell.
      end.
    end.
  end.
  else do:
    if ip-binloc = "PROD" then do:
      ip-actioncode = "Product".
    end.
    else do:
      /* this is not right - why didn't you hit F7 or scan another bin */
      assign ip-actioncode = "BinErr".
      message "Invalid Bin-Rescan".
      bell.
    end.
  end.
end.
if rptFlag then ip-actioncode = "PrintReport".
end procedure.

procedure vendor_special_information:                                          
 define input parameter inVendNo like apsv.vendno no-undo.                     
 define input parameter inField as char no-undo.   /* Product */               
 define input-output parameter ioScanProd as char no-undo.                     
   
def var ediPartner like apsv.edipartner no-undo.                                
def buffer sv-apsv for apsv.

find first apsv where apsv.cono = g-cono and apsv.vendno = inVendNo no-lock 
  no-error.                                                                    
   if avail apsv then do:                                                            ediPartner = trim(apsv.edipartner).

if inVendNo <> 0 then do:
  find sv-apsv where 
       sv-apsv.cono = g-cono and
       sv-apsv.vendno = inVendNo no-lock no-error.
end.
else do:
  find sv-apsv where 
       sv-apsv.cono = g-cono and
       sv-apsv.edipartner = ediPartner no-lock no-error.
end.

if avail sv-apsv then do:

  dec(ioScanProd) no-error.
  if error-status:error then do: 
    /* only integers can be upc numbers */
  end.
else do:
    
    find icsv use-index k-produpc where 
         icsv.cono = g-cono and 
         icsv.section4 = dec(ioScanProd) and
         icsv.section3 = sv-apsv.vendno and
         icsv.vendno = sv-apsv.vendno 
         no-lock no-error.
         
    if avail icsv then do:
      assign ioScanProd = icsv.prod.   
      return.
    end.
   end.  
end.
/* ipx-field = product */
/* UPC MOD */


  find first zsastz where zsastz.cono = g-cono and zsastz.labelfl = false and
    zsastz.codeiden = "VendorChars" and
    zsastz.primarykey = string(inVendNo) no-lock no-error.
  
  if avail zsastz then do:     

   def var new-fieldaval like ioScanProd. 
  
     assign new-fieldaval = ioScanProd. 
        /*  message "new-fieldaval" new-fieldaval.   */
  
  
    if inField = "Product" and 
       substring(ioScanProd,1,1) = substring(zsastz.codeval[7],1,1) then do:

      /*  message "found 7". pause.   */
    
      if ioScanProd begins zsastz.codeval[7] then do:
        ioScanProd = substring(ioScanProd, length(zsastz.codeval[7]) + 1,
          length(ioScanProd) - length(zsastz.codeval[7])).       
      
           /* message "the7" ioScanProd. pause. */ 
      end.           
  end.
  
  else
  
  /*
  message "checkign"  substring(ioScanProd,1,1)
                      substring(zsastz.codeval[8],1,2). pause.
  message "length" length(zsastz.codeval[8]) + 1   
          "senlen" length(ioScanProd) - length(zsastz.codeval[8]). pause.
  */                    
                      
  if substring(ioScanProd,1,2) = substring(zsastz.codeval[8],1,2) then 

/*  if ioScanProd begins substring(zsastz.codeval[8],1,2) then  */

    assign 
    ioScanProd = substring(ioScanProd, length(zsastz.codeval[8]) + 1,
                    length(ioScanProd) - length(zsastz.codeval[8])).       
 
        /*   message "ioScanProd #8" ioScanProd. pause.    */
        
        find first icsp where icsp.cono = g-cono and
                              icsp.prod = ioScanProd
                              no-lock no-error.
                              
        if avail icsp then 
            message "". 
            /*
            message "found it". pause.   
            */
                              
        if not avail icsp then do:
            /*  message "notfound". pause.   */
           assign ioScanProd =  new-fieldaval. 
            /*  message "new" new-fieldaval. pause.  */ 
           
          /* message "running3" inField. pause.     */
        assign ioScanProd = new-fieldaval. 
         /* message "after assign" ioScanProd. pause.  */

        
  if substring(ioScanProd,1,1) = substring(zsastz.codeval[3],1,1) then
     /* message "at 3??". pause.   */
     if ioScanProd begins zsastz.codeval[3] then 
        ioScanProd = substring(ioScanProd, length(zsastz.codeval[3]) + 1,
          length(ioScanProd) - length(zsastz.codeval[3])).       
 
        /*   message "ioScanProd #3" ioScanProd. pause.       */
         
        
 end. /* else do */
        
                
 end.               
                 
 else
 if not avail(zsastz) and inField = "Product" then return. 
 
end. 
end procedure.

procedure improvise_product:
/* message "improvise". pause. */
define input-output parameter v-prodsrch as char format "x(24)" no-undo.

  v-prodsrch = replace (v-prodsrch," ","").
/*  v-prodsrch = replace (v-prodsrch,"s","5").
    v-prodsrch = replace (v-prodsrch,"o","0").
    v-prodsrch = replace (v-prodsrch,"i","1").
    v-prodsrch = replace (v-prodsrch,"l","1").
    v-prodsrch = replace (v-prodsrch,"z","2").
    v-prodsrch = replace (v-prodsrch,"v","u"). 
*/
  v-prodsrch = replace (v-prodsrch,"-","").
  v-prodsrch = replace (v-prodsrch,"/","").
  v-prodsrch = replace (v-prodsrch,"*","").
  v-prodsrch = replace (v-prodsrch,'"',"").
  v-prodsrch = replace (v-prodsrch,"x","").
  v-prodsrch = replace (v-prodsrch,"~~","").
  v-prodsrch = replace (v-prodsrch,"'","").
  v-prodsrch = replace (v-prodsrch,"(","").
  v-prodsrch = replace (v-prodsrch,")","").
  v-prodsrch = replace (v-prodsrch,"`","").
  v-prodsrch = replace (v-prodsrch,".","").
  v-prodsrch = replace (v-prodsrch,"#","").
  v-prodsrch = replace (v-prodsrch,"<","").
  v-prodsrch = replace (v-prodsrch,">","").
  v-prodsrch = replace (v-prodsrch,"~\","").
end procedure. 


Procedure RFLoop: 
def var zx-x as int initial 1 no-undo.
assign zx-l = no
       zx-x = 1.
repeat while program-name(zx-x) <> ?:         
  if program-name(zx-x) = "zzrfli.p" then do:                                      zx-l = yes.                              
    leave. 
  end.                                      
  assign zx-x = zx-x + 1.                     
end.                                          
end procedure.


