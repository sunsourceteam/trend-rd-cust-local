/* kpet-zsdipck.i  */
/*  **************************************************************************
 
   Pick Pack Detail Creation 

   Created:        06/27/07
   Author:         Tah
   Modifications:

   Parameters the same as d-oeepp1.i to accomodate all modules:

       &returnfl   = "oeel.returnfl"
       &cubes      = "oeel.cubes"
       &weight     = "oeel.weight"
       &head       = "oeeh"
       &line       = "oeel"
       &section    = "oeeh"
       &ourproc    = "oeepp"
       &specnstype = "oeel.specnstype"
       &altwhse    = "oeel.altwhse"
       &vendno     = "oeel.vendno"
       &kitfl      = "x-false"
       &botype     = "oeel.botype"
       &orderdisp  = "oeeh.orderdisp"
       &prevqtyship= "oeel.prevqtyshp"
       &netamt     = "oeel.netamt"
       &prtpricefl = "oeeh.pickprtfl"
       &bono       = "oeel.bono"
       &orderno    = "oeel.orderno"
       &ordersuf   = "oeel.ordersuf"
       &custno     = "oeeh.custno"
       &corechgty  = "oeel.corechgty"
       &qtybo      = "~{oeepp1bo.las~}" 
       &ordertype  = "o"
       &prefix     = "order"
       &seqno      = "0"
       &stkqtyshp  = "oeel.stkqtyship"
       &nosnlots   = "oeel.nosnlots"
       &comtype    = ""oe""
       &comlineno  = "oeel.lineno"
       &shipto     = "oeeh.shipto"
       &twlstgedit = " and oeeh.stagecd < 3 "} 
 


Inits   Date      Description
-----   --------  ----------------------------------------------------------


*************************************************************************    */
if icsdAutoPck = "Y" and v-loopcnt = 1 and
   x-pckdetailfl and 
   ("{&line}" = "oeel" and s-qtyship > 0 or
     "{&line}" = "oeelk" and {&stkqtyshp} > 0 or 
    ("{&line}" <> "oeel"  and "{&line}" <> "oeelk" 
      and {&stkqtyshp} > 0)) then do:
     
 
  {w-icsw.i {&line}.shipprod
      "if {&altwhse} ne '' then {&altwhse} else {&line}.whse" no-lock "zx-"}
  {w-icsp.i {&line}.shipprod no-lock "zx-"}

  if avail zx-icsp and zx-icsp.statustype = "L" then do:
    assign x-pckdetailfl = x-pckdetailfl. 
  end.
  else do:
  
    create zsdipckdtl.

    if avail zx-icsp then do:
      if avail zx-icsw then do:
        assign
          zsdipckdtl.binloc1       =  if zx-icsw.binloc1 = "new part" then
                                         " "
                                      else
                                        zx-icsw.binloc1 
          zsdipckdtl.binloc2       =  if zx-icsw.binloc2 = "new part" then
                                         " "
                                      else
                                        zx-icsw.binloc2. 
      end.
    end.
      else do:
     /* Non Stock? */
    find first wmsbp where 
               wmsbp.cono = g-cono + 500 and
               wmsbp.whse = {&line}.whse and 
               wmsbp.prod = {&line}.shipprod no-lock no-error.
    if avail wmsbp then do:
      assign zsdipckdtl.binloc1       =  if wmsbp.binloc = "new part" then
                                           " "
                                         else
                                           wmsbp.binloc. 
      find next wmsbp where 
                wmsbp.cono = g-cono + 500 and
                wmsbp.whse = {&line}.whse and 
                wmsbp.prod = {&line}.shipprod no-lock no-error.
      if avail wmsbp then 
        assign zsdipckdtl.binloc2       =  if wmsbp.binloc = "new part" then
                                             " "
                                           else
                                             wmsbp.binloc. 
      end.  
    end.
    for each   wmsbp where 
               wmsbp.cono = g-cono + 500 and
               wmsbp.whse = {&line}.whse and 
               wmsbp.prod = {&line}.shipprod no-lock:
      find first wmsb where 
                 wmsb.cono = wmsbp.cono and
                 wmsb.whse = wmsbp.whse and 
                 wmsb.binloc = wmsbp.binloc no-lock no-error.
      if avail wmsb then do:
        if wmsb.priority = 9 then    
          zsdipckdtl.zzbinfl      = yes.
      end.  
    end.
  
    find first wmsb where 
               wmsb.cono = g-cono + 500 and
               wmsb.whse = {&line}.whse and 
               wmsb.binloc = zsdipckdtl.binloc1 no-lock no-error.
    if avail wmsb then
      assign    zsdipckdtl.locationid     = wmsb.building.
  
  
    assign
    
       zsdipckdtl.cono           = g-cono
       zsdipckdtl.ordertype      = "W"
       zsdipckdtl.orderno        =  {&orderno} 
       zsdipckdtl.ordersuf       =  {&ordersuf} 
       zsdipckdtl.lineno         =  1
       zsdipckdtl.seqno          =  {&seqno}
       zsdipckdtl.whse           =  {&line}.whse
                                      
       zsdipckdtl.pickid         =  b-zsdipckid.pickid 
       zsdipckdtl.pickseq        =  b-zsdipcksq.seqid
                                     
       zsdipckdtl.shipprod       =  {&line}.shipprod   
       zsdipckdtl.stkqtyord      =  {&line}.stkqtyord   
       
       zsdipckdtl.stkqtyship     =    {&stkqtyshp}
       zsdipckdtl.stkqtybo       =  0
       
       zsdipckdtl.stkqtypicked   =  {&stkqtyshp}
       
       zsdipckdtl.realqtypicked  = 0
       /*
       zsdipckdtl.locationid     = ""
       zsdipckdtl.binloc1        = ""
       zsdipckdtl.binloc2        = ""
       zsdipckdtl.zzbinfl        = no
       */
       zsdipckdtl.Pickbin        = ""
       zsdipckdtl.extype         = ""
       zsdipckdtl.exstatustype   = ""
       zsdipckdtl.statustype     = "a"
       zsdipckdtl.completefl     = no
       zsdipckdtl.transproc      = g-ourproc
       zsdipckdtl.recordtype     = "K"
       /* if zsdipckdtl.recordtype = "K" then */
           zsdipckdtl.completefl = true.
       {t-all.i zsdipckdtl}


  end. /* else not labor */    
end.   /* icsdAutoPck = "Y" */ 
