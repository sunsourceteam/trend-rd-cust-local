/***************************************************************************
 PROGRAM NAME: zozr.p
  PROJECT NO.: 
  DESCRIPTION: This program creates a rebate file for Vickers product      
               lines. Build on demand it components will be included. 

  DATE WRITTEN: 08/20/01.
  AUTHOR      : SunSource.  
***************************************************************************/

/* default stream is Air-Draulics */ 

{p-rptbeg.i}
{zsapboydef.i}
def var d_prodcat   as char no-undo.
def var p-prodtype   as char no-undo.


def stream waste.
def stream rebate.
def stream rebate_tmp.
def var fmessage          as char      format "x(80)" no-undo.
define variable vicrec    as character format "x(251)" no-undo.
define variable rfilename as character format "x(24)" no-undo.
define variable ofilename as character format "x(60)" no-undo.
define variable tempfile  as character format "x(60)" no-undo.
define variable co as integer no-undo.
define variable d_prodline like oeel.prodline no-undo.
define new shared var v-tempprt as c format "x(40)" no-undo.
define new shared var s-type    as c format "x(40)" no-undo.
define new shared var aero-type as int  format "9"  no-undo.

def var d_shipprod as c format "x(15)" no-undo.
def var d_shipped as decimal format "99999999" no-undo.
def var d_price as decimal format "9999999.99" no-undo.
def var d_name as c format "x(20)" no-undo.
def var sic as c format "x(4)" no-undo.
def var c_no as c format "x(8)" no-undo.
def var d_line as i format 999 no-undo.
def var d_um as c format "x(4)" no-undo.
def var yyyy as c format "x(4)" no-undo.
def var creditdebit as c format "x(1)" no-undo.
def var bypass_sw as logical no-undo.
def var autotype  as logical init "no" no-undo.
def new shared var pwrrun  as logical init "no"   no-undo.
def var prctype   as char format "x(04)" no-undo.
def var alias-custno as c format "x(30)" no-undo.
def var sun-custno   like oeeh.custno    no-undo.
def new shared var vrebloc  as char format "x(10)" no-undo.
def new shared var progtyp  as int  format "99999" no-undo.
def var icswptyp  as char format "x(04)" no-undo.
def var icsdftyp  as char format "x(03)" no-undo.
def var rebpct    as int  format "9"     no-undo.
def var t-pderamt    as de format ">>>>>>9.9999"       no-undo.
def var t-expreb     as de format ">>>>>>9.9999"       no-undo.
def var t-status     as char  format "x"               no-undo.
def var t-note       as char format "x(30)"            no-undo. 
/* fix006 */
def var x_shipprod  as character format "x(24)" no-undo.
def var x_listprice as decimal format "9999999.99" no-undo.
/* fix006 */
def new shared var p-list    as logical no-undo.
def var company as c format "x(3)" no-undo.
def var ordernum as char format "x(13)" no-undo.
def var x as i no-undo.
def var b_r_date as char no-undo.
def var e_r_date as char no-undo.

def temp-table t-err
    field custno like arsc.custno 
    field whse   like icsd.whse
    index k-err
          custno
          whse.
def temp-table xno_tbl
    field xordno   as char format "x(11)" 
    field xlineno  as int  format "999"
    field xno      as int  format "999" 
    index k-xno1 
          xordno 
          xlineno
          xno     ascending. 
 
define frame f-fname
       skip (2)
       fmessage at 1 
       with width 130 no-labels no-box.

define new shared variable begdt as date no-undo.
define new shared variable enddt as date no-undo.

/* designed to run for previous month if run in current month period */

b_r_date = sapb.rangebeg[1].
v-datein = sapb.rangebeg[1].
 {p-rptdt.i}
begdt = v-dateout.
if string(v-dateout) = v-lowdt then 
  do:
   assign begdt = today.
  end.
if substring(b_r_date,1,2) = "**" or substring(v-datein,1,2) = "" then 
   if month(begdt) = month(today) then  
     do:
      assign begdt = ( if day(today) <= 30 then
                          today - 30
                       else
                          today - 31)
             begdt = date(month(begdt),1,year(begdt)).
     end. 

e_r_date = sapb.rangeend[1].
v-datein = sapb.rangeend[1].
 {p-rptdt.i}
enddt = v-dateout.
if string(v-dateout) = v-highdt then 
   assign enddt = today.
if substring(e_r_date,1,2) = "**" or substring(e_r_date,1,2) = "" then  
   if month(enddt) = month(today) then  
     do:
      assign enddt = (date(month(today),1,year(today)) - 1).
     end. 

if begdt = today and enddt = today then 
  do: 
     message "Date fields were left blank - defaulting to today".
     message "Pls use asterisks for previous month date range".
     pause.
     message "Previous months dates will begin with the first day".
             message " and end with the last day of the previous month".
     pause.
     leave.  
  end.

display "Beginning Date Range " begdt no-label.                     
display "Ending Date Range " enddt no-label. 

if sapb.optvalue[1] = "p" then
   assign pwrrun = yes.
if sapb.optvalue[1] = "e" or sapb.optvalue[1] = "p" then
  do:
  run zozr2.p.
  return.
  end.

if sapb.optvalue[1] = "a" then do:
  run pozr3.p.
  return.
  end.

if sapb.optvalue[1] = "s" then do:
  run pozr13.p.
  return.
  end.

if sapb.optvalue[1] = "h" then do:
  run pozr4.p.
  return.
  end.


if sapb.optvalue[1] = "v" then
 do:
   assign progtyp = 1. 
 end.
else 
if sapb.optvalue[1] = "a" then
   assign progtyp = 4.

output stream waste to "/usr/tmp/T380.txt".  

assign icsdftyp  = sapb.optvalue[5]
       rfilename = sapb.optvalue[2]
       ofilename = "/usr/tmp/" + rfilename + "_" + icsdftyp
       tempfile  = ofilename + "_tmp".

p-list = if sapb.optvalue[3] = "yes" then 
            true 
         else 
            false.

if substring(rfilename,2,8) = "00016700" then 
   c_no = "00016700".
else   
if substring(rfilename,2,8) = "00164900" then 
   c_no = "00164900".
else
if substring(rfilename,2,8) = "00654900" then 
   c_no = "00654900".
else 
if substring(rfilename,2,8) = "00717400" then 
   c_no = "00717400".
else
if substring(rfilename,2,8) = "000eaton" then 
   c_no = "72130503".

assign vrebloc  = sapb.optvalue[4].              

output stream rebate_tmp to value(tempfile).
output stream rebate     to value(ofilename).

if p-list then 
   do:
     {zsapboyload.i}
     if zelection_matrix[1] = 0 and 
        zelection_matrix[2] = 0 and 
        zelection_matrix[3] = 0 and 
        zelection_matrix[4] = 0 and 
        zelection_matrix[5] = 0 and 
        zelection_matrix[6] = 0 and 
        zelection_matrix[7] = 0 then 
        do:
        display
         "**************POZR****************"  at 1 skip 
         "******Error in List Function******"  at 1 skip  
         "****List Criteria Must Be Used****"  at 1 skip
         "**************POZR****************"  at 1.
        return.
        end.
   end.  
else     
    do:
      display
        "**************POZT****************" at 1 skip
        "******Error in List Function******" at 1 skip  
        "****List Criteria Must Be Used****" at 1 skip
        "**************POZT****************" at 1.
      return.
    end.

co = 1.

oeeh-lookup:
for each oeeh where oeeh.cono = co and 
         oeeh.stagecd > 3 and
        ((year(oeeh.invoicedt)  ge year(begdt)    and                         
          month(oeeh.invoicedt) ge month(begdt))  and 
         (year(oeeh.invoicedt)  le year(enddt)    and                        
          month(oeeh.invoicedt) le month(enddt))) and 
         (oeeh.transtype ne "cr" and oeeh.transtype ne "rm")
      no-lock.  

assign company = "".         

    if p-list then 
    do:
       zelection_type  = "c".
       zelection_char4 = " ". 
       zelection_cust  = oeeh.custno. 
       zelection_vend  = 0.
       run zelectioncheck.
       if zelection_good = false then 
          next oeeh-lookup.
    end.            
/*
find first icsd where icsd.cono = g-cono and 
           icsd.whse = oeeh.whse 
           no-lock no-error.
if avail icsd and            
   substring(icsd.branchmgr,1,3) ne icsdftyp then 
   next.   
*/
find first arsc where arsc.cono = 1 and arsc.custno = oeeh.custno
     no-lock no-error.
if avail(arsc) then
    assign sic = string(arsc.siccd[1]).
           substring(vicrec,180,4) = sic.

for each oeel where oeel.cono = co     and 
         oeel.orderno  = oeeh.orderno  and
         oeel.ordersuf = oeeh.ordersuf and 
         oeel.specnstype ne "l"
         no-lock.
         
if oeel.qtyship = 0 then next.

assign d_prodline = oeel.prodline 
       d_line = oeel.lineno
       d_um = oeel.unit
       d_shipprod = oeel.shipprod.

if oeel.specnstype = "n" and
   d_shipprod = "000000" then
       d_shipprod = substring(oeel.proddesc,1,15).
       
if (oeel.arpvendno <> 0 and
   (oeel.arpvendno = 9793750 or
    oeel.arpvendno = 9853210 or
    oeel.arpvendno = 859000))  or  
   (oeel.arpvendno = 0 and
   (oeel.vendno = 9793750 or
    oeel.vendno = 9853210 or
    oeel.vendno = 859000))  then  
      assign d_prodline = "VIC000".
       
if oeel.specnstype ne "n" then do: 
    find first icsw where icsw.cono = co and icsw.whse = oeel.whse and
               icsw.prod = oeel.shipprod no-lock no-error.
    if avail(icsw) then do:
/* fix006 */
       assign x_shipprod = oeel.shipprod
              x_listprice = if avail icsw then 
                               icsw.listprice
                            else
                               0.
/* fix006 */  
       d_prodline = if d_prodline = "vic000" then 
                       d_prodline
                    else
                       icsw.prodline.               
       d_shipprod = if icsw.vendprod ne "" then substring(vendprod,1,15)
                    else substring(oeel.shipprod,1,15).
       end.
    end.

if (d_prodline begins "VIC") and 
    oeel.qtyship > 0 then do:
   find first smsn where smsn.cono = co and smsn.slsrep = oeel.slsrepout
        no-lock no-error.
   if available(smsn) then do:
      assign d_name = substring(smsn.name,1,20).
      end. 
   assign d_price = if oeel.discpct = 0 then oeel.price
                    else ((100 - oeel.discpct) / 100) * oeel.price
          d_shipped = oeel.qtyship
          creditdebit = if oeel.returnfl = yes then "-"
                      else "+".
   run move_rebate_info(input progtyp,
                        input vrebloc).
   end.

if oeel.kitfl = yes then          
 do:
 for each oeelk where oeelk.cono = co     and 
          oeelk.ordertype = "o"           and
          oeelk.orderno   = oeeh.orderno  and 
          oeelk.ordersuf  = oeeh.ordersuf and 
          oeelk.lineno    = oeel.lineno   and 
          oeelk.specnstype ne "l"   
          no-lock.

 if oeelk.qtyship eq 0 then next.
 if oeelk.statustype ne "i" then next.

 assign d_prodline = oeelk.arpprodline      
        d_um = oeelk.unit
        d_shipprod = oeelk.shipprod.


 if (oeelk.arpvendno <> 0 and
    (oeelk.arpvendno = 9793750 or
     oeelk.arpvendno = 9853210 or
     oeelk.arpvendno = 859000)) 
  /* or  
    (oeelk.arpvendno = 0 and
    (oeel.vendno = 9793750 or
     oeel.vendno = 9853210 or
     oeel.vendno = 859000))
  */
    then  
     assign d_prodline = "VIC000".
 
 if oeelk.specnstype = "n" and
    d_shipprod = "000000" then
        d_shipprod = substring(oeelk.proddesc,1,15).
 
 if oeelk.specnstype ne "n" then do:         
     find first icsw where icsw.cono = co and icsw.whse = oeelk.whse and
                icsw.prod = oeelk.shipprod no-lock no-error.
     if avail(icsw) then
      do:
/* fix006 */
        assign x_shipprod = oeelk.shipprod
               x_listprice = if avail icsw then 
                                icsw.listprice
                             else
                                0.
/* fix006 */  
        assign d_prodline = if d_prodline = "vic000" then 
                               d_prodline
                            else
                               icsw.prodline.               
      end.
        find icsp where
             icsp.cono = 1 and 
             icsp.prod = icsw.prod
             no-lock no-error.
             if avail icsp then 
                 assign d_shipprod = (if icsw.vendprod ne "" then 
                                         substring(icsw.vendprod,1,15)
                                      else 
                                         substring(icsp.descrip[1],1,15)). 
     end.                      
 
 if (d_prodline begins "vic") and 
     oeelk.qtyship > 0 then do:
    find first smsn where smsn.cono = co and smsn.slsrep = oeel.slsrepout
         no-lock no-error.
    if available(smsn) then do:
       assign d_name = substring(smsn.name,1,20).
       end. 
 
    assign d_price = if oeel.discpct = 0 then oeelk.price
                     else ((100 - oeel.discpct) / 100) * oeelk.price
           d_shipped = oeelk.qtyship
           creditdebit = "+".
    if oeelk.specnstype ne "N" then
       run PDSC_pricing.   
    run move_rebate_info(input progtyp,
                         input vrebloc).
    end.
  end.  /* loop back to oeelk */  
 end. /*** if kitfl = yes do ***/

end. /* oeel loop end */
end. /*  begin loop */


assign  fmessage =  "*** POZR successfully created file " + ofilename.
display fmessage  
        with frame f-fname.

PROCEDURE PDSC_pricing:

if oeel.kitrollty = "b" or
   oeel.kitrollty = "p" then 
      leave.

find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 1 and pdsc.custno = oeeh.custno and
                pdsc.prod = oeelk.shipprod and oeelk.whse = oeeh.whse and
                pdsc.startdt le oeeh.invoicedt and
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
                
if not avail pdsc then do:
   find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 2 and pdsc.custno = oeeh.custno and
                pdsc.prod = "p-" + icsw.pricetype and
                pdsc.startdt le oeeh.invoicedt and 
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
   if not avail pdsc then do:              
   find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 2 and pdsc.custno = oeeh.custno and
                pdsc.prod = "l-" + string(icsw.arpvendno,"999999999999") +
                             icsw.prodline and
                pdsc.startdt le oeeh.invoicedt and 
                pdsc.whse = oeeh.whse and
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
   if not avail pdsc then do:
        find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 4 and pdsc.custtype = arsc.pricetype and
                pdsc.prod = icsw.pricetype and pdsc.whse = oeeh.whse and
                pdsc.startdt le oeeh.invoicedt and
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
        if not avail pdsc then do:
           find first pdsc where pdsc.cono = 1 and pdsc.statustype = true and
                pdsc.levelcd = 4 and pdsc.custtype = arsc.pricetype and
                pdsc.prod = icsw.pricetype and
                pdsc.startdt le oeeh.invoicedt and
                (pdsc.enddt ge oeeh.invoicedt or
                 pdsc.enddt = ?) no-lock no-error.
     end.    
     end.        
     end.
     end.

     
if not avail pdsc then
   leave.
assign d_price = icsw.listprice.   
repeat x = 1 to 9:
if pdsc.qtybrk[1] = 0  then do:
   if pdsc.prctype = false then 
      d_price = ((100 - pdsc.prcdisc[1]) / 100) * d_price.
   else
      d_price = pdsc.prcmult[1].
   leave.
   end.         
       
if x < 9 then
if oeelk.qtyship lt pdsc.qtybrk[x] then do:
   if pdsc.prctype = false then 
      d_price = ((100 - pdsc.prcdisc[x]) / 100) * d_price.
   else
      d_price = pdsc.prcmult[x].
   leave.         
   end.            
end.
END PROCEDURE.

{vrebate_move1}

PROCEDURE move_common_info:

if d_prodline begins "VIC" then 
  do:
  assign
  substring(vicrec,1,8)    = c_no
  substring(vicrec,9,9)    = substring(string(oeeh.custno,"zzzzzzzzzzz9"),4,9)
  substring(vicrec,18,30)  = arsc.name
  substring(vicrec,48,30)  = oeeh.shiptoaddr[1]
  substring(vicrec,78,30)  = oeeh.shiptoaddr[2]
  substring(vicrec,108,30) = "                              "
  substring(vicrec,138,20) = substring(oeeh.shiptocity,1,20) 
  substring(vicrec,158,3)  = oeeh.shiptost
  substring(vicrec,161,10) = oeeh.shiptozip
  substring(vicrec,171,3)  = "US"
  overlay(vicrec,174,2)    = string(month(oeeh.invoicedt),"99") 
  overlay(vicrec,176,4)    = string(year(oeeh.invoicedt))
  substring(vicrec,180,4)  = sic 
  substring(vicrec,184,15) = d_shipprod
  substring(vicrec,199,8)  = if oeeh.transtype = "RM" or
                               creditdebit = "-" then
                               string(d_shipped * -1, "9999999-")
                            else string(d_shipped, "99999999")
  substring(vicrec,207,20) = d_name
  substring(vicrec,227,10) = string(d_price, "9999999.99-") 
  substring(vicrec,237,15) = string(oeeh.orderno) +
                             "-" + string(oeeh.ordersuf).    
  put stream rebate vicrec at 1 skip.
  export stream rebate_tmp delimiter "," 
         c_no                                               
         substring(string(oeeh.custno,"zzzzzzzzzzz9"),4,9)  
         string(arsc.name,"x(30)")                                          
         string(oeeh.shiptoaddr[1],"x(30)")                                 
         string(oeeh.shiptoaddr[2],"x(30)")                                 
         "                              "                   
         substring(oeeh.shiptocity,1,20)                    
         oeeh.shiptost                                      
         oeeh.shiptozip                                     
         "US"                                               
         string(month(oeeh.invoicedt),"99") +             
         string(year(oeeh.invoicedt))                       
         sic                                                
         string(d_shipprod,"x(15)")                                
         if oeeh.transtype = "RM" or           
            creditdebit = "-" then              
            string(d_shipped * -1, "9999999-")  
         else
            string(d_shipped, "99999999")     
         string(d_name,"x(20)")                                
         string(d_price, "9999999.99-")        
         string(oeeh.orderno,"9999999") + "-" + string(oeeh.ordersuf,"99"). 
  end.

vicrec = "".
END PROCEDURE.

{zsapboycheck.i}



