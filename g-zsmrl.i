/* g-smrl.i 1.11 12/20/96 */
/*si**** Custom ****************************************************************
  CLIENT  : 80281 S.D.I. Operating Partners
  JOB     : sdi083
  AUTHOR  : mat
  DATE    : 09/22/99
  VERSION : 8.0.003
  PURPOSE : SMRL - Add Ranges and New Service Level Calculation
  CHANGES : 
    SI01 09/22/99 mat; Initial Coding
    SI02 02/05/00 mm;  sdi08301; Add option to print order information
******************************************************************************/
/* g-smrl.i 1.2 04/02/98 */
/*h*****************************************************************************
  INCLUDE      : g-smrl.i
  DESCRIPTION  : Variables for smrl.p
  USED ONCE?   : no
  AUTHOR       : jbt
  DATE WRITTEN : 05/01/91
  CHANGES MADE :
    01/23/92 pap; TB# 5272  Add option to use reqshipdt or promisedt to
        determine if late shipment or not (p-datefl) - decrease counts
        from 9- to 7-digits; add variables for lates & incompletes
    10/28/94 djp; TB# 16910 Add region range to report / zzhead added, too
    03/26/98 cm;  TB# 21775 Replace report file with temp-table.
*******************************************************************************/

/** define report input variables  **/
def {1} var b-whse          like poeh.whse                           no-undo.
def {1} var e-whse          like poeh.whse                           no-undo.
def {1} var b-region        like icsd.region                         no-undo.
def {1} var e-region        like icsd.region                         no-undo.
def {1} var b-custno        like oeeh.custno                         no-undo.
def {1} var e-custno        like oeeh.custno                         no-undo.
def {1} var b-shipwhse      like poeh.whse                           no-undo.
def {1} var e-shipwhse      like poeh.whse                           no-undo.
def {1} var b-date          as date                                  no-undo.
def {1} var e-date          as date                                  no-undo.
def {1} var p-prtserv       as dec initial 0                         no-undo.
def {1} var p-prtcust       as c format "x(1)"                       no-undo.
def {1} var p-prtwhse       as c format "x(1)"                       no-undo.
def {1} var p-prtdisp       as c format "x(1)"                       no-undo.
def {1} var p-prtbo         as logical                               no-undo.
def {1} var p-prtnonstk     as logical                               no-undo.
def {1} var p-prtspecial    as logical                               no-undo.
def {1} var p-prtsubs       as logical                               no-undo.
def {1} var p-prtdirect     as logical           initial no          no-undo.
def {1} var p-datefl        as c format "x(1)"                       no-undo.
def {1} var p-usedate       like oeeh.reqshipdt                      no-undo.
def {1} var p-prttype       as c format "x(1)"                       no-undo.
def {1} var s-basefl        as c format "x(1)"                       no-undo.
def {1} var s-basefl2       as c format "x(1)"                       no-undo.
def {1} var s-servmsg       as c format "x(50)"                      no-undo.
def {1} var f-whse          as c format "x(4)"                       no-undo.
def {1} var v-priorwhse     like oeeh.whse                           no-undo.
/*begin si01*/
def {1} var b-custregion    as c format "x(1)"                       no-undo.
def {1} var e-custregion    as c format "x(1)"                       no-undo.
def {1} var b-district      as c format "x(2)"                       no-undo.
def {1} var e-district      as c format "x(2)"                       no-undo.
def {1} var b-group         like smsn.slstype                        no-undo.
def {1} var e-group         like smsn.slstype                        no-undo.
def {1} var b-slsrepin      like oeeh.slsrepin                       no-undo.
def {1} var e-slsrepin      like oeeh.slsrepin                       no-undo.
def {1} var b-slsrepout     like oeeh.slsrepout                      no-undo.
def {1} var e-slsrepout     like oeeh.slsrepout                      no-undo.
def {1} var s-ontimepct     as dec format "zzz9.99"       initial 0 no-undo.
def {1} var s-ontimepct2    as dec format "zzz9.99"       initial 0 no-undo.
def {1} var s-ontimefl      as c format "x(1)"                       no-undo.
def {1} var s-ontimefl2     as c format "x(1)"                       no-undo.
/*end si01*/

/*si02 begin*/
def {1} var p-prtdetail     as logical           initial no          no-undo.
def {1} var z-linerr        as logical           initial no          no-undo.
def {1} var z-orderr        as logical           initial no          no-undo.
def {1} var xinx            as integer           initial 0           no-undo.
def {1} var zx-lit1         as char format "x"   initial " "         no-undo.
def {1} var zx-lit2         as char format "x"   initial " "         no-undo.
/*si02 end*/

/** define variables for report record totals **/
def {1} var r-linecnt       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var r-complete      as int format "zzzzzz9"        initial 0 no-undo.
def {1} var r-incomp        as int format "zzzzzz9"        initial 0 no-undo.
def {1} var r-subs          as int format "zzzzzz9"        initial 0 no-undo.
def {1} var r-requestdt     as date                                  no-undo.
def {1} var r-promisedt     as date                                  no-undo.
def {1} var r-loford        as integer                     initial 0 no-undo.

/* SDI stuff */

def {1} var r2-totamount     like oeeh.totinvamt            initial 0 no-undo.
def {1} var r2-linecnt       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var r2-complete      as int format "zzzzzz9"        initial 0 no-undo.
def {1} var r2-incomp        as int format "zzzzzz9"        initial 0 no-undo.
def {1} var r2-complete2     as int format "zzzzzz9"        initial 0 no-undo.
def {1} var r2-incomp2       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var r2-subs          as int format "zzzzzz9"        initial 0 no-undo.
/* SDI stuff */

/** define customer accumulator variables **/
def {1} var s-custno        as c format "x(13)" initial ""           no-undo.
def {1} var s-lookupnm      as c format "x(15)" initial ""           no-undo.
def {1} var s-whse          like icsw.whse                           no-undo.
def {1} var s-cnt           as int format ">>>>>>9"        initial 0 no-undo.
def {1} var s-ordcnt        as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s-totvalue      as dec format "zzzzzzzzz9.99-" initial 0 no-undo.
def {1} var s-avgvalue      as dec format "zzzzzzzzz9.99-" initial 0 no-undo.
def {1} var s-linecnt       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s-complete      as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s-complete2     as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s-incomp        as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s-incomp2       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s-late          as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s-late2         as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s-lateinc       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s-lateinc2      as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s-subs          as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s-servlevel     as dec format "zzz9.99"       initial 0 no-undo.
def {1} var s-servlevel2    as dec format "zzz9.99"       initial 0 no-undo.

/* SDI stuff  */
def {1} var s2-cnt           as int format ">>>>>>9"        initial 0 no-undo.
def {1} var s2-ordcnt        as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s2-totvalue      as dec format "zzzzzzzzz9.99-" initial 0 no-undo.
def {1} var s2-avgvalue      as dec format "zzzzzzzzz9.99-" initial 0 no-undo.
def {1} var s2-linecnt       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s2-complete      as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s2-complete2     as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s2-incomp        as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s2-incomp2       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s2-late          as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s2-late2         as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s2-lateinc       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s2-lateinc2      as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s2-subs          as int format "zzzzzz9"        initial 0 no-undo.
def {1} var s2-servlevel     as dec format "zzz9.99"       initial 0 no-undo.
def {1} var s2-servlevel2    as dec format "zzz9.99"       initial 0 no-undo.




/** define variables for total lines **/
def {1} var t-text          as c   format "x(17)"          initial 0 no-undo.
def {1} var t-cnt           as int format ">>>>>>9"        initial 0 no-undo.
def {1} var t-ordcnt        as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-totvalue      as dec format "zzzzzzzzz9.99-" initial 0 no-undo.
def {1} var t-linecnt       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-complete      as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-complete2     as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-incomp        as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-incomp2       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-late          as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-late2         as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-lateinc       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-lateinc2      as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-subs          as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-transcnt2     like t-transcnt                initial 0 no-undo.

/* SDI stuff */
def {1} var t2-cnt           as int format ">>>>>>9"        initial 0 no-undo.
def {1} var t2-ordcnt        as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t2-totvalue      as dec format "zzzzzzzzz9.99-" initial 0 no-undo.
def {1} var t2-linecnt       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t2-complete      as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t2-complete2     as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t2-incomp        as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t2-incomp2       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t2-late          as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t2-late2         as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t2-lateinc       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t2-lateinc2      as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t2-subs          as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t2-transcnt2     like t-transcnt                initial 0 no-undo.


/* SDI stuff */




/** define variables for grand totals **/
def {1} var g-cnt           as int format ">>>>>>9"        initial 0 no-undo.
def {1} var g-ordcnt        as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g-totvalue      as dec format "zzzzzzzzz9.99-" initial 0 no-undo.
def {1} var g-linecnt       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g-complete      as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g-complete2     as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g-incomp        as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g-incomp2       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g-late          as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g-late2         as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g-lateinc       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g-lateinc2      as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g-subs          as int format "zzzzzz9"        initial 0 no-undo.

/* SDI stuff */
def {1} var g2-cnt           as int format ">>>>>>9"        initial 0 no-undo.
def {1} var g2-ordcnt        as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g2-totvalue      as dec format "zzzzzzzzz9.99-" initial 0 no-undo.
def {1} var g2-linecnt       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g2-complete      as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g2-complete2     as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g2-incomp        as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g2-incomp2       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g2-late          as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g2-late2         as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g2-lateinc       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g2-lateinc2      as int format "zzzzzz9"        initial 0 no-undo.
def {1} var g2-subs          as int format "zzzzzz9"        initial 0 no-undo.


/* SDI stuff */

/*tb 21775 03/26/98 cm; Add temp-table definition */
def temp-table t-smrl no-undo
    field custno    like oeeh.custno
    field whse      like oeeh.whse
    field orderno   like oeeh.orderno
    field ordersuf  like oeeh.ordersuf
    field totinvamt like oeeh.totinvamt
    field shipdt    like oeeh.shipdt
    field reqshipdt like oeeh.reqshipdt
    field promisedt like oeeh.promisedt
    field linecnt   as int format "zzzzzz9"
    field complete  as int format "zzzzzz9"
    field incomp    as int format "zzzzzz9"
    field subs      as int format "zzzzzz9"
/* SDI stuff */
    field loford      as int
    field zpromord    as int format "zzzzzz9"
    field zreqord     as int format "zzzzzz9"
    field z2totinvamt like oeeh.totinvamt
    field z2subs      as int format "zzzzzz9"
    field z2linecnt   as int format "zzzzzz9"
    field z2complete  as int format "zzzzzz9"
    field z2incomp    as int format "zzzzzz9"
    field z2complete2 as int format "zzzzzz9"
    field z2incomp2   as int format "zzzzzz9"

/* SDI stuff */

    index k-t-smrl is primary unique
        whse        ascending
        custno      ascending
        orderno     ascending
        ordersuf    ascending
    index k-custno is unique
        custno      ascending
        orderno     ascending
        ordersuf    ascending.
    
/*si02 begin - temp table for order detail */

def temp-table t-zzdetail no-undo
    field custno    like oeel.custno
    field orderno   like oeel.orderno
    field ordersuf  like oeel.ordersuf
    field lineno    like oeel.lineno
    field transtype like oeel.transtype
    field reqshipdt like oeel.reqshipdt
    field promisedt like oeel.promisedt
    field shipdt    like oeeh.shipdt
    field shipprod  like oeel.shipprod
    field qtyord    like oeel.qtyord
    field qtyship   like oeel.qtyship
    field whse      like icsw.whse
    field loford    as  integer
    field xlate1    as  integer
    field xlate2    as  integer
    field ylate1    as  integer
    field ylate2    as  integer
    field prttype   as  character format "x"
    field errtype   as  character format "x"
    field netamt    like oeel.netamt
    field marginamt as decimal format "zzzzzz9.99-"
    field marginpct as decimal format "zzz9.9" 
/* SDI stuff */

/* SDI stuff */

    index k-t-zzdetail is primary unique
        custno      ascending
        orderno     ascending
        ordersuf    ascending
        lineno      ascending.

/*si02 end*/        
 
