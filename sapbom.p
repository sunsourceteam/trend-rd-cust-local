
/* sapbo.p 1.2 05/26/98 */
/*h*****************************************************************************
  PROCEDURE     : sapbo
  DESCRIPTION   : Order List Option for SAPB
  AUTHOR        : enp
  DATE WRITTEN  : 07/18/89
  CHANGES MADE  :
    mms 11/18/91 JIT tb 4923
    mms 12/04/91 tb 5164 JIT - changed reqship to promise
    mms 12/31/91 tb 5385 JIT - don't invoice stage 1 JIT
    mms 03/10/92 tb 5976 JIT - changed addon check
    mms 03/27/92 tb 4901 - check addonnet not addonamt
    dkk 03/30/92 tb 6158 - redisplay list correctly
    dea 06/01/92 tb 6832 - Line DO; error changed
    06/18/92 mwb; tb 7040; changed oeepi edit for #5921 to allow for cs refunds
        to process.
    06/18/92 mwb; tb 6889; replaced payamt with tendamt.
    06/18/92 mkb; tb 2821; allow the print of canceled inv
    06/20/92 mwb; tb 5413; check the write-off option in oeao before using
        error 5921.
    06/29/92 mwb; tb 5413; added totqtyshp, totqtyord, totqtyret, boexistsfl
        to 5921.
    07/21/92 jlc; alpha customer number
    11/12/92 mms; TB# 8669 Rush - Sort rush orders to top
    03/03/93 sdc; tb 10245 - DNet Control Path
    08/02/93 rs; TB# 11916 PORII- lists not deleting totally
    06/06/94 dww; TB# 15382 Process OE w/bad terms creates no ARET
    06/07/94 dww; TB# 15845 If any ? found on order don't invoice
    10/02/95 mcd; TB# 19404 Add User Include to sapbo.p(&before_create)
    11/07/95 bj;  TB# 19669 Unable to enter invoice for taxes only. Use
        include p-oeesx.i instead of identical inline code.
    01/23/96 gp;  TB# 3713  Order entry suspend of an SO/DO (T4)
    06/27/96 bmf; TB# 21390 Add user include to sapbo.p (&user_declare_vars)
    09/13/96 jl;  TB# 21702 Trend 8.0 oper2 enhancement - removed
        references to oper2 in the code
    05/18/98 lmk; TB# 18049 Clear-list Remove-from-List frames to standard
    12/12/02 dls; TB# e13523 Additonal Addons in OE.
    04/21/03 lcb; TB# t14244 8Digit Ord No Project
*******************************************************************************/
{g-all.i}
def input  param    v-runtype   as i                no-undo.
/* 1=Delete, 2=Delete and Run, 3=Run Only */
def input  param    v-reportnm  like sapbo.reportnm no-undo.
def input  param    v-cono      like sapbo.cono     no-undo.
def var             v-seqno     as i                no-undo.
def var             s-stagetext as c                no-undo.
def var             v-zero      like sapbo.ordersuf no-undo initial [0].
def var             v-stagetext as c extent 10      no-undo initial
["Entered","Ordered","Picked","Shipped","Invoiced","Paid","","","","Canceled"].
/*tb 5413 06/20/92 mwb */
def var v-writeoffamt    like oeeh.writeoffamt no-undo.

/*tb 15845 06/07/94 dww; If any ? found on order don't invoice */
def            var v-descrip     like oeinvp.descrip         no-undo.
def            var v-datclabel   like sasc.icdatclabel       no-undo.
def            var v-oewriteamt  like sasc.oewriteamt        no-undo.
def            var v-addonfl     as   log                    no-undo.
def            var v-inboundfrt  like addon.addonnet         no-undo.
def            var v-outboundfrt like addon.addonnet        no-undo.
def     buffer b2-sapbo for sapbo.
def     buffer b2-oeeh  for oeeh.
def  var v-vcustno like oeeh.custno no-undo.

/*u User Hook */
{sapbo.z99 &user_declare_vars = "*"}

/*tb 18049 05/18/98 lmk; Add forms f-x f-x2 in new sapb.gfo include */
{sapb.gfo}
{addon-tt.i}

form
    sapbo.orderno   at 1    label "Order #"
    "-"             at 9
    sapbo.ordersuf  at 10    no-label
    oeeh.promisedt  at 13   label "Promised" /* mms */
    s-stagetext     at 23   label "Stage"
        oeeh.transtype  at 33   label "Type"
    oeeh.custno     at 39   label "Customer    "
    arsc.notesfl    at 51   no-label
    arsc.lookupnm   at 53   no-label
with frame f-sapbo width 80 row 6 down title "Orders to Process" overlay.

/*tb 21702 09/13/96 jl; Removed oper2 from search criteria */

if v-runtype le 2 then do for sapbo:
    for each sapbo use-index k-sapbo where
        sapbo.cono      = v-cono        and
        sapbo.reportnm  = v-reportnm
    exclusive-lock transaction:
        delete sapbo.
    end.
end.
if v-runtype = 1 then return.

/*tb 15845 06/07/94 dww; If any ? found on order don't invoice */
do for sasc:
    {w-sasc.i no-lock}
    assign
        v-datclabel  = sasc.icdatclabel
        v-oewriteamt = sasc.oewriteamt.
end.

do for sapbo i = 1 to 11:
    /*tb 21702 09/13/96 jl; Removed oper2 from search criteria */
    if i = 1 then
        find last sapbo use-index k-sapbo where
            sapbo.cono     = v-cono     and
            sapbo.reportnm = v-reportnm
        no-lock no-error.

    /*tb 21702 09/13/96 jl; Removed oper2 from search criteria */
    else
        find prev sapbo use-index k-sapbo where
            sapbo.cono     = v-cono     and
            sapbo.reportnm = v-reportnm
        no-lock no-error.
       
    if avail sapbo then do:
        down 1 with frame f-sapbo.
        display sapbo.orderno sapbo.ordersuf with frame f-sapbo.
        {w-oeeh.i sapbo.orderno sapbo.ordersuf no-lock}

        
        if avail oeeh then do:
            assign s-stagetext = v-stagetext[oeeh.stagecd + 1].

            display
                oeeh.promisedt
                s-stagetext
                oeeh.custno
                oeeh.transtype
            with frame f-sapbo.

            {w-arsc.i oeeh.custno no-lock}
            if avail arsc then
                display
                    arsc.notesfl
                    arsc.lookupnm
                with frame f-sapbo.
            else
                display
                    ""        @ arsc.notesfl
                    v-invalid @ arsc.lookupnm
                with frame f-sapbo.

        end. /* if avail oeeh */
        else
            display
                ""              @ arsc.notesfl
                "Invalid Order" @ arsc.lookupnm
            with frame f-sapbo.

        if i = 1 then v-seqno = sapbo.seqno.

    end. /* if avail sapbo */
    else leave.
end.
if i ne 1 then up i - 1 with frame f-sapbo.

main:
do for sapbo while true transaction on endkey undo main, leave main:
    
    /*tb 11916 08/02/93 rs; PORII- lists not deleting totally */
    {p-sapb.i "sapbo.orderno" "sapbo.ordersuf"}
    {w-oeeh.i "input sapbo.orderno" "input sapbo.ordersuf" no-lock}
    
    
    if avail oeeh then do: /* mms */
        assign s-stagetext = v-stagetext[oeeh.stagecd + 1].

        display
            oeeh.promisedt
            s-stagetext
            oeeh.custno
            oeeh.transtype
        with frame f-sapbo.

        {w-arsc.i oeeh.custno no-lock}
        if avail arsc then
            display
                arsc.notesfl
                arsc.lookupnm
            with frame f-sapbo.
        else
            display
                ""        @ arsc.notesfl
                v-invalid @ arsc.lookupnm
            with frame f-sapbo.

        g-errfl = false.

        /*tb 15845 06/07/94 dww; If any ? found on order don't invoice. Added
            SASC scoping. */
        do for sasc:
            /*tb 10245 03/03/93 sdc*/
            if can-do("oeepi,oeepb,oeepp,oeezi",g-ourproc) then
                {e-dnctrl.i &file = "oeeh"
                            &next = "i = i"}
        end.
         /* get addons */
       {oe-t-addon.i &ordertype   = ""oe""
                 &orderno     = oeeh.orderno
                 &ordersuf    = oeeh.ordersuf
                 &lock        = "no-lock"}
        v-addonfl = false.
        for each t-addon:
            if v-addonfl = false then
                v-addonfl = if (t-addon.addontype  =  no
                            or t-addon.addonnet    = 0) then false
                                                        else true.
            if t-addon.seqno = 1 then
                v-inboundfrt = t-addon.addonnet.
            if t-addon.seqno = 2 then
                v-outboundfrt = t-addon.addonnet.
        end.
        
        
        /**************** Invoicing Checks *************************/
        /* mms 3/27/92 tb 4901 changed addonamt to net on error 5951/5852 */
        /*tb#19669 bj; Use include p-oeesx.i instead of inline code */
        /*tb 3713 01/23/96 gp;  Modify edit for suspended order */


        if g-ourproc = "oeezi" then do:
              
           if oeeh.transtype = "CR" then do:

              for each b2-sapbo where b2-sapbo.cono = g-cono and
                                      b2-sapbo.reportnm = v-reportnm 
                                      BREAK BY oeeh.custno:
                         /*
                         message b2-sapbo.orderno b2-sapbo.ordersuf
                                 "oeeh" oeeh.orderno. pause.
                         */
               find first b2-oeeh where b2-oeeh.cono = g-cono and
                                        b2-oeeh.orderno = b2-sapbo.orderno and
                                        b2-oeeh.ordersuf = b2-sapbo.ordersuf 
                                        no-lock no-error.
                         /*
                          message "here" b2-sapbo.orderno. pause. 
                          message "b2-oeeh" b2-oeeh.custno 
                                  "oeeh" oeeh.custno. pause.
                         */
                        if b2-oeeh.custno = oeeh.custno and 
                                            can-do("cr", oeeh.transtype) then 
                               run err.p(9961).   /* created error message */
               end.
            end.
         end.

        if g-ourproc = "oeezi" then do:
            
            if oeeh.stagecd > 3 then run err.p(5815).
            else if oeeh.orderdisp = "j" and oeeh.stagecd < 2
                then run err.p(6123).

            else if oeeh.stagecd = 2 then run err.p(5815).
            else if oeeh.stagecd = 1 then run err.p(5815).
            else if oeeh.linefl
                then run err.p(5845).
            else if oeeh.approvty ne "y" then run err.p(5846).
            else if can-do("ra,st,fo,qu,bl",oeeh.transtype)
                then run err.p(5849).
            else if oeeh.transtype = "br" and oeeh.stagecd = 0
                then run err.p(5854).
            else if oeeh.transtype = "DO" and oeeh.stagecd ne 3
                then run err.p(5850).
            else if oeeh.inbndfrtfl and
                (v-inboundfrt + oeeh.totdatccost) = 0
                then run err.p(5851).
            else if oeeh.outbndfrtfl and v-outboundfrt = 0
                and oeeh.transtype ne "cs"
                then run err.p(5852).
            /*tb 6832 06/01/92 dea; Compare to totqtyord using qty ship
              independent of DO lines
              NOTE: canceled change 06/22/92 - mjq
              No DO lines will be allowed on S or T orders */
            else if can-do("s,t",oeeh.orderdisp) and
                oeeh.totqtyshp <> oeeh.totqtyord then run err.p(5933).
            else if {v-aret.i oeeh.custno oeeh.orderno oeeh.ordersuf "/*"}
                then run err.p(5934).
            else if {p-oeesx.i}
                then run err.p(5920).
            else if oeeh.updtype = "s"
                then run err.p(5950).
            else if oeeh.openinit ne "" then do:
                run err.p(5608).
                message color normal oeeh.openinit.
            end.
            /*tb 7191 07-21-92 jlc; alpha customer number */
            else if (oeeh.fpcustno <> {c-empty.i}) and not can-find(arsc where
                 arsc.cono = oeeh.cono and arsc.custno = oeeh.fpcustno) then do:
                run err.p(4325).
                message color normal oeeh.fpcustno.
            end.
            /*tb 15382 06/06/94 dww; Process OE w/bad terms creates no ARET */
            else if not can-find(sasta where
                                 sasta.cono     = g-cono        and
                                 sasta.codeiden = "t"           and
                                 sasta.codeval  = oeeh.termstype)
            then do:
                run err.p(4031).
                message color normal "Invalid Terms:" oeeh.termstype.
            end.

            /*tb 15845 06/07/94 dww; If any ? found on order don't invoice.
                First, identify if ANY total field contains a question mark.
                    This is done by adding all the total fields together; if any
                    one of the fields is "?", then the total will be "?".
                Next, identify the first field that contains a "?".
                Use the &sapbo parameter to display an interactive error. */
            else {oeepix.lpr &sasc  = "v-"
                             &sapbo = "*"}

            else if oeeh.orderdisp <> "j" then do:
                /* mms 1/19/92 don't check these on a JIT order */
                /*tb 5413 06/20/92 mwb */
                {p-oeepit.i &oeeh = "v-"
                            &sasc = "v-"}
                if oeeh.nosnlots ne 0 and oeeh.transtype ne "cr"
                    then run err.p(5847).
                else if oeeh.nocatwght ne 0 and oeeh.transtype ne "cr"
                    then run err.p(5848).
                else if oeeh.tendamt + v-writeoffamt > oeeh.totinvamt and
                    oeeh.tendamt > 0 and oeeh.totqtyshp = oeeh.totqtyord and
                    oeeh.totqtyret = 0 and oeeh.boexistsfl = no
                    then run err.p(5921).
            end.

        end.

        /*tb 15845 06/07/94 dww; If any ? found on order don't invoice.
            Removed read of SASC. */
        /* Pick Ticket Checks */
        else if g-ourproc = "oeepp" then do:
            if oeeh.stagecd > 3 or (oeeh.transtype = "br" and
               oeeh.stagecd = 0) then run err.p(5815).
            else if can-do("do,cr,st,fo,qu,bl",oeeh.transtype)
                then run err.p(3642).
            else if oeeh.approvty ne "y" then run err.p(5846).
            else if oeeh.openinit ne "" then do:
                run err.p(5608).
                message color normal oeeh.openinit.
            end.
        end.

        /* Demand Invoices */
        else if g-ourproc = "oerd" then do:
            if oeeh.openinit ne "" then do:
                run err.p(5608).
                message color normal oeeh.openinit.
            end.
        end.

        /* ??? */
        else if g-ourproc = "oeepb" then do:
            if oeeh.ordersuf = 0 then run err.p(5817).
            else if oeeh.stagecd > 1 then run err.p(5818).
            else if oeeh.openinit ne "" then do:
                run err.p(5608).
                message color normal oeeh.openinit.
            end.
        end.

        /* Bill of Lading */
        else if g-ourproc = "oeepl" then do:
            if oeeh.stagecd < 3 or oeeh.stagecd > 5 then
                run err.p(5969).
            else if not can-do("so,br,cs",oeeh.transtype) then
                run err.p(5970).
            else if oeeh.openinit ne "" then do:
                run err.p(5608).
                message color normal oeeh.openinit.
            end.
        end.

        /*tb 19404 10/02/95 mcd; Add User Include sapbo.p */
        {sapbo.z99 &before_create = "*"}

        if g-errfl then do:
            pause.
            next main.
        end.

        /*tb 21702 09/13/96 jl; Removed assign of sapbo.oper2*/
        create sapbo.
        assign
            sapbo.cono      = v-cono
            sapbo.reportnm  = v-reportnm
            v-seqno         = v-seqno + 1
            sapbo.seqno     = v-seqno
            /*tb 8669 11/12/92 mms; The blank forces rush orders to the top */
            sapbo.outputty  = if g-ourproc = "oeepp" then
                                  if oeeh.norushln = 0 then "o"

                                  else ""
                              else sapbo.outputty
            sapbo.orderno
            sapbo.ordersuf.
        {t-all.i sapbo}
        scroll down with frame f-sapbo.
    end.
    else do:
        run err.p(4605).
        next main.
    end.
end.

/*tb 21702 09/13/96 jl; Removed oper2 from search criteria */


if ({k-cancel.i} or {k-jump.i}) and v-reportnm = "" then
for each sapbo use-index k-sapbo where
    sapbo.cono      = v-cono        and
    sapbo.reportnm  = v-reportnm
exclusive-lock transaction:
    delete sapbo.
end.

hide frame f-sapbo no-pause.