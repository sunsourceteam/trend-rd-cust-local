/* p-oerr3.i 1.1 01/03/98 */
/* p-oerr3.i 1.10 09/08/95 */
/*h*****************************************************************************
  INCLUDE      : p-oerr3.i
  DESCRIPTION  : Calculates the Order Total per Line
  USED ONCE?   : no (oere.p, oered.p, and oerrl.p)
  AUTHOR       : mwb
  DATE WRITTEN : 09/12/90
  CHANGES MADE :
    10/25/91 mwb;           Need wo and spec in discamt.
    10/25/91 mwb; TB# 3305  Total order value off line totals.
    10/30/91 mwb; TB# 4882  Totals for lost business, bo field.
    01/15/92 pap; TB# 5225  JIT  added jit flag for display (salesrep & prod).
    08/18/92 mwb; TB# 7319  Added unitconv to replace stkqtyord / qtyord.
    12/03/92 mwb; TB# 4535  Added &sasc to a-oeeltm.i for nest errors.
        Added v-rebatey for "r" (rebates).
    07/13/93 mwb; TB# 12169 display oeel.proddesc if loaded.
    10/29/93 kmw; TB# 11758 Set unit conv to 10 dec
    05/24/94 dww; TB# 15089 Total Order Value & Net Amount same
    04/10/95 kjb; TB# 16383 Display the totalling information based on the lines
    09/08/95 dww; TB# 17521 Performance in OERE.  Changed "sasc" references
        to be passed parameters
    10/11/95 mcd; TB# 18812-E5a 7.0 Rebates Enhancement (E5a) -
        Display figures in consideration of rebates on the Exception Report
    12/11/95 mcd; TB# 19953 **Invalid** display for Non-stock
    02/18/97 jl;  TB# 7241 (4.1) Special Price Costing. Removed s-speccostty
        and s-csunperstk, replaced with speccost.gas
    03/06/97 frb; TB# 7241  Special price costing - replace all occurances
             of "&icsp" with "&icss" 
*******************************************************************************/

{w-icsp.i oeel.shipprod no-lock}

/*tb 7241  (4.1) 02/19/97 jl;  Special Price Costing. Added icss.gfi and     
    speccost.gas */

/*d Get special price/costing information and assign variables */
{icss.gfi
    &prod        = oeel.shipprod
    &icspecrecno = oeel.icspecrecno
    &lock        = "no"}
 

/*tb 12169 07/13/93 mwb; removed 'n' edit added proddesc ne "" edit */
/*tb 11758 10/28/93 kmw; Set unit conv to 10 dec */
/*tb 19953 12/11/95 mcd; **Invalid** display for Non-stock */

assign
    {speccost.gas  &com = "/*"}
    v-prod       = if avail icsp then (oeel.shipprod + icsp.notesfl)
                   else oeel.shipprod

    s-descrip    = if can-do("n,l",oeel.specnstype) then oeel.proddesc
                   else if avail icsp then icsp.descrip[1]
                   else v-invalid 
    
    s-jitflag    = if oeeh.orderdisp = "j" then "jit"
                   else ""
    
    s-stagecd    = v-stage[oeeh.stagecd + 1]
    
    s-disccd     = if oeel.disccd ne 0 then "L"
                   else ""
 
    v-coretype   = if oeel.restockamt ne 0      then "Restock:"
                   else if oeel.corechgty = "r" then "Core Ret:"
                   else if oeel.corechgty = "y" then "Core Chg:"
                   else ""
    
    v-corecharge = if oeel.restockamt ne 0 then
                       if oeel.restockfl = yes then
                           string(oeel.restockamt,">>>>>9.99")
                       else
                            string((round(oeel.netamt *
                                (oeel.restockamt / 100),2)),">>>>>9.99")
                   else if oeel.corechgty = "n" then ""
                   else string(oeel.corecharge,">>>>>9.99")
    
    s-backorder  = if oeel.bono ne 0 then yes
                   else no
    
    v-marcost    = 0
    v-mardiscoth = oeel.discamtoth + oeel.wodiscamt
    v-mardisc    = 0
    v-marnet     = 0
    v-marprice   = 0
    v-marqty     = 0
    s-marginamt  = 0
    s-marginpct  = 0
    v-conv       = if oeel.qtyord ne 0 then
                       {unitconv.gas &decconv = oeel.unitconv}
                   else 1.

/*tb 18812 10/06/95 mcd; Rebate Enhancement 7.0 (E5-a) Added &oerebty */

{a-oeetlm.i &oeel    = "oeel."
            &oeeh    = "oeeh."
            &oerebty = v-oerebty
            &com     = "/*"
            &icss    = "v-"
            &sasc    = "/*"}.

/*tb 4535 12/09/92 mwb; added v-rebatety (displays next to margin) */
/*tb 17521 09/08/95 dww; Performance in OERE. */
assign
    s-marginpct = if s-marginpct < 9999.99- then 9999.99-
                  else if s-marginpct > 9999.99 then 9999.99
                  else s-marginpct
    s-cost      = if v-oecostsale = yes then v-marnet
                  else v-marcost * v-marqty
    s-cost      = if oeel.returnfl = yes then s-cost * -1
                  else s-cost
    s-discamt   = oeel.discamt
    v-rebatety  = if ({&sasc}smcustrebfl = yes and v-custrebamt ne 0) or
                     ({&sasc}smvendrebfl = yes and v-vendrebamt ne 0)
                      then "r"
                  else ""
    v-qtyshipfl = if {p-oeordl.i "oeel." "oeeh."} then no
                  else yes
    i           = 0
    i           = lookup(oeel.transtype,"so,do,cs,rm,cr,bl,br,fo,qu,st").

{&oere}

if oeel.specnstype = "l" then
    assign
        t-lostcnt = t-lostcnt + 1
        t-lostnet = t-lostnet +
                       ((oeel.netord -
                         oeel.wodiscamt - oeel.discamtoth) *
                         (if oeel.returnfl = yes then -1 else 1))
        t-lostord = t-lostord +
                        if v-qtyshipfl = yes then
                         ((((oeel.qtyship * v-marprice)  -
                            (oeel.qtyship * v-mardisc )) - v-mardiscoth)
                           * if oeel.returnfl = yes then -1 else 1)
                        else
                         ((((oeel.qtyord * v-marprice)  -
                            (oeel.qtyord * v-mardisc )) - v-mardiscoth)
                           * if oeel.returnfl = yes then -1 else 1).

else if i < 11 and i ne 0 then

    /*tb 15089 05/24/94 dww; Total Order Value & Net amount same.  Use the
        parameter "&oerr" to accumulate the Total Order Value based on
        Quantity Ordered, not Quantity Shipped. */
    /*tb 16383 04/10/95 kjb; Display the totalling information based on the
        lines.  Calculate the sales, gross and cost of goods from the line
        by including oerr3.lpr (passed in in parameter &oerrl_assign).  It
        will only be included when called from oerrl.p and not from oered.p
        and oerel.p. */
    assign
        t-totord[i]  = t-totord[i] +
                       if v-qtyshipfl = yes and "{&oerr}" = "" then
                            ((((oeel.qtyship * v-marprice)  -
                               (oeel.qtyship * v-mardisc )) - v-mardiscoth)
                             * if oeel.returnfl = yes then -1 else 1)
                       else ((((oeel.qtyord * v-marprice)  -
                               (oeel.qtyord * v-mardisc )) - v-mardiscoth)
                             * if oeel.returnfl = yes then -1 else 1)
        {{&oerrl_assign}}.

/{&oere}* */

