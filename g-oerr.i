/*g-oerr.i 05/20/99 */
/*si**** Custom ****************************************************************
  CLIENT  : 80281 Sunsource
  JOB     : sdi071
  AUTHOR  : dki
  DATE    : 05/20/99
  VERSION : 8.0.003
   ANCHOR : g-oerr.i
  PURPOSE : variables for oerr.p
  CHANGES :
    SI01 05/20/99 dki; sdi071; add sales mgr and salesrep group ranges.
******************************************************************************/
/* g-oerr.i 1.14 08/25/97 */
/*h*****************************************************************************
  INCLUDE      : g-oerr.i
  DESCRIPTION  : Variable definitions for oerr.p
  USED ONCE?   : no
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    06/27/91 mwb; TB# 3670  Added return flag to qty's
    10/30/91 mwb; TB# 4882  Totals for lost business, bo on lines
    01/07/92 pap; TB# 5225  JIT modified margin variable format
    01/15/92 pap; TB# 5225  JIT added jit flag variable
    12/09/92 mwb; TB# 4535  added v-rebatety, v-smvendrebfl, v-smcustrebfl for
        new rebates calc includeed in margin calculation
    03/19/93 kmw; TB# 10080 Margin pricing - change format of s-marginamt
    07/23/93 rs;  TB# 10656 Option to display Tag & Hold orders
    10/28/93 kmw; TB# 11758 Improve Unit Conv Precision
    10/12/94 djp; TB# 16800 Add region to report
    04/10/95 kjb; TB# 16383 Display the totalling information based on the lines
        Added lt-sales, lt-gross, lt-cstgds and lt-count.
    09/07/95 dao; TB# 13489 Added Division # range variable definitions
    10/16/95 mcd; TB# 18812-E7a 7.0 Rebates Enhancement (E7a) -
        Display figures in consideration of rebates on the Order Register
        Added v-oerebty, v-totrebamt ,v-smcustrebfl, v-smvendrebfl
    02/21/97 jl;  TB# 7241 (4.1) Special Price Costing. Removed s-speccostty 
        and s-csunperstk. Added speccost.gva
    08/25/97 kjb; TB# 22385 OERR totals do not include rebates - modified the
        format of t-lostcnt from "<<<<<<9" to "<<<<<9", added v-rebatefl,
        t-torebatefl, t-tocustreb, t-tovendreb, t-rebfl, t-custreb, t-vendreb
        and lt-rebfl
    10/23/00 - ceg - added takenby stuff    
*******************************************************************************/
{g-margin.i}

{g-conv.i}
{speccost.gva}

def var s-lookupnm      as c format "x(15)"                 initial ""  no-undo.
def var s-fplookup      as c format "x(15)"                 initial ""  no-undo.
def var s-disccd        as c format "x"                                 no-undo.
def var s-jitflag       as c format "x(3)"                  initial ""  no-undo.
def var s-margin        as decimal format "zzzzzzz9.99-"    initial 0   no-undo.
def var s-marginamt     as dec format "zzzzz9.99999-"                   no-undo.
def var s-margpct       as decimal format "zzz9.99-"        initial 0   no-undo.
def var s-marginpct     as decimal format "zzz9.99-"        initial 0   no-undo.
def var s-netamt        like oeel.netamt                                no-undo.
def var s-cost          like oeeh.totlineamt                            no-undo.
def var s-discamt       like oeel.discamt                               no-undo.
def var s-dolinefl      as c format "x"                                 no-undo.
def var v-totlineamt    like oeeh.totlineamt                initial 0   no-undo.
def var v-totcost       like oeeh.totcost                   initial 0   no-undo.
def var v-coretype      as c format "x(10)"                 initial ""  no-undo.
def var v-corecharge    as c format "x(9)"                  initial ""  no-undo.
def var v-prod          as c format "x(25)"                 initial ""  no-undo.
def var v-cust          as c format "x(13)"                 initial ""  no-undo.
def var v-fpcustno      as c format "x(13)"                 initial ""  no-undo.
def var s-descrip       as c format "x(24)"                 initial ""  no-undo.
def var s-netsale       as decimal format "zzzzzzzz9.99-"   initial 0   no-undo.
def var s-addon         as decimal format "zzzz9.99-"       initial 0   no-undo.
def var s-tax           as decimal format "zzzzzzzz9.99-"   initial 0   no-undo.
def var s-stagecd       as c format "x(3)"                  initial ""  no-undo.
def var v-label         as c format "x(80)"                 initial ""  no-undo.
def var v-auth          as c format "x(14)"  initial "Authorization:"   no-undo.
def var s-retqtyord     as c format "x(1)"                  initial ""  no-undo.
def var s-retqtyship    as c format "x(1)"                  initial ""  no-undo.


/*o Total var for order types */
def var t-ordtype       as c format "x(20)" extent 10       initial
    ["Stock Order","Direct Order",
     "Counter Sales","Return Merchandise","Corrections",
     "Blanket Order","Blanket Release","Future Order","Quote",
     "Standing Order"]                                                  no-undo.
def var t-sales                as de format ">>>>>>>>>9.99-"  extent 10
                                                              initial 0 no-undo.
def var t-gross                as de format  ">>>>>>>>9.99-"  extent 10
                                                              initial 0 no-undo.
def var t-charge               as de format  ">>>>>>>>9.99-"  extent 10
                                                              initial 0 no-undo.
def var t-down                 as de format  ">>>>>>>>9.99-"  extent 10
                                                              initial 0 no-undo.
def var t-count                as i  format ">>>>>9"          extent 10
                                                              initial 0 no-undo.
def var t-lines                as i  format ">>>>>>9"         extent 10
                                                              initial 0 no-undo.

def var t-tosales              as de format ">>>>>>>>>>9.99-" initial 0 no-undo.
def var t-tocstgds             as de format  ">>>>>>>>>9.99-" initial 0 no-undo.
def var t-togross              as de format  ">>>>>>>>>9.99-" initial 0 no-undo.
def var t-tomarg               as de format      ">>>>>9.99-" initial 0 no-undo.
def var t-tocharge             as de format  ">>>>>>>>>9.99-" initial 0 no-undo.
def var t-todown               as de format  ">>>>>>>>>9.99-" initial 0 no-undo.
def var t-tototord             as de format ">>>>>>>>>>9.99-" initial 0 no-undo.
def var t-tocount              as i  format ">>>>>>9"         initial 0 no-undo.
def var t-tolines              as i  format ">>>>>>9"         initial 0 no-undo.

def var z-tosales              as de format ">>>>>>>>>>9.99-" initial 0 no-undo.
def var z-tocstgds             as de format  ">>>>>>>>>9.99-" initial 0 no-undo.
def var z-togross              as de format  ">>>>>>>>>9.99-" initial 0 no-undo.
def var z-tomarg               as de format      ">>>>>9.99-" initial 0 no-undo.
def var z-tocharge             as de format  ">>>>>>>>>9.99-" initial 0 no-undo.
def var z-todown               as de format  ">>>>>>>>>9.99-" initial 0 no-undo.
def var z-tototord             as de format ">>>>>>>>>>9.99-" initial 0 no-undo.
def var z-tocount              as i  format ">>>>>>9"         initial 0 no-undo.
def var z-tovendreb            as de format ">>>>>>>>>9.99-"  initial 0 no-undo.
def var z-tocustreb            as de format ">>>>>>>>>9.99-"  initial 0 no-undo.
def var z-tolines              as i  format ">>>>>>9"         initial 0 no-undo.
def var z-lostcnt              as i  format ">>>>>9"          initial 0 no-undo.
def var z-lostnet              as i  format ">>>>>>>>>>9.99-" initial 0 no-undo.
def var z-lostord              as i  format ">>>>>>>>>>9.99-" initial 0 no-undo.
def var totslsrep              as logical                    initial no no-undo.

def var z-sales      as de format ">>>>>>>>>9.99-" extent 10 initial 0 no-undo.
def var z-cstgds     as de format  ">>>>>>>>9.99-" extent 10 initial 0 no-undo.
def var z-custreb    as de format ">>>>>>>>>9.99-" extent 10 initial 0 no-undo.
def var z-vendreb    as de format ">>>>>>>>>9.99-" extent 10 initial 0 no-undo.
def var z-charge     as de format  ">>>>>>>>9.99-" extent 10 initial 0 no-undo.
def var z-down       as de format  ">>>>>>>>9.99-" extent 10 initial 0 no-undo.
def var z-totord     as de format ">>>>>>>>>9.99-" extent 10 initial 0 no-undo.
def var z-count      as de format ">>>>>>9"        extent 10 initial 0 no-undo.
def var z-lines      as de format ">>>>>>9"        extent 10 initial 0 no-undo.

/***************************/
def var q-sales      as de format ">>>>>>>>>9.99-" extent 10 initial 0 no-undo. 
def var q-cstgds     as de format  ">>>>>>>>9.99-" extent 10 initial 0 no-undo. 
def var q-custreb    as de format ">>>>>>>>>9.99-" extent 10 initial 0 no-undo. 
def var q-vendreb    as de format ">>>>>>>>>9.99-" extent 10 initial 0 no-undo. 
def var q-charge     as de format  ">>>>>>>>9.99-" extent 10 initial 0 no-undo. 
def var q-down       as de format  ">>>>>>>>9.99-" extent 10 initial 0 no-undo. 
def var q-totord     as de format ">>>>>>>>>9.99-" extent 10 initial 0 no-undo. 
def var q-count      as de format ">>>>>>9"        extent 10 initial 0 no-undo. 
def var q-lines      as de format ">>>>>>9"        extent 10 initial 0 no-undo. 
/****************************/

def {1} shared var lt-sales    as de format ">>>>>>>>>9.99-"  extent 10
                                                              initial 0 no-undo.
def {1} shared var lt-cstgds   as de format ">>>>>>>>>9.99-"  extent 10
                                                              initial 0 no-undo.
def {1} shared var lt-gross    as de format ">>>>>>>>>9.99-"  extent 10
                                                              initial 0 no-undo.
def {1} shared var lt-count    as i  format ">>>>>9"          extent 10
                                                              initial 0 no-undo.
def {1} shared var t-lostcnt   as i  format ">>>>>9"          initial 0 no-undo.
def {1} shared var t-lostnet   as de format ">>>>>>>>>>9.99-" initial 0 no-undo.
def {1} shared var t-lostord   as de format ">>>>>>>>>>9.99-" initial 0 no-undo.
def {1} shared var s-backorder as logical                    initial no no-undo.

def {1} shared var v-smcustrebfl  like sasc.smcustrebfl                 no-undo.
def {1} shared var v-smvendrebfl  like sasc.smvendrebfl                 no-undo.
def {1} shared var v-totrebamt    like oeeh.vendrebord                  no-undo.

def            var v-rebatety     as c format "x"                       no-undo.

/*d Range & Option Variables (all shared) */
def {1} shared var b-custno     like oeeh.custno                        no-undo.
def {1} shared var e-custno     like oeeh.custno                        no-undo.
def {1} shared var b-orderno    like oeeh.orderno                       no-undo.
def {1} shared var e-orderno    like oeeh.orderno                       no-undo.
def {1} shared var b-enterdt    like oeeh.enterdt                       no-undo.
def {1} shared var e-enterdt    like oeeh.enterdt                       no-undo.
def {1} shared var b-prod       like oeel.shipprod                      no-undo.
def {1} shared var e-prod       like oeel.shipprod                      no-undo.
def {1} shared var b-stagecd    like oeeh.stagecd                       no-undo.
def {1} shared var e-stagecd    like oeeh.stagecd                       no-undo.
def {1} shared var b-transtype  like oeeh.transtype                     no-undo.
def {1} shared var e-transtype  like oeeh.transtype                     no-undo.
def {1} shared var b-slsrepin   like oeeh.slsrepin                      no-undo.
def {1} shared var e-slsrepin   like oeeh.slsrepin                      no-undo.
def {1} shared var b-slsrepout  like oeeh.slsrepout                     no-undo.
def {1} shared var e-slsrepout  like oeeh.slsrepout                     no-undo.
def {1} shared var b-oper       like oeeh.takenby                       no-undo.
def {1} shared var e-oper       like oeeh.takenby                       no-undo.
def {1} shared var b-whse       like oeeh.whse                          no-undo.
def {1} shared var e-whse       like oeeh.whse                          no-undo.
def {1} shared var b-region     like icsd.region                        no-undo.
def {1} shared var e-region     like icsd.region                        no-undo.
def {1} shared var b-invoicedt  like oeeh.invoicedt                     no-undo.
def {1} shared var e-invoicedt  like oeeh.invoicedt                     no-undo.
def {1} shared var b-divno      like oeeh.divno                         no-undo.
def {1} shared var e-divno      like oeeh.divno                         no-undo.
def {1} shared var p-printord   as c format "x(1)"                      no-undo.
def {1} shared var p-printsls   as c format "x(1)"                      no-undo.
def {1} shared var p-printnonst as c format "x(1)"                      no-undo.
def {1} shared var p-printback  as logical                              no-undo.
def {1} shared var p-printship  as c format "x(1)"                      no-undo.
def {1} shared var p-printline  as logical                              no-undo.
def {1} shared var p-printtot   as logical                              no-undo.
def {1} shared var v-oecostsale like sasc.oecostsale                    no-undo.
def {1} shared var v-count      as i format ">>>>>9"                    no-undo.
def {1} shared var s-taxhdg1    as c format "x(8)"                      no-undo.
def {1} shared var s-taxhdg2    as c format "x(7)"                      no-undo.
def {1} shared var t-cstgds     as de format  ">>>>>>>>9.99-" extent 10 no-undo.
def {1} shared var t-totord     as de format ">>>>>>>>>9.99-" extent 10 no-undo.
def {1} shared var v-lines      as i format ">>>>>>9"                   no-undo.

def {1} shared var b-mgr           like smsn.mgr       no-undo.    /*si01*/
def {1} shared var e-mgr           like smsn.mgr       no-undo.    /*si01*/
def {1} shared var b-slstype       like smsn.slstype   no-undo.    /*si01*/
def {1} shared var e-slstype       like smsn.slstype   no-undo.    /*si01*/
def {1} shared var v-wideopen      as logical          no-undo.    /*si01*/
def {1} shared var p-slstype       as c format "x(1)"  no-undo.

def {1} shared var v-oerebty    like sasoo.oerebty                      no-undo.

def {1} shared var lt-rebfl     as c  format "x"       extent 10        no-undo.
def var t-rebfl         as c  format "x"               extent 10        no-undo.
def var t-vendreb       as de format ">>>>>>>>>9.99-"  extent 10 init 0 no-undo.
def var t-custreb       as de format ">>>>>>>>>9.99-"  extent 10 init 0 no-undo.
def var t-tovendreb     as de format ">>>>>>>>>9.99-"            init 0 no-undo.
def var t-tocustreb     as de format ">>>>>>>>>9.99-"            init 0 no-undo.
def var t-torebatefl    as c format "x"                                 no-undo.
def var v-rebatefl      as c format "x"                                no-undo. 
def var s-slstype       as c format "x(3)"              no-undo.
def var foundrep        as c format "x"                 no-undo.
def var s-csrfield      as c format "x(17)"             no-undo.
def var s-drilldown     as c format "x(5)"              no-undo.
def var s-ddvalue       as c format "x(3)"              no-undo.

def {1} shared var sw-takenby-only      as logical      no-undo.

/*tb e7852 02/13/01 lcg; A/R Multicurrency variables */
def new   shared  var v-exchrate like sastc.salesexrate               no-undo.
def            var s-domaddon      like s-addon                       no-undo.
def            var s-domtax        like s-tax                         no-undo.
def            var s-domnetsale    like s-netsale                     no-undo.
def            var s-dommargin     like s-margin                      no-undo.
def            var s-dommargpct    like s-margpct                     no-undo.
def            var s-domtotlineord like oeeh.totlineord               no-undo.
def            var s-domprice      like oeel.price                    no-undo.
def            var s-domdiscamt    like s-discamt                     no-undo.
def            var s-domcorecharge like v-corecharge                  no-undo.
def            var s-dommardiscoth like v-mardiscoth                  no-undo.
def            var s-dommarginpct  like s-marginpct                   no-undo.
def            var s-domprodcost   like oeel.prodcost                 no-undo.

/* das - New Customer Mod */
def {1} shared var zsdi-toprinter  as logical init no                 no-undo.
def {1} shared var zsdi-cust       like zsdiarsc.custno               no-undo.
def {1} shared var w-cust          as c format "x(13)"                no-undo.
