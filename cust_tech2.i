
/************ main block ***********/

/* run chk_4_update.  */

/******* procedure block **********/

procedure load_temp_tbl:

  repeat: 
    inx = inx + 1.
    import stream custin delimiter "," c_cust c_shipto c_slsmn c_tech.
     find cst where cst_cono = 1 and cst_shipto = c_shipto and 
                   cst_cust = dec(c_cust) and
                   cst_tech = c_tech no-lock no-error.

     /* This is just ignoring dups if they are really the same */
     if avail cst then /* and cst_slsrepin = c_slsmn then */
        do:
        next.
        end.
     
    create cst.
    assign cst_cono = 1.
    assign cst_cust = dec(c_cust).
    assign cst_shipto = c_shipto.
    assign cst_slsrepin  = c_slsmn.
    assign cst_tech = c_tech.
/*    display cst_cust c_slsmn.       */
  end.

end.

procedure chk_4_entry:
customer_lookup:
  do:
  h_techtype = " ".
  find first cst where cst_cono = 1  and 
                       cst_cust = dec(h_cust) and 
                       cst_shipto = h_shipto and 
                       cst_tech = h_tech no-lock no-error.
    if avail cst then 
       if cst_slsrepin ne h_slsrepin then                   
          assign h_slsrepin = cst_slsrepin
                 h_techtype = cst_tech.             
       else
         assign h_slsrepin = h_slsrepin
                h_techtype = cst_tech.
  /*
    else
      do:
      find first cst where cst_cono = 1  and 
                           cst_cust = dec(h_cust)  
                           no-lock no-error.
      if avail cst then 
        if cst_slsrepin ne h_slsrepin then                   
          assign h_slsrepin = cst_slsrepin.
      end.                  
  */
  end.
end.
    