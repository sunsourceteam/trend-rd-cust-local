/******************************************************************************
 p-arrg.i    04/29/99 SD
******************************************************************************/
def buffer b-aret for aret.
/*  
/* trtrans.aret AR Transactions */
find first b-aret use-index k-cod where b-aret.cono       = aret.cono and
                                        b-aret.invno      = aret.invno and
                                        b-aret.invsuf     = aret.invsuf and
                                        b-aret.statustype = true and
                                        b-aret.transcd    = 11 and
                                        b-aret.sourcecd   = aret.transcd
no-lock no-error.
      
if avail b-aret then do:
  if aret.transcd = 0 and b-aret.period = 0 then
      assign v-currentfl = yes.
  else if aret.transcd = 1 and b-aret.period = 9 then
      assign v-currentfl = yes.
  else assign v-currentfl = no.    
end.                                         
*/

 /* put future invoices and service charges in current slot */
 if aret.transcd = 11 then do:
     if aret.sourcecd = 0 and aret.period = 0 then
         assign v-currentfl = yes.
     else if aret.sourcecd = 1 and aret.period = 9 then
         assign v-currentfl = yes.
     else assign v-currentfl = no.
 end.
 else 
     assign v-currentfl = no.    

/*
assign
    s-payment  = aret.paymtamt + aret.discamt - aret.pifamt
    s-dueamt   = aret.amount   - s-payment.*/
/*    s-discount = aret.origdisc - aret.discamt.  /*  remaining disc avail   */
    if s-discount < 0 then s-discount = 0.*/

assign s-dueamt = aret.amount.

create t-arrgdet.
assign
   t-arrgdet.cono      = arsc.cono
   t-arrgdet.custno    = arsc.custno
   t-arrgdet.creditmgr = arsc.creditmgr
   t-arrgdet.invno     = string(aret.invno) + "-" + string(aret.invsuf,"99")
   t-arrgdet.invsuf    = aret.invsuf
   t-arrgdet.seqno     = aret.seqno
   t-arrgdet.jrnlno    = aret.jrnlno
   t-arrgdet.setno     = aret.setno
   t-arrgdet.invcurr   = 0.0
   t-arrgdet.invage1   = 0.0
   t-arrgdet.invage2   = 0.0
   t-arrgdet.invage3   = 0.0
   t-arrgdet.invage4   = 0.0
   t-arrgdet.invage5   = 0.0
   t-arrgdet.invage6   = 0.0.

   if aret.transcd = 3 then 
     assign t-arrgdet.invno = "Unapp Cash".

  /*  if s-newcust  = yes then do:
      assign s-current = 0.0 
             s-age1    = 0.0
             s-age2    = 0.0
             s-age3    = 0.0
             s-age4    = 0.0
             s-age5    = 0.0
             s-age6    = 0.0
             s-invdays = 0
             s-newcust = no.
    end. /* if newcust */         
    */
   /* if (today - aret.invdt) gt s-invdays then
        assign s-invdays = today - aret.invdt.            */

    if v-currentfl 
      then assign s-current = s-current + s-dueamt.
    else do:   
        if (today - aret.duedt) le 15 
          then assign s-current = s-current + s-dueamt
                      t-arrgdet.invcurr = s-dueamt.
                      
        if (today - aret.duedt) gt 15 and (today - aret.duedt) le 30
          then assign s-age1 = s-age1 + s-dueamt
                      t-arrgdet.invage1 = s-dueamt.
                      
        if (today - aret.duedt) gt 30 and (today - aret.duedt) le 60
          then assign s-age2 = s-age2 + s-dueamt
                      t-arrgdet.invage2 = s-dueamt.
                      
        if (today - aret.duedt) gt 60 and (today - aret.duedt) le 90
          then assign s-age3 = s-age3 + s-dueamt
                      t-arrgdet.invage3 = s-dueamt.
                      
        if (today - aret.duedt) gt 90 and (today - aret.duedt) le 180
          then assign s-age4 = s-age4 + s-dueamt
                      t-arrgdet.invage4 = s-dueamt.
                      
        if (today - aret.duedt) gt 180 and (today - aret.duedt) le 365
          then assign s-age5 = s-age5 + s-dueamt
                      t-arrgdet.invage5 = s-dueamt.
        
        if (today - aret.duedt) gt 365
          then assign s-age6 = s-age6 + s-dueamt
                      t-arrgdet.invage6 = s-dueamt.

        if aret.duedt = ? then
           assign s-current = s-current + s-dueamt.
     end.      
                               
