/* g-arej.i 1.1 01/03/98 */
/* g-arej.i 1.4 7/14/93 */
/***************** g-arej.i *********************************************/
def {1} shared var t-glamtdr as dec format ">>>>>>>>>9.99-" initial 0 no-undo.
def {1} shared var t-glamtcr as dec format ">>>>>>>>>9.99-" initial 0 no-undo.
def {1} shared var v-ptype         like sasp.ptype no-undo.
def {1} shared var v-jrnlno        like sasj.jrnlno no-undo.
def {1} shared var v-sasprec       as recid no-undo.
def {1} shared var v-jrnlcnt       as integer initial 0 no-undo.
def {1} shared var b-setno         like aret.setno no-undo.
def {1} shared var e-setno         like aret.setno no-undo.
def {1} shared var b-period        like sasj.period no-undo.
def {1} shared var e-period        like sasj.period no-undo.
def {1} shared var p-printtrans    as c format "x"    initial "" no-undo.
def {1} shared var p-printgl       as logical initial no no-undo.
def {1} shared var p-printtot      as logical initial no no-undo.

def var s-glno          as character format "x(21)" initial "" no-undo.
def var s-lookupnm      as character format "x(15)" initial "" no-undo.
def var v-transno       like glet.transno no-undo.
def var v-glno          as character format "x(21)" initial 0  no-undo.
def var v-gltitle       as character format "x(32)" initial "" no-undo.
def var v-glamtdr       like glet.amount no-undo.
def var v-glamtcr       like glet.amount no-undo.

/** w-journal for journal totals **/
def {1} shared temp-table w-journal no-undo
    field w-close       as logical initial no
    field w-ourproc     like sasj.ourproc
    field w-jrnlno      like aret.jrnlno
    index k-jrnlno
          w-jrnlno.

/** w-acct for total of general ledger accounts **/
def {1} shared temp-table w-acctno 
    field acct          as char format "x(5)"
    field gtitle        as char format "x(30)"
    field cntr          as int  format "9999999"
    field grtotdr       as dec  format "-zzzzzzzz9.99"
    field grtotcr       as dec  format "-zzzzzzzz9.99"
    index k-acctno
          acct.

/** w-dist records for g/l distribution **/
def {1} shared temp-table w-dist 
    field w-gljrnlno    like glet.jrnlno
    field w-function    like glet.currproc
    field w-glacctno    as c format "x(5)"
    field w-glno        as c format "x(21)"
    field w-title       like v-gltitle
    field w-glamtdr     like glet.amount
    field w-glamtcr     like glet.amount
    field w-gltotdr     as dec format "zzzzzzzzz9.99-"
    index k-dist
          w-gljrnlno  
          w-glacctno.

def {1} shared var s-title          as c format "x(19)"           no-undo.
def {1} shared var s-descrip        like sastn.descrip            no-undo.
def {1} shared var v-multidivset    as logical                    no-undo.
def {1} shared var v-multidivjrnl   as logical                    no-undo.
def {1} shared var v-multidivrprt   as logical                    no-undo.
def {1} shared var v-labelflag      as logical                    no-undo.
def {1} shared var v-origdivno      like glet.gldivno             no-undo.
def {1} shared var v-divdb          like report.de9d2s            no-undo.
def {1} shared var v-divcr          like report.de9d2s-2          no-undo.
