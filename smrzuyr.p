/*
SX 55

SMRZK Product Category Reporting
Author T. Herzog

This report generates sales reports based on parameter input using product
category sales summary file SMSEP. Combining it with some detail transactions
to determine component level sales from all modules KP, OE, and VA.
*/

Define stream EXPT.
Define Var v-del as c format "x(1)" no-undo.
define buffer zz-icsd for icsd.
{p-rptbeg.i}


{cust_tech.i "new"}

 
 
/* cludge beacause of sales manager bug. */

{g-smrs.i new}
{speccost.gva}

def buffer b-oeel for oeel.
def buffer b-oeeh for oeeh.
def var forenddate as date no-undo.

/* for backlog */

def var bookedb4     as logical no-undo.
def var bofound4line as logical no-undo.

DEF var oeelqtyrel   as de              no-undo.
DEF var o_qty        as de              no-undo.
DEF var o_extcost    as de              no-undo.
DEF var o_wrkamt     as de              no-undo.

/* End Backlog  */

def var zsls       as decimal format ">>>,>>>,>>9.99-" no-undo. 
def var zmth       as int no-undo.
def var p-cost     as logical no-undo. 


Define buffer z-sapb for sapb.
Define Var jrk_optvalue as character format "x(24)" no-undo extent 20. 
Define Var xinx         as integer                 no-undo.
Define Var sum_ary      as character               no-undo.
Define Var x-summarybld       as character format "x(24)" no-undo. 
Define Var export_rec   as c format "x(300)"       no-undo.
define var cnt as int.
Define Var v-cludged    as logical  init false     no-undo. 
Define Var v-per1       as de                      no-undo.
Define Var v-per2       as de                      no-undo.
Define Var v-per3       as de                      no-undo.
Define Var v-per4       as de                      no-undo.
Define Var v-per5       as de                      no-undo.
Define Var v-per6       as de                      no-undo.
Define Var v-per7       as de                      no-undo.
Define Var v-per8       as de                      no-undo.
Define Var v-per9       as de                      no-undo.
Define Var v-amt1       as de                      no-undo.
Define Var v-amt2       as de                      no-undo.
Define Var v-amt3       as de                      no-undo.
Define Var v-amt4       as de                      no-undo.
Define Var v-amt5       as de                      no-undo.
Define Var v-amt6       as de                      no-undo.
Define Var v-amt7       as de                      no-undo.
Define Var v-amt8       as de                      no-undo.
Define Var v-amt9       as de                      no-undo.
Define Var v-amt10      as de                      no-undo.
Define Var v-amt11      as de                      no-undo.
Define Var v-amt12      as de                      no-undo.
Define Var v-amt13      as de                      no-undo.
Define Var v-amt14      as de                      no-undo.
Define Var v-amt15      as de                      no-undo.
Define Var v-amt16      as de                      no-undo.
Define Var xit          as logical               no-undo.
Define Var firstbreak as logical                 no-undo.
Define Var v-descend    as logical               no-undo.
Define Var founddata  as logical  init false     no-undo.
Define Var v-cycles   as integer                 no-undo.
Define Var v-delim    as c format "x"            no-undo.
Define Var v-hicust   as dec init 999999999999   no-undo.
define Var v-message  as char format "x(30)"     no-undo.
Define Var v-changecnt as integer                no-undo.
Define Var p-asofdate as integer format "9999"   no-undo.
Define Var p-district as character format "x(3)" no-undo.
Define Var p-otypebeg  as character format "x(2)" no-undo.
Define Var p-otypeend  as character format "x(2)" no-undo.
Define Var p-distbeg  as character format "x(3)" no-undo.
Define Var p-distend  as character format "x(3)" no-undo.
Define Var p-region   as character format "x(1)" no-undo.
Define Var p-regbeg   as character format "x(1)" no-undo.
Define Var p-regend   as character format "x(1)" no-undo.
Define Var p-slsrep   as character format "x(4)" no-undo.
Define Var p-repbeg   as character format "x(4)" no-undo.
Define Var p-repend   as character format "x(4)" no-undo.
Define Var p-whsebeg  as character format "x(4)" no-undo.
/* Abbrv tech names
   PNEU
   HYDR
   MOBL
   FILT
   AUTO
   SERV
   NONE
   CONN
   FABR
*/ 

Define Var p-whseend  as character format "x(4)" no-undo.
Define Var p-techbeg  as character format "x(4)" no-undo.
Define Var p-techend  as character format "x(4)" no-undo.
Define new shared Var p-kcatbeg  as character format "x(4)" no-undo.
Define new shared Var p-kcatend  as character format "x(4)" no-undo.
Define Var p-divbeg   as int no-undo.
Define Var p-divend   as int no-undo.
Define Var p-vpbeg   as character format "x(24)" no-undo.
Define Var p-vpend   as character format "x(24)" no-undo.
Define Var p-vidbeg   as character format "x(24)" no-undo.
Define Var p-vidend   as character format "x(24)" no-undo.


Define Var p-catbeg   as character format "x(4)" no-undo.
Define Var p-catend   as character format "x(4)" no-undo.
Define Var p-datebeg  as date                    no-undo.
Define Var p-dateend  as date                    no-undo.
Define Var p-custbeg  like arsc.custno           no-undo.
Define Var p-custend  like arsc.custno           no-undo.
Define Var p-acctbeg     as integer              no-undo.
Define Var p-acctend     as integer              no-undo.

def var p-list  as logical           no-undo.
def var p-kp      as logical         no-undo.
def var p-bodkits as logical         no-undo.
def var p-vastuff as logical         no-undo.
def var p-costonly as logical        no-undo.
Define Var p-optiontype as character format "x(15)" no-undo.
Define Var p-sorttype as character format "x(15)" no-undo.
Define Var p-totaltype as character format "x(15)" no-undo.
Define Var p-summtype as character format "x(15)" no-undo.
Define Var p-summcounts as character format "x(24)" no-undo.
Define Var p-detail   as character format "x"     no-undo.
Define Var p-export   as character format "x(24)" no-undo.
Define Var p-exportl  as logical                  no-undo.
Define Var p-ignoreKP  as logical                 no-undo.
Define Var p-ignoreVA  as logical                 no-undo.
Define Var p-ignoreBOD as logical                 no-undo.
Define Var h-masterfisc as date                   no-undo.
Define Var v-inx      as integer                  no-undo.
Define Var v-inx2     as integer                  no-undo.
Define Var v-inx3     as integer                  no-undo.
Define Var v-inx4     as integer                  no-undo.
Define Var v-entitys  as integer                  no-undo.
Define Var summ-count as integer                  no-undo.
Define Var v-token    as c      format "x(100)"   no-undo.
/* If more totals are needed just add t-ammounts2..3..4  */

Define Var t-amounts   as dec            extent 10
                                                  no-undo.
Define Var t-amountc   as dec            extent 10
                                                  no-undo.

Define Var t-amounts1  as dec            extent 10
                                                  no-undo.
Define Var t-amounts2  as dec            extent 10
                                                  no-undo.
Define Var t-amounts3  as dec            extent 10
                                                  no-undo.
Define Var t-amounts4  as dec            extent 10
                                                  no-undo.
Define Var t-amounts5  as dec            extent 10
                                                  no-undo.
Define Var t-amounts6  as dec            extent 10
                                                  no-undo.
Define Var t-amounts7  as dec            extent 10
                                                  no-undo.
Define Var t-amounts8  as dec            extent 10
                                                  no-undo.
Define Var t-amounts9  as dec            extent 10
                                                  no-undo.
Define Var t-amounts10  as dec            extent 10                                                  no-undo.
Define Var t-amounts11  as dec            extent 10
                                                  no-undo.
Define Var t-amounts12  as dec            extent 10
                                                  no-undo.
Define Var t-amounts13  as dec            extent 10
                                                  no-undo.
Define Var t-amounts14  as dec            extent 10
                                                 no-undo.
Define Var t-amounts15  as dec            extent 10
                                                  no-undo.
Define Var t-amounts16  as dec            extent 10
                                                  no-undo.
                                                  
                                                  
Define Var v-lookup   as character format "x(2)" extent 10
                                                  no-undo.
Define Var v-sortup   as character format "x(1)" extent 10
                                          init [">",
                                                ">",
                                                ">",
                                                ">",
                                                ">",
                                                ">",
                                                ">",
                                                ">",
                                                ">",
                                                ">"]
                                                  no-undo.
Define Var v-totalup  as character format "x(1)" extent 10
                                          init ["t",
                                                "t",
                                                "t",
                                                "t",
                                                "t",
                                                "t",
                                                "t",
                                                "t",
                                                "t",
                                                "t"]
                                                  no-undo.
Define Var v-summup   as character format "x(1)" extent 10
                                          init [" ",
                                                " ",
                                                " ",
                                                " ",
                                                " ",
                                                " ",
                                                " ",
                                                " ",
                                                " ",
                                                " "]
                                                  no-undo.
Define Var v-subtotalup   as int   format "9" extent 10
                                          init [4,
                                                4,
                                                4,
                                                4,
                                                4,
                                                4,
                                                4,
                                                4,
                                                4,
                                                4]
                                                  no-undo.

Define Var v-summcounts     as char   format "x(6)" extent 10
                                          init ["ALL   ",
                                                "ALL   ",
                                                "ALL   ",
                                                "ALL   ",
                                                "ALL   ",
                                                "ALL   ",
                                                "ALL   ",
                                                "ALL   ",
                                                "ALL   ",
                                                "ALL   "]
                                                  no-undo.
                                                  



Define Var v-mystery  as character format "x(100)"
                                                  no-undo.
Define Var v-sortmystery  as character format "x(100)"
                                                  no-undo.
Define Var v-trimmystery  as character format "x(100)"
                                                  no-undo.
Define Var v-holdmystery  as character format "x(100)"
                                                  no-undo.
Define Var v-newmystery   as character format "x(100)"
                                                  no-undo.
Define Var v-holdsortmystery  as character format "x(100)"
                                                  no-undo.
Define Var v-sizer    as integer                  no-undo.
Define Var u-entitys  as integer                  no-undo.
Define Var x-entitys  as integer                  no-undo.
Define Var x-token    as c                        no-undo.
Define Var v-stotcnt  as int                      no-undo.
Define Var vl-slsrep   like smsn.slsrep            no-undo.
Define Var v-region   as character format "x(1)"  no-undo.
Define Var v-district as character format "x(3)"  no-undo.
Define Var v-final    as character format "x(1)"  no-undo.
Define Var v-technology  as character format "x(30)"  no-undo.
Define Var v-val      as de                       no-undo.





Define Var q-slsrep   like smsn.slsrep            no-undo.
Define Var q-state    as char                     no-undo.
Define Var q-zip      like arsc.zipcd             no-undo.
Define Var q-region   as character format "x(1)"  no-undo.
Define Var q-district as character format "x(3)"  no-undo.
Define Var q-custno   like arsc.custno            no-undo.
Define Var v-summary-lit    as character format "x(8)"
                                                  no-undo.
Define Var v-summary-lit2   as character format "x(40)"
                                                  no-undo.
                                                  
Define Var v-summary-amount as dec      
                                                  no-undo.
Define Var v-summary-val    as character format "x(12)"
                                                  no-undo.
Define Var v-astrik         as character format "x(6)" 
                                                  no-undo.
Define Var h-lookup         as character format "x(15)" no-undo.
Define Var h-repname        as character format "x(30)" no-undo.
Define Var h-genlit         as character format "x(30)" no-undo.
Define Var h-underline      as character format "x(176)" no-undo.

/* Begin Used for reverse coalating sorts  */
Define Var ascii_index as integer no-undo.
Define Var ascii_length as integer no-undo.



define temp-table ascii_bit_swaper
 field char  as char
 field dec   as int
 field undec as int
index ascii_inx dec.

/* End Used for reverse coalating sorts  */


define var s_mgr as char format "x(4)" no-undo.
define var s_name as char format "x(15)" no-undo.


define buffer catmaster for notes.

def new shared temp-table zicp no-undo
  field prod    like icsp.prod
index p
  prod.
  



def new shared temp-table xwo no-undo
  field whse   like icsw.whse
  field custno like arsc.custno
  field shipto like arss.shipto
  field pcat   like icsp.prodcat
  field prod   like icsp.prod
  field yr     as integer
  field mnth   as integer
  field qty    as decimal
  field qtyfl  as decimal
  field salesamt as decimal
  field wono   like kpet.wono
  field wosuf  like kpet.wosuf
index
  whse
  custno
  shipto
  prod
  yr
  mnth descending.





def new shared var yrz as integer extent 2 no-undo.
def var inz as integer          no-undo.

def buffer b-icsp for icsp.
def buffer b-oeelk for oeelk.
def var cc as integer.

def new shared var forbegdate as date no-undo.
def new shared var forupdate as date no-undo.
def var hold-orderno like oeeh.orderno no-undo.
def var hold-ordersuf like oeeh.ordersuf no-undo.
def var hold-lineno like oeel.lineno no-undo.
def var a-year as integer extent 2.
def var a-month as integer extent 2.

def var m-lbl   as c  format "x(9)" extent 3 no-undo.
def var narr1   as c  format "x(10)" no-undo.
def var narr2   as c  format "x(30)" no-undo.
def var narr3   as c  format "x(4)"  no-undo.
def var as-per  as int               no-undo.
def var e-yr    as int               no-undo.
def var e-mo    as int               no-undo.
def var x-yr    as int               no-undo.
def var bgdt    as date              no-undo.
def var eddt    as date              no-undo.
def var z-cat   as c   format "x(5)" no-undo.
def var l-process as logical         no-undo.

/* ?*/

def var newreport as logical no-undo.
def var newpage as logical no-undo.
def var printcust as logical no-undo.
def var validcustinfo as logical no-undo.
def var zk-cost as decimal no-undo.
def var zk-price as decimal no-undo.
def var zk-type as decimal no-undo.
def var zk-found as logical no-undo.
def var vz-cost as decimal no-undo.
def var vz-price as decimal no-undo.
def var v-rebate as decimal no-undo.
def var vz-prodcat as character format "x(4)" no-undo.
def var z-tild  as character format "x" init "~~" no-undo.
def var z-hash  as character format "x" init "#" no-undo.
def var z-cart  as character format "x" init "^" no-undo.
def var z-atsn  as character format "x" init "@" no-undo.
def var z-pcsn  as character format "x" init "%" no-undo.
def var v-vendorid as c format "x(24)" no-undo.
def var v-parentcode as c format "x(24)" no-undo.
def var attr1beg as c format "x(24)" no-undo.
def var attr1end as c format "x(24)" no-undo.
def var attr2beg as c format "x(24)" no-undo.
def var attr2end as c format "x(24)" no-undo.
def var attr3beg as c format "x(24)" no-undo.
def var attr3end as c format "x(24)" no-undo.
def var pk_cat as character format "x(5)" no-undo.
def var pk_custno like arsc.custno no-undo.
def var pk_shipto like arss.shipto no-undo.
def var pk_good   as logical       no-undo.
def var n-mth as c format "x(9)" extent 12 no-undo
init [' January ',
      'February ',
      '  March  ',
      '  April  ',
      '   May   ',
      '  June   ',
      '  July   ',
      ' August  ',
      'September',
      ' October ',
      ' November',
      ' December'].

{z-fiscperiodsN.i "new"}
{zsapboydef.i}


def var v-peckinx as integer no-undo.
def var zzx as integer no-undo.

def var v-peckinglist as character format "x(4)" extent 10 init
  ["fmec",
   "fgr ",
   "fbir",
   "dhou",
   "dmcp",
   "datl",
   "decp",
   "dscp",
   "dwcp",
   "dros"].


DEF temp-table whses no-undo
  field zwhse like icsw.whse
  field zcnt as integer
  
  index k-zpecking
        zcnt
  index k-zwhse
        zwhse.





def new shared temp-table zsmsep no-undo 
  Field cono        as integer
  Field yr          as integer format "99"
  Field custno      as decimal format "zzzzzzzzzzz9"
  Field shipto      as char format "x(8)"
  Field prodcat     as char format "x(5)"
  Field whse        as char format "x(4)"
  Field salesamt    as decimal format "zzzzzzzz9.99-" extent 13
                       init [0,0,0,0,0,0,0,0,0,0,0,0] 
  Field discamt     as decimal format "zzzzzzzz9.99-" extent 13
                       init [0,0,0,0,0,0,0,0,0,0,0,0]
  Field qtysold     as decimal format "zzzzzzzz9.99-" extent 13
                       init [0,0,0,0,0,0,0,0,0,0,0,0] 
  Field cogamt      as decimal format "zzzzzzzz9.99-" extent 13
                       init [0,0,0,0,0,0,0,0,0,0,0,0] 
  Field prntfl      as logical
  
  index k-prodcat
    cono
    yr
    prodcat
    whse
    custno
    shipto.


define buffer b-zsmsep for zsmsep.


DEFINE TEMP-TABLE drpt no-undo
    FIELD sortmystery as character format "x(100)"
    FIELD mystery     as character format "x(100)"
    FIELD stotmystery as character format "x(100)"
    FIELD region    AS CHARACTER FORMAT "X(4)"
    FIELD district  AS CHARACTER FORMAT "x(2)"
    FIELD slsrepin  like oeeh.slsrepin
    FIELD slsrepout LIKE oeeh.slsrepout
    FIELD slsmname  LIKE smsn.name
    FIELD whse      like oeeh.whse
    FIELD custno    LIKE oeeh.custno
    FIELD shipto    like arss.shipto
    FIELD custname  LIKE arsc.name
    FIELD pcat      as c 
    FIELD sell      AS decimal  extent 8
    FIELD cost      as decimal  extent 8
 index dinx
       stotmystery
 index dinx2
       sortmystery.


 DEFINE buffer b-drpt for drpt.

/* ------------------------------------------------------------------------
   Variables Used To Create Trend Like Header
   -----------------------------------------------------------------------*/


DEFINE VARIABLE rpt-dt      as date format "99/99/9999" no-undo.
DEFINE VARIABLE rpt-dow     as char format "x(3)"   no-undo. 
DEFINE VARIABLE rpt-time    as char format "x(5)"  no-undo.
DEFINE VARIABLE rpt-cono    like oeeh.cono         no-undo.
DEFINE VARIABLE rpt-user    as char format "x(08)" no-undo.
DEFINE VARIABLE x-title     as char format "x(177)" no-undo.
DEFINE VARIABLE x-page      as integer format ">>>9" no-undo.
DEFINE VARIABLE dow-lst     as character format "x(30)"
           init "Sun,Mon,Tue,Wed,Thu,Fri,Sat"                 no-undo.

DEFINE VARIABLE monthname   as char format "x(10)" EXTENT 12 initial
["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"].



/* -----------------------------------------------------------------------
   Header Variable Initialization
   ----------------------------------------------------------------------*/

rpt-user = userid("dictdb").
rpt-dt = today.
rpt-time = string(time, "HH:MM").
rpt-dow = entry(weekday(today),dow-lst).
rpt-cono = g-cono.
x-page = 0.

form header
  "~015 " at 1
  with frame f-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: SMRZK"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 168
  page-number          at 173 format ">>>9"
  "~015"              at 177
         "Product Category Sales Analysis Report" at 71
         "~015"              at 177
   with frame f-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header 
   "~015"                    at 177
  with frame f-trn3 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header 
    narr1   at 69
    narr2   at 80
    narr3   at 170
    "~015"  at 178 
    m-lbl[1] at 46
    m-lbl[2] at 67                                       /* 200 */
    m-lbl[3] at 91
    "Curr YTD" at 112
    "Prev YTD" at 133
    "Variance" at 154
    "~015"  at 178
    "Sales" at 45
    "Mgn"   at 55
    "Sales" at 66
    "Mgn"   at 76
    "Sales" at 87
    "Mgn"   at 97
    "Sales" at 108
    "Mgn"   at 118
    "Sales" at 129
    "Mgn"   at 139
    "Sales" at 150
    "S %"   at 161
    "Mgn"   at 168
    "~015"  at 178 
with frame f-trn4 
  down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.


form header 
   h-underline               at 1
   "~015"                    at 177
  with frame f-trn5 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.


  
form
  v-final          at 1
  v-astrik         at 2
  v-summary-lit2   at 10 format "x(28)"
  v-amt1               at 39 format ">>>,>>>,>>9-"
  v-amt2               at 52 format ">>>9.9-" 
  v-amt3               at 59 format ">>>,>>>,>>9-"
  v-amt4               at 73 format ">>>9.9-"  
  v-amt5               at 81 format ">>>,>>>,>>9-"
  v-amt6               at 94 format ">>>9.9-" 
  v-amt7               at 102 format ">>>,>>>,>>9-" 
  v-amt8               at 115 format ">>>9.9-"
  v-amt9               at 124 format ">>>,>>>,>>9-"
  v-amt10              at 136 format ">>>9.9-" 
  v-amt11              at 145 format ">>>,>>>,>>9-"
  v-amt12              at 157 format ">>>>.99-"
  v-amt13              at 165 format ">>>9.9-" 
  "~015"               at 178
with frame f-tot width 178
 no-box no-labels.  

form 
  "~015"           at 1
with frame f-skip width 178 no-box no-labels.  



/* ------ Begin of Parameter Handling and initialization -------- */




/*
assign
        v-perin = "0201"
        p-list     = false
        p-kp       = false
        p-bodkits  = false
        p-vastuff  = false
        p-costonly = false
        p-distbeg = "   "
        p-distend = "```"
        p-regbeg  = "   "
        p-regend  = "`"
        p-repbeg  = "    "
        p-repend  = "````"
        p-divbeg  = 0
        p-divend  = 999
        p-catbeg  = "    "
        p-catend  = "````"
        p-kcatbeg  = "    "
        p-kcatend  = "````"
        p-whsebeg  = "    "
        p-whseend  = "````"

        p-custbeg = 0
        p-custend =  9999999999
        p-optiontype = "r,d,s,p#"
        p-sorttype = ">,>,>,<"
        p-totaltype = "t,t,t,t"
        p-detail = "s".
*/
/* Begin Used for reverse coalating sorts  */
run ascii_coalation.
/* End Used for reverse coalating sorts    */


assign v-delim = chr(001) /* this is used for internal entry delimiter */
       v-del   = chr(09). /* this is used in excel output */
{w-sasc.i no-lock}

assign v-smcustrebfl = sasc.smcustrebfl 
       v-smvendrebfl = sasc.smvendrebfl. 

run crossreference.

if xit then
  leave.

/*
message 
 
 jrk_optvalue [1]
 jrk_optvalue [2]
 jrk_optvalue [3]
 jrk_optvalue [4]
 jrk_optvalue [5]
 jrk_optvalue [6]
 jrk_optvalue [7]
 jrk_optvalue [8]
 jrk_optvalue [9]
 jrk_optvalue [10]
 jrk_optvalue [11]
 jrk_optvalue [12].
 
 pause.
*/

assign 
        p-distbeg = sapb.rangebeg[2]
        p-distend = if sapb.rangeend[2] = "" then 
                      "```"
                    else
                     sapb.rangeend[2]
        p-regbeg  = sapb.rangebeg[1]
        p-regend  = if sapb.rangeend[1] = "" then "`" else sapb.rangeend[1]
        p-repbeg  = sapb.rangebeg[3]
        p-repend  = if sapb.rangeend[3] = "" then 
                      "````" 
                    else sapb.rangeend[3]
        p-custbeg = dec(sapb.rangebeg[4])
        p-custend = if dec(sapb.rangeend[4]) = 0 then v-hicust else
                       dec(sapb.rangeend[4])
        p-catbeg = sapb.rangebeg[5]
        p-catend = sapb.rangeend[5]
        p-kcatbeg = sapb.rangebeg[6]
        p-kcatend = sapb.rangeend[6]
        p-techbeg = sapb.rangebeg[7]
        p-techend = sapb.rangeend[7]
        p-vidbeg = sapb.rangebeg[8]
        p-vidend = sapb.rangeend[8]
        p-vpbeg = sapb.rangebeg[11]
        p-vpend = sapb.rangeend[11]
        p-whsebeg = sapb.rangebeg[9]
        p-whseend = sapb.rangeend[9]
        p-divbeg = int(sapb.rangebeg[10])
        p-divend = (if dec(sapb.rangeend[10]) > 999 then
                      999
                   else
                      int(sapb.rangeend[10]))
        v-perin = jrk_optvalue[1]
        p-optiontype = jrk_optvalue[2]
        p-sorttype = jrk_optvalue[3]
        p-totaltype = jrk_optvalue[4]
        p-summcounts = jrk_optvalue[5]
        p-kp        = if jrk_optvalue[6] = "yes" then yes else no
        p-bodkits   = if jrk_optvalue[7] = "yes" then yes else no
        p-vastuff   = if jrk_optvalue[8] = "yes" then yes else no
        p-list      = if jrk_optvalue[9] = "yes" then yes else no
        p-costonly  = if jrk_optvalue[10] = "yes" then yes else no
/* 11 is to take out  obsolete  */
        p-export    = jrk_optvalue[12]
        p-detail =  "S"
        p-ignoreKP = if jrk_optvalue[14] = "yes" then
                       true
                     else   
                       false
        p-ignoreBOD = if jrk_optvalue[15] = "yes" then
                       true
                     else   
                       false
        p-ignoreVA = if jrk_optvalue[16] = "yes" then
                       true
                     else   
                       false.
                       
/*
   This means we will be exporting to a comma delimited file for
   import into Excel 
*/   



{p-rptper.i}       
assign as-per = v-perout
       p-asofdate = v-perout
       h-underline = fill("-",176).       





v-sizer = length(p-optiontype).
/* bumping option physical length up by 2 because a # and (integer) can
   follow the option and the logic looks out possibly 2 chars to check
   we don't want to run out of string to look at.
*/
assign substring(p-optiontype,(v-sizer + 1),2) = "  ".


/*
 Parse option string looking for the sort order as well as the
 sort - or subtotal S#1 options. That is why above 2 characters are
 added to the input in case the last option is implied. If the # is
 not supplied the sort will be based on the toekn. # will subtotal
 and sort by the selection amount given after the #. If a integer is not
 supplied after the # the v-totalup table is initialized with the default
 report amount to be used.

*/

   
do v-inx = 1 to v-sizer:
  if substring(p-optiontype,v-inx,1) = "," or
     substring(p-optiontype,v-inx,1) = "#" or
     substring(p-optiontype,v-inx,1) = "1" or     
     substring(p-optiontype,v-inx,1) = "2" or     
     substring(p-optiontype,v-inx,1) = "3" or     
     substring(p-optiontype,v-inx,1) = "4" or     
     substring(p-optiontype,v-inx,1) = "5" or     
     substring(p-optiontype,v-inx,1) = "6" or     
     substring(p-optiontype,v-inx,1) = "7" or     
     substring(p-optiontype,v-inx,1) = "8" or     
     substring(p-optiontype,v-inx,1) = "9" or     
     substring(p-optiontype,v-inx,1) = " " then
    next.
  if 
  not can-do("r,d,s,c,t,a,p,g,n,v,w,i,b,o,y",substring(p-optiontype,v-inx,1)) then
    display 
 "Invalid option selection valid entries are R,D,S,C,T,A,P,N,V,W,I,B,O,Y,Z -"
          substring(p-optiontype,v-inx,1).
  u-entitys = u-entitys + 1.

  if substring(p-optiontype,(v-inx + 1),1) = "#" then
    do:
    v-lookup[u-entitys] = substring(p-optiontype,v-inx,1).
    v-summup[u-entitys] = "#".
    if can-do
       ("1,2,3,4,5,6,7,8,9",substring(p-optiontype,(v-inx + 2),1)) then
      do:
      v-subtotalup[u-entitys] = int(substring(p-optiontype,(v-inx + 2),1)).
      end.

    end.
  else
    v-lookup[u-entitys] = substring(p-optiontype,v-inx,1).
  
end.

/*
Get ascending > and descending < options
*/

x-entitys = 0.
v-sizer = length(p-sorttype).
assign substring(p-sorttype,(v-sizer + 1),1) = " ".
do v-inx = 1 to v-sizer:
  if substring(p-sorttype,v-inx,1) = "," or
     substring(p-sorttype,v-inx,1) = " " then
    next.
  if not can-do(">,<",substring(p-sorttype,v-inx,1)) then
    display "Invalid sort selection valid entries are <,>"
                  substring(p-sorttype,v-inx,1).
  x-entitys = x-entitys + 1.
  v-sortup[x-entitys] = substring(p-sorttype,v-inx,1).
end.

/*
 Get total option (P)age Break, (T)otal Print, (S)ort don't print
 and (L)ine advance
*/

x-entitys = 0.
v-sizer = length(p-totaltype).
assign substring(p-totaltype,(v-sizer + 1),1) = " ". 
do v-inx = 1 to v-sizer:
  if substring(p-totaltype,v-inx,1) = "," or
     substring(p-totaltype,v-inx,1) = " " then
    next.
  if not can-do("t,p,s,l",substring(p-totaltype,v-inx,1)) then
    display "Invalid totaling selection valid entries are T,P,S,L -"
                     substring(p-optiontype,v-inx,1).
  x-entitys = x-entitys + 1.
  v-totalup[x-entitys] = substring(p-totaltype,v-inx,1).
end.              




/*
Get summary count values
*/

x-entitys = 0.
v-sizer = length(p-summcounts).
v-inx2 = 0.
do v-inx = 1 to v-sizer:
if substring(p-summcounts,v-inx,1) = "," then
  do:
  v-inx2 = v-inx2 + 1.
  end.

end.  
if v-inx2 > 0 then
  v-inx2 = v-inx2 + 1.
else
if p-summcounts <> "" then
  v-inx2 = 1.
   
do v-inx = 1 to v-inx2: 
 v-token = entry(v-inx,p-summcounts,",").
 v-token = replace(v-token,"0",""). 
 v-token = replace(v-token,"1",""). 
 v-token = replace(v-token,"2",""). 
 v-token = replace(v-token,"3",""). 
 v-token = replace(v-token,"4",""). 
 v-token = replace(v-token,"5",""). 
 v-token = replace(v-token,"6",""). 
 v-token = replace(v-token,"7",""). 
 v-token = replace(v-token,"8",""). 
 v-token = replace(v-token,"9",""). 
 if v-token = "" then
   do:
   v-summcounts[v-inx] = entry(v-inx,p-summcounts,",").
   end.
 else
  v-summcounts[v-inx] = "ALL   ".
end.



if p-detail = "d" then
  assign u-entitys = u-entitys + 1
         v-lookup[u-entitys] = "x".



assign zzl_begcat =  p-catbeg
       zzl_endcat =  p-catend
       zzl_begcust = p-custbeg
       zzl_endcust = p-custend
       zzl_begvname  = p-vidbeg
       zzl_endvname  = p-vidend
       zzl_begvpid  = p-vpbeg
       zzl_endvpid  = p-vpend
       zzl_begrep  = p-repbeg
       zzl_endrep  = p-repend.

assign zel_begcat =  p-catbeg
       zel_endcat =  p-catend
       zel_begcust = p-custbeg
       zel_endcust = p-custend
       zel_begvname  = p-vidbeg
       zel_endvname  = p-vidend
       zel_begvpid  = p-vpbeg
       zel_endvpid  = p-vpend
       zel_begrep  = p-repbeg
       zel_endrep  = p-repend.


if p-list then
  do:
  assign zel_begcat =  p-catbeg
         zel_endcat =  p-catend
         zel_begcust = p-custbeg
         zel_endcust = p-custend
         zel_begvname  = p-vidbeg
         zel_endvname  = p-vidend
         zel_begvpid   = p-vpbeg
         zel_endvpid  = p-vpend
         zel_begrep  = p-repbeg
         zel_endrep  = p-repend.
          

  {zsapboyload.i}

  assign zzl_begcat =  zel_begcat 
         zzl_endcat =  zel_endcat
         zzl_begcust = zel_begcust
         zzl_endcust = zel_endcust
         zzl_begvname  = zel_begvname
         zzl_endvname  = zel_endvname
         zzl_begvpid  = zel_begvpid
         zzl_endvpid  = zel_endvpid 
         zzl_begrep  = zel_begrep 
         zzl_endrep  = zel_endrep.
 

/*
   message zel_begvname  zzl_begvname p-vidbeg
       zel_endvname  zzl_endvname p-vidend. pause.
       
*/
  end.

  

assign zelection_matrix [1] = (if zelection_matrix[1] > 1 then /* Rep   */
                                 zelection_matrix[1]
                              else   
                                 1)
       zelection_matrix [2] = (if zelection_matrix[2] > 1 then  /* Cust */
                                 zelection_matrix[2]
                              else   
                                 1)
       zelection_matrix [3] = (if zelection_matrix[3] > 1 then  /* Cat  */
                                 zelection_matrix[3]
                              else   
                                 1)
       zelection_matrix [8] = (if zelection_matrix[8] > 1 then  /* VName */
                                 zelection_matrix[8]
                              else   
                                 1)
       zelection_matrix [9] = (if zelection_matrix[9] > 1 then  /* VPArent */
                                 zelection_matrix[9]
                              else   
                                 1).
                                 
                    


put control
    "~033~105~033~046~154~061~117~033~046~153~062~123~033~046~154~060~105".



/* calc curr month from input as of date  */
assign e-yr = int(substr(string(as-per,"9999"),3,2))
       e-mo = int(substr(string(as-per,"9999"),1,2)).
                                        
assign m-lbl[1] = n-mth[e-mo].




yrz[2] = e-yr.

if e-yr > 0 then
 yrz[1] = e-yr - 1.
else
 yrz[1] = 99. 

if e-mo ge 3 then
  assign a-year[1] = yrz[2]
         a-year[2] = yrz[2]
         a-month[1] = e-mo - 1
         a-month[2] = e-mo - 2
         m-lbl[2]   = n-mth[a-month[1]]
         m-lbl[3]   = n-mth[a-month[2]].
else         
if e-mo = 2  then
  assign a-year[1] = yrz[2]
         a-year[2] = yrz[1]
         a-month[1] = 1
         a-month[2] = 12
         m-lbl[2]   = n-mth[a-month[1]]
         m-lbl[3]   = n-mth[a-month[2]].
else         
if e-mo = 1  then
  assign a-year[1] = yrz[1]
         a-year[2] = yrz[1]
         a-month[1] = 12
         a-month[2] = 11
         m-lbl[2]   = n-mth[a-month[1]]
         m-lbl[3]   = n-mth[a-month[2]].



run set_export_headers.







zzx = 0.

def var theyear as i no-undo.

if a-year[2] < 50 then
  theyear = a-year[2] + 2000.
else
  theyear = a-year[2] + 1900.
theyear = theyear - 1.
forbegdate = date (1,1,theyear).
if jrk_optvalue[11] = "no " then
  run removeobsolete.

if jrk_optvalue[13] = "yes" then
   run backlog.


do v-peckinx = 1 to 10:
  find whses use-index k-zwhse
       where whses.zwhse = v-peckinglist[v-peckinx] no-lock no-error.
  if not avail whses then
    do:
    create whses.
    zzx = zzx + 1.
    assign whses.zcnt = zzx
           whses.zwhse = v-peckinglist[v-peckinx].
    end.       
end.

for each icsd where icsd.cono = g-cono no-lock:
 find whses use-index k-zwhse
       where whses.zwhse = icsd.whse no-lock no-error.
  if not avail whses then
    do:
    create whses.
    zzx = zzx + 1.
    assign whses.zcnt = zzx
           whses.zwhse = icsd.whse.
    end.       
end.

/* ------ End of Parameter Handling and initialization -------- */


/* ------------------------------------------------------------------------
   Main Logic
   -----------------------------------------------------------------------*/

/* Begin Normal data extraction  */




do inz = 1 to 2:
x-yr = yrz[inz].

if sapb.optvalue[18] = "ytd" and inz = 1 then
   next.  
if p-kp  and inz = 1 then
  do:
  message "kp tbrm " today "@ "  
                   int(truncate (time / 3600,0))  ":"
                   int(truncate ((time modulo 3600) / 60 ,0)).

  run smrzk98.p.
  for each xwo where 
           xwo.whse ge p-whsebeg and
           xwo.whse le p-whseend no-lock:
                         
    assign
        zk-type = 0
        zk-cost = 0
        zk-price = 0.
      find kpet where kpet.cono = g-cono
                     and kpet.wono = xwo.wono
                     and kpet.wosuf = xwo.wosuf 
                     no-lock no-error.
      if avail kpet then do:
        for each b-oeelk use-index k-oeelk
                  where b-oeelk.cono = g-cono
                    and b-oeelk.ordertype = "w"
                    and b-oeelk.orderno = kpet.wono
                    and b-oeelk.ordersuf = kpet.wosuf
                    and b-oeelk.lineno  = 0
                    and b-oeelk.specnstype <> "l"
                     no-lock:
            assign zk-cost = (b-oeelk.qtyneeded  
                            * b-oeelk.prodcost) + zk-cost.      
         end.
      end.
/*    zk-cost = kpet.prodcost.      */
      if zk-cost le 0 then
        next.
      for each oeelk  use-index k-oeelk
                 where oeelk.cono = g-cono and 
                       oeelk.ordertype = "w" and
                       oeelk.orderno = xwo.wono and
                       oeelk.ordersuf = xwo.wosuf  no-lock:
       find icsp where icsp.cono = g-cono and
                       icsp.prod = oeelk.shipprod no-lock no-error.
      
     run create_zsmsep.
     end.
  end.
 message "kp end " today "@ "  
                   int(truncate (time / 3600,0))  ":"
                   int(truncate ((time modulo 3600) / 60 ,0)).


end.


if p-bodkits and inz = 1 then
  do:
  message "bod before " today "@ "  
                 int(truncate (time / 3600,0))  ":"
                   int(truncate ((time modulo 3600) / 60 ,0)).

  assign bgdt = date(01,01,(if yrz[1] < 50 then (yrz[1] + 2000) else 
                       (yrz[1] + 1900))).
  assign z-paramdate = date((if e-mo <> 12 then
                        e-mo + 1
                      else
                        1)
                        ,1
                        ,
                       ( if yrz[2] < 50 then 
                           (yrz[2] + 2000 +
                             (if e-mo <> 12 then 0 else 1))
                          else 
                            (yrz[2] + 1900 +
                            (if e-mo <> 12 then 0 else 1)))). 
  run getenddate.
  eddt = z-paramdate. 
 /*
  message "bod " bgdt eddt. 
 */
  assign 
    zk-price = 0
    zk-type = 0
    zk-cost = 0.
  joeelkloop:
  for each oeelk  use-index  /* k-specns */ k-oeelk
                 where oeelk.cono = g-cono and 
                       oeelk.ordertype = "o" and
                       oeelk.statustype = "i" and
                       oeelk.specnstype <> "l" and
                       oeelk.whse ge p-whsebeg and
                       oeelk.whse le p-whseend
                       no-lock:
                       /*
                       break by string(oeelk.orderno,">>>>>>>>9") + 
                                string(oeelk.ordersuf,"99") +
                                string(oeelk.lineno,">>>9") :
      
    if first-of(string(oeelk.orderno,">>>>>>>>9") + 
                string(oeelk.ordersuf,"99") +
                string(oeelk.lineno,">>>9")) then */
    if oeelk.orderno <> hold-orderno or
       oeelk.lineno <> hold-lineno or
       oeelk.ordersuf <> hold-ordersuf then
      do:                        
      l-process = false.
      assign hold-orderno = oeelk.orderno
             hold-lineno = oeelk.lineno 
             hold-ordersuf = oeelk.ordersuf. 
      find oeel use-index k-oeel
              where oeel.cono = g-cono and
                    oeel.orderno = oeelk.orderno and
                    oeel.ordersuf = oeelk.ordersuf and
                    oeel.lineno = oeelk.lineno and
                    oeel.invoicedt ge bgdt and
                    oeel.invoicedt <  eddt and
                    oeel.specnstype <> "l" and
                    oeel.prodcat ge p-kcatbeg and
                    oeel.prodcat le p-kcatend
                    no-lock no-error.
       if not avail oeel or oeel.qtyship = 0 then
         next joeelkloop.
       assign z-paramdate = oeel.invoicedt.
       run getfiscfmdate.
       z-paramfisc = int(substring(string(z-paramfisc,"9999"),1,2)).
   /*    
       message "bod" z-paramfisc oeel.invoicedt.
   */    
       l-process = true.
       zk-cost = 0.
       for each b-oeelk use-index k-oeelk
                       where b-oeelk.cono = g-cono
                         and b-oeelk.ordertype = oeelk.ordertype
                         and b-oeelk.orderno = oeelk.orderno
                         and b-oeelk.ordersuf = oeelk.ordersuf
                         and b-oeelk.lineno  = oeelk.lineno
                         and b-oeelk.specnstype <> "l"
                         no-lock:
          zk-cost = ((b-oeelk.qtyneeded)   /* this does not use qty ship
                                              because it should be how much the
                                              unit each cost is */
                     * b-oeelk.prodcost) + zk-cost.      
       end.
       
       end.
    
    if avail oeel and l-process and zk-cost <> 0 then 
      run create_zsmsep_bod.
  end.
 message "bod end " today "@ "  
                int(truncate (time / 3600,0))  ":"
                   int(truncate ((time modulo 3600) / 60 ,0)).

end.


if p-vastuff and inz = 1 then
  do:
  message "va before " today "@ "  
                   int(truncate (time / 3600,0))  ":"
                   int(truncate ((time modulo 3600) / 60 ,0)).

  assign bgdt = date(01,01,(if yrz[1] < 50 then (yrz[1] + 2000) else 
                       (yrz[1] + 1900)))
         z-paramdate = date((if e-mo <> 12 then
                        e-mo + 1
                      else
                        1)
                        ,1
                        ,
                       ( if yrz[2] < 50 then 
                           (yrz[2] + 2000 +
                             (if e-mo <> 12 then 0 else 1))
                          else 
                            (yrz[2] + 1900 +
                            (if e-mo <> 12 then 0 else 1)))). 
    run getenddate.
    eddt = z-paramdate. 
   /* 
    message "va " bgdt eddt. 
   */ 
    for each vaeh where vaeh.cono = g-cono and 
                 vaeh.receiptdt <> ? and
                 vaeh.stagecd <> 9   and
                 vaeh.prodcat ge p-kcatbeg and
                 vaeh.prodcat le p-kcatend and
                 vaeh.whse ge p-whsebeg and
                 vaeh.whse le p-whseend
                 no-lock:
   if vaeh.prodcost le 0 or 
      vaeh.prodcost = ? then
     next.
     
   assign
     zk-cost  = 0
     zk-price = 0
     zk-type  = 0
     zk-found = false.

   {z-smrzk.i zk-price zk-found}.  
   if zk-found then
     if
      oeel.invoicedt ge bgdt and
      oeel.invoicedt < eddt then
     do:
     assign z-paramdate = oeel.invoicedt.
     run getfiscfmdate.
     z-paramfisc = int(substring(string(z-paramfisc,"9999"),1,2)).
     /*
     message "va " z-paramfisc oeel.invoicedt.
     */
     for each vaesl where
                       vaesl.cono = g-cono and
                       vaesl.vano = vaeh.vano and 
                       vaesl.vasuf = vaeh.vasuf and
                       can-do ("in,it",vaesl.sctntype) no-lock:
                             /* took out II sections aren't selling them
                                They are going in to inventory            */
       if vaesl.prodcost = ? then
         next.     
       run create_zsmsep_va.
     end.
     end.   
   end.             
   message "va end " today "@ "  
                   int(truncate (time / 3600,0))  ":"
                   int(truncate ((time modulo 3600) / 60 ,0)).

   
 end. 

if x-yr = 1 then
  do:
  assign v-cludged = true.
  message "cludge before " today "@ "  
                   int(truncate (time / 3600,0))  ":"
                   int(truncate ((time modulo 3600) / 60 ,0)).


  run CLUDGE_SM.
  message "cludge sdi " today "@ "  
                 int(truncate (time / 3600,0))  ":"
                   int(truncate ((time modulo 3600) / 60 ,0)).

  end.
else  
if zel_begcust <> 0 or zel_endcust < v-hicust then 
  do:
  for each smsep use-index k-smsep
              
      where    smsep.cono = g-cono and smsep.yr = x-yr
              and smsep.custno ge zel_begcust
              and smsep.custno le zel_endcust
              and smsep.whse ge p-whsebeg
              and smsep.whse le p-whseend
              and smsep.prodcat ge zel_begcat
              and smsep.prodcat le zel_endcat             
              no-lock:
    run create_zsmsep2.
  end.  
  end.
else
if zel_begcat <> "" or not(zel_endcat begins "~~~~") then 
  do:
  message "smsep before " today "@ "  
                   int(truncate (time / 3600,0))  ":"
                   int(truncate ((time modulo 3600) / 60 ,0)).

  for each smsep use-index k-prodcat
              
      where    smsep.cono = g-cono and smsep.yr = x-yr
              and smsep.prodcat ge zel_begcat 
              and smsep.prodcat le zel_endcat             
              and smsep.whse ge p-whsebeg
              and smsep.whse le p-whseend
              and smsep.custno ge zel_begcust
              and smsep.custno le zel_endcust
              no-lock:
    run create_zsmsep2.
  end.
   message "smsep end " today "@ "  
                 int(truncate (time / 3600,0))  ":"
                   int(truncate ((time modulo 3600) / 60 ,0)).

  
  end.
else
  do:
  for each smsep use-index k-smsep
              
      where    smsep.cono = g-cono and smsep.yr = x-yr
              and smsep.prodcat ge zel_begcat 
              and smsep.prodcat le zel_endcat             
              and smsep.whse ge p-whsebeg
              and smsep.whse le p-whseend
              and smsep.custno ge zel_begcust
              and smsep.custno le zel_endcust
              no-lock:
    run create_zsmsep2.
  end.  
  end.
end.

buildrec:
for each zsmsep no-lock:

 if p-ignoreKP and substring(zsmsep.prodcat,5,1) = z-hash then
   next buildrec.
 if p-ignoreBOD and substring(zsmsep.prodcat,5,1) = z-atsn then
   next buildrec.
 if p-ignoreVA and substring(zsmsep.prodcat,5,1) = z-cart then
   next buildrec.
     

 find arsc where arsc.cono = g-cono and
                arsc.custno = zsmsep.custno no-lock no-error.
    
 assign vl-slsrep = if avail arsc then
                    arsc.slsrepout
                  else
                    ""
        q-zip    = if avail arsc then
                     arsc.zipcd
                    else
                     ""

        q-state  = if avail arsc then
                     arsc.state
                    else
                     "".
 if zsmsep.shipto <> "" then
   do:
   find arss where arss.cono = g-cono and
                  arss.custno = zsmsep.custno and
                  arss.shipto = zsmsep.shipto no-lock no-error.
   assign vl-slsrep = if avail arss then
                      arss.slsrepout
                    else
                      vl-slsrep
         q-zip     = if avail arss and arss.zipcd <> "" then
                      arss.zipcd
                   else if avail arss and arss.zipcd = "" then
                     q-zip
                    else
                     ""
         q-state  = if avail arss and arss.state <> "" then
                     arss.state
                   else if avail arss and arss.state = "" then
                     q-state
                    else
                     "".


   end.

 /* Sales Rep by technology  */
 

  find catmaster where catmaster.cono = 1 and 
                       catmaster.notestype = "zz" and
                       catmaster.primarykey  = "apsva" and
                       catmaster.secondarykey = 
                  (substring(zsmsep.prodcat,1,4)) no-lock
                        no-error. 
  if not avail catmaster then
     do:
     v-parentcode = "".
     v-vendorid = substring(zsmsep.prodcat,1,3).
     if substring(zsmsep.prodcat,1,1) = "a" then 
       v-technology = "Automation".
     else if substring(zsmsep.prodcat,1,1) = "f" then
       v-technology = "Fabrication". 
     else if substring(zsmsep.prodcat,1,1) = "h" then 
       v-technology = "Hydraulic".   
     else if substring(zsmsep.prodcat,1,1) = "p" then 
       v-technology =  "Pneumatic".   
     else if substring(zsmsep.prodcat,1,1) = "s" then 
       v-technology =  "Service".     
     else if substring(zsmsep.prodcat,1,1) = "f" then 
       v-technology =  "Filtration".  
     else if substring(zsmsep.prodcat,1,1) = "c" then 
       v-technology =  "Connectors".  
     else if substring(zsmsep.prodcat,1,1) = "m" then 
       v-technology =  "Mobile".      
     else if substring(zsmsep.prodcat,1,1) = "n" then 
       v-technology =  "None".        
     end.
  
  else
    assign
       v-vendorid   = catmaster.noteln[1]
       v-technology = catmaster.noteln[2]
       v-parentcode = catmaster.noteln[3].
    
/* SLS override */ 
      
        
     if p_parm then 
        do:
          assign  h_cust = string(zsmsep.custno)
                  h_slsrepin = vl-slsrep
                  h_shipto = zsmsep.shipto
                  h_tech = if v-technology = "Service" then "s"
                            else "a".
           run chk_4_entry.
           if h_techtype = " " and v-technology = "Service" then
              do:
              h_tech = "a".
              run chk_4_entry.
              end.
                   
           if h_techtype = "s" and v-technology = "service" then
             assign vl-slsrep = h_slsrepin.
           else 
           if h_techtype = "a" then
             assign vl-slsrep = h_slsrepin.
           else
              
             run xsdioetlt.p (input-output vl-slsrep,
                              input substring(zsmsep.prodcat,1,4),
                              input zsmsep.custno,
                              input zsmsep.shipto).
     
 
        
        end.
     else

/* SLS override */
 
 
 run xsdioetlt.p (input-output vl-slsrep,
                  input substring(zsmsep.prodcat,1,4),
                  input zsmsep.custno,
                  input zsmsep.shipto).
     
 find smsn where smsn.cono = g-cono and
                smsn.slsrep = vl-slsrep no-lock no-error.
 if avail smsn then
   assign v-region = substring(smsn.mgr,1,1)
          v-district = substring(smsn.mgr,2,3).
 else
   assign v-region = "z"
          v-district = "888".


 if (substring(v-technology,1,4) < p-techbeg or
     substring(v-technology,1,4) > p-techend) or
     (v-parentcode < zel_begvpid or
      v-parentcode > zel_endvpid) or
     (v-vendorid < zel_begvname or
      v-vendorid > zel_endvname)  then

    
    next.

/* End Normal data extraction  */


/* Beginning of Token Reporting Logic  */

 assign  v-mystery = ""
         v-sortmystery = "".
 do v-inx = 1 to u-entitys: 
   if v-lookup[v-inx] <> "" then
     do:
     if substring(v-summup[v-inx],1,1) = "#" then
       do:
       assign v-stotcnt = v-stotcnt + 1.
       end.
     /* Above just count the #'s                 */
     
     if v-lookup[v-inx] = "w" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
       assign v-token =
                       caps(string(zsmsep.whse,"x(4)")).
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(4)").
       end.
     else
     if v-lookup[v-inx] = "o" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
       assign v-token = if avail arss and zsmsep.shipto <> "" then
                          caps(string(arss.countrycd,"x(2)"))
                        else  
                          caps(string(arsc.countrycd,"x(2)")).
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(4)").
       end.
     else
     if v-lookup[v-inx] = "y" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
       assign v-token = if avail arss and zsmsep.shipto <> "" then
                          caps(string(arss.city,"x(20)"))
                        else  
                          caps(string(arsc.city,"x(20)")).
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(4)").
       end.
     else   
     if v-lookup[v-inx] = "a" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
       assign v-token =
                       caps(string(q-state,"x(2)")).
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" and
          v-summup[v-inx] <> "#" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(2)").
       end.
     else   
     if v-lookup[v-inx] = "z" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
       assign v-token =
                       caps(string(q-zip,"x(10)")).
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" and
          v-summup[v-inx] <> "#" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(10)").
       end.
     else 

     if v-lookup[v-inx] = "g" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
       assign v-token =
                     caps(string(substring (v-technology,1,4),"x(4)")).
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" and
          v-summup[v-inx] <> "#" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                      string(v-token,"x(4)").
       end.
     else 

     if v-lookup[v-inx] = "n" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
       assign v-token =
                      caps( string(v-parentcode,"x(15)")).
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" and
          v-summup[v-inx] <> "#" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(15)").
       end.
     else 

     if v-lookup[v-inx] = "v" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
       assign v-token =
                        caps(string(v-vendorid,"x(15)")).
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" and
          v-summup[v-inx] <> "#" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(15)").
       end.
     else 

     if v-lookup[v-inx] = "p" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
       assign v-token =
                      caps( string(substring(zsmsep.prodcat,1,4),"x(4)")).
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" and
          v-summup[v-inx] <> "#" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(4)").
       end.
     else 
     if v-lookup[v-inx] = "b" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
       assign v-token =
                       caps(string(zsmsep.prodcat,"x(5)")).
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" and
          v-summup[v-inx] <> "#" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(5)").
       end.
     else 
     if v-lookup[v-inx] = "i" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
       assign v-token = if avail arsc then
                          caps(string(arsc.ediyouraddr,"x(30)"))
                        else
                          string("                             a","x(30)").
        v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" and
          v-summup[v-inx] <> "#" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(30)").
       end.
     else 
     if v-lookup[v-inx] = "x" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
         v-token =   string(
                           string(zsmsep.custno,"999999999999") +
                           string(zsmsep.shipto,"x(8)") +
                           string(zsmsep.prodcat,"x(5)") +
                           string(zsmsep.whse,"x(4)") +
                           string("x","x(1)")).
                           
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       v-token.
       end.
     else 
     if v-lookup[v-inx] = "r" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
      
       v-token = caps(string( string(v-region,"x(1)"))).
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" then
         do:
         run ascii_make_it_not (input-output v-token).
         end.
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(1)").
       end.
     else 
     if v-lookup[v-inx] = "d" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
      
       v-token = caps(string( string(v-region,"x(1)") +
                              string(v-district,"x(3)"))).
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(4)").
       end.
     else 
     if v-lookup[v-inx] = "s" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
      
       v-token = caps(string( string(vl-slsrep,"x(4)"))).
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(4)").
       end.
     else
     if v-lookup[v-inx] = "c" then
       do:
       if v-inx > 1 then                   
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
      
       v-token = string( string(zsmsep.custno,"999999999999")).
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(12)").
       end.            
     else 
     if v-lookup[v-inx] = "t" then
       do:
       if v-inx > 1 then
         assign v-mystery = v-mystery + v-delim
                v-sortmystery = v-sortmystery + v-delim.
      
       v-token =  if avail arss then
                     caps(string( string(zsmsep.shipto,"x(15)"))) 
                   else  
                     string("               ","x(15)").
       v-mystery = v-mystery + v-token.
       if v-sortup[v-inx] = "<" then
         run ascii_make_it_not (input-output v-token).
       v-sortmystery = v-sortmystery +
                       string(v-token,"x(15)").
       end.
     end.
  end.

  do zzx = 1 to 7:
     assign t-amounts[zzx] = 0
            t-amountc[zzx] = 0.
  end.          
      

  
  /* PTD */
  if zsmsep.yr = yrz[2] then
    do:
    assign v-val = if p-costonly then
                     zsmsep.cogamt[e-mo]
                   else
                     zsmsep.salesamt[e-mo].
    assign
     t-amounts[1] = v-val
     t-amountc[1] = v-val - zsmsep.cogamt[e-mo].
    end.
   /* PTD 2 */
  if zsmsep.yr = a-year[1] then
    do:
    assign v-val = if p-costonly then
                     zsmsep.cogamt[a-month[1]]
                   else
                     zsmsep.salesamt[a-month[1]].
    assign
     t-amounts[2] = v-val
     t-amountc[2] = v-val -
                    zsmsep.cogamt[a-month[1]].
    end.
                                             
  /* PTD 3 */
  if zsmsep.yr = a-year[2] then
    do:
    assign v-val = if p-costonly then
                     zsmsep.cogamt[a-month[2]]
                   else
                     zsmsep.salesamt[a-month[2]].
   
    assign
     t-amounts[3] = v-val
     t-amountc[3] = v-val -
                    zsmsep.cogamt[a-month[2]].
    end.
    /* YTD */
  if zsmsep.yr = yrz[2] then
    do:
    do v-inx2 = 1 to e-mo:
      assign v-val = if p-costonly then
                       zsmsep.cogamt[v-inx2]
                     else
                       zsmsep.salesamt[v-inx2].
      assign
        t-amounts[4] = v-val + t-amounts[4]
        t-amountc[4] = (v-val -
                       zsmsep.cogamt[v-inx2])  + t-amountc[4].
    end.
  end.
  /* Last YTD */
  if zsmsep.yr = yrz[1] then
    do:
    do v-inx2 = 1 to e-mo:
      assign v-val = if p-costonly then
                       zsmsep.cogamt[v-inx2]
                     else
                       zsmsep.salesamt[v-inx2].
       assign
        t-amounts[5] = v-val + t-amounts[5]
        t-amountc[5] = (v-val -
                         zsmsep.cogamt[v-inx2]) + t-amountc[5].
    end.
  end.
  /* Last YTD total */
  if zsmsep.yr = yrz[1] and p-exportl then
    do:
    do v-inx2 = 1 to 12:
      assign v-val = if p-costonly then
                       zsmsep.cogamt[v-inx2]
                     else
                       zsmsep.salesamt[v-inx2].
       assign
        t-amounts[6] = v-val + t-amounts[6]
        t-amountc[6] = (v-val -
                         zsmsep.cogamt[v-inx2]) + t-amountc[6].
    end.
  end.
  /* BackLog */
  if zsmsep.yr = yrz[2] and p-exportl then
    do:
      assign v-val = if p-costonly then
                       zsmsep.cogamt[13]
                     else
                       zsmsep.salesamt[13].
       assign
        t-amounts[7] = v-val + t-amounts[7]
        t-amountc[7] = (v-val -
                         zsmsep.cogamt[13]) + t-amountc[7].
    end.



  if t-amounts[1] = 0 and t-amountc[1] = 0 and
     t-amounts[2] = 0 and t-amountc[2] = 0 and
     t-amounts[3] = 0 and t-amountc[3] = 0 and
     t-amounts[4] = 0 and t-amountc[4] = 0 and
     t-amounts[5] = 0 and t-amountc[5] = 0 and
     t-amounts[6] = 0 and t-amountc[6] = 0 and       
     t-amounts[7] = 0 and t-amountc[7] = 0 then       

   next.
/* Don't create a record if there is not anything to report  */

  assign v-sortmystery = v-sortmystery + v-delim.
 
  find drpt where drpt.stotmystery = v-sortmystery no-lock no-error.
  
  if not avail drpt then
    do:
    create drpt.
    assign drpt.mystery = v-mystery
           drpt.sortmystery = v-sortmystery
           drpt.stotmystery = v-sortmystery.
    end.
/* Remember in the case of summary mystery DRPT records are becomming
   summary records therefore the only usefull info is the common informatoin
   being summarized. 
   
   That is why all numerics should be accumulative below !!
   This concept is very important to understand before continuing.

*/

    assign
       drpt.region    = v-region
       drpt.district  = v-district
       drpt.slsrepout = vl-slsrep
       drpt.slsmname  = if avail smsm then smsn.name
                        else
                           "No Name Available"
       drpt.whse      = zsmsep.whse
       drpt.custno    = zsmsep.custno
       drpt.shipto    = zsmsep.shipto
       drpt.custname  = if avail arsc then arsc.lookupnm
                        else
                          "No Name Avialable"
       drpt.pcat      = zsmsep.prodcat.
   do zzx = 1 to 5:       
     assign drpt.sell[zzx] = drpt.sell[zzx] + t-amounts[zzx] 
            drpt.cost[zzx] = drpt.cost[zzx] + t-amountc[zzx]. 
   end.
   assign drpt.sell[6] = drpt.sell[4] - drpt.sell[5]. 
          drpt.cost[6] = drpt.cost[4] - drpt.cost[5]. 
   if p-exportl then
     assign drpt.sell[7] = drpt.sell[7] + t-amounts[6] 
            drpt.cost[7] = drpt.cost[7] + t-amountc[6]
            drpt.sell[8] = drpt.sell[8] + t-amounts[7] 
            drpt.cost[8] = drpt.cost[8] + t-amountc[7]. 
             


/* End of Token Reporting Logic  */

if drpt.sell[1] = 0 and
  drpt.sell[2] = 0 and
  drpt.sell[3] = 0 and
  drpt.sell[4] = 0 and
  drpt.sell[5] = 0 and
  drpt.sell[6] = 0 and
  drpt.sell[7] = 0 and
  drpt.sell[8] = 0 and
  drpt.cost[1] = 0 and
  drpt.cost[2] = 0 and
  drpt.cost[3] = 0 and
  drpt.cost[4] = 0 and
  drpt.cost[5] = 0 and
  drpt.cost[6] = 0 and
  drpt.cost[7] = 0 and
  drpt.cost[8] = 0 then
  delete drpt.  /* if we get reversing records that yeild 0 then delete */
end.


/* Beginning '#' subtotal sort preparation  */

if v-stotcnt <> 0 then
  do:
  assign v-holdmystery = "xxxxxxxxxxxxxx"
         v-holdsortmystery = "xxxxxxxxxxxxxx".

  
  
  for each drpt use-index dinx2 : 
              
     if v-holdsortmystery = "xxxxxxxxxxxxxx" then
        v-holdsortmystery = drpt.sortmystery.

     if v-holdmystery = "xxxxxxxxxxxxxx" then
        v-holdmystery = drpt.mystery.
     if v-holdsortmystery <> drpt.sortmystery then
       do:
       assign v-sortmystery = drpt.sortmystery
              v-mystery = drpt.mystery.
       run check_the_subtotals.
       assign v-holdmystery = drpt.mystery
              v-holdsortmystery = drpt.sortmystery.

       end. 
     do v-inx = 1 to (u-entitys + 1):
       assign 
         t-amounts1[v-inx] = t-amounts1[v-inx] + drpt.sell[1]
         t-amounts2[v-inx] = t-amounts2[v-inx] + drpt.cost[1]
         t-amounts3[v-inx] = t-amounts3[v-inx] + drpt.sell[2]
         t-amounts4[v-inx] = t-amounts4[v-inx] + drpt.cost[2]
         t-amounts5[v-inx] = t-amounts5[v-inx] + drpt.sell[3]
         t-amounts6[v-inx] = t-amounts6[v-inx] + drpt.cost[3]
         t-amounts7[v-inx] = t-amounts7[v-inx] + drpt.sell[4]
         t-amounts8[v-inx] = t-amounts8[v-inx] + drpt.cost[4]
         t-amounts9[v-inx] = t-amounts9[v-inx] + drpt.sell[5]
         t-amounts10[v-inx] = t-amounts10[v-inx] + drpt.cost[5]
         t-amounts11[v-inx] = t-amounts11[v-inx] + drpt.sell[6]
         t-amounts12[v-inx] = t-amounts12[v-inx] + drpt.cost[6]
         t-amounts13[v-inx] = t-amounts13[v-inx] + drpt.sell[7]
         t-amounts14[v-inx] = t-amounts14[v-inx] + drpt.cost[7]
         t-amounts15[v-inx] = t-amounts15[v-inx] + drpt.sell[8]
         t-amounts16[v-inx] = t-amounts16[v-inx] + drpt.cost[8].
      end.
   end.
  v-mystery = "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim.
  v-sortmystery = "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim.
  run check_the_subtotals.
end.


   do v-inx = 1 to (u-entitys + 1):
     assign 
       t-amounts1[v-inx] = 0
       t-amounts2[v-inx] = 0
       t-amounts3[v-inx] = 0
       t-amounts4[v-inx] = 0
       t-amounts5[v-inx] = 0
       t-amounts6[v-inx] = 0
       t-amounts7[v-inx] = 0
       t-amounts8[v-inx] = 0
       t-amounts9[v-inx] = 0
       t-amounts10[v-inx] = 0
       t-amounts11[v-inx] = 0
       t-amounts12[v-inx] = 0
       t-amounts13[v-inx] = 0
       t-amounts14[v-inx] = 0
       t-amounts15[v-inx] = 0
       t-amounts16[v-inx] = 0.

    end.



/* End '#' subtotal sort preparation  */

/* Beginning of Summary Count sort preparation  */

assign v-inx = 0
       v-cycles = 0.
do v-inx4 = 1 to 10:
  if v-summcounts[v-inx4] begins "a" or
     v-summcounts[v-inx4] = " " then
    next.
  else  
     v-inx = v-inx + 1.
end.



if v-inx = 0 then
  for each drpt use-index dinx:
    assign drpt.sortmystery = drpt.stotmystery.
  end.  

if v-inx <> 0 then   
  do v-inx4 = 1 to 10:
    if v-summcounts[v-inx4] begins "a" or
       v-summcounts[v-inx4] = " " then
      next.
    if v-cycles > 0 then
      for each drpt use-index dinx:
        assign drpt.stotmystery = drpt.sortmystery.
      end.  
    assign v-cycles = v-cycles + 1.
    
    
    assign v-holdmystery = "xxxxxxxxxxxxxx"
           v-holdsortmystery = "xxxxxxxxxxxxxx".

  
    assign summ-count = 0.
    
    finder:
    
    for each drpt use-index dinx : 
              
      if v-holdsortmystery = "xxxxxxxxxxxxxx" then
         assign summ-count = 1
                v-holdsortmystery = drpt.stotmystery.

      if v-holdmystery = "xxxxxxxxxxxxxx" then
         v-holdmystery = drpt.mystery.
       
       

      if v-holdsortmystery = drpt.stotmystery then
         do:
         summ-count = summ-count.
         end.
      else   
      if v-holdsortmystery <> drpt.stotmystery then
        do:
        assign v-sortmystery = drpt.stotmystery
               v-mystery = drpt.mystery.
        v-changecnt = 0.
        do v-inx3 = 1 to v-inx4:
          if entry(v-inx3,v-holdsortmystery,v-delim) <>
             entry(v-inx3,v-sortmystery,v-delim) then
           do:
           v-changecnt = 1.
           if v-inx3 < v-inx4 then
             do:
             assign v-holdmystery = drpt.mystery
                    v-holdsortmystery = drpt.stotmystery.
             assign drpt.sortmystery = drpt.stotmystery.
             assign summ-count = 1.
             next finder.
             end.
           end.           
        end.
        if v-changecnt = 0 then
          do:
          assign summ-count = summ-count.
          end.
        else          
          assign summ-count = summ-count + 1.   
      end.
      assign v-trimmystery = "".
      if summ-count le int(v-summcounts[v-inx4]) then
        do:
        assign drpt.sortmystery = drpt.stotmystery
               v-holdmystery = drpt.mystery
               v-holdsortmystery = drpt.stotmystery.
        next finder.
        end.
      else  
      if summ-count > int(v-summcounts[v-inx4]) then
        do:                                              
        do v-inx = 1 to u-entitys:
          v-token = entry(v-inx,v-holdsortmystery,v-delim).
          v-sizer = length(v-token).
          if v-inx = v-inx4 then
            do:
            v-token = fill("~~",v-sizer).
            end.
          else
            v-token = entry(v-inx,v-holdsortmystery,v-delim).

          if v-inx > 1 then
            assign v-trimmystery = 
              v-trimmystery +
              v-delim . 
          assign v-trimmystery = v-trimmystery +
                 v-token.
        end.           
      assign drpt.sortmystery = v-trimmystery + v-delim
             v-holdmystery = drpt.mystery
                  v-holdsortmystery = drpt.stotmystery.

      
      end.
    end.
   end.

  v-mystery = "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim.
  v-sortmystery = "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim +
              "`" + v-delim.


/* End Summary Count sort preparation  */



/* Beginning of reporting Engine
   This is where the tokens choosen are subtotaled and reported on
   this logic never needs to be changed for specific reporting,
   unless you have a detail option then the detail frame would be
   inside.

*/
assign v-holdmystery = "xxxxxxxxxxxxxx"
       v-holdsortmystery = "xxxxxxxxxxxxxx".

if not p-exportl then
  do:
  hide frame f-trn1.
  hide frame f-trn2.
  hide frame f-trn3.
  hide frame f-trn4.
  hide frame f-trn5.
  end.


if not p-exportl  then
  do:
  page.
  view frame f-trn1.
  view frame f-trn2.
  view frame f-trn3.
  view frame f-trn4.
  view frame f-trn5.
  end.

for each drpt no-lock use-index dinx2
              break by drpt.sortmystery:

              
              
   if v-holdsortmystery = "xxxxxxxxxxxxxx" then
      v-holdsortmystery = drpt.sortmystery.

   if v-holdmystery = "xxxxxxxxxxxxxx" then
      v-holdmystery = drpt.mystery.
   assign founddata = true.

   if v-holdsortmystery <> drpt.sortmystery then
     do:
     assign v-sortmystery = drpt.sortmystery
            v-mystery = drpt.mystery.
     run check_the_breaks.
     assign v-holdmystery = drpt.mystery
            v-holdsortmystery = drpt.sortmystery.

     end. 
   do v-inx = 1 to (u-entitys + 1):
     assign
       t-amounts1[v-inx] = t-amounts1[v-inx] + drpt.sell[1]
       t-amounts2[v-inx] = t-amounts2[v-inx] + drpt.cost[1]
       t-amounts3[v-inx] = t-amounts3[v-inx] + drpt.sell[2]
       t-amounts4[v-inx] = t-amounts4[v-inx] + drpt.cost[2]
       t-amounts5[v-inx] = t-amounts5[v-inx] + drpt.sell[3]
       t-amounts6[v-inx] = t-amounts6[v-inx] + drpt.cost[3]
       t-amounts7[v-inx] = t-amounts7[v-inx] + drpt.sell[4]
       t-amounts8[v-inx] = t-amounts8[v-inx] + drpt.cost[4]
       t-amounts9[v-inx] = t-amounts9[v-inx] + drpt.sell[5]
       t-amounts10[v-inx] = t-amounts10[v-inx] + drpt.cost[5]
       t-amounts11[v-inx] = t-amounts11[v-inx] + drpt.sell[6]
       t-amounts12[v-inx] = t-amounts12[v-inx] + drpt.cost[6]
       t-amounts13[v-inx] = t-amounts13[v-inx] + drpt.sell[7]
       t-amounts14[v-inx] = t-amounts14[v-inx] + drpt.cost[7]
       t-amounts15[v-inx] = t-amounts15[v-inx] + drpt.sell[8]
       t-amounts16[v-inx] = t-amounts16[v-inx] + drpt.cost[8].


   end.
   
end.
/* Detail Frame would go here
 if not p-exportl then
  detail

*/

if not founddata then
  do:
  display "No Records Found to Process" with frame erros.
  leave.
  end.
/* invalidate hold areas for all encoumpassing control break (FINAL) */

assign v-mystery = "`" + v-delim +
            "`" + v-delim +
            "`" + v-delim +
            "`" + v-delim +
            "`" + v-delim +
            "`" + v-delim +
            "`" + v-delim +
            "`" + v-delim +
            "`" + v-delim +
            "`" + v-delim 
       v-sortmystery = "`" + v-delim +
            "`" + v-delim +
            "`" + v-delim +
            "`" + v-delim +
            "`" + v-delim +
            "`" + v-delim +
            "`" + v-delim +
            "`" + v-delim +
            "`" + v-delim +
            "`" + v-delim.
            

run check_the_breaks.
v-astrik = " ".
do v-inx3 = 1 to (u-entitys - if p-detail = "d" then 1 else 0):                
  substring(v-astrik,v-inx3,1)  = "*".
end.  
if p-detail = "d" then
  v-final = "*".
else
  v-final = " ".
/* Assign percentages    */

/* PTD */
assign v-per1 =
  (if t-amounts1[u-entitys + 1] = 0 and
      t-amounts2[u-entitys + 1] = 0 then
     0
   else if t-amounts2[u-entitys + 1] = 0 then
     100
   else if t-amounts1[u-entitys + 1] = 0 then
     -100
   else if
     ((t-amounts2[u-entitys + 1] /
       t-amounts1[u-entitys + 1]) * 100) < -999 then
        -999.9
   else if
     ((t-amounts2[u-entitys + 1] /
       t-amounts1[u-entitys + 1]) * 100) > 999 then
        999.9
    else    
     (t-amounts2[u-entitys + 1] /
      t-amounts1[u-entitys + 1]) * 100)
 /* PTD 2 */
 v-per2 =
  (if t-amounts3[u-entitys + 1] = 0 and
      t-amounts4[u-entitys + 1] = 0 then
     0
   else if t-amounts4[u-entitys + 1] = 0 then
     100
   else if t-amounts3[u-entitys + 1] = 0 then
     -100
   else if
     ((t-amounts4[u-entitys + 1] /
       t-amounts3[u-entitys + 1]) * 100) < -999 then
       -999.9
   else if
     ((t-amounts4[u-entitys + 1] /
       t-amounts3[u-entitys + 1]) * 100) > 999 then
        999.9
    else                              
     (t-amounts4[u-entitys + 1] /
      t-amounts3[u-entitys + 1]) * 100)
/* PTD 3 */
v-per3 =
  (if t-amounts5[u-entitys + 1] = 0 and
      t-amounts6[u-entitys + 1] = 0 then
     0
   else if t-amounts6[u-entitys + 1] = 0 then
     100
   else if t-amounts5[u-entitys + 1] = 0 then
     -100
   else if
     ((t-amounts6[u-entitys + 1] /
       t-amounts5[u-entitys + 1]) * 100) < -999 then
       -999.9
   else if
     ((t-amounts6[u-entitys + 1] /
       t-amounts5[u-entitys + 1]) * 100) > 999 then
        999.9
    else    
     (t-amounts6[u-entitys + 1] /
      t-amounts5[u-entitys + 1]) * 100).
/* YTD */
assign v-per4 = 
  (if t-amounts7[u-entitys + 1] = 0 and
      t-amounts8[u-entitys + 1] = 0 then
     0
   else if t-amounts8[u-entitys + 1] = 0 then
     100
   else if t-amounts7[u-entitys + 1] = 0 then
     -100
   else if
     ((t-amounts8[u-entitys + 1] /
       t-amounts7[u-entitys + 1]) * 100) < -999 then
        -999.9
   else if
     ((t-amounts8[u-entitys + 1] /
       t-amounts7[u-entitys + 1]) * 100) > 999 then
        999.9
    else    
     (t-amounts8[u-entitys + 1] /
      t-amounts7[u-entitys + 1]) * 100)
/* LYTD */  
v-per5 =
  (if t-amounts9[u-entitys + 1] = 0 and
      t-amounts10[u-entitys + 1] = 0 then
     0
   else if t-amounts10[u-entitys + 1] = 0 then
     100
   else if t-amounts9[u-entitys + 1] = 0 then
     -100
   else if
     ((t-amounts10[u-entitys + 1] /
       t-amounts9[u-entitys + 1]) * 100) < -999 then
        -999.9
   else if
     ((t-amounts10[u-entitys + 1] /
       t-amounts9[u-entitys + 1]) * 100) > 999 then
        999.9
    else    
     (t-amounts10[u-entitys + 1] /
      t-amounts9[u-entitys + 1]) * 100)
v-per6 =      
  (if t-amounts11[u-entitys + 1] = 0 and
      t-amounts7[u-entitys + 1] = 0 then
     0
   else if t-amounts7[u-entitys + 1] = 0 then
     100
   else if t-amounts11[u-entitys + 1] = 0 then
     -100
   else if
     ((t-amounts11[u-entitys + 1] /
       t-amounts7[u-entitys + 1]) * 100) < -999 then
        -999.9
   else if
     ((t-amounts11[u-entitys + 1] /
       t-amounts7[u-entitys + 1]) * 100) > 999 then
        999.9
    else    
     (t-amounts11[u-entitys + 1] /
      t-amounts7[u-entitys + 1]) * 100)
v-per7 =                                                 
  (if (t-amounts8[u-entitys + 1] /
       t-amounts7[u-entitys + 1] * 100) -
      (t-amounts10[u-entitys + 1] /
       t-amounts9[u-entitys + 1] * 100) = ? then
         0
   else if     
     (t-amounts8[u-entitys + 1] /
       t-amounts7[u-entitys + 1] * 100) -
      (t-amounts10[u-entitys + 1] /
       t-amounts9[u-entitys + 1] * 100) < -9999 then
       -9999.9  
   else if     
     (t-amounts8[u-entitys + 1] /
       t-amounts7[u-entitys + 1] * 100) -
      (t-amounts10[u-entitys + 1] /
       t-amounts9[u-entitys + 1] * 100) > 9999 then
       9999.9  
   else
      (t-amounts8[u-entitys + 1] /
       t-amounts7[u-entitys + 1] * 100) -
      (t-amounts10[u-entitys + 1] /
       t-amounts9[u-entitys + 1] * 100)) 
/* L Year */  
 v-per8 =
  (if t-amounts13[u-entitys + 1] = 0 and
      t-amounts14[u-entitys + 1] = 0 then
     0
   else if t-amounts14[u-entitys + 1] = 0 then
     100
   else if t-amounts13[u-entitys + 1] = 0 then
     -100
   else if
     ((t-amounts14[u-entitys + 1] /
       t-amounts13[u-entitys + 1]) * 100) < -999 then
        -999.9
   else if
     ((t-amounts14[u-entitys + 1] /
       t-amounts13[u-entitys + 1]) * 100) > 999 then
        999.9
    else    
     (t-amounts14[u-entitys + 1] /
      t-amounts13[u-entitys + 1]) * 100)
/* Backlog */  
v-per9 =
  (if t-amounts15[u-entitys + 1] = 0 and
      t-amounts16[u-entitys + 1] = 0 then
     0
   else if t-amounts16[u-entitys + 1] = 0 then
     100
   else if t-amounts15[u-entitys + 1] = 0 then
     -100
   else if
     ((t-amounts16[u-entitys + 1] /
       t-amounts15[u-entitys + 1]) * 100) < -999 then
        -999.9
   else if
     ((t-amounts16[u-entitys + 1] /
       t-amounts15[u-entitys + 1]) * 100) > 999 then
        999.9
    else    
     (t-amounts16[u-entitys + 1] /
      t-amounts15[u-entitys + 1]) * 100)
      
   v-summary-lit2 = "Final"
   v-summary-val = " ".



/* v-summary-amount = t-amounts[u-entitys + 1]. */
if not p-exportl then
 do:
 display 
  v-final
  v-astrik
  v-summary-lit2
/* PTD */
  t-amounts1[u-entitys + 1]   @ v-amt1
  v-per1 @ v-amt2
 /* PTD 2 */
  t-amounts3[u-entitys + 1]   @ v-amt3
  v-per2 @ v-amt4
/* PTD 3 */
  t-amounts5[u-entitys + 1]   @ v-amt5
  v-per3 @ v-amt6
/* YTD */
  t-amounts7[u-entitys + 1]   @ v-amt7
  v-per4 @ v-amt8
/* LYTD */  
  t-amounts9[u-entitys + 1]   @ v-amt9
  v-per5 @ v-amt10
  t-amounts11[u-entitys + 1]   @ v-amt11
  v-per6 @ v-amt12
  v-per7 @ v-amt13
  with frame f-tot.
 down with frame f-tot.
 end.
else
  do:
      
  assign export_rec =  v-astrik.

  do v-inx4 = 1 to u-entitys:
  if v-inx4 = 1 then
    v-summary-lit2 = "Final".
  else
    v-summary-lit2 = "".
  if v-lookup[v-inx4] = "w" then
     do:
     if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  v-summary-lit2.
     
     end. 
  else
  if v-lookup[v-inx4] = "o" then
     do:
     if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  v-summary-lit2.
     
     end. 
   else
   if v-lookup[v-inx4] = "y" then
     do:
     if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  v-summary-lit2.
     
     end. 
   else
   if v-lookup[v-inx4] = "c" then
     do:
     if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + v-summary-lit2 + v-del + " ".
     
     end.
  else
  if v-lookup[v-inx4] = "t" then
    do:
    if v-inx4 > 0 then 
      export_rec = export_rec + v-del.

    assign export_rec = export_rec + v-summary-lit2.
     
    end.
  else
  if v-lookup[v-inx4] = "s" then
    do:
    if v-inx4 > 0 then 
      export_rec = export_rec + v-del.

     assign export_rec = export_rec + v-summary-lit2 + v-del + " ".
     
    end.                          
  else
  if v-lookup[v-inx4] = "b" or
     v-lookup[v-inx4] = "p" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

    assign export_rec = export_rec + v-summary-lit2 + v-del + " ".
 
    end.                          
  else
    if v-lookup[v-inx4] = "t" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + v-summary-lit2.
     end.                          
  else
  if v-lookup[v-inx4] = "d" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + v-summary-lit2.
     end.
  else
  if v-lookup[v-inx4] = "r" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + v-summary-lit2.
        
    end.
  else
  if v-lookup[v-inx4] = "v" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + v-summary-lit2.
     end.
  else
  if v-lookup[v-inx4] = "n" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + v-summary-lit2.
     end.
  else
  if v-lookup[v-inx4] = "a" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + v-summary-lit2.
     end.
  else
  if v-lookup[v-inx4] = "z" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + v-summary-lit2.
     end.
  else
  if v-lookup[v-inx4] = "g" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + v-summary-lit2.
     end.
  else
  if v-lookup[v-inx4] = "i" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + v-summary-lit2.
     end.
  
  else
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
    assign export_rec = export_rec +  v-summary-lit2.
    end.
  end.

                                              
                                              
    
   export_rec = export_rec + v-del +
/* PTD */
      string(t-amounts1[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(v-per1,"->>>9.99") + v-del +
/* PTD 2 */
      string(t-amounts3[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(v-per2,"->>>9.99") + v-del +           

/* PTD 3 */
      string(t-amounts5[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(v-per3,"->>>9.99") + v-del +

/* YTD */
      string(t-amounts7[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(v-per4,"->>>9.99") + v-del +

/* LYTD */  
      string(t-amounts9[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(v-per5,"->>>9.99") + v-del +
      string(t-amounts11[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(v-per6,"->>>9.99") + v-del +
      string(v-per7,"->>>9.99") + v-del +
      string(t-amounts13[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(v-per8,"->>>9.99") + v-del +
      string(t-amounts15[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(v-per9,"->>>9.99")  .
      
  assign export_rec = export_rec + chr(13).    
  put stream expt unformatted export_rec.
  end.



/* check_the_subtotals is only invoked for '#' tokens
   it will go through the data and make apppropriate computations.

*/

procedure check_the_subtotals:
 do v-inx = 1 to u-entitys:
   if v-lookup[v-inx] <> "x" then
     if entry(v-inx,v-holdsortmystery,v-delim) <>
        entry(v-inx,v-sortmystery,v-delim)
        then
 /* The subtotalup check is making sure that the entity is not used just for
    sorting. If it is then a comtrol break will not be taken
 */
        do:
        run determine_subtotals.
        leave.
      end.
   end.
end.
/* Determine_Subtotals goes through the tokens finding the '#' options
   and builds a partial MYSTERY field called TRIMMYSTERY. This is used
   to find all the detailed common token entrys in the table for later
   update in Trace_Subtotaling.
*/

procedure determine_subtotals:
  do v-inx2 = u-entitys to v-inx by -1:
    if substring(v-summup[v-inx2],1,1) = "#" then
      do:
      v-trimmystery = "".
      do v-inx4 = 1 to v-inx2:
        if v-inx4 > 1 then
          v-trimmystery = v-trimmystery + v-delim.
        v-trimmystery =
           v-trimmystery +
           entry(v-inx4,v-holdsortmystery,v-delim).
      end.
      v-trimmystery = v-trimmystery + v-delim.
      run trace_subtotaling.


  assign
    t-amounts1[v-inx2] = 0
    t-amounts2[v-inx2] = 0
    t-amounts3[v-inx2] = 0
    t-amounts4[v-inx2] = 0
    t-amounts5[v-inx2] = 0
    t-amounts6[v-inx2] = 0
    t-amounts7[v-inx2] = 0
    t-amounts8[v-inx2] = 0
    t-amounts9[v-inx2] = 0
    t-amounts10[v-inx2] = 0
    t-amounts11[v-inx2] = 0
    t-amounts12[v-inx2] = 0
    t-amounts13[v-inx2] = 0
    t-amounts14[v-inx2] = 0
    t-amounts15[v-inx2] = 0
    t-amounts16[v-inx2] = 0.
    end.
  end.
end.
/* Trace_Subtotaling moves trough the old token key STOTMYSTERY which is a
   copy of SORTMYSTERY, inserting the subtotal into the token. For example
   the customer break would turn E,E100,25 where the subtotal is 50.00 to
   E,E100,50.0025 if ascending if descending it will be the ascii
   oposite bit pattern.
*/

procedure trace_subtotaling:
  for each b-drpt use-index dinx2 where
                b-drpt.sortmystery begins v-trimmystery:
    v-newmystery = "".
    do v-inx4 = 1 to u-entitys:
      if v-inx4 > 1 then
        v-newmystery = v-newmystery + v-delim.
      if v-inx4 = v-inx2 then
        do:
        assign v-descend =
                (if v-sortup[v-inx4] = "<" then
                   true
                 else
                   false).
        assign  v-val =           (
          /* Here the amount    */  if v-subtotalup[v-inx4] = 1 then
         /* that is to be       */    t-amounts1[v-inx4]
         /* accumulated for the */  else
         /* subtotal sort is    */  if v-subtotalup[v-inx4] = 2 then
         /* determined by param */     t-amounts3[v-inx4]
         /* this gives power to */  else
         /* sort on any amount  */  if v-subtotalup[v-inx4] = 3 then
         /* on the report at    */     t-amounts5[v-inx4]
         /* any level.          */  else
                                    if v-subtotalup[v-inx4] = 4 then
                                      t-amounts7[v-inx4]
                                    else
                                    if v-subtotalup[v-inx4] = 5 then
                                      t-amounts9[v-inx4]
                                    else
                                    if v-subtotalup[v-inx4] = 6 then
                                      t-amounts11[v-inx4]
                                    else
                                    if v-subtotalup[v-inx4] = 7 then
                                      t-amounts13[v-inx4]
                                    else
                                    if v-subtotalup[v-inx4] = 8 then
                                      t-amounts15[v-inx4]
                                    else
                                      t-amounts7[v-inx4]).                     
        assign v-token = "".
        run ascii_make_it_number(input v-descend,
                                 input v-val,
                                 input-output v-token).
        v-newmystery = v-newmystery + v-token + 
                   entry(v-inx4,b-drpt.stotmystery,v-delim).
        end.
      else
        v-newmystery = v-newmystery + entry(v-inx4,b-drpt.stotmystery,v-delim).
    end.    
  b-drpt.stotmystery = v-newmystery + v-delim.
  end.
end.

/* Check_The_Breaks checks token string for breaks
*/
procedure check_the_breaks:
 if p-detail = "d" then
   firstbreak = true.
 do v-inx = 1 to u-entitys:
   if v-lookup[v-inx] <> "x" then
     if entry(v-inx,v-holdsortmystery,v-delim) <>
        entry(v-inx,v-sortmystery,v-delim)
        then
 /* The totalup check is making sure that the entity is not used just for
    sorting. If it is then a comtrol break will not be taken
 */
        do:
        if not p-exportl then
          run print_breaks.
        else
          run Export_breaks.
        leave.
        end.
 end.
end.
/* Print_Breaks for the most part checks what the original token in MYSTERY
   and does any special processing before performing Summary_Print
*/

                           
procedure print_breaks:
  do v-inx2 = u-entitys to v-inx by -1:
    h-lookup = entry(v-inx2,v-holdmystery,v-delim).
    if v-lookup[v-inx2] = "r" then
      run summary_print.
    else  
    if v-lookup[v-inx2] = "d" then
      run summary_print.
    else  
    if v-lookup[v-inx2] = "z" then
      run summary_print.
    else  
    if v-lookup[v-inx2] = "s" then
      run summary_print.
    else                       
    if v-lookup[v-inx2] = "c" then
      run summary_print.
    else                       
    if v-lookup[v-inx2] = "t" then
      run summary_print.
    else                       
    if v-lookup[v-inx2] = "a" then
      run summary_print.
    else
    if v-lookup[v-inx2] = "p" then
      run summary_print.
    else
    if v-lookup[v-inx2] = "g" then
      run summary_print.
    else
    if v-lookup[v-inx2] = "n" then
      run summary_print.
    else
    if v-lookup[v-inx2] = "v" then
      run summary_print.
    else
    if v-lookup[v-inx2] = "w" then
      run summary_print.
    else
    if v-lookup[v-inx2] = "o" then
      run summary_print.
    else
    if v-lookup[v-inx2] = "y" then
      run summary_print.
    else
    if v-lookup[v-inx2] = "i" then
      run summary_print.
    else
    if v-lookup[v-inx2] = "b" then
      run summary_print.
  

  
  end.
end.    
/* Summary_Print
   Formats the generic subtotal line for each given token representation. It
   also will use V-TOTALUP table which is the formatting requirements from
   the parameter SORT OPTIONS.
*/


procedure summary_print:
  h-genlit = "".
  v-summary-lit2 = "".
  if v-lookup[v-inx2] = "w" then
     do:
     if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others".
      if v-inx2 = u-entitys then
       assign v-summary-lit2 = h-lookup + " " + h-genlit.
     else  
       assign v-summary-lit2 = "Whse - " + h-lookup + " " + h-genlit.
     
     end. 
  else
  if v-lookup[v-inx2] = "o" then
     do:
     if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others".
      if v-inx2 = u-entitys then
       assign v-summary-lit2 = h-lookup + " " + h-genlit.
     else  
       assign v-summary-lit2 = "Country - " + h-lookup + " " + h-genlit.
     
     end. 
  else
  if v-lookup[v-inx2] = "y" then
     do:
     if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others".
      if v-inx2 = u-entitys then
       assign v-summary-lit2 = h-lookup + " " + h-genlit.
     else  
       assign v-summary-lit2 = "City - " + h-lookup + " " + h-genlit.
     
     end. 
  else
  if v-lookup[v-inx2] = "c" then
     do:
     find arsc where arsc.cono = g-cono and 
                     arsc.custno = dec(h-lookup) no-lock no-error.
     if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
     else
     if avail arsc then
       h-genlit = arsc.lookupnm.                      
     
     if v-inx2 = u-entitys then
       assign v-summary-lit2 = 
                             h-lookup + 
                               " "
                             + h-genlit.
     else
       assign v-summary-lit2 = /* "Customer -  " + */
                             h-lookup + " " + h-genlit.
     end.
  else
  if v-lookup[v-inx2] = "t" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
       assign v-summary-lit2 = h-lookup.
    else
     assign v-summary-lit2 = "Ship-To - " +
                            h-lookup.
    end.
  else
  if v-lookup[v-inx2] = "s" then
    do:
    find smsn where smsn.cono = 1 and
                    smsn.slsrep = h-lookup no-lock no-error.
     
    if avail smsn then
      h-genlit = smsn.name.
    else
      h-genlit = "No Name Available".
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
      assign v-summary-lit2 = h-lookup.
    else
      assign v-summary-lit2 = "SlsRep -" +
                             h-lookup + " " + h-genlit.
    end.                          
  else
  if v-lookup[v-inx2] = "b" or
     v-lookup[v-inx2] = "p" then
    do:
    if substring(h-lookup,5,1) = "~~" then
       overlay (h-lookup,5,3) = "   ".
    else
    if substring(h-lookup,5,1) = "#" then
       overlay (h-lookup,5,3) = "-KP".
    else
    if substring(h-lookup,5,1) = "^" then
       overlay (h-lookup,5,3) = "-VA".
    else
    if substring(h-lookup,5,1) = "@" then
       overlay (h-lookup,5,3) = "-BD".
      
    find sasta use-index k-sasta where sasta.cono = g-cono
         and sasta.codeiden = "C"
         and sasta.codeval = substring(h-lookup,1,4) no-lock no-error.
     assign h-lookup = h-lookup + " " +

                       (if avail sasta then sasta.descrip
                        else "No SASTA Descrip").
    
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
       assign v-summary-lit2 = h-lookup.
    else
       assign v-summary-lit2 = h-lookup + " " + h-genlit.
    end.                          
  else
    if v-lookup[v-inx2] = "t" then
    do:
    h-genlit = "". 
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
      assign v-summary-lit2 = h-lookup.
    else
      assign v-summary-lit2 = "Technology -" +
                             h-lookup + " " + h-genlit.
    end.                          
  else
  if v-lookup[v-inx2] = "d" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
       assign v-summary-lit2 = h-lookup.
    else
     assign v-summary-lit2 = "District -" + 
                              h-lookup.
    end.
  else
  if v-lookup[v-inx2] = "r" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = ""
              v-summary-lit2 = h-lookup.
    if v-inx2 = u-entitys then          
       assign v-summary-lit2 = 
                            h-lookup + " " +
                            (if h-lookup = "e" then
                               "Eastern"
                             else 
                             if h-lookup = "w" then
                               "Western"
                             else
                             if h-lookup = "s" then
                               "SouthWest"    
                             else
                             if h-lookup = "m" then
                               "Mobile OEM"
                             else 
                             if h-lookup = "f" then
                               "Filtration"
                             else
                             if h-lookup = "p" then
                               "PABCO"    
                             else
                             if h-lookup = "r" then
                               "Service"
                             else  
                             if h-lookup = "n" then
                               "North"
                             else
                               "Not Available"). 
              
    else
      assign v-summary-lit2 = "Region - " +
                            h-lookup + " " +
                            (if h-lookup = "e" then
                               "Eastern"
                             else 
                             if h-lookup = "w" then
                               "Western"
                             else
                             if h-lookup = "s" then
                               "SouthWest"    
                             else
                             if h-lookup = "m" then
                               "Mobile OEM"
                             else 
                             if h-lookup = "f" then
                               "Filtration"
                             else
                             if h-lookup = "p" then
                               "PABCO"    
                             else
                             if h-lookup = "r" then
                               "Service"
                             else  
                             if h-lookup = "n" then
                               "North"
                              
                             else
                               "Not Available").   
    end.
  else
  if v-lookup[v-inx2] = "v" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
      assign v-summary-lit2 = h-lookup.
    else
      assign v-summary-lit2 = "Vendor Id - " +
                            h-lookup.
    end.
  else
  if v-lookup[v-inx2] = "n" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
      assign v-summary-lit2 = h-lookup.
    else
      assign v-summary-lit2 = "Vnd Parent Id - " +
                            h-lookup.
    end.
  else
  if v-lookup[v-inx2] = "a" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
      assign v-summary-lit2 = h-lookup.
    else
      assign v-summary-lit2 = "State - " +
                            h-lookup.
    end.
  else
  if v-lookup[v-inx2] = "z" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
      assign v-summary-lit2 = h-lookup.
    else
      assign v-summary-lit2 = "ZipCd - " +
                            h-lookup.
    end.
  else
  if v-lookup[v-inx2] = "g" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
      assign v-summary-lit2 = h-lookup.
    else
      assign v-summary-lit2 = "Technology - " +
                            h-lookup.
    end.
  else
  if v-lookup[v-inx2] = "i" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx2 = u-entitys then
      assign v-summary-lit2 = h-lookup.
    else
      assign v-summary-lit2 = "Customer Grp - " +
                            h-lookup.
    end.
  
  else
     v-summary-lit = v-lookup[v-inx2].
  
  if firstbreak and v-totalup[v-inx2] <> "s" then
    do:
    firstbreak = false.
    end.
  
  v-astrik = " ".           /* even lowest level summary gets no '*'  */
  do v-inx3 = v-inx2 to (u-entitys - if p-detail = "d" then 1 else 1):         
    substring(v-astrik,v-inx3,1)  = "*".
  end.  
  if v-totalup[v-inx2] <> "s" then
    do:
/* assign percents  */
/* PTD */
    assign v-per1 =
      (if t-amounts1[v-inx2] = 0 and
          t-amounts2[v-inx2] = 0 then
         0
       else if t-amounts2[v-inx2] = 0 then
         100
       else if t-amounts1[v-inx2] = 0 then
         -100
       else if
         ((t-amounts2[v-inx2] /
           t-amounts1[v-inx2]) * 100) < -999 then
            -999.9
       else if
         ((t-amounts2[v-inx2] /
           t-amounts1[v-inx2]) * 100) > 999 then
            999.9
       else    
         (t-amounts2[v-inx2] /
          t-amounts1[v-inx2]) * 100).
 /* PTD 2 */
    assign v-per2 =
      (if t-amounts3[v-inx2] = 0 and
          t-amounts4[v-inx2] = 0 then
         0
       else if t-amounts4[v-inx2] = 0 then
         100
       else if t-amounts3[v-inx2] = 0 then
         -100
       else if
         ((t-amounts4[v-inx2] /
           t-amounts3[v-inx2]) * 100) < -999 then
         -999.9
       else if
         ((t-amounts4[v-inx2] /
           t-amounts3[v-inx2]) * 100) > 999 then
          999.9
        else                              
         (t-amounts4[v-inx2] /
          t-amounts3[v-inx2]) * 100).
/* PTD 3 */
    assign v-per3 = 
      (if t-amounts5[v-inx2] = 0 and
          t-amounts6[v-inx2] = 0 then
         0
       else if t-amounts6[v-inx2] = 0 then
         100
       else if t-amounts5[v-inx2] = 0 then
         -100
       else if
         ((t-amounts6[v-inx2] /
           t-amounts5[v-inx2]) * 100) < -999 then
          -999.9
       else if
         ((t-amounts6[v-inx2] /
           t-amounts5[v-inx2]) * 100) > 999 then
          999.9
        else    
         (t-amounts6[v-inx2] /
          t-amounts5[v-inx2]) * 100).
/* YTD */
    assign v-per4 =
      (if t-amounts7[v-inx2] = 0 and
          t-amounts8[v-inx2] = 0 then
         0
       else if t-amounts8[v-inx2] = 0 then
         100
       else if t-amounts7[v-inx2] = 0 then
         -100
       else if
       ((t-amounts8[v-inx2] /
         t-amounts7[v-inx2]) * 100) < -999 then
         -999.9
       else if
       ((t-amounts8[v-inx2] /
         t-amounts7[v-inx2]) * 100) > 999 then
         999.9
       else    
        (t-amounts8[v-inx2] /
         t-amounts7[v-inx2]) * 100).
/* LYTD */  
    assign v-per5 =
      (if t-amounts9[v-inx2] = 0 and
          t-amounts10[v-inx2] = 0 then
         0
       else if t-amounts10[v-inx2] = 0 then
         100
       else if t-amounts9[v-inx2] = 0 then
         -100
       else if
       ((t-amounts10[v-inx2] /
         t-amounts9[v-inx2]) * 100) < -999 then
         -999.9
       else if
       ((t-amounts10[v-inx2] /
         t-amounts9[v-inx2]) * 100) > 999 then
         999.9
       else    
         (t-amounts10[v-inx2] /
         t-amounts9[v-inx2]) * 100).
    assign v-per6 =                                                 
      (if t-amounts11[v-inx2] = 0 and
        t-amounts7[v-inx2] = 0 then
         0
       else if t-amounts7[v-inx2] = 0 then
         100
       else if t-amounts11[v-inx2] = 0 then
         -100
       else if
         ((t-amounts11[v-inx2] /
           t-amounts7[v-inx2]) * 100) < -999 then
            -999.9
       else if
         ((t-amounts11[v-inx2] /
           t-amounts7[v-inx2]) * 100) > 999 then
            999.9
       else    
        (t-amounts11[v-inx2] /
         t-amounts7[v-inx2]) * 100).
       
  assign v-per7 =                                                 
    (if (t-amounts8[v-inx2] /
         t-amounts7[v-inx2] * 100) -
        (t-amounts10[v-inx2] /
         t-amounts9[v-inx2] * 100) = ? then
           0
     else if      
       (t-amounts8[v-inx2] /
         t-amounts7[v-inx2] * 100) -
        (t-amounts10[v-inx2] /
         t-amounts9[v-inx2] * 100) < -9999 then
        -9999.9
     else if      
       (t-amounts8[v-inx2] /
         t-amounts7[v-inx2] * 100) -
        (t-amounts10[v-inx2] /
         t-amounts9[v-inx2] * 100) > 9999 then
        9999.9
     else
        (t-amounts8[v-inx2] /
         t-amounts7[v-inx2] * 100) -
        (t-amounts10[v-inx2] /
         t-amounts9[v-inx2] * 100)).
 
    
                                           
    
    
    display 
      v-final
      v-astrik
      v-summary-lit2
/* PTD */
      t-amounts1[v-inx2]   @ v-amt1
      v-per1 @ v-amt2
/* PTD 2 */
      t-amounts3[v-inx2]   @ v-amt3
      v-per2 @ v-amt4

/* PTD 3 */
      t-amounts5[v-inx2]   @ v-amt5
      v-per3 @ v-amt6

/* YTD */
      t-amounts7[v-inx2]   @ v-amt7
      v-per4 @ v-amt8

/* LYTD */  
      t-amounts9[v-inx2]   @ v-amt9
      v-per5 @ v-amt10
      t-amounts11[v-inx2]   @ v-amt11
      v-per6 @ v-amt12
      v-per7 @ v-amt13

      with frame f-tot.
      down with frame f-tot.
      end.
if v-totalup[v-inx2] = "p" then
  page.
if v-totalup[v-inx2] = "l" then
  display with frame f-skip.

  assign
    t-amounts1[v-inx2] = 0
    t-amounts2[v-inx2] = 0
    t-amounts3[v-inx2] = 0
    t-amounts4[v-inx2] = 0
    t-amounts5[v-inx2] = 0
    t-amounts6[v-inx2] = 0
    t-amounts7[v-inx2] = 0
    t-amounts8[v-inx2] = 0
    t-amounts9[v-inx2] = 0
    t-amounts10[v-inx2] = 0
    t-amounts11[v-inx2] = 0
    t-amounts12[v-inx2] = 0
    t-amounts13[v-inx2] = 0
    t-amounts14[v-inx2] = 0
    t-amounts15[v-inx2] = 0
    t-amounts16[v-inx2] = 0.


end.    


/* Print_Breaks for the most part checks what the original token in MYSTERY
   and does any special processing before performing Summary_Print
*/

                           
procedure Export_breaks:
  do v-inx2 = u-entitys to v-inx by -1:
    h-lookup = entry(v-inx2,v-holdmystery,v-delim).
    if v-lookup[v-inx2] = "r" then
      run summary_Export.
    else  
    if v-lookup[v-inx2] = "d" then
      run summary_Export.
    else  
    if v-lookup[v-inx2] = "z" then
      run summary_Export.
    else  
    if v-lookup[v-inx2] = "s" then
      run summary_Export.
    else                       
    if v-lookup[v-inx2] = "c" then
      run summary_Export.
    else                       
    if v-lookup[v-inx2] = "t" then
      run summary_Export.
    else                       
    if v-lookup[v-inx2] = "a" then
      run summary_Export.
    else
    if v-lookup[v-inx2] = "p" then
      run summary_Export.
    else
    if v-lookup[v-inx2] = "g" then
      run summary_Export.
    else
    if v-lookup[v-inx2] = "n" then
      run summary_Export.
    else
    if v-lookup[v-inx2] = "v" then
      run summary_Export.
    else
    if v-lookup[v-inx2] = "w" then
      run summary_Export.
    else
    if v-lookup[v-inx2] = "o" then
      run summary_Export.
    else
    if v-lookup[v-inx2] = "y" then
      run summary_Export.
    else
    if v-lookup[v-inx2] = "i" then
      run summary_Export.
    else
    if v-lookup[v-inx2] = "b" then
      run summary_Export.
  

  
  end.
end.    
/* Summary_Export
   Formats the generic subtotal line for each given token representation. It
   also will use V-TOTALUP table which is the formatting requirements from
   the parameter SORT OPTIONS.
*/


procedure summary_Export:
  export_rec = "".
  do v-inx4 = 1 to u-entitys: 
  h-genlit = "".
  h-lookup = entry(v-inx4,v-holdmystery,v-delim). 
  v-summary-lit2 = "".
  if v-inx4 > v-inx2  then
     do:
     if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  " ".
     if can-do("c,p,b,s",substring(v-lookup[v-inx4],1,1)) then
      assign export_rec = export_rec +  v-del + " ".
     end. 
  else
  if v-lookup[v-inx4] = "w" then
     do:
     if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others".
     if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  h-lookup.
     
     end. 
  else
  if v-lookup[v-inx4] = "o" then
     do:
     if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others".
     if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  h-lookup.
     
     end. 
   else
   if v-lookup[v-inx4] = "y" then
     do:
     if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others".
     if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  h-lookup.
     
     end. 
   else
   if v-lookup[v-inx4] = "c" then
     do:
     find arsc where arsc.cono = g-cono and 
                     arsc.custno = dec(h-lookup) no-lock no-error.
     if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
     else
     if avail arsc then
       h-genlit = arsc.lookupnm.                      
      if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + h-lookup + v-del + h-genlit.
     
     end.
  else
  if v-lookup[v-inx4] = "t" then
    do:
    if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + h-lookup.
     
    end.
  else
  if v-lookup[v-inx4] = "z" then
    do:
    if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + h-lookup.
     
    end.
  else
  if v-lookup[v-inx4] = "s" then
    do:
    find smsn where smsn.cono = 1 and
                    smsn.slsrep = h-lookup no-lock no-error.
     
    if avail smsn then
      h-genlit = smsn.name.
    else
      h-genlit = "No Name Available".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".

      if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + h-lookup + v-del + h-genlit.
     
    end.                          
  else
  if v-lookup[v-inx4] = "b" or
     v-lookup[v-inx4] = "p" then
    do:
    if substring(h-lookup,5,1) = "~~" then
       overlay (h-lookup,5,3) = "   ".
    else
    if substring(h-lookup,5,1) = "#" then
       overlay (h-lookup,5,3) = "-KP".
    else
    if substring(h-lookup,5,1) = "^" then
       overlay (h-lookup,5,3) = "-VA".
    else
    if substring(h-lookup,5,1) = "@" then
       overlay (h-lookup,5,3) = "-BD".
      
    find sasta use-index k-sasta where sasta.cono = g-cono
         and sasta.codeiden = "C"
         and sasta.codeval = substring(h-lookup,1,4) no-lock no-error.
     assign h-genlit = (if avail sasta then sasta.descrip
                        else "No SASTA Descrip").
    
    if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + h-lookup + v-del + h-genlit.
 
    end.                          
  else
    if v-lookup[v-inx4] = "t" then
    do:
    h-genlit = "". 
    if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + h-lookup.
     end.                          
  else
  if v-lookup[v-inx4] = "d" then
    do:
    if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + h-lookup.
     end.
  else
  if v-lookup[v-inx4] = "r" then
    do:
    if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = ""
              v-summary-lit2 = h-lookup.
    if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + h-lookup.
        
    end.
  else
  if v-lookup[v-inx4] = "v" then
    do:
    if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + h-lookup.
     end.
  else
  if v-lookup[v-inx4] = "n" then
    do:
    if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + h-lookup.
     end.
  else
  if v-lookup[v-inx4] = "a" then
    do:
    if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + h-lookup.
     end.
  else
  if v-lookup[v-inx4] = "g" then
    do:
    if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + h-lookup.
     end.
  else
  if v-lookup[v-inx4] = "i" then
    do:
    if entry(v-inx4,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       assign h-lookup = "All Others"
              h-genlit = "".
    if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + h-lookup.
     end.
  
  else
    do:
    if v-inx4 > 1 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + v-lookup[v-inx4].
    end.
  end.

  v-astrik = " ".           /*  lowest level summary gets no '*'  */
  do v-inx3 = v-inx2 to (u-entitys - if p-detail = "d" then 1 else 1):         
    substring(v-astrik,v-inx3,1)  = "*".
  end.  

 
  
  if v-totalup[v-inx2] <> "s" then
    do:
/* assign percents  */
/* PTD */
    assign export_rec = v-astrik + v-del + export_rec.
    assign v-per1 =
      (if t-amounts1[v-inx2] = 0 and
          t-amounts2[v-inx2] = 0 then
         0
       else if t-amounts2[v-inx2] = 0 then
         100
       else if t-amounts1[v-inx2] = 0 then
         -100
       else if
         ((t-amounts2[v-inx2] /
           t-amounts1[v-inx2]) * 100) < -999 then
            -999.9
       else if
         ((t-amounts2[v-inx2] /
           t-amounts1[v-inx2]) * 100) > 999 then
            999.9
       else    
         (t-amounts2[v-inx2] /
          t-amounts1[v-inx2]) * 100).
 /* PTD 2 */
    assign v-per2 =
      (if t-amounts3[v-inx2] = 0 and
          t-amounts4[v-inx2] = 0 then
         0
       else if t-amounts4[v-inx2] = 0 then
         100
       else if t-amounts3[v-inx2] = 0 then
         -100
       else if
         ((t-amounts4[v-inx2] /
           t-amounts3[v-inx2]) * 100) < -999 then
         -999.9
       else if
         ((t-amounts4[v-inx2] /
           t-amounts3[v-inx2]) * 100) > 999 then
          999.9
        else                              
         (t-amounts4[v-inx2] /
          t-amounts3[v-inx2]) * 100).
/* PTD 3 */
    assign v-per3 = 
      (if t-amounts5[v-inx2] = 0 and
          t-amounts6[v-inx2] = 0 then
         0
       else if t-amounts6[v-inx2] = 0 then
         100
       else if t-amounts5[v-inx2] = 0 then
         -100
       else if
         ((t-amounts6[v-inx2] /
           t-amounts5[v-inx2]) * 100) < -999 then
          -999.9
       else if
         ((t-amounts6[v-inx2] /
           t-amounts5[v-inx2]) * 100) > 999 then
          999.9
        else    
         (t-amounts6[v-inx2] /
          t-amounts5[v-inx2]) * 100).
/* YTD */
    assign v-per4 =
      (if t-amounts7[v-inx2] = 0 and
          t-amounts8[v-inx2] = 0 then
         0
       else if t-amounts8[v-inx2] = 0 then
         100
       else if t-amounts7[v-inx2] = 0 then
         -100
       else if
       ((t-amounts8[v-inx2] /
         t-amounts7[v-inx2]) * 100) < -999 then
         -999.9
       else if
       ((t-amounts8[v-inx2] /
         t-amounts7[v-inx2]) * 100) > 999 then
         999.9
       else    
        (t-amounts8[v-inx2] /
         t-amounts7[v-inx2]) * 100).
/* LYTD */  
    assign v-per5 =
      (if t-amounts9[v-inx2] = 0 and
          t-amounts10[v-inx2] = 0 then
         0
       else if t-amounts10[v-inx2] = 0 then
         100
       else if t-amounts9[v-inx2] = 0 then
         -100
       else if
       ((t-amounts10[v-inx2] /
         t-amounts9[v-inx2]) * 100) < -999 then
         -999.9
       else if
       ((t-amounts10[v-inx2] /
         t-amounts9[v-inx2]) * 100) > 999 then
         999.9
       else    
         (t-amounts10[v-inx2] /
         t-amounts9[v-inx2]) * 100).
    assign v-per6 =                                                 
      (if t-amounts11[v-inx2] = 0 and
        t-amounts7[v-inx2] = 0 then
         0
       else if t-amounts7[v-inx2] = 0 then
         100
       else if t-amounts11[v-inx2] = 0 then
         -100
       else if
         ((t-amounts11[v-inx2] /
           t-amounts7[v-inx2]) * 100) < -999 then
            -999.9
       else if
         ((t-amounts11[v-inx2] /
           t-amounts7[v-inx2]) * 100) > 999 then
            999.9
       else    
        (t-amounts11[v-inx2] /
         t-amounts7[v-inx2]) * 100).
       
  assign v-per7 =                                                 
    (if (t-amounts8[v-inx2] /
         t-amounts7[v-inx2] * 100) -
        (t-amounts10[v-inx2] /
         t-amounts9[v-inx2] * 100) = ? then
           0
     else if      
       (t-amounts8[v-inx2] /
         t-amounts7[v-inx2] * 100) -
        (t-amounts10[v-inx2] /
         t-amounts9[v-inx2] * 100) < -9999 then
        -9999.9
     else if      
       (t-amounts8[v-inx2] /
         t-amounts7[v-inx2] * 100) -
        (t-amounts10[v-inx2] /
         t-amounts9[v-inx2] * 100) > 9999 then
        9999.9
     else
        (t-amounts8[v-inx2] /
         t-amounts7[v-inx2] * 100) -
        (t-amounts10[v-inx2] /
         t-amounts9[v-inx2] * 100)).
/* LYTD */  
    assign v-per8 =
      (if t-amounts13[v-inx2] = 0 and
          t-amounts14[v-inx2] = 0 then
         0
       else if t-amounts14[v-inx2] = 0 then
         100
       else if t-amounts13[v-inx2] = 0 then
         -100
       else if
       ((t-amounts14[v-inx2] /
         t-amounts13[v-inx2]) * 100) < -999 then
         -999.9
       else if
       ((t-amounts14[v-inx2] /
         t-amounts13[v-inx2]) * 100) > 999 then
         999.9
       else    
         (t-amounts14[v-inx2] /
         t-amounts13[v-inx2]) * 100).
         
/* Backlog */  
    assign v-per9 =
      (if t-amounts15[v-inx2] = 0 and
          t-amounts16[v-inx2] = 0 then
         0
       else if t-amounts16[v-inx2] = 0 then
         100
       else if t-amounts15[v-inx2] = 0 then
         -100
       else if
       ((t-amounts16[v-inx2] /
         t-amounts15[v-inx2]) * 100) < -999 then
         -999.9
       else if
       ((t-amounts16[v-inx2] /
         t-amounts15[v-inx2]) * 100) > 999 then
         999.9
       else    
         (t-amounts16[v-inx2] /
         t-amounts15[v-inx2]) * 100).
 
    
                                           
    
   export_rec = export_rec + v-del +
/* PTD */
      string(t-amounts1[v-inx2],"->>>>>>>>9") + v-del +
      string(v-per1,"->>>9.99") + v-del +
/* PTD 2 */
      string(t-amounts3[v-inx2],"->>>>>>>>9") + v-del +
      string(v-per2,"->>>9.99") + v-del +

/* PTD 3 */
      string(t-amounts5[v-inx2],"->>>>>>>>9") + v-del +
      string(v-per3,"->>>9.99") + v-del +

/* YTD */
      string(t-amounts7[v-inx2],"->>>>>>>>9") + v-del +
      string(v-per4,"->>>9.99") + v-del +

/* LYTD */  
      string(t-amounts9[v-inx2],"->>>>>>>>9") + v-del +
      string(v-per5,"->>>9.99") + v-del +
      string(t-amounts11[v-inx2],"->>>>>>>>9") + v-del +
      string(v-per6,"->>>9.99") + v-del +
      string(v-per7,"->>>9.99") + v-del +
      string(t-amounts13[v-inx2],"->>>>>>>>9") + v-del +
      string(v-per8,"->>>9.99") + v-del + 
      string(t-amounts15[v-inx2],"->>>>>>>>9") + v-del +
      string(v-per9,"->>>9.99"). 

  assign export_rec = export_rec + chr(13).    
  put stream expt unformatted export_rec.
  end.

if v-totalup[v-inx2] = "p" 
or v-totalup[v-inx2] = "l" then
  put stream expt unformatted " " chr(13).

  assign
    t-amounts1[v-inx2] = 0
    t-amounts2[v-inx2] = 0
    t-amounts3[v-inx2] = 0
    t-amounts4[v-inx2] = 0
    t-amounts5[v-inx2] = 0
    t-amounts6[v-inx2] = 0
    t-amounts7[v-inx2] = 0
    t-amounts8[v-inx2] = 0
    t-amounts9[v-inx2] = 0
    t-amounts10[v-inx2] = 0
    t-amounts11[v-inx2] = 0
    t-amounts12[v-inx2] = 0
    t-amounts13[v-inx2] = 0
    t-amounts14[v-inx2] = 0
    t-amounts15[v-inx2] = 0
    t-amounts16[v-inx2] = 0.


end.    



/* Since the building of SORTMYSTERY is token based and also serves as a
   coalating tool in reporting These procedures build the closest thing to
   ASCII bit reversal as possible. Since this PROGRESS is configured to
   treat the upper and lower case alphas as equivalents the processes
   reflect that.
*/


/* Coalation procedures */

procedure ascii_coalation:

  create ascii_bit_swaper.
    assign
      ascii_bit_swaper.char = ""
      ascii_bit_swaper.dec  = 0
      ascii_bit_swaper.undec = 127.
  do ascii_index = 2 to 127:
/* skip lower case because for some reason they have the same colate seq */
    if  ascii_index ge 97 and ascii_index le 122 then
      next.

    create ascii_bit_swaper.
    assign
      ascii_bit_swaper.char = chr(ascii_index)
      ascii_bit_swaper.dec  = ascii_index.
  end.
    

  ascii_index = 127.
  for each ascii_bit_swaper:
    ascii_bit_swaper.undec = ascii_index.
    ascii_index = ascii_index - 1.
   /* skip lower case because for some reason they have the same colate seq */
    if ascii_index = 122 then
       assign ascii_index = 96.
    
    if ascii_index = 1 then
      assign ascii_index = 0.
  end.      
end.

procedure ascii_make_it_NOT:

 define input-output parameter asciistring as char.

 ascii_length = length(asciistring).
 do ascii_index = 1 to ascii_length:
   find ascii_bit_swaper where ascii_bit_swaper.dec = 
                               asc(caps(substring(asciistring,ascii_index,1)))
                               no-lock no-error.
   if not avail ascii_bit_swaper then
     next.
  else
     overlay(asciistring,ascii_index,1) = 
             chr(ascii_bit_swaper.undec).
   end.          
 end.  
                                    

procedure ascii_make_it_NUMBER:

 define input        parameter makeitnot as logical.
 define input        parameter d_number as decimal.
 define input-output parameter asciistring as char.

 assign asciistring =
          string(d_number,"+999999999.99").
 if d_number ge 0 then
   overlay(asciistring,1,1) = "`".
/*
   Simply put the above statement is making a positive number + a ` this is
   done because for some odd reason a + is a lower binary value than a -.
   Therefore creating a problem a ` is the highest...simple adjustment.

   Below what is happening is developing a negative numbers sequence. Since
   in a string a higher number is higher because the sign is not understood.
   I'm reversing a - numbers bits so it is the opposite of that. Think about
   it, it really does make sense

 */

 if (makeitnot and d_number < 0) or
    (not makeitnot and d_number ge 0) then
   return.
 ascii_length = length(asciistring).
 do ascii_index = 1 to ascii_length:
   find ascii_bit_swaper where ascii_bit_swaper.dec = 
                               asc(caps(substring(asciistring,ascii_index,1)))
                               no-lock no-error.
   if not avail ascii_bit_swaper then
     next.
  else
     overlay(asciistring,ascii_index,1) = 
             chr(ascii_bit_swaper.undec).
   end.          
 end.  

/* Begin Standard SMRZK procedures  */

                                    
{zsapboycheck.i}

procedure create_zsmsep:

 if   (xwo.qtyfl * oeelk.qtyneeded) *
        ((xwo.salesamt / xwo.qtyfl) *
         (oeelk.prodcost / zk-cost)) <> 0 or
          ((xwo.qtyfl * oeelk.qtyneeded) *
                     oeelk.prodcost) <> 0 then
 do:                    
           
   
  {z-smrzk2.i z-cat icsp.prodcat z-hash}

  assign pk_custno = xwo.custno
         pk_shipto = xwo.shipto
         pk_cat    = icsp.prodcat.
  run precheckparams.
  if not pk_good then
    do:
    run create_rev_zsmsep.
    leave.       
    end.
  
  find zsmsep where
                    zsmsep.cono = g-cono           and
                    zsmsep.custno = xwo.custno   and
                    zsmsep.shipto = xwo.shipto   and
                    zsmsep.prodcat = z-cat  and
                    zsmsep.yr     = xwo.yr       and
                    zsmsep.whse = xwo.whse no-lock no-error.
  if not avail zsmsep then
    do:
    create zsmsep.
    assign zsmsep.cono = g-cono
           zsmsep.custno = xwo.custno
           zsmsep.shipto = xwo.shipto
           zsmsep.prodcat = z-cat
           zsmsep.yr     = xwo.yr
           zsmsep.whse = xwo.whse.
    end.
    assign
      zsmsep.salesamt[xwo.mnth] = zsmsep.salesamt[xwo.mnth] +
        (xwo.qtyfl * oeelk.qtyneeded) *
        ((xwo.salesamt / xwo.qtyfl) *
         (oeelk.prodcost / zk-cost))
      zsmsep.cogamt[xwo.mnth] = zsmsep.cogamt[xwo.mnth] + 
         ((xwo.qtyfl * oeelk.qtyneeded) *
                     oeelk.prodcost).
    
  
run create_rev_zsmsep.
end.
end.



procedure create_rev_zsmsep:
    {z-smrzk2.i z-cat xwo.pcat z-tild}

    
    assign pk_custno = xwo.custno
           pk_shipto = xwo.shipto
           pk_cat    = xwo.pcat.
    run precheckparams.
    if not pk_good then
      do:
      leave.       
      end.
    
    find zsmsep where
                    zsmsep.cono = g-cono           and
                    zsmsep.custno = xwo.custno   and
                    zsmsep.shipto = xwo.shipto   and
                    zsmsep.prodcat = z-cat  and
                    zsmsep.yr     = xwo.yr       and
                    zsmsep.whse = xwo.whse no-error.
    if not avail zsmsep then
      do:
      create zsmsep.
      assign zsmsep.cono = g-cono
             zsmsep.custno = xwo.custno
             zsmsep.shipto = xwo.shipto
             zsmsep.prodcat = z-cat 
             zsmsep.yr     = xwo.yr
             zsmsep.whse = xwo.whse.
      end.
    /*
    message xwo.custno
            xwo.shipto
            xwo.pcat
            xwo.qtyfl
            zk-cost
            oeelk.qtyneeded
            oeelk.prodcost
            xwo.wono
            xwo.wosuf
            "~015".
    */
    assign
      zsmsep.salesamt[xwo.mnth] = zsmsep.salesamt[xwo.mnth] -
        (xwo.qtyfl * oeelk.qtyneeded) *
        ((xwo.salesamt / xwo.qtyfl) *
         (oeelk.prodcost / zk-cost))
      zsmsep.cogamt[xwo.mnth] = zsmsep.cogamt[xwo.mnth] - 
         ((xwo.qtyfl * oeelk.qtyneeded) *
                     oeelk.prodcost).
    

     
  
end.






procedure create_zsmsep2:
  {z-smrzk2.i z-cat smsep.prodcat z-tild}

  assign pk_custno = smsep.custno
         pk_shipto = smsep.shipto
         pk_cat    = smsep.prodcat.
  run precheckparams.
  if not pk_good then
    leave.       
 
  
  
  find zsmsep where 
                    zsmsep.cono = g-cono           and
                    zsmsep.custno = smsep.custno   and
                    zsmsep.shipto = smsep.shipto   and
                    zsmsep.prodcat = z-cat and
                    zsmsep.yr     = smsep.yr       and
                    zsmsep.whse = smsep.whse no-lock no-error.
  if not avail zsmsep then
    do:
    create zsmsep.
    assign zsmsep.cono = g-cono
           zsmsep.custno = smsep.custno
           zsmsep.shipto = smsep.shipto
           zsmsep.prodcat = z-cat
           zsmsep.yr     = smsep.yr
           zsmsep.whse = smsep.whse.
    end.
  do zzx = 1 to 12:
    zsmsep.salesamt[zzx] = zsmsep.salesamt[zzx] +
      (smsep.salesamt[zzx]).
    zsmsep.cogamt[zzx] = zsmsep.cogamt[zzx] + 
      (smsep.cogamt[zzx]).
                          
  end.
end.


procedure create_zsmsep_bod:
  v-rebate = 0.
  for each pder where pder.cono = oeelk.cono and
                      pder.orderno = oeelk.orderno and
                      pder.ordersuf = oeelk.ordersuf and
                      pder.lineno = oeelk.lineno and
                      pder.seqno = oeelk.seqno no-lock:
     v-rebate = v-rebate + (pder.rebateamt * pder.stkqtyship).                
  end.   

  if  ((((oeel.qtyship * oeelk.qtyneeded)  *
      (oeel.price * ((100 - oeel.discpct) / 100) * 
                 oeel.unitconv)) *
       (oeelk.prodcost / zk-cost))         
                           * (if oeel.returnfl then -1 else 1)) <> 0
      or
      (((((oeel.qtyship * oeelk.qtyneeded)  *
       oeel.prodcost) *
       (oeelk.prodcost / zk-cost))         
                           * (if oeel.returnfl then -1 else 1)) 
                           - (v-rebate * oeelk.prodcost / zk-cost)) <> 0 then
  do:                           

     
  
  
  
  
  
  
  
  {z-smrzk2.i z-cat oeelk.prodcat z-atsn}
                 
  assign pk_custno = oeel.custno
         pk_shipto = oeel.shipto
         pk_cat    = oeelk.prodcat.
  run precheckparams.
  if not pk_good then
    do:
    run create_rev_zsmsep_bod.
    leave.       
    end.
  
  find zsmsep where
                    zsmsep.cono = g-cono           and
                    zsmsep.custno = oeel.custno   and
                    zsmsep.shipto = oeel.shipto   and
                    zsmsep.prodcat = z-cat and
                    zsmsep.yr     = 
                    int(substring(string(year(oeel.invoicedt),"9999"),3,2))  and
                    zsmsep.whse = oeel.whse no-lock no-error.
  if not avail zsmsep then
    do:
    create zsmsep.
    assign zsmsep.cono = g-cono
           zsmsep.custno = oeel.custno
           zsmsep.shipto = oeel.shipto
           zsmsep.prodcat = z-cat
           zsmsep.yr     =
             int(substring(string(year(oeel.invoicedt),"9999"),3,2))  
           zsmsep.whse = oeel.whse.
    end.
   zsmsep.salesamt[z-paramfisc] =   
   zsmsep.salesamt[z-paramfisc] +
     ((((oeel.qtyship * oeelk.qtyneeded)  *
      (oeel.price * ((100 - oeel.discpct) / 100) * 
                 oeel.unitconv)) *
       (oeelk.prodcost / zk-cost))         
                           * (if oeel.returnfl then -1 else 1)).

    v-rebate = v-rebate * (oeelk.prodcost / zk-cost).

    zsmsep.cogamt[z-paramfisc] = 
      zsmsep.cogamt[z-paramfisc] + 
      (((((oeel.qtyship * oeelk.qtyneeded)  *
       oeel.prodcost) *
       (oeelk.prodcost / zk-cost))         
                           * (if oeel.returnfl then -1 else 1)) 
                           - v-rebate).

       
      
      
      
      
      
      
/*   
    if oeel.invoicedt ge 01/01/00 and v-rebate <> 0 then
    do:
    message z-cat
            bgdt
            eddt
               ((((oeel.qtyship * oeelk.qtyneeded)  *
      (oeel.price * ((100 - oeel.discpct) / 100) * 
                 oeel.unitconv)) *
       (oeelk.prodcost / zk-cost))         
                           * (if oeel.returnfl then -1 else 1))
          
            
            oeelk.prodcost
            zk-cost
            v-rebate
            zsmsep.salesamt [month(oeel.invoicedt)]
            zsmsep.cogamt [month(oeel.invoicedt)]
            zsmsep.prodcat
            zsmsep.yr
            oeel.invoicedt
            oeelk.seqno
            oeelk.orderno
            oeelk.ordersuf.
    
    pause.        
    end.   
  */  
   run create_rev_zsmsep_bod.
  end.
end.



procedure create_rev_zsmsep_bod:
  for each pder where pder.cono = oeel.cono and
                      pder.orderno = oeel.orderno and
                      pder.ordersuf = oeel.ordersuf and
                      pder.lineno = oeel.lineno 
                      no-lock:
     v-rebate = v-rebate + (pder.rebateamt * pder.stkqtyship).                
  end.                    
  
  {z-smrzk2.i z-cat oeel.prodcat z-tild}
  
  
  assign pk_custno = oeel.custno
         pk_shipto = oeel.shipto
         pk_cat    = oeel.prodcat.
  run precheckparams.
  if not pk_good then
    do:
    leave.       
    end.
 
  
  find zsmsep where
                    zsmsep.cono = g-cono           and
                    zsmsep.custno = oeel.custno   and
                    zsmsep.shipto = oeel.shipto   and
                    zsmsep.prodcat = z-cat and
                    zsmsep.yr     = 
                    int(substring(string(year(oeel.invoicedt),"9999"),3,2))  and
                    zsmsep.whse = oeel.whse no-error.
  if not avail zsmsep then
    do:
    create zsmsep.
    assign zsmsep.cono = g-cono
           zsmsep.custno = oeel.custno
           zsmsep.shipto = oeel.shipto
           zsmsep.prodcat = z-cat
           zsmsep.yr     =
             int(substring(string(year(oeel.invoicedt),"9999"),3,2))  
           zsmsep.whse = oeel.whse.
    end.
       zsmsep.salesamt[z-paramfisc] =   
   zsmsep.salesamt[z-paramfisc] -
     ((((oeel.qtyship * oeelk.qtyneeded)  *
      (oeel.price * ((100 - oeel.discpct) / 100) * 
                 oeel.unitconv)) *
       (oeelk.prodcost / zk-cost))         
                           * (if oeel.returnfl then -1 else 1)).


    v-rebate = v-rebate * (oeelk.prodcost / zk-cost).
    zsmsep.cogamt[z-paramfisc] = 
      zsmsep.cogamt[z-paramfisc] - 
      (((((oeel.qtyship * oeelk.qtyneeded)  *
       oeel.prodcost) *
       (oeelk.prodcost / zk-cost))         
                           * (if oeel.returnfl then -1 else 1)) 
                           - v-rebate).

end.



procedure create_zsmsep_va:
  v-rebate = 0.
  
  find icsw where icsw.cono = g-cono and 
                  icsw.prod = vaesl.shipprod and
                  icsw.whse = vaesl.whse  no-lock no-error.
  if avail icsw then                
    v-rebate = icsw.listprice.
  else
    v-rebate = vaesl.prodcost.
    
   if vaesl.sctntype = "it" then
     do:
     if vaesl.timeactty <> "" then
       v-rebate = vaesl.timeelapsed / 3600.
     else
       v-rebate = vaesl.qtyship.
     end.
   else
     v-rebate = vaesl.qtyship.
   
   if 
     (v-rebate * 
      (zk-price *
      (vaesl.prodcost / vaeh.prodcost))) <> 0 or


      ((v-rebate) * vaesl.prodcost) <> 0 then
    do:   
  
  find icsp where icsp.cono = g-cono and icsp.prod = vaesl.shipprod
    no-lock no-error.
  if avail icsp then
    do:
    {z-smrzk2.i z-cat icsp.prodcat z-cart}
    end.
  else
    do:
    {z-smrzk2.i z-cat vaesl.prodcat z-cart}
    end.
  assign pk_custno = oeel.custno
         pk_shipto = oeel.shipto
         pk_cat    = if avail icsp then icsp.prodcat else vaesl.prodcat.
  run precheckparams.
  if not pk_good then
    do:
    run create_rev_zsmsep_va.
    leave.
    end.       
 
  
  
  find zsmsep where
                    zsmsep.cono = g-cono           and
                    zsmsep.custno = oeel.custno   and
                    zsmsep.shipto = oeel.shipto   and
                    zsmsep.prodcat = z-cat and
                    zsmsep.yr     = 
                    int(substring(string(year(oeel.invoicedt),"9999"),3,2))  
                    and
                    zsmsep.whse = vaesl.whse no-lock no-error.
  if not avail zsmsep then
    do:
    create zsmsep.
    assign zsmsep.cono = g-cono
           zsmsep.custno = oeel.custno
           zsmsep.shipto = oeel.shipto
           zsmsep.prodcat = z-cat
           zsmsep.yr     = 
           int(substring(string(year(oeel.invoicedt),"9999"),3,2))  
           zsmsep.whse = vaesl.whse.
    end.
  
   if vaesl.sctntype = "it" then
     do:
     if vaesl.timeactty <> "" then
       v-rebate = vaesl.timeelapsed / 3600.
     else
       v-rebate = vaesl.qtyship.
     end.
   else
     v-rebate = vaesl.qtyship.
   
   zsmsep.salesamt[z-paramfisc] =
     zsmsep.salesamt[z-paramfisc] +
 
     (v-rebate * 
      (zk-price *
      (vaesl.prodcost / vaeh.prodcost))).


   zsmsep.cogamt[z-paramfisc] = 
      zsmsep.cogamt[z-paramfisc] + 
     ((v-rebate) * vaesl.prodcost). 
     
  run create_rev_zsmsep_va.
  end.
end.




procedure create_rev_zsmsep_va:
  v-rebate = 0.
  if avail oeel then                
    v-rebate = ((oeel.price * ((100 - oeel.discpct) / 100)) * 
                  oeel.unitconv).
  else
    v-rebate = vaesl.prodcost.
  
  
  {z-smrzk2.i z-cat oeel.prodcat z-tild}

  assign pk_custno = oeel.custno
         pk_shipto = oeel.shipto
         pk_cat    = oeel.prodcat.
  run precheckparams.
  if not pk_good then
    do:
    leave.
    end.       
   
  find zsmsep where
                    zsmsep.cono = g-cono           and
                    zsmsep.custno = oeel.custno   and
                    zsmsep.shipto = oeel.shipto   and
                    zsmsep.prodcat = z-cat and
                    zsmsep.yr     =
                    int(substring(string(year(oeel.invoicedt),"9999"),3,2))  
                    and
                    zsmsep.whse = oeel.whse no-lock no-error.
  if not avail zsmsep then
    do:
    create zsmsep.
    assign zsmsep.cono = g-cono
           zsmsep.custno = oeel.custno
           zsmsep.shipto = oeel.shipto
           zsmsep.prodcat = z-cat
           zsmsep.yr     = 
           int(substring(string(year(oeel.invoicedt),"9999"),3,2))             ~             zsmsep.whse = oeel.whse.
    end.
  
   if vaesl.sctntype = "it" then
     do:
     if vaesl.timeactty <> "" then
       v-rebate = vaesl.timeelapsed / 3600.
     else
       v-rebate = vaesl.qtyship.
     end.
   else
     v-rebate = vaesl.qtyship.
   
     zsmsep.salesamt[z-paramfisc] =
     zsmsep.salesamt[z-paramfisc] +
 
    (v-rebate * 
      (zk-price *
      (vaesl.prodcost / vaeh.prodcost)) * -1).


   zsmsep.cogamt[z-paramfisc] = 
      zsmsep.cogamt[z-paramfisc] + 
     (((v-rebate) * vaesl.prodcost) * -1). 
       
     
     
end.

procedure precheckparams.

  pk_good = true.
  zelection_type = "p".
  zelection_char4 = substring (pk_cat,1,4).
  zelection_cust = 0.
  run zelectioncheck.
  if zelection_good = false then
    do:
    pk_good = false.
    next.
    end.
/*
  zelection_type = "x".
  zelection_char4 = substring (pk_cat,1,4).
  zelection_cust = 0.
  run zelectioncheck.
  if zelection_good = false then
    do:
    pk_good = false.
    next.
    end.
*/
  zelection_type = "c".
  zelection_char4 = "".
  zelection_cust = pk_custno.
  run zelectioncheck.
  if zelection_good = false then
    do:
    pk_good = false.
    next.
    end.                            

find arsc where arsc.cono = g-cono and
                arsc.custno = pk_custno no-lock no-error.
if avail arsc then do:
  
     if arsc.divno < p-divbeg or
        arsc.divno > p-divend then
       do:
       pk_good = false.
       next.
       end.
  

  assign
    q-slsrep = arsc.slsrepout
    q-state = arsc.state.
    
end.


if pk_shipto <> "" then
  do:
  find arss where arss.cono = g-cono and
                arss.custno = pk_custno and
                arss.shipto = pk_shipto no-lock no-error.
  if avail arss then
    do:
    q-slsrep = arss.slsrepout.
    q-state = arss.state.
    end.
  end.

 /* Sales Rep by technology  */
 

  find catmaster where catmaster.cono = 1 and 
                       catmaster.notestype = "zz" and
                       catmaster.primarykey  = "apsva" and
                       catmaster.secondarykey = 
                       substring(pk_cat,1,4) no-lock
                        no-error. 
  if not avail catmaster then
     do:
     v-parentcode = "".
     v-vendorid = substring(pk_cat,1,3).
     if substring(pk_cat,1,1) = "a" then 
       v-technology = "Automation".
     else if substring(pk_cat,1,1) = "f" then
       v-technology = "Fabrication". 
     else if substring(pk_cat,1,1) = "h" then 
       v-technology = "Hydraulic".   
     else if substring(pk_cat,1,1) = "p" then 
       v-technology =  "Pneumatic".   
     else if substring(pk_cat,1,1) = "s" then 
       v-technology =  "Service".     
     else if substring(pk_cat,1,1) = "f" then 
       v-technology =  "Filtration".  
     else if substring(pk_cat,1,1) = "c" then 
       v-technology =  "Connectors".  
     else if substring(pk_cat,1,1) = "m" then 
       v-technology =  "Mobile".      
     else if substring(pk_cat,1,1) = "n" then 
       v-technology =  "None".        
     end.
  
  else
    assign
       v-vendorid   = catmaster.noteln[1]
       v-technology = catmaster.noteln[2]
       v-parentcode = catmaster.noteln[3].

     
/* SLS override */  
      
     if p_parm then 
        do:
          assign  h_cust = string(pk_custno)
                  h_slsrepin = q-slsrep
                  h_shipto = pk_shipto
                  h_tech = if v-technology = "Service" then "s"
                            else "a".
           run chk_4_entry.
           if h_techtype = " " and v-technology = "Service" then
              do:
              h_tech = "a".
              run chk_4_entry.
              end.
           
           if h_techtype = "s" and v-technology = "service" then
             assign q-slsrep = h_slsrepin.
           else 
           if h_techtype = "a" then
             assign q-slsrep = h_slsrepin.
           else
              
             run xsdioetlt.p (input-output q-slsrep,
                              input substring(pk_cat,1,4),
                              input pk_custno,
                              input pk_shipto).
     
 
        
        end.
     else
/*  SLS override */
       
   
   
   
   run xsdioetlt.p (input-output q-slsrep,
                    input substring(pk_cat,1,4),
                    input pk_custno,
                    input pk_shipto).
     
 
  
  find smsn where smsn.cono = g-cono and smsn.slsrep = q-slsrep
    no-lock no-error.
  if avail smsn then
    do:
    assign
      q-region  = substring(smsn.mgr,1,1)
      q-district = substring(smsn.mgr,2,3).
    end.
  else
    assign
      q-region  = "z"
      q-district = "999".


  if q-region < p-regbeg or
     q-region > p-regend then
      do:
      pk_good = false.
      next.
      end.
  if q-district < p-distbeg or
     q-district > p-distend then
      do:
      pk_good = false.
      next.
      end.
     

  if q-slsrep < zel_begrep or
     q-slsrep > zel_endrep then
    do:
    pk_good = false.
    next.
    end.

  if p-list = true then
    do:
    zelection_type = "s".
    zelection_char4 = q-slsrep.
    zelection_cust = 0.
    run zelectioncheck.
    if zelection_good = false then
      do:
      pk_good = false.
      next.
      end.
   end.

 if p-list = true then
    do:
    zelection_type = "n".
    zelection_char4 = v-vendorid.
    zelection_cust = 0.
    run zelectioncheck.
    if zelection_good = false then
      do:
      pk_good = false.
      next.
      end.
   end.


 if p-list = true then
    do:
    zelection_type = "e".
    zelection_char4 = v-parentcode.
    zelection_cust = 0.
    run zelectioncheck.
    if zelection_good = false then
      do:
      pk_good = false.
      next.
      end.
   end.


 
 if (substring(v-technology,1,4) < p-techbeg or
     substring(v-technology,1,4) > p-techend) or
     (v-parentcode < zel_begvpid or
      v-parentcode > zel_endvpid) or
     (v-vendorid < zel_begvname or
      v-vendorid > zel_endvname)  then

   do:
   pk_good = false.
   next.
   end.


end.


procedure removeobsolete:
 assign forenddate = today. 
 do forbegdate = forbegdate to forenddate:
   for each zz-icsd where zz-icsd.cono = g-cono 
                      and zz-icsd.whse ge p-whsebeg
                      and zz-icsd.whse le p-whseend no-lock,
       each oeel use-index k-fill where oeel.cono = g-cono
        and oeel.statustype = "I" 
        and oeel.whse = zz-icsd.whse 
        and oeel.invoicedt = forbegdate
        and oeel.specnstype <> "l"
        and oeel.corechgty <> "r"
        and oeel.warrexchgfl = no    
        /*
        and oeel.whse ge p-whsebeg
        and oeel.whse le p-whseend
        */
        /*
        and oeel.slsrepout ge zel_begrep 
        and oeel.slsrepout le zel_endrep
        */
        and oeel.custno ge zel_begcust
        and oeel.custno le zel_endcust
        and oeel.prodcat ge zel_begcat
        and oeel.prodcat le zel_endcat
        and 
   /*
   can-do("wlam,0990,0991,0992,0993,0994,0995,0996,0997,0998",oeel.slsrepout)  
   */
  (can-do("wlam,0986,0987,0988,0989,0990,0991,0992,0993,0994",oeel.slsrepout)  
     or
   can-do("0982,0983,0984,0985,0986,0987,0988",oeel.slsrepout))         
           
        no-lock,
       first oeeh use-index k-oeeh
       where oeeh.cono = g-cono
         and oeeh.orderno =  oeel.orderno
         and oeeh.ordersuf = oeel.ordersuf no-lock,
       first sasj use-index k-inqd
                  where sasj.cono = g-cono and
                        sasj.jrnlno = oeeh.jrnlno2 no-lock:

       zmth = int(substring(string(sasj.perfisc,"9999"),1,2)).
       zmth = month(sasj.postdt).


       {w-icsp.i oeel.shipprod no-lock}

       assign pk_custno = oeel.custno
              pk_shipto = oeel.shipto
              pk_cat    = oeel.prodcat.
                     
       run precheckparams.
       
       if not pk_good then
         next.

       s-discamt = if v-smwodiscfl = yes then
                     (oeel.discamtoth + oeel.wodiscamt)
                   else
                     oeel.discamtoth.
       
       {icss.gfi   &prod          =  oeel.shipprod
                   &icspecrecno   =  oeel.icspecrecno
                   &lock          =  "no"}
       assign {speccost.gas &com = "/*"}
              v-marcost = 0
              v-mardiscoth = s-discamt                  
              v-mardisc = 0
              v-marnet = 0
              v-marnetdiv = 0
              v-marprice = 0
              v-marqty = 0
              s-speccostty = v-speccostty
              s-csunperstk = v-csunperstk
              v-conv = if oeel.unitconv <> 0 then 
                         {unitconv.gas &decconv = oeel.unitconv}
                       else
                         1.
       if avail oeeh then
         do:
         s-notesfl = oeeh.notesfl.
         {a-oeetlm.i &oeel = "oeel."
                     &oeeh = "oeeh."
                     &oerebty =  "'I'"
                     &com     = "/*"
                     &icss = "s-"
                     &sasc = "/*"}
         end.


       s-cost = (v-marcost * v-marqty) - 
                (if v-smvendrebfl = yes then
                    v-vendrebamt
                 else
                    0).

       if oeel.returnfl = true then
         assign
          s-cost = round(s-cost * -1,2)
          v-marnetdiv = round(v-marnetdiv * -1,2).
       else
         assign
          s-cost = round(s-cost,2)
          v-marnetdiv = round(v-marnetdiv,2).
 
       if oeeh.transtype = "cr" then
        s-cost = 0.       
 
       
       {z-smrzk2.i z-cat oeel.prodcat z-tild}
       
       find zsmsep where zsmsep.cono = g-cono
            and         zsmsep.yr = 
                 int(substring( string(year(oeel.invoicedt),"9999"),3,2))                    and       zsmsep.prodcat = z-cat
            and         zsmsep.whse = oeel.whse
            and         zsmsep.custno = oeeh.custno
            and         zsmsep.shipto = oeeh.shipto            no-error.
       if not avail zsmsep then
         do:
         create zsmsep.
         assign zsmsep.cono = g-cono
                zsmsep.yr = 
                int(substring(string(year(oeel.invoicedt),"9999"),3,2))       ~                 zsmsep.prodcat = z-cat
                zsmsep.whse = oeel.whse
                zsmsep.custno = oeeh.custn
                zsmsep.shipto = oeeh.shipto
                zsmsep.salesamt[zmth] = 
                  (v-marnetdiv * -1)

                zsmsep.cogamt [zmth] = 
                   (s-cost * -1).
         end.
       else                        
         do:
         assign
              zsmsep.salesamt[zmth] =
               zsmsep.salesamt[zmth] + 
                 (v-marnetdiv * -1).
              zsmsep.cogamt [zmth] =
               zsmsep.cogamt [zmth] + 
                (s-cost * -1). 
         end.
      end.
    end.
end.   

procedure CLUDGE_SM:
 assign forbegdate = 01/01/01
        forenddate = 12/31/01.
 
 do forbegdate = forbegdate to forenddate:
   for each zz-icsd where zz-icsd.cono = g-cono 
                      and zz-icsd.whse ge p-whsebeg
                      and zz-icsd.whse le p-whseend no-lock,
        each oeel use-index k-fill where oeel.cono = g-cono
        and oeel.statustype = "I"
        and oeel.whse = zz-icsd.whse
        and oeel.invoicedt = forbegdate
        and oeel.specnstype <> "l"
        and oeel.corechgty <> "r"
        and oeel.warrexchgfl = no    
/*      and oeel.whse ge p-whsebeg
        and oeel.whse le p-whseend
*/        
     /* and oeel.slsrepout ge zel_begrep 
        and oeel.slsrepout le zel_endrep  */
        /*  get it from the arsc.   */
        and oeel.custno ge zel_begcust
        and oeel.custno le zel_endcust
        and oeel.prodcat ge zel_begcat
        and oeel.prodcat le zel_endcat
        
        no-lock,
       first oeeh use-index k-oeeh
       where oeeh.cono = g-cono
         and oeeh.orderno =  oeel.orderno
         and oeeh.ordersuf = oeel.ordersuf no-lock,
       first sasj use-index k-inqd
                  where sasj.cono = g-cono and
                        sasj.jrnlno = oeeh.jrnlno2 no-lock:

       zmth = int(substring(string(sasj.perfisc,"9999"),1,2)).
       zmth = month(sasj.postdt).


       {w-icsp.i oeel.shipprod no-lock}

       assign pk_custno = oeel.custno
              pk_shipto = oeel.shipto
              pk_cat    = oeel.prodcat.
                     
       run precheckparams.
       
       if not pk_good then
         next.

       s-discamt = if v-smwodiscfl = yes then
                     (oeel.discamtoth + oeel.wodiscamt)
                   else
                     oeel.discamtoth.
       
       {icss.gfi   &prod          =  oeel.shipprod
                   &icspecrecno   =  oeel.icspecrecno
                   &lock          =  "no"}
       assign {speccost.gas &com = "/*"}
              v-marcost = 0
              v-mardiscoth = s-discamt                  
              v-mardisc = 0
              v-marnet = 0
              v-marnetdiv = 0
              v-marprice = 0
              v-marqty = 0
              s-speccostty = v-speccostty
              s-csunperstk = v-csunperstk
              v-conv = if oeel.unitconv <> 0 then 
                         {unitconv.gas &decconv = oeel.unitconv}
                       else
                         1.
       if avail oeeh then
         do:
         s-notesfl = oeeh.notesfl.
         {a-oeetlm.i &oeel = "oeel."
                     &oeeh = "oeeh."
                     &oerebty = "'I'"
                     &com     = "/*"
                     &icss = "s-"
                     &sasc = "/*"}
         end.


       s-cost = (v-marcost * v-marqty) - 
                (if v-smvendrebfl = yes then
                    v-vendrebamt
                 else
                    0).
       if oeel.returnfl = true then
         assign
          s-cost = round(s-cost * -1,2)
          v-marnetdiv = round(v-marnetdiv * -1,2).
       else
         assign
          s-cost = round(s-cost,2)
          v-marnetdiv = round(v-marnetdiv,2).
      if oeeh.transtype = "cr" then
        s-cost = 0.       
          
          
/*
       message oeel.orderno oeel.ordersuf 
               oeel.lineno oeel.shipprod
               v-marnetdiv s-cost.
       pause.
*/       
       {z-smrzk2.i z-cat oeel.prodcat z-tild}
       
       find zsmsep where zsmsep.cono = g-cono
            and         zsmsep.yr = 
                 int(substring( string(year(oeel.invoicedt),"9999"),3,2))                    and         zsmsep.prodcat = z-cat
            and         zsmsep.whse = oeel.whse
            and         zsmsep.custno = oeeh.custno
            and         zsmsep.shipto = oeeh.shipto            no-error.
       if not avail zsmsep then
         do:
         create zsmsep.
         assign zsmsep.cono = g-cono
                zsmsep.yr = 
                int(substring(string(year(oeel.invoicedt),"9999"),3,2))                       zsmsep.prodcat = z-cat
                zsmsep.whse = oeel.whse
                zsmsep.custno = oeeh.custno
                zsmsep.shipto = oeeh.shipto
                zsmsep.salesamt[zmth] = 
                  (v-marnetdiv)

                zsmsep.cogamt [zmth] = 
                   (s-cost).
         end.
       else                        
         do:
         assign
              zsmsep.salesamt[zmth] =
               zsmsep.salesamt[zmth] + 
                 (v-marnetdiv).
              zsmsep.cogamt [zmth] =
               zsmsep.cogamt [zmth] + 
                (s-cost). 
         end.
      end.
    end.
end.                                    

procedure backlog:

for each oeeh where oeeh.cono = g-cono and
                    oeeh.stagecd le 3 and 
                    oeeh.stagecd <> 9  
                and oeeh.whse ge p-whsebeg
                and oeeh.whse le p-whseend
                 /*  get it from the arsc.   */
                and oeeh.custno ge zel_begcust
                and oeeh.custno le zel_endcust
                and oeeh.transtype <> "qu" no-lock:

 for each oeel where oeel.cono = g-cono and
                    oeel.orderno = oeeh.orderno and
                    oeel.ordersuf = oeeh.ordersuf and
                    oeel.specnstype <> "l" 
                and oeel.corechgty <> "r"
                and oeel.warrexchgfl = no    
                and oeel.whse ge p-whsebeg
                and oeel.whse le p-whseend
                 /*  get it from the arsc.   */
                and oeel.custno ge zel_begcust
                and oeel.custno le zel_endcust
                and oeel.prodcat ge zel_begcat
                and oeel.prodcat le zel_endcat
        
                    no-lock:   
  assign pk_custno = oeel.custno
         pk_shipto = oeel.shipto
         pk_cat    = oeel.prodcat.
                     
  run precheckparams.
    
  if not pk_good then
    next.

 
    
  if oeeh.orderdisp = "j" and oeeh.borelfl = false then
     do:
     for each  b-oeel where b-oeel.cono = oeel.cono 
                       and  b-oeel.orderno = oeel.orderno 
                       and  b-oeel.ordersuf ge (oeel.ordersuf + 1)
                       and  b-oeel.shipprod = oeel.shipprod 
                       and  b-oeel.lineno = oeel.lineno 
                       and  b-oeel.bono = 0
                      no-lock:
       if avail b-oeel then
         do:
         find b-oeeh where b-oeeh.cono = b-oeel.cono and
                         b-oeeh.orderno = b-oeel.orderno and
                         b-oeeh.ordersuf = b-oeel.ordersuf
                         no-lock
                         no-error.
         if avail b-oeeh then
           if b-oeeh.borelfl = false then
             do:
             bookedb4 = true.
             leave.
             end.
           else 
             do:
             bofound4line = true.
             end.
         end.
      end.
    end.
 
   
   if bookedb4 = true then
     next.
   
   
   if oeeh.transtype <> "bl" and oeeh.transtype <> "br"
       and oeeh.orderdisp <> "j" then
     do:
     find first b-oeel where b-oeel.cono = oeel.cono and
                       b-oeel.orderno = oeel.orderno and
                       b-oeel.ordersuf > oeel.ordersuf and
                       b-oeel.shipprod = oeel.shipprod and
                       b-oeel.lineno = oeel.lineno 
                        no-lock no-error.
 
     if avail b-oeel then
       bofound4line = true.
     end.
  
    if oeeh.transtype = "bl" then
      do:
      oeelqtyrel = (if oeel.qtyrel < 0 then 
                      0                      
                    else
                      oeel.qtyrel).
      assign
         o_qty = oeel.qtyord - oeelqtyrel.
      if o_qty < 0 then o_qty = 0.
      end. 
    else
    if (bofound4line and oeeh.stagecd > 0) or oeel.bono ne 00 then
      do:
      o_qty = oeel.qtyship.
      end.
    else
    if not bofound4line then
      do:
      o_qty = oeel.qtyord.
      end.
   if o_qty = 0 then
     next.

     assign v-custrebamt = 0
            v-vendrebamt = 0
            o_extcost = 0
            o_wrkamt = 0.

            
     run oeetlmrb.p
      (oeeh.orderno,
       oeeh.ordersuf,
       oeel.lineno,
       0,   /* SX 55 */
       oeeh.stagecd,
       o_qty,
       output v-custrebamt,
       output v-vendrebamt).    
     assign
       o_extcost = ROUND(o_qty * oeel.prodcost ,2)
                 - v-custrebamt - v-vendrebamt.
       o_wrkamt = ((oeel.price * ((100 - oeel.discpct) / 100)) * 
                   oeel.unitconv)
                * o_qty.

     if oeel.returnfl = true then
       assign
         o_extcost = o_extcost * -1
         o_wrkamt = o_wrkamt * -1.

 {z-smrzk2.i z-cat oeel.prodcat z-tild}
       
 find zsmsep where zsmsep.cono = g-cono
          and         zsmsep.yr = e-yr
          and         zsmsep.prodcat = z-cat
          and         zsmsep.whse = oeel.whse
          and         zsmsep.custno = oeeh.custno
          and         zsmsep.shipto = oeeh.shipto            no-error.
 if not avail zsmsep then
   do:
   create zsmsep.
   assign zsmsep.cono = g-cono
          zsmsep.yr = e-yr
          zsmsep.prodcat = z-cat
          zsmsep.whse = oeel.whse
          zsmsep.custno = oeeh.custno
          zsmsep.shipto = oeeh.shipto
          zsmsep.salesamt[13] = 
                  (o_wrkamt)

                zsmsep.cogamt [13] = 
                   (o_extcost).
   end.
 else                        
   do:
   assign
      zsmsep.salesamt[13] =
         zsmsep.salesamt[13] + 
                (o_wrkamt).
      zsmsep.cogamt [13] =
         zsmsep.cogamt [13] + 
                (o_extcost). 
   end.
 end.
end.
end.

procedure crossreference:


assign x-summarybld = ""
       xit = false.

if g-currproc <> "tbrm" then
  do:
  find notes where notes.cono = g-cono  and 
                   notes.notestype = "zz" and
                   notes.primarykey = g-currproc and
                   notes.secondarykey = sapb.optvalue[2] no-lock no-error.
  if not avail notes then
    do:
    message "Invalid Format Type for " g-currproc " --> " sapb.optvalue[2].
    assign xit = true.
    return.
    end.
  else
    do:
    sum_ary = "a,a,a,a,a,a,a,a,a,a".
    
    overlay(sum_ary,1,(length(notes.noteln[4]))) = notes.noteln[4].
     
    if sapb.optvalue[3] <> "s" then
      x-summarybld = "a,a,a,a,a,a,a,a,a,a".
    else  
    if sapb.optvalue[3] = "s"  then
      do xinx = 1 to 10:
        if entry(xinx,sum_ary,",") = "@" then
          do:
          if xinx > 1 then
            assign x-summarybld = x-summarybld + ",".
          assign x-summarybld = x-summarybld +
                 sapb.optvalue[4].
          end.        
        else
         do:
         if xinx > 1 then
           assign x-summarybld = x-summarybld + ",".
         assign x-summarybld = x-summarybld  + "A".
         end.
      end.
    do xinx = 2 to 4:
     jrk_optvalue[xinx] = notes.noteln[xinx - 1].
     if xinx = 2 and sapb.optvalue[12] <> "v" then
       do:
       jrk_optvalue[xinx] = replace(jrk_optvalue[xinx],"v","n").
      /*  message jrk_optvalue[xinx]. pause. */
       end.
    end.
    jrk_optvalue[1] = sapb.optvalue[1].
    jrk_optvalue[5] = x-summarybld.  

    do xinx = 5 to 11:
     jrk_optvalue[xinx + 1] = sapb.optvalue[xinx].
    end.
    assign jrk_optvalue[13] = "no" 
           jrk_optvalue[14] = "no"
           jrk_optvalue[15] = "no"
           jrk_optvalue[16] = "no".
    end.
  end.
else
  do:
  
  p_parm = if sapb.optvalue[17] = "yes" then true else false. 
  
  do xinx = 1 to 16:
   jrk_optvalue[xinx] = sapb.optvalue[xinx].
  end.
  end.
end. 




procedure set_export_headers:



export_rec = "".

if p-export <> "" then
  do:
  output stream expt to value("/usr/tmp/" + p-export + ".smk").
  p-exportl = true.

  export_rec = "Stot".
  do v-inx4 = 1 to u-entitys:
  v-summary-lit2 = "".
  if v-lookup[v-inx4] = "w" then
     do:
     if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  "Whse".
     
     end. 
  else
  if v-lookup[v-inx4] = "o" then
     do:
     if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  "Country".
     
     end. 
  else
  if v-lookup[v-inx4] = "y" then
     do:
     if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  "City".
     
     end. 
  else
  if v-lookup[v-inx4] = "c" then
     do:
     if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + "Cust #" + v-del + "LookupNm".
     
     end.
  else
  if v-lookup[v-inx4] = "t" then
    do:
    if v-inx4 > 0 then 
      export_rec = export_rec + v-del.

    assign export_rec = export_rec + "ShipTo".
     
    end.
  else
  if v-lookup[v-inx4] = "s" then
    do:
    if v-inx4 > 0 then 
      export_rec = export_rec + v-del.

     assign export_rec = export_rec + "SlsRep" + v-del + "SlsRep Name".
     
    end.                          
  else
  if v-lookup[v-inx4] = "b" or
     v-lookup[v-inx4] = "p" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

    assign export_rec = export_rec + "Pcat" + v-del + "Pcat Descript".
 
    end.                          
  else
    if v-lookup[v-inx4] = "t" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + "ShipTo".
     end.                          
  else
  if v-lookup[v-inx4] = "d" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + "District".
     end.
  else
  if v-lookup[v-inx4] = "r" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + "Region".
        
    end.
  else
  if v-lookup[v-inx4] = "v" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + "Vendor Id".
     end.
  else
  if v-lookup[v-inx4] = "n" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + "Vendor Parent".
     end.
  else
  if v-lookup[v-inx4] = "a" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + "State".
     end.
  else
  if v-lookup[v-inx4] = "z" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + "ZipCd".
     end.
  else
  if v-lookup[v-inx4] = "g" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + "Technology".
     end.
  else
  if v-lookup[v-inx4] = "i" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + "Cust Group".
     end.
  
  else
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  v-lookup[v-inx4].
    end.
  end.
  assign export_rec = export_rec + v-del +
                      m-lbl[1] + v-del   +
                      m-lbl[1] + v-del   +
                      m-lbl[2] + v-del   +
                      m-lbl[2] + v-del   +
                      m-lbl[3] + v-del   +
                      m-lbl[3] + v-del   +
                      "Curr YTD" + v-del +
                      "Curr YTD" + v-del +
                      "Prev YTD" + v-del +
                      "Prev YTD" + v-del +
                      "Variance" + v-del +
                      "Variance" + v-del +
                      "Variance" + v-del +
                      "Prev Year" + v-del +
                      "Prev Year" + v-del +
                      "BackLog" + v-del +
                      "BackLog".

  
  export_rec = export_rec + chr(13).
  put stream expt unformatted export_rec.
  export_rec = " ".
  do v-inx4 = 1 to u-entitys:
  v-summary-lit2 = "".
  if v-lookup[v-inx4] = "w" then
     do:
     if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  " ".
     
     end. 
  else
  if v-lookup[v-inx4] = "o" then
     do:
     if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  " ".
     
     end. 
  else
  if v-lookup[v-inx4] = "y" then
     do:
     if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  " ".
     
     end. 
  else
  if v-lookup[v-inx4] = "c" then
     do:
     if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + " " + v-del + " ".
     
     end.
  else
  if v-lookup[v-inx4] = "t" then
    do:
    if v-inx4 > 0 then 
      export_rec = export_rec + v-del.

    assign export_rec = export_rec + " ".
     
    end.
  else
  if v-lookup[v-inx4] = "s" then
    do:
    if v-inx4 > 0 then 
      export_rec = export_rec + v-del.

     assign export_rec = export_rec + " " + v-del + " ".
     
    end.                          
  else
  if v-lookup[v-inx4] = "b" or
     v-lookup[v-inx4] = "p" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

    assign export_rec = export_rec + " " + v-del + " ".
 
    end.                          
  else
    if v-lookup[v-inx4] = "t" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + " ".
     end.                          
  else
  if v-lookup[v-inx4] = "d" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + " ".
     end.
  else
  if v-lookup[v-inx4] = "r" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + " ".
        
    end.
  else
  if v-lookup[v-inx4] = "v" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + " ".
     end.
  else
  if v-lookup[v-inx4] = "n" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + " ".
     end.
  else
  if v-lookup[v-inx4] = "a" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + " ".
     end.
  else
  if v-lookup[v-inx4] = "z" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + " ".
     end.
  else
  if v-lookup[v-inx4] = "g" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + " ".
     end.
  else
  if v-lookup[v-inx4] = "i" then
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec + " ".
     end.
  
  else
    do:
    if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

     assign export_rec = export_rec +  " ".
    end.
  end.

  
  assign export_rec = export_rec + v-del +
                      "Sales" + v-del +
                      "Margin %" + v-del +
                      "Sales" + v-del +
                      "Margin %" + v-del +
                      "Sales" + v-del +
                      "Margin %" + v-del +
                      "Sales" + v-del +
                      "Margin %" + v-del +
                      "Sales" + v-del +
                      "Margin %" + v-del +
                      "Sales" + v-del +
                      "Diff %" + v-del +
                      "Margin" + v-del +
                      "Sales"  + v-del +
                      "Margin %" + v-del +
                      "Sales"  + v-del +
                      "Margin %".
  export_rec = export_rec + chr(13).
  put stream expt unformatted export_rec.
  
  end. 
end.



{cust_tech2.i}

