/* oeepa6a.p */
/*****************************************************************************
  VERSION 5.5    : oeepa Ack print - SDI specific - landscape(oeepa6a.p) 
  PROCEDURE      : oeepa6a.p /* landscape printing of form */ 
  DESCRIPTION    : Order entry order acknowledgement format 1 print
  AUTHOR         : cm
  DATE WRITTEN   : 09/01/98
  CHANGES MADE   :
    10/20/99 cad; TB# e2761 Tally/Mix project
    03/29/00 rgm; TB# 3-2  Add RxServer Interface to Standard
    02/09/01 jd1; TB# e7852 Add Multicurrency Text
    04/24/01 kjb; TB# e8618 When faxing through RxServer, the footer frame
        was appearing in the body of the document
    02/08/01 dls; TB# e11973 OE Usability - New Subtotal Format
    11/12/02 kjb; TB# e15321 Modify the Tally print logic - Added tallyprt.lfo
        which contains the new variable and form definitions for the new tally
        print logic
    12/06/02 dls; TB# e13523 Additonal Addons in OE
    03/25/03 jkp; TB# e16626 Add option to select Ordered or Shipped Quantity
        in extended lines.
    04/12/04 rgm; TB# e18569 Allow Shipto to be billto address based on ARSS
        invoice setup
    04/23/04 bpa; TB# e17505 Add ICSEC.ADDPRTINFO to Customer Prod line
    06/10/09 das; NonReturn/NonCancelable
    11/01/12 das; Logo Print
******************************************************************************/

{p-rptbeg.i}

def input param    v-faxfl         as logical                          no-undo.
def     shared var v-headingfl     as logical                          no-undo.
def     shared var v-icdatclabel   as c format "x(9)"                  no-undo.
def     shared var p-promofl       as logical initial no               no-undo.
def            var v-pageno        as i                                no-undo.
def            var v-qtybo         as de                               no-undo.
def            var s-lit41a        as c format "x(18)"                 no-undo.
def            var s-lit42a        as c format "x(18)"                 no-undo.
def            var s-lit43a        as c format "x(18)"                 no-undo.
def            var s-lit44a        as c format "x(18)"                 no-undo.
def            var s-lit45a        as c format "x(18)"                 no-undo.
def            var s-lit46a        as c format "x(18)"                 no-undo.
def            var s-lit46b        as c format "x(18)"                 no-undo.
def            var s-lit47a        as c format "x(18)"                 no-undo.
def            var s-lit47b        as c format "x(18)"                 no-undo.
def            var s-lit49a        as c format "x(18)"                 no-undo.
def            var s-lit50a        as c format "x(18)"                 no-undo.
def            var s-lit53a        as c format "x(18)"                 no-undo.
def            var s-prod          as c format "x(70)"                 no-undo.
def            var s-addondesc     as c format "x(18)"                 no-undo.
def            var s-taxes         as de format "zzzzzzzz9.99-"        no-undo.
def            var s-dwnpmtamt     as de format "zzzzzzzz9.99-"        no-undo.
def            var s-shiptoaddr    like oeeh.shiptoaddr                no-undo.
def            var v-linecnt       as i                                no-undo.
def            var v-subnetamt     like oeeh.totinvamt                 no-undo.
def            var v-printfl       as logical                          no-undo.
def            var s-descrip       as c format "x(49)" initial ""      no-undo.
def            var s-price         as c format "x(13)" initial ""      no-undo.
def            var s-netamt        as c format "x(13)" initial ""      no-undo.
def            var v-totqtyshp     like oeeh.totqtyshp                 no-undo.
def            var v-reqtitle      as c format "x(17)" initial ""      no-undo.
def            var v-netord        like oeel.netord    initial 0       no-undo.
def            var v-qtyord        like oeel.qtyord    initial 0       no-undo.
def            var v-totlineamt    like oeeh.totlineamt initial 0      no-undo.
def            var v-invtofl       as logical                          no-undo.
def            var s-restockamt    like oeel.restockamt                no-undo.
def            var s-upcpno        as c format "x(5)"                  no-undo.
def            var s-lineno        as c format "x(3)"                  no-undo.
def            var s-qtyord        as c format "x(11)"                 no-undo.
def            var s-qtybo         as c format "x(11)"                 no-undo.
def            var s-qtyship       as c format "x(11)"                 no-undo.
def            var s-prcunit       as c format "x(4)"                  no-undo.
def            var s-discpct       as c format "x(14)"                 no-undo.
def            var s-sign          as c format "x"                     no-undo.
def            var v-subnetdisp    as c format "x(13)"                 no-undo.
/*das - NonReturn */
def            var s-lasterisk     as c format "x(2)"                  no-undo.
/* tb# e11973 02/11/02 dls; new subtotal logic */
def            var s-subtotdesc    as c format "x(24)" initial ""      no-undo.

/*tb e2761 10/20/99 cad; Tally/Mix project */
def            var s-length        as c format "x(12)"                 no-undo.
def            var x-shipdt   as date format "99/99/99"                no-undo. 
def     shared var p-prtord        as l                                no-undo.
def            var o-prtord        as l                                no-undo.

def     shared buffer b-sasc       for sasc.
def     shared buffer oeeh         for oeeh.
def     shared buffer arsc         for arsc.
def     shared buffer sapbo        for sapbo.
def            buffer b-oeel       for oeel.
def            buffer v-oeel       for oeel.
def            buffer q-oeel       for oeel.
def            buffer v-oeeh       for oeeh.
/*tb e7852 02/09/01; Multicurrency. New var v-currency */
def     shared var v-currency      like sastc.descrip                  no-undo.

/* SX55 */
def new shared var s-cantot    like oeeh.totinvamt     no-undo.   
def            var s-cantax1   as dec                  no-undo.   
def            var s-cantax2   as dec                  no-undo.   
def            var s-cantax3   as dec                  no-undo.   

def            var x-landscape as logical              no-undo.

/*tb 3-2 03/29/00 rgm; Rxserver interface hook for extra vars */
{oeepa1.z99 &define_rxvars = "*"}
{rxserv3.lva "new shared"}


/* Forms & Defines */
{f-oeepa6.i}    /* Forms & Defines */
{tallyprt.lfo &startpos = 5}

/* SX55 */
def buffer x-sasp  for sasp.
def var v-laserend as char no-undo.

{laserstd.lva "new"}

find x-sasp where x-sasp.printernm = sapb.printernm no-lock no-error.
if avail x-sasp and x-sasp.user1[1] = 000 then                       
 assign v-tovid = yes.
else
if sapb.printernm = "vid" then                                       
 assign v-tovid = yes.                                              
     
if v-tovid then
 assign v-tof     = v-esc + "&a0h15V"
        v-deftray = v-upper.
else
  assign v-tof = "  ". 
/*          v-tof = v-tof + "~012" + "~015".    */ 

if not v-faxfl then do : 
 assign v-rxform = v-esc + "&f9y4X"
        x-landscape = no.
 if (avail x-sasp and x-sasp.user3[1] = 001) or
    (avail sapb and sapb.filefl = yes) then do: 
  assign v-rxform = v-esc + "&f12y4X"
         v-laserend = v-esc + "(8U" + v-esc + "(s0p15.00h0s3b4102T" +
                    v-esc + "&k9.5H" + v-esc + "&l8C"
         v-lineterm = v-esc + "&l1O" + v-lineterm
         x-landscape = yes.
 end.
end. 
 
if oeeh.shipto <> "" then
    {w-arss.i oeeh.custno oeeh.shipto no-lock}.

v-invtofl = if oeeh.shipto <> "" and avail arss then arss.invtofl
            else false.

/* oeeh.transtype is needed to determine what form type is to be used */
if not v-tovid then 
 do:
  if not v-faxfl then 
    do: 
    /* check if printer has ALL LOGO Flash */
    if avail x-sasp and x-sasp.user5[10] = 0 then 
    do:
      /* 11/01/12 das; Logo Print */
      if oeeh.transtype = "qu" then
        run zsdigetlogo.p (input "OEQU",
                           input v-esc,
                           input recid(oeeh),
                           input x-landscape).
      else
        run zsdigetlogo.p (input "ACK ",
                           input v-esc,
                           input recid(oeeh),
                           input x-landscape).
    end. /* sapb.uer5[10] = 0 - printer has ALL LOGO Flash */
    else
      do:
      if oeeh.transtype = "qu" then 
        v-rxform = v-esc + "&f12y4x".
      else                          
        v-rxform = v-esc + "&f13y4X".
    end.  
  end. /* not v-faxfl */

 if v-faxfl = no then 
  put control v-reset + v-lineterm + v-deftray + v-noperf +
              v-rxform /* + v-tof si02*/  + v-laserend.
end. /* if not to v-tovid */

view frame f-tot4.

/* Print the invoice */
/* SX55 use p-oeepa6.i - cannot consolidate to p-oeepa9.i,
   the form in wider for this landscape version */
{p-oeepa6.i}

/*tb e8618 04/24/01 kjb; When faxing through RxServer, the footer is
    appearing in the body of the acknowledgement.  Need to set as 'paged' so
    that formating is done correctly. */


if v-faxfl = yes then do:
    hide frame f-head.
    hide frame f-headl.
end.

page.

/* SX 55 */
if not v-tovid and not v-faxfl then   
  put control v-reset.


procedure print_bl:
if oeeh.stagecd <= 3 and oeel.qtyord >= 1 and oeeh.ordersuf = 0 then do:
 
  assign s-message1 = "*** Blanket Releases Exist for the following Quantities"          s-message2 = "and release dates ***".
   
  
    display 
    s-message1
    s-message2
    with frame f-messages.
    down with frame f-messages.

    
    display 
    s-title1
    s-titles2
    s-title3
    s-title3
    with frame f-titles.
/*    down with frame f-titles.  */
     

    for each q-oeel where q-oeel.cono = oeel.cono and
                        q-oeel.orderno = oeel.orderno and
                   /*   q-oeel.ordersuf =  oeel.ordersuf and  */
                        q-oeel.lineno = oeel.lineno and
                        q-oeel.shipprod = oeel.shipprod and
                        q-oeel.qtyord >= 1 /* and 
                        q-oeel.transtype = "br"            */
                        no-lock by q-oeel.user3 with frame f-reldetail:


      find v-oeeh where v-oeeh.cono = q-oeel.cono and
                        v-oeeh.orderno = q-oeel.orderno and
                        v-oeeh.ordersuf = q-oeel.ordersuf and  
                        v-oeeh.stagecd < 3
                        no-lock no-error.

   if avail v-oeeh then do:   
    
    if q-oeel.transtype = "bl" then do: 
      
      for each notes where notes.cono = 1 and
                           notes.notestype = "zz" and
                           notes.primarykey = "JIT-BL" + 
                           string(q-oeel.orderno, "9999999") + 
                           "-" +   
                           string(q-oeel.ordersuf, "99") + "#" 
                           + string(q-oeel.lineno, "999") and 
                           notes.user2 = "F"
                           no-lock by date(notes.secondarykey):  
                        
            assign s-reloeno =  string(q-oeel.orderno, "9999999")  + "-" +
                                string(q-oeel.ordersuf, "99")
                                s-relprod =  q-oeel.shipprod
                   s-relqty  =  dec(notes.user6)
                   s-reldate =  date(notes.secondarykey).
     
              display 
                s-reloeno
                s-relprod
                s-relqty
                s-reldate
              with frame f-reldetail.
              down with frame f-reldetail.

     end. /* for each */
    end. /* if q-oeel */
    else do:
            assign s-reloeno =  string(q-oeel.orderno, "z999999")  + "-" +  
                                string(q-oeel.ordersuf, "99")     
                                s-relprod =  q-oeel.shipprod
                   s-relqty  =  dec(q-oeel.qtyord)
                   s-reldate =  date(substring(q-oeel.user3,1,6) + 
                                substring(q-oeel.user3,9,2)).

              display 
                s-reloeno
                s-relprod
                s-relqty
                s-reldate
              with frame f-reldetail.
              down with frame f-reldetail.
    
    
    end. /* else do */
   end.  /* for each oeel */            
  end. /* for each v-oeeh */    
end. /* <3 and >1 */
 end.  /* procedure */                                 
 
