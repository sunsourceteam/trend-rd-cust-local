
/* p-pdec.i 1.1 01/03/98 */
/* p-pdec.i 1.5 02/25/97 */
/*h*****************************************************************************
  INCLUDE      : p-pdec.i
  DESCRIPTION  : price/cost change logic for pdec.p
  USED ONCE?   : no (in pdecb.p twice)
  AUTHOR       : enp
  DATE WRITTEN : 05/07/90
  CHANGES MADE :
    06/13/94 tdd; TB# 15823 Added 2 user includes
    11/18/94 djp; TB# 14212 Allow list price to change by % of others & cleanup
    04/11/95 gdf; TB# 16985 Option #12 only works for first product
    02/25/97 kjb; TB# 11306 Open a journal when posting with Standard or
        Replacement cost.
    03/03/97 jl;  TB# 7241 (7.1) Special Price Costing. Change special price
        costing icsp fields to standard variables defined in speccost.gva
    03/11/97 kjb; TB# 11306 Code Review Changes
    02/27/01 mjg; TB# e7144 Add pdec to SX.enterprise GUI.
    05/10/01 bp ; TB# e7192  ; Add user hooks.
    03/07/02 sbr; TB# e10678 PDEC - list not updating
    05/16/02 ama; TB# e13071 CFE - wsd184 - v-msgtxt4 is listed twice and
        one s/b a 3 (ajw found while code reviewing pdecc.lpr which was copied
        from here)
    07/05/05 sbr; TB# e22776 Base price by new list uses old list
*******************************************************************************/

    if v-pricetype ne "" then icsw.pricetype = v-pricetype.

    do i = 1 to 3:

        if v-calcord[i] = "b" and (v-basetype ne "" or v-listtype ne "")
        then do:

          /* List Price Change */
          if v-listtype ne "" then do:
          
          if s-vendno <> 0 and (icsw.arpvendno <> s-vendno) then next. 
          if v-dtorfl = no and icsw.whse = "dtor" then next. 
          
            if v-listtype = "n" then do:
            if v-dtorfl = no and icsw.whse = "dtor" then next.
            else
            assign
                icsw.priceupddt     = v-newdt
                icsw.listprice      = v-listprice.
            end.  /* added */
           /*tb 14212 11/18/94 djp; Allow list price to change by % of others */
            else do:
              assign
                c-listprice     = if      v-listtype = "p"    then
                                      icsw.listprice * (v-listprice / 100)
                                  else if v-listtype = "s"    then
                                      icsw.stndcost  * (v-listprice / 100)
                                  else if v-listtype = "l"    then
                                      icsw.lastcost  * (v-listprice / 100)
                                  else if v-listtype = "r"    then
                                      icsw.replcost  * (v-listprice / 100)
                                  else if v-listtype = "b"    then
                                      icsw.baseprice * (v-listprice / 100)
                                  else v-listprice
                v-eri           = if c-listprice + (if can-do("d,p",v-listtype)
                                          then icsw.listprice
                                      else 0) le 0
                                  or c-listprice + (if can-do("d,p",v-listtype)
                                          then icsw.listprice
                                      else 0) > 9999999.99999
                                      then 1
                                  else 0
                icsw.listprice  = if v-eri ne 0 then icsw.listprice
                                  else c-listprice +
                                      if can-do("d,p",v-listtype)
                                          then icsw.listprice
                                  else 0
                icsw.priceupddt = if v-eri ne 0 then icsw.priceupddt
                                  else v-newdt.

              /*tb 14212 11/18/94 djp; Allow list price change by % of others */
              if can-do("p,s,l,r,b",v-listtype) and v-ptarget ne 8
                  and v-eri = 0
              then do:

                /* Rounding */
                /*tb 16985 04/11/95 gdf; pdapp.p calls p-pdapf2.i which runs
                    pdecb.p which calls p-pdec.i which calls p-pdrnd.i. If
                    v-ptarget is = 9, the p-pdrnd.i (which is called from 9
                    places, including BP) will change this value to a different
                    number to aid in the calculations. While this is not correct
                    for the PDAPP function, I have chosen not to modify the
                    p-pdrnd.i include, so no impact will result on BP. Instead
                    I have reset v-ptarget to the old ptarget after each round
                    of calculations for a single ICSW record. */
                assign
                    v-price         = icsw.listprice
                    o-ptarget       = v-ptarget.
                {p-pdrnd.i &pre = "v-"}
                assign
                    icsw.listprice  = v-price
                    v-ptarget       = o-ptarget.
              end.
            end. /* listtype <> "n" */
          end. /* listtype <> "" */

          /* Base Price Change */

          
          if v-basetype ne "" then do:
            if v-basetype = "n" then do:
        
             if s-vendno <> 0 and (icsw.arpvendno <> s-vendno) then next. 
             if v-dtorfl = no and icsw.whse = "dtor" then next. 
             else   
                assign
                    icsw.baseprice  = v-baseprice
                    icsw.priceupddt = v-newdt.
            end. /* basetype = "n" */

            else assign
                c-baseprice     = if v-basetype = "p"    then
                                      icsw.baseprice * (v-baseprice / 100)
                                  else if v-basetype = "s"    then
                                      icsw.stndcost  * (v-baseprice / 100)
                                  else if v-basetype = "r"    then
                                      icsw.replcost  * (v-baseprice / 100)
                                  else if v-basetype = "l"    then
                                      icsw.lastcost  * (v-baseprice / 100)
                                  else if v-basetype = "i"    then
                                      icsw.listprice * (v-baseprice / 100)
                                  else v-baseprice
                v-erb           = if c-baseprice + (if can-do("d,p",v-basetype)
                                          then icsw.baseprice
                                      else 0) le 0
                                  or c-baseprice + (if can-do("d,p",v-basetype)
                                          then icsw.baseprice
                                      else 0) > 9999999.99999
                                      then 1
                                  else 0
                icsw.baseprice  = if v-erb ne 0 then icsw.baseprice
                                  else c-baseprice + if can-do("d,p",v-basetype)
                                           then icsw.baseprice
                                  else 0
                icsw.priceupddt = if v-erb ne 0 then icsw.priceupddt
                                  else v-newdt.

            if can-do("r,s,l",v-basetype) and can-do("h,t",v-speccostty)
                and v-erb = 0
            then
                assign
                    icsw.baseprice  = icsw.baseprice /
                                      (if v-speccostty = "h" then 100
                                       else 1000).

            if can-do("r,s,l,p,i",v-basetype) and v-ptarget ne 8
                and v-erb = 0
            then do:

                /* Rounding */
                /*tb 16985 04/11/95 gdf; pdapp.p calls p-pdapf2.i which runs
                    pdecb.p which calls p-pdec.i which calls p-pdrnd.i. If
                    v-ptarget is = 9, the p-pdrnd.i (which is called from 9
                    places, including BP) will change this value to a different
                    number to aid in the calculations. While this is not correct
                    for the PDAPP function, I have chosen not to modify the
                    p-pdrnd.i include, so no impact will result on BP. Instead
                    I have reset v-ptarget to the old ptarget after each round
                    of calculations for a single ICSW record. */
                assign
                    o-ptarget       = v-ptarget
                    v-price         = icsw.baseprice.
                {p-pdrnd.i &pre = "v-"}
                assign
                    v-ptarget       = o-ptarget
                    icsw.baseprice  = v-price.
            end.
          end. /* v-basetype ne "" */
        end. /* Base Price or List Price Change */

        /* Replacement Cost Change */
        else if v-calcord[i]   = "r" and
                v-repltype    ne ""
        then do:

            /*tb 11306 02/25/97 kjb; Open a journal when posting with Standard
                or Replacement cost - store off the original ICSW Replacement
                cost */
            o-replcost = icsw.replcost.

            if v-repltype = "n" then do:
           
             
             if s-vendno <> 0 and (icsw.arpvendno <> s-vendno) then next. 
             if v-dtorfl = no and icsw.whse = "dtor" then next. 
             else
                assign
                    icsw.replcost   = v-replcost
                    icsw.replcostdt = v-newdt.
            end.

            else assign
                c-replcost      = if      v-repltype = "p"    then
                                      icsw.replcost  * (v-replcost  / 100)
                                  else if v-repltype = "s"    then
                                      icsw.stndcost  * (v-replcost  / 100)
                                  else if v-repltype = "b"    then
                                      icsw.baseprice * (v-replcost  / 100)
                                  else if v-repltype = "l"    then
                                      icsw.lastcost  * (v-replcost  / 100)
                                  else if v-repltype = "i"    then
                                      icsw.listprice * (v-replcost  / 100)
                                  else v-replcost
                v-erx           = if c-replcost  + (if can-do("d,p",v-repltype)
                                          then icsw.replcost
                                      else 0) le 0
                                  or c-replcost  + (if can-do("d,p",v-repltype)
                                          then icsw.replcost
                                      else 0) > 9999999.99999
                                      then 1
                                  else 0
                icsw.replcost   = if v-erx ne 0 then icsw.replcost
                                  else c-replcost + if can-do("d,p",v-repltype)
                                      then icsw.replcost
                                  else 0
                icsw.replcostdt = if v-erx ne 0 then icsw.replcostdt
                                  else v-newdt.

            if v-repltype = "b" and can-do("h,t",v-speccostty)
                and v-erx = 0
            then
                assign
                    icsw.replcost = icsw.replcost *
                                    (if v-speccostty = "h" then 100
                                     else 1000).

            if can-do("b,s,l,p,i",v-repltype) and v-ptarget ne 8
                and v-erx = 0
            then do:

                /* Rounding */
                /*tb 16985 04/11/95 gdf; pdapp.p calls p-pdapf2.i which runs
                    pdecb.p which calls p-pdec.i which calls p-pdrnd.i. If
                    v-ptarget is = 9, the p-pdrnd.i (which is called from 9
                    places, including BP) will change this value to a different
                    number to aid in the calculations. While this is not correct
                    for the PDAPP function, I have chosen not to modify the
                    p-pdrnd.i include, so no impact will result on BP. Instead
                    I have reset v-ptarget to the old ptarget after each round
                    of calculations for a single ICSW record. */
                assign
                    o-ptarget       = v-ptarget
                    v-price         = icsw.replcost.
                {p-pdrnd.i &pre = "v-"}
                assign
                    v-ptarget       = o-ptarget
                    icsw.replcost   = v-price.

            end. /* rounding */

            /*tb 11306 02/25/97 kjb; Open a journal when posting with Standard
                or Replacement cost - load journal data */
            /*d If a journal was opened in the parent procedure and the
                replacement cost has been changed then call pdglupdt.p to
                calculate the difference in the inventory value for the product
                and post that to the journal.  The journal should only be
                updated if posting by replacement cost in ICAOC. */
            if v-icglcost   = "r"    and
               icsw.replcost <> o-replcost
            then do:

                run pdglupdt.p(icsw.replcost,
                               o-replcost,
                               recid(icsp),
                               recid(icsw)).

            end. /* v-icglcost = r */

            /*u User Include */
            {pdecb.z99 &user_endreplcalc = "*"}

        end. /* replacement cost change */

        /* Standard Cost Change */
        else if v-calcord[i]   = "s" and
                v-stndtype    ne ""
        then do:

            /*tb 11306 02/25/97 kjb; Open a journal when posting with Standard
                or Replacement cost - store off the original ICSW Standard
                cost */
            o-stndcost = icsw.stndcost.

            /*tb 15823 06/13/94 tdd; Added user include */
            {pdecb.z99 &user_begstdcalc = "*"}

            if v-stndtype = "n" then do:
               if s-vendno <> 0 and (icsw.arpvendno <> s-vendno) then next. 
               if v-dtorfl = no and icsw.whse = "dtor" then next.

              else do:
                  assign
                    icsw.stndcost   = v-stndcost
                    icsw.stndcostdt = v-newdt.
              end.  /* else do */      
            end.

            else assign
                c-stndcost      = if      v-stndtype = "p"    then
                                      icsw.stndcost  * (v-stndcost  / 100)
                                  else if v-stndtype = "r"    then
                                      icsw.replcost  * (v-stndcost  / 100)
                                  else if v-stndtype = "b"    then
                                      icsw.baseprice * (v-stndcost  / 100)
                                  else if v-stndtype = "l"    then
                                      icsw.lastcost  * (v-stndcost  / 100)
                                  else if v-stndtype
                                   = "i"    then
                                      icsw.listprice * (v-stndcost  / 100)
                                  else v-stndcost
                v-ers           = if c-stndcost  + (if can-do("d,p",v-stndtype)

                                          then icsw.stndcost
                                      else 0) le 0
                                  or c-stndcost  + (if can-do("d,p",v-stndtype)
                                          then icsw.stndcost
                                      else 0) > 9999999.99999
                                      then 1
                                  else 0
                icsw.stndcost   = if v-ers ne 0 then icsw.stndcost
                                  else c-stndcost + if can-do("d,p",v-stndtype)
                                      then icsw.stndcost
                                  else 0
                icsw.stndcostdt = if v-ers ne 0 then icsw.stndcostdt
                                  else v-newdt.

            if v-stndtype = "b" and can-do("h,t",v-speccostty)
                and v-ers = 0
            then
                assign
                    icsw.stndcost   = icsw.stndcost *
                                      (if v-speccostty = "h" then 100
                                       else 1000).

            if can-do("b,r,l,p,i",v-stndtype) and v-ptarget ne 8
                and v-ers = 0
            then do:

                /* Rounding */
                /*tb 16985 04/11/95 gdf; pdapp.p calls p-pdapf2.i which runs
                    pdecb.p which calls p-pdec.i which calls p-pdrnd.i. If
                    v-ptarget is = 9, the p-pdrnd.i (which is called from 9
                    places, including BP) will change this value to a different
                    number to aid in the calculations. While this is not correct
                    for the PDAPP function, I have chosen not to modify the
                    p-pdrnd.i include, so no impact will result on BP. Instead
                    I have reset v-ptarget to the old ptarget after each round
                    of calculations for a single ICSW record. */
                assign
                    o-ptarget       = v-ptarget
                    v-price         = icsw.stndcost.
                {p-pdrnd.i &pre = "v-"}
                assign
                    v-ptarget       = o-ptarget
                    icsw.stndcost   = v-price.

            end. /* rounding */

            /*tb 11306 02/25/97 kjb; Open a journal when posting with Standard
                or Replacement cost - load journal data */
            /*d If a journal was opened in the parent procedure and the
                standard cost has been changed then call pdglupdt.p to
                calculate the difference in the inventory value for the product
                and post that to the journal.  The journal should only be
                updated if posting by standard cost in ICAOC. */
            if v-icglcost   = "s"    and
               icsw.stndcost <> o-stndcost
            then do:

                run pdglupdt.p(icsw.stndcost,
                               o-stndcost,
                               recid(icsp),
                               recid(icsw)).

            end. /* v-icglcost = s */

            /*tb 15823 06/13/94 tdd; Added user include */
            {pdecb.z99 &user_endstdcalc = "*"}

        end. /* Standard cost change */

    end. /* do i = 1 to 3 */

    if g-ourproc = "pdec"
    and (v-eri ne 0 or v-erb ne 0 or v-erx ne 0 or v-ers ne 0)
    then do:
        assign
            v-msgtxt1 = ""
            v-msgtxt2 = ""
            v-msgtxt3 = ""
            v-msgtxt4 = "".

        if v-eri ne 0 then v-msgtxt1 =
           "List Price Calculation Could Not Be Completed".

        if v-erb ne 0 then v-msgtxt2 =
           "Base Price Calculation Could Not Be Completed".

        if v-erx ne 0 then v-msgtxt3 =
           "Repl. Cost Calculation Cound Not Be Completed".

        if v-ers ne 0 then v-msgtxt4 =
           "Stnd. Cost Calculation Could Not Be Completed".

        &IF "{&NXTREND-SYS}" = "TREND" &THEN

          display
              v-msgtxt1
              v-msgtxt2
              v-msgtxt3
              v-msgtxt4
          with frame f-msg.

          do on endkey undo, leave:
              pause.
          end.

          hide frame f-msg no-pause.

        &ENDIF

        if s-whse ne "" then
             undo calc, leave main.
        else undo calc, next.
    end. /* g-ourproc = "pdec" */

    {t-all.i icsw}

