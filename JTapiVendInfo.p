
/* JTapiCustInfor.p
   Purpose:    PAPagon interface Returns vendor A/R balance and 
               any other vendor values kept in SXE

*/

{g-SdiApi.i}    /* Error table */
{g-SdiDebug.i "New"}

Define VAR v-cono      like sasc.cono          no-undo.
Define VAR v-vendor  like APSV.vendno          no-undo.

Define VAR v-APbalance as character            no-undo.
Define VAR v-APlimit   as character            no-undo.
Define VAR v-APtermscd as character            no-undo.

Define VAR s-totalbal  as dec no-undo.
Define VAR g-APcodinbalfl as logical no-undo.
Define VAR j as int no-undo.




Define VAR v-debugfl as   logical      no-undo init true.    
Define Stream mylog.



Procedure JTCustInfo:
  Define input  parameter ip-cono      as char.
  Define input  parameter ip-vendor  as char.
    
  Define output parameter  ip-APtermscd as character.
   
  Define output parameter ip-errcd     as char.
  Define output parameter ip-error     as char.


  run ZsdiDebug ( input ip-cono,
                  input "JTapiCustInfo.p").

  if v-setup-debugfl then do:
    assign v-setup-log-text = 
      "Parameters Cono = " +  
       ip-cono.  

    run ZsdiLogWrite(input "minimum",
                     input v-setup-log-text).
  end.

  int(ip-cono) no-error.
  if not error-status:error then 
    assign v-cono = int(ip-cono).
  else do:
    assign v-errinx = 3
           ip-errcd = string(v-errinx)
           ip-error = v-errorcd[v-errinx].
    return.
  end.          


  if v-setup-debugfl then do:
    assign v-setup-log-text = 
      "Parameters vendor = " +  
       ip-vendor.  

    run ZsdiLogWrite(input "minimum",
                     input v-setup-log-text).
  end.

  dec(ip-vendor) no-error.
  if not error-status:error then 
    assign v-vendor = dec(ip-vendor).
  else do:
    assign v-errinx = 3
           ip-errcd = string(v-errinx)
           ip-error = v-errorcd[v-errinx].
    return.
  end.          


  find apsv where 
       apsv.cono   = v-cono and
       apsv.vendno = v-vendor     no-lock no-error.
  if avail apsv then do:

    find sasta where
         sasta.cono     = v-cono and
         sasta.codeiden = "t"    and
         sasta.codeval  = apsv.termstype no-lock no-error.
      if avail sasta then 
        assign 
          ip-APtermscd    = if sasta.user5 <> "" then 
                              sasta.user5 
                            else
                              sasta.codeval.
      else
         ip-APtermscd      =  sasta.codeval.

  end.
  
  RETURN.
end.

{p-SdiDebug.i}
