/*****************************************************************************
   zsdigetlog.p will determine what logo and form to print on a printer
   x-app - VAXQ - vaepv.p   - VAEXQ Quote Print
           VAQU - vaepq9.p  - VA Quotation Print
                  vaepq9f.p - VA Quotation Print
           OEQU - oeepa6a.p - Acknowledge Print for QU transtype OE's
           XQQU - oeexp.p   - OEEXQ Quote Print
           WPAK - wtep6.p   - Warehouse Transfer Packing Slip
           WPIK - wtep6.p   - Warehouse Transfer Pick Ticket
           INV  - oeepi9.p  - Invoice Print
           ACK  - oeepa6a.p - Acknowledge Print for transtype other thqn QU
           PICK - oeepp9.p  - Pick Ticket Print
           PACK - oeepp9.p  - Packing Slip Print
           RCPT - oeepp9.p  - Receipt Print
           PO   - poepp6a.p - PO Print
                  poepp9a.p - PO Print
*****************************************************************************/

def input parameter        x-app       as c format "x(4)"  no-undo.
def input parameter        x-esc       as c                no-undo.
def input parameter        oe-recid    as recid            no-undo.
def input parameter        x-landscape as logical          no-undo.
def shared var v-rxform    as char                         no-undo.
/* def shared var v-esc       as char                         no-undo. */
def        var x-company   like arsc.salesterr             no-undo.
def        var w-custno    like oeeh.custno                no-undo.
def        var w-shipto    like oeeh.shipto                no-undo.
def        var w-slsrep    like oeeh.slsrepout             no-undo.
def        var ap-logo     as char format "x(4)"           no-undo.
def        var ap-whse     as char format "x(4)"           no-undo.
def        var ic-logo     as char format "x(4)"           no-undo.
/*
def stream wtf.
output stream wtf to "/usr/tmp/logo_debug".
*/
{g-all.i}
if x-app = "VAXQ" then
  do:
  find sapbv where recid(sapbv) = oe-recid no-lock no-error.
  if not avail sapbv then return.
  assign w-custno = sapbv.vendno
         w-shipto = sapbv.user4.
  {w-arsc.i w-custno no-lock}
  if avail arsc then        
    w-slsrep = arsc.slsrepout.
  else
    w-slsrep = " ".
 
end.
else
if x-app = "XQQU" then
  do:
  find oeehb where recid(oeehb) = oe-recid no-lock no-error.
  if not avail oeehb then return.
  assign w-custno = oeehb.custno
         w-shipto = oeehb.shipto
         w-slsrep = oeehb.slsrepout.
end.
else
  do:
  if x-app = "INV " or x-app = "ACK " or x-app = "PICK" or x-app = "PACK" or
     x-app = "RCPT" or x-app = "OEQU" or x-app = "VAQU" then
    do:
    find oeeh where recid(oeeh) = oe-recid no-lock no-error.
    if not avail oeeh then return.
    assign w-custno = oeeh.custno
           w-shipto = oeeh.shipto
           w-slsrep = oeeh.slsrepout.
  end.
  if x-app = "WPAK" or x-app = "WPIK" then
    do:
    find wteh where recid(wteh) = oe-recid no-lock no-error.
    if not avail wteh then return.
    find first wtelo where wtelo.cono  = g-cono and
                           wtelo.wtno  = wteh.wtno and
                           wtelo.wtsuf = wteh.wtsuf
                           no-lock no-error.
    if not avail wtelo then
      do:
      find icsd where icsd.cono = g-cono and
                      icsd.whse = wteh.shipfmwhse
                      no-lock no-error.
      if avail icsd then
        assign x-company = substr(icsd.user5,1,4).
    end. /* not avail wtelo */
    if avail wtelo and wtelo.ordertype = "o" then
      do:
      find oeeh where oeeh.cono     = g-cono and
                      oeeh.orderno  = wtelo.orderaltno and
                      oeeh.ordersuf = wtelo.orderaltsuf
                      no-lock no-error.
      if avail oeeh then
        assign w-custno = oeeh.custno
               w-shipto = oeeh.shipto
               w-slsrep = oeeh.slsrepout.
    end. /* wtelo.ordertype = "o" */
    if avail wtelo and wtelo.ordertype = "f" then
      do:
      find first vaeh where vaeh.cono  = g-cono and
                            vaeh.vano  = wtelo.orderaltno and
                            vaeh.vasuf = wtelo.orderaltsuf
                            no-lock no-error.
      if avail vaeh then
        do:
        find first vaelo where vaelo.cono  = g-cono and
                               vaelo.vano  = vaeh.vano and
                               vaelo.vasuf = vaeh.vasuf
                               no-lock no-error.
        if avail vaelo then
          do:
          find first oeeh where oeeh.cono     = g-cono and
                                oeeh.orderno  = vaelo.orderaltno and
                                oeeh.ordersuf = vaelo.orderaltsuf
                                no-lock no-error.
          if avail oeeh then
            assign w-custno = oeeh.custno    
                   w-shipto = oeeh.shipto    
                   w-slsrep = oeeh.slsrepout.
        end. /* vavail vaelo */
      end. /* avail vaeh */
    end. /* wtelo.ordertype = "f" */
  end. /* x-app = WPAK/WPIK */
  /****
  if x-app = "VAQU" then
    do:
    find vaeh where recid(vaeh) = oe-recid no-lock no-error.
    if not avail vaeh then return.
    find first vaelo where vaelo.cono  = g-cono and
                           vaelo.vano  = vaeh.vano and
                           vaelo.vasuf = vaeh.vasuf
                           no-lock no-error.
    if not avail vaelo then
      do:
      find icsd where icsd.cono = g-cono and
                      icsd.whse = vaeh.whse
                      no-lock no-error.
      if avail icsd then
        assign x-company = substr(icsd.user5,1,4).
    end. /* not avail vaelo */
    if avail vaelo and vaelo.ordertype = "o" then
      do:
      find oeeh where oeeh.cono     = g-cono and
                      oeeh.orderno  = vaelo.orderaltno and
                      oeeh.ordersuf = vaelo.orderaltsuf
                      no-lock no-error.
      if avail oeeh then
        assign w-custno = oeeh.custno
               w-shipto = oeeh.shipto
               w-slsrep = oeeh.slsrepout.
    end. /* vaelo.ordertype = "o" */
    if avail vaelo and vaelo.ordertype = "w" then
      do:
      find first wteh where wteh.cono  = g-cono and
                            wteh.wtno  = vaelo.orderaltno and
                            wteh.wtsuf = vaelo.orderaltsuf
                            no-lock no-error.
      if avail wteh then
        do:
        find first wtelo where wtelo.cono  = g-cono and
                               wtelo.wtno  = wteh.wtno and
                               wtelo.wtsuf = wteh.wtsuf
                               no-lock no-error.
        if avail wtelo then
          do:
          find first oeeh where oeeh.cono     = g-cono and
                                oeeh.orderno  = wtelo.orderaltno and
                                oeeh.ordersuf = wtelo.orderaltsuf
                                no-lock no-error.
          if avail oeeh then
            assign w-custno = oeeh.custno    
                   w-shipto = oeeh.shipto    
                   w-slsrep = oeeh.slsrepout.
        end. /* avail wtelo */
      end. /* avail wteh */
    end. /* vaelo.ordertype = "w" */
  end. /* x-app = VAQU */
  ***/
end. /* not a quote */

if x-app = "INV " or x-app = "ACK " or x-app = "PICK" or x-app = "PACK" or
   x-app = "OEQU" or x-app = "XQQU" or x-app = "WPIK" or x-app = "WPAK" or
   x-app = "VAQU" or x-app = "VAXQ" or x-app = "RCPT" then
  do:
  if x-company = " " then
    do:
    if w-shipto = "" then
      do:
      {w-arsc.i w-custno no-lock}
      if avail arsc then
        assign x-company = substr(arsc.user19,1,4).
    end.
    else
      do:
      {w-arss.i w-custno w-shipto no-lock}
      if avail arss then
        assign x-company = substr(arss.user19,1,4).
    end.
  end. /* x-company not determined */
  if x-company = "FPT"  then 
    do:
    if x-app = "OEQU" or x-app = "XQQU" or 
       x-app = "VAQU" or x-app = "VAXQ" then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f512y4X".
      else
        assign v-rxform = x-esc + "&f509y4X".
    if x-app = "ACK " then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f513y4X".
      else
        assign v-rxform = x-esc + "&f502y4X".
    if x-app = "INV " then 
      assign v-rxform = x-esc + "&f504y4X".
    if x-app = "PICK" or x-app = "WPIK" then 
      assign v-rxform = x-esc + "&f511y4X".
    if x-app = "PACK" or x-app = "WPAK" then
      assign v-rxform = x-esc + "&f503y4X".
    if x-app = "RCPT" then
      assign v-rxform = x-esc + "&f517y4X".
    return.
  end. /* FPT */
  if x-company = "WIST" then 
    do:
    if x-app = "OEQU" or x-app = "XQQU" or 
       x-app = "VAQU" or x-app = "VAXQ" then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f612y4X".
      else
        assign v-rxform = x-esc + "&f609y4X".
    if x-app = "ACK " then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f613y4X".
      else
        assign v-rxform = x-esc + "&f602y4X".
    if x-app = "INV " then 
      assign v-rxform = x-esc + "&f604y4X".
    if x-app = "PICK" or x-app = "WPIK" then 
      assign v-rxform = x-esc + "&f611y4X".
    if x-app = "PACK" or x-app = "WPAK" then
      assign v-rxform = x-esc + "&f603y4X".
    if x-app = "RCPT" then
      assign v-rxform = x-esc + "&f617y4X".
    return.
  end. /* Wistech */
  if x-company = "WARD" then 
    do:
    if x-app = "OEQU" or x-app = "XQQU" or 
       x-app = "VAQU" or x-app = "VAXQ" then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f712y4X".
      else
        assign v-rxform = x-esc + "&f709y4X".
    if x-app = "ACK " then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f713y4X".
      else
        assign v-rxform = x-esc + "&f702y4X".
    if x-app = "INV " then 
      assign v-rxform = x-esc + "&f704y4X".
    if x-app = "PICK" or x-app = "WPIK" then 
      assign v-rxform = x-esc + "&f711y4X".
    if x-app = "PACK" or x-app = "WPAK" then
      assign v-rxform = x-esc + "&f703y4X".
    if x-app = "RCPT" then
      assign v-rxform = x-esc + "&f717y4X".
    return.
  end. /* WARD */
  if x-company = "HSI"  then 
    do:
    if x-app = "OEQU" or x-app = "XQQU" or 
       x-app = "VAQU" or x-app = "VAXQ" then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f812y4X".
      else
        assign v-rxform = x-esc + "&f809y4X".
    if x-app = "ACK " then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f813y4X".
      else
        assign v-rxform = x-esc + "&f802y4X".
    if x-app = "INV " then 
      assign v-rxform = x-esc + "&f804y4X".
    if x-app = "PICK" or x-app = "WPIK" then 
      assign v-rxform = x-esc + "&f811y4X".
    if x-app = "PACK" or x-app = "WPAK" then
      assign v-rxform = x-esc + "&f803y4X".
    if x-app = "RCPT" then
      assign v-rxform = x-esc + "&f817y4X".
    return.
  end. /* HSI */
  if x-company = "FPE"  then 
    do:
    if x-app = "OEQU" or x-app = "XQQU" or 
       x-app = "VAQU" or x-app = "VAXQ" then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f912y4X".
      else
        assign v-rxform = x-esc + "&f909y4X".
    if x-app = "ACK " then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f913y4X".
      else
        assign v-rxform = x-esc + "&f902y4X".
    if x-app = "INV " then 
      assign v-rxform = x-esc + "&f904y4X".
    if x-app = "PICK" or x-app = "WPIK" then 
      assign v-rxform = x-esc + "&f911y4X".
    if x-app = "PACK" or x-app = "WPAK" then
      assign v-rxform = x-esc + "&f903y4X".
    if x-app = "RCPT" then
      assign v-rxform = x-esc + "&f917y4X".
    return.
  end. /* FPE */
  if x-company = "TOPS"  then 
    do:
    if x-app = "OEQU" or x-app = "XQQU" or 
       x-app = "VAQU" or x-app = "VAXQ" then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f1012y4X".
      else
        assign v-rxform = x-esc + "&f1009y4X".
   if x-app = "ACK " then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f1013y4X".
      else
        assign v-rxform = x-esc + "&f1002y4X".
    if x-app = "INV " then 
      assign v-rxform = x-esc + "&f1004y4X".
    if x-app = "PICK" or x-app = "WPIK" then 
      assign v-rxform = x-esc + "&f1011y4X".
    if x-app = "PACK" or x-app = "WPAK" then
      assign v-rxform = x-esc + "&f1003y4X".
    if x-app = "RCPT" then
      assign v-rxform = x-esc + "&f1017y4X".
    return.
  end. /* Topssco */
  if x-company = "FORD"  then 
    do:
    if x-app = "OEQU" or x-app = "XQQU" or 
       x-app = "VAQU" or x-app = "VAXQ" then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f1212y4X".
      else
        assign v-rxform = x-esc + "&f1209y4X".
    if x-app = "ACK " then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f1213y4X".
      else
        assign v-rxform = x-esc + "&f1202y4X".
    if x-app = "INV " then 
      assign v-rxform = x-esc + "&f1204y4X".
    if x-app = "PICK" or x-app = "WPIK" then 
      assign v-rxform = x-esc + "&f1211y4X".
    if x-app = "PACK" or x-app = "WPAK" then
      assign v-rxform = x-esc + "&f1203y4X".
    if x-app = "RCPT" then
      assign v-rxform = x-esc + "&f1217y4X".
    return.
  end. /* Ford-Gelatt */
  if x-company = "PABC" then
    do:
    if x-app = "OEQU" or x-app = "XQQU" or 
       x-app = "VAQU" or x-app = "VAXQ" then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f212y4X".
      else
        assign v-rxform = x-esc + "&f209y4X".
    if x-app = "ACK " then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f210y4X".
      else
        assign v-rxform = x-esc + "&f202y4X".
    if x-app = "INV " then 
      assign v-rxform = x-esc + "&f204y4X".
    if x-app = "PICK" or x-app = "WPIK" then 
      assign v-rxform = x-esc + "&f211y4X".
    if x-app = "PACK" or x-app = "WPAK" then
      assign v-rxform = x-esc + "&f203y4X".
    if x-app = "RCPT" then
      assign v-rxform = x-esc + "&f217y4X".
    return.
  end. /* Pabco */
  if x-company = "SS" or x-company = " " then
    do:
    if x-app = "OEQU" or x-app = "XQQU" or 
       x-app = "VAQU" or x-app = "VAXQ" then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f12y4X".
      else
        assign v-rxform = x-esc + "&f9y4X".
    if x-app = "ACK " then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f13y4X".
      else
        assign v-rxform = x-esc + "&f2y4X".
    if x-app = "INV " then 
      assign v-rxform = x-esc + "&f4y4X".
    if x-app = "PICK" or x-app = "WPIK" then 
      assign v-rxform = x-esc + "&f11y4X".
    if x-app = "PACK" or x-app = "WPAK" then
      assign v-rxform = x-esc + "&f3y4X".
    if x-app = "RCPT" then
      assign v-rxform = x-esc + "&f17y4X".
    return.
  end. /* SunSource */
  if x-company = "WEST"  then 
    do:
    if x-app = "OEQU" or x-app = "XQQU" or 
       x-app = "VAQU" or x-app = "VAXQ" then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f1312y4X".
      else
        assign v-rxform = x-esc + "&f1309y4X".
    if x-app = "ACK " then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f1313y4X".
      else
        assign v-rxform = x-esc + "&f1302y4X".
    if x-app = "INV " then 
      assign v-rxform = x-esc + "&f1304y4X".
    if x-app = "PICK" or x-app = "WPIK" then 
      assign v-rxform = x-esc + "&f1311y4X".
    if x-app = "PACK" or x-app = "WPAK" then
      assign v-rxform = x-esc + "&f1303y4X".
    if x-app = "RCPT" then
      assign v-rxform = x-esc + "&f1317y4X".
    return.
  end. /* Western-Hydrostatic */
  if x-company = "WESH"  then 
    do:
    if x-app = "OEQU" or x-app = "XQQU" or 
       x-app = "VAQU" or x-app = "VAXQ" then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f1412y4X".
      else
        assign v-rxform = x-esc + "&f1409y4X".
    if x-app = "ACK " then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f1413y4X".
      else
        assign v-rxform = x-esc + "&f1402y4X".
    if x-app = "INV " then 
      assign v-rxform = x-esc + "&f1404y4X".
    if x-app = "PICK" or x-app = "WPIK" then 
      assign v-rxform = x-esc + "&f1411y4X".
    if x-app = "PACK" or x-app = "WPAK" then
      assign v-rxform = x-esc + "&f1403y4X".
    if x-app = "RCPT" then
      assign v-rxform = x-esc + "&f1417y4X".
    return.
  end. /* Western-Hydrostatic-without the SunSource */
  
  if x-company = "PGON"  then 
    do:
    /**
    if g-operinit begins "das" then
      do:
      message "zsdigetlogo" "x-app:" x-app. pause.
    end.
    **/
    if x-app = "OEQU" or x-app = "XQQU" or 
       x-app = "VAQU" or x-app = "VAXQ" then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f1512y4X".
      else
        assign v-rxform = x-esc + "&f1509y4X".
    if x-app = "ACK " then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f1513y4X".
      else
        assign v-rxform = x-esc + "&f1502y4X".
    if x-app = "INV " then 
      assign v-rxform = x-esc + "&f1504y4X".
    if x-app = "PICK" or x-app = "WPIK" then 
      assign v-rxform = x-esc + "&f1511y4X".
    if x-app = "PACK" or x-app = "WPAK" then
      assign v-rxform = x-esc + "&f1503y4X".
    if x-app = "RCPT" then
      assign v-rxform = x-esc + "&f1517y4X".
    return.
  end. /* Paragon-Hydrostatic */

  if x-company = "CALH"  then 
    do:
    /*
    if g-operinit begins "das" then
      do:
      message "zsdigetlogo" "x-app:" x-app. pause.
    end.
    */
    if x-app = "OEQU" or x-app = "XQQU" or 
       x-app = "VAQU" or x-app = "VAXQ" then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f1612y4X".
      else
        assign v-rxform = x-esc + "&f1609y4X".
    if x-app = "ACK " then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f1613y4X".
      else
        assign v-rxform = x-esc + "&f1602y4X".
    if x-app = "INV " then 
      assign v-rxform = x-esc + "&f1604y4X".
    if x-app = "PICK" or x-app = "WPIK" then 
      assign v-rxform = x-esc + "&f1611y4X".
    if x-app = "PACK" or x-app = "WPAK" then
      assign v-rxform = x-esc + "&f1603y4X".
    if x-app = "RCPT" then
      assign v-rxform = x-esc + "&f1617y4X".
    return.
  end. /* Callahan-Weber */ 
  
  if x-company = "DJI"  then 
    do:
    if x-app = "OEQU" or x-app = "XQQU" or 
       x-app = "VAQU" or x-app = "VAXQ" then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f1712y4X".
      else
        assign v-rxform = x-esc + "&f1709y4X".
    if x-app = "ACK " then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f1713y4X".
      else
        assign v-rxform = x-esc + "&f1702y4X".
    if x-app = "INV " then 
      assign v-rxform = x-esc + "&f1704y4X".
    if x-app = "PICK" or x-app = "WPIK" then 
      assign v-rxform = x-esc + "&f1711y4X".
    if x-app = "PACK" or x-app = "WPAK" then
      assign v-rxform = x-esc + "&f1703y4X".
    if x-app = "RCPT" then
      assign v-rxform = x-esc + "&f1717y4X".
    return.
  end. /* DJ Industrial */ 
  if x-company = "PERF"  then 
    do:
    if x-app = "OEQU" or x-app = "XQQU" or 
       x-app = "VAQU" or x-app = "VAXQ" then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f1812y4X".
      else
        assign v-rxform = x-esc + "&f1809y4X".
    if x-app = "ACK " then
      if x-landscape = yes then
        assign v-rxform = x-esc + "&f1813y4X".
      else
        assign v-rxform = x-esc + "&f1802y4X".
    if x-app = "INV " then 
      assign v-rxform = x-esc + "&f1804y4X".
    if x-app = "PICK" or x-app = "WPIK" then 
      assign v-rxform = x-esc + "&f1811y4X".
    if x-app = "PACK" or x-app = "WPAK" then
      assign v-rxform = x-esc + "&f1803y4X".
    if x-app = "RCPT" then
      assign v-rxform = x-esc + "&f1817y4X".
    return.
  end. /* PerfectionServo */ 


end. /* invoice */ 
if x-app = "PO  " then
  do:
  assign ap-logo = " "
         ap-whse = " "
         ic-logo = " ".
  find poeh where recid(poeh) = oe-recid no-lock no-error.
  if avail poeh then
    do:
    find notes where notes.cono = g-cono and
                     notes.notestype = "ZZ" and
                     notes.primarykey = "logo print" and
                     notes.secondarykey = string(poeh.vendno,"99999999999") +
                                          string(poeh.shipfmno,"9999")
                     no-lock no-error.
    if not avail notes then
      find notes where notes.cono = g-cono and
                       notes.notestype = "ZZ" and
                       notes.primarykey = "logo print" and
                       notes.secondarykey = string(poeh.vendno,"99999999999")
                       no-lock no-error.
    if avail notes then
      do:
      do i = 1 to 10:
        if num-entries(notes.noteln[i],",") >= 2 and        
               entry(2,notes.noteln[i],",") = "*" then
          do:
          assign ap-logo = entry(1,notes.noteln[i],",")
                 ap-whse = "*".
          leave.
        end.
        else
          do:
          if num-entries(notes.noteln[i],",") >= 2 and 
                 entry(2,notes.noteln[i],",") = poeh.whse then
            do:
            assign ap-logo = entry(1,notes.noteln[i],",")
                   ap-whse = entry(2,notes.noteln[i],",").
            leave.
          end.
        end. /* not "*" */
      end. /* 1 to 10 */
    end. /* avail notes */
    find icsd where icsd.cono = g-cono and
                    icsd.whse = poeh.whse
                    no-lock no-error.
    if avail icsd then
      assign ic-logo = substr(icsd.user5,1,4).
    if ap-whse = "*" then
      assign x-company = if ap-logo <> " " then ap-logo
                         else
                         if ic-logo <> " " then ic-logo 
                         else " ".
    else
    if ap-whse = poeh.whse then
      assign x-company = if ap-logo <> " " then ap-logo
                         else if ic-logo <> " " then ic-logo 
                              else "SS".
    else
    if ap-whse <> poeh.whse then
      assign x-company = if ic-logo <> " " then ic-logo 
                         else "SS".
    else
    if ap-logo = ic-logo then 
      assign x-company = if ap-logo <> " " then ap-logo else "SS".
    else
    if ap-logo <> ic-logo and ap-logo <> " " then
      assign x-company = ap-logo.
    else
    if ap-logo <> ic-logo and ic-logo = " " then
      assign x-company = ap-logo.
    else
      assign x-company = " ".
  
    if x-company = " " then assign x-company = "SS".
  end. /* avail poeh */
  if x-company = "SS" or x-company = " " then
    do:
    assign v-rxform = x-esc + "&f21y4X".
    return.
  end.
  if x-company = "PABC" then
    do:
    assign v-rxform = x-esc + "&f221y4X".
    return.
  end.
  if x-company = "FPE" then
    do:
    assign v-rxform = x-esc + "&f921y4X".
    return.
  end.
  if x-company = "FPT" then
    do:
    assign v-rxform = x-esc + "&f521y4X".
    return.
  end.
  if x-company = "WIST" then
    do:
    assign v-rxform = x-esc + "&f621y4X".
    return.
  end.
  if x-company = "WARD" then
    do:
    assign v-rxform = x-esc + "&f721y4X".
    return.
  end.
  if x-company = "HSI" then
    do:
    assign v-rxform = x-esc + "&f821y4X".
    return.
  end.
  if x-company = "TOPS" then
    do:
    assign v-rxform = x-esc + "&f1021y4X".
    return.
  end.
  if x-company = "FORD" then
    do:
    assign v-rxform = x-esc + "&f1221y4X".
    return.
  end.
  if x-company = "FGA" then
    do:
    assign v-rxform = x-esc + "&f1121y4X".
    return.
  end.
  if x-company = "WEST" then
    do:
    assign v-rxform = x-esc + "&f1321y4X".
    return.
  end.
  if x-company = "WESH" then
    do:
    assign v-rxform = x-esc + "&f1421y4X".
    return.
  end.
  if x-company = "PGON" then
    do:
    assign v-rxform = x-esc + "&f1521y4X".
    return.
  end.
  if x-company = "CALH" then
    do:
    assign v-rxform = x-esc + "&f1621y4X".
    return.
  end.
  if x-company = "DJI" then
    do:
    assign v-rxform = x-esc + "&f1721y4X".
    return.
  end.
  if x-company = "PERF" then
    do:
    assign v-rxform = x-esc + "&f1821y4X".
    return.
  end.
  if v-rxform = " " then
    assign v-rxform = x-esc + "&f21y4X".
  
end. /* PO */
      

