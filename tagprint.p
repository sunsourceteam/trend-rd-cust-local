/*************************************************************************
  Programe Name: tagprint.p
*************************************************************************/

define input parameter zp-lineno as integer.
define input parameter zp-reprint as c format "x".
define input parameter zp-printernm like sasp.printernm.
define input parameter zp-apptype as c format "x". 


{g-all.i}
{g-po.i}
{g-wt.i}
{g-oe.i}
{prodTempTable.i}


def var ctr   as int                         no-undo.
def var c-custno    like oeeh.custno         no-undo. 
def var x-inx as integer                     no-undo.
def var v-orderno   like oeeh.orderno        no-undo.
def var v-ordersuf  like oeeh.ordersuf       no-undo.
def var v-oeline    like oeel.lineno         no-undo.
def var v-torderno  as char format "x(10)"   no-undo.
def var v-filename as c format "x(30)"       no-undo.
def var v-tilda as c format "x" init "~~"    no-undo.
def var v-custprod like  poel.shipprod       no-undo.
def var v-lineprod like  poel.shipprod       no-undo.
def var v-printername as c                   no-undo.
def var v-labelname as c                     no-undo.
def var v-uom       as char format "x(4)"    no-undo.
def var v-rev       as char format "x(2)"    no-undo.
def var v-poline    as char format "xxx"     no-undo.
def var v-shipdt like oeel.reqshipdt         no-undo.
def var v-addr2 as char format "x(30)"       no-undo.
def var v-addr3 as char format "x(30)"       no-undo.
def var v-valid     as logical               no-undo.
def var v-found     as l                     no-undo.
def var v-outputfl  as logical               no-undo.
def var v-quantity  as int  format "zzzzz9"  no-undo. 
def var x-quantity  as int  format "zzzzz9"  no-undo. 
def var p-qty       as int  format "zzzzz9"  no-undo. 
def var x-labels    as int  format "zzzzz9"  no-undo.
def var z-labels    as integer               no-undo.
def var v-icswbinloc1 as char format "x(10)" no-undo.
def var v-icswbinloc2 as char format "x(10)" no-undo.
def var v-desc      as char format "x(20)"   no-undo.
def var v-countrycd  as c  format "x(10)"    no-undo.
def var v-placedby   as char format "x(10)"  no-undo.
def var v-refer      as char format "x(30)"  no-undo.
def var v-lnrefer    as char format "x(08)"  no-undo.
def var v-custpo    as char format "x(20)"   no-undo.
def var v-ordnbr    as char format "x(10)"   no-undo.
def var v-supplier  as char format "x(6)"    no-undo.
def var v-lineno    as char format "x(7)"    no-undo.
def var v-whsechk   as char format "x(4)"    no-undo.
def var v-box1      as int format "zz"       no-undo. 
def var v-box2      as int format "zz"       no-undo. 
def var newtype       as logical             no-undo.
def var boxtype       as logical             no-undo.

Def var v-spec1          as char format "x(60)"  no-undo.
Def var v-spec2          as char format "x(60)"  no-undo.
def var v-spec3          as char format "x(60)"  no-undo. 
Def var v-specs          as char format "x(61)"  extent 16 no-undo.

def var pick-oeehuser1 as char format "x(78)"          no-undo.  
def var pick-oeehuser2 as char format "x(78)"          no-undo.  
def var pick-oeeluser1 as char format "x(78)"          no-undo.  
def var pick-oeeluser2 as char format "x(78)"          no-undo.  
def var pick-oeeluser3 as char format "x(78)"          no-undo.  
def var pick-oeeluser6 as dec  format "99999999.99999" no-undo. 
def var pick-oeeluser8 as char format "x(8)"           no-undo.  
def var pick-noteln1   as char format "x(60)"          no-undo. 
def var pick-noteln2   as char format "x(60)"          no-undo.  
def var pick-noteln3   as char format "x(60)"          no-undo. 
def var pick-noteln4   as char format "x(60)"          no-undo.     
def var pick-noteln5   as char format "x(60)"          no-undo. 
def var pick-noteln6   as char format "x(60)"          no-undo. 
def var pick-custno    like oeeh.custno                no-undo.
def var pick-placedby  as char format "x(10)"          no-undo.  
def var pick-reference as char format "x(24)"          no-undo.  
def var pick-jobno     as char format "x(8)"           no-undo.  
def var pick-custpoln  as char format "x(5)"           no-undo.  
def var pick-shipdt    as date                         no-undo.
def var pick-shipprod  as char format "x(24)"          no-undo.  
def var pick-descrip1  like icsp.descrip[1]            no-undo. 
def var pick-supplierid as char format "x(15)"         no-undo.  
def var pick-revision  as char format "x(6)"           no-undo. 
def var pick-bin-loc1  as char format "x(20)"          no-undo.  
def var pick-bin-loc2  as char format "x(20)"          no-undo. 
def var pick-user1     as char format "x(15)"          no-undo. 
def var pick-user2     as char format "x(15)"          no-undo. 
def var pick-user3     as char format "x(10)"          no-undo. 
def var pick-user4     as char format "x(50)"          no-undo. 
def var pick-user5     as char format "x(50)"          no-undo. 
def var pick-user6     as char format "x(50)"          no-undo. 
def var pick-user7     as char format "x(50)"          no-undo. 
def var pick-user8     as char format "x(50)"          no-undo. 
def var pick-user9     as char format "x(50)"          no-undo. 
def var pick-user10    as char format "x(50)"          no-undo. 
def var pick-user11    as char format "x(50)"          no-undo. 
def var pick-user12    as char format "x(50)"          no-undo. 
def var pick-user13    as char format "x(15)"          no-undo. 
def var pick-user14    as char format "x(15)"          no-undo. 
def var pick-user15    as char format "x(15)"          no-undo. 
def var pick-user16    as char format "x(15)"          no-undo. 
def var pick-user17    as char format "x(15)"          no-undo. 
def var pick-user18    as char format "x(15)"          no-undo. 


def stream bcd.
if zp-apptype = "o" then run oeorderprt.
return.


/****** reprint shipping label printed at oees(f8) time **************/ 
procedure oeorderprt:
 assign v-orderno  = g-orderno
        v-ordersuf = g-ordersuf.



 find oeeh where oeeh.cono     = g-cono and 
                 oeeh.orderno  = v-orderno and
                 oeeh.ordersuf = v-ordersuf no-lock no-error.

/* 'o' selection will find a pack or pick labels regardless **/
find first zsdibarcd where 
           zsdibarcd.cono    = g-cono      and 
           zsdibarcd.type = "cust"      and 
           zsdibarcd.appty = "pick" and
           zsdibarcd.labelnm = "taglabel.lbl" and     
           zsdibarcd.style = "new"         
           no-lock no-error.        

 if not avail zsdibarcd then return. 


 if avail oeeh then do: 

  assign v-torderno  = string(v-orderno,"9999999")
               + "-" + string(v-ordersuf,"99") 
          v-custpo   = oeeh.custpo
          pick-custno = oeeh.custno
          pick-user1 = v-torderno
          pick-user5 = oeeh.shiptocity + ", " +  
                       oeeh.shiptost + "  " +    
                       oeeh.shiptozip           
          pick-supplierid = trim(v-supplier)
          v-lineno   = string(zp-lineno).
  
  assign v-placedby     = oeeh.placedby
         pick-placedby  = oeeh.takenby    /* oeeh.placedby  */  
         pick-oeehuser2 = string(oeeh.enterdt) + " " + string(oeeh.entertm)
         v-refer        = oeeh.refer
         pick-reference = oeeh.refer 
         v-lnrefer      = oeeh.jobno
         pick-jobno     = oeeh.jobno
         pick-user7     = "K"  + oeeh.custpo.    

  assign v-ordnbr = string(dec(oeeh.orderno),"zzzzz99")  + "-" +
                    string(dec(oeeh.ordersuf),"99")
          v-poline = substring(oeeh.custpo,20,3)
          pick-custpoln = substring(oeeh.custpo,20,3)
          v-addr2  = if oeeh.custno = 6600295 then 
                       oeeh.shiptoaddr[2]
                     else 
                       oeeh.shiptoaddr[1]
          v-addr3  = oeeh.shiptocity + ", " + oeeh.shiptost + "  " +
                     oeeh.shiptozip
          v-shipdt = today
          pick-shipdt = today.

  assign pick-supplierid = v-supplier. 

 end.  /*avail oeeh  */ 

 if avail zsdibarcd and zsdibarcd.style = "new" then do: 
  assign newtype = yes
         v-printername = if zp-printernm = "" then zsdibarcd.printernm[1]
                         else zp-printernm
         v-labelname   = zsdibarcd.labelnm
         v-whsechk     = zsdibarcd.whse. 

  find arsc where arsc.cono = g-cono and 
       arsc.custno = oeeh.custno /* zsdibarcd.custno */
       no-lock no-error. 
  
 end. 
 
 assign v-filename = ""
        v-outputfl = true.  /* false */

for each oeel where
         oeel.cono     = g-cono and
         oeel.orderno  = g-orderno and
         oeel.ordersuf = g-ordersuf and
         oeel.lineno   = zp-lineno /* g-oelineno  */
         no-lock:
    
 if oeel.ordertype = "f" then do:
  find first vaeh where 
             vaeh.cono = oeel.cono and
             vaeh.vano = oeel.orderaltno 
             no-lock no-error.

    if avail vaeh then do:
     find first vaes where
                 vaes.cono      = vaeh.cono  and
                 vaes.vano      = vaeh.vano  and
                 vaes.vasuf     = vaeh.vasuf and
                 vaes.sctntype  = "sp" 
                 no-lock no-error.
                 
      if avail vaes and vaes.specdata <> " " then do:
         
        run zsdiparseeditorblock.p (input recid(vaes),
                                     output v-specs ).
                                                     
        assign v-spec1   = right-trim(string(v-specs[1])) 
               v-spec2   = right-trim(string(v-specs[2]))
               v-spec3   = right-trim(string(v-specs[3])).

        /*
        message "v-spec1" v-spec1
                "v-spec2" v-spec2
                "v-spec3" v-spec3.    
        */        
        assign pick-oeeluser1 = v-spec1
               pick-oeeluser3 = v-spec2
               pick-user5     = v-spec3.
      
        /*
        message "the pick1"  pick-oeeluser1
                "the pick2"  pick-oeeluser3
                "the pick3"  pick-user5. pause.
        */
      end. /* specdata <> "" */           
                 
                 
               
       assign substring(pick-oeeluser2,1,7)  = string(vaeh.vano)
              substring(pick-oeeluser2,9,3)  = substring(vaeh.user2,1,3)
              /* HOT */
              substring(pick-oeeluser2,14,1) = substring(vaeh.user1,1,1).
              /* p or c */
     /*
     message "pick-oeeluser2" pick-oeeluser2
             "pick-oeeluser1" pick-oeeluser1
             "pick-oeeluser3" pick-oeeluser3
             "pick-oeehuser1" pick-oeehuser1. pause.
     */
   end. /* avail vaeh */
  end.  /* f */
  
  else
  if oeel.kitfl then do:
     assign substring(pick-oeeluser2,14,1)   = "K". 
  end.
  
 if v-filename = "" and newtype then do: 
  nameloop: 
  do while true:
   v-filename = "/usr/tmp/" + string(time) + string(random(1,999)) +
                "puck".
   if search(v-filename) = ? then
    leave nameloop.
  end.
  output stream bcd to value(v-filename).
  assign v-outputfl = true.
     put stream bcd unformatted
    "/L=" 
    v-labelname
     " "
     "/P="
    v-printername 
   /** bc2000 .ldd file name **/
     " /DR"
     zsdibarcd.txdatanm
     " /C=" 
     zsdibarcd.copies
     " "
    chr(13)
    chr(10).
  end.    

 
                 
 find icsp where icsp.cono = g-cono and 
                 icsp.prod = oeel.shipprod no-lock no-error.
 find icsw where icsw.cono = g-cono and 
                 icsw.whse = oeel.whse and
                 icsw.prod = oeel.shipprod no-lock no-error.

 assign v-lineprod = oeel.shipprod.   
 
 assign pick-shipprod = oeel.shipprod.

  if avail icsp and oeel.specnstype <> "N" then 
     assign pick-descrip1 = icsp.descrip[1].       
  else
     assign pick-descrip1 = oeel.proddesc.
 
 
 if avail icsp then do: 
     
  assign v-uom  = oeel.unit
         v-rev  = if avail icsp then 
                   substring(icsp.descrip[1],22,2)
                  else 
                   " ". 
         v-desc = if avail icsp then 
                   trim(substring(icsp.descrip[1],1,20)) + 
                   trim(substring(icsp.descrip[2],1,20))   
                  else 
                  " ".
 end. 

 
if newtype = yes then do:  
    
   put stream bcd unformatted

   '"taglabel.lbl"' 
   v-tilda    
    oeel.orderno  
   v-tilda
    string(oeel.ordersuf,"99") 
   v-tilda     
    pick-custno    
   v-tilda     
    oeeh.custpo    
   v-tilda     
    if oeeh.custno = 93005471 or oeeh.custno = 93006366 then
      oeeh.shiptonm
    else
      arsc.name 
   v-tilda 
    oeeh.shiptonm  
   v-tilda 
    oeeh.shiptoaddr[1]
   v-tilda    
    oeeh.shiptoaddr[2]  
   v-tilda 
    oeeh.shiptocity  
   v-tilda 
    oeeh.shiptost
   v-tilda 
    oeeh.shiptozip 
   v-tilda 
    oeeh.shiptocity + ", " + 
    oeeh.shiptost + "  " +
    oeeh.shiptozip
   v-tilda 
    oeeh.shipvia 
   v-tilda 
    oeel.whse 
   v-tilda 
    pick-oeehuser1 
   v-tilda 
    pick-oeehuser2 
   v-tilda 
   (if avail icsw then 
     string(icsw.binloc1,"xx/xx/xxx/xxx")
    else 
     " ") 
   v-tilda 
   (if avail icsp then 
     string(icsp.weight) 
    else 
     "0") 
   v-tilda 
   (if avail icsp then 
     string(icsp.cubes)  
    else 
     "0") 
   v-tilda 
    oeeh.shipto 
   v-tilda 
    string(oeel.lineno) 
   v-tilda 
    pick-shipprod 
   v-tilda 
    v-custprod 
   v-tilda 
   pick-descrip1
   v-tilda 
    string(oeel.qtyord)
   v-tilda 
    string(p-qty) 
   v-tilda
    oeel.unit 
   v-tilda 
    pick-oeeluser1      
   v-tilda
    pick-oeeluser2
   v-tilda  
    pick-oeeluser3  
   v-tilda 
    pick-oeeluser6
   v-tilda 
    pick-oeeluser8 
   v-tilda 
    pick-noteln1
   v-tilda 
    pick-noteln2
   v-tilda 
    pick-noteln3
   v-tilda 
    pick-noteln4
   v-tilda 
    pick-noteln5
   v-tilda  
    pick-noteln6
   v-tilda    
    pick-placedby  
   v-tilda    
    pick-reference 
   v-tilda    
    pick-jobno     
   v-tilda    
    pick-custpoln  
   v-tilda    
    pick-shipdt    
   v-tilda    
    pick-supplierid
   v-tilda    
    pick-revision  
   v-tilda    
    string(pick-bin-loc1,"xx/xx/xxx/xxx")  
   v-tilda    
    string(pick-bin-loc2,"xx/xx/xxx/xxx")  
   v-tilda    
    pick-user1     
   v-tilda    
    pick-user2     
   v-tilda    
    pick-user3     
   v-tilda    
    pick-user4     
   v-tilda    
    pick-user5     
   v-tilda 
    pick-user6  
   v-tilda 
    pick-user7  
   v-tilda 
    pick-user8  
   v-tilda 
    pick-user9  
   v-tilda 
    pick-user10 
   v-tilda 
    pick-user11 
   v-tilda 
    pick-user12 
   v-tilda 
    pick-user13 
   v-tilda    
    pick-user14 
   v-tilda    
    pick-user15 
   v-tilda    
    pick-user16 
   v-tilda    
    pick-user17 
   v-tilda    
    pick-user18 
   chr(13)
   chr(10).
 end.

end.  /* each oeel*/  

if v-outputfl = true then do: 
 output stream bcd  close.
 unix silent value("sh /rd/unibar/ibprint.sh " + v-filename).
 unix silent value ("rm " + v-filename).   
end. 
    
end procedure.     
