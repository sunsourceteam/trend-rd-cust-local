/* sdirfpobu.lpr 1.1 01/27/98 */
/*h*****************************************************************************
  INCLUDE      : sdirfpobu.lpr (based on poebtlsl.lpr, rel 9.0)
  DESCRIPTION  : Purchase Order Batch Entry - Line Items - Serial/Lot Processing
                 IB Module; This include runs an "icets.p" specially designed
                 for RF (tiny and VM) terminals
  USED ONCE?   : no
  AUTHOR       : aa, After mtt
  DATE WRITTEN : 01/15/98 after 10/25/95
  CHANGES MADE :
    10/25/95 mtt; TB# 19528 Develop PO Batch Receiving Function
    01/15/98  aal TB# 23693 IB Module. Call ibrfets instead of icets.p;
        removed calls to putscr.gsc.
    12/02/02 blw; TB# L966 - mismatched parameter going to ibrfets.p
*******************************************************************************/

assign
     s-lineno     = b-poelb.lineno
     s-qtyunavail = b-poelb.qtyunavail
     s-reasunavty = b-poelb.reasunavty.

if icsw.serlottype = "s" then do:
 /* serialno auto create for service repair - 04/09/08 */
 {w-icsp.i b-poelb.shipprod no-lock}
 if avail icsp and icsp.prodcat begins "s" then do:    
  def new shared var v-poelbid as recid            no-undo.
  def new shared var v-poeiid  as recid            no-undo.
  assign v-poelbid = recid(b-poelb).
  if avail poel and poel.transtype = "do" then     
   run zsdiserialno_updt.p(input "poezb",           
                            input "do").            
  else                                             
   run zsdiserialno_updt.p(input "poezb",           
                           input " ").             
 end.
    
 {p-nosn.i &ordertype = ""p""
           &orderno   = g-pono
           &ordersuf  = g-posuf
           &com       = "/*"}
end.
else if icsw.serlottype = "l" then do:
 {p-nolots.i &seqno     = 0
             &ordertype = ""p""
             &orderno   = g-pono
             &ordersuf  = g-posuf
             &com       = "/*"}
end.

assign
     v-eachprice = poel.price /  {unitconv.gas &decconv = poel.unitconv}
     v-allprice  = (poel.price / {unitconv.gas &decconv = poel.unitconv}) *
                   s-stkqtyrcv
     v-nosnlots  = round(s-stkqtyrcv - v-noassn,2).

if avail icsp and icsp.prodcat begins "s" and icsw.serlottype = "s" then    
 assign i = 1. 
else  
if icsw.serlottype = "s" then
    /*tb# 23693  aa; 01/15/98 "New" icets.p */
    run sdirfets.p(g-whse,
                b-poelb.shipprod,
                "po",
                g-pono,
                g-posuf,
                b-poelb.lineno,
                0,
                v-returnfl,
                s-stkqtyrcv,
                v-nosnlots,
                poel.stkqtyord,
                v-eachprice,
                g-potype,
                "",
                output s-stkqtyrcv,
                output v-errfl,
                input-output s-qtyunavail).
else if icsw.serlottype = "l" then
    /*tb# 23693  aa; 01/15/98 "New" icetl.p */
    run sdirfetl.p(g-whse,
                b-poelb.shipprod,
                "po",
                g-pono,
                g-posuf,
                b-poelb.lineno,
                0,
                v-returnfl,
                s-stkqtyrcv,
                v-nosnlots,
                poel.stkqtyord,
                v-eachprice,
                g-potype,
                output s-stkqtyrcv,
                output v-errfl,
                input-output s-qtyunavail).

if toggle-sw = no then 
   put screen row 13 column 1 color messages
     "F6Opt F7LD F8vp F9Sch F10U".  
else 
   put screen row 13 column 1 color messages
     "F6Opt F7LD F8VP F9Sch F10U".  

if v-errfl = "s" then do:
    assign
        s-qtyrcv       = round(s-stkqtyrcv /
                         {unitconv.gas &decconv = b-poelb.unitconv},2).
    display s-qtyrcv @ tpoehb.qtyrcv with frame f-poehb. 

    {&comupdt}
    assign
         b-poelb.qtyrcv    = s-qtyrcv
         b-poelb.stkqtyrcv = s-stkqtyrcv.
    /{&compupdt}* */
end.

assign
      b-poelb.qtyunavail = s-qtyunavail.



