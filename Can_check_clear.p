/* get the file and put it in o_file */

def var i_file   as c    format "x(40)".
def var a_file   as c    format "x(40)".   
def var b_file   as c    format "x(40)".   
def var y-year   as c    format "x(4)".
def var x-month  as c    format "x(2)".
def var x-day    as c    format "x(2)".    
def var x-year   as c    format "x(2)".
def var x-date   as c    format "x(6)". 
def var y-date   as c    format "x(6)".
def var i-field1 as c.
def var i-field2 as c.
def var i-field3 as c.
def var i-field4 as c.
def var i-field5 as c.
def var i-field6 as c.
def var i-field7 as c.
def var i-rectype as c.
def var i-acct   as c    format "x(12)".   /* account # */
def var i-type   as c    format "x(1)".    /* record type (R=Reconciliation */
def var i-check  as c    format "x(10)".   /* check # */
def var i-amount as c    format "x(12)".   /* check amount */
def var i-date   as c    format "x(6)".    /* date */   
def var i-rec    as c    format "x(80)".
def var w-acct   as c    format "x(12)".
def var w-type   as c    format "x(1)".
def var w-check  as i    format "9999999999".
def var w-amount as de   format ">>>>>>>>9.99".
def var w-date   as date format "99/99/99".
def var linecnt  as i    format "999".  
def var h-page   as i    format "999".
def var h-date   as date format "99/99/99".
def var o-msg    as c    format "x(30)".    

def var p-bankno   like cret.bankno  init 15 no-undo.
def var p-cleardt  like cret.cleardt  no-undo.   
def var g-operinit like cret.operinit no-undo.  
def var g-cono     like cret.cono init 1 no-undo.

def var clear-cnt as i format ">>>>9".
def var clear-amt like cret.amount.

assign i_file = "/usr/tmp/boa_Can_recon.txt".
assign y-year = string(year(TODAY),"9999").    
assign x-month = string(month(TODAY),"99").
assign x-day   = string(day(TODAY),"99").
assign x-year  = substr(y-year,3,2).
assign h-date  = TODAY. 
assign h-page  = 1. 
assign w-date = TODAY - 1.  
assign p-cleardt = w-date.

form 
    h-date
    "A/P Canadian Cleared Checks Report"   at 20    
    "Page: "                            at 60
    h-page                              at 66 format "zzz9"
    skip(1)
    "Operator: "                        at 1
    g-operinit
    "Clear Date: "                      at 25
    w-date
    "Bank No: "                         at 50
    p-bankno
    skip(2)
    "Check No."                         at 1
    "Amount"                            at 20
    "Message"                           at 50   
    "Cono"                              at 70
    skip
    "-----------"
    "---------------"                   at 16
    "------------------------------"    at 36
    "----"                              at 70
    skip
    with frame f-header no-box no-labels down width 80.
 
form
    w-check             at 1
    w-amount            at 16
    o-msg               at 36
    cret.cono           at 70
    with frame f-dtl no-box no-labels down width 80.

form
    skip(2)
    "Total $ Cleared:"         at  1
    clear-amt                  at 18
    "Total Checks Reconciled:" at 35    
    clear-cnt                  at 60
    with frame f-total no-box no-labels down width 80.




/*make a backup of the original recon file */
assign b_file  = "/usr/tmp/boa_Can_recon" + "_" + x-month +
                                                  x-day   +
                                                  x-year  + ".bkup".

unix cp value(i_file) value(b_file).    

/*prep the audit file*/ 
assign a_file = "/usr/tmp/boa_Can_recon" + "_" + x-month + 
                                                 x-day   +
                                                 x-year  + ".aud".
assign p-bankno  = 15.   

run "print-header".

def stream aud.
input from value(i_file).
output stream aud to value(a_file).
repeat:
  import delimiter ","
    i-rectype i-field1 i-field2 i-field3 i-field4 i-field5 i-field6 i-field7.
  if i-rectype = "02" then
    do:
    assign x-date = i-field4.   
    assign y-date = substr(x-date,3,2) +   /* month */  
                    substr(x-date,5,2) +   /* day   */
                    substr(x-date,1,2).
    assign w-date = date(y-date).
  end.
  if i-rectype = "16" then
    do:
    assign w-check  = int(i-field5)
           w-amount = dec(i-field2) / 100.
    find first cret where cret.cono      = g-cono and    
                          cret.bankno    = p-bankno and
                          cret.ckrectype = 1 and
                          cret.checkno   = w-check and
                          cret.amount    = w-amount
                          no-error.
    if not avail cret then    
      find first cret where cret.cono    = g-cono + 79 and    
                            cret.bankno    = p-bankno and
                            cret.ckrectype = 1 and
                            cret.checkno   = w-check and
                            cret.amount    = w-amount
                            no-error.
    if avail cret and cret.clearfl = yes and 
                      cret.cleardt <> ? and
                      cret.statustype = no then next. /*Check already cleared*/
    if not avail cret then
      do:
      assign o-msg = "Check Not Found in either Company".   
      display w-check 
              w-amount
              o-msg with frame f-dtl.
      down with frame f-dtl.      
      next.
    end.
    else
      do:
      export stream aud delimiter "~011"
        cret.cono
        cret.jrnlno cret.checkno cret.clearfl cret.cleardt cret.statustype
        cret.operinit cret.transdt cret.transtm.   
      
      assign
        cret.clearfl = yes
        cret.cleardt = w-date
        cret.statustype = if cret.clearfl then false else true
        cret.operinit = g-operinit
        cret.transdt = TODAY
        cret.transtm = substr(string(TIME,"HH:MM"),1,2) +
                       substr(string(TIME,"HH:MM"),4,2).
      
      assign clear-cnt = clear-cnt + 1
             clear-amt   = clear-amt   + cret.amount.
      assign o-msg = "".
      display cret.cono
              w-check 
              w-amount
              o-msg with frame f-dtl.
      down with frame f-dtl.          
      assign linecnt = linecnt + 1.
      if linecnt > 55 then
        run "print-header".
    end. /* avail cret */   
  end. /* i-rectype = 16 (detail) */
end. /* repeat */

display clear-amt
        clear-cnt 
        with frame f-total.

procedure print-header:
  assign linecnt = 4.
  display h-date
          h-page
          g-operinit
          p-cleardt
          p-bankno
          with frame f-header.
  down with frame f-header.
  assign h-page = h-page + 1.
end.