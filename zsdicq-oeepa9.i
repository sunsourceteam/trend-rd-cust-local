/* a-oeepa9.i */
/*si**** Custom ****************************************************************
  CLIENT  : 80281 SDI Operating Partners, Inc
  JOB     : SDI06501 - Change to show Canadian Dollar Amts
  ANCHOR  : a-oeepa1.i
  VERSION : 8.0.003
  PURPOSE : 
  CHANGES :
    SI01 (Client originally developed order acknowledgement)
    SI02 03/04/00; ucv/usi; SDI06501 - Change to show Canadian Dollar Amts
******************************************************************************//* a-oeepa9.i 1.3 11/11/92 */
/*h*****************************************************************************
  INCLUDE      : a-oeepa9.i
  DESCRIPTION  :
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    04/03/92 pap; TB# 6219 Line DO - add direct order handling
    11/11/92 mms; TB# 8561 Display product surcharge label from ICAO
    04/17/97 JH; ITB# 1     Center "acknowledgement", remove "upc Vendor",
                            change "order #" to "Invoce #", change
                            "correspondence to" to "remit to", change 
                            "shipped date" to "expected ship date",
                            remove "upc item #.
*******************************************************************************/

if v-headingfl = yes then assign
    /*SI02*/
    {1}litattn = if oeehb.divno = 40 then "    Attn: " + oeehb.placedby else ""  
    /*SI02 end*/

    {1}cancel = if oeehb.stagecd = 9 then "*** C A N C E L L E D ***"
                else ""
/*  {1}lit1b  = "UPC Vendor   Invoice Date     Order #" */
    {1}lit1b  = "                              Quote #  "  /* (ITB# 1) */
    {1}lit2a  = if can-do("ra,rm",oeehb.transtype) then
                    "===================================================="
                else if oeehb.transtype = "cr" then
                    "============================================="
                else ""
    {1}lit3a  = "  Cust #:"
    {1}lit3b  = "PO Date   PO #                   Page #"
    {1}lit6a  = "Bill To:"
    {1}lit6b  = "Correspondence To  "
    {1}lit6bb = "(ONLY)"          
    {1}lit8a  = if g-country = "ca" then "PST Lic#:" else ""
    {1}lit9a  = if g-country = "ca" then "GST Reg#:" else ""
    {1}lit11a = "Ship To:"
    {1}lit11b = "Instructions"
    {1}lit12a = "Ship Point"
    {1}lit12b = "Via"
/*  {1}lit12c = "Shipped"        (ITB# 1)  */
    {1}lit12c = "To Ship By"    /*  (ITB# 1)  */
    {1}lit12d = "Terms"
    {1}lit14a =
"    Product                            Quantity     Quantity     Quantity     "
    {1}lit14b =
"Qty.     Unit       Price                      Amount    "
    {1}lit15a =
"Ln# And Description                    Ordered        B.O.       Shipped      "
    {1}lit15b =
" UM      Price       UM                        (Net)     "
    {1}lit16a =
"------------------------------------------------------------------------------"
    {1}lit16b =
"-------------------------------------------------------".
   
assign
    {1}lit1aa = /*"~033(10U~033(s0p12h0s3b4102T"*/
                if v-headingfl = yes then "Document: " else "" 
    {1}lit1a  = if oeehb.transtype = "so" then
                    "COMMISSION QUOTE"
                else if oeehb.transtype = "cr" then
                    "Commission Correction"
                else if oeehb.transtype = "ra" then
                    "Commission Received On Account"
                else if oeehb.transtype = "qu" then
                    "COMMISSION QUOTE"
                else "COMMISSION QUOTE"
     {1}lit1a = if oeehb.transtype = "bl" then
                    "Commission - Blanket Order"
                else if oeehb.transtype = "do" then
                    "Commission - Direct Order"
                else if oeehb.transtype = "fo" then
                    "Commission - Future Order"
                else if oeehb.transtype = "st" then
                    "Commission - Standing Order"
                else if oeehb.transtype = "br" and oeehb.stagecd <> 0 then
                    "Commission - Blanket Release"
                else if oeehb.transtype = "br" and oeehb.stagecd = 0 then
                    "Commission - Blanket Release (Entered)"
                else {1}lit1a
    {1}lit40a = "Lines      "
    {1}lit40c = "Qty Shipped Total"   
    {1}lit40d =
              /*  
                if can-do("bl,br",oeehb.transtype) and
                    oeehb.lumpbillfl = yes then
                "Amount Billed     " else
              */
                "Total             "
    {1}lit41a = "Order Discount    "
    {1}lit42a = "Other Discount    "
    {1}lit43a = "Core Charge       "
    {1}lit44a = v-icdatclabel
    {1}lit45a = "Taxes             "
    {1}lit46a = "Downpayment       "
    {1}lit46b = "Payment           "
    {1}lit47a = "Commission Total  "
    {1}lit47b = "Payment           "
    {1}lit48a = "Continued"
    {1}lit49a = "G.S.T.            "
    {1}lit50a = "P.S.T.            "
    {1}lit53a = "Restock Amount    ".

