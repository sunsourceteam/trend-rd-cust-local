/*******************************************************************************
  PROCEDURE      : x-hhet.i based on x-hhep.i - (Handheld only) 
  DESCRIPTION    : 
  AUTHOR         : Sunsource
  DATE WRITTEN   : 04/01/07
  CHANGES MADE   : 
*******************************************************************************/

/{&bXX_title}*/
 display v-titlelit with frame f-xx.
/{&bXX_title}*/

/{&temptable}*/
/{&temptable}*/

/{&user_vars}*/
def var trcptFlag as logical no-undo.
def var curlyBracket as char initial "~{" no-undo.
def var tab as char initial "~t" no-undo.
def var v-specialWO as logical no-undo.
on ctrl-a anywhere do:
  apply 311.
  /* on ctrl-a tab. */
end.


/*d Set title for line frame, and number of iterations in line frame */
assign
  v-length =   1
  v-scrollfl = yes.
def var v-lbllit as character format "x(11)" no-undo init "Order#   : ".
form 
   v-whse                at 1   label "Whse     "
   {f-helpRF.i}
   v-putawayinit         at 1   label "Operator "
   v-picktype            at 1   label "Pick Type"
   help "  O - Order    B - Batch  "  
   v-selecttype          at 1   label "Item Type"
   help " A)ll   K)its Only  S)tock" 
   v-lbllit              at 1  no-label
   v-ordernum            at 12 no-label 
   {f-helpRF.i} 
   with frame f-hhet-h width 26 row 1 side-labels overlay
   title "PickPack Select".   

form
  v-vasection       at 1 label "Va Seq. #"
  with frame f-vasection width 24 row 5 side-labels overlay.

/***
form 
   "Whse:"               at 1  
   v-whse                at 7 
   "Operator:"           at 12 
   v-putawayinit         at 22 
   " "                   at 1
   "Part#"               at 1  dcolor 2
   v-shipprod            at 2      
      Help "Verify Product Putaway"
   t-shipprod      at 2
   "Description"         at 1 dcolor 2
   t-proddesc1     at 2
   t-proddesc2     at 2 
   "Putaway Qty:"        at 1  dcolor 2
   " "                   at 13 
   t-stkqtyputaway at 14 
   v-binloc              at 14       
         Help "Verify Bin Putaway"
   "Binloc1:    "        at 1  dcolor 2
   t-binloc1       at 14
   "Binloc2:    "        at 1  dcolor 2
   t-binloc2       at 14
   " "                   at 1
   x-spce          at 26
   with frame f-hhetold width 26 row 1 no-box scroll 1
   no-hide no-labels overlay v-length down.   
***/

form 
   "Whse:"                at 1  
   v-whse                 at 7 
   "Operator:"            at 12 
   v-putawayinit          at 22 
/*
   "Btch:"                at 1
   l-batchno              at 7
   "Seq:"                 at 18
   t-seqno    format ">>9"     at 23
*/
   "Pickbin:"             at 1
   v-binloc               at 14       
         Help "Verify Bin Putaway"
   "Binloc1:    "         at 1  dcolor 2
   t-binloc1              at 14
   "Binloc2:    "         at 1  dcolor 2
   t-binloc2              at 14
   "Part#"                at 1  dcolor 2
   t-type     format "x(9)"   at 8
   v-shipprod             at 2      
      Help "Verify Product Putaway"
   t-shipprod             at 2
   t-proddesc1            at 2
   /* t-proddesc2         at 2 */
   "Qty:"                 at 1  dcolor 2
   t-stkqtyputaway        at 6
   /* "Qty Picked:" */
   v-qtypicked            at 15    /* smafsmaf */    
   /*
   "Order:"               at 1  dcolor 2
   v-ordernum             at 15
   t-orderno              at 8
   "Ln:"                  at 21
   t-lineno  format ">9"  at 24

   */
   "Order:"               at 1 dcolor 2
   v-ordernum             at 15
   t-orderno              at 1
   "Seq:"                 at 13
   t-seqno    format ">>>9"     at 18
   "Ln:"                  at 1
   t-lineno  format ">>9" at 4
   "Btch:"               at 12
   l-batchno             at 17
   x-spce          at 26 
   with frame f-hhet width 26 row 1 no-box scroll 1
   no-hide no-labels overlay v-length down.   

form 
   " Status:    "        at 1  dcolor 2
   v-listlit             at 14
   "Binloc1:    "        at 1  dcolor 2
   t-binloc1       at 14
   "Binloc2:    "        at 1  dcolor 2
   t-binloc2       at 14
   with frame f-hhetb width 26 row 10 no-box 
   no-hide no-labels overlay.   


   form
   "WARNING - MULTIPLE lines  " at 1   dcolor 2                
   "within pick list contain  " at 1   dcolor 2
   "this product.             " at 1   dcolor 2
   "  "                         at 1
   "Product:"                   at 1   dcolor 2
   t-shipprod                   at 2
   "  "
   "Tot PickQty:"               at 2  dcolor 2
   t-put.totqtyprod             at 14 format ">>>>>>9.99-"
   "Tot Lines:  "               at 2  dcolor 2
   t-put.totcntprod             at 14 format ">>>>>>9"
   "BinLoc1:    "               at 2  dcolor 2
   t-binloc1                    at 14
   h-binloc                     at 14    
   /* " Hit Any Key To Continue. " at 1 dcolor 2 */
   with frame f-hhetpop width 26 no-box 
   no-labels overlay.   



/{&user_vars}*/

/{&B-zz_browse_events}*/
/{&B-zz_browse_events}*/

/{&user_popup_proc}*/
/{&user_popup_proc}*/

/{&user_display1}*/
/{&user_display1}*/

/{&user_display2}*/
  put screen row 13 column 1 color messages "F6Ex F7Bin2 F8Clos F9Srch". 
/{&user_display2}*/


/{&user_display3}*/
  put screen row 13 column 1 color messages "                          ".
/{&user_display3}*/


/{&user_Query}*/ 
  /*o Process XXEX screen */  
  {x-xxexcustom3.i &file         = "t-put"
                    /* "t-put" "zsdiput"   tmp-zsdi summary */
                  &status       = "If not v-bufferedsearch then"
                  &frame        = "f-hhet"             
                  &find         = "hhet.lfi" /* "hhep.lfi"  temp table name */                   &beforechoose = "if v-scrollfl and
                                      not v-scrollmode 
                                    then v-scrollfl = false."
                                     

                  &snchoosecond = "and not v-searchfl" 
                  &field        = "x-spce" 
                  &internalfield  = "null" 
                  &go-on        = 'ctrl-o'      
                  &sneditsecond = " or v-searchfl "
                  &searcher     = "hhet.sch" 
                  &searchat     = "at 1"
                  &usefile      = "hhet.lfu" 
                  &display      = "hhet.lds"  /* need to display tmp or find */                  &delete       = "hhet.del"
                  &delfl        = "*" 
                  &keymove      = "hhet.key"
                  &gofile       = "hhet.gof"
                  &edit         = "hhet.led"}          
                  
/{&user_Query}*/ 

/{&user_aftmain1}*/ 
if {k-jump.i} then do:
  run RFjump.
end.
/{&user_aftmain1}*/ 

/{&user_procedures}*/ 
procedure RFjump:
  run zsdijumprf.p.
end.
/{&user_procedures}*/ 


/{&RcptSelect}*/
/* There is a better, one line way to delete temp-tables, EMPTY */
empty temp-table tmp-rcpt.
empty temp-table t-put.
/* F11 Re-entry */
find first zsdipckdtl  use-index k-userid where
           zsdipckdtl.cono = g-cono and     
           zsdipckdtl.user2 = g-operinits 
           no-lock no-error.               

if avail zsdipckdtl and num-entries(zsdipckdtl.xuser3,"~011") ge 3 then do:
  assign v-whse =   entry(1,zsdipckdtl.xuser3,"~011")
         v-picktype = entry(2,zsdipckdtl.xuser3,"~011")
         v-selecttype = entry(3,zsdipckdtl.xuser3,"~011").
  assign g-whse = v-whse.                              

  if v-picktype = "b" then do:  
    for each zsdipckdtl use-index k-userid
                        where zsdipckdtl.cono = g-cono and     
                              zsdipckdtl.user2 = g-operinits and
                              zsdipckdtl.whse = v-whse no-lock
                              break by zsdipckdtl.pickid:                              if first-of(zsdipckdtl.pickid) then do:    
        find tmp-rcpt where tmp-rcpt.receiptid = zsdipckdtl.pickid and
             tmp-rcpt.receiptsuf = 0 no-lock no-error.
        if not avail tmp-rcpt then do:
          /* message "create 2 with ordType" zsdipckdtl.ordertype. pause. */
          create tmp-rcpt.
          trcptFlag = yes.
          tmp-rcpt.receiptid = zsdipckdtl.pickid.
          tmp-rcpt.receiptsuf = 0.
          tmp-rcpt.ordertype = zsdipckdtl.ordertype.
        end.
      end.
    end.
  end.
  else if v-picktype = "o" then do:  
    for each zsdipckdtl use-index k-userid
                        where zsdipckdtl.cono = g-cono and     
                              zsdipckdtl.user2 = g-operinits and
                              zsdipckdtl.whse  = v-whse no-lock
                              break by zsdipckdtl.orderno
                                    by zsdipckdtl.ordersuf:                          if first-of(zsdipckdtl.orderno) then do:    
        if ordType = "f" then
          find tmp-rcpt where tmp-rcpt.receiptid = zsdipckdtl.orderno and
             tmp-rcpt.receiptsuf = zsdipckdtl.ordersuf and 
             tmp-rcpt.section = zsdipckdtl.lineno no-lock no-error.
        else
          find tmp-rcpt where tmp-rcpt.receiptid = zsdipckdtl.orderno and
             tmp-rcpt.receiptsuf = zsdipckdtl.ordersuf
             no-lock no-error.
        if not avail tmp-rcpt then do:
          /* message "create 3 with oType=" zsdipckdtl.ordertype. pause. */
          create tmp-rcpt.
          trcptFlag = yes.
          tmp-rcpt.receiptid  = zsdipckdtl.orderno.
          tmp-rcpt.receiptsuf = zsdipckdtl.ordersuf.
          tmp-rcpt.ordertype  = zsdipckdtl.ordertype.
          tmp-rcpt.section    = zsdipckdtl.lineno.
        end.
      end.
    end.
  end.

  find first tmp-rcpt no-lock no-error. 
  if avail tmp-rcpt then do:            
    v-orderno = tmp-rcpt.receiptid.   
    loaded-sw = true.
    g-whse = v-whse.
  end.  
  assign v-reload = true.
  run setTempRecords(input v-picktype, v-selecttype). 
  return.
  end.   
else do: 
  trcptFlag = no.
  v-reload = false.
  rcpt[1] = "".
  rcpt[2] = "".
  rcpt[3] = "".
  v-ordernum = "".


  lockPickType = no.
  lockedPick = "".

  if h-firsttime then
    assign v-orderno = 0
           v-ordersuf = 0
           v-ordernum = ""
           v-picktype = ""
           v-selecttype = "A"
           v-lbllit = "Order#   : ".
  display   
    v-whse          
    v-putawayinit   
    v-picktype
    v-selecttype
    v-lbllit
    v-ordernum
  with frame f-hhet-h.
 
  run displaytmprcpt(input "hhet", v-ordernum).
  assign     v-completescroll = false
             v-scrollfl = true
             v-scrollmode = false.
 

/**  This already is called repeatedly until loaded-sw = true. **/
  assign h-binsort = "".

  if not h-firsttime then
    if v-whse <> "" and
       v-picktype <> "" and 
       v-selecttype <> "" then do:
      v-ordernum = "".
      next-prompt  v-ordernum with frame f-hhet-h.     
      end.

  assign h-firsttime = false.

   update v-whse /* when v-whse = "" */ /*  when not can-find(first tmp-rcpt) */
                when not trcptFlag
    v-picktype /* when (v-picktype ne "o" and v-picktype ne "b") */
    v-selecttype 
    v-ordernum
  with frame f-hhet-h
  editing:
    readkey.
  
    if frame-field = "v-ordernum" and input v-ordernum:cursor-offset = 1 and
       keylabel(lastkey) = curlyBracket then do:
       assign v-specialWO = true.
      end.  
    else
    if frame-field = "v-ordernum" and input v-ordernum:cursor-offset = 1 and
       keylabel(lastkey) ne curlyBracket then
       assign v-specialWO = false.
  
    if frame-field = "v-whse"
      and ({k-accept.i} or {k-return.i} or keylabel(lastkey) = "cursor-down")
    then do:
      if v-whse ne "" /* and can-find(first tmp-rcpt) */
       and trcptFlag then do:
        message "Default Whse Mismatch". /* Warehouse set". */
        bell. bell. bell.
        display v-whse with frame f-hhet-h.
        next-prompt v-ordernum with frame f-hhet-h.
        next.
      end.
      if can-find(first icsd where icsd.cono = g-cono and icsd.whse =
        input v-whse) then
        assign v-whse = input v-whse.
      else do:
        message "Invalid Warehouse" v-whse.
        bell.
        next-prompt v-whse with frame f-hhet-h.
        next. 
      end.
      message "".
    end.
 
    if frame-field = "v-picktype" and 
    (keylabel(lastkey) = "cursor-down" or {k-after.i})
    then do:
      if lockPickType then do:
        bell.
        message "Pick Type loaded. No Changes".
        v-picktype = lockedPick.
        display v-picktype with frame f-hhet-h.
      end.
      if input v-picktype ne "o" and input v-picktype ne "b" then do:
        message "Pick Type MUST be O or B".
        next-prompt v-picktype with frame f-hhet-h.
        next.
      end.
      else
      if input v-picktype = "o" then do:
        assign v-lbllit = "Order#   : ".
        display v-lbllit with frame f-hhet-h.
      end.
      else
      if input v-picktype = "b" then do:
        assign v-lbllit = "Batch#   : ".
        display v-lbllit with frame f-hhet-h.
      end.
  
      /* leave update else goes to v-whse */
    end.
  
    if frame-field = "v-selecttype" and 
    (keylabel(lastkey) = "cursor-down" or {k-after.i})
    then do:
      if lockPickType then do:
        bell.
       message "Pick Type loaded. No Changes".
        display v-selecttype with frame f-hhet-h.
      end.
      if input v-selecttype ne "k" and input v-selecttype ne "a"
      and input v-selecttype ne "s" then do:
        message "Select A, K, or S".
        next-prompt v-selecttype with frame f-hhet-h.
        next.
      end.
      else message "".
    end.
    
  
    if frame-field = "v-ordernum" and input v-ordernum = "" 
      and ({k-accept.i} or {k-return.i} or keylabel(lastkey) = "cursor-down")
    then do:
      v-ordernum = input v-ordernum.
    end.
    else
    if frame-field = "v-ordernum"
      and ({k-accept.i} or {k-return.i} or keylabel(lastkey) = "cursor-down")
    then do:
      /* message "lastkey" keylabel(lastkey). pause. */
      ordType = "O".
      v-ordernum = input v-ordernum.
  
      if  v-ordernum begins "f" /* va */
       or v-ordernum begins "o" 
       or v-ordernum begins "t"
       or v-ordernum begins "w"
       or v-ordernum begins curlyBracket then do:
        ordType = substring(v-ordernum, 1, 1).
        dotSpot = 0.
        if ordType = curlyBracket then do:
          /* message "curlyBracket". pause. */
          ordType = "W".
          dotSpot = -1.
          v-orderno = int(substring(v-ordernum, 2, length(v-ordernum) - 3)).
          /* should find v-ordersuf in more logical fashion */
          v-ordersuf = int(substring(v-ordernum, length(v-ordernum) - 1)).
          /* message "ord" v-orderno "suf" v-ordersuf. pause. */
        end.
        if dotSpot = 0 then dotSpot = index(v-ordernum, ".").
        if dotSpot = 0 then dotSpot = index(v-ordernum, "-").
        if dotSpot > 0 then do:
          v-orderno = int(substring(v-ordernum, 2, dotSpot - 2)).
          v-ordersuf = int(substring(v-ordernum, dotSpot + 1, 2)).
          v-ordernum = string(v-orderno,">>>>>>>>9") + "-"
                     + string(v-ordersuf,"99").
        end.
      end.
      else do:
        dotSpot = index(v-ordernum, ".").
        if dotSpot = 0 then dotSpot = index(v-ordernum, "-").
        if dotSpot > 0 then do:
          v-orderno = int(substring(v-ordernum, 1, dotSpot - 1)).
          v-ordersuf = int(substring(v-ordernum, dotSpot + 1, 2)).
        end.
      end.
      
      v-picktype = input v-picktype.
      if v-picktype ne "o" and v-picktype ne "b" then do:
        v-picktype = "".
        next-prompt v-picktype with frame f-hhet-h.
        next.
      end.
      if v-picktype = "o" then do:
        if dotSpot = 0 then do:
          message "Order format is xxx-99".
          errFlag = true.
        end.
        else do:
          /* incompatible to find oeeh when using warehouse transfer */
          find first zsdipckdtl use-index k-ordix where
             zsdipckdtl.cono = g-cono and
             zsdipckdtl.ordertype = ordType and
             zsdipckdtl.orderno = v-orderno and
             zsdipckdtl.ordersuf = v-ordersuf and 
             zsdipckdtl.completefl <> yes no-lock no-error.
          if not avail zsdipckdtl then
            find first zsdipckdtl use-index k-ordix where
                       zsdipckdtl.cono = g-cono and
                       zsdipckdtl.ordertype = ordType and    
                       zsdipckdtl.orderno = v-orderno and
                       zsdipckdtl.ordersuf = v-ordersuf 
             no-lock no-error.
          if avail zsdipckdtl then do:
            if v-whse ne zsdipckdtl.whse then do:
              message "Warehouse mismatch".
              errFlag = true.
            end.
          end.
          if not avail zsdipckdtl then do:
            message "Order not prepped to pick".
            errFlag = true.
          end.
          else do:
            if zsdipckdtl.completefl = yes then do:
              message "Order completed".
              errFlag = true.
            end.
            if avail zsdipckdtl and zsdipckdtl.whse <> v-whse then do:
              message "Whse mismatch " + zsdipckdtl.whse.
              errFlag = true.
            end.
            if zsdipckdtl.ordertype = "o" then do:    
              find first oeeh use-index k-oeeh where 
                         oeeh.cono = g-cono and 
                         oeeh.orderno = v-orderno and
                         oeeh.whse = v-whse no-lock no-error.
              if not avail oeeh then do:
                message "Order " v-orderno " not found".
                errFlag = true.
              end.
            end.
          end. 
        end.
        /* dkt hang error */
        if errFlag then do:
           next-prompt v-ordernum with frame f-hhet-h.
           assign errFlag = false.
           next.
       
         end.
      
      
        /* check for use lock */
        find first zsdipckdtl use-index k-ordix where
          zsdipckdtl.cono = g-cono and     
          zsdipckdtl.whse = v-whse and           
          zsdipckdtl.ordertype = ordType and
          zsdipckdtl.orderno =  v-orderno   and
          zsdipckdtl.ordersuf = v-ordersuf   and
          /*
          zsdipckdt.pickid  = (if v-picktype = "b" then integer(v-ordernum)
                                else zsdipckdtl.pickid) and
          */
          (zsdipckdtl.user2 = g-operinits or zsdipckdtl.user2 = "")
          no-lock no-error.                                       
 
          if not avail zsdipckdtl then do:
            errFlag = yes.                
            find first zsdipckdtl use-index k-ordix where
               zsdipckdtl.cono = g-cono and     
               zsdipckdtl.ordertype = ordType and
               zsdipckdtl.whse = v-whse and                            
               zsdipckdtl.orderno = v-orderno   and
               zsdipckdtl.ordersuf = v-ordersuf   
/*
               zsdipckdtl.pickid  = (if v-picktype = "b" then 
                                       integer(v-ordernum)
                                    else zsdipckdtl.pickid) 
*/
              no-lock no-error.                                       
            if avail zsdipckdtl then
              message substring(string(zsdipckdtl.pickseq,"999999999"),6,4) +
               "(Seq#) in use by " +  zsdipckdtl.user2. 
            else
              message "Record in use".      
          end.                            
        /* end check use lock */

      end. /* end v-picktype o */
  
    if v-picktype = "b" then do:
      /**
      dotSpot = index(v-ordernum, ".").
      if dotSpot = 0 then dotSpot = index(v-ordernum, "-").
      if dotSpot > 0 then do:
        v-ordernum = substring(v-ordernum, 1, dotSpot - 1).
      end.
      **/
      if v-ordernum begins "t" then 
        v-orderno = int(substring(input v-ordernum, 2)).
      /* else
        v-orderno = int(input v-ordernum). */
      else do:
        if not (v-ordernum >= "0" and v-ordernum <= "999999999999999999999999")
        then do:
        message "Please enter a batch no".
        /* errFlag = true.
           release oeeh.   */
        bell.
        next-prompt v-ordernum with frame f-hhet-h.
        next.
        end.
        v-orderno = int(input v-ordernum).
      end.
      find first zsdipckdtl  use-index xrpckix where  
                 zsdipckdtl.cono = g-cono and
                 zsdipckdtl.whse = v-whse and 
                 zsdipckdtl.pickid = v-orderno
                 no-lock no-error.
      if not avail zsdipckdtl then do:
        message "Batch not prepped to pick".
        errFlag = true.
      end.
      else do:
        find first zsdipckdtl use-index xrpckix where 
                   zsdipckdtl.cono = g-cono and
                   zsdipckdtl.whse = v-whse and
                   zsdipckdtl.pickid = v-orderno and
                   zsdipckdtl.completefl = no no-lock no-error.
        if not avail zsdipckdtl then do:
          errFlag = yes.
          message "Batch completed".
        end.
        else do:
          find first zsdipckdtl where zsdipckdtl.cono = g-cono and
            zsdipckdtl.whse = v-whse and zsdipckdtl.pickid = v-orderno
          and zsdipckdtl.completefl = no and
          (zsdipckdtl.user2 ne "" and zsdipckdtl.user2 ne g-operinits)
          no-lock no-error.
          if avail zsdipckdtl then do:
            errFlag = yes.
            message "Locked by " zsdipckdtl.user2.
          end.
        end.
        if avail zsdipckdtl then
          assign ordType = zsdipckdtl.ordertype.
      end.
    end. /* B */
    else do:
      /* verify order */
      if ordType = "o" then do:
        find first oeeh where oeeh.cono = g-cono and
        oeeh.orderno = v-orderno and
        oeeh.ordersuf = v-ordersuf no-lock no-error.
        if avail oeeh then do:
          if v-whse ne oeeh.whse then do:
            message "Whse mismatch".
            errFlag = yes.
          end.
        end.
        else do:
          message "Order not found".
          errFlag = yes.
        end. 
      end.
      else if ordType = "t" then do:
        find first wteh where 
        wteh.wtno = v-orderno and
        wteh.wtsuf = v-ordersuf no-lock no-error.
        if avail wteh then do:
          if v-whse ne wteh.shipfmwhse then do:
            message "Whse mismatch".
            errFlag = yes.
          end.
        end.
        else do:
          message "Order not found".
          errFlag = yes.
        end.
      end. /* whse transfer */
    end.
    if errFlag then do:
      errFlag = false.
      release oeeh.
      bell.
      next-prompt v-ordernum with frame f-hhet-h.
      next.
    end.
    v-picktype = input v-picktype.
    v-ordernum = input v-ordernum.
    lockPickType = yes.
    lockedPick     = v-picktype.
    message "".
    g-whse = v-whse.                              

    v-vasection = 0.
    if ordType = "f" then 
      update v-vasection with frame f-vasection.
    else v-vasection = 0.

    if ordType = "f" then
    find tmp-rcpt where tmp-rcpt.receiptid = v-orderno and
       tmp-rcpt.receiptsuf = v-ordersuf and
       tmp-rcpt.section = v-vasection
      no-lock no-error.
    else
       find tmp-rcpt where tmp-rcpt.receiptid = v-orderno and
       tmp-rcpt.receiptsuf = v-ordersuf
      no-lock no-error.
  
    if not avail tmp-rcpt then do:
      /* message "create 1 with oType" ordType. pause. */
      create tmp-rcpt.
      trcptFlag = yes.
      tmp-rcpt.receiptid = v-orderno.
      tmp-rcpt.receiptsuf = v-ordersuf.
      tmp-rcpt.ordertype = ordType.
      tmp-rcpt.section = v-vasection.
      run displaytmprcpt(input "hhet", v-ordernum). 
      assign v-ordernum = "".
      display v-ordernum with frame f-hhet-h.
    end.
    /**
    else do:
    message "already there" tmp-rcpt.ordertype tmp-rcpt.receiptid
      tmp-rcpt.section. 
    pause 3.
    end.
    **/
      if not {k-accept.i} /* or keylabel(lastkey) = "cursor-down") */
      then do:
        next-prompt v-ordernum with frame f-hhet-h.
        next. 
      end.
    end. /* if frame-field = v-orderno */
    if (frame-field = "v-ordernum" and keylabel(lastkey) = curlyBracket) then 
      apply 119.
    else
    if (frame-field = "v-ordernum" and v-specialWO = true and 
        input v-ordernum:cursor-offset = 9 and keylabel(lastkey) = "tab") then 
      apply 46.
    else
    if not (frame-field = "v-ordernum" and keylabel(lastkey) = "tab") then
      apply lastkey.
  end. /* edit loop */

  find first tmp-rcpt no-lock no-error. 
  if avail tmp-rcpt then do:            
    v-orderno = tmp-rcpt.receiptid.   
    loaded-sw = true.
    g-whse = v-whse.
  end.  
  else do:
    bell.
    message "No Receipts found".
  end.
end. /* else re-entry */  
/* per Tami 04/13/2007
  Multiple Receipts
  order by BinLocation1 for all items */

/{&RcptSelect}*/



/{&user_f9proc}*/
readkey pause 0.
/{&user_f9proc}*/

/{&user_f10proc}*/
/{&user_f10proc}*/

/{&user_errormsg}*/
/{&user_errormsg}*/

/{&user_clearmsg}*/
/{&user_clearmsg}*/

/{&user_headdisplay}*/
/{&user_headdisplay}*/

/{&user_reqnotes}*/
/{&user_reqnotes}*/

/{&b-zz_Browse}*/ 
/{&b-zz_Browse}*/ 

/{&srch_Query}*/ 
/{&srch_Query}*/ 
