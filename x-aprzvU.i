&if defined(user_defines1) = 2 &then
 
 
 
 
&endif
 
&if defined(user_defines2) = 2 &then
 
 
 
 
&endif
 
&if defined(user_temptable) = 2 &then
def var b-vendno     like apsv.vendno             no-undo.
def var e-vendno     like apsv.vendno             no-undo.
def var b-shipfm     like apss.shipfmno           no-undo.
def var e-shipfm     like apss.shipfmno           no-undo.
def var b-prodcat    as c  format "x(4)"          no-undo.
def var e-prodcat    as c  format "x(4)"          no-undo.
def var e-product    as c format "x(24)"          no-undo.
def var b-product    as c format "x(24)"          no-undo.
def var b-whse       like poel.whse               no-undo.
def var e-whse       like poel.whse               no-undo.
def var b-rcptdate   like poeh.receiptdt          no-undo.
def var e-rcptdate   like poeh.receiptdt          no-undo.
def var b-buygrp     like oimsp.responsible       no-undo.
def var e-buygrp     like oimsp.responsible       no-undo.
def var b-buyer      like poeh.buyer              no-undo.
def var e-buyer      like poeh.buyer              no-undo.
def var b-type       like poeh.transtype          no-undo.
def var e-type       like poeh.transtype          no-undo.
def var b-ptype      like icsw.pricetype          no-undo.
def var e-ptype      like icsw.pricetype          no-undo.
def var p-format     as c format "x(2)"           no-undo.
def var p-print      as c format "x(1)"           no-undo.
def var p-oan        as logical                   no-undo.
def var p-nonstk     as logical                   no-undo.
def var p-subs       as logical                   no-undo.
def var p-datetype   as c  format "x(1)"          no-undo.
def var p-transit    as i  format "999"           no-undo.
def var p-list       as logical                   no-undo.
def var p-file       as c  format "x(8)"          no-undo.
def var p-parent     as logical                   no-undo.
def var p-lateonly   as logical                   no-undo.
def var zdays        as i  format "999"           no-undo.
def var x-buygrp     as c  format "x(24)"         no-undo.
def var x-buyname    as c  format "x(30)"         no-undo.
def var po-line-count as i  format ">>>>>9"       no-undo.
def var vend-late-lines as i format ">>>>>9"      no-undo.
def var vend-line-cnt   as i format ">>>>>9"      no-undo.
def var order-count   as i  format ">>>>>9"       no-undo.
def var order-value   as de format ">,>>>,>>9.99" no-undo.
def var p-detailprt   as logical                  no-undo.
def var p-regval      as c                        no-undo.
def var s-po          as c  format "x(10)"        no-undo.
def var s-potype      as c  format "x(2)"         no-undo.
def var s-duedate     as date format "99/99/99"   no-undo.
def var s-fstexpdate  as date format "99/99/99"   no-undo.
def var s-expdate     as date format "99/99/99"   no-undo.
def var s-recptdate   as date format "99/99/99"   no-undo.
def var s-poline      as i  format ">>9"          no-undo.
def var s-product     as c  format "x(24)"        no-undo.
def var s-qtyord      as i  format ">>>>>>9"      no-undo.
def var s-qtyrcv      as i  format ">>>>>>9"      no-undo.
def var s-late        as c  format "x(3)"         no-undo.
def var s-line-cost   as i  format ">>>>>>9.99"   no-undo.
def var x-space       as c  format "x(1)"         no-undo.
def var s-buyer       as c  format "x(4)"         no-undo.
def var s-buyernm     as c  format "x(24)"        no-undo.
def var s-responsible as c  format "x(24)"        no-undo.
def var hold-buyer    as c  format "x(4)"         no-undo.
def var prtcntr       as i                        no-undo.
def var w-recptmo     as c  format "x(7)"         no-undo.
{zsapboydef.i}
define temp-table aprzv no-undo
  field vend            like apsv.vendno
  field vparent         as c format "x(20)"
  field vname           as c format "x(30)"
  field whse            like poel.whse
  field po              as c format "x(10)"
  field poline          like poel.lineno
  field potype          like poeh.transtype
  field product         like poel.shipprod
  field prodline        like poel.prodline
  field qtyord          like poel.qtyord
  field qtyrcv          like poel.qtyrcv
  field duedate         as date format "99/99/99"
  field fstexpdate      as date format "99/99/99"
  field expdate         as date format "99/99/99"
  field recptdate       as date format "99/99/99"
  field recptmo         as c    format "x(7)"
  field buyer           like poeh.buyer
  field buygrp          as c    format "x(24)"
  field buyname         as c  format "x(30)"
  field order-cnt       as i  format "->>>>>>9"   
  field order-val       as de format "->>>>>>9.99"
  field line-cost       as de format "->>>>>>9.99"
  field line-cnt        as i  format "->>>>>>9"   
  field line-cmpl       as i  format "->>>>>>9"   
  field line-incmpl     as i  format "->>>>>>9"   
  field line-late       as i  format "->>>>>>9"   
  field svc-level       as de format "->>>9.99"   
  
  index k-aprzv
     vend
     po
     poline.
     
     
define temp-table detail no-undo
    field prtrecid  as recid            
    field cnt       as int              
    index k-detail
          cnt.                          
def buffer b-aprzv for aprzv. 
/*
def stream myfile.
output stream myfile to "/home/das/aprzv/aprzvdat".
*/
 
 
 
 
 
 
 
&endif
 
&if defined(user_drpttable) = 2 &then
/* Use to add fields to the report table */
   FIELD aprecid         AS recid
   FIELD buyer           like poeh.buyer
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_forms) = 2 &then
form header
  "~015 " at 1
  with frame fx-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: APRZV"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 168
  page-number(xpcd)    at 173 format ">>>9"
  "~015"               at 177
         "Vendor Service Level Analysis" at 71
         "~015"              at 177
   with frame fx-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header
  "~015 " at 1
  with frame f-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: APRZV"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 168
  page-number          at 173 format ">>>9"
  "~015"              at 177
  "Vendor Service Level Analysis" at 72
  "~015"              at 177
   with frame f-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header 
   "~015"                    at 177
  with frame f-trn3 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header 
  "---Received Orders---"  at 37
  "------------------------Received Lines-----------------------" at 62
  "~015"                   at 117
  with frame f-trn4 down NO-BOX NO-LABELS no-underline
           WIDTH 178 page-top.
form header
  "# orders"             at 37
  "Total Val"            at 49
  "Total Lns"            at 62
  "Complete"             at 76
  "Incomplete"           at 87
  "Late Ship"            at 101
  "Svc Lvl"              at 116
   "~015"                at 178
   with frame f-trn5 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
            
form
  v-final                at 1
  v-astrik               at 2   format "x(5)"
  v-summary-lit2         at 7   format "x(27)"
  v-amt1           at 34  format ">>>,>>>,>>9-"
  v-amt2           at 47  format ">>>,>>>,>>9-"
  v-amt3           at 60  format ">>>,>>>,>>9-"
  v-amt4           at 73  format ">>>,>>>,>>9-" 
  v-amt5           at 86  format ">>>,>>>,>>9-"
  v-amt6           at 99  format ">>>,>>>,>>9-"
  v-amt7           at 112 format ">>>,>>>,>>9-"
  v-amt8           at 125 format ">>>,>>>,>>9-"
  "~015"           at 178
with frame f-tot width 178
 no-box no-labels.  
form
  "PO#"                 at 5
  "Ty"                  at 18
  "Due Dt"              at 28
  "1st Exp"             at 38
  "Exp Ship"            at 48
  "Rcpt Dt"             at 58
  "Ln"                  at 68
  "Product"             at 78
  "Qty Ord"             at 112
  "Qty Rcv"             at 124
  "Cost"                at 138
  x-space               at 176
  with frame f-dtl1 page-top NO-BOX NO-LABELS WIDTH 178.
form
  s-po           at  5
  s-potype       at  18
  s-duedate      at  28
  s-fstexpdate   at  38
  s-expdate      at  48
  s-recptdate    at  58
  s-poline       at  68
  s-product      at  78
  s-qtyord       at  112
  s-qtyrcv       at  124
  s-late         at  132
  s-line-cost    at  138
  with frame f-dtl2 down NO-BOX NO-LABELS no-underline
           WIDTH 178.
  
form 
  x-space          at 1
  "~015"           at 178
with frame f-skip width 178 no-box no-labels.  
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_extractrun) = 2 &then
  run extractdata.
 
 
 
 
 
 
&endif
 
&if defined(user_exportstatDWheaders) = 2 &then
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  
  if p-detailprt then
     assign export_rec = /* export_rec + v-del + */
            "Vendor"       + v-del +
            "Parent"       + v-del +
            "Name"         + v-del +
            "Whse"         + v-del +
            "PO"           + v-del +
            "POLine"       + v-del +
            "POType"       + v-del +
            "Product"      + v-del +
            "ProdLine"     + v-del +
            "QtyOrd"       + v-del +
            "QtyRcvd"      + v-del +
            "DueDate"      + v-del +
            "1stExDt"      + v-del +
            "ExpDate"      + v-del +
            "RcptDt"       + v-del +
       /*   "RcptMo"       + v-del + */
            "Buyer"        + v-del +
            "BuyerName"    + v-del +
            "OrdCnt"       + v-del +
            "OrdVal"       + v-del +
            "LineCost"     + v-del +
            "LineCnt"      + v-del +
            "ComplLines"   + v-del +
            "IncomplLines" + v-del +
            "LateLines". /* + v-del +
            "SvcLvl".    */
  else
     assign export_rec = export_rec     + v-del   +
                         "RecOrders"    + v-del   +
                         "TotValue"     + v-del   +
                         "TotLines"     + v-del   +
                         "ComplLines"   + v-del   +
                         "IncomplLines" + v-del   +
                         "LateShip"     + v-del   +
                         "ServLvl".
 
 
 
 
 
 
&endif
 
&if defined(user_exportstatheaders) = 2 &then
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  
  assign export_rec = export_rec + v-del +
                      "Received Orders" + v-del +
                      "Total Value" + v-del   +
                      "Total Lines" + v-del   +
                      "Complete Lines" + v-del   +
                      "Incomplete Lines" + v-del   +
                      "Late Ship" + v-del   +
                      "Serv Lvl".
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_foreach) = 2 &then
for each aprzv no-lock:
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_drptassign) = 2 &then
 /* If you add fields to the dprt table this is where you assign them */
 drpt.aprecid = recid(aprzv)
 drpt.buyer   = aprzv.buyer
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_B4endloop) = 2 &then
  /* this is before the end of the for each loop to feed the data BUILDREC */
  /* message "GOT HERE" v-sortmystery. */
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_viewheadframes) = 2 &then
/* Since frames are unique to each program you need to put your
   views and hides here with your names */
if not p-exportl then
  do:
  if not x-stream then
    do:
    hide frame f-trn1.
    hide frame f-trn2.
    hide frame f-trn3.
    hide frame f-trn4.
    hide frame f-trn5.
    end.
  else
   do:
    hide stream xpcd frame f-trn1.
    hide stream xpcd frame fx-trn2.
    hide stream xpcd frame f-trn3.
    hide stream xpcd frame f-trn4.
    hide stream xpcd frame f-trn5.
    end.
  
  end.
if not p-exportl  then
  do:
  if not x-stream then
    do:
    page.
    view frame f-trn1.
    view frame f-trn2.
    view frame f-trn3.
    view frame f-trn4.
    view frame f-trn5.
    end.
  else  
    do:
    page stream xpcd.
    view stream xpcd frame f-trn1.
    view stream xpcd frame fx-trn2.
    view stream xpcd frame f-trn3.
    view stream xpcd frame f-trn4.
    view stream xpcd frame f-trn5.
    end.
   
  end.
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_drptloop) = 2 &then
 /*  buyer print  */          
 form header                  
     /*
     s-buybrk            at 3 
     */
     "Buyer :"           at 4 
     s-buyer             at 12
     "Name:"             at 20
     s-buyernm           at 27
     "Department:"       at 57
     s-responsible       at 69
     "~015"              at 177
 with frame f-buyer no-box no-labels page-top width 177.
 view frame f-buyer.                                    
 if hold-buyer <> drpt.buyer then do:
    if hold-buyer = "" then assign hold-buyer = drpt.buyer.
    run buyer_header(input hold-buyer).
    assign hold-buyer = drpt.buyer.
 end.
 /*
 if v-holdsortmystery = "xxxxxxxxxxxxxx" then do:
    assign v-holdsortmystery = drpt.sortmystery.
    run buyer_header(input drpt.buyer).
 end.
 */
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_detailprint) = 2 &then
   /* Comment this out if you are not using detail 'X' option  */
   
   find aprzv where recid(aprzv) = drpt.aprecid
   no-lock no-error.
   
   /*  designates all other line, we want to print detail only for what
      we are asking for.  Not for the all-other lines */
   
   if p-detailprt and 
      entry((u-entitys - 1),drpt.sortmystery,v-delim) <> 
      fill("~~",length(entry((u-entitys - 1),drpt.sortmystery,v-delim))) then 
     do:
     /* this code skips the all other lines for printing detail */
     
     /* next line would be your detail print procedure */
        
        prtcntr = prtcntr + 1.                
        create detail.
        assign detail.prtrecid = drpt.aprecid
               detail.cnt = prtcntr.         
     end.
     
     if p-detailprt and p-exportDWl then do:
         run print-dtl.
     end.
     
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_finalprint) = 2 &then
assign t-amounts7[u-entitys + 1] = 
   (1 - (t-amounts6[u-entitys + 1] / t-amounts3[u-entitys + 1])) * 100.
   
if not p-exportl then
 do:
 if not x-stream then 
   do:
   display 
    v-final
    v-astrik
    v-summary-lit2
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts2[u-entitys + 1]   @ v-amt2
    t-amounts3[u-entitys + 1]   @ v-amt3
    t-amounts4[u-entitys + 1]   @ v-amt4
    t-amounts5[u-entitys + 1]   @ v-amt5
    t-amounts6[u-entitys + 1]   @ v-amt6
    t-amounts7[u-entitys + 1]   @ v-amt7
    with frame f-tot.
  
   down with frame f-tot.
   end.
 else
   do:
   display stream xpcd
    v-final
    v-astrik
    v-summary-lit2
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts2[u-entitys + 1]   @ v-amt2
    t-amounts3[u-entitys + 1]   @ v-amt3
    t-amounts4[u-entitys + 1]   @ v-amt4
    t-amounts5[u-entitys + 1]   @ v-amt5
    t-amounts6[u-entitys + 1]   @ v-amt6
    t-amounts7[u-entitys + 1]   @ v-amt7
    with frame f-tot.
  
   down stream xpcd with frame f-tot.
   end.
      
 end.
else if not p-exportDWl then
  do:
  assign export_rec =  v-astrik.
  do v-inx4 = 1 to u-entitys:
    if v-totalup[v-inx4] = "n" then
       next.
    if v-inx4 = 1 then
      v-summary-lit2 = "Final".
    else
      v-summary-lit2 = "".
    if v-cells[v-inx4]= "2" then
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec + v-summary-lit2 + v-del + " ".
     
      end.
    else
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec +  v-summary-lit2.
      end.
  end.
  assign export_rec = export_rec + v-del +
      string(t-amounts1[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts2[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts3[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts4[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts5[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts6[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts7[u-entitys + 1],"->>>>>>>>9").
    
  assign export_rec = export_rec + chr(13).    
  put stream expt unformatted export_rec.
  end.
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_summaryframeprint) = 2 &then
    assign t-amounts7[v-inx2] = 
    (1 - (t-amounts6[v-inx2] / t-amounts3[v-inx2])) * 100.
    if not x-stream then
      do:
      display 
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        t-amounts3[v-inx2]   @ v-amt3
        t-amounts4[v-inx2]   @ v-amt4
        t-amounts5[v-inx2]   @ v-amt5
        t-amounts6[v-inx2]   @ v-amt6
        t-amounts7[v-inx2]   @ v-amt7
        with frame f-tot.
      down with frame f-tot.
      end.
    else
      do:
      display stream xpcd
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        t-amounts3[v-inx2]   @ v-amt3
        t-amounts4[v-inx2]   @ v-amt4
        t-amounts5[v-inx2]   @ v-amt5
        t-amounts6[v-inx2]   @ v-amt6
        t-amounts7[v-inx2]   @ v-amt7
        with frame f-tot.
      down stream xpcd with frame f-tot.
      end.
      
    /* added detail after summary */
    /* next line would be your detail print procedure */
      /*
      if p-detailprt and p-exportDWl then do:
         display "GOT HERE".
         run print-dtl.
      end.
      else do:
      */
         if p-detailprt and v-inx2 = (u-entitys - 1) then do:
            display x-space with frame f-skip.
            display x-space with frame f-dtl1.
            down with frame f-dtl1.
            run print-dtl.
            display x-space with frame f-skip.
            down with frame f-skip.
            display x-space with frame f-skip.
            down with frame f-skip.
         end.
      /*
      end.
      */
 
 
 
 
 
 
 
&endif
 
&if defined(user_summaryputexport) = 2 &then
 
/* PTD */
    assign t-amounts7[v-inx2] = 
       (1 - (t-amounts6[v-inx2] / t-amounts3[v-inx2])) * 100.
    assign export_rec = v-astrik + v-del + export_rec.
    assign export_rec = export_rec + v-del +
      string(t-amounts1[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts2[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts3[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts4[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts5[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts6[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts7[v-inx2],"->>>>>>>>9").
      
 
 
 
 
 
 
 
&endif
 
&if defined(user_detailputexport) = 2 &then
   /*
   if p-detailprt and p-exportDWl then do:
         display "GOT HERE".
         run print-dtl.
   end.
   */
 
 
 
 
 
 
 
&endif
 
&if defined(user_procedures) = 2 &then
/* ------------------------------------------------------------------------- */
procedure formatoptions:
/* ------------------------------------------------------------------------- */
   
   
  assign p-portrait = false.
  /* if false then the report will print in LANDSCAPE 178 character */
  
  if sapb.optvalue[1] > "0" and sapb.optvalue[1] ne "99" then 
    do:
    p-optiontype = " ".
    find notes where notes.cono = g-cono and
                     notes.notestype = "zz" and
                     notes.primarykey = "aprzv" and  /* tbxr program */
                     notes.secondarykey = string(p-format) no-lock no-error.
    if not avail notes then
      do:
      display "Format is not valid cannot process request".
      assign p-optiontype = "V"
             p-sorttype   = ">,"
             p-totaltype  = "l"
             p-summcounts = "a".
      return.
      end.               
   
    assign p-optiontype = notes.noteln[1]
           p-sorttype   = notes.noteln[2]
           p-totaltype  = notes.noteln[3]
           p-summcounts = notes.noteln[4]
           /*
           p-export     = "    "
           */
           p-register   = notes.noteln[5]
           p-registerex = notes.noteln[6].
    end.     
  else
  if sapb.optvalue[1] = "99" then 
    do:
    assign p-register   = ""
           p-registerex = "".
    run reportopts(input sapb.user5, 
                   input-output p-optiontype,
                   input-output p-sorttype,  
                   input-output p-totaltype,
                   input-output p-summcounts,
                   input-output p-register,
                   input-output p-regval).
    end.
 end.
 
   
 procedure get-vend-id:
   if aprzv.vend = 81000496    then assign aprzv.vparent    = "ALLENAIR".
   if aprzv.vend = 7000003627  or 
      aprzv.vend = 70200129    then assign aprzv.vparent    = "BALLUFF".
   if aprzv.vend = 70200064    then assign aprzv.vparent    = "BLISSFIELD".
   if aprzv.vend = 81101539    or 
      aprzv.vend = 10000034297 then assign aprzv.vparent    = "BOSCH".
   if aprzv.vend = 81002372    then assign aprzv.vparent    = "CIRCOR".
   if aprzv.vend = 81002456    then assign aprzv.vparent    = "CLIPPARD".
   if aprzv.vend = 70300951    then assign aprzv.vparent    = "COMM CONT".
   if aprzv.vend = 70300948    or 
      aprzv.vend = 70300013    then assign aprzv.vparent    = "CUNO".
   if aprzv.vend = 70400054    then assign aprzv.vparent    = "DELTROL".
   if aprzv.vend = 80073900    or 
      aprzv.vend = 80062950    then assign aprzv.vparent    = "DONALDSON".
   if aprzv.vend = 70400472    then assign aprzv.vparent    = "DURST".
   if aprzv.vend = 81101582    then assign aprzv.vparent    = "DYNEX".
   if aprzv.vend = 222000019   or 
      aprzv.vend = 70500019    then assign aprzv.vparent    = "EATON CHARLYN".
   if aprzv.vend = 222000005   or
      aprzv.vend = 70800005    then assign aprzv.vparent    = "EATON HYD LINE".
   if aprzv.vend = 9853210     or
      aprzv.vend = 222095000   then assign aprzv.vparent    = "EATON VICKERS".
   if aprzv.vend = 70500330    then assign aprzv.vparent    = "EIGHTY TWENTY".
   if aprzv.vend = 70600257    or
      aprzv.vend = 70600066    then assign aprzv.vparent    = "FAIRFIELD".
   if aprzv.vend = 70600017    then assign aprzv.vparent    = "FALLS FAB".
   if aprzv.vend = 81033760    then assign aprzv.vparent    = "FIRESTONE".
   if aprzv.vend = 70600139    then assign aprzv.vparent    = "FLOTORK".
   if aprzv.vend = 70800166    or
      aprzv.vend = 70800237    then assign aprzv.vparent    = "HALDEX BARNES".
   if aprzv.vend = 9369900     then assign aprzv.vparent    = "HANSEN".
   if aprzv.vend = 70800489    then assign aprzv.vparent    = "HIGH CNTRY TEK".
   if aprzv.vend = 70800031    or
      aprzv.vend = 70800443    then assign aprzv.vparent    = "HUSCO".
   if aprzv.vend = 70800253    then assign aprzv.vparent    = "HYDRO-GEAR".
   if aprzv.vend = 70100035    then assign aprzv.vparent    = "ING ARO".
   if aprzv.vend = 10000033411 then assign aprzv.vparent    = "KV PNEU".
   if aprzv.vend = 71300682    then assign aprzv.vparent    = "LINDE".
   if aprzv.vend = 72300013    then assign aprzv.vparent    = "LUBRIQUIP".
   if aprzv.vend = 81012347    then assign aprzv.vparent    = "MCDANIEL".
   if aprzv.vend = 80076710    or
      aprzv.vend = 71400122    then assign aprzv.vparent    = "MITSUBISHI".
   if aprzv.vend = 71500327    then assign aprzv.vparent    = "NACHI".
   if aprzv.vend = 71500045    then assign aprzv.vparent    = "HORDHYD".
   if aprzv.vend = 81013284    then assign aprzv.vparent    = "NORGREN".
   if aprzv.vend = 80084400    then assign aprzv.vparent    = "PARKER SKINNER".
   if aprzv.vend = 70600222    then assign aprzv.vparent    = "PARKER ARLON".
   if aprzv.vend = 216013620   or 
      aprzv.vend = 70300787    or
      aprzv.vend = 81082252    or
      /* aprzv.vend = 80067700    or */  /* assign to parker greer */
      aprzv.vend = 71700471    or
      aprzv.vend = 71700354    or
      aprzv.vend = 70300200    or
      aprzv.vend = 81082251    then assign aprzv.vparent    = "PARKER".
   if aprzv.vend = 71700099    or
      aprzv.vend = 71700408    then assign aprzv.vparent    = "POCLAIN".
   if aprzv.vend = 71700203    then assign aprzv.vparent    = "PRINCE".
   if aprzv.vend = 71900080    then assign aprzv.vparent    = "ROSEDALE".
   if aprzv.vend = 71900177    then assign aprzv.vparent    = "ROTARY".
   if aprzv.vend = 9035000     then assign aprzv.vparent    = 
      "EATON-AEROQUIP".
  
   if aprzv.vend = 81101926    then 
      assign aprzv.vparent    = "". /* judy potts 6-5-12 */
   if aprzv.vend = 10009854623 then 
     assign aprzv.vparent    = "Eaton-Duraforce". /* judy potts 6-5-12 */
       
      
   if aprzv.vend = 70400588    or
      aprzv.vend = 70300733    or
      aprzv.vend = 72200448    or
      aprzv.vend = 72200085    or
      aprzv.vend = 72200575    then assign aprzv.vparent    = "SAUER".
   if aprzv.vend = 9733060     then assign aprzv.vparent    = "SCHROEDER".
   if aprzv.vend = 80084650    then assign aprzv.vparent    = "SMC".
   if aprzv.vend = 81018462    then assign aprzv.vparent    = "SNAP-TITE".
   if aprzv.vend = 81005468    then assign aprzv.vparent    = "SPX".
   if aprzv.vend = 72200634    or
      aprzv.vend = 72201004    then assign aprzv.vparent    = "STERLING".
   if aprzv.vend = 9850610     then assign aprzv.vparent    = "VECTOR".
   if aprzv.vend = 81021498    then assign aprzv.vparent    = "VERSA".
   if aprzv.vend = 72900003    then assign aprzv.vparent    = "ZINGA".
   if aprzv.vend = 81022840    then assign aprzv.vparent    = "WIKA".
   if aprzv.vend = 80067700    then assign aprzv.vparent    = "PARKER GREER".
   if aprzv.vend = 71200398    then assign aprzv.vparent    = "PENTAIR".
   
   if aprzv.vparent = "" then
      assign aprzv.vparent = "ALL OTHER".                      
   find apsv where apsv.cono = g-cono and
                   apsv.vendno = poeh.vendno
                   no-lock
                   no-error.
   if avail apsv then
      assign aprzv.vname = apsv.name.
   else
      assign aprzv.vname = "NAME UNKNOWN".
 end.   /* get-vend-id */
 
 procedure buyer_header:
    def input parameter hv-buyer as char format "x(4)" no-undo.
    {sasta.gfi "b" hv-buyer no}
    assign  s-buyernm = if avail sasta then sasta.descrip
                         else if hv-buyer = "" then "None Assigned"
                              else v-invalid
            /*
            s-buybrk  = "*"
            */
            s-buyer   = hv-buyer.
    find first oimsp where oimsp.person = hv-buyer
                           no-lock no-error.
    if avail oimsp then
       assign s-responsible = oimsp.responsible.
    else
       assign s-responsible = "Unknown Department".
    
 end. /* procedure */
 
 {zsapboycheck.i}
 
procedure print-dtl:
    for each detail:
       find aprzv where recid(aprzv) = detail.prtrecid no-lock no-error.
       delete detail.
       /****DW detail****/
       if p-exportDWl then do:
          if p-detailprt and
             ( not p-lateonly or 
              (p-lateonly and aprzv.line-late > 0 )) then do:
             assign export_rec = "".
             assign export_rec = /* export_rec + v-del + */
                    string(aprzv.vend,"999999999999") + v-del +
                    aprzv.vparent   + v-del +
                    aprzv.vname     + v-del +
                    aprzv.whse      + v-del +
                    aprzv.po        + v-del +
                    string(aprzv.poline,">>9") + v-del +
                    aprzv.potype    + v-del +
                    aprzv.product   + v-del +
                    aprzv.prodline  + v-del +
                    string(aprzv.qtyord,"->>>>>>>>9") + v-del +
                    string(aprzv.qtyrcv,"->>>>>>>>9") + v-del +
                    string(aprzv.duedate,"99/99/99")  + v-del +
                    string(aprzv.fstexpdate,"99/99/99") + v-del +
                    string(aprzv.expdate,"99/99/99") + v-del +
                    string(aprzv.recptdate,"99/99/99") + v-del +
                 /* aprzv.recptmo   + v-del +  */
                    aprzv.buyer     + v-del +
                    aprzv.buyname   + v-del +
                    string(aprzv.order-cnt,"->>>>>>9") + v-del +
                    string(aprzv.order-val,"->>>>>>9.99") + v-del +
                    string(aprzv.line-cost,"->>>>>>9.99") + v-del +
                    string(aprzv.line-cnt,"->>>>>>9") + v-del +
                    string(aprzv.line-cmpl,"->>>>>>9") + v-del +
                    string(aprzv.line-incmpl,"->>>>>>9") + v-del +
                    string(aprzv.line-late,"->>>>>>9"). /* + v-del +
                    string(aprzv.svc-level,"->>>9.99"). */
          end.
          else
             assign export_rec = export_rec + v-del +
                    string(t-amounts1[u-entitys + 1],"->>>>>>>>9") + v-del +
                    string(t-amounts2[u-entitys + 1],"->>>>>>>>9") + v-del +
                    string(t-amounts3[u-entitys + 1],"->>>>>>>>9") + v-del +
                    string(t-amounts4[u-entitys + 1],"->>>>>>>>9") + v-del +
                    string(t-amounts5[u-entitys + 1],"->>>>>>>>9") + v-del +
                    string(t-amounts6[u-entitys + 1],"->>>>>>>>9") + v-del +
                    string(t-amounts7[u-entitys + 1],"->>>>>>>>9").
    
          assign export_rec = export_rec + chr(13) + chr(10).
          put stream expt unformatted export_rec.
          /*
          if p-exportDWl and p-detailprt then
             export stream myfile delimiter ","
                substring(export_rec,1,10)
                string(aprzv.vend,"999999999999") 
                    aprzv.vparent  
                    aprzv.vname    
                    aprzv.whse    
                    aprzv.po     
                    string(aprzv.poline,">>9") 
                    aprzv.potype   
                    aprzv.product   
                    aprzv.prodline 
                    string(aprzv.qtyord,"->>>>>>>>9")
                    string(aprzv.qtyrcv,"->>>>>>>>9") 
                    string(aprzv.duedate,"99/99/99")  
                    string(aprzv.fstexpdate,"99/99/99")
                    string(aprzv.expdate,"99/99/99") 
                    string(aprzv.recptdate,"99/99/99") 
                    aprzv.buyer    
                    string(aprzv.order-cnt,"->>>>>>9") 
                    string(aprzv.order-val,"->>>>>>9.99") 
                    string(aprzv.line-cost,"->>>>>>9.99") 
                    string(aprzv.line-cnt,"->>>>>>9") 
                    string(aprzv.line-cmpl,"->>>>>>9") 
                    string(aprzv.line-incmpl,"->>>>>>9") 
                    string(aprzv.line-late,"->>>>>>9").
          */   
       end.
       else if not p-lateonly or 
             (p-lateonly and aprzv.line-late > 0 ) then do:
          assign s-late = if aprzv.line-late > 0 then "< L" else "".
          display aprzv.po          @ s-po
                  aprzv.potype      @ s-potype
                  aprzv.duedate     @ s-duedate
                  aprzv.fstexpdate  @ s-fstexpdate
                  aprzv.expdate     @ s-expdate
                  aprzv.recptdate   @ s-recptdate
                  aprzv.poline      @ s-poline
                  aprzv.product     @ s-product
                  aprzv.qtyord      @ s-qtyord
                  aprzv.qtyrcv      @ s-qtyrcv
                  s-late
                  aprzv.line-cost   @ s-line-cost
          with frame f-dtl2.
          down with frame f-dtl2.
          assign s-late = "".
       end.
    end.
end.  /* procedure */
Procedure extractdata.
  assign b-vendno    = dec(sapb.rangebeg[1])
         e-vendno    = dec(sapb.rangeend[1])
         b-shipfm    = int(substring(sapb.rangebeg[2],1,4))
         e-shipfm    = int(substring(sapb.rangeend[2],1,4))
         b-prodcat   = sapb.rangebeg[3]
         e-prodcat   = sapb.rangeend[3]
         b-whse      = sapb.rangebeg[4]
         e-whse      = sapb.rangeend[4]
         b-buygrp    = sapb.rangebeg[6]
         e-buygrp    = sapb.rangeend[6]
         b-buyer     = sapb.rangebeg[7]
         e-buyer     = sapb.rangeend[7]
         b-type      = sapb.rangebeg[8]
         e-type      = sapb.rangeend[8]
         b-ptype     = sapb.rangebeg[9]
         e-ptype     = sapb.rangeend[9]
         b-product   = sapb.rangebeg[10]
         e-product   = sapb.rangeend[10]
                                           
         p-format    = sapb.optvalue[1]
         p-print     = sapb.optvalue[2]
         p-oan       = if sapb.optvalue[3] = "yes" then yes else no
         p-nonstk    = if sapb.optvalue[4] = "yes" then yes else no
         p-datetype  = sapb.optvalue[5]
         p-transit   = int(sapb.optvalue[6])
         p-list      = if sapb.optvalue[7] = "yes" then yes else no
         p-export    = sapb.optvalue[8]
         p-parent    = if sapb.optvalue[9] = "yes" then yes else no.
         p-lateonly  = if sapb.optvalue[10] = "yes" then yes else no.
     
  if sapb.rangebeg[5] = "" then
     b-rcptdate = TODAY .
  else do:
     v-datein = sapb.rangebeg[5].
     {p-rptdt.i}
     if string(v-dateout) = v-lowdt then
        b-rcptdate = TODAY.
     else
        b-rcptdate = v-dateout.
  end.
  if sapb.rangeend[5] = "" then
     e-rcptdate = TODAY .
  else do:
     v-datein = sapb.rangeend[5].
     {p-rptdt.i}
     if string(v-dateout) = v-lowdt then
        e-rcptdate = TODAY.
     else
        e-rcptdate = v-dateout.
  end.
                             
  if e-vendno =  0  then assign e-vendno = 999999999999.
  if e-shipfm =  0  then assign e-shipfm = 9999.
  if e-prodcat   =  "" then assign e-prodcat   = "````".
  if e-whse   =  "" then assign e-whse   = "````".
  if e-buygrp =  "" then assign e-buygrp = "````````````````````````".
  if e-buyer  =  "" then assign e-buyer  = "````".
  if e-type   =  "" then assign e-type   = "````".
  if e-ptype  =  "" then assign e-ptype  = "````".
  
  if p-print = "D" then
     assign p-detailprt = yes
            p-detail    = "D".
  else
     assign p-detailprt = no
            p-detail    = "S".
  
  if p-transit > 0 then
     assign zdays = p-transit.
  else
     assign zdays = 7.
  
  run formatoptions.
  assign zzl_begcat   = b-prodcat
         zzl_endcat   = e-prodcat
         zzl_Prodbeg   = b-product
         zzl_Prodend   = e-product.
         /*
         zzl_begvname = b-vid
         zzl_endvname = e-vid
         zzl_begvpid  = b-vp
         zzl_endvpid  = e-vp
         zzl_begbuy   = b-buyer
         zzl_endbuy   = e-buyer
         zzl_begvend  = b-vendno
         zzl_endvend  = e-vendno
         zzl_begwhse  = b-whse
         zzl_endwhse  = e-whse.
         */
  assign zel_begcat   = b-prodcat
         zel_endcat   = e-prodcat
         zel_Prodbeg   = b-product
         zel_Prodend   = e-product.
          
         /*
         zel_begvname = b-vid
         zel_endvname = e-vid
         zel_begvpid  = b-vp
         zel_endvpid  = e-vp
         zel_begbuy   = b-buyer
         zel_endbuy   = e-buyer
         zel_begvend  = b-vendno
         zel_endvend  = e-vendno
         zel_begwhse  = b-whse
         zel_endwhse  = e-whse.
         */
  if p-list then
    do:
    assign zel_begcat   = b-prodcat
           zel_endcat   = e-prodcat
           zel_Prodbeg   = b-product
           zel_Prodend   = e-product.
 
           /*
           zel_begvname = b-vid
           zel_endvname = e-vid
           zel_begvpid  = b-vp
           zel_endvpid  = e-vp
           zel_begbuy   = b-buyer
           zel_endbuy   = e-buyer
           zel_begvend  = b-vendno
           zel_endvend  = e-vendno
           zel_begwhse  = b-whse
           zel_endwhse  = e-whse.
           */
    {zsapboyload.i}
  end.
  
  /* only using list for p-cat */
  assign zelection_matrix [3] = (if zelection_matrix[3] > 1 then  /* Cat  */
                                 zelection_matrix[3]
                              else
                                 1)
         zelection_matrix [11] = (if zelection_matrix[11] > 1 then /* Product */
                                 zelection_matrix[11]
                              else   
                                 1).
  
  if p-exportDWl and b-rcptdate = e-rcptdate and
     b-rcptdate = TODAY then
     assign b-rcptdate = b-rcptdate - 7.
   
   
  for each poeh where poeh.cono       = g-cono     and
                      poeh.stagecd   >= 5          and
                      poeh.stagecd   <  9          and
                      poeh.vendno    >= b-vendno   and
                      poeh.vendno    <= e-vendno   and
                      poeh.shipfmno  >= b-shipfm   and
                      poeh.shipfmno  <= e-shipfm   and
                      poeh.buyer     >= b-buyer    and
                      poeh.buyer     <= e-buyer    and
                     (poeh.transtype  = "PO" or
                      poeh.transtype  = "DO" or
                      poeh.transtype  = "BR")      and
                      poeh.transtype >= b-type     and
                      poeh.transtype <= e-type     and
                      poeh.whse      >= b-whse     and
                      poeh.whse      <= e-whse     and
                      poeh.receiptdt >= b-rcptdate and
                      poeh.receiptdt <= e-rcptdate
                      no-lock,
     each poel where poel.cono           = g-cono     and
                     poel.pono           = poeh.pono  and
                     poel.posuf          = poeh.posuf and
                     poel.qtyrcv         >  0         and
                     poel.shipprod       ge  zel_Prodbeg and
                     poel.shipprod       le  zel_Prodend and
                     poel.nonstockty    <> "r"        and
                     poel.nonstockty    <> "l"     
                     
                     no-lock
     
     break by poeh.vendno
           by poeh.pono   
           by poel.lineno:
     
    
                
        assign zelection_type = "u"
               zelection_char4 = poel.shipprod
               zelection_cust = 0.
        run zelectioncheck.
        if zelection_good = false then
          next.
 
        find icsp where icsp.cono = g-cono and
                        icsp.prod = poel.shipprod
                        no-lock
                        no-error.
        if avail icsp then do:
           assign zelection_type = "p"       
                  zelection_char4 = icsp.prodcat
                  zelection_cust = 0.        
           run zelectioncheck.               
           if zelection_good = false then    
              next.                           
        end.                   
        
        if not avail icsp and p-nonstk = no then next.
        if avail icsp and icsp.statustype = "L" then next.
        
        find icsw where icsw.cono        = g-cono and
                        icsw.prod        = poel.shipprod and
                        icsw.whse        = poel.whse and
                        icsw.pricetype  >= b-ptype and
                        icsw.pricetype  <= e-ptype
                        no-lock
                        no-error.
        if not avail icsw and p-nonstk = no then next.
        if avail icsw and icsw.statustype = "O" and p-oan = no then next.
        
        assign x-buygrp = ""
               x-buyname = "".
        find first oimsp where oimsp.person = poeh.buyer and
                               oimsp.responsible >= b-buygrp and
                               oimsp.responsible <= e-buygrp
                               no-lock no-error.
        if avail oimsp then
           assign x-buygrp  = oimsp.responsible.
        else
           /*
           assign x-buygrp = "".
           */
           next.
        find sasta use-index k-sasta where 
            sasta.cono = g-cono and
            sasta.codeiden = "B" and
            sasta.codeval  = poeh.buyer no-lock no-error.
        assign x-buyname = (if avail sasta then sasta.descrip
                            else "No BUYER Descrip").
        
        find aprzv use-index k-aprzv where 
             aprzv.vend   = poeh.vendno and
             aprzv.po     = string(poeh.pono,"9999999") + "-" +
                            string(poeh.posuf,"99")  and
             aprzv.poline = poel.lineno 
             no-lock
             no-error.
        
        if avail aprzv then do:    /* if late then line-late = 1 */
           assign aprzv.qtyrcv     = aprzv.qtyrcv + poel.qtyrcv
                  aprzv.line-late  = if (poel.reqshipdt + zdays) <
                                        poeh.receiptdt
                                        then aprzv.line-late + 1 
                                     else aprzv.line-late.
           if aprzv.line-incmpl = 1 then assign aprzv.line-late = 0.
        end.
        
        else do:
           create aprzv.
           assign aprzv.vend        = poeh.vendno
                  aprzv.whse        = CAPS(poeh.whse)
                  aprzv.po          = string(poeh.pono,"9999999")
                                     + "-" + string(poeh.posuf,"99")
                  aprzv.poline      = poel.lineno
                  aprzv.potype      = CAPS(poeh.transtype)
                  aprzv.product     = replace(poel.shipprod,"""","'")
                  aprzv.prodline    = CAPS(poel.prodline)
                  aprzv.qtyord      = poel.qtyord
                  aprzv.qtyrcv      = poel.qtyrcv
                  aprzv.duedate     = if poel.duedt = ? then poeh.duedt
                                      else poel.duedt
                  aprzv.fstexpdate  = if poel.reqshipdt = ? then poeh.duedt
                                      else poel.reqshipdt
                  aprzv.expdate     = if poel.expshipdt = ? then poeh.duedt
                                      else poel.expshipdt
                  aprzv.recptdate   = poeh.receiptdt
                  aprzv.buyer       = CAPS(poeh.buyer)
                  aprzv.buygrp      = x-buygrp
                  aprzv.buyname     = x-buyname
                  aprzv.order-val   = if first-of(poeh.pono) then
                                         if poeh.stagecd = 5 then
                                          poeh.totrcvamt - poeh.wodiscamt
                                         else
                                          if can-do("6,7",string(poeh.stagecd))
                                             then
                                               poeh.totinvamt - poeh.wodiscamt
                                             else 0
                                      else 0
                  aprzv.line-cost   = poel.netamt
                  aprzv.line-cnt    = 1
                  aprzv.order-cnt   = if first-of(poeh.pono) then 1 
                                      else 0
                  /*
                  aprzv.line-late   = if (poel.reqshipdt + zdays) < 
                                         poeh.receiptdt
                                         then 1 else 0
                  */
                  aprzv.line-cmpl   = if poel.qtyord = poel.qtyrcv
                                         then 1 else 0
                  aprzv.line-incmpl = if poel.qtyord > poel.qtyrcv
                                         then 1 else 0
                  aprzv.svc-level   = if last-of(poeh.vendno) then
                                        (1 - (vend-late-lines / vend-line-cnt))
                                        * 100
                                      else
                                        0.
           if aprzv.duedate    = ? then do:
              if poeh.expshipdt <> ? then 
                 assign aprzv.duedate = poeh.expshipdt.
              else
                 if poeh.reqshipdt <> ? then
                    assign aprzv.duedate = poeh.reqshipdt.
                 else
                    assign aprzv.duedate = poeh.enterdt.
           end.
           if aprzv.fstexpdate = ? then do:
              if poeh.expshipdt <> ? then 
                 assign aprzv.fstexpdate = poeh.expshipdt.
              else
                 if poeh.reqshipdt <> ? then
                    assign aprzv.fstexpdate = poeh.reqshipdt.
                 else
                    assign aprzv.fstexpdate = poeh.enterdt.
           end.
           if aprzv.expdate    = ? then do:
              if poeh.expshipdt <> ? then 
                 assign aprzv.expdate = poeh.expshipdt.
              else
                 if poeh.reqshipdt <> ? then
                    assign aprzv.expdate = poeh.reqshipdt.
                 else
                    assign aprzv.expdate = poeh.enterdt.
           end.
           
           assign aprzv.recptmo   = 
              string(MONTH(poeh.receiptdt),"99") + "/" + 
              string(YEAR(poeh.receiptdt),"9999").
           /* 0 is assign if late - if incomplete, a line is not late */
           if p-datetype = "E" then
              assign aprzv.line-late = 
                     if (poel.expshipdt + zdays) < poeh.receiptdt 
                     then 1 else 0.
           if p-datetype = "D" then
              assign aprzv.line-late = 
                     if (poel.duedt + zdays) < poeh.receiptdt 
                     then 1 else 0.
           if p-datetype = "1" then
              assign aprzv.line-late = 
                     if (poel.reqshipdt + zdays) < poeh.receiptdt
                     then 1 else 0.
           if aprzv.line-incmpl = 1 then assign aprzv.line-late = 0.
           
           run "get-vend-id".
        end.
        
        if first-of(poeh.vendno) then do:
           assign vend-late-lines = 0
                  vend-line-cnt   = 0.
        end.
        if (poel.reqshipdt + zdays) < poeh.receiptdt and 
           poel.qtyord = poel.qtyrcv  then
           assign vend-late-lines = vend-late-lines + 1.
        assign vend-line-cnt = vend-line-cnt + 1.
        
  end. /* each poeh */
  /*
  output to "/home/das/aprzv.dat".
  for each aprzv no-lock:
            
      export delimiter ","
        aprzv.vend   
        aprzv.whse 
        aprzv.po   
        aprzv.poline   
        aprzv.potype 
        aprzv.product   
        aprzv.qtyord 
        aprzv.qtyrcv
        aprzv.duedate
        aprzv.fstexpdate
        aprzv.expdate
        aprzv.recptdate
        aprzv.buyer
        aprzv.buygrp
        aprzv.order-cnt
        aprzv.order-val
        aprzv.line-cost
        aprzv.line-cnt
        aprzv.line-cmpl
        aprzv.line-incmpl
        aprzv.line-late
        aprzv.svc-level.
        
  end.
  output close.
  */
 
end. 
 
 
 
 
 
 
&endif
 
