/* --------------------------------------------------------------------------
     p-DCstring.i 

     Purpose:      Create a common overlay command that can be arrived
                   at from the DC Template spreadsheet.
                   
---------------------------------------------------------------------------  */
 ( if {&exp1} = "INT(" then
    string(INT({&source}),{&format}) 
   else
   if {&exp1} = "DEC(" then
    string(DEC({&source}),{&format})
   else
   if {&exp1} = "DATE(" and {&source} <> "" then
    string(DATE({&source}),{&format})
   else
   if {&exp1} = "DATE(" and {&source} = "" then
    string({&source},"x(6)")
   else
    string({&source},{&format})  ) no-error.
     
                        
