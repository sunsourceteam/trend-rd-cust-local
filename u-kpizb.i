
def var v-myframe   as character format "x(5)" no-undo.
def var o-prod      like kpsk.prod      no-undo.

   put screen row 21 col 3 color messages
   " F6-ICSW   F7-Comp POIZS   F8-Kit POIZS   F9-Prod Avail  F10-Comp Cost Dt".

if {k-func6.i} then do:
   
   form
   "Product: "   at 2
   var-prod      at 14
   "Whse: "      at 45
   icsw.whse     at 53
   with frame f-micsw width 80 centered no-labels no-box down row 2.
   
   form
   g-prod           colon 8 label "Product"
   {f-help.i}
   icsp.notesfl     at 34 no-label
   icsp.descrip[1]  at 36 no-label
   g-whse           colon 66 label "Whse"
   {f-help.i}
   with frame f-icsw side-labels title g-title row 1 column 1 width 80 overlay.
    
   
   assign o-prod = g-prod    
          g-prod = var-prod. 
   
  
  
  view frame f-icsw.
  display 
    g-prod
    g-whse 
  with frame f-icsw.

   {w-icsw.i var-prod g-whse exclusive-lock}
    v-idicsw = recid(icsw).
 
   run icswb.p.      

   assign g-prod = o-prod. 

   run Refresh-Frames.   
   
   next dsply.
end.


else if {k-func7.i} then do:
  assign o-prod = g-prod
         g-prod = var-prod.  
  run poizs.p.
 
/*  assign g-prod = o-prod.  */
 
  run Refresh-Frames.   
 
  next dsply.
  
end.

else if {k-func8.i} then do:
 
   run poizs.p.
   
   run Refresh-Frames.   
   
   next dsply.

end.   
  
else if {k-func9.i} then do:

   assign o-prod = g-prod
          g-prod = var-prod.  
   run zsdiiciap.p.

   assign g-prod = o-prod. 
 
   run Refresh-Frames.   
  
   next dsply.

end.


else if {k-func10.i} then do:

   find first icsw where icsw.cono = g-cono and
                         icsw.prod = var-prod and
                         icsw.whse = g-whse
                         no-lock no-error.
     if avail icsw then
      display
   
      icsw.priceupddt
      with frame f-price.
   
   readkey.  

   if {k-cancel.i} then do:
   
    hide frame f-price.
    clear frame f-price.
   
    run Refresh-Frames.   
  
    next dsply.


   end.

end.    


put screen row 21 col 3 color messages
   " F6-ICSW   F7-Comp POIZS   F8-Kit POIZS   F9-Prod Avail  F10-Comp Cost Dt".


if {k-jump.i} or {k-select.i} or {k-cancel.i} or not
   (g-modulenm = "" or g-modulenm = "kp") then do:
   leave main.
   hide message no-pause.
   next.
   
end. 

