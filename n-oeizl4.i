/* n-oeizl4.i 1.1 01/03/98 */
/* n-oeizl4.i 1.6 01/29/94 */
/*h*****************************************************************************
  SX 55
  INCLUDE      : n-oeizl4.i
  DESCRIPTION  : Find next/prev for OEIZL where v-mode = 4
  USED ONCE?   : yes
  AUTHOR       : rhl
  DATE WRITTEN :
  CHANGES MADE :
    12/23/91 pap; TB#  5266 JIT/Line Due - reqship to promise dt
    01/29/94 kmw; TB# 11747 Make stage a range
    03/14/96 tdd; TB# 20713 Inquiry not working properly
    11/04/99 gfk; TB# 26830 Improve performance by including whse in oeel find
       and remove it from the oeeh find.
    08/08/02 lcb; TB# e14281 User Hooks
*******************************************************************************/

/*e*****************************************************************************
     Selection Criteria for this procedure:
       v-mode ==> 4:
               Product: entered on the screen
    Kit Component Type: "o" (Orders Only)
             Customer#: entered on the screen
               CustPO#: <blank>
*******************************************************************************/
do:
confirm = yes.

if ("{1}" = "FIRST" and s-directionfl )  then
  assign v-xmode = "C".
else
if ("{1}" = "LAST" and not s-directionfl )  then
  assign v-xmode = "O".



if v-xmode = "C" then
  do:


find {1} oeelb  use-index k-prod where
         oeelb.cono     = g-cono and
         oeelb.shipprod = s-prod    and
         oeelb.user7    = s-custno  and
/*         (s-shipto     = ""   or oeelb.shipto    = s-shipto) and
         
         (s-whse = "" or oeelb.whse = s-whse)        and
*/
         (s-transtype <> "bo")                       and
         (s-doonlyfl   = no   or oeelb.botype    = "d") no-lock no-error. 

if avail oeelb then do:
    find oeehb use-index k-oeeh where
         oeehb.cono     = g-cono        and
         oeehb.batchnm  = oeelb.batchnm and
         oeehb.seqno    = oeelb.seqno   and
         oeehb.custno   = s-custno  and
         (s-whse = "" or oeehb.whse = s-whse)        and
         (s-shipto     = ""   or oeehb.shipto    = s-shipto) and

         (s-takenby    = ""   or oeehb.takenby   = s-takenby) and
         (s-transtype  = ""   or s-transtype    = "QU" /* oeehb.transtype*/) 
         and 

          
         ((oeehb.stagecd <> 1 and oeehb.stagecd >= integer(s-stagecdl)) or
           (oeehb.stagecd = 1 and integer(s-stagecdl) = 0)) and
          (oeehb.stagecd <= integer(s-stagecdh)) and

         oeehb.sourcepros = "ComQu" and
         oeehb.seqno = 2 and

         (s-promisedt  = ?    or oeehb.promisedt >= s-promisedt)

         {oeizl.z99 &user_find_oeizl = "*"
                    &file            = "oeehb"}
    no-lock no-error.
    if not avail oeehb then confirm = no.
end.
 
if avail oeelb then
  find zxinq where zxinq.present = true  no-lock no-error.
else
  find zxinq where zxinq.present = false no-lock no-error.


end.  


if v-xmode = "C" and (not avail oeelb) and
   ("{1}" = "first" or "{1}" = "next" ) then
  do:
  assign v-xmode = "O"
         confirm = yes.

  find first oeel use-index k-prod
       where oeel.cono = g-cono and
             oeel.shipprod = " " no-lock no-error. 
  end.
if v-xmode = "O" then
do:

find {1} oeel use-index k-prod where
         oeel.cono     = g-cono and
         oeel.shipprod = s-prod and
         oeel.custno   = s-custno and
         (s-shipto     = ""   or oeel.shipto    = s-shipto) and
         (s-whse = "" or oeel.whse = s-whse)  and
         (s-transtype  = ""   or s-transtype    = oeel.transtype or
         (s-transtype  = "bo"))       and
         (s-doonlyfl   = no   or oeel.botype    = "d")
no-lock no-error.
if avail oeel then do:
    find oeeh use-index k-oeeh where
         oeeh.cono     = g-cono        and
         oeeh.orderno  = oeel.orderno  and
         oeeh.ordersuf = oeel.ordersuf and
         (s-transtype ne "bo" or (s-transtype = "bo" and oeeh.borelfl)) and
         (s-takenby    = ""   or oeeh.takenby   = s-takenby) and

         (oeeh.stagecd >= integer(s-stagecdl)) and
         (oeeh.stagecd <= integer(s-stagecdh)) and

         (s-promisedt  = ?    or oeeh.promisedt >= s-promisedt)

         {oeizl.z99 &user_find_oeizl = "*"
                    &file            = "oeeh"}
    no-lock no-error.
    if not avail oeeh then confirm = no.
end.

if avail oeel then
  find zxinq where zxinq.present = true  no-lock no-error.
else
  find zxinq where zxinq.present = false no-lock no-error.
end.

if v-xmode = "o" and (not avail oeel) and
   ("{1}" = "Prev" or "{1}" = "Last" ) then
  do:
  assign v-xmode = "C"
         confirm = yes.


find last oeelb  use-index k-prod where
         oeelb.cono     = g-cono and
         oeelb.shipprod = s-prod    and
         oeelb.user7   = s-custno  and
/*       (s-shipto     = ""   or oeelb.shipto    = s-shipto) and 
         
         (s-whse = "" or oeelb.whse = s-whse)        and
*/
         (s-transtype <> "bo")                       and
         (s-doonlyfl   = no   or oeelb.botype    = "d") no-lock no-error. 

if avail oeelb then do:
    find oeehb use-index k-oeeh where
         oeehb.cono     = g-cono        and
         oeehb.batchnm  = oeelb.batchnm and
         oeehb.seqno    = oeelb.seqno   and
         oeehb.custno   = s-custno  and
         (s-whse = "" or oeehb.whse = s-whse)        and
         (s-shipto     = ""   or oeehb.shipto    = s-shipto) and

         (s-takenby    = ""   or oeehb.takenby   = s-takenby) and
         (s-transtype  = ""   or s-transtype    = "QU" /* oeehb.transtype */) 
         and 
          
          ((oeehb.stagecd <> 1 and oeehb.stagecd >= integer(s-stagecdl)) or
           (oeehb.stagecd = 1 and integer(s-stagecdl) = 0)) and
          (oeehb.stagecd <= integer(s-stagecdh)) and

         oeehb.sourcepros = "ComQu" and
         oeehb.seqno = 2 and

         (s-promisedt  = ?    or oeehb.promisedt >= s-promisedt)

         {oeizl.z99 &user_find_oeizl = "*"
                    &file            = "oeehb"}
    no-lock no-error.
    if not avail oeehb then confirm = no.
end.
 
if avail oeelb then
  find zxinq where zxinq.present = true  no-lock no-error.
else
  find zxinq where zxinq.present = false no-lock no-error.


end.   


end.

