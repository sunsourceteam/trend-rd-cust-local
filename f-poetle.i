/* SX 55
  f-poetle.i 1.2 3/13/92 */
/*h****************************************************************************
  INCLUDE      : f-poetle.i
  DESCRIPTION  : PO Line items extended screen
  USED ONCE?   : no
  AUTHOR       : jl
  DATE WRITTEN :
  CHANGES MADE :
    05/14/97 jl;  TB# 5594 (4E) Added s-reasunavty
    05/10/99 bm;  TB# 18548 Added expshipdt, reqshipdt
    07/31/02 kjb; TB# e14255 Cubes being pushed to new line
******************************************************************************/
/* f-poetle.i      (po line items extended screen)            */
form
   s-stkqtyord    colon 20   label "Stock Qty Ordered"
   s-stkunit      at 33      no-label
   s-duedt        colon 20   label "Due Date"
/* s-reqshipdt    colon 20   label "Requested Ship Date" */  /* renamed */
   s-reqshipdt    colon 20   label "1st Exp Ship Date"
   s-expshipdt    colon 20   label "Expected Ship Date"
   s-weight       colon 20   label "Weight"
   s-cubes        colon 20   label "Cube"
   s-reasunavty   colon 20   label "Reason Unavailable" skip(1)
"Total Weight      Total Cube" at 6
    s-totlnwt                       at 6  no-label
    s-totlncubes                    at 22 no-label
with frame f-poetle row 7 side-labels overlay centered width 40

