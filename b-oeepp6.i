/* b-oeepp6.i
 
   Include to print caterpillar bar codes.
   
   Parameters: 
               &qtytype  = ORDERED for qty ordered or else qty shipped is used
               
               

*/               





  assign w-addr1 = ""
         w-addr2 = ""
         w-city  = ""
         w-state = ""
         w-zip   = "".
         
  for each oeel use-index k-oeel where oeel.cono = oeeh.cono and
                                       oeel.orderno = oeeh.orderno and
                                       oeel.ordersuf = oeeh.ordersuf
                                                       no-lock: 
      find icsp use-index k-icsp where icsp.cono = oeeh.cono and
                                       icsp.prod = oeel.shipprod
                                       no-lock no-error.
      if avail icsp then do:
         assign sun-descrip = icsp.descrip[1].
      end.
      if not avail icsp then do:
         assign sun-descrip = "DESCRIPTION UNKNOWN".
      end.
      assign substring(sun-description,1,24) = oeel.shipprod.
      assign substring(sun-description,26,15) = oeel.proddesc.
/*      assign order-nbr  = oeeh.orderno. */
      assign cat-po     = oeeh.custpo.
      assign sun-partno = oeel.shipprod.
      assign cat-partno = substring(oeel.reqprod,1,10).
      if cat-partno = "" then do:                                     
         find first icsec use-index k-altprod where
                                    icsec.cono = oeel.cono and
                                    icsec.altprod = oeel.shipprod and
                                    icsec.rectype = "c" and
                                    icsec.custno = 233912
                                    no-lock no-error.
         if avail icsec then
            assign cat-partno = icsec.prod.                           
      end.                                                             
      
/*      if substring(cat-partno,1,3) = "003" then                */
/*         assign cat-classcd = "3".                             */
/*      if substring(cat-partno,1,1) = "4" then                  */
/*         assign cat-classcd = "4".                             */
      assign substring(cat-bar-part,3,10) = cat-partno.       
      
      find icsw use-index k-whse where icsw.cono   = oeeh.cono and
                                       icsw.whse   = oeel.whse and
                                       icsw.prod   = oeel.shipprod 
                                       no-lock no-error.
      if avail icsw then do:
         assign lead-time = icsw.leadtmavg.
         if lead-time > 0 then do:
            assign lead-time = lead-time / 7.
            assign time-frame = "WEEKS".
         end.
         else do:
            assign lead-time = 0.
            assign time-frame = "".
         end.
      end.
      if not avail icsw then do:
         assign lead-time = 0.
         assign time-frame = "".
      end.
         
      assign quant     = if {&qtytype} = "ordered" then
                           integer (oeel.qtyord)
                         else
                           integer(oeel.qtyship).  
      assign req-date  = oeeh.reqshipdt.
      assign promise-date = oeeh.promisedt.
      if oeel.unit = "EA" or oeel.unit = "EACH" then
         assign unit-of-msr = "PC".
      assign disp-qty  = string(quant).
      assign order-qty = string(quant,">>>>>>>").
      do i = 1 to 7:
         if substring(order-qty,i,1) = "" then
            assign substring(order-qty,i,1) = "0".
      end.
      assign substring(cat-bar-qty,3,7) = order-qty.
      assign substring(cat-rlse-nbr,3,7) = substring(oeeh.refer,3,7).
      assign substring(cat-bar-rlse,5,7) = substring(oeeh.refer,3,7).
      assign release-nbr = substring(oeeh.refer,1,9).
      assign substring(cat-srl-nbr,5,7) = substring(oeeh.refer,3,7).
      assign substring(cat-bar-srl,7,7) = substring(oeeh.refer,3,7).
      assign substring(cat-pick-bar-rlse,6,7) = substring(oeeh.refer,3,7).
      assign substring(cat-rlse-nbr-disp,1,5) = "0000-".
      assign substring(cat-rlse-nbr-disp,6,7) = substring(oeeh.refer,3,7).

      assign count = 0.
      assign y = 1.
      do i = 1 to 78:
         assign user3-char = substring(oeeh.user3,i,1).
         if user3-char = "*" then do:
            assign count = count + 1.
            assign y = 1.
            next.
         end.
         if count = 0 then do:
            assign substring(order-type,y,1) = user3-char.
            assign y = y + 1.
         end.
         if count = 1 then do:
            assign substring(deliver-loc,y,1) = user3-char.
            assign y = y + 1.
         end.
         if count = 2 then do:
           assign substring(bin-loc,y,1) = user3-char. /* Bin Loc - Remarks */
         assign y = y + 1.
         end.
         if count = 3 then do:
            assign substring(cat-info,y,1) = user3-char.  /* Dept,Sec,Acct */
            assign y = y + 1.
         end.
         if count = 4 then do:
/*           assign substring(dock-nbr,y,1) = user3-char. Dock not used here */
            assign y = y + 1.
         end.
         if count = 5 then do:
/*            assign substring(sun-description,y,1) = user3-char. */
            assign substring(cat-classcd,y,1) = user3-char.
            assign y = y + 1.
         end.
         if count = 6 then do: 
            assign substring(order-nbr,y,1) = user3-char. 
            assign y = y + 1. 
         end.    

         if count > 6 then leave.  /* positions 7,8,9 and 10 are not used */
     end.
/*     assign bin-loc = substring(deliver-loc,5,2).    Set from User3 above */
     assign dock-nbr = substring(oeeh.user2,20,8).
     assign substring(cat-bar-part,13,1) = cat-classcd.
     assign department  = substring(cat-info,1,3).
     assign section     = substring(cat-info,4,2).
     assign account-nbr = substring(cat-info,6,4).
     assign substring(cat-delv-to,1,2) = substring(deliver-loc,1,2).
     assign city-loc-cd = substring(deliver-loc,1,2).
     assign substring(cat-delv-to,3,1) = "-".
     assign substring(cat-delv-to,4,4) = substring(deliver-loc,3,4).
     
     find zsastz use-index k-primary where zsastz.cono     = 1   and
                                           zsastz.labelfl  = no  and
                                           zsastz.codeiden = "CAT Locations" and
                                           zsastz.primarykey = city-loc-cd
                                           no-lock no-error.
     assign city-loc = "UNDEFINED CITY".
     if avail zsastz then do:
        assign city-loc = zsastz.codeval[1].
     end.
     
     assign count = 0
            y = 1.
     do i = 10 to 78:
        assign user1-char = substring(oeeh.user1,i,1).
        if user1-char = "*" or (i = 78 and substring(oeeh.user1,i,1) = "")
        then do:
           assign count = count + 1
                  y = 1.
           next.
        end.
        if count = 0 then
           assign substring(w-addr1,y,1) = user1-char
                  y = y + 1.
        if count = 1 then 
           assign substring(w-addr2,y,1) = user1-char
                  y = y + 1.
        if count = 2 then 
           assign substring(w-city,y,1) = user1-char
                  y = y + 1.
        if count = 3 then
           assign substring(w-state,y,1) = user1-char
                  y = y + 1.
        if count = 4 then
           assign substring(w-zip,y,1) = user1-char
                  y = y + 1.
        if count > 4 then leave.
     end.
     assign ship-to-addr1 = left-trim(w-addr1)
            ship-to-addr2 = left-trim(w-addr2)
            ship-to-city  = left-trim(w-city)
            ship-to-state = left-trim(w-state)
            ship-to-zip   = left-trim(w-zip).
     
     if order-type = "ZBLOCK" then do:
        assign order-label = "ZWIP BLOCK".
        assign cat-storage = "Z".
        assign cat-locatn  = "BLOCK".
     end.
     if order-type = "ZWIP"   then do:
        assign order-label = "ZWIP".
        assign cat-storage = "".
        assign cat-locatn  = " ZWIP".
     end.
     if order-type = "CONSIG" then do:
        assign order-label = "CONSIGNMENT".
        assign cat-storage = "".
        assign cat-locatn  = " CONSI".
     end.
     
   run generate-barcode.
   if order-label = "ZWIP" then do:
      run generate-picktkt.
   end.
     
  end.

procedure generate-barcode: 
put control "~033~046~1541~117".     /* landscape */
put control "~033~046~15412~104".    /* lines per inch */
put control "~033(10U~033(s1p7v0s0b4148T".  /* small font */
put control "PART NO." "~012".

put control "(P)" "~012" "~012" "~012".    /* skip 3 lines */

put control "  " "~033(10U~033(s1p36v0s0b4148T".  /* large font */
put control cat-partno " " cat-classcd.
put control "~033(10U~033(s1p24v0s0b4148T".       /* medium font */
put control "  " order-label "~012" "~012" "~012".

put control "~033(0Y~033(s0p4.69h12.0v0s0b0T".    /* bar code font */
do i = 1 to 6:
  put control " " cat-bar-part "~012".
/*  assign i = i + 1. */
end.

put control "~033(10U~033(slp6v0s0b4148T".       /* default font */
put control "~033~046~14112~122".           /* adjust cursor to 12th row */
put control "_____________________________".
put control "~012" "~012".
put control "~033(10U~033(s1p7v0s0b4148T".  /* small font */
put control "QUANTITY" "~012".
put control "(Q)" "~012" "~012" "~012".    /* skip 3 lines */
put control "~033(10U~033(s1p36v0s0b4148T".  /* large font */ 
put control " " disp-qty "       " unit-of-msr "~012".
put control "~033~046~14116~122".          /* adjust cursor to 16th row */
put control "~033(0Y~033(s0p4.69h12.0v0s0b0T".    /* bar code font */
do i = 1 to 6:
   put control "                " cat-bar-rlse "~012".
end.

put control "~033~046~14121~122".           /* adjust cursor to 21st row */
do i = 1 to 6:
   put control " " cat-bar-qty "~012".
end.

put control "~033~046~14122~122".           /* adjust cursor to 22nd row */
put control "               ".
put control "~033(10U~033(s1p7v0s0b4148T".  /* small font */  
put control "REQ # / RELEASE NO." "~012".
put control "~033(10U~033(s1p36v0s0b4148T".  /* large font */
put control "                   ".
put control "~033(10U~033(s1p7v0s0b4148T".  /* small font */ 
put control " " "(K)" "~012" "~012" "~012".
put control "~033(10U~033(s1p36v0s0b4148T".  /* large font */
put control "                    " cat-rlse-nbr "~012".
put control "~033~046~14126~122".            /* adjust cursor to 26th row */
put control "~033(10U~033(slp6v0s0b4148T".       /* default font */       
put control "_____________________________" "~012" "~012".
put control "~033(10U~033(s1p7v0s0b4148T".  /* small font */   
put control "SUPPLIER".
put control "~033(10U~033(s1p36v0s0b4148T".  /* large font */
put control "                     ".
put control "~033(10U~033(s1p7v0s0b4148T".  /* small font */   
put control "ENGINEERING CHANGE" "~012".
put control "(V)" "~012" "~012" "~012".
put control "~033(10U~033(s1p36v0s0b4148T".  /* large font */
put control " " supplier-cd "~012" "~012" "~012".
put control "~033(0Y~033(s0p4.69h12.0v0s0b0T".    /* bar code font */
do i = 1 to 6:
   put control " " cat-bar-sup "~012".
end.

put control "~033~046~14140~122".            /* adjust cursor to 40th row */
put control "~033(10U~033(s1p26v0s0b4148T".  /* default font */
put control "__________________________________" "~012" "~012".
put control "~033(10U~033(s1p7v0s0b4148T".  /* small font */
put control "SERIAL".
put control "~033(10U~033(s1p36v0s0b4148T".  /* large font */
put control "                      ".
put control "~033(10U~033(s1p7v0s0b4148T".  /* small font */
put control "DELIVERY LOCATION" "~012".
put control "(S)" "~012" "~012" "~012".
put control "~033(10U~033(s1p36v0s0b4148T".  /* large font */
put control " " cat-srl-nbr "~012".
put control "                        " cat-delv-to "~012" "~012".
put control "~033(0Y~033(s0p4.69h12.0v0s0b0T".    /* bar code font */ 
do i = 1 to 6:
   put control " " cat-bar-srl "~012".
end.
put control "~033~046~14149~122".            /* adjust cursor to 49th row */
put control "                   ".

if ship-to-addr1 = "" and
   ship-to-addr2 = "" and
   ship-to-city  = "" and
   ship-to-state = "" and
   ship-to-zip   = "" then do:
   put control "~033(10U~033(s1p12v0s0b4148T".  /* large font */
   put control city-loc "~012".
end.
else do:
   put control "~033(10U~033(s1p9v0s0b4148T".  /* smaller font */
   put control ship-to-name  "~012" "~012" "                             ".
   put control "                                                         ".
   put control "                ".
   put control "~033(10U~033(s1p9v0s0b4148T".  /* smaller font */
   put control ship-to-addr1 "~012" "~012" "                             ".
   put control "                                                         ".
   put control "                ".
   put control "~033(10U~033(s1p9v0s0b4148T".  /* smaller font */
   put control ship-to-addr2 "~012" "~012" "                             ".
   put control "                                                         ".
   put control "                ".
   put control "~033(10U~033(s1p9v0s0b4148T".  /* smaller font */
   put control ship-to-city "   " ship-to-state "   " ship-to-zip "~012".
end.

put control "~033~046~14154~122".            /* adjust cursor to 54nd row */
put control "~033(10U~033(s1p26v0s0b4148T".  /* large font */
put control "__________________________________" "~012".
put control "~033~046~14130~122".            /* adjust cursor to 30th row */
put control "~033(10U~033(s1p24v0s0b4148T".  /* large font */
do i = 1 to 25:                              /* create vertical line */ 
   put control "                                   " "|" "~012".
end.
put control "~012". 
put control "~033(10U~033(s1p7v0s0b4148T".  /* small font */ 
put control "DESCRIPTION" "~012" "~012".
put control "~033(10U~033(s1p10v0s0b4148T".  /* type font */ 
put control " " sun-description "             ".
put control "                   " sun-descrip "~012".
put control "~033(10U~033(s1p26v0s0b4148T".  /* large font */
put control "__________________________________" "~012" "~012".
put control "                           ".
put control "~033(10U~033(s1p7v0s0b4148T".  /* small font */
put control "STORAGE LOCATION INPLT/ASSEM/PART" "~012".
put control "~033(10U~033(s1p10v0s0b4148T".  /* type font */ 
put control " " "Purchase Order # ".
put control cat-po "~012" "~012".
put control "~033(10U~033(s1p26v0s0b4148T".  /* large font */
put control "                           ".
put control "~033(10U~033(s1p10v0s0b4148T".  /* type font */
put control cat-storage "       " cat-locatn "      ".
put control "** " order-label " **" "~012".
put control "~033(10U~033(s1p26v0s0b4148T".  /* large font */
put control "_______________________" "~012".
put control "                                    ".
put control "~033(10U~033(s1p10v0s0b4148T".  /* type font */
put control "LEAD TIME: " lead-time "  " time-frame "~012" "~012".
put control "        " substring(cat-delv-to,1,2) "      ".
put control substring(cat-delv-to,4,4) "~012".
put control "~033(10U~033(s1p26v0s0b4148T".  /* large font */   
put control "_______________________" "~012". 
put control "~033(10U~033(s1p26v0s0b4148T".  /* large font */
put control "                                    ".
put control "~033(10U~033(s1p10v0s0b4148T".  /* type font */
put control "Supplier Part #" "~012" "~012".
put control "~033(10U~033(s1p26v0s0b4148T".  /* large font */
put control "                                    ".
put control "~033(10U~033(s1p10v0s0b4148T".  /* type font */
put control sun-partno /* "SB15120                " */ "~012".
put control "~033(10U~033(s1p26v0s0b4148T".  /* large font */
put control "__________________________________" "~012" "~012".
put control "~033~046~14163~122".            /* adjust cursor to 63rd row */
put control "~033(10U~033(s1p24v0s0b4148T".  /* large font */
do i = 1 to 11:
   put control "                            " "|" "~012".
end.
put control "~033~046~14165~122".            /* adjust cursor to 65th row */
do i = 1 to 9:
   put control "                                " "|" "~012".
end.
put control "~033~046~14165~122".            /* adjust cursor to 65th row */
do i = 1 to 9:
   put control "                                     " "|" "~012".
end.
put control "~033~046~14169~122".            /* adjust cursor to 69th row */
do i = 1 to 5:
   put control "      " "|" "~012".
end.
put control "~033~046~14169~122".            /* adjust cursor to 69th row */
do i = 1 to 5:
   put control "            " "|" "~012". 
end.

put control "~033~046~14175~122".            /* adjust cursor to 75th row */
put control "~033(10U~033(s1p7v0s0b4148T".  /* small font */
put control "REMARKS" "~012".
put control "~033(10U~033(s1p10v0s0b4148T".  /* type font */ 
put control "          " bin-loc.
put control "                                                                ".
put control "           " req-date "~012" "~012".
put control " " "REQ #" release-nbr "~012" "~012".
put control " " "DEL TO:   " dock-nbr.
put control "  " "ORDER # " order-nbr.
put control "     " "DEPT  "    department.
put control "     " "SEC  "     section "~012" "~012".
put control " " "ACCT "    account-nbr.
put control "    " "EQP # "   equip-nbr "~012" "~012".
put control " " "ORD QTY " order-qty.
put control "     " promise-date.
put control "                                    " ">> Supplier Original <<".
put control "~012" "~012" "~012" "~012" "~012".
put control "~033(10u~033(s1p12v0s3b4101T".
put control "    ".
put control 
    "SunSource Technology Services - Serving Industry for over 50 years". 
end procedure.  /* create-barcode */




procedure generate-picktkt:
put control "~033~105".                     /* reset to page */
put control "~033~046~15412~104".                 /* lines per inch */

put control "~033(10U~033(s1p10v0s0b4148T".       /* small font */
put control "     " cat-po "~012" "~015".

put control "~033~046~14103~122".           /* adjust cursor to 3rd row */
put control "~033(10U~033(s1p24v0s0b4148T".       /* medium font */
put control "                                  " "PICK" "~015".

put control "~033~046~14109~122".           /* adjust cursor to 9th row */
put control "~033(0Y~033(s0p4.69h12.0v0s0b0T".    /* bar code font */
do i = 1 to 6:
   put control "             " cat-pick-bar-rlse "~012" "~015".
end.
put control "~033~046~14117~122".           /* adjust cursor to 17th row */
put control "~033(10U~033(s1p14v0s0b4148T".       /* small font */
put control "                                                       ".
put control cat-rlse-nbr "~012" "~015".

put control "~033~046~14133~122".           /* adjust cursor to 33rd row */
put control "~033(10U~033(s1p100v0s0b4148T".       /* large font */
put control "  " cat-delv-to "~015".

put control "~033(10U~033(s1p10v0s0b4148T".        /* small font */
put control "~033~046~14137~122".           /* adjust cursor to 37th row */
assign pick-date = string(TODAY,"99/99/99").
assign pick-time = string(TIME,"HH:MM").
put control "     " pick-date "   " pick-time "~012" "~015".

/* Build Form */
/* Horizontal Lines */
put control "~033~046~14138~122".           /* adjust cursor to 38th row */
put control "~033(10U~033(slp6v0s0b4148T".         /* default font */
put control "_____________________________________" "~012" "~015".
put control "~033~046~14142~122".           /* adjust cursor to 42nd row */
put control "_____________________________________" "~012" "~015".
put control "~033~046~14148~122".           /* adjust cursor to 48nd row */
put control "_____________________________________" "~012" "~015".
put control "~033~046~14152~122".           /* adjust cursor to 52st row */
put control "_____________________________________" "~012" "~015". 
put control "~033~046~14158~122".           /* adjust cursor to 58th row */
put control "_____________________________________" "~012" "~015".
put control "~033~046~14162~122".           /* adjust cursor to 62th row */
put control "_____________________________________" "~012" "~015".
put control "~033~046~14166~122".           /* adjust cursor to 66nd row */
put control "_____________________________________" "~012" "~015".
put control "~033~046~14170~122".           /* adjust cursor to 70th row */
put control "~033(10U~033(s1p23v0s0b4148T".       /* small font */ 
put control "                                                         ".
put control "___________" "~012" "~015".
put control "~033~046~14174~122".           /* adjust cursor to 74th row */
put control "                                                         ".
put control "___________" "~012" "~015".


/* Vertical Lines */

put control "~033~046~14142~122".           /* adjust cursor to 42th row */
put control "~033(10U~033(slp6v0s0b4148T".         /* default font */
put control "        " "|" "        " "|" " " "|" "  " "|" "    " "|" "~012".
put control "~015".
put control "~033~046~14152~122".           /* adjust cursor to 52th row */
put control "       " "|" "       " "|" "      " "|" "       " "|" "~012".
put control "~015".
put control "~033~046~14156~122".           /* adjust cursor to 56th row */ 
do i = 1 to 3:
   put control "                   " "|" "~012" "~015".
end.
put control "~033~046~14158~122".           /* adjust cursor to 58th row */
put control "                      " "|" "   " "|" "~012" "~015".
put control "~033~046~14162~122".           /* adjust cursor to 62nd row */
put control "   " "|" "   " "|" "       " "|" "   " "|" "  " "|" "     " "|"  "~  " "|" "~012" "~015".
put control "~033~046~14166~122".           /* adjust cursor to 66th row */
put control "   " "|" "   " "|" "       " "|" "   " "|" "  " "|" "     " "|" 
 "~012" "~015".
put control "~033~046~14170~122".           /* adjust cursor to 70nd row */
put control "                            " "|" "  " "|" "~012" "~015".
put control "~033~046~14174~122".           /* adjust cursor to 74th row */
put control "                            " "|" "~012" "~015".
 
/* Load Form Headers */
/* Row 1 */
put control "~033~046~14140~122".           /* adjust cursor to 40th row */
put control "~033(10U~033(s1p7v0s0b4148T".       /* small font */
put control "IDENT/ITEM" "                                       ".
put control "LOAD CTL NO." "                                    ".
put control "RSF" "        " "E/C" "              " "OPER".
put control "                         " "SERIAL NO." "~012" "~015".

/* Row 2 */
put control "~033~046~14144~122".           /* adjust cursor to 44th row */
put control "DESCRIPTION" "~012" "~015".

/* Row 3 */
put control "~033~046~14150~122".           /* adjust cursor to 50th row */
put control "PO/INPLT/ASSEM/PART" "                " "QUANTITY/UM".
put control "                              " "PKG QTY".
put control "                                " "TOT RECD/ DEL/BAL".
put control "                       " "TRAFFIC/SCHED" "~012" "~015".

/* Row 4 */
put control "~033~046~14154~122".           /* adjust cursor to 54th row */
put control "                                                               ".
put control "                                                               ".
put control "               STORAGE LOCATION" "~012" "~015".

/* Row 5 */
put control "~033~046~14160~122".           /* adjust cursor to 60th row */
put control "                                                               ".
put control "                                                               ".
put control "                                                         ".
put control "MATL CTL" "     " "SH" "~012" "~015".

/* Row 6 */
put control "~033~046~14164~122".           /* adjust cursor to 64th row */   
put control "                                                               ".
put control "                                                               ".
put control "                                                         ".      
put control "DATE" "~012" "~015".

/* Row 7 */
put control "~033~046~14168~122".           /* adjust cursor to 68th row */   
put control "REMARKS                                                 ".
put control "                                                               ".
put control "                                                         ".      
put control "INSP" "             " "SH" "~012" "~015".

/* Row 8 */
put control "~033~046~14172~122".           /* adjust cursor to 72nd row */   
put control "                                                               ".
put control "                                                               ".
put control "                                                         ".      
put control "DATE" "~012" "~015".



/* Load Data */
/* Row 1 */
put control "~033~046~14142~122".            /* adjust cursor to 42th row */
put control "~033(10U~033(s1p10v0s3b4148T".  /* medium font, bold */
put control "  " cat-classcd "  ".
put control "~033(10U~033(s1p14v0s3b4148T".  /* medium font, bold */
put control cat-partno.
put control "~033(10U~033(s1p10v0s0b4148T".  /* medium font */
put control "                    " cat-rlse-nbr-disp "~012" "~015".

/* Row 2 */
put control "~033~046~14146~122".            /* adjust cursor to 46th row */
put control "                 " sun-partno " " sun-descrip "~012" "~015".

/* Row 3 */
put control "~033~046~14152~122".            /* adjust cursor to 52th row */
put control "                                                      ". 
put control disp-qty " " unit-of-msr "~012" "~015".

/* Row 4 */
put control "~033~046~14156~122".            /* adjust cursor to 56th row */
put control "  PO# " cat-po.
put control "~033~046~14157~122".            /* adjust cursor to 57th row */
put control "                                                               ".
put control "    " cat-storage "        " cat-locatn "~012" "~015".

/* Row 5 */
put control "~033~046~14162~122".            /* adjust cursor to 62nd row */
put control "  " city-loc-cd "           " substring(cat-delv-to,4,4)
            "~012" "~015".

/* Remarks Area */
put control "~033~046~14172~122".            /* adjust cursor to 72nd row */
put control "  " "REQ # " release-nbr "~012" "~015".
put control "~033~046~14175~122".            /* adjust cursor to 75th row */
put control "  " "DEL TO:   " dock-nbr.                
put control "    " "ORDER # " order-nbr.                
put control "    " "DEPT  "    department.           
put control "    " "SEC  "     section "~012" "~015".
put control "~033~046~14178~122".            /* adjust cursor to 78th row */
put control "  " "ACCT "  account-nbr.               
put control "    " "EQP # "   equip-nbr "~012" "~015".
put control "~033~046~14181~122".            /* adjust cursor to 81st row */
put control "  " "ORD QTY " order-qty.                 
put control "     " promise-date.                     

end. /* procedure */

