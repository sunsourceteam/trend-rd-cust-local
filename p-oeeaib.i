/* p-oeeaib.i 1.1 01/03/98 */
/* p-oeeaib.i 1.1 01/03/98 */
/*h*****************************************************************************  INCLUDE      : p-oeeaib.i
  DESCRIPTION  : Fields in "notes" record in edi 810, 843, and 855.
  USED ONCE?   : no
  AUTHOR       : jlc
  DATE WRITTEN : 02/23/94
  CHANGES MADE :
*******************************************************************************/
"notes " +
    s-note