if index(v-start,"*") ne 0 then 
  do:
  find {1} arss use-index k-arss  where
           arss.cono = g-cono and
           arss.custno = s-custno and
           arss.shipto matches v-start and
           (s-openfl = false or arss.jobclosedt = ? or arss.jobclosedt > today)
  no-lock no-error.
  end.
else
  do:
  find {1} arss use-index k-arss where 
           arss.cono = g-cono and
           arss.custno =s-custno and
       {2}
           arss.shipto >= v-start and
      /{2}*  */
          (s-openfl = false or arss.jobclosedt = ? or arss.jobclosedt > today)

          no-lock no-error.
  end.
if avail arss then
  assign
    s-addr1 = if arss.addr[1] <> "" then arss.addr[1] else arss.addr[2]
    s-select = arss.shipto
    s-slsrep = if arss.slsrepout <> "" then arss.slsrepout else arsc.slsrepout
    s-cityst = if arss.jobdesc ne "" then
                 "Job: " + arss.jobdesc 
               else
               if arss.state = "" then 
                 "Loc: " + arss.city
               else
                 "Loc: " + arss.city + ", " + arss.state.