/* poehb.sch */
if v-bufferedsearch then do:
  assign sr-prod  = if substring(v-start,1,2) = "P%" then
                      substring(v-start,3,(length(v-start) - 2))
                    else
                      v-start.
  if v-vend-accum <> 0 then do:
    assign v-prodsrcha = sr-prod.
    /* UPC added code */
    run improvise_product ( input tpoehb.vendno,
                            input v-vend-accum-edi,
                            input "Product", 
                            input-output v-prodsrcha).
    /* UPC added code */
    if not can-find(first tpoehb where 
                      tpoehb.prod begins sr-prod no-lock) and
       not can-find(first tpoehb where 
                      tpoehb.vprod begins sr-prod no-lock) and
       not can-find(first tpoehb where 
                      tpoehb.prod begins v-prodsrcha no-lock) then        
      run vendor_special_information (input v-vend-accum,
                                      /* SDI DANFOSS 11/14/05    */
                                      input v-vend-accum-edi,
                                      /* SDI DANFOSS 11/14/05    */
                                      input "Product",
                                      input-output sr-prod).
  end.
  assign v-srchfnd  = false
         v-bufferedsearch = false.
  run srch-brws (input-output sc-recid).
  if sc-recid <> ? then do:
    find srch where recid(srch) = sc-recid no-lock no-error.
    find tpoehb where recid(tpoehb) = srch.srecid use-index k-temp1 no-error.
    if avail tpoehb then do:  
      hide frame f-zz.
      hide frame f-zpoinfo2.
      assign confirm = true
             v-searchfl = true
             v-nextfl = true
             v-curpos = 1
             v-recid  = 0.
      clear frame f-poehb all no-pause.
      input clear.
      readkey pause 0.
      next top.
    end.
    else do:
      hide frame f-zz. 
      hide frame f-zpoinfo2.
      readkey pause 0.
      next dsply.
    end.
  end.
  else do:
    if toggle-sw = no then do:
      find tpoehb use-index k-temp1 where
        recid(tpoehb) = v-recid[1] no-error.
      find prev tpoehb use-index k-temp1 no-error.
      if not avail tpoehb then v-newdisplay = true.
    end.
    else do:
      find tpoehb use-index k-temp2 where
        recid(tpoehb) = v-recid[1] no-error.
      find prev tpoehb use-index k-temp2 no-error.
      if not avail tpoehb then v-newdisplay = true.
    end.
    hide frame f-zz. 
    hide frame f-zpoinfo2.

    assign confirm = false
           v-searchfl = false
           v-nextfl = true
           v-curpos = 1
           v-recid  = 0.
    clear frame f-poehb all no-pause.
    input clear.
    readkey pause 0.
    next top.
  end.
end.