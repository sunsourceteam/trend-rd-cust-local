/* main function is taken from z-oeetln.i */


zsdi-marqty    =  {&oeel}qtyord

/*d Calculate the price and discount to use for the net calculation.
    Since Bid Prep now defaults s-csunperstk to 1 (not zero) and
    catalog items are treated like stock items, g-ourproc is
    checked to see which calculation should be used.
    All programs calling a-oeetln.i should be using v- in the &icss param to
    handle the nonstocks that are changed to lost business */

zsdi-marprice  = {&oeel}price *
                 (if ({&oeel}specnstype = "n" or {&icss}speccostty = "") and
                    not can-do("bpet,bpes",substring(g-ourproc,1,4)) then 1
                  else ({&icss}csunperstk * v-conv))
zsdi-mardisc   = if not {&oeel}disctype then zsdi-marprice * ({&disc} / 100)
                 else {&oeel}discamt *
                    (if ({&oeel}specnstype = "n" or {&icss}speccostty = "") and
                     not can-do("bpet,bpes",substring(g-ourproc,1,4)) then 1
                     else ({&icss}csunperstk * v-conv))

  /*d Calculate the net amount of the line item */
zsdi-netamt    = if (v-maint-l = "a")                or
                  can-do("n,l",{&oeel}specnstype) or
                 ({&icss}speccostty <> "c")       or
                 ({&oeel}chrgqty = 0) then
                 round((zsdi-marprice * zsdi-marqty),2) -
                 round((zsdi-mardisc  * zsdi-marqty),2)
              else
  /*e Calculate the net for a catch weight product
            when the charge qty has been entered  */
                round(({&oeel}price * {&oeel}chrgqty),2) -
                 (if not {&oeel}disctype then
                    round((({&oeel}price * ({&disc} / 100))
                    * {&oeel}chrgqty),2)
                  else round(({&disc} * {&oeel}chrgqty),2))

  /*d Calculate the net amount of the line item */
zsdi-netamt2   = if (v-maint-l = "a")                or
                  can-do("n,l",{&oeel}specnstype) or
                 ({&icss}speccostty <> "c")       or
                 ({&oeel}chrgqty = 0) then
                 (zsdi-marprice * zsdi-marqty) -
                 (zsdi-mardisc  * zsdi-marqty)
              else
  /*e Calculate the net for a catch weight product
            when the charge qty has been entered  */
                ({&oeel}price * {&oeel}chrgqty) -
                 (if not {&oeel}disctype then
                    (({&oeel}price * ({&disc} / 100))
                    * {&oeel}chrgqty)
                  else round(({&disc} * {&oeel}chrgqty),2))

zsdi-margamt   = ((zsdi-marprice * zsdi-marqty) -
                  (zsdi-mardisc * zsdi-marqty)) -
                  (s-prodcost * zsdi-marqty) 
zsdi-margamt   = round(zsdi-margamt,2)



