/* oeir.p 1.1 01/02/98 */
/* oeir.p 1.24 10/01/97 */
/*h*****************************************************************************
  PROCEDURE     : oeir
  DESCRIPTION   : OE credit release
  AUTHOR        : rhl
  DATE WRITTEN  : 09/19/89
  CHANGES MADE  :
*******************************************************************************/{g-xxex.i}
{g-ar.i}
{g-oe.i}
{g-ic.i}
{g-inqpd.i}
{g-print.i}
{g-oestg.i}
{g-oei99.i}
{oerah.gva "new"}
{oeir.lva "new"}

/*e BUFFERS */
def buffer b-arsc for arsc.
def buffer b-oeeh for oeeh.
def buffer b-oeehc for oeehc.

/*e SHARED variables */
def     shared var v-oereqdays  like sasc.oereqdays         no-undo.
def     shared var v-idoeeh     as recid                    no-undo.
def     shared var v-oercptfrmt as i                        no-undo.
def     shared var v-oelinefl   like sasc.oelinefl          no-undo.
def     shared var v-holdoverfl like sasoo.holdoverfl       no-undo.

def            var o-approvty   like oeeh.approvty          no-undo.
def            var o-custno     like g-custno               no-undo.
def            var o-secure     as i format "9"             no-undo.

def            var s-formend    as c format "x"             no-undo.

def            var v-auth       as logical                  no-undo.
def            var v-errfl      as logical                  no-undo.
def            var v-fkey       as c format "x(5)" extent 5
                                initial ["","","","",""]    no-undo.
def            var v-idarsc     as recid                    no-undo.
def            var v-initfl     as logical                  no-undo.
def            var v-pick       as c format "x"             no-undo.
def            var v-wsize      as i initial 9999999        no-undo.

/*e Variables for days forward setting */
def            var v-days       like sasc.oereqdays         no-undo.
def            var v-forward    like icsd.jitdays   init 0  no-undo.
def            var v-odate      like oeeh.promisedt         no-undo.
def            var v-promisedt  like oeeh.promisedt         no-undo.
def            var v-sat        as i                init 0  no-undo.
def            var v-sun        as i                init 0  no-undo.

/*tb e7852 09/15/00 lcg;  Added new variable for currency display  */
def            var s-currencyty as char format "x(12)"      no-undo.

def            var lCCInstalled as l                        no-undo.
def            var cCCInstalled as char                     no-undo.
def            var lCCWarning   as l                        no-undo.

/* tb 25308 added user hook, &user_defvar */
/*
{oeir.z99 &user_defvar = "*"}
*/

def var xh-custno like arsc.custno no-undo init 0.
def var xh-shipto like arss.shipto no-undo init "".
def var x-focuscust as logical no-undo init false.
def var s-takenby   like oeeh.takenby no-undo.

/*tb 16304 08/08/94 dww; Lookup Indicator Missing from Whse */
{valmsg.gas &string = "2007,4601,4303,3738"}

/*d Assign initial variables */
assign
  v-length = 7
  v-msg    =
        " Order #    Appr Ty Stg Promised   Total Order Taken Whse Cr Mgr  Customer Pri"
.

/*tb 16304 08/08/94 dww; Lookup Indicator Missing from Whse */
/*tb e7852 07/28/00 lcg; AR Multicurrency Add currency display */
/*e FORMS */
form /* Banner form */
  "  Customer #   Takenby   "        at 1
  "Type  Whse"                       at 28
  "Orders Ready   Beg Dt    End Dt"  at 40
  "Appr"                             at 74
  g-custno                           at  2
    validate({v-arsc.i g-custno}, {valmsg.gme 4303})
  {f-help.i}
  arsc.notesfl                       at 14
  s-takenby                          at 19
  s-transtype                        at 29
    validate(if can-do("so,do,rm,cr,bo,br,,cs",s-transtype) then true
             else false, {valmsg.gme 3738})
    help "SO, DO, RM, CR, BR, BO, CS or Blank"
  s-whse                             at 34
    validate({v-icsd.i s-whse}, {valmsg.gme 4601})
    help "Enter a Warehouse or Blank for All                          [L]"
  s-shipfl                           at 44
    help "If Yes, Only Orders With a Ship Quantity Will be Displayed"
  s-begdt                            at 54
  s-enddt                            at 64
  s-approvtys                        at 75
    validate(s-approvtys ne "y", {valmsg.gme 2007})
    help "Enter Approve Type or Blank for All On Hold"
  arsc.lookupnm                      at 2
  s-currencyty                       at 57
  v-msg                              at 1
  skip(13)
  s-formend                          at 1
    with frame f-oeirs no-labels centered no-underline title g-title width 80
       overlay.

form /* Line screen */
  s-order                             at 1
  oeeh.notesfl                        at 14
  s-fpty                              at 15
  oeeh.approvty                       at 17
  oeeh.transtype                      at 21
  s-stagecd                           at 24
  oeeh.promisedt                      at 28
  s-dolinety                          at 36
  oeeh.totinvamt                      at 38
  oeeh.takenby                        at 52
  oeeh.whse                           at 57
  oeehc.creditmgr                     at 63
  oeeh.custno                         at 67
  arsc.notesfl                        at 42
  oeehc.priority                      at 43
  arsc.name                           at 46
    with frame f-oeir overlay no-labels width 78 column 2 scroll 1
    no-box no-underline row 6 v-length down.

pause 0 before-hide.

/*d Load Globals SASC etc. */
run oeebti.p.

/*tb 17154 11/23/94 kjb; Add user include */
/*
{oeir.z99 &use_bu = "*"}
*/

 if g-menusel = "im" then
   s-shipfl = no.
 assign x-checkfocus = true.
 

/*Determine if the Credit Card Third Party Interface is installed*/
/*
run tpc.p(input "CreditCard":u,
          output cCCInstalled).
lCCInstalled = if cCCInstalled = "yes" then yes else no.
*/

/* MAIN PROCESSING */
main:
do while true with frame f-oeir on endkey undo main, leave main:

  /*d Housekeeping */
  /*{p-setup.i f-oeirs}*/
  if {k-jump.i} then leave main.
  if {k-cancel.i} then hide message no-pause.
  clear frame f-oeirs no-pause.
  
  clear frame f-oeir all.
  hide frame f-oeir.
  view frame f-oeirs.

  display v-msg with frame f-oeirs.
  color display input v-msg with frame f-oeirs.
  o-custno = ?.
  put screen row 21 col 3 color message "                                                                            ".
  if v-errfl = no then hide message no-pause.
                  else v-errfl = no.

  /*d Updating logic */
  update
    g-custno
    s-takenby 
    s-transtype
    s-whse
    s-shipfl
    s-begdt
    s-enddt
    s-approvtys
      with frame f-oeirs editing:

    readkey.
    if {k-after.i} then do for arsc:
      {t-custno.i &field   = g-custno
                  &frame   = f-oeirs
                  &display = "arsc.lookupnm"}
      /*tb e7852 09/15/00 lcg;  Added currency display  */
      if avail arsc and arsc.currencyty ne "" then do:
        s-currencyty    = "Currency: " + arsc.currencyty.
        display s-currencyty with frame f-oeirs.
      end.
      else do:
        s-currencyty    = "".
        display s-currencyty with frame f-oeirs.
      end.
    end.
  if {k-after.i} then
    if g-custno <> xh-custno then
      assign g-custno
          x-checkfocus = true
          xh-custno = g-custno.


    apply lastkey.

  end. /* editing loop */

  if g-custno = {c-empty.i} then
    display "" @ arsc.notesfl "" @ arsc.lookupnm with frame f-oeirs.

  /*d Check security */
  g-currproc = "oeir".

  /*
  do for sassm, sasos:
    {p-secure.i g-currproc}
  end.

  if g-secure < 3 then do:
    /* Inquiry Only */
    run err.p(1107).
    {pause.gsc}
  end.
  */
  
  /*d Scrolling screen necessary assignments */
  assign
    s-custno   = g-custno
    v-scrollfl = true.
  put screen row 21 col 3 color message g-bottom.
  /*d Scrolling screen logic.  Scoped on oeehc to prevent share lock */
  /*tb 21537 10/01/97 kjb; Removed &add parameter.  We were passing a value
       of 'no' but x-xxex.i only expects either a value of 'yes' or no value
       at all. */
  do for oeehc:
    {x-xxex.i
       &file       = "oeehc"
       &frame      = "f-oeir"
       &find       = "n-oeizc.i"
       &field      = "s-order"
       &display    = "d-oeir.i"
       &usefile    = "p-oeirx.i"
       &add        = "no"
       &action     = a-oeir.i}
  end.

  leave main.

end. /* main block */

if g-lkupfl then hide frame f-oeir.

/*d User hook */
/*
{oeir.z99 &user_001 = "*"}
*/
if g-lkupfl = yes then hide frame f-oeirs no-pause.
