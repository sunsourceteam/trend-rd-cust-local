/* h-ares5.i 1.8 7/13/93 */
/*h*****************************************************************************
  INCLUDE               : h-ares5.i
  DESCRIPTION           : Heading assignments for ARES1 - Standard AR Statements
  USED ONLY ONCE?       :
  AUTHOR                :
  DATE WRITTEN          :
  CHANGES MADE AND DATE : 07/16/92 pap; TB# 6829 - Regardless of "Print heading
                                         flag, print the "Remit To" information
                          tb 11968 06/24/93 jrg; Added headings for the period
                                        balances to containing SVs and Credits
                                        when the Special Transaction
                                        flag is set.
                          tb 12196 07/13/93 jrg; Updated h-footerln* to Trend
                                        standards.
                          wpp 02/05/99 Change header and footer vars.
                          10/27/00 - ceg - 00-84 - Stuff to increase 
                          x-ponum size 
                          
*******************************************************************************/
/*tb 6829 07/16/92 pap; Print "Remit To" information in all cases */

{w-sasc.i no-lock}

assign
    h-coname            = if avail sasc then sasc.name else ""
    h-coaddr[1]         = if avail sasc then sasc.addr[1] else ""
    h-coaddr[2]         = if avail sasc then sasc.addr[2] else ""
    h-cocitystzip       = if avail sasc
                          then sasc.city + ", " + sasc.state + " " + sasc.zipcd
                          else "".

if v-headingfl
then do:
    /*e Print all the headings with the information */
    assign
        h-remitto           = "Remit To:"
        h-custno            = "Customer #"
        h-stmtdt            = "Statement Date"
        h-totdue            = "Total Due"
        h-amtpaid           = "Amount Paid"
        h-amtpaidln         = "$_______________"
        h-remit1            = "Please Return This Portion"
        h-remit2            = "With Your Remittance"
        h-uline             = fill("-",80)
        h-stmtdt2           = h-stmtdt
        h-custno2           = h-custno
        h-headingln         = if p-referfl then
"Inv Date Due Date Type Status  Invoice #/Reference         Charge       Credit"
                              else

/* wpp 02/05/99 p-referfl will always be no per assignment in ares.z99. */
"Inv Date Type Invoice #   PO Number           Inv Amt     Amt Pd      Amt Due"

/* wpp 02/05/99 Replace this*********************************************
     h-footerln1         =
    "      Current       Period 2       Period 3       Period 4       Period 5"
 ************************************************************************/

     /* wpp 01/30/99 Reassign the h-footerln1 var. */
     h-footerln1 = "      Current       30+ Days       45+ Days       60+ Days"
                 + "        Over 90"
 
/*tb 11968 06/24/93 jrg; Added headings. */
        h-footerln2n        =
"  Service Chg     Future Due       On Order    Svc Chg YTD"
        h-footerln2b        =
"  Memo SV Chg     Future Due       On Order    Svc Chg YTD"
        h-footerln2s        =
"  Memo SV Chg     Future Due       On Order    Svc Chg YTD"
        h-footerln2m        =
"  Service Chg     Future Due       On Order    Svc Chg YTD".


end.

p-stmtdt2      = p-stmtdt.
