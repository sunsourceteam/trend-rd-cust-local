
/* g-kpep.i 1.1 01/03/98 */
/* g-kpep.i 1.1 2/16/92 */
/*h*****************************************************************************
  INCLUDE      : g-kpep.i
  DESCRIPTION  : Kit Production Print Work Orders - Shared Variables Include
  USED ONCE?   : no
  AUTHOR       : mtt
  DATE WRITTEN : 06/27/95
  CHANGES MADE :
    06/26/95 mtt; TB# 18611 Make changes for KP Work Order Demand (W1)
    06/04/96 gp;  TB# 19484 (C6) Warehouse Logistics - KPEP.  Change Whse
        range to an option.
    09/13/96 bmf; TB# 21702 Trend 8.0 oper2 enhancement - removed
        references to oper2 in the code
    10/26/96 mtt; TB# 21254 (2F) Develop Value Add Module
    06/11/01 bpa; TB# e8862 PB Kit rework
    07/19/01 bpa; TB# e8862 More PB Kit Rework
*******************************************************************************/

def {1} shared var p-listfl          as l                         no-undo.
def {1} shared var p-whse            like kpet.whse               no-undo.

def {1} shared var b-wono            like kpet.wono               no-undo.
def {1} shared var e-wono            like kpet.wono               no-undo.
def {1} shared var b-enterdt         as date                      no-undo.
def {1} shared var e-enterdt         as date                      no-undo.

def {1} shared var v-modwmfl         like sasa.modwmfl            no-undo.
def {1} shared var v-needed          like kpet.qtyord             no-undo.
def {1} shared var s-commit         as dec format "zzzzzzzz9.99-" no-undo.
def {1} shared var s-reserve        as dec format "zzzzzzzz9.99-" no-undo.

/*tb 21702 09/13/96 bmf; Changed definition of v-oper2 from like sapb.oper2
    to like sasoo.oper2*/
def {1} shared var v-oper2           like sasoo.oper2             no-undo.

/* Warehouse Manager - Auto allocate (pull) process - oeetlwma.p */
def {1} shared var s-kitfl           like oeel.kitfl              no-undo.
def {1} shared var v-stkqtyship      like oeel.stkqtyship         no-undo.
def {1} shared var g-shipprod        like oeel.shipprod           no-undo.
def {1} shared var v-whse            like oeel.whse               no-undo.
def {1} shared var s-returnfl        like oeel.returnfl           no-undo.
def {1} shared var v-wmqtyship       like oeel.wmqtyship          no-undo.
def {1} shared var s-lineno          like oeel.lineno             no-undo.
def {1} shared var v-wmpprimfl       like sasc.wmpprimfl          no-undo.
/*tb 21254 10/26/96 mtt; Develop Value Add Module */
def {1} shared var v-valineitemfl    as l                         no-undo.

def            var v-msg             as c format "x(30)"          no-undo.
def            var v-stagecd         like kpet.stagecd            no-undo.
def            var v-totputqtys      like poeh.totqtyord          no-undo.
def            var v-oeid            as recid                     no-undo.
def            var g-lineno          like poei.lineno             no-undo.

def            var bc-orderno        as c format "x(12)"          no-undo.
def            var bc-orderno2       as c format "x(17)"          no-undo.
def            var bc-ordernox       as c format "x(17)"          no-undo.
def            var bc-barbeg         as c format "x(28)"          no-undo.
def            var bc-barend         as c format "x(28)"          no-undo.
def            var w-asterisk        as c format "x"     init "*" no-undo.
def            var tobar             as logical init no           no-undo.
def            var v-tovid           as logical init no           no-undo.


def buffer x-sasp for sasp.




