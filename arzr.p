 {p-rptbeg.i} 
def var totalar as decimal no-undo.
def var reporttype as character format "x" no-undo.
def var delimopt as character format "x(30)" no-undo.


def stream dailyx.
def stream monthlyx.

output stream dailyx to "/usr/tmp/ardailyx.ext".
output stream monthlyx to "/usr/tmp/armonthlyx.ext".



if sapb.optvalue[1] <> "" then
  reporttype = sapb.optvalue[1].
else
  reporttype = "d".


for each arsc where arsc.cono = 1 and
                    ((arsc.periodbal[1] +
                      arsc.periodbal[2] + 
                      arsc.periodbal[3] +
                      arsc.periodbal[4] + 
                      arsc.periodbal[5]) > 3000) no-lock:
                      
  if can-do ("b,d",reporttype) then
    do:
    export stream dailyx delimiter "|"
       arsc.custno
       arsc.name
       arsc.avgpaydays
       arsc.periodbal[1]
       arsc.periodbal[2]
       arsc.periodbal[3]
       arsc.periodbal[4]
       arsc.periodbal[5]
       arsc.creditmgr
       arsc.ordbal.
     end.
   
   if can-do ("b,m",reporttype) then
     do: 
     export stream monthlyx delimiter "|"
       arsc.custno
       arsc.termstype
       arsc.credlim
       arsc.dunsno
       arsc.highbal
       arsc.nopastdue
       arsc.noinv
       arsc.slsrepout
       arsc.slsrepin
       arsc.pricetype
       arsc.salesterr
       arsc.siccd[1]
       arsc.siccd[2]
       arsc.siccd[3]
       arsc.addr[1]
       arsc.city
       arsc.state
       arsc.zip
       arsc.phoneno
       arsc.faxphoneno
       arsc.salesytd.
     end.
       
 
 end. 
      
output stream dailyx close.
output stream monthlyx close.

if can-do ("b,d",reporttype) then
  do:
  os-command no-wait value("ftp -n < /home/tomh/dailyarftp").
  end.  

if can-do ("b,m",reporttype) then
  do:
  os-command no-wait value("ftp -n < /home/tomh/monthlyarftp").
  end.  
  
