{g-all.i}
def stream ordersurlog.

output stream ordersurlog to "/usr/tmp/surchgupdt0602.logz".
def var ztotsur         as decimal no-undo.
def var ztotsur2        as decimal no-undo.
def var zhtotsur        as decimal no-undo.
def var zhtotsur2       as decimal no-undo.

def var v-netordfl      as logical no-undo.
def var zlinesur        as decimal no-undo.
def var zinx            as integer   no-undo.
def var hdlfnd          as logical no-undo.
def var xinx            as integer   no-undo.
def var cntz            as integer   no-undo.
def buffer b-oeeh for oeeh.
def buffer b-oeel for oeel.

cntz = 0.
for each oeeh where oeeh.cono = 1 and oeeh.stagecd < 2 and
  oeeh.stagecd <> 0 and oeeh.transtype <> "rm" and
  oeeh.transtype <> "cr":
  assign hdlfnd = false
         xinx   = 0
         ztotsur = 0.

  for each oeel where oeel.cono = 1 and 
                      oeel.orderno =  oeeh.orderno and
                      oeel.ordersuf = oeeh.ordersuf and
                      oeel.specnstype <> "l" no-lock:    
      
    assign zlinesur = 0.
/*   Do all so everything is re-evaluated          
    if oeel.datccost = 0 then
      do:
*/
      if oeel.kitfl = true and  
         oeel.specnstype = "n" then
       do:                    
       run zsdioeelkdatc.p (input oeel.orderno,
                            input oeel.ordersuf,
                            input oeel.lineno,
                            input-output zlinesur).
       end.
     else
     if oeel.specnstype <> "n" then                   
       run zsdidatccalc.p (input oeel.whse,
                           input oeel.shipprod,
                           input oeel.custno,
                           input 
                           ((oeel.price * ((100 - oeel.discpct) / 100))), 
                           input-output zlinesur).

      if zlinesur <> 0 then
        do:
        find b-oeeh where recid(b-oeeh) = recid (oeeh) exclusive-lock. 
        find b-oeel where recid(b-oeel) = recid (oeel) exclusive-lock. 
         
        find icsw where icsw.cono = 1 and icsw.whse = oeel.whse and
                        icsw.prod = oeel.shipprod no-lock no-error.
        
        
        assign  v-netordfl = if (can-do("fo,qu,st,bl",oeeh.transtype)  or
                                can-do("s,t",oeeh.orderdisp)          or
                               (oeeh.stagecd    < 2                  and
                                oeeh.boexistsfl = no                 and
                                oeeh.totqtyshp  = 0                  and
                                oeeh.totqtyret  = 0                  and
                                oeel.qtyship    = 0)) then 
                               yes 
                            else
                                no.
         export stream ordersurlog
           delimiter "|" oeeh.orderno oeeh.ordersuf oeeh.stagecd
                         oeeh.transtype
                         oeel.lineno
                         oeel.shipprod
                         oeel.kitfl
                         (if avail icsw then 
                            icsw.arpvendno 
                          else
                            oeel.vendno)
                         oeel.qtyord
                         oeel.qtyship
                         oeel.datccost
                         zlinesur
                         oeeh.totdatccost
                         oeeh.totdatccost2
                         oeeh.totinvamt.
            
        assign ztotsur = zlinesur *
                         (if v-netordfl = no then
                             oeel.qtyship 
                          else
                             oeel.qtyord).
        if v-netordfl = no  then  
          assign ztotsur2 = zlinesur * oeel.qtyord.
        else
          assign ztotsur2 = zlinesur * oeel.qtyship.

        if v-netordfl = no then  
          assign zhtotsur2 = oeel.datccost * oeel.qtyord.
        else
          assign zhtotsur2 = oeel.datccost * oeel.qtyship.
          
        assign zhtotsur = oeel.datccost *
                         (if v-netordfl = no then
                             oeel.qtyship 
                          else
                             oeel.qtyord).


        if oeel.datccost <> 0 then do:
           assign b-oeeh.totdatccost = b-oeeh.totdatccost - zhtotsur
                  b-oeeh.totdatccost2 = b-oeeh.totdatccost2 - zhtotsur2
                  b-oeeh.totinvamt   = b-oeeh.totinvamt - zhtotsur.

        end.                     
                             
                             
        assign b-oeeh.totdatccost = b-oeeh.totdatccost + ztotsur
               b-oeeh.totdatccost2 = b-oeeh.totdatccost2 + ztotsur2
               b-oeel.datccost    = zlinesur
               b-oeeh.totinvamt   = b-oeeh.totinvamt + ztotsur.
        end.     
      
/*      end. Do all now ...end if oeel.datcost = 0 */
  end.
end.                    