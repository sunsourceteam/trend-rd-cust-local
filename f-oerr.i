/* SX 55 
   f-oerr.i 1.8 08/25/97 */
/*h*****************************************************************************
  INCLUDE      : f-oerr.i
  DESCRIPTION  : Frames for OERR
  USED ONCE?   : No (oerrcust.p, oerrinv.p, oerrl.p, oerrord.p, oerrp.p, 
                     oerrprod.p & oerruinv.p)
  AUTHOR       : mwb
  DATE WRITTEN : 09/12/90
  CHANGES MADE :
    01/06/92 pap; TB#  5225 JIT - add promised date to display and group items
        which are related together
    05/13/92 dea; TB#  6472 Added DO line indicator (s-dolinefl)
    11/14/92 dea; TB#  5972 Form to separate orders
    12/09/92 mwb; TB#  4535 Added the v-rebatety for "r" if rebates taken out
    01/25/93 mms; TB#  9644 Added rushfl display
    04/10/95 kjb; TB# 16383 Display the totalling information based on the lines
    10/16/95 mcd; TB# 18812-E7a 7.0 Rebates Enhancement (E7a) -
        Display figures in consideration of rebates on the Order Register
    03/21/96 kr;  TB# 12406 Variables defined in form, moved out of form
    02/21/97 jl;  TB# 7241 (4.1) Special Price Costing.  Replaced s-speccostty
        with v-speccostty and s-speccost with v-prccostper.
    08/25/97 kjb; TB# 22385 OERR totals do not include rebates
*******************************************************************************/

{&com}

/*tb 12406 03/21/96 kr; Variables defined in form */
def            var v-ordname    as c format "x(18)"                    no-undo.
def            var v-sales      as de format ">>>>>>>>>9.99-"          no-undo.
def            var v-cstgds     as de format  ">>>>>>>>9.99-"          no-undo.
def            var v-gross      as de format  ">>>>>>>>9.99-"          no-undo.
def            var v-marg       as de format     ">>>>>9.99-"          no-undo.
def            var v-charge     as de format   ">>>>>>>9.99-"          no-undo.
def            var v-down       as de format  ">>>>>>>>9.99-"          no-undo.
def            var v-totord     as de format ">>>>>>>>>9.99-"          no-undo.

/*d f-oerra - title line for order line */
form
    "Appr/"                                         at  33
    s-taxhdg1                                       at  79
    "Net Sale/"                                     at  91
    "TknBy/CSR"                                     at 103 
    
    skip

    "Order #"                                       at   3
    "Customer #"                                    at  13
    "Ty"                                            at  26
    "Stg"                                           at  29
    "Whse"                                          at  33
    "Entered"                                       at  39
    "Req Ship"                                      at  48
    "Promised"                                      at  57
    "Addon Amt"                                     at  66
    s-taxhdg2                                       at  79
    "Tot Ord Val"                                   at  90
    "SlsIn/Out"                                     at 103
    "Margin"                                        at 114
    "Margin %"                                      at 124 

    skip
with frame f-oerra no-box no-labels page-top width 132.

/* das - new cuatomer mod */
form
  "** NEW CUSTOMER **"                              at  17
with frame f-NewCust no-box no-labels down width 132.

/* das - new cuatomer mod */
form
  "** NEW CUSTOMER **"                              at  11
with frame f-NewCustV no-box no-labels down width 132.



/*d f-oerrb - line for order line  */
form
    oeeh.orderno                                    at   1
    "-"                                             at   9  /* SX 55 */
    oeeh.ordersuf                                   at  10  /* SX 55 */
    oeeh.notesfl                                    at  12  /* SX 55 */
    v-cust                                          at  13
    oeeh.transtype                                  at  26
    s-stagecd                                       at  29
    oeeh.approvty                                   at  33
    oeeh.enterdt                                    at  39
    oeeh.reqshipdt                                  at  48
    oeeh.promisedt                                  at  57
    s-addon                                         at  66
    s-tax                                           at  75
    s-netsale                                       at  89
    oeeh.takenby                                    at 103
    s-slstype                                       at 108
    s-margin                                        at 111  /* SX 55 was 110 */
    s-margpct                                       at 123 format ">>>>9.99-"
    v-rebatety                                      at 132
    s-lookupnm                                      at  13
    oeeh.whse                                       at  33
    v-auth                                          at  39
    oeeh.wtauth                                     at  54
    oeeh.psttaxamt                                  at  76 format "zzzzzzz9.99-"
    oeeh.totlineord                                 at  89
    oeeh.slsrepin                                   at 103
    oeeh.slsrepout                                  at 108
with frame f-oerrb no-box no-labels down width 132.

form
    oeeh.orderno                                    at   1
    "-"                                             at   8
    oeeh.ordersuf                                   at   9
    oeeh.notesfl                                    at  11
    v-cust                                          at  13 format "x(12)"       
    oeeh.transtype                                  at  25
    s-stagecd                                       at  28
    oeeh.approvty                                   at  32
    oeeh.enterdt                                    at  35
    oeeh.reqshipdt                                  at  44
    oeeh.promisedt                                  at  53
    s-addon                                         at  62 
    s-tax                                           at  72 format "zzzzzz9.99-"
    s-netsale                                       at  84
    oeeh.takenby                                    at  98
    s-slstype                                       at 103
    s-margin                                        at 106 format "zzzzz9.99-"
    s-margpct                                       at 117 format ">>>>9.99-"
    v-rebatety                                      at 126
    "**********"                                    at   1
    s-lookupnm                                      at  13
    oeeh.whse                                       at  31
    v-auth                                          at  38
    oeeh.wtauth                                     at  52
    oeeh.psttaxamt                                  at  70 format "zzzzzzz9.99-"
    oeeh.totlineord                                 at  84
    oeeh.slsrepin                                   at  98
    oeeh.slsrepout                                  at 103
with frame f-oerrbHLT no-box no-labels down width 132.


/*d f-oerrc - title line for line items when in order line order */
form
   "Product/"                                       at  11
   "Ordered/"                                       at  39
   "Unit/"                                          at  49
   "Price-Calc/"                                    at  57
   "Disc Level/"                                    at  77
   "Salesrep"                                       at 110
   "Ln#"                                            at   5
   "N"                                              at   9
   "Description"                                    at  11
   "Shipped"                                        at  39
   "BO?"                                            at  49
   "Cost"                                           at  60
   "WO/Spec Amt"                                    at  76
   "Req Ship"                                       at  90
   "Promised"                                       at 100
   "In   Out"                                       at 110
   "Margin %"                                       at 121 
   
   skip

   "------------------------------"                 at   5
   "------------------------------"                 at  35
   "------------------------------"                 at  65
   "--------------------------------------"         at  95
with frame f-oerrc no-box no-labels page-top width 132.

/*d f-oerrd - detail line for line items when in order line order */
form
    oeel.lineno                                     at   5
    s-dolinefl                                      at   8
    oeel.specnstype                                 at   9
    v-prod                                          at  11
    oeel.qtyord                                     at  37
    s-retqtyord                                     at  47
    oeel.unit                                       at  49
    oeel.price                                      at  55
    oeel.priceclty                                  at  68
    s-discamt                                       at  75
    s-disccd                                        at  88
    oeel.reqshipdt                                  at  90
    oeel.promisedt                                  at 100
    oeel.slsrepin                                   at 110
    oeel.slsrepout                                  at 115
    s-marginpct                                     at 121
    v-rebatety                                      at 130
    oeel.rushfl                                     at   5
    s-descrip                                       at  11
    oeel.qtyship                                    at  37
    s-retqtyship                                    at  47
    s-backorder                                     at  49
    oeel.prodcost                                   at  55
    v-speccostty                                    at  68
    v-prccostper                                    at  70
    v-mardiscoth                                    at  75
    v-coretype                                      at  89
    v-corecharge                                    at  99

    skip
with frame f-oerrd no-box no-labels down width 132.

/*d f-oerrf - title line for totals by order type */
/*tb 22385 08/25/97 kjb; Modified column label frame to allow for rebate
    indicator */
form
    skip(1)

    "Order"                                         at  22
    "# of"                                          at  30
    "Cost Of"                                       at  54 
    
    skip

    "Tot By Order Type:"                            at   1
    "Count"                                         at  22
    "Lines"                                         at  30
    "Net Sales"                                     at  40
    "Goods Sold"                                    at  53
    "Margin"                                        at  71
    "Margin %"                                      at  80
    "COD Charge"                                    at  92
    "Downpayments"                                  at 104
    "Tot Ord Val"                                   at 120 
    
    skip

    "----------------------------------------"      at  20
    "----------------------------------------"      at  60
    "---------------------------------"             at 100
with frame f-oerrf no-box no-labels width 132.

/*d f-oerrf1 - detail line for totals by order so,do,fo,st,cs,rm,cr,bl,br,qu */
/*tb 22385 08/25/97 kjb; Modified totals detail frame to allow for rebate
    indicator */
form
    v-ordname                                       at   1
    v-count                                         at  21
    v-lines                                         at  28
    v-sales                                         at  36
    v-cstgds                                        at  51
    v-gross                                         at  65
    v-marg                                          at  79
    v-rebatefl                                      at  89
    v-charge                                        at  90
    v-down                                          at 104
    v-totord                                        at 118
with frame f-oerrf1 no-box no-labels down width 132.

/*d f-oerrf2 - grand total line for all order types  */
/*tb 22385 08/25/97 kjb; Modified grand total line frame to allow for rebate
    indicator */
form
    "------- ------- --------------"                at  20
    "------------- ------------- ----------"        at  51
    "------------- ------------- ---------------"   at  90 
    
    skip

    t-tocount                                       at  20
    t-tolines                                       at  28
    t-tosales                                       at  36
    t-tocstgds                                      at  51
    t-togross                                       at  65
    t-tomarg                                        at  79
    t-torebatefl                                    at  89
    t-tocharge                                      at  90
    t-todown                                        at 104
    t-tototord                                      at 118 
    
    skip(1)

    "Lost Business Lines:"                          at   1
    t-lostcnt                                       at  21
    t-lostnet                                       at  27
    t-lostord                                       at 118
    
    skip(1)
    
    s-csrfield                                      at   1
    s-drilldown                                     at  19
    s-ddvalue                                       at  25
with frame f-oerrf2 no-box no-labels width 132.

/{&com}* */

/*d f-oerrg - title for line items if in product order */
form
    "Order #/"                                      at   3
    "Customer #/"                                   at  13
    "Ty/"                                           at  38
    "Ordered"                                       at  50
    "Unit/"                                         at  60
    "Price-Calc/"                                   at  67
    "Disc Level/"                                   at  86
    "SlsIn/"                                        at 117
    "Tkn By"                                        at   4
    "Name"                                          at  13
    "Stg"                                           at  38
    "Ln# N"                                         at  42
    "Shipped"                                       at  50
    "BO?"                                           at  60
    "Cost"                                          at  70
    "WO/Spec Amt"                                   at  85
    "Req Ship"                                      at  99
    "Promised"                                      at 108
    "SlsOut"                                        at 117
    "Margin %"                                      at 124
with frame f-oerrg no-box no-labels page-top width 132.  

/*d f-oerrh - detail line for line items if in product order  */
form
    oeel.orderno                                    at   1
    "-"                                             at   9 /* SX 55 */
    oeel.ordersuf                                   at  10 /* SX 55 */
    oeeh.notesfl                                    at  12 /* SX 55 */
    v-cust                                          at  13
    oeel.transtype                                  at  38
    oeel.lineno                                     at  42
    s-dolinefl                                      at  45
    oeel.specnstype                                 at  46
    oeel.qtyord                                     at  48
    s-retqtyord                                     at  58
    oeel.unit                                       at  60
    oeel.price                                      at  65
    oeel.priceclty                                  at  78
    s-discamt                                       at  84
    s-disccd                                        at  97
    oeel.reqshipdt                                  at  99
    oeel.promisedt                                  at 108
    oeel.slsrepin                                   at 118
    s-marginpct                                     at 124
    v-rebatety                                      at 132
    oeel.rushfl                                     at   1
    oeeh.takenby                                    at   6
    s-lookupnm                                      at  13
    s-stagecd                                       at  38
    s-jitflag                                       at  44
    oeel.qtyship                                    at  48
    s-retqtyship                                    at  58
    s-backorder                                     at  60
    oeel.prodcost                                   at  65
    v-speccostty                                    at  78
    v-prccostper                                    at  80
    v-mardiscoth                                    at  84
    v-coretype                                      at  98
    v-corecharge                                    at 108
    oeel.slsrepout                                  at 118
with frame f-oerrh no-box no-labels down width 132.

/*d f-oerri - title for line items if in slsrep order */
form
    "Order #/"                                      at   3
    "Customer #/"                                   at  13
    "Ty/"                                           at  29
    "Product/"                                      at  39
    "Ordered"                                       at  66
    "Unit/"                                         at  76
    "Price-Calc/"                                   at  83
    "Disc Level/"                                   at 103
    "SlsIn/"                                        at 116
    "Tkn By"                                        at   4
    "Name"                                          at  13
    "Stg"                                           at  29
    "Ln# N"                                         at  33
    "Description"                                   at  39
    "Shipped"                                       at  66
    "BO?"                                           at  76
    "Cost"                                          at  86
    "WO/Spec Amt"                                   at 102
    "SlsOut"                                        at 116
    "Margin %"                                      at 124
with frame f-oerri no-box no-labels page-top width 132.  

/*d f-oerrj - detail line for line items if in salesrep order  */
form
    oeel.orderno                                    at   1
    "-"                                             at   9 /* SX 55 */
    oeel.ordersuf                                   at  10 /* SX 55 */
    oeeh.notesfl                                    at  12 /* SX 55 */
    v-cust                                          at  13
    oeel.transtype                                  at  29
    oeel.lineno                                     at  33
    s-dolinefl                                      at  36
    oeel.specnstype                                 at  37
    v-prod                                          at  39
    oeel.qtyord                                     at  64
    s-retqtyord                                     at  74
    oeel.unit                                       at  76
    oeel.price                                      at  81
    oeel.priceclty                                  at  94
    s-discamt                                       at 101
    s-disccd                                        at 114
    oeel.slsrepin                                   at 117
    s-marginpct                                     at 124
    v-rebatety                                      at 132
    oeel.rushfl                                     at   1
    oeeh.takenby                                    at   6
    s-lookupnm                                      at  13
    s-stagecd                                       at  29
    s-jitflag                                       at  35
    s-descrip                                       at  39
    oeel.qtyship                                    at  64
    s-retqtyship                                    at  74
    s-backorder                                     at  76
    oeel.prodcost                                   at  81
    v-speccostty                                    at  94
    v-prccostper                                    at  96
    v-mardiscoth                                    at 101
    oeel.slsrepout                                  at 117
    v-coretype                                      at  92
    v-corecharge                                    at 103
with frame f-oerrj no-box no-labels down width 132.

{&com}

/*d f-oerrfp - additional line if Floor Plan order */
form
    v-fpcustno                           colon 22    label "Invoiced To Cust"
    s-fplookup                           at    38 no-label
with frame f-oerrfp no-box side-labels width 132.

/{&com}* */

/*d f-bar - form to separate orders */
form
    "=  =  =  =  =  =  =  =  =  =  "                at   1
    "=  =  =  =  =  =  =  =  =  =  "                at  31
    "=  =  =  =  =  =  =  =  =  =  "                at  61
    "=  =  =  =  =  =  =  =  =  =  "                at  91
    "=  =  =  =  "                                  at 121
with frame f-bar no-box width 132.

/*d f-legend - legend for rebates out of margin */
form
    skip(1)
    "Legend: 'r' - next to the Margin indicates vendor or customer "   at  1
    "rebates have been used to calculate Margin."                      at 63
with frame f-legend no-box no-labels down page-bottom width 132.

/*tb 16383 04/10/95 kjb; Added legend for totals since COD and downpayments
    are not shown if report is run for nonstocks, specials or drops. */
/*d f-totleg - legend for totals by order type */
form
    skip(1)
    "Legend: '-' indicates not applicable when report run for "   at  1
    "nonstocks, specials or drops."                               at 58
with frame f-totleg no-box no-labels down page-bottom width 132.
