/* vaetse.las 1.1 01/03/98 */
/* vaetse.las 1.1 08/08/96 */
/*h****************************************************************************
  INCLUDE      : vaetse.las
  DESCRIPTION  : Value Add Entry Transaction - Processing Sections Extended
                 Information Screen - EX/IT/IS - Assign Include
  USED ONCE?   : no
  AUTHOR       : mtt
  DATE WRITTEN : 08/08/96
  CHANGES MADE :
                 08/08/96 mtt; TB# 21254 (2A) Develop Value Add Module
******************************************************************************/

do for apsv, apss, icsd, sasta:
    assign
      s-sctndestnotesfl = ""
      s-sctnextrnotesfl = "".

    if s-sctntype = "ex" then do:
      if s-sctnextrvendno ne 0 then
        {w-apsv.i s-sctnextrvendno no-lock}
      if s-sctnextrshipfmno ne 0 then
        {w-apss.i s-sctnextrvendno s-sctnextrshipfmno no-lock}
      assign
        s-sctnextrnotesfl = if avail apsv then apsv.notesfl else ""
        s-sctnfromname    = if s-sctnextrshipfmno = 0 and avail apsv
                              then apsv.name
                            else
                            if s-sctnextrshipfmno ne 0 and avail apss
                              then apss.name
                            else ""
        s-sctnfromaddr[1] = if s-sctnextrshipfmno = 0 and avail apsv
                              then apsv.addr[1]
                            else
                            if s-sctnextrshipfmno ne 0 and avail apss
                              then apss.addr[1]
                            else ""
        s-sctnfromaddr[2] = if s-sctnextrshipfmno = 0 and avail apsv
                              then apsv.addr[2]
                            else
                            if s-sctnextrshipfmno ne 0 and avail apss
                              then apss.addr[2]
                            else ""
        s-sctnfromcity    = if s-sctnextrshipfmno = 0 and avail apsv
                              then apsv.city
                            else
                            if s-sctnextrshipfmno ne 0 and avail apss
                              then apss.city
                            else ""
        s-sctnfromstate   = if s-sctnextrshipfmno = 0 and avail apsv
                              then apsv.state
                            else
                            if s-sctnextrshipfmno ne 0 and avail apss
                              then apss.state
                            else ""
        s-sctnfromzipcd   = if s-sctnextrshipfmno = 0 and avail apsv
                              then apsv.zipcd
                            else
                            if s-sctnextrshipfmno ne 0 and avail apss
                              then apss.zipcd
                            else ""
        v-head1           = "External Vendor #:"
        v-head2           = "Ship From:".

    end.
  else do:
    if s-sctnintrwhse ne "" then
      {w-icsd.i s-sctnintrwhse no-lock}
    assign
      s-sctnfromname    = if avail icsd then icsd.name
                          else ""
      s-sctnfromaddr[1] = if avail icsd then icsd.addr[1]
                          else ""
      s-sctnfromaddr[2] = if avail icsd then icsd.addr[2]
                          else ""
      s-sctnfromcity    = if avail icsd then icsd.city
                          else ""
      s-sctnfromstate   = if avail icsd then icsd.state
                          else ""
      s-sctnfromzipcd   = if avail icsd then icsd.zipcd
                          else ""
      v-head3           = if s-sctntype = "it" then
                            "Internal Processing Whse:"
                          else
                            "         Inspection Whse:".
  end.

  {w-sasta.i "s" s-sctnshipviaty no-lock}
   s-sctnshipviadsc = if s-sctnshipviaty ne "" then
                        if avail sasta then sasta.descrip
                         else v-invalid
                      else "".

   if s-sctndesttype = "v" and s-sctndestvendno ne 0 then do:
     {w-apsv.i s-sctndestvendno no-lock}
     s-sctndestnotesfl = if avail apsv then apsv.notesfl
                         else s-sctndestnotesfl.
   end.

 end.

 assign
   v-headprod        = if s-sctndesttype = "f" then
                         "Final Product"
                       else
                         "Intermediate Product"
   v-title           = (if s-sctntype = "ex" then
                          "External"
                        else
                        if s-sctntype = "it" then
                          "Internal"
                        else
                          "Inspection")
                        + " Process Extended Information For Sq#: "
                        + string(s-seqno).
 /* tah default sdi */
   def buffer bx-icsd for icsd.
   def buffer bx-vaeh for vaeh.
   if s-sctnshipinstr = "" and program-name(1) = "vaetse.p" then do: 
       {vaeh.gfi &vano    = "g-vano"
                 &vasuf   = "g-vasuf"
                 &lock    = "no"
                 &buf     = "bx-"}
     {w-icsd.i bx-vaeh.whse no-lock "bx-"}
     if avail bx-icsd then do:
        assign s-sctnshipinstr = bx-icsd.shipinstr.
     end.  
   end.
/* tah default sdi */
  