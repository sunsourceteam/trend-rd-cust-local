if {k-func6.i} then
  do:
  assign g-modulenm = ""
         g-title = "Order Entry Inquiry - Orders".
  run oeio.p.
   put screen row 21 col 3   
    color message
  " F6-Order Detail   F7-P.O. Detail   F8-Delete Tie                        ".     put screen row 8 col 1  color message
  " P.O.      PO   Seq   Item                       Tied      Tied  Tied Kit".
   put screen row 9 col 1 color message
  " Number    Line       Number                     Order     Line     Seq  ".
  if {k-jump.i} or {k-select.i} or not
     (g-modulenm = "" or g-modulenm = "pa") then 
    leave main.
  hide message no-pause.
  next.
  end.
else
if {k-func7.i} then
  do:
  assign g-modulenm = ""
         g-title = "Purchase Entry Inquiry - Orders".
   run poio.p.
   put screen row 21 col 3   
     color message
  " F6-Order Detail   F7-P.O. Detail   F8-Delete Tie                        ".      put screen row 8 col 1  color message
  " P.O.      PO   Seq   Item                       Tied      Tied  Tied Kit".
    put screen row 9 col 1 color message
  " Number    Line       Number                     Order     Line     Seq  ".
    if {k-jump.i} or {k-select.i} or not
     (g-modulenm = "" or g-modulenm = "pa") then 
    leave main.
  hide message no-pause.
  next.

  end.
else
if {k-func8.i} then
  do:
  assign g-pono = dec(substring(o-frame-value,1,7))
         g-posuf = int(substring(o-frame-value,9,2))
         g-polineno = int(substring(o-frame-value,12,3))
         v-seqno = int(substring(o-frame-value,16,3)).

        
  find poelo where poelo.cono = g-cono and
                  poelo.pono = g-pono and
                  poelo.posuf = g-posuf and
                  poelo.lineno = g-polineno and
                  poelo.seqno = v-seqno no-lock no-error.


  
if avail poelo then
  do:
  find poel where poel.cono = g-cono and
                  poel.pono = g-pono and
                  poel.posuf = g-posuf  and 
                  poel.lineno = poelo.lineno no-lock no-error.
  if avail poel then 
     assign z-shipprod = poel.shipprod.
  else
     assign z-shipprod = "". 
   assign z-orderno =  poelo.orderaltno
          z-ordersuf = poelo.orderaltsuf
          z-lineno   = poelo.linealtno
          z-seqno    = poelo.seqaltno.
  end.       
else
  do:
  assign z-orderno =  0
         z-ordersuf = 0
         z-lineno   = 0
         z-shipprod = ""
         z-seqno    = 0.
  end.       

  z-answer = no.
  display 
    g-pono
    g-posuf
    g-polineno
    v-seqno
    z-shipprod
    z-orderno
    z-ordersuf
    z-lineno
    z-seqno
    with frame f-potied.


  do on endkey undo, leave:
    update z-answer with frame f-potied.
  end.
  hide frame f-potied no-pause.
  
  if z-answer = yes then
    do:
            
    find poelo where poelo.cono = g-cono and
                  poelo.pono = g-pono and
                  poelo.posuf = g-posuf and
                  poelo.lineno = g-polineno and
                  poelo.seqno = v-seqno no-error.

    find poel  where poel.cono = g-cono and
                  poel.pono = g-pono and
                  poel.posuf = g-posuf and
                  poel.lineno = g-polineno 
                   no-error.


   
    if avail poelo and avail poel then
      do:
      find oeeh where oeeh.cono = g-cono  and
                      oeeh.orderno = poelo.orderaltno and
                      oeeh.ordersuf = poelo.orderaltsuf no-error.
      if avail oeeh then
        do:              
        if oeeh.stagecd = 9 or 
           oeeh.stagecd ge 5 then
          do:
          run err.p (5687).
          next.
          end. 
          
        
        
        
        if poelo.seqaltno = 0 then
          do:
          find oeel where oeel.cono = g-cono  and
                        oeel.orderno = poelo.orderaltno and
                        oeel.ordersuf = poelo.orderaltsuf and
                        oeel.lineno = poelo.linealtno  no-error.
          if avail oeel then
            do:              
            assign oeel.ordertype = " "
                   oeel.orderaltno = 0
                   oeel.linealtno = 0.
            delete poelo.
            end.       

        else if poelo.seqaltno ne 0 then
          do:
          find oeelk where oeelk.cono = g-cono  and
                        oeelk.orderno = poelo.orderaltno and
                        oeelk.ordersuf = poelo.orderaltsuf and
                        oeelk.lineno = poelo.linealtno and
                        oeelk.seqno  = poelo.seqaltno  no-error.
          if avail oeel then
            do:              
            assign oeelk.ordertype = " "
                   oeelk.orderaltno = 0
                   oeelk.linealtno = 0.
            delete poelo.
            end.       
          end.
        end.
      end.
    end.
  end.
end.


