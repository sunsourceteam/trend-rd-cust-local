/****************************************************************************
*                                                                           *
* f-pdxmcdtl.i - Sales Matrix Margin Detail Forms                           *
*                                                                           *
****************************************************************************/

form
"Cust"                  at 1
"Prod"                  at 7
"Contract"              at 136
"Price"                 at 1
"Price"                 at 7
"Reg"                   at 14
"Customer"              at 20
"Ship"                  at 33
"Customer"              at 38
"Order"                 at 47
"Order"                 at 57
"Invoice"               at 64
"Sales"                 at 77
"Matrix"                at 90
"Invoiced"              at 101
"Discount/"             at 136
"PD"                    at 153
"PD"                    at 161


"Type"                  at 1
"Type"                  at 7
"Dist"                  at 14
"Number"                at 20
"To"                    at 33
"Name"                  at 38
"Number"                at 47
"Line"                  at 57
"Date"                  at 64
"Amount"                at 77
"Margin%"               at 90
"Margin%"               at 101
"Product"               at 111
"Net Price"             at 136
"Record#"               at 153
"Level"                 at 161
"Exp Date"              at 167
"-----"                 at 1
"-----"                 at 7
"----"                  at 14
"------------"          at 20
"----"                  at 33
"----------"            at 38
"----------"            at 45
"-----"                 at 57
"--------"              at 64
"------------"          at 74
"---------"             at 88
"---------"             at 100
"---------------------" at 111
"---------"             at 136
"-------"               at 153
"-----"                 at 161
"--------"              at 167
with frame f-dd-header no-underline no-box no-labels page-top down width 178.


form
s-cprice                at 1  
s-pprice                at 7  
s-rgn                   at 14 
s-custno                at 20
s-shipto                at 33
s-name                  at 38
s-order                 at 45
s-line                  at 58
s-date                  at 64
s-isales                at 76 
s-mmarg                 at 89 
s-imarg                 at 101
oeel.shipprod           at 111
matrix-price            at 136
s-type                  at 145
s-disctp                at 147
s-recno                 at 153
s-level                 at 163
s-enddt                 at 167
with frame f-dd-detail no-underline no-box no-labels down width 178.

form
skip(1)
"Totals for"            at 33
s-ytd-isales            at 74
s-ytd-mmarg             at 88
s-ytd-imarg             at 100
skip(1)
with frame f-dd-total no-underline no-box no-labels down width 178.



form
skip(1)
"Legend:"                                                at 5
"Price - Value in preceding field is customer price"     at 15
"D%L - Value in preceding field is discount off of list" at 70
"P%L - Price = Value in preceding field * List"          at 130
with frame f-dd-trailer no-box no-labels page-bottom 
     down width 178.

