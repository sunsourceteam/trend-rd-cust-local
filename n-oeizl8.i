/* n-oeizl8.i 1.1 01/03/98 */
/* n-oeizl8.i 1.5 01/29/94 */
/*h*****************************************************************************
  SX 55
  INCLUDE      : n-oeizl8.i
  DESCRIPTION  : Find next/prev for OEIZL where v-mode = 8
  USED ONCE?   : yes
  AUTHOR       : rhl
  DATE WRITTEN :
  CHANGES MADE :
    12/23/91 pap; TB#  5266 JIT/Line Due - reqship to promise dt
    01/29/94 kmw; TB# 11747 Make stage a range
    05/09/95 mtt; TB#  5180 When looking for a product-need kit ln
    03/14/96 tdd; TB# 20713 Inquiry not working properly
    08/08/02 lcb; TB# e14281 User Hooks
    08/09/02 emc; TB# e6846 Slow with whse and stagecd range
*******************************************************************************/

/*e*****************************************************************************
     Selection Criteria for this procedure:
       v-mode ==> 8:
               Product: <blank>
             Customer#: <blank> (0)
               CustPO#: <blank>
      Stage Code Range: <blank> (wide open - 0 to 9)
*******************************************************************************/
do:

confirm = yes.


if ("{1}" = "FIRST" and s-directionfl )  then
  assign v-xmode = "C".
else
if ("{1}" = "LAST" and not s-directionfl )  then
  assign v-xmode = "O".



if v-xmode = "C" then
  do:

if s-whse = "" then do:
    find {1} oeehb use-index k-oeehb where
             oeehb.cono = g-cono and
             (s-shipto     = ""   or oeehb.shipto    = s-shipto)  and
             (s-takenby    = ""   or oeehb.takenby   = s-takenby) and
             (s-transtype  = ""   or s-transtype    = "QU" 
             /* oeehb.transtype */) and
             (s-transtype  <> "bo")  and

          
            ((oeehb.stagecd <> 1 and oeehb.stagecd >= integer(s-stagecdl)) or
             (oeehb.stagecd = 1 and integer(s-stagecdl) = 0)) and
             (oeehb.stagecd <= integer(s-stagecdh)) and
             oeehb.sourcepros = "ComQu" and
             oeehb.seqno = 2 and

             (s-promisedt  = ?    or oeehb.promisedt >= s-promisedt)

             {oeizl.z99 &user_find_oeizl = "*"
                        &file            = "oeehb"}
    no-lock no-error.
end.    /* No whse specified */
else do:
    /* With no index specified progress will pick k-whsestagecd.  This will
       improve performance whenever whse is entered with no customer, product
       or customer PO */
    find {1} oeehb   where
             oeehb.cono = g-cono and
             oeehb.whse = s-whse and
             (s-shipto     = ""   or oeehb.shipto    = s-shipto)  and
             (s-takenby    = ""   or oeehb.takenby   = s-takenby) and
             (s-transtype  = ""   or s-transtype    = "QU" 
             /* oeehb.transtype */) and
             (s-transtype  <> "bo" ) and

          
             ((oeehb.stagecd <> 1 and oeehb.stagecd >= integer(s-stagecdl)) or
              (oeehb.stagecd = 1 and integer(s-stagecdl) = 0)) and
              (oeehb.stagecd <= integer(s-stagecdh)) and

             (s-promisedt  = ?    or oeehb.promisedt >= s-promisedt) and
             oeehb.sourcepros = "ComQu" and
             oeehb.seqno = 2
             {oeizl.z99 &user_find_oeizl = "*"
                        &file            = "oeehb"}
    no-lock no-error.
end.    /* Whse Specified */

/*tb 6507 05/07/92 dea; added check for line DO orders */
if avail oeehb and s-doonlyfl = yes then do:
    confirm = no.
    find first oeelb  where
               oeelb.cono     = g-cono and
               oeelb.batchnm  = oeehb.batchnm and
               oeelb.seqno    = oeehb.seqno   and
               oeelb.botype   = "d"
    no-lock no-error.
    if avail oeelb then confirm = yes.
end.
 
if avail oeehb then
  find zxinq where zxinq.present = true  no-lock no-error.
else
  find zxinq where zxinq.present = false no-lock no-error.

end.   


if v-xmode = "C" and (not avail oeehb) and
   ("{1}" = "first" or "{1}" = "next" ) then
  do:
  assign v-xmode = "O"
         confirm = yes.
  if s-whse = "" then 
    find first oeeh use-index k-oeeh where
               oeeh.cono = g-cono no-lock no-error.
  else          
    find first oeeh where
             oeeh.cono = g-cono and
             oeeh.whse = " "    and
             oeeh.shipto  = " "  and
             oeeh.takenby   = "" and
             oeeh.transtype = "" and
             oeeh.borelfl = no and
             oeeh.stagecd  = 0   and
             oeeh.promisedt = 01/01/1970 no-lock no-error.
   end.


if v-xmode = "O" then
do:

if s-whse = "" then do:
    find {1} oeeh use-index k-oeeh where
             oeeh.cono = g-cono and
             (s-shipto     = ""   or oeeh.shipto    = s-shipto)  and
             (s-takenby    = ""   or oeeh.takenby   = s-takenby) and
             (s-transtype  = ""   or s-transtype    = oeeh.transtype or
             (s-transtype  = "bo" and oeeh.borelfl)) and

             (oeeh.stagecd >= integer(s-stagecdl)) and
             (oeeh.stagecd <= integer(s-stagecdh)) and

             (s-promisedt  = ?    or oeeh.promisedt >= s-promisedt)

             {oeizl.z99 &user_find_oeizl = "*"
                        &file            = "oeeh"}
    no-lock no-error.
end.    /* No whse specified */
else do:
    /* With no index specified progress will pick k-whsestagecd.  This will
       improve performance whenever whse is entered with no customer, product
       or customer PO */
    find {1} oeeh where
             oeeh.cono = g-cono and
             oeeh.whse = s-whse and
             (s-shipto     = ""   or oeeh.shipto    = s-shipto)  and
             (s-takenby    = ""   or oeeh.takenby   = s-takenby) and
             (s-transtype  = ""   or s-transtype    = oeeh.transtype or
             (s-transtype  = "bo" and oeeh.borelfl)) and

             (oeeh.stagecd >= integer(s-stagecdl)) and
             (oeeh.stagecd <= integer(s-stagecdh)) and

             (s-promisedt  = ?    or oeeh.promisedt >= s-promisedt)

             {oeizl.z99 &user_find_oeizl = "*"
                        &file            = "oeeh"}
    no-lock no-error.
end.    /* Whse Specified */

/*tb 6507 05/07/92 dea; added check for line DO orders */
if avail oeeh and s-doonlyfl = yes then do:
    confirm = no.
    find first oeel use-index k-oeel where
               oeel.cono     = g-cono and
               oeel.orderno  = oeeh.orderno and
               oeel.ordersuf = oeeh.ordersuf and
               oeel.botype   = "d"
    no-lock no-error.
    if avail oeel then confirm = yes.
end.



if avail oeeh then
  find zxinq where zxinq.present = true  no-lock no-error.
else
  find zxinq where zxinq.present = false no-lock no-error.
end.

if v-xmode = "o" and (not avail oeeh) and
   ("{1}" = "Prev" or "{1}" = "Last" ) then
  do:
  assign v-xmode = "C"
         confirm = yes.

if s-whse = "" then do:
    find last oeehb use-index k-oeehb where
             oeehb.cono = g-cono and
             (s-shipto     = ""   or oeehb.shipto    = s-shipto)  and
             (s-takenby    = ""   or oeehb.takenby   = s-takenby) and
             (s-transtype  = ""   or s-transtype    = "QU"
               /* oeehb.transtype */) and
             (s-transtype  <> "bo")  and
          
             ((oeehb.stagecd <> 1 and oeehb.stagecd >= integer(s-stagecdl)) or
              (oeehb.stagecd = 1 and integer(s-stagecdl) = 0)) and
             (oeehb.stagecd <= integer(s-stagecdh)) and


             (0 >= integer(s-stagecdl)) and
             (oeehb.stagecd <= integer(s-stagecdh)) and

             (s-promisedt  = ?    or oeehb.promisedt >= s-promisedt) and
             oeehb.sourcepros = "ComQu" and
             oeehb.seqno = 2

             {oeizl.z99 &user_find_oeizl = "*"
                        &file            = "oeehb"}
    no-lock no-error.
end.    /* No whse specified */
else do:
    /* With no index specified progress will pick k-whsestagecd.  This will
       improve performance whenever whse is entered with no customer, product
       or customer PO */
    find last oeehb where
             oeehb.cono = g-cono and
             oeehb.whse = s-whse and
             (s-shipto     = ""   or oeehb.shipto    = s-shipto)  and
             (s-takenby    = ""   or oeehb.takenby   = s-takenby) and
             (s-transtype  = ""   or s-transtype    = "QU" 
             /* oeehb.transtype */) and
             (s-transtype  <> "bo" ) and
          
             ((oeehb.stagecd <> 1 and oeehb.stagecd >= integer(s-stagecdl)) or
              (oeehb.stagecd = 1 and integer(s-stagecdl) = 0)) and
             (oeehb.stagecd <= integer(s-stagecdh)) and

             (s-promisedt  = ?    or oeehb.promisedt >= s-promisedt) and
             oeehb.sourcepros = "ComQu" and
             oeehb.seqno = 2
             {oeizl.z99 &user_find_oeizl = "*"
                        &file            = "oeehb"}
    no-lock no-error.
end.    /* Whse Specified */

/*tb 6507 05/07/92 dea; added check for line DO orders */
if avail oeehb and s-doonlyfl = yes then do:
    confirm = no.
    find first oeelb  where
               oeelb.cono     = g-cono and
               oeelb.batchnm  = oeehb.batchnm and
               oeelb.seqno    = oeehb.seqno   and
               oeelb.botype   = "d"
    no-lock no-error.
    if avail oeelb then confirm = yes.
end.
 
if avail oeehb then
  find zxinq where zxinq.present = true  no-lock no-error.
else
  find zxinq where zxinq.present = false no-lock no-error.

end.   



end.                  
