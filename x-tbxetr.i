/{&user_temptable}*/
def  var i-turnmo as de format ">9" no-undo.
def var h-buyer    like icsl.buyer no-undo.
def var h-prodline like icsl.prodline no-undo.
def var h-vendor   like icsl.vendno no-undo.
def var h-person   as char no-undo.
def var h-searchlevel as i no-undo.
def var d-name as char no-undo.
  
  
def   temp-table obsolete no-undo 
  field o-prod     like icsw.prod
  field o-proddesc as c format "x(30)"
  field o-vendor   like apsv.vendno
  field o-vname    as c format "x(30)"
  field o-ls12month  as integer 
  field o-6month   as integer
  field o-12month  as integer 
  field o-24month  as integer
  field o-48month  as integer
  field o-60month  as integer
  field o-g60month  as integer
  field o-ns7      as integer
  field o-ns12     as integer
  field o-ns24     as integer
  field o-total    as integer
  field o-asofdate as date
 
 
  field o-enterdt  as date
  field o-rcptdt   as date
  field o-invdt    as date
  field o-firstrcpt as date
  field o-rcptohdt as date
  field o-12b4rcpt as integer   
  field o-rcptoh   as integer    
    
    
  index o-inx
        o-prod.
def temp-table wuyers  no-undo
  Field wregion as char format "x(1)"
  Field wdistrict as char format "x(3)"
 /*
    Field wxbreakreg as char
    Field wxbreakpct as char
    Field wxbreaklst as char
    Field wxcust as char
    Field wxshipto as char
    Field wxrange as integer
  */
  Field woh like icsw.qtyonhand
  Field wavgcost like icsw.avgcost  
  Field wcustno  like arsc.custno
  Field wshipto  like arss.shipto    
  Field wlstsale  as  integer  
  Field wlsttrans  as  character 
  Field wmaster as logical
  Field wwhse like icsw.whse
  Field wicsdregion like icsd.region
  Field wprod as char format "x(24)"
  Field wprodcat as char format "x(4)"
  Field wgroup as char format "x(4)"
  Field wbuyer as char format "x(4)"
  Field wdept as char format "x(15)"
  Field wname  as char format "x(15)"
  Field wvendor like icsw.arpvendno
  Field wpline like icsl.prodline
  Field wcur as de format ">>>>>>>9.99-"
  Field wncur as de format ">>>>>>>9.99-"
  Field wavg as de format ">>>>>>>9.99-"
  Field wcgs as de format ">>>>>>>9.99-"
  Field wncgs as de format ">>>>>>>9.99-"
  Field wls12 as de format ">>>>>>>9.99-" 
  Field w6 as de format ">>>>>>>9.99-" 
  Field w12 as de format ">>>>>>>9-"    
  Field w24 as de format ">>>>>>>9.99-" 
  Field w48 as de format ">>>>>>>9.99-" 
  Field w60  as de format ">>>>>>>9.99-"    
  Field wg60  as de format ">>>>>>>9.99-"    
  Field wns7 as de format ">>>>>>>9.99-" 
  Field wns12 as de format ">>>>>>>9.99-" 
  Field wns24 as de format ">>>>>>>9.99-"
  Field wqls12 as de format ">>>>>>>9.99-" 
  Field wq6 as de format ">>>>>>>9.99-" 
  Field wq12 as de format ">>>>>>>9.99-" 
  Field wq24 as de format ">>>>>>>9.99-" 
  Field wq48 as de format ">>>>>>>9.99-" 
  Field wq60  as de format ">>>>>>>9.99-" 
  Field wqg60  as de format ">>>>>>>9.99-" 
  Field wqns7  as de format ">>>>>>>9.99-" 
  Field wqns12 as de format ">>>>>>>9.99-" 
  Field wqns24 as de format ">>>>>>>9.99-" 

  Field wsales  as de format ">>>>>>>9.99-" 
  Field wcost   as de format ">>>>>>>9.99-" 
  Field wmargin as de format ">>>>>>>9.99-" 
  Field wasofdt     as date 
  Field wannualCgs as de format ">>>>>>>9.99-" 
  Field wtotinv     as de format ">>>>>>>9.99-" 
  Field wobso      as de  format ">>>>>>>9.99-" 
  Field wqobso      as de  format ">>>>>>>9.99-" 
   index wkey
     wmaster
     wwhse
     wprod
   index wkey2
     wmaster
     wprod
     wwhse
index wkey3  
      wmaster
      wregion
      wdistrict
      wicsdregion
      wwhse
      wgroup
      wbuyer
      wdept
      wpline
      wprodcat
      wvendor
      wcustno
      wprod.
      
      
      
def temp-table r1range  no-undo
Field r1region   as char format "x"
Field r1district as char format "x(3)"
Field r1icsdregion like icsd.region
Field r1whse like icsw.whse
Field r2group as char format "x(4)"
field r2dept as char format "x(15)"
Field r2buyer as char format "x(4)"
Field r2vendor like apsv.vendno
Field r2pline like icsl.prodline
Field r3prodcat as character format "x(4)"
 
index r1key
      r1region
      r1district
      r1icsdregion
      r1whse
      r2group
      r2dept
      r2buyer
      r2vendor
      r2pline
      r3prodcat.
Define Var v-annual-sinv  as dec                 no-undo.
Define Var v-annual-nsinv as dec                 no-undo.
Define Var v-annualCogs as dec                   no-undo.
Define Var v-totalinv as dec                     no-undo.                  
Define Var v-lasttrans as character format "x(15)"      no-undo.
Define Var v-holdlasttrans as character format "x(15)"      no-undo.
Define Var p-detailprt as logical    no-undo.
Define Var p-list      as logical    no-undo.
Define Var p-obsolete  as logical    no-undo.
Define Var p-recvmonths   as integer no-undo.
Define Var p-ohonly    as logical    no-undo.
Define Var p-pastsuff  as char       no-undo.
Define Var p-format    as int        no-undo.
Define Var p-regval    as char       no-undo.
Define Var p-bgrpbeg  like icsd.buygroup  no-undo.
Define Var p-bgrpend  like icsd.buygroup  no-undo.
Define Var p-deptbeg  as c  format "x(4)" no-undo.
Define Var p-deptend  as c  format "x(4)" no-undo.
Define Var p-buyrbeg  as c  format "x(4)" no-undo.
Define Var p-buyrend  as c  format "x(4)" no-undo.
Define Var p-vendbeg  as de format ">>>>>>>>>>>9" no-undo.
Define Var p-vendend  as de format ">>>>>>>>>>>9" no-undo.
Define Var p-linebeg  as c  format "x(6)" no-undo.
Define Var p-lineend  as c  format "x(6)" no-undo.
Define Var b-postdt as date no-undo.
Define Var e-postdt as date no-undo.
define var p-currentinfo    as logical no-undo.
def Var p-master as character format "x" no-undo.
def Var p-stored as character format "x(24)" no-undo.
def var p-asofdate as date no-undo.
Define Var p-distbeg  as character format "x(3)" no-undo.
Define Var p-distend  as character format "x(3)" no-undo.
Define Var p-region   as character format "x(1)" no-undo.
Define Var p-regbeg   as character format "x(1)" no-undo.
Define Var p-regend   as character format "x(1)" no-undo.
Define Var p-iregbeg   as character format "x(1)" no-undo.
Define Var p-iregend   as character format "x(1)" no-undo.
Define Var p-slsrep   as character format "x(4)" no-undo.
Define Var p-repbeg   as character format "x(4)" no-undo.
Define Var p-repend   as character format "x(4)" no-undo.
Define Var p-prodbeg   as character format "x(24)" no-undo.
Define Var p-prodend   as character format "x(24)" no-undo.
Define Var p-whsebeg  as character format "x(4)" no-undo.
/* Abbrv tech names
   PNEU
   HYDR
   MOBL
   FILT
   AUTO
   SERV
   NONE
   CONN
   FABR
*/ 
Define Var p-whseend  as character format "x(4)" no-undo.
Define Var p-techbeg  as character format "x(4)" no-undo.
Define Var p-techend  as character format "x(4)" no-undo.
Define Var p-divbeg   as int no-undo.
Define Var p-divend   as int no-undo.
Define Var p-vpbeg   as character format "x(24)" no-undo.
Define Var p-vpend   as character format "x(24)" no-undo.
Define Var p-vidbeg   as character format "x(24)" no-undo.
Define Var p-vidend   as character format "x(24)" no-undo.
Define Var p-custbeg  as decimal                 no-undo.
Define Var p-custend  as decimal                 no-undo.
Define Var p-pcatbeg  as character format "x(4)" no-undo.
Define Var p-pcatend  as character format "x(4)" no-undo.
Define Var PK_Good    as logical                   no-undo.
Define Var vl-slsrep   like smsn.slsrep            no-undo.
Define Var vl-csrrep   like smsn.slsrep            no-undo.
Define Var v-region   as character format "x(1)"  no-undo.
Define Var v-district as character format "x(3)"  no-undo.
Define Var v-technology  as character format "x(30)"  no-undo.
Define Var v-parentcode as char format "x(24)"  no-undo.
Define Var v-vendorid as char format "x(24)"  no-undo.
define buffer catmaster for notes.
{zsapboydef.i}
{g-sdietr.i}
 
 
 
 
 
 
/{&user_temptable}*/
 
/{&user_drpttable}*/
/* Use to add fields to the report table */
    FIELD region    AS CHARACTER FORMAT "X(4)"
    FIELD district  AS CHARACTER FORMAT "x(2)"
    FIELD icsdregoin like icsd.region
    FIELD buyer     LIKE poeh.buyer
    FIELD buyernm   as character format "x(15)"
    FIELD name      as character format "x(15)"
    FIELD vendno    LIKE poeh.vendno
    FIELD shipto    like arss.shipto
    FIELD custname  LIKE arsc.name
    FIELD pcat      as c 
    FIELD lstsale   as integer
    FIELD lsttrans  as char
 
 
 
 
 
 
 
 
/{&user_drpttable}*/
 
/{&user_forms}*/
form header
  " " at 1
  with frame fx-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: ICXM"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 120
  page-number(xpcd)    at 125 format ">>>9"
   "Inventory Measurables Report" at 48
   with frame fx-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 132 page-top.
form header
  " " at 1
  with frame f-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: ICXE"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 120
   page-number        at 125 format ">>>9"
   "Inventory Measurables Report" at 48
   with frame f-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 132 page-top.
form header 
   " "                    at 1
  with frame f-trn3 down NO-BOX NO-LABELS no-underline
            WIDTH 132 page-top.
form header 
    "YTD"       at 62
    "Current"   at 72
    "YTD Turn"  at 82
    "YTD Gross" at 93
    "YTD"       at 107
    "Policy 21" at 114
    "Factor"    at 125
    "Sales"     at 60
    "Inventory" at 71
    "Rate"      at 84
    "Margin"    at 94
    "ETR"       at 107
with frame f-trn4 
  down NO-BOX NO-LABELS no-underline
            WIDTH 132 page-top.
form header 
   h-underline               at 1  format "x(130)"
  with frame f-trn5 down NO-BOX NO-LABELS no-underline
            WIDTH 132 page-top.
  
form
  v-final          at 1
  v-astrik         at 2   format "x(5)"
  v-summary-lit2   at 7   format "x(47)"
  v-amt1           at 55  format ">>>,>>>,>>9-"
  v-amt2           at 69  format ">>>,>>>,>>9-"
  v-amt3           at 84  format ">>9.99-"
  v-amt4           at 94  format ">>9.99-" 
  v-amt5           at 104 format ">>>.99-"
  v-amt6           at 112 format ">>,>>>,>>9-"
  v-amt7           at 125 format ">>9.99-"
with frame f-tot width 132 
          no-box no-labels.  
form 
  " "           at 1
with frame f-skip width 132 no-box no-labels.  
 
define var n-whse like icsw.whse no-undo.
defin var n-prod like icsw.prod no-undo.
{p-zfindbuygne.i
    n-whse
    n-prod
    h-buyer
    h-vendor
    h-prodline
    h-searchlevel
    }
 
 
 
 
 
 
/{&user_forms}*/
 
/{&user_extractrun}*/
 run sapb_vars.
 run extract_data.   
 
 
 
/{&user_extractrun}*/
 
/{&user_exportstatDWheaders}*/
  assign export_rec = export_rec + v-del +
                      "YtdSales" + v-del +
                      "CurInvQty" + v-del   +
                      "CurInvVal" + v-del   +
                      "YtdTurnRate" + v-del   +
                      "YtdGM" + v-del   +
                      "YTDETR" + v-del   +
                      "Pol21Qty" + v-del   +
                      "Pol21Val" + v-del   +
                      "Factor" + v-del +
                      "AsofDate" + v-del +
                      "CostOfGoods" + v-del +
                      "AnnualCOGs".
 
 
 
/{&user_exportstatDWheaders}*/
 
/{&user_exportstatheaders}*/
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  
  assign export_rec = export_rec + v-del +
                      "Ytd Sales" + v-del +
                      "Current Inv Qty" + v-del   +
                      "Current Inv Val" + v-del   +
                      "Ytd Turn Rate" + v-del   +
                      "Ytd Gross Margin" + v-del   +
                      "YTD ETR" + v-del   +
                      "Policy 21 Qty" + v-del   +
                      "Policy 21 Val" + v-del   +
                      "Factor" + v-del +
                      "AsOfDate" + v-del +
                      " ".
 
 
 
 
 
 
 
/{&user_exportstatheaders}*/
 
/{&user_foreach}*/

for each r1range  no-lock:
  if  r1range.r1region     < p-regbeg   or
      r1range.r1region     > p-regend   or
      r1range.r1district   < p-distbeg  or
      r1range.r1district   > p-distend  or
      r1range.r1icsdregion < p-iregbeg  or
      r1range.r1icsdregion > p-iregend  or
      r1range.r1whse       < zel_begwhse or
      r1range.r1whse       > zel_endwhse or
      r1range.r2group      < p-bgrpbeg  or
      r1range.r2group      > p-bgrpend  or
      r1range.r2buyer      < zel_begbuy or 
      r1range.r2buyer      > zel_endbuy or
      r1range.r2dept       < p-deptbeg  or
      r1range.r2dept       > p-deptend  or
      r1range.r2pline      < p-linebeg   or
      r1range.r2pline      > p-lineend  or
      r1range.r3prodcat    < zel_begcat  or
      r1range.r3prodcat    > zel_endcat or
      r1range.r2vendor     < zel_begvend  or
      r1range.r2vendor     > zel_endvend   then
    next buildrec.
  pk_good = true.
  zelection_type = "w".
  zelection_char4 = r1range.r1whse.
  zelection_cust = 0.
  run zelectioncheck.
  if zelection_good = false then
    do:
    pk_good = false.
    next buildrec.
    end.
       
  pk_good = true.
  zelection_type = "b".
  zelection_char4 = r1range.r2buyer.
  zelection_cust = 0.
  run zelectioncheck.
  if zelection_good = false then
    do:
    pk_good = false.
    next buildrec.
    end.
 
  pk_good = true.
  zelection_type = "v".
  zelection_vend = r1range.r2vendor.
  zelection_char4 = " ".
  run zelectioncheck.
  if zelection_good = false then
    do:
    pk_good = false.
    next buildrec.
    end.
 
 pk_good = true.
 zelection_type = "p".
 zelection_char4 = r1range.r3prodcat.
 zelection_cust = 0.
 run zelectioncheck.
 if zelection_good = false then
   do:
   pk_good = false.
   next buildrec.
   end.
/* rep, tech, vendor id, vendor parent */ 
 find catmaster where catmaster.cono = 1 and 
                      catmaster.notestype = "zz" and
                      catmaster.primarykey  = "apsva" and
                      catmaster.secondarykey = 
                      r1range.r3prodcat no-lock
                       no-error. 
 if not avail catmaster then
   do:
   v-parentcode = "".
   v-vendorid = r1range.r3prodcat.
   if r1range.r3prodcat  = "a" then 
     v-technology = "Automation".
   else if r1range.r3prodcat  = "f" then
     v-technology = "Fabrication". 
   else if r1range.r3prodcat = "h" then 
     v-technology = "Hydraulic".   
   else if r1range.r3prodcat  = "p" then 
     v-technology =  "Pneumatic".   
   else if r1range.r3prodcat = "s" then 
     v-technology =  "Service".     
   else if r1range.r3prodcat = "f" then 
     v-technology =  "Filtration".  
   else if r1range.r3prodcat  = "c" then 
     v-technology =  "Connectors".  
   else if r1range.r3prodcat  = "m" then 
     v-technology =  "Mobile".      
   else if r1range.r3prodcat = "n" then 
     v-technology =  "None".        
   end.
  
else
  assign
     v-vendorid   = catmaster.noteln[1]
     v-technology = catmaster.noteln[2]
     v-parentcode = catmaster.noteln[3].
    
if (substring(v-technology,1,4) < p-techbeg or
    substring(v-technology,1,4) > p-techend) or
    (v-parentcode < p-vpbeg or
     v-parentcode > p-vpend) 
                                then
  do:
  pk_good = false.
  next buildrec.
  end.
pk_good = true.
zelection_type = "n".
zelection_char4 = v-vendorid.
zelection_cust = 0.
run zelectioncheck.
if zelection_good = false then
  do:
  pk_good = false.
  next buildrec.
  end.
buildrecin:
for each wuyers use-index wkey3 where
      wuyers.wmaster     = false                 and
      wuyers.wregion     = r1range.r1region      and
      wuyers.wdistrict   = r1range.r1district    and
      wuyers.wicsdregion = r1range.r1icsdregion  and
      wuyers.wwhse       = r1range.r1whse        and
      wuyers.wgroup      = r1range.r2group       and
      wuyers.wbuyer      = r1range.r2buyer       and
      wuyers.wdept       = r1range.r2dept        and
      wuyers.wvendor     = r1range.r2vendor      and
      wuyers.wpline      = r1range.r2pline       and
      wuyers.wprodcat    = r1range.r3prodcat     and 
      wuyers.wcustno     ge zel_begcust          and
      wuyers.wcustno     le zel_endcust 
      no-lock:
      
 
        
  assign pk_good = true
         zelection_type = "c"
         zelection_char4 = " "
         zelection_cust = wuyers.wcustno.
  run zelectioncheck.
  if zelection_good = false then
    do:
    pk_good = false.
    next buildrecin.
    end.
 
                   
 
 /* Sales Rep by technology  */
  /*  
   run checkparams.
   
   if not pk_good then
    next buildrec.
  */
   
 /*
   if p-recvmonths <> 0 then
     do:
     find obsolete where obsolete.o-prod = wuyers.wprod no-lock no-error.
     if avail obsolete then
       if obsolete.o-rcptdt > today - (p-recvmonths * 30) and
          obsolete.o-rcptdt <> ? then
          next buildrecin.
     end.
   if p-ohonly and wuyers.wcur = 0 then
     next buildrecin.
  */
 /*
 end. /* wuyers */
 end. /* ranges */
*/
 
 
 
 
 
 
/{&user_foreach}*/
 
/{&user_drptassign}*/
 /* If you add fields to the dprt table this is where you assign them */
 
 
 
 
 
 
 
/{&user_drptassign}*/
 
/{&user_B4endloop}*/
/* this is before the end of the foreach loop. */
  end. /* default the program has 1 end for a for each we have 2 */
       /* for eaches here so we needed an extra end */
 
 
 
 
 
/{&user_B4endloop}*/
 
/{&user_viewheadframes}*/
/* Since frames are unique to each program you need to put your
   views and hides here with your names */
 
 if g-operinits = "tah" then
   do:
   message p-optiontype p-sorttype p-totaltype p-summcounts.
    message p-register p-registerex.
   message "Entry " u-entitys.
   message "Reporting "  today "@ "  
                 int(truncate (time / 3600,0))  ":"
                 int(truncate ((time modulo 3600) / 60 ,0)).
   end.
 


if not p-exportl then
  do:
  if not x-stream then
    do:
    hide frame f-trn1.
    hide frame f-trn2.
    hide frame f-trn3.
    hide frame f-trn4.
    hide frame f-trn5.
    end.
  else
   do:
    hide stream xpcd frame f-trn1.
    hide stream xpcd frame fx-trn2.
    hide stream xpcd frame f-trn3.
    hide stream xpcd frame f-trn4.
    hide stream xpcd frame f-trn5.
    end.
  
  end.
if not p-exportl  then
  do:
  if not x-stream then
    do:
    page.
    view frame f-trn1.
    view frame f-trn2.
    view frame f-trn3.      
    view frame f-trn4.      
    end.
  else  
    do:
    page stream xpcd.
    view stream xpcd frame f-trn1.
    view stream xpcd frame fx-trn2.
    view stream xpcd frame f-trn3.
    view stream xpcd frame f-trn4.
    end.
   
  end.
 
 
 
 
 
 
 
/{&user_viewheadframes}*/
 
/{&user_drptloop}*/
 
 
 
 
 
 
 
/{&user_drptloop}*/
 
/{&user_detailprint}*/
   /*
   /* Comment this out if you are not using detail 'X' option  */
   if p-detailprt and 
      entry(u-entitys,drpt.sortmystery,v-delim) <> 
      fill("~~",length(entry(u-entitys,drpt.sortmystery,v-delim))) then 
     do:
     /* this code skips the all other lines for printing detail */
     
     /* next line would be your detail print procedure */
     p-detailprt = p-detailprt.
     end.
   */
 
 
 
 
 
 
/{&user_detailprint}*/
 
/{&user_finalprint}*/
if not p-exportl then
 do:
 assign t-amounts6[u-entitys + 1] =
    (if t-amounts5[u-entitys + 1] = 0 then 
       0                       
     else if t-amounts2[u-entitys + 1] = 0 then 
       100                   
     else if t-amounts5[u-entitys + 1] / t-amounts2[u-entitys + 1] < -999 then
       -999 
     else if t-amounts5[u-entitys + 1] / t-amounts2[u-entitys + 1] > 999 then
       999   
     else t-amounts5[u-entitys + 1] / t-amounts2[u-entitys + 1]).              
 assign t-amounts7[u-entitys + 1] = 
    (if t-amounts1[u-entitys + 1] = 0 then 
       0                       
     else if t-amounts1[u-entitys + 1] = 0 then 
       0                   
     else if t-amounts4[u-entitys + 1] / t-amounts1[u-entitys + 1] < -999 then
       -999 
     else if t-amounts4[u-entitys + 1] / t-amounts1[u-entitys + 1] > 999 then 
       999   
     else t-amounts4[u-entitys + 1] / t-amounts1[u-entitys + 1]).              
 assign t-amounts8[u-entitys + 1] = 
          t-amounts6[u-entitys + 1] * 
          t-amounts7[u-entitys + 1].
 assign t-amounts10[u-entitys + 1] = 
    (if t-amounts9[u-entitys + 1] = 0 then 
       0                       
     else if t-amounts2[u-entitys + 1] = 0 then
       0                       
     else if t-amounts9[u-entitys + 1] / t-amounts2[u-entitys + 1] < -999 then
       -999 
     else if t-amounts9[u-entitys + 1] / t-amounts2[u-entitys + 1] > 999 then 
       999   
     else t-amounts9[u-entitys + 1] / t-amounts2[u-entitys + 1]).                    
 assign t-amounts7[u-entitys + 1] = 
          t-amounts7[u-entitys + 1] * 100.
 if t-amounts7[u-entitys + 1] > 999.99 then
   assign t-amounts7[u-entitys + 1] = 999.99.
 else
 if t-amounts7[u-entitys + 1] < -999.99 then
   assign t-amounts7[u-entitys + 1] = -999.99.
 if not x-stream then 
   do:
   display 
    v-final
    v-astrik
    v-summary-lit2
    t-amounts1[u-entitys + 1]  @ v-amt1
    t-amounts2[u-entitys + 1]  @ v-amt2
    t-amounts6[u-entitys + 1]  @ v-amt3
    t-amounts7[u-entitys + 1]  @ v-amt4
    t-amounts8[u-entitys + 1]  @ v-amt5
    t-amounts9[u-entitys + 1]  @ v-amt6
    t-amounts10[u-entitys + 1] @ v-amt7
    with frame f-tot.
  
   down with frame f-tot.
   end.
 else
   do:
   display stream xpcd
    v-final
    v-astrik
    v-summary-lit2
    t-amounts1[u-entitys + 1]  @ v-amt1
    t-amounts2[u-entitys + 1]  @ v-amt2
    t-amounts6[u-entitys + 1]  @ v-amt3
    t-amounts7[u-entitys + 1]  @ v-amt4
    t-amounts8[u-entitys + 1]  @ v-amt5
    t-amounts9[u-entitys + 1]  @ v-amt6
    t-amounts10[u-entitys + 1] @ v-amt7
    with frame f-tot.
  
   down stream xpcd with frame f-tot.
   end.
      
 end.
else
  do:
  assign export_rec =  v-astrik.
  do v-inx4 = 1 to u-entitys:
    if v-totalup[v-inx4] = "n" then
       next.
    if v-inx4 = 1 then
      v-summary-lit2 = "Final".
    else
      v-summary-lit2 = "".
    if v-cells[v-inx4]= "2" then
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec + v-summary-lit2 + v-del + " ".
     
      end.
    else
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec +  v-summary-lit2.
      end.
  end.
  assign t-amounts6[u-entitys + 1] =
    (if t-amounts5[u-entitys + 1] = 0 then 
       0                       
     else if t-amounts2[u-entitys + 1] = 0 then 
       100                   
     else if t-amounts5[u-entitys + 1] / t-amounts2[u-entitys + 1] < -999 then
       -999 
     else if t-amounts5[u-entitys + 1] / t-amounts2[u-entitys + 1] > 999 then
       999   
     else t-amounts5[u-entitys + 1] / t-amounts2[u-entitys + 1]).                assign t-amounts7[u-entitys + 1] = 
    (if t-amounts1[u-entitys + 1] = 0 then 
       0                       
     else if t-amounts1[u-entitys + 1] = 0 then 
       0                   
     else if t-amounts4[u-entitys + 1] / t-amounts1[u-entitys + 1] < -999 then
       -999 
     else if t-amounts4[u-entitys + 1] / t-amounts1[u-entitys + 1] > 999 then 
       999   
     else t-amounts4[u-entitys + 1] / t-amounts1[u-entitys + 1]).                    
   assign t-amounts8[u-entitys + 1] = 
          t-amounts6[u-entitys + 1] * 
          t-amounts7[u-entitys + 1].
   assign t-amounts10[u-entitys + 1] = 
    (if t-amounts9[u-entitys + 1] = 0 then 
       0                       
     else if t-amounts2[u-entitys + 1] = 0 then
       0                       
     else if t-amounts9[u-entitys + 1] / t-amounts2[u-entitys + 1] < -999 then
       -999 
     else if t-amounts9[u-entitys + 1] / t-amounts2[u-entitys + 1] > 999 then 
       999   
     else t-amounts9[u-entitys + 1] / t-amounts2[u-entitys + 1]).                    
   assign t-amounts7[u-entitys + 1] = 
          t-amounts7[u-entitys + 1] * 100.
   if t-amounts7[u-entitys + 1] > 999.99 then
     assign t-amounts7[u-entitys + 1] = 999.99.
   else
   if t-amounts7[u-entitys + 1] < -999.99 then
     assign t-amounts7[u-entitys + 1] = -999.99.
 
   
   
   assign export_rec = export_rec + v-del +
      string(t-amounts1[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts11[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts2[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts6[u-entitys + 1],"->>>>>9.99") + v-del +
      string(t-amounts7[u-entitys + 1],"->>>>>9.99") + v-del +
      string(t-amounts8[u-entitys + 1],"->>>>>9.99") + v-del +
      string(t-amounts12[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts9[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts10[u-entitys + 1],"->>>>>9.99").
    
  assign export_rec = export_rec + chr(13).    
  if not p-exportDWl then
    put stream expt unformatted export_rec.
  end.
 
 
 
 
 
 
 
/{&user_finalprint}*/
 
/{&user_summaryframeprint}*/
  assign t-amounts6[v-inx2] =
    (if t-amounts5[v-inx2] = 0 then 
       0                       
     else if t-amounts2[v-inx2] = 0 then 
       100                   
     else if t-amounts5[v-inx2] / t-amounts2[v-inx2] < -999 then
       -999 
     else if t-amounts5[v-inx2] / t-amounts2[v-inx2] > 999 then
       999   
     else t-amounts5[v-inx2] / t-amounts2[v-inx2]).
                     
  assign t-amounts7[v-inx2] = 
    (if t-amounts1[v-inx2] = 0 then 
       0                       
     else if t-amounts1[v-inx2] = 0 then 
       0                   
     else if t-amounts4[v-inx2] / t-amounts1[v-inx2] < -999 then
       -999 
     else if t-amounts4[v-inx2] / t-amounts1[v-inx2] > 999 then 
       999   
     else t-amounts4[v-inx2] / t-amounts1[v-inx2]).                    
   assign t-amounts8[v-inx2] = 
          t-amounts6[v-inx2] * 
          t-amounts7[v-inx2].
   assign t-amounts10[v-inx2] = 
    (if t-amounts9[v-inx2] = 0 then 
       0                       
     else if t-amounts2[v-inx2] = 0 then
       0                       
     else if t-amounts9[v-inx2] / t-amounts2[v-inx2] < -999 then
       -999 
     else if t-amounts9[v-inx2] / t-amounts2[v-inx2] > 999 then 
       999   
     else t-amounts9[v-inx2] / t-amounts2[v-inx2]).                     
    
    
    assign t-amounts7[v-inx2] = 
           t-amounts7[v-inx2] * 100.
    if t-amounts7[v-inx2] > 999.99 then
      assign t-amounts7[v-inx2] = 999.99.
    else
    if t-amounts7[v-inx2] < -999.99 then
      assign t-amounts7[v-inx2] = -999.99.
       
    if not x-stream then
      do:
      display 
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        t-amounts6[v-inx2]   @ v-amt3
        t-amounts7[v-inx2]   @ v-amt4
        t-amounts8[v-inx2]   @ v-amt5
        t-amounts9[v-inx2]   @ v-amt6
        t-amounts10[v-inx2]   @ v-amt7
        
        with frame f-tot.
      down with frame f-tot.
      end.
    else
      do:
      display stream xpcd
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        t-amounts6[v-inx2]   @ v-amt3
        t-amounts7[v-inx2]   @ v-amt4
        t-amounts8[v-inx2]   @ v-amt5
        t-amounts9[v-inx2]   @ v-amt6
        t-amounts10[v-inx2]   @ v-amt7
        with frame f-tot.
      down stream xpcd with frame f-tot.
      end.
 
 
 
 
 
 
 
/{&user_summaryframeprint}*/
 
/{&user_summaryputexport}*/
 
  assign t-amounts6[v-inx2] =
    (if t-amounts5[v-inx2] = 0 then 
       0                       
     else if t-amounts2[v-inx2] = 0 then 
       100                   
     else if t-amounts5[v-inx2] / t-amounts2[v-inx2] < -999 then
       -999 
     else if t-amounts5[v-inx2] / t-amounts2[v-inx2] > 999 then
       999   
     else t-amounts5[v-inx2] / t-amounts2[v-inx2]).
                     
  assign t-amounts7[v-inx2] = 
    (if t-amounts1[v-inx2] = 0 then 
       0                       
     else if t-amounts1[v-inx2] = 0 then 
       0                   
     else if t-amounts4[v-inx2] / t-amounts1[v-inx2] < -999 then
       -999 
     else if t-amounts4[v-inx2] / t-amounts1[v-inx2] > 999 then 
       999   
     else t-amounts4[v-inx2] / t-amounts1[v-inx2]).                    
   assign t-amounts8[v-inx2] = 
          t-amounts6[v-inx2] * 
          t-amounts7[v-inx2].
   assign t-amounts10[v-inx2] = 
    (if t-amounts9[v-inx2] = 0 then 
       0                       
     else if t-amounts2[v-inx2] = 0 then
       0                       
     else if t-amounts9[v-inx2] / t-amounts2[v-inx2] < -999 then
       -999 
     else if t-amounts9[v-inx2] / t-amounts2[v-inx2] > 999 then 
       999   
     else t-amounts9[v-inx2] / t-amounts2[v-inx2]).                     
    
    
    assign export_rec = v-astrik + v-del + export_rec.
    
    assign t-amounts7[v-inx2] = 
           t-amounts7[v-inx2] * 100.
    if t-amounts7[v-inx2] > 999.99 then
      assign t-amounts7[v-inx2] = 999.99.
    else
    if t-amounts7[v-inx2] < -999.99 then
      assign t-amounts7[v-inx2] = -999.99.
       
    
    assign export_rec = export_rec + v-del +
      string(t-amounts1[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts11[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts2[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts6[v-inx2],"->>>>>9.99") + v-del +
      string(t-amounts7[v-inx2],"->>>>>9.99") + v-del +
      string(t-amounts8[v-inx2],"->>>>>9.99") + v-del +
      string(t-amounts12[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts9[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts10[v-inx2],"->>>>>9.99") + v-del + 
      string(p-asofdate,"99/99/9999").
      
  if p-exportDWl then
    assign export_rec = export_rec + v-del +
      string(t-amounts3[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts5[v-inx2],"->>>>>>>>9").
 
 
 
 
 
 
 
/{&user_summaryputexport}*/
 
/{&user_procedures}*/
procedure sapb_vars:
find sapb where recid(sapb) = v-sapbid no-lock no-error.
 
assign p-bgrpbeg = sapb.rangebeg[1]
       p-bgrpend = sapb.rangeend[1]
       p-regbeg = sapb.rangebeg[2]
       p-regend = sapb.rangeend[2]
       p-whsebeg = sapb.rangebeg[3]
       p-whseend = sapb.rangeend[3]
       p-buyrbeg = sapb.rangebeg[4]
       p-buyrend = sapb.rangeend[4]
       p-vendbeg = dec(sapb.rangebeg[5])
       p-vendend = dec(sapb.rangeend[5])
       p-deptbeg = sapb.rangebeg[7]
       p-deptend = sapb.rangeend[7]
       p-linebeg = sapb.rangebeg[6]
       p-lineend = sapb.rangeend[6]
       p-techbeg = sapb.rangebeg[8]
       p-techend = sapb.rangeend[8]
       p-vpbeg = sapb.rangebeg[9]
       p-vpend = sapb.rangeend[9]
       p-iregbeg = sapb.rangebeg[10]
       p-iregend = sapb.rangeend[10]
       p-prodbeg = sapb.rangebeg[11]
       p-prodend = sapb.rangeend[11]
       p-vidbeg  = sapb.rangebeg[12]
       p-vidend  = sapb.rangeend[12]
       p-custbeg = dec(sapb.rangebeg[13])
       p-custend = dec(sapb.rangeend[13])
       p-pcatbeg  = sapb.rangebeg[14]
       p-pcatend  = sapb.rangeend[14]
       p-repbeg  = sapb.rangebeg[15]
       p-repend  = sapb.rangeend[15].
assign  p-distbeg = substring(p-regbeg,2,3)
        p-distend = substring(p-regend,2,3)
        p-regbeg = substring(p-regbeg,1,1)
        p-regend = substring(p-regend,1,1).
assign       
       i-turnmo = int(sapb.optvalue[1])
       p-list = if sapb.optvalue[3] = "yes" then true else false
       p-export = sapb.optvalue[4]
       p-currentinfo = if sapb.optvalue[6] = "yes" then true else false
       /*
       p-obsolete = if sapb.optvalue[5] = "yes" then true else false
       p-recvmonths = int(sapb.optvalue[6])
       p-ohonly   = if sapb.optvalue[7] = "yes" then true else false
       */
       p-pastsuff = sapb.optvalue[5].
if sapb.optvalue[7] = "" or
   sapb.optvalue[7] = ? then
  p-asofdate = today.
else
  do:
  v-datein = sapb.optvalue[7].
  {p-rptdt.i}
  if string(v-dateout) = v-lowdt then
    p-asofdate = today.
  else  
    p-asofdate = v-dateout.
  end.
       
if i-turnmo = 0 then
  if month(today) = 12 then
    i-turnmo = 1.
  else
    i-turnmo = month(today) - 1.
assign zzl_begcat =  p-pcatbeg
       zzl_endcat =  p-pcatend
       zzl_begcust = p-custbeg
       zzl_endcust = p-custend
       zzl_begvname  = p-vidbeg
       zzl_endvname  = p-vidend
       zzl_begvend   = p-vendbeg
       zzl_endvend   = p-vendend
       zzl_begbuy    = p-buyrbeg
       zzl_endbuy    = p-buyrend
       zzl_begwhse   = p-whsebeg
       zzl_endwhse   = p-whseend
       zzl_begrep  = p-repbeg
       zzl_endrep  = p-repend.
assign zel_begcat =  p-pcatbeg
       zel_endcat =  p-pcatend
       zel_begcust = p-custbeg
       zel_endcust = p-custend
       zel_begvname  = p-vidbeg
       zel_endvname  = p-vidend
       zel_begvend   = p-vendbeg
       zel_endvend   = p-vendend
       zel_begbuy    = p-buyrbeg
       zel_endbuy    = p-buyrend
       zel_begwhse   = p-whsebeg
       zel_endwhse   = p-whseend
       zel_begrep  = p-repbeg
       zel_endrep  = p-repend.
if p-list then
  do:
  assign zel_begcat =  p-pcatbeg
         zel_endcat =  p-pcatend
         zel_begcust = p-custbeg
         zel_endcust = p-custend
         zel_begvname  = p-vidbeg
         zel_endvname  = p-vidend
         zel_begvend   = p-vendbeg
         zel_endvend   = p-vendend
         zel_begbuy    = p-buyrbeg
         zel_endbuy    = p-buyrend
         zel_begwhse   = p-whsebeg
         zel_endwhse   = p-whseend
         zel_begrep  = p-repbeg
         zel_endrep  = p-repend.
          
    {zsapboyload.i}
  end.
/* Zelection_matrix has to have an extent for each selection type 
   right now we have. During the load these booleans are set showing what
   options have been selected.
   
   s - sales rep               ....Index 1
   c - customer number         ....Index 2
   p - product cat             ....Index 3
   b - buyer                   ....Index 4
   t - Order types             ....Index 5
   w - Warehouse               ....Index 6
   v - Vendor number           ....Index 7
   N - Vendor Name             ....Index 8
   
 PLEASE KEEP THIS DOCUMENTATION UPDATED WHEN CHANGED 
 
*/ 
  
assign zelection_matrix [1] = (if zelection_matrix[1] > 1 then /* Rep   */
                                 zelection_matrix[1]
                              else   
                                 1)
       zelection_matrix [2] = (if zelection_matrix[2] > 1 then  /* Cust */
                                 zelection_matrix[2]
                              else   
                                 1)
       zelection_matrix [3] = (if zelection_matrix[3] > 1 then  /* Cat  */
                                 zelection_matrix[3]
                              else   
                                 1)
       zelection_matrix [4] = (if zelection_matrix[4] > 1 then  /* Buyer  */
                                 zelection_matrix[4]
                              else   
                                 1)
       zelection_matrix [6] = (if zelection_matrix[6] > 1 then  /* Whse */
                                 zelection_matrix[6]
                              else   
                                 1)
       zelection_matrix [7] = (if zelection_matrix[7] > 1 then  /* Vendor */
                                 zelection_matrix[7]
                              else   
                                 1)
                                 
       zelection_matrix [8] = (if zelection_matrix[8] > 1 then  /* VName */
                                 zelection_matrix[8]
                              else   
                                 1).
                    
    
  run formatoptions.
end.
/* ------------------------------------------------------------------------- */
procedure formatoptions:
/* ------------------------------------------------------------------------- */
   
   
  assign p-portrait = true.
  /* if false then the report will print in LANDSCAPE 178 character */
  
  if sapb.optvalue[2] > "0" and sapb.optvalue[2] ne "99" then 
    do:
    p-optiontype = " ".
    p-format = int(sapb.optvalue[2]).
    find notes where notes.cono = g-cono and
                     notes.notestype = "zz" and
                     notes.primarykey = "icxe" and  /* tbxr program */
                     notes.secondarykey = string(p-format) no-lock no-error.
    if not avail notes then
      do:
      display "Format is not valid cannot process request".
      assign p-optiontype = "r"
             p-sorttype   = ">,"
             p-totaltype  = "l"
             p-summcounts = "a".
      return.
      end.               
   
    assign p-optiontype = notes.noteln[1]
           p-sorttype   = notes.noteln[2]
           p-totaltype  = notes.noteln[3]
           p-summcounts = notes.noteln[4]
           p-register   = notes.noteln[5]
           p-registerex = notes.noteln[6].
        
    find x-sapb where recid(x-sapb) = recid(sapb) exclusive-lock.
    
    assign x-sapb.user5 = 
             p-optiontype + "~011" +
             p-sorttype   + "~011" +
             p-totaltype  + "~011" +
             p-summcounts + "~011" +
             p-register   + "~011" +
             p-registerex.     
    
    end.     
  else
  if sapb.optvalue[2] = "99" then 
    do:
    assign p-register   = ""
           p-registerex = "".
    run reportopts(input sapb.user5, 
                   input-output p-optiontype,
                   input-output p-sorttype,  
                   input-output p-totaltype,
                   input-output p-summcounts,
                   input-output p-register,
                   input-output p-registerex).
    end.
 
 run Print_reportopts (input recid (sapb),
                       input g-cono).
 
 
 end.
   
/* ------------------------------------------------------------------------- */
procedure extract_data:
/* ------------------------------------------------------------------------- */
 if g-operinits = "tah" then
   do:
   message p-optiontype p-sorttype p-totaltype p-summcounts.
   message p-register p-registerex.
   message "before introduce"  today "@ "  
                 int(truncate (time / 3600,0))  ":"
                 int(truncate ((time modulo 3600) / 60 ,0)).
   end.
 run introduce_stats.
 if g-operinits = "tah" then
   do:
   message "after introduce" today "@ "  
                 int(truncate (time / 3600,0))  ":"
                 int(truncate ((time modulo 3600) / 60 ,0)).
   end.
/* 
 input stream xpcd from value(v-printdir + "tbzbmnthdet.dat").
    
 repeat:
 create obsolete.
 import stream xpcd delimiter "~011"
       obsolete.o-prod
       obsolete.o-proddesc
       obsolete.o-vendor
       obsolete.o-vname    
       obsolete.o-ls12month 
       obsolete.o-6month   
       obsolete.o-12month   
       obsolete.o-24month  
       obsolete.o-48month  
       obsolete.o-60month  
       obsolete.o-g60month  
       obsolete.o-ns7
       obsolete.o-ns12     
       obsolete.o-ns24     
       obsolete.o-total    
       obsolete.o-asofdate 
       obsolete.o-enterdt  
       obsolete.o-rcptdt   
       obsolete.o-invdt    
       obsolete.o-firstrcpt 
       obsolete.o-rcptohdt 
       obsolete.o-12b4rcpt    
       obsolete.o-rcptoh.       
        
  end.
  
  input stream xpcd close.
*/
if not v-process then
  return.  /* loaded from the repository */
if p-pastsuff <> " " and
  search(v-printdir + "wuyers.cxt0" + "." + p-pastsuff) <> ? then
  do: 
  input stream xpcd from
     value(v-printdir + "wuyers.cxt0" + "." + p-pastsuff).
   
  message "TBZN - Check option 1, diverting repository to " p-pastsuff.
  end.   
   
else                    
  input stream xpcd from value(v-printdir + "wuyers.cxt0").
/* bring in repository */
 if g-operinits = "tah" then
   do:
   message "before wuyers" today "@ "  
                 int(truncate (time / 3600,0))  ":"
                 int(truncate ((time modulo 3600) / 60 ,0)).
   end.
repeat:
  create wuyers.
  import stream xpcd wuyers
      except wsales wcost wmargin wasofdt.
  find first spprd use-index rdpinx where 
             spprd.region      = wuyers.wregion   and
             spprd.district    = wuyers.wdistrict and
             spprd.whse        = wuyers.wwhse     and
             spprd.custno      = wuyers.wcustno   and
             spprd.shipto      = wuyers.wshipto   and
             spprd.prod        = wuyers.wprod  no-error.
  if avail spprd then
    assign wuyers.wsales  = spprd.xsales        
           wuyers.wcost   = spprd.sales
           wuyers.wmargin = spprd.xmgn
           spprd.xused    = true.  
  else
    assign wuyers.wsales  = 0        
           wuyers.wcost   = 0
           wuyers.wmargin = 0.  
  assign wuyers.wannualCgs = 
         ( (wuyers.wcgs + wuyers.wncgs) / /* i-turnmo */ 12) * 12
         wuyers.wqobso = 
           wuyers.wq60   + 
           wuyers.wqg60  + 
           wuyers.wqns12 + 
           wuyers.wqns24
         wuyers.wobso = 
           wuyers.w60   + 
           wuyers.wg60  + 
           wuyers.wns12 + 
           wuyers.wns24
         wuyers.wasofdt = p-asofdate. 
    if p-currentinfo then  
      do:
      h-prodline = "".
      h-buyer = "".
      h-searchlevel = 0.
      if h-buyer = "" then
        do:
        {p-zfindbuygne2.i
               wuyers.wwhse
               wuyers.wprod
               h-buyer
               h-vendor
               h-prodline
               h-searchlevel}
               
        end.
           
      if h-vendor = 0 then
        run vendor_lookup(input wuyers.wwhse,
                          input wuyers.wprod).
      if h-prodline = ""  then
        assign
          h-prodline = "misc".
  
      find sasta where sasta.cono = g-cono
                   and sasta.codeiden = "B"
                   and sasta.codeval = h-buyer
        no-lock no-error.
      assign d-name = if avail sasta then 
                        sasta.descrip 
                      else 
                        "BuyerNotFound".
  
      find oimsp where oimsp.person = h-buyer no-lock no-error.
      if avail oimsp then
        h-person = oimsp.responsible.
      else   
        h-person = "NTFND".
      assign
         wuyers.wname            = d-name
         wuyers.wpline           = h-prodline
         wuyers.wdept            = h-person
         wuyers.wbuyer           = h-buyer
         wuyers.wvendor          = h-vendor.
        
      end.    
  run createranges.
  
end.
 if g-operinits = "tah" then
   do:
   message "After wuyers" today "@ "  
                 int(truncate (time / 3600,0))  ":"
                 int(truncate ((time modulo 3600) / 60 ,0)).
   end.
input stream xpcd close.
 if g-operinits = "tah" then
   do:
   message "before Adders " today "@ "  
                 int(truncate (time / 3600,0))  ":"
                 int(truncate ((time modulo 3600) / 60 ,0)).
   end.
  
for each spprd use-index rdpinx2 where
         spprd.xused = false:
  assign spprd.xused             = true. 
  find icsd where icsd.cono = g-cono and 
                  icsd.whse = spprd.whse no-lock no-error.
  if  not avail icsd then
    next.
  
  
  h-prodline = "".
  h-buyer = "".
  h-searchlevel = 0.
  {p-zfindbuygne2.i
         spprd.whse
         spprd.prod
         h-buyer
         h-vendor
         h-prodline
         h-searchlevel}
  if h-vendor = 0 then
    run vendor_lookup(input spprd.whse,
                      input spprd.prod).
 
       
       
  if h-prodline = ""  then
    assign
      h-prodline = "misc".
  
  find sasta where sasta.cono = g-cono
               and sasta.codeiden = "B"
              and sasta.codeval = h-buyer
       no-lock no-error.
  assign d-name = if avail sasta then sasta.descrip else "BuyerNotFound".
 
  
  
  find oimsp where oimsp.person = h-buyer no-lock no-error.
  if avail oimsp then
    h-person = oimsp.responsible.
  else   
    h-person = "NTFND".
  find icsw where icsw.cono = g-cono and
                  icsw.whse = spprd.whse and
                  icsw.prod = spprd.prod no-lock no-error.
  find icsp where icsp.cono = g-cono and
                  icsp.prod = spprd.prod no-lock no-error.
                  
  
  create wuyers.
  assign wuyers.wregion          = spprd.region
         wuyers.wdistrict        = spprd.district
         wuyers.woh              = 0
         wuyers.wavgcost         = 0
         wuyers.wcustno          = spprd.custno
         wuyers.wshipto          = spprd.shipto
         wuyers.wlstsale         = spprd.lstsale 
         wuyers.wlsttrans        = spprd.lsttrans
         wuyers.wmaster          = false
         wuyers.wwhse            = spprd.whse
         wuyers.wname            = d-name
         wuyers.wpline           = h-prodline
         wuyers.wgroup           = icsd.buygroup
         wuyers.wicsdregion      = icsd.region
         wuyers.wdept            = h-person
         wuyers.wbuyer           = h-buyer
         wuyers.wprod            = spprd.prod
         wuyers.wprodcat         = spprd.prodcat
         wuyers.wvendor          = h-vendor
         wuyers.wcur             = 0
         wuyers.wncur            = 0
         wuyers.wavg             = 0
         wuyers.wcgs             = 0
         wuyers.wncgs            = 0
         wuyers.wls12            = 0
         wuyers.w6               = 0
         wuyers.w12              = 0
         wuyers.w24              = 0
         wuyers.w48              = 0
         wuyers.w60              = 0
         wuyers.wg60             = 0
         wuyers.wns7             = 0
         wuyers.wns12            = 0
         wuyers.wns24            = 0
         wuyers.wqls12           = 0
         wuyers.wq6              = 0
         wuyers.wq12             = 0
         wuyers.wq24             = 0
         wuyers.wq48             = 0
         wuyers.wq60             = 0
         wuyers.wqg60            = 0
         wuyers.wqns7            = 0
         wuyers.wqns12           = 0
         wuyers.wqns24           = 0
         wuyers.wsales           = spprd.xsales
         wuyers.wcost            = spprd.sales 
         wuyers.wmargin          = spprd.xmgn
         wuyers.wannualCgs       = 0
         wuyers.wtotinv          = 0
         wuyers.wqobso           = 0
         wuyers.wobso            = 0
         wuyers.wasofdt          = p-asofdate
/* New */
         wuyers.wnewfile     = "Y"
         wuyers.wcgs3        = 0      
         wuyers.wratio       = 1.00
         wuyers.wkittype     = if avail icsp then 
                                   icsp.kittype
                               else
                                   " "
         wuyers.warptype     = if avail icsw then
                                 icsw.arptype
                               else
                                 " "
         wuyers.wparent      =  if avail icsp and avail icsw then
                                  if (icsp.kittype = "p" and icsw.arptype = "k")                                     or 
                                     (icsp.kittype = "b" or icsw.arptype = "f")                                    then
                                      "Y"
                                  else
                                     " "
                                 else 
                                     " ".
/* New */              
  run createranges.
 end.
 if g-operinits = "tah" then
   do:
   message "After  Adders " today "@ "  
                 int(truncate (time / 3600,0))  ":"
                 int(truncate ((time modulo 3600) / 60 ,0)).
   message "before export" today "@ "  
          int(truncate (time / 3600,0))  ":"
          int(truncate ((time modulo 3600) / 60 ,0)).
   end.
  
output stream xpcd to 
  value (v-printdir + "tbetr" + string(i-turnmo,"99") + ".sxk").
for each wuyers no-lock:
  export stream xpcd wuyers except wqobso wasofdt .
end.  
output stream xpcd close. 
 if g-operinits = "tah" then
   do:
   message "After  export " today "@ "  
         int(truncate (time / 3600,0))  ":"
         int(truncate ((time modulo 3600) / 60 ,0)).
   end.
 
end.     
     
     
/* End Normal data extraction  */
  
Procedure createranges:
      
find r1range where
 r1range.r1region       = wuyers.wregion and
 r1range.r1district     = wuyers.wdistrict and
 r1range.r1icsdregion   = wuyers.wicsdregion  and
 r1range.r1whse         = wuyers.wwhse   and
 r1range.r2group = wuyers.wgroup and
 r1range.r2buyer = wuyers.wbuyer and
 r1range.r2dept  = wuyers.wdept and
 r1range.r2vendor = wuyers.wvendor and
 r1range.r2pline = wuyers.wpline and
 r1range.r3prodcat = wuyers.wprodcat no-lock no-error.
if not avail r1range then
  do:
  create r1range.
  assign  r1range.r1region       = wuyers.wregion
          r1range.r1district     = wuyers.wdistrict
          r1range.r1icsdregion   = wuyers.wicsdregion
          r1range.r1whse         = wuyers.wwhse 
          r1range.r2group = wuyers.wgroup
          r1range.r2dept  = wuyers.wdept 
          r1range.r2buyer = wuyers.wbuyer
          r1range.r2pline = wuyers.wpline
          r1range.r3prodcat = wuyers.wprodcat
          r1range.r2vendor = wuyers.wvendor.
  end.
end.
procedure vendor_lookup.
define input parameter xx-whse like icsw.whse no-undo.
define input parameter xx-prod like icsw.prod no-undo.
   assign h-vendor = 0.
   find icsw where icsw.cono = g-cono and
                   icsw.whse = xx-whse and
                   icsw.prod = xx-prod no-lock no-error.
   if avail icsw then
    do:
    h-vendor = icsw.arpvend.
    end.
  if not avail icsw or h-vendor = 0 then  
    do:
    find last poel use-index k-prod
                   where poel.cono = g-cono and
                         poel.shipprod = xx-prod and
                         poel.whse = xx-whse no-lock no-error.
    if avail poel then
      do:
      h-vendor = poel.vendno. 
      end.
    else
      do:
       find last poel use-index k-prod
                   where poel.cono = g-cono and
                         poel.shipprod = xx-prod 
                         no-lock no-error.
       if avail poel then
         do:
         h-vendor = poel.vendno. 
         end.
      end.
     end.
end.
{zsapboycheck.i}
{p-sdietr.i} 
 
 
 
 
 
 
 
/{&user_procedures}*/
 
/* TBXR F6 CodeGen Includes from here down are CodeGen Generated  */ 
/{&user_optiontype}*/
if not
can-do("A,B,C,D,E,G,H,I,L,M,P,R,T,V,W",substring(p-optiontype,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries A,B,C,D,E,G,H,I,L,M,P,R,T,V,W "
     substring(p-optiontype,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries A,B,C,D,E,G,H,I,L,M,P,R,T,V,W "
     substring(p-optiontype,v-inx,1).
/{&user_optiontype}*/
 
/{&user_registertype}*/
if not
can-do("A,B,C,D,E,G,H,I,L,M,P,R,T,V,W",substring(p-register,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries A,B,C,D,E,G,H,I,L,M,P,R,T,V,W "
     substring(p-register,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries A,B,C,D,E,G,H,I,L,M,P,R,T,V,W "
     substring(p-register,v-inx,1).
/{&user_registertype}*/
 
/{&user_DWvarheaders1}*/
if v-lookup[v-inx4] = "A" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "ProdCat".
  assign export_rec = export_rec + v-del + "ProdCatDesc".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "B" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Buyer".
  assign export_rec = export_rec + v-del + "BuyerName".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "CustNbr".
  assign export_rec = export_rec + v-del + "CustName".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "District".
  end. 
else  
if v-lookup[v-inx4] = "E" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "VendParentName".
  end. 
else  
if v-lookup[v-inx4] = "G" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "BuyerGroup".
  end. 
else  
if v-lookup[v-inx4] = "H" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Technology".
  end. 
else  
if v-lookup[v-inx4] = "I" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "VendorId".
  end. 
else  
if v-lookup[v-inx4] = "L" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "ProdLine".
  end. 
else  
if v-lookup[v-inx4] = "M" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Department".
  end. 
else  
if v-lookup[v-inx4] = "P" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Prod".
  assign export_rec = export_rec + v-del + "ProdDesc".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Region".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "CustShipTo".
  end. 
else  
if v-lookup[v-inx4] = "V" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "VendNbr".
  assign export_rec = export_rec + v-del + "VendName".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Warehouse".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
/{&user_DWvarheaders1}*/
 
/{&user_exportvarheaders1}*/
if v-lookup[v-inx4] = "A" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Product".
  assign export_rec = export_rec + v-del + "Category".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "B" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Buyer".
  assign export_rec = export_rec + v-del + "Buyer Name".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Customer".
  assign export_rec = export_rec + v-del + "Customer".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "District".
  end. 
else  
if v-lookup[v-inx4] = "E" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Vendor".
  end. 
else  
if v-lookup[v-inx4] = "G" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Buyer Group".
  end. 
else  
if v-lookup[v-inx4] = "H" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Technology".
  end. 
else  
if v-lookup[v-inx4] = "I" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Vendor ID".
  end. 
else  
if v-lookup[v-inx4] = "L" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Product".
  end. 
else  
if v-lookup[v-inx4] = "M" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "P" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Product".
  assign export_rec = export_rec + v-del + "Product".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Region".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Customer".
  end. 
else  
if v-lookup[v-inx4] = "V" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Vendor".
  assign export_rec = export_rec + v-del + "Vendor".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Warehouse".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
/{&user_exportvarheaders1}*/
 
/{&user_exportvarheaders2}*/
if v-lookup[v-inx4] = "A" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Product".
  assign export_rec = export_rec + v-del + "Category Desc".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "B" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  assign export_rec = export_rec + v-del + "".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Number".
  assign export_rec = export_rec + v-del + "Name".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "E" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Parent".
  end. 
else  
if v-lookup[v-inx4] = "G" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "H" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "I" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "L" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Line".
  end. 
else  
if v-lookup[v-inx4] = "M" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "P" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  assign export_rec = export_rec + v-del + "Description".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Ship-to".
  end. 
else  
if v-lookup[v-inx4] = "V" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Number".
  assign export_rec = export_rec + v-del + "Name".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
/{&user_exportvarheaders2}*/
 
/{&user_loadtokens}*/
if v-lookup[v-inx] = "A" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(wuyers.wprodcat,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "B" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(wuyers.wbuyer,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "C" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(wuyers.wcustno,"999999999999")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"999999999999").   
  end.                                              
else                                              
if v-lookup[v-inx] = "D" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(wuyers.wdistrict,"x(3)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(3)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "E" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(v-parentcode,"x(15)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(15)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "G" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(wuyers.wgroup,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "H" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(v-technology,"x(15)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(15)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "I" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(v-vendorid,"x(15)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(15)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "L" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(wuyers.wpline,"x(6)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(6)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "M" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(wuyers.wdept,"x(10)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(10)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "P" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(wuyers.wprod,"x(24)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(24)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "R" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(wuyers.wregion,"x(1)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(1)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "T" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(wuyers.wshipto,"x(15)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(15)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "V" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(wuyers.wvendor,"999999999999")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"999999999999").   
  end.                                              
else                                              
if v-lookup[v-inx] = "W" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(wuyers.wwhse,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
  assign v-token = " ".
/{&user_loadtokens}*/
 
/{&user_totaladd}*/
  assign t-amounts[1] = wuyers.wsales.
  assign t-amounts[2] = wuyers.wcur + wuyers.wncur.
  assign t-amounts[3] = wuyers.wcgs + wuyers.wncgs.
  assign t-amounts[4] = wuyers.wmargin.
  assign t-amounts[5] = wuyers.wannualCgs.
  assign t-amounts[6] = 0.
  assign t-amounts[7] = 0.
  assign t-amounts[8] = 0.
  assign t-amounts[9] = wuyers.wobso.
  assign t-amounts[10] = 0.
  assign t-amounts[11] = wuyers.woh.
  assign t-amounts[12] = wuyers.wqobso.
/{&user_totaladd}*/
 
/{&user_numbertotals}*/
  if v-subtotalup[v-inx4] = 1 then
    v-val = t-amounts1[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 2 then
    v-val = t-amounts2[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 3 then
    v-val = t-amounts3[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 4 then
    v-val = t-amounts4[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 5 then
    v-val = t-amounts5[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 6 then
    v-val = 
      {x-tbxetr-fm1.i}
  else 
  if v-subtotalup[v-inx4] = 7 then
    v-val = 
      {x-tbxetr-fm7.i}
  else 
  if v-subtotalup[v-inx4] = 8 then
    v-val = 
      t-amounts6[v-inx4] * (t-amounts7[v-inx4] / 100).
  else 
  if v-subtotalup[v-inx4] = 9 then
    v-val = t-amounts9[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 10 then
    v-val = 
      {x-tbxetr-fm10.i}
  else 
  if v-subtotalup[v-inx4] = 11 then
    v-val = t-amounts11[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 12 then
    v-val = t-amounts12[v-inx4].
  else 
    v-val = t-amounts1[v-inx4].
  do inz = 1 to 12:
  if inz = 1 then
    v-regval[inz] = t-amounts1[v-inx4].
  else 
  if inz = 2 then
    v-regval[inz] = t-amounts2[v-inx4].
  else 
  if inz = 3 then
    v-regval[inz] = t-amounts3[v-inx4].
  else 
  if inz = 4 then
    v-regval[inz] = t-amounts4[v-inx4].
  else 
  if inz = 5 then
    v-regval[inz] = t-amounts5[v-inx4].
  else 
  if inz = 6 then
    v-regval[inz] = 
      {x-tbxetr-fm1.i}
  else 
  if inz = 7 then
    v-regval[inz] = 
      {x-tbxetr-fm7.i}
  else 
  if inz = 8 then
    v-regval[inz] = 
      t-amounts6[v-inx4] * (t-amounts7[v-inx4] / 100).
  else 
  if inz = 9 then
    v-regval[inz] = t-amounts9[v-inx4].
  else 
  if inz = 10 then
    v-regval[inz] = 
      {x-tbxetr-fm10.i}
  else 
  if inz = 11 then
    v-regval[inz] = t-amounts11[v-inx4].
  else 
  if inz = 12 then
    v-regval[inz] = t-amounts12[v-inx4].
  else 
    assign inz = inz.
  end.
/{&user_numbertotals}*/
 
/{&user_summaryloadprint}*/
  if v-lookup[v-inx2] = "A" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-prodcat.i}
      end. 
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Product Category - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "B" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-buyer.i}
      end. 
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Buyer - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "C" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-arsc.i}
      end. 
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Customer - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "D" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "District - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "E" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Vendor Parent Name - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "G" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Buyer Group - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "H" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Technology - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "I" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Vendor ID - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "L" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Product Line - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "M" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Department - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "P" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-product.i}
      end. 
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Product - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "R" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Region - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "T" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Customer Shipto - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "V" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-vendor.i}
      end. 
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Vendor Number - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "W" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Warehouse - " + h-lookup + " " + h-genlit.
    end.
  else
      v-summary-lit = v-lookup[v-inx2].
/{&user_summaryloadprint}*/
 
/{&user_summaryloadexport}*/
  if v-inx4 > v-inx2  then 
    do:
    if v-inx4 > 1 then          
      export_rec = export_rec + v-del. 
    assign export_rec = export_rec +  " ".
    if v-cells[v-inx4] = "2" then
      assign export_rec = export_rec +  v-del + " ".
    end. 
  else 
  if v-lookup[v-inx4] = "A" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-prodcat.i}
      end. 
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "B" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-buyer.i}
      end. 
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "C" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-arsc.i}
      end. 
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "999999999999" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "999999999999" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "D" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(3)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(3)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "E" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(15)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(15)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "G" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "H" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(15)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(15)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "I" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(15)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(15)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "L" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(6)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(6)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "M" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(10)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(10)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "P" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-product.i}
      end. 
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(24)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(24)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "R" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(1)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(1)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "T" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(15)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(15)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "V" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-vendor.i}
      end. 
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "999999999999" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "999999999999" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "W" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
    do:
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    assign export_rec = export_rec + v-lookup[v-inx4].
    end.
/{&user_summaryloadexport}*/
 
/* TBXR F6 CodeGen End of Includes that are CodeGen Generated  */ 
