/* z-faxc2.i RxLaser / VSIfax Template */
/*si**** Custom ****************************************************************
  AUTHOR  : mm
  VERSION : 8.0.004
  ANCHOR  : p-faxc2.i
  PURPOSE : RxLaser Invoice Print - Catalog version.
  CHANGES :
    rx01 02/03/99 jbt; Upgrade for VSIFax Gold.
    rx02 04/26/99 mm;  Additional VSIFax Gold changes. Removed call to faxcom.p
         as it is no longer needed.
    rx03 05/10/99 mm;  Additional VSIFAX Gold changes.
    rx04 08/31/99 mm;  Override check for hard copy flag if device from screen
         header is a fax. Was causing 2nd copy of fax to print w/o overlay.
*******************************************************************************/
/* p-faxc2.i 1.2 04/09/98 */
/*h*****************************************************************************
  INCLUDE      : p-faxc2.i
  DESCRIPTION  : Cover Page for Fax
  USED ONCE?   : no (bpepbf1.p, oeepaf1.p, oeepif1.p, p-ares1.i)
  AUTHOR       : enp
  DATE WRITTEN : 11/27/92
  CHANGES MADE :
    11/27/92 enp; TB# 8889 Add VisiFax Capability
    07/19/93 pjt; TB# 9599 Getting "sasp record not available" message
    08/20/93 enp; TB# 12648 VSIFAX from F10 prints do not compress
    09/07/93 rs;  TB# 12932 Trend Doc Mgr for Rel 5.2
    10/20/93 enp; TB# 13098 Allow an 'l' in 10 digit phone numbe
    11/09/94 rs;  TB# 17051 Transfer Info To Notification Msg
    09/15/95 dww; TB# 19314 Remove Imbedded spaces from the Fax Phone Number
    04/07/98 cm;  TB# 24793 Wrong page size used for fax output
*******************************************************************************/

/*e Parameters
   &extent = extent from sasc (1=PO, 2=OEEPA 3=OEEPI 4=ARES 5=BPEPB 6=BPEPV
   &file   = file name
*******************************************************************************/

/*d Faxbox */
if avail sasp then do:

    if not (sasp.pcommand begins "fx" or               /*rx01*/
            sasp.pcommand begins "vfx") then do:       /*rx01*/

        output through value(sasp.pcommand) paged.

        /*tb 19314 09/15/95 dww; Remove Imbedded Space from Fax Phone Number */
        {fixphone.gca {&file}faxphoneno s-faxphoneno {&comvar}}

        assign
            v-faxstring = "^h" + v-wide +
                          (if sasc.oifaxpreno = "1" and
                              substring(s-faxphoneno,1,1) = "1" then ""
                           else sasc.oifaxpreno) +
                          s-faxphoneno + sasc.oifaxsufno + "."
            v-faxto[1]  = {&file}name
            v-faxto[2]  = if sasc.oifaxattn[{&extent}] then
                              /{&com}*/
                                if "{&file}" = "v-" then v-apmgr
                                else
                              /{&com}*/
                              arsc.apmgr
                          else {&file}pocontctnm.

        put unformatted v-faxstring.

        if sasc.oifg begins "j" then put unformatted "^" + sasc.oifg.

        display sasc.name v-faxto v-title s-faxphoneno v-from
        with frame f-fax.

        page.
    end.

    else do:
        /*d Vsifax */

        /*tb 19314 09/15/95 dww; Remove Imbedded Space from Fax Phone Number */
        {fixphone.gca {&file}faxphoneno s-faxphoneno "/*"}

        /*si begin*/
        s-faxphoneno = if sapb.faxphoneno ne ""              and
                          sapb.faxphoneno ne s-faxphoneno    and
                          sapb.faxphoneno ne "NOFAXNUMBER"   and
                          sapb.faxphoneno ne "NO FAX NUMBER" and
                          sapb.faxphoneno ne "N0FAXNUMBER"   and
                          sapb.faxphoneno ne "N0 FAX NUMBER"
                        then
                           sapb.faxphoneno
                        else
                           s-faxphoneno.
        /*si end*/

        /*tb 17051 11/09/94 rs; Transfer Info to Notification Msg */
        /*d tag1 & tag2 will be set for custom Transmit Notify Prog */
        assign
            v-faxto[1]   = if sapb.faxto1 <> "" then sapb.faxto1    /*rx03*/
                           else {&file}name
            v-faxto[2]   = if sapb.faxto2 <> "" then sapb.faxto2    /*rx03*/
                           else if sasc.oifaxattn[{&extent}] then
                               /{&com}*/
                               if "{&file}" = "v-" then v-apmgr
                               else
                               /{&com}*/
                               arsc.apmgr
                            else {&file}pocontctnm.
            /*si begin*/
            /* Comment out original v-faxcmd build...
            v-faxcmd     = " -n" +
                           (if sasc.oifaxpreno = "1" and
                               substring(s-faxphoneno,1,1) = "1" then ""
                           else sasc.oifaxpreno) +
                           s-faxphoneno + sasc.oifaxsufno +
                           " -t TOCO=" +
                           "~"" + v-faxto[1] + "~"" +
                           " -t TONAME=" +
                           "~"" + v-faxto[2] + "~"" +
                           " -t FXNAME=Accounting"  +
                           " -t tag1=" +
                           "~"" + v-faxto[1] + "~"" +
                           " -t tag2=" +
                           "~"" + {&tag} + "~"".
            */

        /* Replace with this build, which emulates vsifax.gpr */
        if sasp.pcommand begins "fx" then                       /*rx01*/
            /*d standard VSIFAX */
            assign
            v-faxcmd     = " -n" +
                           (if sasc.oifaxpreno = "1" and
                               substring(s-faxphoneno,1,1) = "1"
                            then ""
                            else sasc.oifaxpreno) +
                           s-faxphoneno +
                           sasc.oifaxsufno +
                           /*rx03 begin */
                           " -t TOCO=" + "~"" + v-faxto[1] + "~"" +
                           " -t TONAME=" + "~"" + v-faxto[2] + "~"" +
                           " -t TONUM=" + "~"" + sapb.faxphoneno + "~"" +
                           " -t FXNAME=" + "~"" +
                           (if sapb.faxfrom <>  ""
                            then sapb.faxfrom
                            else "Accounting") + "~"" +
                           /*rx03 end*/
                           " -t tag1=" + "~"" + v-faxto[1] + "~"" +
                           " -t tag2=" + "~"" + {&tag} + "~""
            /*rx02 begin - build the command line comment */
            v-faxcmd = v-faxcmd +
                       (if sapb.faxcom[1] ne ""
                        then " -t _NOTE60x2=" + "~"" + sapb.faxcom[1] + "~""
                        else "") +
                       (if sapb.faxcom[2] ne ""
                        then (" -t _NOTE60_2=" + "~"" + sapb.faxcom[2] + "~"")
                        else "") +
                       (if sapb.faxcom[3] ne ""
                        then (" -t _NOTE60_3=" + "~"" + sapb.faxcom[3] + "~"")
                        else "") +
                       (if sapb.faxcom[4] ne ""
                        then (" -t _NOTE60_4=" + "~"" + sapb.faxcom[4] + "~"")
                        else "") +
                       (if sapb.faxcom[5] ne ""
                        then (" -t _NOTE60_5=" + "~"" + sapb.faxcom[5] + "~"")
                        else "") +
                       (if sapb.faxcom[6] ne ""
                        then (" -t _NOTE60_6=" + "~"" + sapb.faxcom[6] + "~"")
                        else "") +
                       (if sapb.faxcom[7] ne ""
                        then (" -t _NOTE60_7=" + "~"" + sapb.faxcom[7] + "~"")
                        else "") +
                       (if sapb.faxcom[8] ne ""
                        then (" -t _NOTE60_8=" + "~"" + sapb.faxcom[8] + "~"")
                        else "") +
                       (if sapb.faxcom[9] ne ""
                        then (" -t _NOTE60_9=" + "~"" + sapb.faxcom[9] + "~"")
                        else "") +
                       (if sapb.faxcom[10] ne ""
                        then (" -t _NOTE60_10=" + "~"" + sapb.faxcom[10] + "~"")
                        else "").
            /*rx02 end*/

        /*rx01 rx02 begin */
        else do:
            /*d VSIFAX Gold */

            /*rx03 begin */
            if (sapb.faxcom[1] <> "" or sapb.faxcom[2] <> "" or
                sapb.faxcom[3] <> "" or sapb.faxcom[4] <> "" or
                sapb.faxcom[5] <> "" or sapb.faxcom[6] <> "" or
                sapb.faxcom[7] <> "" or sapb.faxcom[8] <> "" or
                sapb.faxcom[9] <> "" or sapb.faxcom[10] <> "")
            then do:

                /*d memo file frame */
                form
                    sapb.faxcom[1]      at 1
                    sapb.faxcom[2]      at 1
                    sapb.faxcom[3]      at 1
                    sapb.faxcom[4]      at 1
                    sapb.faxcom[5]      at 1
                    sapb.faxcom[6]      at 1
                    sapb.faxcom[7]      at 1
                    sapb.faxcom[8]      at 1
                    sapb.faxcom[9]      at 1
                    sapb.faxcom[10]     at 1
                with frame f-memo no-box no-labels no-underline.

                /*d create temporary file for memo lines */
                nameloop:  do while true:
                    v-memofile = v-rxfaxdir + string(time) +
                                 string(random(1,999)).
                    if search(v-memofile) = ? then leave nameloop.
                end.

                output to value(v-memofile) no-echo.

                display
                    sapb.faxcom[1]
                    sapb.faxcom[2]
                    sapb.faxcom[3]
                    sapb.faxcom[4]
                    sapb.faxcom[5]
                    sapb.faxcom[6]
                    sapb.faxcom[7]
                    sapb.faxcom[8]
                    sapb.faxcom[9]
                    sapb.faxcom[10]
                with frame f-memo.

                output close.

            end.
            /*rx03 end*/

            assign
            v-faxcmd     = " -n" +
                           (if sasc.oifaxpreno = "1" and
                               substring(s-faxphoneno,1,1) = "1"
                            then ""
                            else sasc.oifaxpreno) +
                           s-faxphoneno +
                           sasc.oifaxsufno +
                           /*rx03 begin*/
                           " -t tco=" + "~"" + v-faxto[1] + "~"" +
                           " -t tnm=" + "~"" + v-faxto[2] + "~"" +
                           " -t fnm=" + "~"" +
                           (if sapb.faxfrom <> ""
                            then sapb.faxfrom
                            else "Accounting") + "~"" +
                           " -t tg1=" + "~"" + v-faxto[1] + "~"" +
                           " -t tg2=" + "~"" + {&tag} + "~"" +
                           (if (sapb.faxcom[1] <> "" or sapb.faxcom[2] <> "" or
                                sapb.faxcom[3] <> "" or sapb.faxcom[4] <> "" or
                                sapb.faxcom[5] <> "" or sapb.faxcom[6] <> "" or
                                sapb.faxcom[7] <> "" or sapb.faxcom[8] <> "" or
                                sapb.faxcom[9] <> "" or sapb.faxcom[10] <> "")
                            then " -t ntf=" + "~"" + v-memofile + "~""
                            else "").
                           /*rx03 end*/

        end.
        /*rx01 rx02 end */

        /* Build the command line comment parms*/
/*      run faxcom.p(input-output v-faxcmd,recid(sapb)).              rx02*/

        /*tb 17051 11/09/94 rs; Transfer Info to Notification Msg rearranged
            logic to void going oversize */
        find sassr where recid(sassr) = v-sassrid no-lock no-error.

         /* Print to normal fax device or to RxLaser file
           dependent on g-ourproc */
        /* NOTE: the prog list should only include those docs
           which are RxLaser/VSIFax*/

        /*rx01 begin */
        if avail sassr then do:
            if can-do("oeepa,oeepi,oerd,oeet,ares",g-ourproc) then do:
                if v-reportno = 0 then do:
                    output to value(v-rxfax) paged page-size
                                                     value(sassr.pglength).
                end.
                else if v-reportno ne 0 then do:
                    output to value(v-rxfax) paged page-size
                                        value(sassr.xpglength[v-reportno]).
                end.
                v-rxfaxcmd = v-rxfaxcmd + v-faxcmd.
            end.  /* valid vsifax function */
            else do:
                if v-reportno = 0 then do:
                    output through value(sasp.pcommand) value(v-faxcmd) paged
                    page-size value(sassr.pglength).
                end.
                else if v-reportno ne 0 then do:
                    output through value(sasp.pcommand) value(v-faxcmd) paged
                    page-size value(sassr.xpglength[v-reportno]).
                end.
            end.
        end.  /* if avail sassr */
        else output through value(sasp.pcommand) value(v-faxcmd) paged.
        /*rx01 end */

        if avail sassr and
           ((v-reportno eq 0 and sassr.wide = yes) or
            (v-reportno ne 0 and sassr.xwide[v-reportno] = yes))
        then do:
            v-faxcmd = chr(15).
            put control v-faxcmd.
        end.
    end.
end.

/*d Set Flag for printing or archiving */
{&out} = if sasc.oifaxhardfl[{&extent}]
             and not (sasp.pcommand begins "fx" or              /*rx04*/
                 sasp.pcommand begins "vfx")                    /*rx04*/
             then "p"
         else if v-coldfl then "x"
         else {&out}.

