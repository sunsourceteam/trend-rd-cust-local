&if defined(user_temptable) = 2 &then
/* x-varxq.i */
/*
  DESCRIPTION: This program creates a custom quote report fo VA repair 
               quotes and orders created from quotes entered thru vaexq.
  11/28/2006  TA
  MODIFICATIONS:
    1/6/11 TAH 004 - Introduce shop supplies and freight charge along with
                     software controlled percent adder for flexability.
*/
{zsapboydef.i}    
{zsapboycheck.i} 
def var p-regval  as char  no-undo.    
  
{g-vastg2.i}
/* TAH004 1/6/11 */
def new shared var s-shopsupplies  as dec format ">,>>>,>>9.99-"       no-undo.
def new shared var s-shopsuppliesmult as dec                           no-undo.
def var            v-minadder  as dec                                  no-undo.
def buffer f-vaspsl for vaspsl.
def buffer btt-icsp for icsp.
def buffer zl-notes for notes.
define buffer x-vaspsl for vaspsl.
define buffer x-vasps  for vasps.
/* zsap switches */
def var sw-s as logical no-undo.
def var sw-b as logical no-undo.
def var sw-c as logical no-undo.
def var sw-p as logical no-undo.
def var sw-t as logical no-undo.
/* def Ranges */
def var rQuote    as char extent 2 no-undo.    
def var rWhse     as char extent 2 no-undo.    
def var rCust     as dec extent 2 no-undo.    
def var rShipTo   as char extent 2 no-undo.   
def var rVend     as dec extent 2 no-undo.   
def var rVendId   as char extent 2 no-undo.   
def var rRegion   as char extent 2 no-undo.   
def var rDistrict as char extent 2 no-undo.   
def var rProd   as char extent 2 no-undo.   
def var rEnterDt as date extent 2 no-undo.  
def var rStage  as char format "xxx" extent 2 no-undo.   
def var rStageI as int               extent 2 no-undo.   
def var v-stginx as int no-undo.
def var rSlsIn  as char extent 2 no-undo.   
def var rSlsOut as char extent 2 no-undo.   
def var rTaken  as char extent 2 no-undo.   
def var rCancelDt as date extent 2 no-undo. 
def var rLostBus  as char extent 2 no-undo. 
/* def Options */
def var oSort as char no-undo.
def var oDetail as char no-undo.
def var oList   as logical no-undo.
def var oNotes  as char no-undo.
def var oConv   as logical no-undo.
def var oCancelled as logical no-undo.
def var oAuditClsStage as logical no-undo.
def var oLostBusiness as logical no-undo.
def var lenSort as integer no-undo.
def var iter    as integer no-undo.
def var sortfield as char extent 4 no-undo.
def var salescode as char no-undo.
def var insidecode as char no-undo.
def var CurrSlsrep as char no-undo.
def var InSlsrep as char no-undo.
def var slssort as char no-undo.
def var w-vendno like icsw.arpvendno.
/* notes */
def var showNotes     as char no-undo.  
def var showNoteType  as char no-undo.   
def var printNoteln   as char no-undo.  
Define buffer catmaster for notes.
Define var v-vendorid   as c format "x(24)" no-undo.
Define var v-parentcode as c format "x(24)" no-undo.
Define var v-technology as c format "x(24)" no-undo.
/* user3 vars */
def var v-shipto  like oeeh.shipto no-undo.
def var v-contact as char no-undo.
def var v-phone   as char format "(999)999-9999" no-undo.
def var v-email   as char no-undo.
def var v-operid  as char no-undo. 
def var totCost   as dec  no-undo.
def var v-rebFlag as log  no-undo.
def var totMargin as dec  no-undo.
def var lostbuscode as char no-undo.
def var lastvasp  as recid no-undo.
def var dataRegion as char no-undo.
def var dataDistrict as char no-undo.
def var dataCustName as char no-undo.
def var lostbusreason as char no-undo.
def var q-pistons   as l                  no-undo.
def var q-blocks    as l                  no-undo.
def var q-other     as l                  no-undo.
def var qty-pistons as int format "zz9"   no-undo. 
def var qty-blocks  as int format "zz9"   no-undo. 
def var qty-other   as int format "zz9"   no-undo. 
def var q-otherdesc as char format "x(30)" no-undo. 
def var v-esttst-h as int format "99"     no-undo.
def var v-esttst-m as int format "99"     no-undo.
def var v-estasy-h as int format "99"     no-undo.
def var v-estasy-m as int format "99"     no-undo.
def var v-esttdn-h as int format "99"     no-undo.
def var v-esttdn-m as int format "99"     no-undo.
def var t-csttdn-h as dec                 no-undo.
def var t-csttdn-m as dec                 no-undo.
def var t-cstasy-h as dec                 no-undo.
def var t-cstasy-m as dec                 no-undo.
def var t-csttst-h as dec                 no-undo.
def var t-csttst-m as dec                 no-undo.
def var v-comment  as char format "x"     no-undo. 
def var v-prod     as char format "x(24)" no-undo. 
def var v-stage    as char format "xxx"   no-undo. 
def var v-custno like oeeh.custno format ">>>>>9999999" no-undo.  
def var v-margin as dec no-undo.
def var v-enterdt as date.  
def var v-quoteno as char format "x(13)"  no-undo.
def var v-error   as char format "x(78)"  no-undo.
def var v-leadtm   as char format "x(4)"  no-undo.
def var v-vendnm   as char format "x(24)" no-undo.
def var v-operinits like g-operinits      no-undo.
def var audit-vendno like apsv.vendno     no-undo.
def var non-genuine as log                no-undo.
def var v-serial   as char format "x(20)" no-undo.
def var v-override as char format "x"     no-undo.
def var v-descrip1 as char format "x(24)" no-undo.
def var v-descrip2 as char format "x(24)" no-undo.
def var v-vendno   like apsv.vendno       no-undo.
def var v-partno   as char format "x(30)" no-undo.
def var h-transtype as char format "x(2)" no-undo.
def var x-quoteno as char format "x(13)" no-undo.
def var specnsl as char no-undo.
def var sumPrice  as dec no-undo.
def var sumCost   as dec  no-undo.
def var HPrice  as dec no-undo.
def var HCost   as dec  no-undo.
def var v-timeprc like oeelb.price  no-undo. 
def var linePrice like oeelb.price    no-undo.
def var lineCost  like oeelb.prodcost no-undo.
def var lineMarg  like oeelb.prodcost no-undo.
def var v-margimprov like oeelb.prodcost no-undo.
def var lineqty   as char format "x(10)" no-undo. 
def var gtotPrice like oeelb.price no-undo.
def var gtotCost  like oeelb.prodcost  no-undo.
def var subtPrice like oeelb.price extent 5 no-undo.
def var subtCost  like oeelb.prodcost  extent 5 no-undo.
def var stPrice   like oeelb.price no-undo.
def var stCost    like oeelb.prodcost no-undo.
def var mrgPerc   as decimal format "99999.9999" no-undo.
def var cntLines  as int extent 5 no-undo.
def var cntOrders as int extent 5 no-undo.
def var cntConvert as int no-undo.
def var cntCancel  as int no-undo.
def var cntOrder   as int no-undo.
def var cntLine    as int no-undo.
def var cntConvertOrders as int extent 5 no-undo.
def var cntCancelOrders  as int extent 5 no-undo.
def var unasgndtFlag as logical no-undo initial yes.
do iter = 1 to 5:
  subtPrice[iter] = 0.
  subtCost[iter] = 0.
  cntLines[iter] = 0.
  cntOrders[iter] = 0.
  cntConvertOrders[iter] = 0.
  cntCancelOrders[iter]  = 0.
end.
def temp-table repTable
  field quoteno as char
  field stage   as char format "xxx"
  field vaspid as recid
  field vaspslid as recid
  field sortfld  as int 
  field sortfld2 as int
  field sortfld3 as char
  field sortfld4 as char
  field salescd as char
  field region  as char format "x"
  field district as char format "x(3)"
  field takenby  as char format "x(4)"
  field Custno   like arsc.custno format "zzzzzz999999"
  field slsrepin like smsn.slsrep
  field srepinnm like arsc.name 
  field srepinph as char format "(999)999-9999" 
  field slsrepout   like smsn.slsrep
  field slsrepinst  like smsn.slsrep
  field slsrepoutst like smsn.slsrep
  field lostbusinessst as char format "x(2)"
  field lostbusinesscd as char format "x(2)"
  field whse           as char format "x(4)"
  field enterdt   like oeeh.enterdt
  field senterdt  as char format "x(10)"  /* tah 10/26/09 */
  field canceldt  like oeeh.enterdt
  field custname as char
  field margmult as dec format "z99.99"
  field marggain as dec 
  field sumPrice as dec
  field sumCost  as dec
  field QSumPrice as dec
  field QSumCost  as dec
  field CvtSumPrice as dec
  field CvtSumCost  as dec
  field CanSumPrice as dec
  field CanSumCost  as dec
  field OpnSumPrice as dec
  field OpnSumCost  as dec
  field cntConvert as int 
  field cntCancel  as int 
  field cntOrder   as int 
  field cntLine    as int 
  field transtype  like oeeh.transtype
  field specnstype as char format "x"
  
  index k-repTable sortfld
                   sortfld2
                   quoteno 
                   vaspid
                   vaspslid
  index k-repTable2 quoteno 
  index k-repTable3 quoteno vaspid 
                    sortfld
                    sortfld2.
              /*    lostbusinessst 
                    vaspslid.   */
def var v-slsrepoutsort as logical no-undo.
def var v-slsrepinsort  as logical no-undo.
def var v-lostbusiness  as logical no-undo.
def buffer b-repTable for repTable. 
def buffer h-repTable for repTable. 
 
 
&endif
 
&if defined(user_drpttable) = 2 &then
/* Use to add fields to the report table */
    FIELD rptrecid        AS recid   
 
 
&endif
 
&if defined(user_forms) = 2 &then
form 
  "Non-Genuine Parts?            Quantity?" at 1 
  q-pistons            at 1
  "Pistons y/n?"       at 5 
  qty-pistons          at 36 
  q-blocks             at 1
  "Blocks y/n?"        at 5 
  qty-blocks           at 36 
  q-other              at 1
  q-otherdesc          at 5 
  qty-other            at 36 
  skip(1)
  with frame f-genparts no-box no-labels col 5 width 60.
form
  "EnterDt"       at 1
  "Stg"           at 10
  "TknBy"         at 19
  "Cust#"         at 33
  "Name"          at 39
  "RepairNo"      at 56
  "InSlsNo"       at 67
  "InSlsRepName"  at 76
  "InSlsPhone#"   at 100
/*  "Email"       at 108 */
  "TotalSales"    at 125
  "TotalCost"     at 142
  "Mgn% "         at 154
  "CancelDt"      at 1
  "Lost Bus"      at 10
  "Ship To"       at 31
  "Order#/Type"   at 54
  "Repair PartNo" at 72
  "SerialNo"      at 104
  "override = *"  at 126
  "--------"      at 1   
  "------"        at 10  
  "----"          at 19  
  "----------"    at 28  
  "---------------"          at 39  
  "------------"             at 55  
  "----"                     at 70
  "-----------------------"  at 76  
  "--------------"           at 100  
/*   "---------"                at 108   */
  "--------------"           at 121
  "--------------"           at 137
  "-------"                  at 152
with frame f-header no-labels 
     stream-io no-box no-underline width 178 page-top.  
             
form header
  "Seq/"            at 3
  "Line#"           at 7
  "Product"         at 15
  " "               at 38
  "Qty"             at 49
  "Price"           at 57
  "Cost"            at 70
  "Mgn%"            at 77
  "LstMaintDt"      at 82
  "Oper/Cstr"       at 93
  "Icsp Description" 
                    at 103
  "Entered Description"  
                    at 124
  "LeadTm"          at 146
/*  "VendNo"        at 130  */
  "Vendor Name"     at 158 
with frame f-lineheader no-labels
     stream-io no-box no-underline width 178 page-top.     
form             
  specnsl          at 2   format "x(1)"
  vaspsl.seqno     at 4   format "zz9" 
  vaspsl.lineno    at 8   format ">>9"  
  v-prod           at 12  format "x(24)"
  lineqty          at 43  format "x(08)"    
  linePrice        at 52  format ">>>,>>>.99-"    
  lineCost         at 64  format ">>>,>>>.99-"    
  v-margin         at 75  format ">>9.99-"      
  v-enterdt        at 84  format "99/99/99"
  v-operid         at 93  format "x(4)"
  v-descrip1       at 98 format "x(24)" 
  v-descrip2       at 123 format "x(24)"  
  v-leadtm         at 148 format "x(4)"
  v-comment        at 153 format "x"
  v-vendnm         at 154 format "x(24)"
with frame f-linedtl no-labels 
     stream-io no-box no-underline width 200.
form             
  "--> "                      at 89   
  v-operinits                 at 93
  audit-vendno                at 98 format ">>>>>>999999"
  v-vendnm                    at 152 format "x(24)"
 with frame f-Auditdtl no-labels 
     no-box no-underline width 200.
form
  skip(1)
  repTable.enterdt    at 1
  repTable.stage      at 10
  repTable.takenby    at 19
  v-custno            at 28   format ">>>>>>>>>9"
  repTable.custname   at 39   format "x(15)"
  v-quoteno           at 55   format "x(13)"
  reptable.slsrepin   at 70   format "x(4)"
  reptable.srepinnm   at 76   format "x(23)"
  reptable.srepinph   at 100  format "(999) 999-9999"
  repTable.sumPrice   at 120  format ">>>,>>>,>>>.99-"
  v-override          at 135  format "x"
  repTable.sumCost    at 136  format ">>>,>>>,>>>.99-"
  v-margin            at 152  format ">>9.99-"
  vasp.user9          at 1    format "99/99/99" 
  lostbuscode         at 10   format "x(3)"
  v-shipto            at 34   format "x(8)"
  vasp.user2          at 53   format "x(10)"
  repTable.transtype  at 65   format "x(2)"
  v-partno            at 72   format "x(30)"
  v-serial            at 104  format "x(15)"  
  "============="     at 121
  "=============="    at 137
  "======="           at 152
  with frame f-vasp no-labels /* stream-io */ 
     no-box no-underline width 178 down.
form
  showNotes     at 5  format "x(6)"
  showNoteType  at 13 format "x(3)"
  printNoteln   at 17 format "x(80)"
with frame f-notes no-labels 
     stream-io no-box no-underline width 178 down.
form
  sortfield[1]  at 1   format "x(10)"
  sortfield[2]  at 12  format "x(10)"
  sortfield[3]  at 22  format "x(10)"
  sortfield[4]  at 32  format "x(10)"
  cntConvert    at 45  format "->>>9" label "Conv:"
  cntCancel     at 55  format "->>>9" label "Canc:"
  cntOrder      at 65  format "->>>9" label "Ord:"
  cntLine       at 75  format "->>,>>9" label "Lines:"
  stPrice       at 126 format "->>>,>>>,>>>.99"   
  stCost        at 142 format "->>>,>>>,>>>.99"   
  mrgPerc       at 158 format "->>9.99"   
with frame f-subtotal no-labels 
     stream-io no-box no-underline width 178 down.
form
  cntConvertOrders[5]   at 1   format "->>>,>>>,>>9" label "Total Converted:"
  cntCancelOrders[5]    at 20  format "->>>,>>>,>>9" label "Total Cancelled:"
  cntOrders[5]          at 40  format "->>>,>>>,>>9" label "Total Orders:"
  cntLines[5]           at 60  format "->>>,>>>,>>9" label "Total Lines:"
  gtotPrice             at 126 format "->>>,>>>,>>9.99" label "TotalSales:"   
  gtotCost              at 142 format "->>>,>>>,>>9.99" label "TotalCost:"  
  mrgPerc               at 158 format "->>9.99"         label "Margin:"
with frame f-gtotal 
     stream-io no-box no-underline width 178.  
/*
form
  v-final                at 1
  v-astrik               at 2   format "x(5)"
  v-summary-lit2         at 7   format "x(27)"
  v-amt7   at 35  format ">,>>>,>>9" label "Tot Orders"
  v-amt6   at 49  format ">,>>>,>>9" label "Tot Lines"
  v-amt3   at 63  format ">,>>>,>>9" label "Tot Conv Ord."
  v-amt4   at 74  format ">,>>>,>>9" label "Tot Can Ord. "
  v-amt1   at 126 format ">>>,>>>,>>9.99-"  label "TotalSales"  
  v-amt2   at 142 format ">>>,>>>,>>9.99-"  label "TotalCost"  
  v-amt5   at 158 format ">>9.99-"          label "Margin%"
  "~015"   at 178
with frame f-tot width 178
 no-box no-labels.  
*/
form
  "   Orders"             at 40
  "    Lines"             at 50
  " Cnvt Ord"             at 60
  "  Can Ord"             at 70
  "     TotalSales"       at 120
  "      TotalCost"       at 136
  "  Mgn% "               at 152
  "---------"             at 40
  "---------"             at 50
  "---------"             at 60
  "---------"             at 70
  "---------------"       at 120
  "---------------"       at 136
  "-------"               at 152
  v-final                at 1
  v-astrik               at 2   format "x(5)"
  v-summary-lit2         at 7   format "x(27)"
  v-amt7    at 40  format ">,>>>,>>9"           no-label 
  v-amt6    at 50  format ">,>>>,>>9"           no-label
  v-amt3    at 60  format ">,>>>,>>9"           no-label
  v-amt4    at 70  format ">,>>>,>>9"           no-label 
  "Quoted   :"   at 109
  v-amt8    at 120 format ">>>,>>>,>>9.99-"     no-label   
  v-amt9    at 136 format ">>>,>>>,>>9.99-"     no-label   
  v-amt10   at 152 format ">>9.99-"             no-label 
  "Converted:"   at 109
  v-amt11   at 120 format ">>>,>>>,>>9.99-"     no-label   
  v-amt12   at 136 format ">>>,>>>,>>9.99-"     no-label   
  v-amt13   at 152 format ">>9.99-"             no-label 
  "LBusiness:"   at 109
  v-amt14   at 120 format ">>>,>>>,>>9.99-"     no-label   
  v-amt15   at 136 format ">>>,>>>,>>9.99-"     no-label  
  v-amt16   at 152 format ">>9.99-"             no-label 
  "Open     :"   at 109
  v-amt17   at 120 format ">>>,>>>,>>9.99-"     no-label   
  v-amt18   at 136 format ">>>,>>>,>>9.99-"     no-label   
  v-amt19   at 152 format ">>9.99-"             no-label 
  "~015"   at 178
with frame f-tot width 178 no-underline
 no-box no-labels.  
form 
  " "              at 1
  "~015"           at 178
with frame f-skip width 178 no-box no-labels.  
 
 
&endif
 
&if defined(user_extractrun) = 2 &then
/* TAH004 1/6/11 */
 find notes use-index k-notes where                        
      notes.cono         = g-cono and                      
      notes.notestype    = "zz" and                        
      notes.primarykey   = "service shop markup" and      
      notes.secondarykey = " " and         
      notes.pageno       = 1                               
      no-lock no-error.                                       
 if not avail notes then
   assign s-shopsuppliesmult = .02.
 else
   assign s-shopsuppliesmult = 
   (dec(notes.noteln[1])  / 100). 
 find notes use-index k-notes where                        
      notes.cono         = g-cono and                      
      notes.notestype    = "zz" and                        
      notes.primarykey   = "Service Minimum markup" and      
      notes.secondarykey = " " and         
      notes.pageno       = 1                               
      no-lock no-error.                                       
 if not avail notes then
   assign v-minadder = 1.04.
 else
   assign v-minadder = 1 + (dec(notes.noteln[1])  / 100). 
/* TAH004 1/6/11 */ 
 
 run sapb_vars.
 run extract_data.
 
&endif
 
&if defined(user_exportstatDWheaders) = 2 &then
 
 
&endif
 
&if defined(user_exportstatheaders) = 2 &then
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  
  assign export_rec = export_rec + v-del +
                      "Period 1" + v-del +
                      "Period 2" + v-del   +
                      "Period 3" + v-del   +
                      "Period 4" + v-del   +
                      "Period 5" + v-del   +
                      "Period 6 " + v-del   +
                      " " + v-del +
                      " ".
 
 
&endif
 
&if defined(user_foreach) = 2 &then
for each repTable where repTable.vaspslid = 0 no-lock:
 
 
&endif
 
&if defined(user_drptassign) = 2 &then
 /* If you add fields to the dprt table this is where you assign them */
   drpt.rptrecid = recid(repTable)
 
 
&endif
 
&if defined(user_B4endloop) = 2 &then
  /* this is before the end of the for each loop to feed the data BUILDREC */
 
 
&endif
 
&if defined(user_viewheadframes) = 2 &then
/* Since frames are unique to each program you need to put your
   views and hides here with your names */
if not p-exportl then
  do:
  if not x-stream then
    do:
    hide frame f-header.
    end.
  else
   do:
    hide stream xpcd frame f-header.
    end.
  
  end.
if not p-exportl  then
  do:
  if not x-stream then
    do:
    page.
    if oDetail <> "s" then
      view frame f-header.
    end.
  else  
    do:
    page stream xpcd.
    if oDetail > "s" then
      view stream xpcd frame f-header.
    end.
   
  end.
 
 
&endif
 
&if defined(user_drptloop) = 2 &then
 
 
&endif
 
&if defined(user_detailprint) = 2 &then
   /* Comment this out if you are not using detail 'X' option  */
   if p-detail = "d" and oDetail <> "s" and
      entry(u-entitys,drpt.sortmystery,v-delim) <> 
      fill("~~",length(entry(u-entitys,drpt.sortmystery,v-delim))) then 
     do:
     /* this code skips the all other lines for printing detail */
     
     /* next line would be your detail print procedure */
     run DetailPrint (input  drpt.rptrecid ).
     end.
 
 
&endif
 
&if defined(user_finalprint) = 2 &then
assign t-amounts5[u-entitys + 1] =
        (if t-amounts1[u-entitys + 1] = 0 then 0
         else if ((t-amounts1[u-entitys + 1] - t-amounts2[u-entitys + 1]) /
                   t-amounts1[u-entitys + 1]) * 100 < -999 then -999
         else if ((t-amounts1[u-entitys + 1] - t-amounts2[u-entitys + 1]) /
                   t-amounts1[u-entitys + 1]) * 100 > 999 then 999
         else ((t-amounts1[u-entitys + 1] - t-amounts2[u-entitys + 1]) /
                t-amounts1[u-entitys + 1]) * 100).
assign t-amounts10[u-entitys + 1] =
        (if t-amounts8[u-entitys + 1] = 0 then 0
         else if ((t-amounts8[u-entitys + 1] - t-amounts9[u-entitys + 1]) /
                   t-amounts8[u-entitys + 1]) * 100 < -999 then -999
         else if ((t-amounts8[u-entitys + 1] - t-amounts9[u-entitys + 1]) /
                   t-amounts8[u-entitys + 1]) * 100 > 999 then 999
         else ((t-amounts8[u-entitys + 1] - t-amounts9[u-entitys + 1]) /
                t-amounts8[u-entitys + 1]) * 100).
 assign t-amounts13[u-entitys + 1] =
        (if t-amounts11[u-entitys + 1] = 0 then 0
         else if ((t-amounts11[u-entitys + 1] - t-amounts12[u-entitys + 1]) /
                   t-amounts11[u-entitys + 1]) * 100 < -999 then -999
         else if ((t-amounts11[u-entitys + 1] - t-amounts12[u-entitys + 1]) /
                   t-amounts11[u-entitys + 1]) * 100 > 999 then 999
         else ((t-amounts11[u-entitys + 1] - t-amounts12[u-entitys + 1]) /
                t-amounts11[u-entitys + 1]) * 100).
 assign t-amounts16[u-entitys + 1] =
        (if t-amounts14[u-entitys + 1] = 0 then 0
         else if ((t-amounts14[u-entitys + 1] - t-amounts15[u-entitys + 1]) /
                   t-amounts14[u-entitys + 1]) * 100 < -999 then -999
         else if ((t-amounts14[u-entitys + 1] - t-amounts15[u-entitys + 1]) /
                   t-amounts14[u-entitys + 1]) * 100 > 999 then 999
         else ((t-amounts14[u-entitys + 1] - t-amounts15[u-entitys + 1]) /
                t-amounts14[u-entitys + 1]) * 100).
 assign t-amounts17[u-entitys + 1] = 
       (t-amounts8[u-entitys + 1] - t-amounts11[u-entitys + 1]).
 assign t-amounts18[u-entitys + 1] = 
       (t-amounts9[u-entitys + 1] - t-amounts12[u-entitys + 1]).
 assign t-amounts19[u-entitys + 1] =
        (if t-amounts17[u-entitys + 1] = 0 then 0
         else if ((t-amounts17[u-entitys + 1] - t-amounts18[u-entitys + 1]) /
                   t-amounts17[u-entitys + 1]) * 100 < -999 then -999
         else if ((t-amounts17[u-entitys + 1] - t-amounts18[u-entitys + 1]) /
                   t-amounts17[u-entitys + 1]) * 100 > 999 then 999
         else ((t-amounts17[u-entitys + 1] - t-amounts18[u-entitys + 1]) /
                t-amounts17[u-entitys + 1]) * 100).
         
if not p-exportl then do: 
 if not x-stream then 
   do:
   display 
    v-final
    v-astrik
    v-summary-lit2
    t-amounts3[u-entitys + 1]   @ v-amt3
    t-amounts4[u-entitys + 1]   @ v-amt4
    t-amounts6[u-entitys + 1]   @ v-amt6
    t-amounts7[u-entitys + 1]   @ v-amt7
    t-amounts8[u-entitys + 1]   @ v-amt8
    t-amounts9[u-entitys + 1]   @ v-amt9
    t-amounts10[u-entitys + 1]   @ v-amt10
    t-amounts11[u-entitys + 1]   @ v-amt11
    t-amounts12[u-entitys + 1]   @ v-amt12
    t-amounts13[u-entitys + 1]   @ v-amt13
    t-amounts14[u-entitys + 1]   @ v-amt14
    t-amounts15[u-entitys + 1]   @ v-amt15
    t-amounts16[u-entitys + 1]   @ v-amt16
    t-amounts17[u-entitys + 1]   @ v-amt17
    t-amounts18[u-entitys + 1]   @ v-amt18
    t-amounts19[u-entitys + 1]   @ v-amt19
    with frame f-tot.
    down with frame f-tot.
   end.
 else
   do:
   display stream xpcd
    v-final
    v-astrik
    v-summary-lit2
    t-amounts3[u-entitys + 1]   @ v-amt3
    t-amounts4[u-entitys + 1]   @ v-amt4
    t-amounts6[u-entitys + 1]   @ v-amt6
    t-amounts7[u-entitys + 1]   @ v-amt7
    t-amounts8[u-entitys + 1]   @ v-amt8
    t-amounts9[u-entitys + 1]   @ v-amt9
    t-amounts10[u-entitys + 1]   @ v-amt10
    t-amounts11[u-entitys + 1]   @ v-amt11
    t-amounts12[u-entitys + 1]   @ v-amt12
    t-amounts13[u-entitys + 1]   @ v-amt13
    t-amounts14[u-entitys + 1]   @ v-amt14
    t-amounts15[u-entitys + 1]   @ v-amt15
    t-amounts16[u-entitys + 1]   @ v-amt16
    t-amounts17[u-entitys + 1]   @ v-amt17
    t-amounts18[u-entitys + 1]   @ v-amt18
    t-amounts19[u-entitys + 1]   @ v-amt19
    with frame f-tot.
   down stream xpcd with frame f-tot.
   end.
 end.
else
  do:
  assign export_rec =  v-astrik.
  do v-inx4 = 1 to u-entitys:
    if v-totalup[v-inx4] = "n" then
       next.
    if v-inx4 = 1 then
      v-summary-lit2 = "Final".
    else
      v-summary-lit2 = "".
    if v-cells[v-inx4]= "2" then
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec + v-summary-lit2 + v-del + " ".
     
      end.
    else
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec +  v-summary-lit2.
      end.
  end.
  assign export_rec = export_rec + v-del +
      string(t-amounts3[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts4[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts6[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts7[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts8[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts9[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts10[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts11[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts12[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts13[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts14[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts15[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts16[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts17[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts18[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts19[u-entitys + 1],"->>>>>>>>9").
    
  assign export_rec = export_rec + chr(13).    
  put stream expt unformatted export_rec.
  end.
 
 
&endif
 
&if defined(user_summaryframeprint) = 2 &then
    assign t-amounts5[v-inx2] =
       (if t-amounts1[v-inx2] = 0 then 0
        else if ((t-amounts1[v-inx2] - t-amounts2[v-inx2]) /
                  t-amounts1[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts1[v-inx2] - t-amounts2[v-inx2]) /
                  t-amounts1[v-inx2]) * 100 > 999 then 999
        else ((t-amounts1[v-inx2] - t-amounts2[v-inx2]) /
               t-amounts1[v-inx2]) * 100).
    assign t-amounts10[v-inx2] =
       (if t-amounts8[v-inx2] = 0 then 0
        else if ((t-amounts8[v-inx2] - t-amounts9[v-inx2]) /
                  t-amounts8[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts8[v-inx2] - t-amounts9[v-inx2]) /
                  t-amounts8[v-inx2]) * 100 > 999 then 999
        else ((t-amounts8[v-inx2] - t-amounts9[v-inx2]) /
               t-amounts8[v-inx2]) * 100).
               
    assign t-amounts13[v-inx2] =
       (if t-amounts11[v-inx2] = 0 then 0
        else if ((t-amounts11[v-inx2] - t-amounts12[v-inx2]) /
                  t-amounts11[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts11[v-inx2] - t-amounts12[v-inx2]) /
                  t-amounts11[v-inx2]) * 100 > 999 then 999
        else ((t-amounts11[v-inx2] - t-amounts12[v-inx2]) /
               t-amounts11[v-inx2]) * 100).
    assign t-amounts16[v-inx2] =
       (if t-amounts14[v-inx2] = 0 then 0
        else if ((t-amounts14[v-inx2] - t-amounts15[v-inx2]) /
                  t-amounts14[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts14[v-inx2] - t-amounts15[v-inx2]) /
                  t-amounts14[v-inx2]) * 100 > 999 then 999
        else ((t-amounts14[v-inx2] - t-amounts15[v-inx2]) /
               t-amounts14[v-inx2]) * 100).
    assign t-amounts19[v-inx2] =
       (if t-amounts17[v-inx2] = 0 then 0
        else if ((t-amounts17[v-inx2] - t-amounts18[v-inx2]) /
                  t-amounts17[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts17[v-inx2] - t-amounts18[v-inx2]) /
                  t-amounts17[v-inx2]) * 100 > 999 then 999
        else ((t-amounts17[v-inx2] - t-amounts18[v-inx2]) /
               t-amounts17[v-inx2]) * 100).
               
    if not x-stream then
      do:
      display 
        v-final
        v-astrik
        v-summary-lit2
        t-amounts3[v-inx2]   @ v-amt3
        t-amounts4[v-inx2]   @ v-amt4
        t-amounts6[v-inx2]   @ v-amt6
        t-amounts7[v-inx2]   @ v-amt7
        t-amounts8[v-inx2]   @ v-amt8
        t-amounts9[v-inx2]   @ v-amt9
        t-amounts10[v-inx2]   @ v-amt10
        t-amounts11[v-inx2]   @ v-amt11
        t-amounts12[v-inx2]   @ v-amt12
        t-amounts13[v-inx2]   @ v-amt13
        t-amounts14[v-inx2]   @ v-amt14
        t-amounts15[v-inx2]   @ v-amt15
        t-amounts16[v-inx2]   @ v-amt16
        t-amounts17[v-inx2]   @ v-amt17
        t-amounts18[v-inx2]   @ v-amt18
        t-amounts19[v-inx2]   @ v-amt19
        
        with frame f-tot.
      down with frame f-tot.
      end.
    else
      do:
      display stream xpcd
        v-final
        v-astrik
        v-summary-lit2
        t-amounts3[v-inx2]   @ v-amt3
        t-amounts4[v-inx2]   @ v-amt4
        t-amounts6[v-inx2]   @ v-amt6
        t-amounts7[v-inx2]   @ v-amt7
        t-amounts8[v-inx2]   @ v-amt8
        t-amounts9[v-inx2]   @ v-amt9
        t-amounts10[v-inx2]   @ v-amt10
        t-amounts11[v-inx2]   @ v-amt11
        t-amounts12[v-inx2]   @ v-amt12
        t-amounts13[v-inx2]   @ v-amt13
        t-amounts14[v-inx2]   @ v-amt14
        t-amounts15[v-inx2]   @ v-amt15
        t-amounts16[v-inx2]   @ v-amt16
        t-amounts17[v-inx2]   @ v-amt17
        t-amounts18[v-inx2]   @ v-amt18
        t-amounts19[v-inx2]   @ v-amt19
 
        
        with frame f-tot.
      down stream xpcd with frame f-tot.
      end.
 
 
&endif
 
&if defined(user_summaryputexport) = 2 &then
 
    assign export_rec = v-astrik + v-del + export_rec.
    assign export_rec = export_rec + v-del +
      string(t-amounts3[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts4[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts6[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts7[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts8[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts9[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts10[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts11[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts12[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts13[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts14[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts15[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts16[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts17[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts18[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts19[v-inx2],"->>>>>>>>9").
      
 
 
&endif
 
&if defined(user_detailputexport) = 2 &then
 
 
&endif
 
&if defined(user_procedures) = 2 &then
procedure sapb_vars:
/* set Ranges */
rQuote[1]       = if sapb.rangebeg[1] = "" then 
                   "aaaa-0000001"
                  else  
                     sapb.rangebeg[1].
rQuote[2]       = if sapb.rangeend[1] = "" or 
                     sapb.rangeend[1] begins "~~~~~~~~~~~~~~~~" then 
                   "zzzz-9999999"
                  else  
                     sapb.rangeend[1].
rCust[1]        = decimal(sapb.rangebeg[2]). 
rCust[2]        = decimal(sapb.rangeend[2]).
rShipTo[1]      = sapb.rangebeg[3].
rShipTo[2]      = sapb.rangeend[3].
rVend[1]        = decimal(sapb.rangebeg[4]).
rVend[2]        = decimal(sapb.rangeend[4]).
rVendId[1]      = sapb.rangebeg[5].
rVendId[2]      = sapb.rangeend[5].
rProd[1]        = sapb.rangebeg[6].
rProd[2]        = sapb.rangeend[6].
rEnterDt[1]     = date(sapb.rangebeg[7]).
rEnterDt[2]     = date(sapb.rangeend[7]).
rStage[1]       = sapb.rangebeg[8].
rStage[2]       = sapb.rangeend[8].
rTaken[1]       = sapb.rangebeg[9].
rTaken[2]       = sapb.rangeend[9].
rCancelDt[1]    = date(sapb.rangebeg[10]).
rCancelDt[2]    = date(sapb.rangeend[10]).
rLostBus[1]     = sapb.rangebeg[11].
rLostBus[2]     = sapb.rangeend[11].
rWhse[1]        = sapb.rangebeg[12].
rWhse[2]        = sapb.rangeend[12].
assign rStagei[1] = 0
       rStagei[2] = 0.
if rStage[1] = " " then
  assign rStageI[1] = 1.
else  
  do v-stginx = 1 to 16:
    if rStage[1] = v-stage2[v-stginx] then
      assign rStageI[1] = v-stginx.
  end.    
if rStage[2] ge "~~~~~~" then
  assign rStageI[2] = 16.
else  
  do v-stginx = 1 to 16:
    if rStage[2] = v-stage2[v-stginx] then
      assign rStageI[2] = v-stginx.
  end.    
if rStageI[1] = 0 or
   rStageI[2] = 0 then do:
   message "Invalid stages(s) entered in ranges".
   message "Valid stages are: Rcv,Sch,Tdn,Cst,Qtd,Apr,Por,Pin,Srp".
   message "                  Asy,Tst,Pnt,Inf,Can,Cls,Scp".
   return.
end.
if rStageI[1] > rStageI[2] then do:
   message "Begin Stage cannot exceed the to stage".
   message "Valid stages are: Rcv,Sch,Tdn,Cst,Qtd,Apr,Por,Pin,Srp".
   message "                  Asy,Tst,Pnt,Inf,Can,Cls,Scp".
   return.
end.
    
   
   
if num-entries(rQuote[2],"-") = 1 then do: 
 assign rQuote[2] = rQuote[2] + "-9999999".
end. 
else
if num-entries(rQuote[2],"-") = 2 then do: 
 assign rQuote[2] = entry (1,rQuote[2],"-") + "-" +
        string(int(entry(2,rquote[2],"-")),"9999999").
end. 
if num-entries(rQuote[1],"-") = 1 then do: 
 assign rQuote[1] = rQuote[1] + "-0000000".
end. 
else
if num-entries(rQuote[1],"-") = 2 then do: 
 assign rQuote[1] = entry (1,rQuote[1],"-") + "-" +
        string(int(entry(2,rquote[1],"-")),"9999999").
end. 
/** FIX001 - TA  **/
/** entered date **/
if sapb.rangebeg[7] ne "" then do: 
 v-datein = sapb.rangebeg[7].
 {p-rptdt.i}
 if string(v-dateout) = v-lowdt then
  rEnterDt[1] = 01/01/1900.
 else  
  rEnterDt[1] = v-dateout.
end.
else
  rEnterDt[1] = 01/01/1900.
if sapb.rangeend[7] ne "" then do: 
 v-datein = sapb.rangeend[7].
 {p-rptdt.i}
 if string(v-dateout) = v-lowdt then
  rEnterDt[2] = 12/31/2049.
 else  
  rEnterDt[2] = v-dateout.
end.
else
  rEnterDt[2] = 12/31/2049.
/** cancel date **/
if sapb.rangebeg[10] ne "" then do: 
 v-datein = sapb.rangebeg[10].
 {p-rptdt.i}
 if string(v-dateout) = v-lowdt then
  rCancelDt[1] = 01/01/1900.
 else  
  rCancelDt[1] = v-dateout.
end.
else
  rCancelDt[1] = 01/01/1900.
if sapb.rangeend[10] ne "" then do: 
 v-datein = sapb.rangeend[10].
 {p-rptdt.i}
 if string(v-dateout) = v-lowdt then
  rCancelDt[2] = 12/31/2049.
 else  
  rCancelDt[2] = v-dateout.
end.
else
  rCancelDt[2] = 12/31/2049.
/** FIX001 - TA  **/  
if rCancelDt[1] = 12/31/49 and rCancelDt[2] = 12/31/49 then do:
  unasgndtFlag = yes.
  rCancelDt[1] = 01/01/1950.
end.  
else unasgndtFlag = no.
/* set Options */
oSort          = sapb.optvalue[1].    
oDetail        = sapb.optvalue[2].
oList          = if sapb.optvalue[3] = "yes" then yes else no. 
oNotes         = sapb.optvalue[4].    
oConv          = if sapb.optvalue[5] = "yes" then yes else no. 
oCancelled     = if sapb.optvalue[6] = "yes" then yes else no. 
oAuditClsStage = if sapb.optvalue[7] = "yes" then yes else no. 
p-detail = /* if oDetail = "s" then
             "s"
           else */
             "d". 
oLostBusiness = if sapb.optvalue[8] = "yes" then yes else no. 
lenSort = length(oSort).
if oList then do:
  /* stuff for zsapboycheck.i */
  /* do I have to set zel_ too? */
  assign 
  zzl_begwhse = rWhse[1]
  zzl_endwhse = rWhse[2]
  zzl_begcust = rCust[1]
  zzl_endcust = rCust[2]
  zzl_begvend = rVend[1]
  zzl_endvend = rVend[2]
  zzl_begrep  = rSlsOut[1]
  zzl_endrep  = rSlsOut[2] 
  zzl_begcat  = rProd[1]
  zzl_endcat  = rProd[2]
  zzl_begvname  = rVendId[1]
  zzl_endvname  = rVendId[2]
  zzl_begshipto = rShipTo[1]
  zzl_endshipto = rShipTo[2].
  assign    
  zel_begwhse = rWhse[1]
  zel_endwhse = rWhse[2]
  zel_begcust = rCust[1]     
  zel_endcust = rCust[2]     
  zel_begvend = rVend[1]     
  zel_endvend = rVend[2]     
  zel_begrep  = rSlsOut[1]   
  zel_endrep  = rSlsOut[2]   
  zel_begcat  = rProd[1]     
  zel_endcat  = rProd[2]     
  zel_begvname  = rVendId[1] 
  zel_endvname  = rVendId[2] 
  zel_begshipto = rShipTo[1] 
  zel_endshipto = rShipTo[2].
  
  {zsapboyload.i}                                 
  assign                             
   rWhse[1]    = zel_begwhse
   rWhse[2]    = zel_endwhse
   rCust[1]    = zel_begcust
   rCust[2]    = zel_endcust
   rVend[1]    = zel_begvend
   rVend[2]    = zel_endvend
   rSlsOut[1]  = zel_begrep
   rSlsOut[2]  = zel_endrep
   rProd[1]    = zel_begcat
   rProd[2]    = zel_endcat
   rVendId[1]    = zel_begvname
   rVendId[2]    = zel_endvname
   rShipTo[1]    = zel_begshipto
   rShipTo[2]    = zel_endshipto. 
   assign
         zelection_matrix [2] = (if zelection_matrix[2] > 1 then  /* Cust */
                                  zelection_matrix[2]
                                 else   
                                  1)
         zelection_matrix [1] = (if zelection_matrix[1] > 1 then  /* Rep */
                                  zelection_matrix[1]
                                 else   
                                  1)
          zelection_matrix [7] = (if zelection_matrix[7] > 1 then  /* Vend */
                                  zelection_matrix[7]
                                 else   
                                  1)
         zelection_matrix [6] = (if zelection_matrix[6] > 1 then  /* Whse */
                                  zelection_matrix[6]
                                 else   
                                  1)
         zelection_matrix [3] = (if zelection_matrix[3] > 1 then  /* Cat  */
                                 zelection_matrix[3]
                              else   
                                 1)
         zelection_matrix [8] = (if zelection_matrix[8] > 1 then  /* VName */
                                 zelection_matrix[8]
                              else   
                                 1)
         zelection_matrix [9] = (if zelection_matrix[9] > 1 then  /* VPArent */
                                 zelection_matrix[9]
                              else   
                                 1)
         zelection_matrix [11] = (if zelection_matrix[11] > 1 then /* Product */
                                 zelection_matrix[11]
                              else   
                                 1)
  
         zelection_matrix [12] = (if zelection_matrix[12] > 1 then /* shippto */
                                  zelection_matrix[12]
                                 else   
                                  1).
                                 
 
end. /* if oList */  
    
 run formatoptions.
end.
/* ------------------------------------------------------------------------- */
procedure formatoptions:
/* ------------------------------------------------------------------------- */
   
/** FIX001 - TA  **/  
  assign p-portrait = false.
  /* if false then the report will print in LANDSCAPE 178 character */
  
  if sapb.optvalue[1] > "0" and sapb.optvalue[1] ne "99" then do:
    p-optiontype = " ".
    find notes where notes.cono = g-cono and
                     notes.notestype = "zz" and
                     notes.primarykey = "varxq" and  /* tbxr program */
                     notes.secondarykey = 
                     sapb.optvalue[1]  no-lock no-error.
    if not avail notes then
      do:
      display "Format is not valid cannot process request".
      assign p-optiontype = "c"
             p-sorttype   = ">,"
             p-totaltype  = "S"
             p-summcounts = "a".
      end.               
    else do:
      assign p-optiontype = notes.noteln[1]
             p-sorttype   = notes.noteln[2]
             p-totaltype  = notes.noteln[3]
             p-summcounts = notes.noteln[4]
             p-export     = "    "
             p-register   = notes.noteln[5]
             p-registerex = notes.noteln[6].
      end.
  end.     
  else
  if sapb.optvalue[1] = "99" then 
    do:
    assign p-register   = ""
           p-registerex = "".
    run reportopts(input sapb.user5, 
                   input-output p-optiontype,
                   input-output p-sorttype,  
                   input-output p-totaltype,
                   input-output p-summcounts,
                   input-output p-register,
                   input-output p-regval).
    end.
 if p-optiontype = "" then
   assign p-optiontype = "c"
          p-sorttype   = ">,"
          p-totaltype  = "S"
          p-summcounts = "a".
   
 run Print_reportopts (input recid (sapb),
                       input g-cono).
end.
procedure extract_data:
sumPrice = 0.
sumCost  = 0.
        
/* Main, select 1 ranges */
for each zsdivasp use-index k-custno where  
  zsdivasp.cono = g-cono and
  zsdivasp.custno >= rCust[1]      and 
  zsdivasp.custno <= rCust[2]      and
  zsdivasp.repairno >= entry (1,rquote[1],"-") + "-" +
        string(int(entry(2,rquote[1],"-")),"9999999")   and 
  zsdivasp.repairno <= entry (1,rquote[2],"-") + "-" +
        string(int(entry(2,rquote[2],"-")),"9999999")   and 
  zsdivasp.shipto  >= rShipTo[1]  and 
  zsdivasp.shipto  <= rShipTo[2]  and   
  zsdivasp.vendno  >= rVend[1]    and 
  zsdivasp.vendno  <= rVend[2]    and 
  zsdivasp.enterdt >= rEnterDt[1] and 
  zsdivasp.enterdt <= rEnterDt[2] and
  zsdivasp.takenby >= rTaken[1]   and
  zsdivasp.takenby <= rTaken[2]   and
  zsdivasp.rectype = " "
  no-lock:
 find vasp where vasp.cono = g-cono and 
      vasp.shipprod = zsdivasp.repairno
      no-lock no-error.
 if not avail vasp then next. 
assign cntConvert = 0
       cntCancel  = 0
       cntOrder   = 0
       cntLine    = 0.
if int(vasp.user7) = 0 then do:
  next.
end.
else
  assign v-stage      = v-stage2[int(vasp.user7)].
if int(vasp.user7) >= rStageI[1] and int(vasp.user7) <= rStageI[2] then 
 v-stage = v-stage.
else   
 next.
/* Converted orders */
  if oConv and vasp.user2 = "" then
   next.
  /* Cancelled only */
  if oCancelled and vasp.user7 ne 14 then
   next.
  if oList then do:
    zelection_type = "c".              
    zelection_char4 = "". 
    zelection_cust = dec(string(vasp.user5,"999999999999")).                
    run zelectioncheck.             
    if not zelection_good then next.
  end.
  find first arsc where arsc.cono = g-cono and
             arsc.custno = zsdivasp.custno 
       no-lock no-error.
 
  if avail arsc then dataCustName = arsc.name.
  else dataCustName = "".
   
  if avail zsdivasp and not oList and
    (zsdivasp.custno < rCust[1] or zsdivasp.custno > rCust[2]) then 
   next.       
  
  if oList then do:
    zelection_type = "w".              
    zelection_char4 = zsdivasp.whse. 
    zelection_cust = 0.                
    run zelectioncheck.             
    if not zelection_good then next.
  end.
  if avail zsdivasp and not oList and 
    (zsdivasp.whse < rwhse[1] or zsdivasp.whse > rwhse[2]) then 
   next.
  
  assign h-transtype = " ".
  assign v-margimprov = 0.
  if vasp.user2 ne "" then do:
     assign cntConvert = cntConvert + 1.   
     find first vaeh where
                vaeh.cono = g-cono and
                vaeh.vano = int(substring(vasp.user2,1,7)) and       
                vaeh.user2 = vasp.shipprod
                no-lock no-error.
     if avail vaeh and avail vasp and vasp.user7 ne 14 then 
       assign h-transtype = vaeh.transtype.
  end.
  if vasp.user7 = 14 then
    assign cntCancel = cntCancel + 1.
  
  assign sumPrice = 0
         sumCost  = 0.
  
  for each x-vasps where
           x-vasps.cono     = g-cono    and 
           x-vasps.shipprod = vasp.shipprod  
           no-lock,
     each x-vaspsl use-index k-vaspsl where
          x-vaspsl.cono     = g-cono        and 
          x-vaspsl.shipprod = x-vasps.shipprod and
          x-vaspsl.seqno    = x-vasps.seqno  
          no-lock:
    assign v-margimprov = v-margimprov + x-vaspsl.xxde1. 
  end.
  
  /** not used per Kevin N.
  assign v-margimprov = if avail vaeh then 
                         round(vaeh.user6,2)
                        else 
                        if avail vasp then 
                         dec(substring(vasp.user1,160,10))
                        else 
                         0.
  if avail vaeh then do: 
   find first vaesl where 
        vaesl.cono   = g-cono                         and 
        vaesl.vano   = int(substring(vasp.user2,1,7)) and 
        vaesl.seqno  = 3                              and 
        vaesl.lineno = 1                              and 
        vaesl.shipprod = "Margin Improvement"
        no-lock no-error.
   if avail vaesl then do:  
    assign v-margimprov = vaesl.qtyord.   
    release vaesl. 
   end. 
   release vaeh.
  end. 
  *****/
  if avail vaeh then
   release vaeh. 
  find first arsc where arsc.cono = g-cono and
             arsc.custno = dec(string(vasp.user5,"999999999999")) 
       no-lock no-error.
  if avail arsc then do: 
   assign dataCustName = arsc.name
          Inslsrep     = arsc.slsrepin
          currslsrep   = arsc.slsrepout. 
   find first smsn where smsn.cono = g-cono and  
              smsn.slsrep = inslsrep
        no-lock no-error. 
   if avail smsn then 
    assign v-contact = smsn.name            
           v-email   = trim(smsn.email) 
           v-phone   = smsn.phoneno.
  end.           
  else 
   assign dataCustName = "" 
          Inslsrep     = ""
          currslsrep   = ""
          v-contact    = ""
          v-phone      = ""
          v-email      = "". 
  for each vaspsl use-index k-vaspsl 
     where vaspsl.cono = g-cono            and
           vaspsl.shipprod = vasp.shipprod  
           no-lock:
  
    if oList then do:                   
      zelection_type = "p".             
      zelection_char4 = vaspsl.prodcat.
      zelection_cust = 0.               
      run zelectioncheck.               
      if not zelection_good then next.  
    end. 
    
    assign w-vendno     = 0
           v-vendorid   =  " "
           v-technology =  " "
           v-parentcode =  " ".
    
    find icsp where icsp.cono = g-cono and 
                    icsp.prod = vaspsl.compprod no-lock no-error.
    find icsw where icsw.cono = g-cono and 
                    icsw.whse = substring(vasp.shipprod,1,4) and 
                    icsw.prod = vaspsl.compprod no-lock no-error.
    assign w-vendno = vaspsl.arpvendno.
                    /* fix for auditing  */
                      (if avail icsw then
                        icsw.arpvendno
                       else
                        vaspsl.arpvendno).
     
    if avail icsp then do: 
      find catmaster where catmaster.cono = 1 and 
           catmaster.notestype    = "zz" and
           catmaster.primarykey   = "apsva" and
           catmaster.secondarykey = icsp.prodcat
      no-lock no-error. 
      if not avail catmaster then do:
       v-parentcode = "".
       v-vendorid = substring(icsp.prodcat,1,3).
      end.
      else
       assign v-vendorid   = catmaster.noteln[1].
    end.
    if (v-vendorid < rVendid[1] or v-vendorid > rVendid[2]) or
       (w-vendno < rVend[1] or w-vendno > rVend[2]) then  
       next.
    if oList then do: 
     zelection_type = "n".
     zelection_char4 = v-vendorid.
     zelection_cust = 0.
     run zelectioncheck.
     if zelection_good = false then
       next.
     assign zelection_type = "v"
            zelection_vend = w-vendno
            zelection_char4 = " ".
     run zelectioncheck.
     if zelection_good = false then
      next.
    end. 
    /** Line item detail info  **/  
    assign cntLine     = cntLine + 1.
    create repTable.    
    repTable.margmult  = dec(substring(vasp.user1,5,6)).             
    repTable.marggain  = if vaspsl.compprod = "margin improvement" then
                          v-margimprov
                         else 
                          vaspsl.xxde1.
    repTable.stage     = v-stage2[int(vasp.user7)].
    repTable.sortfld   = vaspsl.seqno. 
    repTable.sortfld2  = vaspsl.lineno. 
    repTable.slsrepout = currslsrep. 
    repTable.slsrepin  = inslsrep.  
    repTable.srepinnm  = v-contact.
    repTable.srepinph  = v-phone. 
    repTable.takenby   = zsdivasp.takenby. 
    repTable.enterdt   = zsdivasp.enterdt.
    repTable.senterdt  = string(year(zsdivasp.enterdt),"9999") + "/" +
                         string(month(zsdivasp.enterdt),"99") + "/" +
                         string(day(zsdivasp.enterdt),"99").
    repTable.canceldt  = if avail vasp and vasp.user9 ne ? then 
                          vasp.user9 
                         else 
                          ?.
    repTable.custno    = dec(string(vasp.user5,"999999999999")).
    repTable.vaspid    = recid(vasp). 
    repTable.quoteno   = vaspsl.shipprod.
    repTable.vaspslid  = recid(vaspsl).
    repTable.region    = dataRegion.
    repTable.district  = dataDistrict.
    repTable.custname  = dataCustName.
    repTable.whse      = zsdivasp.whse.
    repTable.cntConvert = 0.
    repTable.cntCancel  = 0.
    repTable.cntOrder   = 0.
    repTable.cntLine    = 0.
    repTable.lostbusinesscd = if vaspsl.nonstockty = "l" then 
                                "can"
                              else 
                                "  ".             
    repTable.lostbusinessst = if v-lostbusiness then
                                if vaspsl.nonstockty = "l" then 
                                  "can"              
                                else
                                  "  "
                              else
                                  " ".
    repTable.slsrepinst = if v-slsrepinsort then 
                             vaspsl.operinit 
                          else
                               "     ".       
    repTable.slsrepoutst = if v-slsrepoutsort then 
                             vaspsl.operinit 
                          else
                              "    ".   
    
    /** FIX001 - TA  **/  
    repTable.specnstype = if vaspsl.nonstockty = "l" then 
                        "l" 
                       else
                        " ".           
    assign  t-csttdn-h = 0
            t-csttdn-m = 0 
            t-cstasy-h = 0
            t-cstasy-m = 0 
            t-csttst-h = 0
            t-csttst-m = 0.
    if vaspsl.compprod = "teardown" then   
     assign v-esttdn-h = dec(substring(vasp.user4,50,2))
            v-esttdn-m = dec(substring(vasp.user4,52,2))
            t-csttdn-h =  (v-esttdn-h * vaspsl.prodcost)  
            t-csttdn-m = ((v-esttdn-m * vaspsl.prodcost) / 60).    
    else          
    if vaspsl.compprod = "service repair" then do: 
     assign v-estasy-h = dec(substring(vasp.user4,54,2))
            v-estasy-m = dec(substring(vasp.user4,56,2))
            t-cstasy-h =  (v-estasy-h * vaspsl.prodcost)  
            t-cstasy-m = ((v-estasy-m * vaspsl.prodcost) / 60).    
    end.
    else 
    if vaspsl.compprod = "test time" then 
     assign v-esttst-h = dec(substring(vasp.user4,58,2))
            v-esttst-m = dec(substring(vasp.user4,60,2))
            t-csttst-h =  (v-esttst-h * vaspsl.prodcost)  
            t-csttst-m = ((v-esttst-m * vaspsl.prodcost) / 60). 
      
    /* TAH004 1/6/11 */  
   if vaspsl.compprod = "Shop Supplies" and
      can-find(first f-vaspsl where
                     f-vaspsl.cono     = g-cono and 
                     f-vaspsl.shipprod = vaspsl.shipprod and 
                     f-vaspsl.compprod = "shop supplies" and  
                     f-vaspsl.seqno = 3 no-lock) then 
     
     assign s-shopsupplies = round(
                              (round(dec(substring(vasp.user1,150,10)),2) + 
                               round(dec(substring(vasp.user1,160,10)),2))
                               * s-shopsuppliesmult,2).
   else
    assign s-shopsupplies = 0. 
    /* TAH004 1/6/11 */  
 
    
    sumPrice = if (vaspsl.nonstockty = "l" and vasp.user7 <> 14) or 
                   vasp.user7 = 14 then 
                0
               else  
               if vaspsl.compprod = "margin improvement" then 
                v-margimprov
               else
               if vaspsl.compprod = "shop supplies" then 
                s-shopsupplies
               else  
               if vaspsl.compprod = "teardown" then                    
                (t-csttdn-h + t-csttdn-m)  
               else  
               if vaspsl.compprod = "service repair" then  
                (t-cstasy-h + t-cstasy-m) 
               else  
               if vaspsl.compprod = "test time" then  
                (t-csttst-h + t-csttst-m) 
               else 
                vaspsl.prodcost * vaspsl.qtyneeded. 
    
    
/*     TAH004 1/6/11 */
    find btt-icsp where 
         btt-icsp.cono = 1 and
         btt-icsp.prod = vasp.refer no-lock no-error.
                         
    find zl-notes use-index k-notes where                        
         zl-notes.cono         = g-cono and                      
         zl-notes.notestype    = "zz" and                        
         zl-notes.primarykey   = "standard cost markup" and      
         zl-notes.secondarykey = (if avail btt-icsp then
                                     btt-icsp.prodcat 
                                  else
                                    "#(notvalid)#") and         
         zl-notes.pageno       = 1                               
         no-lock no-error.                                       
    if avail zl-notes then do:                    
      assign sumPrice =
         round((sumPrice * 
               max((1 + (dec(zl-notes.noteln[1])  / 100)),v-minadder) ) / 
               ((100 - dec(substring(vasp.user1,5,6))) / 100),2).
    end.    
    else
    if dec(substring(vasp.user1,5,6)) > 0 then 
        assign sumPrice =
          round((sumPrice * v-minadder ) / 
               ((100 - dec(substring(vasp.user1,5,6))) / 100),2).
    else
      assign sumPrice =
         round(sumPrice *  v-minadder,2).
/* TAH004 1/6/11   
    
    if dec(substring(vasp.user1,5,6)) > 0 then 
     assign sumPrice =
           (sumPrice * 1.04) / ((100 - dec(substring(vasp.user1,5,6))) / 100).
    else 
     assign sumPrice = sumPrice * 1.04.
   TAH004 1/6/11  */
    
    sumCost  = if (vaspsl.nonstockty = "l" and vasp.user7 <> 14) or 
                   vasp.user7 = 14 then 
                0
               else  
               if vaspsl.compprod = "margin improvement" then 
                v-margimprov
               else
               if vaspsl.compprod = "shop supplies" then 
                s-shopsupplies
               else  
               if vaspsl.compprod = "teardown" and 
                 (t-csttdn-h + t-csttdn-m) > 0 then 
                 (t-csttdn-h + t-csttdn-m) 
               else  
               if vaspsl.compprod = "service repair" and   
                 (t-cstasy-h + t-cstasy-m) > 0 then
                 (t-cstasy-h + t-cstasy-m)   
               else  
               if vaspsl.compprod = "test time" and 
                 (t-csttst-h + t-csttst-m) > 0 then
                 (t-csttst-h + t-csttst-m)   
               else 
               if vaspsl.compprod = "teardown" or  
                  vaspsl.compprod = "service repair" or   
                  vaspsl.compprod = "test time" then 
                 0
               else
                 vaspsl.prodcost * vaspsl.qtyneeded.  
    if avail zl-notes then do:                    
      assign sumCost =
         (sumCost * 
             max((1 + (dec(zl-notes.noteln[1])  / 100)),v-minadder) ). 
    end.    
    else
       assign sumCost = sumCost *  v-minadder.
   
    
    assign /* sumCost = sumCost * 1.04       */
           sumCost = if vaspsl.seqno = 5 then 
                      sumcost * -1
                     else 
                      sumcost.
    HPrice = if vasp.user7 = 14 then 
                0
             else  
             if vaspsl.nonstockty = "l" then 
              vaspsl.user6 * vaspsl.qtyneeded 
             else   
             if vaspsl.compprod = "margin improvement" then  
              v-margimprov 
             else
             if vaspsl.compprod = "shop supplies" then 
               s-shopsupplies
             else  
             if vaspsl.compprod = "teardown" then  
              (t-csttdn-h + t-csttdn-m)  
             else  
             if vaspsl.compprod = "service repair" then  
               (t-cstasy-h + t-cstasy-m) 
             else  
             if vaspsl.compprod = "test time" then  
              (t-csttst-h + t-csttst-m)  
             else 
              vaspsl.prodcost * vaspsl.qtyneeded. 
    if avail zl-notes then do:                    
      assign HPrice =
         round((HPrice *
                max((1 + (dec(zl-notes.noteln[1])  / 100)),v-minadder) ) / 
                ((100 - dec(substring(vasp.user1,5,6))) / 100),2).
    end.    
    else
    if dec(substring(vasp.user1,5,6)) > 0 then 
      assign HPrice =
         round((HPrice * v-minadder ) / 
                ((100 - dec(substring(vasp.user1,5,6))) / 100),2).
    else    
       assign HPrice = round(HPrice *  v-minadder,2).
/* TAH004 1/6/11   
    
    if dec(substring(vasp.user1,5,6)) > 0 then 
     assign HPrice = 
           (HPrice * 1.04) / (100 - dec(substring(vasp.user1,5,6))) * 100.
    else 
     assign HPrice = HPrice * 1.04. 
*/
    HCost  = if vasp.user7 = 14 then 
              0
             else 
             if vaspsl.nonstockty = "l" then 
              vaspsl.user6 * vaspsl.qtyneeded 
             else   
             if vaspsl.compprod = "margin improvement" then 
                v-margimprov  
             else
             if vaspsl.compprod = "shop supplies" then 
               s-shopsupplies
             else  
             if vaspsl.compprod = "teardown" and 
               (t-csttdn-h + t-csttdn-m) > 0 then 
               (t-csttdn-h + t-csttdn-m) 
             else  
             if vaspsl.compprod = "service repair" and   
               (t-cstasy-h + t-cstasy-m) > 0 then
               (t-cstasy-h + t-cstasy-m)   
             else  
             if vaspsl.compprod = "test time" and 
               (t-csttst-h + t-csttst-m) > 0 then
               (t-csttst-h + t-csttst-m)   
             else 
             if vaspsl.compprod = "teardown" or  
                vaspsl.compprod = "service repair" or   
                vaspsl.compprod = "test time" then 
               0
             else                
               vaspsl.prodcost * vaspsl.qtyneeded.
/* TAH004 1/6/11    */
 
    if avail zl-notes then do:                    
      assign HCost =
         (HCost *
            max((1 + (dec(zl-notes.noteln[1])  / 100)),v-minadder) ).  
    end.    
    else
       assign Hcost = HCost *  v-minadder.
/* TAH004 1/6/11    */
 
    
    assign /* HCost = HCost * 1.04 */
           HCost = if vaspsl.seqno = 5 then 
                      HCost * -1
                     else 
                      HCost.
    find b-repTable use-index k-repTable2
                where b-repTable.quoteno    = vasp.shipprod
                  and b-repTable.slsrepinst = (if v-slsrepinsort then 
                                                 vaspsl.operinit
                                               else
                                                 "    ")
                  and b-repTable.slsrepoutst = (if v-slsrepoutsort then 
                                                  vaspsl.operinit
                                                else
                                                  "    ") 
                  and b-repTable.lostbusinessst = 
                                            (if v-lostbusiness then 
                                              if vaspsl.nonstockty = "l" then  
                                               "can"
                                              else
                                               "  "
                                             else
                                              "  ") 
                  and b-repTable.vaspslid     = 0     
                  no-lock no-error.    
                                                            
    if avail b-repTable then do: 
      assign b-repTable.margmult    = dec(substring(vasp.user1,5,6))
             b-repTable.cntLine     = b-repTable.cntLine + 1
             b-repTable.sumPrice    = b-repTable.sumPrice + sumPrice           
             b-repTable.sumCost     = b-repTable.sumCost + sumCost
             b-repTable.QsumPrice   = if vaspsl.nonstockty ne "l" then 
                                       b-repTable.QsumPrice + hPrice           
                                      else 
                                       b-repTable.QsumPrice
             b-repTable.QsumCost    = if vaspsl.nonstockty ne "l" then 
                                       b-repTable.QsumCost + hCost
                                      else 
                                       b-repTable.QsumCost
             b-repTable.CvtsumPrice = b-repTable.CvtsumPrice +
                                      (if vasp.user2 <> "" and 
                                        vaspsl.nonstockty ne "l" then 
                                         hPrice
                                       else
                                        0)           
             b-repTable.CvtsumCost  = b-repTable.CvtsumCost +
                                     (if vasp.user2 <> "" and 
                                       vaspsl.nonstockty ne "l" then 
                                         hCost
                                       else
                                         0)           
             b-repTable.OpnSumPrice = b-repTable.OpnSumPrice +
                                     (if vasp.user7 <> 14 and 
                                       vaspsl.nonstockty ne "l" then 
                                       hPrice
                                      else
                                       0)           
             b-repTable.OpnSumCost  = b-repTable.OpnSumCost +
                                     (if vasp.user7 <> 14 and 
                                       vaspsl.nonstockty ne "l" then 
                                       hCost
                                      else
                                       0)           
             b-repTable.CansumPrice = b-repTable.CansumPrice +
                                      (if  /* vasp.user7 = 14 or  */
                                        vaspsl.nonstockty = "l" then
                                         hPrice
                                       else
                                         0)           
             b-repTable.CansumCost  = b-repTable.CansumCost +
                                      (if /* vasp.user7 = 14 or */
                                        vaspsl.nonstockty = "l" then 
                                         hCost
                                       else
                                          0).           
    end.
    else do:
    
      assign cntOrder   = 1.
 
  /* Header with totals */
      create b-repTable.                 
      b-repTable.margmult   = dec(substring(vasp.user1,5,6)).             
      b-repTable.marggain   = if vaspsl.compprod = "margin improvement" then
                               v-margimprov
                              else 
                               vaspsl.xxde1.
      b-repTable.stage      = v-stage2[int(vasp.user7)].
      b-repTable.quoteno    = vaspsl.shipprod. 
      b-repTable.sortfld    = 0. 
      b-repTable.sortfld2   = 0. 
      b-repTable.vaspid     = recid(vasp). 
      b-repTable.vaspslid   = 0.            
      b-repTable.whse       = zsdivasp.whse.
      b-repTable.transtype  = h-transtype.
      b-repTable.custname   = dataCustName.
      b-repTable.slsrepout  = currslsrep. 
      b-repTable.slsrepin   = inslsrep.  
      b-repTable.srepinnm   = v-contact.
      b-repTable.srepinph   = v-phone. 
      b-repTable.takenby    = zsdivasp.takenby. 
      b-repTable.slsrepinst = if v-slsrepinsort then 
                              vaspsl.operinit
                              else
                               " ".                                            
      b-repTable.slsrepoutst = if v-slsrepoutsort then 
                                vaspsl.operinit
                               else
                                " ".   
      
      b-repTable.lostbusinesscd = if vaspsl.nonstockty = "l" then 
                                    "can" 
                                  else
                                     "  ".             
      b-repTable.lostbusinessst = if v-lostbusiness then
                                    if vaspsl.nonstockty = "l" then 
                                      "can"              
                                    else
                                       " "
                                  else
                                     "  ".
      b-repTable.enterdt     = zsdivasp.enterdt.
      b-repTable.senterdt    = string(year(zsdivasp.enterdt),"9999") + "/" + 
                               string(month(zsdivasp.enterdt),"99") +  "/" +
                               string(day(zsdivasp.enterdt),"99").
      b-repTable.canceldt    = if avail vasp and vasp.user9 ne ? then 
                                vasp.user9 
                               else 
                                ?.
      b-repTable.custno      = dec(string(vasp.user5,"999999999999")).    
      b-repTable.cntConvert  = cntConvert.
      b-repTable.cntCancel   = cntCancel.
      b-repTable.cntOrder    = CntOrder.
      b-repTable.cntLine     = 1.
      b-repTable.sumPrice    = sumPrice.
      b-repTable.sumCost     = sumCost.
      b-repTable.QsumPrice   = hPrice.           
      b-repTable.QsumCost    = hCost.
      b-repTable.CvtsumPrice = if vasp.user2 ne "" then                    
                                hPrice
                               else
                                0.           
      b-repTable.CvtsumCost  = if vasp.user2 ne "" then 
                                 hCost
                               else
                                 0.           
      b-repTable.OpnSumPrice = if vasp.user7 <> 14 then 
                                 hPrice
                               else
                                 0.           
      b-repTable.OpnSumCost  = if vasp.user7 <> 14 then   
                                 hCost
                               else
                                 0.           
      
      b-repTable.CansumPrice = if /* vasp.user7 = 14 or */
                                vaspsl.nonstockty = "l" then 
                                 hPrice
                               else                      
                                 0.           
      b-repTable.CansumCost  = if /* vasp.user7 = 14 or */
                                vaspsl.nonstockty = "l" then 
                                 hCost
                               else
                                 0.           
    end.
  end. /* vaspsl */   
  assign repTable.sumPrice = dec(substring(vasp.user1,180,10))
       b-repTable.sumPrice = dec(substring(vasp.user1,180,10)). 
end. /* vasp loop */  
end.
  
Procedure DetailPrint:
  define input parameter x-recid as recid no-undo.
  
  find first repTable use-index k-repTable where
       recid(repTable) = x-recid no-lock no-error.
  find first vasp     where recid(vasp) = repTable.vaspid no-lock no-error.
  find first zsdivasp where
             zsdivasp.cono = g-cono             and 
             zsdivasp.repairno = vasp.shipprod  and 
             zsdivasp.whse = entry(1,vasp.shipprod,"-")
             no-lock no-error.
   
  if num-entries(vasp.shipprod,"-") ge 2 then do: 
    assign v-quoteno = entry (1,vasp.shipprod,"-") + "-" +
            string(int(entry(2,vasp.shipprod,"-")),"9999999").
    assign v-quoteno = entry(1,v-quoteno,"-") + "-" + 
             left-trim(entry(2,v-quoteno,"-"),"0").
  end.
  else
    next.
  
  
  assign x-quoteno = v-quoteno 
         v-quoteno = v-quoteno + "q".
  assign v-custno     = dec(string(vasp.user5,"999999999999"))
         v-margimprov = dec(substring(vasp.user1,160,10)).
 
  if repTable.sumPrice ne 0 then do:
    v-margin = dec(substring(vasp.user1,5,6)).
  end.
  else 
    v-margin = 0.
/*  lostbuscode = substring(oeehb.user3,53,2).*/
  lostbusreason = "".
  if trim(lostbuscode) ne "" then do:
    find first sasta where sasta.cono = g-cono and
    sasta.codeiden = "e" and sasta.codeval = lostbuscode
    no-lock no-error.
    if avail sasta then lostbusreason = sasta.descr.
    release sasta.
  end.  
  /* show header */
  if oDetail ne "s" then do:
    if repTable.vaspslid = 0 then do: 
      assign v-margin   = if v-margin < (999.99 * -1) then 
                           (999.99 * -1)
                          else   
                           v-margin
             v-partno   = vasp.refer
             v-serial   = substring(vasp.user3,50,22)
             v-override = if substring(vasp.user1,250,1) = "y" then 
                           "*" 
                          else 
                           " ".
      if v-override ne "*" then 
       assign repTable.sumPrice =
          round(( round(repTable.sumCost,2) / ((100 - v-margin) / 100)),2).
     
       
      display  
        repTable.enterdt    
        repTable.stage   
        repTable.takenby   
        v-custno     
        repTable.custname
        v-quoteno   
        repTable.slsrepin
        repTable.srepinnm
        repTable.srepinph       
        repTable.sumPrice
        v-override
        repTable.sumCost         
        v-margin 
        vasp.user9   
        v-shipto     
        lostbuscode    
        vasp.user2
        repTable.transtype  
        v-partno
        v-serial
      with frame f-vasp.
      down with frame f-vasp.
    end.
    /* show notes */
    showNotes    = "Notes:".
    showNoteType = "".
    printNoteln  = "".
    
    /** print audit info if option 7 is yes **/  
    if oAuditClsStage = yes then do: 
     find zsdivasp where
          zsdivasp.cono     = g-cono and 
          zsdivasp.repairno = vasp.shipprod
          no-lock no-error.
     if avail zsdivasp and zsdivasp.user1 ne " " then do:      
      assign q-pistons   = if entry(1,zsdivasp.user1,"^") = "yes" then 
                            yes
                           else 
                            no
             qty-pistons = if entry(1,zsdivasp.user1,"^") = "yes" then 
                            int(entry(2,zsdivasp.user1,"^"))
                           else 
                            0
             q-blocks    = if entry(3,zsdivasp.user1,"^") = "yes" then 
                            yes 
                           else 
                            no 
             qty-blocks  = if entry(3,zsdivasp.user1,"^") = "yes" then 
                            int(entry(4,zsdivasp.user1,"^"))
                           else 
                            0
             q-other     = if entry(5,zsdivasp.user1,"^") = "yes" then 
                            yes 
                           else 
                            no
             qty-other   = if entry(5,zsdivasp.user1,"^") = "yes" then 
                            int(entry(6,zsdivasp.user1,"^"))
                           else 
                            0
             q-otherdesc = entry(7,zsdivasp.user1,"^"). 
      display q-pistons qty-pistons q-blocks qty-blocks        
              q-other   qty-other   q-otherdesc with frame f-genparts.        
     end. 
    end. 
    
    if oNotes ne "n" and repTable.vaspslid = 0 then do:
      if oNotes = "S" or oNotes = "B" then do:
        /* show spec and internal shipping notes */
        for each notes where notes.cono = g-cono and                         
          notes.notestype = "in" and notes.primarykey = vasp.shipprod and   
          notes.secondarykey = vasp.whse no-lock by notes.pageno:             
          showNoteType = "(S)".  
          do iter = 1 to 16:
           if trim(notes.noteln[iter]) ne "" then do:
            printNoteln = replace(trim(notes.noteln[iter]),chr(10)," ").
            display showNotes showNoteType printNoteln with frame f-notes.
            down with frame f-notes.
           end.
           showNotes    = "".
           showNoteType = "".  
          end.
          display with frame f-skip.
        end. /* for each */
        for each notes where notes.cono = g-cono and                         
          notes.notestype = "fa" and notes.primarykey = vasp.shipprod and   
          notes.secondarykey = vasp.whse and 
          notes.pageno = 2 no-lock:
          showNoteType = "(I)".  
          do iter = 1 to 16:
           if trim(notes.noteln[iter]) ne "" then do:
            printNoteln = replace(trim(notes.noteln[iter]),chr(10)," ").
            display showNotes showNoteType printNoteln with frame f-notes.
            down with frame f-notes.
           end.
           showNotes    = "".
           showNoteType = "".  
          end.
         display with frame f-skip.
       end. /* for each */ 
      end. /* if "i" */
      if oNotes = "E" or oNotes = "B" then do:
        /* show external customer notes */
        for each notes where notes.cono = g-cono and                    
          notes.notestype = "fa" and notes.primarykey = vasp.shipprod and
          notes.secondarykey = vasp.whse and 
          notes.pageno = 1 no-lock:
          showNoteType = "(E)".                                             
          do iter = 1 to 16:            
            if iter = 1 then 
              display with frame f-skip.
            if trim(notes.noteln[iter]) ne "" then do:                      
              printNoteln = replace(trim(notes.noteln[iter]),chr(10)," ").
              display showNotes showNoteType printNoteln with frame f-notes.
              down with frame f-notes.
            end.                               
            showNotes    = "".
            showNoteType = "". 
          end.                                                              
          display with frame f-skip.
         end.                                                             
      end.
    end.
    /* show line item detail */
    if oDetail ne "s" and oDetail ne "h" then do:
      display with frame f-lineheader.
      for each b-repTable use-index k-repTable3 where 
               b-repTable.vaspid = repTable.vaspid and
               b-repTable.slsrepoutst = repTable.slsrepoutst and
               b-repTable.slsrepinst  = repTable.slsrepinst and
               b-repTable.lostbusinessst  = repTable.lostbusinessst and
               b-repTable.vaspslid <> 0  
               no-lock
               break by b-repTable.sortfld 
                     by b-repTable.sortfld2:
        find first vaspsl where recid(vaspsl) = b-repTable.vaspslid 
          no-lock no-error.
        find first vasp where recid(vasp) = repTable.vaspid
          no-lock no-error.
    /* TAH004 1/6/11 */  
        if vaspsl.compprod = "Shop Supplies" and
           can-find(first f-vaspsl where
                     f-vaspsl.cono     = g-cono and 
                     f-vaspsl.shipprod = vaspsl.shipprod and 
                     f-vaspsl.compprod = "shop supplies" and  
                     f-vaspsl.seqno = 3 no-lock) then 
     
          assign s-shopsupplies = round((
                                 round(dec(substring(vasp.user1,150,10)),2) + 
                                 round(dec(substring(vasp.user1,160,10)),2))
                               * s-shopsuppliesmult),2).
        else
          assign s-shopsupplies = 0. 
    /* TAH004 1/6/11 */  
        if vaspsl.compprod = "margin improvement" then do: 
         assign v-margimprov = 0.
         for each x-vasps where
                  x-vasps.cono     = g-cono    and 
                  x-vasps.shipprod = vasp.shipprod  
                  no-lock,
             each x-vaspsl use-index k-vaspsl where
                  x-vaspsl.cono     = g-cono        and 
                  x-vaspsl.shipprod = x-vasps.shipprod and
                  x-vaspsl.seqno    = x-vasps.seqno  
             no-lock:
          assign v-margimprov = v-margimprov + x-vaspsl.xxde1. 
         end.
        end. /* vaspsl margin improvement  */
        /* 
        if avail vasp and vasp.user2 ne "" then 
          find first vaeh where
                     vaeh.cono  = g-cono and
                     vaeh.vano  = int(substring(vasp.user2,1,7)) and       
                     vaeh.user2 = vasp.shipprod
                     no-lock no-error.
         assign v-margimprov = if avail vaeh then 
                                round(vaeh.user6,2)
                               else 
                               if avail vasp then 
                                dec(substring(vasp.user1,160,10))
                               else 
                                0.
         if avail vaeh then do: 
          find first vaesl where 
               vaesl.cono   = g-cono                         and 
               vaesl.vano   = int(substring(vasp.user2,1,7)) and 
               vaesl.seqno  = 3                              and 
               vaesl.lineno = 1                              and 
               vaesl.shipprod = "Margin Improvement"
               no-lock no-error.
          if avail vaesl then do:  
           assign v-margimprov = vaesl.qtyord.   
           release vaesl. 
          end. 
          release vaeh.
         end. 
        end. /* vaspsl margin improvement  */
        */
        if avail vaeh then
         release vaeh. 
        
        assign t-csttdn-h = 0
               t-csttdn-m = 0 
               t-cstasy-h = 0
               t-cstasy-m = 0 
               t-csttst-h = 0
               t-csttst-m = 0.
        if vaspsl.compprod = "teardown" then   
         assign v-esttdn-h = dec(substring(vasp.user4,50,2))
                v-esttdn-m = dec(substring(vasp.user4,52,2))
                t-csttdn-h =  (v-esttdn-h * vaspsl.prodcost)  
                t-csttdn-m = ((v-esttdn-m * vaspsl.prodcost) / 60).    
        else          
        if vaspsl.compprod = "service repair" then do: 
         assign v-estasy-h = dec(substring(vasp.user4,54,2))
                v-estasy-m = dec(substring(vasp.user4,56,2))
                t-cstasy-h =  (v-estasy-h * vaspsl.prodcost)  
                t-cstasy-m = ((v-estasy-m * vaspsl.prodcost) / 60).    
        end.
        else 
        if vaspsl.compprod = "test time" then 
         assign v-esttst-h = dec(substring(vasp.user4,58,2))
                v-esttst-m = dec(substring(vasp.user4,60,2))
                t-csttst-h =  (v-esttst-h * vaspsl.prodcost)  
                t-csttst-m = ((v-esttst-m * vaspsl.prodcost) / 60). 
        
        if vaspsl.compprod = "" then 
         assign v-prod = vaspsl.proddesc2. 
        else 
         assign v-prod = vaspsl.compprod.
        
        if vaspsl.operinit ne "" then 
         assign currSlsrep = vaspsl.operinit.  
        else 
        if vasp.operinit ne "" then 
         assign currSlsrep = vasp.operinit. 
         
        if dec(substring(vasp.user1,5,6)) ne 0 then
         v-margin =  dec(substring(vasp.user1,5,6)).
        else
         v-margin = 0.
        assign v-operid  = vaspsl.operinit
               v-enterdt = vaspsl.transdt
               v-margin  = if v-margin < (999.99 * -1) then 
                            (999.99 * -1)
                           else   
                            v-margin.
        /* show qty or time sensitive qtys */
        if vaspsl.compprod = "Shop Supplies" and
           vaspsl.seqno = 3 then
         assign lineqty = string(s-shopsupplies,">>>>>.99").
        else  
        if vaspsl.compprod = "margin improvement" then  
         assign lineqty = string(v-margimprov,">>>>>.99").
        else  
        if vaspsl.compprod = "teardown" then 
         assign lineqty = "   " + string(v-esttdn-h,"99") + ":" + 
                                  string(v-esttdn-m,"99"). 
        else  
        if vaspsl.compprod = "service repair" then    
         assign lineqty = "   " + string(v-estasy-h,"99") + ":" + 
                                  string(v-estasy-m,"99").  
        else  
        if vaspsl.compprod = "test time" then  
         assign lineqty = "   " + string(v-esttst-h,"99") + ":" + 
                                  string(v-esttst-m,"99"). 
        else 
         assign lineqty = string(vaspsl.qtyneeded,">>>>>.99"). 
        lineCost = if vaspsl.nonstockty = "l" and vasp.user7 <> 14 then 
                    0
                   else  
                   if vaspsl.compprod = "margin improvement" and
                    v-margimprov > 0 then 
                    1.00
                   else  
                   if vaspsl.compprod = "Shop Supplies" and
                      vaspsl.seqno = 3   and
                      s-shopsupplies > 0 then 
                    1.00
                   else  
                   if vaspsl.compprod = "teardown" and   
                    (t-csttdn-h + t-csttdn-m) > 0 then 
                    vaspsl.prodcost
                   else  
                   if vaspsl.compprod = "service repair" and 
                    (t-cstasy-h + t-cstasy-m) > 0 then 
                    vaspsl.prodcost 
                   else  
                   if vaspsl.compprod = "test time" and   
                    (t-csttst-h + t-csttst-m) > 0 then 
                    vaspsl.prodcost 
                   else 
                   if vaspsl.compprod = "teardown" or  
                      vaspsl.compprod = "service repair" or   
                      vaspsl.compprod = "test time" then 
                    0
                   else                                    
                    vaspsl.prodcost.
        lineCost  = if vaspsl.seqno = 5 then 
                     lineCost * -1
                    else 
                     lineCost.
        linePrice = if vaspsl.nonstockty = "l" and vasp.user7 <> 14 then 
                     0
                    else  
                    if vaspsl.compprod = "margin improvement" then 
                      v-margimprov
                    else 
                    if vaspsl.compprod = "Shop Supplies" then 
                     s-shopsupplies
                    else
                    if vaspsl.compprod = "teardown" then  
                     (t-csttdn-h + t-csttdn-m)  
                    else  
                    if vaspsl.compprod = "service repair" then  
                     (t-cstasy-h + t-cstasy-m) 
                    else  
                    if vaspsl.compprod = "test time" then  
                     (t-csttst-h + t-csttst-m) 
                    else 
                     vaspsl.prodcost * vaspsl.qtyneeded. 
        linePrice  = if vaspsl.seqno = 5 then 
                     linePrice * -1
                    else 
                     linePrice.
        if vaspsl.nonstockty  = "L" then
         specnsl = "l".
        else do:
         specnsl = "".
         if vasp.user7 = 14 then 
          specnsl = "l".
        end.
        if oDetail = "L" then do:
          lostbuscode = "".             
          lostbusreason = "".                                    
          if trim(lostbuscode) ne "" then do:                    
              find first sasta where sasta.cono = g-cono and       
              sasta.codeiden = "e" and sasta.codeval = lostbuscode 
              no-lock no-error.                                    
              if avail sasta then lostbusreason = sasta.descr.     
              release sasta.                                       
          end.                                                   
        end.
 
        assign v-comment = " ".
        find x-vasps where
             x-vasps.cono     = g-cono        and 
             x-vasps.shipprod = vasp.shipprod and  
             x-vasps.seqno    = vaspsl.seqno 
             no-lock no-error. 
        if avail x-vasps and x-vasps.sctntype = "in" then do: 
         assign v-vendno  = vaspsl.arpvendno.
        /** find po comment vendor or info if available **/
         find com use-index k-com where 
              com.cono     = g-cono    and 
              com.comtype  = x-quoteno and 
              com.orderno  = 0         and
              com.ordersuf = 0         and
              com.lineno   = vaspsl.lineno  
              no-lock no-error. 
         if avail com and dec(substring(com.noteln[9],17,12)) > 0 then do: 
          assign v-vendno = dec(substring(com.noteln[9],17,12))
                 v-vendno = dec(string(v-vendno,"999999999999"))
                 v-comment = if vaspsl.compprod ne "margin improvement" then 
                              "!"
                             else 
                              " ".
          find apsv where apsv.cono = g-cono and 
               apsv.vendno = v-vendno 
               no-lock no-error.
         end. 
         else do:
          find apsv where apsv.cono = g-cono and 
               apsv.vendno = v-vendno
               no-lock no-error.
         end.
        end. 
        
        assign v-descrip1 = vaspsl.proddesc
               v-descrip2 = vaspsl.proddesc2
               v-leadtm   = substring(vaspsl.user5,1,4). 
               
        if vaspsl.compprod ne "teardown"           and 
           vaspsl.compprod ne "margin improvement" and 
           vaspsl.compprod ne "service repair"     and 
           vaspsl.compprod ne "test time"          and 
           not (vaspsl.compprod = "shop supplies"  and
                vaspsl.seqno = 3) and
           not (vaspsl.compprod = "freight charge" and
                vaspsl.seqno = 3) and
           vaspsl.compprod ne "" then  
         assign v-vendnm   = if avail apsv then 
                              apsv.name 
                             else 
                              "no name found"
                v-vendno   = if avail apsv then 
                              apsv.vendno
                             else 
                              v-vendno
              audit-vendno = v-vendno. 
        else 
         assign v-vendno = 0 
                v-vendnm = "".
        display specnsl
                vaspsl.seqno 
                vaspsl.lineno
                v-prod  
                lineqty 
                v-operid
                linePrice        
                lineCost     
                v-margin           
                v-enterdt  
                v-descrip1
                v-descrip2 
                v-leadtm  
                v-comment
                v-vendnm  
                with frame f-linedtl.
                down with frame f-linedtl.
      
       if avail vasp and vasp.user7 = 15 
          and int(substring(vasp.user2,1,7)) > 0
          and vaspsl.seqno = 4 and vaspsl.nonstockty ne "l"    
          and oAuditClsStage = yes then do: 
        if avail vasp and int(substring(vasp.user2,1,7)) > 0
           and vaspsl.seqno = 4 then do: 
         find first vaesl where
                    vaesl.cono     = g-cono and
                    vaesl.vano     = int(substring(vasp.user2,1,7)) and       
                    vaesl.vasuf    = 00            and 
                    vaesl.seqno    = vaspsl.seqno  and 
                    vaesl.lineno   = vaspsl.lineno and 
                    vaesl.shipprod = vaspsl.compprod
                    no-lock no-error.
         if avail vaesl and vaesl.arpvendno > 0 then 
          assign audit-vendno = vaesl.arpvendno. 
         else 
          assign audit-vendno = v-vendno.
         if avail vaesl and vaesl.orderalttype = "p" then do: 
          find poel where poel.cono = g-cono and 
               poel.pono   = vaesl.orderaltno and 
               poel.lineno = vaesl.linealtno
               no-lock no-error.
          if avail poel then do:      
           assign audit-vendno = poel.vendno. 
          end.
         end.
         find apsv where apsv.cono = g-cono and 
              apsv.vendno = audit-vendno          
              no-lock no-error.
         if avail apsv then 
          assign v-vendnm = apsv.name. 
         assign v-operinits = " ". 
         find last ztmk use-index k-ztmk-sc2 where
                   ztmk.cono = g-cono    and 
                   ztmk.ordertype = "sc" and 
                   ztmk.user1     = vasp.shipprod and 
                   ztmk.stagecd   = 4
                   no-lock no-error.
         if avail ztmk then 
          assign v-operinits = ztmk.operinit. 
         
        end. /* tied avail vaesl */          
        if (v-vendno > 0 and audit-vendno > 0) and  
           (v-vendno ne audit-vendno) then do: 
         display v-operinits v-vendnm audit-vendno with frame f-auditdtl. 
         down with frame f-auditdtl.           
        end. 
       end. /* closed stage audit - vasp.user7 = 15 */ 
      end. /* for each */
      display " " with frame f-linedtl.
                 down with frame f-linedtl.
      hide frame f-lineheader.
    end. /** if odetail = **/
  end.
  lastvasp = recid(vasp).
  release vasp.
  release vaspsl.
end.
 
 
&endif
 
/* TBXR F6 CodeGen Includes from here down are CodeGen Generated  */ 
&if defined(user_optiontype) = 2 &then
if not
can-do("C,E,S,T,W",substring(p-optiontype,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries C,E,S,T,W "
     substring(p-optiontype,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries C,E,S,T,W "
     substring(p-optiontype,v-inx,1).
&endif
 
&if defined(user_registertype) = 2 &then
if not
can-do("C,E,S,T,W",substring(p-register,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries C,E,S,T,W "
     substring(p-register,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries C,E,S,T,W "
     substring(p-register,v-inx,1).
&endif
 
&if defined(user_exportvarheaders1) = 2 &then
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Cust Name".
  end. 
else  
if v-lookup[v-inx4] = "E" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "EnterDt".
  end. 
else  
if v-lookup[v-inx4] = "S" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "StageCd".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "TakenBy".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Whse".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_exportvarheaders2) = 2 &then
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "E" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "S" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_DWvarheaders1) = 2 &then
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "E" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "EnterDt".
  end. 
else  
if v-lookup[v-inx4] = "S" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "StageCd".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "TakenBy".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Whse".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_loadtokens) = 2 &then
if v-lookup[v-inx] = "C" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.custname,"x(30)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(30)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "E" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.senterdt,"x(12)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(12)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "S" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.stage,"x(3)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(3)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "T" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.Takenby,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "W" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.whse,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "X" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.quoteno,"x(12)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(12)").   
  end.                                              
else                                              
  assign v-token = " ".
&endif
 
&if defined(user_totaladd) = 2 &then
  assign t-amounts[1] = repTable.sumPrice.
  assign t-amounts[2] = repTable.sumCost.
  assign t-amounts[3] = repTable.cntConvert.
  assign t-amounts[4] = repTable.cntCancel.
  assign t-amounts[5] = 0.
  assign t-amounts[6] = repTable.cntLine.
  assign t-amounts[7] = repTable.cntOrder.
  assign t-amounts[8] = repTable.QsumPrice.
  assign t-amounts[9] = repTable.QsumCost.
  assign t-amounts[10] = 0.
  assign t-amounts[11] = repTable.CvtSumPrice.
  assign t-amounts[12] = repTable.CvtSumCost.
  assign t-amounts[13] = 0.
  assign t-amounts[14] = repTable.CanSumPrice.
  assign t-amounts[15] = repTable.CanSumCost.
  assign t-amounts[16] = 0.
  assign t-amounts[17] = repTable.OpnSumPrice.
  assign t-amounts[18] = repTable.Opnsumcost.
  assign t-amounts[19] = 0.
&endif
 
&if defined(user_numbertotals) = 2 &then
  if v-subtotalup[v-inx4] = 1 then
    v-val = t-amounts1[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 2 then
    v-val = t-amounts2[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 3 then
    v-val = t-amounts3[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 4 then
    v-val = t-amounts4[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 5 then
    v-val = 
      {x-varxq-fmt1.i}
  else 
  if v-subtotalup[v-inx4] = 6 then
    v-val = t-amounts6[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 7 then
    v-val = t-amounts7[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 8 then
    v-val = t-amounts8[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 9 then
    v-val = t-amounts9[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 10 then
    v-val = 
      {x-varxq-fmt1.i "8" "9"}
  else 
  if v-subtotalup[v-inx4] = 11 then
    v-val = t-amounts11[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 12 then
    v-val = t-amounts12[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 13 then
    v-val = 
      {x-varxq-fmt1.i "11" "12"}
  else 
  if v-subtotalup[v-inx4] = 14 then
    v-val = t-amounts14[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 15 then
    v-val = t-amounts15[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 16 then
    v-val = 
      {x-varxq-fmt1.i "14" "15"}
  else 
  if v-subtotalup[v-inx4] = 17 then
    v-val = t-amounts17[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 18 then
    v-val = t-amounts18[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 19 then
    v-val = 
      {x-varxq-fmt1.i "17" "18"}
  else 
    v-val = t-amounts1[v-inx4].
  do inz = 1 to 19:
  if inz = 1 then
    v-regval[inz] = t-amounts1[v-inx4].
  else 
  if inz = 2 then
    v-regval[inz] = t-amounts2[v-inx4].
  else 
  if inz = 3 then
    v-regval[inz] = t-amounts3[v-inx4].
  else 
  if inz = 4 then
    v-regval[inz] = t-amounts4[v-inx4].
  else 
  if inz = 5 then
    v-regval[inz] = 
      {x-varxq-fmt1.i}
  else 
  if inz = 6 then
    v-regval[inz] = t-amounts6[v-inx4].
  else 
  if inz = 7 then
    v-regval[inz] = t-amounts7[v-inx4].
  else 
  if inz = 8 then
    v-regval[inz] = t-amounts8[v-inx4].
  else 
  if inz = 9 then
    v-regval[inz] = t-amounts9[v-inx4].
  else 
  if inz = 10 then
    v-regval[inz] = 
      {x-varxq-fmt1.i "8" "9"}
  else 
  if inz = 11 then
    v-regval[inz] = t-amounts11[v-inx4].
  else 
  if inz = 12 then
    v-regval[inz] = t-amounts12[v-inx4].
  else 
  if inz = 13 then
    v-regval[inz] = 
      {x-varxq-fmt1.i "11" "12"}
  else 
  if inz = 14 then
    v-regval[inz] = t-amounts14[v-inx4].
  else 
  if inz = 15 then
    v-regval[inz] = t-amounts15[v-inx4].
  else 
  if inz = 16 then
    v-regval[inz] = 
      {x-varxq-fmt1.i "14" "15"}
  else 
  if inz = 17 then
    v-regval[inz] = t-amounts17[v-inx4].
  else 
  if inz = 18 then
    v-regval[inz] = t-amounts18[v-inx4].
  else 
  if inz = 19 then
    v-regval[inz] = 
      {x-varxq-fmt1.i "17" "18"}
  else 
    assign inz = inz.
  end.
&endif
 
&if defined(user_summaryloadprint) = 2 &then
  if v-lookup[v-inx2] = "C" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "CustName - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "E" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Entered Date - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "S" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "StageCd - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "T" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Taken By - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "W" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Warehouse - " + h-lookup + " " + h-genlit.
    end.
  else
      v-summary-lit = v-lookup[v-inx2].
&endif
 
&if defined(user_summaryloadexport) = 2 &then
  if v-inx4 > v-inx2  then 
    do:
    if v-inx4 > 1 then          
      export_rec = export_rec + v-del. 
    assign export_rec = export_rec +  " ".
    if v-cells[v-inx4] = "2" then
      assign export_rec = export_rec +  v-del + " ".
    end. 
  else 
  if v-lookup[v-inx4] = "C" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(30)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(30)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "E" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(12)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(12)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "S" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(3)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(3)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "T" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "W" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
    do:
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    assign export_rec = export_rec + v-lookup[v-inx4].
    end.
&endif
 
/* TBXR F6 CodeGen End of Includes that are CodeGen Generated  */ 
