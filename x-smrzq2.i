
/{&user_temptable}*/

 {g-secure.i} 

/* si02 begin - variables required for margin calculations */
 {g-margin.i} 

def new shared var v-conv          as de                              no-undo.
def new shared var s-marginamt     as dec format "zzzzz9.99999-"      no-undo.
def new shared var s-marginpct     as dec format "zzz9.99-"           no-undo.

def new shared var v-oerebty       like sasoo.oerebty                 no-undo.
def new shared var v-smcustrebfl   like sasc.smcustrebfl              no-undo.
def new shared var v-smvendrebfl   like sasc.smvendrebfl              no-undo.
def new shared var v-oecostsale     like sasc.oecostsale              no-undo.
def var b-prodcat                   as char format "x(4)"             no-undo.
def var e-prodcat                   as char format "x(4)"             no-undo.
def var b-tech                      as char format "x(4)"             no-undo.
def var e-tech                      as char format "x(4)"             no-undo.
def var v-technology                as char format "x(30)"            no-undo.
def var v-holdcustomer              like arsc.custno                  no-undo.
def var v-custcnt                   as int                            no-undo.
def var x-per1                      as char format "x(8)"             no-undo.
def var x-per2                      as char format "x(8)"             no-undo.



def buffer zz-icsd for icsd.
def buffer catmaster for notes.

{rebnet.gva}
{speccost.gva}

define buffer xz-oeeh for oeeh.
define buffer xz-oeel for oeel.

/** define report input variables  **/
def  var b-whse          like poeh.whse                           no-undo.
def  var e-whse          like poeh.whse                           no-undo.
def  var b-region        like icsd.region                         no-undo.
def  var e-region        like icsd.region                         no-undo.
def  var b-custno        like oeeh.custno                         no-undo.
def  var e-custno        like oeeh.custno                         no-undo.
def  var b-shipwhse      like poeh.whse                           no-undo.
def  var e-shipwhse      like poeh.whse                           no-undo.
def  var b-date          as date                                  no-undo.
def  var e-date          as date                                  no-undo.
def  var p-regval        as c                                     no-undo.
def  var p-prtserv       as dec initial 0                         no-undo.
def  var p-prtcust       as c format "x(1)"                       no-undo.
def  var p-prtwhse       as c format "x(1)"                       no-undo.
def  var p-prtdisp       as c format "x(1)"                       no-undo.
def  var p-prtbo         as logical                               no-undo.
def  var p-prtnonstk     as logical                               no-undo.
def  var p-prtspecial    as logical                               no-undo.
def  var p-prtsubs       as logical                               no-undo.
def  var p-prtdirect     as logical           initial no          no-undo.
def  var p-datefl        as c format "x(1)"                       no-undo.
def  var p-usedate       like oeeh.reqshipdt                      no-undo.
def  var p-prttype       as c format "x(1)"                       no-undo.
def  var p-format        as integer                               no-undo.
def  var s-basefl        as c format "x(1)"                       no-undo.
def  var s-basefl2       as c format "x(1)"                       no-undo.
def  var s-servmsg       as c format "x(50)"                      no-undo.
def  var f-whse          as c format "x(4)"                       no-undo.
def  var v-priorwhse     like oeeh.whse                           no-undo.
def  var b-custregion    as c format "x(1)"                       no-undo.
def  var e-custregion    as c format "x(1)"                       no-undo.
def  var b-district      as c format "x(3)"                       no-undo.
def  var e-district      as c format "x(3)"                       no-undo.
def  var b-group         like smsn.slstype                        no-undo.
def  var e-group         like smsn.slstype                        no-undo.
def  var b-slsrepin      like oeeh.slsrepin                       no-undo.
def  var e-slsrepin      like oeeh.slsrepin                       no-undo.
def  var x-slsrepin      like oeeh.slsrepin                       no-undo.
def  var x-slsrepout     like oeeh.slsrepout                      no-undo.
def  var b-takenby       like oeeh.takenby                        no-undo.
def  var e-takenby       like oeeh.takenby                        no-undo.
def  var b-slsrepout     like oeeh.slsrepout                      no-undo.
def  var e-slsrepout     like oeeh.slsrepout                      no-undo.
def  var s-ontimepct     as dec format "zzz9.99"        initial 0 no-undo.
def  var s-ontimepct2    as dec format "zzz9.99"        initial 0 no-undo.
def  var s-ontimefl      as c format "x(1)"                       no-undo.
def  var s-ontimefl2     as c format "x(1)"                       no-undo.
def  var p-prtdetail     as logical           initial no          no-undo.
def  var z-linerr        as logical           initial no          no-undo.
def  var z-orderr        as logical           initial no          no-undo.
def  var d-entitys       as integer           initial 0           no-undo.
def  var xinx            as integer           initial 0           no-undo.
def  var zx-lit1         as char format "x"   initial " "         no-undo.
def  var zx-lit2         as char format "x"   initial " "         no-undo.
def  var zx-lit3         as char format "x"   initial " "         no-undo.
def  var prtcntr         as integer                               no-undo.

/** define variables for report record totals **/
def  var r-linecnt       as int format "zzzzzz9"        initial 0 no-undo.
def  var r-complete      as int format "zzzzzz9"        initial 0 no-undo.
def  var r-incomp        as int format "zzzzzz9"        initial 0 no-undo.
def  var r-subs          as int format "zzzzzz9"        initial 0 no-undo.
def  var r-requestdt     as date                                  no-undo.
def  var r-promisedt     as date                                  no-undo.
def  var r-loford        as integer                     initial 0 no-undo.

def  var r2-totamount     like oeeh.totinvamt            initial 0 no-undo.
def  var r2-linecnt       as int format "zzzzzz9"        initial 0 no-undo.
def  var r2-complete      as int format "zzzzzz9"        initial 0 no-undo.
def  var r2-incomp        as int format "zzzzzz9"        initial 0 no-undo.
def  var r2-complete2     as int format "zzzzzz9"        initial 0 no-undo.
def  var r2-incomp2       as int format "zzzzzz9"        initial 0 no-undo.
def  var r2-subs          as int format "zzzzzz9"        initial 0 no-undo.

/** define customer accumulator variables **/
def  var s-lit           as c format "x(30)" initial ""           no-undo.
def  var s-custno        as c format "x(13)" initial ""           no-undo.
def  var s-lookupnm      as c format "x(15)" initial ""           no-undo.
def  var s-whse          like icsw.whse                           no-undo.
def  var s-cnt           as int format ">>>>>>9"        initial 0 no-undo.
def  var s-ordcnt        as int format "zzzzzz9"        initial 0 no-undo.
def  var s-totvalue      as dec format "zzzzzzzzz9.99-" initial 0 no-undo.
def  var s-avgvalue      as dec format "zzzzzzzzz9.99-" initial 0 no-undo.
def  var s-linecnt       as int format "zzzzzz9"        initial 0 no-undo.
def  var s-complete      as int format "zzzzzz9"        initial 0 no-undo.
def  var s-complete2     as int format "zzzzzz9"        initial 0 no-undo.
def  var s-incomp        as int format "zzzzzz9"        initial 0 no-undo.
def  var s-incomp2       as int format "zzzzzz9"        initial 0 no-undo.
def  var s-late          as int format "zzzzzz9"        initial 0 no-undo.
def  var s-late2         as int format "zzzzzz9"        initial 0 no-undo.
def  var s-lateinc       as int format "zzzzzz9"        initial 0 no-undo.
def  var s-lateinc2      as int format "zzzzzz9"        initial 0 no-undo.
def  var s-subs          as int format "zzzzzz9"        initial 0 no-undo.
def  var s-servlevel     as dec format "zzz9.99-"       initial 0 no-undo.
def  var s-servlevel2    as dec format "zzz9.99-"       initial 0 no-undo.
def  var s-servlevel3    as dec format "zzz9.99-"       initial 0 no-undo.
def var zx-cost          like oeel.prodcost                      no-undo.
def var zx-sell          like oeel.price                         no-undo.

def  var s2-cnt           as int format ">>>>>>9"        initial 0 no-undo.
def  var s2-ordcnt        as int format "zzzzzz9"        initial 0 no-undo.
def  var s2-totvalue      as dec format "zzzzzzzzz9.99-" initial 0 no-undo.
def  var s2-avgvalue      as dec format "zzzzzzzzz9.99-" initial 0 no-undo.
def  var s2-linecnt       as int format "zzzzzz9"        initial 0 no-undo.
def  var s2-complete      as int format "zzzzzz9"        initial 0 no-undo.
def  var s2-complete2     as int format "zzzzzz9"        initial 0 no-undo.
def  var s2-incomp        as int format "zzzzzz9"        initial 0 no-undo.
def  var s2-incomp2       as int format "zzzzzz9"        initial 0 no-undo.
def  var s2-late          as int format "zzzzzz9"        initial 0 no-undo.
def  var s2-late2         as int format "zzzzzz9"        initial 0 no-undo.
def  var s2-lateinc       as int format "zzzzzz9"        initial 0 no-undo.
def  var s2-lateinc2      as int format "zzzzzz9"        initial 0 no-undo.
def  var s2-subs          as int format "zzzzzz9"        initial 0 no-undo.
def  var s2-servlevel     as dec format "zzz9.99"       initial 0 no-undo.
def  var s2-servlevel2    as dec format "zzz9.99"       initial 0 no-undo.

/** define variables for total lines **/
def {1} var t-text          as c   format "x(17)"          initial 0 no-undo.
def {1} var t-cnt           as int format ">>>>>>9"        initial 0 no-undo.
def {1} var t-ordcnt        as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-totvalue      as dec format "zzzzzzzzz9.99-" initial 0 no-undo.
def {1} var t-linecnt       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-complete      as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-complete2     as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-incomp        as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-incomp2       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-late          as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-late2         as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-lateinc       as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-lateinc2      as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-subs          as int format "zzzzzz9"        initial 0 no-undo.
def {1} var t-transcnt2     like t-transcnt                initial 0 no-undo.

def temp-table t-prtdet no-undo
    field prtrecid  as recid
    field cnt       as int 
    index k-t-prtzzdet is primary unique
          cnt.

def temp-table t-zzdet no-undo
    field custno    like oeel.custno
    field shipto    like oeel.shipto
    field orderno   like oeel.orderno
    field ordersuf  like oeel.ordersuf
    field lineno    like oeel.lineno
    field transtype like oeel.transtype
    field expshipdt like oeel.reqshipdt
    field promisedt like oeel.promisedt
    field usedt     like oeel.promisedt
    field shipdt    like oeeh.shipdt
    field shipprod  like oeel.shipprod
    field qtyord    like oeel.qtyord
    field qtyship   like oeel.qtyship
    field whse      like icsw.whse
    field region    as  character format "x"
    field district  as  character format "xxx"
    field swhse     as  character format "xxxx"
    field sregion   as  character format "x"
    field slstype   as  character format "xxx"
    field repin     as  character format "xxxx"
    field repout    as  character format "xxxx"
    field takenby   as  character format "xxxx"
    field prodcat   as  character format "xxxx"
    field tech      as  character format "xxxx"
    field loford    as  integer
    field xlate1    as  integer
    field xlate2    as  integer
    field xlate3    as  integer
    field ylate1    as  integer
    field ylate2    as  integer
    field ylate3    as  integer
    field prttype   as  character format "x"
    field rectype   as  character format "x"
    field netamt    like oeel.netamt
    field marginamt as decimal format "zzzzzz9.99-"
    field marginpct as decimal format "zzz9.9-" 
    field zpromord  as int format "zzzzzz9"
    field zreqord   as int format "zzzzzz9"
    field cnt       as int format "zzzzzz9"
    field totinvamt like oeeh.totinvamt
    field subs      as int format "zzzzzz9"
    field linecnt   as int format "zzzzzz9"
    field complete  as int format "zzzzzz9"
    field incomp    as int format "zzzzzz9"
    field complete2 as int format "zzzzzz9"
    field incomp2   as int format "zzzzzz9"
    field z2totinvamt like oeeh.totinvamt
    field z2subs      as int format "zzzzzz9"
    field z2linecnt   as int format "zzzzzz9"
    field z2complete  as int format "zzzzzz9"
    field z2incomp    as int format "zzzzzz9"
    field z2complete2 as int format "zzzzzz9"
    field z2incomp2   as int format "zzzzzz9"

    index k-t-zzdet is primary unique
        rectype     ascending
        whse        ascending
        custno      ascending
        orderno     ascending
        ordersuf    ascending
        lineno      ascending.

/{&user_temptable}*/


/{&user_drpttable}*/
   field zqrecid as recid
/{&user_drpttable}*/

/{&user_forms}*/

{f-under.i}
{f-blank.i}

form /* f-smrl2 - title line for warehouses  */
    "-------- Warehouse Transfers --------"  at 31
    "------------------ Transfer Lines -------------------"  at 69
    "Whse"              at 1
    "Name"              at 8
    "Number"            at 32
    "Total Value"       at 40
    "Avg Value"         at 57
    "Number"            at 70
    "Comp"              at 81
    "Incomp"            at 88
    "Late"              at 99
    "Late&Inc"          at 104
    "Sub/Upg"           at 115
    "Serv Lev"          at 124
with frame f-smrl2 no-box no-labels page-top width 132.

form /* f-smrl3 - detail line for customer print */
    s-custno            at 1
    s-lookupnm          at 15
    s-ordcnt            at 31
    s-totvalue          at 39
    s-avgvalue          at 54
    s-linecnt           at 69
    s-complete          at 78
    s-incomp            at 87
    s-late              at 96
    s-lateinc           at 105
    s-ontimepct         at 115 
    s-ontimefl          at 123 
    s-servlevel         at 124
    s-basefl            at 132
with frame f-smrl3 no-box no-labels down width 132.

form  /** f-smrl3a - extra line if requested & promised dates are used **/
    s-linecnt           at 69
    s-complete2         at 78
    s-incomp2           at 87
    s-late2             at 96
    s-lateinc2          at 105
    s-ontimepct2        at 115 
    s-ontimefl2         at 123 
    s-servlevel2        at 124
    s-basefl2           at 132
with frame f-smrl3a no-box no-labels down width 132.

form /* f-smrl4 - detail line for warehouse print */
    s-whse              at 1
    s-lookupnm          at 8
    s-ordcnt            at 31
    s-totvalue          at 39
    s-avgvalue          at 54
    s-linecnt           at 69
    s-complete          at 78
    s-incomp            at 87
    s-late              at 96
    s-lateinc           at 105
    s-subs              at 115
    s-servlevel         at 124
with frame f-smrl4 no-box no-labels down width 132.

form  /* f-smrl5 - total line for analysis  */
    "------- -------------- --------------"         at 31
    "-------  -------  -------"                     at 69
    "-------  -------   -------  --------"          at 96
    with frame f-smrl5 no-box no-labels width 132.

form /** Qualification statement for totals when serv level is not 0 **/
    s-servmsg                           at 20
    with frame f-footer1 no-box no-labels width 132 page-bottom.

form  /** f-footer - legend line  **/
    skip(1)
    "Service Level:"                         at 20
    "(p) - Based Upon Promised Ship Date"
    "(e) - Based Upon Expected Ship Date"
    with frame f-footer no-box no-labels width 132 page-bottom.

form /* f-smrl7 - title line if totals only for both cust and whse */
    "--------------- Orders --------------"  at 31
    "------------------- Line Items ----------------------"  at 69
    "Number"            at 32
    "Total Value"       at 40
    "Avg Value"         at 57
    "Number"            at 70
    "Comp"              at 81
    "Incomp"            at 88
    "Late"              at 99
    "Late&Inc"          at 104
    "Sub/Upg"           at 115
    "Serv Lev"          at 124
    "------------------------------"    at 31
    "------------------------------"    at 61       
    "------------------------------"    at 91
    "-----------"                       at 121
with frame f-smrl7 no-box no-labels width 132.

form /* f-smrl8 - title line if totals only for cust */
    "---------- Invoiced Orders ----------"  at 31
    "------------------ Invoiced Lines -------------------"  at 69
    "Number"            at 32
    "Total Value"       at 40
    "Avg Value"         at 57
    "Number"            at 70
    "Comp"              at 81
    "Incomp"            at 88
    "Late"              at 99
    "Late&Inc"          at 104
    "Shipping%"         at 114 
    "Serv Lev"          at 124
    "------------------------------"    at 31
    "------------------------------"    at 61
    "------------------------------"    at 91       
    "------------"                      at 121
with frame f-smrl8 no-box no-labels width 132.

form /* f-smrl9 - title line if totals only for whse */
    "-------- Warehouse Transfers --------"  at 31
    "------------------ Transfer Lines -------------------"  at 69
    "Number"            at 32
    "Total Value"       at 40
    "Avg Value"         at 57
    "Number"            at 70
    "Comp"              at 81
    "Incomp"            at 88
    "Late"              at 99
    "Late&Inc"          at 104
    "Sub/Upg"           at 115
    "Serv Lev"          at 124
with frame f-smrl9 no-box no-labels width 132.

/*si02 begin - new frames for order detail */

form /* f-detailhd - title line for order details */
    "Order#"            at 7
    "Ty"                at 16
    "Exp/Ship"          at 20
    "Promise"           at 30
    "Ship"              at 42
    "Ln"                at 50
    "Product"           at 54
    "Qty Ord"           at 80
    "Qty Shp"           at 90
    "Net Sales"         at 101
    "Margin$"           at 114
    "Mar%"              at 126
with frame f-detailhd no-box no-labels page-top width 132.

form /* f-detail - frame for order details */
    zx-lit1                                      at 4
    t-zzdet.orderno                              at 5
    t-zzdet.ordersuf                             at 13
    t-zzdet.transtype                            at 16
    t-zzdet.expshipdt                            at 20
    t-zzdet.promisedt                            at 30
    t-zzdet.shipdt                               at 40
    t-zzdet.lineno                               at 49
    t-zzdet.shipprod                             at 54
    t-zzdet.qtyord       format ">>>>9.99-"      at 79
    t-zzdet.qtyship      format ">>>>9.99-"      at 89
    zx-lit2                                      at 98
    zx-lit3                                      at 99
    t-zzdet.netamt       format ">>>>>>9.99-"    at 100
    t-zzdet.marginamt    format ">>>>>>9.99-"    at 112
    t-zzdet.marginpct    format ">>>9.9-"        at 124
with frame f-detail no-box no-labels width 132.

form
  "Customer #"      at 1
  "Lookup Name"     at 15
  "Orders"          at 32
  "Lines"           at 41
  "Total Value"     at 49
  "Orders"          at 63
  "Lines"           at 72
  "Total Value"     at 80
  "Orders"          at 94
  "Lines"           at 103
  "Order%"          at 112
  "Line%"           at 121
   "------------------------------"    at 31
   "------------------------------"    at 61
   "-------------------"               at 91       
  with frame f-head2c no-box no-labels page-top width 132.
/*
form
  s-servmsg                    at 31           
  "-------Invoiced-------"     at 31
  "-------Complete------"      at 54
  "Late"            at 80
  "On Time"         at 89
  with frame f-head1 no-box no-labels page-top width 132.
*/
form
  "-------Invoiced-------"     at 31
  "-------Complete------"      at 54
  "Late"            at 80
  "On Time"         at 89
  with frame f-head1s no-box no-labels page-top width 132.

form header
  "Customer #"      at 3
  "Lookup Name"     at 15
  "Lines"           at 32
  "Total Value"     at 40
  "Lines"           at 58
  "Total Value"     at 66
  "Lines"           at 80
  "Line%"           at 89
   "------------------------------"          at 1 
   "------------------------------"          at 31
   "------------------------------"          at 61
   "---------------------------------------" at 91       
  with frame f-head2cs no-box no-labels page-top width 132.

form header
  "Whse"            at 1
  "Name"            at 8
  "Orders"          at 32
  "Total Value"     at 41
  "Lines"           at 56
  "Orders"          at 63
  "Total Value"     at 72
  "Lines"           at 87
  "Orders"          at 94
  "Lines"           at 103
  "Orders"          at 112
  "Lines"           at 121
  with frame f-head2w no-box no-labels page-top width 132.

form /* f-smrl3 - detail line for customer print */
    s-lit               at 1 
    s-linecnt           at 31 
    s-totvalue          at 39
    s2-linecnt          at 56
    s2-totvalue         at 64
    s2-lateinc          at 79
    s-servlevel         at 88 format ">>9.99"
    s-basefl            at 94 
    f-whse              at 96 
with frame f-smrl3s no-box no-labels down width 132.

form  /** f-smrl3a - extra line if requested & promised dates are used **/
    s2-lateinc2         at 79
    s-servlevel2        at 88 format ">>9.99"
    s-basefl2           at 94
with frame f-smrl3as no-box no-labels down width 132.

form /* f-smrl4 - detail line for warehouse print */
    s-whse               at 1
    s-lookupnm           at 8
    s-ordcnt             at 31
    s-linecnt            at 39
    s-totvalue           at 47
    s2-ordcnt            at 62
    s2-linecnt           at 70
    s2-totvalue          at 78
    s2-late              at 93
    s2-lateinc           at 101
    s-ontimepct          at 111
    s-servlevel         at 120 format ">>9.99"
    s-basefl            at 126
with frame f-smrl4s no-box no-labels down width 132.

form /* f-smrl6wt - totals for warehouse totals  */
    t-text              at 1
    t-cnt               at 21
    t-ordcnt            at 31
    t-totvalue          at 39
    s-avgvalue          at 54
    t-linecnt           at 69
    t-complete          at 78
    t-incomp            at 87
    t-late              at 96
    t-lateinc           at 105
    t-subs              at 115
    s-servlevel         at 124
with frame f-smrl6wt no-box no-labels down width 132.

/{&user_forms}*/

/{&user_extractrun}*/
 run sapb_vars.
 run extract_data.
/{&user_extractrun}*/

/{&user_optiontype}*/

  if not
  can-do("W,C,A,B,R,D,G,I,O,P,T,H",substring(p-optiontype,v-inx,1))
   then
    if not x-stream then 
       display 
         "Invalid option selection valid entries W,C,B,R,D,G,I,O,P,T,H"
              substring(p-optiontype,v-inx,1).
     else
       display stream xpcd
         "Invalid option selection valid entries W,C,B,R,D,G,I,O,P,T,H"
              substring(p-optiontype,v-inx,1).

/{&user_optiontype}*/

/{&user_registertype}*/

  if not
  can-do("W,C,B,R,D,G,I,O,P,T,H",substring(p-register,v-inx,1))
   then
    if not x-stream then 
       display 
         "Invalid option selection valid entries W,C,B,R,D,G,I,O,P,T,H"
              substring(p-register,v-inx,1).
     else
       display stream xpcd
         "Invalid option selection valid entries W,C,B,R,D,G,I,O,P,T,H"
              substring(p-register,v-inx,1).

/{&user_registertype}*/

/{&user_exportvarheaders1}*/

   if v-lookup[v-inx4] = "W" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Warehouse".
    
    end. 
   else 
   if v-lookup[v-inx4] = "H" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Ship To".
    
    end. 
   else 
   if v-lookup[v-inx4] = "C" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Customer".
     assign export_rec = export_rec + v-del + "Customer".
/* Customer has two columns the sales rep + name */
    
    end. 
   else 
   if v-lookup[v-inx4] = "B" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Oe Inv/WT Ship Date".
    end. 
   else
   if v-lookup[v-inx4] = "R" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Region".
    end. 
   else
   if v-lookup[v-inx4] = "D" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "District".
    end. 
   else
   if v-lookup[v-inx4] = "G" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Sales Rep Group".
    end. 

   else
   if v-lookup[v-inx4] = "I" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Sales Rep In".
/*     assign export_rec = export_rec + v-del + "Sales Rep In". */.
    end. 

   else
   if v-lookup[v-inx4] = "O" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Sales Rep Out".
/*      assign export_rec = export_rec + v-del + "Sales Rep Out". */
    end. 

   else
   if v-lookup[v-inx4] = "P" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "Product Category".
    end. 
   else 
   if v-lookup[v-inx4] = "T" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  "TakenBy".
    end. 

/{&user_exportvarheaders1}*/

/{&user_exportvarheaders2}*/

   if v-lookup[v-inx4] = "D" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  " ".
    
    end. 
   else
   if v-lookup[v-inx4] = "C" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  " ".
     assign export_rec = export_rec + v-del + "Name".
/* Customer has two columns the sales rep + name */
    end. 
   else 
   if v-lookup[v-inx4] = "V" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  " ".
    
    end. 
   else
   if v-lookup[v-inx4] = "M" then
    do:
    if v-inx4 > 0 then 
       export_rec = export_rec + v-del.
     assign export_rec = export_rec +  " ".
     assign export_rec = export_rec + v-del + "Name".
/* Credit Mgr has two columns the sales rep + name */
    
    end. 

/{&user_exportvarheaders2}*/

/{&user_exportstatheaders}*/
/{&user_exportstatheaders}*/


/{&user_foreach}*/
 for each t-zzdet no-lock:
/{&user_foreach}*/

/{&user_loadtokens}*/

 if v-lookup[v-inx] = "w" then
   do:
   if v-inx > 1 then
     assign v-mystery = v-mystery + v-delim
            v-sortmystery = v-sortmystery + v-delim.
   assign v-token =
          caps(string(t-zzdet.whse,"x(4)")).
   assign v-mystery = v-mystery + v-token.
   if v-sortup[v-inx] = "<" then
     run ascii_make_it_not (input-output v-token).
   assign  v-sortmystery = v-sortmystery +
                           string(v-token,"x(4)").
   end.
  else

 if v-lookup[v-inx] = "h" then
   do:
   if v-inx > 1 then
     assign v-mystery = v-mystery + v-delim
            v-sortmystery = v-sortmystery + v-delim.
   assign v-token =
          caps(string(t-zzdet.shipto,"x(8)")).
   assign v-mystery = v-mystery + v-token.
   if v-sortup[v-inx] = "<" then
     run ascii_make_it_not (input-output v-token).
   assign  v-sortmystery = v-sortmystery +
                           string(v-token,"x(8)").
   end.
  else
  if v-lookup[v-inx] = "c" then
   do:
   if v-inx > 1 then
     assign v-mystery = v-mystery + v-delim
            v-sortmystery = v-sortmystery + v-delim.
   assign v-token =
          caps(string(t-zzdet.custno,"999999999999")).
   assign v-mystery = v-mystery + v-token.
   if v-sortup[v-inx] = "<" then
     run ascii_make_it_not (input-output v-token).
   assign  v-sortmystery = v-sortmystery +
                           string(v-token,"x(12)").
   end.
  else
  if v-lookup[v-inx] = "b" then
    do:
    if v-inx > 1 then
      assign v-mystery = v-mystery + v-delim
             v-sortmystery = v-sortmystery + v-delim.
    assign v-token = caps(string(year(t-zzdet.shipdt),"9999") +
                          string(month(t-zzdet.shipdt),"99")  +
                          string(day(t-zzdet.shipdt),"99")).   
    assign v-mystery = v-mystery + v-token.
    if v-sortup[v-inx] = "<" then
      run ascii_make_it_not (input-output v-token).
    assign v-sortmystery = v-sortmystery +
                           string(v-token,"x(8)").
    end.
  else
  if v-lookup[v-inx] = "r" then
   do:
   if v-inx > 1 then
     assign v-mystery = v-mystery + v-delim
            v-sortmystery = v-sortmystery + v-delim.
   assign v-token =
          caps(string(t-zzdet.sregion,"x(1)")).
   assign v-mystery = v-mystery + v-token.
   if v-sortup[v-inx] = "<" then
     run ascii_make_it_not (input-output v-token).
   assign  v-sortmystery = v-sortmystery +
                           string(v-token,"x(1)").
   end.
  else
  if v-lookup[v-inx] = "d" then
    do:
    if v-inx > 1 then
      assign v-mystery = v-mystery + v-delim
             v-sortmystery = v-sortmystery + v-delim.
    assign v-token =
           caps(string(t-zzdet.region,"x(1)") +
                string(t-zzdet.district,"x(3)")).
    assign v-mystery = v-mystery + v-token.
    if v-sortup[v-inx] = "<" then
      run ascii_make_it_not (input-output v-token).
    assign v-sortmystery = v-sortmystery +
                           string(v-token,"x(4)").
    end.
  else
  if v-lookup[v-inx] = "g" then
    do:
    if v-inx > 1 then                   
      assign v-mystery = v-mystery + v-delim
             v-sortmystery = v-sortmystery + v-delim.
    v-token = caps(string(t-zzdet.slstype,"xxx")).
    v-mystery = v-mystery + v-token.
    if v-sortup[v-inx] = "<" then
      run ascii_make_it_not (input-output v-token).
    v-sortmystery = v-sortmystery +
                    string(v-token,"x(3)").
    end.
  else
  if v-lookup[v-inx] = "i" then
    do:
    if v-inx > 1 then
      assign v-mystery = v-mystery + v-delim
             v-sortmystery = v-sortmystery + v-delim.
    assign v-token =
           caps(string(t-zzdet.repin,"x(4)")).
    assign v-mystery = v-mystery + v-token.
    if v-sortup[v-inx] = "<" then
      run ascii_make_it_not (input-output v-token).
    assign v-sortmystery = v-sortmystery +
                           string(v-token,"x(4)").
    end.
  else
  if v-lookup[v-inx] = "o" then
    do:
    if v-inx > 1 then
      assign v-mystery = v-mystery + v-delim
             v-sortmystery = v-sortmystery + v-delim.
    assign v-token =
           caps(string(t-zzdet.repout,"x(4)")).
    assign v-mystery = v-mystery + v-token.
    if v-sortup[v-inx] = "<" then
      run ascii_make_it_not (input-output v-token).
    assign v-sortmystery = v-sortmystery +
                           string(v-token,"x(4)").
    end.
  else
  if v-lookup[v-inx] = "p" then
    do:
    if v-inx > 1 then
      assign v-mystery = v-mystery + v-delim
             v-sortmystery = v-sortmystery + v-delim.
    assign v-token =
           caps(string(t-zzdet.prodcat,"x(4)")).
    assign v-mystery = v-mystery + v-token.
    if v-sortup[v-inx] = "<" then
      run ascii_make_it_not (input-output v-token).
    assign v-sortmystery = v-sortmystery +
                           string(v-token,"x(4)").
    end.
  else
  if v-lookup[v-inx] = "t" then
    do:
    if v-inx > 1 then
      assign v-mystery = v-mystery + v-delim
             v-sortmystery = v-sortmystery + v-delim.
    assign v-token =
           caps(string(t-zzdet.takenby,"x(4)")).
    assign v-mystery = v-mystery + v-token.
    if v-sortup[v-inx] = "<" then
      run ascii_make_it_not (input-output v-token).
    assign v-sortmystery = v-sortmystery +
                           string(v-token,"x(4)").
    end.
  else  
  if v-lookup[v-inx] = "x" then
    do:
    if v-inx > 1 then
      assign v-mystery = v-mystery + v-delim
             v-sortmystery = v-sortmystery + v-delim.
    assign v-token =  string(recid(t-zzdet),"999999999999").
    assign v-mystery = v-mystery + v-token.
    if v-sortup[v-inx] = "<" then
      run ascii_make_it_not (input-output v-token).
    assign v-sortmystery = v-sortmystery +
                           v-token.
    end.
  
/{&user_loadtokens}*/

/{&user_totaladd}*/

 assign t-amounts[1]    = t-zzdet.cnt
        t-amounts[2]    = t-zzdet.linecnt     
        t-amounts[3]    = t-zzdet.totinvamt   
        t-amounts[4]    = t-zzdet.z2linecnt   
        t-amounts[5]    = t-zzdet.z2totinvamt
        t-amounts[6]    = t-zzdet.complete
        t-amounts[7]    = t-zzdet.incomp
/** only add to late exp/ship date -  first line of det **/
        t-amounts[8]    = if t-zzdet.usedt < t-zzdet.shipdt then  
                             t-zzdet.z2complete
                          else 
                             0
        t-amounts[9]    = if t-zzdet.usedt < t-zzdet.shipdt then  
                             t-zzdet.z2incomp
                          else 
                             0
/** only add to late promise date ctr - second line of det **/
        t-amounts[10]    = if t-zzdet.promise < t-zzdet.shipdt then  
                             t-zzdet.z2complete
                          else 
                             0
        t-amounts[11]    = if t-zzdet.promise < t-zzdet.shipdt then  
                             t-zzdet.z2incomp
                          else 
                             0
        t-amounts[12]   = t-zzdet.xlate1
        t-amounts[13]   = t-zzdet.xlate2
        t-amounts[14]   = t-zzdet.ylate1
        t-amounts[15]   = t-zzdet.ylate2
        t-amounts[16]   = t-zzdet.loford.
                                  
/{&user_totaladd}*/

/{&user_drptassign}*/
   drpt.zqrecid = recid(t-zzdet)   
/{&user_drptassign}*/

/{&user_viewheadframes}*/
  form header
    s-servmsg                    at 44           
    "-------Invoiced-------"     at 31
    "-------Complete------"      at 54
   "Late"            at 80
   "On Time"         at 89
   with frame f-head1 no-box no-labels page-top width 132.
   if not x-stream then 
     do: 
        view frame f-head1.
        view frame f-head2cs.
        view frame f-footer.
     end.   
/{&user_viewheadframes}*/

/{&user_drptloop}*/
/{&user_drptloop}*/

/{&user_detailprint}*/
  /** if not = all others only - print only items we choose to see */

  find t-zzdet where recid(t-zzdet) = drpt.zqrecid
       no-lock no-error.

/** if the register entity(in this case "c") doesn't meet the 
    option criteria(see formatoptions procedure) the entity is 
    filled in with ~~~~. The ~~~~ will not create detail here. **/        
/**------------------------------------------------------------**/
  if p-detail = "d" and 
     entry((u-entitys - 1),drpt.sortmystery,v-delim) <> 
     fill("~~",length(entry((u-entitys - 1),drpt.sortmystery,v-delim))) then 
   do:
     if p-prttype = "n" or 
       (p-prttype = "e" and
        (t-zzdet.loford > 0 and t-zzdet.z2incomp > 0)) then
       next. 
     else 
       do:    
         prtcntr = prtcntr + 1 .
         create t-prtdet.
         assign t-prtdet.prtrecid = drpt.zqrecid
                t-prtdet.cnt = prtcntr.
       end.
   end.
/{&user_detailprint}*/

/{&user_finalprint}*/
  display with frame f-blank.
  assign v-per1 =
        if t-amounts4[u-entitys + 1]   = 0 then
           100 
        else
           100 - round(((t-amounts8[u-entitys + 1] / 
                         t-amounts4[u-entitys + 1]) * 100),2). 
  assign v-per2 =
        if t-amounts4[u-entitys + 1]   = 0 then
           100 
        else
           100 - round(((t-amounts10[u-entitys + 1] / 
                         t-amounts4[u-entitys + 1]) * 100),2). 

 if p-datefl = "b" then 
  do: 
    assign s-basefl  = "e"
           s-basefl2 = "p".
  end.
 else 
    assign s-basefl = p-datefl.       

 if t-amounts8[u-entitys + 1] = 0 and 
    t-amounts4[u-entitys + 1] = 0 then
    x-per1 = "        ". 
 else
    x-per1 = string(v-per1,"zzz9.99-").            
 if t-amounts10[u-entitys + 1] = 0 and 
    t-amounts4[u-entitys + 1] = 0 then
    x-per2 = "        ". 
 else
    x-per2 = string(v-per2,"zzz9.99-").            
                 
                
 if not x-stream then 
   do: 
    display     
     v-summary-lit2              @ s-lit 
     t-amounts2[u-entitys + 1]   @ s-linecnt
     t-amounts3[u-entitys + 1]   @ s-totvalue
     t-amounts4[u-entitys + 1]   @ s2-linecnt
     t-amounts5[u-entitys + 1]   @ s2-totvalue      
     t-amounts8[u-entitys + 1]   @ s2-lateinc          
     x-per1                      @ s-servlevel
     s-basefl                    @ s-basefl
     with frame f-smrl3s.
     down with frame f-smrl3s.
     if p-datefl = "b" then 
        display  t-amounts10[u-entitys + 1]  @ s2-lateinc2          
                 x-per2                      @ s-servlevel2
                 s-basefl2                   @ s-basefl2
                 with frame f-smrl3as.
                 down with frame f-smrl3as.
   end.
 if p-prtwhse <> "n" then 
  do:
     hide frame f-head1.
     hide frame f-head2cs.
     run whse-data.
  end.
/{&user_finalprint}*/

/{&user_numbertotals}*/

         /* Here the amount     */  if v-subtotalup[v-inx4] = 1 then
         /* that is to be       */  
                                     assign v-val = 
                                      (100 -
                                      (if t-amounts4[v-inx4] = 0 then
                                         0
                                       else
                                         round(((t-amounts8[v-inx4] /
                                                 t-amounts4[v-inx4])
                                          * 100),2))).         
         /* accumulated for the */   
                                    else
         /* subtotal sort is    */  if v-subtotalup[v-inx4] = 2 then
                                     assign v-val = 
                                      (100 -
                                      (if t-amounts4[v-inx4] = 0 then
                                         0
                                       else
                                         round(((t-amounts10[v-inx4] /
                                                 t-amounts4[v-inx4])
                                          * 100),2))).  
         /* determined by param */  
         /* this gives power to */  else
         /* sort on any amount  */  if v-subtotalup[v-inx4] = 3 then
         /* on the report at    */    assign v-val = t-amounts3[v-inx4].
         /* any level.          */  else
                                    if v-subtotalup[v-inx4] = 4 then
                                      assign v-val = t-amounts4[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 5 then
                                      assign v-val = t-amounts5[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 6 then
                                      assign v-val =  t-amounts6[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 7 then
                                      assign v-val = t-amounts7[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 8 then
                                      assign v-val = t-amounts8[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 9 then
                                      assign v-val = t-amounts9[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 10 then
                                      assign v-val = t-amounts10[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 11 then
                                      assign v-val = t-amounts11[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 12 then
                                      assign v-val = t-amounts12[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 13 then
                                      assign v-val = t-amounts13[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 14 then
                                      assign v-val = t-amounts14[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 15 then
                                      assign v-val = t-amounts15[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 16 then
                                      assign v-val = t-amounts16[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 17 then
                                      assign v-val = t-amounts17[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 18 then
                                      assign v-val = t-amounts18[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 19 then
                                      assign v-val = t-amounts19[v-inx4].
                                    else
                                    if v-subtotalup[v-inx4] = 20 then
                                      assign v-val = t-amounts20[v-inx4].
                                    else
                                      v-val = t-amounts1[v-inx4].              

                                    do inz = 1 to 20:
         /* The registers need  */  if inz = 1 then
         /* to be updated for   */   
                                     v-regval[inz] = 
                                       (100 -
                                       (if t-amounts4[v-inx4] = 0 then
                                         0
                                       else
                                         round(((t-amounts8[v-inx4] /
                                                 t-amounts4[v-inx4])
                                          * 100),2))).         
         /* accumulation of the */  else
         /* totals that can be  */  if inz = 2 then
                                     v-regval[inz] = 
                                       (100 -
                                       (if t-amounts4[v-inx4] = 0 then
                                         0
                                       else
                                         round(((t-amounts10[v-inx4] /
                                                 t-amounts4[v-inx4])
                                          * 100),2))).         
         /* controlled by the   */         
         /* parameters          */  else
         /*                     */  if inz = 3 then
         /*                     */    v-regval[inz] = t-amounts3[v-inx4].      
         /*                     */  else
                                    if inz = 4 then
                                      v-regval[inz] = t-amounts4[v-inx4].
                                    else
                                    if inz = 5 then
                                      v-regval[inz] = t-amounts5[v-inx4].   
                                    else
                                    if inz = 6 then
                                      v-regval[inz] = t-amounts6[v-inx4].    
                                    else
                                    if inz = 7 then
                                      v-regval[inz] = t-amounts7[v-inx4]. 
                                    else
                                    if inz = 8 then
                                      v-regval[inz] = t-amounts8[v-inx4].      
                                    else
                                    if inz = 9 then
                                      v-regval[inz] = t-amounts9[v-inx4].      
                                    else
                                    if inz = 10 then
                                      v-regval[inz] = t-amounts10[v-inx4].     
                                    else
                                    if inz = 11 then
                                      v-regval[inz] = t-amounts11[v-inx4].     
                                    else
                                    if inz = 12 then
                                      v-regval[inz] = t-amounts12[v-inx4].     
                                    else
                                    if inz = 13 then
                                      v-regval[inz] = t-amounts13[v-inx4].     
                                    else
                                    if inz = 14 then
                                      v-regval[inz] = t-amounts14[v-inx4].     
                                    else
                                    if inz = 15 then
                                      v-regval[inz] = t-amounts15[v-inx4].     
                                    else
                                    if inz = 16 then
                                      v-regval[inz] = t-amounts16[v-inx4].     
                                    else
                                    if inz = 17 then
                                      v-regval[inz] = t-amounts17[v-inx4].     
                                    else
                                    if inz = 18 then
                                      v-regval[inz] = t-amounts18[v-inx4].     
                                    else
                                    if inz = 19 then
                                      v-regval[inz] = t-amounts19[v-inx4].     
                                    else
                                    if inz = 20 then
                                      v-regval[inz] = t-amounts20[v-inx4].     
                                    end.

 
/{&user_numbertotals}*/

/{&user_summaryloadprint}*/

  assign d-entitys = if p-detail = "d" then
                       u-entitys - 1 
                     else 
                       u-entitys.
  
  if v-lookup[v-inx2] = "w" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = d-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "Whse - " + h-lookup + " " + h-genlit.
     
   end. 
  else
  if v-lookup[v-inx2] = "C" then
     do:
     if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
       do:
       assign h-lookup = "All Others"
              h-genlit = "".
       if v-inx2 = d-entitys then
         assign v-summary-lit2 = 
                             substring(h-genlit,1,9) + " " +
                             h-lookup. 
       else
         assign v-summary-lit2 = "Cust -  " +
                             substring(h-genlit,1,9) + " " +
                             h-lookup. 
       end.
     else
       do:
       find arsc where arsc.cono = g-cono and 
                       arsc.custno = dec(h-lookup) no-lock no-error.
       if avail arsc then
         h-genlit = arsc.lookupnm.                      
       if v-inx2 = d-entitys then
         assign v-summary-lit2 = string(dec(h-lookup),">>>>>>>>>>>9") + " " +
                                 substring(h-genlit,1,15).
       else
         assign v-summary-lit2 = "Cust - " +
                                 string(dec(h-lookup),">>>>>>>>>>>9") + " " +
                                 substring(h-genlit,1,15).
 
       end.
     end.
  else
  if v-lookup[v-inx2] = "B" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = d-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "Oe Inv/Wt Ship Date - " + h-lookup + " "
                                                      + h-genlit.
   end. 
  else 
  if v-lookup[v-inx2] = "R" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = d-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "Region - " + h-lookup + " " + h-genlit.
     
   end. 
  else 
  if v-lookup[v-inx2] = "H" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = d-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "ShipTo - " + h-lookup + " " + h-genlit.
     
   end. 
  else 
  if v-lookup[v-inx2] = "g" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = d-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "Sales Group - " + h-lookup + " " + h-genlit.
   end. 
  else 
  if v-lookup[v-inx2] = "D" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = d-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "District - " + h-lookup + " " + h-genlit.
   end. 
  else 
  if v-lookup[v-inx2] = "I" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = d-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "Sales Rep In - " + h-lookup + " " + h-genlit.
   end. 
  else 
  if v-lookup[v-inx2] = "O" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = d-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "Sales Rep Out - " + h-lookup + " " + h-genlit.
   end. 
  else 
  if v-lookup[v-inx2] = "P" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = d-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "Product Category - " + h-lookup + " " + h-genlit.
   end. 
  else 
  if v-lookup[v-inx2] = "T" then
   do:
   if entry(v-inx2,v-holdsortmystery,v-delim) begins "~~~~~~~~~~~~~~" then
     assign h-lookup = "All Others".
   if v-inx2 = d-entitys then
     assign v-summary-lit2 = h-lookup + " " + h-genlit.
   else  
     assign v-summary-lit2 = "Taken By - " + h-lookup + " " + h-genlit.
   end. 
 /{&user_summaryloadprint}*/

/{&user_summaryframeprint}*/
 assign v-per1 =
        if t-amounts4[v-inx2]   = 0 then
           100 
        else
           100 - round(((t-amounts8[v-inx2] / 
                         t-amounts4[v-inx2]) * 100),2). 
 assign v-per2 =
        if t-amounts4[v-inx2]   = 0 then
           100 
        else
           100 - round(((t-amounts10[v-inx2] / 
                         t-amounts4[v-inx2]) * 100),2). 


 if t-amounts8[v-inx2] = 0 and 
    t-amounts4[v-inx2] = 0 then
    x-per1 = "        ". 
 else
    x-per1 = string(v-per1,"zzz9.99-").            
 if t-amounts10[v-inx2] = 0 and 
    t-amounts4[v-inx2] = 0 then
    x-per2 = "        ". 
 else
    x-per2 = string(v-per2,"zzz9.99-").            
                          
                         
                         
 if p-datefl = "b" then 
  do: 
    assign s-basefl  = "e"
           s-basefl2 = "p".
  end.
 else 
    assign s-basefl = p-datefl.       
  
 if not x-stream then 
   do: 
    display     
     v-summary-lit2       @ s-lit 
     t-amounts2[v-inx2]   @ s-linecnt
     t-amounts3[v-inx2]   @ s-totvalue
     t-amounts4[v-inx2]   @ s2-linecnt
     t-amounts5[v-inx2]   @ s2-totvalue      
     t-amounts8[v-inx2]   @ s2-lateinc          
     x-per1               @ s-servlevel
     s-basefl             @ s-basefl
     with frame f-smrl3s.
     down with frame f-smrl3s.
     if p-datefl = "b" then 
        display  t-amounts10[v-inx2]   @ s2-lateinc2          
                 x-per2                @ s-servlevel2
                 s-basefl2             @ s-basefl2
                 with frame f-smrl3as.
                 down with frame f-smrl3as.
     if p-prtdetail and v-inx2 = d-entitys then
        run print-cust-dtl.
     end.
 /{&user_summaryframeprint}*/

/{&user_summaryloadexport}*/
/{&user_summaryloadexport}*/

/{&user_summaryputexport}*/
/{&user_summaryputexport}*/

/{&user_procedures}*/

/**-----------------Begin Procedure Section-------------------------------**/
procedure sapb_vars:
/**-----------------------------------------------------------------------**/

/* LOAD PARAMETERS FROM SAPB */
assign 
    b-whse     = sapb.rangebeg[1]
    e-whse     = sapb.rangeend[1]
    b-custno   = {c-asignd.i "sapb.rangebeg[2]" "1" "12"}
    e-custno   = {c-asignd.i "sapb.rangeend[2]" "1" "12"}
    b-shipwhse = sapb.rangebeg[3]
    e-shipwhse = sapb.rangeend[3]
    v-datein   = sapb.rangebeg[4].
{p-rptdt.i}
assign 
    b-date   = v-dateout
    v-datein = sapb.rangeend[4].
{p-rptdt.i}
assign 
    e-date       = v-dateout
    b-region     = sapb.rangebeg[5]
    e-region     = sapb.rangeend[5]
    b-district   = sapb.rangebeg[6]
    e-district   = sapb.rangeend[6]
    b-group      = sapb.rangebeg[7]
    e-group      = sapb.rangeend[7]
    b-slsrepin   = sapb.rangebeg[8]
    e-slsrepin   = sapb.rangeend[8]
    b-slsrepout  = sapb.rangebeg[9]
    e-slsrepout  = sapb.rangeend[9]
    b-prodcat    = sapb.rangebeg[10]
    e-prodcat    = sapb.rangeend[10]
    b-tech       = sapb.rangebeg[11]
    e-tech       = sapb.rangeend[11]
    b-takenby    = sapb.rangebeg[12]
    e-takenby    = sapb.rangeend[12]
    p-format     = int(sapb.optvalue[13])
    p-prtcust    = sapb.optvalue[2]
    p-prtwhse    = sapb.optvalue[3]
    p-prtdisp    = sapb.optvalue[4]
    p-prtspecial = if sapb.optvalue[5] = "yes" then yes else no
    p-prtnonstk  = if sapb.optvalue[6] = "yes" then yes else no
    p-prtsubs    = if sapb.optvalue[7] = "yes" then yes else no
    p-prtbo      = if sapb.optvalue[8] = "yes" then yes else no
    p-prtdirect  = if sapb.optvalue[9] = "yes" then yes else no
    p-datefl     = sapb.optvalue[10]
    p-prttype    = sapb.optvalue[12]
    p-prtdetail  = if sapb.optvalue[12] <> "n" and                 
                   can-do("d",p-prtcust) then yes else no.

/* if p-detail is not set then zsdixprtx.p will not create detail items */
if p-prtdetail then 
   p-detail = "d".

if b-prodcat = "" then 
   assign b-prodcat = "    ".
if e-prodcat = "" then 
   assign e-prodcat = "zzzz".
   
if b-tech = "" then 
   assign b-tech = "    ".
if e-tech = "" then 
   assign e-tech = "zzzz".

if b-takenby = "" then 
   assign b-takenby = "    ".
if e-takenby = "" then 
   assign e-takenby = "zzzz".

{p-rptcha.i 
    &field  = "sapb.optvalue[11]"
    &result = "p-prtserv"}
 
if p-prtserv <> 0 then  
   s-servmsg = "Totals Represent Service Levels at or Below  "
                + string(p-prtserv) + "%".
else 
   s-servmsg = " ".

run formatoptions.

end. /* end of sapb_vars procedure */

/**-------------------------------------------------------------------------**/
procedure formatoptions: 
/**-------------------------------------------------------------------------**/
  assign p-portrait = true.
  
  if sapb.optvalue[1] > "0" and sapb.optvalue[1] ne "99" then 
  do:
    p-optiontype = " ".
   find notes where
        notes.cono = g-cono        and 
        notes.notestype = "zz"     and
        notes.primarykey = "smrzq2" and
        notes.secondarykey = sapb.optvalue[1]
        no-lock no-error.
   if not avail notes then
    do:
     message "Invalid Sort Format Type for Smrzq " " --> " sapb.optvalue[1].
     pause 6.
     return.
    end.
   else
    do:
     p-summcounts = "a,a,a,a,a,a".
     overlay(p-optiontype,1,(length(notes.noteln[1]))) = notes.noteln[1].
     overlay(p-sorttype,1,  (length(notes.noteln[2]))) = notes.noteln[2].
     overlay(p-totaltype,1, (length(notes.noteln[3]))) = notes.noteln[3].
     overlay(p-summcounts,1,(length(notes.noteln[4]))) = notes.noteln[4].
    end.
  end. /* do sapb.optvalue[13] */
  else 
  if sapb.optvalue[1] = "99" then 
    do:
    run reportopts(input sapb.user5, 
                   input-output p-optiontype,
                   input-output p-sorttype,  
                   input-output p-totaltype,
                   input-output p-summcounts,
                   input-output p-register,
                   input-output p-regval).

/* Customer has to be chosen if detail is chosen                */    
    if index(p-optiontype,"c",1) = 0 and p-detail = "d"then
      do:
      assign p-optiontype = p-optiontype + ",c"
             p-sorttype   = p-sorttype + ",>"
             p-totaltype  = p-totaltype + ",t"
             p-summcounts = p-summcounts + ",a".
      end.       
    end.
 
  assign p-register   = ""
         p-registerex = "".
  if dec(sapb.optvalue[11]) <> 0 then  /* min service % set register param */
     assign p-register   = "c#1"      /* customer total 3 on report        */
            p-registerex = "L" + sapb.optvalue[11].
/*** c#1 is the bucket or accum the expression(L or ge or <1000) will be 
    applied to **/
  if sapb.optvalue[10] = "b" and
     dec(sapb.optvalue[11]) <> 0 then 
   do:
     assign p-register   = p-register + ","  
            p-registerex = p-registerex + ",".
     assign p-register   = "c#2" /* customer total 1 in total accums  */
            p-registerex = "L" + sapb.optvalue[11].
   end.

end procedure. /* procedure formatoptions */

/**-------------------------------------------------------------------------**/
procedure extract_data: 
/**-------------------------------------------------------------------------**/

/* Get records and load temp-table */
if p-prtcust <> "" then 
do:
    /* tb21775 03/26/98 cm; Improve performance; Create multiple for each
        statements based on ranges entered */
    /* If a single warehouse is entered, read icsd first.  If a range of
        warehouses is entered, it is more efficient to read the oeeh records
        ONCE and the icsd multiple times as warehouse is not part of the oeeh
        index.  Otherwise you need to read ALL oeeh records within the customer
        or invoice date range once per warehouse. */
    if b-whse = e-whse then 
    do:
        {icsd.gfi &whse = b-whse
                  &lock = "no"}
            /* Invoice date range entered, use k-invproc */
            if string(b-date) <> v-lowdt or string(e-date) <> v-highdt then
           
            for each oeeh use-index k-invproc where
                oeeh.cono       = g-cono          and
                oeeh.invoicedt >= b-date          and 
                oeeh.invoicedt <= e-date          and
                oeeh.custno    >= b-custno        and 
                oeeh.custno    <= e-custno        and
/*
                oeeh.slsrepin  >= b-slsrepin      and /* si01 */
                oeeh.slsrepin  <= e-slsrepin      and /* si01 */
                oeeh.slsrepout >= b-slsrepout     and /* si01 */
                oeeh.slsrepout <= e-slsrepout     and /* si01 */
*/
                oeeh.takenby   >= b-takenby       and /* si01 */
                oeeh.takenby   <= e-takenby       and /* si01 */
                oeeh.stagecd   <> 9               and
                oeeh.whse       = icsd.whse       and 
              ((oeeh.orderdisp  = ""              and 
                can-do("b,r",p-prtdisp))           or
               (oeeh.orderdisp <> ""              and 
                can-do("b,s",p-prtdisp)))         and
               (can-do("so,cs,br",oeeh.transtype)  or
               (oeeh.transtype  = "do"            and 
                p-prtdirect     = yes))         
            no-lock:
               run extract_oeel. 
            end.  /*  for each oeeh */

            /* Use k-custno if invoice date range not entered */
            else
            for each oeeh use-index k-custno where
                oeeh.cono       = g-cono          and
                oeeh.custno    >= b-custno        and 
                oeeh.custno    <= e-custno        and
/*
                oeeh.slsrepin  >= b-slsrepin      and /* si01 */
                oeeh.slsrepin  <= e-slsrepin      and /* si01 */
                oeeh.slsrepout >= b-slsrepout     and /* si01 */
                oeeh.slsrepout <= e-slsrepout     and /* si01 */
*/                
                oeeh.takenby   >= b-takenby       and /* si01 */
                oeeh.takenby   <= e-takenby       and /* si01 */
                oeeh.stagecd   <> 9               and
                oeeh.whse       = icsd.whse       and 
                oeeh.invoicedt >= b-date          and 
                oeeh.invoicedt <= e-date          and
              ((oeeh.orderdisp  = ""              and 
                can-do("b,r",p-prtdisp))           or
               (oeeh.orderdisp <> ""              and 
                can-do("b,s",p-prtdisp)))         and
               (can-do("so,cs,br",oeeh.transtype)  or
               (oeeh.transtype  = "do"            and 
                p-prtdirect     = yes))          
            no-lock:
               run extract_oeel.   
            end.  /* for each oeeh */
    end.  /* if b-whse = e-whse */

    /* Invoice date range entered, use k-invproc */
    else if string(b-date) <> v-lowdt or string(e-date) <> v-highdt then
    do:
    for each zz-icsd where zz-icsd.cono = g-cono and
             zz-icsd.whse      >= b-whse          and 
             zz-icsd.whse      <= e-whse          no-lock: 

    for each oeeh use-index k-invproc where
        oeeh.cono       = g-cono          and
        oeeh.invoicedt >= b-date          and 
        oeeh.invoicedt <= e-date          and
        oeeh.custno    >= b-custno        and 
        oeeh.custno    <= e-custno        and
/*
        oeeh.slsrepin  >= b-slsrepin      and /* si01 */
        oeeh.slsrepin  <= e-slsrepin      and /* si01 */
        oeeh.slsrepout >= b-slsrepout     and /* si01 */
        oeeh.slsrepout <= e-slsrepout     and /* si01 */
*/
        oeeh.takenby   >= b-takenby       and /* si01 */
        oeeh.takenby   <= e-takenby       and /* si01 */
        oeeh.stagecd   <> 9               and
        oeeh.whse       = zz-icsd.whse    and  
      ((oeeh.orderdisp  = ""              and 
        can-do("b,r",p-prtdisp))           or
       (oeeh.orderdisp <> ""              and 
        can-do("b,s",p-prtdisp)))         and
       (can-do("so,cs,br",oeeh.transtype)  or
       (oeeh.transtype  = "do"            and 
        p-prtdirect     = yes))     
    no-lock:
          run extract_oeel.   
    end.  /* for each oeeh */
    end. /* for each icsd */ 
    end.  /* do */
    /* Use k-custno if invoice date range not entered */
    else 
    for each oeeh use-index k-custno where
        oeeh.cono       = g-cono          and
        oeeh.custno    >= b-custno        and 
        oeeh.custno    <= e-custno        and
/*  
        oeeh.slsrepin  >= b-slsrepin      and /* si01 */
        oeeh.slsrepin  <= e-slsrepin      and /* si01 */
        oeeh.slsrepout >= b-slsrepout     and /* si01 */
        oeeh.slsrepout <= e-slsrepout     and /* si01 */
*/
        oeeh.takenby   >= b-takenby       and /* si01 */
        oeeh.takenby   <= e-takenby       and /* si01 */
        oeeh.stagecd   <> 9               and
        oeeh.whse      >= b-whse          and 
        oeeh.whse      <= e-whse          and 
        oeeh.invoicedt >= b-date          and 
        oeeh.invoicedt <= e-date          and
      ((oeeh.orderdisp  = ""              and 
        can-do("b,r",p-prtdisp))           or
       (oeeh.orderdisp <> ""              and 
        can-do("b,s",p-prtdisp)))         and
       (can-do("so,cs,br",oeeh.transtype)  or
       (oeeh.transtype  = "do"            and 
        p-prtdirect     = yes))           and
       (oeeh.borelfl    = no or p-prtbo)
    no-lock:
        run extract_oeel. 
    end.  /* for each oeeh */
end.
end procedure. 


/**-------------------------------------------------------------------------**/
procedure extract_oeel: 
/**-------------------------------------------------------------------------**/

 r-loford = 0.
 if oeeh.boexistsfl and
    can-find(first xz-oeeh where
                   xz-oeeh.cono     = g-cono and
                   xz-oeeh.orderno  = oeeh.orderno and
                   xz-oeeh.ordersuf > oeeh.ordersuf) then
   r-loford = 0.
 else 
   r-loford = 1.

for each oeel use-index k-oeel where
    oeel.cono         = g-cono         and
    oeel.orderno      = oeeh.orderno   and
    oeel.ordersuf     = oeeh.ordersuf  and
   (oeel.prodcat     >= b-prodcat      and 
    oeel.prodcat     <= e-prodcat)     and  
    oeel.warrexchgfl  = no             and
    oeel.returnfl     = no             and
    oeel.statustype  <> "c"            and
   (can-do("y,n,p",oeel.botype)         or
    oeel.botype       = "d"            and 
    p-prtdirect)                       and
   (not can-do("s,u", oeel.xrefprodty)  or
   (can-do("s,u", oeel.xrefprodty)     and 
    p-prtsubs))                        and
  ((oeel.specnstype   = "")             or
   (oeel.specnstype   = "n"            and 
    p-prtnonstk       = yes)            or
   (oeel.specnstype   = "s"            and 
    p-prtspecial      = yes))
no-lock:


x-slsrepin = (if oeel.slsrepin <> "" then
               oeel.slsrepin
            else
               oeeh.slsrepin).



{w-smsn.i x-slsrepin no-lock} 
if avail smsn then
  do:
   if smsn.slsrep < b-slsrepin or 
      smsn.slsrep > e-slsrepin 
   then next.   
  end.
else
  do:
   if x-slsrepin < b-slsrepin or 
      x-slsrepin > e-slsrepin 
   then next.   
  end.



x-slsrepout = (if oeel.slsrepout <> "" then
                 oeel.slsrepout
               else
                 oeeh.slsrepout).


{w-smsn.i x-slsrepout no-lock} 
if avail smsn then
  do:
  if substring(smsn.mgr,1,1) < b-region or
     substring(smsn.mgr,1,1) > e-region 
  then next.
  if substring(smsn.mgr,2,3) < b-district or     
     substring(smsn.mgr,2,3) > e-district        
  then next.
  if smsn.slstype < b-group or
     smsn.slstype > e-group 
  then next.
  if smsn.slsrep < b-slsrepout or 
     smsn.slsrep > e-slsrepout 
  then next.   
  end.
else
  do:
  if "Z" < b-region or
     "Z" > e-region 
  then next.
  if "999" < b-district or     
     "999" > e-district        
  then next.
  if "Z" < b-group or
     "Z" > e-group 
  then next.
  if x-slsrepout < b-slsrepout or 
     x-slsrepout > e-slsrepout 
  then next.   
  end.





 /* Range Technology Check  */
  find catmaster where catmaster.cono = 1 and 
                       catmaster.notestype = "zz" and
                       catmaster.primarykey  = "apsva" and
                       catmaster.secondarykey = 
                       (substring(oeel.prodcat,1,4))
                       no-lock no-error. 
  if not avail catmaster then
     do:
     if substring(oeel.prodcat,1,1) = "a" then 
       v-technology = "Automation".
     else if substring(oeel.prodcat,1,1) = "f" then
       v-technology = "Fabrication". 
     else if substring(oeel.prodcat,1,1) = "h" then 
       v-technology = "Hydraulic".   
     else if substring(oeel.prodcat,1,1) = "p" then 
       v-technology =  "Pneumatic".   
     else if substring(oeel.prodcat,1,1) = "s" then 
       v-technology =  "Service".     
     else if substring(oeel.prodcat,1,1) = "f" then 
       v-technology =  "Filtration".  
     else if substring(oeel.prodcat,1,1) = "c" then 
       v-technology =  "Connectors".  
     else if substring(oeel.prodcat,1,1) = "m" then 
       v-technology =  "Mobile".      
     else if substring(oeel.prodcat,1,1) = "n" then 
       v-technology =  "None".        
     end.
  else
    assign
       v-technology = catmaster.noteln[2].
    
  if (substring(v-technology,1,4) < b-tech or 
      substring(v-technology,1,4) > e-tech) then 
      next.
  
assign
    r-linecnt    = 0
    r-complete   = 0
    r-incomp     = 0
    r-subs       = 0
    r-promisedt  = ?
    r-requestdt  = ?.
assign
    r2-totamount = 0
    r2-linecnt   = 0
    r2-complete  = 0
    r2-incomp    = 0
    r2-subs      = 0.

    assign
        r-linecnt  = r-linecnt + 1
        r-complete = r-complete +
                       if oeel.stkqtyord <= oeel.stkqtyship and
                           not can-do("s,u", oeel.xrefprodty) then 1
                       else 0
        r-incomp   = r-incomp +
                       if oeel.stkqtyord > oeel.stkqtyship and
                           not can-do("s,u", oeel.xrefprodty) then 1
                       else 0
        r-subs     = r-subs +
                        if can-do("s,u", oeel.xrefprodty) and
                            p-prtsubs = yes then 1
                        else 0.

   if (((oeeh.orderdisp = "j" and oeel.jitpickfl = yes) or 
       oeeh.orderdisp <> "j") and (oeel.qtyord - oeel.qtyship) le 0) then
    do:                            
     assign r2-linecnt  = r2-linecnt + 1. 

     if r-requestdt = ? and oeeh.orderdisp <> "j" then
        r-requestdt =  
        if date(substring(oeel.user3,11,10)) ne ? then 
           date(substring(oeel.user3,11,10))
        else 
        if oeel.reqshipdt ne ? then        
           oeel.reqshipdt
        else
           oeeh.reqshipdt.
     if r-promisedt  = ? and oeeh.orderdisp <> "j" then
        r-promisedt  =
        if date(substring(oeel.user3,1,10)) ne ? then 
           date(substring(oeel.user3,1,10))
        else 
        if oeel.promisedt ne ? then 
           oeel.promisedt
        else
           oeeh.promisedt.

     assign   
        r2-totamount  =  r2-totamount + 
                 ((oeel.price * ((100 - oeel.discpct) / 100)) * 
                    oeel.unitconv)
                  * (if oeel.returnfl = yes then (oeel.qtyship * -1)
                     else oeel.qtyship)
        
        r2-complete = r2-complete +
                       if oeel.stkqtyord <= oeel.stkqtyship and
                           not can-do("s,u", oeel.xrefprodty) then 1
                       else 0
        r2-incomp   = r2-incomp +
                       if oeel.stkqtyord > oeel.stkqtyship and
                           not can-do("s,u", oeel.xrefprodty) then 1
                       else 0
        r2-subs     = r2-subs +
                        if can-do("s,u", oeel.xrefprodty) and
                            p-prtsubs = yes then 1
                        else 0.
    end.  /* if incomplete line or order disp = "j" */

    /*si03 begin - create detail records in temp-table*/           

        /*d calculate margins */
        /* generic program below will get margin amounts like a-oeetlm.i */
         run zsdisellcost.p(input oeel.cono, input oeel.qtyship,
                            input oeel.orderno, input oeel.ordersuf, 
                            input oeel.lineno, 
                            input-output zx-cost,
                            input-output zx-sell).

        s-marginamt = zx-sell - zx-cost.
        if zx-sell ne 0 then 
           s-marginpct = ((zx-sell - zx-cost) / zx-sell * 100).
        else 
           s-marginpct = 0.
        
        if s-marginpct < -100 then 
           s-marginpct = -100.
           
        create t-zzdet.
        assign
            t-zzdet.prttype     = p-datefl 
            t-zzdet.rectype     = "o"
            t-zzdet.whse        = oeel.whse
            t-zzdet.custno      = oeel.custno
            t-zzdet.shipto      = oeel.shipto
            t-zzdet.orderno     = oeel.orderno
            t-zzdet.ordersuf    = oeel.ordersuf
            t-zzdet.lineno      = oeel.lineno
            t-zzdet.transtype   = oeel.transtype
            t-zzdet.expshipdt   = if oeeh.orderdisp <> "j" then   
            if date(substring(oeel.user3,11,10)) ne ? then 
               date(substring(oeel.user3,11,10))
            else 
            if oeel.reqshipdt ne ? then 
               oeel.reqshipdt
            else
              oeeh.reqshipdt
            else ?
            t-zzdet.promisedt   = if oeeh.orderdisp <> "j" then
            if date(substring(oeel.user3,1,10)) ne ? then 
               date(substring(oeel.user3,1,10))
            else 
            if oeel.promisedt ne ? then 
               oeel.promisedt
            else
               oeeh.promisedt
            else ?
            t-zzdet.shipdt      = if avail oeeh then 
                                     oeeh.shipdt
                                  else ?
            t-zzdet.shipprod    = oeel.shipprod
            t-zzdet.qtyord      = oeel.qtyord
            t-zzdet.qtyship     = oeel.qtyship
            t-zzdet.netamt      = oeel.netamt
            t-zzdet.loford      = r-loford
            t-zzdet.marginamt   = s-marginamt
            t-zzdet.marginpct   = s-marginpct
            t-zzdet.region      = if avail smsn then 
                                    substring(smsn.mgr,1,1)
                                  else 
                                    ""  
            t-zzdet.district    = if avail smsn then 
                                    substring(smsn.mgr,2,3)
                                  else 
                                    ""
            t-zzdet.swhse       = oeel.whse
            t-zzdet.sregion     = if avail smsn then 
                                    substring(smsn.mgr,1,1) 
                                  else 
                                    ""
            t-zzdet.slstype     = if avail smsn then 
                                    smsn.slstype
                                  else 
                                    ""
            t-zzdet.repin       = oeel.slsrepin
            t-zzdet.repout      = oeel.slsrepout
            t-zzdet.takenby     = oeeh.takenby
            t-zzdet.prodcat     = oeel.prodcat
            t-zzdet.tech        = substring(v-technology,1,4).

        assign
         p-usedate   = if can-do("b,e",p-datefl) then 
                          t-zzdet.expshipdt
                       else
                          t-zzdet.promisedt
         t-zzdet.usedt = p-usedate.

        assign                   
         t-zzdet.xlate1      = if p-usedate < t-zzdet.shipdt or
                                  p-prttype = "a" then
                                  t-zzdet.loford
                               else 0
         t-zzdet.xlate2      = if t-zzdet.promisedt < t-zzdet.shipdt or
                                  p-prttype = "a" then 
                                  t-zzdet.loford
                               else 0
         t-zzdet.ylate1      = if p-usedate < t-zzdet.shipdt or
                                  p-prttype = "a" then 
                                  1
                               else 0
         t-zzdet.ylate2      = if t-zzdet.promisedt < t-zzdet.shipdt or
                                  p-prttype = "a" then 
                                  1
                               else 0.
     
     if r-linecnt > 0 then 
      do:
         /* invoiced lines -----------------*/   
         assign t-zzdet.totinvamt   = zx-sell
                t-zzdet.linecnt     = r-linecnt
                t-zzdet.complete    = r-complete
                t-zzdet.incomp      = r-incomp
                t-zzdet.subs        = r-subs
         /* completed lines ----------------*/ 
                t-zzdet.z2totinvamt = r2-totamount
                t-zzdet.z2linecnt   = r2-linecnt
                t-zzdet.z2complete  = r2-complete
                t-zzdet.z2incomp    = r2-incomp
                t-zzdet.z2subs      = r2-subs.
        /*---------------------------------*/
      end.

end. /* for each */
end procedure.

/**-------------------------------------------------------------------------**/
procedure print-cust-dtl: 
/**-------------------------------------------------------------------------**/
   xinx = 0.
  for each t-prtdet:
   find t-zzdet where
        recid(t-zzdet) = t-prtdet.prtrecid
        no-lock no-error.
      delete t-prtdet.

      if p-datefl = "b" and ((t-zzdet.xlate1 + t-zzdet.xlate2 + 
                              t-zzdet.ylate1 + t-zzdet.ylate2) = 0) then  
         next.
      else  
      if p-datefl <> "e" and ((t-zzdet.xlate1 + t-zzdet.ylate1) = 0) then
         next.
      else  
      if p-datefl <> "p" and ((t-zzdet.xlate2 + t-zzdet.ylate2) = 0) then
         next.

      zx-lit1 = if t-zzdet.loford > 0 then ">" else "".
      zx-lit2 = if (t-zzdet.qtyord - t-zzdet.qtyship)
                   le 0 then 
                     "<"
                else "".

      zx-lit3 = "".          

      zx-lit3 = 
         if t-zzdet.usedt < t-zzdet.shipdt or
            (t-zzdet.promisedt < t-zzdet.shipdt and
             p-datefl = "b") then 
           "L"                           
         else
           zx-lit3.
   /*   
      zx-lit3 = 
        (if p-datefl = "b" and (t-zzdet.ylate1 + t-zzdet.ylate2) <> 0 then  
           "L"
         else  
         if p-datefl <> "e" and t-zzdet.ylate1 <> 0 then
           "L"
         else  
         if p-datefl <> "p" and t-zzdet.ylate2 <> 0 then
           "L"
         else
           zx-lit3).
   */
                
      if xinx = 0 then
       do:
          display with frame f-blank.
          display with frame f-detailhd.
          xinx = 1.
       end.
           
      display zx-lit1
              t-zzdet.orderno   
              t-zzdet.ordersuf  
              t-zzdet.transtype 
              t-zzdet.expshipdt 
              t-zzdet.promisedt 
              t-zzdet.shipdt    
              t-zzdet.lineno    
              t-zzdet.shipprod  
              t-zzdet.qtyord    
              t-zzdet.qtyship   
              zx-lit2
              zx-lit3
              t-zzdet.netamt    
              t-zzdet.marginamt 
              t-zzdet.marginpct
              with frame f-detail.
              down with frame f-detail.
                            
    end. /* for each */
    hide frame f-detailhd.
    if xinx <> 0 then 
        display with frame f-blank. 
end procedure. 

/**-------------------------------------------------------------------------**/
procedure whse-data: 
/**-------------------------------------------------------------------------**/
/*o**** WAREHOUSE SERVICE LEVEL SECTION ******/
/*d Reset display accumulators */
page.
assign
    s-lookupnm  = ""
    s-whse      = ""
    s-ordcnt    = 0
    s-totvalue  = 0
    s-avgvalue  = 0
    s-linecnt   = 0
    s-complete  = 0
    s-incomp    = 0
    s-late      = 0
    s-lateinc   = 0
    s-subs      = 0
    s-servlevel = 0
    t-text      = ""
    t-cnt       = 0
    t-ordcnt    = 0
    t-totvalue  = 0
    t-linecnt   = 0
    t-complete  = 0
    t-complete2 = 0
    t-incomp    = 0
    t-incomp2   = 0
    t-late      = 0
    t-late2     = 0
    t-lateinc   = 0
    t-lateinc2  = 0
    t-subs      = 0.

/*d Start whse records */
if p-prtwhse <> "n" then do:

    /*tb 21775 03/26/98 cm; Change break by to follow wteh index */
    readl2:
    for each icsd use-index k-icsd no-lock where
        icsd.cono    = g-cono   and
        icsd.whse   >= b-whse   and 
        icsd.whse   <= e-whse,

        each wteh use-index k-fmwhse no-lock where
            wteh.cono        = g-cono     and
            wteh.shipfmwhse  = icsd.whse  and
            wteh.shiptowhse >= b-shipwhse and 
            wteh.shiptowhse <= e-shipwhse and
            wteh.shipdt     >= b-date     and 
            wteh.shipdt     <= e-date     and
           (wteh.transtype   = "wt"        or
            wteh.transtype   = "do"       and 
            p-prtdirect      = yes)       and
           (wteh.wtsuf       = 00          or
           (wteh.wtsuf      <> 0          and 
            p-prtbo          = yes)),

        each wtel use-index k-wtel no-lock where
            wtel.cono        = g-cono          and
            wtel.wtno        = wteh.wtno       and
            wtel.wtsuf       = wteh.wtsuf      and
           (wtel.nonstockty  = ""               or
           (wtel.nonstockty  = "s"             and 
            p-prtspecial     = yes))           and
           (wtel.nonstockty  = ""               or
           (wtel.nonstockty  = "n"             and 
            p-prtnonstk      = yes))           and
           (not can-do("s,u", wtel.xrefprodty)  or
           (can-do("s,u", wtel.xrefprodty)     and 
            p-prtsubs        = yes))
    break by wteh.cono
          by wteh.shipfmwhse
          by wteh.wtno:

    
      /* Range Technology Check  */
       find catmaster where catmaster.cono = 1 and 
                            catmaster.notestype = "zz" and
                            catmaster.primarykey  = "apsva" and
                            catmaster.secondarykey = 
                            (substring(wtel.prodcati,1,4))
                            no-lock no-error. 
       if not avail catmaster then
        do:
          if substring(wtel.prodcati,1,1) = "a" then 
            v-technology = "Automation".
          else if substring(wtel.prodcati,1,1) = "f" then
            v-technology = "Fabrication". 
          else if substring(wtel.prodcati,1,1) = "h" then 
            v-technology = "Hydraulic".   
          else if substring(wtel.prodcati,1,1) = "p" then 
            v-technology =  "Pneumatic".   
          else if substring(wtel.prodcati,1,1) = "s" then 
            v-technology =  "Service".     
          else if substring(wtel.prodcati,1,1) = "f" then 
            v-technology =  "Filtration".  
          else if substring(wtel.prodcati,1,1) = "c" then 
            v-technology =  "Connectors".  
          else if substring(wtel.prodcati,1,1) = "m" then 
            v-technology =  "Mobile".      
          else if substring(wtel.prodcati,1,1) = "n" then 
            v-technology =  "None".        
          end.
       else
         assign
            v-technology = catmaster.noteln[2].
    
       if (substring(v-technology,1,4) < b-tech or 
           substring(v-technology,1,4) > e-tech) then 
           next.
        
       if first-of(wteh.wtno) then
          assign s-ordcnt   = s-ordcnt + 1
                 s-totvalue = s-totvalue + wteh.totshipamt +
                              wteh.addonnet[1] + wteh.addonnet[2].

       assign
            s-linecnt  = s-linecnt + 1
            s-complete = s-complete + if wteh.reqshipdt >= wteh.shipdt and
                                          wtel.stkqtyord <= wtel.stkqtyship and
                                          not can-do("s,u", wtel.xrefprodty)
                                          then 1 else 0
            s-incomp   = s-incomp + if wteh.reqshipdt >= wteh.shipdt and
                                        wtel.stkqtyord > wtel.stkqtyship and
                                        not can-do("s,u", wtel.xrefprodty)
                                        then 1 else 0
            s-late     = s-late + if wteh.reqshipdt < wteh.shipdt and
                                      wtel.stkqtyord <= wtel.stkqtyship and
                                      not can-do("s,u", wtel.xrefprodty)
                                      then 1 else 0
            s-lateinc  = s-lateinc + if wteh.reqshipdt < wteh.shipdt and
                                         wtel.stkqtyord > wtel.stkqtyship and
                                         not can-do("s,u", wtel.xrefprodty)
                                         then 1 else 0
            s-subs     = s-subs + if can-do("s,u", wtel.xrefprodty)
                                      then 1 else 0.

        if last-of(wteh.shipfmwhse) then do:

            assign
                s-whse      = wteh.shipfmwhse
                s-lookupnm  = if avail icsd then substring(icsd.name,1,15)
                              else if wteh.shipfmwhse = "" then ""
                              else v-invalid
                s-servlevel = if s-linecnt = 0 then 0
                              else round(((s-complete / s-linecnt) * 100),2)
                s-avgvalue  = if s-ordcnt = 0 then 0
                              else (s-totvalue / s-ordcnt).

            if p-prtserv <> 0 then do:
            
                if s-servlevel >= p-prtserv then do:
                
                    assign
                        s-custno   = ""     
                        s-lookupnm = ""
                        s-ordcnt   = 0      
                        s-totvalue = 0
                        s-linecnt  = 0
                        s-complete = 0      
                        s-incomp   = 0
                        s-late     = 0      
                        s-lateinc  = 0
                        s-subs     = 0.
                        
                    next readl2.
                
                end. /* if s-servlevel >= p-prtserv */
                
            end. /* if p-prtserv <> 0 */

            if t-cnt = 0 and p-prtwhse = "d" then do:
                
                display skip with frame f-blank.
                display with frame f-smrl2.
                display with frame f-under.
            
            end. /* if t-cnt = 0 and .... */

            if p-prtwhse = "d" then do:
            
                display 
                    s-whse              
                    s-lookupnm
                    s-ordcnt
                    s-totvalue          
                    s-avgvalue
                    s-linecnt           
                    s-complete
                    s-incomp            
                    s-late
                    s-lateinc           
                    s-subs
                    s-servlevel
                with frame f-smrl4.
                down with frame f-smrl4.
            
            end. /* if p-prtwhse = d */

            assign
                t-cnt       = t-cnt      + 1
                t-ordcnt    = t-ordcnt   + s-ordcnt
                t-totvalue  = t-totvalue + s-totvalue
                t-linecnt   = t-linecnt  + s-linecnt
                t-complete  = t-complete + s-complete
                t-incomp    = t-incomp   + s-incomp
                t-late      = t-late     + s-late
                t-lateinc   = t-lateinc  + s-lateinc
                t-subs      = t-subs     + s-subs
                s-whse      = ""     
                s-lookupnm  = ""
                s-ordcnt    = 0      
                s-totvalue  = 0
                s-linecnt   = 0
                s-complete  = 0      
                s-incomp    = 0
                s-late      = 0      
                s-lateinc   = 0
                s-subs      = 0
                t-transcnt  = t-transcnt + 1
                t-transcnt2 = t-transcnt2 + 1.

        end. /* if last-of(wteh.shipfmwhse) */
    
    end. /* for each icsd */

    /*d Warehouse totals */
    if t-transcnt2 <> 0 then do:
    
        /*d Total lines */
        if p-prtwhse = "d" then
            display with frame f-smrl5.   
        else
        if p-prtwhse = "s" then
         do:
            display with frame f-blank.
            display with frame f-smrl9.
            display with frame f-under.
         end. /* else if can-do(p-prtcust) */

        assign
            s-avgvalue  = if t-ordcnt = 0 then 0
                          else (t-totvalue / t-ordcnt)
            s-servlevel = round(((t-complete / t-linecnt) * 100),2)
            t-text      = "Total Warehouses:".

        if p-prtwhse <> 's' then display with frame f-smrl5.

        display 
            t-text
            t-cnt           
            t-ordcnt
            t-totvalue      
            s-avgvalue
            t-linecnt
            t-complete      
            t-incomp
            t-late          
            t-lateinc
            t-subs
            s-servlevel
        with frame f-smrl6wt.

        if p-prtserv <> 0 then display s-servmsg with frame f-smrlq.

    end. /* if t-transcnt2 <> 0 */

end. /* if p-prtwhse <> n */
end procedure.

/{&user_procedures}*/

