/* u-smsn.i 1.1 01/03/98 */
/* u-smsn.i 1.1 2/16/92 */
/*  u-smsn.i  */
/*
    02/13/02 lbr; TB#e12226 Added ability to enter salesman email
    10/29/02 mtt; TB# e15115 Set up events and triggers
    09/09/04 bpa; TB# e20045 Add Sync To CRM flag using smsn.xxl1
*************************************************************************/

    smsn.name
    smsn.addr
    smsn.city
    smsn.state
    smsn.zipcd
    smsn.slstitle
    smsn.phoneno
    smsn.email

    smsn.mgr  
    smsn.slstype
    smsn.commfl
    smsn.commtype
    smsn.phonesuf

    smsn.geocd   {&com} when v-modvtfl = yes and v-geoindxty = "t":u /{&com}* */
    smsn.lettercd[1] validate
        ({v-cmsl.i "input smsn.lettercd[1]"},
        "Paragraph Not Set Up In Letter/Paragraph Setup - CMSL(4707)")
    smsn.lettercd[2] validate
        ({v-cmsl.i "input smsn.lettercd[2]"},
        "Paragraph Not Set Up In Letter/Paragraph Setup - CMSL(4707)")
    smsn.lettercd[3] validate
        ({v-cmsl.i "input smsn.lettercd[3]"},
        "Paragraph Not Set Up In Letter/Paragraph Setup - CMSL(4707)")
    smsn.lettercd[4] validate
        ({v-cmsl.i "input smsn.lettercd[4]"},
        "Paragraph Not Set Up In Letter/Paragraph Setup - CMSL(4707)")
    smsn.lettercd[5] validate
        ({v-cmsl.i "input smsn.lettercd[5]"},
        "Paragraph Not Set Up In Letter/Paragraph Setup - CMSL(4707)")
    smsn.lettercd[6] validate
        ({v-cmsl.i "input smsn.lettercd[6]"},
        "Paragraph Not Set Up In Letter/Paragraph Setup - CMSL(4707)")
    smsn.letterdir
    smsn.securefl
    smsn.oper2
    smsn.synccrmfl
    
    smsn.site
    smsn.sysname
    smsn.modphoneno
    smsn.beglastdt
    smsn.endlastdt
    smsn.synccrmfl

    smsn.begprosno
    smsn.endprosno
    
    /* dkt */
    smsn.user1
