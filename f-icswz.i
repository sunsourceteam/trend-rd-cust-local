
   icsw.statustype           colon 16
   s-statustype              at    21 no-label
   icsw.baseprice            colon 16
   icsw.listprice            colon 16
   icsw.replcost             colon 16 label "Repl Cost"
   icsw.pricetype            colon 16
   s-pricetype               at    23 no-label
   skip(1)
   icsw.arptype              colon 16 label "ARP"
   icsw.arpvendno            colon 16
   icsw.prodline             colon 16
   s-linenm                  at 25 no-label
   skip(1)
   icsw.rebatety             colon 16
   skip(1)
   icsw.leadtmavg            colon 16
   skip(4)
   
 with frame f-icswz side-labels row 4 width 80 overlay
   
