/***************************************************************************
 PROGRAM NAME: poetle-ack.p
  PROJECT NO.: 99-16
  DESCRIPTION: This program is the screen entry program for the
                acknowledgement data.
               This program is run from poetle.z99 
 DATE WRITTEN: 02/10/00
       AUTHOR: Sir Carl E. Graul Esq.
     MODIFIED: 08/09/00 - ceg - changed verbage for s-ackcd-v

***************************************************************************/

{sts-po-acknow1.i "shared"}

def var sw-chged as logical init no.
def var sw-error as logical init no.

assign sw-poetle = yes.
run "100-disp".
run "200-upd".
run "300-values".
hide frame f-acknow1.

/* if {k-cancel.i}  then 
   lastkey = 301.     */

/* ***** eoj stuff ***** */


/* ############################ end of program ######################### */ 

procedure 100-disp:
display 
    sw-ack
    s-ackcd
    s-ackcd-v
    s-chgdt
    s-track-no
    s-ackno
    with frame f-acknow1.
end procedure. /* 100-disp */
    
procedure 200-upd:    
update 
    sw-ack
    s-ackcd
    s-track-no
    s-ackno
    with frame f-acknow1
    editing:
    readkey.
    assign sw-error = no.
    if {k-accept.i} then 
      do:  
        if s-ackcd = "n" and oc-ackcd ="r" then
         do:
            assign s-ackcd = oc-ackcd.
            display s-ackcd
                with frame f-acknow1.
            message "The P/O has already been received".
            assign sw-error = yes.
         end. /* if s-ackcd = "r" and oc-ackcd ="n" */
        else
        if oc-ackcd = "a" and (input s-ackcd ne "a"
                            or input sw-ack = no) then
         do:
            assign s-ackcd = oc-ackcd
                   sw-ack = yes.
            display s-ackcd
               with frame f-acknow1.
         message "Line has already acknowledged".      
            sw-error = yes.
         end. /* if oc-ackcd = "a" and (s-ackno = "r" or s-ackcd = "n") */
        if sw-error = yes then
            do:
              next-prompt s-ackcd. 
              next.
            end.  
        else 
            leave.
      end.
    /* editing acknowledge line */
    if frame-field = "sw-ack" then do:
        apply lastkey.
        assign sw-ack.
        next-prompt s-ackcd.
        next.
    end. /* if frame-field = "sw-ack" */
    
    /* editing s-ackcd */
    if frame-field = "s-ackcd" then do:
        apply lastkey.
        assign s-ackcd.        

        if lookup(s-ackcd,"n,r,a") = 0 then do:
            assign s-ackcd = oc-ackcd.
            display s-ackcd
                with frame f-acknow1.
            message "Only 'n', 'r', or 'a' is allowed".    
            assign sw-error = yes.    
        end. /* if s-ackcd <> "n" or s-ackcd <> "r" or s-ackcd <> "a" */

        if s-ackcd = "n" and oc-ackcd ="r"   then do:
            assign s-ackcd = oc-ackcd.
            display s-ackcd
                with frame f-acknow1.
            message "The P/O has already been received".
            assign sw-error = yes.
        end. /* if s-ackcd = "r" and oc-ackcd ="n" */

        if oc-ackcd = "a" and lookup(s-ackno,"a") = 0  then do:
            assign s-ackcd = oc-ackcd.
            display s-ackcd
               with frame f-acknow1.
         message "Line has already acknowledged".      
            sw-error = yes.
        end. /* if oc-ackcd = "a" and (s-ackno = "r" or s-ackcd = "n") */
        if sw-error = yes then
            next-prompt s-ackcd.
            next.
        if s-ackcd = "a" then 
            assign s-ackcd-v = "Acknowledged".
        if s-ackcd = "r" then
            assign s-ackcd-v = "Received".
        if s-ackcd = "n" then
            assign s-ackcd-v = "Not Ackged".
        display s-ackcd-v with frame f-acknow1.
        next-prompt s-track-no.
    end. /* if frame-field = "s-ackcd" */
    
    /* editing s-track-no */
    if frame-field = "s-track-no" then do:
        apply lastkey.
        assign s-track-no.
        next-prompt  s-ackno.
        next.
    end. /* if frame-field = "s-trackno" */
    
    /* editing s-ackno */
    if frame-field = "s-ackno" then do:
        apply lastkey.
        assign s-ackno.
        if keyfunction(lastkey) = "go" then leave.
        
    end. /* if frame-field = "s-ackno" */
/*    next. */
    end. /* editing */
/*  end.   */
end procedure. /* 200-upd */

 
procedure 300-values:
assign sw-chged = no.
if sw-ack = yes then
    assign sw-chged = yes.
if s-ackcd <> oc-ackcd then
    assign sw-chged = yes.
if s-track-no <> oc-track-no then
    assign sw-chged = yes.
if s-ackno <> oc-ackno then
    assign sw-chged = yes.
if sw-chged = no then
    leave.
assign s-ackcd = "a"
       s-chgdt = today
       s-ackuser = cur-user.
end procedure. /* 300-values */

