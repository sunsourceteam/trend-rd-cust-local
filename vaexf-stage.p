/* ---------------------------------------------------------------------------

    Program ID: vaexq-stage.p
    Purpose:    Move stages in the rules of the t-stages table and
                update ztmk table with the appropriate inforamtion.
    Parameters: Input ip-cono    - Company id
                      ip-quoteno - Quote being worked on
                      ip-stagefm - Current Stage
                      ip-stageto - If blank next stage is used
                      ip-operator - Operator marked in ztmk
                output
                      ip-stagecd - Stage moved to 
                      ip-errorcd - Error code if necessary
                      ip-message - Error message

--------------------------------------------------------------------------- */
Define Input    Parameter ip-cono     like icsw.cono             no-undo.
Define Input    Parameter ip-quoteno  like zsdivasp.repairno     no-undo.
Define Input    Parameter ip-stagefm  as char                    no-undo.
Define Input    Parameter ip-stageto  as char                    no-undo.
Define Input    Parameter ip-operator like sasoo.oper2            no-undo.
Define Input    Parameter ip-secure   as integer                 no-undo.

Define Output   Parameter ip-stagecd  as integer                 no-undo.
Define Output   Parameter ip-stagenm  as char                    no-undo.
Define Output   Parameter ip-errorcd  as integer                 no-undo.
Define Output   Parameter ip-message  as char format "x(50)"     no-undo.



{g-all.i}
{g-fabstg.i}
define var v-upstg      as integer          no-undo.
define var v-downstg    as integer          no-undo.
define var v-privupstg      as integer          no-undo.
define var v-privdownstg    as integer          no-undo.

define var v-cancelled  as integer          no-undo.
define var v-closed     as integer          no-undo.
define var v-stageto    as integer          no-undo.
define var v-stagefm    as integer          no-undo.

/* Load stages Temp table */

{p-fabstg.i}

for each t-stages no-lock:
  if t-stages.shortname = "CAN" then
     v-cancelled = t-stages.stage.
  if t-stages.shortname = "CLS" then
     v-closed = t-stages.stage.
end.


if ip-stagefm = "" then return.

if ip-stagefm <> "" then do:
  find t-stages where t-stages.shortname = ip-stagefm no-lock no-error.
  if not avail t-stages then do:
    assign ip-errorcd = 1
           ip-message = "(fstg:1) Stage is not valid - " +
                         ip-stagefm.
    return.
  end.
  else
    assign v-stagefm = t-stages.stage
           v-upstg    = t-stages.upstg  
           v-downstg  = t-stages.dwnstg
           v-privupstg = t-stages.privupstg
           v-privdownstg = t-stages.privdwnstg.


end.                         

if ip-stageto <> "" then do:
  find t-stages where t-stages.shortname = ip-stageto no-lock no-error.
  if not avail t-stages then do:
    assign ip-errorcd = 1
           ip-message = "(fstg:1) Stage is not valid - " +
                         ip-stageto.
  end.
  else
    assign v-stageto = t-stages.stage.

end. 
else
if ip-stageto = "NXT" then do:
  assign v-stageto = v-upstg.                        
  find t-stages where t-stages.stage = v-stageto no-lock no-error.
  if avail t-stages then
    assign ip-stageto = t-stages.shortname.
end.
else
if ip-stageto = "" then do:
  assign v-stageto = v-stagefm.                        
  find t-stages where t-stages.stage = v-stageto no-lock no-error.
  if avail t-stages then
    assign ip-stageto = "NON"
           v-stageto = 0.
end.



if v-stagefm <> 0 and v-stageto <> 0 then do:
  find t-stages where t-stages.stage = v-stagefm no-lock no-error.
  if not avail t-stages then do:
    assign ip-errorcd = 2
           ip-message = "(fstg:2) Stage is not valid to close " +
                         ip-stagefm
           v-upstg    = t-stages.upstg  
           v-downstg  = t-stages.dwnstg
           v-privupstg = t-stages.privupstg
           v-privdownstg = t-stages.privdwnstg.

  end.

  find t-stages where t-stages.stage = v-stageto no-lock no-error.
  if not avail t-stages then do:
    assign ip-errorcd = 3
           ip-message = "(fstg:3) Stage is not valid cannot move to " +
                         ip-stageto.
    return.
  end.
  else if v-stageto > v-stagefm then do:

    if v-stagefm ge v-closed then do:
      assign ip-errorcd = 5
             ip-message = "(fstg:5) Cannot modify Quote in this stage " +
                         ip-stagefm.
      return.
    end.                     
    else     
    if ip-secure = 5 and
       v-stageto > v-privupstg and 
       v-stageto <> v-cancelled and  
       v-stageto <> v-closed then do:
      assign ip-errorcd = 4
             ip-message = "(fstg:4) Current stage " +
                         ip-stagefm + " not authorized to move to " +
                         ip-stageto.
      return.                   
    end.
    else
    if ip-secure = 5 and v-stagefm < v-closed then do:
      run punch-out (input v-stagefm).
      run punch-in  (input v-stageto).
    end.
    else
    if v-stageto > v-upstg and 
       v-stageto <> v-cancelled and  
       v-stageto <> v-closed then do:
      assign ip-errorcd = 4
             ip-message = "(fstg:4) Current stage " +
                         ip-stagefm + " not authorized to move to " +
                         ip-stageto.
      return.                   
    end.
    else do:
      run punch-out (input v-stagefm).
      run punch-in  (input v-stageto).
    end.
  end.  /* v-stageto > v-stagefm */
  else if v-stageto < v-stagefm then do:

    if v-stagefm ge v-closed then do:
      assign ip-errorcd = 5
             ip-message = "(fstg:5) Cannot modify Quote in this stage " +
                         ip-stagefm.
      return.
    end.                     
    else     
    if ip-secure = 5 and
       v-stageto < v-privdownstg and 
       v-stageto <> v-cancelled and  
       v-stageto <> v-closed then do:
      assign ip-errorcd = 4
             ip-message = "(fstg:4) Current stage " +
                         ip-stagefm + " not authorized to move to " +
                         ip-stageto.
      return. 
    end.                    
    else
    
    if ip-secure = 5 and v-stagefm < v-closed then do:
      run punch-out (input v-stagefm).
      run punch-in  (input v-stageto).
    end.
    else
    if v-stageto < v-downstg and 
       v-stageto <> v-cancelled and  
       v-stageto <> v-closed then do:
      assign ip-errorcd = 4
             ip-message = "(fstg:4) Current stage " +
                         ip-stagefm + " not authorized to move to " +
                         ip-stageto.
      return. 
    end.                    
    else do:
      run punch-out (input v-stagefm).
      run punch-in  (input v-stageto).
    end.
  end.
end. /* v-stagefm <> 0 and v-stageto <> 0  */
else
if v-stagefm <> 0 and ip-stageto = "NON" then do:
  find t-stages where t-stages.stage = v-stagefm no-lock no-error.
  if not avail t-stages then do:
    assign ip-errorcd = 2
           ip-message = "(fstg:2) Stage is not valid to close " +
                         ip-stagefm
           v-upstg    = t-stages.upstg  
           v-downstg  = t-stages.dwnstg
           v-privupstg = t-stages.privupstg
           v-privdownstg = t-stages.privdwnstg.


  end.
  
  assign v-stageto = v-stagefm.
  
  if ip-secure = 5 and v-stagefm < v-closed then do:
    run punch-out (input v-stagefm).
  end.
  else
  if ip-secure < 3 then do:
    assign ip-errorcd = 4
           ip-message = "(fstg:4) Current stage " +
                       ip-stagefm + " not authorized to move to " +
                       ip-stageto.
    return.                   
  end.  
  else do:
      run punch-out (input v-stagefm).

  end.

end. /* v-stagefm <> 0 and v-stageto <> 0  */
 

  
  
  
  
/* ------------------------------------------------------------------------- */
Procedure punch-out:
/* ------------------------------------------------------------------------- */
  define input parameter ipo-stagefm as integer no-undo.     

  find ztmk USE-INDEX k-ztmk-sc2 WHERE
       ztmk.cono      = ip-cono    AND
       ztmk.ordertype = "fb"      AND
       ztmk.user1     = ip-quoteno AND
       ztmk.statuscd  = 0 no-error.
  
  if avail ztmk then do:
    ASSIGN 
      ip-stagecd      = ztmk.stagecd
      ip-stagenm      = ip-stagefm
      ztmk.operinit   = ip-operator
      ztmk.punchoutdt = TODAY
      ztmk.statuscd   = 5
      ztmk.punchouttm = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                        SUBSTRING(STRING(TIME,"hh:mm"),4,2) +
                        SUBSTRING(STRING(TIME,"hh:mm:ss"),7,2).
  END.
END.

    
  
/* ------------------------------------------------------------------------- */
Procedure punch-in:
/* ------------------------------------------------------------------------- */
  define input parameter ipo-stageto as integer no-undo.     
     
  ASSIGN ip-stagecd = ipo-stageto.
  CREATE ztmk.
  ASSIGN 
    ztmk.jobseqno  = 0
    ztmk.cono      = ip-cono
    ztmk.ordertype = "fb"
    ztmk.user1     = ip-quoteno
    ztmk.stagecd   = ipo-stageto
    ztmk.statuscd  = if ipo-stageto = v-cancelled then
                       5
                     else
                       0
    ztmk.techid    = ip-operator
    ztmk.punchindt = TODAY
    ztmk.punchintm = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                     SUBSTRING(STRING(TIME,"hh:mm"),4,2) +
                     SUBSTRING(STRING(TIME,"hh:mm:ss"),7,2)
    ztmk.operinit  = ip-operator
    ztmk.transtm   = IF ztmk.transtm = "" THEN
                       SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                       SUBSTRING(STRING(TIME,"hh:mm"),4,2)
                     ELSE
                       ztmk.transtm
    ztmk.transdt   = IF ztmk.transdt = ? THEN
                       TODAY
                     ELSE
                       ztmk.transdt.
 
END.

