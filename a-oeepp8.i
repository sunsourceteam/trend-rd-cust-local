/* a-oeepp8.i 04/23/97 */
/*si**** Custom ****************************************************************
  CLIENT  : 80281 SDI
  JOB     : SDI008 Custom Print Packing List
  AUTHOR  : jsb
  DATE    : 04/23/97
  VERSION : 7.0.003
  PURPOSE : Run Packing List from PMEP and OEES
  CHANGES :
    SI01 04/23/97 jsb; Original Changes                  
    si02 08/29/97 jsb; SDI00801
*******************************************************************************/
/* a-oeepp1.i 1.4 8/10/92 */
/*h*****************************************************************************
  INCLUDE      : a-oeepp1.i
  DESCRIPTION  : OE print
  USED ONCE?   : no
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    08/03/92 mkb; TB# 7326 Remove amount(net) field
    07/30/98 dbf; IT#    3 Do not print colum headers for totals if print flag
                           set to no on order header
*******************************************************************************/

/*d Assign literal values for labels on pick ticket */
if {2} = yes then assign
    {1}lit1a  = "Document:"
    {1}lit1b  = /*"UPC Vendor  " + if sapb.currproc = "wtep" then "Transfer #"
                 else si01 */ "   Order #"
    {1}lit3a  = if sapb.currproc ne "wtep" then
                "Cust #:" else ""
    {1}lit3b  = (if sapb.currproc = "wtep" then "Order Dt     "
                else "PO Date   PO #") +
                "    Page #"  /*si02*/
    {1}lit6a  = "B"  /*si01*/
    {1}lit6b  = "I"  /*si01*/
    {1}lit6c  = "L"  /*si01*/
    {1}lit6d  = "L"  /*si01*/
    {1}lit11a = "S"  /*si01*/
    {1}lit11d = "H"  /*si01*/
    {1}lit11e = "I"  /*si01*/
    {1}lit11f = "P"  /*si01*/
    {1}lit11b = "Instructions"
/*    {1}lit11c = "Staging Area"
    {1}lit12a = "Ship Point"     si02*/
    {1}lit12b = "Via"
    {1}lit12c = "Shipped"
/*    {1}lit12d = "Terms"  si01*/
    {1}lit12e = "Request"
    /*si01*/
    {1}lit14a =
"Line         Product            Quantity   Quantity   Quantity" /*si02*/
    {1}lit14b = /* "  Qty" +  si02*/
                /* dbf IT#3 beg */
                /*
                if g-ourproc <> "wtep" then  "   Qty    Amount"
                else ""
                */
                if g-ourproc = "wtep" then  ""
                else 
                   if oeeh.pickprtfl = true then "   Qty    Amount"
                   else "   Qty          "
                /* dbf IT#3 end */
    {1}lit15a =
" #       And Description         Ordered     B.O.      Shipped"  /*si02*/
    {1}lit15b = /* "  UOM" +  si02*/
                /* dbf IT#3 beg */
                /*
                if g-ourproc <> "wtep" then "   UOM     (Net)"
                else ""
                */
                if g-ourproc = "wtep" then ""
                else
                   if oeeh.pickprtfl = true then "   UOM     (Net)"
                   else "   UOM          "
                /* dbf IT#3 end */
    {1}lit16a =
"------------------------------------------------------------------------------"
/*    {1}lit16b =
"------------------"  si02*/
    {1}lit40a = "Lines Total"
    {1}lit40b = " ** # of Lines Not Printed "
    {1}lit40c = " Qty Shipped Total"
/*    {1}lit40d = "Total __________ _____"  si01*/
    {1}lit41a =
"Picked By:   Packed By:    Checked By:     Pick Time:    TakenBy:    Freight:"
/*si02*/
/*    {1}lit41b = "t Charges:"  si02*/
    {1}lit42a = "Continued"
/*    {1}lit42b = "Received By:       Date Received:"   si01*/ .
