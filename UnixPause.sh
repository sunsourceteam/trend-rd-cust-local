#!/bin/sh
old_tty_setting=$(stty -g)   # Save old terminal setting.
stty -icanon -echo -iexten -noflsh -isig
                      # Disable "canonical" mode for terminal.
                      # Also, disable *local* echo.
		      # Also, disable signal for interrupt
key=$(dd bs=1 count=1 conv=swab,noerror 2> /dev/null)   
			     # Using 'dd' to get a keypress.
stty "$old_tty_setting"      # Restore old setting. 
