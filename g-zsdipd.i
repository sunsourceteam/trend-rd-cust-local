{&combufferpdsc}
def new shared buffer b-pdsc for pdsc.
/{&combufferpdsc}* */

{&combuffericsp}
def {&new} shared buffer b-icsp for icsp.
/{&combuffericsp}* */

{&combuffers}
def {&new} shared buffer b-icsc for icsc.
def {&new} shared buffer b-oeel for oeel.
def {&new} shared buffer b-icss for icss.
/{&combuffers}* */

/* Primary variables defined for this include (won't be commented out) */
def {&new} shared var v-lastlevel as i no-undo.

/* The following variables are defined in g-oeetx.i */
{&comset1}
def {&new} shared var v-pricetype like arsc.pricetype no-undo.
def {&new} shared var v-pricecd like arsc.pricecd no-undo.
def {&new} shared var v-disccd like arsc.disccd no-undo.
def {&new} shared var v-pdsecure as logical extent 44 no-undo.
def {&new} shared var v-termslinefl like oeeh.termslinefl no-undo.
def {&new} shared var v-termspct like oeeh.termspct no-undo.
/{&comset1}* */

/* The following variables are defined in g-oeetl.i */
{&comset2}
def {&new} shared var s-lineno like oeel.lineno no-undo.
def {&new} shared var s-cost like oeel.prodcost no-undo.
def {&new} shared var s-kitfl like oeel.kitfl no-undo.

/* def {&new} shared var s-tallyfl like oeel.tallyfl no-undo. */

def {&new} shared var v-costoverfl like oeel.costoverfl no-undo.
def {&new} shared var v-prodcost as dec format ">>>>>>9.999999" no-undo.
def {&new} shared var v-idoeel as recid no-undo.
def {&new} shared var v-manprice as logical no-undo.
def {&new} shared var v-unit like icsp.unitstock no-undo.
/{&comset2}* */

/* The following variables are defined in g-oeet.i */
{&comset3}
def {&newao} shared var v-pdwhsefl like sasc.pdwhsefl no-undo.
def {&newao} shared var v-oepricefl like sasoo.oepricefl no-undo.
def {&newao} shared var v-pdjobfl like sasc.pdjobfl no-undo.
/{&comset3}* */

/* The following variables are defined in g-oeetx.i (separate set) */
{&comset4}
def {&newao} shared var v-iccosttype like sasc.iccosttype no-undo.
    /* SX30   SunSource  

def {&newao} shared var v-icglcost like sasc.icsmglcost no-undo.
       SX30   SunSource  */

def {&newao} shared var v-icincaddcp like sasc.icincaddcp no-undo.
def {&newao} shared var v-icincaddsm like sasc.icincaddsm no-undo.
/*tb 19724 05/06/99 W^3; Added var for sm/gl cost split. */
def {&newao} shared var v-icincaddgl like sasc.icincaddgl no-undo.
def {&newao} shared var v-pdlevelpl like sasc.pdlevelpl no-undo.
def {&newao} shared var v-pdlevelpc like sasc.pdlevelpc no-undo.
/*tb 19724 05/06/99 jbs; Added var for sm/gl cost split. */
def shared var v-icsmcost like sasc.icsmcost no-undo.
/{&comset4}* */

/* The following variables are defined in g-oeetl.i */
{&comset5}
def {&new} shared var s-unit like icsp.unitsell no-undo.
def {&new} shared var s-qtyord like oeel.qtyord no-undo.
def {&new} shared var s-prodcat like icsp.prodcat no-undo.
def {&new} shared var s-prod like oeel.shipprod no-undo.
def {&new} shared var s-specnstype like oeel.specnstype no-undo.
def {&new} shared var s-termspct like oeel.termspct no-undo.
def {&new} shared var s-jobno like pdsc.jobno no-undo.
def {&new} shared var s-commtype like pdsc.commtype no-undo.

def {&new} shared var v-conv as decimal no-undo.
def {&new} shared var v-stkqtyord like oeel.stkqtyship no-undo.
def {&new} shared var v-stkunit like icsp.unitstock no-undo.
def {&new} shared var v-maint-l as c format "x" no-undo.
def {&new} shared var v-errfl as c format "x" no-undo.
def {&new} shared var v-idicsp as recid no-undo.
def {&new} shared var v-linefl like oeeh.linefl no-undo.
/{&comset5}* */

/* The following variables are defined in g-oeetl.i */
{&comset6}
def {&new} shared var s-pdcost like oeel.prodcost no-undo.
def {&new} shared var s-price like oeel.price no-undo.
def {&new} shared var v-actqty like pdsc.actqty no-undo.
def {&new} shared var v-cataddfl as l no-undo.
/{&comset6}* */

/* The following variables are defined in g-oeetlo.i */
{&comset7}
def {&new} shared var o-stkqtyord like oeel.stkqtyord no-undo.
def {&new} shared var o-totqtyord like oeel.stkqtyord no-undo.
def {&new} shared var o-pdrecno like oeel.pdrecno init 0 no-undo.
/{&comset7}* */

/* The following variables are defined in g-oeetl.i */
{&comset8}
def {&new} shared var s-disccd like oeel.disccd no-undo.
def {&new} shared var s-disctype like oeel.disctype no-undo.
def {&new} shared var s-discamt like oeel.discamt no-undo.
def {&new} shared var s-priceclty like oeel.priceclty no-undo.
def {&new} shared var s-pricecd like oeel.pricecd no-undo.
def {&new} shared var s-prvendno like icsw.arpvendno no-undo.
def {&new} shared var s-prline like icsw.prodline no-undo.
def {&new} shared var s-qtybreakty like pdsc.qtybreakty no-undo.
def {&new} shared var s-kitrollty like icsp.kitrollty no-undo.
def {&new} shared var s-pricelevel like arsc.pricecd no-undo.

def {&new} shared var v-baseprice like icsw.baseprice no-undo.
def {&new} shared var v-listprice like icsw.listprice no-undo.
def {&new} shared var v-pdcost like oeel.prodcost no-undo.
def {&new} shared var v-pdrecno like pdsc.pdrecno no-undo.
def {&new} shared var v-priceonty like pdsc.priceonty no-undo.
def {&new} shared var v-prodtype like icsw.pricetype no-undo.
def {&new} shared var v-promofl like oeel.promofl no-undo.
def {&new} shared var v-qtytype like pdsc.qtytype no-undo.
def {&new} shared var v-price like oeel.price no-undo.
def {&new} shared var v-idpdsc as recid no-undo.
def {&new} shared var v-kitcostfl as l no-undo.
def {&new} shared var v-priceroll as l no-undo.
/{&comset8}* */

{&comlocalvars}
def var v-badrec as logical no-undo.
def var v-blankunit like icsp.unitstock initial "" no-undo.
def var v-hit as integer no-undo.
def var v-hita as c format "x(2)" no-undo.
def var v-levelcd like pdsc.levelcd no-undo.
def var v-pridpdsc as recid no-undo.
def var v-promodisc like oeel.discamt no-undo.
def var v-promofound as logical initial "no" no-undo.
def var v-promoprice like oeel.price no-undo.
def var v-prpdrecno like pdsc.pdrecno no-undo.
def var v-ptarget like pdsc.ptarget no-undo.
def var v-qtyyymm like pdsc.qtyyymm no-undo.
def var v-rprice as dec format ">>>9.99999999" no-undo.
def var v-sellunit like icsp.unitstock no-undo.
/{&comlocalvars}* */



