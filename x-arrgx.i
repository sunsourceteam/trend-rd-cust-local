
/{&user_temptable}*/

{g-ar.i}



/*   Variables from screen */
def  var b-creditmgr      like arsc.creditmgr              no-undo.
def  var e-creditmgr      like arsc.creditmgr              no-undo.
def  var b-region         as character format "x(1)"       no-undo.
def  var e-region         as character format "x(1)"       no-undo.  
def  var b-district       as character format "x(3)"       no-undo.
def  var e-district       as character format "x(3)"       no-undo.
def  var b-slsrep         like smsn.slsrep                 no-undo.
def  var e-slsrep         like smsn.slsrep                 no-undo.
def  var b-divno          as integer format ">>9"          no-undo.
def  var e-divno          as integer format ">>9"          no-undo.
def  var b-custno         as dec     format ">>>>>>>>>>>9" no-undo.
def  var e-custno         as dec     format ">>>>>>>>>>>9" no-undo.

def  var p-agecredit      as logical                       no-undo.
def  var p-minbal         as decimal                       no-undo.
def  var p-mindays        as integer                       no-undo.
def  var p-detailprt      as logical                       no-undo.
def  var p-div            as logical                       no-undo.
def  var p-sum            as logical                       no-undo.
def  var p-prtby          as char format "x(1)"            no-undo.
def  var p-dispute        as logical                       no-undo.
def  var p-debit          as logical                       no-undo.
def  var p-format         as integer                       no-undo.
/* Foott Hill Mod */
def {1} var p-foothill       as logical                       no-undo.
def var p-rundate         as date no-undo.
/* Foot Hill Mod */
def  var p-regval        as c                                     no-undo.
def var v-regdist        as char format     "x(4)"                no-undo.  
def var v-holdcustomer   like arsc.custno                            no-undo.
def var s-slsrepout      as character     format "x(4)"              no-undo.
def var s-dueamt         like aret.amount    format ">>>>>>>9.99-"   no-undo.
def var v-currentfl      as logical                     initial no   no-undo.
def var s-3mnthsales     like aret.amount    format ">>>>>>>9.99-"   no-undo.
def var s-name           as character        format "x(09)"          no-undo.
def var s-custno         like arsc.custno    format "zzzzzzz9"       no-undo.
def var s-countrycd      like arsc.countrycd                         no-undo.
def var s-region         as char             format "x(1)"           no-undo.
def var s-district       as char             format "x(3)"           no-undo.
def var i-months         as integer                                  no-undo.
def var s-day            as integer                                  no-undo.
def var s-month          as integer                                  no-undo.
def var s-year           as integer                                  no-undo.
def var p-printfl        as logical                     initial yes  no-undo.
def var s-invdays        as integer                     initial 0    no-undo.
def var s-invno          as character                                no-undo.
/* Foot Hill Mod */
def buffer z1-aret for aret.
def buffer z2-aret for aret.
def var v-exchange as de no-undo.
def var v-currencycd as character no-undo.

def var v-transhold       as integer                                  no-undo.
def var v-translist      as char format "x(2)"               no-undo extent 12
 initial
 ["IN","SV","RB","UC","CD","MC","CR","CK","DB","RV","XX","PY"].
/* Foot Hill Mod */
def var cg-days as integer init 0 no-undo.
def var cg-mindays as integer init 0 no-undo.    
def var cg-bucket as integer init 0 no-undo.
def var cg-age like aret.amount extent 7 no-undo.
def var cg-i as integer no-undo.
def var cg-t-tot like aret.amount init 0.
  
           
def temp-table t-arrgdet no-undo
    field cono       like arsc.cono
    field custno     like arsc.custno
    field shipto     like arss.shipto
    field name       like arsc.lookupnm format "x(09)"
    field creditmgr  like arsc.creditmgr
    field countrycd  as character
    field currencycd as character
    field invno      as character    format "x(15)" /* "x(10)" */
    field invsuf     like aret.invsuf
    field seqno      like aret.seqno
    field jrnlno     like aret.jrnlno
    field setno      like aret.setno
    field region     as char format "x(1)"
    field district   as char format "x(3)"
    field slsrepout  like arsc.slsrepout
/* Foot Hill Mod    */
    field divno      as integer format "999"
    field type       as char format "x(2)"
    field invoicedt  as date 
    field terms      as char format "x(8)" 
    field exchange   as de format "9.99999"
    field disputefl  as character format "x(3)"
/* Foot Hill Mod    */
    field sal3mnth   like aret.amount      format ">>>>>>>9.99-"
    field uncash     like aret.amount      format ">>>>>>>>9.99-"
    field misccr     like aret.amount      format ">>>>>>>>9.99-"
    field invdays    as integer
    field daysos     as dec
    field dso        as dec            format ">>>>>>9.99-"
    field artot      as dec            format ">>>>>>>>9.99-"
    field tot-age    as dec            format ">>>>>>>>9.99-"
    field duedt      like aret.duedt
    field unagedcr   like aret.amount  format ">>>>>>>>9.99-"
    field invcurr    like aret.amount  format ">>>>>>>>9.99-"
    field invage1    like aret.amount  format ">>>>>>>>9.99-"
    field invage2    like aret.amount  format ">>>>>>>>9.99-"
    field invage3    like aret.amount  format ">>>>>>>>9.99-"
    field invage4    like aret.amount  format ">>>>>>>>9.99-"
    field invage5    like aret.amount  format ">>>>>>>>9.99-"
    field invage6    like aret.amount  format ">>>>>>>>9.99-"
    index k-t-arrgdet is primary 
          cono        
          custno      
          creditmgr   
          invno       
          invsuf      
          seqno       
          jrnlno 
          setno.  
/{&user_temptable}*/
 
/{&user_drpttable}*/
/* Use to add fields to the report table */
    FIELD arrecid        AS recid   
/{&user_drpttable}*/
 
/{&user_forms}*/
form header
  "~015 " at 1
  with frame fx-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: ARRG"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 168
  page-number(xpcd)    at 173 format ">>>9"
  "~015"               at 177
  "Accounts Receivable Aging report" at 73
   "~015"              at 177
   with frame fx-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header
  "~015 " at 1
  with frame f-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: ARRG"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 168
  page-number          at 173 format ">>>9"
  "~015"              at 177
  "Accounts Receivable Aging report" at 73
  "~015"              at 177
   with frame f-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header 
   "~015"                    at 177
  with frame f-trn3 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header 
   "~015"                    at 177
  with frame f-trn4 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form  header /* Header line */
    "Name"                at 2
    "  Customer"          at 15
    " Last 3 mths"        at 35
    "     Net A/R"        at 48
    "   Unapplied"        at 62
    "       Misc"         at 75
    "  Current**"         at 88
    "-------------"       at 101
    "-------------"       at 114
    "--Past Due---"       at 127
    "-------------"       at 140
    "-------------"       at 153
    "------------"       at 166
    "~015"                at 178
  with frame f-trn5 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header
    "Invoice #"           at 2
    "     #  "            at 15
    "        DSO"         at 23
    "    Sales"           at 35
    "      Total"         at 48
    "    Credits"         at 62
    "     Credits"        at 75
    "   0-15 days"        at 88
    "  15-30 days"        at 101
    "  31-60 days"        at 114
    "  61-90 days"        at 127
    " 91-180 days"        at 140
    "181-365 days"        at 153
    "   366 plus"        at 166
    "~015"                at 178
      with frame f-trn6 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header
    "----------"          at 2
    "--------"            at 12
    "--------------"      at 20
    "--------------"      at 34
    "--------------"      at 48
    "-------------"       at 62
    "-------------"       at 75
    "-------------"       at 88
    "-------------"       at 101
    "-------------"       at 114
    "-------------"       at 127 
    "-------------"       at 140
    "-------------"       at 153
    "------------"        at 166
    "~015"                at 178
  with frame f-trn7 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form
    v-summary-lit2        at 1  format "x(22)"
    t-arrgdet.dso            at 24
    t-arrgdet.sal3mnth       at 35
    t-arrgdet.artot          at 47
    t-arrgdet.uncash         at 61
    t-arrgdet.misccr         at 74
    t-arrgdet.invcurr        at 87
    t-arrgdet.invage1           at 100
    t-arrgdet.invage2           at 113
    t-arrgdet.invage3           at 126
    t-arrgdet.invage4           at 139
    t-arrgdet.invage5           at 152
    t-arrgdet.invage6           at 165
    "~015"           at 178
 with frame f-tot width 178
       no-box no-labels.  
form 
  "~015"           at 1
with frame f-skip width 178 no-box no-labels.  
form   /* Customer line */
    t-arrgdet.name           at 2
    t-arrgdet.custno         at 12
    "~015"                at 178 
with frame f-customer no-box no-labels down width 180.
form   /* Customer line */
    t-arrgdet.custno         at 2
    arsc.name             at 15
    arsc.addr[1]          at 47
    arsc.addr[2]          at 79
    arsc.city             at 111
    arsc.state            at 133
    arsc.zip              at 137
    arsc.countrycd        at 148 
    "~015"                at 178 
with frame f-customerfoot no-box no-labels down width 180.
form /* Customer total line */
    "----------"          at 25
    "-----------"         at 36
    "------------"        at 48
    "-------------"       at 61
    "------------"        at 75
    "------------"        at 88
    "------------"        at 101
    "------------"        at 114
    "------------"        at 127
    "------------"        at 140
    "------------"        at 153
    "------------"        at 167
    "~015"                at 179
  with frame f-totuline no-box no-labels down width 180.
form   /* Detail line */
    t-arrgdet.name           at 2
    t-arrgdet.custno         at 12  
    t-arrgdet.dso            at 24
    t-arrgdet.sal3mnth       at 35
    t-arrgdet.artot          at 47
    t-arrgdet.uncash         at 61
    t-arrgdet.misccr         at 74
    t-arrgdet.invcurr           at 87
    t-arrgdet.invage1           at 100
    t-arrgdet.invage2           at 113
    t-arrgdet.invage3           at 126
    t-arrgdet.invage4           at 139
    t-arrgdet.invage5           at 152
    t-arrgdet.invage6           at 165
    "~015"                at 178
with frame f-detail no-box no-labels down width 180.
form    
    "=========="      at 25
    "==========="     at 36
    "============"    at 48
    "============="   at 61
    "============"    at 75
    "============"    at 88
    "============"    at 101
    "============"    at 114
    "============"    at 127
    "============"    at 140
    "============"    at 153
    "============"    at 166
    "~015"            at 178
 with frame f-totequalline no-box no-labels down width 180.
form   /* Invoice line */
    t-arrgdet.invno             at 2
    t-arrgdet.invcurr           at 87
    t-arrgdet.invage1           at 100
    t-arrgdet.invage2           at 113
    t-arrgdet.invage3           at 126
    t-arrgdet.invage4           at 139
    t-arrgdet.invage5           at 152
    t-arrgdet.invage6           at 165
    "~015"                      at 178
    with frame f-invoice no-box no-labels down width 180.
/* Foot Hill Mod */
form   /* Invoice line */
    t-arrgdet.invno             at 2
    t-arrgdet.type              at 18
    t-arrgdet.invoicedt         at 21
    t-arrgdet.duedt             at 31
    t-arrgdet.terms             at 40
    t-arrgdet.exchange          at 49
    t-arrgdet.invcurr           at 87
    t-arrgdet.invage1           at 100
    t-arrgdet.invage2           at 113
    t-arrgdet.invage3           at 126
    t-arrgdet.invage4           at 139
    t-arrgdet.invage5           at 152
    t-arrgdet.invage6           at 165
    "~015"                      at 178
    with frame f-ftinvoice no-box no-labels down width 180.
/* Foot Hill Mod */     
 
/{&user_forms}*/
 
/{&user_extractrun}*/
 run sapb_vars.
 run extract_data.
/{&user_extractrun}*/

/{&user_exportstatDWheaders}*/
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  
  assign export_rec = export_rec + v-del +
                      "DSO" + v-del +
                      "Lst_3_Mth_Sls" + v-del +
                      "AR_Total" + v-del +
                      "UnApplied_Cash" + v-del +
                      "Misc_CR" + v-del +
                      "Age_0Thru15_Days" + v-del +
                      "Age_16Thru30_Days" + v-del +
                      "Age_31Thru60_Days" + v-del +
                      "Age_61Thru90_Days" + v-del +
                      "Age_91Thru180_Days" + v-del +
                      "Age_181Thru365_Days" + v-del +
                      "Age_366Plus_Days" + v-del +
                      "AR_Aged".
  if p-detailprt then
    assign export_rec = export_rec + v-del +
           "InvoiceNo"   + v-del +   
           "InvoiceType"  + v-del +   
           "InvoiceDate" + v-del +
           "DueDate"     + v-del +
           "InvoiceTerms"  + v-del +
           "DisputeFl"    + v-del +
           "InvoiceAge" + v-del +
           "InvoiceExchangeRate" + v-del +
           "Countrycd"           + v-del +
           "Currencycd"           + v-del +
           "RunDate".
 




/{&user_exportstatDWheaders}*/
 
 
/{&user_exportstatheaders}*/
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  
  assign export_rec = export_rec + v-del +
                      "DSO" + v-del +
                      "Lst_3_Mth_Sls" + v-del +
                      "AR_Total" + v-del +
                      "UnApplied Cash" + v-del +
                      "Misc_CR" + v-del +
                      "0   - 15  Days" + v-del +
                      "16  - 30  Days" + v-del +
                      "31  - 60  Days" + v-del +
                      "61  - 90  Days" + v-del +
                      "91  - 180 Days" + v-del +
                      "181 - 365 Days" + v-del +
                      "366 Plus  Days" + v-del +
                      "AR_Aged".
  if p-detailprt then
    assign export_rec = export_rec + v-del +
           "Invoice No."   + v-del +   
           "Invoice Type"  + v-del +   
           "Invoice Date " + v-del +
           "Due Date "     + v-del +
           "Invoice Terms "  + v-del +
           "DisputeFL " + v-del +
           "Invoice Age "    + v-del +
           "Invoice Exchange Rate" + v-del +
           "CountryCd" + v-del +
           "Currencycd" + v-del +
           "RunDate".
                         
/{&user_exportstatheaders}*/
 
/{&user_foreach}*/
for each t-arrgdet no-lock:
assign v-regdist = string(string(t-arrgdet.region,"x") +
                          string(t-arrgdet.district,"x(4)")).
/{&user_foreach}*/
 
/{&user_drptassign}*/
 /* If you add fields to the dprt table this is where you assign them */
   drpt.arrecid = recid(t-arrgdet)
/{&user_drptassign}*/

/{&user_B4endloop}*/
/{&user_B4endloop}*/

 
/{&user_viewheadframes}*/
/* Since frames are unique to each program you need to put your
   views and hides here with your names */
if not p-exportl then
  do:
  if not x-stream then
    do:
    hide frame f-trn1.
    hide frame f-trn2.
    hide frame f-trn3.
    hide frame f-trn4.
    hide frame f-trn5.                  
    hide frame f-trn6.
    hide frame f-trn7.
    end.
  else
   do:
    hide stream xpcd frame f-trn1.
    hide stream xpcd frame fx-trn2.
    hide stream xpcd frame f-trn3.
    hide stream xpcd frame f-trn4.
    hide stream xpcd frame f-trn5.
    hide stream xpcd frame f-trn6.
    hide stream xpcd frame f-trn7.
    end.
  
  end.
if not p-exportl  then
  do:
  if not x-stream then
    do:
    page.
    view frame f-trn1.
    view frame f-trn2.
    view frame f-trn3.
    view frame f-trn4.
    view frame f-trn5.
    view frame f-trn6.
    view frame f-trn7.
    end.
  else  
    do:
    page stream xpcd.
    view stream xpcd frame f-trn1.
    view stream xpcd frame fx-trn2.
    view stream xpcd frame f-trn3.
    view stream xpcd frame f-trn4.
    view stream xpcd frame f-trn5.
    view stream xpcd frame f-trn6.
    view stream xpcd frame f-trn7.
    end.
   
  end.
/{&user_viewheadframes}*/
 
/{&user_drptloop}*/
if v-holdsortmystery = "xxxxxxxxxxxxxx" then
    do:
     assign  v-holdcustomer = 999999999999.
    end.
 
/{&user_drptloop}*/
 
/{&user_detailprint}*/
   /* Comment this out if you are not using detail 'X' option  */
   if p-detailprt and 
      entry(u-entitys,drpt.sortmystery,v-delim) <> 
      fill("~~",length(entry(u-entitys,drpt.sortmystery,v-delim))) then 
     do:
     v-inx2 = u-entitys.
     if p-exportl then
       run print-dtl-export (input drpt.arrecid).
     else
       run print-dtl (input drpt.arrecid).
     end.
/{&user_detailprint}*/
 
/{&user_finalprint}*/
assign v-per1 =  if t-amounts2[u-entitys + 1]   = 0 then
                   0 
                 else
                   (t-amounts3[u-entitys + 1] * 90) / 
                    t-amounts2[u-entitys + 1].
                    
if not p-exportl then
 do:
 if not x-stream then 
   do:
   display with frame f-totequalline.
   display
     v-summary-lit2              @ v-summary-lit2
     v-per1                      @ t-arrgdet.dso
     t-amounts2[u-entitys + 1]   @ t-arrgdet.sal3mnth
     t-amounts3[u-entitys + 1]   @ t-arrgdet.artot
     t-amounts4[u-entitys + 1]   @ t-arrgdet.uncash
     t-amounts5[u-entitys + 1]   @ t-arrgdet.misccr
     t-amounts6[u-entitys + 1]   @ t-arrgdet.invcurr
     t-amounts7[u-entitys + 1]   @ t-arrgdet.invage1
     t-amounts8[u-entitys + 1]   @ t-arrgdet.invage2
     t-amounts9[u-entitys + 1]   @ t-arrgdet.invage3
     t-amounts10[u-entitys + 1]  @ t-arrgdet.invage4
     t-amounts11[u-entitys + 1]  @ t-arrgdet.invage5
     t-amounts12[u-entitys + 1]  @ t-arrgdet.invage6
     with frame f-tot.
   down with frame f-tot.
   end.
 else
   do:
   display stream xpcd with frame f-totequalline.
   display stream xpcd
     v-summary-lit2               @ v-summary-lit2
     v-per1                      @ t-arrgdet.dso
     t-amounts2[u-entitys + 1]   @ t-arrgdet.sal3mnth
     t-amounts3[u-entitys + 1]   @ t-arrgdet.artot
     t-amounts4[u-entitys + 1]   @ t-arrgdet.uncash
     t-amounts5[u-entitys + 1]   @ t-arrgdet.misccr
     t-amounts6[u-entitys + 1]   @ t-arrgdet.invcurr
     t-amounts7[u-entitys + 1]   @ t-arrgdet.invage1
     t-amounts8[u-entitys + 1]   @ t-arrgdet.invage2
     t-amounts9[u-entitys + 1]   @ t-arrgdet.invage3
     t-amounts10[u-entitys + 1]  @ t-arrgdet.invage4
     t-amounts11[u-entitys + 1]  @ t-arrgdet.invage5
     t-amounts12[u-entitys + 1]  @ t-arrgdet.invage6
    with frame f-tot.
  
   down stream xpcd with frame f-tot.
   end.
      
 end.
else
  do:
  assign export_rec =  v-astrik.
  do v-inx4 = 1 to u-entitys:
    if v-totalup[v-inx4] = "n" then
       next.
    if v-inx4 = 1 then
      v-summary-lit2 = "Final".
    else
      v-summary-lit2 = "".
    if can-do("c,m",v-lookup[v-inx4]) then
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec + v-summary-lit2 + v-del + " ".
     
      end.
    else
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec +  v-summary-lit2.
      end.
  end.
  assign export_rec = export_rec + v-del +
      string(v-per1,"->>>>>>>>9") + v-del +
      string(t-amounts2[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts3[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts4[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts5[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts6[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts7[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts8[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts9[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts10[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts11[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts12[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts13[u-entitys + 1],"->>>>>>>>9").

  assign export_rec = export_rec + chr(13).    
  if not p-exportDWl then  
    put stream expt unformatted export_rec.
  end.
/{&user_finalprint}*/
 
/{&user_summaryframeprint}*/
     assign v-per1 =  if t-amounts2[v-inx2]   = 0 then
                        0 
                      else
                        (t-amounts3[v-inx2] * 90) / 
                         t-amounts2[v-inx2].
     if not x-stream then
      do:
      display 
        v-summary-lit2       @ v-summary-lit2
        v-per1               @ t-arrgdet.dso
        t-amounts2[v-inx2]   @ t-arrgdet.sal3mnth
        t-amounts3[v-inx2]   @ t-arrgdet.artot
        t-amounts4[v-inx2]   @ t-arrgdet.uncash
        t-amounts5[v-inx2]   @ t-arrgdet.misccr
        t-amounts6[v-inx2]   @ t-arrgdet.invcurr
        t-amounts7[v-inx2]   @ t-arrgdet.invage1
        t-amounts8[v-inx2]   @ t-arrgdet.invage2
        t-amounts9[v-inx2]   @ t-arrgdet.invage3
        t-amounts10[v-inx2]  @ t-arrgdet.invage4
        t-amounts11[v-inx2]  @ t-arrgdet.invage5
        t-amounts12[v-inx2]  @ t-arrgdet.invage6
  
        with frame f-tot.
      down with frame f-tot.
      end.
    else
      do:
      display stream xpcd
        v-summary-lit2       @ v-summary-lit2
        v-per1               @ t-arrgdet.dso
        t-amounts2[v-inx2]   @ t-arrgdet.sal3mnth
        t-amounts3[v-inx2]   @ t-arrgdet.artot
        t-amounts4[v-inx2]   @ t-arrgdet.uncash
        t-amounts5[v-inx2]   @ t-arrgdet.misccr
        t-amounts6[v-inx2]   @ t-arrgdet.invcurr
        t-amounts7[v-inx2]   @ t-arrgdet.invage1
        t-amounts8[v-inx2]   @ t-arrgdet.invage2
        t-amounts9[v-inx2]   @ t-arrgdet.invage3
        t-amounts10[v-inx2]  @ t-arrgdet.invage4
        t-amounts11[v-inx2]  @ t-arrgdet.invage5
        t-amounts12[v-inx2]  @ t-arrgdet.invage6
 
        with frame f-tot.
      down stream xpcd with frame f-tot.
      end.
/{&user_summaryframeprint}*/
 
/{&user_summaryputexport}*/
    assign v-per1 =  if t-amounts2[v-inx2]   = 0 then
                       0 
                     else
                       (t-amounts3[v-inx2] * 90) / 
                       t-amounts2[v-inx2].
  
/* PTD */
    assign export_rec = v-astrik + v-del + export_rec.
    assign export_rec = export_rec + v-del +
      string(v-per1,"->>>>>>>>9") + v-del +
      string(t-amounts2[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts3[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts4[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts5[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts6[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts7[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts8[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts9[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts10[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts11[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts12[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts13[v-inx2],"->>>>>>>>9").
 
/{&user_summaryputexport}*/
 
/{&user_detailputexport}*/

    assign v-per1 =  if t-arrgdet.sal3mnth   = 0 then
                       0 
                     else
                       (t-arrgdet.artot * 90) / 
                        t-arrgdet.sal3mnth.
  
/* PTD */
    assign export_rec = v-astrik + v-del + export_rec.
    assign export_rec = export_rec + v-del +
      string(v-per1,"->>>>>>>>9") + v-del +
      string(t-arrgdet.sal3mnth,"->>>>>>>>9.99") + v-del +
      string(t-arrgdet.artot ,"->>>>>>>>9.99")   + v-del +
      string(t-arrgdet.uncash,"->>>>>>>>9.99")   + v-del +
      string(t-arrgdet.misccr,"->>>>>>>>9.99")   + v-del +
      string(t-arrgdet.invcurr,"->>>>>>>>9.99")  + v-del +
      string(t-arrgdet.invage1,"->>>>>>>>9.99")  + v-del +
      string(t-arrgdet.invage2,"->>>>>>>>9.99")  + v-del +
      string(t-arrgdet.invage3,"->>>>>>>>9.99")  + v-del +
      string(t-arrgdet.invage4,"->>>>>>>>9.99")  + v-del +
      string(t-arrgdet.invage5,"->>>>>>>>9.99")  + v-del +
      string(t-arrgdet.invage6,"->>>>>>>>9.99")  + v-del +
      string(t-arrgdet.tot-age,"->>>>>>>>9.99")  + v-del +
      caps(t-arrgdet.invno)                      + v-del +   
      caps(t-arrgdet.type)                       + v-del +   
      (if t-arrgdet.invoicedt = ? then
         string(today,"99/99/9999")
       else 
         string(t-arrgdet.invoicedt,"99/99/9999")) + v-del +
      (if t-arrgdet.duedt = ? and
          t-arrgdet.invoicedt <> ? then
         string(t-arrgdet.invoicedt,"99/99/9999")
       else
       if t-arrgdet.duedt = ? and
          t-arrgdet.invoicedt = ? then
         string(today,"99/99/9999")
       else
         string( t-arrgdet.duedt,"99/99/9999"))  + v-del +
      caps(t-arrgdet.terms)                      + v-del +
      caps(t-arrgdet.disputefl)                  + v-del +
      string(t-arrgdet.invdays,"->>>>>>9")       + v-del +
      string(t-arrgdet.exchange,"9.99999")       + v-del + 
      caps(t-arrgdet.countrycd )                 + v-del +
      caps(t-arrgdet.currencycd )                + v-del +
      string(p-rundate,"99/99/9999").
 


/{&user_detailputexport}*/






/{&user_procedures}*/
procedure sapb_vars:
 assign
    b-creditmgr  = sapb.rangebeg[1]
    e-creditmgr  = sapb.rangeend[1]
    b-region     = sapb.rangebeg[2]
    e-region     = sapb.rangeend[2]
    b-district   = sapb.rangebeg[3]
    e-district   = sapb.rangeend[3]
    b-slsrep     = sapb.rangebeg[4]
    e-slsrep     = sapb.rangeend[4]
    b-divno      = if sapb.rangebeg[5] = "" then 0 else 
                       int(substr(sapb.rangebeg[5],1,3))
    e-divno      = if sapb.rangeend[5] = "" then 999 else 
                      min (999,dec(substr(sapb.rangeend[5],1,3)))
   
    b-custno     = dec(substr(sapb.rangebeg[6],1,12))
    e-custno     = dec(substr(sapb.rangeend[6],1,12))

    
    p-format     = int(sapb.optvalue[1])
  
    p-agecredit  = if sapb.optvalue[2] = "Yes" then yes else no
    p-minbal     = dec(sapb.optvalue[4])  
    p-mindays    = int(sapb.optvalue[6])   
    p-detailprt  = if sapb.optvalue[7] = "Yes" then yes else no
    p-detail     = if p-detailprt then "d" else "s".
    
    if sapb.optvalue[8] = "yes" then
       assign p-dispute = yes.
    else
       assign p-dispute = no.
       
    if sapb.optvalue[9] = "yes" then
       assign p-debit = yes.
    else 
       assign p-debit = no.
/* Foot Hill Mod */    
    assign p-foothill = if sapb.optvalue[10] = "yes" then true else false.
/* Foot Hill Mod */    
    assign p-export = sapb.optvalue[11].
    assign p-rundate = today.
    if day(today - 1) = 1 then
       p-rundate = today - 1.
    run formatoptions.
end.
/* ------------------------------------------------------------------------- */
procedure formatoptions:
/* ------------------------------------------------------------------------- */
   
   
  assign p-portrait = false.
  
  if sapb.optvalue[1] > "0" and sapb.optvalue[1] ne "99" then 
    do:
    p-optiontype = " ".
    find notes where notes.cono = g-cono and
                     notes.notestype = "zz" and
                     notes.primarykey = "arrg" and
                     notes.secondarykey = string(p-format) no-lock no-error.
    if not avail notes then
      do:
      display "Format is not valid cannot process request".
      assign p-optiontype = "r"
             p-sorttype   = ">,"
             p-totaltype  = "l"
             p-summcounts = "a".
      return.
      end.               
   
    assign p-optiontype = notes.noteln[1]
           p-sorttype   = notes.noteln[2]
           p-totaltype  = notes.noteln[3]
           p-summcounts = notes.noteln[4].
    assign p-register   = ""
           p-registerex = "".
    if dec(sapb.optvalue[4]) <> 0 then   /* min ar balance set register param */
       assign p-register   = "c#3"  /* customer total 3 on report        */
              p-registerex = "g" + sapb.optvalue[4].
    if dec(sapb.optvalue[6]) <> 0 and 
       dec(sapb.optvalue[4]) <> 0 then   
       assign p-register   = p-register + ","  
              p-registerex = p-registerex + ",".
    
    if dec(sapb.optvalue[6]) <> 0 then   /* min ar balance set register param */
      assign p-register   = "c#13" /* customer total 1 in total accums  */
             p-registerex = "g0".
    end.     
  else
  if sapb.optvalue[1] = "99" then 
    do:
    assign p-register   = ""
           p-registerex = "".
    run reportopts(input sapb.user5, 
                   input-output p-optiontype,
                   input-output p-sorttype,  
                   input-output p-totaltype,
                   input-output p-summcounts,
                   input-output p-register,
                   input-output p-registerex).
/* Customer has to be chosen if Balance Options are choosen     */    
    if index(p-optiontype,"c",1) = 0 and
       dec(sapb.optvalue[6]) <> 0 and 
       dec(sapb.optvalue[4]) <> 0 then   
      do:
      assign p-optiontype = p-optiontype + ",c"
             p-sorttype   = p-sorttype + ",>"
             p-totaltype  = p-totaltype + ",t"
             p-summcounts = p-summcounts + ",a".
      end.       
    
 
   if dec(sapb.optvalue[4]) <> 0 then   /* min ar balance set register param */
       do:
       if p-register <> "" then
         assign p-register = p-register + ","
                p-registerex = p-registerex + ",".
       
       assign p-register   = p-register + "c#3" 
            /* customer total 3 on report        */
              p-registerex = p-registerex + "g" + sapb.optvalue[4].
       end.
   if dec(sapb.optvalue[6]) <> 0 and 
      dec(sapb.optvalue[4]) <> 0 then   
      assign p-register   = p-register + ","  
             p-registerex = p-registerex + ",".
    
   if dec(sapb.optvalue[6]) <> 0 then   /* min ar balance set register param */
      assign p-register   = "c#13" /* customer total 1 in total accums  */
             p-registerex = "g0".
   end.
  run Print_reportopts (input recid (sapb),
                       input g-cono).

 end.
   
   
   
   
procedure extract_data:
 if p-mindays = 0 then
   do:
   assign cg-mindays = 0.
   assign cg-bucket = 0.
   end. /* if p-mindays = 0 */
 else
 if p-mindays le 15 then 
   do: 
   assign cg-mindays = 15.
   assign cg-bucket = 1.                       
   end. /* if p-mindays le 15 */
 else
 if p-mindays gt 15 and p-mindays le 30 then 
   do:
   assign cg-mindays = 30.
   assign cg-bucket = 2.
   end. /* if p-mindays gt 15 and  le 30 */
 else                     
 if  p-mindays gt 30 and p-mindays le 60 then 
   do:
   assign cg-mindays = 60.
   assign cg-bucket = 3.
   end. /* if p-mindays gt 30 and  le 60 */
 else                     
 if p-mindays gt 60 and p-mindays le 90 then 
   do:
   assign cg-mindays = 90.
   assign cg-bucket = 4.
   end. /* if p-mindays gt 60 and le 90 */
 else                     
 if p-mindays gt 90 and p-mindays le 180 then
   do:
   assign cg-mindays = 180.
   assign cg-bucket = 5.
   end. /* if p-mindays gt 90 le 180 */
 else                         
 if p-mindays gt 180 and p-mindays le 365 then
   do:
   assign cg-mindays = 365.
   assign cg-bucket = 6.
   end. /* if p-mindays gt 180 le 365 */
 else       
 if p-mindays gt 365 then 
   do:
   assign cg-mindays = 366.
   assign cg-bucket = 7.
   end. /* if p-mindays gt 365 */
/* ################# loop to build temp files ################# */
customer:
/* trstat.arsc AR Customer File */
for each arsc use-index k-arsc where
        arsc.cono       =   g-cono        and
/*         arsc.custno     = 72202004 and      */
        arsc.creditmgr  ge  b-creditmgr   and
        arsc.creditmgr  le  e-creditmgr   and
        arsc.slsrepout  ge  b-slsrep      and
        arsc.slsrepout  le  e-slsrep      and
        arsc.divno      ge  b-divno       and
        arsc.divno      le  e-divno       and
        arsc.custno     ge  b-custno      and
        arsc.custno     le  e-custno   
        no-lock:
    /* Eliminate customers with no transactions */
    /* trtrans.aret AR Transactions */
    find first aret use-index k-invno where
        aret.cono         =   g-cono       and
        aret.custno       =   arsc.custno  and
        aret.statustype   =   true         and
        aret.invsuf       = (if p-debit = true then
                                 99
                               else
                                 aret.invsuf) and
        aret.disputefl    = (if p-dispute = true then
                               yes
                             else
                               aret.disputefl) and
        can-do("3,5,11",string(aret.transcd))
        no-lock no-error.
    
    
    if not avail aret and p-foothill then 
      next customer.
    assign s-3mnthsales = 0.
    run "120-3month".
    
    assign s-slsrepout = arsc.slsrepout.
      
    run 110-regdist.    
   /* take out if they want to use shipto's */
    if b-region <> "" or b-district <> "" then 
      do:
        /* trsm.smsn Sales Rep File */
      if s-region lt b-region or 
         s-region gt e-region then
         next customer.
      if s-district lt b-district or 
         s-district gt e-district then
         next customer.
      end. /* if b-region <> "" or b-district <> "" */
 
 
    
    
    
    /* For now to balance to the old arrg
    if aret.shipto <> "" then
      do:
      find arss where arss.cono = 1 and 
                      arss.custno = arsc.custno and
                      arss.shipto = aret.shipto no-lock no-error.
      if avail arss then
        assign s-slsrepout = arss.slsrepout.                
      end.
    */
    assign
        s-name       = arsc.name
        s-custno     = arsc.custno
        s-countrycd  = arsc.countrycd.
    
    if not avail aret and s-3mnthsales = 0 then
        next customer.
    else
    if not avail aret then
      do:
      /* No A/R transactions but sales need to be put into the summary
         so a fake detail transaction is made to carry the sales 
      */   
      run 110-regdist.
      if b-region <> "" or b-district <> "" then 
        do:
        /* trsm.smsn Sales Rep File */
        if s-region lt b-region or 
           s-region gt e-region then
           next customer.
        if s-district lt b-district or 
           s-district gt e-district then
           next customer.
        end. /* if b-region <> "" or b-district <> "" */
 
     
      create t-arrgdet.
      assign
        t-arrgdet.creditmgr = arsc.creditmgr
        t-arrgdet.divno     = arsc.divno
        t-arrgdet.region    = s-region
        t-arrgdet.district  = s-district
        t-arrgdet.cono      = arsc.cono
        t-arrgdet.custno    = arsc.custno
        t-arrgdet.shipto    = if avail aret then aret.shipto else ""
        t-arrgdet.name      = s-name
        t-arrgdet.countrycd = if s-countrycd = "" then "US" else s-countrycd
        t-arrgdet.currencycd = " "
        t-arrgdet.invno     = "DoNotPrint"
        t-arrgdet.invsuf    = 0
        t-arrgdet.seqno     = 0
        t-arrgdet.jrnlno    = 0
        t-arrgdet.setno     = 0
        t-arrgdet.duedt     = ?
        t-arrgdet.invoicedt = ?
        t-arrgdet.slsrepout = s-slsrepout
        t-arrgdet.exchange  = 1
        t-arrgdet.terms     = " "
        t-arrgdet.type      = "  "
        t-arrgdet.invcurr   = 0.0
        t-arrgdet.invage1   = 0.0
        t-arrgdet.invage2   = 0.0
        t-arrgdet.invage3   = 0.0
        t-arrgdet.invage4   = 0.0
        t-arrgdet.invage5   = 0.0
        t-arrgdet.invage6   = 0.0
        t-arrgdet.sal3mnth  = s-3mnthsales
        t-arrgdet.invdays   = 0
        t-arrgdet.daysos    = 0
        t-arrgdet.uncash    = 0
        t-arrgdet.misccr    = 0
        t-arrgdet.artot     = 0
        t-arrgdet.tot-age   = 0.
        next customer.
      end. 
       
      
    run "110-age-loop".
end.      /*  for each arsc  */
hide all no-pause.
{p-rptend.i}
end.
/* ------------------------------------------------------------------------- */
procedure 110-age-loop:
/* ------------------------------------------------------------------------- */
/* transcd 3,5 are credits, 11 is all types of debit */            
uacredit:
/* trtrans.aret AR Transactions */
  for each aret use-index k-invno where
          aret.cono         =   g-cono       and
          aret.custno       =   arsc.custno  and
          aret.statustype   =   true         and
          aret.invsuf       = (if p-debit = true then
                                 99
                               else
                                 aret.invsuf) and
          aret.disputefl    = (if p-dispute = true then
                               yes
                             else
                               aret.disputefl) and
          can-do("3,5,11",string(aret.transcd)) no-lock
          by aret.invdt desc:
    
                            
/* trtrans.aret AR Transactions */
/* put future invoices and service charges in current slot */
/* They now want to age service charges */
    if aret.transcd = 11 then 
      do:
      if aret.sourcecd = 0 and aret.period = 0 then
        assign v-currentfl = yes.
      else if aret.sourcecd = 1 and aret.period = 9 then
        assign v-currentfl = no.
      else assign v-currentfl = no.
      end.
    else 
      assign v-currentfl = no.    
    assign s-dueamt = aret.amount.
/* Foot Hill Mod  */
    find z1-aret use-index k-invdt where 
         z1-aret.cono = aret.cono     and
         z1-aret.custno = aret.custno and
         z1-aret.invno = aret.invno   and
         z1-aret.invsuf = aret.invsuf and
         z1-aret.seqno = 0 no-lock no-error.
     
    if not avail z1-aret then
      assign v-transhold = aret.transcd.
    else
      assign v-transhold = z1-aret.transcd.
    find oeeh where oeeh.cono = g-cono and
                    oeeh.orderno = z1-aret.invno and
                    oeeh.ordersuf = z1-aret.invsuf no-lock no-error.
    /*
    if avail oeeh then
      s-slsrepout = oeeh.slsrepout.
    */
    if avail oeeh and arsc.divno = 40 then
      assign v-exchange = if (substr(oeeh.user5,1,1) = "c" or
                              oeeh.currencyty = "lb") then
                            oeeh.user6
                          else
                            1
             v-currencycd = if (substr(oeeh.user5,1,1) = "c" and
                                oeeh.user6 <> 0) then
                              "CA" 
                            else
                            if (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                              then
                              "LB"
                            else  
                              " ".   
    else
    if avail oeeh then
       assign v-exchange = if (substr(oeeh.user5,1,1) = "c" or
                              oeeh.currencyty = "lb") then
                            oeeh.user6
                          else
                            1

              v-currencycd = if (substr(oeeh.user5,1,1) = "c" and
                                 oeeh.user6 <> 0) then
                               "CA" 
                             else
                             if (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                               then
                               "LB"
                             else  
                               " ".   

    
    else
      assign
        v-exchange = 1
        v-currencycd = " " .
/* Foot Hill Mod  */
      
    run 110-regdist.    
    if b-region <> "" or b-district <> "" then 
      do:
        /* trsm.smsn Sales Rep File */
      if s-region lt b-region or 
         s-region gt e-region then
         next uacredit.
      if s-district lt b-district or 
         s-district gt e-district then
         next uacredit.
      end. /* if b-region <> "" or b-district <> "" */
 
 
    
    assign
        s-name       = arsc.name
        s-custno     = arsc.custno
        s-countrycd  = arsc.countrycd.
    
    create t-arrgdet.
    assign
      t-arrgdet.creditmgr = arsc.creditmgr
      t-arrgdet.divno     = arsc.divno
      t-arrgdet.region    = s-region
      t-arrgdet.district  = s-district
      t-arrgdet.cono      = arsc.cono
      t-arrgdet.custno    = arsc.custno
      t-arrgdet.shipto    = aret.shipto
      t-arrgdet.name      = s-name
      t-arrgdet.countrycd = if s-countrycd = "" then "US" else s-countrycd
      t-arrgdet.currencycd = v-currencycd
      t-arrgdet.invno     = string(aret.invno) + "-" + string(aret.invsuf,"99")
      t-arrgdet.invsuf    = aret.invsuf
      t-arrgdet.seqno     = aret.seqno
      t-arrgdet.jrnlno    = aret.jrnlno
      t-arrgdet.setno     = aret.setno
      t-arrgdet.duedt     = aret.duedt
      t-arrgdet.invoicedt = aret.invdt
      t-arrgdet.slsrepout = s-slsrepout
/* Foot Hill Mod    */
      t-arrgdet.exchange  = v-exchange
      t-arrgdet.terms     = aret.termstype
      t-arrgdet.type      = if v-transhold ge 0 and v-transhold le 11 then
                              v-translist[v-transhold + 1]
                            else   
                              "  "
      t-arrgdet.disputefl  = if aret.disputefl = yes then "yes" else "no "
/* Foot Hill Mod    */
      t-arrgdet.invcurr   = 0.0
      t-arrgdet.invage1   = 0.0
      t-arrgdet.invage2   = 0.0
      t-arrgdet.invage3   = 0.0
      t-arrgdet.invage4   = 0.0
      t-arrgdet.invage5   = 0.0
      t-arrgdet.invage6   = 0.0
      t-arrgdet.sal3mnth  = s-3mnthsales.
    /* After putting this in one detail record zero it out so accumulators
        don't keep adding the value for each invoice.
    */    
    assign s-3mnthsales = 0.
    
    if aret.transcd = 3 then 
      assign t-arrgdet.invno = "Unapp Cash".
    assign t-arrgdet.invdays = today - aret.invdt
           t-arrgdet.daysos = 0.
    
    if aret.transcd = 3 then
      assign t-arrgdet.uncash =  aret.amount.
    else
    if aret.transcd = 5 then
      assign t-arrgdet.misccr  =  aret.amount.
                                
    if not p-agecredit and can-do("3,5",string(aret.transcd)) then
      do:
      assign v-currentfl  =  no
             t-arrgdet.invno     = "DoNotPrint".
      /* When Age Credits option is choosen they don't want to see them
         so here this will tell the detail print not to print
         if this changes just take out the invno assignment.
      */   
      end. 
    else
    if v-currentfl then
      assign t-arrgdet.invcurr = s-dueamt.
    else 
      do:   
      if (today - aret.duedt) le 15 then
        assign t-arrgdet.invcurr = s-dueamt
               t-arrgdet.daysos = 15.
                      
      if (today - aret.duedt) gt 15 and (today - aret.duedt) le 30 then
        assign t-arrgdet.invage1 = s-dueamt
               t-arrgdet.daysos = 30.
                                 
      if (today - aret.duedt) gt 30 and (today - aret.duedt) le 60 then
        assign t-arrgdet.invage2 = s-dueamt
               t-arrgdet.daysos = 60.
                      
      if (today - aret.duedt) gt 60 and (today - aret.duedt) le 90 then
        assign t-arrgdet.invage3 = s-dueamt
              t-arrgdet.daysos = 90.
                      
      if (today - aret.duedt) gt 90 and (today - aret.duedt) le 180 then
        assign t-arrgdet.invage4 = s-dueamt
               t-arrgdet.daysos = 180.
                      
      if (today - aret.duedt) gt 180 and (today - aret.duedt) le 365 then
        assign t-arrgdet.invage5 = s-dueamt
               t-arrgdet.daysos = 365.
        
      if (today - aret.duedt) gt 365 then
        assign t-arrgdet.invage6 = s-dueamt
               t-arrgdet.daysos = 366.
      if aret.duedt = ? then
        assign t-arrgdet.invcurr = s-dueamt.
      end. /* else v-currentfl */
  if v-currentfl = yes and not p-exportDWl then 
    assign t-arrgdet.invno = t-arrgdet.invno + " **".
  assign t-arrgdet.artot = 
         t-arrgdet.invcurr +
         t-arrgdet.invage1 + 
         t-arrgdet.invage2 + 
         t-arrgdet.invage3 + 
         t-arrgdet.invage4 + 
         t-arrgdet.invage5 + 
         t-arrgdet.invage6 + 
         (if p-agecredit then
             0 
          else
             t-arrgdet.uncash)  +
         (if p-agecredit then
             0 
          else
             t-arrgdet.misccr).
 
 /* determine how much A/R aging is within the parameter MinDays */
 assign cg-age[1] = t-arrgdet.invcurr
        cg-age[2] = t-arrgdet.invage1
        cg-age[3] = t-arrgdet.invage2
        cg-age[4] = t-arrgdet.invage3
        cg-age[5] = t-arrgdet.invage4
        cg-age[6] = t-arrgdet.invage5
        cg-age[7] = t-arrgdet.invage6.
  if cg-bucket = 0 then
    do cg-i = 1 to 7:
      assign t-arrgdet.tot-age = t-arrgdet.tot-age + cg-age[cg-i].
    end. 
  else
    do cg-i = 7 to cg-bucket by -1:
      assign t-arrgdet.tot-age = t-arrgdet.tot-age + cg-age[cg-i].
    end. 
  
  end. /* for each aret use-index k-invno */
end procedure. /* 110-age-loop */
/* ------------------------------------------------------------------------- */
procedure 110-regdist:
/* ------------------------------------------------------------------------- */
    
    find first smsn use-index k-smsn where 
            smsn.cono = arsc.cono 
        and smsn.slsrep = s-slsrepout 
        no-lock no-error.
        
    if available(smsn) then
        assign s-region   = substring(smsn.mgr,1,1)
               s-district = substring(smsn.mgr,2,3).
    else
        assign s-region = "Z"
               s-district = "999".
 
  end.
 
/* ------------------------------------------------------------------------- */
procedure 120-3month:
/* ------------------------------------------------------------------------- */
/* get sales amounts for last 3 full months */     
assign s-day   = day(today).
assign s-month = month(today).           
assign s-year  = year(today).
do i-months = 1 to 3:
  if i-months > 1 or
     s-day <= 15 then 
    do:  /* Due to Fiscal Calendar, ARRG is run at eom */
    assign s-month = s-month - 1.
    if s-month = 0 then 
      assign s-month = 12
             s-year  = s-year - 1.
    end.
/* trsm.smsc SM Customer History */           
  for each smsc where smsc.cono = arsc.cono and
           smsc.yr   = int(substring(string(s-year),3)) and
           smsc.custno = arsc.custno no-lock:
    assign s-3mnthsales = s-3mnthsales +  smsc.salesamt[s-month].
  end. /* for each smsc where smsc.cono = arsc.cono */
end. /* do i-months = 1 to 3 */
end procedure. /* 120-3month */
/* ------------------------------------------------------------------------- */
procedure print-dtl:
/* ------------------------------------------------------------------------- */
define input parameter zx-recid as recid no-undo.
find t-arrgdet where recid(t-arrgdet) = zx-recid no-lock. 
if t-arrgdet.invno = "DoNotPrint" then
  next.
if t-arrgdet.custno <> v-holdcustomer then
  do:
  v-holdcustomer = t-arrgdet.custno.
  
  if not p-foothill then
    do:
     display
         t-arrgdet.name
         t-arrgdet.custno
         with frame f-customer.
    end. 
  else
  if p-foothill then
    do:
    find arsc where arsc.cono = g-cono and 
                    arsc.custno = t-arrgdet.custno no-lock no-error.
    display
       t-arrgdet.custno
       arsc.name             
       arsc.addr[1]          
       arsc.addr[2]          
       arsc.city             
       arsc.state            
       arsc.zip             
       with frame f-customerfoot.
    end.
  end. 
    if t-arrgdet.daysos >= cg-mindays and not p-foothill then
      do:
        display
            t-arrgdet.invno
            t-arrgdet.invcurr
            t-arrgdet.invage1
            t-arrgdet.invage2
            t-arrgdet.invage3
            t-arrgdet.invage4
            t-arrgdet.invage5
            t-arrgdet.invage6
            with frame f-invoice.
        down with frame f-invoice.
      end.
    else
    if t-arrgdet.daysos >= cg-mindays and p-foothill then
      do:
        display
            t-arrgdet.invno
            t-arrgdet.type
            t-arrgdet.invoicedt
            t-arrgdet.duedt
            t-arrgdet.terms
            t-arrgdet.exchange
            t-arrgdet.invcurr
            t-arrgdet.invage1
            t-arrgdet.invage2
            t-arrgdet.invage3
            t-arrgdet.invage4
            t-arrgdet.invage5
            t-arrgdet.invage6
            with frame f-ftinvoice.
        down with frame f-ftinvoice.
      end.
    /*
    end. /* each t-arrgdet */
    */
end procedure. /* printt-dtl */
  
/* ------------------------------------------------------------------------- */
procedure print-dtl-export:
/* ------------------------------------------------------------------------- */
define input parameter zx-recid as recid no-undo.
  find t-arrgdet where recid(t-arrgdet) = zx-recid no-lock. 
  if t-arrgdet.invno = "DoNotPrint" then
    next.
  
  find arsc where arsc.cono = g-cono and 
                  arsc.custno = t-arrgdet.custno no-lock no-error.
  if t-arrgdet.daysos >= cg-mindays  then
    do:
    run Detail_export. /* Detail_export is part of zsdixrptx.p  */
    end.
end procedure. /* printt-dtl-export */
  
    
/{&user_procedures}*/
 
/* TBXR F6 CodeGen Includes from here down are CodeGen Generated  */ 
/{&user_optiontype}*/
if not
can-do("C",substring(p-optiontype,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries C "
     substring(p-optiontype,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries C "
     substring(p-optiontype,v-inx,1).
/{&user_optiontype}*/
 
/{&user_registertype}*/
if not
can-do("C",substring(p-register,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries C "
     substring(p-register,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries C "
     substring(p-register,v-inx,1).
/{&user_registertype}*/
 
/{&user_DWvarheaders1}*/
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Custno".
  assign export_rec = export_rec + v-del + "CustName".
  assign v-cells[v-inx4] = "2".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
/{&user_DWvarheaders1}*/
 
/{&user_exportvarheaders1}*/
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Customer".
  assign export_rec = export_rec + v-del + "Number".
  assign v-cells[v-inx4] = "2".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
/{&user_exportvarheaders1}*/
 
/{&user_exportvarheaders2}*/
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Customer".
  assign export_rec = export_rec + v-del + "Name".
  assign v-cells[v-inx4] = "2".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
/{&user_exportvarheaders2}*/
 
/{&user_loadtokens}*/
if v-lookup[v-inx] = "C" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(zsmsep.custno,"999999999999")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"999999999999").   
  end.                                              
else                                              
  assign v-token = " ".
/{&user_loadtokens}*/
 
/{&user_totaladd}*/
  assign t-amounts[1] = zsmsep.salesamt[1].
/{&user_totaladd}*/
 
/{&user_numbertotals}*/
  if v-subtotalup[v-inx4] = 1 then
    v-val = t-amounts1[v-inx4].
  else 
    v-val = t-amounts1[v-inx4].
  do inz = 1 to 1:
  if inz = 1 then
    v-regval[inz] = t-amounts1[v-inx4].
  else 
    assign inz = inz.
  end.
/{&user_numbertotals}*/
 
/{&user_summaryloadprint}*/
  if v-lookup[v-inx2] = "C" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-arsc.i}
      end. 
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Customer - " + h-lookup + " " + h-genlit.
    end.
  else
      v-summary-lit = v-lookup[v-inx2].
/{&user_summaryloadprint}*/
 
/{&user_summaryloadexport}*/
  if v-inx4 > v-inx2  then 
    do:
    if v-inx4 > 1 then          
      export_rec = export_rec + v-del. 
    assign export_rec = export_rec +  " ".
    if v-cells[v-inx4] = "2" then
      assign export_rec = export_rec +  v-del + " ".
    end. 
  else 
  if v-lookup[v-inx4] = "C" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-arsc.i}
      end. 
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "999999999999" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        assign x-str1 = h-genlit.
        run String_Ready (input x-str1,
                           input-output h-genlit).
        end.
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
     end.
    else
      do:
      if "999999999999" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
    do:
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    assign export_rec = export_rec + v-lookup[v-inx4].
    end.
/{&user_summaryloadexport}*/
 
 
/* TBXR F6 CodeGen End of Includes that are CodeGen Generated  */ 
