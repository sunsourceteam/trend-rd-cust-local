/*sx5.5*/
/* p-oeepi9.i 5/27/97 */
/*si**** Custom ****************************************************************
CLIENT  : 80281 SDI
JOB     : sdi009 Custom invoice print
AUTHOR  : msk
VERSION : 7
CHANGES :
    si01 5/27/97 msk; 80281/sdi009; initial changes
        - remove UPC vendor from header
        - swap ship-to and bill-to in header
        - remove corr/remit from header
        - add remittance stub section fo invoice
        - NOTE:  because of problems with this doc, v-tof has been included
          in the page-top frame
    si02 6/13/97 msk; 80281/sdi00901
        - use a ZZ note to print remit-to address
    si03 08/26/97 jsb; SDI00902
    si04 10/28/97 sv;  sdi027; Upgraded Trend v8
    si05 03/04/99 pcs; sdi065; added code for canadian Currency print
    si06 03/31/99 gc;  sdi065; fixed canadian tax equivalent message
    si07 04/08/99 gc;  sdi065; split pst/gst taxes
    it01 11/21/97 jph: add company headings to invoice
         03/11/09 tah: addon change
         04/22/09 das: promotion line change
*******************************************************************************/

/* p-oeepi1.i 1.16 02/21/94 */
/*h*****************************************************************************
  INCLUDE      : p-oeepi1.i
  DESCRIPTION  : Print logic for printing invoices (oeepi1.p, oeepif1.p)
  USED ONCE?   : No
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    tb 4762 10/14/91 mwb; page#, order #, and shipped date to fit pre-prt
    tb 6889 06/06/92 mwb; replaced oeeh.payamt with oeeh.tendamt
    tb 2821 06/16/92 mkb; display totamt tendered on the original invoice
    tb 5413 06/20/92 mwb; if the header as a write off amount set - add it
        to the down payment display (BO rounding problems when down
        payment is applied).
    07/20/92 mkb; TB# 7191 Alphanumeric changes for custno
    tb 7497 09/01/92 smb; if arao flag says no to credit discounts then rm's and
        credit cr's shouldn't print the Cash Discount
    tb 8561 11/11/92 mms; Display product surcharge label from ICAO
    tb 9041 12/07/92 ntl; Canadian tax change
    tb 10838 04/07/93 jlc; Print Drop Ship on DOs
    04/28/93 rs; TB# 6404 Negative addon on RM update incorrect
    02/21/94 dww; TB# 13890 File name not specified causing error

    TAH 001 03/11/09 add 11 to printable addons 
    
*******************************************************************************/

def var cSlash         as char                  no-undo.
def var v-appliedamt   like aret.amount         no-undo.
def var v-custno       like aret.custno         no-undo.
def new shared var s-cantot       as decimal               no-undo.
def new shared var s-cantax       as decimal               no-undo.
def new shared var s-cantax1      as decimal               no-undo.
def new shared var s-cantax2      as decimal               no-undo.
def new shared var s-price        as c              no-undo.
def new shared var var-total      as c             init 0 no-undo.
def new shared var v-wtehrecid    as recid                 no-undo.
def var var-total2 as decimal no-undo.
    assign
        v-pageno    = page-number - 1
        v-subnetamt = 0.

form header
    v-tof         format "x(12)"
    skip(1)
    s-lithd1 as c format "x(50)"        at 43   /* beg of it01 */
    s-lithd2 as c format "x(4)"         at 43
    s-phonehd     format "(xxx)xxx-xxxx/xxxx"  at 47
    s-lithd3 as c format "x(4)"         at 67 
    s-faxhd       format "(xxx)xxx-xxxx"  at 71   /* end if it01 */
    s-lit1aa as c format "x(10)"         at 2
    s-lit1a  as c format "x(35)"         at 12
    s-lit1b as c format "x(39)"          at 93
    s-lit2a as c format "x(43)"          at 2
    today                                at 108
    wteh.wtno                            at 123     
    "-"                                  at 130
    wteh.wtsuf                           at 131
    s-lit3a as c format "x(09)"          at 2
    bx-icsd.whse                         at 12 
    s-duplicate as c format "x(25)"      at 53        /* si01; moved down */
    s-lit3b as c format "x(39)"          at 94
    s-financed  as c format "x(25)"      at 53        /* si01; moved down */
    wteh.enterdt                         at 94
/*    oeeh.custpo                          at 104 */
    page-num - v-pageno format "zzz9"    at 129
    /*skip(1)*/
    s-SPlit    as c format "x(13)"       at 89
    s-SPname   as c format "x(30)"       at 103
    s-lit11a as c format "x(8)"          at 2           /* si01 */
    s-billname like arsc.name            at 12          /* si01 */
/*    s-lit6b as c format "x(18)"          at 48
    b-sasc.conm                          at 68              si01 */
    s-radd1                              at 58          /* si02 */
    s-SPaddr1  as c format "x(30)"       at 103
    s-billaddr1 like arsc.name           at 12          /* si01 */
/*    b-sasc.addr[1]                       at 68            si01 */
    s-radd2                              at 58          /* si02 */
    s-SPaddr2  as c format "x(30)"       at 103
    s-billaddr2 like arsc.name           at 12          /* si01 */
/*    b-sasc.addr[2]                       at 68            si01 */
    s-radd3                              at 58          /* si02 */
    s-lit8a as c format "x(9)"           at 106
    s-pstlicense as c format "x(11)"     at 116
    s-billcity as c format "x(35)"       at 12          /* si01 */
/*    s-corrcity as c format "x(35)"       at 68            si01 */
    s-radd4                              at 58          /* si02 */
    s-lit9a as c format "x(9)"           at 106
    s-gstregno as c format "x(10)"       at 116
    s-billcountry as c format "x(30)"    at 12
    /*
    skip(1)
    */
    s-lit6a as c format "x(8)"           at 2           /* si01 */
    s-shiptonm like oeeh.shiptonm        at 12          /* si01 */
    s-lit11b as c format "x(12)"         at 50
    s-shiptoaddr[1]                      at 12          /* si01 */
    wteh.shipinstr                       at 50
    s-shiptoaddr[2]                      at 12          /* si01 */
    s-lit12a as c format "x(10)"         at 50
    s-lit12b as c format "x(3)"          at 81
    s-lit12c as c format "x(7)"          at 105
    s-lit12d as c format "x(5)"          at 117
    s-shipcity as c format "x(35)"       at 12          /* si01 */
    s-whsedesc as c format "x(30)"       at 50
    s-shipvia as c format "x(12)"        at 81
    s-cod as c format "x(6)"             at 94
    s-shipdt like oeeh.shipdt            at 105
    s-terms as c format "x(12)"          at 117
    skip(1)
    s-rpttitle as c format "x(130)"      at 1 skip(1)
with frame f-head no-box no-labels width 132 page-top.

form header
    s-lit14a as c format "x(75)"         at 1
    s-lit14b as c format "x(53)"         at 76
    s-lit15a as c format "x(75)"         at 1
    s-lit15b as c format "x(53)"         at 76
    s-lit16a as c format "x(78)"         at 1
    s-lit16b as c format "x(53)"         at 79
with frame f-headl no-box no-labels width 132 page-top.

form
    skip(1)
    v-linecnt         format "zz9"       at 1
    s-lit40a     as c format "x(11)"     at 5
    s-lit40c     as c format "x(17)"     at 44
    s-totqtyshp  as c format "x(13)"     at 62
    s-lit40d     as c format "x(18)"     at 97
    s-totlineamt as c format "x(13)"     at 117
with frame f-tot1 no-box no-labels no-underline width 132.

form
    s-canlit as c     format "x(60)"     at 30  /*si05*/ /*si07*/
    s-title2 as c format "x(20)"         at 97
    s-amt2   as dec format "zzzzzzzz9.99-" at 117 
    with frame f-tot2 no-box no-labels width 132 no-underline down.

form
    s-currencyty as c format "x(44)"     at 44  /*si05*/
    s-invtitle as c format "x(18)"       at 97
    s-totinvamt like oeeh.totinvamt      at 117
with frame f-tot3 no-box no-labels no-underline width 132.

form header

    /* si01; new stub stuff */
    s-stub1                         to 109
    v-stub1     when s-stub1 <> ""  at 111
    s-stub2                         to 109
    v-stub2     when s-stub1 <> ""  at 111
    s-stub3                         to 109
    v-stub3     when s-stub1 <> ""  at 111
    s-stub4                         to 109
    v-stub4     when s-stub1 <> ""  at 111
    s-stub5                         to 109
    v-stub5     when s-stub1 <> ""  to 130
    s-stub6                         to 109
    s-stub7                         to 109
    s-lit48a as c format "x(9)"          at 1
    s-lit48b as c format "x(13)"         when 0 <> 0  /*si03*/
                                         at 76
    s-termsdiscamt as c format "x(13)"   when 0 <> 0  /*si03*/
                                         at 90
    s-paid   as c format "x(20)"         when 0 <> 0  /*si03*/
                                         at 104
with frame f-tot4 no-box no-labels width 132 page-bottom.

form header                     /*tb 7497 9/1/92 smb*/

    /* si01; new stub stuff */
    s-stub1                         to 109
    v-stub1     when s-stub1 <> ""  at 111
    s-stub2                         to 109
    v-stub2     when s-stub1 <> ""  at 111
    s-stub3                         to 109
    v-stub3     when s-stub1 <> ""  at 111
    s-stub4                         to 109
    v-stub4     when s-stub1 <> ""  at 111
    s-stub5                         to 109
    v-stub5     when s-stub1 <> ""  to 130
    s-stub6                         to 109
    s-stub7                         to 109
    /* si01; end of new stub stuff */

    s-lit48az as c format "x(9)"         at 1
with frame f-tot4z no-box no-labels width 132 page-bottom.

form    /*tb 2821 06/16/92 mkb */
    "Full Amount Tendered For All Orders:"  at 5
    0                                       at 42
    "*** Back Order/Release Exists ***"             at 5
with frame f-tot5 no-box no-labels width 132.
    /*d Load the labels for the invoice print */

    {a-wtxpp9.i s-}                                     /* si01 */
    
    assign s-phonehd = icsd.phoneno
           s-faxhd   = icsd.faxphone.
    
    
    
    v-totlineamt = if wteh.stagecd  < 2 then
                        wteh.totordamt
                   else
                        wteh.totshipamt.
    assign s-cantot = 0
           s-cantax1 = 0
           s-cantax2 = 0.
    /*d Load header fields */
    {w-sasta.i "S" wteh.shipviaty no-lock}
    assign s-billname   = bx-icsd.name
           s-billaddr1  = bx-icsd.addr[1]
           s-billaddr2  = bx-icsd.addr[2]
           s-billcity   = bx-icsd.city + ", " + bx-icsd.state + " " 
                          + bx-icsd.zipcd
           s-billcountry = if avail bx-icsd and
                             substr(bx-icsd.whse,2,1) = "x" then 
                             "CANADA"
                           else " "
  

           s-duplicate  =  "*** C U S T O M S ***"
           s-financed   =  " "
           s-cod        = " "
           s-shipdt     = if wteh.shipdt = ? then 
                             today 
                          else wteh.shipdt
           s-totlineamt = string(v-totlineamt,"zzzzzzzz9.99-")
           s-taxes      = 0
           s-dwnpmtamt  = 0
           /*tb 5413 06/20/92 mwb */
           s-dwnpmtamt  = s-dwnpmtamt + 0
           v-linecnt    = 0
           s-paid         = ""   /*si03*/
           s-termsdiscamt = ""   /*si03*/
           s-lit48b       = ""   /*si03*/
           s-shiptonm      = wteh.shiptonm
           s-shiptoaddr[1] = wteh.shiptoaddr[1]
           s-shiptoaddr[2] = wteh.shiptoaddr[2]
           s-shipcity      = wteh.shiptocity + ", " + wteh.shiptost +
                             " " + wteh.shiptozip
/* bb
           s-gstregno      = if g-country = "ca" then b-sasc.fedtaxid else ""
           s-pstlicense    = if g-country = "ca" then oeeh.pstlicenseno
                             else ""
*/
           s-shipvia       = if avail sasta then 
                               sasta.descrip 
                             else wteh.shipviaty
           /*tb 10838 04/07/93 jlc; Print Drop ship on DOs */
           s-whsedesc      = if wteh.transtype = "do" then
                                "** Drop Ship **"
                             else
                             if avail icsd  then 
                               icsd.name   
                             else 
                               wteh.shipfmwhse
           s-SPlit         = if avail icsd then
                               "Shipped From:"
                             else " "
           s-SPname        = if avail icsd then
                               icsd.name
                             else " "
           s-SPaddr1       = if avail icsd then
                               icsd.addr[1]
                             else " "
           s-SPaddr2       = if avail icsd then
                               string(icsd.city  + ", " +
                                      icsd.state + " "  + icsd.zipcd,"x(30)")
                             else " ".
                  
    assign s-rpttitle = "".

    /*d Load addon descriptions based on language */
    
    
    /*{p-oeadfp.i}*/
    s-terms = " ".
    view frame f-head.
    view frame f-headl.
    s-lit48az = s-lit48a.

    /* si01; start with clear stub text */
    assign
        s-stub1 = ""
        s-stub2 = ""
        s-stub3 = ""
        s-stub4 = ""
        s-stub5 = ""
        s-stub6 = ""
        s-stub7 = ""
        v-stub1 = ""
        v-stub2 = ""
        v-stub3 = ""
        v-stub4 = ""
        v-stub5 = "".

    view frame f-tot4.

 /*d Print the customer notes  
 if arsc.notesfl ne "" then
   run notesprt.p(g-cono,"c",string(oeeh.custno),"","oeepi",8,5).
             
    /*d Print the shipto notes  */
   /*tb #5366 05/14/99 lbr; Added shipto notes */
 if avail arss and arss.notesfl ne "" then
   run notesprt.p(g-cono,"cs",string(oeeh.custno),oeeh.shipto,
                        "oeepi",8,5).
*/                    
   /*d Print the order notes  */
 if wteh.notesfl ne "" then
   run notesprt.p(g-cono,"t",string(wteh.wtno),string(wteh.wtsuf),
                        "oeepi",8,5).
                          
 assign v-totqtyshp = 0
 v-wtehrecid = recid(wteh).

 run wtxpp9l.p.                          /* si01 */
 s-totlineamt = string(v-totlineamt * 1.129,"zzzzzzzz9.99-").

 /*d******** Print the order totals ********************/
    s-totqtyshp = if substring (string(v-totqtyshp,"999999999.99-"),11,2) = "00"
                   then string(v-totqtyshp,"zzzzzzzz9-")
                   else string(v-totqtyshp,"zzzzzzzz9.99-").

    clear frame f-tot1.
    display v-linecnt       s-lit40a    s-lit40c
            s-totqtyshp     s-lit40d    s-totlineamt 
            with frame f-tot1.

        
     assign s-amt2 = 0.
    clear frame f-tot3.
    /*tb 5413 06/20/92 mwb; added the writeoffamt to totinvamt */
     assign s-totinvamt = v-totlineamt
            s-invtitle  = s-lit47a.
           
    /*si05 added s-currencyty*/
    s-currencyty = "**** INVOICE REFLECTS U.S. CURRENCY ****".
    s-totinvamt = (s-totinvamt * 1.129). /*si05*/   
    display s-invtitle s-currencyty s-totinvamt  with frame f-tot3.

    
    assign s-lit48a  = "Last Page"
           s-lit48az = "Last Page"
           s-lit48b  = ""
           s-termsdiscamt = ""
           s-paid       = "".
    /* si01; set stub text and info on last page */
    assign
        s-stub1 = "Customer Name :"
        s-stub2 = "Customer No. :"
        s-stub3 = "Invoice Date :"
        s-stub4 = "Invoice No. :"
        s-stub5 = "Invoice Amt. :"
        s-stub6 = "Amt. Remitted :"
        s-stub7 = "Check No. :"
        v-stub1 = bx-icsd.name
        v-stub2 = string(bx-icsd.whse)
        v-stub3 = string(if wteh.shipdt = ? then 
                           today 
                         else 
                           wteh.shipdt)
        v-stub4 = string(wteh.wtno,">>>>>>9") +
                  "-" +
                  string(wteh.wtsuf,"99")
        v-stub5 = string(s-totinvamt,"zzzzzzzz9.99-").
        
