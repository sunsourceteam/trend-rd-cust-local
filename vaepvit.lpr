/****************************************************************************
 Include: vaepvit.lpr.
          Variables that must be loaded for this include are:
    Required parameters used in this include are:
     &stitle    - Used to display appropriate section header
     &qthead    - The Returned Quantity heading
     &xdash1    - Extra dashes required to underscore &qthead
     &xdash2    - Extra dashes to mark a space for write-in of returned qty
    Optional parameters used in this include are:
****************************************************************************/
/* message "running vaepvit.p 22". pause. */


if vaspsl.nonstockty ne "n" then 
 {w-icsp.i vaspsl.compprod no-lock}

if vaspsl.prodcat ne "" then do:                                                 find notes use-index k-notes where                              
      notes.cono         = g-cono and                            
      notes.notestype    = "zz" and                              
      notes.primarykey   = "standard cost markup" and            
      notes.secondarykey = vaspsl.prodcat and                         
      notes.pageno       = 1                                     
      no-lock no-error.                                             
end.
if not avail notes then do:
 find notes use-index k-notes where                          
      notes.cono         = g-cono and                        
      notes.notestype    = "zz" and                          
      notes.primarykey   = "standard cost markup" and        
      notes.secondarykey = "" and                            
      notes.pageno       = 1                                 
      no-lock no-error.                                         
end.

if v-vafstinvfl = yes then do: 
    /*
    assign
        v-vafstinvfl   = no
        s-vadata       = {&stitle}.
    display s-vadata with frame f-vadata.
    down with frame f-vadata.

    assign
         s-vadata                  = "   "
         substring(s-vadata,1,32)  = "--------------------------------"
         substring(s-vadata,33,1) = {&xdash1}. 
     display s-vadata with frame f-vadata.
     down with frame f-vadata.
    */
 end.

assign s-lncolon      = ":" 
       s-lnlineno     = vaspsl.lineno
       s-lnshipprod   = if p-desconly then                        
                          ""                                     
                        else                                      
                        if substring(vaspsl.user5,1,24) ne "" then 
                           substring(vaspsl.user5,1,24)            
                        else   
                           vaspsl.compprod
       s-lncostty     = " "
       s-lnhours      = if can-do("it,is",vasps.sctntype) then 
                                truncate(vaspsl.timeelapsed / 3600,0)
                        else 0
       s-lnminutes    = if can-do("it,is",vasps.sctntype) then 
                                 (vaspsl.timeelapsed mod 3600) / 60    
                        else 0                                 
       s-lnprodcost   = string(dec(vaspsl.prodcost * 1.04))
       s-lnproddesc   = vaspsl.proddesc + vaspsl.proddesc2
       s-lnprodcost   = string(dec(s-lnprodcost),"zzzzzz9.99").

                                 
if not p-prtprc then 
 assign s-lnnetamt = "".
else    
 assign s-lnnetamt = 
        string((dec(s-lnminutes) * round(dec(s-lnprodcost),2) / 60) +
         (dec(s-lnhours) * round(dec(s-lnprodcost),2)),"999999.99")
        s-lnnetamt = if p-margpct > 0 then 
                      string(((dec(s-lnnetamt) / p-margpct)) * 100) 
                     else 
                      s-lnnetamt
        s-lnnetamt = string(dec(s-lnnetamt),"zzzzzz9.99"). 
      
 /* vaspsl.timeelapsed = (s-hours * 60 * 60) + (s-minutes * 60) */
                          
if p-detail then 
  display 0 @ s-lnlineno   format "zzz"   
              s-lnshipprod  
              s-lncostty      
              s-lnhours     
              s-lncolon
              s-lnminutes   
              s-lnprodcost  
              s-lnnetamt    
              s-lnproddesc  
          with frame f-vaepq1it.
          down with frame f-vaepq1it.
/*
   if vaspsl.compprod = "test time" or 
      vaspsl.compprod = "service repair" then 
      assign qa-miscamt    = qa-miscamt    + dec (s-lnprodcost)
             qa-misctotamt = qa-misctotamt + dec(s-lnnetamt).
   else 
*/ 
      
 assign ittot = ittot + dec(s-lnnetamt). 
