Procedure Check_for_Dups:
  define input parameter        ip-tblname     as char     no-undo.
  define input parameter        ip-recno       as int      no-undo.

  define input-output parameter ip-city        as char     no-undo.
  define input-output parameter ip-state       as char     no-undo.
  define input-output parameter ip-zipcd       as char     no-undo.


  define input-output parameter ip-h-errorcd   as char     no-undo.
  define input-output parameter ip-errorlit    as char     no-undo.


  find b-t-data where 
       b-t-data.recno = ip-recno and
       b-t-data.name  = "custno" no-lock no-error.
  find arsc where 
       arsc.cono   = g-cono and
       arsc.custno = dec(b-t-data.fieldval) no-lock no-error.
  if avail arsc then do:      
      assign ip-h-errorcd  = "U"
             ip-errorlit   = "Duplucate customer record"
             ip-city       = arsc.city
             ip-state      = arsc.state
             ip-zipcd      = arsc.zipcd.
  end.

end.

Procedure Acquire_Recid:
  define input parameter        ip-tblname     as char     no-undo.
  define input parameter        ip-recno       as int      no-undo.
  define input-output parameter ip-rowid       as rowid    no-undo.

  find b-t-data where 
       b-t-data.recno = ip-recno and
       b-t-data.name  = "custno" no-lock no-error.
  find arsc where 
       arsc.cono   = g-cono and
       arsc.custno = dec(b-t-data.fieldval) no-lock no-error.
  if avail arsc then do:      
      assign ip-rowid = rowid(arsc).
  end.

end.





Procedure Other_supporting_Edits:
  define input parameter        ip-tblname     as char     no-undo.
  define input parameter        ip-field-name  as char     no-undo.
  
  define input-output parameter ip-field-value      as char     no-undo.
  define input-output parameter ip-field-value-orig as char     no-undo.
  
  define input-output parameter ip-d-errorcd   as char     no-undo.
  define input-output parameter ip-h-errorcd   as char     no-undo.
  define input-output parameter ip-errorlit    as char     no-undo.
 

  
  if ip-field-name = "countrycd" and ip-field-value <> "" then do:
    find sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "W" and
         sasta.codeval  = ip-field-value no-lock no-error.
    if not avail sasta then do:
      create sasta.
      assign 
        sasta.cono = g-cono 
        sasta.codeiden = "W" 
        sasta.codeval  = ip-field-value 
        sasta.descrip  = "t".
    end.
    else
    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "CountryCd is not set up in SASTT".
    end.
  end.


  if ip-field-name = "taxablety" then do:
    
    if ip-field-value = " " or ip-field-value = "0" then do:
      assign ip-field-value-orig = ip-field-value
             ip-field-value      = "N".
     
    
    end.
    else
    if ip-field-value = "1"  then do:
      assign ip-field-value-orig = ip-field-value
             ip-field-value      = "Y".
    end.         
    else do:
      assign ip-field-value-orig = ip-field-value
             ip-field-value      = "N".
    end.
  
  end.


  
  if ip-field-name = "termstype" then do:
    find first sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "T" and
         sasta.user5    = ip-field-value no-lock no-error.

    if avail sasta and sasta.codeval <> ip-field-value then
      assign ip-field-value-orig = ip-field-value
             ip-field-value      = sasta.codeval.

    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "TermsType is not set up in SASTT".
    end.
  end.



  if ip-field-name = "consolterms" and 
     ip-field-value <> "" then do:
    find sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "T" and
         sasta.codeval  = ip-field-value no-lock no-error.
    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Consolterms is not set up in SASTT".
    end.
  end.


  if ip-field-name = "langcd" and ip-field-value <> "" then do:
    find sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "Y" and
         sasta.codeval  = ip-field-value no-lock no-error.
    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Languag code is not set up in SASTT".
    end.
  end.


  if ip-field-name = "custtype" and ip-field-value <> "" then do:
    find sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "CU" and
         sasta.codeval  = ip-field-value no-lock no-error.
    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "CustType is not set up in SASTT".
    end.
  end.

  if ip-field-name = "shipviaty" and ip-field-value <> "" then do:
    find first sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "s" and
         sasta.user5  = ip-field-value no-lock no-error.

 
    if avail sasta and sasta.codeval <> ip-field-value then do:
      assign ip-field-value-orig = ip-field-value
             ip-field-value      = sasta.codeval.
    end.

    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Ship Via type is not set up in SASTT".
    end.
  end.


  if ip-field-name = "salesterr" and ip-field-value <> "" then do:
    find sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "Z" and
         sasta.codeval  = ip-field-value no-lock no-error.
    if not avail sasta then do:
      create sasta.
      assign 
        sasta.cono = g-cono 
        sasta.codeiden = "Z" 
        sasta.codeval  = ip-field-value 
        sasta.descrip  = "t".
    end.
    else
    
    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "SalesTerr is not set up in SASTT".
      message "salesterr". pause.       
    end.
  end.


  if ip-field-name = "pricetype" and ip-field-value <> "" then do:
    find sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "J" and
         sasta.codeval  = ip-field-value no-lock no-error.
    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "PriceType is not set up in SASTT".
    end.
  end.

  if ip-field-name = "nontaxtype" and ip-field-value <> "" then do:
    find sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "N" and
         sasta.codeval  = ip-field-value no-lock no-error.
    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "NonTaxType is not set up in SASTT".
    end.
  end.



  if ip-field-name = "creditmgr" and ip-field-value <> "" then do:
    find sasoo where
         sasoo.cono = g-cono and
         sasoo.oper2  = ip-field-value no-lock no-error.
    if not avail sasoo then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Credit Manager is not set up in SASOO".
    end.
  end.


  if ip-field-name = "divno" and ip-field-value <> "" then do:
    find sastn where
         sastn.cono = g-cono and
         sastn.codeiden = "V" and
         sastn.codeval  = int(ip-field-value) no-lock no-error.
    if not avail sastn then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Divno is not set up in SASTT".
    end.
  end.

  if ip-field-name = "mediacd" and int(ip-field-value) <> 0 then do:
    find sastn where
         sastn.cono = g-cono and
         sastn.codeiden = "P" and
         sastn.codeval  = int(ip-field-value) no-lock no-error.
    if not avail sastn then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "MediaCd is not set up in SASTT".
    end.
  end.


  if ip-field-name = "addonnum" and int(ip-field-value) <> 0 then do:
    find sastn use-index k-sastn where
         sastn.cono     = g-cono and
         sastn.codeiden = "a":u  and
         sastn.codeval  = int(ip-field-value) and
         sastn.catppt   = no
        no-lock no-error.
    if not avail sastn then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Addon type is not set up in SASTO".
    end.
  end.







  if ip-field-name = "currencyty" and ip-field-value <> "" then do:
    find sastc where
         sastc.cono        = g-cono and
         sastc.currencyty  = ip-field-value no-lock no-error.
    if ip-field-value = "US" then
      assign ip-field-value-orig = ip-field-value
             ip-field-value      = " ".
    else
    if not avail sastc then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Currencyty is not set up in SASTC".
    end.
  end.

  if ip-field-name = "bankno" and ip-field-value <> "" then do:
    find crsb where
         crsb.cono    = g-cono and
         crsb.bankno = int(ip-field-value) no-lock no-error.
    if not avail crsb then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Bank not set up in CRSB".
    end.
  end.


  if ip-field-name = "slsrepout" and ip-field-value <> "" then do:
    find smsn where
         smsn.cono = g-cono and
         smsn.slsrep  = ip-field-value no-lock no-error.
    if not avail smsn then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "SlsRepOut is not set up in SMSN".
    end.
  end.


  if ip-field-name = "slsrepin" and ip-field-value <> "" then do:
    find smsn where
         smsn.cono = g-cono and
         smsn.slsrep  = ip-field-value no-lock no-error.
    if not avail smsn then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "SlsRepIn is not set up in SMSN".
    end.
  end.



  if ip-field-name = "whse" and ip-field-value <> "" then do:
    find icsd where
         icsd.cono = g-cono and
         icsd.whse  = ip-field-value no-lock no-error.
    if not avail icsd then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Whse is not set up in ICSD".
    end.
  end.



  if ip-field-name = "rebatety" and ip-field-value <> "" then do:
    find pdst where
         pdst.cono = g-cono and
         pdst.codeiden = "CT" and
         pdst.codeval  = ip-field-value no-lock no-error.
    if not avail pdst then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Rebatety is not set up in PDST".
    end.
  end.

/*
  if ip-field-name = "statecd" and ip-field-value <> "" then do:
    find sasgm where
         sasgm.cono = g-cono and
         sasgm.recty = 2 and
    if not avail sasgm then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Rebatety is not set up in PDST".
      message "rebatety". pause.       
    end.
  end.
*/


end.


Procedure Ancillary_Tables:
  Define input parameter ip-recid            as recid no-undo.


  def var   ip-city   as char                  no-undo.       
  def var   ip-state  as char                  no-undo.       
  def var   ip-zipcd  as char                  no-undo.       
  def var   ip-geocd  as char                  no-undo.       
  def var   ip-okfl   as l                     no-undo.       
  def var   ip-errorfl    as l                 no-undo.       
  def var   ip-errorno    as i                 no-undo.       
 
  define buffer slsmx  for notes.
  define buffer x-slsmx  for notes.
  define buffer slslb  for notes.
  define buffer bip-arsc for arsc.
  define buffer bip-arss for arss.


  find bip-arsc where recid(bip-arsc) = ip-recid no-error.
  if avail bip-arsc then do:
    assign bip-arsc.edipartner  = string(bip-arsc.custno)
           bip-arsc.statecd     = bip-arsc.state.
    assign bip-arsc.user10 = "NN"
           bip-arsc.xxl13  = yes
           bip-arsc.xxl2  = no. 

    assign bip-arsc.lookupnm = replace(bip-arsc.lookupnm,"`","'").
    assign bip-arsc.name     = replace(bip-arsc.name,"`","'").
    
    if bip-arsc.nontaxtype = " " and bip-arsc.taxablety = "N" then
       assign bip-arsc.nontaxtype  = "P".
    else if bip-arsc.taxablety = "Y" then
       assign bip-arsc.nontaxtype = " ".

    run Country_Check (input-output bip-arsc.countrycd, 
                       input-output bip-arsc.zipcd,
                       input-output bip-arsc.state,
                       input-output bip-arsc.shipinstr).

    if   
      ( bip-arsc.countrycd <> "CA" and
        bip-arsc.countrycd <> "US") then do:
      assign /* Iternational Customers get IT */
            bip-arsc.shipinstr = 
              bip-arsc.countrycd +  " " + bip-arsc.zipcd.             
      assign
        bip-arsc.countrycd = "IT" 
        bip-arsc.zipcd = "00000".
    end.
  
    assign bip-arsc.statecd = bip-arsc.state.
     
    if bip-arsc.whse = "CSE"  then
      assign bip-arsc.divno = 040  /* Canada is a separate Division */
             bip-arsc.bankno = 40.
    else  
    if bip-arsc.whse <> "CSE"  then do:
      find icsd where 
           icsd.cono = g-cono and
           icsd.whse = bip-arsc.whse no-lock no-error.
      if avail icsd then 
        assign bip-arsc.divno = icsd.divno
               bip-arsc.bankno = if icsd.divno = 29 then
                                   3
                                 else
                                   1.
      else
        assign bip-arsc.divno = 010  /* Canada is a separate Division */
               bip-arsc.bankno = 1.
    end.
 
    overlay (bip-arsc.user19,1,4) = if bip-arsc.divno = 29 then
                                      "PERF"
                                    else
                                      "PGON".
     assign bip-arsc.creditmgr     = if bip-arsc.divno = 29 then
                                       "PERF"
                                     else
                                       bip-arsc.creditmgr.
     assign ip-city = bip-arsc.city
           ip-state = bip-arsc.state
           ip-zipcd = bip-arsc.zipcd
           ip-geocd = string(bip-arsc.geocd).
     run zsdiGeoChk.p (                                                                              input ip-city,
                    input ip-state, 
                    input ip-zipcd,                                   
                    output ip-geocd,                                     
                    output ip-okfl,                                            
                    output ip-errorfl,                                         
                    output ip-errorno).                                         
   if not ip-errorfl then                                                             assign bip-arsc.geocd = int (ip-geocd). 

 

        /* bip-arsc.lookupnm    = substring(bip-arsc.name,1,15) */
    
    run  makeslsmx (input bip-arsc.cono).

    find slslb  where slslb.cono = bip-arsc.cono       and 
                      slslb.notestype    = "zz"    and
                      slslb.primarykey   = "slsmx" and
                      slslb.secondarykey = "label"
                      no-lock no-error. 
    find slsmx  where slsmx.cono         = bip-arsc.cono and 
                      slsmx.notestype    = "zz"      and
                      slsmx.primarykey   = "slsmx"   and
                      slsmx.secondarykey = string(arsc.custno,"99999999999")
                      no-lock no-error. 
    
    if avail slslb and not avail slsmx then do: 
      create slsmx. 
      assign slsmx.cono         = bip-arsc.cono  
             slsmx.printfl4     = no 
             slsmx.notestype    = "zz" 
             slsmx.primarykey   = "slsmx" 
             slsmx.secondarykey = string(bip-arsc.custno,"99999999999").
      assign bip-arsc.user10 = "NN"
             bip-arsc.xxl13  = yes
             bip-arsc.xxl2  = no 
             slsmx.xxl12 = yes
             slsmx.noteln[16]   = " "
             slsmx.noteln[1]    = " " 
             slsmx.noteln[2]    = " " 
             slsmx.noteln[3]    = " " 
             slsmx.noteln[4]    = " " 
             slsmx.noteln[5]    = " " 
             slsmx.noteln[6]    = " " 
             slsmx.noteln[7]    = " " 
             slsmx.noteln[8]    = " " 
             slsmx.noteln[9]    = " ".
    end. 
    if bip-arsc.state = "CA" and bip-arsc.divno <> 29 then do:
      find bip-arss where
           bip-arss.cono   = bip-arsc.cono and 
           bip-arss.custno = bip-arsc.custno and
           bip-arss.shipto = "CaStep" no-lock no-error.
             
      if not avail bip-arss then do:
        run build_arss_CaStep ( input bip-arsc.cono,
                                input bip-arsc.custno).
      end.
    end.
 end.
 release bip-arsc.
end.

Procedure Ancillary_Tables_Update:
  Define input parameter ip-recid            as recid no-undo.
  Define input parameter ip-ocity            as char  no-undo.
  Define input parameter ip-ostate           as char  no-undo.
  Define input parameter ip-ozipcd           as char  no-undo.




  def var   ip-city   as char                  no-undo.       
  def var   ip-state  as char                  no-undo.       
  def var   ip-zipcd  as char                  no-undo.       
  def var   ip-geocd  as char                  no-undo.       
  def var   ip-okfl   as l                     no-undo.       
  def var   ip-errorfl    as l                 no-undo.       
  def var   ip-errorno    as i                 no-undo.       
  
  
  define buffer slsmx  for notes.
  define buffer x-slsmx  for notes.
  define buffer slslb  for notes.
  define buffer bip-arsc for arsc.
  define buffer bip-arss for arss.


  find bip-arsc where recid(bip-arsc) = ip-recid no-error.
  if avail bip-arsc then do:
    assign bip-arsc.edipartner  = string(bip-arsc.custno)
           bip-arsc.statecd     = bip-arsc.state.

    assign bip-arsc.lookupnm = replace(bip-arsc.lookupnm,"`","'").
    assign bip-arsc.name     = replace(bip-arsc.name,"`","'").
        
    run Country_Check (input-output bip-arsc.countrycd, 
                        input-output bip-arsc.zipcd,
                        input-output bip-arsc.state,
                        input-output bip-arsc.shipinstr).
    if bip-arsc.whse = "CSE" then
      assign bip-arsc.divno = 040  /* Canada is a separate Division */
             bip-arsc.bankno = 40.
    else
    if bip-arsc.whse <> "CSE"  then do:
      find icsd where 
           icsd.cono = g-cono and
           icsd.whse = bip-arsc.whse no-lock no-error.
      if avail icsd then 
        assign bip-arsc.divno = icsd.divno
               bip-arsc.bankno = if icsd.divno = 29 then
                                   3
                                 else
                                   1.
      else
        assign bip-arsc.divno = 010  /* Canada is a separate Division */
               bip-arsc.bankno = 1.
    end.
    
    overlay (bip-arsc.user19,1,4) = if bip-arsc.divno = 29 then
                                      "PERF"
                                    else
                                      "PGON".
 
      assign bip-arsc.creditmgr     = if bip-arsc.divno = 29 then
                                       "PERF"
                                     else
                                       bip-arsc.creditmgr.
     if   
      ( bip-arsc.countrycd <> "CA" and
        bip-arsc.countrycd <> "US") then do:
      assign /* Iternational Customers get IT */
            bip-arsc.shipinstr = 
              bip-arsc.countrycd +  " " + bip-arsc.zipcd.             
      assign
        bip-arsc.state     = "IT"
        bip-arsc.countrycd = "IT" 
        bip-arsc.zipcd = "00000".
    end.

    if bip-arsc.nontaxtype = " " and bip-arsc.taxablety = "N" then
       assign bip-arsc.nontaxtype  = "P".
    else if bip-arsc.taxablety = "Y" then
       assign bip-arsc.nontaxtype = " ".
    assign bip-arsc.statecd = bip-arsc.state.
    if ip-ocity  <> bip-arsc.city   or
       ip-ostate <> bip-arsc.state or
       ip-ozipcd <> bip-arsc.zipcd then do:
      assign ip-city = bip-arsc.city
             ip-state = bip-arsc.state
             ip-zipcd = bip-arsc.zipcd
             ip-geocd = string(bip-arsc.geocd).
      run zsdiGeoChk.p (                                                                                input ip-city,
                        input ip-state, 
                        input ip-zipcd,                                   
                        output ip-geocd,                                     
                        output ip-okfl,                                                                  output ip-errorfl,                                                               output ip-errorno).                                           if not ip-errorfl then                                                            assign bip-arsc.geocd = int (ip-geocd). 
     end.


        /* bip-arsc.lookupnm    = substring(bip-arsc.name,1,15) */
    
    run  makeslsmx (input bip-arsc.cono).

    find slslb  where slslb.cono = bip-arsc.cono       and 
                      slslb.notestype    = "zz"    and
                      slslb.primarykey   = "slsmx" and
                      slslb.secondarykey = "label"
                      no-lock no-error. 
    find slsmx  where slsmx.cono         = bip-arsc.cono and 
                      slsmx.notestype    = "zz"      and
                      slsmx.primarykey   = "slsmx"   and
                      slsmx.secondarykey =
                         string(bip-arsc.custno,"99999999999")
                      no-lock no-error. 
    
    if avail slslb and not avail slsmx then do: 
      create slsmx. 
      assign slsmx.cono         = bip-arsc.cono  
             slsmx.printfl4     = no 
             slsmx.notestype    = "zz" 
             slsmx.primarykey   = "slsmx" 
             slsmx.secondarykey = string(bip-arsc.custno,"99999999999").
      assign bip-arsc.user10 = "NN"
             bip-arsc.xxl13  = yes
             bip-arsc.xxl2  = no 
             slsmx.xxl12 = yes
             slsmx.noteln[16]   = " "
             slsmx.noteln[1]    = " " 
             slsmx.noteln[2]    = " " 
             slsmx.noteln[3]    = " " 
             slsmx.noteln[4]    = " " 
             slsmx.noteln[5]    = " " 
             slsmx.noteln[6]    = " " 
             slsmx.noteln[7]    = " " 
             slsmx.noteln[8]    = " " 
             slsmx.noteln[9]    = " ".
    end.
    if bip-arsc.state = "CA" and bip-arsc.divno <> 29 then do:
      find bip-arss where
           bip-arss.cono   = bip-arsc.cono and 
           bip-arss.custno = bip-arsc.custno and
           bip-arss.shipto = "CaStep" no-lock no-error.
             
      if not avail bip-arss then do:
        run build_arss_CaStep ( input bip-arsc.cono,
                                input bip-arsc.custno).
      end.
    end.
     

 end.
 release bip-arsc.
end.


procedure makeslsmx:
define input parameter ip-cono as int no-undo.

def buffer b-notes for notes. 

    
find first notes  where notes.cono = ip-cono         and 
                  notes.notestype    = "zz"    and
                  notes.primarykey   = "slsmx" and
                  notes.secondarykey = "label"
                  no-lock no-error.
if avail notes then return.                   
    
for each notes  where 
         notes.cono = 1       and 
         notes.notestype    = "zz"    and
         notes.primarykey   = "slsmx" and
         notes.secondarykey = "label"   no-lock: 
  create b-notes.
   buffer-copy notes except notes.cono
  to b-notes
     assign b-notes.cono = ip-cono.
  end.
end.  

Procedure Country_check:
  define input-output parameter ip-countrycd like arsc.countrycd no-undo.
  define input-output parameter ip-zipcd     like arsc.zipcd     no-undo.
  define input-output parameter ip-state     like arsc.state     no-undo.
  define input-output parameter ip-instr     like arsc.shipinstr no-undo.

  define var v-CAcityname  as char format "x(26)" no-undo.
  define var v-CAstatecode as char format "x(2)"no-undo.
  define var vi-inx as integer no-undo.

   
 
 if ip-countrycd <> "CA" and
    ip-countrycd <> "US" and 
    ip-countrycd <> ""   and
    ip-countrycd <> "IT" then do:

    assign /* Iternational Customers get IT */
            ip-instr = 
              ip-countrycd +  " " + ip-zipcd.               
      assign
        ip-state     = "IT"
        ip-countrycd = "IT" 
        ip-zipcd = "00000".

    end.
 else 
 if ip-countrycd =  ""   then do:
   if ip-countrycd = "" and ip-zipcd <> "" then do:
     if substring(ip-zipcd,1,5) <> "00000":U then
       zip:
       do vi-inx = 1 to 5:
      /*d A US zip code must have integers in the first five positions */
         if substring(ip-zipcd,vi-inx,1) >= '0':U and
                     substring(ip-zipcd,vi-inx,1) <= '9':U  then
           ip-countrycd = "US":u.
         else do:
           leave zip.
         end.
      end. /* check 1st five positions of zip code */
            
   /*d If the address is not a US address, check if it is Canadian */
      if ip-countrycd = "" then do:
        run ticachk.p (input  caps(ip-state),
                       input  substring(ip-zipcd, 1, 5),
                       output v-CAcityname,
                       output v-CAstatecode).
        if v-CAcityname  <> "" or
           v-CAstatecode <> "" then
          ip-countrycd = "CA":u.
      end. /* if arsc.countrycd = "" - Canada check */
    end.
  end.
end. 

  

/* California taxes */
    
    Procedure build_arss_CaStep:
  
      define input parameter         ip-cono   like arsc.cono   no-undo.
      define input parameter         ip-custno like arsc.custno no-undo.
     
      
      define buffer pb-arss  for arss.
      define buffer pb-arsc  for arsc.
 
      define buffer pb2-arss for arss.
      
      find notes where notes.cono = ip-cono and
                       notes.notestype = "ZZ" and
                       notes.primarykey = "JT Integration" no-lock no-error.
      if avail notes then do:

        find pb-arsc where 
             pb-arsc.cono   = ip-cono    and                 
             pb-arsc.custno = ip-custno no-lock no-error.
        if avail pb-arsc then do:
          create pb2-arss.
          assign
            pb2-arss.addonno[1]            = pb-arsc.addonno[1]
            pb2-arss.addonno[2]            = pb-arsc.addonno[2]
            pb2-arss.addonno[3]            = pb-arsc.addonno[3]
            pb2-arss.addonno[4]            = pb-arsc.addonno[4]
            pb2-arss.addonnum[1]           = pb-arsc.addonnum[1]
            pb2-arss.addonnum[2]           = pb-arsc.addonnum[2]
            pb2-arss.addonnum[3]           = pb-arsc.addonnum[3]
            pb2-arss.addonnum[4]           = pb-arsc.addonnum[4]
            pb2-arss.addonnum[5]           = pb-arsc.addonnum[5]
            pb2-arss.addonnum[6]           = pb-arsc.addonnum[6]
            pb2-arss.addonnum[7]           = pb-arsc.addonnum[7]
            pb2-arss.addonnum[8]           = pb-arsc.addonnum[8]
            pb2-arss.addr[1]               = pb-arsc.addr[1]
            pb2-arss.addr[2]               = pb-arsc.addr[2]
            pb2-arss.addr3                 = pb-arsc.addr3
            pb2-arss.bofl                  = pb-arsc.bofl
            /* pb2-arss.bondedfl              = pb-arsc.bondedfl   */
            /* pb2-arss.bondno                = pb-arsc.bondno     */
            pb2-arss.ccexp                 = pb-arsc.ccexp
            pb2-arss.ccno                  = pb-arsc.ccno
            pb2-arss.city                  = pb-arsc.city
            pb2-arss.citycd                = pb-arsc.citycd
            pb2-arss.cono                  = pb-arsc.cono
            pb2-arss.consolformat          = pb-arsc.consolformat
            pb2-arss.consolinterval        = pb-arsc.consolinterval
            pb2-arss.consolinvty           = pb-arsc.consolinvty
            pb2-arss.consolterms           = pb-arsc.consolterms
            pb2-arss.countrycd             = pb-arsc.countrycd
            pb2-arss.countycd              = pb-arsc.countycd
            pb2-arss.credlim               = 0
            pb2-arss.custno                = pb-arsc.custno
            pb2-arss.custpo                = pb-arsc.custpo
            /* pb2-arss.custshipto            = pb-arsc.custshipto  */
            pb2-arss.disccd                = pb-arsc.disccd
            pb2-arss.dunsno                = pb-arsc.dunsno
            /* pb2-arss.eackto                = pb-arsc.eackto      */
            pb2-arss.eacktype              = pb-arsc.eacktype
            pb2-arss.easngrp               = pb-arsc.easngrp
            pb2-arss.easnto                = pb-arsc.easnto
            pb2-arss.ecommwhse             = pb-arsc.ecommwhse
            pb2-arss.edi840fl              = pb-arsc.edi840fl
            pb2-arss.ediackver             = pb-arsc.ediackver
            pb2-arss.edicatprodfl          = pb-arsc.edicatprodfl
            pb2-arss.edichgcd              = pb-arsc.edichgcd
            pb2-arss.edictrlno             = pb-arsc.edictrlno
            pb2-arss.edienvtag[1]          = pb-arsc.edienvtag[1]
            pb2-arss.edienvtag[2]          = pb-arsc.edienvtag[2]
            pb2-arss.ediinvver             = pb-arsc.ediinvver
            pb2-arss.edijitfl              = pb-arsc.edijitfl
            pb2-arss.edinetwork            = pb-arsc.edinetwork
            pb2-arss.edinsprodfl           = pb-arsc.edinsprodfl
            pb2-arss.ediordcd              = pb-arsc.ediordcd
            pb2-arss.edipartaddr           = pb-arsc.edipartaddr
            pb2-arss.edipartner            = " "
            pb2-arss.ediprcfl              = pb-arsc.ediprcfl
            pb2-arss.ediprintnotesfl       = pb-arsc.ediprintnotesfl
            pb2-arss.editermsfl            = pb-arsc.editermsfl
            pb2-arss.ediyouraddr           = pb-arsc.ediyouraddr
            /* pb2-arss.einvto               = pb-arsc.einvto      */
            pb2-arss.einvtype              = pb-arsc.einvtype
            pb2-arss.email                 = pb-arsc.email
            pb2-arss.enterdt               = today
            /* pb2-arss.epropto               = pb-arsc.epropto     */
            pb2-arss.eproptype             = pb-arsc.eproptype
            /* pb2-arss.estcompdt             = pb-arsc.estcompdt   */
            pb2-arss.faxphoneno            = pb-arsc.faxphoneno
            pb2-arss.fpcustno              = pb-arsc.fpcustno
            /*
            pb2-arss.genaddr[1]            = pb-arsc.genaddr[1]
            pb2-arss.genaddr[2]            = pb-arsc.genaddr[2]
            pb2-arss.genaddr3              = pb-arsc.genaddr3
            pb2-arss.gencity               = pb-arsc.gencity
            pb2-arss.gennm                 = pb-arsc.gennm
            pb2-arss.genphoneno            = pb-arsc.genphoneno
            pb2-arss.genst                 = pb-arsc.genst
            pb2-arss.genzip                = pb-arsc.genzip
            */
            pb2-arss.geocd                 = pb-arsc.geocd
            /* pb2-arss.holddays              = pb-arsc.holddays    */
            /* pb2-arss.holdfl                = pb-arsc.holdfl      */
            pb2-arss.holdpercd             = pb-arsc.holdpercd
            pb2-arss.inbndfrtfl            = pb-arsc.inbndfrtfl
            pb2-arss.invtofl               = false
            /* pb2-arss.invtotype             = pb-arsc.invtotype   */
            /* pb2-arss.jobclosedt            = pb-arsc.jobclosedt   */
            /* pb2-arss.jobcodbal             = pb-arsc.jobcodbal    */
            /* pb2-arss.jobdesc               = pb-arsc.jobdesc      */
            /* pb2-arss.jobfutinvbal          = pb-arsc.jobfutinvbal
            pb2-arss.jobmisccrbal          = 0
            pb2-arss.jobordbal             = 0
            pb2-arss.jobperiodbal[1]       = 0
            pb2-arss.jobperiodbal[2]       = 0
            pb2-arss.jobperiodbal[3]       = 0
            pb2-arss.jobperiodbal[4]       = 0
            pb2-arss.jobperiodbal[5]       = 0
            pb2-arss.jobservchgbal         = 0
            pb2-arss.jobuncashbal          = 0
            */
            pb2-arss.langcd                = pb-arsc.langcd
            pb2-arss.lastconsoldt          = ?
            pb2-arss.lastsaledt            = pb-arsc.lastsaledt
            /*
            pb2-arss.lenaddr[1]            = pb-arsc.lenaddr[1]
            pb2-arss.lenaddr[2]            = pb-arsc.lenaddr[2]
            pb2-arss.lenaddr3              = pb-arsc.lenaddr3
            pb2-arss.lencity               = pb-arsc.lencity
            pb2-arss.lennm                 = pb-arsc.lennm
            pb2-arss.lenst                 = pb-arsc.lenst
            pb2-arss.lenzip                = pb-arsc.lenzip
            pb2-arss.lienamt               = pb-arsc.lienamt
            pb2-arss.lienfiledt            = pb-arsc.lienfiledt
            pb2-arss.lienfileoper          = pb-arsc.lienfileoper
            pb2-arss.lienpreamt            = pb-arsc.lienpreamt
            pb2-arss.lienpredt             = pb-arsc.lienpredt
            pb2-arss.lienpreoper           = pb-arsc.lienpreoper
            pb2-arss.lienprewith           = pb-arsc.lienprewith
            pb2-arss.linetermsfl           = pb-arsc.linetermsfl
            */
            pb2-arss.mediacd               = pb-arsc.mediacd
            pb2-arss.name                  = pb-arsc.name
            pb2-arss.nextconsoldt          = pb-arsc.nextconsoldt
            pb2-arss.noinvcopy             = pb-arsc.noinvcopy
            pb2-arss.nontaxtype            = pb-arsc.nontaxtype
            pb2-arss.notesfl               = pb-arsc.notesfl
            /* pb2-arss.obsedi                = pb-arsc.obsedi  */
            pb2-arss.operinit              = pb-arsc.operinit
            pb2-arss.orderdisp             = pb-arsc.orderdisp
            pb2-arss.other1cd              = pb-arsc.other1cd
            pb2-arss.other2cd              = pb-arsc.other2cd
            pb2-arss.outbndfrtfl           = pb-arsc.outbndfrtfl
            /*
            pb2-arss.ownaddr[1]            = pb-arsc.ownaddr[1]
            pb2-arss.ownaddr[2]            = pb-arsc.ownaddr[2]
            pb2-arss.ownaddr3              = pb-arsc.ownaddr3
            pb2-arss.owncity               = pb-arsc.owncity
            pb2-arss.ownnm                 = pb-arsc.ownnm
            pb2-arss.ownst                 = pb-arsc.ownst
            pb2-arss.ownzip                = pb-arsc.ownzip
            */
            pb2-arss.pdcustno              = pb-arsc.pdcustno
            pb2-arss.phoneno               = pb-arsc.phoneno
            pb2-arss.pickprno              = pb-arsc.pickprno
            pb2-arss.pocontctnm            = pb-arsc.pocontctnm
            pb2-arss.pophoneno             = pb-arsc.pophoneno
            pb2-arss.poreqfl               = pb-arsc.poreqfl
            pb2-arss.pricecd               = pb-arsc.pricecd
            pb2-arss.pricetype             = pb-arsc.pricetype
            pb2-arss.ptxapfl               = pb-arsc.ptxapfl
            pb2-arss.ptxarfl               = pb-arsc.ptxarfl
            pb2-arss.ptxtransbillfl        = pb-arsc.ptxtransbillfl
            pb2-arss.ptxtype               = pb-arsc.ptxtype
            /* pb2-arss.restrictfl            = pb-arsc.restrictfl     */
            /* pb2-arss.revestdt              = pb-arsc.revestdt       */
            pb2-arss.route                 = pb-arsc.route
            /* pb2-arss.salesamt              = pb-arsc.salesamt       */
            pb2-arss.salesterr             = pb-arsc.salesterr
            pb2-arss.servchgfl             = pb-arsc.servchgfl
            pb2-arss.shipinstr             = pb-arsc.shipinstr
            pb2-arss.shiplbl               = pb-arsc.shiplbl
            pb2-arss.shipto                = "CaStep"  
            /* pb2-arss.shiptoeasncd          = pb-arsc.shiptoeasncd  */
            pb2-arss.shipviaty             = pb-arsc.shipviaty
            /* pb2-arss.slslimitamt           = pb-arsc.slslimitamt   */
            pb2-arss.slsrepin              = pb-arsc.slsrepin
            pb2-arss.slsrepout             = pb-arsc.slsrepout
            pb2-arss.spcdefaultty          = pb-arsc.spcdefaultty
            /* pb2-arss.startdt               = pb-arsc.startdt       */
            pb2-arss.state                 = pb-arsc.state
            pb2-arss.statecd               = pb-arsc.statecd
            pb2-arss.statustype            = pb-arsc.statustype
            pb2-arss.subfl                 = pb-arsc.subfl
            pb2-arss.synccrmfl             = pb-arsc.synccrmfl
            pb2-arss.syncmddfl             = pb-arsc.syncmddfl
            pb2-arss.taxablety             = pb-arsc.taxablety
            pb2-arss.taxauth               = pb-arsc.taxauth
            pb2-arss.taxcert               = pb-arsc.taxcert
            pb2-arss.taxdt                 = pb-arsc.taxdt
            pb2-arss.taxreg                = pb-arsc.taxreg
            pb2-arss.termstype             = pb-arsc.termstype
            pb2-arss.transdt               = pb-arsc.transdt
            pb2-arss.transproc             = pb-arsc.transproc
            pb2-arss.transtm               = pb-arsc.transtm
            pb2-arss.user1                 = pb-arsc.user1
            pb2-arss.user10                = "N"
            pb2-arss.user11                = pb-arsc.user11
            pb2-arss.user12                = pb-arsc.user12
            pb2-arss.user13                = pb-arsc.user13
            pb2-arss.user14                = pb-arsc.user14
            pb2-arss.user15                = pb-arsc.user15
            pb2-arss.user16                = pb-arsc.user16
            pb2-arss.user17                = pb-arsc.user17
            pb2-arss.user18                = pb-arsc.user18
            pb2-arss.user19                = pb-arsc.user19
            pb2-arss.user2                 = pb-arsc.user2
            pb2-arss.user20                = pb-arsc.user20
            pb2-arss.user21                = pb-arsc.user21
            pb2-arss.user22                = pb-arsc.user22
            pb2-arss.user23                = pb-arsc.user23
            pb2-arss.user24                = pb-arsc.user24
            pb2-arss.user3                 = pb-arsc.user3
            pb2-arss.user4                 = pb-arsc.user4
            pb2-arss.user5                 = pb-arsc.user5
            pb2-arss.user6                 = pb-arsc.user6
            pb2-arss.user7                 = pb-arsc.user7
            pb2-arss.user8                 = pb-arsc.user8
            pb2-arss.user9                 = pb-arsc.user9
            pb2-arss.webpage               = pb-arsc.webpage
            pb2-arss.whse                  = pb-arsc.whse
            pb2-arss.wodisccd              = pb-arsc.wodisccd
            pb2-arss.xxc1                  = pb-arsc.xxc1
            /* pb2-arss.xxc14                 = pb-arsc.xxc14    */
            pb2-arss.xxc15                 = pb-arsc.xxc15
            pb2-arss.xxc2                  = pb-arsc.xxc2
            pb2-arss.xxc3                  = pb-arsc.xxc3
            pb2-arss.xxda1                 = pb-arsc.xxda1
            pb2-arss.xxda14                = pb-arsc.xxda14
            pb2-arss.xxda15                = pb-arsc.xxda15
            pb2-arss.xxda2                 = pb-arsc.xxda2
            pb2-arss.xxda3                 = pb-arsc.xxda3
            /* pb2-arss.xxde1                 = pb-arsc.xxde1  */
            /* pb2-arss.xxde12                = pb-arsc.xxde12  */
            pb2-arss.xxde13                = pb-arsc.xxde13
            pb2-arss.xxde14                = pb-arsc.xxde14
            pb2-arss.xxde15                = pb-arsc.xxde15
            pb2-arss.xxi1                  = pb-arsc.xxi1
            /* pb2-arss.xxi12                 = pb-arsc.xxi12 */
            pb2-arss.xxi13                 = pb-arsc.xxi13
            pb2-arss.xxi14                 = pb-arsc.xxi14
            pb2-arss.xxi15                 = pb-arsc.xxi15
            /* pb2-arss.xxl11                 = pb-arsc.xxl11  */
            pb2-arss.xxl13                 = pb-arsc.xxl13
            pb2-arss.xxl14                 = pb-arsc.xxl14
            pb2-arss.xxl15                 = pb-arsc.xxl15
            pb2-arss.xxl2                  = pb-arsc.xxl2
            pb2-arss.zipcd                 = pb-arsc.zipcd.
                    
          run build_CTRLO_arss_CaStep (input pb2-arss.cono,
                                       input pb2-arss.custno,
                                       input pb2-arss.shipto).
        end.
      end.
    end.


    Procedure build_CTRLO_arss_CaStep:
      define input parameter ip-cono   like arsc.cono   no-undo.
      define input parameter ip-custno like arsc.custno no-undo.
      define input parameter ip-shipto like arss.shipto no-undo.
      
      define buffer slsmx  for notes.  
      define buffer x-slsmx  for notes.
      define buffer slslb  for notes.  
      
      
      
      
      
      
      find slslb  where slslb.cono = ip-cono       and 
                        slslb.notestype    = "zz"    and
                        slslb.primarykey   = "slsmx" and
                        slslb.secondarykey = "label"
                        no-lock no-error. 
      find slsmx  where slsmx.cono         = ip-cono and 
                        slsmx.notestype    = "zz"      and
                        slsmx.primarykey   = "slsmx"   and
                        slsmx.secondarykey =
                          string(ip-custno,"99999999999") + ip-shipto
                      no-lock no-error. 
    
      if avail slslb and not avail slsmx then do: 
        create slsmx. 
        assign slsmx.cono         = ip-cono  
               slsmx.printfl3     = no 
               slsmx.printfl4     = no 
               slsmx.notestype    = "zz" 
               slsmx.primarykey   = "slsmx" 
               slsmx.secondarykey = 
                       string(ip-custno,"99999999999") + ip-shipto.
        assign 
            slsmx.noteln[1]    = " " 
            slsmx.noteln[2]    = " " 
            slsmx.noteln[3]    = " " 
            slsmx.noteln[4]    = " " 
            slsmx.noteln[5]    = " " 
            slsmx.noteln[6]    = " " 
            slsmx.noteln[7]    = " " 
            slsmx.noteln[8]    = " " 
            slsmx.noteln[9]    = " ".
      end. 

    end.

/* California taxes */
      
   
