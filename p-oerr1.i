/* p-oerr1.i 1.1 01/03/98 */
/* p-oerr1.i 1.4 08/25/97 */
/*h*****************************************************************************
  INCLUDE      : p-oerr1.i
  DESCRIPTION  : Calculate Totals per order for Totals Page in OERR
  USED ONCE?   : no (p-oerr.i, oere.p)
  AUTHOR       : mwb
  DATE WRITTEN : 09/12/90
  CHANGES MADE :
    10/24/91 mwb; TB#  4343 Took off calc of -1 for CR type
    10/25/91 mwb; TB#  3305 Moved totord calc to oerrl line calc
    05/24/94 dww; TB# 15089 Total Order Value & Net Amount same. Cleaned up the
        assign statement.
    08/25/97 kjb; TB# 22385 OERR totals do not include rebates
*******************************************************************************/

/*TB# 4343, mwb 10/24/91 - took off calc of -1 for cr type moved totord value
    10/25/91 to lines **/

/*tb 15089 05/24/94 dww; Total Order Value & Net Amount same */
/*o Calculating totals for order type */
assign
    i = 0
    i = lookup(oeeh.transtype,"so,do,cs,rm,cr,bl,br,fo,qu,st").

if (i < 11) and (i <> 0) then

    /*tb 22385 08/25/97 kjb; Added the rebate totals to the end of the assign
        statement that can be commented out if the totaling has already been
        done. */
    assign 
        t-sales[i]   = t-sales[i]  + if i = 4 then
                           (v-totlineamt - oeeh.wodiscamt -
                            oeeh.specdiscamt) * -1
                       else (v-totlineamt - oeeh.wodiscamt -
                            oeeh.specdiscamt)
        t-gross[i]   = t-gross[i]  + (v-totlineamt - v-totcost -
                           oeeh.wodiscamt - oeeh.specdiscamt)
        t-charge[i]  = t-charge[i] + (if oeeh.codfl then oeeh.totinvamt
                                      else 0)
        t-down[i]    = t-down[i]   + (if oeeh.dwnpmttype then oeeh.dwnpmtamt
                       else ((oeeh.dwnpmtamt * oeeh.totinvamt) / 100))
        t-cstgds[i]  = t-cstgds[i] + if i = 4 then v-totcost * -1
                       else v-totcost
        t-count[i]   = t-count[i]  + 1
        t-lines[i]   = t-lines[i] + oeeh.nolineitem 
        {&com_asgn}
        t-custreb[i] = t-custreb[i] + v-totcustrebamt
        t-vendreb[i] = t-vendreb[i] + v-totvendrebamt
        t-rebfl[i]   = if v-totcustrebamt <> 0 or v-totvendrebamt <> 0
                           then "r" 
                       else t-rebfl[i]
        /{&com_asgn}* */.


