
for each matrix use-index k-matrix no-lock
   break by matrix.region
         by matrix.pprice-type
         by matrix.cprice-type
         by matrix.pline:
                               
   if matrix.sales-ytd-amt[1] < p-high or 
      matrix.sales-ytd-amt[1] > p-low then next.
                               
   find disc use-index k-disc where
        disc.region      = matrix.region and
        disc.pprice-type = matrix.pprice-type and
        disc.cprice-type = matrix.cprice-type and
        disc.pline       = matrix.pline
        no-lock
        no-error.
   
   find sasta where sasta.cono = g-cono and
                    sasta.codeiden = "k" and
                    sasta.codeval  = matrix.pprice-type
                    no-lock no-error.
   if avail sasta then
      assign s-ptype-desc = sasta.descrip.
   else
      assign s-ptype-desc = "** BLANK PRICE TYPE".

   find sasta where sasta.cono = g-cono and   
                    sasta.codeiden = "j" and  
                    sasta.codeval  = matrix.cprice-type
                    no-lock no-error.         
   if avail sasta then                        
      assign s-ctype-desc = sasta.descrip.    
   else                                       
      assign s-ctype-desc = "".
   
   assign s-region      = if matrix.region = "F" then "Filtration"
                          else
                          if matrix.region = "N" then "North"
                          else
                          if matrix.region = "S" then "South"
                          else
                          if matrix.region = "M" then "Mobil"
                          else
                          if matrix.region = "R" then "Service"
                          else
                          if matrix.region = "P" then "Pabco"
                          else
                          "".
   assign s-ctype       = matrix.cprice-type
          /* s-ctype-desc  = matrix.ctype-desc */
          s-ptype       = matrix.pprice-type
          /* s-ptype-desc  = matrix.ptype-desc  */
          s-pline       = matrix.pline
          s-pline-desc  = matrix.pline-desc.
          
          
/* SDI003 - 08222002 Begin */

   assign w-printcount = w-printcount + 1.

/* SDI003 - 08222002 End */

   display s-region
           s-ctype
           s-ctype-desc
           s-ptype
           s-ptype-desc
           s-pline
           s-pline-desc
         with frame f-subhdr.
   down with frame f-subhdr.

 
 do i = 1 to 2:
   if i = 1 then
      assign s-description = "PREVIOUS YEAR".
   else do:

/* SDI003 - 08222002 Begin 
   
      assign s-description = "".
      display s-description
              with frame f-detail.
      down with frame f-detail.

   SDI003 - 08222002 End */

      assign s-description = "CURRENT  YEAR".
   end.
   display s-description       
           with frame f-detail.
   down with frame f-detail.   
    
   assign s-description = " Total Sales:"
          s-line-amt    = matrix.sales-ytd-amt[i]
          s-0-5         = matrix.s0-5[i]
          s-6-10        = matrix.s6-10[i]
          s-11-15       = matrix.s11-15[i]
          s-16-20       = matrix.s16-20[i]
          s-21-25       = matrix.s21-25[i]
          s-26-30       = matrix.s26-30[i]
          s-31-35       = matrix.s31-35[i]
          s-36-40       = matrix.s36-40[i]
          s-41-45       = matrix.s41-45[i]
          s-46-50       = matrix.s46-50[i]
          s-51-55       = matrix.s51-55[i]
          s-56-60       = matrix.s56-60[i]
          s-61-65       = matrix.s61-65[i]
          s-66-70       = matrix.s66-70[i]
          s-71-75       = matrix.s71-75[i]
          s-76-80       = matrix.s76-80[i]
          s-81-85       = matrix.s81-85[i]
          s-86-90       = matrix.s86-90[i]
          s-91-95       = matrix.s91-95[i]
          s-96-100      = matrix.s96-100[i].
   run "display-line".
   
   assign s-description = " % of  Sales:"
          s-line-amt    = if matrix.sales-ytd-amt[i] <> 0 then
                     (matrix.sales-ytd-amt[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-0-5         = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s0-5[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-6-10        = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s6-10[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-11-15       = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s11-15[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-16-20       = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s16-20[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-21-25       = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s21-25[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-26-30       = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s26-30[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-31-35       = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s31-35[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-36-40       = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s36-40[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-41-45       = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s41-45[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-46-50       = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s46-50[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-51-55       = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s51-55[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-56-60       = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s56-60[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-61-65       = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s61-65[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-66-70       = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s66-70[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-71-75       = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s71-75[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-76-80       = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s76-80[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-81-85       = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s81-85[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-86-90       = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s86-90[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-91-95       = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s91-95[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0
          s-96-100      = if matrix.sales-ytd-amt[i] <> 0 then
                        (matrix.s96-100[i] / matrix.sales-ytd-amt[i]) * 100
                          else 0.
   run "display-line".
   
   assign s-description = " Nbr of Trans:"
          s-line-amt    = matrix.c0-5[i]   +  matrix.c6-10[i]  +                           matrix.c11-15[i] +  matrix.c16-20[i] + 
                          matrix.c21-25[i] +  matrix.c26-30[i] + 
                          matrix.c31-35[i] +  matrix.c36-40[i] + 
                          matrix.c41-45[i] +  matrix.c46-50[i] + 
                          matrix.c51-55[i] +  matrix.c56-60[i] +
                          matrix.c61-65[i] +  matrix.c66-70[i] +                           matrix.c71-75[i] +  matrix.c76-80[i] + 
                          matrix.c81-85[i] +  matrix.c86-90[i] +
                          matrix.c91-95[i] +  matrix.c96-100[i]   
          s-0-5         = matrix.c0-5[i]     
          s-6-10        = matrix.c6-10[i]    
          s-11-15       = matrix.c11-15[i]   
          s-16-20       = matrix.c16-20[i]   
          s-21-25       = matrix.c21-25[i]   
          s-26-30       = matrix.c26-30[i]   
          s-31-35       = matrix.c31-35[i]
          s-36-40       = matrix.c36-40[i]
          s-41-45       = matrix.c41-45[i]
          s-46-50       = matrix.c46-50[i]
          s-51-55       = matrix.c51-55[i]
          s-56-60       = matrix.c56-60[i]
          s-61-65       = matrix.c61-65[i]
          s-66-70       = matrix.c66-70[i]
          s-71-75       = matrix.c71-75[i]
          s-76-80       = matrix.c76-80[i]
          s-81-85       = matrix.c81-85[i]
          s-86-90       = matrix.c86-90[i]
          s-91-95       = matrix.c91-95[i]
          s-96-100      = matrix.c96-100[i].
   run "display-line".
   
   assign s-description = " % of Trans:"
          s-line-amt    = if matrix.trans-count[i] <> 0 then
                        (matrix.trans-count[i] / matrix.trans-count[i]) * 100
                          else 0
          s-0-5         = if matrix.trans-count[i] <> 0 then
                        (matrix.c0-5[i] / matrix.trans-count[i]) * 100
                          else 0
          s-6-10        = if matrix.trans-count[i] <> 0 then
                        (matrix.c6-10[i] / matrix.trans-count[i]) * 100
                          else 0
          s-11-15       = if matrix.trans-count[i] <> 0 then
                        (matrix.c11-15[i] / matrix.trans-count[i]) * 100
                          else 0
          s-16-20       = if matrix.trans-count[i] <> 0 then
                        (matrix.c16-20[i] / matrix.trans-count[i]) * 100
                          else 0
          s-21-25       = if matrix.trans-count[i] <> 0 then
                        (matrix.c21-25[i] / matrix.trans-count[i]) * 100
                          else 0
          s-26-30       = if matrix.trans-count[i] <> 0 then
                        (matrix.c26-30[i] / matrix.trans-count[i]) * 100
                          else 0
          s-31-35       = if matrix.trans-count[i] <> 0 then
                        (matrix.c31-35[i] / matrix.trans-count[i]) * 100
                          else 0
          s-36-40       = if matrix.trans-count[i] <> 0 then
                        (matrix.c36-40[i] / matrix.trans-count[i]) * 100
                          else 0
          s-41-45       = if matrix.trans-count[i] <> 0 then
                        (matrix.c41-45[i] / matrix.trans-count[i]) * 100
                          else 0
          s-46-50       = if matrix.trans-count[i] <> 0 then
                        (matrix.c46-50[i] / matrix.trans-count[i]) * 100
                          else 0
          s-51-55       = if matrix.trans-count[i] <> 0 then
                        (matrix.c51-55[i] / matrix.trans-count[i]) * 100
                          else 0
          s-56-60       = if matrix.trans-count[i] <> 0 then
                        (matrix.c56-60[i] / matrix.trans-count[i]) * 100
                          else 0
          s-61-65       = if matrix.trans-count[i] <> 0 then
                        (matrix.c61-65[i] / matrix.trans-count[i]) * 100
                          else 0
          s-66-70       = if matrix.trans-count[i] <> 0 then
                        (matrix.c66-70[i] / matrix.trans-count[i]) * 100
                          else 0
          s-71-75       = if matrix.trans-count[i] <> 0 then
                        (matrix.c71-75[i] / matrix.trans-count[i]) * 100
                          else 0
          s-76-80       = if matrix.trans-count[i] <> 0 then
                        (matrix.c76-80[i] / matrix.trans-count[i]) * 100
                          else 0
          s-81-85       = if matrix.trans-count[i] <> 0 then
                        (matrix.c81-85[i] / matrix.trans-count[i]) * 100
                          else 0
          s-86-90       = if matrix.trans-count[i] <> 0 then
                        (matrix.c86-90[i] / matrix.trans-count[i]) * 100
                          else 0
          s-91-95       = if matrix.trans-count[i] <> 0 then
                        (matrix.c91-95[i] / matrix.trans-count[i]) * 100
                          else 0
          s-96-100      = if matrix.trans-count[i] <> 0 then
                        (matrix.c96-100[i] / matrix.trans-count[i]) * 100
                          else 0.
   run "display-line".

   assign s-description = " Avg Invoice Mrg %:"
          s-line-amt    = if matrix.i-amount[i] <> 0 then
                           ((matrix.i-amount[i] - 
                            (matrix.i-cost[i] + matrix.i-discount[i])) /
                            (matrix.i-amount[i] - matrix.i-discount[i])) * 100
                          else 0
          s-0-5         = if matrix.s0-5[i] <> 0 then
                           ((matrix.s0-5[i] -
                             matrix.cst0-5[i]) / matrix.s0-5[i]) * 100
                          else 0
          s-6-10        = if matrix.s6-10[i] <> 0 then
                           ((matrix.s6-10[i] -
                             matrix.cst6-10[i]) / matrix.s6-10[i]) * 100
                          else 0
          s-11-15       = if matrix.s11-15[i] <> 0 then
                           ((matrix.s11-15[i] -
                             matrix.cst11-15[i]) / matrix.s11-15[i]) * 100
                          else 0
          s-16-20       = if matrix.s16-20[i] <> 0 then
                           ((matrix.s16-20[i] -
                             matrix.cst16-20[i]) / matrix.s16-20[i]) * 100
                          else 0
          s-21-25       = if matrix.s21-25[i] <> 0 then
                           ((matrix.s21-25[i] -
                             matrix.cst21-25[i]) / matrix.s21-25[i]) * 100
                          else 0
          s-26-30       = if matrix.s26-30[i] <> 0 then
                           ((matrix.s26-30[i] -
                             matrix.cst26-30[i]) / matrix.s26-30[i]) * 100
                          else 0
          s-31-35       = if matrix.s31-35[i] <> 0 then
                           ((matrix.s31-35[i] -
                             matrix.cst31-35[i]) / matrix.s31-35[i]) * 100
                          else 0
          s-36-40       = if matrix.s36-40[i] <> 0 then
                           ((matrix.s36-40[i] -
                             matrix.cst36-40[i]) / matrix.s36-40[i]) * 100
                          else 0
          s-41-45       = if matrix.s41-45[i] <> 0 then
                           ((matrix.s41-45[i] -
                             matrix.cst41-45[i]) / matrix.s41-45[i]) * 100
                          else 0
          s-46-50       = if matrix.s46-50[i] <> 0 then
                           ((matrix.s46-50[i] -
                             matrix.cst46-50[i]) / matrix.s46-50[i]) * 100
                          else 0
          s-51-55       = if matrix.s51-55[i] <> 0 then
                           ((matrix.s51-55[i] -
                             matrix.cst51-55[i]) / matrix.s51-55[i]) * 100
                          else 0
          s-56-60       = if matrix.s56-60[i] <> 0 then
                           ((matrix.s56-60[i] -
                             matrix.cst56-60[i]) / matrix.s56-60[i]) * 100
                          else 0
          s-61-65       = if matrix.s61-65[i] <> 0 then
                           ((matrix.s61-65[i] -
                             matrix.cst61-65[i]) / matrix.s61-65[i]) * 100
                          else 0
          s-66-70       = if matrix.s66-70[i] <> 0 then
                           ((matrix.s66-70[i] -
                             matrix.cst66-70[i]) / matrix.s66-70[i]) * 100
                          else 0
          s-71-75       = if matrix.s71-75[i] <> 0 then
                           ((matrix.s71-75[i] -
                             matrix.cst71-75[i]) / matrix.s71-75[i]) * 100
                          else 0
          s-76-80       = if matrix.s76-80[i] <> 0 then
                           ((matrix.s76-80[i] -
                             matrix.cst76-80[i]) / matrix.s76-80[i]) * 100
                          else 0
          s-81-85       = if matrix.s81-85[i] <> 0 then
                           ((matrix.s81-85[i] -
                             matrix.cst81-85[i]) / matrix.s81-85[i]) * 100
                          else 0
          s-86-90       = if matrix.s86-90[i] <> 0 then
                           ((matrix.s86-90[i] -
                             matrix.cst86-90[i]) / matrix.s86-90[i]) * 100
                          else 0
          s-91-95       = if matrix.s91-95[i] <> 0 then
                           ((matrix.s91-95[i] -
                             matrix.cst91-95[i]) / matrix.s91-95[i]) * 100
                          else 0
          s-96-100      = if matrix.s96-100[i] <> 0 then
                           ((matrix.s96-100[i] -
                           matrix.cst96-100[i]) / matrix.s96-100[i]) * 100
                          else 0.
   run "display-line".


   assign w-amount = w-amount + matrix.m-amount[i]
          w-cost   = w-cost + matrix.m-cost[i].
          
 end. /* do i = 1 to 2 */
 
 assign s-description = " Avg Matrix Mrg %:"
        s-line-amt    = if w-amount <> 0 then
                           ((w-amount - w-cost) / w-amount) * 100
                        else
                           0.
 assign xx-mar = dec(s-line-amt).
 
 display s-description
         s-line-amt
         with frame f-detail.
 down with frame f-detail.
 
 assign w-amount = 0
        w-cost   = 0.
 
 find disc use-index k-disc where
      disc.region      = matrix.region and
      disc.pprice-type = matrix.pprice-type and
      disc.cprice-type = matrix.cprice-type and
      disc.pline       = matrix.pline
      no-lock
      no-error.
 if avail disc then do:
    assign i = 2.
    assign s-description = " Disc/Mult Used:"
           s-line-amt    = disc.pct-mult
           xx-disc       = disc.pct-mult.

    display s-description       
            s-line-amt          
    with frame f-detail.
    down with frame f-detail.   
  
/* SDI002-08202002 Begin */
 
    assign s-description = "Desired Multiplier:"
           s-line-amt    = 0.

    assign xx-place = 0.

    do xx-inx = 3 to 98 by 5:        
      xx-place = xx-place + 1.       
      xx-margline[xx-place] = xx-inx.
    end.                             
        
    do xx-inx = 1 to 20:                       
      if xx-mar ge xx-margline[xx-inx] - 2 and 
         xx-mar le xx-margline[xx-inx] + 2 and 
         xx-inx <> 1 then do:
         xx-place = xx-inx.                     
         leave.                                 
      end.                                   
      if xx-mar ge xx-margline[xx-inx] - 3 and 
         xx-mar le xx-margline[xx-inx] + 2 and 
         xx-inx = 1 then do:
         xx-place = xx-inx.                     
         leave.                                 
      end.                                   
    end.                                       
                  
    run number-line      
        (input xx-mar,   
         input xx-disc,  
         input xx-place).
                  
    assign sd-0-5    = xx-discounts[1]
           sd-6-10   = xx-discounts[2]
           sd-11-15  = xx-discounts[3]
           sd-16-20  = xx-discounts[4]
           sd-21-25  = xx-discounts[5]
           sd-26-30  = xx-discounts[6]
           sd-31-35  = xx-discounts[7]
           sd-36-40  = xx-discounts[8]
           sd-41-45  = xx-discounts[9]
           sd-46-50  = xx-discounts[10]
           sd-51-55  = xx-discounts[11]
           sd-56-60  = xx-discounts[12]
           sd-61-65  = xx-discounts[13]
           sd-66-70  = xx-discounts[14]
           sd-71-75  = xx-discounts[15]
           sd-76-80  = xx-discounts[16]
           sd-81-85  = xx-discounts[17]
           sd-86-90  = xx-discounts[18]
           sd-91-95  = xx-discounts[19]
           sd-96-100 = xx-discounts[20].
                                  
       
/* SDI002-08202002 end */

    
    display s-description
            sd-0-5
            sd-6-10
            sd-11-15
            sd-16-20
            sd-21-25
            sd-26-30
            sd-31-35
            sd-36-40
            sd-41-45
            sd-46-50
            sd-51-55
            sd-56-60
            sd-61-65
            sd-66-70
            sd-71-75
            sd-76-80
            sd-81-85
            sd-86-90
            sd-91-95
            sd-96-100
    with frame f-detaildot.
/*
    down with frame f-detail.
*/                              
 end.   

/* SDI003 - 08222002 Begin */
/* This is here just to not print the blank line at the end of a page
   the page is so tight that if this line is printed it will run over */

if w-printcount ge 2 and i = 2 then
 assign s-description = "".
else
 do:
 assign s-description = "".

 display s-description
         with frame f-detail.
 down with frame f-detail.
 end.

 if w-printcount ge 2 and i = 2 then
   do:
   assign w-printcount = 0.
   page.
   end.
 else
/* SDI003 - 08222002 End */
 if (last-of(matrix.region)) then 
   page.

end. /* for each matrix */

/*   
procedure display-line:
   display s-description
           s-line-amt   
           s-0-5        
           s-6-10       
           s-11-15      
           s-16-20      
           s-21-25      
           s-26-30      
           s-31-35      
           s-36-40      
           s-41-45      
           s-46-50      
           s-51-55      
           s-56-60      
           s-61-65      
           s-66-70      
           s-71-75      
           s-76-80      
           s-81-85  
           s-86-90  
           s-91-95  
           s-96-100 
           with frame f-detail.
   down with frame f-detail.
end. /* procedure */

procedure number-line:                                
   define input parameter zx-margin   as decimal no-undo.
   define input parameter zx-discount as decimal no-undo.
   define input parameter zx-place    as decimal no-undo.
                            
   if zx-discount < 100 then                             
      zx-discount = 1 - (zx-discount / 100).             
   else                                                  
      zx-discount = zx-discount / 100.                   
                                                              
   if zx-margin < 100 then                               
      zx-margin = 1 - (zx-margin / 100).                 
   else                                                  
      zx-margin = zx-margin / 100.                       
                                                            
   xx-sell = 100 * zx-discount.                          
   xx-cost = xx-sell * zx-margin.                        

   if zx-place > 0 then 
      do xx-inx = zx-place to 1 by -1:                          
         assign xx-sellcal =  xx-cost / ( if xx-margline[xx-inx] > 100 then   
                                             xx-margline[xx-inx] / 100        
                                          else
                                             1 - (xx-margline[xx-inx] / 100)).
         assign xx-sell = xx-sellcal / 100.                     
                xx-discounts[xx-inx] = xx-sell.                        
   end.                                                      
   if zx-place < 20 then
      do xx-inx = (zx-place + 1) to 20 by 1:                    
         assign xx-sellcal =                                    
                 xx-cost / ( if xx-margline[xx-inx] > 100 then   
                                xx-margline[xx-inx] / 100        
                             else                                
                                1 - (xx-margline[xx-inx] / 100)).
         assign xx-sell = xx-sellcal / 100.
     end.
end. /* procedure */
*/
