/***************************************************************************
 PROGRAM NAME: n-zicsvlu.i
  PROJECT NO.: 00-44
  DESCRIPTION: This program is the include ofr zicspvlu.p 
 DATE WRITTEN: 06/13/00
       AUTHOR: Sir Carl E. Graul Esq.
     MODIFIED: 

***************************************************************************/

/* ########################## TREND INCLUDES ######################### */


/* ########################### DEFINITIONS ########################### */


/* ############################## LOGIC ############################## */

find {1} icsw use-index k-vendor where
        icsw.cono       = g-cono
    and icsw.whse       = svv-whse
    and icsw.arpvendno  = s-vendno
    {2}
    and icsw.prod >= v-start
        /{2}* */
    no-lock no-error.
    

SkipInactive:

do while true :

  if available icsw then
    do:
    {w-zicsp.i icsw.prod no-lock}
    if not avail icsp then
      do:
      if "{1}" = "first" or 
         "{1}" = "next" then
        do:

        find next icsw use-index k-vendor where
            icsw.cono       = g-cono
        and icsw.whse       = svv-whse
        and icsw.arpvendno  = s-vendno
           {2}
        and icsw.prod >= v-start
          /{2}* */
        no-lock no-error.
        end.    
      else
      if "{1}" = "last" or 
         "{1}" = "prev" then
        do:
        
        find prev icsw use-index k-vendor where
                 icsw.cono       = g-cono
             and icsw.whse       = svv-whse
             and icsw.arpvendno  = s-vendno
                {2}
             and icsw.prod >= v-start
                /{2}* */ 
             no-lock no-error.
         
        end.
      next SkipInactive.
      end.
    leave SkipInactive.
    end.
  else 
    leave skipInactive.
  end.

    
    
    
if available(icsp) then
    assign cg-statustype = icsp.statustype.
else
    assign cg-statustype = "".

if available icsw and cg-statustype <> "i" then
  do:
    {p-netavl.i "/*"}      
    {w-zicsp.i icsw.prod no-lock}
    assign
        s-descrip = icsp.descrip[1] + icsp.descrip[2]
        s-qty = if icsp.statustype  = "l" then "  Labor"  
            else if icsp.kittype    = "b" then "    Kit"  
            else if not available icsw then    "    N/A"  
            else if s-netavail > 9999999 then  "+++++++"  
            else if s-netavail < 0       then  "      0"  
            else string(truncate(s-netavail,0),"zzzzzz9").
  end. /* if available icsw and cg-statustype <> "i" */
else
    assign s-qty = ""
           s-descrip = v-invalid.
 

/* ############################ EOJ STUFF ############################ */


/* ######################### END OF PROGRAM ########################## */ 
/* ####################### INTERNAL PROCEDURES ####################### */


