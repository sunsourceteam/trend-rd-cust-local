
def buffer zx-wteh for wteh.
def buffer zx-wtel for wtel.
def buffer zx-poeh for poeh.
def buffer zx-poel for poel.
def buffer zx-vaeh for vaeh.
 
 
if v-delfl = yes then
  do:
 
  for each oeel use-index k-oeel where
         oeel.cono = g-cono and
         oeel.orderno = oeeh.orderno and
         oeel.ordersuf = oeeh.ordersuf and
         (oeel.orderaltno <> 0 or
          oeel.kitfl = true)    and
         oeel.statustype <> "c" and
         oeel.specnstype <> "l"
                                      no-lock:
  
  if oeel.kitfl <> true then
    do:  
    
    if oeel.ordertype = "t" then 
      do:
      find last zx-wteh use-index k-wteh where
                zx-wteh.wtno = oeel.orderaltno
                no-lock no-error.
 
       find last zx-wtel use-index k-wtel
          where  zx-wtel.wtno = oeel.orderaltno and
                 zx-wtel.lineno = oeel.linealtno no-lock no-error.
               
       if avail zx-wtel and zx-wtel.wtsuf <> zx-wteh.wtsuf then
          find zx-wteh use-index k-wteh 
           where zx-wteh.wtno = zx-wtel.wtno and
               zx-wteh.wtsuf = zx-wtel.wtsuf no-lock no-error.
      
       if not avail zx-wteh or
          not avail zx-wtel then
          v-delfl = v-delfl.
     
       if avail zx-wteh then
        do:
        if zx-wteh.stagecd > 1 then
          do:
          v-delfl = no.
          run err.p (1902).
          next updt.
          end.
      
        end.
     end.
   else if oeel.ordertype = "p" then 
     do:
     find last zx-poeh use-index k-poeh where
               zx-poeh.cono = g-cono and
               zx-poeh.pono = oeel.orderaltno
               no-lock no-error.

     find last zx-poel use-index k-poel
        where  
        zx-poel.cono = g-cono and
        zx-poel.pono = oeel.orderaltno and
        zx-poel.lineno = oeel.linealtno no-lock no-error.
               
     if avail zx-poel and zx-poel.posuf <> zx-poeh.posuf then
        find zx-poeh use-index k-poeh 
         where
          zx-poeh.cono = g-cono and
          zx-poeh.pono = zx-poel.pono and
          zx-poeh.posuf = zx-poel.posuf no-lock no-error.
      
     if not avail zx-poeh or
        not avail zx-poel then
        v-delfl = v-delfl.
     if avail zx-poeh then
       do:
       if zx-poeh.stagecd > 1 then
         do:
         v-delfl = no.
         run err.p (1902).
         next updt.
         end.
       end.
     end.
   else if oeel.ordertype = "f" then 
     do:
     find last zx-vaeh use-index k-vaeh where
               zx-vaeh.cono = g-cono and
               zx-vaeh.vano = oeel.orderaltno
               no-lock no-error.

     if avail zx-vaeh then
       do:
       if zx-vaeh.stagecd > 1  then
         do:
         v-delfl = no.
         run err.p (1902).
         next updt.
         end.
 
       end.    
     end.
   end.
  
 else if oeel.kitfl = true then
     
   for each oeelk use-index k-oeelk where
            oeelk.cono = g-cono and
            oeelk.ordertype = "o" and
            oeelk.orderno = oeeh.orderno and
            oeelk.ordersuf = oeeh.ordersuf and
            oeelk.lineno = oeel.lineno no-lock:
   
   if oeelk.ordertype = "t" then 
     do:
      find last zx-wteh use-index k-wteh where
                zx-wteh.wtno = oeelk.orderaltno
                no-lock no-error.
 
       find last zx-wtel use-index k-wtel
          where  zx-wtel.wtno = oeelk.orderaltno and
                 zx-wtel.lineno = oeelk.linealtno no-lock no-error.
               
       if avail zx-wtel and zx-wtel.wtsuf <> zx-wteh.wtsuf then
          find zx-wteh use-index k-wteh 
           where zx-wteh.wtno = zx-wtel.wtno and
               zx-wteh.wtsuf = zx-wtel.wtsuf no-lock no-error.
      
       if not avail zx-wteh or
          not avail zx-wtel then
          v-delfl = v-delfl.
     
       if avail zx-wteh then
        do:
        if zx-wteh.stagecd > 1 then
          do:
          v-delfl = no.
          run err.p (1902).
          next updt.
          end.
      
        end.
     end.
   else if oeelk.ordertype = "p" then 
     do:
     find last zx-poeh use-index k-poeh where
               zx-poeh.cono = g-cono and
               zx-poeh.pono = oeelk.orderaltno
               no-lock no-error.

     find last zx-poel use-index k-poel
        where  
              zx-poel.cono = g-cono and
              zx-poel.pono = oeelk.orderaltno and
              zx-poel.lineno = oeelk.linealtno no-lock no-error.
               
     if avail zx-poel and zx-poel.posuf <> zx-poeh.posuf then
        find zx-poeh use-index k-poeh 
         where 
             zx-poeh.cono = g-cono and
             zx-poeh.pono = zx-poel.pono and
             zx-poeh.posuf = zx-poel.posuf no-lock no-error.
      
     if not avail zx-poeh or
        not avail zx-poel then
        v-delfl = v-delfl.
     if avail zx-poeh then
       do:
       if zx-poeh.stagecd > 1 then
         do:
         v-delfl = no.
         run err.p (1902).
         next updt.
         end.
       end.
     end.
   else if oeel.ordertype = "f" then 
     do:
     find last zx-vaeh use-index k-vaeh where
               zx-vaeh.cono = g-cono and
               zx-vaeh.vano = oeelk.orderaltno
               no-lock no-error.

     if avail zx-vaeh then
       do:
       if zx-vaeh.stagecd > 1  then
         do:
         v-delfl = no.
         run err.p (1902).
         next updt.
         end.
 
       end.    
    end.
  end.
 end.
end.

 

        
      
           
