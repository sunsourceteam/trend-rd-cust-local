define stream sdebug.


define buffer cv-oeehb for oeehb.
define var v-conversion as logical no-undo.
define var v-question   as logical no-undo.


define var x-error      as logical no-undo.
define var x-backed-quote-up as logical no-undo.
define var x-returncode      as character no-undo.
define new shared var s-canceled as logical no-undo.

form
oeehb.batchnm    colon 10 label "Quote" space(0) "q"
oeehb.custno     colon 10 label "CustNo"
oeehb.shipto     colon 10 label "Shipto"
oeehb.shiptonm   colon 34 label "Ship To"
oeehb.transtype  colon 10 label "Type"
 help "SO, DO, FO, or BL "
 validate (can-do ("SO,DO,FO,BL",input oeehb.transtype),
           "Must be SO, DO, FO, or BL (3656)")
oeehb.shiptoaddr[1]    at 36 no-label
oeehb.whse       colon 10 label "Whse"
 validate (can-find (first icsd where icsd.cono = g-cono and
                                      icsd.whse = input oeehb.whse no-lock),
           "Warehouse Not Setup in Warehouse Setup - ICSD (4601)")                           
oeehb.shiptoaddr[2]    at 36 no-label
oeehb.custpo     colon 10 label "CustPO"
oeehb.shiptocity       at 36 no-label
oeehb.shiptost         at 58 no-label
oeehb.shiptozip        at 61 no-label
v-contact           colon 10 label "Contact"
oeehb.geocd      colon 45 label "GeoCode"
oeehb.shipviaty    colon 10 label "ShipVia"
/*
 validate (can-find (first sasta where sasta.cono = g-cono and 
                                       sasta.codeiden = "s" and
                                       sasta.codeval = oeehb.shipViaty
                                       no-lock),
           "Ship Via Not Setup in System Table - SASTT (4030)")                */                        
 
oeehb.inBndFrtFl colon 29 label "Freight"
oeehb.outBndFrtFl      at 35 no-label
oeehb.orderdisp  colon 45 label "Disp"
oeehb.slsrepout  colon 62 label "Salesman"
/*
 validate (can-find (first smsn where smsn.cono = g-cono and 
                                      smsn.slsrep = oeehb.slsrepout
                                       no-lock),
           "Salesrep Not Setup in Salesrep Setup - SMSN (4604)")               */                          
 
oeehb.slsrepin      at 69 no-label
/*
 validate (can-find (first smsn where smsn.cono = g-cono and 
                                      smsn.slsrep = oeehb.slsrepin
                                       no-lock),
           "Salesrep Not Setup in Salesrep Setup - SMSN (4604)")                
*/
oeehb.reqshipdt  colon 10 label "Req Ship"
oeehb.refer      colon 10 label "Refer"
oeehb.shipinstr  colon 10 label "ShipInstr"



with frame f-oeexqcnt row 1 side-labels overlay
  title "Quote Conversion Options" width 80.
form
oeehb.batchnm    colon 10 label "Quote" space(0) "q"
oeehb.custno     colon 10 label "CustNo"
oeehb.transtype  colon 60 label "Type"
oeehb.shipto     colon 10 label "Shipto"
oeehb.whse       colon 60 label "Whse"



with frame f-lineconvert row 1 side-labels overlay
  title "Quote Line Conversion Maintenance " width 80.

form
"                           F8-Extend    F9-BackToHeader                        "
  at 1 dcolor 2
with frame f-statuslinex no-box no-labels overlay row 21.  


def var v-converting as logical no-undo.


def var v-powtintfllocal  as logical                     no-undo.
def var o-inBndFrtFl      like oeeh.inbndfrtfl           no-undo.      
def var o-outBndFrtFl     like oeeh.outbndfrtfl          no-undo.       
def var o-transtype       as character                   no-undo.
def var hx-batchnm        like oeehb.batchnm             no-undo.
def var hx-custno         like oeehb.custno              no-undo.
def var hx-transtype      like oeehb.transtype           no-undo.
def var hx-shiptonm       like oeehb.shiptonm            no-undo.
def var hx-shipto         like oeehb.shipto              no-undo.
def var hx-shiptoaddr1    as   char format "x(30)"       no-undo.
def var hx-whse           like oeehb.whse                no-undo.
def var hx-shiptoaddr2    as   char format "x(30)"       no-undo.
def var hx-custpo         like oeehb.custpo              no-undo.
def var hx-shiptocity     like oeehb.shiptocity          no-undo.
def var hx-shiptost       like oeehb.shiptost            no-undo.
def var hx-shiptozip      like oeehb.shiptozip           no-undo.
def new shared var hx-contact        as   character format "x(60)"  no-undo.
def var hx-shipvia        like oeehb.shipvia             no-undo.
def var hx-inBndFrtFl     like oeehb.inbndfrtfl          no-undo.
def var hx-outBndFrtFl    like oeehb.outbndfrtfl         no-undo.
def var hx-orderdisp      like oeehb.orderdisp           no-undo.
def var hx-slsrepout      like oeehb.slsrepout           no-undo.
def var hx-slsrepin       like oeehb.slsrepin            no-undo.
def var hx-reqshipdt      like oeehb.reqshipdt           no-undo.
def var hx-refer          like oeehb.refer               no-undo.
def var hx-shipinstr      like oeehb.shipinstr           no-undo.
 

















def var zx-errorty        as integer                     no-undo.              def var v-shiptofl        as logical                     no-undo.
def var v-shiptogeo       like oeeh.geocd                no-undo.
def var garbageout        as decimal extent 10           no-undo. 
def var v-tax010errorno   as decimal init 0              no-undo.
def var v-tax010errorfl   as logical                     no-undo.

{g-vertar.i &new = "new"}
{rdtax010.sva "new"}


&global-define NORMAL 1
&global-define EXEMPT 2

&global-define FEDERAL   1
&global-define STATE     2
&global-define COUNTY    3
&global-define CITY      4
&global-define SECCOUNTY 5
&global-define SECCITY   6
{tiadch.lgr
    &city  = "oeehb.shiptocity"
    &state = "oeehb.shiptost"
    &zipcd = "oeehb.shiptozip"
    &geocd = "oeehb.geocd"
    &frame = "f-oeexqcnt"}
                    
{idtaxirn.lva}
                    
 /*d Third party tax interface identification function */
 run idtaxi.p(input  g-cono,
              output v-modvtfl,
              output v-geocdfl,
              output v-geotxrgfl,
              output v-geoindxty).



on f12 of v-contact in frame f-oeexqcnt
  do:
  run zsdicontact.p (input "uu",
                     input g-cono,
                     input v-custno,
                     input v-Shipto,
                     input-output v-contact,
                     input-output v-cphone,
                     input-output v-phone,
                     input-output v-email).

  on cursor-up cursor-up.
  on cursor-down cursor-down.
  /* The above program by default sets keys back to SXE keys so here it's
     being set back
  */   

  display 
    v-contact
    
  with frame f-oeexqcnt.
end.                    
 
