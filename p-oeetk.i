/* p-oeetk.i 1.1 01/03/98 */
/* p-oeetk.i 1.12 03/16/94 */
/*h****************************************************************************
  INCLUDE      : p-oeetk.i
  DESCRIPTION  : Processing include for component screen (used by xxix)
  USED ONCE?   : yes
  AUTHOR       : dao
  DATE WRITTEN :
  CHANGES MADE :
    09/24/92 dkk; TB#  7993/7994 Allow update of the refer, printfl,pricefl
    01/30/93 dby;           Add access to oeelk created in Bid Prep ordertype =
        "b & o"
    03/05/93 kmw; TB#  9090 BOD kit screen
    03/05/93 kmw; TB# 10289 Don't repaint screen... and oversize
    05/11/93 kmw; TB# 11286 BOD kit keyword not displayed
    07/06/93 kmw; TB# 11684 Capitalize Soft Function Key Labels - added all CAP
        put screen commands and lowercase reset. Did NOT comment each one.
        F6 Detail split due to oversize
    10/18/93 kmw; TB# 13011 Availability checked after product
    10/29/93 kmw; TB# 11758 Set unit conv to 10 dec
    03/14/94 dww; TB# 15141 Allow decimals in Conversion factor - oversize due
        to unitconv.gas. Introduced a variable v-kitconv to consolidate several
        of the unitconv.gas includes.
    06/09/95 ajw; TB# 18575 Nonstock/Special Kit Component Project (A1)
    07/18/95 ajw; TB# 18575 Nonstock/Special Kit Component Project (A17)
    03/20/96 tdd; TB# 19564 NS kit component filled erroneously
    03/30/97 mms; TB#  7241 (5.1) Spec8.0 Special Price Costing; added icss
        and speccost logic.
    04/08/97 jl;  TB# 22938 Cannot scroll option screen
    04/11/97 jl;  TB# 22958 Screen does not redisplay after adding options.
    08/11/97 jl;  TB# 23568 Kit component screen F7-Add/Delete
    09/14/00 mms; TB# e6508 WT BOD Project - Added logic to block serial or
        lot entry on components when working in OE with a fabricated BOD.
    01/08/01 lbr; TB# e5104 Added var passed to icets.p.
    04/06/01 mms; TB# e8830 Block reload of s-netavail on prebuilt kit.
    08/19/03 blr; tb# T11498 Cross Reference Functionality for Kit Components
        Project.  Allow Supersedes & Substitutes.
*******************************************************************************/

/*tb 22958 04/11/97 jl;  Screen does not redisplay after adding options */
/*d Set redisplay flag to no.  When this flag is set to yes after certain
    Function key logic will cause the program to redisplay all the records in
    the scrolling screen. */
assign v-redispfl = no.

/*o Accept product/qty/unit input if return pressed */
if {k-return.i} then do:
    run oeetkr.p(input v-recid[frame-line(f-oeetk)],
                 input v-length,
                 output v-outerrfl,
                 output v-kpid).

    /*d Redisplay function key strip */
    {d-oeetkf.i}

    /*tb 10289 03/05/93 kmw; Don't repaint screen */
    if {k-cancel.i} or {k-func13.i} then next dsply.
    find oeelk where recid(oeelk) = v-kpid no-lock no-error.
    if avail oeelk then do:
        {d-oeetk.i}
    end.

    if frame-line(f-oeetk) ne v-length then down 1 with frame f-oeetk.
    if {k-jump.i} then next main.
    /*tb 10289 03/05/93 kmw; Don't repaint screen */
    if g-ourproc = "kpet" then next dsply.
    next.
end.

/*o Display the detail screen if an F6 is pressed */
else if {k-func6.i} then do for b-oeelk:

    find b-oeelk where recid(b-oeelk) = v-recid[frame-line(f-oeetk)]
        exclusive-lock no-error.

    if avail b-oeelk then do:

        run oeetkd.p.

        if b-oeelk.comptype = "r" then
            display b-oeelk.instructions @ s-descrip with frame f-oeetk.

        find oeelk where recid(oeelk) = recid(b-oeelk) no-lock no-error.

        if avail oeelk then do:

            assign
                v-prod                       = oeelk.shipprod
                v-recid[frame-line(f-oeetk)] = recid(oeelk).

            {d-oeetk.i}

            next dsply.
        end. /*if avail oeelk*/
    end. /*if avail b-oeelk then do*/

    else run err.p(1106).
    if {k-jump.i} then next main.
    /*tb 10289 03/05/93 kmw; Don't repaint screen */
    next dsply.
end.    /* [F6] Detail */

/*o Allow Add/Delete of components if F7 pressed */
else if {k-func7.i} then do:
    put screen row 21 col 21 color messages "ADD/DELETE".
    run oeetkm.p (input integer(frame-value)).
    /*tb 9090 03/05/93 kmw; Changed from "return" to "next main" to be
         consistent with other function keys */

    if {k-jump.i} then next main.
    /*tb 19564 03/20/96 tdd; NS kit component filled erroneously */
    /*next l-disp.*/

    /*tb 23568 08/11/97 jl; Screen does not redisplay after adding options */
    assign v-redispfl = yes.

end.  /* [F7] Add/Delete */

/*o Allow entry of the options if F8 pressed */
else if {k-func8.i} and k-ordtype = "w" then do:
    if k-optfl = false then do:
        bell.
        next.
    end.

    put screen row 21 col 38 color messages "OPTIONS".
    run oeetko.p.
    if {k-jump.i} then next main.

    /*tb 10289 03/05/93 kmw; Don't repaint screen */
    /*tb 11286 05/11/93 kmw; BOD kit keyword not displayed as entered */
    /*tb 19564 03/20/96 tdd; NS kit component filled erroneously */
    /*next l-disp.*/

    /*tb 22958 04/11/97 jl;  Screen does not redisplay after adding options */
    assign v-redispfl = yes.

end.  /* [F8] Options */

/*o Allow interface to Warehouse Manager if F8 pressed */
else if {k-func8.i} and k-ordtype <> "w" and v-modwmfl and
        ( k-ordtype <> "o":u or v-bodtransferty <> "t":u ) and
        not can-do("st,fo,bl,qu,do,cr,ra",g-oetype) then do for b-oeelk:
    find b-oeelk where recid(b-oeelk) = v-recid[frame-line(f-oeetk)]
        no-lock no-error.
    if avail b-oeelk then {w-icsw.i b-oeelk.shipprod v-whse no-lock}
    /*tb 18575 06/09/95 ajw; Nonstock/Special Kit Component.  Added code to
        block F8 for nonstock components*/
    if not avail b-oeelk or not avail icsw or b-oeelk.comptype = "r" or
       b-oeelk.specnstype = "n" then do:
        run err.p(1106).
        next dsply.
    end.

    else if icsw.wmfl = yes then do:
        put screen row 21 col 38 color messages "WHSE MGR".
        v-oeid = recid(b-oeelk).
        run oeetkwm.p(v-oeid).
        put screen row 21 col 38 color messages "Whse Mgr".
    end.

    next dsply.
end.  /* [F8] Whse Mgr */

/*o Allow entry of Keywords if F9 is pressed */
else if {k-func9.i} and k-ordtype = "w" then do:
    if k-keyfl = false then do:
        bell.
        next.
    end.

    put screen row 21 col 53 color messages "KEYWORDS".
    run oeetkk.p.
    if {k-jump.i} then next main.
    /*tb 10289 03/05/93 kmw; Don't repaint screen */
    /*tb 11286 05/11/93 kmw; BOD kit keyword not displayed as entered */
    /*tb 19564 03/20/96 tdd; NS kit component filled erroneously */
    /*next l-disp.*/
end.   /* [F9] Keywords */

/*o Allow for Serial/Lot # interface if F9 pressed */
else if {k-func9.i} and k-ordtype <> "w" and
    ( k-ordtype <> "o":u or v-bodtransferty <> "t":u ) and
        not can-do("st,fo,bl,qu,do,cr,ra",g-oetype) then do for b-oeelk:
    find b-oeelk where recid(b-oeelk) = v-recid[frame-line(f-oeetk)]
        no-lock no-error.
    if avail b-oeelk then {w-icsw.i b-oeelk.shipprod g-whse no-lock}

    /*d Find related special price costing information. */
    if avail b-oeelk then
        {icss.gfi
            &prod        = b-oeelk.shipprod
            &icspecrecno = b-oeelk.icspecrecno
            &lock        = "no"}

    /*tb 18575 06/09/95 ajw; Nonstock/Special Kit Component.  Added code to
        block F8 for nonstock components*/
    if not avail b-oeelk or not avail icsw or b-oeelk.comptype = "r" or
        b-oeelk.specnstype = "n" or not avail icss then do:
        run err.p(1106).
        /*tb 10289 03/05/93 kmw; Don't repaint screen */
        next dsply.
    end.

    put screen row 21 col 53 color messages "SERIAL/LOT".

    /*tb 15141 03/14/94 dww; Allow decimals in Conversion factor - consolidate
        the next several uses of the unitconv.gas include into one variable */
    /*d Load special price costing variables for lot and serial record
        create. */
    assign
        {speccost.gas &com = "/*"}
        v-kitconv = {unitconv.gas &decconv = b-oeelk.conv}.

    if icsw.serlottype = "s" then do:
        {p-nosn.i &seqno     = b-oeelk.seqno
                  &ordertype = b-oeelk.ordertype
                  &orderno   = b-oeelk.orderno
                  &ordersuf  = b-oeelk.ordersuf}
        v-nosnlots = ((if b-oeelk.qtyneeded >= 0 then b-oeelk.qtyneeded
                       else b-oeelk.qtyneeded * -1)
                       * v-kitconv * v-stkqtyship) - v-noassn.
    end.

    else if icsw.serlottype = "l" then do:
        {p-nolots.i &seqno      = b-oeelk.seqno
                    &ordertype  = b-oeelk.ordertype
                    &orderno    = b-oeelk.orderno
                    &ordersuf   = b-oeelk.ordersuf}
        v-nosnlots = ((if b-oeelk.qtyneeded >= 0 then b-oeelk.qtyneeded
                       else b-oeelk.qtyneeded * -1)
                       * v-kitconv * v-stkqtyship) - v-noassn.
    end.

    assign
        v-errfl     = "n"
        v-qtyneeded = if b-oeelk.qtyneeded >= 0 then b-oeelk.qtyneeded
                      else b-oeelk.qtyneeded * -1
        g-prod      = b-oeelk.shipprod
        s-retseqno  = if b-oeelk.retseqno = 0 then b-oeelk.seqno
                      else b-oeelk.retseqno.

    if icsw.serlottype = "s" then
        /*tb e5104 Added "" param before output param */
        run icets.p(v-whse,
                    b-oeelk.shipprod,
                    substring(g-ourproc,1,2),
                    k-orderno,
                    k-ordersuf,
                    k-lineno,
                    b-oeelk.seqno,
                    if b-oeelk.qtyneeded < 0 then yes else s-returnfl,
                    v-qtyneeded * v-kitconv * v-stkqtyship,
                    v-nosnlots,
                    v-qtyneeded * v-kitconv * v-stkqtyord,
                    0,
                    g-oetype,
                    "",
                    output v-nosnlots,
                    output v-errfl,
                    input-output v-qtyunavail).

    else if icsw.serlottype = "l" then
        run icetl.p(v-whse,
                    b-oeelk.shipprod,
                    substring(g-ourproc,1,2),
                    g-orderno,
                    g-ordersuf,
                    s-lineno,
                    b-oeelk.seqno,
                    if b-oeelk.qtyneeded < 0 then yes else s-returnfl,
                    v-qtyneeded * v-kitconv * v-stkqtyship,
                    v-nosnlots,
                    v-qtyneeded * v-kitconv * v-stkqtyord,
                    0,
                    g-oetype,
                    output v-nosnlots,
                    output v-errfl,
                    input-output v-qtyunavail).

    put screen row 21 col 53 color messages "Serial/Lot".
    next dsply.
end.   /* [F9] Ser/Lot */

/*o Allow for Options/Keywords if F10 pressed */
else if {k-func10.i} and k-ordtype <> "w" and (k-optfl or k-keyfl) then do:
    status default "Select Function or Press PF4 to End".
    bell.
    if k-optfl and k-keyfl then
    put screen row 21 col 4 color messages
" F6-Options   F7-Keywords                                                ".
    f10key:
    do while true on endkey undo f10key, leave f10key:
        readkey.
        /*tb 10289 03/05/93 kmw; Don't repaint screen */
        if {k-func.i} and not ({k-func6.i} or {k-func7.i}) then next dsply.

        /*o Process Options from an F10/F6 */
        if {k-func6.i} or (k-optfl and not k-keyfl) then do:
            if k-optfl = false then do:
                bell.
                /*tb 10289 03/05/93 kmw; Don't repaint screen */
                next dsply.
            end.

            put screen row 21 col 8 color messages "OPTIONS".
            run oeetko.p.
            if {k-jump.i} then next main.
            /*tb 19564 03/20/96 tdd; NS kit component filled erroneously */
            /*next l-disp.*/

            /*tb 22938 04/08/97 jl;  Added redisplay of keyfunctions */
            {d-oeetkf.i}

        end. /* [F6] Options */

        /*o Process Keywords from an F10/F6 */
        if {k-func7.i} or (k-keyfl and not k-optfl) then do:
            if k-keyfl = false then do:
                bell.
                /*tb 10289 03/05/93 kmw; Don't repaint screen */
                next dsply.
            end.

            put screen row 21 col 21 color messages "KEYWORDS".
            run oeetkk.p.
            if {k-jump.i} then next main.
            /*tb 11286 05/11/93 kmw; BOD kit keyword not displayed as entered */
            /*tb 19564 03/20/96 tdd; NS kit component filled erroneously */
            /*next / * dsply * / l-disp.*/
        end. /* [F7] Keyword */

        leave f10key.
    end. /* do while true */

    /*tb 10289 03/05/93 kmw; Don't repaint screen */
    /*tb 19564 03/20/96 tdd; NS kit component filled erroneously */
    /*next l-disp.*/

    /*tb 22958 04/11/97 jl;  Screen does not redisplay after adding options */
    assign v-redispfl = yes.

end.            /* [F10] Opt/Key  */

/*tb 22958 04/11/97 jl;  Added v-redispfl */

/*o Finish upon <GO> */
if {k-accept.i} or {k-jump.i} or {k-cancel.i} or v-redispfl then do:

    /*tb 18575 07/18/95 ajw; Nonstock/Special Kit Component.  Code Split*/
    /*d Check for eXpected required options, keywords, or nonstock components.
        Determine if the user is allowed to leave the component screen.*/
    k-reqfl = no.

    run oeetkx.p(output v-nsreqfl).

    if v-nsreqfl = yes then do:
        /*e Required Non-Stock Component(s) Have Not Been Entered*/
        run err.p(6426).
        {pause.gsc}
        next main.
    end.

    if k-reqfl = yes then do:
        /*e Required Options or Keywords Have Not Been Entered*/
        run err.p(5958).
        {pause.gsc}
        next main.
    end.

    {oeetk.z99 &user_aft_oeetkx = "*"}
               
    hide message no-pause.
    run err.p(9611).

    /* If this is a fabricated kit then do not reload the availability.
       This functionality is not necessary in this case as the netavail
       for the kit is the actual in icsw in the sales whse and is not
       based on the components put on the kit.  The whse at this time
       is the fabrication whse which may or may not be the same as the

       sales whse.  The design is that the available for a fabricated
       kit should be the available from the kit level not the components
       as is done with regular BOD kits.  This allows the user to see that
       there is product available and elect to ship a kit that might
       already be in stock preassembled instead of going through the
       fabrication process. */

    if v-bodtransferty <> "t":u then do:

        /*d Calculate the qty available of the kit */
        run kpavail.p.

        assign
            v-buildqty = if s-specnstype = "n" and v-buildqty = 9999999.99
                             then 0
                         else v-buildqty
            s-netavail = truncate(v-buildqty / v-conv,0).

    end. /* if v-bodtransferty <> t */

    /*tb 19564 03/20/96 tdd; NS kit component filled erroneously */
    /*tb 22958 04/11/97 jl;  Screen does not redisplay after adding options */
    if {k-cancel.i} or v-redispfl then next l-disp.

end.