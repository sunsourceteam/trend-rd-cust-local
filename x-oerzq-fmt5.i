(if t-amounts{1}[v-inx4] = 0 then 0
 else if ((t-amounts{1}[v-inx4] - t-amounts{2}[v-inx4]) 
            / t-amounts{1}[v-inx4]) * 100  < -999 then 
         -999
 else if ((t-amounts{1}[v-inx4] - t-amounts{2}[v-inx4]) 
            / t-amounts{1}[v-inx4]) * 100  > 999 then
         999
 else ((t-amounts{1}[v-inx4] - t-amounts{2}[v-inx4]) 
         / t-amounts{1}[v-inx4]) * 100).
