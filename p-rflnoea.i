/* SX 55 */
/* p-lnoea.i 1.3 09/11/98 */
/* p-lnoea.i 1.6 05/08/97 */
/*h*****************************************************************************
  INCLUDE      : p-lnoea.i
  DESCRIPTION  : OE Create/assign of lot records and lot history
  USED ONCE?   : no (icetloe.p, icetloeo.p, icetloes.p, oeetbsl.p, oeetlcrs.p)
  AUTHOR       : kmw
  DATE WRITTEN :
  CHANGES MADE :
    05/15/90 kmw;           Also used in oeepbfo.p to copy PO lots to order
        on DO
    07/19/93 mwb; TB# 12241 Don't update icsel until invoicing for returns
    10/02/96 mtt; TB# 19484 (D1E) Warehouse Logistics Module
    10/15/96 mtt; TB# 21254 (2F) Develop Value Add Module
    03/07/97 jl;  TB# 7241 (7.2) Special Price Costing. Added assign of
        icspecrecno to icetl.
    05/08/97 kjb; TB# 17505 Lots inactivated when product still in lot causing
        that lot not to be displayed in the lot allocation screen
    07/31/98 mtt; TB# 24049 PV: Common code use for GUI interface
    09/10/98 mtt; TB# 24049 PV: Common code use for GUI interface
    11/11/99 bp ; TB #26804 Add conversion of the prodcost field.  This field
        will now be carried in the ICETL SPC unit, which should match the
        line or historical unit (but which may not be the same as the SPC
        unit on the line).  Because the "vc-" variables may not always be
        loaded, we use the b-icss buffer to make the conversion.
    09/21/06 sbr; TB# e24898 Lot detail does not print
    01/09/08 ejk; ERP-28328 Error icetl already exists.
*******************************************************************************/

/*    {& com}         - Used in oeepbfo.p
      {& return}      - Used for returns oeetlcrs.p
      {& comdisplay}  - To prevent WL from displaying data
      {& oeordertype} - Passed ordertype - either "o" for OE, "f" for VA
      {& ignorerecid  - Preprocessor to ignore recid assignments
*/

if not v-returnfl then do:

    if (not avail icetl) /* or (v-method = "o") */ then do:

        find first icetl where
                   icetl.cono      eq g-cono         and
                   icetl.prod      eq v-prod         and
                   icetl.whse      eq v-whse         and
                   icetl.lotno     eq icsel.lotno    and
                   icetl.ordertype eq {&oeordertype} and
                   icetl.orderno   eq v-ordno        and
                   icetl.ordersuf  eq v-ordsuf       and
                   icetl.lineno    eq v-lineno       and
                   icetl.seqno     eq v-seqno
        exclusive-lock no-error.

        if not avail icetl
        then do:
            create icetl.

            /*tb 17505 05/08/97 kjb; Lots inactivated when product still in lot
             causing that lot not to be displayed in the lot allocation screen.
             Changed the loading of the postdt field so that it is only loaded
             if the OE order has not been invoiced or the VA section has not
             been shipped.  This means that the postdt field will be blank until
             the OE order is invoiced or the VA section is shipped.  The benefit
             is that any ICETL without a postdt is still active and the lot
             can't be closed until all ICETLs for that lot have been posted.
            */
            assign
                s-lotno           = icsel.lotno
                icetl.cono        = g-cono
                icetl.prod        = v-prod
                icetl.whse        = v-whse
                icetl.lotno       = s-lotno
                icetl.orderno     = v-ordno
                icetl.ordersuf    = v-ordsuf
                icetl.lineno      = v-lineno
                icetl.seqno       = v-seqno
                icetl.ordertype   = {&oeordertype}
                icetl.icspecrecno = v-icspecrecno
                icetl.returnfl    = false
                icetl.custno      = g-custno
                icetl.shipto      = g-shipto
                icetl.ictype      = v-ictype
                icetl.prodcost    = icsel.prodcost * {speccost.gco
                                                     &curricss = "b-icss."
                                                     &histicss = "v-"}

                {&com}
                v-proofqty        = v-proofqty - s-quantity
                s-selectfl        = true
                /{&com}* */
            .
        end. /* if not avail icetl, create */

        assign
            icetl.quantity    = icetl.quantity + s-quantity
            icetl.postdt      = if {&oeordertype} = "o":u and
                                  can-find (first oeeh use-index k-oeeh where
                                      oeeh.cono     = g-cono   and
                                      oeeh.orderno  = v-ordno  and
                                      oeeh.ordersuf = v-ordsuf and
                                      oeeh.stagecd < 4)
                                  then ?
                                else if {&oeordertype} = "f":u and
                                  can-find (first vaes use-index k-vaes where
                                      vaes.cono  = g-cono   and
                                      vaes.vano  = v-ordno  and
                                      vaes.vasuf = v-ordsuf and
                                      vaes.seqno = v-seqno  and
                                      vaes.stagecd < 4)
                                  then ?
                                else g-today
            icsel.qtyavail    = if v-ictype = "do":u then icsel.qtyavail
                                else icsel.qtyavail - s-quantity
        .

        {t-all.i icetl}
        {t-all.i icsel}

        {&com}
        &IF "{&ignorerecid}" = "" &THEN
        if v-method ne "o" then v-recid2[frame-line]  = recid(icetl).
        &ENDIF
        /{&com}* */

    end. /* if (not avail icetl) or (v-method = "o") */

    {&com}
    else do:
        assign
            icsel.qtyavail = if v-ictype = "do" then icsel.qtyavail
                             else icsel.qtyavail + icetl.quantity - s-quantity
            v-proofqty     = v-proofqty + icetl.quantity - s-quantity
            icetl.quantity = s-quantity
            s-selectfl     = true.

        {t-all.i icetl}
        {t-all.i icsel}

        if s-quantity = 0 then do:
            delete icetl.
            s-selectfl = false.
        end.

    end.
    /{&com}* */

end.

{&com}
else do:
    if (not avail icetl) /* or (v-method = "o")  */ then do:

        create icetl.

        /*tb 17505 05/08/97 kjb; Lots inactivated when product still in lot
            causing that lot not to be displayed in the lot allocation screen.
            Changed the loading of the postdt field so that it is only loaded
            if the OE order has not been invoiced or the VA section has not
            been shipped.  This means that the postdt field will be blank until
            the OE order is invoiced or the VA section is shipped.  The benefit
            is that any ICETL without a postdt is still active and the lot
            can't be closed until all ICETLs for that lot have been posted. */
        assign
            s-lotno           = icsel.lotno
            icetl.cono        = g-cono
            icetl.prod        = v-prod
            icetl.whse        = v-whse
            icetl.lotno       = s-lotno
            icetl.orderno     = v-ordno
            icetl.ordersuf    = v-ordsuf
            icetl.lineno      = v-lineno
            icetl.seqno       = v-seqno
            icetl.ordertype   = {&oeordertype}
            icetl.returnfl    = true
            icetl.custno      = g-custno
            icetl.quantity    = s-quantity
            icetl.reasunavty  = s-reasunavty
            icetl.qtyunavail  = s-qtyunavail
            icetl.prodcost    = icsel.prodcost * {speccost.gco
                                                     &curricss = "b-icss."
                                                     &histicss = "v-"}

            icetl.postdt      = if {&oeordertype} = "o":u and
                                  can-find (first oeeh use-index k-oeeh where
                                      oeeh.cono     = g-cono   and
                                      oeeh.orderno  = v-ordno  and
                                      oeeh.ordersuf = v-ordsuf and
                                      oeeh.stagecd < 4)
                                  then ?
                                else if {&oeordertype} = "f":u and
                                  can-find (first vaes use-index k-vaes where
                                      vaes.cono  = g-cono   and
                                      vaes.vano  = v-ordno  and
                                      vaes.vasuf = v-ordsuf and
                                      vaes.seqno = v-seqno  and
                                      vaes.stagecd < 4)
                                  then ?
                                else g-today
            icetl.ictype      = v-ictype
            icetl.icspecrecno = v-icspecrecno
            v-qtyunavail      = v-qtyunavail + s-qtyunavail
            v-proofqty        = v-proofqty - s-quantity
            s-selectfl        = true.

        {t-all.i icetl}

        {&return}

        &IF "{&ignorerecid}" = "" &THEN
        if v-method ne "o" then v-recid2[frame-line] = recid(icetl).
        &ENDIF
    end.

    else do:
        assign
            icetl.reasunavty = s-reasunavty
            v-proofqty       = v-proofqty + icetl.quantity - s-quantity
            v-qtyunavail     = v-qtyunavail - icetl.qtyunavail + s-qtyunavail
            icetl.qtyunavail = s-qtyunavail
            icetl.quantity   = s-quantity
            s-selectfl       = true.

        {t-all.i icetl}

        {&return}

        if s-quantity = 0 then do:
            delete icetl.

            s-selectfl = false.

            /* need to reset icetl recid back to the invoiced icetl */
            if s-retorderno <> 0 then do:
                assign
                    o-ordno  = v-ordno
                    o-ordsuf = v-ordsuf
                    o-lineno = v-lineno
                    v-ordno  = s-retorderno
                    v-ordsuf = s-retordersuf
                    v-lineno = s-retlineno.

                find {p-icetl.i} no-lock no-error.

                &IF "{&ignorerecid}" = "" &THEN
                if avail icetl then v-recid2[frame-line] = recid(icetl).
                &ENDIF
            end.

        end.

    end.

end.

{&return}

/{&com}* */

{&comdisplay}
display v-proofqty with frame f-icetl.
/{&comdisplay}* */

