/***************************************************************************
 PROGRAM NAME: sts-po-acknow1.i
  DESCRIPTION: Defines variables for p/o acknowledgement subroutines 
 DATE WRITTEN: 11/18/99
       AUTHOR: Sir Carl E. Graul Esq.
     MODIFIED: 

***************************************************************************/

def {1} var s-chgdt as date.
def {1} var s-track-no as char format "x(35)".
def {1} var s-ackno as char format "x(35)".
def {1} var s-ackuser as char format "x(8)".
def {1} var s-ackcd as char format "x(1)".
def {1} var s-ackcd-v as char format "x(12)".
def {1} var s-maint-a as logical format "add/chg" init yes.

def {1} var oc-chgdt as date.
def {1} var oc-track-no as char format "x(35)".
def {1} var oc-ackno as char format "x(35)".
def {1} var oc-ackuser as char format "x(8)".
def {1} var oc-ackcd as char format "x(1)".
def {1} var oc-ackcd-v as char format "x(12)".

def {1} var orig-prod like icsw.prod. /* replacement for oc-prod */
def {1} var orig-duedt as date no-undo. /* replacement for oc-duedt */

/* das - ontime */
def {1} var ackdate    as date no-undo.
def {1} var expdate    as date no-undo.
/* das - ontime */

def {1} var sw-ack as logical init no.
def {1} var sw-ok-update as logical init no.
def {1} var sw-poetle as logical init no.
def {1} var cur-user as char format "x(8)".
def {1} var ack-recid as recid.
def {1} var ack-pass as integer.
def {1} var ack-user1 like poel.user1.
def {1} var ack-user2 like poel.user2.
def {1} var new-ackcd as char format "x(1)".

def {1} frame f-acknow1
        skip
        "             Acknowledge line  "
        sw-ack
        skip
        "         Acknowledgement code  "
        s-ackcd
        "  "
        s-ackcd-v
        skip
        " Date of last acknowledgement  "
        s-chgdt
        skip
        "                 Tracking no.  "
        s-track-no
        skip
        "          Acknowledgement no.  " 
        s-ackno     
        skip
    with no-labels row 10 centered 
        title "Acknowledgement Information".

def {1} frame f-acknow2
        skip
        "                     Due Date  "
        orig-duedt
        skip 
        "        1st Expected Shp Date  "
        ackdate
        skip
        "           Expected Ship Date  "
        expdate
        skip
        "             Acknowledge line  "
        sw-ack
        skip
        "         Acknowledgement code  "
        s-ackcd
        "  "
        s-ackcd-v
        skip
        " Date of last acknowledgement  "
        s-chgdt
        skip
        "                 Tracking no.  "
        s-track-no
        skip
        "          Acknowledgement no.  " 
        s-ackno     
        skip
    with no-labels row 10 centered 
        title "Acknowledgement Information".
         
        


