assign v-oneinthedirection = false.
if "{&direction}" = "prev" then
  do:
  find t-whseinfo where t-whseinfo.whse = s-whse[1] no-lock no-error.
  if not avail t-whseinfo then
    next.
  assign v-inx = 12.
  end.  
if "{&direction}" = "next" then
  do:
  find t-whseinfo where t-whseinfo.whse = s-whse[12] no-lock no-error.
  if not avail t-whseinfo then
    next.
  assign v-inx = 1.
  end.  

do v-inx2 = 1 to 12:    
  find {&direction} t-whseinfo no-lock no-error.
  if avail t-whseinfo and v-oneinthedirection = false then
    do:
    assign v-oneinthedirection = true.
    end.
  else
  if not avail t-whseinfo and v-oneinthedirection = false then
    do:
    leave.
    end.
    
  if "{&direction}" = "prev" and avail t-whseinfo then do:
    if t-whseinfo.zavail > 999 then
      assign disp-avail = "***".
    else
    if t-whseinfo.zavail < 0 then
      assign disp-avail = "***".
    else
      assign disp-avail = string(t-whseinfo.zavail,"zz9").

    if t-whseinfo.zoo > 999 then
      assign disp-oo = "***".
    else
    if t-whseinfo.zoo < 0 then
      assign disp-oo = "***".
    else
      assign disp-oo = string(t-whseinfo.zoo,"zz9").
     
    assign h-avail[v-inx] = disp-avail + "/" + disp-oo.
/*
    h-avail[winx] = string(t-whseinfo.zavail,"zz9") + "/" +
                    string(t-whseinfo.zoo,"zz9").
*/

  
      
      assign s-whse [v-inx] =  t-whseinfo.whse
/*             h-avail[v-inx] = string(t-whseinfo.zavail,"zzzzzz9") */
             v-inx = v-inx - 1.

  
  
  end.
  if "{&direction}" = "next" and avail t-whseinfo then do:
    if t-whseinfo.zavail > 999 then
      assign disp-avail = "***".
    else
    if t-whseinfo.zavail < 0 then
      assign disp-avail = "***".
    else
      assign disp-avail = string(t-whseinfo.zavail,"zz9").

    if t-whseinfo.zoo > 999 then
      assign disp-oo = "***".
    else
    if t-whseinfo.zoo < 0 then
      assign disp-oo = "***".
    else
      assign disp-oo = string(t-whseinfo.zoo,"zz9").
     
    assign h-avail[v-inx] = disp-avail + "/" + disp-oo.
/*
    h-avail[winx] = string(t-whseinfo.zavail,"zz9") + "/" +
                    string(t-whseinfo.zoo,"zz9").
*/

 
      assign s-whse [v-inx] =  t-whseinfo.whse
 /*            h-avail[v-inx] = string(t-whseinfo.zavail,"zzzzzz9") */
             v-inx = v-inx + 1.
  end.
  if "{&direction}" = "prev" and not avail t-whseinfo then
      assign s-whse [v-inx] =  "    "
             h-avail[v-inx] = "       "
             v-inx = v-inx - 1.
  if "{&direction}" = "next" and not avail t-whseinfo then
      assign s-whse [v-inx] =  "    "         
             h-avail[v-inx] =  "       " 
             v-inx = v-inx + 1.
             
end.           

display s-whse[1]  s-whse[2]  s-whse[3]  s-whse[4]  s-whse[5]  s-whse[6]
        h-avail[1] h-avail[2] h-avail[3] h-avail[4] h-avail[5] h-avail[6]
        s-whse[7]  s-whse[8]  s-whse[9]  s-whse[10] s-whse[11]  s-whse[12]
        h-avail[7] h-avail[8] h-avail[9] h-avail[10] h-avail[11] h-avail[12]
        with frame f-whses.
