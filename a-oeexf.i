/******************************************************************************
  INCLUDE      : a-oeexf.i
  DESCRIPTION  : Detail Quote display line for OEEXF
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN : 
  CHANGES MADE :
               
                
                
                
*******************************************************************************/

if (({k-func6.i} or {k-func8.i}) and not {c-chan.i}) or
    {k-func7.i} or {k-func9.i}
  then 
  do:
  
  if {k-func6.i} then 
    do:
    g-title = "Create Follow-Up".
    run oeexfa.p.
    display with frame f-oeexfs.
  end.
  else if {k-func7.i} then 
    do:
    assign
       g-title = "View Follow-Up"
       g-prod  = if avail oeelb then oeelb.shipprod else "".
    run oeexfi.p.
    display with frame f-oeexfs.
  end.

  if {k-func8.i} then
    do:
    assign /*g-orderno  = v-quoteno*/
           w-currproc = g-currproc
           w-ourproc  = g-ourproc
           g-currproc = "oeizo"
           v-quoteno  = g-orderno.
    display 
            blank-line1 
            blank-line3 
            blank-line4 
            blank-line5 
            blank-line6 
            blank-line7 
            blank-line8 
            blank-line9 
            blank-line10
            blank-line11
            blank-line12
            blank-line13
            blank-line14
            blank-line15
            blank-line16
            blank-line17
            blank-line18
            blank-line19
            blank-line20
            blank-line21
            blank-line22
            blank-line23
            with frame blank-screen.
    run zsdiob.p.
    assign g-currproc = w-currproc
           g-ourproc  = w-ourproc
           g-orderno  = v-quoteno.
    if not avail oeehb then
      find oeehb where oeehb.cono       = g-cono and
                       oeehb.batchnm    = string(g-orderno,">>>>>>>9") and
                       oeehb.sourcepros = "Quote"                      
                       no-lock no-error.                               
    display
      g-orderno
      oeehb.custno
      arsc.notesfl
      arsc.lookupnm
      oeehb.shipto
      "" @ arss.notesfl
      arss.notesfl when oeehb.shipto <> "" and avail arss
      oeehb.transtype
      oeehb.takenby
      s-stagecd
      oeehb.bofl
      s-orderdisp
      oeehb.totinvamt
      oeehb.enterdt
      oeehb.reqshipdt
      oeehb.promisedt
      oeehb.canceldt
    with frame f-oeexfs.

  end.

  if {k-func9.i} then
    do:
    assign v-quoteno  = g-orderno
           w-currproc = g-currproc
           w-ourproc  = g-ourproc
           g-currproc = "oeexq".
    display 
            blank-line1 
            blank-line3 
            blank-line4 
            blank-line5 
            blank-line6 
            blank-line7 
            blank-line8 
            blank-line9 
            blank-line10
            blank-line11
            blank-line12
            blank-line13
            blank-line14
            blank-line15
            blank-line16
            blank-line17
            blank-line18
            blank-line19
            blank-line20
            blank-line21
            blank-line22
            blank-line23
            with frame blank-screen.
    run oeexq.p.
    assign g-currproc = w-currproc
           g-ourproc  = w-ourproc
           g-orderno  = v-quoteno.
    if not avail oeehb then
    find oeehb where oeehb.cono       = g-cono and
                     oeehb.batchnm    = string(g-orderno,">>>>>>>9") and
                     oeehb.sourcepros = "Quote"                      
                     no-lock no-error.                               
  display
      g-orderno
      oeehb.custno
      arsc.notesfl
      arsc.lookupnm
      oeehb.shipto
      "" @ arss.notesfl
      arss.notesfl when oeehb.shipto <> "" and avail arss
      oeehb.transtype
      oeehb.takenby
      s-stagecd
      oeehb.bofl
      s-orderdisp
      oeehb.totinvamt
      oeehb.enterdt
      oeehb.reqshipdt
      oeehb.promisedt
      oeehb.canceldt
    with frame f-oeexfs.

  end. /* if {k-func9.i} */
  /**
  if not avail oeehb then
    find oeehb where oeehb.cono       = g-cono and
                     oeehb.batchnm    = string(g-orderno,">>>>>>>9") and
                     oeehb.sourcepros = "Quote"                      
                     no-lock no-error.                               
  display
      g-orderno
      oeehb.custno
      arsc.notesfl
      arsc.lookupnm
      oeehb.shipto
      "" @ arss.notesfl
      arss.notesfl when oeehb.shipto <> "" and avail arss
      oeehb.transtype
      oeehb.takenby
      s-stagecd
      oeehb.bofl
      s-orderdisp
      oeehb.totinvamt
      oeehb.enterdt
      oeehb.reqshipdt
      oeehb.promisedt
      oeehb.canceldt
    with frame f-oeexfs.
  */
  put screen row 21 col 3 color message v-fmsg.

  /*tb 15236 04/04/94 rs; Channel gets jump box from F7 extended */
  if ({k-jump.i} or {k-select.i}) and not {c-chan.i} then leave main.

  hide message no-pause.
  next dsply.
end. /* function key */
else if {k-func19.i} then run oeexqcomd.p.
else if {k-select.i} then next.

put screen row 21 col 3 color message v-fmsg.
