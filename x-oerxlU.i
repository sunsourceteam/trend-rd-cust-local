/{&user_temptable}*/
 

Define Var b-Stage         like oeeh.stagecd   no-undo.
Define Var e-Stage         like oeeh.stagecd   no-undo.
Define Var b-EnterDt       as   date           no-undo.
Define Var e-EnterDt       as   date           no-undo.
Define Var b-InvoiceDt     as   date           no-undo.
Define Var e-InvoiceDt     as   date           no-undo.
Define Var b-Region        as   char format "x(4)"
                                               no-undo.
Define Var e-Region        as   char format "x(4)"
                                               no-undo.

Define Var v-DateCheck     as   date           no-undo.


Define Var b-CSR            like oeel.slsrepin  no-undo.
Define Var e-CSR            like oeel.slsrepin  no-undo.
Define Var b-CSM            like oeel.slsrepin  no-undo.
Define Var e-CSM            like oeel.slsrepin  no-undo.
Define Var b-chgdt          as date             no-undo.
Define Var e-chgdt          as date             no-undo.
Define Var p-format         as  character       no-undo.
Define Var p-list           as  logical         no-undo.
Define Var v-transDt        as date  extent 2   no-undo.
/* New */
Define Var p-days           as  int             no-undo.
Define Var p-excludeBRfl   as  logical         no-undo.
Define Var v-inxbl          as int             no-undo.
/* New */




def var h-person            like oimsp.perdept  no-undo.
Define var  x-ordcnt        as decimal          no-undo.
Define var  x-chgcnt        as decimal          no-undo.
Define var  v-csr-plus-name as char             no-undo.
Define var  v-csr           as char             no-undo.
Define var  v-csm-plus-name as char             no-undo.
Define var  v-csm           as char             no-undo.
Define var  v-daystoship    as int   format ">>>>9-"  no-undo.
Define var  v-chgby         like oeeh.operinit  no-undo.
Define var  v-seclevel      as int format "9"   no-undo.
Define var  v-name          like arsc.name      no-undo.
Define var  v-oReqDt        as date format "99/99/99" no-undo.
Define var  v-nReqDt        as date format "99/99/99" no-undo. 
Define var  v-comment       as char format "x(12)"    no-undo. 
Define var  v-reqdiff       as char format "x"        no-undo.
 
 
{zsapboydef.i}

define {&sharetype} temp-table xlog no-undo
  field lgrecid           as recid
  field transdt           as date 
  field transtm           as char
  field operinit          as char
  field csr               as character  format "x(4)"
  field csr-plus-name     as character  format "x(35)"
  field csm               as character  format "x(4)"
  field csm-plus-name     as character  format "x(35)"
 
  field orderno           like oeel.orderno
  field ordersuf          like oeel.ordersuf
  field lineno            like oeel.lineno
  field ordcnt            as decimal 
  field chgcnt            as decimal   
  field Region            as char
  field Takenby           as char
  field ChgBy             as char
  index x1
     lgrecid
  
  index xx
     orderno
     ordersuf
     lineno 
     transdt.
 define buffer b-xlog for xlog.
  
 
 
                                    
 
 
/{&user_temptable}*/
 
/{&user_drpttable}*/
/* Use to add fields to the report table */
  field xrecid   as recid
 
 
 
 
 
 
/{&user_drpttable}*/
 
/{&user_forms}*/
form header
  "~015 " at 1
  with frame fx-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: OERXL"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 168
  page-number(xpcd)    at 173 format ">>>9"
  "~015"               at 177
         "Order Promise Date Maintenance Report" at 71
         "~015"              at 177
   with frame fx-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header
  "~015 " at 1
  with frame f-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: OERXL"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 168
  page-number          at 173 format ">>>9"
  "~015"              at 177
  "Order Promise Date Maintenance Report" at 71
  "~015"              at 177
   with frame f-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header 
   "~015"                    at 177
  with frame f-trn3 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
            
            
form header 
   "~015"                       at 177
  with frame f-trn7 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
            
            
form
  v-final                at 1
  v-astrik               at 2   format "x(5)"
  v-summary-lit2         at 7   format "x(52)"
  v-amt1           at 133  format ">>>,>>9-"  /* count   Lines       */
  v-amt2           at 142  format ">>>,>>9-"  /* changed Lines       */
  v-amt3           at 153  format ">>>,>>9-"  /* changed Lines Out   */
  "~015"           at 178
with frame f-tot width 178
 no-box no-labels.  
form 
  "~015"           at 1
with frame f-skip width 178 no-box no-labels.  
 
define var   v-oPromiseDt    as date no-undo.              
Define var   v-nPromiseDt    as date no-undo.              
define var   v-days          as integer   format ">>>>>9-" no-undo.             

form header 
   "Customer Name"              at 1
   "OrderNo"                    at 34                
   "Line"                       at 42
   "TknBy"                      at 47
   "OIMSP Comment"              at 54
   "LineAmt"                    at 72
   "DaysChg"                    at 81
   "EnterDt"                    at 90
   "ShipDt"                     at 99
   "Old ReqDt"                  at 107
   "New ReqDt"                  at 117
   "Old PromDt"                 at 129
   "New PromDt"                 at 140
   "DaysToShip"                 at 152
   "Chg By"                     at 163
   "SecLvl"                     at 172
   "~015"                       at 178
  with frame f-trn4 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
  
form  
   v-name                        at 1
   
 
   zsdioeellog.OrderNo           at 32 format ">>>>>>>9"
   "-"                           at 40
   zsdioeellog.ordersuf          at 41
   zsdioeellog.lineno            at 43
   oeeh.takenby                  at 47
   v-comment                     at 55
   oeel.netord                   at 69
   v-days                        at 82
   oeel.enterDt                  at 90
   oeeh.shipDt                   at 99
   v-oReqDt                      at 108
   v-nReqDt                      at 118  
   v-reqdiff                     at 127
   v-oPromiseDt                  at 129
   zsdioeellog.User12            at 137 format "X"
   v-nPromiseDt                  at 140
   zsdioeellog.user13            at 148 format "X"
   v-daystoship                  at 152
   v-chgby                       at 164
   v-seclevel                    at 175
  with frame f-dtlprt down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
  
 
 
 
/{&user_forms}*/
 
/{&user_extractrun}*/
 run sapb_vars.
 run extract_data.
 
 
 
 
 
/{&user_extractrun}*/
 
/{&user_exportstatDWheaders}*/
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  
  if p-detail = "d"  then do:
    assign export_rec = export_rec + v-del +
                       "Customer Name" + v-del +   
                       "OrderNo"       + v-del +
                       "Line"          + v-del +
                       "TknBy"         + v-del +
                       "OIMSP Comment" + v-del +   
                       "LineAmt"       + v-del +   
                       "DaysChg"       + v-del +     
                       "EnterDt"       + v-del +   
                       "ShipDt"        + v-del +       
                       "Old ReqDt"     + v-del +    
                       "New ReqDt"     + v-del +     
                       "ReqDt Changed" + v-del +
                       "Old PromDt"    + v-del +   
                       "Old PromDt Code" + v-del +
                       "New PromDt"    + v-del +   
                       "New PromDt Code" + v-del +
                        
                       "DaysToShip"    + v-del +    
                       "Chg By"        + v-del +    
                       "SecLvl"        + v-del +    
      
                       "LineCount"  + v-del +
                       "LinesChanged"   + v-del +
                       "LineChangedOut".
   end.
  else  
    assign export_rec = export_rec + v-del +
                        "LineCount"  + v-del +
                        "LinesChanged"   + v-del +
                        "LineChangedOut".
                          
 
/{&user_exportstatDWheaders}*/
 
/{&user_exportstatheaders}*/
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  
  
  if p-detail = "d"  then do:
    assign export_rec = export_rec + v-del +
                       "Customer Name" + v-del +   
                       "OrderNo"       + v-del +
                       "Line"          + v-del +
                       "TknBy"         + v-del +
                       "OIMSP Comment" + v-del +   
                       "LineAmt"       + v-del +   
                       "DaysChg"       + v-del +     
                       "EnterDt"       + v-del +   
                       "ShipDt"        + v-del +       
                       "Old ReqDt"     + v-del +    
                       "New ReqDt"     + v-del +     
                       "ReqDt Changed" + v-del +
                       "Old PromDt"    + v-del +   
                       "Old PromDt Code" + v-del +
 
                       "New PromDt"    + v-del +   
                       "New PromDt Code" + v-del +
                  
                       "DaysToShip"    + v-del +    
                       "Chg By"        + v-del +    
                       "SecLvl"        + v-del +    
      
                       "LineCount"  + v-del +
                       "LinesChanged"   + v-del +
                       "LineChangedOut".
   end.
  else  
    assign export_rec = export_rec + v-del +
                      "LineCount"  + v-del +
                      "LinesChanged"   + v-del +
                      "LineChangedOut".
 
 
/{&user_exportstatheaders}*/
 
/{&user_foreach}*/
for each xlog use-index xx no-lock:
 
/{&user_foreach}*/
 
/{&user_drptassign}*/
 /* If you add fields to the dprt table this is where you assign them */
   drpt.xrecid = recid(xlog)
 
 
/{&user_drptassign}*/
 
/{&user_viewheadframes}*/
/* Since frames are unique to each program you need to put your
   views and hides here with your names */
if not p-exportl then
  do:
  if not x-stream then
    do:
/*  hide frame f-trn1.  */
    hide frame f-trn2.
    hide frame f-trn3.
    hide frame f-trn4.
    
    end.
  else
   do:
/*  hide stream xpcd frame f-trn1.  */
    hide stream xpcd frame fx-trn2.
    hide stream xpcd frame f-trn3.
    hide stream xpcd frame f-trn4.
    end.
  
  end.
if not p-exportl  then
  do:
  if not x-stream then
    do:
    page.
/*  view frame f-trn1.   */
    view frame f-trn2.
    view frame f-trn3.
    view frame f-trn4.
    
    end.
  else  
    do:
    page stream xpcd.
/*  view stream xpcd frame f-trn1. */
    view stream xpcd frame fx-trn2.
    view stream xpcd frame f-trn3.
    view stream xpcd frame f-trn4.
    
    end.
   
  end.
 
 
 
 
 
 
/{&user_viewheadframes}*/
 
/{&user_drptloop}*/
 
 
/{&user_drptloop}*/
 
/{&user_detailprint}*/
   /* Comment this out if you are not using detail 'X' option  */
   if p-detail = "d"  and 
      entry(u-entitys,drpt.sortmystery,v-delim) <> 
      fill("~~",length(entry(u-entitys,drpt.sortmystery,v-delim))) then 
     do:
     /* this code skips the all other lines for printing detail */
     
     /* next line would be your detail print procedure */
     run detail-print (input drpt.xrecid).
 
     
     end.
 
 
 
 
 
 
/{&user_detailprint}*/
 
/{&user_finalprint}*/
if not p-exportl then
 do:
 
 
 if not x-stream then 
   do:
   display 
    v-final
    v-astrik
    v-summary-lit2
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts2[u-entitys + 1]   @ v-amt2
    t-amounts3[u-entitys + 1]   @ v-amt3
    with frame f-tot.
  
   down with frame f-tot.
   end.
 else
   do:
   display stream xpcd
    v-final
    v-astrik
    v-summary-lit2
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts2[u-entitys + 1]   @ v-amt2
    t-amounts3[u-entitys + 1]   @ v-amt3
    with frame f-tot.
  
   down stream xpcd with frame f-tot.
   end.
      
 end.
else
  do:
  assign export_rec =  v-astrik.
  do v-inx4 = 1 to u-entitys:
    if v-totalup[v-inx4] = "n" then
       next.
    if v-inx4 = 1 then
      v-summary-lit2 = "Final".
    else
      v-summary-lit2 = "".
    if v-cells[v-inx4]= "2" then
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec + v-summary-lit2 + v-del + " ".
     
      end.
    else
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec +  v-summary-lit2.
      end.
  end.
 
  if p-detail = "d"  then 
     
    assign export_rec = export_rec 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del +
      string(t-amounts1[u-entitys + 1],"->>>>>>>>9")  + v-del +
      string(t-amounts2[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts3[u-entitys + 1],"->>>>>>>>9") .
  else
    assign export_rec = export_rec + v-del +
      string(t-amounts1[u-entitys + 1],"->>>>>>>>9")  + v-del +
      string(t-amounts2[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts3[u-entitys + 1],"->>>>>>>>9") .
   

    
    
  assign export_rec = export_rec + chr(13).    
  put stream expt unformatted export_rec.
  end.
 
 
 
 
 
 
/{&user_finalprint}*/
 
/{&user_summaryframeprint}*/
  
  
    if not x-stream then
      do:
      display 
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        t-amounts3[v-inx2]   @ v-amt3
        with frame f-tot.
      down with frame f-tot.
      end.
    else
      do:
      display stream xpcd
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        t-amounts3[v-inx2]   @ v-amt3
        with frame f-tot.
      down stream xpcd with frame f-tot.
      end.
 
 
 
 
 
 
/{&user_summaryframeprint}*/
 
/{&user_summaryputexport}*/
 
  
 
    assign export_rec = v-astrik + v-del + export_rec.
 
  if p-detail = "d"  then 
    assign export_rec = export_rec  
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del 
    + v-del +
 
      string(t-amounts1[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts2[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts3[v-inx2],"->>>>>>>>9").
else
    assign export_rec = export_rec + v-del +
 
      string(t-amounts1[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts2[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts3[v-inx2],"->>>>>>>>9").
 
 
/{&user_summaryputexport}*/
 
/{&user_procedures}*/
procedure sapb_vars:

  assign
    b-Stage      = int(substring(sapb.rangebeg[1],1,1))
    e-Stage      = int(substring(sapb.rangeend[1],1,1))
    b-Region     = sapb.rangebeg[4]
    e-Region     = sapb.rangeend[4]
    
    p-detail  = if sapb.optvalue[2] = "d" then "d" else "s" 
    p-export  = sapb.optvalue[3]
    p-excludeBRfl = if sapb.optvalue[4] = "yes" then true else false
    p-days    = int(sapb.optvalue[5]).

 
  if sapb.rangebeg[2] ne "" then do: 
    v-datein = sapb.rangebeg[2].
    {p-rptdt.i}
  if string(v-dateout) = v-lowdt then
    b-EnterDt = 01/01/1900.
  else  
    b-EnterDt = v-dateout.
  end.
  else
    b-EnterDt = 01/01/1900.

  if sapb.rangeend[2] ne "" then do: 
    v-datein = sapb.rangeend[2].
    {p-rptdt.i}
  if string(v-dateout) = v-lowdt then
    e-EnterDt = 12/31/49.
  else  
    e-EnterDt = v-dateout.
  end.
  else
    e-EnterDt = 12/31/49.
   


  if sapb.rangebeg[3] ne "" then do: 
    v-datein = sapb.rangebeg[3].
    {p-rptdt.i}
  if string(v-dateout) = v-lowdt then
    b-InvoiceDt = 01/01/1900.
  else  
    b-InvoiceDt = v-dateout.
  end.
  else
    b-InvoiceDt = 01/01/1900.

  if sapb.rangeend[3] ne "" then do: 
    v-datein = sapb.rangeend[3].
    {p-rptdt.i}
  if string(v-dateout) = v-lowdt then
    e-InvoiceDt = 12/31/49.
  else  
    e-InvoiceDt = v-dateout.
  end.
  else
    e-EnterDt = 12/31/49.
   

  if sapb.rangebeg[5] ne "" then do: 
    v-datein = sapb.rangebeg[5].
    {p-rptdt.i}
  if string(v-dateout) = v-lowdt then
    b-ChgDt = 01/01/1900.
  else  
    b-ChgDt = v-dateout.
  end.
  else
    b-ChgDt = 01/01/1900.

  if sapb.rangeend[5] ne "" then do: 
    v-datein = sapb.rangeend[5].
    {p-rptdt.i}
  if string(v-dateout) = v-lowdt then
    e-ChgDt = 12/31/49.
  else  
    e-ChgDt = v-dateout.
  end.
  else
    e-ChgDt = 12/31/49.
   
  
  run formatoptions.
  
end.
/* ------------------------------------------------------------------------- */
procedure formatoptions:
/* ------------------------------------------------------------------------- */
   
   
  assign p-portrait = false.
  /* if false then the report will print in LANDSCAPE 178 character */
  
  if sapb.optvalue[1] > "0" and sapb.optvalue[1] ne "99" then 
    do:
    p-optiontype = " ".
    p-format = string(int(sapb.optvalue[1])).
    find notes where notes.cono = g-cono and
                     notes.notestype = "zz" and
                     notes.primarykey = "oerzl" and  /* tbxr program */
                     notes.secondarykey = string(p-format) no-lock no-error.
    if not avail notes then
      do:
      display "Format is not valid cannot process request".
      assign p-optiontype = "r"
             p-sorttype   = ">,"
             p-totaltype  = "l"
             p-summcounts = "a".
      return.
      end.               
   
    assign p-optiontype = notes.noteln[1]
           p-sorttype   = notes.noteln[2]
           p-totaltype  = notes.noteln[3]
           p-summcounts = notes.noteln[4]
           p-register   = notes.noteln[5]
           p-registerex = notes.noteln[6].
    
    find x-sapb where recid(x-sapb) = recid(sapb) exclusive-lock.
    
    assign x-sapb.user5 = 
             p-optiontype + "~011" +
             p-sorttype   + "~011" +
             p-totaltype  + "~011" +
             p-summcounts + "~011" +
             p-register   + "~011" +
             p-registerex.     
    
    
    end.     
  else
  if sapb.optvalue[1] = "99" then 
    do:
    assign p-register   = ""
           p-registerex = "".
    run reportopts(input sapb.user5, 
                   input-output p-optiontype,
                   input-output p-sorttype,  
                   input-output p-totaltype,
                   input-output p-summcounts,
                   input-output p-register,
                   input-output p-registerex).
    end.
 run Print_reportopts (input recid (sapb),
                       input g-cono).
 end.
   
 
procedure extract_data:
 
  for each zsdioeellog where                                                   
           zsdioeellog.cono = g-cono and
           zsdioeellog.user1 = "Plog"  no-lock:               
 
    if  zsdioeellog.transdt < b-ChgDt or
        zsdioeellog.transdt > e-ChgDt then 
      next. 

    
    find oeel where 
         oeel.cono = g-cono and 
         oeel.orderno = zsdioeellog.orderno and                       
         oeel.ordersuf = zsdioeellog.ordersuf and 
         oeel.lineno   = zsdioeellog.lineno no-lock no-error.                      find oeeh where 
         oeeh.cono = g-cono and 
         oeeh.orderno = zsdioeellog.orderno and                       
         oeeh.ordersuf = zsdioeellog.ordersuf no-lock no-error.                
    
    if not avail oeel or 
      (avail oeeh and (oeeh.stagecd = 9 or oeeh.stagecd = 0)) or
      (avail oeel and oeel.specnstype = "l") then
      next.
    assign v-csr-plus-name = zsdioeellog.operinit
           v-csr           = zsdioeellog.operinit
           v-csm-plus-name = " "
           v-csm           = " ".
   
    find oimsp use-index k-oimsp where
         oimsp.person = zsdioeellog.operinit no-lock no-error.
    if avail oimsp then do:
      assign v-csr-plus-name = zsdioeellog.operinit + " - " + oimsp.name
             v-csm           = oimsp.miscdata[2].
    end.           
   
    if v-csm <> " " then do:    
      find oimsp use-index k-oimsp where
           oimsp.person = v-csm no-lock no-error.
      if avail oimsp then do:
        assign v-csm-plus-name = v-csm + " - " + oimsp.name.
      end.
    end.                             
    
/*
    if v-csm < b-csm or v-csm > e-csm then
      next.
    if v-csr < b-csr or v-csr > e-csr then
      next.
*/

    if not avail oeel or 
      (avail oeeh and (oeeh.stagecd < b-Stage or 
                       oeeh.stagecd > e-Stage))  then
      next.
 
    assign v-DateCheck =    
      (if oeeh.transtype = "fo" or oeeh.transtype = "br" then
         oeeh.createdt 
       else
         oeel.enterdt).       
 
    if avail oeeh and (v-DateCheck < b-EnterDt or
                       v-DateCheck > e-EnterDt) then
      next.

     assign v-DateCheck = (if oeeh.stagecd ge 4 and oeeh.stagecd le 5 then
                            oeeh.invoicedt
                          else
                            b-invoiceDt - 1).

    if b-InvoiceDt = 01/01/1900 and
       e-InvoiceDt = 12/31/49 then do:
      /* Null Statement */
    end.
    else do:   
      if avail oeeh and (v-DateCheck < b-InvoiceDt or
                         v-DateCheck > e-InvoiceDt) then
        next.
    end.                    
   
    find oimsp use-index k-oimsp where
         oimsp.person = oeeh.takenby no-lock no-error.
    if avail oimsp then 
      assign v-comment       = oimsp.comment.  
    else
      assign v-comment       = " ".

 
    if v-comment < b-Region or
       v-comment > E-Region then
      next. 
       
   
    date ( zsdioeellog.User8 ) no-error.
    if error-status:error then 
      assign  v-opromisedt = ?.   
    else
      assign v-opromisedt =  date( zsdioeellog.User8 ).

    date ( zsdioeellog.User9 ) no-error.
    if error-status:error then 
      assign  v-npromisedt = ?.   
    else
      assign v-npromisedt =  date( zsdioeellog.User9 ).
 
   if v-npromisedt = v-opromisedt then
      next.
 
   assign v-days = zsdioeellog.transdt -   
    (if oeeh.transtype = "fo" or oeeh.transtype = "br" then
      oeeh.createdt 
   else
      oeel.enterdt).       

    if p-days <> 0 then do:
      if v-days <= p-days then do:
          /* Null statment */
      end.   
      else do:
        next.
      end.
    end.
    
    assign v-chgby = zsdioeellog.operinit.
 
    
    create xlog.
    assign
      xlog.lgrecid     = recid(zsdioeellog)
      xlog.transdt     = zsdioeellog.transdt
      xlog.chgby       = v-chgby
      xlog.takenby     = oeeh.takenby
      xlog.region      = v-comment
      xlog.csr         = caps(oeel.slsrepin)
      xlog.csm         = caps(v-csm)
      xlog.csm-plus-name   = caps(v-csm-plus-name)
      xlog.csr-plus-name   = caps(v-csr-plus-name)
       
      xlog.orderno     = zsdioeellog.orderno
      xlog.ordersuf    = zsdioeellog.ordersuf        
      xlog.lineno      = zsdioeellog.lineno        
      xlog.ordcnt      = 1.
      
    if zsdioeellog.User8 <> zsdioeellog.User9 then
      xlog.chgcnt      = 1.        
        
  end.
end.

procedure detail-print:
 define input parameter ip-recid as recid no-undo.
 
 find b-xlog where recid(b-xlog) = ip-recid no-lock no-error.
 if not avail b-xlog then
   return.
 
 
 find zsdioeellog where recid(zsdioeellog) = b-xlog.lgrecid no-lock no-error.
 if not avail zsdioeellog then
   return.
    
 find oeel where 
      oeel.cono = 1 and 
      oeel.orderno = zsdioeellog.orderno and                       
      oeel.ordersuf = zsdioeellog.ordersuf and 
      oeel.lineno   = zsdioeellog.lineno no-lock no-error.  
 if not avail oeel or oeel.specnstype = "l"  then 
   return.
 find oeeh where 
      oeeh.cono = g-cono and 
      oeeh.orderno = zsdioeellog.orderno and                       
      oeeh.ordersuf = zsdioeellog.ordersuf no-lock no-error.  
 if not avail oeeh then 
   return.
    
 date ( zsdioeellog.User8 ) no-error.
 if error-status:error then 
   assign  v-opromisedt = ?.   
 else
   assign v-opromisedt =  date( zsdioeellog.User8 ).

 date ( zsdioeellog.User9 ) no-error.
 if error-status:error then 
   assign  v-npromisedt = ?.   
 else
   assign v-npromisedt =  date( zsdioeellog.User9 ).
    
 assign  
   v-oReqDt  = zsdioeellog.oldreqshipdt   
   v-nReqDt  = zsdioeellog.newreqshipdt.   
 
 
 assign v-days = zsdioeellog.transdt -   
    (if oeeh.transtype = "fo" or oeeh.transtype = "br" then
      oeeh.createdt 
   else
      oeel.enterdt).       


 find arsc where 
      arsc.cono = g-cono and
      arsc.custno = oeeh.custno no-lock no-error.
 find arss where 
      arss.cono = g-cono and
      arss.custno = oeeh.custno and
      arss.shipto = oeeh.shipto no-lock no-error.
 if avail arss then
   assign v-name = arss.name.
 else if avail arsc then
   assign v-name = arsc.name.
 assign v-chgby = zsdioeellog.operinit.

 find first sasos where                               
            sasos.cono  = g-cono and                  
            sasos.oper2 = zsdioeellog.operinit and             
            sasos.menuproc = "zxt1" no-lock  no-error.                                 
 if avail sasos then
   assign v-seclevel = sasos.securcd[6].
 else
   assign v-seclevel = 0.                   

 assign v-daystoship    =  oeeh.shipdt - zsdioeellog.transdt.  

 if v-daystoship = ? then 
   assign v-daystoship = 0.
   
 find oimsp use-index k-oimsp where
      oimsp.person = oeeh.takenby no-lock no-error.
 if avail oimsp then 
   assign v-comment       = oimsp.comment.  
 else
   assign v-comment       = " ".

 assign v-reqdiff = if v-oReqDt <>  v-nReqDt  then
                      "*"
                    else
                      " ".
 if p-export <> " " then do:
 
   ASSIGN 
     export_rec = "".
 
   DO v-inxbl = 1 TO u-entitys:
     IF v-totalup[v-inxbl] <> "n" THEN DO:
       assign export_rec = export_rec + v-del + " ".
     end.
   END.
   
   assign export_rec = export_rec + v-del +
     v-name                   + v-del +   
     string(zsdioeellog.OrderNo) + "-" +
     string(zsdioeellog.ordersuf,"99") + v-del +      
     string(zsdioeellog.lineno)        + v-del +    
     oeeh.takenby              + v-del +     
     v-comment                 + v-del +       
     string(oeel.netord)               + v-del +     
     string(v-days,"99999-")                    + v-del +     
       
     (if oeel.enterdt = ? then
       " "
     else  
       string(oeel.enterDt,"99/99/99"))              + v-del +   
     (if oeeh.shipDt = ? then
       " "
     else  
       string(oeeh.shipDt,"99/99/99"))               + v-del +   

     (if v-oReqDt = ? then
       " "
     else  
       string(v-oReqDt,"99/99/99"))                  + v-del +      
     (if v-nReqDt = ? then
       " "
     else  
       string(v-nReqDt,"99/99/99"))                  + v-del +      
     v-reqdiff                                       + v-del +
     (if v-oPromiseDt = ? then
       " "
     else  
       string(v-oPromiseDt,"99/99/99"))              + v-del +      
     zsdioeellog.User12        + v-del +     
     (if v-nPromiseDt = ? then
       " "
     else  
       string(v-nPromiseDt,"99/99/99"))              + v-del +        

     zsdioeellog.user13        + v-del +     

     string(v-daystoship,"99999-" )             + v-del +        

     v-chgby                   + v-del +       
     string(v-seclevel).     
   export_rec = export_rec + CHR(13).
   PUT STREAM expt UNFORMATTED export_rec.
 
 end.   
 else do :
 
   display           
     v-name                     
     zsdioeellog.OrderNo      
     zsdioeellog.ordersuf     
     zsdioeellog.lineno   
     oeeh.takenby    
     v-comment      
     oeel.netord    
     v-days       
     oeel.enterDt  
     oeeh.shipDt  
     v-oReqDt     
     v-nReqDt     
     v-reqdiff
     v-oPromiseDt      
     zsdioeellog.User12    
     v-nPromiseDt      
     zsdioeellog.user13    
     v-daystoship      
     v-chgby      
     v-seclevel     
    with frame f-dtlprt.
 
    down with frame f-dtlprt.
  end.


end.
   
  
  
  
  
{zsapboycheck.i}
 
 
 
 
 
 
/{&user_procedures}*/
 
