/* p-oeeaid.i 1.1 01/03/98 */
/* p-oeeaid.i 1.1 01/03/98 */
/*h*****************************************************************************  INCLUDE      : p-oeepaid.i
  DESCRIPTION  : Fields used in the "shipto" record in the edi 810, 843, and 855
  USED ONCE?   : no
  AUTHOR       : jlc
  DATE WRITTEN : 02/23/94
  CHANGES MADE :
    TB# e17245 05/12/03 lbr; Added shipto dunsno
    TB# e18165 08/10/03 jlc; Add additional identifiers for eBill
*******************************************************************************/
"Shipto" +
    caps(s-shiptonm) +
    caps(s-shiptoaddr[1]) +
    caps(s-shiptoaddr[2]) +
    caps(s-shiptocity) +
    caps(s-shiptostate) +
    caps(s-shiptozipcd) +
    caps(s-shiptophone) +
    caps(s-shiptouser1) +
    caps(s-shiptouser2) +
    caps(s-shipto)   /* +
    caps(s-arssdunsno)  +
    caps(s-arsserpid)   +
    caps(s-arsstpid) */