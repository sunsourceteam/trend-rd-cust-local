/* --------------------------------------------------------------------
   Include :     p-zfindbuy.i
   Description:  Determine buyer to be displayed
                 Parameter {1} is Cono
                 Parameter {2} is orderno
                 Parameter {3} is orderuf
                 Parameter {4} is line number
                           {5} is the seq
                 parameter {6} is buyer returned
                 parameter {7} is the prodline
                 parameter {8} is search number
                
   Date Written: 4/20/99
   Changes Made:


----------------------------------------------------------------------- */
/*
def var z-peckinx as integer.
def var z-peckmax as integer init 9.
def var z-searchlevel as integer no-undo.

def var z-peckinglist as character format "x(4)" extent 9 init
  ["dros",
   "dren",
   "dhou",
   "dmcp",
   "datl",
   "decp",
   "dscp",
   "dwcp",
   "dxcp"].


define var xzbuyer like poeh.buyer.
define var xzprodline like icsl.prodline no-undo.
define var xz-whse like icsw.whse no-undo.
define buffer xzvaeh for vaeh.
define buffer xzvaesl for vaesl.
define buffer xzicsl for icsl.
define buffer xzicsw for icsw.
define buffer xzicsw2 for icsw.
*/
assign xzbuyer = "". 
       z-searchlevel = 0.
find xzvaeh where xzvaeh.cono = g-cono
              and xzvaeh.vano = {2}
              and xzvaeh.vasuf = {3} no-lock no-error.

if avail xzvaeh then
  do:
  
  find  xzvaesl where xzvaesl.cono = g-cono
                      and xzvaesl.vano = {2}
                      and xzvaesl.vasuf = {3}
                      and xzvaesl.seqno = {4}
                      and xzvaesl.lineno = {5} no-lock no-error.
  if avail xzvaesl then
    do:
    
    find xzicsw where xzicsw.cono = g-cono and
                      xzicsw.whse = xzvaesl.whse and
                      xzicsw.prod = xzvaesl.shipprod no-lock no-error.
       
    if xzvaesl.arpvendno = 0 and avail xzicsw then
       xzvend = xzicsw.arpvendno.   
    else
       xzvend = xzvaesl.arpvendno.
    if xzvaesl.arpprodline = " " and avail xzicsw then
       xzprodline = xzicsw.prodline.   
    else
       xzprodline = xzvaesl.arpprodline. 
     
    
    find first xzicsl where xzicsl.cono     = g-cono
                         and xzicsl.whse     = xzvaesl.whse
                         and xzicsl.vendno   = xzvend
                         and xzicsl.prodline = xzprodline no-lock
                                                               no-error. 
    if avail xzicsl then
      do:
      assign xzbuyer = xzicsl.buyer
             xzprodline = xzicsl.prodline
             z-searchlevel = 1.
      end.
    else
      do:
      find first xzicsl where xzicsl.cono     = g-cono
                         and xzicsl.whse     = xzvaesl.whse
                         and xzicsl.vendno   = xzvend no-lock
                                                               no-error.
      if avail xzicsl then  
        assign xzbuyer = xzicsl.buyer
               xzprodline = xzicsl.prodline
               z-searchlevel = 2.
      else
        do:
        find first xzicsl where xzicsl.cono     = g-cono
                           and xzicsl.whse      = xzvaesl.whse
                           and xzicsl.prodline  = xzprodline no-lock
                                                               no-error.
        if avail xzicsl then  
          assign xzbuyer = xzicsl.buyer
                 xzprodline = xzicsl.prodline
                 z-searchlevel = 3.
        else 
          do:
          if avail xzicsw then
            do:
            if xzicsw.prodline = "" then
              if xzicsw.arptype = "w" then
                do:
                find xzicsw2 where xzicsw2.cono = g-cono and
                                   xzicsw2.whse = xzicsw.arpwhse and
                                   xzicsw2.prod = xzicsw.prod no-lock no-error.
                if avail xzicsw2 then                   
                  do:
                  assign xz-whse = xzicsw2.whse.
                  {p-zfindlinegne.i
                     xz-whse}.
                  end.
                end.
              if xzbuyer = "" then
                do z-peckinx = 1 to z-peckmax:
                  {p-zfindlinegne.i
                     z-peckinglist[z-peckinx]}.
                if xzbuyer <> "" then
                  do:
                  assign z-searchlevel = 7 + z-peckinx.
                  leave.
                  end.
               end.     
            end.
          end.
        end.     
      end.          
    end.  
  end.
if ({8} > z-searchlevel or ({8} = 0 and z-searchlevel <> 0)) and
     xzbuyer <> "" then
  assign {6} = xzbuyer
         {7} = xzprodline
         {8} = z-searchlevel.

