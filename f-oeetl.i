/* f-oeetl.i 1.1 01/03/98 */
/* f-oeetl.i 1.2 12/21/92 */
/*h****************************************************************************  INCLUDE      : f-oeetl.i
  DESCRIPTION  : Shared frame for oeetl
  USED ONCE?   : No
  AUTHOR       :
  DATE WRITTEN : 12/21/92
  CHANGES MADE :
    12/21/92 mms; TB#  8566 Added rushdesc to frame display
    12/12/95 kr;  TB# 19875 Remove Lookup Indicator on Price,Disc
    09/19/01 usb; TB# 10576 Add Price Origin Code - BMAT OE Usability Project
    06/10/09 das; NonReturn/NonCancelable
******************************************************************************/
/* used for zzsdi08.p - uncomment when compiling zzsdi08.p */
/* def var s-NR  as c format "x(1)"     no-undo.       */    
/* def var s-URL as c format "x*1)"     no-undo.       */    

form
  s-commentfl                 at  1 no-label
  s-lineno                    at  2    label "Ln#"
      {f-help.i}
  s-specnstype                at  6    label "N"    
  s-prod                      at  8    label "Product"
      {f-help.i}
  s-notesfl                   at 32 no-label
  s-qtyord                    at 33    label "Quantity "
  s-returnfl                  at 43 no-label
  s-unit                      at 45    label "Unit"
      {f-help.i}
  s-price                     at 50    label "Price    "
      {f-help.i}
  s-qtybreakty                at 63 no-label
  /* NonReturn */
  s-NR                        at 64    label "R"
  
  s-discamt                   at 65    label "Discount "
      {f-help.i}
  s-disctype                  at 78 no-label
  s-rushdesc                  at  1 no-label
  s-URL                       at  6 no-label    
  s-proddesc                  at  8 no-label
  s-speccost                  at 57 no-label
  s-netamt                    at 62 no-label
  s-taxablety                 at 76 no-label
  s-priceorigcd               at 77 no-label
with frame f-oeetl row 5 width 80 no-underline no-hide overlay scroll 1
     v-oelineno down no-label                          
              
