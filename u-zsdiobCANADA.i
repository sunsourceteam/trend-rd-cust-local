/*h*****************************************************************************  INCLUDE      : u-zsdiob.i
  DESCRIPTION  : List of fields to display in zsdiob
  USED ONCE?   : zsdiob
  AUTHOR       : tah
  DATE WRITTEN : 04/17/06
  CHANGES MADE :
*******************************************************************************/

/*   oeehb.currencyty */
   s-countrycd
   oeehb.notesfl
   oeehb.approvty
   oeehb.takenby
   "QU" @ oeehb.transtype /*   oeehb.transtype  */
   s-orderdisp
   arsc.custno
   arsc.notesfl
   s-stagecd
   oeehb.whse
   arsc.name
   arss.name when avail arss and arss.invtofl @ arsc.name
   oeehb.shiptonm
   arsc.addr
   arss.addr[1] when avail arss and arss.invtofl @ arsc.addr[1]
   arss.addr[2] when avail arss and arss.invtofl @ arsc.addr[2]
   oeehb.shiptoaddr
   arsc.city
   arss.city when avail arss and arss.invtofl @ arsc.city
   oeehb.shiptocity
   arsc.state
   arss.state when avail arss and arss.invtofl @ arsc.state
   oeehb.shiptost
   oeehb.shiptozip
   arsc.zipcd
   arss.zipcd when avail arss and arss.invtofl @ arsc.zipcd
   oeehb.shipinstr
   oeehb.orderno
   oeehb.reqshipdt
   oeehb.custpo
/*   oeehb.jrnlno2 */
   s-cancelcd
   s-shipviaty
   oeehb.refer  
   oeehb.jobno
   oeehb.promisedt
   oeehb.shipdt
   oeehb.billdt
   oeehb.canceldt
/*   oeehb.invoicedt */
/*   oeeh.paiddt     */
   s-termstype
   oeehb.slsrepin
   oeehb.slsrepout
   oeehb.placedby
/*   oeehb.actfreight   when sasoo.oepricefl <> "n" */
   s-addondesc       when sasoo.oepricefl <> "n"
   s-otheraddon      when sasoo.oepricefl <> "n" and
   s-otheraddon    <> 0
   ""                when s-otheraddon     = 0   @   s-otheraddon
   s-addon           when sasoo.oepricefl <> "n"
   ""                when s-addonamt[1]    = 0   @   s-addonamt[1]
   ""                when s-addonamt[2]    = 0   @   s-addonamt[2]
   ""                when s-addonamt[3]    = 0   @   s-addonamt[3]
   ""                when s-addonamt[4]    = 0   @   s-addonamt[4]
   k-addonamt[1]     when sasoo.oepricefl <> "n" and
                          s-addonamt[1]   <> 0
   k-addonamt[2]     when sasoo.oepricefl <> "n" and
                          s-addonamt[2]   <> 0
   k-addonamt[3]     when sasoo.oepricefl <> "n" and
                          s-addonamt[3]   <> 0
   k-addonamt[4]     when sasoo.oepricefl <> "n" and
                          s-addonamt[4]   <> 0
   k-wodiscamt    when sasoo.oepricefl <> "n" @ oeehb.wodiscamt 
   k-specdiscamt  when sasoo.oepricefl <> "n" @ oeehb.specdiscamt 
/*   oeehb.termsdiscamt when sasoo.oepricefl <> "n"
     oeehb.termspct     when sasoo.oepricefl <> "n" */
   k-salestax        when sasoo.oepricefl <> "n"  @ s-salestax  
   k-totlineord      when sasoo.oepricefl <> "n"  @ s-totlineord
   k-totlineamt      when sasoo.oepricefl <> "n"  @ s-totlineamt
   k-totinvamt       when sasoo.oepricefl <> "n"  @ s-totinvamt  
   k-payamt          when sasoo.oepricefl <> "n"  @ s-payamt 
   s-cod             when sasoo.oepricefl <> "n"
   s-finthru
   oeehb.enterdt

