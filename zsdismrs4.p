/* smrs4.p 1.2 07/01/98 */
/*******************************************************************************
  PROCEDURE            : smrs4     (format 4)   prod by cust
  DESCRIPTION          : sales manager sales analysis report
  AUTHOR               : mwb
  DATE LAST MODIFIED   : 01/25/90
  CHANGES MADE AND DATE:
    04/20/93 gwa tb 10715 - Use oeel.proddesc for Non-Stock records.....
    08/05/93 enp; TB# 5221 Add Full & Partial desc option suppressed prod
        & desc print on dups
    03/06/97 frb; TB# 7241  Special price costing
    06/10/98 lmk; TB# 20381 Need processing per warehouse
    05/13/99 sbr; TB#  5366 Would like notes at AR Ship To level
    04/21/03 lcb; TB# t14244 8Digit Ord No Project-removed notesfl
    10/22/05 bp ; TB# e23249 Add user hooks.
*******************************************************************************/

{zsdismrsx.z99 &top = "*"}

/* RANGE AND OPTION VARIABLES */
{g-all.i}
{g-secure.i}
{g-smrs.i}
def shared var t-transcnt      as integer initial 0 no-undo.
/*tb 7241 03/06/97 frb; Declare special cost variables */
{speccost.gva}

/*tb 5366 05/13/99 sbr; Added variable definition for v-shipto. */
def        var v-shipto        as c format "x(9)"   no-undo.

form /* banner title */
    v-flag1         at 1  no-label
    v-prod          colon 10 label " Product"
    s-proddescf     at 37 no-label
    v-flag2         at 1  no-label
    v-custno        colon 10 label "Customer"
    arsc.name       at 37 no-label
with frame f-smrs no-box side-labels page-top width 132.

form /* title line analysis method */
    "Whse"              at 1            "Cat"               at 6
    "Ship To"           at 15           "Sls"               at 24
    "Order #"           at 54           "Inv Date"          at 65
    "Quantity"          at 77           "Net Sale"          at 91
    "Disc %"            at 103          "Margin"            at 116
    "Margin %"          at 125 skip
with frame f-smrsta no-box no-labels page-top width 132.

/*tb 5366 05/13/99 sbr; Use "v-shipto" instead of "oeel.shipto" in frame. */
form  /* detail line , analysis */
    oeel.whse           at 1            oeel.prodcat        at 6
    v-shipto            at 15           v-slsrep            at 24
    oeel.orderno        at 54           "-"                 at 62
    oeel.ordersuf       at 63
    oeel.invoicedt      at 66
    s-quantity          at 74
    s-netsale           at 88           s-discpct           at 103
    s-margin            at 111          s-margpct           at 125
with frame f-smrsda no-box no-labels down width 132.

form  /* title for balance method  */
    "Whse"              at 1            "Cat"               at 6
    "Ship To"           at 15           "Sls"               at 24
    "Order #"           at 54           "Inv Date"          at 65
    v-label             at 78           "Line Disc"         at 93
    "Order Disc"        at 106          "Cost"              at 126
with frame f-smrstb no-box no-labels page-top width 132.

/*tb 5366 05/13/99 sbr; Use "v-shipto" instead of "oeel.shipto" in frame. */
form  /* detail line for  balance method  */
    oeel.whse           at 1            oeel.prodcat        at 6
    v-shipto            at 15           v-slsrep            at 24
    oeel.orderno        at 54           "-"                 at 62
    oeel.ordersuf       at 63
    oeel.invoicedt      at 66
    s-gross             at 77           s-linedisc          at 91
    s-orddisc           at 105          s-cost              at 119
with frame f-smrsdb no-box no-labels down width 132.

form  /*  total line per sub-totals - analysis  */
    t-sumcnt       colon 37 label "Total # Of Trans For The Customer"
    t-sumquan      at 73  no-label
    t-sumnet       at 87  no-label
    t-sumdspc      at 102 no-label
    t-summarg      at 110 no-label
    t-summgpc      at 124 no-label
with frame f-smrsprodtota no-box side-labels width 132.

form  /*  total line per main break totals - analysis  */
    t-totcnt        colon 37 label "Total # Of Trans For The Product"
    t-totquan       at 73  no-label
    t-totnet        at 87  no-label
    t-totdspc       at 102 no-label
    t-totmarg       at 110 no-label
    t-totmgpc       at 124 no-label
with frame f-smrscattota no-box side-labels width 132.

form  /*  total line per sub-totals - balance  */
    t-sumcnt       colon 37 label "Total # Of Trans For The Customer"
    t-sumgross     at 76  no-label
    t-sumline      at 90  no-label
    t-sumord       at 104 no-label
    t-sumcost      at 118 no-label
with frame f-smrsprodtotb no-box side-labels width 132.

form  /*  total line per main break totals - balance  */
    t-totcnt        colon 37 label "Total # Of Trans For The Product"
    t-totgross      at 76  no-label
    t-totline       at 90  no-label
    t-totord        at 104 no-label
    t-totcost       at 118 no-label
with frame f-smrscattotb no-box side-labels width 132.

form /* banner title for summary  */
    v-prod              colon 8  label "Product"
    s-proddescf         at 37 no-label
with frame f-smrssumb no-box side-labels page-top width 132.

form  /* title for  summary , analysis  */
    "Customer #"        at 3            "Name"              at 17
    "# Of Recs"         at 64
    "Quantity"          at 77           "Net Sale"          at 91
    "Disc %"            at 103          "Margin"            at 116
    "Margin %"          at 125
with frame f-smrssumta no-box no-labels page-top width 132.

form  /* detail line for summary, analysis */
    v-custno           at 3            arsc.name          at 17
    t-sumcnt           at 65
    t-sumquan          at 73           t-sumnet           at 87
    t-sumdspc          at 102          t-summarg          at 110
    t-summgpc          at 124
with frame f-smrssumda no-box no-labels down width 132.

form /* title line for  summary , balance  */
    "Customer #"        at 3            "Name"              at 17
    "# Of Recs"         at 64
    v-label             at 78           "Line Disc"         at 93
    "Order Disc"        at 106          "Cost"              at 126
with frame f-smrssumtb no-box no-labels page-top width 132.

form  /* detail line for summary , balance  */
    v-custno           at 3             arsc.name          at 17
    t-sumcnt           at 65
    t-sumgross         at 76            t-sumline          at 90
    t-sumord           at 104           t-sumcost          at 118
with frame f-smrssumdb no-box no-labels down width 132.

form  /* title for total, analysis  */
    "Product"           at 3            "Description"       at 30
    "# Of Recs"         at 64
    "Quantity"          at 77           "Net Sale"          at 91
    "Disc %"            at 103          "Margin"            at 116
    "Margin %"          at 125
with frame f-smrstotta no-box no-labels page-top width 132.

form  /* detail line for total, analysis  */
    v-prod              at 3            s-proddesc          at 30
    t-totcnt            at 65
    t-totquan           at 73           t-totnet            at 87
    t-totdspc           at 102          t-totmarg           at 110
    t-totmgpc           at 124
with frame f-smrstotda no-box no-labels down width 132.

form /* title for total , balance  */
    "Product"           at 3            "Description"       at 30
    "# Of Recs"         at 64
    v-label             at 78           "Line Disc"         at 93
    "Order Disc"        at 106          "Cost"              at 126
with frame f-smrstottb no-box no-labels page-top width 132.

form /* detail for total , balance  */
    v-prod              at 3            s-proddesc          at 30
    t-totcnt            at 65
    t-totgross          at 76           t-totline           at 90
    t-totord            at 104          t-totcost           at 118
with frame f-smrstotdb no-box no-labels down width 132.

{f-smrsxa.i} /*  grand total title line for analysis   */
{f-smrsga.i}  /*  grand total detail line for analysis  */
{f-smrsxb.i} /*  grand totle title line for balance    */
{f-smrsgb.i}  /*  grand total detail line for balance   */
{f-smrsa.i}    /*  total break line for analysis ------- */
{f-smrsb.i}    /*  total break line for balance -------- */
{f-smrspd.i 7} /*  prod descrip                          */
{f-under.i}
{f-blank.i}

{zsdismrsx.z99 &aft_form = "*"}

/* BEGIN REPORT LOGIC */
if p-printdet = "t" then do:
    if p-printbal = "a" then display with frame f-smrstotta.
    else if p-printbal = "b" then display v-label with frame f-smrstottb.
    display with frame f-under.
end.

{zsdismrsx.z99 &user_bef_foreach = "*"}

/* 20381 06/10/98 lmk; Add for each icsd to select by whse */
for each {p-smrs.i}
    break by oeel.cono   by oeel.shipprod
          by oeel.custno by oeel.whse by oeel.prodcat
         {zsdismrsx.z99 &user_breakby = "*"}:

    {zsdismrsx.z99 &top_foreach = '*'}

    {zsdismrsx.z99 &user_befdisp = '*'}

    if first-of(oeel.custno) or first-of(oeel.shipprod) then do:
        if first-of(oeel.custno) then do:
            assign v-flag2   = "*"
                   v-custno  = ""
                   v-custno  = string(oeel.custno) + arsc.notesfl.
        end.
        if first-of(oeel.shipprod) then do:
            {w-icsp.i oeel.shipprod no-lock}
            assign v-flag1    = "*" {p-smrspd.i}.
            if p-printdet = "s" then do:
                display v-prod s-proddescf with frame f-smrssumb.
                if p-printbal = "a" then display with frame f-smrssumta.
                else if p-printbal = "b" then display v-label
                    with frame f-smrssumtb.
                display with frame f-under.
            end.
        end.
        if p-printdet = "d" then do:
            display v-flag1 v-custno arsc.name v-flag2 v-prod s-proddescf
                    with frame f-smrs.
            if p-printbal = "a" then display with frame f-smrsta.
            else if p-printbal = "b" then display v-label with frame f-smrstb.
            display with frame f-under.
        end.
    end.
    if p-printbal = "a" then do:
        {p-smrsa.i}  /* calculates margin,margpct,quantity,netsale,discpct for
                            analysis method  */
        assign v-slsrep   = if p-printsls = "i" then oeel.slsrepin
                            else if p-printsls = "o" then oeel.slsrepout
                            else "".

        /*tb 5366 05/13/99 sbr; Added w-arss.i include to find arss record. */
        {w-arss.i oeel.custno oeel.shipto no-lock}

        /*tb 5366 05/13/99 sbr; Added code to set v-shipto. */
        v-shipto = oeel.shipto + if avail arss then arss.notesfl else "".

        if p-printdet = "d" then do:

            /*tb 5366 05/13/99 sbr; Display v-shipto instead of oeel.shipto. */
            display oeel.whse       when first-of(oeel.whse)
                    oeel.prodcat    when first-of(oeel.prodcat)
                    v-shipto
                    v-slsrep        oeel.orderno    oeel.ordersuf
                    oeel.invoicedt  s-netsale
                    s-discpct       with frame f-smrsda.
            down with frame f-smrsda.
        end.
        assign s-quantity = 0 s-netsale = 0 s-discpct = 0 s-margin = 0
               s-margpct = 0 s-cost = 0 s-discnum = 0 s-discden = 0.
    end.
    if p-printbal = "b" then do:
        {p-smrsb.i}    /* calculates gross,linedesc,orddesc,cost for balance  */
        v-slsrep    = if p-printsls = "i" then oeel.slsrepin
                      else if p-printsls = "o" then oeel.slsrepout
                      else "".

        /*tb 5366 05/13/99 sbr; Added w-arss.i include to find arss record. */
        {w-arss.i oeel.custno oeel.shipto no-lock}

        /*tb 5366 05/13/99 sbr; Added code to set v-shipto. */
        v-shipto = oeel.shipto + if avail arss then arss.notesfl else "".

        if p-printdet = "d" then do:

            /*tb 5366 05/13/99 sbr; Display v-shipto instead of oeel.shipto. */
            display oeel.whse       when first-of(oeel.whse)
                    oeel.prodcat    when first-of(oeel.prodcat)
                    v-shipto
                    v-slsrep        oeel.orderno    oeel.ordersuf
                    oeel.invoicedt  s-gross         s-linedisc
                    s-orddisc
                    with frame f-smrsdb.
            if g-seecostfl then display s-cost with frame f-smrsdb.
            down with frame f-smrsdb.
        end.
        assign s-gross = 0  s-linedisc = 0 s-orddisc = 0 s-cost = 0.
    end.
    if last-of(oeel.custno) or last-of(oeel.shipprod) then do:
        hide frame f-smrs.
        hide frame f-smrsta.
        hide frame f-smrstb.
        if last-of(oeel.shipprod) and p-printdet = "s" then do:
            hide frame f-smrssumb.
            hide frame f-smrssumta.
            hide frame f-smrssumtb.
            hide frame f-under.
        end.
        if not can-do("t,s",p-printdet) then hide frame f-under.
        if last-of(oeel.custno) then do:
            if p-printbal = "a" then do:
                {p-smrs1.i}  /* det total margin and disc % */
                if p-printdet = "d" then do:
                    display with frame f-smrsa.
                    if g-seecostfl then
                        display t-sumcnt t-sumquan t-sumnet t-sumdspc
                                t-summarg t-summgpc with frame f-smrsprodtota.
                    else display t-sumcnt t-sumquan t-sumnet t-sumdspc
                        with frame f-smrsprodtota.
                end.
                if p-printdet = "s" then do:
                    if g-seecostfl then
                        display t-summarg t-summgpc with frame f-smrssumda.
                    display v-custno arsc.name t-sumcnt t-sumquan t-sumnet
                            t-sumdspc with frame f-smrssumda.
                    down with frame f-smrssumda.
                end.
                assign t-totcnt   = t-totcnt  + t-sumcnt
                       t-totquan  = t-totquan + t-sumquan
                       t-totnet   = t-totnet  + t-sumnet
                       t-totmarg  = t-totmarg + t-summarg
                       t-totdiscden = t-totdiscden + t-discden
                       t-totdiscnum = t-totdiscnum + t-discnum
                       t-totcost   = t-totcost   + t-sumcost
                       t-sumcnt  = 0        t-sumquan = 0

                       t-sumnet  = 0        t-sumdspc = 0
                       t-summarg = 0        t-summgpc = 0
                       t-sumcost = 0        t-discnum  = 0      t-discden = 0.
            end.
            else do:
                if p-printdet = "d" then do:
                    display with frame f-smrsb.
                    if g-seecostfl then
                        display t-sumcnt t-sumgross t-sumline t-sumord
                                t-sumcost with frame f-smrsprodtotb.
                    else
                        display t-sumcnt t-sumgross t-sumline t-sumord
                            with frame f-smrsprodtotb.
                end.
                if p-printdet = "s" then do:
                    display v-custno   arsc.name t-sumcnt
                            t-sumgross t-sumline     t-sumord
                            with frame f-smrssumdb.
                    if g-seecostfl then display t-sumcost
                        with frame f-smrssumdb.
                    down with frame f-smrssumdb.
                end.
                assign t-totcnt   = t-totcnt   + t-sumcnt
                       t-totgross = t-totgross + t-sumgross
                       t-totline  = t-totline  + t-sumline
                       t-totord   = t-totord   + t-sumord
                       t-totcost  = t-totcost  + t-sumcost
                       t-sumcnt   = 0        t-sumgross = 0
                       t-sumline  = 0        t-sumord   = 0
                       t-sumcost  = 0.
            end.
        end.
        if last-of(oeel.shipprod) then do:
            if p-printbal = "a" then do:
                {p-smrs2.i}    /* totals margin and disc % */
                if p-printdet = "d" or p-printdet = "s" then do:
                    if p-printdet = "s" then display with frame f-smrsa.
                    if g-seecostfl then
                        display t-totcnt t-totquan t-totnet t-totdspc t-totmarg
                            t-totmgpc with frame f-smrscattota.
                    else display t-totcnt t-totquan t-totnet t-totdspc
                        with frame f-smrscattota.
                    if p-printpage then page.
                    if p-printdet = "s" and not p-printpage then do:
                        display skip with frame f-blank.
                        down with frame f-blank.
                    end.
                end.
                if p-printdet = "t" then do:
                    if g-seecostfl then display t-totmarg t-totmgpc
                        with frame f-smrstotda.
                    display v-prod       s-proddesc  t-totcnt  t-totquan
                            t-totnet     t-totdspc with frame f-smrstotda.
                    down with frame f-smrstotda.
                    if p-descripty = "f" then
                        display s-proddescf with frame f-smrspd.
                end.
                assign t-grandcnt =  t-grandcnt + t-totcnt
                       t-grandquan = t-grandquan + t-totquan
                       t-grandnet  = t-grandnet  + t-totnet
                       t-grandmarg = t-grandmarg + t-totmarg
                       t-grdiscden = t-grdiscden + t-totdiscden
                       t-grdiscnum = t-grdiscnum + t-totdiscnum
                       t-grandcost = t-grandcost + t-totcost
                       t-totcnt    = 0          t-totquan = 0
                       t-totnet    = 0          t-totdspc = 0
                       t-totmarg   = 0          t-totmgpc = 0
                       t-totcost   = 0    t-totdiscden = 0     t-totdiscnum = 0.
            end.
            else do:
                if p-printdet = "d" or p-printdet = "s" then do:
                    if p-printdet = "s" then display with frame f-smrsb.
                    if g-seecostfl then
                        display t-totcnt t-totgross t-totline t-totord t-totcost
                            with frame f-smrscattotb.
                    else display t-totcnt t-totgross t-totline t-totord
                        with frame f-smrscattotb.
                    if p-printpage then page.
                    if p-printdet = "s" and not p-printpage then do:
                        display skip with frame f-blank.
                        down with frame f-blank.
                    end.
                end.
                if p-printdet = "t" then do:
                    display v-prod    s-proddesc t-totcnt t-totgross
                            t-totline t-totord with frame f-smrstotdb.
                    if g-seecostfl then display t-totcost
                        with frame f-smrstotdb.
                    down with frame f-smrstotdb.
                    if p-descripty = "f" then
                        display s-proddescf with frame f-smrspd.
                end.
                assign t-grandgro  = t-grandgro  + t-totgross
                       t-grandcnt  = t-grandcnt  + t-totcnt
                       t-grandline = t-grandline + t-totline
                       t-grandord  = t-grandord  + t-totord
                       t-grandcost = t-grandcost + t-totcost
                       t-totcnt    = 0          t-totgross = 0
                       t-totline   = 0          t-totord   = 0
                       t-totcost   = 0.
            end.
        end.
        if p-printdet = "d" and not p-printpage then do:
            display skip with frame f-blank.
            down with frame f-blank.
        end.
    end.
    assign s-oecost = 0  s-netamt = 0  s-margin = 0  s-margpct = 0
           v-slsrep = "" v-flag1  = "" v-flag2   = "".
    t-transcnt = t-transcnt + 1.
end. /*  end for each  */

hide frame f-smrstotta.
hide frame f-smrstottb.
hide frame f-under.

/******************** grand totals  *********************************/
{p-smrsgr.i}