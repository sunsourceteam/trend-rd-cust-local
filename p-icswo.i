/* SX 55 */
/*h****************************************************************************
  INCLUDE      : p-icswo.i
  DESCRIPTION  : Local processing for ICSWO
  USED ONCE?   : yes
  AUTHOR       : NxT
  DATE WRITTEN : 02/16/92
  CHANGES MADE :
    09/10/99 cm;  TB# 13389 Add WT Transfer Unit
******************************************************************************/
assign confirm = true.
if input icsw.unitbuy <> o-unitbuy or
   (input icsw.unitbuy <> icsw.unitbuy and
    icsw.unitbuy <> "" ) then do:

/* SX 55 */
   if input icsw.unitbuy <> icsw.unitbuy then do:
      find sasos use-index k-sasos where sasos.cono     = g-cono and
                                         sasos.oper2    = g-operinit and
                                         sasos.menuproc = "zxt1"
                                         no-lock no-error.
      if avail sasos then do:
          if sasos.securcd[2] < 3 then do:
             run err.p(1904).
             next-prompt icsw.unitbuy with frame f-icswo.
             next.
          end.
      end.
   end.
end.
/* SX 55 */

if input icsw.unitbuy <> o-unitbuy then do:
  if input icsw.unitbuy ne "" then do:
    assign v-unitbuy = input icsw.unitbuy.
    {p-unit.i &prod       = "g-prod"
              &unitconvfl = "icsp.unitconvfl"
              &unitstock  = "icsp.unitstock"
              &unit       = "v-unitbuy"
              &desc       = "v-desc"
              &conv       = "v-conv"
              &confirm    = "confirm"}.
    display v-desc @ s-unitbuydesc v-conv @ s-unitbuyconv
      with frame f-icswo.
    if not confirm and g-secure > 2 then do:
      run err.p(4026).
      next-prompt icsw.unitbuy with frame f-icswo.
      next.
    end.
    else if g-secure > 2 then do:
      hide message no-pause.
    end.
  end.
  else do:
    display "" @ s-unitbuydesc 0 @ s-unitbuyconv with frame f-icswo.
    o-unitbuy = "".
  end.
  o-unitbuy = input icsw.unitbuy.
end.                                 /* unit buy description display */

/*tb 13389 09/10/99 cm; Add WT Transfer Unit */
if input icsw.unitwt <> o-unitwt then do:
   /* SX55  */
   find sasos use-index k-sasos where sasos.cono     = g-cono and
                                      sasos.oper2    = g-operinit and
                                      sasos.menuproc = "zxt1"
                                      no-lock no-error.
   if avail sasos then do:
      if sasos.securcd[2] < 3 then do:
         run err.p(1904).
         next-prompt icsw.unitwt with frame f-icswo.
         next.
      end.
    end.
    /* SX 55 */
 
    if input icsw.unitwt <> "" then do:
        assign v-unitwt = input icsw.unitwt.
        {p-unit.i
            &prod       = "g-prod"
            &unitconvfl = "icsp.unitconvfl"
            &unitstock  = "icsp.unitstock"
            &unit       = "v-unitwt"
            &desc       = "v-desc"
            &conv       = "v-conv"
            &confirm    = "confirm"}.
        display
            v-desc @ s-unitwtdesc
            v-conv @ s-unitwtconv
        with frame f-icswo.

        if not confirm and g-secure > 2 then do:
            run err.p(4026).
            next-prompt icsw.unitwt with frame f-icswo.
            next.
        end.
        else if g-secure > 2 then do:
            hide message no-pause.
        end.
    end.
    else do:
        display "" @ s-unitwtdesc 0 @ s-unitwtconv with frame f-icswo.
        o-unitwt = "".
    end.
    o-unitwt = input icsw.unitwt.
end.                                 /* unit wt description display */

if confirm or g-secure = 2 then do:
  if input icsw.unitstnd <> o-unitstnd then do:
    assign v-unitstnd = input icsw.unitstnd.
    if input icsw.unitstnd ne "" then do:
      {p-unit.i &prod       = "g-prod"
                &unitconvfl = "icsp.unitconvfl"
                &unitstock  = "icsp.unitstock"
                &unit       = "v-unitstnd"
                &desc       = "v-desc"
                &conv       = "v-conv"
                &confirm    = "confirm"}.
      display v-desc @ s-unitstnddesc v-conv @ s-unitstndconv
         with frame f-icswo.
      if not confirm and g-secure > 2 then do:
        run err.p(4026).
        next-prompt icsw.unitstnd with frame f-icswo.
        next.
      end.
      else if g-secure > 2 then do:
        hide message no-pause.
      end.
    end.
    else do:
      display "" @ s-unitstnddesc 0 @ s-unitstndconv with frame f-icswo.
      o-unitstnd = "".
    end.
    o-unitstnd = input icsw.unitstnd.
  end.
end.                                 /* unit stnd description display */
if input icsw.overreasin <> o-overreasin then do:
    o-overreasin = input icsw.overreasin.
    {w-sasta.i "o" "input icsw.overreasin" no-lock}
    if avail sasta then
      display sasta.descrip @ s-overreasindesc with frame f-icswo.
    else if input icsw.overreasin = "" then 
       display "" @ s-overreasindesc  with frame f-icswo.
    else display v-invalid @ s-overreasindesc with frame f-icswo.
end.                                 /* usage override reason in */
if input icsw.overreasout <> o-overreasout then do:
    o-overreasout = input icsw.overreasout.
    {w-sasta.i "o" "input icsw.overreasout" no-lock}
    if avail sasta then
      display sasta.descrip @ s-overreasoutdesc with frame f-icswo.
    else if input icsw.overreasout = "" then 
       display "" @ s-overreasoutdesc with frame f-icswo.
    else display v-invalid @ s-overreasoutdesc with frame f-icswo.
end.                                 /* usage override reason in */
if input icsw.frozentype <> o-frozentype then do:
    o-frozentype = input icsw.frozentype.
    {w-sasta.i "f" "input icsw.frozentype" no-lock}
    if avail sasta then
      display sasta.descrip @ s-frozendesc with frame f-icswo.
    else if input icsw.frozentype = "" then 
       display "" @ s-frozendesc with frame f-icswo.
    else display v-invalid @ s-frozendesc with frame f-icswo.
end.                                 /* frozen reason display */
if icsw.frozenmmyy entered or icsw.frozentype entered or icsw.frozenmos entered
    then assign icsw.frozenbyty = yes.

if confirm then apply lastkey.
