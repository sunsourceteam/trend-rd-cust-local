def var b-vendortype        as c    format "x(3)"          no-undo.
def var e-vendortype        as c    format "x(3)"          no-undo.
def var b-vendor            as de   format "9999999999.99" no-undo.
def var e-vendor            as de   format "9999999999.99" no-undo.
def var o-jrnlno            as i    format "99999999"      no-undo. 
def var o-company           as c    format "x(2)"          no-undo.
def var errmsg              as c    format "x(70)"         no-undo.

/*TRANSMISSION SIGN-ON RECORD*/
/******
def var ts_dir           as c    format "x(5)"           initial "$$ADD".
def var ts_space         as c    format "x(1)"           initial "". 
def var ts_id            as c    format "x(3)"           initial "ID=".
def var ts_rem_id        as c    format "x(8)"           initial "STOUSRF1".
def var ts_space2        as c    format "x(1)"           initial "".
def var ts_bid           as c    format "x(4)"           initial "BID=".
def var ts_batch_id      as c    format "x(26)"          initial
                                                 "'NWFACH12238275'".
def var ts_filler        as c    format "x(32)"          initial "".
******/

/* FILE HEADER RECORD */
def var h-record_type       as i    format "9"        initial 1.
def var h-priority_code     as i    format "99"       initial 01.
def var h-immed_dest        as c    format "x(10)"    initial " 021000021".
def var h-immed_origin      as c    format "x(10)"    initial "1416549000".
def var h-trans_date        as c    format "x(6)".    /* Must be YYMMDD */
def var h-trans_time        as c    format "x(4)"     initial "".
def var h-file_id_mod       as c    format "x"        initial "A".
def var h-record_size       as i    format "999"      initial 094.
def var h-blocking_factor   as i    format "99"       initial 10.
def var h-format_code       as i    format "9"        initial 1.
def var h-immed_dest_name   as c    format "x(23)"    
                                      initial "JPMORGAN CHASE         ".
def var h-immed_origin_name as c    format "x(23)".    
                                     /* initial "SUNSOURCE              ".  */
                                      
def var h-reference_code    as c    format "x(8)"     initial "".

/* COMPANY HEADER RECORD */
def var ch-record_type      as i    format "9"           initial 5.
def var ch-service_class-cd as i    format "999"         initial 200.
def var ch-co_name          as c    format "x(16)"       
                                      initial "SUNSOURCE      ".    
def var ch-co_dis_name      as c    format "x(20)"       initial "".

/* not that we have Multiple Companies, we must assign ch-co_id */
def var ch-co_id            as i    format "9999999999". /*initial 1416549000*/
def var ch-stnd_entry_class as c    format "x(3)"        initial "PPD".
def var ch-co_entry_disc    as c    format "x(10)"       initial "   EXPENSE".
def var ch-co_desc_date     as c    format "x(6)"        initial "".
def var ch-eff_date         as c    format "x(6)".       /* Must be YYMMDD */
def var ch-settlement_date  as c    format "x(3)"        initial "".
def var ch-orig_stat_cd     as c    format "x"           initial "1".
def var ch-orig_dfi_id      as i    format "99999999". /* initial 07100001. */
def var ch-batch_nbr        as i    format "9999999"     initial 00000001.

/* ENTRY DETAIL RECORD */
def var d-record_type       as i    format "9"        initial 6.
def var d-transaction_cd    as i    format "99". 
def var d-receiving_dfi_id  as i    format "99999999".
def var d-check_digit       as i    format "9".
def var d-dfi_account_nbr   as c    format "x(17)".
def var d-amount            as i    format "9999999999".
def var d-individual_id_nbr as c    format "x(15)"    initial "".
def var d-employee_name     as c    format "x(22)".
def var d-descret_data      as c    format "x(2)"     initial "".
def var d-addenda_rec_ind   as i    format "9"        initial 0.
def var d-constant          as i    format "99999999" initial 07100001.
def var d-trace_number      as i    format "9999999".

/* COMPANY CONTROL RECORD */
def var cc-record_type      as i    format "9"            initial 8.
def var cc-service_cl_cd    as i    format "999"          initial 200.
def var cc-entry_add_cnt    as i    format "999999".
def var cc-entry_hash       as i    format "9999999999".
/*def var cc-entry_hash       as de   format "9999999999".*/
def var cc-tot_debit_amount as i    format "999999999999" initial "0".
def var cc-tot_cred_amount  as i    format "999999999999". 

/* now that we Multiple Companies, we must assign cc-company_id */
def var cc-company_id       as c    format "9999999999". /*initial 1416549000*/
def var cc-auth_code        as c    format "x(19)"        initial "".
def var cc-reserved         as c    format "x(6)"         initial "".
def var cc-orig_dfi_id      as i    format "99999999"     initial 02100002. 
def var cc-batch_nbr        as i    format "9999999"      initial 0000001.

/* FILE CONTROL RECORD */
def var fc-record_type      as i    format "9"            initial 9.
def var fc-batch_cnt        as i    format "999999"       initial 000001.
def var fc-block_cnt        as i    format "999999"       initial 000001.
def var fc-entry_add_cnt    as i    format "99999999".
def var fc-entry_hash       as i    format "9999999999".
def var fc-tot_file_debit   as i    format "999999999999" initial "0".
def var fc-tot_file_cred    as i    format "999999999999".
def var fc-reserved         as c    format "x(39)"        initial "".

/* WORK FIELDS */
def var detail-cnt          as i    format "9999999".
def var tot-bal-amt         as i    format "9999999999".
def var check-digit         as c    format "x".
def var receiving-dfi-id    as c    format "x(8)".
def var entry-hash          as i    format "999999999999999".
/*def var entry-hash          as de   format "999999999999999".*/
def var hash-amt            as c    format "x(15)".
def var trunc-hash          as c    format "x(10)".
def var trans-date          as date format "99/99/99".
def var work-date           as c    format "x(8)".
def var disp-date           as c    format "x(6)".
def var disp-date2          as c    format "x(6)".
def var x-today             as date format "99/99/9999".
def var x-time              as c    format "x(5)".
def var x-page              as i    format ">>>9".
def var x-line              as i    format "999".
def var x-page-size         as i    format "99"           initial 50.
def var x-company           as i    format ">>>9"         initial 1.
def var s-grand-amount      as de   format "99999999.99".


/*FOR ZIDC RECORDS
def var z_trans_date   like h-trans_date.
def var z_file_id_mod  like h-file_id_mod.*/

{p-rptbeg.i}

Define frame f-header
    skip
    "Date:" at 1 
    x-today format "99/99/99"  at 7 
    "at "      at   16
    x-time     at   19
    "~015"
    "Company:" at 1
    x-company   at 10
    "Operator:" at 100
    sapb.operinit at 110
    "Page:" at 121
    x-page at 127 format ">>>9" 
    "~015"  at 130 
    "Function: apze" at 1 
    "Employee Expense Direct Deposit" at 72
    skip
    skip
   "Employee Vendor#" at 1
   "Employee Name"    at 29
   "Amount"           at 76
   skip
   "----------------" at 1 
   "------------------------------" at 19
   "-----------"     at 72

with down stream-io width 132 no-labels no-box no-underline.

Define frame f-detail
   apsv.vendno        at 4
   d-employee_name    at 19
   apet.amount        at 72 format ">>>>>>>9.99"
with down stream-io width 132 no-labels no-box no-underline.
 
Define frame f-total 
   skip
   skip
   "-----------"     at 72
   "Total Amount:"   at 58
   s-grand-amount    at 72 format ">>>>>>>9.99"
with down stream-io width 132 no-labels no-box no-underline. 


define stream deposit.
define var o_file           as char format "x(30)".
define var o_doctype        as char format "x".

assign o_file = "/usr/tmp/bank/ACHexp1".

find zidc use-index docinx3 where zidc.cono  = 1 and
                             zidc.docttype   = string(TODAY,"99/99/99") and
                             zidc.doctype    = "ACH"
                             no-error.
               
if avail zidc then
  do:
  if zidc.docdisp = "A" then
    assign o_doctype = "B".
  if zidc.docdisp = "B" then
    assign o_doctype = "C".
  if zidc.docdisp = "C" then
    assign o_doctype = "D".
  if zidc.docdisp = "D" then
    assign o_doctype = "E".
  if zidc.docdisp = "E" then
    assign o_doctype = "F".
  if zidc.docdisp = "F" then
    assign o_doctype = "G".
  if zidc.docdisp = "G" then
    assign o_doctype = "H".
  if zidc.docdisp = "H" then         
    assign o_doctype = "I".
  if zidc.docdisp = "I" then
    assign o_doctype = "J".
  if zidc.docdisp = "J" then
    assign o_doctype = "K".
  if zidc.docdisp = "K" then
    assign o_doctype = "L".
  if zidc.docdisp = "L" then
    assign o_doctype = "M".
  if zidc.docdisp = "M" then
    assign o_doctype = "N".
  if zidc.docdisp = "N" then
    assign o_doctype = "O".
  if zidc.docdisp = "O" then
    assign o_doctype = "P".
  if zidc.docdisp = "P" then
    assign o_doctype = "Q".
  if zidc.docdisp = "Q" then
    assign o_doctype = "R".
  if zidc.docdisp = "R" then
    assign o_doctype = "S".
  if zidc.docdisp = "S" then
    assign o_doctype = "T".
  if zidc.docdisp = "T" then
    assign o_doctype = "U".
  if zidc.docdisp = "U" then
    assign o_doctype = "V".
  if zidc.docdisp = "V" then
    assign o_doctype = "W".
  if zidc.docdisp = "W" then
    assign o_doctype = "X".
  if zidc.docdisp = "X" then
    assign o_doctype = "Y".
  if zidc.docdisp = "Y" then
    assign o_doctype = "Z".
  assign zidc.docdisp = o_doctype.
end. /* if avail zidc */
else
  do:
  assign o_doctype = "A".
  create zidc.
  assign zidc.cono      = 1.
  assign zidc.docttype  = string(TODAY,"99/99/99").
  assign zidc.doctype  = "ACH".
  assign zidc.docdisp   = o_doctype.
  assign zidc.docdate   = TODAY.
end. 

define variable destfilename as char        format "x(40)".
define variable outputfilename as char      format "x(40)".
define variable destfilecounter as char     format "x(40)".

output stream deposit to value(o_file + o_doctype + ".txt").

define variable flsuff  as character.
define variable flsuff2 as character.


substring(flsuff,1,2,"character") =
  substring(string(today),1,2,"character").
substring(flsuff,3,2,"character") = 
  substring(string(today),4,2,"character").

substring (flsuff,5,1,"character") = ".".

substring(flsuff,6,2,"character") =
  substring(string(time,"HH:MM:SS"),1,2,"character").
substring(flsuff,8,2,"character") = 
  substring(string(time,"HH:MM:SS"),4,2,"character").
substring(flsuff,10,2,"character") = 
  substring(string(time,"HH:MM:SS"),7,2,"character").


substring(destfilename,1,16,"character") = "/usr/tmp/ACHback".
substring(destfilename,17,11,"character") = flsuff.
substring(destfilecounter,1,19,"character") = "/usr/tmp/ACHcounter".

/* put date into YYMMDD format */
assign x-today    = TODAY.
assign x-time     = string(time,"HH:MM").

assign trans-date = TODAY.
assign work-date  = string(trans-date).
assign substring(disp-date,1,2) = substring(work-date,7,2).
assign substring(disp-date,3,2) = substring(work-date,1,2).
assign substring(disp-date,5,2) = substring(work-date,4,2).

assign work-date  = string(trans-date + 1).
assign substring(disp-date2,1,2) = substring(work-date,7,2).
assign substring(disp-date2,3,2) = substring(work-date,1,2).
assign substring(disp-date2,5,2) = substring(work-date,4,2).


assign x-page = 1.
assign x-line = 4.
display 
   x-today     x-time     x-company     sapb.operinit     x-page
 with frame f-header.   
   
/* {p-rptbeg.i} */
/* 100 */
assign b-vendortype = sapb.rangebeg[1].
assign e-vendortype = sapb.rangeend[1].

assign b-vendor  = decimal(sapb.rangebeg[2]).
if sapb.rangeend[2] = "" then
   assign e-vendor = 9999999999.99.
else
   assign e-vendor  = decimal(sapb.rangeend[2]).
   
assign o-jrnlno  = integer(sapb.optvalue[1]).    
assign o-company = sapb.optvalue[2].

errmsg = "".
if b-vendortype ne "edc" and b-vendortype ne "eds" and
   b-vendortype ne "sdc" and b-vendortype ne "sds" and
   b-vendortype ne "ddc" and b-vendortype ne "dds" and
   b-vendortype ne "xdc" and
   b-vendortype ne "adc" and b-vendortype ne "udc" and
   b-vendortype ne "ldc" and b-vendortype ne "lds" and
   b-vendortype ne "pdc"
   then
   assign errmsg = 
   "Vendor Type must be Employee Direct Deposit Type of EDC/S, SDC/S, DDC/S, UDC, ADC, XDC, LDC/S or PDC".
if e-vendortype ne "edc" and e-vendortype ne "eds" and
   e-vendortype ne "sdc" and e-vendortype ne "sds" and
   e-vendortype ne "ddc" and e-vendortype ne "dds" and
   e-vendortype ne "xdc" and
   e-vendortype ne "adc" and e-vendortype ne "udc" and
   e-vendortype ne "ldc" and e-vendortype ne "lds" and
   e-vendortype ne "pdc"
   then
   assign errmsg = 
   "Vendor Type must be Employee Direct Deposit Type of EDC/S, SDC/S, DDC/S, UDC, XDC, ADC, LDC/S or PDC".


if o-jrnlno = 0 then
   assign errmsg = 
   "Journal Number is required".
   
if errmsg ne "" then do:
   display errmsg.
end.
else do:

/******
/*Create TRANSMISSION SIGN-ON RECORD*/
put stream deposit UNFORMATTED
        ts_dir               format "x(5)"           
        ts_space             format "x(1)"            
        ts_id                format "x(3)"           
        ts_rem_id            format "x(8)"           
        ts_space2            format "x(1)"           
        ts_bid               format "x(4)"           
        ts_batch_id          format "x(26)"          
        ts_filler            format "x(32)"         
        skip.
*******/


/* Create FILE HEADER RECORD */

find zidc use-index docinx3 where zidc.cono      = 1 and
                                  zidc.docttype  = string(TODAY,"99/99/99") and
                                  zidc.doctype   = "ACH"
                                  no-error.
                        
   if avail zidc then 
    do:
       assign h-trans_date = disp-date.
       assign substring(h-trans_time,1,2) = substring(x-time,1,2).
       assign substring(h-trans_time,3,2) = substring(x-time,4,2).
        
        if zidc.docdisp = "A" then
           assign h-file_id_mod = "B".
        if zidc.docdisp = "B" then
           assign h-file_id_mod = "C".
        if zidc.docdisp = "C" then
           assign h-file_id_mod = "D".
        if zidc.docdisp = "D" then
           assign h-file_id_mod = "E".
         if zidc.docdisp = "E" then
           assign h-file_id_mod = "F".
         if zidc.docdisp = "F" then
           assign h-file_id_mod = "G".
         if zidc.docdisp = "G" then
           assign h-file_id_mod = "H".
         if zidc.docdisp = "H" then
           assign h-file_id_mod = "I".
         if zidc.docdisp = "I" then
           assign h-file_id_mod = "J".
         if zidc.docdisp = "J" then
           assign h-file_id_mod = "K".
         if zidc.docdisp = "K" then
           assign h-file_id_mod = "L".
         
        if g-cono = 80 and o-company = "PF" then
          assign h-immed_origin_name = "PERFECTION SERVO       ".   
        else
          assign h-immed_origin_name = "SUNSOURCE              ".    
        put stream deposit UNFORMATTED
          h-record_type          format "9"
          h-priority_code        format "99"
          h-immed_dest           format "x(10)"
          h-immed_origin         format "x(10)"
          h-trans_date           format "x(6)"
          h-trans_time           format "x(4)"
          h-file_id_mod          format "x"
          h-record_size          format "999"
          h-blocking_factor      format "99"
          h-format_code          format "9"
          h-immed_dest_name      format "x(23)"
          h-immed_origin_name    format "x(23)"
          h-reference_code       format "x(8)"
         skip.
         assign zidc.docdisp = h-file_id_mod.
    end.     
   else 
      do:
        assign h-trans_date = disp-date.
        assign substring(h-trans_time,1,2) = substring(x-time,1,2).
        assign substring(h-trans_time,3,2) = substring(x-time,4,2).
        
        put stream deposit UNFORMATTED
          h-record_type          format "9"
          h-priority_code        format "99"
          h-immed_dest           format "x(10)"
          h-immed_origin         format "x(10)"
          h-trans_date           format "x(6)"
          h-trans_time           format "x(4)"
          h-file_id_mod          format "x"
          h-record_size          format "999"
          h-blocking_factor      format "99"
          h-format_code          format "9"
          h-immed_dest_name      format "x(23)"
          h-immed_origin_name    format "x(23)"
          h-reference_code       format "x(8)"
         skip.
         run create_zidc.
      end.
      
/* Create COMPANY HEADER RECORD */
assign ch-eff_date = disp-date2.

if g-cono = 80 and o-company = "PA" then
  assign ch-co_id       = 1416549000
         ch-co_name     = "SUNSOURCE      "
         ch-orig_dfi_id = 02100002.

  /**
  assign ch-co_id       = 1500000520
         ch-co_name     = "SUNSOURCE      "
         ch-orig_dfi_id = 07100001.
  **/
if g-cono = 80 and o-company = "PF" then
  assign ch-co_id       = 1186506000
         ch-co_name     = "PERFECTION     "
         ch-orig_dfi_id = 02100002.
if g-cono =  1 then
  assign ch-co_id       = 1416549000
         ch-co_name     = "SUNSOURCE      "
         ch-orig_dfi_id = 02100002.
  
assign ch-stnd_entry_class = if b-vendortype = "SDC" or
                                b-vendortype = "SDS" then
                               "PPD"
                             else
                               "CCD".

put stream deposit UNFORMATTED
        ch-record_type         format "9"
        ch-service_class-cd    format "999"
        ch-co_name             format "x(16)"
        ch-co_dis_name         format "x(20)"
        ch-co_id               format "9999999999"
        ch-stnd_entry_class    format "x(3)"
        ch-co_entry_disc       format "x(10)"
        ch-co_desc_date        format "x(6)"
        ch-eff_date            format "x(6)"
        ch-settlement_date     format "x(3)"
        ch-orig_stat_cd        format "x"
        ch-orig_dfi_id         format "99999999"
        ch-batch_nbr           format "9999999"
        skip.
        
/* Create ENTRY DETAIL RECORD */
assign detail-cnt = 0.
assign tot-bal-amt = 0.
for each apet use-index k-jrnl where apet.cono     = g-cono   and
                                     apet.jrnlno   = o-jrnlno and
                                     apet.vendno  >= b-vendor and
                                     apet.vendno  <= e-vendor and
                                     apet.transcd  = 07         /* payment */
                                     no-lock:
    find apsv use-index k-apsv where apsv.cono     = g-cono         and
                                     apsv.vendno   = apet.vendno    and
                                    (apsv.vendtype >= b-vendortype   or
                                     apsv.vendtype <= e-vendortype) and
                                     apsv.vendbanktrno ne ""        and
                                     apsv.vendbankacct ne ""     /* and */
/*                                   apsv.currbal  > 0 */
                                     no-lock no-error.
    
    if avail apsv then do:
       assign detail-cnt = detail-cnt + 1.
       assign receiving-dfi-id   = substring(apsv.vendbanktrno,1,8).
       assign check-digit        = substring(apsv.vendbanktrno,9,1).
       assign d-receiving_dfi_id = integer(receiving-dfi-id).
       assign d-check_digit      = integer(check-digit).
       assign d-dfi_account_nbr  = apsv.vendbankacct.
       assign d-amount           = apet.amount * 100.
       assign s-grand-amount     = s-grand-amount + apet.amount.
       assign d-employee_name    = CAPS(apsv.name).
       assign tot-bal-amt        = tot-bal-amt + (apet.amount * 100).
       assign d-trace_number     = detail-cnt.
       assign entry-hash         = entry-hash + d-receiving_dfi_id.
       if (apsv.vendtype = "edc" or
           apsv.vendtype = "ddc" or
           apsv.vendtype = "sdc" or
           apsv.vendtype = "ldc") then
          assign d-transaction_cd = 22.
       if (apsv.vendtype = "eds" or
           apsv.vendtype = "dds" or
           apsv.vendtype = "sds" or
           apsv.vendtype = "lds") then
          assign d-transaction_cd = 32.
       if (apsv.vendtype = "adc" or
           apsv.vendtype = "xdc" or
           apsv.vendtype = "udc" or
           apsv.vendtype = "pdc") then
          assign d-transaction_cd = 22.
       assign d-individual_id_nbr = if apsv.vendno = 10009854222 then
                                      "378750455931000"
                                    else
                                    if apsv.vendno = 10009854281 then
                                      "IL560869/711141"
                                    else
                                    if apsv.vendno = 10009854821 then
                                      "IL560869/854192"
                                    else
                                    if apsv.vendno = 10009854874 then
                                      "    POC39141191"
                                    else
                                    if apsv.vendno = 6000104 then
                                      "          XW383"
                                    else
                                    if apsv.vendno = 10000036835 then
                                      " P#12659/L#7617"
                                    else  
                                    if apsv.vendno = 6000062 then
                                      "  1FC0210011152"
                                    else
                                    if apsv.vendno = 61933500 then
                                      "           D010"
                                    else
                                    if apsv.vendno = 61934000 then
                                      "           D010"
                                    else
                                    if apsv.vendno = 20000018 then
                                      "       00419270"
                                    else
                                    if apsv.vendno = 10009855785 then
                                      "  8310006068665"
                                    else
                                    if apsv.vendno = 10009855776 then
                                      "379639121611006"
                                    else
                                    if apsv.vendno = 10009855779 then
                                      "379639128612007"
                                    else
                                    if apsv.vendno = 10009855778 then
                                      "379639130611005"
                                    else
                                    if apsv.vendno = 10009855777 then
                                      "379639137611008"
                                    else
                                    if apsv.vendno = 10009855775 then
                                      "379639145611008"
                                    else
                                    if apsv.vendno = 10009855900 then
                                      "379639888361001"
                                    else
                                    if apsv.vendno = 80000112 and
                                       apsv.cono   = 80 then
                                      "       CR613453"
                                    else  
                                      "               ".
       if d-individual_id_nbr = " " then
         assign d-individual_id_nbr = string(apsv.vendno).
       put stream deposit UNFORMATTED
              d-record_type       format  "9"
              d-transaction_cd    format  "99"
              d-receiving_dfi_id  format  "99999999"
              d-check_digit       format  "9"
              d-dfi_account_nbr   format  "x(17)"
              d-amount            format  "9999999999"
              d-individual_id_nbr format  "x(15)"
              d-employee_name     format  "x(22)"
              d-descret_data      format  "x(2)"
              d-addenda_rec_ind   format  "9"
              d-constant          format  "99999999"
              d-trace_number      format  "9999999"
              skip.
             
       display 
           apsv.vendno     d-employee_name     apet.amount
       with frame f-detail.
           
        assign x-line = x-line + 1.
        if (x-line + 4) > page-size then do:
           run headers.
        end.
    end.
end.
              

/*Create BATCH CONTROL RECORD */
assign cc-entry_add_cnt     = detail-cnt.
assign hash-amt             = string(entry-hash).
assign trunc-hash           = substring(hash-amt,1,10).
assign cc-entry_hash        = integer(trunc-hash).
assign cc-tot_cred_amount   = tot-bal-amt.

if g-cono = 80 and o-company = "PA" then
  /**
  assign cc-company_id = "1500000520".  
  **/
  assign cc-company_id = "1416549000".
if g-cono = 80 and o-company = "PF" then
  assign cc-company_id = "1186506000".
if g-cono =  1 then
  assign cc-company_id = "1416549000".

put stream deposit UNFORMATTED
       cc-record_type             format  "9"
       cc-service_cl_cd           format  "999"
       cc-entry_add_cnt           format  "999999"
       cc-entry_hash              format  "9999999999"
       cc-tot_debit_amount        format  "999999999999"
       cc-tot_cred_amount         format  "999999999999"
       cc-company_id              format  "9999999999"
       cc-auth_code               format  "x(19)"
       cc-reserved                format  "x(6)"
       cc-orig_dfi_id             format  "99999999"
       cc-batch_nbr               format  "9999999"
       skip.
       
/* Create FILE CONTROL RECORD */
assign fc-entry_add_cnt     = detail-cnt.
assign fc-entry_hash        = integer(trunc-hash).
assign fc-tot_file_cred     = tot-bal-amt.
put stream deposit UNFORMATTED
       fc-record_type             format  "9"
       fc-batch_cnt               format  "999999"
       fc-block_cnt               format  "999999"
       fc-entry_add_cnt           format  "99999999"
       fc-entry_hash              format  "9999999999"
       fc-tot_file_debit          format  "999999999999"
       fc-tot_file_cred           format  "999999999999"
       fc-reserved                format  "x(39)"
       skip.

display 
   s-grand-amount with frame f-total.

end. /* if */

PROCEDURE headers:
  page.
  assign x-page = x-page + 1.   
  assign x-line = 4.   
  display   
     x-today     x-time     g-cono     sapb.operinit     x-page   
   with frame f-header.   
end.


output stream deposit close.

if opsys = "unix" then
    do:
    os-copy value(o_file + o_doctype + ".txt") value(destfilename).
    end.

/*************************Procedure create_zidc*******************************/
procedure create_zidc.

  create zidc.
    assign zidc.cono       = 1.
           zidc.docttype   = string(TODAY,"99/99/99").
           zidc.doctype    = "ACH".
           zidc.docdisp    = h-file_id_mod.
           zidc.docdate    = TODAY.

  end.


