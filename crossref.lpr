/* crossref.lpr 1.2 03/09/98 */
/*h****************************************************************************
  INCLUDE      : crossref.lpr
  DESCRIPTION  : Cross Reference Lookup - Processing Include
  USED ONCE?   : no
  AUTHOR       : mtt
  DATE WRITTEN : 02/17/98
  CHANGES MADE :
    02/16/98 mtt; TB# 24049 PV: Common code use for GUI interface
    11/20/98 sbr; TB# 25090 Do not allow nonstock entry from catalog
    09/25/00 mjg; TB# 6522 Stronger scoping of sasoo.
    05/24/01  aa; TB# e5018 Add Product autolookup. Note: Added "a" (exact ICSP
        match), "q" for 'Begins' match in ICSC and ICSP, and "k" for CONTAINS
        (keyword) in ICSC, ICSP amd ICSW. (Only compiles for GUI)
    09/10/01 rgm; TB# e10271 Corrected auto-lookup for ambiguous interchange.
    10/10/01 rgm; TB# e9679 Fix for vendor part and alt-vendor product
    10/24/01 rgm; TB# e10896 Correction for pulling exact match of cust prod
        when matching 1 cust and several stock products (or vice versa)
    05/16/02 kjb; TB# e11740 Allow a catalog to pass through if the user can
        add catalogs into stock
    05/21/02 dls; TB# e13644 Replace spec char with a "_" for lookups.
    06/03/02 mwb; TB# e12302 Auto Product lookup fails on 'begins' lookup ('q').
    06/10/02 rgm; TB# e13525 Add option to disable automatic "*" wildcards
        before more of them have issues.
    02/06/03 bpa; TB# e16032 Auto lookup needs to use wildcard to check keyindex
    08/04/03 blr; TB# t11498 Cross Reference Functionality for Kit Components
        Project.  Added ability for "ps" Supersedes & Substitutes.
    05/26/04 bpa; TB# e19370 v-prod not being set back to original after
        checking keyindex if none found
    04/17/05 bp ; TB# e22019 Add user hooks.
    05/05/05 jbt; TB# e22019 Add user hooks.
    12/19/05 kjb; TB# e21338 Interchanges not recognized from SXAPI
        and Translation Code Cleanup
    02/28/06 sbr; TB# e23482 PDAA core dump on multiple interchanges
    11/09/06 ejk; TB# e22553 Allow use of barcode xref unit
******************************************************************************/

    {crossref.z99 &user_top = "*"}

    {&comui}
    clear frame f-lookup all.
    hide frame f-lookup no-pause.
    /{&comui}* */

    /*o Initialize variables */
    assign
        out-prod = ""
        out-whse = ""
        out-type = ""
        out-unit = ""
        out-qty  = 0.

    /*o Create a work file and display the cross references on the screen */
    l-top:
    do j = 1 to length(v-type)
    {&comui}
    on endkey undo {&label}, leave {&label}
    /{&comui}* */
    :

        assign
            v-icswfl = false
            v-icscfl = false.

        {crossref.z99 &user_xref = "*"}

        /*d for Bar Code, Customer Specific, Trade Service */
        if can-do("B,C,T,":u,substring(v-type,j,1)) and
            index(v-tyused,substring(v-type,j,1)) <> 0
        then do:

            {crossref.z99 &customer = "*"}

            /*d Find the ICSEC record */
            if substring(v-type,j,1) <> "c":u
            then
                {w-icsec.i substring(v-type,j,1) v-prod 0 no-lock}
            else
                find first icsec use-index k-custno where
                           icsec.cono    = g-cono   and
                           icsec.rectype = "c":u    and
                           icsec.custno  = v-custno and
                           icsec.prod    = v-prod
                no-lock no-error.

            /*d If the product in not set up, ask if it is a catalog product */
            if avail icsec then do:

                /*d Check ICSW First */
                if v-whse <> ""
                then
                    v-icswfl = {v-icsw.i icsec.altprod v-whse "/*"}.
                else do:

                    find first b-icsw where
                               b-icsw.cono = g-cono        and
                               b-icsw.prod = icsec.altprod
                    no-lock no-error.

                    assign
                        v-icswfl = if avail b-icsw then yes else no
                        v-whse   = if avail b-icsw then b-icsw.whse else v-whse.

                end. /* end of if v-whse <> "" */

                /*d If Not ICSW then Check ICSC */
                if v-icswfl = false then do:

                    v-icscfl = {v-icsc.i icsec.altprod "/*"}.

                    /*d Found in Catalog, Ask if Should Use */
                    if v-icscfl = true and icsec.rectype <> "t":u then do:

                        /*d Ask to Use Catalog **/
                        {&comui}
                        do on endkey undo, leave:

                            s-catfl = true.

                            if substring(g-ourproc,1,2) <> "bp":u then
                                update s-catfl with frame f-cat.

                        end. /* end of do on endkey undo */

                        if s-catfl = yes then
                          do:
                          {p-CatalogSwap1.i  &prodX = icsec.altprod}
                        end.

                        hide frame f-cat no-pause.

                        if {k-cancel.i} or {k-jump.i} then leave {&label}.

                        if not s-catfl then next l-top.
                        /{&comui}* */

                        {&catprompt1}

                    end. /* end of if v-icscfl = true and .... */

                end. /* end of if v-icswfl = false */
                if v-icswfl = true or v-icscfl = true or icsec.rectype = "t":u
                then do:
                    
                    assign
                        out-prod = icsec.altprod
                        out-whse = v-whse
                        out-type = substring(v-type,j,1)
                        out-qty  = if out-type = "c":u then icsec.orderqty
                                   else 0
                        out-unit = if out-type = "c":u then icsec.unitsell
                                   else if out-type eq "b":u  and
                                           index(g-currproc,"oeet":u) ne 0
                                        then icsec.unitstnd
                                   else ""
                        out-type = if v-icscfl = false then out-type
                                   else if out-type = "i":u then "z":u
                                   else if out-type = "b":u then "w":u
                                   else if out-type = "c":u then "y":u
                                   else out-type.

                    return.

                end. /* end of if v-icswfl = true or .... */

                else next l-top.

            end. /* end of if avail icsec */

            /*tb 19260 10/02/95 mcd; Add User Include to crossref.p */
            {crossref.z99 &user_bct = "*"}

            else next l-top.

        end. /* end of if can-do("B,C,T,",substring(v-type,j,1)) */

        /****** for Vendor's Product from ICSW ******/
        /*d for Vendor's Part # */
        else if substring(v-type,j,1) = "m":u then do:

            /*d Look in ICSW for the manufacturers part number in the
                vendprod field */
            /*d Look With Vendno if PO Process */
         &if "{&NXTREND-SYS}":u = "POWERVUE":u &then
            if g-vendno <> 0
         &else
            if can-do("po,ap":u,substring(g-ourproc,1,2))
         &endif
            then
                find first icsw use-index k-manprod where
                           icsw.cono      = g-cono   and
                           icsw.whse      = v-whse   and
                           icsw.vendprod  = v-prod   and
                           icsw.arpvendno = g-vendno
                no-lock no-error.

            /*d Else Look Without Vendor */
            else
                find first icsw use-index k-manprod where
                           icsw.cono     = g-cono and
                           icsw.whse     = v-whse and
                           icsw.vendprod = v-prod
                no-lock no-error.

            if avail icsw then do:

                assign
                    out-prod = icsw.prod
                    out-whse = v-whse
                    out-type = "M":u.

                return.

            end. /* end of if avail icsw */

            /*d If the product is not in ICSW, look for an alternate vendor
                product in ICSEC */
         &if "{&NXTREND-SYS}":u = "POWERVUE":u &then
            else if g-vendno <> 0 then do:
         &else
            else if can-do("po,ap":u,substring(g-ourproc,1,2)) then do:
         &endif

                find first icsec use-index k-altprod where
                           icsec.cono    = g-cono   and
                           icsec.altprod = v-prod   and
                           icsec.rectype = "v":u    and
                           icsec.keyno   = g-vendno
                no-lock no-error.

                if avail icsec then do:

                    assign
                        out-prod = icsec.prod
                        out-whse = v-whse
                        out-type = "M":u.

                    return.

                end. /* end of if avail icsec */

            end. /* end of else if can-do("po,ap",substring(g-ourproc,1,2)) */

            else next l-top.

        end. /* end of else if substring(v-type,j,1) = m */

        /*d**** for a stocked product defined in ICSW ******/
        else if substring(v-type,j,1) = "x":u then do:

            if ({v-icsw.i v-prod v-whse "/*"}) then do:

                assign
                    out-prod = v-prod
                    out-whse = v-whse
                    out-type = "X":u.

                return.

            end. /* end of if v-icsw.i */

            else next l-top.

        end. /* end of else if substring(v-tyep,j.1) = x */

        /*o**** ICSC Catalog ******/
        /*d for a Catalog product */
        else if substring(v-type,j,1) = "g":u then do:

            {crossref.z99 &catalog = "*" &comui = {&comui} }

            v-icscfl = {v-icsc.i v-prod "/*"}.

            if v-icscfl then do:

                {&comui}
                s-catfl = true.

                do on endkey undo, leave:

                    if substring(g-ourproc,1,2) <> "bp":u then
                        update s-catfl with frame f-cat.

                end. /* end of do on endkey undo */

                if s-catfl = yes then
                  do:
                  {p-CatalogSwap1.i  &prodX = v-prod}
                end.

                hide frame f-cat no-pause.

                if {k-cancel.i} or {k-jump.i} then leave {&label}.

                if not s-catfl then next l-top.


                /*tb 25090 11/20/98 sbr; Do not allow catalog if the user does
                    not have security to enter nonstocks. */
                /*tb e11740 05/16/02 kjb; If the user cannot enter nonstocks
                    on an OE line but can create stock items from catalog items
                    then allow them through and we'll catch back in OE.
                    Added sasoo.catconvertfl check. */
                if g-currproc = "oeet":u then do for sasoo:
                    {w-sasoo.i  g-operinits no-lock}
                    if not avail sasoo or
                        (avail sasoo             and
                         sasoo.nonstockfl   = no and
                         sasoo.catconvertfl = no)
                    then do:
                        run err.p(1122).
                        next l-top.
                    end.
                end.

                /{&comui}* */

                {&catprompt2}

                assign
                    out-prod = v-prod
                    out-whse = v-whse
                    out-type = "G":u.

                return.

            end. /* end of if v-icscfl */

            else next l-top.

        end. /* end of else if substring(v-type,j,1) = g */

        /*d** for Substitutes, Upgrades, and Alternate Warehouses ***/
        else if can-do("s,u,w":u,substring(v-type,j,1)) then do:

            if confirm = true then next l-top.

            if v-type <> "ps":u then do:

                for each w-icsw:

                   delete w-icsw.

                end. /* end of for each w-icsw */
            end. /*if v-type <> ps */

            assign
                confirm = true
                s-title = if v-type <> "ps":u
                                then ""
                          else s-title
                i       = if v-type <> "ps":u
                                then 0
                           else i.

            if v-xref99fl = yes then do:

                /*u xref99.p ("suw", in and out product, whse, type, qty, unit,
                               and positioning); Before subs, upgrades, and
                               alternate whses */
                v-done = no.

                run xref99.p("suw":u,
                             input-output v-prod,
                             input-output v-whse,
                             input-output v-type,
                             input-output v-custno,
                             input-output v-row,
                             input-output v-down,
                             input-output out-prod,
                             input-output out-whse,
                             input-output out-type,
                             input-output out-qty,
                             input-output out-unit).

                if v-done then return.

                find first w-icsw no-lock no-error.

                if avail w-icsw
                then
                    i = 1.

            end. /* end of if v-xref99fl = yes */

            /*d Find subs */

            if index(v-type,"s":u) <> 0 and index(v-tyused,"s":u) <> 0 then do:
                        s-title = if v-type <> "ps":u then "Substitutes"
                                  else s-title + " and Substitutes".
                {p-xref.i S}
            end. /* end of if index(v-type,s) <> 0 and .... */

            /*d Find upgrades */

            if index(v-type,"u":u) <> 0 and index(v-tyused,"u":u) <> 0 then do:

                s-title = if s-title = "" then "Upgrades"
                          else s-title + ", Upgrades".
                {p-xref.i U}

            end. /* end of if index(v-type,u) <> 0 */

            /*d Find alternate whses as defined in ICSEW */

            if index(v-type,"w":u) <> 0 and v-whsefl then do:

                /*tb 19484 07/18/96 gp; If either the selling whse, or the
                    alternate whse is a WL whse, the alternate whse info
                    does not display on the cross reference screen when a
                    product with not enough stock in the selling whse is
                    entered in OEETL.  The company is checked first, then
                    the selling whse, then the alternate whse. */

                /*d Check sasa for WL purchase */
                {wlwhse.gas &b       = " "
                            &icsdcom = "/*"}

                /*d Check if the selling whse is a WL whse */
                if v-modwlfl = yes then do:
                    {wlwhse.gas &whse     = "v-whse"
                                &whselive = "no"
                                &b2       = " "
                                &sasacom  = "/*"}
                end. /* if v-modwlfl = yes */

                s-title = if s-title = "" then "Warehouses"
                          else s-title + ", Warehouses".

                {w-icsp.i v-prod no-lock}

                if v-wlwhsefl = no then
                for each icsew where
                         icsew.cono = g-cono and
                         icsew.whse = v-whse
                no-lock:

                    /*d Check if the alternate whse is a WL whse */
                    if v-modwlfl = yes then do:
                        {wlwhse.gas &whse     = "icsew.whse2"
                                    &whselive = "no"
                                    &b2       = " "
                                    &sasacom  = "/*"}
                    end. /* if v-modwlfl = yes */

                    if v-wlwhsefl = yes then next.

                    {w-icsw.i v-prod icsew.whse2 no-lock}

                    if avail icsp and avail icsw then do:

                        if icsw.qtyonhand - icsw.qtyreservd - icsw.qtycommit
                                <= 0 then next.

                        if i = 100 then leave.

                        else i = i + 1.

                        create w-icsw.
                        assign
                            w-icsw.prod      = "Alt. Whse " + icsew.whse2
                            w-icsw.notesfl   = ""
                            w-icsw.qtyavail  = truncate(icsw.qtyonhand
                                               - icsw.qtyreservd
                                               - icsw.qtycommit,0)
                            w-icsw.descrip   = if icsw.ordcalcty <> "m":u
                                                   and w-icsw.qtyavail >
                                               (icsw.linept + icsw.ordqtyin)
                                               then string(w-icsw.qtyavail -
                                               (icsw.linept + icsw.ordqtyin),
                                               "zzzzzzzz9 Surplus")
                                               else if icsw.ordcalcty =
                                               "m":u and w-icsw.qtyavail >
                                               icsw.linept
                                               then string(
                                               w-icsw.qtyavail - icsw.linept,
                                               "zzzzzzzz9 Surplus")
                                               else ""
                            w-icsw.unit      = icsp.unitstock
                            w-icsw.usage     = icsw.usagerate
                            w-icsw.surpfl    = if icsw.ordcalcty <> "m":u
                                               and w-icsw.qtyavail >
                                               (icsw.linept + icsw.ordqtyin)
                                               then true else if
                                               icsw.ordcalcty =
                                               "m":u and w-icsw.qtyavail >
                                               icsw.linept then true
                                               else false
                            w-icsw.rectype   = "W":u.

                    end. /* if avail icsp and avail icsw */

                end. /* end of for each icsew */

            end. /* end of if index(v-type,w) <> 0 and v-whsefl */

        end. /* end of else if can-do(s,u,w,substring(v-type,j,1)) */

        /*d for Options */
        else
        if can-do("o":u,substring(v-type,j,1)) and index(v-tyused,"o":u) <> 0
        then do:

            for each w-icsw:

                delete w-icsw.

            end. /* end of for each w-icsw */

            assign
                i       = 0
                v-recid = 0
                s-title = "Options".

            {p-xref.i O}

        end. /* end of else if can-do(o,substring(v-type,j,1)) and .... */

        /*d for Interchanges */
        else
        if can-do("i":u,substring(v-type,j,1)) and index(v-tyused,"i":u) <> 0
        then do:

            if v-xref99fl then do:

                v-done = no.

                /*u xref99.p ("i", in and out product, whse, type, qty, unit,
                             and positioning); Before interchanges */
                run xref99.p("i":u,
                             input-output v-prod,
                             input-output v-whse,
                             input-output v-type,
                             input-output v-custno,
                             input-output v-row,
                             input-output v-down,
                             input-output out-prod,
                             input-output out-whse,
                             input-output out-type,
                             input-output out-qty,
                             input-output out-unit).
                if v-done then return.

            end. /* end of if v-xref99fl */

            find icsec use-index k-icsec where
                 icsec.cono    = g-cono and
                 icsec.prod    = v-prod and
                 icsec.rectype = "i":u
            no-lock no-error.

            if avail icsec then do:

                /*d Check ICSW First */
                /*tb e21338 12/19/05 kjb; When Interchange cross references
                    are run from the SXAPI, no whse is passed in.  Will now
                    attempt to find the alternate product in a whse. */
                if v-whse <> ""
                then
                    v-icswfl = {v-icsw.i icsec.altprod v-whse "/*"}.
                else do:

                    find first b-icsw where
                               b-icsw.cono = g-cono        and
                               b-icsw.prod = icsec.altprod
                    no-lock no-error.

                    assign
                        v-icswfl = if avail b-icsw then yes else no
                        v-whse   = if avail b-icsw then b-icsw.whse else v-whse.

                end. /* end of if v-whse <> "" */

                if not v-icswfl
                then
                    v-icscfl = {v-icsc.i icsec.altprod "/*"}.

                if v-icscfl then do:

                    /*d* Ask to Use Catalog **/
                    {&comui}
                    do on endkey undo, leave:
                        s-catfl = true.

                        if substring(g-ourproc,1,2) <> "bp":u then
                            update s-catfl with frame f-cat.

                    end. /* end of do on endkey undo */
                    if s-catfl = yes then
                      do:
                      {p-CatalogSwap1.i  &prodX = icsec.altprod}
                    end.
                    hide frame f-cat no-pause.

                    if {k-cancel.i} or {k-jump.i} then leave {&label}.

                    if not s-catfl then next l-top.

                    /{&comui}* */

                    {&catprompt3}

                end. /* end of if v-icscfl */

                if v-icscfl = true or v-icswfl = true then do:

                    assign
                        out-prod = icsec.altprod
                        out-whse = v-whse
                        out-type = if v-icscfl then "z":u else "i":u.

                    /*d Z Type Used to Indicate Catalog Interchange */

                    return.

                end. /* end of if v-icscfl = true or v-icswfl = true */

                else next l-top.

            end. /* end of if avail icsec */

            if ambiguous icsec then do:

                for each w-icsw:

                    delete w-icsw.

                end. /* end of for each w-icsw */

                /* some variables are only for use from the GUI side */
                &if "{&NXTREND-SYS}":u = "POWERVUE":u &then
                    assign
                        pv-outfieldlist = pv-outfieldlist
                                            + ( if pv-outfieldlist <> ''
                                                then pv-Delimiter else '' )
                                            + 'multxreffl':u
                        out-exitfl      = yes.
                &endif

                /* Don't Load. Caller uses this value to assess results*/
                assign
                    out-prod = ""
                    i        = 0
                    v-recid  = 0
                    s-title  = "Interchanges".

                if v-whse = "" then do:
                    find first icsec use-index k-icsec where
                         icsec.cono    = g-cono and
                         icsec.prod    = v-prod and
                         icsec.rectype = "i":u
                    no-lock no-error.
                    if avail icsec then do:
                        find first b-icsw where
                                   b-icsw.cono = g-cono        and
                                   b-icsw.prod = icsec.altprod
                        no-lock no-error.
                        v-whse = if avail b-icsw then b-icsw.whse
                                 else v-whse.
                    end.
                end.

                {p-xref.i I}

            end. /* end of if ambiguous icsec */

            else next l-top.

        end. /* end of else if can-do("i",substring(v-type,j,1)) and .... */

        /*d for supersedes */
        else if can-do("p":u,substring(v-type,j,1)) and
            index(v-tyused,substring(v-type,j,1)) <> 0
        then do:

            if v-xref99fl then do:

                v-done = no.
                /*u xref99.p ("p", in and out product, whse, type, qty, unit,
                             and positioning); Before supersedes */
                run xref99.p("p":u,
                             input-output v-prod,
                             input-output v-whse,
                             input-output v-type,
                             input-output v-custno,
                             input-output v-row,
                             input-output v-down,
                             input-output out-prod,
                             input-output out-whse,
                             input-output out-type,
                             input-output out-qty,
                             input-output out-unit).
                if v-done then return.
            end. /*if v-xref99fl then do*/

            find icsec use-index k-icsec where
                 icsec.cono    = g-cono and
                 icsec.prod    = v-prod and
                 icsec.rectype = "p":u
            no-lock no-error.

            if avail icsec then do:

                if g-currproc = "oeio":u or g-currproc = "kpio":u then do:

                   for each w-icsw:
                        delete w-icsw.
                   end. /* end of for each w-icsw */

                   assign
                      i       = 0
                      v-recid = 0
                      s-title = "Supersedes".

                   {p-xref.i P}
                end. /*if g-currproc = "oeio" or "kpio" */

                else if ({v-icsp.i icsec.altprod "/*"}) or
                        ({v-icsc.i icsec.altprod "/*"})
                then do:

                    assign
                        out-prod = icsec.altprod
                        out-whse = v-whse
                        out-type = "p":u.

                    return.

                end. /* end of if v-icsp.i or .... */

                else next l-top.

            end. /* end of if avail icsec */

            else if ambiguous icsec then do:

                for each w-icsw:

                    delete w-icsw.

                end. /* end of for each w-icsw */

                assign
                    i       = 0
                    v-recid = 0
                    s-title = "Supersedes".

                {p-xref.i P}

            end. /* end of else if ambiguous icsec */

            else next l-top.

        end. /* end of else if can-do(p,substring(v-type,j,1)) and .... */

        /*Start: ''Begins'' and ''Contains'' logic applies only to GUI */
        &if "{&NXTREND-SYS}":u = "POWERVUE":u &then

        /*d**** For an exact match in ICSP */
        else if substring(v-type,j,1) = "a":u then do:

            if can-find(icsp where icsp.cono = g-cono
                               and icsp.prod = v-prod )
            then do:

                assign
                    out-prod = v-prod
                    out-type = "a":u.

                return.

            end. /* end of if v-icsw.i */

            else next l-top.

        end. /* end of else if substring(v-tyep,j.1) = x */

        /*"q" for "Begins" */
        else if substring(v-type,j,1) = "q":U then do:

            assign
                out-prod    = ""
                i           = 0
                rICSECRecid = ?
                rICSPRecid  = ?.

            IcsecBegins: /*Customer Produt, begins */
            for each icsec where
                     icsec.cono    = g-cono   and
                     icsec.rectype = "c":U    and
                     icsec.custno  = v-custno and
                     icsec.prod    begins v-prod
                no-lock:


                i = i + 1.
                if i = 1 then do:
                    rICSECRecid = recid(icsec).
                    next IcsecBegins.
                end.

                /* There are more than one, report back multiple match (begins).
                   This only executes when i = 2  */
                /* Don't Load out-prod. Caller uses this value to assess
                   results*/
                /* Field List and Field Data have the same effect of
                   "Add-Return-Data-Element" */
                assign
                    pv-outfieldlist = pv-outfieldlist
                                        + ( if pv-outfieldlist <> ''
                                            then pv-Delimiter else '' )
                                        + 'BeginsMatch':u
                    pv-outfielddata = pv-outfielddata
                                        + ( if pv-outfielddata <> ''
                                            then pv-Delimiter else '' )
                                        + icsec.altprod
                    out-exitfl      = yes
                    out-prod        = ""
                    out-type        = substring(v-type,j,1)
                    rICSECRecid     = ?.
                return.

            end. /* for each icsec for custnomer prod */

            assign
                out-prod = ""
                i        = 0.

            IcspBegins:
            for each icsp where
                     icsp.cono    = g-cono   and
                     icsp.prod    begins v-prod
                no-lock:

                i = i + 1.
                if i = 1 then do:
                    rICSPRecid = recid(icsp).
                    next IcspBegins.
                end.

                /* Don't Load out-prod. Caller uses this value to assess
                   results*/
                /* Field List and Field Data have the same effect of
                   "Add-Return-Data-Element" */
                assign
                      pv-outfieldlist = pv-outfieldlist
                                        + ( if pv-outfieldlist <> ''
                                            then pv-Delimiter else '' )
                                        + 'BeginsMatch':u
                      pv-outfielddata = pv-outfielddata
                                        + ( if pv-outfielddata <> ''
                                            then pv-Delimiter else '' )
                                        + v-prod
                      out-exitfl      = yes
                      out-prod        = ""
                      out-type        = substring(v-type,j,1)
                      rICSPRecid      = ?.

                return.

            end. /* for each icsp */

            if (rICSPRecid <> ? or rICSECRecid <> ?) and
               (rICSPRecid = ?  or rICSECRecid = ?)
            then do:

                if rICSPRecid <> ? then do:

                    find first icsp where recid(icsp) = rICSPRecid no-lock.
                    assign
                        out-prod = icsp.prod
                        out-type = substring(v-type,j,1).
                end.
                else if rICSECRecid <> ? then do:

                    find first icsec where recid(icsec) = rICSECRecid no-lock.
                    assign
                        out-prod = icsec.altprod
                        out-type = substring(v-type,j,1).
                end.
                return.
            end.
        end. /* "Begins" */

        /* "k" for "Contains" */
        else if substring(v-type,j,1) = "k":U then do:

            assign
                out-prod = ""
                i        = 0.

            {removespecchar.i &NewValue  = v-prod
                              &FieldName = "'x'"
                              &ModAster  = no}

            assign
                v-orig-prod = v-prod
                v-prod = v-prod + "*".

            IcspContains:
            for each icsp where
                     icsp.cono    = g-cono   and
                     icsp.keyindex CONTAINS v-prod
                no-lock:

                i = i + 1.
                if i = 1 then do:
                    assign
                       out-prod = icsp.prod
                       out-type = substring(v-type,j,1).
                    next IcspContains.
                end.

                assign
                    v-prod          = v-orig-prod
                    pv-outfieldlist = pv-outfieldlist
                                      + ( if pv-outfieldlist <> ''
                                          then pv-Delimiter else '' )
                                      + "ContainsMatch":u
                    pv-outfielddata = pv-outfielddata
                                      + ( if pv-outfielddata <> ''
                                          then pv-Delimiter else '' )
                                      + v-prod
                    out-exitfl      = yes
                    out-prod        = ""
                    out-type        = substring(v-type,j,1).

                return.

            end. /* for each icsp */

            v-prod = v-orig-prod.

            /* Treat as exact match (only one found) */
            if i = 1 then return.

            assign
                out-prod = ""
                i        = 0.

            IcscContains:
            for each icsc where
                     icsc.keyindex CONTAINS v-prod
                no-lock:

                i = i + 1.
                if i = 1 then do:

                    /* Special case: With ICSC only return unique if ICSP
                       exists  (do not run the lookup window in this case).
                       If ICSP does not exist, run the lookup (even if ther is
                       only one icsc */
                    {w-icsp.i icsc.catalog no-lock}

                    if avail icsp then do:
                        assign
                           out-prod = icsc.catalog
                           out-type = substring(v-type,j,1).
                        next IcscContains.
                    end.

                end.

                /* This only executes when i = 2 (or icsp does not exist) */
                assign
                      pv-outfieldlist = pv-outfieldlist
                                        + ( if pv-outfieldlist <> ''
                                            then pv-Delimiter else '' )
                                        + "ContainsMatch":u
                      pv-outfielddata = pv-outfielddata
                                        + ( if pv-outfielddata <> ''
                                            then pv-Delimiter else '' )
                                        + v-prod
                      out-exitfl      = yes
                      out-prod        = ""
                      out-type        = substring(v-type,j,1).

                return.

            end. /* for each icsc */

            /* Treat as exact match (only one found) */
            if i = 1 then return.

            assign
                out-prod = ""
                i        = 0.

            IcswContains:
            for each icsw where
                     icsw.cono    = g-cono   and
                     icsw.keyindex CONTAINS v-prod
                no-lock:

                i = i + 1.
                if i = 1 then do:
                    assign
                       out-prod = icsw.prod
                       out-type = substring(v-type,j,1).
                    next IcswContains.
                end.

                assign
                      pv-outfieldlist = pv-outfieldlist
                                        + ( if pv-outfieldlist <> ''
                                            then pv-Delimiter else '' )
                                        + "ContainsMatch":u
                      pv-outfielddata = pv-outfielddata
                                        + ( if pv-outfielddata <> ''
                                            then pv-Delimiter else '' )
                                        + v-prod
                      out-exitfl      = yes
                      out-prod        = ""
                      out-type        = substring(v-type,j,1).

                return.

            end. /* for each icsw */

            /* Treat as exact match (only one found) */
            if i = 1 then return.

        end. /* K, "Contains" */

        &ENDIF
        /*End: ''Begins'' and ''Contains'' logic (applies only to GUI) */

    end. /* end of l-top */

    {crossref.z99 &user_bd = "*"}

    if i = 0 then leave {&label}.
