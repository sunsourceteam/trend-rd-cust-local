output to "/usr/tmp/testkpmod.lxg".


def var recno as integer no-undo.
def var sellqty as decimal no-undo.
def var sellqty2 as decimal no-undo. 
def var skipprod as character format "x(24)" no-undo.
def var zxx as integer no-undo.
def var xyr as integer no-undo.


def new shared temp-table xwo no-undo
  field whse   like icsw.whse
  field custno like arsc.custno
  field shipto like smsew.shipto
  field pcat   like icsp.prodcat
  field prod   like icsp.prod
  field yr     as integer
  field mnth   as integer
  field qty    as decimal
  field qtyfl  as decimal
  field salesamt as decimal
  field wono   like kpet.wono
  field wosuf  like kpet.wosuf
index
  whse
  custno
  shipto
  prod           
  yr
  mnth descending.


run smrzk98x.p.
for each xwo :
export xwo.
end.