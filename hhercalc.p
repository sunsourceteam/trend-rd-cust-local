/* hhercalp.p */
define input-output parameter x-rec as decimal no-undo.

define var x-adder  like poel.qtyrcv  format ">>>>>9.99" no-undo.

form
"Add Entered Value to "     at 1
"the current Qty"           at 1
x-rec                       at 1  format ">>>>>9.99"
"Additional Qty"            at 1
x-adder                     at 1
with frame f-adder width 26 row 4 overlay no-labels.   

display x-rec with frame f-adder.
 do on endkey undo, leave:
   update
     x-adder 
       with frame f-adder
      xxeditloop: 
   
        editing:
 
        readkey.
        apply lastkey.
      end.
 end.

 if not {k-cancel.i} and not {k-jump.i} then
   assign x-rec = x-rec + x-adder.


 hide frame f-adder.
 pause 0.         
