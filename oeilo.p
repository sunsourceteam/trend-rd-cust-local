/*h****************************************************************************
  PROCEDURE      : oeilo
  DESCRIPTION    : Lookup orders
  AUTHOR         : rhl
  DATE WRITTEN   : 09/08/89
  CHANGES MADE   :
  12/23/91 pap; TB#  5266 JIT/Line Due - replace requested date with promised
        date
  06/04/92 smb; TB#  6867 Operator choice of Qty/Value
  07/21/92 mwb; TB#  7191 added custno changes
  07/02/93 mwb; TB# 11967 stop channel customer from jump box
  01/29/94 kmw; TB# 11747 Make stage a range
  02/21/94 dww; TB# 14593 Lookup name for product doesn't display
  03/29/94 mwb; TB# 14196 Added crossref.gpr for barcode and interchange
            cross references.
  04/29/94 tdd; TB# 15509 Added user hook &user_aftermain
  01/10/95 dww; TB# 17132 Function Key bar appearring too early.
  05/09/95 mtt; TB#  5180 When looking for a product-need kit ln
  03/14/96 tdd; TB# 20713 Inquiry not working properly
  07/15/97 jl;  TB# 23377 Global Defaults not being set
  03/31/99 lbr; TB# 20955 Added ability to display orders in accending = yes           or decending = no
  05/11/99 cm;  TB# 5366 Added shipto notes
  01/05/00 dki; TB# 27118 Add user hook &user_beforeupd
  02/12/01 lcg; TB# e7852 AR Multicurrency changes
  08/09/02 emc; TB# e6846 Slow with whse and stagecd range
  10/02/02 emc; TB# e13512 Slow searches
                    *******************************************************************************/{g-all.i}
{g-ar.i}
{g-ic.i}
{g-oe.i}
{g-inq.i}
/*tb 14593 02/21/94 dww; Lookup Name for Product Doesn't Display */
def var o-prod      like icsp.prod      no-undo.
/*tb 14196 03/29/94 mwb; added variables for global crossref.gpr */
{g-xref.i}

{f-oeilo.i "new"}

assign
  s-custno = g-custno
  s-shipto = g-shipto
  s-prod   = g-prod
  s-whse   = g-whse.

main:
do while true with frame f-oeilo on endkey undo main, leave main:
  {p-setup.i f-oeilos}
  hide frame f-oeilo.
  view frame f-oeilos.

  display " " @ s-crossref with frame f-oeilos.
  display v-msg1 v-msg2 v-msg3 with frame f-oeilos.
  color display input v-msg1 v-msg2 v-msg3 with frame f-oeilos.

  /*tb 8343 enp 01/08/93 Change oeilo to see value not quantity */
  /*tb 14593 02/21/94 dww; Lookup Name for Product Doesn't Display */
  /*tb 5366 05/11/99 cm; Added shipto notes */
  assign
    s-valuefl = if s-prod = "" then true else false
    o-prod    = ?
    o-custno  = ?
    o-shipto  = ?.

  hide message no-pause.
  display s-custno with frame f-oeilos.

  /*tb 17132 01/10/95 dww; Function Key appearring too early */
  put screen row 21 column 3 color messages
  "                                                                            ".

  /*tb 27117 add user hook */
  {oeilo.z99 &user_beforeupd = "*"}

  /*tb 5180 05/09/95 mtt; When looking for a product-need kit ln */
  update
    s-custno when not {c-chan.i}
      validate({v-arsc.i s-custno},
    "Customer Not Set Up in Customer Setup - ARSC (4303)")
    s-shipto
      validate(if s-shipto = "" then true
                                else {v-arss.i "input s-custno" s-shipto},
    "Customer/Ship To Not Setup - ARSS (4304)")
    s-prod
    s-kitty
      help "(O)rders Only, (K)it Components Only, (B)oth-Slower Access"
    validate(can-do("o,k,b",s-kitty),
    "Must be (O)rder, (K)it Components, or (B)oth (4838)")
    s-custpo
    s-usekeyfl
    s-takenby
    /*tb 3763 & 6636 6-4-92 smb;added whse option;if no entry
            won't change s-whse*/
    s-whse
      validate(if s-whse = "" then true
                              else {v-icsd.i s-whse "/*"},
    "Warehouse Not Setup in Warehouse Setup - ICSD (4601)")
    s-transtype
      validate(if can-do("bo,br,,",s-transtype) then true
               else if s-transtype = "ra" then true
                    else if {v-oetype.i s-transtype} then true
                    else false,
      "Must be SO, DO, FO, ST, CS, QU, RM, CR, BO, BL, RA or BR (3647)")
      help "SO, DO, FO, ST, CS, QU, RM, CR, BO, BL, BR, RA or Blank"
      /*tb 6507 05/04/92 dea; Add query for DO line orders only */
    s-doonlyfl
    /*tb 11747 01/29/94 kmw; Make stage a range; Don't allow a blank stage */
    s-stagecdl
      validate(can-do("0,1,2,3,4,5,9",s-stagecdl),
      "Must be 0, 1, 2, 3, 4, 5 or 9 (3141)")
      help "0-Enter 1-Order 2-Pick 3-Ship 4-Invc 5-Paid or 9-Cancel"
    s-stagecdh validate(can-do("0,1,2,3,4,5,9",s-stagecdh),
      "Must be 0, 1, 2, 3, 4, 5 or 9 (3141)")
      help "0-Enter 1-Order 2-Pick 3-Ship 4-Invc 5-Paid or 9-Cancel"
    s-promisedt
      help "Display Orders Promised From (Date)"
      /*tb 6867 6-4-92 smb; operator choice of qty/value*/
    s-valuefl
      help "(V)alue or (Q)uantity"
    s-directionfl
      with frame f-oeilos editing:
        readkey.
        if {k-after.i} then do:
          assign
            s-prod
            s-valuefl.
            /*tb 5366 05/11/99 cm; Added shipto notes */
            /*tb e7852 07/28/00 lcg; Multicurr Added display currency. *****
            {t-custno.i &field    = s-custno
                        &frame    = f-oeilos
                        &display  = "arsc.lookupnm"
                        &code     = "g-custno = s-custno."
                        &shiptofl = "*"
                        &shipto   = s-shipto}
            *******************************Removed by tb e7852 */

            /*tb e7852 07/28/00 lcg; A/R Multicurr Added display currency */
            {g-help.i}

            /* dkt Add Hook 06/03/2005 */
            {oeilo.z99 &user_edit = "*"}  


            if input s-custno ne o-custno then do:
              assign s-custno.
              o-custno = s-custno.
              if {c-chan.i} and g-oecustno ne s-custno then do:
                run err.p(1100).
                next-prompt s-custno with frame f-oeilos.
                next.
              end.
              if input {c-edit.i "s-custno"} then do:
                {w-arsc.i o-custno no-lock}
                if avail arsc then do:
                  display arsc.notesfl arsc.lookupnm with frame f-oeilos.
                  if arsc.currencyty = "" then
                    color display normal arsc.currencyty
                    with frame f-oeilos.
                  else
                    color display message arsc.currencyty
                    with frame f-oeilos.
                  display arsc.currencyty with frame f-oeilos.
                end.
                else if g-ourproc begins "arsc" then o-custno = ?.
                else do:
                  assign
                    g-f12pass = o-custno
                    o-custno  = ?
                    g-f12fl   = true.
                  apply 312.
                end.
                g-custno = s-custno.
              end.
              else display "" @ arsc.notesfl "" @ arsc.lookupnm
                           "" @ arsc.currencyty with frame f-oeilos.
            end.

            /*tb 8343 enp 01/08/93 Change oeilo to see value not quantity */
            /*tb 14593 02/21/94 dww; Lookup Name for Product Doesn't Display */
            if s-prod ne o-prod then do:
              hide message no-pause.
              s-valuefl = if s-prod = "" then true else false.
              display s-valuefl with frame f-oeilos.
              if s-prod = "" then
                display "" @ s-crossref
                        "" @ icsp.notesfl
                        "" @ icsp.lookupnm with frame f-oeilos.
              else if {c-edit.i s-custno} and avail arsc and arsc.custprodfl
                then do:
                  run crossref.p(s-prod,s-whse,"c",s-custno,1,5,
                    output v-prod, output v-whse,
                    output v-type, output v-crqty, output v-crunit).
                  if v-prod ne "" then do:
                    s-prod = v-prod.
                    display s-prod
                      "Crossref" @ s-crossref with frame f-oeilos.
                  end.
                  else
                    display "" @ s-crossref with frame f-oeilos.
                  {w-icsp.i s-prod no-lock}
                  if avail icsp then
                    display icsp.notesfl
                    icsp.lookupnm with frame f-oeilos.
                    /*tb 14196 03/29/94 mwb; added other cross references */
                  else do:
                    {crossref.gpr &prod   = s-prod
                                  &whse   = s-whse
                                  &custno = s-custno
                                  &row    = 8
                                  &down   = 5}
                    if v-refprod ne "" then do:
                      {w-icsp.i s-prod no-lock}
                      display s-prod
                        icsp.notesfl  when avail icsp
                        icsp.lookupnm when avail icsp
                        "Crossref" @ s-crossref with frame f-oeilos.
                    end.
                    else
                      display "" @ icsp.notesfl
                        "Non Stock" @ icsp.lookupnm
                        with frame f-oeilos.
                  end.
                end.
                else do:
                  {w-icsp.i s-prod no-lock}
                  if avail icsp then
                    display icsp.notesfl
                      icsp.lookupnm with frame f-oeilos.
                      /*tb 14196 03/29/94 mwb; added other cross references */
                  else do:
                    {crossref.gpr &prod   = s-prod
                                  &whse   = s-whse
                                  &custno = s-custno
                                  &row    = 8
                                  &down   = 5}
                    if v-refprod ne "" then do:
                      {w-icsp.i s-prod no-lock}
                      display s-prod
                        icsp.notesfl  when avail icsp
                        icsp.lookupnm when avail icsp
                        "Crossref" @ s-crossref with frame f-oeilos.
                    end.
                    else
                      display "" @ icsp.notesfl
                        "Non Stock" @ icsp.lookupnm
                        with frame f-oeilos.
                  end.
                end.
              o-prod = s-prod.
            end.
          end.
        apply lastkey.
      end.

      /* dkt Add Hook 06/03/2005 */
      {oeilo.z99 &user_afterheader = "*"}  

      /*tb 11747 01/29/94 kmw; Make stage a range */
      if s-stagecdh < s-stagecdl then do:
        run err.p(3734).
        {pause.gsc "3"}
        next main.
      end.
      if s-custno ne {c-empty.i} then {w-arsc.i s-custno no-lock}
      if s-prod ne "" and {c-edit.i s-custno} and arsc.custprodfl then do:
        run crossref.p(s-prod,s-whse,"c",s-custno,1,5,
                       output v-prod, output v-whse,
                       output v-type, output v-crqty, output v-crunit).
        if v-prod ne "" then do:
          s-prod = v-prod.
          display s-prod "Crossref" @ s-crossref with frame f-oeilos.
        end.
      end.
      /*tb 14593 02/21/94 dww; Lookup Name for Product Doesn't Display */
      if s-prod ne "" then do:
        {w-icsp.i s-prod no-lock}
        if avail icsp then
          display icsp.notesfl
          icsp.lookupnm with frame f-oeilos.
        /*tb 14196 03/29/94 mwb; added other cross references */
        else do:
          {crossref.gpr &prod   = s-prod
                        &whse   = s-whse
                        &custno = s-custno
                        &row    = 8
                        &down   = 5}
          if v-refprod ne "" then do:
            {w-icsp.i s-prod no-lock}
            display s-prod
              icsp.notesfl  when avail icsp
              icsp.lookupnm when avail icsp
              "Crossref" @ s-crossref with frame f-oeilos.
          end.
          else display "" @ icsp.notesfl
            "Non Stock" @ icsp.lookupnm with frame f-oeilos.
        end.
      end.

      /*tb 9247 02/01/93 rhl; Slow if only stagecd entered */
      /*tb 11747 01/29/94 kmw; Make stage a range */
      /*tb 5180 05/09/95 mtt; When looking for a product-need kit ln */
      /*tb e13512 Only use keyword inquiry when no product selected - will want
           to go thru lines/kits first.  If didn't enter any key fields, then
           also don't want to go thru keywords.  If a lot of records must be
           generated keywords will be slower - a temp table of matches must
           be generated and dislayed. */
      if s-usekeyfl and s-prod = "" and
        (s-custno    <> {c-empty.i} or
         s-shipto    <> "" or
         s-takenby   <> "" or
         s-whse      <> "" or
         s-promisedt <> ?  or
         s-stagecdl  > "0" or
         s-stagecdh  < "9") then v-mode = 0.
      else if s-prod = ""    and {c-edit.i s-custno}      and s-custpo = ""
        then v-mode = 1.
        else if s-prod ne ""   and s-custno =  {c-empty.i} and s-custpo =  ""
        then v-mode = 2.
        else if s-prod = ""    and s-custno =  {c-empty.i} and s-custpo ne ""
        then v-mode = 3.
        else if s-prod ne ""   and s-custno ne {c-empty.i} and s-custpo =  ""
        then v-mode = 4.
        else if s-prod = ""    and s-custno ne {c-empty.i} and s-custpo ne ""
        then v-mode = 5.
        else if s-prod ne ""   and s-custno =  {c-empty.i} and s-custpo ne ""
        then v-mode = 6.
        else if s-prod ne ""   and s-custno ne {c-empty.i} and s-custpo ne ""
        then v-mode = 7.
        else if ((s-stagecdl ne "0") or (s-stagecdh ne "9")) and s-whse = ""
        then v-mode = 9.
        else v-mode = 8.

    /*tb 6867 6-4-92 smb; operator choice of qty/value*/
    /*tb 23377 07/15/97 jl; Added assign of global variables for custno, shipto
         prod and whse */
    assign
      v-msg2     = if can-do("1,4,5,7",string(v-mode)) then "PO"
                   else " Customer # Ship To "
      v-msg3     = if s-valuefl then "     Value  "
                   else if can-do("2,4",string(v-mode)) then "Quantity    "
                        else "  Quantity   "
      g-modulenm = ""
      g-menusel  = ""
      g-custno   = s-custno
      g-shipto   = s-shipto
      g-prod     = s-prod
      g-whse     = s-whse.

    display v-msg2 with frame f-oeilos.
      color display input v-msg2 with frame f-oeilos.
    display v-msg3 with frame f-oeilos.
      color display input v-msg3 with frame f-oeilos.
    /*tb 17132 01/10/95 dww; Function Key appearring too early */
    if {c-chan.i} then put screen row 21 col 3 color message
      " F6-Confirm     F7-Price/Avail F8-Lkup Prod                                 ".
    else put screen row 21 col 3 color message
      " F6-Detail      F7-Confirm     F8-Customer    F9-Prod Avail  F10-Incoming   ".

    if v-mode = 0
      then run oeilok.p.
    /*tb 5180 05/09/95 mtt; When looking for a product-need kit ln */
    else if can-do("1,7",string(v-mode))
         then run oeilo1.p.
         else if v-mode = 2 and s-kitty = "k" and s-whse ne ""
         then run oeilo2k1.p.
         else if v-mode = 2 and s-kitty = "k" and s-whse = ""
         then run oeilo2k2.p.
         else if v-mode = 2 and s-kitty = "b"
         then run oeilo2b.p.
         else if v-mode = 2 and s-kitty = "o"
         then run oeilo2.p.
         else if can-do("3,6",string(v-mode))
         then run oeilo3.p.
         else if v-mode = 4 and s-kitty = "k" and s-whse ne ""
         then run oeilo4k1.p.
         else if v-mode = 4 and s-kitty = "k" and s-whse = ""
         then run oeilo4k2.p.
         else if v-mode = 4 and s-kitty = "b"
         then run oeilo4b.p.
         else if v-mode = 4 and s-kitty = "o"
         then run oeilo4.p.
         else if v-mode = 5
         then run oeilo5.p.
         else if v-mode = 8
         then run oeilo8.p.
         else if v-mode = 9
         then run oeilo6.p.

    if {k-cancel.i} then next.
    if g-modulenm ne "" and g-modulenm ne "pa" then do:
      hide frame f-oeilo.
      hide frame f-oeilos.
      return.
    end.
    if {k-select.i} or {k-jump.i} then leave.
end.

/*tb 15509 04/29/94 tdd; Added user hook */
{oeilo.z99 &user_aftermain = "*"}
if g-lkupfl then hide frame f-oeilo.
if {k-select.i} then do:
  /*tb 11967 07/02/93 mwb; Pressing <go> no longer goes to jump box */
  if {c-chan.i} then do:
    readkey pause 0.
    run jump.p.
  end.
  else do:
    {j-all2.i "frame f-oeilos"}.
  end.
end.
else do:
  {j-all.i "frame f-oeilos"}
end.
