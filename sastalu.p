/* sastalu.p 1.3 11/05/98 */
/*******************************************************************************
  PROCEDURE   : sastalu
  DESCRIPTION : lookup sasta items
  AUTHOR      : enp
  DATE WRITTEN: 07/31/89
  CHANGES MADE: 09/26/90 dao Eliminated pause when exiting xxxxlu.i by reseting
                         g-errfl
                04/05/93 rhl; TB# 8741 F12 Product Category Lookup
                        not working cross company
                07/25/94 rs; TB# 3491 Rpt Manager Schedule Enhancement
                10/11/94 rs; TB# 13046 SASTT values description lookup
                12/30/94 rs; TB# 17173 Country Code Lookup has no Title
                07/25/95 jb; TB# 18922 Add Sales Tax Override Code (Type 'aa')
                02/03/98 sbr; TB# 22687 Function key line not going away after
                         lookup on category
                10/15/98 sbr; TB# 25423 Screen doesn't refresh after F12 lookup
                06/24/99 sbr; TB# 26296 Lookup display remains on screen
                03/13/07 blw; TB# e25682 User Group for sasoo and saic usage.
*******************************************************************************/
{g-lu.i}
def input param v-type like sasta.codeiden                              no-undo.

def var v-setup        as c initial ["sastt"]                           no-undo.
def var v-start        like sasta.codeval                               no-undo.
def var v-title2       as c format "x(15)"                              no-undo.
def var s-cono         like g-cono                                      no-undo.
def var v-viewty       as c                                             no-undo.

/*tb 3491 07/25/94 rs; Rpt Manager Schedule Enhancement */
/*tb 17173 12/30/94 rs; Country Code Lookup has no Title */
/*tb 18922 07/26/95 jb; Add "Tax Override" Label  */

def var v-title       as c format "x(15)" extent 21 initial

    ["Tax Override",
     "Buyer","Prod Category","Lost Business",
     "Frozen Reason","G/L Rpt Group","Report Schedule",
     "Family Group",
     "Cust Price Type","Prod Price Type","Reason Unavail",
     "Return/Adjust","Non Tax Reason","Usage Override",
     "Lifo Category",
     "Ship Via","Terms",
     "Country","Language","Sales Territory","User Group"] no-undo.

/*tb 8741  04/05/93 rhl; F12 Product Category Lookup not working */
/*tb 3491  07/25/94 rs;  Rpt Manager Schedule Enhancement */
/*tb 13046 10/07/94 rs;  SASTT values description lookup */
/*tb 18922 07/26/95 jb;  Add Tax Override Code 'aa' to lookup list */

assign
    s-cono   = if v-type = "c" and index(frame-field,"prodcati") ne 0
                   then g-cono2 else g-cono
    v-length = 12
    i        = lookup(v-type,"aa,b,c,e,f,g,h,i,j,k,l,m,n,o,q,s,t,w,y,z,ug")
    v-title2 = if i ne 0 then v-title[i] else ""
    v-viewty = if v-type = "e" then "D" else "C".

/*tb 22687 02/03/98 sbr; Set v-length to 11 so the function key line will
disappear after a lookup on category is done on a non-stock item in oeet */
/*tb 25423 10/15/98 sbr; Added "or v-type = "e"" to if condition in order to
correct the same problem when a lookup is done on the lost business reason */
/*tb 26296 06/24/99 sbr; Added "or g-currproc = "icamu"" to the if condition in
order to correct the same problem when lookups are done within icamu banner */
if (g-currproc = "oeet" and (v-type = "c" or v-type = "e")) or
    g-currproc = "icamu" then
  assign v-length = 11.

form
 sasta.codeval  format "x(6)"       at  1 label "Code"
   help ""
 sasta.descrip                      at  9 label "Description"
   help ""
with frame f-lookup overlay column 35 scroll 1
  row 3 v-length down title v-title2 + " by Code".

/*tb 13046 10/07/94 rs; SASTT values description lookup */
/*d Frame used by xxxxlu.i & placed here to avoid frame scope problems */
form
  v-start format "x(20)" label "Start at"
with frame f-start side-labels overlay centered row 6.

pause 0 before-hide.
on f11 bell.

main:
repeat for sasta with frame f-lookup on endkey undo main, leave main
                                     on error  undo main, leave main:

/*tb 13046 10/07/94 rs; SASTT values description lookup */
    v-viewty = if {k-func6.i} then "C"
               else if {k-func7.i} then "D"
               else v-viewty.
    view frame f-lookup.

 /*tb 13046 10/07/94 rs; SASTT values description lookup */
 /*d Access by codeval */
    if v-viewty = "C" then do:

 /*tb 22687 02/03/98 sbr; Display at v-length + 6 in order to make the
        function key line disappear after the lookup is done on the category */
      put screen row (v-length + 6) col 36 color message
          " F6-CODE   F7-Descr           ".
      {xxxxlu.i
               &find    = "n-sasalu.i"
               &file    = "sasta"
               &chfield = "sasta.codeval"
               &display = "sasta.descrip sasta.codeval"
               &where   = "w-sasalu.i"
               &go-on   = "F6 F7"
               &func    = "*"
               &funcpr  = "if {k-func6.i} or {k-func7.i} then next main.
                           else if {k-func.i} then do:
                              bell.
                                                                                                              next dsply.
                              end."
               &global  = "frame-value = frame-value"}.
      end.
/*tb 13046 10/07/94 rs; SASTT values description lookup */
/*d Access by Description */
      else do:

  /*tb 22687 02/03/98 sbr; Display at v-length + 6 in order to make the
     function key line disappear after the lookup is done on the category */
        put screen row (v-length + 6) col 36 color message
           " F6-Code   F7-DESCR           ".

        {xxxxlu.i
                 &find    = "n-sasalu.i"
                 &file    = "sasta"
                 &chfield = "sasta.descrip"
                 &display = "sasta.descrip sasta.codeval"
                 &where   = "w-sasalu.i"
                 &go-on   = "F6 F7"
                 &func    = "*"
                 &funcpr  = "if {k-func6.i} or {k-func7.i} then next main.
                             else if {k-func.i} then do:
                               bell.
                               next dsply.
                             end."
                 &com     = "/*"
                 &global  = "find sasta where
                             recid(sasta) = v-recid[frame-line(f-lookup)]
                             no-lock no-error.
                             frame-value = if avail sasta then sasta.codeval
                             else ''"}.
        end.

g-errfl = no.

end.  /* main */

on f11 endkey.
  hide frame f-lookup no-pause.
status default.
