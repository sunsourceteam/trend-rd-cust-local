{g-all.i} 
def input-output      param  p-reasoncd    as char                      no-undo.
def input-output      param  p-comment     as char                      no-undo.

def var v-reason      as char format "x(2)"     label "ReasonCd"    no-undo.
def var v-descrip     as char format "x(30)"   label "Comment"      no-undo.
def var h-chandle     as widget-handle                              no-undo.
def var h-chandle2    as widget-handle                              no-undo.
def var i-inx         as integer                                    no-undo.  
def var j-inx         as integer                                    no-undo.   
def var n-inx         as integer                                    no-undo.   
def var v-commentline as character                                  no-undo.   
def var v-coffset     as integer                                    no-undo.   
def var v-cstartPos   as integer                                    no-undo.   
def var v-dummyfl     as logical                                    no-undo.          


define temp-table t-rsn no-undo 
    field rsncd   as char format "x(2)" 
    field descrip as char format "x(30)"
    index k-rsncd 
          rsncd.
define query q-rsn for t-rsn scrolling.
define browse b-rsn query q-rsn
       display t-rsn.rsn 
               t-rsn.descrip
          with 10 down centered no-labels
          title "Reason Codes". 
define frame f-rsncode
  b-rsn at row 1 col 1
  "      Up/down arrow to scroll     " dcolor 2 at 1   
  with width 55 row 7 col 24 no-hide no-labels overlay no-box.   

 
 
form
  v-reason  at 1
  v-descrip at 2 view-as editor inner-chars 40 inner-lines 5
with frame f-reason width 60 row 6 center side-labels overlay
title "Change / Cancel Reason Code".

on entry of v-descrip in frame f-reason do:
  on cursor-up cursor-up.
  on cursor-down cursor-down.
end.
on leave of v-descrip in frame f-reason do:
  on cursor-up back-tab.
  on cursor-down tab.
end.

/*
on cursor-up of v-descrip in frame f-reason do:
  h-chandle2 =  v-descrip:handle in frame f-reason.
  if h-chandle2:cursor-line = 1 and keylabel(lastkey) = "cursor-up" then do:
   apply "entry" to v-reason in frame f-reason.
  end.
end.
*/ 


on any-key of b-rsn in frame f-rsncode do:
  if keyfunction  (lastkey) = "end-error" or
     lastkey = 311 or lastkey = 313 then do:
    hide frame f-rsncode.
    apply "window-close" to current-window.
  end.   
  else 
  if keyfunction  (lastkey) = "return" or 
     keyfunction  (lastkey) = "go" then do: 
    assign v-reason = t-rsn.rsncd.
    hide frame f-rsncode.
    apply "window-close" to current-window.
  end.
end.   




run Loadreasons.

find first t-rsn no-lock no-error.
if not avail t-rsn then return.

assign v-reason  = p-reasoncd
       v-descrip = replace(p-comment,"~~","").
display
  v-reason  
  v-descrip 
with frame f-reason. 

Main:
do while true with frame f-reason on endkey undo Main, next Main:
  update 
    v-reason
    v-descrip
  with frame f-reason 
  editing:
    readkey.
    if keylabel(lastkey) = "f12" then do:
      on cursor-up cursor-up.
      on cursor-down cursor-down.
      run ReasonBrowse.
      on cursor-up back-tab.
      on cursor-down tab.
      display v-reason with frame f-reason.
      next-prompt v-reason with frame f-reason.
    end.  
    if frame-field = "v-reason" and 
    (can-do("13,9,509,501,502,21,1070,1068,505,506",string(lastkey)) or
     keyfunction(lastkey) = "go") or
    (FRAME-FIELD = "v-reason" and keylabel(lastkey) = "cursor-right" and
     v-reason:cursor-offset = 8) or   
   (FRAME-FIELD = "v-reason" and keylabel(lastkey) = "cursor-left" and
    v-reason:cursor-offset = 1) then do:
      find zsastz where
           zsastz.cono     = g-cono and 
           zsastz.codeiden = "ChangeOE" and
           zsastz.labelfl  = false and
           zsastz.primarykey = input v-reason no-lock no-error.
      if not avail zsastz then do:
        run err.p (4027).
        next.
      end.  
      else
        assign v-reason = input v-reason.
    
    end.

      if frame-field = "v-descrip" and 
      (can-do("13",string(lastkey)) or
       keyfunction(lastkey) = "go") or
      (FRAME-FIELD = "v-descrip" and keylabel(lastkey) = "cursor-left" and
       v-descrip:cursor-offset = 1) or 
       {k-accept.i} then do:
      if replace(replace(                                                                 replace(input v-descrip,"~011",""),"~012",""),"~015","") = "" or               replace(replace(                                                                 replace(input v-descrip,"~011",""),"~012",""),"~015","") begins "."             then do:                                                                      run err.p (1703).
        next-prompt v-descrip with frame f-reason.
        next.
      end.  
      else
        assign v-descrip = input v-descrip.
    end.
    if lastkey <> 404 then
      apply lastkey.
  end.  

 
  
  if lastkey = 404 or
    lastkey = 311 or lastkey = 313 then do:
      run err.p (1703).
      display v-reason
              v-descrip with frame f-reason.
      next-prompt v-descrip with frame f-reason.
      next main.
  end.
  if keyfunction  (lastkey) = "go" then do:
/* Update information */
    h-chandle =  v-descrip:handle in frame f-reason.
    assign p-reasoncd = v-reason.
    assign p-comment = "".
    assign v-cstartPos  = 1
           j-inx        = h-chandle:num-lines.
    do i-inx = 1 to j-inx:                          
      if i-inx > j-inx then
        leave.                /* no more lines */
      else do:
   /* Code to read each line how it sits in the EDITOR widget */
        assign 
          v-coffset = if i-inx = j-inx then
                        h-chandle:length
                      else
                        h-chandle:convert-to-offset ( i-inx + 1, 1 )
           
          v-dummyfl = if i-inx = j-inx and j-inx = 1 then
                        h-chandle:set-selection (v-cstartPos, v-coffset )
                      else
                        h-chandle:set-selection (v-cstartPos, v-coffset - 1)
           v-commentline = h-chandle:selection-text
           v-cstartPos = v-coffset
           v-dummyfl = h-chandle:clear-selection(). 
           if p-comment = " " then
             assign p-comment = v-commentline.
           else
             assign p-comment = p-comment + "~~" + v-commentline.
      
      end.
    end.
    leave main.
  end.


hide frame f-rsncode.
hide frame f-reason.
end.

hide frame f-rsncode.
hide frame f-reason.
on cursor-up back-tab.
on cursor-down tab.
 

procedure LoadReasons:

  for each zsastz where
           zsastz.cono     = g-cono and 
           zsastz.codeiden = "ChangeOE" and
           zsastz.labelfl  = false no-lock:
    create t-rsn.
    assign t-rsn.rsncd   = zsastz.primarykey
           t-rsn.descrip = zsastz.secondarykey.
  end.
end.         
                    

procedure ReasonBrowse:
  if lastkey  = 404 then do: 
    on cursor-up back-tab.
    on cursor-down tab.
    close query q-rsn.
    leave.
  end.
  open query q-rsn for each t-rsn.
  enable  b-rsn with frame f-rsncode. 
  apply "entry" to b-rsn in frame f-rsncode.
  apply "focus" to b-rsn in frame f-rsncode.
  display b-rsn with frame f-rsncode.
  on cursor-up cursor-up.
  on cursor-down cursor-down.
  status default 
     " Up/Down arrow to scroll..".  
  wait-for window-close of current-window. 
  on cursor-up back-tab.
  on cursor-down tab.
  

end. 

   
