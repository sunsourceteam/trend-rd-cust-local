/* g-porzc.i 1.1 01/03/98 */
/* g-porzc.i 1.6 08/20/96 */
/*h*****************************************************************************
  INCLUDE      : g-porzc.i
  DESCRIPTION  : Shared buffers and variables for porbx.p
  USED ONCE?   : NO, used more than once
  AUTHOR       : jbt
  DATE WRITTEN : 11/01/94
  CHANGES MADE :
    11/16/92 mms; TB# 8718 Rush - Add whse range & exception Rush POs<stage5
    06/08/93 enp; TB# 11544 Displays lookupnm instead of descrip[1]
    10/24/94 djp; TB# 16902 Add region to report. Added b-region and e-region.
    01/26/96 dww; TB# 14239 Performance issue when 1 vendor, 1 whse, and 1
        Product line selected
    08/20/96 kjb; TB#  4389 Add option to reprint products - added p-reprintfl
    05/24/00 hpl; TB# e5061 Insert reserve type field to temp table to allow the         data to be displayed on the report.
*******************************************************************************/

def {1} shared var p-oimsp      as c format "x(24)"             no-undo.
def {1} shared var p-prtord     as c format "x(1)"              no-undo.
def {1} shared var p-sodays     as decimal                      no-undo.
def {1} shared var p-latedays   as decimal                      no-undo.
def {1} shared var p-prtbos     as logical                      no-undo.
def {1} shared var p-prtrcpts   as logical                      no-undo.
def {1} shared var p-rushfl     as logical                      no-undo.
def {1} shared var p-dueorexp   as char format "x(1)"           no-undo.
def {1} shared var daysdiff     as int format "999-"            no-undo.
def {1} shared var p-notltbo    as logical                      no-undo.
def {1} shared var p-onlybo     as logical                      no-undo.
def {1} shared var p-faxopt     as char format "x"              no-undo.
def {1} shared var p-list       as logical                      no-undo.
def {1} shared var p-sortby     as c format "x(1)"              no-undo.
def {1} shared var p-vendbrk    as logical                      no-undo.
def {1} shared var p-reprintfl  as logical                      no-undo.
def {1} shared var p-exclshp    as logical                      no-undo.
def {1} shared var b-buyer      like sasta.codeiden             no-undo.
def {1} shared var e-buyer      like sasta.codeiden             no-undo.
def {1} shared var b-vendno     like apsv.vendno                no-undo.
def {1} shared var e-vendno     like apsv.vendno                no-undo.
def {1} shared var b-lnduedt    as date                         no-undo.
def {1} shared var e-lnduedt    as date                         no-undo.
def {1} shared var b-poentdt    as date                         no-undo.
def {1} shared var e-poentdt    as date                         no-undo.
def {1} shared var b-prodline   like icsl.prodline              no-undo.
def {1} shared var e-prodline   like icsl.prodline              no-undo.
def {1} shared var b-product    like poel.shipprod              no-undo.
def {1} shared var e-product    like poel.shipprod              no-undo.
def {1} shared var b-whse       like icsw.whse                  no-undo.
def {1} shared var e-whse       like icsw.whse                  no-undo.
def {1} shared var b-ordtype    as char format "x(2)"           no-undo.
def {1} shared var e-ordtype    as char format "x(2)"           no-undo.
def {1} shared var b-oimsp      as char format "x(24)"          no-undo.
def {1} shared var e-oimsp      as char format "x(24)"          no-undo.
/* Parameters used by reporting  through SAPB */
/*
Def {1} shared Var p-optiontype as character format "x(15)" no-undo.
Def {1} shared Var p-sorttype as character format "x(15)" no-undo.
Def {1} shared Var p-totaltype as character format "x(15)" no-undo.
Def {1} shared Var p-summtype as character format "x(15)" no-undo.
Def {1} shared Var p-summcounts as character format "x(24)" no-undo.
def {1} shared stream xpcd.
*/

def {1} shared temp-table t-porzc no-undo
        field ordtyp        like poelo.ordertype
        field pono          like poeh.pono
        field posuf         like poeh.posuf
        field lineno        like poel.lineno
        field notesfl       like poeh.notesfl
        field whse          like poeh.whse
        field shipprod      like poel.shipprod
        field buyer         like poeh.buyer
        field buyername     like sasta.descrip
        field condition     as c format "x"
        field vendno        like poeh.vendno
        field shipfmno      like poel.shipfmno
        field vendnm        like apsv.name
        field netavail      like icsw.qtyonhand
        field stkqtyord     like poel.stkqtyord
        field orderdt       like poeh.orderdt
        field duedt         like poel.duedt
        field expshipdt     like poel.expshipdt
        field lastackdt     like poel.duedt
        field rushdt        like poeh.orderdt
        field transtype     like poeh.transtype
        field nonstockty    like poel.nonstockty
        field reservety     like icsw.reservety
        field totqtyord     like poeh.totqtyord
        field totlineamt    like poeh.totlineamt
        field stagecd       like poeh.stagecd
        field lnprc         like poel.price
        field rushfl        like poeh.rushfl
        field totlns        as int 
        field atyp          as char format "x"
        field ackno	    as char format "x(10)"
        field trackno	    as char format "x(18)"
      index k-porbx is primary unique
        vendno      ascending
        shipfmno    ascending
        shipprod    ascending
        rushdt      ascending
        pono        ascending
        posuf       ascending
        lineno      ascending.



