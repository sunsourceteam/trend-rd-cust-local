/* n-oeetlp.i 1.2 05/07/98 */
/*h*****************************************************************************
  INCLUDE     : n-oeetlp.i
  DESCRIPTION : PDSC lookup of pricing records for OE line item pricing
  USED ONCE?  : no
  AUTHOR      : kmw
  DATE WRITTEN: 08/30/89
  CHANGES MADE:
 

*******************************************************************************/

/*d Find a pricing record using the appropriate key */
v-badrec = yes.

if zi-pdscfl then
  do:
  find b-pdsc where 
       b-pdsc.cono = g-cono and 
       b-pdsc.pdrecno = zi-pdscrec no-lock no-error.
  /*d Qualify that the record found is valid */
    {q-oeetlp.i "b-"}
  end.  
else            
pdsc-loop:
for each b-pdsc use-index {&index} where
         b-pdsc.cono       = g-cono      and
         b-pdsc.pdrecno   = (if zi-PDSCfl then
                                 zi-PDSCrec
                              else
                                 b-pdsc.pdrecno)  and
         b-pdsc.statustype = yes         and
         b-pdsc.levelcd    = {&levelcd}  and
         {&where}
         b-pdsc.prod       = {&prod}     and
         /*tb 2311 12-10-91 kmw; Job Pricing- specific whse then "" */
         b-pdsc.whse       = {&whse}     and
         b-pdsc.units      = {&unit}     and
         b-pdsc.startdt    <= g-today    and
         /*tb 11450 05/18/93 kmw; Enhance Pricing Performance - add enddt
                                  and ignore quote checks */
         /*d If the end date is entered and past, disqualify the record */
        (b-pdsc.enddt      >= g-today    or
         b-pdsc.enddt       = ?)         and
         /*d If "ignore quote" is selected on the F12 price screen, don't accept
             any quote records */
       ((    can-do("i,il",s-priceclty) and b-pdsc.quoteno = "") or
        (not can-do("i,il",s-priceclty))) and

         /*d We Don't use base or list to price non-catalog nonstock items */
         not (
             b-pdsc.prctype = no               and
             can-do("b,l":u, b-pdsc.priceonty) and
             s-specnstype   = "n":u            and
             not avail b-icsc)

         no-lock:
    /*d Qualify that the record found is valid */
    {q-oeetlp.i "b-"}
    if v-badrec then next  pdsc-loop.
    else             leave pdsc-loop.
end.   /* for each pdsc */

if not v-badrec then do:

    /*u User Hook */
    {n-oeetlp.z99 &user_before_price_calc = "*"}

    /*d Calculate the price/discount */
    run oeetlpa.p.

    /*u User Hook */
    {n-oeetlp.z99 &user_aft_price_calc = "*"}

    /*d If this record is a promotional record, save into promo variables */
    if v-promofl then
        assign v-promofound = yes
               v-promoprice = v-price
               v-promodisc  = s-discamt
               v-pridpdsc   = recid(b-pdsc)
               v-hit        = 6.
    else leave pricing.
end.


