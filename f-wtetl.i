
form
    s-commentfl                 at 1 no-label
    s-lineno                    at 3 label "Ln#"
      {f-help.i}
    s-nonstockty                at 7 label "N"
    s-prodinrcvfl               at 9 label "Stk"
    s-prod                      at 13
      {f-help.i}
    s-notesfl                   at 37 no-label
    s-qtyord                    at 39 label "Quantity "
    s-unit                      at 50 label "Unit"
      {f-help.i}
    s-approvety                 at 55 label "Ap"
    v-tieappr                   at 57 no-label    
    s-prodcost                  at 58 label "Cost    "
    s-speccost                  at 72 no-label
    s-proddesc                  at 13 no-label
    s-netamt                    at 66 no-label
with frame f-wtetl row 5 width 80 no-underline no-hide overlay scroll 1
     5 down