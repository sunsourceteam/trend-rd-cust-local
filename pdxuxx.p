/***************************************************************************
 PROGRAM NAME: pdxu.p
  PROJECT NO.: sun506
  DESCRIPTION: This program will update PDSC records from the Note records 
               that are changed or deleted from the PDXP screen. 
  DATE WRITTEN: 08/29/02.
  AUTHOR      : SunSource TA.  
***************************************************************************/
/*
{g-all.i} 
*/

define var g-cono like oeeh.cono no-undo init 1.
define var zx-prod  like icsp.prod                  no-undo.
define var zx-itemk as char                         no-undo.
define var zx-valid as logical               no-undo.
define var x-result             as char      format "x(24)"     no-undo.
define var x-hashlen            as integer                      no-undo.
def var chksw                   as logical init no              no-undo.
define var b-prcmult            like pdsc.prcmult[1]            no-undo.
def buffer b-notes for notes.
def var xx-xx as integer no-undo.
  notesdel:
  for each notes where
           notes.cono       = g-cono  and 
           notes.notestype  = "zz"    and 
           notes.primarykey begins "dpdsc"  
           exclusive-lock:
 xx-xx = xx-xx + 1.
 display substring(notes.primarykey,18,12) format "x(12)" xx-xx. 
   pdscdel:
   for each pdsc where
            pdsc.cono       = g-cono  and
            pdsc.statustype = true    and
            pdsc.levelcd    = 1       and
            pdsc.custno     = dec(substring(notes.primarykey,18,12)) and 
            pdsc.whse       = ""      and
            pdsc.custtype   = substring(notes.primarykey,30,8)      and
            substring(pdsc.user5,13,12)
                            =  substring(notes.primarykey,6,12)
                              /* 12 char marker 12 char vendor */
            exclusive-lock:
       assign zx-prod    = pdsc.prod
              zx-itemk   = notes.secondarykey
              zx-valid   = no.
       run zsdigrep.p
           (input g-cono,input zx-prod,input zx-itemk,input-output zx-valid).
       if not zx-valid then 
          next pdscdel. 
       else
       do:
        chksw = no.   
        b-notesdel:
        for each b-notes where
                 b-notes.cono         = g-cono       and 
                 b-notes.notestype    = "zz"         and 
                 b-notes.primarykey   = 
                   substring(notes.primarykey,2,36) 
                 exclusive-lock
                 break by b-notes.origpageno descending:
          assign 
             zx-valid = no
             zx-itemk = b-notes.secondarykey.
          run zsdigrep.p
           (input g-cono,input zx-prod,input zx-itemk,input-output zx-valid).
          if zx-valid then
           do:
            assign b-notes.requirefl = yes
                   chksw = yes.
            leave b-notesdel.
           end.
       end.
        if chksw = no then 
           assign pdsc.statustype = false.
       end. /**end do  **/
   end. /* each pdsc **/   
   delete notes.     
  end. /* for each notes */        

   
/****  change PDSC records that apply to notes.requirefl = yes ***/   

   assign xx-xx = 0.
   
   for each notes where
            notes.cono       = g-cono      and 
            notes.notestype  = "zz"        and 
            notes.primarykey begins "pdsc" and 
            notes.requirefl  = yes  
            exclusive-lock:

    xx-xx = xx-xx + 1.
    put screen row 8 string(xx-xx). 
    for each pdsc where
             pdsc.cono       = g-cono  and
             pdsc.statustype = true    and
             pdsc.levelcd    = 1       and
             pdsc.custno     = dec(substring(notes.primarykey,17,12)) and 
             pdsc.whse       = ""      and
             pdsc.custtype   = substring(notes.primarykey,29,8)      and
             pdsc.custtype   = ""      and
             substring(pdsc.user5,13,12)
                             =  substring(notes.primarykey,5,12)
                               /* 12 char marker 12 char vendor */
             exclusive-lock:
       assign zx-prod    = pdsc.prod
              zx-itemk   = notes.secondarykey
              zx-valid   = no.
       assign b-prcmult  = pdsc.prcmult[1].
       run zsdigrep.p
         (input g-cono, input zx-prod, input zx-itemk, input-output zx-valid). 

       if zx-valid = true then 
        do: 
         assign 
/* SDIMULT */
         pdsc.prcmult[1] = if dec(notes.noteln[2]) ne 0 and 
                             (dec(notes.noteln[2]) > dec(notes.noteln[1])) then
                              100 - dec(notes.noteln[2])
                           else    
                              100 - dec(notes.noteln[1]) 
         pdsc.prcdisc[1] = 0 
/*

         pdsc.prcdisc[1] = if dec(notes.noteln[2]) ne 0 and 
                             (dec(notes.noteln[2]) > dec(notes.noteln[1])) then
                              dec(notes.noteln[2])
                           else    
                              dec(notes.noteln[1]) 
 */
         pdsc.enddt      = if notes.noteln[3] = "  /  /  " then
                              ? 
                           else   
                              date(notes.noteln[3])
         pdsc.user9      = if b-prcmult <> pdsc.prcmult[1] then
                             TODAY
                           else
                             pdsc.user9.
        end.
    end. /* each pdsc */
    assign notes.requirefl = no.
   end. /* each notes */

