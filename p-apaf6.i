/* p-apaf6.i 1.1 01/03/98 */
/* p-apaf6.i 1.1 01/03/98 */
/*h*****************************************************************************
  INCLUDE      : p-apaf6.i
  DESCRIPTION  : A/P Federal 1099 Print - 1099-MISC format
  USED ONCE?   : yes
  AUTHOR       : r&d
  DATE WRITTEN :
  CHANGES MADE :
    10/22/93 kmw; TB# 12955 Replace apsv.name with s-recipnm
    01/31/94 bj ; TB# 17718 Print Both Lines of Vendor Address
    01/16/97 nlp; TB# 22339 Address and Zip codes printing outside of boxes
    07/03/01 gwk; TB# e8953 New boxes on 1099 Misc form
    01/15/03 lcb; TB# e16084 Add payer phone number
    08/30/05 gwk; TB# e21598 Account number now required    
*******************************************************************************/

/* tb #9601 3-1-93 beth */
    assign s-box1    = if apsv.fed1099box = 1 then s-amount else 0
           s-box2    = if apsv.fed1099box = 2 then s-amount else 0
           s-box3    = if apsv.fed1099box = 3 then s-amount else 0
           s-box4    = if apsv.fed1099box = 4 then s-amount else 0
           s-box5    = if apsv.fed1099box = 5 then s-amount else 0
           s-box6    = if apsv.fed1099box = 6 then s-amount else 0
           s-box7    = if apsv.fed1099box = 7 then s-amount else 0
           s-box8    = if apsv.fed1099box = 8 then s-amount else 0
           s-checkfl = if apsv.fed1099box = 9 then "X" else ""
           s-box10   = if apsv.fed1099box = 10 then s-amount else 0
           s-box13   = if apsv.fed1099box = 13 then s-amount else 0
           s-box14   = if apsv.fed1099box = 14 then s-amount else 0
           s-box15   = if apsv.fed1099box = 15 then s-amount else 0
           s-box16   = if apsv.fed1099box = 16 then s-amount else 0
           s-box18   = if apsv.fed1099box = 18 then s-amount else 0
           s-payerno = if apsv.fed1099box = 17 then s-payerno else "".

    if p-printmedia = "p" then do:
        /*tb 17718 01/31/95 bj; Print Both Vendor Address Lines */
        display v-void      when p-printtype = "v"
                v-correct   when p-printtype = "c"
                s-phoneno
                s-name          s-addr          s-city          s-state
                s-zip           s-fedtaxid      apsv.fedtaxid   s-recipnm

                apsv.addr[1] when apsv.addr[2] = "" @ apsv.addr[2]
                apsv.addr[1] when apsv.addr[2] ne "" @ apsv.addr[1]
                apsv.addr[2] when apsv.addr[2] ne "" @ apsv.addr[2]

                apsv.city       apsv.state      apsv.zip
                
                apsv.vendno
                
                s-box1      when apsv.fed1099box = 1
                s-box2      when apsv.fed1099box = 2
                s-box3      when apsv.fed1099box = 3
                s-box4      when apsv.fed1099box = 4
                s-box5      when apsv.fed1099box = 5
                s-box6      when apsv.fed1099box = 6
                s-box7      when apsv.fed1099box = 7
                s-box8      when apsv.fed1099box = 8
                s-checkfl   when apsv.fed1099box = 9
                s-box10     when apsv.fed1099box = 10
                s-payerno   when apsv.fed1099box = 17                
                s-box13     when apsv.fed1099box = 13
                s-box14     when apsv.fed1099box = 14
                s-box15     when apsv.fed1099box = 15
                s-box16     when apsv.fed1099box = 16
                s-box18     when apsv.fed1099box = 18
                with frame f-apaf6.
        down with frame f-apaf6.
    end.

