/* p-zpoque.i */
/* ------------------------------------------------------------------------
  Program Name :  P-ZPOQUE.I

  Description  :  Display module for POEZQ using xxixx.i

  Author       :  Thomas Herzog


------------------------------------------------------------------------- */

assign g-pono = dec(substring(o-frame-value,2,7))
       g-posuf = int(substring(o-frame-value,10,2))
       g-polineno = int(substring(o-frame-value,13,3)).
      
        
find poel                   where poel.cono = g-cono and
                                  poel.pono = g-pono and
                                  poel.posuf = g-posuf and
                                  poel.lineno = g-polineno
                                  no-lock no-error.

if avail poel then
  do:
  find icsp where icsp.cono = 1 and 
                  icsp.prod = poel.shipprod no-lock no-error.
  if avail icsp then
    do:
/*    message "1". pause.        */
    g-prod = icsp.prod.
    end.
/*  else
    do:
    message "2". pause.
    end.      */

  find poeh where poeh.cono = g-cono and
                  poeh.pono = g-pono and
                  poeh.posuf = g-posuf no-lock no-error.
  
  find first poelo use-index k-poelo
             where poelo.cono = g-cono    and
                   poelo.pono = g-pono    and
                   poelo.posuf = g-posuf  and
                   poelo.lineno = g-polineno
                   no-lock no-error.

  
  if avail poelo then
    do:
    assign g-orderno = poelo.orderaltno
           g-ordersuf = poelo.orderaltsuf.
  
  
    find oeeh where oeeh.cono = g-cono and
                    oeeh.orderno = g-orderno and
                    oeeh.ordersuf = g-ordersuf no-lock no-error.
    end.
   
  end.
