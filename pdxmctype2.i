/*****************************************************************************
   
 pdxmctype2.i  -  Contract Price Matrix Reporting - 
                  This include is called by pdxmc.p.
                  Prints the report
                
*****************************************************************************/
  assign wk-ytd-sales  = 0
         wk-pytd-sales = 0.
           
  
  for each matrix use-index k-type2  where matrix.customer <> 0 and
                                           /*
                                           matrix.type-cat-line <> "" and
                                           */
                                           matrix.ytd-sales <> 0
                                           no-lock
                                              
      break by matrix.region
            by (if substring(e-region,2,3) = "999" then matrix.blank-sort
                else matrix.district)
            by matrix.ytd-sales descending
            by matrix.customer
            by matrix.ship-to
            by matrix.type-cat-line:
            
      /*
      if matrix.c-sales = 0 or
         matrix.i-sales = 0 then next.
      */
         
      assign i-mgn = 0
             c-mgn = 0.
      
      if (first-of(matrix.region)) and not first-page then PAGE.
      if (first-of(if substring(e-region,2,3) = "999" then matrix.blank-sort
          else matrix.district)) then do:
          if not first-page then PAGE.
          else
             assign first-page = no.
       end.

      if (matrix.i-sales - matrix.i-disc) <> 0 then
         assign i-mgn =                 /*  (net-sales - cost) / net-sales)  */
            (((matrix.i-sales - matrix.i-disc) - matrix.i-cost)
                                     / (matrix.i-sales - matrix.i-disc)) * 100

                w-tot-isales = w-tot-isales + (matrix.i-sales - matrix.i-disc)
                w-isales     = w-isales + (matrix.i-sales - matrix.i-disc)
                w-icost      = w-icost  + matrix.i-cost
                w-tot-icost  = w-tot-icost  + matrix.i-cost.

      else
         assign i-mgn = 0.
      
      if (matrix.c-sales - matrix.c-disc) <> 0 then do:
         assign c-mgn =        /* c-sales already has discount considered */
                (matrix.c-sales - matrix.c-cost) / matrix.c-sales * 100

                w-tot-csales = w-tot-csales + matrix.c-sales
                w-csales     = w-csales + matrix.c-sales
                w-ccost      = w-ccost  + matrix.c-cost
                w-tot-ccost  = w-tot-ccost  + matrix.c-cost.
      end.
      else
         assign c-mgn = 0.

      assign w-multiplier = 0
             w-price-inc  = 0.
             
      if matrix.mult-prc <> 0 and matrix.g-mult-prc <> 0 then
         assign w-multiplier = matrix.mult-prc - matrix.g-mult-prc
                w-price-inc = ((w-multiplier / matrix.g-mult-prc) * -1) * 100.

      if matrix.mult-prc =  0 and matrix.g-mult-prc <> 0 then
         assign w-multiplier = (100 - matrix.g-mult-prc)
                w-price-inc  = matrix.g-mult-prc.
         
      if matrix.mult-prc <> 0 and matrix.g-mult-prc  = 0 then
         assign w-multiplier = (matrix.mult-prc - 100)
                w-price-inc  = (matrix.mult-prc * -1).

      if (last-of(matrix.customer)) then
          assign wk-ytd-sales  = wk-ytd-sales  + matrix.ytd-sales
                 wk-pytd-sales = wk-pytd-sales + matrix.pytd-sales.
                 
      if (last-of(matrix.type-cat-line)) then
                 wk-prod-ytd-sales = wk-prod-ytd-sales + matrix.prod-ytd-sales.

      assign s-price-desc = "".
      find sasta use-index k-sasta where sasta.cono = 1
                                     and sasta.codeiden = "k"
                                     and sasta.codeval  = matrix.pprice-type
                                         no-lock no-error.
      if avail sasta then
         assign s-price-desc = substring(sasta.descrip,1,14).
      else
         assign s-price-desc = "description n/a".

/*
      if matrix.customer <> wk-customer or counter > 32 then do:
         assign counter = 1.
         assign wk-customer = matrix.customer.
*/
         display matrix.customer
                 matrix.ship-to
                 matrix.name
                 matrix.salesrep
                 matrix.pprice-type
                 s-price-desc
                 matrix.ytd-sales
                 matrix.pytd-sales
                 matrix.prod-ytd-sales
                 matrix.type-cat-line
                 matrix.type-ind
                 matrix.pd-record
                 matrix.upd-date
                 matrix.mult-prc
                 matrix.c-percent
                 matrix.g-mult-prc
                 matrix.g-percent
                 i-mgn
                 c-mgn
                 with frame f-type2-detail.
                 down with frame f-type2-detail.
/*
      end.
      else do:
         display matrix.pprice-type
                 s-price-desc
                 prod-ytd-sales
                 matrix.type-cat-line
                 matrix.type-ind
                 matrix.pd-record
                 matrix.upd-date
                 matrix.mult-prc
                 matrix.c-percent
                 matrix.g-mult-prc
                 matrix.g-percent
                 i-mgn
                 c-mgn
                 with frame f-type2-detail.
                 down with frame f-type2-detail.
                 assign counter = counter + 1.
      end.
*/

      if (last-of(if substring(e-region,2,3) = "999" then matrix.blank-sort
                  else matrix.district)) then do:
                  
         if substring(e-region,2,3) = "999" then do:
            if substring(e-region,1,1) = "n" then
               assign wk-rgn = "Nrth".
            if substring(e-region,1,1) = "f" then
               assign wk-rgn = "Filt".
            if substring(e-region,1,1) = "s" then
               assign wk-rgn = " Sth".
            if substring(e-region,1,1) = "m" then
               assign wk-rgn = "Mobl".
            if substring(e-region,1,1) = "p" then
               assign wk-rgn = " Pab".
            if substring(e-region,1,1) = "r" then
               assign wk-rgn = "Srvc".
         end.
         else
            assign  substring(wk-rgn,1,1)   = matrix.region
                    substring(wk-rgn,2,3)   = matrix.district.
                    
         assign  wk-i-mrg = (w-isales - w-icost) / w-isales * 100
                 wk-c-mrg = (w-csales - w-ccost) / w-csales * 100.


         display wk-rgn
                 wk-ytd-sales 
                 wk-pytd-sales
                 wk-prod-ytd-sales
                 wk-i-mrg
                 wk-c-mrg
                 with frame f-2-totals.
                 down with frame f-2-totals.
         assign  wk-grand-ytd-sales  = wk-grand-ytd-sales  + wk-ytd-sales
                 wk-grand-pytd-sales = wk-grand-pytd-sales + wk-pytd-sales
                 wk-grand-prod-ytd-sales = wk-grand-prod-ytd-sales +
                                           wk-prod-ytd-sales.
         assign  wk-ytd-sales = 0
                 wk-pytd-sales = 0
                 wk-prod-ytd-sales = 0
                 wk-i-mrg = 0
                 wk-c-mrg = 0
                 w-isales = 0
                 w-csales = 0
                 w-icost  = 0
                 w-ccost  = 0
                 wk-rgn   = ""
                 counter = 0.
         page.
      end.
      

  end.   /* for each matrix */    


  assign wk-i-mrg = ((w-tot-isales - w-tot-icost) / w-tot-isales) * 100
         wk-c-mrg = ((w-tot-csales - w-tot-ccost) / w-tot-csales) * 100.
           

  display wk-grand-ytd-sales
          wk-grand-pytd-sales
          wk-grand-prod-ytd-sales
          wk-i-mrg
          wk-c-mrg
        with frame f-2-grand.
        down with frame f-2-grand.
                                
