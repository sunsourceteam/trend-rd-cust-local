 
/{&user_temptable}*/
/* Put temp-table and field definitions here */

{zsapboydef.i} 

DEFINE VARIABLE qbf-i AS INTEGER NO-UNDO.             
DEFINE VARIABLE qbf-t AS INTEGER NO-UNDO.             
DEFINE SHARED VARIABLE qbf-total AS INTEGER NO-UNDO.  


Def var p-list      as logical. 
def var pk_good    as logical.                   
def var p-detailprt   as logical.                                              def var p-regval      as c.
def var p-format      as int.

         
  def var s-bwhse  as char format "x(4)".  
  def var s-ewhse  as char format "x(4)".               
  def var s-bdate  as date.                             
  def var s-edate  as date.                             
  def var s-bregion as char format "x(8)".              
  def var s-eregion as char format "x(8)".              
  def var s-bdist  as char format "x(4)".               
  def var s-edist  as char format "x(4)".               
                                                        
                                                        
Define temp-table holiday no-undo
  Field h-whse            as char format "x(4)"  
  Field h-manager         as char format "x(4)"
  Field h-custno          as integer format "zzzzzzzzzzzzz9"
  Field h-name            as char format "x(30)"
  Field h-orderno         as integer format "zzzzzz9"
  Field h-ordersuf        as integer format "99"
  Field h-orderdisp       as char format "x(1)"
  Field h-approvty        as char format "x(1)"
  Field h-totlineord      as dec format "zzzzzzzzz9.99-"
  Field h-totlineamt      as dec format "zzzzzzzzz9.99-"
  Field h-reqshipdt       as date
  
  index k-custno
        h-custno
        h-orderno.


/{&user_temptable}*/

/{&user_drpttable}*/
/* Use to add fields to the report table if needed */
 field holidayid as recid

/{&user_drpttable}*/

/{&user_forms}*/
/* Place form definitions here */

form
"~015"         at 1
  with frame f-skip width 178 no-box no-labels.
  
  Define frame f-holiday-detail
  holiday.h-whse              at 1    label "Whse"  
  holiday.h-manager           at 6    label "Reg/Dist."
  holiday.h-custno            at 15   label "Customer #"
  holiday.h-name              at 31   label "Cust. Name"
  holiday.h-orderno           at 62   label "Order #"
  holiday.h-ordersuf          at 72   label "Suffix"
  holiday.h-orderdisp         at 79   label "Disp."
  holiday.h-approvty          at 85   label "A.Type"
  holiday.h-totlineord        at 93   label "Line Total"
  holiday.h-totlineamt        at 107   label "Line Amt."
  holiday.h-reqshipdt         at 124   label "ReqShipdt"
  with down width 178 no-box no-labels.

/{&user_forms}*/

/{&user_extractrun}*/
/* This is a place to load SAPB ranges and options and extract temp-table */

 run sapb_vars.
 run extract_data. 
/{&user_extractrun}*/

/{&user_exportstatheaders}*/
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
 
  assign export_rec =
         export_rec   +  " ".
         
  if p-detailprt then
    assign export_rec = export_rec + v-del .

/{&user_exportstatheaders}*/

/{&user_exportstatheaders1}*/
  assign export_rec = "". 
/{&user_exportstatheaders1}*/

/{&user_foreach}*/
/* must have an iterating block here - a label is in zsdixrptx.p */
   for each holiday no-lock: 

/{&user_foreach}*/

/{&user_drptassign}*/
 /* If you add fields to the dprt table this is where you assign them */
      drpt.holidayid = recid(holiday)
/{&user_drptassign}*/

/{&user_viewheadframes}*/
/* Since frames are unique to each program you need to put your
   views and hides here with your names */

if not p-exportl then do:
 if not x-stream then do:
  hide frame f-trn1.
  hide frame f-trn3.
 end.
 else do:
  hide stream xpcd frame fx-trn1.
  hide stream xpcd frame fx-trn3.
 end.
end.


if not p-exportl  then do:
 if not x-stream then do:
  page.
  view frame f-trn1.
  view frame f-trn3.
 end.
 else do:
  page stream xpcd.
  view stream xpcd frame fx-trn1.
  view stream xpcd frame fx-trn3.
 end.
end.
else do: 
 page.
 view frame f-trn1.
 view frame f-trn3.
end.


/{&user_viewheadframes}*/

/{&user_drptloop}*/

/{&user_drptloop}*/


/{&user_detailprint}*/
   /* Comment this out if you are not using detail 'X' option  */

   if p-detailprt and                        
      entry(u-entitys,drpt.sortmystery,v-delim) <> 
      fill("~~",length(entry(u-entitys,drpt.sortmystery,v-delim))) then do: 
    /* this code skips the all other lines for printing detail */
    /* next line would be your detail print procedure */
     run print-dtl(input drpt.holidayid).
   end.


/{&user_detailprint}*/

/{&user_finalprint}*/
/{&user_finalprint}*/

/{&user_summaryframeprint}*/
/{&user_summaryframeprint}*/

/{&user_summaryputexport}*/
/{&user_summaryputexport}*/

/{&user_procedures}*/

procedure sapb_vars:

/* set Ranges */

/* set Options */

assign                               
 s-bwhse  = sapb.rangebeg[1]         
 s-ewhse  = sapb.rangeend[1]         
 s-bdate  = date(sapb.rangebeg[2])   
 s-edate  = date(sapb.rangeend[2])   
 s-bregion = sapb.rangebeg[3]        
 s-eregion = sapb.rangeend[3]
 p-format  = int(sapb.optvalue[1])
 p-list    = if sapb.optvalue[2] = "yes" then yes
             else no.


assign zel_begwhse = s-bwhse
       zel_endwhse = s-ewhse 
       zzl_begwhse = s-bwhse
       zzl_endwhse = s-ewhse.
        
if p-list then do:
  {zsapboyload.i}
end.  

/* Zelection_matrix has to have an extent for each selection type 
   right now we have. During the load these booleans are set showing what
   options have been selected.
   
   s - sales rep               ....Index 1
   c - customer number         ....Index 2
   p - product cat             ....Index 3
   b - buyer                   ....Index 4
   t - Order types             ....Index 5
   w - Warehouse               ....Index 6
   v - Vendor number           ....Index 7
   N - Vendor Name             ....Index 8
   
 PLEASE KEEP THIS DOCUMENTATION UPDATED WHEN CHANGED 
 
*/ 
  
assign zelection_matrix [1] = (if zelection_matrix[1] > 1 then /* Rep   */
                                 zelection_matrix[1]
                              else   
                                 1)
       zelection_matrix [2] = (if zelection_matrix[2] > 1 then  /* Cust */
                                 zelection_matrix[2]
                              else   
                                 1)
       zelection_matrix [3] = (if zelection_matrix[3] > 1 then  /* Cat  */
                                 zelection_matrix[3]
                              else   
                                 1)
       zelection_matrix [4] = (if zelection_matrix[4] > 1 then  /* Buyer  */
                                 zelection_matrix[4]
                              else   
                                 1)
       zelection_matrix [6] = (if zelection_matrix[6] > 1 then  /* Whse */
                                 zelection_matrix[6]
                              else   
                                 1)
       zelection_matrix [7] = (if zelection_matrix[7] > 1 then  /* Vendor */
                                 zelection_matrix[7]
                              else   
                                 1)
                                 
       zelection_matrix [8] = (if zelection_matrix[8] > 1 then  /* VName */
                                 zelection_matrix[8]
                              else   
                                 1).
                    
 



assign p-detailprt = yes
       p-detail    = "d"      /* TAH */
       p-portrait  = no. 


 run formatoptions.


end.


/* ------------------------------------------------------------------------- */
procedure formatoptions:
/* ------------------------------------------------------------------------- */
   
  assign p-portrait = false.
  /* if false then the report will print in LANDSCAPE 178 character */
  
  if sapb.optvalue[1] > "0" and sapb.optvalue[1] ne "99" then do: 
   p-optiontype = " ".

   find notes where notes.cono = g-cono and
                    notes.notestype = "zz" and
                    notes.primarykey = "oerxr" and  /* tbxr program */
                    notes.secondarykey = string(p-format) no-lock no-error.
   if not avail notes then do: 
    display "Format is not valid cannot process request".
    assign p-optiontype = "o"  
            /* TAH if nothing is selected default to order number sort */
           p-sorttype   = ">,"
           p-totaltype  = "l"
           p-summcounts = "a".
    return.
   end.               
   
   assign p-optiontype = notes.noteln[1]
          p-sorttype   = notes.noteln[2]
          p-totaltype  = notes.noteln[3]
          p-summcounts = notes.noteln[4]
          p-export     = "    "
          p-register   = notes.noteln[5]
          p-registerex = notes.noteln[6].
  end.     
  else
  if sapb.optvalue[1] = "99" then do: 
   assign p-register   = ""
          p-registerex = "".
   run reportopts(input sapb.user5, 
                  input-output p-optiontype,
                  input-output p-sorttype,  
                  input-output p-totaltype,
                  input-output p-summcounts,
                  input-output p-register,
                  input-output p-regval).
  end.

  run print_reportopts(input recid(sapb), 
                       input g-cono).

end. /* procedure */

procedure extract_data:


 FOR EACH nxt.icsd WHERE nxt.icsd.cono = 1 AND                         
                         nxt.icsd.salesfl = yes                        
                         NO-LOCK,                                      
     EACH nxt.oeeh OF nxt.icsd WHERE nxt.oeeh.cono = 1 AND               
                                        (oeeh.whse >= zel_begwhse and        
                                         oeeh.whse <= zel_endwhse) and       
                                         oeeh.stagecd = 1 and            
                                         oeeh.transtype <> "RM" and      
                                        (oeeh.reqshipdt >= s-bdate and   
                                         oeeh.reqshipdt <= s-edate)      
                                         NO-LOCK,                        
       EACH nxt.smsn WHERE (nxt.oeeh.cono = nxt.smsn.cono and              
                       nxt.oeeh.slsrepout = nxt.smsn.slsrep) AND           
                                           (nxt.smsn.cono = 1) and                                                         (nxt.smsn.mgr >= s-bregion and                                                   nxt.smsn.mgr <= s-eregion)                                                     NO-LOCK,                                        
          EACH nxt.arsc OF nxt.oeeh WHERE nxt.arsc.cono = 1 NO-LOCK:          

              pk_good = true.                   
              zelection_type = "w".             
              zelection_char4 = oeeh.whse. 
              zelection_cust = 0.               
              run zelectioncheck.               
              if zelection_good = false then    
                do:                             
                pk_good = false.                
                next.                  
                end.                        
         create holiday.
         assign holiday.h-whse          = oeeh.whse
                holiday.h-manager       = smsn.mgr
                holiday.h-custno        = arsc.custno
                holiday.h-name          = arsc.name
                holiday.h-orderno       = oeeh.orderno
                holiday.h-ordersuf      = oeeh.ordersuf
                holiday.h-orderdisp     = oeeh.orderdisp
                holiday.h-approvty      = oeeh.approvty
                holiday.h-totlineord    = oeeh.totlineord
                holiday.h-totlineamt    = oeeh.totlineamt
                holiday.h-reqshipdt     = oeeh.reqshipdt.
       end. /*for each */       
            
end. /* big loop */



/*---------------------------------------------------------------------- */
procedure print-dtl:                                                    
/*---------------------------------------------------------------------- */
 define input parameter ip-recid as recid no-undo.                     
find holiday where recid(holiday) = ip-recid no-lock no-error.          
                                                                        
     display
        holiday.h-whse
        holiday.h-manager    
        holiday.h-custno     
        holiday.h-name       
        holiday.h-orderno    
        holiday.h-ordersuf   
        holiday.h-orderdisp  
        holiday.h-approvty   
        holiday.h-totlineord 
        holiday.h-totlineamt 
        holiday.h-reqshipdt  
                 with frame f-holiday-detail.                                                              down with frame f-holiday-detail.                     
 end.                                                                    
                                                                         
                                                                         

{zsapboycheck.i} 

/{&user_procedures}*/


/* TBXR F6 CodeGen Includes from here down are CodeGen Generated  */ 

/{&user_optiontype}*/
/{&user_optiontype}*/
   
/{&user_registertype}*/
/{&user_registertype}*/
 
/{&user_exportvarheaders1}*/
/{&user_exportvarheaders1}*/

/{&user_exportvarheaders2}*/
/{&user_exportvarheaders2}*/

/{&user_loadtokens}*/
/{&user_loadtokens}*/

/{&user_totaladd}*/
/{&user_totaladd}*/

/{&user_numbertotals}*/
/{&user_numbertotals}*/

/{&user_summaryloadprint}*/
/{&user_summaryloadprint}*/

/{&user_summaryloadexport}*/
/{&user_summaryloadexport}*/


/* TBXR F6 CodeGen End of Includes that are CodeGen Generated  */ 






