/* n-oeizl6.i 1.1 01/03/98 */
/* n-oeizl6.i 1.3 09/22/97 */
/*h*****************************************************************************
  SX 55
  INCLUDE      : n-oeizl6.i
  DESCRIPTION  : Find next/prev for OIEZL where v-mode = 9
  USED ONCE?   : no
  AUTHOR       : rhl
  DATE WRITTEN :
  CHANGES MADE :
    12/09/93 mwb; TB# 13733 Preload orders in report record
    05/09/95 mtt; TB#  5180 When looking for a product-need kit ln
    09/22/97 kjb; TB# 22361 Quantity/Value displayed is incorrect when 'B' is
        entered for the order type
*******************************************************************************/

/*e*****************************************************************************
     Selection Criteria for this procedure:
       v-mode ==> 2:
               Product: entered on the screen
    Kit Component Type: "b" (Both OE orders and Kit Components)
             Customer#: <blank> (0)
               CustPO#: <blank>
       v-mode ==> 4:
               Product: entered on the screen
    Kit Component Type: "b" (Both OE orders and Kit Components)
             Customer#: entered on the screen
               CustPO#: <blank>
       v-mode ==> 9:
               Product: <blank>
             Customer#: <blank> (0)
               CustPO#: <blank>
      Stage Code Range: entered on the screen
*******************************************************************************/

/*tb 22361 09/22/97 kjb; Initialize v-netamt and v-uselnfl each time */
assign
    confirm   = yes
    v-netamt  = 0
    v-uselnfl = no.

find {1} report use-index k-c20 where
         report.cono     = g-cono     and
         report.oper2    = g-operinit and
         report.reportnm = g-ourproc
no-lock no-error.

if avail report then do:

    /*tb 22361 09/22/97 kjb; If the Quantity/Value was taken from the lines,
        then load the values stored in the REPORT record into local variables
        that will be used by the display include. */
    assign
        v-netamt  = report.de9d2s
        v-uselnfl = report.l.
   if report.c20 begins "O" then
      do:
      {w-oeeh.i report.i7 report.i2 no-lock}.
      if not avail oeeh then confirm = no.
      v-xmode = "O".
      end.

   else
   if report.c20 begins "B" then
      do:
      find oeehb where oeehb.cono = g-cono and 
                       oeehb.batchnm = string(report.i8,">>>>>>>9") and
                       oeehb.seqno =  2 no-lock no-error.
      if not avail oeehb then confirm = no.
      v-xmode = "C".
      end.

end.
else confirm = no.

