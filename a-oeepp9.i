/* a-oeepp1.i 1.4 8/10/92 */
/*h*****************************************************************************
  INCLUDE      : a-oeepp1.i
  DESCRIPTION  : OE print
  USED ONCE?   : no
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    08/03/92 mkb; TB# 7326 Remove amount(net) field
    06/11/96 jms; TB# 21254 (10A) Develop Value Add Module
                    Changed a-lit1b, a-lit3a, a-lit3b to include "va" info.
*******************************************************************************/

/*d Assign literal values for labels on pick ticket */
if {2} = yes then assign
    {1}lit1a  = "Document:"
    {1}lit1b  = "UPC Vendor  " + (if sapb.currproc = "wtep" then "Transfer #"
                 else if sapb.currproc = "vaepp" then "    VA Order #    Sq #"
                 else "   Order #")
    {1}lit3a  = if sapb.currproc ne "wtep" then "Cust #:" 
                else if sapb.currproc = "vaepp" then "Vend #:"
                else ""
    {1}lit3b  = (if sapb.currproc = "wtep" and s-custpo = "" then 
                   "Order Dt     "
                else if sapb.currproc = "vaepp" then "Order Dt     "
                else "PO Date   PO #") +
                "                   Page #"
    {1}lit6a  = "Bill To:"
   /* {1}lit6b  = "Correspondence To:" */
    {1}lit11a = "Ship To:"
    {1}lit11b = "Instructions"
    {1}lit11c = "Staging Area"
    {1}lit12a = "Ship Point"
    {1}lit12b = "Via"
    {1}lit12c = "Shipped"
    {1}lit12d = "Terms"
    {1}lit12e = "Request"
    {1}lit14a =
"Line         Product             UPC      Bin         Quantity     Quantity   "
    {1}lit14b = "  Quantity   Qty                #" +
                if g-ourproc <> "wtep" then  "          Amount"
                else ""
    {1}lit15a =
" #       And Description        Item#   Location      Ordered        B.O.     "
    {1}lit15b = "  Shipped    UM    Received   Cartons" +
                if g-ourproc <> "wtep" then "      (Net)"
                else ""
    {1}lit16a =
"------------------------------------------------------------------------------"
    {1}lit16b =
"--------------------------------------------------"
    {1}lit40a = "Lines Total"
    {1}lit40b = " ** # of Lines Not Printed "
    {1}lit40c = " Qty Shipped Total"
    {1}lit40d = "Total __________ _____"
    {1}lit41a =
"Picked By:    Packed By:     Checked By:      Cube:        Weight:      Freigh"
    {1}lit41b = "t Charges:"
    {1}lit42a = "Continued"
    {1}lit42b = "Received By:       Date Received:".
