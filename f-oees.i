/* f-oees.i 1.1 01/03/98 */
/* f-oees.i 1.2 03/30/93 */
/*h*****************************************************************************
  INCLUDE      : f-oees.i
  DESCRIPTION  : Form for oees.p
  USED ONCE?   : No (oees.p, oeesi.p & oeesl.p)
  AUTHOR       :
  DATE WRITTEN : 02/17/93
  CHANGES MADE :
    02/17/96 rhl; TB#  5868 Suffix lookup indicator is missing [L]
    03/20/06 kr;  TB# 12406 Variables defined in form, moved out of form
    05/10/99 lbr; TB# 5366  Added shipto notes
    05/31/03 lcb; TB# t14244 8Digit Ord No Project
    08/12/03 mms; TB# e17778 8 Digit OrderNo-moved notes flag to right one.
*******************************************************************************/

form
    "Order#    Type   Customer#    Ship To  Whse  PO" at  3
    arsc.lookupnm                                     at 63
    g-orderno                                         at  1
        {f-help.i}
    "-"                                               at  9
    g-ordersuf                                        at  10
        {f-help.i}
    oeeh.notesfl                                      at 12
    g-oetype                                          at 14
    g-custno                                          at 19
    arsc.notesfl                                      at 31
    g-shipto                                          at 33
    arss.notesfl                                      at 41
    g-whse                                            at 42
    s-custpo                                          at 48
    s-blbofl                                          at 71
    s-stagecd                                         at 74
with frame f-oees title g-title row 1 width 80 no-labels side-labels overlay.
