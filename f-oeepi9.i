/*sx5.5*/
/* f-oeepi9.i 5/27/97 */
/*si**** Custom ****************************************************************
CLIENT  : 80281 SDI
JOB     : sdi009 Custom invoice print
AUTHOR  : msk
VERSION : 7
CHANGES :
    si01 5/27/97 msk; 80281/sdi009; initial changes
        - remove UPC from detail line

*******************************************************************************/

/* f-oeepi9.i 1.4 4/15/93 */
/*h*****************************************************************************
  INCLUDE      : f-oeepi9.i
  DESCRIPTION  : Forms and Defines for Printing Invoices
  USED ONCE?   : oeepi9.p, oeepif1.p
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    04/06/92 pap; TB# 6238 Line DO - add direct order indicator
    04/15/93 jlc; TB# 10156 - Print customer product
    11/06/95 gp;  TB# 18091 Expand UPC Number (T10).  Add s-upcpno.
*******************************************************************************/


{f-blank.i}

form
    s-lineno                             at 1
    oeel.shipprod                        at 5
/*    s-upcpno                             at 30            si01 */
    s-qtyord                             at 38
    s-qtybo                              at 51
    s-qtyship                            at 64
    oeel.unit                            at 76
    s-price                              at 81
    s-prcunit                            at 96
    s-discpct                            at 102
    s-netamt                             at 117
    s-sign                               at 130 skip
    s-descrip                            at 5
with frame f-oeel no-box down no-labels no-underline width 132.

/*sx55*/
/*Form to display additional Comments*/
form
    s-descrip            at 8
with frame f-comments no-box down no-labels no-underline.

form  /* form to display core charge  */
    oeel.shipprod                        at 5
    s-price                              at 81
    s-netamt                             at 117
    s-sign                               at 130
with frame f-oeel1 no-box no-labels no-underline down width 132.

/*tb 10156 04/15/93 jlc; Print customer product */
form
    v-reqtitle              at 5
    s-prod                  at 23
with frame f-oeelc no-box no-labels width 132.

/** 4/6/92 pap  TB# 6238 Line DO **/
form
    "** DIRECT ORDER **"    at 8
with frame f-oeeldo no-box no-labels width 132.
/** 4/6/92 pap  **/

form
    "** CORE RETURN **"   at 8
with frame f-oeelr no-box side-labels width 132.

form
    "Kit"                 at 1
    oeelk.instructions    at 5
with frame f-refer no-box no-labels width 132.

form
    "Serial #:"               at 8
    s-serialno[1]             at 18
    s-serialno[2]             at 42
    s-serialno[3]             at 66
    s-serialno[4]             at 91
    s-serialno[5]             at 115
with frame f-serial no-box no-labels width 138.

form
    s-lot1d  as c format "x(6)"   at 8
    s-lot1   like icetl.lotno     at 15
    s-qty1d  as c format "x(4)"   at 37
    s-qty1   like icetl.qtycosted at 42
    s-lot2d  as c format "x(6)"   at 59
    s-lot2   like icetl.lotno     at 66
    s-qty2d  as c format "x(4)"   at 88
    s-qty2   like icetl.qtycosted at 93
with frame f-lot no-box no-labels width 132.

form
with frame f-com no-box no-labels col 8 width 80.

form
with frame f-notes no-box no-labels col 8 width 80.

form
    "============="                      at 117
    "Subtotal:"                          at 107
    v-subnetdisp  as c format "x(13)"    at 117 skip(1)
with frame f-oeelsub no-box no-labels no-underline width 132.
