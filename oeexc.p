/******************************************************************************
  PROCEDURE     : oeexc.p
  DESCRIPTION   : Custom Commission Quote Entry
  AUTHOR        : SunSource
  DATE WRITTEN  : 02/02/15
  CHANGES MADE  : 

******************************************************************************/
{g-all.i}
{g-ap.i}
{g-ar.i}
{g-oe.i}
{g-ic.i}
{g-jump.i}
{g-oeipv.i &new = new}


def var x-monit as character format "x(20)" no-undo.
def new shared var zi-displaylinefl as logical                   no-undo.
def new shared var zi-icspecrecno  like oeel.icspecrecno         no-undo.


define var   ip-orderno like oeeh.orderno                              no-undo.
define var   ip-update  as logical                                     no-undo.

def new shared var v-start         like icsp.lookupnm      no-undo.
def new shared var v-save          like icsp.lookupnm      no-undo.
def new shared var s-words         as c extent 5           no-undo.

def var kitfl as logical format "x(1)" init no.
def var kqty as de format "9999.99" init 0.

/* smaf */
def var exceptCode    as char format "x(10)"   no-undo.          
def var v-comment     as char format "x(20)"   no-undo.
def var v-flag        as logical               no-undo.          
def buffer m-oeehb for oeehb.
def buffer c-oeelb for oeelb.
def var old-outbndfrtfl like oeehb.outbndfrtfl no-undo.
def var old-inbndfrtfl like oeehb.outbndfrtfl no-undo.

def var w-shipchgfl   as logical       init no no-undo. /*not used but needed
                                                          since OEEXQ and OEEXC
                                                          share shiptopopup.p*/

def new shared var v-optprod       like oeel.shipprod     no-undo.
def new shared var v-idoeeh0       as recid               no-undo. 
def new shared var v-idoeeh        as recid               no-undo. 
def new shared var v-idbpeh        as recid               no-undo.
def new shared var v-idbpel        as recid               no-undo.
def new shared var v-nsokfl        as logical init no     no-undo.
def            var v-bodexists     as logical init no     no-undo.
def new shared var v-idicsp2       as recid               no-undo.
/*def new shared var v-stkqtyship    like oeel.stkqtyship   no-undo.*/
def            var w-batchnm       as i format ">>>>>>>9" no-undo. 
def            var x-quoteno       as c format "x(8)"     no-undo. 
def            var v-secure        as i format "9"        no-undo. 
def            var v-reqshipdt     like oeehb.reqshipdt   no-undo. 
def            var p-reqshipdt     like oeehb.reqshipdt   no-undo.  
def            var p-promisedt     like oeehb.promisedt   no-undo. 
def new shared var v-reqprod       like icsw.prod         no-undo.  
def            var v-xrefty        like oeel.xrefprodty   no-undo.
def new shared var orig-price      like icsw.listprice    no-undo.
def            var o-slsrepout     like oeel.slsrepout    no-undo.
def            var o-slsrepin      like oeel.slsrepin     no-undo.
def            var v-shipfl        as logical             no-undo. 
def new shared var v-listfl        as c format "x(1)"     no-undo.
def            var de-fl           as logical init no     no-undo.
def            var v-sellmultfl    as logical init no     no-undo.
def new shared var v-addontype   like sastn.addontype extent 9 init yes no-undo.
def            var x-addonno       as integer             no-undo.
def            var x-statecd       like arsc.statecd      no-undo.
def            var i-inx           as integer             no-undo.  
def            var j-inx           as integer             no-undo.  
def            var n-inx           as integer             no-undo.  
define         var v-commentline             as character no-undo.
define         var v-coffset                 as integer   no-undo.
define         var v-cstartPos               as integer   no-undo.
define         var v-dummyfl                 as logical   no-undo.
define         var h-chandle                 as widget-handle    no-undo.
define         var kpsk-line       like kpsk.seqno        no-undo.
define         var h-surcharge     like icsw.stndcost     no-undo.
/* v-chgsrchgfl - needed to determine if surcharge was changed in F8-Extend */
def new shared var v-chgsrchgfl    as logical             no-undo.
def            var typeover        as logical             no-undo.
def            var v-taxablefl     as logical             no-undo.
def            var c-inx           as integer             no-undo. 
def            var p-inx           as integer             no-undo.
def            var n-IE            as c  format "x(1)"    no-undo.
/*def            var extendopen      as logical    init no  no-undo.  */
def new shared var g-bpcurrproc    as c  format "x(8)"    no-undo. 
def new shared var v-lcomment      as c  format "x(1)"    no-undo. 
def            var v-commentfl     as logical    init no  no-undo. 
def            var v-allfl         as logical    init no  no-undo.
def            var v-ackfl         as logical    init yes no-undo.
def            var v-pckfl         as logical    init no  no-undo.
def            var v-advfl         as logical    init no  no-undo.
def            var v-invfl         as logical    init no  no-undo.
def            var v-shipreqfl     as logical    init no  no-undo.  
def            var v-oefoundfl     as logical    init no  no-undo.
def new shared var manual-change   as logical    init no  no-undo.
def            var manualdtfl      as logical    init no  no-undo.
def            var refreshfl       as logical    init no  no-undo.
def            var manual-jump     as logical    init no  no-undo.
def            var v-hcustno       like oeel.custno       no-undo.
def            var errorfl         as logical    init no  no-undo.
def new shared var apr-error       as logical    init no  no-undo.
def new shared var apr-msg         as c format "x(50)"    no-undo.
def new shared var v-qfilename     as c format "x(28)"    no-undo.
def new shared var inquiryfl       as logical    init no  no-undo.
def            var s-verify        as logical    init yes no-undo.
def            var s-sysrefresh    as c  format "x(1)"    no-undo.
def            var s-pctrefresh    as de format "99.99"   no-undo.
def new shared var v-ctrlaovr      as logical             no-undo.
def new shared var v-slsrepovr     as logical             no-undo.
def new shared var v-slsrepinovr   as logical             no-undo.
def            var v-nextmainfl    as logical             no-undo.
def new shared var s-oelockfl        as logical  init no  no-undo.
def new shared var s-wwhse         like oeeh.whse         no-undo.
def            var v-messagelit    as c format "x(74)"    no-undo.
def new shared var v-dispGP        as logical    init yes no-undo.

def new shared var v-slsrep1       like smsn.slsrep       no-undo.
def new shared var v-slsrep2       like smsn.slsrep       no-undo.
def new shared var v-slsrep3       like smsn.slsrep       no-undo.
def new shared var v-company1      as c format "x(13)"    no-undo.
def new shared var v-company2      as c format "x(13)"    no-undo.
def new shared var v-slsrep1%      as de format ">>9.9"   no-undo.
def new shared var v-slsrep2%      as de format ">>9.9"   no-undo.
def new shared var v-slsrep3%      as de format ">>9.9"   no-undo.
def new shared var v-company1%     as de format ">>9.9"   no-undo.
def new shared var v-company2%     as de format ">>9.9"   no-undo.
def new shared var tot-Scomm%      as de format ">>9.9"   no-undo.
def new shared var tot-Ccomm%      as de format ">>9.9"   no-undo.
def            var hold-qty        like bpel.qtyord       no-undo.

def new shared var v-surcharge  like  icsw.stndcost           no-undo.
def new shared var v-stndcost   like  icsw.stndcost           no-undo.
def new shared var v-stndcostdt  like  icsw.stndcostdt       no-undo.
def var v-qtyonhand  like  icsw.qtyonhand          no-undo.
def var v-qtyonorder like  icsw.qtyonorder         no-undo.
def new shared var v-leadtime   like  icsw.leadtmavg          no-undo.
def new shared var v-sparebamt  like  icsw.stndcost           no-undo.
def new shared var v-pdtype     like  pdsc.levelcd            no-undo.
def new shared var v-pdrecord   like  pdsc.pdrecno            no-undo.
def new shared var v-pdamt      as de format ">>>>9.9999"     no-undo.
def new shared var v-pd$md      as c  format "x(1)"           no-undo.
def new shared var v-pdbased    as c  format "x(4)"           no-undo.
def new shared var v-totnet     as de format ">>>>>>9.99"     no-undo.
def new shared var v-totcost    as de format ">>>>>>9.99-"    no-undo.
def new shared var v-rebatefl   as c  format "x(1)"           no-undo.
def new shared var v-totmarg    as de format ">>>>>>9.99-"    no-undo.
def new shared var v-margpct    as de format ">>>>>>9.99-"    no-undo.
def            var x-name       like  apsv.name               no-undo.
def            var xx-line      like  oeelb.lineno            no-undo.
def            var createline   like  oeelb.lineno            no-undo.
def            var linecount    like  oeelb.lineno            no-undo.
def            var splitcount   like  oeelb.lineno            no-undo.
def            var bpi          as i  format "999"            no-undo.

def new shared var v-delfl  as logical            no-undo.     
def new shared var v-xreffl as logical init yes   no-undo.
def var v-chgfl             as logical            no-undo.
def var v-cnvfl             as logical            no-undo.
def var F7-pressed          as logical init no    no-undo.
def var v-continue          as logical init no    no-undo.
/* TAH FG */
def var v-commissionfl  as logical            no-undo.
def var v-commissionmsg as character          no-undo.
def var commission-amt  like oeelb.netamt     no-undo.
def var w-commission    like oeelb.netamt     no-undo.
def var v-checkingfl    as logical            no-undo.
def var edit-amt        like oeelb.netamt     no-undo.

/* x-whse - keep track of line whse. Standard Trend apps change v-whse and
   OEEXC needs to maintain the user assigned whse */
def            var x-whse          like icsw.whse         no-undo.

/* hsfh-whse-whse - keep track of whse for components */
def            var h-whse          like icsw.whse         no-undo.
def            var h-prod          like icsw.prod         no-undo.
def            var h-countrycd     like oeehb.countrycd   no-undo.

/* orig-whse - used to keep track of line whse to calculate ship-dt */
def            var orig-whse       like icsw.whse         no-undo.

def            var hc-whse         like icsw.whse         no-undo.
def            var hc-prod         like icsw.prod         no-undo.
def            var hc-prodcost     like icsw.stndcost     no-undo.
def            var hc-vendno       like icsw.arpvendno    no-undo.

def            var hdr-whse        like icsw.whse         no-undo.
def new shared var h-listprc       like oeel.price        no-undo.
/* Added to fix up the oeebtd.p whse   %TAH */
def            var d-whse          like icsw.whse         no-undo.
def            var d-divno         like arsc.divno        no-undo.
def            var x-specnstype    like oeel.specnstype   no-undo.
def new shared var x-prodtype      as c  format "x(4)"    no-undo.
/* s-notes already defined in g-oeipv.i */
/*deff new shared var s-notesfl       as c  format "x(1)"    no-undo.*/
def            var v-jumpfl        as logical  init no    no-undo.
def            var v-5603fl        as logical  init no    no-undo.
def            var v-5680fl        as logical  init no    no-undo.
def            var v-5966fl        as logical  init no    no-undo.
def            var v-internalfl    as logical             no-undo.
def            var v-faxpopup      as logical  init no    no-undo.
def            var v-taknbyovr     as logical  init no    no-undo.
def            var v-qtyfirst      as logical  init no    no-undo.

def            var v-lostcan       as l format "lost/lost"   init yes no-undo.
def            var v-lostmode      as l format "lost/delete" init yes no-undo.
def            var v-delmode       as l format "lost/delete" init yes no-undo.
def            var v-delkmode      as l format "yes/no"      init no  no-undo.
def            var v-lostbusty     as c format "x(2)"     no-undo.
def            var hold-lineno     like oeelb.lineno      no-undo.
def            var hold-linedt     as date format "99/99/99"          no-undo.
def            var disp-avail      as c format "x(3)"     no-undo.
def            var disp-oo         as c format "x(3)"     no-undo.
def            var F12-keyed       as logical init no     no-undo.
/*def            var F11-quote       as i format ">>>>>>>9" no-undo.*/
def            var F11-proc        as c format "x(8)"     no-undo.
def            var typed-in        as logical init no     no-undo.
def            var x-qtyavail      like icsw.qtyonhand    no-undo.
def            var hold-specnstype like oeelb.specnstype  no-undo.
def            var hold-reasunavty like oeelb.reasunavty  no-undo.
def            var hold-operinit   like oeelb.operinit    no-undo.
def            var hold-transdt    like oeelb.transdt     no-undo.
def            var hold-transtm    like oeelb.transtm     no-undo.
def new shared var x-qty           like oeel.qtyship  extent 7  no-undo.
def new shared var x-prod          like oeel.shipprod extent 7  no-undo.
def new shared var x-unit          like icsp.unitsell extent 7  no-undo.
def            var x-cursorup      as i format "999" init 501 no-undo.
def            var v-hprod         like icsw.prod         no-undo.
def            var xtype           as c format "x(1)"     no-undo.
def            var newprod         as logical init no     no-undo.
def            var v-avail         like icsw.qtyonhand    no-undo.
def            var cancel-xref     as logical init no     no-undo.
def            var f8-return       as c format "x(10)"    no-undo.
def            var x-uparrowfl     as logical             no-undo.

/* Conversion Variables */
def            var v-enterdt      like oeelb.enterdt      no-undo.
def            var v-xxda1        like oeelb.xxda1        no-undo.
def            var v-priceoverfl  like oeelb.priceoverfl  no-undo.
def            var v-usagefl      like oeelb.usagefl      no-undo.
def            var v-taxgroup     like oeelb.taxgroup     no-undo.
def            var v-gststatus    like oeelb.gststatus    no-undo.
def new shared var v-ordertype    like oeelb.ordertype    no-undo.
def            var v-nontaxtype   like oeelb.nontaxtype   no-undo.
def new shared var v-botype       like oeelb.botype       no-undo.
def            var v-disctype     like oeelb.disctype     no-undo.
/*def            var v-slsrepin     like oeelb.slsrepin     no-undo.*/
def            var msgerror       as logical  init no     no-undo.
def            var tied-po        like poeh.pono          no-undo.
def            var tied-wt        like wteh.wtno          no-undo.
def            var same-whse      as logical              no-undo.
def            var splitfl        as logical   init no    no-undo.

/* das - SHIPVIA edit */
def new shared var z-shiperr      as logical               no-undo.
def new shared var idx            as i  format ">9"        no-undo.

/* Used for Margin Check */
def var ssdi-actmarg    as decimal                           no-undo.
def var zsdi-sasozxt1   as integer                           no-undo.
def var x-net           as decimal                           no-undo.
def var zsdi-marqty     as decimal                           no-undo.
def var zsdi-marprice   as decimal                           no-undo.
def var zsdi-mardisc    as decimal                           no-undo.
def var zsdi-margamt    as decimal                           no-undo.
def var zsdi-netamt     as decimal                           no-undo.
def var zsdi-netamt2    as decimal                           no-undo.
def var zsdi-profit     as decimal                           no-undo. 
def var zsdi-margpct    as decimal                           no-undo. 
def var zsdi-rebamt     as decimal                           no-undo. 
def var zsdi-prodcost   as decimal                           no-undo. 
def var zsdi-lvl        as int format "9"                    no-undo.
def var zsdi-errmsg     as character format "x(76)" dcolor 3 no-undo.
def var zsdi-somechange as logical                           no-undo.
def var zsdi-todaypct   as dec                               no-undo.
def var zsdi-chgpct     as dec                               no-undo.
def var zsdi-prctyp     like icsw.pricetype                  no-undo.
def var zsdi-pctdiff    as dec  format "9999.99-"            no-undo.
def var zsdi-outrep     like oeeh.slsrepout                  no-undo.
def var zsdi-errfl      as int format "9"                    no-undo.
def var zsdi-prod       like oeel.shipprod                   no-undo.
def var zsdi-custno     like oeeh.custno                     no-undo.
def var zsdi-pdsclvl    as int format "9"                    no-undo.
def var zx-recid        as recid                             no-undo.
def var zx-priced       like oeel.price                      no-undo.
def var zx-pdrecid      as recid                             no-undo.
def var zx-prdprctype   like oeel.pricetype                  no-undo.
def var zx-discamt      like oeel.discamt                    no-undo.
def var zx-discpct      like oeel.discamt                    no-undo.
def var zh-discamt      like oeel.discamt                    no-undo.
def new shared var s-chrgqty       like oeel.qtyord          no-undo.
def new shared var s-prodcost      like oeel.prodcost        no-undo.
def new shared var s-returnfl      as logical                no-undo.
def new shared var s-pricetype     like oeel.pricetype       no-undo.
def            var auth-price      like oeel.price           no-undo.
def            var auth-discamt    like oeel.discamt         no-undo.
def            var auth-disctype   like oeel.disctype        no-undo.
def            var auth-prodcost   like oeel.prodcost        no-undo.
def            var auth-prodcat    like oeel.prodcat         no-undo.

def new shared var v-idoeehb       as recid                  no-undo.
def new shared var v-idoeelb       as recid                  no-undo.


/* used for oeetlrb.p during kit processing */
def new shared var s-prodline      like oeel.prodline          no-undo.
def new shared var s-slsrepin     like oeel.slsrepin           no-undo.
def new shared var s-slsrepout    like oeel.slsrepout          no-undo.
def new shared var s-botype       like oeel.botype initial "n" no-undo.
def new shared var s-ordertype    like oeel.ordertype          no-undo.
def new shared var s-qtyship      as dec format "zzzzzz9.99"   no-undo.
def new shared var o-rebamt       like pder.rebateamt          no-undo.
def new shared var s-vendno       like oeel.vendno             no-undo.
def new shared var s-vvendno      like oeel.vendno             no-undo.
def new shared var s-netamt       like oeel.netamt             no-undo.
def new shared var s-qtyneeded    like oeelk.qtyneeded         no-undo.
def new shared var v-oeicssty     like arsc.spcdefaultty       no-undo.
def new shared var v-vaqtyneeded  as deci                      no-undo.


/*def new shared var v-rebamt        like icsw.stndcost        no-undo.*/
/*def var v-errfl         as c format "x(1)"                   no-undo.*/
def new shared var zsdiconfirm     as logical init yes         no-undo.
def new shared var orig-cost       like oeel.prodcost        no-undo.
def new shared var over-cost       like oeel.prodcost        no-undo.

/* Non-Stock hold fields */
def new shared var ns-descrip        as c format "x(24)"            no-undo.
def new shared var ns-descrip2       as c format "x(24)"            no-undo.
def new shared var ns-vendno         like oeel.vendno               no-undo.
def new shared var ns-prodline       like icsl.prodline             no-undo.
def new shared var ns-prodcat        like oeel.prodcat              no-undo.
def new shared var ns-listprc        like oeel.price                no-undo.
def new shared var ns-listfl         as c format "x(1)"             no-undo.
def new shared var ns-prodcost       like oeel.prodcost             no-undo.
def new shared var ns-leadtm         like oeel.leadtm               no-undo.
def new shared var ns-ptype          like oeel.pricetype            no-undo.
def new shared var comp-leadtm       like oeel.leadtm               no-undo.



{zsdi-margin.i}
{g-pdcalc.i &combuffers = "/*"
            &new = "new"}

def new shared var v-powtintfl as logical no-undo.



/* Used for Tax Authority Load */                                   
def var v-shipfromstatecd   like sasgm.statecd            no-undo.
def var v-shipfromcountycd  like sasgm.countycd           no-undo.
def var v-shipfromcitycd    like sasgm.citycd             no-undo.
def var v-shipfromother1cd  like sasgm.othercd            no-undo.
def var v-shipfromother2cd  like sasgm.othercd            no-undo.
def var v-Shiptostatecd     like sasgm.statecd            no-undo.
def var v-Shiptocountycd    like sasgm.countycd           no-undo.
def var v-Shiptocitycd      like sasgm.citycd             no-undo.
def var v-Shiptoother1cd    like sasgm.othercd            no-undo.
def var v-Shiptoother2cd    like sasgm.othercd            no-undo.
def var v-taxstatecd        like sasgm.statecd            no-undo.
def var v-taxcountycd       like sasgm.countycd           no-undo.
def var v-taxcitycd         like sasgm.citycd             no-undo.
def var v-taxother1cd       like sasgm.othercd            no-undo.
def var v-taxother2cd       like sasgm.othercd            no-undo.
def var v-interstfl         as logical initial no         no-undo.
/*def var v-compcd            as integer initial 0          no-undo.*/

def var v-shiptonm          like oeehb.shiptonm           no-undo.
def var v-shiptoaddr1       like oeehb.shiptoaddr[1]      no-undo.
def var v-shiptoaddr2       like oeehb.shiptoaddr[2]      no-undo.
def var v-shiptocity        like oeehb.shiptocity         no-undo.
def new shared var v-shiptost          like oeehb.shiptost           no-undo.
def new shared var v-shiptozip         like oeehb.shiptozip          no-undo.
def var v-shiptogeocd       like oeehb.geocd              no-undo.
                   

/* Used for the bottom of the item lookup  */
def new shared var s-whse      as c format "x(04)" extent 12 dcolor 3 no-undo.
def new shared var s-wavail    as c format "x(07)" extent 12 dcolor 3 no-undo.
def new shared var s-netavail  like icsw.qtyunavail  no-undo.
def new shared var l-vendno    as c format "x(12)" dcolor 3      no-undo.
def new shared var l-vendnm    like apsv.name      dcolor 3      no-undo.


def new shared var v-crprod     like icsp.prod           no-undo.
def new shared var v-crwhse     like icsd.whse           no-undo.
def new shared var v-crtype     like oeel.xrefprodty     no-undo.
def new shared var v-crqty      like icsec.orderqty      no-undo.
def new shared var v-crunit     like icsec.unitsell      no-undo.
def new shared var v-errflchar  as c format "x"          no-undo.

def new shared var catalogfl    as   logical             no-undo.
def new shared var cataddfl     as c format "x(2)"       no-undo.
def            var x-catlineno  like oeel.lineno         no-undo.
def            var v-zsdireturncd as char                no-undo.


def new shared temp-table t-whseinfo no-undo
  field whse   like icsw.whse
  field zavail like icsw.qtyintrans
  field zoo    like icsw.qtyintrans
  field arptp  like icsw.arptype
 index fkx
   zavail descending
   whse ascending.
{ar_defvars_lookupoe.i new}

def var v-offset          as integer                    no-undo.
def var v-browsekeyed     as logical                    no-undo.
def var v-prodbrowsekeyed as logical                    no-undo.
def var v-custbrowsekeyed as logical                    no-undo.
def new shared var v-linebrowsekeyed as logical                    no-undo.
def var v-Shiptobrowsekeyed as logical                  no-undo.

def var v-custbrowserow   as integer       init 1       no-undo.
def var v-quoteloadfl     as logical       init no      no-undo.


def var v-custbrowseopen  as logical                    no-undo.
def var v-Shiptobrowseopen  as logical                  no-undo.
def var v-prodbrowseopen  as logical                    no-undo.
def var v-linebrowseopen  as logical                    no-undo.
def var v-mode            as character format "x(1)"    no-undo.
def new shared var v-hmode           as character format "x(1)"    no-undo.
def new shared var v-lmode           as character format "x(1)"    no-undo.
def var v-qmode           as character format "x(1)"    no-undo.
def var v-applygo         as logical                    no-undo.
def var v-oneinthedirection as logical                  no-undo.
def var v-inx             as integer                    no-undo.            
def var v-inx2            as integer                    no-undo.
def var v-rowid           as rowid                      no-undo.            
def var v-totavail        as integer                    no-undo.
def var v-totoo           as integer                    no-undo.
/*def var v-type            as c                          no-undo.*/
/*def var v-cqty            as dec                        no-undo.*/
/*def var v-cunit           as c                          no-undo.*/

def var v-framestate      as char                       no-undo.
def var v-CurrentKeyPlace as integer                    no-undo.
def var v-ManualFrameJump as logical                    no-undo.
def var v-NormalMove      as integer init 0             no-undo.
def var v-loadbrowseline  as logical                    no-undo.
def var v-top1            as logical init no            no-undo.
def var v-middle1         as logical init no            no-undo.
def var v-bottom2         as logical init no            no-undo.
def var v-zsdiobfl        as logical init yes           no-undo.

/* Product Add On the Fly */
def new shared var otf-create    as logical      init no       no-undo.
def new shared var otf-kit       as logical      init no       no-undo.
def new shared var w-loadefaults as logical      init no       no-undo.
def new shared var w-vendefaults as logical      init no       no-undo.
def new shared var otf-icsl-n    as logical                    no-undo.
def new shared var w-buyer       like icsl.buyer               no-undo.
def new shared var otf-error     as logical      init no       no-undo.
def            var w-vendname    as c format "x(15)"           no-undo.
def            var w-currproc    like g-currproc               no-undo.
def            var w-ourproc     like g-ourproc                no-undo.
def            var w-lineno      like g-oelineno               no-undo.
def            var i-lineno      like oeelb.lineno             no-undo.
def            var zsdi-confirm  as logical                    no-undo.

def var v-KeyMovement1    as char format "x(2)"  Label "Move 1"    no-undo.  
def var v-KeyMovement2    as char format "x(2)"  Label "Move 2"    no-undo.  
def var v-KeyMovement3    as char format "x(2)"  Label "Move 3"    no-undo.  
def var v-KeyMovement4    as char format "x(2)"  Label "Move 4"    no-undo.  

def var v-xKeyMovement1   as char format "x(3)"  Label "Move 1"    no-undo.  
def var v-xKeyMovement2   as char format "x(3)"  Label "Move 2"    no-undo.  
def var v-xKeyMovement3   as char format "x(3)"  Label "Move 3"    no-undo.  
def var v-xKeyMovement4   as char format "x(3)"  Label "Move 4"    no-undo.  

def var w-key             as integer                               no-undo.
def var ip-control        as c    format "x(1)"                    no-undo.

def new shared temp-table OptionMoves no-undo
  field OptionIndex  as integer
  field Procedure    as character format "x(20)"
  field Procname     as character format "x(2)"
  index oinx
    OptionIndex.

def buffer Opts     for OptionMoves.
def buffer sc-notes for notes.
def buffer t-oeelb  for oeelb.
def buffer t-oeehb  for oeehb.
/* CallMod */                      
def buffer z-com for com.          
/* CallMod */                      
/*def buffer z-oeehb  for oeehb.*/       /*das-03/20/2008*/
def buffer b-sasoo  for sasoo.
def buffer b-sasos  for sasos.
/*def buffer zx-pdsc  for pdsc.*/        /*das-03/20/2008*/
def buffer t-icsw   for icsw.
/*def new shared buffer b-icsp for icsp.*/
def buffer t-sastn  for sastn.
def buffer a-arsc   for arsc.
def buffer a-arss   for arss.
def buffer u-icsp   for icsp.
def buffer o-bpel   for bpel.

def var hold-prodlutype   as char format "x(1)"                    no-undo.
def var v-prodlutype      as char format "x(1)"                    no-undo.
def var v-custlutype      as char format "x(1)"                    no-undo.
def var v-Shiptolutype    as char format "x(1)"                    no-undo.


def new shared var v-quoteno         as integer format ">>>>>>>9"   no-undo.
/*def var v-queue           as c    format "x(1)" init "q" no-undo.*/
def var v-notefl          as c    format "x(1)"          no-undo.
def var v-arnotefl        as c    format "x(1)"          no-undo.
def var v-icnotefl        as c    format "x(1)"          no-undo.  /**das**/
def var v-takenby                    like oeeh.takenby              no-undo.
/*def new shared var v-custno       like oeeh.custno             no-undo.*/
def new shared var v-shipto         like oeeh.shipto               no-undo.
def var v-custname                  as c format "x(20)"            no-undo.
def new shared var v-compfocus      as c format "x(2)"             no-undo.   
def new shared var v-shipdt         as date format "99/99/99"      no-undo.
def var v-canceldt                  as date format "99/99/99"      no-undo.
def new shared var v-quotetot       as de format ">>>>>>9.99"      no-undo.
def new shared var v-commtot        as de format ">>>>>>9.99"      no-undo.
def new shared var v-Hcommpct       as de format ">>9.99"          no-undo.
def new shared var v-Lcommpct       as de format ">>9.99"          no-undo.
def new shared var v-totcomm        as de format ">>>>>>9.99"      no-undo.
def            var ss-commtot       as de format ">>>>>>9.99"      no-undo.
def            var c-commtot        as de format ">>>>>>9.99"      no-undo.

def var v-quotemgn%       as de format ">>>>>9.99-"      no-undo.
def var x-quotemgn%       as de format ">>>>>>9.99-"     no-undo.
/*def var v-mgn%cost        like oeehb.totdatccost         no-undo.*/
def var v-mgn%cost        like oeelb.prodcost         no-undo.

def var mgn%check         as de format ">>>>>>>>>9.99-"  no-undo.
def var v-mgn%net         as de format ">>>>>>9.99-"     no-undo.
def new shared var v-rarrnotesfl     as logical                     no-undo.

                                                         
def var o-seqno           like oeel.lineno               no-undo.
def var o-lineno          like oeel.lineno               no-undo.
/*def var o-prod            like oeel.shipprod             no-undo.*/
def var o-descrip         as   character format "x(24)"  no-undo.
def var o-xprod           like icsw.prod                 no-undo.
def var o-whse            like icsw.whse                 no-undo.  
def var o-listprc         like oeel.price                no-undo.
def var o-sellprc         like oeel.price                no-undo.
def var o-Lcommpct        as de format ">>9.99"          no-undo.
def var o-gp              as decimal format ">>9.99-"    no-undo. 
def var o-disc            as decimal format ">>9.99"     no-undo.
def var o-disctype        as logical                     no-undo.
def var o-qty             like oeel.qtyord               no-undo.  
def var o-qtyrem          like oeel.qtyord               no-undo.
def var o-shipdt          as date    format "99/99/99"   no-undo.
                                                         
def var o-vendno          like oeel.vendno               no-undo.
def var o-prodcat         like oeel.prodcat              no-undo.
def var o-prodcost        like oeel.prodcost             no-undo.
def var o-leadtm          like oeel.leadtm               no-undo.
def var o-pricetype       like oeel.pricetype            no-undo.
def var o-specnstype      like oeel.specnstype           no-undo.
                                                         
def new shared var v-seqno           like oeel.lineno               no-undo.
def new shared var v-lineno          like oeel.lineno               no-undo.
/*def new shared var v-prod            like oeel.shipprod           no-undo.*/
def new shared var v-descrip         as   character format "x(24)"  no-undo.
def new shared var v-descrip2        as   character format "x(24)"  no-undo.
def new shared var v-xprod           as   character format "x(22)"  no-undo.
def var xcount            as i format ">>,>>9"           no-undo.
def var x-count           as i format "9"                no-undo.
def var count             as i format "999"              no-undo.
/*def var w-docno           as c format "x(9)"             no-undo.*/

/*def var v-pricetype     like icsw.pricetype            no-undo.*/
/*def new shared var v-whse            like icsw.whse               no-undo.*/
def new shared var v-listprc         like oeel.price                no-undo.
def new shared var v-sellprc         like oeel.price                no-undo.
def            var h-sellprc         like oeel.price                no-undo.
def            var disp-sellprc      like oeel.price                no-undo.
def            var auth-sellprc      like oeel.price                no-undo.
def            var ho-sellprc        like oeel.price                no-undo.
def new shared var v-gp              as decimal format ">>9.99-"    no-undo. 
def            var x-gp              as dec format ">>>>>>>9.99-"   no-undo.
def new shared var v-disc            as decimal format ">>>9.99-"   no-undo.
/*def var v-disctype      as logical format "$/%"        no-undo.*/
def new shared var v-qty             like oeel.qtyord               no-undo.
def new shared var v-qtyrem          as de format ">>>>9.99-"       no-undo.
def            var w-qtyremain       like oeel.qtyord               no-undo.
def            var v-hqty            like oeel.qtyord               no-undo.
def var oeetipdt          as date    format "99/99/99"   no-undo.
def new shared var v-matrixed        as character      format "x"   no-undo.
def var v-foundfl         as logical                     no-undo.
def var v-statustype      like icsw.statustype           no-undo.

def new shared var v-vendno          like apsv.vendno               no-undo.
def new shared var v-vendname        like apsv.name                 no-undo.

def new shared var v-prodcat         like oeel.prodcat              no-undo.
def new shared var v-prodline        like icsl.prodline             no-undo.
def new shared var v-glcost          as de format ">>>>>9.99"       no-undo.
/*def var v-prodcost        like oeel.prodcost             no-undo.*/
def new shared var v-cost   like oeel.prodcost                      no-undo.
def new shared var v-leadtm          like oeel.leadtm               no-undo.
/*def var v-pricetype       like oeel.pricetype            no-undo.*/
def new shared var v-ptype           like oeel.pricetype            no-undo.
def new shared var v-specnstype      like oeel.specnstype           no-undo.
def var v-rushfl          like oeel.rushfl               no-undo.
/*def new shared var v-costoverfl      like oeel.costoverfl          no-undo.*/
def new shared var h-prodcost        like oeel.prodcost             no-undo.

def var v-custnum         as char format "x(15)"        no-undo.
def var v-custlu          as char format "x(15)"        no-undo.
def var v-hcustlu         as char format "x(15)"        no-undo.
def var v-topcustlu       as char format "x(15)"        no-undo.
def var v-currdate        as date format "99/99/99"     no-undo.
def var v-curryr          as char format "x(4)"         no-undo.
def var v-lastyr          as char format "x(4)"         no-undo.
def var v-procyr          as int  format "99"           no-undo.
def var v-prolyr          as int  format "99"           no-undo.
def var v-yr              as char format "x(2)"         no-undo.



def            var v-email           as char format "x(30)"        no-undo.
def new shared var v-contact         as char format "x(19)"        no-undo.
def            var v-phone           as char format "xxx-xxx-xxxx" no-undo.
def            var v-cphone          as char format "xxx-xxx-xxxx" no-undo.
def            var v-custpo          like oeeh.custpo              no-undo.
def            var v-currency        as char format "x(12)"        no-undo.
def            var v-currencyty      as char format "x(1)"         no-undo.
def            var v-terms           like arsc.termstype           no-undo.
def            var v-comments        as char format "x(60)"  
                                     view-as editor inner-chars 60
                                                    inner-lines 6
                                                    scrollbar-vertical.
                                            

def var v-lphone          as char format "xxx-xxx-xxxx" no-undo.
def var v-lcity           like arsc.city                no-undo.
def var v-lstate          like arsc.state               no-undo.
def var v-lzipcd          like arsc.zipcd               no-undo.


define new shared temp-table t-lines no-undo
  field lineno      like oeel.lineno
  field seqno       like oeelk.seqno
  field specnstype  like oeel.specnstype
  field prod        like oeel.shipprod
  field icnotefl    like icsp.notesfl
  field descrip     as character format "x(24)"
  field descrip2    as character format "x(24)"
  field xprod       as character format "x(22)"
  field whse        like oeel.whse
  field slsrepout   like arsc.slsrepout
  field slsrepin    like arsc.slsrepin
  field vendno      like oeel.vendno
  field prodcat     like oeel.prodcat
  field prodline    like oeel.prodline
  field listprc     like oeel.price
  field listfl      as c format "x(1)"
  field pricetype   like oeel.pricetype
  field discpct     as dec format ">>9.99-"
  field disctype    as logical
  field sellprc     like oeel.price
  field Lcommpct    as de format ">>9.99"
  field prodcost    like oeel.prodcost
  field glcost      like oeel.glcost
  field surcharge   like oeel.datccost
  field leadtm      like oeel.leadtm
  field gp          as dec format ">>9.99-"
  field qty         like oeel.qtyord
  field qtyrem      as dec format ">>>>9.99-"
  field shipdt      as date format "99/99/99"
  field pdscrecno   like oeel.pdrecno
  field qtybrkty    as char format "x(1)"
  field pdtype      like  pdsc.levelcd        
  field pdamt       as de format ">>>>9.9999" 
  field pd$md       as c  format "x(1)"
  field pdbased     as c  format "x(4)"
  field totnet      as de format ">>>>>>9.99-"
  field lcomment    as c  format "x(1)"
  field totcost     as de format ">>>>>>9.99-"
  field rebatefl    as c  format "x(1)"
  field sparebamt   like  icsw.stndcost
  field totmarg     as de format ">>>>>>9.99-"
  field margpct     as de format ">>>>>>9.99-"
  field convertfl   as logical
  index ix1
    lineno
    seqno.
    
def buffer xt-lines for t-lines.


/* %toms% */
{f-oeexccnt.i}
/* %toms% */


/* Product lookup query */

def new shared temp-table w-prod no-undo
  field prod like icsp.prod
index pxi
    prod.
/*define buffer b-icsp   for icsp.*/
define buffer b-w-prod for w-prod.

define buffer bq-icsw  for icsw.
define buffer L-oeehb  for oeehb.
define new shared buffer b-oeehb  for oeehb.
define new shared buffer b-oeelb  for oeelb.
define new shared buffer b-icsw   for icsw.
define new shared buffer b-oeeh   for oeeh.
define new shared buffer b-oeel   for oeel.
define buffer b-arsc   for arsc.
define buffer b-arss   for arss.
define buffer b-icsl   for icsl.
define query  q-prodlu  for  b-w-prod, bq-icsw, icsp  scrolling.

/* das - added for catalog logic */
def new shared  var s-commentfl     as c format "x"           no-undo.
def new shared  var s-rushdesc      as c format "x(4)"        no-undo.
def new shared  var s-taxablety     like oeel.taxablety       no-undo.
def new shared  var s-yn            as logical initial "no"   no-undo.
def new shared  var s-speccost      as c format "x(4)"        no-undo.
def new shared  var v-xrefdispfl    as l       init no        no-undo.
def new shared  var v-xrefprodty    like oeel.xrefprodty      no-undo.
def new shared  var v-oelineno      like sasoo.oelineno       no-undo.
def new shared  var k-keyfl         as logical                no-undo.
def new shared  var k-optfl         as logical                no-undo.
def new shared  var k-reqfl         as logical                no-undo.
def new shared  var s-backorder     as dec format "zzzzzzzz9.99-"     no-undo.
def new shared  var s-bodfabwhse    like oeel.whse                    no-undo.
def new shared  var v-wtboderrfl    as l                              no-undo.
def new shared  var s-corecharge    like oeel.corecharge              no-undo.
def new shared  var s-datccost      like oeel.datccost                no-undo.
def new shared  var s-gststatus     like oeel.gststatus               no-undo.
def new shared  var s-leadtm        like oeel.leadtm                  no-undo.
def new shared  var s-lostbustyl    like oeel.lostbusty               no-undo.


/*def new shared frame f-oeetl.
{f-oeetl.i}.*/
{g-oedopw.i "new"}.

/* TAH 07/02/09 */
define var    v-prodbeg as character format "x(26)" no-undo.

define browse b-prodlu query q-prodlu
  display /* (icsp.statustype + " " + icsp.prod) format "x(26)" 
             TAH 07/02/09 */
          v-prodbeg
          icsp.descrip[1]  v-totavail  v-totoo
    /*with 6 down centered no-hide no-labels */
    with 5 down centered no-hide no-labels
     title
"Product                    Description                TotAvail   TotOnOrd".


def var v-prodlulit         as character format "x(76)"            no-undo.
def var v-blanks            as c         format "x(76)"            no-undo.

form
 b-prodlu
 v-prodlulit
 /*at column 3 row 10 dcolor 2*/
 at column 3 row 8 dcolor 2
with frame f-prodlu no-labels overlay row 9 no-box.

form
  v-Whse              at 3 label "Whse"
    {f-help.i}
    validate(if {k-func.i} then true else ({v-icsd.i v-whse "/*"}),
    "Warehouse Not Set Up in Warehouse Setup - ICSD (4601)")
  v-vendno            at 1 label "Vendno"
    {f-help.i}
    validate(if {k-func.i} then true else ({v-apsv.i v-vendno "/*"}),
    "Vendor # Not Set Up in Vendor Setup - APSV (4301)")
     with frame f-icvend overlay col 25 row 3 title "Vendor" side-labels.



/* Order Lines Query */
define query  q-lines for t-lines scrolling.
define browse b-lines query q-lines
display (if t-lines.seqno <> 0  then
             "   "
           else
             string(t-lines.lineno,">>9")) + 
             (if v-conversion and t-lines.convertfl then
                "X"
              else  
              if t-lines.specnstype <> "" then
                 t-lines.specnstype
              else
                 " ") + " " +
          string(t-lines.prod,"x(24)") + 
          string(t-lines.icnotefl,"x(1)") +
          string(t-lines.qty,">>>>9.99") +
          string(t-lines.qtyrem,">>>>9.99-") +
          (if t-lines.seqno  = 0 then
             string((string(t-lines.sellprc,">>>>>>9.9999") + " " +
                     string(t-lines.Lcommpct,">>9.99") +
                       " "),"x(20)") + " " + /*  " " +*/
               string(t-lines.gp,">>9.99-") 
             + 
             string(t-lines.lcomment,"x(1)")
           else
              "                         " + string(t-lines.lcomment,"x(1)"))
              format "x(74)"

    with 10 down centered no-hide no-labels
    /*with 9  down centered no-hide no-labels*/
    title 
"   Ln# Product                      Qty    QtyRmn  Sell Price   Comm%    GP% ".


form
 b-lines
with frame f-lines no-labels width 80 overlay row 9 no-box.
/*with frame f-lines no-labels width 80 overlay row 10 no-box.*/


/* Customer Lookup Query */

def var v-custlulit         as character format "x(74)"            no-undo.

def new shared temp-table w-cust no-undo
  field custno   like arsc.custno
index sxi
    custno.
define buffer b-w-cust for w-cust.

define query  q-custlu for b-w-cust, arsc scrolling.

define browse b-custlu query q-custlu
  display ((if arsc.statustype then
             "A"
            else
             "I") +
           string(arsc.custno,">>>>>>>>>>9")) format "x(13)"
          arsc.lookupnm
          (arsc.city + "," + arsc.state) format "x(23)"
          arsc.phone
    with 8 down centered no-hide no-labels
    title
"Stat     Custno  LookupName      City/State              Phone                  ".

form
 b-custlu
 v-custlulit
 at column 3 row 10 dcolor 2
with frame f-custlu no-labels overlay row v-custbrowserow no-box.


form
  v-lCity             at 3 label "City   "
  v-lState            at 3 label "State  "
  " "                 at 1  
  "OR"                at 1  
  " "                 at 1    
  v-lzipcd            at 3 label "ZipCode"

     with frame f-arzipcd overlay col 25 row 3 
      title "City/State or Zip Code" side-labels.


form
  v-lphone            at 3 label "Phone# "

     with frame f-arphone overlay col 25 row 3 
      title "Phone Number Lookup" side-labels.

/* Customer Shipto Lookup Query */

def var v-Shiptolulit         as character format "x(74)"            no-undo.

def new shared temp-table w-shipto no-undo
  field custno   like arsc.custno
  field shipto   like arss.shipto

index sxi
    shipto.
define buffer b-w-shipto for w-shipto.

define query  q-shiptolu for b-w-shipto, arss , arsc scrolling.

define browse b-shiptolu query q-shiptolu
  display ((if arss.statustype then
             "A "
            else
             "I ") +
           string(arss.shipto,"x(8)")) format "x(10)"
          arss.addr[1]
          (arss.city + "," + arss.state) format "x(23)"
          arss.slsrepout
    with 8 down centered no-hide no-labels 
    title
"     Shipto   Address                        City/State              SRep     ".

form
 b-shiptolu
 v-Shiptolulit
 at column 3 row 10 dcolor 2

with frame f-shiptolu no-labels overlay row v-custbrowserow no-box.




/* Frame at Top of page */
form
  "Quote:"     at 1
  v-quoteno    at 8   {f-help.i}
  "C"          at 16
  v-notefl     at 17
  "Cust#:"     at 18       
  v-custno     at 24
  v-arnotefl   at 36
  v-custname   at 37
  "Whse:"      at 59
  v-whse       at 64
  "TknBy:"     at 69
  v-takenby    at 76   

  "Customer        ShipTo     Contact               Phone    CustomerPO" 
  at 1 dcolor 2
  "            "  at 69 dcolor 2
  v-custlu        at  1       {f-help.i}
  v-shipto        at 17       {f-help.i}
  v-contact       at 26       {f-help.i}
  v-phone         at 46
  v-custpo        at 59
  v-terms         at  1
  "Cancel Dt"     at  5 
  v-canceldt      at 15
  "Quote Tot"     at 24
  v-quotetot      at 34 
  "Comm $ Tot"    at 45
  v-commtot       at 56
  "Comm%"         at 67
  v-Hcommpct      at 73 
  /*skip (1)*/
  with frame f-top1 no-labels no-box overlay row 1.


/* Top Customer Lookup */

form
  "Customer LookUp:"     at 1  dcolor 2
  v-topcustlu  at 18
  with frame f-toplu no-labels no-box overlay row 2.

/* Line Item Entry Frame */

def var v-linelit              as character format "x(80)"    no-undo.
def var v-linenormlit          as character format "x(80)"
   init
"Ln# Product                       Qty      Price/EA    Comm%  Ship Dt  Total   ".
form
  v-linelit    at 1 dcolor 2
  v-lineno     at 1
  v-specnstype at 4   /*dcolor 2*/
  v-prod       at 5   {f-help.i}
  v-icnotefl   at 29               
  v-qty        at 31
  v-sellprc    at 42
  v-Lcommpct   at 56
  v-shipdt     at 63
  v-totnet     at 71
  v-descrip    at 1
  v-descrip2   at 25
  "x:"         at 50
  v-xprod      at 52 
  v-lcomment   at 79
with frame f-mid1 overlay no-labels no-box row 5.




form
  v-prod      at 1
  v-descrip   at 1 label "Descr "
  v-vendno    at 1 
  v-prodline  at 24 label "Prodline"
  v-prodcat   at 1 
  v-listprc   at 1
  v-listfl    at 22 no-label
  v-prodcost  at 1 label "Cost "
  v-leadtm    at 24
  v-ptype     at 1  
with frame f-midnstk overlay side-labels row 9 centered
 title "Non-Stock Entry".


/* Quote Comment Entry Frame */

form
  v-comments at 13
  "INternal"     at column  1 row 1
  "Quote Notes:" at column  1 row 2
  with frame f-bot2i no-labels no-box overlay row 13.

form
  "On All:"       at 13
  v-allfl         at 21
  "On Ack:"       at 25
  v-ackfl         at 33 
  "On Pck:"       at 37
  v-pckfl         at 45
  "On Adv:"       at 49
  v-advfl         at 57
  "On Inv:"       at 61
  v-invfl         at 69
  v-comments at 13
  "EXternal"     at column  1 row 1 
  "Quote Notes:" at column  1 row 2
  with frame f-bot2e no-labels no-box overlay row 12.

def var blankline1  as c format "x(80)" init " " no-undo.
def var blankline2  as c format "x(80)" init " " no-undo.
def var blankline3  as c format "x(80)" init " " no-undo.
def var blankline4  as c format "x(80)" init " " no-undo.
def var blankline5  as c format "x(80)" init " " no-undo.
def var blankline6  as c format "x(80)" init " " no-undo.
def var blankline7  as c format "x(80)" init " " no-undo.
def var blankline8  as c format "x(80)" init " " no-undo.
def var blankline9  as c format "x(80)" init " " no-undo.
def var blankline10 as c format "x(80)" init " " no-undo.
def var blankline11 as c format "x(80)" init " " no-undo.
def var blankline12 as c format "x(80)" init " " no-undo.
def var blankline13 as c format "x(80)" init " " no-undo.
def var blankline14 as c format "x(80)" init " " no-undo.
def var blankline15 as c format "x(80)" init " " no-undo.
def var blankline16 as c format "x(80)" init " " no-undo.
def var blankline17 as c format "x(80)" init " " no-undo.

form 
 blankline1  at 1
 blankline2  at 1
 blankline3  at 1
 blankline4  at 1
 blankline5  at 1
 blankline6  at 1
 blankline7  at 1
 blankline8  at 1
 blankline9  at 1
 blankline10 at 1
 blankline11 at 1
 blankline12 at 1
 blankline13 at 1
 blankline14 at 1
 blankline15 at 1
 blankline16 at 1
 blankline17 at 1
 with frame f-blank no-box no-labels overlay row 4.
 

form
"                                         F9-Print                               "
  at 1 dcolor 2
with frame f-oeexcline no-box no-labels overlay row 21.

form
"F6-CUSTOMER   F7-Lines    F8-Extend      F9-ShipTo Addr     F10-Notes           "
  at 1 dcolor 2
with frame f-statusline no-box no-labels overlay row 21.

form
"F6-Customer   F7-Lines                                      F10-NOTES           "
  at 1 dcolor 2
with frame f-statusline1 no-box no-labels overlay row 21.

form
"F6-Customer   F7-LINES     F8-Extend    F9-Line Comments    F10-Notes           "
  at 1 dcolor 2
with frame f-statusline2 no-box no-labels overlay row 21.  

/*
form
"F6-Customer                F8-Extend    F9-LINE COMMENTS    F10-Notes          "
  at 1 dcolor 2
with frame f-statusline3 no-box no-labels overlay row 21.  
*/


/* TAH FG */
def var h-rebfl  as c format "x(1)"    no-undo.
{speccost.gva "new shared"}

def var update-line as c format "x(32)" no-undo. 


def temp-table rfresh no-undo
  FIELD seltype    as c  format "x(1)"
  FIELD lineno     /*like  oeel.lineno*/ as i format ">9"
  FIELD specnstype like  oeel.specnstype
  FIELD prod       like  oeel.shipprod
  FIELD whse       like  oeel.whse
  FIELD qty        as de format ">>>>9.99"
  FIEld avail-qty  as de format ">>>>9.99"
  FIELD cur-sell   as de format ">>>>>9.99"
  FIELD fut-sell   as de format ">>>>>9.99"
  FIELD sav-sell   as de format ">>>>>9.99"
  INDEX k-rfrsh
        lineno
        prod.

define query  q-rfresh for rfresh scrolling.
define browse b-rfresh query q-rfresh

  display rfresh.seltype
          rfresh.lineno
          rfresh.specnstype
          rfresh.prod
          rfresh.whse
          rfresh.qty
          rfresh.avail-qty
          rfresh.cur-sell
          rfresh.fut-sell
/*with 10 down centered no-hide no-labels*/
with 7 down centered no-hide no-labels
    title 
"   Ln#   Product                  Whse   CurQty   AvlQty   CurrPrc   FutrPrc ".
form
 b-rfresh
with frame f-rfresh no-labels width 80 overlay row /*9*/ 11 no-box.

form
  v-continue  at 1 label
      "You are accepting to convert all remaining quantities.  Continue?"
  with frame f-allqty side-labels row 5 centered overlay.


/*
def stream addon_aud.
output stream addon_aud to "/usr/tmp/addon_handling.aud" APPEND.
*/
/* CallMod */
define buffer b-CallCost for notes.                           
define var    v-SdiCallCost like icsw.stndcost no-undo.       
                                                              
                                                                               find b-CallCost where                                         
     b-CallCost.Cono = g-cono and                             
     b-CallCost.NotesType = "ZZ" and                          
     b-CallCost.Primarykey = "SdiCallCost" no-lock no-error.  
if avail b-CallCost then                                      
  v-SdiCallCost = dec(b-CallCost.noteln[1]).                  
else                                                          
  v-SdiCallCost = -9871234.987653.                            
                                                                  
/* CallMod */                                                                  



/* ------------------------- Event Declarations ----------------------------  */


on f12 of v-quoteno 
  do:
  run oeexclkup.p(output x-quoteno).
  assign v-quoteno = int(x-quoteno)
         w-batchnm = int(x-quoteno).
  display v-quoteno with frame f-top1.
  next-prompt v-quoteno with frame f-top1.
  next.
end.

on f12 of v-custlu in frame f-top1
  do:

  run TopCustlookup.
    
end.


on leave of v-topcustlu in frame f-toplu
  do:
  close query q-custlu.
  assign v-custbrowseopen = false.
  hide frame f-custlu.
  hide frame f-zcustinfo.
  
  on cursor-up back-tab.   
  on cursor-down tab.
end.


on entry of v-topcustlu in frame f-toplu
  do:
 
  on cursor-up cursor-up.  
  on cursor-down cursor-down.
  run DoCustLu (input v-TopCustlu,
                input "normal",
                input "f-toplu").
end.

/* FG look */
 
on CTRL-A of v-topcustlu in frame f-toplu
do:
  on cursor-up back-tab.
  on cursor-down tab.
   
  if avail arsc then do:
    run zarssllu2.p (input (string(arsc.custno))).
  end.

  if v-custbrowseopen = false then do: 
    on cursor-up back-tab.
    on cursor-down tab.
  end.
  else do:
    on cursor-up cursor-up.    /**&&**/
    on cursor-down cursor-down.
  end. 

  return.

end. 
/* FG look */
 



on f17,f18,f19,f20 of v-topcustlu in frame f-toplu
  do:
  close query q-custlu.
  assign v-custbrowseopen = false
         v-custbrowsekeyed = false.
 
  if keylabel(lastkey) = "f17" then
    assign v-custlutype = "l".
  else
  if keylabel(lastkey) = "f18" then
    assign v-custlutype = "p".
  else
  if keylabel(lastkey) = "f19" then
    assign v-custlutype = "x".
  else
  if keylabel(lastkey) = "f20" then
    assign v-custlutype = "z".
  else
  if keylabel(lastkey) = "f12" then
    assign v-custlutype = g-custlutype.

  run DoCustlu (input v-topcustlu,
                input (if keylabel(lastkey) = "f12" then
                         "normal"
                       else
                         "function"),
                input "f-toplu" ).
end.

on leave of v-shipto in frame f-top1
  do:
  close query q-Shiptolu.
  assign v-Shiptobrowseopen = false
         v-Shiptobrowsekeyed = false.
  hide frame f-Shiptolu.
  hide frame f-zcustinfo.
    
  on cursor-up back-tab.   
  on cursor-down tab.
end.


on entry of v-shipto in frame f-top1
  do:
   
  on cursor-up cursor-up.   
  on cursor-down cursor-down.
  assign v-Shiptolutype = g-custlutype.
  run DoShiptoLu (input v-shipto,
                  input "normal",
                  input "f-top1").
end.

on f12,f17,f18,f19,f20 of v-shipto in frame f-top1
  do:
  close query q-Shiptolu.
  assign v-Shiptobrowseopen = false
         v-Shiptobrowsekeyed = false.
  if keylabel(lastkey) = "f17" then
    assign v-Shiptolutype = "l".
  else
  if keylabel(lastkey) = "f18" then
    assign v-Shiptolutype = "p".
  else
  if keylabel(lastkey) = "f20" then
    assign v-Shiptolutype = "z".
  else
  if keylabel(lastkey) = "f12" then
    assign v-Shiptolutype = g-custlutype.

  run DoShiptolu (input v-shipto,
                  input (if keylabel(lastkey) = "f12" then
                           "normal"
                         else
                           "function"),
                  input "f-top1").
end.


on leave of v-custlu in frame f-top1
  do:
  close query q-custlu.
  assign v-custbrowseopen = false.
  hide frame f-custlu.
  hide frame f-zcustinfo.
  
  on cursor-up back-tab.   
  on cursor-down tab.
end.


on entry of v-custlu in frame f-top1
  do:
  on cursor-up cursor-up.   
  on cursor-down cursor-down.
  run DoCustLu (input v-Custlu,
                input "normal",
                input "f-top1").
    
end.


/* FG look */
 
on CTRL-A of v-custlu in frame f-top1
do:
  on cursor-up back-tab.
  on cursor-down tab.
   
  if avail arsc then do:
    run zarssllu2.p (input (string(arsc.custno))).
  end.

  if v-custbrowseopen = false then do: 
    on cursor-up back-tab.
    on cursor-down tab.
  end.
  else do:
    on cursor-up cursor-up.    /**&&**/
    on cursor-down cursor-down.
  end. 

  return.

end. 
/* FG look */
 



on f12,f17,f18,f19,f20 of v-custlu in frame f-top1
  do:
  close query q-custlu.
  assign v-custbrowseopen = false
         v-custbrowsekeyed = false.
 
  if keylabel(lastkey) = "f17" then
    assign v-custlutype = "l".
  else
  if keylabel(lastkey) = "f18" then
    assign v-custlutype = "p".
  else
  if keylabel(lastkey) = "f19" then
    assign v-custlutype = "x".
  else
  if keylabel(lastkey) = "f20" then
    assign v-custlutype = "z".
  else
  if keylabel(lastkey) = "f12" then
    assign v-custlutype = g-custlutype.
  /*
  run DoCustlu (input v-custlu,
                input (if keylabel(lastkey) = "f12" then
                         "normal"
                       else
                         "function"),
                input "f-top1").
  */
end.

on f12 of v-contact in frame f-top1
  do:
  run zsdicontact.p (input "uu",
                    input g-cono,
                    input v-custno,
                    input v-shipto,
                    input-output v-contact,
                    input-output v-cphone,
                    input-output v-phone,
                    input-output v-email).
  if v-cphone <> "" and lastkey <> 404 /*PF4*/ then
    assign v-phone = v-cphone.
  on cursor-up cursor-up.
  on cursor-down cursor-down.
  /* The above program by default sets keys back to SXE keys so here it's
     being set back
  */   
  display 
    v-contact
    v-phone
  with frame f-top1.
end.                    
                      


on f17,f18,f19,f20 of v-prod in frame f-mid1

  do:
  close query q-prodlu.
  assign v-prodbrowseopen = false
         v-prodbrowsekeyed = false.
 
  if keylabel(lastkey) = "f17" then
    assign v-prodlutype = "l".
  else
  if keylabel(lastkey) = "f18" then
    assign v-prodlutype = "p".
  else
  if keylabel(lastkey) = "f19" then
    assign v-prodlutype = "x".
  else
  if keylabel(lastkey) = "f20" then
    assign v-prodlutype = "v".
  else
  if keylabel(lastkey) = "f12" then
    assign v-prodlutype = g-prodlutype.
   
  run Doprodlu (input v-Prod,
                input (if keylabel(lastkey) = "f12" then
                         "normal"
                       else
                         "function") ).
   
end.

    
on leave of v-prod in frame f-mid1
  do:
  /*
  close query q-prodlu.
  assign v-prodbrowseopen = false
         v-prodbrowsekeyed = false.
  hide frame f-prodlu.
  */
  on cursor-up cursor-up.
  on cursor-down cursor-down.
  
end.
  
on entry of v-prod in frame f-mid1
  do:
  on cursor-up cursor-up.     
  on cursor-down cursor-down.
  if v-prod <> "" then
    do:
    assign v-prodlutype = g-prodlutype.
    
    run DoProdLu (input v-Prod,
                  input "normal").

  end.
end.

on leave of v-lineno in frame f-mid1      
  do:                                   
  on cursor-up cursor-up. /*back-tab.*/      
  on cursor-down cursor-down. /*tab.*/
end.

on entry of v-qty in frame f-mid1 do:
  /* %toms% */  
  if not v-conversion then do:
  /* %toms% */  
    on cursor-up back-tab.     
    on cursor-down tab.
    if v-prod <> "" then
      do:
      if v-prodlutype = " " then
        assign v-prodlutype = g-prodlutype.
      run DoProdLu (input v-Prod,
                    input "normal").
    end.
/* %toms% */  
  end.
/* %toms% */  
             
end.


on entry of v-sellprc in frame f-mid1 do:
  on cursor-up back-tab.    
  on cursor-down tab.
  if v-prod <> "" then
    do:
    if v-prodlutype = " " then
      assign v-prodlutype = g-prodlutype.
    run DoProdLu (input v-Prod,
                  input "normal").

  end.
end.



on entry of v-shipdt in frame f-mid1 do:
/* %toms% */  
  if not v-conversion then do:
/* %toms% */  

    on cursor-up back-tab.     
    on cursor-down tab.
    if v-prod <> "" then
      do:
      if v-prodlutype = " " then
        assign v-prodlutype = g-prodlutype.
      run DoProdLu (input v-Prod,
                    input "normal").

    end.
/* %toms% */  
  end.
/* %toms% */  

end.


on row-display of b-prodlu in frame f-prodlu
  do:
/* TAH 07/02/09  */
 assign v-prodbeg = icsp.statustype + " " + icsp.prod. 
 if avail icsp and 
    substr(icsp.user11,1,1) = "o" then 
   icsp.descrip[1]:dcolor in browse b-prodlu = 2.
 else  
   icsp.descrip[1]:dcolor in browse b-prodlu = 0.

/* TAH 07/02/09  */


  assign v-totavail = 0 
         v-totoo    = 0.
 
  for each icsw where icsw.cono =  g-cono  and
                      icsw.prod = icsp.prod  and
                      not can-do("c",substr(icsw.whse,1,1)) no-lock:
    find first icsd where 
               icsd.cono = g-cono and 
               icsd.whse = icsw.whse and
               icsd.salesfl = yes and
               icsd.custno  = 0   
               no-lock no-error.
    if avail icsd then
      assign v-totavail = v-totavail + 
             icsw.qtyonhand - icsw.qtyreserv - icsw.qtycommit
             v-totoo = v-totoo + (icsw.qtyonorder - (icsw.qtydemand +
                                                     icsw.qtybo)).
  end.
end.

/* -------------------------     MAIN          ----------------------------  */
assign g-currproc    = "oeexc"
       g-ourproc     = "oeexc"
       g-custno      = 0
       g-shipto      = ""
       g-orderno     = if lastkey >= 308 and
                          lastkey <= 309 and (g-callproc begins "oeilo" or
                                              g-callproc = "oeizs") then
                          g-orderno else 0
       v-quoteno     = 0
       v-shipto      = ""
       v-shiptonm    = ""
       v-shiptoaddr1 = ""
       v-shiptoaddr2 = ""
       v-shiptocity  = ""
       v-shiptost    = ""
       v-shiptozip   = ""
       v-shiptogeocd =  0
       v-hmode      = "a"   /* "a" for add mode on header;"c" for change */
       v-messagelit = " ".
       

for each t-lines:
  delete t-lines.
end.

on page-up page-up.
on page-down page-down.

for each sasoo where sasoo.cono   = g-cono and
                     sasoo.oper2  = g-operinit and
                     sasoo.user3 <> " ":
  assign v-quoteno = int(sasoo.user3)
         w-batchnm = int(sasoo.user3)
         F11-proc  = sasoo.user2
         sasoo.user3 = " "
         sasoo.user2 = " "
         v-promoprcwin   = if sasoo.promoprcwin = ""
                             then "y"
                           else sasoo.promoprcwin.
  
  for each OptionMoves:
    delete OptionMoves.
  end.
  
end.
find first sasos where sasos.cono = g-cono and
                       sasos.oper2 = g-operinits and
                       sasos.menuproc = "zxt1"
                       no-lock no-error.
if avail sasos and sasos.securcd[12] > 1 then
  assign v-faxpopup = true.
else
  assign v-faxpopup = false.
if avail sasos and sasos.securcd[15] > 1 then
  assign v-taknbyovr = true.
else
  assign v-taknbyovr = false.
assign v-prodlutype = g-prodlutype
       v-custlutype = g-custlutype.

assign ip-control = "C".
run oeexqoptions.p (input ip-control).

assign v-linebrowseopen = false.  

if v-jumpfl = yes then  
  assign v-framestate = "m1"
         v-CurrentKeyPlace = 2.
else
  assign v-CurrentKeyPlace = 0.
run FindNextFrame (input v-NormalMove,   /* Don't want lastkey to effect */
                   input-output v-CurrentKeyPlace ,
                   input-output v-framestate).

if v-framestate = "" then
  assign v-framestate = "t1"
         v-CurrentKeyPlace = 1.
  
assign v-takenby    = g-operinit   /* das - originall, g-operinits was used */
       v-lmode      = "a"  /* "a" for add mode on line;"c" for change */
       v-qmode      = "a".

/* if called from oeizo */
if lastkey >= 308 and
   lastkey <= 309 and (g-callproc begins "oeilo" or
                       g-callproc = "oeizs") then
  do:
  assign v-quoteno = g-orderno
         w-batchnm = g-orderno
         v-framestate = "m1".

end.

main:
do while true on endkey undo main, leave main:

  if (w-batchnm > 0 and F11-proc <> " ") or
     (w-batchnm > 0 and (g-callproc begins "oeilo" or
                         g-callproc = "oeizs")) then
    do:
    run LoadQuote (input-output errorfl).
    find first b-oeehb where b-oeehb.cono = g-cono and
               b-oeehb.batchnm = string(w-batchnm,">>>>>>>9") and
               b-oeehb.seqno   = 2 and
               b-oeehb.sourcepros = "ComQu"
               no-lock no-error.
    if avail b-oeehb and b-oeehb.stagecd = 9 then
      do:
      assign v-quoteno = 0.
      display v-quoteno with frame f-top1.
      leave main.
    end.
    if (g-callproc = "oeizs" or g-callproc begins "oeilo") and
        inquiryfl = yes then
      do:
      apply -2.
      leave main.
    end.
      
    if errorfl = yes then 
      do:
      /*
      next-prompt v-quoteno with frame f-top1.
      */
      assign errorfl = no.
      next main.
    end.
    assign ip-control = "C"
           v-prodlutype = g-prodlutype
           v-jumpfl = yes.
    
    run oeexqoptions.p (input ip-control).

    if F11-proc = "Top1" then
      run Top1.
    else if F11-proc = "Middle1" then
      run Middle1.
    else if F11-proc = "Bottom2" then
      run Bottom2.

    else if (g-callproc = "oeizs" or g-callproc begins "oeilo") and
             v-zsdiobfl = yes then
      do:
      run Middle1.
    end.
    else
      run Top1.
    assign F11-proc = " ".
  end. /* if w-batchnm > 0 */
  display with frame f-top1.
  
  if not v-conversion then do:
    if v-quoteno = 0 then
      display with frame f-oeexcline.
    else
      display with frame f-statusline.
  end.
  else
    display with frame f-statuslinex.
  if v-framestate = "b2" then 
    run Bottom2.
  else
  if v-framestate = "t1" then 
    run Top1.
  else     
  /* no need to call middle1 again if already called from above */
  if v-framestate = "m1" and (g-callproc <> "zsdiob" and
                       substr(g-callproc,1,5) <> "oeilo" and
                              g-callproc <> "oeizs") then
    do:
    run Middle1.
  end.
  else  
  if v-framestate = "xx" and not {k-jump.i} and manual-jump = no then 
    do:
    if g-callproc <> "zsdiob" /*and substr(g-callproc,1,5) <> "oeilo"*/ then
      run Clear-Frames.
    if v-quoteno = 0 then
       leave.
    else 
      do:
      if v-quoteno > 0 then 
        do:
        if keylabel(lastkey) = "PF4" then /*das-we don't want to go through */
          leave.                          /* this if oeexqinq was called    */
        find first oeelb where oeelb.cono = g-cono and
                         oeelb.batchnm = string(v-quoteno,">>>>>>>9") and
                         oeelb.seqno = 2
                         no-lock no-error.
        if not avail oeelb then 
          do:
          run oeexcdelqu.p. /* delete the quote */
          run Clear-Frames.
          run Initialize-All.
          if keylabel(lastkey) = "PF4" and v-quoteno <> 0 then
            do:
            run Clear-Frames.                                             
            run Initialize-All.                                           
            run Setup-Options.                                            
            display v-quoteno  v-notefl   
                    v-custno   
                    v-arnotefl           
                    v-custname v-takenby  v-custlu   v-shipto v-whse
                    v-contact  v-phone    v-custpo              v-terms
                    v-canceldt v-quotetot v-commtot  v-Hcommpct    
            with frame f-top1.                                            
            release oeehb no-error.                                       
            release oeelb no-error.                                       
            release notes no-error.                                       
            next main.                                                    
          end.
        end. /* not avail oeelb */
        else
          do:
          if (g-callproc = "oeizs" or g-callproc begins "oeilo") then
            do:
            run zoeexc95.p.
            /* readkey, pause 0 is required on return from zoeexc95.p */
            readkey pause 0.
            run Convert_Option.
            run Clear-Frames.
          end.
          if v-faxpopup and errorfl = no then
            do:
            run Clear-Frames.
            run oeexcg.p. 
          end.
        end.

        run Clear-Frames.
        run Initialize-All.
        run Setup-Options.
        display v-quoteno  v-notefl v-custno  v-arnotefl v-custname v-takenby   
                v-custlu   v-shipto v-contact v-phone    v-custpo 
                v-terms   v-whse
                v-canceldt v-quotetot v-commtot  v-Hcommpct
        with frame f-top1.
        release oeehb no-error.
        release oeelb no-error.
        release notes no-error.
        next main.
      end. /* v-quoteno > 0 */
      if v-quoteno <> 0 and not {k-jump.i} then 
        do:
        run Clear-Frames.
        if v-faxpopup and errorfl = no then
          run oeexcg.p. 
        run Clear-Frames.
        run Initialize-All.
        run Setup-Options.
        display v-quoteno  v-notefl v-custno  v-arnotefl v-custname v-takenby   
                v-custlu   v-shipto v-contact v-phone    v-custpo  
                v-terms    v-whse
                v-canceldt v-quotetot v-commtot  v-Hcommpct
        with frame f-top1.
        release oeehb no-error.
        release oeelb no-error.
        release notes no-error.
        next main.
      end.
      else
        leave.
    end. /* else v-quoteno > 0 */
    leave.
  end. /* v-framestate = "xx" and not {k-jump.i} */
  
  if manual-jump then do:
    if v-top1 = yes then
      assign F11-proc = "Top1".
    if v-middle1 = yes then
      assign F11-proc = "Middle1".
    if v-bottom2 = yes then
      assign F11-proc = "Bottom2".
    assign manual-jump = no.
    release oeehb no-error.
    release oeelb no-error.
    release notes no-error.
    next main.
  end.
  
  if {k-jump.i} then 
    do:
    leave.
  end.

  if keylabel(lastkey) = "PF4" and not manual-jump then
    do:
    if v-quoteno = 0 then
      do:
      for each bpeh where 
               bpeh.cono = g-cono and
               bpeh.bpid = string(v-quoteno,">>>>>>>9") + "C":
        for each bpel where bpel.cono = g-cono and
                            bpel.bpid = bpeh.bpid:
          delete bpel.
        end.
        delete bpeh.
      end.
    end.
    if de-fl = yes then /* das - deadly embrace */
      do:
      assign v-quoteno = 0
             w-batchnm = 0
             w-key     = 13
             de-fl     = no.
      apply lastkey.
      next main.
    end.
    if v-quoteno = 0 then
      do:
      for each bpeh where 
               bpeh.cono = g-cono and
               bpeh.bpid = string(v-quoteno,">>>>>>>9") + "C":
        for each bpel where bpel.cono = g-cono and
                            bpel.bpid = bpeh.bpid:
          delete bpel.
        end.
        delete bpeh.
      end.
      leave main.
    end. /* v-quoteno = 0 */
    else 
      do:
      if v-quoteno > 0 then 
        do:                                 /* check if oeexqinq was called */
        if avail oeehb and oeehb.stagecd = 9 then leave.
        find first oeelb where oeelb.cono = g-cono and
                               oeelb.batchnm = string(v-quoteno,">>>>>>>9") and
                               oeelb.seqno = 2
                               no-lock no-error.

        if not avail oeelb then 
          do:
          run oeexcdelqu.p.  /* delete the quote */
          run Clear-Frames.
          run Initialize-All.
          if keylabel(lastkey) = "PF4" and v-quoteno <> 0 then 
            do:
            run Clear-Frames.
            run Initialize-All.
            run Setup-Options.
            display v-quoteno  v-notefl   v-custno   v-arnotefl
                    v-custname v-takenby  v-custlu   v-shipto  
                    v-contact  v-phone    v-custpo    
                    v-terms    v-whse
                    v-canceldt v-quotetot v-commtot  v-Hcommpct
            with frame f-top1.
            release oeehb no-error.
            release oeelb no-error.
            release notes no-error.
            next main.
          end. /* avail b-oeehb */
        end. /* not avail oeelb */
        else /* avail oeelb */
          do:
          run zoeexc95.p.
          /* readkey, pause 0 is required on return from zoeexc95.p */
          readkey pause 0.
          run Convert_Option.
          if v-nextmainfl = yes then
            next main.
        end. /* else avail oeelb */
        run Clear-Frames.
        run Initialize-All.
        run Setup-Options.
        display v-quoteno  v-notefl v-custno  v-arnotefl v-custname v-takenby   
                v-custlu   v-shipto v-contact v-phone    v-custpo 
                v-terms    v-whse
                v-canceldt v-quotetot v-commtot  v-Hcommpct
        with frame f-top1.
        release oeehb no-error.
        release oeelb no-error.
        release notes no-error.
        next main.
      end. /* if v-quoteno > 0 */
    end. /* else v-quoteno <> 0 */
  end. /* if keylabel(lastkey) = "PF4" or {k-jump.i} */
           
  if keylabel(lastkey) = "PF1" then 
    do:
    /* leave if we already were in middle 1 and we're ready to get out */
    if v-quoteno > 0 and v-framestate = "m1" and
      (g-callproc = "oeizf" or g-callproc begins "oeilo") then
      assign v-framestate = "xx".  
    assign v-cnvfl = no.
    if v-quoteno > 0 and v-framestate = "xx" then 
      do:
      find first oeelb where oeelb.cono = g-cono and
                             oeelb.batchnm = string(v-quoteno,">>>>>>>9") and
                             oeelb.seqno = 2
                             no-lock no-error.

      if not avail oeelb then 
        do:
        run oeexcdelqu.p.
        run Clear-Frames.
        run Initialize-All.
        run Setup-Options.
        display v-quoteno  v-notefl   v-custno   v-arnotefl v-custname 
                v-takenby  v-custlu   v-shipto   v-contact  v-phone    
                v-custpo    v-terms    v-whse
                v-canceldt v-quotetot v-commtot  v-Hcommpct
        with frame f-top1.
        release oeehb no-error.
        release oeelb no-error.
        release notes no-error.
        next main.
      end. /* if not avail oeelb */
      else /* avail oeelb */
        do:
        run zoeexc95.p.
        /* readkey, pause 0 is required on return from zoeexc95.p */
        readkey pause 0.
        run Convert_Option.
      end. /* else */
      if v-quoteno <> 0 then 
        do:
        if v-faxpopup and v-cnvfl = no and errorfl = no then
          do:
          run Clear-Frames.
          run oeexcg.p. 
        end.
        run Clear-Frames.
        run Initialize-All.
        run Setup-Options.
        display v-quoteno  v-notefl v-custno  v-arnotefl v-custname v-takenby   
                v-custlu   v-shipto v-contact v-phone    v-custpo 
                v-terms    v-whse
                v-canceldt v-quotetot v-commtot  v-Hcommpct
        with frame f-top1.
        release oeehb no-error.
        release oeelb no-error.
        release notes no-error.
        next main.
      end.
      else
        leave.
    end. /* if v-quoteno > 0 */
  end. /* if lastkey = "F1" */

  if keylabel(lastkey) = "PF4" then
    do:
    if w-batchnm > 0 then 
      do:
      run Clear-Frames.
      run Initialize-All.
      run Setup-Options.
      display v-quoteno  v-notefl v-custno  v-arnotefl v-custname v-takenby   
              v-custlu   v-shipto v-contact v-phone    v-custpo 
              v-terms    v-whse
              v-canceldt v-quotetot v-commtot  v-Hcommpct
      with frame f-top1.
      release oeehb no-error.
      release oeelb no-error.
      release notes no-error.
      next main.
    end.
    /*
    else
      do:
      for each bpeh where 
               bpeh.cono = g-cono and
               bpeh.bpid = string(w-batchnm,">>>>>>>9") + "C":
        for each bpel where bpel.cono = g-cono and
                            bpel.bpid = bpeh.bpid:
          delete bpel.
        end.
        delete bpeh.
      end.
    end.
    */
  end.
end. /* main */




{j-all.i "frame f-top1"}

on cursor-up back-tab.
on cursor-down tab.


/* -------------------------------------------------------------------------  */
Procedure Top1:
/* -------------------------------------------------------------------------  */
  hide frame f-bot2i no-pause.
  hide frame f-bot2e no-pause.
  hide frame f-mid1  no-pause.
  hide frame f-lines no-pause.

  hide message no-pause.
  assign v-top1 = yes.

  Top1Loop:
  do with frame f-top1 on endkey undo, leave Top1Loop:
    update 
           v-quoteno when v-quoteno = 0
           v-takenby when v-taknbyovr = true
           v-custlu
           v-shipto
           v-contact
           v-phone
           v-custpo
           v-canceldt
          /* v-quotetot*/
          /* v-commtot */
           v-Hcommpct
    go-on ({k-sdigoon.i})
    editing:
    readkey.

     put screen row 21 col 1 
     color message
     "F6-Customer   F7-LINES     F8-Extend     F9-ShipTo Addr     F10-Notes             ".

      if keylabel(lastkey) = "F8" and v-custno > 0 and v-quoteno > 0 then
        do:
        run oeexcheus.p.
        readkey pause 0.
      end.
      if keylabel(lastkey) = "F9" and v-custno = 0 then
        do:
        if input v-quoteno = 0 and g-orderno > 0 then
          do:
          find oeehb where 
               oeehb.cono = g-cono and  
               oeehb.batchnm = string(g-orderno,">>>>>>>9") and
               oeehb.seqno = 2 and
               oeehb.sourcepros = "ComQu"
               no-lock no-error.
          if avail oeehb then
            assign v-quoteno = g-orderno.
        end.
        else
          assign g-orderno = input v-quoteno.
        run Clear-Frames.
        run oeexcg.p. 
        run Clear-Frames.
        run Initialize-All.
        run Setup-Options.
        display v-quoteno  v-notefl v-custno  v-arnotefl v-custname v-takenby   
                v-custlu   v-shipto v-contact v-phone    v-custpo 
                v-terms    v-whse
                v-canceldt v-quotetot v-commtot  v-Hcommpct
        with frame f-top1.
        next. /*leave Top1Loop.*/
      end.
      
      if keylabel(lastkey) = "F10" and v-custno = 0 then
        do:
        message "Valid Customer is required".
        next-prompt v-custno with frame f-Top1.
        next.
      end.
      
      if keylabel(lastkey) = "F7" and v-custno = 0 then
        do:
        message "Valid Customer is required".
        next-prompt v-custno with frame f-Top1.
        next.
      end.
      if keylabel(lastkey) = "F11" or keylabel(lastkey) = "PF2" then
        do:
        if keylabel(lastkey) = "F11" then
          run Tag_SASOO (input w-batchnm,
                         input "Top1   ").
        if keylabel(lastkey) = "PF2" then apply lastkey.
        if keylabel(lastkey) = "PF2" then
          do:
         put screen row 21 col 1 
         color message
         "F6-CUSTOMER   F7-Lines                  F9-ShipTo Addr      F10-Notes            ".
         view frame f-statusline.
        end.
        else
          leave Top1Loop.
      end.
      if keylabel(lastkey) = "PF4" then 
        do: 
        if frame-field = "v-custlu" or frame-field = "v-quoteno"
                                    or de-fl = yes then
          do:
          if v-quoteno = 0 then
            do:
            for each bpeh where 
                     bpeh.cono = g-cono and
                     bpeh.bpid = string(v-quoteno,">>>>>>>9") + "C":
              for each bpel where bpel.cono = g-cono and
                                  bpel.bpid = bpeh.bpid:
                delete bpel.
              end.
              delete bpeh.
            end.
          end. /* v-quoteno = 0 */
          leave Top1Loop.
        end.
        else 
          do:
          if v-quoteno = 0 then
            do:
            for each bpeh where bpeh.cono = g-cono and
                     bpeh.bpid = string(v-quoteno,">>>>>>>9") + "C":
              for each bpel where bpel.cono = g-cono and
                                  bpel.bpid = bpeh.bpid:
                delete bpel.
              end.
              delete bpeh.
            end.
            assign v-shipto   = ""
                   v-contact  = ""
                   v-phone    = ""
                   v-custpo   = ""
                   v-currency = ""
                   v-terms    = "".
            display v-shipto
                    v-contact
                    v-phone
                    v-custpo
                    
                    v-terms
            with frame f-top1.
          end.
          next-prompt v-custlu with frame f-top1.
          run DoCustLu (input v-custlu,
                        input "normal",
                        input "f-top1" ).
          next.
        end. /* frame-field not v-custlu or v-quoteno */
      end. /* lastkey <> "PF4" */

      if input v-quoteno > 0 and can-do("9,13,401",string(lastkey)) and
         not v-quoteloadfl and frame-field = "v-quoteno" then 
        do:
        assign v-quoteno = input v-quoteno.
        /* das - deadly embrace logic */
        do transaction:
          assign de-fl = no.
          find oeehb where 
               oeehb.cono = g-cono and  
               oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
               oeehb.seqno = 2 and
               oeehb.sourcepros = "ComQu"
               no-error.
          if avail oeehb then
            do:
            if oeehb.inprocessfl = yes and oeehb.user2 <> g-operinit then
              do:
              message "Comm Order " v-quoteno 
                      " in use by " oeehb.user2 + " (5608)".
              assign de-fl = yes.
              bell.
              next-prompt v-quoteno with frame f-top1.
              release oeehb.
              next.
            end.
            else
              assign de-fl = no.
            assign oeehb.inprocessfl = yes
                   oeehb.user2       = g-operinit.
          end. /* if avail oeehb */
        end. /* do transaction */
        
        assign w-batchnm = input v-quoteno
               v-quoteno = input v-quoteno
               g-orderno = input v-quoteno.
        run LoadQuote (input-output errorfl).
        if errorfl = yes then
          do:
          assign v-quoteno = 0
                 w-batchnm = 0
                 errorfl   = no.
          display v-quoteno with frame f-top1.
          next-prompt v-quoteno with frame f-top1.
          /*assign errorfl = no.*/  /*moved above*/
          next.
        end.
        leave Top1Loop.
      end. /* input v-quoteno > 0 */
      
      if input v-quoteno = 0 and can-do("9,13,401",string(lastkey)) then
        do:
        hide message no-pause.
        /*
        if v-canceldt = ? then
          do:
          /*FG wanted canceldt to default to ?*/
          assign v-canceldt = TODAY + 365.
          display 
                  v-canceldt 
                  v-quotetot 
                  v-commtot  v-Hcommpct
                  with frame f-top1.
          if not v-conversion then
            display with frame f-statusline.
          else
            display with frame f-statuslinex.
          if v-taknbyovr = true then
            next-prompt v-takenby with frame f-top1.
          else
            next-prompt v-custlu with frame f-top1.
          next.
        end. /* v-canceldt = ? */
        else
          do:
          */
          if not v-conversion then
            display with frame f-statusline.
          else
            display with frame f-statuslinex.
        /*end.*/
      end. /* v-quoteno = 0 and can-do("9,13,401",string(lastkey)) */

      if can-do("f6,f7,f10",keylabel(lastkey)) and v-custbrowseopen = true then
        do:
        if arsc.custno <> v-custno then 
          {w-arsc.i v-custno no-lock}
        run Assign-Update-Quote.
        display v-quoteno with frame f-top1.
        close query q-custlu.
        assign v-custbrowseopen = false
               v-manualframejump = true
               manual-jump = true.
        hide frame f-custlu.
        hide frame f-zcustinfo.
        apply lastkey.
        leave Top1Loop.
      end.
      if can-do("f6,f7,f10",keylabel(lastkey)) and 
         v-Shiptobrowseopen = true then
        do:
        run Assign-Update-Quote.
        display v-quoteno with frame f-top1.
        close query q-Shiptolu.
        assign v-Shiptobrowseopen = false
               v-Shiptobrowsekeyed = false
               v-manualframejump = true
               manual-jump = true.
        hide frame f-Shiptolu.
        hide frame f-zcustinfo.
        apply lastkey.
        leave Top1Loop.
      end.

      if not avail oeehb then
        find oeehb where oeehb.cono = g-cono and  
                         oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
                         oeehb.seqno = 2 and
                         oeehb.sourcepros = "ComQU"
                         no-lock no-error.
      if avail oeehb then 
        do:
        assign v-shiptonm    = if v-shiptonm    = "" then oeehb.shiptonm
                               else v-shiptonm
               v-shiptoaddr1 = if v-shiptoaddr1 = "" then oeehb.shiptoaddr[1]
                               else v-shiptoaddr1
               v-shiptoaddr2 = if v-shiptoaddr2 = "" then oeehb.shiptoaddr[2]
                               else v-shiptoaddr2
               v-shiptocity  = if v-shiptocity = "" then CAPS(oeehb.shiptocity)
                               else v-shiptocity
               v-shiptost    = if v-shiptost    = "" then CAPS(oeehb.shiptost)
                               else v-shiptost
               v-shiptozip   = if v-shiptozip   = "" then oeehb.shiptozip
                               else v-shiptozip
               v-shiptogeocd = if v-shiptogeocd = 0 then oeehb.geocd
                               else v-shiptogeocd.
      end. /* avail oeehb */
      
      if keylabel(lastkey) = "F9" then 
        do:
        if frame-field = "v-shipto" then
          do:
          message "F9-ShipTo Addr cannot be accessed - Must be in Contacts" +
                  " field or beyond".
          next-prompt v-shipto with frame f-top1.
          next.
        end.
        run shiptopopup.p (input        v-quoteno,
                           input        v-custno,
                           input        v-shipto,
                           input-output v-shiptonm,
                           input-output v-shiptoaddr1,
                           input-output v-shiptoaddr2,
                           input-output v-shiptocity,
                           input-output v-shiptost,
                           input-output v-shiptozip,
                           input-output v-shiptogeocd,
                           output       w-shipchgfl).
        find oeehb where oeehb.cono = g-cono and  
                         oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
                         oeehb.seqno = 2 and
                         oeehb.sourcepros = "ComQu"
                         no-lock no-error.
        assign v-taxablefl = if avail oeehb then
                               oeehb.taxablefl
                             else
                               v-taxablefl.
        readkey pause 0.
      end. /* F9 was pressed */
      
      if frame-field = "v-takenby" then
        do:
        if can-do("9,13,401,502,508",string(lastkey)) then
          do:
          next-prompt v-custlu with frame f-top1.
          next.
        end.
      end. /* frame-field = "v-takenby" */
      
      if frame-field = "v-custlu" then 
        do:
        assign v-custlu = input v-custlu. /* needed for input in DoCustLu */
        if not can-do("9,13,404,501,502,507,508",string(lastkey)) then
          assign v-custbrowsekeyed = false.
        if lastkey >= 33 and lastkey <= 122 then
          assign typed-in = yes.
        if (not can-do("9,21,13,501,502,507,508",string(lastkey)) and
            keyfunction(lastkey) ne "go" and
          /* FG look */
            keylabel(lastkey) ne "CTRL-A" and
          /* FG look */
            keyfunction(lastkey) ne "end-error") then 
          do:
          apply lastkey.
          assign v-custlu = input v-custlu. /* needed for input in DoCustLu */
          if can-do("13,401,503,504",string(lastkey)) /*return, go, arrow R/L*/
             and not avail arsc then 
            do:
            run err.p(4303).
            next-prompt v-custlu with frame f-top1.
            next.
          end.
          on cursor-up cursor-up.
          on cursor-down cursor-down.
          if can-do("317,318,319,320",string(lastkey)) then
            run DoCustlu (input v-custlu,
                          input "function",
                          input "f-top1").
          else
            run DoCustlu (input v-custlu,
                          input "normal",
                          input "f-top1").
          next.
        end. /* if not a "movement" key */
                  /* FG look */
        else 
        if keylabel(lastkey) = "CTRL-A" then do:
          apply lastkey.
          
          run MoveCustLu (input 502,
                          input "f-top1").
          assign v-custbrowsekeyed = true.
          
          next.         
        end.
                  /* FG look */
 
        else 
          do:
          if (lastkey = 501 or
              lastkey = 502 or
              lastkey = 507 or
              lastkey = 508) and v-custbrowseopen = true then 
            do:
            if not avail arsc then 
              do:
              run err.p(4303).
              next-prompt v-custlu with frame f-top1.
               next.
            end.
            run MoveCustLu (input lastkey,
                            input "f-top1").
            assign v-custbrowsekeyed = true.
            next.
          end.
          if can-do("9,13,401,503,504",string(lastkey)) /*return,go,arrow R/L*/
                and not avail arsc then 
            do:
            run err.p(4303).
            next-prompt v-custlu with frame f-top1.
            next.
          end.
          if can-do("9,13,401,503,504",string(lastkey)) /*return,go,arrow R/L*/
             and avail arsc and arsc.selltype = "n" and v-5680fl = no then
            do:
            assign v-5680fl = yes.
            run warning.p(5680).
          end.
        end. /* else a "movement" key */
        
        if input v-custlu <> v-hcustlu or (avail arsc and
                 v-custno <> arsc.custno) then 
          do:
          if v-custno <> 0 then 
            do:
            /* check if user changed customer manually */
            if v-custbrowsekeyed = no and typed-in = no then 
              do:
              {w-arsc.i v-custno no-lock}
              if lastkey = 401 then 
                do:
                assign v-shipto      = if v-shipto = "" and avail arsc then
                                         arsc.shipto else ""
                       v-shiptonm    = if v-shiptonm    = "" and 
                                          v-shiptoaddr1 = "" and
                                          v-shiptoaddr2 = "" and
                                          v-shiptocity  = "" and
                                          v-shiptost    = "" and avail arsc
                                         then arsc.name
                                       else v-shiptonm
                       v-shiptoaddr1 = if v-shiptonm    = "" and
                                          v-shiptoaddr1 = "" and 
                                          v-shiptoaddr2 = "" and
                                          v-shiptocity  = "" and 
                                          v-shiptost    = "" and avail arsc
                                         then arsc.addr[1]
                                       else v-shiptoaddr1
                       v-shiptoaddr2 = if v-shiptonm    = "" and
                                          v-shiptoaddr1 = "" and
                                          v-shiptoaddr2 = "" and 
                                          v-shiptocity  = "" and
                                          v-shiptonm    = "" and avail arsc 
                                         then arsc.addr[2]
                                       else v-shiptoaddr2
                       v-shiptocity  = if v-shiptonm    = "" and
                                          v-shiptoaddr1 = "" and
                                          v-shiptoaddr2 = "" and
                                          v-shiptocity  = "" and
                                          v-shiptonm    = "" and avail arsc 
                                         then arsc.city
                                       else v-shiptocity
                       v-shiptost    = if v-shiptonm    = "" and
                                          v-shiptoaddr1 = "" and
                                          v-shiptoaddr2 = "" and
                                          v-shiptocity  = "" and
                                          v-shiptost    = "" and avail arsc 
                                         then arsc.state
                                       else v-shiptost
                       v-shiptozip   = if v-shiptonm    = "" and
                                          v-shiptoaddr1 = "" and
                                          v-shiptoaddr2 = "" and
                                          v-shiptocity  = "" and
                                          v-shiptost    = "" and
                                          v-shiptozip   = "" and avail arsc 
                                         then arsc.zip
                                       else v-shiptozip
                       v-shiptogeocd = if v-shiptogeocd = 0   and avail arsc 
                                         then arsc.geocd
                                       else v-shiptogeocd
                       v-whse        = if avail arsc then arsc.whse else ""
                       v-custpo      = if avail arsc then arsc.custpo else ""
                       v-slsrepout   = if avail arsc then arsc.slsrepout
                                       else ""
                       v-slsrepin    = if avail arsc then arsc.slsrepin 
                                       else "".
                run Assign-Update-Quote.
                display v-quoteno 
                        v-shipto 
                        v-custpo with frame f-top1.
                leave Top1Loop.
              end.
              else
                do:
                assign v-Shiptolutype = g-custlutype.
                run DoShiptoLu (input v-shipto,
                                input "normal",
                                input "f-top1" ).
                next-prompt v-shipto with frame f-top1.
                next.
              end.
            end.
            assign v-chgfl  = yes
                   typed-in = no.
            chgcust:
            do on endkey undo chgcust, leave chgcust:
              update v-chgfl label
    "You are changing the customer on this quote.  Do you want to continue?"
     with frame f-chg side-labels row 5 centered overlay.
   
              if keyfunction(lastkey) = "GO" or
                keyfunction(lastkey) = "RETURN" then do:
                hide frame f-chg no-pause.
                leave chgcust.
              end.
            end. /* chgcust */
          end. /* if v-custno <> 0 */
          if v-chgfl = no then 
            do:
            {w-arsc.i v-hcustno no-lock "b-"}
            if avail b-arsc then
              {w-arsc.i v-hcustno no-lock}
            if v-shipto <> "" then
              {w-arss.i v-hcustno v-shipto no-lock}
          end.
          if v-chgfl = yes then
            do:
            assign g-shipto      = ""
                   v-shipto      = ""
                   v-shiptonm    = ""
                   v-shiptoaddr1 = ""
                   v-shiptoaddr2 = ""
                   v-shiptocity  = ""
                   v-shiptost    = ""
                   v-shiptozip   = ""
                   v-shiptogeocd = 0
                   v-custpo      = ""
                   v-taxablefl   = no
                   v-5680fl      = no
                   v-slsrepout   = ""
                   v-slsrepin    = ""
                   v-custpo      = ""
                   v-currencyty  = ""
                   v-currency    = ""
                   v-terms       = "".
       
            find first t-lines no-lock no-error.
              
            for each oeehb where 
                     oeehb.cono = g-cono and  
                     oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
                     oeehb.seqno      = 2 and
                     oeehb.sourcepros = "ComQu":
              assign oeehb.whse          = if avail t-lines then oeehb.whse
                                           else ""
                     oeehb.shipto        = ""
                     oeehb.shiptonm      = ""
                     oeehb.shiptoaddr[1] = ""
                     oeehb.shiptoaddr[2] = ""
                     oeehb.shiptocity    = ""
                     oeehb.shiptost      = ""
                     oeehb.shiptozip     = ""
                     oeehb.geocd         = 0
                     oeehb.taxablefl     = no
                     oeehb.nontaxtype    = ""
                     oeehb.custpo        = ""
                     oeehb.custno        = if avail arsc then arsc.custno
                                           else oeehb.custno
                     oeehb.divno         = if avail arsc then arsc.divno
                                           else oeehb.divno
                     oeehb.termstype     = if avail arss then arss.termstype
                                           else
                                             if avail arsc then arsc.termstype
                                             else oeehb.termstype
                     oeehb.codfl         = if avail arss and arss.termstype =
                                                             "COD" then yes
                                           else 
                                           if avail arsc and arsc.termstype = 
                                                             "COD" then yes
                                           else no
                                           /*if yes then no else no*/
                     oeehb.user5         = " "
                     oeehb.user6         = 0
                     oeehb.countrycd     = "".
            end.
          end.                                               

          if w-batchnm <> 0 then 
            do transaction:
            find oeehb where 
                 oeehb.cono       = g-cono and   
                 oeehb.batchnm    = string(w-batchnm,">>>>>>>9") and
                 oeehb.seqno      = 2 and
                 oeehb.sourcepros = "ComQu"
                 no-error.
            if avail oeehb then 
              do:
              assign v-hcustlu     = v-custlu
                     v-custlu      = arsc.lookupnm
                     v-custno      = arsc.custno
                     g-custno      = arsc.custno
                     v-custname    = substr(arsc.name,1,20)
                     v-whse        = arsc.whse
                     v-slsrepout   = arsc.slsrepout
                     v-slsrepin    = arsc.slsrepin
                     o-slsrepout   = arsc.slsrepout
                     v-currency    = if arsc.currencyty = "CN" then
                                        "CURRENCY: CN"
                                     else if arsc.currencyty = "LB" then
                                        "CURRENCY: LB"
                                     else " "
                     v-terms       = if avail arss then arss.termstype
                                     else arsc.termstype
                     v-arnotefl    = arsc.notesfl
                     v-shipto      = if v-chgfl = yes then "" else v-shipto
                     v-contact     = if v-chgfl = yes then "" else v-contact
                     v-phone       = if v-chgfl = yes then "" else v-phone
                     v-custpo      = if v-chgfl = yes then arsc.custpo else ""
                     v-notefl      = if v-chgfl = yes then "" else v-notefl
                     v-canceldt    = /*if v-chgfl = yes then today + 360
                                     else*/ v-canceldt
                     v-shipto      = if v-chgfl = yes then arsc.shipto else ""
                     v-shiptonm    = if v-chgfl = yes then arsc.name else ""
                     v-shiptoaddr1 = if v-chgfl = yes then arsc.addr[1] else ""
                     v-shiptoaddr2 = if v-chgfl = yes then arsc.addr[2] else ""
                     v-shiptocity  = if v-chgfl = yes then arsc.city  else ""
                     v-shiptost    = if v-chgfl = yes then arsc.state else ""
                     v-shiptozip   = if v-chgfl = yes then arsc.zipcd else ""
                     v-slsrepout   = if v-chgfl = yes then arsc.slsrepout 
                                     else ""
                     v-slsrepin    = if v-chgfl = yes then arsc.slsrepin 
                                     else ""
                     v-shiptogeocd = 0
                     oeehb.termstype = if avail arss then arss.termstype
                                       else
                                         if avail arsc then arsc.termstype
                                         else oeehb.termstype
                     oeehb.codfl   = if avail arss and arss.termstype =
                                                              "COD" then yes
                                     else 
                                       if avail arsc and arsc.termstype = 
                                                              "COD" then yes
                                       else no.
              assign oeehb.taxablefl = if avail arss then
                                         if arss.taxablety = "n"
                                           then no
                                         else yes
                                       else
                                       if avail arsc then
                                         if arsc.taxablety = "n"
                                           then no
                                         else yes
                                       else v-taxablefl
                     v-taxablefl     = oeehb.taxablefl
                     oeehb.nontaxtype = if v-chgfl = yes then
                                          if avail arss then arss.nontaxtype
                                          else
                                            if avail arsc then arsc.nontaxtype
                                            else
                                              oeehb.nontaxtype
                                        else
                                          oeehb.nontaxtype.
              display v-notefl
                      v-custno
                      v-arnotefl
                      v-custname
                      v-whse
                      v-takenby
                      v-shipto
                      v-contact
                      v-phone
                      v-custpo
                      
                      v-terms
                      v-canceldt
                      v-quotetot
                      v-commtot  v-Hcommpct
                      v-custlu 
              with frame f-top1.
              if v-arnotefl = "!" then 
                do:
                run noted.p(yes,"c",string(v-custno),"").
                /*next-prompt v-shipto with frame f-top1.
                  next.*/
                readkey pause 0.
              end.
              run Update_OEEHB.
              
              for each oeelb where oeelb.cono    = g-cono and
                                   oeelb.batchnm = oeehb.batchnm and
                                   oeelb.seqno   = oeehb.seqno:
                assign oeelb.user7 = oeehb.custno.
              end.
              
              run Quote-Total.
              hide frame f-mid1.
              assign v-linebrowseopen = no.
            end. /* if avail oeehb */
          end. /* do transaction */
          else 
            do:
            assign v-hcustlu     = v-custlu
                   v-custlu      = arsc.lookupnm
                   v-custno      = arsc.custno
                   g-custno      = arsc.custno
                   v-custname    = substr(arsc.name,1,20)
                   v-whse        = arsc.whse
                   v-slsrepout   = arsc.slsrepout
                   v-slsrepin    = arsc.slsrepin
                   o-slsrepout   = arsc.slsrepout
                   v-arnotefl    = arsc.notesfl
                   v-currency    = if arsc.currencyty = "CN" then
                                     "CURRENCY: CN"
                                   else if arsc.currencyty = "LB" then
                                     "CURRENCY: LB"
                                   else " "
                   v-terms       = if avail arss then arss.termstype
                                   else arsc.termstype
                   v-custpo      = arsc.custpo
                   v-shipto      = arsc.shipto
                   v-shiptonm    = arsc.name 
                   v-shiptoaddr1 = arsc.addr[1]
                   v-shiptoaddr2 = arsc.addr[2]
                   v-shiptocity  = arsc.city
                   v-shiptost    = arsc.state
                   v-shiptozip   = arsc.zip
                   v-shiptogeocd = arsc.geocd
                   v-custpo      = arsc.custpo.

            display  v-custname
                     v-custno
                     v-arnotefl
                     v-whse
                     v-takenby
                     v-custlu
                     v-shipto
                     v-terms
                     v-custpo
            with frame f-top1.
            if v-arnotefl = "!" then 
              do:
              run noted.p(yes,"c",string(v-custno),"").
              readkey pause 0.
            end.
          end. /* else not avail oeehb */

        end. /* if customer has changed */
        
        if (lastkey = 13 or lastkey = 9 or keyfunction(lastkey) = "go")
            and v-custbrowsekeyed and avail arsc and
            arsc.custno = v-custno then 
          do:
          if v-arnotefl = "!" then 
            do:
            run noted.p(yes,"c",string(v-custno),"").
            readkey pause 0.
          end.
          assign v-custlu      = arsc.lookupnm 
                 v-custno      = arsc.custno
                 g-custno      = arsc.custno
                 v-custname    = substr(arsc.name,1,20)
                 v-whse        = arsc.whse
                 v-currency    = if arsc.currencyty = "CN" then
                                   "CURRENCY: CN"
                                 else if arsc.currencyty = "LB" then
                                   "CURRENCY: LB"
                                 else " "
                 v-terms       = if avail arss then arss.termstype
                                 else arsc.termstype
                 v-slsrepout   = arsc.slsrepout
                 v-slsrepin    = arsc.slsrepin
                 o-slsrepout   = arsc.slsrepout
                 v-arnotefl    = arsc.notesfl
                 v-custpo      = arsc.custpo
                 v-shipto      = arsc.shipto
                 v-shiptonm    = if v-shiptonm <> "" then arsc.name
                                 else " "
                 v-shiptoaddr1 = if v-shiptoaddr1 <> "" then arsc.addr[1]
                                 else " "
                 v-shiptoaddr2 = if v-shiptoaddr2 <> "" then arsc.addr[2]
                                 else " "
                 v-shiptocity  = if v-shiptocity <> "" then arsc.city
                                 else " "
                 v-shiptost    = if v-shiptost <> "" then arsc.state
                                 else " "
                 v-shiptozip   = if v-shiptozip <> "" then arsc.zipcd
                                 else " "
                 v-shiptogeocd = if v-shiptogeocd <> 0 then arsc.geocd
                                 else 0.
          display  v-custname
                   v-custno
                   v-arnotefl
                   v-whse
                   v-takenby
                   v-custlu
                   
                   v-terms
          with frame f-top1.
        
          display v-custlu with frame f-top1.
          assign v-custbrowsekeyed = false.
          hide frame f-custlu.
          hide frame f-zcustinfo.
        
          if v-arnotefl = "!" then 
            do:
            run noted.p(yes,"c",string(v-custno),"").
            readkey pause 0.
          end.
        end. /* lastkey was Return or Go */
        else
          assign v-custbrowsekeyed = false
                 typed-in = false.
        if lastkey = 401 and v-custlu <> " " and 
                             frame-field <> "v-Hcommpct" then
          do:
          hide frame f-custlu.   
          hide frame f-zcustinfo.
          next-prompt v-Hcommpct with frame f-Top1.
          next.
        end.
      end. /* frame-field = v-custlu */
      
      if lastkey = 401 and v-custlu <> " " and 
                           frame-field <> "v-Hcommpct" then
        do:
        hide frame f-custlu.   
        hide frame f-zcustinfo.
        next-prompt v-Hcommpct with frame f-Top1.
        next.
      end.
      
      if frame-field = "v-shipto" then
        do:
        if (lastkey >= 48 and lastkey <= 57) or    /* shipto was changed */
           (lastkey >= 97 and lastkey <= 122) then
          assign v-chgfl = yes.
          
        on cursor-down cursor-down.
        on cursor-up   cursor-up.
        hide frame f-custlu.
        hide message no-pause.
        assign typed-in = no
               v-5680fl = no
               v-5966fl = no.
        if not can-do("9,13,501,502,507,508",string(lastkey)) then
           assign v-shiptobrowsekeyed = false.
        if keylabel(lastkey) = "PF4" then 
          do:
          next-prompt v-shipto with frame f-top1.
          next.
        end.
        if keylabel(lastkey) = "F12" then
          assign F12-keyed = yes.

        /**das**/
        if can-do("9,13,401",string(lastkey)) and 
           input v-shipto = "" and 
           avail arsc and
           arsc.shipreqfl = yes and v-shiptobrowsekeyed = false then
          do:
          run err.p(5690).
          next-prompt v-shipto with frame f-top1.
          next.
        end.
        if can-do("9,13,401",string(lastkey)) and avail arss then 
          do:
          
          if input v-shipto <> "" or v-Shiptobrowsekeyed = true then
            do:
            if input v-shipto <> "" then
              do:
              assign v-shipto = input v-shipto.
              find arss where arss.cono = g-cono and
                              arss.custno = v-custno and
                              arss.shipto = input v-shipto
                              no-lock no-error.
            end.
            if not avail arss then
              do:
              assign v-shiptonm    = if v-mode = "C" then v-shiptonm 
                                     else arsc.name
                     v-shiptoaddr1 = if v-mode = "C" then v-shiptoaddr1 
                                     else arsc.addr[1]
                     v-shiptoaddr2 = if v-mode = "C" then v-shiptoaddr2
                                     else arsc.addr[2]
                     v-shiptocity  = if v-mode = "C" then v-shiptocity
                                     else arsc.city
                     v-shiptost    = if v-mode = "C" then v-shiptost
                                     else arsc.state
                     v-shiptogeocd = if v-mode = "C" then v-shiptogeocd
                                     else arsc.geocd
                     g-shipto      = ""
                     v-slsrepout   = if v-mode = "C" then v-slsrepout
                                     else arsc.slsrepout
                     v-slsrepin    = if v-mode = "C" then v-slsrepin
                                     else arsc.slsrepin
                     o-slsrepout   = if v-mode = "C" then v-slsrepout
                                     else arsc.slsrepout.
              if v-mode = "C" then

                assign v-custpo = if v-custpo = "" then arsc.custpo
                                  else v-custpo.
              else
                assign v-custpo = arsc.custpo
                       v-slsrepout = arsc.slsrepout
                       v-slsrepin  = arsc.slsrepin.
            end. /* arss not available */
            else
              do:
              if v-mode <> "C" or v-chgfl = yes then
                assign v-shiptonm    = arss.name
                       v-shiptoaddr1 = arss.addr[1]
                       v-shiptoaddr2 = arss.addr[2]
                       v-shiptocity  = arss.city
                       v-shiptost    = arss.state
                       v-shiptozip   = arss.zip
                       v-shiptogeocd = arss.geocd
                       g-shipto      = arss.shipto
                       v-slsrepout   = arss.slsrepout
                       o-slsrepout   = arss.slsrepout
                       v-slsrepin    = arss.slsrepin
                       v-taxablefl   = if arss.taxablety = "n" 
                                       then no else yes.
              if v-hmode = "a" then
                assign v-custpo = arss.custpo.
              else
                assign v-custpo = if v-chgfl = yes then 
                                    if arss.custpo <> " " then
                                      arss.custpo
                                    else v-custpo
                                  else v-custpo.
              run Update_ShipTo_Info.
              assign v-chgfl = no.
            end. /* arss is available */
          end. /* v-shipto <> " " */
          else 
            do:
            for each oeehb where 
                     oeehb.cono = g-cono and  
                     oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
                     oeehb.seqno      = 2 and
                     oeehb.sourcepros = "ComQu":
              if v-mode <> "C" or v-chgfl = yes then
                do:
                assign oeehb.shipto        = ""
                       oeehb.orderdisp     = arsc.orderdisp
                       oeehb.shiptonm      = if v-shiptonm <> "" then
                                               v-shiptonm else arsc.name     
                       oeehb.shiptoaddr[1] = if v-shiptoaddr1 <> "" then
                                               v-shiptoaddr1 else arsc.addr[1]
                       oeehb.shiptoaddr[2] = if v-shiptoaddr2 <> "" then
                                               v-shiptoaddr2 else arsc.addr[2]
                       oeehb.shiptocity    = if v-shiptocity <> "" then
                                               v-shiptocity else arsc.city
                       oeehb.shiptost      = if v-shiptost <> "" then
                                               v-shiptost else arsc.state
                       oeehb.shiptozip     = if v-shiptozip <> "" then
                                               v-shiptozip else arsc.zip
                       oeehb.geocd         = if v-shiptogeocd <> 0 then
                                               v-shiptogeocd else arsc.geocd    
                       oeehb.codfl         = if arsc.termstype = "COD" then yes
                                             else no
                       oeehb.slsrepout     = arsc.slsrepout
                       oeehb.slsrepin      = arsc.slsrepin 
                       oeehb.taxablefl     = if arsc.taxablety = "n" then no
                                               else yes.
                if v-shiptoaddr1 = " " and
                   v-shiptocity  = " " and
                   v-shiptost    = " " then
                  assign v-shiptonm          = arsc.name
                         v-shiptoaddr1       = arsc.addr[1]
                         v-shiptoaddr2       = arsc.addr[2]
                         v-shiptocity        = arsc.city
                         v-shiptost          = arsc.state
                         v-shiptozip         = arsc.zip
                         v-shiptogeocd       = arsc.geocd.
                assign g-shipto            = ""
                       v-slsrepout         = arsc.slsrepout
                       o-slsrepout         = arsc.slsrepout
                       v-slsrepin          = arsc.slsrepin
                       v-taxablefl         = if arsc.taxablety = "n" then no
                                             else yes.
              end.       
              if v-mode <> "C" or v-chgfl = yes then
                do:
                for each oeelb where oeelb.cono = g-cono and
                                     oeelb.batchnm = oeehb.batchnm and
                                     oeelb.seqno   = oeehb.seqno:
                  assign oeelb.slsrepout  = arsc.slsrepout
                         oeelb.slsrepin   = arsc.slsrepin
                         oeelb.taxablefl  = if arsc.taxablety = "n" then
                                              no
                                            else yes
                         oeelb.nontaxtype = arsc.nontaxtype.
                end. /* each oeelb */
              end.
            end. /* each oeehb */
            /* If shipto is not used, force the program to NOT find ARSS */
            find arss where arss.cono = g-cono and
                            arss.custno = v-custno and
                            arss.shipto = "zzzzzzzzzz"
                            no-lock no-error.
          end. /* not avail arss */
        end. /* can-do("9,13,401",string(lastkey)) and avail arss */
        
        if (not can-do("9,21,13,306,307,310,501,502,507,508",string(lastkey))
            and
            keyfunction(lastkey) ne "go" and
            keyfunction(lastkey) ne "end-error") then
          do:
          apply lastkey.
          if frame-field <> "v-shipto" then
            next.
          if v-shipto <> input v-shipto and v-mode = "c" then
            assign v-chgfl = yes.
          
          assign v-shipto = input v-shipto
                 g-shipto = input v-shipto.
          if not avail arss then
            {w-arss.i v-custno v-shipto no-lock}
          
          if can-do
             ("13,401,306,307,308,309,310,501,502,503,504",string(lastkey)) 
             and not avail arss and v-shipto <> "" then 
            do:
            run Check_ARSS (input v-custno,
                            input v-shipto,
                            input-output v-5966fl).
            if v-5966fl = yes then
              run err.p(5966).
            else
              run err.p(4304).
            next-prompt v-shipto with frame f-top1.
            next.
          end.
          
          run DoShiptoLu (input v-shipto,
                          input "normal",
                          input "f-top1").
          next.
        end. /* if not a "movement" key */
        else if can-do("306,307,308,309,310,501,502,507,508",string(lastkey))
             and v-Shiptobrowseopen = true then
          do:
          if not avail arss and v-shipto <> "" then 
            do:
            run Check_ARSS (input v-custno,
                            input v-shipto,
                            input-output v-5966fl).
            if v-5966fl = yes then
              run err.p(5966).
            else
              run err.p(4304).
            next-prompt v-shipto with frame f-top1.
            next.
          end.
          run MoveShiptoLu (input lastkey,
                            input "f-top1").
          assign v-Shiptobrowsekeyed = true.
          next.
        end. /* one of the "movement" keys was pressed */
        else 
        if can-do("13,306,307,308,309,310,401,501,502,503,504",string(lastkey))
            and not avail arss and v-shipto <> "" then
          do:
          run Check_ARSS (input v-custno,
                          input v-shipto,
                          input-output v-5966fl).
          if v-5966fl = yes then
            run err.p(5966).
          else
            run err.p(4304).
          next-prompt v-shipto with frame f-top1.
          next.
        end.
        else
        if (lastkey = 13 or keyfunction(lastkey) = "go" or lastkey = 9) and
           (v-Shiptobrowsekeyed or F12-keyed) and avail arss then
          do:
          assign F12-keyed   = no
                 v-shipto    = arss.shipto
                 g-shipto    = arss.shipto
                 v-custpo    = arss.custpo
                 v-slsrepout = arss.slsrepout
                 v-slsrepin  = arss.slsrepin
                 o-slsrepout = arss.slsrepout
                 v-Shiptobrowsekeyed = false
                 v-chgfl     = yes.
          run Update_ShipTo_Info.
          display v-shipto 
                  v-custpo with frame f-top1.
          /*assign v-Shiptobrowsekeyed = false.*/  /*moved above*/
          hide frame f-Shiptolu.
          hide frame f-zcustinfo.
        end.
        else
          do:
          if lastkey = 13 or keyfunction(lastkey) = "go" or lastkey = 9 then
            do:
/* TAH */
            if (v-shipto <> "" and not avail arss) or
               (v-shipto <> "" and avail arss and arss.statustype = no)
               then 
              do:
              run Check_ARSS (input v-custno,
                            input v-shipto,
                            input-output v-5966fl).
              if v-5966fl = yes then
                run err.p(5966).
              else
                run err.p(4304).
              next-prompt v-shipto with frame f-top1.
              next.
            end.
/* TAH */
            assign v-Shiptobrowsekeyed = false.
            display v-custpo with frame f-top1.
          end.
          else
            display v-custpo with frame f-top1.
        end. /* movement key was pressed */
      end. /* if frame-field = v-shipto */
      
      if frame-field = "v-phone" and input v-phone <> " " and
         can-do("9,13,401",string(lastkey)) then
        do:
        assign v-contact = input v-contact.
        if input v-contact = " " then
          do:
          message "A Contact Name is required if Contact Phone is Entered".
          next-prompt v-contact with frame f-top1.
          next.
        end.
        assign v-phone = input v-phone.
      end.
      
      if frame-field = "v-custpo" and 
         input v-custpo <> ""     and 
         can-do("9,13,401,501,502,503,504,507,508",string(lastkey)) /*and
         v-quoteno = 0*/ then
        do:
        find last oeeh where 
                  oeeh.cono = g-cono and
                  oeeh.custpo = input v-custpo and
                  oeeh.custpo <> " " and
                  oeeh.custno = v-custno
                  no-lock no-error.
        if avail oeeh then 
          do:
          if not avail arsc then
            find arsc where arsc.cono = g-cono and
                            arsc.custno = v-custno
                            no-lock no-error.
          if arsc.xxl13 = yes then /* SX 55 */
            do:
            message color normal                                        
              "Warning- Duplicate Customer PO Found " string(oeeh.orderno) "-"
               string(oeeh.ordersuf,"99").
            bell.                                                       
          end.
          else 
            do:
            run err.p(5682).
            next-prompt v-custpo with frame f-top1.
            next.
          end.
        end. /* avail oeeh */
        else
          do:
          find last oeehb where 
                    oeehb.cono = g-cono and
                    oeehb.custpo = input v-custpo and
                    oeehb.custpo <> " " and
                    oeehb.custno = v-custno
                    no-lock no-error.
          if avail oeehb then 
            do:
            message color normal                                        
              "Warning- Duplicate Customer PO Found on Quote " oeehb.batchnm.
            bell.                                                       
          end. /* avail oeehb */
        end. /* if oeeh not avail, check oeehb */
      end. /* frame-field = v-custpo */
      
      if frame-field = "v-canceldt"    and 
         can-do("9,13,401",string(lastkey)) and
         v-canceldt <> input v-canceldt then
        do:
        assign v-canceldt = input v-canceldt.
        display v-lostcan colon 30 
        label "Send to Lost Business" with frame f-lost.
        update
          v-lostcan 
          v-lostbusty colon 30 label "Lost Business Reason" validate
          (if not input v-lostcan then true 
           else {v-sasta.i E v-lostbusty "/*"},
          "Lost Business Reason Not Setup in System Table - SASTT (4009)")
          {f-help.i}
        with frame f-lost side-labels row 5 centered overlay.
        hide frame f-lost.
        if (v-lostcan = yes or v-lostcan = no) and v-lostbusty <> " " then 
          do:  /* lost was entered */
          if input v-canceldt <= TODAY then
            assign v-specnstype = "l".
          /*display v-specnstype with frame f-mid1.*/
  
          for each oeelb where 
                   oeelb.cono = g-cono and
                   oeelb.batchnm = string(w-batchnm,">>>>>>>9") and
                   oeelb.seqno   = oeehb.seqno:    
          
            if oeelb.specnstype <> "l" then
              do:
              if input v-canceldt <= TODAY then
                assign oeelb.specnstype = v-specnstype.
              assign oeelb.reasunavty = v-lostbusty
                     oeelb.transdt    = TODAY
                     oeelb.operinit   = g-operinit
                     oeelb.transtm    = STRING(TIME,"HH:MM:SS").
            end.
          end. /* each oeelb */
          for each oeehb where 
                   oeehb.cono = g-cono and  
                   oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
                   oeehb.seqno      = 2 and
                   oeehb.sourcepros = "ComQu":
            assign oeehb.stagecd    = if input v-canceldt <= TODAY then
                                        9
                                      else
                                        oeehb.stagecd
                   oeehb.canceldt   = v-canceldt
                   oeehb.transdt    = TODAY
                   oeehb.operinit   = g-operinit
                   oeehb.transtm    = STRING(TIME,"HH:MM:SS").
            overlay (oeehb.user3,53,2) = v-lostbusty.
            if input v-canceldt <= TODAY then
              overlay (oeehb.user3,55,2) = "  ".
          end. /* for each oeehb */
          hide frame f-lost no-pause.
          
          hide frame f-lost.
          
          display v-quoteno  v-notefl    v-custno    v-arnotefl  v-custname 
                  v-takenby  v-custlu    v-shipto    v-contact   v-phone    
                  v-custpo     v-terms     v-whse
                  v-canceldt v-quotetot  v-commtot  v-Hcommpct
          with frame f-top1.

          if not avail oeehb then
            find oeehb where oeehb.cono = g-cono and  
                             oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
                             oeehb.seqno = 1 and
                             oeehb.sourcepros = "ComQu"
                             no-lock no-error.
          leave Top1Loop.
        end. /* v-lostcan = yes */
      end. /* frame-field = v-canceldt */ 

      /**
      if frame-field = "v-Hcommpct" and input v-Hcommpct = 0 and
        can-do("9,13,401",string(lastkey)) then
        do:
        message "Quote Commision Percentage must have a value".
        next-prompt v-Hcommpct with frame f-Top1.
        next.
      end.
      **/
      
      if can-do("f6,f7,f10",keylabel(lastkey)) then
        do:
        run Assign-Update-Quote.
        display v-quoteno with frame f-top1.
        close query q-Shiptolu.
        assign v-Shiptobrowseopen = false
               v-Shiptobrowsekeyed = false
               v-manualframejump = true.
        hide frame f-Shiptolu.
        hide frame f-zcustinfo.
        apply lastkey.
        leave Top1Loop.
      end.
      apply lastkey.
    end. /* editing */
    
    if can-do("9,13,401",string(lastkey)) or         
      (frame-field = "v-Hcommpct" and 
        (keyfunction(lastkey) = "return" or
         keyfunction(lastkey) = "go")) then 
      do:
      /**das**/
      if v-shipto = " " and avail arsc and arsc.shipreqfl = yes then
        do:
        run err.p(5690).
        next-prompt v-shipto with frame f-top1.
        next.
      end.
      if v-quoteno = 0 then
        run Assign-Update-Quote.
      display v-quoteno with frame f-top1.
      
      if not can-find(first oeelb where oeelb.cono = g-cono and
                                        oeelb.batchnm = oeehb.batchnm and
                                        oeelb.seqno   = oeehb.seqno
                                        no-lock) then
        do:
        find last oeeh where 
                  oeeh.cono = g-cono and
                  oeeh.custpo = input v-custpo and
                  oeeh.custpo <> " " and
                  oeeh.custno = v-custno
                  no-lock no-error.
        if avail oeeh then 
          do:
          if not avail arsc then
            find arsc where arsc.cono = g-cono and
                            arsc.custno = v-custno
                            no-lock no-error.
          if arsc.xxl13 = yes then /* SX 55 */
            do:
            assign v-messagelit = 
              "Warning- Duplicate Customer PO Found " + string(oeeh.orderno) +
               "-" + string(oeeh.ordersuf,"99").
            put screen row 22 col 3 v-messagelit.
          end.
          else 
            do:
            assign v-messagelit =                              
              "Customer PO# Already Exists (5682)".  
            put screen row 22 col 3 color message v-messagelit.
            next-prompt v-custpo with frame f-top1.
            next.
          end.
        end. /* avail oeeh */
      end. /* no lines entered */
      run oeexcsplit.p.
      if lastkey = 404 and tot-Ccomm% = 0 then
        do:
        readkey pause 0.
        assign v-framestate = "t1"
               v-quoteno = 0
               w-batchnm = 0
               typed-in  = yes
               v-custlu  = v-hcustlu.
        next-prompt v-custlu with frame f-top1.
        next.
      end.
      if tot-Ccomm% > 0 then
        do:
        assign ss-commtot = (v-quotetot * (v-Hcommpct * .01)).
        assign c-commtot  = (ss-commtot * (tot-Ccomm% * .01)).
        assign v-commtot  = ss-commtot - c-commtot.
        display v-commtot with frame f-top1.
      end.
      readkey pause 0.
    end. /* enter on last field or go */
    
    if keyfunction(lastkey) = "go" then
      do:
      close query q-custlu.
      assign v-custbrowseopen = false.
      hide frame f-custlu.
      hide frame f-zcustinfo.
    end.
    if frame-field = "v-Hcommpct" and (keyfunction(lastkey) = "return" or
                                       keyfunction(lastkey) = "go") then 
      do:
      if v-quoteno = 0 then
        run Assign-Update-Quote.
      display v-quoteno with frame f-top1.
      leave Top1Loop.
    end.
    /*3337*/
    if v-quoteno = 0 then 
      do:
      run Assign-Update-Quote.
      display v-quoteno with frame f-top1.
    end.
    
    {k-sdinavigationQU.i &jmpcode = "return." }
    if {k-accept.i} then 
      leave Top1Loop.
  end. /* Top1Loop do while */
  
  {k-sdinavigationQU.i &jmpcode = "return." }
  
  if {k-jump.i} or {k-cancel.i} and v-quoteno > 0 then
    do:
    run FindNextFrame (input lastkey,
                       input-output v-CurrentKeyPlace,
                       input-output v-framestate).
    leave.
  end.
  else
    if v-quoteno > 0 then
    run FindNextFrame (input lastkey,
                       input-output v-CurrentKeyPlace,
                       input-output v-framestate).
  assign v-top1 = no.
end. /* Procedure Top1 */

/* -------------------------------------------------------------------------  */
Procedure Middle1:
/* -------------------------------------------------------------------------  */
  /****************************************/
  hide frame f-bot2i no-pause.
  hide frame f-bot2e no-pause.
  hide frame f-shiptolu no-pause.
  hide frame f-custlu.
  hide frame f-zcustinfo.
  display with frame f-statusline2.
  assign v-middle1     = yes
         manual-change = no
         manualdtfl    = no
         hold-linedt   = ?
         x-prodtype    = "prod"
         v-5680fl      = no
         typeover      = no
         v-zsdiobfl    = no.
  if v-linebrowseopen = false then
    do:
    run Line_Browse.
    assign v-linebrowseopen = true.
  end.                              
  display b-lines with frame f-lines.
  
  if v-messagelit <> " " then
    put screen row 22 col 3 v-messagelit.
    
/* %toms% */
  if not v-conversion then
    display with frame f-statusline2.
  else
    display with frame f-statuslinex.
    
/* %toms% */
  MidLoop:
  do while true on endkey undo MidLoop: /*, leave midloop:*/
     
/* %toms% */  
    if v-conversion and not v-loadbrowseline then 
      do:
      assign hold-lineno = 0.
      find first t-lines where no-lock no-error.
      if avail t-lines then 
        do:
        assign v-lineno = t-lines.lineno   
               v-lmode  = "c".
        run Conversion-Load.
      end.         
      else
        leave.
    end.
    else 
    if v-conversion and v-loadbrowseline then 
      do:
      find first t-lines where t-lines.lineno = v-lineno no-error.
      if avail t-lines then 
        do:
        assign v-lineno = t-lines.lineno   
               v-lmode  = "c".

/* added to keep track of the conversion browse  %TAH */
        v-rowid = rowid(t-lines). 
        b-lines:refreshable = false.
        reposition q-lines to rowid(v-rowid).
        get next q-lines.
        b-lines:refreshable = true.
        run Conversion-Load.
      end. /* avail t-lines */
      else
        leave.
    end. /* if conversion */
    
    else
/* %toms% */  
      do:
      if not v-loadbrowseline or 
        (v-loadbrowseline = yes and v-lmode = "c") then
        do:
        if not v-linebrowsekeyed then
          do:
          find last t-lines where no-lock no-error.
          if avail t-lines then
            do:
            if msgerror = no then
              run Initialize-Line.
            assign v-lineno     = t-lines.lineno + 1  
                   v-lmode      = "a"
                   v-prodlutype = g-prodlutype
                   v-Lcommpct   = v-Hcommpct
                   manualdtfl   = no.
          end. /* avail t-lines */
          else
            assign v-lineno     = 1
                   v-lmode      = "a"
                   v-prodlutype = g-prodlutype
                   v-Lcommpct   = v-Hcommpct
                   manualdtfl   = no.
        end. /* not v-linebrowsekeyed */
        else
          do:
          if avail t-lines then
            assign v-seqno      = t-lines.seqno
                   v-lineno     = t-lines.lineno      
                   v-prod       = t-lines.prod         
                   v-icnotefl   = t-lines.icnotefl
                   v-xprod      = t-lines.xprod
                   v-vendno     = t-lines.vendno
                   v-slsrepout  = t-lines.slsrepout
                   v-slsrepin   = t-lines.slsrepin
                   v-prodcat    = t-lines.prodcat
                   v-prodline   = t-lines.prodline
                   v-listprc    = t-lines.listprc
                   v-listfl     = t-lines.listfl
                   v-ptype      = t-lines.pricetype
                   v-sellprc    = t-lines.sellprc
                   v-Lcommpct   = t-lines.Lcommpct
                   v-prodcost   = t-lines.prodcost
                   v-glcost     = t-lines.glcost
                   v-surcharge  = t-lines.surcharge
                   v-totcost    = t-lines.totcost
                   v-sparebamt  = t-lines.sparebamt
                   v-rebatefl   = t-lines.rebatefl
                   v-leadtm     = t-lines.leadtm
                   v-gp         = t-lines.gp           
                   v-descrip    = t-lines.descrip
                   v-descrip2   = t-lines.descrip2
                   v-qty        = t-lines.qty
                   v-qtyrem     = t-lines.qtyrem
                   v-shipdt     = t-lines.shipdt     
                   v-specnstype = t-lines.specnstype
                   v-matrixed   = if t-lines.qtybrkty <> " " then
                                     t-lines.qtybrkty
                                  else
                                     " "
                   v-pdrecord   = t-lines.pdscrecno
                   v-pdtype     = t-lines.pdtype
                   v-pdamt      = t-lines.pdamt
                   v-pd$md      = t-lines.pd$md
                   v-pdbased    = t-lines.pdbased
                   v-totnet     = t-lines.totnet
                   v-lcomment   = t-lines.lcomment
                   v-totcost    = t-lines.totcost
                   v-rebatefl   = t-lines.rebatefl
                   v-sparebamt  = t-lines.sparebamt
                   v-totmarg    = t-lines.totmarg
                   v-margpct    = t-lines.margpct
                   v-linebrowsekeyed = no.
        end. /* else v-linebrowsekeyed */
      end. /*not v-loadbrowseline or (v-loadbrowseline = yes and v-lmode = c */      else
        do:
        find last t-lines where no-lock no-error.
        if avail t-lines then
          do:
          if msgerror = no then
            run Initialize-Line.
          assign v-lineno     = t-lines.lineno + 1
                 v-lmode      = "a"
                 v-prodlutype = g-prodlutype
                 v-Lcommpct   = v-Hcommpct
                 manualdtfl   = no.
        end. /* avail t-lines */
      end.
    end. /* not conversion */

    find arsc where arsc.cono = g-cono and
                    arsc.custno = v-custno no-lock no-error. 

    if v-shipto <> "" then
      find arss where arss.cono = g-cono and
                      arss.custno = v-custno and
                      arss.shipto  = v-shipto no-lock no-error. 

    assign v-loadbrowseline = false
           v-linelit = v-linenormlit
           v-prod    = if catalogfl = yes and g-prod <> " " then g-prod 
                       else v-prod.
    assign disp-sellprc = v-sellprc
            v-sellprc = round(v-sellprc,2).
    if v-conversion then
      do:
      /*
      if g-operinit begins "das" then
        do:
        message "hold-lineno:" hold-lineno
                "v-lineno:"    v-lineno
                "v-qty:" v-qty
                "t-lines.qtyrem:" t-lines.qtyrem. pause.
      end.
      */
      assign v-qty       = if hold-lineno = v-lineno then v-qty 
                            else t-lines.qtyrem
             t-lines.qty = if hold-lineno = v-lineno then t-lines.qtyrem
                            else t-lines.qty.
    end.

    display         
      v-linelit
      v-lineno     
      v-specnstype
      v-prod  
      v-icnotefl
      v-qty     
      v-sellprc
      v-Lcommpct
      v-shipdt
      v-totnet
      v-descrip
      v-descrip2
      v-xprod
      v-lcomment
      with frame f-mid1.
    assign v-sellprc = disp-sellprc.
    assign o-lineno  = v-lineno      
           o-specnstype = v-specnstype
           o-prod    = v-prod         
           o-xprod   = v-xprod          
           o-listprc = v-listprc      
           o-sellprc = v-sellprc     
           o-gp      = v-gp           
           o-qty     = v-qty         
           o-shipdt  = v-shipdt
           v-prodbrowsekeyed = false.
           
/* %toms% */
    if not v-conversion then
      do:
      next-prompt v-prod with frame f-mid1.
    end.
    else
      next-prompt v-lineno with frame f-mid1.
/* %toms% */
    update   
      v-lineno
      v-prod           when not v-conversion   /* tomh */
      v-qty           
      v-sellprc        when not v-conversion   /* tomh */
      v-Lcommpct       when not v-conversion
      v-shipdt       
      go-on ({k-sdigoon.i})  
    with frame f-mid1
    editing:
      v-applygo = false.
      readkey.
    
    if v-conversion and keylabel(lastkey) = "F20" then next.
    assign x-prodtype     = "prod".
           
/* %toms2% */
/* space marks / unmarks lines */      
      if frame-field = "v-lineno" and lastkey = 32 and v-conversion then 
        do:
        run conversion-marking.
        next.
      end.      

      if can-do("f6,f7,f8,f10,CTRL-D",keylabel(lastkey))
         and  v-conversion then 
        do:
        if can-do("f8",keylabel(lastkey)) then 
          do:
          run oeexcleus.p.
          display v-Lcommpct with frame f-mid1.
          /*run conversion-detail.*/
          if v-lineno  <> 1 then
            v-linebrowsekeyed = true.
            
          apply "Entry" to v-lineno in frame f-mid1.
          next-prompt v-lineno with frame f-mid1.
        end.
        else
          next.
      end. /* can-do("f6,f7,f8,f10,CTRL-D",keylabel(lastkey)) and conversion*/
      
      if v-conversion and {k-cancel.i} then
        leave MidLoop.
      
      if v-conversion and can-do("f9",keylabel(lastkey)) then
        do:
        assign v-qty    = input v-qty 
               v-shipdt = input v-shipdt.
        display v-lineno with frame f-mid1.
        run Conversion_Line_Edit.
        if errorfl = yes then 
          do:
          next-prompt v-lineno with frame f-mid1.
          message 
"Line# " v-lineno " is converted. Change qty or select lines to convert in F7".
          assign errorfl = no.
          next.
        end.
        run Conversion_Line_Split.
        assign splitfl = yes.
        apply lastkey.
        leave MidLoop.
      end.
      if keylabel(lastkey) = "F11" or keylabel(lastkey) = "PF2" then
        do:
        if keylabel(lastkey) = "F11" then
          run Tag_SASOO (input w-batchnm,
                         input "Middle1").
        if keylabel(lastkey) = "PF2" then apply lastkey.
        if keylabel(lastkey) = "PF2" then
          do:
          put screen row 21 col 1 
          color message
         "F6-Customer   F7-LINES     F8-Extend    F9-Line Comments    F10-Notes           ".
          view frame f-statusline2.
        end.
        else
          leave MidLoop.
      end.


      if can-do("f6,f7,f10",keylabel(lastkey)) 
         and not v-conversion /* %toms% */ then
        do:
        close query q-prodlu.
        assign v-prodbrowseopen = false
               v-prodbrowsekeyed = false
               v-manualframejump = true
               v-linebrowseopen = false.
        hide frame f-prodlu.
        apply lastkey.
        leave MidLoop.
      end.
 
      if (v-lmode = "a" and avail icsp) or
         (input v-qty <> v-qty and avail icsp) then 
        do:
        assign v-hqty = v-qty  /* das - moved assign v-hqty here from below to
                                  force oeexq to determine price */
               v-qty = input v-qty
               v-sellmultfl = if icsp.sellmult > 0 then true else false.
               v-qty = if icsp.sellmult > 0 then
                         icsp.sellmult * (truncate(v-qty / icsp.sellmult,0) +
                         if (v-qty / icsp.sellmult) -
                           truncate(v-qty / icsp.sellmult,0) > 0
                           then 1
                         else 0)
                       else v-qty.
        assign v-qtyrem = v-qty.
      end.
      else
        do:
        if (v-lmode = "a" and not avail icsp) or
          (input v-qty <> v-qty and not avail icsp) then 
          assign v-hqty = v-qty 
                 v-qty = input v-qty
                 v-sellmultfl = v-sellmultfl
                 v-qtyrem = v-qty.
      end.


      if frame-field = "v-qty" then
        do:
        if can-do("9,13,401,501,502,503,504",string(lastkey)) then 
          do:
          assign v-qty = input v-qty.
          if v-conversion then
            do:
            find oeelb where
                 oeelb.cono    = g-cono and
                 oeelb.batchnm = string(w-batchnm,">>>>>>>9") and
                 oeelb.seqno = 2 and
                 oeelb.lineno  = v-lineno
            no-lock no-error.
            if avail oeelb then
              do:
              if v-qty > (oeelb.qtyord - oeelb.xxde4) then
                do:
                message "Qty Remaining is less than Qty Available to convert".
                next-prompt v-qty with frame f-mid1.
                next.
              end.
              /**** NEW CODE ****/
              /*
              if g-operinit begins "das" then
                do:
                message "here in NEW CODE"
                        "v-lineno:" v-lineno. pause.
              end.
              */
              find t-lines where t-lines.lineno = v-lineno and
                                 t-lines.seqno  = 0  
                                 no-error.
              if avail t-lines then
                assign t-lines.qtyrem = t-lines.qtyrem - v-qty
                       t-lines.qty    = v-qty.
              /*
              if g-operinit begins "das" and avail t-lines then
                do:
                message "avail t-lines"
                        "t-lines.qtyrem:" t-lines.qtyrem
                        "v-qty:" v-qty. pause.
              end.
              */
            end.
          end.
          run Edit_v-qty.
          assign v-hqty = 0.
        end. /* movement key was pressed */
      end. /*if frame-field = "v-qty" and movement key was pressed */
      
      if frame-field = "v-qty" and keylabel(lastkey) = "F8" then 
         do:
         assign v-prodcost = 0
                w-commission = 0.
         for each bpel where bpel.cono   = g-cono and
                  bpel.bpid   = string(v-quoteno,">>>>>>>9") + "C" and
                  bpel.lineno = v-lineno and
                  bpel.revno <= 3
                  no-lock:
           assign w-commission = w-commission + bpel.awardprice.
         end.
         assign v-prodcost = v-sellprc - w-commission.
         assign v-totcost = v-prodcost * input v-qty
                v-totnet  = v-sellprc  * input v-qty.
      end.
      
      if keylabel(lastkey) = "F9" and v-prod <> " " 
/* %Toms10 */
        and not v-conversion 
/* %Toms10 */
       
      then 
        do:
        assign g-bpcurrproc = string(w-batchnm,">>>>>>>9")
               g-ourproc    = "oeebt"
               v-commentfl  = no.

        /*assign v-commentfl = no.*/ /*moved above*/
        run com.p("oe",1,0,v-lineno, output v-commentfl).
        assign g-ourproc    = "oeexc".
        if v-commentfl then 
          do:
          if v-lcomment = "p" or v-lcomment = "x" then 
            assign v-lcomment = "x".
          else
            assign v-lcomment = "c".
        end.
        else
          assign v-lcomment = " ".
        display v-lcomment with frame f-mid1.
        next.
      end. /* if lastkey = "F9" and v-prod <> " " */
        

      if keylabel(lastkey) = "PF4" then 
        do:
        assign typeover = no.
        if v-lmode = "a" then
          do:
          for each t-lines where t-lines.lineno = v-lineno:
            delete t-lines.
          end.
          for each com use-index k-com where 
                   com.cono       = g-cono     and 
                   com.comtype    = "puc"      and 
                   com.orderno    = v-quoteno  and
                   com.ordersuf   = 0          and
                   com.lineno     = v-lineno:
                   delete com.
          end.
          
          find u-icsp where u-icsp.cono = g-cono and
                            u-icsp.prod = v-prod
                            no-lock no-error.
        end. /* v-lmode = "a" */
        if otf-create = yes then
          do:
          run oeexqOTFdel.p.
          assign otf-create = no.
        end.
        /* initialize */
        assign v-prod       = ""
               v-reqprod    = ""
               v-icnotefl   = ""
               v-specnstype = " "
               v-ptype      = ""
               orig-whse    = ""
               v-matrixed   = " "
               v-listprc    = 0
               v-sellprc    = 0
               v-gp         = 0
               v-qty        = 1
               /*v-qtyrem     = 0*/
               v-shipdt     = ?
               v-descrip    = ""
               v-descrip2   = ""
               v-xprod      = ""
               v-lcomment   = " "
               v-totcost    = 0
               v-totmarg    = 0
               v-margpct    = 0
               v-pdrecord   = 0
               v-pdtype     = 0
               v-pdamt      = 0
               v-pd$md      = ""
               v-pdbased    = " "
               v-totnet     = 0
               v-costoverfl = no
               v-5603fl     = no
               v-statustype = " "
               v-surcharge  = 0 
               h-surcharge  = 0 
               v-chgsrchgfl = no
               v-slsrepovr  = no
               v-taxablefl  = no
               v-slsrepout  = ""
               v-slsrepin   = ""
               o-specnstype = " "
               o-prod       = ""
               o-xprod      = ""
               o-whse       = ""
               o-listprc    = 0
               o-sellprc    = 0
               o-gp         = 0
               o-disc       = 0
               o-disctype   = no
               o-qty        = 1
             /*o-qtyrem     = 0*/
               o-shipdt     = ?
               cancel-xref  = no
               v-xreffl     = yes
               v-ctrlaovr   = no
               ns-descrip   = ""
               ns-descrip2  = ""
               ns-vendno    =  0
               ns-prodline  = ""
               ns-prodcat   = ""
               ns-listprc   =  0
               ns-listfl    = "n"
               ns-prodcost  =  0
               ns-leadtm    = 30
               ns-ptype     = ""
               orig-cost    = 0
               over-cost    = 0
               s-mprice     = 0
               s-mcostplus  = 0
               s-mmargin    = 0
               s-mdiscamt   = 0
               s-mdiscpct   = 0
               s-discfl     = no
               manual-change = no.
               
        find last oeelb where 
                  oeelb.cono = g-cono and
                  oeelb.batchnm     = string(w-batchnm,">>>>>>>9") and
                  oeelb.seqno       = oeehb.seqno
                  no-lock no-error.
        if avail oeelb then
          assign v-lineno = oeelb.lineno + 1
                 v-lmode  = "a".
        else
          assign v-lineno = 1.
        
        assign disp-sellprc = v-sellprc
               v-sellprc = round(v-sellprc,2).

        display v-linelit
                v-lineno     
                v-specnstype
                v-prod  
                v-icnotefl
                v-qty     
                v-sellprc
                v-Lcommpct
                v-shipdt      
                v-totnet
                v-descrip
                v-descrip2
                v-xprod
                v-lcomment
        with frame f-mid1.
        assign v-sellprc = disp-sellprc.
        next-prompt v-prod with frame f-mid1.

        hide message no-pause.

        run DolineLu.
        next.
      end. /* if lastkey = "PF4" */

      
      if keylabel(lastkey) = "F8" and v-prod <> " " and
         frame-field <> "v-lineno"
/* %toms4% */
          and not v-conversion
/* %toms4% */
          then 
        do:
        if v-lmode = "a" and frame-field = "v-prod" then next.
        if v-lmode = "a" and frame-field = "v-qty" then next.
        if v-lmode = "a" and frame-field = "v-sellprc" then next.
        find com use-index k-com where 
             com.cono       = g-cono     and 
             com.comtype    = "puc"      and 
             com.orderno    = v-quoteno  and
             com.ordersuf   = 0          and
             com.lineno     = v-lineno  
             no-lock no-error.
        if avail com then assign v-rarrnotesfl = yes.
        assign f8-return = " ".
        if frame-field = "v-prod"     then assign f8-return = "v-prod".
        if frame-field = "v-qty"      then assign f8-return = "v-qty".
        if frame-field = "v-sellprc"  then assign f8-return = "v-sellprc".
        if frame-field = "v-Lcommpct" then assign F8-return = "v-Lcommpct".
        if frame-field = "v-shipdt"   then assign f8-return = "v-shipdt".

        if v-lmode = "c" then
          do:
          find oeelb where oeelb.cono    = g-cono and
                           oeelb.batchnm = string(w-batchnm,">>>>>>>9") and
                           oeelb.seqno   = oeehb.seqno   and
                           oeelb.lineno  = v-lineno
                           no-error.
          if avail oeelb then 
            assign v-slsrepout = oeelb.slsrepout
                   v-slsrepin  = oeelb.slsrepin.
        end.
        else
          if v-lmode = "c" then
            assign v-slsrepout = oeelb.slsrepout
                   v-slsrepin  = oeelb.slsrepin.
        if v-slsrepout = "" then
          do:
          if avail arsc then
            assign v-slsrepout = arsc.slsrepout.
          if avail arss then
            assign v-slsrepout = arss.slsrepout.
          if v-slsrepout = "" then
            assign v-slsrepout = o-slsrepout.
        end.
        if v-slsrepin = "" then
          do:
          if avail arsc then
            assign v-slsrepin = arsc.slsrepin.
          if avail arss then
            assign v-slsrepin = arss.slsrepin.
          if v-slsrepin = "" then
            assign v-slsrepin = o-slsrepin.
        end.
        assign v-totcost = /*if avail oeelb and v-lmode = "c" then
                             oeelb.prodcost * oeelb.qtyord
                           else*/
                             v-prodcost * v-qty.
                  
        run oeexcleus.p.
        /* readkey, pause 0 is required on return from F8-Extend */
        readkey pause 0.
        display v-Lcommpct with frame f-mid1.
/* %toms% */
        if not v-conversion then
          display with frame f-statusline2.
        else
          display with frame f-statuslinex.
/* %toms% */
        if f8-return = "v-prod" then
          next-prompt v-prod    with frame f-mid1.
        if f8-return = "v-sellprc" then
          next-prompt v-sellprc with frame f-mid1.
        if f8-return = "v-shipdt" then
          next-prompt v-shipdt  with frame f-mid1.
        assign f8-return = " ".
        next.
      end. /* if lastkey = "F8" */
      
      On CTRL-D anywhere         
        do:
        if v-prod <> " " then 
          do:
          display v-delmode colon 30 
          label "Delete or Lost Business" with frame del.
          update
          v-delmode 
          v-lostbusty colon 30 label "Lost Business Reason" validate
          (if not input v-delmode then true else {v-sasta.i E v-lostbusty "/*"},
          "Lost Business Reason Not Setup in System Table - SASTT (4009)")
          {f-help.i}
          with frame del side-labels row 5 centered overlay.
          hide frame del.
          if v-delmode = no then 
            do:   /* delete was entered */
            run oeexcdelln.p.
            run Quote-Total.
            run LoadQuote (input-output errorfl).
            run Initialize-Line.
            assign g-prod = "".
            run middle1.
          end. /* if v-delmode = no */
          if v-delmode = yes then 
            do:  /* lost was entered */
            assign v-specnstype = "l".
            display v-specnstype with frame f-mid1.
            find oeelb where 
                 oeelb.cono = g-cono and
                 oeelb.batchnm = string(w-batchnm,">>>>>>>9") and
                 oeelb.seqno   = oeehb.seqno and
                 oeelb.lineno  = v-lineno
                 no-error.
            if avail oeelb then
              assign oeelb.specnstype = v-specnstype
                     oeelb.reasunavty = v-lostbusty.
            run Quote-Total.
            run LoadQuote (input-output errorfl).
            if errorfl = yes then 
              do:
              next-prompt v-quoteno with frame f-top1.
              assign errorfl = no.
              next.
            end.
            run Initialize-Line.
            run middle1.
          end. /* if v-delmode = yes */ 
        end. /* if v-prod <> " " */ 
        hide frame del.
      end. /* on CTRL-D */
      
      /* Product Add On the Fly */
      On CTRL-P of v-prod 
        do:
        if v-prod = "" then next.
        if avail icsp and icsp.statustype = "I" then next.
        assign v-prod = input v-prod.
        run OTF_Routine (input v-prod,
                         input v-whse,
                         input v-lineno,
                         input x-prodtype).
/* %toms% */
        if not v-conversion then do:
/* %toms% */
        display with frame f-statusline.
        display v-notefl
                v-custno
                v-arnotefl
                v-custname
                v-whse
                v-takenby
                v-shipto
                v-contact
                v-phone
                v-custpo
                v-terms
                v-canceldt
                v-quotetot
                v-commtot  v-Hcommpct
                v-custlu 
        with frame f-top1.
/* %toms% */

        end.
/* %toms% */
      end. /* on CTRL-P */
                                                        
      if frame-field = "v-lineno" then
        do:
        if v-conversion and lastkey = 503 then
          do:
          next-prompt v-lineno with frame f-mid1.
          next.
        end.
        if v-conversion then
          do:
          assign v-lineno = input v-lineno.
          find t-lines where t-lines.lineno = v-lineno and
                             t-lines.seqno  = 0  
                             no-lock no-error.
          if avail t-lines then
            assign v-qty = t-lines.qtyrem       /**** NEW CODE ****/
                   v-shipdt = t-lines.shipdt.
          display v-qty v-shipdt with frame f-mid1.
          assign hold-lineno = v-lineno.
        end.  

        if not can-do("9,13,401,501,502,507,508",string(lastkey)) then
           assign v-linebrowsekeyed = false.
        if keyfunction(lastkey) = "go" and input v-prod = " " and
           v-linebrowsekeyed = false and v-lineno = input v-lineno then 
          do:
          next-prompt v-prod with frame f-mid1.
          next.
        end.
        if (not can-do("9,13,501,502,507,508",string(lastkey)) and
            keyfunction(lastkey) ne "go" and
            keyfunction(lastkey) ne "end-error") then
          do:
          apply lastkey.
          if frame-field <> "v-lineno" then
            next.
          if v-linebrowseopen = false then
            run DolineLu.
          else
            enable b-lines with frame f-lines.
            
          /*apply "Entry" to v-lineno in frame f-mid1.*/
            
          next.
        end. /* if not a "movement" key */
        else if (lastkey = 501 or
                 lastkey = 502 or
                 lastkey = 507 or
                 lastkey = 508) and v-linebrowseopen = true then
          do:
          run MovelineLu (input lastkey).
          assign v-linebrowsekeyed = true.
          next.
        end.
        else 
          do:
          if (lastkey = 13  or keyfunction(lastkey) = "go" or
              lastkey =  9) then     /* das - 03/19/09 */
            do:
            /*assign v-linebrowsekeyed = false.*/   /*das-moved lower in code*/

            /* key combination up arrow and enter from v-prod to v-lineno is
               causing an insert to occur. This reverses the insert - das */
            if x-uparrowfl = yes then
              do:
              assign w-key = 403.
              apply w-key.
              assign x-uparrowfl = no.
            end.
            /* das - 06/19/09 - added the "or" so t-lines was found */
            if (input v-lineno <> v-lineno) or
               (input v-lineno = v-lineno and v-lmode = "c" and
                                              v-linebrowsekeyed = false) then
              find first t-lines where t-lines.lineno = input v-lineno
                                       no-lock no-error.
            if avail t-lines then 
              do:
              assign v-loadbrowseline = true
                     v-lmode      = "c"
                     v-seqno      = t-lines.seqno
                     v-lineno     = t-lines.lineno      
                     v-prod       = t-lines.prod         
                     v-icnotefl   = t-lines.icnotefl
                     v-xprod      = t-lines.xprod
                     v-vendno     = t-lines.vendno
                     v-slsrepout  = t-lines.slsrepout
                     v-slsrepin   = t-lines.slsrepin
                     v-prodcat    = t-lines.prodcat
                     v-prodline   = t-lines.prodline
                     v-listprc    = t-lines.listprc      
                     v-listfl     = t-lines.listfl
                     v-ptype      = t-lines.pricetype
                     v-sellprc    = t-lines.sellprc
                     v-Lcommpct   = t-lines.Lcommpct
                     v-prodcost   = t-lines.prodcost
                     v-glcost     = t-lines.glcost
                     v-surcharge  = t-lines.surcharge
                     v-totcost    = t-lines.totcost
                     v-sparebamt  = t-lines.sparebamt
                     v-rebatefl   = t-lines.rebatefl
                     v-leadtm     = t-lines.leadtm
                     v-gp         = t-lines.gp           
                     v-descrip    = t-lines.descrip
                     v-descrip2   = t-lines.descrip2
                     v-qty        = if hold-lineno <> v-lineno then
                                      t-lines.qty
                                    else v-qty
                     v-qtyrem     = t-lines.qtyrem
                     v-shipdt     = t-lines.shipdt     
                     v-specnstype = t-lines.specnstype
                     v-matrixed   = if t-lines.qtybrkty <> " " then
                                       t-lines.qtybrkty
                                    else
                                       " "
                     v-pdrecord    = t-lines.pdscrecno
                     v-pdtype     = t-lines.pdtype
                     v-pdamt      = t-lines.pdamt
                     v-pd$md      = t-lines.pd$md
                     v-pdbased    = t-lines.pdbased
                     v-totnet     = t-lines.totnet
                     v-lcomment   = t-lines.lcomment
                     v-totcost    = t-lines.totcost
                     v-rebatefl   = t-lines.rebatefl
                     v-totmarg    = t-lines.totmarg
                     v-margpct    = t-lines.margpct
                     o-lineno     = t-lines.lineno      
                     o-seqno      = t-lines.seqno
                     o-prod       = t-lines.prod         
                     o-whse       = t-lines.whse
                     o-slsrepout  = t-lines.slsrepout
                     o-slsrepin   = t-lines.slsrepin
                     o-listprc    = t-lines.listprc      
                     o-sellprc    = t-lines.sellprc     
                     o-Lcommpct   = t-lines.Lcommpct
                     o-gp         = t-lines.gp           
                     o-disc       = t-lines.discpct
                     o-disctype   = t-lines.disctype
                     o-qty        = t-lines.qty          
                     o-qtyrem     = t-lines.qtyrem
                     o-shipdt     = t-lines.shipdt
                     o-specnstype = t-lines.specnstype.     
              /* display product lookup and whse info on existing lines*/
              if v-prod <> "" and v-qmode = "c" and v-lmode = "c" 
/* %toms% */  
                and not v-conversion then do:
/* %toms% */  
                run DoProdLu (input v-prod,   
                              input "normal").
              end.
            end. /* if avail t-lines */
            else
              assign v-lineno = 1
                     v-lmode  = "a".
            
            if v-seqno = 0 then 
              do:
              assign disp-sellprc = v-sellprc
                     v-sellprc = round(v-sellprc,2).

              display v-lineno 
                      v-specnstype    
                      v-prod  
                      v-icnotefl
                      v-sellprc    
                      v-totnet
                      v-qty        
                      v-Lcommpct
                      v-shipdt     
                      v-lcomment
                      with frame f-mid1.
              assign v-sellprc = disp-sellprc.
/* %toms% */
              if not v-conversion then
                do:
                next-prompt v-prod with frame f-mid1.
              end.
              else
                do:
                if lastkey = 13 or lastkey = 9 then  /* enter or tab */
                  do:
                  assign v-qtyfirst = yes.
                  next-prompt v-qty  with frame f-mid1.  /* das-09/14/09 */ 
                end.
                else
                  do:
                  next-prompt v-lineno with frame f-mid1.
                end.
              end.
/* %toms% */
              assign v-linebrowsekeyed = false.
              next.
            end. /* if v-seqno = 0 */
            assign v-linebrowsekeyed = false.  /*das-moved from above*/
          end. /* if lastkey RETURN or F1 */
        end. /* else lastkey not UP, DOWN, RIGHT, LEFT ARROW */ 
      end. /* frame-field = v-lineno */

      if frame-field = "v-sellprc" then 
        do:
        hide message no-pause.
        if can-do("46,48,49,50,51,52,53,54,55,56,57",string(lastkey)) then
          assign manual-change = yes. /* "0" thru "9" or "." was entered */
        if can-do("9,13,501,502,503,504,401",string(lastkey)) then
          do:
          if v-sellprc <> input v-sellprc then 
            do:
          /* pricing edits */
            assign s-prod        = v-prod    
                   s-qtyord      = v-qty
                   s-chrgqty     = v-qty
                   s-price       = input v-sellprc
                   s-specnstype  = v-specnstype
                   s-discamt     = 0 /*v-disc*/
                   s-disctype    = no
                   s-prodcost    = v-prodcost 
                   s-returnfl    = no
                   s-pricetype   = v-ptype
                   s-prodcat     = v-prodcat
                   v-rebamt      = v-sparebamt
                   v-sellprc     = input v-sellprc.
            run Edit_v-sellprc.
            if v-errfl = "b" then 
              do:
              next-prompt v-sellprc with frame f-mid1.
              assign v-errfl = " ".
              next.
            end.
          end. /* if v-sellprc <> input v-sellprc */
          assign v-totcost    = v-prodcost * v-qty
                 v-totnet     = v-sellprc  * v-qty.
          assign v-quotetot = v-totnet.
          assign ss-commtot = (v-quotetot * (v-Hcommpct * .01)).
          assign c-commtot  = (ss-commtot * (tot-Ccomm% * .01)).
          assign v-commtot  = ss-commtot - c-commtot.
          for each b-oeelb where 
                   b-oeelb.cono = g-cono and
                   b-oeelb.batchnm = string(v-quoteno,">>>>>>>9") and
                   b-oeelb.lineno <> v-lineno and
                   b-oeelb.seqno = 2
                   no-lock:
            assign v-quotetot = v-quotetot + b-oeelb.netamt.
          end.
          assign ss-commtot = (v-quotetot * (v-Hcommpct * .01)).
          assign c-commtot  = (ss-commtot * (tot-Ccomm% * .01)).
          assign v-commtot  = ss-commtot - c-commtot.
          display v-totnet with frame f-mid1.
          display v-quotetot v-commtot with frame f-top1.
        end. /* if lastkey is a movement key */
      end. /* if frame-field = "v-sellprc" */
      
      if frame-field = "v-Lcommpct" and 
        can-do("9,13,501,502,503,504",string(lastkey)) then
        do:
        assign v-Lcommpct = input v-Lcommpct. 
        run Edit_v-Lcommpct.
        /*
        next-prompt v-shipdt with frame f-mid1.
        next.
        */
      end.
      
      if keylabel(lastkey) = "F20" then 
        do:
        run noted.p(yes,"p",v-prod,"").
        readkey pause 0.
      end.

      if frame-field = "v-prod" then
        do:
        on cursor-up cursor-up.
        on cursor-down cursor-down.
        
        if v-prod = "" and g-prod <> "" and lastkey = 13 then
          do:
          assign v-prod = g-prod.
          display v-prod with frame f-mid1.
          next.
        end.
        
        if catalogfl = yes then
          do:
          display v-prod with frame f-mid1.
        end.
        
        if v-lmode = "c" and v-prod <> "" and
          not
          can-do("1,4,9,13,15,16,401,501,502,503,504,507,508",string(lastkey))
          then
          assign typeover = yes.
          
        if can-do("9,13,401,23,320,502,503",string(lastkey)) then
          do:
          assign typeover = no.
          hide message no-pause.
        end.
        if typeover = yes then
          do:
          message
    "Typeover of Product is not allowed. You must cancel, then enter product".
          next-prompt v-prod with frame f-mid1.
          next.
        end.
          
        if lastkey = 501 then
          assign x-uparrowfl = yes.
        else
          assign x-uparrowfl = no.
        
        if not can-do("9,13,401,23,501,502,507,508",string(lastkey)) then
           assign v-prodbrowsekeyed = false.

        if keyfunction(lastkey) = "go" and v-prod = "" then 
          leave MidLoop.
        
        /* qu lines were disappearing */
        if keyfunction(lastkey) = "CURSOR-LEFT" or v-lmode = "c" then 
          do:
          run Line_Browse.
          /*
          open query q-lines for each t-lines. 
          */
          assign v-linebrowseopen = true.
          display b-lines with frame f-lines.
        end.
        
        if frame-field = "v-prod" and v-specnstype = "l" and 
           lastkey <> 404 then
          do:
          message "Changes not allowed to lines sent to lost business.  Press F4 to continue".
          next-prompt v-prod with frame f-mid1.
          next.
        end.
        
        if keyfunction(lastkey) = "CURSOR-UP" and 
           v-prodbrowsekeyed = false then
          do:
          next-prompt v-lineno with frame f-mid1.
          next.
        end.
        if can-do("9,21,13,401",string(lastkey)) and 
           input v-prod = "" and g-prod = "" then 
          do:
          run err.p (2100).
          next-prompt v-prod with frame f-mid1.
          next.
        end.
        
        if (not can-do("9,21,13,501,502,507,508",string(lastkey)) and
            keyfunction(lastkey) ne "go" and
            keyfunction(lastkey) ne "end-error" and
            keylabel(lastkey) ne "Ctrl-W") then
          do:
          apply lastkey.
          
          if frame-field <> "v-prod" then
            do:
            if v-linebrowseopen = false then
              run DolineLu.                 
            next.
          end.
            
          assign v-prod = input v-prod. 
          
          run DoProdLu (input v-prod,
                        input "normal").
          next.
        end. /* if not a "movement" key */
        else 
          do:
          if (lastkey = 501 or
              lastkey = 502 or
              lastkey = 507 or
              lastkey = 508) and v-prodbrowseopen = true  then
            do:
            run MoveProdLu (input lastkey).
            assign v-prodbrowsekeyed = true.
            next.
          end.
        end.
/* not an else if because of the v-applygo */
        if (lastkey = 9 or lastkey = 13 or keyfunction(lastkey) = "go" or 
          v-applygo or keylabel(lastkey) = "F12") then
          do:
          on cursor-up back-tab.     
          on cursor-down tab.
          assign v-prod       = input v-prod
                 /*v-reqprod    = input v-prod*/
                 v-prodlutype = g-prodlutype
                 orig-whse    = v-whse.
          /* we're losing v-slsrepout if user press PF4 */
          if v-slsrepout = "" then
            do:
            if avail arsc then
              assign v-slsrepout = arsc.slsrepout.
            if avail arss then
              assign v-slsrepout = arss.slsrepout.
            if v-slsrepout = "" then
              assign v-slsrepout = o-slsrepout.
            run xsdioetlt.p (input-output v-slsrepout,
                             input v-prodcat,         
                             input v-custno,          
                             input v-shipto).
          end. /* slesrepout = "" */
          
          if v-prodbrowsekeyed = true and avail icsp then
            assign v-prod = icsp.prod.
          assign g-prod = v-prod.
          /* loosing the body of the browse and CTRL-W display when prodlutype
             is not a P */
          assign hold-prodlutype = v-prodlutype.
                 v-prodlutype = "P".
          run DoProdLu (input v-prod,   
                        input "normal").
          display v-prod with frame f-mid1.
          if v-whse = "" then 
            do:
            run Find_Whse (input x-prodtype).
            display v-shipdt with frame f-mid1.
          end.
          
          assign x-whse       = v-whse
                 x-specnstype = v-specnstype.
          run XRef_Check(input v-whse,
                         input v-seqno,
                         input-output v-qty,
                         input-output v-prod).
          if lastkey = 404 then 
            do:
            readkey pause 0.
            assign cancel-xref = yes
                   w-key = 13.
            apply w-key.
          end.
          assign v-whse       = x-whse
                 v-specnstype = x-specnstype.

          find icsp where icsp.cono = g-cono and
                          icsp.prod = v-prod no-lock no-error.
          if avail icsp then 
            do: 
            assign v-prodcat = icsp.prodcat.
            if icsp.statustype = "I" then
              do:
              display v-prod with frame f-mid1.
              /*
              if g-operinit begins "das" then
                do:
                message "got here ". pause.
              end.
              */
              assign v-messagelit = 
                 "Product has Inactive Status.  See Buyer to Activate".
              put screen row 23 col 3 color message v-messagelit.
              /* DON'T STOP THE CSR FROM ENTERING THE QUOTE
              next-prompt v-prod with frame f-mid1.
              next.
              */
            end. /* icsp.statustype = "I" */
            {speccost.gas}
            assign v-icnotefl = icsp.notesfl
                   v-descrip  = icsp.descrip[1]
                   v-descrip2 = icsp.descrip[2]
                   v-prodcat  = if v-prodcat = " " then
                                  icsp.prodcat
                                else
                                  v-prodcat
                   xcount     = 0.
            if lastkey = 9 or lastkey = 13 or lastkey = 401 then
              do:
              /* das - 6/20/2007 - begin */
              if v-specnstype = "n" and v-lmode = "c" then
                do:
                run oeexcnspopup.p.    
                if v-listfl = "L" and 
                   v-ptype <> " " and
                  (v-lmode = "a" or 
                  (h-listprc <> v-listprc and v-lmode = "c")) then
                  do:
                  assign v-sellprc = 0.
                  /*das - Auth_fix_0314*/
                  assign auth-sellprc = v-sellprc
                         disp-sellprc = v-sellprc
                          v-sellprc = round(v-sellprc,2).
                  display v-sellprc
                          v-qty
                          v-Lcommpct
                  with frame f-mid1.
                  assign v-sellprc = disp-sellprc.
                end.
                if {k-cancel.i} then
                  do:
                  next-prompt v-prod with frame f-mid1.
                  next.
                end.
              end. /* v-specnstype = "n" */
              /* das - 6/20/2007 - end */
              if v-whse <> "" and v-prod <> "" then 
                do:
                {w-icsw.i v-prod v-whse no-lock}
                find first b-oeelb where 
                           b-oeelb.cono = g-cono and
                           b-oeelb.batchnm = string(w-batchnm,">>>>>>>9") and
                           b-oeelb.seqno   = 2
                           no-lock no-error.
                if avail b-oeelb and avail icsw and 
                         b-oeelb.vendno <> icsw.arpvendno then
                  do:
                  message
   "Vendor must be the same on all lines " + string(b-oeelb.vendno) +
                       ". Enter new quote or as Non-Stock".
                  next-prompt v-prod with frame f-mid1.
                  next.
                end.
                if not can-find (first icsw where icsw.cono = g-cono and
                                 icsw.whse = v-whse and
                                 icsw.prod = v-prod no-lock) then
                  do:
                  find b-icsp where b-icsp.cono = g-cono and
                                    b-icsp.prod = v-prod 
                                    no-lock no-error.
                  if not avail b-icsp then
                    do:
                    run zsdimaincpy.p (input g-cono,
                                       input " ",
                                       input v-whse,
                                       input v-prod,
                                       input v-lineno,
                                       input-output zsdi-confirm).
                    if not can-find (first icsw where 
                                           icsw.cono = g-cono and
                                           icsw.whse = v-whse and
                                           icsw.prod = v-prod no-lock) then
                      do:
                      run oeexqICSW.p.
                    /* Callmod */
                      if not 
                        can-find (first icsw where 
                                        icsw.cono = g-cono and
                                        icsw.whse = v-whse and
                                        icsw.prod = v-prod no-lock) then do:
                         run err.p (4602).
                         next.
                      end.                  
  
/* Callmod */              
                     
                    end. /* not avail b-icsp */
                  end. /* icsp */
                end. /* icsw not found */  
              end. /* v-whse <> "" and v-prod <> "" */
              
              run Get-Whse-Info (input v-prod,
                                 input v-whse,
                                 input v-qty,
                                 input-output v-shipdt,
                                 input-output v-specnstype,
                                 input-output v-ptype,
                                 input-output v-foundfl,
                                 input-output v-statustype,
                                 input-output v-prodline,
                                 input-output v-rarrnotesfl).

            end. /*lastkey = 9 or lastkey = 13 or lastkey = 401 */
            
            display v-prod  with frame f-mid1.
            if v-lmode = "a" or v-prod <> o-prod then
              do:
              if avail icsw then
                do:
                find apsv where apsv.cono = g-cono and
                                apsv.vendno = icsw.arpvendno and
                                apsv.arcustno <> 0
                                no-lock no-error.
                if not avail apsv then
                  do:
                  message "Vendor " icsw.arpvendno 
                          " Does not have a valid AR Customer set up".
                  next-prompt v-prod with frame f-mid1.
                  next.
                end.
              end.
              if v-icnotefl = "!" and v-lmode = "a" then 
                do:
                run noted.p(yes,"p",v-prod,"").
                readkey pause 0.
              end.
              assign auth-sellprc = v-sellprc
                     disp-sellprc = v-sellprc
                     v-sellprc = round(v-sellprc,2).
              display v-sellprc
                      v-Lcommpct
              with frame f-mid1.
              assign v-sellprc = disp-sellprc.
            end. /* if v-lmode = "a" or v-prod <> o-prod */

            if v-surcharge > 0 then
       message "A Surcharge of" v-surcharge "Per EACH Exists for this Product".
            if v-lmode = "a" and v-slsrepovr = no then
              do:
              if v-slsrepout = "" then
                do:
                if avail arsc then
                  assign v-slsrepout = arsc.slsrepout.
                if avail arss then
                  assign v-slsrepout = arss.slsrepout.
                if v-slsrepout = "" then
                  assign v-slsrepout = o-slsrepout.
              end.

              run xsdioetlt.p (input-output v-slsrepout,
                               input v-prodcat,
                               input v-custno,
                               input v-shipto).
            end. /* v-lmode = "a" and v-slsrepovr = no */

            find /*first*/ icsec where icsec.cono = g-cono and
                       icsec.rectype = "c" and
                       icsec.custno  = v-custno and
                       icsec.altprod = v-prod
                       no-lock no-error.
            if avail icsec then
              do:
              assign v-xprod   = icsec.prod
                     v-reqprod = icsec.prod.
              /*run bottom_prodoe (input v-prod).*/
            end.
            else
              /* das 041212 - we should display what's in reqprod */
              assign /*v-xprod = " "*/
                     v-xprod   = v-reqprod
                     v-reqprod = v-reqprod.

            display v-prod
                    v-qty
                    v-icnotefl
                    v-specnstype
                    v-descrip
                    v-xprod
                    v-descrip2
                    v-shipdt
                    v-Lcommpct
            with frame f-mid1.
            next-prompt v-qty with frame f-mid1.
            if catalogfl = yes then
              assign catalogfl = no
                     g-prod    = " ".
            next.
          end. /* avail icsp */
          else /* not avail icsp */
            do:
            assign v-qty = input v-qty.
            
            run oeexcnspopup.p.
              
            assign catalogfl = no
                   g-prod    = " ".
            if v-listfl = "L" and 
               v-ptype <> " " and
              (v-lmode = "a" or 
              (h-listprc <> v-listprc and v-lmode = "c")) then
              do:
              assign v-sellprc = 0.
              run oeexqnsprice.p.
              /*das-Auth_Fix_0314*/
              assign auth-sellprc = v-sellprc
                     disp-sellprc = v-sellprc
                     v-sellprc = round(v-sellprc,2).
              display v-sellprc
                      v-Lcommpct
                      v-qty    
              with frame f-mid1.
              assign v-sellprc = disp-sellprc.
            end.
            if {k-cancel.i} then
              do:
              next-prompt v-prod with frame f-mid1.
              next.
            end.
              
            assign v-Prod = input v-prod
                   v-specnstype = if s-yn = no and catalogfl = no then "n"
                                  else " ".

            /*das - don't recalculate v-shipdt on a change */
            if v-lmode = "a" then
              do:
              if manualdtfl = no then
                assign v-shipdt    = if v-leadtm = 0 then TODAY + 30
                                     else TODAY + v-leadtm
                       hold-linedt = v-shipdt.
            end.
            else
              assign v-shipdt    = v-shipdt
                     hold-linedt = v-shipdt.
            assign disp-sellprc = v-sellprc
                   v-sellprc = round(v-sellprc,2).

            display v-specnstype
                    v-prod  
                    v-icnotefl
                    v-Lcommpct
                    v-shipdt
                    v-descrip 
                    v-xprod
                    v-sellprc
                    v-descrip2
            with frame f-mid1.
            assign v-sellprc = disp-sellprc.
            next-prompt v-qty with frame f-mid1.
            next.
          end. /* if not avail icsp */
          
          if v-shipto = "" then 
            do:
            if v-whse = "" then
              do:
              run Find_Whse (input x-prodtype).
            end.
          end.
          else 
            do:
            if v-whse = "" then
              do:
              run Find_Whse (input x-prodtype).
            end.
          end.
          display v-shipdt with frame f-mid1.
          assign o-prod = v-prod
                 v-matrixed = " ".
          assign disp-sellprc = v-sellprc
                 v-sellprc = round(v-sellprc,2).

          display v-lineno   
                  v-specnstype  
                  v-prod  
                  v-icnotefl
                  v-qty     
                  v-sellprc
                  v-Lcommpct
                  v-shipdt
                  v-descrip
                  v-xprod
                  v-descrip2
          with frame f-mid1.
          assign v-sellprc = disp-sellprc.
          assign v-prodbrowsekeyed = false.
          hide frame f-prodlu no-pause.
          
          if newprod then
            do:
            run DoProdLu (input v-prod,   /* run DoProdLu here so that the */
                        input "normal").  /* browse isn't lost when cursor */
            assign newprod = no.          /* moves to qty field            */
            next-prompt v-qty with frame f-mid1.
            next.                                
          end.
        end. /* if lastkey = RETURN, F1 or F12 */
        else 
          assign v-prodbrowsekeyed = false.
      end. /* if frame-field = "v-prod" */

      apply lastkey.
      
      if keyfunction(lastkey) = "go" or (frame-field = "v-shipdt" and
                                         keyfunction(lastkey) = "return") then
        do:
        assign apr-error = no.
        run Continue_Middle1_Process.
        /***
        if v-conversion then
          do:
          assign edit-amt = 0.
          assign edit-amt = v-sellprc * input v-qty.
          for each b-oeelb where 
                   b-oeelb.cono = g-cono and
                   b-oeelb.batchnm = string(v-quoteno,">>>>>>>9") and
                   b-oeelb.lineno <> v-lineno and
                   b-oeelb.seqno = 2
                   no-lock:
            assign edit-amt = edit-amt + b-oeelb.netamt.
          end.
          if edit-amt > v-quotetot then
            do:
            message "Line Totals of " + string(edit-amt,">>>>>>9.99") + 
                    " cannot exceed the Quote Total of " +
                    string(v-quotetot,">>>>>>9.99").
            next-prompt v-sellprc with frame f-mid1.
            next.
          end.
          hide message no-pause.
        end. /* if v-conversion */
        ***/
      end. /* if go */
    end. /* editing loop */
    
    /* check if the PromDt was manually entered */
    if v-shipdt <> hold-linedt then
      do:
      assign manualdtfl = yes
             hold-linedt = ?.
    end.
    
    if keyfunction(lastkey) = "end-error" then
      do:
      run FindNextFrame (input lastkey,
                         input-output v-CurrentKeyPlace,
                         input-output v-framestate).

      close query q-lines.
      assign v-linebrowseopen = false.
      hide frame f-lines.
      leave. /* MidLoop.*/
    end.    
    
    if keyfunction(lastkey) = "go" or (frame-field = "v-shipdt" and
                                       keyfunction(lastkey) = "return") 
                                       
/* %Toms10 */
        or (keylabel(lastkey) = "F9" and v-conversion )

/* %Toms10 */


      then
      do:
      
      if not avail oeehb then
        find oeehb where oeehb.cono = g-cono and
                         oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
                         oeehb.seqno   = 2 and
                         oeehb.sourcepros = "ComQu"
                         no-lock no-error.
            
      if v-specnstype <> "n" and
        (oeehb.whse <> "" and
         (not can-find( icsw where icsw.cono = g-cono and
                                   icsw.prod = v-prod and
                                   icsw.whse = if v-conversion then h-whse 
                                               else oeehb.whse no-lock))) then 
        do:
        message v-prod + " ICSW does not exist at Banner Whse " + 
                CAPS(oeehb.whse) + "-Use CTRL-P to Add".
        assign msgerror = true.
        next-prompt v-prod with frame f-mid1.
        next.
      end.


/* LongLead 021510   */
     
      if (v-lmode = "a" or 
          (v-lmode = "c" and o-prod <> v-prod)) and 
        (({k-accept.i} or
        ((( frame-field = "v-shipdt" and
          {k-after.i} )  or
        (frame-field = "v-shipdt" and
        ((v-shipdt:cursor-offset = v-shipdt:width-chars and
           keylabel(lastkey) = "CURSOR-RIGHT") or                       
        (v-shipdt:cursor-offset = 1 AND 
           keylabel(lastkey) = "CURSOR-LEFT") )   ) ))))
        and
        not (can-find (first com use-index k-com where 
                             com.cono = g-cono     and 
                             com.comtype    = "puc"       and 
                             com.orderno    = v-quoteno  and
                             com.ordersuf   = 0          and
                             com.lineno     = v-lineno  
                             no-lock) ) then 
        do:
        assign g-whse = v-whse.
      end.
     
/* LongLead 021510 */
        
      assign v-totnet = v-sellprc * v-qty
             v-prod = input v-prod.
             /*v-reqprod = input v-prod.*/
      {w-icsp.i v-prod no-lock}
      /*if not avail icsp then /* non stock */*/
        assign v-totcost    = round(v-prodcost * v-qty,2)
               v-totnet     = round(v-sellprc  * v-qty,2).
      assign v-gp         = if v-totnet <> 0 then
                             (((v-totnet - v-totcost) / v-totnet) * 100)
                            else
                              0.
      
      find last icss where icss.cono = g-cono and
                           icss.prod = v-prod
                           no-lock no-error.
      if avail icss then
        assign zi-icspecrecno = icss.icspecrecno.
      else
        do:
        create icss.
        assign icss.cono        = g-cono
               icss.prod        = v-prod
               icss.icspecrecno = 1
               icss.transdt     = TODAY
               icss.transtm     = string(TIME,"HH:MM")
               icss.operinit    = g-operinits
               icss.statusfl    = no
               icss.transproc   = "oeexc"
               zi-icspecrecno   = 1.
      end. /* not avail icss */
      find t-lines where t-lines.lineno = v-lineno      and
                         t-lines.seqno  = 0 no-error.
      if not avail t-lines then
        do:  /* das - 06/30/09 - added the do: */
        create t-lines.
        assign t-lines.lineno     = v-lineno     
               t-lines.seqno      = 0
               t-lines.prod       = v-prod       
               t-lines.icnotefl   = v-icnotefl
               t-lines.descrip    = v-descrip
               t-lines.descrip2   = v-descrip2
               t-lines.xprod      = v-xprod
               t-lines.vendno     = v-vendno
               t-lines.slsrepout  = v-slsrepout
               t-lines.slsrepin   = v-slsrepin
               t-lines.prodcat    = v-prodcat
               t-lines.prodline   = v-prodline
               t-lines.listprc    = v-listprc
               t-lines.listfl     = v-listfl
/* %toms2% */
               t-lines.convertfl  = if v-conversion then 
                                      t-lines.convertfl else no
/* %toms2% */
               t-lines.pricetype  = if v-ptype <> " " then v-ptype
                                    else t-lines.pricetype
               t-lines.sellprc    = v-sellprc    
               t-lines.Lcommpct   = v-Lcommpct
               t-lines.prodcost   = v-prodcost
               t-lines.glcost     = v-glcost
               t-lines.surcharge  = v-surcharge
               t-lines.sparebamt  = v-sparebamt
               t-lines.leadtm     = v-leadtm
               t-lines.gp         = if v-gp = ? then 0 else v-gp        
               t-lines.qty        = v-qty        
               t-lines.qtyrem     = v-qtyrem
               t-lines.shipdt     = v-shipdt
               t-lines.specnstype = v-specnstype
               t-lines.pdamt      = v-pdamt
               t-lines.pd$md      = v-pd$md
               t-lines.pdbased    = v-pdbased
               t-lines.totnet     = v-sellprc * v-qty
               t-lines.lcomment   = v-lcomment
               t-lines.totcost    = v-prodcost * v-qty
               t-lines.rebatefl   = if v-rebatefl <> " " then v-rebatefl
                                    else t-lines.rebatefl
               t-lines.totmarg    = (v-sellprc - v-prodcost) * v-qty
               t-lines.margpct    = if v-sellprc <> 0 then
                                 ((v-sellprc - v-prodcost) / v-sellprc) * 100
                                    else
                                      0.
      end. /* not avail t-lines */
    
      find oeelb where oeelb.cono     = g-cono and
                       oeelb.batchnm  = string(w-batchnm,">>>>>>>9") and
                       oeelb.seqno    = 2 /*oeehb.seqno*/   and
                       oeelb.lineno   = v-lineno
                       no-error.
      if avail oeelb then 
        do:
        run Update_OEELB.
        run Quote-Total.
      end.
      else 
        do:
        if v-lineno = 1 and oeehb.whse = " " then 
          do:
          find     t-oeehb where 
                   t-oeehb.cono       = g-cono and   
                   t-oeehb.batchnm    = string(w-batchnm,">>>>>>>9") and
                   t-oeehb.seqno      = 2 and
                   t-oeehb.sourcepros = "ComQu" no-error.
          if avail t-oeehb then 
            do:
            assign t-oeehb.whse = v-whse
                   hdr-whse     = v-whse.
            if t-oeehb.whse <> " " then
              do:
          /* %Tom 08/03 make sure oeehb is recent */
              assign d-whse                = v-whse
                     t-oeehb.shiptonm      = if v-shiptonm <> "" then
                                                v-shiptonm 
                                             else
                                                t-oeehb.shiptonm
                     t-oeehb.shiptoaddr[1] = if v-shiptoaddr1 <> "" then
                                                v-shiptoaddr1
                                             else
                                                t-oeehb.shiptoaddr[1]
                     t-oeehb.shiptoaddr[2] = if v-shiptoaddr2 <> "" then
                                               v-shiptoaddr2 
                                             else
                                               t-oeehb.shiptoaddr[2]
                     t-oeehb.shiptocity    = if v-shiptocity <> "" then
                                               v-shiptocity
                                             else
                                               t-oeehb.shiptocity
                     t-oeehb.shiptost      = if v-shiptost <> "" then
                                               v-shiptost
                                             else
                                               t-oeehb.shiptost
                     t-oeehb.shiptozip     = if v-shiptozip <> "" then
                                               v-shiptozip
                                             else
                                               t-oeehb.shiptozip
                     t-oeehb.geocd         = if v-shiptogeocd <> 0 then
                                               v-shiptogeocd 
                                             else
                                               t-oeehb.geocd.
              assign v-shiptonm    = if v-shiptonm    = "" then 
                                       t-oeehb.shiptonm
                                     else v-shiptonm
                     v-shiptoaddr1 = if v-shiptoaddr1 = "" then 
                                       t-oeehb.shiptoaddr[1]
                                     else v-shiptoaddr1
                     v-shiptoaddr2 = if v-shiptoaddr2 = "" then 
                                       t-oeehb.shiptoaddr[2]
                                     else v-shiptoaddr2
                     v-shiptocity  = if v-shiptocity = "" then 
                                       CAPS(t-oeehb.shiptocity)
                                     else v-shiptocity
                     v-shiptost    = if v-shiptost    = "" then 
                                       CAPS(t-oeehb.shiptost)
                                     else v-shiptost
                     v-shiptozip   = if v-shiptozip   = "" then 
                                       t-oeehb.shiptozip
                                     else v-shiptozip
                     v-shiptogeocd = if v-shiptogeocd = 0 then
                                       t-oeehb.geocd
                                     else v-shiptogeocd.
              /* oeebtd.p is changing countrycd from ARSC */
              assign h-countrycd = t-oeehb.countrycd.
              if not v-conversion then
                run oeebtd.p("oeehb.",recid(t-oeehb),?,yes,v-shipfl).
              else
                run oeebtd.p("oeehb.",recid(t-oeehb),?,no,v-shipfl).
              assign t-oeehb.countrycd = h-countrycd.
              
              /* oeebtd.p is changing the shipto info. Change back if needed */
         
              /* %Tom 08/03 make sure oeehb buffer is the new one  */
      
              find t-oeehb where t-oeehb.cono = g-cono and
                   t-oeehb.batchnm = string(w-batchnm,">>>>>>>9") and 
                   t-oeehb.seqno = 2 and
                   t-oeehb.sourcepros = "ComQu"
                   no-error.
/* The warehouse was getting zapped as well */

              if t-oeehb.whse <> d-whse then
                assign t-oeehb.whse = d-whse
                       v-whse       = d-whse
                       hdr-whse     = d-whse.
              assign t-oeehb.approvty = "y". /* quotes are always approved */
              if t-oeehb.shiptonm      <> v-shiptonm then
                assign t-oeehb.shiptonm      = v-shiptonm.
              if t-oeehb.shiptoaddr[1] <> v-shiptoaddr1 then
                assign t-oeehb.shiptoaddr[1] = v-shiptoaddr1.
              if t-oeehb.shiptoaddr[2] <> v-shiptoaddr2 then
                assign t-oeehb.shiptoaddr[2] = v-shiptoaddr2.
              if t-oeehb.shiptocity    <> v-shiptocity then
                assign t-oeehb.shiptocity    = v-shiptocity.
              if t-oeehb.shiptozip     <> v-shiptozip then
                assign t-oeehb.shiptozip     = v-shiptozip.
              if t-oeehb.shiptost      <> v-shiptost then
                assign t-oeehb.shiptost      = v-shiptost.
              if t-oeehb.geocd         <> v-shiptogeocd then
                assign t-oeehb.geocd         = v-shiptogeocd.
              assign v-idoeeh  = recid(t-oeehb)
                     v-idoeeh0 = ?
                     g-secure  = v-secure
                     g-whse    = t-oeehb.whse
                     g-custno  = t-oeehb.custno
                     g-orderno = int(w-batchnm).
              run oeebti.p.
            end. /* t-oeehb.whse <> " " */
          end. /* avail t-oeehb */
        end. /* lineno = 1 and whse = " " */

        create oeelb.
        run Create_OEELB.
        run Quote-Total.
      end. /* oeelb not avail */
      
      if v-lmode = "a" then
        do:
        run Xref_Check_for_Options (input v-whse,
                                    input v-seqno,
                                    input-output v-qty,
                                    input-output v-prod).
      end.
      if v-linebrowseopen = false then
        do:
        close query q-lines.
        assign v-linebrowseopen = false.
      end.
        
      Run Line_Browse.
        
      /* initialize */
      if not v-conversion then
        assign v-lineno    = 0 /*/* %toms% */ if not v-conversion then
                                            0
                                          else
                                            v-lineno */
               v-seqno      = 0
               v-prod       = ""
               v-reqprod    = ""
               v-icnotefl   = ""
               v-specnstype = " "
               v-xprod      = ""
               orig-whse    = ""
               v-matrixed   = " "
               v-listprc    = 0
               v-listfl     = "n"
               v-sellprc    = 0
               v-gp         = 0
               v-disc       = 0
               v-qty        = 1
               /*v-qtyrem     = 0*/
               v-shipdt     = ?
               v-descrip    = ""
               v-descrip2   = ""
               v-lcomment   = " "
               v-prodcost   = 0
               h-prodcost   = 0
               v-glcost     = 0
               v-stndcost   = 0
               v-vendno     = 0  
               v-prodcat    = " "
               v-prodline   = " "
               v-sparebamt  = 0
               v-totcost    = 0
               v-totmarg    = 0
               v-margpct    = 0
               v-pdrecord   = 0
               v-pdtype     = 0
               v-pdamt      = 0
               v-pd$md      = ""
               v-pdbased    = " "
               v-totnet     = 0
               v-ptype      = " "
               v-rarrnotesfl = no
               v-5603fl     = no
               v-statustype = " "
               v-costoverfl = no
               v-surcharge  = 0 
               h-surcharge  = 0 
               v-chgsrchgfl = no
               v-slsrepovr  = no
               v-taxablefl  = no
               v-slsrepout  = ""
               v-slsrepin   = ""
               v-ctrlaovr   = no
               /*v-slsrepout = " "*/
               o-lineno     = 0
               o-seqno      = 0
               o-specnstype = " "
               o-prod       = ""
               o-xprod      = ""
               o-whse       = ""
               o-listprc    = 0
               o-sellprc    = 0
               o-gp         = 0
               o-disc       = 0
               o-disctype   = no
               o-qty        = 1
               o-shipdt     = ?
               cancel-xref  = no
               v-xreffl     = yes
               ns-descrip   = ""  
               ns-descrip2  = ""
               ns-vendno    =  0  
               ns-prodline  = ""  
               ns-prodcat   = ""  
               ns-listprc   =  0  
               ns-listfl    = "n" 
               ns-prodcost  =  0  
               ns-leadtm    = 30  
               ns-ptype     = ""  
               orig-cost    = 0
               over-cost    = 0
               manual-change = no.
       
/* %Toms10 */
      if (keylabel(lastkey) = "F9" and v-conversion ) then
        do:
        leave. /* MidLoop.*/
      end.
/* %Toms10 */



    end. /* if GO or frame-field = v-shipdt and lastkey = RETURN */      
    
    {k-sdinavigationQU.i &jmpcode = "return." }
  end.  /* MidLoop */
  
  /*release icsw.*/
  hide message no-pause.
 
  {k-sdinavigationQU.i &jmpcode = "leave." }
  
  if {k-jump.i} or {k-cancel.i} then
    do:
    run FindNextFrame (input lastkey,
                       input-output v-CurrentKeyPlace,
                       input-output v-framestate).

    leave.
  end.
  else
    run FindNextFrame (input lastkey,
                       input-output v-CurrentKeyPlace ,
                       input-output v-framestate).

  if keyfunction(lastkey) = "end-error" then
    do:
    run FindNextFrame (input lastkey,
                       input-output v-CurrentKeyPlace,
                       input-output v-framestate).

  end.

  assign v-middle1 = no
         v-linebrowseopen = no.
/****************************************************/  
end. /* Procedure Middle1 */


/* -------------------------------------------------------------------------  */
Procedure Bottom2:
/* -------------------------------------------------------------------------  */
 
  hide frame f-mid1   no-pause.
  hide frame f-lines  no-pause.
  hide frame f-prodlu no-pause.
  hide frame f-shiptolu no-pause.
  assign v-bottom2    = yes
         v-internalfl = yes
         v-5680fl     = no.

  /*
  on cursor-up tab.  
  on cursor-down cursor-down.
  */
  
  if not v-conversion then
    display with frame f-statusline1.
  else
    display with frame f-statuslinex.
  
  /* Added for returning from Jumping */
  if (v-CurrentKeyPlace <> 2 or v-framestate <> "B2") and 
      v-ManualFrameJump = yes then
    do:
    assign v-CurrentKeyPlace = 2
           v-framestate      = "B2"
           v-ManualFrameJump = true.
    find first sasos where sasos.cono = g-cono and
                       sasos.oper2 = g-operinits and
                       sasos.menuproc = "zxt1"
                       no-lock no-error.
    if avail sasos and sasos.securcd[12] > 1 then
      assign v-faxpopup = true.
    else
      assign v-faxpopup = false.
    if avail sasos and sasos.securcd[15] > 1 then
      assign v-taknbyovr = true.
    else
      assign v-taknbyovr = false.
  end. /* return from jumping */
  
  
  Bottom2Loop:
  
  do while true on endkey undo, leave Bottom2Loop:
    if v-internalfl then
      do:
      assign v-comments = "".
      for each notes where notes.cono = g-cono and
        notes.notestype = "ba"                 and
        notes.primarykey   = string(v-quoteno,">>>>>>>9") and
        notes.secondarykey = "I" + "c"
        no-lock:
        do j-inx = 1 to 16:
          assign v-comments =   v-comments  + 
                               (if notes.noteln[j-inx] <> "" then
                                   replace(notes.noteln[j-inx],chr(10),"") 
                                   + chr(10)  /* Editor block each line has a 
                                                line feed at the end   %TAH */
                                else
                                   "").
        end.                           
      end.
    
      display v-comments
        with frame f-bot2i.
     
      update v-comments
        go-on /*({k-sdigoon.i})  */ (f6 f7 f8 f10)
        with frame f-bot2i
      editing:
        readkey.
        if keylabel(lastkey) = "F10" then
          do:
          if input v-comments <> "" then 
            do:
            assign v-comments = input v-comments.
            run notes-create.
            for each t-oeehb where 
                     t-oeehb.cono       = g-cono and   
                     t-oeehb.batchnm    = string(w-batchnm,">>>>>>>9") and
                     t-oeehb.seqno      = 2 and
                     t-oeehb.sourcepros = "ComQu":
              if t-oeehb.notesfl <> "*" then assign t-oeehb.notesfl = "*"
                                                    v-notefl        = "*".
            end.
            display v-notefl with frame f-top1.
          end.
          readkey pause 0.
          assign v-internalfl = no
                 v-comments = "".
          hide frame f-bot2i no-pause.
          next Bottom2Loop.
        end.
    
        if keylabel(lastkey) = "PF2" then apply lastkey.
        if keylabel(lastkey) = "PF2" then
          do:
          put screen row 21 col 1 
          color message
         "F6-Customer   F7-Lines                                      F10-NOTES           ".
         view frame f-statusline1.
        end.

        if {k-jump.i} or {k-cancel.i} then
          do:
          if keylabel(lastkey) = "F11" then
            do:
            run Tag_SASOO (input w-batchnm,
                           input "Bottom2").
            hide frame f-bot2i no-pause.
            leave Bottom2Loop.
          end.
          
          run FindNextFrame (input lastkey,
                             input-output v-CurrentKeyPlace,
                             input-output v-framestate).

          if v-CurrentKeyPlace <= 0 then 
            do:
            hide frame f-bot2i no-pause.
            apply lastkey.
            leave.
          end.
          else 
            do:
            hide frame f-bot2i no-pause.
            apply lastkey.
            return.
          end.
          hide frame f-bot2i no-pause.
        end. /* if {k-jump.i} or {k-cancel.i} */
        apply lastkey.
      end. /* editing */
    end. /* internal */
    else
      do:
      assign v-comments = "".
      for each notes where notes.cono = g-cono and
        notes.notestype = "ba"                 and
        notes.primarykey   = string(v-quoteno,">>>>>>>9") and
        notes.secondarykey = "E" + "c"
        no-lock:
        do j-inx = 1 to 16:
          assign v-comments =  v-comments + 
                               (if notes.noteln[j-inx] <> "" then
                                   replace(notes.noteln[j-inx],chr(10),"") 
                                   + chr(10)  /* Editor block each line has a 
                                                line feed at the end   %TAH */
                                else
                                   "").
        end.    
        
        assign v-allfl =  notes.printfl 
               v-ackfl =  notes.printfl2
               v-pckfl =  notes.printfl3
               v-advfl =  notes.printfl4
               v-invfl =  notes.printfl5.
      end.
     display  v-comments
              v-allfl 
              v-ackfl
              v-pckfl
              v-advfl 
              v-invfl
               with frame f-bot2e.
      update v-comments
             v-allfl
             v-ackfl
             v-pckfl
             v-advfl
             v-invfl
        go-on /*({k-sdigoon.i})  */ (f6 f7 f8 f10)
        with frame f-bot2e
      editing:
        readkey.
        
        if frame-field = "v-allfl" and input v-allfl = yes then
          do:
          assign v-allfl = yes
                 v-ackfl = yes
                 v-pckfl = yes
                 v-advfl = yes
                 v-invfl = yes.
          display v-allfl 
                  v-ackfl 
                  v-pckfl 
                  v-advfl 
                  v-invfl 
                  with frame f-bot2e.
        end.
        
        if keylabel(lastkey) = "F10" then
          do:
          if input v-comments <> "" then 
            do:
            assign v-comments = input v-comments.
           /* {notes_create.i}             code size problems %TAH */
            run notes-create.
            for each t-oeehb where 
                     t-oeehb.cono       = g-cono and   
                     t-oeehb.batchnm    = string(w-batchnm,">>>>>>>9") and
                     t-oeehb.seqno      = 2 and
                     t-oeehb.sourcepros = "ComQu":
              if t-oeehb.notesfl <> "*" then assign t-oeehb.notesfl = "*"
                                                    v-notefl        = "*".
            end.
            display v-notefl with frame f-top1.
          end.
          readkey pause 0.
          assign v-internalfl = yes
                 v-comments = ""
                 v-allfl    = no
                 v-ackfl    = yes
                 v-pckfl    = no
                 v-advfl    = no
                 v-invfl    = no.
          hide frame f-bot2e.
          next Bottom2Loop.
        end.
        
        if keylabel(lastkey) = "PF2" then apply lastkey.
        if keylabel(lastkey) = "PF2" then
          do:
          put screen row 21 col 1 
          color message
         "F6-Customer   F7-Lines                                      F10-NOTES           ".
         view frame f-statusline1.
        end.
        
        if {k-jump.i} or {k-cancel.i} then
          do:
          if keylabel(lastkey) = "F11" or keylabel(lastkey) = "PF2" then
            do:
            run Tag_SASOO (input w-batchnm,
                           input "Bottom2").
            hide frame f-bot2e no-pause.
            leave Bottom2Loop.
          end.

          run FindNextFrame (input lastkey,
                             input-output v-CurrentKeyPlace,
                             input-output v-framestate).

          if v-CurrentKeyPlace <= 0 then 
            do:
            hide frame f-bot2e no-pause.
            apply lastkey.
            leave.
         end.
         else 
           do:
           hide frame f-bot2e no-pause.
           apply lastkey.
           return.
         end.
         hide frame f-bot2e no-pause.
        end. /* if {k-jump.i} or {k-cancel.i} */
        apply lastkey.  
      end. /* editing */
    end. /* external */

    
    if {k-accept.i} then 
      do:
      if v-comments <> "" then 
        do:
        /* {notes_create.i}             code size problems %TAH */
        run notes-create.
        for each t-oeehb where 
                 t-oeehb.cono       = g-cono and   
                 t-oeehb.batchnm    = string(w-batchnm,">>>>>>>9") and
                 t-oeehb.seqno      = 2 and
                 t-oeehb.sourcepros = "ComQu":
          if t-oeehb.notesfl <> "*" then 
            assign t-oeehb.notesfl = "*"
                   v-notefl        = "*".
        end.
        display v-notefl with frame f-top1.
      end.
      hide frame f-bot2e.
      leave Bottom2Loop.
    end.

    {k-sdinavigationQU.i &jmpcode = "return." }
  end.  /* Bottom2Loop */
  /*
  end. /* not das */
  */
  
  if {k-cancel.i} then 
    do:
    if v-top1 = yes then
      assign v-CurrentKeyPlace = 1
             v-framestate      = "t1".
    if v-middle1 = yes then
      assign v-CurrentKeyPlace = 2
             v-framestate      = "m1".
    /*
    run FindNextFrame (input lastkey,
                       input-output v-CurrentKeyPlace,
                       input-output v-framestate).
    */
    hide frame f-bot2i   no-pause.
    hide frame f-bot2e   no-pause.
    hide frame f-refresh no-pause.
    hide frame f-new-refresh no-pause.
    {k-sdinavigationQU.i &jmpcode = "return." }
    leave.
  end. /* if k-cancel */
  else 
    do:
    if not {k-jump.i} then
      run FindNextFrame (input lastkey,
                         input-output v-CurrentKeyPlace,
                         input-output v-framestate).
    hide frame f-bot2i   no-pause.
    hide frame f-bot2e   no-pause.
  end.
  assign v-bottom2 = no.
end. /* Procedure Bottom2 */


/* -------------------------------------------------------------------------  */
Procedure TopCustlookup:
/* -------------------------------------------------------------------------  */
  assign v-topcustlu = "".
  display v-topcustlu with frame f-toplu.
  TopCustLoop:
  do while true on endkey undo, leave TopCustLoop:
        
    update v-topcustlu
           go-on ({k-sdigoon.i} )  
    with frame f-toplu
    editing:
      readkey.
      if can-do("f6,f7,f10,f11",keylabel(lastkey)) then
        do:
        close query q-custlu.
        assign v-custbrowseopen = false
               v-manualframejump = true.
        hide frame f-custlu.
        hide frame f-zcustinfo.
        apply lastkey.
        leave TopCustLoop.
      end.
      
      if frame-field = "v-topcustlu" then
        do:
        if (not can-do("9,21,13,501,502,507,508",string(lastkey)) and
            keyfunction(lastkey) ne "go" and
            keyfunction(lastkey) ne "end-error") then
          do:
          apply lastkey.
          if frame-field <> "v-topcustlu" then
            next.
          assign v-topcustlu = input v-topcustlu. 
          run DoCustLu (input v-topcustlu,
                        input "normal",
                        input "f-toplu" ).
          /*assign v-custbrowsekeyed = true.*/
          next.
        end.
        else 
          if (lastkey = 501 or
              lastkey = 502 or
              lastkey = 507 or
              lastkey = 508) and v-custbrowseopen = true then
            do:
            run MoveCustLu (input lastkey,
                            input "f-toplu").
            assign v-custbrowsekeyed = true.
            next.
          end.
        else 
          if (lastkey = 13 or keyfunction(lastkey) = "go")
              and not avail arsc then
            do:
            assign v-topcustlu = input v-topcustlu. 
            display  v-custname
                     v-custno
            with frame f-top1.
            assign v-custbrowsekeyed = false.
            hide frame f-custlu.
            hide frame f-zcustinfo.
          end.
          else
            if (lastkey = 13 or keyfunction(lastkey) = "go")
                and v-custbrowsekeyed and avail arsc then
              do:
              assign v-custlu    = arsc.lookupnm 
                     v-topcustlu = arsc.lookupnm 
                     v-custno    = arsc.custno
                     g-custno    = arsc.custno
                     v-arnotefl  = arsc.notesfl
                     v-custname  = substr(arsc.name,1,20)
                     v-whse      = arsc.whse
                     v-currency  = if arsc.currencyty = "CN" then
                                     "CURRENCY: CN"
                                   else if arsc.currencyty = "LB" then
                                     "CURRENCY: LB"
                                   else " "
                     v-terms     = if avail arss then arss.termstype
                                   else arsc.termstype.
              display  v-custname
                       v-whse
                       v-custno
                       v-arnotefl
                       v-takenby
                       v-custlu
                       v-currency
                       v-terms
              with frame f-top1.
          
              display v-custlu with frame f-top1.
              assign v-custbrowsekeyed = false.
              hide frame f-custlu.
              hide frame f-zcustinfo.
              apply lastkey.
              leave.
            end.
            else
              assign v-custbrowsekeyed = false.
          end.
          apply lastkey.
    end. /* editing */
    if keyfunction(lastkey) = "go" or
       lastkey = 13 then
      do:
      close query q-custlu.
      assign v-custbrowseopen = false.
      hide frame f-custlu.
      hide frame f-zcustinfo.
    end. 
    if {k-accept.i} or lastkey = 13 then
      leave TopCustLoop.
  
  end.   /* TopCustLoop */

  {k-sdinavigationQU.i &jmpcode = "return." }

end. /* Procedure TopCustlookup */


/* -------------------------------------------------------------------------  */
Procedure DocustLu:
/* -------------------------------------------------------------------------  */
 
  define input parameter ix-custlu    as character format "x(15)" no-undo.
  define input parameter ix-runtype   as character                no-undo.
  define input parameter ix-callframe as character                no-undo.
  
  hide frame f-custlu.
  assign v-custnum = replace(ix-custlu,"0","")
         v-custnum = replace(v-custnum," ","")   
         v-custnum = replace(v-custnum,"1","")   
         v-custnum = replace(v-custnum,"2","")   
         v-custnum = replace(v-custnum,"3","")   
         v-custnum = replace(v-custnum,"4","")   
         v-custnum = replace(v-custnum,"5","")   
         v-custnum = replace(v-custnum,"6","")   
         v-custnum = replace(v-custnum,"7","")   
         v-custnum = replace(v-custnum,"8","")   
         v-custnum = replace(v-custnum,"9","").  

  if length(v-custnum) = 0 and ix-runtype = "normal" and 
     v-custlutype = g-custlutype and ix-custlu <> "" then
    do:
    for each b-w-cust:
      delete b-w-cust.
    end.  
    if can-find(first arsc use-index k-arsc-stat where 
                      arsc.cono = g-cono and
                      arsc.statustype = yes and
                      arsc.custno ge dec(ix-custlu)
                      no-lock) then 
      do:
      create b-w-cust.
      assign b-w-cust.custno = ?. /*trick query so they all can be the same*/
    end.
    open query q-custlu for each b-w-cust no-lock,
                            each arsc outer-join where 
                                 arsc.cono = g-cono and 
                                 arsc.custno ge dec(ix-custlu) and
                                 arsc.statustype = yes no-lock.

    assign v-custlulit =
  " Shift/F7-LOOKUPNM  Shift/F8-Phone  Shift/F9-Keywds  Shift/F10-City/St/Zip".
    end.
  else
  if v-custlutype = "l" then
    do:
    for each b-w-cust:
      delete b-w-cust.
    end.  
    if can-find(first arsc use-index k-lkup-stat where 
                      arsc.cono = g-cono and
                      arsc.statustype = yes and
                      arsc.lookupnm begins ix-custlu
                      no-lock) then 
      do:
      create b-w-cust.
      assign b-w-cust.custno = ?. /*trick query so they all can be the same*/
    end.
    open query q-custlu for each b-w-cust no-lock,
                            each arsc outer-join where 
                                 arsc.cono = g-cono and 
                                 arsc.lookupnm begins ix-custlu and
                                 arsc.statustype = yes no-lock.
    assign v-custlulit =
  " Shift/F7-LOOKUPNM  Shift/F8-Phone  Shift/F9-Keywds  Shift/F10-City/St/Zip".
    end.

  else
  if v-custlutype = "p" then
    do:
    for each b-w-cust:
      delete b-w-cust.
    end.
    if ix-runtype = "function" then
      do:
      Phoneloop:
      do while true on endkey undo Phoneloop, leave Phoneloop:
        update 
              v-lphone
          go-on (f17,f18,f19,f20,f6,f7,f8,f9 )  
          with frame f-arphone.
        if v-lphone = "" and {k-accept.i} then
          do:
          run err.p (2100).
          next-prompt v-lphone with frame f-arphone.
          next Phoneloop.
          end.
        if can-find(first arsc use-index k-phone-stat where 
                          arsc.cono = g-cono and 
                          arsc.statustype = yes and
                          arsc.phoneno begins v-lphone
                          /*arsc.lookupnm begins ix-custlu and*/
                          no-lock) then 
         do:
         create b-w-cust.
         assign b-w-cust.custno = ?. /*trick query so they all can be the same*/
        end.
        hide frame f-arphone.
        if {k-accept.i} or {k-cancel.i} then
          leave Phoneloop.
        if {k-jump.i} then
          do:
          return.  
          end.
        if keyfunction(lastkey) = "f17" or
           keyfunction(lastkey) = "f18" or
           keyfunction(lastkey) = "f19" or
           keyfunction(lastkey) = "f20" or
           keyfunction(lastkey) = "f6" or
           keyfunction(lastkey) = "f7" or
           keyfunction(lastkey) = "f8" or
           keyfunction(lastkey) = "f9" then
           do:
           return.  
         end.
      end.         /* PhoneLoop */
 
      open query q-custlu for each b-w-cust no-lock,
                             each arsc outer-join use-index k-phone-stat where 
                                  arsc.cono = g-cono and 
                                  arsc.statustype = yes and
                                  arsc.phoneno begins v-lphone
                                  /*arsc.lookupnm begins ix-custlu and*/
                                  no-lock.
      assign v-custlulit =
  " Shift/F7-LookupNm  Shift/F8-PHONE  Shift/F9-Keywds  Shift/F10-City/St/Zip".
      end.
    end.
  else
  if v-custlutype = "z" then
    do:
    for each b-w-cust:
      delete b-w-cust.
    end.
    if ix-runtype = "function" then
      do:
      ziploop:
      do while true on endkey undo ziploop, leave ziploop:
        update 
              v-lcity
              v-lstate
              v-lzipcd
          go-on (f17,f18,f19,f20,f6,f7,f8,f9 )  
          with frame f-arzipcd.
        
        if v-lcity <> "" and v-lstate = "" and {k-accept.i} then
          do:
          run err.p (2100).
          next-prompt v-lstate with frame f-arzipcd.
          next ziploop.
        end.
        hide frame f-arzipcd.
        if {k-accept.i} or {k-cancel.i} then
          leave ziploop.
        if {k-jump.i} then
          do:
          return.  
          end.
        if keyfunction(lastkey) = "f17" or
          keyfunction(lastkey) = "f18" or
          keyfunction(lastkey) = "f19" or
          keyfunction(lastkey) = "f20" or
          keyfunction(lastkey) = "f6" or
          keyfunction(lastkey) = "f7" or
          keyfunction(lastkey) = "f8" or
          keyfunction(lastkey) = "f9" then
          do:
          return.  
          end.
      end.         /* ZipLoop */
      hide frame f-arzipcd. 
      if v-lcity = "" and v-lstate = "" and v-lzipcd <> "" and 
        can-find(first arsc use-index k-zipcd-stat where 
                       arsc.cono = g-cono and 
                       arsc.statustype = yes and 
                       arsc.zipcd begins v-lzipcd and
                       arsc.lookupnm begins ix-custlu
                       no-lock) then 
       do:
       create b-w-cust.
       assign b-w-cust.custno = ?. /* trick query so they all can be the same*/
     end.
     else
     if can-find(first arsc use-index k-addr-stat where
                       arsc.cono = g-cono and
                       arsc.statustype = yes and
                       arsc.state = v-lstate and
                       arsc.city begins v-lcity and
                       arsc.lookupnm begins ix-custlu
                       no-lock) then
       do:
       create b-w-cust.
       assign b-w-cust.custno = ?. /*trick query they can be the same*/
     end.
     if v-lcity = "" and v-lstate = "" and v-lzipcd <> "" then
       do:
       open query q-custlu for each b-w-cust  no-lock,
       
                              each arsc outer-join use-index k-zipcd-stat where 
                                   arsc.cono = g-cono and 
                                   arsc.statustype = yes and
                                   arsc.zipcd begins v-lzipcd and
                                   arsc.lookupnm begins ix-custlu
                                   no-lock.
        assign v-custlulit =
  " Shift/F7-LookupNm  Shift/F8-Phone  Shift/F9-Keywds  Shift/F10-CITY/ST/ZIP".
        end.
      else
        do:
        open query q-custlu for each b-w-cust  no-lock,
    
                               each arsc outer-join use-index k-addr-stat where 
                                    arsc.cono = g-cono and 
                                    arsc.statustype = yes and
                                    arsc.state = v-lstate and
                                    arsc.city  begins v-lcity and
                                    arsc.lookupnm begins ix-custlu
                                    no-lock.
        assign v-custlulit =
  " Shift/F7-LookupNm  Shift/F8-Phone  Shift/F9-Keywds  Shift/F10-CITY/ST/ZIP".
        end.
      end.
    else
    if ix-runtype = "normal" then
      do:
      if v-lcity = "" and v-lstate = "" and v-lzipcd <> "" and
        can-find(first arsc use-index k-zipcd-stat where
                       arsc.cono = g-cono and
                       arsc.statustype = yes and
                       arsc.zipcd begins v-lzipcd and
                       arsc.lookupnm begins ix-custlu
                       no-lock) then
        do:
        create b-w-cust.
        assign b-w-cust.custno = ?. /* trick query they can be the same*/
      end.
      else
      if can-find(first arsc use-index k-addr-stat where
                        arsc.cono = g-cono and
                        arsc.statustype = yes and
                        arsc.state = v-lstate and
                        arsc.city begins v-lcity and
                        arsc.lookupnm begins ix-custlu
                        no-lock) then
        do:
        create b-w-cust.
        assign b-w-cust.custno = ?. /*trick query they can be the same*/
      end.
      if v-lcity = "" and v-lstate = "" and v-lzipcd <> "" then
        do:
        open query q-custlu for each b-w-cust  no-lock,
 
                              each arsc outer-join use-index k-zipcd-stat where 
                                   arsc.cono = g-cono and 
                                   arsc.statustype = yes and
                                   arsc.zipcd begins v-lzipcd and
                                   arsc.lookupnm begins ix-custlu
                                   no-lock.
        assign v-custlulit =
  " Shift/F7-LookupNm  Shift/F8-Phone  Shift/F9-Keywds  Shift/F10-CITY/ST/ZIP".
        end.
      else  
        do:
        open query q-custlu for each b-w-cust  no-lock,
                               each arsc outer-join use-index k-addr-stat where 
                                    arsc.cono = g-cono and 
                                    arsc.statustype = yes and
                                    arsc.state = v-lstate and
                                    arsc.city  begins v-lcity and
                                    arsc.lookupnm begins ix-custlu
                                    no-lock.
       assign v-custlulit =
  " Shift/F7-LookupNm  Shift/F8-Phone  Shift/F9-Keywds  Shift/F10-CITY/ST/ZIP".
       end.
      end.
    end.  /* if z */
  else
  if v-custlutype = "x" then
    do:
    if ix-runtype = "function" then
      do:
      run zsdicustkwd.p.
      open query q-custlu for each b-w-cust  no-lock,
 
                              each arsc use-index k-arsc-stat where 
                                   arsc.cono = g-cono and 
                                   arsc.statustype = yes and
                                   arsc.custno = b-w-cust.custno and
                                   arsc.lookupnm begins ix-custlu no-lock.

 
    assign v-custlulit =
  " Shift/F7-LookupNm  Shift/F8-Phone  Shift/F9-KEYWDS  Shift/F10-City/St/Zip".
  
      end.
    else
    if ix-runtype = "normal" then
      do:
/*    run zsdicustkwd.p. */
       open query q-custlu for each b-w-cust  no-lock,
 
                              each arsc use-index k-arsc-stat where 
                                   arsc.cono = g-cono and 
                                   arsc.statustype = yes and
                                   arsc.custno = b-w-cust.custno and
                                   arsc.lookupnm begins ix-custlu no-lock.

      assign v-custlulit =
  " Shift/F7-LookupNm  Shift/F8-Phone  Shift/F9-KEYWDS  Shift/F10-City/St/Zip".
  
      end.
    end. 
  assign v-custbrowseopen = true.    

  if ix-callframe = "f-toplu" then
    assign v-custbrowserow = 6
           lu-row = 16.
  else
  if ix-callframe = "f-top1" then
    assign v-custbrowserow = 6
           lu-row = 16.
                               
  display b-custlu v-custlulit with frame f-custlu.
  if avail arsc then
    run bottom_custoe.p (input arsc.custno,
                         input " ").
  if ix-callframe = "f-toplu" then
    apply "focus" to v-topcustlu in frame f-toplu.
  else
  if ix-callframe = "f-top1" then
    apply "focus" to v-custlu in frame f-top1.
end. /* Procedure DocustLu */

 
/* -------------------------------------------------------------------------  */
Procedure MovecustLu:
/* -------------------------------------------------------------------------  */
  
  define input parameter ix-lastkey as integer                 no-undo.
  define input parameter ix-callframe as character             no-undo.
  
  enable b-custlu with frame f-custlu.
  apply lastkey to b-custlu in frame f-custlu.
  if lastkey = 502 and  v-custbrowsekeyed = false then 
    do:
    apply x-cursorup to b-custlu in frame f-custlu.
  end.


  if avail arsc then
    run bottom_custoe.p (input arsc.custno,
                         input " ").


  apply "focus" to v-custlu in frame f-top1.

  if ix-callframe = "f-toplu" then
    put screen row 12 col 3 color message v-custlulit.
  else  
  if ix-callframe = "f-top1" then
    put screen row 15 col 3 color message v-custlulit.
end. /* Procedure MovecustLu */


/* -------------------------------------------------------------------------  */
Procedure DoShiptoLu:
/* -------------------------------------------------------------------------  */
 
  define input parameter ix-shiptolu  as character format "x(8)"  no-undo.
  define input parameter ix-runtype   as character                no-undo.
  define input parameter ix-callframe as character                no-undo.
  
  hide frame f-shiptolu.
  if v-Shiptolutype = "l" then
    do:
    for each b-w-shipto:
      delete b-w-shipto.
    end.  
    if can-find(first arss use-index k-arss where
                      arss.cono = g-cono and
                      arss.custno = v-custno and
                      arss.shipto begins ix-Shiptolu and
                      arss.statustype = yes no-lock) then do:
       create b-w-shipto.  /* trick query so they all can be the same */
       assign b-w-shipto.custno = ?
              b-w-shipto.shipto = "".
    end.
 
    open query q-shiptolu for each b-w-shipto no-lock,
                            each arss outer-join where 
                                 arss.cono = g-cono and 
                                 arss.custno = v-custno and
                                 arss.shipto begins ix-Shiptolu and
                                 arss.statustype = yes no-lock,
                            each arsc where arsc.cono = g-cono and
                                 arsc.custno = arss.custno no-lock.    
    /*
    if not avail arss then /* clean up outer join problem on a empty query */
      do:
      b-shiptolu:delete-current-row() in frame f-shiptolu.
      end.    
    */
    assign v-Shiptolulit =
  " Shift/F7-LOOKUPNM  Shift/F8-Phone                   Shift/F10-City/St/Zip".
 
    end.

  else
  if v-Shiptolutype = "p" then
    do:
    for each b-w-shipto:
      delete b-w-shipto.
    end.  
    if can-find(first arss use-index k-phone where 
                      arss.cono = g-cono and 
                      arss.custno = v-custno and
                      arss.phoneno begins v-lphone and
                      arss.shipto begins ix-Shiptolu and
                      arss.statustype = yes no-lock) then do:
       create b-w-shipto. /* trick query so they all can be the same */
       assign b-w-shipto.custno = ?
              b-w-shipto.shipto = "".
    end.
    if ix-runtype = "function" then
      do:
         
      Phoneloop:
      do while true on endkey undo Phoneloop, leave Phoneloop:
        update 
              v-lphone
          go-on (f17,f18,f19,f20,f6,f7,f8,f9 )  
          with frame f-arphone.
        
        if v-lphone = "" and {k-accept.i} then
          do:
          run err.p (2100).
          next-prompt v-lphone with frame f-arphone.
          next Phoneloop.
          end.
        
        hide frame f-arphone.
        
        if {k-accept.i} or {k-cancel.i} then
          leave Phoneloop.
        if {k-jump.i} then
          do:
          return.  
          end.
        if keyfunction(lastkey) = "f17" or
          keyfunction(lastkey) = "f18" or
          keyfunction(lastkey) = "f19" or
          keyfunction(lastkey) = "f20" or
          keyfunction(lastkey) = "f6" or
          keyfunction(lastkey) = "f7" or
          keyfunction(lastkey) = "f8" or
          keyfunction(lastkey) = "f9" then
          do:
          return.  
          end.
      end.         /* PhoneLoop */
     
      open query q-shiptolu for each b-w-shipto no-lock,
  
                              each arss outer-join use-index k-phone where 
                                   arss.cono = g-cono and 
                                   arss.custno = v-custno and
                                   arss.phoneno begins v-lphone and
                                   arss.shipto begins ix-Shiptolu and
                                   arss.statustype = yes no-lock,
                              each arsc where arsc.cono = g-cono and
                                   arsc.custno = arss.custno no-lock.    
      /*       
      if not avail arsc then /* clean up outer join problem on a empty query */
        do:
        b-shiptolu:delete-current-row() in frame f-shiptolu.
        end.    
      */
 
      assign v-Shiptolulit =
  " Shift/F7-LookupNm  Shift/F8-PHONE                   Shift/F10-City/St/Zip".
      end.
    end.
  else
  if v-Shiptolutype = "z" then
    do:
    for each b-w-shipto:
      delete b-w-shipto.
    end.  
    if can-find(first arss use-index k-zipcd where 
                      arss.cono = g-cono and 
                      arss.custno = v-custno and 
                      arss.zipcd begins v-lzipcd and
                      arss.shipto begins ix-Shiptolu and
                      arss.statustype = yes no-lock) then do:
       create b-w-shipto. /* trick query so they all can be the same */
       assign b-w-shipto.custno = ?
              b-w-shipto.shipto = "".
    end.
    
    if ix-runtype = "function" then
      do:
      ziploop:
      do while true on endkey undo ziploop, leave ziploop:
        update 
              v-lcity
              v-lstate
              v-lzipcd
          go-on (f17,f18,f19,f20,f6,f7,f8,f9 )  

          with frame f-arzipcd.
        
        if v-lcity <> "" and v-lstate = "" and {k-accept.i} then
          do:
          run err.p (2100).
          next-prompt v-lstate with frame f-arzipcd.
          next ziploop.
          end.
 
        hide frame f-arzipcd.
        
        if {k-accept.i} or {k-cancel.i} then
          leave ziploop.
        if {k-jump.i} then
          do:
          return.  
          end.
        if keyfunction(lastkey) = "f17" or
          keyfunction(lastkey) = "f18" or
          keyfunction(lastkey) = "f19" or
          keyfunction(lastkey) = "f20" or
          keyfunction(lastkey) = "f6" or
          keyfunction(lastkey) = "f7" or
          keyfunction(lastkey) = "f8" or
          keyfunction(lastkey) = "f9" then
          do:
          return.  
          end.
      end.         /* ZipLoop */
       
      if v-lcity = "" and v-lstate = "" and v-lzipcd <> "" then
        do:
        open query q-shiptolu for each b-w-shipto  no-lock,
    
                                each arss outer-join use-index k-zipcd where 
                                     arss.cono = g-cono and 
                                     arss.custno = v-custno and 
                                     arss.zipcd begins v-lzipcd and
                                     arss.shipto begins ix-Shiptolu and
                                     arss.statustype = yes no-lock,
                                each arsc where arsc.cono = g-cono and
                                     arsc.custno = arss.custno no-lock.    
        /*
        if not avail arss then 
          /* clean up outer join problem on a empty query */
          do:
          b-shiptolu:delete-current-row() in frame f-shiptolu.
          end.    
        */
        assign v-Shiptolulit =
  " Shift/F7-LookupNm  Shift/F8-Phone                   Shift/F10-CITY/ST/ZIP".
        end.
      else
        do:
        open query q-shiptolu for each b-w-shipto  no-lock,
    
                                each arss outer-join use-index k-addr where 
                                     arss.cono = g-cono and 
                                     arss.custno = v-custno and
                                     arss.state = v-lstate and
                                     arss.city  begins v-lcity and
                                     arss.shipto begins ix-Shiptolu and
                                     arss.statustype = yes no-lock,
                                each arsc where arsc.cono = g-cono and
                                     arsc.custno = arss.custno no-lock.    

        /*
        if not avail arss then 
          /* clean up outer join problem on a empty query */
          do:
          b-shiptolu:delete-current-row() in frame f-shiptolu.
          end.    
        */
        assign v-Shiptolulit =
  " Shift/F7-LookupNm  Shift/F8-Phone                   Shift/F10-CITY/ST/ZIP".
        end.
      end.
    else
    if ix-runtype = "normal" then
      do:
      if v-lcity = "" and v-lstate = "" and v-lzipcd <> "" then
        do:
 
        open query q-shiptolu for each b-w-shipto  no-lock,
 
                                each arss outer-join use-index k-zipcd where 
                                     arss.cono = g-cono and 
                                     arss.custno = v-custno and
                                     arss.zipcd begins v-lzipcd and
                                     arss.shipto begins ix-Shiptolu and
                                     arss.statustype = yes no-lock,
                                each arsc where arsc.cono = g-cono and
                                     arsc.custno = arss.custno no-lock.    
        /*
        if not avail arss then 
          /* clean up outer join problem on a empty query */
          do:
          b-shiptolu:delete-current-row() in frame f-shiptolu.
          end.    
        */
        assign v-Shiptolulit =
  " Shift/F7-LookupNm  Shift/F8-Phone                   Shift/F10-CITY/ST/ZIP".
  
        end.
      else  
        do:
 
        open query q-shiptolu for each b-w-shipto  no-lock,
   
                                each arss outer-join use-index k-addr where 
                                     arss.cono = g-cono and 
                                     arss.custno = v-custno and
                                     arss.state = v-lstate and
                                     arss.city  begins v-lcity and
                                     arss.shipto begins ix-Shiptolu and
                                     arss.statustype = yes no-lock,
                                each arsc where arsc.cono = g-cono and
                                     arsc.custno = arss.custno no-lock.    
        /*
        if not avail arss then 
          /* clean up outer join problem on a empty query */
          do:
          b-shiptolu:delete-current-row() in frame f-shiptolu.
          end. 
        */
       assign v-Shiptolulit =
  " Shift/F7-LookupNm  Shift/F8-Phone                   Shift/F10-CITY/ST/ZIP".
       end.
      end.
    end.  /* if z */
 /* 
  else
  if v-Shiptolutype = "x" then
    do:
    if ix-runtype = "function" then
      do:
      run zsdicustkwd.p.
      open query q-shiptolu for each b-w-shipto  no-lock,
 
                              each arss where 
                                   arss.cono = g-cono and 
                                   arss.custno = b-w-shipto.custno and
                                   arss.shipto = b-w-shipto.shipto and
                                   arss.statustype = yes no-lock,
                              each arsc where arsc.cono = g-cono and
                                   arsc.custno = arss.custno no-lock.    

 
    assign v-Shiptolulit =
  " Shift/F7-LookupNm  Shift/F8-Phone  Shift/F9-KEYWDS  Shift/F10-City/St/Zip".
  
      end.
    else
    if ix-runtype = "normal" then
      do:
/*    run zsdicustkwd.p. */
       open query q-shiptolu for each b-w-shipto  no-lock,
 
                              each arss where 
                                   arss.cono = g-cono and 
                                   arss.custno = b-w-shipto.custno and
                                   arss.shipto = b-w-shipto.shipto and
                                   arss.statustype = yes no-lock,
                              each arsc where arsc.cono = g-cono and
                                   arsc.custno = arss.custno no-lock.    

      assign v-Shiptolulit =
  " Shift/F7-LookupNm  Shift/F8-Phone  Shift/F9-KEYWDS  Shift/F10-City/St/Zip".
  
      end.
    end. 
  */
  assign v-Shiptobrowseopen = true.    

  /*
  if ix-callframe = "f-top1" then
    assign v-custbrowserow = 3
           lu-row = 13.                                    
  else
  */
  if ix-callframe = "f-top1" then
    assign v-custbrowserow = 6
           lu-row = 16.

                               
                               
  display b-shiptolu v-Shiptolulit with frame f-shiptolu.
  if avail arss then
    run bottom_custoe.p (input arsc.custno,
                         input arss.shipto).
  if ix-callframe = "f-top1" then
    apply "focus" to v-shipto in frame f-top1.
  else
  if ix-callframe = "f-top1" then
    apply "focus" to v-custlu in frame f-top1.
end. /* Procedure DoShiptoLu */

 
/* -------------------------------------------------------------------------  */
Procedure MoveshiptoLu:
/* -------------------------------------------------------------------------  */
  
  define input parameter ix-lastkey as integer                 no-undo.
  define input parameter ix-callframe as character             no-undo.
  
  enable b-shiptolu with frame f-shiptolu.
  apply lastkey to b-shiptolu in frame f-shiptolu.
  if lastkey = 502 and  v-shiptobrowsekeyed = false then 
    do:
    apply x-cursorup to b-shiptolu in frame f-shiptolu.
  end.


  if avail arss then
    run bottom_custoe.p (input arsc.custno,
                         input arss.shipto).


  apply "focus" to v-shipto in frame f-top1.

  if ix-callframe = "f-top1" then
    put screen row 15 col 3 color message v-Shiptolulit.
end. /* Procedure MoveshiptoLu */


/* -------------------------------------------------------------------------  */
Procedure DoProdLu:
/* -------------------------------------------------------------------------  */
   define input parameter ix-Prod    as character format "x(24)" no-undo.
   define input parameter ix-runtype as character                no-undo.

  hide frame f-prodlu.
  if v-prodlutype = "l" then
    do:
    for each b-w-prod:
      delete b-w-prod.
    end.  
    if can-find(first icsp use-index k-lkup where 
                      icsp.cono = g-cono and
                      icsp.statustype <> "I" and
                      icsp.lookupnm begins ix-prod no-lock) then 
      do:
      create b-w-prod.
      assign b-w-prod.prod = ".". /*trick query so they all can be the same*/
    end.
    
    open query q-prodlu for each b-w-prod  no-lock,  
    
                            first bq-icsw where bq-icsw.cono = g-cono no-lock,
       
                            each icsp outer-join use-index k-lkup where 
                                 icsp.cono = g-cono and 
                                 icsp.statustype <> "I" and
                                 icsp.lookupnm begins ix-prod no-lock.
    /*
    if not avail icsp then /* clean up outer join problem on a empty query */
      do:
      b-prodlu:delete-current-row() in frame f-prodlu.
      end.    
    */
    assign v-prodlulit =
  "SHIFT  /F7-LOOKUPNM   /F8-Product   /F9-Keywds   /F10-Vendor   /F3-Catalog".
    end.
  else
  if v-prodlutype = "p" then
    do:
    for each b-w-prod:
      delete b-w-prod.
    end.  
    if can-find(first icsp use-index k-icsp where 
                      icsp.cono = g-cono and
                      icsp.statustype <> "I" and
                      icsp.prod begins ix-prod no-lock) then do:
       create b-w-prod.
       assign b-w-prod.prod = ".". /*trick query so they all can be the same*/
    end.
    open query q-prodlu for each b-w-prod no-lock,

                            first bq-icsw where bq-icsw.cono = g-cono no-lock,
     
                            each icsp outer-join use-index k-icsp where 
                                 icsp.cono = g-cono and 
                                 icsp.statustype <> "I" and
                                 icsp.prod begins ix-prod no-lock.
                                 
    /*
    if not avail icsp then /* clean up outer join problem on a empty query */
      do:
      b-prodlu:delete-current-row() in frame f-prodlu.
      end. 
    */
    assign v-prodlulit =
  "SHIFT  /F7-LookUpNm   /F8-PRODUCT   /F9-Keywds   /F10-Vendor   /F3-Catalog".
    end. 
  else
  if v-prodlutype = "x" then
    do:
    if ix-runtype = "function" then
      do:
      run zsdiprodkwd.p.
      open query q-prodlu for each b-w-prod where
                              b-w-prod.prod begins ix-prod no-lock,
                              first bq-icsw where bq-icsw.cono = g-cono no-lock,
                              each icsp where 
                                   icsp.cono = g-cono and 
                                   icsp.statustype <> "I" and
                                   icsp.prod = b-w-prod.prod no-lock.
      end.
      /*
      open query q-prodlu for each b-w-prod  no-lock,
                              first bq-icsw where bq-icsw.cono = g-cono no-lock,
                              each icsp where 
                                   icsp.cono = g-cono and 
                                   icsp.statustype <> "I" and
                                   icsp.prod = b-w-prod.prod no-lock.
 
      end.
      */
    else
    if ix-runtype = "normal" then
      do:
      run zsdiprodkwd.p. 
      open query q-prodlu for each b-w-prod where
                              b-w-prod.prod begins ix-prod no-lock,
                              first bq-icsw where bq-icsw.cono = g-cono no-lock,
                              each icsp where 
                                   icsp.cono = g-cono and 
                                   icsp.statustype <> "I" and
                                   icsp.prod = b-w-prod.prod no-lock.
      end.
     
    assign v-prodlulit =
  "SHIFT  /F7-LookUpNm   /F8-Product   /F9-KEYWDS   /F10-Vendor   /F3-Catalog".
    end.
  else
  if v-prodlutype = "v" then
    do:
    if ix-runtype = "function" then
      do:
      vendloop:
      do while true on endkey undo vendloop, leave vendloop:
        update v-whse
               v-vendno
          go-on (f17,f18,f19,f20,f6,f7,f8,f9 )  

          with frame f-icvend.
        hide frame f-icvend.
        if {k-accept.i} or {k-cancel.i} then
          leave vendloop.
        if {k-cancel.i} or {k-jump.i} then
          do:
          return.  
          end.
        /*
        if keylabel(lastkey) = "PF4" then do:   /**&&**/
          return.
        end.
        */
          
        if keyfunction(lastkey) = "f17" or
          keyfunction(lastkey) = "f18" or
          keyfunction(lastkey) = "f19" or
          keyfunction(lastkey) = "f20" or
          keyfunction(lastkey) = "f6" or
          keyfunction(lastkey) = "f7" or
          keyfunction(lastkey) = "f8" or
          keyfunction(lastkey) = "f9" then
          do:
          return.  
          end.

      end.         
      
      
      for each b-w-prod:
        delete b-w-prod.
      end.  
      /*
      if can-find(first icsp use-index k-icsp where 
                        icsp.cono = g-cono and
                        icsp.statustype <> "I" and
                        icsp.prod begins ix-prod no-lock) then do:
      */
        create b-w-prod.
        assign b-w-prod.prod = ".". /*trick query so they all can be the same*/
      /*
      end.
      */
      open query q-prodlu for each b-w-prod where 
                                   b-w-prod.prod = "." no-lock,
    
                              each bq-icsw outer-join
                                   use-index k-vendor where 
                                   bq-icsw.cono = g-cono and 
                                   bq-icsw.whse = v-whse and
                                   bq-icsw.arpvendno = v-vendno  no-lock,
                              each icsp where 
                                   icsp.cono = g-cono and 
                                   icsp.statustype <> "I" and
                                   icsp.prod = bq-icsw.prod no-lock.
                                   
          
      if not avail icsp then /* clean up outer join problem on a empty query */
        do:
        b-prodlu:delete-current-row() in frame f-prodlu.
        end. 
      
      end.
      
    else
    if ix-runtype = "normal" then
      do:
      open query q-prodlu for each b-w-prod no-lock,
 
    
                              each bq-icsw outer-join 
                                   use-index k-whse where 
                                   bq-icsw.cono = g-cono and 
                                   bq-icsw.whse = v-whse and
                                   bq-icsw.arpvendno = v-vendno and
                                   bq-icsw.prod begins ix-prod no-lock,
                              each icsp where 
                                   icsp.cono = g-cono and 
                                   icsp.statustype <> "I" and
                                   icsp.prod = bq-icsw.prod and
                                   icsp.prod begins ix-prod no-lock.
                                   
      /*    
      if not avail icsp then /* clean up outer join problem on a empty query */
        do:
        b-prodlu:delete-current-row() in frame f-prodlu.
        end. 
      */
      end.
     
    assign v-prodlulit =
  "SHIFT  /F7-LookUpNm   /F8-Product   /F9-KEYWDS   /F10-Vendor   /F3-Catalog".
    end.                                    
  
                                   
                                   
                                   
  assign v-prodbrowseopen = true. 
  display b-prodlu v-prodlulit with frame f-prodlu.
end. /* Procedure DoProdLu */

/*************
/* -------------------------------------------------------------------------  */
Procedure DoCatLu:
/* -------------------------------------------------------------------------  */
   define input parameter ix-Prod    as character format "x(24)" no-undo.
   define input parameter ix-runtype as character                no-undo.
   assign v-start = " "
          v-save  = " ".
   run oeexqicsclu.p.
   readkey pause 0.
   assign s-prod = g-prod
          v-prod = g-prod.
   if s-prod <> " " then
     assign catalogfl = yes.
   display v-prod with frame f-mid1.
end. /* Procedure DoCatLu */
************/


/* -------------------------------------------------------------------------  */
 Procedure MoveProdLu:
/* -------------------------------------------------------------------------  */
 
  define input parameter ix-lastkey as integer                 no-undo.
/* TAH 07/02/09 */ 
  set b-prodlu:refreshable = false with frame f-prodlu.
/* TAH 07/02/09 */
  enable b-prodlu with frame f-prodlu.
/* TAH 07/02/09 */ 
  v-prodbeg:dcolor in browse b-prodlu = 0.
/* TAH 07/02/09 */
 
  apply lastkey to b-prodlu in frame f-prodlu.

  if lastkey = 502 and  v-prodbrowsekeyed = false then 
    do:
    apply x-cursorup to b-prodlu in frame f-prodlu.
  end.
  
/* TAH 07/02/09 */
  v-prodbeg:dcolor in browse b-prodlu = 2.
  disable b-prodlu with frame f-prodlu.
  set b-prodlu:refreshable = true with frame f-prodlu.
/* TAH 07/02/09 */
   
  put screen row 16 col 3 color message v-prodlulit. 

end. /* Procedure MoveProdLu */

/* -------------------------------------------------------------------------  */
Procedure DoLineLu:
/* -------------------------------------------------------------------------  */
  
  hide frame f-lines.
  run Line_Browse.
  /*
  open query q-lines for each t-lines. 
  */
                                  
  assign v-linebrowseopen = true.
  display b-lines with frame f-lines.
  /*
  assign v-blanks = ">>>>>>>>>>>>>>>>>".
  */
  /*
  assign v-blanks = "                 ".
  put screen row 22 col 3 /*color message*/ v-blanks.
  */
end. /* Procedure DolineLu */

/* -------------------------------------------------------------------------  */
Procedure MoveLineLu:
/* -------------------------------------------------------------------------  */
 
  define input parameter ix-lastkey as integer                 no-undo.
  enable b-lines with frame f-lines.
  apply lastkey to b-lines in frame f-lines.
  if lastkey = 502 and  v-linebrowsekeyed = false then 
    do:
    apply x-cursorup to b-lines in frame f-lines.
  end.

end. /* Procedure MoveLineLu */

{p-oeexcproc.i}

{p-oeexccnt.i}


