/* p-oeeaih.i 1.1 01/03/98 */
/* p-oeeaih.i 1.1 01/03/98 */
/*h****************************************************************************  INCLUDE      : p-oeepaih.i
  DESCRIPTION  : Fields used in the "Addon" record in the edi 810, 843, and 855
  USED ONCE?   : no
  AUTHOR       : jlc
  DATE WRITTEN : 02/23/94
  CHANGES MADE :
*******************************************************************************/
"Addon " +
    caps(s-addallchg) +
    caps(s-addonamt)  +
    caps(s-addondesc) +
    caps(s-addsvccd)  +
    caps(s-addchgcd)

