/***************************************************************************
 PROGRAM NAME: porzc_b.p
  PROJECT NO.: 
  DESCRIPTION: This program will fax vendor expediting information created
               from PORZX. If the option 10 is selected on the PORZX screen.
  DATE WRITTEN: 10/13/04.
  AUTHOR      : SunSource.  
  MODIFIED: 
***************************************************************************/

define input parameter p-prttype as char format "x". 

def new shared var s-printer   like sasp.printernm    no-undo.
def new shared var s-directory as char format "x(40)" no-undo.
def new shared var z-filename  as c format "x(70)"    no-undo.
def new shared var s-filename  as c format "x(70)"    no-undo.

define var s-type       as c format "x(1)" no-undo.
define var buyer_in     as c format "x(4)" extent 8 no-undo.
define var vndr_in      as i format "999999999999" extent 15 no-undo.
define var ln_cntr      as i format "99".
define var hold_name    as c format "x(30)" init " ". 
define var comment-ws   as c format "x(25)" init " ". 
define var newackn-dt   as c format "xx/xx/xx" init " ". 
define var expship-dt   as date      no-undo. 
define var expship-dtl  as c format "x(9)" init " ". 
define var chgflg       as c format "x" init " ". 
define var vndrord      as c format "x(15)" init " ". 
define var blnk_buyer   as c format "x(4)" no-undo.
define var v-buyernm    like apsv.name  no-undo.
define var v-oimspph    like oimsp.phoneno    init "" .
define var v-oimspfax   like oimsp.faxphoneno init "".
define var v-faxphone   like apsv.faxphoneno.
define var v-fromnm     like apsv.name  no-undo.
define var v-expdnm     like apsv.name  no-undo.
define var m-headerfl   as logical.
define var gota-hit     as logical.
define var m-ponbr      as char format "x(10)" init " "  no-undo.
define var v-headingfl  as logical.
define var v-myoutputty as c format  "x(1)" init "f".
define var v-tempprt    as c format  "x(40)" no-undo.
define var v-20size     as c format  "x(22)".
define var v-14size     as c format  "x(22)".
define var v-14bold     as c format  "x(22)".
define var v-compress   as c format  "x(22)".
define var v-sdifont    as c format  "x(50)".
define var idx2         as int                          no-undo.
define shared var p-n-codes as logical init no          no-undo.
define shared var p-excl-r  as char format "x"          no-undo.
define shared var p-excl-x  as char format "x"          no-undo.
define shared var p-blnk-buyer as logical               no-undo.
define shared var p-excl-n  as char format "x"          no-undo.
define shared var p-excl-s  as char format "x"          no-undo.
define shared var p-valids  as char format "x(30)"      no-undo.
define shared var p-delim   as char format "x" init "," no-undo.

define var ackncd       as c format "x" init " ". 

define temp-table bts no-undo 
    field bts_cono       like poeh.cono 
    field bts_buyer      like poeh.buyer
    field bts_vendno     like poeh.vendno
    field bts_shipfmno   like poeh.shipfm 
    field bts_pono       like poeh.pono
    field bts_posuf      like poeh.posuf 
    field bts_lineno     like poel.lineno
    field bts_stagecd    like poeh.stagecd
    field bts_ordaltno   like poeh.orderaltno
    field bts_ordaltsuf  like poeh.orderaltsuf
    field bts_ordentdt   like poeh.enterdt
    field bts_ordduedt   like poeh.duedt
    field bts_receiptdt  like poeh.receiptdt
    field bts_totlineamt like poeh.totlineamt
    field bts_linecnt    like poeh.nolineitem
    field bts_transdt    like poeh.transdt
    field bts_transtm    like poeh.transtm
    index bts_cono 
          bts_buyer
          bts_vendno
          bts_shipfmno
          bts_pono
          bts_lineno
          bts_ordentdt.



{p-rptbeg.i}
{g-porzc.i}

DEFINE VARIABLE qbf-i AS INTEGER NO-UNDO.
DEFINE VARIABLE qbf-t AS INTEGER NO-UNDO.

def buffer b-sapbo for sapbo.
def            var s-faxphoneno    like apsv.faxphoneno        no-undo.
def            var v-reportno      as i        initial 0       no-undo.
def            var v-coldfl        like sassr.coldfl           no-undo.

/*tb 19484 10/03/96 des; Warehouse Logistics Add return logic */
def            var v-processty   as char format "x(3)"         no-undo.

def buffer b-sasc  for sasc.
def buffer b-icsd  for icsd.
def buffer b-poeh  for poeh.

{lasersdi.lva new}

define frame n-header
    skip (5)     
    v-compress at 1 no-label
    skip (1)
    with width 150 no-labels no-box down.

/*********   main logic begins here *****************/

v-20size = "~033" + "(10U" + "~033" + "(s1p20v0s3b4148T".
v-14size = "~033" + "(10U" + "~033" + "(s1p14v0s3b4148T".

v-compress  =  /*"~033"  + "(8U" + "~033" + "(s0p16.67h8.5v0s0b0T". */
               "~033" + "(8U" + "~033" + "(s0p12.50h8.5v0s0b0T" +
               "~033" + "&k9.5h" + "~033" + "&l8C".  
 
v-sdifont   = v-esc + "(8U" + v-esc + "(s0p12.50h8.5v0s0b0T" +
              v-esc + "&k9.5h" + v-esc + "&l8C".

v-14bold = "~033" + "(10U" + "~033" + "(s1p14v0s3b25093". 

{w-sasc.i no-lock " " b-}

/*si01 begin*/
if p-prttype = "f" then 
do:
{w-sasp.i "'fax'" no-lock}
if (not avail sasp) or (sasp.ptype <> "f") or 
    g-ourproc = "poerm" then 
 do:                                    /*si01*/
    /*o Set up the fax banner and heading variables */
    {p-fax1.i &extent = "1"}
 end. /* if not avail sasp */
/*si01 end */
end.
else 
do:
   {w-sasp.i sapb.printernm no-lock}
   if avail sasp then 
      assign v-tempprt = sasp.pcommand.
end. 

assign
    v-headingfl = true
    v-title     = "Purchase Order"
/*si01 begin */
    v-rxfaxdir  = sasc.printdir
    v-rxfaxdir  = v-rxfaxdir +
                  (if substr(v-rxfaxdir,length(v-rxfaxdir),1) = "/"
                   then "" else "/")
    v-tempprt   = if not avail sasp then 
                     v-rxfaxdir + sapb.printernm
                  else 
                     v-tempprt.
/*si01 end*/

ASSIGN qbf-t     = TIME.

run build_newtmp.
assign s-type = p-prttype.
if avail sasp and sasp.printernm = "vid" then 
 do: 
   assign s-directory = v-rxfaxdir
          s-printer   = "vid".
 end.

DO FOR poeh,poel,apsv,sasta,oimsp:
mainloop:
for each bts where bts_cono > 0  and bts_pono > 0  no-lock,
      each poeh where poeh.cono  = bts_cono and 
                      poeh.pono  = bts_pono and
                      poeh.posuf = bts_posuf
      no-lock,
      each poel of poeh
          where  poel.cono   = poeh.cono and 
                 poel.pono   = bts_pono  and 
                 poel.posuf  = bts_posuf and 
                 poel.lineno = bts_lineno
                 no-lock,
      each apsv OF poeh where
           apsv.cono = bts_cono NO-LOCK
           BREAK by apsv.vendno
                 by bts_shipfmno
                 BY bts_buyer descending 
                 BY poeh.pono:

   assign m-ponbr = string(dec(poel.pono)) + "-00"
          vndrord = right-trim(substring(poel.user2,38,35)).       

   if poeh.shipfmno ne 0 then
     {w-apss.i poeh.vendno poeh.shipfmno no-lock}

   if poeh.billtowhse ne "" then 
      {w-icsd.i poeh.billtowhse no-lock}

   if  substring(poel.user1,40,8) ne "" and 
       poel.reqshipdt <  poel.expshipdt then 
       assign chgflg = "*".
   else 
       assign chgflg = " ".

   assign expship-dt  = poel.expshipdt
          expship-dtl = string(expship-dt,"99/99/99") + chgflg. 
   
   if poel.nonstockty = "" then
   do:                    
      {w-icsw.i poel.shipprod poel.whse no-lock}      
   end.
   
  /** checking for icsw B/O situaiton - option 4 
      this option ignores option 1 - days late check **/
      
  /* gota-hit controls printing */
   assign gota-hit = yes.

  if p-onlybo or p-notltbo then 
  do:
    if poel.nonstockty = "n" or
      (avail icsw and                              
      (icsw.qtybo ne 0 or icsw.qtydemand ne 0)) then 
     do: 
        assign gota-hit = yes.
     end. 
    else 
    if p-onlybo then
       assign gota-hit = no.
   end.  
  daysdiff = 0. 
  if p-dueorexp = "d" then 
  do:
    if poel.duedt >= today then do: 
     assign daysdiff = poel.duedt - today. 
     /* if daysdiff < 0 and ((daysdiff * -1) >= p-latedays) then  */
     
       /* option 3  checks icsw b/o qty's in conjunction with opt 1 */ 
      if daysdiff <= p-latedays then  
      do:
         assign gota-hit = yes.
      end.
     else 
        assign gota-hit = no.
   end. /* poel > today */     
  end.
  else  
  if p-dueorexp = "e" then 
   do:
     if poel.expshipdt >= today then do:
      assign daysdiff = poel.expshipdt - today.
      /* if daysdiff < 0 and ((daysdiff * -1) >= p-latedays) then  */
      
      /* option 3 checks icsw b/o qty's in conjunction with opt 1 */ 
      if daysdiff <= p-latedays then  
       do:
          assign gota-hit = yes.
       end.
      else 
        assign gota-hit = no.
   end.
   end. /* poel > today */
  if poeh.buyer = "" then  
  do:
   assign blnk_buyer = "". 
   find first icsl where 
              icsl.cono     = g-cono      and 
              icsl.whse     = poeh.whse   and 
              icsl.vendno   = poeh.vendno and 
              icsl.prodline = poel.prodline                
              no-lock no-error.
    if not avail icsl then 
    do:
       find first icsl where 
                  icsl.cono     = g-cono      and 
                  icsl.vendno   = poeh.vendno and 
                  icsl.prodline = poel.prodline
                  no-lock no-error.
    end.
    if not avail icsl then 
    do:
       find first icsl where 
                  icsl.cono     = g-cono  and 
                  icsl.vendno   = poeh.vendno 
                  no-lock no-error.
    end.
    if not avail icsl then 
    do:
       find first icsl where 
                  icsl.cono     = g-cono    and 
                  icsl.whse     = poeh.whse and
                  icsl.prodline = poel.prodline
                  no-lock no-error.
    end.
    if avail icsl then 
       assign blnk_buyer = icsl.buyer.
   end. 
   
   if not p-blnk-buyer then 
      if poeh.buyer = "" and 
         blnk_buyer = "" then 
         assign gota-hit = no.
   else  
   if p-blnk-buyer then 
      if poeh.buyer  = "" then 
         assign gota-hit = yes.
      else 
         assign gota-hit = no.
   
   if gota-hit = yes then 
   do:
      assign idx2  = 1.   
      do idx2 = 1 to 6:
         if entry(idx2,p-valids,p-delim) ne substring(poel.user1,1,1) then 
            assign gota-hit = no.
         else 
            assign gota-hit = yes. 
         if gota-hit = yes then 
            leave.
      end.
   end.

   if poel.nonstockty = "l" then  
      assign gota-hit = no.

  assign ackncd = substring(poel.user1,1,1).

  IF first-of(bts_shipfmno) or 
     first-of(apsv.vendno)  or 
     first-of(bts_buyer)    then
  do:
  if poeh.buyer ne "" and v-oimspph = "" and v-oimspfax = "" then 
  do: 
    find sasta WHERE 
         sasta.cono     = g-cono and 
         sasta.codeiden = "b"    and 
         sasta.codeval  = poeh.buyer
         no-lock no-error.         
    if avail sasta then 
       assign v-buyernm = sasta.descrip.
    else 
       assign v-buyernm = "". 
    find oimsp where oimsp.person = poeh.buyer
         no-lock no-error.
    if avail oimsp then 
       assign v-oimspph  = oimsp.phoneno
              v-oimspfax = oimsp.faxphoneno.
  end.
  else
  if poeh.buyer = "" and v-oimspph = "" and v-oimspfax = "" then 
  do:
   find first icsl where 
              icsl.cono     = g-cono      and 
              icsl.whse     = poeh.whse   and 
              icsl.vendno   = poeh.vendno and 
              icsl.prodline = poel.prodline                
              no-lock no-error.
    if not avail icsl then 
    do:
       find first icsl where 
                  icsl.cono     = g-cono      and 
                  icsl.vendno   = poeh.vendno and 
                  icsl.prodline = poel.prodline
                  no-lock no-error.
    end.
    if not avail icsl then 
    do:
       find first icsl where 
                  icsl.cono     = g-cono  and 
                  icsl.vendno   = poeh.vendno 
                  no-lock no-error.
    end.
    if not avail icsl then 
    do:
       find first icsl where 
                  icsl.cono     = g-cono    and 
                  icsl.whse     = poeh.whse and
                  icsl.prodline = poel.prodline
                  no-lock no-error.
    end.
    if avail icsl then 
       assign blnk_buyer = icsl.buyer.
    else 
       assign blnk_buyer = "".        
    find sasta WHERE 
         sasta.cono     = g-cono and 
         sasta.codeiden = "b"    and 
         sasta.codeval  = blnk_buyer
         no-lock no-error.         
    if avail sasta then 
       assign v-buyernm = sasta.descrip.
    else 
       assign v-buyernm = "". 
    find oimsp where oimsp.person = blnk_buyer
         no-lock no-error.
    if avail oimsp then 
       assign v-oimspph  = oimsp.phoneno
              v-oimspfax = oimsp.faxphoneno.
  end. /* poeh.buyer */
  end. 
 
  IF first-of(bts_shipfmno) or 
     first-of(apsv.vendno)  or 
     first-of(bts_buyer)    then
   do:
       if substring(apsv.faxphoneno,1,1) = "1" then 
          assign v-faxphone =  if poeh.shipfmno ne 0 then
                                  substring(apss.faxphoneno,2,14)
                               else 
                                  substring(apsv.faxphoneno,2,14)
                 v-fromnm   = if poeh.shipfmno ne 0 then 
                                  apss.name 
                              else 
                                  apsv.name
                 v-expdnm   = if poeh.shipfmno ne 0 then 
                                  apss.expednm 
                              else 
                                  apsv.expednm.
       else   
          assign v-faxphone =  if poeh.shipfmno ne 0 then
                                  apss.faxphoneno 
                               else    
                                  apsv.faxphoneno
                 v-fromnm   = if poeh.shipfmno ne 0 then 
                                  apss.name 
                              else 
                                  apsv.name
                 v-expdnm   = if poeh.shipfmno ne 0 then 
                                  apss.expednm 
                              else 
                                  apsv.expednm.

        /*  Randomize disk file name  */
        nameloop: 
         do while true:
            assign v-rxfax = v-rxfaxdir + string(time) + string(random(1,999))
                   s-filename = substring(v-rxfax,10,8).
            if search(v-rxfax) = ? then
             do:
               leave nameloop.
             end.
         end.

      if s-printer ne "vid" or 
          not avail sasp then  
       do:
          output close.  
       end.
      else 
      if s-printer = "vid" then 
          output to value(v-rxfax) paged page-size 46.

      /* Open the fax device for faxing */
      if s-printer ne "vid" or
         not avail sasp then  
      do: 
       if poeh.shipfmno ne 0 and avail apss and apss.epoto = false then 
        do:
            /*tb 17051 11/09/94 rs; Transfer Info to Notification Msg */
         {porzcfax.i
                &extent = "1"
                &file   = "apss."
                &out    = "v-myoutputty"   
                &tag    = "'VEND:' + string(poeh.vendno)"}    
        end.
      else
        do:          
          {porzcfax.i
                &extent = "1"
                &file   = "apsv."
                &out    = "v-myoutputty"   
                &tag    = "'VEND:' + string(poeh.vendno)"
                &comvar = "/*"}
        end.   
      end.
    /*si01 begin; Assign RxLaser fax variables (forms don't need headings)*/
       assign  v-headingfl = no
               v-stdfont   = v-esc + "(8U" + v-esc + "(s0p16.67h8.5v0s0b0T" +
                             v-esc + "&k9.5h" + v-esc + "&l8C"
               v-startpos  = v-esc + "&a0h0V".
    end. /**** end of do for first-of apsv ******/

    FORM HEADER
      skip (3)
      v-14size at 1 
      space(30) "        Purchase Order Expedite Report" 
      space(29)
"             The following PO's show as being past due to last acknowledged " 
    skip  
      space(14)
   "ship date. Please provide updated shipping date(s) and reference" 
    skip 
      space(14)
   "vendor sales order number. Thank you."
      v-compress at 1 no-label
     skip(1)
     "TO  :"  at 1 v-fromnm 
           FORMAT "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" SPACE(15)
     "FROM :" v-buyernm
           FORMAT "xxxxxxxxxxxxxxxxxxxxxxxx" SKIP
     "     " v-expdnm
           FORMAT "xxxxxxxxxxxxxxxxxxxx" SPACE(25)
     "PHONE:" v-oimspph sKIP
     "FAX :" string(v-faxphone,"(999)999-9999") 
           FORMAT "xxxxxxxxxxxxxxx" SPACE(30)
     "FAX  :" v-oimspfax format "(999)999-9999"
     "Attn:" at 1 
     skip(1)
      WITH FRAME qbf-header PAGE-TOP COLUMN 1 WIDTH 98
                          NO-ATTR-SPACE NO-LABELS NO-BOX.
   VIEW FRAME qbf-header.                             
   FORM header 
  "Date     Po Number       Vendor         Product                    Qty   Expected  New Ackn'd "  at 1 
  "Placed                  Order Nbr                                        Ship Date Date   Comments"               at 1    
"--------------------------------------------------------------------------------------------------------------------------------"  at 1
   WITH FRAME qbf-header2 PAGE-TOP COLUMN 1 WIDTH 140
                          NO-ATTR-SPACE NO-LABELS NO-BOX.
   VIEW FRAME qbf-header2.                             
   FORM HEADER
     SPACE(42) TODAY FORMAT "99/99/99" SKIP
     SPACE(42) STRING(qbf-t,"HH:MM:SS") FORMAT "xxxxxxxx" SKIP
     WITH FRAME qbf-footer PAGE-BOTTOM COLUMN 1 WIDTH 98
           NO-ATTR-SPACE NO-LABELS NO-BOX.
   VIEW FRAME qbf-footer.
   FORM
    poeh.enterdt 
    m-ponbr         FORMAT "xxxxxxxxxx" VIEW-AS FILL-IN
    ackncd          format "x"
    vndrord         format "x(15)"
    poel.shipprod   FORMAT "x(24)"      VIEW-AS FILL-IN
    poel.qtyord     FORMAT "zzzzz9.99"  VIEW-AS FILL-IN
    expship-dtl     format "x(9)" 
    newackn-dt      format "xx/xx/xx"
    comment-ws 
    skip(1)
    WITH NO-ATTR-SPACE COLUMN 1 WIDTH 150 NO-BOX NO-VALIDATE no-labels.
    if gota-hit = yes then 
       display m-ponbr   no-label
               ackncd
               vndrord
               poel.shipprod
               expship-dtl
               poeh.enterdt    
               poel.qtyord
               newackn-dt.

    if last-of(apsv.vendno)  or 
       last-of(bts_shipfmno) or
       last-of(bts_buyer)  then
    do:
       assign v-oimspph  = ""
              v-oimspfax = "".

     /* Close the fax device */
     /* 
      if not ((sasp.pcommand begins "fx") or (sasp.pcommand begins "vfx")) then
         put unformatted "^T".       /*si02 - added vfx*/
     */ 

      output close.

     /* NOTE: following commented-out code is handy for debugging */
/*    
      output to /usr/tmp/faxmsg.
      message "rxfaxcmd: "  v-rxfaxcmd.  v-rxfax pause..
      output close.  
*/    

     /*NOTE: change ***CLIENT*** in following line to client acronym*/
      v-ovlyfile =
        if search("/si/rxfiles/fax/sdi/77.fax") <> ? then 
                   "/si/rxfiles/fax/sdi/77.fax"
        else
          "/custom/rxforms/sdi/fh9.fax".

     /* to use portrait uncomment this line     
      run rxfaxmrg.p(v-rxfax,v-ovlyfile,v-port,v-stdfont,v-startpos).
     */

      run rxfaxmrg.p(v-rxfax,v-ovlyfile,v-port,v-stdfont,v-startpos).  
/********************************************************************/

      if s-type = "p" then 
       do:
          if not avail sasp then 
          do:
             unix silent value("cat " + v-rxfax + " >> " + v-tempprt).
          end.
          else 
          if s-printer ne "vid" then
             unix silent value(v-tempprt + " " + v-rxfax).
       end.
      else   
         unix silent value(v-rxfaxcmd) value(v-rxfax).

      if s-printer ne "vid" then 
         v-rxfaxcmd = "rm " + v-rxfax.   
/* comment out line below and print file to printer to test page-size etc.. */
      if s-printer ne "vid" or 
          not avail sasp then  
          unix silent value(v-rxfaxcmd).      
      if search(v-memofile) <> ? then                                 
         unix silent rm value(v-memofile).                           
      if s-printer = "vid" then 
       do: 
         run porzc_vid.p.
       end.
    end. /* last of */
 end. /* end of for each loop here */
end. /* scoped to do for */

procedure build_newtmp:
for each t-porzc no-lock:
  {w-poeh.i t-porzc.pono t-porzc.posuf no-lock "b-"}
  if avail b-poeh then 
  do:   
     find bts where bts_cono      = b-poeh.cono     and
                    bts_buyer     = b-poeh.buyer    and
                    bts_vendno    = b-poeh.vendno   and
                    bts_shipfmno  = b-poeh.shipfmno and 
                    bts_pono      = b-poeh.pono     and
                    bts_lineno    = t-porzc.lineno  and 
                    bts_ordentdt  = b-poeh.enterdt
                    no-error.
     if avail bts then next.
     create bts.
     assign bts_cono       = b-poeh.cono 
            bts_buyer      = b-poeh.buyer
            bts_vendno     = b-poeh.vendno
            bts_shipfmno   = b-poeh.shipfmno
            bts_pono       = b-poeh.pono
            bts_lineno     = t-porzc.lineno
            bts_posuf      = b-poeh.posuf 
            bts_stagecd    = b-poeh.stagecd
            bts_ordaltno   = b-poeh.orderaltno
            bts_ordaltsuf  = b-poeh.orderaltsuf
            bts_ordentdt   = b-poeh.enterdt
            bts_ordduedt   = b-poeh.duedt
            bts_receiptdt  = b-poeh.receiptdt
            bts_totlineamt = b-poeh.totlineamt
            bts_linecnt    = b-poeh.nolineitem
            bts_transdt    = b-poeh.transdt
            bts_transtm    = b-poeh.transtm.
  end. /* if avail poeh */
end.
   
end procedure.   

   
   
