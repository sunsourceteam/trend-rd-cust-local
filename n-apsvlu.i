/*n-apsvlu.i  1.1 01/03/98 */
/*n-apsvlu.i  1.2 7/29/93  */
/*n-apsvlu.i */
/*tb 7191 07/20/92 mwb; added vendno change */
/*tb 10582 05/05/93 jrg; Copied from AR lookup */


if index (v-start,"*") ne 0 then
  do:
  find {1} apsv use-index k-lkup where apsv.cono = g-cono
       {2} and apsv.lookupnm matches v-start
       /{2}* */
   and 
        ((g-ourproc begins "oe" and
           not can-do("`,',|",substring(apsv.lookupnm,1,1))) or
         (not g-ourproc begins "oe"))
       no-lock no-error.
  end.
else
  do:
  find {1} apsv use-index k-lkup where apsv.cono = g-cono
       {2} and apsv.lookupnm >= v-start
       /{2}* */
       and 
        ((g-ourproc begins "oe" and
           not can-do("`,',|",substring(apsv.lookupnm,1,1))) or
         (not g-ourproc begins "oe"))
       no-lock no-error.
  end.


  assign
      s-city         = if avail apsv then apsv.city + ", " + apsv.state
                       else ""
      s-status       = if avail apsv then
                         if not apsv.statustype then "(I)"
                         else ""
                       else "".
