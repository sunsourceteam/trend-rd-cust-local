/* z-poparc.i 1.1 6/12/92 */
/*h*****************************************************************************
  INCLUDE              : z-poparc.i
  DESCRIPTION          : Custom Assignments for Pop-Up Window on Setups
  AUTHOR               : enp
  DATE LAST MODIFIED   : 04/22/92
  CHANGES MADE AND DATE:
*******************************************************************************/
define buffer z-arsc for arsc.


define var v-technology as character format "x(30)" no-undo.
define buffer catmaster for notes.
define buffer slsmx     for notes.

define var j-slsinx as integer no-undo.

/* def   shared var x-checkfocus  as logical no-undo. */


On CTRL-O  anywhere  
do:
  if g-ourproc begins "arss" then
  do:
   run zsdiarsspop.p.
  end.
end.  
   

