/* SX 55*/
/*h*****************************************************************************
  INCLUDE      : u-oeetle.i
  DESCRIPTION  : OE extended line item screen update
  USED ONCE?   : yes
  AUTHOR       : kmw
  DATE WRITTEN : 08/04/89
  CHANGES MADE :
    11/01/91 mms;           Oepricefl change logic
    03/19/92 pap; TB#  4454 Line DO - add security field oetietype to replace
        oeorderfl which controls PO/WT ties by the operator
    03/26/92 mjq; TB#  6222 Line DO - PO/WT Interface
    04/16/92 mjq; TB#  6337 Line DO - allow DO line on S or T order disposition
        NOTE: 06/23/92 Per Roger Linn's phone conversation on 6/19 w/Mark Miller
              of Braas - there will be NO DO Lines on S,T orders
    04/27/92 mjq; TB#  6339 Line DO - authorization window
    06/13/92 dby; TB#  6523 Allow override of BO type on labor product
    06/19/92 kmw; TB#  7053 Add deallocation ability
    06/30/92 mjq; TB#  7122 Line DO - allow changes to/from DO lines on
        BL,FO,ST, or QU orders
    11/04/92 mms; TB#  8566 Rush - Add variables & rearrange updt
    11/19/92 mms; TB#  8566 Rush - add datccost load for rush type icdatcty
    12/03/92 mwb; TB#  4535 Added &sasc for a-oeetlm.i
    12/07/92 ntl; TB#  9041 Canadian Tax Changes (QST)
    02/12/93 kmw; TB#  6296 Lookup to tie to PO/WT
    02/25/93 kmw; TB#  7946 Tie authorization not required
    02/25/93 kmw; TB#  7908 Record usage for WT drops
    02/26/93 kmw; TB#  9700 Authorization message does not stay
    04/07/93 mms; TB# 10027 Remove tie on special and line no longer shows on
        PO/WT rrar
    04/12/93 kmw; TB# 10378 Allow entry of LT for non-stocks
    05/17/93 kmw; TB# 11370 Progress error received w/tie
    06/03/93 kmw; TB#  9740 Don't allow DO line on COD order
    06/29/93 mms; TB# 11855 Rush charge updating each time <CR> is pressed off
        Rush Flag field; authorization window a problem when reducing datccost
    07/07/93 mwb; TB# 11866 Calculate margin based on order totals.
    07/26/93 mwb; TB# 12391 Changed edits on the qtyship for DO lines.  Was
        allowing chg for 'cs' DO lines.
    09/07/93 mms; TB# 12619 Only allow transfer tie on DO COD
    09/23/93 mwb; TB# 12583 Added param for user hook &user_001
    10/20/93 kmw; TB# 13273 JIT date window display
    10/27/93 kmw; TB# 13494 Qty Shipped set to 1 after 0 entered
    04/26/94 tdd; TB# 15472 Added user hook
    06/26/94 dww; TB# 15884 Add user include call to u-oeetle.i
    02/09/95 dww; TB# 17779 Canadian Tax changes per Groupe Laurier CIM and
        Systemetrix.
    03/10/95 kjb; TB# 17422 Added another call to user hook u-oeetle.z99
    03/16/95 mtt; TB# 15386 NS Kit tied to a OE/WT drop line
    04/06/95 tdd; TB# 16337 Error 6181 on non-serial line DO
    04/06/95 tdd; TB# 16632 PO/WT created for BOD kit.  Also made changes due
        to code oversize.
    05/16/95 kjb; TB# 17690 When converting an ST to a DO, do not allow access
        to the botype field.  It should always be a "d" and not be changed.
    06/08/95 dww; TB# 17401 DATC (Product Surcharge) should be calculating on
        QU Orders.  Previously all QU Orders stored a zero in oeel.datccost.
    06/29/95 kjb; TB# 18530 Add user include in u-oeetle.i (&user_005)
    08/15/95 mms; TB# 18699 Code split and general cleanup
    08/21/95 mms; TB# 19124 Added new applhelp.gva include; added g-applhelpty
        logic; removed s-wcono logic that was setting a variable for applhelp
    09/19/95 wpp; TB# 15593 Set Cost vars using the new SASO flag.
    09/22/95 gdf; TB# 18812 7.0 Rebates Enhancement (C1a) Added update of
        s-frzrebty.
    10/18/95 kr;  TB# 19565 Introduce new ARP Type of "M" (VMI)
    04/08/96 kr;  TB#  2103 Create a PO RM from an OE order
    05/23/96 kr;  TB# 21218 Fix user include in u-oeetle.i, &user_canada_when
        added " = * "
    09/03/96 jms; TB# 21254 (17A)Develop Value Add Module
    12/03/96 gp;  TB# 20973 Tie created if BO type is 'n'. Add msg 6470.
        Also added counter sale edit.
    03/06/97 frb; TB# 7241  Special price costing - replace all occurances
             of "&icsp" with "&icss"
    03/07/97 mms; TB#  7241 Spec8.0 Special Price Costing; Renamed a-oeetln.i
        &icsp parameter to &icss.
    04/09/98 bm;  TB# 24800 Canadian tax changes.
    07/21/98 cm;  TB# 21056 Error takes user to wrong field
    10/09/98 gfk; TB# 11961 9.0 OE Shipped Quantity Override Security.
        Added v-oeqtyshipty logic to shipped quantity
    01/26/99 lbr; TB# 24786 Added checks for non-stock BOD kits
    03/12/99 bm;  TB# 25975; Do not allow change to rushfl on bo change.
    05/10/99 sbr; TB# 26119 F7 extend - usage change to yes on do
    06/30/99 lbr; TB# 4588  Added security for in/out sales rep change
    09/15/99 ama; TB# e2761 Tally Project - Need to be able to handle new
        icsp.kittype of "M"; Added M where kittype referenced
    10/15/99 ama; TB# e2761 Tally Project - Changed err#5615 to be for only
        kittype b again and added kittype m for err#7003
    10/19/99 ama; TB# e2761 Tally Project - fix logic for change on 10/15/99
    02/22/00 kjb; TB# 27154 Do not allow change to RUSH if a backorder exists
    03/07/00 bm;  TB# 27105 Roll GUI authorization system to character
    03/15/00 kjb; TB# 27154 Code Review change
    04/26/00 des; TB# e5061 Inventory Allocation Project.
    06/16/00 des; TB# e5061 Inventory Allocation - Do not delay lines when
        qtyship is overridden
    07/28/00 des; TB# e5061 Inventory Allocation - Add actual call to ICEAR
    07/29/00 mms; TB# e5662 WT BOD Project - Allow "t" type BOD kit to be
        tied to a workorder or a transfer.
    08/02/00 des; TB# e5061 Inventory Allocation - Remove reallocate logic from
        inside editing loop.
    09/14/00 mms; TB# e6508 WT BOD Project - Added v-orderaltnofl and
        v-ordertypefl as well as v-bodtransferty logic.
    02/12/01 lcg; TB# e7852 AR Multicurrency changes
    03/14/01 lcg; TB# 8198 Remove Multicurrency test
    09/24/01 doc; TB# e10623; Allow entry of VA quote on OE quote
    11/20/01 doc; TB# e10523; Fix order type required for quote lines; added
        v-rushfl
    01/30/02 doc; TB# e11964; Allow direct tie to po/wt for quote order lines.
    04/08/02 gwk; TB# e12023 User hooks
*******************************************************************************/
/*d Update of extended screen fields */
/*tb 21056 07/21/98 cm; Error takes user to wrong field. Do not force user to
    usage field when an error has occured. */
if v-powtintfl and s-botype = "p" and v-errno = 0 then
    next-prompt s-usagefl with frame f-oeetle.
if s-kitfl then
  put screen row 17 column 65 color messages "F9-Kit".
/*tb 15593 04/05/95 wpp; Set the OEAO Cost & Override vars from SASO. */
assign
    v-updtcstfl    = if v-oecostoverty = "a"
                         then yes
                     else if (can-do("b,d",v-oecostoverty) and
                         can-do("d,p",s-botype)) then yes
                     else if (can-do("b,s",v-oecostoverty) and
                         s-specnstype = "s") then yes
                     else no
    /*tb e7852 07/28/00 lcg; A/R Multicurrency apply Exchange rate & label */
    v-cost         = if v-updtcstfl then "Cost:"
                     /*tb 8198 03/14/01 lcg; remove test . . .
                        if v-exchrate ne 1
                            then "Domestic Cost:"
                            else "         Cost:" . . . */
                     else v-cost
    v-savecost     = if not g-seecostfl and v-updtcstfl and
                         not v-costoverfl and not v-resetfl
                             then s-prodcost
                     else 0
    s-prodcost     = if not g-seecostfl and v-updtcstfl and
                         not v-costoverfl and not v-resetfl
                             then 0
                     else s-prodcost
/* SX 55 */
    v-botypefl     = if s-returnfl = no                         and
                        not can-do("s,t",v-orderdisp)           and
                        s-botype  <> "p"                        and
                        g-oetype  <> "do"                       and
                        (s-botype  <> "d"                       or
                         (can-do("bl,fo,qu,st",g-oetype)        and
                        s-stordty  = no)) then yes
                                           else no
    v-datccostfl   = if can-do("so,do,br,cs,qu",g-oetype)      and
                        v-icdatclabel <> ""                    and
                        s-returnfl     = no then yes
                                            else no
             
    v-nontaxtypefl = if v-taxablety <> "n" then yes
                                           else no
    
/* SX 55 */    
    
    v-orderaltnofl = if ((can-do("so,br,cs,do,qu",g-oetype)     and
                          not can-do("p,d",s-botype))            or
                          (can-do("p,d",s-botype)               and
                           (v-maint-l    = "a"                   or
                            s-orderaltno = 0)                   and
                           not can-do("bl,fo,qu,st",g-oetype))) and
                         v-bodtransferty <> "t" then yes
                     else no
    v-ordertypefl  = if ((can-do("so,br,cs,qu",g-oetype)        and
                          not can-do("d,p",s-botype))            or
                         (s-botype     = "d"                    and
                          (v-maint-l    = "a"                    or
                           s-orderaltno = 0)))                  and
                        v-bodtransferty <> "t" then yes
                     else no
/* SX 55 */
    v-prodcostfl   = if v-updtcstfl          and
                        g-oetype     <> "cr" and
                        s-warrexchgfl = no then yes
                                           else no
    v-promisedtfl  = if v-orderdisp = "j" or v-lndtentfl then yes
                      else no
    v-qtyshipfl    = if can-do("br,cs,rm,so",g-oetype)     and
                        s-botype <> "d"                    and
                        v-oeqtyshipty <> "N" then yes
                                             else no
    v-reqshipdtfl  = if v-orderdisp = "j" or v-lndtentfl then yes
                     else no
/* SX 55 */
    
    v-rushfl =  if can-do("so,do,br,cs",g-oetype) and s-returnfl = no    and
                   not (b-oeeh.borelfl and v-maint-l = "c"
                        and g-ordersuf <> 0)                             and
                    not (g-ordersuf = 0 and v-maint-l = "c"
                         and v-bonofl  = yes)                            and
                    not (b-oeeh.borelfl = no and g-oetype = "br"
                         and g-ordersuf <> 0)
                    then yes
                else no
/* SX 55 */
    v-taxablefl    = if v-taxablety <> "n" and g-country <> "ca" then yes
                     else no
    v-taxgroupfl   = if v-taxablety <> "n" then yes
                     else no
    v-termspctfl   = if  v-termslinefl = yes and
                         v-corechgfl   = no  and
                         v-termsoverfl = yes then yes
                                             else no.
/* SX 55 */
/*tb 4588 06/30/99 lbr; Display slsrep in/out in case they are
    not updateable */
display
    v-cost
    s-slsrepin
    s-slsrepout
    s-unitstnd                                 /* SX 55 */
    lblWLPickType when g-oetype = "cs":u and   /* SX 55 */
                       v-wlwhsefl = yes
    s-jitbl.

/*tb e5061 04/26/00 des; Inventory Allocation Project */
update

    {&com}

    s-qtyship
 
        when can-do("br,cs,rm,so",g-oetype) and s-botype <> "d"
             and v-oeqtyshipty <> "N"
        validate(if s-qtyship <= s-qtyord then true
                 else false,
                 "Qty Shipped Has Exceeded Qty Ordered (5644)")

    /{&com}* */

    s-botype
        validate(if (avail b-icsp                               and
                     b-icsp.statustype = "l"                    and
                     can-do("y,n",input s-botype))               or
                    (can-do("y,n,p",input s-botype)              or
                    (input s-botype    = "d"                    and
                     can-do("bl,br,cs,do,fo,qu,so,st",g-oetype) and
                     not can-do("s,t,w",v-orderdisp)))      then true
                 else false,
                 if ((not avail b-icsp                 or
                      b-icsp.statustype <> "l")       and
                      not can-do("s,t,w",v-orderdisp)) then
                      "Must be Y, N, P, or D (3666)"
                 else if can-do("s,t,w",v-orderdisp) and input s-botype = "d"
                  then
      "Ship Complete or Tag and Hold Not Allowed with Direct Order Lines (6193)"
                 else "Must be Y or N (3730)")
        help "(Y)es, (N)o, (P)ickup on Accumulative PO or (D)irect Order"
            when v-botypefl = yes
/* SX 55 */     and       
                s-returnfl = no                                and
                 not can-do("s,t",v-orderdisp)                  and
                 s-botype  <> "p"                               and
                 g-oetype  <> "do"                              and
                (s-botype  <> "d"                                or
                (can-do("bl,fo,qu,st",g-oetype)                 and
                 s-stordty  = no))
/* SX 55 */
    {&com}
    /*tb 21254 09/03/96 jms; Develop Value Add Module - added "f"abrication to
        s-ordertype and to the error messages */
    s-ordertype
        validate(if (can-do("y,n",input s-botype)          and
                     can-do("p,,t,w,f",input s-ordertype)  and
                     g-oetype <> "qu")                     or
                    (s-botype          = "d"               and
                     can-do("p,,t,f",input s-ordertype))   or
                    (s-botype          = "p"               and
                     input s-ordertype = "p"               and
                     g-oetype <> "qu")                     or
                    (can-do("y,n",input s-botype)          and
                     can-do("f,",input s-ordertype)        and
                     g-oetype = "qu")
                 then true
                 else false,
                 if g-oetype = "qu" then
                    "Must be F for QU Order"
                 else if can-do("y,n",input s-botype) then
                     "Must be P, T, W, F or Blank (3744)"
                 else "Must be P, T, F, or Blank for DO Line (3745)")
        help "(P)urch Order, Whse (T)ransfer, (W)ork Order, (F)ab VA or Blank"
            when v-ordertypefl = yes

    s-orderaltno
        when v-orderaltnofl = yes
        {f-help.i}

    /{&com}* */

    /*tb 24800 04/09/98 bm; Canadian tax changes. Only if not canada*/
    s-taxablefl
    /* SX 55
        when v-taxablety <> "n" and g-country <> "ca" */
        when v-taxablefl = yes

    /*u User Hook */
    {u-oeetle.z99 &user_canada_when = "*"}

    s-nontaxtype
        when v-taxablety <> "n"
        validate({v-sasta.i N s-nontaxtype},
                 "Non Tax Code Not Set Up in System Table - SASTT (4021)")

    s-taxgroup
/* SX 55     when v-taxablety <> "n"   */
      when v-taxgroupfl = yes 
      
    /*tb 25975 03/12/99 bm; Do not allow change to rushfl on bo change.*/
    /*tb 27154 02/22/00 kjb; If a backorder already exists for a line, then
        do not allow the Rush flag to be changed. It is alright to display,
        but don't allow update. */
    s-rushfl
        when v-rushfl = yes

    s-datccost
/* SX 55        when can-do("so,do,br,cs,qu",g-oetype) and
               v-icdatclabel <> ""                and
              s-returnfl     = no */
      when v-datccostfl = yes

    s-frzrebty
        when v-frzrebfl = yes

    s-printpricefl

    s-subtotalfl

  {u-oeetle.z99 &user_disp2_end = "*"}

    s-usagefl
        when v-usageokfl

    s-jobno
    /* SX 55 */
    s-wlpicktype
        when g-oetype = "cs":u and v-wlwhsefl = yes
             validate(if can-do("c,,w":u,input s-wlpicktype) then true
                      else false,
           "C)ounter Zone, W)hse Picking, or Blank (Counter and Whse) (6720)")
    /* SX 55 */
    s-prodcat
        validate({v-sasta.i "C" s-prodcat "/*"},
                 "Product Category Not Set Up in System Table - SASTT (4020)")

    /*tb 4588 06/30/99 lbr; Added slsrep in/out security */
    s-slsrepout   when not {c-chan.i} and can-do("o,b",v-oeslsrepfl)
        validate({v-smsn.i s-slsrepout "/*"},
                 "Salesrep Not Set Up in Salesrep Setup - SMSN (4604)")

    s-slsrepin when not {c-chan.i} and can-do("i,b",v-oeslsrepfl)
        validate({v-smsn.i s-slsrepin},
                 "Salesrep Not Set Up in Salesrep Setup - SMSN (4604)")
    s-corecharge
    s-termspct
       /* SX 55  when v-termslinefl = yes and
             v-corechgfl   = no  and
             v-termsoverfl = yes */
         when v-termspctfl = yes             


    /*tb 15593 04/05/95 wpp; Update Cost based on SASO checks. */
    s-prodcost
       /* SX 55 when v-updtcstfl          and
             g-oetype     <> "cr" and
             s-warrexchgfl = no */
       when v-prodcostfl = yes             

    s-leadtm

    v-reqshipdt
/* SX 55         when v-orderdisp = "j" */
        when v-reqshipdtfl

    v-promisedt
/* SX 55         when v-orderdisp = "j"  */
        when v-promisedtfl
/* SX 55 */
    s-discamt
      when v-oeextlfl and v-oepricefl = "e"
    s-disctype
      when v-oeextlfl and v-oepricefl = "e"
/* SX 55 */                        

  {u-oeetle.z99 &user_disp_end = "*"}


go-on (f6 f8 f10 f9) editing:

    readkey.

    /*u User Hook */
    {oeetleus.z99 &user_readkey = "*"}

    /*d Block F6 Vendor/Whse interface when the order is a blanket, future,
        quote or standing order and the line is not a drop ship; also, block
        F6 screen when the product is a BOD kit */
    if {k-func6.i} then
    do:

        if can-do("bl,fo,qu,st",g-oetype) and s-botype <> "d" then
        do:

            bell.
            next.

        end. /* end of if can-do(g-oetype) and .... */

        /*d BOD kit not allowed on PO/WT/WO */
        if s-kitfl and v-bodtransferty <> "t":u then
        do:

            run err.p(5615).
            next-prompt s-botype.
            next.

        end. /* end of if s-kitfl */

        /*tb 20973 12/03/96 gp; Tie created if BO type is 'n' */
        /*e If back orders are not created and the order is a counter sale
            or has a ship complete/tag&hold disposition, the sales line cannot
            be tied */
        if (g-oetype = "cs" or
            not can-do("s,t",b-oeeh.orderdisp)) and
            s-returnfl = no                     and
            (not avail b-icsp or b-icsp.statustype <> "s":u) and
            {u-oeetle.z99 &user_first_err_6470 = "*"}
            input s-botype = "n" then
        do:
            run err.p(6470).
            next-prompt s-botype.
            next.
        end. /* end if g-oetype = cs ... */

    end. /* end of if k-func6 */

    /*d This field is used in applhelp to determine which lookup to run */
    if frame-field = "s-orderaltno" then g-applhelpty = input s-ordertype.

    /*d Block F10 deallocation under the following conditions:
        the order is not an SO, counter sale, or blanker release
        the line is a return line
        the line is a core return
        the line is a nonstock or lost business line
        the product is labor product
        the product is a BOD kit or the product is a Mix - Tally kit
        quantity ship equals quantity ordered
        the function is not being called out of oeetl
        the line is tied to a po, wt, or wo
        the line is a drop ship line */
    if {k-func10.i}                     and
        (not can-do("so,cs,br",g-oetype) or
         s-returnfl                      or
         v-corechgfl                     or
         can-do("n,l",s-specnstype)      or
         b-icsp.statustype = "l"         or
         can-do("b,m",b-icsp.kittype)    or
         s-qtyship         = s-qtyord    or
         g-ourproc        <> "oeetl"     or
         s-ordertype      <> ""          or
         s-botype          = "d")
    then do:

        run err.p(6132).
        next.

    end. /* end of if k-func10.i and .... */

    /*o Edit field entry */
    if ({k-after.i} or {k-func6.i}) or
       ({k-right.i} and
        can-do("s-botype,s-ordertype,s-taxgroup":u,frame-field))
      then do:

        /*d Canadian tax edit */
        if frame-field = "s-taxgroup" and g-country = "ca" then
        do:

            s-taxablefl = if v-taxablety = "y" and input s-taxgroup <> 0 then
                              yes
                          else no.
            display s-taxablefl with frame f-oeetle.

        end. /* end of if frame-field = s-tabablety and .... */

        /*d Tax group change */
        if can-do("y,v",v-taxablety) and
            o-taxgrp <> input s-taxgroup then
        do:

            {v-taxgrp.i v-statecd "input s-taxgroup"}

            o-taxgrp = if confirm = no then ?
                       else input s-taxgroup.

            if not confirm then
            do:

                next-prompt s-taxgroup.
                next.

            end. /* end of if not confirm */

        end. /* end of if can-do(v-taxablety) and .... */

        /*d Pick up/ordertype edits */
        if frame-field = "s-botype" then
        do:


           if input s-botype = "d":u and can-do("s,t,w":u,v-orderdisp)
               then do:
             run err.p(6193).
             next-prompt s-botype.
             next.
            end.
            /*d DO or AC Tie Not Allowed When Serial(s), Lot(s), or Whse Mgr is
                Allocated */
            if can-do("d,p",input s-botype) and
                ({p-kpeab.i
                    &orderno  = g-orderno
                    &ordersuf = g-ordersuf
                    &lineno   = s-lineno
                    &com      = "/*"})

            then do:

                run err.p(6181).
                next-prompt s-botype.
                next.

            end. /* end of if can-do(input s-botype) and .... */

            if input s-botype <> "d" and s-botype = "d" then
            do:

                /*tb 19565 10/20/95 kr; Introduce new ARP Type of "M" (VMI) */
                assign
                    s-botype
                    v-powtintfl = no
                    s-ordertype = ""
                    s-vvendno   = 0
                    s-vshipfmno = 0
                    s-wwhse     = ""
                    s-vendno    = if avail b-icsw then
                                      if can-do("v,m",b-icsw.arptype) then
                                          b-icsw.arpvendno
                                      else 0
                                  else s-vendno
                    s-whse      = if avail b-icsw then
                                      if b-icsw.arptype = "w" then
                                          b-icsw.arpwhse
                                      else ""
                                  else s-whse
                    s-prodline  = if avail b-icsw and
                                      can-do("v,m",b-icsw.arptype) then
                                      b-icsw.prodline
                                  else s-prodline.

                display s-ordertype.
                next.

            end. /* end of input s-botype <> "d" and s-botype = "d" */

            if v-maint-l = "c" then
            do:

                if b-oeel.botype <> "p" and input s-botype = "p" then
                do:

                    run err.p(6023).
                    next-prompt s-botype.
                    next.

                end. /* end of b-oeel.botype <> p and .... */

                if b-oeel.botype <> "d" and input s-botype = "d" and
                    not can-do("bl,fo,qu,st",g-oetype)
                    {&user_001} then
                do:

                    run err.p(6142).
                    next-prompt s-botype.
                    next.

                end. /* end of if b-oeel.botype <> d and .... */

            end. /* end of if v-maint-l = "c" */

            /*d If the operator is not allowed to enter a drop ship,
                check for authorization */
            if not (v-doauth or v-poauth) and
               ((v-oetietype = "n" and can-do("p,d",input s-botype)) or
                (v-oetietype = "t" and input s-botype = "p"))
            then do:
                run err.p(1151).
                /*tb 27105 03/07/00 bm; Roll GUI auth system to char
                    Replaced saauth.p with authchk.p */
                run authchk.p(g-cono,
                              g-operinit,
                              "oeet":U,
                              "ties":U,
                              "type":U,
                              "":U,
                              "":U,
                              output v-poauth).

                if v-poauth = no then do:
                    next-prompt s-botype.
                    next.
                end. /* end of if k-cancel.i */

            end. /* end of auth required */

            /*d Back order type change edits */
            if input s-botype = "d" and s-botype <> "d" then
            do:

                /*d Don't allow bod kits on do orders unless they are
                    a fabricated BOD kit.
                    Don't allow Mix - Tally kits on do orders either */
                if (avail b-icsp and
                    (b-icsp.kittype = "m":u or
                     (b-icsp.kittype = "b":u and v-bodtransferty <> "t":u))) or
                   (s-specnstype = "n" and s-kitfl = yes) then
                do:

                    run err.p(5701).
                    next-prompt s-botype.
                    next.

                end. /* end of if avail b-icsp and .... */

                /*d Assign all fields from input buffer; use include to assign
                    as some fields are lost when a "d" is entered */
                {a-oeetle.i}

                assign
                    s-qtyship = 0
                    s-usagefl = no.

                /*u User Hook */
                {oeetleus.z99 &user_005 = "*"}

                display s-qtyship s-usagefl.
                next-prompt s-ordertype.
                if not {k-func6.i} then next main.

            end. /* end of if input s-botype = "d" and s-botype <> "d" */

            /*tb# 24786 add checks for non-stock BOD kits */
            /* o BOD kits can not have tied po's. BOD kits can only be tied
               to do's when they are fabricated kits. */
            if s-kitfl and (input s-botype = "p":u or
                            (input s-botype = "d":u and
                             v-bodtransferty <> "t":u)) then
            do:
                if input s-botype = "d" then
                run err.p(5701).
                else run err.p(5762).
                next-prompt s-botype.
                next.

            end. /* if s-botype = p and s-kitfl yes */

            if v-maint-l = "a" and input s-botype = "p" then
            do:

                /*d Assign all fields from input buffer; use include to assign
                    as some fields are lost when a "d" is entered */
                {a-oeetle.i}

                assign
                    s-ordertype = "p"
                    s-usagefl   = no.

                display
                    s-ordertype
                    s-usagefl.
                next-prompt s-orderaltno.
                next main.

            end. /* end of if v-maint-l = a and .... */

        end. /* end of if frame-field = "s-botype" */

        /*d Order type fields edits */
        if frame-field = "s-ordertype" then
        do:

            /*u User Hook */
            {u-oeetle.z99 &user_ordtype = "*"}

            /*tb 2103 04/08/96 kr; Create a PO RM from an OE order, added the
                if s-returnty block to block from creating a PO RM from the
                F7-Extend without it being a vendor return type */
            if s-returnfl = yes and s-returnty <> "v" then
            do:

                run err.p(6071).
                next-prompt s-ordertype.
                next.

            end. /* end of if s-returnty <> v */

            if v-maint-l = "a" and input s-ordertype <> s-ordertype and
               (v-powtintfl = yes or s-orderaltno = 0) then
            do:

                assign
                    s-orderaltno = 0
                    v-powtintfl  = no.
                display s-orderaltno with frame f-oeetle.

            end. /* end of if v-maint-l = a and .... */

            /*d BOD kit not allowed on PO/WT/WO unless it's a fabricated
                BOD*/
            /*tb 24786 Added checks for non-stock BOD kits */

            if (avail b-icsp and b-icsp.kittype = "b":u and
                 ((b-icsp.bodtransferty = "" and input s-ordertype <> "") or
                  (b-icsp.bodtransferty = "t":u and
                   not can-do(",w,t":u,input s-ordertype)))) or
                (s-specnstype = "n" and s-kitfl = yes and
                 input s-ordertype <> "") then do:

                run err.p(5615).
                next-prompt s-ordertype.
                next.

            end. /* end of if avail b-icsp and .... */

            /*d Add kittype M for Mix - Tally product also */
            if (avail b-icsp and b-icsp.kittype = "m"       and
                    input s-ordertype <> "")                 or

               (avail b-icsp and b-icsp.memomixfl and input s-ordertype <> "")
            then do:

                run err.p(7003).
                next-prompt s-ordertype.
                next.

            end. /* end of if avail b-icsp and .... */


            /*tb 20973 12/03/96 gp; Tie created if BO type is 'n' */
            /*e If back orders are not created and the order is a counter sale
                or has a ship complete/tag&hold disposition, the sales line                 cannot be tied */
            if (g-oetype = "cs" or
                not can-do("s,t",b-oeeh.orderdisp)) and
                input s-botype = "n"                and
                s-returnfl = no                     and
                {u-oeetle.z99 &user_2nd_err_6470 = "*"}
                can-do("t,p,w,f",input s-ordertype) then
            do:
                run err.p(6470).
                next-prompt s-ordertype.
                next.
            end. /* end if g-oetype = cs ... */

            /*d Operator security prohibits this ordertype; Tie authorization
                not required */
            if not (v-doauth or v-poauth) and
               ((v-oetietype = "p" and input s-ordertype = "t") or
                (v-oetietype = "t" and input s-ordertype = "p") or
                (v-oetietype = "n" and can-do("t,p",input s-ordertype)))
            then do:
                run err.p(1152).
                /*tb 27105 03/07/00 bm; Roll GUI auth system to char
                    Replaced saauth.p with authchk.p */
                run authchk.p(g-cono,
                              g-operinit,
                              "oeet":U,
                              "ties":U,
                              "type":U,
                              "":U,
                              "":U,
                              output v-poauth).
                if v-poauth = no then do:
                    next-prompt s-ordertype.
                    next.
                end.
            end. /* end of authorization required */

            /*d On an ordertype change record usage for WT drops */
            if input s-ordertype <> o-ordertype then
            do:

                if input s-ordertype <> "" then do:
                    {oeetlbo.led &b        = "b3-"
                                 &orderno  = g-orderno
                                 &ordersuf = g-ordersuf
                                 &lineno   = s-lineno}
                end.
               if input s-botype = "d" then
                do:

                    /*tb 26119 05/10/99 sbr; Replaced "b-icsw.statustype = "s""

                        with "can-do("s,o",b-icsw.statustype)" in assign. */
                    assign
                        s-usagefl = if input s-ordertype = "t" then yes
                                    else if input s-ordertype = "p" and
                                        can-do("l,n",s-specnstype) then no
                                    else if input s-ordertype = "p" and
                                        can-do("s,o",b-icsw.statustype) then no
                                    else yes.
                    display s-usagefl.

                end. /* end of if input s-botype = d */

                /*d Reset v-powtintfl as it is causing problems when doing the
                    update. The prodline is getting removed on specials and then
                    the lines will not show on appropriate rrar's */
                assign
                    v-powtintfl = if input s-ordertype = "" then no
                                  else v-powtintfl
                    o-ordertype = input s-ordertype.

            end. /* end of if ordertype change */

            /*tb 21254 09/03/96 jms; Develop Value Add Module */
            /*e check if va module has been purchased */
            if input s-ordertype = "f" then do:

                {w-sasa.i no-lock}
                if sasa.modvafl = no then do:
                    run err.p(1124).
                    next-prompt s-ordertype.
                    next.

                end. /*if sasa.modvafl = no */

            end. /* if s-ordertype = "f" */

        end. /* end of if frame-field = "s-ordertype" */

        if not v-powtintfl                         and
              (s-botype           = "d"             or
              (can-do("p,t",input s-ordertype)     and
               input s-orderaltno = 0))            and
             ((frame-field        = "s-orderaltno" and
               can-do("br,cs,do,so,rm",g-oetype))   or
              (frame-field        = "s-ordertype"  and
               can-do("bl,fo,qu,st",g-oetype))) then leave.

        if can-do("bl,fo,qu,st",g-oetype) and
            v-maint-l          = "c"      and
            s-botype           = "d"      and
            input s-ordertype <> s-ordertype
        then do:

            assign
                s-ordertype
                v-powtintfl = no.
            leave.

        end. /* end of if can-do(g-oetype) */

        if  v-maint-l           = "a"            and
            frame-field         = "s-orderaltno" and
            input s-orderaltno <> s-orderaltno  then
            assign
                v-powtintfl = no
                v-powtnew   = no.

        /*o Qty ship changed */
        if input s-qtyship <> o-chgqtyship then
        do:
            /*tb11961 If sasoo.oeqtyshipty = "D" then only allow a
              decrease in quantity shipped*/
            if v-oeqtyshipty = "D" and input s-qtyship > oc-qtyship then
            do:
                run err.p(1174).
                next-prompt s-qtyship.
                next.
            end.

            assign
                s-qtyship
                o-qtyship      = s-qtyship
                o-chgqtyship   = s-qtyship
                lShpQtyOverFl  = true
                v-delayresrvfl = false
                {a-oeetln.i
                    &oeel      = "s-"
                    &oeeh      = "b-oeeh."
                    &icss      = "v-"
                    &disc      = "s-discamt"
                    &useqty    = "n"
                    &qty       = "s-qtyship"}
                s-netord       = s-netamt
                s-discpct      = s-discamt.

            /*d Base margin on ordered */
            {&com}
            {a-oeetlm.i
                &oeel    = "s-"
                &oeeh    = "b-oeeh."
                &icss    = "v-"
                &oerebty = "v-oerebty"
                &com     = "/{&blank}*"
                &com2    = "/{&blank}*"
                &sasc    = "/*"}
            /{&com}* */

            /*d Display based on order totals */
            assign
                s-cost   = v-marcost * v-marqty
                o-netamt = (v-marprice * v-marqty) -
                           (v-mardisc  * v-marqty).
                s-netamt = s-netamt * (if s-returnfl then -1 else 1).

            {&com}
            if g-seecostfl = yes then
                display
                    s-cost
                    o-netamt     when v-oepricefl <> "n" @ s-netamt
                    s-marginamt  when v-oepricefl <> "n"
                    s-marginpct  when v-oepricefl <> "n".
            else display o-netamt when v-oepricefl <> "n" @ s-netamt.
            /{&com}* */

        end. /* end of if input s-qtyship <> o-chgqtyship */

        /*d Assign rushfl to prevent multiple increases on each return off rush
            field */
        if  frame-field     = "s-rushfl" and
            input s-rushfl  = yes        and
            input s-rushfl <> s-rushfl  then
        do:

            assign
                s-rushfl
                s-datccost.

            run oeetled.p.

            display s-datccost.

        end. /* end of if frame-field = s-rushfl and .... */

        /*d Authorization required to reduce datccost */
        if  frame-field      = "s-datccost" and
            input s-datccost < s-datccost   and
            v-datcauth = no
        then do:
            run err.p(1156).
            /*tb 27105 03/07/00 bm; Roll GUI auth system to char
                Replaced saauth.p with authchk.p */
            run authchk.p(g-cono,
                          g-operinit,
                          "oeet":U,
                          "extended":U,
                          "surcharge":U,
                          "":U,
                          "":U,
                          output v-datcauth).
            if v-datcauth = no then do:
                next-prompt s-datccost.
                next.
            end. /* end of if input s-datccost < s-datccost and .... */
        end. /* end of if frame-field = s-datccost and .... */
/* SX 55 */
        if v-oeextlfl and
          ( (frame-field = "s-discamt" and input s-discamt <> o-discamt) or
            (frame-field = "s-disctype" and input s-disctype = yes) )
        then do:
          assign
            s-discamt
            s-disctype
            v-manprice    = yes
            s-priceorigcd = "O"
            v-discamt     = if s-disctype = yes then
                              if can-do("n,l",s-specnstype) then
                                s-discamt
                              else if v-speccostty = ""
                              then s-discamt / v-conv
                              else s-discamt
                            else v-discamt
             v-idpdsc      = ?
             v-pdrecno     = 0
             s-priceclty   = ""
             s-pricecd     = 0
             s-pricelevel  = 0
             s-disccd      = 0
             v-qtytype     = ""
             s-qtybreakty  = ""
             o-discamt     = s-discamt
             s-kitrollty   = if s-kitrollty = "b" then "c"
                             else if s-kitrollty = "p" then ""
                             else s-kitrollty
             s-commtype    = if s-specnstype = "n" then s-commtype
                             else "".
         {&com}
         if  s-returnfl     = no   and
             s-kitfl        = no   and
             v-oevrebcalcty = "e"  and
             not can-find(first swoeel use-index k-swoeel  where
                                swoeel.cono       = g-cono     and
                                swoeel.orderno    = g-orderno  and
                                swoeel.ordersuf   = g-ordersuf and
                                swoeel.solineno   = s-lineno   and                                              swoeel.warrantycd = "w")
          then do:
            for each pder use-index k-pder where
                     pder.cono     = g-cono           and
                     pder.orderno  = b-oeeh.orderno   and
                     pder.ordersuf = b-oeeh.ordersuf  and
                     pder.lineno   = s-lineno         and
                     pder.rebatecd <> "p"
            exclusive-lock:
                                                        
              v-rebamt = 0.
              /* User Include */
              {oeetleus.z99 &del_pderman = "*"}
              delete pder.
                                                                                        end.  /* for each pder */
             
            run oeetlrb.p(?, ?, ?).
          end. /* if s-rebturnfl = no ... */                
        /{&com}* */
                                        
     end.  /*  discount  */
                                                
/* SX 55 */
        /*tb 15593 04/05/95 wpp; Set the OEAO Cost & Override vars from SASO. */
        if not g-seecostfl and v-updtcstfl and s-prodcost <> 0 and
            frame-field = "s-prodcost" then
                v-resetfl = yes.
        if not g-seecostfl and v-updtcstfl and s-prodcost = 0 then
            s-prodcost = v-savecost.

    end. /* end of if {k-after.i} or {k-func6.i} */

    /*u User Hook */
    {oeetleus.z99 &user_editing = "*"}

    apply lastkey.

end. /* end of editing loop */



