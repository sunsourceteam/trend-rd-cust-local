/* hhep.sch */

/* poehb.sch */
if v-bufferedsearch then do:
  /**
  assign sr-prod  = if substring(v-start,1,2) = "P%" then
                      substring(v-start,3,(length(v-start) - 2))
                    else
                      v-start.
  **/
  /**
  find next t-put where t-put.whse = v-whse
  and t-put.receiptid = v-receiptid and t-put.prod begins v-shipprod
  no-lock no-error.
  if not avail t-put then
  find first t-put where t-put.whse = v-whse
  and t-put.receiptid = v-receiptid and t-put.prod begins v-shipprod
  no-lock no-error.
   
  if avail t-put then do:
    v-shipprod = t-put.prod.
  end.
  else find first t-put.
  **/
  
  find next tsum-put where tsum-put.prod begins v-shipprod no-lock no-error.
  if not avail tsum-put then
    find first tsum-put where tsum-put.prod begins v-shipprod no-lock no-error.
  if avail tsum-put then v-shipprod = tsum-put.prod.
  else find first tsum-put.
  input clear.
  pause 0.
  v-bufferedsearch = no.
end.