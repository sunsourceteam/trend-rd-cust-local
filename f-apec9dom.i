
form    /* Domestic Check Form */
   skip (7)
   p-checkdt               at 28
   p-checkno               at 42
   p-checkamt              at 59
   skip(1)
   v-textamt               at 9
   v-textamt2              at 9
   skip(1)
   v-addr[1]               at 8
   v-addr[2]               at 8
   v-addr[3]               at 8
   v-addr[4]               at 8
   skip (2)
with frame f-checkdom no-labels width 80 no-box.