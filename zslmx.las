/* zslmx.las 1.1 05/16/00 */
/*h****************************************************************************
  INCLUDE      : zslmx.las
  DESCRIPTION  : Kit Production Schedule Setup Sequence # Screen
                 Assign Include
  USED ONCE?   : yes 
  AUTHOR       : sd
  DATE WRITTEN : 05/16/00
  CHANGES MADE :
******************************************************************************/

assign
     s-itemnbr       = {&buf}sled.prod
     s-desc          = {&buf}sled.descrip[1]
     s-price         = {&buf}sled.listprice 
     s-cost          = {&buf}sled.replcost   
     s-prctype       = {&buf}sled.pricetype.
         
