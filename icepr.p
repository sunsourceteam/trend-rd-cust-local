/* icepr.p 10/30/97 */
/*si**** Custom ****************************************************************
  CLIENT  : 80281 SDI
  JOB     : SDI028
  AUTHOR  : jsb
  DATE    : 10/30/97
  VERSION : 8.0.000
  PURPOSE : Added amount difference and product line subtotal
  CHANGES :
    SI               ; Custom changes
    SI01 10/30/97 jsb; original code
    
    SI02 12/17/97 daoc; SDI027; Trend V8 upgrade
                        Reapplied custom changes to current standard V8 
                        program
 ******************************************************************************/

/* icepr.p 1.11 11/29/95 */
/*h*****************************************************************************
  PROCEDURE      : icepr
  DESCRIPTION    : IC Physical Count Sheet & Cycle Count Sheet Recon Report
  AUTHOR         : enp
  DATE WRITTEN   : 07/19/91
  CHANGES MADE   :
    03/04/93 enp; TB#  6354 ICSP Special Costing Products Value
    05/04/93 enp; TB#  8550 Discrepancy between ICEPR and ICEPU
    05/14/93 rs;  TB# 11276 Always uses average cost for valuation
    10/28/93 kmw; TB# 11758 Improve Unit Conv Precision
    04/06/95 dww; TB# 15655 Quantity Difference Display Problem. Put local
        variables into icepr.lva and forms into icepr.lfo
    11/29/95 dww; TB# 15655 Quantity Difference Display Problem
    04/01/96 tdd; TB# 20270 WMAO WM in Use not considered
    09/13/96 bmf; TB# 21702 Trend 8.0 oper2 enhancement - removed
        references to oper2 in the code
    02/19/97 jkp; TB#  7241 (7.2) Spec8.0 Special Price Costing.  Added find
        of icss record.
    09/30/98 dbf; TB# IT001 add total to the variance dollars column
*******************************************************************************/

{p-rptbeg.i}
{g-ic.i}
{g-icepr.i new}
{speccost.gva}
{icepr.lva}

def var v-diffamt   as dec format "zzzz9.99-" no-undo.                    /*SI*/
def var v-diffamttot as dec format "zzzz9.99-" no-undo.
def var v-tickstrz  as character format "x(10)" no-undo.

{icepr.lfo}
{f-under.i}

/*o Load parameters from SAPB, SASC, SASA */
/*tb 20270 04/01/96 tdd; WMAO WM in Use not considered */
assign
    p-whse       = sapb.optvalue[1]
    p-runno      = integer(substring(sapb.optvalue[2],1,3))
    p-allprodfl  = if sapb.optvalue[3] = "yes" then true else false
    p-varpct     = integer(substring(sapb.optvalue[4],1,3))
    p-alltickfl  = if sapb.optvalue[5] = "yes" then true else false
    p-wmauditfl  = if sapb.optvalue[6] = "yes" then true else false
    p-serallfl   = if sapb.optvalue[7] = "yes" then true else false
    p-serdetfl   = if sapb.optvalue[8] = "yes" then true else false
    v-wmfl       = if p-wmauditfl = no then no
                   else if avail sasa and sasa.modwmfl = yes and
                       avail sasc and sasc.wmintfl = yes
                       then yes
                   else no
    v-rptcost    = if dec(sapb.optvalue[9]) <> 0 then          /*si01*/
                    dec(sapb.optvalue[9])                      /*si01*/
                   else if avail sasc then sasc.icphyadjam 
                   else 0
    v-costmsg    = if v-rptcost ne 0 then
                      "        (C)  Indicates an Adjustment Above Cost of " +
                      string(v-rptcost,"$>>>>>>9.99") else ""
    /* SX30 Sunsource 
    v-icsmglcost = sasc.icsmglcost.
       SX30 Sunsource */
    v-icglcost = sasc.icglcost.


/************* BEGIN REPORT LOGIC *************/
main:
do on endkey undo, leave:

    /*d Remove old report records */
    {report.gde}

    /*o********** Perform error checking ****************************/
    if not ({v-icsd.i p-whse "/*"}) then do:
        /*d** Print an error if the whse doesn't exist */
        assign
            v-msg = "Whse " + p-whse + " Does Not Exist"
            v-errfl = true.

        display v-msg with frame f-stat.
        down with frame f-stat.
    end.

    if not can-find(first icsep where
        icsep.cono  = g-cono    and
        icsep.whse  = p-whse    and
        icsep.runno = p-runno)
    then do:
        /*d** Print an error if the run number doesn't exist */
        assign
            v-msg = "Run " + string(p-runno) + " Does Not Exist"
            v-errfl = true.

        display v-msg with frame f-stat.
        down with frame f-stat.
    end.

    /*o If an error exists, do not print the report */
    if v-errfl  then do:
        v-msg = "Report Cannot be Completed.".
        display v-msg with frame f-stat.
        down with frame f-stat.
        leave main.
    end.

    {w-icsd.i p-whse no-lock}

    /*o Read through each count record (ICSEP) for the whse/run # */
    find first icsep use-index k-icsep where
        icsep.cono  = g-cono    and
        icsep.whse  = p-whse    and
        icsep.runno = p-runno
    no-lock no-error.

    assign
        v-phyfl     = if avail icsep then icsep.phyfl else true
        v-phyno     = if v-phyfl then 1 else 2
        v-tickets   = if avail icsd then icsd.icpctickfl[v-phyno] else false
        v-tprint    = if avail icsd then icsd.icpctprtfl[v-phyno] else false
        h-ticket    = if v-tprint   then "Ticket(s)" else "Seq#(s)".

    /*d Section 1 - List by Product/Bin */
    display p-whse p-runno h-ticket with frame f-sec1h.
    display with frame f-under.

    for each icsep use-index k-prod where
        icsep.cono      = g-cono    and
        icsep.whse      = p-whse    and
        icsep.runno     = p-runno
    no-lock:

        {w-icsp.i icsep.prod no-lock}
        {w-icsw.i icsep.prod p-whse no-lock}

        /*tb  7241 (7.2) 02/19/97 jkp; Added find of icss record. */
        {icss.gfi 
            &type        = "last"
            &prod        = icsep.prod
            &lock        = "no"}

        /*tb 15655 04/06/95 dww; Quantity Difference Percent Problem. */
        assign
            v-tcreatefl = false /* Reset Ticket Create Flag */
            v-costfl    = false /* Reset Cost Threshold Flag */
            v-unitconv  = 1     /* Reset Unit Conversion */
            v-createfl  = if icsep.createfl then true else v-createfl
            v-counted   = 0
            v-tickstr   = ""
            v-notick    = 0
            v-diffqty   = 0
            v-longdiffpct = 0
            v-diffpct   = 0
            v-diffamt   = 0                                               /*SI*/
            /*tb 11276 05/14/93 rs; Always uses average cost for valuation */
            v-prodcost    = if v-icglcost = "a" then icsw.avgcost
                            else if v-icglcost = "r" then icsw.replcost
                            else if v-icglcost = "s" then icsw.stndcost
                            else if v-icglcost = "l" then icsw.lastcost
                            else icsw.avgcost.

        /*d*** Total Up Tickets ****/
        for each icset use-index k-prod where
            icset.cono  = g-cono        and
            icset.whse  = p-whse        and
            icset.runno = p-runno       and
            icset.prod  = icsep.prod    and
           (icsep.wmfl  = false or icset.binloc = icsep.binloc)
        no-lock:

            /*tb 7808 09/15/92 mwb; add all sequences together */
            /*tb 8550 05/04/93 enp; copy icepu code to icepr   */
            if icset.entfl = yes or                /* amt based on ent qty */
              (icset.entfl = no and icset.qty = 0) /* amt based on expect */
            then 
                assign
                    v-tcreatefl = if icset.createfl then true else v-tcreatefl
                    v-createfl  = if icset.createfl then true else v-createfl
                    v-counted   = v-counted + 
                                  if icset.entfl = yes or icset.rectype = "2" 
                                    then icset.qty
                                  else icsep.qtyexp
                    v-notick    = v-notick + 1
                    v-tickstr   = v-tickstr +
                                 if length(v-tickstr) + 2 +
                                     length(string(icset.ticketno)) le 21 
                                     then (if length(v-tickstr) = 0 then ""
                                           else ", ") + string(icset.ticketno)
                                 else "...".
        end.
        
        /*d*** Count not equal Expected, # of Tickets Not Zero ****/
        if v-notick ne 0 and v-counted ne icsep.qtyexp then do:

            /*tb 6354 03/04/93 enp; ICSP Special Costing Products Value */
            /*d Handle Counting Unit/Stocking Unit Conversion */
            if avail icsp and icsp.unitcnt ne "" then do:
                v-unit = icsp.unitcnt.

                {p-unit.i  &unitstock  = icsp.unitstock
                           &unitconvfl = icsp.unitconvfl
                           &unit       = icsp.unitcnt
                           &desc       = s-desc
                           &conv       = v-unitconv
                           &confirm    = confirm
                           &prod       = icsp.prod}.
            end.

            else if avail icsp then
                assign
                    v-unit     = icsp.unitstock
                    v-unitconv = 1.

            /*tb 6354 03/04/93 enp; ICSP Special Costing Products Value */
            /*tb 11276 05/14/93 rs; Always uses average cost for valuation */
            /*tb 15655 04/06/95 dww; Quantity Difference Percent Problem.  Now
                using a decimal field with positions to the right of the decimal
                point to calculate the percent. Previously, the field was
                format "zzzz9" and was rounding or truncating the value. */
            /*d Handle Quantity & Cost Differences Calculations */
            assign
                {speccost.gas
                    &com = "/*"}
                v-qtyexp      = icsep.qtyexp
                v-diffqty     = v-counted - icsep.qtyexp
                v-longdiffpct = if (v-counted =  0 and v-qtyexp ne 0) or
                                   (v-counted ne 0 and v-qtyexp =  0) then 100
                                else if v-diffqty = 0 then 0
                                else 100 - ((v-counted / v-qtyexp) * 100)
                v-longdiffpct = v-longdiffpct *
                                    if v-longdiffpct < 0 then -1 else 1
                v-diffpct     = v-longdiffpct
                v-incfl       = if v-counted > v-qtyexp then true else false
                v-costfl      = if avail icsw and
                                   (v-prodcost * {speceach.gco}) *
                                  (v-diffqty * (if v-diffqty < 0 then -1 else 1)
                                   * v-unitconv) ge v-rptcost then true
                                else false
                v-diffamt     = v-diffqty * v-prodcost.                 /*SI*/

            /*e******************* Store for Later Sections ****************/
            if (icsep.serlotty ne "" and v-diffqty ne 0) or
               (icsep.wmfl and v-wmfl and v-diffqty ne 0) or
               (icsep.createfl or v-tcreatefl)
            then l-store: do for report transaction:
                if icsep.createfl = false and v-tcreatefl = false and
                   v-repprod = icsep.prod then leave l-store.

                create report.

                if icsep.prod = v-repprod then 
                    assign
                        v-wm = ""
                        v-sl = "".
                else 
                    assign
                        v-wm = "wm"
                        v-sl = icsep.serlotty.

                /*d Used to Get Only One WM Record in Report per Product */
                /*tb 21702 09/13/96 bmf; Changed assign of sapb.oper2 to "" */
                assign
                    report.cono     = g-cono
                    v-repprod       = icsep.prod
                    report.oper2    = ""
                    report.reportnm = sapb.reportnm
                    report.rid      = recid(icsep)
                    report.c20      = if icsep.wmfl then v-wm else ""
                    report.c24      = v-sl
                    report.outputty = if icsep.createfl or v-tcreatefl
                                      then "c" else ""
                    report.c24-2    = if report.outputty = "c" then
                                        if icsep.createfl then "Product"
                                        else "Entry"
                                      else ""
                    report.c24-3    = icsep.prod
                    report.de9d2s   = v-diffqty
                    report.de9d2s-2 = v-counted
                    v-serfl         = if avail icsw and icsw.serlottype = "s"
                                        then true else v-serfl
                    v-lotfl         = if avail icsw and icsw.serlottype = "l"
                                        then true else v-lotfl
                    v-wm2fl         = if icsep.wmfl then true else v-wm2fl
                    v-createfl      = if report.outputty = "c" then true
                                        else v-createfl.
            end.

        end.

        /*o Display if All selected, or if Over %, or if Over $ */
        if ((v-longdiffpct > p-varpct)      or
            p-allprodfl = yes               or
            (abs(v-diffamt) > v-rptcost)    or     /*si01*/
            (v-rptcost ne 0 and v-costfl = yes)
           )
        then do:

            assign
                s-descrip = if avail icsp then
                                icsp.descrip[1] + " " + icsp.descrip[2]
                            else v-invalid
                v-msg     = if icsep.wmfl = yes then "(WM)"
                            else if v-notick > 1 then "(ME)"
                            else ""
                v-msg2    = if icsep.createfl = yes or v-tcreatefl = yes
                                then "(A)"
                            else ""
                v-msg3    = if v-rptcost ne 0 and v-costfl = yes then "C"
                            else "".
            v-tickstrz = substring (v-tickstr,1,10).
       /*     v-diffamttot = diffamt(total).     */
            display
                icsep.prod
                icsep.binloc
                v-msg
                v-msg2
                icsep.qtyexp
                icsep.unit
                v-msg3
                v-counted    when v-notick  ne 0
                v-diffqty    when v-notick  ne 0
                v-diffpct    when v-diffqty ne 0
                v-incfl      when v-diffqty ne 0
                v-diffamt    when v-diffamt ne 0                       /*SI*/
              /*  v-diffamttot when v-diffamt ne 0  */          /* IT001 */
                v-tickstrz
            with frame f-sec1 width 132.
            down with frame f-sec1.

            display s-descrip with frame f-sec1l2.
        end.

    end.    /* ICSEP Loop for Section 1 */

    display v-costmsg with frame f-legend.

    hide frame f-sec1h no-pause.
    hide frame f-under no-pause.

    /*d********* Serial/Lot Display  *****************/
    if v-serfl = yes or v-lotfl = yes then run iceprsl.p.

    /*d********* WM Audit   *****************/
    if v-wmfl = yes and v-wm2fl = yes then run iceprwm.p.

    /*d********* Ticket Display    *****************/
    if v-tickets then run iceprt.p.

    /*d********* Display manually created records   *****************/
    if v-createfl = yes then do:
        v-msg = "Manually Created Records".
        hide frame f-sec1h no-pause.
        hide frame f-under no-pause.

        page.

        display p-whse p-runno v-msg with frame f-sec2h.
        display with frame f-under.
        /*tb 21702 09/13/96 bmf; Changed assign of sapb.oper2 to "" */
        for each report use-index k-report no-lock where
            report.cono     = g-cono        and
            report.reportnm = sapb.reportnm and
            report.oper2    = ""            and
            report.outputty = "c",

        each icsep where
            recid(icsep) = report.rid
        no-lock:

            display icsep.prod icsep.binloc report.c24-2 with frame f-sec2.
            down with frame f-sec2.
        end.

        display with frame f-legend2.
    end.

    hide frame f-sec1h no-pause.
    hide frame f-under no-pause.

    /*d* Print All Other Reports **/
    {p-icepx.i p-whse p-runno}

    /*d Remove any REPORT Records */
    {report.gde}
end.

hide all no-pause.

/* Copyright 1989-1997 by NxTrend Technology, Inc. */
/* All Rights Reserved. */
