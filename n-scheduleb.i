/*
If any changes to parameters are made, look in the following programs/includes:
g-zsdiicspuser.i
zsdiicscuser.p
zsdiicspuser.p
icsp-tariff2.p
zsditariffprt.p
icrzb.p
n-icizk.i
n-scheduleb.i
n-schedulebc.i
n-schedulebk.i
*/

confirm = true.
find {1} oeel use-index k-oeel where 
         oeel.cono = g-cono and
         oeel.orderno = g-orderno and
         oeel.ordersuf = g-ordersuf
      /* oeel.shipprod = g-prod    */
         no-lock no-error.
         
if not avail oeel then do:
   confirm = false.
end.
   
if avail oeel then do:

      assign var-prod = oeel.shipprod
             var-whse = oeel.whse.
      {w-icsp.i var-prod no-lock}
      {w-icsw.i oeel.shipprod var-whse no-lock}

            if avail icsp then do:
              run zsdiicspuser.p  (INPUT "I", /* Inquiry */
                         INPUT recid(icsp),
                       /* Tariff Parameters */
                        INPUT-OUTPUT  vp-CanadianTariffNbr,  
                        INPUT-OUTPUT  vp-USATariffNbr,   
                        INPUT-OUTPUT  vp-CountryOfOrigin, 
                        INPUT-OUTPUT  vp-MaintDt,  
                        INPUT-OUTPUT  vp-CustomsExpireDt,
                        INPUT-OUTPUT  vp-NAFTAexpiredt,  

                       /* Schedule B Parameter */
                        INPUT-OUTPUT  vp-ScheduleBInfo,  
                        INPUT-OUTPUT  vp-ScheduleBMaintDt, 

                       /* RAF */
                        INPUT-OUTPUT  vp-source,
                        INPUT-OUTPUT  vp-sourcedt,
                    
                       /* MISC Fields */
                        INPUT-OUTPUT vp-SurChargePct,  
                        INPUT-OUTPUT vp-NR-NC,  
                        INPUT-OUTPUT vp-UNSPSCcode,
                        INPUT-OUTPUT vp-flagtr,     
                        INPUT-OUTPUT vp-Protected,  
                        INPUT-OUTPUT vp-gasandoil,  
                        INPUT-OUTPUT vp-core,       
                        INPUT-OUTPUT vp-sensitivity,
                             

                       /* Fabrication Labor Information */
                        INPUT-OUTPUT vp-Labor,              
                        INPUT-OUTPUT vp-Documentation,     
                        INPUT-OUTPUT vp-DrawingFee,      
                        INPUT-OUTPUT vp-ElectricalLabor,    
                        INPUT-OUTPUT vp-MEchanicalLabor).   
                        
               assign var-descrip = icsp.descrip[1].         
 
       end. /* avail icsp */
       /*
       else do:
        /*  message "no icsp". pause.  */
        assign 
         vp-CountryOfOrigin = ""
         vp-CustomsExpireDt = ""
         vp-MaintDt         = ""
         var-descrip        = oeel.proddesc
         vp-CanadianTariffNbr  = ""
         vp-USATariffNbr       = ""
         vp-ScheduleBInfo      = ""
         vp-ScheduleBMaintDt   = ""
        
       /* next. */
       end. 
       */ 
           if avail icsw then do:
             assign var-netamt = oeel.qtyord * icsw.avgcost
                    var-avgcost = icsw.avgcost.
                    
           end.  /* if avail icsw */
           else do:
            /*  message "no icsw avail". pause.  */                
             assign var-netamt = oeel.qtyord * oeel.prodcost
                    var-avgcost = oeel.prodcost.
            /*  next. */
           end.
             
    end. /* avail oeel */
 

