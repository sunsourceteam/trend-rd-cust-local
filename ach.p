/*
def     shared var b-vendortype  as c  format "x(3)"                no-undo.
def     shared var e-vendortype  as c  format "x(3)"                no-undo.
def     shared var b-vendno      as de format "9999999999.99"       no-undo.
def     shared var e-vendno      as de format "9999999999.99"       no-undo.
def     shared var p-jrnlno      as i  format "99999999"            no-undo.
def     shared var o-ACHtype     as c  format "x(1)"                no-undo.
def     shared var p-bankno      like  crsb.bankno                  no-undo.
def     shared var errmsg        as c  format "x(70)"               no-undo.
*/

def     shared var g-operinit    like  sapb.operinit                no-undo.
def     shared var g-cono        like  sapb.cono                    no-undo.

{g-apec.i}

def buffer b-apet for apet.
def buffer c-apet for apet.

/* FILE HEADER RECORD */
def var h-record_type       as i    format "9"        initial 1.
def var h-priority_code     as i    format "99"       initial 01.
def var h-immed_dest        as c    format "x(10)"    initial " 111000025".
/*  06/11/19
def var h-immed_origin      as c    format "x(10)"    initial "3390933202". 
*/
def var h-immed_origin      as c    format "x(10)"    initial "2223827595".
def var h-trans_date        as c    format "x(6)".    /* Must be YYMMDD */
def var h-trans_time        as c    format "x(4)"     initial "".
def var h-file_id_mod       as c    format "x"        initial "A".
def var h-record_size       as i    format "999"      initial 094.
def var h-blocking_factor   as i    format "99"       initial 10.
def var h-format_code       as i    format "9"        initial 1.
def var h-immed_dest_name   as c    format "x(23)"    
                                      initial "BANK OF AMERICA        ".
def var h-immed_origin_name as c    format "x(23)"     
                                      initial "STS OPERATING INC      ".
                                      
def var h-reference_code    as c    format "x(8)"     initial "".

/* COMPANY HEADER RECORD */
def var ch-record_type      as i    format "9"           initial 5.
def var ch-service_class-cd as i    format "999"         initial 220.   
def var ch-co_name          as c    format "x(16)"       
                                      initial "STS OPERATING IN".    
def var ch-co_dis_name      as c    format "x(20)"       initial "".

def var ch-co_id            as c    format "x(10)". 
def var ch-stnd_entry_class as c    format "x(3)".
def var ch-co_entry_disc    as c    format "x(10)"       initial " ECHECKPAY".
def var ch-co_desc_date     as c    format "x(6)"        initial "".
def var ch-eff_date         as c    format "x(6)".       /* Must be YYMMDD */
def var ch-settlement_date  as c    format "x(3)"        initial "".
def var ch-orig_stat_cd     as c    format "x"           initial "1".
def var ch-orig_dfi_id      as i    format "99999999"    initial 11100002. 
def var ch-batch_nbr        as i    format "9999999"     initial 00000001.

/* ENTRY DETAIL RECORD */
def var d-record_type       as i    format "9"        initial 6.
def var d-transaction_cd    as i    format "99". 
def var d-receiving_dfi_id  as c    format "99999999".         /*fix*/
def var d-check_digit       as i    format "9".
def var d-dfi_account_nbr   as c    format "x(17)".
def var d-amount            as i    format "9999999999".
def var d-individual_id_nbr as c    format "x(15)"    initial "".
def var d-addenda_cnt       as i    format "9999".
def var d-employee_name     as c    format "x(16)".
def var d-descret_data      as c    format "x(2)"     initial "".
def var d-addenda_rec_ind   as i    format "9"        initial 1.    
def var d-constant          as i    format "99999999" initial 11100002. 
def var d-trace_number      as i    format "9999999".

def var x-slash             as c format "x(1)"  init "~\".
def var x-ast               as c format "x(1)"  init "*".
def var CTX-segment         as c.
def var CTX-detail          as c    format "x(83)".
def var CTX-line            as c    format "x(94)".     
def var CTX-seqno           as c    format "x(4)".
def temp-table CTX no-undo
/* REMIT (CTX) RECORD */    
  FIELD vendno           like apsv.vendno
  FIELD jrnlno           like apet.jrnlno
  FIELD checkno          like apet.checkno  
  FIELD r-record-type    as c   format "x"        
  FIELD r-addenda-type   as c   format "x(2)"     
  FIELD r-ISA-0          as c   format "x(3)"     
  FIELD r-ISA-1          as c   format "x(2)"     
  FIELD r-ISA-2          as c   format "x(10)"    
  FIELD r-ISA-3          as c   format "x(2)"     
  FIELD r-ISA-4          as c   format "x(10)"        
  FIELD r-ISA-5          as c   format "x(2)"     
  FIELD r-ISA-6          as c   format "x(10)"    
  FIELD r-ISA-7          as c   format "x(2)"     
  FIELD r-ISA-8          as c   format "x(13)"    
  FIELD r-ISA-9          as c   format "x(6)"   
  FIELD r-ISA-10         as c   format "x(4)"    
  FIELD r-ISA-11         as c   format "x(1)"   
  FIELD r-ISA-12         as c   format "x(5)"   
  FIELD r-ISA-13         as c   format "x(9)"   
  FIELD r-ISA-14         as c   format "x(1)"   
  FIELD r-ISA-15         as c   format "x(1)"      
  FIELD r-ISA-16         as c   format "x(1)"   
  FIELD r-GS-0           as c   format "x(2)"   
  FIELD r-GS-1           as c   format "x(2)"   
  FIELD r-GS-2           as c   format "x(9)"   
  FIELD r-GS-3           as c   format "x(13)"  
  FIELD r-GS-4           as c   format "x(8)"   
  FIELD r-GS-5           as c   format "x(6)"   
  FIELD r-GS-6           as c   format "x(9)"   
  FIELD r-GS-7           as c   format "x(1)"   
  FIELD r-GS-8           as c   format "x(6)"   
  FIELD r-ST-0           as c   format "x(1)"   
  FIELD r-ST-1           as c   format "x(3)"   
  FIELD r-ST-2           as c   format "x(9)"   
  FIELD r-BPR-0          as c   format "x(3)"   
  FIELD r-BPR-1          as c   format "x(1)"  
  FIELD r-BPR-2          as c   format "x(9)" 
  FIELD r-BPR-3          as c   format "x(1)"    
  FIELD r-BPR-4          as c   format "x(3)"  
  FIELD r-BPR-5          as c   format "x(3)"  
  FIELD r-BPR-6          as c   format "x(2)"  
  FIELD r-BPR-7          as c   format "x(9)"   
  FIELD r-BPR-8          as c   format "x(2)"   
  FIELD r-BPR-9          as c   format "x(10)"  
  FIELD r-BPR-10         as c   format "x(10)"         
  FIELD r-BPR-11         as c   format "x(1)"   
  FIELD r-BPR-12         as c   format "x(2)"   
  FIELD r-BPR-13         as c   format "x(9)"   
  FIELD r-BPR-14         as c   format "x(2)"   
  FIELD r-BPR-15         as c   format "x(10)"  
  FIELD r-BPR-16         as c   format "x(8)"   
  FIELD r-TRN-0          as c   format "x(3)"   
  FIELD r-TRN-1          as c   format "x(1)"   
  FIELD r-TRN-2          as c   format "x(8)"   
  FIELD r-TRN-3          as c   format "x(9)"     
  FIELD r-CUR-0          as c   format "x(3)"   
  FIELD r-CUR-1          as c   format "x(2)"   
  FIELD r-CUR-2          as c   format "x(3)"   
  FIELD r-CUR-3          as c   format "x(1)"   
  FIELD r-CUR-4          as c   format "x(2)"   
  FIELD r-CUR-5          as c   format "x(3)"   
  FIELD r-REF-0          as c   format "x(3)"   
  FIELD r-REF-1          as c   format "x(2)"   
  FIELD r-REF-2          as c   format "x(8)"   
  FIELD r-REF-3          as c   format "x(7)"       
  FIELD r-N1a-0          as c   format "x(2)"   
  FIELD r-N1a-1          as c   format "x(2)"   
  FIELD r-N1a-2          as c   format "x(30)"  
  FIELD r-N2a-0          as c   format "x(2)"   
  FIELD r-N2a-1          as c   format "x(30)"  
  FIELD r-N3a-0          as c   format "x(2)"   
  FIELD r-N3a-1          as c   format "x(30)"  
  FIELD r-N4a-0          as c   format "x(2)"   
  FIELD r-N4a-1          as c   format "x(30)"  
  FIELD r-N4a-2          as c   format "x(2)"    
  FIELD r-N4a-3          as c   format "x(9)"   
  FIELD r-N1b-0          as c   format "x(2)"   
  FIELD r-N1b-1          as c   format "x(2)"   
  FIELD r-N1b-2          as c   format "x(30)"    
  FIELD r-ENT-0          as c   format "x(3)"   
  FIELD r-ENT-1          as c   format "x(1)"   
  FIELD r-RMR-0   as c   format "x(3)"     extent 99
  FIELD r-RMR-1   as c   format "x(2)"     extent 99
  FIELD r-RMR-2   as c   format "x(20)"     extent 99
  FIELD r-RMR-3   as c   format "x(1)"     extent 99
  FIELD r-RMR-4   as c   format "x(12)"    extent 99
  FIELD r-RMR-5   as c   format "x(12)"    extent 99
  FIELD r-REFe-0  as c   format "x(3)"     extent 99
  FIELD r-REFe-1  as c   format "x(2)"     extent 99
  FIELD r-REFe-2  as c   format "x(20)"     extent 99
  FIELD r-DTM-0   as c   format "x(3)"     extent 99
  FIELD r-DTM-1   as c   format "x(3)"     extent 99
  FIELD r-DTM-2   as c   format "x(8)"     extent 99
  FIELD r-SE-0           as c   format "x(2)"   
  FIELD r-SE-1           as c   format "x(2)"   
  FIELD r-SE-2           as c   format "x(5)"   
  FIELD r-GE-0           as c   format "x(2)"   
  FIELD r-GE-1           as c   format "x(1)"   
  FIELD r-GE-2           as c   format "x(9)"   
  FIELD r-IEA-0          as c   format "x(3)"   
  FIELD r-IEA-1          as c   format "x(1)"   
  FIELD r-IEA-2          as c   format "x(9)"   
  INDEX k-CTX
        vendno
        jrnlno
        checkno.
  
  


def var r-element-delim     as c    format "x(1)"     initial "*".   
def var r-segment-delim     as c    format "x(1)"     initial ""\"".
def var r-pay_info          as c    format "x(80)".
def var r-sequence          as i    format "9999".  
def var r-trace_number      as i    format "9999999".     /* detail-cnt */
/***/



/* COMPANY CONTROL RECORD */
def var cc-record_type      as i    format "9"            initial 8.
def var cc-service_cl_cd    as i    format "999"          initial 220.  
def var cc-entry_add_cnt    as i    format "999999".
def var cc-entry_hash       as de   format "9999999999".          /*fix*/
def var cc-tot_debit_amount as de   format "999999999999"         /*fix*/
                                   initial "000000000000".
def var cc-tot_cred_amount  as de   format "999999999999".        /*fix*/

def var cc-company_id       as c    format "x(10)".
def var cc-auth_code        as c    format "x(19)"        initial "".
def var cc-reserved         as c    format "x(6)"         initial "".
def var cc-orig_dfi_id      as i    format "99999999"     initial 11100002. 
def var cc-batch_nbr        as i    format "9999999"      initial 0000001.

/* FILE CONTROL RECORD */
def var fc-record_type      as i    format "9"            initial 9.
def var fc-batch_cnt        as i    format "999999"       initial 000001.
def var fc-block_cnt        as i    format "999999"       initial 000001.
def var fc-entry_add_cnt    as i    format "99999999".
def var fc-entry_hash       as de   format "9999999999".     /*fix*/
def var fc-tot_file_debit   as de   format "999999999999"    /*fix*/
                                   initial "000000000000".
def var fc-tot_file_cred    as de   format "999999999999".   /*fix*/
def var fc-reserved         as c    format "x(39)"        initial "".

/* WORK FIELDS */
def var detail-cnt          as i    format "9999999".   
def var record-cnt          as i    format "9999999".
def var tot-bal-amt         as i    format "9999999999".
def var check-digit         as c    format "x".
def var receiving-dfi-id    as c    format "x(8)".
def var entry-hash          as c    format "999999999999999".       /*fix*/
/*def var entry-hash          as de   format "999999999999999".*/
def var hash-amt            as c    format "x(15)".
def var trunc-hash          as c    format "x(10)".
def var trans-date          as date format "99/99/99".
def var work-date           as c    format "x(8)".
def var disp-date           as c    format "x(6)".
def var disp-date2          as c    format "x(6)".
def var x-today             as date format "99/99/9999".
def var x-time              as c    format "x(5)".
def var x-page              as i    format ">>>9".
def var x-line              as i    format "999".
def var x-page-size         as i    format "99"           initial 50.
def var x-company           as i    format ">>>9"         initial 1.    
def var ISA-time            as c    format "x(4)".
def var s-grand-amount      as de   format "99999999.99".
def var zz                  as i    format "999".   
def var yy                  as i    format "999".   
def var w-addenda-cnt       as de   format "99999.99".

def temp-table ACHrpt no-undo
  FIELD vendno  like apsv.vendno
  FIELD name    like apsv.name
  FIELD amount  like apet.amount     
  FIELD checkno like apet.checkno   
  FIELD notesfl like apet.notesfl
  FIELD tiedfl  as   c format "x(3)"
  INDEX k-over
        vendno
        amount DESCENDING.
def temp-table remit no-undo
  FIELD vendno    like apsv.vendno
  FIELD checkno   like apet.checkno
  FIELD amount    like apet.amount   
  FIELD apinvno   like apet.apinvno
  FIELD reference like apet.refer
  INDEX k-remit
        vendno
        checkno
        apinvno.



def var x-month             as c    format "x(2)"          no-undo.     
def var x-day               as c    format "x(2)"          no-undo.
def var x-year              as c    format "x(2)"          no-undo. 
def var x-wyear             as c    format "x(4)"          no-undo.
def var x-wtime             as c    format "x(8)"          no-undo. 
def var x-ftime             as c    format "x(6)"          no-undo.
def var x-blank             as c                           no-undo. 
def var x-total             as de   format ">>>>>>>>9.99"  no-undo.

def frame  f-ACHheader 
  "~015"  at 1
  "Date:" at 1
  x-today format "99/99/99" at   7
  "at "                     at  16
  x-time                    at  19
  "~015"
  "Company:"                at   1
  g-cono                    at  10
  g-operinit                at  60
  "Page:"                   at  69
  x-page                    at  74 format ">>>9"
  "~015"                    at  78
  "Function: apec"          at   1
  "ACH Payments"            at  32
  "~015"                    at  78 
  "~015"                    at  78
  "Vendor#"                 at   1
  "Vendor Name"             at  17  
  "Check Amt"               at  50  
  "Check Nbr"               at  63
  "Tied?"                   at  73
  "~015"                    at  78
  "---------------"         at   1
  "------------------------------" at 17
  "-----------"             at  49  
  "-----------"             at  61
  "-----"                   at  73
  "~015"                    at  78
with down stream-io width  80 no-labels no-box no-underline.

define frame f-ACHdetail
   ACHrpt.vendno            at   4
   ACHrpt.name              at  17
   ACHrpt.amount            at  49 format ">>>>>>>9.99-"  
   ACHrpt.checkno           at  61 format ">>>>>>>>>9"      
   ACHrpt.tiedfl            at  74
   ACHrpt.notesfl           at  77
   "~015"                   at  78
with down stream-io width 80 no-labels no-box no-underline.

define frame f-remithdr 
  x-blank                    at   1
  "Invoice"                  at   7
  "Tied PO"                  at  24
  "Amount"                   at  53
  "~015"                     at  78
  "---------------"          at   7
  "------------------------" at  24
  "----------"               at  53
  "~015"                     at  78
with down stream-io width 80 no-labels no-box no-underline.
    
define frame f-remit
   remit.apinvno            at   7
   remit.reference          at  24 
   remit.amount             at  53 format ">>>>>>>9.99-"     
   "~015"                   at  78
with down stream-io width 80 no-labels no-box no-underline.            

define frame f-blank
   x-blank                  at   1  
   "~015"                   at  78
with down stream-io width 80 no-labels no-box no-underline.
define frame f-total
   x-blank                  at   1
   "Total for Journal"      at  17
   p-jrnlno                 at  35
   x-total                  at  48
with down stream-io width 80 no-labels no-box no-underline.
  
/*EMAIL Variables*/
def var cMailSubject  as character format "x(40)"  no-undo.
def var cMailMessage  as character format "x(130)" no-undo.
def var cMailto       as character format "x(40)"  no-undo.
def var cType         as character                 no-undo.
def var iCounter      as integer                   no-undo.
def var currUser      as character format "x(10)"  no-undo.
def var senderAddress as character                 no-undo.
def var v-emailfile   as character                 no-undo.
def var v-rmfile      as character                 no-undo.




assign x-month    = string(month(TODAY),"99").          
assign x-day      = string(day(TODAY),"99").
assign x-wyear    = string(year(TODAY),"9999").
assign x-year     = substr(x-wyear,3,2).   
assign x-wtime    = string(time,"HH:MM:SS").    
assign x-ftime    = substr(x-wtime,1,2) + 
                    substr(x-wtime,4,2) +   
                    substr(x-wtime,7,2).
assign x-time     = string(time,"HH:MM").
assign ISA-time   = substr(x-wtime,1,2) +
                    substr(x-wtime,4,2).






define stream deposit.
define var o_file           as char format "x(40)".
define var o_doctype        as char format "x".

put control "~033~046~1541~117".   /* landscape format */

assign o_file = "/usr/tmp/bank/cdrhydra_NACHA_" + x-month + 
                                                  x-day   +
                                                  x-year  +  "_" +
                                                  x-ftime +
                                                  ".txt".      

define variable destfilename as char        format "x(40)".
define variable outputfilename as char      format "x(40)".
define variable destfilecounter as char     format "x(40)".

output stream deposit to value(o_file).

define variable flsuff  as character.
define variable flsuff2 as character.


substring(flsuff,1,2,"character") =
  substring(string(today),1,2,"character").
substring(flsuff,3,2,"character") = 
  substring(string(today),4,2,"character").

substring (flsuff,5,1,"character") = ".".

substring(flsuff,6,2,"character") =
  substring(string(time,"HH:MM:SS"),1,2,"character").
substring(flsuff,8,2,"character") = 
  substring(string(time,"HH:MM:SS"),4,2,"character").
substring(flsuff,10,2,"character") = 
  substring(string(time,"HH:MM:SS"),7,2,"character").


substring(destfilename,1,16,"character") = "/usr/tmp/ACHback".
substring(destfilename,17,11,"character") = flsuff.
substring(destfilecounter,1,19,"character") = "/usr/tmp/ACHcounter".

/* put date into YYMMDD format */
assign x-today    = TODAY.
assign x-time     = string(time,"HH:MM").

assign trans-date = TODAY.
assign work-date  = string(trans-date).
assign substring(disp-date,1,2) = substring(work-date,7,2).
assign substring(disp-date,3,2) = substring(work-date,1,2).
assign substring(disp-date,5,2) = substring(work-date,4,2).

assign work-date  = string(trans-date + 1).
assign substring(disp-date2,1,2) = substring(work-date,7,2).
assign substring(disp-date2,3,2) = substring(work-date,1,2).
assign substring(disp-date2,5,2) = substring(work-date,4,2).

   
assign h-trans_date = disp-date.
assign substring(h-trans_time,1,2) = substring(x-time,1,2).
assign substring(h-trans_time,3,2) = substring(x-time,4,2).
        
find crsb where crsb.cono = g-cono and
                crsb.bankno = p-bankno
                no-lock no-error.
if not avail crsb then
  do:
  message "ERROR - CRSB record not found for bank: " p-bankno.      
  pause.
  return.
end.

find first apet where apet.cono = g-cono and
                      apet.jrnlno = p-jrnlno  and       
                      apet.bankno = p-bankno
                      /*
                      apet.vendno >= b-vendno and
                      apet.vendno <= e-vendno   
                      */
                      no-lock no-error.
if avail apet then
  find apsv where apsv.cono = g-cono and
                  apsv.vendno = apet.vendno
                  no-lock no-error.

put stream deposit UNFORMATTED
   h-record_type          format "9"
   h-priority_code        format "99"
   h-immed_dest           format "x(10)"
   h-immed_origin         format "x(10)"
   h-trans_date           format "x(6)"
   h-trans_time           format "x(4)"
   h-file_id_mod          format "x"
   h-record_size          format "999"
   h-blocking_factor      format "99"
   h-format_code          format "9"
   h-immed_dest_name      format "x(23)"
   h-immed_origin_name    format "x(23)"
   h-reference_code       format "x(8)"
   CHR(13)
   CHR(10).
   /*skip.  */
      
/* Create COMPANY HEADER RECORD */
assign ch-eff_date = disp-date2.


assign ch-stnd_entry_class = if avail apsv and (apsv.vendtype = "SDC" or
                                                apsv.vendtype = "SDS") then
                               "PPD"
                             else
                               "CCD".

/*  06/11/19
if ch-stnd_entry_class = "CCD" then
  assign ch-co_id      = "3390933202"
         cc-company_id = "3390933202". 
else
  assign ch-co_id      = "4390933202"
         cc-company_id = "4390933202".
*/
assign ch-co_id      = "2223827595".    /* 06/11/19 */
assign cc-company_id = "2223827595".    /* 06/11/19 */
assign ch-stnd_entry_class = "CTX".  

put stream deposit UNFORMATTED
        ch-record_type         format "9"
        ch-service_class-cd    format "999"
        ch-co_name             format "x(16)"
        ch-co_dis_name         format "x(20)"
        ch-co_id               format "x(10)"
        ch-stnd_entry_class    format "x(3)"
        ch-co_entry_disc       format "x(10)"
        ch-co_desc_date        format "x(6)"
        ch-eff_date            format "x(6)"
        ch-settlement_date     format "x(3)"
        ch-orig_stat_cd        format "x"
        ch-orig_dfi_id         format "99999999"
        ch-batch_nbr           format "9999999"
        CHR(13)
        CHR(10).
        /*
        skip.*/
        
/* Create ENTRY DETAIL RECORD */
assign detail-cnt = 0.  
assign record-cnt = 0.
assign tot-bal-amt = 0.
for each apet use-index k-jrnl where apet.cono     = g-cono   and
                                     apet.jrnlno   = p-jrnlno and /*
                                     apet.vendno  >= b-vendno and
                                     apet.vendno  <= e-vendno and */  
                                     apet.bankno   = p-bankno and
                                     apet.transcd  = 07          /* payment */
                                     no-lock:
  find apsv use-index k-apsv where apsv.cono     = g-cono         and
                                   apsv.vendno   = apet.vendno    and   
                                   apsv.vendbanktrno ne ""        and
                                   apsv.vendbankacct ne ""     /*   and 
                                   apsv.currbal  > 0           */
                                   no-lock no-error.
  
  if avail apsv then 
    do:
    assign detail-cnt = detail-cnt + 1 
           record-cnt = record-cnt + 1.
    assign receiving-dfi-id   = substring(apsv.vendbanktrno,1,8).
    assign check-digit        = substring(apsv.vendbanktrno,9,1).
    assign d-receiving_dfi_id = receiving-dfi-id.             /*fix*/
    assign d-check_digit      = integer(check-digit).
    assign d-dfi_account_nbr  = apsv.vendbankacct.
    assign d-amount           = apet.amount * 100.
    assign s-grand-amount     = s-grand-amount + apet.amount.
    assign d-employee_name    = CAPS(substr(apsv.name,1,16)).   
    assign d-employee_name    = replace(d-employee_name,"|"," ").
    assign tot-bal-amt        = tot-bal-amt + (apet.amount * 100).
    assign d-trace_number     = detail-cnt.
    assign entry-hash         = string(dec(entry-hash) + 
                                       dec(d-receiving_dfi_id)).    /*fix*/
    if (apsv.vendtype = "edc" or
        apsv.vendtype = "ddc" or
        apsv.vendtype = "sdc" or
        apsv.vendtype = "ldc") then
       assign d-transaction_cd = 22.
    if (apsv.vendtype = "eds" or
        apsv.vendtype = "dds" or
        apsv.vendtype = "sds" or
        apsv.vendtype = "lds") then
      assign d-transaction_cd = 32.
    if (apsv.vendtype = "adc" or
        apsv.vendtype = "xdc" or
        apsv.vendtype = "udc" or
        apsv.vendtype = "pdc") then
      assign d-transaction_cd = 22.
    assign d-individual_id_nbr = if apsv.vendno = 10009856144 then
                                   "IL202020/150495"
                                 else
                                 if apsv.vendno = 10009854222 then
                                   "378750455931000"
                                 else
                                 if apsv.vendno = 10009854281 then
                                   "IL560869/711141"
                                 else
                                 if apsv.vendno = 10009854821 then
                                   "IL560869/854192"
                                 else
                                 if apsv.vendno = 10009854874 then
                                   "    POC39141191"
                                 else
                                 if apsv.vendno = 6000104 then
                                   "          XW383"
                                 else
                                 if apsv.vendno = 10000036835 then
                                   " P#12659/L#7617"
                                 else  
                                 if apsv.vendno = 6000062 then
                                   "  1FC0210011152"
                                 else
                                 if apsv.vendno = 61933500 then
                                   "           D010"
                                 else
                                 if apsv.vendno = 61934000 then
                                   "           D010"
                                 else
                                 if apsv.vendno = 20000018 then
                                   "       00419270"
                                 else
                                 if apsv.vendno = 10009855785 then
                                   "  8310006068665"
                                 else
                                 if apsv.vendno = 10009855776 then
                                   "379639121611006"
                                 else
                                 if apsv.vendno = 10009855779 then
                                   "379639128612007"
                                 else
                                 if apsv.vendno = 10009855778 then
                                   "379639130611005"
                                 else
                                 if apsv.vendno = 10009855777 then
                                   "379639137611008"
                                 else
                                 if apsv.vendno = 10009855775 then
                                   "379639145611008"
                                 else
                                 if apsv.vendno = 10009855900 then
                                   "379639888361001"
                                 else
                                 if apsv.vendno = 80000112 and
                                    apsv.cono   = 80 then
                                   "       CR613453"
                                 else  
                                   "               ".
    if d-individual_id_nbr = " " then
      assign d-individual_id_nbr = string(apsv.vendno).
    /* moved lower in logic due to CTX count */
    /*
    put stream deposit UNFORMATTED
           d-record_type       format  "9"
           d-transaction_cd    format  "99"
           d-receiving_dfi_id  format  "99999999"
           d-check_digit       format  "9"
           d-dfi_account_nbr   format  "x(17)"
           d-amount            format  "9999999999"
           d-individual_id_nbr format  "x(15)"
           d-employee_name     format  "x(22)"
           d-descret_data      format  "x(2)"
           d-addenda_rec_ind   format  "9"
           d-constant          format  "99999999"
           d-trace_number      format  "9999999"
           CHR(13)
           CHR(10). 
           /*
           skip.*/
    */
    
    find first notes where notes.cono = g-cono and
                           notes.notestype = "zz" and
                           notes.primarykey = "ACH-CTX" and
                           notes.secondarykey = apsv.edipartner
                           no-lock no-error.
    find ACHrpt where ACHrpt.vendno = apet.vendno and     
                      ACHrpt.checkno = apet.checkno 
                      no-error. 
    if not avail ACHrpt then
      do:
      create ACHrpt.
      assign ACHrpt.vendno  = apsv.vendno    
             ACHrpt.name    = apsv.name
             ACHrpt.amount  = apet.amount
             ACHrpt.checkno = apet.checkno.
    end.
    for each b-apet where b-apet.cono    = apet.cono and
                          b-apet.jrnlno  = apet.jrnlno and
                          b-apet.vendno  = apet.vendno and
                          b-apet.checkno = apet.checkno and
                          b-apet.transcd = 03               /* INVOICE */
                          no-lock:
      find CTX where CTX.vendno = b-apet.vendno and
                     CTX.jrnlno = b-apet.jrnlno and
                     CTX.checkno = b-apet.checkno 
                     no-error.
      if avail CTX then
        do:
        REMITLOOP:
        do zz = 1 to 99:     
          if CTX.r-RMR-0[zz] = " " then  
            do:
            assign CTX.r-RMR-0[zz]     = "RMR"
                   CTX.r-RMR-1[zz]     = "BM"
                   CTX.r-RMR-2[zz]     = right-trim(string(b-apet.apinvno))
                   CTX.r-RMR-3[zz]     = ""
                   CTX.r-RMR-4[zz]     = string(b-apet.amount)
                 /*  CTX.r-RMR-5[zz]     = string(b-apet.amount)    */
                   CTX.r-REFe-0[zz]    = "REF"
                   CTX.r-REFe-1[zz]    = "EQ"
                   CTX.r-REFe-2[zz]    = right-trim(b-apet.refer)
                   CTX.r-DTM-0[zz]     = "DTM"
                   CTX.r-DTM-1[zz]     = "003"
                   CTX.r-DTM-2[zz]     = string(year(b-apet.invdt),"9999") +
                                         string(month(b-apet.invdt),"99")  +
                                         string(day(b-apet.invdt),"99").
            leave REMITLOOP.
          end. /* found an empty occurance */     
        end. /* do zz = 1 to 99 */
      end.
      else
        do:
        create CTX.
        assign CTX.vendno         = b-apet.vendno
               CTX.jrnlno         = b-apet.jrnlno 
               CTX.checkno        = b-apet.checkno  
               CTX.r-record-type  = "7"
               CTX.r-addenda-type = "05"
               CTX.r-ISA-0        = "ISA"
               CTX.r-ISA-1        = "00"
               CTX.r-ISA-2        = "STS-EFT"
               CTX.r-ISA-3        = "00"
               CTX.r-ISA-4        = "STS-EFT"
               CTX.r-ISA-5        = "01"
               CTX.r-ISA-6        = "160946992"
               CTX.r-ISA-7        = if avail notes then
                                      notes.noteln[1]
                                    else
                                      ""
               CTX.r-ISA-8        = if avail notes then
                                      notes.noteln[2]
                                    else
                                      "         "
               CTX.r-ISA-9        = x-year + x-day + x-month
               CTX.r-ISA-10       = ISA-time
               CTX.r-ISA-11       = "U"
               CTX.r-ISA-12       = "00200"
               CTX.r-ISA-13       = "000000001"
               CTX.r-ISA-14       = "0"
               CTX.r-ISA-15       = "P"
               CTX.r-ISA-16       = ">"
               CTX.r-GS-0         = "GS"
               CTX.r-GS-1         = "RA"
               CTX.r-GS-2         = "160946992"
               CTX.r-GS-3         = if avail notes then
                                      notes.noteln[1]
                                    else
                                      "         "
               CTX.r-GS-4         = x-wyear + x-month + x-day
               CTX.r-GS-5         = x-ftime
               CTX.r-GS-6         = "000000001"
               CTX.r-GS-7         = "X"
               CTX.r-GS-8         = "004010"
               CTX.r-ST-0         = "ST"
               CTX.r-ST-1         = "820"
               CTX.r-ST-2         = "00001"
               CTX.r-BPR-0        = "BPR"
               CTX.r-BPR-1        = "X"   
               CTX.r-BPR-2        = string(apet.amount)
               CTX.r-BPR-3        = "C"   
               CTX.r-BPR-4        = "ACH"
               CTX.r-BPR-5        = "CTX"
               CTX.r-BPR-6        = "01"
               CTX.r-BPR-7        = apsv.vendbanktrno
               CTX.r-BPR-8        = "DA"    
               CTX.r-BPR-9        = apsv.vendbankacct
               CTX.r-BPR-10       = string(apsv.vendno)
               CTX.r-BPR-11       = ""
               CTX.r-BPR-12       = "01"
               CTX.r-BPR-13       = crsb.transrouteno
               CTX.r-BPR-14       = "DA"
               CTX.r-BPR-15       = right-trim(substr(crsb.bankacct,10,11))    
               CTX.r-BPR-16       = x-wyear + x-month + x-day
               CTX.r-TRN-0        = "TRN"
               CTX.r-TRN-1        = "1"
               CTX.r-TRN-2        = string(apet.checkno)
               CTX.r-TRN-3        = "SUNSOURCE"
               CTX.r-CUR-0        = "CUR"
               CTX.r-CUR-1        = "PR"
               CTX.r-CUR-2        = if apsv.currencyty = "" or 
                                       apsv.currencyty = "us" then 
                                      "USD" 
                                    else
                                      "CAD"
               CTX.r-CUR-3        = ""
               CTX.r-CUR-4        = "PE"
               CTX.r-CUR-5        = CTX.r-CUR-2
               CTX.r-REF-0        = "REF"
               CTX.r-REF-1        = "CK"
               CTX.r-REF-2        = string(apet.checkno)
               CTX.r-REF-3        = "PAYMENT"   
               CTX.r-N1a-0        = "N1"
               CTX.r-N1a-1        = "PE"
               CTX.r-N1a-2        = replace(apsv.name,"|"," ")
               CTX.r-N2a-0        = "N2"
               CTX.r-N2a-1        = apsv.addr[1]
               CTX.r-N3a-0        = "N3"
               CTX.r-N3a-1        = apsv.addr[2]
               CTX.r-N4a-0        = "N4"
               CTX.r-N4a-1        = apsv.city
               CTX.r-N4a-2        = apsv.state
               CTX.r-N4a-3        = apsv.zip
               CTX.r-N1b-0        = "N1"
               CTX.r-N1b-1        = "PR"
               CTX.r-N1b-2        = "SUNSOURCE"
               CTX.r-ENT-0        = "ENT"
               CTX.r-ENT-1        = "1"
               CTX.r-RMR-0[1]     = "RMR"
               CTX.r-RMR-1[1]     = "BM"
               CTX.r-RMR-2[1]     = right-trim(string(b-apet.apinvno))
               CTX.r-RMR-3[1]     = ""
               CTX.r-RMR-4[1]     = string(b-apet.amount)
            /*   CTX.r-RMR-5[1]     = string(b-apet.amount) */
               CTX.r-REFe-0[1]    = "REF"
               CTX.r-REFe-1[1]    = "EQ"
               CTX.r-REFe-2[1]    = right-trim(b-apet.refer)
               CTX.r-DTM-0[1]     = "DTM"
               CTX.r-DTM-1[1]     = "003"
               CTX.r-DTM-2[1]     = string(year(b-apet.invdt),"9999") +
                                    string(month(b-apet.invdt),"99")  +
                                    string(day(b-apet.invdt),"99")
               CTX.r-SE-0         = "SE"
               CTX.r-SE-1         = "13"
               CTX.r-SE-2         = "00001"
               CTX.r-GE-0         = "GE"
               CTX.r-GE-1         = "1"
               CTX.r-GE-2         = "000000001"
               CTX.r-IEA-0        = "IEA"
               CTX.r-IEA-1        = "1"
               CTX.r-IEA-2        = "000000001".
      end.
      find remit where remit.vendno  = b-apet.vendno and
                       remit.checkno = b-apet.checkno and
                       remit.refer   = b-apet.apinvno
                       no-error.
      if not avail remit then
        do:
        create remit.
        assign remit.vendno    = b-apet.vendno
               remit.checkno   = b-apet.checkno
               remit.amount    = b-apet.amount
               remit.apinvno   = b-apet.apinvno
               remit.reference = b-apet.refer.
      end. /* not avail remit */ 
      
      find first poeh where poeh.cono = g-cono and
                            poeh.pono = int(b-apet.refer) and
                            poeh.vendno = apet.vendno
                            no-lock no-error.
      find ACHrpt where ACHrpt.vendno = apet.vendno and
                        ACHrpt.checkno = apet.checkno
                        no-error.
      if avail ACHrpt then
        assign ACHrpt.tiedfl = if avail poeh and ACHrpt.tiedfl = " " then
                                 "yes"
                               else
                                 if not avail poeh then
                                   "no"
                                 else
                                   ACHrpt.tiedfl
               ACHrpt.notesfl = if ACHrpt.notesfl = " " then 
                                  b-apet.notesfl
                                else
                                  ACHrpt.notesfl.
    end. /* each b-apet (transcd = 03 INVOICE) */       
    
    for each c-apet where c-apet.cono      = apet.cono and
                          c-apet.pidjrnlno = apet.jrnlno and
                          c-apet.vendno    = apet.vendno and
                          c-apet.checkno   = apet.checkno and
                          c-apet.transcd   = 05     /* CREDITS */
                          no-lock:    
      find CTX where CTX.vendno  = c-apet.vendno and
                     CTX.jrnlno  = c-apet.jrnlno and
                     CTX.checkno = c-apet.checkno
                     no-error.
      if avail CTX then
        do:
        REMITLOOP:
        do zz = 1 to 99:     
          if CTX.r-RMR-0[zz] = " " then  
            do:
            assign CTX.r-RMR-0[zz]     = "RMR"
                   CTX.r-RMR-1[zz]     = "BM"
                   CTX.r-RMR-2[zz]     = right-trim(string(c-apet.apinvno))
                   CTX.r-RMR-3[zz]     = ""
                   CTX.r-RMR-4[zz]     = string(c-apet.amount)
                /*   CTX.r-RMR-5[zz]     = string(c-apet.amount)    */
                   CTX.r-REFe-0[zz]    = "REF"
                   CTX.r-REFe-1[zz]    = "EQ"
                   CTX.r-REFe-2[zz]    = right-trim(c-apet.refer)
                   CTX.r-DTM-0[zz]     = "DTM"
                   CTX.r-DTM-1[zz]     = "003"
                   CTX.r-DTM-2[zz]     = string(year(c-apet.invdt),"9999") +
                                         string(month(c-apet.invdt),"99")  +
                                         string(day(c-apet.invdt),"99").
            leave REMITLOOP.
          end. /* found an empty occurance */     
        end. /* do zz = 1 to 99 */
      end.
      else
        do:
        create CTX.
        assign CTX.vendno         = c-apet.vendno
               CTX.jrnlno         = c-apet.jrnlno 
               CTX.checkno        = c-apet.checkno  
               CTX.r-record-type  = "7"
               CTX.r-addenda-type = "05"
               CTX.r-ISA-0        = "ISA"
               CTX.r-ISA-1        = "00"
               CTX.r-ISA-2        = "STS-EFT"
               CTX.r-ISA-3        = "00"
               CTX.r-ISA-4        = "STS-EFT"
               CTX.r-ISA-5        = "01"
               CTX.r-ISA-6        = "160946992"
               CTX.r-ISA-7        = if avail notes then 
                                      notes.noteln[1]
                                    else
                                      " "
               CTX.r-ISA-8        = if avail notes then
                                      notes.noteln[2]
                                    else
                                      "         "
               CTX.r-ISA-9        = x-year + x-day + x-month
               CTX.r-ISA-10       = ISA-time
               CTX.r-ISA-11       = "U"
               CTX.r-ISA-12       = "00200"
               CTX.r-ISA-13       = "000000001"
               CTX.r-ISA-14       = "0"
               CTX.r-ISA-15       = "P"
               CTX.r-ISA-16       = ">"
               CTX.r-GS-0         = "GS"
               CTX.r-GS-1         = "RA"
               CTX.r-GS-2         = "160946992"
               CTX.r-GS-3         = if avail notes then
                                      notes.noteln[2]
                                    else
                                      "         "
               CTX.r-GS-4         = x-wyear + x-month + x-day
               CTX.r-GS-5         = x-ftime
               CTX.r-GS-6         = "000000001"
               CTX.r-GS-7         = "X"
               CTX.r-GS-8         = "004010"
               CTX.r-ST-0         = "ST"
               CTX.r-ST-1         = "820"
               CTX.r-ST-2         = "00001"
               CTX.r-BPR-0        = "BPR"
               CTX.r-BPR-1        = "X"   
               CTX.r-BPR-2        = string(apet.amount)
               CTX.r-BPR-3        = "C"   
               CTX.r-BPR-4        = "ACH"
               CTX.r-BPR-5        = "CTX"
               CTX.r-BPR-6        = "01"
               CTX.r-BPR-7        = apsv.vendbanktrno
               CTX.r-BPR-8        = "DA"    
               CTX.r-BPR-9        = apsv.vendbankacct
               CTX.r-BPR-10       = string(apsv.vendno)
               CTX.r-BPR-11       = ""
               CTX.r-BPR-12       = "01"
               CTX.r-BPR-13       = "071923284"
               CTX.r-BPR-14       = "DA"
               CTX.r-BPR-15       = "8765429470"
               CTX.r-BPR-16       = x-wyear + x-month + x-day
               CTX.r-TRN-0        = "TRN"
               CTX.r-TRN-1        = "1"
               CTX.r-TRN-2        = string(apet.checkno)
               CTX.r-TRN-3        = "SUNSOURCE"
               CTX.r-CUR-0        = "CUR"
               CTX.r-CUR-1        = "PR"
               CTX.r-CUR-2        = if apsv.currencyty = "" or 
                                       apsv.currencyty = "us" then 
                                      "USD" 
                                    else
                                      "CAD"
               CTX.r-CUR-3        = ""
               CTX.r-CUR-4        = "PE"
               CTX.r-CUR-5        = CTX.r-CUR-2
               CTX.r-REF-0        = "REF"
               CTX.r-REF-1        = "CK"
               CTX.r-REF-2        = string(apet.checkno)
               CTX.r-REF-3        = "PAYMENT"   
               CTX.r-N1a-0        = "N1"
               CTX.r-N1a-1        = "PE"
               CTX.r-N1a-2        = apsv.name
               CTX.r-N2a-0        = "N2"
               CTX.r-N2a-1        = apsv.addr[1]
               CTX.r-N3a-0        = "N3"
               CTX.r-N3a-1        = apsv.addr[2]
               CTX.r-N4a-0        = "N4"
               CTX.r-N4a-1        = apsv.city
               CTX.r-N4a-2        = apsv.state
               CTX.r-N4a-3        = apsv.zip
               CTX.r-N1b-0        = "N1"
               CTX.r-N1b-1        = "PR"
               CTX.r-N1b-2        = "SUNSOURCE"
               CTX.r-ENT-0        = "ENT"
               CTX.r-ENT-1        = "1"
               CTX.r-RMR-0[1]     = "RMR"
               CTX.r-RMR-1[1]     = "BM"
               CTX.r-RMR-2[1]     = right-trim(string(c-apet.apinvno))
               CTX.r-RMR-3[1]     = ""
               CTX.r-RMR-4[1]     = string(c-apet.amount)
            /*   CTX.r-RMR-5[1]     = string(c-apet.amount) */
               CTX.r-REFe-0[1]    = "REF"
               CTX.r-REFe-1[1]    = "EQ"
               CTX.r-REFe-2[1]    = right-trim(c-apet.refer)
               CTX.r-DTM-0[1]     = "DTM"
               CTX.r-DTM-1[1]     = "003"
               CTX.r-DTM-2[1]     = string(year(c-apet.invdt),"9999") +
                                    string(month(c-apet.invdt),"99")  +
                                    string(day(c-apet.invdt),"99")
               CTX.r-SE-0         = "SE"
               CTX.r-SE-1         = "13"
               CTX.r-SE-2         = "00001"
               CTX.r-GE-0         = "GE"
               CTX.r-GE-1         = "1"
               CTX.r-GE-2         = "000000001"
               CTX.r-IEA-0        = "IEA"
               CTX.r-IEA-1        = "1"
               CTX.r-IEA-2        = "000000001".
      end.
      
      find remit where remit.vendno  = c-apet.vendno and
                       remit.checkno = c-apet.checkno and
                       remit.refer   = c-apet.apinvno
                       no-error.
      if not avail remit then
        do:
        create remit.
        assign remit.vendno    = c-apet.vendno
               remit.checkno   = c-apet.checkno
               remit.amount    = c-apet.amount    
               remit.apinvno   = c-apet.apinvno
               remit.reference = c-apet.refer.
      end. /* not avail remit */
      find first poeh where poeh.cono = g-cono and
                            poeh.pono = int(c-apet.refer) and
                            poeh.vendno = apet.vendno
                            no-lock no-error.
      find ACHrpt where ACHrpt.vendno = apet.vendno and
                        ACHrpt.checkno = apet.checkno
                        no-error.
      if avail ACHrpt then
        assign ACHrpt.tiedfl = if avail poeh and ACHrpt.tiedfl = " " then
                                 "yes"
                               else
                                 if not avail poeh then
                                   "no"
                                 else
                                   ACHrpt.tiedfl.   
    end. /* each c-apet - transcd = 05 CREDITS */
    
    assign CTX-segment = ""
           CTX-detail  = ""
           CTX-line    = ""
           CTX-seqno   = "".
    for each CTX where CTX.vendno  = apet.vendno and
                       CTX.jrnlno  = apet.jrnlno and
                       CTX.checkno = apet.checkno 
                       no-lock: 
      
      assign /*CTX-segment = CTX-segment + CTX.r-record-type
             CTX-segment = CTX-segment + CTX.r-addenda-type     */
             CTX-segment = CTX-segment + CTX.r-ISA-0        + x-ast
             CTX-segment = CTX-segment + CTX.r-ISA-1        + x-ast
             CTX-segment = CTX-segment + CTX.r-ISA-2        + x-ast
             CTX-segment = CTX-segment + CTX.r-ISA-3        + x-ast
             CTX-segment = CTX-segment + CTX.r-ISA-4        + x-ast
             CTX-segment = CTX-segment + CTX.r-ISA-5        + x-ast
             CTX-segment = CTX-segment + CTX.r-ISA-6        + x-ast
             CTX-segment = CTX-segment + CTX.r-ISA-7        + x-ast
             CTX-segment = CTX-segment + CTX.r-ISA-8        + x-ast
             CTX-segment = CTX-segment + CTX.r-ISA-9        + x-ast
             CTX-segment = CTX-segment + CTX.r-ISA-10       + x-ast
             CTX-segment = CTX-segment + CTX.r-ISA-11       + x-ast
             CTX-segment = CTX-segment + CTX.r-ISA-12       + x-ast
             CTX-segment = CTX-segment + CTX.r-ISA-13       + x-ast
             CTX-segment = CTX-segment + CTX.r-ISA-14       + x-ast
             CTX-segment = CTX-segment + CTX.r-ISA-15       + x-ast
             CTX-segment = CTX-segment + CTX.r-ISA-16       + x-slash
             CTX-segment = CTX-segment + CTX.r-GS-0         + x-ast
             CTX-segment = CTX-segment + CTX.r-GS-1         + x-ast
             CTX-segment = CTX-segment + CTX.r-GS-2         + x-ast
             CTX-segment = CTX-segment + CTX.r-GS-3         + x-ast
             CTX-segment = CTX-segment + CTX.r-GS-4         + x-ast
             CTX-segment = CTX-segment + CTX.r-GS-5         + x-ast
             CTX-segment = CTX-segment + CTX.r-GS-6         + x-ast
             CTX-segment = CTX-segment + CTX.r-GS-7         + x-ast
             CTX-segment = CTX-segment + CTX.r-GS-8         + x-slash
             CTX-segment = CTX-segment + CTX.r-ST-0         + x-ast
             CTX-segment = CTX-segment + CTX.r-ST-1         + x-ast
             CTX-segment = CTX-segment + CTX.r-ST-2         + x-slash
             CTX-segment = CTX-segment + CTX.r-BPR-0        + x-ast
             CTX-segment = CTX-segment + CTX.r-BPR-1        + x-ast
             CTX-segment = CTX-segment + CTX.r-BPR-2        + x-ast
             CTX-segment = CTX-segment + CTX.r-BPR-3        + x-ast
             CTX-segment = CTX-segment + CTX.r-BPR-4        + x-ast
             CTX-segment = CTX-segment + CTX.r-BPR-5        + x-ast
             CTX-segment = CTX-segment + CTX.r-BPR-6        + x-ast
             CTX-segment = CTX-segment + CTX.r-BPR-7        + x-ast
             CTX-segment = CTX-segment + CTX.r-BPR-8        + x-ast
             CTX-segment = CTX-segment + CTX.r-BPR-9        + x-ast
             CTX-segment = CTX-segment + CTX.r-BPR-10       + x-ast
             CTX-segment = CTX-segment + CTX.r-BPR-11       + x-ast
             CTX-segment = CTX-segment + CTX.r-BPR-12       + x-ast
             CTX-segment = CTX-segment + CTX.r-BPR-13       + x-ast
             CTX-segment = CTX-segment + CTX.r-BPR-14       + x-ast
             CTX-segment = CTX-segment + CTX.r-BPR-15       + x-ast
             CTX-segment = CTX-segment + CTX.r-BPR-16       + x-slash
             CTX-segment = CTX-segment + CTX.r-TRN-0        + x-ast
             CTX-segment = CTX-segment + CTX.r-TRN-1        + x-ast
             CTX-segment = CTX-segment + CTX.r-TRN-2        + x-ast
             CTX-segment = CTX-segment + CTX.r-TRN-3        + x-slash
             CTX-segment = CTX-segment + CTX.r-CUR-0        + x-ast
             CTX-segment = CTX-segment + CTX.r-CUR-1        + x-ast
             CTX-segment = CTX-segment + CTX.r-CUR-2        + x-ast
             CTX-segment = CTX-segment + CTX.r-CUR-3        + x-ast
             CTX-segment = CTX-segment + CTX.r-CUR-4        + x-ast
             CTX-segment = CTX-segment + CTX.r-CUR-5        + x-slash
             CTX-segment = CTX-segment + CTX.r-REF-0        + x-ast
             CTX-segment = CTX-segment + CTX.r-REF-1        + x-ast
             CTX-segment = CTX-segment + CTX.r-REF-2        + x-ast
             CTX-segment = CTX-segment + CTX.r-REF-3        + x-slash
             CTX-segment = CTX-segment + CTX.r-N1a-0        + x-ast
             CTX-segment = CTX-segment + CTX.r-N1a-1        + x-ast
             CTX-segment = CTX-segment + CTX.r-N1a-2        + x-slash
             CTX-segment = CTX-segment + CTX.r-N2a-0        + x-ast
             CTX-segment = CTX-segment + CTX.r-N2a-1        + x-slash
             CTX-segment = CTX-segment + CTX.r-N3a-0        + x-ast
             CTX-segment = CTX-segment + CTX.r-N3a-1        + x-slash
             CTX-segment = CTX-segment + CTX.r-N4a-0        + x-ast
             CTX-segment = CTX-segment + CTX.r-N4a-1        + x-ast
             CTX-segment = CTX-segment + CTX.r-N4a-2        + x-ast
             CTX-segment = CTX-segment + CTX.r-N4a-3        + x-slash
             CTX-segment = CTX-segment + CTX.r-N1b-0        + x-ast
             CTX-segment = CTX-segment + CTX.r-N1b-1        + x-ast
             CTX-segment = CTX-segment + CTX.r-N1b-2        + x-slash
             CTX-segment = CTX-segment + CTX.r-ENT-0        + x-ast
             CTX-segment = CTX-segment + CTX.r-ENT-1        + x-slash.
      do zz = 1 to 99:
        if CTX.r-RMR-0[zz] = "" then leave.
        assign CTX-segment = CTX-segment +  CTX.r-RMR-0[zz]  + x-ast
               CTX-segment = CTX-segment +  CTX.r-RMR-1[zz]  + x-ast
               CTX-segment = CTX-segment +  CTX.r-RMR-2[zz]  + x-ast
               CTX-segment = CTX-segment +  CTX.r-RMR-3[zz]  + x-ast
               CTX-segment = CTX-segment +  CTX.r-RMR-4[zz]  + x-slash
          /*     CTX-segment = CTX-segment +  CTX.r-RMR-5[zz]  + x-slash    */
               CTX-segment = CTX-segment +  CTX.r-REFe-0[zz] + x-ast
               CTX-segment = CTX-segment +  CTX.r-REFe-1[zz] + x-ast
               CTX-segment = CTX-segment +  CTX.r-REFe-2[zz] + x-slash
               CTX-segment = CTX-segment +  CTX.r-DTM-0[zz]  + x-ast
               CTX-segment = CTX-segment +  CTX.r-DTM-1[zz]  + x-ast
               CTX-segment = CTX-segment +  CTX.r-DTM-2[zz]  + x-slash.
      end. /* 1 to 99 */
      assign CTX-segment = CTX-segment + CTX.r-SE-0         + x-ast
             CTX-segment = CTX-segment + CTX.r-SE-1         + x-ast
             CTX-segment = CTX-segment + CTX.r-SE-2         + x-ast
             CTX-segment = CTX-segment + CTX.r-GE-0         + x-ast
             CTX-segment = CTX-segment + CTX.r-GE-1         + x-ast
             CTX-segment = CTX-segment + CTX.r-GE-2         + x-slash
             CTX-segment = CTX-segment + CTX.r-IEA-0        + x-ast
             CTX-segment = CTX-segment + CTX.r-IEA-1        + x-ast
             CTX-segment = CTX-segment + CTX.r-IEA-2        + x-slash.
    
      /* Round Up */
      assign w-addenda-cnt = length(CTX-segment) / 80.
      if w-addenda-cnt = truncate(w-addenda-cnt,0) then
        assign d-addenda_cnt = w-addenda-cnt.
      else
        assign d-addenda_cnt = truncate(w-addenda-cnt,0) + 1.
  
      put stream deposit UNFORMATTED
           d-record_type       format  "9"
           d-transaction_cd    format  "99"
           d-receiving_dfi_id  format  "99999999"
           d-check_digit       format  "9"
           d-dfi_account_nbr   format  "x(17)"
           d-amount            format  "9999999999"
           d-individual_id_nbr format  "x(15)"
           d-addenda_cnt       format  "9999"
           d-employee_name     format  "x(16)"
           d-descret_data      format  "x(4)"
           d-addenda_rec_ind   format  "9"
           d-constant          format  "99999999"
           d-trace_number      format  "9999999"
           CHR(13)
           CHR(10). 

      
      do yy = 1 to 99:
        assign CTX-seqno  = string((int(CTX-seqno) + 1),"9999").  
        assign CTX-detail = substr(CTX-segment,(1 + ((yy - 1) * 80)),80).    
        if CTX-detail = "" then leave.  
        if length(CTX-detail) < 80 then
          do:
          assign CTX-line = CTX.r-record-type + CTX.r-addenda-type +
                            CTX-detail. 
          overlay(CTX-line,84,11) = string(CTX-seqno,"9999") + 
                                    string(d-trace_number,"9999999").
        end.
        else
          assign CTX-line = CTX.r-record-type + CTX.r-addenda-type +
                            CTX-detail + string(CTX-seqno,"9999") + 
                            string(d-trace_number,"9999999").
        assign record-cnt = record-cnt + 1.
        put stream deposit UNFORMATTED
          CTX-line
          CHR(13)
          CHR(10).
          /*
          skip.*/
        assign CTX-line = "".
      end. /* do yy = 1 to 99 */
    end. /* each CTX */  
  end. /* avail apsv */
end. /* each apet */
              

/*Create BATCH CONTROL RECORD */
assign cc-entry_add_cnt     = record-cnt.   /*detail-cnt.*/
assign hash-amt             = string(entry-hash).
assign trunc-hash           = substring(hash-amt,1,10).
assign cc-entry_hash        = dec(trunc-hash).          /*fix*/
assign cc-tot_cred_amount   = tot-bal-amt.



put stream deposit UNFORMATTED
       cc-record_type             format  "9"
       cc-service_cl_cd           format  "999"
       cc-entry_add_cnt           format  "999999"
       cc-entry_hash              format  "9999999999"
       cc-tot_debit_amount        format  "999999999999"
       cc-tot_cred_amount         format  "999999999999"
       cc-company_id              format  "x(10)"
       cc-auth_code               format  "x(19)"
       cc-reserved                format  "x(6)"
       cc-orig_dfi_id             format  "99999999"
       cc-batch_nbr               format  "9999999"
       CHR(13)
       CHR(10).
       /*
       skip.*/
       
/* Create FILE CONTROL RECORD */
assign fc-entry_add_cnt     = record-cnt.  /*detail-cnt.*/
assign fc-entry_hash        = dec(trunc-hash).           /*fix*/
assign fc-tot_file_cred     = tot-bal-amt.
put stream deposit UNFORMATTED
       fc-record_type             format  "9"
       fc-batch_cnt               format  "999999"
       fc-block_cnt               format  "999999"
       fc-entry_add_cnt           format  "99999999"
       fc-entry_hash              format  "9999999999"
       fc-tot_file_debit          format  "999999999999"
       fc-tot_file_cred           format  "999999999999"
       fc-reserved                format  "x(39)"
       CHR(13)
       CHR(10).
       /*
       skip.*/
/*
display 
   s-grand-amount with frame f-total.
*/  
/*
PROCEDURE headers:
  page.
  assign x-page = x-page + 1.   
  assign x-line = 4.   
  display   
     x-today     x-time     g-cono     g-operinit     x-page   
   with frame f-header.   
end.
*/
PROCEDURE ACHheaders:
  page.
  assign x-page = x-page + 1.   
  assign x-line = 4.   
  display   
     x-today     x-time     g-cono     g-operinit     x-page   
   with frame f-ACHheader.   
end.



output stream deposit close.

if opsys = "unix" then
  do:
  os-copy value(o_file) value(destfilename).
end.

def var payfile as c format "x(45)"  no-undo.
assign payfile = "/usr/tmp/ACHpayments_" + x-month + x-day + x-year + ".txt".
output to value(payfile).

  /* put date into YYMMDD format */
  assign x-today    = TODAY.
  assign x-time     = string(time,"HH:MM"). 
  assign x-page = 1.
  assign x-line = 4.
  display
     x-today     x-time     g-cono     g-operinit     x-page
      with frame f-ACHheader.
  for each ACHrpt no-lock break by ACHrpt.tiedfl
                                by ACHrpt.amount DESCENDING:
    assign x-total = x-total + ACHrpt.amount.
    display ACHrpt.vendno 
            ACHrpt.name
            ACHrpt.amount
            ACHrpt.checkno  
            ACHrpt.tiedfl
            ACHrpt.notesfl
            with frame f-ACHdetail.
    down with frame f-ACHdetail.
    assign x-line = x-line + 1.
    if (x-line + 4) > x-page-size then
      do:
      run ACHheaders.
    end.
    
    /***** Separate process if Detailed Remit is requested
    display x-blank with frame f-remithdr.  
    assign x-line = x-line + 1.
    down with frame f-remithdr.
    for each remit where remit.vendno  = ACHrpt.vendno  and
                         remit.checkno = ACHrpt.checkno 
                         no-lock:
      display remit.apinvno
              remit.apinvno
              remit.reference
              remit.amount
      with frame f-remit.
      down with frame f-remit.  
      assign x-line = x-line + 1.
    end.
    display x-blank with frame f-blank.
    down with frame f-blank.
    assign x-line = x-line + 1.
    *******/
  end. /* for each ACHrpt */
  display x-blank with frame f-blank.
  display x-blank
          p-jrnlno
          x-total
          with frame f-total.
  down with frame f-total.
  output close.
  
  /* EMAIL the ACH Payment File */
  assign cMailSubject = "Bank of America ACH Payment Results"
         cMailto      = "ltews@sunsrce.com"     + " " +  
                        "roleksiak@sunsrce.com" + " " + 
                        "gyocheva@sunsrce.com"  + " " +
                        "yradomski@sunsrce.com" + " " + 
                        "ebitri@sunsrce.com"    + " " +
                        "pabraham@sunsrce.com"  + " " +
                        "dsivak@sunsrce.com"   
         cMailmessage = "The attached report contains ACH Payments. " +
                        "Please review and contact AP to release  "  +
                        "for payment."
         v-emailfile  = payfile.
             
        /* send out over50K.txt via email */
        unix silent value ("sh /rd/cust/local/jailtinv.sh -b " + 
                           "-s " + "'" + cMailsubject + "' " +   
                           "-l " + "'" + cMailMessage + "' " +   
                           "-n '" + g-operinit + "' " +          
                           "'" + cMailto + "'" + " " +        
                           "'" + v-emailfile + "'").
