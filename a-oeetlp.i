/*h*****************************************************************************
  INCLUDE      : a-oeetlp.i
  DESCRIPTION  : Kit Component - calculate price and cost rollups for kits
  USED ONCE?   : Yes (oeetlpr.p)
  AUTHOR       : kmw
  DATE WRITTEN :
  CHANGES MADE :
    07/31/91 kmw; TB#  2277 Kit Enhancements - add pricefl for price roll
    08/11/91 kmw; TB#  1541 Kit Enhancement - Do PDSC pricing for components
    08/23/91 kmw; TB#  2827 Kit Enhancements - always roll cost
    09/13/91 kmw; TB#  4506 Allow negative components
    10/16/92 dkk; TB#  8019 Allow pricing of negative option components
    11/10/92 kmw; TB#  8625 Add product line/category pricing
    11/22/92 kmw; TB#  8624 Add margin pricing
    02/01/93 mwb; TB#  9673 Removed the v-price < 0 edit(tb8019) to outside the
        kit roll loop (oeetlpr.p).
    02/18/93 kmw; TB#  4720 PDSC actual not updated properly - Replace o-qtyord
        with o-totqtyord
    03/17/93 kmw; TB#  9389 BOD pricing cost roll
    04/22/93 dby; TB# 11005 Fix price type override for bp kits
    04/26/93 kmw; TB#  5984 Lastcost only 2 places on kit
    07/20/93 dww; TB#  9303 BOD price is incorrect if roll and disc
    07/22/93 mwb; TB# 12239 Round component cost to 5 places to roll to OE
        line cost (reflects oeepi inv). If the rounding changes, chg oeepi also.
        (a-oeepig.i, p-oeepic.i).
    10/29/93 kmw; TB# 11758 Set unit conv to 10 dec
    06/06/95 ajw; TB# 18575 Nonstock/Special Kit Component Project (A5)
    10/18/95 gp;  TB# 19510 Cost for special kit component changes
    01/08/96 kr;  TB# 12306 Code cleanup
    03/09/97 mms; TB#  7241 Spec8.0 Special Price Costing; Reworked repricing
        and recosting to handle the situation where the history record may not
        be special price or cost in the same fashion as current data.
        Added the b-icss buffer as new shared for current icsp information on
       special price and cost conversions; added the supporting icss logic.
    01/20/99 cm;  TB# 25179 Kit component price rollup incorrect if PDSC based
        on (c)ost or (m)argin
    06/07/99 lbr; TB# 25823 Removed Special Price cost conversions already done
        in p-oeetll.i
    09/02/99 bp ; TB# 26575 Add logic to permit use of SPC defaults.  Here,
        remove references to the ICSS buffer.  The historical SPC data is
        in the "v-" variables and the current data is in the "vc-" variables.
        Both are set in oeetlpr.p
    09/30/99  aa; TB# e2761 Tally Project - Tally pricing.  Added logic for
        OEELM.  Added parameter for OEELK, OEELM
    10/14/99  aa; TB# e2761 - Tally Project - added named parameters &pricefl
                and &unit to handle fields that don't exist in KPSM; added loop
                through KPSM for OEIP inquiries (see oeetlpr.p, oeetlpr.lca)
    11/02/99 bp ; TB# 26575 Added additional logic to control SPC conversion
        after pricing.
    10/05/00 abe; TB#6706 add user hook {&user_bottom}.
    03/22/01 lbr; TB# e8419 Merged the 2 p-oeetlm.i
    02/07/01 lbr; TB# t24039 Added pricing based on rebted cost,rebated margin
    02/28/02 bm;  TB# e9300 Add new values to price calc type field (priceclty)
        for calculating price on base price and list price.
    07/30/04 emc; TB# e20444 Kit roll prices/margins w/ RM/RC component
    04/25/05 emc; TB# e21464 Promo window not honoring rebate w/ RM or RC -
        Do not apply SPC factor to pdcost before pricing - wait until after
        pricing.  Undo parts of TB t25179, t26575 and t24039.
    06/14/05 emc; TB# e8184 Code Cleanup
    06/14/05 emc; TB# e8184 Catalog component does not pull PD
    11/23/05 sbr; TB# e20702 PDRC misses neg margin products
    03/15/07 jhb; TB# e25598 Fix cost override of BOD non-stock catalog
        component
    07/26/07 mwb; INDF-01205 Fix to TB e25598 (ERP-17873) for the kit roll.
        Was not rolling components with spec/price costing set. 'no oeelk'
        progress error. Roll failed in OEIP and eSales (Get-ProdPrice).
    02/04/08 emc; ERP-32299  SPC factor not applied to OAN products
    03/04/08 emc; ERP-33094  Backordered component added to BOD kit cost
*******************************************************************************/

/*o NOTE: This include is called for nonstock components;
          also this include is doing a conversion between current special price
          cost product information stored in the b-icss buffer and history or
          existing line record special price cost product information stored
          in v- special costing variables.  It returns the required price and
          product cost in the history or document units.  */

/*d ********** Perform the G/L cost roll *****************************/
/*e Cost roll logic hard coded in OEETQL.P; chg there also */
/*d Not called from p-oeetll.i */
if not v-loadchg and (can-do("b,c":u,v-kitrollty) or v-kitcostfl) and
   not v-costoverfl then
do:

    /*d Calculate the v-prodcost for the component */
    if avail {&oeelk_m} and {&specnstype} = "n":u and avail b-icsc then
        v-prodcost = if {&costoverfl} = true then {&oeelk_m}.prodcost

                     else if v-icsmcost = "s":u then b-icsc.stndcost
                     else b-icsc.prodcost.
    else
        assign {p-oeetlc.i "b2-icsw."}.

    /*d Update the prodcost on the OEELK record for a BOD kit
        In line add mode, the product cost on the OEELK record is updated.
        In line change mode, the product cost on the OEELK record is
        updated if the product is a stocked component or special
        component without a tie.  If the product is a special component
        with a tie the product cost is not updated because the cost may
        have been overridden (i.e. from the PO RRAR).  If the kit is
        accessed in line change mode and the special component added, the
        OEELK product cost is updated because there cannot be a tie at
        this point. */

/*
    if g-operinits = "tah" and avail {&oeelk_m}  then do: 
      message avail {&oeelk_m} {&orderalttype} 
      {&costoverfl} {&specnstype} {&oeelk_m}.prodcost
      {&oeelk_m}.shipprod.
      pause.
    end.   
*/

    if avail {&oeelk_m} and {&orderalttype} = "@" and
      not {&costoverfl} and {&specnstype} = "S" then
      assign v-prodcost = {&oeelk_m}.prodcost.
 
 
    if avail {&oeelk_m} and {&costoverfl} = false then do:
       find b-{&oeelk_m} where recid(b-{&oeelk_m}) = recid({&oeelk_m})
       exclusive-lock no-error.

       /*d Convert the product cost back to the history, oeelk, special cost
           units from the current before loading into oeelk.  Only apply the
           line unit conversion factor to the oeelk.prodcost if this is a new
           cost being assigned.  If v-prodcost is simply a rewrite of what was
           on the oeelk before then the product cost is already correct per
           line unit. */
       if avail b-{&oeelk_m} then do:
           assign
               v-prodcost            = if v-maint-l = "c":u     and
                                         {&specnstype} = "s":u and
                                         {&orderalttype} <> ""
                                       then b-{&oeelk_m}.prodcost
                                       else v-prodcost *
                                            {speccost.gco
                                                &curricss = "vc-"
                                                &histicss = "v-"}
               b-{&oeelk_m}.prodcost = v-prodcost *
                                      if (v-maint-l = "c":u and
                                         {&specnstype} = "s":u and
                                         {&orderalttype} <> "" ) or
                                          v-speccostty <> ""
                                      then 1
                                      else v-conv.
       end. /* end of if avail b-oeelk */

    end. /* end of if avail oeelk */

    /*d Adjust the cost for any special costing factors; use the history
        product special costing factors as the v-prodcost is now in history
        units.  */
    if v-speccostty <> "" and {&specnstype} <> "n":u then
        assign
            v-prodcost = v-prodcost * v-csunperstk
            v-prodcost = if      v-speccostty = "h":u then v-prodcost / 100
                         else if v-speccostty = "t":u then v-prodcost / 1000
                         else v-prodcost.

    /*d Accumulate the cost of each component for a kit cost */
    s-cost = s-cost + round(v-prodcost * v-costqty,5).

end. /* end of if not v-loadchg and .... */

/*d Calculate the pricing cost for a price roll */
/*e always run, even if called from p-oeetll.i and always reset v-pdcost for
    oeetlp cost based pricing. Only overwrite v-pdcost with s-pdcost if
    doing cost roll */

/*d Calc v-pdcost */
if avail {&oeelk_m} and {&specnstype} = "n":u and avail b-icsc then
    v-pdcost   = (if {&costoverfl} = true then {&oeelk_m}.prodcost
                  else if v-iccosttype = "s":u then b-icsc.stndcost
                  else b-icsc.prodcost) *
                 (if b-icsc.speccostty = "" then 1
                  else b-icsc.csunperstk /
                  (if b-icsc.speccostty = "t":u then 1000
                   else if b-icsc.speccostty = "h":u then 100
                   else 1)).
else
    assign {p-pdcost.i "b2-icsw."}.

/*d Roll the price of each component into the kit */

/*d Called from p-oeetll.i */
if not v-loadchg and can-do("b,p":u,v-kitrollty) and
   not can-do("p,c,m,bp,lp":u,s-priceclty) and {&pricefl} = yes and
   not v-manprice then
do:

   /*d Load pricing variables based on component */
   assign
       v-baseprice = if avail {&oeelk_m} and {&specnstype} = "n":u and
                       avail b-icsc
                     then b-icsc.baseprice
                     else b2-icsw.baseprice
       v-listprice = if avail {&oeelk_m} and {&specnstype} = "n":u and
                       avail b-icsc
                     then b-icsc.listprice
                     else b2-icsw.listprice
       v-prodtype  = if avail {&oeelk_m} and {&specnstype} = "n":u and
                       avail b-icsc
                     then b-icsc.pricetype
                     else b2-icsw.pricetype
       s-prvendno  = if avail {&oeelk_m} and {&specnstype} = "n":u and
                       avail b-icsc
                     then b-icsc.vendno
                     else b2-icsw.arpvendno
       s-prline    = if avail {&oeelk_m} and {&specnstype} = "n":u and
                       avail b-icsc
                     then b-icsc.prodline
                     else b2-icsw.prodline
       s-prodcat   = if avail {&oeelk_m} and {&specnstype} = "n":u and
                       avail b-icsc
                     then b-icsc.prodcat
                     else b-icsp.prodcat
       s-prod      = if avail {&oeelk_m} and {&specnstype} = "n":u and
                       avail b-icsc
                     then b-icsc.catalog
                     else b2-icsw.prod
       v-stkunit   = if avail {&oeelk_m} and {&specnstype} = "n":u and
                       avail b-icsc
                     then {&unit}
                     else b-icsp.unitstock
       s-unit      = {&unit}.

   if v-spcconvertfl = yes then
       assign
           v-baseprice = v-baseprice * {specprc.gco &curricss = vc-
                                                    &histicss = v-}
           v-listprice = v-listprice * {specprc.gco &curricss = vc-
                                                    &histicss = v-}.

   if substring(g-ourproc,1,2) = "bp":u then run bpoeetla.p.

   assign
        sv-v-price    = v-price
        sv-v-errfl    = v-errfl
        v-pdrecno     = 0
        v-ncompqty    = v-compqty
        v-noqtyord    = o-totqtyord
        v-nostkqtyord = o-stkqtyord
        v-nsqtyord    = s-qtyord
        v-nvstkqtyord = v-stkqtyord
        v-compqty     = if v-compqty < 0 then - v-compqty
                        else v-compqty
        o-totqtyord   = if o-totqtyord < 0 then - o-totqtyord
                        else o-totqtyord
        o-stkqtyord   = if o-stkqtyord < 0 then - o-stkqtyord
                        else o-stkqtyord
        s-qtyord      = if s-qtyord < 0 then - s-qtyord
                        else s-qtyord
        v-stkqtyord   = if v-stkqtyord < 0 then - v-stkqtyord
                        else v-stkqtyord.

    if v-pdcustomfl then run oeetlp99.p(no, yes).

    else if (v-pricecd = 0 and v-disccd = 0) then
        v-price = if b-icsp.priceonty = "l":u then v-listprice
                  else v-baseprice.

    else run oeetlp.p (no, yes).

    assign
        v-compqty     = v-ncompqty
        o-totqtyord   = v-noqtyord
        o-stkqtyord   = v-nostkqtyord
        s-qtyord      = v-nsqtyord
        v-stkqtyord   = v-nvstkqtyord.

    /*d Adjust for special costing factors using the current product data. */
    if can-do("h,t":u,v-speccostty) then
    do:

        s-price = v-price.

        {p-oeetlm.i
            &speccostty     = "v-speccostty"
            &priceclty      = "s-priceclty"
            &priceonty      = "v-priceonty"
            &pdscrecid      = "v-idpdsc"
            &price          = "s-price"
            &pdrecno        = "v-pdrecno"
            &custpricecd    = "v-pricecd"}

        v-price = s-price.

    end. /* end of if can-do(speccostty) */

    /*d Accumulate the price for each component into a kit price after
        converting the price to the history or document special price costing
        units. This conversion should impact both v-price and s-price. */
    /*tb 25179 01/20/99 cm; Kit component price rollup incorrect if PDSC based
        on (c)ost or (m)argin. If this component price was calculated from a
        PDSC record that was based on cost or margin, then the price will be in
        stocking units and does not need to be converted from special units. */
    /*tb 25823 06/02/99 lbr; Removed specprc.gco from v-price */
    /*tb 26575 11/02/99 bp ; Add check of the prctype field in the SPC
        conversion for v-pricecalc.  The fact that the v-priceonty is "c" or
        "m" is not sufficient.  Must determine whether the PDSC record uses
        a hard coded dollar amount (pdsc.prctype field = "yes").*/
    /*tb t24039 02/07/01 lbr; Added rc,rm Rebated Cost, Rebated Margin */

    assign
        s-price       = v-price
        v-pricecalc   = v-price *
                            if v-speccostty = "" then 1
                            else v-csunperstk
        v-price       = sv-v-price

        v-errfl       = sv-v-errfl
        v-price       = v-price + (round((v-pricecalc * v-compqty),5) -
                           (if substring(g-ourproc,1,2) = "bp":u then 0
                            else round((v-pricecalc * s-discamt / 100 *
                                v-compqty),5))).


    /*d Update the OEELK price if the kit is a BOD kit */
    if avail {&oeelk_m} then do:

       find b-{&oeelk_m} where recid({&oeelk_m}) = recid(b-{&oeelk_m})
       exclusive-lock no-error.

       if avail b-{&oeelk_m} then
           assign
               b-{&oeelk_m}.pdrecno = v-pdrecno
               b-{&oeelk_m}.price   = v-pricecalc *
                                     (if v-speccostty <> "" then
                                         (1 / v-csunperstk)
                                      else v-conv)
               b-{&oeelk_m}.price   = if substring(g-ourproc,1,2) ne "bp":u
                                      then b-{&oeelk_m}.price -
                                          (b-{&oeelk_m}.price * s-discamt / 100)
                                      else b-{&oeelk_m}.price.

    end. /* end of if avail oeelk */
end. /* end of if not v-loadchg and .... */

/*d v-pdcost is in current special price costing units.  The net effect of this
    calcualtion is to reload v-pdcost to a stocking unit level.  Must be done
    after pricing. */
if vc-speccostty <> "" and {&specnstype} <> "n":u then
    assign
        v-pdcost = v-pdcost * vc-csunperstk
        v-pdcost = if      vc-speccostty = "h":u then v-pdcost / 100
                   else if vc-speccostty = "t":u then v-pdcost / 1000
                   else v-pdcost.

if can-do("b,c":u,v-kitrollty) and not v-costoverfl then
    s-pdcost = s-pdcost + round(v-pdcost * v-costqty,5).

/*User Hook*/
{a-oeetlp.z99 &user_bottom = "*"}