/* vaer.p 1.6 10/22/98 */
/*h*****************************************************************************
  PROCEDURE    : vaer
  DESCRIPTION  : VA - RRAR
  AUTHOR       : vjp
  DATE WRITTEN : 09/04/96
  CHANGES MADE :
    08/08/96 vjp; TB# 21254 (9A) Develop Value Add Module
    12/12/96 mtt; TB# 21254 (9A) Develop Value Add Module
    03/11/97 jms; TB# 7241 (5.3.5) Special Price/Cost Project - Added icss.gfi,
    speccost.gas, and speccost.gva
    10/04/97 vjp; TB# 23935 Tie special kit lines from oe and special VA
        component lines from IN sections during vaer.p
    10/22/97 vjp; TB# 23935 Fix find of oeelk and vaesl special orders
    02/05/98 kjb; TB# 24509 Alpha custno displays 'zzzzz' with customer #
    06/19/98 lmk; TB# 20381 Need processing per warehouse
    08/24/98 bm;  TB# 24812 Add Rush Functionality type "IN" Lines.
        Added rushfl as 2nd last param when run vaetcret
        Added option (p-procrns) to select special, rush, both, neither
        for  OE and VA orders.
    09/04/98 bm;  TB# 25282 Don't pick up special lines if stage is not 1 or 2
    10/05/98 kjb; TB#  9731 Add a second nonstock description.
    04/09/99 sbr; TB# 25978 OE BL special kit item shows on VAER
    04/19/99 sbr; TB# 25921 OAN items print with no orders
    03/22/01 gp;  TB# e7065 OAN prod with demand not replenished
    11/29/01 doc; TB# e8227 Copy VA order to new VA order in OEET. Added
        1st 2 parameters passed to vaetcret.p
    04/02/03 mms; TB# e16707 Only pull non bo section.
    05/11/05 ajw; TB# e21883 Include DNR products if the option is set.
*******************************************************************************/

{p-rptbeg.i &comsasc = "/*"
            &comsasa = "/*"
            &comsapb = "/*"}

{g-ic.i "new"}
{g-conv.i}
{vaer.lva "new"}
{va.gva}

/*tb 7241 (5.3.5) 03/11/97 jms; Special Price/Cost Project */
{speccost.gva "new shared"}

def buffer b-oeeh for oeeh.

/* Range and options variables */
def var b-whse        like icsd.whse                        no-undo.
def var e-whse        like icsd.whse                        no-undo.
def var p-usageprt    as logical                            no-undo.
def var p-specialprod as logical                            no-undo.
def var p-dnrfl       as logical                            no-undo.
def var p-procrns     as c format "x"                       no-undo.

/*tb 24812 08/24/98 bm; Select rush and/or special. Added f-opterr */
/* Options Error Condition Frame */
form
    s-opterr    as c format "x(100)"  at 5
with frame f-opterr no-box no-labels no-underline down width 132.

 

/*>>>>>>>>>>>>>>>>>>>>>>>>>Internal Procedures<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/

procedure error-print:

    /* Passed buffers / variables */

    /* Define (redirect) local buffers to be used for primary tables */

    /*Local buffers / variables */

    /* Error Condition Frame */
    form
        s-error     as c format "x(100)"  at 5
    with frame f-except no-box no-labels no-underline down width 132.

    {f-blank.i}

    /*d Assign the error message */
    assign
        s-type = if s-vamsg = "" then "**" + s-type
                 else s-type

            /*d Product not set up in ICSP */
        s-error = if v-errfl = "a" then
                     s-vamsg + s-type + s-prod +
                     " Not Set Up in Product Setup **"

                  /*d Invalid units */
                  else if v-errfl = "b" then
                     s-vamsg + s-type  + s-prod +
                     " Units Not Set Up in Unit Tables - ICSEU or SASTT **"

                  /*d Product not active */
                  else if v-errfl = "c" then
                     s-vamsg + s-type + s-prod + " Not Active **"

                  else if v-errfl = "l" then
                " ** The Order Qty Was Decreased by the Quantity Ordered for" +
                    " Suspended Special Orders ** "

                  /*d Invalid Message */
                  else "** INVALID **".

    display s-error with frame f-except.
    down with frame f-except.

    display with frame f-blank.
    down with frame f-blank.

end procedure.  /* error-print */

procedure check-oe-specials:

    /* Passed buffers / parameters */

    def param buffer pb-icsp for icsp.
    def param buffer pb-icsw for icsw.

    /* Define (redirect) local buffers to be used for primary tables */

    def buffer oeel                  for oeel.
    def buffer oeeh                  for oeeh.
    def buffer oeelk                 for oeelk.

    /* Local buffers / variables */

    def var v-first       as logical initial yes            no-undo.
    def var v-vanetavail  as decimal format "zzzzzzzz9.99-" no-undo.

    def var s-error       as c format "x(100)"              no-undo.

    {vaers.lfo}
    {f-under.i}

    /*d Calculate the net available qty for the kit */
    assign
        v-vanetavail = {p-poavl.i pb-}.

    /*o*********** Find all special order prebuilt kits in OE ****************/
    /*d performance - added invoicedt = ? to follow key */
    /*d Not checking oeel.transtype */

    loop1:
    /*tb 20381 06/19/98 lmk; Move oeel.whse up index to improve efficiency */
    /*tb 24812 08/24/98 bm; Select rush and/or special using p-procrns */
    for each oeel use-index k-fill where
             oeel.cono        = g-cono          and
             oeel.statustype  = "a"             and
             oeel.whse        = g-whse          and
             oeel.invoicedt   = ?               and
             oeel.shipprod    = pb-icsp.prod    and
             oeel.specnstype  = "s"             and
             oeel.bono        = 0               and
             oeel.transtype  <> "qu"            and
             oeel.ordertype   = ""              and
             oeel.returnfl    = no              and
             oeel.kitfl       = no              and
             oeel.qtyship     < oeel.qtyord     and
             (p-procrns       = "b"          or
              (p-procrns      = "s"       and
               not oeel.rushfl)              or
              (p-procrns      = "r"       and
               oeel.rushfl))
    exclusive-lock:
        do for oeeh:

            {w-oeeh.i oeel.orderno oeel.ordersuf no-lock}
            if not avail oeeh then next loop1.

            /*tb 25282 09/04/98 bm;  Don't pick up special lines
                if stage is not 1 or 2 */
            if avail oeeh and not can-do("1,2",string(oeeh.stagecd)) then do:
                next loop1.
            end. /* if oeeh.stagecd <> 1 or 2 */

            /*d Don't pick up special lines on suspended orders */
            
            if avail oeeh then do:
              find notes where                        
                      notes.cono = oeeh.cono       and     
                      notes.notestype    = "zz"    and       
                      notes.primarykey   = "slsmx" and       
                      notes.secondarykey =                   
                      string(oeeh.custno,"99999999999") +  
                      oeeh.shipto               and        
                      notes.printfl2     =  yes              
                      no-lock no-error.                      
                    
                    if avail notes then 
                      assign v-compfc = "CF".
                    /*
                    else                      1
                      assign v-compfc = "NOt".
                    */
                   /* display v-compfc with frame f-compfc.     */               
              end.  /* cmpfc */
            
            
            if avail oeeh and oeeh.updtype = "s"
            then do:

                v-ksuspendqty = v-ksuspendqty + (oeel.stkqtyord -
                                    oeel.stkqtyship).
                next loop1.

              end. /* end if oeeh.updtype = s */

            /*d Assign display variables */
            /*d Make changes for VA Order Demand */
            /*tb 24509 02/05/98 kjb; Alpha custno issue; changed s-custwhse
                from string(oeel.custno,"zzzzzzzzzzz9") to
                {c-asignc.i oeel.custno} */
            
            assign
                v-errfl     = ""
                s-orderno   = oeel.orderno
                s-ordersuf  = oeel.ordersuf
                s-special   = "o"
                s-jobno     = oeel.jobno
                s-approve   = if avail oeeh then  oeeh.approvty else ""
                s-custwhse  = {c-asignc.i oeel.custno}
                s-qtyord    = oeel.qtyord - oeel.qtyship
                s-qtyship   = s-qtyord.

            /*d Create the records */
            if p-updatefl = yes then do:
               /*tb 24812 08/24/98 bm; added oeel.rushfl as 2nd last param*/
                /*tb 9731 10/05/98 kjb; Added a new parameter for the second
                    nonstock description - 14th in list */
                /*e Not copying from a VA order, so set 1st 2 params to 0. */
                run vaetcret.p(0,
                               0,
                               pb-icsw.prod,
                               pb-icsw.whse,
                               "",
                               oeel.qtyord - oeel.qtyship,
                               oeel.unit,
                               oeel.botype,
                               oeeh.approvty,
                               if oeeh.orderdisp = "j" then oeel.reqshipdt
                                   else oeeh.reqshipdt,
                               if oeeh.orderdisp = "j" then oeel.promisedt
                                   else oeeh.promisedt,
                               0,
                               0,
                               "",
                               "",
                               "",
                               pb-icsp.prodcat,
                               "o",
                               oeel.orderno,
                               oeel.ordersuf,
                               oeel.lineno,
                               0,
                               oeel.rushfl,
                               no).
            end.  /* if p-updatefl = yes then do */
            /*d Display the line */
            if v-first then
            do:
                display with frame f-specsh.
                display with frame f-under.
                down with frame f-under.

            end. /* end of if v-first */

            assign
                v-first   = no.

            {vaers.ldi &file = "oeel"}

            v-oevascnt = v-oevascnt + (oeel.stkqtyord -
                                       oeel.stkqtyship).

        
        end. /* end of do for oeeh */
    end. /* end of for each oeel - loop1 */

end procedure.  /* check-oe-specials */

/*tb 23935 10/07/97 vjp; Create ties to special order kit component
    lines */
procedure check-kit-specials:

    /* Passed buffers / parameters */

    def param buffer pb-icsp for icsp.
    def param buffer pb-icsw for icsw.

    /* Define (redirect) local buffers to be used for primary tables */

    def buffer oeel                  for oeel.
    def buffer oeeh                  for oeeh.
    def buffer oeelk                 for oeelk.

    /* Local buffers / variables */

    def var v-first       as logical initial yes            no-undo.
    def var v-vanetavail  as decimal format "zzzzzzzz9.99-" no-undo.

    def var s-error       as c format "x(100)"              no-undo.

    {vaers.lfo}
    {f-under.i}

    /*d Calculate the net available qty for the kit */
    assign
        v-vanetavail = {p-poavl.i pb-}.

    /*o*********** Find all special order prebuilt kits in OE ****************/
    /*d performance - added invoicedt = ? to follow key */
    /*d Not checking oeel.transtype */

    loop1:

     for each oeelk no-lock use-index k-specns where
         oeelk.cono            = g-cono                and
         oeelk.statustype      = "a"                   and
         oeelk.specnstype      = "s"                   and
         oeelk.whse            = g-whse                and
         oeelk.shipprod        = pb-icsp.prod          and

         oeelk.ordertype       = "o"                   and

         oeelk.qtyship         < oeelk.qtyord          and
         oeelk.orderalttype    = "",

    first oeel no-lock use-index k-oeel where
          oeel.cono           = g-cono         and
          oeel.orderno        = oeelk.orderno  and
          oeel.ordersuf       = oeelk.ordersuf and
          oeel.lineno         = oeelk.lineno   and
          oeel.bono           = 0              and
          oeel.returnfl       = no             and
          oeel.botype         <> "d"           and
          oeel.transtype      <> "qu"

:

        do for oeeh:

            {w-oeeh.i oeel.orderno oeel.ordersuf no-lock}
            if not avail oeeh then next loop1.

            /*tb 25978 04/09/99 sbr; Don't pick up special lines if stage is
                not 1 or 2. */
            if not can-do("1,2",string(oeeh.stagecd)) then next loop1.

            /*d Don't pick up special lines on suspended orders */

            if avail oeeh and oeeh.updtype = "s"
            then do:

                v-ksuspendqty = v-ksuspendqty + (oeelk.stkqtyord -
                                    oeelk.stkqtyship).
                next loop1.

              end. /* end if oeeh.updtype = s */

            /*d Assign display variables */
            /*d Make changes for VA Order Demand */
            assign
                v-errfl     = ""
                s-orderno   = oeel.orderno
                s-ordersuf  = oeel.ordersuf
                s-special   = "o"
                s-jobno     = oeel.jobno
                s-approve   = if avail oeeh then  oeeh.approvty else ""
                s-custwhse  = string(oeel.custno,"zzzzzzzzzzz9")
                s-qtyord    = oeelk.qtyord - oeelk.qtyship
                s-qtyship   = s-qtyord.

            /*d Create the records */
            if p-updatefl = yes then do:

                /*tb 24812 08/24/98 bm; added oeel.rushfl as 2nd last param*/
                /*tb 9731 10/05/98 kjb; Added a new parameter for the second
                    nonstock description - 14th in list */
                /*e Not copying from a VA order, so set 1st 2 params to 0. */
                run vaetcret.p(0,
                               0,
                               pb-icsw.prod,
                               pb-icsw.whse,
                               "",
                               oeelk.qtyord - oeelk.qtyship,
                               oeelk.unit,
                               oeel.botype,
                               oeeh.approvty,
                               if oeeh.orderdisp = "j" then oeel.reqshipdt
                                   else oeeh.reqshipdt,
                               if oeeh.orderdisp = "j" then oeel.promisedt

                                   else oeeh.promisedt,
                               0,
                               0,
                               "",
                               "",
                               "",
                               pb-icsp.prodcat,
                               "o",
                               oeel.orderno,
                               oeel.ordersuf,
                               oeel.lineno,
                               oeelk.seqno,
                               oeel.rushfl,
                               no).
            end.  /* if p-updatefl = yes then do */

            /*d Display the line */
            if v-first then
            do:
                display with frame f-specsh.
                display with frame f-under.
                down with frame f-under.

            end. /* end of if v-first */

            assign
                v-first   = no.

            {vaers.ldi &file = "oeelk"}

            v-oevascnt = v-oevascnt + (oeelk.stkqtyord -
                                       oeelk.stkqtyship).
        end. /* end of do for oeeh */

    end. /* end of for each oeel - loop1 */

end procedure.  /* check-kit-specials */

/*tb 23935 10/07/97 vjp; Create ties to special order VA inventory section
    lines */
procedure check-va-specials:

    /* Passed buffers / parameters */

    def param buffer pb-icsp for icsp.
    def param buffer pb-icsw for icsw.

    /* Define (redirect) local buffers to be used for primary tables */

    def buffer vaesl                 for vaesl.
    def buffer vaes                  for vaes.
    def buffer vaeh                  for vaeh.

    /* Local buffers / variables */

    def var v-first       as logical initial yes            no-undo.
    def var v-vanetavail  as decimal format "zzzzzzzz9.99-" no-undo.

    def var s-error       as c format "x(100)"              no-undo.

    {vaers.lfo}
    {f-under.i}

    /*d Calculate the net available qty for the kit */
    assign
        v-vanetavail = {p-poavl.i pb-}.

    /*o*********** Find all special order VA Inventory components*************/

    loop1:

    /*tb 24812 08/24/98 bm; Select rush and/or special using p-procrns */
    for each vaesl no-lock use-index k-specns where
         vaesl.cono            = g-cono                and
         vaesl.completefl      = no                    and
         vaesl.nonstockty      = "s"                   and
         vaesl.whse            = g-whse                and
         vaesl.orderalttype    = ""                    and
         vaesl.boseqno         = 0                     and
         vaesl.qtyship         < vaesl.qtyord          and
         vaesl.shipprod        = pb-icsp.prod          and
         (p-procrns            = "b"               or
          (p-procrns           = "s"           and
           not vaesl.rushfl)                       or
          (p-procrns           = "r"           and
           vaesl.rushfl)),

    first vaes no-lock use-index k-vaes where
          vaes.cono           = g-cono         and
          vaes.vano           = vaesl.vano     and
          vaes.vasuf          = vaesl.vasuf    and
          vaes.seqno          = vaesl.seqno

:

        do for vaeh:
            {vaeh.gfi &vano  = vaesl.vano
                      &vasuf = vaesl.vasuf
                      &lock  = "no"}

            if not avail vaeh or vaeh.transtype = "qu" then
                leave loop1.
            /*d Assign display variables */
            /*d Make changes for VA Order Demand */
            assign
                v-errfl     = ""
                s-orderno   = vaesl.vano
                s-ordersuf  = vaesl.vasuf
                s-special   = "f"
                s-approve   = if avail vaeh then  vaeh.approvty else ""
                s-custwhse  = vaesl.whse
                s-qtyord    = vaesl.qtyord - vaesl.qtyship
                s-qtyship   = s-qtyord.

            /*d Create the records */
            if p-updatefl = yes then do:

                /*tb 24812 08/24/98 bm; added vaesl.rushfl
                    as 2nd last param*/
                /*tb 9731 10/05/98 kjb; Added a new parameter for the second
                    nonstock description - 14th in list */
                /*e Not copying from a VA order, so set 1st 2 params to 0. */
                run vaetcret.p(0,
                               0,
                               pb-icsw.prod,
                               pb-icsw.whse,
                               "",
                               vaesl.qtyord - vaesl.qtyship,
                               vaesl.unit,
                               if vaesl.directfl = yes then "d"
                               else "",
                               vaeh.approvty,
                               vaeh.reqshipdt,
                               vaeh.promisedt,
                               0,
                               0,
                               "",
                               "",
                               "",
                               pb-icsp.prodcat,
                               "f",
                               vaesl.vano,
                               vaesl.vasuf,
                               vaesl.lineno,
                               vaesl.seqno,
                               vaesl.rushfl,
                               no).
            end.  /* if p-updatefl = yes then do */

            /*d Display the line */
            if v-first then
            do:
                display with frame f-specsh.
                display with frame f-under.
                down with frame f-under.

            end. /* end of if v-first */

            assign
                v-first   = no.

            {vaers.ldi &file = "vaesl"}

            v-oevascnt = v-oevascnt + (vaesl.stkqtyord -
                                       vaesl.stkqtyship).
        end. /* end of do for oeeh */

    end. /* end of for each oeel - loop1 */

end procedure.  /* check-kit-specials */

procedure check-wt-specials:

    /* Passed buffers / parameters */

    def param buffer pb-icsp for icsp.
    def param buffer pb-icsw for icsw.

    /* Define (redirect) local buffers to be used for primary tables */

    def buffer wtel for wtel.
    def buffer wteh for wteh.

    /* Local buffers / variables */
    def var v-first       as l init yes                no-undo.

    /* Unit Conversion */
    {g-conv.i}

    def var s-error       as c format "x(100)"                  no-undo.

    {vaers.lfo}

    {f-under.i}

    /*d Calculate the net available qty for the va */
    assign
        v-knetavail = {p-poavl.i pb-}.

    /*o**** Find all special orders prebuilt kits in WT ******/
    loop2:
    for each wtel use-index k-specns where
             wtel.cono       = g-cono       and
             wtel.statustype = "a"          and
             wtel.nonstockty = "s"          and
             wtel.shipfmwhse = g-whse       and
             wtel.ordertype  = ""           and
             wtel.shipprod   = pb-icsp.prod and
             wtel.approvety  = "y"          and
             wtel.qtyship    < wtel.qtyord  and
             wtel.vendno     = 0            and
             wtel.bono       = 0
    exclusive-lock:

        do for wteh:

            {w-wteh.i wtel.wtno wtel.wtsuf no-lock}
            if not avail wteh then next loop2.
            

            find first wtelo use-index k-wtelo where wtelo.cono = 1 and
                                            wtelo.wtno = wtel.wtno and
                                            wtelo.wtsuf = wtel.wtsuf and
                                            wtelo.lineno = wtel.lineno
                                            no-lock no-error.
        if avail wtelo and wtelo.ordertype = "o" then do:
           find first b-oeeh use-index k-oeeh where b-oeeh.cono = 1 and
                                       b-oeeh.orderno = wtelo.orderaltno and
                                       b-oeeh.ordersuf = wtelo.orderaltsuf
                                       no-lock no-error.
                                                  
                 if avail b-oeeh then 
                    assign s-approve = b-oeeh.approvty.
                 else
                    assign s-approve = wtel.approvety. 
                    
                find notes where                        
                      notes.cono = b-oeeh.cono       and     
                      notes.notestype    = "zz"    and       
                      notes.primarykey   = "slsmx" and       
                      notes.secondarykey =                   
                      string(b-oeeh.custno,"99999999999") +  
                      b-oeeh.shipto               and        
                      notes.printfl2     =  yes              
                      no-lock no-error.                      
                    
                    if avail notes then 
                      assign v-compfc = "CF".
                    /*
                    else 
                      assign v-compfc = "NOt".
                    */
                  /* display v-compfc with frame f-compfc.   */                   


                    
        end.

            
            assign
                v-errfl     = ""
                s-orderno   = wtel.wtno
                s-ordersuf  = wtel.wtsuf
                s-special   = "t"
                s-custwhse  = wtel.shipfmwhse
            /*     s-approve   = wtel.approvety   smaf */
                s-qtyord    = wtel.qtyord - wtel.qtyship
                s-qtyship   = s-qtyord.

            /*d Create the va and component records */
            if p-updatefl = yes then

                /*tb 24812 08/24/98 bm; added no for rushfl as 2nd last param*/
                /*tb 9731 10/05/98 kjb; Added a new parameter for the second
                    nonstock description - 14th in list */
                /*e Not copying from a VA order, so set 1st 2 params to 0. */
                run vaetcret.p(0,
                               0,
                               pb-icsw.prod,
                               pb-icsw.whse,
                               "",
                               wtel.qtyord - wtel.qtyship,
                               wtel.unit,
                               "y",
                               "y",
                               wteh.reqshipdt,
                               wteh.reqshipdt,
                               0,
                               0,
                               "",
                               "",
                               "",
                               pb-icsp.prodcat,
                               "t",
                               wtel.wtno,
                               wtel.wtsuf,
                               wtel.lineno,
                               0,
                               no,
                               no).

            /*d Display the Special Line */
            if v-first then
            do:

                display with frame f-specsh.
                display with frame f-under.
                down with frame f-under.

            end. /* end of if v-first */

            assign
                v-first   = no.

            {vaers.ldi &file = "wtel"}

            v-wtvascnt = v-wtvascnt + (wtel.stkqtyord -
                                       wtel.stkqtyship).

        end. /* do for wteh */
    end. /* for each wtel */
end procedure.  /* check-wt-specials */

procedure load-ao-sapb-options:

    /* Passed buffers / parameters */

    /* Define (redirect) local buffers to be used for primary tables */

    def buffer sapb for sapb.
    def buffer sasa for sasa.
    def buffer sasc for sasc.

    /* Local buffers /variables */

    do for sapb, sasa, sasc:

        find sapb where recid(sapb) = v-sapbid no-lock no-error.

        /*d Set variable for product cost to assign to records */
        {w-sasc.i no-lock}

        {w-sasa.i no-lock}

        /*d Make changes for VA Order Demand */
        if avail sasc then
            assign
                v-icglcost = sasc.icglcost.

        /*o Check the Module Purchase Flag */
        if not sasa.modvafl then
        do:

            hide all no-pause.
            {p-rptend.i}
            run err.p(1124).
            return.

        end. /* end of if not sasa.modvafl */

        /*o Load SAPB parameters */
        assign
             b-whse        = substring(sapb.rangebeg[1],1,4)
             e-whse        = substring(sapb.rangeend[1],1,4)
             p-updatefl    = if sapb.optvalue[1] = "C" then yes else no
             p-inits       = sapb.optvalue[2]
             p-usageprt    = if sapb.optvalue[3] = "yes" then yes else no
             p-specialprod = if sapb.optvalue[4] = "yes" then yes else no
             p-dnrfl       = if sapb.optvalue[5] = "yes" then yes else no
             p-procrns     = sapb.optvalue[6]
             v-reportnm    = sapb.reportnm
             s-vamsg       = if p-updatefl = yes
                             then
                                 "** NO VA ORDER CREATED: "
                             else
                                 "".

        /*tb 24812 08/24/98 bm; Select rush and/or special using p-procrns */
        if p-specialprod = yes and p-procrns = "n" then do:
            hide all no-pause.
            {p-rptend.i}
            s-opterr = "** Option to Process Only Spec/Rush " +
                       "Conflicts with Option To Process None **".
            display s-opterr with frame f-opterr.
            down with frame f-opterr.
            return.
        end.

    end. /* end of do for sasa, sasc */

end procedure. /* load-ao-sapb-options */

procedure process-rrar:

    /* Passed buffers / parameters */

    /* Define (redirect) local buffers to be used for primary tables */
    /*tb 7241 (5.3.5) 03/12/97 jms; Special Price/Cost Project - Added
        buffer for icss */

    def buffer sasta      for sasta.
    def buffer icsp       for icsp.
    def buffer icsd       for icsd.
    def buffer icsw       for icsw.
    def buffer icseu      for icseu.
    def buffer icswu      for icswu.
    def buffer icss       for icss.

    /* Local buffers / variables */

    def buffer b-icsp     for icsp.

    def        var k             as int                                no-undo.
    def        var v-first       as log                                no-undo.
    def        var v-kconv       as de                                 no-undo.
    def        var s-netavail    as dec format "zzzzzzzz9.99-"         no-undo.
    def        var s-desc        as c                                  no-undo.
    def        var s-error       as c format "x(100)"                  no-undo.

    {vaer.lfo}

    /****** Main ******/
    view frame f-footer.

    /*o Find each VA Product in ICSP */
    main:
    for each icsd no-lock use-index k-icsd  where
             icsd.cono       = g-cono and
             icsd.whse      >= b-whse and
             icsd.whse      <= e-whse,

        each icsw no-lock use-index k-vendprod where
             icsw.cono      = g-cono    and
             icsw.whse      = icsd.whse and
             icsw.arptype   = "f"       and
             icsw.statustype ne "d"     and
             ( p-dnrfl = yes or
             (p-dnrfl = no and icsw.statustype ne "x") ),

        first icsp no-lock use-index k-icsp where
              icsp.cono         = g-cono    and

              icsp.prod         = icsw.prod and
              icsp.statustype   = "a"       and
              icsp.kittype      = ""

    :
        g-whse = icsd.whse.

        /*tb 7241 (5.3.5) 03/11/97 jms; Special Price/Cost Project - Find
            the icss and assign the speccost variables */
        if avail icsp and avail icsw then do:
            {icss.gfi &prod        = icsp.prod
                      &icspecrecno = icsp.icspecrecno
                      &lock        = "no"}

            {speccost.gas}

        end. /* if avail icsp */

        /*d Add assign of v-ksuspendqty */
        assign
            v-ksuspendqty = 0
            v-errfl       = ""
            v-oevascnt    = 0
            v-wtvascnt    = 0.

        /*d************ Special orders for OE **********************/
        run check-oe-specials(buffer icsp,
                              buffer icsw).

       /*tb 23935 10/07/97 vjp; Create ties to special order kit component
           lines */
       /*d************ Special kit components for OE **********************/
        run check-kit-specials(buffer icsp,
                               buffer icsw).

       /*tb 23935 10/07/97 vjp; Create ties to special order VA inventory
           lines */
       /*d************ Special kit components for OE **********************/
        run check-va-specials(buffer icsp,
                               buffer icsw).

        /*d************* Specials for WT ************************/

        run check-wt-specials(buffer icsp,
                              buffer icsw).

        /*d Go to the next product if only special order items were requested */
        if p-specialprod then next main.

        /*Process the Stocked and OAN prod with demand
          Additionally, if the option to process DNR products("X") is
          set, process those as well.  NOTE:  These DNR products
          are qualified in the main for each, so there is no
          need to check the Option here, just checking the value in the DB.*/
        if can-do("s,x":u,icsw.statustype) or
            (icsw.statustype = "o":u and
             (icsw.qtybo      > 0   or
              icsw.qtydemand  > 0))
        then do:

            /*d Determine if need to replenish */
            /*d Add assign of v-kqtybld */
            /*d Subtract special lines on suspended orders */
            /*d Initialize variables */
            v-knetavail = {p-poavl.i}.

            assign
                s-qtyonorder = icsw.qtyonorder /* save off for display */
                v-kicswrecid = recid(icsw)
                v-knetavail  = {p-poavl.i}
                v-stkqtyord  = if p-updatefl = yes then
                                   if icsw.orderpt - v-knetavail > 0 then
                                       icsw.linept - v-knetavail -
                                       v-ksuspendqty
                                   else 0
                               else if icsw.orderpt - v-knetavail -
                                        v-oevascnt - v-wtvascnt > 0 then
                                        icsw.linept - v-knetavail -
                                        v-oevascnt - v-wtvascnt -
                                        v-ksuspendqty
                                    else 0
                v-stkqtyship = v-stkqtyord
                s-special    = ""
                v-errfl      = "" /* clear out from special processing */
                {p-netavl.i}

            if (v-stkqtyord = 0) or
                (v-knetavail > icsw.orderpt)
            then next.

            /*d Find the procuct in ICSP */
            find b-icsp use-index k-icsp where
                 b-icsp.cono    = g-cono    and
                 b-icsp.prod    = icsp.prod no-lock no-error.

            /*d Component not set up in ICSP */
            v-errfl = if not avail b-icsp then "a"

                      else if b-icsp.statustype = "i" then "c"

                      else v-errfl.
            /*d Check Component Units */
            if v-errfl = "" then do:

               {p-unit.i &unitstock    =   b-icsp.unitstock
                         &unitconvfl   =   b-icsp.unitconvfl
                         &unit         =   icsp.unitstock
                         &desc         =   s-desc
                         &conv         =   v-kconv
                         &confirm      =   confirm
                         &prod         =   icsp.prod}

               if not confirm then v-errfl = "b".

            end.

            /*d**** Print the product if error exists ******/
            if v-errfl <> "" then do:

                 assign
                    s-prod    = icsp.prod.
                 /*d Print the va product */
                 {vaer.ldi}

                 /*d Print any exceptions that exist */
                 run error-print.
                 next.

            end. /* end of if v-errfl <> 0 */

            if p-updatefl = yes then do:

                /*tb 24812 08/24/98 bm; added no for rushfl as 2nd last param*/
                /*tb 9731 10/05/98 kjb; Added a new parameter for the second
                    nonstock description - 14th in list */
                /*e Not copying from a VA order, so set 1st 2 params to 0. */
                run vaetcret.p(0,
                               0,
                               icsw.prod,
                               icsw.whse,
                               "",
                               v-stkqtyord,
                               "EACH",
                               "y",
                               "y",
                               ?,
                               ?,
                               0,
                               0,
                               "",
                               "",
                               "",
                               icsp.prodcat,
                               "",
                               0,
                               0,
                               0,
                               0,
                               no,
                               no).
            end.

        end. /* end of if icsw.statustype = s */

        /*tb 25921 04/19/99 sbr; Added "else next." in order to prevent the
            products with statustype ne "s" from being processed here since
            they have already been processed as specials. */
        else next.

        s-msg = if icsw.statustype = "o" and
                    icsw.qtybo     > 0   or
                    icsw.qtydemand > 0 then
                        "** The Order Quantity Covers Special" +
                        " Order/Kit Components **"
                else "".

        /*d Display the ordering information */
        {vaer.ldi}

        /*o Display usage information */
        if p-usageprt then
        do:

            display skip with frame f-blank.
            down with frame f-blank.
            v-conv = 1.

            /* show usage history */
            {a-urrar.i}

            display skip with frame f-blank.
            down with frame f-blank.

        end. /* end of if p-usageprt */

    end. /* for each icsd, .. */

    page.
    {p-norec.i}
    hide all no-pause.
    {p-rptend.i}
end procedure. /* process-rrar */

/*tb 24812 08/24/98 bm; Select rush and/or special.
    Do not run process-rrar if invalid option combination */
main1:
do:
    run load-ao-sapb-options.
    if s-opterr = "" then
    run process-rrar.
end.