/* SX 55 */
/* vaeth.las 1.1 01/03/98 */
/* vaeth.las 1.1 07/01/96 */
/*h****************************************************************************
  INCLUDE      : vaeth.las
  DESCRIPTION  : Value Add Entry Transactions - Header Screen Assign Include
  USED ONCE?   : yes
  AUTHOR       : mtt
  DATE WRITTEN : 07/01/96
  CHANGES MADE :
    07/01/96 mtt; TB# 21254 (2A) Develop Value Add Module
    04/24/03 lcb; TB# t14244 8Digit Ord No Project
    06/04/03 sbr; TB# e15216 Add taken by or created by initials
******************************************************************************/

assign
     s-vareqshipdt  = {&buf}vaeh.reqshipdt
     s-varefer      = {&buf}vaeh.refer
     s-vacustprod   = {&buf}vaeh.custprod
     s-vaestcost    = {&buf}vaeh.estcost
     s-vawriteofffl = {&buf}vaeh.writeofffl

     s-vapromisedt  = {&buf}vaeh.promisedt
     s-vaestcompdt  = {&buf}vaeh.estcompdt
     s-varevno      = {&buf}vaeh.revno
     s-vaprevvano   = {&buf}vaeh.prevvano
     s-vaapprovty   = {&buf}vaeh.approvty
     s-vauser1      = {&buf}vaeh.user1
     s-vauser2      = if num-entries({&buf}vaeh.user2,"-") <> 0 then 
                         {&buf}vaeh.user2
                          /*
                         entry(1,{&buf}vaeh.user2,"-") + "-" +
                         left-trim(entry(2,{&buf}vaeh.user2,"-"),"0")
                         */
                      else 
                         {&buf}vaeh.user2
     
     
     
/*     s-vauser2      = {&buf}vaeh.user2 */
     s-vacreatedby  = {&buf}vaeh.createdby
     s-vareceiptdt  = {&buf}vaeh.receiptdt

     s-pndinvamt    = {&buf}vaeh.pndinvamt
     s-wipinvamt    = {&buf}vaeh.wipinvamt

     s-pndextrnamt  = {&buf}vaeh.pndextrnamt
     s-wipextrnamt  = {&buf}vaeh.wipextrnamt

     s-pndintrnest  = {&buf}vaeh.pndintrnest

     s-pndintrnamt  = {&buf}vaeh.pndintrnamt
     s-wipintrnamt  = {&buf}vaeh.wipintrnamt

     s-pndaddons    = {&buf}vaeh.pndaddons
     s-wipaddons    = {&buf}vaeh.wipaddons

     s-pndinvinamt  = {&buf}vaeh.pndinvinamt * -1
     s-wipinvinamt  = {&buf}vaeh.wipinvinamt * -1

     {vaethtot.las &pndtotal     = "s-pndtotal"
                   &wiptotal     = "s-wiptotal"
                   &pndintrndsp  = "s-pndintrndsp"
                   &pndintrnty   = "s-pndintrnty"
                   &vaehprefix   = "{&buf}vaeh."}

     s-vaprodcost   = {&buf}vaeh.prodcost
     s-vajrnlno     = {&buf}vaeh.jrnlno.

find first vaelo use-index k-vaelo where
           vaelo.cono      = g-cono and
           vaelo.vano      = g-vano and
           vaelo.vasuf     = g-vasuf
no-lock no-error.

if avail vaelo then do:
    if vaelo.ordertype = "o" then do:
        {w-oeeh.i vaelo.orderaltno vaelo.orderaltsuf no-lock}
        if avail oeeh then do:
           {w-arsc.i oeeh.custno no-lock}
           {w-oeel.i oeeh.orderno oeeh.ordersuf vaelo.linealtno no-lock}
        end.
    end.
    else
    if vaelo.ordertype = "f" then do:
        {vaeh.gfi &vano  = "vaelo.orderaltno"
                  &vasuf = "vaelo.orderaltsuf"
                  &lock  = "no"
                  &buf   = "{&tiebuf}"}
    end.
    else
    if vaelo.ordertype = "t" then
        {w-wteh.i vaelo.orderaltno vaelo.orderaltsuf no-lock}

end.

assign
     s-tietype      = if avail vaelo
                      then
                          vaelo.ordertype
                      else ""
     s-tieorderno   = if avail vaelo
                      then
                          string(vaelo.orderaltno,"zzzzzzz9")
                      else ""
     s-tiedash      = if avail vaelo
                      then
                          "-"
                      else ""
     s-tieordersuf  = if avail vaelo
                      then
                          string(vaelo.orderaltsuf,"99")
                      else ""
     s-tienotesfl   = if avail vaelo
                      then
                          if vaelo.ordertype = "o"
                          then
                              if avail oeeh
                              then
                                  oeeh.notesfl
                              else ""
                          else
                          if vaelo.ordertype = "f"
                          then
                              if avail {&tiebuf}vaeh
                              then
                                  {&tiebuf}vaeh.notesfl
                              else ""
                          else
                          if vaelo.ordertype = "t"
                          then
                              if avail wteh
                              then
                                  wteh.notesfl
                              else ""
                          else ""
                      else ""
     s-tielineno    = if avail vaelo
                      then
                          string(vaelo.linealtno,"zzz9")
                      else ""
     s-tieseqno     = if avail vaelo
                      then
                          string(vaelo.seqaltno,"zz9")
                      else ""

     s-tiestage     = if avail vaelo
                      then
                          if vaelo.ordertype = "o"
                          then
                              if avail oeeh
                              then
                                  v-stage[oeeh.stagecd + 1]
                              else ""
                          else
                          if vaelo.ordertype = "f"
                          then
                              if avail {&tiebuf}vaeh then
                                  v-vaehstage[{&tiebuf}vaeh.stagecd + 1]
                              else ""
                          else
                          if vaelo.ordertype = "t"
                          then
                              if avail wteh
                              then
                                  v-wtstage[wteh.stagecd + 1]
                              else  ""
                          else ""
                      else ""
     s-tieprice     = if avail vaelo then
                          if vaelo.ordertype = "o"
                          then
                              if avail oeeh and avail oeel
                              then
                                  string(oeel.price,"zzzzzz9.99999")
                              else ""
                          else ""
                      else ""
     s-tiename      = if avail vaelo
                      then
                          if vaelo.ordertype = "o"
                          then
                              if avail oeeh and avail arsc
                              then
                                  arsc.name
                              else ""
                          else
                          if vaelo.ordertype = "t"
                          then
                              if avail wteh
                              then
                                  "From: " + wteh.shipfmwhse + " To: " +
                                  wteh.shiptowhse
                              else ""
                          else ""
                      else "".

