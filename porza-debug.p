/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
/*        THIS IS ONLY FOR DEBUGGING DO NOT USE THIS AS SOURCE CODE         */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
/*
PROGRAM : ZSDIXRPTX - Generic reporting program
If an outside stream is desired
x-stream is true, otherwise the default stream is used
The shared stream if x-stream is true is named xpcd and
is assumed to be opened output before this program is called
The variables described below in some cases are offered to users in
free format giving them maximun flexability to generate reports. In other
cases a format is offered with the variables actually defined in NOTES
table.
Variables determining how program will process data:
  p-optiontype -  This needs to be a comma delimited string that
                  lists the order the report will be printed in.
                  Each token is user defined and populated through
                  user hooks. If the sort is desired to be by the
                  total VALUE of the subtotal of the token, the token
                  should be followed by #xx where xx is a number from
                  1 to 40. Values populated into these totals are user
                  defined through user hook {&user_numbertotals}. So in
                  this example if C stood for customer and S sales rep,
                  s,c#4 would sort by the sales rep 1234 and the subtotal
                  value of the customer based in the value placed in the
                  fourth position of the totals table. For ease of
                  user understanding the positions of the totals normally
                  follow the columns on the report beign printed.
  p-sorttype   -  This is a comma delimited string that dictated the
                  sort sequence '>' means ascending and '>' means
                  descending. Each entry here coresponds to the tokens
                  requested in the p-optiontype variable.
  p-totaltype  -  This is a comma delimited string. Each entry here
                  coresponds to the tokens requested in the p-optiontype
                  variable. The entry dictates what will be done with the
                  subtotal for each token defined in p-optiontype the
                  valid options are :
                    T - Total line for data
                    P - Page break then Total Line
                    S - Sort only do not print this data or Total But Export
                        column
                    N - Sort only do not print this data or Total not Export
                    Ux - Print an underline and react like the 'L' option
                    Lx - Total line then line advance x number of times
                    I - Ignore All Other Lines.
                  With these options the format of the report is varied as
                  well as controlling if a subtotal will be printed.
  p-summcounts -  This is a comma delimited string that is optional.
                  Each entry here coresponds to the tokens requested in
                  the p-optiontype variable.
                  Valid entrys are:
                    A   - Print all information.
                    x   - Where x is the count of appropriate token to be
                        printed and all the others will print combined in
                        an all others line.
                    Cx  - The letter C stands for count, where x is the count
                        of appropriate token to be printed and all the
                        others will print combined in an all others line.
                    Lx  - The letter L stands for Less Than, where x is the
                        value that will be compared to. This is only valid
                        for p-optiontype's that use a # option since that
                        is where the value is compared to, others will
                        print combined in an all others line.
                    Gx  - The letter G stands for Greater Than, where x is the
                        value that will be compared to. This is only valid
                        for p-optiontype's that use a # option since that
                        is where the value is compared to, others will
                        print combined in an all others line.
  p-register   -  This is a comma delimited string that is optional. If a
                  token is mentioned in the p-optiontype it can be put in
                  the p-register string. This string is different than all
                  the above strings because positioning is not based on
                  p-optiontype entrys. For example in the p-option example
                  s,c#4 since s is mentioned this variable can be
                  s#1,s#4,s#10,c#5. This will cause the program to keep
                  a register for each of the values listed.
  p-registerex -  This is a comma delimited string that coresponds to the
                  p-register variable. This reporesents the expression to
                  be applied to the register. Valid expressions are:
                  Lx  - The letter L stands for Less Than, where x is the
                  value that will be compared to. This corresponds
                  to entry's in p-register.
                  Gx  - The letter G stands for Greater Than, where x is the
                        value that will be compared to. This corresponds
                        to entry's in p-register.
                  LEx - The letter LE stands for Less Than or Equal,
                        where x is the value that will be compared to.
                        This corresponds to entry's in p-register.
                  GEx - The letter GE stands for Greater Than or Equal,
                        where x is the value that will be compared to.
                        This corresponds to entry's in p-register.
  p-detail     -  p-detail is an 'S' of only summary information is
                  going to be printed. 'S' does not make records in
                  the internal tables at the detail level. 'D' means
                  detail information is needed and updated internal
                  tables accordingly.
  p-export     -  If this variable is populated the program assumes
                  it is a filename and the output is directed to a
                  TAB delimited file controled be user hooks.
MODIFICATIONS:
TAH    03/05/09 brought registers from 10 to 40
                added  hook  &user_summaryputexportaftput
                             &user_exportstatheaders1
                             &user_exportstatheaders2
TAH002 03/08/10 Fixed the I option for not printing / exporting All Other
                summary totals
TAH    11/22/10 Major code upgrade and cleanup. Prepare for Version 1.1
                where the generated code include is separated from the 
                user include. This is done because a include cannot be
                more that 15000 characters and I hit that. 
            
                Final total should not export on DW file
                export added to final logic instead of user logic
                
                Version 1.1 changes :
                
                   Separate user code from generated code includes
                            user code:      x-prognameU.i
                            generated code: x-prognameG.i
                              NOTE: generated code will default to 
                                    x-zsdixrptxG.i for the template 
                                    unless one exists under the program
                                    name.
                            Degug option:  Merges the hooks into the 
                                           source code zsdixrptx.p and
                                           makes a merged list so one can
                                           debug the program easier. This 
                                           output should NEVER be used as
                                           the source since you will loose
                                           all upgrades and it will not
                                           be apparent that the program is
                                           not TBXR generated.
                                    
-------------------------------------------------------------------------- */
DEFINE VAR x-stream AS LOGICAL NO-UNDO.
DEFINE STREAM xpcd.
{p-rptbeg.i}
DEFINE BUFFER x-sapb FOR sapb.
/* EXPT is used to EXPORT data to an EXCEL file format */
DEFINE STREAM EXPT.
DEFINE VAR v-del AS c FORMAT "x(1)" NO-UNDO.
DEFINE VAR zzx          AS INTEGER                  NO-UNDO.
DEFINE VAR export_rec   AS c FORMAT "x(300)"        NO-UNDO.
/* Misc totals and percent variables used in generic formats */
DEFINE VAR v-per1       AS de                       NO-UNDO.
DEFINE VAR v-per2       AS de                       NO-UNDO.
DEFINE VAR v-per3       AS de                       NO-UNDO.
DEFINE VAR v-per4       AS de                       NO-UNDO.
DEFINE VAR v-per5       AS de                       NO-UNDO.
DEFINE VAR v-per6       AS de                       NO-UNDO.
DEFINE VAR v-per7       AS de                       NO-UNDO.
DEFINE VAR v-per8       AS de                       NO-UNDO.
DEFINE VAR v-per9       AS de                       NO-UNDO.
DEFINE VAR v-amt1       AS de                       NO-UNDO.
DEFINE VAR v-amt2       AS de                       NO-UNDO.
DEFINE VAR v-amt3       AS de                       NO-UNDO.
DEFINE VAR v-amt4       AS de                       NO-UNDO.
DEFINE VAR v-amt5       AS de                       NO-UNDO.
DEFINE VAR v-amt6       AS de                       NO-UNDO.
DEFINE VAR v-amt7       AS de                       NO-UNDO.
DEFINE VAR v-amt8       AS de                       NO-UNDO.
DEFINE VAR v-amt9       AS de                       NO-UNDO.
DEFINE VAR v-amt10      AS de                       NO-UNDO.
DEFINE VAR v-amt11      AS de                       NO-UNDO.
DEFINE VAR v-amt12      AS de                       NO-UNDO.
DEFINE VAR v-amt13      AS de                       NO-UNDO.
DEFINE VAR v-amt14      AS de                       NO-UNDO.
DEFINE VAR v-amt15      AS de                       NO-UNDO.
DEFINE VAR v-amt16      AS de                       NO-UNDO.
DEFINE VAR v-amt17      AS de                       NO-UNDO.
DEFINE VAR v-amt18      AS de                       NO-UNDO.
DEFINE VAR v-amt19      AS de                       NO-UNDO.
DEFINE VAR v-amt20      AS de                       NO-UNDO.
DEFINE VAR v-amt21      AS de                       NO-UNDO.
DEFINE VAR v-amt22      AS de                       NO-UNDO.
DEFINE VAR v-amt23      AS de                       NO-UNDO.
DEFINE VAR v-amt24      AS de                       NO-UNDO.
DEFINE VAR v-amt25      AS de                       NO-UNDO.
DEFINE VAR v-amt26      AS de                       NO-UNDO.
DEFINE VAR v-amt27      AS de                       NO-UNDO.
DEFINE VAR v-amt28      AS de                       NO-UNDO.
DEFINE VAR v-amt29      AS de                       NO-UNDO.
DEFINE VAR v-amt30      AS de                       NO-UNDO.
DEFINE VAR v-amt31      AS de                       NO-UNDO.
DEFINE VAR v-amt32      AS de                       NO-UNDO.
DEFINE VAR v-amt33      AS de                       NO-UNDO.
DEFINE VAR v-amt34      AS de                       NO-UNDO.
DEFINE VAR v-amt35      AS de                       NO-UNDO.
DEFINE VAR v-amt36      AS de                       NO-UNDO.
DEFINE VAR v-amt37      AS de                       NO-UNDO.
DEFINE VAR v-amt38      AS de                       NO-UNDO.
DEFINE VAR v-amt39      AS de                       NO-UNDO.
DEFINE VAR v-amt40      AS de                       NO-UNDO.
DEFINE VAR v-amt41      AS de                       NO-UNDO.
DEFINE VAR v-amt42      AS de                       NO-UNDO.
DEFINE VAR v-amt43      AS de                       NO-UNDO.
DEFINE VAR v-amt44      AS de                       NO-UNDO.
DEFINE VAR v-amt45      AS de                       NO-UNDO.
DEFINE VAR v-amt46      AS de                       NO-UNDO.
DEFINE VAR v-amt47      AS de                       NO-UNDO.
DEFINE VAR v-amt48      AS de                       NO-UNDO.
DEFINE VAR v-amt49      AS de                       NO-UNDO.
DEFINE VAR v-amt50      AS de                       NO-UNDO.
DEFINE VAR v-amt51      AS de                       NO-UNDO.
DEFINE VAR v-amt52      AS de                       NO-UNDO.
DEFINE VAR v-amt53      AS de                       NO-UNDO.
DEFINE VAR v-amt54      AS de                       NO-UNDO.
DEFINE VAR v-amt55      AS de                       NO-UNDO.
DEFINE VAR v-amt56      AS de                       NO-UNDO.
DEFINE VAR v-amt57      AS de                       NO-UNDO.
DEFINE VAR v-amt58      AS de                       NO-UNDO.
DEFINE VAR v-amt59      AS de                       NO-UNDO.
DEFINE VAR v-amt60      AS de                       NO-UNDO.
DEFINE VAR firstbreak AS LOGICAL                    NO-UNDO.
DEFINE VAR v-descend    AS LOGICAL                  NO-UNDO.
DEFINE VAR founddata  AS LOGICAL  INIT FALSE        NO-UNDO.
DEFINE VAR v-cycles   AS INTEGER                    NO-UNDO.
DEFINE VAR v-delim    AS c FORMAT "x"               NO-UNDO.
DEFINE VAR v-changecnt AS INTEGER                   NO-UNDO.
/* Parameters used by reporting  through SAPB */
DEFINE VAR p-optiontype AS CHARACTER FORMAT "x(15)" NO-UNDO.
DEFINE VAR p-sorttype   AS CHARACTER FORMAT "x(15)" NO-UNDO.
DEFINE VAR p-totaltype  AS CHARACTER FORMAT "x(15)" NO-UNDO.
DEFINE VAR p-summtype   AS CHARACTER FORMAT "x(15)" NO-UNDO.
DEFINE VAR p-summcounts AS CHARACTER FORMAT "x(24)" NO-UNDO.
DEFINE VAR p-register   AS CHARACTER FORMAT "x(15)" NO-UNDO.
DEFINE VAR p-registerex AS CHARACTER FORMAT "x(15)" NO-UNDO.
DEFINE VAR p-portrait   AS LOGICAL INIT FALSE       NO-UNDO.
DEFINE VAR p-detail   AS CHARACTER FORMAT "x"       NO-UNDO.
DEFINE VAR p-export   AS CHARACTER FORMAT "x(24)"   NO-UNDO.
DEFINE VAR p-exportl  AS LOGICAL                    NO-UNDO.
DEFINE VAR p-exportDWl  AS LOGICAL                  NO-UNDO.
DEFINE VAR p-dualoutl   AS LOGICAL   init false     NO-UNDO.
/* the above need to be set for the report to work */
DEFINE VAR x-str1 AS CHARACTER                      NO-UNDO.
DEFINE VAR x-str2 AS CHARACTER                      NO-UNDO.
DEFINE VAR x-fnd  AS INTEGER                        NO-UNDO.
DEFINE VAR x-len  AS INTEGER                        NO-UNDO.
DEFINE VAR v-inx      AS INTEGER                    NO-UNDO.
DEFINE VAR v-inx2     AS INTEGER                    NO-UNDO.
DEFINE VAR v-inx3     AS INTEGER                    NO-UNDO.
DEFINE VAR v-inx4     AS INTEGER                    NO-UNDO.
/* TAH002 */
DEFINE VAR v-inxI     AS INTEGER                    NO-UNDO.
DEFINE VAR v-IgnoreAllOtherFl AS LOGICAL            NO-UNDO.
/* TAH002 */
DEFINE VAR v-entitys  AS INTEGER                    NO-UNDO.
DEFINE VAR summ-count AS INTEGER                    NO-UNDO.
DEFINE VAR v-token    AS c      FORMAT "x(100)"     NO-UNDO.
/* If more totals are needed just add t-ammounts2..3..4  */
DEFINE VAR v-registers AS DEC            EXTENT 40  NO-UNDO.
DEFINE VAR v-register2 AS DEC            EXTENT 40  NO-UNDO.
DEFINE VAR v-InOrOtherFl AS LOGICAL                 NO-UNDO.
DEFINE VAR t-amounts   AS DEC            EXTENT 60
                                                    NO-UNDO.
DEFINE VAR t-amounts1  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts2  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts3  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts4  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts5  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts6  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts7  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts8  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts9  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts10  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts11  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts12  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts13  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts14  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts15  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts16  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts17  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts18  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts19  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts20  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts21  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts22  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts23  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts24  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts25  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts26  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts27  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts28  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts29  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts30  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts31  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts32  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts33  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts34  AS DEC            EXTENT 10 
                                                    NO-UNDO.
DEFINE VAR t-amounts35  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts36  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts37  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts38  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts39  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts40  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts41  AS DEC            EXTENT 10 
                                                    NO-UNDO.
DEFINE VAR t-amounts42  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts43  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts44  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts45  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts46  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts47  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts48  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts49  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts50  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts51  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts52  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts53  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts54  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts55  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts56  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts57  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts58  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts59  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR t-amounts60  AS DEC            EXTENT 10
                                                    NO-UNDO.
DEFINE VAR v-lookup    AS CHARACTER FORMAT "x(2)" EXTENT 10
                                                    NO-UNDO.
DEFINE VAR v-cells     AS CHARACTER FORMAT "x(2)" EXTENT 10
                                                    NO-UNDO.
/* New */
DEFINE VAR v-ValOrCnt  AS CHARACTER FORMAT "x(1)" EXTENT 10
                                                    NO-UNDO
    INIT ["C",
          "C",
          "C",
          "C",
          "C",
          "C",
          "C",
          "C",
          "C",
          "C"].
/* New */
DEFINE VAR v-sortup   AS CHARACTER FORMAT "x(1)" EXTENT 10
    INIT [">",
          ">",
          ">",
          ">",
          ">",
          ">",
          ">",
          ">",
          ">",
          ">"]
                                                    NO-UNDO.
DEFINE VAR v-totalup  AS CHARACTER FORMAT "x(1)" EXTENT 10
    INIT ["t",
          "t",
          "t",
          "t",
          "t",
          "t",
          "t",
          "t",
          "t",
          "t"]
                                                    NO-UNDO.  
DEFINE VAR v-totalupcnt AS INT  EXTENT 10
    INIT [1,
          1,
          1,
          1,
          1,
          1,
          1,
          1,
          1,
          1]
                                                    NO-UNDO.
DEFINE VAR v-summup   AS CHARACTER FORMAT "x(1)" EXTENT 10
    INIT [" ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " "]
                                                    NO-UNDO.
DEFINE VAR v-subtotalup   AS INT   FORMAT "9" EXTENT 10
    INIT [4,
          4,
          4,
          4,
          4,
          4,
          4,
          4,
          4,
          4]
                                                    NO-UNDO.
DEFINE VAR v-summcounts     AS CHAR   FORMAT "x(6)" EXTENT 10
    INIT ["ALL   ",
          "ALL   ",
          "ALL   ",
          "ALL   ",
          "ALL   ",
          "ALL   ",
          "ALL   ",
          "ALL   ",
          "ALL   ",
          "ALL   "]
                                                    NO-UNDO.
DEFINE VAR v-drptRecid AS RECID                     NO-UNDO.
DEFINE VAR v-mystery  AS CHARACTER FORMAT "x(100)"
                                                    NO-UNDO.
DEFINE VAR v-sortmystery  AS CHARACTER FORMAT "x(100)"
                                                    NO-UNDO.
DEFINE VAR v-trimmystery  AS CHARACTER FORMAT "x(100)"
                                                    NO-UNDO.
DEFINE VAR v-trimhold     AS CHARACTER FORMAT "x(100)"
                                                    NO-UNDO.
DEFINE VAR v-holdmystery  AS CHARACTER FORMAT "x(100)"
                                                    NO-UNDO.
DEFINE VAR v-newmystery   AS CHARACTER FORMAT "x(100)"
                                                    NO-UNDO.
DEFINE VAR v-holdsortmystery  AS CHARACTER FORMAT "x(100)"
                                                    NO-UNDO.
DEFINE VAR v-sizer    AS INTEGER                    NO-UNDO.
DEFINE VAR v-finalbreak  AS LOGICAL  INIT FALSE     NO-UNDO.
DEFINE VAR u-entitys  AS INTEGER                    NO-UNDO.
DEFINE VAR u-rentitys  AS INTEGER                   NO-UNDO.
DEFINE VAR x-entitys  AS INTEGER                    NO-UNDO.
DEFINE VAR x-token    AS c                          NO-UNDO.
DEFINE VAR v-stotcnt  AS INT                        NO-UNDO.
DEFINE VAR v-sregcnt  AS INT                        NO-UNDO.
DEFINE VAR v-final    AS CHARACTER FORMAT "x(1)"    NO-UNDO.
DEFINE VAR v-val      AS de                         NO-UNDO.
DEFINE VAR v-RegVal   AS de  EXTENT 40              NO-UNDO.
DEFINE VAR v-summary-lit    AS CHARACTER FORMAT "x(8)"
                                                    NO-UNDO.
DEFINE VAR v-summary-lit2   AS CHARACTER FORMAT "x(40)"
                                                    NO-UNDO.
DEFINE VAR v-summary-amount AS DEC
                                                    NO-UNDO.
DEFINE VAR v-summary-val    AS CHARACTER FORMAT "x(12)"
                                                    NO-UNDO.
DEFINE VAR v-astrik         AS CHARACTER FORMAT "x(6)"
                                                    NO-UNDO.
DEFINE VAR h-lookup         AS CHARACTER FORMAT "x(15)" 
                                                    NO-UNDO.
DEFINE VAR h-repname        AS CHARACTER FORMAT "x(30)" 
                                                    NO-UNDO.
DEFINE VAR h-genlit         AS CHARACTER FORMAT "x(30)" 
                                                    NO-UNDO.
DEFINE VAR h-genlit_h       AS CHARACTER FORMAT "x(30)" 
                                                    NO-UNDO.
DEFINE VAR h-hold-lit       AS CHARACTER            NO-UNDO.
DEFINE VAR h-underline      AS CHARACTER FORMAT "x(176)" 
                                                    NO-UNDO.
/* Begin Used for reverse coalating sorts  */
DEFINE VAR ascii_index      AS INTEGER              NO-UNDO.
DEFINE VAR ascii_length     AS INTEGER              NO-UNDO.
DEFINE TEMP-TABLE ascii_bit_swaper
  FIELD CHAR  AS CHAR
  FIELD DEC   AS INT
  FIELD undec AS INT
INDEX ascii_inx 
  DEC.
/* End Used for reverse coalating sorts  */
DEF VAR inz AS INTEGER                              NO-UNDO.
DEF VAR cc AS INTEGER                               NO-UNDO.
DEF VAR m-lbl   AS c  FORMAT "x(9)" EXTENT 3        NO-UNDO.
DEF VAR narr1   AS c  FORMAT "x(10)"                NO-UNDO.
DEF VAR narr2   AS c  FORMAT "x(30)"                NO-UNDO.
DEF VAR narr3   AS c  FORMAT "x(4)"                 NO-UNDO.
DEF VAR as-per  AS INT                              NO-UNDO.
DEF VAR e-yr    AS INT                              NO-UNDO.
DEF VAR e-mo    AS INT                              NO-UNDO.
DEF VAR x-yr    AS INT                              NO-UNDO.
DEF VAR bgdt    AS DATE                             NO-UNDO.
DEF VAR eddt    AS DATE                             NO-UNDO.
DEF VAR z-cat   AS c   FORMAT "x(5)"                NO-UNDO.
DEF VAR l-process AS LOGICAL                        NO-UNDO.
/*    */
DEF VAR newreport     AS LOGICAL                    NO-UNDO.
DEF VAR newpage       AS LOGICAL                    NO-UNDO.
DEF VAR printcust     AS LOGICAL                    NO-UNDO.
DEF VAR z-tild        AS CHARACTER FORMAT "x" INIT "~~" 
                                                    NO-UNDO.
DEF VAR v-6tildas     as CHARACTER                  NO-UNDO.
DEF VAR z-hash        AS CHARACTER FORMAT "x" INIT "#" 
                                                    NO-UNDO.
DEF VAR z-cart        AS CHARACTER FORMAT "x" INIT "^" 
                                                    NO-UNDO.
DEF VAR z-atsn        AS CHARACTER FORMAT "x" INIT "@" 
                                                    NO-UNDO.
DEF VAR z-pcsn        AS CHARACTER FORMAT "x" INIT "%" 
                                                    NO-UNDO.
DEF VAR n-mth         AS c FORMAT "x(9)" EXTENT 12  NO-UNDO
  INIT [' January ',
        'February ',
        '  March  ',
        '  April  ',
        '   May   ',
        '  June   ',
        '  July   ',
        ' August  ',
        'September',
        ' October ',
        ' November',
        ' December'].
/* Your Shared Temp Table, Forms and any of your Global Variables */
/* segment size makes the need for possibly a few hooks */
/* >>>>>>>>> BEGIN INSERTED HOOK user_defines1 */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
/*    THIS HOOK WAS NOT INCLUDED IN THE USERS INCLUDE !!!! */ 
/* >>>>>>>>>   END INSERTED HOOK user_defines1 */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
/* Your Shared Temp Table, Forms and any of your Global Variables */
/* Your Shared Temp Table, Forms and any of your Global Variables */
/* segment size makes the need for possibly a few hooks */
/* >>>>>>>>> BEGIN INSERTED HOOK user_defines2 */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
/*    THIS HOOK WAS NOT INCLUDED IN THE USERS INCLUDE !!!! */ 
/* >>>>>>>>>   END INSERTED HOOK user_defines2 */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
/* Your Shared Temp Table, Forms and any of your Global Variables */
/* Your Shared Temp Table, Forms and any of your Global Variables */
/* >>>>>>>>> BEGIN INSERTED HOOK user_temptable */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
def var p-detailprt     as logical          no-undo.
/* RANGES */
def     var b-buyer         as c      format "x(4)"           no-undo.
def     var e-buyer         as c      format "x(4)"           no-undo.
def     var b-vendno        as dec    format "999999999999"   no-undo.
def     var e-vendno        as dec    format "999999999999"   no-undo.
def     var b-region        as c      format "x(4)"           no-undo.
def     var e-region        as c      format "x(4)"           no-undo.
def     var b-enterdt       as date   format "99/99/99"       no-undo.
def     var e-enterdt       as date   format "99/99/99"       no-undo.
def     var p-format        as int                            no-undo.
def     var p-regval        as c                              no-undo.
def     var o-sort          as c      format "x(1)"           no-undo.
def     var v-weekdays      as  int                           no-undo.     
def     var v-idate         as date   format "99/99/99"       no-undo.
define {&sharetype} temp-table porzatemp no-undo
field buyer       as c   format     "x(4)"
field name        as c   format     "x(20)"
field vend        as dec format     ">>>>>>>>>>>9"
field region      as c   format     "x(4)"
field pono        like   poeh.pono
field tot-po      as i   format     ">>>9"        initial 0
field tot-line    as i   format     ">>>9"        initial 0  
field tot-ack     as i   format     ">>>9"        initial 0
field tot-per     as dec format     ">99.99"      initial 0
field un3         as i   format     ">>>9"        initial 0
field un4-10      as i   format     ">>>9"        initial 0
field un11-20     as i   format     ">>>9"        initial 0
field ov21        as i   format     ">>>9"        initial 0
index k-buyer
      buyer
index k-region
      region.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
/* >>>>>>>>>   END INSERTED HOOK user_temptable */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
/* Your Shared Temp Table, Forms and any of your Global Variables */
DEFINE TEMP-TABLE drpt NO-UNDO
  FIELD sortmystery AS CHARACTER FORMAT "x(100)"
  FIELD mystery     AS CHARACTER FORMAT "x(100)"
  FIELD stotmystery AS CHARACTER FORMAT "x(100)"
/* >>>>>>>>> BEGIN INSERTED HOOK user_drpttable */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
/* Use to add fields to the report table */
   field porzatempid as recid
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
/* >>>>>>>>>   END INSERTED HOOK user_drpttable */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
  FIELD xval        AS DECIMAL  EXTENT 60
  /* New */
  FIELD Vtots       AS DECIMAL  EXTENT 10
  FIELD VRegister1  AS DECIMAL  EXTENT 10
  FIELD VRegister2  AS DECIMAL  EXTENT 10
  FIELD VRegister3  AS DECIMAL  EXTENT 10
  FIELD VRegister4  AS DECIMAL  EXTENT 10
  FIELD VRegister5  AS DECIMAL  EXTENT 10
  FIELD VRegister6  AS DECIMAL  EXTENT 10
  FIELD VRegister7  AS DECIMAL  EXTENT 10
  FIELD VRegister8  AS DECIMAL  EXTENT 10
  FIELD VRegister9  AS DECIMAL  EXTENT 10
  FIELD VRegister10 AS DECIMAL  EXTENT 10
  FIELD VRegister11 AS DECIMAL  EXTENT 10
  FIELD VRegister12 AS DECIMAL  EXTENT 10
  FIELD VRegister13 AS DECIMAL  EXTENT 10
  FIELD VRegister14 AS DECIMAL  EXTENT 10
  FIELD VRegister15 AS DECIMAL  EXTENT 10
  FIELD VRegister16 AS DECIMAL  EXTENT 10
  FIELD VRegister17 AS DECIMAL  EXTENT 10
  FIELD VRegister18 AS DECIMAL  EXTENT 10
  FIELD VRegister19 AS DECIMAL  EXTENT 10
  FIELD VRegister20 AS DECIMAL  EXTENT 10
  FIELD VRegister21 AS DECIMAL  EXTENT 10 
  FIELD VRegister22 AS DECIMAL  EXTENT 10
  FIELD VRegister23 AS DECIMAL  EXTENT 10
  FIELD VRegister24 AS DECIMAL  EXTENT 10
  FIELD VRegister25 AS DECIMAL  EXTENT 10
  FIELD VRegister26 AS DECIMAL  EXTENT 10
  FIELD VRegister27 AS DECIMAL  EXTENT 10
  FIELD VRegister28 AS DECIMAL  EXTENT 10
  FIELD VRegister29 AS DECIMAL  EXTENT 10
  FIELD VRegister30 AS DECIMAL  EXTENT 10
  FIELD VRegister31 AS DECIMAL  EXTENT 10
  FIELD VRegister32 AS DECIMAL  EXTENT 10
  FIELD VRegister33 AS DECIMAL  EXTENT 10
  FIELD VRegister34 AS DECIMAL  EXTENT 10
  FIELD VRegister35 AS DECIMAL  EXTENT 10
  FIELD VRegister36 AS DECIMAL  EXTENT 10
  FIELD VRegister37 AS DECIMAL  EXTENT 10
  FIELD VRegister38 AS DECIMAL  EXTENT 10
  FIELD VRegister39 AS DECIMAL  EXTENT 10
  FIELD VRegister40 AS DECIMAL  EXTENT 10
/* New */
/* Below this is application specific */
INDEX dinx
      stotmystery
INDEX dinx2
      sortmystery.
DEFINE BUFFER b-drpt FOR drpt.
DEFINE TEMP-TABLE vdrpt NO-UNDO
  FIELD trimmystery     AS CHARACTER FORMAT "x(100)"
  FIELD Vtots           AS DECIMAL  EXTENT 10
  FIELD VRegister1      AS DECIMAL  EXTENT 10
  FIELD VRegister2      AS DECIMAL  EXTENT 10
  FIELD VRegister3      AS DECIMAL  EXTENT 10
  FIELD VRegister4      AS DECIMAL  EXTENT 10
  FIELD VRegister5      AS DECIMAL  EXTENT 10
  FIELD VRegister6      AS DECIMAL  EXTENT 10
  FIELD VRegister7      AS DECIMAL  EXTENT 10
  FIELD VRegister8      AS DECIMAL  EXTENT 10
  FIELD VRegister9      AS DECIMAL  EXTENT 10
  FIELD VRegister10     AS DECIMAL  EXTENT 10
  FIELD VRegister11     AS DECIMAL  EXTENT 10
  FIELD VRegister12     AS DECIMAL  EXTENT 10
  FIELD VRegister13     AS DECIMAL  EXTENT 10
  FIELD VRegister14     AS DECIMAL  EXTENT 10
  FIELD VRegister15     AS DECIMAL  EXTENT 10
  FIELD VRegister16     AS DECIMAL  EXTENT 10
  FIELD VRegister17     AS DECIMAL  EXTENT 10
  FIELD VRegister18     AS DECIMAL  EXTENT 10
  FIELD VRegister19     AS DECIMAL  EXTENT 10
  FIELD VRegister20     AS DECIMAL  EXTENT 10
  FIELD VRegister21     AS DECIMAL  EXTENT 10
  FIELD VRegister22     AS DECIMAL  EXTENT 10
  FIELD VRegister23     AS DECIMAL  EXTENT 10
  FIELD VRegister24     AS DECIMAL  EXTENT 10
  FIELD VRegister25     AS DECIMAL  EXTENT 10
  FIELD VRegister26     AS DECIMAL  EXTENT 10
  FIELD VRegister27     AS DECIMAL  EXTENT 10
  FIELD VRegister28     AS DECIMAL  EXTENT 10
  FIELD VRegister29     AS DECIMAL  EXTENT 10
  FIELD VRegister30     AS DECIMAL  EXTENT 10
  FIELD VRegister31     AS DECIMAL  EXTENT 10
  FIELD VRegister32     AS DECIMAL  EXTENT 10
  FIELD VRegister33     AS DECIMAL  EXTENT 10
  FIELD VRegister34     AS DECIMAL  EXTENT 10
  FIELD VRegister35     AS DECIMAL  EXTENT 10
  FIELD VRegister36     AS DECIMAL  EXTENT 10
  FIELD VRegister37     AS DECIMAL  EXTENT 10
  FIELD VRegister38     AS DECIMAL  EXTENT 10
  FIELD VRegister39     AS DECIMAL  EXTENT 10
  FIELD VRegister40     AS DECIMAL  EXTENT 10
INDEX dinx
      trimmystery.
DEFINE TEMP-TABLE Registers NO-UNDO
  FIELD TokenInx        AS INTEGER
  FIELD TokenVal        AS CHAR
  FIELD RegisterNo      AS INTEGER
  FIELD Expression      AS CHAR
  FIELD VValue          AS DEC
INDEX tvx
      TokenVal
      RegisterNo
INDEX tvx2
      TokenInx.
/* ------------------------------------------------------------------------
Variables Used To Create Trend Like Header
-----------------------------------------------------------------------*/
DEFINE VARIABLE rpt-dt      AS DATE FORMAT "99/99/9999" 
                                                    NO-UNDO.
DEFINE VARIABLE rpt-dow     AS CHAR FORMAT "x(3)"   NO-UNDO.
DEFINE VARIABLE rpt-time    AS CHAR FORMAT "x(5)"   NO-UNDO.
DEFINE VARIABLE rpt-cono    LIKE oeeh.cono          NO-UNDO.
DEFINE VARIABLE rpt-user    AS CHAR FORMAT "x(08)"  NO-UNDO.
DEFINE VARIABLE x-title     AS CHAR FORMAT "x(177)" NO-UNDO.
DEFINE VARIABLE x-page      AS INTEGER FORMAT ">>>9" 
                                                    NO-UNDO.
DEFINE VARIABLE dow-lst     AS CHARACTER FORMAT "x(30)"
INIT "Sun,Mon,Tue,Wed,Thu,Fri,Sat"                  NO-UNDO.
DEFINE VARIABLE monthname   AS CHAR FORMAT "x(10)" 
                            EXTENT 12 
  INITIAL ["JANUARY",
           "FEBRUARY",
           "MARCH",
           "APRIL",
           "MAY",
           "JUNE",
           "JULY",
           "AUGUST",
           "SEPTEMBER",
           "OCTOBER",
           "NOVEMBER",
           "DECEMBER"].
/* -----------------------------------------------------------------------
Header Variable Initialization
----------------------------------------------------------------------*/
ASSIGN 
  rpt-user = USERID("dictdb")
  rpt-dt = TODAY
  rpt-time = STRING(TIME, "HH:MM")
  rpt-dow = ENTRY(WEEKDAY(TODAY),dow-lst)
  rpt-cono = g-cono
  x-page = 0.
/* >>>>>>>>> BEGIN INSERTED HOOK user_forms */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
/* FORM HEADER */
form 
  "Buyer"                 at 1
  "Name"                  at 7
  "#PO's"                 at 28
  "#Lines"                at 34
  "#Ack'ed"               at 41 
  "Percent"               at 49
/*  "#Unack'ed <=3 Days"      at 68     */
  "#Unack'ed 4-10 Days"     at 88
  "#Unack'ed 11-20 Days"    at 109
  "#Unack'ed 21+ Days"      at 132  
  "-----"                 at 1
  "--------------------"  at 7
  "-----"                 at 28
  "------"                at 34
  "-------"               at 41
  "------------------"    at 49
  "-------------------"   at 68
  "--------------------"  at 88                       
  "------------------"    at 109
  "------------------"    at 128
  with frame porzatemp-hdr no-underline no-box no-labels page-top down width 178. 
form 
  porzatemp.buyer        at 1 
  porzatemp.name         at 7
  porzatemp.tot-po       at 28
  porzatemp.tot-line     at 34
  porzatemp.tot-ack      at 41
  porzatemp.tot-per      at 49
  "%"                    at 57
/*  porzatemp.un3          at 68    */
  porzatemp.un4-10       at 88
  porzatemp.un11-20      at 109
  porzatemp.ov21         at 132
  with frame porzatemp-dtl no-underline no-box no-labels down width 178.
/* 
form 
  skip(1)
  skip(1)
  "Total All Regions" at 1
  var-total-po        at 28
  var-total-line      at 34
  var-total-ack       at 41
  var-total-per       at 48 
  "%"                 at 55
  with frame porzatemp-totals no-underline no-box no-labels down width 178. 
*/
/*   
form
  "Subtotals"         at 1
  subcountpo          at 28
  subcountline        at 34
  subcountack         at 41
  subcountper         at 48
  with frame f-subtotals no-underline no-box no-labels down width 178.  
*/  
  
form
  skip(1)
  "Region: "         at 1
  porzatemp.region       at 9
  with frame f-region no-underline no-box no-labels down width 178. 
  
 
form
v-final                 at 1
v-astrik                at 2        format "x(2)"
v-summary-lit2          at 4        format "x(27)"
"~015"
v-amt1                  at 28       format     ">>>9"        
v-amt2                  at 34       format     ">>>9"        
v-amt3                  at 41       format     ">>>9"         
v-amt4                  at 49       format     ">99.99"
"%"                     at 57
"~015"                  at 178
skip(1)
with frame f-tot width 178  no-box no-labels.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
/* >>>>>>>>>   END INSERTED HOOK user_forms */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
/* Functions */
/* This function Xappend is used to avoid the what seems to be a bug
   in passed string concatinations through includes it seems as though
   is messses up the character count ant therfore issues a 135 compile
   error statement size over 15000
*/   
 
 
FUNCTION Xappend  RETURNS CHARACTER ( INPUT-OUTPUT ip-String AS CHAR,
                                      INPUT        ip-token AS CHAR):
  ASSIGN ip-string = ip-string + v-del + ip-token.
  RETURN (ip-string).
END FUNCTION.    
FUNCTION AmtZero RETURNS LOGICAL (INPUT ip-tbl60 AS DEC EXTENT 60):
  IF 
    ip-tbl60[1] = 0  AND ip-tbl60[11] = 0 AND
    ip-tbl60[2] = 0  AND ip-tbl60[12] = 0 AND
    ip-tbl60[3] = 0  AND ip-tbl60[13] = 0 AND
    ip-tbl60[4] = 0  AND ip-tbl60[14] = 0 AND
    ip-tbl60[5] = 0  AND ip-tbl60[15] = 0 AND
    ip-tbl60[6] = 0  AND ip-tbl60[16] = 0 AND
    ip-tbl60[7] = 0  AND ip-tbl60[17] = 0 AND
    ip-tbl60[8] = 0  AND ip-tbl60[18] = 0 AND
    ip-tbl60[9] = 0  AND ip-tbl60[19] = 0 AND
    ip-tbl60[10] = 0 AND ip-tbl60[20] = 0 AND
    ip-tbl60[21] = 0  AND ip-tbl60[31] = 0 AND
    ip-tbl60[22] = 0  AND ip-tbl60[32] = 0 AND
    ip-tbl60[23] = 0  AND ip-tbl60[33] = 0 AND
    ip-tbl60[24] = 0  AND ip-tbl60[34] = 0 AND
    ip-tbl60[25] = 0  AND ip-tbl60[35] = 0 AND
    ip-tbl60[26] = 0  AND ip-tbl60[36] = 0 AND
    ip-tbl60[27] = 0  AND ip-tbl60[37] = 0 AND
    ip-tbl60[28] = 0  AND ip-tbl60[38] = 0 AND
    ip-tbl60[29] = 0  AND ip-tbl60[39] = 0 AND
    ip-tbl60[30] = 0  AND ip-tbl60[40] = 0 AND
    ip-tbl60[41] = 0  AND ip-tbl60[51] = 0 AND
    ip-tbl60[42] = 0  AND ip-tbl60[52] = 0 AND
    ip-tbl60[43] = 0  AND ip-tbl60[53] = 0 AND
    ip-tbl60[44] = 0  AND ip-tbl60[54] = 0 AND
    ip-tbl60[45] = 0  AND ip-tbl60[55] = 0 AND
    ip-tbl60[46] = 0  AND ip-tbl60[56] = 0 AND
    ip-tbl60[47] = 0  AND ip-tbl60[57] = 0 AND
    ip-tbl60[48] = 0  AND ip-tbl60[58] = 0 AND
    ip-tbl60[49] = 0  AND ip-tbl60[59] = 0 AND
    ip-tbl60[50] = 0  AND ip-tbl60[60] = 0 THEN
    RETURN (TRUE).
  ELSE
    RETURN (FALSE).
END FUNCTION.
/* ------ Begin of Parameter Handling and initialization -------- */
Assign v-6tildas = z-tild + z-tild + z-tild + z-tild + z-tild + z-tild.
/* Begin Used for reverse coalating sorts  */
RUN Ascii_Coalation.
/* End Used for reverse coalating sorts    */
ASSIGN 
  v-delim = CHR(001) /* this is used for internal entry delimiter  */
  v-del   = CHR(09). /* this is used in excel output This is a TAB */
{p-rptper.i}
/* >>>>>>>>> BEGIN INSERTED HOOK user_extractrun */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 run sapb_vars.
 run extract_data.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
/* >>>>>>>>>   END INSERTED HOOK user_extractrun */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
ASSIGN 
  h-underline = FILL("-",176).
ASSIGN
  v-sizer = LENGTH(p-optiontype).
/*
bumping option physical length up by 2 because a # and (integer) can
follow the option and the logic looks out possibly 2 chars to check
we don't want to run out of string to look at.
*/
ASSIGN 
  SUBSTRING(p-optiontype,(v-sizer + 1),3) = "   ".
/*
  Parse option string looking for the sort order as well as the
  sort - or subtotal S#1 options. That is why above 2 characters are
  added to the input in case the last option is implied. If the # is
  not supplied the sort will be based on the token. # will subtotal
  and sort by the selection amount given after the #. If a integer is not
  supplied after the # the v-totalup table is initialized with the default
  report amount to be used.
*/
DO v-inx = 1 TO v-sizer:
  IF SUBSTRING(p-optiontype,v-inx,1) = "," OR
     SUBSTRING(p-optiontype,v-inx,1) = "#" OR
     SUBSTRING(p-optiontype,v-inx,1) = "1" OR
     SUBSTRING(p-optiontype,v-inx,1) = "2" OR
     SUBSTRING(p-optiontype,v-inx,1) = "3" OR
     SUBSTRING(p-optiontype,v-inx,1) = "4" OR
     SUBSTRING(p-optiontype,v-inx,1) = "5" OR
     SUBSTRING(p-optiontype,v-inx,1) = "6" OR
     SUBSTRING(p-optiontype,v-inx,1) = "7" OR
     SUBSTRING(p-optiontype,v-inx,1) = "8" OR
     SUBSTRING(p-optiontype,v-inx,1) = "9" OR
     SUBSTRING(p-optiontype,v-inx,1) = " " THEN
    NEXT.
  /* Below Can-do's and error displays nee to be changed to your option codes */
  
/* >>>>>>>>> BEGIN INSERTED HOOK user_optiontype */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
if not
can-do("B,R,V",substring(p-optiontype,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries B,R,V "
     substring(p-optiontype,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries B,R,V "
     substring(p-optiontype,v-inx,1).
/* >>>>>>>>>   END INSERTED HOOK user_optiontype */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
  
     ASSIGN
      u-entitys = u-entitys + 1.
  
    IF SUBSTRING(p-optiontype,(v-inx + 1),1) = "#" THEN DO:
      ASSIGN
        v-lookup[u-entitys] = SUBSTRING(p-optiontype,v-inx,1)
        v-summup[u-entitys] = "#".
      IF CAN-DO
      ("1,2,3,4,5,6,7,8,9",SUBSTRING(p-optiontype,(v-inx + 2),1)) AND
        CAN-DO
      ("1,2,3,4,5,6,7,8,9,0",SUBSTRING(p-optiontype,(v-inx + 3),1)) THEN DO:
        ASSIGN
          v-subtotalup[u-entitys] = INT(SUBSTRING(p-optiontype,(v-inx + 2),2)).
      END.
    ELSE
    IF CAN-DO
    ("1,2,3,4,5,6,7,8,9",SUBSTRING(p-optiontype,(v-inx + 2),1)) THEN DO:
      ASSIGN
        v-subtotalup[u-entitys] = INT(SUBSTRING(p-optiontype,(v-inx + 2),1)).
    END.
    
    
  END.
  ELSE
    ASSIGN
      v-lookup[u-entitys] = SUBSTRING(p-optiontype,v-inx,1).
  
END. /* End of Do v-inx */
/*
Get ascending > and descending < options
*/
ASSIGN 
  x-entitys = 0
  v-sizer = LENGTH(p-sorttype)
  SUBSTRING(p-sorttype,(v-sizer + 1),1) = " ".
DO v-inx = 1 TO v-sizer:
  IF SUBSTRING(p-sorttype,v-inx,1) = "," OR
     SUBSTRING(p-sorttype,v-inx,1) = " " THEN
    NEXT.
  IF NOT CAN-DO(">,<",SUBSTRING(p-sorttype,v-inx,1)) THEN
    IF NOT x-stream THEN
      DISPLAY "Invalid sort selection valid entries are <,>"
              SUBSTRING(p-sorttype,v-inx,1).
    ELSE
      DISPLAY STREAM xpcd "Invalid sort selection valid entries are <,>"
                          SUBSTRING(p-sorttype,v-inx,1).
  
  ASSIGN
    x-entitys = x-entitys + 1
    v-sortup[x-entitys] = SUBSTRING(p-sorttype,v-inx,1).
END. /* END of DO v-inx */
/*
  Get total option (P)age Break, (T)otal Print, (S)ort don't print
  and (L)ine advance
*/
/*
  Types are T - Total line for data
            P - Page break then Total Line
            S - Sort only do not print this data or Total But Export column
            N - Sort only do not print this data or Total not Export
            Ux - Print an underline and react like the 'L' option
            Lx - Total line then line advance x number of times
            I  - Ignore printing the all others line
*/
ASSIGN
  x-entitys = 0.
  v-sizer = LENGTH(p-totaltype).
v-inx2 = 0.
DO v-inx = 1 TO v-sizer:
  IF SUBSTRING(p-totaltype,v-inx,1) = "," THEN DO:
    ASSIGN 
      v-inx2 = v-inx2 + 1.
  END.
END.
IF v-inx2 > 0 THEN
  ASSIGN 
    v-inx2 = v-inx2 + 1.
ELSE
IF p-totaltype <> "" THEN
  ASSIGN 
    v-inx2 = 1.
DO v-inx = 1 TO v-inx2:
  ASSIGN
    v-token = ENTRY(v-inx,p-totaltype,",")
    x-token = ENTRY(v-inx,p-totaltype,",")
    x-token = REPLACE(v-token," ","")
    v-token = REPLACE(v-token," ","")
    v-token = REPLACE(v-token,"0","")
    v-token = REPLACE(v-token,"1","")
    v-token = REPLACE(v-token,"2","")
    v-token = REPLACE(v-token,"3","")
    v-token = REPLACE(v-token,"4","")
    v-token = REPLACE(v-token,"5","")
    v-token = REPLACE(v-token,"6","")
    v-token = REPLACE(v-token,"7","")
    v-token = REPLACE(v-token,"8","")
    v-token = REPLACE(v-token,"9","").
  
  
  IF v-token = "" THEN DO:
    ASSIGN
      v-totalup[v-inx] = "l"
      v-totalupcnt[v-inx] = 1.
  END.
  ELSE
  IF v-token = "l" THEN DO:
    ASSIGN 
      v-totalup[v-inx] = "L".
    IF LENGTH(v-token) <> LENGTH(x-token) THEN
      ASSIGN
        v-totalupcnt[v-inx] = 
        INT(REPLACE(ENTRY(v-inx,p-totaltype,","),"l","")).
    ELSE
      ASSIGN 
        v-totalupcnt[v-inx] = 1.
  END.
  ELSE
  IF v-token = "u" THEN DO:
    ASSIGN 
      v-totalup[v-inx] = "u".
    IF LENGTH(v-token) <> LENGTH(x-token) THEN
      ASSIGN
        v-totalupcnt[v-inx] = 
        INT(REPLACE(ENTRY(v-inx,p-totaltype,","),"u","")).
    ELSE
      ASSIGN v-totalupcnt[v-inx] = 1.
  END.
  ELSE DO:
    ASSIGN 
      v-totalup[v-inx] = v-token
      v-totalupcnt[v-inx] = 1.
    IF NOT CAN-DO("t,p,s,n,l,u,i",
    SUBSTRING(ENTRY(v-inx,p-totaltype,","),1,1)) THEN
      IF NOT x-stream THEN
        DISPLAY 
          "Invalid totaling selection valid entries are T,P,S,L,N,U,I -"
          SUBSTRING(ENTRY(v-inx,p-totaltype,","),1,1).
    ELSE
      DISPLAY STREAM xpcd
        "Invalid totaling selection valid entries are T,P,S,L,N,U,I -"
        SUBSTRING(ENTRY(v-inx,p-totaltype,","),1,1).
    
  END.
END.
/*
Get summary count values
*/
ASSIGN 
  x-entitys = 0
  v-sizer = LENGTH(p-summcounts)
  v-inx2 = 0.
DO v-inx = 1 TO v-sizer:
  IF SUBSTRING(p-summcounts,v-inx,1) = "," THEN DO:
    ASSIGN
      v-inx2 = v-inx2 + 1.
  END.
  
END.
IF v-inx2 > 0 THEN
  ASSIGN
    v-inx2 = v-inx2 + 1.
ELSE
IF p-summcounts <> "" THEN
  ASSIGN
    v-inx2 = 1.
DO v-inx = 1 TO v-inx2:
  ASSIGN
    v-token = ENTRY(v-inx,p-summcounts,",")
    v-token = REPLACE(v-token,"0","")
    v-token = REPLACE(v-token,"1","")
    v-token = REPLACE(v-token,"2","")
    v-token = REPLACE(v-token,"3","")
    v-token = REPLACE(v-token,"4","")
    v-token = REPLACE(v-token,"5","")
    v-token = REPLACE(v-token,"6","")
    v-token = REPLACE(v-token,"7","")
    v-token = REPLACE(v-token,"8","")
    v-token = REPLACE(v-token,"9","").
  IF v-token = "" THEN DO:
    ASSIGN
      v-ValOrCnt[v-inx] = "C"
      v-summcounts[v-inx] = ENTRY(v-inx,p-summcounts,",").
  END.
  ELSE IF v-token = "l" THEN DO:
    ASSIGN
      v-ValOrCnt[v-inx] = "L"
      v-summcounts[v-inx] = REPLACE(ENTRY(v-inx,p-summcounts,","),"l","").
  END.
  ELSE IF v-token = "g" THEN DO:
    ASSIGN
      v-ValOrCnt[v-inx] = "G"
      v-summcounts[v-inx] = REPLACE(ENTRY(v-inx,p-summcounts,","),"g","").
  END.
  ELSE IF v-token = "C" THEN DO:
    ASSIGN
      v-ValOrCnt[v-inx] = "C"
      v-summcounts[v-inx] = REPLACE(ENTRY(v-inx,p-summcounts,","),"c","").
  END.
  ELSE
    ASSIGN
      v-summcounts[v-inx] = "ALL   ".
END.
ASSIGN
  v-sizer = LENGTH(p-register).
/*
  bumping option physical length up by 2 because a # and (integer) can
  follow the option and the logic looks out possibly 2 chars to check
  we don't want to run out of string to look at.
*/
ASSIGN 
  SUBSTRING(p-register,(v-sizer + 1),3) = "   ".
/*
  Parse register string looking for the for subtotal S#1 options. That is
  why above 2 characters are added to the input in case the last register is
  entered wrong. # will subtotal by the selection amount given after the #. 
  If a  integer is not supplied after the # the entry is ignored
*/
DO v-inx = 1 TO v-sizer:
  IF  SUBSTRING(p-register,v-inx,1) = "," OR
      SUBSTRING(p-register,v-inx,1) = "#" OR
      SUBSTRING(p-register,v-inx,1) = "1" OR
      SUBSTRING(p-register,v-inx,1) = "2" OR
      SUBSTRING(p-register,v-inx,1) = "3" OR
      SUBSTRING(p-register,v-inx,1) = "4" OR
      SUBSTRING(p-register,v-inx,1) = "5" OR
      SUBSTRING(p-register,v-inx,1) = "6" OR
      SUBSTRING(p-register,v-inx,1) = "7" OR
      SUBSTRING(p-register,v-inx,1) = "8" OR
      SUBSTRING(p-register,v-inx,1) = "9" OR
      SUBSTRING(p-register,v-inx,1) = " " THEN
    NEXT.
  /* Below Can-do's and error displays nee to be changed to your option codes */
  
/* >>>>>>>>> BEGIN INSERTED HOOK user_registertype */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
if not
can-do("B,R,V",substring(p-register,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries B,R,V "
     substring(p-register,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries B,R,V "
     substring(p-register,v-inx,1).
/* >>>>>>>>>   END INSERTED HOOK user_registertype */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
   
  ASSIGN
    u-rentitys = u-rentitys + 1.
  
  IF SUBSTRING(p-register,(v-inx + 1),1) = "#" THEN DO:
    CREATE registers.
    ASSIGN 
      registers.TokenInx = u-rentitys
      registers.TokenVal = SUBSTRING(p-register,v-inx,1).
    IF CAN-DO
    ("1,2,3,4,5,6,7,8,9",SUBSTRING(p-register,(v-inx + 2),1)) AND
    CAN-DO
    ("1,2,3,4,5,6,7,8,9,0",SUBSTRING(p-register,(v-inx + 3),1)) THEN DO:
      ASSIGN
        registers.RegisterNo = INT(SUBSTRING(p-register,(v-inx + 2),2)).
    END.
    ELSE IF CAN-DO
    ("1,2,3,4,5,6,7,8,9",SUBSTRING(p-register,(v-inx + 2),1)) THEN DO:
      ASSIGN
        registers.RegisterNo = INT(SUBSTRING(p-register,(v-inx + 2),1)).
    END.
  END.
END. /* Do v-inx */
ASSIGN
  u-rentitys = 0
  v-sizer = LENGTH(p-registerex)
  v-inx2 = 0.
DO v-inx = 1 TO v-sizer:
  IF SUBSTRING(p-registerex,v-inx,1) = "," THEN DO:
    ASSIGN
      v-inx2 = v-inx2 + 1.
  END.
END.
IF v-inx2 > 0 THEN
  ASSIGN
    v-inx2 = v-inx2 + 1.
ELSE
IF p-registerex <> "" THEN
  ASSIGN
    v-inx2 = 1.
DO v-inx = 1 TO v-inx2:
  ASSIGN
    v-token = ENTRY(v-inx,p-registerex,",")
    v-token = REPLACE(v-token,"0","")
    v-token = REPLACE(v-token,"1","")
    v-token = REPLACE(v-token,"2","")
    v-token = REPLACE(v-token,"3","")
    v-token = REPLACE(v-token,"4","")
    v-token = REPLACE(v-token,"5","")
    v-token = REPLACE(v-token,"6","")
    v-token = REPLACE(v-token,"7","")
    v-token = REPLACE(v-token,"8","")
    v-token = REPLACE(v-token,"9","")
  /* Make Decimal values available */
    v-token = REPLACE(v-token,"-","")
    v-token = REPLACE(v-token,".","").
  
  
  IF v-token <> "le" AND
     v-token <> "ge" AND
     v-token <> "g" AND
     v-token <> "l"  THEN
    NEXT.
  
  FIND Registers WHERE 
       Registers.TokenInx = v-inx 
  EXCLUSIVE-LOCK.
  
  
  
  IF v-token = "l" THEN DO:
    ASSIGN 
      Registers.expression = "L"
      Registers.VValue =
        DEC(REPLACE(ENTRY(v-inx,p-registerex,","),"l","")).
  END.
  ELSE IF v-token = "G" THEN DO:
    ASSIGN 
      Registers.expression = "G"
      Registers.VValue =
        DEC(REPLACE(ENTRY(v-inx,p-registerex,","),"G","")).
  END.
  ELSE IF v-token = "le" THEN DO:
    ASSIGN 
      Registers.expression = "LE"
      x-token =
        REPLACE(ENTRY(v-inx,p-registerex,","),"l","")
      Registers.VValue =
        DEC(REPLACE(x-token,"e","")).
  END.
  
  ELSE IF v-token = "ge" THEN DO:
    ASSIGN 
    Registers.expression = "GE"
      x-token = REPLACE(ENTRY(v-inx,p-registerex,","),"g","")
    Registers.VValue =
      DEC(REPLACE(x-token,"e","")).
  END.
END.
IF p-detail = "d" THEN
  ASSIGN 
    u-entitys = u-entitys + 1
    v-lookup[u-entitys] = "x".
/* Landscape control Codes */
IF NOT x-stream AND NOT p-portrait THEN
  PUT CONTROL
    "~033~046~1541~117".   /* landscape format */
ELSE
IF NOT p-portrait THEN
  PUT STREAM xpcd CONTROL
    "~033~046~1541~117".   /* landscape format */
/* This is only if exporting a file */
ASSIGN 
  export_rec = "".
IF p-export <> "" AND p-export BEGINS "DW_" THEN DO:
  OUTPUT STREAM expt TO VALUE("/usr/tmp/" + p-export + ".exp").
  ASSIGN 
    p-exportl = TRUE
    p-exportDWl = TRUE
    export_rec = "Stot".
  DO v-inx4 = 1 TO u-entitys:
    IF v-totalup[v-inx4] = "n" THEN
      NEXT.
    
    ASSIGN 
      v-summary-lit2 = "".
/* >>>>>>>>> BEGIN INSERTED HOOK user_DWvarheaders1 */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
if v-lookup[v-inx4] = "B" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "V" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
/* >>>>>>>>>   END INSERTED HOOK user_DWvarheaders1 */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
   END.
  
/* >>>>>>>>> BEGIN INSERTED HOOK user_exportstatDWheaders */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
/*    THIS HOOK WAS NOT INCLUDED IN THE USERS INCLUDE !!!! */ 
/* >>>>>>>>>   END INSERTED HOOK user_exportstatDWheaders */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
  
  ASSIGN
    export_rec = export_rec + CHR(13) + CHR(10).
  PUT STREAM expt UNFORMATTED export_rec.
END.
ELSE IF p-export <> "" THEN DO:
  OUTPUT STREAM expt TO VALUE("/usr/tmp/" + p-export + ".ixk").
  ASSIGN
    p-exportl = TRUE
    export_rec = "Stot".
  DO v-inx4 = 1 TO u-entitys:
    IF v-totalup[v-inx4] = "n" THEN
      NEXT.
    ASSIGN
      v-summary-lit2 = "" .
   
/* >>>>>>>>> BEGIN INSERTED HOOK user_exportvarheaders1 */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
if v-lookup[v-inx4] = "B" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "V" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
/* >>>>>>>>>   END INSERTED HOOK user_exportvarheaders1 */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
  
   END.
  
/* >>>>>>>>> BEGIN INSERTED HOOK user_exportstatheaders1 */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
/*    THIS HOOK WAS NOT INCLUDED IN THE USERS INCLUDE !!!! */ 
/* >>>>>>>>>   END INSERTED HOOK user_exportstatheaders1 */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
  export_rec = export_rec + CHR(13).
  
  PUT STREAM expt UNFORMATTED export_rec.
  
  /* Two tier headings */
  ASSIGN
    export_rec = " ".
  DO v-inx4 = 1 TO u-entitys:
    IF v-totalup[v-inx4] = "n" THEN
      NEXT.
    
    ASSIGN 
      v-summary-lit2 = "".
/* >>>>>>>>> BEGIN INSERTED HOOK user_exportvarheaders2 */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
if v-lookup[v-inx4] = "B" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "V" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
/* >>>>>>>>>   END INSERTED HOOK user_exportvarheaders2 */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
  
  
  END.
  
  
/* >>>>>>>>> BEGIN INSERTED HOOK user_exportstatheaders */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  
 
     put stream expt unformatted " " +
     v-del + v-del + v-del + v-del + v-del + v-del + v-del +
     v-del
     v-del
     v-del
     v-del
     /*  v-del  */
     chr(13).
 
 
     assign export_rec = 
                         export_rec         +
                         "Buyer"                 + v-del +
                         "Name"                  + v-del +
                         "#PO's"                 + v-del +
                         "#Lines"                + v-del +
                         "#Ack'ed"               + v-del +
                         "Percent"               + v-del +
                         "#Unack'ed > 3 Days"    + v-del +
                         "#Unack'ed 4-10 Days"   + v-del +
                         "#Unack'ed 11-20 Days"  + v-del +
                         "#Unack'ed 21+ Days".
                          
           if p-detailprt then 
              assign export_rec = export_rec + v-del.            
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
/* >>>>>>>>>   END INSERTED HOOK user_exportstatheaders */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
 
/* >>>>>>>>> BEGIN INSERTED HOOK user_exportstatheaders2 */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
/*    THIS HOOK WAS NOT INCLUDED IN THE USERS INCLUDE !!!! */ 
/* >>>>>>>>>   END INSERTED HOOK user_exportstatheaders2 */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
  
  
  export_rec = export_rec + CHR(13).
  
  PUT STREAM expt UNFORMATTED export_rec.
END.
/*  End of Export */
/* ------------------------------------------------------------------------
Main Logic
-----------------------------------------------------------------------*/
/* Begin Normal data extraction  */
/* Code split because of segment size all routines are
   now moved to procedures
*/   
buildrec:
  /* The Shared Table used for detail */
/* >>>>>>>>> BEGIN INSERTED HOOK user_foreach */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
for each porzatemp no-lock:
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
/* >>>>>>>>>   END INSERTED HOOK user_foreach */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
  /* End Normal data extraction  */
  /* Beginning of Token Reporting Logic  */
/* >>>>>>>>> BEGIN INSERTED HOOK user_B4tokens */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
/*    THIS HOOK WAS NOT INCLUDED IN THE USERS INCLUDE !!!! */ 
/* >>>>>>>>>   END INSERTED HOOK user_B4tokens */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
  RUN  TokenReportingLogic 
      (INPUT-OUTPUT v-drptRecid).
  /* End of Token Reporting Logic  */
  if v-drptRecid = ? then
    next.
  FIND drpt WHERE RECID(drpt) = v-drptRecid NO-ERROR.
  IF AmtZero (INPUT drpt.xval) THEN
  /* Function to check amounts 1 - 60 all zero (Code Split) */
    DELETE drpt.  /* if we get reversing records that yeild 0 then delete */
/* >>>>>>>>> BEGIN INSERTED HOOK user_B4endloop */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
/*    THIS HOOK WAS NOT INCLUDED IN THE USERS INCLUDE !!!! */ 
/* >>>>>>>>>   END INSERTED HOOK user_B4endloop */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
END. /* buildrec */
/* 
  At this point all data is collected from the source(s) has been
  internally formatted for the reporting engine to handle it.
*/
ASSIGN 
  v-sregcnt = 0.
FOR EACH registers NO-LOCK:
  ASSIGN
    v-sregcnt = v-sregcnt + 1.
END.
/* Beginning '#' subtotal sort preparation  
   This procedure accumulates all the subtotals involved 
   with the user selection for the report run. The subtotals
   are kept for later use 
*/
RUN SubtotalSortPreparation.
/* Beginning of Summary Count sort preparation  
   This procedure will check out the count options for each sorted
   item and determine the number of subtotals inside each subtotal
   that are to be outputted to the media chosen.
*/
RUN  SummaryCountPreparation.
/* Beginning of reporting Engine 
   Reporting routines that interpret the subtotals and output
   options and determine the appropriate output for the 
   selected media type
*/
RUN ReportingEngine.
/* Done Processing  */
RETURN.
/* Reporting Procedures  Broken down because of segment sizes */
/* ----------------------------------------------------------------------- */
PROCEDURE TokenReportingLogic:
/* ----------------------------------------------------------------------- */
      
  DEFINE INPUT-OUTPUT PARAMETER ip-drptRecid AS RECID NO-UNDO.
  
  /* Beginning of Token Reporting Logic  */
  
  ASSIGN  
    v-mystery = ""
    v-sortmystery = "".
  DO v-inx = 1 TO u-entitys:
    IF v-lookup[v-inx] <> "" THEN DO:
      IF SUBSTRING(v-summup[v-inx],1,1) = "#" THEN DO:
        ASSIGN 
          v-stotcnt = v-stotcnt + 1.
      END.
      /* Above just count the #'s                 */
      
/* >>>>>>>>> BEGIN INSERTED HOOK user_loadtokens */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
if v-lookup[v-inx] = "B" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(porzatemp.buyer,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "R" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(porzatemp.region,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "V" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(porzatemp.vend,"999999999999")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"999999999999").   
  end.                                              
else                                              
if v-lookup[v-inx] = "X" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(recid(porzatemp),"999999999999")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"999999999999").   
  end.                                              
else                                              
  assign v-token = " ".
/* >>>>>>>>>   END INSERTED HOOK user_loadtokens */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
       
    END.
  END.
  
  DO zzx = 1 TO 60:
    ASSIGN 
      t-amounts[zzx] = 0.
  END.
  
  
  /* assign your amounts to the supplied variables */
  
/* >>>>>>>>> BEGIN INSERTED HOOK user_totaladd */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
  assign t-amounts[1] = porzatemp.tot-po.
  assign t-amounts[2] = porzatemp.tot-line.
  assign t-amounts[3] = porzatemp.tot-ack.
  assign t-amounts[4] = 0.
  assign t-amounts[5] = 1.
/* >>>>>>>>>   END INSERTED HOOK user_totaladd */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
   
  IF AmtZero (t-amounts) THEN DO: 
  /* Function to check amounts 1 - 60 all zero Code Split*/
    ip-drptRecid = ?.    
    RETURN.
  END.
  /* Don't create a record if there is not anything to report  */
  
  ASSIGN 
    v-sortmystery = v-sortmystery + v-delim.
  
  FIND drpt WHERE drpt.stotmystery = v-sortmystery NO-LOCK NO-ERROR.
  
  IF NOT avail drpt THEN DO:
    /* custom changes to the DRPT table are permitted */
    CREATE drpt.
    ASSIGN
      drpt.mystery = v-mystery
/* >>>>>>>>> BEGIN INSERTED HOOK user_drptassign */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 /* If you add fields to the dprt table this is where you assign them */
  drpt.porzatempid = recid(porzatemp)
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
/* >>>>>>>>>   END INSERTED HOOK user_drptassign */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
      drpt.sortmystery = v-sortmystery
      drpt.stotmystery = v-sortmystery.
  END.
  /*
    Remember in the case of summary mystery DRPT records are becomming
    summary records therefore the only usefull info is the common informatoin
    being summarized.
  
    That is why all numerics should be accumulative below !!
    This concept is very important to understand before continuing.
  
  */
  
  DO zzx = 1 TO 60:
    ASSIGN 
      drpt.xval[zzx] = drpt.xval[zzx] + t-amounts[zzx].
  END.
  
  ASSIGN 
    ip-drptRecid = RECID(drpt).
  /* End of Token Reporting Logic  */
END. /* End Procedure TokenReportingLogic */
/* ----------------------------------------------------------------------- */
PROCEDURE SubtotalSortPreparation:
  /* ----------------------------------------------------------------------- */
  /* Beginning '#' subtotal sort preparation  */
  
  IF v-stotcnt <> 0 OR
     v-sregcnt <> 0 THEN DO:
    ASSIGN 
      v-holdmystery = "xxxxxxxxxxxxxx"
      v-holdsortmystery = "xxxxxxxxxxxxxx".
    
    
    
    FOR EACH drpt USE-INDEX dinx2 :
      
      IF v-holdsortmystery = "xxxxxxxxxxxxxx" THEN
         ASSIGN
          v-holdsortmystery = drpt.sortmystery.
      
      IF v-holdmystery = "xxxxxxxxxxxxxx" THEN
        ASSIGN 
          v-holdmystery = drpt.mystery.
      IF v-holdsortmystery <> drpt.sortmystery THEN DO:
        ASSIGN 
          v-sortmystery = drpt.sortmystery
          v-mystery = drpt.mystery.
        RUN Check_The_Subtotals.
        ASSIGN 
          v-holdmystery = drpt.mystery
          v-holdsortmystery = drpt.sortmystery.
        
      END.
      DO v-inx = 1 TO (u-entitys + 1):
        RUN Amount_Accumulation (INPUT v-inx).
      END. /* END do v-inx */
    END. /* For Each DRPT */
    
    ASSIGN
      v-mystery = "`" + v-delim +
                  "`" + v-delim +
                  "`" + v-delim +
                  "`" + v-delim +
                  "`" + v-delim +
                  "`" + v-delim +
                  "`" + v-delim +
                  "`" + v-delim +
                  "`" + v-delim +
                  "`" + v-delim
      v-sortmystery = "`" + v-delim +
                      "`" + v-delim +
                      "`" + v-delim +
                      "`" + v-delim +
                      "`" + v-delim +
                      "`" + v-delim +
                      "`" + v-delim +
                      "`" + v-delim +
                      "`" + v-delim +
                      "`" + v-delim.
    RUN Check_The_Subtotals.
  END. /* End IF v-stotcnt <> 0 OR
                 v-sregcnt <> 0 */
 
  
  
  DO v-inx = 1 TO (u-entitys + 1):
    RUN Total_Iteration_Initialization (INPUT v-inx).
  END. /* End Do v-inx */
    
END. /* End Procedure SubtotalSortPreparation  */
/* ----------------------------------------------------------------------- */
PROCEDURE SummaryCountPreparation:
/* ----------------------------------------------------------------------- */
  
  /* Beginning of Summary Count sort preparation  */
  
  ASSIGN 
    v-inx = 0
    v-cycles = 0.
  DO v-inx4 = 1 TO 10:
    IF v-summcounts[v-inx4] BEGINS "a" OR
       v-summcounts[v-inx4] = " " THEN
      NEXT.
    ELSE DO:
      CREATE registers.
      ASSIGN 
        registers.TokenInx = v-inx + 100
        registers.TokenVal = v-lookup[v-inx4]
        registers.RegisterNo = v-subtotalup[v-inx4]
        Registers.expression = v-ValOrCnt[v-inx4]
        Registers.VValue = DEC(v-summcounts[v-inx4]).
      
    END.
  END.
  
  FOR EACH registers NO-LOCK:
    ASSIGN
      v-inx = v-inx + 1.
  END.
  
  
  
  IF v-inx = 0 THEN
    FOR EACH drpt USE-INDEX dinx:
      ASSIGN 
        drpt.sortmystery = drpt.stotmystery.
    END.
  
  IF v-inx <> 0 THEN DO:
    DO v-inx4 = 1 TO 10:
      FIND FIRST registers WHERE 
                 registers.TokenVal = v-lookup[v-inx4]
      NO-LOCK NO-ERROR.
      IF NOT avail registers THEN
        NEXT.
      IF v-cycles > 0 THEN
        FOR EACH drpt USE-INDEX dinx:
          ASSIGN 
            drpt.stotmystery = drpt.sortmystery.
        END.
      ASSIGN 
        v-cycles = v-cycles + 1.
    
    
      ASSIGN 
        v-holdmystery = "xxxxxxxxxxxxxx"
        v-holdsortmystery = "xxxxxxxxxxxxxx"
        summ-count = 0.
    
      FINDER:
    
      FOR EACH drpt USE-INDEX dinx :
      
        IF v-holdsortmystery = "xxxxxxxxxxxxxx" THEN
          ASSIGN 
            summ-count = 1
            v-holdsortmystery = drpt.stotmystery.
      
        IF v-holdmystery = "xxxxxxxxxxxxxx" THEN
          ASSIGN
            v-holdmystery = drpt.mystery.
      
      
      
        IF v-holdsortmystery = drpt.stotmystery THEN DO:
          ASSIGN
            summ-count = summ-count.
        END.
        ELSE IF v-holdsortmystery <> drpt.stotmystery THEN DO:
          ASSIGN 
            v-sortmystery = drpt.stotmystery
            v-mystery = drpt.mystery.
            v-changecnt = 0.
          DO v-inx3 = 1 TO v-inx4:
            IF ENTRY(v-inx3,v-holdsortmystery,v-delim) <>
              ENTRY(v-inx3,v-sortmystery,v-delim) THEN DO:
              ASSIGN         
                v-changecnt = 1.
              IF v-inx3 < v-inx4 THEN DO:
                ASSIGN 
                  v-holdmystery = drpt.mystery
                  v-holdsortmystery = drpt.stotmystery
                  drpt.sortmystery = drpt.stotmystery
                  summ-count = 0.
              /* next finder. */
                LEAVE.
              END.
            END.
          END. /* Do v-inx3 */
          IF v-changecnt = 0 THEN DO:
            ASSIGN 
              summ-count = summ-count.
          END.
          ELSE
            ASSIGN 
              summ-count = summ-count + 1.
        END. /* Else If v-holdsortmystery */
      
        ASSIGN 
          v-trimmystery = ""
          v-val = drpt.Vtots[v-inx4]
          v-registers[1]  = drpt.VRegister1[v-inx4]
          v-registers[2]  = drpt.VRegister2[v-inx4]
          v-registers[3]  = drpt.VRegister3[v-inx4]
          v-registers[4]  = drpt.VRegister4[v-inx4]
          v-registers[5]  = drpt.VRegister5[v-inx4]
          v-registers[6]  = drpt.VRegister6[v-inx4]
          v-registers[7]  = drpt.VRegister7[v-inx4]
          v-registers[8]  = drpt.VRegister8[v-inx4]
          v-registers[9]  = drpt.VRegister9[v-inx4]
          v-registers[10] = drpt.VRegister10[v-inx4]
          v-registers[11] = drpt.VRegister11[v-inx4]
          v-registers[12] = drpt.VRegister12[v-inx4]
          v-registers[13] = drpt.VRegister13[v-inx4]
          v-registers[14] = drpt.VRegister14[v-inx4]
          v-registers[15] = drpt.VRegister15[v-inx4]
          v-registers[16] = drpt.VRegister16[v-inx4]
          v-registers[17] = drpt.VRegister17[v-inx4]
          v-registers[18] = drpt.VRegister18[v-inx4]
          v-registers[19] = drpt.VRegister19[v-inx4]
          v-registers[20] = drpt.VRegister20[v-inx4]
          v-registers[21] = drpt.VRegister21[v-inx4]
          v-registers[22] = drpt.VRegister22[v-inx4]
          v-registers[23] = drpt.VRegister23[v-inx4]
          v-registers[24] = drpt.VRegister24[v-inx4]
          v-registers[25] = drpt.VRegister25[v-inx4]
          v-registers[26] = drpt.VRegister26[v-inx4]
          v-registers[27] = drpt.VRegister27[v-inx4]
          v-registers[28] = drpt.VRegister28[v-inx4]
          v-registers[29] = drpt.VRegister29[v-inx4]
          v-registers[30] = drpt.VRegister30[v-inx4]
          v-registers[31] = drpt.VRegister31[v-inx4]
          v-registers[32] = drpt.VRegister32[v-inx4]
          v-registers[33] = drpt.VRegister33[v-inx4]
          v-registers[34] = drpt.VRegister34[v-inx4]
          v-registers[35] = drpt.VRegister35[v-inx4]
          v-registers[36] = drpt.VRegister36[v-inx4]
          v-registers[37] = drpt.VRegister37[v-inx4]
          v-registers[38] = drpt.VRegister38[v-inx4]
          v-registers[39] = drpt.VRegister39[v-inx4]
          v-registers[40] = drpt.VRegister40[v-inx4]
          v-trimhold = "".
      
      
        IF v-inx4 < u-entitys AND
           v-sregcnt > 0 THEN
          RUN FindTrimMystery (INPUT drpt.mystery,
                               INPUT-OUTPUT v-val,
                               INPUT-OUTPUT v-registers[1],
                               INPUT-OUTPUT v-registers[2],
                               INPUT-OUTPUT v-registers[3],
                               INPUT-OUTPUT v-registers[4],
                               INPUT-OUTPUT v-registers[5],
                               INPUT-OUTPUT v-registers[6],
                               INPUT-OUTPUT v-registers[7],
                               INPUT-OUTPUT v-registers[8],
                               INPUT-OUTPUT v-registers[9],
                               INPUT-OUTPUT v-registers[10],
                               INPUT-OUTPUT v-registers[11],
                               INPUT-OUTPUT v-registers[12],
                               INPUT-OUTPUT v-registers[13],
                               INPUT-OUTPUT v-registers[14],
                               INPUT-OUTPUT v-registers[15],
                               INPUT-OUTPUT v-registers[16],
                               INPUT-OUTPUT v-registers[17],
                               INPUT-OUTPUT v-registers[18],
                               INPUT-OUTPUT v-registers[19],
                               INPUT-OUTPUT v-registers[20],
                               INPUT-OUTPUT v-registers[21],
                               INPUT-OUTPUT v-registers[22],
                               INPUT-OUTPUT v-registers[23],
                               INPUT-OUTPUT v-registers[24],
                               INPUT-OUTPUT v-registers[25],
                               INPUT-OUTPUT v-registers[26],
                               INPUT-OUTPUT v-registers[27],
                               INPUT-OUTPUT v-registers[28],
                               INPUT-OUTPUT v-registers[29],
                               INPUT-OUTPUT v-registers[30],
                               INPUT-OUTPUT v-registers[31],
                               INPUT-OUTPUT v-registers[32],
                               INPUT-OUTPUT v-registers[33],
                               INPUT-OUTPUT v-registers[34],
                               INPUT-OUTPUT v-registers[35],
                               INPUT-OUTPUT v-registers[36],
                               INPUT-OUTPUT v-registers[37],
                               INPUT-OUTPUT v-registers[38],
                               INPUT-OUTPUT v-registers[39],
                               INPUT-OUTPUT v-registers[40]).
      
      
      
      
        RUN Evaluate_Registers (INPUT v-lookup[v-inx4],
                                INPUT summ-count,
                                INPUT v-val,
                                INPUT v-registers[1],
                                INPUT v-registers[2],
                                INPUT v-registers[3],
                                INPUT v-registers[4],
                                INPUT v-registers[5],
                                INPUT v-registers[6],
                                INPUT v-registers[7],
                                INPUT v-registers[8],
                                INPUT v-registers[9],
                                INPUT v-registers[10],
                                INPUT v-registers[11],
                                INPUT v-registers[12],
                                INPUT v-registers[13],
                                INPUT v-registers[14],
                                INPUT v-registers[15],
                                INPUT v-registers[16],
                                INPUT v-registers[17],
                                INPUT v-registers[18],
                                INPUT v-registers[19],
                                INPUT v-registers[20],
                                INPUT v-registers[21],
                                INPUT v-registers[22],
                                INPUT v-registers[23],
                                INPUT v-registers[24],
                                INPUT v-registers[25],
                                INPUT v-registers[26],
                                INPUT v-registers[27],
                                INPUT v-registers[28],
                                INPUT v-registers[29],
                                INPUT v-registers[30],
                                INPUT v-registers[31],
                                INPUT v-registers[32],
                                INPUT v-registers[33],
                                INPUT v-registers[34],
                                INPUT v-registers[35],
                                INPUT v-registers[36],
                                INPUT v-registers[37],
                                INPUT v-registers[38],
                                INPUT v-registers[39],
                                INPUT v-registers[40],
                                INPUT-OUTPUT v-InOrOtherfl).
      
        IF v-InOrOtherFl = TRUE THEN DO:
          ASSIGN 
            drpt.sortmystery = drpt.stotmystery
            v-holdmystery = drpt.mystery
            v-holdsortmystery = drpt.stotmystery.
          NEXT FINDER.
        END.
        ELSE
        IF v-InOrOtherFl = FALSE THEN DO:
          DO v-inx = 1 TO u-entitys:
            ASSIGN
              v-token = ENTRY(v-inx,v-holdsortmystery,v-delim)
              v-sizer = LENGTH(v-token).
          /*  Goes to all others if it is the index we've determined
              or if it's a multiple token evaluation for example
              Customer #1 g1000 , Customer #3 l1000 takes two
              indexes but either being true dumps the customer in
              all others.
          */
            IF v-inx = v-inx4 OR
               v-lookup[v-inx] = v-lookup[v-inx4] THEN DO:
              ASSIGN
                v-token = FILL(z-tild,v-sizer).
            END.
            ELSE
              ASSIGN
                v-token = ENTRY(v-inx,v-holdsortmystery,v-delim).
          
            IF v-inx > 1 THEN
              ASSIGN 
                v-trimmystery = v-trimmystery +
                                v-delim.
         
            ASSIGN                   
                v-trimmystery = v-trimmystery +
                                v-token.
          END. /* do v-inx */
          ASSIGN 
            drpt.sortmystery = v-trimmystery + v-delim
            v-holdmystery = drpt.mystery
            v-holdsortmystery = drpt.stotmystery.
        END. /* end v-InOrOtherFl = FALSE */
      END.   /* For Each DRPT        */
    END.     /* DO v-inx4 = 1 TO 10: */ 
  END.       /* IF v-inx <> 0        */
 
  ASSIGN 
    v-mystery = "`" + v-delim +
                "`" + v-delim +
                "`" + v-delim +
                "`" + v-delim +
                "`" + v-delim +
                "`" + v-delim +
                "`" + v-delim +
                "`" + v-delim +
                "`" + v-delim +
                "`" + v-delim
    v-sortmystery = "`" + v-delim +
                    "`" + v-delim +
                    "`" + v-delim +
                    "`" + v-delim +
                    "`" + v-delim +
                    "`" + v-delim +
                    "`" + v-delim +
                    "`" + v-delim +
                    "`" + v-delim +
                    "`" + v-delim.
  
END. /* End Procedure SummaryCountPreparation  */
/* ----------------------------------------------------------------------- */
PROCEDURE ReportingEngine:
/* ----------------------------------------------------------------------- */
  
  /*  Beginning of reporting Engine
      This is where the tokens choosen are subtotaled and reported on
      this logic never needs to be changed for specific reporting,
      unless you have a detail option then the detail frame would be
      inside.
  */
  ASSIGN 
    v-holdmystery = "xxxxxxxxxxxxxx"
    v-holdsortmystery = "xxxxxxxxxxxxxx".
    
/* >>>>>>>>> BEGIN INSERTED HOOK user_viewheadframes */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
/* Since frames are unique to each program you need to put your
   views and hides here with your names */
if not p-exportl then
  do:
  if not x-stream then
    do:
    hide frame porzatemp-hdr.
    end.
  else
   do:
    hide stream xpcd frame porzatemp-hdr.
    end.
  
  end.
if not p-exportl  then
  do:
  if not x-stream then
    do:
    page.
    view frame porzatemp-hdr.
    end.
  else  
    do:
    page stream xpcd.
    view stream xpcd frame porzatemp-hdr.
    end.
   
  end.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
/* >>>>>>>>>   END INSERTED HOOK user_viewheadframes */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
  
  FOR EACH drpt NO-LOCK USE-INDEX dinx2
    BREAK BY drpt.sortmystery:
    
/* >>>>>>>>> BEGIN INSERTED HOOK user_drptloop */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
/* >>>>>>>>>   END INSERTED HOOK user_drptloop */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
    
    IF v-holdsortmystery = "xxxxxxxxxxxxxx" THEN DO:
      ASSIGN  
        v-holdsortmystery = drpt.sortmystery.
    END.
    IF v-holdmystery = "xxxxxxxxxxxxxx" THEN
      ASSIGN 
        v-holdmystery = drpt.mystery.
    
    ASSIGN 
      founddata = TRUE.
    
    IF v-holdsortmystery <> drpt.sortmystery THEN DO:
      ASSIGN 
        v-sortmystery = drpt.sortmystery
        v-mystery = drpt.mystery.
      RUN Check_The_Breaks.
      ASSIGN
        v-holdmystery = drpt.mystery
        v-holdsortmystery = drpt.sortmystery.
      
    END.
    
    DO v-inx = 1 TO (u-entitys + 1):
      RUN Amount_Accumulation (INPUT v-inx).
    END. /* DO v-inx = 1 TO (u-entitys + 1) */
    
/* >>>>>>>>> BEGIN INSERTED HOOK user_detailprint */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
   /* Comment this out if you are not using detail 'X' option  */
   if p-detailprt and 
      entry(u-entitys,drpt.sortmystery,v-delim) <> 
      fill("~~",length(entry(u-entitys,drpt.sortmystery,v-delim))) then 
     do:
     /* this code skips the all other lines for printing detail */
     /* next line would be your detail print procedure */
     if p-exportl then 
      run print-dtl-export(input drpt.porzatempid).
     else 
      run print-dtl(input drpt.porzatempid).    
     end.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
/* >>>>>>>>>   END INSERTED HOOK user_detailprint */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
    
  END.   /* FOR EACH drpt  */ 
  
  IF NOT founddata THEN DO:
    IF NOT x-stream THEN
      DISPLAY "No Records Found to Process" WITH FRAME erros.
    ELSE
      DISPLAY STREAM xpcd 
        "No Records Found to Process" WITH FRAME erros.
    
    LEAVE.
  END.
  /* invalidate hold areas for all encoumpassing control break (FINAL) */
  
  ASSIGN
    v-mystery = "`" + v-delim +
                "`" + v-delim +
                "`" + v-delim +
                "`" + v-delim +
                "`" + v-delim +
                "`" + v-delim +
                "`" + v-delim +
                "`" + v-delim +
                "`" + v-delim +
                "`" + v-delim
    v-sortmystery = "`" + v-delim +
                    "`" + v-delim +
                    "`" + v-delim +
                    "`" + v-delim +
                    "`" + v-delim +
                    "`" + v-delim +
                    "`" + v-delim +
                    "`" + v-delim +
                    "`" + v-delim +
                    "`" + v-delim.
  
  ASSIGN
    v-finalbreak = TRUE.
  RUN Check_The_Breaks.
  
  ASSIGN 
    v-astrik = " ".
  DO v-inx3 = 1 TO (u-entitys - IF p-detail = "d" THEN 1 ELSE 0):
    ASSIGN
      SUBSTRING(v-astrik,v-inx3,1)  = "*".
  END.
  
  ASSIGN
    v-astrik = REPLACE(v-astrik," ","").
  IF p-detail = "d" THEN
    ASSIGN
      v-final = "*".
  ELSE
    ASSIGN
      v-final = " ".
 
 /* Assign percentages    */
  
  ASSIGN
    v-summary-lit2 = " Final"
    v-summary-val = " ".
    
  /* v-summary-amount = t-amounts[u-entitys + 1]. */
  
/* >>>>>>>>> BEGIN INSERTED HOOK user_finalprint */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
if not p-exportl then
 do:
  assign v-per1 = 
       if t-amounts3[u-entitys + 1] = 0 then
         0
       else
       (t-amounts3[u-entitys + 1] / t-amounts2[u-entitys + 1]) * 100.
       
 if not x-stream then 
   do:
   display 
    v-final
    v-astrik
    v-summary-lit2
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts2[u-entitys + 1]   @ v-amt2    
    t-amounts3[u-entitys + 1]   @ v-amt3
    v-per1                      @ v-amt4
    with frame f-tot.
    down with frame f-tot.
   end.
 else
   do:
   display stream xpcd
    v-final
    v-astrik
    v-summary-lit2
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts2[u-entitys + 1]   @ v-amt2    
    t-amounts3[u-entitys + 1]   @ v-amt3 
    v-per1                      @ v-amt4   
    with frame f-tot.
    down stream xpcd with frame f-tot.
   end.
      
      
 end.
else
  do:
  assign export_rec =  v-astrik.
  do v-inx4 = 1 to u-entitys:
    if v-totalup[v-inx4] = "n" then
       next.
    if v-inx4 = 1 then
      v-summary-lit2 = "Final".
    else
      v-summary-lit2 = "".
    if v-cells[v-inx4]= "2" then
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec + v-summary-lit2 + v-del + " ".
     
      end.
    else
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec +  v-summary-lit2.
      end.
  end.
   
    assign v-per1 = 
       if t-amounts3[u-entitys + 1] = 0 then
         0
       else
       (t-amounts3[u-entitys + 1] / t-amounts2[u-entitys + 1]) * 100.
     
    assign export_rec = export_rec + v-del +
      string(t-amounts1[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts2[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts3[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts4[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(v-per1, "->99.99"). 
    
  assign export_rec = export_rec + chr(13).    
  put stream expt unformatted export_rec.
  end.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
/* >>>>>>>>>   END INSERTED HOOK user_finalprint */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
  
  /* Handle final logic like Summary_Export */
  IF p-exportl THEN DO:  
    IF p-exportDWl THEN
      ASSIGN 
        export_rec = export_rec + CHR(13) + CHR(10).
    ELSE
      ASSIGN 
        export_rec = export_rec + CHR(13).
    IF  NOT p-exportDWl THEN
      PUT STREAM expt UNFORMATTED export_rec.
  END. 
/* >>>>>>>>> BEGIN INSERTED HOOK user_endprint */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
/*    THIS HOOK WAS NOT INCLUDED IN THE USERS INCLUDE !!!! */ 
/* >>>>>>>>>   END INSERTED HOOK user_endprint */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
  
END. /* End Procedure ReportingEngine */
/* 
    Check_The_Subtotals is only invoked for '#' tokens
    it will go through the data and make ap  propriate computations.
*/
/* ----------------------------------------------------------------------- */
PROCEDURE Check_The_Subtotals:
/* ----------------------------------------------------------------------- */
  DO v-inx = 1 TO u-entitys:
    IF v-lookup[v-inx] <> "x" THEN
      IF ENTRY(v-inx,v-holdsortmystery,v-delim) <>
         ENTRY(v-inx,v-sortmystery,v-delim) THEN DO:
    /*  The subtotalup check is making sure that the entity is not used 
        just for sorting. If it is then a comtrol break will not 
        be taken
    */
      RUN Determine_Subtotals.
      LEAVE.
    END.
  END.
END. /* end Procedure Check_The_Subtotals */
/*  Determine_Subtotals goes through the tokens finding the '#' options
    and builds a partial MYSTERY field called TRIMMYSTERY. This is used
    to find all the detailed common token entrys in the table for later
    update in Trace_Subtotaling.
*/
/* ----------------------------------------------------------------------- */
PROCEDURE Determine_Subtotals:
/* ----------------------------------------------------------------------- */
  DO v-inx2 = u-entitys TO v-inx BY -1:
    IF SUBSTRING(v-summup[v-inx2],1,1) = "#" OR
      CAN-FIND(FIRST registers WHERE registers.tokenval =
               v-lookup[v-inx2]) THEN DO:
      ASSIGN 
        v-trimmystery = ""
        v-trimhold = "".
      DO v-inx4 = 1 TO v-inx2:
        IF v-inx4 > 1 THEN
          ASSIGN 
            v-trimmystery = v-trimmystery + v-delim
            v-trimhold    = v-trimhold    + v-delim.
        ASSIGN 
          v-trimmystery =
          v-trimmystery +
            ENTRY(v-inx4,v-holdsortmystery,v-delim)
          v-trimhold =
            v-trimhold +
            ENTRY(v-inx4,v-holdmystery,v-delim).
        
      END.
      ASSIGN 
        v-trimmystery = v-trimmystery + v-delim
        v-trimhold    = v-trimhold    + v-delim.
      
      RUN Trace_Subtotaling.
      
      RUN Total_Iteration_Initialization (INPUT v-inx2).
            
    END.
  END.
END.
/* Trace_Subtotaling moves trough the old token key STOTMYSTERY which is a
copy of SORTMYSTERY, inserting the subtotal into the token. For example
the customer break would turn E,E100,25 where the subtotal is 50.00 to
E,E100,50.0025 if ascending if descending it will be the ascii
oposite bit pattern.
*/
/* ----------------------------------------------------------------------- */
PROCEDURE Trace_Subtotaling:
  /* ----------------------------------------------------------------------- */
  FOR EACH b-drpt USE-INDEX dinx2 WHERE
           b-drpt.sortmystery BEGINS v-trimmystery:
    ASSIGN
      v-newmystery = "".
    DO v-inx4 = 1 TO u-entitys:
      IF v-inx4 > 1 THEN
        ASSIGN
          v-newmystery = v-newmystery + v-delim.
      IF v-inx4 = v-inx2 THEN DO:
        ASSIGN v-descend =
                    (IF v-sortup[v-inx4] = "<" THEN
                       TRUE
                     ELSE
                       FALSE).
        
/* >>>>>>>>> BEGIN INSERTED HOOK user_numbertotals */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
  if v-subtotalup[v-inx4] = 1 then
    v-val = t-amounts1[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 2 then
    v-val = t-amounts2[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 3 then
    v-val = t-amounts3[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 4 then
    v-val = 
      if t-amounts3[u-entitys + 1] = 0 then
         0
      else
       (t-amounts3[u-entitys + 1] / t-amounts2[u-entitys + 1]
      ) * 100.
  else 
  if v-subtotalup[v-inx4] = 5 then
    v-val = t-amounts5[v-inx4].
  else 
    v-val = t-amounts1[v-inx4].
  do inz = 1 to 5:
  if inz = 1 then
    v-regval[inz] = t-amounts1[v-inx4].
  else 
  if inz = 2 then
    v-regval[inz] = t-amounts2[v-inx4].
  else 
  if inz = 3 then
    v-regval[inz] = t-amounts3[v-inx4].
  else 
  if inz = 4 then
    v-regval[inz] = 
      if t-amounts3[u-entitys + 1] = 0 then
         0
      else
       (t-amounts3[u-entitys + 1] / t-amounts2[u-entitys + 1]
      ) * 100.
  else 
  if inz = 5 then
    v-regval[inz] = t-amounts5[v-inx4].
  else 
    assign inz = inz.
  end.
/* >>>>>>>>>   END INSERTED HOOK user_numbertotals */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
         
        IF v-inx4 < u-entitys THEN DO:
          FIND vdrpt WHERE vdrpt.trimmystery = v-trimhold NO-LOCK NO-ERROR.
          IF avail vdrpt THEN
            ASSIGN 
              vdrpt.VTots[v-inx4] = v-val
              vdrpt.VRegister1[v-inx4]  = v-regval[1]
              vdrpt.VRegister2[v-inx4]  = v-regval[2]
              vdrpt.VRegister3[v-inx4]  = v-regval[3]
              vdrpt.VRegister4[v-inx4]  = v-regval[4]
              vdrpt.VRegister5[v-inx4]  = v-regval[5]
              vdrpt.VRegister6[v-inx4]  = v-regval[6]
              vdrpt.VRegister7[v-inx4]  = v-regval[7]
              vdrpt.VRegister8[v-inx4]  = v-regval[8]
              vdrpt.VRegister9[v-inx4]  = v-regval[9]
              vdrpt.VRegister10[v-inx4] = v-regval[10]
              vdrpt.VRegister11[v-inx4] = v-regval[11]
              vdrpt.VRegister12[v-inx4] = v-regval[12]
              vdrpt.VRegister13[v-inx4] = v-regval[13]
              vdrpt.VRegister14[v-inx4] = v-regval[14]
              vdrpt.VRegister15[v-inx4] = v-regval[15]
              vdrpt.VRegister16[v-inx4] = v-regval[16]
              vdrpt.VRegister17[v-inx4] = v-regval[17]
              vdrpt.VRegister18[v-inx4] = v-regval[18]
              vdrpt.VRegister19[v-inx4] = v-regval[19]
              vdrpt.VRegister20[v-inx4] = v-regval[20]
              vdrpt.VRegister21[v-inx4] = v-regval[21]
              vdrpt.VRegister22[v-inx4] = v-regval[22]
              vdrpt.VRegister23[v-inx4] = v-regval[23]
              vdrpt.VRegister24[v-inx4] = v-regval[24]
              vdrpt.VRegister25[v-inx4] = v-regval[25]
              vdrpt.VRegister26[v-inx4] = v-regval[26]
              vdrpt.VRegister27[v-inx4] = v-regval[27]
              vdrpt.VRegister28[v-inx4] = v-regval[28]
              vdrpt.VRegister29[v-inx4] = v-regval[29]
              vdrpt.VRegister30[v-inx4] = v-regval[30]
              vdrpt.VRegister31[v-inx4] = v-regval[31]
              vdrpt.VRegister32[v-inx4] = v-regval[32]
              vdrpt.VRegister33[v-inx4] = v-regval[33]
              vdrpt.VRegister34[v-inx4] = v-regval[34]
              vdrpt.VRegister35[v-inx4] = v-regval[35]
              vdrpt.VRegister36[v-inx4] = v-regval[36]
              vdrpt.VRegister37[v-inx4] = v-regval[37]
              vdrpt.VRegister38[v-inx4] = v-regval[38]
              vdrpt.VRegister39[v-inx4] = v-regval[39]
              vdrpt.VRegister40[v-inx4] = v-regval[40].
          ELSE DO:
            CREATE vdrpt.
            ASSIGN 
              vdrpt.trimmystery = v-trimhold
              vdrpt.VTots[v-inx4] =  v-val
              vdrpt.VRegister1[v-inx4]  = v-regval[1]
              vdrpt.VRegister2[v-inx4]  = v-regval[2]
              vdrpt.VRegister3[v-inx4]  = v-regval[3]
              vdrpt.VRegister4[v-inx4]  = v-regval[4]
              vdrpt.VRegister5[v-inx4]  = v-regval[5]
              vdrpt.VRegister6[v-inx4]  = v-regval[6]
              vdrpt.VRegister7[v-inx4]  = v-regval[7]
              vdrpt.VRegister8[v-inx4]  = v-regval[8]
              vdrpt.VRegister9[v-inx4]  = v-regval[9]
              vdrpt.VRegister10[v-inx4] = v-regval[10]
              vdrpt.VRegister11[v-inx4] = v-regval[11]
              vdrpt.VRegister12[v-inx4] = v-regval[12]
              vdrpt.VRegister13[v-inx4] = v-regval[13]
              vdrpt.VRegister14[v-inx4] = v-regval[14]
              vdrpt.VRegister15[v-inx4] = v-regval[15]
              vdrpt.VRegister16[v-inx4] = v-regval[16]
              vdrpt.VRegister17[v-inx4] = v-regval[17]
              vdrpt.VRegister18[v-inx4] = v-regval[18]
              vdrpt.VRegister19[v-inx4] = v-regval[19]
              vdrpt.VRegister20[v-inx4] = v-regval[20]
              vdrpt.VRegister21[v-inx4] = v-regval[21]
              vdrpt.VRegister22[v-inx4] = v-regval[22]
              vdrpt.VRegister23[v-inx4] = v-regval[23]
              vdrpt.VRegister24[v-inx4] = v-regval[24]
              vdrpt.VRegister25[v-inx4] = v-regval[25]
              vdrpt.VRegister26[v-inx4] = v-regval[26]
              vdrpt.VRegister27[v-inx4] = v-regval[27]
              vdrpt.VRegister28[v-inx4] = v-regval[28]
              vdrpt.VRegister29[v-inx4] = v-regval[29]
              vdrpt.VRegister30[v-inx4] = v-regval[30]
              vdrpt.VRegister31[v-inx4] = v-regval[31]
              vdrpt.VRegister32[v-inx4] = v-regval[32]
              vdrpt.VRegister33[v-inx4] = v-regval[33]
              vdrpt.VRegister34[v-inx4] = v-regval[34]
              vdrpt.VRegister35[v-inx4] = v-regval[35]
              vdrpt.VRegister36[v-inx4] = v-regval[36]
              vdrpt.VRegister37[v-inx4] = v-regval[37]
              vdrpt.VRegister38[v-inx4] = v-regval[38]
              vdrpt.VRegister39[v-inx4] = v-regval[39]
              vdrpt.VRegister40[v-inx4] = v-regval[40].
          END. /* Else do                 */
        END. /* end IF v-inx4 < u-entitys */
        ELSE
          ASSIGN 
            b-drpt.VTots[v-inx4] =  v-val
            b-drpt.VRegister1[v-inx4]  = v-regval[1]
            b-drpt.VRegister2[v-inx4]  = v-regval[2]
            b-drpt.VRegister3[v-inx4]  = v-regval[3]
            b-drpt.VRegister4[v-inx4]  = v-regval[4]
            b-drpt.VRegister5[v-inx4]  = v-regval[5]
            b-drpt.VRegister6[v-inx4]  = v-regval[6]
            b-drpt.VRegister7[v-inx4]  = v-regval[7]
            b-drpt.VRegister8[v-inx4]  = v-regval[8]
            b-drpt.VRegister9[v-inx4]  = v-regval[9]
            b-drpt.VRegister10[v-inx4] = v-regval[10]
            b-drpt.VRegister11[v-inx4] = v-regval[11]
            b-drpt.VRegister12[v-inx4] = v-regval[12]
            b-drpt.VRegister13[v-inx4] = v-regval[13]
            b-drpt.VRegister14[v-inx4] = v-regval[14]
            b-drpt.VRegister15[v-inx4] = v-regval[15]
            b-drpt.VRegister16[v-inx4] = v-regval[16]
            b-drpt.VRegister17[v-inx4] = v-regval[17]
            b-drpt.VRegister18[v-inx4] = v-regval[18]
            b-drpt.VRegister19[v-inx4] = v-regval[19]
            b-drpt.VRegister20[v-inx4] = v-regval[20]
            b-drpt.VRegister21[v-inx4] = v-regval[21]
            b-drpt.VRegister22[v-inx4] = v-regval[22]
            b-drpt.VRegister23[v-inx4] = v-regval[23]
            b-drpt.VRegister24[v-inx4] = v-regval[24]
            b-drpt.VRegister25[v-inx4] = v-regval[25]
            b-drpt.VRegister26[v-inx4] = v-regval[26]
            b-drpt.VRegister27[v-inx4] = v-regval[27]
            b-drpt.VRegister28[v-inx4] = v-regval[28]
            b-drpt.VRegister29[v-inx4] = v-regval[29]
            b-drpt.VRegister30[v-inx4] = v-regval[30]
            b-drpt.VRegister31[v-inx4] = v-regval[31]
            b-drpt.VRegister32[v-inx4] = v-regval[32]
            b-drpt.VRegister33[v-inx4] = v-regval[33]
            b-drpt.VRegister34[v-inx4] = v-regval[34]
            b-drpt.VRegister35[v-inx4] = v-regval[35]
            b-drpt.VRegister36[v-inx4] = v-regval[36]
            b-drpt.VRegister37[v-inx4] = v-regval[37]
            b-drpt.VRegister38[v-inx4] = v-regval[38]
            b-drpt.VRegister39[v-inx4] = v-regval[39]
            b-drpt.VRegister40[v-inx4] = v-regval[40].
        
        
        ASSIGN  
          v-token = "".
        RUN Ascii_Make_It_Number(INPUT v-descend,
                                 INPUT v-val,
                                 INPUT-OUTPUT v-token).
        ASSIGN
          v-newmystery = v-newmystery + v-token +
            ENTRY(v-inx4,b-drpt.stotmystery,v-delim).
      END. /* end IF v-inx4 = v-inx2 */
      ELSE
        ASSIGN
          v-newmystery = 
            v-newmystery + ENTRY(v-inx4,b-drpt.stotmystery,v-delim).
    END. /* end DO v-inx4 = 1 TO u-entitys */
    IF SUBSTRING(v-summup[v-inx2],1,1) = "#" THEN
      ASSIGN 
        b-drpt.stotmystery = v-newmystery + v-delim.
  END. /* end FOR EACH b-drpt */
END.   /* end Procedure Trace_Subtotaling */
/* ----------------------------------------------------------------------- */
PROCEDURE FindTrimMystery:
/* ----------------------------------------------------------------------- */
  
  DEFINE INPUT PARAMETER       zx-mystery   AS CHARACTER FORMAT "x(100)" 
                                                                  NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-val      AS DEC                NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register1 AS DEC               NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register2 AS DEC               NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register3 AS DEC               NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register4 AS DEC               NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register5 AS DEC               NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register6 AS DEC               NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register7 AS DEC               NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register8 AS DEC               NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register9 AS DEC               NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register10 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register11 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register12 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register13 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register14 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register15 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register16 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register17 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register18 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register19 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register20 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register21 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register22 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register23 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register24 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register25 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register26 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register27 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register28 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register29 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register30 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register31 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register32 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register33 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register34 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register35 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register36 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register37 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register38 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register39 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-register40 AS DEC              NO-UNDO.
  
  
  ASSIGN  
    v-trimhold = "".
  DO v-inx2 = 1 TO v-inx4:
    IF v-inx2 > 1 THEN
      ASSIGN v-trimhold    = v-trimhold    + v-delim.
    ASSIGN v-trimhold =
           v-trimhold +
           ENTRY(v-inx2,zx-mystery,v-delim).
    
  END.
  ASSIGN 
    v-trimhold = v-trimhold + v-delim.
  
  FIND vdrpt WHERE 
       vdrpt.trimmystery = v-trimhold 
  NO-LOCK NO-ERROR.
  
  IF avail vdrpt THEN
    ASSIGN 
      zx-val = vdrpt.VTots[v-inx4]
      zx-register1  = vdrpt.VRegister1[v-inx4]
      zx-register2  = vdrpt.VRegister2[v-inx4]
      zx-register3  = vdrpt.VRegister3[v-inx4]
      zx-register4  = vdrpt.VRegister4[v-inx4]
      zx-register5  = vdrpt.VRegister5[v-inx4]
      zx-register6  = vdrpt.VRegister6[v-inx4]
      zx-register7  = vdrpt.VRegister7[v-inx4]
      zx-register8  = vdrpt.VRegister8[v-inx4]
      zx-register9  = vdrpt.VRegister9[v-inx4]
      zx-register10 = vdrpt.VRegister10[v-inx4]
      zx-register11 = vdrpt.VRegister11[v-inx4]
      zx-register12 = vdrpt.VRegister12[v-inx4]
      zx-register13 = vdrpt.VRegister13[v-inx4]
      zx-register14 = vdrpt.VRegister14[v-inx4]
      zx-register15 = vdrpt.VRegister15[v-inx4]
      zx-register16 = vdrpt.VRegister16[v-inx4]
      zx-register17 = vdrpt.VRegister17[v-inx4]
      zx-register18 = vdrpt.VRegister18[v-inx4]
      zx-register19 = vdrpt.VRegister19[v-inx4]
      zx-register20 = vdrpt.VRegister20[v-inx4]
      zx-register21 = vdrpt.VRegister21[v-inx4]
      zx-register22 = vdrpt.VRegister22[v-inx4]
      zx-register23 = vdrpt.VRegister23[v-inx4]
      zx-register24 = vdrpt.VRegister24[v-inx4]
      zx-register25 = vdrpt.VRegister25[v-inx4]
      zx-register26 = vdrpt.VRegister26[v-inx4]
      zx-register27 = vdrpt.VRegister27[v-inx4]
      zx-register28 = vdrpt.VRegister28[v-inx4]
      zx-register29 = vdrpt.VRegister29[v-inx4]
      zx-register30 = vdrpt.VRegister30[v-inx4]
      zx-register31 = vdrpt.VRegister31[v-inx4]
      zx-register32 = vdrpt.VRegister32[v-inx4]
      zx-register33 = vdrpt.VRegister33[v-inx4]
      zx-register34 = vdrpt.VRegister34[v-inx4]
      zx-register35 = vdrpt.VRegister35[v-inx4]
      zx-register36 = vdrpt.VRegister36[v-inx4]
      zx-register37 = vdrpt.VRegister37[v-inx4]
      zx-register38 = vdrpt.VRegister38[v-inx4]
      zx-register39 = vdrpt.VRegister39[v-inx4]
      zx-register40 = vdrpt.VRegister40[v-inx4].
  ELSE
    ASSIGN zx-val = 0.
END.  /* end Procedure FindTrimMystery */
/* ----------------------------------------------------------------------- */
PROCEDURE Evaluate_Registers:
/* ----------------------------------------------------------------------- */
  
  DEFINE INPUT PARAMETER        zx-tokenval AS CHARACTER FORMAT "x(1)"  
                                                                  NO-UNDO.
  DEFINE INPUT PARAMETER        zx-counter  AS INTEGER            NO-UNDO.
  DEFINE INPUT PARAMETER        zx-subval   AS DEC                NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register1 AS DEC               NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register2 AS DEC               NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register3 AS DEC               NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register4 AS DEC               NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register5 AS DEC               NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register6 AS DEC               NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register7 AS DEC               NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register8 AS DEC               NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register9 AS DEC               NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register10 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register11 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register12 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register13 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register14 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register15 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register16 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register17 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register18 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register19 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register20 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register21 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register22 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register23 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register24 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register25 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register26 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register27 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register28 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register29 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register30 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register31 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register32 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register33 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register34 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register35 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register36 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register37 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register38 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register39 AS DEC              NO-UNDO.
  DEFINE INPUT PARAMETER        zx-register40 AS DEC              NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-InOrOtherfl    AS LOGICAL      NO-UNDO.
  
  
  ASSIGN 
    v-register2[1]  = zx-register1
    v-register2[2]  = zx-register2
    v-register2[3]  = zx-register3
    v-register2[4]  = zx-register4
    v-register2[5]  = zx-register5
    v-register2[6]  = zx-register6
    v-register2[7]  = zx-register7
    v-register2[8]  = zx-register8
    v-register2[9]  = zx-register9
    v-register2[10] = zx-register10
    v-register2[11] = zx-register11
    v-register2[12] = zx-register12
    v-register2[13] = zx-register13
    v-register2[14] = zx-register14
    v-register2[15] = zx-register15
    v-register2[16] = zx-register16
    v-register2[17] = zx-register17
    v-register2[18] = zx-register18
    v-register2[19] = zx-register19
    v-register2[20] = zx-register20
    v-register2[21] = zx-register21
    v-register2[22] = zx-register22
    v-register2[23] = zx-register23
    v-register2[24] = zx-register24
    v-register2[25] = zx-register25
    v-register2[26] = zx-register26
    v-register2[27] = zx-register27
    v-register2[28] = zx-register28
    v-register2[29] = zx-register29
    v-register2[30] = zx-register30
    v-register2[31] = zx-register31
    v-register2[32] = zx-register32
    v-register2[33] = zx-register33
    v-register2[34] = zx-register34
    v-register2[35] = zx-register35
    v-register2[36] = zx-register36
    v-register2[37] = zx-register37
    v-register2[38] = zx-register38
    v-register2[39] = zx-register39
    v-register2[40] = zx-register40.
  
  
  ASSIGN 
    zx-InOrOtherfl = TRUE.
  
  FOR EACH registers WHERE 
           registers.tokenval = zx-tokenval 
  NO-LOCK:
    
    IF registers.tokeninx > 100 THEN DO:
      IF registers.expression = "c" THEN
        IF zx-counter > registers.vvalue THEN DO:
          ASSIGN 
            zx-InOrOtherfl = FALSE.
          LEAVE.
        END.
        ELSE
          ASSIGN 
            zx-InOrOtherfl = TRUE.
      ELSE
      IF registers.expression = "l" THEN
        IF zx-subval > registers.vvalue THEN DO:
          ASSIGN 
            zx-InOrOtherFl = FALSE.
          LEAVE.
        END.
        ELSE
          ASSIGN 
            zx-InOrOtherFl = TRUE.
      ELSE
      IF registers.expression = "g" THEN
        IF zx-subval < registers.vvalue THEN DO:
          ASSIGN 
            zx-InOrOtherFl = FALSE.
          LEAVE.
        END.
        ELSE
          ASSIGN 
            zx-InOrOtherFl = TRUE.
      ELSE
      IF registers.expression = "le" THEN
        IF zx-subval > registers.vvalue THEN DO:
          ASSIGN 
            zx-InOrOtherFl = FALSE.
          LEAVE.
        END.
        ELSE
          ASSIGN 
            zx-InOrOtherFl = TRUE.
      ELSE
      IF registers.expression = "ge" THEN
        IF zx-subval < registers.vvalue THEN DO:
          ASSIGN 
            zx-InOrOtherFl = FALSE.
          LEAVE.
        END.
        ELSE
          ASSIGN 
            zx-InOrOtherFl = TRUE.
    END. /* IF registers.tokeninx > 100 */
    ELSE DO:
      IF registers.expression = "c" THEN
        IF zx-counter > registers.vvalue THEN DO:
          ASSIGN 
            zx-InOrOtherFl = FALSE.
          LEAVE.
        END.
        ELSE
          ASSIGN 
            zx-InOrOtherFl = TRUE.
      ELSE
      IF registers.expression = "l" THEN
        IF v-register2[registers.registerno] GE registers.vvalue THEN DO:
          ASSIGN 
            zx-InOrOtherFl = FALSE.
          LEAVE.
        END.
        ELSE
          ASSIGN 
            zx-InOrOtherFl = TRUE.
      ELSE
      IF registers.expression = "g" THEN
        IF v-register2[registers.registerno] LE registers.vvalue THEN DO:
          ASSIGN 
            zx-InOrOtherFl = FALSE.
          LEAVE.
        END.
        ELSE
          ASSIGN 
            zx-InOrOtherFl = TRUE.
      ELSE
      IF registers.expression = "le" THEN
        IF v-register2[registers.registerno] > registers.vvalue THEN DO:
          ASSIGN 
            zx-InOrOtherFl = FALSE.
          LEAVE.
        END.
        ELSE
          ASSIGN 
            zx-InOrOtherFl = TRUE.
      ELSE
      IF registers.expression = "ge" THEN
        IF v-register2[registers.registerno] < registers.vvalue THEN
          IF zx-subval < registers.vvalue THEN DO:
            ASSIGN 
              zx-InOrOtherFl = FALSE.
            LEAVE.
          END.
        ELSE
          ASSIGN 
            zx-InOrOtherFl = TRUE.
    END. /* Else Do */
  END.   /* FOR EACH registers */
END.     /* end Procedure Evaluate_Registers */
/* Check_The_Breaks checks token string for breaks */
/* ----------------------------------------------------------------------- */
PROCEDURE Check_The_Breaks:
/* ----------------------------------------------------------------------- */
  IF p-detail = "d" THEN
    ASSIGN
      firstbreak = TRUE.
  DO v-inx = 1 TO u-entitys:
    IF v-lookup[v-inx] <> "x" THEN DO:
      IF ENTRY(v-inx,v-holdsortmystery,v-delim) <>
         ENTRY(v-inx,v-sortmystery,v-delim) THEN DO:
    /* The totalup check is making sure that the entity is not used just for
    sorting. If it is then a control break will not be taken
    */
        IF NOT p-exportl THEN
          RUN Print_Breaks.
        ELSE IF p-dualoutl and p-exportl then DO: 
          RUN Export_Breaks.
          RUN Print_Breaks.
        END.
        ELSE
          RUN Export_Breaks.
          
        LEAVE.
      END.
    END.  /* IF v-lookup[v-inx] <> "x"  */ 
  END.   /* DO v-inx = 1 TO u-entitys */
END.      /* end Procedure Check_The_Breaks */
/* Print_Breaks for the most part checks what the original token in MYSTERY
and does any special processing before performing Summary_Print
*/
/* ----------------------------------------------------------------------- */
PROCEDURE Print_Breaks:
/* ----------------------------------------------------------------------- */
  DO v-inx2 = u-entitys TO v-inx BY -1:
    ASSIGN
      h-lookup = ENTRY(v-inx2,v-holdmystery,v-delim).
    
    
    /* TAH002 */
    ASSIGN 
      v-IgnoreAllOtherFl = FALSE.
    
    /* 
        Check embedded All Other Breaks to see if this is enclosed
        if it is then it is part of the exclusion 
    
    */
    
    DO v-inxI = v-inx2 TO 1 BY -1:
      IF v-inxI > 1 THEN DO:
        IF (v-totalup[v-inxI] = "i" AND
            ENTRY(v-inxI - 1,v-holdsortmystery,v-delim) 
            BEGINS  v-6tildas ) THEN DO:
          ASSIGN 
            v-IgnoreAllOtherFl = TRUE.
          LEAVE.
        END.
      END.
    END.
    
    /* TAH002 */
    IF v-lookup[v-inx2] <> "x" THEN
      RUN Summary_Print.
    
  END.  /* DO v-inx2 = u-entitys TO v-inx  */
END.   /* end Procedure Print_Breaks */
/* 
    Summary_Print
    Formats the generic subtotal line for each given token representation. 
    It also will use V-TOTALUP table which is the formatting requirements 
    from the parameter SORT OPTIONS.
*/
/* ----------------------------------------------------------------------- */
PROCEDURE Summary_Print:
/* ----------------------------------------------------------------------- */
  ASSIGN
    h-genlit = ""
    v-summary-lit2 = "".
    
/* >>>>>>>>> BEGIN INSERTED HOOK user_summaryloadprint */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
  if v-lookup[v-inx2] = "B" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Buyer - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "R" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Region - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "V" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Vendor - " + h-lookup + " " + h-genlit.
    end.
  else
      v-summary-lit = v-lookup[v-inx2].
/* >>>>>>>>>   END INSERTED HOOK user_summaryloadprint */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
   
  
  IF firstbreak AND v-totalup[v-inx2] <> "s" AND
      v-totalup[v-inx2] <> "n" THEN DO:
    ASSIGN
      firstbreak = FALSE.
  END.
  
  
  ASSIGN
    v-astrik = " ".           /* even lowest level summary gets no '*'  */
  
  DO v-inx3 = v-inx2 TO (u-entitys - IF p-detail = "d" THEN 1 ELSE 1):
    ASSIGN
      SUBSTRING(v-astrik,v-inx3,1)  = "*".
  END.
  
  ASSIGN
    v-astrik = REPLACE(v-astrik," ","").
  
  
  IF v-totalup[v-inx2] = "U" THEN
    IF NOT x-stream THEN
      DISPLAY WITH FRAME f-totuline.
    ELSE
      DISPLAY STREAM xpcd WITH FRAME f-totuline.
  
  
  IF (v-totalup[v-inx2] <> "s" AND
      v-totalup[v-inx2] <> "n") AND
    NOT ( v-IgnoreAllOtherFl ) THEN DO:
  /* TA002
  (v-totalup[v-inx2] = "i" and
  entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas)
  */
      
/* >>>>>>>>> BEGIN INSERTED HOOK user_summaryframeprint */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
   assign v-per1 = 
       if t-amounts3[v-inx2] = 0 then
         0
       else
       (t-amounts3[v-inx2] / t-amounts2[v-inx2]) * 100.
  
    if not x-stream then
      do:
      display 
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        t-amounts3[v-inx2]   @ v-amt3
        v-per1               @ v-amt4   
        with frame f-tot.
      down with frame f-tot.
      end.
    else
      do:
      display stream xpcd
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        t-amounts3[v-inx2]   @ v-amt3
        v-per1               @ v-amt4
        with frame f-tot.
      down stream xpcd with frame f-tot.
      end.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
/* >>>>>>>>>   END INSERTED HOOK user_summaryframeprint */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
    
  END.
  
  IF v-totalup[v-inx2] = "p" AND NOT v-finalbreak THEN
    IF NOT x-stream THEN
      PAGE.
    ELSE
      PAGE STREAM xpcd.
  
  
  IF v-totalup[v-inx2] = "l" OR
      v-totalup[v-inx2] = "u" THEN DO:
    DO zzx = 1 TO v-totalupcnt[v-inx2]:
      IF NOT x-stream THEN DO:
        DISPLAY WITH FRAME f-skip.
        DOWN WITH FRAME f-skip.
      END.
      ELSE
      DO:
        DISPLAY STREAM xpcd WITH FRAME f-skip.
        DISPLAY WITH FRAME f-skip.
      END.
    
    END.
  END.
  
  RUN Total_Iteration_Initialization (INPUT v-inx2).
    
END. /* end Procedure Summary_Print */
/* Print_Breaks for the most part checks what the original token in MYSTERY
and does any special processing before performing Summary_Print
*/
/* ----------------------------------------------------------------------- */
PROCEDURE Export_Breaks:
/* ----------------------------------------------------------------------- */
  DO v-inx2 = u-entitys TO v-inx BY -1:
    ASSIGN
      h-lookup = ENTRY(v-inx2,v-holdmystery,v-delim).
    
    /* TAH002 */
    ASSIGN v-IgnoreAllOtherFl = FALSE.
    
    /* Check embedded All Other Breaks to see if this is enclosed
    if it is then it is part of the exclusion */
    
    DO v-inxI = v-inx2 TO 1 BY -1:
      IF v-inxI > 1 THEN DO:
        IF (v-totalup[v-inxI] = "i" AND
        ENTRY(v-inxI - 1,v-holdsortmystery,v-delim) 
        BEGINS  v-6tildas) THEN DO:
          ASSIGN 
            v-IgnoreAllOtherFl = TRUE.
          LEAVE.
        END.
      END.
    END.
    
    /* TAH002 */
    IF NOT CAN-DO("x",v-lookup[v-inx2]) THEN
      RUN Summary_Export.
    
  END.   /* DO v-inx2 = u-entitys TO v-inx  */
END.    /* end Procedure Export_Breaks */
/* Summary_Export
Formats the generic subtotal line for each given token representation. It
also will use V-TOTALUP table which is the formatting requirements from
the parameter SORT OPTIONS.
*/
/* ----------------------------------------------------------------------- */
PROCEDURE Summary_Export:
  /* ----------------------------------------------------------------------- */
  ASSIGN 
    export_rec = "".
  DO v-inx4 = 1 TO u-entitys:
    IF v-totalup[v-inx4] <> "n" THEN DO:
      ASSIGN
        h-genlit = ""
        h-lookup = ENTRY(v-inx4,v-holdmystery,v-delim)
        v-summary-lit2 = "" .
      
/* >>>>>>>>> BEGIN INSERTED HOOK user_summaryloadexport */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
  if v-inx4 > v-inx2  then 
    do:
    if v-inx4 > 1 then          
      export_rec = export_rec + v-del. 
    assign export_rec = export_rec +  " ".
    if v-cells[v-inx4] = "2" then
      assign export_rec = export_rec +  v-del + " ".
    end. 
  else 
  if v-lookup[v-inx4] = "B" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "R" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "V" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "999999999999" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "999999999999" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
    do:
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    assign export_rec = export_rec + v-lookup[v-inx4].
    end.
/* >>>>>>>>>   END INSERTED HOOK user_summaryloadexport */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
     
    END.
  END.
  
  v-astrik = " ".           /*  lowest level summary gets no '*'  */
  DO v-inx3 = v-inx2 TO (u-entitys - IF p-detail = "d" THEN 1 ELSE 1):
    ASSIGN
      SUBSTRING(v-astrik,v-inx3,1)  = "*".
  END.
  
  ASSIGN
    v-astrik = REPLACE(v-astrik," ","").
  
   
  IF (v-totalup[v-inx2] <> "s" AND
      v-totalup[v-inx2] <> "n") AND
      NOT
      (v-IgnoreAllOtherFl ) THEN DO:
  
  /*  TAH002 
  old before ignore fix
   (v-totalup[v-inx2] = "i" and
  entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas)
  
  */
  
/* >>>>>>>>> BEGIN INSERTED HOOK user_summaryputexport */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
   assign v-per1 = 
       if t-amounts3[u-entitys + 1] = 0 then
         0
       else
       (t-amounts3[u-entitys + 1] / t-amounts2[u-entitys + 1]) * 100.
 
 
 
    assign export_rec = v-astrik + v-del + export_rec.
    assign export_rec = export_rec + v-del +
      string(t-amounts1[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts2[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts3[v-inx2],"->>>>>>>>9") + v-del +
      string(v-per1,"->99.99").
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
/* >>>>>>>>>   END INSERTED HOOK user_summaryputexport */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
    
    IF p-exportDWl THEN
      ASSIGN 
        export_rec = export_rec + CHR(13) + CHR(10).
    ELSE
      ASSIGN 
        export_rec = export_rec + CHR(13).
    IF (p-exportDWl AND v-astrik = "") OR
        NOT p-exportDWl THEN
      PUT STREAM expt UNFORMATTED export_rec.
/* >>>>>>>>> BEGIN INSERTED HOOK user_summaryputexportaftput */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
/*    THIS HOOK WAS NOT INCLUDED IN THE USERS INCLUDE !!!! */ 
/* >>>>>>>>>   END INSERTED HOOK user_summaryputexportaftput */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
  
  END. /* IF (v-totalup[v-inx2] <> "s" AND */
  
  IF v-totalup[v-inx2] = "p" THEN DO:
    IF v-totalup[v-inx2] = "l" THEN DO:
      DO zzx = 1 TO v-totalupcnt[v-inx2]:
        IF p-export BEGINS "DW_" THEN
    /* put stream expt unformatted " " chr(13) chr(10). */
          ASSIGN 
            p-exportDWl = p-exportDWl.
        ELSE
          PUT STREAM expt UNFORMATTED " " CHR(13).
      END.
    END.
  END.  
 
  IF NOT p-dualoutl THEN
    RUN Total_Iteration_Initialization (INPUT v-inx2).
  
  
END. /* end Procedure Summary_Export */
/* ----------------------------------------------------------------------- */
PROCEDURE Detail_Export:
/* ----------------------------------------------------------------------- */
  ASSIGN
    export_rec = "".
  DO v-inx4 = 1 TO u-entitys:
    IF v-totalup[v-inx4] <> "n" THEN DO:
      ASSIGN
        h-genlit = ""
        h-lookup = ENTRY(v-inx4,v-holdmystery,v-delim)
        v-summary-lit2 = "".
      
/* >>>>>>>>> BEGIN INSERTED HOOK user_summaryloadexport */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
  if v-inx4 > v-inx2  then 
    do:
    if v-inx4 > 1 then          
      export_rec = export_rec + v-del. 
    assign export_rec = export_rec +  " ".
    if v-cells[v-inx4] = "2" then
      assign export_rec = export_rec +  v-del + " ".
    end. 
  else 
  if v-lookup[v-inx4] = "B" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "R" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "V" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "999999999999" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "999999999999" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
    do:
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    assign export_rec = export_rec + v-lookup[v-inx4].
    end.
/* >>>>>>>>>   END INSERTED HOOK user_summaryloadexport */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
       
      /* Above should be the same as the summary */
    END.
  END.
  
  ASSIGN
    v-astrik = " "           /*  lowest level summary gets no '*'  */
    v-astrik = REPLACE(v-astrik," ","").
  
  
/* >>>>>>>>> BEGIN INSERTED HOOK user_detailputexport */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
/*    THIS HOOK WAS NOT INCLUDED IN THE USERS INCLUDE !!!! */ 
/* >>>>>>>>>   END INSERTED HOOK user_detailputexport */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
  
  IF p-export BEGINS "DW_" THEN
    ASSIGN 
      export_rec = export_rec + CHR(13) + CHR(10).
  ELSE
    ASSIGN 
      export_rec = export_rec + CHR(13).
  
  PUT STREAM expt UNFORMATTED export_rec.
  
  
END. /* end Procedure Detail_Export */
/* These few procedures are in procedures just to keep code 
   segment size down as small as possible
*/   
/* ----------------------------------------------------------------------- */
PROCEDURE Amount_Accumulation:
/* ----------------------------------------------------------------------- */
  DEFINE INPUT PARAMETER ip-inx AS INTEGER NO-UNDO.
  
  ASSIGN
    t-amounts1[ip-inx]  = t-amounts1[ip-inx] + drpt.xval[1]
    t-amounts2[ip-inx]  = t-amounts2[ip-inx] + drpt.xval[2]
    t-amounts3[ip-inx]  = t-amounts3[ip-inx] + drpt.xval[3]
    t-amounts4[ip-inx]  = t-amounts4[ip-inx] + drpt.xval[4]
    t-amounts5[ip-inx]  = t-amounts5[ip-inx] + drpt.xval[5]
    t-amounts6[ip-inx]  = t-amounts6[ip-inx] + drpt.xval[6]
    t-amounts7[ip-inx]  = t-amounts7[ip-inx] + drpt.xval[7]
    t-amounts8[ip-inx]  = t-amounts8[ip-inx] + drpt.xval[8]
    t-amounts9[ip-inx]  = t-amounts9[ip-inx] + drpt.xval[9]
    t-amounts10[ip-inx] = t-amounts10[ip-inx] + drpt.xval[10]
    t-amounts11[ip-inx] = t-amounts11[ip-inx] + drpt.xval[11]
    t-amounts12[ip-inx] = t-amounts12[ip-inx] + drpt.xval[12]
    t-amounts13[ip-inx] = t-amounts13[ip-inx] + drpt.xval[13]
    t-amounts14[ip-inx] = t-amounts14[ip-inx] + drpt.xval[14]
    t-amounts15[ip-inx] = t-amounts15[ip-inx] + drpt.xval[15]
    t-amounts16[ip-inx] = t-amounts16[ip-inx] + drpt.xval[16]
    t-amounts17[ip-inx] = t-amounts17[ip-inx] + drpt.xval[17]
    t-amounts18[ip-inx] = t-amounts18[ip-inx] + drpt.xval[18]
    t-amounts19[ip-inx] = t-amounts19[ip-inx] + drpt.xval[19]
    t-amounts20[ip-inx] = t-amounts20[ip-inx] + drpt.xval[20]
    t-amounts21[ip-inx] = t-amounts21[ip-inx] + drpt.xval[21]
    t-amounts22[ip-inx] = t-amounts22[ip-inx] + drpt.xval[22]
    t-amounts23[ip-inx] = t-amounts23[ip-inx] + drpt.xval[23]
    t-amounts24[ip-inx] = t-amounts24[ip-inx] + drpt.xval[24]
    t-amounts25[ip-inx] = t-amounts25[ip-inx] + drpt.xval[25]
    t-amounts26[ip-inx] = t-amounts26[ip-inx] + drpt.xval[26]
    t-amounts27[ip-inx] = t-amounts27[ip-inx] + drpt.xval[27]
    t-amounts28[ip-inx] = t-amounts28[ip-inx] + drpt.xval[28]
    t-amounts29[ip-inx] = t-amounts29[ip-inx] + drpt.xval[29]
    t-amounts30[ip-inx] = t-amounts30[ip-inx] + drpt.xval[30]
    t-amounts31[ip-inx] = t-amounts31[ip-inx] + drpt.xval[31]
    t-amounts32[ip-inx] = t-amounts32[ip-inx] + drpt.xval[32]
    t-amounts33[ip-inx] = t-amounts33[ip-inx] + drpt.xval[33]
    t-amounts34[ip-inx] = t-amounts34[ip-inx] + drpt.xval[34]
    t-amounts35[ip-inx] = t-amounts35[ip-inx] + drpt.xval[35]
    t-amounts36[ip-inx] = t-amounts36[ip-inx] + drpt.xval[36]
    t-amounts37[ip-inx] = t-amounts37[ip-inx] + drpt.xval[37]
    t-amounts38[ip-inx] = t-amounts38[ip-inx] + drpt.xval[38]
    t-amounts39[ip-inx] = t-amounts39[ip-inx] + drpt.xval[39]
    t-amounts40[ip-inx] = t-amounts40[ip-inx] + drpt.xval[40]
    t-amounts41[ip-inx] = t-amounts41[ip-inx] + drpt.xval[41]
    t-amounts42[ip-inx] = t-amounts42[ip-inx] + drpt.xval[42]
    t-amounts43[ip-inx] = t-amounts43[ip-inx] + drpt.xval[43]
    t-amounts44[ip-inx] = t-amounts44[ip-inx] + drpt.xval[44]
    t-amounts45[ip-inx] = t-amounts45[ip-inx] + drpt.xval[45]
    t-amounts46[ip-inx] = t-amounts46[ip-inx] + drpt.xval[46]
    t-amounts47[ip-inx] = t-amounts47[ip-inx] + drpt.xval[47]
    t-amounts48[ip-inx] = t-amounts48[ip-inx] + drpt.xval[48]
    t-amounts49[ip-inx] = t-amounts49[ip-inx] + drpt.xval[49]
    t-amounts50[ip-inx] = t-amounts50[ip-inx] + drpt.xval[50]
    t-amounts51[ip-inx] = t-amounts51[ip-inx] + drpt.xval[51]
    t-amounts52[ip-inx] = t-amounts52[ip-inx] + drpt.xval[52]
    t-amounts53[ip-inx] = t-amounts53[ip-inx] + drpt.xval[53]
    t-amounts54[ip-inx] = t-amounts54[ip-inx] + drpt.xval[54]
    t-amounts55[ip-inx] = t-amounts55[ip-inx] + drpt.xval[55]
    t-amounts56[ip-inx] = t-amounts56[ip-inx] + drpt.xval[56]
    t-amounts57[ip-inx] = t-amounts57[ip-inx] + drpt.xval[57]
    t-amounts58[ip-inx] = t-amounts58[ip-inx] + drpt.xval[58]
    t-amounts59[ip-inx] = t-amounts59[ip-inx] + drpt.xval[59]
    t-amounts60[ip-inx] = t-amounts60[ip-inx] + drpt.xval[60].
END.   /* end Procedure Amount_Accumulation */
/* ----------------------------------------------------------------------- */
PROCEDURE Total_Iteration_Initialization:
/* ----------------------------------------------------------------------- */
  DEFINE INPUT PARAMETER ip-inx AS INTEGER NO-UNDO.
  
  ASSIGN
    t-amounts1[ip-inx]  = 0
    t-amounts2[ip-inx]  = 0
    t-amounts3[ip-inx]  = 0
    t-amounts4[ip-inx]  = 0
    t-amounts5[ip-inx]  = 0
    t-amounts6[ip-inx]  = 0
    t-amounts7[ip-inx]  = 0
    t-amounts8[ip-inx]  = 0
    t-amounts9[ip-inx]  = 0
    t-amounts10[ip-inx] = 0
    t-amounts11[ip-inx] = 0
    t-amounts12[ip-inx] = 0
    t-amounts13[ip-inx] = 0
    t-amounts14[ip-inx] = 0
    t-amounts15[ip-inx] = 0
    t-amounts16[ip-inx] = 0
    t-amounts17[ip-inx] = 0
    t-amounts18[ip-inx] = 0
    t-amounts19[ip-inx] = 0
    t-amounts20[ip-inx] = 0
    t-amounts21[ip-inx] = 0
    t-amounts22[ip-inx] = 0
    t-amounts23[ip-inx] = 0
    t-amounts24[ip-inx] = 0
    t-amounts25[ip-inx] = 0
    t-amounts26[ip-inx] = 0
    t-amounts27[ip-inx] = 0
    t-amounts28[ip-inx] = 0
    t-amounts29[ip-inx] = 0
    t-amounts30[ip-inx] = 0
    t-amounts31[ip-inx] = 0
    t-amounts32[ip-inx] = 0
    t-amounts33[ip-inx] = 0
    t-amounts34[ip-inx] = 0
    t-amounts35[ip-inx] = 0
    t-amounts36[ip-inx] = 0
    t-amounts37[ip-inx] = 0
    t-amounts38[ip-inx] = 0
    t-amounts39[ip-inx] = 0
    t-amounts40[ip-inx] = 0
    t-amounts41[ip-inx] = 0
    t-amounts42[ip-inx] = 0
    t-amounts43[ip-inx] = 0
    t-amounts44[ip-inx] = 0
    t-amounts45[ip-inx] = 0
    t-amounts46[ip-inx] = 0
    t-amounts47[ip-inx] = 0
    t-amounts48[ip-inx] = 0
    t-amounts49[ip-inx] = 0
    t-amounts50[ip-inx] = 0
    t-amounts51[ip-inx] = 0
    t-amounts52[ip-inx] = 0
    t-amounts53[ip-inx] = 0
    t-amounts54[ip-inx] = 0
    t-amounts55[ip-inx] = 0
    t-amounts56[ip-inx] = 0
    t-amounts57[ip-inx] = 0
    t-amounts58[ip-inx] = 0
    t-amounts59[ip-inx] = 0
    t-amounts60[ip-inx] = 0.
END.   /* end Procedure Total_Iteration_Initialization */    
/* 
  Since the building of SORTMYSTERY is token based and also serves as a
  coalating tool in reporting. These procedures build the closest thing to
  ASCII bit reversal as possible. Since this PROGRESS is configured to
  treat the upper and lower case alphas as equivalents the processes
  reflect that.
*/
/* Coalation procedures */
/* ----------------------------------------------------------------------- */
PROCEDURE Ascii_Coalation:
/* ----------------------------------------------------------------------- */
  
  CREATE ascii_bit_swaper.
  ASSIGN
    ascii_bit_swaper.CHAR = ""
    ascii_bit_swaper.DEC  = 0
    ascii_bit_swaper.undec = 127.
  DO ascii_index = 2 TO 127:
    /* skip lower case because for some reason they have the same colate seq */
    IF  ascii_index GE 97 AND ascii_index LE 122 THEN
      NEXT.
    
    CREATE ascii_bit_swaper.
    ASSIGN
      ascii_bit_swaper.CHAR = CHR(ascii_index)
      ascii_bit_swaper.DEC  = ascii_index.
  END.
  
  
  ASSIGN
    ascii_index = 127.
  FOR EACH ascii_bit_swaper:
    ASSIGN
      ascii_bit_swaper.undec = ascii_index
      ascii_index = ascii_index - 1.
    /* skip lower case because for some reason they have the same colate seq */
    IF ascii_index = 122 THEN
      ASSIGN 
        ascii_index = 96.
    
    IF ascii_index = 1 THEN
      ASSIGN 
        ascii_index = 0.
  END.
END.  /* end Procedure Ascii_Coalation */
/* ----------------------------------------------------------------------- */
PROCEDURE Ascii_Make_It_NOT:
/* ----------------------------------------------------------------------- */
  
  DEFINE INPUT-OUTPUT PARAMETER asciistring AS CHAR.
  
  ASSIGN
    ascii_length = LENGTH(asciistring).
  
  DO ascii_index = 1 TO ascii_length:
    FIND ascii_bit_swaper WHERE 
         ascii_bit_swaper.DEC = 
              asc(CAPS(SUBSTRING(asciistring,ascii_index,1)))
    NO-LOCK NO-ERROR.
    IF NOT avail ascii_bit_swaper THEN
      NEXT.
    ELSE
      OVERLAY(asciistring,ascii_index,1) =
        CHR(ascii_bit_swaper.undec).
  END.
END.   /* end Procedure Ascii_Make_It_NOT */
/* ----------------------------------------------------------------------- */
PROCEDURE Ascii_Make_It_Number:
  /* ----------------------------------------------------------------------- */
  
  DEFINE INPUT        PARAMETER makeitnot AS LOGICAL.
  DEFINE INPUT        PARAMETER d_number AS DECIMAL.
  DEFINE INPUT-OUTPUT PARAMETER asciistring AS CHAR.
  
  ASSIGN 
    asciistring = STRING(d_number,"+999999999.99").
  IF d_number GE 0 THEN
    OVERLAY(asciistring,1,1) = "`".
  /*
    Simply put the above statement is making a positive number + a ` 
    this is done because for some odd reason a + is a lower binary 
    value than a -. Therefore creating a problem a ` is the highest...simple 
    adjustment.
  
    Below what is happening is developing a negative numbers sequence. Since
    in a string a higher number is higher because the sign is not understood.
    I'm reversing a - numbers bits so it is the opposite of that. Think about
    it, it really does make sense
  
  */
  
  IF (makeitnot AND d_number < 0) OR
      (NOT makeitnot AND d_number GE 0) THEN
    RETURN.
  ASSIGN 
    ascii_length = LENGTH(asciistring).
  DO ascii_index = 1 TO ascii_length:
    FIND ascii_bit_swaper WHERE 
         ascii_bit_swaper.DEC =
            asc(CAPS(SUBSTRING(asciistring,ascii_index,1)))
    NO-LOCK NO-ERROR.
    IF NOT avail ascii_bit_swaper THEN
      NEXT.
    ELSE
      OVERLAY(asciistring,ascii_index,1) =
        CHR(ascii_bit_swaper.undec).
  END.
END.   /* end Procedure Ascii_Make_It_Number */
/* This procedure interfaces to the Report Generator Parameter code */
/* ----------------------------------------------------------------------- */
PROCEDURE ReportOpts:
/* ----------------------------------------------------------------------- */
  DEFINE INPUT        PARAMETER zx-sapbuser5 AS CHAR            NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-options   AS CHAR            NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-sortopt   AS CHAR            NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-totalopt  AS CHAR            NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-summopt   AS CHAR            NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-regopt    AS CHAR            NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER zx-regexpr   AS CHAR            NO-UNDO.
  
  DEFINE VAR v-inx     AS INTEGER NO-UNDO.
  DEFINE VAR v-entitys AS INTEGER NO-UNDO.
  DEFINE VAR v-size    AS INTEGER NO-UNDO.
  
  ASSIGN 
    zx-sapbuser5 = REPLACE(zx-sapbuser5," ","")
    v-size = LENGTH(zx-sapbuser5).
  
  IF v-size = 0 THEN DO:
    MESSAGE "No Report Options were generated".
    RETURN.
  END.
  
  ASSIGN
    v-entitys = 0.
  
  DO v-inx = 1 TO v-size:
    IF SUBSTRING(zx-sapbuser5,v-inx,1) = "~011" THEN
      ASSIGN 
        v-entitys = v-entitys + 1.
  END.
  
  ASSIGN 
    v-entitys = v-entitys + 1.
  
  DO v-inx = 1 TO v-entitys:
    IF v-inx = 1 THEN
      zx-options = ENTRY(v-inx,zx-sapbuser5,"~011").
    IF v-inx = 2 THEN
      zx-sortopt = ENTRY(v-inx,zx-sapbuser5,"~011").
    IF v-inx = 3 THEN
      zx-totalopt = ENTRY(v-inx,zx-sapbuser5,"~011").
    IF v-inx = 4 THEN
      zx-summopt = ENTRY(v-inx,zx-sapbuser5,"~011").
    IF v-inx = 5 THEN
      zx-regopt = ENTRY(v-inx,zx-sapbuser5,"~011").
    IF v-inx = 6 THEN
      zx-regexpr = ENTRY(v-inx,zx-sapbuser5,"~011").
  END.
  
END.    /* end Procedure ReportOpts */
/* 
  This procedure interfaces to the Report Generator Parameter code
  and prints a page for the user
*/
/* ----------------------------------------------------------------------- */
PROCEDURE Print_ReportOpts:
/* ----------------------------------------------------------------------- */
  DEFINE INPUT        PARAMETER zx-sapbrecid    AS RECID           NO-UNDO.
  DEFINE INPUT        PARAMETER zx-cono         AS INT             NO-UNDO.
  
  
  RUN zsdigenprint (INPUT zx-sapbrecid,
                    INPUT zx-cono).
  
END.
/* ----------------------------------------------------------------------- */
PROCEDURE String_Ready:
/* ----------------------------------------------------------------------- */
  
  DEFINE INPUT        PARAMETER String_Raw   AS CHARACTER NO-UNDO.
  DEFINE INPUT-OUTPUT PARAMETER String_Ready AS CHARACTER NO-UNDO.
  
  ASSIGN
    x-fnd = INDEX(String_Raw,"""",1).
  IF x-fnd = 0 THEN DO:
    ASSIGN 
      String_Ready = """" + String_Raw + """".
    RETURN.
  END.
  ELSE DO:
    ASSIGN 
      String_Ready = """"
      String_Raw = RIGHT-TRIM(String_Raw)
      x-len = LENGTH(String_Raw).
    DO x-fnd = 1 TO x-len:
      IF SUBSTRING(String_Raw,x-fnd,1) = """" THEN
        ASSIGN 
          String_Ready = String_Ready + """".
      
      ASSIGN 
        String_Ready = String_Ready + SUBSTRING(String_Raw,x-fnd,1).
    END.
    ASSIGN 
      String_Ready = String_Ready + """".
  END.
  
END. /* end Procedure String_Ready */
/* All user procedures are placed below */
/* >>>>>>>>> BEGIN INSERTED HOOK user_procedures */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
procedure sapb_vars:
  assign b-buyer  = sapb.rangebeg[1]
         e-buyer  = sapb.rangeend[1]
         b-vendno = dec(sapb.rangebeg[2])
         e-vendno = dec(sapb.rangeend[2])
         b-region = sapb.rangebeg[3]
         e-region = sapb.rangeend[3]
         b-enterdt = date(sapb.rangebeg[4])
         e-enterdt = date(sapb.rangeend[4])
       /*  o-sort  = sapb.optvalue[1].  */
         p-format  = int(sapb.optvalue[1])
         p-export  = sapb.optvalue[2].
      assign p-detailprt = yes
             p-detail    = "D"
             p-portrait  = no.
             
             
  run formatoptions.
end.
/* ------------------------------------------------------------------------- */
procedure formatoptions:
/* ------------------------------------------------------------------------- */
   
   
  assign p-portrait = false.
  /* if false then the report will print in LANDSCAPE 178 character */
  
  if sapb.optvalue[1] > "0" and sapb.optvalue[1] ne "99" then 
    do:
    p-optiontype = " ".
    find notes where notes.cono = g-cono and
                     notes.notestype = "zz" and
                     notes.primarykey = "porzatemp" and  /* tbxr program */
                     notes.secondarykey = string(p-format) no-lock no-error.
    if not avail notes then
      do:
      display "Format is not valid cannot process request".
      assign p-optiontype = "r"
             p-sorttype   = ">,"
             p-totaltype  = "l"
             p-summcounts = "a".
      return.
      end.               
   
    assign p-optiontype = notes.noteln[1]
           p-sorttype   = notes.noteln[2]
           p-totaltype  = notes.noteln[3]
           p-summcounts = notes.noteln[4]
           p-export     = "    "
           p-register   = notes.noteln[5]
           p-registerex = notes.noteln[6].
    end.     
  else
  if sapb.optvalue[1] = "99" then 
    do:
    assign p-register   = ""
           p-registerex = "".
    run reportopts(input sapb.user5, 
                   input-output p-optiontype,
                   input-output p-sorttype,  
                   input-output p-totaltype,
                   input-output p-summcounts,
                   input-output p-register,
                   input-output p-regval).
    end.
 end.
   
 
procedure extract_data: 
for each poeh where poeh.cono       = g-cono and    
                    poeh.transtype <> "rm" and
                    poeh.stagecd    = 2 and
                   (poeh.buyer     >= b-buyer and
                    poeh.buyer     <= e-buyer) and
                   (poeh.vendno    >= b-vendno and
                    poeh.vendno    <= e-vendno)
                    no-lock:
  find oimsp where oimsp.person = poeh.buyer and
                  (oimsp.responsible >= b-region and
                   oimsp.responsible <= e-region)
                   no-lock no-error.
                         
    if not avail oimsp then next.
           
    for each poel where poel.cono        = poeh.cono and 
                        poel.pono        = poeh.pono and
                        poel.posuf       = poeh.posuf and
                        poel.nonstockty <> "R" and
                        substr(poel.user1,1,1) <> "x" and
                        poel.enterdt <= e-enterdt
                        no-lock:
    
     
     
find porzatemp where porzatemp.buyer = poeh.buyer no-error. 
    
 if avail porzatemp then do:
    /* message "avail porzatemp" porzatemp.buyer.   */
    /* message "poel.pono" poel.pono.   */
     if (poel.enterdt >= b-enterdt and poel.enterdt <= e-enterdt) then
     do:
       if porzatemp.pono <> poeh.pono then
          assign porzatemp.tot-po = porzatemp.tot-po + 1
                 porzatemp.pono   = poeh.pono.
        
            
          run countdays.p (input poel.user9,    
                           input poel.enterdt,
                           output v-weekdays).
         
         if v-weekdays <= 3 then do:
   
          if substr(poel.user1,1,1) = "a" or substr(poel.user1,1,1) = "s" then                assign porzatemp.tot-ack = porzatemp.tot-ack + 1.  
             /* message "ackcount " poel.pono.  */
           end. /* <= 3 */
            
            
            assign porzatemp.tot-line = porzatemp.tot-line + 1     
                   porzatemp.tot-per  = (porzatemp.tot-ack /                   
                   porzatemp.tot-line) * 100.    
                   porzatemp.un3      = porzatemp.tot-line - porzatemp.tot-ack.
       end. /* between dates */
       
       if (substr(poel.user1,1,1) = " " or 
           substr(poel.user1,1,1) = "n" or
           substr(poel.user1,1,1) = "r") then 
       do:   
           
              
           run countdays.p (input today, 
                            input poel.enterdt,                            output v-weekdays).  
             
             if v-weekdays >= 21 then do:
                assign porzatemp.ov21 = porzatemp.ov21 + 1.         
                /* message " >=21 " poel.pono.      */
             end.
               
               
             if v-weekdays >= 11 and v-weekdays <= 20 then do:  
                assign porzatemp.un11-20 = porzatemp.un11-20 + 1.   
                /* message " >11<20" poel.pono.     */
             end.
             if v-weekdays >= 4 and v-weekdays <= 10 then do:   
                assign porzatemp.un4-10 = porzatemp.un4-10 + 1. 
                  /* message ">4<10" poel.pono.         */
             end.
             
            end. /* if substr(poel.user1,1,1) */
 
 
 end.     /* if avail porzatemp  */         
        
 else if not avail porzatemp then do:   
 
        create porzatemp.
          assign porzatemp.buyer       = poeh.buyer
                 porzatemp.name        = oimsp.name
                 porzatemp.region      = oimsp.responsible
                 porzatemp.pono        = poeh.pono
                 porzatemp.tot-po      = 0
                 porzatemp.tot-line    = 0
                 porzatemp.tot-ack     = 0
                 porzatemp.tot-per     = 0.
         
         run countdays.p (input poel.user9,    
                          input poel.enterdt,
                          output v-weekdays).
         
       
       if (poel.enterdt >= b-enterdt and poel.enterdt <= e-enterdt) then
       do:
         
         if v-weekdays <= 3 then do:
   
          if substr(poel.user1,1,1) = "a" or substr(poel.user1,1,1) = "s" then                 assign porzatemp.tot-ack = porzatemp.tot-ack + 1.     
              /* message "ackcount2" poel.pono. */
            
         end. /* <= 3 */
            
            
            assign porzatemp.tot-line = porzatemp.tot-line + 1     
                   porzatemp.tot-per  = (porzatemp.tot-ack /                   
                   porzatemp.tot-line) * 100.    
                   porzatemp.un3      = porzatemp.tot-line - porzatemp.tot-ack.
      
       end. /* between dates */
       
       if (substr(poel.user1,1,1) = " " or 
           substr(poel.user1,1,1) = "n" or
           substr(poel.user1,1,1) = "r") then 
       do:   
           
              
           run countdays.p (input today, 
                            input poel.enterdt,
                            output v-weekdays).  
             
             if v-weekdays >= 21 then do:
                assign porzatemp.ov21 = porzatemp.ov21 + 1.         
                /* message " >=21 " poel.pono.  */
             end.
               
               
             if v-weekdays >= 11 and v-weekdays <= 20 then do:  
                assign porzatemp.un11-20 = porzatemp.un11-20 + 1.   
                /* message " >11<20" poel.pono.     */
             end.
             if v-weekdays >= 4 and v-weekdays <= 10 then do:  
                assign porzatemp.un4-10 = porzatemp.un4-10 + 1. 
                  /* message ">4<10" poel.pono.         */
             end.
             
       end. /* if substr(poel.user1,1,1) */
        end.     /* else create               */
    end.         /* for each poel             */
end.             /* for each poeh             */    
end.  /* extract data */        
        
/* ------------------------------------------------------------------------- */
procedure print-dtl:
/* ------------------------------------------------------------------------- */
define input parameter ip-recid as recid no-undo.
find porzatemp where recid(porzatemp) = ip-recid no-lock no-error.
    display porzatemp.buyer
            porzatemp.name 
            porzatemp.tot-po
            porzatemp.tot-line
            porzatemp.tot-ack 
            porzatemp.tot-per
        /*  porzatemp.un3   */
            porzatemp.un4-10 
            porzatemp.un11-20 
            porzatemp.ov21
            with frame porzatemp-dtl.
    down with frame porzatemp-dtl.
end. /* procedure print-dtl */  
  
 
 
/* ------------------------------------------------------------------------- */
procedure print-dtl-export:
/* ------------------------------------------------------------------------- */
define input parameter ip-recid as recid no-undo.
find porzatemp where recid(porzatemp) = ip-recid no-lock no-error.
            
  put stream expt unformatted
            "   "
            v-del
            porzatemp.buyer 
            v-del
            porzatemp.name  
            v-del
            porzatemp.tot-po    
            v-del
            porzatemp.tot-line  
            v-del
            porzatemp.tot-ack   
            v-del
            porzatemp.tot-per   
            v-del
            porzatemp.un3   
            v-del
            porzatemp.un4-10    
            v-del
            porzatemp.un11-20   
            v-del
            porzatemp.ov21  
            chr(13).
end. /* procedure print-dtl */  
  
 
 
  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
/* >>>>>>>>>   END INSERTED HOOK user_procedures */
/* <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> */
 
