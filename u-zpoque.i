
/* u-zpoque.i */
/* ------------------------------------------------------------------------
  Program Name :  U-ZPOQUE.I

  Description  :  Update module containing function key logic 
                  module for POEZQ using xxixx.i

  Author       :  Thomas Herzog


------------------------------------------------------------------------- */

def buffer b2-zidc for zidc.

if {k-func7.i} then
  do:
  s-position = frame-line(f-poquel).
  if not avail poel then
    do:
    assign g-pono = dec(substring(o-frame-value,2,7))
           g-posuf = int(substring(o-frame-value,10,2))
           g-polineno = int(substring(o-frame-value,13,3)).
      
        
    find poel                   where poel.cono = g-cono and
                                      poel.pono = g-pono and
                                      poel.posuf = g-posuf and
                                      poel.lineno = g-polineno
                                      no-lock no-error.
    end.

  assign b-cost = poel.price
         g-shipfmno = poel.shipfmno
         b-buyer = poeh.buyer
         g-vendno = poel.vendno
         b-shipinstr = poeh.shipinstr
         b-shipvia = poeh.shipviaty.
          
  
  find first poelo where poelo.cono = poel.cono and
                         poelo.pono = poel.pono and
                         poelo.posuf = poel.posuf and
                         poelo.lineno = poel.lineno no-lock no-error.
  if avail poelo then
    do: 
    find oeeh  where oeeh.cono = g-cono and
                     oeeh.orderno = poelo.orderaltno and
                     oeeh.ordersuf = poelo.orderaltsuf no-lock no-error.

    end. 
  
  if avail oeeh then
    do:
    find arsc where arsc.cono = g-cono and 
                    arsc.custno = oeeh.custno no-lock no-error.
    find oimsp where oimsp.person = arsc.creditmgr no-lock no-error.
   
  
    assign d-crmgr  = arsc.creditmgr
           d-crname  = if avail oimsp then oimsp.name else " ".
    end.
  else
    assign d-crmgr = " "
           d-crname  = " ". 
  display poel.qtyord
          z-avail
          z-coavail 
          d-crmgr
          d-crname with frame f-poqued.
  stain:
  do while true: 
  update g-vendno g-shipfmno b-buyer b-cost b-shipinstr b-shipvia
    go-on (f6 f7 f8 f9 f10) with frame f-poqued editing:
    readkey.    
  
    
    if {k-after.i} or {k-func.i} then
      do:
       if frame-field = "g-vendno" then do: 
         if (input g-vendno <> poel.vendno and
             input g-vendno <> g-vendno ) then 
           do:
            Message "** Warning this change effects entire Purchase Order".    
            pause.
            hide message.
           end.
       end.
       else
       if frame-field = "g-shipfmno" then  do:
         if input g-shipfmno <> poel.shipfmno then 
           do:
            Message "** Warning this change effects entire Purchase Order".
           end.
       end.
       if frame-field = "b-buyer" then  do:
         if input b-buyer <> poeh.buyer then 
           do:
            Message "** Warning this change effects entire Purchase Order".
           end.
       end.
       else
       if frame-field = "b-shipinstr" then do:
         if input b-shipinstr <> poeh.shipinstr then 
           do:
           Message "** Warning this change effects entire Purchase Order".
           end.
       end. /* added */    
        else
        if frame-field = "b-shipvia" then do:
         if input b-shipvia <> poeh.shipviaty then 
           do:
           Message "** Warning this change effects entire Purchase Order".
           end.
        end.
      
      assign g-vendno b-cost g-shipfmno b-buyer b-shipinstr b-shipvia.
      end.
    apply lastkey.
  end.

  
  if g-vendno <> poel.vendno or
     g-shipfmno <> poel.shipfmno or
     b-buyer <> poeh.buyer or
     b-shipinstr <> poeh.shipinstr or 
     b-shipvia <> poeh.shipvia  then
    do:
    
    find z-apsv where z-apsv.cono = g-cono and
                      z-apsv.vendno = g-vendno no-lock no-error.

    if g-shipfmno <> 0 then
      find z-apss where z-apss.cono = g-cono and
                        z-apss.vendno = g-vendno and
                        z-apss.shipfmno = g-shipfmno  no-lock no-error.
     if not avail z-apsv or
        (g-shipfmno <> 0 and not avail z-apss) then              
       do:
         Message "Vendor (APSV) or ShipFm (APSS) is not valid".
         bell.
         next stain.
       end.
   end. /* added */
  leave stain.
  /* end. removed */
end.  
 
 
 
 
 
 
 
 
  if b-cost <> poel.price then
    do:
    
             
    find poeh               where poeh.cono = g-cono and
                                  poeh.pono = g-pono and
                                  poeh.posuf = g-posuf                                                            no-error.


 
            
    find poel               where poel.cono = g-cono and
                                  poel.pono = g-pono and
                                  poel.posuf = g-posuf and
                                  poel.lineno = g-polineno
                                  no-error.
    
    find apsv where apsv.cono = 1 and
                    apsv.vendno = poel.vendno no-error.
    
    
    z-licost = poel.price * poel.qtyord.
    assign poeh.totlineamt = poeh.totlineamt - z-licost
           poeh.totlineamt = poeh.totlineamt - z-licost
           poel.netamt = poel.netamt - z-licost
           apsv.ordbal = apsv.ordbal - z-licost.
    
    z-licost = b-cost * poel.qtyord.
    assign poeh.totlineamt = poeh.totlineamt + z-licost
           poeh.totlineamt = poeh.totlineamt + z-licost
           poel.netamt = poel.netamt + z-licost
           apsv.ordbal = apsv.ordbal + z-licost.
    assign poel.price = b-cost.        

  
    for each poelo where poelo.cono = poel.cono and
                         poelo.pono = poel.pono and
                         poelo.posuf = poel.posuf and
                         poelo.lineno = poel.lineno:
      if poelo.seqno <> 0 then
        do: 
      
        find oeeh  where oeeh.cono = g-cono and
                         oeeh.orderno = poelo.orderaltno and
                         oeeh.ordersuf = poelo.orderaltsuf no-error.

         
        find oeel where oeel.cono = g-cono and
                        oeel.orderno = poelo.orderaltno and
                        oeel.ordersuf = poelo.orderaltsuf and
                        oeel.lineno = poelo.linealtno                                                   no-error.
        
        find oeelk where oeelk.cono = g-cono and
                         oeelk.ordertype = "o" and
                         oeelk.orderno = poelo.orderaltno and
                         oeelk.ordersuf = poelo.orderaltsuf and
                         oeelk.lineno = poelo.linealtno and
                         oeelk.seqno = poelo.seqno no-error.
        if avail oeelk  and can-do("s,n",oeelk.specnstype)
                                               then
          do:
              
          z-licost = oeelk.prodcost * oeelk.qtyord.
          assign oeeh.totcostord = oeeh.totcostord - z-licost
                 oeel.prodcost   = oeel.prodcost - z-licost.
          z-licost = b-cost * oeelk.qtyord.
          assign oeeh.totcostord = oeeh.totcostord + z-licost
                 oeel.prodcost   = oeel.prodcost + z-licost.
          assign oeelk.prodcost = b-cost.        


         end.
       else
         do: 
         find oeeh  where oeeh.cono = g-cono and
                          oeeh.orderno = poelo.orderaltno and
                          oeeh.ordersuf = poelo.orderaltsuf no-error.
        
         
         find oeel where oeel.cono = g-cono and
                         oeel.orderno = poelo.orderaltno and
                         oeel.ordersuf = poelo.orderaltsuf and
                         oeel.lineno = poelo.linealtno                                                   no-error.
         if avail oeel and (can-do("s,n",oeel.specnstype)
                      or oeel.botype = "d") then
           do:         
           z-licost = oeel.prodcost * oeel.qtyord.
           assign oeeh.totcostord = oeeh.totcostord - z-licost
                 
           z-licost = b-cost * oeel.qtyord.
           assign oeeh.totcostord = oeeh.totcostord + z-licost.

           assign oeel.prodcost = b-cost.        
           end.

 
         end.              
        end. 
      end.
    end.
  
 
  if g-vendno <> poel.vendno or
     g-shipfmno <> poel.shipfmno or
     b-buyer <> poeh.buyer or 
     b-shipinstr <> poeh.shipinstr or 
     b-shipvia <> poeh.shipvia then
    do:
    find poeh               where poeh.cono = g-cono and
                                  poeh.pono = g-pono and
                                  poeh.posuf = g-posuf                                                           no-error.


    
    find apsv where apsv.cono = g-cono  and
                    apsv.vendno = poel.vendno no-error.
    
    find z-apsv where z-apsv.cono = g-cono and
                      z-apsv.vendno = g-vendno  no-error.

    if g-shipfmno <> 0 then
      find z-apss where z-apss.cono = g-cono and
                        z-apss.vendno = g-vendno and
                        z-apss.shipfmno = g-shipfmno  no-lock no-error.
                      
    
    for each poel where poel.cono = g-cono and 
                        poel.pono = g-pono:
    
      z-licost = poel.price * poel.qtyord.
      assign apsv.ordbal = apsv.ordbal - z-licost.
    
      assign z-apsv.ordbal = z-apsv.ordbal + z-licost
             poel.vendno = z-apsv.vendno
             poel.shipfmno = g-shipfmno
             poeh.vendno   = z-apsv.vendno
             poeh.shipfmno = g-shipfmno.
     
      for each poelo where poelo.cono = poel.cono and
                           poelo.pono = poel.pono and
                           poelo.posuf = poel.posuf and
                           poelo.lineno = poel.lineno:
         if poelo.seqno <> 0 then
            do: 
            find oeelk where oeelk.cono = g-cono and
                             oeelk.ordertype = "o" and
                             oeelk.orderno = poelo.orderaltno and
                             oeelk.ordersuf = poelo.orderaltsuf and
                             oeelk.lineno = poelo.linealtno and
                             oeelk.seqno = poelo.seqno no-error.
            if avail oeelk then
               assign oeelk.arpvendno = z-apsv.vendno.
            end.
           else
            do: 
            find oeel where oeel.cono = g-cono and
                            oeel.orderno = poelo.orderaltno and
                            oeel.ordersuf = poelo.orderaltsuf and
                            oeel.lineno = poelo.linealtno                                                  no-error.
            if avail oeel then
               assign oeel.arpvendno = z-apsv.vendno.
            end.              
        end. 
      end.
      /* message "s-buyer" s-buyer "poel" poeh.pono poeh.posuf. pause. */
      find first b2-zidc use-index docinx6
                 where b2-zidc.cono       = g-cono and
                       b2-zidc.docttype   = "pq" and
                       b2-zidc.docpartner = s-buyer and
                       b2-zidc.docnumber = poeh.pono and /*
                       b2-zidc.docvendno  ge b-vendno and    */
                       b2-zidc.docuser1   = "Inbatch"      
                       no-error.

         if avail b2-zidc then do:
             assign b2-zidc.docpartner = b-buyer.
         end.   
      
    
    assign poeh.vendno = z-apsv.vendno
           poeh.shipfmno = g-shipfmno
           poeh.buyer = b-buyer
           poeh.shipinstr = b-shipinstr
           poeh.shipviaty = b-shipvia.

    
    
        
 
    
    end.
  
   

  
  hide frame f-poqued.

 put screen row 21 col 3   
   color message
"F6-Prt Tog, F7-Extend, F8-OEIO, F9-ICIAW, F10-POIO, Ctl-A Tog All,Ctl-P Print".   
/*   
" F6-Prt Toggle  F7-Extend  F8-OEIO  F9-ICIAW  F10-POIO  Ctl-A - Toggle All ". */
put screen row 5 col 1 color message
"   P.O.     Lne   Product                      Vendor Qord  CAvl" +
"       Cost Rush".
 

              
 
  input clear.
  assign o-frame-value = "". 
  leave dsply.
  end.
else
if {k-func8.i} then
  do:
  s-position = frame-line(f-poquel).
  if not avail oeeh or
     not avail poelo then
     run err.p(6169).
  else
    do: 
    assign g-modulenm = ""
          g-title = "Order Entry Inquiry - Orders".
    run oeio.p.
    g-vendno = z-vendno.
    put screen row 21 col 3   
    color message
"F6-Prt Tog, F7-Extend, F8-OEIO, F9-ICIAW, F10-POIO, Ctl-A Tog All,Ctl-P Print".   
/*
" F6-Prt Toggle  F7-Extend  F8-OEIO  F9-ICIAW  F10-POIO  Ctl-A - Toggle All ". */
     put screen row 5 col 1 color message
"   P.O.     Lne   Product                      Vendor Qord  CAvl" +
"       Cost Rush".
    if {k-jump.i} or {k-select.i} or not
      (g-modulenm = "" or g-modulenm = "pa") then 
      leave main.
    hide message no-pause.
    assign o-frame-value = "".
    next.
    end.
  end.
else
if {k-func9.i} then
  do:
  s-position = frame-line(f-poquel).
  if not avail icsp then
    do:
    run err.p (4600).
    assign o-frame-value = "".
    next.
    end.
  else  
    do:
    assign g-modulenm = ""
         g-title = "Inventory Control Inquiry - Warehouse Availability".
    run iciaw.p.
    g-vendno = z-vendno.
    put screen row 21 col 3   
     color message
"F6-Prt Tog, F7-Extend, F8-OEIO, F9-ICIAW, F10-POIO, Ctl-A Tog All,Ctl-P Print".   
/*
" F6-Prt Toggle  F7-Extend  F8-OEIO  F9-ICIAW  F10-POIO  Ctl-A - Toggle All ". */  
    put screen row 5 col 1 color message
"   P.O.     Lne   Product                      Vendor Qord  CAvl" +
"       Cost Rush".
  
    if {k-jump.i} or {k-select.i} or not
      (g-modulenm = "" or g-modulenm = "pa") then 
      leave main.
    hide message no-pause.
    assign o-frame-value = "".
    next.
    end.
  end.
else
if {k-func10.i} then
  do:
  s-position = frame-line(f-poquel).
  if not avail poeh then
     run err.p(4606).
  else
    do: 
    assign g-modulenm = ""
          g-title = "Purchase Order Inquiry - Orders".
    run poio.p.
    g-vendno = z-vendno.
    put screen row 21 col 3   
    color message
"F6-Prt Tog, F7-Extend, F8-OEIO, F9-ICIAW, F10-POIO, Ctl-A Tog All,Ctl-P Print".   
  
/*
" F6-Prt Toggle  F7-Extend  F8-OEIO  F9-ICIAW  F10-POIO  Ctl-A - Toggle All ". */
      put screen row 5 col 1 color message
"   P.O.     Lne   Product                      Vendor Qord  CAvl" +
"       Cost Rush".
    if {k-jump.i} or {k-select.i} or not
      (g-modulenm = "" or g-modulenm = "pa") then 
      leave main.
    hide message no-pause.
    assign o-frame-value = "".
    next.
    end.
  end.
else
if {k-func6.i} then
  do:
  s-position = frame-line(f-poquel).
  assign g-pono = dec(substring(o-frame-value,2,7))
         g-posuf = int(substring(o-frame-value,10,2))
         g-polineno = int(substring(o-frame-value,13,3)).
         

        
  find poeh where poeh.cono = g-cono and
                  poeh.pono = g-pono and
                  poeh.posuf = g-posuf 
                  no-error.


  
if avail poeh then
  do:
  z-refresh = true.
  assign poeh.xxc1 = if poeh.xxc1 =  "xeet" then "oeet" else "xeet".
  find first zidc  use-index docinx1
            where zidc.cono = g-cono and
                  zidc.docttype   = "pq" and
                  zidc.docnumber = poeh.pono
                                  no-error.
  if avail zidc then
    assign zidc.docdisp = if zidc.docdisp = "=" then " " else "=".
  input clear.
  assign o-frame-value = "".
  leave dsply.
  end.
end.

else
if {k-func20.i} then
  do:
  s-position = frame-line(f-poquel).
  run noted (no,"x",string(g-pono)," ").
  input clear.
  assign o-frame-value = "".
  end.   




