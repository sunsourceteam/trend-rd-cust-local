{g-all.i}
{g-inq.i}

def input param v-runtype as i                  no-undo.
def input param v-reportnm like sapbo.reportnm  no-undo.
def input param v-cono     like sapbo.cono      no-undo.
/*
def    shared var s-listfunc as l               no-undo.
*/
def           var s-listfunc as l               no-undo.

def var         s-descr    as c format "x(78)"  no-undo.
def var         v-fkey     as c format "x(5)" extent 5 initial  " "
                                                no-undo.    
def var         v-loaded   as l init false      no-undo.
def var         v-prodcat  like icsp.prodcat    no-undo.
def var         v-custno   like arsc.custno     no-undo.
def var         v-slsrep   like smsn.slsrep     no-undo.
def var         v-type     as c format "x"      no-undo.
def var         v-imode    as c format "x"      no-undo.
def var         v-generic  as c format "x(25)"  no-undo.
def var         v-temp     as c format "x(12)"  no-undo.
def var         v-temp2    as c format "x(12)"  no-undo.
def var         v-genlit   as c format "x(30)"  no-undo.
def var         v-seqno    as i                 no-undo.
def var         x-seq      as i                 no-undo.
def var         x-inx      as i                 no-undo.
def var         x-text     as c format "x(78)"  no-undo.
def var         x-filename as c format "x(100)" no-undo.
def var         x-foundnote as logical          no-undo.

/* This table stores the report specific help for custom reports 
   
   The file structure that is searched is /rd/cust/sunsource/inhouse/helper
*/    

def temp-table helper no-undo

field hseq  as integer   
field hdesc as character format "x(78)"

index hinx
      hseq.


form
 helper.hseq     format ">>>9" at 3
 
 helper.hdesc    format "x(65)" at 12
with frame f-helpy no-labels no-underline overlay 
     width 78 column 2 row 6 scroll 1 v-length down title 
     "SunSource Custom Help ".


/*
define query qhelp for helper cache 0 scrolling.
define browse bhelp query qhelp
  display
    hdesc
  with no-box no-labels overlay size-chars 78 by 16 down.
*/

/*
form
 bhelp
with frame f-helpx no-labels no-underline overlay width 80 title g-title.
*/


form
    v-type      at 1    label "Type"
    v-imode     at 5    label "InqMode"
    v-generic   at 15   label "Selection"
    v-genlit    at 43   label "Description"
with frame f-sapbxo width 80 row 6 down title "Report Selection List" overlay.



v-length = 14.

s-listfunc = true.

if v-runtype le 2 then
  for each sapbo where 
    sapbo.cono = v-cono and
    sapbo.reportnm = v-reportnm
    exclusive-lock transaction:
    delete sapbo.
  end.
     
if v-runtype = 1 then return.

do for sapbo i = 1 to 11:
  if i = 1 then
    find last sapbo use-index k-transtm where
      sapbo.reportnm = v-reportnm and
      sapbo.cono = v-cono
      no-lock no-error.
  else
      find prev sapbo use-index k-transtm where
      sapbo.reportnm = v-reportnm and
      sapbo.cono = v-cono
      no-lock no-error.
  if avail sapbo then
    do:
    down 1 with frame f-sapbxo.
    if sapbo.user1 = "t" then
      do:
      assign v-type = sapbo.user1 
             v-imode = sapbo.user2
             v-prodcat = sapbo.prodcat
             v-generic = sapbo.prodcat.
      
      if v-prodcat = "rm" then
         v-genlit = "Return Type".
      else
      if v-prodcat = "so" then
         v-genlit = "Stock type".
      else    
      if v-prodcat = "do" then
         v-genlit = "Direct type".
      else 
      if v-prodcat = "bl" then
         v-genlit = "Blanket type".
      else 
      if v-prodcat = "br" then
         v-genlit = "Blanket Release type".
      else 
      if v-prodcat = "cs" then
         v-genlit = "Counter type".
      else 
      if v-prodcat = "cr" then
         v-genlit = "Correction type".
      else 
      if v-prodcat = "po" then
         v-genlit = "Purchase order type".
      else 
      if v-prodcat = "fo" then
         v-genlit = "Future type".
      else
      if v-prodcat = "st" then
        v-genlit = "Standing Order type".
      else 
      if v-prodcat = "qu" then
         v-genlit = "Quote type".
      else 
      if v-prodcat = "ac" then
         v-genlit = "Acummulative type".
      else 
         v-genlit = "** Invalid Type ** ".
      display v-type 
              v-imode
              v-generic 
              v-genlit 
        with frame f-sapbxo.
      if i = 1 then
        v-seqno = sapbo.seqno.
      end.  
    else
    if sapbo.user1 = "b" then
      do:
      assign v-type = sapbo.user1 
             v-imode = sapbo.user2
             v-prodcat = sapbo.prodcat
             v-generic = sapbo.prodcat.
      {w-sasta.i "b" "v-prodcat" "no-lock"}
      if avail sasta then
        display v-type 
                v-imode
                v-generic 
                sasta.descrip @ v-genlit 
        with frame f-sapbxo.
      else
        display v-type
                v-imode
                v-generic 
                "** Invalid Product Buyer **" @ v-genlit 
        with frame f-sapbxo.
      if i = 1 then
        v-seqno = sapbo.seqno.
      end.  
    else
    if sapbo.user1 = "l" then
      do:
      assign v-type = sapbo.user1 
             v-imode = sapbo.user2
             v-prodcat = sapbo.prodcat
             v-generic = sapbo.prodcat.
      {w-sasta.i "E" "v-prodcat" "no-lock"}
      if avail sasta then
        display v-type 
                v-imode
                v-generic 
                sasta.descrip @ v-genlit 
        with frame f-sapbxo.
      else
        display v-type
                v-imode
                v-generic 
                "** Invalid Lost Business Reason **" @ v-genlit 
        with frame f-sapbxo.
      if i = 1 then
        v-seqno = sapbo.seqno.
      end.  
    else
    if sapbo.user1 = "k" then
      do:
      assign v-type = sapbo.user1 
             v-imode = sapbo.user2
             v-prodcat = sapbo.prodcat
             v-generic = sapbo.prodcat.
/*
      {w-sasta.i "b" "v-prodcat" "no-lock"}

      if avail sasta then
*/
        display v-type 
                v-imode
                v-generic 
                "TakenBy" @ v-genlit 
        with frame f-sapbxo.
/*
      else
        display v-type
                v-imode
                v-generic 
                "** Invalid Product Buyer **" @ v-genlit 
        with frame f-sapbxo.
*/
      if i = 1 then
        v-seqno = sapbo.seqno.
      end.  
    else
    if sapbo.user1 = "h" then
      do:
      assign v-type = sapbo.user1 
             v-imode = sapbo.user2
             v-prodcat = sapbo.prodcat
             v-generic = sapbo.prodcat.
        display v-type 
                v-imode
                v-generic 
                "ShipTo" @ v-genlit 
        with frame f-sapbxo.
      if i = 1 then
        v-seqno = sapbo.seqno.
      end.  
    else
    if sapbo.user1 = "p" then
      do:
      assign v-type = sapbo.user1 
             v-imode = sapbo.user2
             v-prodcat = sapbo.prodcat
             v-generic = sapbo.prodcat.
      {w-sasta.i "c" "v-prodcat" "no-lock"}
      if avail sasta then
        display v-type 
                v-imode
                v-generic 
                sasta.descrip @ v-genlit 
        with frame f-sapbxo.
      else
        display v-type 
                v-imode
                v-generic 
                "** Invalid Product Category **" @ v-genlit 
        with frame f-sapbxo.
      if i = 1 then
        v-seqno = sapbo.seqno.
      end.  
    else
    if sapbo.user1 = "s" then
      do:
      assign v-type = sapbo.user1 
             v-imode = sapbo.user2
             v-slsrep = sapbo.prodcat
             v-generic = sapbo.prodcat.
      {w-smsn.i "v-slsrep" "no-lock"}
      if avail smsn then
        display v-type
                v-imode
                v-generic
                smsn.name @ v-genlit 
        with frame f-sapbxo.
      else
        display v-type
                v-imode
                v-generic 
                "** Invalid Sales Rep **" @ v-genlit 
        with frame f-sapbxo.
      if i = 1 then
        v-seqno = sapbo.seqno.
      end.  
    else
    if sapbo.user1 = "j" then
      do:
      assign v-type = sapbo.user1 
             v-imode = sapbo.user2
             v-slsrep = sapbo.prodcat
             v-generic = sapbo.prodcat.
      {w-smsn.i "v-slsrep" "no-lock"}
      if avail smsn then
        display v-type
                v-imode
                v-generic
                smsn.name @ v-genlit 
        with frame f-sapbxo.
      else
        display v-type
                v-imode
                v-generic 
                "** Invalid SMSN Sales Rep **" @ v-genlit 
        with frame f-sapbxo.
      if i = 1 then
        v-seqno = sapbo.seqno.
      end.  
   else
   if sapbo.user1 = "c" then
      do:
      assign v-type = sapbo.user1 
             v-imode = sapbo.user2
             v-custno = sapbo.custno
             v-generic = string(sapbo.custno,"999999999999").
      {w-arsc.i  "v-custno" "no-lock"}
      if avail arsc then
        display v-type 
                v-imode
                v-generic 
                arsc.name @ v-genlit 
        with frame f-sapbxo.
      else
        display v-type
                v-imode
                v-generic 
                "** Invalid Customer **" @ v-genlit 
        with frame f-sapbxo.
      if i = 1 then
        v-seqno = sapbo.seqno.
      end.  
      else
   if sapbo.user1 = "v" then
      do:
      assign v-type = sapbo.user1 
             v-imode = sapbo.user2
             v-custno = sapbo.custno
             v-generic = string(sapbo.custno,"999999999999").
      {w-apsv.i  "v-custno" "no-lock"}
      if avail apsv then
        display v-type 
                v-imode
                v-generic 
                apsv.name @ v-genlit 
        with frame f-sapbxo.
      else
        display v-type
                v-imode
                v-generic 
                "** Invalid Vendor **" @ v-genlit 
        with frame f-sapbxo.
      if i = 1 then
        v-seqno = sapbo.seqno.
      end.  
    else
    if sapbo.user1 = "w" then
      do:
      assign v-type = sapbo.user1 
             v-imode = sapbo.user2
             v-prodcat = sapbo.prodcat
             v-generic = sapbo.prodcat.
      {w-icsd.i  "v-prodcat" "no-lock"}
      if avail icsd then
        display v-type 
                v-imode
                v-generic 
                icsd.name @ v-genlit 
        with frame f-sapbxo.
      else
        display v-type
                v-imode
                v-generic 
                "** Invalid Warehouse **" @ v-genlit 
        with frame f-sapbxo.
      if i = 1 then
        v-seqno = sapbo.seqno.
      end. 
   else
   if sapbo.user1 = "n" then
      do:
      assign v-type = sapbo.user1 
             v-imode = sapbo.user2
             v-prodcat = sapbo.prodcat
             v-generic = sapbo.prodcat.
      find first notes where notes.cono = g-cono and 
                             notes.notestype = "zz" and
                             notes.primarykey  = "apsva" and
                             notes.secondarykey = sapbo.user3
                        no-lock
                        no-error. 
      if avail notes then
        display v-type 
                v-imode
                v-generic 
                notes.noteln[1] @ v-genlit 
        with frame f-sapbxo.
      else
        display v-type
                v-imode
                v-generic 
                "** Invalid Vendor ID **" @ v-genlit 
        with frame f-sapbxo.
      if i = 1 then
        v-seqno = sapbo.seqno.
      end. 
   else
   if sapbo.user1 = "e" then
      do:
      assign v-type = sapbo.user1 
             v-imode = sapbo.user2
             v-prodcat = sapbo.prodcat
             v-generic = sapbo.prodcat.
      find first notes where notes.cono = g-cono and 
                             notes.notestype = "zz" and
                             notes.primarykey  = "apsva" and
                             notes.secondarykey = sapbo.user3
                        no-lock
                        no-error. 
      if avail notes then
        display v-type 
                v-imode
                v-generic 
                notes.noteln[3] @ v-genlit 
        with frame f-sapbxo.
      else
        display v-type
                v-imode
                v-generic 
                "** Invalid Vendor Parent ID **" @ v-genlit 
        with frame f-sapbxo.
      if i = 1 then
        v-seqno = sapbo.seqno.
      end. 
   else
   if sapbo.user1 = "G" then
      do:
      assign v-type = sapbo.user1 
             v-imode = sapbo.user2
             v-prodcat = sapbo.prodcat
             v-generic = sapbo.prodcat.
      find first notes where notes.cono = g-cono and 
                             notes.notestype = "zz" and
                             notes.primarykey  = "apsva" and
                             notes.secondarykey = sapbo.user3
                        no-lock
                        no-error. 
      if avail notes then
        display v-type 
                v-imode
                v-generic 
                notes.noteln[2] @ v-genlit 
        with frame f-sapbxo.
      else
        display v-type
                v-imode
                v-generic 
                "** Invalid Technology Parent ID **" @ v-genlit 
        with frame f-sapbxo.
      if i = 1 then
        v-seqno = sapbo.seqno.
      end. 
   else
   if sapbo.user1 = "U" then
      do:
      assign v-type = sapbo.user1 
             v-imode = sapbo.user2
             v-prodcat = sapbo.prodcat
             v-generic = sapbo.prodcat.
      find first icsp where icsp.cono = g-cono and 
                            icsp.prod = sapbo.user3
                        no-lock
                        no-error. 
      if avail icsp then
        display v-type 
                v-imode
                v-generic 
                icsp.descrip[1] @ v-genlit 
        with frame f-sapbxo.
      else
        display v-type
                v-imode
                v-generic 
                "** Non-Stock Item **" @ v-genlit 
        with frame f-sapbxo.
      if i = 1 then
        v-seqno = sapbo.seqno.
     
    end.
   end.
 else
   leave.
end.   
 if i ne 1 then up i - 1 with frame f-sapbxo.
 
   
 mainproc:

 do for sapbo while true transaction 
                         on endkey undo mainproc, leave mainproc:
   hide frame f-x no-pause.
   clear frame f-sapbxo no-pause.
 
   display " " @ v-type
           " " @ v-imode
           "                         " @ v-generic
           "                              "  @ v-genlit
           with frame f-sapbxo.
   put screen row 21  column 3 color messages
"F6-Clr List                                            F10 - Help".
   
   prompt-for v-type v-imode v-generic 
          go-on (F6) with frame f-sapbxo.                      
   
   hide message no-pause.

/*

   if {k-func10.i} then
     do:
     if not v-loaded then
       do:
       v-loaded = true.
       x-filename = g-ourproc.
       find first notes where notes.cono = g-cono and 
                        notes.notestype = "zz" and
                        notes.primarykey = "sdihelp" and
                        notes.secondarykey = x-filename no-lock no-error.
       if not avail notes then
         x-filename = "generic".
       x-seq = 0.
       for each   notes where notes.cono = g-cono and 
                        notes.notestype = "zz" and
                        notes.primarykey = "sdihelp" and
                        notes.secondarykey = x-filename no-lock.
     
         do x-inx = 1 to 16:
           create helper.
           x-seq = x-seq + 1.
           assign helper.hdesc = notes.noteln[x-inx]
                  helper.hseq  = x-seq.
         end.
       end.
       create helper.
       x-seq = x-seq + 1.
       assign helper.hdesc = "--End Of Help-- "
              helper.hseq  = x-seq.
       end.
     /*
     
     on cursor-up cursor-up.
     on cursor-down cursor-down.
       
     open query qhelp for each helper use-index hinx no-prefetch no-lock.
     enable bhelp with frame f-helpx.
     wait-for window-close of current-window.
        
     hide frame f-helpx no-pause.
    
     on cursor-up back-tab.
     on cursor-down tab.
     next mainproc.
     */
     
     main:
     
     repeat on endkey undo main, leave main
            on error  undo main,  leave main:
     if {k-cancel.i} then
       do:
       input clear.
       leave main.
       end.
       
       {xxilx.i
         &find = "n-zsdihelp.i"
         &frame = "f-helpy"
         &field = "helper.hseq"
         &display = "d-zsdihelp.i"
         &file = "helper"
                  
         }
     message "test " . pause.
     leave main.
     end.

    next mainproc.
          
                           
     
   end.
   
  */ 
   
   if {k-func6.i} then  
     do:
     put screen row 21  column 3 color messages
"F6-Clr List                                            F10 - Help".
     confirm = true.
     do on endkey undo, leave:
       update 
         confirm label "Clear List ?"
         with frame f-x2 side-labels row 8 column 20 overlay.
     end.
     if confirm = yes and not {k-cancel.i} then
       do:
       for each sapbo where
         sapbo.cono = v-cono and
         sapbo.reportnm = v-reportnm
         exclusive-lock:
         delete sapbo.
       end.
       clear frame f-sapbxo all no-pause.
       run err.p(5048).
       end.
     else
       run err.p(5049).
  hide frame f-x2 no-pause.
  put screen row 21  column 3 color messages
"F6-Clr List                                            F10 - Help".
  next mainproc.
  end.
if input v-type = "" and input v-imode = "" and input v-generic = "" then
  do:
  leave mainproc.
  end.
find sapbo where
  sapbo.cono = v-cono and
  sapbo.reportnm = v-reportnm and
  sapbo.custno = (if input v-type = "c" or input v-type = "v" then
                   dec(input v-generic)
                 else
                   0)          and
  sapbo.prodcat = (if input v-type <> "c" or input v-type = "v" then
                   input v-generic
                  else
                   " ")        and
  sapbo.user1   = input v-type         
 exclusive-lock no-error.  
                 
if avail sapbo then
  do on endkey undo mainproc, next mainproc:    
  confirm = true.
  update 
    confirm label "Remove From List ? "
    with frame f-x side-labels row 8 column 20 overlay.
  if confirm then
    do:
    message v-type + " " + v-imode + " " + v-generic + " Removed from List".
    delete sapbo.
    end.
  hide frame f-x.
  next mainproc.
  end.
    

 if input v-type <> "s" and
    input v-type <> "j" and
    input v-type <> "p" and
    input v-type <> "t" and
    input v-type <> "c" and
    input v-type <> "b" and
    input v-type <> "l" and
    input v-type <> "k" and
    input v-type <> "h" and
    input v-type <> "v" and
    input v-type <> "w" and
    input v-type <> "n" and
    input v-type <> "e" and
    input v-type <> "G" and
    input v-type <> "U"       then
   do:
   run err.p (5437).
   next mainproc.
   end.

if input v-imode <> "i" and
   input v-imode <> "x" and
   input v-imode <> " " then
  do:
  message "Invalid InqMode Entered".
  next mainproc.
  end.
  
 
 
 
 if input v-type = "t" then
   do:
      
      if input v-generic = "rm" then
         v-genlit = "Return Type".
      else
      if input v-generic = "so" then
         v-genlit = "Stock type".
      else    
      if input v-generic = "do" then
         v-genlit = "Direct type".
      else 
      if input v-generic = "bl" then
         v-genlit = "Blanket type".
      else 
      if input v-generic = "br" then
         v-genlit = "Blanket Release type".
      else 
      if input v-generic = "cs" then
         v-genlit = "Counter type".
      else 
      if input v-generic = "cr" then
         v-genlit = "Correction type".
      else 
      if input v-generic = "po" then
         v-genlit = "Purchase order type".
      else 
      if input v-generic = "fo" then
         v-genlit = "Future type".
      else
      if input v-generic = "st" then
        v-genlit = "Standing Order type".
      else 
      if input v-generic = "qu" then
         v-genlit = "Quote type".
      else 
      if input v-generic = "ac" then
         v-genlit = "Acummulative type".
      else 
         v-genlit = "** Invalid Type ** ".

   
   if v-genlit <> "** Invalid Type ** " then
     do:
     display v-genlit with frame f-sapbxo.
     find last sapbo use-index k-sapbo where sapbo.cono = v-cono 
                               and sapbo.reportnm = v-reportnm
                               no-lock no-error.
     if not avail sapbo then
       v-seqno = 0.
     else
       v-seqno = sapbo.seqno.                

     
     
     create sapbo.
     assign
       sapbo.cono     = v-cono
       sapbo.reportnm =  v-reportnm
       v-seqno        = v-seqno + 1
       sapbo.custno   = 0
       sapbo.prodcat  = input v-generic
       sapbo.orderno  = v-seqno
       sapbo.seqno    = v-seqno
       sapbo.user2    = input v-imode
       sapbo.user1    = input v-type.
     {t-all.i sapbo}
     scroll down with frame f-sapbxo.

     end.  
   else
     do:
     run err.p(4020).
     next mainproc.
     end.
   end.  
 else

 /* 'b' is for BUYER selection               */
 
 if input v-type = "b" then
   do:
   {w-sasta.i "b" "input v-generic" "no-lock"}
   if avail sasta then
     do:
     display sasta.descrip @ v-genlit with frame f-sapbxo.
     find last sapbo use-index k-sapbo where sapbo.cono = v-cono 
                               and sapbo.reportnm = v-reportnm
                               no-lock no-error.
     if not avail sapbo then
       v-seqno = 0.
     else
       v-seqno = sapbo.seqno.                

     

     create sapbo.
     assign
       sapbo.cono     = v-cono
       sapbo.reportnm =  v-reportnm
       v-seqno        = v-seqno + 1
       sapbo.custno   = 0
       sapbo.prodcat  = input v-generic
       sapbo.orderno  = v-seqno
       sapbo.seqno    = v-seqno
       sapbo.user2    = input v-imode
       sapbo.user1    = input v-type.
     {t-all.i sapbo}
     scroll down with frame f-sapbxo.

     end.  
   else
     do:
     run err.p(4020).
     next mainproc.
     end.
   end.  
 else

 /* 'L' is for LOST BUSINESS TYPE selection               */
 
 if input v-type = "L" then
   do:
   {w-sasta.i "E" "input v-generic" "no-lock"}
   if avail sasta then
     do:
     display sasta.descrip @ v-genlit with frame f-sapbxo.
     find last sapbo use-index k-sapbo where sapbo.cono = v-cono 
                               and sapbo.reportnm = v-reportnm
                               no-lock no-error.
     if not avail sapbo then
       v-seqno = 0.
     else
       v-seqno = sapbo.seqno.                

     

     create sapbo.
     assign
       sapbo.cono     = v-cono
       sapbo.reportnm =  v-reportnm
       v-seqno        = v-seqno + 1
       sapbo.custno   = 0
       sapbo.prodcat  = input v-generic
       sapbo.orderno  = v-seqno
       sapbo.seqno    = v-seqno
       sapbo.user2    = input v-imode
       sapbo.user1    = input v-type.
     {t-all.i sapbo}
     scroll down with frame f-sapbxo.

     end.  
   else
     do:
     run err.p(4009).
     next mainproc.
     end.
   end.  
 else


 /* 'K' is for Taken By selection               */
 
 if input v-type = "k" then
   do:
   display "Taken By" @ v-genlit with frame f-sapbxo.
   find last sapbo use-index k-sapbo where sapbo.cono = v-cono 
                             and sapbo.reportnm = v-reportnm
                             no-lock no-error.
   if not avail sapbo then
     v-seqno = 0.
   else
     v-seqno = sapbo.seqno.                

    create sapbo.
    assign
      sapbo.cono     = v-cono
      sapbo.reportnm =  v-reportnm
      v-seqno        = v-seqno + 1
      sapbo.custno   = 0
      sapbo.prodcat  = input v-generic
      sapbo.orderno  = v-seqno
      sapbo.seqno    = v-seqno
      sapbo.user2    = input v-imode
      sapbo.user1    = input v-type.
    {t-all.i sapbo}
    scroll down with frame f-sapbxo.
   end.  
 else

 /* 'H' is for ShipTo By selection               */
 
 if input v-type = "H" then
   do:
   display "Ship To" @ v-genlit with frame f-sapbxo.
   find last sapbo use-index k-sapbo where sapbo.cono = v-cono 
                             and sapbo.reportnm = v-reportnm
                             no-lock no-error.
   if not avail sapbo then
     v-seqno = 0.
   else
     v-seqno = sapbo.seqno.                

    create sapbo.
    assign
      sapbo.cono     = v-cono
      sapbo.reportnm =  v-reportnm
      v-seqno        = v-seqno + 1
      sapbo.custno   = 0
      sapbo.prodcat  = input v-generic
      sapbo.orderno  = v-seqno
      sapbo.seqno    = v-seqno
      sapbo.user2    = input v-imode
      sapbo.user1    = input v-type.
    {t-all.i sapbo}
    scroll down with frame f-sapbxo.
   end.  
 else
 /* 'n' is for Vendor Name selection               */
 
 if input v-type = "n" then
   do:
   assign x-foundnote = false. 
   for each notes where notes.cono = g-cono and 
                        notes.notestype = "zz" and
                        notes.primarykey  = "apsva" and
                        notes.noteln[1] = input v-generic                                         no-lock:
     x-foundnote = true.      
     leave.
   end.              
                 
   if x-foundnote then
     do:
     display notes.noteln[1] @ v-genlit with frame f-sapbxo.
     find last sapbo use-index k-sapbo where sapbo.cono = v-cono 
                               and sapbo.reportnm = v-reportnm
                               no-lock no-error.
     if not avail sapbo then
       v-seqno = 0.
     else
       v-seqno = sapbo.seqno.                

     

     create sapbo.
     assign
       sapbo.cono     = v-cono
       sapbo.reportnm =  v-reportnm
       v-seqno        = v-seqno + 1
       sapbo.custno   = 0
       sapbo.user3    = notes.secondarykey
       sapbo.prodcat  = notes.noteln[1]
       sapbo.orderno  = v-seqno
       sapbo.seqno    = v-seqno
       sapbo.user2    = input v-imode
       sapbo.user1    = input v-type.
     {t-all.i sapbo}
     scroll down with frame f-sapbxo.

     end.  
   else
     do:
     run err.p(4020).
     next mainproc.
     end.
   end.  
 else

 /* 'i' is for Vendor parent ID selection               */
 
 if input v-type = "e" then
   do:
   assign x-foundnote = false. 
   for each notes where notes.cono = g-cono and 
                        notes.notestype = "zz" and
                        notes.primarykey  = "apsva" and
                        notes.noteln[3] = input v-generic                      
                        no-lock:
     x-foundnote = true.      
     leave.
   end.              
                 
   if x-foundnote then
     do:
     display notes.noteln[3] @ v-genlit with frame f-sapbxo.
     find last sapbo use-index k-sapbo where sapbo.cono = v-cono 
                               and sapbo.reportnm = v-reportnm
                               no-lock no-error.
     if not avail sapbo then
       v-seqno = 0.
     else
       v-seqno = sapbo.seqno.                

     

     create sapbo.
     assign
       sapbo.cono     = v-cono
       sapbo.reportnm =  v-reportnm
       v-seqno        = v-seqno + 1
       sapbo.custno   = 0
       sapbo.user3    = notes.secondarykey
       sapbo.prodcat  = notes.noteln[3]
       sapbo.orderno  = v-seqno
       sapbo.seqno    = v-seqno
       sapbo.user2    = input v-imode
       sapbo.user1    = input v-type.
     {t-all.i sapbo}
     scroll down with frame f-sapbxo.

     end.  
   else
     do:
     run err.p(4020).
     next mainproc.
     end.
   end.  
 else

 /* 'g' is for Technology selection               */
 
 if input v-type = "G" then
   do:
   assign x-foundnote = false. 
   for each notes where notes.cono = g-cono and 
                        notes.notestype = "zz" and
                        notes.primarykey  = "apsva" and
                        notes.noteln[2] begins input v-generic                      
                        no-lock:
     x-foundnote = true.      
     leave.
   end.              
                 
   if x-foundnote then
     do:
     display notes.noteln[2] @ v-genlit with frame f-sapbxo.
     find last sapbo use-index k-sapbo where sapbo.cono = v-cono 
                               and sapbo.reportnm = v-reportnm
                               no-lock no-error.
     if not avail sapbo then
       v-seqno = 0.
     else
       v-seqno = sapbo.seqno.                

     

     create sapbo.
     assign
       sapbo.cono     = v-cono
       sapbo.reportnm =  v-reportnm
       v-seqno        = v-seqno + 1
       sapbo.custno   = 0
       sapbo.user3    = notes.secondarykey
       sapbo.prodcat  = substring(notes.noteln[2],1,4)
       sapbo.orderno  = v-seqno
       sapbo.seqno    = v-seqno
       sapbo.user2    = input v-imode
       sapbo.user1    = input v-type.
     {t-all.i sapbo}
     scroll down with frame f-sapbxo.

     end.  
   else
     do:
     run err.p(4020).
     next mainproc.
     end.
   end.  
 else

 /* 'U' is for Product selection               */
 
 if input v-type = "U" then
   do:
   assign x-foundnote = false. 
   find first  ICSP  where icsp.cono = g-cono and 
                        icsp.prod = input v-generic        
                        no-lock no-error.
   if avail icsp or not avail icsp  then
     do:
     display   if avail icsp then
                  icsp.descrip[1]
               else
                  "** Non-Stock Product **" @ v-genlit 
                  with frame f-sapbxo.
     find last sapbo use-index k-sapbo where sapbo.cono = v-cono 
                               and sapbo.reportnm = v-reportnm
                               no-lock no-error.
     if not avail sapbo then
       v-seqno = 0.
     else
       v-seqno = sapbo.seqno.                

     

     create sapbo.
     assign
       sapbo.cono     = v-cono
       sapbo.reportnm =  v-reportnm
       v-seqno        = v-seqno + 1
       sapbo.custno   = 0
       sapbo.prodcat  = input v-generic
       sapbo.orderno  = v-seqno
       sapbo.seqno    = v-seqno
       sapbo.user2    = input v-imode
       sapbo.user1    = input v-type.
     {t-all.i sapbo}
     scroll down with frame f-sapbxo.

     end.  
   else
     do:
     run err.p(4020).
     next mainproc.
     end.
  end.   
 
 else

/* 'p' is for PRODCAT              */
 if input v-type = "p" then
   do:
   {w-sasta.i "c" "input v-generic" "no-lock"}
   if avail sasta then
     do:
     display sasta.descrip @ v-genlit with frame f-sapbxo.
     find last sapbo use-index k-sapbo where sapbo.cono = v-cono 
                               and sapbo.reportnm = v-reportnm
                               no-lock no-error.
     if not avail sapbo then
       v-seqno = 0.
     else
       v-seqno = sapbo.seqno.                

     
     create sapbo.
     assign
       sapbo.cono     = v-cono
       sapbo.reportnm =  v-reportnm
       v-seqno        = v-seqno + 1
       sapbo.custno   = 0
       sapbo.prodcat  = input v-generic
       sapbo.orderno  = v-seqno
       sapbo.seqno    = v-seqno
       sapbo.user2    = input v-imode
       sapbo.user1    = input v-type.
     {t-all.i sapbo}
     scroll down with frame f-sapbxo.

     end.  
   else
     do:
     run err.p(4020).
     next mainproc.
     end.
   end.  
 else
 /*   's' is for SALES REP    */
 
 if (input v-type = "s" or
     input v-type = "j") then
   do:
   {w-smsn.i "input v-generic" "no-lock"}
   if avail smsn then
     do:
     display smsn.name @ v-genlit with frame f-sapbxo.
     find last sapbo use-index k-sapbo where sapbo.cono = v-cono 
                               and sapbo.reportnm = v-reportnm
                               no-lock no-error.
     if not avail sapbo then
       v-seqno = 0.
     else
       v-seqno = sapbo.seqno.                

     
     create sapbo.
     assign
       sapbo.cono     = v-cono
       sapbo.reportnm =  v-reportnm
       v-seqno        = v-seqno + 1
       sapbo.custno   = 0
       sapbo.prodcat  = input v-generic
       sapbo.orderno  = v-seqno
       sapbo.seqno    = v-seqno
       sapbo.user2    = input v-imode
       sapbo.user1    = input v-type.
     {t-all.i sapbo}
     scroll down with frame f-sapbxo.

     end.  
   else
     do:
     run err.p(4604).
     next mainproc.
     end.
   end.  

 else

 /* 'w' is for WAREHOUSE selection               */
 
 if input v-type = "w" then
   do:
   {w-icsd.i "input v-generic" "no-lock"}
   if avail icsd then
     do:
     display icsd.name @ v-genlit with frame f-sapbxo.
     find last sapbo use-index k-sapbo where sapbo.cono = v-cono 
                               and sapbo.reportnm = v-reportnm
                               no-lock no-error.
     if not avail sapbo then
       v-seqno = 0.
     else
       v-seqno = sapbo.seqno.                

     

     create sapbo.
     assign
       sapbo.cono     = v-cono
       sapbo.reportnm =  v-reportnm
       v-seqno        = v-seqno + 1
       sapbo.custno   = 0
       sapbo.prodcat  = input v-generic
       sapbo.orderno  = v-seqno
       sapbo.seqno    = v-seqno
       sapbo.user2    = input v-imode
       sapbo.user1    = input v-type.
     {t-all.i sapbo}
     scroll down with frame f-sapbxo.

     end.  
   else
     do:
     run err.p(4020).
     next mainproc.
     end.
   end.  

else

/*   'c' is for CUSTOMER NUMBER      */

if input v-type = "c" then

  do:
 
  v-temp = input v-generic.
  v-temp2 = replace(v-temp,"0"," ").
  v-temp2 = replace(v-temp2,"1"," ").
  v-temp2 = replace(v-temp2,"2"," ").
  v-temp2 = replace(v-temp2,"3"," ").
  v-temp2 = replace(v-temp2,"4"," ").
  v-temp2 = replace(v-temp2,"5"," ").
  v-temp2 = replace(v-temp2,"6"," ").
  v-temp2 = replace(v-temp2,"7"," ").
  v-temp2 = replace(v-temp2,"8"," ").
  v-temp2 = replace(v-temp2,"9"," ").
  if v-temp2 <> "" then
    do:
    run err.p(3123).
    next mainproc.
    end.                       
   
  
  {w-arsc.i  "dec(input v-generic)" "no-lock"}
  if avail arsc then
    do:
    display arsc.name @ v-genlit with frame f-sapbxo.
    find last sapbo use-index k-sapbo where sapbo.cono = v-cono 
                               and sapbo.reportnm = v-reportnm
                               no-lock no-error.
     if not avail sapbo then
       v-seqno = 0.
     else
       v-seqno = sapbo.seqno.                

     
    create sapbo.
    assign
       sapbo.cono     = v-cono
       sapbo.reportnm =  v-reportnm
       v-seqno        = v-seqno + 1
       sapbo.custno   = dec (input v-generic)
       sapbo.prodcat  = " "
       sapbo.orderno  = v-seqno
       sapbo.seqno    = v-seqno
       sapbo.user2    = input v-imode
       sapbo.user1    = input v-type.
     {t-all.i sapbo}
     scroll down with frame f-sapbxo.

     end.  
   else
     do:
     run err.p(4303).
     next mainproc.
     end.
  end.  
else

/*   'v' is for VENDOR NUMBER      */

if input v-type = "v" then

  do:
 
  v-temp = input v-generic.
  v-temp2 = replace(v-temp,"0"," ").
  v-temp2 = replace(v-temp2,"1"," ").
  v-temp2 = replace(v-temp2,"2"," ").
  v-temp2 = replace(v-temp2,"3"," ").
  v-temp2 = replace(v-temp2,"4"," ").
  v-temp2 = replace(v-temp2,"5"," ").
  v-temp2 = replace(v-temp2,"6"," ").
  v-temp2 = replace(v-temp2,"7"," ").
  v-temp2 = replace(v-temp2,"8"," ").
  v-temp2 = replace(v-temp2,"9"," ").
  if v-temp2 <> "" then
    do:
    run err.p(3123).
    next mainproc.
    end.                       
   
  
  {w-apsv.i  "dec(input v-generic)" "no-lock"}
  if avail apsv then
    do:
    display apsv.name @ v-genlit with frame f-sapbxo.
    find last sapbo use-index k-sapbo where sapbo.cono = v-cono 
                               and sapbo.reportnm = v-reportnm
                               no-lock no-error.
     if not avail sapbo then
       v-seqno = 0.
     else
       v-seqno = sapbo.seqno.                

     
    create sapbo.
    assign
       sapbo.cono     = v-cono
       sapbo.reportnm =  v-reportnm
       v-seqno        = v-seqno + 1
       sapbo.custno   = dec (input v-generic)
       sapbo.prodcat  = " "
       sapbo.orderno  = v-seqno
       sapbo.seqno    = v-seqno
       sapbo.user2    = input v-imode
       sapbo.user1    = input v-type.
     {t-all.i sapbo}
     scroll down with frame f-sapbxo.

     end.  
   else
     do:
     run err.p(4303).
     next mainproc.
     end.
  end.
end.
if ({k-cancel.i} or {k-jump.i}) and v-reportnm = "" then
  for each sapbo where
    sapbo.cono = v-cono and
    sapbo.reportnm = v-reportnm
    exclusive-lock transaction:
    delete sapbo.
  end.
hide frame f-sapbxo no-pause.    
                   
                   
