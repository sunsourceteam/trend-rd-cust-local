Procedure Check_for_Dups:
  define input parameter        ip-tblname     as char     no-undo.
  define input parameter        ip-recno       as int      no-undo.
  define input-output parameter ip-h-errorcd   as char     no-undo.
  define input-output parameter ip-errorlit    as char     no-undo.

  find b-t-data where 
       b-t-data.recno = ip-recno and
       b-t-data.name  = "vendno" no-lock no-error.
  find apsv where 
       apsv.cono   = g-cono and
       apsv.vendno = dec(b-t-data.fieldval) no-lock no-error.
  if avail apsv then do:      
      assign ip-h-errorcd  = "U"
             ip-errorlit   = "Duplucate customer record".
  end.

end.

Procedure Acquire_Recid:
  define input parameter        ip-tblname     as char     no-undo.
  define input parameter        ip-recno       as int      no-undo.
  define input-output parameter ip-rowid       as rowid    no-undo.

  find b-t-data where 
       b-t-data.recno = ip-recno and
       b-t-data.name  = "vendno" no-lock no-error.
  find apsv where 
       apsv.cono   = g-cono and
       apsv.vendno = dec(b-t-data.fieldval) no-lock no-error.
  if avail apsv then do:      
      assign ip-rowid = rowid(apsv).
  end.

end.





Procedure Other_supporting_Edits:
  define input parameter        ip-tblname     as char     no-undo.
  define input parameter        ip-field-name  as char     no-undo.
  
  define input-output parameter ip-field-value      as char     no-undo.
  define input-output parameter ip-field-value-orig as char     no-undo.
  
  define input-output parameter ip-d-errorcd   as char     no-undo.
  define input-output parameter ip-h-errorcd   as char     no-undo.
  define input-output parameter ip-errorlit    as char     no-undo.
 

  
  if ip-field-name = "countrycd" and ip-field-value <> "" then do:
    find sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "W" and
         sasta.codeval  = ip-field-value no-lock no-error.
    if not avail sasta then do:
      create sasta.
      assign 
        sasta.cono = g-cono 
        sasta.codeiden = "W" 
        sasta.codeval  = ip-field-value 
        sasta.descrip  = "t".
    end.
    else
    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "CountryCd is not set up in SASTT".
    end.
  end.


  if ip-field-name = "termstype" then do:
    if ip-field-value = "1%10net10" or
       ip-field-value = "1%10net20" then
      assign ip-field-value = "1%10net30". 
    
    find first sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "T" and
         sasta.user5    = ip-field-value no-lock no-error.

    if avail sasta and sasta.codeval <> ip-field-value then
      assign ip-field-value-orig = ip-field-value
             ip-field-value      = sasta.codeval.

    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "TermsType is not set up in SASTT".
    end.
  end.



  if ip-field-name = "consolterms" and 
     ip-field-value <> "" then do:
    find sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "T" and
         sasta.codeval  = ip-field-value no-lock no-error.
    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Consolterms is not set up in SASTT".
    end.
  end.


  if ip-field-name = "langcd" and ip-field-value <> "" then do:
    find sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "Y" and
         sasta.codeval  = ip-field-value no-lock no-error.
    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Languag code is not set up in SASTT".
    end.
  end.


  if ip-field-name = "custtype" and ip-field-value <> "" then do:
    find sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "CU" and
         sasta.codeval  = ip-field-value no-lock no-error.
    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "CustType is not set up in SASTT".
    end.
  end.

  if ip-field-name = "shipviaty" and ip-field-value <> "" then do:
    find first sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "s" and
         sasta.user5  = ip-field-value no-lock no-error.

 
    if avail sasta and sasta.codeval <> ip-field-value then
      assign ip-field-value-orig = ip-field-value
             ip-field-value      = sasta.codeval.

    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Ship Via type is not set up in SASTT".
    end.
  end.


  if ip-field-name = "salesterr" and ip-field-value <> "" then do:
    find sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "Z" and
         sasta.codeval  = ip-field-value no-lock no-error.
    if not avail sasta then do:
      create sasta.
      assign 
        sasta.cono = g-cono 
        sasta.codeiden = "Z" 
        sasta.codeval  = ip-field-value 
        sasta.descrip  = "t".
    end.
    else
    
    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "SalesTerr is not set up in SASTT".
      message "salesterr". pause.       
    end.
  end.


  if ip-field-name = "pricetype" and ip-field-value <> "" then do:
    find sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "J" and
         sasta.codeval  = ip-field-value no-lock no-error.
    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "PriceType is not set up in SASTT".
    end.
  end.

  if ip-field-name = "vendortype" and ip-field-value <> "" then do:
    find sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "vt" and
         sasta.codeval  = ip-field-value no-lock no-error.
    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Vendor Type is not set up in SASTT".
    end.
  end.



  if ip-field-name = "nontaxtype" and ip-field-value <> "" then do:
    find sasta where
         sasta.cono = g-cono and
         sasta.codeiden = "N" and
         sasta.codeval  = ip-field-value no-lock no-error.
    if not avail sasta then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "NonTaxType is not set up in SASTT".
    end.
  end.



  if ip-field-name = "creditmgr" and ip-field-value <> "" then do:
    find sasoo where
         sasoo.cono = g-cono and
         sasoo.oper2  = ip-field-value no-lock no-error.
    if not avail sasoo then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Credit Manager is not set up in SASOO".
    end.
  end.


  if ip-field-name = "divno" and ip-field-value <> "" then do:
    find sastn where
         sastn.cono = g-cono and
         sastn.codeiden = "V" and
         sastn.codeval  = int(ip-field-value) no-lock no-error.
    if not avail sastn then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Divno is not set up in SASTT".
    end.
  end.

  if ip-field-name = "mediacd" and int(ip-field-value) <> 0 then do:
    find sastn where
         sastn.cono = g-cono and
         sastn.codeiden = "P" and
         sastn.codeval  = int(ip-field-value) no-lock no-error.
    if not avail sastn then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "MediaCd is not set up in SASTT".
    end.
  end.


  if ip-field-name = "addonnum" and int(ip-field-value) <> 0 then do:
    find sastn use-index k-sastn where
         sastn.cono     = g-cono and
         sastn.codeiden = "a":u  and
         sastn.codeval  = int(ip-field-value) and
         sastn.catppt   = no
        no-lock no-error.
    if not avail sastn then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Addon type is not set up in SASTO".
    end.
  end.







  if ip-field-name = "currencyty" and ip-field-value <> "" then do:
    find sastc where
         sastc.cono        = g-cono and
         sastc.currencyty  = ip-field-value no-lock no-error.
    if not avail sastc then do:

      if ip-field-value = "CD" or
         ip-field-value = "CA" then
 
        assign ip-field-value-orig = ip-field-value
               ip-field-value      = "CN".
      else
 
      if ip-field-value = "US" then
        assign ip-field-value-orig = ip-field-value
               ip-field-value      = "  ".
      else
        assign ip-d-errorcd  = "E"
               ip-h-errorcd  = "E"
               ip-errorlit   = "Currencyty is not set up in SASTC".
    end.
  end.

  if ip-field-name = "bankno" and ip-field-value <> "" then do:
    find crsb where
         crsb.cono    = g-cono and
         crsb.bankno = int(ip-field-value) no-lock no-error.
    if not avail crsb then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Bank not set up in CRSB".
    end.
  end.


  if ip-field-name = "slsrepout" and ip-field-value <> "" then do:
    find smsn where
         smsn.cono = g-cono and
         smsn.slsrep  = ip-field-value no-lock no-error.
    if not avail smsn then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "SlsRepOut is not set up in SMSN".
    end.
  end.


  if ip-field-name = "slsrepin" and ip-field-value <> "" then do:
    find smsn where
         smsn.cono = g-cono and
         smsn.slsrep  = ip-field-value no-lock no-error.
    if not avail smsn then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "SlsRepIn is not set up in SMSN".
    end.
  end.



  if ip-field-name = "whse" and ip-field-value <> "" then do:
    find icsd where
         icsd.cono = g-cono and
         icsd.whse  = ip-field-value no-lock no-error.
    if not avail icsd then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Whse is not set up in ICSD".
    end.
  end.



  if ip-field-name = "rebatety" and ip-field-value <> "" then do:
    find pdst where
         pdst.cono = g-cono and
         pdst.codeiden = "CT" and
         pdst.codeval  = ip-field-value no-lock no-error.
    if not avail pdst then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Rebatety is not set up in PDST".
    end.
  end.

/*
  if ip-field-name = "statecd" and ip-field-value <> "" then do:
    find sasgm where
         sasgm.cono = g-cono and
         sasgm.recty = 2 and
    if not avail sasgm then do:
      assign ip-d-errorcd  = "E"
             ip-h-errorcd  = "E"
             ip-errorlit   = "Rebatety is not set up in PDST".
      message "rebatety". pause.       
    end.
  end.
*/


end.


Procedure Ancillary_Tables:
  Define input parameter ip-recid            as recid no-undo.

  define buffer slsmx  for notes.
  define buffer x-slsmx  for notes.
  define buffer slslb  for notes.
  define buffer bip-apsv for apsv.

  find bip-apsv where recid(bip-apsv) = ip-recid no-error.

  if avail bip-apsv then do:

    if bip-apsv.bankno = 93 then
      assign bip-apsv.currencyty = "CN"
             bip-apsv.divno = 040.
    else
    if bip-apsv.bankno = 41 then
      assign bip-apsv.currencyty = "CN"
             bip-apsv.divno = 040.
    else
    if bip-apsv.bankno = 51 then
      assign bip-apsv.currencyty = " "
             bip-apsv.divno = 040.
    else do:
      find crsb where
           crsb.cono    = g-cono and
           crsb.bankno =  bip-apsv.bankno no-lock no-error.
      if avail crsb then
        assign bip-apsv.currencyty = crsb.currencyty
               bip-apsv.divno = crsb.divno.
    end.
    /* Save off the JT vendor ID for reference */
    assign bip-apsv.comment     = bip-apsv.lookupnm.  
    
    assign bip-apsv.edipartner  = string(bip-apsv.vendno)
           bip-apsv.lookupnm    = substring(bip-apsv.name,1,15).

     find slsmx  where slsmx.cono         = bip-apsv.cono and 
                      slsmx.notestype    = "zz"      and
                      slsmx.primarykey   = "logo print"   and
                      slsmx.secondarykey =
                       string(bip-apsv.vendno,"99999999999")
                      no-lock no-error. 
    
    if avail slslb and not avail slsmx then do: 
      create slsmx. 
      assign slsmx.cono         = bip-apsv.cono  
             slsmx.printfl4     = no 
             slsmx.notestype    = "zz" 
             slsmx.primarykey   = "logo print" 
             slsmx.secondarykey = string(bip-apsv.vendno,"99999999999").
      assign 
             slsmx.noteln[1]    = if bip-apsv.divno = 29 then
                                    "PERF,*"                                                                    else
                                    "PGON,*". 
    end. 

 end.

 release bip-apsv.
end.

Procedure Ancillary_Tables_Update:
  Define input parameter ip-recid            as recid no-undo.

  define buffer slsmx  for notes.
  define buffer x-slsmx  for notes.
  define buffer slslb  for notes.
  define buffer bip-apsv for apsv.

  find bip-apsv where recid(bip-apsv) = ip-recid no-error.

  if avail bip-apsv then do:
 

    /* Save off the JT vendor ID for reference */
    assign bip-apsv.comment     = bip-apsv.lookupnm.  
     
    
    assign bip-apsv.edipartner  = string(bip-apsv.vendno)
           bip-apsv.lookupnm    = substring(bip-apsv.name,1,15).
   

    if bip-apsv.bankno = 93 then
      assign bip-apsv.currencyty = "CN"
             bip-apsv.divno = 040.
    else
    if bip-apsv.bankno = 41 then
      assign bip-apsv.currencyty = "CN"
             bip-apsv.divno = 040.
    else
    if bip-apsv.bankno = 51 then
      assign bip-apsv.currencyty = " "
             bip-apsv.divno = 040.
    else do:
      find crsb where
           crsb.cono    = g-cono and
           crsb.bankno =  bip-apsv.bankno no-lock no-error.
      if avail crsb then
        assign bip-apsv.currencyty = crsb.currencyty
               bip-apsv.divno = crsb.divno.
    end.
  
     
    find slsmx  where slsmx.cono         = bip-apsv.cono and 
                      slsmx.notestype    = "zz"      and
                      slsmx.primarykey   = "logo print"   and
                      slsmx.secondarykey =
                       string(bip-apsv.vendno,"99999999999")
                      no-lock no-error. 
    
    if avail slslb and not avail slsmx then do: 
      create slsmx. 
      assign slsmx.cono         = bip-apsv.cono  
             slsmx.printfl4     = no 
             slsmx.notestype    = "zz" 
             slsmx.primarykey   = "logo print" 
             slsmx.secondarykey = string(bip-apsv.vendno,"99999999999").
      assign 
             slsmx.noteln[1]    = if bip-apsv.divno = 29 then
                                    "PERF,*"                                                                    else
                                    "PGON,*". 
    end. 
 end.
 release bip-apsv.
end.

/*
procedure makeslsmx:
define input parameter ip-cono as int no-undo.

def buffer b-notes for notes. 

/*    
find first notes  where notes.cono = ip-cono         and 
                  notes.notestype    = "zz"    and
                  notes.primarykey   = "slsmx" and
                  notes.secondarykey = "label"
                  no-lock no-error.
if avail notes then return.                   
    
for each notes  where 
         notes.cono = 1       and 
         notes.notestype    = "zz"    and
         notes.primarykey   = "slsmx" and
         notes.secondarykey = "label"   no-lock: 
  create b-notes.
   buffer-copy notes except notes.cono
  to b-notes
     assign b-notes.cono = ip-cono.
  end.
*/  
end. 
 */