/* --------------------------------------------------------------------
   Include :     p-zfindbuy.i
   Description:  Determine buyer to be displayed
                 Parameter {1} is Cono
                 Parameter {2} is orderno
                 Parameter {3} is orderuf
                 Parameter {4} is line number
                 parameter {5} is buyer returned
   Date Written: 4/20/99
   Changes Made:


----------------------------------------------------------------------- */
/*
def var z-peckinx as integer.
def var z-peckmax as integer init 9.
def var z-searchlevel as integer no-undo.

def var z-peckinglist as character format "x(4)" extent 9 init
  ["dros",
   "dren",
   "dhou",
   "dmcp",
   "datl",
   "decp",
   "dscp",
   "dwcp",
   "dxcp"].


define var xzbuyer like poeh.buyer.
define var xz-whse like icsw.whse no-undo.
define buffer xzoeeh for oeeh.
define buffer xzoeel for oeel.
define buffer xzicsl for icsl.
define buffer xzicsw for icsw.
define buffer xzicsw2 for icsw.
*/
assign xzbuyer = "". 
       z-searchlevel = 0.
find xzoeeh where xzoeeh.cono = g-cono
              and xzoeeh.orderno = {2}
              and xzoeeh.ordersuf = {3} no-lock no-error.

if avail xzoeeh then
  do:
  
  find  xzoeel where xzoeel.cono = g-cono
                      and xzoeel.orderno = {2}
                      and xzoeel.ordersuf = {3}
                      and xzoeel.lineno = {4} no-lock no-error.
  if avail xzoeel then
    do:
    
    find xzicsw where xzicsw.cono = g-cono and
                      xzicsw.whse = xzoeel.whse and
                      xzicsw.prod = xzoeel.shipprod no-lock no-error.
       
    
    find first xzicsl where xzicsl.cono     = g-cono
                         and xzicsl.whse     = xzoeeh.whse
                         and xzicsl.vendno   = 
                            (if xzoeel.arpvendno <> 0 then
                                xzoeel.arpvendno 
                             else
                                xzoeel.vendno)
                         and xzicsl.prodline = xzoeel.prodline no-lock
                                                               no-error. 
    if avail xzicsl then
      do:
      assign xzbuyer = xzicsl.buyer
             z-searchlevel = 1.
      end.
    else
      do:
      find first xzicsl where xzicsl.cono     = g-cono
                         and xzicsl.whse     = xzoeeh.whse
                         and xzicsl.vendno   = 
                            (if xzoeel.arpvendno <> 0 then
                                xzoeel.arpvendno 
                             else
                                xzoeel.vendno) no-lock no-error.
      if avail xzicsl then  
        assign xzbuyer = xzicsl.buyer
               z-searchlevel = 2.
      else
        do:
        find first xzicsl where xzicsl.cono     = g-cono
                           and xzicsl.whse     = xzoeeh.whse
                           and xzicsl.prodline  = xzoeel.prodline no-lock
                                                               no-error.
        if avail xzicsl then  
          assign xzbuyer = xzicsl.buyer
                 z-searchlevel = 3.
        else 
          do:
          if avail xzicsw then
            do:
            if xzicsw.prodline = "" then
              if xzicsw.arptype = "w" then
                do:
                find xzicsw2 where xzicsw2.cono = g-cono and
                                   xzicsw2.whse = xzicsw.arpwhse and
                                   xzicsw2.prod = xzicsw.prod no-lock no-error.
                if avail xzicsw2 then                   
                  do:
                  assign xz-whse = xzicsw2.whse.
                  {p-zfindline.i
                     xz-whse}.
                  end.
                end.
              if xzbuyer = "" then
                do z-peckinx = 1 to z-peckmax:
                  {p-zfindline.i
                     z-peckinglist[z-peckinx]}.
                if xzbuyer <> "" then
                  do:
                  assign z-searchlevel = 7 + z-peckinx.
                  leave.
                  end.
               end.     
            end.
          end.
        end.     
      end.          
    end.  
  end.
if ({6} > z-searchlevel or ({6} = 0 and z-searchlevel <> 0)) and
     xzbuyer <> "" then
  assign {5} = xzbuyer
         {6} = z-searchlevel.

