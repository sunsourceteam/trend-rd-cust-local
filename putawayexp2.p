/*  output to /usr/tmp/drosexp.     */
put control "~033~046~1541~117".
def var v-time as integer no-undo.
assign v-time = time.

do for zsdiput,zsdiputdtl:
form header
space(72) "DROS & DPEO Open Putaway Exceptions" skip
today format "99/99/99" skip          
string(v-time,"HH:MM") format "xxxxxxxx" skip
with frame a-header page-top column 1 width 178
no-attr-space no-labels no-box.
view frame a-header.

for each zsdiput where zsdiput.cono = 1 and
                       (zsdiput.whse = "DROS" or
                       zsdiput.whse = "DPEO") and  
                       zsdiput.extype <> " "      
                       no-lock,

    each zsdiputdtl where zsdiput.cono = zsdiputdtl.cono and
                          zsdiput.whse = zsdiputdtl.whse and
                          zsdiput.receiptid = zsdiputdtl.receiptid and
                          zsdiput.shipprod = zsdiputdtl.shipprod and 
                          zsdiputdtl.extype <> " " and     
                          zsdiputdtl.completefl = no              
                          no-lock break by zsdiputdtl.whse:

                          disp zsdiput.whse
                               zsdiput.receiptid
                               zsdiput.putawayinit
                               zsdiputdtl.ordertype
                               zsdiputdtl.orderno
                               zsdiputdtl.ordersuf no-label
                               zsdiputdtl.lineno
                               zsdiput.shipprod 
                               zsdiput.stkqtyrcv label "Rcv Qty"
                               zsdiput.stkqtyputaway label "PutAway Qty"
                               zsdiputdtl.binloc1
                               zsdiput.receiptdt
                               zsdiput.putawaydt label "PutAway Dt"
                               zsdiput.completefl
                               zsdiputdtl.completefl label "Dtl Complete Flag"
                               with width 178. 
end.
end.

for each zsdiput where zsdiput.cono = 1 and                                    
                       (zsdiput.whse = "DROS" or                                
                       zsdiput.whse = "DPEO") and                              
                       zsdiput.extype = " " and                                
                       zsdiput.completefl = no no-lock,                        
                  
    each zsdiputdtl where zsdiput.cono = zsdiputdtl.cono and                   
                          zsdiput.whse = zsdiputdtl.whse and                   
                          zsdiput.receiptid = zsdiputdtl.receiptid and         
                          zsdiput.shipprod = zsdiputdtl.shipprod   and         
                          zsdiputdtl.extype = " " and                          
                          zsdiputdtl.completefl = no                           
                          no-lock break by zsdiputdtl.whse:                    
                              
                          disp zsdiput.whse                             
                               zsdiput.receiptid                        
                               zsdiput.receiverinit                     
                               zsdiputdtl.ordertype                     
                               zsdiputdtl.orderno                       
                               zsdiputdtl.ordersuf no-label             
                               zsdiputdtl.lineno                        
                               zsdiput.shipprod 
                               zsdiput.stkqtyrcv label "Rcv Qty"
                               zsdiput.stkqtyputaway label "PutAway Qty"
                               zsdiputdtl.binloc1                       
                               zsdiput.receiptdt                        
                               zsdiput.putawaydt label "PutAway Dt"            
                               "Not Loaded In Handheld" label "Status"
                               with width 178.                          
end.                       
