/*h****************************************************************************
  INCLUDE      : zsdioeetg.lpr from oeetg.lpr
  DESCRIPTION  : OE Print Parameters - Processing Include
  USED ONCE?   : no
  AUTHOR       : mtt
  DATE WRITTEN : 03/12/98
  CHANGES MADE :
    03/12/98 mtt; TB# 24049 PV: Common code use for GUI interface
    04/15/98 cm;  TB# 22068 SAPB record not always deleted
    03/04/99 jba; PVTB#1730 skip delete SAPB if no SASP and (sapb.backfl=yes),
                            to allow print to file/email in GUI,
    05/05/99 jl;  TB# 17865 Expanded tag updated for faxing
    07/20/99 sbr; TB# 25541 F10 - F10 Fax# defaulting from ARSC
    01/10/00 sbr; TB# 26770 F10 Fax - Invoice Print, Fax # not used
    01/11/00 sbr; TB# 26767 F10 - Fax # not defaulting using ARSE
    01/27/00 mjg; TB#e2785  Demand print not working correctly in GUI.
    11/06/01 lsl; TB# e11093 Moved the ck of sapb.filefl to allow F10
                  demand email to pass through the email where appropriate
                  code if doc format = 3.
     02/04/02 rgm; TB# e11699 Correction to Email Demand RxServer print.
     06/24/02 rgm; TB# e13550 modify to work with F10 prints and custom code
     09/20/02 bp ; TB# e14704 Add user hooks.
     01/25/03 bp ; TB# e16009 Add user hooks.
     02/02/03 bp ; TB# e16009 Add user hooks.
     10/09/03 bp ; TB# e18547 Add additional hooks.
******************************************************************************/

def            var v-adoctypefl      as l                            no-undo.
def            var v-arxserverfl     as l                            no-undo.
def            var v-idoctypefl      as l                            no-undo.
def            var v-irxserverfl     as l                            no-undo.

{oeetg.z99 &Before_Acknowledgement = "*"}  

/*o*****************  Acknowledgement Print  *******************************/
if {&ackprintfl} then do:
    assign
        v-sapbid  = ?
        v-sassrid = ?.
  
    def var okMessage as character format "x(20)" 
            initial "Creating Acknowledgement".
    put screen row 21 col 8 color message okMessage.
    ackblock:
    do /* for b-sapb, sapb, sasp, sassr, sasc, arsc, arss, sapbo */
    transaction:
        /*tb 15298 04/07/94 mwb; moved oeeh read to top to set the g-whse
             corectly for the @ack sapb read */
        /* do for oeehb: */
          find first oeehb where
                     oeehb.cono = g-cono and 
                     oeehb.batchnm = string({&orderno},">>>>>>>9") and
                     oeehb.seqno = 2 and
                     oeehb.sourcepros = "ComQu" no-lock no-error.
            /*tb 21053 04/25/96 tdd; Assign c-empty.i, not 0, on custno */
            
            assign
                g-whse   = if avail oeehb then oeehb.whse   else g-whse
                {&custno} = if avail oeehb then oeehb.custno else {c-empty.i}
                {&shipto} = if avail oeehb then oeehb.shipto else "".
        /* end. */

        /*d Find the stored report that contains the default options for the
            acknowledgement print */
        find b-sapb use-index k-sapb where
            b-sapb.cono     = g-cono and
            b-sapb.reportnm = ("@ack" + g-whse)
        no-lock no-error.

        /*tb#e2785 01/27/00 mjg; Demand print not working correctly in GUI */
        /*d Create the SAPB record for processing */
        {p-rptnm.i v-reportnm {&report-prefix}}

        /*d Find SASSR Record */
        {w-sassr.i ""oeexp"" no-lock}
        if not avail sassr then do:
            run err{&errorno}.p(4004).
            {&comui}
            {pause.gsc}
            /{&comui}* */
            {&errorexit}
            return.
        end. /* if not avail sassr */

        okMessage = okMessage + ".".
        put screen row 21 col 8 color message okMessage.
        create sapb.

        /*tb 19803 12/19/95 sbl(CGA); Force background printer if SASSR set
                   to print background only. */
        assign
            sapb.backfl      = if sassr.backfl = yes then yes else no
            sapb.starttype   = if g-whereappfl = yes then "whereapp":u else ""
            sapb.currproc    = "oeexp"
            sapb.cono        = g-cono
            sapb.reportnm    = v-reportnm
            sapb.printoptfl  = no
            sapb.priority    = 9
            sapb.printernm   = {&printernmack}
            sapb.rpttitle    = {&promomsg}.

        if not avail b-sapb
        then
            assign
               sapb.optvalue[1] = "0"
               sapb.optvalue[2] = "yes"
               sapb.optvalue[4] = "no"
               sapb.optvalue[5] = "no".
        else do:
            do i = 1 to 20:
                sapb.optvalue[i] = b-sapb.optvalue[i].
            end.

            sapb.rpttitle = b-sapb.rpttitle.
        end. /* else do */

        assign
          sapb.optvalue[1] = "0"
          sapb.optvalue[2] = "yes"
          sapb.optvalue[4] = "no"
          sapb.optvalue[5] = "no".

        okMessage = okMessage + ".".
        put screen row 21 col 8 color message okMessage.

        {&acksapbassigns}


        /* tb e11093 11/09/01 lsl; begin - added a check for format 3 docs
        (RxServer) and the email where appropriate doc type.  This sets the
        sapb.filefl = no, so that email where appropriate output is used
        instead of the email-report code in rptctl.p */
        /* TB 311699 02/04/01 rgm; We should not require the "e-mail where
        appropriate" flag be set on a demand print */
        if {&printernmack} = 'E-MAIL':u then
        do:
            {w-arsc.i {&custno} no-lock}

            if not avail arsc then do:
                run err{&errorno}.p(4303).
                {&comui}
                {pause.gsc}
                /{&comui}* */
                {&errorexit}
                return.
            end.

            v-adoctypefl = yes.

           /* set the format type */                                        
             run rxformck.p (&IF "{&NXTREND-SYS}" = "POWERVUE" &THEN          
                                input g-cono,                                                                &ENDIF                                           
                                input "ack":u,                                                                 input "m":u,                                     
                              output v-arxserverfl).                           
            assign
               sapb.filefl   = if v-adoctypefl and v-arxserverfl then no
                               else yes.
        end.

        {oeetg.z99 &aft_ackasgn = "*"}
        {t-all.i sapb}

        {w-sasp.i {&printernmack} no-lock}

        if not avail sasp or {&printernmack} = 'E-MAIL':u or 
           (avail sasp and sasp.ptype = "F") then 
            assign sapb.filefl = yes.
        else 
            assign sapb.filefl = no. 
        if avail sasp and sasp.ptype = "f" then
          assign sapb.faxphoneno = v-faxphoneno
                 sapb.faxto1     = v-faxto1
                 sapb.faxto2     = v-faxto2 
                 sapb.faxcom[1]  = sdi-faxcom[1]  
                 sapb.faxcom[2]  = sdi-faxcom[2]  
                 sapb.faxcom[3]  = sdi-faxcom[3]  
                 sapb.faxcom[4]  = sdi-faxcom[4]  
                 sapb.faxcom[5]  = sdi-faxcom[5]  
                 sapb.faxcom[6]  = sdi-faxcom[6]  
                 sapb.faxcom[7]  = sdi-faxcom[7]  
                 sapb.faxcom[8]  = sdi-faxcom[8]  
                 sapb.faxcom[9]  = sdi-faxcom[9]  
                 sapb.faxcom[10] = sdi-faxcom[10]. 
 
        {oeetg.z99 &aft_acksasp = "*"}

        /*tb 22068 04/15/98 cm; Delete temporary SAPB record before return */
        /*tb  1730 03/04/99 jba; no SASP necessary if GUI (background) */
        if ( not avail sasp )
        and sapb.backfl<>yes         /* TB 1730 */
        then do:
            {delete.gde sapb}
            run err{&errorno}.p(4035).
            {&comui}
            {pause.gsc}
            /{&comui}* */
            {&errorexit}
            return.
        end. /* not avail sasp */

        /*tb 13497 10/27/93 rhl; F10 print to fax: Pipe to Subprocess */
        /*  Add long distance code  */
        if avail sasp and sasp.ptype = "f" then do:
            if v-faxphoneno = "" then do:
                {w-sasc.i no-lock}
                /*tb 15298 04/07/94 mwb; moved OEEH find to top for g-whse and
                    loaded oeeh custno, shipto as v-'s */
                {w-arsc.i {&custno} no-lock}
                if not avail arsc then leave ackblock.

                if {&shipto} ne " " then
                    {w-arss.i {&custno} {&shipto} no-lock}

                if ({&shipto} ne " " and avail arss and arss.faxphoneno = "") or
                   ({&shipto} =  " " and avail arsc and arsc.faxphoneno = "")
                then do:

                    /* tb 20968 02/03/98 sbr; check arsso "To:" field when
                       faxing */
                    /*tb 25541 07/20/99 sbr; Removed "and arss.invtofl" from
                        if condition when faxing acknowledgement. */
                    /*tb 26767 01/11/00 sbr; Added "and arss.eackto = false"
                        to the if condition. */
                    if {&shipto} ne " " and avail arss and arss.eackto = false
                    then do:
                        {p-fxardf.i &file   = "arss."
                                    &extent = 2}
                    end. /* {&shipto} ne "" */

                    else do:
                        {p-fxardf.i &file   = "arsc."
                                    &extent = 2}
                    end. /* else */

                    /*u User Hook just prior to p-fxparm.i */
                    {oeetg.z99 &user_002 = "*"}

                    /*tb 16974 11/08/94 rs; Need > 2 comment lines */
                    /*tb 17288 06/16/95 dww; Unix Error in On-Line Doc. */
                    {&comfax}
                    {p-fxparm.i &proc = "oeetg"
                                &pre  = "v-"}
                    if {k-endkey.i} then do:
                        delete sapb.
                        g-errfl = no.
                        run warning.p(6339).
                        return.
                    end. /* k-endkey.i */
                    /{&comfax}* */

                    {&ackfaxprocessing}

                end. /* {&shipto} ne "" */

            end. /* v-faxphoneno = "" */

            /*tb 19314 09/15/95 dww; Remove Imbedded Space from Fax Phone
                Number */
            /*tb 17865 05/05/99 jl; Added &faxtag1 and &faxtag2 */
            {p-fxasgn.i &option = 7
                        &faxtag1 = "sapb.faxto1"
                        &faxtag2 = "'OE: ' + string(g-orderno) + '-' +

                                      string(g-ordersuf,'99')"
                        &comvar = "/*"}

        end. /* avail sasp */

        /*d Ensure the existance of the necessary records */
        {w-sapb.i g-cono v-reportnm exclusive-lock}
        if not avail sapb then do:
            run err{&errorno}.p(4036).
            {&comui}
            {pause.gsc}
            /{&comui}* */
            {&errorexit}
            return.
        end. /* not avail sapb */

        okMessage = okMessage + ".".
        put screen row 21 col 8 color message okMessage.
        /*d Create the SAPBO record for the order to print */
        create sapbo.

        /*tb 26770 01/10/00 sbr; Removed " and sapb.faxphoneno = "" " from the
            if clause within the assign statement for sapbo.outputty. */
        assign sapbo.orderno   = {&orderno}
               sapbo.ordersuf  = {&ordersuf}
               sapbo.reportnm  = v-reportnm
               sapbo.cono      = g-cono
               sapbo.reprintfl = no
               sapbo.route     = ""
               sapbo.outputty  = if available sasp      /* TB#1730 */
                                 and sasp.ptype = "f"
                                 then "f" else "p".

        {oeetg.z99 &aft_ackoasgn = "*"}

        if avail sasp and sasp.ptype = "f" then
          do:
          assign sapbo.user3  = v-faxphoneno   
                 sapbo.user4  = v-faxto1
                 sapbo.user5   = sapb.faxcom[1] + " " +   
                                 sapb.faxcom[2] + " " +   
                                 sapb.faxcom[3] + " " +   
                                 sapb.faxcom[4] + " " +   
                                 sapb.faxcom[5] + " " +   
                                 sapb.faxcom[6] + " " +   
                                 sapb.faxcom[7] + " " +   
                                 sapb.faxcom[8] + " " +   
                                 sapb.faxcom[9] + " " +   
                                 sapb.faxcom[10].   
          end.  
        
        if s-printernmack = "E-Mail" then
          do:
          assign sapbo.outputty = "m" 
                 sapbo.user3  =  substring(sapb.faxphoneno,4,
                                           length(sapb.faxphoneno) - 3)   
                 sapbo.user5   = sapb.faxcom[1] + " " +   
                                sapb.faxcom[2] + " " +   
                                sapb.faxcom[3] + " " +   
                                sapb.faxcom[4] + " " +   
                                sapb.faxcom[5] + " " +   
                                sapb.faxcom[6] + " " +   
                                sapb.faxcom[7] + " " +   
                                sapb.faxcom[8] + " " +   
                                sapb.faxcom[9] + " " +   
                                sapb.faxcom[10].   
          end.  
  
        {t-all.i sapbo}

        assign  v-sapbid  = recid(sapb)
                v-sassrid = recid(sassr).

        /*tb 19803 01/16/96  vlp(CGA); Report Scheduler. Create RSES record
             and skip run if running in background */
        if sapb.backfl = yes then do:
            assign
                v-sapbid     = ?
                sapb.delfl   = yes
                sapb.startdt = today
                sapb.starttm = time.

            {&comrssched}
            {rssched.gpr}
            /{&comrssched}* */
            {&reportsched}

        end. /* sapb.backfl = yes */

    end. /* ackblock */

    okMessage = okMessage + ".".
    put screen row 21 col 8 color message okMessage.

    /*d Print the acknowledgement */
    {&comoeepa}
    if v-sapbid ne ? then do:
        run rptctl2.p("oeexcp.p",yes,0,yes,0,yes).
                      /* program to run,
                         open printer(y/n),
                         printer # to open (0-5),
                         close output (y/n),
                         output # to close (0-5),
                         print files (y/n).   */
                                                   
        do /* for sapb, sapbo */ transaction:
            {w-sapb.i g-cono v-reportnm exclusive-lock}
            if avail sapb then delete sapb.

            {w-sapbo.i g-cono v-reportnm {&orderno} {&ordersuf}
                exclusive-lock}
            if avail sapbo then delete sapbo.
        end. /* do for sapb, sapbo transaction */

    end. /* v-sapbid ne ? */
    /{&comoeepa}* */

   put screen row 21 col 8 "                                         ".
   
end. /* Acknowledgement Print */


