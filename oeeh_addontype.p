def buffer x-oeeh for oeeh.
def var x-date as  date format "99/99/99".
def var o-date as  c    format "x(8)".
def var x-file as  c    format "x(30)".
def var i      as  i    format "9".

assign x-date = TODAY.
assign o-date = string(MONTH(x-date)) +
                string(DAY(x-date))   +
                string(YEAR(x-date)).
assign x-file = "/usr/tmp/addontypeAUD." + o-date.

output to value(x-file).

for each icsd where icsd.cono = 1 and icsd.salesfl = yes no-lock:
  for each oeeh where oeeh.cono     = icsd.cono and
                      oeeh.whse     = icsd.whse and
                      oeeh.stagecd  = 3 and
                      oeeh.approvty = "y"
                      no-lock:

    
        
    for each addon use-index k-seqno where
             addon.cono      = icsd.cono       and
             addon.ordertype = "oe" and
             addon.orderno   = oeeh.orderno   and
             addon.ordersuf  = oeeh.ordersuf and
             addon.addonno   = 1 no-lock:
    /* 
    do i = 1 to 4:
    */
      if addon.addontype = no and addon.addonamt <> 0 and 
         addon.addonamt <> addon.addonnet then
        do:
        find sastn where sastn.cono = 1       and
                         sastn.codeiden = "a" and
                         sastn.codeval  = addon.addonno
                         no-lock no-error.
        if avail sastn and sastn.addontype <> addon.addontype then
          do:
          export delimiter ","
            oeeh.orderno oeeh.ordersuf oeeh.transtype oeeh.approvty
            addon.addonamt addon.addontype addon.addonnet.
          find x-oeeh where x-oeeh.cono     = 1            and
                            x-oeeh.orderno  = oeeh.orderno and
                            x-oeeh.ordersuf = oeeh.ordersuf
                            no-error.
          if avail x-oeeh then
           assign x-oeeh.approvty = "z".
          leave.
        end.
      end. /* if addontype = % and addonamt <> 0 */
    end. /* i = 1 to 4 */
  end. /* each oeeh */
end. /* each icsd */