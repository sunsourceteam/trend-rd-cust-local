/*****************************************************************************
   
 pdxmcfile1.i  - Contract Price Matrix Reporting - 
                  This include is called by pdxmc.p.
                  Prints the report
                
*****************************************************************************/
  assign wk-ytd-sales  = 0
         wk-pytd-sales = 0.
  
  for each forecast use-index k-type1  where forecast.contract-type = 1
                                             no-lock 
                                              
      break by forecast.region
            by forecast.district
            by forecast.ytd-sales descending
            by forecast.customer
            by forecast.ship-to
            by forecast.product:

      
      assign wk-i-mrg  = 0
             wk-c-mrg  = 0
             wk-oc-mrg = 0
             wk-g-mrg  = 0
             w-change  = 0
             w-impact  = 0.
      
      if (last-of(forecast.region)) then do:
         assign counter = 0.
         if p-avgcost = yes then               
            assign w-costtype = "  Average   ".
         else                                  
            assign w-costtype = "Replacement ".
               
         PAGE.
      end.
      
      if (first-of(forecast.district)) then do:
          if not first-page then do:
             if p-avgcost = yes then               
                assign w-costtype = "  Average   ".
             else                                  
                assign w-costtype = "Replacement ".
             PAGE.
          end.
          else
             assign first-page = no.
       end.

      if forecast.i-sales <> 0 then
         assign wk-i-mrg =             /*  (net-sales - cost) / net-sales)  */
             ((forecast.i-sales - forecast.i-cost) 
                                     / forecast.i-sales) * 100
                                     
                w-tot-isales = w-tot-isales + forecast.i-sales
                w-isales     = w-isales + forecast.i-sales
                w-icost      = w-icost  + forecast.i-cost
                w-tot-icost  = w-tot-icost  + forecast.i-cost.
      else
         assign wk-i-mrg = 0.

      if forecast.oc-sales <> 0 then
         assign wk-oc-mrg =    /* oc-sales already has discounts considered */
                   ((forecast.oc-sales - forecast.oc-cost) / forecast.oc-sales)
                               * 100
                
                w-tot-ocsales = w-tot-ocsales + forecast.oc-sales
                w-ocsales     = w-ocsales + forecast.oc-sales
                w-occost      = w-occost  + forecast.oc-cost
                w-tot-occost  = w-tot-occost  + forecast.oc-cost.
      else
         assign wk-oc-mrg = 0.

      if forecast.c-sales <> 0 then
         assign wk-c-mrg =      /* c-sales already has discounts considered */
                      ((forecast.c-sales - forecast.c-cost) / forecast.c-sales)
                               * 100
                
                w-tot-csales = w-tot-csales + forecast.c-sales
                w-csales     = w-csales + forecast.c-sales
                w-ccost      = w-ccost  + forecast.c-cost
                w-tot-ccost  = w-tot-ccost  + forecast.c-cost
                w-change     = ((forecast.c-sales - forecast.oc-sales) /
                                 forecast.c-sales) * 100.
      else
         assign wk-c-mrg = 0
                w-change = 0.
      
      if forecast.g-sale <> 0 then 
         assign wk-g-mrg       /* g-sales already has discount considered */
                   = ((forecast.g-sales - forecast.g-cost) / forecast.g-sales)
                               * 100
                
                w-tot-gsales = w-tot-gsales + forecast.g-sales
                w-gsales     = w-gsales + forecast.g-sales
                w-gcost      = w-gcost  + forecast.g-cost
                w-tot-gcost  = w-tot-gcost  + forecast.g-cost.

      else
         assign wk-g-mrg = 0.
         
      if (last-of(forecast.customer)) then
          assign wk-ytd-sales  = wk-ytd-sales  + forecast.ytd-sales.
                 
      if (last-of(forecast.product)) then
          assign w-impact = ((wk-c-mrg - wk-oc-mrg) * forecast.adj-prod-sales)
                                                                        / 100
                 wk-prod-ytd-sales = wk-prod-ytd-sales + 
                                                    forecast.adj-prod-sales.

         display forecast.customer
                 forecast.ship-to
                 forecast.name
                 forecast.salesrep
                 forecast.region
                 forecast.district
                 forecast.ytd-sales
                 forecast.adj-prod-sales
                 forecast.product
                 forecast.mult-price
                 forecast.prc-type
                 forecast.new-mult-price
                 forecast.new-prc-type
                 forecast.qtybreakch
                 w-change
                 forecast.pdrecno
                 w-impact
                 wk-c-mrg
                 wk-oc-mrg
                 wk-i-mrg
                 /*
                 wk-g-mrg
                 */
                 with frame forecast1-detail.
                 down with frame forecast1-detail.
      
      if (last-of(forecast.district)) then do:
                  
         if substring(e-region,2,3) = "999" then do:
            if substring(e-region,1,1) = "n" then
               assign wk-rgn = "Nrth".
            if substring(e-region,1,1) = "f" then
               assign wk-rgn = "Filt".
            if substring(e-region,1,1) = "s" then
               assign wk-rgn = " Sth".
            if substring(e-region,1,1) = "m" then
               assign wk-rgn = "Mobl".
            if substring(e-region,1,1) = "p" then
               assign wk-rgn = " Pab".
            if substring(e-region,1,1) = "r" then
               assign wk-rgn = "Srvc".

         end.
         else
             assign  substring(wk-rgn,1,1)   = forecast.region
                     substring(wk-rgn,2,3)   = forecast.district.
                     
         assign  wk-oc-mrg = (w-ocsales - w-occost) / w-ocsales * 100
                 wk-i-mrg  = (w-isales  - w-icost)  / w-isales * 100
                 wk-c-mrg  = (w-csales  - w-ccost)  / w-csales * 100
                 wk-g-mrg  = (w-gsales  - w-gcost)  / w-gsales * 100.
                 
         assign  wk-tot-impact = ((wk-c-mrg - wk-oc-mrg) * wk-prod-ytd-sales)
                                                                         / 100.

                  
         display wk-rgn
                 wk-ytd-sales
                 wk-prod-ytd-sales
                 wk-tot-impact
                 wk-oc-mrg
                 wk-c-mrg
                 wk-i-mrg
                 /*
                 wk-g-mrg
                 */
                 with frame forecast1-totals.
                 down with frame forecast1-totals.
                 
         assign  wk-grand-ytd-sales  = wk-grand-ytd-sales  + wk-ytd-sales
                 wk-grand-pytd-sales = wk-grand-pytd-sales + wk-pytd-sales
                 wk-grand-prod-ytd-sales = wk-grand-prod-ytd-sales +
                                           wk-prod-ytd-sales.
         assign  wk-ytd-sales = 0
                 wk-pytd-sales = 0
                 wk-prod-ytd-sales = 0
                 wk-tot-impact = 0
                 wk-i-mrg  = 0
                 wk-c-mrg  = 0
                 wk-oc-mrg = 0
                 wk-g-mrg  = 0
                 w-isales  = 0
                 w-csales  = 0
                 w-ocsales = 0
                 w-gsales  = 0
                 w-icost   = 0
                 w-ccost   = 0
                 w-occost  = 0
                 w-gcost   = 0
                 wk-rgn    = ""
                 counter   = 0.    
         if p-avgcost = yes then               
            assign w-costtype = "  Average   ".
         else                                  
            assign w-costtype = "Replacement ".
               
         page.
      end.
      
      
      
  end.   /* for each matrix */   
   

  assign wk-i-mrg  = ((w-tot-isales  - w-tot-icost)  / w-tot-isales)  * 100
         wk-c-mrg  = ((w-tot-csales  - w-tot-ccost)  / w-tot-csales)  * 100
         wk-oc-mrg = ((w-tot-ocsales - w-tot-occost) / w-tot-ocsales) * 100
         wk-g-mrg  = ((w-tot-gsales  - w-tot-gcost)  / w-tot-gsales)  * 100.

  assign wk-grand-impact = ((wk-c-mrg - wk-oc-mrg) * wk-grand-prod-ytd-sales)
                                                                         / 100.
  
  display wk-grand-ytd-sales
          wk-grand-prod-ytd-sales
          wk-grand-impact
          wk-oc-mrg
          wk-c-mrg
          wk-i-mrg
          /*
          wk-g-mrg
          */
         
          with frame forecast1-grand.
          down with frame forecast1-grand.
