/*h****************************************************************************
  INCLUDE      : u-icib.i
  DESCRIPTION  : Update list for ICIB
  USED ONCE?   : yes
  AUTHOR       : NxT
  DATE WRITTEN : 07/27/93
  CHANGES MADE :
                 09/10/99 cm;  TB# 13389 Add WT Transfer Unit
                 05/02/00 des; TB# e5061 Inventory Allocation Project - Add                  reservation type
                 06/08/00 maa; TB# e5061 Inventory Allocation Project - Add                  reservation days
    ******************************************************************************/
/*tb 12355 07/27/93 pjt; Added s-prodstat */
/*tb 13389 09/10/99 cm; Add WT Transfer Unit */
/*tb e5061 05/02/00 des; Inventory Allocation Project - Add s-reservety  */
/*tb e5061 06/08/00 maa; Inventory Allocation Project - Add s-reservedays  */
g-prod
icsp.notesfl
g-whse
s-unit
icsp.descrip
icsw.binloc1    when icsw.binloc1 ne ""
icsw.binloc2    when icsw.binloc2 ne ""
s-source
s-sourceno
s-status
icsw.class
s-prodstat
s-reservety
s-reservedays
/* Section 2 */
s-days
/* Line 1 */
s-qtyonhand
s-qtyonord
s-usagerate
s-usagetxt
/* Line 2 */
s-qtyreservd
s-qtyrcvd
s-ordpttext
s-ordpt
/* Line 3 */
s-qtycommit
s-qtybo
s-linepttext
s-linept
/* Line 4 */
s-wtdemand
s-ordqty
/* Line 5 */
s-availnow
s-availfut
s-ordcalcty
/* Section 3 */
apsv.name           when avail apsv
icsw.arpvendno      when icsw.arpvendno ne 0
icsl.buyer          when avail icsl
icsw.prodline
icsw.famgrptype
icsw.lastpowtdt
icsw.lastrcptdt
icsw.lastinvdt
icsw.lastsodt
icsw.lastcntdt
icsw.pricetype
icsw.leadtmavg
s-poduedt
s-wtduedt
s-safeallow
s-seastext
s-seasord
icsw.unitbuy
icsw.unitstnd
icsw.unitwt
icsp.unitstock
s-alttext

                            