/* vaepi1in.lpr 1.1 01/03/98 */
/* vaepi1in.lpr 1.1 08/21/96 */
/*h****************************************************************************
    INCLUDE      : vaepi1in.lpr
    DESCRIPTION  : Print Inventory Compnts & Inventory IN on Internal Processes
    USED ONCE?   : yes
    AUTHOR       : smn
    DATE WRITTEN : 08/21/96
    CHANGES MADE :
        08/21/96 smn; TB# 21254 (11A) Develop Value Add Module
        12/03/96 bao; TB# 21254 (21B) Add underline to separate 'in'& 'ii'
            headers from 'in'&'ii' data; add 'N'on-stock field; Make changes so
            this include can be used for both inventory components and
            inventory in vaes records
******************************************************************************/
/*e

    Variables that must be loaded for this include are:

    Required parameters used in this include are:
     &stitle    - Used to display appropriate section header
     &qthead    - The Returned Quantity heading
     &xdash1    - Extra dashes required to underscore &qthead
     &xdash2    - Extra dashes to mark a space for write-in of returned qty

    Optional parameters used in this include are:
*/

if vaesl.nonstockty ne "n" then 
  {w-icsp.i vaesl.shipprod no-lock}

if v-vafstinvfl = yes then do:
    assign
        v-vafstinvfl   = no
        s-vadata       = {&stitle}.
/* printable -----
    if p-detail then
      do:
      display s-vadata with frame f-vadata.
      down with frame f-vadata.

      assign
        s-vadata                  = ""
        substring(s-vadata,1,35)  = "-----------------------------------"
        substring(s-vadata,36,35) = "-----------------------------------"
        substring(s-vadata,71,56) = 
               "--------------------------------------------------------"
        substring(s-vadata,127,1) = {&xdash1}. 
      display s-vadata with frame f-vadata.
      down with frame f-vadata.
      end.
 */
end. /* if v-vafstinvfl = yes */


assign va-rebamt = 0.
if b-vaes.sctntype  = "in" or b-vaes.sctntype  = "ii" then 
do:
find first pder use-index k-pder where 
           pder.cono     = g-cono          and 
           pder.orderno  = oeeh.orderno    and 
           pder.ordersuf = oeeh.ordersuf   and  
           pder.lineno   = vaelo.linealtno and 
           pder.shipprod = vaesl.shipprod
           no-lock no-error.
   if avail pder then       
    do: 
       assign va-rebamt = pder.rebateamt.
    end.    
   else 
       assign va-rebamt = 0.
end.
       
/*d Print the notes for the section */
if v-seqhld ne b-vaes.seqno and b-vaes.sctntype = "in" then 
do:
  if avail b-vaes then 
   if b-vaes.notesfl = "" then
    do:
      assign v-seqhld  = b-vaes.seqno
             s-comdata = "Section " + b-vaes.sctntype +
                         " - " +  "Seq# " + string(vaesl.seqno,"zz9"). 
 /* --- printable    

      if p-detail then
        do:
        display s-comdata no-label with frame f-com3.
        down with frame f-com3.
        assign s-comdata = "--------------------------------".
        display s-comdata no-label with frame f-com3.
        down with frame f-com3.
        end.
*/
     end.
    if b-vaes.notesfl ne "" then
    do:
      assign v-seqhld  = b-vaes.seqno
             s-comdata = "Section " + b-vaes.sctntype +
                         " - " +  "Seq# " + string(vaesl.seqno,"zz9") +
                         " Note(s): ".

      if p-detail then
        do:
        display s-comdata no-label with frame f-com3.
        down with frame f-com3.
        assign s-comdata = "--------------------------------".
        display s-comdata no-label with frame f-com3.
        down with frame f-com3.
         {vaepi1nt.lpr &notestype    = ""fl""
                       &primarykey   = "string(b-vaes.vano) + '-' +
                                        string(b-vaes.vasuf)"
                       &secondarykey = "string(b-vaes.seqno)"}
        end.
    end. /* if b-vaes.notesfl ne "" for section */
end.

if vaesl.nonstockty ne "n" then
    {w-icsp.i vaesl.shipprod no-lock}

find first saindex use-index k-saindex where 
           saindex.cono     = g-cono         and 
           saindex.xtype    = "zx"           and 
           saindex.prod     = vaesl.shipprod and 
           saindex.user3    = "fullprod"
           no-lock no-error.

if p-margpct > 0 and b-vaes.sctntype ne "ex" then 
 do:  
   qa-netamt = (((vaesl.prodcost * 1.03) - va-rebamt) * vaesl.qtyord) +
              ((((vaesl.prodcost  * 1.03) - va-rebamt) * vaesl.qtyord) 
                  * p-margpct).
  
   qa-cost   =  ((vaesl.prodcost * 1.03) - va-rebamt) + 
                (((vaesl.prodcost * 1.03) - va-rebamt) * p-margpct).

 end.
else 
if b-vaes.sctntype ne "ex" then 
 do:
   qa-netamt = (((vaesl.prodcost * 1.03) - va-rebamt) * vaesl.qtyord).  
   qa-cost   =   (vaesl.prodcost * 1.03) - va-rebamt. 
  end.
else 
 do:
   qa-netamt = ((vaesl.prodcost * 1.03) * vaesl.qtyord).  
   qa-cost   =   vaesl.prodcost * 1.03. 
  end.
  
if qa-netamt < 0 then 
   s-sign = "-".
else    
   s-sign = "".

if substring(vaesl.user5,25,1) = "n" /* and vaesl.sctntype ne "ii" */ then 
 do:
  assign qa-miscamt    = qa-miscamt + qa-cost
         qa-misctotamt = qa-misctotamt + qa-netamt.
  
 end.
else
if substring(vaesl.user5,25,1) ne "n" /* or vaesl.sctntype = "ii" */ then 
do: 
assign s-netamt   = if p-prtprc then 
                      string(qa-netamt,"zzzzzz9.99-") 
                    else "" 
       s-lineno   = string(vaesl.lineno,"zz9")
       s-qtyord   = string(vaesl.qtyord,"zzzzzz9") 
       v-unit     = vaesl.unit
       s-prcunit  = vaesl.unit
       s-price    = if p-prtprc then                 
                        string(qa-cost,"zzzzz9.99") 
                    else ""                          
       v-shipprod = if p-desconly then                        
                     ""                                     
                    else                                      
                    if substring(vaesl.user5,1,24) ne "" then 
                     substring(vaesl.user5,1,24)            
                    else                                      
                     vaesl.shipprod
       s-descrip  = if vaesl.nonstockty = "n" then          
                       vaesl.proddesc                       
                    else if avail saindex and saindex.user4 ne "" then 
                            saindex.user4
                    else if avail icsp then icsp.descrip[1] 
                    else v-invalid.                          

 if b-vaes.sctntype = "ii" then 
    assign s-netamt = if p-prtprc then
                        string(dec(s-netamt) * -1,"zzzzzz9.99-")
                      else " ". 
 if p-detail then
   do:
   display  s-netamt "   " @ s-lineno  s-qtyord v-shipprod  
            v-unit   s-prcunit s-price  s-descrip  
    with frame f-vaesl.
   down with frame f-vaesl.
   end.
end.
       
