/*h*****************************************************************************
  INCLUDE      : p-kperb.i
  DESCRIPTION  : Kit Component Processign for KPER (KP RRAR)
  USED ONCE?   : no (p-kperb.i)
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    05/29/92 pap; TB# 6596 - Allow negative components on kits
    10/18/93 kmw; TB# 13332 Component quantity; add {&qty}
    02/01/95 dww; TB# 13191 Set OEELK StatusType correctly.  Use new include
        'oeelk.gcr' for OEELK record creation.
    06/26/95 mtt; TB# 18611 Make changes for KP Work Order Demand (W1)
    03/16/97 mms; TB#  7241 Spec8.0 Special Price Costing; added assign of the
        icspecrecno.
    05/20/03 tmg; TB# e17279 Cores kit processing
*******************************************************************************/

/*d Validate all components */
if v-errfl = "" then do:
    {q-oeetli.i &prod    = "{&file}.comprod"
                &unit    = "{&file}.unit"
                &comment = "/*"
                &kitprodtype = v-kitprodtype}

    if v-errfl <> "" then
        assign s-type = " Component "
               s-prod = {&file}.comprod.
end.   /* v-errfl is blank */

/*d Display an Error Message if it exists */
if v-errfl <> "" then do:
    if s-special = "" then do:
        v-kqtybld = 0.
        /*d Display ordering information */
        {p-kperd.i}
        /*d Print the error message */
        run kperpe.p.
    end.

    /*d Remove all records if an error exists */
    if p-update then do:
        {p-kperc.i}
    end.

    if s-special = "" then next {&label}.
end.    /* if v-errfl is not blank */

/*d Calculate the total qty needed, qty available & build qty */
/*tb 6596 05/29/92 pap; Exclude negative components from calculation */
/*tb 13332 10/18/93 kmw; Component quantity incorrectly set */
if {&file}.qtyneeded > 0 and {&qty} > 0 then
    assign v-cqtyneed    = {&file}.qtyneeded * {&qty} * v-kconv

           
           
           v-cnetavail   = if avail b2-icsw then
                                    b2-icsw.qtyonhand  -
                                    b2-icsw.qtyreservd -
                                    b2-icsw.qtycommit
                           else 0
           {p-kperb.z99 &netavail = "*"}
           v-totcqtyneed = v-cqtyneed * k-stkqtyord
           v-cqtybld     = v-cnetavail / v-cqtyneed
           v-totbld      = if v-first and not p-update then
                               if v-totbld > v-cqtybld then v-cqtybld
                               else v-totbld
                           else v-totbld.

if p-update and v-errfl = "" then do:

    /*d Assign the next available Sequence # */
    /*tb 18611 06/26/95 mtt; Make changes for KP Work Order Demand */
    find last b-oeelk use-index k-oeelk where
              b-oeelk.cono      = g-cono and
              b-oeelk.ordertype = "w"    and
              b-oeelk.orderno   = g-wono and
              b-oeelk.ordersuf  = g-wosuf
    no-lock no-error.

    v-nextseqno = if avail b-oeelk then b-oeelk.seqno + 1
                  else 1.

    /*tb 13191 02/01/95 dww; Set StatusType on all OEELK records created.
        Use new include 'oeelk.gcr' */
    /*d Create the OEELK records; Assign the icspecrecno from the current
        icsp record data. */
    {oeelk.gcr
        &ordertype  = "'w'"
        &orderno    = "g-wono"
        &ordersuf   = "g-wosuf"
        &lineno     = "0"
        &seqno      = "v-nextseqno"}

    assign
        oeelk.conv         = v-kconv
        oeelk.comptype     = kpsk.comptype
        oeelk.groupoptname = if kpsk.comptype = "g" then kpsk.comprod
                             else ""
        oeelk.shipprod     = {&file}.comprod
        oeelk.qtyneeded    = {&file}.qtyneeded * {&qty}
        oeelk.qtyord       = oeelk.qtyneeded * k-stkqtyord
        oeelk.stkqtyord    = oeelk.qtyneeded * k-stkqtyord * v-kconv
        oeelk.qtyship      = oeelk.qtyord
        oeelk.stkqtyship   = oeelk.stkqtyord
        oeelk.serlottype   = if can-do("s,l",b2-icsw.serlottype)
                                 then b2-icsw.serlottype
                             else ""
        oeelk.subfl        = {&file}.subfl
        oeelk.variablefl   = {&file}.variablefl
        oeelk.pricefl      = {&file}.pricefl
        oeelk.reqfl        = {&file}.reqfl
        oeelk.printfl      = {&file}.printfl
        oeelk.unit         = {&file}.unit
        oeelk.whse         = g-whse
        oeelk.icspecrecno  = b-icsp.icspecrecno.

    {t-all.i oeelk}
end.

/* If this is not a labor product, calculate qty to build */
/*tb 6596 05/29/92 pap; Exclude negative components from the calculation, too */
/*tb 13332 10/18/93 kmw; Component quantity incorrectly set */
if b-icsp.statustype <> "l" and {&file}.qtyneeded > 0 and {&qty} > 0 then
    assign v-cstkqtyord     = {&file}.qtyneeded * {&qty} * v-kconv *
                              k-stkqtyord
           v-kqtybld        = if (v-cqtybld < k-stkqtyord) and
                                 (v-cqtybld < v-kqtybld)
                                  then v-cqtybld
                              else v-kqtybld
           v-kqtybld        = if v-kqtybld > k-stkqtyord
                                  then truncate(k-stkqtyord,0)
                              else truncate(v-kqtybld,0)
           v-kqtybld        = if v-kqtybld <= 0 then 0
                              else v-kqtybld.
