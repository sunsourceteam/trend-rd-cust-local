
/*sx5.5*/
/* oeepi9.p 5/27/97 */
/*si**** Custom ****************************************************************
CLIENT  : 80281 SDI
JOB     : sdi009 Custom invoice print
AUTHOR  : msk
VERSION : 7
CHANGES :
    si01 5/27/97 msk; 80281/sdi009; initial changes
        - NOTE: unlike std RxLaser jobs, this document acts a bit screwy; it
          seems to ignore the v-tof variable on first pages. Therefore, to
          insure it honors the command, I have put the v-tof variable in the
          page-top frame and removed it from the 'put control' statement.
    si02 6/13/97 msk; 80281/sdi00901
        - use a ZZ note to print remit-to address
    si03 10/28/97  SV; sdi027; Upgraded Trend v8
    si04 03/04/99 pcs; sdi065; added changes for Canadian Currency print
    si05 04/08/99 gc;  sdi06502 - split gst/pst taxes on invoice
    it01 11/21/97 jph; make lockbox address division sensitive 
         06/10/09 das; Allow 0 amount invoices that were tendered to print
*******************************************************************************/

/* oeepi1.p 1.15 9/28/93 */
/*******************************************************************************
  PROCEDURE            : oeepi1
  DESCRIPTION          : order entry invoice processing format 1 - NEMA
  AUTHOR               : kmw
  DATE LAST MODIFIED   : 02/16/90
  CHANGES MADE AND DATE: 01/16/91 mwb; split out p-oeepi1.i for oversize
                         06/06/92 mwb; TB# 6889; removed oeeh.payamt and
                                       replaced with oeeh.tendamt.
                         06/13/92 mjm; TB# 6756; added "oerd" to the minimum and
                                       counter sales check for printing
                         06/18/92 mkb; TB# 6756; added logic to not check
                                       minimum if ourproc = "oerd"
                         06/19/92 mkb; TB# 4531; removed conditions to check
                                       if invoice should be printed to v-oeepi.i
                         07/20/92 mkb; TB# 7191 Alphanumeric changes for custno
                         11/11/92 mms; TB# 8561 Display product surcharge label
                                       from ICAO
                         02/04/93 mwb; TB# 9760; made comment of 4531 for mkb
                                       more visible with the ***** boarder.
                         09/07/93 rs;  TB# 12932 Trend Doc Mgr for Rel 5.2
                         02/06/95 wpp; TB# 14181 Expand sapbo.seqno format.
                         11/05/97 JPH; TB#11/05/97 Obtain oe addon description
                                       2 for addon 1 + 2 + 3 single entry. Also
                                       combined on invoice print.
                         it01     jph; get lockbox based on division
                         it02     jph; added logic to get invoice heading 
                                       phone numbers from oimsp file. 
                         IT#4 07/31/98 dbf; add logic to skip RM and CR orders
                                       and orders with zero total invoice amt
                                       and to segregate multiple page invoices
                                       together.

 TAH 021207  Addition of P500 logic 

*******************************************************************************/

define input parameter ip-operinit like sasos.operinit  no-undo.
define input parameter ip-cono     like oeeh.cono      no-undo.
define input parameter ip-orderno  like oeeh.orderno   no-undo.
define input parameter ip-ordersuf like oeeh.orderno   no-undo.
define input parameter ip-sapboid   as   recid           no-undo.




{p-rptbeg.i}
{g-oeepiM.i " " " " "/*"}
define shared buffer b-arsc for arsc.
define shared buffer b-icsp for icsp.
define shared buffer b-icsw for icsw.
define shared buffer b-icsc for icsc. 
define shared buffer b-icss for icss.  

/*
 create sapbo.
         assign 
            sapbo.cono  = g-cono 
            sapbo.reportnm = string(g-operinit) + sapb.reportnm
            sapbo.orderno  = ip-orderno  
            sapbo.ordersuf = ip-ordersuf 
            sapbo.outputty = "p"
            sapbo.transproc =  "oeepi".

*/ 



def var v-index         as i               no-undo.
def var v-line          as i initial 46    no-undo.

/* si01; in lasersdi
def var v-pageno        as i               no-undo.
*/

def var v-type          as c format "x(1)" initial "o" no-undo.
def var v-headingfl     as logical          no-undo.
def var s-phonehd       as c format "x(18)" no-undo.  /* it02 */
def var s-faxhd         as c format "x(13)" no-undo.  /* it02 */    

def var s-sname-phonehd as c format "x(35)" no-undo.
def var s-semail        as c format "x(30)" no-undo.
def var s-cname-phonehd as c format "x(35)" no-undo.
def var s-cemail        as c format "x(30)" no-undo.
def var x-phone         as c format "x(18)" no-undo.
def var x-company       as c format "x(4)"  no-undo.

def var s-lit41a        as c format "x(18)" no-undo.
def var s-lit42a        as c format "x(18)" no-undo.
def var s-lit43a        as c format "x(18)" no-undo.
def var s-lit44a        as c format "x(18)" no-undo.
def var s-lit45a        as c format "x(18)" no-undo.
def var s-lit46a        as c format "x(18)" no-undo.
def var s-lit46b        as c format "x(18)" no-undo.
def var s-lit47a        as c format "x(18)" no-undo.
def var s-lit47b        as c format "x(18)" no-undo.
def var s-lit48c        as c format "x(10)" no-undo.
def var s-lit48d        as c format "x(20)" no-undo.
def var s-lit49a        as c format "x(20)" no-undo.
def var s-lit50a        as c format "x(20)" no-undo.
def var s-lit51a        as c format "x(18)" no-undo.
def var s-lit52a        as c format "x(18)" no-undo.
def var s-lit53a        as c format "x(18)" no-undo.
def var s-addondesc     as c format "x(18)" extent 4 no-undo.
def var s-taxes         as de format "zzzzzzzz9.99-" no-undo.
def var s-dwnpmtamt     as de format "zzzzzzzz9.99-" no-undo.
def var s-serialno      like icses.serialno extent 5 no-undo.
def var s-shiptoaddr    like oeeh.shiptoaddr         no-undo.
def var v-mininvamt     like oeeh.totinvamt          no-undo.
def var v-printfl       as logical                   no-undo.
def var v-invtofl       as logical                   no-undo.
def var v-printokfl     as logical                   no-undo.
def var v-totlineamt    like oeeh.totlineamt initial 0 no-undo.
def new shared var v-subnetamt     like oeeh.totinvamt          no-undo.
def new shared var v-totqtyshp     like oeeh.totqtyshp          no-undo.
def new shared var v-linecnt       as i                         no-undo.
def new shared var v-oeehrecid     as recid no-undo.
def     shared var v-coldfl        like sassr.coldfl            no-undo.
/*tb 4531 07/01/92 mms made new shared for v-oeepix.i in oeepi1.p */
def new shared var p-shipfl        as logical                   no-undo.
/*tb 8561 11/11/92 mms */
def var v-icdatclabel              as c format "x(9)"           no-undo.

/* si01; new variables */
def var s-stub1     as c format "x(15)"     no-undo.
def var v-stub1     as c format "x(20)"     no-undo.
def var s-stub2     as c format "x(14)"     no-undo.
def var v-stub2     as c format "x(13)"     no-undo.
def var s-stub3     as c format "x(14)"     no-undo.
def var v-stub3     as c format "x(8)"      no-undo.
def var s-stub4     as c format "x(13)"     no-undo.
def var v-stub4     as c format "x(10)"     no-undo.
def var s-stub5     as c format "x(14)"     no-undo.
def var v-stub5     as c format "x(14)"     no-undo.
def var s-stub6     as c format "x(15)"     no-undo.
def var s-stub7     as c format "x(11)"     no-undo.

/* si02 */
def var s-radd1     as c format "x(30)"     no-undo.
def var s-radd2     as c format "x(30)"     no-undo.
def var s-radd3     as c format "x(30)"     no-undo.
def var s-radd4     as c format "x(30)"     no-undo.


/*si04*/
def new shared var s-cantot    like oeeh.totinvamt     no-undo. 
def            var s-cantax1   as dec                  no-undo. /*si05*/
def            var s-cantax2   as dec                  no-undo. /*si05*/


/* TB# 11/05/97 */ 
def var s-frthandle as c format "x(20)"     no-undo.
def var s-lockboxkey as c format "x(25)"    no-undo. /* it01 */

/* IT#4 dbf */
def var kinx         as integer             no-undo.
def var ln-count     like oeel.lineno       no-undo.

/* das - lockbox by statecode */
def var x-combineaddons as logical          no-undo.
def var st          as i  format "99"       no-undo.
def var gotit       as logical              no-undo.
def var south-state as c  format "x(2)" extent 17 
    init ["AL","AR","AZ","CA","CO","FL","GA","LA","MS","NC","NM","NV", 
          "OK","SC","TN","TX","UT"]         no-undo.

def var all-states  as c  format "x(2)" extent 51
    init ["AL","AK","AZ","AR","CA","CO","CT","DE","DC","FL",
          "GA","HI","ID","IL","IN","IA","KS","KY","LA","ME",
          "MD","MA","MI","MN","MS","MO","MT","NE","NV","NH",
          "NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI",
          "SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"] no-undo.
def var w-div       like  arsc.divno        no-undo.
/* das - Tendering */
def var w-noinvcopy like  arsc.noinvcopy    no-undo.      

def var company-cust like arsc.salesterr    no-undo.
def var shippointfl  as   logical init no   no-undo.

define buffer b-sapbo for sapbo.
define buffer b-sapb  for sapb.
define buffer x-sasp  for sasp.

form
    with frame f-notes no-box no-labels col 8 width 80.

/* si01 - variables */
{lasersdi.lva new}
/* message "sapb.printernm" sapb.printernm. pause.   */

if sapb.printernm = "vid"
then v-tovid = yes.
assign
    v-tof     = v-esc + "&a0h15V"
    v-deftray = v-upper.
find x-sasp where x-sasp.printernm = sapb.printernm no-lock no-error.

/* smaf */
{sasc.gfi &lock = "no"
          &b    = "b-"}
          
/**************************** main logic *****************************/
hide message no-pause.

/*tb 8561 11/10/92 mms */
/* message "b-sasc" b-sasc.icdatclabel. pause.  */

if b-sasc.icdatclabel <> "" then do:
    do i = 1 to 4:
        if not can-do(" ,",substring(b-sasc.icdatclabel,i,1)) then leave.
    end.
    v-icdatclabel = (if i = 1 then b-sasc.icdatclabel
                     else if i = 2 then substring(b-sasc.icdatclabel,2,3)
                     else if i = 3 then substring(b-sasc.icdatclabel,3,2)
                     else substring(b-sasc.icdatclabel,4,1)) +
                    " Cost".
end.

/* TB# 11/05/97  */
assign s-frthandle = "Frt & Hndlg".
                       

v-headingfl = b-sasc.oeinvheadfl.

/* si01: invoice is always type 4 (4y part of code) */
v-rxform = v-esc + "&f4y4X".
if not v-tovid then put control
    v-deftray + v-rxform + /*v-lineterm +*/
    v-goth17 + v-noperf + v-6lpi /*+ v-tof*/.

/* SPLITTER */
/* das - split off the pabco file only during invoice processing (oeepi) */
if g-currproc = "oeepi" and sapb.reportnm begins "oeepinv"
   then 
  do:
  if v-outputty = "p" then do:
    for each sapbo use-index k-outputty where
             sapbo.cono     = g-cono         and
             sapbo.reportnm = sapb.reportnm  and
             sapbo.outputty = v-outputty 
             exclusive-lock:
      find oeeh use-index k-oeeh where oeeh.cono     = g-cono and
                                       oeeh.orderno  = sapbo.orderno and
                                       oeeh.ordersuf = sapbo.ordersuf
                                       no-lock no-error.
      if avail oeeh then 
        do:
        /* message "found oeeh". pause.  */
        {w-arsc.i oeeh.custno no-lock}
        if avail arsc then 
          do:
          /* WISTECH */
          if substr(arsc.user19,1,4) = "wist" then 
            do:
            assign sapbo.outputty = "y".
            next.
          end.
          /* FPT */
          if substr(arsc.user19,1,4) = "fpt" then 
            do:
            assign sapbo.outputty = "z".
            next.
          end.
          /* WARDEN */
          if substr(arsc.user19,1,4) = "ward" then
            do:
            assign sapbo.outputty = "w".
            next.
          end.
          /* HSI */
          if substr(arsc.user19,1,4) = "HSI" then
            do:
            assign sapbo.outputty = "h".
            next.
          end.
          /* FPE */
          if substr(arsc.user19,1,4) = "FPE" then
            do:
            assign sapbo.outputty = "e".
            next.
          end.
          if substr(arsc.user19,1,4) = "TOPS" then
            do:
            assign sapbo.outputty = "t".
            next.
          end.
          if substr(arsc.user19,1,4) = "FORD" then
            do:
            assign sapbo.outputty = "g".
            next.
          end.
          if substr(arsc.user19,1,4) = "FGA" then
            do:
            assign sapbo.outputty = "g".
            next.
          end.
          if substr(arsc.user19,1,4) = "WEST" then
            do:
            assign sapbo.outputty = "b".
            next.
          end.
          if substr(arsc.user19,1,4) = "WESH" then
            do:
            assign sapbo.outputty = "b".
            next.
          end.
          if substr(arsc.user19,1,4) = "PABC" then
            do:
            assign sapbo.outputty = "a".
            next.
          end.
          if substr(arsc.user19,1,4) = "PGON" then
            do:
            assign sapbo.outputty = "c".
            next.
          end.
          if substr(arsc.user19,1,4) = "DJI" then
            do:
            assign sapbo.outputty = "j".
            next.
          end.
          if substr(arsc.user19,1,4) = "PERF" then
            do:
            assign sapbo.outputty = "k".
            next.
          end.
        end. /* avail ARSC */
      end. /* avail OEEH */
    end. /* each SAPBO */
  end. /* v-outputty = "p" */
end. /* g-currproc = "oeepi" and nightly invoicing */
/* das - end splitter */


/* IT#4 dbf beg */
/*
message "after splitter". pause.
message "v-outputty" v-outputty. pause. 
*/
define buffer b4-sapbo for sapbo. 
assign v-outputty = "P".  

/*
for each b4-sapbo where b4-sapbo.cono = g-cono and
                        b4-sapbo.reportnm = "smaf" + sapb.reportnm
                        exclusive-lock:
                        
     message "avail sapbo"
            b4-sapbo.reportnm
            b4-sapbo.orderno
            b4-sapbo.outputty
            b4-sapbo.transproc.
end.            
*/

for each sapbo /* use-index k-outputty where
    sapbo.cono     = g-cono         and
    sapbo.reportnm = "smaf" + sapb.reportnm  and
    sapbo.outputty = v-outputty */ 
    where recid(sapbo) = ip-sapboid
exclusive-lock:
   
   ln-count = 3.
       
   for each oeel where 
       oeel.cono     = g-cono         and
       oeel.orderno  = sapbo.orderno  and
       oeel.ordersuf = sapbo.ordersuf
   no-lock:
           
      ln-count = ln-count + 2.
              
      if oeel.commentfl = true then do:
               
         for each com where 
             com.cono     = g-cono        and
             com.comtype  = "oe"          and
             com.orderno  = oeel.orderno  and
             com.ordersuf = oeel.ordersuf and
             com.lineno   = oeel.lineno   and
             com.printfl  = true
         no-lock:

            do kinx = 1 to 16 by 1:
   
               if com.noteln[kinx] <> "" then
      
                  ln-count = ln-count + 1.
        
            end.          /* do kinx 1 to 16 */
                    
         end.             /* for each com */

      end.                /* if oeel.commentfl = true */
              
   end.                   /* for each oeel */

   if ln-count > 26 then 
   
      sapbo.user6 = 2.
      
   else
   
      sapbo.user6 = 1.
      
end.                      /* for each sapbo */
/* IT#4 dbf end */

/*o ***** Read through each SAPBO record and print the invoice *********/
for each sapbo /* use-index k-outputty where
    sapbo.cono     = g-cono         and
    sapbo.reportnm = "smaf" + sapb.reportnm  and
    sapbo.outputty = v-outputty   */
    where recid(sapbo) = ip-sapboid

    no-lock
    by sapbo.cono by sapbo.reportnm
    by if can-do("z,r",p-sort) then sapbo.route
       /*tb 02/06/95 wpp TB# 14181 Expand sapbo.seqno format. */
       else if p-sort = "e" then string(sapbo.seqno,"zzzzzz9")
       else                      sapbo.reportnm
    by sapbo.orderno by sapbo.ordersuf:

    {w-oeeh.i sapbo.orderno sapbo.ordersuf no-lock}
     /* message "third sapbo". pause.   */
    /***** das - begin - future logic to print logos 
    if g-operinit = "rr11"  or   /* Marjie Bain      */
       g-operinit = "ar05"  or   /* Laura Troutman   */
       g-operinit = "ar01"  or   /* Laura Troutman   */
       g-operinit = "artm"  or   /* Laura Troutman   */
       g-operinit = "addw"  or   /* Dawn Wolff       */
       g-operinit = "ar08"  or   /* Kathy Baldwin    */
       g-operinit = "ar02"  or   /* Vicky Sinkule    */
       g-operinit = "ar04"  or   /* David Potter     */
       g-operinit = "ar06"  or   /* Joe Calhoun      */
       g-operinit = "das"   or
     /*g-operinit = "das1"  or
       g-operinit = "star"  or
       g-operinit = "sta1"  or
       g-operinit = "njew"  or
       g-operinit = "west"  or*/
       g-operinit = "tomh" then
      do:
      ****/
      if avail x-sasp and x-sasp.user5[10] = 0 then
        run zsdigetlogo.p (input "INV ",
                           input v-esc,
                           input recid(oeeh),
                           input no).
      else
        assign v-rxform = v-esc + "&f4y4X".
    /*end.*/
    
    if not v-tovid then put control
      v-deftray + v-rxform + /*v-lineterm +*/
      v-goth17 + v-noperf + v-6lpi /*+ v-tof*/.
    /** das - end *****/
    
    /* IT#4 dbf beg */
    /* as of 11/03/03, all but 0 invoices will print */
    /* das - Tendering */

   /* message "totinv" oeeh.totinvamt "tend" oeeh.tendamt  g-ourproc. pause.  */
    if oeeh.totinvamt = 0 and oeeh.tendamt = 0 and g-ourproc ne "oerd" then
      next.
    
    /* IT#4 dbf end */
    
    /* das - CAT Consumption Invoice (846) */
    if substring(oeeh.user2,1,3) = "846" and g-ourproc ne "oerd" then next.
       /*
       (g-operinit <> "flat" and
        g-operinit <> "as04" and
        g-operinit <> "smaf" and
        g-operinit <> "sdos") then next.
       */
        
    /* CAT -  No invoices if ASN - 862 */
    if substring(oeeh.user2,1,3) = "862" and g-ourproc ne "oerd" and
        oeeh.custno = 233912 then next.
        

    {w-arsc.i oeeh.custno no-lock}
    
    /*tb 4531 06/19/92 mkb; move checks to v-oeepix.i */
    {v-oeepix.i}
    if v-printokfl = no and oeeh.tendamt = 0 then next. /*das - Tendering*/
    
    /******************** NO LONGER USED ***************************
    if substr(arsc.user19,1,4) = "WARD" then
      assign s-lockboxkey = "lockbox address warden".
    else
    if substr(arsc.user19,1,4) = "WIST" then
      assign s-lockboxkey = "lockbox address wistech".
    else 
    if substr(arsc.user19,1,4) = "FPT" or
       substr(arsc.user19,1,4) = "FORD" or
       substr(arsc.user19,1,4) = "FGA" then
      assign s-lockboxkey = "lockbox address southern".
    else 
    if substr(arsc.user19,1,4) = "FPE" then
      assign s-lockboxkey = "lockbox address FPE".
    else
    if substr(arsc.user19,1,4) = "HSI" then
      assign s-lockboxkey = "lockbox address HSI".
    else
    if substr(arsc.user19,1,4) = "WEST" then
      assign s-lockboxkey = "lockbox address WEST".
    else
    if substr(arsc.user19,1,4) = "WESH" then
      assign s-lockboxkey = "lockbox address WEST".
    else
    if substr(arsc.user19,1,4) = "PGON" then
      assign s-lockboxkey = "lockbox address Paragon".
    else
    if substr(arsc.user19,1,4) = "PGON" and arsc.divno = 40 then
      if arsc.bankno = 40 then
        assign s-lockboxkey = "lockbox address PGON CAN".
      else
        assign s-lockboxkey = "lockbox address PGON USD".
    else
    if substr(arsc.user19,1,4) = "CALH" then
      assign s-lockboxkey = "lockbox address Callahan".
    else
    if substr(arsc.user19,1,4) = "DJI " and arsc.divno = 40 then    
      if arsc.bankno = 94 then
        assign s-lockboxkey = "lockbox address DJI CAD".
      else
        assign s-lockboxkey = "lockbox address DJI USD".
    else
    if substr(arsc.user19,1,4) = "PERF" then
      assign s-lockboxkey = "lockbox address Perfection".
    else
      do: /* not one of the above mentioned SS Companies */
      assign gotit = no.
      if arsc.divno = 30 then 
        do:
        do st = 1 to 17:
          if arsc.state = south-state[st] then 
            do:
            assign s-lockboxkey = "lockbox address southern".
            assign gotit = yes.
            leave.
          end. /* if arsc.state = south */
        end. /* 1 to 17 */
        if gotit = no then 
          do:
          if arsc.countrycd = "MX" and arsc.custno <> 1230003026 then
            assign s-lockboxkey = "lockbox address southern".
          else
            assign s-lockboxkey = "lockbox address northern".
        end. /* gotit = no */
      end. /* arsc.divno = 30 */
      else
        do:
        if arsc.divno = 40 then 
          do:
          if substring(oeeh.user5,1,1) = "C" then
            s-lockboxkey = "lockbox address canada CAD".
          else
            s-lockboxkey = "lockbox address canada USD".
        end. /* arsc.divno = 40 */
        else
          assign s-lockboxkey = "lockbox address northern".
      end. /* not division 30 */
    end. /* not one of the other SS Companies */    
    ***********************************************************/
    
    if arsc.bankno = 1 or arsc.bankno = 8 then
       assign s-lockboxkey = "lockbox address BofA".   
    else    
      do:   
      if arsc.bankno = 10 or arsc.bankno = 11 then
        assign s-lockboxkey = "lockbox address BofA CAD".
      else
      if arsc.bankno = 12 then
        assign s-lockboxkey = "lockbox address BofA USD".
      else
      if substr(arsc.user19,1,4) = "PGON" then
        if arsc.bankno = 40 then
          assign s-lockboxkey = "lockbox address PGON CAN".
        else
          assign s-lockboxkey = "lockbox address PGON USD".
      else
      if substr(arsc.user19,1,4) = "DJI " then
        if arsc.bankno = 94 then
          assign s-lockboxkey = "lockbox address DJI CAD".
        else
          assign s-lockboxkey = "lockbox address DJI USD".
      else
      if arsc.bankno = 91 or arsc.bankno = 94 or arsc.bankno = 40 then
        assign s-lockboxkey = "lockbox address canada CAD".
      else
        assign s-lockboxkey = "lockbox address canada USD".
    end. /* arsc.divno = 40 */

    find first notes use-index k-notes where
               notes.cono       = g-cono            and
               notes.notestype  = "zz"              and
               notes.primarykey = s-lockboxkey
               no-lock no-error.
    assign s-radd1 = if avail notes then notes.noteln[1] else ""
           s-radd2 = if avail notes then notes.noteln[2] else ""
           s-radd3 = if avail notes then notes.noteln[3] else ""
           s-radd4 = if avail notes then notes.noteln[4] else "". 
    /* end of it01 */
    
        /* beg it02 */
     find first oimsp where oimsp.person = arsc.creditmgr no-lock no-error.
     if avail(oimsp) then  
         assign s-phonehd = oimsp.phoneno
                s-faxhd = oimsp.faxphoneno.
        /* end of it02 */
        
    /*tb 4531 06/19/92 mkb; move checks to v-oeepixf.i ************************
    if not avail oeeh then next.                                            *
                                                                            *
    / *d Check the transaction type if the operator wants to print quotes   *
        only (from OERD) * /                                                *
    if g-ourproc = "oerd" and p-quote = "q" and oeeh.transtype <> "qu" and  *
        not p-selectfl then next.                                           *
                                                                            *
    / *d Do not print the invoice based on the order stage, minimum amount  *
        to print, and transaction type * /                                  *
    if g-ourproc = "oeepi"                                                  *
    / *tb 6756 06/13/92 mjm; added oerd if not list * /                     *
       or                                                                   *
       (g-ourproc = "oerd" and p-selectfl = no) then do:                    *
    / *tb 6756 06/18/92 mkb; do not check for minimum if ourproc = oerd * / *
        if g-ourproc ne "oerd"  and oeeh.stagecd < 4 then next.             *
        / *tb 6889 06/06/92 mwb; use tendamt instead of 3 payamts * /       *
        assign v-mininvamt = oeeh.totinvamt - oeeh.tendamt                  *
               v-mininvamt = if can-do("cr,cs,so",oeeh.transtype) and       *
                                 v-mininvamt < 0 then v-mininvamt * -1      *
                             else v-mininvamt.                              *
        if (v-mininvamt < b-sasc.oemninvamt) or                             *
           (oeeh.transtype = "cs" and v-mininvamt = 0) then next.           *
    end.                                                                    *
                                                                            *
    if not avail arsc then next.                                            *
    tb 4531 06/19/92 mkb; end ***********************************************/

    if oeeh.shipto ne "" then {w-arss.i oeeh.custno oeeh.shipto no-lock}

    /*d Print the # of invoices requested for customer */
    assign v-invtofl = if oeeh.shipto ne "" and avail arss then arss.invtofl
                       else false.
  
    
    /* das - Tendering */
    /*
    if oeeh.shipto ne "" and avail arss then
      assign w-noinvcopy = arss.noinvcopy.
    else
      assign w-noinvcopy = arsc.noinvcopy.
    */

    /* we want 4 copies of invoice for CN or IT */
      assign w-noinvcopy = 4. 
      

    /* das - Tendering we need to honor the noinvcopy 
    if w-noinvcopy = 0 and oeeh.tendamt > 0 then
      assign w-noinvcopy = 1.
    */  
    
     /* message "please here?". pause.   */
    
    /* 07/20/92 mkb; TB# 7191 Alphanumeric changes for custno
    do v-index = 1 to ((if      p-printto = "f" and oeeh.fpcustno <> 0 then 1 */
    /*
    do v-index = 1 to ((if      p-printto = "f" and
        oeeh.fpcustno <> {c-empty.i} then 1
                   /*tb 3639 12-20-91 kmw; Allow 1 copy from oerd */
                        else if p-honorfl = no then 1
                        else if oeeh.shipto ne "" and avail arss then
                            arss.noinvcopy
                        else arsc.noinvcopy)
                        /* 07/20/92 mkb; TB# 7191
                           Alphanumeric changes for custno
                        + if oeeh.fpcustno = 0 then 0 */
                        + if oeeh.fpcustno = {c-empty.i} then 0
                          else if can-do("f,o",p-printto) then 0
                          else 1):
       */
                          
      do v-index = 1 to 5:                    
                          
        /*o Main printing logic */
/*
         message "bp" v-index arsc.noinvcopy oeeh.fpcustno p-printto. pause.

*/

        {p-oeepi9.i}                        /* si01 */
        page.
    end.  /* of do loop to print multiple copies */

    /*tb 12932 09/07/93 rs; Trend Doc Mgr for Rel 5.2 */
    if v-coldfl and v-outputty = "p" then do for b-sapbo:
      find b-sapbo where recid(b-sapbo) = recid(sapbo)
                     exclusive-lock no-error.
      if avail b-sapbo then
         b-sapbo.outputty = "x".
    end.

end.



/* si01 - reset */
if not v-tovid then put control v-reset.

hide all no-pause.
hide message no-pause.
/* Copyright 1989,1990,1991,1992,1993,1994 by R & D Systems Company */
/* All Rights Reserved. */
