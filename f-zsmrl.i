/* f-smrl.i 1.1 2/16/92 */
/*si**** Custom ****************************************************************
  CLIENT  : 80281 S.D.I. Operating Partners
  JOB     : sdi083
  AUTHOR  : mat
  DATE    : 09/22/99
  VERSION : 8.0.003
  PURPOSE : SMRL - Add Ranges and New Service Level Calculation
  CHANGES : 
    SI01 09/22/99 mat; Initial Coding
    SI02 02/05/00 mm;  sdi08301; Add option to print order information
******************************************************************************/
/*  f-smrl.i - forms for smrl     */

/** 1/24/92 pap  TB#5272 - change Name to Lookup
                           change formatting of columns                     **/

{f-under.i}
{f-blank.i}

form /* f-smrl0 - title line for customer display */
    "---------- Invoiced Orders ----------"  at 31
    "------------------ Invoiced Lines -----------"  at 69    /* si01 */
    "On Time"                                        at 115   /* si01 */
with frame f-smrl0 no-box no-labels page-top width 132.

form /* f-smrl0w - title line for customer by warehouse display */
    "Whse:"                                  at 1
    s-whse                                   at 7
    "---------- Invoiced Orders ----------"  at 31
    "---------------- Invoiced Lines -------------"  at 69   /* si02 */
    "On Time"                                        at 115  /* si02 */
with frame f-smrl0w no-box no-labels page-top width 132.

form /* f-smrl1 - column heading line for customer display */
    "Customer #"        at 1
    "Lookup Name"       at 15
    "Number"            at 32
    "Total Value"       at 40
    "Avg Value"         at 57
    "Number"            at 70
    "Comp"              at 81
    "Incomp"            at 88
    "Late"              at 99
    "Late&Inc"          at 104
    "Shipping%"         at 114 /* si01 */
    "Serv Lev"          at 124
with frame f-smrl1 no-box no-labels page-top width 132.

form /* f-smrl2 - title line for warehouses  */
    "-------- Warehouse Transfers --------"  at 31
    "------------------ Transfer Lines -------------------"  at 69
    "Whse"              at 1
    "Name"              at 8
    "Number"            at 32
    "Total Value"       at 40
    "Avg Value"         at 57
    "Number"            at 70
    "Comp"              at 81
    "Incomp"            at 88
    "Late"              at 99
    "Late&Inc"          at 104
    "Sub/Upg"           at 115
    "Serv Lev"          at 124
with frame f-smrl2 no-box no-labels page-top width 132.

form /* f-smrl3 - detail line for customer print */
    s-custno            at 1
    s-lookupnm          at 15
    s-ordcnt            at 31
    s-totvalue          at 39
    s-avgvalue          at 54
    s-linecnt           at 69
    s-complete          at 78
    s-incomp            at 87
    s-late              at 96
    s-lateinc           at 105
    s-ontimepct         at 115 /* si01 */
    s-ontimefl          at 123 /* si01 */
    s-servlevel         at 124
    s-basefl            at 132
with frame f-smrl3 no-box no-labels down width 132.

form  /** f-smrl3a - extra line if requested & promised dates are used **/
    s-linecnt           at 69
    s-complete2         at 78
    s-incomp2           at 87
    s-late2             at 96
    s-lateinc2          at 105
    s-ontimepct2        at 115 /* si01 */
    s-ontimefl2         at 123 /* si01 */
    s-servlevel2        at 124
    s-basefl2           at 132
with frame f-smrl3a no-box no-labels down width 132.

form /* f-smrl4 - detail line for warehouse print */
    s-whse              at 1
    s-lookupnm          at 8
    s-ordcnt            at 31
    s-totvalue          at 39
    s-avgvalue          at 54
    s-linecnt           at 69
    s-complete          at 78
    s-incomp            at 87
    s-late              at 96
    s-lateinc           at 105
    s-subs              at 115
    s-servlevel         at 124
with frame f-smrl4 no-box no-labels down width 132.

form  /* f-smrl5 - total line for analysis  */
    "------- -------------- --------------"         at 31
    "-------  -------  -------"                     at 69
    "-------  -------   -------  --------"          at 96
    with frame f-smrl5 no-box no-labels width 132.

form   /** Qualification statement for totals when serv level is not 0 **/
    s-servmsg                           at 81
    with frame f-smrlq no-box no-labels width 132.

form  /** f-footer - legend line  **/
    skip(1)
    "Service Level:"                            at 5
    "(r) - Based Upon Requested Ship Date, "
    "(p) - Based Upon Promised Ship Date"
    with frame f-footer no-box no-labels width 132 page-bottom.

form /* f-smrl6t - totals for customer totals  */
    t-text              at 1
    t-cnt               at 21
    t-ordcnt            at 31
    t-totvalue          at 39
    s-avgvalue          at 54
    t-linecnt           at 69
    t-complete          at 78
    t-incomp            at 87
    t-late              at 96
    t-lateinc           at 105
    s-ontimepct         at 115 /* si01 */
    s-ontimefl          at 123 /* si01 */
    s-servlevel         at 124
    s-basefl            at 132
with frame f-smrl6t no-box no-labels down width 132.

form /* f-smrl6t2 - totals for 2nd line of customer totals  */
    t-linecnt           at 69
    t-complete2         at 78
    t-incomp2           at 87
    t-late2             at 96
    t-lateinc2          at 105
    s-ontimepct2        at 115 /* si01 */
    s-ontimefl2         at 123 /* si01 */
    s-servlevel2        at 124
    s-basefl2           at 132
with frame f-smrl6t2 no-box no-labels width 132.

form /* f-smrl6wt - totals for warehouse totals  */
    t-text              at 1
    t-cnt               at 21
    t-ordcnt            at 31
    t-totvalue          at 39
    s-avgvalue          at 54
    t-linecnt           at 69
    t-complete          at 78
    t-incomp            at 87
    t-late              at 96
    t-lateinc           at 105
    t-subs              at 115
    s-servlevel         at 124
with frame f-smrl6wt no-box no-labels down width 132.

form /* f-smrl6g - totals for grand totals  */
    t-text              at 1
    g-cnt               at 21
    g-ordcnt            at 31
    g-totvalue          at 39
    s-avgvalue          at 54
    g-linecnt           at 69
    g-complete          at 78
    g-incomp            at 87
    g-late              at 96
    g-lateinc           at 105
    s-ontimepct         at 115 /* si01 */
    s-ontimefl          at 123 /* si01 */
    s-servlevel         at 124
    s-basefl            at 132
with frame f-smrl6g no-box no-labels width 132.

form /* f-smrl6g2 - totals for 2nd line of customer and grand totals  */
    g-linecnt           at 69
    g-complete2         at 78
    g-incomp2           at 87
    g-late2             at 96
    g-lateinc2          at 105
    s-ontimepct2        at 115 /* si01 */
    s-ontimefl2         at 123 /* si01 */
    s-servlevel2        at 124
    s-basefl2           at 132
with frame f-smrl6g2 no-box no-labels width 132.

form /* f-smrl7 - title line if totals only for both cust and whse */
    "--------------- Orders --------------"  at 31
    "------------------- Line Items ----------------------"  at 69
    "Number"            at 32
    "Total Value"       at 40
    "Avg Value"         at 57
    "Number"            at 70
    "Comp"              at 81
    "Incomp"            at 88
    "Late"              at 99
    "Late&Inc"          at 104
    "Sub/Upg"           at 115
    "Serv Lev"          at 124
    "------------------------------"    at 31
    "------------------------------"    at 61       /**??**/
    "------------------------------"    at 91
    "-----------"                       at 121
with frame f-smrl7 no-box no-labels width 132.

form /* f-smrl8 - title line if totals only for cust */
    "---------- Invoiced Orders ----------"  at 31
    "------------------ Invoiced Lines -------------------"  at 69
    "Number"            at 32
    "Total Value"       at 40
    "Avg Value"         at 57
    "Number"            at 70
    "Comp"              at 81
    "Incomp"            at 88
    "Late"              at 99
    "Late&Inc"          at 104
    "Shipping%"         at 114 /* si01 */
    "Serv Lev"          at 124
    "------------------------------"    at 31
    "------------------------------"    at 61
    "------------------------------"    at 91       /** ??**/
    "------------"                      at 121
with frame f-smrl8 no-box no-labels width 132.

form /* f-smrl9 - title line if totals only for whse */
    "-------- Warehouse Transfers --------"  at 31
    "------------------ Transfer Lines -------------------"  at 69
    "Number"            at 32
    "Total Value"       at 40
    "Avg Value"         at 57
    "Number"            at 70
    "Comp"              at 81
    "Incomp"            at 88
    "Late"              at 99
    "Late&Inc"          at 104
    "Sub/Upg"           at 115
    "Serv Lev"          at 124
with frame f-smrl9 no-box no-labels width 132.

/*si02 begin - new frames for order detail */

form /* f-detailhd - title line for order details */
    "Order#"            at 7
    "Ty"                at 16
    "Request"           at 20
    "Promise"           at 30
    "Ship"              at 42
    "Ln"                at 50
    "Product"           at 54
    "Qty Ord"           at 80
    "Qty Shp"           at 90
    "Net Sales"         at 101
    "Margin$"           at 114
    "Mar%"              at 126
with frame f-detailhd no-box no-labels page-top width 132.

form /* f-detail - frame for order details */
    zx-lit1                                         at 4
    t-zzdetail.orderno                              at 5
    t-zzdetail.ordersuf                             at 13
    t-zzdetail.transtype                            at 16
    t-zzdetail.reqshipdt                            at 20
    t-zzdetail.promisedt                            at 30
    t-zzdetail.shipdt                               at 40
    t-zzdetail.lineno                               at 49
    t-zzdetail.shipprod                             at 54
    t-zzdetail.qtyord       format ">>>>9.99-"      at 79
    t-zzdetail.qtyship      format ">>>>9.99-"      at 89
    zx-lit2                                         at 98
    t-zzdetail.netamt       format ">>>>>>9.99-"    at 100
    t-zzdetail.marginamt    format ">>>>>>9.99-"    at 112
    t-zzdetail.marginpct    format ">>>9.9-"        at 124
with frame f-detail no-box no-labels width 132.

/*si02 end*/


/* SDI Stuff */
form
  "-----------Invoiced-----------"        at 31
  "-----------Complete-----------"        at 62
  "Late"            at 95
  "Late"            at 103
  "On Time"         at 111
  "On Time"         at 120
  with frame f-head1 no-box no-labels page-top width 132.

form
  "Customer #"      at 1
  "Lookup Name"     at 15
  "Orders"          at 32
  "Lines"           at 41
  "Total Value"     at 49
  "Orders"          at 63
  "Lines"           at 72
  "Total Value"     at 80
  "Orders"          at 94
  "Lines"           at 103
  "Order%"          at 112
  "Line%"           at 121
  with frame f-head2c no-box no-labels page-top width 132.


form
  "-----------Invoiced-----------"        at 31
  "-----------Complete-----------"        at 62
  "Late"            at 95
  "Late"            at 103
  "On Time"         at 111
  "On Time"         at 120
  with frame f-head1s no-box no-labels page-top width 132.

form
  "Orders"          at 32
  "Lines"           at 41
  "Total Value"     at 49
  "Orders"          at 63
  "Lines"           at 72
  "Total Value"     at 80
  "Orders"          at 94
  "Lines"           at 103
  "Order%"          at 112
  "Line%"           at 121
   "------------------------------"    at 31
   "------------------------------"    at 61
   "------------------------------"    at 91       /** ??**/
   "------------"                      at 121
  with frame f-head2cs no-box no-labels page-top width 132.





form
  "Whse"            at 1
  "Name"            at 8
  "Orders"          at 32
  "Total Value"     at 41
  "Lines"           at 56
  "Orders"          at 63
  "Total Value"     at 72
  "Lines"           at 87
  "Orders"          at 94
  "Lines"           at 103
  "Orders"          at 112
  "Lines"           at 121
  with frame f-head2w no-box no-labels page-top width 132.



form /* f-smrl3 - detail line for customer print */
    s-custno            at 1
    s-lookupnm          at 15
    s-ordcnt            at 31
    s-linecnt           at 39 
    s-totvalue          at 47
    s2-ordcnt           at 62
    s2-linecnt          at 70
    s2-totvalue         at 78
    s2-late             at 93
    s2-lateinc          at 101
    s-ontimepct         at 111 /* si01 */
    s-servlevel         at 120 format ">>9.99"
    s-basefl            at 126 
    f-whse              at 128 
with frame f-smrl3s no-box no-labels down width 132.

form  /** f-smrl3a - extra line if requested & promised dates are used **/
    s2-ordcnt            at 62
    s2-linecnt           at 70
    s2-late2             at 93
    s2-lateinc2          at 101
    s-ontimepct2        at 111 /* si01 */
    s-servlevel2        at 120 format ">>9.99"
    s-basefl2           at 126
with frame f-smrl3as no-box no-labels down width 132.

form /* f-smrl4 - detail line for warehouse print */
    s-whse               at 1
    s-lookupnm           at 8
    s-ordcnt             at 31
    s-linecnt            at 39
    s-totvalue           at 47
    s2-ordcnt            at 62
    s2-linecnt           at 70
    s2-totvalue          at 78
    s2-late              at 93
    s2-lateinc           at 101
    s-ontimepct          at 111
    s-servlevel         at 120 format ">>9.99"
    s-basefl            at 126
with frame f-smrl4s no-box no-labels down width 132.


form /* f-smrl6t - totals for customer totals  */
    t-text              at 1
    t2-cnt              at 21
    t-ordcnt            at 31
    t-linecnt           at 39
    t-totvalue          at 47
    t2-ordcnt           at 62
    t2-linecnt          at 70
    t2-totvalue         at 78
    t2-late             at 93
    t2-lateinc          at 101
    s-ontimepct         at 111
    s-servlevel         at 120  format ">>9.99"
    s-basefl            at 126
with frame f-smrl6ts no-box no-labels down width 132.

form /* f-smrl6t2 - totals for 2nd line of customer totals  */
    t2-ordcnt            at 62
    t2-linecnt           at 70
    t2-late2             at 93
    t2-lateinc2          at 101
    s-ontimepct2        at 111 
    s-servlevel2        at 120  format ">>9.99"
    s-basefl2           at 126
with frame f-smrl6t2s no-box no-labels width 132.

form /* f-smrl6wt - totals for warehouse totals  */
     t-text              at 1
    t2-cnt              at 21
    t-ordcnt            at 31
    t-totvalue          at 39
    t-linecnt           at 54
    t2-ordcnt           at 62
    t2-totvalue         at 70
    t2-linecnt          at 85
    t2-late             at 93
    t2-lateinc          at 101
    s-ontimepct         at 111
    s-servlevel         at 120
with frame f-smrl6wts no-box no-labels down width 132.

form /* f-smrl6g - totals for grand totals  */
    t-text              at 1
    g2-cnt              at 21
    g-ordcnt            at 31
    g-linecnt           at 39
    g-totvalue          at 47
    g2-ordcnt           at 62
    g2-linecnt          at 70
    g2-totvalue         at 78
    g2-late             at 93
    g2-lateinc          at 101
    s-ontimepct         at 111
    s-servlevel         at 120 format ">>9.99"
    s-basefl            at 126

with frame f-smrl6gs no-box no-labels width 132.

form /* f-smrl6g2 - totals for 2nd line of customer and grand totals  */
    g2-ordcnt            at 62
    g2-linecnt           at 70
    g2-late2             at 93
    g2-lateinc2          at 101
    s-ontimepct2        at 111 
    s-servlevel2        at 120  format ">>9.99"
    s-basefl2           at 126

with frame f-smrl6g2s no-box no-labels width 132.

