/*oeepehdr.lpr*/
/*h****************************************************************************
  INCLUDE      : oeepehdr.lpr
  DESCRIPTION  : Processing logic used by oeepe (Advanced Shipping Notice)
                 to prepare header variables for producing the following
                 documents.  It is used in both CHUI and GUI:
                 1) 856 - EDI Advanced Shipping Notice (ASN)
  USED ONCE?   : yes
  AUTHOR       : ajw
  DATE WRITTEN : 06/27/00
  CHANGES MADE :
    06/27/00 ajw; TB# e5636 Buildnet Enable; export ASN as an XML Document.
        NOTE:  We're not currently using it in the XML document, but this logic
        is packaged up so we could be positioned in the future to use it.
    05/21/01 rcl; TB# e6873 Increase ICSP weight and cube fields to 5 decimals.
******************************************************************************/
    /*NOTE:  The references to the EDIH and EDIL EDI files are only used for
             EDI Processing.  If this include is called for the XML format,
             this logic is commented out throughout this include file.
             Additionally, all places where the data is exported is also
             commented out.  We handle the exporting of the XML data in
             a different place in XML... This include is just used to prepare
             variables consistently between EDI and XML.*/

    /*d WRITE ASN RECORD */
    {&do-block1-com}
    if report.c20 begins "o" or first-of(report.c20) then do for arsc, arss:
    /{&do-block1-com}* */

        {&ar-finds-com}
        if {&send-to-type} = "s" then /* SEND TO SHIPTO ADDR */
            {w-arss.i oeeh.custno oeeh.shipto no-lock}
        else if {&send-to-type} = "g" then /* SEND TO GROUP ADDR */
            {w-arss.i oeeh.custno
                      {&shipto-field}
                      no-lock}
        else /* SEND TO CUSTOMER ADDR */
            {w-arsc.i oeeh.custno no-lock}
        /{&ar-finds-com}* */

        /*o Find the EDI Partner ID from ARSS/ARSC */
        assign
            s-partner        = if avail arss and arss.edipartner ne "" then
                                   string(arss.edipartner,"x(15)")
                               else if avail arsc and arsc.edipartner ne "" then
                                   string(arsc.edipartner,"x(15)")
                               else v-15spaces
            s-sendtonm       = if avail arss and arss.edipartner ne "" then
                                   string(arss.name,"x(30)")
                               else if avail arsc and arsc.edipartner ne "" then
                                   string(arsc.name,"x(30)")
                               else v-30spaces
            s-sendtoaddr1    = if avail arss and arss.edipartner ne "" then
                                   string(arss.addr[1],"x(30)")
                               else if avail arsc and arsc.edipartner ne "" then
                                   string(arsc.addr[1],"x(30)")
                               else v-30spaces
            s-sendtoaddr2    = if avail arss and arss.edipartner ne "" then
                                   string(arss.addr[2],"x(30)")
                               else if avail arsc and arsc.edipartner ne "" then
                                   string(arsc.addr[2],"x(30)")
                               else v-30spaces
            s-sendtocity     = if avail arss and arss.edipartner ne "" then
                                   string(arss.city,"x(20)")
                               else if avail arsc and arsc.edipartner ne "" then
                                   string(arsc.city,"x(20)")
                               else v-20spaces
            s-sendtost       = if avail arss and arss.edipartner ne "" then
                                   string(arss.state,"xx")
                               else if avail arsc and arsc.edipartner ne "" then
                                   string(arsc.state,"xx")
                               else v-2spaces
            s-sendtozipcd    = if avail arss and arss.edipartner ne "" then
                                   string(arss.zipcd,"x(10)")
                               else if avail arsc and arsc.edipartner ne "" then
                                   string(arsc.zipcd,"x(10)")
                               else v-10spaces
            s-sendtoph       = if avail arss and arss.edipartner ne "" then
                                   string(arss.phone,"(xxx)xxx-xxxx/xxxx")
                               else if avail arsc and arsc.edipartner ne "" then
                                   string(arsc.phone,"(xxx)xxx-xxxx/xxxx")
                               else v-18spaces.

        /* ADVANCED SHIPPING NOTICE RECORD */
        /*d User hook before ASN record write */
        {oeepe.z99 &asn_before = "*"}

        {&displaycom}
        put stream edi856 unformatted "ASN   " +
                        caps(s-partner) +
                        caps(s-sendtonm) +
                        caps(s-sendtoaddr1) +
                        caps(s-sendtoaddr2) +
                        caps(s-sendtocity) +
                        caps(s-sendtost) +
                        caps(s-sendtozipcd) +
                        caps(s-sendtoph).
        /{&displaycom}* */

        /*d User hook after ASN record write */
        {oeepe.z99 &asn_after = "*"}

        {&displaycom}
        /*d use the skip to finish this record */
        put stream edi856 unformatted skip.
        /{&displaycom}* */

        assign
            v-totorders = 0.

    {&do-block1-com}
    end. /* END PUT ASN RECORD */
    /{&do-block1-com}* */

    /*d Order Record */
    /*d Assign variables for the banner/header display */
    {w-sasta.i "s" oeeh.shipvia no-lock}
    assign
        v-linecnt        = 0
        v-oeehid         = recid(oeeh)

        s-cono           = string(oeeh.cono,"9999")
        s-transtype      = string(oeeh.transtype,"xx")
        s-orderno        = string(oeeh.orderno,"9999999")
        s-ordersuf       = string(oeeh.ordersuf,"99")
        s-whse           = string(oeeh.whse,"x(4)")
        s-shipdt         = if oeeh.shipdt <> ? then
                               string(oeeh.shipdt,"99/99/99")
                           else v-8spaces
        s-totweight      = string(oeeh.totweight,"zzzzzzzz9.99999-")
        s-totcubes       = string(oeeh.totcubes,"zzzzzzzz9.99999")
        s-nopackages     = string(oeeh.nopackages,"zzz9")
        s-pkgid          = string(oeeh.pkgid,"x(15)")
        s-pkglabel       = string(oeeh.pkglabel,"x(20)")
        s-shipvia        = string(oeeh.shipvia,"xxxx")
        s-shipviadesc    = if avail sasta then string(sasta.descrip,"x(12)")
                           else v-12spaces
        s-shipinstr      = string(oeeh.shipinstr,"x(30)")
        s-route          = string(oeeh.route,"xxx/xx/xx")
        s-custpo         = string(oeeh.custpo,"x(22)")
        s-refer          = string(oeeh.refer,"x(24)")
        s-promisedt      = if oeeh.promisedt <> ? then
                               string(oeeh.promisedt,"99/99/99")
                           else v-8spaces
        s-nolineitems    = string(oeeh.nolineitem,"zz9").

    {w-smsn.i oeeh.slsrepin no-lock}
    assign
        s-slsrepin       = string(oeeh.slsrepin,"xxxx")
        s-slsrepinnm     = if avail smsn then string(smsn.name,"x(30)")
                           else v-30spaces
        s-slsrepinph     = if avail smsn then
                               string(smsn.phoneno,"(xxx)xxx-xxxx/xxxx")
                           else v-18spaces.

    {w-smsn.i oeeh.slsrepout no-lock}
    assign
        s-slsrepout      = string(oeeh.slsrepout,"xxxx")
        s-slsrepoutnm    = if avail smsn then string(smsn.name,"x(30)")
                           else v-30spaces
        s-slsrepoutph    = if avail smsn then
                               string(smsn.phoneno,"(xxx)xxx-xxxx/xxxx")
                           else v-18spaces
        s-oeehuser1      = string(oeeh.user1,"x(78)")
        s-oeehuser2      = string(oeeh.user2,"x(78)")
        s-oeehuser3      = string(oeeh.user3,"x(78)")
        s-oeehuser4      = string(oeeh.user4,"x(78)")
        s-oeehuser5      = string(oeeh.user5,"x(78)")
        s-oeehuser6      = string(oeeh.user6,"zzzzzzzz9.99999-")
        s-oeehuser7      = string(oeeh.user7,"zzzzzzzz9.99999-")
        s-oeehuser8      = if oeeh.user8 <> ? then
                               string(oeeh.user8,"99/99/99")
                           else v-8spaces
        s-oeehuser9      = if oeeh.user9 <> ? then
                              string(oeeh.user9,"99/99/99")
                           else v-8spaces.
    {&edicom}
    assign
        s-oeehuser10   = if avail edih then
                            string(edih.user1,"x(80)")
                        else v-80spaces
        s-oeehuser11   = if avail edih then
                            string(edih.user2,"x(8)")
                         else v-8spaces
        s-oeehuser12   = if avail edih then
                            string(edih.user3,"x(8)")
                         else v-8spaces.
    /{&edicom}* */

    /* ADVANCED SHIPPING NOTICE HEADER RECORD */

    /*d User hook before HEADER record write */
    {oeepe.z99 &header_before = "*"}

    {&displaycom}
    put stream edi856 unformatted "Header" +
                    caps(s-cono) +
                    caps(s-transtype) +
                    caps(s-orderno) +
                    caps(s-ordersuf) +
                    caps(s-whse) +
                    caps(s-shipdt) +
                    caps(s-totweight) +
                    caps(s-totcubes) +
                    caps(s-nopackages) +
                    caps(s-pkgid) +
                    caps(s-pkglabel) +
                    caps(s-shipvia) +
                    caps(s-shipviadesc) +
                    caps(s-shipinstr) +
                    caps(s-route) +
                    caps(s-custpo) +
                    caps(s-refer) +
                    caps(s-promisedt) +
                    caps(s-nolineitems) +
                    caps(s-slsrepin) +
                    caps(s-slsrepinnm) +
                    caps(s-slsrepinph) +
                    caps(s-slsrepout) +
                    caps(s-slsrepoutnm) +
                    caps(s-slsrepoutph) +
                    caps(s-oeehuser1) +
                    caps(s-oeehuser2) +
                         v-43spaces   +
                    caps(s-oeehuser3) +
                    caps(s-oeehuser4) +
                    caps(s-oeehuser5) +
                    caps(s-oeehuser6) +
                    caps(s-oeehuser7) +
                    caps(s-oeehuser8) +
                    caps(s-oeehuser9) +
                    caps(s-oeehuser10) +
                    caps(s-oeehuser11) +
                    caps(s-oeehuser12).
    /{&displaycom}* */

    /*d User hook after HEADER record write */
    {oeepe.z99 &header_after = "*"}

    {&displaycom}
    /*d use the skip to finish this record */
    put stream edi856 unformatted skip.
    /{&displaycom}* */

    /*d Customer Record */
    /*d Assign variables for the customer display */
    {&ar-finds-com}
    do for arsc:
    {w-arsc.i oeeh.custno no-lock}
    /{&ar-finds-com}* */

    /*tb 21760 08/16/96 jkp; Add arse user fields. */
    assign
        s-custno        = string(oeeh.custno,"999999999999")
        s-name          = if avail arsc then string(arsc.name,"x(30)")
                          else v-30spaces
        s-addr1         = if avail arsc then string(arsc.addr[1],"x(30)")
                          else v-30spaces
        s-addr2         = if avail arsc then string(arsc.addr[2],"x(30)")
                          else v-30spaces
        s-city          = if avail arsc then string(arsc.city,"x(20)")
                          else v-20spaces
        s-state         = if avail arsc then string(arsc.state,"xx")
                          else v-2spaces
        s-zipcd         = if avail arsc then string(arsc.zipcd,"x(10)")
                          else v-10spaces
        s-arscuser1     = string(arsc.user1,"x(8)")
        s-arscuser2     = string(arsc.user2,"x(8)")
        s-arscuser3     = string(arsc.user3,"x(78)")
        s-arscuser4     = string(arsc.user4,"x(78)")
        s-arscuser5     = string(arsc.user5,"x(78)")
        s-arscuser6     = string(arsc.user6,"zzzzzzzz9.99999-")
        s-arscuser7     = string(arsc.user7,"zzzzzzzz9.99999-")
        s-arscuser8     = if arsc.user8 <> ? then
                              string(arsc.user8,"99/99/99")
                          else v-8spaces
        s-arscuser9     = if arsc.user9 <> ? then
                              string(arsc.user9,"99/99/99")
                          else v-8spaces
        s-arseuser1     = string(arsc.edinetwork,"x(7)")
        s-arseuser2     = string(arsc.edipartaddr,"x(15)")
        s-arseuser3     = string(arsc.ediyouraddr,"x(15)").

    {&ar-finds-com}
    end. /* do for arsc */
    /{&ar-finds-com}* */

    /* ADVANCED SHIPPING NOTICE CUSTOMER RECORD */

    /*d User hook before CUSTOM record write */
    {oeepe.z99 &custom_before = "*"}

    {&displaycom}
    /*tb 21760 08/16/96 jkp; Add output of arse user fields. */
    put stream edi856 unformatted "Custom" +
                    caps(s-custno) +
                    caps(s-name) +
                    caps(s-addr1) +
                    caps(s-addr2) +
                    caps(s-city) +
                    caps(s-state) +
                    caps(s-zipcd) +
                    caps(s-arscuser1) +
                    caps(s-arscuser2) +
                    caps(s-arscuser3) +
                    caps(s-arscuser4) +
                    caps(s-arscuser5) +
                    caps(s-arscuser6) +
                    caps(s-arscuser7) +
                    caps(s-arscuser8) +
                    caps(s-arscuser9) +
                    caps(s-arseuser1) +
                    caps(s-arseuser2) +
                    caps(s-arseuser3).
    /{&displaycom}* */

    /*d User hook after CUSTOM record write */
    {oeepe.z99 &custom_after = "*"}

    {&displaycom}
    /*d use the skip to finish this record */
    put stream edi856 unformatted skip.
    /{&displaycom}* */

    /*d Ship To Record */
    /*d Assign variables for the shipto display */

    assign
        s-shipto         = string(oeeh.shipto,"x(8)")
        s-shiptonm       = string(oeeh.shiptonm,"x(30)")
        s-shiptoaddr1    = string(oeeh.shiptoaddr[1],"x(30)")
        s-shiptoaddr2    = string(oeeh.shiptoaddr[2],"x(30)")
        s-shiptocity     = string(oeeh.shiptocity,"x(20)")
        s-shiptostate    = string(oeeh.shiptost,"xx")
        s-shiptozipcd    = string(oeeh.shiptozip,"x(10)").

    /* ADVANCED SHIPPING NOTICE SHIPTO RECORD */
    /*d User hook before SHIPTO record write */
    {oeepe.z99 &shipto_before = "*"}

    {&displaycom}
    put stream edi856 unformatted "Shipto" +
                    caps(s-shipto) +
                    caps(s-shiptonm) +
                    caps(s-shiptoaddr1) +
                    caps(s-shiptoaddr2) +
                    caps(s-shiptocity) +
                    caps(s-shiptostate) +
                    caps(s-shiptozipcd).
    /{&displaycom}* */

    /*d User hook after SHIPTO record write */
    {oeepe.z99 &shipto_after = "*"}

    {&displaycom}
    /*d use the skip to finish this record */
    put stream edi856 unformatted skip.
    /{&displaycom}* */