/***************************************************************************
 PROGRAM NAME: n-zicpplu.i
  PROJECT NO.: 00-44
  DESCRIPTION: This is an include for zicspplu.p new p/n lookup 
 DATE WRITTEN: 06/09/00
       AUTHOR: Sir Carl E. Graul Esq.
     MODIFIED: 

***************************************************************************/

/* ########################## TREND INCLUDES ######################### */


/* ########################### DEFINITIONS ########################### */


/* ############################## LOGIC ############################## */
if index(v-start, "*") <> 0 then 
  do:
    find {1} icsp use-index k-icsp where 
            icsp.cono = g-cono
        and icsp.prod matches v-start
        and icsp.statustype <> "i"
        no-lock no-error.
  end. /* if index(v-start, "*") <> 0 */
else
  do:
    find {1} icsp use-index k-icsp where
            icsp.cono = g-cono {2}
        and icsp.prod >= v-start
        /{2}* */
        and icsp.statustype <> "i"
        no-lock no-error.
  end. /* else if index(v-start, "*") <> 0 */
  
if available icsp then
  do:
    {w-icsw.i icsp.prod g-whse no-lock}
    if available icsw then
      do:
        {p-netavl.i "/*"}      
      end. /* if available icsw */
      assign
        s-descrip = icsp.descrip[1] + icsp.descrip[2]
        s-txt = (if can-do("i,s",icsp.statustype) then icsp.statustype
                else "") +
                (if not available icsw then "-" else "")
        s-qty = if icsp.statustype        = "l" then "  Labor"  
                else if icsp.kittype    = "b" then "    Kit"  
                else if not available icsw then    "    N/A"  
                else if s-netavail > 9999999 then  "+++++++"  
                else if s-netavail < 0       then  "      0"  
                else string(truncate(s-netavail,0),"zzzzzz9").
  end. /* if available icsp */
else
  assign s-txt = ""
         s-qty = ""
         s-descrip = v-invalid.
  

/* ############################ EOJ STUFF ############################ */


/* ######################### END OF PROGRAM ########################## */ 
/* ####################### INTERNAL PROCEDURES ####################### */


