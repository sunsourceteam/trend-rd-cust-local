/* SX 55
   f-oeetkd.i 1.1 01/03/98 */
/* f-oeetkd.i 1.6 10/27/93 */
/*h*****************************************************************************
  INCLUDE      : f-oeetkd.i
  DESCRIPTION  : F6 Detail and Add frames
  USED ONCE?   : no
  AUTHOR       : kmw
  DATE WRITTEN : 07/08/93
  CHANGES MADE :
    07/06/93 kmw; TB# 11684 oeetk.p oversize, moved frames
    07/26/93 mwb; TB# 12247 Changed price and cost display, since price label
       and price can be blank.
    09/17/93 kmw; TB# 11896 Ability to reserve kit comp - add qtyreservd,
        rearrange qtys on screen, display ordered and shipped instead of
        just shipped, display stocking unit
    05/22/95 ajw; TB# 12769 Added F6-Detail to OEIO Kit Component Screen.  Also
        removed dead code and old TB comments.
    05/30/95 ajw; TB# 18575 Nonstock/Special Kit Component Project (A1)
    12/13/95 kr ; TB# 19155 Correct Screen display
    12/13/95 mcd; TB# 19855 Special costing kit component display
        added s-speccostty
    12/28/95 ajw; TB# 20024 Progress V7 frame problem
        b-oeelk.shipprod changed colon to at
    1/10/96 gp;  TB# 20229 "Cost:" and special price type display.
        Changed b-oeelk.prodcost from "colon 50" back to "no-label",
                added format to right justify the field.  Add s-specpricety.
                Right justify the price field.
    06/22/00 maa; TB# e5061 Inventory Allocation Project
    09/22/03 ap;  TB# e10844 RBC Back Order Kit Component Processing.
            (roll to 3.0)
    09/23/03 ap:  TB# e11268 RBC-set BO allowed flag on comp add
    07/05/05 mwb; TB# L1685 Added wlpicktype for counter sale drop control.
    09/06/05 ejk; tb# L1789 rename oeelk.xxc4 to oeelk.wlpicktype
     ******************************************************************************/

/*tb 12769 05/22/95 ajw; Add F6-Detail to the OEIO Component Screen. Changed
    the icsp.notesfl and icsp.unitstock to use variables for display.*/
/*tb 18575 05/30/95 ajw; Nonstock/Special Kit Component Project.  Added the
  specnstype to the frame.  Also moved some fields right 2.  Moved the "Prod"
  label from the product to the specnstype.  Removed Validation of Product;
  and moved this to the Update-Editing Loop.  Also added Tie Fields.*/

form    /* Detail, Components */
        b-oeelk.specnstype      colon 12    label "Prod"
                   help
                   "(N)on-Stock, (S)pecial, or Blank"
        b-oeelk.shipprod        at    16 no-label
        s-notesfl               at    40 no-label
        s-descrip               at    16 no-label
        s-kitbotype             at    26 no-label
        s-comtype               colon 12    label "Comp Type"
        b-oeelk.qtyneeded       colon 50    label "Qty Needed"
                        format "zzzzzzzz9.99-"
        b-oeelk.unit                     no-label
        b-oeelk.groupoptname    colon 12    label "Grp/Opt/Key"
        s-totneeded             colon 50    label "Ordered"
            format "zzzzzzzz9.99-"
        s-unitstock                      no-label
        b-oeelk.refer           colon 12    label "Refer"
        s-totship               colon 50    label "Shipped"
            format "zzzzzzzz9.99-"
        b-oeelk.ovshipfl                 no-label
        b-oeelk.reqfl           colon 12    label "Required"
        b-oeelk.qtyreservd      colon 50    label "Reserved"
            format "zzzzzzzz9.99"
        b-oeelk.variablefl      colon 12    label "Variable"
         /*tb e5061 06/19/00 maa; Add reservation delay message to frame */
        s-delaymsg              at    52 no-label
        b-oeelk.subfl           colon 12    label "Subs"
        s-tietitle              at    22 no-label
        s-pdprodcost            at    46 no-label
        b-oeelk.prodcost                 no-label
            format "zzzzzz9.99999"
        s-speccostty            at    66 no-label
        b-oeelk.pricefl         colon 12    label "Price"
        s-tiedata               at    24 no-label
        s-pdprice               at    45 no-label
        b-oeelk.price                    no-label
            format "zzzzzz9.99999"
        s-specpricety           at    66 no-label
        b-oeelk.printfl         colon 12    label "Print"
        s-pdtitle               at    43 no-label
        b-oeelk.pdrecno                  no-label
        b-oeelk.compboty        colon 12    label "Back Order"
        help "Component Back Order - Blank Not Allowed, (B)ack Order Allowed"
                validate(can-do(",b",b-oeelk.compboty),
          "Component Back Order - Blank Not Allowed, (B)ack Order Allowed")
        lblWLPickType           at    19 no-label
        b-oeelk.wlpicktype      at    28 no-label
                format "x"
                help
                "C)ounter Zone, W)hse Pick, or Blank (Counter and Whse)"
with frame f-det1 row 5 col 5 width 72 overlay side-labels
title "Detail for " + string(b-oeelk.seqno).

form    /* Detail, Reference */
    s-comtype               colon 13    label "Comp Type"
    b-oeelk.instructions    colon 13    label "Reference"
    b-oeelk.printfl         colon 13    label "Print"
with frame f-det2 row 5 col 5 width 72 overlay side-labels
            title "Detail for " + string(b-oeelk.seqno).

/* SX 55 */                                                                     
/* #206890 form only used in kpea */                                 
form                                                                 
  b-oeelk.refer           colon 12    label "Refer"                
  s-totship               colon 50    label "Shipped"              
       format "zzzzzzzz9.99-"                                       
  b-oeelk.reqfl           colon 12    label "Required"             
  b-oeelk.qtyreservd      colon 50    label "Reserved"             
       format "zzzzzzzz9.99"                                        
  b-oeelk.variablefl      colon 12    label "Variable"             
  b-oeelk.subfl           colon 12    label "Subs"                 
  s-tietitle              at    22    no-label                     
  b-oeelk.pricefl         colon 12    label "Price"                
  s-tiedata                    at 24       no-label                
  z-technician            colon 50        label "Technician"       
  b-oeelk.printfl         colon 12    label "Print"                
  z-seqno                    colon 50         label "Prod Seq #"   
with frame zf-kpea row 11 col 6 width 70 overlay side-labels no-box. 
