/* zsapboyload

   Load custom sapbo records into temp-table.

*/
{&delcom}
display  with frame zygot1.
display  with frame zygot2.
/{&delcom}* */

for each sapbo where 
   sapbo.cono = g-cono and
   sapbo.reportnm = sapb.reportnm exclusive-lock:
  
    if sapbo.user1 = "v" then
    do:
    if zelection_matrix[7] < 2 then
       zelection_matrix[7] = if can-do("x,i",sapbo.user2) then 1 else 2.
    create zsapbo.
    assign zsapbo.selection_type = sapbo.user1
           zsapbo.selection_mode = sapbo.user2
           zsapbo.selection_char4 = ""
           zsapbo.selection_cust = sapbo.custno.

    if sapbo.user2 <> "x" then
     do:
     if zel_begvend > sapbo.custno or
        zel_begvend = 0 then
        zel_begvend = sapbo.custno.
   
     if zel_endvend < sapbo.custno or
        zel_endvend = zel_hivend then
        zel_endvend = sapbo.custno.
     end.
{&delcom}    
    display  zsapbo.selection_type 
             zsapbo.selection_mode 
             zsapbo.selection_cust
             with frame zygot3.  
    down with frame zygot3.

    if sapb.delfl = true then
      delete sapbo.
/{&delcom}* */
    end.

  else
  if sapbo.user1 = "c" then
    do:
    if zelection_matrix[2] < 2 then
       zelection_matrix[2] = if can-do("x,i",sapbo.user2) then 1 else 2.
    create zsapbo.
    assign zsapbo.selection_type = sapbo.user1
           zsapbo.selection_mode = sapbo.user2
           zsapbo.selection_char4 = ""
           zsapbo.selection_cust = sapbo.custno.

    if sapbo.user2 <> "x" then
     do:
     if zel_begcust > sapbo.custno or
        zel_begcust = 0 then
        zel_begcust = sapbo.custno.
   
     if zel_endcust < sapbo.custno or
        zel_endcust = zel_hicust then
        zel_endcust = sapbo.custno.
     end.
{&delcom}    
    display      zsapbo.selection_type 
                 zsapbo.selection_mode 
                 zsapbo.selection_cust 
                 with frame zygot3.
    down with frame zygot3. 

    if sapb.delfl = true then
      delete sapbo.
/{&delcom}* */

    end.
  else
    do:
    if sapbo.user1 = "s" then
       do:
       if zelection_matrix[1] < 2 then
         zelection_matrix[1] = if can-do("x,i",sapbo.user2) then 1 else 2.
       end.
    else   
    if sapbo.user1 = "p" then
      do:
      if zelection_matrix[3] < 2 then
        zelection_matrix[3] = if can-do("x,i",sapbo.user2) then 1 else 2.
      end.
    else
    if sapbo.user1 = "b" then
      do:
      if zelection_matrix[4] < 2 then
         zelection_matrix[4] = if can-do("x,i",sapbo.user2) then 1 else 2.
      end.
    else
    if sapbo.user1 = "t" then
      do:
      if zelection_matrix[5] < 2 then
         zelection_matrix[5] = if can-do("x,i",sapbo.user2) then 1 else 2.
      end.
    else
    if sapbo.user1 = "w" then
      do:
      if zelection_matrix[6] < 2 then
         zelection_matrix[6] = if can-do("x,i",sapbo.user2) then 1 else 2.
      end.
    else
    if sapbo.user1 = "n" then
      do:
      if zelection_matrix[8] < 2 then
         zelection_matrix[8] = if can-do("x,i",sapbo.user2) then 1 else 2.
      end.
    else
    if sapbo.user1 = "e" then
      do:
      if zelection_matrix[9] < 2 then
         zelection_matrix[9] = if can-do("x,i",sapbo.user2) then 1 else 2.
      end.
    else
    if sapbo.user1 = "G" then
      do:
      if zelection_matrix[10] < 2 then
         zelection_matrix[10] = if can-do("x,i",sapbo.user2) then 1 else 2.
      end.
    else
    if sapbo.user1 = "U" then
      do:
      if zelection_matrix[11] < 2 then
         zelection_matrix[11] = if can-do("x,i",sapbo.user2) then 1 else 2.
      end.

   else
    if sapbo.user1 = "h" then
      do:
      if zelection_matrix[12] < 2 then
         zelection_matrix[12] = if can-do("x,i",sapbo.user2) then 1 else 2.
      end.
   else
   if sapbo.user1 = "L" then
      do:
      if zelection_matrix[13] < 2 then
         zelection_matrix[13] = if can-do("x,i",sapbo.user2) then 1 else 2.
   end.
   else
   if sapbo.user1 = "K" then
      do:
      if zelection_matrix[14] < 2 then
         zelection_matrix[14] = if can-do("x,i",sapbo.user2) then 1 else 2.
   end.
   else
   if sapbo.user1 = "BT" then
      do:
      if zelection_matrix[15] < 2 then
         zelection_matrix[15] = if can-do("x,i",sapbo.user2) then 1 else 2.
   end.
   else
   if sapbo.user1 = "EX" then
      do:
      if zelection_matrix[16] < 2 then
         zelection_matrix[16] = if can-do("x,i",sapbo.user2) then 1 else 2.
   end.
   else
   if sapbo.user1 = "O" then
      do:
      if zelection_matrix[17] < 2 then
         zelection_matrix[17] = if can-do("x,i",sapbo.user2) then 1 else 2.
   end.
   else
   if sapbo.user1 = "j" then
       do:
       if zelection_matrix[18] < 2 then
         zelection_matrix[18] = if can-do("x,i",sapbo.user2) then 1 else 2.
       end.


    create zsapbo.
    assign zsapbo.selection_type = sapbo.user1
           zsapbo.selection_mode = sapbo.user2
           zsapbo.selection_char4 = sapbo.prodcat
           zsapbo.selection_cust = 0.
 
   if sapbo.user2 <> "x" then
     do:
     if sapbo.user1 = "s" then
       do:
       if zel_begrep > sapbo.prodcat or
          zel_begrep = "    " then
          zel_begrep = sapbo.prodcat.
   
       if zel_endrep < sapbo.prodcat or
          zel_endrep = "~~~~~~~~" then
          zel_endrep = sapbo.prodcat.
       end.
     else
     if sapbo.user1 = "p" then
       do:
       if zel_begcat > sapbo.prodcat or
          zel_begcat = "    " then
          zel_begcat = sapbo.prodcat.
   
       if zel_endcat < sapbo.prodcat or
          zel_endcat = "~~~~~~~~" then
          zel_endcat = sapbo.prodcat.
       end.
     else
     if sapbo.user1 = "u" then
       do:
       if zel_Prodbeg > sapbo.prodcat or
          zel_Prodbeg = " " then
          zel_Prodbeg = sapbo.prodcat.
   
       if zel_Prodend < sapbo.prodcat or
          zel_Prodend begins "~~~~~~~~" then
          zel_Prodend = sapbo.prodcat.
       end.
      
     else
     if sapbo.user1 = "b" then
       do:
       if zel_begbuy > sapbo.prodcat or
          zel_begbuy = "    " then
          zel_begbuy = sapbo.prodcat.
   
       if zel_endbuy < sapbo.prodcat or
          zel_endbuy = "~~~~~~~~" then
          zel_endbuy = sapbo.prodcat.
       end.
      else
     if sapbo.user1 = "t" then
       do:
       if zel_begtype > sapbo.prodcat or
          zel_begtype = "    " then
          zel_begtype = sapbo.prodcat.
   
       if zel_endtype < sapbo.prodcat or
          zel_endtype = "~~~~~~~~" then
          zel_endtype = sapbo.prodcat.
       end.
     else
     if sapbo.user1 = "w" then
       do:
       if zel_begwhse > sapbo.prodcat or
          zel_begwhse = "    " then
          zel_begwhse = sapbo.prodcat.
   
       if zel_endwhse < sapbo.prodcat or
          zel_endwhse = "~~~~~~~~" then
          zel_endwhse = sapbo.prodcat.
       end.
     else
     if sapbo.user1 = "n" then
       do:
       if zel_begvname > sapbo.prodcat or
          zel_begvname = "    " then
          zel_begvname = sapbo.prodcat.
   
       if zel_endvname < sapbo.prodcat or
          zel_endvname begins "~~~~~~~~" then
          zel_endvname = sapbo.prodcat.
       end.
     else
     if sapbo.user1 = "e" then
       do:
       if zel_begvpid > sapbo.prodcat or
          zel_begvpid = "    " then
          zel_begvpid = sapbo.prodcat.
   
       if zel_endvpid < sapbo.prodcat or
          zel_endvpid begins "~~~~~~~~" then
          zel_endvpid = sapbo.prodcat.
       end.
     else
     if sapbo.user1 = "G" then
       do:
       if zel_techbeg > substring(sapbo.prodcat,1,4) or
          zel_techend = "    " then
          zel_techbeg = substring(sapbo.prodcat,1,4).
   
       if zel_techend < substring(sapbo.prodcat,1,4) or
          zel_techend begins "~~~~~~~~" then
          zel_techend = substring(sapbo.prodcat,1,4).
       end.
         
     else
     if sapbo.user1 = "h" then
       do:
       if zel_begshipto > substring(sapbo.prodcat,1,8) or
          zel_endshipto = "    " then
          zel_begshipto = substring(sapbo.prodcat,1,8).
   
       if zel_endshipto < substring(sapbo.prodcat,1,8) or
          zel_endshipto begins "~~~~~~~~" then
          zel_endshipto = substring(sapbo.prodcat,1,8).
       end.
    
     else
     if sapbo.user1 = "L" then
       do:
       if zel_begLbus > substring(sapbo.prodcat,1,2) or
          zel_endLbus = "  " then
          zel_beglbus = substring(sapbo.prodcat,1,2).
   
       if zel_endlbus < substring(sapbo.prodcat,1,2) or
          zel_endlbus begins "~~~~" then
          zel_endlbus = substring(sapbo.prodcat,1,2).
       end.
          
     else
     if sapbo.user1 = "k" then
       do:
       if zel_begtakenby > substring(sapbo.prodcat,1,4) or
          zel_endtakenby = "    " then
          zel_begtakenby = substring(sapbo.prodcat,1,4).
   
       if zel_endtakenby < substring(sapbo.prodcat,1,4) or
          zel_endtakenby begins "~~~~~~~~" then
          zel_endtakenby = substring(sapbo.prodcat,1,4).
       end.
          
     else
     if sapbo.user1 = "bt" then
       do:

     if zel_begbatch > sapbo.custno or
        zel_begbatch = 0 then
        zel_begbatch = sapbo.custno.
   
    /* zel_endbatch = zel_hicust. */
    /*
    if zel_endbatch = zel_hicust then
      message "EQUAL" zel_endbatch zel_hicust.
    else
      message "NOT=" zel_endbatch zel_hicust.
    pause.
    */

     if zel_endbatch < sapbo.custno or
        zel_endbatch = zel_hicust then
        zel_endbatch = sapbo.custno.
     end.
     else
     if sapbo.user1 = "ex" then
       do:
       if zel_begexcept > substring(sapbo.prodcat,1,4) or
          zel_endexcept = "    " then
          zel_begexcept = substring(sapbo.prodcat,1,4).
   
       if zel_endexcept < substring(sapbo.prodcat,1,4) or
          zel_endexcept begins "~~~~~~~~" then
          zel_endexcept = substring(sapbo.prodcat,1,4).
       end.
     
     end.
     else
     if sapbo.user1 = "o" then
       do:

       if zel_begbatch > sapbo.custno or
          zel_begbatch = 0 then
          zel_begbatch = sapbo.custno.
   
       if zel_endbatch < sapbo.custno or
          zel_endbatch = zel_hicust then
          zel_endbatch = sapbo.custno.
       end.
     else
     if sapbo.user1 = "j" then
       do:
       if zel_Ibegrep > sapbo.prodcat or
          zel_Ibegrep = "    " then
          zel_Ibegrep = sapbo.prodcat.
   
       if zel_Iendrep < sapbo.prodcat or
          zel_Iendrep = "~~~~~~~~" then
          zel_Iendrep = sapbo.prodcat.
       end.
  


 {&delcom}
    if zsapbo.selection_char4 ne "" then do:
    display "Type - " at 1
               zsapbo.selection_type at 9 format "x(2)"
             "Mode - " at 12
               zsapbo.selection_mode at 19
             "Selection - "   at 21
               zsapbo.selection_char4 at 33 format "x(25)"
             with frame zygot4
             no-box no-underline no-labels .
    down with frame zygot4.             
    end.

    if sapb.delfl = true then
      delete sapbo.
/{&delcom}* */
    end.
end.