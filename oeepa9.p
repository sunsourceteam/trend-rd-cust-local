/* oeepa9.p */
/*****************************************************************************
  VERSION 5.5    : oeepa Acknowledgement print - SDI specific format(9) - 
                 : calls landscape(oeepa6a.p) and protrait(oeepa9a.p)
  PROCEDURE      : oeepa9.p
  DESCRIPTION    : order entry order acknowledgement format 1 - NEMA
  AUTHOR         : mwb
  DATE WRITTEN   : 03/05/90
  CHANGES MADE   :
    08/05/92 pap; TB# 7245 - Remove qty backordered & qty shipped from order
        acknowledgement
    11/11/92 mms; TB# 8561 Display product surcharge label from ICAO
    09/07/93 rs;  TB# 12932 Trend Doc Mgr for Rel 5.2
    03/30/94 rs;  TB# 15240 Visifax not printing report headings
    02/06/95 wpp; TB# 14181 Expand sapbo.seqno format.
    03/26/96 kr;  TB# 12406 Variables defined in form, moved to calling program
    09/13/96 kjb; TB# 21702 Trend 8.0 oper2 enhancement - removed references
        to oper2 in the code
    09/08/98 jl;  TB# 25144 Add the interface to Vsifax Gold. Added vfx
    09/01/98 cm;  TB# 19427 Add multi language capability.
        Split into oeepa1a.p.
    05/15/99 jl;  TB# 26204 Added NxTFax script logic
    03/29/00 rgm; TB# 3-2  Add RxServer Interface to Standard
    02/09/01 jd1; TB# e7852 Add Multicurrency Text
    03/18/04 rgm; TB# e11709 Adjust scoping of SAPBO
    10/14/05 ds;  TB# e23308 Fixed problem with demand print on zero dollor
                  counter sales
    06/10/09 das; NonReturn/NonCancelable
******************************************************************************/

{p-rptbeg.i}

/** SX 55  ***************************************************
*** SX 55  *****************************************************************/

def            var v-index         as i                                no-undo.
def            var v-line          as i initial 46                     no-undo.
def            var v-pageno        as i                                no-undo.
def            var v-type          as c format "x(1)" initial "o"      no-undo.
def            var v-qtybo         as de                               no-undo.
def            var s-lit41a        as c format "x(18)"                 no-undo.
def            var s-lit42a        as c format "x(18)"                 no-undo.
def            var s-lit43a        as c format "x(18)"                 no-undo.
def            var s-lit44a        as c format "x(18)"                 no-undo.
def            var s-lit45a        as c format "x(18)"                 no-undo.
def            var s-lit46a        as c format "x(18)"                 no-undo.
def            var s-lit46b        as c format "x(18)"                 no-undo.
def            var s-lit47a        as c format "x(18)"                 no-undo.
def            var s-lit47b        as c format "x(18)"                 no-undo.
def            var s-lit49a        as c format "x(18)"                 no-undo.
def            var s-lit50a        as c format "x(18)"                 no-undo.
def            var s-lit53a        as c format "x(18)"                 no-undo.
def            var s-prod          as c format "x(24)"                 no-undo.
def            var s-addondesc     as c format "x(18)" extent 4        no-undo.
def            var s-taxes         as de format "zzzzzzzz9.99-"        no-undo.
def            var s-dwnpmtamt     as de format "zzzzzzzz9.99-"        no-undo.
def            var s-shiptoaddr    like oeeh.shiptoaddr                no-undo.
def            var v-linecnt       as i                                no-undo.
/*
def            var v-minordamt     like oeeh.totinvamt                 no-undo.
*/
def            var v-subnetamt     like oeeh.totinvamt                 no-undo.
def            var v-printfl       as logical                          no-undo.
/*
def            var v-headingfl     as logical                          no-undo.
def     shared var p-sort          as c format "x(1)"                  no-undo.
*/
def     shared var p-promofl       as logical initial no               no-undo.
/*
def     shared var p-minorder      as decimal initial 0                no-undo.
def     shared buffer b-sasc       for sasc.
*/
def            buffer v-oeel       for oeel.
def            buffer v-oeeh       for oeeh.
def            buffer b-oeel       for oeel.
def            var s-descrip       as c format "x(49)" initial ""      no-undo.
def            var s-price         as c format "x(13)" initial ""      no-undo.
def            var s-netamt        as c format "x(13)" initial ""      no-undo.
def            var v-totqtyshp     like oeeh.totqtyshp                 no-undo.
def            var v-reqtitle      as c format "x(17)" initial ""      no-undo.
def            var v-netord        like oeel.netord    initial 0       no-undo.
def            var v-qtyord        like oeel.qtyord    initial 0       no-undo.
def            var v-totlineamt    like oeeh.totlineamt initial 0      no-undo.
def            var v-invtofl       as logical                          no-undo.
def            var s-restockamt    like oeel.restockamt                no-undo.
/*
def            var v-icdatclabel   as c format "x(9)"                  no-undo.
*/
def            var s-upcpno        as c format "x(5)"                  no-undo.

/*tb 12406 03/20/96 kr; Variables defined in form */
def            var s-lineno        as c format "x(3)"                  no-undo.
def            var s-qtyord        as c format "x(11)"                 no-undo.
def            var s-qtybo         as c format "x(11)"                 no-undo.
def            var s-qtyship       as c format "x(11)"                 no-undo.
def            var s-prcunit       as c format "x(4)"                  no-undo.
def            var s-discpct       as c format "x(14)"                 no-undo.
def            var s-sign          as c format "x"                     no-undo.
def            var v-subnetdisp    as c format "x(13)"                 no-undo.
/*das - NonReturn */
def            var s-lasterisk     as c format "x(2)"                  no-undo.

/*si02*/
def new shared var s-cantot    like oeeh.totinvamt     no-undo. 
def            var s-cantax1   as dec                  no-undo. 
def            var s-cantax2   as dec                  no-undo. 

/*
def buffer b-sapbo for sapbo.
*/

/* smaf - BL info  */
def buffer q-oeel for oeel.

form
 s-message1 as char format "x(56)"  at 20
 s-message2 as char format "x(50)"  at 76
 skip(1)
with frame f-messages no-box no-labels width 132 no-underline.


form
 s-title1   as char format "x(10)"   at 13 label "Order#"
 s-titles2   as char format "x(24)"   at 30 label "Product"
 s-title3   as char format "x(8)"    at 60 label "Quantity"
 s-title4   as char format "x(20)"   at 81 label "Release Date"
with frame f-titles no-box no-labels width 132 no-underline.


form 
 s-reloeno   as char format "x(10)"    at 13  label "Order#"
 s-relprod   as char format "x(24)"    at 30  label "Product"
 s-relqty    as i format "zzzzz9"      at 60  label "Qunatity."
 s-reldate   as date                   at 81  label "Release Date"
with frame f-reldetail no-box no-labels width 132 no-underline.

/* smaf - BL info */ 
 

/* SX55 */
def buffer x-sasp  for sasp.
find x-sasp where x-sasp.printernm = sapb.printernm no-lock no-error.

/* SX55 - done in the oeepa9a.p or oeepa6a.p 
def buffer x-sasp  for sasp.
{laserstd.lva "new"}
find x-sasp where x-sasp.printernm = sapb.printernm no-lock no-error.
if avail x-sasp and x-sasp.user3[1] <> 000 then do:                      
 run oeepa6.p.
 return.
end.
*/

/*** SX 55  *****************************************************************/

/*tb 19427 08/31/98 cm; Add multi language capability */
def new shared var v-headingfl     as logical                          no-undo.
def new shared var v-icdatclabel   as c format "x(9)"                  no-undo.
def     shared var v-coldfl        like sassr.coldfl                   no-undo.
def     shared var p-sort          as c format "x(1)"                  no-undo.
def     shared var p-minorder      as decimal initial 0                no-undo.
def            var v-minordamt     like oeeh.totinvamt                 no-undo.
def            var o-language      as char format "x(8)"               no-undo.
def     shared var v-ourproc       as c                                no-undo.

/*tb e7852 02/09/01; Multicurrency. Added new variable v-currency. */
def new shared var v-currency      like sastc.descrip                  no-undo.

{multlang.gva}

/*tb 19427 09/01/98 cm; Add multi language capability */
/*d Define shared buffers for use in split program */
def new shared buffer oeeh         for oeeh.
def new shared buffer arsc         for arsc.
def new shared buffer sapbo        for sapbo.
def     shared buffer b-sasc       for sasc.
def            buffer b-sapbo      for sapbo.

/****** main logic ******/
hide message no-pause.

if b-sasc.icdatclabel <> "" then
do:

    do i = 1 to 4:

        if not can-do(" ,",substring(b-sasc.icdatclabel,i,1)) then leave.

    end. /* end of do i - 1 to 4 */

    v-icdatclabel = (if i = 1 then b-sasc.icdatclabel
                     else if i = 2 then substring(b-sasc.icdatclabel,2,3)
                     else if i = 3 then substring(b-sasc.icdatclabel,3,2)
                     else substring(b-sasc.icdatclabel,4,1)) +
                    " Cost".

end. /* end of if b-sasc.icdatclabel <> "" */


/*tb 15240 03/30/94 rs; Visifax not printing report headings */
if v-saspid <> 0 then
    find sasp where recid(sasp) = v-saspid no-lock no-error.

/*tb 19427 09/01/98 cm; Add multi language capability */
/*tb 26204 05/15/99 jl; added nxtfax.lcn */
assign
    /* SX 55 */
    /*
    v-headingfl = if v-saspid <> 0 and avail sasp and
                  ({nxtfax.lcn  &file  = "sasp."} or
                   {vsifax.lcn  &file  = "sasp."})
                  then yes
    */
    v-headingfl = if v-saspid <> 0 and avail sasp and
                 sasp.pcommand begins "fx"
              then yes
                  else b-sasc.oeackheadfl
    o-language  = current-language.

/****** print the invoice ******/
/*tb 21702 09/13/96 kjb; Remove references to SAPBO, SAPB oper2 in the code */
for each sapbo use-index k-outputty where
         sapbo.cono     = g-cono        and
         sapbo.reportnm = sapb.reportnm and
         sapbo.outputty = v-outputty
no-lock
    by sapbo.cono
    by sapbo.reportnm
    by (if p-sort = "z"
        then sapbo.route
        else
            if p-sort = "e"
            /*tb 02/06/95 wpp; TB# 14181 Expand sapbo.seqno format. */
            then string(sapbo.seqno,"zzzzzz9")
            else sapbo.reportnm)
    by sapbo.orderno
    by sapbo.ordersuf:

    {w-oeeh.i sapbo.orderno sapbo.ordersuf no-lock}
    if not avail oeeh then next.

    if v-ourproc = "oeepa" then do:
     /*e If this order has more returned than sold, ignore the negative
         when comparing whether it is greater than the minimum order amount
         specified in the options of the report */
     assign v-minordamt = oeeh.totinvamt
            v-minordamt = if can-do("cs,so,cr",oeeh.transtype)  and
                           v-minordamt < 0 then 
                           v-minordamt * -1
                          else v-minordamt.

     if (v-minordamt < p-minorder) and p-minorder <> 0
      then next.

     /*e If this is a counter sale and the total invoice amount is zero,
         don't print                                                     */
     if oeeh.transtype = "cs" and v-minordamt = 0
      then next.

    end. /* end of if g-ourproc = oeepa */

    {w-arsc.i oeeh.custno no-lock}
    if not avail arsc then next.

    /*tb 19427 09/01/98 cm; Add multi language capability */
    /*d Set language to the customers default */
    if g-multlangfl = yes then do for sasta:
        if arsc.langcd <> "" then
            {sasta.gfi "Y" arsc.langcd "no"}
        else
            {sasta.gfi "Y" b-sasc.langcd "no"}

        current-language = if not avail sasta or
                               avail sasta and sasta.trmgrlang = "" then "?"
                           else sasta.trmgrlang.
    end.

    /*tb e7852 02/09/01 jd1; setup currency text */
    if arsc.currencyty <> "" then do: 
     {w-sastc.i arsc.currencyty no-lock}
     if avail sastc then v-currency = sastc.descrip.
     else v-currency = "".
    end.
    else do:
     v-currency = "".
    end.

    /*tb 19427 09/01/98 cm; Add multi language capability */
    /*d The call to a new program is necessary to allow Progress to use the
        proper text segment for the customers language. */
    /*tb 3-2 03/29/00 rgm; Rxserver interface call when appropriate */
    &IF DEFINED(rxserv_format) = 0 &THEN do:
     if avail x-sasp and x-sasp.user3[1] <> 000 then  
      run oeepa6a.p(no).
     else
      run oeepa9a.p(no).
    end.
    &ELSE
        run oeepa{&rxserv_format}a.p(no).
    &ENDIF

    /* SX 55 */
    if oeeh.shipto <> "" then
     {w-arss.i oeeh.custno oeeh.shipto no-lock}.

    v-invtofl = if oeeh.shipto <> "" and avail arss
                then arss.invtofl
                else false.

    /* print logic */
    /* SX55
    {p-oeepa9.i}
    page.
    */

    if v-coldfl and v-outputty = "p" then do for b-sapbo transaction:

     find b-sapbo where recid(b-sapbo) = recid(sapbo)
         exclusive-lock no-error.
     if avail b-sapbo then b-sapbo.outputty = "x".

    end. /* end of if v-coldfl and .... */

end. /* end of for each sapbo */

/*tb 19427 09/01/98 cm; Add multi language capability */
/*d Set language back to the operators setup */
current-language = o-language.

/* smaf -  Print BL information */

procedure print_bl:
if oeeh.stagecd <= 3 and oeel.qtyord >= 1 and oeeh.ordersuf = 0 then do:

assign s-message1 = "*** Blanket Releases Exist for the following Quantities"          s-message2 = "and release dates ***".
                      
    display
    s-message1
    s-message2
    with frame f-messages.
    down with frame f-messages.

    display 
    s-title1
    s-titles2
    s-title3
    s-title3
    with frame f-titles.
/*    down with frame f-titles. */


    for each q-oeel where q-oeel.cono = oeel.cono and
                        q-oeel.orderno = oeel.orderno and
                    /*    q-oeel.ordersuf =  oeel.ordersuf and  */
                        q-oeel.lineno = oeel.lineno and
                        q-oeel.shipprod = oeel.shipprod and 
                        q-oeel.qtyord >= 1 /*
                        q-oeel.transtype = "br"            */
                        no-lock by q-oeel.user3 with frame f-reldetail:
                        

   find v-oeeh where v-oeeh.cono = q-oeel.cono and
                     v-oeeh.orderno = q-oeel.orderno and
                     v-oeeh.ordersuf = q-oeel.ordersuf and  
                     v-oeeh.stagecd < 3
                     no-lock no-error.

  if avail v-oeeh then do: 

    if q-oeel.transtype = "bl" then do:  
    
       for each notes where notes.cono = 1 and                                                              notes.notestype = "zz" and                                                      notes.primarykey = "JIT-BL" +     
                           string(q-oeel.orderno, "9999999") +   
                           string(q-oeel.ordersuf, "99") + "#" 
                           + string(q-oeel.lineno, "999") and
                           notes.user2 = "F"  
                           no-lock by date(notes.secondarykey):                                              
            assign s-reloeno =  string(q-oeel.orderno, "z999999")  + "-" + 
                                string(q-oeel.ordersuf, "99") 
                                s-relprod =  q-oeel.shipprod
                   s-relqty  =  dec(notes.user6)
                   s-reldate =  date(notes.secondarykey).

                display 
                s-reloeno
                s-relprod
                s-relqty
                s-reldate
              with frame f-reldetail.
              down with frame f-reldetail.
                   
       end. /* for each */
    end. /* if oeel */
    else do:
            assign s-reloeno =  string(q-oeel.orderno, "z999999")  + "-" +     
                                string(q-oeel.ordersuf, "99")
                                s-relprod =  q-oeel.shipprod
                   s-relqty  =  dec(q-oeel.qtyord)
                   s-reldate =  date(substring(q-oeel.user3,1,6) + 
                                substring(q-oeel.user3,9,2)).
                          
               display 
                s-reloeno
                s-relprod
                s-relqty
                s-reldate
              with frame f-reldetail.
              down with frame f-reldetail.
                 
    end. /* else do */
   end.  /* for each oeel */            
   end.  /* for each v-ooeh */
end. /* >3 and >1 */
 end.  /* procedure */                      
