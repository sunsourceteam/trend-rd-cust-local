def     shared var v-quoteno     as i  format ">>>>>>>9"     no-undo.
def     shared var v-lineno      like  oeelb.lineno          no-undo.
def     shared var v-custno      like  arsc.custno           no-undo.
def     shared var v-shipto      like  arss.shipto           no-undo.
def     shared var v-whse        like  icsw.whse             no-undo.
def     shared var v-prod        like  icsw.prod             no-undo.
def     shared var v-qty         like  oeel.qtyord           no-undo.
def     shared var x-prodtype    as c  format "x(4)"         no-undo.
def     shared var v-prodcost    like  icsw.replcost         no-undo.
def     shared var v-specnstype  like  oeel.specnstype       no-undo.
def     shared var v-leadtime    like  icsw.leadtmavg        no-undo.
def     shared var v-sparebamt   like  icsw.stndcost         no-undo.
def     shared var v-pdtype      like  pdsc.levelcd          no-undo.
def     shared var v-pdrecord    like  pdsc.pdrecno          no-undo.
def     shared var v-totnet      as de format ">>>>>>9.99-"  no-undo.
def     shared var v-rebatefl    as c  format "x(1)"         no-undo.

def     shared var v-slsrepout   like  arsc.slsrepout        no-undo.
def     shared var v-slsrepin    like arsc.slsrepin          no-undo.
def     shared var v-totcost     as de format ">>>>>>9.99"   no-undo.
def     shared var v-rarrnotesfl as logical                  no-undo.
def     shared var v-prodcat     like  icsp.prodcat          no-undo.
def     shared var v-stndcost    like  icsw.stndcost         no-undo.
def     shared var v-stndcostdt  like  icsw.stndcostdt       no-undo.
def     shared var v-vendno      like  oeel.vendno           no-undo.
def     shared var v-totcomm     as de format ">>>>>>9.99"   no-undo.
def     shared var v-Hcommpct    as de format ">>9.9"        no-undo.
def            var x-recid       as recid                    no-undo.
def            var s-commpct     as de format ">>9.99"       no-undo.
/*
def     shared var v-costoverfl  like oeel.costoverfl        no-undo.
def     shared var v-Lcommpct    as de format ">>9.9"        no-undo.
*/

{g-all.i}
{g-ar.i}
{g-oe.i}
{g-ic.i}
{g-jump.i}

def var x-vendname   like  apsv.name                  no-undo.
def var x-slsrep1    like  smsn.slsrep                no-undo.
def var x-slsrep1%   as de format ">>9.9"             no-undo.
def var x-slsrep1$   as de format ">>>>>9.99"         no-undo.
def var x-company1   as c  format "x(15)"             no-undo.
def var x-company1%  as de format ">>9.9"             no-undo.
def var x-company1$  as de format ">>>>>9.99"         no-undo.
def var x-slsrep2    like  smsn.slsrep                no-undo.
def var x-slsrep2%   as de format ">>9.9"             no-undo.
def var x-slsrep2$   as de format ">>>>>9.99"         no-undo.
def var x-company2   as c  format "x(15)"             no-undo.
def var x-company2%  as de format ">>9.9"             no-undo.
def var x-company2$  as de format ">>>>>9.99"         no-undo.
def var x-slsrep3    like  smsn.slsrep                no-undo.
def var x-slsrep3%   as de format ">>9.9"             no-undo.
def var x-slsrep3$   as de format ">>>>>9.99"         no-undo.


form
"F6-Customer               F8-EXTEND    F9-Line Comments     F10-Notes           "
  at 1 dcolor 2
with frame f-statusline no-box no-labels overlay row 21.  

form
  "Vendor#:"                      at  8
  v-vendno                        at 16
  x-vendname                      at 30
  "PCat:"                         at 62
  v-prodcat                       at 68
  "Product:"                      at  2
  v-prod                          at 16 
  "QtyOrd:"                       at 43
  v-qty                           at 51
  "Unit Cost:"                    at 40
  v-prodcost                      at 51
  skip(1)
  "Sales Rep"                     at  3
  "Split %"                       at 14
  "Comm $"                        at 26
  "Company Split"                 at 43
  "Split %"                       at 61
  "Comm $"                        at 73
  x-slsrep1                       at  5
  x-slsrep1%                      at 16
  x-slsrep1$                      at 24
  x-company1                      at 44
  x-company1%                     at 63
  x-company1$                     at 70
  x-slsrep2                       at  5
  x-slsrep2%                      at 16
  x-slsrep2$                      at 24
  x-company2                      at 44
  x-company2%                     at 63
  x-company2$                     at 70
  x-slsrep3                       at  5
  x-slsrep3%                      at 16
  x-slsrep3$                      at 24
  skip(1)
  "Net"                           at 19
  "Cost"                          at 34
  "Comm $"                        at 48
  "Comm %"                        at 63
  v-totnet   format "zzzzzz9.99"  at 15 
  v-totcost  format "zzzzzz9.99"  at 30 
  v-totcomm  format "zzzzzz9.99"  at 46
  s-commpct  format "zz9.99"      at 64
     with frame f-extend overlay row 8 centered title "EXTEND" no-labels.

{w-sasoo.i g-operinits no-lock}

display with frame f-statusline.
if v-prodcat = " " then
  do:
  find icsp where icsp.cono = g-cono and
                  icsp.prod = v-prod
                  no-lock no-error.
  if avail icsp then
    assign v-prodcat = icsp.prodcat.
end.   
find apsv where apsv.cono = 1 and
                apsv.vendno = v-vendno
                no-lock no-error.
if avail apsv then
  assign x-vendname = apsv.name.
else
  assign x-vendname = "Vendor UnKnown".

find oeelb where oeelb.cono = g-cono and
                 oeelb.batchnm = string(v-quoteno,">>>>>>>9") and
                 oeelb.seqno   = 2 and
                 oeelb.lineno  = v-lineno
                 no-lock no-error.
if avail oeelb then
  assign s-commpct = oeelb.termspct.
find first bpeh where bpeh.cono = g-cono and
                      bpeh.bpid = string(v-quoteno,">>>>>>>9") + "C" and
                      bpeh.termstype <> "0.0"
                      no-lock no-error.
if avail bpeh and bpeh.termstype <> "0.0" then           
    assign v-Hcommpct = dec(bpeh.termstype).

for each bpel where 
         bpel.cono   = g-cono and
         bpel.bpid   = string(v-quoteno,">>>>>>>9") + "C" and
         bpel.lineno = v-lineno
         no-lock:
  if bpel.revno = 1 then
    assign x-slsrep1   = bpel.itemid 
           x-slsrep1%  = dec(bpel.commtype)
           x-slsrep1$  = bpel.awardprice.
  if bpel.revno = 2 then
    assign x-slsrep2   = bpel.itemid 
           x-slsrep2%  = dec(bpel.commtype)
           x-slsrep2$  = bpel.awardprice.
  if bpel.revno = 3 then
    assign x-slsrep3   = bpel.itemid 
           x-slsrep3%  = dec(bpel.commtype)
           x-slsrep3$  = bpel.awardprice.
  if bpel.revno = 4 then
    assign x-company1  = bpel.itemid
           x-company1% = dec(bpel.commtype)
           x-company1$ = bpel.awardprice.
  if bpel.revno = 5 then
    assign x-company2  = bpel.itemid
           x-company2% = dec(bpel.commtype)
           x-company2$ = bpel.awardprice.
end. 
assign v-totcomm = x-slsrep3$ + x-slsrep2$ + x-slsrep1$.
  
F8-loop:
  do while true on endkey undo, leave F8-loop:
    display v-vendno       
            x-vendname     
            v-prodcat      
            v-prod         
            v-qty
            v-prodcost
            x-slsrep1    when x-slsrep1   <> " "
            x-slsrep1%   when x-slsrep1%  <> 0
            x-slsrep1$   when x-slsrep1$  <> 0
            x-company1   when x-company1  <> " "
            x-company1%  when x-company1% <> 0
            x-company1$  when x-company1$ <> 0
            x-slsrep2    when x-slsrep2   <> " "
            x-slsrep2%   when x-slsrep2%  <> 0
            x-slsrep2$   when x-slsrep2$  <> 0
            x-company2   when x-company2  <> " "
            x-company2%  when x-company2% <> 0
            x-company2$  when x-company2$ <> 0
            x-slsrep3    when x-slsrep3   <> " "
            x-slsrep3%   when x-slsrep3%  <> 0
            x-slsrep3$   when x-slsrep3$  <> 0
            v-totnet     
            v-totcost
            v-totcomm
            s-commpct
            with frame f-extend.
     /*
     update v-prodcost with frame f-extend.
     if frame-field = "v-prodcost" and input v-prodcost <> v-prodcost and
       {k-accept.i} or lastkey = 13 then
       do:
       assign v-prodcost = input v-prodcost.
       assign v-totcost  = v-prodcost * v-qty.
       assign v-totcomm  = v-totnet - v-totcost.
       assign v-costoverfl = yes
              v-Lcommpct = ((v-totnet - v-totcost) / v-totnet) * 100.
     end.
     */
     if (keyfunction(lastkey)) = "go" or lastkey = 13 then
       do:
       apply lastkey.
       leave F8-loop.
     end.
     if (keyfunction(lastkey)) = "END-ERROR" then
       do:
       apply lastkey.
       leave F8-loop.
     end.
     readkey.
   end. /* F8-Loop */
   hide frame f-extend no-pause.
   hide frame f-statusline no-pause.
   
   
   return.


