/* poreceiveupdateJT.i */
/* update the po receiving info */

if s-updatefl then do:
  release poelb.

  for each poehb use-index k-openinit where                        
           poehb.cono        = g-cono  and    
           poehb.receivefl   = yes          and    
           poehb.whse        = g-whse       and    
           poehb.openinit    = g-operinit   and
           poehb.whse        = s-rcvwhse    and     
           poehb.updtfl      = yes          and
           substring(poehb.user5,1,6) = "inb856" 
  no-lock:           
    /* change to for-each! Move non-stock bin check to here! dkt */     

    if substring(poehb.user5,1,6) = "inb856" then
      find first poelb where poelb.cono = poehb.cono and
                 poelb.shipmentid = poehb.shipmentid and
                 poelb.pono  = poehb.pono and
                 poelb.posuf = poehb.posuf and
                 substring(poelb.user5,7,1) <> "y" and 
                 (poelb.qtyrcv <> 0  and poelb.user6 <> 0)
      no-lock no-error.
    
     if avail poelb then do:
      assign s-updatefl = no.
      run zsdierrx.p 
        (input "UnAck ASN" + " " + string(poelb.pono) + " " + "Ln" + " " 
        + string(poelb.lineno),
         input "yes").
        
        /*
        (input "ASNed Line exists without verification " +
                string(poelb.pono) + "-" + string(poelb.posuf,"99")
                + " Line " + string(poelb.lineno), 
         input "yes"). 
        */ 

      pause.
    end.
  end.
end.
    
if s-updatefl then do:      
  for each poehb use-index k-openinit where        
         poehb.cono        = g-cono  and   
         poehb.receivefl   = yes          and   
         poehb.whse        = g-whse       and   
         poehb.openinit    = g-operinit   and   
         poehb.whse        = s-rcvwhse    and   
         poehb.updtfl      = yes                
         no-lock:                                        
                  
  for each poelb where poelb.cono = poehb.cono and       
            poelb.shipmentid = poehb.shipmentid and       
            poelb.pono  = poehb.pono and                  
            poelb.posuf = poehb.posuf and                 
            poelb.qtyrcv <> 0                           
   no-lock:                                                 
      /* part moved up, autoWhse = yes */
      /* dkt update if match, else ignore it */                        
     find icsd where icsd.cono = g-cono and icsd.whse = poehb.whse    
     no-lock no-error.                                                

     if avail icsd then                                               
        if entry( 1, icsd.user4) = "Y" then do:                          
          find first zzbin                                             
            where zzbin.cono = poelb.cono                       
            and zzbin.whse = poehb.whse                                  
            and zzbin.prod = poelb.shipprod                              
            and zzbin.transproc = "PO"+ STRING(poehb.pono)
            /*  + "-" + STRING(poehb.posuf) use shipmentid?? */
            and zzbin.binloc ne ""  exclusive-lock no-error.             
            
          if avail zzbin then do:                                      
            /* update */                                               
            find icsw where 
                 icsw.cono = g-cono and                       
                 icsw.prod = poelb.shipprod and
                 icsw.whse = poehb.whse no-error.            
            find first wmsb where 
                       wmsb.cono = g-cono + 500 and
                       wmsb.whse = poehb.whse and 
                       wmsb.binloc = zzbin.binloc no-error.                                       
            if avail wmsb then do:                                          
              assign wmsb.statuscode = "A"                                                            wmsb.assigncode = "N"                                   
                     wmsb.priority   = 1.                                                    if avail icsw then do:                                        
                assign wmsb.assigncode = icsw.statustype.                                     if icsw.binloc1 = "" or icsw.binloc1 = "New Part" then do:   
                  icsw.binloc1 = zzbin.binloc.                           
                end.                                                        
                else do:                                                    
                    /* error out if mismatch per Tami */                      
                    /* if = then probably set through earlier zzbin record */ 
                if icsw.binloc1 ne zzbin.binloc and
/* INSPECT */
                   icsw.wmrestrict <> "insp" and
                   not(zzbin.binloc begins "insp" ) then do:                
/* INSPECT */    
                   message zzbin.prod " for bin " zzbin.binloc           
                       " should be in " icsw.binloc1.                       
                   s-updatefl = no.                                      
                   bell.                                                 
                   pause.                                                
                end.                                                    
/* INSPECT */
                else if avail icsw and icsw.wmrestrict = "insp" and
                   zzbin.binloc begins "insp"  and                 
                   icsw.binloc1 ne zzbin.binloc and
                   icsw.binloc2 ne zzbin.binloc and
               
                   not can-find(first b-zzbin where                            
                                      b-zzbin.cono = g-cono and 
                                      b-zzbin.whse = poehb.whse and
                                      b-zzbin.prod = poelb.shipprod and 
                                      b-zzbin.binloc  = zzbin.binloc) then do:
                   create b-zzbin.
                   assign wmsb.statuscode = "A"
                          wmsb.assigncode = if avail icsw then 
                                              icsw.statustype
                                            else
                                              "N"
                          wmsb.priority   = 9.
                   assign  b-zzbin.cono  = g-cono  
                           b-zzbin.whse  = zzbin.whse  
                           b-zzbin.binloc = zzbin.binloc 
                           b-zzbin.prod   = zzbin.prod
                           b-zzbin.operinit = g-operinits                                                 b-zzbin.transdt = TODAY                                                          b-zzbin.transtm = 
                             replace(STRING(TIME,"HH:MM:SS"),":","") 
                           b-zzbin.transproc = "PO" + string(poehb.pono).
                end.

/* INSPECT */    
              
              end.                                                      
            end. /* icsw */                                                
          find first wmsbp where 
                     wmsbp.cono = g-cono + 500 and         
                     wmsbp.whse = poehb.whse and 
                     wmsbp.binloc = zzbin.binloc and    
                     wmsbp.prod = poelb.shipprod no-lock no-error.           
          if not avail wmsbp then do:                                 
            create wmsbp.                                              
            assign wmsbp.cono = g-cono + 500                                 
                   wmsbp.whse = poehb.whse.                                   
/*            {t-all.i wmsbp} */
            assign
                   wmsbp.transproc = "poreceiveupdate.i"
                   wmsbp.binloc = zzbin.binloc                               
                   wmsbp.prod = poelb.shipprod.                               
           end.
         end. /* avail wmsb */                                            
         if not avail wmsb then do:                                     
           message "Bin "  zzbin.binloc " not found".                   
           message "PO" poelb.pono " caused update to stop".            
           s-updatefl = no.                                             
           bell.                                                        
           pause.                                                       
         end.                                                           
         /* end update */                                               
            /* delete found zzbin - it did its job */                      
          delete zzbin.                                                  
        end. /* if avail zzbin */                                        
      end. /* if autoWhse */        
        /* end insert autoWhse */
    end. /* new for each poelb */
  end.
end.

if s-updatefl = no then do:
  assign g-currproc = hld-currproc
         g-ourproc  = hld-currproc
         g-exitproc = hld-currproc.
  return.
end.
else
if s-updatefl then do:
  assign s-rcvwhse = s-rcvwhse.
  {w-icsd.i s-rcvwhse "no-lock" "x-"} 
  if not avail x-icsd then do:
    bell.
    message "Warehouse To Receive For Not Set Up in Warehouse Setup - ICSD"
      s-rcvwhse.
    pause.
  end.
  else 
    assign g-whse = s-rcvwhse
           b-whse = s-rcvwhse.
  
  assign g-currproc   = "poei"
         g-ourproc    = "poei".
  
  run poezbco.p (input sasc.crarfl,
                 input p-postdt,
                 input p-period).  
  g-exitproc = "poei".
end. 
     
find b-sasc where b-sasc.cono = g-cono no-lock no-error.
cPOCrctReason = if avail b-sasc then b-sasc.pocrctreason else "".

/*d If the journal was not opened or the journal procedure is not for POEI,
    get out of the Receiving */
if g-jrnlno = 0 or g-jrnlproc ne g-currproc then leave.

/*d If a journal is already open, set the whse to the whse on the first POEI
    record */
if g-jrnlno > 0 then do:
  assign h-jrnlno = g-jrnlno.
  find first poei use-index k-lineno where
             poei.cono   = g-cono and
             poei.jrnlno = g-jrnlno
             no-lock no-error.
          
  if avail poei then 
    do for poeh, poel:
      {w-poeh.i poei.pono poei.posuf no-lock}
      {w-poel.i poei.pono poei.posuf poei.lineno no-lock}
      if avail poel then
        assign v-divno = poeh.divno
               v-whse  = poel.whse
               v-firstfl = yes.
    end.
end. /* g-jrnlno > 0 */
     
if avail poei then
  {w-poeh.i poei.pono poei.posuf no-lock b-}
else
  {w-poeh.i g-pono g-posuf no-lock b-}

if v-firstfl = yes and v-pocapfl = yes and
  (v-pocapaddfl = yes or v-pocapdiscfl = yes) and
  g-potype ne "rm" then
  do i = 1 to 4:
    if b-poeh.addonno[i] > 0 then
      {w-sastn.i "x" b-poeh.addonno[i] no-lock}
    assign
      s-poadddist[i]  = if v-vapofl = yes
                          then "m"
                          else if b-poeh.addonno[i] > 0
                                  and avail sastn
                               then sastn.state
                               else ""
      s-addondesc[i]  = if b-poeh.addonno[i] > 0 and avail sastn
                          then sastn.descrip
                          else "".
  end. /* do 1 to 4 */
assign v-poadddist = if s-poadddist[1] = "m" or
                        s-poadddist[2] = "m" or
                        s-poadddist[3] = "m" or
                        s-poadddist[4] = "m" then "m" else "a"
       lCrctExists = false.
do transaction:
if s-updatefl then do:
    
    /* Update Zsdiputid here so we can capture copies */

    /* This is temporary in company 501 and is used in 
       oeepbpr.z99 once and then deleted       SX 55 
    */   
    do for b-zsdiputid:
    find   b-zsdiputid use-index jrnlix  where
           b-zsdiputid.cono = g-cono + 500 and
           b-zsdiputid.whse = g-whse and
           b-zsdiputid.jrnlno = g-jrnlno no-error.
    if not avail b-zsdiputid then do:
      create b-zsdiputid.
      assign b-zsdiputid.cono      = g-cono + 500 
               b-zsdiputid.whse      = g-whse
               b-zsdiputid.jrnlno    = g-jrnlno 
               b-zsdiputid.receiptid = 1.
      end.
    
    assign b-zsdiputid.receiptid = s-rcptno. 
    end.
    /* Update Zsdiputid here so we can capture copies */
end.
end.

if s-updatefl then 
  do transaction:
    /*e Actual Processing of the Batch Files */
    /* fix up suffixes */
    for each poehb use-index k-openinit where                        
             poehb.cono        = g-cono  and    
             poehb.receivefl   = yes          and    
             poehb.whse        = g-whse       and    
             poehb.openinit    = g-operinit   and
             poehb.whse        = s-rcvwhse /* v-saso-whse */  and     
             poehb.updtfl      = yes no-lock:                
       
      assign zx-suff = poehb.posuf
             zx-okfl = true.
      run poehb_suffix (input poehb.pono,
                        input-output zx-suff,
                        input-output zx-okfl,
                        input-output zx-error).
      if not zx-okfl then do:
        message {poehb_suffix.gcr  
                   zx-error
                   zx-l                
                   poehb.pono
                   poehb.posuf}.
        pause 5.
      end.
    end. /* for each poehb */
  end.

if s-updatefl then do:
   
  /* print and update, then delete batch */
  run poeibwpstkJT.p(input g-whse, input g-operinit).
  run RFLoop.
  if not zx-l then do:
    /* print report */
    {zsdipoeiupd.lpr &message = "run zsdierrx.p "
                     &confirm = "confirm"
                     &jrnlno  = "g-jrnlno"
                     &next    =
          " assign g-currproc = 'poezb' g-exitproc = g-currproc. undo main ,
                   next main."
                     &crctconfirm   = "lCrctExists = true"
                     &pocrctreason  = "cPOCrctReason = ''"}
  end.
  else do:
    /* print report, handheld */
    {zsdipoeiupd.lpr &message = "run ibrferrx.p "                            
                     &confirm = "confirm"
                     &jrnlno  = "g-jrnlno"   
                     &next    =                                                            " assign g-currproc = 'hher' g-exitproc = g-currproc. undo main , 
                         next main."                                                                &crctconfirm   = "lCrctExists = true"                                           &pocrctreason  = "cPOCrctReason = ''"}                       end.      
  assign v-addondist  = 0
         v-wodiscdist = 0.
  assign x-f10-whse = s-rcvwhse.

  /*d Perform the automatic allocation of addons and the
      whole order discount */
  if v-pocapfl = yes and
    (v-pocapaddfl = yes or
    (v-pocapdiscfl = yes and can-do("w,c,d,u",v-powodist)))
    then run poeia.p.
       
  /* Adjust and create correction PO's if necessary */
  if lCrctExists = true then
    run poeicrct.p(input g-jrnlno,
                   input g-setno,
                   input s-adjbofl).
    
  /*d Perform the update of other related/tied tables */
  run poeiu.p.
  assign g-currproc = hld-currproc
         g-ourproc  = hld-currproc
         g-exitproc = hld-currproc.
  message "before ICupdate " g-cono g-jrnlno h-jrnlno.
  run zsdiICupdate ( input g-cono,
                     input h-jrnlno).
  
  
  
  return. 
end. /* s-updatefl */

assign g-currproc = hld-currproc
       g-ourproc  = hld-currproc
       g-exitproc = hld-currproc.
end. /* main */

release sasj. /* hanging up on journal assignment */
hide all no-pause.

Procedure RFLoop:                          
assign zx-l = no                           
       zx-x = 1.                           
repeat while program-name(zx-x) <> ?:      
  if program-name(zx-x) = "hher.p" then do:
    zx-l = yes.                          
    leave.                                 
  end.                                     
  assign zx-x = zx-x + 1.                  
end.                                       
end procedure.                             
