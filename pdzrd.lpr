/* pdsrd.lpr 1.1 01/03/98 */
/* {filename} {version} {date} */
/*h*****************************************************************************
  INCLUDE : pdsrd.lpr
  DESCRIPTION : Find Next Rebate Record Number to Use.
  USED ONCE? : yes
  AUTHOR : gdf
  DATE WRITTEN : 07/21/95
  CHANGES MADE :
   07/21/95 gdf; TB# 18812 7.0 Rebates Enhancement (A5a/b) Complete Rewrite.
   08/31/99 ajw; TB# e2328 Common code use for GUI interface
*******************************************************************************/
/*d Find the next Rebate Record Number to use. */
 find last pdsr use-index k-rebrecno where
           pdsr.cono = (g-cono + 500)
           no-lock no-error.
 if not avail pdsr then s-rebrecno = 1.
 else do:
   if pdsr.rebrecno < 9000000
     then s-rebrecno = pdsr.rebrecno + 1.
   else do:
     do i = 1 to 8999999:
     find pdsr use-index k-rebrecno where
          pdsr.cono = (g-cono + 500) and
          pdsr.rebrecno = i
          no-lock no-error.
     if not avail pdsr then leave.
     end. /* do i = 1 to 8999999 */

   if i <> 9000000 then s-rebrecno = i.
   else do:
/*d Too Many Rebate Records;
    Purge Unneeded Records */
    run err.p(6429).
    next main.
    end. /* else do */
  end. /* else do */
end. /* else do */



