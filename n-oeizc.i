/* n-oeir.i 1.1 01/03/98 */
/* n-oeir.i 1.4 1/11/94 */
/*h*****************************************************************************

  INCLUDE      : n-oeir.i
  DESCRIPTION  : Order Entry Credit Release (OEIR) - OEEHC search
  USED ONCE?   : yes (oeir.p)
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    10/01/93 rhl; TB# 12731; Pages are not scrolling properly
    01/11/94 mwb; TB# 14107; cross locking - oeeh, added do for's
    03/08/95 gp;  TB# 15446; Ctrl-F brings up open order, added v-srcorderno,
                      v-srcpriority and v-srcbestordno criteria
    10/20/99 gfk; TB# 26831; Performance improvement using {3}, {4}
                      and {5} only when selected as criteria for the find
                *******************************************************************************/


/*e The order numbers display in the following priority order:
  Priority 1 - counter sale
  Priority 2 - will call orders
  Priority 9 - other orders
    If CTRL-F is used and an order # entered which is not on hold but
    is within the range of orders displaying on the screen, the cursor
    is positioned on the next order # regardless of the priority.  This
    means we have to search in priority order.  For example: priority 1
    orders 50, 60 and 80, and priority 9 orders 55 and 58 are displaying
    on the screen.  If order # 54 is entered via CTRL-F, we will put
    the cursor on order # 55.                                           */

  if {c-edit.i s-custno} then
    find {1} oeehc use-index k-custno no-lock where
            oeehc.custno      = s-custno      and
            oeehc.cono        = g-cono        and
            oeehc.statustype  = yes
    /{3}*/
        and oeehc.orderno = v-srcorderno
    /{3}*/
    /{4}*/
        and oeehc.priority  = v-srcpriority
    /{4}*/
    /{5}*/
        and oeehc.orderno > v-srcbestordno
    /{5}*/
       no-error.
    
  else if s-takenby ne "" then
    find {1} oeehc  no-lock where
             oeehc.cono        = g-cono        and
             oeehc.statustype  = yes  /*  and
             (substring(oeehc.user3,1,4) = s-takenby or
              substring(oeehc.user3,1,4) = "    ")  */
    /{3}*/
         and oeehc.orderno = v-srcorderno
    /{3}*/
    /{4}*/
         and oeehc.priority  = v-srcpriority
    /{4}*/
    /{5}*/
         and oeehc.orderno > v-srcbestordno
    /{5}*/
      no-error.
 
  else
    find {1} oeehc use-index k-inq no-lock where
             oeehc.cono        = g-cono        and
             oeehc.statustype  = yes
    /{3}*/
         and oeehc.orderno = v-srcorderno
    /{3}*/
    /{4}*/
         and oeehc.priority  = v-srcpriority
    /{4}*/
    /{5}*/
         and oeehc.orderno > v-srcbestordno
    /{5}*/
      no-error.
    
        /*tb 14107 01/11/94 mwb; added do for oeeh, arsc */
    do for oeeh, arsc:
      {p-oeirw.i}
    if s-takenby <> "" then
      if not avail oeeh or oeeh.takenby <> s-takenby then
        confirm = false.                            
/*
    if g-menusel = "ir" then
       if avail oeeh and can-do ("a,t,m",oeeh.approvty) then
         confirm = false.
    if g-menusel = "im" then
       if avail oeeh and not can-do("f,d,m",oeeh.approvty) then
         confirm = false.
*/
/*
if g-operinits = "tah" then 
  do:
  message g-menusel g-ourproc.
  pause.
  end.
 */
   end.

   
