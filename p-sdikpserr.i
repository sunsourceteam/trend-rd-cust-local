/*h*****************************************************************************
  INCLUDE      : q-kpserr.i
  DESCRIPTION  : Used to display Kit error messages for either the kit itself
 or the component products (ie. no related sequence #s)
 during setups.
  USED ONCE?   : no     Used for:  kpet (kit error checks)
   kpsk, kpsg, kpso, kpss -> p-kpsedt.i
   kpsk - kit product check
  AUTHOR       : ??
  DATE WRITTEN : 02/16/92
  CHANGES MADE :
    03/06/95 ajw; TB# 17395 Incorrect Msg: is 4720, change to 4716
    12/29/98 ajw; TB# 24717 Common Code for GUI Interface.
    09/22/99 ama; TB# e2761 - Tally Project - Add message for new
       kittype of M
    11/01/02 tmg; TB# e8651 Cores 61, core items cannot be components
    05/16/03 tmg; TB# e17279 Cores kit processing
    06/04/03 cm;  TB# e17279 CR issues for tmg
 ******************************************************************************/

/* Error Conditions */

/* ICSP setup */
if v-errfl = "a":u then run err{&pv}.p(4600).

/* Units */
else if v-errfl = "b":u then run err{&pv}.p(4026).

/* ICSP status */
else if v-errfl = "c":u then run err{&pv}.p(4735).

/* Catch weight */
else if v-errfl = "d":u then run err{&pv}.p(5638).

/* Kit type = b */
else if v-errfl = "e":u then run err{&pv}.p(4720).

/* ICSW setup  */
else if v-errfl = "f":u then run err{&pv}.p(4602).

/* ICSW status */
else if v-errfl = "g":u then run err{&pv}.p(4736).

/* Core charge */
else if v-errfl = "j":u then run err{&pv}.p(6065).

/*tb 17395 03/03/95 ajw; Wrong message from KPET*/
/*e Product Must be a Prebuilt Kit*/
else if v-errfl = "k":u then run err{&pv}.p(4716).

/* Kit type = m */
else if v-errfl = "m":u then run err{&pv}.p(7003).

        /*Kit Component cannot be implied core item*/
        else if v-errfl = "r":u then run err{&pv}.p(4738).

        /*Kit Component reman Parent not reman*/
        else if v-errfl = "s":u then run err{&pv}.p(4738).


