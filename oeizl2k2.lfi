/* oeizl2k2.lfi 1.2 06/12/98 */
/*h*****************************************************************************
  SX 55
  INCLUDE      : oeizl2k2.lfi
  DESCRIPTION  : Find Next/Prev for OEIZL when v-mode = 2
  USED ONCE?   : yes
  AUTHOR       : mtt
  DATE WRITTEN : 05/11/95
  CHANGES MADE :
    05/11/95 mtt; TB#  5180 When looking for a product-need kit ln
    03/14/96 tdd; TB# 20713 Inquiry not working properly
    05/29/98 cm;  TB# 24295 Incorrect qty/value on components. Need to consider
        quantity needed per kit.
    08/08/02 lcb; TB# e14281 User Hooks
*******************************************************************************/

/*e*****************************************************************************
     Selection Criteria for this procedure:
       v-mode ==> 2:
               Product: entered on the screen
    Kit Component Type: "k" (Kit Components Only)
             Customer#: <blank> (0)
               CustPO#: <blank>
                  Whse: <blank>
*******************************************************************************/
do:
confirm = yes.


if ("{1}" = "FIRST" and s-directionfl )  then
  assign v-xmode = "C".
else
if ("{1}" = "LAST" and not s-directionfl )  then
  assign v-xmode = "O".

if v-xmode = "C" then
  do:
 
find {1} oeehb use-index k-oeehb where
         oeehb.cono     = g-cono        and

         (s-transtype  = ""  or  s-transtype    = "QU" 
           /* oeehb.transtype */ ) and
/*  or       (s-transtype  = "bo" and oeeh.borelfl)) and  */

         (s-takenby    = ""   or oeehb.takenby   = s-takenby) and
          
         ((oeehb.stagecd <> 1 and oeehb.stagecd >= integer(s-stagecdl)) or
          (oeehb.stagecd = 1 and integer(s-stagecdl) = 0)) and
         (oeehb.stagecd <= integer(s-stagecdh)) and

         (s-promisedt  = ?    or oeehb.promisedt >= s-promisedt) and
         (s-custpo     = ""   or oeehb.custpo    = s-custpo) and
         (s-shipto     = ""   or oeehb.shipto    = s-shipto) and
         oeehb.sourcepros = "ComQu" and
         oeehb.seqno = 2
         {oeizl.z99 &user_find_oeizl = "*"
                    &file            = "oeehb"}

no-lock no-error.
if avail oeehb then do:
    confirm = no.
    find first oeelk use-index k-prod where
               oeelk.cono       = g-cono and
               oeelk.whse       = oeehb.whse and
               oeelk.shipprod   = s-prod and
               oeelk.statustype = "a" and
               oeelk.orderno    = int(oeehb.batchnm) and
               oeelk.ordersuf   = 00  and
               oeelk.ordertype  = "b"
    no-lock no-error.
    if avail oeelk then confirm = yes.
    else do:
        find first oeelk use-index k-prod where
                   oeelk.cono       = g-cono and
                   oeelk.whse       = oeehb.whse and
                   oeelk.shipprod   = s-prod and
                   oeelk.statustype = "i" and
                   oeelk.orderno    = int(oeehb.batchnm) and
                   oeelk.ordersuf   = 00 and
                   oeelk.ordertype  = "b"
        no-lock no-error.
        if avail oeelk then confirm = yes.
    end.
    if confirm = yes and avail oeelk and s-doonlyfl = yes then do:
        find first oeelb where
                   oeelb.cono     = g-cono and
                   oeelb.batchnm  = oeehb.batchnm and
                   oeelb.seqno    = oeehb.seqno   and
                   oeelb.lineno   = oeelk.lineno  and
                   oeelb.botype   = "d"
        no-lock no-error.
        confirm = if avail oeelb then yes else no.
    end.
end.

/*tb 24295 05/29/98 cm; Incorrect qty/value on components. Use IP to calculate
    component totals. */

if confirm = yes then do:
/*
    run tot-order-lines in this-procedure (buffer oeeh, output v-netamt).

    /*d Set a flag so that the display include will know to use the value
        just calculated instead of the value off the order header record */
*/  
    v-uselnfl = yes.

end. /* if confirm = yes */

if avail oeehb then
  find zxinq where zxinq.present = true  no-lock no-error.
else
  find zxinq where zxinq.present = false no-lock no-error.

end.
  
if v-xmode = "C" and ((confirm = yes and not avail oeehb) or 
                        not avail oeehb) and
   ("{1}" = "first" or "{1}" = "next" ) then
  do:
  assign v-xmode = "O"
         confirm = yes.
  find first oeeh use-index k-oeeh where
             oeeh.cono     = g-cono      no-lock no-error.

  end.

if v-xmode = "O" then
do:
find {1} oeeh use-index k-oeeh where
         oeeh.cono     = g-cono        and

         (s-transtype  = ""  or  s-transtype    = oeeh.transtype or
         (s-transtype  = "bo" and oeeh.borelfl)) and

         (s-takenby    = ""   or oeeh.takenby   = s-takenby) and

         (oeeh.stagecd >= integer(s-stagecdl)) and
         (oeeh.stagecd <= integer(s-stagecdh)) and

         (s-promisedt  = ?    or oeeh.promisedt >= s-promisedt) and
         (s-custpo     = ""   or oeeh.custpo    = s-custpo) and
         (s-shipto     = ""   or oeeh.shipto    = s-shipto)
         {oeizl.z99 &user_find_oeizl = "*"
                    &file            = "oeeh"}

no-lock no-error.
if avail oeeh then do:
    confirm = no.
    find first oeelk use-index k-prod where
               oeelk.cono       = g-cono and
               oeelk.whse       = oeeh.whse and
               oeelk.shipprod   = s-prod and
               oeelk.statustype = "a" and
               oeelk.orderno    = oeeh.orderno and
               oeelk.ordersuf   = oeeh.ordersuf and
               oeelk.ordertype  = "o"
    no-lock no-error.
    if avail oeelk then confirm = yes.
    else do:
        find first oeelk use-index k-prod where
                   oeelk.cono       = g-cono and
                   oeelk.whse       = oeeh.whse and
                   oeelk.shipprod   = s-prod and
                   oeelk.statustype = "i" and
                   oeelk.orderno    = oeeh.orderno and
                   oeelk.ordersuf   = oeeh.ordersuf and
                   oeelk.ordertype  = "o"
        no-lock no-error.
        if avail oeelk then confirm = yes.
    end.
    if confirm = yes and avail oeelk and s-doonlyfl = yes then do:
        find first oeel use-index k-oeel where
                   oeel.cono     = g-cono and
                   oeel.orderno  = oeeh.orderno and
                   oeel.ordersuf = oeeh.ordersuf and
                   oeel.lineno   = oeelk.lineno and
                   oeel.botype   = "d"
        no-lock no-error.
        confirm = if avail oeel then yes else no.
    end.
end.

/*tb 24295 05/29/98 cm; Incorrect qty/value on components. Use IP to calculate
    component totals. */
if confirm = yes then do:

    run tot-order-lines in this-procedure (buffer oeeh, output v-netamt).

    /*d Set a flag so that the display include will know to use the value
        just calculated instead of the value off the order header record */
    v-uselnfl = yes.

end. /* if confirm = yes */

if avail oeeh  then
  find zxinq where zxinq.present = true  no-lock no-error.
else
  find zxinq where zxinq.present = false no-lock no-error.
end.



if v-xmode = "o" and (not avail oeeh) and
   ("{1}" = "Prev" or "{1}" = "Last" ) then
  do:
  assign v-xmode = "C"
         confirm = yes.
 
find last oeehb  use-index k-oeehb where
          oeehb.cono     = g-cono        and

         (s-transtype  = ""  or  s-transtype    = "QU" 
            /* oeehb.transtype */ ) and
/*  or       (s-transtype  = "bo" and oeeh.borelfl)) and  */

         (s-takenby    = ""   or oeehb.takenby   = s-takenby) and
          
         ((oeehb.stagecd <> 1 and oeehb.stagecd >= integer(s-stagecdl)) or
          (oeehb.stagecd = 1 and integer(s-stagecdl) = 0)) and
         (oeehb.stagecd <= integer(s-stagecdh)) and

         (s-promisedt  = ?    or oeehb.promisedt >= s-promisedt) and
         (s-custpo     = ""   or oeehb.custpo    = s-custpo) and
         (s-shipto     = ""   or oeehb.shipto    = s-shipto) and
         oeehb.sourcepros = "ComQu" and
         oeehb.seqno = 2
         {oeizl.z99 &user_find_oeizl = "*"
                    &file            = "oeehb"}

no-lock no-error.
if avail oeehb then do:
    confirm = no.
    find first oeelk use-index k-prod where
               oeelk.cono       = g-cono and
               oeelk.whse       = oeehb.whse and
               oeelk.shipprod   = s-prod and
               oeelk.statustype = "a" and
               oeelk.orderno    = int(oeehb.batchnm) and
               oeelk.ordersuf   = 00  and
               oeelk.ordertype  = "b"
    no-lock no-error.
    if avail oeelk then confirm = yes.
    else do:
        find first oeelk use-index k-prod where
                   oeelk.cono       = g-cono and
                   oeelk.whse       = oeehb.whse and
                   oeelk.shipprod   = s-prod and
                   oeelk.statustype = "i" and
                   oeelk.orderno    = int(oeehb.batchnm) and
                   oeelk.ordersuf   = 00 and
                   oeelk.ordertype  = "b"
        no-lock no-error.
        if avail oeelk then confirm = yes.
    end.
    if confirm = yes and avail oeelk and s-doonlyfl = yes then do:
        find first oeelb where
                   oeelb.cono     = g-cono and
                   oeelb.batchnm  = oeehb.batchnm and
                   oeelb.seqno    = oeehb.seqno   and
                   oeelb.lineno   = oeelk.lineno  and
                   oeelb.botype   = "d"
        no-lock no-error.
        confirm = if avail oeelb then yes else no.
    end.
end.

/*tb 24295 05/29/98 cm; Incorrect qty/value on components. Use IP to calculate
    component totals. */

if confirm = yes then do:
/*
    run tot-order-lines in this-procedure (buffer oeeh, output v-netamt).

    /*d Set a flag so that the display include will know to use the value
        just calculated instead of the value off the order header record */
*/  
    v-uselnfl = yes.

end. /* if confirm = yes */

if avail oeehb then
  find zxinq where zxinq.present = true  no-lock no-error.
else
  find zxinq where zxinq.present = false no-lock no-error.

end.
 


end.
