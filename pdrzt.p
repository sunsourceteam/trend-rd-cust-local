/*  pdrzy.p
    pdis/pdrzt -  SPA pricing override report worksheet.
*/


{p-rptbeg.i}

{g-pdcalc.i &new = "new"}       
{speccost.gva "new shared"}     

def var cntz as integer.
def var v-i        as integer                    no-undo.
def var d-days     as i   format ">>9-"          no-undo.
def var d-asofdate as date                       no-undo.
def var n-type     as c   format "x"             no-undo.
def var n-title    as c   format "x(100)"        no-undo.
def var n-name     as c   format "x(20)"         no-undo.
def var n-yr       as i   format "99"            no-undo.
def var n-sls      as de  format ">>>>>>>-"      no-undo.
def var n-ct       as i   format "zzzzzzz99999"  no-undo.
def var n-vend     like pdsr.vendno              no-undo.
def var k-status   as logical                    no-undo.   
def var k-fromdt   as date                       no-undo.
def var k-todate   as date                       no-undo.
def var v-types    as character format "x"       no-undo.
/* def var v-levelcd like pdsc.levelcd            no-undo.*/
def var v-pricetypeb  like arsc.pricetype       no-undo.
def var v-pricetypee  like arsc.pricetype       no-undo.
def var v-prodprctpb  as character format "x(4)" no-undo.
def var v-prodprctpe  as character format "x(4)" no-undo.
def var v-prodprcatpb  as character format "x(4)" no-undo.
def var v-prodprcatpe  as character format "x(4)" no-undo.
def var v-whsebg     like icsw.whse no-undo.
def var v-whseed     like icsw.whse no-undo.
def var v-vendbg     like icsw.arpvendno no-undo.
def var v-vended     like icsw.arpvendno no-undo.
def var v-buyerbg    like poeh.buyer no-undo.
def var v-buyered    like poeh.buyer no-undo.
def var v-custbg     like arsc.custno no-undo.
def var v-custed     like arsc.custno no-undo.
def var n-newsort    as logical no-undo.
def var n-qtybrk     as integer format "zzzzzz9" no-undo.
def var n-mult       as character format "x" no-undo.
def var zac          as integer no-undo.
def var rzac         as integer no-undo.
def var x-buyer      like poeh.buyer no-undo.
def var x-refer      as char format "x(14)" no-undo.
def var h-prodline   like icsw.prodline  no-undo.
def var h-prodcat    like icsp.prodcat   no-undo.
def var h-prctype    like icsw.pricetype no-undo.
def var cat-sw       as logical init "no" no-undo. 
def var typ-sw       as logical init "no"  no-undo. 
def var v-ordno      as char format "x(10)" no-undo.
def var v-oeelid     as recid               no-undo.
def var v-shortnm    as char format "x(15)" no-undo.

def var p-pdrecno like oeel.pdrecno no-undo.      
def var p-prdprctype like icsw.pricetype no-undo. 
def var p-priced like oeel.price no-undo.         
def var p-discamt like oeel.discamt no-undo.      
def var p-listprice like oeel.price no-undo.      

/* SDI By Pcat */                                               
def buffer catmaster for notes.                                 
Def Var v-technology  as character format "x(30)"  no-undo.     
def var v-vendorid as c format "x(24)" no-undo.                 
def var v-parentcode as c format "x(24)" no-undo.               

def var w-month          as c    format "x(9)"       no-undo.
def var w-date           as c    format "x(10)"      no-undo.
def var work-date        as date format "99/99/9999" no-undo.
def var b1-date          as date format "99/99/9999" no-undo.
def var e1-date          as date format "99/99/9999" no-undo.
def var ze1-date         as date format "99/99/9999" no-undo.
def var b2-date          as date format "99/99/9999" no-undo.
def var e2-date          as date format "99/99/9999" no-undo.
def var beg-date         as date format "99/99/9999" no-undo.
def var leapyr           as de   format "9999.99"    no-undo.
def var leaptst          as c    format "x(7)"       no-undo.
def var p-perend                    as int                      no-undo.
def var b-invdate        as date                     no-undo.
def var e-invdate        as date                     no-undo.
def var x-invdate        as date                     no-undo.


define temp-table zpdzr 
    field custno   like pdsr.custno
    field rebrecno like pdsr.rebrecno
    field vendno   like pdsr.vendno
    field name     as char format "x(14)" 
    field buyer    as char format "x(4)"
    field vendname as char format "x(14)"
 index kz-custno
       custno   ascending  
       rebrecno ascending
       vendno   ascending.

form header
   "Date: " today "Time: " string(time,"HH:MM:SS")
   "Cost Overrides W/O SPA's" at 75
   "Operator: " at 143
   g-operinits  at 153
   "Page " at 162 page-number format ">>9" 
    n-title            at 60 
    skip(1)
with frame f-hd width 180 page-top.

/*
    "PD #"             at 1
    "Lvl"              at 7
    "Cust No."         at 13
    "Cust Name"        at 24
    "Vendor No."       at 42
    "Vendor Name"      at 54
    "Contract No."     at 73
    "Product"          at 94
    "Amount"           at 121
    "Typ"              at 130
    "Shp"              at 134
    "Buyer"            at 138
    "Reference"        at 144
    "Start Dt"         at 160
    "Expr Dt"          at 170
with frame f-hd width 180 page-top.

form
    pdsr.rebrecno     at 1 format ">>>>>>>"
    pdsr.levelcd      at 9  format ">"
    zpdzr.custno      at 11
    zpdzr.name        at 24
    pdsr.vendno       at 40
    zpdzr.vendname    at 53
    pdsr.contractno   at 69
    pdsr.levelkey     at 94 
    pdsr.rebateamt    at 119
    pdsr.rebcalcty    at 132
    pdsr.dropshipty   at 135
    x-buyer           at 138
    x-refer           at 144 
    pdsr.startdt      at 160
    pdsr.enddt        at 170
with frame f-pd  width 200 no-box no-labels.

*/

form 
 oeeh.whse         
 oeel.costoverfl  
 oeeh.invoicedt
 v-ordno       
 oeeh.transtype
 oeel.lineno   
 oeel.specnstype    
 oeel.shipprod     
 oeel.qtyship format "zzzz9.99"   
 oeel.prodcat      
 oeel.prodcost     
 oeel.price        
 oeel.ordertype
 oeel.orderaltno
 oeel.custno       
 oeeh.shipto format "x(4)"
 v-shortnm
 with down frame f-pder width 280 no-box.     

/*
assign d-days = dec(sapb.optvalue[1])
       n-yr = int(substr(string(today,"99/99/99"),7,2))
       n-type = sapb.optvalue[2].
*/ 

if sapb.rangebeg[1] ne "" then
   v-buyerbg = sapb.rangebeg[1].
else 
   v-buyerbg = " ".
if sapb.rangeend[1] ne "" then
   v-buyered = sapb.rangeend[1].
else 
   v-buyered = "zzzz".

if dec(sapb.rangebeg[2]) ne 0 then
   v-vendbg = dec(sapb.rangebeg[2]).
else
   v-vendbg = 0.
if dec(sapb.rangeend[2]) ne 0 then
   v-vended = dec(sapb.rangeend[2]).
else
   v-vended = 999999999999.
/*  
if dec(sapb.rangebeg[3]) ne 0 then
   v-custbg = dec(sapb.rangebeg[3]).
else
   v-custbg = 0.
if dec(sapb.rangeend[3]) ne 0 then
   v-custed = dec(sapb.rangeend[3]).
else
   v-custed = 999999999999.
*/ 

if sapb.rangebeg[4] ne "    " then
   v-whsebg = sapb.rangebeg[4].
else
   v-whsebg = "aaaa".
if sapb.rangeend[4] ne "" then
   v-whseed = sapb.rangeend[4].
else
   v-whseed = "````".                                   
  
if sapb.rangebeg[5] ne "    " then
  do:
   typ-sw = yes.
   v-prodprctpb = sapb.rangebeg[5].
  end.
else             
   v-prodprctpb = "    ".
if sapb.rangeend[5] begins "~~~~" then
   v-prodprctpe = "````".
else
  do:
   typ-sw = yes.
   v-prodprctpe = sapb.rangeend[5].
  end.

if sapb.rangebeg[6] ne "    " then
  do:
   cat-sw = yes.
   v-prodprcatpb = sapb.rangebeg[6].
  end. 
else
   v-prodprcatpb = "    ".
if sapb.rangeend[6] begins "~~~~" then
   v-prodprcatpe = "````".
else 
  do: 
   cat-sw = yes.
   v-prodprcatpe = sapb.rangeend[6].
  end. 

 if v-pricetypee begins "~~~~~~~~" then
  v-pricetypee = "````". 
 if  v-prodprctpe begins "~~~~~~~~" then
   v-prodprctpe = "````".
 if v-prodprcatpe begins "~~~~~~~~" then
   v-prodprcatpe = "````".
 
if sapb.rangebeg[3] ne "" then do:
 v-datein = sapb.rangebeg[3].
 {p-rptdt.i}
 if string(v-dateout) = v-lowdt then
   b-invdate = 01/01/1900.
 else  
   b-invdate = v-dateout.
end.
else
  b-invdate = 01/01/1900.

if sapb.rangeend[3] ne "" then do:
 v-datein = sapb.rangeend[3].
 {p-rptdt.i}
 if string(v-dateout) = v-highdt then
   e-invdate = 12/31/2049.
 else
   e-invdate = v-dateout.
end.
else
  e-invdate = 12/31/2049.

assign n-title = " Cost  Overrides - From " +
                  string(b-invdate,"99/99/99") + " To " + 
                  string(e-invdate,"99/99/99").

put control "~033~046~1541~117".   /* landscape format */
view frame f-hd.

oeeh-loop: 
for each icsd where 
         icsd.cono = g-cono and 
         icsd.whse >= v-whsebg       and 
         icsd.whse <= v-whseed  no-lock: 
do x-invdate = b-invdate to e-invdate:             
for each oeeh use-index k-invproc where          
    oeeh.cono = g-cono          and              
    oeeh.whse = icsd.whse       and 
    oeeh.invoicedt =  x-invdate and   
    oeeh.transtype <> "rm"      and          
    oeeh.transtype <> "cr"              
    no-lock:                   
  oeel-loop: 
  for each oeel use-index k-oeel where                            
      oeel.cono     = g-cono        and         
      oeel.orderno  = oeeh.orderno  and          
      oeel.ordersuf = oeeh.ordersuf and         
      oeel.costoverfl  = yes       and 
      oeel.specnstype <> "l"       and
      oeel.qtyship <> 0          
      no-lock:                     
                    
  find icsp where icsp.cono = g-cono  and                      
       icsp.prod = oeel.shipprod no-lock no-error.                      

  find first pder use-index k-pder where                          
             pder.cono       = g-cono + 500           
         and pder.orderno    = oeel.orderno 
         and pder.ordersuf   = 00 
         and pder.lineno     = oeel.lineno   
         and pder.whse       = oeeh.whse        
         no-lock no-error.       
  if avail pder then next oeel-loop.
  assign v-ordno  = string(oeel.orderno) + "-" + string(oeel.ordersuf)
         v-oeelid = recid(oeel).                         
  
  run zsdipricer4.p(input oeel.priceoverfl,
                    input v-oeelid, 
                    input oeel.shipprod, 
                    input oeel.qtyship, 
                    input oeeh.whse, 
                    input oeeh.custno,
                    input-output p-pdrecno,
                    input-output p-prdprctype, 
                    input-output p-priced, 
                    input-output p-discamt,
                    input-output p-listprice).
    
  assign v-shortnm = "". 
  find arsc where arsc.cono = g-cono  
   and arsc.custno = oeeh.custno 
   no-lock no-error.
  if avail arsc then 
    assign v-shortnm = arsc.lookupnm.
    
  display oeeh.whse 
          oeel.costoverfl label "Flg"
          oeeh.invoicedt
          v-ordno label "OrdNo" 
          oeeh.transtype   label "Type"
          oeel.lineno 
          oeel.specnstype  label "N/S"
          oeel.shipprod
          oeel.qtyship     label "Qty" 
          oeel.prodcat     label "PCat" 
          oeel.prodcost    Label "OeLineCost" 
          oeel.price       label "OeLinePrce" 
          oeel.ordertype   label "Tiety"
          oeel.orderaltno  label "TieOrd"
          oeel.custno 
          oeeh.shipto      label "Shp2" 
          v-shortnm        label "LookUpNm"
  with frame f-pder.  
  down with frame f-pder. 
 
 end. /* oeel loop */
end.  /* each oeeh */
end.
end.






