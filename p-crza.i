assign x-1stLine = yes.
for each apet where apet.cono    = g-cono       and
                    apet.vendno  = cret.vendno  and
                    apet.checkno = cret.checkno and
                  /*apet.jrnlno  = cret.jrnlno  and*/
                    apet.transcd = 03
                    no-lock:
  if num-entries(apet.refer,"-") > 1 then
    assign x-po  = entry(1,apet.refer,"-")
           x-suf = entry(2,apet.refer,"-").
  else
    assign x-po  = apet.refer
           x-suf = "0".
  find poeh where poeh.cono = g-cono and
                  poeh.pono = int(x-po) and
                  poeh.posuf = int(x-suf) and
                  poeh.vendno = cret.vendno
                  no-lock no-error.
  if avail poeh then
    do:
    if x-1stLine = yes then
      do:
      display cret.checkno cret.refer cret.vendno
              cret.enterdt cret.module
              cret.ckrectype s-manfl
              s-clrfl cret.statustype
              s-credit    when s-credit ne 0
              s-debit     when s-debit  ne 0
              poeh.pono
              poeh.posuf
              poeh.totinvamt
              apet.apinvno
              apet.amount
              apet.discamt
      with frame f-det1.
      down with frame f-det1.
      assign x-1stLine = no.
    end.
    else
      do:
      display poeh.pono
              poeh.posuf
              poeh.totinvamt
              apet.apinvno
              apet.amount
              apet.discamt
      with frame f-podtl.
      down with frame f-podtl.
    end. /* print the consecutive lines */
  end. /* avail poeh */
  else
    do:
    display cret.checkno cret.refer cret.vendno
              cret.enterdt cret.module
              cret.ckrectype s-manfl
              s-clrfl cret.statustype
              s-credit    when s-credit ne 0
              s-debit     when s-debit  ne 0
              0 @ poeh.pono
              0 @ poeh.posuf
    with frame f-det1.
    down with frame f-det1.
    leave.
  end. /* po is not available */
end. /* each apet */    
