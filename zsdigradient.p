
/***************************************************************************
 PROGRAM NAME: zsdigradient.p
  PROJECT NO.: 
  DESCRIPTION:  This program is interactive with p-oeetlu.i. At order entry,
                type 4 pdsc margins are checked with a flex gradient to
                determine the security level OE updates.
                Security level control records are created in the PDZG screen. 
  DATE WRITTEN: 07/25/06.
  AUTHOR      : SunSource.  
  MODIFIED:     das -02/20/09 Added Shipto as input parm to capture the proper
                     customer price type 
***************************************************************************/

{g-all.i}

define input parameter v-slsrepout    like oeeh.slsrepout. 
define input parameter v-custno       like oeeh.custno.
/* das - 02/20/09 - added v-shipto */
define input parameter v-shipto       like oeeh.shipto. 
define input parameter v-oldmarg      as decimal.
define input parameter v-chgmarg      as decimal. 
define input parameter v-lvl          as integer. 
define input parameter v-prod         like icsp.prod. 
define input parameter v-whse         like icsw.whse. 
define input parameter v-type         like icsw.pricetype.
define input parameter v-FlatRate     as logical.
define input parameter v-nstyp        like s-specnstype.
define input parameter v-oetyp        like oeel.ordertype.
define input parameter v-prcdiff      as decimal. 
define input parameter v-trendprc     as decimal. 
define input-output parameter v-errfl as integer. 
define input-output parameter v-errmsg as char . 

def var v-margdiff  as decimal format "999.99-" no-undo.
def var p-custtype  as char format "x(3)"       no-undo.
def var p-type      like icsw.pricetype         no-undo.
def var hit1        as logical                  no-undo. 
def var margdisplay as dec                      no-undo.

def buffer x-zsastz for zsastz.
def var    x-inx    as int                     no-undo.

assign p-custtype = "".

/* das - 02/20/09 - added arss logic */
if v-shipto <> "" then
  do:
  find arss where arss.cono   = g-cono and
                  arss.custno = v-custno and
                  arss.shipto = v-shipto
                  no-lock no-error.
  if avail arss then
    assign p-custtype = substr(arss.pricetype,1,4).
end.
else
  do:
  find first arsc where
             arsc.cono   = g-cono and 
             arsc.custno = v-custno
             no-lock no-error. 
  if avail arsc then 
     assign p-custtype = substring(arsc.pricetype,1,4).
end.

if v-FlatRate then do:
  /* Flat Rate Gradient check */
  assign p-custtype = "FLAT".
end.         





if v-type = "" then  
do: 
  find first icsw where 
             icsw.cono = g-cono and 
             icsw.prod = v-prod and 
             icsw.whse = v-whse 
       no-lock no-error.
  if avail icsw then 
     assign p-type = icsw.pricetype.  
end.
else 
  assign p-type = v-type.

assign v-margdiff = v-oldmarg - v-chgmarg
       v-errmsg   = ""
       hit1       = no.

do x-inx = v-lvl to 1 by -1: 
  find first zsastz where
             zsastz.cono       = g-cono and 
             zsastz.codeiden   = "OE-PType-GrMarg-Ctrl" + string(x-inx,"9") and 
             zsastz.labelfl    = false and
             zsastz.primarykey =  string(p-custtype,"xxxx") + 
                                  string(p-type,"xxxx")
             no-lock no-error.
  if not avail zsastz then
  do:
    find first zsastz use-index k-zsastz where                                 
              zsastz.cono     = g-cono and   
              zsastz.codeiden = "OE-Ptype-GrMarg-Ctrl" + string(x-inx,"9") and 
              zsastz.labelfl  = false                     and        
              substring(zsastz.primarykey,1,4) = string(p-custtype,"xxxx") and
              substring(zsastz.primarykey,5,1) = "*" 
              no-lock no-error. 
    if not avail zsastz then 
     do:
      find first zsastz use-index k-zsastz where                               
                 zsastz.cono     = g-cono and 
                 zsastz.codeiden = "OE-Ptype-GrMarg-Ctrl" + string(x-inx,"9")
             and zsastz.labelfl  = false and        
                 substring(zsastz.primarykey,1,1) = "*"      and 
                 substring(zsastz.primarykey,5,4) = string(p-type,"xxxx") 
                 no-lock no-error. 
     if not avail zsastz then  
      do:
       find first zsastz use-index k-zsastz where                            
                  zsastz.cono     = g-cono and   
                  zsastz.codeiden = "OE-Ptype-GrMarg-Ctrl" + string(x-inx,"9")
              and zsastz.labelfl  = false                     and         
                  substring(zsastz.primarykey,1,1) = "*"      and 
                  substring(zsastz.primarykey,5,1) = "*"  
                  no-lock no-error. 
      end.
     end.
  end.
 
 if avail zsastz and 
    dec(substring(zsastz.codeiden,21,1)) <= x-inx then 
 do: 
    leave.
 end. 
 if not avail zsastz then return.
end.

if not avail zsastz then return.

if avail zsastz then
  assign margdisplay = (v-oldmarg - v-chgmarg) - dec(zsastz.codeval[1]). 
else
  assign margdisplay = (v-oldmarg - v-chgmarg).    
if margdisplay < 999.99- then 
 assign margdisplay = 999.99-.
else  
if margdisplay > 999.99 then 
 assign margdisplay = 999.99.

if avail zsastz and v-chgmarg < (v-oldmarg - dec(zsastz.codeval[1])) then do: 
 if v-chgmarg < (v-oldmarg - dec(zsastz.codeval[1])) and 
    dec(substring(zsastz.codeiden,21,1)) <= v-lvl    and  
    v-oetyp ne "f" then do: 
  assign v-errfl = 1. 
  assign v-errmsg = 
    "Max marg change allowed is "
    + string(dec(zsastz.codeval[1]),">>9.999-") 
    + "% "
    + "for marg "
    + string(v-oldmarg,">>9.999-") 
    + "%"  
    + " Exceeded by "
    + string(margdisplay,">>9.99-")
    + " %  " 
    + "(PDZG:err100)".
 end.
end.
else 
if avail zsastz and (v-prcdiff > dec(zsastz.codeval[3])) then do: 
 if (v-prcdiff > dec(zsastz.codeval[3]))               and
     dec(substring(zsastz.codeiden,21,1)) <= v-lvl     and  
     v-oetyp ne "f" then do:  
   assign v-errfl = 1. 
   if v-FlatRate then
    assign v-errmsg =
      "Max price change allowed is " 
      + string(dec(zsastz.codeval[3]),">>>>>9.99")  
      + " for FlatRate " 
      + string(dec(v-trendprc),">>>>>9.99") + "                "
      + " Exceeded by "
      + string( (v-prcdiff - dec(zsastz.codeval[3])),">>>>>9.99")  
      + " (PDZG:err101)".
   else
     assign v-errmsg =
      "Max price change allowed is " 
      + string(dec(zsastz.codeval[3]),">>>>>9.99")  
      + " for unit price " 
      + string(dec(v-trendprc),">>>>>9.99") + "                "
      + " Exceeded by "
      + string( (v-prcdiff - dec(zsastz.codeval[3])),">>>>>9.99")  
      + " (PDZG:err101)".
  end.
end. 
else
assign v-errfl = 0.


