/* z-popapv.i 1.1 01/03/98 */
/* z-popapv.i 1.1 6/12/92 */
/*h****************************************************************************
  INCLUDE              : z-popapv.i
  DESCRIPTION          : Custom Assignments for Pop-Up Window on Setups
  AUTHOR               : jlc
  DATE LAST MODIFIED   : 06/04/92
  CHANGES MADE AND DATE: 05/19/05 - das - pop-up screen for email contacts
******************************************************************************/  
  ON CTRL-O anywhere do:
     if g-ourproc begins "apsv" then do:
        assign g-shipfmno = 0.
        run zsdiapsvpop.p.
     end.
  end.



