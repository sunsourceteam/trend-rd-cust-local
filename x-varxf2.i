&if defined(user_temptable) = 2 &then
/* x-varxf.i */
/*
DESCRIPTION: This program creates a custom quote report fo VA Fab
quotes and orders created from quotes entered thru vaexq.
11/28/2006  TA
*/
{zsapboydef.i}
{zsapboycheck.i}
DEF VAR p-regval  AS CHAR  NO-UNDO.
{g-fabstg.i}
DEFINE BUFFER x-vaspsl FOR vaspsl.
DEFINE BUFFER x-vasps  FOR vasps.
/* zsap switches */
DEF VAR sw-s AS LOGICAL NO-UNDO.
DEF VAR sw-b AS LOGICAL NO-UNDO.
DEF VAR sw-c AS LOGICAL NO-UNDO.
DEF VAR sw-p AS LOGICAL NO-UNDO.
DEF VAR sw-t AS LOGICAL NO-UNDO.
DEF VAR iv-addon AS DEC NO-UNDO.
/* def Ranges */
DEF VAR rQuote    AS CHAR EXTENT 2 NO-UNDO.
DEF VAR rWhse     AS CHAR EXTENT 2 NO-UNDO.
DEF VAR rCust     AS DEC EXTENT 2 NO-UNDO.
DEF VAR rShipTo   AS CHAR EXTENT 2 NO-UNDO.
DEF VAR rVend     AS DEC EXTENT 2 NO-UNDO.
DEF VAR rVendId   AS CHAR EXTENT 2 NO-UNDO.
DEF VAR rRegion   AS CHAR EXTENT 2 NO-UNDO.
DEF VAR rDistrict AS CHAR EXTENT 2 NO-UNDO.
DEF VAR rProd   AS CHAR EXTENT 2 NO-UNDO.
DEF VAR rEnterDt AS DATE EXTENT 2 NO-UNDO.
DEF VAR rStage  AS CHAR FORMAT "xxx" EXTENT 2 NO-UNDO.
DEF VAR rStageI AS INT               EXTENT 2 NO-UNDO.
DEF VAR v-stginx AS INT NO-UNDO.
DEF VAR rSlsIn  AS CHAR EXTENT 2 NO-UNDO.
DEF VAR rSlsOut AS CHAR EXTENT 2 NO-UNDO.
DEF VAR rTaken  AS CHAR EXTENT 2 NO-UNDO.
DEF VAR rCancelDt AS DATE EXTENT 2 NO-UNDO.
DEF VAR rLostBus  AS CHAR EXTENT 2 NO-UNDO.
DEF VAR rUnitType AS CHAR EXTENT 2 NO-UNDO.
/* def Options */
DEF VAR oSort AS CHAR NO-UNDO.
DEF VAR oDetail AS CHAR NO-UNDO.
DEF VAR oList   AS LOGICAL NO-UNDO.
DEF VAR oNotes  AS CHAR NO-UNDO.
DEF VAR oConv   AS char  NO-UNDO.
DEF VAR oCancelled AS LOGICAL NO-UNDO.
DEF VAR oExtract   AS Char   NO-UNDO.
DEF VAR oLostBusiness AS LOGICAL NO-UNDO.
DEF VAR lenSort AS INTEGER NO-UNDO.
DEF VAR iter    AS INTEGER NO-UNDO.
DEF VAR sortfield AS CHAR EXTENT 4 NO-UNDO.
DEF VAR salescode AS CHAR NO-UNDO.
DEF VAR insidecode AS CHAR NO-UNDO.
DEF VAR CurrSlsrep AS CHAR NO-UNDO.
DEF VAR InSlsrep AS CHAR NO-UNDO.
DEF VAR slssort AS CHAR NO-UNDO.
DEF VAR w-vendno LIKE icsw.arpvendno.
/* notes */
DEF VAR showNotes     AS CHAR NO-UNDO.
DEF VAR showNoteType  AS CHAR NO-UNDO.
DEF VAR printNoteln   AS CHAR NO-UNDO.
DEFINE BUFFER catmaster FOR notes.
DEFINE VAR v-reptablefl as logical          NO-UNDO.
DEFINE VAR v-vendorid   AS c FORMAT "x(24)" NO-UNDO.
DEFINE VAR v-parentcode AS c FORMAT "x(24)" NO-UNDO.
DEFINE VAR v-technology AS c FORMAT "x(24)" NO-UNDO.
/* user3 vars */
DEF VAR v-shipto  LIKE oeeh.shipto NO-UNDO.
DEF VAR v-contact AS CHAR NO-UNDO.
DEF VAR v-phone   AS CHAR FORMAT "(999)999-9999" NO-UNDO.
DEF VAR v-email   AS CHAR NO-UNDO.
DEF VAR v-operid  AS CHAR NO-UNDO.
DEF VAR v-hours   AS INT  NO-UNDO.
DEF VAR v-minutes  AS INT  NO-UNDO.
DEF VAR totCost   AS DEC  NO-UNDO.
DEF VAR v-rebFlag AS LOG  NO-UNDO.
DEF VAR totMargin AS DEC  NO-UNDO.
DEF VAR lostbuscode AS CHAR NO-UNDO.
DEF VAR lastvasp  AS RECID NO-UNDO.
DEF VAR dataRegion AS CHAR NO-UNDO.
DEF VAR dataDistrict AS CHAR NO-UNDO.
DEF VAR dataCustName AS CHAR NO-UNDO.
DEF VAR lostbusreason AS CHAR NO-UNDO.
DEF VAR q-pistons   AS l                  NO-UNDO.
DEF VAR q-blocks    AS l                  NO-UNDO.
DEF VAR q-other     AS l                  NO-UNDO.
DEF VAR qty-pistons AS INT FORMAT "zz9"   NO-UNDO.
DEF VAR qty-blocks  AS INT FORMAT "zz9"   NO-UNDO.
DEF VAR qty-other   AS INT FORMAT "zz9"   NO-UNDO.
DEF VAR q-otherdesc AS CHAR FORMAT "x(30)" NO-UNDO.
DEF VAR v-esttst-h AS INT FORMAT "99"     NO-UNDO.
DEF VAR v-esttst-m AS INT FORMAT "99"     NO-UNDO.
DEF VAR v-estasy-h AS INT FORMAT "99"     NO-UNDO.
DEF VAR v-estasy-m AS INT FORMAT "99"     NO-UNDO.
DEF VAR v-esttdn-h AS INT FORMAT "99"     NO-UNDO.
DEF VAR v-esttdn-m AS INT FORMAT "99"     NO-UNDO.
DEF VAR t-csttdn-h AS DEC                 NO-UNDO.
DEF VAR t-csttdn-m AS DEC                 NO-UNDO.
DEF VAR t-cstasy-h AS DEC                 NO-UNDO.
DEF VAR t-cstasy-m AS DEC                 NO-UNDO.
DEF VAR t-csttst-h AS DEC                 NO-UNDO.
DEF VAR t-csttst-m AS DEC                 NO-UNDO.
DEF VAR v-comment  AS CHAR FORMAT "x"     NO-UNDO.
DEF VAR v-prod     AS CHAR FORMAT "x(24)" NO-UNDO.
DEF VAR v-stage    AS CHAR FORMAT "xxx"   NO-UNDO.
DEF VAR v-custno LIKE oeeh.custno FORMAT ">>>>>9999999" NO-UNDO.
DEF VAR v-margin AS DEC NO-UNDO.
DEF VAR v-enterdt AS DATE.
DEF VAR v-quoteno AS CHAR FORMAT "x(13)"  NO-UNDO.
DEF VAR v-error   AS CHAR FORMAT "x(78)"  NO-UNDO.
DEF VAR v-leadtm   AS CHAR FORMAT "x(4)"  NO-UNDO.
DEF VAR v-vendnm   AS CHAR FORMAT "x(24)" NO-UNDO.
DEF VAR v-operinits LIKE g-operinits      NO-UNDO.
DEF VAR audit-vendno LIKE apsv.vendno     NO-UNDO.
DEF VAR non-genuine AS LOG                NO-UNDO.
DEF VAR v-serial   AS CHAR FORMAT "x(20)" NO-UNDO.
DEF VAR v-override AS CHAR FORMAT "x"     NO-UNDO.
DEF VAR v-descrip1 AS CHAR FORMAT "x(24)" NO-UNDO.
DEF VAR v-descrip2 AS CHAR FORMAT "x(24)" NO-UNDO.
DEF VAR v-vendno   LIKE apsv.vendno       NO-UNDO.
DEF VAR v-partno   AS CHAR FORMAT "x(30)" NO-UNDO.
DEF VAR h-transtype AS CHAR FORMAT "x(2)" NO-UNDO.
DEF VAR x-quoteno AS CHAR FORMAT "x(13)" NO-UNDO.
DEF VAR specnsl AS CHAR NO-UNDO.
DEF VAR sumPrice  AS DEC NO-UNDO.
DEF VAR sumCost   AS DEC  NO-UNDO.
DEF VAR HPrice  AS DEC NO-UNDO.
DEF VAR HCost   AS DEC  NO-UNDO.
DEF VAR v-closedStg as integer NO-UNDO.
DEF VAR v-cancelStg as integer NO-UNDO.
DEF VAR v-timeprc LIKE oeelb.price  NO-UNDO.
DEF VAR linePrice LIKE oeelb.price    NO-UNDO.
DEF VAR lineCost  LIKE oeelb.prodcost NO-UNDO.
DEF VAR lineMarg  LIKE oeelb.prodcost NO-UNDO.
DEF VAR v-margimprov LIKE oeelb.prodcost NO-UNDO.
DEF VAR lineqty   AS CHAR FORMAT "x(10)" NO-UNDO.
DEF VAR gtotPrice LIKE oeelb.price NO-UNDO.
DEF VAR gtotCost  LIKE oeelb.prodcost  NO-UNDO.
DEF VAR subtPrice LIKE oeelb.price EXTENT 5 NO-UNDO.
DEF VAR subtCost  LIKE oeelb.prodcost  EXTENT 5 NO-UNDO.
DEF VAR stPrice   LIKE oeelb.price NO-UNDO.
DEF VAR stCost    LIKE oeelb.prodcost NO-UNDO.
DEF VAR mrgPerc   AS DECIMAL FORMAT "99999.9999" NO-UNDO.
DEF VAR cntLines  AS INT EXTENT 5 NO-UNDO.
DEF VAR cntOrders AS INT EXTENT 5 NO-UNDO.
DEF VAR cntConvert AS INT NO-UNDO.
DEF VAR cntCancel  AS INT NO-UNDO.
DEF VAR cntOrder   AS INT NO-UNDO.
DEF VAR cntLine    AS INT NO-UNDO.
DEF VAR cntConvertOrders AS INT EXTENT 5 NO-UNDO.
DEF VAR cntCancelOrders  AS INT EXTENT 5 NO-UNDO.
DEF VAR unasgndtFlag AS LOGICAL NO-UNDO INITIAL YES.
DEF TEMP-TABLE repTable
  FIELD quoteno AS CHAR
  FIELD stage   AS CHAR FORMAT "xxx"
  FIELD unittype AS CHAR
  FIELD vaspid AS RECID
  FIELD vaspslid AS RECID
  FIELD sortfld  AS INT
  FIELD sortfld2 AS INT
  FIELD sortfld3 AS CHAR
  FIELD sortfld4 AS CHAR
  FIELD salescd AS CHAR
  FIELD region  AS CHAR FORMAT "x"
  FIELD rdistrict AS CHAR FORMAT "x(4)"
  FIELD district AS CHAR FORMAT "x(3)"
  FIELD takenby  AS CHAR FORMAT "x(4)"
  FIELD Custno   LIKE arsc.custno FORMAT "zzzzzz999999"
  FIELD slsrepin LIKE smsn.slsrep
  FIELD srepnm LIKE arsc.NAME
  FIELD srepph AS CHAR FORMAT "(999)999-9999"
  FIELD slsrepout   LIKE smsn.slsrep
  FIELD slsrepinst  LIKE smsn.slsrep
  FIELD slsrepoutst LIKE smsn.slsrep
  FIELD lostbusinessst AS CHAR FORMAT "x(2)"
  FIELD lostbusinesscd AS CHAR FORMAT "x(2)"
  FIELD whse           AS CHAR FORMAT "x(4)"
  FIELD enterdt   LIKE oeeh.enterdt
  FIELD senterdt  AS CHAR FORMAT "x(10)"  /* tah 10/26/09 */
  FIELD canceldt  as date
  FIELD custname AS CHAR
  FIELD margmult AS DEC FORMAT "z99.99"
  FIELD marggain AS DEC
  FIELD sumPrice AS DEC
  FIELD sumCost  AS DEC
  FIELD QSumPrice AS DEC
  FIELD QSumCost  AS DEC
  FIELD CvtSumPrice AS DEC
  FIELD CvtSumCost  AS DEC
  FIELD CanSumPrice AS DEC
  FIELD CanSumCost  AS DEC
  FIELD OpnSumPrice AS DEC
  FIELD OpnSumCost  AS DEC
  FIELD cntConvert AS INT
  FIELD cntCancel  AS INT
  FIELD cntOrder   AS INT
  FIELD cntLine    AS INT
  FIELD transtype  LIKE oeeh.transtype
  FIELD specnstype AS CHAR FORMAT "x"
INDEX k-repTable sortfld
  sortfld2
  quoteno
  vaspid
  vaspslid
INDEX k-repTable2 quoteno
INDEX k-repTable3 quoteno vaspid
  sortfld
  sortfld2.
/*    lostbusinessst
vaspslid.   */
DEF VAR v-slsrepoutsort AS LOGICAL NO-UNDO.
DEF VAR v-slsrepinsort  AS LOGICAL NO-UNDO.
DEF VAR v-lostbusiness  AS LOGICAL NO-UNDO.
DEF VAR v-UnitPart      AS character NO-UNDO.
DEF BUFFER b-repTable FOR repTable.
DEF BUFFER h-repTable FOR repTable.
&endif
 
&if defined(user_drpttable) = 2 &then
/* Use to add fields to the report table */
FIELD rptrecid        AS RECID
&endif
 
&IF DEFINED(user_forms) = 2 &THEN
FORM
  "FabQuote"       AT 1
  "Stg"           AT 15
  "TknBy/"         AT 20
  "Cust#"         AT 30
  "ShipTo"        AT 38
  "Cust Name/"    AT 47 
  "SlsRep RepName"        AT 66
  "EnterDt/"       at 95
  "Reference/"     AT 104
  "  Tied Va#"   AT 124
  "TotalSales"    AT 140
  "TotalCost"     AT 156
  "Mgn% "         AT 169
  "Whse"          AT 21
  "UnitType - Part#" at 47
  "CancelDt"      AT 95
  "LostRsnCd"     AT 104 
  "override = *"  AT 140
  "-----------"              AT 1
  "---"                      AT 15
  "----"                     AT 20
  "------------"             AT 25
  "--------"                 AT 38
  "---------------"          AT 47   
  "----"                     AT 66
  "-----------------------"  AT 71
  "--------"                 AT 95
  "---------------------"    AT 104
  "----------"               AT 126
  "-------------"            AT 137
  "--------------"           AT 152
  "-------"                  AT 167
WITH FRAME f-header NO-LABELS
STREAM-IO NO-BOX NO-UNDERLINE WIDTH 178 PAGE-TOP.
FORM HEADER
  "Sctn"            At 3 
  "Seq/"            AT 8
  "Line#"           AT 13
  "Product"         AT 19
  "Description"     AT 44
  "Qty"             AT 75
  "Cost"            at 84
  "ExtCost"         at 94
  "LstMaint"        AT 104
  "Oper     "       AT 115
/*
  "LeadTm"          AT 146
  "Vendor Name"     AT 153
*/
WITH FRAME f-lineheader NO-LABELS
STREAM-IO NO-BOX NO-UNDERLINE WIDTH 178 PAGE-TOP.
FORM
  vasps.sctntype   AT 4
  specnsl          AT 7   FORMAT "x(1)"
  vaspsl.seqno     AT 9   FORMAT "zz9"
  vaspsl.lineno    AT 13  FORMAT ">>9"
  v-prod           AT 19  FORMAT "x(24)"
  v-descrip1       AT 44  FORMAT "x(24)"
  lineqty          AT 70  FORMAT "x(08)"
  lineCost         AT 78  FORMAT ">>>,>>>.99-"
  linePrice        AT 91  FORMAT ">>>,>>>.99-"
  v-enterdt        AT 104  FORMAT "99/99/99"
  v-operid         AT 115  FORMAT "x(4)"
/*
  v-leadtm         AT 148 FORMAT "x(4)"
  v-comment        AT 153 FORMAT "x"
  v-vendnm         AT 154 FORMAT "x(24)"
*/
WITH FRAME f-linedtl NO-LABELS
STREAM-IO NO-BOX NO-UNDERLINE WIDTH 200.
FORM
/*  SKIP(1) */
  v-quoteno           AT 1
  repTable.stage      AT 15
  repTable.takenby    AT 20
  v-custno            AT 25   FORMAT ">>>>>>>>>>>9"
  v-shipto            AT 38   FORMAT "x(8)"
  repTable.custname   AT 47   FORMAT "x(15)"  
  reptable.slsrepout  AT 66   FORMAT "x(4)"
  reptable.srepnm     AT 71   FORMAT "x(23)"
  repTable.enterdt    AT 95
  v-serial            AT 104  FORMAT "x(21)"
  vasp.user2          AT 126  FORMAT "x(10)"
  repTable.sumPrice   AT 137  FORMAT ">,>>>,>>>.99-"
  v-override          AT 150  FORMAT "x"
  repTable.sumCost    AT 153  FORMAT ">,>>>,>>>.99-"
  v-margin            AT 167  FORMAT ">>9.99-"
  repTable.whse       AT 20
  v-unitpart          AT 47   FORMAT "x(48)"  
  repTable.canceldt   AT 96   FORMAT "99/99/99"
  lostbuscode         AT 105  FORMAT "x(3)"
  "============="     AT 137
  "=============="    AT 152
  "======="           AT 167
WITH FRAME f-vasp NO-LABELS /* stream-io */
NO-BOX NO-UNDERLINE WIDTH 178 DOWN.
FORM
 showNotes     AT 5  FORMAT "x(6)"
  showNoteType  AT 13 FORMAT "x(3)"
  printNoteln   AT 17 FORMAT "x(80)"
WITH FRAME f-notes NO-LABELS
STREAM-IO NO-BOX NO-UNDERLINE WIDTH 178 DOWN.
FORM
  sortfield[1]  AT 1   FORMAT "x(10)"
  sortfield[2]  AT 12  FORMAT "x(10)"
  sortfield[3]  AT 22  FORMAT "x(10)"
  sortfield[4]  AT 32  FORMAT "x(10)"
  cntConvert    AT 45  FORMAT "->>>9" LABEL "Conv:"
  cntCancel     AT 55  FORMAT "->>>9" LABEL "Canc:"
  cntOrder      AT 65  FORMAT "->>>9" LABEL "Ord:"
  cntLine       AT 75  FORMAT "->>,>>9" LABEL "Lines:"
  stPrice       AT 126 FORMAT "->>>,>>>,>>>.99"
  stCost        AT 142 FORMAT "->>>,>>>,>>>.99"
  mrgPerc       AT 158 FORMAT "->>9.99"
WITH FRAME f-subtotal NO-LABELS
STREAM-IO NO-BOX NO-UNDERLINE WIDTH 178 DOWN.
FORM
  cntConvertOrders[5]   AT 1   FORMAT "->>>,>>>,>>9" LABEL "Total Converted:"
  cntCancelOrders[5]    AT 20  FORMAT "->>>,>>>,>>9" LABEL "Total Cancelled:"
  cntOrders[5]          AT 40  FORMAT "->>>,>>>,>>9" LABEL "Total Orders:"
  cntLines[5]           AT 60  FORMAT "->>>,>>>,>>9" LABEL "Total Lines:"
  gtotPrice             AT 126 FORMAT "->>>,>>>,>>9.99" LABEL "TotalSales:"
  gtotCost              AT 142 FORMAT "->>>,>>>,>>9.99" LABEL "TotalCost:"
  mrgPerc               AT 158 FORMAT "->>9.99"         LABEL "Margin:"
WITH FRAME f-gtotal
STREAM-IO NO-BOX NO-UNDERLINE WIDTH 178.
FORM
  "   Orders"             AT 40
  "    Lines"             AT 50
  " Cnvt Ord"             AT 60
  "  Can Ord"             AT 70
  "     TotalSales"       AT 135
  "      TotalCost"       AT 151
  "  Mgn% "               AT 167
  "---------"             AT 40
  "---------"             AT 50
  "---------"             AT 60
  "---------"             AT 70
  "---------------"       AT 135
  "---------------"       AT 151
  "-------"               AT 167
  v-final                AT 1
  v-astrik               AT 2   FORMAT "x(5)"
  v-summary-lit2         AT 7   FORMAT "x(27)"
  v-amt7    AT 40  FORMAT ">,>>>,>>9"           no-label
  v-amt6    AT 50  FORMAT ">,>>>,>>9"           no-label
  v-amt3    AT 60  FORMAT ">,>>>,>>9"           no-label
  v-amt4    AT 70  FORMAT ">,>>>,>>9"           no-label
  "Quoted   :"   AT 124
  v-amt8    AT 135 FORMAT ">>>,>>>,>>9.99-"     no-label
  v-amt9    AT 151 FORMAT ">>>,>>>,>>9.99-"     no-label
  v-amt10   AT 167 FORMAT ">>9.99-"             no-label
  "Converted:"   AT 124
  v-amt11   AT 135 FORMAT ">>>,>>>,>>9.99-"     no-label
  v-amt12   AT 151 FORMAT ">>>,>>>,>>9.99-"     no-label
  v-amt13   AT 167 FORMAT ">>9.99-"             no-label
  "LBusiness:"   AT 124
  v-amt14   AT 135 FORMAT ">>>,>>>,>>9.99-"     no-label
  v-amt15   AT 151 FORMAT ">>>,>>>,>>9.99-"     no-label
  v-amt16   AT 167 FORMAT ">>9.99-"             no-label
  "Open     :"   AT 124
  v-amt17   AT 135 FORMAT ">>>,>>>,>>9.99-"     no-label
  v-amt18   AT 151 FORMAT ">>>,>>>,>>9.99-"     no-label
  v-amt19   AT 167 FORMAT ">>9.99-"             no-label
  "~015"   AT 178
WITH FRAME f-tot WIDTH 178 NO-UNDERLINE
NO-BOX NO-LABELS.
FORM
  " "              AT 1
  "~015"           AT 178
WITH FRAME f-skip WIDTH 178 NO-BOX NO-LABELS.
&endif
 
&if defined(user_extractrun) = 2 &then
DO iter = 1 TO 5:
  subtPrice[iter] = 0.
  subtCost[iter] = 0.
  cntLines[iter] = 0.
  cntOrders[iter] = 0.
  cntConvertOrders[iter] = 0.
  cntCancelOrders[iter]  = 0.
END.
{p-fabstg.i}
find t-stages where t-stages.shortname = "CLS" no-lock no-error.
if avail t-stages then
  assign v-closedStg  = t-stages.stage.
else
  assign v-closedStg  = 900.
find t-stages where t-stages.shortname = "Can" no-lock no-error.
if avail t-stages then
  assign v-cancelStg  = t-stages.stage.
else
  assign v-cancelStg  = 999.
RUN sapb_vars.
RUN extract_data.
if oExtract <> "" then do:
  output stream expt to value("/usr/tmp/" + oExtract + ".ixk").
  assign export_rec =
     "FabQuote"
     + v-del +
     "TiedVano"
      + v-del +
      "Stage"
      + v-del +
      "Whse"
      +  v-del +
      "Takenby"
      + v-del +
      "CustNo"
      + v-del +
      "ShipTo"
      + v-del +
      "CustName"
      + v-del +
      "SlsRepOut"  
      + v-del +
      "SlsRepName"   
      + v-del +
      "EnterDt"
      + v-del +
      "CancelDt"
      + v-del +
      "LostBusCd"
      + v-del +
      "LostBusReason"
      + v-del +
      "Reference"    
      + v-del +
      "UnitType"
      + v-del +
      "PartNumber"
      + v-del +
      "PriceOverFl"
      + v-del +
      "Price"
      + v-del +
      "Cost"
      + v-del +
      "Margin" + chr(13).
   put stream expt unformatted export_rec.
 
end.  
&endif
 
&if defined(user_exportstatDWheaders) = 2 &then
&endif
 
&if defined(user_exportstatheaders) = 2 &then
/* This is the headers for the information you want to export
the control break headers are taken care of this is just for
the totals you want printed */
/*
ASSIGN export_rec = export_rec + v-del +
  "Period 1" + v-del +
  "Period 2" + v-del   +
  "Period 3" + v-del   +
  "Period 4" + v-del   +
  "Period 5" + v-del   +
  "Period 6 " + v-del   +
  " " + v-del +
  " ".
*/
&endif
 
&if defined(user_foreach) = 2 &then
FOR EACH repTable WHERE 
         repTable.vaspslid = 0 NO-LOCK:
  
  
  &endif
  
  &if defined(user_drptassign) = 2 &then
  /* If you add fields to the dprt table this is where you assign them */
  drpt.rptrecid = RECID(repTable)
  
  
  &ENDIF
  
  &IF DEFINED(user_B4endloop) = 2 &THEN
  /* this is before the end of the for each loop to feed the data BUILDREC */
  
  
  &endif
  
  &if defined(user_viewheadframes) = 2 &then
  /* Since frames are unique to each program you need to put your
  views and hides here with your names */
  IF NOT p-exportl and oExtract = "" THEN DO:
    IF NOT x-stream THEN DO:
      HIDE FRAME f-header.
    END.
    ELSE DO:
      HIDE STREAM xpcd FRAME f-header.
    END.
    
  END.
  IF NOT p-exportl  and oExtract = "" THEN DO:
    IF NOT x-stream THEN DO:
      PAGE.
      IF oDetail <> "s" THEN
      VIEW FRAME f-header.
    END.
    ELSE DO:
      PAGE STREAM xpcd.
      IF oDetail > "s" THEN
      VIEW STREAM xpcd FRAME f-header.
    END.
    
  END.
  
  
  &endif
  
  &if defined(user_drptloop) = 2 &then
  
  
  &endif
  
  &if defined(user_detailprint) = 2 &then
  /* Comment this out if you are not using detail 'X' option  */
  IF p-detail = "d" AND oDetail <> "s" AND
    ENTRY(u-entitys,drpt.sortmystery,v-delim) <>
    FILL("~~",LENGTH(ENTRY(u-entitys,drpt.sortmystery,v-delim))) THEN DO:
    /* this code skips the all other lines for printing detail */
    
    /* next line would be your detail print procedure */
    RUN DetailPrint (INPUT  drpt.rptrecid ).
  END.
  
  
  &endif
  
  &if defined(user_finalprint) = 2 &then
  ASSIGN t-amounts5[u-entitys + 1] =
        (IF t-amounts1[u-entitys + 1] = 0 THEN 0
         ELSE IF ((t-amounts1[u-entitys + 1] - t-amounts2[u-entitys + 1]) /
                   t-amounts1[u-entitys + 1]) * 100 < -999 THEN -999
         ELSE IF ((t-amounts1[u-entitys + 1] - t-amounts2[u-entitys + 1]) /
                   t-amounts1[u-entitys + 1]) * 100 > 999 THEN 999
         ELSE ((t-amounts1[u-entitys + 1] - t-amounts2[u-entitys + 1]) /
                t-amounts1[u-entitys + 1]) * 100).
  ASSIGN t-amounts10[u-entitys + 1] =
        (IF t-amounts8[u-entitys + 1] = 0 THEN 0
         ELSE IF ((t-amounts8[u-entitys + 1] - t-amounts9[u-entitys + 1]) /
                   t-amounts8[u-entitys + 1]) * 100 < -999 THEN -999
         ELSE IF ((t-amounts8[u-entitys + 1] - t-amounts9[u-entitys + 1]) /
                   t-amounts8[u-entitys + 1]) * 100 > 999 THEN 999
         ELSE ((t-amounts8[u-entitys + 1] - t-amounts9[u-entitys + 1]) /
                t-amounts8[u-entitys + 1]) * 100).
  ASSIGN t-amounts13[u-entitys + 1] =
        (IF t-amounts11[u-entitys + 1] = 0 THEN 0
         ELSE IF ((t-amounts11[u-entitys + 1] - t-amounts12[u-entitys + 1]) /
                   t-amounts11[u-entitys + 1]) * 100 < -999 THEN -999
         ELSE IF ((t-amounts11[u-entitys + 1] - t-amounts12[u-entitys + 1]) /
                   t-amounts11[u-entitys + 1]) * 100 > 999 THEN 999
         ELSE ((t-amounts11[u-entitys + 1] - t-amounts12[u-entitys + 1]) /
                t-amounts11[u-entitys + 1]) * 100).
  ASSIGN t-amounts16[u-entitys + 1] =
        (IF t-amounts14[u-entitys + 1] = 0 THEN 0
         ELSE IF ((t-amounts14[u-entitys + 1] - t-amounts15[u-entitys + 1]) /
                   t-amounts14[u-entitys + 1]) * 100 < -999 THEN -999
         ELSE IF ((t-amounts14[u-entitys + 1] - t-amounts15[u-entitys + 1]) /
                   t-amounts14[u-entitys + 1]) * 100 > 999 THEN 999
         ELSE ((t-amounts14[u-entitys + 1] - t-amounts15[u-entitys + 1]) /
                t-amounts14[u-entitys + 1]) * 100).
  ASSIGN t-amounts17[u-entitys + 1] =
        (t-amounts8[u-entitys + 1] - t-amounts11[u-entitys + 1]).
  ASSIGN t-amounts18[u-entitys + 1] =
        (t-amounts9[u-entitys + 1] - t-amounts12[u-entitys + 1]).
  ASSIGN t-amounts19[u-entitys + 1] =
      (IF t-amounts17[u-entitys + 1] = 0 THEN 0
       ELSE IF ((t-amounts17[u-entitys + 1] - t-amounts18[u-entitys + 1]) /
                 t-amounts17[u-entitys + 1]) * 100 < -999 THEN -999
       ELSE IF ((t-amounts17[u-entitys + 1] - t-amounts18[u-entitys + 1]) /
                 t-amounts17[u-entitys + 1]) * 100 > 999 THEN 999
       ELSE ((t-amounts17[u-entitys + 1] - t-amounts18[u-entitys + 1]) /
              t-amounts17[u-entitys + 1]) * 100).
  
  IF NOT p-exportl and oExtract = "" THEN DO:
    IF NOT x-stream THEN
    DO:
      DISPLAY
        v-final
        v-astrik
        v-summary-lit2
        t-amounts3[u-entitys + 1]   @ v-amt3
        t-amounts4[u-entitys + 1]   @ v-amt4
        t-amounts6[u-entitys + 1]   @ v-amt6
        t-amounts7[u-entitys + 1]   @ v-amt7
        t-amounts8[u-entitys + 1]   @ v-amt8
        t-amounts9[u-entitys + 1]   @ v-amt9
        t-amounts10[u-entitys + 1]   @ v-amt10
        t-amounts11[u-entitys + 1]   @ v-amt11
        t-amounts12[u-entitys + 1]   @ v-amt12
        t-amounts13[u-entitys + 1]   @ v-amt13
        t-amounts14[u-entitys + 1]   @ v-amt14
        t-amounts15[u-entitys + 1]   @ v-amt15
        t-amounts16[u-entitys + 1]   @ v-amt16
        t-amounts17[u-entitys + 1]   @ v-amt17
        t-amounts18[u-entitys + 1]   @ v-amt18
        t-amounts19[u-entitys + 1]   @ v-amt19
      WITH FRAME f-tot.
      DOWN WITH FRAME f-tot.
    END.
    ELSE
    DO:
      DISPLAY STREAM xpcd
        v-final
        v-astrik
        v-summary-lit2
        t-amounts3[u-entitys + 1]   @ v-amt3
        t-amounts4[u-entitys + 1]   @ v-amt4
        t-amounts6[u-entitys + 1]   @ v-amt6
        t-amounts7[u-entitys + 1]   @ v-amt7
        t-amounts8[u-entitys + 1]   @ v-amt8
        t-amounts9[u-entitys + 1]   @ v-amt9
        t-amounts10[u-entitys + 1]   @ v-amt10
        t-amounts11[u-entitys + 1]   @ v-amt11
        t-amounts12[u-entitys + 1]   @ v-amt12
        t-amounts13[u-entitys + 1]   @ v-amt13
        t-amounts14[u-entitys + 1]   @ v-amt14
        t-amounts15[u-entitys + 1]   @ v-amt15
        t-amounts16[u-entitys + 1]   @ v-amt16
        t-amounts17[u-entitys + 1]   @ v-amt17
        t-amounts18[u-entitys + 1]   @ v-amt18
        t-amounts19[u-entitys + 1]   @ v-amt19
      WITH FRAME f-tot.
      DOWN STREAM xpcd WITH FRAME f-tot.
    END.
  END.
  ELSE IF p-exportl THEN
  DO:
    ASSIGN export_rec =  v-astrik.
    DO v-inx4 = 1 TO u-entitys:
      IF v-totalup[v-inx4] = "n" THEN
        NEXT.
      IF v-inx4 = 1 THEN
        v-summary-lit2 = "Final".
      ELSE
        v-summary-lit2 = "".
      IF v-cells[v-inx4]= "2" THEN DO:
        IF v-inx4 > 0 THEN
          export_rec = export_rec + v-del.
        ASSIGN export_rec = export_rec + v-summary-lit2 + v-del + " ".
        
      END.
      ELSE DO:
        IF v-inx4 > 0 THEN
          export_rec = export_rec + v-del.
        ASSIGN export_rec = export_rec +  v-summary-lit2.
      END.
    END.
    ASSIGN export_rec = 
      export_rec + v-del +
      STRING(t-amounts3[u-entitys + 1],"->>>>>>>>9") + v-del +
      STRING(t-amounts4[u-entitys + 1],"->>>>>>>>9") + v-del +
      STRING(t-amounts6[u-entitys + 1],"->>>>>>>>9") + v-del +
      STRING(t-amounts7[u-entitys + 1],"->>>>>>>>9") + v-del +
      STRING(t-amounts8[u-entitys + 1],"->>>>>>>>9") + v-del +
      STRING(t-amounts9[u-entitys + 1],"->>>>>>>>9") + v-del +
      STRING(t-amounts10[u-entitys + 1],"->>>>>>>>9") + v-del +
      STRING(t-amounts11[u-entitys + 1],"->>>>>>>>9") + v-del +
      STRING(t-amounts12[u-entitys + 1],"->>>>>>>>9") + v-del +
      STRING(t-amounts13[u-entitys + 1],"->>>>>>>>9") + v-del +
      STRING(t-amounts14[u-entitys + 1],"->>>>>>>>9") + v-del +
      STRING(t-amounts15[u-entitys + 1],"->>>>>>>>9") + v-del +
      STRING(t-amounts16[u-entitys + 1],"->>>>>>>>9") + v-del +
      STRING(t-amounts17[u-entitys + 1],"->>>>>>>>9") + v-del +
      STRING(t-amounts18[u-entitys + 1],"->>>>>>>>9") + v-del +
      STRING(t-amounts19[u-entitys + 1],"->>>>>>>>9").
    
    ASSIGN export_rec = export_rec + CHR(13).
    PUT STREAM expt UNFORMATTED export_rec.
  END.
  
  
  &endif
  
  &if defined(user_summaryframeprint) = 2 &then
  ASSIGN t-amounts5[v-inx2] =
        (IF t-amounts1[v-inx2] = 0 THEN 0
         ELSE IF ((t-amounts1[v-inx2] - t-amounts2[v-inx2]) /
                   t-amounts1[v-inx2]) * 100 < -999 THEN -999
         ELSE IF ((t-amounts1[v-inx2] - t-amounts2[v-inx2]) /
                   t-amounts1[v-inx2]) * 100 > 999 THEN 999
         ELSE ((t-amounts1[v-inx2] - t-amounts2[v-inx2]) /
                t-amounts1[v-inx2]) * 100).
  ASSIGN t-amounts10[v-inx2] =
        (IF t-amounts8[v-inx2] = 0 THEN 0
         ELSE IF ((t-amounts8[v-inx2] - t-amounts9[v-inx2]) /
                   t-amounts8[v-inx2]) * 100 < -999 THEN -999
         ELSE IF ((t-amounts8[v-inx2] - t-amounts9[v-inx2]) /
                   t-amounts8[v-inx2]) * 100 > 999 THEN 999
         ELSE ((t-amounts8[v-inx2] - t-amounts9[v-inx2]) /
                t-amounts8[v-inx2]) * 100).
  
  ASSIGN t-amounts13[v-inx2] =
        (IF t-amounts11[v-inx2] = 0 THEN 0
         ELSE IF ((t-amounts11[v-inx2] - t-amounts12[v-inx2]) /
                   t-amounts11[v-inx2]) * 100 < -999 THEN -999
         ELSE IF ((t-amounts11[v-inx2] - t-amounts12[v-inx2]) /
                   t-amounts11[v-inx2]) * 100 > 999 THEN 999
         ELSE ((t-amounts11[v-inx2] - t-amounts12[v-inx2]) /
                t-amounts11[v-inx2]) * 100).
  ASSIGN t-amounts16[v-inx2] =
        (IF t-amounts14[v-inx2] = 0 THEN 0
         ELSE IF ((t-amounts14[v-inx2] - t-amounts15[v-inx2]) /
                   t-amounts14[v-inx2]) * 100 < -999 THEN -999
         ELSE IF ((t-amounts14[v-inx2] - t-amounts15[v-inx2]) /
                   t-amounts14[v-inx2]) * 100 > 999 THEN 999
         ELSE ((t-amounts14[v-inx2] - t-amounts15[v-inx2]) /
                t-amounts14[v-inx2]) * 100).
  ASSIGN t-amounts19[v-inx2] =
        (IF t-amounts17[v-inx2] = 0 THEN 0
         ELSE IF ((t-amounts17[v-inx2] - t-amounts18[v-inx2]) /
                   t-amounts17[v-inx2]) * 100 < -999 THEN -999
         ELSE IF ((t-amounts17[v-inx2] - t-amounts18[v-inx2]) /
                   t-amounts17[v-inx2]) * 100 > 999 THEN 999
         ELSE ((t-amounts17[v-inx2] - t-amounts18[v-inx2]) /
                t-amounts17[v-inx2]) * 100).
  
  IF NOT x-stream and oExtract = "" THEN DO:
    DISPLAY
      v-final
      v-astrik
      v-summary-lit2
      t-amounts3[v-inx2]   @ v-amt3
      t-amounts4[v-inx2]   @ v-amt4
      t-amounts6[v-inx2]   @ v-amt6
      t-amounts7[v-inx2]   @ v-amt7
      t-amounts8[v-inx2]   @ v-amt8
      t-amounts9[v-inx2]   @ v-amt9
      t-amounts10[v-inx2]   @ v-amt10
      t-amounts11[v-inx2]   @ v-amt11
      t-amounts12[v-inx2]   @ v-amt12
      t-amounts13[v-inx2]   @ v-amt13
      t-amounts14[v-inx2]   @ v-amt14
      t-amounts15[v-inx2]   @ v-amt15
      t-amounts16[v-inx2]   @ v-amt16
      t-amounts17[v-inx2]   @ v-amt17
      t-amounts18[v-inx2]   @ v-amt18
      t-amounts19[v-inx2]   @ v-amt19
    
    WITH FRAME f-tot.
    DOWN WITH FRAME f-tot.
  END.
  ELSE IF oExtract = "" THEN
  DO:
    DISPLAY STREAM xpcd
      v-final
      v-astrik
      v-summary-lit2
      t-amounts3[v-inx2]   @ v-amt3
      t-amounts4[v-inx2]   @ v-amt4
      t-amounts6[v-inx2]   @ v-amt6
      t-amounts7[v-inx2]   @ v-amt7
      t-amounts8[v-inx2]   @ v-amt8
      t-amounts9[v-inx2]   @ v-amt9
      t-amounts10[v-inx2]   @ v-amt10
      t-amounts11[v-inx2]   @ v-amt11
      t-amounts12[v-inx2]   @ v-amt12
      t-amounts13[v-inx2]   @ v-amt13
      t-amounts14[v-inx2]   @ v-amt14
      t-amounts15[v-inx2]   @ v-amt15
      t-amounts16[v-inx2]   @ v-amt16
      t-amounts17[v-inx2]   @ v-amt17
      t-amounts18[v-inx2]   @ v-amt18
      t-amounts19[v-inx2]   @ v-amt19
    
    
    WITH FRAME f-tot.
    DOWN STREAM xpcd WITH FRAME f-tot.
  END.
  
  
  &endif
  
  &if defined(user_summaryputexport) = 2 &then
  
  ASSIGN export_rec = v-astrik + v-del + export_rec.
  ASSIGN export_rec = 
    export_rec + v-del +
    STRING(t-amounts3[v-inx2],"->>>>>>>>9") + v-del +
    STRING(t-amounts4[v-inx2],"->>>>>>>>9") + v-del +
    STRING(t-amounts6[v-inx2],"->>>>>>>>9") + v-del +
    STRING(t-amounts7[v-inx2],"->>>>>>>>9") + v-del +
    STRING(t-amounts8[v-inx2],"->>>>>>>>9") + v-del +
    STRING(t-amounts9[v-inx2],"->>>>>>>>9") + v-del +
    STRING(t-amounts10[v-inx2],"->>>>>>>>9") + v-del +
    STRING(t-amounts11[v-inx2],"->>>>>>>>9") + v-del +
    STRING(t-amounts12[v-inx2],"->>>>>>>>9") + v-del +
    STRING(t-amounts13[v-inx2],"->>>>>>>>9") + v-del +
    STRING(t-amounts14[v-inx2],"->>>>>>>>9") + v-del +
    STRING(t-amounts15[v-inx2],"->>>>>>>>9") + v-del +
    STRING(t-amounts16[v-inx2],"->>>>>>>>9") + v-del +
    STRING(t-amounts17[v-inx2],"->>>>>>>>9") + v-del +
    STRING(t-amounts18[v-inx2],"->>>>>>>>9") + v-del +
    STRING(t-amounts19[v-inx2],"->>>>>>>>9").
  
  
  
  &endif
  
  &if defined(user_detailputexport) = 2 &then
  
  
  &endif
  
  &if defined(user_procedures) = 2 &then
  PROCEDURE sapb_vars:
    /* set Ranges */
    rQuote[1]       = 
      IF sapb.rangebeg[1] = "" THEN
        "0000-0000001"
      ELSE
        sapb.rangebeg[1].
    rQuote[2]       = 
      IF sapb.rangeend[1] = "" OR
        sapb.rangeend[1] BEGINS "~~~~~~~~~~~~~~~~" THEN
          "9999-9999999"
      ELSE
        sapb.rangeend[1].
    ASSIGN
      rCust[1]        = DECIMAL(sapb.rangebeg[2])
      rCust[2]        = DECIMAL(sapb.rangeend[2])
      rShipTo[1]      = sapb.rangebeg[3]
      rShipTo[2]      = sapb.rangeend[3]
      rUnitType[1]    = sapb.rangebeg[4]
      rUnitType[2]    = sapb.rangeend[4]
      rEnterDt[1]     = DATE(sapb.rangebeg[5])
      rEnterDt[2]     = DATE(sapb.rangeend[5])
      rStage[1]       = sapb.rangebeg[6]
      rStage[2]       = sapb.rangeend[6]
      rTaken[1]       = sapb.rangebeg[7]
      rTaken[2]       = sapb.rangeend[7]
      rCancelDt[1]    = DATE(sapb.rangebeg[8])
      rCancelDt[2]    = DATE(sapb.rangeend[8])
      rLostBus[1]     = sapb.rangebeg[9]
      rLostBus[2]     = sapb.rangeend[9]
      rWhse[1]        = sapb.rangebeg[10]
      rWhse[2]        = sapb.rangeend[10]
      rSlsOut[1]      = sapb.rangebeg[11]
      rSlsOut[2]      = sapb.rangeend[11]
      rRegion[1]      = sapb.rangebeg[12]
      rRegion[2]      = sapb.rangeend[12]
      rDistrict[1]    = sapb.rangebeg[13]
      rDistrict[2]    = sapb.rangeend[13]
      rStagei[1] = 0
      rStagei[2] = 0.
    IF rStage[1] = " " THEN DO:
      FIND FIRST t-stages USE-INDEX k-stg NO-LOCK NO-ERROR.
      ASSIGN rStageI[1] = IF NOT avail t-stages THEN 0 ELSE t-stages.stage.
    END.
    ELSE
      FOR EACH t-stages NO-LOCK:
        IF rStage[1] = t-stages.shortname THEN
        ASSIGN rStageI[1] = t-stages.stage.
      END.
    IF rStage[2] GE "~~~~~~" THEN DO:
      FIND LAST t-stages USE-INDEX k-stg NO-LOCK NO-ERROR.
      ASSIGN rStageI[2] = IF NOT avail t-stages THEN 0 ELSE t-stages.stage.
    END.
    ELSE
      FOR EACH t-stages NO-LOCK:
        IF rStage[2] = t-stages.shortname THEN
      ASSIGN rStageI[2] = t-stages.stage.
      END.
    IF rStageI[1] = 0 OR
       rStageI[2] = 0 THEN DO:
      MESSAGE "Invalid stages(s) entered in ranges".
      MESSAGE "Valid stages are: Rcv,Sch,Tdn,Cst,Qtd,Apr,Por,Pin,Srp".
      MESSAGE "                  Asy,Tst,Pnt,Inf,Can,Cls,Scp".
      RETURN.
    END.
    IF rStageI[1] > rStageI[2] THEN DO:
      MESSAGE "Begin Stage cannot exceed the to stage".
      MESSAGE "Valid stages are: Rfq,Asn,Qap,Qte,Apr,Ack,Dwg,Sch,Asy".
      MESSAGE "                  Tst,Crt,Doc,Cls,Can".
      RETURN.
    END.
    
    
    
    IF NUM-ENTRIES(rQuote[2],"-") = 1 THEN DO:
      ASSIGN rQuote[2] = rQuote[2] + "-9999999".
    END.
    ELSE
    IF NUM-ENTRIES(rQuote[2],"-") = 2 THEN DO:
      ASSIGN rQuote[2] = ENTRY (1,rQuote[2],"-") + "-" +
        STRING(INT(ENTRY(2,rquote[2],"-")),"9999999").
    END.
    IF NUM-ENTRIES(rQuote[1],"-") = 1 THEN DO:
      ASSIGN rQuote[1] = rQuote[1] + "-0000000".
    END.
    ELSE
    IF NUM-ENTRIES(rQuote[1],"-") = 2 THEN DO:
      ASSIGN rQuote[1] = ENTRY (1,rQuote[1],"-") + "-" +
        STRING(INT(ENTRY(2,rquote[1],"-")),"9999999").
    END.
    /** FIX001 - TA  **/
    /** entered date **/
    IF sapb.rangebeg[5] NE "" THEN DO:
      ASSIGN v-datein = sapb.rangebeg[5].
      {p-rptdt.i}
      IF STRING(v-dateout) = v-lowdt THEN
        rEnterDt[1] = 01/01/1900.
      ELSE
        ASSIGN rEnterDt[1] = v-dateout.
    END.
    ELSE
      ASSIGN rEnterDt[1] = 01/01/1900.
    IF sapb.rangeend[5] NE "" THEN DO:
      ASSIGN v-datein = sapb.rangeend[5].
      {p-rptdt.i}
      IF STRING(v-dateout) = v-lowdt THEN
        ASSIGN rEnterDt[2] = 12/31/2049.
      ELSE
        ASSIGN rEnterDt[2] = v-dateout.
    END.
    ELSE
      ASSIGN rEnterDt[2] = 12/31/2049.
    /** cancel date **/
    IF sapb.rangebeg[8] NE "" THEN DO:
      ASSIGN v-datein = sapb.rangebeg[8].
      {p-rptdt.i}
      IF STRING(v-dateout) = v-lowdt THEN
        ASSIGN rCancelDt[1] = 01/01/1900.
      ELSE
        ASSIGN rCancelDt[1] = v-dateout.
    END.
    ELSE
      ASSIGN rCancelDt[1] = 01/01/1900.
    IF sapb.rangeend[8] NE "" THEN DO:
      ASSIGN v-datein = sapb.rangeend[8].
      {p-rptdt.i}
      IF STRING(v-dateout) = v-lowdt THEN
        ASSIGN rCancelDt[2] = 12/31/2049.
      ELSE
        ASSIGN rCancelDt[2] = v-dateout.
    END.
    ELSE
      ASSIGN rCancelDt[2] = 12/31/2049.
    /** FIX001 - TA  **/
    IF rCancelDt[1] = 12/31/49 AND rCancelDt[2] = 12/31/49 THEN DO:
      ASSIGN
        unasgndtFlag = YES 
        rCancelDt[1] = 01/01/1950.
    END.
    ELSE
      ASSIGN unasgndtFlag = NO.
    /* set Options */
    ASSIGN
      oSort          = sapb.optvalue[1]
      oDetail        = sapb.optvalue[2]
      oList          = IF sapb.optvalue[3] = "yes" THEN YES ELSE NO
      oNotes         = sapb.optvalue[4]
      oConv          = sapb.optvalue[5] 
      oCancelled     = IF sapb.optvalue[6] = "yes" THEN YES ELSE NO
      oExtract       = sapb.optvalue[7]
      p-detail = /* if oDetail = "s" then
                     "s"
                    else */
                "d"
      lenSort = LENGTH(oSort).
      ASSIGN
        zzl_begwhse = rWhse[1]
        zzl_endwhse = rWhse[2]
        zzl_begcust = rCust[1]
        zzl_endcust = rCust[2]
        zzl_begrep  = rSlsOut[1]
        zzl_endrep  = rSlsOut[2]
        zzl_begshipto = rShipTo[1]
        zzl_endshipto = rShipTo[2]
        
     
     
        zel_begwhse = rWhse[1]
        zel_endwhse = rWhse[2]
        zel_begcust = rCust[1]
        zel_endcust = rCust[2]
        zel_begrep  = rSlsOut[1]
        zel_endrep  = rSlsOut[2]
        zel_begshipto = rShipTo[1]
        zel_endshipto = rShipTo[2].
      
    IF oList THEN DO:
      /* stuff for zsapboycheck.i */
      /* do I have to set zel_ too? */
      ASSIGN
        zzl_begwhse = rWhse[1]
        zzl_endwhse = rWhse[2]
        zzl_begcust = rCust[1]
        zzl_endcust = rCust[2]
        zzl_begrep  = rSlsOut[1]
        zzl_endrep  = rSlsOut[2]
        zzl_begshipto = rShipTo[1]
        zzl_endshipto = rShipTo[2]
     
        zel_begwhse = rWhse[1]
        zel_endwhse = rWhse[2]
        zel_begcust = rCust[1]
        zel_endcust = rCust[2]
        zel_begrep  = rSlsOut[1]
        zel_endrep  = rSlsOut[2]
        zel_begshipto = rShipTo[1]
        zel_endshipto = rShipTo[2].
      
      {zsapboyload.i}
      ASSIGN
        rWhse[1]    = zel_begwhse
        rWhse[2]    = zel_endwhse
        rCust[1]    = zel_begcust
        rCust[2]    = zel_endcust
        rSlsOut[1]  = zel_begrep
        rSlsOut[2]  = zel_endrep
        rShipTo[1]    = zel_begshipto
        rShipTo[2]    = zel_endshipto.
      ASSIGN
        zelection_matrix [2] = (IF zelection_matrix[2] > 1 THEN  /* Cust */
                                  zelection_matrix[2]
                                ELSE
                                  1)
        zelection_matrix [1] = (IF zelection_matrix[1] > 1 THEN  /* Rep */
                                zelection_matrix[1]
                              ELSE
                                1)
        zelection_matrix [7] = (IF zelection_matrix[7] > 1 THEN  /* Vend */
                                zelection_matrix[7]
                              ELSE
                                1)
        zelection_matrix [6] = (IF zelection_matrix[6] > 1 THEN  /* Whse */
                                zelection_matrix[6]
                              ELSE
                                1)
        zelection_matrix [3] = (IF zelection_matrix[3] > 1 THEN  /* Cat  */
                                zelection_matrix[3]
                              ELSE
                                1)
        zelection_matrix [8] = (IF zelection_matrix[8] > 1 THEN  /* VName */
                                zelection_matrix[8]
                              ELSE
                                1)
        zelection_matrix [9] = (IF zelection_matrix[9] > 1 THEN  /* VPArent */
                                zelection_matrix[9]
                              ELSE
                                1)
        zelection_matrix [11] = (IF zelection_matrix[11] > 1 THEN /* Product */
                                  zelection_matrix[11]
                               ELSE
                                1)
      
        zelection_matrix [12] = (IF zelection_matrix[12] > 1 THEN /* shippto */
                                 zelection_matrix[12]
                               ELSE
                                1).
      
      
    END. /* if oList */
    
    RUN formatoptions.
  END.
/* ------------------------------------------------------------------------- */
  PROCEDURE formatoptions:
/* ------------------------------------------------------------------------- */
    define buffer fb-sapb for sapb.
    /** FIX001 - TA  **/
    ASSIGN p-portrait = FALSE.
    /* if false then the report will print in LANDSCAPE 178 character */
    
    IF sapb.optvalue[1] > "0" AND sapb.optvalue[1] NE "99" THEN DO:
      ASSIGN p-optiontype = " ".
      FIND notes WHERE notes.cono = g-cono AND
           notes.notestype = "zz" AND
           notes.primarykey = "varxf" AND  /* tbxr program */
           notes.secondarykey =
            sapb.optvalue[1]  NO-LOCK NO-ERROR.
      IF NOT avail notes THEN
      DO:
        DISPLAY "Format is not valid cannot process request".
        ASSIGN 
          p-optiontype = "c"
          p-sorttype   = ">,"
          p-totaltype  = "S"
          p-summcounts = "a".
      END.
      ELSE DO:
        ASSIGN 
          p-optiontype = notes.noteln[1]
          p-sorttype   = notes.noteln[2]
          p-totaltype  = notes.noteln[3]
          p-summcounts = notes.noteln[4]
          p-export     = "    "
          p-register   = notes.noteln[5]
          p-registerex = notes.noteln[6].
      END.
    do for fb-sapb:
    find fb-sapb where recid(fb-sapb) = recid(sapb).
    
    
    assign fb-sapb.user5 = p-optiontype + "~011" +
                           p-sorttype   + "~011" +
                           p-totaltype + "~011" +
                           p-summcounts + "~011" +
                           p-register  + "~011" +
                           p-registerex + "~011" .
    find sapb where recid(sapb) = recid(fb-sapb) no-lock no-error.
    end.
 
    END.
    ELSE
    IF sapb.optvalue[1] = "99" THEN
    DO:
      ASSIGN 
        p-register   = ""
        p-registerex = "".
      RUN reportopts(INPUT sapb.user5,
                     INPUT-OUTPUT p-optiontype,
                     INPUT-OUTPUT p-sorttype,
                     INPUT-OUTPUT p-totaltype,
                     INPUT-OUTPUT p-summcounts,
                     INPUT-OUTPUT p-register,
                     INPUT-OUTPUT p-regval).
    END.
    IF p-optiontype = "" THEN
      ASSIGN 
        p-optiontype = "c"
        p-sorttype   = ">,"
        p-totaltype  = "S"
        p-summcounts = "a".
    
    RUN Print_reportopts (INPUT RECID (sapb),
                          INPUT g-cono).
  END.
/* ------------------------------------------------------------------------- */
  PROCEDURE extract_data:
/* ------------------------------------------------------------------------- */
     ASSIGN
      sumPrice = 0
      sumCost  = 0.
    
    /* Main, select 1 ranges */
    FOR EACH zsdivasp USE-INDEX k-custno WHERE
             zsdivasp.cono = g-cono AND
             zsdivasp.custno >= rCust[1]      AND
             zsdivasp.custno <= rCust[2]      AND
             zsdivasp.repairno >= ENTRY (1,rquote[1],"-") + "-" +
              STRING(INT(ENTRY(2,rquote[1],"-")),"9999999")   AND
             zsdivasp.repairno <= ENTRY (1,rquote[2],"-") + "-" +
              STRING(INT(ENTRY(2,rquote[2],"-")),"9999999")   AND
             zsdivasp.mfgname >= rUnitType[1] AND
             zsdivasp.mfgname <= rUnitType[2] AND
             zsdivasp.shipto  >= rShipTo[1]  AND
             zsdivasp.shipto  <= rShipTo[2]  AND
             zsdivasp.xuser3  >= rLostBus[1] AND
             zsdivasp.xuser3  <= rLostBus[2] AND
             zsdivasp.enterdt >= rEnterDt[1] AND
             zsdivasp.enterdt <= rEnterDt[2] AND
             zsdivasp.whse    >= rWhse[1]    AND
             zsdivasp.whse    <= rWhse[2]    AND
             zsdivasp.takenby >= rTaken[1]   AND
             zsdivasp.takenby <= rTaken[2]   AND
             zsdivasp.rectype = "fb"
    NO-LOCK:
      FIND vasp WHERE 
           vasp.cono = g-cono AND
           vasp.shipprod = zsdivasp.repairno
      NO-LOCK NO-ERROR.
      IF NOT avail vasp THEN 
        NEXT.
      ASSIGN 
        cntConvert = 0
        cntCancel  = 0
        cntOrder   = 0
        cntLine    = 0.
      IF INT(zsdivasp.stagecd) = 0 THEN DO:
        NEXT.
      END.
      ELSE DO:
        FIND t-stages WHERE 
             t-stages.stage = zsdivasp.stagecd NO-LOCK NO-ERROR.
        IF NOT avail t-stages THEN
          FIND FIRST t-stages WHERE t-stages.stage > 0 NO-LOCK NO-ERROR.
        
        
        IF avail t-stages THEN
          ASSIGN v-stage    = t-stages.shortname.
      END.
      IF zsdivasp.stagecd >= rStageI[1] AND zsdivasp.stagecd <= rStageI[2] THEN
        ASSIGN v-stage = v-stage.
      ELSE
        NEXT.
      /* Converted orders */
      IF oConv= "y" AND vasp.user2 = "" THEN
        NEXT.
      IF oConv= "n" AND vasp.user2 <> "" THEN
        NEXT.
       
      
      /* Cancelled only */
      IF oCancelled THEN DO:
        FIND t-stages WHERE 
             t-stages.stage = zsdivasp.stagecd NO-LOCK NO-ERROR.
        IF avail t-stages AND t-stages.shortname <> "Can" THEN
          NEXT.
      END.
      
            
      IF oList THEN DO:
        ASSIGN 
          zelection_type = "k"
          zelection_char4 = zsdivasp.takenby
          zelection_cust = 0.
        RUN zelectioncheck.
        IF NOT zelection_good THEN 
          NEXT.
      END.
       
      
      IF oList THEN DO:
        ASSIGN 
          zelection_type = "c"
          zelection_char4 = ""
          zelection_cust = zsdivasp.custno.
        RUN zelectioncheck.
        IF NOT zelection_good THEN 
          NEXT.
      END.
      
      FIND FIRST arsc WHERE 
                 arsc.cono = g-cono AND
                 arsc.custno = zsdivasp.custno
      NO-LOCK NO-ERROR.
      
      IF avail arsc THEN 
        ASSIGN dataCustName = arsc.NAME.
      ELSE 
        ASSIGN dataCustName = "".
      
      IF oList THEN DO:
        ASSIGN 
          zelection_type = "w"
          zelection_char4 = zsdivasp.whse
          zelection_cust = 0.
        RUN zelectioncheck.
        IF NOT zelection_good THEN 
          NEXT.
      END.
      
      ASSIGN h-transtype = " ".
      IF vasp.user2 NE "" THEN DO:
        ASSIGN cntConvert = cntConvert + 1.
        FIND FIRST vaeh WHERE
                   vaeh.cono = g-cono AND
                   vaeh.vano = INT(SUBSTRING(vasp.user2,1,7)) AND
                   vaeh.user2 = vasp.shipprod
        NO-LOCK NO-ERROR.
        IF avail vaeh AND avail vasp AND vasp.user7 NE 14 THEN
          ASSIGN h-transtype = vaeh.transtype.
      END.
      
      FIND t-stages WHERE t-stages.stage = zsdivasp.stagecd NO-LOCK NO-ERROR.
      IF avail t-stages AND t-stages.shortname = "Can" THEN
        ASSIGN cntCancel = cntCancel + 1.
      
      ASSIGN 
        sumPrice = 0
        sumCost  = 0.
      
      FIND FIRST arsc WHERE arsc.cono = g-cono AND
                 arsc.custno = zsdivasp.custno NO-LOCK NO-ERROR.
      IF avail arsc THEN DO:
        ASSIGN 
          dataCustName = arsc.NAME
          Inslsrep     = arsc.slsrepin
          currslsrep   = arsc.slsrepout.
        FIND FIRST smsn WHERE 
                   smsn.cono = g-cono AND
                   smsn.slsrep = currslsrep
        NO-LOCK NO-ERROR.
        IF avail smsn THEN
        ASSIGN 
          v-contact = smsn.NAME
          v-email   = TRIM(smsn.email)
          v-phone   = smsn.phoneno
          dataregion = substring(smsn.mgr,1,1)
          datadistrict = substring(smsn.mgr,2,3).
      END.
      ELSE
      ASSIGN 
        dataCustName = ""
        Inslsrep     = ""
        currslsrep   = ""
        v-contact    = ""
        v-phone      = ""
        v-email      = ""
        dataregion = ""
        datadistrict = "".
      if (dataregion < rRegion[1] or
          dataregion > rRegion[2]) or
         (datadistrict < rDistrict[1] or
          datadistrict > rDistrict[2]) then
        next.  
            
      ASSIGN 
        zelection_type = "s"
        zelection_char4 = currslsrep
        zelection_cust = 0.
      RUN zelectioncheck.
      IF NOT zelection_good THEN 
        NEXT.
 
 
       
      
      
      FOR EACH vaspsl USE-INDEX k-vaspsl WHERE
               vaspsl.cono = g-cono            AND
               vaspsl.shipprod = vasp.shipprod
      NO-LOCK:
        
        FIND vasps WHERE
             vasps.cono = g-cono AND
             vasps.whse = vaspsl.whse AND
             vasps.shipprod = vaspsl.shipprod AND
             vasps.seqno    = vaspsl.seqno 
        NO-LOCK NO-ERROR.
        
       /* Skip the EX sections since they are not needed at this 
          point. They extracted at VA entry only
       */   
  
        find first zsdivaspmap where
                   zsdivaspmap.cono = g-cono and
                   zsdivaspmap.rectype = "S" and        /* VAES */
                   zsdivaspmap.prod    = vasps.shipprod and
                   zsdivaspmap.seqno   = vasps.seqno    and
                   zsdivaspmap.typename = "ExLabor" no-lock no-error.
        if avail zsdivaspmap then
          next.
        find first zsdivaspmap where
                   zsdivaspmap.cono = g-cono and
                   zsdivaspmap.rectype = "S" and        /* VAES */
                   zsdivaspmap.prod    = vasps.shipprod and
                   zsdivaspmap.seqno   = vasps.seqno  and
                   zsdivaspmap.typename = "Exelec" no-lock no-error.
        if avail zsdivaspmap then
          next.
        
        ASSIGN 
          w-vendno     = 0
          v-vendorid   =  " "
          v-technology =  " "
          v-parentcode =  " ".
        
        FIND icsp WHERE 
             icsp.cono = g-cono AND
             icsp.prod = vaspsl.compprod 
        NO-LOCK NO-ERROR.
        FIND icsw WHERE 
             icsw.cono = g-cono AND
             icsw.whse = zsdivasp.whse AND
             icsw.prod = vaspsl.compprod 
        NO-LOCK NO-ERROR.
        ASSIGN w-vendno = vaspsl.arpvendno.
        /* fix for auditing  */
        (IF avail icsw THEN
           icsw.arpvendno
         ELSE
           vaspsl.arpvendno).
        
        IF avail icsp THEN DO:
          FIND catmaster WHERE 
               catmaster.cono = 1 AND
               catmaster.notestype    = "zz" AND
               catmaster.primarykey   = "apsva" AND
               catmaster.secondarykey = icsp.prodcat
          NO-LOCK NO-ERROR.
          IF NOT avail catmaster THEN DO:
            ASSIGN 
              v-parentcode = "".
              v-vendorid = SUBSTRING(icsp.prodcat,1,3).
          END.
          ELSE
          ASSIGN v-vendorid   = catmaster.noteln[1].
        END.
        /** Line item detail info  **/
        
        FIND t-stages WHERE t-stages.stage = zsdivasp.stagecd NO-LOCK NO-ERROR.
        FIND first repTable use-index k-repTable2 where
                   repTable.quoteno = vaspsl.shipprod no-lock no-error.
        if not avail repTable and zsdivasp.OVERRIDE <> "" then do:
          assign v-repTablefl = true.
        end.
        else       
          assign v-repTablefl = false.
         
        ASSIGN 
          cntLine     = cntLine + 1.
        CREATE repTable.
        ASSIGN
          repTable.stage     = IF avail t-stages THEN
                                 t-stages.shortname
                               ELSE
                                "XXX" 
          repTable.sortfld   = vaspsl.seqno
          repTable.sortfld2  = vaspsl.lineno
          repTable.slsrepout = currslsrep
          repTable.slsrepin  = inslsrep
          repTable.srepnm  = v-contact
          repTable.srepph  = v-phone
          repTable.unittype = zsdivasp.mfgname
          repTable.takenby   = zsdivasp.takenby
          repTable.enterdt   = zsdivasp.enterdt
          repTable.senterdt  = STRING(YEAR(zsdivasp.enterdt),"9999") + "/" +
                               STRING(MONTH(zsdivasp.enterdt),"99") + "/" +
                               STRING(DAY(zsdivasp.enterdt),"99")
          repTable.canceldt  = IF avail zsdivasp AND zsdivasp.xuser4 NE ? THEN
                                 zsdivasp.xuser4
                               ELSE
                                 ?
          repTable.custno    = zsdivasp.custno
          repTable.vaspid    = RECID(vasp)
          repTable.quoteno   = vaspsl.shipprod
          repTable.vaspslid  = RECID(vaspsl)
          repTable.region    = dataRegion
          repTable.rdistrict  = dataregion + dataDistrict
          repTable.district  = dataDistrict
          repTable.custname  = dataCustName
          repTable.whse      = zsdivasp.whse
          repTable.cntConvert = 0
          repTable.cntCancel  = 0
          repTable.cntOrder   = 0
          repTable.cntLine    = 0
          repTable.lostbusinesscd = IF zsdivasp.stagecd = v-cancelSTG or
                                      vaspsl.nonstockty = "l" THEN
                                      zsdivasp.xuser3
                                    ELSE
                                      "  "
          repTable.lostbusinessst = IF v-lostbusiness THEN
                                      IF zsdivasp.stagecd = v-cancelSTG or
                                         vaspsl.nonstockty = "l" THEN
                                        zsdivasp.xuser3
                                      ELSE
                                        "  "
                                    ELSE
                                      " "
          repTable.slsrepinst = IF v-slsrepinsort THEN
                                  currslsrep
                                ELSE
                                  "     "
          repTable.slsrepoutst = IF v-slsrepoutsort THEN
                                  currslsrep
                                ELSE
                                  "    "
        
        /** FIX001 - TA  **/
          repTable.specnstype = IF vaspsl.nonstockty = "l" THEN
                                  "l"
                                ELSE
                                  " ".
        sumPrice = /* IF (vaspsl.nonstockty = "l" AND avail t-stages AND
                     t-stages.shortname = "CAN") OR
                    (avail t-stages AND t-stages.shortname = "Can") THEN
                     0
                   ELSE */  IF vasps.sctntype = "IT" then
                    ((vaspsl.timeelapsed / 3600) * vaspsl.prodcost)   
                   ELSE
                    vaspsl.prodcost * vaspsl.qtyneeded.
 
       sumPrice = IF vasps.sctntype = "II" THEN
                      sumPrice * -1
                    ELSE
                      sumPrice.
 
        FIND icsp WHERE 
             icsp.cono =  g-cono AND
             icsp.prod = zsdivasp.partno 
        NO-LOCK NO-ERROR.
        IF avail icsp THEN
          FIND notes USE-INDEX k-notes WHERE
               notes.cono         = g-cono AND
               notes.notestype    = "zz" AND
               notes.primarykey   = "standard cost markup" AND
               notes.secondarykey = icsp.prodcat AND
               notes.pageno       = 1
          NO-LOCK NO-ERROR.
        IF NOT avail icsp OR
          (avail icsp AND NOT avail notes) THEN
          FIND notes USE-INDEX k-notes WHERE
               notes.cono         = g-cono AND
               notes.notestype    = "zz" AND
               notes.primarykey   = "standard cost markup" AND
               notes.secondarykey = "" AND
               notes.pageno       = 1
          NO-LOCK NO-ERROR.
        IF avail notes THEN
        /* calculate the cost gain for nonstock or v-prodcost override  */
          ASSIGN iv-addon =  (1 + (DEC(notes.noteln[1])  / 100)).
        ELSE
          ASSIGN iv-addon = 1.03.
        
        IF zsdivasp.OVERRIDE <> "" and v-reptablefl THEN
          ASSIGN sumPrice = zsdivasp.sellprc.
        else
        IF zsdivasp.OVERRIDE <> "" and not v-reptablefl THEN
          ASSIGN sumPrice = 0.
        ELSE
        IF zsdivasp.margpct > 0 THEN
          ASSIGN sumPrice =
                (sumPrice * iv-addon) /
                (100 - zsdivasp.margpct) * 100.
        ELSE
          ASSIGN 
            sumPrice = sumPrice * iv-addon.
      
        ASSIGN
            sumCost  = /* IF (vaspsl.nonstockty = "l" AND
                         avail t-stages AND t-stages.shortname = "Can") OR
                         (avail t-stages AND t-stages.shortname = "Can") THEN
                         0
                       ELSE */ IF vasps.sctntype = "IT" then
                         ((vaspsl.timeelapsed / 3600) * vaspsl.prodcost)   
                       ELSE
                         vaspsl.prodcost * vaspsl.qtyneeded.
        
        
        ASSIGN 
          sumCost = sumCost * iv-addon
          sumCost = IF vasps.sctntype = "II" THEN
                      sumcost * -1
                    ELSE
                      sumcost
          HPrice = IF vasps.sctntype = "IT" then
                    ((vaspsl.timeelapsed / 3600) * vaspsl.prodcost)   
                   ELSE
                     vaspsl.prodcost * vaspsl.qtyneeded.
     
        HPrice = IF vasps.sctntype = "II" THEN
                      HPrice * -1
                    ELSE
                      HPrice.
 
        
        IF zsdivasp.OVERRIDE <> "" and v-reptablefl THEN
          ASSIGN HPrice = zsdivasp.sellprc.
        else
        IF zsdivasp.OVERRIDE <> "" and not v-reptablefl THEN
          ASSIGN HPrice = 0.
        ELSE
        IF zsdivasp.margpct > 0 THEN
          ASSIGN HPrice =
                (HPrice * iv-addon) / (100 - zsdivasp.margpct) * 100.
        ELSE
          ASSIGN 
            HPrice = HPrice * iv-addon.
        ASSIGN    
            HCost  = IF vasps.sctntype = "IT" then
                       ((vaspsl.timeelapsed / 3600) * vaspsl.prodcost)   
                     ELSE
                       vaspsl.prodcost * vaspsl.qtyneeded.
        ASSIGN 
          HCost = HCost * iv-addon
          HCost = IF vasps.sctntype = "II" THEN
                    HCost * -1
                  ELSE
                    HCost.
        HCOST = round(Hcost,2).
        HPrice = round(HPrice,2).
        sumcost = round(sumcost,2).
        sumprice = round(sumprice,2).
        
        FIND b-repTable USE-INDEX k-repTable2 WHERE 
             b-repTable.quoteno    = vasp.shipprod AND
             b-repTable.slsrepinst = (IF v-slsrepinsort THEN
                                        currslsrep
                                      ELSE
                                        "    ") AND
             b-repTable.slsrepoutst = (IF v-slsrepoutsort THEN
                                         currslsrep
                                       ELSE
                                         "    ") AND
            b-repTable.lostbusinessst =
            (IF v-lostbusiness THEN
               IF vaspsl.nonstockty = "l" THEN
                 "can"
               ELSE
                 "  "
             ELSE
               "  ") AND
            b-repTable.vaspslid     = 0
        NO-LOCK NO-ERROR.
        
        IF avail b-repTable THEN DO:
           /*
           message "Updt" HPrice SumPrice vaspsl.shipprod. pause.
           */
           ASSIGN 
            b-repTable.margmult    = zsdivasp.margpct
            b-repTable.cntLine     = b-repTable.cntLine + 1
            b-repTable.sumPrice    = b-repTable.sumPrice + sumPrice
            b-repTable.sumCost     = b-repTable.sumCost + sumCost
            b-repTable.QsumPrice   = IF vaspsl.nonstockty NE "l" THEN                                                 b-repTable.QsumPrice + HPrice
                                     ELSE
                                       b-repTable.QsumPrice
            b-repTable.QsumCost    = IF vaspsl.nonstockty NE "l" THEN
                                       b-repTable.QsumCost + HCost
                                     ELSE
                                       b-repTable.QsumCost
            b-repTable.CvtsumPrice = b-repTable.CvtsumPrice +
                                     (IF vasp.user2 <> "" AND
                                        vaspsl.nonstockty NE "l" /* and
                                        zsdivasp.stagecd <> v-cancelStg */ THEN
                                        hPrice
                                          ELSE
                                        0)
            b-repTable.CvtsumCost  = b-repTable.CvtsumCost +
                                    (IF vasp.user2 <> "" AND
                                       vaspsl.nonstockty NE "l" /* and
                                       zsdivasp.stagecd <> v-cancelStg  */ THEN
                                       hCost
                                     ELSE
                                       0)
            b-repTable.OpnSumPrice = b-repTable.OpnSumPrice +
                                    (IF zsdivasp.stagecd < v-closedStg AND
                                      vaspsl.nonstockty NE "l" THEN
                                       hPrice
                                     ELSE
                                       0)
            b-repTable.OpnSumCost  = b-repTable.OpnSumCost +
                                    (IF zsdivasp.stagecd < v-closedStg AND
                                      vaspsl.nonstockty NE "l" THEN
                                       hCost
                                     ELSE
                                       0)
            b-repTable.CansumPrice = b-repTable.CansumPrice +
                                    (IF  zsdivasp.stagecd = v-cancelStg THEN
                                       hPrice
                                     ELSE
                                       0)
            b-repTable.CansumCost  = b-repTable.CansumCost +
                                    (IF zsdivasp.stagecd = v-cancelStg THEN
                                       hCost
                                     ELSE
                                       0).
        END.
        ELSE DO:
          
          ASSIGN cntOrder   = 1.
          /*
          message "new" HPrice SumPrice vaspsl.shipprod. pause.
          */
          /* Header with totals */
          CREATE b-repTable.
          ASSIGN
            b-repTable.margmult   = zsdivasp.margpct
            b-repTable.stage      = IF avail t-stages THEN
                                      t-stages.shortname
                                    ELSE
                                      "XXX"
            b-repTable.quoteno    = vaspsl.shipprod
            b-repTable.sortfld    = 0
            b-repTable.sortfld2   = 0
            b-repTable.vaspid     = RECID(vasp)
            b-repTable.vaspslid   = 0
            b-repTable.whse       = zsdivasp.whse
            b-repTable.transtype  = h-transtype
            b-repTable.custname   = dataCustName
            b-repTable.slsrepout  = currslsrep
            b-repTable.slsrepin   = inslsrep
            b-repTable.rdistrict  = dataregion + dataDistrict
            b-repTable.district   = dataDistrict
            b-repTable.region     = dataregion
            b-repTable.unittype = zsdivasp.mfgname
            b-repTable.srepnm   = v-contact
            b-repTable.srepph   = v-phone
            b-repTable.takenby    = zsdivasp.takenby
            b-repTable.slsrepinst = IF v-slsrepinsort THEN
                                      currslsrep
                                    ELSE
                                      " "
            b-repTable.slsrepoutst = IF v-slsrepoutsort THEN
                                       currslsrep
                                     ELSE
                                       " "
          
            b-repTable.lostbusinesscd = IF vaspsl.nonstockty = "l" or
                                           zsdivasp.stagecd = v-cancelStg THEN
                                          zsdivasp.xuser3
                                        ELSE
                                          "  "
            b-repTable.lostbusinessst = IF v-lostbusiness THEN
                                          IF vaspsl.nonstockty = "l" or 
                                             zsdivasp.stagecd = v-cancelStg THEN
                                            zsdivasp.xuser3
                                          ELSE
                                            " "
                                        ELSE
                                          "  "
            b-repTable.enterdt     = zsdivasp.enterdt
            b-repTable.senterdt    = STRING(YEAR(zsdivasp.enterdt),"9999") 
                                       + "/" +
                                     STRING(MONTH(zsdivasp.enterdt),"99") 
                                       +  "/" +
                                     STRING(DAY(zsdivasp.enterdt),"99")
            b-repTable.canceldt    = IF avail zsdivasp AND zsdivasp.xuser4 NE ?
                                     THEN
                                       zsdivasp.xuser4
                                     ELSE
                                       ?
            b-repTable.custno      = zsdivasp.custno
            b-repTable.cntConvert  = cntConvert
            b-repTable.cntCancel   = cntCancel
            b-repTable.cntOrder    = CntOrder
            b-repTable.cntLine     = 1
            b-repTable.sumPrice    = sumPrice
            b-repTable.sumCost     = sumCost
            b-repTable.QsumPrice   = hPrice
            b-repTable.QsumCost    = hCost
            b-repTable.CvtsumPrice = IF vasp.user2 NE "" and
                                        zsdivasp.stagecd <> v-cancelStg THEN
                                       hPrice
                                     ELSE
                                       0
            b-repTable.CvtsumCost  = IF vasp.user2 NE "" and
                                        zsdivasp.stagecd <> v-cancelStg THEN
                                       hCost
                                     ELSE
                                       0
            b-repTable.OpnSumPrice = if zsdivasp.stagecd < v-closedStg
                                     THEN
                                       hPrice
                                     ELSE
                                       0
            b-repTable.OpnSumCost  = if zsdivasp.stagecd < v-closedStg
                                     THEN
                                       hCost
                                     ELSE
                                       0
          
            b-repTable.CansumPrice = IF zsdivasp.stagecd= v-cancelStg THEN
                                       hPrice
                                     ELSE
                                       0
            b-repTable.CansumCost  = IF zsdivasp.stagecd = v-cancelStg THEN
                                       hCost
                                     ELSE
                                       0.
        END.
      END. /* vaspsl */
/*
      ASSIGN 
        repTable.sumPrice = zsdivasp.repairtotl
        b-repTable.sumPrice = zsdivasp.repairtotl.
*/
    END. /* vasp loop */
  END.
/* ------------------------------------------------------------------------- */
  PROCEDURE DetailPrint:
/* ------------------------------------------------------------------------- */
 
    DEFINE INPUT PARAMETER x-recid AS RECID NO-UNDO.
    
    FIND FIRST repTable USE-INDEX k-repTable WHERE
         RECID(repTable) = x-recid 
    NO-LOCK NO-ERROR.
    FIND FIRST vasp     WHERE 
         RECID(vasp) = repTable.vaspid 
    NO-LOCK NO-ERROR.
    FIND zsdivasp WHERE
         zsdivasp.cono = g-cono AND
         zsdivasp.rectype = "fb" AND
         zsdivasp.repairno = vasp.shipprod 
    NO-LOCK NO-ERROR.
    
    
    IF NUM-ENTRIES(vasp.shipprod,"-") GE 2 THEN DO:
      ASSIGN v-quoteno = ENTRY (1,vasp.shipprod,"-") + "-" +
                         STRING(INT(ENTRY(2,vasp.shipprod,"-")),"9999999").
      ASSIGN v-quoteno = ENTRY(1,v-quoteno,"-") + "-" +
                         LEFT-TRIM(ENTRY(2,v-quoteno,"-"),"0").
    END.
    ELSE
      NEXT.
    
    
    ASSIGN 
      x-quoteno = v-quoteno
      v-quoteno = v-quoteno + "q"
      v-custno     = zsdivasp.custno.
    
    IF repTable.sumPrice NE 0 THEN
      ASSIGN v-margin = zsdivasp.margpct.
    ELSE 
      ASSIGN v-margin = 0.
    ASSIGN lostbusreason = ""
           lostbuscode = repTable.lostbusinesscd.
    IF TRIM(lostbuscode) NE "" THEN DO:
      FIND FIRST sasta WHERE 
                 sasta.cono = g-cono AND
                 sasta.codeiden = "e" AND 
                 sasta.codeval = lostbuscode
      NO-LOCK NO-ERROR.
      IF avail sasta THEN 
        ASSIGN lostbusreason = sasta.descr.
      RELEASE sasta.
    END.
    /* show header */
    IF oDetail NE "s" THEN DO:
      IF repTable.vaspslid = 0 THEN DO:
        ASSIGN 
          v-margin   = IF v-margin < (999.99 * -1) THEN
                         (999.99 * -1)
                       ELSE
                         v-margin
          v-partno   = vasp.refer
          v-serial   = zsdivasp.custpo
          v-override = IF zsdivasp.OVERRIDE <> "" THEN
                         "*"
                       ELSE
                         " ".
        IF v-override NE "*" THEN
          ASSIGN 
            repTable.sumPrice =
            (repTable.sumCost / (100 - v-margin)) * 100.
    
        assign v-unitpart = Left-trim(Right-trim(zsdivasp.mfgname)) + " - " +
                            Left-trim(Right-trim(zsdivasp.partno)).    
    
  
       if oExtract <> "" then do:
         assign export_rec =
           v-quoteno      
           + v-del +
           vasp.user2
           + v-del +
           repTable.stage      
           + v-del +
           repTable.whse
           + v-del +
           repTable.takenby    
           + v-del +
           string(v-custno)            
           + v-del +
           v-shipto       
           + v-del +
           string(repTable.custname,"x(30)")  
           + v-del +
           reptable.slsrepout 
           + v-del +
           string(reptable.srepnm,"X(30)")   
            + v-del +
           (if repTable.enterdt = ? then
             " "
            else
              string(repTable.enterdt,"99/99/9999"))    
           + v-del +
           (if repTable.canceldt = ? then
              " "
            else
              string(repTable.canceldt,"99/99/9999")) 
           + v-del +
           string (lostbuscode,"x(2)")       
           + v-del +  
           string(lostbusreason,"x(30)")
           + v-del +
           string(v-serial,"x(22)")           
           + v-del +
           string(zsdivasp.mfgname,"x(40)")
           + v-del +
           string(zsdivasp.partno,"x(40)")    
           + v-del +
           string(v-override,"x(1)")       
           + v-del +
           string(repTable.sumPrice,"->>>>>>>>9.99")  
           + v-del +
           string(repTable.sumCost,"->>>>>>>>9.99")  
           + v-del +
           string(v-margin,"->>>9.99")  + chr(13).
          put stream expt unformatted export_rec.
        end.
        if not p-exportl and oExtract = "" THEN DO:
          DISPLAY
            v-quoteno           
            repTable.stage      
            repTable.takenby    
            v-custno            
            v-shipto       
            repTable.custname  
            reptable.slsrepout 
            reptable.srepnm   
            repTable.enterdt    
            v-serial           
            vasp.user2        
            repTable.sumPrice  
            v-override       
            repTable.sumCost  
            v-margin   
            repTable.whse
            v-unitpart 
            repTable.canceldt 
            lostbuscode         
          WITH FRAME f-vasp.
          DOWN WITH FRAME f-vasp.
        END.
      END.
      /* show notes */
      ASSIGN 
        showNotes    = "Notes:"
        showNoteType = ""
        printNoteln  = "".
      
    END.
    
    IF oNotes NE "n" AND repTable.vaspslid = 0 THEN DO:
      IF oNotes = "S" OR oNotes = "B" THEN DO:
        /* show spec and internal shipping notes */
        FOR EACH notes WHERE 
                 notes.cono = g-cono AND
                 notes.notestype = "in" AND 
                 notes.primarykey = vasp.shipprod AND
                 notes.secondarykey = vasp.whse 
        NO-LOCK BREAK BY notes.pageno:
          ASSIGN showNoteType = "(S)".
          DO iter = 1 TO 16:
            IF TRIM(notes.noteln[iter]) NE "" THEN DO:
              ASSIGN
                printNoteln = REPLACE(TRIM(notes.noteln[iter]),CHR(10)," ").
              DISPLAY showNotes showNoteType printNoteln WITH FRAME f-notes.
              DOWN WITH FRAME f-notes.
            END.
            ASSIGN 
              showNotes    = ""
              showNoteType = "".
          END.
          DISPLAY WITH FRAME f-skip.
        END. /* for each */
        FOR EACH notes WHERE 
                 notes.cono = g-cono AND
                 notes.notestype = "fa" AND 
                 notes.primarykey = vasp.shipprod AND
                 notes.secondarykey = vasp.whse AND
                 notes.pageno = 2 NO-LOCK:
          ASSIGN showNoteType = "(I)".
          DO iter = 1 TO 16:
            IF TRIM(notes.noteln[iter]) NE "" THEN DO:
              ASSIGN
                printNoteln = REPLACE(TRIM(notes.noteln[iter]),CHR(10)," ").
              DISPLAY showNotes showNoteType printNoteln WITH FRAME f-notes.
              DOWN WITH FRAME f-notes.
            END.
            ASSIGN 
              showNotes    = ""
              showNoteType = "".
          END.
          DISPLAY WITH FRAME f-skip.
        END. /* for each */
      END. /* if "S" Or "B" */
      IF oNotes = "E" OR oNotes = "B" THEN DO:
        /* show external customer notes */
        FOR EACH notes WHERE 
                 notes.cono = g-cono AND
                 notes.notestype = "fa" AND 
                 notes.primarykey = vasp.shipprod AND
                 notes.secondarykey = vasp.whse AND
                 notes.pageno = 1 NO-LOCK:
          ASSIGN showNoteType = "(E)".
          DO iter = 1 TO 16:
            IF iter = 1 THEN
              DISPLAY WITH FRAME f-skip.
            IF TRIM(notes.noteln[iter]) NE "" THEN DO:
              ASSIGN
                printNoteln = REPLACE(TRIM(notes.noteln[iter]),CHR(10)," ").
              DISPLAY showNotes showNoteType printNoteln WITH FRAME f-notes.
              DOWN WITH FRAME f-notes.
            END.
            ASSIGN 
              showNotes    = ""
              showNoteType = "".
          END.
          DISPLAY WITH FRAME f-skip.
        END.
      END. /* Notes "E" or "B" */ 
    END.
    /* show line item detail */
    IF oDetail NE "s" AND oDetail NE "h" THEN DO:
      IF not p-exportl and oExtract = "" then
        DISPLAY WITH FRAME f-lineheader.
      FOR EACH b-repTable USE-INDEX k-repTable3 WHERE
               b-repTable.vaspid = repTable.vaspid AND
               b-repTable.slsrepoutst = repTable.slsrepoutst AND
               b-repTable.slsrepinst  = repTable.slsrepinst AND
               b-repTable.lostbusinessst  = repTable.lostbusinessst AND
               b-repTable.vaspslid <> 0
      NO-LOCK BREAK BY b-repTable.sortfld
                    BY b-repTable.sortfld2:
        FIND FIRST vaspsl WHERE 
                   RECID(vaspsl) = b-repTable.vaspslid
        NO-LOCK NO-ERROR.  
        FIND FIRST vasp WHERE 
                   RECID(vasp) = repTable.vaspid
        NO-LOCK NO-ERROR.
        IF avail vaeh THEN
          RELEASE vaeh.
        
        FIND vasps WHERE
             vasps.cono = g-cono AND
             vasps.whse = vaspsl.whse AND
             vasps.shipprod = vaspsl.shipprod AND
             vasps.seqno    = vaspsl.seqno 
        NO-LOCK NO-ERROR.
        
        IF vaspsl.compprod = "" THEN
          ASSIGN v-prod = vaspsl.proddesc2.
        ELSE
          ASSIGN v-prod = vaspsl.compprod.
        
        IF vaspsl.operinit NE "" THEN
          ASSIGN currSlsrep = vaspsl.operinit.
        ELSE
        IF vasp.operinit NE "" THEN
          ASSIGN currSlsrep = vasp.operinit.
        
        IF zsdivasp.margpct NE 0 THEN
          ASSIGN v-margin = zsdivasp.margpct.
        ELSE
          ASSIGN v-margin = 0.
        ASSIGN 
          v-operid  = vaspsl.operinit
          v-enterdt = vaspsl.transdt
          v-margin  = IF v-margin < (999.99 * -1) THEN
                        (999.99 * -1)
                      ELSE
                        v-margin.
    
        IF vasps.sctntype = "IT" then do:
         
          assign v-hours = truncate(vaspsl.timeelapsed / 3600,0)
                 v-minutes =  (vaspsl.timeelapsed mod 3600) / 60.
                
          assign lineqty = 
            string (v-hours,">>>>9") + ":" +
            string (v-minutes,"99") + "  ".
        END.
        ELSE
          ASSIGN lineqty = STRING(vaspsl.qtyneeded,">>>>>.99").
        
        FIND t-stages WHERE 
             t-stages.stage = zsdivasp.stagecd
        NO-LOCK  NO-ERROR.
        
        ASSIGN
          lineCost = IF vaspsl.nonstockty = "l" /* AND avail t-stages AND
                     t-stages.shortname = "Can" */  THEN
                       0
                     ELSE
                       vaspsl.prodcost
          lineCost  = IF vasps.sctntype = "II" THEN
                        lineCost * -1
                      ELSE
                        lineCost
          linePrice = IF vaspsl.nonstockty = "l" /* AND avail t-stages AND
                      t-stages.shortname = "Can" */ THEN
                        0
                      ELSE IF vasps.sctntype = "IT" then
                        ((vaspsl.timeelapsed / 3600) * vaspsl.prodcost)   
                       ELSE
                        vaspsl.prodcost * vaspsl.qtyneeded.
          linePrice  = IF vasps.sctntype = "II" THEN
                         linePrice * -1
                       ELSE
                         linePrice.
        IF vaspsl.nonstockty  = "L" THEN
          ASSIGN specnsl = "l".
        ELSE DO:
          ASSIGN 
            specnsl = "".
          IF zsdivasp.stagecd = v-cancelStg THEN
            ASSIGN specnsl = "l".
        END.
        IF oDetail = "L" THEN DO:
          ASSIGN 
            lostbuscode = ""
            lostbusreason = "".
          IF TRIM(lostbuscode) NE "" THEN DO:
            FIND FIRST sasta WHERE 
                       sasta.cono = g-cono AND
                       sasta.codeiden = "e" AND 
                       sasta.codeval = lostbuscode
            NO-LOCK NO-ERROR.
            IF avail sasta THEN 
              ASSIGN lostbusreason = sasta.descr.
            RELEASE sasta.
          END.
        END.
        
        ASSIGN 
          v-comment = " ".
        FIND x-vasps WHERE
             x-vasps.cono     = g-cono        AND
             x-vasps.shipprod = vasp.shipprod AND
             x-vasps.seqno    = vaspsl.seqno
        NO-LOCK NO-ERROR.
        IF avail x-vasps AND x-vasps.sctntype = "in" THEN DO:
          ASSIGN 
            v-vendno  = vaspsl.arpvendno.
          /** find po comment vendor or info if available **/
          FIND com USE-INDEX k-com WHERE
               com.cono     = g-cono    AND
               com.comtype  = x-quoteno AND
               com.orderno  = 0         AND
               com.ordersuf = 0         AND
               com.lineno   = vaspsl.lineno
          NO-LOCK NO-ERROR.
          IF avail com AND DEC(SUBSTRING(com.noteln[9],17,12)) > 0 THEN DO:
            ASSIGN 
              v-vendno = DEC(SUBSTRING(com.noteln[9],17,12))
              v-vendno = DEC(STRING(v-vendno,"999999999999"))
              v-comment = IF vaspsl.compprod NE "margin improvement" THEN
                            "!"
                          ELSE
                            " ".
            FIND apsv WHERE 
                 apsv.cono = g-cono AND
                 apsv.vendno = v-vendno
            NO-LOCK NO-ERROR.
          END.
          ELSE DO:
            FIND apsv WHERE 
                 apsv.cono = g-cono AND
                 apsv.vendno = v-vendno
            NO-LOCK NO-ERROR.
          END.
        END.
        
        find icsp where     
             icsp.cono = g-cono and
             icsp.prod = vaspsl.compprod  no-lock no-error.
        
        ASSIGN 
          v-descrip1 = if avail icsp then icsp.descrip[1] else vaspsl.proddesc
          v-descrip2 = vaspsl.proddesc2
          v-leadtm   = SUBSTRING(vaspsl.user5,1,4).
        
        ASSIGN 
          v-vendnm   = IF avail apsv THEN
                         apsv.NAME
                       ELSE
                         "  "
          v-vendno   = IF avail apsv THEN
                         apsv.vendno
                       ELSE
                         v-vendno
          audit-vendno = v-vendno.
        
        IF avail x-vasps AND ( x-vasps.sctntype <> "in" 
           AND x-vasps.sctntype <> "II") THEN 
          assign 
            v-vendno = 0
            v-vendnm = "".
        
        FIND vasps WHERE
             vasps.cono     = g-cono        AND
             vasps.shipprod = vasp.shipprod AND
             vasps.seqno    = vaspsl.seqno
         NO-LOCK NO-ERROR.
         linecost = round(linecost,2).
         lineprice = round(lineprice,2).
         if not p-exportl and oExtract = "" THEN DO:
           DISPLAY 
             vasps.sctntype
             specnsl
             vaspsl.seqno
             vaspsl.lineno
             v-prod
             lineqty
             v-operid
             linePrice
             lineCost
             v-enterdt
             v-descrip1
           WITH FRAME f-linedtl.
           DOWN WITH FRAME f-linedtl.
         end.
        
        /* Locate Inventory section from Template */
        FIND zsdivaspmap WHERE
             zsdivaspmap.cono = g-cono AND
             zsdivaspmap.rectype  = "S" AND        /* VAES */
             zsdivaspmap.prod     = zsdivasp.repairno  AND
             zsdivaspmap.typename = "INVENTORY" AND
             zsdivaspmap.lineno = 0  
        NO-LOCK  NO-ERROR.
        
/*        
        IF avail vasp AND avail t-stages AND t-stages.shortname = "CLS"
          AND zsdivasp.xvano > 0 AND
          (avail zsdivaspmap AND vaspsl.seqno = zsdivaspmap.seqno)
          AND vaspsl.nonstockty NE "l"
          AND oAuditClsStage = YES THEN DO:
          IF avail vasp AND zsdivasp.xvano > 0
            AND vaspsl.seqno = zsdivaspmap.seqno THEN DO:
            FIND FIRST vaesl WHERE
                       vaesl.cono     = g-cono AND
                       vaesl.vano     = zsdivasp.xvano AND
                       vaesl.vasuf    = 00            AND
                       vaesl.seqno    = vaspsl.seqno  AND
                       vaesl.lineno   = vaspsl.lineno AND
                       vaesl.shipprod = vaspsl.compprod
            NO-LOCK NO-ERROR.
/*
            IF avail vaesl AND vaesl.arpvendno > 0 THEN
              ASSIGN audit-vendno = vaesl.arpvendno.
            ELSE
              ASSIGN audit-vendno = v-vendno.
            IF avail vaesl AND vaesl.orderalttype = "p" THEN DO:
              FIND poel WHERE 
                   poel.cono = g-cono AND
                   poel.pono   = vaesl.orderaltno AND
                   poel.lineno = vaesl.linealtno
              NO-LOCK NO-ERROR.
              IF avail poel THEN DO:
                ASSIGN audit-vendno = poel.vendno.
              END.
              
              FIND apsv WHERE 
                   apsv.cono = g-cono AND
                   apsv.vendno = audit-vendno
              NO-LOCK NO-ERROR.
              IF avail apsv THEN
                ASSIGN 
                  v-vendnm = apsv.NAME
                  v-operinits = " ".
              
            END. /* tied avail vaesl */
            
            IF (v-vendno > 0 AND audit-vendno > 0) AND
              (v-vendno NE audit-vendno) THEN DO:
              DISPLAY 
                v-operinits 
                v-vendnm 
                audit-vendno 
              WITH FRAME f-auditdtl.
              DOWN WITH FRAME f-auditdtl.
            END.
*/
          END.
        END. /* closed stage audit -  */
*/
      END. /* for each */
      if not p-exportl and oExtract = "" THEN DO:
         DISPLAY 
          " " 
         WITH FRAME f-linedtl.
         DOWN WITH FRAME f-linedtl.
         HIDE FRAME f-lineheader.
      END.
    END. /** if odetail = **/
  
  ASSIGN 
    lastvasp = RECID(vasp).
  RELEASE vasp.
  RELEASE vaspsl.
  END.
&endif
 
/* TBXR F6 CodeGen Includes from here down are CodeGen Generated  */
&if defined(user_optiontype) = 2 &then
if not
can-do("C,D,E,O,Q,R,S,T,U,W",substring(p-optiontype,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries C,D,E,O,Q,R,S,T,U,W "
     substring(p-optiontype,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries C,D,E,O,Q,R,S,T,U,W "
     substring(p-optiontype,v-inx,1).
&endif
 
&if defined(user_registertype) = 2 &then
if not
can-do("C,D,E,O,Q,R,S,T,U,W",substring(p-register,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries C,D,E,O,Q,R,S,T,U,W "
     substring(p-register,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries C,D,E,O,Q,R,S,T,U,W "
     substring(p-register,v-inx,1).
&endif
 
&if defined(user_exportvarheaders1) = 2 &then
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Cust Name".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "District".
  end. 
else  
if v-lookup[v-inx4] = "E" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "EnterDt".
  end. 
else  
if v-lookup[v-inx4] = "O" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "SlsRepOut".
  end. 
else  
if v-lookup[v-inx4] = "Q" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "QuoteNo".
  end. 
else  
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Region".
  end. 
else  
if v-lookup[v-inx4] = "S" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "StageCd".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "TakenBy".
  end. 
else  
if v-lookup[v-inx4] = "U" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "UnitType".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Whse".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_exportvarheaders2) = 2 &then
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "E" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "O" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "Q" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "S" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "U" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_DWvarheaders1) = 2 &then
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "District".
  end. 
else  
if v-lookup[v-inx4] = "E" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "EnterDt".
  end. 
else  
if v-lookup[v-inx4] = "O" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "SlsRepOut".
  end. 
else  
if v-lookup[v-inx4] = "Q" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "QuoteNo".
  end. 
else  
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Region".
  end. 
else  
if v-lookup[v-inx4] = "S" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "StageCd".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "TakenBy".
  end. 
else  
if v-lookup[v-inx4] = "U" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "UnitType".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Whse".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_loadtokens) = 2 &then
if v-lookup[v-inx] = "C" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.custname,"x(30)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(30)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "D" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.RDistrict,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "E" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.senterdt,"x(12)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(12)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "O" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.SlsRepOut,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "Q" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.quoteno,"x(12)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(12)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "R" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.region,"x(1)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(1)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "S" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.stage,"x(3)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(3)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "T" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.Takenby,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "U" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.unittype,"x(30)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(30)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "W" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.whse,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "X" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.quoteno,"x(12)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(12)").   
  end.                                              
else                                              
  assign v-token = " ".
&endif
 
&if defined(user_totaladd) = 2 &then
  assign t-amounts[1] = repTable.sumPrice.
  assign t-amounts[2] = repTable.sumCost.
  assign t-amounts[3] = repTable.cntConvert.
  assign t-amounts[4] = repTable.cntCancel.
  assign t-amounts[5] = 0.
  assign t-amounts[6] = repTable.cntLine.
  assign t-amounts[7] = repTable.cntOrder.
  assign t-amounts[8] = repTable.QsumPrice.
  assign t-amounts[9] = repTable.QsumCost.
  assign t-amounts[10] = 0.
  assign t-amounts[11] = repTable.CvtSumPrice.
  assign t-amounts[12] = repTable.CvtSumCost.
  assign t-amounts[13] = 0.
  assign t-amounts[14] = repTable.CanSumPrice.
  assign t-amounts[15] = repTable.CanSumCost.
  assign t-amounts[16] = 0.
  assign t-amounts[17] = repTable.OpnSumPrice.
  assign t-amounts[18] = repTable.Opnsumcost.
  assign t-amounts[19] = 0.
&endif
 
&if defined(user_numbertotals) = 2 &then
  if v-subtotalup[v-inx4] = 1 then
    v-val = t-amounts1[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 2 then
    v-val = t-amounts2[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 3 then
    v-val = t-amounts3[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 4 then
    v-val = t-amounts4[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 5 then
    v-val = 
      {x-varxq-fmt1.i}
  else 
  if v-subtotalup[v-inx4] = 6 then
    v-val = t-amounts6[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 7 then
    v-val = t-amounts7[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 8 then
    v-val = t-amounts8[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 9 then
    v-val = t-amounts9[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 10 then
    v-val = 
      {x-varxq-fmt1.i "8" "9"}
  else 
  if v-subtotalup[v-inx4] = 11 then
    v-val = t-amounts11[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 12 then
    v-val = t-amounts12[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 13 then
    v-val = 
      {x-varxq-fmt1.i "11" "12"}
  else 
  if v-subtotalup[v-inx4] = 14 then
    v-val = t-amounts14[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 15 then
    v-val = t-amounts15[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 16 then
    v-val = 
      {x-varxq-fmt1.i "14" "15"}
  else 
  if v-subtotalup[v-inx4] = 17 then
    v-val = t-amounts17[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 18 then
    v-val = t-amounts18[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 19 then
    v-val = 
      {x-varxq-fmt1.i "17" "18"}
  else 
    v-val = t-amounts1[v-inx4].
  do inz = 1 to 19:
  if inz = 1 then
    v-regval[inz] = t-amounts1[v-inx4].
  else 
  if inz = 2 then
    v-regval[inz] = t-amounts2[v-inx4].
  else 
  if inz = 3 then
    v-regval[inz] = t-amounts3[v-inx4].
  else 
  if inz = 4 then
    v-regval[inz] = t-amounts4[v-inx4].
  else 
  if inz = 5 then
    v-regval[inz] = 
      {x-varxq-fmt1.i}
  else 
  if inz = 6 then
    v-regval[inz] = t-amounts6[v-inx4].
  else 
  if inz = 7 then
    v-regval[inz] = t-amounts7[v-inx4].
  else 
  if inz = 8 then
    v-regval[inz] = t-amounts8[v-inx4].
  else 
  if inz = 9 then
    v-regval[inz] = t-amounts9[v-inx4].
  else 
  if inz = 10 then
    v-regval[inz] = 
      {x-varxq-fmt1.i "8" "9"}
  else 
  if inz = 11 then
    v-regval[inz] = t-amounts11[v-inx4].
  else 
  if inz = 12 then
    v-regval[inz] = t-amounts12[v-inx4].
  else 
  if inz = 13 then
    v-regval[inz] = 
      {x-varxq-fmt1.i "11" "12"}
  else 
  if inz = 14 then
    v-regval[inz] = t-amounts14[v-inx4].
  else 
  if inz = 15 then
    v-regval[inz] = t-amounts15[v-inx4].
  else 
  if inz = 16 then
    v-regval[inz] = 
      {x-varxq-fmt1.i "14" "15"}
  else 
  if inz = 17 then
    v-regval[inz] = t-amounts17[v-inx4].
  else 
  if inz = 18 then
    v-regval[inz] = t-amounts18[v-inx4].
  else 
  if inz = 19 then
    v-regval[inz] = 
      {x-varxq-fmt1.i "17" "18"}
  else 
    assign inz = inz.
  end.
&endif
 
&if defined(user_summaryloadprint) = 2 &then
  if v-lookup[v-inx2] = "C" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "CustName - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "D" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "District - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "E" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Entered Date - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "O" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "SlsRepOut - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "Q" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Quoteno - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "R" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Region - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "S" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "StageCd - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "T" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Taken By - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "U" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Unit Type - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "W" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Warehouse - " + h-lookup + " " + h-genlit.
    end.
  else
      v-summary-lit = v-lookup[v-inx2].
&endif
 
&if defined(user_summaryloadexport) = 2 &then
  if v-inx4 > v-inx2  then 
    do:
    if v-inx4 > 1 then          
      export_rec = export_rec + v-del. 
    assign export_rec = export_rec +  " ".
    if v-cells[v-inx4] = "2" then
      assign export_rec = export_rec +  v-del + " ".
    end. 
  else 
  if v-lookup[v-inx4] = "C" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(30)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(30)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "D" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "E" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(12)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(12)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "O" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "Q" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(12)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(12)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "R" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(1)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(1)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "S" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(3)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(3)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "T" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "U" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(30)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(30)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "W" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
    do:
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    assign export_rec = export_rec + v-lookup[v-inx4].
    end.
&endif
 
