/* zsdivarwpgld.lpr 1.1 01/03/98 */
/* zsdivarwpgld.lpr 1.1 08/15/96 */
/*h****************************************************************************
  INCLUDE      : varwpgld.lpr
  DESCRIPTION  : Value Add Reports - Work In Process (WIP) Trial Balance -
                 Print GL Distribution
  USED ONCE?   : yes
  AUTHOR       : mtt
  DATE WRITTEN : 08/15/96
  CHANGES MADE :
                 08/15/96 mtt; TB# 21254 (8C) Develop Value Add Module
******************************************************************************/

/*o **************************************************************
    This is the beginning of section two.  Section two prints if
    the detail level is 'd'etail or 'g'l.  It rolls back through
    the temp table records created in section one, sorting by G/L
    info so that all ICSEG totals appear with their appropriate
    G/L record.  No calculations are done here as they were
    already done and stored in the section above.  This section
    is skipped entirely if detail level it 't'otal only.  The only
    exception is that the same header information is used for
    for all detail levels.
    ******************************************************************/

 /*d Set Headings for Section Two or Totals Only */
 /*d Set Page Top */
 if p-totals = "d" then
    page.

 /*d If Needed, Change From Side Labels to Top Labels */
 if length(s-lbl1) = 5 then s-lbl1 = substring(s-lbl1,1,4).
 if length(s-lbl2) = 5 then s-lbl2 = substring(s-lbl2,1,4).
/*
 display v-title1
         v-title2
         v-title3
         v-title4
         s-lbl1
         s-lbl2
         s-printasof
    with frame f-gltop.
 display with frame f-under.
*/
/*d Second Section */
if p-totals ne "t" then do:
/*d GL Records First, Then ICSEG Records */
  for each t-glsa
     no-lock:

    assign
      s-glno      = t-glsa.glno
      s-glname    = t-glsa.glname
      v-sasoofl   = t-glsa.sasoofl.
/*
    display s-glno
            s-glname
            v-sasoofl
      with frame f-glhdr.
    down with frame f-glhdr.
*/
    for each t-key no-lock use-index k-glno where
                           t-key.glno = t-glsa.glno
                        by t-key.key1 by t-key.key2:

      /*d Display */
      assign
        v-key1    = t-key.key1
        v-key2    = t-key.key2
        s-invval1 = t-key.catval1
        s-invval2 = t-key.catval2
        s-extdiff = t-key.extdiff
        s-perdiff = t-key.perdiff.
/*
      display s-invval1
              v-key1
              v-key2
              s-invval2  when p-compare ne ""
              s-extdiff  when p-compare ne ""
              s-perdiff  when p-compare ne ""
          with frame f-gldet.
      down with frame f-gldet.
*/
      end.

   /*d Display Totals */
    assign
      s-invval1   = t-glsa.catval1
      s-invval2   = t-glsa.catval2
      s-glbal     = t-glsa.balance
      s-extdiff   = s-invval1 - s-invval2
      s-perdiff   = if s-invval1 <> 0 then
                     round(((s-extdiff / s-invval1) * 100),2)
                    else 0
      s-perdiff   = minimum(9999.99,
                     if s-perdiff < 0 then s-perdiff * -1
                        else s-perdiff)
      s-offby     = if s-glbal > s-invval1 then
                      "G/L High by " +
                      string(s-glbal - s-invval1,">>>>>>>>9.99-")
                    else if s-glbal < s-invval1 then
                     "G/L Low  by " +
                     string(s-invval1 - s-glbal,">>>>>>>>9.99-")
                    else "".

    display s-invval1
            s-glbal     when p-compare ne ""
            s-offby     when p-compare ne ""
            v-title5
            v-title6
            v-title7
            s-invval2   when p-compare ne ""
            s-extdiff   when p-compare ne ""
            s-perdiff   when p-compare ne ""
     with frame f-gltotx.
     end.
   end.  /* if p-totals ne "t"  */

/*d Handle Grand Totals */
 assign
    s-offby    = if t-grglbal > t-grdval1 then
                   "G/L High by " +
                   string(t-grglbal - t-grdval1,">>>>>>>>9.99-")
                 else if t-grglbal < t-grdval1 then
                   "G/L Low  by " +
                   string(t-grdval1 - t-grglbal,">>>>>>>>9.99-")
                 else ""
    s-extdiff  = t-grdval1 - t-grdval2
    s-perdiff  = if t-catval1 <> 0 then
                   round(((s-extdiff / t-grdval1) * 100),2)
                 else 0
    s-perdiff  = minimum(9999.99,
                   if s-perdiff < 0 then s-perdiff * -1 else s-perdiff)
    s-invval1  = t-grdval1
    s-invval2  = t-grdval2
    s-glbal    = t-grglbal.

  display with frame f-totals.
  clear frame f-gltot no-pause.

  display s-invval1
          s-glbal
          s-offby
          s-invval2 when p-compare ne ""
          s-extdiff when p-compare ne ""
          s-perdiff when p-compare ne ""
    with frame f-gltot.

/*d Hide Header */
hide frame f-under no-pause.
hide frame f-gltop no-pause.
hide frame f-glhdr no-pause.


