{g-all.i}
{g-ic.i}
{g-oe.i}
{g-kp.i}
{g-wt.i}
{g-po.i}
{va.gva}


def var        hivend      as decimal init 999999999999 no-undo.
def var        v-myframe   as character format "x(5)" no-undo.
def var        a-desc      as character format "x(30)" no-undo.
def var        a-qty       as integer   format ">>>>9-" no-undo.
def var        a-min       as integer   format ">>>>9-" no-undo.
def var        a-max       as integer   format ">>>>9-" no-undo.

Define  new shared temp-table dps
  Field orderno       like oeeh.orderno
  Field ordersuf      like oeeh.ordersuf
  Field lineno        like oeel.lineno
  field seqno         like oeelk.seqno
  Field shipprod      like oeel.shipprod
  Field orderaltno    like oeelk.orderaltno
  Field orderalttype  like oeelk.orderalttype
  Field linealtno     like oeelk.linealtno
index lineno
      seqno  
      linealtno.

define new shared buffer s-dps for dps.
 

define new shared query dmd-q for s-dps scrolling. 

define new shared browse dmd-b query dmd-q 
  display  
  s-dps.orderno     column-label "Ord"
  s-dps.ordersuf    column-label " " 
  s-dps.lineno      column-label "Ln"   
  s-dps.seqno       column-label "Seq"
  s-dps.shipprod    column-label "Product"
  s-dps.orderaltno    column-label "TieOrd"
  s-dps.orderalttype  column-label "Type"
  s-dps.linealtno    column-label  "AltLn"
   
   with no-box size-chars 78 by 15 down.


def var v-orderno like oeel.orderno no-undo.
def var v-ordersuf like oeel.ordersuf no-undo.
def var v-lineno  like oeel.lineno no-undo.



define new shared frame f-update
  "Order #"      at 1
   g-orderno   at 1   no-label 
   "-"         at 9
   g-ordersuf  at 10  no-label
   g-oelineno  at 15  no-label     
  
  dmd-b
  with  no-underline no-labels width 80 title g-title.



main:
 
do while true  on endkey undo main, leave main:
 {p-setup.i f-update}
 
  update  
         g-orderno g-ordersuf g-oelineno with frame f-update.
 if {k-cancel.i} or {k-jump.i}    then
      leave main.

 if g-oelineno <> 0 then
   do:
   find oeeh where oeeh.cono = g-cono and 
                   oeeh.orderno = g-orderno and
                   oeeh.ordersuf = g-ordersuf no-lock no-error.
   if not avail oeeh then
     do:
     run err.p (4605).
     next.
     end. 
   else
   if oeeh.stagecd > 4 then
    do:
    run err.p (2202).
    next.
    end.
   
   find oeel where oeel.cono = g-cono and
                   oeel.orderno = g-orderno and
                   oeel.ordersuf = g-ordersuf and
                   oeel.lineno = g-oelineno no-lock no-error.
   if g-oelineno <> 0 then
     if not avail oeel then
       do:
       run err.p (4605).
       next.
       end.                      
   end.
 else
   do:
   find oeeh where oeeh.cono = g-cono and 
                   oeeh.orderno = g-orderno and
                   oeeh.ordersuf = g-ordersuf no-lock no-error.
   if not avail oeeh then
     do:
     run err.p (4605).
     next.
     end.                  
    else
   if oeeh.stagecd > 4 then
    do:
    run err.p (2202).
    next.
    end.
   
  end.
   
 hide message no-pause.

 run aquireitems.

 
 on cursor-up cursor-up.
 on cursor-down cursor-down.
 

 run oext99.p. 
 

 
 on cursor-up back-tab.
 on cursor-down tab.


 {j-all.i "frame f-update"}.

end.

   
procedure aquireitems:
       
for each s-dps:
  delete s-dps.
end.  

if g-oelineno <> 0 then
  do:
  find oeel where oeel.cono = g-cono  
                  and oeel.orderno = g-orderno
                  and oeel.ordersuf = g-ordersuf
                  and oeel.lineno = g-oelineno no-lock.
   
   if oeel.kitfl = false then
     do:
     if (oeel.orderaltno <> 0 or oeel.ordertype <> "" or
       oeel.linealtno <> 0) then
       do:
       create s-dps.
       assign
         s-dps.orderno      =  oeel.orderno
         s-dps.ordersuf     =  oeel.ordersuf
         s-dps.lineno       =  oeel.lineno
         s-dps.seqno        =  0
         s-dps.shipprod     =  oeel.shipprod
         s-dps.orderaltno   =  oeel.orderaltno
         s-dps.orderalttype =  oeel.ordertype
         s-dps.linealtno    =  oeel.linealtno.
      
       end.
     end.
   else
     do:
     for each oeelk where oeelk.cono = g-cono
                     and oeelk.ordertype = "o"
                     and oeelk.orderno = g-orderno
                     and oeelk.ordersuf = g-ordersuf
                     and oeelk.lineno = g-oelineno
                     no-lock:
        
       if (oeelk.orderaltno <> 0 or oeelk.orderalttype <> "" or
           oeelk.linealtno <> 0) then
         do:
         create s-dps.
         assign
           s-dps.orderno      =  oeelk.orderno
           s-dps.ordersuf     =  oeelk.ordersuf
           s-dps.lineno       =  oeelk.lineno
           s-dps.seqno        =  oeelk.seqno
           s-dps.shipprod     =  oeelk.shipprod
           s-dps.orderaltno   =  oeelk.orderaltno
           s-dps.orderalttype =  oeelk.orderalttype
           s-dps.linealtno    =  oeelk.linealtno.
        end.
      end.
    end.
  end.
else
  do:
  for each oeel where oeel.cono = g-cono 
                  and oeel.orderno = g-orderno
                  and oeel.ordersuf = g-ordersuf
                                         no-lock:
   
   if oeel.kitfl = false then
     do:
     if (oeel.orderaltno <> 0 or oeel.ordertype <> "" or
       oeel.linealtno <> 0) then
       do: 
       create s-dps.
       assign
         s-dps.orderno      =  oeel.orderno
         s-dps.ordersuf     =  oeel.ordersuf
         s-dps.lineno       =  oeel.lineno
         s-dps.seqno        =  0
         s-dps.shipprod     =  oeel.shipprod
         s-dps.orderaltno   =  oeel.orderaltno
         s-dps.orderalttype =  oeel.ordertype
         s-dps.linealtno    =  oeel.linealtno.
       end.
     end.
   else
     do:
     for each oeelk where oeelk.cono = g-cono
                     and oeelk.ordertype = "o"
                     and oeelk.orderno = g-orderno
                     and oeelk.ordersuf = g-ordersuf
                     and oeelk.lineno = oeel.lineno
                     no-lock:
       if  (oeelk.orderaltno <> 0 or oeelk.orderalttype <> "" or
          oeelk.linealtno <> 0) then
        do:
        create s-dps.
        assign
          s-dps.orderno      =  oeelk.orderno
          s-dps.ordersuf     =  oeelk.ordersuf
          s-dps.lineno       =  oeelk.lineno
          s-dps.seqno        =  oeelk.seqno
          s-dps.shipprod     =  oeelk.shipprod
          s-dps.orderaltno   =  oeelk.orderaltno
          s-dps.orderalttype =  oeelk.orderalttype
          s-dps.linealtno    =  oeelk.linealtno.
        end.
      end.
  end.
  end.
end.


end.


