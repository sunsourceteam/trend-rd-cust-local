/*h*****************************************************************************
  INCLUDE         : x-xxex.i
  DESCRIPTION     : Generic scrolling entry function
  USED ONLY ONCE? : no
  AUTHOR          : enp/dea/mtt
  DATE WRITTEN    : 07/22/92
  CHANGES MADE    :
    10/19/92 mtt; No tb#  Changed the status message to indicate the users
        should press <GO> instead of cancel to leave the function. This was
        needed because TBEB now uses XXEX for adding customers and it comes into
        XXEX in transaction state, and a <CANCEL> at the F7 screen will back out
        all work.
    12/01/92 mms; TB# 8828  Made v-holdline global and added or v-holdline ne 0
        to force a redisplay of the screen on k-ctrlf.i
    12/08/92 mtt; TB# 8339  Modified delete mode to force a previous page if the
        user deletes the last line on a page (deletes line#1). Before, if they
        did this and attempt to do a prev-page, it wouldn't work.
    02/08/93 dby; qualify pre/next pg/etc for v-scrollfl
    02/18/93 kmw; TB# 4776  Disallow F12 on Choose
    02/19/93 dby; TB# 10070 Added readkey pause 0 back into code
    03/23/93 mtt; TB# 10389 If return was pressed during an inquiry on any other
        line except the last one, then it cause screen display problems.
    04/09/93 mtt;tb# 10070/10039, fixed the scroll down check to only perform
        the scroll down if the cursor is on the last line of the frame. If the
        user pressed <RETURN> quickly through the lines, it was activating the
        scroll down in the middle of the screen.
    05/28/93 enp; TB# 10854 All lines don't display at first
    09/13/93 dea; TB# 12984 Highlight line to be deleted
    02/16/94 dea; TB# 14735 Added go-on parameter to choose.
    08/26/94 rav; TB# 16229 Change XXEX to allow custom GO process.
    09/02/94 rav; TB# 15848 TB 4776 needs additional logic added
    07/13/95 dww; TB# 18244 Extra Characters found in END statements in DOS
        compile.
    01/03/95 dww; TB# 19385 Terminal Hangs when adding record
    05/22/96 klb; TB# 21131 Ensure function keys work under DOS
    10/23/96 jl;  TB# 20300 Allow option to find records in reverse order.
        Added &direction so, if passed "reverse", find last will be subsituted
        for the find first, ect. This is handled as a preprocessor and
        currently does not support switching on the fly.
    04/07/97 mtt; TB# 22649 Develop Buyer's Control Center Feature
    07/01/97 mms; TB# 22649 Buyer's Control Center - Edit Shopping List new;
        Added ability to build status line variable CTRL F display based on
        &action being passed.
    10/07/97 kjb; TB# 21537 x-xxex.i expects either 'yes' or blank to be passed
        in through the &add parameter.  When a value of 'no' was passed in from
        OEIR, an infinite loop was initiated.  Changed the code where it is
        checking for a blank to check for not equal 'yes' so that a value of
        'no' will be valid.
    07/07/98 cm;  TB# 19396 CTRL-F causes page down when no &action specified
    04/05/99 lbr; TB#  20955 Changed direction to be logical to support                switching directions on the fly
*******************************************************************************/

/*e****************************************************************************
    &file      = File name;                               req parm, inq & entry.
    &frame     = Frame name;                              req parm, inq & entry.
    &field     = Choose field;                            req parm, inq & entry.
    &find      = Find next/prev (n-) include;             req parm, inq & entry.
    &display   = Display (d-) include;                    req parm, inq & entry.
    &edit      = Edit logic (e-) include;                 opt parm, entry only.
    &add       = "yes" if Add mode is allowed;            opt parm, entry only.
    &sel       = Special selection criteria for ctrlf     opt parm, inq & entry.
    &status    = Override std status message;             opt parm, inq & entry.
    &usefile   = Program-specific func keys;              opt parm, inq & entry.
    &action    = Action (Search) (a-) include;            opt parm, inq & entry.
    &inqfl     = "/ *" if no entry allowed (inquiry);     opt parm, req for inq.
    &delfl     = "*" to include delete logic;             opt parm, entry only.
    &deledit   = Include file for further editing for del opt parm, entry only.
    &delete    = Delete logic (p-) include;               conditional parameter
                                                          avail if &delfl = "*".
    &go-on     = Add keys to go-on in choose              opt parm, inq & entry.
    &gofile    = Go pressed - execute special check file  opt parm, inq & entry.
    &direction = no to display records in reverse         opt parm, inq & entry
                 index order
    &choosecond = If you want to control whether the      opt parm, inq & entry
                  choose gets executed or not.  This can be used if you wish
                  to allow the user to add a single record, which it then loops
                  back up to the top, bypassing the choose and falling into the
                  usefile param (TB#22649).

g-xxex.i contains:
    v-msg           - Text for Scrolling frame labels.
    v-status        - Text for status message during choose.
    v-scrollfl      - 'yes' if in scrolling/choose mode, 'no' if in add mode.
                      Can be set in calling procedure to begin in add mode.
                      MUST be set to 'yes' when used in inquiries.
    v-nextfl        - Internal to x-xxex.i, used to set direction of search
                      through file (next/prev).
    v-recid         - Holds recid for record on a given line (array).
    v-curpos        - Internal to x-xxex.i, used to set line position when
                      loading v-recid prior to display.
    v-length        - Set in calling procedure, is the # of lines in the
                      scrolling frame.
    v-newdisplay    - 'yes' if the display is to be begun using first/last
                      finds.
    v-nextpage      - Set to 'yes' if a next page is to be triggered by
                      operator interaction.
    v-holdline      - When set to anything other than 0 this will force a
                      repaint of the screen.

    'confirm' is a global variable from g-all.i which is extensively used by
              xxex to provide for more efficient record finds. This can also
              be used in the n- include to qualify or reject successive
              records in the file's key sequence.

    X-XXEX.i works by calculating recid's for each line to be displayed prior
             to any screen painting, this allows more efficient record finds
             and screen display. Keystrokes for prev/next page etc. are
             used to set parameters which dictate how the recid's are to be
             recalculated prior to a screen repaint.

             Security (g-secure) will control what help message is displayed
             and will return the appropriate error messages for violations.
******************************************************************************/

/*tb 15848 09/02/94 rav; TB 4776 needs additional logic added */
{x-xxex.z99 &defvar = "*"}

view frame {&frame}.

/*d Initialize status line and set for a new display */
assign
    v-status     = (if "{&action}" <> "" then "CTL-F)ind, " else "") +
                       (if g-secure > 3 and "{&add}" = "yes" then "CTL-A)dd, "
                        else "") +
                       (if g-secure = 5 and "{&delfl}" = "*" then "CTL-D)el, "
                        else "") +
                   "Up/Dn, Prev/Next Page, or GO"
    v-newdisplay = true
    v-nextpage   = false
    /*tb 8339 12/08/92 mtt;use v-prevpage for auto prev paging during delete */
    v-prevpage   = false
    v-scrollfl   = if g-secure < 4 or "{&edit}" = "" then yes
                   else v-scrollfl.

top:
do while true:

    if {k-jump.i} then leave top.

    /*d Find first (edit mode) or last (Add mode) record for new display */
    if v-newdisplay then do:

        /*tb 10854 05/28/93 enp; All lines don't display at first */
        input clear.
        readkey pause 0.

        assign
            v-recid     = 0
            v-curpos    = 1
            confirm     = true.

        clear frame {&frame} all.

        /*d Not Scrolling */
        if not v-scrollfl then do:
            {{&find} last "/*"}
            assign v-nextfl = false
                   v-curpos = v-length - 1.
        end.
        /*d* Scrolling **/
        else do:
            /*tb 20955 04/05/99 lbr; Changed to logical to support direction
                changes during run time */
            /*tb 20300 10/23/96 jl; Added &direction option for display order */
            &if "{&direction}" = "" &then
                {{&find} first}.
            &else
                if {&direction} then do:
                    {{&find} first}.
                end.
                else do:
                    {{&find} last}.
                end.
            &endif.

            v-nextfl = true.
        end.

        v-newdisplay = false.

    end. /* if v-newdisplay */

    /*o Frame Painting */
    if not v-newdisplay then do while true:

        /*d confirm = no indicates that a new record needs to be found
            prior to displaying a line */
        /*tb 8339 12/08/92 mtt;use v-prevpage for auto prev paging */
        do while not confirm and (avail {&file} or v-prevpage = true):

            confirm = yes.

            if v-nextfl then do:
                /*tb 20955 04/05/99 lbr; Changed to logical to support direction
                    changes during run time */
                /*tb 20300 10/23/96 jl; Added &direction option for display */
                &if "{&direction}" = "" &then
                    {{&find} next "/*"}.
                &else
                    if {&direction} then do:
                        {{&find} next "/*}.
                    end.
                    else do:
                        {{&find} prev "/*"}.
                    end.
                &endif.
            end.
            else do:
                /*tb  20955 04/05/99 lbr; Changed to logical to support direction
                    changes during run time */
                /*tb 20300 10/23/96 jl; Added &direction option for display */
                &if "{&direction}" = "" &then
                    {{&find} prev "/*"}.
                &else
                    if {&direction} then do:
                      {{&find} prev "/*"}.
                    end.
                    else do:
                        {{&find} next "/*"}.
                    end.
                &endif.
             end.
        end.

        /*d Trap keystrokes to rearrange screen for next record to display */
        if avail {&file} then do:

            if {k-nextpg.i} or v-nextpage = true then do:

                assign
                    v-nextpage = false
                    v-nextfl   = true
                    v-curpos   = 1
                    v-recid    = 0.

                clear frame {&frame} all no-pause.

            end.

            /*tb 8339 12/08/92 mtt;use v-prevpage for auto prev paging
               during delete */
            else if {k-prevpg.i} or v-prevpage = true then do:

                assign
                    v-prevpage = false
                    v-nextfl   = false
                    v-curpos   = v-length
                    v-recid    = 0.

                clear frame {&frame} all no-pause.

            end.

            else if {k-scrlup.i} then do:

                do i = v-length to 2 by -1:
                    v-recid[i] = v-recid[i - 1].
                end.

                assign
                    v-nextfl = false
                    v-curpos = 1
                    confirm  = no.

                scroll down with frame {&frame}.

            end.

            else if {k-scrldn.i} then do:

                do i = 2 to v-length:
                    v-recid[i - 1] = v-recid[i].
                end.

                assign
                    v-nextfl          = true
                    confirm           = no
                    v-curpos          = v-length
                    v-recid[v-length] = 0.

                scroll up with frame {&frame}.

            end.

            /*d Load recid for record on current display line */
            v-recid[v-curpos] = recid({&file}).

            /*d Re-position cursor if required */
            if v-nextfl and v-curpos < v-length then
                v-curpos = v-curpos + 1.
            else if v-nextfl = no and v-curpos > 1 then
                v-curpos = v-curpos - 1.
            else if {k-scrlup.i} or {k-scrldn.i} then do:
                v-curpos = 0.
                leave.
            end.
            else do:
                v-curpos = if not v-nextfl then v-length else 1.
                leave.
            end.

            /*d Clear Lastkey, to avoid trapping further down */
            /*tb 10854 05/28/93 enp; All lines don't display at first */
            input clear.
            readkey pause 0.

            confirm = no.

        end.      /* avail &file */
        else do:  /* not avail &file */

            /*d If the search has gone backwards to the beginning of the
                file, and the page is not full, reset to fill from 1st record */
            if v-nextfl = no and v-curpos >= 1 and v-recid[v-curpos + 1] ne 0
            then do:

                find {&file} where recid({&file}) = v-recid[v-curpos + 1]
                    no-lock no-error.

                assign
                    v-recid    = 0
                    v-recid[1] = recid({&file})
                    v-curpos   = if avail {&file} then 2 else 1
                    confirm    = no
                    v-nextfl   = yes.

                next.

            end.

            /* End of Display */
            run err.p(9013).

            leave.

        end.
    end.  /* not v-newdisplay */

    /*o Display screen using recids loaded above */
    do i = frame-line({&frame}) to v-length:

        if ({k-scrlup.i} and i ne 1) or ({k-scrldn.i} and i ne v-length)
            then next.


        find {&file} where recid({&file}) = v-recid[i] no-lock no-error.

        if avail {&file} then do:

            {{&display}}
            /* das - VA Notes */
            find com use-index k-com where 
                               com.cono = g-cono      and
                               com.comtype    = "va" + 
                                                string(s-seqno,"999") and
                               com.orderno    = g-vano     and
                               com.ordersuf   = g-vasuf    and
                               com.lineno     = s-lnlineno   and
                               com.printfl    = yes
                               no-lock no-error.
            if avail com then do:
               if s-lncommentfl = "c" or s-lncommentfl = "x" then
                  assign s-lncommentfl = "x".
               else
                  assign s-lncommentfl = "p".
            end.
            /* das - VA Notes */

            if frame-line({&frame}) ne v-length and not {k-scrlup.i} then
                down with frame {&frame}.
        end.
    end.

    /*d Reposition cursor after page is painted */
    if v-nextfl = yes and not {k-scrldn.i} and v-scrollfl = yes then
        up frame-line({&frame}) - 1 with frame {&frame}.

    /*o* Scroll/Add Section **/
    dsply:
    do while true with frame {&frame} on endkey undo main, next main:

        /*d Execute Choose if in scroll mode */
        /*tb 22649 04/07/97 mtt; Develop Buyer's Control Center Feature */
        if v-scrollfl = yes {&choosecond}
        then do:

            status default v-status.

            /*o Override status message if needed */
            {&status}

            /*o* Choose & Select Keys **/
            /*tb  4776 02/18/93 kmw; Disallow F12 on Choose */
            /*tb 14735 02/16/94 dea; Added go-on parameter for SI */
            /*tb 21131 05/22/96 klb; Add shift-f9 and shift-f10 for DOS
                functionality */
            choose row {&field} no-error
                go-on(f12 f19 shift-f9 f20 shift-f10 {&go-on})
                with frame {&frame}.

            color display normal {&field} with frame {&frame}.

            hide message no-pause.
        end.

        /*o* F12 **/
        /*tb 4776 02/18/93  kmw; Disallow F12 on Choose */
        /*tb 15848 09/02/94 rav; TB 4776 needs additional logic added */
        /*d If F12 pressed, go into the choose again */
        if {k-func12.i} then do:
            {x-xxex.z99 &func12 = "*"}

            /* Lookup Not Valid Here */
            run err.p(2003).
            next.
        end.

        /*o* GO **/
        /*tb 16229 08/26/94 rav; change XXEX to allow custom GO process */
        if v-scrollfl and {k-select.i} then do:

            {{&gofile}}

            hide message no-pause.
            leave main.
        end.  /* Go processing */
        else

        /*o* Custom Keys, F6-F10 **/
        {{&usefile}}

        /*o* Edit **/
        {&inqfl}
        if ({k-return.i} and v-recid[frame-line({&frame})] ne 0) or
            v-scrollfl = no then
        do for b-{&file} transaction:

            if g-secure < 3 then do:

                /* Inquiry Only */
                run err.p(1107).

                {pause.gsc}

                hide message no-pause.
                next dsply.
            end.

            find b-{&file} where recid(b-{&file}) =
                v-recid[frame-line({&frame})] exclusive-lock no-error.

            if not avail b-{&file} and v-scrollfl = yes then do:

                /* Record Not Found */
                run err.p(1013).
                next dsply.
            end.

            /*o Line Editing */
            {{&edit}}

            if avail b-{&file} then
                v-recid[frame-line({&frame})] = recid(b-{&file}).

            /*d Scroll up for new line if in Add mode */
            if frame-line({&frame}) ne v-length and not {k-cancel.i} then
                down with frame {&frame}.
            else if not v-scrollfl then do:

                do i = 2 to v-length:
                    v-recid[i - 1] = v-recid[i].
                end.

                assign
                    v-recid[v-length] = 0
                    v-nextfl          = true.

                scroll up with frame {&frame}.

                /*tb 19385 01/03/96 dww; Terminal Hangs when Adding Record */
                /*d Please do not remove this line of code.  It may seem
                    irrelevant, but it allows the Progress V6 compiler (and
                    possibly the V7 compiler) to NOT hang terminals when
                    adding records on an IBM RS/6000 system */
                v-nextfl = yes.

            end.

            release b-{&file}.

            input clear.
            readkey pause 0.

        end.  /* Edit Logic */

        /*o* Go into Add Mode **/
        else if {k-ctrla.i} or {k-return.i} then do:

            if g-secure < 4 then do:

                /* Security Violation - Add Not Allowed */
                run err.p(1108).

                {pause.gsc}

                hide message no-pause.
                next dsply.
            end.

            /*tb 21537 10/07/97 kjb; Changed from '= ""' to 'ne "yes"' so that
                if a value of "no" is passed in, the code will process it
                correctly. */
            if "{&add}" ne "yes" then do:
                bell.
                next dsply.
            end.

            assign
                v-scrollfl   = false
                v-newdisplay = true.

            next top.

        end. /* ctrl-a or <ret> on blank line */
        /*tb 10389 03/23/93 mtt;have an 'else' within the logic that can
            be commented out for inquiry functions, so the next check
            that was added will not execute.  */
        else
        /{&inqfl}* */

        /*tb 10389 03/23/93 mtt;check for return on any line other than the
             last one, so the scroll-down keystroke (below) doesn't force a
             scroll down.  */
        if {k-return.i} and frame-line({&frame}) ne v-length then do:
            down with frame {&frame}.
            next dsply.
        end.

        /*o Delete */
        /{&delfl}*/
        else if {k-delete.i} and v-recid[frame-line({&frame})] ne 0
        then do:

            /*e Code from p-delete.i is brought in whole because comment
                string for {2} cannot be passed in non-delete mode. */
            if g-secure < 5 then do:

                /* Security Violation - Delete Not Allowed */
                run err.p(1109).

                {pause.gsc}

                hide message no-pause.
                next dsply.
            end.

            confirm = no.

            pause 0 before-hide.

            /*tb 12984 09/13/93 dea; highlight choose field of record del */
            color display message {&field} with frame {&frame}.

            do on endkey undo, leave:
                update confirm label "Delete?" auto-return
                with frame del side-labels row 1 column 67 overlay.
            end.

            hide frame del no-pause.

            if confirm then do:

                /*e if the &deledit include is passed, then execute it. This
                    include must turn confirm = yes if the record can be
                    actually deleted.  */
                /*tb 8339 12/08/92 mtt;automatically read record in
                    &file buffer (to have index set properly) to force
                    prev-page. */
                find {&file} where recid({&file}) =
                    v-recid[frame-line({&frame})] no-lock no-error.

                if "{&deledit}" <> "" then do:
                    confirm = no.
                    {{&deledit}}
                end.

                if confirm then do:

                    do for b-{&file} transaction:

                        find b-{&file} where recid(b-{&file}) =
                            v-recid[frame-line({&frame})]
                            exclusive-lock no-error.

                        {{&delete}}

                    end.

                    input clear.
                    readkey pause 0.

                    do i = frame-line({&frame}) + 1 to v-length:
                        v-recid[i - 1] = v-recid[i].
                    end.

                    v-recid[v-length] = 0.

                    scroll from-current up with frame {&frame}.

                    find {&file} where recid({&file}) =
                        v-recid[v-length - 1] no-lock no-error.

                    assign
                        v-curpos  = v-length
                        confirm   = no
                        v-nextfl  = yes.

                    if avail {&file} then do:

                        down v-length - frame-line({&frame})
                            with frame {&frame}.

                        next top.
                    end.
                    /*tb 8339 12/08/92 mtt;If first record on the screen was
                      deleted no more line items are shown on the screen
                      (first recid = 0), then force a prev-page.  */
                    else if v-recid[1] = 0 then do:

                        assign
                               v-prevpage = true
                               v-curpos   = 0
                               confirm    = false
                               v-nextfl   = false.

                        next top.

                    end.
                    else next dsply.

                end. /* confirm is still true from &deledit include */
                else next dsply.

            end.  /* confirm = true from delete screen. */
            else next dsply.

        end.  /* delete */
        /{&delfl}*/

        /*o* Find to New Location **/
        if {k-ctrlf.i} {{&sel}} or v-holdline ne 0 then do:

            /*TB 19396 07/07/98 cm; CTRL-F causes page down when no &action
                specified.  Send bell and don't perform any action. */
            if {k-ctrlf.i} {{&sel}} and "{&action}" = "" then do:
                bell.
                next dsply.
            end.

            {{&action}}

            if {k-cancel.i} or {k-jump.i} then next dsply.
            else if avail {&file} then do:

                assign
                    confirm = true
                    v-nextfl = true
                    v-curpos = 1
                    v-recid  = 0.

                clear frame {&frame} all no-pause.

                input clear.
                readkey pause 0.

                next top.
            end.

        end. /* if k-ctrlf.i */

        /* 02/28/93 dby */
        if v-scrollfl then do:

            /*o* Previous Page **/
            if {k-prevpg.i} then do:

                if v-recid[1] = 0 or v-recid[1] = ? then do:
                    run err.p(9014).
                    next dsply.
                end.

                find {&file} where recid({&file}) = v-recid[1] no-lock no-error.

                assign
                    v-curpos = 0
                    confirm  = false
                    v-nextfl = false.

                next top.
            end.  /* if k-prevpg.i */

            /*o Next Page */
            else if {k-nextpg.i} or v-nextpage = true then do:

                if v-recid[v-length] = 0 or v-recid[v-length] = ? then do:
                    run err.p(9015).
                    next dsply.
                end.

                find {&file} where recid({&file}) = v-recid[v-length]
                    no-lock no-error.

                assign
                    confirm  = false
                    v-nextfl = true.

                next top.
            end.  /* if k-nextpg.i */

            /*o* Cursor Up **/
            else if {k-scrlup.i} then do:

                find {&file} where recid({&file}) = v-recid[1]
                    no-lock no-error.

                if avail {&file} then do:

                    assign
                       v-curpos = 0
                       confirm  = false
                       v-nextfl = false.

                    next top.
                end.
                else do:

                    run err.p(9014).

                    next dsply.
                end.
            end.  /* if k-scrlup.i */

            /*o* Cursor Down **/
            /*tb 10070 04/09/93 mtt;only activate the scroll down when the
                 cursor is on the last line of the screen.  */
            else if {k-scrldn.i} and frame-line({&frame}) = v-length then do:

                find {&file} where recid({&file}) = v-recid[v-length]
                    no-lock no-error.

                if avail {&file} then do:

                    assign
                        confirm  = false
                        v-nextfl = true.

                    next top.
                end.
                else do:

                    run err.p(9015).

                    next dsply.
                end.

            end. /* if k-scrldn.i */

        end. /* if scrolling */

    end. /** End of Scroll Section (dsply) **/

    status default.
    leave.

end. /* Top */



