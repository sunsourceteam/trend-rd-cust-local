 

/* g-poebustk.i 1.2 09/28/98 */
/*h*****************************************************************************
  INCLUDE      : g-poebu.i
  DESCRIPTION  : Shared buffers and variables for poebu.p
  USED ONCE?   : no
  AUTHOR       : jbt
  DATE WRITTEN : 08/18/92
  CHANGES MADE :
    08/24/92 dea; TB#  7581 Workfile to keep divisional balances for Div Acct
    05/23/95 kjb; TB# 15836 BO flag = no on OE DO but BO is created.  Added
        parameter 3 because poebu1.p oversized.
    10/11/95 kr ; TB# 19525 Move handheld functions to Batch Menu.
    03/16/97 mms; TB#  7241 (5.1) Spec8.0 Special Price Costing;  Removed
        definition of speccostty as it is not required.
    09/25/98 cm;  TB# 23507 Use new include to define w-div
*******************************************************************************/

def {1} buffer b-iceh     for iceh.
def {1} buffer b-report   for report.
def {1} buffer b-sasc     for sasc.

def {1} var s-prod        as c format "x(25)"            no-undo.
def {1} var s-whse        like icsw.whse                 no-undo.
def {1} var s-count       as integer format "zzzzzzzz9"  no-undo.
def {1} var b-whse        like icsw.whse                 no-undo.
def {1} var e-whse        like icsw.whse                 no-undo.
def {1} var b-date        as date                        no-undo.
def {1} var e-date        as date                        no-undo.
def {1} var p-postdt      as date                        no-undo.
def {1} var p-period      like sasj.period               no-undo.
def {1} var p-update      as logical                     no-undo.
def {1} var p-print       as logical                     no-undo.
def {1} var f-receiving   as logical initial no          no-undo.
def {1} var f-company     as logical initial no          no-undo.
def {1} var f-date        as logical initial no          no-undo.
def {1} var f-whse        as logical initial no          no-undo.
def {1} var f-order       as logical initial no          no-undo.
def {1} var f-product     as logical initial no          no-undo.
def {1} var f-return      as logical initial no          no-undo.
def {1} var v-idpoeh      as recid                       no-undo.
def {1} var v-dataid      like iceh.dataid               no-undo.
def {1} var v-snfl        as logical initial no          no-undo.
def {1} var v-c2          as c format "x(2)"             no-undo.
def {1} var v-podec       like report.de12d0             no-undo.
def {1} var v-transcnt    as integer                     no-undo.
def {1} var v-maxtrans    as integer initial 25          no-undo.
/*
def {1} var v-lastkey     like iceh.keyno                no-undo.
def {1} var v-orderkey    like iceh.keyno                no-undo.
def {1} var v-whsekey     like iceh.keyno                no-undo.
def {1} var v-conokey     like iceh.keyno                no-undo.
def {1} var v-datekey     like iceh.keyno                no-undo.
def {1} var v-funckey     like iceh.keyno                no-undo.
*/
def {1} var v-dateskip    as logical initial no          no-undo.
def {1} var v-conoskip    as logical initial no          no-undo.
def {1} var v-whseskip    as logical initial no          no-undo.
{2}
def {1} var v-reportnm    like sapb.reportnm             no-undo.
/{2}*  */
def {1} var v-dte         as date                        no-undo.
/* def {1} var v-whse        like icsw.whse                 no-undo. */
def {1} var v-prod        like icsw.prod                 no-undo.
def {1} var v-pono        like poeh.pono                 no-undo.
def {1} var v-posuf       like poeh.posuf                no-undo.
def {1} var v-type        as c format "x(1)"             no-undo.
def {1} var v-cqty        as dec                         no-undo.
def {1} var v-cunit       as c format "x(4)"             no-undo.
def {1} var v-subfound    as logical                     no-undo.
def {1} var v-subprod     like poel.shipprod             no-undo.
def {1} var v-qty         as integer                     no-undo.
def {1} var v-proofqty    as integer                     no-undo.
def {1} var v-notesfl     like icsp.notesfl              no-undo.
def {1} var v-vendprod    like icsw.vendprod             no-undo.
def {1} var v-serlottype  like icsw.serlottype           no-undo.
def {1} var v-serialno    like icses.serialno            no-undo.
def {1} var v-binloc1     like icsw.binloc1              no-undo.
{3}
/*tb 23507 09/25/98 cm; Use new include to define w-div */
{gldiv.gtt &new = "{1}"}
/{3}*  */



