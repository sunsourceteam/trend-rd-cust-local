/*
output to "/tmp1/smrzs.dat".
*/
/***********************************************************************
    Procedure    : smrzs
    Description  : sales report sorted by region, district, sales rep
    Author       : Dave Farmer
    Date Written : 01/06/98
    Changes Made :


 SDI001 - Add freight calculations into sales figures based on parameter
          input
 04/13/11  tah  Add branch logic

************************************************************************/

{p-rptbeg.i}
/*
/* ----used for test with out include ---
*/
def var g-cono like icsd.cono.
g-cono = 1.
def var pagesize   as integer.
pagesize = 45.
*/
def buffer c-smss for smss.
def buffer p-smss for smss.
def var xt as integer no-undo.
def var tyr as integer no-undo.

def var gex           as dec format ">>>>-"           no-undo.
def var gey           as dec format ">>>>-"           no-undo.
def var line-max      AS INT INIT 42                 NO-UNDO.
def var line-cnt      as int                         no-undo.
def var a             as int                         no-undo.
def var w-region      as char format "x"             no-undo.
def var w-dist        as dec format "999"            no-undo.
def var w-branch      as char format "x(4)"          no-undo.
def var branchcnt     as int                         no-undo.
def var regcnt        as int                         no-undo.
def var distcnt       as int                         no-undo.
def var subcnt        as int                         no-undo.
def var grndcnt       as int                         no-undo.
def var first-time    as log init true               no-undo.

DEF VAR rpt-ytd-hdr1  AS CHAR FORMAT "X(14)"         NO-UNDO.
DEF VAR rpt-ytd-hdr2  AS CHAR FORMAT "X(14)"         NO-UNDO.
                                                     
def var rpt-dt        as date                        no-undo.
def var end-dt        as date                        no-undo.
def var beg-dt        as date                        no-undo.

def var mo-1          as int format "99"             no-undo.
def var mo-2          as int format "99"             no-undo.
def var mo-3          as int format "99"             no-undo.
def var rpt-mo        as int format "99"             no-undo.

def var yr-1          as int format "9999"           no-undo.
def var yr-2          as int format "9999"           no-undo.
def var yr-3          as int format "9999"           no-undo.
def var lst-yr        as int format "9999"           no-undo.
def var rpt-yr        as int format "9999"           no-undo.
def var moend-dt      as date format "99/99/9999"    no-undo.
def var ytd-mos       as int format "99"             no-undo.

def var w-divno       like arsc.divno               no-undo.
def var v-divno       as int format "9999"          no-undo.
def var w-salesterr   like arsc.salesterr           no-undo.
def var w-sls-lst-yr  as dec format "zzzzzzzz9.99-" no-undo.
def var w-cst-lst-yr  as dec format "zzzzzzzz9.99-" no-undo.
def var w-sign        as int format "9-"            no-undo.

/* Branch */

def var branchsub-1-sls  as dec format ">>>,>>>,>>9-"  no-undo.
def var branchsub-2-sls  as dec format ">>>,>>>,>>9-"  no-undo.
def var branchsub-3-sls  as dec format ">>>,>>>,>>9-"  no-undo.
def var branchsub-4-sls  as dec format ">>>,>>>,>>9-"  no-undo.
def var branchsub-5-sls  as dec format ">>>,>>>,>>9-"  no-undo.
def var branchsub-6-sls  as dec format ">>>,>>>,>>9-"  no-undo.
def var branchsub-1-cst  as dec format ">>>,>>>,>>9-"  no-undo.
def var branchsub-2-cst  as dec format ">>>,>>>,>>9-"  no-undo.
def var branchsub-3-cst  as dec format ">>>,>>>,>>9-"  no-undo.
def var branchsub-4-cst  as dec format ">>>,>>>,>>9-"  no-undo.
def var branchsub-5-cst  as dec format ">>>,>>>,>>9-"  no-undo.
def var branchsub-6-gm   as dec format ">>>,>>>,>>9-"  no-undo.


/* Branch */

def var regsub-1-sls  as dec format ">>>,>>>,>>9-"  no-undo.
def var regsub-2-sls  as dec format ">>>,>>>,>>9-"  no-undo.
def var regsub-3-sls  as dec format ">>>,>>>,>>9-"  no-undo.
def var regsub-4-sls  as dec format ">>>,>>>,>>9-"  no-undo.
def var regsub-5-sls  as dec format ">>>,>>>,>>9-"  no-undo.
def var regsub-6-sls  as dec format ">>>,>>>,>>9-"  no-undo.
def var regsub-1-cst  as dec format ">>>,>>>,>>9-"  no-undo.
def var regsub-2-cst  as dec format ">>>,>>>,>>9-"  no-undo.
def var regsub-3-cst  as dec format ">>>,>>>,>>9-"  no-undo.
def var regsub-4-cst  as dec format ">>>,>>>,>>9-"  no-undo.
def var regsub-5-cst  as dec format ">>>,>>>,>>9-"  no-undo.
def var regsub-6-gm   as dec format ">>>,>>>,>>9-"  no-undo.

def var grndtot-1-sls as dec format ">>>,>>>,>>9-"  no-undo.
def var grndtot-2-sls as dec format ">>>,>>>,>>9-"  no-undo.
def var grndtot-3-sls as dec format ">>>,>>>,>>9-"  no-undo.
def var grndtot-4-sls as dec format ">>>,>>>,>>9-"  no-undo.
def var grndtot-5-sls as dec format ">>>,>>>,>>9-"  no-undo.
def var grndtot-6-sls as dec format ">>>,>>>,>>9-"  no-undo.
def var grndtot-1-cst as dec format ">>>,>>>,>>9-"  no-undo.
def var grndtot-2-cst as dec format ">>>,>>>,>>9-"  no-undo.
def var grndtot-3-cst as dec format ">>>,>>>,>>9-"  no-undo.
def var grndtot-4-cst as dec format ">>>,>>>,>>9-"  no-undo.
def var grndtot-5-cst as dec format ">>>,>>>,>>9-"  no-undo.
def var grndtot-6-gm  as dec format ">>>,>>>,>>9-"  no-undo.

def var distsub-1-sls as dec format ">>>,>>>,>>9-"  no-undo.
def var distsub-2-sls as dec format ">>>,>>>,>>9-"  no-undo.
def var distsub-3-sls as dec format ">>>,>>>,>>9-"  no-undo.
def var distsub-4-sls as dec format ">>>,>>>,>>9-"  no-undo.
def var distsub-5-sls as dec format ">>>,>>>,>>9-"  no-undo.
def var distsub-6-sls as dec format ">>>,>>>,>>9-"  no-undo.
def var distsub-1-cst as dec format ">>>,>>>,>>9-"  no-undo.
def var distsub-2-cst as dec format ">>>,>>>,>>9-"  no-undo.
def var distsub-3-cst as dec format ">>>,>>>,>>9-"  no-undo.
def var distsub-4-cst as dec format ">>>,>>>,>>9-"  no-undo.
def var distsub-5-cst as dec format ">>>,>>>,>>9-"  no-undo.
def var distsub-6-gm  as dec format ">>>,>>>,>>9-"  no-undo.

def var tot-1-sls     as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-2-sls     as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-3-sls     as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-4-sls     as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-5-sls     as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-6-sls     as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-1-cst     as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-2-cst     as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-3-cst     as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-4-cst     as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-5-cst     as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-6-gm      as dec format ">>>,>>>,>>9-"  no-undo.

def var reggm1        as dec format ">>>9.99-"      no-undo.
def var reggm2        as dec format ">>>9.99-"      no-undo.
def var reggm3        as dec format ">>>9.99-"      no-undo.
def var reggm4        as dec format ">>>9.99-"      no-undo.
def var reggm5        as dec format ">>>9.99-"      no-undo.
def var reggm6        as dec format ">>>9.99-"      no-undo.
def var reg-per-plan  as dec format ">>>9.99-"      no-undo.
                   
def var distgm1       as dec format ">>>9.99-"      no-undo.
def var distgm2       as dec format ">>>9.99-"      no-undo.
def var distgm3       as dec format ">>>9.99-"      no-undo.
def var distgm4       as dec format ">>>9.99-"      no-undo.
def var distgm5       as dec format ">>>9.99-"      no-undo.
def var distgm6       as dec format ">>>9.99-"      no-undo.
def var dist-per-plan as dec format ">>>9.99-"      no-undo.

/* Branch */
                   
                   
def var branchgm1       as dec format ">>>9.99-"      no-undo.
def var branchgm2       as dec format ">>>9.99-"      no-undo.
def var branchgm3       as dec format ">>>9.99-"      no-undo.
def var branchgm4       as dec format ">>>9.99-"      no-undo.
def var branchgm5       as dec format ">>>9.99-"      no-undo.
def var branchgm6       as dec format ">>>9.99-"      no-undo.
def var branch-per-plan as dec format ">>>9.99-"      no-undo.



/* Branch */

def var subgm1        as dec format ">>>9.99-"      no-undo.
def var subgm2        as dec format ">>>9.99-"      no-undo.
def var subgm3        as dec format ">>>9.99-"      no-undo.
def var subgm4        as dec format ">>>9.99-"      no-undo.
def var subgm5        as dec format ">>>9.99-"      no-undo.
def var subgm6        as dec format ">>>9.99-"      no-undo.

def var totgm1        as dec format ">>>9.99-"      no-undo.
def var totgm2        as dec format ">>>9.99-"      no-undo.
def var totgm3        as dec format ">>>9.99-"      no-undo.
def var totgm4        as dec format ">>>9.99-"      no-undo.
def var totgm5        as dec format ">>>9.99-"      no-undo.
def var grndgm-plan   as dec format ">>>9.99-"      no-undo.

def var w-gm-mo-1     as dec format ">>>9.99-"      no-undo.
def var w-gm-mo-2     as dec format ">>>9.99-"      no-undo.
def var w-gm-mo-3     as dec format ">>>9.99-"      no-undo.

def var gm1           as dec format ">>>9.99-"      no-undo.
def var gm2           as dec format ">>>9.99-"      no-undo.
def var gm3           as dec format ">>>9.99-"      no-undo.
def var gm4           as dec format ">>>9.99-"      no-undo.
def var gm5           as dec format ">>>9.99-"      no-undo.
def var gm6           as dec format ">>>9.99-"      no-undo.
def var grnd-per-plan as dec format ">>>9.99-"      no-undo.

def var k             as int                        no-undo.

def var x-divno       as char format "x(27)"        no-undo.
def var x-slsrep      as char format "x(35)"        no-undo.
def var x-branch      as char format "x(4)"         no-undo.
def var x-tech        as char format "x(29)"        no-undo.
def var x-mo-1        as char format "x(18)"        no-undo.
def var x-mo-2        as char format "x(18)"        no-undo.
def var x-mo-3        as char format "x(18)"        no-undo.
def var x-region      as char format "x"            no-undo.
def var x-dist        as int format "999"           no-undo.
def var x-repnm       like smsn.name                no-undo.

def var temp-sls      as dec format ">>>,>>>,>>9-"  no-undo.
def var temp-cst      as dec format ">>>,>>>,>>9-"  no-undo.

def var b-slsrep      like oeel.slsrepout           no-undo.
def var e-slsrep      like oeel.slsrepout           no-undo.
def var b-slsgrp      like smsn.slstype             no-undo.
def var e-slsgrp      like smsn.slstype             no-undo.
def var b-slsmgr      like smsn.mgr                 no-undo.
def var e-slsmgr      like smsn.mgr                 no-undo.
def var b-region      as char format "x"            no-undo.
def var e-region      as char format "x"            no-undo.
def var b-dist        as dec format "999"           no-undo.
def var e-dist        as dec format "999"           no-undo.
def var b-branch      like smsn.site                no-undo.
def var e-branch      like smsn.site                no-undo.

def var p-sortfl      as char format "x"            no-undo.
def var p-rptfl       as char format "x"            no-undo.
def var p-frtfl       as logical                    no-undo.
def var p-schgfl       as logical                    no-undo.

/* ------------------------------------------------------------------------
   Variables Used To Create Trend Like Header
   -----------------------------------------------------------------------*/


DEFINE VARIABLE zrpt-dt      as date format "99/99/9999" no-undo.
DEFINE VARIABLE zrpt-dow     as char format "x(3)"   no-undo. 
DEFINE VARIABLE zrpt-time    as char format "x(5)"  no-undo.
DEFINE VARIABLE zrpt-cono    like oeeh.cono         no-undo.
DEFINE VARIABLE zrpt-user    as char format "x(08)" no-undo.
DEFINE VARIABLE zx-title     as char format "x(177)" no-undo.
DEFINE VARIABLE zx-page      as integer format ">>>9" no-undo.
DEFINE VARIABLE zdow-lst     as character format "x(30)"
           init "Sun,Mon,Tue,Wed,Thu,Fri,Sat"                 no-undo.

DEFINE VARIABLE zmonthname   as char format "x(10)" EXTENT 12 initial
["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"].



def temp-table tt-reg-det no-undo
    field cono       like arsc.cono
    field region     as char format "x"
    field dist       as dec format "999"
    field branch     like smsn.site
    field slsrep     like arsc.slsrepout
    field repnm      as char format "x(20)"
    field custno     like arsc.custno
    field custnm     like arsc.lookupnm
    field mo-1-sls   as dec format ">>>,>>>,>>9-"
    field mo-1-cst   as dec format ">>>,>>>,>>9-"
    field mo-2-sls   as dec format ">>>,>>>,>>9-"
    field mo-2-cst   as dec format ">>>,>>>,>>9-"
    field mo-3-sls   as dec format ">>>,>>>,>>9-"
    field mo-3-cst   as dec format ">>>,>>>,>>9-"
    field sls-annual as dec format ">>>,>>>,>>9-"
    field cst-annual as dec format ">>>,>>>,>>9-"
    field sls-ytd    as dec format ">>>,>>>,>>9-"
    field cst-ytd    as dec format ">>>,>>>,>>9-"
    field sls-lst-yr as dec format ">>>,>>>,>>9-"
    field cst-lst-yr as dec format ">>>,>>>,>>9-"
    field sls-plan   as dec format ">>>,>>>,>>9-"
    field gm-plan    as dec format ">>>,>>>,>>9-"
    field per-plan   as dec format ">>>9.99-"
        index k-det1     
        cono
        region
        dist
        branch
        slsrep
        sls-ytd descending
        
        index k-det2
        cono
        slsrep.
        

def temp-table tt-reg-sum no-undo
    field cono       like arsc.cono
    field region     as char format "x"
    field dist       as dec format "999"
    field branch     like smsn.site
    field mo-1-sls   as dec format ">>>,>>>,>>9-"
    field mo-1-cst   as dec format ">>>,>>>,>>9-"
    field mo-2-sls   as dec format ">>>,>>>,>>9-"
    field mo-2-cst   as dec format ">>>,>>>,>>9-"
    field mo-3-sls   as dec format ">>>,>>>,>>9-"
    field mo-3-cst   as dec format ">>>,>>>,>>9-"
    field sls-annual as dec format ">>>,>>>,>>9-"
    field cst-annual as dec format ">>>,>>>,>>9-"
    field sls-ytd    as dec format ">>>,>>>,>>9-"
    field cst-ytd    as dec format ">>>,>>>,>>9-"
    field sls-lst-yr as dec format ">>>,>>>,>>9-"
    field cst-lst-yr as dec format ">>>,>>>,>>9-"
    field sls-plan   as dec format ">>>,>>>,>>9-"
    field gm-plan    as dec format ">>>,>>>,>>9-"
    field per-plan   as dec format ">>>9.99-"
    field count      as int format ">>>>>>9"    
        index k-det1     
        cono
        region
        dist
        branch
        sls-ytd descending.


DEFINE FRAME kheadings1
  header
  "~033~105~033~046~154~061~117~033~046~153~062~123~033~046~154~060~105" at 1
         with  down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.
DEFINE FRAME kheadings1a
         zrpt-dt           at 1 space
         zrpt-dow               space
         zrpt-time              space(3)
         "Company:"             space
         zrpt-cono              space
         "Function: smrzs"      space
         "Operator:"            space
         zrpt-user              space
         "page:"                at 169
         zx-page                at 174
         "~015"                 at 178
         with  down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.
         
DEFINE FRAME kheadings1b
         "Sales Manager Sales by District/Salesman Report" at 71
         "~015"              at 178
         with  down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.





form
    "Region:"       at 5
    x-region        at 13
    "~015"          at 178
with frame f-regtitle no-box no-labels width 178.

form 
    
    x-mo-1          at 15
    x-mo-2          at 34
    x-mo-3          at 53
    "Sales"         at 107
    "Sales"         at 132
    "% of Adj%   "  at 156
    "~015"          at 178
    "Sales"         at 21
    "GM %"          at 31
    "Sales"         at 43
    "GM %"          at 53
    "Sales"         at 65
    "GM %"          at 75
    "Sales YTD"     at 85
    "GM %"          at 99
    "Last Year"     at 107
    "GM %"          at 123
    "Plan YTD"      at 132
    "GM %"          at 147
    "Plan Plan   "  at 156
    "~015"          at 178
    "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "------------------------------" at 131
    "~015" at 178
 with frame f-reghdr no-box no-labels width 178.

form 
    "Region:"       at 5
    x-region        at 13
    "District:"     at 20
    x-dist          at 30
/*    
    "Branch:"        at 35
    x-branch         at 43
*/
    "~015"          at 178
with frame f-disttitle no-box no-labels width 178.

form 
    x-mo-1          at 38
    x-mo-2          at 58
    x-mo-3          at 78
    "Sales"         at 119
    "Sales"         at 140
    "% of Adj% "    at 160
    "~015"          at 178
    "Sales"         at 43
    "GM %"          at 52
    "Sales"         at 63
    "GM %"          at 72
    "Sales"         at 83
    "GM %"          at 92
    "Sales YTD"     at 99
    "GM %"          at 112
    "Last Year"     at 119
    "GM %"          at 132
    "Plan YTD"      at 140
    "GM %"          at 152
    "Plan Plan "    at 160
    "~015"          at 178
    "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "----------------------------------------------" at 131
    "~015"          at 178
with frame f-disthdr no-box no-labels width 178.

form
    "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "-----------------------------" at 131
    "~015"           at 178
    "District:"       at 1
    x-region          at 10
    x-dist            at 11
    distsub-1-sls     at 15
    distgm1           at 28
    distsub-2-sls     at 37
    distgm2           at 50
    distsub-3-sls     at 59
    distgm3           at 72
    distsub-4-sls     at 81
    distgm4           at 96
    distsub-5-sls     at 105
    distgm5           at 120
    distsub-6-sls     at 129
    distgm6           at 144
    dist-per-plan     at 153
    gey              at 162
  /*  gex              at 168 */
    "~015"           at 178
    "~015"           at 178
with frame f-sumdistsub no-box no-labels width 178.


form
    "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "-----------------------------" at 131
    "~015"           at 178
    "Region Totals:" at 1
    regsub-1-sls     at 15
    reggm1           at 28
    regsub-2-sls     at 37
    reggm2           at 50
    regsub-3-sls     at 59
    reggm3           at 72
    regsub-4-sls     at 81
    reggm4           at 96
    regsub-5-sls     at 105
    reggm5           at 120
    regsub-6-sls     at 129
    reggm6           at 144
    reg-per-plan     at 153
    gey              at 162
  /*  gex              at 168 */
    "~015"           at 178
with frame f-sumregsub no-box no-labels width 178.


form
    "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "-----------------------------------------------" at 131
    "~015" at 178
    "Region Totals:" at 1
    regsub-1-sls     at 37
    reggm1           at 49
    regsub-2-sls     at 57
    reggm2           at 69
    regsub-3-sls     at 77
    reggm3           at 89
    regsub-4-sls     at 97
    reggm4           at 109
    regsub-5-sls     at 117
    reggm5           at 129
    regsub-6-sls     at 137
    reggm6           at 149
    reg-per-plan     at 157
    gey              at 166
 /*   gex              at 172  */
     "~015" at 178
with frame f-detregsub no-box no-labels width 178.

form
    "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "-----------------------------------------------" at 131
    "~015" at 178
    "District:"        at 1
    x-dist             at 11
    distsub-1-sls      at 37
    distgm1            at 49
    distsub-2-sls      at 57
    distgm2            at 69
    distsub-3-sls      at 77
    distgm3            at 89
    distsub-4-sls      at 97
    distgm4            at 109
    distsub-5-sls      at 117
    distgm5            at 129
    distsub-6-sls      at 137
    distgm6            at 149
    dist-per-plan      at 157
    gey                at 166
/*    gex                at 172    */
    "~015" at 178
with frame f-distsub no-box no-labels width 178.

/* Branch */

form
    "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "-----------------------------------------------" at 131
    "~015" at 178
    "Branch Tot"  at 1
    x-branch   at 12
    ":"     at 16
    branchsub-1-sls      at 37
    branchgm1            at 49
    branchsub-2-sls      at 57
    branchgm2            at 69
    branchsub-3-sls      at 77
    branchgm3            at 89
    branchsub-4-sls      at 97
    branchgm4            at 109
    branchsub-5-sls      at 117
    branchgm5            at 129
    branchsub-6-sls      at 137
    branchgm6            at 149
    branch-per-plan      at 157
    gey                at 166
/*    gex                at 172    */
    "~015" at 178
    "~015" at 178
with frame f-branchsub no-box no-labels width 178.



form 
    "District:"           at 1
    x-dist                at 11
    tt-reg-sum.mo-1-sls   at 15
    gm1                   at 28
    tt-reg-sum.mo-2-sls   at 37
    gm2                   at 50
    tt-reg-sum.mo-3-sls   at 59
    gm3                   at 72
    tt-reg-sum.sls-ytd    at 81
    gm4                   at 96
    tt-reg-sum.sls-lst-yr at 105
    gm5                   at 120
    tt-reg-sum.sls-plan   at 129
    gm6                   at 144
    tt-reg-sum.per-plan   at 153
    gey                   at 162
  /*  gex                   at 168   */
    "~015" at 178
with frame f-branchdtl no-box no-labels width 178.

form 
   "Branch: "              at 1
    x-branch                 at 11
    tt-reg-sum.mo-1-sls   at 15
    gm1                   at 28
    tt-reg-sum.mo-2-sls   at 37
    gm2                   at 50
    tt-reg-sum.mo-3-sls   at 59
    gm3                   at 72
    tt-reg-sum.sls-ytd    at 81
    gm4                   at 96
    tt-reg-sum.sls-lst-yr at 105
    gm5                   at 120
    tt-reg-sum.sls-plan   at 129
    gm6                   at 144
    tt-reg-sum.per-plan   at 153
    gey                   at 162
  /*  gex                   at 168   */
    "~015" at 178
with frame f-regbranchdtl no-box no-labels width 178.



/* Branch */


form 
    "District:"           at 1
    x-dist                at 11
    tt-reg-sum.mo-1-sls   at 15
    gm1                   at 28
    tt-reg-sum.mo-2-sls   at 37
    gm2                   at 50
    tt-reg-sum.mo-3-sls   at 59
    gm3                   at 72
    tt-reg-sum.sls-ytd    at 81
    gm4                   at 96
    tt-reg-sum.sls-lst-yr at 105
    gm5                   at 120
    tt-reg-sum.sls-plan   at 129
    gm6                   at 144
    tt-reg-sum.per-plan   at 153
    gey                   at 162
  /*  gex                   at 168   */
    "~015" at 178
with frame f-regdtl no-box no-labels width 178.

form 
    "Sales Rep:"          at 1
    tt-reg-det.slsrep     at 12
    tt-reg-det.repnm      at 17
    tt-reg-det.mo-1-sls   at 37
    gm1                   at 49
    tt-reg-det.mo-2-sls   at 57
    gm2                   at 69
    tt-reg-det.mo-3-sls   at 77
    gm3                   at 89
    tt-reg-det.sls-ytd    at 97
    gm4                   at 109
    tt-reg-det.sls-lst-yr at 117
    gm5                   at 129
    tt-reg-det.sls-plan   at 137
    gm6                   at 149
    tt-reg-det.per-plan   at 157
    gey                   at 166
/*    gex                   at 172  */
    "~015" at 178
with frame f-distdtl no-box no-labels width 178.

form
    "Report Totals:" at 1
    grndtot-1-sls    at 15
    gm1              at 28
    grndtot-2-sls    at 37
    gm2              at 50
    grndtot-3-sls    at 59
    gm3              at 72
    grndtot-4-sls    at 81
    gm4              at 96
    grndtot-5-sls    at 105
    gm5              at 120
    grndtot-6-sls    at 129
    gm6              at 144
    grnd-per-plan    at 153
    gey              at 162
  /*  gex              at 168  */
    "~015" at 178
with frame f-totdtl no-box no-labels width 178.


/* -----------------------------------------------------------------------
   Header Variable Initialization
   ----------------------------------------------------------------------*/

zrpt-user = userid("dictdb").
zrpt-dt = today.
zrpt-time = string(time, "HH:MM").
zrpt-dow = entry(weekday(today),zdow-lst).
zrpt-cono = g-cono.
zx-page = 0.




assign b-region = sapb.rangebeg[1]
       e-region = sapb.rangeend[1]
       b-dist   = dec(sapb.rangebeg[2])
       e-dist   = dec(sapb.rangeend[2])
       b-slsrep = sapb.rangebeg[3]
       e-slsrep = sapb.rangeend[3]
       b-slsgrp = sapb.rangebeg[4]
       e-slsgrp = sapb.rangeend[4]
       b-slsmgr = sapb.rangebeg[5]
       e-slsmgr = sapb.rangeend[5]
    /*   rpt-dt   = date(sapb.optvalue[1])       */
       p-rptfl  = sapb.optvalue[2]
/* Branch */
       b-branch = sapb.rangebeg[6]
       e-branch = sapb.rangeend[6].
assign b-branch = "    "
       e-branch = "~~~~~~~~".
/* Branch */


if sapb.optvalue[1] = "" then
  rpt-dt  = today.
else
  do:
  v-datein = sapb.optvalue[1].
  {p-rptdt.i}
  if string(v-dateout) = v-lowdt then
    rpt-dt  = today.
  else  
    rpt-dt  = v-dateout.
  end.

assign p-frtfl = if sapb.optvalue[3] = "yes" then true else false.
       p-schgfl = if sapb.optvalue[4] = "yes" then true else false.






/*
/* -------------------begin test area ----------------------- */


assign b-region = " " 
       e-region = "`"
       b-dist = 000 
       e-dist = 999
       b-slsrep = "    "
       e-slsrep = "````"
       b-slsgrp = "    "
       e-slsgrp = "````"
       b-slsmgr = "    "
       e-slsmgr = "````"
       rpt-dt = 12/31/1998
       p-rptfl = "d".

*/
/*--------------------end test area -------------------------- */
   assign
       rpt-mo   = MONTH(rpt-dt)
       rpt-yr   = YEAR(rpt-dt)
       end-dt   = rpt-dt.

if rpt-yr <= 2000 then
   do:
   lst-yr = rpt-yr - 1901.
   if rpt-yr < 2000 then
     tyr = rpt-yr - 1900.
   else
     tyr = rpt-yr - 2000.
   end.
else
   do:
   lst-yr = rpt-yr - 2001.
   tyr = rpt-yr - 2000.
   end.
IF rpt-mo = 12 THEN 
    ASSIGN moend-dt = DATE(01,01,rpt-yr + 1) - 1.
ELSE 
    ASSIGN moend-dt = DATE (rpt-mo + 1,01,rpt-yr) - 1.

IF moend-dt = end-dt THEN
   ASSIGN ytd-mos = rpt-mo.
ELSE DO:
   ASSIGN ytd-mos = rpt-mo - 1.
   
   IF ytd-mos = 0 THEN
      ASSIGN rpt-ytd-hdr1 = "(Annualization"
             rpt-ytd-hdr2 = " Not possible)".
   ELSE
      ASSIGN moend-dt     = DATE(rpt-mo,01,rpt-yr) - 1
             rpt-ytd-hdr1 = "Annualized As "
             rpt-ytd-hdr2 = "of " + STRING(moend-dt,"99/99/9999").
END.             

IF rpt-mo < 3 THEN
   ASSIGN beg-dt = DATE(rpt-mo + 10,01,rpt-yr - 1).
ELSE 
   ASSIGN beg-dt = DATE(rpt-mo - 2,01,rpt-yr).

ASSIGN mo-1    = MONTH(end-dt)
       yr-1    = YEAR(end-dt)
       mo-3    = MONTH(beg-dt)
       yr-3    = YEAR(beg-dt).
       
IF lst-yr LT 1998 THEN DO:
   
   IF yr-1 = yr-3 THEN
      ASSIGN beg-dt = DATE(01,01,yr-1).

END. /* if lst-yr lt 1998 */
ELSE
   ASSIGN beg-dt = DATE(01,01,lst-yr).

IF mo-1 = 01 THEN 
   ASSIGN mo-2 = 12
          yr-2 = yr-3.
ELSE 
   ASSIGN mo-2 = mo-1 - 1
          yr-2 = yr-1.

if mo-1 = 01 then
   assign
       x-mo-1 = "<----January----->"
       x-mo-2 = "<----December---->"
       x-mo-3 = "<----November---->".
             
if mo-1 = 02 then
   assign 
       x-mo-1 = "<----Febuary----->"
       x-mo-2 = "<----January----->"
       x-mo-3 = "<----December---->".
             
if mo-1 = 03 then
   assign 
       x-mo-1 = "<-----March------>"
       x-mo-2 = "<----Febuary----->"
       x-mo-3 = "<----January----->".
             
if mo-1 = 04 then
   assign 
       x-mo-1 = "<-----April------>"
       x-mo-2 = "<-----March------>"
       x-mo-3 = "<----Febuary----->".

if mo-1 = 05 then
   assign 
       x-mo-1 = "<------May------->"
       x-mo-2 = "<-----April------>"
       x-mo-3 = "<-----March------>".
           if mo-1 = 06 then
   assign 
       x-mo-1 = "<------June------>"
       x-mo-2 = "<------May------->"
       x-mo-3 = "<-----April------>".
      
if mo-1 = 07 then
   assign 
       x-mo-1 = "<-----July------->"
       x-mo-2 = "<-----June------->"
       x-mo-3 = "<------May------->".
      
if mo-1 = 08 then
   assign 
       x-mo-1 = "<-----August----->"
       x-mo-2 = "<------July------>"
       x-mo-3 = "<------June------>".
      
if mo-1 = 09 then
   assign 
       x-mo-1 = "<---September---->"
       x-mo-2 = "<-----August----->"
       x-mo-3 = "<------July------>".
      
if mo-1 = 10 then
   assign 
       x-mo-1 = "<----October----->"
       x-mo-2 = "<---September---->"
       x-mo-3 = "<-----August----->".
      
if mo-1 = 11 then
   assign 
       x-mo-1 = "<----November---->"
       x-mo-2 = "<----October----->"
       x-mo-3 = "<---September---->".
      
if mo-1 = 12 then
   assign 
       x-mo-1 = "<----December---->"
       x-mo-2 = "<----November---->"
       x-mo-3 = "<----October----->".

FOR EACH c-smss 
   WHERE c-smss.cono   = g-cono
     AND c-smss.yr      = tyr 
     and c-smss.slsreptype = true no-lock:
  
  /*
     find sasta
     where sasta.cono     = c-smss.cono
       and sasta.codeiden = "z"
       and sasta.codeval  = w-salesterr
      no-lock no-error.
      
      find sastn
     where sastn.cono     = c-smss.cono
       and sastn.codeiden = "v"
       and sastn.codeval  = w-divno
      no-lock no-error.
    */
  
     
      find smsn
     where smsn.cono                = c-smss.cono
       and smsn.slsrep              = c-smss.slsrep
      no-lock no-error.
 
     if not avail smsn then
       do:
       if    " "            >= b-slsgrp
         and " "            <= e-slsgrp
         and " "            >= b-branch
         and " "            <= e-branch
         and " "            >= b-slsmgr
         and " "            <= e-slsmgr then
           
         assign w-region = " "
                w-branch = "z999"
                w-dist   = 000.
       else
         next.
       end.
    else
      do:
      find smsn
       where smsn.cono                = c-smss.cono
         and smsn.slsrep              = c-smss.slsrep
         and smsn.slstype            >= b-slsgrp
         and smsn.slstype            <= e-slsgrp
         and smsn.mgr                >= b-slsmgr
         and smsn.mgr                <= e-slsmgr
         and smsn.site               >= b-branch
         and smsn.site               <= e-branch
         no-lock no-error.
      if avail smsn then
           
         assign w-region = substring(smsn.mgr,1,1)
                w-branch = smsn.site
                w-dist   = dec(substring(smsn.mgr,2,3)).
     else
       next.
     end.
        
     if avail c-smss then
         do:
       
         if w-region < b-region or
            w-region > e-region or
            w-dist   < b-dist   or
            w-dist   > e-dist   or
            w-branch < b-branch or
            w-branch > e-branch or
            c-smss.slsrep > e-slsrep or
            c-smss.slsrep < b-slsrep  then 
            
            next.
                                 
         find tt-reg-det
        where tt-reg-det.cono   = c-smss.cono
          and tt-reg-det.region = w-region
          and tt-reg-det.dist   = w-dist
          and tt-reg-det.branch = w-branch
          and tt-reg-det.slsrep = c-smss.slsrep
         no-lock no-error.

         if not avail tt-reg-det then do:
            create tt-reg-det.
            assign tt-reg-det.cono   = c-smss.cono
                   tt-reg-det.region = w-region
                   tt-reg-det.dist   = w-dist
                   tt-reg-det.branch = w-branch
                   tt-reg-det.slsrep = c-smss.slsrep
                   tt-reg-det.repnm  = if avail smsn then smsn.name
                                       else "No Name Avail".
                  
                   
         end. /* if not avail tt-reg-det */
         
         if mo-1 ge 3 then
           do:
           ASSIGN tt-reg-det.mo-1-sls = tt-reg-det.mo-1-sls +
                          c-smss.salesamt[mo-1]
                   tt-reg-det.mo-1-cst = tt-reg-det.mo-1-cst + 
                          c-smss.cogamt[mo-1].
                                  
           ASSIGN tt-reg-det.mo-2-sls = tt-reg-det.mo-2-sls + 
                          c-smss.salesamt[mo-2]
                   tt-reg-det.mo-2-cst = tt-reg-det.mo-2-cst + 
                          c-smss.cogamt[mo-2].
                                  
           ASSIGN tt-reg-det.mo-3-sls = tt-reg-det.mo-3-sls + 
                          c-smss.salesamt[mo-3]
                   tt-reg-det.mo-3-cst = tt-reg-det.mo-3-cst + 
                          c-smss.cogamt[mo-3].
           end.                       
         else
         if mo-1 = 2 then
           do:
           ASSIGN tt-reg-det.mo-1-sls = tt-reg-det.mo-1-sls +
                          c-smss.salesamt[mo-1]
                   tt-reg-det.mo-1-cst = tt-reg-det.mo-1-cst + 
                          c-smss.cogamt[mo-1].
                                  
           ASSIGN tt-reg-det.mo-2-sls = tt-reg-det.mo-2-sls + 
                          c-smss.salesamt[mo-2]
                   tt-reg-det.mo-2-cst = tt-reg-det.mo-2-cst + 
                          c-smss.cogamt[mo-2].
           end.                       
         else
         if mo-1 = 1 then
           do:
           ASSIGN tt-reg-det.mo-1-sls = tt-reg-det.mo-1-sls +
                          c-smss.salesamt[mo-1]
                   tt-reg-det.mo-1-cst = tt-reg-det.mo-1-cst + 
                          c-smss.cogamt[mo-1].
           end.
        
       
                                  
        do xt = 1 to rpt-mo:        
            ASSIGN tt-reg-det.sls-ytd = tt-reg-det.sls-ytd + 
                  c-smss.salesamt[xt]
                   tt-reg-det.cst-ytd = tt-reg-det.cst-ytd + 
                  c-smss.cogamt[xt].
       end.
            
      end.       /* if avail smsn */
end.             /* for each oeel, each oeeh */


for each    p-smss where p-smss.cono = g-cono
                   and   p-smss.yr = lst-yr
                   and   p-smss.slsreptype = true
                   no-lock:
      
      

      find smsn
     where smsn.cono                = p-smss.cono
       and smsn.slsrep              = p-smss.slsrep
      no-lock no-error.
 
     if not avail smsn then
       do:
       if    " "            >= b-slsgrp
         and " "            <= e-slsgrp
         and " "            >= b-branch
         and " "            <= e-branch
         and " "            >= b-slsmgr
         and " "            <= e-slsmgr then
           
         assign w-region = " "
                w-branch = "z999"
                w-dist   = 000.
       else
         next.
       end.
    else
      do:
      find smsn
       where smsn.cono                = p-smss.cono
         and smsn.slsrep              = p-smss.slsrep
         and smsn.slstype            >= b-slsgrp
         and smsn.slstype            <= e-slsgrp
         and smsn.mgr                >= b-slsmgr
         and smsn.mgr                <= e-slsmgr
         and smsn.site               >= b-branch
         and smsn.site               <= e-branch
         no-lock no-error.
      if avail smsn then
           
         assign w-region = substring(smsn.mgr,1,1)
                w-branch = smsn.site
                w-dist   = dec(substring(smsn.mgr,2,3)).
     else
       next.
     end.
       
     if avail p-smss then
         do:
       
         if w-region < b-region or
            w-region > e-region or
            w-dist   < b-dist   or
            w-dist   > e-dist   or
            w-branch < b-branch   or
            w-branch > e-branch   or
            p-smss.slsrep > e-slsrep or
            p-smss.slsrep < b-slsrep  then 
            
            next.
                                 
         find tt-reg-det
        where tt-reg-det.cono   = p-smss.cono
          and tt-reg-det.region = w-region
          and tt-reg-det.dist   = w-dist
          and tt-reg-det.branch = w-branch
          and tt-reg-det.slsrep = p-smss.slsrep
         no-lock no-error.

         if not avail tt-reg-det then do:
            create tt-reg-det.
            assign tt-reg-det.cono   = p-smss.cono
                   tt-reg-det.region = caps(w-region)
                   tt-reg-det.dist   = w-dist
                   tt-reg-det.branch = caps(w-branch)
                   tt-reg-det.slsrep = p-smss.slsrep
                   tt-reg-det.repnm  = if avail smsn then smsn.name
                                       else "No Name Avail".
                  
                   
         end. /* if not avail tt-reg-det */
         
         if mo-1 = 2 then
           do:
           if avail p-smss then
              ASSIGN tt-reg-det.mo-3-sls = tt-reg-det.mo-3-sls + 
                          p-smss.salesamt[12]
                   tt-reg-det.mo-3-cst = tt-reg-det.mo-3-cst + 
                          p-smss.cogamt[12].
           end.                       
         else
         if mo-1 = 1 then
           do:
           if avail p-smss then
             do:
             ASSIGN tt-reg-det.mo-2-sls = tt-reg-det.mo-2-sls + 
                          p-smss.salesamt[12]
                   tt-reg-det.mo-2-cst = tt-reg-det.mo-2-cst + 
                          p-smss.cogamt[12].
             ASSIGN tt-reg-det.mo-3-sls = tt-reg-det.mo-3-sls + 
                          p-smss.salesamt[11]
                   tt-reg-det.mo-3-cst = tt-reg-det.mo-3-cst + 
                          p-smss.cogamt[11].
             end.
           end.
        
       
        do xt = 1 to rpt-mo:                            
         if avail p-smss then
           assign tt-reg-det.sls-lst-yr = tt-reg-det.sls-lst-yr 
                     + p-smss.salesamt[xt] 
                   tt-reg-det.cst-lst-yr = tt-reg-det.cst-lst-yr 
                     + p-smss.cogamt[xt].
       end.
            
      end.       /* if avail smsn */
end.             /* for each oeel, each oeeh */

if p-frtfl = true then
FOR EACH zsmsq 
   WHERE zsmsq.cono   = g-cono
     AND zsmsq.year   = tyr 
     AND zsmsq.quotaty    = "ZFRT"
     NO-LOCK:
     
      find smsn
     where smsn.cono                = zsmsq.cono
       and smsn.slsrep              = zsmsq.primarykey
      no-lock no-error.
 
     if not avail smsn then
       do:
       if    " "            >= b-slsgrp
         and " "            <= e-slsgrp
         and " "            >= b-branch
         and " "            <= e-branch
         and " "            >= b-slsmgr
         and " "            <= e-slsmgr then
           
         assign w-region = " "
                w-branch = "z999"
                w-dist   = 000.
       else
         next.
       end.
    else
      do:
      find smsn
       where smsn.cono                = zsmsq.cono
         and smsn.slsrep              = zsmsq.primarykey
         and smsn.slstype            >= b-slsgrp
         and smsn.slstype            <= e-slsgrp
         and smsn.mgr                >= b-slsmgr
         and smsn.mgr                <= e-slsmgr
         and smsn.site               >= b-branch
         and smsn.site               <= e-branch
        no-lock no-error.
      if avail smsn then
           
         assign w-region = substring(smsn.mgr,1,1)
                w-branch = smsn.site
                w-dist   = dec(substring(smsn.mgr,2,3)).
     else
       next.
     end.

/*
     if w-region = "w" then
       next.
*/

     if avail smsn and smsn.commtype = "XFRT" then
        next.
        
     if avail zsmsq then
         do:
       
         if w-region < b-region or
            w-region > e-region or
            w-dist   < b-dist   or
            w-dist   > e-dist   or
            w-branch < b-branch or
            w-branch > e-branch or
            zsmsq.primarykey > e-slsrep or
            zsmsq.primarykey < b-slsrep  then 
            
            next.
                                 
         find tt-reg-det
        where tt-reg-det.cono   = zsmsq.cono
          and tt-reg-det.region = w-region
          and tt-reg-det.dist   = w-dist
          and tt-reg-det.branch = w-branch
          and tt-reg-det.slsrep = zsmsq.primarykey
         no-lock no-error.

         if not avail tt-reg-det then do:
            create tt-reg-det.
            assign tt-reg-det.cono   = zsmsq.cono
                   tt-reg-det.region = caps(w-region)
                   tt-reg-det.dist   = w-dist
                   tt-reg-det.branch = caps(w-branch)
                   tt-reg-det.slsrep = zsmsq.primarykey
                   tt-reg-det.repnm  = if avail smsn then smsn.name
                                       else "No Name Avail".
                  
                   
         end. /* if not avail tt-reg-det */
         
         if mo-1 ge 3 then
           do:
           ASSIGN tt-reg-det.mo-1-sls = tt-reg-det.mo-1-sls +
                          zsmsq.quotaamt[mo-1].
           ASSIGN tt-reg-det.mo-2-sls = tt-reg-det.mo-2-sls + 
                          zsmsq.quotaamt[mo-2].
           ASSIGN tt-reg-det.mo-3-sls = tt-reg-det.mo-3-sls + 
                          zsmsq.quotaamt[mo-3].
           end.                       
         else
         if mo-1 = 2 then
           do:
           ASSIGN tt-reg-det.mo-1-sls = tt-reg-det.mo-1-sls +
                          zsmsq.quotaamt[mo-1].
           ASSIGN tt-reg-det.mo-2-sls = tt-reg-det.mo-2-sls + 
                          zsmsq.quotaamt[mo-2].
           end.                       
         else
         if mo-1 = 1 then
           do:
           ASSIGN tt-reg-det.mo-1-sls = tt-reg-det.mo-1-sls +
                          zsmsq.quotaamt[mo-1].
           end.
        
       
                                  
        do xt = 1 to rpt-mo:        
            ASSIGN tt-reg-det.sls-ytd = tt-reg-det.sls-ytd + 
                  zsmsq.quotaamt[xt].
       end.
            
      end.       /* if avail smsn */
end.             /* for each oeel, each oeeh */

if p-frtfl = true then
FOR EACH zsmsq
   WHERE zsmsq.cono   = g-cono
     AND zsmsq.year   = lst-yr 
     AND zsmsq.quotaty    = "ZFRT"
     NO-LOCK:

   find smsn
     where smsn.cono                = zsmsq.cono
       and smsn.slsrep              = zsmsq.primarykey
      no-lock no-error.
 
     if not avail smsn then
       do:
       if    " "            >= b-slsgrp
         and " "            <= e-slsgrp
         and " "            >= b-branch
         and " "            >= e-branch
         and " "            >= b-slsmgr
         and " "            <= e-slsmgr then
           
         assign w-region = " "
                w-branch = "z999"
                w-dist   = 000.
       else
         next.
       end.
    else
      do:
      find smsn
       where smsn.cono                = zsmsq.cono
         and smsn.slsrep              = zsmsq.primarykey
         and smsn.slstype            >= b-slsgrp
         and smsn.slstype            <= e-slsgrp
         and smsn.mgr                >= b-slsmgr
         and smsn.mgr                <= e-slsmgr
         and smsn.site               >= b-branch
         and smsn.site               <= e-branch
        no-lock no-error.
      if avail smsn then
           
         assign w-region = substring(smsn.mgr,1,1)
                w-branch = smsn.site
                w-dist   = dec(substring(smsn.mgr,2,3)).
     else
       next.
     end.
/*
     if w-region = "w" then
       next.
*/

     if avail smsn and smsn.commtype = "XFRT" then
       next.
       
     if avail zsmsq then
         do:
       
         if w-region < b-region or
            w-region > e-region or
            w-dist   < b-dist   or
            w-dist   > e-dist   or
            w-branch < b-branch or
            w-branch > e-branch or
            zsmsq.primarykey > e-slsrep or
            zsmsq.primarykey < b-slsrep  then 
            
            next.
                                 
         find tt-reg-det
        where tt-reg-det.cono   = zsmsq.cono
          and tt-reg-det.region = w-region
          and tt-reg-det.dist   = w-dist
          and tt-reg-det.branch = w-branch
          and tt-reg-det.slsrep = zsmsq.primarykey
         no-lock no-error.

         if not avail tt-reg-det then do:
            create tt-reg-det.
            assign tt-reg-det.cono   = zsmsq.cono
                   tt-reg-det.region = caps(w-region)
                   tt-reg-det.dist   = w-dist
                   tt-reg-det.branch = caps(w-branch)
                   tt-reg-det.slsrep = zsmsq.primarykey
                   tt-reg-det.repnm  = if avail smsn then smsn.name
                                       else "No Name Avail".
                  
                   
         end. /* if not avail tt-reg-det */
         
         if mo-1 = 2 then
           do:
           if avail zsmsq then
              ASSIGN tt-reg-det.mo-3-sls = tt-reg-det.mo-3-sls + 
                          zsmsq.quotaamt[12].
           end.                       
         else
         if mo-1 = 1 then
           do:
           if avail zsmsq then
             do:
             ASSIGN tt-reg-det.mo-2-sls = tt-reg-det.mo-2-sls + 
                          zsmsq.quotaamt[12].
             ASSIGN tt-reg-det.mo-3-sls = tt-reg-det.mo-3-sls + 
                          zsmsq.quotaamt[11].
             end.
           end.
        
       
        do xt = 1 to rpt-mo:                            
         if avail zsmsq then
           assign tt-reg-det.sls-lst-yr = tt-reg-det.sls-lst-yr 
                     + zsmsq.quotaamt[xt]. 
       end.
            
      end.       /* if avail smsn */
end.             /* for each oeel, each oeeh */

/* Surcharge Modification */

if p-schgfl = true then
FOR EACH zsmsq 
   WHERE zsmsq.cono   = g-cono
     AND zsmsq.year   = tyr 
     AND zsmsq.quotaty    = "ZSSUR" /* Sauer Surcharges */
     NO-LOCK:
     
      find smsn
     where smsn.cono                = zsmsq.cono
       and smsn.slsrep              = zsmsq.primarykey
      no-lock no-error.
 
     if not avail smsn then
       do:
       if    " "            >= b-slsgrp
         and " "            <= e-slsgrp
         and " "            >= b-branch
         and " "            >= e-branch
         and " "            >= b-slsmgr
         and " "            <= e-slsmgr then
           
         assign w-region = " "
                w-branch = "z999"
                w-dist   = 000.
       else
         next.
       end.
    else
      do:
      find smsn
       where smsn.cono                = zsmsq.cono
         and smsn.slsrep              = zsmsq.primarykey
         and smsn.slstype            >= b-slsgrp
         and smsn.slstype            <= e-slsgrp
         and smsn.mgr                >= b-slsmgr
         and smsn.mgr                <= e-slsmgr
         and smsn.site               >= b-branch
         and smsn.site               <= e-branch
        no-lock no-error.
      if avail smsn then
           
         assign w-region = substring(smsn.mgr,1,1)
                w-branch = smsn.site
                w-dist   = dec(substring(smsn.mgr,2,3)).
     else
       next.
     end.

/*
     if w-region = "w" then
       next.


     if avail smsn and smsn.commtype = "XFRT" then
        next.
*/        
     if avail zsmsq then
         do:
       
         if w-region < b-region or
            w-region > e-region or
            w-dist   < b-dist   or
            w-dist   > e-dist   or
            w-branch < b-branch or
            w-branch > e-branch or
            zsmsq.primarykey > e-slsrep or
            zsmsq.primarykey < b-slsrep  then 
            
            next.
                                 
         find tt-reg-det
        where tt-reg-det.cono   = zsmsq.cono
          and tt-reg-det.region = w-region
          and tt-reg-det.dist   = w-dist
          and tt-reg-det.branch = w-branch
          and tt-reg-det.slsrep = zsmsq.primarykey
         no-lock no-error.

         if not avail tt-reg-det then do:
            create tt-reg-det.
            assign tt-reg-det.cono   = zsmsq.cono
                   tt-reg-det.region = caps(w-region)
                   tt-reg-det.dist   = w-dist
                   tt-reg-det.branch = caps(w-branch)
                   tt-reg-det.slsrep = zsmsq.primarykey
                   tt-reg-det.repnm  = if avail smsn then smsn.name
                                       else "No Name Avail".
                  
                   
         end. /* if not avail tt-reg-det */
         
         if mo-1 ge 3 then
           do:
           ASSIGN tt-reg-det.mo-1-sls = tt-reg-det.mo-1-sls +
                          zsmsq.quotaamt[mo-1].
           ASSIGN tt-reg-det.mo-2-sls = tt-reg-det.mo-2-sls + 
                          zsmsq.quotaamt[mo-2].
           ASSIGN tt-reg-det.mo-3-sls = tt-reg-det.mo-3-sls + 
                          zsmsq.quotaamt[mo-3].
           end.                       
         else
         if mo-1 = 2 then
           do:
           ASSIGN tt-reg-det.mo-1-sls = tt-reg-det.mo-1-sls +
                          zsmsq.quotaamt[mo-1].
           ASSIGN tt-reg-det.mo-2-sls = tt-reg-det.mo-2-sls + 
                          zsmsq.quotaamt[mo-2].
           end.                       
         else
         if mo-1 = 1 then
           do:
           ASSIGN tt-reg-det.mo-1-sls = tt-reg-det.mo-1-sls +
                          zsmsq.quotaamt[mo-1].
           end.
        
       
                                  
        do xt = 1 to rpt-mo:        
            ASSIGN tt-reg-det.sls-ytd = tt-reg-det.sls-ytd + 
                  zsmsq.quotaamt[xt].
       end.
            
      end.       /* if avail smsn */
end.             /* for each oeel, each oeeh */

if p-schgfl = true then
FOR EACH zsmsq
   WHERE zsmsq.cono   = g-cono
     AND zsmsq.year   = lst-yr 
     AND zsmsq.quotaty    = "ZSSUR" /* Sauer SurCharge */
     NO-LOCK:

   find smsn
     where smsn.cono                = zsmsq.cono
       and smsn.slsrep              = zsmsq.primarykey
      no-lock no-error.
 
     if not avail smsn then
       do:
       if    " "            >= b-slsgrp
         and " "            <= e-slsgrp
         and " "            >= b-branch
         and " "            <= e-branch
         and " "            >= b-slsmgr
         and " "            <= e-slsmgr then
           
         assign w-region = " "
                w-branch = "z999"
                w-dist   = 000.
       else
         next.
       end.
    else
      do:
      find smsn
       where smsn.cono                = zsmsq.cono
         and smsn.slsrep              = zsmsq.primarykey
         and smsn.slstype            >= b-slsgrp
         and smsn.slstype            <= e-slsgrp
         and smsn.mgr                >= b-slsmgr
         and smsn.mgr                <= e-slsmgr
         and smsn.site               >= b-branch
         and smsn.site               <= e-branch
        no-lock no-error.
      if avail smsn then
           
         assign w-region = substring(smsn.mgr,1,1)
                w-branch = smsn.site
                w-dist   = dec(substring(smsn.mgr,2,3)).
     else
       next.
     end.
/*
     if w-region = "w" then
       next.


     if avail smsn and smsn.commtype = "XFRT" then
       next.
*/       
     if avail zsmsq then
         do:
       
         if w-region < b-region or
            w-region > e-region or
            w-dist   < b-dist   or
            w-dist   > e-dist   or
            w-branch < b-branch or
            w-branch > e-branch or
            zsmsq.primarykey > e-slsrep or
            zsmsq.primarykey < b-slsrep  then 
            
            next.
                                 
         find tt-reg-det
        where tt-reg-det.cono   = zsmsq.cono
          and tt-reg-det.region = w-region
          and tt-reg-det.dist   = w-dist
          and tt-reg-det.branch = w-branch
          and tt-reg-det.slsrep = zsmsq.primarykey
         no-lock no-error.

         if not avail tt-reg-det then do:
            create tt-reg-det.
            assign tt-reg-det.cono   = zsmsq.cono
                   tt-reg-det.region = caps(w-region)
                   tt-reg-det.dist   = w-dist
                   tt-reg-det.branch = caps(w-branch)
                   tt-reg-det.slsrep = zsmsq.primarykey
                   tt-reg-det.repnm  = if avail smsn then smsn.name
                                       else "No Name Avail".
                  
                   
         end. /* if not avail tt-reg-det */
         
         if mo-1 = 2 then
           do:
           if avail zsmsq then
              ASSIGN tt-reg-det.mo-3-sls = tt-reg-det.mo-3-sls + 
                          zsmsq.quotaamt[12].
           end.                       
         else
         if mo-1 = 1 then
           do:
           if avail zsmsq then
             do:
             ASSIGN tt-reg-det.mo-2-sls = tt-reg-det.mo-2-sls + 
                          zsmsq.quotaamt[12].
             ASSIGN tt-reg-det.mo-3-sls = tt-reg-det.mo-3-sls + 
                          zsmsq.quotaamt[11].
             end.
           end.
        
       
        do xt = 1 to rpt-mo:                            
         if avail zsmsq then
           assign tt-reg-det.sls-lst-yr = tt-reg-det.sls-lst-yr 
                     + zsmsq.quotaamt[xt]. 
       end.
            
      end.       /* if avail smsn */
end.             /* for each oeel, each oeeh */



/* Surcharge Modification */



for each tt-reg-det
exclusive-lock:

    assign tt-reg-det.sls-annual = ((tt-reg-det.sls-ytd / ytd-mos) * 12)
           tt-reg-det.cst-annual = ((tt-reg-det.cst-ytd / ytd-mos) * 12).

    FIND zsmsq                            
   WHERE zsmsq.cono       = tt-reg-det.cono
     AND zsmsq.year       = rpt-yr
     AND zsmsq.quotaty    = "SR"
     AND zsmsq.primarykey = tt-reg-det.slsrep
    NO-LOCK NO-ERROR.
  
    IF AVAILABLE zsmsq THEN do:

       do a = 1 to rpt-mo:
          assign
             tt-reg-det.sls-plan = tt-reg-det.sls-plan + zsmsq.quotaamt[a]
             tt-reg-det.gm-plan  = tt-reg-det.gm-plan + zsmsq.quotagp[a].
               
       end. /* do a = 1 to rpt-mo */
            
       assign 
          tt-reg-det.per-plan =
            ((tt-reg-det.sls-ytd - tt-reg-det.cst-ytd)
             / tt-reg-det.gm-plan)
            * 100.
      
    end. /* if avail zsmsq */
    else 
       assign
          tt-reg-det.sls-plan = 0
          tt-reg-det.gm-plan  = 0.00
          tt-reg-det.per-plan = 0.00.
   find tt-reg-sum
   where tt-reg-sum.region = tt-reg-det.region
     and tt-reg-sum.dist   = tt-reg-det.dist
     and tt-reg-sum.branch = tt-reg-det.branch
    exclusive-lock no-error.
 
    if not avail tt-reg-sum then do:
       create tt-reg-sum.
       assign tt-reg-sum.cono   = tt-reg-det.cono
              tt-reg-sum.region = tt-reg-det.region
              tt-reg-sum.dist   = tt-reg-det.dist
              tt-reg-sum.branch = tt-reg-det.branch
              tt-reg-sum.count  = 0.
    end. /* if not avail */
 
    assign
       tt-reg-sum.mo-1-sls   = tt-reg-sum.mo-1-sls + tt-reg-det.mo-1-sls
       tt-reg-sum.mo-1-cst   = tt-reg-sum.mo-1-cst + tt-reg-det.mo-1-cst
       tt-reg-sum.mo-2-sls   = tt-reg-sum.mo-2-sls + tt-reg-det.mo-2-sls
       tt-reg-sum.mo-2-cst   = tt-reg-sum.mo-2-cst + tt-reg-det.mo-2-cst
       tt-reg-sum.mo-3-sls   = tt-reg-sum.mo-3-sls + tt-reg-det.mo-3-sls
       tt-reg-sum.mo-3-cst   = tt-reg-sum.mo-3-cst + tt-reg-det.mo-3-cst
       tt-reg-sum.sls-annual = tt-reg-sum.sls-annual + tt-reg-det.sls-annual
       tt-reg-sum.cst-annual = tt-reg-sum.cst-annual + tt-reg-det.cst-annual
       tt-reg-sum.sls-ytd    = tt-reg-sum.sls-ytd + tt-reg-det.sls-ytd
       tt-reg-sum.cst-ytd    = tt-reg-sum.cst-ytd + tt-reg-det.cst-ytd
       tt-reg-sum.sls-lst-yr = tt-reg-sum.sls-lst-yr + tt-reg-det.sls-lst-yr
       tt-reg-sum.cst-lst-yr = tt-reg-sum.cst-lst-yr + tt-reg-det.cst-lst-yr
       tt-reg-sum.sls-plan   = tt-reg-sum.sls-plan + tt-reg-det.sls-plan
       tt-reg-sum.gm-plan    = tt-reg-sum.gm-plan + tt-reg-det.gm-plan
       tt-reg-sum.per-plan   = if avail zsmsq then
                                 ((tt-reg-sum.sls-ytd - tt-reg-sum.cst-ytd)
                                  / tt-reg-sum.gm-plan) * 100
                               else
                                  0
       tt-reg-sum.count      = tt-reg-sum.count + 1.

end.        /* for each tt-reg-det */    



for each zsmsq                            
   WHERE zsmsq.cono       = g-cono
     AND zsmsq.year       = rpt-yr
     AND zsmsq.quotaty    = "SR"
     
    NO-LOCK:
  
    
      find smsn
      where smsn.cono                = g-cono
       and smsn.slsrep               = zsmsq.primarykey
       and smsn.slstype             >= b-slsgrp
       and smsn.slstype             <= e-slsgrp
       and smsn.mgr                 >= b-slsmgr
       and smsn.mgr                 <= e-slsmgr
       and smsn.site                >= b-branch
       and smsn.site                <= e-branch
      no-lock no-error.

          
      if avail smsn then do:
         
         assign w-region = substring(smsn.mgr,1,1)
                w-branch = smsn.site
                w-dist   = dec(substring(smsn.mgr,2,3)).

         if w-region < b-region or
            w-region > e-region or
            w-dist   < b-dist   or
            w-dist   > e-dist   or
            w-branch < b-branch or
            w-branch > e-branch or
            smsn.slsrep > e-slsrep or
            smsn.slsrep < b-slsrep  then 
            
            next.
         end.
      else
         next.  
       
       
       find tt-reg-det use-index k-det2 where 
            tt-reg-det.cono = g-cono and
            tt-reg-det.slsrep = zsmsq.primarykey  no-lock no-error.
            
       if avail tt-reg-det then
          next.

       create tt-reg-det.
       assign tt-reg-det.cono   = g-cono
              tt-reg-det.region = caps(w-region)
              tt-reg-det.dist   = w-dist
              tt-reg-det.branch = caps(w-branch)
              tt-reg-det.slsrep = zsmsq.primarykey
              tt-reg-det.repnm  = smsn.name.
           
       do a = 1 to rpt-mo:
          assign
             tt-reg-det.sls-plan = tt-reg-det.sls-plan + zsmsq.quotaamt[a]
             tt-reg-det.gm-plan  = tt-reg-det.gm-plan + zsmsq.quotagp[a].
               
       end. /* do a = 1 to rpt-mo */
            
       assign 
          tt-reg-det.per-plan = 0.
                
   find tt-reg-sum
   where tt-reg-sum.region = tt-reg-det.region
     and tt-reg-sum.dist   = tt-reg-det.dist
    exclusive-lock no-error.
 
    if not avail tt-reg-sum then do:
       create tt-reg-sum.
       assign tt-reg-sum.cono   = tt-reg-det.cono
              tt-reg-sum.region = tt-reg-det.region
              tt-reg-sum.dist   = tt-reg-det.dist
              tt-reg-sum.branch = tt-reg-det.branch
              tt-reg-sum.count  = 0.
    end. /* if not avail */
 
    assign
       tt-reg-sum.mo-1-sls   = tt-reg-sum.mo-1-sls + tt-reg-det.mo-1-sls
       tt-reg-sum.mo-1-cst   = tt-reg-sum.mo-1-cst + tt-reg-det.mo-1-cst
       tt-reg-sum.mo-2-sls   = tt-reg-sum.mo-2-sls + tt-reg-det.mo-2-sls
       tt-reg-sum.mo-2-cst   = tt-reg-sum.mo-2-cst + tt-reg-det.mo-2-cst
       tt-reg-sum.mo-3-sls   = tt-reg-sum.mo-3-sls + tt-reg-det.mo-3-sls
       tt-reg-sum.mo-3-cst   = tt-reg-sum.mo-3-cst + tt-reg-det.mo-3-cst
       tt-reg-sum.sls-annual = tt-reg-sum.sls-annual + tt-reg-det.sls-annual
       tt-reg-sum.cst-annual = tt-reg-sum.cst-annual + tt-reg-det.cst-annual
       tt-reg-sum.sls-ytd    = tt-reg-sum.sls-ytd + tt-reg-det.sls-ytd
       tt-reg-sum.cst-ytd    = tt-reg-sum.cst-ytd + tt-reg-det.cst-ytd
       tt-reg-sum.sls-lst-yr = tt-reg-sum.sls-lst-yr + tt-reg-det.sls-lst-yr
       tt-reg-sum.cst-lst-yr = tt-reg-sum.cst-lst-yr + tt-reg-det.cst-lst-yr
       tt-reg-sum.sls-plan   = tt-reg-sum.sls-plan + tt-reg-det.sls-plan
       tt-reg-sum.gm-plan    = tt-reg-sum.gm-plan + tt-reg-det.gm-plan
       tt-reg-sum.per-plan   = if avail zsmsq then
                                 ((tt-reg-sum.sls-ytd - tt-reg-sum.cst-ytd)
                                  / tt-reg-sum.gm-plan) * 100
                               else
                                  0
       tt-reg-sum.count      = tt-reg-sum.count + 1.

end.        /* for each zsmsq */    


if p-rptfl = "s" then do:
   
   for each tt-reg-sum
   exclusive-lock
   break 
         by tt-reg-sum.region
         by tt-reg-sum.dist
         by tt-reg-sum.branch
         by tt-reg-sum.per-plan descending
         by (tt-reg-sum.sls-ytd - tt-reg-sum.cst-ytd)  descending:
      
      assign
         regsub-1-sls = regsub-1-sls + tt-reg-sum.mo-1-sls
         regsub-1-cst = regsub-1-cst + tt-reg-sum.mo-1-cst
         regsub-2-sls = regsub-2-sls + tt-reg-sum.mo-2-sls
         regsub-2-cst = regsub-2-cst + tt-reg-sum.mo-2-cst
         regsub-3-sls = regsub-3-sls + tt-reg-sum.mo-3-sls
         regsub-3-cst = regsub-3-cst + tt-reg-sum.mo-3-cst
         regsub-4-sls = regsub-4-sls + tt-reg-sum.sls-ytd
         regsub-4-cst = regsub-4-cst + tt-reg-sum.cst-ytd
         regsub-5-sls = regsub-5-sls + tt-reg-sum.sls-lst-yr
         regsub-5-cst = regsub-5-cst + tt-reg-sum.cst-lst-yr
         regsub-6-sls = regsub-6-sls + tt-reg-sum.sls-plan
         regsub-6-gm  = regsub-6-gm  + tt-reg-sum.gm-plan
         regcnt       = regcnt + tt-reg-sum.count
         grndcnt      = grndcnt + tt-reg-sum.count.

 /* Branch */     
      assign
         distsub-1-sls = distsub-1-sls + tt-reg-sum.mo-1-sls
         distsub-1-cst = distsub-1-cst + tt-reg-sum.mo-1-cst
         distsub-2-sls = distsub-2-sls + tt-reg-sum.mo-2-sls
         distsub-2-cst = distsub-2-cst + tt-reg-sum.mo-2-cst
         distsub-3-sls = distsub-3-sls + tt-reg-sum.mo-3-sls
         distsub-3-cst = distsub-3-cst + tt-reg-sum.mo-3-cst
         distsub-4-sls = distsub-4-sls + tt-reg-sum.sls-ytd
         distsub-4-cst = distsub-4-cst + tt-reg-sum.cst-ytd
         distsub-5-sls = distsub-5-sls + tt-reg-sum.sls-lst-yr
         distsub-5-cst = distsub-5-cst + tt-reg-sum.cst-lst-yr
         distsub-6-sls = distsub-6-sls + tt-reg-sum.sls-plan
         distsub-6-gm  = distsub-6-gm  + tt-reg-sum.gm-plan
         distcnt       = distcnt + tt-reg-sum.count.


/* Branch */ 

      if first-of(tt-reg-sum.region) then do:
         
         x-region = tt-reg-sum.region.
         
         first-time = false.
         
         run write-sum-hdgs.
                                               
      end. /* if first-of(tt-reg-sum.region) */
      
   
      
      
      assign
         reggm6 = reggm6 + tt-reg-sum.gm-plan.
         
      if tt-reg-sum.sls-plan <> 0 then
         tt-reg-sum.per-plan =
           ((tt-reg-sum.sls-ytd - tt-reg-sum.cst-ytd)
             / tt-reg-sum.gm-plan) * 100.
      else
         tt-reg-sum.per-plan = 0.
                                    
      if tt-reg-sum.per-plan > 999 then
         tt-reg-sum.per-plan = 999.
      else
      if tt-reg-sum.per-plan < -999 then
         tt-reg-sum.per-plan = -999.
                                    
                  
      if tt-reg-sum.mo-1-sls <> 0 then
         gm1 = ((tt-reg-sum.mo-1-sls - tt-reg-sum.mo-1-cst) /
                 tt-reg-sum.mo-1-sls) * 100.
      else
         gm1 = 0.00.
      
      if tt-reg-sum.mo-2-sls <> 0 then
         gm2 = ((tt-reg-sum.mo-2-sls - tt-reg-sum.mo-2-cst) / 
                 tt-reg-sum.mo-2-sls) * 100.
      else 
         gm2 = 0.00.
      
      if tt-reg-sum.mo-3-sls <> 0 then
         gm3 = ((tt-reg-sum.mo-3-sls - tt-reg-sum.mo-3-cst) /
                 tt-reg-sum.mo-3-sls) * 100.
      else
         gm3 = 0.00.
      
      if tt-reg-sum.sls-ytd <> 0 then
         gm4 = ((tt-reg-sum.sls-ytd - tt-reg-sum.cst-ytd) / 
                 tt-reg-sum.sls-ytd) * 100.
      else
         gm4 = 0.00.
      
      if tt-reg-sum.sls-lst-yr <> 0 then
         gm5 = ((tt-reg-sum.sls-lst-yr - tt-reg-sum.cst-lst-yr) /
                 tt-reg-sum.sls-lst-yr) * 100.
      else
         gm5 = 0.00.
      
      
      if tt-reg-sum.sls-plan <> 0 then
         gm6 = (tt-reg-sum.gm-plan /
                 tt-reg-sum.sls-plan) * 100.
      else
         gm6 = 0.00.
      
      run fix-gm.
      
      assign 
        x-dist    = tt-reg-sum.dist
        x-region  = tt-reg-sum.region
        x-branch  = tt-reg-sum.branch.
      
      if line-counter + 1 > 45 then do:
        run write-sum-hdgs.
      end.

    
      if round(tt-reg-sum.per-plan,0) > 69 and tt-reg-sum.gm-plan <> 0 then
        gey = tt-reg-sum.per-plan.
      else
        gey = 0.   
      run gey_determination.

      gex = truncate(((gm4 - gm6) / .25),0) + 100.     
      if gm6 = 0 then
        gex = 0.
      
      disp 
         x-branch
         tt-reg-sum.mo-1-sls
         gm1
         tt-reg-sum.mo-2-sls
         gm2
         tt-reg-sum.mo-3-sls
         gm3
         tt-reg-sum.sls-ytd
         gm4
         tt-reg-sum.sls-lst-yr
         gm5
         tt-reg-sum.sls-plan
         gm6
         tt-reg-sum.per-plan
         gey
     /*    gex  */
      with frame f-regbranchdtl down.
      down with frame f-regbranchdtl.

/* Branch */
      
      
      if last-of(tt-reg-sum.dist) then do:
      
         if distsub-1-sls <> 0 then
            distgm1 = 
            ((distsub-1-sls - distsub-1-cst) / distsub-1-sls) * 100.
         else
            distgm1 = 0.00.
            
         if distsub-2-sls <> 0 then
            distgm2 = 
            ((distsub-2-sls - distsub-2-cst) / distsub-2-sls) * 100.
         else
            distgm2 = 0.00.

         if distsub-3-sls <> 0 then
            distgm3 = 
            ((distsub-3-sls - distsub-3-cst) / distsub-3-sls) * 100.
         else
            distgm3 = 0.00.
         
         if distsub-4-sls <> 0 then
            distgm4 = 
            ((distsub-4-sls - distsub-4-cst) / distsub-4-sls) * 100.
         else
            distgm4 = 0.00.

         if distsub-5-sls <> 0 then
            distgm5 = 
            ((distsub-5-sls - distsub-5-cst) / distsub-5-sls) * 100.
         else
            distgm5 = 0.00.

         if distsub-6-sls <> 0 then
            distgm6 = 
            (distsub-6-gm / distsub-6-sls) * 100.
         else
            distgm6 = 0.00.


        
            
         if distsub-6-gm <> 0 then
            dist-per-plan =
             ((distsub-4-sls - distsub-4-cst) / distsub-6-gm) * 100.
         else 
            dist-per-plan = 0.00.
         
         run fix-distgm.

         if line-counter + 1 > 45 then do:
            run write-sum-hdgs.
         end.

    
        if round(dist-per-plan,0) > 69 and distsub-6-gm <> 0 then
          gey = dist-per-plan.  
        else
          gey = 0.   
 
         run gey_determination.

        gex = truncate(((distgm4 - distgm6) / .25),0) + 100.     
        if distgm6 = 0 then
           gex = 0.
              
         disp 
            x-region
            x-dist
            distsub-1-sls
            distgm1
            distsub-2-sls
            distgm2
            distsub-3-sls
            distgm3
            distsub-4-sls
            distgm4
            distsub-5-sls
            distgm5
            distsub-6-sls
            distgm6
            dist-per-plan
            gey
        /*    gex   */
         with frame f-sumdistsub down.
         down with frame f-sumdistsub.
         assign
            distsub-1-sls  = 0
            distsub-1-cst  = 0
            distsub-2-sls  = 0
            distsub-2-cst  = 0
            distsub-3-sls  = 0
            distsub-3-cst  = 0
            distsub-4-sls  = 0
            distsub-4-cst  = 0
            distsub-5-sls  = 0
            distsub-5-cst  = 0
            distsub-6-sls  = 0
            distsub-6-gm   = 0
            distgm6        = 0
            distcnt        = 0
            dist-per-plan  = 0.
            
      end. /* if last-of(tt-reg-sum.dist) */
   
/* Branch */      
      if last-of(tt-reg-sum.region) then do:
      
         if regsub-1-sls <> 0 then
            reggm1 = ((regsub-1-sls - regsub-1-cst) / regsub-1-sls) * 100.
         else
            reggm1 = 0.00.
            
         if regsub-2-sls <> 0 then
            reggm2 = ((regsub-2-sls - regsub-2-cst) / regsub-2-sls) * 100.
         else
            reggm2 = 0.00.

         if regsub-3-sls <> 0 then
            reggm3 = ((regsub-3-sls - regsub-3-cst) / regsub-3-sls) * 100.
         else
            reggm3 = 0.00.
         
         if regsub-4-sls <> 0 then
            reggm4 = ((regsub-4-sls - regsub-4-cst) / regsub-4-sls) * 100.
         else
            reggm4 = 0.00.

         if regsub-5-sls <> 0 then
            reggm5 = ((regsub-5-sls - regsub-5-cst) / regsub-5-sls) * 100.
         else
            reggm5 = 0.00.

         if regsub-6-sls <> 0 then
            reggm6 = (regsub-6-gm / regsub-6-sls) * 100.
         else
            reggm6 = 0.00.


        
            
         if regsub-6-gm <> 0 then
            reg-per-plan =
             ((regsub-4-sls - regsub-4-cst) / regsub-6-gm) * 100.
         else 
            reg-per-plan = 0.00.
         
         run fix-reggm.

         if line-counter + 1 > 45 then do:
            run write-sum-hdgs.
         end.

    
        if round(reg-per-plan,0) > 69 and regsub-6-gm <> 0 then
          gey = reg-per-plan.  
        else
          gey = 0.   
 
         run gey_determination.

        gex = truncate(((reggm4 - reggm6) / .25),0) + 100.     
        if reggm6 = 0 then
           gex = 0.
              
         disp 
            regsub-1-sls
            reggm1
            regsub-2-sls
            reggm2
            regsub-3-sls
            reggm3
            regsub-4-sls
            reggm4
            regsub-5-sls
            reggm5
            regsub-6-sls
            reggm6
            reg-per-plan
            gey
        /*    gex   */
         with frame f-sumregsub down.
         down with frame f-sumregsub.

         assign
            grndtot-1-sls = grndtot-1-sls + regsub-1-sls
            grndtot-1-cst = grndtot-1-cst + regsub-1-cst
            grndtot-2-sls = grndtot-2-sls + regsub-2-sls
            grndtot-2-cst = grndtot-2-cst + regsub-2-cst
            grndtot-3-sls = grndtot-3-sls + regsub-3-sls
            grndtot-3-cst = grndtot-3-cst + regsub-3-cst
            grndtot-4-sls = grndtot-4-sls + regsub-4-sls
            grndtot-4-cst = grndtot-4-cst + regsub-4-cst
            grndtot-5-sls = grndtot-5-sls + regsub-5-sls
            grndtot-5-cst = grndtot-5-cst + regsub-5-cst
            grndtot-6-sls = grndtot-6-sls + regsub-6-sls
            grndtot-6-gm  = grndtot-6-gm  + regsub-6-gm
            regsub-1-sls  = 0
            regsub-1-cst  = 0
            regsub-2-sls  = 0
            regsub-2-cst  = 0
            regsub-3-sls  = 0
            regsub-3-cst  = 0
            regsub-4-sls  = 0
            regsub-4-cst  = 0
            regsub-5-sls  = 0
            regsub-5-cst  = 0
            regsub-6-sls  = 0
            regsub-6-gm   = 0
            reggm6        = 0
            regcnt        = 0
            reg-per-plan  = 0.
            
      end. /* if last-of(tt-reg-sum.region) */
   end.    /* for each tt-reg-sum */      
end.       /* if p-rptfl */
else do:

   for each tt-reg-det
   break by tt-reg-det.region
         by tt-reg-det.dist
         by tt-reg-det.branch
         by tt-reg-det.per-plan descending
         by (tt-reg-det.sls-ytd  - tt-reg-det.cst-ytd) descending
         by tt-reg-det.slsrep:

      assign
         distsub-1-sls = distsub-1-sls + tt-reg-det.mo-1-sls
         distsub-1-cst = distsub-1-cst + tt-reg-det.mo-1-cst
         distsub-2-sls = distsub-2-sls + tt-reg-det.mo-2-sls
         distsub-2-cst = distsub-2-cst + tt-reg-det.mo-2-cst
         distsub-3-sls = distsub-3-sls + tt-reg-det.mo-3-sls
         distsub-3-cst = distsub-3-cst + tt-reg-det.mo-3-cst
         distsub-4-sls = distsub-4-sls + tt-reg-det.sls-ytd
         distsub-4-cst = distsub-4-cst + tt-reg-det.cst-ytd
         distsub-5-sls = distsub-5-sls + tt-reg-det.sls-lst-yr
         distsub-5-cst = distsub-5-cst + tt-reg-det.cst-lst-yr
         distsub-6-sls = distsub-6-sls + tt-reg-det.sls-plan
         distsub-6-gm  = distsub-6-gm  + tt-reg-det.gm-plan
         distgm6       = distgm6 + tt-reg-det.gm-plan
         reggm6        = reggm6 + tt-reg-det.gm-plan
         grndgm-plan   = grndgm-plan + tt-reg-det.gm-plan
         regcnt        = regcnt + 1
         distcnt       = distcnt + 1
         grndcnt       = grndcnt + 1.
/* Branch */


      assign
         branchsub-1-sls = branchsub-1-sls + tt-reg-det.mo-1-sls
         branchsub-1-cst = branchsub-1-cst + tt-reg-det.mo-1-cst
         branchsub-2-sls = branchsub-2-sls + tt-reg-det.mo-2-sls
         branchsub-2-cst = branchsub-2-cst + tt-reg-det.mo-2-cst
         branchsub-3-sls = branchsub-3-sls + tt-reg-det.mo-3-sls
         branchsub-3-cst = branchsub-3-cst + tt-reg-det.mo-3-cst
         branchsub-4-sls = branchsub-4-sls + tt-reg-det.sls-ytd
         branchsub-4-cst = branchsub-4-cst + tt-reg-det.cst-ytd
         branchsub-5-sls = branchsub-5-sls + tt-reg-det.sls-lst-yr
         branchsub-5-cst = branchsub-5-cst + tt-reg-det.cst-lst-yr
         branchsub-6-sls = branchsub-6-sls + tt-reg-det.sls-plan
         branchsub-6-gm  = branchsub-6-gm  + tt-reg-det.gm-plan
         branchgm6       = branchgm6 + tt-reg-det.gm-plan
         branchcnt       = branchcnt + 1.

/* Branch */         

      if first-of(tt-reg-det.region) or
         first-of(tt-reg-det.dist)  
         then do:
         
          
         assign
            x-region = tt-reg-det.region
            x-dist   = tt-reg-det.dist
            x-branch = tt-reg-det.branch.

         run write-det-hdgs.

      end.
      
      if tt-reg-det.mo-1-sls <> 0 then
         gm1 = ((tt-reg-det.mo-1-sls - tt-reg-det.mo-1-cst) / 
                 tt-reg-det.mo-1-sls) * 100.
      else
         gm1 = 0.00.
      
      if tt-reg-det.mo-2-sls <> 0 then
         gm2 = ((tt-reg-det.mo-2-sls - tt-reg-det.mo-2-cst) / 
                 tt-reg-det.mo-2-sls) * 100.
      else 
         gm2 = 0.00.
      
      if tt-reg-det.mo-3-sls <> 0 then
         gm3 = ((tt-reg-det.mo-3-sls - tt-reg-det.mo-3-cst) / 
                 tt-reg-det.mo-3-sls) * 100.
      else
         gm3 = 0.00.
      
      if tt-reg-det.sls-ytd <> 0 then
         gm4 = ((tt-reg-det.sls-ytd - tt-reg-det.cst-ytd) / 
                 tt-reg-det.sls-ytd) * 100.
      else
         gm4 = 0.00.
      
      if tt-reg-det.sls-lst-yr <> 0 then
         gm5 = ((tt-reg-det.sls-lst-yr - tt-reg-det.cst-lst-yr) /
                 tt-reg-det.sls-lst-yr) * 100.
      else
         gm5 = 0.00.

      if tt-reg-det.sls-plan <> 0 then
         gm6 = (tt-reg-det.gm-plan / tt-reg-det.sls-plan) * 100.
      else
         gm6 = 0.00.
      
      
      run fix-gm.
      
      if line-counter + 1 > 45 then do:
        
         run write-det-hdgs.
      end.
     
            
      run fix-gm.
      
      if line-counter + 1 > page-size then do:
         page.
         run write-det-hdgs.
      end.
      
    
      if round(tt-reg-det.per-plan,0) > 69 and tt-reg-det.gm-plan <> 0 then
        gey = tt-reg-det.per-plan.  
      else
        gey = 0.   
 
      run gey_determination.
      
      gex = truncate(((gm4 - gm6) / .25),0) + 100.
      if gm6 = 0 then
         gex = 0.
      disp 
         tt-reg-det.slsrep
         tt-reg-det.repnm
         tt-reg-det.mo-1-sls
         gm1
         tt-reg-det.mo-2-sls
         gm2
         tt-reg-det.mo-3-sls
         gm3
         tt-reg-det.sls-ytd
         gm4
         tt-reg-det.sls-lst-yr
         gm5
         tt-reg-det.sls-plan
         gm6
         tt-reg-det .per-plan
         gey
    /*     gex    */
      with frame f-distdtl down.
      down with frame f-distdtl.
/* Branch */

      if last-of(tt-reg-det.branch) then do:
        assign x-branch = tt-reg-det.branch.
         if branchsub-1-sls <> 0 then
            branchgm1 = 
            ((branchsub-1-sls - branchsub-1-cst) / branchsub-1-sls) * 100.
         else
            branchgm1 = 0.00.
            
         if branchsub-2-sls <> 0 then
            branchgm2 = 
            ((branchsub-2-sls - branchsub-2-cst) / branchsub-2-sls) * 100.
         else
            branchgm2 = 0.00.

         if branchsub-3-sls <> 0 then
            branchgm3 = 
            ((branchsub-3-sls - branchsub-3-cst) / branchsub-3-sls) * 100.
         else
            branchgm3 = 0.00.
         
         if branchsub-4-sls <> 0 then
            branchgm4 = 
            ((branchsub-4-sls - branchsub-4-cst) / branchsub-4-sls) * 100.
         else
            branchgm4 = 0.00.

         if branchsub-5-sls <> 0 then
            branchgm5 = 
            ((branchsub-5-sls - branchsub-5-cst) / branchsub-5-sls) * 100.
         else
            branchgm5 = 0.00.

         if branchsub-6-gm <> 0 then
            branch-per-plan =
             ((branchsub-4-sls - branchsub-4-cst)
              / branchsub-6-gm) * 100.
         else
            branch-per-plan = 0.00.
         
         if branchsub-6-sls <> 0 then
            branchgm6 = (branchsub-6-gm / branchsub-6-sls) * 100.
            
         run fix-branchgm.
         
         if line-counter + 1 > page-size then do:
            page.
            run write-det-hdgs.
         end.
        
    
        if round(branch-per-plan,0) > 69 and branchsub-6-gm <> 0 then
          gey = branch-per-plan.  
        else
          gey = 0.  
           
        run gey_determination.
   
        gex = truncate(((branchgm4 - branchgm6) / .25),0) + 100.
        if branchgm6 = 0 then
          gex = 0.
         disp 
            x-branch
            branchsub-1-sls
            branchgm1
            branchsub-2-sls
            branchgm2
            branchsub-3-sls
            branchgm3
            branchsub-4-sls
            branchgm4
            branchsub-5-sls
            branchgm5
            branchsub-6-sls
            branchgm6
            branch-per-plan
            gey
     /*       gex      */
         with frame f-branchsub down.
         down with frame f-branchsub.
         assign
            branchsub-1-sls = 0
            branchsub-1-cst = 0
            branchsub-2-sls = 0
            branchsub-2-cst = 0
            branchsub-3-sls = 0
            branchsub-3-cst = 0
            branchsub-4-sls = 0
            branchsub-4-cst = 0
            branchsub-5-sls = 0
            branchsub-5-cst = 0
            branchsub-6-sls = 0
            branchgm6       = 0
            branchcnt       = 0
            branchsub-6-gm  = 0
            branch-per-plan = 0.

      end. /* if last-of(tt-reg-det.branch) */
 
 
/* Branch */      
      if last-of(tt-reg-det.dist) then do:
      
         if distsub-1-sls <> 0 then
            distgm1 = ((distsub-1-sls - distsub-1-cst) / distsub-1-sls) * 100.
         else
            distgm1 = 0.00.
            
         if distsub-2-sls <> 0 then
            distgm2 = ((distsub-2-sls - distsub-2-cst) / distsub-2-sls) * 100.
         else
            distgm2 = 0.00.

         if distsub-3-sls <> 0 then
            distgm3 = ((distsub-3-sls - distsub-3-cst) / distsub-3-sls) * 100.
         else
            distgm3 = 0.00.
         
         if distsub-4-sls <> 0 then
            distgm4 = ((distsub-4-sls - distsub-4-cst) / distsub-4-sls) * 100.
         else
            distgm4 = 0.00.

         if distsub-5-sls <> 0 then
            distgm5 = ((distsub-5-sls - distsub-5-cst) / distsub-5-sls) * 100.
         else
            distgm5 = 0.00.

         if distsub-6-gm <> 0 then
            dist-per-plan =
             ((distsub-4-sls - distsub-4-cst)
              / distsub-6-gm) * 100.
         else
            dist-per-plan = 0.00.
         
         if distsub-6-sls <> 0 then
            distgm6 = (distsub-6-gm / distsub-6-sls) * 100.
            
         run fix-distgm.
         
         if line-counter + 1 > page-size then do:
            page.
            run write-det-hdgs.
         end.
        
    
        if round(dist-per-plan,0) > 69 and distsub-6-gm <> 0 then
          gey = dist-per-plan.  
        else
          gey = 0.  
           
        run gey_determination.
   
        gex = truncate(((distgm4 - distgm6) / .25),0) + 100.
        if distgm6 = 0 then
          gex = 0.
         disp 
            distsub-1-sls
            distgm1
            distsub-2-sls
            distgm2
            distsub-3-sls
            distgm3
            distsub-4-sls
            distgm4
            distsub-5-sls
            distgm5
            distsub-6-sls
            distgm6
            dist-per-plan
            gey
     /*       gex      */
         with frame f-distsub down.
         down with frame f-distsub.
         
         assign
            regsub-1-sls  = regsub-1-sls + distsub-1-sls
            regsub-1-cst  = regsub-1-cst + distsub-1-cst
            regsub-2-sls  = regsub-2-sls + distsub-2-sls
            regsub-2-cst  = regsub-2-cst + distsub-2-cst
            regsub-3-sls  = regsub-3-sls + distsub-3-sls
            regsub-3-cst  = regsub-3-cst + distsub-3-cst
            regsub-4-sls  = regsub-4-sls + distsub-4-sls
            regsub-4-cst  = regsub-4-cst + distsub-4-cst
            regsub-5-sls  = regsub-5-sls + distsub-5-sls
            regsub-5-cst  = regsub-5-cst + distsub-5-cst
            regsub-6-sls  = regsub-6-sls + distsub-6-sls
            regsub-6-gm   = regsub-6-gm + distsub-6-gm 
            distsub-1-sls = 0
            distsub-1-cst = 0
            distsub-2-sls = 0
            distsub-2-cst = 0
            distsub-3-sls = 0
            distsub-3-cst = 0
            distsub-4-sls = 0
            distsub-4-cst = 0
            distsub-5-sls = 0
            distsub-5-cst = 0
            distsub-6-sls = 0
            distgm6       = 0
            distcnt       = 0
            distsub-6-gm  = 0
            dist-per-plan = 0.

      end. /* if last-of(tt-reg-det.dist) */
       
      
      if last-of(tt-reg-det.region) then do:         
         if regsub-1-sls <> 0 then
            reggm1 = ((regsub-1-sls - regsub-1-cst) / regsub-1-sls) * 100.
         else
            reggm1 = 0.00.
            
         if regsub-2-sls <> 0 then
            reggm2 = ((regsub-2-sls - regsub-2-cst) / regsub-2-sls) * 100.
         else
            reggm2 = 0.00.

         if regsub-3-sls <> 0 then
            reggm3 = ((regsub-3-sls - regsub-3-cst) / regsub-3-sls) * 100.
         else
            reggm3 = 0.00.
         
         if regsub-4-sls <> 0 then
            reggm4 = ((regsub-4-sls - regsub-4-cst) / regsub-4-sls) * 100.
         else
            reggm4 = 0.00.

         if regsub-5-sls <> 0 then
            reggm5 = ((regsub-5-sls - regsub-5-cst) / regsub-5-sls) * 100.
         else
            reggm5 = 0.00.

         if regsub-6-gm <> 0 then
            reg-per-plan =
            ((regsub-4-sls - regsub-4-cst)
              / regsub-6-gm) * 100.
         else
            reg-per-plan = 0.00.

         if regsub-6-sls <> 0 then
            reggm6 = (regsub-6-gm / regsub-6-sls) * 100.
         else
            reggm6 = 0.00.
         
         run fix-reggm.
         
         if line-counter + 1 > 45 then do:
            run write-det-hdgs.
         end.
         
    
        if round(reg-per-plan,0) > 69 and regsub-6-gm <> 0 then
          gey = reg-per-plan.  
        else
          gey = 0.   
 
        run gey_determination.

         gex = truncate(((reggm4 - reggm6) / .25),0) + 100.     
         if reggm6 = 0 then
           gex = 0.

         disp 
            regsub-1-sls
            reggm1
            regsub-2-sls
            reggm2
            regsub-3-sls
            reggm3
            regsub-4-sls
            reggm4
            regsub-5-sls
            reggm5
            regsub-6-sls
            reggm6
            reg-per-plan
            gey
         /*   gex   */
         with frame f-detregsub down.
         down with frame f-detregsub.

         assign
            grndtot-1-sls = grndtot-1-sls + regsub-1-sls
            grndtot-1-cst = grndtot-1-cst + regsub-1-cst
            grndtot-2-sls = grndtot-2-sls + regsub-2-sls
            grndtot-2-cst = grndtot-2-cst + regsub-2-cst
            grndtot-3-sls = grndtot-3-sls + regsub-3-sls
            grndtot-3-cst = grndtot-3-cst + regsub-3-cst
            grndtot-4-sls = grndtot-4-sls + regsub-4-sls
            grndtot-4-cst = grndtot-4-cst + regsub-4-cst
            grndtot-5-sls = grndtot-5-sls + regsub-5-sls
            grndtot-5-cst = grndtot-5-cst + regsub-5-cst
            grndtot-6-sls = grndtot-6-sls + regsub-6-sls
            grndtot-6-gm  = grndtot-6-gm  + regsub-6-gm
            regsub-1-sls  = 0
            regsub-1-cst  = 0
            regsub-2-sls  = 0
            regsub-2-cst  = 0
            regsub-3-sls  = 0
            regsub-3-cst  = 0
            regsub-4-sls  = 0
            regsub-4-cst  = 0
            regsub-5-sls  = 0
            regsub-5-cst  = 0
            regsub-6-sls  = 0
            reggm6        = 0
            regcnt        = 0
            regsub-6-gm   = 0
            reg-per-plan  = 0.
            
      end. /* if last-of(tt-reg-sum.region) */
   end.    /* for each tt-reg-det */
end.       /* else do */

if grndtot-1-sls <> 0 then
   gm1 = ((grndtot-1-sls - grndtot-1-cst) / grndtot-1-sls) * 100.
else
   gm1 = 0.00. 
            
if grndtot-2-sls <> 0 then
   gm2 = ((grndtot-2-sls - grndtot-2-cst) / grndtot-2-sls) * 100.
else
   gm2 = 0.00.

if grndtot-3-sls <> 0 then
   gm3 = ((grndtot-3-sls - grndtot-3-cst) / grndtot-3-sls) * 100.
else
   gm3 = 0.00.
        
if grndtot-4-sls <> 0 then
   gm4 = ((grndtot-4-sls - grndtot-4-cst) / grndtot-4-sls) * 100.
else
   gm4 = 0.00.

if grndtot-5-sls <> 0 then
   gm5 = ((grndtot-5-sls - grndtot-5-cst) / grndtot-5-sls) * 100.
else
   gm5 = 0.00.

if grndtot-6-gm <> 0 then
   grnd-per-plan =
    ((grndtot-4-sls - grndtot-4-cst)
     / grndtot-6-gm) * 100.
else 
   grnd-per-plan = 0.00.

if grndtot-6-sls <> 0 then
   gm6 = (grndtot-6-gm / grndtot-6-sls) * 100.
else 
   gm6 = 0.00.
   
run fix-gm.

run write-sum-hdgs.


    
if round(grnd-per-plan,0) > 69 and grndtot-6-gm <> 0 then
   gey = grnd-per-plan.  
else
   gey = 0.   
 
run gey_determination.


gex = truncate(((gm4 - gm6) / .25),0) + 100.     

if gm6 = 0 then
  gex = 0.
           
         
disp 
   grndtot-1-sls
   gm1
   grndtot-2-sls
   gm2
   grndtot-3-sls
   gm3
   grndtot-4-sls
   gm4
   grndtot-5-sls
   gm5
   grndtot-6-sls
   gm6
   grnd-per-plan
   gey
/*   gex   */
with frame f-totdtl down.
down with frame f-totdtl.

PROCEDURE fix-gm.
    IF gm1 LT -999.99 THEN
       ASSIGN gm1 = -999.99.
    ELSE 
       IF gm1 GT 999.99 THEN
          ASSIGN gm1 = 999.99.
    
    IF gm2 LT -999.99 THEN
       ASSIGN gm2 = -999.99.
    ELSE 
       IF gm2 GT 999.99 THEN
          ASSIGN gm2 = 999.99.
    
    IF gm3 LT -999.99 THEN
       ASSIGN gm3 = -999.99.
    ELSE 
       IF gm3 GT 999.99 THEN
          ASSIGN gm3 = 999.99.
    
    IF gm4 LT -999.99 THEN
       ASSIGN gm4 = -999.99.
    ELSE 
       IF gm4 GT 999.99 THEN
          ASSIGN gm4 = 999.99.
    
    IF gm5 LT -999.99 THEN
       ASSIGN gm5 = -999.99.
    ELSE 
       IF gm5 GT 999.99 THEN
          ASSIGN gm5 = 999.99.
    
    IF gm6 LT -999.99 THEN
       ASSIGN gm6 = -999.99.
    ELSE 
       IF gm6 GT 999.99 THEN
          ASSIGN gm6 = 999.99.


    RETURN.
END PROCEDURE. /* fix-gm */

PROCEDURE fix-reggm.
    IF reggm1 LT -999.99 THEN
       ASSIGN reggm1 = -999.99.
    ELSE 
       IF reggm1 GT 999.99 THEN
          ASSIGN reggm1 = 999.99.
    
    IF reggm2 LT -999.99 THEN
       ASSIGN reggm2 = -999.99.
    ELSE 
       IF reggm2 GT 999.99 THEN
          ASSIGN reggm2 = 999.99.
    
    IF reggm3 LT -999.99 THEN
       ASSIGN reggm3 = -999.99.
    ELSE 
       IF reggm3 GT 999.99 THEN
          ASSIGN reggm3 = 999.99.
    
    IF reggm4 LT -999.99 THEN
       ASSIGN reggm4 = -999.99.
    ELSE 
       IF reggm4 GT 999.99 THEN
          ASSIGN reggm4 = 999.99.
    
    IF reggm5 LT -999.99 THEN
       ASSIGN reggm5 = -999.99.
    ELSE 
       IF reggm5 GT 999.99 THEN
          ASSIGN reggm5 = 999.99.
    
    IF reggm6 LT -999.99 THEN
       ASSIGN reggm6 = -999.99.
    ELSE 
       IF reggm6 GT 999.99 THEN
          ASSIGN reggm6 = 999.99.

    RETURN.
END PROCEDURE. /* fix-gm */

PROCEDURE fix-distgm.
    IF distgm1 LT -999.99 THEN
       ASSIGN distgm1 = -999.99.
    ELSE 
       IF distgm1 GT 999.99 THEN
          ASSIGN distgm1 = 999.99.
    
    IF distgm2 LT -999.99 THEN
       ASSIGN distgm2 = -999.99.
    ELSE 
       IF distgm2 GT 999.99 THEN
          ASSIGN distgm2 = 999.99.
    
    IF distgm3 LT -999.99 THEN
       ASSIGN distgm3 = -999.99.
    ELSE 
       IF distgm3 GT 999.99 THEN
          ASSIGN distgm3 = 999.99.
    
    IF distgm4 LT -999.99 THEN
       ASSIGN distgm4 = -999.99.
    ELSE 
       IF distgm4 GT 999.99 THEN
          ASSIGN distgm4 = 999.99.
    
    IF distgm5 LT -999.99 THEN
       ASSIGN distgm5 = -999.99.
    ELSE 
       IF distgm5 GT 999.99 THEN
          ASSIGN distgm5 = 999.99.
    
    IF distgm6 LT -999.99 THEN
       ASSIGN distgm6 = -999.99.
    ELSE 
       IF distgm6 GT 999.99 THEN
          ASSIGN distgm6 = 999.99.

    RETURN.
END PROCEDURE. /* fix-gm */

/* Branch */

PROCEDURE fix-branchgm.
    IF branchgm1 LT -999.99 THEN
       ASSIGN branchgm1 = -999.99.
    ELSE 
       IF branchgm1 GT 999.99 THEN
          ASSIGN branchgm1 = 999.99.
    
    IF branchgm2 LT -999.99 THEN
       ASSIGN branchgm2 = -999.99.
    ELSE 
       IF branchgm2 GT 999.99 THEN
          ASSIGN branchgm2 = 999.99.
    
    IF branchgm3 LT -999.99 THEN
       ASSIGN branchgm3 = -999.99.
    ELSE 
       IF branchgm3 GT 999.99 THEN
          ASSIGN branchgm3 = 999.99.
    
    IF branchgm4 LT -999.99 THEN
       ASSIGN branchgm4 = -999.99.
    ELSE 
       IF branchgm4 GT 999.99 THEN
          ASSIGN branchgm4 = 999.99.
    
    IF branchgm5 LT -999.99 THEN
       ASSIGN branchgm5 = -999.99.
    ELSE 
       IF branchgm5 GT 999.99 THEN
          ASSIGN branchgm5 = 999.99.
    
    IF branchgm6 LT -999.99 THEN
       ASSIGN branchgm6 = -999.99.
    ELSE 
       IF branchgm6 GT 999.99 THEN
          ASSIGN branchgm6 = 999.99.

    RETURN.
END PROCEDURE. /* fix-gm */


/* Branch */
Procedure  gey_determination.
  if round(gey,0) < 70 then
    gey = 0.
  if round(gey,0) = 70 then
    gey = 50.
  else
  if round(gey,0) = 71 then
    gey = 53.
  else 
  if round(gey,0) = 72 then
    gey = 56.
  else 
  if round(gey,0) = 73 then
    gey = 59.
  else 
  if round(gey,0) = 74 then
    gey = 62.
  else 
  if round(gey,0) = 75 then
    gey = 65.
  else 
  if round(gey,0) = 76 then
    gey = 68.
  else 
  if round(gey,0) = 77 then
    gey = 71.
  else 
  if round(gey,0) = 78 then
    gey = 74.
  else 
  if round(gey,0) = 79 then
    gey = 77.
  else 
    gey = round(gey,0).
   
end. /* gey_determination */


procedure write-sum-hdgs.
  assign zx-page = zx-page + 1.
  if page-number ge 1 then
    page.
  display with frame kheadings1 down width 178.

  display
       zrpt-dt
       zrpt-dow
       zrpt-time
       zrpt-cono
       zrpt-user
       zx-page
       with frame kheadings1a no-underline no-box no-labels down width 178.

  display with frame kheadings1b down width 178.
   

    disp
       x-region
    with frame f-regtitle down.
    down with frame f-regtitle.

    disp 
       x-mo-1
       x-mo-2
       x-mo-3
    with frame f-reghdr down.
    down with frame f-reghdr.

    return.
end procedure. /* write-sum-hdgs */

procedure write-det-hdgs.
  assign zx-page = zx-page + 1.
  if page-number ge 1 then
    page.
  display with frame kheadings1 down width 178.

  display
       zrpt-dt
       zrpt-dow
       zrpt-time
       zrpt-cono
       zrpt-user
       zx-page
       with frame kheadings1a no-underline no-box no-labels down width 178.

  display with frame kheadings1b down width 178.
    
    disp  
       x-region
       x-dist
       
    with frame f-disttitle down.
    down with frame f-disttitle.
     
    disp 
       x-mo-1
       x-mo-2
       x-mo-3
    with frame f-disthdr down.
    down with frame f-disthdr.
    
    return.
end procedure. /* write-det-hdgs */
    

