/* z-sapbozdef.i         
 
   Definitions for Custom SAPBO reocrds

*/

define temp-table zsapbo no-undo

  field selection_cust as de format "999999999999"
 index k-prime is unique primary
  selection_cust ascending.

define var zelection_good      as logical              no-undo.
define var zelection_matrix    as logical extent 4  
 init
 [false,
  false,
  false,
  false]                                                   no-undo.

define var zelection_type      as character format "x"     no-undo. 
define var zelection_slsrep  as character format "x(4)"    no-undo.
define var zelection_prodcat   as character format "x(4)"  no-undo.
define var zelection_char4     as character format "x(4)"  no-undo.
define var zelection_cust      like arsc.custno            no-undo.