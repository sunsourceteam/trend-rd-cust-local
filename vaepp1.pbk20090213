/* vaepp1.p */
/*h****************************************************************************
  PROCEDURE      : vaepp1.p
  DESCRIPTION    : Value Add Pick Ticket Format 1 - NEMA
  AUTHOR         : jms
  DATE WRITTEN   : 06/07/96
  CHANGES MADE   :
    06/07/96 jms; TB# 21254 Develop Value Add Module-pick ticket print format 1
    08/20/96 gp;  TB# 19484 (B2a) Warehouse Logistics.  Add wlwhse.gva.
    09/19/96 jms; TB# 21254 Develop Value Add Module - corrected comments
    11/12/96 jms; TB# 21702 Removed references to oper2
    08/24/98 bm;  TB# 24812 Add Rush Functionality type "IN" Lines.
        Removed v-norushln and Replaced with new field vaes.norushln.
        Print rush orders first when using a range.
        Use sapbv.apinvno positions 1 - 7 for vano instead sapbv.vendno
        Use sapbv.apinvno positions 9 - 10 for vasuf instead sapbv.apinvno
    12/23/98 cm;  TB# 14835 Allow notes specific by suffix
    03/29/00 rgm; TB# 3-2   Add RxServer Interface to Standard
    07/10/00 hpl; TB# e5061 Add $comva parameter to d-oeepp1.i to comment out
        code for inventory allocation that is not used by value add module.
    09/21/00 mms; TB# e6508 WT BOD Project - Added &orderalttype param to call
        to d-oeepp1.i.
    11/29/00 lbr; TB# e6764 Added alternate source indication to oeepp
    01/22/01 mms; TB# e7644 WT BOD Project - added scope-def for wm and va
        comment parameters.  Sent in a value for twlstgedit as it is a required
        parameter; added o-whse variable definition.
    05/25/01 rcl; TB# e6873 Increase weight and cubes to 5 decimals.
    01/07/02 bpa; TB# t14097 Change to oeepp required addition of variable         definition here
    01/15/02 lbr; TB# e3207 Had to add param to oeepp1n.p
    05/23/02 bp ; TB# e13692 Add user hook.
    10/10/02 jbt; TB# e11863 Call generic notes printing program.
    12/12/02 gkd; TB# L963   Send all lines to TWL when printing changes only
    04/22/03 blw; TB# L1211 - Added si mod wl009 & wl010 to standard
    11/12/03 blw; TB# L759  - Integrate VA with TWL; Create WLIT records.
    11/14/03 blw; TB# L759  - Integrate VA with TWL; Print Carton Records.
    02/04/04 blw; TB# L759  - Rename vaes.xxc6 field to vaes.wletsetno.
    03/31/04 bm;  TB# e13915 Picktickets don't print foreign language
    11/10/04 blw; TB# L1576 - Do not send "IN" sections to TWL when "IT" final.
    01/28/05 rgm; TB# e21353 Subtotal lines not displaying on PickTicket
******************************************************************************/

{vaepp1.z99 &user_top = "*"}

def var     s-wminfo        as c format "x(31)"                 no-undo.
def var     v-vapckheadfl   as l                                no-undo.
def var     v-desc          like sasta.descrip   init ""        no-undo.
def var     v-kitconv       as de                               no-undo.
def var     o-reprintfl     as l            init no             no-undo.
def var     o-whse          like icsw.whse                      no-undo.
def var     v-loopcnt       as i            init 1              no-undo.
def var     v-printcnt      as i            init 1              no-undo.
def var     v-shiptonmfile  like oeeh.shiptonm                  no-undo.
def var     v-vendnowhse    as c format "x(12)"                 no-undo.
def var     v-custpo        like oeeh.custpo                    no-undo.
def var     v-termstype     like oeeh.termstype                 no-undo.
def var     v-totqtyshp     like oeeh.totqtyshp                 no-undo.
def var     v-totqtyord     like oeeh.totqtyord                 no-undo.
def var     v-codfl         like oeeh.codfl                     no-undo.
def var     v-lumpbillfl    like oeeh.lumpbillfl                no-undo.
def var     v-name          like arsc.name                      no-undo.
def var     v-addr          like arsc.addr                      no-undo.
def var     v-city          like arsc.city                      no-undo.
def var     v-state         like arsc.state                     no-undo.
def var     v-zipcd         like arsc.zipcd                     no-undo.
def var     v-nolineitem    like oeeh.nolineitem                no-undo.
def var     v-nodolines     like oeeh.nodolines                 no-undo.
def var     v-totcubes      like oeeh.totcubes                  no-undo.
def var     v-totweight     like oeeh.totweight                 no-undo.
def var     v-noassn        like oeel.nosnlots                  no-undo.
def var     v-vafstspecfl   as l                                no-undo.
def var     s-vadata        as c format "x(30)"                 no-undo.
def var     s-specdata      like vaes.specdata                  no-undo.
def var     v-wlupdtype     as c                                no-undo.
/*tb e6764 11/28/00 lbr; Added p-printdropshipfl for d-oeepp1.i not needed
    here but have to define for the include */
def var     p-printdropshipfl as l      init no                 no-undo.
/*tb t14097 01/07/02 bpa; Added p-useaddr for oeepp1h.las not needed
    here but have to define for the include */
def var     p-useaddr       as c format "x(1)"                  no-undo.

def new shared var v-subnetamt     like oeeh.totinvamt                 no-undo.
def new shared var v-subnetdisp    as c format "x(13)"                 no-undo.

/*e From {vaepp.lva} */
def     shared var p-packty          as c format "x"            no-undo.
def     shared var p-prtspecinstrfl  as l                       no-undo.
def     shared var v-orderdisp       like oeeh.orderdisp        no-undo.
def buffer b-vaes for vaes.
def buffer b-icsd for icsd.
def buffer b2-vaes for vaes.

/*tb L1211 04/22/03 blw; Define variables and frames for cartons*/
{wldata.lva}
{wldata.lfo}

{vaepp1.z99 &user_bef_defvar = "*"}

/*e Variables defined in form f-oeepp1.i */
{oeepp1.lva}
{vaepp1.lva &new = "new"}

{q-oeepp1.i &line = "vaesl"}

{wlwhse.gva "shared"}
{wlptemp.gva}

/*d Set the WM module purchased flag */
{w-sasa.i no-lock}
v-modwmfl = if avail sasa and sasa.modwmfl and
               avail sasc and sasc.wmintfl then yes
            else no.

/*d Checks to make sure the WL module has been purchased */
{wlsasa.gas}.

/*e Form for specifications/Instructions */
{vaepp1sp.lfo}

/*d Determine whether or not headings should print when fax is printername */
{oeepp1p.lpr &pckheadfl = "vapckheadfl"}

/* BEGIN REPORT LOGIC */
/*d Print the bulk pick ticket ahead of the individual pick tickets */

{b-oeepp1.i
    &line     = "vaesl"
    &flag     = v-vapckheadfl
    &sapbfile = "sapbv"
    &comva      = "/*"}

{vaepp1.z99 &user_bef_main = "*"}

/*tb L1211 11/14/03 blw; Connect to TWL, create cartons, then disconnect */
/* Assign the cartonfl from the sapb */

assign
    p-cartonfl    = if g-ourproc = "wltrans" then yes
                    else if sapb.optvalue[11] = "yes" then yes
                    else no.

do for b2-icsd:
    {w-icsd.i p-whse no-lock b2-}

    /* Set the whse WL flag */
    {wlicsd.gas &live = "yes"
                &b2   = "b2-"}.
end.

{wldataq.lpr &ord_type = "f"}

/*d Read each SAPBV record for each pick ticket to print sorting rush orders
  to the top */

main:

for each sapbv use-index k-sapbv where
         sapbv.cono     = g-cono        and
         sapbv.reportnm = sapb.reportnm
no-lock

by sapbv.cono
by sapbv.reportnm
by sapbv.vendno
by sapbv.type

/*d Sort pick tickets in route, entry, or order # order */
/*tb 24812 08/24/98 bm; Use sapbv.apinvno for vano and vasuf,
    Instead of vendno for vano and apinvno for vasuf.
    Added "by sapbv.vendno" above (always 0) and
    "by type" (Rush = no, else yes). This will print rush orders first */
/*e In the SAPBV record some field names do not reflect the contents of that
    field:     sapbv.selecttype  is the same as   sapbo.route
               sapbv.seqno2      is a counter of the order list as entered
               sapbv.apinvno,1,7 is the VA order number
               sapbv.apinvno,9,2 is the VA order suffix number
               sapbv.seqno       is the VA section seqno number       */

by if p-sortty = "r" then sapbv.selecttype
   else string(sapbv.cono)
by if p-sortty = "e" then sapbv.seqno2
   else sapbv.vendno
by sapbv.apinvno:
    /*tb 24812 08/24/98 bm; Use sapbv.apinvno for vano and vasuf,
        Instead of vendno for vano and apinvno for vasuf */
    {vaeh.gfi &vano  = integer(substring(sapbv.apinvno,1,7))
              &vasuf = integer(substring(sapbv.apinvno,9,2))
              &lock  = no}
    {vaes.gfi &vano  = integer(substring(sapbv.apinvno,1,7))
              &vasuf = integer(substring(sapbv.apinvno,9,2))
              &seqno = sapbv.seqno
              &lock  = no}
    if not avail vaeh or not avail vaes then next main.
    if vaes.sctntype <> "IN" then next main.

    assign
        v-nolineitem = 0
        v-nodolines  = 0
        v-totqtyshp  = 0
        v-totqtyord  = 0
        v-totcubes   = 0
        v-totweight  = 0.

    for each vaesl use-index k-vaesl where
        vaesl.cono  = g-cono     and
        vaesl.vano  = vaes.vano  and
        vaesl.vasuf = vaes.vasuf and
        vaesl.seqno = vaes.seqno
    no-lock:
        assign
            v-nolineitem = v-nolineitem + 1
            v-nodolines  = if vaesl.directfl = yes then
                               v-nodolines + 1
                           else v-nodolines
            v-totqtyshp  = v-totqtyshp + vaesl.stkqtyship
            v-totqtyord  = v-totqtyord + vaesl.stkqtyord
            v-totcubes   = v-totcubes  + vaesl.extcubes
            v-totweight  = v-totweight + vaesl.extweight.
    end. /* for each vaesl*/

    /*d If all lines are "do"s, skip this order */
    if v-nodolines = v-nolineitem then next main.
    
/* PickPack */  
    assign icsdAutoPck = "N"
           icsdAutoXdock = "N".
    
    find bi-icsd where bi-icsd.cono = g-cono and
                       bi-icsd.whse = vaeh.whse no-lock no-error.
    if avail bi-icsd then do:
      if num-entries(bi-icsd.user4) > 2 then do:
        if ENTRY(3, bi-icsd.user4) = "n" or
           ENTRY(3, bi-icsd.user4) = " " then
          assign icsdAutoPck = "N".
        else
        if ENTRY(3, bi-icsd.user4) = "Y" then
          assign icsdAutoPck = "Y".
        else
          assign icsdAutoPck = "N".
      end.

      if num-entries(bi-icsd.user4) > 3 then do:
        if ENTRY(4, bi-icsd.user4) = "n" or
           ENTRY(4, bi-icsd.user4) = " " then
          assign icsdAutoXdock = "N".
        else
        if ENTRY(4, bi-icsd.user4) = "Y" then
          assign icsdAutoXdock = "Y".
        else
          assign icsdAutoXdock = "N".
      end.
      
    end.    

    if icsdAutoPck = "Y" then do:
      if (x-hseqordno           <>  vaeh.vano or
          x-hseqordsuf          <>  vaeh.vasuf or
          x-hsection            <>  vaes.seqno ) and
          can-find (first zsdipckdtl use-index k-ordix where
                          zsdipckdtl.cono      = g-cono   and
                          zsdipckdtl.ordertype = "f"          and
                          zsdipckdtl.orderno   = vaeh.vano and
                          zsdipckdtl.ordersuf  = vaeh.vasuf and
                          zsdipckdtl.lineno    = vaes.seqno
      no-lock)  then do:
        find first zsdipckdtl where
                   zsdipckdtl.cono      = g-cono       and
                   zsdipckdtl.ordertype = "f"          and
                   zsdipckdtl.orderno   = vaeh.vano    and
                   zsdipckdtl.ordersuf  = vaeh.vasuf   and
                   zsdipckdtl.lineno    = vaes.seqno
                   no-lock no-error.
        if avail  zsdipckdtl then do:
          find first b-zsdipcksq use-index wrcptix  where
                     b-zsdipcksq.cono        = g-cono   and
                     b-zsdipcksq.whse        = vaeh.whse    and
                     b-zsdipcksq.seqid       = zsdipckdtl.pickseq 
                     no-lock no-error.
      
          find first b-zsdipckid use-index wrcptix  where
                     b-zsdipckid.cono        = g-cono   and
                     b-zsdipckid.whse        = vaeh.whse    and
                     b-zsdipckid.pickid      = zsdipckdtl.pickid  
                     no-lock no-error.
          if avail b-zsdipckid and
             avail b-zsdipcksq then
            assign x-pckid                = b-zsdipckid.pickid
                   x-pckdetailfl          = false
                   x-pckskipidfl          = true.       
        end. 
      end.
      else
      if (x-hseqordno           <>  vaeh.vano or
          x-hseqordsuf          <>  vaeh.vasuf or
          x-hsection            <>  vaes.seqno ) and
        x-pckassignfl = true  then do:
        assign x-pckid                = x-pckassigned
               x-pckskipidfl          = false.
      end.  
          
      if x-pckassignfl = false and
        x-pckskipidfl = false then do:
        find last zsdipckid use-index wrcptix  where
                  zsdipckid.cono = g-cono and
                  zsdipckid.whse = vaeh.whse no-lock no-error.
        if not avail zsdipckid then do:
          create b-zsdipckid.
          assign b-zsdipckid.cono      = g-cono 
                 b-zsdipckid.whse      = vaeh.whse
                 b-zsdipckid.pickid    = 1
                 b-zsdipckid.procid    = g-currproc
                 x-pckid               = 1 
                 x-pckdetailfl         = true
                 x-pckassigned         = x-pckid
                 x-pckassignfl         = true.
                 
        end.
        else do:
          create b-zsdipckid.
          assign b-zsdipckid.cono      = g-cono 
                 b-zsdipckid.whse      = vaeh.whse
                 b-zsdipckid.pickid    = zsdipckid.pickid + 1
                 b-zsdipckid.procid    = g-currproc
                 x-pckid               = b-zsdipckid.pickid
                 x-pckdetailfl         = true
                 x-pckassigned         = x-pckid
                 x-pckassignfl         = true.
        end.
      end. /*   not  pckassgnicsdAutoPck  */
      else
      if x-pckid <> 0 then do:
        find last b-zsdipckid use-index wrcptix  where
                  b-zsdipckid.cono = g-cono and
                  b-zsdipckid.whse = vaeh.whse  and
                  b-zsdipckid.pickid = x-pckid no-lock no-error.
      /* Make sure we have the buffer */
      end.
  
      if (x-hseqordno           <>  vaeh.vano or
          x-hseqordsuf          <>  vaeh.vasuf or
          x-hsection            <>  vaes.seqno) and
          (/* kpet.notimesprt < 2 OR */
              /* not vaes.printpckfl or */
               
           not can-find (first zsdipckdtl where
                               zsdipckdtl.cono      = g-cono   and
                               zsdipckdtl.ordertype = "f"          and
                               zsdipckdtl.orderno   = vaeh.vano and
                               zsdipckdtl.ordersuf  = vaeh.vasuf and
                               zsdipckdtl.lineno    = vaes.seqno
      no-lock))  then do:
        find last zsdipcksq use-index wrcptix  where
                  zsdipcksq.cono = g-cono and
                  zsdipcksq.whse = vaeh.whse no-lock no-error.
        if not avail zsdipcksq then do:
          create b-zsdipcksq.
          assign b-zsdipcksq.cono      = g-cono 
                 b-zsdipcksq.whse      = vaeh.whse
                 b-zsdipcksq.seqid     = 1
                 b-zsdipcksq.procid    = g-currproc
                 x-hseqordno           = vaeh.vano
                 x-hseqordsuf          = vaeh.vasuf
                 x-hsection            = vaes.seqno
                 x-pckdetailfl         = true.
        end.
        else do:
          create b-zsdipcksq.
          assign b-zsdipcksq.cono      = g-cono 
                 b-zsdipcksq.whse      = vaeh.whse
                 b-zsdipcksq.seqid     = zsdipcksq.seqid + 1
                 b-zsdipcksq.procid    = g-currproc
                 x-hseqordno           = vaeh.vano
                 x-hseqordsuf          = vaeh.vasuf
                 x-hsection            = vaes.seqno
                 x-pckdetailfl         = true.
        end.
      end. /*    not can-find */
      else
      if (x-hseqordno           <>  vaeh.vano or
          x-hseqordsuf          <>  vaeh.vasuf or
          x-hsection            <>  vaes.seqno)
          /* and kpet.notimesprt ge 2 */
          /* and  vaes.printpckfl */
         then do:
        find first zsdipckdtl where
                   zsdipckdtl.cono = g-cono and
                   zsdipckdtl.ordertype = "f" and
                   zsdipckdtl.orderno = vaeh.vano and
                   zsdipckdtl.ordersuf = vaeh.vasuf and
                   zsdipckdtl.lineno   = vaes.seqno no-lock no-error.
        if avail  zsdipckdtl then do:
          find  first b-zsdipcksq use-index wrcptix  where
                      b-zsdipcksq.cono = g-cono and
                      b-zsdipcksq.whse = vaeh.whse and
                      b-zsdipcksq.seqid = zsdipckdtl.pickseq no-lock no-error.
          if not avail b-zsdipcksq then do:
            find last zsdipcksq use-index wrcptix  where
                      zsdipcksq.cono = g-cono and
                      zsdipcksq.whse = vaeh.whse no-lock no-error.
            if not avail zsdipcksq then do:
              create b-zsdipcksq.
              assign b-zsdipcksq.cono      = g-cono 
                     b-zsdipcksq.whse      = vaeh.whse
                     b-zsdipcksq.seqid     = 1
                     b-zsdipcksq.procid    = g-currproc
                     x-hseqordno           = vaeh.vano
                     x-hseqordsuf          = vaeh.vasuf
                     x-hsection            = vaes.seqno
                     x-pckdetailfl         = true.
            end.
            else do:
              create b-zsdipcksq.
              assign b-zsdipcksq.cono      = g-cono 
                     b-zsdipcksq.whse      = vaeh.whse
                     b-zsdipcksq.seqid     = zsdipcksq.seqid + 1
                     b-zsdipcksq.procid    = g-currproc
                     x-hseqordno           = vaeh.vano
                     x-hseqordsuf          = vaeh.vasuf
                     x-hsection            = vaes.seqno
                     x-pckdetailfl         = true.
            end.
          end.
        end.
      end. /*    icsdAutoPck = "Y" */
    end. /* if avail oeel   */
  
    if avail vaeh then
      assign       x-hseqordno           = vaeh.vano
                   x-hseqordsuf          = vaeh.vasuf
                   x-hsection            = vaes.seqno.

/* PickPack */  


    /*d Assign no times to print variables; only print second pack list
        when doing a ship complete print that has the option set to print a
        second packing document when complete, in this case the dual print
        should only occur when the order header indicates that it is necessary
        to print the order, otherwise only a single print document needs to
        be printed; the order must further have the total quantity ready to
        ship equal to the total quantity ordered */
     /*tb 21254 06/12/96 jms; Develop Value Add Module - This section does
        nothing at this time, it is in place for future implementation.*/
    assign
        v-orderdisp = ""
        v-printcnt  = if v-orderdisp = "s" and
                         v-totqtyshp = v-totqtyord and
                         (p-packty = "b"  or
                            (p-packty = "f" and vaes.pickcnt <> 1))
                         then 2
                      else 1
        o-reprintfl = p-reprintfl.

    /*d Warehouse Logistics creation of WLET, WLEH, WLEL, etc.. for TWL */
    if v-modwlfl       = yes                                and
       vaes.stagecd    < 5                                  and
       vaes.wletsetno  = " "                                and
       g-ourproc       <> "wltrans":u                       and
       can-find( first vaesl use-index k-vaesl where
                       vaesl.cono      = vaes.cono          and
                       vaesl.vano      = vaes.vano          and
                       vaesl.vasuf     = vaes.vasuf         and
                       vaesl.seqno     = vaes.seqno)        and
       not can-find (first b2-vaes use-index k-vaes where
                           b2-vaes.cono     = vaes.cono     and
                           b2-vaes.vano     = vaes.vano     and
                           b2-vaes.vasuf    = vaes.vasuf    and
                           b2-vaes.seqno    > vaes.seqno    and
                           b2-vaes.sctntype = "IT":U)
    then do:

        /*d Create a PCK WLET record for the printing run */
        {wlpcrt.gpr &whse      = vaeh.whse
                    &b         = "b-"
                    &priority  = 5
                    &transtype = ""S""
                    &process   = ""PCK"" }

        /*d Create a WLEH record for each order */
        {wlpproc.gpr &invoutcond    = yes
                     &wlsetno       = v-wlsetno
                     &whse          = vaeh.whse
                     &ordertype     = ""f""
                     &orderno       = vaes.vano
                     &ordersuf      = vaes.vasuf
                     &lineno        = 0
                     &seqno         = vaes.seqno
                     &msdssheetno   = """"
                     &updtype       = ""A""
                     &shipmentid    = """"}

        if v-wlwhsefl = yes then
            v-printcnt = if vaes.orderdisp = "s" then 1
                         else v-printcnt.

    end. /* end if v-modwlfl */

    /*d Control for print of more than one copy when printing ship complete */
    do v-loopcnt = 1 to v-printcnt:

        assign
            p-reprintfl    = if v-loopcnt = 2 then no else p-reprintfl
            v-pageno       = page-number - 1
            v-reprintfl    = sapbv.allfl
            v-shiptonmfile = ""
            v-vendnowhse   = if vaes.destvendno ne 0 then
                                 string(vaes.destvendno, "zzzzzzzzzzz9")
                             else vaes.destwhse.
            v-custpo       = "".

        /*d Define the form for the pick ticket header */
        {f-oeepp9.i                                          
            &zcom2        = "/*" 
            &head         = "vaeh"
            &section      = "vaes"
            &pref         = "va"
            &orderdt      = "vaeh.enterdt"
            &pickdt       = "vaes.pickeddt"
            &shiptonm     = "v-shiptonmfile"
            &custno       = "v-vendnowhse"
            &shipto       = " ' ' "  
            &custpo       = "v-custpo"}
        /*o Assign the labels for the literal headings */
        {a-oeepp1.i s- v-vapckheadfl}

        s-lit3a = if vaes.destvendno ne 0 then "Vend #:"
                  else "Whse".

        /*d Assign the pick ticket header display variables */
        assign
            v-termstype    = ""
            v-prtpricefl   = no

            v-codfl        = no
            v-lumpbillfl   = no
            v-name         = "N/A"
            v-addr         = ""
            v-city         = ""
            v-state        = ""
            v-zipcd        = ""
            v-vaehid       = recid(vaeh)
            v-vaesid       = recid(vaes)
            s-seqno        = string(vaes.seqno,"zz9").
/* SX 55 */

 /*d Assign and display variables for Work Order and Kit Product */
 /* ta - barcode on work order begin */
        if not v-tovid then do:
          assign bc-orderno2  = w-asterisk +
                 "F"  +
                  string(vaeh.vano,"9999999") +
                  "."  +
                  string(vaeh.vasuf,"99") +
                  w-asterisk
                  bc-orderno2                 = caps(bc-orderno2)
                  bc-ordernox                 = bc-orderno2. 
        end.
        else 
          assign bc-orderno2 = ""
                 bc-ordernox = "".  

    if not v-tovid and tobar and avail b-zsdipcksq then 
        assign v-tof = 
                /*                v-esc + "&a0h15v" + */
                v-esc + "&a274V" + v-esc + "&a0h" + 
                v-esc + 
                "(10U" + 
                v-esc +
                "(s0p6.00h10.0v0s3b4168T" +
                "Seq# " +
                substring(string(b-zsdipcksq.seqid,"999999999"),6,4) +
                "                         VA Order  " +
                v-esc + "&l6D" +
                v-esc + "(8U" +
                v-esc + "(s0p16.67h0s0b4102T" +
                v-esc + "&a7375V" + v-esc + "&a31C" + 
                "BatchId: " +
                string(b-zsdipckid.pickid) + "  " +
                "~033(8U~033(s1p20v0s0b24576T" +
                "*" + string(b-zsdipckid.pickid,"999999999") + 
                "*" +
                v-esc + "&a336V" + v-esc + "&a0h" + 
                v-esc + "&l6D" +
                v-esc + "(8U" +
                v-esc + "(s0p16.67h0s0b4102T".
    else if v-tovid  then 
      assign v-tof = " ".
    else if tobar and icsdAutoPck = "y" then
      assign v-tof = "~033(10U~033(s0p17h0s0b4102T~033&a560V~033&a0h". 
    else
      assign v-tof = " ".

/* SX 55 */
        /*tb 24812 08/24/98 bm; Replaced v-norushln with vaes.norushln */
        {oeepp1h.las
           &termstype  = "v-termstype"
           &head       = "vaeh"
           &section    = "vaes"
           &orderdisp  = "v-orderdisp"
           &totqtyshp  = "v-totqtyshp"
           &totqtyord  = "v-totqtyord"
           &norushln   = "vaes.norushln"
           &prtpricefl = "v-prtpricefl"
           &codfl      = "v-codfl"
           &lumpbillfl = "v-lumpbillfl"
           &arsc       = "v-"
           &shiptonm   = "vaes.destname"
           &shiptofile = "vaes.dest"
           &shiptost   = "vaes.deststate"
           &shiptozip  = "vaes.destzipcd"
           &comva      = "/*"}
/* SX 55 */
        if icsdAutoPck = "y" then do:                                         
           hide frame f-head.                                                  
           if not tobar then                                                   
             view frame f-headva. /* Added a custom VA version in F-OEEPP9.I */
           else                                                                
             view frame f-headvabc.                                            
         end.                                                                  
/* SX 55 */
      
        /*tb L1211 11/14/03 blw; Print the TWL cartons on the order*/
        {wldatap.lpr
            &orderno  = "vaes.vano"
            &ordersuf = "vaes.vasuf"
            &vaordseq = "vaes.seqno"}

        /*d Print the order notes  */
        /*tb 14835 12/23/98 cm; Allow notes specific by suffix */
        /*tb# 3207 Added param to oeepp1n.p */
        if vaeh.notesfl <> "" then
            run notesprt.p(g-cono,"fh",string(vaeh.vano),string(vaeh.vasuf),
                             "vaepp",8,1).

        if vaes.notesfl <> "" then
            run notesprt.p(g-cono,"fs",
                             string(vaes.vano) + "-" + string(vaes.vasuf),
                             string(vaes.seqno),"vaepp",8,1).

        /*d Print the Specification/Instructions  */
        if p-prtspecinstrfl = yes then do:
            v-vafstspecfl = yes.

            for each b-vaes use-index k-section where
                b-vaes.cono           = g-cono          and
                b-vaes.completefl     = no              and
                b-vaes.sctntype       = "SP"            and
                b-vaes.vano           = vaeh.vano       and
                b-vaes.vasuf          = vaeh.vasuf      and
                b-vaes.specdata       ne ""             and
                ((vaes.destvendno ne 0 and can-do("B,E", b-vaes.specprtty)) or
                (vaes.destwhse  ne "" and can-do("B,I", b-vaes.specprtty)))
            no-lock:

                {vaepp1sp.lpr &buffer = "b-"}
            end. /* for each b-vaes */

            If vaes.specprtfl = yes and vaes.specdata ne "" then do:
                {vaepp1sp.lpr}
            end.
        end. /* if p-prtspecinstrfl */

        /*o If WM has been purchased and lines exist on the order, print the
            pick ticket through WM logic */
        /*d If WL whse WM cannot be used */
        if v-modwmfl and v-wlwhsefl = no and v-nolineitem <> 0 then

            /*tb 3-2 03/29/00 rgm; Rxserver interface call when appropriate */
            /*e coded this way so we can still grep on vaepp1wm */
            &IF DEFINED(rxserv_format) = 0 &THEN
                if v-oepickordty = "r" then run vaepp1wm.p.
                else run vaepp1ws.p.
            &ELSE
                if v-oepickordty = "r" then run vaepp{&rxserv_format}wm.p.
                else run vaepp{&rxserv_format}ws.p.
            &ENDIF

        /*d If no line items exist on the pick ticket, print a message on the
            pick ticket */
        else if v-nolineitem = 0 then
        do:

            display "** No Line Items to Print **" with frame f-blank.
            down with frame f-blank.

        end.

        /*o Find each line item to print in VAESL; skip DO lines; skip JIT line
            not ready to print; Sort the lines in bin location, product, or line
            order as requested */
        else for each vaesl use-index k-vaesl where
                      vaesl.cono            = g-cono        and
                      vaesl.vano            = vaeh.vano     and
                      vaesl.vasuf           = vaeh.vasuf    and
                      vaesl.seqno           = vaes.seqno    and
                      vaesl.directfl        = no            and
                      vaesl.statustype      = yes
        no-lock
        /* Include code for JIT using a new field called vaesl.jitpickfl
            (oeeh.orderdisp ne "j" or vaesl.jitpickfl = yes)  */

        by  if v-oepickordty = "r" then vaesl.binloc
            else if v-oepickordty = "p" then vaesl.shipprod
            else string(vaesl.lineno,"999"):

            /*d Display the line on the print */
            assign
                v-nosnlots = 0
                v-noassn   = 0
                v-comtype  = "fl" + string(vaes.seqno)
                v-qtybo    = vaesl.qtyord - vaesl.qtyship.

            do for icsw, icsp:
                if vaesl.nonstockty ne "n" then do:
                    {w-icsw.i vaesl.shipprod vaesl.whse no-lock}
                    {w-icsp.i vaesl.shipprod no-lock}

                    if avail icsw and icsw.serlottype = "s" then do:
                        {p-nosn.i &ordertype = ""f""
                                  &orderno   = "vaesl.vano"
                                  &ordersuf  = "vaesl.vasuf"
                                  &seqno     = "vaes.seqno"
                                  &comlineno = "/*"
                                  &lineno    = "icets.lineno = vaesl.lineno"}
                    end.
                    else if avail icsw and icsw.serlottype = "l" then do:
                        {p-nolots.i &ordertype = ""f""
                                    &orderno   = "vaesl.vano"
                                    &ordersuf  = "vaesl.vasuf"
                                    &seqno     = "vaes.seqno"
                                    &comlineno = "/*"
                                    &lineno    = "icetl.lineno = vaesl.lineno"}
                    end.
                    v-nosnlots  = round(vaesl.stkqtyship - v-noassn,2).
                end. /* if vaesl.nonstockty ... */

                /* Define preprocessors wm and va for use in kitcompprt.
                   - comwmout will take out wm processing which was &comva
                   - comoeout will take out oe specific processing which was
                   &comr.  The &comr parameter is still passed as it is used
                   in custom .z99 specific calls.
                   Note that even though &twlstgedit is not needed it is
                   a required  parameter becaused it is passed to a second
                   layer.  Therefore something must be passed along thus the                   condition to do no harm of " and true ". */

                &scoped-define comwmout out
                &scoped-define comoeout out

                {d-oeepp1.i
                    &comr         = "/*"
                    &coms         = "/*"
                    &combo        = "/*"
                    &comxref      = "/*"
                    &commsds      = "/*"
                    &returnfl     = "v-returnfl"
                    &cubes        = "vaesl.extcubes"
                    &weight       = "vaesl.extweight"
                    &head         = "vaeh"
                    &line         = "vaesl"
                    &section      = "vaes"
                    &ourproc      = "vaepp"
                    &specnstype   = "vaesl.nonstockty"
                    &altwhse      = "v-altwhse"
                    &vendno       = "v-vendno"
                    &kitfl        = "v-kitfl"
                    &botype       = "v-botype"
                    &orderalttype = "vaesl.orderalttype"
                    &orderdisp    = "v-orderdisp"
                    &prevqtyship  = "vaesl.prevqtyship"
                    &netamt       = "v-netamt"
                    &prtpricefl   = "v-prtpricefl"
                    &bono         = "vaeh.bono"
                    &orderno      = "vaesl.vano"
                    &ordersuf     = "vaesl.vasuf"
                    &corechgty    = "v-corechgty"
                    &qtybo        = "v-qtybo"
                    &ordertype    = "f"
                    &prefix       = "va"
                    &seqno        = "vaesl.seqno"
                    &stkqtyshp    = "vaesl.stkqtyship"
                    &nosnlots     = "v-nosnlots"
                    &comtype      = "v-comtype"
                    &comlineno    = "vaesl.lineno"
                    &shipto       = "vaes.destwhse"
                    &custno       = "v-custno"
                    &twlstgedit   = " and true "
                    &whse         = vaeh.whse
                    &altwhse    = "" 
                    &comva      = "/*"}

            end. /* end of do for icsw, icsp */

        end. /* end of for each vaesl */

        /*d Load order totals for display */
        assign
            s-linecntx   = string(v-linecnt,"zz9")
            v-noprintx   = string(v-noprint,"zz9")
            s-totqtyshp  = string(a-totqtyshp *
                               (if can-do("rm",vaeh.transtype) then -1
                                else 1),"zzzzzzzz9.99-")
            s-totlineamt = ""
            s-totcubes   = string(if v-orderdisp = "j" then a-totcubes
                           else v-totcubes,"zzzzzzzz9.99999-")
            s-totweight  = string(if v-orderdisp = "j" then a-totweight
                           else v-totweight,"zzzzzzzz9.99999-")
            s-lit42a     = "Last Page".

        /*o Force a page break between pick tickets */
        /*e Page after the last pick ticket to force the display of page
            bottom fields */
        page.

    end. /* end of do v-loopcnt */

    /*d Restore original option setting */
    p-reprintfl = o-reprintfl.

end. /* end of for each sapbv */

/* Set the WLET records to active */
{wlpactv.gpr &priority = 5}

hide all no-pause.
hide message no-pause.

{vaepp1.z99 &user_procedure_define = "*"} 
