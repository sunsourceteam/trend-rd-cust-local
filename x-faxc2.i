/*h*****************************************************************************
  INCLUDE      : p-faxc2.i
  DESCRIPTION  : Cover Page for Fax
  USED ONCE?   : no (bpepbf1.p, oeepaf1.p, oeepif1.p, p-ares1.i)
  AUTHOR       : enp
  DATE WRITTEN : 11/27/92
  CHANGES MADE :
    11/27/92 enp; TB# 8889 Add VisiFax Capability
    07/19/93 pjt; TB# 9599 Getting "sasp record not available" message
    08/20/93 enp; TB# 12648 VSIFAX from F10 prints do not compress
    09/07/93 rs;  TB# 12932 Trend Doc Mgr for Rel 5.2
    10/20/93 enp; TB# 13098 Allow an 'l' in 10 digit phone numbe
    11/09/94 rs;  TB# 17051 Transfer Info To Notification Msg
    09/15/95 dww; TB# 19314 Remove Imbedded spaces from the Fax Phone Number
    04/07/98 cm;  TB# 24793 Wrong page size used for fax output
    09/08/98 jl;  TB# 25144 Add the interface to Vsifax Gold. Added vfx
    04/13/99 cm;  TB# 26054 added &comui functionality
    05/15/99 jl;  TB# 26204 Added nxtfax script logic
    10/08/99 kjb; TB# 26690 Added RxLaser and Optio 3.0 logic to standard
    10/25/99 cm;  TB# 26054 Removed OPTIO interface
    11/08/99 lbr; TB# 26690 Fixed problem with optio/fax losing header
    01/06/00 rgm; TB# 27121 Optio Printer language information
    03/29/00 rgm; TB# 3-2  Add RxServer Interface to Standard
    06/09/00 rgm; TB# A9   Fix for RxServer Faxing issue
    07/18/00 sbr; TB# A18  Rxserver Faxing No notes on cover sheet
    09/25/00 bpa; TB# A50  VsiFax not including comments (memo) on cover sheet
    10/23/00 lbr; TB# e6867 From field not using screen value
    03/27/01 mjg; TB# e7108 Extra page when faxing without a coversheet.
    04/24/01 kjb; TB# e8618 When faxing through RxServer, the footer frame
        was appearing in the body of the document
    06/20/01 lbr; TB# e9095 Added PC User Name and PC Password for
        conformation of rxlaser faxing
    12/07/01 lbr; TB# e11405 Moved user name and password to the end and
        added email address
    05/22/02 rcl; TB# e13657 VSIfax extra blank page.
    05/30/02 rgm; TB# e12526 VSIFax e-mail confirmation correction for NxTFax
    08/14/02 lcb; TB# e12313 Report Manager overwriting output files
******************************************************************************/

/*e Parameters
   &extent = extent from sasc (1=PO, 2=OEEPA 3=OEEPI 4=ARES 5=BPEPB 6=BPEPV
   &file   = file name
*******************************************************************************/

/*d Faxbox */
if avail sasp then do:

        /*tb 26690 10/08/99 kjb; Add email notification address logic needed
            for faxing with Optio.  Define these variables only once - in case
            this include is included twice */
        &IF DEFINED(defined-sasoo)=0 &THEN
            &GLOBAL-DEFINE defined-sasoo TRUE
            def buffer z-sasoo     for sasoo.
            def var    z-emailaddr as char format "x(40)" no-undo.
            def var    z-emailfl   as log                 no-undo.
            def var    z-type      as char format "x(5)"  no-undo.
        &ENDIF

        assign
            z-emailfl   = no
            z-emailaddr = ""
            z-type      = if      {&extent} = 1 then "POEPP"
                          else if {&extent} = 2 then "OEEPA"
                          else if {&extent} = 3 then "OEEPI"
                          else if {&extent} = 4 then "ARES"
                          else if {&extent} = 5 then "BPEPB"
                          else                       "BPEPV".
        /* tb e9095 Added pcusername and pcpassword for email notification
            for rxserver */
       for first z-sasoo fields (cono oper2 tqemailaddr pcpassword pcusername) 
           where
            z-sasoo.cono         = g-cono     and
            z-sasoo.oper2        = g-operinit
        no-lock:
            assign
                z-pcusername = z-sasoo.pcusername
                z-pcpassword = z-sasoo.pcpassword.
        end.

        /* set the e-mail address from pv_user */
        z-pcemailaddr = "".
        for first pv_user fields(email) where
            pv_user.cono  = g-cono and
            pv_user.oper2 = g-operinit
        no-lock:
            assign
                z-emailfl     = if pv_user.email <> ""
                                 then yes else no
                z-emailaddr   = pv_user.email
                z-pcemailaddr = pv_user.email.
        end.

        /*tb 19314 09/15/95 dww; Remove Imbedded Space from Fax Phone Number */
        {fixphone.gca {&file}faxphoneno s-faxphoneno {&comvar}}

        /*tb 17051 11/09/94 rs; Transfer Info to Notification Msg */
        /*tb 26690 10/08/99 kjb; Moved assignment of v-faxtag2 outside of
            conditional loop so it can be used in more than one place. */
        /*tb A18 07/18/00 sbr; Added v-from to the assign statement */
        /*d tag1 & tag2 will be set for custom Transmit Notify Prog */
        assign
            v-faxto[1]   = {&file}name
            v-faxto[2]   = if sasc.oifaxattn[{&extent}] then
                               /{&com}*/
                               if "{&file}" = "v-" then v-apmgr
                               else
                               /{&com}*/
                               arsc.apmgr
                            else {&file}pocontctnm
            v-faxcmd     = (if sasc.oifaxpreno = "1" and
                               substring(s-faxphoneno,1,1) = "1" then ""
                           else sasc.oifaxpreno) +
                           s-faxphoneno + sasc.oifaxsufno
            v-faxtag2    = {&tag}
            v-pcommand   = sasp.pcommand
            v-from       = if v-from = "" then
                               "Accounting"
                           else
                               v-from.

        /*tb 17051 11/09/94 rs; Transfer Info to Notification Msg rearranged
            logic to void going oversize */
        find sassr where recid(sassr) = v-sassrid no-lock no-error.
        /*tb# 6867 10/23/00 lbr; Added v-faxfrom - v-from */
        run get-printdir.
            /*tb A49 09/27/00 bpa; Needed to move the creation of
                 memo file to procedure in p-fax1.i; broke 64K segment */
        run create-memofile ("yes").

        v-faxcmd = " -n " + v-faxcmd.

        if index(sasp.pcommand,"ncc":u) = 0 then
           v-faxcmd = v-faxcmd +
                         (" -t tco=" +
                          "~"" + v-faxto[1] + "~"" +
                          " -t tnm=" +
                          "~"" + v-faxto[2] + "~"" +
                          " -t fnm=" +
                          "~"" + v-from + "~"" +
                          " -t tg1=" +
                          "~"" + v-faxto[1] + "~"" +
                          " -t tg2=" +
                          "~"" + {&tag} + "~"" +
                         (if z-emailaddr="" then "" else
                            " -t mad=" + "~"" + z-emailaddr + "~"" ) +
                         (if v-memofl = no then "" else
                            " -t ntf=" + "~"" + z-memofilenm + "~"")).

        /*tb 24793 04/07/98 cm; Wrong page size used for fax output */
        if avail sassr then do:
           z-filenm = z-memofilenm + "b".
           output to value(z-filenm) paged
                page-size value(sassr.pglength).

        end. /* if avail sassr */

end. /* if avail sasp */

/*d Set Flag for printing or archiving */
{&out} = if sasc.oifaxhardfl[{&extent}] then "p"
         else if v-coldfl then "x"
         else {&out}.


