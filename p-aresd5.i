/* p-aresd5.i 1.7 7/13/93 */
/*h****************************************************************************
  INCLUDE      : p-aresd5.i
  DESCRIPTION  : Detail Records for Statement Print, format1
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    06/22/93 jrg; TB# xxxxx Added "u" for UC records.
    07/12/93 jrg; TB# 12196 Changed - to + on v-credits.
    06/21/94 fef; TB# 15724 Include misc credits and future dues (when option 
        8 = y) in shipto subtotal.
    06/20/97 jkp; TB# 23081 Remove v-totbal variable from assign.  It is no
        longer used.
    wpp 02/05/99; Modify the detailed display of a line.
    10/27/00 - ceg - 00-84 - Stuff to increase x-ponum size 
    
*******************************************************************************/

{1}

    if first-of(b-report.c13) and b-report.c13 ne "" then 
    do:
    
        assign 
            v-subtot    = 0.
            
        display b-report.c13 with frame f-subtop.
        
    end. /* if first-of(b-report.c13) and b-report.c13 ne "" */
    
/{1}* */

/*tb       06/22/93 jrg; Added "u" for UC records. */
if (not can-do("i,m,u",b-report.c1)) and p-actonlyfl
    and arsc.statementty ne "b" then next.

/*tb       06/22/93 jrg; Removed desc on MC, using its invno now. */
/*tb 12196 07/12/93 jrg; Changed - to + on v-credits. */
/*tb 15724 06/21/94 fef; Include misc credits and future dues (when option 8 =
     y) in v-subtot when printing in shipto order. */
/*tb 23081 06/20/97 jkp; Removed assign of v-totbal (was v-totbal = v-totbal +
     (v-charge + v-credit).  This variable is no longer used. */

/* wpp 2/5/99 Find 'Invoice' record for original amount. */
find first bx-aret use-index k-cod where
    bx-aret.cono    = b-report.cono and
    bx-aret.invno   = b-report.i7   and
    bx-aret.invsuf  = b-report.i3   and
    bx-aret.transcd = 0             and
    bx-aret.custno  = dec(b-report.c12)
no-lock no-error.

if avail bx-aret then
   assign x-povar = bx-aret.refer. 
else
   assign x-povar = "".
 
if not avail bx-aret then
find first bx-aret use-index k-cod where
    bx-aret.cono    = b-report.cono and
    bx-aret.invno   = b-report.i7   and
    bx-aret.invsuf  = b-report.i3   and
    bx-aret.transcd = 5             and
    bx-aret.custno  = dec(b-report.c12)
no-lock no-error.

/* wpp not sure if I need the UC types here yet

if not avail bx-aret and b-report.c2 = "UC" then
    find first bx-aret use-index k-duedt where
        bx-aret.cono    = b-report.cono     and
        bx-aret.custno  = dec(b-report.c12) and
        bx-aret.transcd = 3                 and
        bx-aret.statustype                  and
        bx-aret.invdt   = b-report.dt       and
        bx-aret.seqno   = b-report.de12d5-4
    no-lock no-error.
 
 THIS IS FOR COD STUFF
if x-prtcod and not avail bx-aret then
    find first bx-aret use-index k-cod where
        bx-aret.cono    = b-report.cono and
        bx-aret.invno   = b-report.i7   and
        bx-aret.invsuf  = b-report.i3   and
        bx-aret.transcd = 4             and
        bx-aret.custno  = dec(b-report.c12)
    no-lock no-error.
**********************************************************/

/* wpp 02/05/99 Find the Customer PO number. */
if avail bx-aret then
find first oeeh where
    oeeh.cono     = bx-aret.cono  and
    oeeh.orderno  = bx-aret.invno and
    oeeh.ordersuf = bx-aret.invsuf
no-lock no-error.

if avail oeeh then 
       v-exrate = oeeh.user6.
    else    
       v-exrate = 1.00.
/*
message oeeh.orderno oeeh.ordersuf " "
                     oeeh.user6 " " b-report.de9d2s " " b-report.c1.
pause.
*/

if x-nocred and avail bx-aret  then
  if bx-aret.amount le 0 then
    next.

assign
    v-charge    = if  b-report.c1 <> "U"  and  /* Unapplied Cash */
                      b-report.c1 <> "P"  and  /* Payment */
                     (v-bankno ne 10 and 
                      v-bankno ne 11) then 
                      b-report.de9d2s
                  else 
                  if  b-report.c1 <> "U"  and  /* Unapplied Cash */ 
                      b-report.c1 <> "P"  and  /* Payment */
                     (v-bankno = 10 or 
                      v-bankno = 11) then
                      b-report.de9d2s * (if avail oeeh then
                                           v-exrate 
                                         else 
                                           1)
                  else 0
/*  v-credit    = if b-report.c1 ne "i" then
                      b-report.de9d2s
                  else 0
wpp */

    v-credit    = if avail bx-aret and 
                    v-bankno ne 10 and 
                    v-bankno ne 11 then
                     bx-aret.amount
                  else
                  if avail bx-aret and (v-bankno = 10 or 
                                        v-bankno = 11) then
                     bx-aret.amount * (if avail oeeh then
                                         v-exrate 
                                       else 
                                         1)  
                  else 0 
/*  v-credit    = if b-report.c1 ne "i" and avail bx-aret then
                     if v-bankno ne 10 and 
                        v-bankno ne 11 then
                        bx-aret.amount 
                     else
                        if avail bx-aret and 
                          (v-bankno = 10 or 
                           v-bankno = 11) then
                           bx-aret.amount * (if avail oeeh then 
                                               v-exrate    
                                             else 
                                               1)
                        else 0
                   else 0 */
    x-partial   = v-credit - v-charge
    x-ponum     = if avail oeeh and b-report.c2 <> "SC" then oeeh.custpo 
                  else if x-povar <> "" then x-povar
                  else ""
    v-subtot    = v-subtot + if can-do("future,active,due", b-report.c6) then
                      /*(v-charge + v-credit)*/ v-charge
                  else 0
    v-invoice   = if b-report.c1 = "p" then
                      b-report.c20
                  else if b-report.c1 = "u" then
                      "UNAPPLIED"
                  else string(b-report.i7,">>>>>>>9") + "-" +
                       string(b-report.i3,"99").
/*
if avail bx-aret and can-do("3,5",string(bx-aret.transcd)) then do:
message v-charge x-partial v-credit.
pause.
end.
*/ 
 
/* wpp 2/5/99 Change the display of credits. */
if avail bx-aret and can-do("3,5",string(bx-aret.transcd)) then assign
/*    v-charge  = x-partial */
    v-credit = v-charge
    x-partial = 0.

/*
message "v-credit = " v-credit.
pause.
*/

/* wpp 2/5/99 Modified detail form. */
if v-charge ne 0 or v-credit ne 0 then
    display
        b-report.dt
    /*  b-report.dt-2
        b-report.c6                     wpp */
        b-report.c2
        v-invoice
        x-ponum
        v-credit     when v-credit  ne 0
        x-partial    when x-partial ne 0
        v-charge     when v-charge  ne 0
    with frame f-detail.
    
if p-referfl and b-report.c24 ne "" then
    display 
        b-report.c24 
    with frame f-refer.
    down with frame f-detail.
    
{1}

    if last-of(b-report.c13) and b-report.c13 ne "" then 
      do: 
        display 
            b-report.c13 
            v-subtot 
        with frame f-subbot.
        
      end. /* if last-of(b-report.c13) and b-report.c13 ne "" */
    
/{1}* */
