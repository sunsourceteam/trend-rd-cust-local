/* SX 55 SDIVERSION */
/* inexpstrm.lcn */
/*h****************************************************************************
  PROCEDURE      : inexpstrm.lcn
  DESCRIPTION    : Export stream statement for DW
  AUTHOR         : mm
  DATE WRITTEN   : 02/20/04
  CHANGES MADE   :
     02/20/04 mm;  TB# e19527 Complete rewrite of DW logic
******************************************************************************/
/* Common export command for DW extracts
    &file   = file name to process
    &fields = field list if applicable, or file name
*/

v-reccount = 0.

if ("{&file}" = "w-customer" or
    "{&file}" = "w-plan"  or
    "{&file}" = "w-vendor" or
    "{&file}" = "w-salesrep" or
    "{&file}" = "w-prodwhse")  and v-dwlevel = 1
then
  p-delimiter = "x".
else
  p-delimiter = "t".

for each {&file}:
     case p-delimiter:
        when "x" then do:
            /* Old file export */
            put stream strm-export unformatted {&fields}.
        end.
        when "t" then do:
            /* tab */
            export stream strm-export delimiter "\t" {&fields}.
        end.
        otherwise do:
            /* csv */
            export stream strm-export delimiter "," {&fields}.
        end.
    end case.
    
    v-reccount = v-reccount + 1.
end.