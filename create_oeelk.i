if not avail oeehb then
  find oeehb where oeehb.cono = g-cono and
                   oeehb.batchnm = string(w-batchnm,">>>>>>>9") and 
                   oeehb.sourcepros = "Quote"
                   no-error.
if avail oeehb and oeehb.whse <> v-whse then
  find icsw where icsw.cono = g-cono and
                  icsw.prod = v-prod and
                  icsw.whse = oeehb.whse
                  no-lock no-error.
else
  find icsw where icsw.cono = g-cono and
                  icsw.prod = v-prod and
                  icsw.whse = v-whse
                  no-lock no-error.

find icsp where icsp.cono = g-cono and
                icsp.prod = v-prod
                no-lock no-error.
  
assign oeelk.cono       = g-cono
       oeelk.ordertype  = "b"
       oeelk.orderno    = v-quoteno
       oeelk.ordersuf   = 0
       oeelk.lineno     = v-lineno
       oeelk.seqno      = v-seqno
       oeelk.statustype = "a"
       oeelk.transtype  = "QU"

       oeelk.shipprod     = v-prod
       oeelk.qtyneeded    = input v-kqty
       oeelk.qtyord       = input v-kqty
       oeelk.unit         = if avail icsp and icsp.unitstock <> "" then
                              icsp.unitstock
                            else
                            if avail icsp and icsp.unitsell <> "" then
                              icsp.unitsell
                            else
                              "EACH"
                            /*
                            if avail icsw then icsw.unitstnd 
                            else if avail icsp and icsp.unitsell <> "" then
                                icsp.unitsell 
                            else
                            if avail icsp and icsp.unitstock <> "" then
                               icsp.unitstock
                            else 
                              "EACH"
                            */
       oeelk.custno       = v-custno
       oeelk.whse         = if avail oeehb then oeehb.whse else v-whse
       oeelk.arpwhse      = if avail icsw then icsw.arpwhse else v-whse
       oeelk.arpvendno    = if avail icsw then icsw.arpvendno else v-vendno 
       oeelk.arpprodline  = if avail icsw then icsw.prodline else  v-prodline
       oeelk.pricetype    = if avail icsw then icsw.pricetype else v-ptype
       oeelk.user7        = if avail icsw then dec(icsw.leadtmavg)
                            else dec(v-leadtm)
       oeelk.comptype     = "c"
       oeelk.pricefl      = no
       oeelk.price        = if v-specnstype = "n" then v-listprc else v-sellprc
       oeelk.prodcost     = v-prodcost
       oeelk.glcost       = if avail icsw then icsw.replcost else v-prodcost
       oeelk.prodcat      = if v-prodcat <> " " then v-prodcat else
                            if avail icsp then icsp.prodcat else v-prodcat
       oeelk.refer        = v-descrip
       oeelk.specnstype   = v-specnstype
       oeelk.pdrecno      = zi-pdscrecno
       /*
       oeelk.pricefl      = no
       */
       oeelk.stkqtyord    = 0
       oeelk.stkqtyship   = 0
       oeelk.reqfl        = no
       overlay(oeelk.user4,1,4)   = v-whse
       overlay(oeelk.user4,5,7)   = string(v-gp,">>9.99-")
       overlay(oeelk.user4,41,1)  = zi-qtybrkty
       overlay(oeelk.user4,42,8)  = string(zi-pdscrecno,">>>>>>>9")
       overlay(oeelk.user4,50,1)  = string(zi-level,"9")
       overlay(oeelk.user4,51,10) = string(v-pdamt,">>>>9.9999")
       overlay(oeelk.user4,61,1)  = v-pd$md
       overlay(oeelk.user4,62,11) = string(zi-totnet,">>>,>>9.99-")
       overlay(oeelk.user4,74,1)  = v-lcomment
       overlay(oeelk.user3,44,11) = string(zi-totcost,">>>,>>9.99-")
       overlay(oeelk.user3,55,1)  = v-rebatefl
       overlay(oeelk.user3,56,11) = string(zi-totmargin,">>>,>>9.99-")
       overlay(oeelk.user3,67,11) = string(zi-margpct,">>>,>>9.99-")
       oeelk.user6        = v-sparebamt
       oeelk.user8        = v-shipdt.
       
{t-all.i oeelk}
