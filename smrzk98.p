def var recno as integer no-undo.
def var sellqty as decimal no-undo.
def var sellqty2 as decimal no-undo. 
def var skipprod as character format "x(24)" no-undo.
def var zxx as integer no-undo.
def var xyr as integer no-undo.
def shared var p-kcatbeg   as c  format "x(4)" no-undo.
def shared var p-kcatend   as c  format "x(4)" no-undo.
def shared var yrz as integer extent 2 no-undo.
def shared var g-cono as integer no-undo.



def shared temp-table xwo no-undo
  field whse   like icsw.whse
  field custno like arsc.custno
  field shipto like smsew.shipto
  field pcat   like icsp.prodcat
  field prod   like icsp.prod
  field yr     as integer
  field mnth   as integer
  field qty    as decimal
  field qtyfl  as decimal
  field salesamt as decimal
  field wono   like kpet.wono
  field wosuf  like kpet.wosuf
index
  whse
  custno
  shipto
  prod
  yr
  mnth descending.



def temp-table zsmz no-undo
  field whse   like icsw.whse
  field custno like arsc.custno
  field shipto like arss.shipto
  field pcat   like icsp.prodcat
  field prod   like icsp.prod
  field yr     as integer
  field mnth   as integer
  field qty    as decimal
  field qtyfl  as decimal
  field recno  as integer
  field salesamt as decimal
  field wono   like kpet.wono
index jal
  prod
  yr   descending
  mnth descending.

def temp-table kitchron no-undo
 field whse   like icsw.whse
 field yr     like smsew.yr
 field inx    as integer
 field kpdt   as date
 field icord  as decimal
 field ictp   as character
 field wono   like kpet.wono
 field wosuf  like kpet.wosuf
 field woqty  as decimal
 field kpprod as character format "x(24)"
 
 index k-kitchron
       kpprod
       kpdt descending.
 

for each icsp where icsp.cono = g-cono and
                    icsp.prodcat ge p-kcatbeg and
                    icsp.prodcat le p-kcatend and  
                    icsp.kittype = "p"  no-lock:
  
  sellqty = 0.
  do xyr = 1 to 2:
  for each smsew where smsew.cono = g-cono
              and smsew.yr = yrz[xyr]
              and smsew.prod = icsp.prod
              and smsew.componentfl <> true
              use-index k-prod no-lock:

    do zxx = 1 to 12:
      if smsew.qtysold [zxx] <> 0 then
        do:
        recno = recno + 1.
        create zsmz.
        assign
         zsmz.whse   = smsew.whse
         zsmz.custno = smsew.custno
         zsmz.shipto = smsew.shipto
         zsmz.pcat   = icsp.prodcat
         zsmz.prod   = smsew.prod
         zsmz.yr     = if smsew.yr < 50 then (smsew.yr + 2000) else 
                                             (smsew.yr + 1900)
         zsmz.mnth   = zxx
         zsmz.qty    = smsew.qtysold[zxx]
         zsmz.salesamt = (smsew.salesamt[zxx] / smsew.qtysold[zxx])
         zsmz.recno  = recno
         zsmz.wono   = 0.
        end.
      sellqty = sellqty + smsew.qtysold[zxx].
    end.  
  end.
  end.
  if sellqty = 0 then 
    next.
  
  sellqty2 = sellqty.
  kpaccum:
  for each icet where icet.cono = g-cono 
                 /*  and icet.whse = smsew.whse   */
                   and icet.transtype = "re" 
                   and can-do("kp",icet.module)
                   and icet.prod = icsp.prod
                  
                   and icet.postdt le 
                   date(12,31,
                        if yrz[2] < 50 then
                           (yrz[2] + 2000)
                        else
                           (yrz[2] + 1900)) 
                   and icet.postdt ge 
                   date(1,01,
                        if yrz[1] < 50 then
                           (yrz[1] + 2000)
                        else
                           (yrz[1] + 1900)) no-lock
                   break by icet.postdt:
/* just parent items are processed */  
    find kpet use-index k-prod 
                        where kpet.cono = g-cono
                          and kpet.shipprod = icsp.prod
                          and kpet.whse     = icet.whse
                          and kpet.wono     = icet.orderno
                          and kpet.wosuf    = icet.ordersuf no-lock no-error.
    if not avail kpet then
      next kpaccum.                   
    
    if icet.transtype = "re" then
      if icet.module = "kp" then
         do:
          create kitchron.
          assign
             kitchron.whse     = icet.whse
             kitchron.kpdt     = icet.postdt
             kitchron.icord    = sellqty2
             kitchron.ictp     = "kp"
             kitchron.wono     = icet.orderno
             kitchron.wosuf    = icet.ordersuf
             kitchron.woqty    = icet.stkqtyship
             kitchron.kpprod   = icsp.prod.
                              
          sellqty = sellqty - icet.stkqtyship.
          if sellqty le 0 then
            leave kpaccum.
        end. 
   end.      
end.
sellqty2 = 0.
zxx = 0.

for each kitchron use-index k-kitchron break by kitchron.kpprod:
  if kitchron.kpprod = skipprod then
    next.
    
  if first-of(kitchron.kpprod) then
    do:
    find first zsmz use-index jal
         where zsmz.prod = kitchron.kpprod no-lock no-error.
    if not avail zsmz then
      do:
      skipprod = kitchron.kpprod.
      next.
      end.
    end.
  do while kitchron.woqty > 0:
    
    if zsmz.qty < 0 then
      do:
      assign sellqty = zsmz.qty.
      assign zsmz.qtyfl = sellqty.
      create xwo.
      assign
        xwo.whse   = zsmz.whse
        xwo.custno = zsmz.custno
        xwo.shipto = zsmz.shipto
        xwo.prod   = zsmz.prod
        xwo.pcat   = zsmz.pcat
        xwo.yr     = int(substring(string(zsmz.yr),3,2))
        xwo.mnth   = zsmz.mnth
        xwo.qtyfl  = sellqty
        xwo.wono   = kitchron.wono
        xwo.salesamt = zsmz.salesamt * sellqty
        xwo.wosuf  = kitchron.wosuf.
      end.
    else
    if zsmz.qty > zsmz.qtyfl then
      do:
      assign sellqty = minimum(kitchron.woqty,(zsmz.qty - zsmz.qtyfl)).
      assign zsmz.qtyfl = sellqty + zsmz.qtyfl
             kitchron.woqty = kitchron.woqty -             
                          sellqty.
      create xwo.
      assign
        xwo.whse   = zsmz.whse
        xwo.custno = zsmz.custno
        xwo.shipto = zsmz.shipto
        xwo.prod   = zsmz.prod
        xwo.pcat   = zsmz.pcat
        xwo.yr     = int(substring(string(zsmz.yr),3,2))
        xwo.mnth   = zsmz.mnth
        xwo.qtyfl  = sellqty
        xwo.wono   = kitchron.wono
        xwo.salesamt = zsmz.salesamt * sellqty
        xwo.wosuf  = kitchron.wosuf.
      end.

    if zsmz.qtyfl = zsmz.qty then
      do:
      find next zsmz use-index jal
           where zsmz.prod = kitchron.kpprod no-lock no-error.
      if not avail zsmz then
        do:
        assign
          kitchron.woqty = 0
          skipprod = kitchron.kpprod.
        next.
        end.
      end.
  end.                          
  
  
 
  if last-of(kitchron.kpprod) and avail zsmz then
    do:
    do while true:
      if zsmz.qty > zsmz.qtyfl then
        do:
        assign zsmz.qtyfl = zsmz.qty - zsmz.qtyfl.
        create xwo.
        assign
          xwo.whse   = zsmz.whse
          xwo.custno = zsmz.custno
          xwo.shipto = zsmz.shipto
          xwo.prod   = zsmz.prod
          xwo.pcat   = zsmz.pcat
          xwo.yr     = int(substring(string(zsmz.yr),3,2))
          xwo.mnth   = zsmz.mnth
          xwo.qtyfl  = zsmz.qtyfl
          xwo.salesamt = zsmz.salesamt * zsmz.qtyfl 
          xwo.wono   = kitchron.wono
          xwo.wosuf  = kitchron.wosuf.
        end.
      find next zsmz use-index jal 
           where zsmz.prod = kitchron.kpprod no-lock no-error.
      if not avail zsmz then
        do:
        skipprod = kitchron.kpprod.
        leave.
        end.
    end.
  end. 
end.    

