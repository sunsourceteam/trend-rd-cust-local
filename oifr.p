define var fileint as character format "x(9)".
define var filenamer as character format "x(14)".
define var filenamer2 as character format "x(14)".
define var commandline as character format "x(140)".
define var disrec as character format "x(130)".

fileint = string(random(111111111,999999999)).
filenamer = "/usr/tmp/" + fileint + ".flog".
filenamer2 = "/usr/tmp/" + fileint + ".stat".


/*
define stream rpt1.
  output stream rpt1 to /usr/tmp/faxrpt.doc paged page-size 60.
*/              

define var t_request_id as integer.
define var t_dest as character format "x(6)".
define var t_tries as integer format "999".
define var t_queuetime as character format "x(20)".
define var t_queue_time as character format "x(20)".
define var t_starttime as character format "x(20)".
define var t_start_time as character format "x(20)".
define var t_elapsed as character format "x(20)".
define var t_filename as character format "x(30)".
define var t_dialstring as character format "x(15)".
define var t_csi as character format "x(10)".
define var t_logname as character format "x(10)".
define var t_res as character format "x(15)".
define var t_pages as integer format "9999".
define var t_naks as integer format "99999".
define var t_a_result as character format "x(10)".
define var t_r_result as character format "x(10)".
define var t_alias as character format "x(30)".
define var t_tag1 as character format "x(30)".
define var t_tag2 as character format "x(30)".
define var t_groupnbr as character format "x(20)".
define var t_groupseq as character format "x(20)".

define var v_number  like poeh.vendno.
define var v_vendor as character format "x(30)".
define var v_buyer as character format "x(4)".
define var v_type as character format "x(2)".
define var v_orderno as integer format "99999999999".
define var v_code1 as character format "x(10)".
define var v_code2 as character format "x(10)".
define var v_ordsuf as integer format "99".
define var x as integer.
define var v_dash as character format "x".
define var v_docno as integer format "99999999999".
define var po_type as character format "x(2)".
define var companyno like poeh.cono.

define var o_update as character format "x".
define var o_endnbr as character format "x(4)".
define var o_begnbr as character format "x(4)".
define var o_optiontype as character format "x(2)".
define var o_optiondays as integer.
define var o_datep as date.
define var o_error1 as logical.

define frame fax_report
     zidc.doctype at 1 format "x(2)"
     v_orderno at 4 format ">>>>>>>>9"
     v_dash at 13 format "x(1)"
     v_ordsuf at 14 format "99"
     po_type at 17 format "x(2)"  
     zidc.docuser2 at 20 format "x(11)" 
     v_code1 at 32 format "x(10)"
     v_code2 at 43 format "x(10)"
     zidc.docuser3 at 54 format "x(4)"
     zidc.docgsnbr at 59 format ">>>>>>>9"
     zidc.docpartner at 68 format "x(4)"
     v_number at 73 format ">>>>>>>>>>9"
     v_vendor  at 85 format "x(30)"
     zidc.docuser4 at 115 format "x(12)"
     header "Date: " today
            "FAX Transmission Report" at 25
            "Page " at 65 page-number format ">>9" skip(0) 
            "Time: " string(time,"HH:MM:SS") skip (2)
            "DType  DocNbr  Typ TrnsDate    Code1      Code2    "
            " Oper EntryNbr I.D.          Destination Nbr & Name"
            "            Fax #"
             skip (1) with width 132.
companyno = 1.

output to value(filenamer2 + "t").
commandline = "vfxstat -t -U vsifax > " + filenamer2.
os-command silent value (commandline).
output close.
input from value(filenamer2).
repeat:
  import unformatted disrec.
  display disrec at 1 format "x(130)" with frame ald 
   no-box no-underline no-labels width 132.
  end.
page.
input close.

os-delete value(filenamer2).  
  


{p-rptbeg.i}

if sapb.optvalue[1] = "oe" then
  o_optiontype = "oe".

if sapb.optvalue[1] = "po" then
  o_optiontype = "po".
  
if sapb.optvalue[1] = "in" then
  o_optiontype = "in".
  
if sapb.optvalue[1] = "qu" then
  o_optiontype = "qu".

if sapb.optvalue[2] ne "" then
  o_optiondays = int(sapb.optvalue[2]).
else
  o_optiondays = 375.

if sapb.optvalue[3] = "u" then
  do:
  if sapb.reportnm = "invupdt"   or 
     sapb.reportnm = "nighupdt" or  
     sapb.reportnm = "noonupdt"  then 
     do: 
        o_update = "u".
     end.   
  else
     do:
        message "No Security clearance for update mode".
        message "Hit any key to run in non-update mode".
        pause.
        o_update = " ".
     end.
  end.   
else 
  assign o_update = " ".

if sapb.optvalue[1] ne "oe" and sapb.optvalue[1] ne "po" 
                            and sapb.optvalue[1] ne "in" 
                            and sapb.optvalue[1] ne "qu" then
  o_error1 = true.

if sapb.rangebeg[1] ne "" then
  o_begnbr = sapb.rangebeg[1].
else
  o_begnbr = "      ".

if sapb.rangeend[1] ne "" then
  o_endnbr = sapb.rangeend[1].
else
  o_endnbr = "~~~~".         


/*
 o_optiondays = 8.
 o_begnbr = "    ".
 o_endnbr = "~~~~".
 o_optiontype = "po".    
 o_update = "u".
*/




if o_update = "u" then
  do:
  output to value(filenamer + "t").
  commandline = "vfxolog -U vsifax -F pipe -f "
   + "seq,asq,nat,sbt,sbt,sti,sti,eti,thn,tfn,cli,cli,res,npg"
   + ",nat,ars,rrs,cli,tg1,tg2,gse,hst > " + filenamer.
  os-command silent value (commandline).
  output close.
  input from value(filenamer).


repeat:
  t_request_id = 0.
  t_dest = "      ".
  t_tries  = 0.
  t_queuetime = "".
  t_queue_time = "".
  t_starttime = "".
  t_start_time = "".
  t_elapsed  = "".
  t_filename = "".
  t_dialstring = "".
  t_csi = "".
  t_logname = "".
  t_res  = "".
  t_pages = 0.
  t_naks = 0.
  t_a_result = "".
  t_r_result = "".
  t_alias = "".
  t_tag1 = "".
  t_tag2 = "".
  t_groupnbr = "".
  t_groupseq = "".
  import delimiter "|"   t_request_id
                         t_dest
                         t_tries
                         t_queuetime
                         t_queue_time
                         t_starttime
                         t_start_time
                         t_elapsed
                         t_filename
                         t_dialstring
                         t_csi
                         t_logname
                         t_res
                         t_pages
                         t_naks
                         t_a_result
                         t_r_result
                         t_alias
                         t_tag1
                         t_tag2
                         t_groupnbr
                         t_groupseq.
                         
  if int(substring(t_queue_time,5,2)) > month(today) then
    o_datep = date(int(substring(t_queue_time,5,2)),
                   int(substring(t_queue_time,7,2)),
                   int(substring(t_queue_time,1,4))).
  else
    o_datep = date(int(substring(t_queue_time,5,2)),
                   int(substring(t_queue_time,7,2)),
                   int(substring(t_queue_time,1,4))).

  
  v_type = substring(t_tag2,1,2,"character").

  x=0.
  
  if v_type = "po" or v_type = "oe" or v_type = "in" or v_type = "qu" then
    do:
    x=index ((substring(t_tag2,1,15,"character")),"-").
    if x > 4 then
      do:
      v_orderno = integer(substring(t_tag2,4,(x - 4))).
      v_ordsuf = integer(substring(t_tag2,(x + 1),2)).
      end.
    else
      do:
      v_orderno = 0.
      v_ordsuf = 0.
      end.
    end.
  else
    next.
    
   /* display v_orderno v_ordsuf t_tag2 x.    */
    v_docno = v_orderno * 100.
    v_docno = v_docno + v_ordsuf.
    
    find zidc use-index docinx1 where
             zidc.cono = companyno
         and zidc.docttype = "fax"
         and zidc.docnumber = v_docno 
         and zidc.docgsnbr = t_request_id no-error.
                  
    if t_starttime = "" then
       t_starttime = t_queuetime.
    
    if not avail zidc then
      do:
      create zidc.

      assign zidc.cono = companyno
         zidc.docdate = o_datep
         zidc.docdesc = "Fax transmition from TREND"
         zidc.docdisp = "t"
         zidc.docgsnbr = t_request_id
         zidc.docnumber = v_docno 
         zidc.docpartner = " "
         zidc.docstatus = 0
         zidc.docstnbr = int(substring(t_starttime,5,10))
         zidc.docttype = "FAX"
         zidc.docuser2 =  substring(t_queue_time,5,2) + "/"
                        + substring(t_queue_time,7,2) + " " 
                        + substring(t_queue_time,9,2) + ":"
                        + substring(t_queue_time,11,2)
         substring(zidc.docuser1,1,10) = t_a_result
         substring(zidc.docuser1,21,10) = t_r_result
         zidc.docuser4 = t_dialstring
         zidc.doctype = v_type.
      end.
    else     
      do:
/* Vsi Fax puts an entry into the queue for each time it retries the
   starttime is how I'm determining the most recent status
*/      
      if int(substring(t_starttime,5,10)) > zidc.docstnbr then       
        assign
           zidc.docuser4 = t_dialstring
           zidc.docstnbr = int(substring(t_starttime,5,10))
           
           zidc.docuser2 =  substring(t_queue_time,5,2) + "/"
                          + substring(t_queue_time,7,2) + " " 
                          + substring(t_queue_time,9,2) + ":"
                          + substring(t_queue_time,11,2)
            substring(zidc.docuser1,1,10) = t_a_result
           substring(zidc.docuser1,21,10) = t_r_result. 
      end.
   if zidc.docuser1 = "NORMAL" and zidc.docuser2 = "NORMAL" then
     docstatus = 9.
   if doctype = "po" then
      do:
        find poeh where 
             poeh.cono = companyno and
             poeh.pono = int(truncate(zidc.docnumber / 100,0)) and
             poeh.posuf = int(zidc.docnumber - 
                                   (truncate(zidc.docnumber / 100,0) * 100)) 
                      no-lock no-error.                  
      if available poeh then
         do:
         find apsv where apsv.cono = companyno
                     and apsv.vendno = poeh.vendno no-lock no-error.
         zidc.docuser3 = poeh.operinit.
       
         if poeh.buyer = "" then
           zidc.docpartner = poeh.whse.
         else
           zidc.docpartner = poeh.buyer.
                 
         find first poel where poel.cono = poeh.cono 
                           and poel.pono = poeh.pono
                           and poel.posuf = poeh.posuf
                           no-lock no-error.
        if avail poel then
           do:
           find icsl where icsl.cono = poel.cono  
                       and icsl.whse = poel.whse 
                       and icsl.vendno = poel.vendno
                       and icsl.prodline = poel.prodline no-lock no-error.
           if avail icsl then
             do:
             zidc.docpartner = icsl.buyer.
             end.
           else
           if poel.buyer <> "" then
             zidc.docpartner = poel.buyer.          
         
           end.
          end.
      else               
         do:
         zidc.docpartner = "n/a".
         end.
      end.     
    
   if doctype = "oe" then
      do:
      Find oeeh where oeeh.cono = companyno and
                      oeeh.orderno = int(truncate(zidc.docnumber / 100,0)) and
                      oeeh.ordersuf = int(zidc.docnumber - 
                                   (truncate(zidc.docnumber / 100,0) * 100)) 
                     no-lock no-error.                  
      if available oeeh then
         do:
         find arsc where arsc.cono = oeeh.cono 
                     and arsc.custno = oeeh.custno no-lock no-error.
         zidc.docuser3 = oeeh.operinit.
         zidc.docpartner = oeeh.takenby.
         end.
      else               
         do:
         zidc.docpartner = "n/a".
         end.
      end.
      
   if doctype = "in" then do: 
      find oeeh where oeeh.cono = companyno and 
                      oeeh.orderno = int(truncate(zidc.docnumber / 100,0)) and
                      oeeh.ordersuf = int(zidc.docnumber - 
                                   (truncate(zidc.docnumber / 100,0) * 100))
                     no-lock no-error.
      if available oeeh then do:
         zidc.docuser3   = string(oeeh.custno).
         zidc.docfadate  = oeeh.invoicedt.
         zidc.docvendno  = (oeeh.totinvamt * 100).
         find arsc where arsc.cono = oeeh.cono and
                         arsc.custno = oeeh.custno
                         no-lock no-error.
         if avail arsc then
            zidc.docpartner = arsc.creditmgr.
         if not avail arsc then
            zidc.docpartner = "n/a".
      end.
   end.
   
   if doctype = "qu" then do: 
      find oeehb where 
           oeehb.cono = companyno and 
           oeehb.batchnm = 
               string(int(truncate(zidc.docnumber / 100,0)),">>>>>>>9") and
           oeehb.seqno   = 1 and
           oeehb.sourcepros = "Quote"
           no-lock no-error.
      if available oeehb then do:
        assign zidc.docuser3   = string(oeehb.custno)
               zidc.docfadate  = TODAY
               zidc.docvendno  = (oeehb.totinvamt * 100)
               zidc.docpartner = oeehb.takenby.
        /*       
        find arsc where arsc.cono = oeehb.cono and
                        arsc.custno = oeehb.custno
                        no-lock no-error.
        if avail arsc then
          zidc.docpartner = oeehb.takenby.
        if not avail arsc then
          zidc.docpartner = "n/a".
        */
      end.
   end.
 end.
end.  
  
  
  
  for each zidc  where 
            zidc.cono = companyno and
            zidc.docttype = "fax" use-index docinx1 no-lock 
     with frame fax_report:
    if doctype = o_optiontype and
        zidc.docdate > (today - o_optiondays) and
       
        (zidc.docpartner  le o_endnbr and
        zidc.docpartner ge o_begnbr) then      
      do:
      v_number = 0.
      v_vendor = "".
      end.
    else
      next.

    if doctype = "po" then
      do:
        find poeh where poeh.cono = companyno and
                      poeh.pono = int(truncate(zidc.docnumber / 100,0)) and
                      poeh.posuf = int(zidc.docnumber - 
                                   (truncate(zidc.docnumber / 100,0) * 100)) 
                      no-lock no-error.                  
      if available poeh then
         do:
         find apsv where apsv.cono = companyno
                     and apsv.vendno = poeh.vendno no-lock no-error.
         if avail apsv then
           v_vendor = apsv.name.
         else  
           v_vendor = "No Name".
         v_number = poeh.vendno.       
         po_type = poeh.transtype.
         end.
      end.     

    if doctype = "oe" then
      do:
        v_vendor = "".
        v_number = 0.
        
        find oeeh where oeeh.cono = companyno and
                      oeeh.orderno = int(truncate(zidc.docnumber / 100,0)) and
                      oeeh.ordersuf = int(zidc.docnumber - 
                                   (truncate(zidc.docnumber / 100,0) * 100)) 
                     no-lock no-error.                  
      if available oeeh then
         do:
         find arsc where arsc.cono = oeeh.cono 
                     and arsc.custno = oeeh.custno no-lock no-error.
         if avail arsc then
           v_vendor = arsc.name.
         else
           v_vendor = "No Name".
         v_number = oeeh.custno.  
         po_type = oeeh.transtype.
         end.
      else               
         do:
         po_type = "na".
         end.
      end.     

    if doctype = "in" then do:
        v_vendor = "". 
        v_number = 0. 
        
        find oeeh where oeeh.cono = companyno and 
                      oeeh.orderno = int(truncate(zidc.docnumber / 100,0)) and
                      oeeh.ordersuf = int(zidc.docnumber - 
                                   (truncate(zidc.docnumber / 100,0) * 100))
                     no-lock no-error. 
      if available oeeh then do:
         find arsc where arsc.cono = oeeh.cono 
                     and arsc.custno = oeeh.custno no-lock no-error. 
         if avail arsc then 
           v_vendor = arsc.name. 
         else 
           v_vendor = "No Name". 
         v_number = oeeh.custno. 
         po_type = oeeh.transtype. 
      end. 
      else do:
         po_type = "na". 
      end. 
    end. 

    
    if doctype = "qu" then do: 
      find oeehb where 
           oeehb.cono = companyno and 
           oeehb.batchnm = 
                 string(int(truncate(zidc.docnumber / 100,0)),">>>>>>>9") and
           oeehb.seqno = 1 and
           oeehb.sourcepros = "Quote"
           no-lock no-error.
      if available oeehb then
        do:
        find arsc where arsc.cono = oeehb.cono and
                        arsc.custno = oeehb.custno 
                        no-lock no-error.
        if avail arsc then
          v_vendor = arsc.name.
        else
          v_vendor = "No Name".
        v_number = oeehb.custno.  
        po_type = oeehb.transtype.
      end.
      else               
        do:
        po_type = "na".
      end.
    end.

    v_orderno = int(truncate(zidc.docnumber / 100,0)).
    v_ordsuf = int(zidc.docnumber -
                    (truncate(zidc.docnumber / 100,0) * 100)).
    v_dash = "-".
    
    v_code1 = substring (zidc.docuser1,1,10).
    v_code2 = substring (zidc.docuser1,21,10).
    
      display 
            zidc.doctype 
            v_orderno
            v_dash
            v_ordsuf
            po_type
            zidc.docuser2
            v_code1
            v_code2
            zidc.docuser3
            zidc.docgsnbr
            zidc.docpartner
            v_vendor 
            v_number
            zidc.docuser4
             with stream-io no-box no-labels.
  
  end. 
 
 if o_update  = "u" then
  do:
  v_vendor = v_vendor.
  /* os-delete value(filenamer). */
  /* os-delete value(filenamer + "t").  */
  end.
 os-delete value(filenamer2 + "t").
  
