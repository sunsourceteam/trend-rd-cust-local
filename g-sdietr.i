{&com} 
define stream xprd.
define stream zprd.


define var j-x as integer                         no-undo.
define var v-ohper  as dec                      no-undo.
define var v-totreg as dec                      no-undo.
define var v-calper as dec                      no-undo.
define var v-ooqty  as dec                      no-undo.
define var v-pcat     as char format "x(4)"     no-undo.
define var v-whse     as char format "x(4)"     no-undo.
define var v-prod     as char format "x(24)"    no-undo.
define var v-cust     like arsc.custno          no-undo.
define var v-vend     like arsc.custno          no-undo.
define var v-shipto   like arss.shipto          no-undo.
define var v-lsttrans as character              no-undo.
define var v-lstsale  as integer                no-undo.
define var v-skip     as char format "x(1)"     no-undo.
define var v-ytd      as de                     no-undo.
define var v-lytd     as de                     no-undo.

/* */

define var v-ytdxsls   as de                     no-undo.
define var v-lytdxsls  as de                     no-undo.

define var v-ytdxmgn   as de                     no-undo.
define var v-lytdxmgn  as de                     no-undo.

/* */

define var v-regions  as int      extent 5      no-undo.
define var v-cnt      as int                    no-undo.
define var v-printdir like sasc.printdir        no-undo.
define var v-testmode as logical    init true   no-undo.
define var v-process as logical                 no-undo.
define var v-name as integer                    no-undo.



define temp-table spprd no-undo

  field region     as char format "x(1)"
  field district   as char format "x(3)"
  field prod       as char format "x(24)"
  field whse       as char format "x(4)"
  field prodcat    as char format "x(4)"
  field vendno     as dec
  field custno     like arsc.custno
  field shipto     like arss.shipto
  Field lstsale    as integer
  field lsttrans   as character
  field sales      as de
/* */
  field xsales      as de
  field xmgn        as de
/* */
  field xpct       as de   
  field xused      as logical
  
  index rdpinx
        region
        district
        whse
        custno
        shipto
        prod
  
  index rdpinx2
        xused
        region
        district
        whse
        custno
        shipto
        prod
   index rdpprd
        prod
        region
        district
        whse
        custno
        shipto.
        
        
define temp-table t-spprd no-undo

  field whse       as char format "x(4)"
  field prod       as char format "x(24)"
  field sales      as de
/* */
  field xsales      as de
  field xmgn        as de
/* */

  index rdpinx
        whse
        prod.
