/*h*****************************************************************************
  INCLUDE      : p-apec9.i
  DESCRIPTION  : Print Routine for Standard Checks - APEC1.P
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    01/30/93 mms; TB#  9735 AP FP interface.
    04/28/93 jrw; TB# 10541 Modified for MC having an invoice number, and
        being negative.
    06/14/93 jrg;           Added transaction code to detail.
    12/17/93 amg; TB# 13846 A 6 digit check runs over to two lines.
    01/31/94 amg; TB# 14601 Made assing's into one statement.
    01/31/94 amg; TB# 14603 Check is always rapping onto second line
        regardless of size.
    03/29/94 bj ; TB# 14772 Show credit memo & invoice not just net.
    03/29/94 bj ; TB# 12652 MC don't sort in inv. # order.
    11/01/95 kmj; TB# 19056 Overflow printing as attached with CR's.
    12/06/96 jkp; TB# 19517 Added EDI 820 Outbound processing.  Performed
        extensive code cleanup.
    02/10/97 jkp; TB# 19517 Rearranged selections and break bys to follow
        indexes.
    09/08/98 cm;  TB# 19427 Add multi language capability.
    04/22/99 cm;  TB# 26054 Split EDI code into common include
    11/30/99 gwk; TB# e3247 Since version 8.2, apec has been printing the
        vendor# on the stub on the line below where it should.
    03/29/00 rgm; TB# 3-2  Add RxServer Interface to Standard
    04/07/00 mm;  TB# 3-2  Add RxServer Interface to Standard (missed some)
    06/09/00 rgm; TB# A9   Fix for RxServer Faxing issue
    06/28/00 rgm; TB# A27  Change for RxServer Check Print
    12/11/00 bm;  TB# e6845 Allow MC and Invoice with same inv# and due date.
        Back out e2975 and change apec to print duplicate #'s.
    04/20/01 sbr; TB# e8047 Do not allow print of cleared/bal check
    06/29/01 gwk; TB# e9241 Allow reprint of check that has not been cleared
    11/26/01 bpa; TB# e7859 Print voided checks in proper alignment
    03/04/02 lcb; TB# e12469 Overflow prints on check face
*******************************************************************************/

/*e*****************************************************************************
  REPORT FILE ASSIGNS  :

    This list is alphabetical by report field.  If no specific type (based on
        the transcd) is listed, the field is used for all types.  If specific
        types are listed, it is only used for the types listed.  The file
        prefix (b-) is not identified, unless it is critical to understanding
        the assignment.
    Types used are:
        type 0 - Draft Header - when foreign currency is being used AND a
                                draft is being purchased
        type 3 - Scheduled Pay
        type 5 - Miscellanecous Credit
        type 7 - Payment


    report.c1       = (type 0-drafthd) "f"  (foreign)
                      (type 3-schdpay) "i"
                      (type 5-misccrd) "m"
    report.c12      = (type 7-payment) apsv.lookupnm
    report.c13      = (type 7-payment) if apet.manaddrfl then
                                           apemm.zipcd
                                       else apsv.zipcd
    report.c15      = (type 3-schdpay) apet.apinvno
                      (type 5-misccrd) apet.apinvno
                      (type 7-payment) apet.apinvno
    report.c2       = (type 7-payment) if apet.manaddrfl then
                                           apemm.state
                                       else apsv.state
    report.c2-2     = (type 3-schdpay) if avail apemf then
                                           "f "  (floorplan)
                                       else ""
    report.c20      = (type 7-payment) if apet.manaddrfl then
                                           apemm.city
                                       else apsv.city
    report.c20-2    = (type 7-payment) if apet.vendno ne apet.vendno2 and
                                           avail b-apsv then
                                               b-apsv.apcustno
                                       else apsv.apcustno
    report.c24      = (type 0-drafthd) if avail apsv then
                                           apsv.name
                                       else "** Unknown **"
                      (type 3-schdpay) apet.refer
                      (type 5-misccrd) apet.refer
                      (type 7-payment) if apet.manaddrfl then
                                           apemm.name
                                       else apsv.name
    report.c24-2    = (type 0-drafthd) if avail sastc then
                                           sastc.descrip
                                       else "** Unknown **"
                      (type 7-payment) if apet.manaddrfl then
                                           apemm.addr[1]
                                       else apsv.addr[1]
    report.c24-3    = (type 7-payment) if apet.manaddrfl then
                                           apemm.addr[2]
                                       else apsv.addr[2]
    report.c4       = (type 7-payment) sastc.shortdesc   (Blank Unless Foreign)
    report.c4-2     = (type 7-payment) sastc.draftfl     ("yes" to Purchase
                                                          Draft, else "no")
    report.c6       = (type 7-payment) apet.currencyty   (Blank Unless Foreign)
    report.cono     = apet.cono
    report.de12d0   = apet.vendno
    report.de9d2s   = (type 3-schdpay) apet.paymtamt
                      (type 5-misccrd) apet.paymtamt
                      (type 7-payment) if apet.currencyty ne "" and
                                           apet.exchgrate[2] ne 0 and
                                           avail sastc and
                                           sastc.draftfl then
                                               round(apet.paymtamt *
                                                   apet.exchgrate[2],2)
                                       else apet.paymtamt
    report.de9d2s-2 = (type 3-schdpay) apet.discamt
    report.de9d2s-3 = (type 5-misccrd) apet.amount
    report.dt       = (type 3-schdpay) apet.invdt
                      (type 5-misccrd) apet.invdt
    report.faxfl    = (type 7-payment) true if a physical check will be printed
                                       false if EDI will used for the payment
    report.i2       = apet.transcd     0 = Draft Header
                                       3 = Sheduled Pay
                                       5 = Miscellaneous Credit
                                       7 = Payment
    report.i3       = apet.setno
    report.i4       = (type 7-payment) apet.seqno     (Indicates # print lines)
    report.i7       = (type 7-payment) apet.checkno   (This is the check number
                                                       if a physical check was
                                                       printed, otherwise it is
                                                       the trace number of the
                                                       electronic payment.)
    report.l        = (type 0-drafthd) false
                      (type 3-schdpay) false
                      (type 5-misccrd) false
                      (type 7-payment) true
    report.oper2    = g-operinit
    report.outputty = (type 7-payment) "e" if the payee vendor uses EDI for
                                           payments - otherwise blank
    report.reportnm = sapb.reportnm
    report.rid      = recid(b-apet)
*******************************************************************************/

/*e  The report.faxfl is being used on a type 7 (payment) to specify if a
     physical check will be printed or if an electronic means will be used to
     send the check.  If the report.faxfl is set to true, a physical check will
     be printed.  If the reprot.faxfl is set to false, an electronic means is
     being used to send the check.  The report.outputty is being used on a
     type 7 (payment) to indicate if the payee uses EDI.  If the payee uses
     EDI, the report.outputty will be set to an "e", otherwise, it will be
     blank. */

/*tb 19517 02/10/97 jkp; Added assign of g-outputty and g-faxfl to be used
     in apecvd.p. */
assign
    g-outputty = report.outputty
    g-faxfl    = report.faxfl.

if p-bankno = 95 or p-bankno = 15 or p-bankno = 16 or p-bankno = 17 or
                    p-bankno = 20 then
  assign p-cancheckdt = string(YEAR(p-checkdt),"9999") +
                        string(MONTH(p-checkdt),"99")   +
                        string(DAY(p-checkdt),"99").
if p-bankno = 93 or ((p-bankno = 41 or p-bankno = 51) and g-cono = 80) then
  assign p-cancheckdt = string(DAY(p-checkdt),"99")    +
                        string(MONTH(p-checkdt),"99")  +
                        string(YEAR(p-checkdt),"9999").

/*SI01-Begin*/
/* si01: load check form for each new check */
if p-nonneg = "no" or (p-nonneg = "yes" and
                       apsv.email = " ") then       /* das - emailing */
  put control
      v-reset +
      v-lineterm +
      v-deftray +
      v-rxform +
      v-noperf +
      v-6lpi +
      v-chkfont.
else
  put control
    v-lineterm +
    v-deftray +
    v-rxform +
    v-noperf +
    v-6lpi + 
    v-chkfont.

/*SI01-End*/
/*tb 19517 12/06/96 jkp; Added loop around all of this processing to allow for
     not  printing the check when ACH or some other electronic method is used.
     This is only available when using EDI.  EDI will do it's own processing
     at the end of this code, whether a check was printed or not. */
if report.faxfl = yes then
do:

  /*tb e8047 04/20/01 sbr; Modified the code block that handles existing cret
      records to include code that will prevent the use of check numbers that
      have already been cleared. */
  /*tb e9241 06/29/01 gwk; Allow reprint of checks if p-reprintfl is on */
  /*d On a reprint, by the time this code is hit, the cret record has already
      been changed to an inactive status and the cleardt and clearfl have been
      set, so checking the statustype won't tell us anything.  We have to take
      their word for it that they know what they are doing on a reprint, so we
      allow a reprint of any check number (we void the cret record of the
      first printing, and that's already happened by the time this code gets
      hit.) */
  do while can-find(first cret use-index k-checkno where
      cret.cono      = g-cono    and
      cret.bankno    = p-bankno  and
      cret.checkno   = p-checkno and
      cret.ckrectype = 1         and
      p-reprintfl    = no):

    for each cret use-index k-checkno where
        cret.cono      = g-cono    and
        cret.bankno    = p-bankno  and
        cret.checkno   = p-checkno and
        cret.ckrectype = 1         no-lock:

      if cret.statustype = true then do:

        /*e Creates CRET records.  Sending the set number and the reference. */
        run apecvd.p(report.i3,"(Duplicate Check)").
        
        /*tb A9 06/09/00 rgm; Rxserver interface change when appropriate */
        &IF DEFINED(rxserv_format) = 0 &THEN
            do j = 1 to 20:
        &ELSE
             display with frame f-top.
             do j = 1 to (18 + INTEGER({&rxserv_chkoffset})):
        &ENDIF

            display
            with frame f-invoices.

            down with frame f-invoices.

        end. /* do j = 1 to 20 */

        /* si01 - rather than spacing down, specify check location */
        put control v-chkpos1.                              /*si01*/
        if p-bankno <> 95 and p-bankno <> 93 and p-bankno <> 81 and
           p-bankno <> 41 and p-bankno <> 51 then
          display
            v-void                                       @ v-textamt
            v-voidnum                                    @ p-checkamt
            "(Check # " + string(p-checkno) + " Exists)" @ v-addr[1]
            ""                                           @ v-addr[2]
            ""                                           @ v-addr[3]
            ""                                           @ v-addr[4]
            p-checkdt
            p-checkno                                    when p-numprintfl
          with frame f-checkdom.
        else
          display
              v-void                                       @ v-textamt
              v-voidnum                                    @ v-checkamt
              "(Check # " + string(p-checkno) + " Exists)" @ v-addr[1]
              ""                                           @ v-addr[2]
              ""                                           @ v-addr[3]
              ""                                           @ v-addr[4]
              p-cancheckdt
              p-checkno                                    when p-numprintfl
          with frame f-cancheck.
      end. /* if cret.statustype = true */

    end. /* for each cret */
    
  /*  if p-nonneg = "no" or (p-nonneg = "yes" and apsv.email = " ") then*/
      put control v-ff.                            /* si01 - force page */
    
    assign
        v-lastcheckno = p-checkno
        p-checkno     = p-checkno + 1.

  end. /* do while can-find cret */

    
    /*SI01-Begin*/
    /* si01 - init pageno
              init lines per page
              set up micr line */
    assign
        v-pageno  = 1
        v-pglines = v-chklines
        v-chkdata = if (p-bankno = 91 /*or p-bankno = 93*/) then
                       v-micrpos + v-micrfont + ":" + 
                       string(p-checkno) + v-bankmicr
                       /*
                       string(p-checkno,">>>>>999") + v-bankmicr
                       */
                    else 
                      if p-bankno = 95 then
                         v-micrpos + v-micrfont + ":" + 
                         string(p-checkno) + v-bankmicr + "  43"
                      else
                        if p-bankno = 93 or (p-bankno = 41 and g-cono = 80)
                         then
                          v-micrpos + v-micrfont + ":" +
                          string(p-checkno) + v-bankmicr
                      else
                        if p-bankno = 51 and g-cono = 80 then
                          v-micrpos + v-micrfont + ":" + 
                          string(p-checkno) + v-bankmicr + "  45"
                      else
                        if p-bankno = 22 then
                          v-micrpos + v-micrfont + "  " + ":" +
                          string(p-checkno) + v-bankmicr
                      else
                        if p-bankno = 53 or p-bankno = 15 or
                           p-bankno = 16 or p-bankno = 17 or
                           p-bankno = 20 or p-bankno = 29 or
                          (p-bankno = 2 and g-cono = 80) or
                          (p-bankno = 4 and g-cono = 80)
                          then
                          v-micrpos + v-micrfont +  ":" +
                         string(p-checkno,"9999999") +  v-bankmicr
                      else 
                        if p-bankno = 5  or  
                           p-bankno = 44 /*or p-bankno = 15*/ then
                          v-micrpos + v-micrfont + ":" +
                           string(p-checkno) + v-bankmicr
                      else
                        if p-bankno = 15 or p-bankno = 16 or    /*Canada*/
                           p-bankno = 17 or p-bankno = 20 then  
                          v-micrpos + v-micrfont + ":" +
                           string(p-checkno,"999999999") + v-bankmicr
                      else
                         v-micrpos + v-micrfont + ":" + 
                         string(p-checkno,">>>>>999") + v-bankmicr.
    /*SI01-End*/
    
    
    /*tb 19056 11/02/95 kmj; Credit memos are not reflected in the variable
         report.i4.  This causes an unwanted overflow condition when the number
         of credit memos and the number of invoices are greater than the "page"
         size.  To correct this, a count of the number of credit memos will be
         done.  This count will then be subtrtacted from the "page" size.  The
         number of credit memos is stored in v-crmemocnt. */
    v-crmemocnt = 0.

    /*tb 19517 02/10/97 jkp; Rearranged selection to follow the index.  Added
         b-report.outputty. */
    /*e This b-report loop is ONLY used to count the number of credit memos. */
    /*e Report.outputty is blank except for type 7 where it can be blank or
        "e".  Report.l is true if type 7 (payment), otherwise it is false.
        Report.i3 is the apet.setno. */
    if p-nonneg = "yes" and apsv.email <> " " then
      assign sdi-cono = 2.
    else
      assign sdi-cono = v-cono.
    for each b-report use-index k-report where
             b-report.cono     = sdi-cono   and
             b-report.oper2    = g-operinit and
             b-report.reportnm = v-reportnm and
             b-report.outputty = ""         and
             b-report.l        = false      and
             b-report.i3       = report.i3
    no-lock:

        find b-apet where
             recid(b-apet) = b-report.rid
        no-lock no-error.

        /*tb e6845 12/11/00 bm; Allow MC and IN with same inv# and due date.
            Back out e2975 and change apec to print duplicate #'s. */
        /*e Apet.transcd 3 is Scheduled Pays. Apet.statustype no is inactive.
            Apet.paymtcd 2 is Credit Applied and 3 is Manual Check or Misc.
            Credit Applied.
            Only look for credits applied to Invoices (Sch Payments),
            as Misc. Credits do not have payments applied to themselves.*/

        if b-apet.transcd = 3 then
        for each apet use-index k-apinvno where
                 apet.cono       = v-cono          and
                 apet.vendno     = b-report.de12d0 and
                 apet.apinvno    = b-report.c15    and
                 apet.transcd    = 3               and
                 apet.statustype = no              and
                 apet.duedt      = b-apet.duedt    and
                (apet.paymtcd    = 3               or
                 apet.paymtcd    = 2)
        no-lock:
            v-crmemocnt = v-crmemocnt + 1.
        end. /* for each apet */
    end. /* for each b-report */

    /*tb  9735 01/30/93 mms */
    /*tb 19056 11/02/95 kmj; Subtract number of credit memos from "page"
         size. */
    /*e V-stream2fl is set to true when a separate stub is requested
        (p-remittype = "s"), OR an overflow stub is requested (p-remittype =
        "o") and the number of lines (report.i4) is greater than 12 - the
        number of credit memo lines, OR an overflow stub is requested and the
        number of lines is greater than 11 - the number of credit memo lines
        and the currency is foreign (report.c4 ne "") and a draft is being
        purchased (report.c4-2 = "yes"), OR floor planning is being used
        (report.c2-2 = "f"). */

        /* tb e12469 03/04/02 lcb; Overflow prints on check face. */
    assign
        /*tb A9 06/09/00 rgm; Rxserver interface change when appropriate */
        &IF DEFINED(rxserv_format) = 0 &THEN
        v-stream2fl = if p-remittype = "s"                                   or
                      (p-remittype = "o" and report.i4 > (12 - v-crmemocnt)) or
                      (p-remittype = "o" and report.i4 > (11 - v-crmemocnt) and
                      report.c4 ne "" and report.c4-2 = "yes")               or
                      report.c2-2 = "f" then
                          true
                      else false
        &ELSE
        v-stream2fl = if p-remittype = "s"                                   or
                      (p-remittype = "o" and report.i4 > (31 - v-crmemocnt)) or
                      (p-remittype = "o" and report.i4 > (30 - v-crmemocnt) and
                      report.c4 ne "" and report.c4-2 = "yes")               or
                      report.c2-2 = "f" then
                          true
                      else false
        &ENDIF
        i           = 0
        t-gross     = 0
        t-net       = 0
        t-disc      = 0.
          
    /*SI01-Begin*/
    /* si01; move check print before detail print */
    /******************* Print Check *****************************/
    if report.de9d2s ne 0 then
        run wordnum.p(report.de9d2s,output v-textamt).
    assign
        v-textamt   = if report.de9d2s = 0 then
                "*** Zero Amount - VOID ***" else
              if report.c4 ne "" and report.c4-2 = "no"
              then v-textamt + " " + report.c4
              else v-textamt
              /* No Draft Purchased, Print in Foreign Currency */
        v-addr[4]   = CAPS(report.c20 + ", " + report.c2 + "  " + report.c13)
        v-addr[3]   = CAPS(report.c24-3)
        v-addr[2]   = CAPS(report.c24-2)
        v-addr[1]   = CAPS(report.c24)
        p-checkamt  = report.de9d2s
        v-checkamt  = report.de9d2s
        v-textamt   = v-textamt +
              fill("*",maximum(0,65 - length(v-textamt)))
        report.i7   = p-checkno
        v-chktext   = v-textamt

       /*tb 6260 06/13/92 mjm: split textamt into two lines if necessary */
       /*tb 13846 12/17/93 amg; A 6 digit check runs over two lines */
       /*tb 14601 01/31/94 amg; Made assign's into one statement */
       /*tb 14603 01/31/94 amg; Check is always rapping to second line */
       v-textamt  = substring(v-chktext,1,66)
       v-blankpos = r-index(v-textamt," ")
       v-textamt  = if length(v-chktext) > 66 then
           substring(v-chktext,1,v-blankpos - 1) else v-textamt
       v-textamt2 = if length(v-chktext) > 66 then
           substring(v-chktext,v-blankpos + 1) else " ".

    do j = 1 to 3:
        if v-addr[j]  = "" then assign
        v-addr[j]     = v-addr[j + 1]
        v-addr[j + 1] = "".
    end.
    
    /* position for check print */
    put control v-chkpos1.
    if p-bankno <> 95 and p-bankno <> 93 and p-bankno <> 81 and
       p-bankno <> 41 and p-bankno <> 51 and p-bankno <> 15 and
       p-bankno <> 16 and p-bankno <> 17 and p-bankno <> 20 then
      display
          p-checkdt
          p-checkamt
          v-textamt
          v-textamt2
          v-addr
          p-checkno    when p-numprintfl
      with frame f-checkdom.
    else
      display
          p-cancheckdt
          v-checkamt
          v-textamt
          v-textamt2
          v-addr
          p-checkno    when p-numprintfl
      with frame f-cancheck.

    /* print micr line, reposition and select check font for remittance */
    put control v-chkdata + v-tof + v-chkfont.
    /* si01 - removed headings from top frame */
    /*if emailcnt = 0 or p-nonneg = "no" then*/
      display
        v-pageno
        report.de12d0
        p-checkno
      with frame f-top.
    /*if p-nonneg = "yes" and apsv.email <> " " then
      assign emailcnt = emailcnt + 1.*/
    /*SI01-End*/
     /*SI01 - Begin == all stream2 not needed ....
      *******************************************
      *******************************************
    **********************************
    **********************************/
 
    /*********************** Invoices & Misc Credits **********************/
    /*tb 12652 03/29/94 bj ; Swapped by b-report.c1 and b-report.c15 in by. */
    /*tb 19517 02/10/97 jkp; Rearranged selection to follow index.  Added
         outputty to the selection.   Added cono oper2, reportnm, and outputty
         to the break by to follow index. */
    /*e Report.outputty is blank except for type 7 where it can be blank or
        "e".  Report.l is true if type 7 (Payment) otherwise false.  Report.i3
        is the apet.setno.  Report.c15 is apet.apinvno.  Report.c1 is "i"
        (scheduled pay), "f" (foreign currency & purchasing draft), or "m"
        (misc. credit). */
    if p-nonneg = "yes" and apsv.email <> " " then
      assign sdi-cono = 2.
    else
      assign sdi-cono = v-cono.
    assign emailcnt = 0.
    for each b-report use-index k-report where
             b-report.cono     = sdi-cono   and
             b-report.oper2    = g-operinit and
             b-report.reportnm = v-reportnm and
             b-report.outputty = ""         and
             b-report.l        = false      and
             b-report.i3       = report.i3
    no-lock
    break by b-report.cono
          by b-report.oper2
          by b-report.reportnm
          by b-report.outputty
          by b-report.i3
          by b-report.c15
          by b-report.c1:
        /*****
        if p-nonneg = "yes" and apsv.email <> " " then
          do:
          assign emailcnt = emailcnt + 1.
          if emailcnt > 10 then
            do:
            assign emailcnt = 0.
            put control   
              v-lineterm +
              v-deftray + 
              v-rxform +  
              v-noperf +  
              v-6lpi +    
              v-chkfont.  
            assign v-pageno = v-pageno + 1.
            put control v-ff.
            display          
               v-pageno
               report.de12d0  
               p-checkno      
            with frame f-top.
          end.
        end. /* p-nonneg = "yes" */
        ******/
        /*tb 14772 04/04/94 bj ; Moved find of apet. */
        find apet where
             recid(apet) = b-report.rid
        no-lock no-error.

        /*tb 14772 03/29/94 bj ; Added call to apecmc.p. */
        v-dsplyfl = no.
        /*tb 3-2 03/29/00 rgm; Rxserver interface call when appropriate */
        &IF DEFINED(rxserv_format) = 0 &THEN
            run apecmc.p
                (input recid(b-report), input v-stream2fl, input-output i).
        &ELSE
            run apecmc{&rxserv_format}.p
                (input recid(b-report), input v-stream2fl, input-output i).
        &ENDIF

        /*tb 10541 04/28/93 jrw; (was above v-gross) */
        /*tb 14772 03/29/94 bj ; New and modified assigns. */
        /*tb ????? 06/14/93 jrg; Added transaction code (v-transcd) to detail.
             */
        if p-nonneg = "yes" and apsv.email <> " " and 
                                v-dsplyfl = no and v-pageno > 2 then
          assign i = i + 1.
        
        assign
            
            /* si01 - commented out line increment because that is now
               done in laserlin.i, which also pages as needed for laser,
               and is used in APECMC9.P above and in this code. */
            /* i = if v-dsplyfl = no then i + 1 else i */

            v-gross   = if b-report.c1 = "f" then
                            0
                        else if b-report.c1 = "m" then
                            b-report.de9d2s-3
                        else b-report.de9d2s + b-report.de9d2s-2
            v-net     = if b-report.c1 = "f" then
                            0
                        else b-report.de9d2s
            v-refer   = b-report.c24
            v-disc    = if b-report.c1 = "f" then
                            0
                        else (b-report.de9d2s-2  * -1)
            v-invno   = if b-report.c1 = "f" then
                            ""
                        else if (b-report.c1 = "i" and
                            apet.apptranscd = 8) then
                                apet.appinvno
                        else if b-report.c1 = "i" and v-dsplyfl = no then
                            b-report.c15
                        else if b-report.c1 = "i" then
                            "Payment"
                        else b-report.c15
            v-transcd = if b-report.c1 = "f" then
                            ""
                        else if (b-report.c1 = "i" and
                            apet.apptranscd = 8) then
                                v-translist[apet.apptranscd + 1]
                        else v-translist[b-report.i2 + 1]
            t-gross   = t-gross + v-gross
            t-net     = t-net   + v-net
            t-disc    = t-disc  + v-disc.

        /*SI01 - no alternate steams ... comment out ...
        ***********************************************
        ***********************************************
        **********************************
        **********************************/
        
        do:  /*si01 - added do:*/
            /*SI01-Begin ... rewrite ... comment out ...
            *********************************************
            *********************************************
            **************************************
            **************************************/

            if b-report.c1 = "f" then
            do:

                display
                    b-report.c24
                    b-report.c24-2
                with frame f-fcurr.
                down with frame f-fcurr.
                {laserlin.i}                                /*si01*/

            end. /* if b-report.c1 */

            else if b-report.c1 ne "i" or v-dsplyfl = no then
            do:
                /*tb 10541 04/28/93 jrw */
                /*tb ????? 06/14/93 jrg; (was beside v-transcd) */
                display
                    b-report.dt when b-report.c1 ne "f"
                    v-invno
                    v-refer     when b-report.c1 ne "f"
                    v-transcd
                    v-gross     when b-report.c1 ne "f"
                    v-net       when b-report.c1 ne "f"
                    v-disc      when b-report.c1 ne "f"
                with frame f-invoices.
                down with frame f-invoices.
                {laserlin.i}                                /*si01*/

            end. /* else if b-report.c1 ne "i" or v-dsplyfl = no */

            /*e Report.i3 is the apet.setno. */
            if last-of(b-report.i3) then
            do:

                display
                    t-net
                    t-disc
                    t-gross
                with frame f-totals.
                /*message  "J:" j  "I:" i.*/
           /*tb 3-2 03/29/00 rgm; Rxserver interface change when appropriate */
                 &IF DEFINED(rxserv_format) = 0 &THEN
                    do j = 1 to (14 - i):
                &ELSE
                    do j = 1 to ((14 + INTEGER({&rxserv_chkoffset})) - i):
                &ENDIF
                    display
                    with frame f-invoices.

                    down with frame f-invoices.
                end. /* do j = 1 to (14 - i) */

            end. /* if last of(b-report.i1) */

        end. /* if not v-stream2fl (else) */
    end. /* for each b-report (end of invoices and  misc credits) */

  /* if p-nonneg = "no" or (p-nonneg = "yes" and apsv.email = " ") then */
      if v-pglines = v-chklines then put control v-ff.  /*SI01-page*/

end. /* if report.faxfl = yes */

   /*SI01 - not used here moved above SdI01 custom code - comment out ...
    ***********************************************
    ***********************************************
     ***********************************************
     ***********************************************/

assign
    v-lastcheckno = if report.faxfl = yes then
                        p-checkno
                    else v-lastcheckno
    p-checkno     = if report.faxfl = yes then
                        p-checkno + 1
                    else p-checkno
    p-traceno     = if report.faxfl = no then
                        p-traceno + 1
                    else p-traceno
    v-textamt2    = "".


/*tb 19517 12/06/96 jkp; Added complete section to output EDI data. */
/*tb 26054 04/22/99 cm; Split EDI code into common include */
/*o Output EDI Data */

/* Added edi processing 07/29/04  -smaf- */

{apecedi.gpr}

/********************************* SI02 - Block EDI processing    
{apecedi.gpr}
********************************** SI02 - Block EDI processing */




