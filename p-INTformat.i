
/*
   Interpret-Input will take the from the given file and create 
   t-record for the record level logging and
   t-data   for the field level logging and combine the attributes 
            from t-formats (g-INTformat.i).
*/            
procedure Interpret-Input:
  
  define input parameter        ip-tablename  as char    no-undo.
  define input parameter        ip-datarec    as char    no-undo.
  define input parameter        ip-numfields  as int     no-undo.

  Define Var iv-index           as int                no-undo.
  Define Var iv-token           as char               no-undo.
  Define Var iv-fmtText         as char               no-undo.
  
  Define Var iv-errorcd         as char               no-undo.
  Define Var iv-errorlit        as char               no-undo.
   


  create t-record. 
  assign 
      t-record.recno            =  v-reccount
      t-record.tblname          =  ip-tablename 
      t-record.errorcd          =  " ".
  
  Do iv-index = 1 to ip-numfields:
    assign iv-token = "". 
    find t-formats use-index k-locat where 
         t-formats.locator = iv-index no-lock no-error.
    if avail t-formats then do:
      assign iv-token  =
        substring(ip-datarec,t-formats.fldbeg,t-formats.length).
    end.
    else
    if not avail t-formats then do:
      assign ip-datarec = "error " + string(iv-index).
    end.
    if avail t-formats and t-formats.fldtype = "DATE" and
           ( iv-token = " " or iv-token = "000000") then do:
       assign iv-token = ?.    
    end.  


    assign iv-errorcd = ""
           iv-errorlit = "".

    if t-formats.name = "phoneno" or
       t-formats.name = "faxphoneno" or
       t-formats.name = "pophoneno" then
      assign iv-token = 
        replace(replace(replace(iv-token,"-",""),"(",""),"(","").
    
    assign iv-fmtText = {p-INTstring.i 
                           &exp1    = "t-formats.fldexpression1"
                           &source = "iv-token"
                           &format  = "t-formats.fldexpression2"}

    if error-status:error or 
       error-status:num-messages <> 0 then do:
       
      assign iv-errorcd = if error-status:error then
                            "E"                /* error */
                          else
                            "W"               /* warning */
            iv-errorlit = error-status:get-message  (1).            
    end.
    if iv-errorcd <> "" then
      assign t-record.errorcd = if t-record.errorcd = "E" then
                                  t-record.errorcd
                                else
                                  iv-errorcd. 
    
    create t-data. 
    assign 
      t-data.recno            =  v-reccount
      t-data.tblname          =  ip-tablename 
      t-data.fieldval         =  iv-token
      t-data.locator          =  t-formats.locator 
      t-data.fldbegin         =  t-formats.fldbegin 
      t-data.fldend           =  t-formats.fldend 
      t-data.length           =  t-formats.length 
      t-data.descr            =  t-formats.descr 
      t-data.name             =  t-formats.name 
      t-data.extent           =  t-formats.extent
      t-data.fldtype          =  t-formats.fldtype 
      t-data.errorcd          =  iv-errorcd 
      t-data.errorlit         =  iv-errorlit
      t-data.fldexpression1   =  t-formats.fldexpression1 
      t-data.fldexpression2   =  t-formats.fldexpression2
      t-data.fldexpression3   =  t-formats.fldexpression3.

      find zsdifieldx where
           zsdifieldx.rectype = " " and
           zsdifieldx.tblname = t-data.tblname and
           zsdifieldx.fldname = t-data.name + 
           left-trim(string(t-data.extent,">>")) no-lock no-error.
      if not avail zsdifieldx and t-data.name <> "skip" then do:
        message "No default found" t-data.tblname t-data.name +
          left-trim(string(t-data.extent,">>")). pause.
      end.     
      else
      if not avail zsdifieldx and t-data.name = "skip" then do:
        next.
      end.     
  
      else
      if t-data.fieldval  = "" and avail zsdifieldx then do:
        assign t-data.fieldval = zsdifieldx.defaultval.
      end.     
  end. /* do iv-index */
  run Interpret-Defaults ( input ip-tablename,
                           input t-record.recno).
end.


/*
   Interpret-Defaults will take the from the given defaults and create 
   t-data   for the field level logging and combine the attributes 
            from t-formats (g-INTformat.i).
*/            
procedure Interpret-Defaults:
  
  Define input parameter        ip-tablename  as char    no-undo.
  Define input parameter        ip-recordno   as int     no-undo.

  Define buffer bt-record       for t-record.
  Define Var iv-index           as int                no-undo.
  Define Var iv-token           as char               no-undo.
  Define Var iv-fmtText         as char               no-undo.
  
  Define Var iv-errorcd         as char               no-undo.
  Define Var iv-errorlit        as char               no-undo.
   

  find bt-record where bt-record.recno = ip-recordno no-error.
  if not avail bt-record  then
    return.

  For each zsdifieldx where
           zsdifieldx.tblname = t-data.tblname no-lock:

    find t-data where  
         t-data.recno            =  v-reccount       and
         t-data.tblname          =  ip-tablename     and
         t-data.name             =  zsdifieldx.fldnameE and
         t-data.extent           =  zsdifieldx.extent no-error.
   /*
    if avail t-data and  t-data.fieldval <> " " then 
      next.    
   
        if zsdifieldx.fldnameE = "phoneno" then do: 
 
    message "2" ip-tablename zsdifieldx.fldnameE   zsdifieldx.extent
    t-data.fieldval. pause.

    end.
  */  
    if avail t-data then
      next.
    
    
    assign iv-token = "". 
    assign iv-token  = zsdifieldx.defaultval.        
    if zsdifieldx.fldtype = "DATE" and
           ( iv-token = " " or iv-token = "000000") then do:
       assign iv-token = ?.    
    end.  


    assign iv-errorcd = ""
           iv-errorlit = "".
    assign iv-fmtText = {p-INTstring.i 
                           &exp1    = "zsdifieldx.fldexpression1"
                           &source = "iv-token"
                           &format  = "zsdifieldx.fldexpression2"}

    if error-status:error or 
       error-status:num-messages <> 0 then do:
       
      assign iv-errorcd = if error-status:error then
                            "E"                /* error */
                          else
                            "W"               /* warning */
            iv-errorlit = error-status:get-message  (1).            
    end.
    if iv-errorcd <> "" then
      assign bt-record.errorcd = if bt-record.errorcd = "E" then
                                  bt-record.errorcd
                                else
                                  iv-errorcd. 
    
    
    create t-data. 
    assign 
      t-data.recno            =  v-reccount
      t-data.tblname          =  ip-tablename 
      t-data.fieldval         =  iv-token
      t-data.locator          =  0 
      t-data.fldbegin         =  0 
      t-data.fldend           =  0 
      t-data.length           =  0 
      t-data.descr            =  zsdifieldx.descr 
      t-data.name             =  zsdifieldx.fldnameE
      t-data.extent           =  zsdifieldx.extent
      t-data.fldtype          =  zsdifieldx.fldtype 
      t-data.errorcd          =  iv-errorcd 
      t-data.errorlit         =  iv-errorlit
      t-data.fldexpression1   =  zsdifieldx.fldexpression1 
      t-data.fldexpression2   =  zsdifieldx.fldexpression2
      t-data.fldexpression3   =  zsdifieldx.fldexpression3.
    assign t-data.fieldval = zsdifieldx.defaultval.
         
  end. /* for each */

end.

