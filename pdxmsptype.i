/*****************************************************************************
   
 pdxmsptype.i   - Sales Price Matrix Print Include
                
*****************************************************************************/

procedure sort-by-ptype:
  
  for each matrix use-index k-ptype no-lock
                                              
     break by matrix.pd-level
           by matrix.customer
           by matrix.ship-to
           by matrix.pprice-type
           by matrix.i-ytd-amt descending
           by matrix.product:

     assign s-isales    = s-isales + (matrix.i-amount - matrix.i-discount)
            s-imrgsales = s-imrgsales + 
                             (matrix.i-amount - matrix.i-discount) -
                              matrix.i-cost
            s-msales    = s-msales + matrix.m-amount
            s-mmrgsales = s-mmrgsales +
                              (matrix.m-amount - matrix.m-cost)
            s-gsales    = s-gsales + (matrix.g-amount - matrix.g-discount)
            s-gmrgsales = s-gmrgsales +
                             (matrix.g-amount - matrix.g-discount) -
                              matrix.g-cost.
                          
     assign s-ytd-isales = s-ytd-isales + (matrix.i-amount - matrix.i-discount)
            s-ytd-imrgsales = s-ytd-imrgsales +
                              (matrix.i-amount - matrix.i-discount) -
                               matrix.i-cost
            s-ytd-msales    = s-ytd-msales + matrix.m-amount
            s-ytd-mmrgsales = s-ytd-mmrgsales +
                              (matrix.m-amount - matrix.m-discount) -
                               matrix.m-cost
            s-ytd-gsales = s-ytd-gsales + (matrix.g-amount - matrix.g-discount)
            s-ytd-gmrgsales = s-ytd-gmrgsales +
                              (matrix.g-amount - matrix.g-discount) -
                               matrix.g-cost.
     
     if s-isales <> 0 then
        assign s-imarg = (s-imrgsales / s-isales) * 100.
     if s-msales <> 0 then
        assign s-mmarg = (s-mmrgsales / s-msales) * 100.
     if s-gsales <> 0 then
        assign s-gmarg = (s-gmrgsales / s-gsales) * 100. 
        
     if p-format = 1 then do:
        if p-gnlmtrx = no then do:
           display matrix.customer     
                   matrix.ship-to
                   matrix.name         
                   matrix.salesrep     
                   matrix.pprice-type  
                   matrix.proddesc     
                   matrix.ytdsales     
                   matrix.last-ytdsales
                   matrix.i-ytd-amt    
                   matrix.pct-mult     
                   matrix.type         
                   matrix.pd-disctp    
                   s-imarg             
                   s-mmarg             
                   matrix.pd-recno     
                   matrix.pd-level     
                   matrix.pd-enddt     
                   matrix.pd-trandt    
           with frame f-1-detail.
           down with frame f-1-detail.
        end.
        else do:
           display matrix.customer     
                   matrix.ship-to
                   matrix.name         
                   matrix.salesrep     
                   matrix.pprice-type  
                   matrix.proddesc     
                   matrix.ytdsales     
                   matrix.last-ytdsales
                   matrix.i-ytd-amt    
                   matrix.pct-mult     
                   matrix.type         
                   matrix.pd-disctp    
                   s-imarg             
                   s-mmarg             
                   s-gmarg             
                   matrix.pd-recno     
                   matrix.pd-level     
                   matrix.pd-enddt     
                   matrix.pd-trandt    
           with frame f-1g-detail.
           down with frame f-1g-detail.
        end.
     end.
     else do:
        if p-gnlmtrx = no then do:
           display matrix.customer   
                   matrix.ship-to
                   matrix.name       
                   matrix.cprice-type
                   matrix.salesrep   
                   matrix.pprice-type
                   matrix.proddesc   
                   matrix.i-ytd-amt  
                   matrix.pct-mult   
                   matrix.type       
                   matrix.pd-disctp  
                   s-imarg           
                   s-mmarg           
                   matrix.pd-recno   
                   matrix.pd-level   
                   matrix.pd-enddt   
           with frame f-2-detail.
           down with frame f-2-detail.
        end.
        else do:
           display matrix.customer   
                   matrix.ship-to
                   matrix.name       
                   matrix.cprice-type
                   matrix.salesrep   
                   matrix.pprice-type
                   matrix.proddesc   
                   matrix.i-ytd-amt  
                   matrix.pct-mult   
                   matrix.type       
                   matrix.pd-disctp  
                   s-imarg           
                   s-mmarg           
                   s-gmarg           
                   matrix.pd-recno   
                   matrix.pd-level   
                   matrix.pd-enddt   
           with frame f-2g-detail.
           down with frame f-2-detail.
        end.
     end.
     
     assign s-isales    = 0
            s-msales    = 0
            s-gsales    = 0
            s-imrgsales = 0
            s-mmrgsales = 0
            s-gmrgsales = 0
            s-imarg     = 0
            s-mmarg     = 0
            s-gmarg     = 0.
            
     /*      
     if (last-of(matrix.customer)) then do:
        if s-ytd-isales <> 0 then
           assign s-ytd-imarg  = (s-ytd-imrgsales / s-ytd-isales) * 100.
        if s-ytd-msales <> 0 then
           assign s-ytd-mmarg  = (s-ytd-mmrgsales / s-ytd-msales) * 100.
        if s-ytd-gsales <> 0 then
           assign s-ytd-gmarg  = (s-ytd-gmrgsales / s-ytd-gsales) * 100.
         
        if p-format = 1 then do:     
           if p-gnlmtrx = no then do:
              display s-ytd-isales
                      s-ytd-imarg
                      s-ytd-mmarg
              with frame f-1-subtot.
              down with frame f-1-subtot.
           end.
           else do:
              display s-ytd-isales
                      s-ytd-imarg
                      s-ytd-mmarg
                      s-ytd-gmarg
              with frame f-1g-subtot.
              down with frame f-1g-subtot.
           end.
        end.
        if p-format = 2 then do:     
           if p-gnlmtrx = no then do:
              display s-ytd-isales
                      s-ytd-imarg
                      s-ytd-mmarg
              with frame f-2-subtot.
              down with frame f-2-subtot.
           end.
           else do:
              display s-ytd-isales
                      s-ytd-imarg
                      s-ytd-mmarg
                      s-ytd-gmarg
              with frame f-2g-subtot.
              down with frame f-2g-subtot.
           end.
        end.

          
/* Accumlate District Totals */
         assign s-ytd-tisales    = s-ytd-tisales    + s-ytd-isales
                s-ytd-timrgsales = s-ytd-timrgsales + s-ytd-imrgsales
         
                s-ytd-tmsales    = s-ytd-tmsales    + s-ytd-msales
                s-ytd-tmmrgsales = s-ytd-tmmrgsales + s-ytd-mmrgsales
                
                s-ytd-tgsales    = s-ytd-tgsales    + s-ytd-gsales
                s-ytd-tgmrgsales = s-ytd-tgmrgsales + s-ytd-gmrgsales
         
         
/* Accumlate Regional Totals */                                    
                r-ytd-tisales    = r-ytd-tisales    + s-ytd-isales
                r-ytd-timrgsales = r-ytd-timrgsales + s-ytd-imrgsales
         
                r-ytd-tmsales    = r-ytd-tmsales    + s-ytd-msales
                r-ytd-tmmrgsales = r-ytd-tmmrgsales + s-ytd-mmrgsales
                
                r-ytd-tgsales    = r-ytd-tgsales    + s-ytd-gsales
                r-ytd-tgmrgsales = r-ytd-tgmrgsales + s-ytd-gmrgsales
            
/* Accumulate Grand Totals */                                      
                 g-ytd-tisales    = g-ytd-tisales    + s-ytd-isales
                 g-ytd-timrgsales = g-ytd-timrgsales + s-ytd-imrgsales
          
                 g-ytd-tmsales    = g-ytd-tmsales    + s-ytd-msales
                 g-ytd-tmmrgsales = g-ytd-tmmrgsales + s-ytd-mmrgsales
                 
                 g-ytd-tgsales    = g-ytd-tgsales    + s-ytd-gsales
                 g-ytd-tgmrgsales = g-ytd-tgmrgsales + s-ytd-gmrgsales.
            

         
         assign s-ytd-isales    = 0
                s-ytd-msales    = 0
                s-ytd-gsales    = 0
                s-ytd-mmarg     = 0
                s-ytd-imarg     = 0
                s-ytd-gsales    = 0
                s-ytd-mmrgsales = 0
                s-ytd-imrgsales = 0
                s-ytd-gmrgsales = 0.
     end. /* if last of customer */
     */
     /* 
     if (last-of(matrix.district)) then do:
          if s-ytd-tisales <> 0 then
             assign s-ytd-timarg  = (s-ytd-timrgsales / s-ytd-tisales) * 100.
          if s-ytd-tmsales <> 0 then
             assign s-ytd-tmmarg  = (s-ytd-tmmrgsales / s-ytd-tmsales) * 100.
          if s-ytd-tgsales <> 0 then
             assign s-ytd-tgmarg  = (s-ytd-tgmrgsales / s-ytd-tgsales) * 100.

          if p-format = 1 then do:     
             if p-gnlmtrx = no then do:
                display s-ytd-tisales
                        s-ytd-timarg
                        s-ytd-tmmarg
                with frame f-1-district.
                down with frame f-1-district.
             end.
             else do:
             display s-ytd-tisales       
                     s-ytd-timarg       
                     s-ytd-tmmarg       
                     s-ytd-tgmarg
             with frame f-1g-district.     
             down with frame f-1g-district.
             end.
          end.
          if p-format = 2 then do:     
             if p-gnlmtrx = no then do:
                display s-ytd-tisales
                        s-ytd-timarg
                        s-ytd-tmmarg
                with frame f-2-district.
                down with frame f-2-district.
             end.
             else do:
             display s-ytd-tisales       
                     s-ytd-timarg       
                     s-ytd-tmmarg       
                     s-ytd-tgmarg
             with frame f-2g-district.     
             down with frame f-2g-district.
             end.
          end.

                                                   
         assign s-ytd-tisales      = 0 
                s-ytd-tmsales      = 0
                s-ytd-tgsales      = 0
                s-ytd-timarg       = 0 
                s-ytd-tmmarg       = 0
                s-ytd-tgmarg       = 0
                s-ytd-timrgsales   = 0
                s-ytd-tmmrgsales   = 0
                s-ytd-tgmrgsales   = 0.
          page.       
                                                  
      end. /* if last of district */
      if (last-of(matrix.region)) then do:
          if r-ytd-tisales <> 0 then                                         
             assign r-ytd-timarg  = (r-ytd-timrgsales / r-ytd-tisales) * 100.
          if r-ytd-tmsales <> 0 then                                         
             assign r-ytd-tmmarg  = (r-ytd-tmmrgsales / r-ytd-tmsales) * 100.
          if r-ytd-tgsales <> 0 then
             assign r-ytd-tgmarg  = (r-ytd-tgmrgsales / r-ytd-tgsales) * 100.
             
          /*
          if matrix.region = "M" then
             assign r-rgn = "MOBIL".
          if matrix.region = "F" then
             assign r-rgn = "FILT ".
          if matrix.region = "N" then
             assign r-rgn = "NORTH".
          if matrix.region = "S" then
             assign r-rgn = "SOUTH".
          if matrix.region = "P" then
             assign r-rgn = "PABCO".
          if matrix.region = "R" then
             assign r-rgn = "SRVC ".
          */
             
          if p-format = 1 then do:     
             if p-gnlmtrx = no then do:
                display  r-ytd-tisales
                         r-ytd-timarg
                         r-ytd-tmmarg
                         r-ytd-tgmarg
                with frame f-1-region.
                down with frame f-1-region.
             end.
             else do:
                display r-ytd-tisales     
                        r-ytd-timarg      
                        r-ytd-tmmarg      
                        r-ytd-tgmarg      
                with frame f-1g-region.     
                down with frame f-1g-region.
             end.
          end.
          if p-format = 2 then do:     
             if p-gnlmtrx = no then do:
                display  r-ytd-tisales
                         r-ytd-timarg
                         r-ytd-tmmarg
                         r-ytd-tgmarg
                with frame f-2-region.
                down with frame f-2-region.
             end.
             else do:
                display r-ytd-tisales     
                        r-ytd-timarg      
                        r-ytd-tmmarg      
                        r-ytd-tgmarg      
                with frame f-2g-region.     
                down with frame f-2g-region.
             end.
          end.                                  
             

          assign r-ytd-tisales      = 0 
                 r-ytd-tmsales      = 0 
                 r-ytd-tgsales      = 0
                 r-ytd-timarg       = 0 
                 r-ytd-tmmarg       = 0
                 r-ytd-tgmarg       = 0
                 r-ytd-timrgsales   = 0 
                 r-ytd-tmmrgsales   = 0
                 r-ytd-tgmrgsales   = 0.
         page.
      end. /* if last of region */
      */
  end.
   
   if g-ytd-tisales <> 0 then                                             
      assign g-ytd-timarg  = (g-ytd-timrgsales / g-ytd-tisales) * 100.    
   if g-ytd-tmsales <> 0 then                                             
      assign g-ytd-tmmarg  = (g-ytd-tmmrgsales / g-ytd-tmsales) * 100.    
   if g-ytd-tgsales <> 0 then
      assign g-ytd-tgmarg  = (g-ytd-tgmrgsales / g-ytd-tgsales) * 100.


   page.

   if p-format = 1 then do:     
      if p-gnlmtrx = no then do:
         display g-ytd-tisales
                 g-ytd-timarg
                 g-ytd-tmmarg
         with frame f-1-grand.
      end.
      else do:
         display g-ytd-tisales      
                 g-ytd-timarg       
                 g-ytd-tmmarg       
                 g-ytd-tgmarg       
         with frame f-1g-grand.
      end.
   end.
   if p-format = 2 then do:     
      if p-gnlmtrx = no then do:
         display g-ytd-tisales
                 g-ytd-timarg
                 g-ytd-tmmarg
         with frame f-2-grand.
      end.
      else do:
         display g-ytd-tisales      
                 g-ytd-timarg       
                 g-ytd-tmmarg       
                 g-ytd-tgmarg       
         with frame f-2g-grand.
      end.
   end.

end. /* procedure */                                




