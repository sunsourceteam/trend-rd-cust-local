
{g-all.i}
{g-ic.i}
{g-oe.i}
{g-kp.i}
{g-wt.i}
{g-po.i}
{va.gva}

if g-currproc <> "icizp" then
  hide all no-pause.

def var        v-type      as c format "x" no-undo init "A".
def var        v-fut       as de           no-undo.
def var        hivend      as decimal init 999999999999 no-undo.
def var        v-myframe   as character format "x(5)" no-undo.
def var        a-desc      as character format "x(30)" no-undo.
def var        a-qty       as integer   format ">>>>9-" no-undo.
def var        a-min       as integer   format ">>>>9-" no-undo.
def var        a-max       as integer   format ">>>>9-" no-undo.
def var        z-maxreturn as integer   no-undo.
def var        v-choice    as c format "x" no-undo.  
def var        v-descripM  as c no-undo.
def var        v-prodcatM  as c no-undo.
def var        x-lookup    as char no-undo.
def var        x-lookup2   as char no-undo.

def var v-whse like icsw.whse no-undo.          
def var v-prod like icsw.prod no-undo.          
def var v-custno like arsc.custno no-undo.      
def var v-descrip like icsp.descrip[1] no-undo.
def var v-QryDesc as char              no-undo.
def var v-entries as int               no-undo.
def var v-inx     as int               no-undo.



def var v-prodcat like icsp.prodcat no-undo.
def var v-vendno  like icsw.arpvendno no-undo.
def var v-row as int no-undo.                   
def var v-down as int no-undo.                  
def var out-whse like icsw.whse no-undo.        
def var out-prod like icsw.prod no-undo.        
def var out-type as char  no-undo.              
def var out-qty as dec  no-undo.                
def var out-unit as char  no-undo.              
                                                

form
  "Search Type:" at 8
  v-choice       at 25  no-label
  help " (P)roduct, (D)escription (C)ategory (V)endor" 
  "Whse (optional):"  at 30
  v-whse              at 46 no-label
  
  
  with frame f-type no-underline width 80 row 1 overlay  title g-title.        

form
  " " at 1
  " " at 1
  " " at 1
  " " at 1
  " " at 1
  " " at 1
  " " at 1
  " " at 1
  " " at 1
  " " at 1
  " " at 1
  " " at 1
  " " at 1
  " " at 1
  " " at 1
  " " at 1
  " " at 1
  " " at 1
  " " at 1
  " " at 1
  " " at 1
  " " at 1
with frame f-blank width 80 row 1 overlay no-box.

form
/*  "Search Type:" at 8
  v-choice       at 25  no-label  */
  "Product"      at 8
  "(A - Current Availability" at 44
  v-prod   at 8   no-label
   /*  validate({x-icsp.i "input v-prod"},
                 "No Products begin with the entered beginning")   */
  " F - Future Availability"  at 44 
  "Description"  at 8
  v-type   at 40 no-label 
  " O - OnHand Availability)"  at 44 
  v-descrip at 8  no-label
   /*  validate({x-icsp2.i "input v-descrip"},
                "No Products begin with the entered description")    */
  "Category"     at 8              
  v-prodcat at 8  no-label
   /*  validate({x-icsp3.i "input v-prodcat"},
                "No Products begin with the entered Category")    */
                 
  "Vendor"       at 8
  v-vendno       at 8   no-label
    /*  validate({x-icsp4.i "input v-vendno"},
                "No Products begin with the entered vendor")    */
                
  with frame f-update no-underline width 78 /* 80  */ row 4 overlay.  
  /*  title g-title.  */


main:

do while true on endkey undo main, next main:
  assign v-prod = ""
         v-descrip = ""
         v-prodcat = ""
         v-vendno  = 0.

/*  assign v-whse = g-whse.  */

  {p-setup.i f-type}

   put screen row 21 col 3 
"                                                                             ".
   put screen row 22 col 3 
"                                                                             ".
   put screen row 23 col 3                                              
"                                                                             ".

   /***
   put screen row 24 col 3 
"                                                                             ".
   put screen row 25 col 3 
"                                                                             ".
   put screen row 26 col 3 
"                                                                             ".
   put screen row 27 col 3 
"                                                                             ".
   put screen row 28 col 3 
"                                                                             ".
   put screen row 29 col 3 
"                                                                             ".
   ***/
  view frame f-blank.
  display v-choice 
          v-whse with frame f-type. 
  update v-choice 
         v-whse with frame f-type
  editing:
    readkey.
    if {k-cancel.i} or {k-jump.i} then do:
  
      leave main.
    end.
    apply lastkey.
  end.  

/*   assign g-whse = v-whse.  */
/*   hide frame f-blank.    smaf */


  main2:

  do while true on endkey undo main2, next main2:
  view frame f-blank.  /* smaf  */
  if g-currproc <> "icizp" then
    view frame f-type.
  put screen row 21 col 3 
 "                                                                            ".
    
    {p-setup.i f-update}   
    display v-prod v-descrip v-prodcat v-vendno v-type with frame f-update.  
 
    update 
     /* v-choice    */
      v-prod     when v-choice = "p"      
      v-descrip  when v-choice = "d"  
      v-prodcat  when v-choice = "c"
      v-vendno   when v-choice = "v"
      v-type 
    with frame f-update

    editing:
      readkey.
    
      if {k-cancel.i} or {k-jump.i} then do:

        on cursor-up back-tab.
        on cursor-down tab.
        hide frame f-update.

        put screen row 21 col 3 
   "                                                                  ".
       next main.
      end.
      else  
      if {k-after.i} then do:
        if frame-field = "v-prod" then do:
          next-prompt  v-prod.
          if {x-icsp.i "input v-prod"} and input v-prod <> ""  then
            next-prompt v-prod.
          else do:
/*          run err.p (4600).   */
            next-prompt  v-prod.
            apply lastkey.
            next.
          end. 
        end.
        ELSE 
        if frame-field = v-descrip then do:
          next-prompt v-descrip.
          if {x-icsp2.i "input v-descrip"} and input v-descrip <> "" then
            next-prompt v-descrip.
          else do:
            next-prompt v-descrip.
            apply lastkey.
            next.
          end. /* else do */
         
        end.  /* frame-field v-descrip */
      ELSE 
      if frame-field = v-prodcat then do:
        next-prompt v-prodcat.
         
        if {x-icsp3.i "input v-prodcat"} and input v-prodcat <> "" then
          next-prompt v-prodcat.
        else do:
          next-prompt v-prodcat.
          apply lastkey.
          next.
        end. /* else do */
      end.  /* frame-field v-prodcat */
          
     /*  
      ELSE 
      if frame-field = v-vendno then do:
         next-prompt v-vendno.
     if {x-icsw4.i "input v-vendno"} and input v-vendno <> 0 then   
            next-prompt v-vendno.
         else do:
            next-prompt v-vendno.
            apply lastkey.
            next.
         end. /* else do */
         
      end.  /* frame-field v-vendno */
          
     */   
          
      if frame-field = "v-type" then do:
        next-prompt  v-type.
      end.   
    end.
    apply lastkey.
  end.  
 
 hide message no-pause.

 if g-currproc <> "icizp" then
   view frame f-type.
 run icizp-brwse.p (input v-type,
                    input v-choice,
                    input v-prod,
                    input v-descrip, 
                    input v-prodcat, 
                    input v-vendno,
                    input v-whse).    

 on cursor-up back-tab.
 on cursor-down tab.
 hide frame f-update.
 hide frame f-blank. 



 {j-all.i "frame f-update"}.
 END.
end.

procedure titlemorph:

find sassm where sassm.currproc = v-myframe no-lock no-error.
if avail sassm then
  g-title = sassm.frametitle.
end.  


