/***************************************************************************
 SX 55
 PROGRAM NAME: oimzl.p loosely based on oeet95sdi.p
  PROJECT NO.: 
  DESCRIPTION:  This program is fired up from oeet99.p.  
  DATE WRITTEN: 03/28/2005.
  AUTHOR      : SunSource. dkt  
  MODIFIED: 
***************************************************************************/
{g-all.i}
{g-print.i}
{g-oe.i}
{g-ar.i}
{g-ic.i}
{g-rptctl.i "new"}

/* v-type should be assigning what types of files I am sending-
   invoice, order, etc.
   Then flags matching what is in oeet90.p can be set 
    
   POs are from poet90sdi.p. 
   Oeet90sdi.p/oeet+F10 gives v-types of az or in
   oeet.p has v-type zz
*/
   

def var o-blprt as logical no-undo.

define input parameter v-type as char.
def shared var v-idoeeh    as   recid         no-undo.
def shared var v-oerptfrmt as   i             no-undo.
def shared var v-oelinefl  like sasc.oelinefl no-undo.

def var v-ackokfl      as logical            no-undo.
def var v-rtrnfl       as logical            no-undo.
def var s-ackprintfl   as logical            no-undo.
def var s-printernmack as character format "x(10)" no-undo.
def var s-printernminv as character format "x(10)" no-undo.   /*dkt*/
def var v-faxphoneno  like sapb.faxphoneno   no-undo.
def var v-faxto1      like sapb.faxto1       no-undo.
def var v-faxto2      like sapb.faxto2       no-undo.
def var v-faxpriority as character format "x(10)"           no-undo.
def var v-faxfrom     like sapb.faxfrom      no-undo.
def var v-faxcom      like sapb.faxcom       no-undo.
def var v-emailaddr   as character format "x(50)" extent  3 no-undo.
def var sdi-faxcom    as character format "x(75)" extent 10 no-undo.
def var v-emailaddrgp as character format "x(150)"          no-undo.

def var exitFlag      as logical             no-undo.

def var letterType as character format "x(2)" no-undo. /*dkt*/
def var letterCode as character format "x(12)" no-undo.
def var retainFlag as logical label "Retain Copy" no-undo.
def var v-blprt as logical label "BL" no-undo.
def var v-crdfrm as logical label "CA" no-undo.
def var dontapply  as logical no-undo.
def var cancelFlag as logical no-undo.
def var runFlag as logical no-undo.
def var errMessage as character format "x(60)" no-undo.
def var sendType as character format "x(10)" no-undo.
def var s-invprintfl as logical no-undo.
def var o-faxphoneno as character no-undo.
def var o-faxto2 as character no-undo.
def var o-emailaddr as character format "x(50)" extent 3 no-undo.
def var v-extent as integer no-undo.
/* rs; VSIFAX problem */
/* {valmsg.gas &string = "4035,4052"} */
def var lastgorderno as integer no-undo.


/* lpr variables */
def var v-pick as c no-undo.
def var s-printer as c no-undo.
def buffer bm-oeeh for oeeh.
def buffer bm-arss for arss.
define buffer b-sapb for sapb.
define var cReportPrefix as c no-undo.
def var v-custno like oeeh.custno no-undo.
def var v-shipto like oeeh.shipto no-undo.
def var v-emailaddrlt as c format "x(150)" no-undo.
def var s-rcptprintfl as l no-undo.
def shared var v-oercptfrmt as i no-undo.
def var s-promomsg like sapb.rpttitle no-undo.
def var v-contact as char initial "" no-undo.

form
    g-orderno      colon 9       {f-help.i}
    "-"            at 19                      /* SX 55 */
    g-ordersuf     at 20 no-label             /* SX 55 */
    {f-help.i}
    /* reserve space for g-custno dkt */
    g-custno       at 40 
    sendType       colon 10 label "Form Type"
       help "A)cknowledge Order, I)nvoice"
    /* s-ackprintfl   colon 5 label "Ack" */
    s-printernmack colon 30 label "Printer"
       help "Enter 'Fax' to fax, 'E-mail' to email, or F12 to find a printer"
    letterCode     colon 50 label "Letter"
       help "Hit F12 for lookup                                         [L]"
    v-faxphoneno   colon 10 label "To Fax #"
    /*  validate(v-faxphoneno ne "","This is a Required Field (2100)") */
       help "Enter 'L' in First Position for Local# Requiring Area Code"
    v-faxpriority           label " Priority"
       help "Normal, Low, High, Urgent"
    retainFlag     colon 63
       help "Yes will send an email copy to you" 
    /*  validate("Low","High","Normal","Urgent") */
    v-emailaddr[1]    colon 7 label "Email"
       help "Email addresses separated by ;s"
    v-emailaddr[2]    at 9 no-label  
       help "Email addresses separated by ;s"
    v-emailaddr[3]    at 9 no-label  
       help "Email addresses separated by ;s"
    v-blprt        colon 63
       help "Yes will print BL information ;s"
    v-faxto2       colon 5 format "x(50)" label "To"
      /*  validate(v-faxto2 ne "",{valmsg.gme 4052}) */
        help "Individual Receiving the Fax (Used on Cover Sheet)"
    v-crdfrm       colon 63
       help "Yes will send Credit Application"
    v-faxto1       colon 5 format "x(50)" label "Co"
      /*  validate(v-faxto1 ne "",{valmsg.gme 4052}) */
              help "Company Receiving the Fax (Used on Cover Sheet)"
    v-faxfrom      colon 5 format "x(50)" label "From"
        help "Individual Sending the Fax (Used on Cover Sheet)"
    
    sdi-faxcom[1]       at 1 no-label
    sdi-faxcom[2]       at 1 no-label
    sdi-faxcom[3]       at 1 no-label
    sdi-faxcom[4]       at 1 no-label
    sdi-faxcom[5]       at 1 no-label
    sdi-faxcom[6]       at 1 no-label
    sdi-faxcom[7]       at 1 no-label
    sdi-faxcom[8]       at 1 no-label
    sdi-faxcom[9]       at 1 no-label
    sdi-faxcom[10]      at 1 no-label
with frame f-oimzl row 1 column 1 side-labels overlay width 78
title " Fax Information ".

form
    g-orderno      colon 9       {f-help.i}
    "-"            at 18
    g-ordersuf     at 19 no-label
    {f-help.i}
    /* reserve space for g-custno dkt */
    g-custno       at 40 
    sendType       colon 10 label "Form Type"
       help "A)cknowledge Order, I)nvoice"
    /* s-ackprintfl   colon 5 label "Ack" */
    s-printernmack colon 30 label "Printer"
       help "Enter 'Fax' to fax, 'E-mail' to email, or F12 to find a printer"
    letterCode     colon 50 label "Letter"
       help "Hit F12 for lookup                                         [L]"
    v-faxphoneno   colon 10 label "To Fax #"
    /*  validate(v-faxphoneno ne "","This is a Required Field (2100)") */
       help "Enter 'L' in First Position for Local# Requiring Area Code"
    v-faxpriority           label " Priority"
       help "Normal, Low, High, Urgent"
    retainFlag     colon 63
       help "Yes will send an email copy to you" 
    /*  validate("Low","High","Normal","Urgent") */
    v-emailaddr[1]    colon 7 label "Email"
       help "Email addresses separated by ;s"
    v-emailaddr[2]    at 9 no-label  
       help "Email addresses separated by ;s"
    v-emailaddr[3]    at 9 no-label  
       help "Email addresses separated by ;s"
    v-blprt       colon 63
       help "Yes will print BL information ;s"
    v-faxto2       colon 5 format "x(50)" label "To"
      /*  validate(v-faxto2 ne "",{valmsg.gme 4052}) */
        help "Individual Receiving the Fax (Used on Cover Sheet)"
    v-crdfrm       colon 63
       help "Yes will send Credit Application"
    v-faxto1       colon 5 format "x(50)" label "Co"
      /*  validate(v-faxto1 ne "",{valmsg.gme 4052}) */
        help "Company Receiving the Fax (Used on Cover Sheet)"
    v-faxfrom      colon 5 format "x(50)" label "From"
        help "Individual Sending the Fax (Used on Cover Sheet)"
    
    sdi-faxcom[1]       at 1 no-label
    sdi-faxcom[2]       at 1 no-label
    sdi-faxcom[3]       at 1 no-label
    sdi-faxcom[4]       at 1 no-label
    sdi-faxcom[5]       at 1 no-label
    sdi-faxcom[6]       at 1 no-label
    sdi-faxcom[7]       at 1 no-label
    sdi-faxcom[8]       at 1 no-label
    sdi-faxcom[9]       at 1 no-label
    sdi-faxcom[10]      at 1 no-label
with frame f-oimzl2 row 1 column 1 side-labels overlay width 78
title " Fax Information ".

/* get security */
/*  g-secure.   */

/* inserted help stuff */                                           
define temp-table tletters no-undo                                  
  field ttype      like cmsl.transproc /* dkt */
  field tlettercd  like cmsl.lettercd                             
  field tletter    like cmsl.user1 label "Letter"                 
  field tauthor    like cmsl.user2 label "Author"                 
  field tdesc      like cmsl.description                          
    index tk-ltr tlettercd tletter tauthor.                         
define query q-tlr for tletters scrolling.                          
define browse b-tlr query q-tlr                                     
  display tletters.tletter                                          
          tletters.tauthor                                          
          tletters.tdesc                                            
  with 6 down width 64 centered overlay no-hide no-labels           
  title "Letter   Author   Description                            ".
define frame f-tlr                                               
  b-tlr at row 2 col 1                                              
  with width 75 overlay no-hide no-labels no-box.                   

on any-key of b-tlr do:                             
  if keylabel(lastkey) = "F1" or                    
     keylabel(lastkey) = "Return"                   
  then do:                                      
    letterCode = tletters.tlettercd.              
    apply "window-close" to current-window.         
  end.                                              
  if {k-cancel.i} then do:                         
    apply "window-close" to current-window.        
    hide frame f-tlr.                              
  end.                                             
end.                                               
  
Procedure LookupLetterCode:
/* on f12 of letterCode do: */                                                
def var whichType as char no-undo.
if v-type = "" or v-type = "az" then whichType = "OE".
if v-type = "in" then whichType = "IN".               
  
for each cmsl where ( cmsl.user2 = "" or cmsl.user2 = g-operinits )   
  and cmsl.user1 ne "" and index(cmsl.lettercd,"-") > 1               
  and (cmsl.transproc = whichType OR cmsl.transproc = "")
break by cmsl.lettercd:                                            
/* dkt begin */
  if sendType = "Ack" then 
    if cmsl.transproc ne "OE" and cmsl.transproc ne "" then next.
  if sendType = "Inv" then
    if cmsl.transproc ne "IN" and cmsl.transproc ne "" then next.
  if sendType = "PO" then
    if cmsl.transproc ne "PO" and cmsl.transproc ne "" then next.
  find first tletters use-index tk-ltr where
    tletters.tlettercd = cmsl.lettercd and
    tletters.tletter   = cmsl.user1    and
    tletters.tauthor   = cmsl.user2    no-lock no-error.
  if not avail tletters then do:
    create tletters.                                                  
    assign tletters.tlettercd = cmsl.lettercd                              
           tletters.ttype     = cmsl.transproc
           tletters.tletter   = cmsl.user1                                 
           tletters.tauthor   = cmsl.user2                                 
           tletters.tdesc     = cmsl.description + "|" + cmsl.transproc.           end.         
end.                                                                  
open query q-tlr preselect each tletters use-index tk-ltr.            
enable  b-tlr with frame f-tlr.                                       
apply "entry" to b-tlr.  
display b-tlr with frame f-tlr.                                       
on cursor-up cursor-up.                          
on cursor-down cursor-down.                      
wait-for window-close of current-window.         
hide frame f-tlr.                                
  
on cursor-up back-tab.
on cursor-down tab.
find first cmsl use-index k-cmsl                 
  where cmsl.lettercd = tletters.tlettercd and
  cmsl.user1 ne "" and index(cmsl.lettercd,"-") > 1               
  no-lock no-error.                              
  
if avail cmsl then do:
  letterCode = cmsl.user1.
  do i = 1 to 10:
    sdi-faxcom[i] = cmsl.noteln[i].
  end.
  release cmsl.
  /* display letterCode sdi-faxcom with frame f-oimzl. 
  next-prompt v-faxphoneno.
  next.   */
end.
/* should make lastkey = null */
/*  readkey pause 0.  */
on cursor-up back-tab.                           
on cursor-down tab.                              
close query q-tlr.                               
/* next-prompt v-faxphoneno. */
/* return.  */
end.                                                

Procedure InitStuff:
def var tempfaxphoneno as character no-undo.
def var tempfaxto2     as character no-undo.
/* s-ackprintfl = yes. */
/* errMessage = "". */
assign
  s-invprintfl    = g-invprintfl
  s-ackprintfl    = g-ackprintfl
  s-promomsg      = g-promomsg.
if s-printernmack = "" then s-printernmack = g-printernmack.
if s-printernmack = "" then s-printernmack = g-printernminv. 
/* if sendType = "" or sendType = "Ack" then do:  */
if v-type = "" or v-type = "az" or v-type = "zz" then sendType = "Ack".
if v-type = "in" then sendType = "Inv".
if v-type = "po" then sendType = "PO".

if sendType = "Ack" then do:                       
  s-ackprintfl = yes.                     
  s-invprintfl = no.                      
  v-extent = 2.                           
end.                      
if sendType = "Inv" then do:
  s-ackprintfl = no.
  s-invprintfl = yes.
  v-extent = 3.
end.
if sendType = "PO" then do:
  s-ackprintfl = yes.
  s-invprintfl = no.
  v-extent = 2.
end.

if g-orderno > 0 then do:
  find first bm-oeeh use-index k-oeeh where
   bm-oeeh.cono = g-cono and bm-oeeh.orderno = g-orderno and
   bm-oeeh.ordersuf = g-ordersuf no-lock no-error.
  if avail bm-oeeh then do:
    run zsdicontact.p(input "1", input g-cono, input bm-oeeh.custno,
      input bm-oeeh.shipto, input-output v-contact,
      input-output tempfaxto2, input-output tempfaxphoneno,
      input-output v-emailaddrgp).
    g-custno = bm-oeeh.custno.
    if lastgorderno = g-orderno then do:
      if v-faxto2 = "" then v-faxto2 = v-contact. /* tempfaxto2. */
      if v-faxphoneno = "" then v-faxphoneno = tempfaxphoneno.
      if v-emailaddr[1] = "" then v-emailaddr[1] = v-emailaddrgp.
      if v-faxphoneno ne "" then do:
        assign
          o-faxphoneno = v-faxphoneno
          o-faxto2     = v-faxto2
          o-emailaddr  = v-emailaddr[1] + v-emailaddr[2] + v-emailaddr[3].
      end.
    end.
    else do:
      v-faxto1 = "".
      v-faxfrom = "".
      /* changed order number */
      v-faxto2 = v-contact. /* tempfaxto2. */                        
      v-faxphoneno = tempfaxphoneno.           
      v-emailaddr[1] = v-emailaddrgp.        
      assign                                                           
        o-faxphoneno = v-faxphoneno                                      
        o-faxto2     = v-faxto2                                          
        o-emailaddr  = v-emailaddr[1] + v-emailaddr[2] + v-emailaddr[3].
    end.
    if bm-oeeh.shipto ne "" then
      {w-arss.i bm-oeeh.custno bm-oeeh.shipto no-lock " " "bm-"}
    if bm-oeeh.shipto ne "" and avail bm-arss and
      (s-ackprintfl and bm-arss.eackto = false) then do:
        {p-fxsdif.i &file = "bm-arss."
                    &extent = v-extent }
        find first bm-arss where bm-arss.cono = g-cono and
          bm-arss.custno = g-custno no-lock no-error.
        if bm-arss.eacktype = "f" then  
          s-printernmack = "fax".
        else
        if bm-arss.eacktype = "m" then
          s-printernmack = "e-mail".
    end.   
    else do:
      /* find first arsc where arsc.cono = g-cono and arsc.custno = g-custno    
         no-lock no-error. */
      {p-fxsdif.i &file = "arsc."       
                  &extent = v-extent }
      if arsc.eacktype = "f" then       
        if s-printernmack = "" then s-printernmack = "fax".         
      else                              
        if arsc.eacktype = "m" then       
          if s-printernmack = "" then s-printernmack = "e-mail".      
     /*  release arsc. */ 
    end.
    release bm-arss.   
  end. /* end oeeh */
  release bm-oeeh.
  lastgorderno = g-orderno.
end.      
end procedure.

errMessage = "".                                 
run ClearDisplay.                     
main:
do while not exitFlag with frame f-oizml on endkey undo main, leave main:
  /* zsdicontact fills default info for customer contact,
     updates when order number updates */
     
  /* only pop the screen up from oe if its "az" or they have security */    
  {w-sasos.i g-operinits "'zxt1'" no-lock}
  if avail sasos and sasos.securcd[12] > 1 or 
     can-do("az,in",v-type) then /* v-type = "az" then */
     assign v-type = v-type.
  else
    return.

  v-faxpriority = "Normal".
  run InitStuff.
  run DisplayInfo.
  do while not exitflag:
    run UpdateInfo.
  end.
  if exitFlag then do:
    if cancelFlag then do:
      hide frame f-oimzl.
      leave.
    end.
        run ErrorCheck.
    if runFlag then do:
      /* message "Sending the" sendType. */
      run RunPrint.
      if runFlag then do:
        g-ourproc  = "oeet".
        g-currproc = "oeet".

        
        
        /* Should find the time to rewrite someday */
        {zsdioeetg.lpr  &runtype        = "v-type"
                        &runpick        = "v-pick"
                        &orderno        = "g-orderno"
                        &ordersuf       = "g-ordersuf"
                        &report-prefix  = cReportPrefix
                        &whseprinter    = "s-printer"
                        &pickprintfl    = "g-pickprintfl"
                        &printernmpck   = "g-printernmpck"
                        &invprintfl     = "g-invprintfl"
                        &printernminv   = "g-printernminv"
                        &promomsg       = "g-promomsg"
                        &custno         = "v-custno"
                        &shipto         = "v-shipto"
                        &ackprintfl     = "s-ackprintfl"
                        &printernmack   = "s-printernmack"
                        &rcptprintfl    = "s-rcptprintfl"
                    &invsapbassigns = "if g-printernminv  = 'E-MAIL':u then
                        assign sapb.inusecd    = "'n'"
                               sapb.backfl     = yes              
                               sapb.demandfl   = yes              
                               sapb.filefl     = yes              
                               sapb.delfl      = yes              
                               sapb.faxphoneno = v-emailaddrlt    
                               sapb.faxcom[1]  = sdi-faxcom[1]    
                               sapb.faxcom[2]  = sdi-faxcom[2]    
                               sapb.faxcom[3]  = sdi-faxcom[3]    
                               sapb.faxcom[4]  = sdi-faxcom[4]    
                               sapb.faxcom[5]  = sdi-faxcom[5]    
                               sapb.faxcom[6]  = sdi-faxcom[6]    
                               sapb.faxcom[7]  = sdi-faxcom[7]    
                               sapb.faxcom[8]  = sdi-faxcom[8]    
                               sapb.faxcom[9]  = sdi-faxcom[9]    
                               sapb.faxcom[10] = sdi-faxcom[10]   
                               sapb.user2      = string(retainFlag)
                               sapb.user3      = string(v-crdfrm)
                               sapb.optvalue[11]  = string(v-blprt)
                               sapb.optvalue[8] = "'no'"."
                     &acksapbassigns = "if g-printernmack = 'E-MAIL':u then
                        assign sapb.inusecd    = "'n'"
                               sapb.backfl     = yes            
                               sapb.demandfl   = yes            
                               sapb.filefl     = yes            
                               sapb.delfl      = yes            
                               sapb.faxphoneno = v-emailaddrlt  
                               sapb.faxcom[1]  = sdi-faxcom[1]  
                               sapb.faxcom[2]  = sdi-faxcom[2]  
                               sapb.faxcom[3]  = sdi-faxcom[3]  
                               sapb.faxcom[4]  = sdi-faxcom[4]  
                               sapb.faxcom[5]  = sdi-faxcom[5]  
                               sapb.faxcom[6]  = sdi-faxcom[6]  
                               sapb.faxcom[7]  = sdi-faxcom[7]  
                               sapb.faxcom[8]  = sdi-faxcom[8]  
                               sapb.faxcom[9]  = sdi-faxcom[9]  
                               sapb.faxcom[10] = sdi-faxcom[10] 
                               sapb.user2      = string(retainFlag)
                               sapb.user3      = string(v-crdfrm)
                               sapb.optvalue[11] = string(v-blprt)
                               sapb.optvalue[8] = "'no'".
                         else if g-printernmack = 'FAX':u then 
                             assign sapb.optvalue[11] = string(v-blprt)
                                    sapb.user3 = string(v-crdfrm).
                         else
                           assign sapb.optvalue[11] = sapb.optvalue[11]
                                  sapb.user3 = string(v-crdfrm)."}
                                       
       /* Shhh! I am an oeet process!
          g-currproc = "oimzl". */
       /*
       assign g-ackprintfl = no     
              s-ackprintfl = no     
              g-invprintfl = no
              s-invprintfl = no
              s-printernmack = ""   
              g-printernmack = ""   
              s-promomsg = ""       
              g-promomsg = "". 
       */
        run ClearDisplay.
        run DisplayInfo.
      end.
    end.
    /*
    runFlag = no.
    exitFlag = no.
    */
  end.
  hide frame f-oimzl.   
end.

Procedure RunPrint:
  if g-orderno > 0 then do:
    {w-oeeh.i g-orderno g-ordersuf no-lock "bm-"}
    if not avail bm-oeeh then do:
      runFlag = no.
      run err.p(4605).
      next-prompt g-orderno.
      leave. /* next */
    end.
    if bm-oeeh.openinit ne "" then do:
      runFlag = no.
      message "Order " g-orderno " in use by " bm-oeeh.openinit + "(5608)".
      bell.
      next-prompt g-orderno.
      leave. /* next. */
    end.
  end.
  /* Set globals for printing */
  if s-printernmack = "email" then s-printernmack = "E-MAIL".
  /* if s-printernminv = "email" then s-printernminv = "E-MAIL". */
  assign g-ackprintfl   = s-ackprintfl
         g-invprintfl   = s-invprintfl
         g-printernmack = s-printernmack.
  if g-invprintfl then g-printernminv = g-printernmack.       
  /*       g-promomsg     = s-promomsg.   */
  if g-orderno = 0 then do:
    hide frame f-oimzl no-pause.
    message "This function currently requires an order number".
    return.
  end.
  hide frame f-oimzl no-pause.  /* star */
  
  if g-printernmack = 'E-MAIL':u or g-printernminv = 'E-MAIL':u then
    cReportPrefix = "oeet@":u.
  else
    cReportPrefix = "".
    
 /* already seems to add ;s */
 assign v-emailaddrlt = "XM:" + v-emailaddr[1] + 
                                v-emailaddr[2] + 
                                v-emailaddr[3]
        v-faxcom[1] = sdi-faxcom[1]
        v-faxcom[2] = sdi-faxcom[2]
        v-faxcom[3] = sdi-faxcom[3]
        v-faxcom[4] = sdi-faxcom[4]
        v-faxcom[5] = sdi-faxcom[5]
        v-faxcom[6] = sdi-faxcom[6]
        v-faxcom[7] = sdi-faxcom[7]
        v-faxcom[8] = sdi-faxcom[8]
        v-faxcom[9] = sdi-faxcom[9]
        v-faxcom[10] = sdi-faxcom[10].
end.
/*  leave. */

Procedure ErrorCheck:
  errMessage = "".
  runFlag = no.
  exitFlag = no.
  if g-orderno = 0 or g-orderno = ? then do:
    errMessage = "You must have a valid order number!".
    next-prompt g-orderno with frame f-oimzl.
    leave.
  end.
  if s-printernmack = "" then do:
     errMessage = "Enter a printer name or cancel".
     next-prompt s-printernmack with frame f-oimzl.
     leave.
  end.
  else                      
  if s-printernmack = "fax" then do:
     assign v-blprt = input v-blprt.
     if v-faxfrom = "" then do:                                
       errMessage = "Please enter who is sending the message".  
       next-prompt v-faxfrom with frame f-oimzl.     
       leave.                                                                       end.      
     if v-faxphoneno = "" then do:                             
       errMessage = "must have a fax number to fax to".                               next-prompt v-faxphoneno with frame f-oimzl.                
       leave.
     end.                                                                
     if v-faxto1 = "" then do:                                   
       errMessage = "Must have a Person to fax to".         
       next-prompt v-faxto1 with frame f-oimzl.
       leave.
     end.  
     if v-faxto2 = "" then do:     
       errMessage = "Please have a Company to fax to".       
       next-prompt v-faxto2 with frame f-oimzl.  
       leave.                              
     end.                                                     
  end. /* if fax */                                            
  else
  if s-printernmack = "email" or s-printernmack = "e-mail" then do:
    if index(v-emailaddr[1],"@") <= 0 then do:  
      errMessage = "Please enter a valid email address".                
      next-prompt v-emailaddr[1] with frame f-oimzl.      
      next.                                      
    end.
  end.                       
  else do:                    
    find first sasp use-index k-sasp where sasp.printernm =     
      s-printernmack no-lock no-error.                          
    if not avail sasp then do:                                  
      errMessage = "Invalid Printer: " + s-printernmack.               
      next-prompt s-printernmack.                               
      leave.
    end.                                                     
    release sasp.                                               
  end.
  exitFlag = yes.
  runFlag = yes.
end procedure.

Procedure UpdateInfo:

  message errMessage.
  update g-orderno
         g-ordersuf
         /* sendType      */
         /* s-ackprintfl  */ 
         s-printernmack 
         letterCode
         v-faxphoneno       when g-secure = 5 and s-printernmack = "fax"
         /*  v-faxpriority  */
         retainFlag         /* when s-printernmack = "e-mail" */
         v-emailaddr[1]     /* when s-printernmack = "e-mail" */
         v-emailaddr[2]     /* when s-printernmack = "e-mail" */  
         v-emailaddr[3]     /* when s-printernmack = "e-mail" */  
         v-blprt            /* when s-printernmack = "e-mail" */
         v-faxto2           when /* g-secure = 5 and */ s-printernmack = "fax"
         v-crdfrm           /* when substring(arsc.user10,1,1) = "y" */
         v-faxto1
         v-faxfrom
         text (sdi-faxcom)
 /* go-on(f9) */ with frame f-oimzl
    editing:
      readkey.
      /* Item  = input Item. */
      v-rtrnfl = no.
      assign v-blprt = v-blprt.
        if {k-func12.i} and frame-field = "letterCode"
        then do:
        Run LookupLetterCode.
        Run DisplayInfo.
        /*
        message "after display". pause 3.
        dontapply = yes.     */
        if g-secure = 5 and s-printernmack = "fax" then 
          next-prompt v-faxphoneno with frame f-oimzl.  
          else if s-printernmack = "e-mail" then
            next-prompt retainFlag with frame f-oimzl.    
            else next-prompt v-faxto1 with frame f-oimzl.   
            next.                                           
            
        /** next-prompt letterCode with frame f-oimzl. **/
        /* leave UpdateInfo. */
        /* return no-apply. */
      end.
      if {k-func12.i} and frame-field = "v-faxphoneno" then do:
        next-prompt v-faxphoneno with frame f-oimzl.
        return no-apply.  
      end.
      if {k-after.i} then do:
        /* check fields */
        if frame-field = "g-orderno" then
          g-orderno = input g-orderno. 
        if frame-field = "v-blprt" then
          v-blprt = input v-blprt.                       
        if frame-field = "v-crdfrm" then 
          v-crdfrm = input v-crdfrm.
        if frame-field = "g-ordersuf" then do:
          g-ordersuf = input g-ordersuf.      
          run InitStuff.
          run DisplayInfo.
        end.                     
        if frame-field = "sendType" then do:
          sendType = input sendType.
          if substring(sendType,1,1) = "a" then do:
            sendType = "Ack".
            s-ackprintfl = yes.
            s-invprintfl = no.
            v-extent = 2.
            message "".
          end.
          else
          if substring(sendType,1,1) = "i" then do:
            sendType = "Inv".
            s-ackprintfl = no.
            s-invprintfl = yes.
            v-extent = 3.
            message "".
          end.
          else do:
            message "Invalid Form Type. Please pick A)ck or I)nvoice".
            sendType = "".
            next-prompt sendType with frame f-oimzl.
            next.
          end.
          display sendType with frame f-oimzl.
        end.
        
        if frame-field = "s-printernmack" then do:
          s-printernmack = input s-printernmack.
          
          /* smaf  
          if {k-accept.i} or {k-return.i} then do:
           update 
           v-emailaddr[1]                
           v-emailaddr[2]                
           v-emailaddr[3].
          /*  with frame f-oimzl.                     
           next-prompt letterCode with frame f-oimzl.  */
          end.     */       
          
          if not (s-printernmack = "fax" or s-printernmack = "e-mail"
            or s-printernmack = "email") then do:
            find first sasp use-index k-sasp where sasp.printernm =
              s-printernmack no-lock no-error.
            if not avail sasp then do:
              message "Invalid Printer: " s-printernmack.
              next-prompt s-printernmack.
              next.
            end.
            else message "".
            release sasp.
          end.
          else if s-printernmack = "e-mail" then do:
          /**  
          update
              retainFlag                   
              v-emailaddr[1]                
              v-emailaddr[2]                
              v-emailaddr[3]                
              v-blprt
              v-crdfrm   /*  when substring(arsc.user10,1,1) = "n" */
            with frame f-oimzl.  **/
            next-prompt letterCode with frame f-oimzl.
            next.
          end.
          else if s-printernmack = "fax" /* and g-secure = 5 smaf */ then do:
           /** update
              v-faxphoneno
              v-faxto2
              v-blprt
              v-crdfrm   /* when substring(arsc.user10,1,1) = "y" */
            with frame f-oimzl.    **/
            next-prompt letterCode with frame f-oimzl.
            next.
          end.
          else message "".
        end.
        
       /*  if frame-field = "v-blprt" then do:
           v-blprt = input v-blprt.
        end. 
       */   
       
        if frame-field = "v-faxfrom" then do:
          v-faxfrom = input v-faxfrom. 
          if v-faxfrom = "" then do:
            message "Please enter who is sending the message".
            next-prompt v-faxfrom with frame f-oimzl.
            next.
          end.
          else
            message "".
        end.
        if frame-field = "v-faxpriority" then do:
          v-faxpriority = input v-faxpriority.
          if v-faxpriority = "l" then v-faxpriority = "Low".
          if v-faxpriority = "h" then v-faxpriority = "High".
          if v-faxpriority = "n" then v-faxpriority = "Normal".
          if v-faxpriority = "u" then v-faxpriority = "Urgent".
          if v-faxpriority ne "Low" and v-faxpriority ne "High" and
             v-faxpriority ne "Normal" and v-faxpriority ne "Urgent" then do:
            message "Faxpriority cannot be set to " v-faxpriority.
            v-faxpriority = "Normal".
            pause.
          end.
          display v-faxpriority with frame f-oimzl.   
        end.
        if frame-field = "v-emailaddr" and 
           (s-printernmack = "email" or s-printernmack = "e-mail")
           then do:
             v-emailaddr[1] = input v-emailaddr[1].
             if index(v-emailaddr[1],"@") <= 0 then do:
               message "Please enter a valid email address".
               next-prompt v-emailaddr[1] with frame f-oimzl.
               next.
             end.
             else
               message "".
        end.  
        if frame-field = "v-faxphoneno" then do:
          v-faxphoneno = input v-faxphoneno.
          if s-printernmack = "fax" and v-faxphoneno = "" then do:
            message "must have a fax number to fax to".
            next-prompt v-faxphoneno with frame f-oimzl.
            next.
          end.
          else message "".
        end.
        if frame-field = "v-faxto1" then do:
          v-faxto1 = input v-faxto1.
          if s-printernmack = "fax" and v-faxto1 = "" then do:
            message "Must have a Person to fax to".
            next-prompt v-faxto1 with frame f-oimzl.
            next.
          end.
          else message "".
        end.
        if frame-field = "v-crdfrm" then do:
          v-crdfrm = input v-crdfrm.
        end.  
        if frame-field = "v-faxto2" then do:
          v-faxto2 = input v-faxto2.
          if s-printernmack = "fax" and v-faxto2 = "" then do:    
            message "Please have a Company to fax to".               
            next-prompt v-faxto2 with frame f-oimzl.              
            next.
          end.            
          else message "".                                        
        end.
        if frame-field = "letterCode" then do:
          letterCode = input letterCode.
          find first cmsl use-index k-cmsl                      
            where cmsl.lettercd = letterCode            
            no-lock no-error.                                   
          if not avail cmsl then 
            find first cmsl use-index k-cmsl 
              where cmsl.lettercd begins letterCode
              and cmsl.user1 = letterCode
              and cmsl.user2 = g-operinits
              no-lock no-error.
          if not avail cmsl then 
            find first cmsl use-index k-cmsl
              where cmsl.lettercd begins letterCode
              and cmsl.user1 = letterCode
              and cmsl.user2 = ""
              no-lock no-error.
          if avail cmsl then do:                                
             /* letterCode = cmsl.user1. */                            
              do i = 1 to 10:                                     
                if sdi-faxcom[i] = "" then 
                  sdi-faxcom[i] = cmsl.noteln[i].                   
              end.                                                
              release cmsl.                                       
          end.    
          run DisplayInfo.
          /**
          if g-secure = 5 and s-printernmack = "fax" then
            next-prompt v-faxphoneno with frame f-oimzl.  
          else if s-printernmack = "e-mail" then 
            next-prompt retainFlag with frame f-oimzl.
          else next-prompt v-faxto1 with frame f-oimzl.
          next.
          **/                 
        end. /* letterCode */
      end.
      if {k-cancel.i} then do:
        exitFlag = yes.
        cancelFlag = yes.
        hide frame f-oimzl.
        apply lastkey.
        leave.
      end.
      if keylabel(lastkey) = "PF1" then do:
        exitFlag = yes.
        cancelFlag = no.
        apply lastkey.
        /* run the code */
        leave.
      end.

      if {k-func6.i} then do:
        v-rtrnfl = yes.
        if g-orderno > 0 then do:
          find first bm-oeeh use-index k-oeeh where
            bm-oeeh.cono = g-cono and bm-oeeh.orderno = g-orderno and
            bm-oeeh.ordersuf = g-ordersuf no-lock no-error.
          if avail bm-oeeh then do:  
            run zsdicontact.p(input "uu", input g-cono, input bm-oeeh.custno,
              input bm-oeeh.shipto, input-output v-contact,
              input-output v-faxto2,
              input-output v-faxphoneno, input-output v-emailaddrgp).
            v-emailaddr[1] = v-emailaddrgp.
            assign v-faxto2 = v-contact.
            display v-faxphoneno v-emailaddr v-faxto2 with frame f-oimzl.
          end.
          release bm-oeeh.
        end.  
        next.
      end. /* F6 */
      
      if {k-func7.i} then do: 
        assign v-rtrnfl = yes.
        run Update_OneTimer.
        display v-faxphoneno v-faxto2 with frame f-oimzl.
        next.
      end.

      if {k-func8.i} then do:
         run oipw2.p.
         next.
      end.   
       
      if not dontapply then
        apply lastkey.
      else
        dontapply = no.
  end. /* editing */       
end procedure.

Procedure DisplayInfo:
  display  g-orderno       g-ordersuf  g-custno     /*  s-ackprintfl */
           sendType
           s-printernmack  letterCode  v-faxphoneno   v-faxpriority  
           retainFlag
           v-emailaddr     v-blprt v-faxto2    v-crdfrm v-faxto1     v-faxfrom
           sdi-faxcom
          with frame f-oimzl.
  
  put screen row 21 col 8 color message
   " F6-Browse Selection  F7-OnTime Fax Info   F8-Western                   ". 
end procedure.

Procedure ClearDisplay:                                                 
  /* 
  g-orderno = "".
  g-ordersuf = "".
  g-custno 
  s-ackprintfl */
  s-printernmack = "".
  letterCode = "".
  v-faxphoneno = "".
  retainFlag = no.                                                 
  v-emailaddr ="".
/*   v-blprt = no.  */  /* smaf */
  v-faxto2 = "". 
  v-faxto1 = "".
  v-faxfrom = "".       
  sdi-faxcom = "".                                                 
end procedure.                                                                   
 
procedure Update_OneTimer:
 
display
  g-orderno
  g-ordersuf
  sendtype        
  s-printernmack  
  lettercode
  retainflag
  v-blprt
  v-faxphoneno 
  v-faxpriority
  v-faxto2     
  v-crdfrm
  v-faxto1
  v-faxfrom

  sdi-faxcom[1]   
  sdi-faxcom[2]   
  sdi-faxcom[3]   
  sdi-faxcom[4]   
  sdi-faxcom[5]   
  sdi-faxcom[6]   
  sdi-faxcom[7]   
  sdi-faxcom[8]   
  sdi-faxcom[9]   
  sdi-faxcom[10]
with frame f-oimzl2.

update v-faxphoneno
  v-faxto2 
  v-blprt
  v-crdfrm
  with frame f-oimzl2
editing:
  readkey.
  if {k-cancel.i} then do:
    hide frame f-oimzl2.
    apply lastkey.
    leave.
  end.
  apply lastkey.
end.
hide frame f-oimzl2.
display v-faxphoneno v-faxto2 v-crdfrm v-blprt with frame f-oimzl.

put screen row 21 col 8 color message
  " F6-Browse Selection  F7-OnTime Fax Info   F8-Western                   ". 
pause 0.
end procedure.

