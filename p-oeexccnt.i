/******************************************************************************
  INCLUDE       : p-oeexqcnt.i
  DESCRIPTION   : Custom Quote Entry - Conversion
  USED ONCE?    : No (oeexc.p, oeexcinq.p)
  AUTHOR        : SunSource
  DATE WRITTEN  : 05/01/15
  CHANGES MADE  : 
                  
******************************************************************************/



/* ------------------------------------------------------------------------- */ 
Procedure Conversion-Load: 
/* ------------------------------------------------------------------------- */
 
  find icsp where icsp.cono = g-cono and
                  icsp.prod = t-lines.prod no-lock no-error.
  
  assign v-loadbrowseline = true
                     v-lmode = "c"
                     v-lcomment =   t-lines.lcomment  
                     v-seqno      = t-lines.seqno
                     v-lineno     = t-lines.lineno      
                     v-prod       = t-lines.prod         
                     v-xprod      = t-lines.xprod
                     v-whse       = t-lines.whse         
                     v-vendno     = t-lines.vendno
                     v-prodcat    = t-lines.prodcat
                     v-listprc    = t-lines.listprc      
                     v-ptype      = t-lines.pricetype
                     v-sellprc    = t-lines.sellprc
                     v-Lcommpct   = t-lines.Lcommpct
                     v-prodcost   = t-lines.prodcost
                     v-glcost     = t-lines.glcost
                     v-totcost    = t-lines.totcost
                     v-leadtm     = t-lines.leadtm
                     v-gp         = t-lines.gp           
                     v-descrip    = t-lines.descrip
                     v-qty        = if hold-lineno <> v-lineno then
                                      t-lines.qty
                                    else v-qty
                     v-shipdt     = t-lines.shipdt     
                     v-specnstype = t-lines.specnstype
                     v-totnet     = t-lines.totnet
                     v-lcomment   = t-lines.lcomment
                     v-totcost    = t-lines.totcost
                     v-rebatefl   = t-lines.rebatefl
                     v-totmarg    = t-lines.totmarg
                     v-margpct    = t-lines.margpct
                     o-lineno     = t-lines.lineno      
                     o-seqno      = t-lines.seqno
                     o-prod       = t-lines.prod         
                     o-whse       = t-lines.whse         
                     o-listprc    = t-lines.listprc      
                     o-sellprc    = t-lines.sellprc     
                     o-Lcommpct   = t-lines.Lcommpct
                     o-gp         = t-lines.gp           
                     o-disc       = t-lines.discpct
                     o-disctype   = t-lines.disctype
                     o-qty        = t-lines.qty          
                     o-shipdt     = t-lines.shipdt
                     o-specnstype = t-lines.specnstype.     
              /* display product lookup and whse info on existing lines*/
end. /* Conversion-Load */

/* ------------------------------------------------------------------------- */ 
Procedure Conversion-Loadx: 
/* ------------------------------------------------------------------------- */
 
  find icsp where icsp.cono = g-cono and
                  icsp.prod = xt-lines.prod no-lock no-error.
  
  assign v-loadbrowseline = true
                     v-lmode = "c"
                     v-lcomment =   xt-lines.lcomment  
                     v-seqno      = xt-lines.seqno
                     v-lineno     = xt-lines.lineno      
                     v-prod       = xt-lines.prod         
                     v-xprod      = xt-lines.xprod
                     v-whse       = xt-lines.whse         
                     v-vendno     = xt-lines.vendno
                     v-prodcat    = xt-lines.prodcat
                     v-listprc    = xt-lines.listprc      
                     v-ptype      = xt-lines.pricetype
                     v-sellprc    = xt-lines.sellprc
                     v-Lcommpct   = xt-lines.Lcommpct
                     v-prodcost   = xt-lines.prodcost
                     v-glcost     = xt-lines.glcost
                     v-totcost    = xt-lines.totcost
                     v-leadtm     = xt-lines.leadtm
                     v-gp         = xt-lines.gp           
                     v-disc       = xt-lines.discpct
                     /*v-disctype   = xt-lines.disctype*/
                     v-descrip    = xt-lines.descrip
                     v-qty        = if hold-lineno <> v-lineno then
                                      xt-lines.qty
                                     else v-qty
                     v-shipdt     = xt-lines.shipdt     
                     v-specnstype = xt-lines.specnstype
                     v-matrixed   = if xt-lines.qtybrkty <> "" then
                                       xt-lines.qtybrkty
                                    else
                                       " "
                     v-pdrecord    = xt-lines.pdscrecno
                     v-pdtype     = xt-lines.pdtype
                     v-pdamt      = xt-lines.pdamt
                     v-pd$md      = xt-lines.pd$md
                     v-totnet     = xt-lines.totnet
                     v-lcomment   = xt-lines.lcomment
                     v-totcost    = xt-lines.totcost
                     v-rebatefl   = xt-lines.rebatefl
                     v-totmarg    = xt-lines.totmarg
                     v-margpct    = xt-lines.margpct
                     o-lineno     = xt-lines.lineno      
                     o-seqno      = xt-lines.seqno
                     o-prod       = xt-lines.prod         
                     o-whse       = xt-lines.whse         
                     o-listprc    = xt-lines.listprc      
                     o-sellprc    = xt-lines.sellprc     
                     o-Lcommpct   = xt-lines.Lcommpct
                     o-gp         = xt-lines.gp           
                     o-disc       = xt-lines.discpct
                     o-disctype   = xt-lines.disctype
                     o-qty        = xt-lines.qty          
                     o-shipdt     = xt-lines.shipdt
                     o-specnstype = xt-lines.specnstype.     
              /* display product lookup and whse info on existing lines*/
end. /* Conversion-Loadx */

   
/* ------------------------------------------------------------------------- */ 
Procedure Conversion: 
/* ------------------------------------------------------------------------- */
  define input parameter ix-recid as recid no-undo.
  def buffer xi-sasoo for sasoo.
  def buffer xi-icsd  for icsd.
  def buffer xi-sasos for sasos.
  def buffer xi-arsc for arsc.
  def buffer xi-arss for arss.
  def buffer xi-oeelb for oeelb.
  def var xi-oeehbrecid as recid no-undo.
  def var o-shipvia like oeeh.shipvia no-undo.

  on cursor-up back-tab.   
  on cursor-down tab.

  on leave of oeehb.transtype in frame f-oeexqcnt do:
    if oeehb.transtype <> "SO" then
      do:
      message "Converted Commission Quote must be an SO Type".
      next-prompt oeehb.transtype with frame f-oeexqcnt.
      next.
    end.
    /*assign s-oelockfl = input s-oelockfl.*/
    hide frame f-FOlock no-pause.
    put screen row 13 col 3 color 
    message 
 " F7-Line Maintenance   F8-Vendor/Whse                                      ".
  end.
  
  assign s-canceled = false.  
  find first  xi-sasoo where xi-sasoo.cono   = g-cono and
                             xi-sasoo.oper2  = g-operinits no-lock no-error.
  if not avail xi-sasoo then 
    leave.
  
  assign v-shiptofl       = xi-sasoo.shiptofl
         v-powtintfllocal = false.
  
  for each xi-oeelb where
       xi-oeelb.cono        = g-cono and
       xi-oeelb.batchnm     = string(v-quoteno,">>>>>>>9") and
       xi-oeelb.seqno       = 2 :
    overlay (xi-oeelb.user5,2,3) = "no ".
  end. 

/* Create a Copy of this quote                                    */
  
  run oeexcrelx.p (input "Prepare",
                   input ix-recid,
                   output x-error). 
  if x-error then
    do:
    message "Fatal Preparation error". pause 5.
    assign s-canceled = true.
    return.
    end.
 else
    assign x-backed-quote-up = true.
 
 /* das - 02/23/09 - tax fix */
 find oeehb where recid(oeehb) = ix-recid no-error.
 if avail oeehb then
   do:
   assign h-whse = oeehb.whse.
   /* get the customer that corrisponds with the vendor (APSV.APCUSTNO)*/
   find first oeelb where oeelb.cono       = g-cono and
                          oeelb.batchnm    = oeehb.batchnm and
                          oeelb.seqno      = oeehb.seqno and
                          oeelb.specnstype <> "L"
                          no-lock no-error.
   if avail oeelb then
     do:
     find icsw where icsw.cono = g-cono and
                     icsw.whse = h-whse and
                     icsw.prod = oeelb.shipprod
                     no-lock no-error.
     if avail icsw then
       find apsv where apsv.cono = g-cono and
                       apsv.vendno = icsw.arpvendno
                       no-lock no-error.
     else
       find apsv where apsv.cono = g-cono and
                       apsv.vendno = oeelb.arpvendno
                       no-lock no-error.
     if avail apsv and apsv.arcustno > 0 then
       do:
       find arsc where arsc.cono = g-cono and
                       arsc.custno = apsv.arcustno.
       if avail arsc then
         do:
         assign oeehb.custno     = arsc.custno
                oeehb.shipto     = ""
                oeehb.nontaxty   = arsc.nontaxtype
                oeehb.taxablefl  = no
                oeehb.taxamt     = 0
                oeehb.taxauth    = " "
                oeehb.taxdefltty = " "
                oeehb.taxovercd  = " "
                oeehb.taxoverfl  = no
                oeehb.taxsaleamt = 0.
       find b-oeehb where b-oeehb.cono       = g-cono + 500 and
                          b-oeehb.batchnm    = oeehb.batchnm and
                          b-oeehb.seqno      = oeehb.seqno   and
                          b-oeehb.sourcepros = oeehb.sourcepros
                          no-error.
       if avail b-oeehb then
         assign b-oeehb.xxde5 = arsc.custno.
       end. /* avail arsc */
     end. /* avail apsv */
   end. /* avail oeelb */
   for each oeelb where oeelb.cono       = g-cono and       
                        oeelb.batchnm    = oeehb.batchnm and
                        oeelb.seqno      = oeehb.seqno and  
                        oeelb.specnstype <> "L":
     assign oeelb.taxablefl = no
            oeelb.taxgroup  = 0.
            overlay(oeelb.user4,1,4) = "ACVC".
   end.  
   assign oeehb.whse = "ACVC".
 end. /* avail oeehb */
 
 if avail oeehb then
   do:
   /*if not avail icsd then*/
   find icsd where icsd.cono = g-cono and 
                   icsd.whse = oeehb.whse 
                   no-lock no-error.
   if avail icsd then
    assign oeehb.reqshipdt =                                              
      (if ((int(substring(string(time,"hh:mm:ss"),1,2)) * 100) +      
            int(substring(string(time,"hh:mm:ss"),4,2))) >              
          ((truncate(truncate(icsd.enddaycut / 60 ,0) / 60,0) * 100) + 
           (truncate(icsd.enddaycut / 60 ,0)) mod 60) then             
            oeehb.reqshipdt  + 1                                      
       else                                                          
         oeehb.reqshipdt).                                         
    
    run zsdiweekend (input-output oeehb.reqshipdt,                        
                     input oeehb.whse,                                    
                     input g-cono).                                        
  end.
ReleaseLoop:

do while true on endkey undo Releaseloop, leave ReleaseLoop:

  assign v-conversion = true
         v-loadbrowseline = false
         v-lmode = "c".
  run Clear-Frames.
  find oeehb where recid(oeehb) = ix-recid no-error.
  assign xi-oeehbrecid = recid(oeehb)
         v-contact = substr(oeehb.user3,1,20)
         v-custno = oeehb.custno
         v-shipto = oeehb.shipto.
  if oeehb.transtype = "QU" then
     oeehb.transtype = "SO".
  if oeehb.stagecd = 0 then
     oeehb.stagecd = 1.   


  display
    oeehb.batchnm
    oeehb.custno
    oeehb.transtype  
    oeehb.shiptonm  
    oeehb.shipto  
    oeehb.shiptoaddr[1] 
    oeehb.whse   
    oeehb.shiptoaddr[2] 
    oeehb.custpo   
    oeehb.shiptocity
    oeehb.shiptost   
    oeehb.shiptozip
    v-contact
    oeehb.shipvia    
    oeehb.inBndFrtFl   
    oeehb.outBndFrtFl   
    oeehb.orderdisp  
    oeehb.slsrepout  
    oeehb.slsrepin     
    oeehb.reqshipdt  
    oeehb.refer
    oeehb.shipinstr
      with frame f-oeexqcnt.
 put screen row 13 col 3 color 
           message 
 " F7-Line Maintenance   F8-Vendor/Whse                                      ".


  assign o-InBndFrtfl  = oeehb.InBndFrtFl 
         o-OutBndFrtfl = oeehb.OutBndFrtfl
         o-shipvia     = oeehb.shipvia.


  conversionloop:

do while true on endkey undo conversionloop, leave conversionloop:
     put screen row 13 col 3 color 
           message 
 " F7-Line Maintenance   F8-Vendor/Whse                                      ".
    assign g-custno = oeehb.custno
           g-shipto = oeehb.shipto

           hx-batchnm        = oeehb.batchnm             
           hx-custno         = oeehb.custno              
           hx-transtype      = oeehb.transtype           
           hx-shiptonm       = oeehb.shiptonm            
           hx-shipto         = oeehb.shipto              
           hx-shiptoaddr1    = oeehb.shiptoaddr[1]      
           hx-whse           = oeehb.whse                
           hx-shiptoaddr2    = oeehb.shiptoaddr[2]       
           hx-custpo         = oeehb.custpo              
           hx-shiptocity     = oeehb.shiptocity          
           hx-shiptost       = oeehb.shiptost            
           hx-shiptozip      = oeehb.shiptozip           
           hx-contact        =  v-contact
           hx-shipvia        = oeehb.shipvia             
           hx-inBndFrtFl     = oeehb.inbndfrtfl          
           hx-outBndFrtFl    = oeehb.outbndfrtfl         
           hx-orderdisp      = oeehb.orderdisp           
           hx-slsrepout      = oeehb.slsrepout           
           hx-slsrepin       = oeehb.slsrepin            
           hx-reqshipdt      = oeehb.reqshipdt           
           hx-refer          = oeehb.refer
           hx-shipinstr      = oeehb.shipinstr.           
    
    update
      oeehb.custno       when not v-conversion
      oeehb.shipto
      /*
      oeehb.shiptonm       when v-shiptofl = yes
      oeehb.shiptoaddr[1]  when v-shiptofl = yes
      oeehb.shiptoaddr[2]  when v-shiptofl = yes
      oeehb.shiptocity     when v-shiptofl = yes
      oeehb.shiptost       when v-shiptofl = yes
      oeehb.shiptozip      when v-shiptofl = yes
      oeehb.geocd          when v-shiptofl = yes
      
      oeehb.transtype  
      */
      oeehb.whse    
      oeehb.custpo
      v-contact
      oeehb.shipvia 
      oeehb.reqshipdt  
      oeehb.refer  
      oeehb.shipinstr

      oeehb.shiptonm       when v-shiptofl = yes
      oeehb.shiptoaddr[1]  when v-shiptofl = yes
      oeehb.shiptoaddr[2]  when v-shiptofl = yes
      oeehb.shiptocity     when v-shiptofl = yes
      oeehb.shiptost       when v-shiptofl = yes
      oeehb.shiptozip      when v-shiptofl = yes
      oeehb.geocd          when v-shiptofl = yes
      
      go-on(f7,f8) 

        with frame f-oeexqcnt 
      editing:
     put screen row 13 col 3 color 
           message 
 " F7-Line Maintenance   F8-Vendor/Whse                                      ".

       readkey.
     
       hide message no-pause.   /* das - 03/18/09 - added to remove display */
                                 /*                  of previous error msg   */
       if {k-cancel.i}  then 
         do:
         message  "You hit (F4) CANCEL." skip 
                  "Cancel this Conversion ?"
           view-as alert-box question buttons yes-no            
         update zx-s as logical .                                 
         if zx-s then 
           do:
           assign ip-update = false
                  s-canceled = true.
                  undo Releaseloop, leave Releaseloop.                                    end.                  
         else 
           do:
           put screen row 13 col 3 color 
           message 
 " F7-Line Maintenance   F8-Vendor/Whse                                      ".

           next conversionloop.          
         end.
       end. /* if k-cancel */

       if {k-after.i} or {k-func.i} then 
         do: 
         if can-do("306,307,308,309,310,401",string(lastkey)) then
           do:  /* if a func key, update the shipto info */
           for each t-oeehb where 
                    t-oeehb.cono       = oeehb.cono    and
                    t-oeehb.batchnm    = oeehb.batchnm and
                    t-oeehb.seqno      = oeehb.seqno   and
                    t-oeehb.sourcepros = oeehb.sourcepros:
             assign t-oeehb.shiptonm      = input oeehb.shiptonm
                    t-oeehb.shiptoaddr[1] = input oeehb.shiptoaddr[1]
                    t-oeehb.shiptoaddr[2] = input oeehb.shiptoaddr[2]
                    t-oeehb.shiptocity    = input oeehb.shiptocity
                    t-oeehb.shiptost      = input oeehb.shiptost
                    t-oeehb.shiptozip     = input oeehb.shiptozip
                    t-oeehb.geocd         = input oeehb.geocd
                    v-shiptonm            = t-oeehb.shiptonm
                    v-shiptoaddr1         = t-oeehb.shiptoaddr[1]
                    v-shiptoaddr2         = t-oeehb.shiptoaddr[2]
                    v-shiptocity          = t-oeehb.shiptocity
                    v-shiptost            = t-oeehb.shiptost
                    v-shiptozip           = t-oeehb.shiptozip
                    v-shiptogeocd         = t-oeehb.geocd.

             find xi-arsc where xi-arsc.cono = g-cono and
                                xi-arsc.custno = g-custno no-lock no-error.
             if t-oeehb.shipto <> "" then 
               do:
               find xi-arss where xi-arss.cono = g-cono and
                                  xi-arss.custno = g-custno and
                                  xi-arss.shipto = 
                                   t-oeehb.shipto no-lock no-error.
 
               if avail xi-arss then 
                 do:
                 if xi-arss.state        <> t-oeehb.shiptost then 
                   do:
                   /*assign /* t-oeehb.taxablefl = true  Chuck tax take out
                             t-oeehb.nontaxtype = ""  */
                          t-oeehb.statecd = t-oeehb.shiptost.*/
                   for each oeelb where 
                            oeelb.cono = g-cono and
                            oeelb.batchnm = t-oeehb.batchnm and
                            oeelb.seqno   = t-oeehb.seqno:
                     assign oeelb.taxablefl    = t-oeehb.taxablefl
                            oeelb.nontaxtype   = t-oeehb.nontaxtype.
                   end. /* each oeelb */
                 end. /* xi-ars.state <> t-oeehb.shiptost */         
               end. /* if avail xi-arss */
             end. /* t-oeehb.shipto <> "" */
             else 
             if xi-arsc.state        <> t-oeehb.shiptost then 
               do:
               assign /* t-oeehb.taxablefl = true
                         t-oeehb.nontaxtype = "" Chuck tax take out */
                      t-oeehb.state = t-oeehb.shiptost.
               for each oeelb where 
                        oeelb.cono = g-cono and
                        oeelb.batchnm = t-oeehb.batchnm and
                        oeelb.seqno   = t-oeehb.seqno:
                 assign oeelb.taxablefl    = t-oeehb.taxablefl
                        oeelb.nontaxtype   = t-oeehb.nontaxtype.
               end. /* for each oeelb */
             end. /* xi-arsc.state <> shiptost */
           end. /* for each t-oeehb */
         end. /* a movement key was pressed */

         if frame-field = "shiptost" and
            input oeehb.shiptost <> oeehb.shiptost and
                     not oeehb.taxablefl then do:
           /* assign /* oeehb.taxablefl = true
                     oeehb.nontaxtype = ""  Chuck tax take out */
                  oeehb.statecd = input oeehb.shiptost.*/
           for each oeelb where oeelb.cono = g-cono and
                    oeelb.batchnm = oeehb.batchnm and
                    oeelb.seqno   = oeehb.seqno:
              assign oeelb.taxablefl    = oeehb.taxablefl
                     oeelb.nontaxtype   = oeehb.nontaxtype.
           end.
         end. /* frame-field = "shiptost */
         
         if frame-field = "shipto" and
            hx-shipto <> input oeehb.shipto then 
           do:
/* Perfection */        
  
           run oeexqCustFocusPop.p (input g-cono,
                                    input 0,
                                    input 0,
                                    input g-custno,
                                    input (input oeehb.shipto)).
/* Perfection */        
   

           find xi-arsc where xi-arsc.cono = g-cono and
                              xi-arsc.custno = g-custno no-lock no-error.
           find xi-arss where xi-arss.cono = g-cono and
                              xi-arss.custno = g-custno and
                              xi-arss.shipto = 
                                input oeehb.shipto no-lock no-error.
           if avail xi-arss then
             assign v-shipfl = true.
           else 
             do:
             if input oeehb.shipto <> "" then
               do:
               run err.p (4304).
               assign hx-shipto    = oeehb.shipto.
               next-prompt oeehb.shipto with frame f-oeexqcnt.
               next.
             end.
           end.
/* 10-23-12 tah APR fix */           
 
           assign v-shiptostatecd = if avail xi-arss then xi-arss.state 
                                    else
                                    if avail xi-arsc then xi-arsc.state 
                                    else " ".
/* 10-23-12 tah APR fix */           
           
           assign oeehb.shipto          = input oeehb.shipto
                  oeehb.xxl12           = no /* reset xxl12 so all shipto info
                                                will reset */
                  hx-shipto            = oeehb.shipto
                  oeehb.statecd       = if avail xi-arss then 
                                          xi-arss.statecd
                                         else
                                        if avail xi-arsc then 
                                          xi-arsc.statecd
                                        else " "
                  oeehb.shiptonm       = if avail xi-arss then xi-arss.name 
                                         else
                                        if avail xi-arsc then xi-arsc.name 
                                         else " "
                  oeehb.shiptoaddr[1] = if avail xi-arss then xi-arss.addr[1]
                                        else
                                        if avail xi-arsc then xi-arsc.addr[1] 
                                        else " "
                  oeehb.shiptoaddr[2] = if avail xi-arss then xi-arss.addr[2] 
                                        else
                                        if avail xi-arsc then xi-arsc.addr[2] 
                                        else " "
                  oeehb.shiptocity    = if avail xi-arss then xi-arss.city 
                                        else
                                        if avail xi-arsc then xi-arsc.city 
                                        else " "
                  oeehb.shiptost      = if avail xi-arss then xi-arss.state 
                                        else
                                        if avail xi-arsc then xi-arsc.state 
                                        else " "
                  oeehb.shiptozip     = if avail xi-arss then xi-arss.zip 
                                        else
                                        if avail xi-arsc then xi-arsc.zip 
                                        else " "
                  oeehb.geocd   = if avail xi-arss then xi-arss.geocd 
                                        else
                                        if avail xi-arsc then xi-arsc.geocd 
                                        else 0
                  /*
                  oeehb.outbndfrtfl   = if avail xi-arss then 
                                          xi-arss.outbndfrtfl
                                        else if avail xi-arsc then
                                          xi-arsc.outbndfrtfl
                                        else oeehb.outbndfrtfl
                  oeehb.inbndfrtfl    = if avail xi-arss then
                                          xi-arss.inbndfrtfl
                                        else if avail xi-arsc then
                                          xi-arsc.inbndfrtfl
                                        else oeehb.inbndfrtfl
                  */
                  oeehb.outbndfrtfl   = no
                  oeehb.inbndfrtfl    = no
                  o-InBndFrtfl        = oeehb.inbndfrtfl
                  o-OutBndFrtfl       = oeehb.outbndfrtfl.
                    
           assign  oeehb.slsrepout     = if avail xi-arss then 
                                            xi-arss.slsrepout
                                          else
                                          if avail xi-arsc then 
                                            xi-arsc.slsrepout
                                          else oeehb.slsrepout
                    oeehb.slsrepin      = if avail xi-arss then 
                                            xi-arss.slsrepin
                                          else
                                          if avail xi-arsc then 
                                            xi-arsc.slsrepin
                                          else oeehb.slsrepin
                    oeehb.termstype     = if avail xi-arss then
                                            xi-arss.termstype
                                          else
                                          if avail xi-arsc then
                                            xi-arsc.termstype
                                          else oeehb.termstype
                    oeehb.taxablefl     = if avail xi-arss then
                                            if xi-arss.taxablety = "n"
                                              then no
                                            else yes
                                          else
                                          if avail xi-arsc then
                                            if xi-arsc.taxablety = "n"
                                              then no
                                            else yes
                                          else oeehb.taxablefl
                    oeehb.nontaxtype    = if avail xi-arss then
                                            xi-arss.nontaxtype
                                          else
                                          if avail xi-arsc then
                                            xi-arsc.nontaxtype
                                          else
                                            oeehb.nontaxtype
                    oeehb.shipinstr     = if avail xi-arss then
                                            xi-arss.shipinstr
                                          else
                                          if avail xi-arsc then
                                            xi-arsc.shipinstr
                                          else
                                            oeehb.shipinstr
                    oeehb.orderdisp     = if avail xi-arss then
                                            xi-arss.orderdisp
                                          else
                                          if avail xi-arsc then
                                            xi-arsc.orderdisp
                                          else
                                            oeehb.orderdisp.
                    
           /* das - 070507 - if shipto's change, slsrep info must change */
           for each oeelb where oeelb.cono    = g-cono and
                                 oeelb.batchnm = oeehb.batchnm and
                                 oeelb.seqno   = oeehb.seqno:
              assign oeelb.slsrepout  = if avail xi-arss then xi-arss.slsrepout
                                        else
                                        if avail xi-arsc then xi-arsc.slsrepout
                                        else oeelb.slsrepout
                     oeelb.slsrepin   = if avail xi-arss then xi-arss.slsrepin
                                        else
                                        if avail xi-arsc then xi-arsc.slsrepin
                                        else oeelb.slsrepin
                     oeelb.taxablefl  = if avail xi-arss then
                                          if xi-arss.taxablety = "n"
                                            then no
                                          else yes
                                        else
                                          if avail xi-arsc then
                                            if xi-arsc.taxablety = "n"
                                              then no
                                            else yes
                                          else oeelb.taxablefl
                     oeelb.nontaxtype = if avail xi-arss then
                                          xi-arss.nontaxtype
                                        else
                                          if avail xi-arsc then
                                            xi-arsc.nontaxtype
                                          else
                                            oeelb.nontaxtype.
           end. /* each oeelb */
          
           display
             oeehb.shiptonm  
             oeehb.shipto  
             oeehb.shiptoaddr[1] 
             oeehb.shiptoaddr[2] 
             oeehb.shiptocity
             oeehb.shiptost   
             oeehb.shiptozip
             oeehb.slsrepout    /* das */
             oeehb.slsrepin     /* das */
             oeehb.shipviaty
             oeehb.shipinstr
             oeehb.orderdisp
             oeehb.inbndfrtfl 
             oeehb.outbndfrtfl
           with frame f-oeexqcnt.
 put screen row 13 col 3 color 
           message 
 " F7-Line Maintenance   F8-Vendor/Whse                                      ".
         end. /* if frame-field = oeehb.shipto */
         
         if frame-field = "whse" and 
            oeehb.whse <> input oeehb.whse then
           do:
           
           assign v-bodexists = no.
           for each oeelb where oeelb.cono    = g-cono and
                                oeelb.batchnm = oeehb.batchnm and
                                oeelb.seqno   = oeehb.seqno
                                no-lock:
             if substr(oeelb.user5,1,1) = "y" then
               do:
               assign v-bodexists = yes.
               leave.
             end. /* if BOD exists */
           end. /* for each oeelb */
         end. /* if frame-field = "whse" */
          
         if frame-field = "refer" then 
           do:
           if input oeehb.refer = "Credit Card Order" then
              assign oeehb.termstype = "crcd".
         end. 
          
         if frame-field = "v-contact" then
           do:
           if input v-contact <> hx-contact then
             do:
             /* if user adds contact at conversion, populate the placedby,
                but don't change original contact info
             overlay(oeehb.user3,1,20)  = string(v-contact,"x(20)").
             overlay(oeehb.user3,21,12) = string(v-phone,"x(12)").  
             overlay(oeehb.user3,33,20) = string(v-email,"x(20)").  
             */
             assign oeehb.placedby      = input v-contact.
           end.
         end. /* frame-field = "v-contact" */
          
         if frame-field = "shipviaty" and
           oeehb.shipviaty <> input oeehb.shipviaty then 
           do:
           if not can-find (first sasta where sasta.cono = g-cono and 
                                              sasta.codeiden = "s" and
                                              sasta.codeval =
                                                input oeehb.shipViaty
                                              no-lock) then 
             do:
             run err.p (4030).
             next-prompt oeehb.shipviaty with frame f-oeexqcnt.
             next.
           end. /* if not can-find sasta */
           if zx-errorty = 0 then
             assign o-InBndFrtfl  = oeehb.InBndFrtFl 
                    o-OutBndFrtfl = oeehb.OutBndFrtfl
                    o-shipvia     = input oeehb.shipviaty
                    oeehb.shipviaty = input oeehb.shipviaty.

           display
             oeehb.shipviaty
             oeehb.InBndFrtfl
             oeehb.OutBndFrtfl
           with frame f-oeexqcnt.
 put screen row 13 col 3 color 
           message 
 " F7-Line Maintenance   F8-Vendor/Whse                                      ".
         end. /* frame-field = "v-shipviaty */
         
         if frame-field = "orderdisp" and (lastkey =   9 or
                                           lastkey =  13 or
                                           lastkey = 401 or
                                           lastkey = 502 or
                                           lastkey = 508) then
           do:
           assign oeehb.orderdisp = input oeehb.orderdisp.
           if oeehb.orderdisp = "S" then
             do:
             for each oeelb where oeelb.cono = g-cono and
                                  oeelb.batchnm = oeehb.batchnm and
                                  oeelb.seqno   = oeehb.seqno:
               assign oeelb.botype = "N".
             end. /* each oeelb */
           end. /* orderdisp = "s" */
           else
             do:
             for each oeelb where oeelb.cono = g-cono and
                                  oeelb.batchnm = oeehb.batchnm and
                                  oeelb.seqno   = oeehb.seqno:
               if oeelb.botype = "n" then
                 assign oeelb.botype = "y".
             end. /* each oeelb */
           end. /* orderdisp <> "s" */
         end. /* orderdisp */
         
         if (frame-field = "custpo" and
           can-do("9,13,401,501,502,503,504,507,508",string(lastkey))) or
           LASTKEY = 401 or
           LASTKEY = KEYCODE("F7") or LASTKEY = KEYCODE("F8") or
           (FRAME-FIELD = "geocd" and LASTKEY = 13) 
           then
           do:
           if input oeehb.custpo <> " " then
             assign oeehb.custpo = input oeehb.custpo.
           if oeehb.custpo ne "" then 
             do:
             find arsc where arsc.cono   = g-cono and
                             arsc.custno = oeehb.custno no-lock no-error.
             find last oeeh use-index k-custpo where
                       oeeh.cono = g-cono and
                       oeeh.custpo = input oeehb.custpo and
                       oeeh.custno = arsc.custno
                       no-lock no-error.
             if avail oeeh then
               do:
               if arsc.xxl13 = no then 
                 do: /* SX 55 */
                 message "Duplicate Customer PO's not allowed for this ARSC".
                 next-prompt oeehb.custpo with frame f-oeexqcnt.
                 next.
               end. 
               if arsc.xxl13 = yes and v-oefoundfl = no then /* SX 55 */
                 do:
                 message color normal
                 "Warning - Duplicate Customer PO Found " string(oeeh.orderno)
                  "-" string(oeeh.ordersuf,"99").
                  assign v-oefoundfl = yes.
                 pause.
               end. /* allow dups = yes */
             end. /* oeeh found */
             else
               do:
               find last L-oeehb where 
                         L-oeehb.cono     = g-cono and
                         L-oeehb.custpo   = oeehb.custpo and
                         L-oeehb.custpo  <> " " and
                         L-oeehb.custno   = oeehb.custno and
                         L-oeehb.batchnm <> oeehb.batchnm and
                         L-oeehb.seqno    = 2 and
                         L-oeehb.sourcepros = "ComQu"
                         no-lock no-error.
               if avail L-oeehb and v-oefoundfl = no then 
                 do:
                 message color normal                                        
              "Warning- Duplicate Customer PO Found on Quote " L-oeehb.batchnm.
                 assign v-oefoundfl = yes.
                 pause.
               end. /* avail oeehb */
             end. /* if oeeh not avail, check oeehb */
           end. /* custpo <> blank */
         end. /* frame-field = custpo and movement key pressed */
         
        /* das - Shipvia Edit Checks */
        if LASTKEY = 401 or
           LASTKEY = KEYCODE("F7") or LASTKEY = KEYCODE("F8") or
          (FRAME-FIELD = "shipviaty" and 
           can-do("13,9,509,503,504,21,1070,1068,505,506",string(lastkey))) or
          (FRAME-FIELD = "geocd" and LASTKEY = 13) then 
          do:
          assign oeehb.shipviaty = input oeehb.shipviaty.
          find sasta where sasta.cono = g-cono  and
                           sasta.codeiden = "s" and
                           sasta.codeval = oeehb.shipviaty and
                           sasta.usagefl = yes and
                           sasta.warrexchgfl = no and
                          (sasta.whse = "upsc" or
                           sasta.whse = "fdxc")
                           no-lock no-error.
          if avail sasta then
            do:
            assign z-shiperr = no.
            assign oeehb.shipinstr = input oeehb.shipinstr.
            if (substr(oeehb.shipinstr,1,1) >= "0" and  
                substr(oeehb.shipinstr,1,1) <= "9") or  
               (substr(oeehb.shipinstr,2,1) >= "0" and  
                substr(oeehb.shipinstr,2,1) <= "9") or  
               (substr(oeehb.shipinstr,3,1) >= "0" and  
                substr(oeehb.shipinstr,3,1) <= "9") or  
               (substr(oeehb.shipinstr,4,1) >= "0" and  
                substr(oeehb.shipinstr,4,1) <= "9") or  
               (substr(oeehb.shipinstr,5,1) >= "0" and  
                substr(oeehb.shipinstr,5,1) <= "9") or
               (substr(oeehb.shipinstr,6,1) >= "0" and  
                substr(oeehb.shipinstr,6,1) <= "9") then
              assign z-shiperr = no.
            else
              assign z-shiperr = yes.
            if z-shiperr = no then
              do:
              do idx = 1 to 6:
                if substr(oeehb.shipinstr,idx,1) = " " then
                  do:
                  assign z-shiperr = yes.
                  leave.
                end.
              end.
            end.
            if z-shiperr = no then
              do:
              /* UPSCollect must have the 7th possition blank */
              if sasta.whse = "UPSC" and 
                 substr(oeehb.shipinstr,7,1) <> " " then
                 assign z-shiperr = yes.
             end.
             if z-shiperr = no then
               do:
               /* FDXCollect can be 7 or 9 digits long. 
                  The 8th or 10th pos s/b blank */
               if sasta.whse = "FDXC" then
                 do:
                 if (substr(oeehb.shipinstr,8,1)  = " " and
                     substr(oeehb.shipinstr,7,1) <> " ") or
                    (substr(oeehb.shipinstr,9,1)  = " " and
                     substr(oeehb.shipinstr,8,1) <> " ") or
                    (substr(oeehb.shipinstr,10,1) = " " and
                     substr(oeehb.shipinstr,9,1) <> " ") or
                    (substr(oeehb.shipinstr,11,1) = " " and
                     substr(oeehb.shipinstr,10,1) <> " ") then
                   assign z-shiperr = no.
                 else
                   assign z-shiperr = yes.
               end. /* FDXC */
             end. /* z-shiperr = no */

             if z-shiperr = yes then
               do:
             message "Ship Instructions must include complete Collect Acct#," +
                     " beginning in 1st position".
               next-prompt oeehb.shipinstr with frame f-oeexqcnt.
               next.
             end.
          end. /* avail sastt */
        end. /* movement key was entered (shipvia edits */
  
      assign   oeehb.shiptonm      = input oeehb.shiptonm      /* das 7/5/07 */
               oeehb.shiptoaddr[1] = input oeehb.shiptoaddr[1] /* das 7/5/07 */
               oeehb.shiptoaddr[2] = input oeehb.shiptoaddr[2] /* das 7/5/07 */
               oeehb.shiptocity    = input oeehb.shiptocity
               oeehb.shiptost      = input oeehb.shiptost
               oeehb.shiptozip     = input oeehb.shiptozip
               oeehb.geocd         = input oeehb.geocd.
       end. /* accept */
         
       apply lastkey.
      end. /* edit loop */
      
  if {k-accept.i} or 
    (frame-field = "geocd" and keyfunction(lastkey) = "return") then
    do:
    {w-arsc.i oeehb.custno no-lock "xi-"}
    /*d Customer PO # is required */
    if xi-arsc.poreqfl and oeehb.custpo = ""  and
       not can-do("qu,cr,rm",oeehb.transtype) then 
      do:
      run err.p (5681).
      next-prompt oeehb.custpo with frame f-oeexqcnt.
      next conversionloop.
    end.

    if substr(oeehb.whse,1,1) <> "A" then
      do:
      message "Warehouse must be an Accounting Warehouse.".
      next-prompt oeehb.whse with frame f-oeexqcnt.
      next conversionloop.
    end.
     
    if oeehb.termstype = "cod"
      /* per Tami, should only happen when cod, not cia or wxfr */
      /*(oeehb.termstype = "cod" or 
         oeehb.termstype = "cia" or
         oeehb.termstype = "wxfr")*/ and
      oeehb.transtype = "do" then 
      do:
      run err.p (6304).
      next-prompt oeehb.transtype with frame f-oeexqcnt.
      next conversionloop.
    end.
 
    if xi-arsc.shipreqfl and oeehb.shipto = "" then 
      do:
      run err.p (5690).
      next-prompt oeehb.shipto with frame f-oeexqcnt.
      next conversionloop.
    end.
     
    if oeehb.orderdisp ne "" and not can-do("w,t,s,j",oeehb.orderdisp) then 
      do:
      run err.p (3621).
      next-prompt oeehb.orderdisp with frame f-oeexqcnt.
      next conversionloop.
    end.
 
    
    if not can-find (first smsn where smsn.cono = g-cono and 
                                      smsn.slsrep = oeehb.slsrepout
                                      no-lock) then 
      do:
      run err.p (4604).
      next-prompt oeehb.slsrepout with frame f-oeexqcnt.
      next conversionloop.
    end.
 
    if not can-find (first smsn where smsn.cono = g-cono and 
                                      smsn.slsrep = oeehb.slsrepin
                                      no-lock) then 
      do:
      run err.p (4604).
      next-prompt oeehb.slsrepin with frame f-oeexqcnt.
      next conversionloop.
    end.

    
    if not can-find (first sasta where sasta.cono = g-cono and 
                                       sasta.codeiden = "s" and
                                       sasta.codeval = oeehb.shipVia
                                       no-lock) then do:
      run err.p (4030).
      next-prompt oeehb.shipvia with frame f-oeexqcnt.
      next conversionloop.
    end.
    
    if oeehb.custpo ne "" then 
      do:
      find arsc where arsc.cono = g-cono and
                      arsc.custno = oeehb.custno no-lock no-error.
      if can-find(first oeeh use-index k-custpo where
                        oeeh.cono = g-cono and
                        oeeh.custpo = oeehb.custpo and
                        oeeh.custno = arsc.custno) then 
        do:
        if arsc.xxl13 = no then 
          do: /* SX 55 */
          message "Duplicate Customer PO's not allowed for this ARSC".
          next-prompt oeehb.custpo with frame f-oeexqcnt.
          next conversionloop.
        end. 
      end.
    end.
  
    /* das - 03/18/09 - don't allow Direct WT Whse be the same as the Banner */
    if input oeehb.transtype = "do" and
      entry(1,oeehb.user4,"^") = "t" and
      entry(9,oeehb.user4,"^") = input oeehb.whse then
      do:
      run err.p (5885).
      next-prompt oeehb.whse with frame f-oeexqcnt.
      next conversionloop.
    end.
  
  /** taxware stuff **/
  /*  Force correct taxing information */
  /*d If an error occurred, we need to force the user to enter valid
                            ship to data.  Currently the only errors being
                            trapped are:
                    1  - non-numeric zip code
                    3  - invalid state code
                    6  - zip code not in range for specified state
                    95 - Taxware could not deterimine taxing jurisdiction. */

    assign
      v-shiptost    = caps(oeehb.shipToSt)
      v-shiptocity  = caps(oeehb.shipToCity)
      v-shiptozip   = caps(oeehb.shipToZip)
      v-shiptogeo   = oeehb.geoCd.
    if oeehb.transtype = "CS":u or oeehb.orderDisp = "W":u then
      do:
      find xi-icsd where xi-icsd.cono = g-cono and 
                         xi-icsd.whse = oeehb.whse no-lock no-error.
      if avail xi-icsd then
        assign
          v-shiptost    = caps(xi-icsd.state)
          v-shiptocity  = caps(xi-icsd.city)
          v-shiptozip   = caps(xi-icsd.zipcd)
          v-shiptogeo   = xi-icsd.geocd.
    end. /* if CS or will call */
    
    if F7-pressed = no then
      do:
      update v-continue label
      "You are accepting to convert all remaining quantities.  Continue?"
       with frame f-allqty side-labels row 5 centered overlay.

      if v-continue = no then 
        do:
        hide frame f-allqty no-pause.
        next-prompt oeehb.whse with frame f-oeexqcnt.
        next conversionloop.
      end. /* v-continue = no */
      else
        do:
        for each oeelb where oeelb.cono = g-cono + 500 and
                 oeelb.batchnm = oeehb.batchnm and
                 oeelb.seqno   = oeehb.seqno:
          assign oeelb.xxde4 = oeelb.qtyord.
          assign oeelb.specnstype = "l"  
                 oeelb.reasunavty = "CO".
        end.
        /****
        /* 12/08/2015 */
        for each oeelb where oeelb.cono = g-cono and
                             oeelb.batchnm = oeehb.batchnm and
                             oeelb.seqno   = oeehb.seqno and
                             oeelb.specnstype <> "L":
          assign oeelb.qtyord = oeelb.qtyord - oeelb.xxde4.
          if oeelb.qtyord < 0 then
            assign oeelb.qtyord = 0
                   oeelb.specnstype = "L".
        end.
        ****/
      end.
      hide frame f-allqty no-pause.
    end. /* Line Maintenance was not used */
    
    overlay(oeehb.user3,1,20) = v-contact.
    overlay(oeehb.user3,65,8) = oeehb.batchnm.

    assign oeehb.promisedt =  oeehb.reqshipdt.  
    /*
    if g-operinit begins "das" then
      do:
      message "in p-oeexccnt"
              "splitfl:" splitfl. pause.
    end.
    */
    if splitfl = no then
      do:
      run Conversion_Line_Split.
    end.
    assign splitfl = yes.
  end. /* f-accept.i or RETURN on geocd */

  if keylabel(lastkey) = "f7" then
    do:
    hide frame f-oeexqcnt.
    assign F7-pressed = yes.
    display  
      oeehb.batchnm    
      oeehb.custno     
      oeehb.transtype  
      oeehb.shipto    
      oeehb.whse      
      with frame f-lineconvert.
    assign v-loadbrowseline = false
           v-lmode = "c".
    on cursor-up cursor-up.   
    on cursor-down cursor-down.
    run Middle1.
                  
    run Clear-Frames.

    hide frame f-lineconvert.
    hide frame f-oeexqcnt.
    hide frame f-lines.

    put screen row 13 col 3 color 
     message 
 " F7-Line Maintenance   F8-Vendor/Whse                                      ".

    on cursor-up back-tab.   
    on cursor-down tab.
    next conversionloop.
    end.

  
  
  if keylabel(lastkey) = "f7" or
     keylabel(lastkey) = "f8" then
    next conversionloop. 

  /* .. */
  
  
  
  
  
  leave conversionloop.
  end.   /* Conversionloop  */

  assign ip-update = true
         x-returncode = "".
  /* 
  run oeexqrel.p (input recid(b-oeehb),input-output ip-update,
                  input-output x-returncode).
  */
  
  /*****/
  if ip-update then 
    do:
    
     run oeexcrelu.p (input recid(oeehb),      
                      input-output ip-orderno).
  end.
  else
  if x-returncode = "back" then
    next  ReleaseLoop.
  /*****/
  
  leave ReleaseLoop.  
    


end.    /*  ReleaseLoop */ 
assign v-conversion = false.

find oeehb where oeehb.cono = g-cono + 500 and
                 oeehb.batchnm = string(w-batchnm,">>>>>>>9") and 
                 oeehb.seqno = 2 and
                 oeehb.sourcepros = "ComQu"
                 no-lock no-error.
if avail oeehb then
  assign ix-recid = recid(oeehb).
run oeexcrelx.p (input "Restore",
                   input ix-recid,
                   output x-error). 

run Clear-Frames.
hide frame f-lineconvert.
hide frame f-oeexqcnt.
hide frame f-lines.


end.

 


/*****************************************************************************/
  PROCEDURE PopUp:
/*****************************************************************************/
  
  DEFINE VAR inTitle AS CHAR FORMAT "x(15)" NO-UNDO.
  DEFINE QUERY q-reason FOR zsastz SCROLLING.
  DEFINE OUTPUT PARAMETER exceptCode AS CHAR NO-UNDO.
  
  
  
  DEFINE BROWSE b-reason QUERY q-reason
  DISPLAY 
    zsastz.primarykey FORMAT "x(4)"
    zsastz.secondarykey FORMAT "x(24)"
  WITH 12 DOWN WIDTH 30 OVERLAY NO-HIDE NO-LABELS. /* NO-BOX.  */
  
  FORM
    inTitle  AT 1 DCOLOR 2
    b-reason AT 1
  WITH FRAME f-reason WIDTH 30 NO-LABELS OVERLAY NO-BOX.
  
  ASSIGN 
    inTitle = "FrtFlags Exceptions".
  
  ON any-key OF b-reason IN FRAME f-reason DO:
    IF {k-accept.i} OR {k-return.i} THEN DO:
      IF avail zsastz THEN
        ASSIGN exceptCode = zsastz.primarykey.
      ELSE
        ASSIGN exceptCode = "".
      HIDE FRAME f-reason.
      APPLY "window-close" TO FRAME f-reason.
    END.
    IF {k-cancel.i} THEN DO:
      ASSIGN exceptCode = "".
      HIDE FRAME f-reason.
      APPLY "window-close" TO FRAME f-reason.
    END.
  END.
  ON cursor-up cursor-up.
  ON cursor-down cursor-down.
  
  OPEN QUERY q-reason FOR EACH zsastz WHERE zsastz.cono = g-cono AND
                               zsastz.codeiden = "FrtFlagsExceptions" AND
                               zsastz.labelfl = NO
  NO-LOCK BY codeiden BY primarykey BY secondarykey.
  CLEAR FRAME f-reason.
  ENABLE b-reason WITH FRAME f-reason.
  APPLY "ENTRY" TO b-reason IN FRAME f-reason.
  DISPLAY inTitle WITH FRAME f-reason.
/*   PUT SCREEN ROW 13 COLUMN 1 "                    ".  */
  WAIT-FOR window-close OF FRAME f-reason.        
  CLOSE QUERY q-reason.
  APPLY "FOCUS" TO oeehb.inbndfrtfl in frame f-oeexqcnt.
  ON cursor-up back-tab.
  ON cursor-down tab.
  
END. /* procedure popup */

/********************************/
PROCEDURE PopUpComm:
/********************************/

  DEFINE VAR inTitle AS CHAR FORMAT "x(20)" NO-UNDO.
  DEFINE OUTPUT PARAMETER v-comment AS CHAR FORMAT "x(20)" NO-UNDO.
  
  
  
  FORM
    inTitle  AT 1 DCOLOR 2  
    v-comment AT 1  
  WITH FRAME f-reason2 WIDTH 20 row 8 NO-LABELS OVERLAY NO-BOX.
  
  assign inTitle = "Comments".
  display inTitle with frame f-reason2.
  UPDATE 
  v-comment with frame f-reason2 
  
  editing:
    READKEY.
    
    IF {k-accept.i} OR {k-return.i} THEN DO:
      assign v-comment = input v-comment.
      IF v-comment <> "" THEN DO:
         CLEAR FRAME F-REASON2.
         HIDE FRAME F-REASON2.
         return.
         /* APPLY "WINDOW-CLOSE" TO FRAME F-REASON2.  */
      END.   
      ELSE do:
        ASSIGN v-comment = "".
        message "Comment Required". pause.
        next-prompt v-comment with frame f-reason2.
        next.
        /* message "Comment Required".   
        CLEAR FRAME F-REASON2.
        HIDE FRAME f-reason2.
        APPLY "window-close" TO FRAME f-reason2.   */
      end. /* else do */
    END.
    IF {k-cancel.i} THEN DO:
      ASSIGN exceptCode = "".
      CLEAR FRAME F-REASON2.
      HIDE FRAME f-reason2.
      return.
      /* APPLY "window-close" TO FRAME f-reason2.    */
    END.
  
  apply lastkey.

  END.
    
    
END. /* procedure popupcomm */





/* -------------------------------------------------------------------------  */
Procedure conversion-marking:
/* -------------------------------------------------------------------------  */
        
  define buffer ix-lines for t-lines.
  define buffer ix-oeelb for oeelb.

  
  enable b-lines with frame f-lines.

/*  find ix-lines where ix-lines.lineno = t-lines.lineno.
  
  assign ix-lines.convertfl = if ix-lines.convertfl = yes then no
                             else if ix-lines.convertfl = no then yes
                             else no.
*/
  assign t-lines.convertfl = if t-lines.convertfl = yes then no
                             else if t-lines.convertfl = no then yes
                             else no.
  find ix-oeelb where
       ix-oeelb.cono        = g-cono and
       ix-oeelb.batchnm     = string(v-quoteno,">>>>>>>9") and
       ix-oeelb.seqno       = 2 and
       ix-oeelb.lineno      = t-lines.lineno no-error.
  if avail ix-oeelb then
    overlay (ix-oeelb.user5,2,3) = if t-lines.convertfl then "yes" else "no ".
  
  hide frame f-statuslinex.
  b-lines:refresh(). 
  display b-lines with frame f-lines.

end.                               

/* ------------------------------------------------------------------------- */ 
Procedure Conversion-Detail: 
/* ------------------------------------------------------------------------- */
  define buffer ix-oeelb for oeelb.
  define buffer ix-oeehb for oeehb.

  def var xi-oeehbrecid as recid no-undo.
  def var xi-linesrecid as recid no-undo.
  def var xi-oeelkrecid as recid no-undo.

  assign xi-linesrecid  = recid(t-lines).
   find ix-oeelb where
        ix-oeelb.cono        = g-cono and
        ix-oeelb.batchnm     = string(v-quoteno,">>>>>>>9") and
        ix-oeelb.seqno       = 2 and
        ix-oeelb.lineno      = v-lineno no-error.

    find ix-oeehb where
         ix-oeehb.cono        = g-cono and
         ix-oeehb.batchnm     = string(v-quoteno,">>>>>>>9") and
         ix-oeehb.seqno       = 2 no-lock no-error.
   
   assign xi-oeehbrecid = recid(ix-oeehb).

   assign ip-update = true.
/*
        put screen row 13 col 3 color 
           message 
 " F7-Line Maintenance   F8-Vendor/Whse                                      ".


*/
   if v-seqno = 0 then do:

     find ix-oeelb where
          ix-oeelb.cono        = g-cono and
          ix-oeelb.batchnm     = string(v-quoteno,">>>>>>>9") and
          ix-oeelb.seqno       = 2 and
          ix-oeelb.lineno      = v-lineno no-error.

     find xt-lines where xt-lines.lineno = ix-oeelb.lineno and
                         xt-lines.seqno  = 0 no-error. 
     assign 
            xt-lines.specnstype = ix-oeelb.specnstype
            xt-lines.descrip    = ix-oeelb.proddesc
            xt-lines.qty        = ix-oeelb.qtyord
            xt-lines.vendno     = ix-oeelb.vendno
            xt-lines.prodcat    = ix-oeelb.prodcat
            xt-lines.prodcost   = ix-oeelb.prodcost
            xt-lines.leadtm     = ix-oeelb.leadtm
            xt-lines.pricetype  = ix-oeelb.pricetype
            xt-lines.lcomment   = substr(ix-oeelb.user4,74,4)
            xt-lines.shipdt     = if substr(ix-oeelb.user4,26,8) = "?       "                                    then
                                    ?
                                 else
                                    date(substr(ix-oeelb.user4,26,8))
            v-surcharge        = ix-oeelb.datccost 
            v-sparebamt        = ix-oeelb.user6
            v-sellprc          = ix-oeelb.price        
            v-specnstype       = ix-oeelb.specnstype  
            v-rushfl           = ix-oeelb.rushfl    
            v-costoverfl       = ix-oeelb.costoverfl 
            orig-cost          = dec(substr(ix-oeelb.user1,2,7))
            over-cost          = dec(substr(ix-oeelb.user1,2,7))
            v-leadtm           = ix-oeelb.leadtm
            zi-icspecrecno     = ix-oeelb.icspecrecno 
            v-prodcat          = ix-oeelb.prodcat 
            v-prodline         = ix-oeelb.prodline    
            v-slsrepout        = ix-oeelb.slsrepout 
            v-prodcost         = ix-oeelb.prodcost  
            v-glcost           = dec(ix-oeelb.user2)
            v-lcomment         = substr(ix-oeelb.user4,74,1).
  
     
     end.

  run Conversion-Loadx.
  readkey pause 0.
  display  
     oeehb.batchnm    
     oeehb.custno     
     oeehb.transtype  
     oeehb.shipto    
     oeehb.whse      
     with frame f-lineconvert.
  b-lines:refresh() in frame f-lines. 
  display b-lines with frame f-lines.


  
end.


/* ------------------------------------------------------------------------- */ 
Procedure Check_Quote_Mortality: 
/* ------------------------------------------------------------------------- */
    define buffer mx-oeelb for oeelb.
    
    find first oeeh where oeeh.cono     = g-cono and
                    oeeh.orderno  = ip-orderno and
                    oeeh.enterdt  = TODAY 
                    no-lock no-error.
    if not avail oeeh then
      do:
      if v-quoteno = ip-orderno then
        do:
        message "Quote did NOT Convert. View error(s) in file "
                + v-qfilename +
                " in OIPF".
        pause.
        assign g-orderno = v-quoteno
               errorfl   = yes.
        for each b-oeelb where 
                 b-oeelb.cono    = g-cono and
                 b-oeelb.batchnm = string(v-quoteno,">>>>>>>9") and
                 b-oeelb.seqno   = 2:
          assign b-oeelb.xxde4 = 0.
        end.
      end. /* if ip-orderno = v-quoteno */
    end. /* not avail oeeh */
    else
      do:
      if can-find(first oeel where   oeel.cono = g-cono and
                                     oeel.orderno = oeeh.orderno and
                                     oeel.ordersuf = oeeh.ordersuf and
                                     oeel.shipprod matches '*COMMISSION*'
                                     no-lock) then
        do:
        /* keep track of the qty converted */
        assign linecount = 0.
        for each bpeh where bpeh.cono = g-cono and
                            bpeh.bpid = oeehb.batchnm + "C" and
                            bpeh.slsrepout <> " "
                            no-lock:
          assign linecount = linecount + 1.
        end.
        if linecount = 0 then
          assign linecount = 1.
        for each oeelb where oeelb.cono    = g-cono and
                             oeelb.batchnm = oeehb.batchnm and
                             oeelb.seqno   = oeehb.seqno and
                             oeelb.specnstype <> "l" and
                      substr(oeelb.user5,2,3) <> "yes"
                             no-lock:
          assign i-lineno = int(truncate((oeelb.lineno / linecount),0)).
          if i-lineno > 0 then
            find first c-oeelb where 
                       c-oeelb.cono     = g-cono + 500 and
                       c-oeelb.batchnm  = oeelb.batchnm and
                       c-oeelb.seqno    = oeelb.seqno and
                       c-oeelb.lineno   = i-lineno and
                       c-oeelb.shipprod = oeelb.proddesc
                       no-error.
          if avail c-oeelb then
            do:
            assign c-oeelb.xxde4 = oeelb.qtyord.
            if c-oeelb.qtyord - c-oeelb.xxde4 = 0 then
              assign c-oeelb.specnstype = "l"
                     c-oeelb.reasunavty = "CO".
          end. /* avail c-oeelb */
        end. /* each oeelb in g-cono */
        find first b-oeelb where b-oeelb.cono = g-cono + 500 and
                                 b-oeelb.batchnm = oeehb.batchnm and
                                 b-oeelb.seqno   = oeehb.seqno and
                                 b-oeelb.specnstype <> "l"
                                 no-lock no-error.
        if not avail b-oeelb then
          do:
          /* cancel the commission quote - quantities have been exhausted */
          for each b-oeehb where b-oeehb.cono = g-cono + 500 and
                                 b-oeehb.batchnm = oeehb.batchnm and
                                 b-oeehb.seqno   = oeehb.seqno:
            assign b-oeehb.stagecd = 9.
            overlay (b-oeehb.user3,53,4) = "CQ".
          end.
        end. /* all quantites have been exhausted */
      end. /* found a converted line */
    end. /* oeeh exists and we had a successful conversion */
    
    assign w-qtyremain = 0.
    for each b-oeelb where 
             b-oeelb.cono    = g-cono + 500 and
             b-oeelb.batchnm = string(v-quoteno,">>>>>>>9") and
             b-oeelb.seqno   = 2 and
             b-oeelb.specnstype <> "L"
             no-lock:
      assign w-qtyremain = w-qtyremain + (b-oeelb.qtyord - b-oeelb.xxde4).
    end.
              
    if inquiryfl = no then
      do:
      if w-qtyremain > 0 then
        do:
        message "Quantities still exist. This quote will remain open"
          view-as alert-box.
      end.
      else
        do:
        assign v-question = no.
        /*
        message  "Do you want to keep this quote Open ?"         
          view-as alert-box question buttons yes-no            
        update v-question.                                 
        if v-question or {k-cancel.i} then
          do:
          return.
        end.
        */
        find first b-oeehb where 
                   b-oeehb.cono    = g-cono and
                   b-oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
                   b-oeehb.seqno   = 2 no-error.
        if avail b-oeehb then
          do:
          for each mx-oeelb where mx-oeelb.cono = g-cono and
                                  mx-oeelb.batchnm = b-oeehb.batchnm and
                                  mx-oeelb.seqno = b-oeehb.seqno:
            assign mx-oeelb.specnstype = "l"
                   mx-oeelb.reasunavty = if b-oeehb.enterdt = TODAY then
                                           "CR  "
                                         else
                                           "CQ  ".
          end.
        end. /* avail b-oeehb */
  
        if avail b-oeehb then 
          do:
          assign b-oeehb.stagecd = 9.
          overlay (b-oeehb.user3,53,4) = if b-oeehb.enterdt = TODAY then
                                           "CR  "
                                         else
                                           "CQ  ".
          assign b-oeehb.canceldt = TODAY.          /* das - 05/21/09 */
        end. /* avail b-oeehb */
      end. /* question alert box */
    end. /* inquiryfl = no */

end. /* Procedure Check_Quote_Mortality */


/* ------------------------------------------------------------------------- */ 
Procedure Check_Quote_Restore: 
/* ------------------------------------------------------------------------- */
/* Retrieve Original Quote from Backup  (in Conversion procedure)           */
   if x-backed-quote-up then do:
     find first b-oeehb where b-oeehb.cono = g-cono and  
                b-oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
                b-oeehb.seqno = 2 and
                b-oeehb.sourcepros = "ComQu"
                no-lock no-error.          
     /*
     run oeexqrelx.p (input "Restore",
                      input recid(b-oeehb),
                      output x-error). 
     */
     if x-error then
       do:
       message "Fatal Preparation error". pause 5.
       assign s-canceled = true.
     end.
     else
       assign x-backed-quote-up = false.
     
   end.
end.
  
