/* SX 55 */
/* oeetg.p 1.3 03/13/98 */
/*h****************************************************************************
  PROCEDURE      : oeetg
  DESCRIPTION    : OE print parameters
  AUTHOR         : rhl
  DATE WRITTEN   : 08/12/89
  CHANGES MADE   :
    05/05/92 pap; TB# 6307/6516 Do not allow order on hold to print from the
        suspended window
    06/05/92 pap; TB# 6888  Do not check validity of printer name unless the
        print flag is set to yes
    06/15/92 mkb, TB# 2812  Allow print of canceled invoices
    06/22/92 mkb; TB# 4531  Problem with invoice count
    11/13/92 mms; TB# 8669  Rush enhancement
    02/04/93 rhl; TB# 5157  Print uses OEAOB commit inventory
    03/03/93 sdc; TB# 10245 DNet Control Path
    03/05/93 rhl; TB# 10294 F10 fax printing of Pck tckt
    03/24/93 kmw; TB# 10457 Tendering doesn't open drawer
    08/06/93 mwb; TB# 12527 Added Setup Up to Set Up
    08/12/93 rhl; TB# 10718 Allow *whse* in printer device
    10/28/93 rhl; TB# 13391 **Pipe to subprocess has been broken
    11/18/93 rs;  TB# 13720 Pick Ticket Print problem
    02/23/94 mwb; TB# 14928 Scoping problem on pick tickets
    04/07/94 mwb; TB# 15297/15298 Setting of the global whse based on the oeeh
        record, not carried in g-
    05/27/94 dww; TB# 15811 Add OERD Security to Invoice Printing
    11/08/94 rs;  TB# 16974 Need > 2 comment lines for VSIFAX
    11/18/94 jrb; TB# 17133 Added user include  &user_001
    11/20/94 mms; TB# 14261 Added Ship Complete packing slip logic
    12/09/94 rs;  TB# 15970 Vsifax problem
    01/10/95 dww; TB# 17221 Position of suffix is wrong.
    02/23/95 mtt; TB# 15314 Promotional msg not printing(stored rpt)
    04/18/95 kjb; TB# 17702 Add Service Warranty options to OERD
    05/30/95 dww; TB# 17288 Unix Error in On-Line Doc.  Fax Frame needs to be
        corrected.
    07/17/95 tdd; TB# 18866 Proper fields not prompted on error
    07/19/95 dww; TB# 17288 Unix Error in On-Line Doc.  Frame needs to display
        before Put Screen is done.
    09/15/95 dww; TB# 19314 Remove Imbedded spaces from the Fax Phone Number
    10/02/95 mcd; TB# 19437 Add User Include to oeetg.p (&user_b4print)
    12/04/95 tdd; TB#  6874 Check security on F10 Print
    12/19/95 sbl; TB# 19803 (CGA) Force SAPB Background Flag if set in SASSR.
    01/16/96 vlp; TB# 19803 (CGA) Report Scheduler - add include to create
        RSES record if run in background.
    04/25/96 tdd; TB# 21053 Problems w/alpha custno compile
    09/13/96 mes; TB# 21702 Trend 8.0 oper2 enhancement - g-operinit and ""
        were removed from the parameter list for w-sapbo.i and w-sapb.i
    09/13/96 dww; TB# 21702 Trend 8.0 Enhancement - remove oper2
    12/12/96 jsh; TB# 22162 Added user hook (&user_002)
    11/25/97 rh;  TB# 24224 Added user hook &after_assign
    02/03/98 sbr; TB# 20968 ARSSO "To:" field is not honored when faxing
    03/12/98 mtt; TB# 24049 PV: Common code use for GUI interface
    12/15/98 lbr; TB# 25658 Added user hook &user_after_main
    07/20/98 sbr; TB# 25541 F10 - F10 Fax# defaulting from ARSC
    10/21/99 abe; TB# 26766 Added user hooks &user_defvar,&user_security,
                                             &user_prtval
    01/11/00 sbr; TB# 26767 F10 - Fax # not defaulting using ARSE
    02/02/01 mjg; TB# e7216 Added e-mail as printer device.
    11/26/02 rgm; TB# e15146 Modifications for rxserver print options
    12/01/03 bm;  TB# e17860 Printers do not default correctly in oeet.
    12/03/03 rgm; TB# e18210 Don't run OEET/POET e-mail in background
    03/29/04 blw; TB# L1464 - Do not overwrite printer for "A"uto print.
    04/30/04 sbr; TB# e19404 F10 print, order type not defaulting back
    05/05/04 sbr; TB# e16575 SASO email address defaults in TO: field
    05/06/04 rgm; TB# e19893 Allow demand print to use EDI conditionally
    01/17/05 jbt; TB# 21580 Add user hook.
******************************************************************************/

{g-all.i}
{g-rptctl.i "new"}
{g-ic.i}
{g-oe.i}
{g-print.i}

/* A-Auto Print D-Display P-Pick */
def input param v-type              as c format "x(1)"              no-undo.
/* A-Auto Pick Ticket (y/n)      */
def input param v-pick              as c format "x(1)"              no-undo.

def     shared var v-idoeeh         as recid                        no-undo.
def     shared var v-oercptfrmt     as i                            no-undo.
def     shared var v-oelinefl       like sasc.oelinefl              no-undo.

def            var s-autoprintfl    as logical format "yes/no"      no-undo.
def            var s-whereappfl     as logical format "yes/no"      no-undo.
def            var s-invprintfl     as logical format "yes/no"      no-undo.
def            var s-ackprintfl     as logical format "yes/no"      no-undo.
def            var s-pickprintfl    as logical format "yes/no"      no-undo.
def            var s-rcptprintfl    as logical format "yes/no"      no-undo.
def            var s-printernmpck   as c format "x(10)"             no-undo.
def            var s-printernmack   as c format "x(10)"             no-undo.
def            var s-printernminv   as c format "x(10)"             no-undo.
def            var s-printernmrcpt  as c format "x(10)"             no-undo.
def            var s-promomsg       like sapb.rpttitle              no-undo.
def            var v-ackokfl        as l format "yes/no"            no-undo.
def            var v-pckokfl        as l format "yes/no"            no-undo.
def            var v-invokfl        as l format "yes/no"            no-undo.
def            var s-printer        as c format "x(10)"             no-undo.
def            var v-ackptype       like sasp.ptype                 no-undo.
def            var v-invptype       like sasp.ptype                 no-undo.
def            var v-extent         as i                            no-undo.
/*tb 15297/15298 04/07/94 mwb; added v-custno, v-shipto to carry forward */
def            var v-custno         like oeeh.custno                no-undo.
def            var v-shipto         like oeeh.shipto                no-undo.
def            var cReportPrefix    as c                            no-undo.

def buffer b-oeeh       for oeeh.
def buffer b-sapb       for sapb.
def buffer pv_user      for pv_user.

/*u* User Hook*/
{oeetg.z99 &user_defvar = "*"}

/*tb 15970 12/09/94 rs; VSIFAX problem */
{valmsg.gas &string = "4035,4052"}

{f-oeetg.i}

/*tb 17288 06/16/95 dww; Unix Error in On-Line Doc. */
{f-fax.i    &proc   = "oeetg"
            &pre    = "v-"}

{f-email.i  &proc   = "oeetg"
            &pre    = "v-"
            &comvar = "/*"}

/*tb e17860 12/01/03 bm; Printers do not default correctly in oeet.*/
/*tb L1464  03/29/04 blw; Auto print has printer already specified.*/
/*d Load the Default document printers */
if g-oeprtloadedfl = no and
    v-type <> "A" then
do for sasoo:
    g-oeprtloadedfl = yes.

    {w-sasoo.i g-operinits no-lock}
    if avail sasoo then do:
        assign
            g-printernmrcpt = if sasoo.rprinternm <> "" then
                                  sasoo.rprinternm
                              else g-printernmrcpt.

        if sasoo.whse <> "" then do for icsd:
            {w-icsd.i sasoo.whse no-lock}
            if avail icsd then
            assign
                g-printernmpck = if icsd.printernm[1] <> "" then
                                     icsd.printernm[1]
                                 else g-printernmpck
                g-printernminv = if icsd.printernm[2] <> "" then
                                     icsd.printernm[2]
                                 else g-printernminv
                g-printernmack = if icsd.printernm[3] <> "" then
                                     icsd.printernm[3]
                                 else g-printernmack.
        end. /*sasoo.whse <> "" */
    end. /* avail sasoo */

    if g-orderno <> 0 then do for oeeh:
        {w-oeeh.i g-orderno g-ordersuf no-lock}
        if avail oeeh then do for icsd:
            {w-icsd.i oeeh.whse no-lock}
            if avail icsd then
            assign
                g-printernmpck = if icsd.printernm[1] <> "" then
                                     icsd.printernm[1]
                                 else g-printernmpck
                g-printernminv = if icsd.printernm[2] <> "" then
                                     icsd.printernm[2]
                                 else g-printernminv
                g-printernmack = if icsd.printernm[3] <> "" then
                                     icsd.printernm[3]
                                 else g-printernmack.
        end.  /* avail oeeh */
    end.  /* g-orderno <> 0 */

end.  /* g-oeprtloadedfl = no */

/*o Initialize all the global F10 print variables */
assign
    s-autoprintfl   = g-autoprintfl
    s-whereappfl    = g-whereappfl
    s-invprintfl    = g-invprintfl
    s-ackprintfl    = g-ackprintfl
    s-pickprintfl   = g-pickprintfl
    s-rcptprintfl   = g-rcptprintfl
    s-printernmpck  = g-printernmpck
    s-printernmack  = g-printernmack
    s-printernminv  = g-printernminv
    s-printernmrcpt = g-printernmrcpt
    s-promomsg      = g-promomsg.

/*u User Include*/
{oeetg.z99 &after_assign = "*"}

pause 0 before-hide.

/*tb 18866 07/17/95 tdd; Proper fields not prompted on error */
next-prompt s-pickprintfl with frame f-oeetg.

if v-type = "D"
then

/*tb 14928 02/23/94 mwb; added oeeh, sasp,sassr, arsc, arss, sasc - scoping */
main:
do for oeeh, sasp, sassr, arsc, arss, sasc while true with frame f-oeetg
    on endkey undo main, leave main on error undo main, leave main:

    view frame f-oeetg.

    put screen row 17 column 3 color messages
"                                              F9-E-Mail Addr  F10-Fax #      ".

    /*d Check to see if the operator has security to print an invoice */
    /*tb 6874 12/04/95 tdd; Check security on F10 Print */
    assign
        v-ackokfl = no
        v-pckokfl = no
        v-invokfl = no.

    /*tb 6874 12/04/95 tdd; Check security on F10 Print */
    do for sassm, sasos:
        {w-sassm.i ""oeepa"" no-lock}
        if avail sassm then do:
            if sassm.securpos = 0 then v-ackokfl = yes.
            else do:
                {w-sasos.i g-operinits sassm.menuproc no-lock}

                if avail sasos and sasos.securcd[sassm.securpos] > 2
                    then v-ackokfl = yes.
            end.

        end.

        {w-sassm.i ""oeepp"" no-lock}
        if avail sassm then do:
            if sassm.securpos = 0 then v-pckokfl = yes.
            else do:
                {w-sasos.i g-operinits sassm.menuproc no-lock}

                if avail sasos and sasos.securcd[sassm.securpos] > 2
                    then v-pckokfl = yes.
            end.

        end.

        {w-sassm.i ""oerd"" no-lock}
        if avail sassm then do:
            if sassm.securpos = 0 then v-invokfl = yes.
            else do:
                {w-sasos.i g-operinits sassm.menuproc no-lock}

                /*tb 15811 05/27/94 dww; Add OERD Security to Invoice Printing.
                    Increased the sasos check to be > 2; not > 1.*/
                if avail sasos and sasos.securcd[sassm.securpos] > 2
                    then v-invokfl = yes.
            end.

        end.

        /*u* User Hook*/
        {oeetg.z99 &user_security = "*"}

    end.

    /*tb 6874 12/04/95 tdd; Check security on F10 Print */
    if v-ackokfl = no then do:

        s-ackprintfl = no.

        display
            s-ackprintfl
            s-printernmack
        with frame f-oeetg.
    end.

    if v-pckokfl = no then do:

        s-pickprintfl = no.

        display
            s-pickprintfl
            s-printernmpck
        with frame f-oeetg.
    end.

    /*tb 15811 05/27/94 dww; Add OERD Security to Invoice Printing */
    if v-invokfl = no then do:

        /*d Disallow printing an invoice */
        s-invprintfl = no.

        display
            s-invprintfl
            s-printernminv
        with frame f-oeetg.
    end. /* if v-invokfl = no */

    /*tb 10718 08/12/93 rhl; Allow *whse* in printer device */
    /*o Allow entry of the order to print and which document(s) to print */
    /*tb 6874 12/04/95 tdd; Check security on F10 Print */
    update
        g-orderno
        g-ordersuf
        s-ackprintfl    when v-ackokfl
        s-printernmack  when v-ackokfl
        s-pickprintfl   when v-pckokfl
        s-printernmpck  when v-pckokfl
        s-invprintfl    when v-invokfl
        s-printernminv  when v-invokfl
        s-rcptprintfl
        s-printernmrcpt
        s-promomsg
        s-autoprintfl   when g-ourproc ne "oees"
        s-whereappfl
    go-on(f9 f10) with frame f-oeetg editing:
        readkey.

        if {k-after.i} or {k-func.i} then do:
            if input g-orderno <> g-orderno then v-faxphoneno = "".
            assign
                g-orderno
                g-ordersuf.
        end.
        apply lastkey.
    end.

    /*d Do not allow auto print of receipts */
    if  s-rcptprintfl   = yes   and
        s-invprintfl    = no    and
        s-pickprintfl   = no    and
        s-ackprintfl    = no    and
        s-autoprintfl   = yes
    then do:
        run err.p(5887).
        next-prompt s-rcptprintfl.
        next.
    end.

     {oeetg.z99 &user_aftupdate = "*"} /* SX 55 */
                                 
                                 
    /*d Validate the receipt's printer */
    /*tb 6888 6/5/92 pap; Add check if receipt print flag is set to yes */
    if s-printernmrcpt ne "" and s-rcptprintfl = yes
    then do:
        {w-sasp.i s-printernmrcpt no-lock}
        if not avail sasp
        then do:
            run err.p(4035).
            next-prompt s-printernmrcpt.
            next.
        end.

        if not can-do("r,d",sasp.ptype) or s-printernmrcpt = "E-Mail":u
        then do:
            run err.p(5882).
            next-prompt s-printernmrcpt.
            next.
        end.

        if sasp.ptype = "v"
        then do:
            run err.p(5000).
            next-prompt s-printernmrcpt.
            next.
        end.

    end.    /* receipt printer name validation */

    /*d Validate the pick ticket printer */
    /*tb 6888 06/05/92 pap; Add check if pick ticket print flag is set to yes */
    /*tb 10718 08/12/93 rhl; Allow *whse* in printer device */

    if s-printernmpck ne "" and s-pickprintfl = yes and
       s-printernmpck ne "*whse*"
    then do:
        {w-sasp.i s-printernmpck no-lock}
        if not avail sasp then do:
            run err.p(4035).
            next-prompt s-printernmpck.
            next.
        end.

        /* check printer type validation */
        {rxcheck.lpr
            &sasp     = sasp
            &currproc = """oeepp"""
            &prompt_clause = s-printernmpck
            &printernm = s-printernmpck
            &display_clause = "display s-printernmpck." }

        if can-do("v",sasp.ptype) then do:
            run err.p(5000).
            next-prompt s-printernmpck.
            next.
        end.

        /*tb 10294 03/05/93 rhl; F10 fax printing of Orders */
        if can-do("f",sasp.ptype) or s-printernmpck = "E-Mail":u then do:
            run err.p(1134).
            next-prompt s-printernmpck.
            next.
        end.

    end.    /* Pick ticket printer validation */

    /*d Validate the invoice printer */
    /*tb 6888 6/5/92 pap; Add check if invoice print flag is set to yes */
    if s-printernminv ne "" and s-invprintfl = yes
    and s-printernminv ne "e-mail"  /* SX 55 */
    
    then do:
        {w-sasp.i s-printernminv no-lock}
        if not avail sasp
        then do:
            run err.p(4035).
            next-prompt s-printernminv.
            next.
        end.

        {rxcheck.lpr
            &sasp     = sasp
            &currproc = """oeepi"""
            &prompt_clause = s-printernminv
            &printernm = s-printernminv
            &display_clause = "display s-printernminv." }

        if sasp.ptype = "v"


        then do:
            run err.p(5000).
            next-prompt s-printernminv.
            next.
        end.

        v-invptype = sasp.ptype.
    end.    /* Invoice printer name validation */

    /*d Validate the acknowledgement printer */
    /*tb 6888 6/5/92 pap; Add check if acknowledgmt print flag is set to yes */
    if s-printernmack ne "" and s-ackprintfl = yes
    then do:
        {w-sasp.i s-printernmack no-lock}
        if not avail sasp then do:
            run err.p(4035).
            next-prompt s-printernmack.
            next.
        end.

        {rxcheck.lpr
            &sasp     = sasp
            &currproc = """oeepa"""
            &prompt_clause = s-printernmack
            &printernm = s-printernmack
            &display_clause = "display s-printernmack." }

        if sasp.ptype = "v" then do:
            run err.p(5000).
            next-prompt s-printernmack.
            next.
        end.

        v-ackptype = sasp.ptype.
    end.    /* Acknowledgement printer name validation */

    /*u* User Hook*/
    {oeetg.z99 &user_prtval = "*"}

    if v-ackptype ne "f" and v-invptype ne "f" and {k-func10.i}
    then do:
        run err.p(6338).
        next-prompt s-printernmack.
        next.
    end.

    else if not s-ackprintfl and not s-invprintfl and {k-func10.i}
    then do:
        run err.p(6337).
        next-prompt s-ackprintfl.
        next.
    end.

    /*o Demand print an order if it is requested */
    if g-orderno > 0
    then do:
        {w-oeeh.i g-orderno g-ordersuf no-lock}

        /*d Make sure the order is valid */
        if not avail oeeh then do:
            run err.p(4605).
            next-prompt g-orderno.
            next.
        end.

        /*tb 19437 10/02/95 mcd; Add User Include to oeetg.p */
        {oeetg.z99 &user_b4print = "*"}

        /*tb 10245 03/03/93 sdc*/
        if s-pickprintfl then
            {e-dnctrl.i &file = "oeeh"
                        &next = "next-prompt s-pickprintfl. next"}

        /*o Only print the order if it is not in use by anyone */
        if oeeh.openinit ne "" then do:
            message "Order in Use by " oeeh.openinit + "(5608)".
            bell.
            next-prompt g-orderno.
            next.
        end.

        /*d Don't allow printing a pick ticket for a shipped order.  Also
            check to ensure the type of order and stage is valid for picking,
            and that the order has been approved */
        if s-pickprintfl then do:
            if oeeh.stagecd > 3
            then do:
                run err.p(5815).
                next-prompt g-orderno.
                next.
            end.

            if can-do("cr,qu,bl,st,fo,do",oeeh.transtype) or oeeh.stagecd = 0
            then do:
                run err.p(5819).
                next-prompt g-orderno.
                next.
            end.

            if oeeh.approvty ne "y" then do:
                run err.p(5838).
                next-prompt g-orderno.
                next.
            end.

        end.    /* if printpickfl is set */

        /*d Don't allow printing an invoice or receipt for an invoiced order */
        if (s-invprintfl or s-rcptprintfl) and oeeh.stagecd > 5
        then do:
            run err.p(5816).
            next-prompt g-orderno.
            next.
        end.

        /*tb 10457 03/24/93 kmw; add BR and DO */
        /*d Only allow receipt printing for certain orders */
        if s-rcptprintfl and not can-do("cr,cs,so,rm,ra,br,do",oeeh.transtype)
        then do:
            run err.p(5819).
            next-prompt g-orderno.
            next.
        end.

        /* F9 print to e-mail */
        if {k-func9.i} then do:

          if not v-faxphoneno begins "EM:" then do:
             for first pv_user fields(email) where
                       pv_user.cono  = g-cono and
                       pv_user.oper2 = g-operinit
                       no-lock:
               assign
                   v-faxphoneno = pv_user.email.
             end.

             if avail oeeh then do:
                 {w-arsc.i oeeh.custno no-lock}
                 if avail arsc and arsc.email <> "" then
                     v-faxphoneno = arsc.email.
                 if oeeh.shipto ne "" then do:
                     {w-arss.i oeeh.custno oeeh.shipto no-lock}
                     if avail arss and arss.email <> "" then
                         v-faxphoneno = arss.email.
                 end.

                 {oeetg.z99 &user_email_default = "*"}

             end.

          end. /* if not v-faxphoneno */
/* Don't need  SX 55
          {p-emailparm.i
             &pre   = "v-"
             &proc  = "oeetg"
             &addit = "if ~{k-cancel.i~} then
                          next-prompt g-orderno.
                       next."}
*/   
        end. /* if k-func9.i */

        /*tb 13497 10/27/93 rhl; F10 print to fax: Pipe to Subprocess */
        else if {k-func10.i} then do:
            {w-sasc.i no-lock}
            {w-arsc.i oeeh.custno no-lock}
            if not avail arsc then do:
                run err.p(4303).
                next-prompt g-orderno.
                next.
            end.

            v-extent = if s-ackprintfl then 2 else 3.

            if oeeh.shipto ne "" then
                {w-arss.i oeeh.custno oeeh.shipto no-lock}

            /* tb 20968 02/03/98 sbr; check arsso "To:" field when faxing */
            /*tb 25541 07/20/99 sbr; Use "((s-invprintfl and arss.invtofl) or
                not s-invprintfl)" instead of "arss.invtofl" in if condition. */
            /*tb 26767 01/11/00 sbr; Use "((s-invprintfl and (arss.invtofl or
                (arss.einvtype ne "" and arss.einvto = false))) or
                (s-ackprintfl and arss.eackto = false))" instead of
                "((s-invprintfl and arss.invtofl) or not s-invprintfl)" */
            if oeeh.shipto ne "" and avail arss and
                ((s-invprintfl and (arss.invtofl or (arss.einvtype ne "" and
                 arss.einvto = false))) or
                (s-ackprintfl and arss.eackto = false))
            then do:
                {p-fxardf.i &file   = "arss."
                            &extent = v-extent}
            end.

            else do:
                {p-fxardf.i &file   = "arsc."
                            &extent = v-extent}
            end.

            /*u User Hook just prior to p-fxparm.i */
            {oeetg.z99 &user_002 = "*"}

            /*tb 16974 11/08/94 rs; Need > 2 comment lines */
            /*tb 17288 06/16/95 dww; Unix Error in On-Line Doc. */
            {p-fxparm.i &proc = "oeetg"
                        &pre  = "v-"}

            if {k-cancel.i} or {k-jump.i} then leave main.

        end. /* if {k-func10.i} */

    end. /* if order number is greater than zero */

    /* force open e-mail dialog if empty */
    if (s-printernmack = "E-Mail" or s-printernminv = "E-Mail") and
         not v-faxphoneno begins "EM:":u then do:

       for first pv_user fields(email) where
                 pv_user.cono  = g-cono and
                 pv_user.oper2 = g-operinit
                 no-lock:
         assign
             v-faxphoneno = pv_user.email.
       end.

       if avail oeeh then do:
           {w-arsc.i oeeh.custno no-lock}
           if avail arsc and arsc.email <> "" then
               v-faxphoneno = arsc.email.
           if oeeh.shipto ne "" then do:
               {w-arss.i oeeh.custno oeeh.shipto no-lock}
               if avail arss and arss.email <> "" then
                   v-faxphoneno = arss.email.
           end.
       end.
/* SX 55
       {p-emailparm.i
           &pre   = "v-"
           &proc  = "oeetg"
           &addit = "if ~{k-cancel.i~} then
                        next-prompt g-orderno.
                     next."}
 */
    end.

    /*o Set the global printing variables from screen entry */
    assign
        g-autoprintfl   = s-autoprintfl
        g-whereappfl    = s-whereappfl
        g-invprintfl    = s-invprintfl
        g-ackprintfl    = s-ackprintfl
        g-pickprintfl   = s-pickprintfl
        g-rcptprintfl   = s-rcptprintfl
        g-printernmpck  = s-printernmpck
        g-printernmack  = s-printernmack
        g-printernminv  = s-printernminv
        g-printernmrcpt = s-printernmrcpt
        g-promomsg      = s-promomsg.

    if g-orderno = 0 or not (g-pickprintfl = yes or g-invprintfl = yes or
       g-ackprintfl = yes or g-rcptprintfl = yes)
    then do:
        hide frame f-oeetg no-pause.
        return.
    end.

    leave main.
end.    /* main */

if not {k-jump.i} then hide frame f-oeetg no-pause.


/*o If an order number has not been entered, return to the calling program */
if g-orderno = 0 or {k-endkey.i} or {k-jump.i} then return.

if g-printernmack = 'E-MAIL':u or g-printernminv = 'E-MAIL':u then
  assign
      cReportPrefix = "oeet@":u.
else
  assign
      cReportPrefix = "".

/*u* User Hook*/
{oeetg.z99 &user_after_main = "*"}

/*tb 24049 03/12/98 mtt; PV: Common code use for GUI interface */
{oeetg.lpr &runtype       = "v-type"
           &runpick       = "v-pick"
           &orderno       = "g-orderno"
           &ordersuf      = "g-ordersuf"
           &report-prefix = cReportPrefix
           &whseprinter   = "s-printer"
           &pickprintfl   = "g-pickprintfl"
           &printernmpck  = "g-printernmpck"
           &invprintfl    = "g-invprintfl"
           &printernminv  = "g-printernminv"
           &promomsg      = "g-promomsg"
           &custno        = "v-custno"
           &shipto        = "v-shipto"
           &ackprintfl    = "g-ackprintfl"
           &printernmack  = "g-printernmack"
           &rcptprintfl   = "g-rcptprintfl"

           &invsapbassigns = "if g-printernminv = 'E-MAIL':u then
                               assign                                                                             sapb.inusecd     = "'n'"
                                   sapb.demandfl    = yes
                                   sapb.filefl      = yes
                                   sapb.delfl       = yes
                                   sapb.faxphoneno  = v-faxphoneno
                                   sapb.faxcom[1]   = v-faxcom[1]
                                   sapb.faxcom[2]   = v-faxcom[2]
                                   sapb.faxcom[3]   = v-faxcom[3]
                                   sapb.faxcom[4]   = v-faxcom[4]
                                   sapb.faxcom[5]   = v-faxcom[5]
                                   sapb.faxcom[6]   = v-faxcom[6]
                                   sapb.faxcom[7]   = v-faxcom[7]
                                   sapb.faxcom[8]   = v-faxcom[8]
                                   sapb.faxcom[9]   = v-faxcom[9]
                                   sapb.faxcom[10]  = v-faxcom[10]
                                   sapb.optvalue[8] = "'no'"."

           &acksapbassigns = "if g-printernmack = 'E-MAIL':u then
                               assign                                                                             sapb.inusecd     = "'n'"
                                   sapb.demandfl    = yes
                                   sapb.filefl      = yes
                                   sapb.delfl       = yes
                                   sapb.faxphoneno  = v-faxphoneno
                                   sapb.faxcom[1]   = v-faxcom[1]
                                   sapb.faxcom[2]   = v-faxcom[2]
                                   sapb.faxcom[3]   = v-faxcom[3]
                                   sapb.faxcom[4]   = v-faxcom[4]
                                   sapb.faxcom[5]   = v-faxcom[5]
                                   sapb.faxcom[6]   = v-faxcom[6]
                                   sapb.faxcom[7]   = v-faxcom[7]
                                   sapb.faxcom[8]   = v-faxcom[8]
                                   sapb.faxcom[9]   = v-faxcom[9]
                                   sapb.faxcom[10]  = v-faxcom[10]
                                   sapb.optvalue[8] = "'no'"."


           }

do for sasoo:
    {w-sasoo.i g-operinit no-lock}
    g-oetype = if avail sasoo and sasoo.oetrntype ne ""
               then sasoo.oetrntype
               else "SO".

end.
