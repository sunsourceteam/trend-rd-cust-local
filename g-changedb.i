/*
define temp-table zsdioeellog no-undo
 Field cono             like oeeh.cono
 Field custno           like arsc.custno
 Field oslsrepout       like smsn.slsrep
 Field slsrepout        like smsn.slsrep
 Field oslsrepin        like smsn.slsrep
 Field slsrepin         like smsn.slsrep
 Field oregdist         as char format "x(4)"
 Field regdist          as char format "x(4)"
 Field orderno          like oeeh.orderno
 Field ordersuf         like oeeh.ordersuf
 Field lineno           like oeel.lineno
 Field shipprod         like oeel.shipprod
 Field oqtyord          like oeel.qtyord
 Field qtyord           like oeel.qtyord
 Field oprice           like oeel.price
 Field price            like oeel.price
 Field oreqshipdt       like oeeh.reqshipdt
 Field reqshipdt        like oeeh.shipdt    
 Field lostbusty        like oeeh.lostbusty

 Field ActualTime       as integer
 Field ActualDate       as date
 
 Field operinit         like oeeh.operinit
 Field transproc        like oeeh.transproc
 Field transdt          like oeeh.transdt
 Field transtm          like oeeh.transtm

 Index k-ord
       cono
       orderno
       ordersuf
       lineno
       ActualDate
       ActualTime.

*/