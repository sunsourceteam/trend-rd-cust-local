/* z-sapbozload

   Load custom sapbo records into temp-table.

*/

display "Selection Criteria List" with frame zygot1.
display " " with frame zygot2.

for each sapbo where 
   sapbo.cono = g-cono and
   sapbo.reportnm = sapb.reportnm exclusive-lock:

  
  if sapbo.user1 = "c" then
    do:
    zelection_matrix[2] = true.
    create zsapbo.
    assign zsapbo.selection_type = sapbo.user1 
           zsapbo.selection_char4 = ""
           zsapbo.selection_cust = sapbo.custno.
    display "Type - " at 1
               zsapbo.selection_type at 9
            "Selection - "   at 12
               zsapbo.selection_cust at 25 with frame zygot3
               no-box no-underline no-labels .
    if sapb.delfl = true then
      delete sapbo.
    end.
  else
    do:
    if sapbo.user1 = "s" then
       zelection_matrix[1] = true.
    else   
    if sapbo.user1 = "p" then
       zelection_matrix[3] = true.
    else
    if sapbo.user1 = "x" then
       zelection_matrix[4] = true.
    else
    if sapbo.user1 = "i" then
       zelection_matrix[6] = true.
    else
     if sapbo.user1 = "b" then
       zelection_matrix[5] = true.

    create zsapbo.
    assign zsapbo.selection_type = sapbo.user1 
           zsapbo.selection_char4 = sapbo.prodcat
           zsapbo.selection_cust = 0.
    display "Type - " at 1
               zsapbo.selection_type at 9
            "Selection - "   at 12
               zsapbo.selection_char4 at 25 with frame zygot4
             no-box no-underline no-labels .
    if sapb.delfl = true then
      delete sapbo.
    end.
end.