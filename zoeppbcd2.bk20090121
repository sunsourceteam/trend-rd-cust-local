/* zoeppbcd2.p

   01/18/2008 dkt modified to use new barcode 2000.
   This label only finds oeeh - all other variables are null.

   02/13/2008 dkt
   This prints PACK type labels zsdibarcd
   
   uses: 
   pickTempTable.i
   createLabelName.p
   pickbcdtt.i
*/

define input parameter zp-recid as recid.
define input parameter zp-reprint as c format "x".
define input parameter zp-printernm like sasp.printernm.

{g-all.i}
{g-rptctl.i}
{g-oe.i}
def var x-inx as integer                    no-undo.
def var v-pono like oeeh.orderno            no-undo.
def var v-posuf like oeeh.ordersuf          no-undo.
def var v-poline like oeel.lineno           no-undo.
def var v-filename as c format "x(30)"      no-undo.
def var v-tilda as c format "x" init "~~"   no-undo.
def var v-custprod like  poel.shipprod      no-undo.
def var v-printername as c                  no-undo.
def var v-labelname as c                    no-undo.

def var v-outputfl  as logical              no-undo.
def var v-outputofl  as logical              no-undo.
def var v-quantity  like oeel.qtyord        no-undo. 
def var x-quantity  like oeel.qtyord        no-undo. 
def var x-labels    as integer              no-undo.
def var z-labels    as integer              no-undo.
def var v-icswbinloc1 as char format "x(10)" no-undo.
def var v-icswbinloc2 as char format "x(10)" no-undo.

def stream bcd.

find oeeh where recid(oeeh) = zp-recid no-lock no-error.

/* start new barcodes */
{pickTempTable.i}

def var newtype as logical initial no no-undo.
if avail oeeh then do:
  find zsdibarcd where
    zsdibarcd.cono = g-cono and
    zsdibarcd.custno = oeeh.custno and
    zsdibarcd.apptype = "pack" and
    zsdibarcd.labelnm ne ""
    no-lock no-error.
  if avail zsdibarcd then do:
    if zsdibarcd.style = "new" then do:
      assign newtype = yes
             v-printername = zsdibarcd.printernm[1]
             v-labelname   = zsdibarcd.labelnm.
            /*  v-whsechk     = zsdibarcd.whse. */
    end.
  end.
end. /* avail oeeh */


assign v-outputofl = false. 

if not newtype then do:
  if zp-printernm = "" then do:
    for each notes use-index k-notes
      where notes.cono = g-cono and 
      notes.notestype = "zz" and
      notes.primarykey = "barcode" and
      notes.secondarykey = " " no-lock:
  
      do x-inx = 1 to 16:
        if notes.noteln[x-inx] <> " " then do:
          if oeeh.whse = entry(1,notes.noteln[x-inx],",") and
          entry(6,notes.noteln[x-inx],",") = "PACK" then do:
            zp-printernm = entry(2,notes.noteln[x-inx],",").
            v-labelname = "pack" + entry(3,notes.noteln[x-inx],",") + ".lbl".
          end.
        end.
      end.       
    end.
    v-printername = zp-printernm.
  end.
  else do:
    for each notes use-index k-notes
      where notes.cono = g-cono and 
      notes.notestype = "zz" and
      notes.primarykey = "barcode" and
      notes.secondarykey = " " no-lock:
  
      do x-inx = 1 to 16:
        if notes.noteln[x-inx] <> " " then do:
          if zp-printernm = entry(2,notes.noteln[x-inx],",") then do:
            v-labelname = "pack" + entry(3,notes.noteln[x-inx],",") + ".lbl".
          end.
        end.
      end.       
    end.
    v-printername = zp-printernm.
  end.
end. /* if not newtype */

if v-printername = "" or not avail oeeh then return.

v-filename = "".
v-outputfl = false.

if not newtype then do:
  if v-filename = "" then do:
    nameloop: 
    do while true:
      v-filename = "/usr/tmp/" + string(time) + string(random(1,999)).
      if search(v-filename) = ? then
        leave nameloop.
    end.
    output stream bcd to value(v-filename).
    assign v-outputfl = true.
    put stream bcd unformatted
      "/L=" v-labelname  " " "/P=" v-printername  chr(13) chr(10).
  end.                       
  v-outputofl = true.
  x-labels = 1.
  do z-labels = 1 to x-labels:
    put stream bcd unformatted
         "imtr1"
         v-tilda
         " " 
         v-tilda
         " "
         v-tilda
         " "
         v-tilda
         v-icswbinloc1  format "x(10)"   /* never set */
         v-tilda
         v-icswbinloc2  format "x(10)"   /* never set */
         v-tilda
         v-custprod /* never set */
         v-tilda
         " "
         v-tilda
         " "
         v-tilda
         " "
         v-tilda
         " "
         v-tilda
         " "
         v-tilda
         " "
         v-tilda
         string(
                string(oeeh.orderno,">>>>>>9") + "-" +
                string(oeeh.ordersuf,"99"))
         v-tilda
         oeeh.ordersuf
         v-tilda
         0
         v-tilda
         " "
         v-tilda
         " "
         v-tilda
         " "
         v-tilda
         " "
         v-tilda
         " "
         v-tilda
         oeeh.custpo
         v-tilda
         x-quantity   /* never set */
         v-tilda
         oeeh.promisedt
         v-tilda
         oeeh.shipinstr
         chr(13)
         chr(10).
  end.
/* end. */

  if v-outputfl = true and v-outputofl = true then do:
    output stream bcd  close.
    unix silent value("sh /rd/unibar/ibprint.sh " + v-filename).
    unix silent value ("rm " + v-filename).
  end.
  else
  if v-outputfl = true and v-outputofl = false then do:
    output stream bcd  close.
    unix silent value ("rm " + v-filename).
  end.
end. /* if not newtype */
 
if newtype then do: /* more decriptive than "else" */
  run createLabelName.p(input g-operinits, output v-filename).
  /* v-outputfl = true. */
  output stream bcd to value(v-filename).
  put stream bcd unformatted
    "/L=" v-labelname  " /P=" v-printername " /DR" zsdibarcd.txdatanm
    " /C=" zsdibarcd.copies " " chr(13) chr(10).
  /* add the record */
  empty temp-table pick.
  create pick.
  assign
    pick.labelname = v-labelname  /* "packzebra.lbl" */
    pick.orderno   = STRING(oeeh.orderno)
    pick.ordersuf  = STRING(oeeh.ordersuf,"99")
    pick.custpo    = oeeh.custpo
    pick.user4     = string(oeeh.promisedt,"99/99/99") 
    pick.user5     = oeeh.shipinstr.
  
  /* put data to stream file */
  {pickbcdtt.i}

  output stream bcd  close.
  unix silent value("sh /usr/unibar/ibprint.sh " + v-filename).

  if g-operinit ne "dt01" then
    unix silent value ("rm " + v-filename).
  else do:
    message "zoeppbcd2.p printed " v-filename.
    pause.
    def var zx-l as logical.                   
    def var zx-x as int.                       
    assign zx-l = no                           
           zx-x = 1.                           
           repeat while program-name(zx-x) <> ?:      
             message program-name(zx-x). pause.       
               if program-name(zx-x) = "hher.p" then do:
                   zx-l = yes.                            
                       leave.                                 
                         end.                                     
                           assign zx-x = zx-x + 1.                  
                           end.                                       
                           
  
  end.
end. /* if newtype */

