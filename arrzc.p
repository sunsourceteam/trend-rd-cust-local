/*******************************************************************************
  SX 55
  arrzc.p
    Credit Hold Order Report  - list orders that were not released on the 
    same day as entered.
    Open orders on hold report. Option 2. 
    Requested    : Chuck Freeman.
    Date Written : 10/10/03
******************************************************************************/

{p-rptbeg.i}

def var b-whse       as c  format "x(4)"      no-undo.
def var e-whse       as c  format "x(4)"      no-undo.
def var b-entdt      as da format "99/99/99"  no-undo.
def var e-entdt      as da format "99/99/99"  no-undo.
def var b-crmgr      as c  format "x(4)"      no-undo.
def var e-crmgr      as c  format "x(4)"      no-undo.
def var b-region     as c  format "x(1)"      no-undo.
def var e-region     as c  format "x(1)"      no-undo.
def var b-dist       as c  format "x(3)"      no-undo.
def var e-dist       as c  format "x(3)"      no-undo.
def var crsumm       as logical               no-undo.
def var onhold       as logical               no-undo.
def var prtdet       as logical               no-undo.
def var incerrs      as logical               no-undo.
def var iholds       as logical               no-undo.
def var p-sort       as character             no-undo.
def var n-lines      as i format ">>9"        no-undo.
def var n-late       as i  format ">>9"       no-undo.
def var n-cnt        as i  format ">>>>9"     no-undo.
def var s-count      as i  format ">>>>>>>9"  no-undo.
def var n-conversion as logical               no-undo.
def var skip-sw      as logical               no-undo.
def var htyp         as logical               no-undo.
def var only-htyp    as logical               no-undo.
def var t-cntr       as de format ">>>>>>>9"  no-undo.
def var t-entr       as de format ">>>>>>>9"  no-undo.
def var t-ontime     as de format ">>>>>>>9"  no-undo.
def var t-late       as de format ">>>>>>>9"  no-undo.
def var t-ytyp-o     as de format ">>>>>>>9"  no-undo.
def var t-pct        as de format ">>9.99-"   no-undo.
def var t-avg        as de format ">>>9.99-"  no-undo.
def var t-avghld     as int                   no-undo.
def var lu-crmgrnm   as char format "x(24)"   no-undo.
def var h-spc        as char format "x(1)"    no-undo.
def var h-hdr1       as char format "x(110)"  no-undo.
def var h-hdr2       as char format "x(110)"  no-undo.
def var s-tm         like oeehc.approvetm     no-undo.                   
def var e-tm         like oeehc.approvetm     no-undo.  
def var xstg         as int                   no-undo. 
def var hld-recid    as recid                 no-undo.                
def var ol-recid     as recid                 no-undo.                
def var hld-transdt  as date                  no-undo.             
def var hld-transtm  like zsdioeehc.transtm no-undo.             
def var act-transdt  as date                  no-undo.             
def var fhld-recid   as recid                 no-undo.                
def var fhld-transdt as date                  no-undo.                
def var hld-orderno  like oeeh.orderno        no-undo. 
def var hld-ordersuf like oeeh.ordersuf       no-undo.
def var hld-mgr as char format "xxxx"         no-undo. 
def var htoday  as date                       no-undo.
def var daysub       as int                   no-undo.  
def var daybeg       as int                   no-undo.  
def var a-approveinit like zsdioeehc.approveinit  no-undo.
def var a-approvedt   as char format  "x(8)"  no-undo.
def var ws-dayslt     as int                  no-undo.
def var mgrtot-hld    as int                  no-undo.
def var mgr-ypct      as dec format "zzz9.99-" no-undo. 
                
DEFINE VARIABLE qbf-i     AS INTEGER NO-UNDO.
DEFINE VARIABLE qbf-t     AS INTEGER NO-UNDO.
DEFINE VARIABLE qbf-total AS INTEGER NO-UNDO.
DEFINE VARIABLE qbf-012   AS DECIMAL NO-UNDO.
DEFINE VARIABLE qbf-013   AS DECIMAL NO-UNDO.

def temp-table mgrsum no-undo
  field crmgr    like oeehc.creditmgr  
  field crmgrnm  as char format "x(24)"
  field entords  as de format ">>>>>>>9" 
  field ontime   as de format ">>>>>>>9" 
  field notrels  as de format ">>>>>>>9" 
  field avghld   as int
  field pct      as de format ">>9.99-" 
  field ytyp-l   as de format ">>>>>>9"
  field ytyp-o   as de format ">>>>>>9"
  index mgrkey
        crmgr  ascending. 

define buffer ol-zsdioeehc for zsdioeehc.
define buffer a-zsdioeehc  for zsdioeehc.
define buffer f-zsdioeehc  for zsdioeehc.
define buffer h-zsdioeehc  for zsdioeehc.

def temp-table zdet no-undo
  field whse         as character format "x(4)"
  field region       as char format "x" 
  field dist         as char format "x(03)"
  field orderno      like oeeh.orderno  
  field ordersuf     like oeeh.ordersuf
  field custno       like oeeh.custno
  field crmgr        like arsc.creditmgr 
  field name         as char format "x(24)"
  field entdt        like oeeh.enterdt 
  field apprvdt      like oeehc.approvedt 
  field apprvtm      like oeehc.approvetm 
  field apprvinit    like oeehc.approveinit
  field approvty     like oeeh.approvty
  field priority     like oeehc.priority
  field transproc    as char format "x(05)"
  field transdt      like oeehc.approvedt 
  field transtm      like oeehc.approvetm 
  field operinit     like oeehc.operinit
  field dayslt       as int format ">>>>-"
  field difftm       like oeehc.approvetm
  field oeehtyp      like oeeh.approvty
  index zdetkey
        region
        dist
        crmgr   ascending
        entdt   ascending 
        orderno 
        ordersuf
        transdt
        transtm.

form 
    zdet.region    at 71
    zdet.dist      at 72
    zdet.entdt     at 77
    zdet.apprvdt   at 89
    zdet.apprvinit at 98
    zdet.oeehtyp   at 103
    zdet.orderno   at 104  /* SX 55 */
    "-"            at 112
    zdet.ordersuf  at 113
    zdet.custno    at 116
    zdet.approvty  at 130 
    zdet.transproc at 133
    zdet.transdt   at 140
    zdet.transtm   at 150
    zdet.operinit  at 157
    zdet.dayslt    at 165
with frame f-detprt no-labels width 180 no-box overlay down.

form 
 mgrsum.crmgr      at 1                                   
 mgrsum.crmgrnm    at 8
 mgrsum.entords    at 33
 mgrsum.notrels    at 45 
 mgrsum.pct        at 57
 "%"               at 65
 mgr-ypct          at 67
 "%"               at 75 
 with frame f-crmgrtot no-labels width 132 no-box overlay down.

form
    "Total order hold records processed  :" at 2
    t-entr                                  at 40   skip
    "Total records not released same day :" at 2
    t-late                                  at 40   skip
    "Percent of records released same day:" at 2
    t-pct                                   at 45
    "%"                                     at 52
    with frame f-tot no-labels. 
    
form
    "Total order hold records processed  :" at 2
    t-entr                                  at 40   skip
    "Total order hold on time processed  :" at 2
    t-ontime                                at 40   skip
    "Total orders not released same day  :" at 2
    t-late                                  at 40   skip
    "Percent of orders released same day :" at 2
    t-pct                                   at 45
    "%"                                     at 52
    "Percent of 'Y' SameDay NotLate Ords :" at 2
    t-avg                                   at 44
    "%"                                     at 52 
    with frame f-tot2 no-labels. 
 
assign         
    v-datein = sapb.rangebeg[1].
{p-rptdt.i}
assign b-entdt = v-dateout
       v-datein = sapb.rangeend[1].
{p-rptdt.i}
assign e-entdt = v-dateout.

assign b-crmgr   = sapb.rangebeg[2]
       e-crmgr   = sapb.rangeend[2]
       b-region  = sapb.rangebeg[3]
       e-region  = sapb.rangeend[3]
       b-dist    = sapb.rangebeg[4]
       e-dist    = sapb.rangeend[4]. 

assign crsumm  = if sapb.optvalue[1] = "yes" then yes else no
       prtdet  = if sapb.optvalue[2] = "yes" then yes else no
       incerrs = if sapb.optvalue[3] = "yes" then yes else no
       onhold  = if sapb.optvalue[4] = "yes" then yes else no
       iholds  = if sapb.optvalue[5] = "yes" then yes else no.

if prtdet then 
 assign
  h-hdr1 = 
"SameDayY% OE                      OE                        <-------   Release    Data   ------->"
  h-hdr2 =
"Reg/Dist  Hold Dt  ApproveDt AInt Typ  Order No     CustNo  RTyp Proc   Trandt    Trantm RLInit  DaysLate" .
else 
 assign h-hdr1 = "YRelease%" 
        h-hdr2 = "Same Day" .

put control "~033~046~1541~117".   /* landscape format */

form header 
"CrMgr"            at 1  
"Name"             at 8
"HldOrds"          at 34
"Not Released"     at 43
"Released%"        at 57
h-hdr1             at 68
"Entered"          at 34 
"Same Day"         at 43 
"Same Day"         at 57
h-spc              at 66         
h-hdr2             at 68
with frame header1 no-labels width 180 no-box overlay page-top.

form header 
"Date Range Parameter: "  at 60
 b-entdt                  at 82
"To"                      at 91 
 e-entdt                  at 94 
with frame f-date1 no-labels no-underline width 180 
     no-box overlay page-top.

    /*** need to keep track of a monday subtractor - to subtract from 
         begin date to go back and get friday's trans after 6:00 pm ***/
    if weekday(b-entdt) = 2 then 
       assign daybeg = 3. 
    else 
       assign daybeg = 1.

begin-main:
 for each ol-zsdioeehc use-index k-oeehc 
    where ol-zsdioeehc.cono     = g-cono             and 
          ol-zsdioeehc.orderno  > 0                  and 
         (ol-zsdioeehc.transdt >= (b-entdt - daybeg) and 
          ol-zsdioeehc.transdt <= e-entdt)           and 
          substring(ol-zsdioeehc.user1,1,1) = "h"
          no-lock
          break by ol-zsdioeehc.cono 
                by ol-zsdioeehc.orderno 
                by ol-zsdioeehc.ordersuf
                by ol-zsdioeehc.transdt 
                by ol-zsdioeehc.transtm
                by ol-zsdioeehc.user1: 
 
      find  oeeh use-index k-oeeh 
      where oeeh.cono     = g-cono
        and oeeh.orderno  = ol-zsdioeehc.orderno  
        and oeeh.ordersuf = ol-zsdioeehc.ordersuf
        and oeeh.transtype <> "CR"
        and oeeh.transtype <> "RM" 
        and oeeh.transtype <> "QU" 
        no-lock no-error.

      if not avail oeeh then 
         next begin-main.
      if avail oeeh and oeeh.stagecd = 9 then  
         next begin-main. 
      if avail oeeh and oeeh.transtype = "br" and 
               oeeh.stagecd = 0 then 
         next begin-main.      
      find first arsc where
                 arsc.cono   = g-cono and 
                 arsc.custno = oeeh.custno
                 no-lock no-error.
      if avail arsc then 
         assign hld-mgr = arsc.creditmgr. 
      else 
         assign hld-mgr = "none". 
      find first arss where
                 arss.cono   = g-cono      and 
                 arss.custno = oeeh.custno and 
                 arss.shipto = oeeh.shipto
                 no-lock no-error.
      find first smsn where
                 smsn.cono   = g-cono and 
                 smsn.slsrep = (if oeeh.slsrepout ne "" then 
                                   oeeh.slsrepout
                                else
                                if avail arss then 
                                   arss.slsrepout
                                else 
                                   arsc.slsrepout)
                 no-lock no-error.
      if avail smsn then 
      do:
        if substring(smsn.mgr,1,1) < b-region or 
           substring(smsn.mgr,1,1) > e-region then 
           next begin-main.
        if substring(smsn.mgr,2,3) < b-dist or 
           substring(smsn.mgr,2,3) > e-dist then 
           next begin-main.
       end.
       {w-arsc.i oeeh.custno "no-lock"}
       if avail arsc and
         (arsc.creditmgr < b-crmgr or arsc.creditmgr > e-crmgr) then 
          next begin-main.
       else 
       do:
          if avail arsc then 
          do:
            find oimsp where oimsp.person = arsc.creditmgr no-lock no-error.
            if avail oimsp then
              assign lu-crmgrnm = oimsp.name.
            else 
              assign lu-crmgrnm = "none".
          end.
       end. 
    
    /*** need to keep track of a friday subtractor if after 6:00 pm to 
         make days late logic work in create temp rec             ***/
    if weekday(ol-zsdioeehc.transdt) = 6 and 
       substring(ol-zsdioeehc.transtm,1,2) >= "16" then 
       assign daysub = 3. 
    else 
       assign daysub = 1.
 
     /*** run for same day after 6:00 pm ***/
    if (ol-zsdioeehc.transdt = e-entdt               and
        substring(ol-zsdioeehc.transtm,1,2) >= "16") then 
        next begin-main.
    else
    /*** select if previous day and after 6:00 pm - friday or not ***/
    if (substring(ol-zsdioeehc.transtm,1,2) >= "16" and  
       (ol-zsdioeehc.transdt = (b-entdt - daysub)))      then 
    do:
       assign t-cntr   = t-cntr + 1
              ol-recid = recid(ol-zsdioeehc).
       run create-temp(input ol-zsdioeehc.orderno,
                       input ol-zsdioeehc.ordersuf, 
                       input yes).
    end.
    else 
    if (ol-zsdioeehc.transdt = b-entdt              and 
        substring(ol-zsdioeehc.transtm,1,2) < "16") then 
    do:
       assign t-cntr   = t-cntr + 1
              ol-recid = recid(ol-zsdioeehc).
       run create-temp(input ol-zsdioeehc.orderno,
                       input ol-zsdioeehc.ordersuf, 
                       input yes).
    end.
    else 
    if ol-zsdioeehc.transdt >= b-entdt and 
       ol-zsdioeehc.transdt <= e-entdt then 
    do:
       assign t-cntr   = t-cntr + 1
              ol-recid = recid(ol-zsdioeehc).
       run create-temp(input ol-zsdioeehc.orderno,
                       input ol-zsdioeehc.ordersuf,
                       input yes).
    end.
 end.  /* for each */

 /** option to include all transactions */ 
 if iholds = yes then
 do: 
  begin-main2:
  for each icsd where icsd.cono    = g-cono and 
                      icsd.salesfl = yes no-lock:
     do xstg = 1 to 3:
      hold-loop:
      for each oeeh use-index k-whsestagecd where
               oeeh.cono     = g-cono    and 
               oeeh.whse     = icsd.whse and 
               oeeh.stagecd  = xstg      and  
               oeeh.approvty = "h"       and 
               oeeh.transtype <> "CR"    and 
               oeeh.transtype <> "RM"    and 
               oeeh.transtype <> "QU"     
               no-lock:
        find first arsc where
                   arsc.cono   = g-cono and 
                   arsc.custno = oeeh.custno
                   no-lock no-error.
        if avail arsc then 
           assign hld-mgr = arsc.creditmgr. 
        else 
           assign hld-mgr = "none". 
        find first arss where
                   arss.cono   = g-cono      and 
                   arss.custno = oeeh.custno and 
                   arss.shipto = oeeh.shipto
                   no-lock no-error.
        find first smsn where
                   smsn.cono   = g-cono and 
                   smsn.slsrep = (if oeeh.slsrepout ne "" then 
                                     oeeh.slsrepout
                                  else
                                  if avail arss then 
                                     arss.slsrepout
                                   else 
                                     arsc.slsrepout)
                   no-lock no-error.
         if avail smsn then 
         do:
             if substring(smsn.mgr,1,1) < b-region or 
                substring(smsn.mgr,1,1) > e-region then 
                next hold-loop.
             if substring(smsn.mgr,2,3) < b-dist or 
                substring(smsn.mgr,2,3) > e-dist then 
                next hold-loop.
         end.
         {w-arsc.i oeeh.custno "no-lock"}
         if avail arsc and
           (arsc.creditmgr < b-crmgr or arsc.creditmgr > e-crmgr) then 
         do: 
               next hold-loop.
         end.
         else 
         do:
             if avail arsc then 
             do:
              find oimsp where oimsp.person = arsc.creditmgr
                   no-lock no-error.
              if avail oimsp then
                 assign lu-crmgrnm = oimsp.name.
              else 
                 assign lu-crmgrnm = "".
             end.
         end. 
         /*** don't duplicate from above run ***/
          find first zdet where 
                     zdet.orderno  = oeeh.orderno  and 
                     zdet.ordersuf = oeeh.ordersuf and 
                     zdet.oeehtyp  = oeeh.approvty
                     no-lock no-error.
          if not avail zdet then            
          do:
              find last  zsdioeehc where
                         zsdioeehc.cono     = g-cono        and 
                         zsdioeehc.orderno  = oeeh.orderno  and 
                         zsdioeehc.ordersuf = oeeh.ordersuf and 
                         substring(zsdioeehc.user1,1,1) = "h"
                         no-lock no-error.
              if avail zsdioeehc then 
              do:
                 assign t-cntr      = t-cntr + 1
                        hld-transdt = zsdioeehc.transdt.
                 run h-typerec.
                 find mgrsum where mgrsum.crmgr = hld-mgr
                      no-lock no-error.
                  if not avail mgrsum then
                  do:
                     create mgrsum.
                     assign mgrsum.crmgrnm = lu-crmgrnm   
                            mgrsum.crmgr   = hld-mgr
                            mgrsum.entords = mgrsum.entords + 1
                            mgrsum.notrels = mgrsum.notrels + 1.
                  end.
                  else 
                     assign  mgrsum.entords = mgrsum.entords + 1
                             mgrsum.notrels = mgrsum.notrels + 1.
              end. /* avail zsdioeehc */
          end. /* avail zdet */
      end.       
    end. /* for each */
  end. /* for each icsd */
 end. /* ihold = yes */
 
 view frame f-date1.
 view frame header1.

 for each mgrsum where 
          mgrsum.crmgr >= b-crmgr and mgrsum.crmgr <= e-crmgr
          no-lock
          break by mgrsum.crmgr : 

    assign t-entr   = t-entr   + mgrsum.entords 
           t-late   = t-late   + mgrsum.notrels
           t-ytyp-o = t-ytyp-o + mgrsum.ytyp-o.
           
    if last-of(mgrsum.crmgr) then
     do:
        assign mgrsum.pct = if mgrsum.entords = 0 or 
                              (mgrsum.entords - mgrsum.notrels = 0) then 0
                            else if mgrsum.notrels = 0 then 100
                            else (mgrsum.notrels  / mgrsum.entords) * 100
               mgrsum.pct = if mgrsum.pct ne 0 and mgrsum.pct ne 100  then 
                               100 - mgrsum.pct
                            else 
                               mgrsum.pct
               t-ontime   = t-ontime + (mgrsum.entords - mgrsum.notrels)
               mgr-ypct   = if (mgrsum.entords - mgrsum.notrels) = 0 then 
                              0
                            else   
                             (mgrsum.ytyp-o / mgrsum.entords) * 100.
        display  mgrsum.crmgr   mgrsum.crmgrnm
                 mgrsum.entords mgrsum.notrels
                 mgrsum.pct mgr-ypct
                 with frame f-crmgrtot. 
                 down with frame f-crmgrtot. 
        if prtdet then
        do:
          for each zdet where zdet.crmgr = mgrsum.crmgr 
              no-lock
              break by zdet.region 
                    by zdet.dist
                    by zdet.crmgr
                    by zdet.entdt 
                    by zdet.orderno 
                    by zdet.ordersuf: 
           if avail zdet then 
               display zdet.region  zdet.dist  zdet.orderno zdet.ordersuf 
                       zdet.custno  zdet.entdt zdet.apprvdt zdet.apprvinit     
                       zdet.approvty    
                       zdet.oeehtyp 
                       zdet.transproc
                       zdet.transdt  
                       zdet.transtm  
                       zdet.operinit        
                       zdet.dayslt  when zdet.dayslt > 0                      
                       with frame f-detprt.
                       down with frame f-detprt.
          end.  
        end.  /* if prtdet  */
 end.  /* last of crmgr  */
end. /* each mgrsum */ 

assign t-pct = if t-entr = 0 then 0
               else if t-late = 0 then 100
               else (t-late / t-entr) * 100
       t-pct = if t-pct ne 0 and t-pct ne 100 then 
                  100 - t-pct
               else 
                  t-pct
       t-avg = if t-entr ne 0 then 
                 (t-ytyp-o / t-entr) * 100 
               else  
                  0.

display t-entr t-late t-pct t-ontime t-avg with frame f-tot2.               

if onhold then 
  run onhold-rpt.

/*** procedure section ***/

procedure create-temp: 
define input parameter ct-orderno  like oeeh.orderno. 
define input parameter ct-ordersuf like oeeh.ordersuf.
define input parameter ct-flg      as logical. 

proc-loop:
repeat:

   if ct-flg = yes then
   do: 
    /*** find h type to begin processing loop ***/
    find zsdioeehc where
         zsdioeehc.cono   = g-cono and 
         recid(zsdioeehc) = ol-recid
         no-lock no-error.
    if avail zsdioeehc then 
      assign ct-flg = no.
   end. 

   assign a-approvedt   = ""
          a-approveinit = "".
   if substring(zsdioeehc.user1,1,1) ne "y" and 
      substring(zsdioeehc.user1,1,1) ne "m" then 
   do: 
     if substring(zsdioeehc.user1,1,1) = "h" then 
     do: 
        assign hld-recid    = recid(zsdioeehc)
               hld-transtm  = zsdioeehc.transtm 
               hld-orderno  = zsdioeehc.orderno 
               hld-ordersuf = zsdioeehc.ordersuf 
               skip-sw      = no
               htyp         = yes. 
        assign hld-transdt = if substring(zsdioeehc.transtm,1,2) >= "16" and 
                                weekday(zsdioeehc.transdt) = 6 then 
                                zsdioeehc.transdt + 3 
                             else 
                             if substring(zsdioeehc.transtm,1,2) >= "16" then 
                                zsdioeehc.transdt + 1
                             else 
                                zsdioeehc.transdt.
     end.
   end.
   else 
   if substring(zsdioeehc.user1,1,1) = "m" then  
   do:
      assign skip-sw = yes.
      find next zsdioeehc where
                zsdioeehc.cono     = g-cono       and 
                zsdioeehc.orderno  = hld-orderno  and 
                zsdioeehc.ordersuf = hld-ordersuf  
                no-lock no-error.
      if avail zsdioeehc and substring(zsdioeehc.user1,1,1) ne "h" then
         next proc-loop.
      else 
      if not avail zsdioeehc then 
         leave.
      else     
      if (substring(zsdioeehc.user1,1,1) = "h" and avail zsdioeehc) then
         leave.
   end. 
   else 
   if substring(zsdioeehc.user1,1,1) = "y" then  
   do:
      /* here we hit an h as first rec and followed by a y only */
      if skip-sw = yes then 
      do: 
         assign skip-sw = no.
         find next zsdioeehc where
                   zsdioeehc.cono     = g-cono       and 
                   zsdioeehc.orderno  = hld-orderno  and 
                   zsdioeehc.ordersuf = hld-ordersuf  
                   no-lock no-error.
         if avail zsdioeehc and substring(zsdioeehc.user1,1,1) ne "h" then
            next proc-loop.
         else 
         if not avail zsdioeehc then 
            leave.
         else     
         if (substring(zsdioeehc.user1,1,1) = "h" and avail zsdioeehc) then
            leave.
      end.   
   end.
    
   /*** need to know if there is only an 'h' type record ***/
   find last h-zsdioeehc where
             h-zsdioeehc.cono     = g-cono             and 
             h-zsdioeehc.orderno  = zsdioeehc.orderno  and 
             h-zsdioeehc.ordersuf = zsdioeehc.ordersuf  
             no-lock no-error.
   if avail h-zsdioeehc              and
      recid(h-zsdioeehc) = hld-recid and 
      substring(h-zsdioeehc.user1,1,1) = "h" then 
      assign only-htyp = yes. 
   else    
      assign only-htyp = no.
     
   if (substring(zsdioeehc.user1,1,1) ne "m" and htyp) and   
      (substring(zsdioeehc.user1,1,1) ne "h" and htyp) or
       only-htyp then 
   do:
      if (zsdioeehc.transdt > hld-transdt)         or 
         (substring(zsdioeehc.user1,1,1) = "h"     and
          only-htyp and zsdioeehc.transdt < today) or 

         (substring(zsdioeehc.user1,1,1) = "h"     and
          substring(zsdioeehc.transtm,1,2) < "16"  and 
          only-htyp and zsdioeehc.transdt = today) then 
         /** late record - can be y or any type **/
      do: 
         find mgrsum where mgrsum.crmgr = arsc.creditmgr no-lock no-error.
         if not avail mgrsum then
         do:
            create mgrsum.
            assign mgrsum.crmgrnm = lu-crmgrnm   
                   mgrsum.crmgr   = hld-mgr
                   mgrsum.entords = mgrsum.entords + 1
                   mgrsum.notrels = mgrsum.notrels + 1.
         end.
         else 
            assign  mgrsum.entords = mgrsum.entords + 1
                    mgrsum.notrels = mgrsum.notrels + 1.
         find a-zsdioeehc where recid(a-zsdioeehc) = hld-recid
              no-lock no-error.
         if substring(zsdioeehc.user1,1,1) = "y" then
         do:
            assign a-approvedt   = string(zsdioeehc.approvedt,"99/99/99")    
                   a-approveinit = zsdioeehc.approveinit.
         end.
         
         if avail a-zsdioeehc then 
         do:
            run f-typerec.
         end.
      end.
      else
      do:
         /* not late add to ontime entords accuums   */
         find mgrsum where mgrsum.crmgr = arsc.creditmgr no-lock no-error.
         if not avail mgrsum then
          do:
            create mgrsum.
            assign mgrsum.crmgrnm = lu-crmgrnm   
                   mgrsum.ontime  = mgrsum.ontime + 1 
                   mgrsum.crmgr   = hld-mgr
                   mgrsum.entords = mgrsum.entords + 1.
          end.
          else 
            assign mgrsum.entords = mgrsum.entords + 1
                   mgrsum.ontime  = mgrsum.ontime  + 1.
          if substring(zsdioeehc.user1,1,1) = "y" then
          do:
             assign mgrsum.ytyp-o = mgrsum.ytyp-o + 1.
          end.
         /** include non-errors in detail rpt ***/
         if incerrs then  
         do: 
            find a-zsdioeehc where recid(a-zsdioeehc) = hld-recid
                 no-lock no-error.
            if substring(zsdioeehc.user1,1,1) = "y" then 
               assign a-approvedt   = string(zsdioeehc.approvedt,"99/99/99")    
                      a-approveinit = zsdioeehc.approveinit.
            if avail a-zsdioeehc then 
            do:
               run f-typerec.
            end.
         end.
        /****************************************/
      end. /* end else */
   assign htyp = no.
   end.  /* valid htyp and not h or m */

   find next zsdioeehc where
             zsdioeehc.cono     = g-cono       and 
             zsdioeehc.orderno  = hld-orderno  and 
             zsdioeehc.ordersuf = hld-ordersuf 
             no-lock no-error.
    if not avail zsdioeehc then 
       leave. 
    else     
    if (avail zsdioeehc and substring(zsdioeehc.user1,1,1) = "h") then
       leave.
 
end. /* repeat each zsdioeehc  */   
end procedure.

procedure f-typerec:
do:
 define buffer y-zsdioeehc for zsdioeehc.
 
 /* creating hold record that is late and type =  h p or r i o etc. 
    with no y type to follow j**/

    /*** need to bump friday trans to monday if after 6:00 pm to 
         make days late logic work in create temp rec   ***/
    if weekday(zsdioeehc.transdt) = 6 and 
       substring(zsdioeehc.transtm,1,2) >= "16" then 
       assign daysub = 3. 
    else 
       assign daysub = 1.
    if weekday(today) = 2 and 
      (b-entdt ne today)  then 
       assign htoday  = today - 3. 
    else 
       assign htoday  = today. 
    /******************************************************/   
    assign ws-dayslt = if (zsdioeehc.transdt = htoday and 
                          (zsdioeehc.transdt = b-entdt or
                           zsdioeehc.transdt = b-entdt - 1) and 
                           zsdioeehc.transdt = hld-transdt) or 
                          (zsdioeehc.transdt = htoday and 
                           substring(zsdioeehc.transtm,1,2) < "16" and  
                           zsdioeehc.transdt = e-entdt and 
                           zsdioeehc.transdt = hld-transdt) or
                         ((zsdioeehc.transdt = htoday - daysub) and
                           substring(zsdioeehc.transtm,1,2) >= "16" and 
                          (zsdioeehc.transdt + daysub) = e-entdt and 
                          (zsdioeehc.transdt + 1) = hld-transdt) then  
                          1
                       else  
                       if substring(zsdioeehc.transtm,1,2) >= "16" and 
                          zsdioeehc.transdt ne hld-transdt and 
                          only-htyp then 
                          today - hld-transdt
                       else 
                       if zsdioeehc.transdt ne hld-transdt and 
                          substring(zsdioeehc.transtm,1,2) >= "16" then 
                         (zsdioeehc.transdt + 1) - hld-transdt   
                       else 
                       if zsdioeehc.transdt ne hld-transdt then 
                          zsdioeehc.transdt - hld-transdt  
                       else
                       if not incerrs then   
                          today - hld-transdt
                       else
                           0.

    find y-zsdioeehc where
         y-zsdioeehc.cono = g-cono and 
         recid(y-zsdioeehc) = recid(a-zsdioeehc)
         no-lock no-error.
    if avail y-zsdioeehc then 
       find next y-zsdioeehc where
                 y-zsdioeehc.cono     = g-cono               and 
                 y-zsdioeehc.orderno  = a-zsdioeehc.orderno  and 
                 y-zsdioeehc.ordersuf = a-zsdioeehc.ordersuf and 
                 y-zsdioeehc.transtm  > a-zsdioeehc.transtm  and 
                 substring(y-zsdioeehc.user1,1,1) = "y"
                 no-lock no-error.

    find first oeeh use-index k-oeeh 
         where oeeh.cono     = g-cono
           and oeeh.orderno  = zsdioeehc.orderno  
           and oeeh.ordersuf = zsdioeehc.ordersuf
           no-lock no-error.
    if avail oeeh and oeeh.stagecd ne 9 then 
    do:   
    create zdet. 
    assign zdet.whse      = oeeh.whse 
           zdet.region    = substring(smsn.mgr,1,1)
           zdet.dist      = substring(smsn.mgr,2,3)
           zdet.orderno   = oeeh.orderno 
           zdet.ordersuf  = oeeh.ordersuf
           zdet.custno    = oeeh.custno 
           zdet.crmgr     = hld-mgr 
           zdet.name      = lu-crmgrnm
           zdet.entdt     = hld-transdt 
           zdet.apprvdt   = if avail y-zsdioeehc then 
                               y-zsdioeehc.approvedt 
                            else    
                               zsdioeehc.approvedt
           zdet.apprvtm   = if avail y-zsdioeehc then 
                               y-zsdioeehc.approvetm
                            else 
                               zsdioeehc.approvetm  
           zdet.approvty  = substring(zsdioeehc.user1,1,1)
           zdet.apprvinit = if avail y-zsdioeehc then
                               y-zsdioeehc.approveinit 
                            else    
                               zsdioeehc.approveinit
           zdet.oeehtyp   = oeeh.approvty
           zdet.transproc = zsdioeehc.transproc
           zdet.transdt   = zsdioeehc.transdt
           zdet.transtm   = zsdioeehc.transtm 
           zdet.operinit  = zsdioeehc.operinit
           zdet.dayslt    = ws-dayslt.
    find mgrsum where mgrsum.crmgr = zdet.crmgr
         exclusive-lock no-error.
    if avail mgrsum then
       assign mgrsum.avghld = mgrsum.avghld + zdet.dayslt.
    end.
end.
end procedure.   

procedure h-typerec:
do:
 define buffer y-zsdioeehc for zsdioeehc.
 
 /* creating hold record that is late and type =  h p or r i o etc. 
    with no y type to follow **/

    assign ws-dayslt = today - hld-transdt.

    if ws-dayslt <= 0 and not incerrs  then 
       leave.

    create zdet. 
    assign zdet.whse      = oeeh.whse 
           zdet.region    = substring(smsn.mgr,1,1)
           zdet.dist      = substring(smsn.mgr,2,3)
           zdet.orderno   = oeeh.orderno 
           zdet.ordersuf  = oeeh.ordersuf
           zdet.custno    = oeeh.custno 
           zdet.crmgr     = hld-mgr 
           zdet.name      = lu-crmgrnm
           zdet.entdt     = hld-transdt 
           zdet.apprvdt   = zsdioeehc.approvedt
           zdet.apprvtm   = zsdioeehc.approvetm  
           zdet.approvty  = substring(zsdioeehc.user1,1,1)
           zdet.apprvinit = zsdioeehc.approveinit
           zdet.oeehtyp   = oeeh.approvty
           zdet.transproc = zsdioeehc.transproc
           zdet.transdt   = zsdioeehc.transdt
           zdet.transtm   = zsdioeehc.transtm 
           zdet.operinit  = zsdioeehc.operinit
           zdet.dayslt    = today - hld-transdt.
    find mgrsum where mgrsum.crmgr = zdet.crmgr
         exclusive-lock no-error.
    if avail mgrsum then
       assign mgrsum.avghld = mgrsum.avghld + zdet.dayslt.
end.
end procedure.   

procedure onhold-rpt: 
hide frame header1.
form header
"      Open Orders On Hold Report" at 45 
with frame header2 no-labels width 132 no-box overlay page-top.
    view frame header2.
page.

proc-loop2:
for each icsd where icsd.cono = g-cono and 
         icsd.salesfl = yes no-lock,
    each oeeh use-index k-invproc
   WHERE oeeh.cono = g-cono                                   AND
         oeeh.whse = icsd.whse and oeeh.invoicedt = ?         AND 
        (oeeh.enterdt >= b-entdt and oeeh.enterdt <= e-entdt) and
         oeeh.stagecd > 0 AND oeeh.approvty <> "y" 
         NO-LOCK,
    EACH arsc OF oeeh WHERE arsc.cono = g-cono and 
        (arsc.creditmgr >= b-crmgr             and
         arsc.creditmgr <= e-crmgr) NO-LOCK,
    each smsn where smsn.cono = g-cono         and 
         smsn.slsrep = arsc.slsrepout          and 
       ((substring(smsn.mgr,1,1) >= b-region   and 
         substring(smsn.mgr,1,1) <= e-region)  and 
        (substring(smsn.mgr,2,3) >= b-dist     and 
         substring(smsn.mgr,2,3) <= e-dist)) no-lock
    BREAK BY arsc.creditmgr 
          BY oeeh.slsrepin
          BY oeeh.whse 
          BY oeeh.promisedt
          qbf-total = 1 TO qbf-total + 1:
    
     if (oeeh.transtype = "qu" or oeeh.transtype = "bl" or
         oeeh.transtype = "br") and 
         oeeh.stagecd = 0 then 
         next. 
     
     IF FIRST-OF(arsc.creditmgr) AND NOT FIRST(arsc.creditmgr) THEN
        PAGE.
     ASSIGN qbf-012 = oeeh.totlineord - oeeh.totcostord
            qbf-013 = (arsc.periodbal[3] + arsc.periodbal[4]) +
                       arsc.periodbal[5].
     FORM
     arsc.creditmgr COLUMN-LABEL "Cr Mgr" FORMAT "x(4)" VIEW-AS FILL-IN
     oeeh.slsrepin COLUMN-LABEL "Sls In" FORMAT "x(4)" VIEW-AS FILL-IN
     oeeh.custno COLUMN-LABEL "Customer #" FORMAT ">>>>>>>>>>9"
          VIEW-AS FILL-IN
     arsc.name COLUMN-LABEL "Name" FORMAT "x(25)" VIEW-AS FILL-IN
     oeeh.orderno COLUMN-LABEL "Order #" FORMAT "zzzzzz9" VIEW-AS FILL-IN
     oeeh.ordersuf COLUMN-LABEL "" FORMAT "99" VIEW-AS FILL-IN
     oeeh.approvty COLUMN-LABEL "Ap" FORMAT "x(1)" VIEW-AS FILL-IN
     oeeh.enterdt COLUMN-LABEL "Entered" FORMAT "99/99/99" VIEW-AS FILL-IN
     oeeh.promisedt COLUMN-LABEL "Promised" FORMAT "99/99/99" VIEW-AS FILL-IN
     oeeh.totlineord COLUMN-LABEL "Ord Value" FORMAT "zzzzzz9.99-"
          VIEW-AS FILL-IN
     oeeh.totcostord COLUMN-LABEL "Ord Cost" FORMAT "zzzzz9.99-" 
          VIEW-AS FILL-IN
     qbf-012 COLUMN-LABEL "Margin $" FORMAT "->>>>>9.99" VIEW-AS FILL-IN
     qbf-013 COLUMN-LABEL "Past Due" FORMAT "->>>>>9.99" VIEW-AS FILL-IN
     WITH NO-ATTR-SPACE COLUMN 1 WIDTH 134 NO-BOX NO-VALIDATE.
     DISPLAY
      arsc.creditmgr
      oeeh.slsrepin
      oeeh.custno
      arsc.name
      oeeh.orderno
      oeeh.ordersuf
      oeeh.approvty
      oeeh.enterdt
      oeeh.promisedt
      oeeh.totlineord (SUB-TOTAL BY arsc.creditmgr)
      oeeh.totlineord (SUB-TOTAL)
      oeeh.totcostord
      qbf-012 (SUB-TOTAL BY arsc.creditmgr)
      qbf-012 (SUB-TOTAL)
      qbf-013.
END. /* for each oeeh */
end procedure.






