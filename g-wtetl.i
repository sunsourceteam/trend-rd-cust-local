/*h*****************************************************************************
  INCLUDE         : g-wtetl.i
  DESCRIPTION     : Shared variables for WT entry line items
  USED ONLY ONCE? : no
  AUTHOR          : rhl
  DATE WRITTEN    :
  CHANGES MADE    :
    10/17/92 mms; TB# 8306 WM/WT interface
    10/28/93 kmw; TB# 11758 Improve Unit Conv Precision
    07/13/94 mtt; TB# 10835 Need Option: create WT BO at which stage
    05/19/95 gp;  TB#  6550 Need PO/WT/WO# for tie.  Added v-tietype
        and v-tieno.
    10/13/98 kjb; TB#  9731 Add a second nonstock description.  Added
        s-proddesc1 and s-proddesc2.
    01/13/99 mtt; TB# 24049 PV: Common code use for GUI interface
    06/01/00 des; TB# e5061 Inventory Allocation - Add definition for
        delayresrvfl.
    06/16/00 des; TB# e5061 Inventory Allocation - Add variable lShpQtyOverFl to
        track qtyship override in character
    11/10/03 cm;  TB# e18753 WT Line Delete - Shared variable error
*******************************************************************************/

{g-ic.i {4}}

{3}
def {1} buffer b-icsp     for icsp.      /* shipping  */
def {1} buffer b2-icsp    for icsp.      /* receiving */
def {1} buffer b-icsw     for icsw.      /* shipping  */
def {1} buffer b2-icsw    for icsw.      /* receiving */
def {1} buffer b-wtel     for wtel.
/{3}* */

/*tb 10835 07/13/94 mtt; Need Option: create WT BO at which stage */
{2}
def {1} buffer b-wteh     for wteh.
/{2}* */

def {1} var o-approvety  like wtel.approvety             no-undo.
def {1} var o-bofl       like wtel.bofl                  no-undo.
def {1} var o-catwtfl    like wtel.catwtflo init no      no-undo.
def {1} var o-cubes      like wtel.cubes init 0          no-undo.
def {1} var o-netord     like wtel.netord init 0         no-undo.
def {1} var o-netamt     like wtel.netamt init 0         no-undo.
def {1} var o-nonstockty like wtel.nonstockty            no-undo.
def {1} var o-nosnlots   like wtel.nosnlotso init 0      no-undo.
def {1} var o-prod       like icsp.prod                  no-undo.
def {1} var o-prodcati   like wtel.prodcati              no-undo.
def {1} var o-prodcato   like wtel.prodcato              no-undo.
def {1} var o-prodcost   like wtel.prodcost init 0       no-undo.
def {1} var o-stkqtyord  like wtel.stkqtyord init 0      no-undo.
def {1} var o-stkqtyship like wtel.stkqtyship init 0     no-undo.
def {1} var o-qtyord     like wtel.qtyord init 0         no-undo.
def {1} var o-qtyship    like wtel.qtyship init 0        no-undo.
def {1} var o-totqtyord  like wtel.qtyord  initial 0     no-undo.
def {1} var o-totqtyship like wtel.qtyship initial 0     no-undo.
def {1} var o-unit       like wtel.unit                  no-undo.
def {1} var o-unitchg    like wtel.unit                  no-undo.
def {1} var o-v-idicsp   as recid                        no-undo.
def {1} var o-v-idicsp2  as recid                        no-undo.
def {1} var o-v-idicsw   as recid                        no-undo.
def {1} var o-v-idicsw2  as recid                        no-undo.
def {1} var o-weight     like wtel.weight init 0         no-undo.
def {1} var v-tieappr    as char format "x(1)"           no-undo.  

def {1} var s-approvety  like wtel.approvety             no-undo.
def {1} var s-bofl       like wtel.bofl initial "n"      no-undo.
def {1} var s-commentfl  as c format "x"                 no-undo.
def {1} var s-duedt      like wtel.duedt                 no-undo.
def {1} var s-lineno     like wtel.lineno                no-undo.
def {1} var s-msg        as c format "x(18)"             no-undo.
def {1} var s-netamt     like wtel.netamt                no-undo.
def {1} var s-netavail   as dec format "zzzzzzzz9.99-"   no-undo.
def {1} var s-nonstockty like wtel.nonstockty            no-undo.
def {1} var s-notesfl    like icsp.notesfl               no-undo.
def {1} var s-prod       like wtel.shipprod              no-undo.
def {1} var s-prodcati   like wtel.prodcati              no-undo.
def {1} var s-prodcato   like wtel.prodcato              no-undo.
def {1} var s-prodcost   like wtel.prodcost              no-undo.
def {1} var s-proddesc   as c format "x(49)"             no-undo.
def {1} var s-proddesc1  like wtel.proddesc              no-undo.
def {1} var s-proddesc2  like wtel.proddesc2             no-undo.
def {1} var s-prodline   like icsl.prodline              no-undo.
def {1} var s-prodinrcvfl like wtel.prodinrcvfl          no-undo.
def {1} var s-qtyord     like wtel.qtyord                no-undo.
def {1} var s-qtyship    like wtel.qtyship               no-undo.
def {1} var s-speccost   as c format "x(7)"              no-undo.
def {1} var s-surplus    as dec format "zzzzzzzz9.99-"   no-undo.
def {1} var s-totlineamt like wteh.totlineamt            no-undo.
def {1} var s-unit       like wtel.unit                  no-undo.
def {1} var s-usagefl    like wtel.usagefl initial "yes" no-undo.
def {1} var s-vendno     like apsv.vendno                no-undo.
def {1} var s-whse       like icsd.whse                  no-undo.
def {1} var s-yn         as logical initial "yes"        no-undo.

def {1} var v-commentfl  like wtel.commentfl             no-undo.
/*tb 11758 10/28/93 kmw; Improve Unit Conversion Precision */
def {1} var v-conv       as de                           no-undo.
def {1} var v-convs      as de                           no-undo.
def {1} var v-crprod     like icsp.prod                  no-undo.
def {1} var v-crwhse     like icsd.whse                  no-undo.
def {1} var v-crtype     like wtel.xrefprodty            no-undo.
def {1} var v-crqty      like icsec.orderqty             no-undo.
def {1} var v-crunit     like icsec.unitsell             no-undo.
def {1} var v-delayresrvfl like wtel.delayresrvfl        no-undo.
def {1} var v-desc       like sasta.descrip              no-undo.
def {1} var v-donefl     as logical initial yes          no-undo.
def {1} var v-errfl      as c format "x"                 no-undo.
def {1} var v-firsttime  as logical                      no-undo.
def {1} var v-func       as logical initial "no"         no-undo.
def {1} var v-idwtel     as recid                        no-undo.
def {1} var v-idicsp     as recid                        no-undo.
def {1} var v-idicsp2    as recid                        no-undo.
def {1} var v-idicsw     as recid                        no-undo.
def {1} var v-idicsw2    as recid                        no-undo.
def {1} var v-lastkey    as i                            no-undo.
def {1} var v-linefl     like wteh.linefl                no-undo.
def {1} var v-maint-l    as c format "x"                 no-undo.
def {1} var v-nextlineno like wtel.lineno                no-undo.
def {1} var v-nosnlots   like wtel.nosnlotso             no-undo.
def {1} var v-prodcost   as dec format ">>>>>>9.999999"  no-undo.
def {1} var v-qtychg     as logical initial no           no-undo.
def {1} var v-reqprod    like wtel.shipprod              no-undo.
def {1} var v-stkqtyord  like wtel.stkqtyord             no-undo.
def {1} var v-stkqtyship like wtel.stkqtyship            no-undo.
def {1} var v-surplus    as dec format "zzzzzzzz9.99-"   no-undo.
def {1} var v-unit       like icsp.unitsell              no-undo.
def {1} var v-wmqtyship  like wtel.wmqtyship             no-undo.
def {1} var v-wmqtyassn  like oeel.wmqtyship             no-undo.
def {1} var v-xreffl     as logical initial no           no-undo.
def {1} var v-xrefprodty like wtel.xrefprodty            no-undo.
def {1} var v-tietype    like wtel.ordertype             no-undo.
def {1} var v-tieno      like wtel.orderaltno            no-undo.

def {1} var lShpQtyOverFl as logical init no             no-undo.
def {1} var v-oldwtno    like wteh.wtno                  no-undo.
def {1} var v-oldwtsuf   like wteh.wtsuf                 no-undo.

