/* d-oeizl.i 1.1 01/03/98 */
/* d-oeizl.i 1.6 09/19/97 */
/*h*****************************************************************************
  SX 55
  INCLUDE      : d-oeizl.i
  DESCRIPTION  : OE Inquiry - Line Item Display
  USED ONCE?   : no
  AUTHOR       : rhl
  DATE WRITTEN : 09/08/89
  CHANGES MADE :
    12/23/91 pap; TB#  5266 JIT/Line Due - replace requested ship date with
        promised date
    06/04/92 smb; TB#  6867 Operator choice of Qty/Value
    07/20/92 mwb; TB#  7191 Added custno change
    01/26/95 dww; TB# 17308 Add User Hook Includes
    04/10/95 tdd; TB# 15201 RM orders show as positive
    09/19/97 kjb; TB# 14318 Quantity/Value displayed is incorrect when a
        Customer PO# is entered
*******************************************************************************/


if v-xmode = "O" then
  do:
  {p-oeord.i "oeeh."}
/*tb 15201 04/10/95 tdd; RM orders show as positive */
/*tb 14318 09/19/97 kjb; Modify assignment of s-value to use totals from the
    order lines instead of the order header if a product and a customer PO#
    were entered. */
  assign
      s-suspend = if oeeh.updtype = "s" then "s"
                  else if oeeh.fpcustno ne {c-empty.i} then "f"
                  else ""
      s-order   = string(oeeh.orderno,"zzzzzzz9") + "-" +
                  string(oeeh.ordersuf,"99") + oeeh.notesfl
      s-value   = if s-valuefl = yes then
                      if v-uselnfl = yes then
                          string(v-netamt, "zzzzzzzz9.99-")
                      else
                          string(v-totlineamt *
                          (if oeeh.transtype = "rm" then -1 else 1),
                          "zzzzzzzz9.99-")
                  else if v-uselnfl = yes then
                       string(v-netamt, "zzzzzzzz9.99-")
                  else if v-totlineamt = oeeh.totlineord
                       then string(oeeh.totqtyord *
                       (if oeeh.transtype = "rm" then -1 else 1),
                       "zzzzzzzz9.99-")
                  else string(oeeh.totqtyshp *
                      (if oeeh.transtype = "rm" then -1 else 1),
                      "zzzzzzzz9.99-").
                      
 assign s-pocust  = if s-custpojobty = "j":u  then 
                      oeeh.jobno
                    else if s-custpojobty = "p":u    then oeeh.custpo
                    else {c-asignc.i oeeh.custno} + " " + oeeh.shipto.

 assign  s-dtranstype = oeeh.transtype
         s-dstagecd   = oeeh.stagecd
         s-dpromisedt = oeeh.promisedt
         s-dinvoicedt = oeeh.invoicedt
         s-dtakenby   = oeeh.takenby.
 
  end.
else
if v-xmode = "C" then
  do:
  assign  v-totlineamt = /* oeehb.totlineamt. */

                         oeehb.totinvamt.
/*tb 15201 04/10/95 tdd; RM orders show as positive */
/*tb 14318 09/19/97 kjb; Modify assignment of s-value to use totals from the
    order lines instead of the order header if a product and a customer PO#
    were entered. */
  assign
      s-suspend = "C" /*
                  if oeehb.updtype = "s" then "s"
                  else if oeeh.fpcustno ne {c-empty.i} then "f"
                  else ""
                  */
      s-order =   substring(string(int(oeehb.batchnm),">>>>>>>9"),1,8)                              + "c" + "  " + oeehb.notesfl
      s-value   = if s-valuefl = yes then
                      if v-uselnfl = yes then
                          string(v-netamt, "zzzzzzzz9.99-")
                      else
                          string(v-totlineamt,"zzzzzzzz9.99-")
                  else if v-uselnfl = yes then
                       string(v-netamt, "zzzzzzzz9.99-")
                       else
                       string(0,"zzzzzzzz9.99-"). 
 assign s-pocust  = if s-custpojobty = "j":u  then 
                      oeehb.jobno
                    else if s-custpojobty = "p":u    then oeehb.custpo
                    else {c-asignc.i oeehb.custno} + " " + oeehb.shipto.

 if s-valuefl = no and v-uselnfl = false then do:
   assign v-totlineamt = 0.
   for each oeelb use-index k-oeelb where
            oeelb.cono     = g-cono and
            oeelb.batchnm  = oeehb.batchnm and
            oeelb.seqno    = oeehb.seqno no-lock:
      assign v-totlineamt = v-totlineamt + oeelb.qtyord.
   end.   
   assign s-value   = string(v-totlineamt,"zzzzzzzz9.99-").
       
 end.  
 assign  s-dtranstype = "ZQ"
         s-dstagecd   = if oeehb.stagecd = 9 then 9 else 0
         s-dpromisedt = oeehb.promisedt
         s-dinvoicedt = ?
         s-dtakenby   = oeehb.takenby.
         
 end.





display
    s-order
    s-dtranstype @ oeeh.transtype
    s-dstagecd   @ oeeh.stagecd
    s-suspend
    s-dpromisedt @ oeeh.promisedt
    s-dinvoicedt @ oeeh.invoicedt
    s-dtakenby   @ oeeh.takenby
    s-pocust
    s-value
with frame f-oeizl.

/* SX 55
if v-xmode = "O" then
  do:
  if can-do("1,4,5,7",string(v-mode)) then
      if oeeh.jobno ne "" and oeeh.jobno ne "ZWIP" and oeeh.jobno ne "ZBLOCK"
          then display "Job # " + oeeh.jobno @ s-pocust with frame f-oeizl.
      else display oeeh.custpo @ s-pocust with frame f-oeizl.
  else display {c-asignc.i oeeh.custno} + " " + oeeh.shipto @ s-pocust
       with frame f-oeizl.
  end.
else
if v-xmode = "C" then
  do:
  if can-do("1,4,5,7",string(v-mode)) then
      display oeehb.custpo @ s-pocust with frame f-oeizl.
  else display {c-asignc.i oeehb.custno} + " " + oeehb.shipto @ s-pocust
       with frame f-oeizl.
  end.
SX 55 */
if avail oeeh then
  do: 
/*tb 17308 01/26/95 dww; Add User Hooks */
{d-oeizl.z99 &user_line_display = "*"}
  end.


