/* z-sapbozload

   Load custom sapbo records into temp-table.

*/
display "Exclude Customer List" with frame zygot1.
display " " with frame zygot2.

for each sapbo where 
   sapbo.cono = g-cono and
   sapbo.reportnm = sapb.reportnm exclusive-lock:

  
   create zsapbo.
   assign zsapbo.selection_cust = sapbo.custno.
   display "Excluded Customer - "   at 5
               zsapbo.selection_cust at 25 with frame zygot3
               no-box no-underline no-labels .
    if sapb.delfl = true then
      delete sapbo.
end.