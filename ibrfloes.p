/* ibrfloes.p 1.5 02/19/98 */
/*h*****************************************************************************
  PROCEDURE    : ibrfloes.p (Replaces icetloes.p for RF terminals; IB Module)
  DESCRIPTION  : Display, Assign, and Deassign Lot #'s, OE
  AUTHOR       : enp
  DATE WRITTEN :
  CHANGES MADE :
    08/23/91 mwb;           Credit return changes.
    08/29/91 mwb;           Split out the first display screen, select method.
    02/18/93 kmw; TB#  4776 Disallow F12 on Choose
    07/28/93 tdd; TB# 12319 Changes to s-lotno for user hook
    03/19/96 kr;  TB# 12406 Variables defined in form, moved to calling
        programs
    10/15/96 mtt; TB# 21254 (2F) Develop Value Add Module
    03/10/97 mms; TB#  7241 (5.1) Spec8.0 Special Price Costing; added
        speccost.gva as shared.
    02/09/98  aa; TB# 23693 copied from stnd 9.0, adatpted for RF terminals
                      (IB Project)
    02/19/98  aa; TB# 23693 Use ibrfloes.lpr instead of ibrfloes.ldi (not
                            needed any more)
    05/13/99 jbs; TB# 23693 Added g-screenpos for variable positioning.
    12/21/99 bp ; TB# 26804 Added SPC conversion in display and define of the
        b-icss buffer.
******************************************************************************/


/* for icetl */
def input-output param v-whse       like icsd.whse                      no-undo.
def input-output param v-prod       like icsp.prod                      no-undo.
def input-output param v-type       as c                                no-undo.
def input-output param v-ordno      like oeeh.orderno                   no-undo.
def input-output param v-ordsuf     like oeeh.ordersuf                  no-undo.
def input-output param v-lineno     like oeel.lineno                    no-undo.
def input-output param v-seqno      as i                                no-undo.
def input-output param v-returnfl   as logical                          no-undo.
def input-output param v-origqty    as dec format ">>>>>>>>9.99-"       no-undo.
def input-output param v-proofqty   as dec format ">>>>>>>>9.99-"       no-undo.
def input-output param v-ordqty     as dec format ">>>>>>>>9.99-"       no-undo.
def input-output param v-cost       like icsel.prodcost                 no-undo.
def input-output param v-ictype     like icet.transtype                 no-undo.
def input-output param v-outqty     as dec format  ">>>>>>>>9.99-"      no-undo.
def input-output param v-errfl      as c                                no-undo.
def input-output param v-qtyunavail as dec format  ">>>>>>>>9.99-"      no-undo.

/* from g-icetl.i **/
{g-all.i}
{g-ic.i}
{g-oe.i}
{g-po.i}
{g-ar.i}
{g-ap.i}
{g-wt.i}

/*tb 26804 12/21/99 bp ; Add define of buffer.*/
def shared buffer b-icss for icss.

/*tb 23693 05/13/99 jbs; Added var */
def shared var g-screenpos as i no-undo.

def shared frame f-icetl.
def shared frame f-icetlout.

{speccost.gva "shared"}
def {1} shared var v-seecostfl      as logical                          no-undo.
def {1} shared var v-icsnpofl       as logical                          no-undo.
def {1} shared var v-method         as c                                no-undo.
def {1} shared var s-title          as c format "x(76)"                 no-undo.
def {1} shared var v-iclotrcptty    as c                                no-undo.
def {1} shared var v-sfl            as logical                          no-undo.
def {1} shared var s-lotno          like icetl.lotno                    no-undo.
def            var s-selectfl       as logical format "*/ "             no-undo.
def            var v-first          as recid                            no-undo.
def            var v-last           like aret.seqno                     no-undo.
def            var v-recid          as recid extent 13                  no-undo.
def            var v-recid2         as recid extent 13                  no-undo.
def            var s-quantity       as dec format "zzzzzzz9.99"         no-undo.
def     shared var s-qtyunavail     like icetl.quantity                 no-undo.
def     shared var s-reasunavty     like icetl.reasunavty               no-undo.
def            var v-frow           as i                                no-undo.

def            var v-cfl            as logical                          no-undo.
def            var v-firstfl        as logical                          no-undo.
def            var v-oldfl          as logical                          no-undo.
def            var v-quantity       as dec format "zzzzzzz9.99"         no-undo.

/* for returns */
def buffer b-icetl for icetl.
def     shared var s-retorderno     like oeel.retorderno                no-undo.
def     shared var s-retordersuf    like oeel.retordersuf               no-undo.
def     shared var s-retlineno      like oeel.retlineno                 no-undo.
def     shared var s-returnty       like oeel.returnty                  no-undo.
def     shared var v-maint-l        as c format "x"                     no-undo.
def     shared var s-retseqno       like oeelk.retseqno                 no-undo.
def            var o-ordno          like oeel.orderno                   no-undo.
def            var o-ordsuf         like oeel.ordersuf                  no-undo.
def            var o-lineno         like oeel.lineno                    no-undo.
def            var o-seqno          like oeelk.retseqno                 no-undo.
def            var v-count          as integer                          no-undo.
def            var v-created        as logical                          no-undo.

/*tb 12406 03/19/96 kr; Variables defined in form */
def            var s-rettext        as c format "x(7)"                  no-undo.
def            var s-seqno          as c format "x(4)"                  no-undo.
def            var s-kplabel        as c format "x(5)"                  no-undo.
def            var s-wono           as i format "zzzzzz9"               no-undo.
def            var s-kpdash         as c format "x"                     no-undo.
def            var s-wosuf          as i format "99"                    no-undo.
/*tb 21254 10/15/96 mtt; Develop Value Add Module */
def            var v-ordertype      like icets.ordertype                no-undo.


/*tb 23693 01/19/97 Repleaces f-icetl.i */
{ibrfetl.lfo} /*tb 23693 Repaced f-icetl.i...*/

assign
    o-ordno       = v-ordno
    o-ordsuf      = v-ordsuf
    o-lineno      = v-lineno
    o-seqno       = v-seqno
    v-ordertype   = if v-type = "oe" then "o" else "f".

/*** MAIN LOGIC ***/
main:
do while true with frame f-icetlout on endkey undo main, leave main:

        clear frame f-icetlout all no-pause.

        /*tb 23693 02/19/98 aa; removed put screen logic; not needed for RF)*/
        status default "Return to Select/Deselect".

        assign
            v-recid  = 0
            v-recid2 = ?.

        if v-returnfl = yes and s-retorderno <> 0 then
            assign
                o-ordno  = v-ordno
                o-ordsuf = v-ordsuf
                o-lineno = v-lineno
                o-seqno  = v-seqno
                v-ordno  = s-retorderno
                v-ordsuf = s-retordersuf
                v-lineno = s-retlineno
                v-seqno  = s-retseqno.

        l-find:
        do i = 1 to 9:

            l-icetl:
            do while true:

                if i = 1 and v-count = 0 then
                do:

                    {n-lnoe1.i first}

                end. /* end of if i = 1 and v-count = 0 */

                else do:

                    {n-lnoe1.i next}

                end. /* end of if i <> 1 or v-count <> 0 */

                if avail icsel then
                do:

                    s-lotno  = icsel.lotno.
                    {n-lnoe2.i next}
                    find {p-icetl.i} no-lock no-error.
                    if not avail icetl and v-returnfl = yes and
                        s-retorderno <> 0 then
                    do:

                        v-count = v-count + 1.
                        next l-icetl.

                    end. /* end of if not avail icetl and .... */

                    if s-retorderno <> 0 and v-returnfl = yes then
                    do:

                        /* finds if curr ord icetl exists */
                        /*tb 21254 10/10/96 mtt; Develop Value Add Module */
                        {p-icetlr.i &oeordertype = "v-ordertype"}

                    end. /* end of if s-retorderno <> 0 */

                    else
                        assign
                            s-selectfl  = if avail icetl then true else false
                            s-quantity  = if avail icetl then icetl.quantity
                                          else 0
                            v-recid2[i] = if avail icetl then recid(icetl)
                                          else ?.

                    assign
                        v-recid[i]  = recid(icsel)
                        v-first     = if i = 1 then recid(icsel)
                                      else v-first.

                    /*tb 23693 p-lnoed.i Not needed in RF (IB Project) */
                    display s-lotno with frame f-icetlout.
                    down with frame f-icetlout.

                end. /* end of if avail icsel */

                else leave l-find.
                leave l-icetl.

            end. /* end of do - l-icetl */

        end. /* end of l-find loop */

        up i - 1 with frame f-icetlout.
        v-count = 0.

        do while true on endkey undo, leave:

            choose row s-lotno go-on(f12) no-error with frame f-icetlout.
            color display normal s-lotno with frame f-icetlout.
            hide message no-pause.

            /*d If F12 pressed, go into the choose again */
            if {k-func12.i} then
            do:

                run ibrferr.p(2003).
                next.

            end. /* end of if k-func12.i */

            if {k-accept.i} or {k-func7.i} or {k-func8.i} then
            do:

                v-errfl = "c".
                return.

            end. /* end of if k-accept.i or .... */

            else if {k-return.i} then
            do:

                if frame-value = "" then
                do:

                    run ibrferr.p(4623).
                    next.

                end. /* end of if frame-value = "" */

                find icsel where recid(icsel) = v-recid[frame-line]
                    exclusive-lock no-wait no-error.

                if locked icsel then
                do:

                    run ibrferr.p(4624).
                    pause.
                    next.

                end. /* end of if locked icsel */
                assign s-lotno = if avail icsel then icsel.lotno else
                                 frame-value.
                if v-recid2[frame-line] <> 0 then find icetl where
                    recid(icetl) = v-recid2[frame-line]
                    exclusive-lock no-error.

                l-update:
                do on endkey undo, leave l-update:

                    assign
                        s-qtyunavail = if avail icetl and v-returnfl
                                           then icetl.qtyunavail
                                       else 0
                        s-reasunavty = if avail icetl and v-returnfl
                                           and icetl.orderno = o-ordno
                                           then icetl.reasunavty
                                       else if v-returnfl = yes then
                                           s-reasunavty
                                       else "".

                    /* Update */
                    {ibrfloes.lpr} /*tb 23693 Replaces u-lnoe.i  */
                    if v-returnfl = yes and s-retorderno <> 0 then
                    do:
                        assign
                            v-ordno  = o-ordno
                            v-ordsuf = o-ordsuf
                            v-lineno = o-lineno
                            v-seqno  = o-seqno
                            s-lotno  = icsel.lotno.
                        find {p-icetl.i} exclusive-lock no-error.

                    end. /* end of if v-returnfl = yes and .... */
                    else
                      if v-recid2[frame-line] <> 0 then find icetl where
                        recid(icetl) = v-recid2[frame-line]
                        exclusive-lock no-error.


                    /* Assign */
                    /*tb 21254 10/10/96 mtt; Develop Value Add Module */

                    {p-rflnoea.i &oeordertype = "v-ordertype"}

                    /*tb 23693 Changed frame for RF */
                    display
                        s-selectfl
                    with frame f-icetlout.
                    if frame-line <> 9 and not {k-cancel.i}
                        then down with frame f-icetlout.

                end. /* end of l-update */

                hide frame f-rf-icetlout.

                if {k-cancel.i} then
                do:

                    hide message no-pause.
                    hide frame f-return no-pause.
                    hide frame f-return2 no-pause.

                    if avail icetl then
                        s-quantity = if icetl.orderno = s-retorderno
                                         then 0
                                     else icetl.quantity.
                    else s-quantity = 0.

                end. /* end of if k-cancel.i */

            end. /* end of else if k-return.i */

            else if {k-scrlup.i} or {k-prevpg.i} then
            do:

                if v-recid[1] = v-first or v-recid[1] = 0 then
                do:

                    run ibrferr.p(9013).
                    next.

                end. /* end of if v-recid[1] = v-first or .... */

                else do:

                    if v-returnfl = yes and s-retorderno <> 0 then
                        assign
                            o-ordno  = v-ordno
                            o-ordsuf = v-ordsuf
                            o-lineno = v-lineno
                            o-seqno  = v-seqno
                            v-ordno  = s-retorderno
                            v-ordsuf = s-retordersuf
                            v-lineno = s-retlineno
                            v-seqno  = s-retseqno.

                    find icsel where recid(icsel) = v-recid[1] no-lock no-error.

                    l-find:
                    do i = 9 to 1 by -1:

                        l-icetl:
                        do while true:

                            {n-lnoe1.i prev}

                            if avail icsel then
                            do:

                                s-lotno = icsel.lotno.
                                {n-lnoe2.i prev}
                                find {p-icetl.i} no-lock no-error.
                                if not avail icetl and v-returnfl = yes and
                                    s-retorderno <> 0 then
                                do:

                                    v-count = v-count + 1.
                                    next l-icetl.

                                end. /* end of if not avail icetl and .... */

                                if i = 9 then
                                do:

                                    assign
                                        v-recid  = 0
                                        v-recid2 = 0.
                                    clear frame f-icetlout all no-pause.
                                    down 8 with frame f-icetlout.

                                end. /* end of if i = 9 */

                                if s-retorderno <> 0 and
                                v-returnfl = yes then
                                do:

                                    /*tb 21254 10/10/96 mtt; Develop Value Add
                                      Module */
                                    /* curr ord icetl */
                                    {p-icetlr.i &oeordertype = "v-ordertype"}

                                end. /* end of if s-retorderno <> 0 and .... */

                                else
                                    assign
                                        s-selectfl  = if avail icetl
                                                          then true else false
                                        s-quantity  = if avail icetl then
                                                          icetl.quantity else 0
                                        v-recid2[i] = if avail icetl then
                                                          recid(icetl) else ?.

                                v-recid[i] = recid(icsel).
                               /*tb 23693 p-lnoed.i Not needed RF (IB proj) */
                                display s-lotno with frame f-icetlout.
                                up with frame f-icetlout.

                            end. /* end of if avail icsel */

                            else leave l-find.
                            leave l-icetl.

                        end. /* end do while true */

                    end. /* end of l-find loop */

                    if i = 9 then run ibrferr.p(9013).
                    else up i - 1 with frame f-icetlout.
                    next.

                end. /* end of else do */

            end. /* end of else if k-scrlup.i or .... */

            else if {k-scrldn.i} or {k-nextpg.i} then
            do:

                if v-recid[9] = 0 then
                do:

                    run ibrferr.p(9013).
                    next.

                end. /* end of if v-recid[9] = 0 */

                else do:

                    if v-returnfl = yes and s-retorderno <> 0 then
                        assign
                            o-ordno  = v-ordno
                            o-ordsuf = v-ordsuf
                            o-lineno = v-lineno
                            o-seqno  = v-seqno
                            v-ordno  = s-retorderno
                            v-ordsuf = s-retordersuf
                            v-lineno = s-retlineno
                            v-seqno  = s-retseqno.

                    find icsel where recid(icsel) = v-recid[9]
                        no-lock no-error.

                    l-find:
                    do i = 1 to 9:

                        l-icetl:
                        do while true:

                            {n-lnoe1.i next}
                            if avail icsel then
                            do:

                                s-lotno = icsel.lotno.
                                {n-lnoe2.i next}
                                find {p-icetl.i} no-lock no-error.
                                if not avail icetl and v-returnfl = yes and
                                    s-retorderno <> 0 then
                                do:

                                    v-count = v-count + 1.
                                    next l-icetl.

                                end. /* end of if not avail icetl and .... */

                                if i = 1 then
                                do:

                                    clear frame f-icetlout all no-pause.
                                    assign
                                        v-recid  = 0
                                        v-recid2 = 0.

                                end. /* end of if i = 1 */

                                if s-retorderno <> 0 and
                                    v-returnfl = yes then
                                do:

                                    /*tb 21254 10/10/96 mtt; Develop Value Add
                                         Module */
                                    {p-icetlr.i &oeordertype = "v-ordertype"}

                                end. /* end of if s-retorderno <> 0 and .... */

                                else
                                    assign
                                        s-selectfl  = if avail icetl
                                                          then true else false
                                        s-quantity  = if avail icetl then
                                                          icetl.quantity else 0
                                        v-recid2[i] = if avail icetl then
                                                          recid(icetl) else ?.

                                v-recid[i] = recid(icsel).

                                /*tb 23693 p-lnoed.i Not needed in RF (IB Prj)*/
                                display s-lotno with frame f-icetlout.
                                down with frame f-icetlout.

                            end. /* end of if avail icsel */

                            else leave l-find.
                            leave l-icetl.

                        end. /* end of l-icetl */

                    end. /* end of l-find */

                    if i = 1 then run ibrferr.p(9013).
                    else up i - 1 with frame f-icetlout.
                    next.

                end. /* end of else do */

            end. /* end of else if k-scrldn.i or .... */

        end. /* end of do while true loop */

    leave main.

end. /* end of main */

assign
    v-ordno  = o-ordno
    v-ordsuf = o-ordsuf
    v-lineno = o-lineno
    v-seqno  = o-seqno.