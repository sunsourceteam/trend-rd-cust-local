/* p-smrl.i 10/08/99 */
/*si**** Custom ****************************************************************
  CLIENT  : 80281 S.D.I. Operating Partners
  JOB     : sdi083
  AUTHOR  : mat
  DATE    : 09/22/99
  VERSION : 8.0.003
  PURPOSE : SMRL - Add New Ranges and New Service Level Calculation
  CHANGES : 
    SI01 09/22/99 mat; Initial Coding
    SI02 01/19/00 gc;  sdi083 - fixed problem with district range 
    SI03 02/05/00 mm;  sdi08301; Add option to print order information
******************************************************************************/
/* p-smrl.i 1.2 04/02/98 */
/*h****************************************************************************
  INCLUDE      : p-smrl.i
  DESCRIPTION  : Processing logic for SMRL - service level report
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    07/13/92 pap;  TB# 7191 Customer # change. Use report.c12 instead of
        report.de12d0 for customer #
    03/26/98 cm;  TB# 21775 Improve performance and code cleanup.  Replace
        report file with temp-table.
******************************************************************************/

/* begin si01 */
{w-smsn.i oeeh.slsrepout no-lock}
if avail smsn then
do:
   if substring(smsn.mgr,1,1) < b-custregion or
      substring(smsn.mgr,1,1) > e-custregion 
   then next.
   if substring(smsn.mgr,2,3) < b-district or     /*si02 length was 2*/
      substring(smsn.mgr,2,3) > e-district        /*si02 length was 2*/
   then next.
end.

{w-smsn.i oeeh.slsrepin no-lock}
if avail smsn then
do:
   if smsn.slstype < b-group or
      smsn.slstype > e-group 
   then next.
end.
/*si01 end*/

assign
    r-linecnt  = 0
    r-complete = 0
    r-incomp   = 0
    r-subs     = 0
    r-promisedt = ?
    r-loford = 0
    r-requestdt  = ?.
/* SDI Stuff */
assign
    r2-totamount = 0
    r2-linecnt  = 0
    r2-complete = 0
    r2-incomp   = 0
    r2-subs     = 0.
/* SDI Stuff */

 if oeeh.boexistsfl and
    can-find(first xz-oeeh where xz-oeeh.cono = g-cono and
                           xz-oeeh.orderno = oeeh.orderno and
                           xz-oeeh.ordersuf > oeeh.ordersuf) then
   r-loford = 0.
 else 
   r-loford = 1.

for each oeel use-index k-oeel where
    oeel.cono         = g-cono         and
    oeel.orderno      = oeeh.orderno   and
    oeel.ordersuf     = oeeh.ordersuf  and
   (oeel.prodcat     >= b-prodcat      and 
    oeel.prodcat     <= e-prodcat)     and  
    oeel.warrexchgfl  = no             and
    oeel.returnfl     = no             and
    oeel.statustype  <> "c"            and
   (can-do("y,n,p",oeel.botype)         or
    oeel.botype       = "d"            and 
    p-prtdirect)                       and
   (not can-do("s,u", oeel.xrefprodty)  or
   (can-do("s,u", oeel.xrefprodty)     and 
    p-prtsubs))                        and
  ((oeel.specnstype   = "")             or
   (oeel.specnstype   = "n"            and 
    p-prtnonstk       = yes)            or
   (oeel.specnstype   = "s"            and 
    p-prtspecial      = yes))
no-lock:

 /* Range Technology Check  */
  find catmaster where catmaster.cono = 1 and 
                       catmaster.notestype = "zz" and
                       catmaster.primarykey  = "apsva" and
                       catmaster.secondarykey = 
                       (substring(oeel.prodcat,1,4))
                       no-lock no-error. 
  if not avail catmaster then
     do:
     if substring(oeel.prodcat,1,1) = "a" then 
       v-technology = "Automation".
     else if substring(oeel.prodcat,1,1) = "f" then
       v-technology = "Fabrication". 
     else if substring(oeel.prodcat,1,1) = "h" then 
       v-technology = "Hydraulic".   
     else if substring(oeel.prodcat,1,1) = "p" then 
       v-technology =  "Pneumatic".   
     else if substring(oeel.prodcat,1,1) = "s" then 
       v-technology =  "Service".     
     else if substring(oeel.prodcat,1,1) = "f" then 
       v-technology =  "Filtration".  
     else if substring(oeel.prodcat,1,1) = "c" then 
       v-technology =  "Connectors".  
     else if substring(oeel.prodcat,1,1) = "m" then 
       v-technology =  "Mobile".      
     else if substring(oeel.prodcat,1,1) = "n" then 
       v-technology =  "None".        
     end.
  else
    assign
       v-technology = catmaster.noteln[2].
    
  if (substring(v-technology,1,4) < b-tech or 
      substring(v-technology,1,4) > e-tech) then 
      next.

    assign
        r-linecnt  = r-linecnt + 1
        r-complete = r-complete +
                       if oeel.stkqtyord <= oeel.stkqtyship and
                           not can-do("s,u", oeel.xrefprodty) then 1
                       else 0
        r-incomp   = r-incomp +
                       if oeel.stkqtyord > oeel.stkqtyship and
                           not can-do("s,u", oeel.xrefprodty) then 1
                       else 0
        r-subs     = r-subs +
                        if can-do("s,u", oeel.xrefprodty) and
                            p-prtsubs = yes then 1
                        else 0.

/* SDI Stuff */
    
   if (((oeeh.orderdisp = "j" and oeel.jitpickfl = yes) or 
       oeeh.orderdisp <> "j") and (oeel.qtyord - oeel.qtyship) le 0) then
    do:                            
    assign
              
     r2-linecnt  = r2-linecnt + 1. 
                                    
     if r-requestdt = ? then
        r-requestdt = if oeeh.orderdisp <> "j" then 
                              oeel.reqshipdt
                           else
                              oeeh.reqshipdt.
     if r-promisedt = ? then
        r-promisedt  = if oeeh.orderdisp <> "j" then
                             oeel.promisedt
                           else
                             oeeh.promisedt.
     assign   
        r2-totamount  =  r2-totamount + 
                 ((oeel.price * ((100 - oeel.discpct) / 100)) * 
                    oeel.unitconv)
                  * (if oeel.returnfl = yes then (oeel.qtyship * -1)
                     else oeel.qtyship)
        
        r2-complete = r2-complete +
                       if oeel.stkqtyord <= oeel.stkqtyship and
                           not can-do("s,u", oeel.xrefprodty) then 1
                       else 0
        r2-incomp   = r2-incomp +
                       if oeel.stkqtyord > oeel.stkqtyship and
                           not can-do("s,u", oeel.xrefprodty) then 1
                       else 0
        r2-subs     = r2-subs +
                        if can-do("s,u", oeel.xrefprodty) and
                            p-prtsubs = yes then 1
                        else 0.
    end.

/* SDI Stuff */

    /*si03 begin - create detail records in temp-table*/           
    if p-prtdetail = yes then do:

        /*d calculate margins */
        {a-oeetlm.i
            &oeel    = "oeel."
            &oeeh    = "oeeh."
            &icss    = "v-"
            &oerebty = "v-oerebty"
            &com     = "/{&blank}*"
            &com2    = "/{&blank}*"
            &sasc    = "/*"}
    
        create t-zzdet.
        assign
            t-zzdet.prttype     = p-datefl 
            t-zzdet.errtype     = ""
            t-zzdet.whse        = oeel.whse
            t-zzdet.custno      = oeel.custno
            t-zzdet.orderno     = oeel.orderno
            t-zzdet.ordersuf    = oeel.ordersuf
            t-zzdet.lineno      = oeel.lineno
            t-zzdet.transtype   = oeel.transtype
            t-zzdet.reqshipdt   = if oeeh.orderdisp <> "j" then 
                                      oeel.reqshipdt
                                  else
                                      oeeh.reqshipdt
            t-zzdet.promisedt   = if oeeh.orderdisp <> "j" then
                                     oeel.promisedt
                                  else
                                     oeeh.promisedt
            t-zzdet.shipdt      = if avail oeeh then 
                                     oeeh.shipdt
                                  else ?
            t-zzdet.shipprod    = oeel.shipprod
            t-zzdet.qtyord      = oeel.qtyord
            t-zzdet.qtyship     = oeel.qtyship
            t-zzdet.netamt      = oeel.netamt
            t-zzdet.loford      = r-loford
            t-zzdet.marginamt   = s-marginamt
            t-zzdet.marginpct   = s-marginpct
            t-zzdet.z2totinvamt = r2-totamount
            t-zzdet.z2linecnt   = r2-linecnt
            t-zzdet.z2complete  = r2-complete
            t-zzdet.z2incomp    = r2-incomp
            t-zzdet.loford      = r-loford
            t-zzdet.z2subs      = r2-subs
            t-zzdet.region      = substring(smsn.mgr,1,1)  
            t-zzdet.district    = substring(smsn.mgr,2,3)
            t-zzdet.swhse       = oeel.whse
            t-zzdet.sregion     = substring(smsn.mgr,1,1) 
            t-zzdet.slstype     = smsn.slstype
            t-zzdet.repin       = oeel.slsrepin
            t-zzdet.repout      = oeel.slsrepout
            t-zzdet.prodcat     = oeel.prodcat
            t-zzdet.tech        = substring(v-technology,1,4).

      assign
        p-usedate   = if can-do("r",p-datefl) then 
                         t-zzdet.reqshipdt
                      else
                      if can-do("p",p-datefl) then  
                         t-zzdet.promisedt
                      else
                         t-zzdet.shipdt.
      assign                   
        t-zzdet.xlate1      = if p-usedate < t-zzdet.shipdt or
                                    p-prttype = "a"
                                    then t-zzdet.loford
                                 else 0
        t-zzdet.xlate2      = if t-zzdet.promisedt < t-zzdet.shipdt or
                                    p-prttype = "a"
                                    then t-zzdet.loford
                                 else 0
        t-zzdet.ylate1      = if p-usedate < t-zzdet.shipdt or
                                    p-prttype = "a"
                                    then 1
                                 else 0
        t-zzdet.ylate2      = if t-zzdet.promisedt < 
                                    t-zzdet.shipdt or p-prttype = "a"
                                    then 1
                                 else 0.
    
    end.
    /*si03 end*/
end.
/*
if r-linecnt > 0 then do:
    create t-smrl.
    assign
        t-smrl.custno    = oeeh.custno
        t-smrl.whse      = oeeh.whse
        t-smrl.orderno   = oeeh.orderno
        t-smrl.ordersuf  = oeeh.ordersuf
        t-smrl.totinvamt = oeeh.totlineamt 
        t-smrl.shipdt    = oeeh.shipdt
        t-smrl.reqshipdt = r-requestdt  
        t-smrl.promisedt = r-promisedt
        t-smrl.linecnt   = r-linecnt
        t-smrl.complete  = r-complete
        t-smrl.incomp    = r-incomp
        t-smrl.subs      = r-subs
/* SDI Stuff */
        t-smrl.z2totinvamt = r2-totamount
        t-smrl.z2linecnt   = r2-linecnt
        t-smrl.z2complete  = r2-complete
        t-smrl.z2incomp    = r2-incomp
        t-smrl.loford      = r-loford
        t-smrl.z2subs      = r2-subs.

/* SDI Stuff */
end.  /* if r-linecnt > 0 */
*/
