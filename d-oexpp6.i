/* message "d-oexpp6.i". pause.   */

/* d-oeepp1.i */
/* d-oeepp1.i 1.3 10/23/98 */
/*h*****************************************************************************
  PROCEDURE     : d-oeepp1.i
  DESCRIPTION   : Used in oeepp1wm.p, oeepp1.p, oeepp1ws.p, vaepp1.p,
                    vaepp1wm.p and vaepp1ws.p`
  AUTHOR        : R&D
  DATE WRITTEN  :
  CHANGES MADE  :
    05/28/91 mwb; TB# 3228  MSDS info not on reprint
    09/11/91 kmw; TB# 2277  Kit enhancements
    10/30/91 mwb; TB# 4616  Qtyord as neg if return
    07/07/92 mms; TB# 7176  Honor linefl on list
    07/29/92 mkb; TB# 7191  Alpha custno changes
    09/09/92 mms; TB# 5113  Don't assn wm on pickup on way
    04/19/93 jlc; TB# 10995 Display customer product info
    06/07/93 kmw; TB# 11519 Qty ship & qty bo is not right
    06/08/93 kmw; TB# 11690 MSDS message appears twice
    07/06/93 kmw; TB# 11717 Print Pick Ticket flag
    07/06/93 mwb; TB# 11604 Component customer product.
    10/10/93 jlc; tb# 13103 EDI customer product wrong.
    10/29/93 kmw; TB# 11758 Set unit conv to 10 dec
    11/08/93 mwb; TB# 13520 Carry unitconv to 10 dec.
    11/16/93 mwb; TB# 13627 Change icsw exclusive to no-lock
    12/01/93 mwb; TB# 13754 oeepp1 oversize.
    02/21/94 dww; TB# 13890 File name not specified causing error
    03/14/94 dww; TB# 15141 Allow decimals in Conversion factor
    11/20/94 mms; TB# 14261 Added Ship Complete packing slip logic
    04/27/95 mtt; TB# 13356 Serial/Lot # doesn't print on PO/RM
    06/22/95 ajw; TB# 18575 Nonstock/Special Kit Component Project (G1)
    11/01/95 gp;  TB# 18091 Expand UPC Number (T20)
    11/06/95 gp;  TB# 18091 Expand UPC Number (T20)
    06/26/96 jms; TB# 21254 (10A) Develop Value Add Module
        Added parameter &lineno to use a numeric variable
    08/20/96 gp;  TB# 19484 (B2a) Warehouse Logistics.  Add creation
        of WLEL records for lines and WLELK records for BOD kit components.
    09/09/96 gp;  TB# 19484 (B2a) Warehouse Logistics.  Add MSDS logic.
    09/16/96 mwb; TB# 19484 (B7) Whse Logistics. Added the CS edit per line.
        Counter Sale orders are excluded from oeepp, moved to oeepi.
    10/03/96 des; TB# 19484 (B1a) Warehouse Logistics Do not send type "r"
        components.  Only send type "c".
    10/30/96 ajw; TB# 19484 (B13) Warehouse Logistics - Added ShipmentID
    12/26/96 mwb; TB# 22380 Whse Logistics.  If the Primary Counter bin
        location is blank in ICSW and the order is a Counter Sale, then
        display a blank 'Bin Loc:' field so the operator has room to write
        in the location to pick from the Location inquiry in TWL.
    12/29/96 jms; TB# 21254 (10A) Develop Value Add Module - changed
        wmet.ordertype from "o" to {&ordertype} and
        wmet.seqno     from 0   to {&seqno}.
    01/31/97 jl;  TB# 22491 Whse Logistic. If line is tied to AC/PO don't send
    02/10/97 mwb; TB# 22557 Whse Logistics. No longer sending the Negative
        Components as Receipt records - passed as PCK records. Sending all
        the components with the kit to avoid double receipts.
    07/17/97 mwb; TB# 23455 Add edit against Order Stage to block the creation
        of a download to TWL if the order is in stage 3 or greater.  Customers
        are using PMEP F10-Print once the order is shipped as a packing slip
        to place in the last box.  The downloads were erroring out (unable to
        update TWL order - not open).
    12/01/97 mwb; TB# 24253 Added edit to block tag and hold orders.  Need to
        change the order disposition to get the order downloaded.
    09/10/98 gkd; TB# 25120 Removed tag and hold order block because order
        disposition changed now.
    10/21/98 jgc; TB# 9731 Expand non-stock description to 2 lines.
    10/12/99 ama; TB# e2761 Tally Project - Add printing of oeelm records for
        Mix-Tally Kits
    10/21/99 ama; TB# e2761 Tally Project - Comment out the oeel.tallyfl check
        so vaepp1.p will compile
    10/26/99 ama; TB# e2761 Tally Project - Add memomixfl check for memo disp
        will be different than other mix kits display
    11/01/99 ama; TB# e2761 Tally Project - Changing "Mix" to "Tly"
    11/09/99 ama; TB# e2761 Tally Project - changes in memo tally print
    03/07/00 cm;  TB# e4369 Memo tally components display with "** Invalid **"
    03/29/00 rgm; TB# 3-2  Add RxServer Interface to Standard
    12/18/01 sbr; TB# e11166 "?" appears in Unit Price
    02/02/09 dt01 findfirstbin.p
*******************************************************************************/
/*e     Parameters:
           &returnfl    = Return flag
           &head        = Header information file
           &section     = Header file for "OE" or Section file for "VA"
           &line        = Line item file
           &cubes       = The number of cube units for the entire line item
           &weight      = Weight of the entire line item
           &ourproc     = The process that calls this program
           &specnstype  = Special or non-stock type
           &altwhse     = Alternate warehouse
           &vendno      = Vendor number
           &kitfl       = Kit flag
           &botype      = Back order type
           &prevqtyship = Previous quantity shipped
           &netamt      = Net amount - the fully extended price of the line
           &prtpricefl  = Print price flag
           &bono        = Back order number
           &orderno     = Order number
           &ordersuf    = Order suffix
           &custno      = Customer number
           &comxref     = Do not use for "oe", comment out for "va" - no xref
           &corechgty   = Core charge type
           &combo       = Do not use for "oe", comment out for "va" - no BO's
           &qtybo       = Quantity back ordered
           &orderdisp   = Order disposition
           &ordertype   = "o" for order entry, "f" for Value Add
           &prefix      = "order" for order entry, "va" for value add
           &seqno       = "0" zero for order entry, "vaesl.seqno" for "va"
           &stkqtyshp   = Stock quantity shipped
           &nosnlots    = Number of serial or lots
           &comtype     = Comment type "oe" for "oe", v-comtype for "va"
           &comlineno   = Comment line number
           &shipto      = Ship to
*******************************************************************************/

/*o Accumulate the totals for the bottom of the pick ticket */
assign
    v-linecnt    = v-linecnt + 1
    a-totcubes   = a-totcubes + if {&returnfl} then 0 else {&cubes}
    a-totweight  = a-totweight + if {&returnfl} then 0 else {&weight}
    a-totlineamt = a-totlineamt + ({&netamt} * if {&returnfl} then -1 else 1).

/*d 1. Skip the print of this line if 0 qty ship lines are suppressed.
    2. For Ship Complete, Tag and Hold orders, only print new/changed items if
       the order is not complete.
    3. If a pick ticket reprint is requested and this line has not been changed
       since the last pick, skip it */
/*
    if  ((v-oelinefl = no and {&line}.stkqtyship = 0) or
         ({&line}.printpckfl = no and p-reprintfl))

        or

        (can-do("s,t",{&orderdisp}) and
         {&section}.pickcnt > 1             and
         v-completefl = no            and
         p-reprintfl = yes            and
         ({&line}.printpckfl = no or
          {&line}.stkqtyship = 0)        and
         g-ourproc = "{&ourproc}")

        or

        (v-reprintfl     = yes and
         p-reprintfl     = yes and
         {&line}.printpckfl = no)
    then do:

        v-noprint = v-noprint + 1.
        next.

    end.
*/

/*o Load line variables */
if {&specnstype} ne "n" then do:

    {w-icsw.i {&line}.shipprod
        "if {&altwhse} ne '' then {&altwhse} else {&line}.whse" no-lock}
    {w-icsp.i {&line}.shipprod no-lock}

end.
else
    /* Added read of catalog so we can print notes */
        {w-icsc.i {&line}.shipprod no-lock}.
        
        

/*tb 18091 11/01/95 gp;  Find product section of UPC number */
if {&line}.arpvendno <> 0 then
    {icsv.gfi "'u'" {&line}.shipprod {&line}.arpvendno no}
else
    {icsv.gfi "'u'" {&line}.shipprod {&vendno} no}

/*tb 18091 11/01/95 gp;  Assign product section of UPC Number */
assign
    v-wmfl      = if {&specnstype} ne "n" and avail icsw and not {&kitfl}
                      and {&botype} <> "p"

                      {&coms}
                      and w-binorder.w-wmid <> 0
                      /{&coms}* */

                      then icsw.wmfl
                  else no

    {&comt}
    a-totqtyshp = a-totqtyshp + if {&returnfl} then 0 else {&line}.qtyship
    /{&comt}* */

    v-conv      = {unitconv.gas &decconv = {&line}.unitconv}
    s-qtybo     = xlines.qtyord 
    s-qtyship   = xlines.printq -
                      ((if p-reprintfl = yes then {&prevqtyship} else 0)
                        / v-conv)
    s-qtybo     = xlines.qtyord  - xlines.printq
                        
    s-qtyship   = if {&returnfl} = yes then s-qtyship * -1
                  else if s-qtyship < 0 then 0
                  else s-qtyship
    v-qtyship   = s-qtyship
    s-qtyord    = if {&returnfl} = yes then {&line}.qtyord * -1
                  else {&line}.qtyord
    s-lineno    = string({&line}.lineno,"zz9")
    s-aster     = if v-completefl = yes    and {&line}.printpckfl = yes and
                      can-do("s,t",{&orderdisp}) then "*"
                  else ""
    s-netamt    = if {&returnfl} = yes then {&netamt} * -1
                  else {&netamt}.

   assign  s-qtybo     = if s-qtybo < 0 then 
                           0 
                         else
                           s-qtybo.
                  

    if {&specnstype} = "n" then do:
      /* dt01 02/03/2009 
      for each wmsbp where wmsbp.cono = 500 + g-cono and
      wmsbp.whse = oeeh.whse and wmsbp.prod = oeel.shipprod no-lock,
      each wmsb where wmsb.cono = wmsbp.cono and wmsb.whse = wmsbp.whse
      and wmsb.binloc = wmsbp.binloc no-lock break by wmsb.priority:
        leave.
      end.
      */
      define var wmsbid  as recid initial ? no-undo.
      define var wmsbpid as recid initial ? no-undo.
      run findfirstbin.p(input g-cono, input oeeh.whse, input oeel.shipprod,
                         output wmsbid, output wmsbpid).
      if wmsbid ne ? and wmsbpid ne ? then do:                   
        find wmsbp where recid(wmsbp) = wmsbpid no-lock no-error.
        find wmsb  where recid(wmsb)  = wmsbid  no-lock no-error.
      end.                                                       
    

      {w-icsw.i {&line}.shipprod
         "if {&altwhse} ne '' then {&altwhse} else {&line}.whse" no-lock}
      {w-icsp.i {&line}.shipprod no-lock}

    end.  


    /**SX30 Sunsource  bin loc   ***/
    assign 

    s-binloc    = if {&botype} = "p" then "Pick Up"
                  else if {&altwhse} ne "" then "Whse: " + {&altwhse} 
                  else if avail icsw  then string(icsw.binloc1,"xx/xx/xxx/xxx")
                  else if avail wmsb then
                    string(wmsb.binloc,"xx/xx/xxx/xxx") 
                  else 
                  if {&specnstype} = "n" and not avail wmsb then
                    "Non Stock"
                  else  
                    "".
                  /*
                  if {&specnstype} = "n"
                  &if "{&line}" = "oeel" &then
                  and oeel.binloc <> "" then 
                      string(oeel.binloc,"xx/xx/xxx/xxx")
                  else if {&specnstype} = "n"    
                  &endif
                  then /* "Non Stock" */
                  (if avail wmsbp then wmsbp.binloc)
                  else if {&botype} = "p" then "Pick Up"
                  else if {&altwhse} ne "" then "Whse: " + {&altwhse}
                  else if not avail icsw then ""
                  else string(icsw.binloc1,"xx/xx/xxx/xxx")
                  */
 /*
    
    s-binloc    = if {&specnstype} = "n"
                  &if "{&line}" = "oeel" &then
                  and oeel.binloc <> "" then 
                      string(oeel.binloc,"xx/xx/xxx/xxx")
                  else if {&specnstype} = "n"    
                  &endif
                  then "Non Stock"
                  else if {&botype} = "p" then "Pick Up"
                  else if {&altwhse} ne "" then "Whse: " + {&altwhse}
                  else if not avail icsw then ""
                  else string(icsw.binloc1,"xx/xx/xxx/xxx")
*/
    s-upcpno    = if avail icsv then
                      {upcno.gas &section  = "4"
                                 &sasc     = "v-"
                                 &icsv     = "icsv."
                                 &comdelim = "/*"}
                  else "00000".

    /*tb 3-2 03/29/00 rgm; Rxserver interface extra variable code */
    /*tb e11166 12/18/01 sbr; Modified the assign statement for s-rxprice to
        include a check for s-qtyship equal to zero. */
    &IF DEFINED(rxserver) <> 0 &THEN
        s-rxline1 = "".
        if {&prtpricefl} = yes then
        assign
            s-rxprice  = if s-qtyship <> 0 then
                             s-netamt / s-qtyship
                         else 0
            s-rxline1  = &if "{&head}" = "oeeh" &then
                             if oeel.corechgty = "r"
                             then string(oeel.corecharge,"zzzzzz9.99-")
                             else
                         &endif
                         if s-rxprice * 100 = integer(s-rxprice * 100)
                              then string(s-rxprice,"zzzzzz9.99-")
                              else string(s-rxprice,"zzzzzz9.99999-").
    &ENDIF


/*d Back ordered qty not used in Value Add */
{&combo}
/*d Set the back ordered qty */
if {&bono} > 0 then do:

    find b-{&line} use-index k-{&line} where
        b-{&line}.cono        = g-cono         and
        b-{&orderno}          = {&orderno}     and
        b-{&ordersuf}         = {&bono}        and
        b-{&line}.lineno      = {&line}.lineno and
        b-{&line}.statustype ne "c" no-lock no-error.

    if avail b-{&line} then s-qtybo = b-{&line}.qtyord.

end. /* set s-qtybo */
/{&combo}* */

/*tb 3-2 03/29/00 rgm; Rxserver interface hook for extra variables */
{oexpp6.z99 &before_linedisplay = "*"
            &comr        = "{&comr}"
            &coms        = "{&coms}"
            &returnfl    = "{&returnfl}"
            &cubes       = "{&cubes}"
            &weight      = "{&weight}"
            &head        = "{&head}"
            &line        = "{&line}"
            &section     = "{&section}"
            &ourproc     = "{&ourproc}"
            &specnstype  = "{&specnstype}"
            &altwhse     = "{&altwhse}"
            &vendno      = "{&vendno}"
            &kitfl       = "{&kitfl}"
            &botype      = "{&botype}"
            &orderdisp   = "{&orderdisp}"
            &prevqtyship = "{&prevqtyship}"
            &netamt      = "{&netamt}"
            &prtpricefl  = "{&prtpricefl}"
            &bono        = "{&bono}"
            &orderno     = "{&orderno}"
            &ordersuf    = "{&ordersuf}"
            &custno      = "{&custno}"
            &corechgty   = "{&corechgty}"
            &qtybo       = "{&qtybo}"
            &ordertype   = "{&ordertype}"
            &prefix      = "{&prefix}"
            &seqno       = "{&seqno}"
            &stkqtyshp   = "{&stkqtyshp}"
            &nosnlots    = "{&nosnlots}"
            &comtype     = "{&comtype}"
            &comlineno   = "{&comlineno}"
            &shipto      = "{&shipto}"
            &twlstgedit  = "{&twlstgedit}" }

/*d Display the line if WM has not been purchased or is not used */
/*tb 18091 11/01/95 gp;  Change UPC number display */
/*tb 3-2 03/29/00 rgm; Rxserver interface added s-rxline vars for rxserver */
if not v-wmfl or not v-modwmfl then do:

    display
        s-lineno
        s-aster
        {&line}.shipprod
        {&line}.unit
        s-qtyord
        s-qtybo
        s-qtyship
        s-upcpno
        s-binloc
        s-netamt when {&prtpricefl} = yes
        "____________ _____" @ s-underline
        &IF DEFINED(rxserver) <> 0 &THEN
            s-rxline1
        &ENDIF
    with frame f-oeel.

    /*tb 9731 10/21/98 jgc; Expand non-stock description.  Added second
        display line for proddesc2. */
    if {&specnstype} = "n" then do:

        if {&line}.proddesc ne "" then display {&line}.proddesc @ s-descrip
            with frame f-oeel.
        if {&line}.proddesc2 ne "" then do:
            display {&line}.proddesc2 @ s-descrip3 with frame f-oeel2.
            down with frame f-oeel2.
        end.
    end.

    else do:

        if avail icsw and {&altwhse} ne "" then
            display icsw.binloc1 @ s-binloc2 with frame f-oeel.

        else if avail icsw and icsw.binloc2 ne "" then
            display icsw.binloc2 @ s-binloc2 with frame f-oeel.

        if not avail icsp or not avail icsw then
            display v-invalid @ s-descrip with frame f-oeel.

        else display icsp.descrip[1] @ s-descrip with frame f-oeel.

    end.

    down with frame f-oeel.

    if avail icsp and icsp.descrip[2] ne "" and {&specnstype} ne "n" then do:

        display icsp.descrip[2] @ s-descrip3 with frame f-oeel2.

        if avail icsw and {&altwhse} ne "" then
            display icsw.binloc2 @ s-binloc3 with frame f-oeel2.


        /*tb 19484 09/16/96 mwb; added the down with frame line */
        down with frame f-oeel2.
    end.

    /*tb 19484 09/16/96 mwb; added displaying of the counter bin location
        for WL.  Can setup a product with a primary counter bin or show room
        location.  Needs to display with the other bins for Primary Picking */
    if v-wlwhsefl = yes and {&head}.transtype = "cs"
    then do for wlicsw:

        {wlicsw.gfi &prod = "{&line}.shipprod"
                    &whse = "{&line}.whse"
                    &lock = "no"}

        /*d Counter Bin Location */
        if avail wlicsw and wlicsw.bincntr ne "" then
            display "            Counter Bin:" @ s-descrip3
                    wlicsw.bincntr @ s-binloc3 with frame f-oeel2.


        /*tb 22380 12/26/96 mwb; added blank location display */
        else
            display "            Counter Bin:" @ s-descrip3
                    "_____________" @ s-binloc3 with frame f-oeel2.

        down with frame f-oeel2.

    end.
    /*tb 19484 09/16/96 mwb; end of Counter Bin display */

    s-lastfl = yes.

end.    /* not WM */

/*d Process for a WM product */
else do:

    {&coms}
    assign
        s-netfl   = w-binorder.w-netfl
        s-lastfl  = w-binorder.w-lastfl
        s-qtyship = if avail wmet then wmet.qtyactual / v-conv else 0
        s-qtyship = if {&returnfl} = yes then s-qtyship * -1
                    else if s-qtyship < 0 then 0
                    else s-qtyship
        v-qtyship = if avail wmet then wmet.qtyactual / v-conv else 0
        s-binloc  = if avail wmet then string(wmet.binloc,"xx/xx/xxx/xxx")
                    else "".

    /{&coms}* */

    {&comt}
    assign
        s-netfl   = yes
        s-lastfl  = yes
        s-qtyship = 0
        v-frameln = 1
        s-binloc  = "Whse Mgr".
    /{&comt}* */

    /*o Display the line item */
    /*tb 18091 11/01/95 gp;  Change UPC number assignment */
    display
        s-lineno
        s-aster
        {&line}.shipprod
        s-upcpno
        s-binloc
        s-qtyord
        s-qtybo
        {&line}.unit
        "____________ _____" @ s-underline
        &IF DEFINED(rxserver) <> 0 &THEN
            s-rxline1
        &ENDIF

        {&comb}
        s-qtyship
        "Whse Mgr" @ s-binloc2
        /{&comb}* */

        {&comt}
        "" when s-qtyord <> s-qtybo @ s-qtyship
        0 when s-qtyord = s-qtybo @ s-qtyship
        /{&comt}* */

    with frame f-oeel.

    {&comb}
    assign
        s-wminfo  = if {&altwhse} ne "" then "Whse: " + {&altwhse} +
                        " (of " + string({&line}.qtyship) + " Total)"
                    else "(of " + string({&line}.qtyship) + " Total)".

    display
        icsp.descrip[1] @ s-descrip
        s-wminfo @ s-descrip2
    with frame f-oeel.
    down with frame f-oeel.
    /{&comb}* */

    /*o Display the WM bins to pick from */
    /*tb 21254 12/29/96 jms; Develop Value Add Module - changed ordertype to
        {&ordertype} from "o" */
    {&comt}
    for each wmet use-index k-ord where
        wmet.cono      = {&line}.cono     and
        wmet.ordertype = "{&ordertype}"   and
        wmet.orderno   = {&orderno}       and
        wmet.ordersuf  = {&ordersuf}      and
        wmet.lineno    = {&line}.lineno   and
        wmet.seqno     = {&seqno}
    no-lock:

        if v-frameln = 1 then do:

            assign
                s-wmqtyship = wmet.qtyactual / v-conv
                s-wmqtyship = if {&returnfl} = yes then s-wmqtyship * -1
                              else if s-wmqtyship < 0 then 0
                              else s-wmqtyship
                s-descrip2  = if {&altwhse} ne "" then "Whse: " +
                                  {&altwhse} + "              " +
                                  string(s-wmqtyship,">>>>>>>>9.99-")
                              else "                        " +
                                  string(s-wmqtyship,">>>>>>>>9.99-")
                s-binloc2   = wmet.binloc.

            display
                icsp.descrip[1] @ s-descrip
                s-binloc2 s-descrip2
            with frame f-oeel.
                    down with frame f-oeel.

            v-frameln = v-frameln + 1.

        end. /* if frameln = 1 */

        else do:

            assign
                s-wmqtyship = wmet.qtyactual / v-conv
                s-wmqtyship = if {&returnfl} = yes then s-wmqtyship * -1
                              else if s-wmqtyship < 0 then 0
                              else s-wmqtyship
                s-binlocwm  = wmet.binloc.

            display
                s-binlocwm
                s-wmqtyship
            with frame f-oewm.
            down with frame f-oewm.

        end. /* not frameln 1 */

    end. /* for each wmet */

    /*tb 9731 10/21/98 jgc; Expand non-stock description.  Added second
        display line for proddesc2. */
    if {&line}.stkqtyship = 0 or {&specnstype} = "n" then do:

        display
            {&line}.proddesc when {&specnstype} = "n" @ s-descrip
            icsp.descrip[1] when {&specnstype} = "" @ s-descrip
        with frame f-oeel.
        down with frame f-oeel.
        if {&line}.proddesc2 ne "" then do:
            display {&line}.proddesc2 @ s-descrip3 with frame f-oeel2.
            down with frame f-oeel2.
        end.

    end. /* stkqtyship = 0, specnstype = n */
    /{&comt}* */

end. /* process for wm product */

/*tb 3-2 03/29/00 rgm; Rxserver interface hooks for extra varaibles */
{oexpp6.z99 &before_xref = "*"
            &comr        = "{&comr}"
            &coms        = "{&coms}"
            &returnfl    = "{&returnfl}"
            &cubes       = "{&cubes}"
            &weight      = "{&weight}"
            &head        = "{&head}"
            &line        = "{&line}"
            &section     = "{&section}"
            &ourproc     = "{&ourproc}"
            &specnstype  = "{&specnstype}"
            &altwhse     = "{&altwhse}"
            &vendno      = "{&vendno}"
            &kitfl       = "{&kitfl}"
            &botype      = "{&botype}"
            &orderdisp   = "{&orderdisp}"
            &prevqtyship = "{&prevqtyship}"
            &netamt      = "{&netamt}"
            &prtpricefl  = "{&prtpricefl}"
            &bono        = "{&bono}"
            &orderno     = "{&orderno}"
            &ordersuf    = "{&ordersuf}"
            &custno      = "{&custno}"
            &corechgty   = "{&corechgty}"
            &qtybo       = "{&qtybo}"
            &ordertype   = "{&ordertype}"
            &prefix      = "{&prefix}"
            &seqno       = "{&seqno}"
            &stkqtyshp   = "{&stkqtyshp}"
            &nosnlots    = "{&nosnlots}"
            &comtype     = "{&comtype}"
            &comlineno   = "{&comlineno}"
            &shipto      = "{&shipto}"
            &twlstgedit  = "{&twlstgedit}" }


/*06/07/96 jms; TB# 21254 Develop Value Add Module
            Value Add does not have cross reference capability */
{&comxref}

v-reqtitle = if oeel.xrefprodty = "c" then "   Customer Prod:"
             else if oeel.xrefprodty = "i" then "Interchange Prod:"
             else if oeel.xrefprodty = "p" then " Superseded Prod:"
             else if oeel.xrefprodty = "s" then " Substitute Prod:"
             else if oeel.xrefprodty = "u" then "    Upgrade Prod:"
             else "".

if can-do("c,i,p,s,u",oeel.xrefprodty) and oeel.reqprod <> "" then do:

    s-prod  =   oeel.reqprod.

    display
        s-prod
        v-reqtitle
    with frame f-oeelc.
    down with frame f-oeelc.

end. /* cross ref prod display */

if oeel.xrefprodty ne "c" then do:

    s-prod = if oeel.reqprod ne "" then oeel.reqprod
             else {&line}.shipprod.

    {n-icseca.i "first" s-prod ""c"" {&custno} no-lock}

    if avail icsec then do:

        assign
            s-prod     = icsec.prod
            v-reqtitle = "   Customer Prod:".

        display
            s-prod
            v-reqtitle
        with frame f-oeelc.

    end.

end. /* xrefprodty display */

/{&comxref}* */

if {&corechgty} = "r" then display with frame f-oeelr.

/*tb 3-2 03/29/00 rgm; Rxserver interface hook for extra varaibles */
{oexpp6.z99 &before_prodnotes = "*"
            &comr        = "{&comr}"
            &coms        = "{&coms}"
            &returnfl    = "{&returnfl}"
            &cubes       = "{&cubes}"
            &weight      = "{&weight}"
            &head        = "{&head}"
            &line        = "{&line}"
            &section     = "{&section}"
            &ourproc     = "{&ourproc}"
            &specnstype  = "{&specnstype}"
            &altwhse     = "{&altwhse}"
            &vendno      = "{&vendno}"
            &kitfl       = "{&kitfl}"
            &botype      = "{&botype}"
            &orderdisp   = "{&orderdisp}"
            &prevqtyship = "{&prevqtyship}"
            &netamt      = "{&netamt}"
            &prtpricefl  = "{&prtpricefl}"
            &bono        = "{&bono}"
            &orderno     = "{&orderno}"
            &ordersuf    = "{&ordersuf}"
            &custno      = "{&custno}"
            &corechgty   = "{&corechgty}"
            &qtybo       = "{&qtybo}"
            &ordertype   = "{&ordertype}"
            &prefix      = "{&prefix}"
            &seqno       = "{&seqno}"
            &stkqtyshp   = "{&stkqtyshp}"
            &nosnlots    = "{&nosnlots}"
            &comtype     = "{&comtype}"
            &comlineno   = "{&comlineno}"
            &shipto      = "{&shipto}"
            &twlstgedit  = "{&twlstgedit}" }
/* SX32 UPGRADE 



/*o Print the product notes  */
if {&specnstype} ne "n" and avail icsp and icsp.notesfl ne "" then
    run oeepp1n.p(g-cono,"p",
                  icsp.prod,
                  "").   
*/

run zsditariffprt (g-cono,{&line}.shipprod,{&line}.whse,"oeepp",8).

/* SX32 UPGRADE */

if {&specnstype} ne "n" and avail icsp and icsp.notesfl ne "" then
     run notesprt.p(g-cono,"p",icsp.prod,"","oeepp",8,1).
     
/*o Print the product notes for catalog cono = 1 for catalogs*/

if {&specnstype} = "n" and avail icsc and icsc.notesfl ne "" then
     run notesprt.p(1,"g",icsc.catalog,"","oeepp",8,1).
         
/* SX32 UPGRADE */
         


/* SX30 SunSource */

/*o Print the serial/lot records */
/*tb 13356 04/27/95 mtt; Serial/Lot # doesn't print on PO/RM */
{p-oeepp1.i &type       = "{&ordertype}"
            &line       = "{&line}"
            &pref       = "{&prefix}"
            &seq        = {&seqno}
            &stkqtyship = "{&stkqtyshp}"
            &snlot      = "{&nosnlots}"}

/*o Print the line item comments  */

if {&line}.commentfl = yes then do:

    {w-com.i "{&comtype}" {&orderno} {&ordersuf} {&comlineno} yes no-lock}

    if avail com then do i = 1 to 16:

        if com.noteln[i] ne "" then do:

            display com.noteln[i] with frame f-com.
            down with frame f-com.

        end.

    end.

end. /* print line item comments */

/*06/26/96 jms; TB# 21254 Develop Value Add Module
    MSDS sheets not used for VA since the customer is own company */
{&commsds}

/*o Display an MSDS sheet notice for an MSDS product */
/*tb 19484 09/09/96 gp; The value in v-msds is not blank when the next
    product is a non-hazardous product.  Add else statement. */
if v-icmsdsprt = yes and (avail icsp and icsp.msdsfl = yes) then do:

    run oeeppu.p("m",
                 {&custno},
                 {&shipto},
                 {&line}.shipprod,
                 icsp.msdschgdt,
                 output v-printfl,
                 0,
                 0,
                 0,
                 0).

    clear frame f-msds.

    if v-printfl = yes then do:

        v-msds = if icsp.msdssheetno ne "" then "** Include MSDS Sheet " +
                     icsp.msdssheetno + " For This Product With This Order **"
                 else
                    "** Include MSDS Sheet For This Product With This Order **".

        display v-msds with frame f-msds.

    end.

end.    /* MSDS */

else v-msds = "".

/{&commsds}* */

/*tb 19484 08/20/96 gp;  Create a WLEL record for each line */
/*tb 19484 09/09/96 gp;  Add MSDS logic for WL whses */
/*tb 19484 09/16/96 mwb; Added the CS edit per line.  No need to write the data
    to WL for a Counter Sale.  The interface moved to oeepi for a Stk Move out
    of oh-hand in WL. */
/*tb 22491 01/31/97 jl;  Added check for botype. If line attached to AC/PO then
    don't send line to TWL. Added Compile time parameter to only compile in
    if pick ticket is an OE pick Ticket. */
/*tb 23455 07/17/97 mwb; Added the stage edit for oeeh.stagecd < 3 */
/*tb 24253 12/01/97 mwb; Added the tag and hold edit */
/*tb 25120 09/10/98 gkd; Removed tag and hold edit */
if v-wlwhsefl = yes and {&head}.transtype ne "cs"
    {&twlstgedit}
    &if "{&line}" = "oeel" &then
        and {&line}.botype <> "P"
    &endif
then do:

    if v-msds ne "" then
        v-msds = if icsp.msdssheetno ne "" then icsp.msdssheetno
                 else "Incl MSDS".

    /*tb 19484 10/30/96 ajw; (B13) Warehouse Logistics - Shipmentid*/
    {wlpproc.gpr &invoutcond    = "{&head}.transtype <> ""rm"""
                 &wlsetno       = v-wlsetno
                 &whse          = {&head}.whse
                 &ordertype     = ""O""
                 &orderno       = {&orderno}
                 &ordersuf      = {&ordersuf}
                 &lineno        = {&comlineno}
                 &seqno         = 0
                 &msdssheetno   = v-msds
                 &updtype       = ""A""
                 &shipmentid    = """"}

end. /* if v-wlwhsefl = yes */

v-whse = if {&altwhse} ne "" then {&altwhse} else {&line}.whse.

/* 06/14/96 jms; TB# 21254 Develop Value Add Module */
/* BOD kits are not used in the Value Add module */

if {&kitfl} = yes and 
   can-find(first oeelk use-index k-oeelk where
       oeelk.cono      = {&line}.cono     and
       oeelk.ordertype = "o"           and
       oeelk.orderno   = {&orderno} and
       oeelk.ordersuf  = {&ordersuf} and
       oeelk.lineno    = {&line}.lineno) then do:
   for each oeelk use-index k-oeelk where
       oeelk.cono      = {&line}.cono     and
       oeelk.ordertype = "o"           and
       oeelk.orderno   = {&orderno}  and
       oeelk.ordersuf  = {&ordersuf} and
       oeelk.lineno    = {&line}.lineno:
       find icsw where icsw.cono = oeelk.cono and
                       icsw.whse = oeelk.whse and
                       icsw.prod = oeelk.shipprod
                       no-lock no-error.
       if avail icsw then
          if icsw.binloc1 = "New Part" then
             assign substring(oeelk.user4,65,13) = "  " + icsw.binloc1
                    substring(oeelk.user4,65,13) = 
                    string(substring(oeelk.user4,65,13),"xx/xx/xxx/xxx").
          else
             assign substring(oeelk.user4,65,13) = 
                    string(icsw.binloc1,"xx/xx/xxx/xxx").
   end.
end.

/*o Explode BOD/non-stock kits and print all components */
if {&kitfl} = yes     and
    can-find(first oeelk use-index k-oeelk where
        oeelk.cono      = {&line}.cono     and
        oeelk.ordertype = "o"           and
        oeelk.orderno   = {&orderno} and
        oeelk.ordersuf  = {&ordersuf} and
        oeelk.lineno    = {&line}.lineno) then
    for each oeelk use-index k-oeelk where
        oeelk.cono      = {&line}.cono     and
        oeelk.ordertype = "o"           and
        oeelk.orderno   = {&orderno}  and
        oeelk.ordersuf  = {&ordersuf} and
        oeelk.lineno    = {&line}.lineno
        
    no-lock
    break by substring(oeelk.user4,65,13):

        if oeelk.comptype = "r" then do:

            display oeelk.instructions with frame f-refer.
            down with frame f-refer.
            next.

        end.

        else do:

            assign v-wmfl = no.

            /*tb 18575 06/22/95 ajw; Nonstock/Special Kit Component.*/
            if oeelk.specnstype <> "n" then do:
                {w-icsw.i oeelk.shipprod v-whse no-lock}
                {w-icsp.i oeelk.shipprod no-lock}
            end.

            /*tb 18091 11/01/95 gp;  Assign product section of UPC number */

            if oeelk.specnstype = "n" or oeelk.arpvendno <> 0 then
                {icsv.gfi "'u'" oeelk.shipprod oeelk.arpvendno no}
            else if avail icsw then
                {icsv.gfi "'u'" oeelk.shipprod icsw.arpvendno no}

            assign
                s-upcpno    = if avail icsv then
                                  {upcno.gas &section  = "4"
                                             &sasc     = "v-"
                                             &icsv     = "icsv."
                                             &comdelim = "/*"}
                              else "00000".
            {&comr}
            /*tb 18575 06/22/95 ajw; Nonstock/Special Kit Component.*/
            if (oeelk.specnstype <> "n") and
               (avail icsw and icsw.wmfl) then do:

                find first wmet use-index k-ord where
                    wmet.cono = oeelk.cono          and
                    wmet.ordertype = "o"            and
                    wmet.orderno   = oeelk.orderno  and
                    wmet.ordersuf  = oeelk.ordersuf and
                    wmet.lineno    = oeelk.lineno   and
                    wmet.seqno     = oeelk.seqno no-lock no-error.

                v-wmfl = if avail wmet then yes else no.

            end. /* if avail icsw and wm */

            /*tb 18575 06/22/95 ajw; Nonstock/Special Kit Component.*/
            if not v-wmfl or oeelk.specnstype = "n" then do:
            /{&comr}* */
                /*d Print the component  */
                /*tb 18575 06/22/95 ajw; Nonstock/Special Kit Component.*/
                assign
                    s-binloc = if oeelk.specnstype = "n" then "Non Stock"
                               else if avail icsw then
                                   string(icsw.binloc1,"xx/xx/xxx/xxx")
                               else ""
                    v-kitconv = {unitconv.gas &decconv =
                                    "({&line}.stkqtyord / {&line}.qtyord)"}.

                clear frame f-oeel.

                /*tb 18575 06/22/95 ajw; Nonstock/Special Kit Component.*/
                /*tb 18091 11/01/95 gp;  Change UPC number display */
                display
                    oeelk.shipprod   @ {&line}.shipprod
                    (oeelk.qtyord * if {&returnfl} = yes then -1
                                    else 1) @ s-qtyord
                    oeelk.qtyneeded * v-qtyship * v-kitconv @ s-qtyship
                    oeelk.unit       @ {&line}.unit
                    s-upcpno
                    s-binloc
                    "Kit"            @ s-lineno
                    "____________ _____" @ s-underline
                with frame f-oeel.

                /*tb 18575 06/22/95 ajw; Nonstock/Special Kit Component.*/
                if (oeelk.specnstype <> "n") and
                   (avail icsw and icsw.binloc2 ne "") then
                    display icsw.binloc2 @ s-binloc2 with frame f-oeel.

                /*tb 18575 06/22/95 ajw; Nonstock/Special Kit Component.*/
                /*tb 9731 10/21/98 jgc; Expand non-stock description.
                    Added second display line for proddesc2. */
                if oeelk.specnstype = "n" then do:
                    display oeelk.proddesc @ s-descrip with frame f-oeel.
                    if {&line}.proddesc2 ne "" then do:
                        display {&line}.proddesc2 @ s-descrip3
                            with frame f-oeel2.
                        down with frame f-oeel2.
                    end.
                end.                
                else
                   if not avail icsp or not avail icsw then
                        display v-invalid @ s-descrip with frame f-oeel.
                    else
                        display icsp.descrip[1] @ s-descrip with frame f-oeel.

                down with frame f-oeel.

                /*tb 18575 06/22/95 ajw; Nonstock/Special Kit Component.*/
                /*tb 19484 09/16/96 mwb; added the down with frame line and
                    the 'do' and 'end' to go with it. */
                if oeelk.specnstype <> "n" and
                   avail icsp and icsp.descrip[2] ne "" then do:
                    display icsp.descrip[2] @ s-descrip3 with frame f-oeel2.
                    down with frame f-oeel2.
                end.

                /*tb 19484 09/16/96 mwb; added displaying of the counter bin
                     location for WL.  Can setup a product with a primary
                     counter bin location.  Needs to display with the other
                     bins for Primary Picking locations from WL */
                if v-wlwhsefl = yes and {&head}.transtype = "cs"
                then do for wlicsw:

                    {wlicsw.gfi &prod = "oeelk.shipprod"
                                &whse = "{&line}.whse"
                                &lock = "no"}

                    if avail wlicsw and wlicsw.bincntr ne "" then
                        display "            Counter Bin:" @ s-descrip3
                                wlicsw.bincntr @ s-binloc3 with frame f-oeel2.

                    /*tb 22380 12/26/96 mwb; added blank cs bin location */
                    else
                        display "            Counter Bin:" @ s-descrip3
                                "_____________" @ s-binloc3
                                with frame f-oeel2.

                    down with frame f-oeel2.
                end.
                /* end 19484 - CS Bin Location Print */

            {&comr}
            end. /* not wm display */

            /*d WM logic for the component */
            else do:

                assign
                    v-frameln = 1
                    s-binloc  = "Whse Mgr"
                    v-conv    = 1
                    v-kitconv = {unitconv.gas &decconv =
                                    "({&line}.stkqtyord / {&line}.qtyord)"}.

                if avail icsp then do:

                    {p-unit.i &prod       = "oeelk.shipprod"
                              &unitconvfl = "icsp.unitconvfl"
                              &unitstock  = "icsp.unitstock"
                              &unit       = "oeelk.unit"
                              &desc       = "v-desc"
                              &conv       = "v-conv"
                              &confirm    = "confirm"}.

                end.

                /*tb 18091 11/01/95 gp;  Change UPC number display */
                display
                    oeelk.shipprod   @ {&line}.shipprod
                    (oeelk.qtyord *
                        if {&returnfl} = yes then -1 else 1) @ s-qtyord
                    oeelk.qtyneeded * v-qtyship * v-kitconv @ s-qtyship
                    oeelk.unit       @ {&line}.unit
                    s-upcpno
                    s-binloc
                    "Kit"            @ s-lineno
                    "____________ _____" @ s-underline
                with frame f-oeel.

                for each wmet use-index k-ord where
                    wmet.cono      = oeelk.cono     and
                    wmet.ordertype = "o"            and
                    wmet.orderno   = oeelk.orderno  and
                    wmet.ordersuf  = oeelk.ordersuf and
                    wmet.lineno    = oeelk.lineno   and
                    wmet.seqno     = oeelk.seqno
                no-lock:

                    if v-frameln = 1 then do:

                        assign
                            s-wmqtyship = wmet.qtyactual / v-conv
                            s-wmqtyship = if {&returnfl} = yes or
                                              oeelk.qtyneeded < 0 then
                                              s-wmqtyship * -1
                                          else if s-wmqtyship < 0 then 0
                                          else s-wmqtyship
                            s-descrip2  = "                        " +
                                             string(s-wmqtyship,">>>>>>>>9.99-")
                            s-binloc2   = wmet.binloc.

                        display
                            icsp.descrip[1] @ s-descrip
                            s-binloc2
                            s-descrip2
                        with frame f-oeel.
                        down with frame f-oeel.

                        v-frameln = v-frameln + 1.

                    end. /* frameln 1 */

                    else do:

                        assign
                            s-wmqtyship = wmet.qtyactual / v-conv
                            s-wmqtyship = if {&returnfl} = yes or
                                              oeelk.qtyneeded < 0 then
                                              s-wmqtyship * -1
                                          else if s-wmqtyship < 0 then 0
                                          else s-wmqtyship
                            s-binlocwm  = wmet.binloc.

                        display
                            s-binlocwm
                            s-wmqtyship
                        with frame f-oewm.
                        down with frame f-oewm.

                    end. /* else do */

                end. /* for each wmet */

            end. /* wm logic for the component */
            /{&comr}* */

            /*d Print the component product notes  */
            /*tb 18575 06/22/95 ajw; Nonstock/Special Kit Component. */
            if oeelk.specnstype <> "n" and
               avail icsp and icsp.notesfl ne "" then
                for each notes where
                    notes.cono         = g-cono    and
                    notes.notestype    = "p"       and
                    notes.primarykey   = icsp.prod and
                    notes.secondarykey = ""        and
                    notes.printfl      = yes
                no-lock:

                    do i = 1 to 16:

                        if notes.noteln[i] ne "" then do:

                            display notes.noteln[i] with frame f-notes.
                            down with frame f-notes.

                        end.

                    end. /* do i */

                end. /* for each notes */

            /*d Print the component serial/lot records */
            /*tb 18575 06/22/95 ajw; Nonstock/Special Kit Component.*/
            /*tb 13356 04/27/95 mtt; Serial/Lot # doesn't print on PO/RM */
            if oeelk.specnstype <> "n" then do:
                {p-oeepp1.i &type       = "o"
                            &line       = "{&line}"
                            &pref       = "{&prefix}"
                            &seq        = oeelk.seqno
                            &stkqtyship = "{&line}.stkqtyship"
                            &snlot      = "{&nosnlots}"}

                assign
                    s-prod     = if oeelk.reqprod = "" then oeelk.shipprod
                                 else oeelk.reqprod
                    v-reqtitle = if oeelk.reqprod <> "" then "  Requested Prod:"
                                 else     "   Customer Prod:".

                {n-icseca.i "first" s-prod ""c"" {&custno} no-lock}

                if avail icsec then do:

                    if oeelk.reqprod <> "" then do:

                        display
                            s-prod
                            v-reqtitle
                        with frame f-oeelc.
                        down with frame f-oeelc.

                    end.

                    assign
                        s-prod     = icsec.prod
                        v-reqtitle = "   Customer Prod:".

                    display
                        s-prod
                        v-reqtitle
                    with frame f-oeelc.

                end. /* avail icsec */
            end. /* End of Specnstype <> "n" */


            /*tb 19484 08/20/96 gp; Create a WLELK record for each component
                except reference components */
            /*tb 19484 09/16/96 mwb; Added the CS edit per line.  No need to
                write the data to WL for a Counter Sale.  The interface moved
                to oeepi for a Stk Move out of oh-hand in WL. */
            /*tb 19484 10/03/96 des; Warehouse Logistics Only allow type "c"
                components to create a wlelk record transaction */
            /*tb 23455 07/17/97 mwb; Added stage edit for oeeh.stagecd < 3 */
            /*tb 24253 12/01/97 mwb; Added the tag and hold edit */
            /*tb 25120 09/10/98 gkd; Removed tag and hold edit */
            if v-wlwhsefl = yes and {&head}.transtype ne "cs" and
                oeelk.comptype = "c" {&twlstgedit}
            then do:
                /*d Create WLELK records for each component */
                /*tb 19484 10/30/96 ajw; (B13) Warehouse Logistics -Shipmentid*/
                /*tb 22557 02/10/97 mwb; Removed Negative Component edit from
                    the invoutcond param - 'and oeelk.stkqtyship >= 0'.
                    Was after the returnfl edit. */
                {wlpproc.gpr &invoutcond    = "{&head}.transtype <> ""rm"" and
                                              {&returnfl} = no"
                             &wlsetno       = v-wlsetno
                             &whse          = oeelk.whse
                             &ordertype     = ""o""
                             &orderno       = oeelk.orderno
                             &ordersuf      = oeelk.ordersuf
                             &lineno        = oeelk.lineno
                             &seqno         = oeelk.seqno
                             &msdssheetno   = """"
                             &updtype       = ""A""
                             &shipmentid    = """"}

            end. /* end if v-wlwhsefl... */

        end. /* else do */

    end. /* for each oeelk */

/*o Explode Mix kits and print all components */
else
if /*oeel.tallyfl = yes     or      */             /*Mix kits OR Memo tallys*/
    can-find(first oeelm use-index k-oeelm where
        oeelm.cono      = {&line}.cono     and
        oeelm.ordertype = "o"           and
        oeelm.orderno   = {&orderno} and
        oeelm.ordersuf  = {&ordersuf} and
        oeelm.lineno    = {&line}.lineno) then
    for each oeelm use-index k-oeelm where
        oeelm.cono      = {&line}.cono     and
        oeelm.ordertype = "o"           and
        oeelm.orderno   = {&orderno}  and
        oeelm.ordersuf  = {&ordersuf} and

        oeelm.lineno    = {&line}.lineno
    no-lock:

        assign v-wmfl = no.

        {w-icsp.i {&line}.shipprod no-lock}
        v-memomixfl = if avail icsp then icsp.memomixfl else no.

        {w-icsw.i oeelm.shipprod v-whse no-lock}
        {w-icsp.i oeelm.shipprod no-lock}

        if avail icsw then
            {icsv.gfi "'u'" oeelm.shipprod icsw.arpvendno no}

            assign
                s-upcpno    = if avail icsv then
                                  {upcno.gas &section  = "4"
                                             &sasc     = "v-"
                                             &icsv     = "icsv."
                                             &comdelim = "/*"}
                              else "00000".

                /*d Print the component  */
                assign
                    s-binloc = if avail icsw then
                                   string(icsw.binloc1,"xx/xx/xxx/xxx")
                               else ""
                    v-kitconv = {unitconv.gas &decconv =
                                    "({&line}.stkqtyord / {&line}.qtyord)"}.

                clear frame f-oeel.

                display
                    if v-memomixfl then
                        ("Length: " + string(oeelm.length))
                    else
                        oeelm.shipprod   @ {&line}.shipprod
                    (oeelm.qtyord * if {&returnfl} = yes then -1
                                    else 1) @ s-qtyord
                    (oeelm.stkqtyord - oeelm.prevqtyshp) @ s-qtyship
     /********      (oeelm.stkqtyord / s-qtyord)      /* qtyneeded */
                        * v-qtyship * v-kitconv @ s-qtyship            ****/
                    oeelm.unit       @ {&line}.unit
                    if v-memomixfl and avail icsp then
                        ""
                    else s-upcpno @ s-upcpno
                    if v-memomixfl then "" else s-binloc @ s-binloc
                    if v-memomixfl then
                        "Mem"
                    else "Tly"            @ s-lineno
                    "____________ _____" @ s-underline
                with frame f-oeel.

                /*tb e4369 03/06/00 cm; Memo tally components display with
                    "** Invalid **" */
                if not v-memomixfl then do:
                    if avail icsw and icsw.binloc2 ne "" then
                        display icsw.binloc2 @ s-binloc2 with frame f-oeel.

                    if not avail icsp or not avail icsw then
                        display v-invalid @ s-descrip with frame f-oeel.
                    else
                        display icsp.descrip[1] @ s-descrip with frame f-oeel.
                end.

                down with frame f-oeel.

                if avail icsp and icsp.descrip[2] ne "" and v-memomixfl eq no
                then do:
                    display icsp.descrip[2] @ s-descrip3 with frame f-oeel2.
                    down with frame f-oeel2.
                end.

            /*d Print the component product notes  */
            if avail icsp and icsp.notesfl ne "" then
                for each notes where
                    notes.cono         = g-cono    and
                    notes.notestype    = "p"       and
                    notes.primarykey   = icsp.prod and
                    notes.secondarykey = ""        and
                    notes.printfl      = yes
                no-lock:

/* SX32 upgrade mod
                    do i = 1 to 16:

                        if notes.noteln[i] ne "" then do:

                            display notes.noteln[i] with frame f-notes.
                            down with frame f-notes.

                        end.

                    end. /* do i */
*/
                run notesprt.p (g-cono,"p",icsp.prod,"","oeepp",8,1).    
                end. /* for each notes */

            /*d Print the component serial/lot records */
                {p-oeepp1.i &type       = "o"
                            &line       = "{&line}"
                            &pref       = "{&prefix}"
                            &seq        = oeelm.seqno
                            &stkqtyship = "{&line}.stkqtyship"
                            &snlot      = "{&nosnlots}"}

                assign
                    s-prod     = if oeelm.reqprod = "" then oeelm.shipprod
                                 else oeelm.reqprod
                    v-reqtitle = if oeelm.reqprod <> "" then "  Requested Prod:"
                                 else     "   Customer Prod:".

                {n-icseca.i "first" s-prod ""c"" {&custno} no-lock}

                if avail icsec then do:

                    if oeelm.reqprod <> "" then do:

                        display
                            s-prod
                            v-reqtitle
                        with frame f-oeelc.
                        down with frame f-oeelc.

                    end.

                    assign
                        s-prod     = icsec.prod
                        v-reqtitle = "   Customer Prod:".

                    display
                        s-prod
                        v-reqtitle
                    with frame f-oeelc.

                end. /* avail icsec */

end. /* for each oeelm */  /*Mix Tally Kit */

/*o Reset the line item print flag */
if {&line}.printpckfl = yes and s-lastfl then
    run oeeppu.p("{&ordertype}",
                 {c-empty.i},
                 " ",
                 " ",
                 g-today,
                 output v-printfl,
                 {&orderno},
                 {&ordersuf},
                 {&line}.lineno,
                 {&seqno}).


