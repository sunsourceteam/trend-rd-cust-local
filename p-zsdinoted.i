/*h*****************************************************************************
  INCLUDE      : p-noted.i
  DESCRIPTION  : Ensures that the note request is valid
  USED ONCE?   : yes (noted.p)
  AUTHOR       : r&d
  DATE WRITTEN : 01/01/90
  CHANGES MADE :
    10/15/92 mwb; TB# 8281; stripped out the CM and TB
                  edits. (cmnoted.p and tbnoted.p - now
                  run independant of noted.p).
    05/26/93 rhl; TB# 11569 BP should use notes file not notesbp
    10/04/93 mtt; TB#12544;General Project TB for Trend Quality
    10/21/93 mwb; TB# 13427; added 'ba' edit for OE batch notes.
    11/04/93 dww; TB# 13598 Add Notes to Supplier Link. Scoping
    06/30/94 bj;  TB# 16018 Add Notes to A/P Transaction
    07/16/94 bj;  TB# 16103 Add Notes to A/R Transaction
    06/30/95 mtt; TB# 18611 Make changes for KP Work Order Demand (Y1)
    02/14/96 gdf; TB# 18176 GL accounts to have notes (F-20)
    03/22/96 dww; TB# 14816 Notes on InterCompany WT not accessible from
        Receiving Company.  Whenever the Note is for a WT, the WT Header record
        is read and the Company is set to the Shipping Company, since WT Notes
        are always created from the Shipping Cono.
    09/09/96 mtt; TB# 21254 (2Q) Develop Value Add Module
    11/27/96 gdf; TB# 21081 Add notes to gleb records.
    12/23/98 cm;  TB# 14835 Allow notes specific by suffix
    04/27/99 lbr; TB# 5366  Added custno and notes type cs for shipto notes
    01/20/03 bp ; TB# e14645 Rollback: Add notes to PMES ship requests.
*******************************************************************************/

v-okfl = no.

/*tb 5956, mwb 03/23/92 - set the int to a decimal **/
if v-notestype = "C" and {v-arsc.i "{c-asignd.i v-primarykey 1 12}" "/*"}
    then v-okfl = yes.

/*tb 5366, lbr 04/27/99 - Make sure an arss record exists */
else if v-notestype = "CS" and
    {v-arss.i "{c-asignd.i v-primarykey 1 12}" v-secondarykey}
    then v-okfl = yes.

/*tb 14835 12/23/98 cm; Allow notes specific by suffix */
else if v-notestype = "O" and
  {v-oeeh.i integer(v-primarykey) integer(v-suffix) "/*"}
    then v-okfl = yes.

else if v-notestype = "P" and {v-icsp.i v-primarykey "/*"} then v-okfl = yes.

else if v-notestype = "g" and {v-icsc.i v-primarykey "/*"} then v-okfl = yes.

/*tb 14835 12/23/98 cm; Allow notes specific by suffix */
else if v-notestype = "T" and
  {v-wteh.i integer(v-primarykey) integer(v-suffix) "/*"}
    then v-okfl = yes.

else if v-notestype = "V" and {v-apsv.i decimal(v-primarykey) "/*"}
    then v-okfl = yes.

/*tb 14835 12/23/98 cm; Allow notes specific by suffix */
else if v-notestype = "X" and
  {v-poeh.i integer(v-primarykey) integer(v-suffix) "/*"}
    then v-okfl = yes.

/*  KP work order */
else if v-notestype = "W"
    /*tb 18611 06/30/95 mtt; Make changes for KP Work Order Demand */
    then v-okfl = yes.

else if v-notestype = "B" and {v-bprev.i v-primarykey 1} then v-okfl = yes.

else if v-notestype = "BV" and
    {v-bpehv.i v-primarykey decimal(v-secondarykey) 0}
    then v-okfl = yes.

else if v-notestype = "BC" and {v-bpehc.i g-bpid g-revno g-custpros g-cptype}
    then v-okfl = yes.

else if v-notestype = "VL" and {v-bpelv.i g-bpid g-revno g-vendno g-quoteno}
    then v-okfl = yes.

/*tb 12544 10/04/93 mtt; General Project TB for Trend Quality */
else if v-notestype = "e" then do for tqee:
    find tqee where recid(tqee) = g-tqrecid no-lock no-error.
    v-okfl = if avail tqee then yes else v-okfl.
end.

/*tb 13427 10/21/93 mwb - oe batch */
else if v-notestype = "zba" then do:

    v-okfl = if can-find(oeehb use-index k-oeehb where
                         oeehb.cono     = g-cono        and
                         oeehb.batchnm  = v-primarykey /* and
                         oeehb.seqno    = integer(v-secondarykey)*/)
                 then yes
             else v-okfl.

end.

/*tb 13598 11/04/93 dww; Add Notes to Supplier Link */
/*d Supplier Link */
else if v-notestype = "sl" then
    v-okfl = yes.

/*tb 16018 06/30/94 bj; Added A/P Trans */
/*d A/P Trans */
else if v-notestype = "ap" and v-primarykey ne "" and v-secondarykey ne "" then
    v-okfl = yes.

/*tb 16103 07/14/94 bj; Added A/R Trans */
/*d A/R Trans */
else if v-notestype = "ar" and v-primarykey ne "" and v-secondarykey ne "" then
    v-okfl = yes.

/*tb 18176 02/14/96 gdf; Added GL Account # notes check */
else if v-notestype = "gl" and v-primarykey ne "" then
    v-okfl = yes.

/*tb 21254 09/09/96 mtt; Develop Value Add Module */
/*tb 14835 12/23/98 cm; Allow notes specific by suffix */
else if v-notestype = "fh" and v-primarykey ne "" and
    {vaeh.gvl &vano        = integer(v-primarykey)
              &vasuf       = integer(v-suffix)
              &comoptional = "/*" }
    then v-okfl = yes.

else if can-do("fs,fa,fb",v-notestype)
    then v-okfl = yes.

/*tb 21081 11/27/96 gdf; Added Balance History notes check */
else if v-notestype = "bh" and v-primarykey ne "" and v-secondarykey ne "" then
    v-okfl = yes.

/*tb 14835 12/23/98 cm; Allow notes specific by suffix */
else if v-notestype = "RO" and
    {v-sweh.i int(v-primarykey) int(v-suffix) "/*"} then v-okfl = yes.

else if v-notestype = "RL" and
    {swel.lvl g-repairordno g-repairordsuf integer(v-secondarykey) "/*"} then
    v-okfl = yes.

else if v-notestype = "WC" and
    {v-swewh.i integer(v-primarykey) "/*"} then v-okfl = yes.

else if v-notestype = "S" then v-okfl = yes.

else if v-notestype = "SR" and
    can-find(pmes where pmes.cono = g-cono and
               pmes.shipreqno = int(v-primarykey)) then v-okfl = yes.
