/* SX 55
   f-arscc.i 1.2 05/22/98 */
/* f-arscc.i 1.8 01/10/95 */
/*h*****************************************************************************
  INCLUDE      : f-arscc.i
  DESCRIPTION  : Form for ARSC Credit Information Screen
  USED ONCE?   : no (arscc.p, arrmc.p)
  AUTHOR       : dea
  DATE WRITTEN : ?
  CHANGES MADE :
    05/12/93 jrg; TB# 10577 - Added high balance
    07/20/93 jrg; TB# 10584 - Added title
    10/01/93 pjt; TB# 13186 - Reformatted avgpaydays
    12/02/93 dea; TB# 13822 - Changed lastpaydt from COLON to AT to work with V7
    01/05/94 tdd; TB# 14112 - Added PM Cash Only flag
    01/10/95 dww; TB# 17474 Forms misalignment in Progress V7.
    05/21/98 rh;  TB# 21742 Validate credit manager initials. Added validation
        to arsc.creditmgr
    11/06/01 kjb; TB# e2614 Remove PM from the cash only flag since we do not
        support Parcel Management anymore.  Also change help message to
                indicate that this field is used by Clippership.
    06/02/03 bp ; TB# e16716 Convert lockbox functionality to standard.
    07/14/03 bp ; TB# e17940 Do not abbreviate the word "Lockbox".
    04/07/06 ejk; TB# e19011 correct format of arsc.avgpaydays
******************************************************************************/

arsc.creditmgr      colon 20
    validate({v-sasoo.i arsc.creditmgr},
                 "Operator Not Set Up in Operator Setup - SASOO (4008)")
arsc.credlim        colon 52
 {arscc.z99 &user_framecredlim = "*"} 
arsc.crestdt        colon 20         arsc.highbal     colon 52
arsc.lastrevdt      colon 20         arsc.holdpercd   colon 52
 {arscc.z99 &user_framehighbal = "*"} 

arsc.nextrevdt      colon 20         arsc.selltype    colon 52

 /*tb 14112 01/05/94 tdd; Add PM Cash Only flag */
arsc.pmcashfl       colon 73 label "Cash Only"
          help "Yes If Cash Only Accepted (Clippership Only)"
arsc.lbxpostty      colon 20 label "Lockbox Post Type"
    validate(can-do("o,s,,t":u,arsc.lbxpostty),
                    "Valid Values are 'O'ldest, 'S'tatement, 'T'otal or Blank")
    help "(O)ldest, (S)tatement, (T)otal or Blank"

arsc.statusdt    colon 56

arsc.apmgr          colon 20
arsc.lastpayamt     colon 56         arsc.lastpaydt   at 71 no-label
arsc.apphoneno      colon 20         arsc.pastduedt   colon 56
arsc.banknm         colon 20         arsc.nopastdue   colon 56
arsc.bankmgr        colon 20         arsc.avgpaydays  colon 56
arsc.bankphoneno    colon 20         arsc.nopay       colon 56
arsc.bankacct       colon 20         arsc.noinv       colon 56
arsc.securfl        colon 20         arsc.enterdt     colon 56
" Credit Services " at 30
arsc.dunsno         colon 20
arsc.lastrtg[1]     colon 56 label "Last Rating"
arsc.lastrtgdt[1]   at 64 no-label
arsc.crsname        colon 20
arsc.lastrtg[2]     colon 56 label "Last Rating"
arsc.lastrtgdt[2]   at 64 no-label
arsc.crref[1]       colon 20 label "Credit References"
arsc.crref[2]       at 47 no-label



with frame f-arscc side-labels title " Credit Information "
    row 4 column 1 overlay width 80 no-hide
