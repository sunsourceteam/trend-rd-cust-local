/****************************************************************************
  INCLUDE      : zsdivakits..p
  DESCRIPTION  : Program is run from vaio.z99 for vaio inquiry. F7 , F8 will 
                 display the item availablity. It will toggle between missing
                 and avaialble va order components.   
  USED ONCE?   : no
  AUTHOR       : SunSource
  DATE WRITTEN : 06/03/08
  CHANGES MADE :
  Mode                  Description
  --------------        ------------------------------------------------------
******************************************************************************/

define input parameter ix-orderno  like vaeh.vano     no-undo.
define input parameter ix-ordersuf like vaeh.vasuf    no-undo.
define input parameter ix-lineno   like vaesl.lineno  no-undo.
define input parameter ix-mode     as   character     no-undo.

{g-all.i}
{g-oe.i}
{g-ic.i}
{g-inqpd.i}
define var  g-forcelufl as logical no-undo.
define var  g-luvalue as char format "x(24)" no-undo.
def var v-myframe     as c                   no-undo.
def var s-mth         as c    format "x(10)" no-undo.

def var h-modulenm as character               no-undo.
def var h-menusel  as character               no-undo.
def var h-bottom    like g-bottom             no-undo.
def var h-inqtype   like sassm.inqtype        no-undo.
def var h-inqtitle  like sassm.inqtitle       no-undo.
def var h-inqproc   like sassm.inqproc        no-undo.
def var h-orderno   like oeeh.orderno         no-undo.
def var h-ordersuf  like oeeh.ordersuf        no-undo.
def var h-lineno    like oeel.lineno          no-undo.
def var h-prod      like icsw.prod            no-undo.
def var h-whse      like icsw.whse            no-undo.
def var v-com       as c format "x"           no-undo.
def var v-prtlbl    as c format "x"           no-undo.
def var v-putscreen as c format "x(76)"       no-undo.
def var r-comments1 as c format "x(60)"       no-undo. 
def var r-comments2 as c format "x(60)"       no-undo. 
def var r-comments3 as c format "x(60)"       no-undo. 
def var r-comments4 as c format "x(60)"       no-undo. 
def var r-comments5 as c format "x(60)"       no-undo. 
def var r-comments6 as c format "x(60)"       no-undo. 
def var r-comments7 as c format "x(60)"       no-undo. 
def var r-shipprod    like oeel.shipprod            no-undo. 
def var r-arpvendno   like oeel.arpvendno           no-undo. 
def var r-prodcst     as dec format "zzzzzz9.9999"  no-undo. 
def var r-shipfmno    like oeel.shipfmno            no-undo. 
def var r-reqshipdt   like oeel.reqshipdt           no-undo. 
def var r-arpprodline like oeel.arpprodline         no-undo. 
def var r-shipvia     as c format "x(4)"            no-undo. 
def var r-leadtm      as c format "x(4)"            no-undo. 
def var r-whse        like oeel.whse                no-undo. 
def var s-netavail    like icsw.qtyonhand           no-undo.

def var lastchg      as char format "x(6)" extent 2
                     initial ["F7","F7"]            no-undo.
def var firstf8      as logical init "yes"          no-undo.

def var v-currentactivity as character init "F7" no-undo.
def var lackingFlag  as logical no-undo.
def var lackingFlag2 as logical no-undo.
def var Stage1Flag as logical no-undo.

def var x-running   as logical no-undo init true.

def var x-missing as logical no-undo.
def var x-cost    like oeel.prodcost format "zzzzzz.99" no-undo.
def var x-intcost like oeel.prodcost format "zzzzz.99"  no-undo.
def var x-totcost like oeel.prodcost format "zzzzzz.99" no-undo.
def var x-rebate  as char format "x(1)" no-undo.
def var x-functions2  as character format "x(76)" no-undo.
def var x-label       as character format "x(18)" no-undo.
def var v-cost    like oeel.prodcost no-undo.

def buffer q-vaesl for vaesl.
def buffer q-vaes  for vaes.

def buffer q-icsw  for icsw.
def buffer q-icsp  for icsp.
def buffer d-notes for notes.
def buffer p-notes for notes.
def buffer q-com   for com.  
def buffer z-vaesl for vaesl.

def var    q-proc  as logical no-undo.

define query 
  q-wodtl for q-vaesl,vaeh,q-vaes,icsp,icsw,com scrolling.
define browse b-wodtl query q-wodtl
  display 
    string(string(q-vaesl.nonstockty,"x")   + string(v-com,"x")  +
           string(q-vaesl.seqno,"99") + "-" + 
           string(q-vaesl.lineno,"999")) format "x(8)"
    q-vaesl.shipprod     
    q-vaesl.qtyneeded format "zzzz9.99-"
    q-vaesl.qtyord    format "zzzz9.99-"
    /***
    (if ix-mode = "a" then
       if avail icsp and icsp.statustype = "l" then
         q-vaesl.qtyord 
       else
       if q-vaesl.nonstockty = "n" or
         (avail icsw and q-vaesl.nonstockty = "s" and
          icsw.statustype <> "o") then
          q-vaesl.qtyship 
       else
         q-vaesl.qtyship              
     else
       q-vaesl.qtyship) format "zzzz9.99"
     ***/
     q-vaesl.xxde4  format "zzzz9.99-"
     v-prtlbl 
     q-vaesl.prodcost format "zzzzz9.99-"

with 10 down width 78 centered overlay no-hide no-labels 
title 
"N/S SeqLn# Part                      QtyNeeded  QtyOrd   NetAvail   UnitCost ".

form                                                            
  ix-orderno             at 1 label "Va# "                  
  space(0) "-" space(0)
  ix-ordersuf            at 15 no-label
  "IN Section's Only"    at 20 
  x-missing              at 48 dcolor 2 label "Missing Only?"
  x-cost                 at 1  label "TotlInvCost"
  x-intcost              at 25 label "TotlIntCost"
  x-totcost              at 48 label "Total Inv+Int Cost" 
  b-wodtl                at row 3  col 1      
  x-functions2           at row 14 col 2 no-label dcolor 2   
  with frame f-wodtl width 80 row 5 centered side-labels                  
  no-hide overlay title 
"                     IN Section - Inventory Detail                          ".

form 
 x-label                 at 1 no-label dcolor 2
 with frame f-keylabels width 23 row 22 col 61 no-box.

assign x-label = "Ctrl-p Missing/All".

display x-label with frame f-keylabels. 

form 
   r-shipprod        at 10    label "   Product" 
   skip 
   skip 
   r-arpvendno       at 10    label "ARP Vendor" 
   r-prodcst         at 35    label "Cost      " 
   r-whse            at 10    label "ARP Whse  " 
   r-arpprodline     at 10    label "Prod Line "
   r-shipvia         at 35    label "Ship Via  "
   r-leadtm          at 35    label "LeadTime  " 
   "Comments:"       at 3 
   r-comments1       at 3    no-label 
   r-comments2       at 3    no-label 
   r-comments3       at 3    no-label 
   r-comments4       at 3    no-label 
   r-comments5       at 3    no-label 
   r-comments6       at 3    no-label 
   r-comments7       at 3    no-label 
   with frame f-rarrnotes overlay side-labels width 65 column 8 row 6 title
   "             Service Repair Purchasing Comments                  ". 


assign x-missing = no. 

on row-display of b-wodtl in frame f-wodtl
do: 
  assign v-com = " ". 
  find vaes where 
       vaes.cono  = g-cono        and 
       vaes.vano  = q-vaesl.vano  and 
       vaes.vasuf = q-vaesl.vasuf and 
       vaes.seqno = q-vaesl.seqno no-lock no-error. 
  if avail vaes then do: 
   if vaes.stagecd = 3 and q-vaesl.qtyship > 0 then      
    assign v-prtlbl = "p". 
   else 
    assign v-prtlbl = " ".    
  end.       
  if avail q-vaesl and q-vaesl.sctntype = "in" then
    do:
    find icsw where icsw.cono = g-cono and
                    icsw.whse = q-vaesl.whse and
                    icsw.prod = q-vaesl.shipprod
                    no-lock no-error.
    for each z-vaesl where z-vaesl.cono = q-vaesl.cono and
                       z-vaesl.vano     = q-vaesl.vano and
                       z-vaesl.vasuf    = q-vaesl.vasuf and
                       z-vaesl.seqno    = q-vaesl.seqno and
                       z-vaesl.lineno   = q-vaesl.lineno and
                       z-vaesl.shipprod = q-vaesl.shipprod:
      if avail icsw then
        assign z-vaesl.xxde4 = icsw.qtyonhand - icsw.qtyreservd -
                               icsw.qtycommit.
    end.
    /*
    if g-operinit = "das" then
      do:
      message "q-vaesl.whse:" q-vaesl.whse
              "q-vaesl.shipprod:" q-vaesl.shipprod
              "icsw.whse:" icsw.whse
              "icsw.prod:" icsw.prod
              "icsw.qtyonhand:" icsw.qtyonhand. pause.
    end.
    */
  end.
  if avail q-vaesl and q-vaesl.seqno = 4 then do: 
   find q-com where q-com.cono = g-cono and 
        q-com.comtype  = (if avail vaeh and num-entries(vaeh.user2,"-") > 1 
                          then 
                           entry(1,vaeh.user2,"-") + "-" +         
                           left-trim(entry(2,vaeh.user2,"-"),"0")      
                          else 
                           "fl" + string(q-vaesl.seqno,"zz9")) and
        q-com.lineno   = q-vaesl.lineno 
        no-lock no-error.
   if avail q-com then 
    assign v-com = "!". 
   else 
    assign v-com = " ". 
  end. 
end.
 

on go of b-wodtl in frame f-wodtl
do:
 if lastkey = 401 then do: 
  readkey pause 0. 
  apply 404.
 end. 
end. 

on any-key of b-wodtl 
do:
    if keylabel(lastkey) = "PF4" or keylabel(lastkey) = "F11" then
     assign x-running = false.
    if keylabel(lastkey) = "F6" then do: 
     disable b-wodtl with frame f-wodtl.
     v-myframe = "poizs".
     run titlemorph.
     assign g-bottom = ""
          h-modulenm = g-modulenm
          h-menusel  = g-menusel
          g-modulenm = "PO"
          g-menusel  = "IZS".
     {w-sassm.i "'poizs'" no-lock}
     do i = 1 to 5:
        assign
          h-inqproc[i]    = g-inqproc[i]
          g-inqproc[i]    = sassm.inqproc[i]
          h-inqtype[i]    = g-inqtype[i]
          g-inqtype[i]    = if sassm.inqtype[i] = "j" then "p" 
                            else sassm.inqtype[1]
          h-inqtitle[i]   = g-inqtitle[i]
          g-inqtitle[i]   = sassm.inqtitle[i]
          substring(g-bottom,(i * 15) - 12) =
                    if g-inqtitle[i] = "" then ""
                    else
                      (if i = 1 then "F6-" else
                       if i = 2 then "F7-" else
                       if i = 3 then "F8-" else
                       if i = 4 then "F9-" else "F10-")
                     + g-inqtitle[i].
     end.
     assign h-orderno   = g-orderno
            h-ordersuf  = g-ordersuf
            h-prod      = g-prod
            h-whse      = g-whse
            g-prod      = if avail q-vaesl then 
                           q-vaesl.shipprod  
                          else 
                           "" 
            g-whse      = if avail q-vaesl then 
                           q-vaesl.whse
                          else 
                           "".
     put screen row 21 col 3 color message 
"                                                                            ".
     message "". 
     if g-currproc = "poizs" then do: 
      hide frame f-keylabels.
     end.
     run poizs.p.
     do i = 1 to 5:
      assign g-inqproc[i]    = h-inqproc[i]
             g-inqtype[i]    = h-inqtype[i]
             g-inqtitle[i]   = h-inqtitle[i].
     end.

    {putscr.gsc &color     = "messages"
                &f6        = "*"
                &f6text    = "Sections"
                &f8        = "*"
                &f8text    = "Ties"
                &f9        = "*"
                &f9text    = "Serial/Lot"}

     assign g-modulenm = h-modulenm
            g-menusel  = h-menusel
            g-orderno  = h-orderno
            g-ordersuf = h-ordersuf
            g-prod     = h-prod
            g-whse     = h-whse.
     on cursor-down cursor-down.
     on cursor-up   cursor-up.
     enable b-wodtl with frame f-wodtl.
     apply "ENTRY" to b-wodtl in frame f-wodtl.
     display x-functions2 with frame f-wodtl.
     on cursor-down cursor-down.
     on cursor-up   cursor-up.
    end.
   else 
   if keylabel(lastkey) = "F7" then do: 
    if v-currentactivity = "F7" then
      if lackingflag  then lackingflag = false.
      else if not lackingflag  then lackingflag = true. 
    assign lackingFlag2 = lackingFlag
           v-currentactivity = "F7".  
    assign x-missing = lackingFlag.
    close query q-wodtl.
    on cursor-down cursor-down.
    on cursor-up   cursor-up.
    run Stage1Query. 
    find vaeh where
         vaeh.cono  = g-cono      and  
         vaeh.vano  = ix-orderno  and 
         vaeh.vasuf = ix-ordersuf  
         no-lock no-error.
    if not lackingFlag then do: 
     if avail vaeh and vaeh.user2 ne "" then 
      assign x-functions2 =                                                   
"  F6-Poizs  (F7-INOPEN , F8-InOpen/InPrt) missing/ALL    F10-Svc/PoComments ".
     else do: 
      assign x-functions2 =                                                   
"  F6-Poizs  (F7-INOPEN , F8-InOpen/InPrt) missing/ALL                       ".
     end. 
    end. 
    else 
    if lackingFlag then do: 
     if avail vaeh and vaeh.user2 ne "" then 
      assign x-functions2 =                                                   
"  F6-Poizs  (F7-INOPEN , F8-InOpen/InPrt) MISSING/all    F10-Svc/PoComments ".
     else 
      assign x-functions2 =                                                   
"  F6-Poizs  (F7-INOPEN , F8-InOpen/InPrt) MISSING/all                       ".
    end. 
    display x-functions2 x-missing with frame f-wodtl.
   end. 
   else 
   if  keylabel(lastkey) = "F8" then do: 
    if v-currentactivity = "F8" then
      if lackingflag2  then lackingflag2 = false.
      else if not lackingflag2  then lackingflag2 = true. 
    assign lackingFlag = lackingFlag2
           v-currentactivity = "F8".  
    assign x-missing = lackingFlag2.
    if not lackingFlag2 then do:  
     if avail vaeh and vaeh.user2 ne "" then 
      assign x-functions2 =                                                   
"  F6-Poizs  (F7-InOpen , F8-INOPEN/INPRT) missing/ALL    F10-Svc/PoComments ".
     else 
      assign x-functions2 =                                                   
"  F6-Poizs  (F7-InOpen , F8-INOPEN/INPRT) missing/ALL                       ".
    end.   
    else 
    if lackingFlag2 then do: 
     if avail vaeh and vaeh.user2 ne "" then 
      assign x-functions2 =                                                   
"  F6-Poizs  (F7-InOpen , F8-INOPEN/INPRT) MISSING/all    F10-Svc/PoComments ".
     else 
      assign x-functions2 =                                                   
"  F6-Poizs  (F7-InOpen , F8-INOPEN/INPRT) MISSING/all                       ".
    end.   
    
    display ix-orderno      
            ix-ordersuf
            x-missing   
            x-cost   
            x-intcost 
            x-totcost
            x-functions2
            with frame f-wodtl.        
    close query q-wodtl.
    assign q-proc = true.
    on cursor-down cursor-down.
    on cursor-up   cursor-up.
    open query q-wodtl 
     for each q-vaesl use-index k-vaesl
        where q-vaesl.cono       = g-cono
          and q-vaesl.vano       = ix-orderno
          and q-vaesl.vasuf      = ix-ordersuf
          and q-vaesl.sctntype   = "in"  
          and q-vaesl.completefl = false
          and q-vaesl.shipprod ne ""           and 
            ((q-vaesl.qtyord > q-vaesl.qtyship and 
              q-vaesl.boseqno = 0 and lackingFlag2) or 
             (lackingFlag2 = false and
               (q-vaesl.boseqno = 0 )

/*            ((q-vaesl.qtyship = 0 and q-vaesl.printpckfl = yes) or 
               (q-vaesl.qtyship > 0 and q-vaesl.printpckfl = no))*/ ) ) 
              no-lock,
         each vaeh outer-join 
        where vaeh.cono  = g-cono      and  
              vaeh.vano  = ix-orderno  and 
              vaeh.vasuf = ix-ordersuf  
              no-lock,
         each q-vaes  
        where q-vaes.cono  = g-cono        and  
              q-vaes.vano  = q-vaesl.vano  and 
              q-vaes.vasuf = q-vaesl.vasuf and 
              q-vaes.seqno = q-vaesl.seqno        
              no-lock,
         each icsp outer-join
        where icsp.cono = g-cono and
              icsp.prod = q-vaesl.shipprod no-lock,
         each icsw outer-join
        where icsw.cono = g-cono       and
              icsw.whse = q-vaesl.whse and
              icsw.prod = q-vaesl.shipprod no-lock,
         each com outer-join
        where com.cono = g-cono               and 
              com.comtype  = (if avail vaeh and num-entries(vaeh.user2,"-") > 1
                               then 
                               entry(1,vaeh.user2,"-") + "-" +         
                               left-trim(entry(2,vaeh.user2,"-"),"0")      
                              else 
                               "fl" + string(q-vaesl.seqno,"zz9")) and
              com.lineno   = q-vaesl.lineno 
              no-lock.
     assign q-proc = false.
   end. /* if f8 */
   else 
   if  keylabel(lastkey) = "F10" then do: 
    if avail com and q-vaesl.seqno = 4 then do: 
     assign r-shipprod    = q-vaesl.shipprod
            r-arpvendno   = dec(substring(com.noteln[9],17,12))
                            /* q-vaesl.arpvendno */
            r-prodcst     = dec(substring(com.noteln[10],17,12))
                            /* q-vaesl.prodcost  */
            r-leadtm      = substring(com.noteln[11],17,4)
            r-whse        = substring(com.noteln[12],17,4)
            r-arpprodline = substring(com.noteln[14],17,6)
            r-shipvia     = substring(com.noteln[13],17,4)
            r-comments1 = com.noteln[1]
            r-comments2 = com.noteln[2]
            r-comments3 = com.noteln[3]
            r-comments4 = com.noteln[4]
            r-comments5 = com.noteln[5]
            r-comments6 = com.noteln[6]
            r-comments7 = com.noteln[7].
     display r-shipprod
             r-arpvendno
             r-whse        
             r-prodcst 
             r-shipvia
             r-leadtm
             r-comments1
             r-comments2
             r-comments3
             r-comments4
             r-comments5
             r-comments6
             r-comments7 with frame f-rarrnotes.
      pause.
      hide frame f-rarrnotes.
     end.
   end. 
 display x-functions2 with frame f-wodtl.        
end.

 on find of vaesl do:
  if q-proc and x-missing then do:
   find q-icsp 
       where q-icsp.cono = g-cono and
             q-icsp.prod = q-vaesl.shipprod no-lock no-error.
   find q-icsw 
       where q-icsw.cono = g-cono and
             q-icsw.whse = q-vaesl.whse and
             q-icsw.prod = q-vaesl.shipprod no-lock no-error.
   /*if avail q-icsw then
     assign s-netavail = q-icsw.qtyonhand - q-icsw.qtyreservd - 
                         q-icsw.qtycommit.*/
  end.
  
  if q-proc and
     q-vaesl.nonstockty <> "n" and
    (avail q-icsp and q-icsp.statustype = "l") and x-missing = true then
    return error.
  else
  if q-proc and x-missing = true and ix-mode = "a" then 
    do: 
    if (q-vaesl.nonstockty = "n" or
        (avail q-icsw and q-vaesl.nonstockty = "s" and
         q-icsw.statustype <> "o")) and q-vaesl.qtyship ge 
         q-vaesl.qtyord then
         return error. 
    else
    if q-vaesl.qtyship ge q-vaesl.qtyord then             
      return error.
    /*
    else
      assign s-netavail = q-icsw.qtyonhand - q-icsw.qtyreservd - 
                          q-icsw.qtycommit.
    */
  end.
  if not avail q-icsw then
    do:
    find q-icsw 
       where q-icsw.cono = g-cono and
             q-icsw.whse = q-vaesl.whse and
             q-icsw.prod = q-vaesl.shipprod no-lock no-error.
  end.
  /*
  if avail q-icsw then
    assign s-netavail = q-icsw.qtyonhand - q-icsw.qtyreservd - 
                        q-icsw.qtycommit.
  */
 end. /* find of vaesl */

 assign lackingFlag  = x-missing
        lackingFlag2 = x-missing.

 assign q-proc  = true.
 on cursor-down cursor-down.
 on cursor-up   cursor-up.

 open query q-wodtl 
   for each q-vaesl use-index k-vaesl
      where q-vaesl.cono       = g-cono
        and q-vaesl.vano       = ix-orderno
        and q-vaesl.vasuf      = ix-ordersuf
        and q-vaesl.sctntype   = "in"  
        and q-vaesl.completefl = false  
        and q-vaesl.shipprod ne ""           and    
          ((q-vaesl.qtyord > q-vaesl.qtyship and 
                lackingFlag ) or not lackingFlag ) 
            no-lock,
       each vaeh outer-join 
      where vaeh.cono   = g-cono      and  
            vaeh.vano   = ix-orderno  and 
            vaeh.vasuf  = ix-ordersuf  
            no-lock,
       each q-vaes  
      where q-vaes.cono  = g-cono        and  
            q-vaes.vano  = q-vaesl.vano  and 
            q-vaes.vasuf = q-vaesl.vasuf and 
            q-vaes.seqno = q-vaesl.seqno and 
            q-vaes.stagecd = 1       
            no-lock,
       each icsp outer-join
      where icsp.cono = g-cono and
            icsp.prod = q-vaesl.shipprod no-lock,
       each icsw outer-join
      where icsw.cono = g-cono and
            icsw.whse = q-vaesl.whse and
            icsw.prod = q-vaesl.shipprod no-lock,
       each com outer-join
      where com.cono = g-cono     and
            com.comtype  = (if avail vaeh and num-entries(vaeh.user2,"-") > 1
                             then 
                             entry(1,vaeh.user2,"-") + "-" +         
                             left-trim(entry(2,vaeh.user2,"-"),"0")
                            else 
                             "fl" + string(q-vaesl.seqno,"zz9")) and
            com.lineno   = q-vaesl.lineno 
            no-lock.

  assign q-proc    = false
         x-cost    = 0 
         x-intcost = 0. 

  for each vaesl where
           vaesl.cono       = g-cono      and
           vaesl.vano       = ix-orderno  and
           vaesl.vasuf      = ix-ordersuf and
           vaesl.completefl = false       and
           vaesl.shipprod ne ""  
           no-lock:
     find first vaes where 
                vaes.cono  = g-cono      and  
                vaes.vano  = vaesl.vano  and 
                vaes.vasuf = vaesl.vasuf and 
                vaes.seqno = vaesl.seqno  
                no-lock no-error.                   

     if avail vaes and vaes.borelfl = no and       
        vaesl.sctntype = "in" then do: 
      assign x-cost =
             x-cost + round((vaesl.prodcost * vaesl.qtyneeded),2). 
     end. 
     if vaesl.sctntype = "it"  then do: 
      assign x-intcost = x-intcost + 
                       ((vaesl.timeelapsed / 3600) * vaesl.qtyneeded) *
                         vaesl.prodcost.    
     end.   
  end.
  assign x-totcost = (x-cost + x-intcost). 

Main:
do while x-running on endkey undo, leave.                         
  on cursor-down cursor-down.
  on cursor-up   cursor-up.
  if avail vaeh and vaeh.user2 ne "" then 
  assign x-functions2 =                                                   
"  F6-Poizs  (F7-INOPEN , F8-InOpen/InPrt) missing/ALL    F10-Svc/PoComments ". 
 else 
  assign x-functions2 =                                                   
"  F6-Poizs  (F7-INOPEN , F8-InOpen/InPrt) missing/ALL                       ". 
  enable b-wodtl with frame f-wodtl.                            
  apply "ENTRY" to b-wodtl in frame f-wodtl.                    
  display ix-orderno      
          ix-ordersuf
          x-missing   
          x-cost   
          x-intcost 
          x-totcost
          x-functions2
          with frame f-wodtl.        

  do while true on endkey undo, leave.                         
   wait-for window-close of current-window.                     
  end.                                                         
  close query q-wodtl.
  hide frame f-wodtl.
  hide frame f-rarrnotes.
  hide frame f-keylabels. 
  on cursor-up back-tab.
  on cursor-down tab.
  leave main.
end.

procedure titlemorph:
  find first sassm where sassm.currproc = v-myframe no-lock no-error.
  if avail sassm then
    g-title = sassm.frametitle.
end.  /* procedure titlemorph */  

procedure Stage1Query:  
 open query q-wodtl 
   for each q-vaesl use-index k-vaesl
      where q-vaesl.cono       = g-cono
        and q-vaesl.vano       = ix-orderno
        and q-vaesl.vasuf      = ix-ordersuf
        and q-vaesl.sctntype   = "in"  
        and q-vaesl.completefl = false  
        and q-vaesl.shipprod ne ""           and    
          ((q-vaesl.qtyord > q-vaesl.qtyship and 
                lackingFlag ) or not lackingFlag ) 
            no-lock,
       each vaeh outer-join 
      where vaeh.cono  = g-cono      and  
            vaeh.vano  = ix-orderno  and 
            vaeh.vasuf = ix-ordersuf  
            no-lock,
       each q-vaes  
      where q-vaes.cono  = g-cono        and  
            q-vaes.vano  = q-vaesl.vano  and 
            q-vaes.vasuf = q-vaesl.vasuf and 
            q-vaes.seqno = q-vaesl.seqno and 
            q-vaes.stagecd = 0
            no-lock,
       each icsp outer-join
      where icsp.cono = g-cono and
            icsp.prod = q-vaesl.shipprod no-lock,
       each icsw outer-join
      where icsw.cono = g-cono and
            icsw.whse = q-vaesl.whse and
            icsw.prod = q-vaesl.shipprod no-lock,
       each com outer-join
      where com.cono = g-cono     and
            com.comtype  = (if avail vaeh and num-entries(vaeh.user2,"-") > 1
                             then 
                             entry(1,vaeh.user2,"-") + "-" +         
                             left-trim(entry(2,vaeh.user2,"-"),"0")
                            else 
                             "fl" + string(q-vaesl.seqno,"zz9")) and
            com.lineno   = q-vaesl.lineno 
            no-lock.

 enable b-wodtl with frame f-wodtl.
 apply "ENTRY" to b-wodtl in frame f-wodtl.

end.

 
