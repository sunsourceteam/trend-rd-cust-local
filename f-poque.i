/* f-poque.i      */
/* ------------------------------------------------------------------------
  Program Name :  F-ZPOQUE.I

  Description  :  Format module for POEZQ using xxixx.i

  Author       :  Thomas Herzog


------------------------------------------------------------------------- */

define {1} shared var o-pono like poeh.pono             no-undo. 
define {1} shared var o-posuf like poeh.posuf           no-undo.

define {1} shared var z-ponoline as character format "x(16)"    no-undo.
define {1} shared var x-ponoline as character format "x(16)"    no-undo.
define {1} shared var z-pono  like poeh.pono             no-undo.
define {1} shared var z-posuf like poeh.posuf            no-undo.
define {1} shared var z-orderno like oeeh.orderno        no-undo.
define {1} shared var z-ordersuf like oeeh.ordersuf      no-undo.
define {1} shared var z-shipprod like poel.shipprod      no-undo.
define {1} shared var b-cost like poel.price             no-undo.
define {1} shared var b-vendno like poel.vendno          no-undo.
define {1} shared var b-vendship like poel.shipfmno      no-undo.
define {1} shared var g-shipfmno like poel.shipfmno      no-undo.

define {1} shared var b-shipinstr like poeh.shipinstr    no-undo.
define {1} shared var b-shipvia   like poeh.shipviaty    no-undo.                                        
define {1} shared var d-crmgr  like arsc.creditmgr       no-undo.
define {1} shared var d-crname as char format "x(20)"    no-undo.


define {1} shared var z-licost like poel.price           no-undo.
define {1} shared var z-lineno like oeel.lineno          no-undo.

define {1} shared var z-seqno  like oeelk.seqno          no-undo.
define {1} shared var z-line like poel.lineno            no-undo.
define {1} shared var z-avail like icsw.qtyonhand        no-undo.
define {1} shared var z-coavail like icsw.qtyonhand      no-undo.
define {1} shared var z-poelrecid  as recid              no-undo.
define {1} shared var z-zidcrecid as recid               no-undo.
define {1} shared var z-refresh as logical               no-undo.

define {1} shared var z-answer as logical                no-undo.
define {1} shared var v-polineno like poel.lineno        no-undo.
define {1} shared var v-seqno    like poelo.seqno        no-undo.
define {1} shared var z-rush     as character format "x" no-undo.
define {1} shared var s-buyer    like poeh.buyer         no-undo.
define {1} shared var g-vendno   like apsv.vendno        no-undo.
define {1} shared var z-vendno   like apsv.vendno        no-undo.
define {1} shared var g-prod     like icsp.prod          no-undo.
define {1} shared var s-position as integer      init 0  no-undo.

define {1} shared var b-buyer    like poeh.buyer         no-undo.

define {1} shared var v-fkey as character format "x(5)" extent 5 initial
   ["OEIO ","POIO ","","",""]             no-undo.

define {1} shared buffer z-apsv for apsv.
define {1} shared buffer z-apss for apss.


define {1} shared frame f-poque.
define {1} shared frame f-poquel.
define {1} shared frame f-poqued.

form 
  s-buyer       at   1 label "Buyer"
    {f-help.i}
  
  sasta.descrip at  16 label "Name"
  
  b-vendno      at  50 label "Vendor"
    {f-help.i}
   with frame f-poque  width 80 row 2 centered side-labels 
   overlay title  g-title.
form 
  z-ponoline       at 1
  poel.nonstockty  at 17
  z-shipprod       at 18
  poel.vendno      at 43 format ">>>>>>>>>9"
  poel.qtyord      at 54 format ">>>9"
  z-coavail        at 59 format ">>>>9"
  poel.price       at 65 format ">>>9.99999"
  poel.transtype   at 76 format "xx"
  z-rush           at 79 format "x"
   
   with frame f-poquel overlay no-labels no-box width 79 column 2 row 6
        scroll 1 v-length down.

form 
    
   poel.qtyord at 5 format ">>>>9"   label "Qty Ord      "
   z-avail     at 5 format ">>>>9"   label "Qty Avail    "
   z-coavail   at 5 format ">>>>>9"  label "Company Avail"
   g-vendno    at 5                  label "Vendor #     " {f-help.i}
           validate(
                    can-find(apsv use-index k-apsv where 
                     apsv.cono = g-cono and apsv.vendno = input g-vendno 
                     /* and
                     apsv.divno = poeh.divno */ ),
   "Vendor Not Set Up in Vendor Setup - APSV (4301) or Division MisMatch") 
          
   g-shipfmno at 5                    label "ShipFmNo     " {f-help.i}
           validate(
                    can-find(apss  where 
                     apss.cono = g-cono and apss.vendno = input g-vendno and
                     apss.shipfmno = input g-shipfmno) or
                     input g-shipfmno = 0,
   "Vendor Ship Fm Not Set Up in - APSS (4301) or Division MisMatch")
     
   b-buyer  at 5                     label "Buyer        " 
   b-cost   at 5                     label "Cost         "
   b-shipinstr at 5                  label "Ship Instr   "    
   b-shipvia   at 5                  label "Ship Via     "
   d-crmgr at 5                      label "CrMgr        "
   d-crname at 5                     label "CrMgr Name   "
   
   
   with frame f-poqued overlay side-labels width 65 column 15 row 7 title 
      "Extended Detail Update".  


assign v-length = 15.
assign v-fkey[1] = ""
       v-fkey[2] = "".
