{g-all.i}
Define shared temp-table xps no-undo
  Field prod        like icsw.prod
  Field zdescrip    as c format "x(30)"

index iprod
      prod.

 

define query dmd-q for xps. /* scrolling. */

define browse dmd-b query dmd-q 
  display  
  xps.prod        column-label "Product"   
  xps.zdescrip column-label "Description"
   with no-box size-chars 78 by 13 down.


define frame  f-q1
  dmd-b
  with  no-underline overlay width 80 row 6.




main:
do while true  on endkey undo main, leave main:


 
 on cursor-up cursor-up.
 on cursor-down cursor-down.
 
 open query dmd-q for each xps use-index iprod no-prefetch no-lock.
 enable dmd-b with frame f-q1.
 
 wait-for window-close of current-window.

 close query dmd-q.
 next main.

 
on cursor-up back-tab.
on cursor-down tab.


