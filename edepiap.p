/*h*****************************************************************************
  PROCEDURE      : edepiap.p
  DESCRIPTION    : EDI-Incoming Invoice - Accounts Payable Transaction Creation
  AUTHOR         : jlc
  DATE WRITTEN   : 05/04/95
  CHANGES MADE   :
    05/05/95 jlc; TB# 15807 New Program.
    11/09/95 fef; TB# 19646 Add edepiap.gas to assign posuf and lineno to
        apebc records.
    12/07/95 jlc; TB# 15807 Discount amount zero.
    01/23/96 jlc; TB# 15807 Discount amount into Schedule Pay record.
    02/16/96 jkp; TB# 15807 Corrected Expensed Addons to write out separate
        GLEBA records for each addon.
    03/08/96 jkp; TB# 20681 Added read of sasc and the output of the
        sasc.apdisputefl to the apebt.disputefl field in the apebt create for
        apebt.transcd "0" and "3". Previously apebt.disputefl was being left
        at the initial value from the dictionary.
    04/19/96 jlc; TB# 20918 Remove the "-99" suffix from one time batch
        records in apbatch.
    05/31/96 gdf; TB# 21264 Expensed addons not recorded correctly. Added
        v-explineno variable to record number of expensed addons per invoice.
    07/25/96 jkp; TB# 21431 Allow Progress to calculate the 4 digit year
        instead of hard-coding the "+ 1900".
    09/10/96 jkp; TB# 21829 Change transcd to "0" when creating GLEBA records
        for A/P Trade Unmatched and Net Difference G/L Posting.  Per GDF, the
        transcd for GLEBA should always be set to zero.  The A/P Expensed
        account posting was already setting the transcd to a zero.
    10/01/96 jkp; TB# 21970 Progress error is being received when trying to
        create a duplicate POELA or APEBA record.  Will now check for the
        records existance and add the amount to the record if it is found,
        otherwise, it will create the record.
    10/22/96 jkp; TB# 21970 Added the use of v-expjrnlno to be used instead of
        "-1 * g-jrnlno".
    01/10/97 jkp; TB# 22436 Added the explanatory comment of how the report
        fields are assigned.  Changed the apebc records to be created from
        the t-lines temp-table instead of the report records.  The t-lines
        records are created in edepia.p.  This was necessary to get the records
        in the correct order for the new edepiap.gas logic.
    02/21/97 jkp; TB#  7241 (5.2) Spec8.0 Special Price Costing.  Added call
        of speccost.gva.
    12/18/97 jlc; TB# 22169 Allow credit invoices into Trend.
    01/18/98 jlc; TB# 24423 Add user hook for terms customization.
    12/08/98 bm;  TB# 25634 Initialize invtype from apsv
    02/16/99 bm;  TB# 25873 Creating credit and difference postings
    02/24/99 bm;  TB# 25486 Load GST and PST to GLEBA
    06/03/99 bm;  TB# 26238 For divisionalized company load addon divno
        based on global value not sastn
    11/01/01 bpa; TB# e9968 If Due Date blank, not using terms to set properly
    01/25/02 blr; TB# e11799 Take AP Character EDI Import process from sx 3.0
        and rollup into sx3.1.004.
    03/19/02 bm;  TB# e12590 Invoice posts to GL in foreign currency.
    06/26/02 blr; TB# e10498 Create seperate EDI Flat File to store
        off the exceptions data for invoices that error out into a
        separate flat file.  This flat file can then be printed by the
        user so they can manually enter the invoice if need be, or
        correct the data and reprocess the flat file by renaming to
        edi810i_dat, or modify the flat file so it can be processed into
        sx, and then rename the file.
    10/02/02 blr; TB# e10498/11799 TB Turn - Issues with Multiple Invoices
        with errors printing on report.
    10/09/02 sbr; TB# e12105 APEBU will not update .005 rounding up
    10/18/02 gwk; TB# e13875 We need an AP Expense posting when we have an
        incoming invoice with no item records (expense invoice)
    09/02/03 bp ; TB# e18204 Add additional user hooks.
    11/14/05 sbr; TB# e23248 EDEPI creating susp hits when dup invs
    07/10/06 gwk; TB# e24080 Clean up terms processing
    06/11/08 sbr; INDF-00800 Expensed addons wrong when net amt=0
    10/13/08 bp ; TB# e26818 Add user hooks.
    12/30/08 gwk; MSF 116090 Assigning incorrect suffix based on qty
    01/25/09 gwk; MSF 117077 Not finding correct PO suffix when multiple
        invoices for the same PO
    02/17/09 kjb; MSF 117773 Allow EDEPI to read in a whole order discount
        value from the EDI 810 Inbound document (MSF RS# 3908)
    02/19/09 gwk; MSF 117757 Duplicate APEBC record error. Also if APEBC
        records exist for suff/line, then APEBC record should have phantom
        suffix (> than highest suffix for this PO)
    07/14/09 bp ; MSF 117919 Add user hooks.
    08/06/09 sbr; MSF 123001 Expense invoices are being set to disputed status
    09/01/09 sbr; MSF 123619 Expense misc. credits are being put on hold/dispute
    09/02/10 dww; MSF 132444 Remove REPORT table from SX
******************************************************************************/

/*e****************************************************************************
  REPORT FILE ASSIGNS  :

    This list is alphabetical by report field.  If no specific type (based on
        the transcd) is listed, the field is used for all types.  If specific
        types are listed, it is only used for the types listed.  The file
        prefix (b-) is not identified, unless it is critical to understanding
        the assignment.

    Types (stored in the report.outputty field) used are:
        type n - Notes Data
        type u - Item User Data
        type a - Addon Data

    Line data is stored in the temp-table t-lines.

    report.c12      = (type a-addon) substr(g-data,20,8)        Trend Addon
    report.c13      = (type a-addon) substr(g-data,28,8)        User2
    report.c20      = (type n-notes) substr(g-data,55,12)
                      (type u-user)  substr(g-data,7,16)        User1 & User2
                      (type a-addon) substr(g-data,108,6)
    report.c24      = (type u-user)  substr(g-data,23,24)       User3
                      (type a-addon) substr(g-data,36,24)       User3
    report.c24-2    = (type n-notes) substr(g-data,7,24)
                      (type u-user)  substr(g-data,47,24)       User3 Continued
                      (type a-addon) substr(g-data,60,24)
    report.c24-3    = (type n-notes) substr(g-data,31,24)
                      (type u-user)  substr(g-data,71,24)       User3 Continued
                      (type a-addon) substr(g-data,84,24)
    report.c4       = (type a-addon) substr(g-data,17,3)
    report.c6       = (type u-user)  substr(g-data,95,6)        User3 Continued
                                                                (final section)
    report.cono     = g-cono
    report.de12d0   = (type n-notes) v-noteseq
                      (type u-user)  dec(v-lnseqno)             Assoc. Line #
                      (type a-addon) 0 (zero)
    report.de9d2s   = (type u-user)  dec(substr(g-data,257,16)) User6
                      (type a-addon) dec(substr(g-data,8,9))    Addon Amount
    report.de9d2s-2 = (type u-user)  dec(substr(g-data,273,16)) User7
    report.dt       = (type u-user)  date(substr(g-data,289,8))
    report.dt-2     = (type u-user)  date(substr(g-data,297,8))
    report.oper2    = g-operinit
    report.outputty = (type n-notes) "n" for Notes Data
                      (type u-user)  "u" for User Data
                      {type a-addon) "a" for Addon Data
    report.reportnm = v-reportnm
*******************************************************************************/

{g-all.i}
{g-post.i}
{g-gl.i}
{edepi.gva}

/* SXE 5.5 */
def  shared var g-wodiscamt     like poeh.wodiscamt          no-undo.


{tt-report.gtt &shared = shared}
{g-rptctl.i}

{edepiap.z99 &top = "*"}

/*tb  7241 (5.2) 02/21/97 jkp; Added call of speccost.gva. */
{speccost.gva "shared"}

def shared stream edilog.
def shared stream edierrdat.

def var v-data  as  c   format "x(600)"     no-undo.

def shared frame f-head.
def shared frame f-edilog.
def shared frame f-errhead.
def shared frame f-err.
def shared frame f-errin.

def var o-updfl             as log format "yes/no"              no-undo.

def var v-addon             as de    extent 2                   no-undo.
def var v-discpct           like apebt.discpct                  no-undo.
def var v-discamt           like apebt.discamt                  no-undo.
def var v-amount            like apebt.amount                   no-undo.
def var v-termstype         like apebt.termstype                no-undo.
def var v-duedt             like apebt.duedt                    no-undo.
def var v-invdt             like apebt.invdt                    no-undo.
def var v-discduedt         like apebt.discdt                   no-undo.
def var v-discdt            like apebt.discdt                   no-undo.
def var v-termsduedt        like duedt                          no-undo.
def var v-termsdiscdt       like discdt                         no-undo.
def var v-user8dt           like apebt.user8                    no-undo.
def var v-user9dt           like apebt.user9                    no-undo.
def var v-duedays           like apebt.duedays                  no-undo.
def var v-discdays          like apebt.discdays                 no-undo.
def var v-expaddonfl        as log format "yes/no"              no-undo.
def var v-exchgrt           like sastc.vouchexrate              no-undo.
def var v-aptradeamt        as de                               no-undo.
def var v-netdiffamt        as de                               no-undo.
def var v-transno           as i                                no-undo.
def var v-posno             as i                                no-undo.
def var v-type              as log format "yes/no"              no-undo.
def var v-glcono            like sasc.cono                      no-undo.
def var v-gldivno           as i format "zzzzzzzz9-"            no-undo.
def var v-gldeptno          as i format "zzzzzzzz9"             no-undo.
def var v-glacctno          as i format "zzzzzzzz9"             no-undo.
def var v-glsubno           as i format "zzzzzzzz9"             no-undo.

def var v-posuf             as i init 99                        no-undo.
def var v-lineno            as i init 999                       no-undo.

def var v-explineno         as i init 1                         no-undo.

def var v-suf               like poel.posuf                     no-undo.
def var v-qtycosted         like poel.qtycosted                 no-undo.

def var v-sufmatch          like poel.posuf                     no-undo.
def var v-qtydiffmatch      like poel.qtycosted                 no-undo.
def var v-qtydiff           like poel.qtycosted                 no-undo.
def var v-matchfoundfl      as log format "yes/no"              no-undo.
def var v-apebcfl           as log format "yes/no"              no-undo.


def workfile w-addon no-undo
    field addonno           as i    format ">9"
    field origamt           as de   format "zzzz9.99-".

{edepi.gfo}

{edepiap.z99 &bef_main = "*"}

def buffer b2-poel for poel.

procedure check-for-dupe:
    if can-find
        (apebc where
         apebc.cono    = g-cono       and
         apebc.jrnlno  = g-jrnlno     and
         apebc.setno   = g-setno      and
         apebc.pono    = t-lines.pono and
         apebc.posuf   = v-apebcposuf and
         apebc.lineno  = v-apebclineno)
    then do:
        v-apebcposuf = if v-apebcposuf > v-lastposuffcnt
                          then v-apebcposuf
                       else v-lastposuffcnt + 1.

        find-next-avail-suff:
        do v-apebcposuf = v-apebcposuf to 100:
            if not can-find
                (apebc where
                 apebc.cono    = g-cono         and
                 apebc.jrnlno  = g-jrnlno       and
                 apebc.setno   = g-setno        and
                 apebc.pono    = t-lines.pono   and
                 apebc.posuf   = v-apebcposuf   and
                 apebc.lineno  = v-apebclineno)
            then leave find-next-avail-suff.

            /* we've used up all 99 suffixes, so use bogus lineno instead. */
            if v-apebcposuf > 99 then
            do v-apebclineno = 999 to 100 by -1:
                if not can-find
                    (apebc where
                     apebc.cono    = g-cono         and
                     apebc.jrnlno  = g-jrnlno       and
                     apebc.setno   = g-setno        and
                     apebc.pono    = t-lines.pono   and
                     apebc.posuf   = v-apebcposuf   and
                     apebc.lineno  = v-apebclineno)
                then leave find-next-avail-suff.

            end. /* do v-apebclineno = 999... */

        end.  /* find-next-avail-suff */

    end.  /* can-find apebc */

end procedure.

main:
do:

    /*d Top of main block */
    {edepiap.z99 &top_main = "*"}

    /*tb 21431 07/25/96 jkp; Changed v-invdt, v-duedt, v-discdt, v-user8dt,
         and v-user9dt to use the Progress date function instead of
         hard-coding the "+ 1900".  This coding also takes care of the
         international and non-international dates because the positioning
         will be based on the date startup parameter. */

    assign
        v-invdt     =   if month(date(substr(g-invdt,1,8))) <> 0 then
                            date(month(date(substr(g-invdt,1,8))),
                                 day  (date(substr(g-invdt,1,8))),
                                 year (date(substr(g-invdt,1,8))))
                        else
                            ?
        v-duedt     =   if month(date(substr(g-duedt,1,8))) <> 0 then
                            date(month(date(substr(g-duedt,1,8))),
                                 day  (date(substr(g-duedt,1,8))),
                                 year (date(substr(g-duedt,1,8))))
                        else
                            ?
        v-discdt    =   if month(date(substr(g-discduedt,1,8))) <> 0 then
                            date(month(date(substr(g-discduedt,1,8))),
                                 day  (date(substr(g-discduedt,1,8))),
                                 year (date(substr(g-discduedt,1,8))))

                        else
                            ?
        v-user8dt   =   if month(date(substr(g-invuser8,1,8))) <> 0 then
                            date(month(date(substr(g-invuser8,1,8))),
                                 day  (date(substr(g-invuser8,1,8))),
                                 year (date(substr(g-invuser8,1,8))))
                        else
                            ?
        v-user9dt   =   if month(date(substr(g-invuser9,1,8))) <> 0 then
                            date(month(date(substr(g-invuser9,1,8))),
                                 day  (date(substr(g-invuser9,1,8))),
                                 year (date(substr(g-invuser9,1,8))))
                        else
                            ?
        v-exchgrt   =   if dec(g-exchgrt) ne 0 then
                            dec(g-exchgrt)
                        else 1
        t-transcnt  =   t-transcnt + 1.

    /*tb e12590 03/19/02 bm; Invoice posts to GL in foreign currency.*/
    /*d Get vendor's exchange rate if they have a currency code. */
    if dec(g-exchgrt) = 0 and avail b-apsv and b-apsv.currencyty ne ""
    then do for sastc:
        {w-sastc.i b-apsv.currencyty no-lock}
        if avail sastc then
        v-exchgrt = sastc.vouchexrate.
    end.


    /*d Calculate terms. */
    {apegeapt.gpr}


    /*d If sassr option is set to require processing of item records,
        ensure item records exist */
    /*tb 22169 12/18/97 jlc; Credit Invoices do not contain item records */
    if p-invpo = no and g-itemfl = no and g-transtype ne "cn" then do:
       {edepiin.ler 8}
       g-okfl   =   no.
    end.

    /*d Error: A/P Batch Notes Already Exist */
    assign
        o-updfl =   p-updfl
        p-updfl =   no.
    run edepint.p.
    p-updfl =   o-updfl.

    {edepiap.z99 &top_apebt = "*"}

    /*tb 20681 03/08/96 jkp; Added: sasc to the do for and the read of sasc */
    /*tb 22169 12/18/97 jlc; Allow credit invoices.  apebt.transcd = 0 or 5 */
    /*tb e10498 & 9228; add the error processing  - changed the if p-updfl
        logic so that it would first check if p-updfl because this would be
        true whether doing the "error" logic, or the apebt/apeba/sasc, or the
        costing records - this caused majority of the rest of the programs
        indention to change. */

    if p-updfl = yes then do:

        if g-okfl = no then do:
           g-okerrfl = yes.
           {edepdat.i}

            for each b-t-excdata:
                delete b-t-excdata.
            end.

        end. /*end of g-okfl = no */

        else do:
            for each b-t-excdata:
                delete b-t-excdata.
            end.
            do for apebt, apeba, sasc:

                {w-sasc.i no-lock}

        /*d Create the apbatch transaction 'invoice' record */
        /*tb 20681 03/08/96 jkp; Added output of the apebt.disputefl field */
        /*tb 20918 04/19/96 jlc; Remove the -99 suffix from apbatch records */
        /*tb 25634 12/08/98 bm;  Initialize invtype from apsv */

                create apebt.
                assign
                    g-apebtid           = recid(apebt)
                    apebt.cono          = g-cono
                    apebt.vendno        = b-apsv.vendno
                    apebt.invsuf        = 99
                    apebt.amount        = dec(g-invamt)
                    apebt.discamt       = v-discamt
                    apebt.disputefl     = if (g-itemfl = no and
                                              g-transtype = "DI":u) or
                                             g-transtype = "CN":u
                                          then false
                                          else if avail sasc and
                                             sasc.apdisputefl = true
                                          then true
                                          else false
                    apebt.transcd       = if g-transtype = "cn" then 5 else 0
                    apebt.seqno         = g-seqno
                    apebt.setno         = g-setno
                    apebt.invdt         = v-invdt
                    apebt.duedt         = v-duedt
                    apebt.discdt        = v-discdt
                    apebt.refer         = g-refer
                    apebt.termstype     = v-termstype
                    apebt.batchnm       = p-batchnm
                    apebt.duedays       = v-duedays
                    apebt.discdays      = v-discdays
                    apebt.nopays        = 1
                    apebt.freqdays      = 0
                    apebt.discpct       = v-discpct
                    apebt.apinvno       = g-invno
                    apebt.addonamt[1]   = t-expaddamt
                    apebt.addonamt[2]   = 0
                    apebt.gsttaxamt     = dec(g-gst)
                    apebt.psttaxamt     = dec(g-pst)
                    apebt.currencyty    = b-apsv.currencyty
                    apebt.invtype       = b-apsv.invtype
                    apebt.exchgrate[1]  = v-exchgrt
                    apebt.exchgrate[2]  = 1
                    apebt.createdby     = string(g-operinit,"xxxx") + "-edi"
                    apebt.divno         = g-divno
                    apebt.jrnlno        = g-jrnlno
                    apebt.user1         = g-invuser1
                    apebt.user2         = g-invuser2
                    apebt.user3         = g-invuser3
                    apebt.user4         = g-invuser4
                    apebt.user5         = g-invuser5
                    apebt.user6         = dec(g-invuser6)
                    apebt.user7         = dec(g-invuser7)
                    apebt.user8         = v-user8dt
                    apebt.user9         = v-user9dt
                    t-creditamt         = if g-transtype = "cn" then
                                             t-creditamt + dec(g-invamt)
                                          else t-creditamt
                    g-seqno             = g-seqno + 1.
                    {t-all.i "apebt"}

                /*d Bottom of the 'invoice' apebt record */
                {edepiap.z99 &bottom_apebt_inv = "*"}

        /*d Create the apbatch transaction 'schedule pay' record */
        /*tb 15807 01/23/96 jlc; Discount Amount must go into Schedule Pay */
        /*tb 20681 03/08/96 jkp; Added output of the apebt.disputefl field */
        /*tb 20918 04/19/96 jlc; Remove the -99 suffix from apbatch records */
        /*tb 22169 12/18/97 jlc; Credit invoices should not create sched pay */
        /*tb 25634 12/08/98 bm;  Initialize invtype from apsv */

                if g-transtype ne "cn" then do:
                    create apebt.
                    assign
                        g-apebtid           = recid(apebt)
                        apebt.cono          = g-cono
                        apebt.vendno        = b-apsv.vendno
                        apebt.invsuf        = 99
                        apebt.amount        = dec(g-invamt)
                        apebt.discamt       = v-discamt
                        apebt.disputefl     = if g-itemfl = no and
                                                 g-transtype = "DI":u
                                              then false
                                              else if avail sasc and
                                                 sasc.apdisputefl = true
                                              then true
                                              else false
                        apebt.transcd       = 3
                        apebt.seqno         = g-seqno
                        apebt.setno         = g-setno
                        apebt.invdt         = v-invdt
                        apebt.duedt         = v-duedt
                        apebt.discdt        = v-discdt
                        apebt.refer         = g-refer
                        apebt.termstype     = v-termstype
                        apebt.batchnm       = p-batchnm
                        apebt.duedays       = 0
                        apebt.discdays      = 0
                        apebt.nopays        = 0
                        apebt.freqdays      = 0
                        apebt.discpct       = 0
                        apebt.apinvno       = g-invno
                        apebt.addonamt[1]   = 0
                        apebt.addonamt[2]   = 0
                        apebt.gsttaxamt     = dec(g-gst)
                        apebt.psttaxamt     = dec(g-pst)
                        apebt.currencyty    = b-apsv.currencyty
                        apebt.invtype       = b-apsv.invtype
                        apebt.exchgrate[1]  = v-exchgrt
                        apebt.exchgrate[2]  = 1
                        apebt.createdby     = string(g-operinit,"xxxx") + "-edi"
                        apebt.divno         = g-divno
                        apebt.jrnlno        = 0
                        apebt.user1         = g-invuser1
                        apebt.user2         = g-invuser2
                        apebt.user3         = g-invuser3
                        apebt.user4         = g-invuser4
                        apebt.user5         = g-invuser5
                        apebt.user6         = dec(g-invuser6)
                        apebt.user7         = dec(g-invuser7)
                        apebt.user8         = v-user8dt
                        apebt.user9         = v-user9dt
                        g-seqno             = g-seqno + 1.
                        {t-all.i "apebt"}

                end.  /* end credit invoice check */

                /*d Bottom of the schedule pay  apebt record */
                {edepiap.z99 &bottom_apebt_sp = "*"}

                /*d Increment the number Invoices that have updated apebt */
                t-updcnt                =   t-updcnt + 1.

                /*d Process Invoice 'addon' records. */
                for each report use-index k-cust where
                    report.cono     =   g-cono      and
                    report.oper2    =   g-operinit  and
                    report.reportnm =   v-reportnm  and
                    report.outputty =   "a"
                no-lock:

                    v-expaddonfl    =   yes.
                    /*d Identify the addon as expensed or capitalized */
                    type-lp:
                    do i = 1 to 4:
                        if avail b-apsv and
                           int(report.c12)  = b-apsv.capaddonno[i]
                        then do:
                            v-expaddonfl    = no.
                            leave type-lp.
                        end.
                    end.

                    /*d Create apeba for capitalized addons */
                    /*tb 20918 04/19/96 jlc; Remove the -99 suffix from apbatch
                         records */

                    if v-expaddonfl = no then do:
                        {w-sastn.i "x" int(report.c12) no-lock}

                 /*tb 21970 10/01/96 jkp; Added find on APEBA to see if the
                   record already exists before creating it.  If it exists,
                   add the capitalized addon amount to the record found.  If
                   it does not exist, create it. */

                        find first apeba use-index k-apeba where
                                   apeba.cono    = g-cono          and
                                   apeba.jrnlno  = g-jrnlno        and
                                   apeba.setno   = g-setno         and
                                   apeba.addonno = int(report.c12) and
                                   apeba.pono    = int(g-pono)
                        exclusive-lock no-error.

                        if avail apeba then
                        do:

                            apeba.origamt = apeba.origamt + report.de9d2s.

                            {t-all.i "apeba"}

                        end. /* if avail apeba */

                        else
                        do:

                            create apeba.

                            assign
                                g-apebtid           =   recid(apebt)
                                apeba.cono          =   g-cono
                                apeba.jrnlno        =   g-jrnlno
                                apeba.setno         =   g-setno
                                apeba.addonno       =   int(report.c12)
                                apeba.vendno        =   b-apsv.vendno
                                apeba.apinvno       =   g-invno
                                apeba.origamt       =   report.de9d2s
                                apeba.applyamt      =   0
                                apeba.updatefl      =   no
                                apeba.currencyty    =   b-apsv.currencyty
                                apeba.exchgrate[1]  =   v-exchgrt
                                apeba.exchgrate[2]  =   1
                                apeba.statustype    =   yes
                                apeba.pono          =   int(g-pono)
                                apeba.alloctype     =   if avail sastn then
                                                            sastn.st
                                                        else "d"
                                apeba.user1         =   substr(report.c12,1,8)
                                apeba.user2         =   substr(report.c13,1,8)
                                apeba.user3         =   report.c24 +
                                                        report.c24-2 +
                                                        report.c24-3 +
                                                        substr(report.c20,1,6)
                                t-capaddamt         =   t-capaddamt +
                                                        report.de9d2s.

                            {t-all.i "apeba"}

                        end. /* not avail apeba */


                    end. /* end capitalized addon */

                    /*d create a work file containing expensed addons */
                    else do:
                            create w-addon.
                            assign
                                w-addon.addonno =   int(report.c12)
                                w-addon.origamt =   report.de9d2s.
                    end.
                end. /* for each 'addon' report */

                /* Create APEBA record for the whole order discount if one
                    was loaded from the EDI document */
                if g-wodiscamt <> 0 then do:

                    find first apeba use-index k-apeba where
                        apeba.cono    = g-cono        and
                        apeba.jrnlno  = g-jrnlno      and
                        apeba.setno   = g-setno       and
                        apeba.addonno = -1            and
                        apeba.pono    = int(g-pono)
                    exclusive-lock no-error.

                    if not avail apeba then do:
                        create apeba.
                        assign
                            apeba.cono         = g-cono
                            apeba.jrnlno       = g-jrnlno
                            apeba.setno        = g-setno
                            apeba.addonno      = -1
                            apeba.vendno       = b-apsv.vendno
                            apeba.apinvno      = g-invno
                            apeba.pono         = int(g-pono)
                            apeba.origamt      = g-wodiscamt
                            apeba.applyamt     = 0
                            apeba.statustype   = yes
                            apeba.updatefl     = no
                            apeba.currencyty   = b-apsv.currencyty
                            apeba.exchgrate[1] = v-exchgrt
                            apeba.exchgrate[2] = 1
                            apeba.alloctype    = "d":u.
                        {t-all.i "apeba"}

                    end. /* not avail apeba */

                end. /* if g-wodiscamt <> 0 */

                /*d Create notes for this apbatch 'invoice' record */
                run edepint.p.

                /*d Bottom of creating 'apbatch' records */
                {edepiap.z99 &bottom_apebt = "*"}

            end. /* do for apebt, apeba, sasc */

            /*d Create batch costing records */
            /*tb 20918 04/19/96 jlc; Remove the -99 suffix from apbatch
                 records */

            do:

                /*d top of creating 'costing' records */
                {edepiap.z99 &top_apebc = "*"}

              /*tb 22436 01/10/97 jkp; Changed to use t-lines instead of report
                     records. */

              /*d Process Purchase Order Costing 'item' records for t-lines
              records. This first t-lines for each will process the records
              based on the shipprod. If a line is found that matches the
              shipprod, the t-lines.updatefl is set to yes. This keeps these
              lines from being processed in the for each t-lines below. If
              these t-lines records do not find a match, a second check is made
              with the for each below based on the ordered product. The product
              that will be checked, i.e., fed into edepiap.gas as &prod. The
              &combogus of slash-asterisk prevents this process from feeding
              into the bogus logic as we want to process it as a bogus only
              when the line cannot be found based on both the shipprod and the
              ordprod. */

                for each t-lines use-index k-lines where
                         t-lines.invno    = g-invno and
                         t-lines.updatefl = no and
                         t-lines.qtycost <> 0
                break by t-lines.invno
                      by t-lines.pono
                      by t-lines.shipprod
                      by t-lines.lnseqno:

                /*tb 22436 01/10/97 jkp; No longer want to add to the previous
                   record.  Will use the logic in edepiap.gas to determine the
                   suffix and line number and will always create an apebc
                   record. */

            /* tb 22436 01/10/97 jkp; Moved create of apebc records to after
               the run of edepiap.gas as described below. Added the
               following assigns to initialize them. */
                    assign
                        v-lastlineno  = if first-of(t-lines.shipprod) then
                                            0
                                        else v-lastlineno
                        v-apebcposuf  = 0
                        v-apebclineno = 0.

           /*tb 22436 01/10/97 jkp; Added if first-of(t-lines.pono) loop. */
           /*d Find last poeh to determine how many suffixes may need to
               be checked to get all of the lines. */
                    if first-of(t-lines.pono) then
                    do:

                        find last b-poeh use-index k-poeh where
                                  b-poeh.cono = g-cono       and
                                  b-poeh.pono = t-lines.pono
                        no-lock no-error.

            /*d Set v-lastposuffcnt to the last possible suffix number. */
                        v-lastposuffcnt = if avail b-poeh then
                                              b-poeh.posuf
                                          else 0.

                    end. /* if first-of(t-lines.pono) */

                    {edepiap.gas
                        &prod     = t-lines.shipprod
                        &combogus = "/*"}

            /*tb 22436 01/10/97 jkp; Moved the create of apebc below the run
               of edepiap.gas and made it conditional to ensure that a
               suffix and line number are ready to be assigned.  We cannot
               created apebc's without these fields as they are part of the
               primary unique key. */

                    run check-for-dupe.

                    if v-apebclineno <> 0 
                    and t-lines.qtycost <> 0 then do:
                    do for apebc:

                        create apebc.

                /*tb 22436 01/10/97 jkp; Changed report fields to t-lines
                     fields. */
                /*d  The apebc.shipprod will always be created with the product
                     that was or is going to be received, i.e., the
                     t-lines.shipprod. */
                        assign
                            apebc.cono         = g-cono
                            apebc.jrnlno       = g-jrnlno
                            apebc.setno        = g-setno
                            apebc.pono         = t-lines.pono
                            apebc.posuf        = v-apebcposuf
                            apebc.lineno       = v-apebclineno
                            apebc.statustype   = yes
                            apebc.vendno       = b-apsv.vendno
                            apebc.apinvno      = g-invno
                            apebc.cost         = t-lines.price
                            apebc.costord      = t-lines.totcost
                            apebc.currencyty   = b-apsv.currencyty
                            apebc.eachfl       = yes
                            apebc.exchgrate[1] = v-exchgrt
                            apebc.shipprod     = t-lines.shipprod
                            apebc.proddesc     = t-lines.proddesc
                            apebc.qtyord       = t-lines.qtycost
                            apebc.unitconv     = t-lines.convqty
                            apebc.whse         = t-lines.whse
                            apebc.updatefl     = no
                            t-pocostamt        = t-pocostamt + t-lines.totcost.

                        {t-all.i apebc}

                        /*d update of creating 'costing' records */
                        {edepiap.z99 &update_apebc = "*"}

                    end.
                    end. /* if v-apebclineno <> 0 do for apebc */

                end.  /* for each t-lines */

          /*tb 22436 01/10/97 jkp; Added this second for each t-lines loop to
           process the lines that the product could not be found based on the
           shipprod.  This loop is so the ordered product can be checked. */
         /*d Process Purchase Order Costing 'item' records for t-lines
           records. If these t-lines record were processed in the for each
           above, then t-lines.updatefl will be yes.  This will allow checking
           the transaction a second time based on the ordered product. The
           product that will be checked, i.e., fed into edepiap.gas as &prod.
           The lack of &combogus will allow this process to feed into the
           bogus logic. */
                for each t-lines use-index k-ordprod where
                         t-lines.invno    = g-invno and
                         t-lines.updatefl = no and
                         t-lines.qtycost <> 0
                break by t-lines.invno
                      by t-lines.updatefl
                      by t-lines.pono
                      by t-lines.ordprod
                      by t-lines.lnseqno:

                    assign
                        v-lastlineno  = if first-of(t-lines.ordprod) then
                                            0
                                        else v-lastlineno
                        v-apebcposuf  = 0
                        v-apebclineno = 0.

                 /*d Find last poeh to determine how many suffixes may need to
                        be checked to get all of the lines. */
                    if first-of(t-lines.pono) then
                    do:

                        find last b-poeh use-index k-poeh where
                                  b-poeh.cono = g-cono       and
                                  b-poeh.pono = t-lines.pono
                        no-lock no-error.

                 /*d Set v-lastposuffcnt to the last possible suffix number. */
                        v-lastposuffcnt = if avail b-poeh then
                                              b-poeh.posuf
                                          else 0.

                    end. /* if first-of(t-lines.pono) */

                    {edepiap.gas
                        &prod     = t-lines.ordprod}

                    run check-for-dupe.

                    if t-lines.qtycost <> 0 then do:
                    do for apebc:

                        create apebc.

               /*d  The apebc.shipprod will always be created with the product
                    that was or is going to be received, i.e., the
                    t-lines.shipprod. */
                        assign
                            apebc.cono         = g-cono
                            apebc.jrnlno       = g-jrnlno
                            apebc.setno        = g-setno
                            apebc.pono         = t-lines.pono
                            apebc.posuf        = v-apebcposuf
                            apebc.lineno       = v-apebclineno
                            apebc.statustype   = yes
                            apebc.vendno       = b-apsv.vendno
                            apebc.apinvno      = g-invno
                            apebc.cost         = t-lines.price
                            apebc.costord      = t-lines.totcost
                            apebc.currencyty   = b-apsv.currencyty
                            apebc.eachfl       = yes
                            apebc.exchgrate[1] = v-exchgrt
                            apebc.shipprod     = t-lines.shipprod
                            apebc.proddesc     = t-lines.proddesc
                            apebc.qtyord       = t-lines.qtycost
                            apebc.unitconv     = t-lines.convqty
                            apebc.whse         = t-lines.whse
                            apebc.updatefl     = no
                            t-pocostamt        = t-pocostamt + t-lines.totcost.

                        {t-all.i apebc}

                        /*d update of creating 'costing' records */
                        {edepiap.z99 &update_apebc = "*"}

                    end. /* do for apebc */
                  end.
                end.  /* for each t-lines for t-lines.updatefl = no */

                /*d bottom of creating 'costing' records */
                {edepiap.z99 &bottom_apebc = "*"}

            end. /* do for costing records */


        /*d Create general ledger postings. 3 possible postings may occur
            per invoice:
         1. AP Trade Unmatched: total costed po's + total capital addons
           (v-aptradeamt)
         2. AP Expensed: total expensed addons
            (t-expaddamt)
         3. Difference posting: Invoice amount - (costed po's + capital addons)
                                - total expensed addons - credit invoice amt
                                (v-netdiffamt)
    **************************************************************************/

            do for gleba:

                /*d Find next transno to use */
                find last gleba use-index k-gleba where
                    gleba.cono     = g-cono     and
                    gleba.batchnm  = p-batchnm  and
                    gleba.setno    = g-setno
                no-lock no-error.

          /*tb 22169 12/19/97 jlc; Allow credit invoices. If credit invoice has
                                   any purchase order lines on it, then we
                                   have to back out v-aptradamt and t-expaddamt
                                   from the credit invoice total */
                /*tb 25873 bm 02/16/99; Creating credit and difference postings
                    Only adjust t-creditamt if a credit invoice */

                /*tb 25486 bm 02/19/99; Load GST and PST to GLEBA.
                    Reduce v-netdiffamt and t-creditamt by gst and pst */
                assign
                    v-aptradeamt    =   t-pocostamt + t-capaddamt -
                                        (if g-wodiscamt <> 0 then g-wodiscamt
                                         else 0)
                    t-creditamt     =   if g-transtype = "cn"
                                        then t-creditamt - v-aptradeamt -
                                             t-expaddamt - dec(g-gst) -
                                             dec(g-pst)
                                        else t-creditamt
                    v-netdiffamt    =   round(dec(g-invamt),2) -
                                        round(v-aptradeamt,2) -
                                        round(t-expaddamt,2) -
                                        round(t-creditamt,2) -
                                        round(dec(g-gst),2) -
                                        round(dec(g-pst),2)
                    v-transno       =   if avail gleba then gleba.transno + 1
                                        else 1.

                /*d Update A/P Trade Unmatched */

                if v-aptradeamt ne 0 then do for sasc:
                    /*tb 25486 bm 02/19/99; Load GST and PST to GLEBA.
                      Default v-type to true so will fall to suspense debit  */
                    assign
                        v-posno    =   11
                        v-type     =   if g-transtype = "cn" then false
                                        else true
                        v-glcono   =   0
                        v-gldivno  =   0
                        v-gldeptno =   0
                        v-glacctno =   0
                        v-glsubno  =   0.
                    {p-glacct.i &div = "yes" &fldno = "2"}

                    create gleba.

            /*tb 21829 09/10/96 jkp; Changed GLEBA transcd from a 1 to a 0. */
            /*tb 22169 12/18/97 jlc; Credit invoices create transcd 1 */
            /*tb e12590 03/19/02 bm; Invoice posts to GL in foreign currency.*/
                    assign
                        gleba.cono     = g-cono
                        gleba.batchnm  = p-batchnm
                        gleba.setno    = g-setno
                        gleba.transno  = v-transno
                        gleba.transcd  = if g-transtype = "cn" then 1 else 0
                        gleba.gldivno  = v-gldivno
                        gleba.gldeptno = v-gldeptno
                        gleba.glacctno = v-glacctno
                        gleba.glsubno  = v-glsubno
                        gleba.amount   = v-aptradeamt *
                                         (if v-exchgrt ne 0 then v-exchgrt
                                          else 1)
                        v-transno      = v-transno + 1.
                end.  /* a/p trade unmatched */

             /*tb 25486 bm 02/19/99; Load GST and PST to GLEBA */
                /*d Update GST */
                if dec(g-gst) ne 0 then do for sasc:

                    {w-sasc.i no-lock}
                    assign
                        v-posno    =   0
                        v-type     =   if g-transtype = "cn" then false
                                       else true
                        v-glcono   =   0
                        v-gldivno  =   if g-gldivfl and sasc.glacctno2[9] ne 0
                                       then g-divno else sasc.gldivno2[9]
                        v-gldeptno =   sasc.gldeptno2[9]
                        v-glacctno =   sasc.glacctno2[9]
                        v-glsubno  =   sasc.glsubno2[9].

                        /*e When v-posno = 0 and one of v-gl vars are passed in,
                            Does not try to determine account from sasoo.
                            The account as passed is checked for validity,
                            If not valid then is set to suspense.
                            Only processing is if &div = yes,
                            determines division then verifies account */
                        {p-glacct.i &div = "yes"}

                    create gleba.

            /*tb 21829 09/10/96 jkp; Changed GLEBA transcd from a 1 to a 0. */
            /*tb 22169 12/18/97 jlc; Credit invoices create transcd 1 */
            /*tb e12590 03/19/02 bm; Invoice posts to GL in foreign currency.*/
                    assign
                        gleba.cono     = g-cono
                        gleba.batchnm  = p-batchnm
                        gleba.setno    = g-setno
                        gleba.transno  = v-transno
                        gleba.transcd  = if g-transtype = "cn" then 1 else 0
                        gleba.gldivno  = v-gldivno
                        gleba.gldeptno = v-gldeptno
                        gleba.glacctno = v-glacctno
                        gleba.glsubno  = v-glsubno
                        gleba.amount   = dec(g-gst) *
                                         (if v-exchgrt ne 0 then v-exchgrt
                                          else 1)
                        v-transno      = v-transno + 1.
                end.  /* gst */

                /*d Update PST */
                if dec(g-pst) ne 0 then do for sasc:

                    {w-sasc.i no-lock}
                    assign
                        v-posno    =   0
                        v-type     =   if g-transtype = "cn" then false
                                       else true
                        v-glcono   =   0
                        v-gldivno  =   if g-gldivfl and sasc.glacctno[9] ne 0
                                       then g-divno else sasc.gldivno[9]
                        v-gldeptno =   sasc.gldeptno[9]
                        v-glacctno =   sasc.glacctno[9]
                        v-glsubno  =   sasc.glsubno[9].

                        {p-glacct.i &div = "yes"}

                    create gleba.

            /*tb 21829 09/10/96 jkp; Changed GLEBA transcd from a 1 to a 0. */
            /*tb 22169 12/18/97 jlc; Credit invoices create transcd 1 */
            /*tb e12590 03/19/02 bm; Invoice posts to GL in foreign currency.*/
                    assign
                        gleba.cono     = g-cono
                        gleba.batchnm  = p-batchnm
                        gleba.setno    = g-setno
                        gleba.transno  = v-transno
                        gleba.transcd  = if g-transtype = "cn" then 1 else 0
                        gleba.gldivno  = v-gldivno
                        gleba.gldeptno = v-gldeptno
                        gleba.glacctno = v-glacctno
                        gleba.glsubno  = v-glsubno
                        gleba.amount   = dec(g-pst) *
                                         (if v-exchgrt ne 0 then v-exchgrt
                                          else 1)
                        v-transno      = v-transno + 1.

                end.  /* pst */

                /*tb 22169 jlc 12/18/97; Allow credit invoices */
                /*d Update A/P Expense for Credit Invoices with no PO's*/

                {edepiap.z99 &b4_glpost_exp = "*"}

                /*tb e13875 07/24/02 gwk; We want to post to the AP Expense
                    account when we have an invoice record with no items
                    (g-trastype = "di" and g-itemfl = no) */
                if t-creditamt ne 0     or
                    (g-itemfl = no and g-transtype = "DI":u)
                then do for sasc:

                    /*tb 25486 bm 02/19/99; Load GST and PST to GLEBA.
                       Default v-type to true so will fall to suspense debit */
                    /*tb e12590 03/19/02 bm; Invoice posts to GL in foreign
                    currency.*/

                    /*tb e13875 07/26/02 gwk; If the vendor record has a GL
                        acct for AP Expense, then we want to use that. */
                    v-gldivno  =   0.

                    {edepiap.z99 &bef_AP_expense = "*"}

                    if avail b-apsv then
                        assign
                            v-gldivno  = if g-gldivfl then b-apsv.divno
                                         else b-apsv.gldivno[4]
                            v-gldeptno = b-apsv.gldeptno[4]
                            v-glacctno = b-apsv.glacctno[4]
                            v-glsubno  = b-apsv.glsubno[4].

                    /*d If APSV record does not have an AP Expense account,
                        then get the account from oper sasoo using p-glacct.i */
                    if v-gldeptno + v-glacctno + v-glsubno = 0
                    then do:
                        assign
                            v-posno    =   10
                            v-type     =   if g-transtype = "cn":u then false
                                           else true
                            v-glcono   =   0
                            v-gldeptno =   0
                            v-glacctno =   0
                            v-glsubno  =   0.
                        {p-glacct.i &div = "yes"}

                    end. /* apsv does not have ap expense account */

                    {edepiap.z99 &aft_AP_expense = "*"}

                    create gleba.

                    /*tb e13875 07/24/02 gwk; If we're dealing with an invoice
                        with no items (expense invoice), then t-creditamt is
                        zero, so we use g-invamt for the posting.  We're in
                        this code block ONLY if t-creditamt is not zero (a
                        credit) OR g-itemfl is no and g-transtype is "DI" (an
                        invoice with no item records -- expense invoice) */
                    assign
                        gleba.cono     = g-cono
                        gleba.batchnm  = p-batchnm
                        gleba.setno    = g-setno
                        gleba.transno  = v-transno
                        gleba.transcd  = if g-transtype = "cn":u then 1 else 0
                        gleba.gldivno  = v-gldivno
                        gleba.gldeptno = v-gldeptno
                        gleba.glacctno = v-glacctno
                        gleba.glsubno  = v-glsubno
                        gleba.amount   = (if t-creditamt <> 0 then t-creditamt
                                          else dec(g-invamt))
                                           *
                                         (if v-exchgrt ne 0 then v-exchgrt
                                          else 1)
                        v-netdiffamt   = if t-creditamt <> 0 then
                                           v-netdiffamt
                                         else v-netdiffamt - dec(g-invamt)
                        v-transno      = v-transno + 1.
                end.  /* a/p expense */

                /*d Update A/P Expensed */
                if t-expaddamt ne 0 or
                   can-find(first report use-index k-cust where
                       report.cono     = g-cono     and
                       report.oper2    = g-operinit and
                       report.reportnm = v-reportnm and
                       report.outputty = "a":u      and
                       report.de9d2s   <> 0)
                then do for sasc:

                /*tb 25486 bm 02/19/99; Load GST and PST to GLEBA.
                 Default v-type to true so will fall to suspense debit  */
                    assign
                        v-glcono    =   0
                        v-type      =   if g-transtype = "cn" then false
                                        else true
                        v-posno     =   0.

            /*d Processed Expensed Addons */
            /*tb 15807 02/16/96 jkp; Extended the w-addon loop to after the
              create and assign of the gleba records.  Also moved the delete
              of the w-addon record with the end of the loop.  In addition,
              changed the gleba.amount to w-addon.origamt from t-expaddamt.
              This outputs the individual amount for the  addon instead of the
              total for all addons. */
            /*tb 22169 12/18/97 jlc; Credit invoices create 1 transcd */
            /*tb 26238 06/03/99 bm;  For divisionalized company,
               load addon divno based on global value not sastn */
            /*tb e12590 03/19/02 bm; Invoice posts to GL in foreign currency.*/

                    for each w-addon:
                        {w-sastn.i "x" w-addon.addonno no-lock}
                        if avail sastn then
                            assign
                                v-gldivno   =   if g-gldivfl and
                                                    sastn.glacctno ne 0
                                                then g-divno else sastn.gldivno
                                v-gldeptno  =   sastn.gldeptno
                                v-glacctno  =   sastn.glacctno
                                v-glsubno   =   sastn.glsubno.

                        {p-glacct.i &div = "yes"}

                        {edepiap.z99 &update_addons = "*"}

                        create gleba.

                        assign
                            gleba.cono     = g-cono
                            gleba.batchnm  = p-batchnm
                            gleba.setno    = g-setno
                            gleba.transno  = v-transno
                            gleba.transcd  = if g-transtype = "cn" then 1 else 0
                            gleba.gldivno  = v-gldivno
                            gleba.gldeptno = v-gldeptno
                            gleba.glacctno = v-glacctno
                            gleba.glsubno  = v-glsubno
                            gleba.amount   = w-addon.origamt *
                                             (if v-exchgrt ne 0 then v-exchgrt

                                              else 1)
                            v-transno      = v-transno + 1.

                    /*tb 21264 05/31/96 gdf; Added the following block to
                       create temporary poela records for expensed addons. */
                    /*tb 21970 10/01/96 jkp; Added find on POELA to see if the
                       record already exists before creating it.  If it exists,
                       add the expensed addon amount to the record found.  If
                       it does not exist, create it. */
                    /*tb 21970 10/22/96 jkp; Added the use of the v-expjrnlno
                       to carry the value for expensed journal numbers (-1 *
                       g-jrnlno) instead of using the full expression in the
                       finds/for eaches, etc. */
                        do for poela:

                            /*e Cannot use w-poela.i include because all fields
                                are not being used in the search. */
                            find first poela use-index k-poela where
                                       poela.cono    = g-cono          and
                                       poela.jrnlno  = v-expjrnlno     and
                                       poela.setno   = g-setno         and
                                       poela.addonno = w-addon.addonno
                            exclusive-lock no-error.

                            if avail poela then
                            do:

                                poela.addonamt = poela.addonamt +
                                                 w-addon.origamt.

                                {t-all.i "poela"}

                            end. /* if avail poela */

                            else
                            do:

                                create poela.

                        /*tb 21970 10/22/96 jkp; Changed poela.jrnlno =
                             (-1 * g-jrnlno) to poela.jrnlno = v-expjrnlno. */
                                assign
                                    poela.cono         = g-cono
                                    poela.jrnlno       = v-expjrnlno
                                    poela.setno        = g-setno
                                    poela.vendno       = g-vendno
                                    poela.lineno       = v-explineno
                                    poela.addonno      = w-addon.addonno
                                    poela.addonamt     = w-addon.origamt
                                    poela.src          = "Exp"
                                    poela.updatefl     = no
                                    v-explineno        = v-explineno + 1.

                                {t-all.i "poela"}

                            end. /* not avail poela */

                        end. /* do for poela */

                        delete w-addon.

                    end. /* w-addon loop */

                end.  /* end a/p expensed */

                {edepiap.z99 &b4_gldiff = "*"}

                /*d Net Difference G/L Posting */
                if v-netdiffamt ne 0 then do for sasc:

                    /*tb 25486 bm 02/19/99; Load GST and PST to GLEBA.
                    Default v-type to true so will fall to suspense debit  */
                    assign
                        v-glcono    =   0
                        v-gldivno   =   0
                        v-gldeptno  =   0
                        v-glacctno  =   0
                        v-glsubno   =   0
                        v-type      =   if g-transtype = "cn" then
                                            false else true
                        v-posno     =   0.

                    if  p-glno ne " " then do:
                        run acctcd.p(p-glno, yes, output v-gldivno,
                                     output v-gldeptno,
                                     output v-glacctno, output v-glsubno).

                    end.  /* p-glno */
                    {p-glacct.i &div = "yes"}

                    create gleba.

            /*tb 21829 09/10/96 jkp; Changed the GLEBA transcd from a 1 to a                         0. */
            /*tb 22169 12/18/97 jlc; Credit invoice create transcd 1 */
            /*tb e12590 03/19/02 bm; Invoice posts to GL in foreign currency.*/
                    assign
                        gleba.cono     = g-cono
                        gleba.batchnm  = p-batchnm
                        gleba.setno    = g-setno
                        gleba.transno  = v-transno
                        gleba.transcd  = if g-transtype = "cn" then 1 else 0
                        gleba.gldivno  = v-gldivno
                        gleba.gldeptno = v-gldeptno
                        gleba.glacctno = v-glacctno
                        gleba.glsubno  = v-glsubno
                        gleba.amount   = v-netdiffamt *
                                         (if v-exchgrt ne 0 then v-exchgrt
                                          else 1)
                        v-transno      = v-transno + 1.

                end.  /* net difference posting */


            end.  /* do for gleba */
        end. /*else do */
    end. /*if p-updfl */
            /*d Bottom of main block */
            {edepiap.z99 &bottom_main = "*"}

end.  /* end main */
