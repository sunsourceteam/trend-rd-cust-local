z-ponoline = string(g-pono,">>>>>>9") + "-" + string(g-posuf,"99") + " "
           + string(poelo.lineno,">>9") + " " 
           + string(poelo.seqno,">>9").

if avail poelo then
  do:
  find poel where poel.cono = g-cono and
                  poel.pono = g-pono and
                  poel.posuf = g-posuf  and
                  poel.lineno = poelo.lineno no-lock no-error.
  if avail poel then 
     assign z-shipprod = poel.shipprod.
  else
     assign z-shipprod = "". 
  
  assign z-orderno =  poelo.orderaltno
         z-ordersuf = poelo.orderaltsuf
         z-lineno   = poelo.linealtno
         z-seqno    = poelo.seqaltno.
  end.       
else
  do:
  assign z-orderno =  0
         z-ordersuf = 0
         z-lineno   = 0
         z-seqno    = 0
         z-shipprod = "".
  end.       



display 
  z-ponoline
  z-shipprod
  z-orderno
  z-ordersuf
  z-lineno
  z-seqno
  with frame f-potiel.

