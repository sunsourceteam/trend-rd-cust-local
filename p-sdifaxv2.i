
/*h*****************************************************************************
  INCLUDE      : p-faxv2.i - SDI custom p-sdifaxv2.i 
  DESCRIPTION  : Vendor fax
  USED ONCE?   : no (bpepvf1.p, poeppf1.p)
  AUTHOR       : enp
  DATE WRITTEN : 11/27/92
  CHANGES MADE :
    11/27/92 enp; TB#  8889 Add VisiFax Capability
    08/20/93 enp; TB# 12648 VSIFAX from F10 prints do not compress
    09/07/93 rs ; TB# 12932 Trend Doc Mgr for Rel 5.2
    10/20/93 enp; TB# 13098 Allow an 'l' in 10 digit phone numbe
    11/09/94 rs ; TB# 17051 Transfer Info To Notification Msg
    09/15/95 dww; TB# 19314 Remove Imbedded spaces from the Fax Phone Number
    10/10/95 kr ; TB# 19526 Renamed the reference to program
        from poepf1.p to poeppf1.p in the header
    09/11/96 dww; TB# 21866 Array Subscript 0 is out of range
    04/07/98 cm;  TB# 24793 Wrong page size used for fax output
    09/08/98 jl;  TB# 25144 Add the interface to Vsifax Gold. Added vfx
    05/04/99 cm;  TB# 26054 added &comui functionality
    05/15/99 jl;  TB# 26204 Add nxtfax script logic
    10/08/99 kjb; TB# 26690 Added RxLaser and Optio 3.0 logic to standard
    10/25/99 cm;  TB# 26054 Remove OPTIO interface
    01/06/00 rgm; TB# 27121 Optio Printer language information
    03/29/00 rgm; TB# 3-2  Add RxServer Interface to Standard
    04/28/00 rgm; TB# A9   Fix for RxServer Faxing issue
    07/18/00 sbr; TB# A18  Rxserver Faxing No notes on cover sheet
    09/27/00 bpa; TB# A49  VsiFax not including comments (memo) on cover sheet
    10/23/00 lbr; TB# e6867 From field not using screen value
    04/24/01 kjb; TB# e8618 When faxing through RxServer, the footer frame
        was appearing in the body of the document
    06/20/01 lbr; TB# e9095 Added PC User Name and PC Password for
        conformation of rxlaser faxing
    04/08/02 rgm; TB# e11405 Moved user name and password to the end and
        added email address
    05/22/02 rcl; TB# e13657 VSIfax extra blank page.
    05/30/02 rgm; TB# e12526 VSIFax e-mail confirmation correction for NxTFax
    08/13/02 lcb; TB# e12313 Report Manager overwriting output files
    12/30/03 rgm; TB# e18708 VSFax needs fax number quoted to handle spaces
    04/02/04 rgm; TB# e19110 Change e-mail fields to minimum length 60
    04/03/07 mtt; TB# e20599 Backout changed for-Add additional address lines
******************************************************************************/

/*e Parameters
   &extent = extent from sasc (1=PO, 2=OEEPA 3=OEEPI 4=ARES 5=BPEPB 6=BPEPV
   &file   = filename
*******************************************************************************/

/*d Faxbox */
/*tb A9 04/28/00 rgm; Fix for Rxserver faxing, add rxserv.lcn */
if not ({nxtfax.lcn  &file = "sasp."} or
        {vsifax.lcn &file = "sasp."} or
        {rxserv.lcn &file = "sasp." &rxstype = "FP"})
then do:

    output through value(sasp.pcommand) paged.

    /*tb 19314 09/15/95 dww; Remove Imbedded Space from Fax Phone Number */
    {fixphone.gca {&file}faxphoneno s-faxphoneno {&comvar}}

    assign
        v-faxstring = "^h" + v-wide +
                      (if sasc.oifaxpreno = "1" and
                          substring(s-faxphoneno,1,1) = "1" then ""
                       else sasc.oifaxpreno) +
                      s-faxphoneno + sasc.oifaxsufno + "."
        v-faxto[1]  = {&file}name
        v-faxto[2]  = if sasc.oifaxattn[{&extent}] then {&file}slsnm
                      else {&file}expednm.

    put unformatted v-faxstring.

    if sasc.oifg begins "j" then put unformatted "^" + sasc.oifg.

    display
        sasc.name
        v-faxto
        v-title
        s-faxphoneno
        v-from
    with frame f-fax.
    page.

end. /* FaxBox */

/*d Visifax */
else do:

    /*tb 26690 10/08/99 kjb; Add email notification address logic needed
        for faxing with Optio.  Define these variables only once - in case
        this include is included twice */
    &IF DEFINED(defined-sasoo)=0 &THEN
        &GLOBAL-DEFINE defined-sasoo TRUE
        def buffer z-sasoo     for sasoo.
        def var    z-emailaddr as char format "x(60)" no-undo.
        def var    z-emailfl   as log                 no-undo.
        def var    z-type      as char format "x(5)"  no-undo.
    &ENDIF

    assign
        z-emailfl   = no
        z-emailaddr = ""
        z-type      = if      {&extent} = 1 then "POEPP"
                      else if {&extent} = 2 then "OEEPA"
                      else if {&extent} = 3 then "OEEPI"
                      else if {&extent} = 4 then "ARES"
                      else if {&extent} = 5 then "BPEPB"
                      else                       "BPEPV".

     /* tb e9095 Added pcusername and pcpassword for email notification
         for rxserver */
     for first z-sasoo fields (cono oper2 tqemailaddr pcpassword pcusername)
      where
        z-sasoo.cono         = g-cono     and
        z-sasoo.oper2        = g-operinit
    no-lock:
        assign
            z-pcusername = z-sasoo.pcusername
            z-pcpassword = z-sasoo.pcpassword.
     end.

     /* set the e-mail address from pv_user */
     z-pcemailaddr = "".
     for first pv_user fields(email) where
         pv_user.cono  = g-cono and
         pv_user.oper2 = g-operinit
     no-lock:
          assign
                z-emailfl     = if pv_user.email <> ""
                                 then yes else no
                z-emailaddr   = pv_user.email
                z-pcemailaddr = pv_user.email.
     end.

    /*tb 19314 09/15/95 dww; Remove Imbedded Space from Fax Phone Number */
    {fixphone.gca {&file}faxphoneno s-faxphoneno "/*"}

    /*tb 17051 11/09/94 rs; Transfer Info to Notification Msg */
    /*tb 26690 10/08/99 kjb; Moved assignment of v-faxtag2 outside of
        conditional loop so it can be used in more than one place. */
    /*tb A18 07/18/00 sbr; Added v-from to the assign statement */
    /*d tag1 & tag2 will be set for custom Transmit Notify Prog */
    assign
        v-faxto[1]   = {&file}name
        v-faxto[2]   = if sasc.oifaxattn[{&extent}] then {&file}slsnm
                       else {&file}expednm
        v-faxcmd     =  (if sasc.oifaxpreno = "1" and
                            substring(s-faxphoneno,1,1) = "1" then ""
                        else sasc.oifaxpreno) +
                       s-faxphoneno + sasc.oifaxsufno
        v-faxtag2    = {&tag}
        v-pcommand   = sasp.pcommand
        v-from       = if v-from = "" then
                           "Purchasing"
                       else
                           v-from.

    find sassr where recid(sassr) = v-sassrid no-lock no-error.

    /*tb #6867 10/23/00 lbr; Added v-faxfrom = v-from */
    if {nxtfax.lcn &file = "sasp."} then
        assign
            v-faxfrom    = v-from
            v-faxto1     = v-faxto[1]
            v-faxto2     = v-faxto[2]
            v-faxtag1    = v-faxto[1]
            v-faxtag2    = {&tag}
            v-faxphoneno = v-faxcmd
            {nxtfax.las &saspcommand = "sasp.pcommand"
                        &newcommand  = "v-pcommand"
                        &faxinfo     = "v-"
                        &com         = "/*"
                        &filecom     = "/*"
                        &emailvar    = z-emailaddr
                        &wide        = "
                      (if avail sassr and
                       ((v-reportno eq 0 and sassr.wide = yes) or
                        (v-reportno ne 0 and sassr.xwide[v-reportno] = yes))
                       then true
                       else false)"}.

    /*tb A18 07/18/00 sbr; Use v-from variable instead of "Purchasing" */
    else if sasp.pcommand begins "fx" then
            v-faxcmd     = " -n " + '"' + v-faxcmd + '"' +
                           " -t TOCO=" +
                           "~"" + v-faxto[1] + "~"" +
                           " -t TONAME=" +
                           "~"" + v-faxto[2] + "~"" +
                           " -t FXNAME=" +
                           "~"" + v-from + "~"" +
                           " -t tag1=" +
                           "~"" + v-faxto[1] + "~"" +
                           " -t tag2=" +
                           "~"" + {&tag} + "~"".

    /*tb 26690 10/08/99 kjb; Added code to allow RxLaser fax */
    /*tb A9 04/28/00 rgm; Fix for Rxserver faxing, add rxserv.lcn */
    else if {rxserv.lcn &file = "sasp." &rxstype = "F"} then
        assign
            v-faxstring = (if sasc.oifaxpreno = "1" and
                              substring(s-faxphoneno,1,1) = "1" then ""
                           else sasc.oifaxpreno) +
                                s-faxphoneno     +
                                sasc.oifaxsufno
            v-faxcmd    = "".

    /*tb 26690 10/08/99 kjb; Added code to allow Optio fax */
    else if index(sasp.pcommand, "optio":U ) > 0 then
        assign
            v-faxstring = (if sasc.oifaxpreno = "1" and
                              substring(s-faxphoneno,1,1) = "1" then ""
                           else sasc.oifaxpreno) +
                                s-faxphoneno     +
                                sasc.oifaxsufno
            v-faxcmd    = "".

    /*tb A18 07/18/00 sbr; Use v-from variable instead of "Purchasing" */
    /*tb A50 09/25/00 bpa; Include comments (memo) */
    else do:
        run get-printdir.
        /*tb A49 09/27/00 bpa; Needed to move the creation of
           memo file to procedure in p-fax1.i; broke 64K segment */
        run create-memofile ("yes").
        v-faxcmd    = " -n " + '"' + v-faxcmd + '"' +
                     (" -t tco=" +
                      "~"" + v-faxto[1] + "~"" +
                      " -t tnm=" +
                      "~"" + v-faxto[2] + "~"" +
                      " -t fnm=" +
                      "~"" + v-from + "~"" +
                      " -t tg1=" +
                      "~"" + v-faxto[1] + "~"" +
                      " -t tg2=" +
                      "~"" + {&tag} + "~"" +
                         (if z-emailaddr="" then "" else
                          " -t mad=" + "~"" + z-emailaddr + "~"" ) +
                      (if v-memofl = no then "" else
                         " -t ntf=" + "~"" + z-memofilenm + "~"")).
              /*tb A49 09/27/00 bpa; added comments */

    end.

    /*tb 24793 04/07/98 cm; Wrong page size used for fax output */
    if avail sassr then do:

        if v-reportno = 0 then do:

            /*tb 26690 10/08/99 kjb; Added code to allow RxLaser fax */
            /*tb 3-2 03/29/00 rgm; Rxserver interface for faxing */
            /*tb A9 04/28/00 rgm; Fix for Rxserver faxing, add rxserv.lcn */
            if {rxserv.lcn &file = "sasp." &rxstype = "F"} then do:


                /*tb 3-2 03/29/00 rgm; Rxserver faxing, replaced code
                output through value(v-pcommand) value(v-faxcmd) paged
                page-size value(sassr.pglength).
                ** end commented out code */

                 run get-printdir.

                 /*tb A49 09/27/00 bpa; Needed to move the creation of
                    memo file to procedure in p-fax1.i; broke 64K segment */
                 run create-memofile ("").

                 /* Second call to get-printdir is no longer necessary as 
                    faxfilenm will be assigned based on the call to     
                    create-memofile. */

                 /* z-filenm will now be assigned based on the call to    
                    create-memofile. */

                   z-filenm = z-memofilenm + "b".

                 /*d Create body temp file */
                 /*tb A9 04/28/00 rgm; FIx for Rxserver faxing,
                                    add "paged" command */
                 /*tb e8618 04/24/01 kjb; Need to check the page length
                     set on the SASSR record */
                 output to value(z-filenm) paged
                     page-size value(sassr.pglength).

            end. /* RxLaser */

            /*tb 26690 10/08/99 kjb; Added code to allow Optio fax */
            else if index(sasp.pcommand, "optio":U ) > 0 then do:

                /*d If faxing through Optio, make the page length 66 to
                    accommodate all lines of header data */
                output through value(v-pcommand) value(v-faxcmd) paged
                page-size 66.

                /* TB#27121 rgm; get printer language value,
                after "-l" on command line */
                s-charpos = 0.
                e-charpos = 0.
                do v-charctr = 1 to 50:
                    if substring(sasp.pcommand,v-charctr,2) = "-l" then
                        s-charpos = v-charctr + 2.
                    if s-charpos gt 0 then
                        v-charctr = 51.
                end.
                if s-charpos gt 0 then do:
                    do v-charctr = s-charpos to 50:
                        if substring(sasp.pcommand,v-charctr,1) ne ""
                        then do:
                            s-charpos = v-charctr.
                            v-charctr = 51.
                        end.
                    end.
                    do v-charctr = s-charpos + 1 to 51:
                        if substring(sasp.pcommand,v-charctr,1) = "" then
                            e-charpos = v-charctr.
                        if e-charpos gt 0 then
                            v-charctr = 51.
                    end.
                    v-printtype = substring(sasp.pcommand,
                                  s-charpos, e-charpos - s-charpos).
                end.
                else v-printtype = "PCL5GL2". /* default value */

                /*d Create Coversheet memo comments file */
                /*tb A18 07/18/00 sbr; Use v-from variable instead of
                    "Purchasing" and also use v-faxcom for &fax_note1-10 */
                {optioprt.gpr &version        = '3.0'
                              &document       = sassr.currproc
                              &operinit       = g-operinit
                              &date           = string(today)
                              &time           = string(time,'HH:MM')
                              &printfl        = 'no'

                              &printer        = sasp.printernm
                              &printerlang    = v-printtype
                              &language       = 'ENGLISH'
                              &emailfl        = 'no'
                              &webpagefl      = 'no'
                              &filefl         = 'no'
                              &copies         = '1'
                              &faxfl          = 'yes'
                              &fax_tonumber   = v-faxstring
                              &fax_tocompany  = v-faxto[1]
                              &fax_toname     = v-faxto[2]
                              &fax_fromname   = v-from
                              &fax_class      = 'any'
                              &fax_compressfl = string(sassr.wide,'yes/no')
                              &fax_priority   = '5'
                              &fax_coverfl    = 'yes'
                              &fax_senddate   = 'TODAY'
                              &fax_sendtime   = 'NOW'
                              &fax_notifyfl   = z-emailfl
                              &fax_notifyaddr = z-emailaddr
                              &fax_nnotes     = '0'
                              &fax_note1      = v-faxcom[1]
                              &fax_note2      = v-faxcom[2]
                              &fax_note3      = v-faxcom[3]
                              &fax_note4      = v-faxcom[4]
                              &fax_note5      = v-faxcom[5]
                              &fax_note6      = v-faxcom[6]
                              &fax_note7      = v-faxcom[7]
                              &fax_note8      = v-faxcom[8]
                              &fax_note9      = v-faxcom[9]
                              &fax_note10     = v-faxcom[10]
                              &fax_tag        = ''
                              &fax_tag1       = v-faxto[1]
                              &fax_tag2       = v-faxtag2
                              &sascname       = sasc.name
                              &sascaddr1      = sasc.addr[1]
                              &sascaddr2      = sasc.addr[2]
                              &sasccsz        = "sasc.city  + ', ' +
                                                 sasc.state + ' '  +
                                                 sasc.zipcd"
                              &sascphone      = sasc.phoneno
                              &noformfeed     = "/*"}

                /*tb 26690 11/08/99 lbr; Added to correct problem of losing the
                    headers */
                page.

            end. /* Optio */

            else
                output through value(v-pcommand) value(v-faxcmd) paged
                page-size value(sassr.pglength).

        end. /* v-reportno = 0 */

        else if v-reportno ne 0 then do:

            /*tb 26690 10/08/99 kjb; Added code to allow RxLaser fax */
            /*tb 3-2 03/29/00 rgm; Rxserver interface for faxing */
            /*tb A9 04/28/00 rgm; Fix for Rxserver faxing, add rxserv.lcn */
            if {rxserv.lcn &file = "sasp." &rxstype = "F"} then do:

                /*tb 3-2 03/29/00 rgm; Rxserver faxing, replaced code
                output through value(v-pcommand) value(v-faxcmd) paged
                page-size value(sassr.pglength).
                ** end commented out code */

                run get-printdir.

                /*tb A49 09/27/00 bpa; Needed to move the creation of
                   memo file to procedure in p-fax1.i; broke 64K segment */
                run create-memofile ("").

                /* Second call to get-printdir is no longer necessary as   
                   z-filenm will be assigned based on the call to 
                   create-memofile. */

               /*z-filenm will now be assigned based on the call to  
                 create-memofile. */

                 z-filenm = z-memofilenm + "b".

                /*d Create body temp file */
                /*tb A9 04/28/00 rgm; FIx for Rxserver faxing,
                                   add "paged" command */
                /*tb e8618 04/24/01 kjb; Need to check the page length
                     set on the SASSR record */
                output to value(z-filenm) paged
                    page-size value(sassr.pglength).


            end. /* RxLaser */

            /*tb 26690 10/08/99 kjb; Added code to allow Optio fax */
            else if index(sasp.pcommand, "optio":U ) > 0 then do:

                /*d If faxing through Optio, make the page length 66 to
                    accommodate all lines of header data */
                output through value(v-pcommand) value(v-faxcmd) paged
                page-size 66.

                /* TB#27121 rgm; get printer language value,
                after "-l" on command line */
                s-charpos = 0.
                e-charpos = 0.
                do v-charctr = 1 to 50:
                    if substring(sasp.pcommand,v-charctr,2) = "-l" then
                        s-charpos = v-charctr + 2.
                    if s-charpos gt 0 then
                        v-charctr = 51.
                end.
                if s-charpos gt 0 then do:
                    do v-charctr = s-charpos to 50:
                        if substring(sasp.pcommand,v-charctr,1) ne ""
                        then do:
                            s-charpos = v-charctr.
                            v-charctr = 51.
                        end.
                    end.
                    do v-charctr = s-charpos + 1 to 51:
                        if substring(sasp.pcommand,v-charctr,1) = "" then
                            e-charpos = v-charctr.
                        if e-charpos gt 0 then
                            v-charctr = 51.
                    end.
                    v-printtype = substring(sasp.pcommand,
                                  s-charpos, e-charpos - s-charpos).
                end.
                else v-printtype = "PCL5GL2". /* default value */

                /*d Create Coversheet memo comments file */
                /*tb A18 07/18/00 sbr; Use v-from variable instead of
                    "Purchasing" and also use v-faxcom for &fax_note1-10 */
                {optioprt.gpr &version        = '3.0'
                              &document       = sassr.currproc
                              &operinit       = g-operinit
                              &date           = string(today)
                              &time           = string(time,'HH:MM')
                              &printfl        = 'no'
                              &printer        = sasp.printernm
                              &printerlang    = v-printtype
                              &language       = 'ENGLISH'
                              &emailfl        = 'no'
                              &webpagefl      = 'no'
                              &filefl         = 'no'
                              &copies         = '1'
                              &faxfl          = 'yes'
                              &fax_tonumber   = v-faxstring
                              &fax_tocompany  = v-faxto[1]
                              &fax_toname     = v-faxto[2]
                              &fax_fromname   = v-from
                              &fax_class      = 'any'
                              &fax_compressfl =
                                  string(sassr.xwide[v-reportno],'yes/no')
                              &fax_priority   = '5'
                              &fax_coverfl    = 'yes'
                              &fax_senddate   = 'TODAY'
                              &fax_sendtime   = 'NOW'
                              &fax_notifyfl   = z-emailfl
                              &fax_notifyaddr = z-emailaddr
                              &fax_nnotes     = '0'
                              &fax_note1      = v-faxcom[1]
                              &fax_note2      = v-faxcom[2]
                              &fax_note3      = v-faxcom[3]
                              &fax_note4      = v-faxcom[4]
                              &fax_note5      = v-faxcom[5]
                              &fax_note6      = v-faxcom[6]
                              &fax_note7      = v-faxcom[7]
                              &fax_note8      = v-faxcom[8]
                              &fax_note9      = v-faxcom[9]
                              &fax_note10     = v-faxcom[10]
                              &fax_tag        = ''
                              &fax_tag1       = v-faxto[1]
                              &fax_tag2       = v-faxtag2
                              &sascname       = sasc.name
                              &sascaddr1      = sasc.addr[1]
                              &sascaddr2      = sasc.addr[2]
                              &sasccsz        = "sasc.city  + ', ' +
                                                 sasc.state + ' '  +
                                                 sasc.zipcd"
                              &sascphone      = sasc.phoneno
                              &noformfeed     = "/*"}

                /*tb 26690 11/08/99 lbr; Added to correct problem of losing the
                    headers */
                page.

            end. /* Optio */

            else
                output through value(v-pcommand) value(v-faxcmd) paged
                page-size value(sassr.xpglength[v-reportno]).

        end. /* v-reportno ne 0 */

    end. /* if avail sassr */

    /*tb 3-2 03/29/00 rgm; Rxserver interface for faxing */
    else do:

        /*tb 3-2 03/29/00 rgm; Rxserver faxing replace code
        output through value(v-pcommand) value(v-faxcmd) paged.
        ***** this code was added to else stmt, so rxserver wouldnt run it
        ***** end commented out code */

        /*tb 26690 10/08/99 kjb; Added code to allow RxLaser fax */
        /*tb A9 04/28/00 rgm; Fix for Rxserver faxing, add rxserv.lcn */
        if {rxserv.lcn &file = "sasp." &rxstype = "F"} then do:

            run get-printdir.
            /*tb A49 09/27/00 bpa; Needed to move the creation of
               memo file to procedure in p-fax1.i; broke 64K segment */
            run create-memofile ("").

            /* Second call to get-printdir is no longer necessary as z-filenm
               will be assigned based on the call to create-memofile. */
           /* z-filenm will now be assigned based on the call to    
              create-memofile. */

              z-filenm = z-memofilenm + "b".

            /*d Create body temp file */
            /*tb A9 04/28/00 rgm; FIx for Rxserver faxing,
                               add "paged" command */
            output to value(z-filenm) paged.

        end. /* RxLaser */

        /*tb 3-2 03/29/00 rgm; Rxserver code from earlier commented section */
        else
            output through value(v-pcommand) value(v-faxcmd) paged.

        /*tb 26690 10/08/99 kjb; Added code to allow Optio fax */
        if index(sasp.pcommand, "optio":U ) > 0 then do:

            /* TB#27121 rgm; get printer language value,
            after "-l" on command line */
            s-charpos = 0.
            e-charpos = 0.
            do v-charctr = 1 to 50:
                if substring(sasp.pcommand,v-charctr,2) = "-l" then
                    s-charpos = v-charctr + 2.
                if s-charpos gt 0 then
                    v-charctr = 51.
            end.
            if s-charpos gt 0 then do:
                do v-charctr = s-charpos to 50:
                    if substring(sasp.pcommand,v-charctr,1) ne ""
                    then do:
                        s-charpos = v-charctr.
                        v-charctr = 51.
                    end.
                end.
                do v-charctr = s-charpos + 1 to 51:
                    if substring(sasp.pcommand,v-charctr,1) = "" then
                        e-charpos = v-charctr.
                    if e-charpos gt 0 then
                        v-charctr = 51.
                end.
                v-printtype = substring(sasp.pcommand,
                              s-charpos, e-charpos - s-charpos).
            end.
            else v-printtype = "PCL5GL2". /* default value */

            /*d Create Coversheet memo comments file */
            /*tb A18 07/18/00 sbr; Use v-from variable instead of
                "Purchasing" and also use v-faxcom for &fax_note1-10 */
            {optioprt.gpr &version        = '3.0'
                          &document       = z-type
                          &operinit       = g-operinit
                          &date           = string(today)
                          &time           = string(time,'HH:MM')
                          &printfl        = 'no'
                          &printer        = sasp.printernm
                          &printerlang    = v-printtype
                          &language       = 'ENGLISH'
                          &emailfl        = 'no'
                          &webpagefl      = 'no'
                          &filefl         = 'no'
                          &copies         = '1'
                          &faxfl          = 'yes'
                          &fax_tonumber   = v-faxstring
                          &fax_tocompany  = v-faxto[1]
                          &fax_toname     = v-faxto[2]
                          &fax_fromname   = v-from
                          &fax_class      = 'any'
                          &fax_compressfl = 'no'
                          &fax_priority   = '5'
                          &fax_coverfl    = 'yes'
                          &fax_senddate   = 'TODAY'
                          &fax_sendtime   = 'NOW'
                          &fax_notifyfl   = z-emailfl
                          &fax_notifyaddr = z-emailaddr
                          &fax_nnotes     = '0'
                          &fax_note1      = v-faxcom[1]

                          &fax_note2      = v-faxcom[2]
                          &fax_note3      = v-faxcom[3]
                          &fax_note4      = v-faxcom[4]
                          &fax_note5      = v-faxcom[5]
                          &fax_note6      = v-faxcom[6]
                          &fax_note7      = v-faxcom[7]
                          &fax_note8      = v-faxcom[8]
                          &fax_note9      = v-faxcom[9]
                          &fax_note10     = v-faxcom[10]
                          &fax_tag        = ''
                          &fax_tag1       = v-faxto[1]
                          &fax_tag2       = v-faxtag2
                          &sascname       = sasc.name
                          &sascaddr1      = sasc.addr[1]
                          &sascaddr2      = sasc.addr[2]
                          &sasccsz        = "sasc.city  + ', ' +
                                             sasc.state + ' '  +
                                             sasc.zipcd"
                          &sascphone      = sasc.phoneno}

        end. /* Optio */

    end. /* not avail sassr */

    /*tb 19314 09/15/95 dww; Remove Imbedded Space from Fax Phone Number */
    /*tb 21866 09/11/96 dww; Array subscript 0 is out of range */
    /*tb 22690 10/08/99 kjb; Do not include the compression character if
        faxing through Optio */
    /*tb 3-2 03/29/00 rgm; Rxserver added "rxs" flag for pcommand */
    /*tb A9 04/28/00 rgm; Fix for Rxserver faxing, add rxserv.lcn */
    if avail sassr                                           and
       not {nxtfax.lcn &file = "v-"}                         and
       not {rxserv.lcn &file = "sasp." &rxstype = "F"}       and
       index(sasp.pcommand, "optio":U ) = 0                  and
       ((v-reportno = 0  and sassr.wide = true) or
        (v-reportno ne 0 and sassr.xwide[v-reportno] = true))
    then do:
        v-faxcmd = chr(15).
        put control v-faxcmd.
    end.

end. /* else do VsiFax */

{&out} = if sasc.oifaxhardfl[{&extent}] then "p"
         else if v-coldfl then "x"
         else {&out}.



