{&com} 
define stream xprd.
define new shared stream xpcd.
define stream zprd.
define stream zpcd.


define var j-x as integer                         no-undo.
define var v-ohper  as dec                      no-undo.
define var v-totreg as dec                      no-undo.
define var v-calper as dec                      no-undo.
define var v-ooqty  as dec                      no-undo.
define var v-region as char format "x(1)"       no-undo.
define var v-district as char format "x(4)"     no-undo.
define var v-pcat     as char format "x(4)"     no-undo.
define var v-prod     as char format "x(24)"    no-undo.
define var v-cust     like arsc.custno          no-undo.
define var v-shipto   like arss.shipto          no-undo.
define var v-lsttrans as character              no-undo.
define var v-lstsale  as integer                no-undo.
define var v-skip     as char format "x(1)"     no-undo.
define var v-ytd as de                          no-undo.
define var v-lytd as de                         no-undo.
define var v-regions  as int      extent 5      no-undo.
define var v-inx      as int                    no-undo.
define var v-cnt      as int                    no-undo.
define var v-printdir like sasc.printdir        no-undo.
define var v-testmode as logical    init true   no-undo.
define var v-process as logical                 no-undo.
define var v-name as integer                    no-undo.

/* evaluate onhand for a quantity per region */

define new shared temp-table treg no-undo

field mgr   like smsn.mgr
field custno like arsc.custno
field shipto like arss.shipto
field qty   like icsw.qtyonhand
field val   like icsw.avgcost
field pct   as de


index i-mgr
      custno
      shipto
      mgr.

/{&com}* */ 



def {&new} temp-table xpart no-undo
    Field wxprod like icsw.prod
    Field wxreg as char
    Field wxpct as decimal
    Field wxlst as char
    Field wxcust like arsc.custno
    Field wxshipto like arss.shipto
index xpi
      wxcust
      wxshipto
      wxreg.


{&com} 

define temp-table spprd no-undo

  field region     as char format "x(1)"
  field district   as char format "x(3)"
  field prod       as char format "x(24)"
  field custno     like arsc.custno
  field shipto     like arss.shipto
  Field lstsale    as integer
  field lsttrans   as character
  field sales      as de
  field xpct       as de   

  index rdpinx
        region
        district
        custno
        shipto
        prod
  index rdpprd
        prod
        region
        district
        custno
        shipto.
        
        
define temp-table sppcd no-undo

  field region     as char format "x(1)"
  field district   as char format "x(3)"
  field custno     like arsc.custno
  field shipto     like arss.shipto
  field pcat       as char format "x(4)"
  field sales      as de
  field xpct       as de   

  index rdcinx
        custno
        shipto
        pcat
        region
        district
  index rdccat
        pcat
        custno
        shipto
        region
        district.       
        
define temp-table t-spprd no-undo

  field prod       as char format "x(24)"
  field sales      as de

  index rdpinx
        prod.
        
        
define temp-table t-sppcd no-undo

  field pcat       as char format "x(4)"
  field sales      as de

  index rdcinx
        pcat.
/{&com}* */ 

