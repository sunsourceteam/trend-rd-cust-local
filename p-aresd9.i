/* p-aresd9.i 1.7 7/13/93 */
/*h*****************************************************************************
  INCLUDE       : p-aresd1.i
  DESCRIPTION   : Detail Records for Statement Print, format1
  AUTHOR        :
  DATE WRITTEN  :
  CHANGES MADE  : 06/22/93 jrg; TB# XXXXX  Added "u" for UC records.
                  07/12/93 jrg; TB# 12196  Changed - to + on v-credits.
                  06/21/94 fef; TB# 15724  Include misc credits and future
                  dues (when option 8 = y) in shipto subtotal.
*******************************************************************************/

            {1}
            if first-of(b-report.c13) and b-report.c13 ne "" then do:
                assign v-subtot    = 0.
                display b-report.c13 with frame f-subtop.
            end.
            /{1}* */

            /*tb       06/22/93 jrg - added "u" for UC records*/
            /*tb 12196 07/12/93 jrg - Changed - to + on v-credits. */
            /*tb 15724 06/21/94 fef - Include misc credits and future dues
              (when option 8 = y) in v-subtot when printing in shipto order */
            /*tb       06/02/93 jrg - Removed desc on MC, using its invno now */

            if (not can-do("i,m,u",b-report.c1)) and p-actonlyfl
            and arsc.statementty ne "b" then next.

            assign
                v-charge    = if b-report.c1 = "i" then
                              b-report.de9d2s else 0
                v-credit    = if b-report.c1 ne "i" then
                              b-report.de9d2s else 0
                v-totbal    = v-totbal + (v-charge + v-credit)
                v-subtot    = v-subtot + if can-do("future,active,due",
                              b-report.c6) then (v-charge + v-credit) else 0
                v-invoice   = if b-report.c1 = "p" then b-report.c20   else
                              if b-report.c1 = "u" then "UNAPPLIED CASH" else
                              string(b-report.i7,">>>>>>9") + "-" +
                              string(b-report.i3,"99").
            v-custpo = "".        
            if b-report.c1 ne "p" and
               b-report.c1 ne "u" then do:
                  find first oeeh where oeeh.cono = g-cono and 
                                        oeeh.orderno = b-report.i7 and
                                        oeeh.ordersuf = b-report.i3
                                        no-lock no-error.
                  if avail(oeeh) then 
                     v-custpo = oeeh.custpo.
               end.            if v-charge ne 0 or v-credit ne 0 then
            display b-report.dt  b-report.dt-2
                    b-report.c6  b-report.c2
                    v-invoice
                    v-charge     when v-charge ne 0
                    v-credit     when v-credit ne 0
            with frame f-detail.
            
            if v-custpo ne "" then
               display v-custpolit v-custpo with frame f-custpo.
               
            if p-referfl and b-report.c24 ne ""
            then display b-report.c24 with frame f-refer.
            down with frame f-detail.
            {1}
            if last-of(b-report.c13) and b-report.c13 ne "" then do:
                display b-report.c13 v-subtot with frame f-subbot.
            end.
            /{1}* */
