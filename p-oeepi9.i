/*sx5.5*/
/* p-oeepi9.i 5/27/97 */
/*si**** Custom ****************************************************************
CLIENT  : 80281 SDI
JOB     : sdi009 Custom invoice print
AUTHOR  : msk
VERSION : 7
CHANGES :
    si01 5/27/97 msk; 80281/sdi009; initial changes
        - remove UPC vendor from header
        - swap ship-to and bill-to in header
        - remove corr/remit from header
        - add remittance stub section fo invoice
        - NOTE:  because of problems with this doc, v-tof has been included
          in the page-top frame
    si02 6/13/97 msk; 80281/sdi00901
        - use a ZZ note to print remit-to address
    si03 08/26/97 jsb; SDI00902
    si04 10/28/97 sv;  sdi027; Upgraded Trend v8
    si05 03/04/99 pcs; sdi065; added code for canadian Currency print
    si06 03/31/99 gc;  sdi065; fixed canadian tax equivalent message
    si07 04/08/99 gc;  sdi065; split pst/gst taxes
    it01 11/21/97 jph: add company headings to invoice
         03/11/09 tah: addon change
         04/22/09 das: promotion line change
    HST  07/06/10 tah: HST tax change
*******************************************************************************/

/* p-oeepi1.i 1.16 02/21/94 */
/*h*****************************************************************************
  INCLUDE      : p-oeepi1.i
  DESCRIPTION  : Print logic for printing invoices (oeepi1.p, oeepif1.p)
  USED ONCE?   : No
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    tb 4762 10/14/91 mwb; page#, order #, and shipped date to fit pre-prt
    tb 6889 06/06/92 mwb; replaced oeeh.payamt with oeeh.tendamt
    tb 2821 06/16/92 mkb; display totamt tendered on the original invoice
    tb 5413 06/20/92 mwb; if the header as a write off amount set - add it
        to the down payment display (BO rounding problems when down
        payment is applied).
    07/20/92 mkb; TB# 7191 Alphanumeric changes for custno
    tb 7497 09/01/92 smb; if arao flag says no to credit discounts then rm's and
        credit cr's shouldn't print the Cash Discount
    tb 8561 11/11/92 mms; Display product surcharge label from ICAO
    tb 9041 12/07/92 ntl; Canadian tax change
    tb 10838 04/07/93 jlc; Print Drop Ship on DOs
    04/28/93 rs; TB# 6404 Negative addon on RM update incorrect
    02/21/94 dww; TB# 13890 File name not specified causing error

    TAH 001 03/11/09 add 11 to printable addons 
    
*******************************************************************************/

def var cSlash         as char                  no-undo.
def var v-appliedamt   like aret.amount         no-undo.
def var v-custno       like aret.custno         no-undo.
def var v-statecan     like oeeh.shiptost       no-undo.
def var v-citycan      like oeeh.shiptocity     no-undo.    
def var x-promokey     like notes.primarykey    no-undo.

    assign
        v-pageno    = page-number - 1
        v-subnetamt = 0.
        
def var sdi-PerishTax as dec no-undo.

form header
    v-tof         format "x(12)"
    skip(1)
    s-lithd1 as c format "x(50)"                    at 43   /* beg of it01 */
    s-lithd2        as c format "x(17)"             at 45
    s-sname-phonehd                                 at 63
    s-lit1aa as c format "x(10)"         at  2
    s-lit1a  as c format "x(35)"         at 12
    s-semail                                        at 63
    s-lit1b as c format "x(39)"          at 93
    s-lit2a as c format "x(43)"          at  2
    s-lithd3        as c format "x(17)"             at 45
    s-cname-phonehd                                 at 63   
    oeeh.invoicedt                       at 108
    oeeh.orderno                         at 122     /* das - sx5.5 - was 123 */
    "-"                                  at 130
    oeeh.ordersuf                        at 131
    s-lit3a as c format "x(09)"          at 2
    oeeh.custno                          at 12
    s-cemail                                        at 63
    s-lit3b as c format "x(39)"          at 94
    s-duplicate as c format "x(25)"      at 53        /* si01; moved down */
    oeeh.enterdt                         at 94
    oeeh.custpo                          at 104
    page-num - v-pageno format "zzz9"    at 129
    s-financed  as c format "x(25)"      at 53        /* si01; moved down */
    /*skip(1)*/
    s-SPlit   as c format "x(13)"        at 89
    s-SPname  as c format "x(30)"        at 103
    s-lit11a as c format "x(8)"          at 2           /* si01 */
    s-billname like arsc.name            at 12          /* si01 */
/*    s-lit6b as c format "x(18)"          at 48
    b-sasc.conm                          at 68              si01 */
    s-radd1                              at 58          /* si02 */
    s-SPaddr1 as c format "x(30)"        at 103
    s-billaddr1 like arsc.name           at 12          /* si01 */
/*    b-sasc.addr[1]                       at 68            si01 */
    s-radd2                              at 58          /* si02 */
    s-SPaddr2 as c format "x(30)"        at 103
    s-billaddr2 like arsc.name           at 12          /* si01 */
/*    b-sasc.addr[2]                       at 68            si01 */
    s-radd3                              at 58          /* si02 */
    s-lit8a as c format "x(9)"           at 106
    s-pstlicense as c format "x(11)"     at 116
    s-billcity as c format "x(35)"       at 12          /* si01 */
/*    s-corrcity as c format "x(35)"       at 68            si01 */
    s-radd4                              at 58          /* si02 */
    s-lit9a as c format "x(9)"           at 106
    s-gstregno as c format "x(10)"       at 116
    s-billcountry as c format "x(30)"    at 12
    /*
    skip(1)
    */
    s-lit6a as c format "x(8)"           at 2           /* si01 */
    s-shiptonm like oeeh.shiptonm        at 12          /* si01 */
    s-lit11b as c format "x(12)"         at 50
    s-shiptoaddr[1]                      at 12          /* si01 */
    oeeh.shipinstr                       at 50
    s-shiptoaddr[2]                      at 12          /* si01 */
    s-lit12a as c format "x(10)"         at 50
    s-lit12b as c format "x(3)"          at 81
    s-lit12c as c format "x(7)"          at 105
    s-lit12d as c format "x(5)"          at 117
    s-shipcity as c format "x(35)"       at 12          /* si01 */
    s-whsedesc as c format "x(30)"       at 50
    s-shipvia as c format "x(12)"        at 81
    s-cod as c format "x(6)"             at 94
    s-shipdt like oeeh.shipdt            at 105
    s-terms as c format "x(12)"          at 117
    skip(1)
  /*s-rpttitle like sapb.rpttitle        at 1 skip(1)*/
    s-rpttitle as c format "x(130)"      at 1 skip(1)
with frame f-head no-box no-labels width 132 page-top.

form header
    s-lit14a as c format "x(75)"         at 1
    s-lit14b as c format "x(53)"         at 76
    s-lit15a as c format "x(75)"         at 1
    s-lit15b as c format "x(53)"         at 76
    s-lit16a as c format "x(78)"         at 1
    s-lit16b as c format "x(53)"         at 79
with frame f-headl no-box no-labels width 132 page-top.

form
    skip(1)
    v-linecnt         format "zz9"       at 1
    s-lit40a     as c format "x(11)"     at 5
    s-lit40c     as c format "x(17)"     at 44
    s-totqtyshp  as c format "x(13)"     at 62
    s-lit40d     as c format "x(18)"     at 97
    s-totlineamt as c format "x(13)"     at 117
with frame f-tot1 no-box no-labels no-underline width 132.

form
    s-canlit as c     format "x(60)"     at 30  /*si05*/ /*si07*/
    s-title2 as c format "x(20)"         at 97
    s-amt2   as dec format "zzzzzzzz9.99-" at 117 
    with frame f-tot2 no-box no-labels width 132 no-underline down.

form
    s-currencyty as c format "x(44)"     at 44  /*si05*/
    s-invtitle as c format "x(18)"       at 97
    s-totinvamt like oeeh.totinvamt      at 117
with frame f-tot3 no-box no-labels no-underline width 132.

form
   s-tracktitle as c format "x(21)"      at 2
with frame f-trackhdr no-box no-labels no-underline width 132.
form
  s-tracknbr    as c format "x(30)"      at 2
with frame f-trackdtl no-box no-labels no-underline width 132.
form
   skip(1)
   s-astline1   as c format "x(70)"      at 2
   s-remittmsg1 as c format "x(70)"      at 2
   s-remittmsg2 as c format "x(70)"      at 2
   s-astline2   as c format "x(70)"      at 2
   skip(1)
with frame f-remittmsg no-box no-labels no-underline width 132. 
assign s-astline1 = 
 "   *******************************************************************".
assign s-remittmsg1 =
 "   *** ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED ABOVE AND    ".
assign s-remittmsg2 = 
 "   ***       MAKE THE NECESSARY CHANGES TO YOUR RECORDS. THANK YOU    ". 
assign s-astline2 = 
 "   *******************************************************************".
form
   skip(1)
   s-BTastline1   as c format "x(103)"     at 2
   s-BTremittmsg1 as c format "x(70)"      at 2
   s-BTremittmsg2 as c format "x(70)"      at 2
   s-BTastline2   as c format "x(103)"     at 2
   skip(1)
with frame f-BTremittmsg no-box no-labels no-underline width 132. 
assign s-BTastline1 = fill("*",103).
assign s-BTremittmsg1 =
 "*** ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED ABOVE AND    ".
assign s-BTremittmsg2 = 
 "*** MAKE THE NECESSARY CHANGES TO YOUR RECORDS. THANK YOU    ". 
assign s-BTastline2 = fill("*",103).





form header

    /* si01; new stub stuff */
    s-stub1                         to 109
    v-stub1     when s-stub1 <> ""  at 111
    s-stub2                         to 109
    v-stub2     when s-stub1 <> ""  at 111
    s-stub3                         to 109
    v-stub3     when s-stub1 <> ""  at 111
    s-stub4                         to 109
    v-stub4     when s-stub1 <> ""  at 111
    s-stub5                         to 109
    v-stub5     when s-stub1 <> ""  to 130
    s-stub6                         to 109
    s-stub7                         to 109
    /* si01; end of new stub stuff */

    s-lit48a as c format "x(9)"          at 1
    s-lit48b as c format "x(13)"         when oeeh.termsdiscamt <> 0  /*si03*/
                                         at 76
    /*tb 07/06/92 mjq; per KMW
    oeeh.termsdiscamt                    at 90 */
    s-termsdiscamt as c format "x(13)"   when oeeh.termsdiscamt <> 0  /*si03*/
                                         at 90
    s-paid   as c format "x(20)"         when oeeh.termsdiscamt <> 0  /*si03*/
                                         at 104
    s-bmsga  as c format "x(130)"        at 2
with frame f-tot4 no-box no-labels width 132 page-bottom.

form header                     /*tb 7497 9/1/92 smb*/

    /* si01; new stub stuff */
    s-stub1                         to 109
    v-stub1     when s-stub1 <> ""  at 111
    s-stub2                         to 109
    v-stub2     when s-stub1 <> ""  at 111
    s-stub3                         to 109
    v-stub3     when s-stub1 <> ""  at 111
    s-stub4                         to 109
    v-stub4     when s-stub1 <> ""  at 111
    s-stub5                         to 109
    v-stub5     when s-stub1 <> ""  to 130
    s-stub6                         to 109
    s-stub7                         to 109
    /* si01; end of new stub stuff */

    s-lit48az as c format "x(9)"         at 1
    s-bmsgaz  as c format "x(130)"       at 2
with frame f-tot4z no-box no-labels width 132 page-bottom.

form    /*tb 2821 06/16/92 mkb */
    "Full Amount Tendered For All Orders:"  at 5
    oeeh.tottendamt                         at 42
    "*** Back Order/Release Exists ***"             at 5
with frame f-tot5 no-box no-labels width 132.

    assign w-div = arsc.divno.
    if w-div <> 30 and substr(oeeh.user5,1,1) = "u" then
      do:
      do i = 1 to 51:
        if oeeh.shiptost = all-states[i] then
          do:
          assign w-div = 30.
          leave.
        end.
      end.
    end.
    if g-ourproc = "oerd" then
      do:
      assign shippointfl = no.
      do i = 1 to 51:
        if oeeh.shiptost = all-states[i] then
          leave.
      end.
      if i > 51 then
        assign shippointfl = yes.
      if shippointfl = no and avail icsd then
        do:
        do i = 1 to 51:
          if icsd.state = all-states[i] then
            leave.
        end.
        if i > 51 then
          assign shippointfl = yes.
      end. /* shippointfl = no and avail icsd */
    end. /* g-ourproc = oerd */

    /*d Load the labels for the invoice print */
    {a-oeepi9.i s-}                                     /* si01 */
    {p-oeord.i "oeeh."}  /*** v-totlineamt ***/
    assign s-cantot = 0
           s-cantax1 = 0
           s-cantax2 = 0.
    /*d Load header fields */
    {w-sasta.i "S" oeeh.shipvia no-lock}
    {w-icsd.i oeeh.whse no-lock}
/* HST */    assign v-statecan = ""
/* HST */            v-citycan = "".
/* HST */     if oeeh.transtype = "cs" or oeeh.orderdisp = "W" then
/* HST */       assign v-statecan = icsd.state
/* HST */              v-citycan  = icsd.city.
/* HST */     else
/* HST */       assign v-statecan = oeeh.shiptost       
/* HST */              v-citycan  = oeeh.shiptocity.
    
    /* das - PerishTax (addon.addonno = 12 and 13) */
    assign sdi-PerishTax = 0.
    for each addon where addon.cono = g-cono and
                         addon.ordertype = "oe" and
                         addon.orderno = oeeh.orderno and
                         addon.ordersuf = oeeh.ordersuf and
                        (addon.addonno = 12 or addon.addonno = 13)
                         no-lock:
      assign sdi-PerishTax = sdi-PerishTax + addon.addonnet.
    end.
    
    /* 07/20/92 mkb; TB# 7191 Alphanumeric changes for custno
    if oeeh.fpcustno <> 0 and v-index = 1 and can-do("f,b",p-printto) then */
    if oeeh.fpcustno <> {c-empty.i} and
        v-index = 1 and can-do("f,b",p-printto) then
        find b-arsc where b-arsc.cono = g-cono and
             b-arsc.custno = oeeh.fpcustno no-lock no-error.
    /* 07/20/92 mkb; TB# 7191 Alphanumeric changes for custno
    assign s-billname   = if oeeh.fpcustno <> 0 and */
    assign s-billname   = if oeeh.fpcustno <> {c-empty.i} and
                             v-index = 1        and
                             avail b-arsc       and
                             can-do("f,b",p-printto) then b-arsc.name
                          else if v-invtofl /*then oeeh.shiptonm*/ 
                                  and avail arss then arss.name
                          else arsc.name
           /* 07/20/92 mkb; TB# 7191 Alphanumeric changes for custno
           s-billaddr1  = if oeeh.fpcustno <> 0 and */
           s-billaddr1  = if oeeh.fpcustno <> {c-empty.i} and
                             v-index = 1        and
                             avail b-arsc       and
                             can-do("f,b",p-printto) then b-arsc.addr[1]
                          else if v-invtofl /*then oeeh.shiptoaddr[1]*/
                                  and avail arss then arss.addr[1]
                          else arsc.addr[1]
           /* 07/20/92 mkb; TB# 7191 Alphanumeric changes for custno
           s-billaddr2  = if oeeh.fpcustno <> 0 and */
           s-billaddr2  = if oeeh.fpcustno <> {c-empty.i} and
                             v-index = 1        and
                             avail b-arsc       and
                             can-do("f,b",p-printto) then b-arsc.addr[2]
                          else if v-invtofl /*then oeeh.shiptoaddr[2]*/
                                  and avail arss then arss.addr[2]
                          else arsc.addr[2]
           /* 07/20/92 mkb; TB# 7191 Alphanumeric changes for custno
           s-billcity   = if oeeh.fpcustno <> 0 and */
           s-billcity   = if oeeh.fpcustno <> {c-empty.i} and
                             v-index = 1        and
                             avail b-arsc       and
                             can-do("f,b",p-printto) then
                            b-arsc.city + ", " + b-arsc.state + " " +
                            b-arsc.zipcd
                          else if v-invtofl /*then 
                            oeeh.shiptocity + ", " + oeeh.shiptost +
                            " " + oeeh.shiptozip*/ and avail arss then
                            arss.city + ", " + arss.state + " " + arss.zipcd
                          else
                            arsc.city + ", " + arsc.state + " " + arsc.zipcd
           s-billcountry = if avail arsc and arsc.countrycd = "CA" then "CANADA"
                        /*   else 
                           if avail arsc and arsc.countrycd = "CN" then "CANADA"
                        */  
                           else
                           if avail arsc and arsc.countrycd = "MX" then "MEXICO"
                           else " "
           /* si01; omit corr/remit stuff
           s-corrcity   = b-sasc.city + ", " + b-sasc.state + " " + b-sasc.zipcd
           */

           s-duplicate  = if oeeh.stagecd = 9 then "*** C A N C E L L E D ***"
                          else if sapbo.reprintfl = no or
                                  p-reprintmsg = yes then ""
                          else "*** D U P L I C A T E ***"
           /* 07/20/92 mkb; TB# 7191 Alphanumeric changes for custno
           s-financed   = if oeeh.fpcustno = 0 then "" */
           s-financed   = if oeeh.fpcustno = {c-empty.i} then ""
                          else "***  F I N A N C E D  ***"
           s-cod        = if oeeh.codfl = no then "" else "C.O.D."
           s-shipdt     = if oeeh.shipdt = ? then oeeh.pickeddt else oeeh.shipdt
           s-totlineamt = if oeeh.lumpbillfl then
                               string(oeeh.lumpbillamt,"zzzzzzzz9.99-")
                          else if oeeh.transtype = "rm" then
                               string((oeeh.totlineamt * -1),"zzzzzzzz9.99-")
                          else string(v-totlineamt,"zzzzzzzz9.99-")
           s-taxes      = oeeh.taxamt[1] + oeeh.taxamt[2] +
                          oeeh.taxamt[3] + oeeh.taxamt[4] + sdi-PerishTax
           s-dwnpmtamt  = if oeeh.transtype = "cs" then oeeh.tendamt
                          else if oeeh.dwnpmttype then oeeh.dwnpmtamt
                          else (oeeh.dwnpmtamt / 100) * oeeh.totinvamt
           /*tb 5413 06/20/92 mwb */
           s-dwnpmtamt  = s-dwnpmtamt + oeeh.writeoffamt
           v-linecnt    = 0
           s-paid         = ""   /*si03*/
           s-termsdiscamt = ""   /*si03*/
           s-lit48b       = ""   /*si03*/
/*           s-paid       = if oeeh.discdt = ? then s-lit48d
                          else s-lit48c + " " + string(oeeh.discdt) si03*/
           s-shiptonm      = oeeh.shiptonm
           s-shiptoaddr[1] = oeeh.shiptoaddr[1]
           s-shiptoaddr[2] = oeeh.shiptoaddr[2]
           s-shipcity      = oeeh.shiptocity + ", " + oeeh.shiptost +
                             " " + oeeh.shiptozip
           s-gstregno      = if g-country = "ca" then b-sasc.fedtaxid else ""
           s-pstlicense    = if g-country = "ca" then oeeh.pstlicenseno
                             else ""
           s-shipvia       = if avail sasta then sasta.descrip else oeeh.shipvia
           /*tb 10838 04/07/93 jlc; Print Drop ship on DOs */
           s-whsedesc      = if oeeh.transtype = "do" then
                                "** Drop Ship **"
                             else
                                if avail icsd  then icsd.name   else oeeh.whse
           s-SPlit         = if g-ourproc = "oerd" and avail icsd and
                                shippointfl = yes then
                               "Shipped From:"
                             else " "
           s-SPname        = if g-ourproc = "oerd" and avail icsd and
                               shippointfl = yes then
                               icsd.name
                             else " "
           s-SPaddr1       = if g-ourproc = "oerd" and avail icsd and
                               shippointfl = yes then
                               icsd.addr[1]
                             else " "
           s-SPaddr2       = if g-ourproc = "oerd" and avail icsd and
                               shippointfl = yes then
                               string(icsd.city  + ", " + 
                                     icsd.state + " "  + icsd.zipcd,"x(30)")
                             else " ".
         /*s-rpttitle      = if p-promofl then sapb.rpttitle   else "".*/
    assign s-rpttitle = "".
    /* print GO GREEN Prom all the time */
    if (avail arss and substr(arss.user19,1,4) = "DJI ") or
       (avail arsc and substr(arsc.user19,1,4) = "DJI ") then
      assign x-promokey = "DJ Invoice Promo".
    else
    if (avail arss and substr(arss.user19,1,4) = "PERF") or
       (avail arsc and substr(arsc.user19,1,4) = "PERF") then
      assign x-promokey = "PERF Invoice Promo".
    else
    if (avail arss and substr(arss.user19,1,4) = "PGON") or
       (avail arsc and substr(arsc.user19,1,4) = "PGON") then
      assign x-promokey = "PGON Invoice Promo".
    else
      assign x-promokey = "Invoice Promo".
    find notes where notes.cono = g-cono and
                     notes.notestype = "zz" and
                     notes.primarykey = x-promokey
                     no-lock no-error.
    if avail notes then
      do:
      assign s-rpttitle = notes.noteln[1] + " " + notes.noteln[2] + " " +
                          notes.noteln[3].
      if LENGTH(s-phonehd) > 11 then
        assign s-rpttitle =
     REPLACE(s-rpttitle,"999-999-9999",string(s-phonehd,"(xxx)xxx-xxxx/xxxx")).
      else
        assign s-rpttitle = 
     REPLACE(s-rpttitle,"999-999-9999",string(s-phonehd,"(xxx)xxx-xxxx")).
    end.
    
    /****
    {w-arsc.i oeeh.custno no-lock}
    if avail arsc then do:
       if w-div <> 30 then
          assign s-rpttitle = "".
    end.
    ****/
      
    /*d Load addon descriptions based on language */
    
    
    /*{p-oeadfp.i}*/
    {w-sasta.i "t" oeeh.termstype no-lock}
    if oeeh.langcd <> "" then
      {w-sals.i oeeh.langcd "t" oeeh.termstype no-lock}
    s-terms = if oeeh.langcd <> "" and avail sals then sals.descrip[1]
              else if avail sasta then sasta.descrip
              else oeeh.termstype.

    view frame f-head.
    if oeeh.transtype = "ra" then hide frame f-headl.
    else view frame f-headl.
    s-lit48az = s-lit48a.
    /*s-bmsgaz  = s-bmsga.  */

    /* si01; start with clear stub text */
    assign
        s-stub1 = ""
        s-stub2 = ""
        s-stub3 = ""
        s-stub4 = ""
        s-stub5 = ""
        s-stub6 = ""
        s-stub7 = ""
        v-stub1 = ""
        v-stub2 = ""
        v-stub3 = ""
        v-stub4 = ""
        v-stub5 = ""
        s-bmsga = ""
        s-bmsgaz = "".

    if (oeeh.transtype = "rm" and /*tb 7497 9/1/92 smb*/
        not b-sasc.arcrdiscfl) or
       (oeeh.transtype = "cr" and
        not b-sasc.arcrdiscfl   and
        oeeh.totinvamt < 0) then view frame f-tot4z.
    else view frame f-tot4.
/* SX32
    /*d Print the customer notes  */
    if arsc.notesfl ne "" then
        for each notes where
                 notes.cono         = g-cono    and
                 notes.notestype    = "c"       and
                 notes.primarykey   = string(oeeh.custno) and
                 notes.secondarykey = ""
        no-lock:
            if notes.printfl = yes then do i = 1 to 16:
                /*tb 13890 02/21/94 dww; File name not specified causing
                     error */
                if notes.noteln[i] ne "" then do:
                    display notes.noteln[i] with frame f-notes.
                    down with frame f-notes.
                end.
            end.
        end.

    /*d Print the order notes  */
    if oeeh.notesfl ne "" then
        for each notes where
                 notes.cono         = g-cono    and
                 notes.notestype    = "o"       and
                 notes.primarykey   = string(oeeh.orderno) and
                 notes.secondarykey = ""
        no-lock:
            if notes.printfl = yes then do i = 1 to 16:
                /*tb 13890 02/21/94 dww; File name not specified causing
                     error */
                if notes.noteln[i] ne "" then do:
                    display notes.noteln[i] with frame f-notes.
                    down with frame f-notes.
                end.
            end.
        end.
SX32 */

if g-currproc = "oeepi" and sapb.reportnm begins "oeepinv" and
   v-outputty = "p" then    
  display s-BTastline1
          s-BTremittmsg1
          s-BTremittmsg2 
          s-BTastline2 with frame f-BTremittmsg.    
else
  display s-astline1
          s-remittmsg1
          s-remittmsg2 
          s-astline2 with frame f-remittmsg.

 /*d Print the customer notes  */
 if arsc.notesfl ne "" then
   run notesprt.p(g-cono,"c",string(oeeh.custno),"","oeepi",8,5).
             
    /*d Print the shipto notes  */
   /*tb #5366 05/14/99 lbr; Added shipto notes */
 if avail arss and arss.notesfl ne "" then
   run notesprt.p(g-cono,"cs",string(oeeh.custno),oeeh.shipto,
                        "oeepi",8,5).
                    
   /*d Print the order notes  */
 if oeeh.notesfl ne "" then
   run notesprt.p(g-cono,"o",string(oeeh.orderno),string(oeeh.ordersuf),
                        "oeepi",8,5).
                          
                          

    if oeeh.transtype <> "ra" then do:
        assign v-totqtyshp = 0
               v-oeehrecid = recid(oeeh).
        run oeepi9l.p.                          /* si01 */
    end.
/* SurCharge */

    s-totlineamt = if oeeh.lumpbillfl then
                      string((oeeh.lumpbillamt +
                             oeeh.totdatccost)
                            * (if oeeh.transtype = "rm" then -1 else 1) *
                              (if (substr(oeeh.user5,1,1) = "c"
                              and oeeh.user6 <> 0) or
                               (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                                 then oeeh.user6 else 1)
                            ,"zzzzzzzz9.99-")
                          else if oeeh.transtype = "rm" then
                               string(((oeeh.totlineamt * -1) +
                               oeeh.totdatccost)
                              * (if oeeh.transtype = "rm" then -1 else 1) *
                                (if (substr(oeeh.user5,1,1) = "c"
                                 and oeeh.user6 <> 0) or
                                  (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                                  then oeeh.user6 else 1)                                                          ,"zzzzzzzz9.99-")
                          else string((v-totlineamt +
                               oeeh.totdatccost)
                              * (if oeeh.transtype = "rm" then -1 else 1) *
                                (if (substr(oeeh.user5,1,1) = "c"
                                 and oeeh.user6 <> 0) or
                                  (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                                  then oeeh.user6 else 1)
                          ,"zzzzzzzz9.99-").

/* SurCharge */



    /*d******** Print the order totals ********************/
    s-totqtyshp = if substring (string(v-totqtyshp,"999999999.99-"),11,2) = "00"
                   then string(v-totqtyshp,"zzzzzzzz9-")
                   else string(v-totqtyshp,"zzzzzzzz9.99-").
    if oeeh.transtype <> "ra" then do:
        clear frame f-tot1.
        /*si05 begin*/
        if ((substring(oeeh.user5,1,1) = "c") or
         (oeeh.currencyty = "lb" and oeeh.user6 <> 0)) and
          not oeeh.lumpbillfl then
            display v-linecnt       s-lit40a    s-lit40c
                    s-totqtyshp     s-lit40d    s-cantot @ s-totlineamt 
                    with frame f-tot1.
        else /*si05 end*/
            display v-linecnt       s-lit40a    s-lit40c
                    s-totqtyshp     s-lit40d    s-totlineamt 
                    with frame f-tot1.
        if ((substring(oeeh.user5,1,1) = "c") or     
         (oeeh.currencyty = "lb" and oeeh.user6 <> 0)) and
          oeeh.lumpbillfl then
          assign s-cantot = dec(s-totlineamt).
        
        if oeeh.wodiscamt <> 0 then do:
            clear frame f-tot2.
            assign s-title2 = s-lit41a
                   s-amt2   = oeeh.wodiscamt * 
                   (if oeeh.transtype = "rm" then -1 else 1) *
                   /*si05 added "if substr..."*/
                   (if (substr(oeeh.user5,1,1) = "c"
                       and oeeh.user6 <> 0) or
                     (oeeh.currencyty = "lb" and oeeh.user6 <> 0)

                      then oeeh.user6
                      else 1)
                    s-cantot = s-cantot + s-amt2. /*si05*/   
            display s-title2    s-amt2      with frame f-tot2.
            down with frame f-tot2.
        end.
        if oeeh.specdiscamt <> 0 then do:
            clear frame f-tot2.
            assign s-title2 = s-lit42a
                   s-amt2   = oeeh.specdiscamt * (if oeeh.transtype = "rm" then
                                                  -1 else 1) *
                   /*si05 added "if substr..."*/
                   (if (substr(oeeh.user5,1,1) = "c"
                       and oeeh.user6 <> 0) or
                       (oeeh.currencyty = "lb" and oeeh.user6 <> 0)

                      then oeeh.user6
                      else 1)
                    s-cantot = s-cantot + s-amt2. /*si05*/   
                                                  
            display s-title2    s-amt2      with frame f-tot2.
            down with frame f-tot2.
        end.
        if oeeh.totcorechg <> 0 then do:
            clear frame f-tot2.
            assign s-title2 = s-lit43a
                   s-amt2   = oeeh.totcorechg * (if oeeh.transtype = "rm" then
                                                 -1 else 1) *
                   /*si05 added "if substr..."*/
                   (if (substr(oeeh.user5,1,1) = "c"
                       and oeeh.user6 <> 0) or
                       (oeeh.currencyty = "lb" and oeeh.user6 <> 0)

                      then oeeh.user6
                      else 1)
                    s-cantot = s-cantot + s-amt2. /*si05*/   
                                                 
            display s-title2    s-amt2      with frame f-tot2.
            down with frame f-tot2.
        end.
        if oeeh.totrestkamt <> 0 then do:
            clear frame f-tot2.
            assign s-title2 = s-lit53a
                   s-amt2   = oeeh.totrestkamt
                              * (if oeeh.transtype = "rm" then -1 else 1) *
                   /*si05 added "if substr..." */
                   (if (substr(oeeh.user5,1,1) = "c"
                       and oeeh.user6 <> 0) or
                       (oeeh.currencyty = "lb" and oeeh.user6 <> 0)

                      then oeeh.user6
                      else 1)
                    s-cantot = s-cantot + s-amt2. /*si05*/   
                              
            display s-title2    s-amt2      with frame f-tot2.
            down with frame f-tot2.
        end. 

        /*si03 begin*/
/*        if oeeh.addonnet[1] <> 0 then do:
            clear frame f-tot2.
            assign s-title2   = s-addondesc[1]
                   s-amt2     = oeeh.addonnet[1] * (if oeeh.transty = "RM" then
                                -1 else 1).
            display s-title2
                    s-amt2
            with frame f-tot2.
            down with frame f-tot2.
        end. */
        {w-smsn.i oeeh.slsrepout "no-lock"}
        assign x-combineaddons = true.
        assign s-amt2 = 0.
        if avail smsn and x-combineaddons = true /* smsn.mgr begins "E"  */ then
          do:
          /** das - sx5.5 - addon logic */
          for each addon where addon.cono = g-cono and
                               addon.ordertype = "oe" and
                               addon.orderno = oeeh.orderno and
                               addon.ordersuf = oeeh.ordersuf
                               no-lock:
            if addon.addonno <> 4 and   
               addon.addonno <> 9 and
/* TAH 001 */
               addon.addonno <> 11 and
/* TAH 001 */
               addon.addonno <> 12 and
               addon.addonno <> 13 then
              assign s-amt2 = s-amt2 + addon.addonnet.
          end. /* each addon */
          if s-amt2 ne 0 then do:   
            clear frame f-tot2.
            assign s-title2 = s-frthandle.
            s-amt2 = s-amt2 * (if oeeh.transtype = "rm" then -1 else 1).
            s-amt2 = s-amt2 * (if (substr(oeeh.user5,1,1) = "c"
                                  and oeeh.user6 <> 0) or
                                 (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                                   then oeeh.user6 else 1).
            s-cantot = s-cantot + s-amt2.  /*si05*/   
            display s-title2
                    s-amt2
            with frame f-tot2.
            down with frame f-tot2.
          end.  
          else
            do:
            for each addon where 
                     addon.cono = g-cono and
                     addon.ordertype = "oe" and
                     addon.orderno = oeeh.orderno and
                     addon.ordersuf = oeeh.ordersuf and
                     addon.addonno = 9
                     no-lock:
              assign s-amt2 = s-amt2 + addon.addonnet.
              find sastn where sastn.cono = g-cono and
                               sastn.codeiden = "a" and
                               sastn.codeval = addon.addonno.
              if avail sastn then
                assign s-title2 = sastn.descrip.
            end. /* each addon */
            if s-amt2 ne 0 then 
              do:
              clear frame f-tot2.
              assign s-amt2 = s-amt2 * (if oeeh.transty = "RM" then -1 else 1).
                     s-amt2 = s-amt2 * (if (substr(oeeh.user5,1,1) = "c" and   
                                          oeeh.user6 <> 0) or
                              (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then
                                       oeeh.user6 else 1).
                     s-cantot = s-cantot + s-amt2.  /*si05*/   
              display s-title2
                      s-amt2
              with frame f-tot2.
              down with frame f-tot2.
            end.
          end.
         end.
      else
        do:
        for each addon where addon.cono = g-cono and
                             addon.ordertype = "oe" and
                             addon.orderno = oeeh.orderno and
                             addon.ordersuf = oeeh.ordersuf
                             no-lock:
          if addon.addonno <> 4 and
             addon.addonno <> 9 and
/* TAH 001 */
             addon.addonno <> 11 and 
/* TAH 001 */
             addon.addonno <> 12 and
             addon.addonno <> 13 then
            assign s-amt2 = s-amt2 + addon.addonnet.
        end. /* each addon */
        if s-amt2 ne 0 then do:   
           clear frame f-tot2.
           assign s-title2 = s-frthandle.
           s-amt2 = s-amt2 * (if oeeh.transtype = "rm" then -1 else 1).
           s-amt2 = s-amt2 * (if (substr(oeeh.user5,1,1) = "c"
                                 and oeeh.user6 <> 0) or
                              (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then
                                 oeeh.user6 else 1).
           s-cantot = s-cantot + s-amt2.  /*si05*/   
           display s-title2
                   s-amt2
            with frame f-tot2.
           down with frame f-tot2.
        end.
      end. /* else */
/* Paul Soldman fuel surcharge  */            
        assign s-amt2 = 0.    
      if avail smsn and x-combineaddons = true /* smsn.mgr begins "e" */ then
        assign s-amt2 = 0.
      else  
        do:
        for each addon where addon.cono = g-cono and
                             addon.ordertype = "oe" and
                             addon.orderno = oeeh.orderno and
                             addon.ordersuf = oeeh.ordersuf and
                             addon.addonno = 9
                             no-lock:
          assign s-amt2 = s-amt2 + addon.addonnet.
          find sastn where sastn.cono = g-cono and
                           sastn.codeiden = "a" and
                           sastn.codeval = addon.addonno.
          if avail sastn then
            assign s-title2 = sastn.descrip.
        end. /* each addon */
        if s-amt2 ne 0 then do:
            clear frame f-tot2.
            assign s-amt2 = s-amt2 * (if oeeh.transty = "RM" then -1 else 1).
            s-amt2 = s-amt2 * (if (substr(oeeh.user5,1,1) = "c"
                                  and oeeh.user6 <> 0) or
                              (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then
                               oeeh.user6 else 1).
            s-cantot = s-cantot + s-amt2.  /*si05*/   
            display s-title2
                    s-amt2
            with frame f-tot2.
            down with frame f-tot2.
            end.
        end.    
/* Paul Soldman fuel surcharge  */            
            
        assign s-amt2 = 0.
        for each addon where addon.cono = g-cono and
                             addon.ordertype = "oe" and
                             addon.orderno = oeeh.orderno and
                             addon.ordersuf = oeeh.ordersuf and
                             addon.addonno = 4
                             no-lock:
          assign s-amt2 = s-amt2 + addon.addonnet.
          find sastn where sastn.cono = g-cono and
                           sastn.codeiden = "a" and
                           sastn.codeval = addon.addonno.
          if avail sastn then
            assign s-title2 = sastn.descrip.
        end. /* each addon */
        if s-amt2 ne 0 then do:
            clear frame f-tot2.
            assign s-amt2 = s-amt2 * (if oeeh.transty = "RM" then -1 else 1).
            s-amt2 = s-amt2 * (if (substr(oeeh.user5,1,1) = "c"
                                  and oeeh.user6 <> 0) or
                             (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then
                              oeeh.user6 else 1).
            s-cantot = s-cantot + s-amt2.  /*si05*/   
            display s-title2
                    s-amt2
            with frame f-tot2.
            down with frame f-tot2.
            end.
        /*si03 end*/

/* Tah 001 */

        assign s-amt2 = 0.
        for each addon where addon.cono = g-cono and
                             addon.ordertype = "oe" and
                             addon.orderno = oeeh.orderno and
                             addon.ordersuf = oeeh.ordersuf and
                             addon.addonno = 11
                             no-lock:
          assign s-amt2 = s-amt2 + addon.addonnet.
          find sastn where sastn.cono = g-cono and
                           sastn.codeiden = "a" and
                           sastn.codeval = addon.addonno.
          if avail sastn then
            assign s-title2 = sastn.descrip.
        end. /* each addon */
        if s-amt2 ne 0 then do:
            clear frame f-tot2.
            assign s-amt2 = s-amt2 * (if oeeh.transty = "RM" then -1 else 1).
            s-amt2 = s-amt2 * (if (substr(oeeh.user5,1,1) = "c"
                                  and oeeh.user6 <> 0) or
                             (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then
                              oeeh.user6 else 1).
            s-cantot = s-cantot + s-amt2.  /*si05*/
            display s-title2
                    s-amt2
            with frame f-tot2.
            down with frame f-tot2.
         end.
        
        /*tb 8561 11/11/92 mms */
        if oeeh.totdatccost <> 0 and v-icdatclabel <> "" then do:
            clear frame f-tot2.
/* TAH 001 */

 /* SurCharge
            assign s-title2 = s-lit44a
                   s-amt2   = oeeh.totdatccost
                              * (if oeeh.transtype = "rm" then -1 else 1) *
                                (if (substr(oeeh.user5,1,1) = "c"
                                 and oeeh.user6 <> 0) or
                              (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then 
                               oeeh.user6 else 1) 
                   s-cantot = s-cantot + s-amt2.  /*si05*/   
             display s-title2    s-amt2      with frame f-tot2.
            down with frame f-tot2.
 */
        end.
/*ca*/  if g-country = "ca" then do:

/* HST */ if can-do("ON,NS,NB,NL",v-statecan) then do:
/* HST */   if oeeh.taxamt[4] <> 0 or
/* HST */      oeeh.taxamt[1] <> 0 then do:     /* print Hst **** tb#9041 */
/* HST */     clear frame f-tot2.
/* HST */     assign s-title2 = s-lit51a
/* HST */            s-amt2 = (oeeh.taxamt[4] + oeeh.taxamt[1])  /* tb#9041*/
/* HST */               * (if oeeh.transtype = "rm" then -1 else 1)
/* HST */               * (if substr(oeeh.user5,1,1) = "c"
/* HST */                    and oeeh.user6 <> 0 then oeeh.user6 else 1)
/* HST */            s-cantot = s-cantot + s-amt2.  /*si05*/   
/* HST */                                  
/* HST */     display s-title2    s-amt2      with frame f-tot2.
/* HST */     down with frame f-tot2.
/* HST */   end.
          end.
/* HST */ else 
/*ca*/      if oeeh.taxamt[4] <> 0 then do:     /* print gst **** tb#9041 */
                clear frame f-tot2.
                assign s-title2 = s-lit51a
                       s-amt2 = oeeh.taxamt[4]               /* tb#9041 */
                                * (if oeeh.transtype = "rm" then -1 else 1)
                                * (if substr(oeeh.user5,1,1) = "c"
                                   and oeeh.user6 <> 0 then oeeh.user6 else 1)
                       s-cantot = s-cantot + s-amt2.  /*si05*/   
                                  
                display s-title2    s-amt2      with frame f-tot2.
                down with frame f-tot2.
            end.
            if oeeh.taxamt[1] <> 0 then do:     /* print pst **** tb#9041 */
                clear frame f-tot2.
                assign s-title2 = s-lit52a
                       s-amt2 = oeeh.taxamt[1]               /* tb#9041 */
/*ca*/                          * (if oeeh.transtype = "rm" then -1 else 1)
                                * (if substr(oeeh.user5,1,1) = "c"
                                  and oeeh.user6 <> 0 then oeeh.user6 else 1)
                       s-cantot = s-cantot + s-amt2.  /*si05*/   

/*ca*/          display s-title2    s-amt2      with frame f-tot2.
/*ca*/          down with frame f-tot2.
/*ca*/      end.
/*ca*/  end.
        else if s-taxes <> 0 then do:
            clear frame f-tot2.
            assign s-title2 = s-lit45a
                   s-amt2   = s-taxes
                              * (if oeeh.transtype = "rm" then -1 else 1)
                              /*si05 added substr..."*/
                              * (if (substr(oeeh.user5,1,1) = "c"
                                  and oeeh.user6 <> 0) or
                                 (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                                  then oeeh.user6 else 1)
            s-cantax1 = if {zzcandiv.zvl w-div} then
                              oeeh.taxamt[4] *
                               (if (oeeh.user6 <> 0 and
                                    substr(oeeh.user5,1,1) = "c") or
                                   (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                                     then 
                                   oeeh.user6 else 1)
                        else 0       
            s-cantax2 = if {zzcandiv.zvl w-div} then 
                              oeeh.taxamt[1] *
                              (if (oeeh.user6 <> 0 and
                                  substr(oeeh.user5,1,1) = "c") or
                               (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                                then 
                                   oeeh.user6 else 1)
                        else 0              
            s-canlit = 
/* HST */               if can-do("ON,NS,NB,NL",v-statecan) and
/* HST */                 {zzcandiv.zvl w-div} then
/* HST */                 "CANADIAN H.S.T #102671252RT:"            
/* HST */              /*   string(s-cantax1 + s-cantax2,"zzzzz9.99-") */
/* HST */               else   
                        if {zzcandiv.zvl w-div} then
                       /*     "CANADIAN TAX AMOUNT: G.S.T.:" +   */
                             "CANADIAN G.S.T. #102671252RT:" + 
                             string(s-cantax1,"zzzzz9.99-") +
                             "  P.S.T.:" +
                             string(s-cantax2,"zzzzz9.99-")
                         else ""
            /* si07 end*/ 
            s-cantot = s-cantot + s-amt2.  /*si05*/
            display s-title2    s-amt2      
                    s-canlit    /*si06*/
            with frame f-tot2.
            down with frame f-tot2.
        end.
        if s-dwnpmtamt <> 0 then do:
            clear frame f-tot2.
            assign s-title2 = if oeeh.transtype = "cs" then s-lit46b
                              else s-lit46a
                   s-amt2   = s-dwnpmtamt
                              * (if oeeh.transtype = "rm" then -1 else 1)
                              * (if (substr(oeeh.user5,1,1) = "c" 
                                 and oeeh.user6 <> 0) or
                                  (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                                   then oeeh.user6
                                 else 1)
                   s-cantot = s-cantot - s-amt2.   /*si05*/
            display s-title2    s-amt2      with frame f-tot2.
            down with frame f-tot2.
        end.
    end.
    clear frame f-tot3.
    /*tb 5413 06/20/92 mwb; added the writeoffamt to totinvamt */
    assign s-totinvamt = if oeeh.transtype = "ra" then oeeh.tendamt
                         else (oeeh.totinvamt - oeeh.tendamt - oeeh.writeoffamt)
                              * (if oeeh.transtype = "rm" then -1 else 1).
           s-invtitle  = if oeeh.transtype = "ra" then s-lit47b else s-lit47a.
           
    /*si05 added s-currencyty*/
    s-currencyty = if {zzcandiv.zvl w-div} or
                     (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then
                       if substr(oeeh.user5,1,1) = "c" then 
                          "**** INVOICE REFLECTS CANADIAN CURRENCY ***"
                       else
                       if (oeeh.currencyty = "lb" and oeeh.user6 <> 0) then
                         "**** INVOICE REFLECTS ENGLISH POUNDS ***"
                       else
                          "**** INVOICE REFLECTS U.S. CURRENCY ****"
                       else "". 
    s-totinvamt = if (substr(oeeh.user5,1,1) = "c") or
                     (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                     then s-cantot
                  else s-totinvamt. /*si05*/          
    if s-currencyty = " " and oeeh.statecd = "IT" and g-ourproc = "OERD" then
      assign s-currencyty = "                                    *** US $".
    display s-invtitle s-currencyty s-totinvamt      with frame f-tot3.

    /* display of Credit Data */
    {oeepicrd.ldi}
    
    /*** das - sx5.5 - the following is taken care of in oeepicrd.ldi 
    /*tb 6404 04/28/93 rs; Negative addon on RM update incorrect */
    if s-totinvamt < 0 and can-do("rm,cs,so,cr",oeeh.transtype)
    then do:
        clear frame f-tot2.
        display skip with frame f-blank.
        down with frame f-blank.
        display s-lit49a @ s-title2    "" @ s-amt2 with frame f-tot2.
        down with frame f-tot2.
        clear frame f-tot2.
        display s-lit50a @ s-title2    "" @ s-amt2 with frame f-tot2.
        down with frame f-tot2.
    end.
    ***/
    
    /*tb 2821 06/16/92 mkb */
    if oeeh.tottendamt ne 0 then do:
       clear frame f-tot5.
       if oeeh.boexistsfl = no and (oeeh.orderdisp = "j"  or
                                    oeeh.transtype = "bl") then do:
           find first b-oeeh use-index k-oeeh where
               b-oeeh.cono       = oeeh.cono         and
               b-oeeh.orderno    = oeeh.orderno      and
               b-oeeh.ordersuf   > 0 no-lock no-error.
           if avail b-oeeh then display oeeh.tottendamt 
                * (if (substr(oeeh.user5,1,1) = "c" and oeeh.user6 <> 0) or
                      (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                     then oeeh.user6  else 1)
                   with frame f-tot5.
       end.
       else if oeeh.boexists = yes
               then display oeeh.tottendamt with frame f-tot5.
    end.
    /*das-Include Tracking Info on Invoice*/
    if can-find(first oeehp use-index k-oeehp where 
                            oeehp.cono       = g-cono and
                            oeehp.orderty    = "o" and
                            oeehp.orderno    = oeeh.orderno and
                            oeehp.ordersuf   = oeeh.ordersuf and
                            oeehp.trackerno <> " ") then
      do:
      assign s-tracktitle = "Tracking Information:".
      display s-tracktitle with frame f-trackhdr.
      down with frame f-trackhdr.
      for each oeehp use-index k-oeehp where 
               oeehp.cono      = g-cono and
               oeehp.orderty   = "o" and
               oeehp.orderno   = oeeh.orderno and
               oeehp.ordersuf  = oeeh.ordersuf and
               oeehp.trackerno <> " "
               no-lock:
        assign s-tracknbr = oeehp.trackerno.
        display s-tracknbr with frame f-trackdtl.
        down with frame f-trackdtl.
      end.
    end.
    
    assign s-bmsga = if g-cono = 1 then
                      "Thank you for your business. " +
                      "Please visit our website at www.sun-source.com. " +
                      "For on-line ordering visit www.sunsourceconnect.com"
                     else
                     if avail arsc and substr(arsc.user19,1,4) = "PGON" then
                      "Thank you for your business. " +
                      "Please visit our website at www.paragontech.com. " +
                      "For any repair need, call day or night (800)229-5350"
                     else
                      "Thank you for your business. " +
                      "Please visit us at www.perfectionservo.com. " +    
                      "For any repair need, call us at (888)628-2800".
    assign s-bmsgaz = s-bmsga.
    assign s-lit48a  = "Last Page"
           s-lit48az = "Last Page"
           /*si03 begin*/
           s-lit48b  = if oeeh.termsdiscamt <> 0 then "Cash Discount" else ""
           s-termsdiscamt = if oeeh.termsdiscamt <> 0 then
                            string(dec(oeeh.termsdiscamt) *
                             (if (substr(oeeh.user5,1,1) = "c" 
                                and oeeh.user6 <> 0) or
                               (oeeh.currencyty = "lb" and oeeh.user6 <> 0)
                                 then oeeh.user6
                                else 1),"zzzzzzzz9.99-") else ""
           s-paid       = if oeeh.termsdiscamt <> 0 and oeeh.discdt = ? then
                          s-lit48d else if oeeh.termsdiscamt <> 0 then
                          s-lit48c + " " + string(oeeh.discdt) else "".
           /*si03 end*/
    /* si01; set stub text and info on last page */
    assign
        s-stub1 = "Customer Name :"
        s-stub2 = "Customer No. :"
        s-stub3 = "Invoice Date :"
        s-stub4 = "Invoice No. :"
        s-stub5 = "Invoice Amt. :"
        s-stub6 = "Amt. Remitted :"
        s-stub7 = "Check No. :"
        v-stub1 = arsc.name
        v-stub2 = string(arsc.custno)
        v-stub3 = string(oeeh.invoicedt)
        v-stub4 = string(oeeh.orderno,">>>>>>9") +
                  "-" +
                  string(oeeh.ordersuf,"99")
        v-stub5 = string(s-totinvamt,"zzzzzzzz9.99-").
