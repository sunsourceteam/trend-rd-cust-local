/* f-oeepp1.i */
/* f-oeepp1.i 1.1 01/03/98 */
/* f-oeepp1.i 1.4 09/08/93 */
/*h*****************************************************************************
  INCLUDE      : f-oeepp1.i
  DESCRIPTION  : Forms and form variable definitions for oe & wt pick tickets
  USED ONCE?   : No (wtep1.p oeepp1.p)
  AUTHOR       : rhl
  DATE WRITTEN : 11/04/89
  CHANGES MADE :
    10/14/91 mwb; TB#  4762 Changed page# to fit pre-printed form
    11/11/92 mms; TB#  8669 Rush - Sort rush orders to top
    09/07/93 mms; TB# 12619 Display cod message if wt tied to cod order;
        added s-codmsg not used in oe
    03/20/96 kr;  TB# 12406 Variables defined in form, moved out of form
    06/12/96 jms; TB# 21254 (10A) Develop Value Add Module
        added parameters &section, &shiptonm, &custno, and &custpo
    08/05/96 jms; TB# 21254 (10A) Develop Value Add Module -
        Cleaned-up lines 19&20
    03/09/00 pg; RxServer hooks
    03/29/00 rgm; TB# 3-2  Add RxServer Interface to Standard
    05/08/00 rgm; TB# A9   Fix for RxServer Faxing issue
    05/25/01 rcl; TB# e6873 Increase weight and cubes to 5 decimals.
*******************************************************************************/

/*tb 12406 03/26/96 kr; Variables defined in form f-oeepp1.i */
def            var s-shiptonm      like {&shiptonm}                no-undo.
def            var s-stagearea     like {&section}.stagearea       no-undo.

/*tb 3-2 03/29/00 rgm; Rxserver interface display vars for rxserver */
form header
    skip(1)

    s-lit1a                           at   2
    s-doctype                         at  12
    s-reprint                         at  53
    s-lit1b                           at  93
    s-rush                            at  12
    s-codmsg                          at  25
    s-filltype                        at  53
    sasc.upcvno                       at  95
    {&head}.{&pref}no                 at 106
    "-"                               at 113
    {&head}.{&pref}suf                at 114
    s-seqno                           at 125
    s-lit3a                           at   4

    {&com}
    {&custno}                     at  12
    /{&com}*  */

    s-orderdisp                       at  53
    s-lit3b                           at  94
    {&orderdt}                        at  94

    {&com}
    {&custpo}                     at 104
    /{&com}*  */

    page-num - v-pageno format "zzz9" at 129
    s-pricedesc                       at   2
    s-lit6a                           at   2
    s-billname                        at  12
    s-lit6b                           at  48
    sasc.conm                         at  68
    s-billaddr1                       at  12
    sasc.addr[1]                      at  68
    s-billaddr2                       at  12
    sasc.addr[2]                      at  68
    s-billcity                        at  12
    s-corrcity                        at  68

    /*tb A9 05/08/00 rgm; Rxserver interface change when appropriate */
    &IF DEFINED(rxserver) = 0 &THEN
        skip(1)
    &ELSE
        s-rxtext1                     at   1
    &ENDIF

    s-lit11a                          at   2
    s-shiptonm                        at  12
    s-lit11b                          at  50
    s-lit11c                          at  81
    s-shiptoaddr[1]                   at  12
    {&section}.shipinstr              at  50
    s-stagearea                       at  81
    s-shiptoaddr[2]                   at  12
    s-lit12a                          at  50
    s-lit12b                          at  81
    s-lit12e                          at 100
    s-lit12c                          at 110
    s-lit12d                          at 120
    s-shipcity                        at  12
    s-whsedesc                        at  50
    s-shipvia                         at  81
    s-cod                             at  94
    {&section}.reqshipdt              at 100
    {&pickdt}                         at 110
    s-terms                           at 120

    &IF DEFINED(rxserver) = 0 &THEN
        skip(1)
        s-lit14a                          at   1
        s-lit14b                          at  79
        s-lit15a                          at   1
        s-lit15b                          at  79
        s-lit16a                          at   1
        s-lit16b                          at  79
    &ELSE
        s-rxhead1                         at   1          /*line 16*/
        s-rxhead2                         at   1          /*line 17*/
        s-rxhead3                         at   1          /*line 18*/
        s-rxhead4                         at   1          /*line 19*/
    &ENDIF

with frame f-head no-box no-labels
           &IF DEFINED(rxserv_width) = 0 &THEN
               width 132
           &ELSE
               width {&rxserv_width}
           &ENDIF
           page-top.


/*tb 3-2 03/29/00 rgm; Rxserver interface display vars for rxserver */
form header
    &IF DEFINED(rxserver) = 0 &THEN
        skip(1)
    &ELSE
        s-rxfoot1                     at   1
    &ENDIF

    s-linecntx                        at   1
    s-lit40a                          at   5
    s-lit40b                          at  20
    v-noprintx                        at  47
    s-lit40c                          at  58
    s-totqtyshp                       at  77
    s-lit40d                          at  91
    s-totlineamt                      at 115
    s-lit41a                          at   1
    s-lit41b                          at  79
    s-totcubes                        at  42
    s-totweight                       at  59
    s-lit42a                          at   1
    s-lit42b                          at  95
with frame f-tot no-box no-labels
           &IF DEFINED(rxserv_width) = 0 &THEN
               width 132
           &ELSE
               width {&rxserv_width}
           &ENDIF
           page-bottom.
