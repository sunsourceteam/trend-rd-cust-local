/* SX 55
   d-oeir.i 1.1 01/03/98 */
/* d-oeir.i 1.2 1/11/94 */
/*h*****************************************************************************
  INCLUDE      : d-oeir.i
  DESCRIPTION  : display line for oeir
  USED ONCE?   : yes
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    12/20/91 pap; TB# 5268  JIT/Line Due - change reqshipdt with promisedt
    12/02/93 mwb; TB# 13455 Ddded lookup of oeeh and arsc for display; x-xxex.i
    01/11/94 mwb; TB# 14107 Cross lock for oeeh - added do for oeeh,arsc
    07/05/94 mms; TB# 15913 Added s-dolinety indicator assign and display
*******************************************************************************/

do for oeeh, arsc:

 {w-oeeh.i oeehc.orderno oeehc.ordersuf no-lock}
 {w-arsc.i oeehc.custno no-lock}

/*d Assign and display detail lines */
 if avail oeeh and avail arsc and avail oeehc then do:
   assign s-order          = string(oeeh.orderno,"zzzzzzz9") + "-" +
            string(oeeh.ordersuf,"99")
          s-fpty           = if oeeh.fpcustno ne {c-empty.i} then "f"
                             else if oeeh.updtype = "s" then "s"
                             else ""
           s-stagecd        = v-stage[oeeh.stagecd + 1]
           s-dolinety       = if oeeh.transtype = "do" or
                                 oeeh.nodolines ne 0 then "d"
                              else "".

   assign x-focuscust = false.
   run CustFocusCheck.p(g-cono, 
                        oeeh.orderno, 
                        oeeh.ordersuf,
                        0,
                        "",
                        input-output x-focuscust).
   if x-focuscust then
     do:
     oeeh.custno:dcolor in frame f-oeir = 2.
     arsc.name:dcolor in frame f-oeir = 2.
     end.
   else
     do:
     oeeh.custno:dcolor in frame f-oeir = 0.
     arsc.name:dcolor in frame f-oeir = 0.
     end.
   
   display s-order
           oeeh.notesfl
           s-fpty
           oeeh.approvty
           oeeh.transtype
           s-stagecd
           oeeh.promisedt
           s-dolinety
           oeeh.totinvamt
           oeeh.takenby
           oeeh.whse
           oeehc.creditmgr
           oeehc.priority
           oeeh.custno
           arsc.notesfl
           arsc.name with frame f-oeir.

  end. /* if avail oeeh, oeehc, arsc */

end. /* do for oeeh, arsc */


