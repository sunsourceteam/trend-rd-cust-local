/* p-oeeaii.i 1.1 01/03/98 */
/* p-oeeaii.i 1.1 01/03/98 */
/*h*****************************************************************************  INCLUDE      : p-oeepaii.i
  DESCRIPTION  : Fields used in the "Total" record in the edi 810, 843, and 855
  USED ONCE?   : no
  AUTHOR       : jlc
  DATE WRITTEN : 02/23/94
  CHANGES MADE :
*******************************************************************************/
"Total " +
    caps(s-linecnt) +
    caps(s-totinvamt) +
    caps(s-totqtyshp) +
    caps(s-totqtyord)