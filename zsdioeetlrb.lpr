/* zsdioeetlrb.lpr 1.9 10/05/98 */
/*h****************************************************************************
INCLUDE : zsdioeetlrb.lpr
DESCRIPTION : Cust and Vend Rebates for Order Entry - Processing Include
USED ONCE? : no
AUTHOR : mtt
DATE WRITTEN : 02/11/98
CHANGES MADE :
02/11/98 mtt; TB# 24049 PV: Common code use for GUI interface
03/06/98 gp; TB# 24339 Rebate type 4 not calc when ARP is whse
04/02/98 gp; TB# 24729 Rebate amount incorrect with UOM
04/13/98 sbr; TB# 24443 Catalog rebate not calculating correctly
05/21/98 bp ; TB# 24948 Add user includes &user_defvar to oeetlrb.p and
&aft_constvarasgn to oeetlrb.lpr
08/17/98 gp; TB# 25204 Nonstock with spec rec # of zero.
Add o-icspecrecno.
09/22/98 gp; TB# 25319 Rebate for special price/cost catalog
09/25/98 gp; TB# 25319 Rebate for special price/cost catalog
09/28/98 gp; TB# 25374 Rebate amount for catalog products
01/22/99 bp ; TB# 25775 Add the following user includes: &aft_recfind,
&aft_rebfind and &bef_icssrestore
09/28/99 bp ; TB# 26575 Add logic for the SPC Enhancement. Here, want
the PDER records to reflect the SPC data for the OEEL/OEELK.
10/16/99 bp ; TB# 26575 Change logic for SPC Enhancement to use "r-"
variables that are set before the call to this procedure. Make
"o-" variables local and remove the "o-vc" variables.
01/19/00 lbr; TB# 27047 No rebate if copied and use actual cost
02/29/00 lbr; TB# e3692 No rebate if copied and use actual cost GUI.
05/22/00 lbr; TB# e5136 No rebate if convert and use actual cost.
10/06/00 lbr; TB# e5870 Customer rebate incorrect with UOM
07/10/01 sah; TB# e8223 Calculate Rebates on VA Orders and Prebuilt Kit
Components, when tied to an OE order. Moved vars to oeetlrb.lva.
******************************************************************************/
     
do for arsc, oeelk, vaesl, vaspsl, icsp, icsw, icsc, pder:

  {w-arsc.i g-custno no-lock}

  if g-ourproc = "vaerb":u and v-idoeelk ne ? then
    find vaesl where recid(vaesl) = v-idoeelk no-lock no-error.

  else
  if g-ourproc = "vasrb":u and v-idoeelk ne ? then
    find vaspsl where recid(vaspsl) = v-idoeelk no-lock no-error.

/*tb 20179 01/04/95 gdf; "no-lock no-error" was added to following
  three finds. */
  else
  if v-idoeelk ne ? then
  find oeelk where recid(oeelk) = v-idoeelk no-lock no-error.

/*tb 7241 (5.1) 02/24/97 jkp; Changed if v-idicsp to a do loop. */
/*tb 23304 06/18/97 mms; Removed icss finds as special price costing
  variables are now shared. */
  if v-idicsp ne ? then
    do:

    find icsp where recid(icsp) = v-idicsp no-lock no-error.
    end. /* if v-idicsp ne ? */

  if v-idicsw ne ? then
    find icsw where recid(icsw) = v-idicsw no-lock no-error.
  {&findicsw}

  if avail vaesl and vaesl.nonstockty = "n":u then
    {w-icsc.i  vaesl.shipprod no-lock}

  else
  if avail vaspsl and vaspsl.nonstockty = "n":u then
    {w-icsc.i vaspsl.compprod no-lock}

  else
  if avail oeelk and oeelk.specnstype = "n" then
    {w-icsc.i  oeelk.shipprod no-lock}

  else
  if s-specnstype = "n" then
    {w-icsc.i  s-prod no-lock}

  if avail vaesl or avail vaspsl then do:

    find last pder use-index k-pder where
                  pder.cono = g-cono + 500 and
                  pder.orderno = g-orderno and
                  pder.ordersuf = g-ordersuf and
                  pder.lineno = s-lineno and
                  pder.rebatecd <> "p":u
                  no-lock no-error.

    v-seqno = if avail pder then pder.seqno + 1
               else 1.

    end. /* avail vaesl or avail vaspsl */

/*d Assign shared variables that will be constant in rebfind.p */
/*e When oeetlrb.p is run for regular kit components, the oeelk.arpvendno
  has not been assigned so the icsw.arpvendno must be used. When
  oeetlrb.p is run for nonstock kit components, the oeelk.arpvendno
  has been assigned from the nonstock entry screen so this field
  is used to assign v-cvendno. For non-bod-kit items, the s-vendno
  is appropriate if it is not 0. In the case of copies s-vendno
  may be zero, so used the shared buffer b-icsw.arpvendno. */
/*tb 22085 12/03/96 tdd; Whenever pull information from ICSP or ICSW,
  make sure that we're not dealing with a Nonstock product. Also needed
  to add the assignment of v-specnstype */
/*tb 23304 06/18/97 mms; Removed assignments of special price costing
  information as it is not carried shared or pulled from the icsc record
  for nonstocks. */
/*tb 23765 09/26/97 gp; Spec price/cost ignored for catalog rebate */
/*tb 24158 12/23/97 kjb; Vendor rebate not calculated on non-stock; added
  v-prodtype to v-cpricetype assignment. v-prodtype is set by the user
  in the nonstock entry screen. */
/*tb 24339 03/06/98 gp; When ARP is whse and vendor/prodline information
  are available in ICSW cannot use s-prodline because it is blank */
/*tb 25204 08/17/98 gp; Save icspecrecno value */


  assign
    v-vendrebord = 0
    v-vendrebamt = 0
    v-custrebamt = 0
    v-rebamt = 0
    v-cvendno = if avail icsw and v-cspecnstype <> "n"
                   then icsw.arpvendno
                else if avail oeelk then oeelk.arpvendno
                else if avail vaesl and vaesl.arpvendno <> 0
                     then vaesl.arpvendno
                else if avail vaspsl and vaspsl.arpvendno <> 0
                     then vaspsl.arpvendno
                else if s-vvendno ne 0 and s-ordertype = "p"
                     then s-vvendno
                else if s-vendno ne 0 then s-vendno
                else if avail b-icsw and v-cspecnstype <> "n"
                     then b-icsw.arpvendno
                else 0
    v-cspecnstype = if avail oeelk then oeelk.specnstype
                    else s-specnstype
    v-specnstype = v-cspecnstype
    v-cprod = if avail oeelk then oeelk.shipprod
              else if avail vaesl then vaesl.shipprod
              else if avail vaspsl then vaspsl.compprod
              else s-prod
    v-cprodrebty = if v-cspecnstype = "n" then
                   if avail icsc then icsc.rebatety
                   else ""
                   else if avail icsw then icsw.rebatety
                   else if avail b-icsw then b-icsw.rebatety
                   else ""
    v-cprodline = if avail icsw and v-cspecnstype <> "n"
                    then icsw.prodline
                  else if avail vaesl and vaesl.arpprodline <> ""
                     then vaesl.arpprodline
                  else if avail vaspsl and vaspsl.arpprodline <> ""
                     then vaspsl.arpprodline
                  else if avail b-icsw and b-icsw.arptype = "w" and
                     (v-cvendno <> 0 and v-cvendno = b-icsw.arpvendno)
                     then b-icsw.prodline
                  else s-prodline
    v-cpricetype = if avail oeelk then oeelk.pricetype
                   else if v-cspecnstype = "n" then
                      if avail icsc then icsc.pricetype
                      else v-prodtype
                   else if avail icsw then icsw.pricetype
                   else if avail b-icsw then b-icsw.pricetype
                   else ""
    v-cprodcat = if avail oeelk then oeelk.prodcat
                   else s-prodcat
    v-crebsubty = if v-cspecnstype = "n" then
                    if avail icsc then icsc.rebsubty
                    else ""
                  else if avail icsw then icsw.rebsubty
                  else if avail b-icsw then b-icsw.rebsubty
                  else ""
    v-ccustno = g-custno
    v-ccustrebty = if avail arsc then arsc.rebatety else ""
                     v-cshipto = g-shipto
    v-cwhse = g-whse
    v-cunit = if avail oeelk then oeelk.unit
              else if avail vaesl then vaesl.unit
              else if avail vaspsl then vaspsl.unit
              else s-unit
    v-cqtyneeded = if avail oeelk then oeelk.qtyneeded
                   else if avail vaesl or avail vaspsl
                     then v-vaqtyneeded
                   else 0
    v-cqtyord = s-qtyord
    v-qtyship = if avail oeelk and substr(g-ourproc,1,2) = "kp":u
                   then s-qtyship * v-conv * oeelk.qtyneeded
                else if avail oeelk then oeelk.qtyship
                else if avail vaesl or avail vaspsl
                   then s-qtyship * v-conv * v-cqtyneeded
                else s-qtyship
    v-cdisctype = s-disctype
    v-cdiscpct = if avail oeelk then 0 else s-discamt
    v-cdiscamt = if avail oeelk then 0 else s-discamt
    o-csunperstk = v-csunperstk
    o-specconv = v-specconv
    o-prccostper = v-prccostper
    o-speccostty = v-speccostty
    o-icspecrecno = v-icspecrecno.
 

/*tb 23765 09/26/97 gp; Spec price/cost ignored for catalog rebate */
/*tb 25319 09/22/98 gp; Since the icsc.icspecrecno is always 0,
  reassign v-icspecrecno */
/*tb 25319 09/25/98 gp; Add icsc check */
/*tb 26575 09/28/99 bp ; Add additional variables for SPC Enhancement*/
  if v-cspecnstype = "n" and avail icsc and icsc.csunperstk ne 0 then
    assign
      {speccost.gas  &file = "icsc"
                     &com = "/*"}
      v-icspecrecno = o-icspecrecno.

/*tb 22109 11/06/96 tdd; Allow NS rebating off ICSC record */
/*tb 24729 04/02/98 gp; Change v-actcost to represent the cost
  per stocking unit */
/*tb 24443 04/13/98 sbr; Removed icsc.csunperstk from the v-listprice and
  v-baseprice calculations for catalog products. */
/*tb 25374 09/28/98 gp; Add special costing conversion to list and
  base price */
/*tb 27047 01/19/00 lbr; Set s-prodcost for copying quotes with rebates
  added -- repeat while program-name */
/*d if this is comming from oeetql-copy orders, then we need to set
  s-prodcost from the oeel.prodcost */
/*d to find out if this is comming from oeetq.p we need loop thru
  the program name function and look for oeetql.p */
/*tb e3692 02/29/00 lbr; Added program name oe/d-oeet-copy.w for GUI */
  repeat while program-name(level) <> ?.
  if program-name(level) = "oeetql.p" or
     program-name(level) = "oeetqll.p" or
     program-name(level) = "oe/d-oeet-copy.w" then do:
    for first oeel fields (prodcost) where
                     oeel.cono = g-cono and
                     oeel.orderno = g-orderno and
                     oeel.ordersuf = g-ordersuf and
                     oeel.lineno = s-lineno
                     use-index k-oeel no-lock:
      s-prodcost = oeel.prodcost.
      end.
    end. /* if program-name */
  level = level + 1.
  end. /* repeat */




/*tb e5870 10/06/00 lbr; Change v-cprice to represent the cost
per stocking unit */

  assign
    v-cnetamt = if avail oeelk and substr(g-ourproc,1,2) <> "kp":u then
                  round (((oeelk.price *
                          (if oeelk.specnstype = "n"
                             or v-speccostty = "" then 1
                           else (v-csunperstk * oeelk.conv))) *
                                 oeelk.qtyship),2)
               else s-netamt
    v-cnetord = s-netamt
    v-orderno = g-orderno
    v-ordersuf = g-ordersuf
    v-lineno = if avail oeelk and substr(g-ourproc,1,2) <> "kp":u
               then oeelk.lineno
               else s-lineno
    v-creturnfl = s-returnfl
    v-slsrepin = s-slsrepin
    v-slsrepout = s-slsrepout
    v-arpvendno = if avail oeelk then oeelk.arpvendno
                  else if avail vaesl then vaesl.arpvendno
                  else if avail vaspsl then vaspsl.arpvendno
                  else if avail icsw and v-cspecnstype <> "n"
                     then icsw.arpvendno
                  else 0
    v-botype = s-botype
    v-oeorderty = if avail oeelk then oeelk.ordertype
                  else s-ordertype
    v-poorderty = "".

  assign
    g-unitconv = if avail oeelk then
                   {unitconv.gas  &decconv = oeelk.conv}
                 else if avail vaesl then
                   {unitconv.gas &decconv = vaesl.unitconv}
                 else if avail vaspsl then
                   {unitconv.gas &decconv = vaspsl.unitconv}
                 else {unitconv.gas &decconv = v-conv}
    v-actcost = (if avail oeelk then oeelk.prodcost
                 else s-prodcost) /
                    (if v-speccostty = "" then g-unitconv
                     else 1)
    v-cprice = (if avail oeelk and substr(g-ourproc,1,2) <> "kp":u
                  then oeelk.price
                else s-price) /
                    (if v-speccostty = "" then g-unitconv
                     else 1)
    v-statustype = "e"
    v-stkqtyship = if avail oeelk and substr(g-ourproc,1,2) <> "kp":u
                     then oeelk.stkqtyship

                   else v-stkqtyship
    v-stkqtyord = if avail oeelk and substr(g-ourproc,1,2) <> "kp":u
                    then oeelk.stkqtyord
                  else v-stkqtyord
    v-seqno = if avail oeelk then oeelk.seqno
              else if avail vaesl or avail vaspsl then v-seqno
              else 0
    v-baseprice = if v-cspecnstype = "n" then
                    if avail icsc then
                      (icsc.baseprice *
                      {speceach.gco  &file = "icsc"})
                    else 0
                  else if avail icsw then icsw.baseprice *
                       ( if v-spcconvertfl = yes then
                         {specprc.gco &curricss = vc-
                                      &histicss = v-}
                        else 1)
                  else if avail b-icsw then b-icsw.baseprice *
                        (if v-spcconvertfl = yes then
                          {specprc.gco &curricss = vc-
                                       &histicss = v-}
                         else 1)
                  else 0
    v-listprice = if v-cspecnstype = "n" then
                    if avail icsc then
                      (icsc.listprice * {speceach.gco 
                                            &file = "icsc"})
                    else 0
                    else if avail icsw then icsw.listprice *
                          (if v-spcconvertfl = yes then
                             {specprc.gco 
                                         &curricss = vc-
                                         &histicss = v-}
                           else 1)
                   else if avail b-icsw then b-icsw.listprice *
                         (if v-spcconvertfl = yes then
                            {specprc.gco 
                                         &curricss = vc-
                                         &histicss = v-}
                          else 1)
                  else 0.

/*tb 26575 09/29/99 bp ; Add SPC Enhancement logic and break assignment
  statement into multiple pieces.*/
  assign
    v-avgcost = if v-cspecnstype = "n" then
                  if avail icsc then
                    (icsc.prodcost * {speceach.gco &file = "icsc"})
                  else 0
                else if avail icsw then icsw.avgcost *
                      (if v-spcconvertfl = yes then
                         {speccost.gco 
                                      &curricss = vc-
                                      &histicss = v-}
                       else 1)
                else if avail b-icsw then b-icsw.avgcost *
                      (if v-spcconvertfl = yes then
                         {speccost.gco 
                                      &curricss = vc-
                                      &histicss = v-}
                       else 1)
                else 0
    v-lastcost = if v-cspecnstype = "n" then
                   if avail icsc then
                     (icsc.prodcost * {speceach.gco  &file = "icsc"})
                    else 0
                 else if avail icsw then icsw.lastcost *
                       (if v-spcconvertfl = yes then
                          {speccost.gco 
                                        &curricss = vc-
                                        &histicss = v-}
                        else 1)
                else if avail b-icsw then b-icsw.lastcost *
                      (if v-spcconvertfl = yes then
                         {speccost.gco 
                                      &curricss = vc-
                                      &histicss = v-}
                       else 1)
                else 0
    v-replcost = if v-cspecnstype = "n" then
                   if avail icsc then
                     (icsc.prodcost * {speceach.gco &file = "icsc"})
                   else 0
                 else if avail icsw then icsw.replcost *
                       (if v-spcconvertfl = yes then
                          {speccost.gco
                                         &curricss = vc-
                                         &histicss = v-}
                        else 1)
                 else if avail b-icsw then b-icsw.replcost *
                       (if v-spcconvertfl = yes then
                          {speccost.gco &curricss = vc-
                                        &histicss = v-}
                        else 1)
                 else 0.

/*tb 26575 09/29/99 bp ; Add SPC Enhancement logic and break assignment
  statement into multiple pieces.*/
  assign
    v-stndcost = if v-cspecnstype = "n" then
                   if avail icsc then
                     (icsc.stndcost * {speceach.gco &file = "icsc"})
                   else 0
                else if avail icsw then icsw.stndcost *
                      (if v-spcconvertfl = yes then
                         {speccost.gco
                                       &curricss = vc-
                                       &histicss = v-}
                       else 1)
                else if avail b-icsw then b-icsw.stndcost *
                      (if v-spcconvertfl = yes then
                        {speccost.gco 
                            &curricss = vc-
                            &histicss = v-}
                       else 1)
               else 0
    v-rebatecost = if v-cspecnstype = "n" then
                     if avail icsc then
                       (icsc.rebatecost *
                         {speceach.gco  &file = "icsc"})
                     else 0
                   else if avail icsw then icsw.rebatecost *
                         (if v-spcconvertfl = yes then
                            {speccost.gco
                                         &curricss = vc-
                                         &histicss = v-}
                          else 1)
                  else if avail b-icsw then b-icsw.rebatecost *
                        (if v-spcconvertfl = yes then
                           {speccost.gco
                                         &curricss = vc-
                                         &histicss = v-}
                         else 1)
                  else 0
    v-lastcostfor = if v-cspecnstype = "n" then
                      if avail icsc then
                        (icsc.prodcost * {speceach.gco  &file = "icsc"})
                      else 0
                    else if avail icsw then icsw.lastcostfor *
                          (if v-spcconvertfl = yes then
                             {speccost.gco 
                                          &curricss = vc-
                                          &histicss = v-}
                           else 1)

                   else if avail b-icsw then b-icsw.lastcostfor *
                         (if v-spcconvertfl = yes then
                            {speccost.gco 
                                          &curricss = vc-
                                          &histicss = v-}
                          else 1)
                   else 0.

  assign
    v-kitprod = if avail oeelk or avail vaesl or avail vaspsl
                  then s-prod
                else ""
    v-kqtyship = if avail oeelk or avail vaesl or avail vaspsl
                   then s-qtyship
                 else 0
    v-kstkqtyship = if avail oeelk and substr(g-ourproc,1,2) <> "kp":u
                      then v-stkqtyship
                    else if avail oeelk or avail vaesl or avail vaspsl
                      then s-qtyship * v-conv
                    else 0
    v-frzrebty = v-oefrzrebty
    v-updatefl = (if g-ourproc begins "oei" then
                    no
                  else  
                    yes).

  {&extraassign}

  end. /* do for arsc, oeelk, vaesl, vaspsl, icsp, icsw, icsc, pder */


/*d Run Rebate calculations for both customer and vendor rebates based on
sales when the rebate calculations have not been frozen. */
do i = 1 to 2:

v-rebatecd = if i = 1 then "c" else "s".

if v-rebatecd = "s" then
  do:
/*d Find appropriate rebate */

  run zsdirebfind.p.
  if v-undofl then return.
  end.
end. /* do i = 1 to 2 */
/*tb 22109 12/05/96 tdd; Initialize special costing variables so not carried
  around to cause other calculation problems */
/*tb 23304 06/18/97 mms; Removed reinitialization as the special price and
  cost variables are carried around shared and were not overwritten above. */
/*tb 23765 09/26/97 gp; Spec price/cost ignored for catalog rebate */
/*tb 25204 08/17/98 gp; Reassign icspecrecno */
/*tb 26575 09/28/99 bp ; Add additional SPC variables. */
  assign
    v-csunperstk = o-csunperstk
    v-specconv = o-specconv
    v-prccostper = o-prccostper
    v-speccostty = o-speccostty
    v-icspecrecno = o-icspecrecno.



    
