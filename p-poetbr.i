/* SX 55
  p-poetbr.i 1.2 10/19/98 */
/*h****************************************************************************
  INCLUDE      : p-poetbr.i
  DESCRIPTION  :
  USED ONCE?   : yes
  AUTHOR       :
  DATE WRITTEN : 11/05/95
  CHANGES MADE :
    11/05/93 mwb; TB# 13520 Carry unitconv to 10 decimals
    07/16/96 kr;  TB# 18307 Don't allow key field changes on BL once a BR has
        been released
    10/05/98 sbr; TB# 
    25360 BR not updating quantity remaining
    01/13/99 mwb; TB# 24049 PV: Common code use for GUI interface
******************************************************************************/

/*d Read the POEL record for the stage 0 BR */
find b-poel use-index k-poel where
     b-poel.cono   = g-cono    and
     b-poel.pono   = poel.pono and
     b-poel.posuf  = 0         and
     b-poel.lineno = poel.lineno
exclusive-lock no-error.

/** SunSource hook - storing poel recid **/
x-recid       = recid(poel).        /* SX 55 */

/*d Set the old variables */
assign
    o-cubes      = poel.cubes
    o-weight     = poel.weight
    o-netamt     = poel.netamt
    o-qtyrcv     = poel.qtyrcv
    o-totqtyrcv  = poel.stkqtyrcv
    o-totqtyord  = poel.stkqtyord
    o-totqtyrcvb = poel.qtyrcv
    o-nosnlots   = poel.nosnlots
    o-qtyord     = poel.qtyord
    v-conv       = {unitconv.gas
                       &decconv = "(b-poel.stkqtyrcv / b-poel.qtyrcv)"}.

/*d Allow operator entry of the quantity to release */
if avail b-poel then do
{&comgui}
on endkey undo, next
/{&comgui}* */
:

    {&comgui}
    update
        poel.qtyord
            validate
                (if poel.qtyord > (b-poel.qtyord - b-poel.qtyrel + o-qtyord)
                    then false else true,
                "Cannot Allocate More Than is Remaining on Blanket (5774)")
    with frame f-poetbl.

    /{&comgui}* */

    {&updateassign}

    /*tb 25360 10/05/98 sbr; Moved assign of poel.qtyrcv = poel.qtyord above
        the call to poel.lfr. */
    poel.qtyrcv   = poel.qtyord.

    /*tb 18307 07/17/96 kr; Don't allow key field changes on BL once a
        BR has been released */
    v-totqtyrel = 0.
    /*d For each poel record find */
    {poel.lfr
        &pono      = "g-pono"
        &posuf     = "0"
        &posuf2    = "99"
        &transtype = "br"
        &prod      = "poel.shipprod"
        &lock      = "no-lock"
        &b         = "b3-"}

    /*d Update the BL PO with the quantity already released */
    /*tb 18307 07/17/96 kr; Don't allow key field changes on BL once a BR
        has been released */
    assign
        b-poel.qtyrel = b-poel.qtyrel + (poel.qtyord - o-qtyord)
        s-qtyremain   = b-poel.qtyord - b-poel.qtyrel.

    {&comgui}
    display s-qtyremain.
    /{&comgui}* */

    /*d Update the POEL record and the header */
    {p-poltot.i}
    {t-all.i poel}
     
end. /* if avail b-poel */


