/******** Custom *************************************************************
   PROCEDURE    : tifmerge
   DESCRIPTION  : merges multiple invoices into one pdf file
   DATE WRITTEN : 01/30/09
   CHANGES MADE :

******************************************************************************/

def        var cMailSubject  as character format "x(40)" no-undo.
def        var cMailMessage  as character format "x(132)" no-undo.
def        var cMailto       as character format "x(40)" no-undo.
/*
  Read a directory
       /home/operinit/
       
  ls for files beginning with (reportnm)*.tif
       
  The theory is that oeepim1 only builds files that have names consisting
  of <reportnm>:<email address>:<invoiceno>"-"<invoicesufix>.tif
  or something similar and unique to the exact run of oeepi that is 
  initiating the oeepim1 program not converting them with tif2pdf.
       
   Then this takes all the tif files that re going to the same destination
   and builds a proper command to tif2pdf to have it combine the files
   and make one PDF file for the bunch.
*/       

def stream io-dir.
def        var x-filename   as char                         no-undo.
def        var x-reportnm   as char                         no-undo.
def        var x-operinit   as c format "x(4)"              no-undo.
def        var s-operinit   as c format "x(4)"              no-undo.
def        var x-invcount   as i format ">>9"               no-undo.
def        var x-deletefile as char                         no-undo.
def        var i            as i format ">>9"               no-undo.
def        var y            as i format ">>9"               no-undo.
def        var iCounter     as i format ">>9"               no-undo.

def shared var v-reportnm   like sapb.reportnm              no-undo.
def shared var g-operinit   as  c format "x(4)"             no-undo.
def shared var g-cono       like oeeh.cono                  no-undo. 
def shared var g-currproc   like sassm.currproc             no-undo.

/*
def        var v-reportnm   like sapb.reportnm              no-undo.
assign v-reportnm = "50999437".
*/

define temp-table TifMerge no-undo
 field reportnm     as char format "x(30)"
 field email        as char
 field custname     as char format "x(30)"
 field suffixfile   as char format "x(11)"
 field custpo       like oeeh.custpo
 field filename     as char 
 field split        as logical
 field company      as char format "x(4)"
 field comp-oper    as char format "x(4)"   
 field divno        like arsc.divno
index Tix
 custname
index Tix2
 custname
 suffixfile.

if g-currproc <> "oeepi" then
  assign x-operinit = g-operinit.

/* Here I'm assuming a report number this probably wull be handed to the real
   program from an input parameter assigned from the OEEPI job that is making
   the files at the time.
*/   

input stream io-dir through value("ls " + v-reportnm + "*.*") no-echo.

repeat:
  do transaction:
  import stream io-dir  unformatted x-filename.
    
    if num-entries(x-filename,"!") ge 3 then do:
      create TifMerge.
      assign TifMerge.reportnm   = entry(1,x-filename,"!")
             TifMerge.custname   = entry(2,x-filename,"!")
             TifMerge.suffixfile = entry(3,x-filename,"!")
             TifMerge.suffixfile = entry(1,TifMerge.suffixfile,".")
             TifMerge.filename   = x-filename.
      find oeeh where oeeh.cono = g-cono and
                      oeeh.orderno = int(entry(1,TifMerge.suffixfile,"-")) and
                      oeeh.ordersuf = int(entry(2,TifMerge.suffixfile,"-"))
                      no-lock no-error.
      if avail oeeh then
        do:
        assign TifMerge.custpo = oeeh.custpo
               TifMerge.divno  = oeeh.divno.
        assign TifMerge.custpo = replace(TifMerge.custpo, "'", "`").
        assign TifMerge.custpo = replace(TifMerge.custpo, """", "`").
        
        if oeeh.shipto <> "" then
          find arss where arss.cono   = g-cono and
                          arss.custno = oeeh.custno and
                          arss.shipto = oeeh.shipto
                          no-lock
                          no-error.
        find arsc where arsc.cono   = g-cono and
                        arsc.custno = oeeh.custno
                        no-lock no-error.
        if oeeh.shipto <> "" and 
          avail arss and arss.einvto = no then 
          do:
          /*email to shipto*/
          assign TifMerge.email = if arss.email <> " " then 
                                    arss.email  
                                  else
                                    "dsivak@sunsrce.com"
                 TifMerge.company = substr(arss.user19,1,4).
          assign TifMerge.comp-oper = if TifMerge.company = "PERF" then
                                        "pinv"
                                      else
                                        "tinv".
          if g-currproc <> "oeepi" then
            assign TifMerge.comp-oper = x-operinit. 
          find notes where notes.cono = g-cono and
                          notes.notestype = "zz" and
                          notes.primarykey = "INVemail" and
                          notes.secondarykey = string(arss.custno) +
                                                      arss.shipto and
                          notes.pageno = 1
                          no-lock no-error.
          if avail notes then
            do:
            do i = 1 to 5:
              if notes.noteln[i] <> " " then
                assign TifMerge.email = TifMerge.email + "; " + notes.noteln[i].
            end.
            if notes.noteln[10] = "y" then
              assign TifMerge.split = yes.
            else
              assign TifMerge.split = no.
          end.
        end. /* avail arss */
        else
        if avail arsc then
          do:
          assign TifMerge.email = if arsc.email <> " " then 
                                    arsc.email  
                                  else
                                    "dsivak@sunsrce.com"
                 TifMerge.company = substr(arsc.user19,1,4).

          assign TifMerge.comp-oper = if TifMerge.company = "PERF" then
                                        "pinv"
                                      else
                                        "tinv".
          if g-currproc <> "oeepi" then
            assign TifMerge.comp-oper = x-operinit. 
          find notes where notes.cono = g-cono and
                           notes.notestype = "zz" and
                           notes.primarykey = "INVemail" and
                           notes.secondarykey = string(arsc.custno) and
                           notes.pageno = 1
                           no-lock no-error.
          if avail notes then
            do:
            do i = 1 to 5:
              if notes.noteln[i] <> " " then
                assign TifMerge.email = TifMerge.email + "; " +
                                        notes.noteln[i].
            end.
            if notes.noteln[10] = "y" then
            assign TifMerge.split = yes.
          end.
          else
            assign TifMerge.split = no.
        end. /* avail arsc */
        else
          assign TifMerge.email = "dsivak@sunsrce.com"
                 TifMerge.comp-oper = "tinv".
      end.
      else
        assign TifMerge.email = "dsivak@sunsrce.com"
               TifMerge.comp-oper = "tinv".

      if v-reportnm begins "oeet@" or g-currproc = "oerd" then
        do:
        find sapb where sapb.reportnm = v-reportnm no-lock no-error.
        if avail sapb then
          assign TifMerge.email = substr(sapb.faxphoneno,4).
          /*
          assign TifMerge.email = if (v-reportnm begins "oeet@" or 
                                    (g-currproc = "oerd" and 
                                     sapb.optvalue[9]= "no")) and
                                    substr(sapb.faxphoneno,4) matches "*@*"
                                    then
                                    substr(sapb.faxphoneno,4)
                                  else
                                    TifMerge.email.
          */
      end.

      assign  TifMerge.email  =   replace(TifMerge.email,";"," ").
      assign  TifMerge.email  =   replace(TifMerge.email,","," ").
      assign  TifMerge.email  =   replace(TifMerge.email,"'","`").
      if TifMerge.email = " " then 
        assign TifMerge.email = "dsivak@sunsrce.com".
      if TifMerge.comp-oper = " " then
        assign TifMerge.comp-oper = "tinv".
    end. /* if num-entries ge 3 */
  end. /* do transaction */
end. /* repeat */

assign x-filename = "".

/* Merge the emails */
for each TifMerge use-index Tix no-lock
                  where TifMerge.split = no
                  break by TifMerge.custname:

  assign x-operinit = TifMerge.comp-oper.
  assign x-invcount = x-invcount + 1.
  assign x-filename = x-filename +
                      (if x-filename = "" then 
                         TifMerge.filename
                       else
                         " " + TifMerge.filename).
  
  if last-of(TifMerge.custname) or x-invcount >= 25 then 
    do:
    assign cMailto = TifMerge.email.
    /* bundle the files together.  Terms page no longer needed */
    unix silent 
     value ("tiff2pdf -t -p l -o " + TifMerge.custname + "_" +
            TifMerge.suffixfile + ".pdf " +
            x-filename).

    if g-cono <> 80 then
     assign cMailsubject = "SunSource Invoice including PO " + TifMerge.custpo.
    else
      if TifMerge.company = "PGON" then
        assign cMailsubject = "Paragon Invoice including PO " + 
                              TifMerge.custpo.
      else
        assign cMailsubject = "Perfection Invoice including PO " +
                              TifMerge.custpo.  
    if v-reportnm begins "oeet@" then
      do:
      find sapb where sapb.reportnm = v-reportnm no-lock no-error.
      if avail sapb then
        do:
        do iCounter = 1 to 10:
          assign cMailMessage = cMailMessage +
                                replace(sapb.faxcom[iCounter],"'","`") + "^".
        end.
        
        run zsdioeepiweb.p (input-output cMailMessage).
 
        end.
      else
        do:
        if g-cono <> 80 then
          assign
          cMailMessage = if TifMerge.divno <> 40 then
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your SunSource Invoice.  "
                + "Please contact your SunSource A/R contact if you "
                + "have any questions."
                + chr(13)
                + chr(13)
                + "If paying by check, please note the Remit Address on the "
                + "invoice.  Or consider ACH."
                + chr(13)
                + "Bank Name:    Bank of America" + chr(13)
                + "ABA Number:   071000039"  + chr(13)
                + "Acct Number:  8670517212"  + chr(13)
                + "Account Name: STS Operating, Inc" + chr(13)  
                + "Remittance Advice to creditservices@sunsrce.com"
            else    
                "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your SunSource Invoice.  "
                + "Please contact your SunSource A/R contact if you "
                + "have any questions."
                + chr(13).
        else
         if TifMerge.company = "PGON" then
           assign
           cMailMessage = if TifMerge.divno <> 40 then
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Paragon Invoice.  " 
                + "Please contact your Paragon A/R contact if you "
                + "have any questions."
                + chr(13)
                + chr(13)
                + "If paying by check, please note the Remit Address on the "
                + "invoice.  Or consider ACH."
                + chr(13)
                + "Bank Name:    Bank of America" + chr(13)
                + "ABA Number:   071000039"  + chr(13)
                + "Acct Number:  8670516609"  + chr(13)
                + "Account Name: STS Operating, Inc" + chr(13)
                + "Remittance Advice to creditservices@sunsrce.com"
              else
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Paragon Invoice.  " 
                + "Please contact your Paragon A/R contact if you "
                + "have any questions."
                + chr(13).
          else
           assign
           cMailMessage = if TifMerge.divno <> 40 then
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Perfection Invoice.  "
                + "Please contact your Perfection A/R Contact if you "    
                + "have any questions. **Please DO NOT REPLY to this "
                + "email**"                
                + chr(13)
                + chr(13)
                + "If paying by check, please note the Remit Address on the "
                + "invoice.  Or consider ACH."
                + chr(13)
                + "Bank Name:    Bank of America" + chr(13)
                + "ABA Number:   071000039"  + chr(13)
                + "Acct Number:  8670516609"  + chr(13)
                + "Account Name: Perfection/STS Operating, Inc" + chr(13)
                + "Remittance Advice to AccountsReceivable@pshinc.com"
             else
                 "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Perfection Invoice.  "
                + "Please contact your Perfection A/R Contact if you "    
                + "have any questions. **Please DO NOT REPLY to this "
                + "email**"                
                + chr(13).
      end.
    end.
    else
      do:
      if g-cono <> 80 then
        assign
        cMailMessage = if TifMerge.divno <> 40 then
            "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
          + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
          + "THANK YOU!****"
          + chr(13)
          + chr(13)
          + "The attached file contains your SunSource Invoice.  "
          + "Please contact your SunSource A/R contact if you "
          + "have any questions."
          + chr(13)
          + chr(13)
          + "If paying by check, please note the Remit Address on the "
          + "invoice.  Or consider ACH."
          + chr(13)
          + "Bank Name:    Bank of America" + chr(13)
          + "ABA Number:   071000039"  + chr(13)
          + "Acct Number:  8670517212"  + chr(13)
          + "Account Name: STS Operating, Inc" + chr(13)  
          + "Remittance Advice to creditservices@sunsrce.com"
        else
            "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
          + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
          + "THANK YOU!****"
          + chr(13)
          + chr(13)
          + "The attached file contains your SunSource Invoice.  "
          + "Please contact your SunSource A/R contact if you "
          + "have any questions."
          + chr(13).
      else
        if TifMerge.company = "PGON" then
          assign
          cMailMessage = if TifMerge.divno <> 40 then
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Paragon Invoice.  " 
                + "Please contact your Paragon A/R contact if you "
                + "have any questions."
                + chr(13)   
                + chr(13)
                + "If paying by check, please note the Remit Address on the "
                + "invoice.  Or consider ACH."
                + chr(13)
                + "Bank Name:    Bank of America" + chr(13)
                + "ABA Number:   071000039"  + chr(13)
                + "Acct Number:  8670516609"  + chr(13)
                + "Account Name: STS Operating, Inc" + chr(13)  
                + "Remittance Advice to creditservices@sunsrce.com"
             else
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Paragon Invoice.  " 
                + "Please contact your Paragon A/R contact if you "
                + "have any questions."
                + chr(13).
        else
         assign
         cMailMessage = if TifMerge.divno <> 40 then
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Perfection Invoice.  "
                + "Please contact your Perfection A/R Contact if you "    
                + "have any questions. **Please DO NOT REPLY to this "
                + "email**"                
                + chr(13)
                + chr(13)
                + "If paying by check, please note the Remit Address on the "
                + "invoice.  Or consider ACH."
                + chr(13)
                + "Bank Name:    Bank of America" + chr(13)
                + "ABA Number:   071000039"  + chr(13)
                + "Acct Number:  8670516609"  + chr(13)
                + "Account Name: STS Operating, Inc" + chr(13)  
                + "Remittance Advice to AccountsReceivable@pshinc.com"
             else
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Perfection Invoice.  "
                + "Please contact your Perfection A/R Contact if you "    
                + "have any questions. **Please DO NOT REPLY to this "
                + "email**"                
                + chr(13).
    end.
    if cMailMessage = "^^^^^^^^^^" then
      do:   
      if g-cono <> 80 then
        assign  
        cMailMessage = if TifMerge.divno <> 40 then
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your SunSource Invoice.  "
                + "Please contact your SunSource A/R contact if you "
                + "have any questions."
                + chr(13)
                + chr(13)
                + "If paying by check, please note the Remit Address on the "
                + "invoice.  Or consider ACH."
                + chr(13)
                + "Bank Name:    Bank of America" + chr(13)
                + "ABA Number:   071000039"  + chr(13)
                + "Acct Number:  8670517212"  + chr(13)
                + "Account Name: STS Operating, Inc" + chr(13)  
                + "Remittance Advice to creditservices@sunsrce.com"
             else
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your SunSource Invoice.  "
                + "Please contact your SunSource A/R contact if you "
                + "have any questions."
                + chr(13).
      else
        if TifMerge.company = "PGON" then
         assign
         cMailMessage = if TifMerge.divno <> 40 then
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Paragon Invoice.  " 
                + "Please contact your Paragon A/R contact if you "
                + "have any questions."
                + chr(13)
                + chr(13)
                + "If paying by check, please note the Remit Address on the "
                + "invoice.  Or consider ACH."
                + chr(13)
                + "Bank Name:    Bank of America" + chr(13)
                + "ABA Number:   071000039"  + chr(13)
                + "Acct Number:  8670516609"  + chr(13)
                + "Account Name: STS Operating, Inc" + chr(13)  
                + "Remittance Advice to creditservices@sunsrce.com"
             else
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Paragon Invoice.  " 
                + "Please contact your Paragon A/R contact if you "
                + "have any questions."
                + chr(13).
        else
         assign
         cMailMessage = if TifMerge.divno <> 40 then
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Perfection Invoice.  "
                + "Please contact your Perfection A/R Contact if you "    
                + "have any questions. **Please DO NOT REPLY to this "
                + "email**"                
                + chr(13)
                + chr(13)
                + "If paying by check, please note the Remit Address on the "
                + "invoice.  Or consider ACH."
                + chr(13)   
                + "Bank Name:    Bank of America" + chr(13)
                + "ABA Number:   071000039"  + chr(13)
                + "Acct Number:  8670516609"  + chr(13)
                + "Account Name: STS Operating, Inc" + chr(13)  
                + "Remittance Advice to AccountsReceivable@pshinc.com"
             else
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Perfection Invoice.  "
                + "Please contact your Perfection A/R Contact if you "    
                + "have any questions. **Please DO NOT REPLY to this "
                + "email**"                
                + chr(13).
    end.

    os-command  value ("sh /rd/cust/local/jailtinv.sh -b " +         
                       "-s " + "'" + cMailsubject + "' " +           
                       "-l " + "'" + cMailMessage + "' " +           
                       "-n '" + x-operinit + "' " +               
                       "'" + cMailto + "' " +
                       "'"  +
                       TifMerge.custname + "_" + TifMerge.suffixfile +
                       ".pdf" +  
                       "'").
    
    /*
    os-command
     value
       ("sh /rd/cust/local/jailtinv.sh -b -s " +
        " 'SunSource Invoice2' -l 'Email Body' 'SunSourceIT@yahoo.com' " +
        "'/usr/tmp/emailinv/JensTestCustomer_1437887-00.pdf'").
    */
    if g-currproc = "oeepi" then            
      unix silent value 
         ("cp " + TifMerge.custname + "_" + TifMerge.suffixfile + ".pdf " +
          "/usr/tmp/emailinv/").
    unix silent value
         ("rm -f " + TifMerge.custname + "_" + TifMerge.suffixfile + ".pdf").

    if x-invcount >= 25 then
      do:
      assign y = num-entries(x-filename," ").
      do i = 1 to y:
        assign x-deletefile = entry(i,x-filename," ").
        unix silent value ("rm -f " + x-deletefile + "*").
        assign x-deletefile = "".
      end. /* i = 1 to y */
    end.
    assign x-filename   = ""
           x-invcount   = 0
           x-deletefile = "".
  end. /* if last of TifMerge.custname or x-invcount >= 25 */
end. /* for each TifMerge */

/* send the remaining email files one at a time - DO NOT Merge */
for each TifMerge use-index Tix no-lock
                  where TifMerge.split = yes:

  assign x-operinit = TifMerge.comp-oper.
  assign x-filename = x-filename +
                      (if x-filename = "" then 
                         TifMerge.filename
                       else
                         " " + TifMerge.filename).
  
  assign cMailto = TifMerge.email.
  unix silent 
     value ("tiff2pdf -t -p l -o " + TifMerge.custname + "_" +
            TifMerge.suffixfile + ".pdf " +
            x-filename).

  if g-cono <> 80 then
     assign cMailsubject = "SunSource Invoice including PO " + TifMerge.custpo.
  else
    if TifMerge.company = "PGON" then
      assign cMailsubject = "Paragon Invoice including PO " + 
                            TifMerge.custpo.
    else
      assign cMailsubject = "Perfection Invoice including PO " +
                            TifMerge.custpo. 
  
  if v-reportnm begins "oeet@" then
    do:
    find sapb where sapb.reportnm = v-reportnm no-lock no-error.
    if avail sapb then
      do:
      do iCounter = 1 to 10:
        assign cMailMessage = cMailMessage +
                              replace(sapb.faxcom[iCounter],"'","`") + "^".
      end.
      
      run zsdioeepiweb.p (input-output cMailMessage).

      end.
    else
      assign    
      cMailMessage = if TifMerge.divno <> 40 then
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your SunSource Invoice.  "
                + "Please contact your SunSource A/R contact if you "
                + "have any questions."
                + chr(13)
                + chr(13)
                + "If paying by check, please note the Remit Address on the "
                + "invoice.  Or consider ACH."
                + chr(13)
                + "Bank Name:    Bank of America" + chr(13)
                + "ABA Number:   071000039"  + chr(13)
                + "Acct Number:  8670517212"  + chr(13)
                + "Account Name: STS Operating, Inc" + chr(13)  
                + "Remittance Advice to creditservices@sunsrce.com"
             else
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your SunSource Invoice.  "
                + "Please contact your SunSource A/R contact if you "
                + "have any questions."
                + chr(13).
  end.
  else
    do:
    if g-cono <> 80 then
        assign
        cMailMessage = if TifMerge.divno <> 40 then
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your SunSource Invoice.  "
                + "Please contact your SunSource A/R contact if you "
                + "have any questions."
                + chr(13) 
                + chr(13)
                + "If paying by check, please note the Remit Address on the "
                + "invoice.  Or consider ACH."
                + chr(13)
                + "Bank Name:    Bank of America" + chr(13)
                + "ABA Number:   071000039"   + chr(13)
                + "Acct Number:  8670517212"  + chr(13)
                + "Account Name: STS Operating, Inc" + chr(13)  
                + "Remittance Advice to creditservices@sunsrce.com"
             else
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your SunSource Invoice.  "
                + "Please contact your SunSource A/R contact if you "
                + "have any questions."
                + chr(13).
      else
        if TifMerge.company = "PGON" then
         assign
         cMailMessage = if TifMerge.divno <> 40 then
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Paragon Invoice.  " 
                + "Please contact your Paragon A/R contact if you "
                + "have any questions."
                + chr(13)
                + chr(13)
                + "If paying by check, please note the Remit Address on the "
                + "invoice.  Or consider ACH."
                + chr(13)
                + "Bank Name:    Bank of America" + chr(13)
                + "ABA Number:   071000039"  + chr(13)
                + "Acct Number:  8670516609"  + chr(13)
                + "Account Name: STS Operating, Inc" + chr(13)  
                + "Remittance Advice to creditservices@sunsrce.com"
             else
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Paragon Invoice.  " 
                + "Please contact your Paragon A/R contact if you "
                + "have any questions."
                + chr(13).
        else
         assign 
         cMailMessage = if TifMerge.divno <> 40 then
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Perfection Invoice.  "
                + "Please contact your Perfection A/R Contact if you "    
                + "have any questions. **Please DO NOT REPLY to this "
                + "email**"                
                + chr(13)   
                + chr(13)
                + "If paying by check, please note the Remit Address on the "
                + "invoice.  Or consider ACH."
                + chr(13)
                + "Bank Name:    Bank of America" + chr(13)
                + "ABA Number:   071000039"  + chr(13)
                + "Acct Number:  8670516609"  + chr(13)
                + "Account Name: STS Operating, Inc" + chr(13)  
                + "Remittance Advice to AccountsReceivable@pshinc.com"
             else
                   "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Perfection Invoice.  "
                + "Please contact your Perfection A/R Contact if you "    
                + "have any questions. **Please DO NOT REPLY to this "
                + "email**"                
                + chr(13).
  end.   
  
  if cMailMessage = "^^^^^^^^^^" then
    do:   
    if g-cono <> 80 then
      assign    
      cMailMessage = if TifMerge.divno <> 40 then
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your SunSource Invoice.  "
                + "Please contact your SunSource A/R contact if you "
                + "have any questions."
                + chr(13)   
                + chr(13)
                + "If paying by check, please note the Remit Address on the "
                + "invoice.  Or consider ACH."
                + chr(13)
                + "Bank Name:    Bank of America" + chr(13)
                + "ABA Number:   071000039"  + chr(13)
                + "Acct Number:  8670517212"  + chr(13)
                + "Account Name: STS Operating, Inc" + chr(13)  
                + "Remittance Advice to creditservices@sunsrce.com"
             else   
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your SunSource Invoice.  "
                + "Please contact your SunSource A/R contact if you "
                + "have any questions."
                + chr(13).   
    else
      if TifMerge.company = "PGON" then
        assign
        cMailMessage = if TifMerge.divno <> 40 then
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Paragon Invoice.  " 
                + "Please contact your Paragon A/R contact if you "
                + "have any questions."
                + chr(13)
                + chr(13)
                + "If paying by check, please note the Remit Address on the "
                + "invoice.  Or consider ACH."
                + chr(13)
                + "Bank Name:    Bank of America" + chr(13)
                + "ABA Number:   071000039"  + chr(13)
                + "Acct Number:  8670516609"  + chr(13)
                + "Account Name: STS Operating, Inc" + chr(13)  
                + "Remittance Advice to creditservices@sunsrce.com"
             else
                   "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Paragon Invoice.  "
                + "Please contact your Perfection A/R Contact if you "    
                + "have any questions. **Please DO NOT REPLY to this "
                + "email**"                
                + chr(13).
      else
        assign
        cMailMessage = if TifMerge.divno <> 40 then
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Perfection Invoice.  "
                + "Please contact your Perfection A/R Contact if you "    
                + "have any questions. **Please DO NOT REPLY to this "
                + "email**"                
                + chr(13)
                + chr(13)
                + "If paying by check, please note the Remit Address on the "
                + "invoice.  Or consider ACH."
                + chr(13)
                + "Bank Name:    Bank of America" + chr(13)
                + "ABA Number:   071000039"  + chr(13)
                + "Acct Number:  8670516609"  + chr(13)
                + "Account Name: STS Operating, Inc" + chr(13)  
                + "Remittance Advice to AccountsReceivable@pshinc.com"
             else
                  "****ATTN: PLEASE NOTE OUR NEW REMIT TO ADDRESS LISTED "
                + "ABOVE AND MAKE THE NECESSARY CHANGES TO YOUR RECORDS.  "
                + "THANK YOU!****"
                + chr(13)
                + chr(13)
                + "The attached file contains your Perfection Invoice.  "
                + "Please contact your Perfection A/R Contact if you "    
                + "have any questions. **Please DO NOT REPLY to this "
                + "email**"                
                + chr(13).
  end.  
  
  os-command  value ("sh /rd/cust/local/jailtinv.sh -b " +         
                     "-s " + "'" + cMailsubject + "' " +           
                     "-l " + "'" + cMailMessage + "' " +           
                     "-n '" + x-operinit + "' " +               
                     "'" + cMailto + "' " +
                     "'"  +
                     TifMerge.custname + "_" + TifMerge.suffixfile +
                     ".pdf" +  
                     "'").
    
  if g-currproc = "oeepi" then            
    unix silent value 
       ("cp " + TifMerge.custname + "_" + TifMerge.suffixfile + ".pdf " +
        "/usr/tmp/emailinv/").
  unix silent value
       ("rm -f " + TifMerge.custname + "_" + TifMerge.suffixfile + ".pdf").
  assign x-filename   = ""
         x-invcount   = 0
         x-deletefile = "".
end. /* for each TifMerge */


unix silent value ("rm -f " + v-reportnm + "*").

  

