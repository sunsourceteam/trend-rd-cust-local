


/******************************************************************************
  INCLUDE       : p-oeexqcnt.i
  DESCRIPTION   : Custom Quote Entry - Conversion
  USED ONCE?    : No (oeexq.p, oeexqinq.p)
  AUTHOR        : SunSource
  DATE WRITTEN  : 05/01/07
  CHANGES MADE  : 03/12/09 das ; when a NS BOD kit is converted the usage flag
                  06/10/09 das; NonReturn/NonCancelable
                  
******************************************************************************/



/* ------------------------------------------------------------------------- */ 
Procedure Conversion-Load: 
/* ------------------------------------------------------------------------- */
 
  find icsp where icsp.cono = g-cono and
                  icsp.prod = t-lines.prod no-lock no-error.
  
  
  assign v-loadbrowseline = true
                     v-lmode = "c"
                     v-lcomment =   t-lines.lcomment  
                     v-seqno      = t-lines.seqno
                     v-lineno     = t-lines.lineno      
                     v-prod       = t-lines.prod         
                     v-xprod      = t-lines.xprod
                     v-whse       = t-lines.whse         
                     v-vendno     = t-lines.vendno
                     v-prodcat    = t-lines.prodcat
                     v-listprc    = t-lines.listprc      
                     v-ptype      = t-lines.pricetype
                     v-sellprc    = t-lines.sellprc
                     v-prodcost   = t-lines.prodcost
                     v-glcost     = t-lines.glcost
                     v-totcost    = t-lines.totcost
                     v-leadtm     = t-lines.leadtm
                     v-gp         = t-lines.gp           
                     v-disc       = t-lines.discpct
                     /*v-disctype   = t-lines.disctype*/
                     v-descrip    = t-lines.descrip
                     v-qty        = t-lines.qty
                     v-kqty       = t-lines.kqty
                     v-shipdt     = t-lines.shipdt     
                     v-specnstype = t-lines.specnstype
                     v-kitfl      = t-lines.kitfl
                     v-matrixed   = if t-lines.qtybrkty <> "" then
                                       t-lines.qtybrkty
                                    else
                                       " "
                     v-pdrecord    = t-lines.pdscrecno
                     v-pdtype     = t-lines.pdtype
                     v-pdamt      = t-lines.pdamt
                     v-pd$md      = t-lines.pd$md
                     v-totnet     = t-lines.totnet
                     v-lcomment   = t-lines.lcomment
                     v-totcost    = t-lines.totcost
                     v-rebatefl   = t-lines.rebatefl
                     v-totmarg    = t-lines.totmarg
                     v-margpct    = t-lines.margpct
                     o-lineno     = t-lines.lineno      
                     o-seqno      = t-lines.seqno
                     o-prod       = t-lines.prod         
                     o-whse       = t-lines.whse         
                     o-listprc    = t-lines.listprc      
                     o-sellprc    = t-lines.sellprc     
                     o-gp         = t-lines.gp           
                     o-disc       = t-lines.discpct
                     o-disctype   = t-lines.disctype
                     o-qty        = t-lines.qty          
                     o-shipdt     = t-lines.shipdt
                     o-kitfl      = t-lines.kitfl
                     o-specnstype = t-lines.specnstype.     
              /* display product lookup and whse info on existing lines*/
end.

/* ------------------------------------------------------------------------- */ 
Procedure Conversion-Loadx: 
/* ------------------------------------------------------------------------- */
 
  find icsp where icsp.cono = g-cono and
                  icsp.prod = xt-lines.prod no-lock no-error.
  
  
  assign v-loadbrowseline = true
                     v-lmode = "c"
                     v-lcomment =   xt-lines.lcomment  
                     v-seqno      = xt-lines.seqno
                     v-lineno     = xt-lines.lineno      
                     v-prod       = xt-lines.prod         
                     v-xprod      = xt-lines.xprod
                     v-whse       = xt-lines.whse         
                     v-vendno     = xt-lines.vendno
                     v-prodcat    = xt-lines.prodcat
                     v-listprc    = xt-lines.listprc      
                     v-ptype      = xt-lines.pricetype
                     v-sellprc    = xt-lines.sellprc
                     v-prodcost   = xt-lines.prodcost
                     v-glcost     = xt-lines.glcost
                     v-totcost    = xt-lines.totcost
                     v-leadtm     = xt-lines.leadtm
                     v-gp         = xt-lines.gp           
                     v-disc       = xt-lines.discpct
                     /*v-disctype   = xt-lines.disctype*/
                     v-descrip    = xt-lines.descrip
                     v-qty        = xt-lines.qty
                     v-kqty       = xt-lines.kqty
                     v-shipdt     = xt-lines.shipdt     
                     v-specnstype = xt-lines.specnstype
                     v-kitfl      = xt-lines.kitfl
                     v-matrixed   = if xt-lines.qtybrkty <> "" then
                                       xt-lines.qtybrkty
                                    else
                                       " "
                     v-pdrecord    = xt-lines.pdscrecno
                     v-pdtype     = xt-lines.pdtype
                     v-pdamt      = xt-lines.pdamt
                     v-pd$md      = xt-lines.pd$md
                     v-totnet     = xt-lines.totnet
                     v-lcomment   = xt-lines.lcomment
                     v-totcost    = xt-lines.totcost
                     v-rebatefl   = xt-lines.rebatefl
                     v-totmarg    = xt-lines.totmarg
                     v-margpct    = xt-lines.margpct
                     o-lineno     = xt-lines.lineno      
                     o-seqno      = xt-lines.seqno
                     o-prod       = xt-lines.prod         
                     o-whse       = xt-lines.whse         
                     o-listprc    = xt-lines.listprc      
                     o-sellprc    = xt-lines.sellprc     
                     o-gp         = xt-lines.gp           
                     o-disc       = xt-lines.discpct
                     o-disctype   = xt-lines.disctype
                     o-qty        = xt-lines.qty          
                     o-shipdt     = xt-lines.shipdt
                     o-kitfl      = xt-lines.kitfl
                     o-specnstype = xt-lines.specnstype.     
              /* display product lookup and whse info on existing lines*/
end.
   
/* ------------------------------------------------------------------------- */ 
Procedure Conversion: 
/* ------------------------------------------------------------------------- */
  define input parameter ix-recid as recid no-undo.

  def buffer xi-sasoo for sasoo.
  def buffer xi2-sasoo for sasoo.
  def buffer xi-icsd  for icsd.
  def buffer xi-sasos for sasos.
  def buffer xi-arsc for arsc.
  def buffer xi-arss for arss.
  def buffer xi-oeelb for oeelb.
  def var xi-oeehbrecid as recid no-undo.
  def var o-shipvia like oeeh.shipvia no-undo.

  on cursor-up back-tab.   
  on cursor-down tab.

  on leave of oeehb.transtype in frame f-oeexqcnt do:
    if input oeehb.transtype = "do" and not v-powtintfllocal then 
      do:
      assign oeehb.transtype = input oeehb.transtype
             x-returncode = ""
             g-oetype = oeehb.transtype.
      run oeexqrelchk.p  (input xi-oeehbrecid, 
                          input-output ip-update,
                          input ?,
                          input ?,
                          input "Hdocheck",
                          input-output x-returncode).  
      assign ip-update = true.
      if not s-canceled and x-returncode <> "back" then
        assign v-powtintfllocal = true.

      readkey pause 0.
    end.
    if input oeehb.transtype = "FO"
      then do:
      update s-oelockfl with frame f-FOlock.
    end.
    
    /*assign s-oelockfl = input s-oelockfl.*/
    hide frame f-FOlock no-pause.
    put screen row 13 col 3 color 
    message 
 " F7-Line Maintenance   F8-Vendor/Whse                                      ".


  
  end.
  
  
  
  assign s-canceled = false.  
  find first  xi-sasoo where xi-sasoo.cono   = g-cono and
                             xi-sasoo.oper2  = g-operinits no-lock no-error.
  if not avail xi-sasoo then 
    leave.
  
  assign v-shiptofl       = xi-sasoo.shiptofl
         v-powtintfllocal = false.
  
  for each xi-oeelb where
       xi-oeelb.cono        = g-cono and
       xi-oeelb.batchnm     = string(v-quoteno,">>>>>>>9") and
       xi-oeelb.seqno       = 1 :
    overlay (xi-oeelb.user5,2,3) = "no ".
  end. 

/* Create a Copy of this quote                                    */
  run oeexqrelx.p (input "Prepare",
                   input ix-recid,
                   output x-error). 
  if x-error then
    do:
    message "Fatal Preparation error". pause 5.
    assign s-canceled = true.
    return.
    end.
 else
    assign x-backed-quote-up = true.

 if avail b-oeehb and b-oeehb.stagecd = 9 then
   do:
   run Reprice.
   run Quote-Total.
 end.
 
 /* das - 02/23/09 - tax fix */
 find oeehb where recid(oeehb) = ix-recid no-error.
 if avail oeehb then do:
   run Load-Defaults.
   assign oeehb.jobno = oeehb.countrycd
          oeehb.countrycd = "".
 /*EN-437*/ 
   assign popupdisplayed = no.
   find first oeelb where oeelb.cono    = oeehb.cono and
                          oeelb.batchnm = oeehb.batchnm and
                          oeelb.seqno   = oeehb.seqno and
               int(substr(oeelb.user12,10,4)) <> 0  and
              date(substr(oeelb.user12,1,8))  <> ?
                          no-lock no-error.
   if avail oeelb then
     do:    
     assign popupdisplayed = yes.
     assign pdatefl = no.   
     message  "Do you want OLT Line Promise Dates Recalculated."  
              view-as alert-box question buttons yes-no            
              update pdatefl as logical.                                 
   end.
   
/* EN-1939 */
   find first  xi2-sasoo where 
               xi2-sasoo.cono   = g-cono and
               xi2-sasoo.oper2  = oeehb.takenby no-lock no-error.
   if not avail xi2-sasoo then 
     assign oeehb.takenby = g-operinits.
/* EN-1939 */   
   for each oeelb where
            oeelb.cono = g-cono and
            oeelb.batchnm = oeehb.batchnm and
            oeelb.seqno   = oeehb.seqno:
     /*
     assign oeelb.taxablefl    = if oeelb.taxablefl = yes and
                                    oeehb.taxablefl = no then oeehb.taxablefl
                                 else oeelb.taxablefl
            oeelb.nontaxtype   = if oeelb.taxablefl = yes then " " 
                                 else oeelb.nontaxtype.
     */
     /* EN-437 */   
     assign w-netavail = 0.
     if pdatefl = yes or (pdatefl = no and 
                          oeehb.enterdt  < TODAY and
                          popupdisplayed = no)  then
       do:
       overlay(oeelb.user12,15,1) = 
               if int(substr(oeelb.user12,10,4)) <> 0 then 
                 "y"
               else
                 " ".
       /*Scenario 1 - No Date Overridden */
       if int(substr(oeelb.user12,10,4)) = 0 then
         do:
         find icsw where icsw.cono = g-cono and
                         icsw.whse = substr(oeelb.user4,1,4) and
                         icsw.prod = oeelb.shipprod
                         no-lock no-error.
         if avail icsw then 
           do:
           /*assign oeelb.leadtm = icsw.leadtmavg.*/
           assign w-netavail = icsw.qtyonhand - (icsw.qtyreservd +
                                                 icsw.qtycommit).
           if oeelb.qtyord > w-netavail then
             assign oeelb.leadtm    = icsw.leadtmavg
                    oeelb.promisedt = TODAY + icsw.leadtmavg
                    oeelb.xxda1     = oeelb.promisedt.   
           else
             assign oeelb.leadtm    = icsw.leadtmavg
                    oeelb.promisedt = TODAY /*+ oeelb.leadtm*/
                    oeelb.xxda1     = oeelb.promisedt.
         end. /* avail icsw */
       end. /* leadtime was not overridden */
       /*Scenario 2 - Date Overridden */
       else
         do:
         if popupdisplayed = yes and pdatefl = no then 
           do:
           overlay(oeelb.user12,15,1) = "n".
           next.    
         end.
         find icsw where icsw.cono = g-cono and
                         icsw.whse = substr(oeelb.user4,1,4) and
                         icsw.prod = oeelb.shipprod
                         no-lock no-error.
         if avail icsw then 
           do:
           assign w-netavail = icsw.qtyonhand - (icsw.qtyreservd +
                                                 icsw.qtycommit).
           if oeelb.qtyord > w-netavail then
             assign oeelb.promisedt = TODAY + int(substr(oeelb.user12,10,4))
                    oeelb.xxda1     = oeelb.promisedt.
           else
             assign oeelb.promisedt = TODAY
                    oeelb.xxda1     = oeelb.promisedt.
         end. /* avail icsw */      
         else
           do:
           assign oeelb.promisedt = TODAY + int(substr(oeelb.user12,10,4))
                  oeelb.xxda1     = oeelb.promisedt.
         end.
       end. /* leadtime was overridden */
       if oeelb.promisedt = TODAY then
         do:
         find icsd where icsd.cono = g-cono and 
                         icsd.whse = substr(oeelb.user4,1,4) 
                         no-lock no-error.
         if avail icsd and oeelb.promisedt = ? or 
                           oeelb.promisedt = today then
          assign oeelb.promisedt = 
            (if ((int(substring(string(time,"hh:mm:ss"),1,2)) * 100) +      
                  int(substring(string(time,"hh:mm:ss"),4,2))) >              
                ((truncate(truncate(icsd.enddaycut / 60 ,0) / 60,0) * 100) + 
                 (truncate(icsd.enddaycut / 60 ,0)) mod 60) then             
                 oeelb.promisedt  + 1                                      
             else                                                          
               oeelb.promisedt).                                         
          /*EN-3047*/
          if w-netavail >= oeelb.qtyord then    
            assign oeelb.promisedt = oeelb.promisedt + 1.
          assign oeelb.xxda1 = oeelb.promisedt.
       end. /* oeelb.promisedt = TODAY */   
       assign h-shipdt = oeelb.promisedt.
       run zsdiweekendh (input-output h-shipdt,                        
                         input-output v-1019fl,
                         input oeehb.whse,                                    
                         input g-cono,
                         input oeehb.divno).
       if v-1019fl = yes then
         do:
         assign oeelb.promisedt = h-shipdt.
         /*run err.p(1019).*/
       end.
       overlay(oeelb.user4,26,8)  = string(oeelb.promisedt,"99/99/99").
     end. /* pdatefl = yes */
     /* EN-437 */
     assign v-prod = oeelb.shipprod
            v-whse = substr(oeelb.user4,1,4).
     if oeehb.shipto <> "" then
       find arss where arss.cono   = g-cono and 
                       arss.custno = oeehb.custno and
                       arss.shipto = oeehb.shipto no-lock no-error.
     else
       find arsc where arsc.cono   = g-cono and
                       arsc.custno = oeehb.custno no-lock no-error.
     run Get_Tax_Flag (input-output v-taxablefl,  
                       input-output v-nontaxtype).
     assign oeelb.taxablefl = v-taxablefl
            oeelb.nontaxtype = v-nontaxtype.
   end. /* each oeelb */
 end. /* avail oeehb */
 if avail oeehb then
   do:
   assign oeehb.reqshipdt = /*if oeehb.reqshipdt < today then*/
                              today
                            /*else
                              oeehb.reqshipdt*/.
   find icsd where icsd.cono = g-cono and 
                   icsd.whse = oeehb.whse 
                   no-lock no-error.
   if avail icsd and oeehb.reqshipdt = today then
     assign oeehb.reqshipdt =                                              
      (if ((int(substring(string(time,"hh:mm:ss"),1,2)) * 100) +      
            int(substring(string(time,"hh:mm:ss"),4,2))) >              
          ((truncate(truncate(icsd.enddaycut / 60 ,0) / 60,0) * 100) + 
           (truncate(icsd.enddaycut / 60 ,0)) mod 60) then             
            oeehb.reqshipdt  + 1                                      
       else                                                          
         oeehb.reqshipdt).                                         
    
   assign h-shipdt = oeehb.reqshipdt.
   run zsdiweekendh (input-output h-shipdt,
                     input-output v-1019fl,
                     input oeehb.whse,
                     input g-cono,
                     input oeehb.divno).
   if v-1019fl = yes then
     do:
     assign oeehb.reqshipdt = h-shipdt.
     run err.p(1019).
   end.
 end.
ReleaseLoop:

do while true on endkey undo Releaseloop, leave ReleaseLoop:

  assign v-conversion = true
         v-loadbrowseline = false
         v-lmode = "c".
  run Clear-Frames.
  find oeehb where recid(oeehb) = ix-recid no-error.
  assign xi-oeehbrecid = recid(oeehb)
         v-contact = substr(oeehb.user3,1,20)
         v-custno = oeehb.custno
         v-shipto = oeehb.shipto.
  if oeehb.transtype = "QU" then
     oeehb.transtype = "SO".
  if oeehb.stagecd = 0 then
     oeehb.stagecd = 1.   

  display
    oeehb.batchnm
    oeehb.custno
    oeehb.transtype  
    oeehb.shiptonm  
    oeehb.shipto  
    oeehb.shiptoaddr[1] 
    oeehb.whse   
    oeehb.shiptoaddr[2] 
    oeehb.custpo   
    oeehb.shiptocity
    oeehb.shiptost   
    oeehb.shiptozip
    v-contact
    oeehb.shipvia    
    oeehb.inBndFrtFl   
    oeehb.outBndFrtFl   
    oeehb.orderdisp  
    oeehb.slsrepout  
    oeehb.slsrepin     
    oeehb.reqshipdt  
    oeehb.refer
    oeehb.shipinstr
      with frame f-oeexqcnt.
 put screen row 13 col 3 color 
           message 
 " F7-Line Maintenance   F8-Vendor/Whse                                      ".


  assign o-InBndFrtfl  = oeehb.InBndFrtFl 
         o-OutBndFrtfl = oeehb.OutBndFrtfl
         o-shipvia     = oeehb.shipvia.


  conversionloop:

do while true on endkey undo conversionloop, leave conversionloop:
     put screen row 13 col 3 color 
           message 
 " F7-Line Maintenance   F8-Vendor/Whse                                      ".
    assign g-custno = oeehb.custno
           g-shipto = oeehb.shipto

           hx-batchnm        = oeehb.batchnm             
           hx-custno         = oeehb.custno              
           hx-transtype      = oeehb.transtype           
           hx-shiptonm       = oeehb.shiptonm            
           hx-shipto         = oeehb.shipto              
           hx-shiptoaddr1    = oeehb.shiptoaddr[1]      
           hx-whse           = oeehb.whse                
           hx-shiptoaddr2    = oeehb.shiptoaddr[2]       
           hx-custpo         = oeehb.custpo              
           hx-shiptocity     = oeehb.shiptocity          
           hx-shiptost       = oeehb.shiptost            
           hx-shiptozip      = oeehb.shiptozip           
           hx-contact        =  v-contact
           hx-shipvia        = oeehb.shipvia             
           hx-inBndFrtFl     = oeehb.inbndfrtfl          
           hx-outBndFrtFl    = oeehb.outbndfrtfl         
           hx-orderdisp      = oeehb.orderdisp           
           hx-slsrepout      = oeehb.slsrepout           
           hx-slsrepin       = oeehb.slsrepin            
           hx-reqshipdt      = oeehb.reqshipdt           
           hx-refer          = oeehb.refer
           hx-shipinstr      = oeehb.shipinstr.           
    
    update
      oeehb.custno       when not v-conversion
      oeehb.shipto
      /*
      oeehb.shiptonm       when v-shiptofl = yes
      oeehb.shiptoaddr[1]  when v-shiptofl = yes
      oeehb.shiptoaddr[2]  when v-shiptofl = yes
      oeehb.shiptocity     when v-shiptofl = yes
      oeehb.shiptost       when v-shiptofl = yes
      oeehb.shiptozip      when v-shiptofl = yes
      oeehb.geocd          when v-shiptofl = yes
      */
      oeehb.transtype  
      oeehb.whse    
      oeehb.custpo
      v-contact
      oeehb.shipvia 
      oeehb.inBndFrtFl  when 
                        can-find(first xi-sasos
                                 where xi-sasos.cono  = g-cono    and 
                                       xi-sasos.oper2 = g-operinits and 
                                       xi-sasos.menuproc = "zxt1" and
                                       xi-sasos.securcd[10] ge 3 no-lock)
 
      oeehb.outBndFrtFl when  
                             can-find(first xi-sasos
                                where xi-sasos.cono  = g-cono    and 
                                      xi-sasos.oper2 = g-operinits and 
                                      xi-sasos.menuproc = "zxt1" and
                                      xi-sasos.securcd[10] ge 3 no-lock)
      oeehb.orderdisp   when not can-do("cr,cs,do",oeehb.transtype)
      oeehb.slsrepout   when  can-do("o,b",xi-sasoo.oeslsrepfl)
      oeehb.slsrepin    when  can-do("i,b",xi-sasoo.oeslsrepfl)
      oeehb.reqshipdt  
      oeehb.refer  
      oeehb.shipinstr

      oeehb.shiptonm       when v-shiptofl = yes
      oeehb.shiptoaddr[1]  when v-shiptofl = yes
      oeehb.shiptoaddr[2]  when v-shiptofl = yes
      oeehb.shiptocity     when v-shiptofl = yes
      oeehb.shiptost       when v-shiptofl = yes
      oeehb.shiptozip      when v-shiptofl = yes
      oeehb.geocd          when v-shiptofl = yes
      
      go-on(f7,f8) 

        with frame f-oeexqcnt 
      editing:
     put screen row 13 col 3 color 
           message 
 " F7-Line Maintenance   F8-Vendor/Whse                                      ".

        readkey.
     
        hide message no-pause.   /* das - 03/18/09 - added to remove display */
                                 /*                  of previous error msg   */
        if input oeehb.transtype = "cr" or
           input oeehb.transtype = "cs" or
           input oeehb.transtype = "do" then
          do:
            assign oeehb.orderdisp = "".
            disable oeehb.orderdisp with frame f-oeexqcnt.
            display oeehb.orderdisp with frame f-oeexqcnt.
        end.
        
        /*******
        if input oeehb.transtype <> "cr" and
           input oeehb.transtype <> "cs" and
           input oeehb.transtype <> "do" and
           input oeehb.shipto <> oeehb.shipto then
          do:
          if oeehb.shipto <> "" then
            do:
            find arss where arss.cono = g-cono and
                            arss.custno = oeehb.custno and
                            arss.shipto = oeehb.shipto
                            no-lock no-error.
            if avail arss and oeehb.orderdisp = input oeehb.orderdisp then
              assign oeehb.orderdisp = arss.orderdisp.
          end.
          else
            do:
            find arsc where arsc.cono = g-cono and
                            arsc.custno = oeehb.custno 
                            no-lock no-error.
            if avail arsc and oeehb.orderdisp = input oeehb.orderdisp then
              assign oeehb.orderdisp = arsc.orderdisp.
          end.
          enable oeehb.orderdisp with frame f-oeexqcnt.
          display oeehb.orderdisp with frame f-oeexqcnt.
        end. /* oeehb.transtype = "do" and input oeehb.transtype = "so" */
        ********/
  
        if {k-cancel.i}  then do:            
          message  "You hit (F4) CANCEL." skip 
                   "Cancel this Conversion ?"
            view-as alert-box question buttons yes-no            
          update zx-s as logical .                                 
          if zx-s then do:       
            assign ip-update = false
                   s-canceled = true.
      
            undo Releaseloop, leave Releaseloop.                                            end.                  
          else do:
            put screen row 13 col 3 color 
           message 
 " F7-Line Maintenance   F8-Vendor/Whse                                      ".

            next conversionloop.          
          end.
       end.

       if {k-after.i} or {k-func.i} then 
         do: 
         if can-do("306,307,308,309,310,401",string(lastkey)) then
           do:  /* if a func key, update the shipto info */
           for each t-oeehb where 
                    t-oeehb.cono       = oeehb.cono    and
                    t-oeehb.batchnm    = oeehb.batchnm and
                    t-oeehb.seqno      = oeehb.seqno   and
                    t-oeehb.sourcepros = oeehb.sourcepros:
             assign t-oeehb.shiptonm      = input oeehb.shiptonm
                    t-oeehb.shiptoaddr[1] = input oeehb.shiptoaddr[1]
                    t-oeehb.shiptoaddr[2] = input oeehb.shiptoaddr[2]
                    t-oeehb.shiptocity    = input oeehb.shiptocity
                    t-oeehb.shiptost      = input oeehb.shiptost
                    t-oeehb.shiptozip     = input oeehb.shiptozip
                    t-oeehb.geocd         = input oeehb.geocd
                    v-shiptonm            = t-oeehb.shiptonm
                    v-shiptoaddr1         = t-oeehb.shiptoaddr[1]
                    v-shiptoaddr2         = t-oeehb.shiptoaddr[2]
                    v-shiptocity          = t-oeehb.shiptocity
                    v-shiptost            = t-oeehb.shiptost
                    v-shiptozip           = t-oeehb.shiptozip
                    v-shiptogeocd         = t-oeehb.geocd.

             find xi-arsc where xi-arsc.cono = g-cono and
                                xi-arsc.custno = g-custno no-lock no-error.
             if t-oeehb.shipto <> "" then do:
               find xi-arss where xi-arss.cono = g-cono and
                                  xi-arss.custno = g-custno and
                                  xi-arss.shipto = 
                                   t-oeehb.shipto no-lock no-error.
 
               if avail xi-arss then do:
                 if xi-arss.state        <> t-oeehb.shiptost then do:    

                   /*assign /* t-oeehb.taxablefl = true  Chuck tax take out
                             t-oeehb.nontaxtype = ""  */
                          t-oeehb.statecd = t-oeehb.shiptost.*/
                   for each oeelb where 
                            oeelb.cono = g-cono and
                            oeelb.batchnm = t-oeehb.batchnm and
                            oeelb.seqno   = t-oeehb.seqno:
                     assign oeelb.taxablefl    = t-oeehb.taxablefl
                            oeelb.nontaxtype   = t-oeehb.nontaxtype.
                   end.
                 end.          
               end.
             end.
             else 
             if xi-arsc.state        <> t-oeehb.shiptost then do:    
               assign /* t-oeehb.taxablefl = true
                         t-oeehb.nontaxtype = "" Chuck tax take out */
                      t-oeehb.state = t-oeehb.shiptost.
               for each oeelb where 
                        oeelb.cono = g-cono and
                        oeelb.batchnm = t-oeehb.batchnm and
                        oeelb.seqno   = t-oeehb.seqno:
                 assign oeelb.taxablefl    = t-oeehb.taxablefl
                        oeelb.nontaxtype   = t-oeehb.nontaxtype.
               end.
             end.          
           end. /* for each */
         end. /* a function key was entered */

         /** das - added APR check only if shiptost has changed **/
         if /*(can-do("9,13,401,501,502,503,504,507,508,
                    306,307,308,309,310",string(lastkey)) and*/
           frame-field = "shiptost" and
           input oeehb.shiptost <> oeehb.shiptost  then
           do:
           assign v-shiptostatecd = input oeehb.shiptost.
           for each oeelb where oeelb.cono = g-cono and
                   oeelb.batchnm = oeehb.batchnm and
                   oeelb.seqno   = oeehb.seqno
                   no-lock:
             run SauerAPRcheck.p (input oeelb.prodcat,
                                  input v-shiptostatecd,
                                  input oeehb.shiptozip,
                                  input-output apr-error,
                                  input-output apr-msg).
             if apr-error = yes then
               leave.
           end. /* each oeelb */
           if apr-error = yes then
             do:
             /* Authpoint Custom for Sauer APR */
             run authchk.p(g-cono,
                           g-operinit,
                           "oeexq":U,
                           "lines":U,
                           "SDISauer":U,
                           "c":U,
                           "":U,
                           output zsdiconfirm).
             /* Auth */
             hide message no-pause.
             /* Auth */
             if zsdiconfirm = no then
               do:
               message color normal apr-msg.
               bell.
               next-prompt oeehb.shiptost with frame f-oeexqcnt.
               next.
             end.
           end. /* if apr-error = yes */
         end. /* frame-field = "shiptost" */
         
         
         if frame-field = "shiptost" and
            input oeehb.shiptost <> oeehb.shiptost and
                     not oeehb.taxablefl then do:
           /* assign /* oeehb.taxablefl = true
                     oeehb.nontaxtype = ""  Chuck tax take out */
                  oeehb.statecd = input oeehb.shiptost.*/
           for each oeelb where oeelb.cono = g-cono and
                    oeelb.batchnm = oeehb.batchnm and
                    oeelb.seqno   = oeehb.seqno:
              assign oeelb.taxablefl    = oeehb.taxablefl
                     oeelb.nontaxtype   = oeehb.nontaxtype.
           end.
         end.          
         
         if frame-field = "shipto" and
            hx-shipto <> input oeehb.shipto then 
           do:
/* Perfection */        
  
           run oeexqCustFocusPop.p (input g-cono,
                                    input 0,
                                    input 0,
                                    input g-custno,
                                    input (input oeehb.shipto)).
/* Perfection */        
   
           find xi-arsc where xi-arsc.cono = g-cono and
                              xi-arsc.custno = g-custno no-lock no-error.
           find xi-arss where xi-arss.cono = g-cono and
                              xi-arss.custno = g-custno and
                              xi-arss.shipto = 
                                input oeehb.shipto no-lock no-error.
           if avail xi-arss then
             assign v-shipfl = true.
           else 
             do:
             if input oeehb.shipto <> "" then
               do:
               run err.p (4304).
               assign hx-shipto    = oeehb.shipto.
               next-prompt oeehb.shipto with frame f-oeexqcnt.
               next.
             end.
           end.

/* 10-23-12 tah APR fix */           
           assign oeehb.shipto = input oeehb.shipto
                  oeehb.xxl12  = no.  /* if shipto is overridden, the F9 will 
                                         override does not take priority*/
           hx-shipto            = oeehb.shipto.
           run Load-Defaults.
           for each oeelb where oeelb.cono = g-cono and
                    oeelb.batchnm = oeehb.batchnm and
                    oeelb.seqno   = oeehb.seqno
                    no-lock:
             run SauerAPRcheck.p (input oeelb.prodcat,
                                  input v-shiptostatecd,
                                  input (if avail arss then arss.zip 
                                         else
                                         if avail arsc then arsc.zip 
                                         else " "),
                                  input-output apr-error,
                                  input-output apr-msg).
             if apr-error = yes then
               leave.
           end. /* each oeelb */
           
           if apr-error = yes then
             do:
             /* Authpoint Custom for Sauer APR */
             run authchk.p(g-cono,
                           g-operinit,
                           "oeexq":U,
                           "lines":U,
                           "SDISauer":U,
                           "c":U,
                           "":U,
                           output zsdiconfirm).
             /* Auth */
             hide message no-pause.
             /* Auth */
             if zsdiconfirm = no then
               do:
               message color normal apr-msg.
               bell.
               next-prompt oeehb.shipto with frame f-oeexqcnt.
               next.
             end.
           end. /* if apr-error = yes */
/* 10-23-12 tah APR fix */           
           
           run oeexqupdt.p.
           run Create_Addons.
           display
             oeehb.shiptonm  
             oeehb.shipto  
             oeehb.shiptoaddr[1] 
             oeehb.shiptoaddr[2] 
             oeehb.shiptocity
             oeehb.shiptost   
             oeehb.shiptozip
             oeehb.slsrepout    /* das */
             oeehb.slsrepin     /* das */
             oeehb.shipviaty
             oeehb.shipinstr
             oeehb.orderdisp
             oeehb.inbndfrtfl 
             oeehb.outbndfrtfl
           with frame f-oeexqcnt.
 put screen row 13 col 3 color 
           message 
 " F7-Line Maintenance   F8-Vendor/Whse                                      ".
         end. /* if frame-field = oeehb.shipto */
         
         if frame-field = "whse" and 
            oeehb.whse <> input oeehb.whse then
           do:
           
           assign v-bodexists = no.
           for each oeelb where oeelb.cono    = g-cono and
                                oeelb.batchnm = oeehb.batchnm and
                                oeelb.seqno   = oeehb.seqno
                                no-lock:
             if substr(oeelb.user5,1,1) = "y" then
               do:
               assign v-bodexists = yes.
               leave.
             end. /* if BOD exists */
           end. /* for each oeelb */
           if v-bodexists = yes then
             do:
             message "A line with a BOD exists.  Warehouse Cannot be Changed".
             next-prompt oeehb.whse with frame f-oeexqcnt.
             next.
           end. /* if v-bodexists */
         end. /* if frame-field = "whse" */
          
         /*EN-1808*/
         if frame-field = "reqshipdt" then
           do:
           if input oeehb.reqshipdt < today then
             do:
      message "Check Req Ship Date. Can't be less than today. Please correct".
             next-prompt oeehb.reqshipdt with frame f-oeexqcnt.
             next.
           end.
           assign var-inputpd     = input oeehb.reqshipdt
                  p-reqshipdt     = input oeehb.reqshipdt
                  oeehb.reqshipdt = var-inputpd
                  v-1019fl        = no.
           run zsdiweekendh (input-output var-inputpd,
                             input-output v-1019fl,
                             input oeehb.whse,
                             input g-cono,
                             input oeehb.divno).
           /*EN-437 - Scenario 3*/
           if p-reqshipdt <> var-inputpd then
             do:
             for each oeelb where oeelb.cono = g-cono and
                                  oeelb.batchnm = oeehb.batchnm and
                                  oeelb.seqno   = oeehb.seqno:
               if oeelb.promisedt < var-inputpd then
                 assign oeelb.promisedt = var-inputpd
                        oeelb.xxda1     = var-inputpd.
             end.
           end.
           /*EN-437*/
             
           if v-1019fl = yes then
             do:
             assign oeehb.reqshipdt = var-inputpd.
             display oeehb.reqshipdt with frame f-oeexqcnt.
             assign v-1019fl = no.
             run err.p(1019).
             next-prompt oeehb.refer with frame f-oeexqcnt.
             next.
           end.
         end. /* frame-field = "reqshipdt" */
         
         if frame-field = "refer" then do:
           if input oeehb.refer = "Credit Card Order" then
              assign oeehb.termstype = "crcd".
         end. 
          
         if frame-field = "v-contact" then
           do:
           if input v-contact <> hx-contact then
             do:
             /* if user adds contact at conversion, populate the placedby,
                but don't change original contact info
             overlay(oeehb.user3,1,20)  = string(v-contact,"x(20)").
             overlay(oeehb.user3,21,12) = string(v-phone,"x(12)").  
             overlay(oeehb.user3,33,20) = string(v-email,"x(20)").  
             */
             assign oeehb.placedby      = input v-contact.
           end.
         end. /* frame-field = "v-contact" */
          
         if frame-field = "shipviaty" and
           oeehb.shipviaty <> input oeehb.shipviaty then 
           do:
           if not can-find (first sasta where sasta.cono = g-cono and 
                                              sasta.codeiden = "s" and
                                              sasta.codeval =
                                                input oeehb.shipViaty
                                              no-lock) then 
             do:
             run err.p (4030).
             next-prompt oeehb.shipviaty with frame f-oeexqcnt.
             next.
           end. /* if not can-find sasta */

           run zsdifrtlock.p (input oeehb.custno,
                              input (oeehb.shipto),
                              input "shipvia",
                              input (input oeehb.shipViaty),
                              input-output oeehb.InBndFrtfl,
                              input-output oeehb.OutBndFrtfl,
                              input-output zx-errorty).
           if zx-errorty = 0 then
             assign o-InBndFrtfl  = oeehb.InBndFrtFl 
                    o-OutBndFrtfl = oeehb.OutBndFrtfl
                    o-shipvia     = input oeehb.shipviaty
                    oeehb.shipviaty = input oeehb.shipviaty.

           display
             oeehb.shipviaty
             oeehb.InBndFrtfl
             oeehb.OutBndFrtfl
           with frame f-oeexqcnt.
 put screen row 13 col 3 color 
           message 
 " F7-Line Maintenance   F8-Vendor/Whse                                      ".
         end. /* frame-field = "v-shipviaty */
         
         if frame-field = "transtype" and
            input  oeehb.transtype  = "do" and
           (can-do("9,13,401,501,502,503,504,507,508",string(lastkey)) or
           /*({k-accept.i} or lastkey = 13 or lastkey = 9 or*/ 
             can-do("f7,f8",keylabel(lastkey)) or
            (frame-field = "geocd" and keyfunction(lastkey) = "return") ) 
            then
           do:
           if can-find (first oeelb where 
                              oeelb.cono    = g-cono and
                              oeelb.batchnm = oeehb.batchnm and
                              oeelb.seqno   = 1 and
                              oeelb.specnstype <> "L" and
                              substr(oeelb.user5,1,1) = "y" and
                              substr(oeelb.user5,2,3) ne "yes" no-lock)
              then do:
              run err.p (5701).
              next-prompt oeehb.transtype with frame f-oeexqcnt.
              next.
           end.
         end. /* transtkype */
         
         if frame-field = "orderdisp" and (lastkey =   9 or
                                           lastkey =  13 or
                                           lastkey = 401 or
                                           lastkey = 502 or
                                           lastkey = 508) then
           do:
           assign oeehb.orderdisp = input oeehb.orderdisp.
           if oeehb.orderdisp = "S" then
             do:
             for each oeelb where oeelb.cono = g-cono and
                                  oeelb.batchnm = oeehb.batchnm and
                                  oeelb.seqno   = oeehb.seqno:
               assign oeelb.botype = "N".
             end. /* each oeelb */
           end. /* orderdisp = "s" */
           else
             do:
             for each oeelb where oeelb.cono = g-cono and
                                  oeelb.batchnm = oeehb.batchnm and
                                  oeelb.seqno   = oeehb.seqno:
               /**
               find icsw where icsw.cono = g-cono and
                               icsw.whse = substr(oeelb.user4,1,4) and
                               icsw.prod = oeelb.shipprod
                               no-lock no-error.
               if avail icsw and icsw.statustype <> "x" 
                             and oeelb.botype     = "N" then
               **/
               if oeelb.botype = "n" then
                 assign oeelb.botype = "y".
             end. /* each oeelb */
           end. /* orderdisp <> "s" */
         end. /* orderdisp */
         
         if frame-field = "slsrepout" and          
            hx-slsrepout <> input oeehb.slsrepout then
           do:
           for each oeelb where oeelb.cono = g-cono and
                                oeelb.batchnm = oeehb.batchnm and
                                oeelb.seqno   = oeehb.seqno:
             assign oeelb.slsrepout = input oeehb.slsrepout.
           end.
         end.
         if frame-field = "slsrepin" and
            hx-slsrepin <> input oeehb.slsrepin then
           do:
           for each oeelb where oeelb.cono = g-cono and
                                oeelb.batchnm = oeehb.batchnm and
                                oeelb.seqno   = oeehb.seqno:
             assign oeelb.slsrepin = input oeehb.slsrepin.
           end.
         end.
         if (frame-field = "custpo" and
           can-do("9,13,401,501,502,503,504,507,508",string(lastkey))) or
           LASTKEY = 401 or
           LASTKEY = KEYCODE("F7") or LASTKEY = KEYCODE("F8") or
           (FRAME-FIELD = "geocd" and LASTKEY = 13) 
           then
           do:
           if input oeehb.custpo <> " " then
             assign oeehb.custpo = input oeehb.custpo.
           if oeehb.custpo ne "" then do:
             find arsc where arsc.cono   = g-cono and
                             arsc.custno = oeehb.custno no-lock no-error.
             find last oeeh use-index k-custpo where
                       oeeh.cono = g-cono and
                       oeeh.custpo = input oeehb.custpo and
                       oeeh.custno = arsc.custno
                       no-lock no-error.
             if avail oeeh then
               do:
               if arsc.xxl13 = no then 
                 do: /* SX 55 */
                 message "Duplicate Customer PO's not allowed for this ARSC".
                 next-prompt oeehb.custpo with frame f-oeexqcnt.
                 next.
               end. 
               if arsc.xxl13 = yes and v-oefoundfl = no then /* SX 55 */
                 do:
                 message color normal
                 "Warning - Duplicate Customer PO Found " string(oeeh.orderno)
                  "-" string(oeeh.ordersuf,"99").
                  assign v-oefoundfl = yes.
                 pause.
               end. /* allow dups = yes */
             end. /* oeeh found */
             else
               do:
               find last L-oeehb where 
                         L-oeehb.cono     = g-cono and
                         L-oeehb.custpo   = oeehb.custpo and
                         L-oeehb.custpo  <> " " and
                         L-oeehb.custno   = oeehb.custno and
                         L-oeehb.batchnm <> oeehb.batchnm
                         no-lock no-error.
               if avail L-oeehb and v-oefoundfl = no then 
                 do:
                 message color normal                                        
              "Warning- Duplicate Customer PO Found on Quote " L-oeehb.batchnm.
                 assign v-oefoundfl = yes.
                 pause.
               end. /* avail oeehb */
             end. /* if oeeh not avail, check oeehb */
           end. /* custpo <> blank */
         end. /* frame-field = custpo and movement key pressed */
         
        /********/
        /* das - Shipvia Edit Checks */
        if LASTKEY = 401 or
           LASTKEY = KEYCODE("F7") or LASTKEY = KEYCODE("F8") or
          (FRAME-FIELD = "shipviaty" and 
           can-do("13,9,509,503,504,21,1070,1068,505,506",string(lastkey))) or
          (FRAME-FIELD = "geocd" and LASTKEY = 13) then 
          do:
          assign oeehb.shipviaty = input oeehb.shipviaty.
          find sasta where sasta.cono = g-cono  and
                           sasta.codeiden = "s" and
                           sasta.codeval = oeehb.shipviaty and
                           sasta.usagefl = yes and
                           sasta.warrexchgfl = no and
                          (sasta.whse = "upsc" or
                           sasta.whse = "fdxc")
                           no-lock no-error.
          if avail sasta then
            do:
            assign z-shiperr = no.
            assign oeehb.shipinstr = input oeehb.shipinstr.
            if (substr(oeehb.shipinstr,1,1) >= "0" and  
                substr(oeehb.shipinstr,1,1) <= "9") or  
               (substr(oeehb.shipinstr,2,1) >= "0" and  
                substr(oeehb.shipinstr,2,1) <= "9") or  
               (substr(oeehb.shipinstr,3,1) >= "0" and  
                substr(oeehb.shipinstr,3,1) <= "9") or  
               (substr(oeehb.shipinstr,4,1) >= "0" and  
                substr(oeehb.shipinstr,4,1) <= "9") or  
               (substr(oeehb.shipinstr,5,1) >= "0" and  
                substr(oeehb.shipinstr,5,1) <= "9") or
               (substr(oeehb.shipinstr,6,1) >= "0" and  
                substr(oeehb.shipinstr,6,1) <= "9") then
              assign z-shiperr = no.
            else
              assign z-shiperr = yes.
            if z-shiperr = no then
              do:
              do idx = 1 to 6:
                if substr(oeehb.shipinstr,idx,1) = " " then
                  do:
                  assign z-shiperr = yes.
                  leave.
                end.
              end.
            end.
            if z-shiperr = no then
              do:
              /* UPSCollect must have the 7th possition blank */
              if sasta.whse = "UPSC" and 
                 substr(oeehb.shipinstr,7,1) <> " " then
                 assign z-shiperr = yes.
             end.
             if z-shiperr = no then
               do:
               /* FDXCollect can be 7 or 9 digits long. 
                  The 8th or 10th pos s/b blank */
               if sasta.whse = "FDXC" then
                 do:
                 if (substr(oeehb.shipinstr,8,1)  = " " and
                     substr(oeehb.shipinstr,7,1) <> " ") or
                    (substr(oeehb.shipinstr,9,1)  = " " and
                     substr(oeehb.shipinstr,8,1) <> " ") or
                    (substr(oeehb.shipinstr,10,1) = " " and
                     substr(oeehb.shipinstr,9,1) <> " ") or
                    (substr(oeehb.shipinstr,11,1) = " " and
                     substr(oeehb.shipinstr,10,1) <> " ") then
                   assign z-shiperr = no.
                 else
                   assign z-shiperr = yes.
               end. /* FDXC */
             end. /* z-shiperr = no */

             if z-shiperr = yes then
               do:
               /*
               assign oeehb.shipviaty = if avail sasta then sasta.descrip
                                    else " ".
               display oeehb.shipviaty with frame f-oeexqcnt.
               */
             message "Ship Instructions must include complete Collect Acct#," +
                     " beginning in 1st position".
               next-prompt oeehb.shipinstr with frame f-oeexqcnt.
               next.
             end.
             /*
             else
               hide message no-pause.
             */
          end. /* avail sastt */
        end. /* movement key was entered */
  

        if LASTKEY = 401 or
           LASTKEY = KEYCODE("F7") or LASTKEY = KEYCODE("F8") or
          (FRAME-FIELD = "outbndfrtfl" and 
           can-do("13,9,509,503,504,21,1070,1068,505,506",string(lastkey))) 
           or
          (FRAME-FIELD = "inbndfrtfl" and 
           can-do("13,9,509,503,504,21,1070,1068,505,506",string(lastkey))) 
           or   
           (FRAME-FIELD = "geocd" and LASTKEY = 13) then 
           do:  
          if oeehb.reqshipdt < today then
            do:
      message "Check Req Ship Date. Can't be less than today. Please correct".
            next-prompt oeehb.reqshipdt with frame f-oeexqcnt.
            next.
          end.

          /**** das - moved - only done if shipto state is changed
          for each oeelb where oeelb.cono = g-cono and
                   oeelb.batchnm = oeehb.batchnm and
                   oeelb.seqno   = oeehb.seqno
                   no-lock:
            run SauerAPRcheck.p (input oeelb.prodcat,
                                 input oeehb.shiptost,
                                 input oeehb.shiptozip,
                                 input-output apr-error,
                                 input-output apr-msg).
            if apr-error = yes then
              leave.
          end. /* each oeelb */
          if apr-error = yes then
            do:
            /* Authpoint Custom for Sauer APR */
            run authchk.p(g-cono,
                          g-operinit,
                          "oeexq":U,
                          "lines":U,
                          "SDISauer":U,
                          "c":U,
                          "":U,
                          output zsdiconfirm).
            /* Auth */
            hide message no-pause.
            /* Auth */
            if zsdiconfirm = no then
              do:
              message color normal apr-msg.
              bell.
              next-prompt oeehb.shiptost with frame f-oeexqcnt.
              next.
            end.
          end. /* if apr-error = yes */
          *****/
          
          
          assign oeehb.outbndfrtfl = input oeehb.outbndfrtfl
                 oeehb.inbndfrtfl  = input oeehb.inbndfrtfl.
         

          
          if oeehb.outbndfrtfl = yes then
            do:
            find sasta where sasta.cono = g-cono  and
                             sasta.codeiden = "s" and
                             sasta.codeval = oeehb.shipviaty and
                             sasta.usagefl = yes and
                             sasta.warrexchgfl = no and
                            (sasta.whse = "upsc" or
                             sasta.whse = "fdxc")
                             no-lock no-error.
            if avail sasta then
              do:
              message 
             "Outbound Freight must be 'NO' when UPS/FEDX Collect is assigned".
              next-prompt oeehb.outbndfrtfl with frame f-oeexqcnt.
              next.
            end. /* collect shipvia found */
          end. /* outbndfrtfl = yes */
         /* end. /* keystrokes */  */
   
                 
         else if frame-field = "outbndfrtfl" then do:

           if o-OutBndFrtfl <> input oeehb.outbndfrtfl and
              oeehb.outbndfrtfl = no 
              then do:

               run zsdifrtlock.p (input arsc.custno,
                                  input oeehb.shipto,
                                  input "frtfl",
                                  input oeehb.shipvia,
                                  input-output oeehb.inbndfrtfl,
                                  input-output oeehb.outbndfrtfl,
                                  input-output zx-errorty).
   
                if zx-errorty <> 0 then
                do:
                 run err.p (1132).
                 next-prompt oeehb.outbndfrtfl with frame f-oeexqcnt.
                 next.
                end.                          
   
             RUN popup (OUTPUT exceptCode).
             APPLY "FOCUS" TO oeehb.outbndfrtfl in frame f-oeexqcnt.

             If exceptCode = " " then do:
               if {k-cancel.i} or keylabel(lastkey) = "PF11" then do:
                assign oeehb.outbndfrtfl = yes. 
                display oeehb.outbndfrtfl with frame f-oeexqcnt.
                next-prompt oeehb.outbndfrtfl with frame f-oeexqcnt. 
                next.
               end. /* added */ 
             end.
             else do:
               run popupcomm (OUTPUT v-comment).
               APPLY "FOCUS" TO oeehb.outbndfrtfl in frame f-oeexqcnt.
               if v-comment = " " then do:
                 /* message "Comment is Required". pause.  */
                 assign oeehb.outbndfrtfl = yes.
                 display oeehb.outbndfrtfl with frame f-oeexqcnt.
                 next-prompt oeehb.outbndfrtfl with frame f-oeexqcnt.
                 next.
               end.  /* v-comment = "" */   

               find first m-oeehb where m-oeehb.cono = g-cono and
                                        m-oeehb.custno = oeehb.custno and
                                        m-oeehb.batchnm = oeehb.batchnm
                                        no-error.
               if avail m-oeehb then do: 
                overlay(m-oeehb.user10,11,19) = g-operinits + " " +   
                                                         exceptCode.
                overlay(m-oeehb.user10,50,10) = string(today,"99/99/99").      
                overlay(m-oeehb.user11,45,20) = v-comment.

                  next-prompt oeehb.orderdisp with frame f-oeexqcnt. 
                  next. 
               end. /* if avail */   
             end.  /* else do  */
         end. /* <> */    
         end. /* no */
       
       if /* LASTKEY = 401 or
          LASTKEY = KEYCODE("F7") or LASTKEY = KEYCODE("F8") or  */
          (FRAME-FIELD = "inbndfrtfl" and 
           can-do("13,9,509,503,504,21,1070,1068,505,506",string(lastkey))) 
           then do:
         
                    
           if o-inBndFrtfl <> input oeehb.inbndfrtfl and
              oeehb.inbndfrtfl = no 
              then do:

            
               run zsdifrtlock.p (input arsc.custno,
                                  input oeehb.shipto,
                                  input "frtfl",
                                  input oeehb.shipvia,
                                  input-output oeehb.inbndfrtfl,
                                  input-output oeehb.outbndfrtfl,
                                  input-output zx-errorty).
   
                if zx-errorty <> 0 then
                do:
                 run err.p (1132).
                 next-prompt oeehb.inbndfrtfl with frame f-oeexqcnt.
                 next.
                end.                          
   
             RUN popup(OUTPUT exceptCode).
             APPLY "FOCUS" TO oeehb.inbndfrtfl in frame f-oeexqcnt.

             If exceptCode = " " then do:
              if {k-cancel.i} or keylabel(lastkey) = "PF11" then do:
                assign oeehb.inbndfrtfl = yes. 
                display oeehb.inbndfrtfl with frame f-oeexqcnt.
                next-prompt oeehb.inbndfrtfl with frame f-oeexqcnt. 
                next.
              end. /* added */  
             end.
             else do:
               run popupcomm (OUTPUT v-comment).
               APPLY "FOCUS" TO oeehb.inbndfrtfl in frame f-oeexqcnt.

               if v-comment = " " then do:
                 /* message "Comment is Required". pause.  */
                 assign oeehb.inbndfrtfl = yes.
                 display oeehb.inbndfrtfl with frame f-oeexqcnt.
                 next-prompt oeehb.inbndfrtfl with frame f-oeexqcnt.
                 next.
               end.  /* v-comment = "" */   

               find first m-oeehb where m-oeehb.cono = g-cono and
                                        m-oeehb.custno = oeehb.custno and
                                        m-oeehb.batchnm = oeehb.batchnm
                                        no-error.
               if avail m-oeehb then do: 
                overlay(m-oeehb.user10,1,9) = g-operinits + " " +   
                                                         exceptCode.
                overlay(m-oeehb.user10,40,10) = string(today,"99/99/99").
                overlay(m-oeehb.user11,20,20) = v-comment.
                  next-prompt oeehb.outbndfrtfl with frame f-oeexqcnt. 
                  next. 
               end. /* if avail */   
             end.  /* else do  */
           end. /* <> */    
         end. /* no */
       
        
        end. /* keystrokes */

        
        
        /****************/
         
        
               assign 
               oeehb.shiptonm      = input oeehb.shiptonm      /* das 7/5/07 */
               oeehb.shiptoaddr[1] = input oeehb.shiptoaddr[1] /* das 7/5/07 */
               oeehb.shiptoaddr[2] = input oeehb.shiptoaddr[2] /* das 7/5/07 */
               oeehb.shiptocity    = input oeehb.shiptocity
               oeehb.shiptost      = input oeehb.shiptost
               oeehb.shiptozip     = input oeehb.shiptozip
               oeehb.geocd         = input oeehb.geocd.
         /* das - always check for the following info - begin */
         /* run oeexqupdt.p. */
         /* das - always check for the following info - end */

       end. /* accept */
         
       apply lastkey.
/*
       assign oeehb.shiptocity = input oeehb.shiptocity
              oeehb.shiptost   = input oeehb.shiptost
              oeehb.shiptozip  = input oeehb.shiptozip
              oeehb.geocd      = input oeehb.geocd.
*/      
      end. /* edit loop */
  
  if {k-accept.i} or 
    (frame-field = "geocd" and keyfunction(lastkey) = "return") then
    do:
     overlay(oeehb.user3,1,20) = v-contact.
     overlay(oeehb.user3,53,8) = oeehb.batchnm.
     assign oeehb.promisedt =  oeehb.reqshipdt.
     {w-arsc.i oeehb.custno no-lock "xi-"}
    /*d Customer PO # is required */
     if xi-arsc.poreqfl and oeehb.custpo = ""  and
       not can-do("qu,cr,rm",oeehb.transtype) then do:
       run err.p (5681).
       next-prompt oeehb.custpo with frame f-oeexqcnt.
       next conversionloop.
     end.

     if oeehb.termstype = "cod"
      /* per Tami, should only happen when cod, not cia or wxfr */
      /*(oeehb.termstype = "cod" or 
         oeehb.termstype = "cia" or
         oeehb.termstype = "wxfr")*/ and
         oeehb.transtype = "do" then do:
       run err.p (6304).
       next-prompt oeehb.transtype with frame f-oeexqcnt.
       next conversionloop.
    end.
 
    if xi-arsc.shipreqfl and oeehb.shipto = "" then do:
       run err.p (5690).
       next-prompt oeehb.shipto with frame f-oeexqcnt.
       next conversionloop.
    end.
     
    if oeehb.orderdisp ne "" and not can-do("w,t,s,j",oeehb.orderdisp) then do:
      run err.p (3621).
      next-prompt oeehb.orderdisp with frame f-oeexqcnt.
      next conversionloop.
      end.
 
    for each oeelb where oeelb.cono = g-cono and
                         oeelb.batchnm = oeehb.batchnm and
                         oeelb.seqno   = oeehb.seqno   and
                         oeelb.specnstype <> "L" and
                         oeelb.specnstype <> "N"
                         no-lock:   
      find icsp where icsp.cono = g-cono and
                      icsp.prod = oeelb.shipprod
                      no-lock no-error.
      if avail icsp and icsp.statustype = "I" then
        do:
        message "Line" oeelb.lineno "is inactive. See Buyer to Activate".
        next-prompt oeehb.shipto with frame f-oeexqcnt.
        next conversionloop.
      end.
      find icsw where icsw.cono = g-cono and
                      icsw.prod = oeelb.shipprod and
                      icsw.whse = substr(oeelb.user4,1,4)
                      no-lock no-error.
      if avail icsw and icsw.statustype = "x" then
        do:
        message "Line" oeelb.lineno "is Do Not Reorder. See Buyer to Activate".
        next-prompt oeehb.shipto with frame f-oeexqcnt.
        next conversionloop.
      end.
    end. /* for each oeelb */
      
    /***** das - removed - only performed if shiptost is changed 
    for each oeelb where oeelb.cono = g-cono and
             oeelb.batchnm = oeehb.batchnm and
             oeelb.seqno   = oeehb.seqno
             no-lock:
      run SauerAPRcheck.p (input oeelb.prodcat,
                           input oeehb.shiptost,
                           input oeehb.shiptozip,
                           input-output apr-error,
                           input-output apr-msg).
      if apr-error = yes then
        leave.
    end. /* each oeelb */
    if apr-error = yes then
      do:
      /* Authpoint Custom for Sauer APR  */
      run authchk.p(g-cono,
                    g-operinit,
                    "oeexq":U,
                    "lines":U,
                    "SDISauer":U,
                    "c":U,
                    "":U,
                    output zsdiconfirm).
      /* Auth */
      hide message no-pause.
      /* Auth */
      if zsdiconfirm = no then
        do:
        message color normal apr-msg.
        bell.
        next-prompt oeehb.shiptost with frame f-oeexqcnt.
        next.
      end.
    end. /* if apr-error = yes */
    *****/
    
    /*** das - replaced with the above logic ***
    run oeexqSauerAPR.p (input "L").
    hide message no-pause.
    if zsdiconfirm = no then
      do:
      message color normal apr-msg.
      bell.
      next-prompt oeehb.shiptost with frame f-oeexqcnt.
      next.
    end. /* zsdiconfirm = no */
    ***/
   if not can-find (first smsn where smsn.cono = g-cono and 
                                      smsn.slsrep = oeehb.slsrepout
                                       no-lock) then do:
      run err.p (4604).
      next-prompt oeehb.slsrepout with frame f-oeexqcnt.
      next conversionloop.
      end.
 
   if not can-find (first smsn where smsn.cono = g-cono and 
                                      smsn.slsrep = oeehb.slsrepin
                                       no-lock) then do:
      run err.p (4604).
      next-prompt oeehb.slsrepin with frame f-oeexqcnt.
      next conversionloop.
      end.

    
    if not can-find (first sasta where sasta.cono = g-cono and 
                                      sasta.codeiden = "s" and
                                      sasta.codeval = oeehb.shipVia
                                      no-lock) then do:
      run err.p (4030).
      next-prompt oeehb.shipvia with frame f-oeexqcnt.
      next conversionloop.
      end.

    if o-shipvia <> oeehb.shipvia then
      do:
      run zsdifrtlock.p (input oeehb.custno,
                         input oeehb.shipto,
                         input "shipvia",
                         input (oeehb.shipVia),
                         input-output oeehb.InBndFrtfl,
                         input-output oeehb.OutBndFrtfl,
                         input-output zx-errorty).
      assign o-InBndFrtfl  = oeehb.InBndFrtFl 
             o-OutBndFrtfl = oeehb.OutBndFrtfl
             o-shipvia     = oeehb.shipvia .
      end.
    
    if o-InBndFrtfl <> oeehb.InBndFrtFl or
       o-OutBndFrtfl <> oeehb.OutBndFrtfl then do:
      
      
      run zsdifrtlock.p (input oeehb.custno,
                         input oeehb.Shipto,
                         input "FrtFl",
                         input (oeehb.shipVia),
                         input-output oeehb.InBndFrtfl,
                         input-output oeehb.outBndFrtfl,
                         input-output zx-errorty).

      if zx-errorty <> 0 then do:
        message "Cannot Change the Freight Flags".
        assign oeehb.InBndFrtfl  = o-InBndFrtfl
               oeehb.OutBndFrtfl = o-OutBndFrtfl.
        next-prompt oeehb.ShipVia with frame f-oeexqcnt.
        next conversionloop.
      end.  
    
    end.

    
    
    
    if oeehb.custpo ne "" then do:
      find arsc where arsc.cono = g-cono and
                      arsc.custno = oeehb.custno no-lock no-error.
      if can-find(first oeeh use-index k-custpo where
                        oeeh.cono = g-cono and
                        oeeh.custpo = oeehb.custpo and
                        oeeh.custno = arsc.custno) then do:
         if arsc.xxl13 = no then do: /* SX 55 */
           message "Duplicate Customer PO's not allowed for this ARSC".
           next-prompt oeehb.custpo with frame f-oeexqcnt.
           next conversionloop.
         end. 
      end.
    end.
  
    /* das - 03/18/09 - don't allow Direct WT Whse be the same as the Banner */
    if input oeehb.transtype = "do" and
      entry(1,oeehb.user4,"^") = "t" and
      entry(9,oeehb.user4,"^") = input oeehb.whse then
      do:
      run err.p (5885).
      next-prompt oeehb.whse with frame f-oeexqcnt.
      next conversionloop.
    end.
  
  /** taxware stuff **/
  /*  Force correct taxing information */
  /*d If an error occurred, we need to force the user to enter valid
                            ship to data.  Currently the only errors being                                   trapped are:
                    1  - non-numeric zip code
                    3  - invalid state code
                    6  - zip code not in range for specified state
                    95 - Taxware could not deterimine taxing jurisdiction. */

    assign
      v-shiptost    = caps(oeehb.shipToSt)
      v-shiptocity  = caps(oeehb.shipToCity)
      v-shiptozip   = caps(oeehb.shipToZip)
      v-shiptogeo   = oeehb.geoCd.
    if oeehb.transtype = "CS":u or oeehb.orderDisp = "W":u
      then do:
      find xi-icsd where xi-icsd.cono = g-cono and 
                         xi-icsd.whse = oeehb.whse no-lock no-error.
      if avail xi-icsd then
        assign
          v-shiptost    = caps(xi-icsd.state)
          v-shiptocity  = caps(xi-icsd.city)
          v-shiptozip   = caps(xi-icsd.zipcd)
          v-shiptogeo   = xi-icsd.geocd.
    end. /* if CS or will call */
    
    run oeehbtirate.p (input  v-shiptocity,
                       input  "",
                       input  v-shiptost,
                       input  v-shiptozip,
                       input  v-shiptogeo,
                       input  "A",
                       input  100.00,
                       output garbageout[1],
                       output garbageout[2],
                       output garbageout[3],
                       output garbageout[4],
                       output garbageout[5],
                       output garbageout[6],
                       output v-tax010errorno).

    if  can-do("1,3,6,95":u,string(v-tax010errorno)) then do:
    /*d City, State, Zip are Not Valid for Tax Purposes */
      run err.p(2214). 
      assign v-tax010errorno = 0.
      next-prompt oeehb.shiptocity with frame f-oeexqcnt.
      next conversionloop.  
    end. 
    if v-tax010errorno = 9999 then do:
      if oeehb.shipto <> "" then do:
        if can-find (arss use-index k-arss where
                     arss.cono   = g-cono      and
                     arss.custno = oeehb.custno and
                     arss.shipto = oeehb.shipto and
                     can-do("US,,CA":u, arss.countrycd)) then do:
          run warning.p (8772).
          v-tax010errorno = 0.
          next-prompt oeehb.shiptocity with frame f-oeexqcnt.
          next conversionloop.  
          end.
      end. /* Ship To exists on OE order */
      else if can-find (arsc use-index k-arsc where
                        arsc.cono   = g-cono      and
                        arsc.custno = oeehb.custno and
                        can-do("US,,CA":u, arsc.countrycd)) then do:
        run warning.p (8772).
        v-tax010errorno = 0.
        next-prompt oeehb.shiptocity with frame f-oeexqcnt.
        next conversionloop.  
        end.
      end. /* unknown address */
  /* end of taxware stuff */  

 
  overlay(oeehb.user3,1,20) = v-contact.
  overlay(oeehb.user3,53,8) = oeehb.batchnm.

  assign oeehb.promisedt =  oeehb.reqshipdt.  
 
  if substr(oeehb.user5,1,1) <> "" or oeehb.countrycd = "lb" then
    do:
    run oeexq92.p.
  end.

  end. /* f-accept.i */                                                     

  /* .. */
  
  if oeehb.transtype = "do" and keylabel(lastkey) = "f8" then
    do:
    assign x-returncode = ""
           g-oetype = oeehb.transtype.
    run oeexqrelchk.p  (input xi-oeehbrecid, 
                        input-output ip-update,
                        input ?,
                        input ?,
                        input "Hdocheck",
                        input-output x-returncode). 
    readkey pause 0.                         
    assign ip-update = true.
    if not s-canceled and x-returncode <> "back" then
      assign v-powtintfllocal = true.
    next conversionloop.
    end.

  if keylabel(lastkey) = "f7" then
    do:
    hide frame f-oeexqcnt.
    display  
      oeehb.batchnm    
      oeehb.custno     
      oeehb.transtype  
      oeehb.shipto    
      oeehb.whse      
      with frame f-lineconvert.
    assign v-loadbrowseline = false
           v-lmode = "c".
    on cursor-up cursor-up.   
    on cursor-down cursor-down.
    run Middle1.
                  
    run Clear-Frames.

    hide frame f-lineconvert.
    hide frame f-oeexqcnt.
    hide frame f-lines.

    put screen row 13 col 3 color 
     message 
 " F7-Line Maintenance   F8-Vendor/Whse                                      ".

    on cursor-up back-tab.   
    on cursor-down tab.
    next conversionloop.
    end.

  
  
  if keylabel(lastkey) = "f7" or
     keylabel(lastkey) = "f8" then
    next conversionloop. 

  /* .. */
  
  
  
  
  
  leave conversionloop.
  end.   /* Conversionloop  */

  if substr(oeehb.user3,53,2) = "CR" or
     substr(oeehb.user3,53,2) = "CQ" or
     substr(oeehb.user3,53,2) = " " then
    overlay(oeehb.user3,53,8) = oeehb.batchnm.

  assign ip-update = false
         x-returncode = "".
   
  run oeexqrel.p (input recid(b-oeehb),input-output ip-update,
                  input-output x-returncode).

  if ip-update then 
    do:
     run oeexqrelu.p (input recid(oeehb),       /********/
                      input-output ip-orderno).
  end.
  else
  if x-returncode = "back" then
    next  ReleaseLoop.

  leave ReleaseLoop.  
    


end.    /*  ReleaseLoop */ 
assign v-conversion = false.

run Clear-Frames.
hide frame f-lineconvert.
hide frame f-oeexqcnt.
hide frame f-lines.


end.

 


/*****************************************************************************/
  PROCEDURE PopUp:
/*****************************************************************************/
  
  DEFINE VAR inTitle AS CHAR FORMAT "x(15)" NO-UNDO.
  DEFINE QUERY q-reason FOR zsastz SCROLLING.
  DEFINE OUTPUT PARAMETER exceptCode AS CHAR NO-UNDO.
  
  
  
  DEFINE BROWSE b-reason QUERY q-reason
  DISPLAY 
    zsastz.primarykey FORMAT "x(4)"
    zsastz.secondarykey FORMAT "x(24)"
  WITH 12 DOWN WIDTH 30 OVERLAY NO-HIDE NO-LABELS. /* NO-BOX.  */
  
  FORM
    inTitle  AT 1 DCOLOR 2
    b-reason AT 1
  WITH FRAME f-reason WIDTH 30 NO-LABELS OVERLAY NO-BOX.
  
  ASSIGN 
    inTitle = "FrtFlags Exceptions".
  
  ON any-key OF b-reason IN FRAME f-reason DO:
    IF {k-accept.i} OR {k-return.i} THEN DO:
      IF avail zsastz THEN
        ASSIGN exceptCode = zsastz.primarykey.
      ELSE
        ASSIGN exceptCode = "".
      HIDE FRAME f-reason.
      APPLY "window-close" TO FRAME f-reason.
    END.
    IF {k-cancel.i} THEN DO:
      ASSIGN exceptCode = "".
      HIDE FRAME f-reason.
      APPLY "window-close" TO FRAME f-reason.
    END.
  END.
  ON cursor-up cursor-up.
  ON cursor-down cursor-down.
  
  OPEN QUERY q-reason FOR EACH zsastz WHERE zsastz.cono = g-cono AND
                               zsastz.codeiden = "FrtFlagsExceptions" AND
                               zsastz.labelfl = NO
  NO-LOCK BY codeiden BY primarykey BY secondarykey.
  CLEAR FRAME f-reason.
  ENABLE b-reason WITH FRAME f-reason.
  APPLY "ENTRY" TO b-reason IN FRAME f-reason.
  DISPLAY inTitle WITH FRAME f-reason.
/*   PUT SCREEN ROW 13 COLUMN 1 "                    ".  */
  WAIT-FOR window-close OF FRAME f-reason.        
  CLOSE QUERY q-reason.
  APPLY "FOCUS" TO oeehb.inbndfrtfl in frame f-oeexqcnt.
  ON cursor-up back-tab.
  ON cursor-down tab.
  
END. /* procedure popup */

/********************************/
PROCEDURE PopUpComm:
/********************************/

  DEFINE VAR inTitle AS CHAR FORMAT "x(20)" NO-UNDO.
  DEFINE OUTPUT PARAMETER v-comment AS CHAR FORMAT "x(20)" NO-UNDO.
  
  
  
  FORM
    inTitle  AT 1 DCOLOR 2  
    v-comment AT 1  
  WITH FRAME f-reason2 WIDTH 20 row 8 NO-LABELS OVERLAY NO-BOX.
  
  assign inTitle = "Comments".
  display inTitle with frame f-reason2.
  UPDATE 
  v-comment with frame f-reason2 
  
  editing:
    READKEY.
    
    IF {k-accept.i} OR {k-return.i} THEN DO:
      assign v-comment = input v-comment.
      IF v-comment <> "" THEN DO:
         CLEAR FRAME F-REASON2.
         HIDE FRAME F-REASON2.
         return.
         /* APPLY "WINDOW-CLOSE" TO FRAME F-REASON2.  */
      END.   
      ELSE do:
        ASSIGN v-comment = "".
        message "Comment Required". pause.
        next-prompt v-comment with frame f-reason2.
        next.
        /* message "Comment Required".   
        CLEAR FRAME F-REASON2.
        HIDE FRAME f-reason2.
        APPLY "window-close" TO FRAME f-reason2.   */
      end. /* else do */
    END.
    IF {k-cancel.i} THEN DO:
      ASSIGN exceptCode = "".
      CLEAR FRAME F-REASON2.
      HIDE FRAME f-reason2.
      return.
      /* APPLY "window-close" TO FRAME f-reason2.    */
    END.
  
  apply lastkey.

  END.
    
    
END. /* procedure popupcomm */





/* -------------------------------------------------------------------------  */
Procedure conversion-marking:
/* -------------------------------------------------------------------------  */
 
  define buffer ix-lines for t-lines.
  define buffer ix-oeelb for oeelb.

  
  enable b-lines with frame f-lines.

/*  find ix-lines where ix-lines.lineno = t-lines.lineno.
  
  assign ix-lines.convertfl = if ix-lines.convertfl = yes then no
                             else if ix-lines.convertfl = no then yes
                             else no.
*/
  assign t-lines.convertfl = if t-lines.convertfl = yes then no
                             else if t-lines.convertfl = no then yes
                             else no.
  find ix-oeelb where
       ix-oeelb.cono        = g-cono and
       ix-oeelb.batchnm     = string(v-quoteno,">>>>>>>9") and
       ix-oeelb.seqno       = 1 and
       ix-oeelb.lineno      = t-lines.lineno no-error.
  if avail ix-oeelb then
    overlay (ix-oeelb.user5,2,3) = if t-lines.convertfl then "yes" else "no ".
  
  hide frame f-statuslinex.
  b-lines:refresh(). 
  display b-lines with frame f-lines.

end.                               

/* ------------------------------------------------------------------------- */ 
Procedure Conversion-Detail: 
/* ------------------------------------------------------------------------- */
  define buffer ix-oeelb for oeelb.
  define buffer ix-oeelk for oeelk.
  define buffer ix-oeehb for oeehb.

  def var xi-oeehbrecid as recid no-undo.
  def var xi-oeelkrecid as recid no-undo.
  def var xi-linesrecid as recid no-undo.

  assign xi-linesrecid  = recid(t-lines).
   find ix-oeelb where
        ix-oeelb.cono        = g-cono and
        ix-oeelb.batchnm     = string(v-quoteno,">>>>>>>9") and
        ix-oeelb.seqno       = 1 and
        ix-oeelb.lineno      = v-lineno no-error.

    find ix-oeehb where
         ix-oeehb.cono        = g-cono and
         ix-oeehb.batchnm     = string(v-quoteno,">>>>>>>9") and
         ix-oeehb.seqno       = 1 no-lock no-error.

    if v-seqno <> 0 then
      find ix-oeelk where
           ix-oeelk.cono        = g-cono    and
           ix-oeelk.ordertype   = "B"       and
           ix-oeelk.orderno     = v-quoteno and
           ix-oeelk.ordersuf    = 0         and
           ix-oeelk.seqno       = v-seqno   and
           ix-oeelk.lineno      = v-lineno no-error.
    if avail ix-oeelk then
     assign xi-oeelkrecid = recid(ix-oeelk).
   else
     assign xi-oeelkrecid = ?.
     

   
   
   
   assign xi-oeehbrecid = recid(ix-oeehb).


   if v-seqno = 0  then 
     do:
     run oeexqrelchk.p  (input xi-oeehbrecid, 
                         input-output ip-update,
                         input recid(ix-oeelb),
                         input  xi-oeelkrecid,
                         input "LDetail",
                         input-output x-returncode).  
   end.
   else
     do:
     run oeexqrelchk.p  (input xi-oeehbrecid, 
                         input-output ip-update,
                         input recid(ix-oeelb),
                         input  xi-oeelkrecid,
                         input "KDetail",
                         input-output x-returncode).  
   end.                    
   assign ip-update = true.
/*
        put screen row 13 col 3 color 
           message 
 " F7-Line Maintenance   F8-Vendor/Whse                                      ".


*/
   if v-seqno = 0 then do:

     find ix-oeelb where
          ix-oeelb.cono        = g-cono and
          ix-oeelb.batchnm     = string(v-quoteno,">>>>>>>9") and
          ix-oeelb.seqno       = 1 and
          ix-oeelb.lineno      = v-lineno no-error.

     find xt-lines where xt-lines.lineno = ix-oeelb.lineno and
                         xt-lines.seqno  = 0 no-error. 
     assign 
            xt-lines.specnstype = ix-oeelb.specnstype
            xt-lines.descrip    = ix-oeelb.proddesc
            xt-lines.qty        = ix-oeelb.qtyord
            xt-lines.vendno     = ix-oeelb.vendno
            xt-lines.prodcat    = ix-oeelb.prodcat
            xt-lines.prodcost   = ix-oeelb.prodcost
            xt-lines.leadtm     = ix-oeelb.leadtm
            xt-lines.pricetype  = ix-oeelb.pricetype
            xt-lines.lcomment   = substr(ix-oeelb.user4,74,4)
            xt-lines.whse       = substr(ix-oeelb.user4,1,4)
            xt-lines.shipdt     = if substr(ix-oeelb.user4,26,8) = "?       "                                    then
                                    ?
                                 else
                                    date(substr(ix-oeelb.user4,26,8))
            v-surcharge        = ix-oeelb.datccost 
            v-sparebamt        = ix-oeelb.user6
            v-sellprc          = ix-oeelb.price        
            v-specnstype       = ix-oeelb.specnstype  
            v-rushfl           = ix-oeelb.rushfl    
            v-costoverfl       = ix-oeelb.costoverfl 
            orig-cost          = dec(substr(ix-oeelb.user1,2,7))
            over-cost          = dec(substr(ix-oeelb.user1,2,7))
            v-leadtm           = ix-oeelb.leadtm
            zi-icspecrecno     = ix-oeelb.icspecrecno 
            v-prodcat          = ix-oeelb.prodcat 
            v-prodline         = ix-oeelb.prodline    
            v-whse             = substr(ix-oeelb.user4,1,4) 
            v-slsrepout        = ix-oeelb.slsrepout 
            v-prodcost         = ix-oeelb.prodcost  
            v-glcost           = dec(ix-oeelb.user2)
            v-lcomment         = substr(ix-oeelb.user4,74,1).
  
     
     end.
     else
     if v-seqno <> 0 then do:
       find ix-oeelk where
            ix-oeelk.cono        = g-cono    and
            ix-oeelk.ordertype   = "B"       and
            ix-oeelk.orderno     = v-quoteno and
            ix-oeelk.ordersuf    = 0         and
            ix-oeelk.seqno       = v-seqno   and
            ix-oeelk.lineno      = v-lineno no-error.

       find xt-lines where xt-lines.lineno = ix-oeelk.lineno and
                           xt-lines.seqno  = v-seqno no-error. 
 
       
       assign
         xt-lines.lineno     =  ix-oeelk.lineno
         xt-lines.seqno      =  ix-oeelk.seqno
         xt-lines.prod       =  ix-oeelk.shipprod
         xt-lines.descrip    =  ix-oeelk.refer
         xt-lines.xprod      =  substr(ix-oeelk.proddesc2,1,22)
         xt-lines.whse       =  ix-oeelk.whse
         xt-lines.convertfl  = if v-conversion then 
                                 if substr(ix-oeelk.user3,2,3) = "yes" then
                                   yes 
                                 else
                                   no
                              else
                                 no
         xt-lines.leadtm     =  int(ix-oeelk.user6)
         xt-lines.vendno     =  ix-oeelk.arpvendno
         xt-lines.prodcat    =  ix-oeelk.prodcat
         xt-lines.pricetype  =  ix-oeelk.pricetype
         xt-lines.listprc    =  if ix-oeelk.specnstype = "n" then 
                                  ix-oeelk.price 
                                else 0
         xt-lines.sellprc    =  ix-oeelk.price
         xt-lines.prodcost   =  ix-oeelk.prodcost
         xt-lines.gp         =  0
         xt-lines.discpct    =  0
         xt-lines.disctype   =  no
         xt-lines.kqty       =  ix-oeelk.qtyord
         xt-lines.shipdt     =  ?
         xt-lines.kitfl      =  no
         xt-lines.specnstype = ix-oeelk.specnstype
         xt-lines.pdscrecno  = ix-oeelk.pdrecno
         xt-lines.lcomment   = substr(ix-oeelk.user4,74,1)
/*         v-surcharge        = ix-oeelk.datccost  */
         v-sellprc          = ix-oeelk.price        
         v-specnstype       = ix-oeelk.specnstype  
/*         v-rushfl           = ix-oeelk.rushfl    */
         v-costoverfl       = ix-oeelk.costoverfl 
         orig-cost          = dec(substr(ix-oeelk.user1,1,7))
         over-cost          = dec(substr(ix-oeelk.user1,1,7))
         v-leadtm           = ix-oeelk.user6
         zi-icspecrecno   = ix-oeelk.icspecrecno 
         v-prodcat          = ix-oeelk.prodcat 
         v-prodline         = ix-oeelk.arpprodline    
         v-whse             = substr(ix-oeelk.user4,1,4) 
/*         v-slsrepout        = ix-oeelk.slsrepout  */
         v-prodcost         = ix-oeelk.prodcost  
         v-glcost           = ix-oeelk.glcost
         v-lcomment         = substr(ix-oeelk.user4,74,1).
     end.

  run Conversion-Loadx.
  readkey pause 0.
  display  
     oeehb.batchnm    
     oeehb.custno     
     oeehb.transtype  
     oeehb.shipto    
     oeehb.whse      
     with frame f-lineconvert.
  b-lines:refresh() in frame f-lines. 
  display b-lines with frame f-lines.


  
end.


/* ------------------------------------------------------------------------- */ 
Procedure Check_Quote_Mortality: 
/* ------------------------------------------------------------------------- */
    define buffer mx-oeelb for oeelb.
    
    /*
    find oeeh where oeeh.cono     = g-cono and
                    oeeh.orderno  = ip-orderno and
                    oeeh.enterdt  = TODAY 
                    no-lock no-error.
    if not avail oeeh then
    */
    if v-quoteno = ip-orderno then
      do:
      message "Quote did NOT Convert. View error(s) in file "
              + v-qfilename +
              " in OIPF".
      pause.
      assign g-orderno = v-quoteno
             errorfl   = yes.
    end. /* if ip-orderno = v-quoteno */
    if inquiryfl = no then
      do:
    assign v-question = no.
    message  "Do you want to keep this quote Open ?"         
      view-as alert-box question buttons yes-no            
    update v-question.                                 
    if v-question or {k-cancel.i} then                                            
      do:
      return.
      end.                  
 
  find first b-oeehb where 
             b-oeehb.cono    = g-cono and
             b-oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
             b-oeehb.seqno   = 1 no-error.
     for each mx-oeelb where mx-oeelb.cono = g-cono and
                             mx-oeelb.batchnm = b-oeehb.batchnm and
                             mx-oeelb.seqno = b-oeehb.seqno:
                            
       assign mx-oeelb.specnstype = "l"
              mx-oeelb.reasunavty = if b-oeehb.enterdt = TODAY then
                                      "CR  "
                                    else
                                      "CQ  ".
     end.
  
  
  
  if avail b-oeehb then do:
    assign b-oeehb.stagecd = 9.
    overlay (b-oeehb.user3,53,4) = if b-oeehb.enterdt = TODAY then
                                     "CR  "
                                   else
                                     "CQ  ".
    assign b-oeehb.canceldt = TODAY.          /* das - 05/21/09 */
  end.                  
  end. /* inquiryfl = no */

end. /* Procedure Check_Quote_Mortality */


/* ------------------------------------------------------------------------- */ 
Procedure Check_Quote_Restore: 
/* ------------------------------------------------------------------------- */
/* Retrieve Original Quote from Backup  (in Conversion procedure)           */
   if x-backed-quote-up then do:
     find first b-oeehb where b-oeehb.cono = g-cono and  
                b-oeehb.batchnm = string(v-quoteno,">>>>>>>9")
                no-lock no-error.          
  
     run oeexqrelx.p (input "Restore",
                      input recid(b-oeehb),
                      output x-error). 
     if x-error then
       do:
       message "Fatal Preparation error". pause 5.
       assign s-canceled = true.
     end.
     else
       assign x-backed-quote-up = false.
     
   end.
end.


