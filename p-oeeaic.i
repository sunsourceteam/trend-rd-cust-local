/* p-oeeaic.i 1.1 01/03/98 */
/* p-oeeaic.i 1.1 01/03/98 */
/*h*****************************************************************************  INCLUDE      : p-oeeaic.i
  DESCRIPTION  : Fields used in the "customer" record in edi 810, 843, and 855.
  USED ONCE?   : no
  AUTHOR       : jlc
  DATE WRITTEN : 02/23/94
  CHANGES MADE :
    08/24/95 jkp; TB# 18444 Add inside salesrep and phoneno.
    05/12/03 lbr; TB# e17245 Added arsc dunsno
    08/10/03 jlc; TB# e18165 Add additional identifiers for eBill
******************************************************************************/"Custom" +
     caps(s-custnm) +
     caps(s-custaddr[1]) +
     caps(s-custaddr[2]) +
     caps(s-custcity) +
     caps(s-custstate) +
     caps(s-custzipcd) +
     caps(s-custphone) +
     caps(s-custuser1) +
     caps(s-custuser2) +
     caps(s-custno) +
     caps(s-slsrepin) +
     caps(s-slsrepinphoneno) /* +
     caps(s-arscdunsno)  +
     caps(s-arscerpid) +
     caps(s-arsctpid)        */