/* p-oepiel.i 1.4 06/08/98 */
/*h*****************************************************************************  INCLUDE      : p-oepiel.i
  DESCRIPTION  : Fields used in the "Item" record in the edi 810, 843, and 855
  USED ONCE?   : no
  AUTHOR       : jlc
  DATE WRITTEN : 02/23/94
  CHANGES MADE :
    05/31/95 jkp; TB# 18581 Add special cost fields.
    06/01/95 jkp; TB# 18488 Add oeel.xrefprodty field.
    07/25/96 jkp; TB# 20599 Add oeel tax fields.
    08/19/96 jkp; TB# 20987 Add line level restocking charges.
    03/27/98 jlc; TB# 24619 Add promisedt and reqshipdt to item record.
    03/27/98 jlc; TB# 24730 Add special non-stock type to item record.
    05/14/98 sbr; TB# 24671 Round taxes after lines are added together.
    05/28/98 kjb; TB# 24765 Add the UPC number to the EDI 810 out file
    12/02/02 ns;  TB# e12955 Add EDI Line #
    04/30/03 lbr; TB# e17245 Add fields to line
    05/05/03 lbr; TB# e17268 added 2 fields
******************************************************************************/
    /*tb 24671 05/14/98 sbr; Moved s-taxamount1, s-taxamount2, s-taxamount3,
        and s-taxamount4 to the end of the file. */

    "Item  " +
             caps(s-lineno)       +
             caps(s-qtyship)      +
             caps(s-unit)         +
             caps(s-price)        +
             caps(s-prodcd)       +
             caps(s-custprod)     +
             caps(s-shipprod)     +
             caps(s-descrip)      +
             caps(s-discpct)      +
             caps(s-qtyord)       +
             caps(s-itemuser1)    +
             caps(s-itemuser2)    +
             caps(s-itemuser3)    +
             caps(s-itemuser4)    +
             caps(s-itemuser5)    +
             caps(s-ordstat)      +
             caps(s-chgrsn)       +
             caps(s-bofl)         +
             caps(s-unitdisc)     +
             caps(s-itemuser6)    +
             caps(s-itemuser7)    +
             caps(s-itemuser8)    +
             caps(s-itemuser9)    +
             caps(s-itemuser10)   +
             caps(s-speccostty)   +
             caps(s-speccostunit) +
             caps(s-specuom)      +
             caps(s-xrefprodty)   +
             caps(s-taxablefl)    +
             caps(s-taxablety)    +
             "           "        +
             "           "        +
             "           "        +
             "           "        +
             caps(s-taxgroup)     +
             caps(s-lnrestockamt) +
             caps(s-lnpromisedt)  +
             caps(s-lnreqshipdt)  +
             caps(s-specnstype)   +
             caps(s-taxamount1)   +
             caps(s-taxamount2)   +
             caps(s-taxamount3)   +
             caps(s-taxamount4)   +
             caps(s-upcno1)       +
             caps(s-upcno2)       +
             caps(s-upcno3)       +
             caps(s-upcno4)       +
             caps(s-upcno5)       +
             caps(s-upcno6)    /* +
             caps(s-edilineno)    +
             /* tb e17245 */
             caps(s-ediqtyunit)   +
             caps(s-ediprcunit)   +
             caps(s-discpct)      +
             caps(s-edinetamt)    +
             caps(s-edinetord)    +
             /* tb e17268 next 2 vars */
             caps(s-ediunitsell)  +
             caps(s-ediunitstock) */
