{p-rptbeg.i}            
{g-cr.i}
{g-ic.i}
{g-oe.i}
{smsvtemp.gtt &shared = "new shared"}

 
def new shared var o-pcatold  like icsp.prodcat         no-undo.
def new shared var o-pcatnew  like icsp.prodcat         no-undo.
def var v-seqno      like sapbo.seqno                   no-undo.
def var o-orderno    like oeeh.orderno                  no-undo.
def var o-ordersuf   like oeeh.ordersuf                 no-undo.                
def var b-invoicedt  like oeeh.invoicedt                no-undo.
def var e-invoicedt  like oeeh.invoicedt                no-undo.
def var o-cust       like arsc.custno extent 15         no-undo.
def var o-custselection as logical                      no-undo.
def var o-orderlist  as logical                         no-undo.
def var o-importfile as c format "x(30)"                no-undo.
def var o-inx        as integer                         no-undo.
def var v-inx        as integer                         no-undo.
def var v-updated    as integer                         no-undo.
def var o-err_v      as character format "x(78)"        no-undo.
def var v-perfisc    as integer format ">>>9"           no-undo.

def var w-cost       as decimal format ">>>,>>>,>>9.99-"
                                                        no-undo.                        
def var w-sell       as decimal format ">>>,>>>,>>9.99-"
                                                        no-undo.                        
def var x-header     as character format "x(30)"       no-undo.

def var v-genlit as c format "x(45)" no-undo.
def var v-order like oeeh.orderno no-undo.
def var v-suf   like oeeh.ordersuf no-undo.
def var v-line  like oeel.lineno no-undo.
def var v-seq   like oeelk.seqno no-undo.
def var v-prodo like oeel.prodcat no-undo.
def var v-prodn like oeel.prodcat no-undo.
def var x-order like oeeh.orderno no-undo.
def var x-suf   like oeeh.ordersuf no-undo.
def var x-line  like oeel.lineno no-undo.
def var x-yseq   like oeelk.seqno no-undo.
def var x-prodo like oeel.prodcat no-undo.
def var x-prodn like oeel.prodcat no-undo.








def new shared temp-table zod
  field orderno like oeeh.orderno
  field ordersuf like oeeh.ordersuf
  field postdt   as date format "99/99/99"
  field invdt    as date format "99/99/99"
  field ordtype  like oeeh.transtype
  field custno   like oeeh.custno
  field oldcat   like oeel.prodcat
  field newcat   like oeel.prodcat
  field pcat      like icsp.prodcat  
  field sell     as dec format ">>>,>>>,>>9.99-"
  field cost     as dec format ">>>,>>>,>>9.99-"
    index ordinx
          pcat
          custno
          orderno
          ordersuf.


def new shared temp-table zos
  field mo       as integer  format "99"
  field yr       as integer  format "9999"
  field oldcat   like icsp.prodcat 
  field newcat   like icsp.prodcat  
  field pcat   like icsp.prodcat   
  field sell     as dec format ">>>,>>>,>>9.99-"
  field cost     as dec format ">>>,>>>,>>9.99-"
    index zosrex
          pcat
          yr
          mo.
          

form
 o-err_v at 1
with frame errors width 132 no-box no-labels no-underline.

form
 sapbo.custno  at 1 format ">>>>>>9"
 sapbo.ordersuf at 12
 o-err_v        at 15
with frame erroro width 132 no-box no-labels no-underline.


define frame head1
  "Order Detail of CR/DB pcat(s)"       at 52
  skip(2)
  "Order"        at 1
  "PostDt"       at 15
  "Inv Dt"       at 25
  "Type"         at 34
  "Cust No"      at 44
  "OldCat"       at 53
  "NewCat"       at 61
  "Sell"         at 79
  "Cost"         at 96
  with page-top frame head1 no-box no-underline no-labels width 132.



define frame head2
  "Period Detail of CR/DB pcat(s)"       at 52
  skip (2)
  "Period"       at 1
  "Sell"         at 25
  "Cost"         at 43
  "Pcat  "       at 49
  
  with page-top frame head2 no-box no-underline no-labels width 132.


form
    "Total"          at 1
    zod.pcat          at 7
    w-sell           at 69 no-label
    w-cost           at 86 no-label   
 with frame total1 width 132 no-box no-underline no-labels.


form

    "Total"          at 1  
    zos.pcat          at 7 
    w-sell           at 15
    w-cost           at 32
    
 with frame total2 width 132 no-labels no-underline.




form
    zod.orderno      at 1  format ">>>>>>9" space(0) 
    "-"              space(0)
    zod.ordersuf     at 9  no-label
    zod.postdt       at 14 no-label
    zod.invdt        at 24 no-label   
    zod.ordtype      at 35 no-label   
    zod.custno       at 40 no-label
    zod.oldcat       at 54 no-label   
    zod.newcat       at 62 no-label   
    zod.sell         at 69 no-label   
    zod.cost         at 86 no-label   
 with frame dtlfm width 132 no-box no-underline no-labels.


form

    zos.mo           at 1  
    "-"              at 3 
    zos.yr           at 4 
    zos.sell         at 15
    zos.cost         at 32
    zos.pcat          at 50
 with frame sumfm width 132 no-labels no-underline.


form 
  v-order     label "Order"
  v-suf       label "Suf"
  v-line      label "Line"
  v-seq       label "Seq"
  v-prodo     label "OldPcat"
  v-prodn     label "NewPcat"
  v-genlit    label "Message"
  with frame errorlist width 132. 




{g-oeepi.i "new" "new" "/*"}

{rebnet.gva "new shared"}
{speccost.gva "new shared"}


v-errfl = "".
o-err_v = "".
o-custselection = false.


{w-sasc.i no-lock " " b-}
o-orderlist = if sapb.optvalue[1] = "yes" then true else false.
o-importfile = sapb.optvalue[2].

define stream importx. 
 


if o-importfile <> "" and o-orderlist = true then
  do:
  v-genlit = "Can't run from a file and a list".
  display v-genlit with frame errorlist.
  return.
  end.

if o-importfile <> "" then
  do:
  if search("/usr/tmp/" + o-importfile) = ? then
    do:
    v-genlit = "File name does not exist in /usr/tmp".
    display v-genlit with frame errorlist.
    return.
    end.

  
  input stream importx from value("/usr/tmp/" + o-importfile).
  repeat with frame errorlist:
  assign  v-genlit = ""
          v-order = 0
          v-suf = 0
          v-line = 0
          v-seq = 0
          v-prodo = ""
          v-prodn = "".
  import stream importx delimiter ","
    v-order v-suf v-line v-seq v-prodo v-prodn.

  /* Take out blank records */
  if v-order = 0 and
     v-suf = 0 and
     v-line = 0 and
     v-seq = 0 and
     v-prodo = "" and
     v-prodn = "" then
     next.
       
    
    
   find sapbo where
        sapbo.cono = g-cono and
        sapbo.reportnm = sapb.reportnm and
        sapbo.custno   = dec(v-order)    and
        sapbo.ordersuf = v-suf      and
        sapbo.xxde1    = v-line     and
        sapbo.xxi1     = v-seq      and
        sapbo.prodcat  = v-prodo
    exclusive-lock no-error.  
                 
   if avail sapbo then
     do:
     v-genlit = "**x* Duplicate occurance **".
     display 
       v-order
       v-suf
       v-line
       v-seq
       v-prodo
       v-prodn
       v-genlit
       with down frame errorlist.
     down with frame errorlist.  
     next.
     end.
   assign
     x-order =  v-order
     x-suf   =  v-suf
     x-line  =  v-line
     x-yseq   = v-seq
     x-prodo =  v-prodo
     x-prodn =  v-prodn.

  if v-prodo <> "blnk" then  
    do:
    {w-sasta.i "c" "v-prodo" "no-lock"}
    if not avail sasta then
      v-genlit = "**x* Old Pcat not Valid **".
    else
      do:
      {w-sasta.i "c" "v-prodn" "no-lock"}
      if not avail sasta then
        v-genlit = "**x* New Pcat not Valid **".
      end.
    end.
  else
      do:
      {w-sasta.i "c" "v-prodn" "no-lock"}
      if not avail sasta then
        v-genlit = "**x* New Pcat not Valid **".
      end.
  
  if not(v-genlit begins "**x* ") then
    do:
    find oeeh where oeeh.cono = g-cono and
                  oeeh.orderno = v-order and
                  oeeh.ordersuf = v-suf 
         no-lock no-error.
    if avail oeeh then
      if oeeh.updtype <> "m" then
        v-genlit = "**x* Order was not updated to sales manager **".
     
    find oeel where oeel.cono = g-cono and
                  oeel.orderno = v-order and
                  oeel.ordersuf = v-suf and
                  oeel.lineno =  v-line no-lock no-error.

  
  
     if not avail oeel then
       v-genlit = "**x* Order/Line not Valid **".
     else
     if (x-prodo = x-prodn  and x-prodo <> "blnk") or
        (x-prodo = "blnk" and x-prodn = "" ) then
       v-genlit = "**x* Pcats are the same **".
     else
     if (x-prodo <> oeel.prodcat and x-prodo <> "blnk") or 
        (x-prodo = "blnk" and oeel.prodcat <> "") then
      v-genlit = "**x* Order/line Pcat MissMatch ** " + oeel.prodcat.
     else
     if oeel.statustype <> "i" then
      v-genlit = "**x* Order/Line not invoiced status ** " + oeel.statustype.
     else
     if oeel.specnstype = "l" then
      v-genlit = "**x* Order/Line was put to lost sales **".
      
      
    end.
  if v-seq <> 0 and not(v-genlit begins "**x* ") then
    do:
    find oeelk where oeelk.cono = g-cono and
                     oeelk.ordertype = "o" and
                     oeelk.orderno = v-order and
                     oeelk.ordersuf = v-suf and
                     oeelk.lineno = v-line and
                     oeelk.seqno =  v-seq  no-lock no-error.
    if not avail oeelk then
      v-genlit = "**x* Kit Order/Seq not Valid **".
    else
    if (x-prodo = x-prodn  and x-prodo <> "blnk") or
        (x-prodo = "blnk" and x-prodn = "" ) then
       v-genlit = "**x* Pcats are the same **".
    else
    if (x-prodo <> oeelk.prodcat and x-prodo <> "blnk") or 
       (x-prodo = "blnk" and oeelk.prodcat <> "") then
      v-genlit = "**x* Kit Order/Seq Pcat MissMatch ** " + oeelk.prodcat.
  
       
    end.
   
   if (x-prodo = x-prodn  and x-prodo <> "blnk") or
      (x-prodo = "blnk" and x-prodn = "" ) then
     v-genlit = "**x* Pcats are the same **".

   
   if not(v-genlit begins "**x* ") then
     do:
     find last sapbo use-index k-sapbo where sapbo.cono = g-cono 
                               and sapbo.reportnm = sapb.reportnm
                               no-lock no-error.
     if not avail sapbo then
       v-seqno = 0.
     else
       v-seqno = sapbo.seqno.                

     o-orderlist = true.
     
     create sapbo.
     assign
       sapbo.cono     = g-cono
       sapbo.reportnm =  v-reportnm
       v-seqno        = v-seqno + 1
       sapbo.prodcat  = v-prodo
       sapbo.custno   = dec(v-order)
       sapbo.ordersuf = v-suf
       sapbo.xxde1    = v-line
       sapbo.xxi1     = v-seq
       sapbo.seqno    = v-seqno
       sapbo.orderno  = v-seqno
       sapbo.xxc2     = v-prodn.
     {t-all.i sapbo}

     end.  
   else
     do:
     display 
       v-order
       v-suf
       v-line
       v-seq
       v-prodo
       v-prodn
       v-genlit
       with down frame errorlist.
     down with frame errorlist.  
     next.
     end.
 end.  
 
page.
end.


if o-orderlist  then
  do:
  for each sapbo where sapbo.cono = sapb.cono and
                       sapbo.reportnm = sapb.reportnm no-lock:

  
  
  find b-oeeh where b-oeeh.cono = g-cono and
                      b-oeeh.orderno = int(sapbo.custno) and
                      b-oeeh.ordersuf = sapbo.ordersuf  and
                      b-oeeh.updtype = "m"             and
                      not can-do("qu,bl,ra",b-oeeh.transtype) no-error.
  
 
  o-pcatold = sapbo.prodcat.
  o-pcatnew = sapbo.xxc2.
  
  if not avail b-oeeh then
     do:
     o-err_v = "Order not found or not in proper status or wrong Slsrep".
     display
       sapbo.custno
       sapbo.ordersuf
       o-err_v
       o-pcatold
       with frame erroro.
     next.
     end.

  v-updated = 0.
  find sasj where sasj.cono = g-cono    and
                  sasj.smmergedfl = yes and
                  sasj.ourproc    = "oeepi" and
                  sasj.jrnlno     = b-oeeh.jrnlno2 no-lock no-error.

  if not avail sasj then
    do:
    display "No Jornal record for order # " b-oeeh.orderno b-oeeh.ordersuf.
    next.
    end.


  assign p-postdt = sasj.postdt
         v-perfisc = sasj.perfisc
         v-yr      = if b-sasc.smstorety = yes then
                       integer(substring(string(v-perfisc,"9999"),3,2))
                     else
                       integer(substring(string(year(p-postdt)),3,2))
         v-mo      = if b-sasc.smstorety = yes then
                       integer(substring(string(v-perfisc,"9999"),1,2))
                     else
                       integer(string(month(p-postdt)))
         v-yrcal   = integer(substring(string(year(p-postdt)),3,2))
         v-mocal   = integer(string(month(p-postdt))).

  assign v-errfl = " ".

  if v-yr = 0 and v-mo = 0 and v-yrcal = 0 and v-mocal = 0 then
     do:
     display "Error computing period values".
     v-errfl = "y".
     end.

  assign g-orderno       = b-oeeh.orderno
         g-ordersuf      = b-oeeh.ordersuf
         v-totvendrebamt = 0
         v-totcustrebamt = 0
         v-vendrebamt = 0
         v-custrebamt = 0
         v-lockedfl  = no
         v-undofl    = no
         t-transcnt  = 1.

  {w-arsc.i b-oeeh.custno no-lock b-}
 
  if sapbo.xxi1 = 0 then
    do:
    oeel-loop:
    for each b-oeel use-index k-oeel where
             b-oeel.cono          = g-cono                and
             b-oeel.orderno       = b-oeeh.orderno        and
             b-oeel.ordersuf      = b-oeeh.ordersuf       and
             b-oeel.lineno        = int(sapbo.xxde1)      and
             b-oeel.corechgty     <> "r"                  and
             b-oeel.specnstype    <> "l"                  and
             b-oeel.warrexchgfl   = no                    and
             b-oeel.prodcat       = (if sapbo.prodcat = "blnk" then
                                        "    "
                                     else
                                        sapbo.prodcat) and
             b-oeel.statustype    = "i":
  
  
  {icss.gfi
   &prod        = b-oeel.shipprod
   &icspecrecno = b-oeel.icspecrecno
   &lock        = "no" }
  {speccost.gas}
  if b-oeel.specnstype <>  "n" then
    do:
    {w-icsp.i b-oeel.shipprod no-lock b-}
    {w-icsw.i b-oeel.shipprod b-oeel.whse no-lock b- no-wait}
    end.
  for each pder use-index k-pder no-lock where
           pder.cono            = g-cono                and
           pder.orderno         = b-oeel.orderno        and
           pder.ordersuf        = b-oeel.ordersuf       and
           pder.lineno          = b-oeel.lineno         and
           pder.seqno           = 0                     and
           can-do("c,s",pder.rebatecd)                  and
           pder.pdersuf         = 0:

    assign v-vendrebamt = if pder.rebatecd = "s" then
                            {rebext.gca
                             &qty = "if v-speccostty = '' then
                                       pder.qtyship
                                     else
                                       pder.stkqtyship" }
                          else
                            v-vendrebamt

           v-custrebamt = if pder.rebatecd = "c" then
                            {rebext.gca
                             &qty = "if v-speccostty = '' then
                                       pder.qtyship
                                     else
                                       pder.stkqtyship" }
                          else
                            v-custrebamt.
  end. /* pder */
  

   run smazps.p (no,no).
   assign b-oeel.prodcat = o-pcatnew
          b-oeel.user4     = "SMAZP - P " + o-pcatold
          v-updated = (v-updated + 1).
  /* Not processing components for my purpose */
   assign v-totvendrebamt = 0
          v-totcustrebamt = 0
          v-vendrebamt = 0
          v-custrebamt = 0.
  end.
end. /* oeel */


if sapbo.xxi1 <> 0 then
  do:
  find b-oeel where b-oeel.cono = 1 and
                    b-oeel.orderno = b-oeeh.orderno and
                    b-oeel.ordersuf = b-oeeh.ordersuf and
                    b-oeel.lineno = int(sapbo.xxde1) and
                    b-oeel.kitfl = yes and
                    b-oeel.specnstype <> "l" and
                    b-oeel.statustype = "i" no-error.       
 
  if avail b-oeel then
  for each b-oeelk use-index k-oeelk where
              b-oeelk.cono          = g-cono                and
              b-oeelk.ordertype     = "o"                   and
              b-oeelk.orderno       = b-oeel.orderno        and
              b-oeelk.ordersuf      = b-oeel.ordersuf       and
              b-oeelk.lineno        = int(sapbo.xxde1)      and
              b-oeelk.seqno         = sapbo.xxi1            and
              b-oeelk.comptype      <> "r"                  and
              b-oeelk.qtyneeded     ge 0:
     
     
     find b-oeel where b-oeel.cono = 1 and
                       b-oeel.orderno = b-oeeh.orderno and
                       b-oeel.ordersuf = b-oeeh.ordersuf and
                       b-oeel.lineno = int(sapbo.xxde1) and
                       b-oeel.kitfl = yes and
                       b-oeel.specnstype <> "l" and
                       b-oeel.statustype = "i" no-error.       
     
     
     {icss.gfi
      &prod        = b-oeelk.shipprod
      &icspecrecno = b-oeelk.icspecrecno
      &lock        = "no" }
     {speccost.gas}
     if b-oeelk.specnstype <>  "n" then
       do:
       {w-icsp.i b-oeelk.shipprod no-lock b-}
       {w-icsw.i b-oeelk.shipprod b-oeel.whse no-lock b- no-wait}
       end.
     for each pder use-index k-pder no-lock where
             pder.cono            = g-cono                and
             pder.orderno         = b-oeel.orderno        and
             pder.ordersuf        = b-oeel.ordersuf       and
             pder.lineno          = b-oeel.lineno         and
             pder.seqno           = b-oeelk.seqno         and
             can-do("c,s",pder.rebatecd)                  and
             pder.pdersuf         = 0:

     assign v-vendrebamt = if pder.rebatecd = "s" then
                             {rebext.gca
                              &qty = "if v-speccostty = '' then
                                        pder.qtyship
                                      else
                                        pder.stkqtyship" }
                           else
                             v-vendrebamt

            v-custrebamt = if pder.rebatecd = "c" then
                             {rebext.gca
                              &qty = "if v-speccostty = '' then
                                        pder.qtyship
                                      else
                                        pder.stkqtyship" }
                           else
                             v-custrebamt.
     end. /* rebate */

    run smazps.p (yes,no).

 
    assign b-oeelk.prodcat = o-pcatnew
           b-oeelk.user4     = "SMAZP - P " + o-pcatold
         v-updated = (v-updated + 1).

    
    assign v-vendrebamt = 0
           v-custrebamt = 0.
    end. /* oeelk   */
    end. /* Kitfl   */
   /* Not processing components for my purpose */

   
  /*
  
  assign b-oeel.prodcat = o-pcatnew
         b-oeel.user4     = "SMAZP - P " + o-pcatold
         v-updated = (v-updated + 1).
  */

 end. /* sapbo */
end. /* custselection */


view frame head1.
assign w-sell = 0
       w-cost = 0.

for each zod
    break by zod.pcat:

  find zos where zos.yr = year(zod.postdt) and
                 zos.mo = month(zod.postdt) and
                 zos.pcat = zod.pcat no-error.
                
  if not avail zos then
    do:
    create zos.
    assign
          zos.oldcat = zod.oldcat
          zos.newcat = zod.newcat
          zos.yr = year(zod.postdt)
          zos.mo = month(zod.postdt)
          zos.pcat = zod.pcat.
     end.
  assign zos.sell = zos.sell + zod.sell
         zos.cost = zos.cost + zod.cost.
  display
    zod.orderno
    zod.ordersuf
    zod.postdt
    zod.invdt
    zod.ordtype
    zod.custno
    zod.oldcat
    zod.newcat
    zod.sell
    zod.cost
    with frame dtlfm no-box no-underline.
down with frame dtlfm.
assign w-sell = w-sell + zod.sell
       w-cost = w-cost + zod.cost. 
if last-of(zod.pcat) then
  do:
  display
    zod.pcat
    w-sell
    w-cost
   with frame total1.
  assign w-sell = 0
         w-cost = 0.
  
  page.
  end.


end.



hide frame head1.
view frame head2. 
page.
assign w-sell = 0
       w-cost = 0.

for each zos
  break by zos.pcat:
display
  zos.pcat
  zos.mo
  zos.yr
  zos.sell
  zos.cost
with frame sumfm no-box no-underline.
down with frame sumfm.
assign w-sell = w-sell + zos.sell
       w-cost = w-cost + zos.cost. 

if last-of(zos.pcat) then
  do:
  display
    zos.pcat
    w-sell
    w-cost
   with frame total2.
  assign w-sell = 0
         w-cost = 0.

  page.
  end.


end.
for each sapbo where sapbo.cono = sapb.cono and
                     sapbo.reportnm = sapb.reportnm:
  delete sapbo.
end.


procedure errdis:
display
 o-err_v 
  with frame errors.
end.      
