/****************************************************************************
    Procedure    : smrzc
    Description  : Custom sales report, sales ytd by division by rep
    Author       : Dave Farmer
    Date Written : 12/21/98
    Cahnges Made :
*****************************************************************************/

{p-rptbeg.i}

def new shared var forbegdate as date no-undo.



def var tyr            as integer format "99" no-undo.
def var repyr          as integer format "99" no-undo.
def VAR tx             AS integer no-undo.
def var slshld         like smsn.slsrep no-undo.
DEF VAR rpt-ytd-hdr1   AS CHAR FORMAT "X(14)"        NO-UNDO.
DEF VAR rpt-ytd-hdr2   AS CHAR FORMAT "X(14)"        NO-UNDO.

def var cntr           as int                        no-undo.
def var rpt-dt         as date                       no-undo.
def var end-dt         as date                       no-undo.
def var beg-dt         as date                       no-undo.
                       
def var mo-1           as int format "99"            no-undo.
def var mo-2           as int format "99"            no-undo.
def var mo-3           as int format "99"            no-undo.
def var rpt-mo         as int format "99"            no-undo.

def var yr-1           as int format "9999"          no-undo.
def var yr-2           as int format "9999"          no-undo.
def var yr-3           as int format "9999"          no-undo.
def var lst-yr         as int format "9999"          no-undo.
def var rpt-yr         as int format "9999"          no-undo.
def var moend-dt       as date format "99/99/9999"   no-undo.
def var ytd-mos        as int format "99"            no-undo.

def var w-divno        like arsc.divno               no-undo.
def var v-divno        as int format "9999"          no-undo.
def var w-salesterr    like arsc.salesterr           no-undo.
def var w-sls-lst-yr   as dec format "zzzzzzzz9.99-" no-undo.
def var w-cst-lst-yr   as dec format "zzzzzzzz9.99-" no-undo.
def var w-sign         as int format "9-"            no-undo.
def var w-region       as char format "x"            no-undo.
def var w-slsrep       like arsc.slsrepout           no-undo.
def var w-dist         as int format "999"           no-undo.
def var x-region       as char format "x"            no-undo.
def var x-dist         as int format "999"           no-undo.
def var x-fileout      as char format "x(24)"        no-undo.
def var x-exportfl     as logical                    no-undo.
def stream x-out.
                       
def var disttot-1-sls  as dec format ">>,>>>,>>9-"  no-undo.
def var disttot-2-sls  as dec format ">>,>>>,>>9-"  no-undo.
def var disttot-3-sls  as dec format ">>,>>>,>>9-"  no-undo.
def var disttot-4-sls  as dec format ">>>,>>>,>>9-"  no-undo.
def var disttot-5-sls  as dec format ">>>,>>>,>>9-"  no-undo.
def var disttot-6-sls  as dec format ">>>,>>>,>>9-"  no-undo.
def var disttot-7-sls  as dec format ">>>,>>>,>>9-"  no-undo.
def var disttot-8-sls  as dec format ">>>,>>>,>>9-"  no-undo.

def var disttot-1-cst  as dec format ">>,>>>,>>9-"  no-undo.
def var disttot-2-cst  as dec format ">>,>>>,>>9-"  no-undo.
def var disttot-3-cst  as dec format ">>,>>>,>>9-"  no-undo.
def var disttot-4-cst  as dec format ">>>,>>>,>>9-"  no-undo.
def var disttot-5-cst  as dec format ">>>,>>>,>>9-"  no-undo.
def var disttot-6-cst  as dec format ">>>,>>>,>>9-"  no-undo.
def var disttot-7-cst  as dec format ">>>,>>>,>>9-"  no-undo.
def var disttot-8-cst  as dec format ">>>,>>>,>>9-"  no-undo.
                      
def var regtot-1-sls   as dec format ">>,>>>,>>9-"  no-undo.
def var regtot-2-sls   as dec format ">>,>>>,>>9-"  no-undo.
def var regtot-3-sls   as dec format ">>,>>>,>>9-"  no-undo.
def var regtot-4-sls   as dec format ">>>,>>>,>>9-"  no-undo.
def var regtot-5-sls   as dec format ">>>,>>>,>>9-"  no-undo.
def var regtot-6-sls   as dec format ">>>,>>>,>>9-"  no-undo.
def var regtot-7-sls   as dec format ">>>,>>>,>>9-"  no-undo.
def var regtot-8-sls   as dec format ">>>,>>>,>>9-"  no-undo.

def var regtot-1-cst   as dec format ">>,>>>,>>9-"  no-undo.
def var regtot-2-cst   as dec format ">>,>>>,>>9-"  no-undo.
def var regtot-3-cst   as dec format ">>,>>>,>>9-"  no-undo.
def var regtot-4-cst   as dec format ">>>,>>>,>>9-"  no-undo.
def var regtot-5-cst   as dec format ">>>,>>>,>>9-"  no-undo.
def var regtot-6-cst   as dec format ">>>,>>>,>>9-"  no-undo.
def var regtot-7-cst   as dec format ">>>,>>>,>>9-"  no-undo.
def var regtot-8-cst   as dec format ">>>,>>>,>>9-"  no-undo.

def var reptot-1-sls   as dec format ">>,>>>,>>9-"  no-undo.
def var reptot-2-sls   as dec format ">>,>>>,>>9-"  no-undo.
def var reptot-3-sls   as dec format ">>,>>>,>>9-"  no-undo.
def var reptot-4-sls   as dec format ">>>,>>>,>>9-"  no-undo.
def var reptot-5-sls   as dec format ">>>,>>>,>>9-"  no-undo.
def var reptot-6-sls   as dec format ">>>,>>>,>>9-"  no-undo.
def var reptot-7-sls   as dec format ">>>,>>>,>>9-"  no-undo.
def var reptot-8-sls   as dec format ">>>,>>>,>>9-"  no-undo.

def var reptot-1-cst   as dec format ">>,>>>,>>9-"  no-undo.
def var reptot-2-cst   as dec format ">>,>>>,>>9-"  no-undo.
def var reptot-3-cst   as dec format ">>,>>>,>>9-"  no-undo.
def var reptot-4-cst   as dec format ">>>,>>>,>>9-"  no-undo.
def var reptot-5-cst   as dec format ">>>,>>>,>>9-"  no-undo.
def var reptot-6-cst   as dec format ">>>,>>>,>>9-"  no-undo.
def var reptot-7-cst   as dec format ">>>,>>>,>>9-"  no-undo.
def var reptot-8-cst   as dec format ">>>,>>>,>>9-"  no-undo.

def var tot-1-sls      as dec format ">>,>>>,>>9-"  no-undo.
def var tot-2-sls      as dec format ">>,>>>,>>9-"  no-undo.
def var tot-3-sls      as dec format ">>,>>>,>>9-"  no-undo.
def var tot-4-sls      as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-5-sls      as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-6-sls      as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-7-sls      as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-8-sls      as dec format ">>>,>>>,>>9-"  no-undo.

def var tot-1-cst      as dec format ">>,>>>,>>9-"  no-undo.
def var tot-2-cst      as dec format ">>,>>>,>>9-"  no-undo.
def var tot-3-cst      as dec format ">>,>>>,>>9-"  no-undo.
def var tot-4-cst      as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-5-cst      as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-6-cst      as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-7-cst      as dec format ">>>,>>>,>>9-"  no-undo.
def var tot-8-cst      as dec format ">>>,>>>,>>9-"  no-undo.

def var alloth-1-sls   as dec format ">>>,>>>,>>9-"  no-undo.
def var alloth-2-sls   as dec format ">>>,>>>,>>9-"  no-undo.
def var alloth-3-sls   as dec format ">>>,>>>,>>9-"  no-undo.
def var alloth-4-sls   as dec format ">>>,>>>,>>9-"  no-undo.
def var alloth-5-sls   as dec format ">>>,>>>,>>9-"  no-undo.
def var alloth-6-sls   as dec format ">>>,>>>,>>9-"  no-undo.
def var alloth-7-sls   as dec format ">>>,>>>,>>9-"  no-undo.
def var alloth-8-sls   as dec format ">>>,>>>,>>9-"  no-undo.

def var alloth-1-cst   as dec format ">>,>>>,>>9-"  no-undo.
def var alloth-2-cst   as dec format ">>,>>>,>>9-"  no-undo.
def var alloth-3-cst   as dec format ">>,>>>,>>9-"  no-undo.
def var alloth-4-cst   as dec format ">>>,>>>,>>9-"  no-undo.
def var alloth-5-cst   as dec format ">>>,>>>,>>9-"  no-undo.
def var alloth-6-cst   as dec format ">>>,>>>,>>9-"  no-undo.
def var alloth-7-cst   as dec format ">>>,>>>,>>9-"  no-undo.
def var alloth-8-cst   as dec format ">>>,>>>,>>9-"  no-undo.

def var top25-1-sls    as dec format ">>,>>>,>>9-"  no-undo.
def var top25-2-sls    as dec format ">>,>>>,>>9-"  no-undo.
def var top25-3-sls    as dec format ">>,>>>,>>9-"  no-undo.
def var top25-4-sls    as dec format ">>>,>>>,>>9-"  no-undo.
def var top25-5-sls    as dec format ">>>,>>>,>>9-"  no-undo.
def var top25-6-sls    as dec format ">>>,>>>,>>9-"  no-undo.
def var top25-7-sls    as dec format ">>>,>>>,>>9-"  no-undo.
def var top25-8-sls    as dec format ">>>,>>>,>>9-"  no-undo.

def var top25-1-cst    as dec format ">>,>>>,>>9-"  no-undo.
def var top25-2-cst    as dec format ">>,>>>,>>9-"  no-undo.
def var top25-3-cst    as dec format ">>,>>>,>>9-"  no-undo.
def var top25-4-cst    as dec format ">>>,>>>,>>9-"  no-undo.
def var top25-5-cst    as dec format ">>>,>>>,>>9-"  no-undo.
def var top25-6-cst    as dec format ">>>,>>>,>>9-"  no-undo.
def var top25-7-cst    as dec format ">>>,>>>,>>9-"  no-undo.
def var top25-8-cst    as dec format ">>>,>>>,>>9-"  no-undo.

def var dist-gm-plan   as dec format ">>>.99-"      no-undo.
def var reg-gm-plan    as dec format ">>>.99-"      no-undo.
def var tot-gm-plan    as dec format ">>>.99-"      no-undo.
def var alloth-gm-plan as dec format ">>>.99-"      no-undo.
def var top25-gm-plan  as dec format ">>>.99-"      no-undo.
def var rep-gm-plan    as dec format ">>>.99-"      no-undo.

def var dist-cnt       as int                     no-undo.
def var reg-cnt        as int                     no-undo.
def var tot-cnt        as int                     no-undo.
def var alloth-cnt     as int                     no-undo.
def var top25-cnt      as int                     no-undo.
def var rep-cnt        as int                     no-undo.

def var w-gm-mo-1      as dec format ">>>.99-"      no-undo.
def var w-gm-mo-2      as dec format ">>>.99-"      no-undo.
def var w-gm-mo-3      as dec format ">>>.99-"      no-undo.
                    
def var gm1            as dec format ">>>.99-"      no-undo.
def var gm2            as dec format ">>>.99-"      no-undo.
def var gm3            as dec format ">>>.99-"      no-undo.
def var gm4            as dec format ">>>.99-"      no-undo.
def var gm5            as dec format ">>>.99-"      no-undo.
def var gm6            as dec format ">>>.99-"      no-undo.
def var gm7            as dec format ">>>.99-"      no-undo.

def var k              as int                        no-undo.

def var x-divno        as char format "x(27)"        no-undo.
def var x-slsrep       as char format "x(35)"        no-undo.
def var x-tech         as char format "x(29)"        no-undo.
def var x-mo-1         as char format "x(18)"        no-undo.
def var x-mo-2         as char format "x(18)"        no-undo.
def var x-mo-3         as char format "x(18)"        no-undo.
def var x-fmo-1        as char format "x(18)"        no-undo.
def var x-fmo-2        as char format "x(18)"        no-undo.
def var x-fmo-3        as char format "x(18)"        no-undo.

def var line-max       as int init 42                no-undo.
def var line-cntr      as int                        no-undo.
def var yrz            as int extent 2               no-undo.
def var inz            as int                        no-undo.


def var b-rep          like oeel.slsrepout           no-undo.
def var e-rep          like oeel.slsrepout           no-undo.
def var b-slsgrp       like smsn.slstype             no-undo.
def var e-slsgrp       like smsn.slstype             no-undo.
def var b-grpcode      as char format "x(4)"         no-undo.
def var e-grpcode      as char format "x(4)"         no-undo.
def var b-slsmgr       like smsn.mgr                 no-undo.
def var e-slsmgr       like smsn.mgr                 no-undo.
def var b-region       as char format "x"            no-undo.
def var e-region       as char format "x"            no-undo.
def var b-dist         as dec format "999"           no-undo.
def var e-dist         as dec format "999"           no-undo.
def var p-sortfl       as char format "x"            no-undo.
def var p-obso         as logical                    no-undo.
def var p-rptfl        as char format "x"            no-undo.
def var p-arsco        as logical                    no-undo.
def var p-inside       as logical                    no-undo.
def var dhis           as logical no-undo.
def var dhis_v         as character format "x(3)" no-undo.
/* ------------------------------------------------------------------------
   Variables Used To Create Trend Like Header
   -----------------------------------------------------------------------*/



define variable rptdate     as date no-undo.
DEFINE VARIABLE rpt-dow     as char format "x(3)"   no-undo.
DEFINE VARIABLE rpt-time    as char format "x(5)"  no-undo.
DEFINE VARIABLE rpt-cono    like oeeh.cono         no-undo.
DEFINE VARIABLE rpt-user    as char format "x(08)" no-undo.
DEFINE VARIABLE x-title     as char format "x(50)" no-undo.
DEFINE VARIABLE x-page      as integer format ">>>9" no-undo.
DEFINE VARIABLE dow-lst     as character format "x(30)"
           init "Sun,Mon,Tue,Wed,Thu,Fri,Sat"                 no-undo.

DEFINE VARIABLE monthname   as char format "x(10)" EXTENT 12 initial
["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"].




def  new shared temp-table zsmsep no-undo 
  Field cono        as integer
  Field yr          as integer format "99"
  Field custno      as decimal format "zzzzzzzzzzz9"
  Field shipto      as char format "x(8)"
  Field lookupnm    like arsc.lookupnm
  Field slsrep      as char format "x(4)"
  Field region      as char format "x(1)"
  Field district    as char format "x(3)"
  Field salesterr   as char format "x(4)"
  Field salesamt    as decimal format "zzzzzzzz9.99-" extent 13
                       init [0,0,0,0,0,0,0,0,0,0,0,0] 
  Field discamt     as decimal format "zzzzzzzz9.99-" extent 13
                       init [0,0,0,0,0,0,0,0,0,0,0,0]
  Field qtysold     as decimal format "zzzzzzzz9.99-" extent 13
                       init [0,0,0,0,0,0,0,0,0,0,0,0] 
  Field cogamt      as decimal format "zzzzzzzz9.99-" extent 13
                       init [0,0,0,0,0,0,0,0,0,0,0,0] 
  Field prntfl      as logical
  
  index k-prodcat
    cono
    yr
    custno
    shipto
    slsrep.


def buffer c-smsc for zsmsep.

def new shared temp-table ysmsq no-undo
  field  cono like       zsmsq.cono 
  field  year like       zsmsq.year 
  field  divno like      zsmsq.divno 
  field  quotaty like    zsmsq.quotaty 
  field  primarykey like zsmsq.primarykey 
  field  quotaamt like   zsmsq.quotaamt
  field  quotagp like    zsmsq.quotagp
 index y-inx 
         cono
         year
         divno
         quotaty
         primarykey.

def buffer c-zsmsq for ysmsq.
def buffer l-zsmsq for ysmsq.





def temp-table tt-sls-det no-undo
    field cono       like arsc.cono
    field divno      like arsc.divno
    field region     as char format "x"
    field dist       as int format "999"
    field slsrep     like arsc.slsrepout
    field repnm      like smsn.name
    field tech       like arsc.salesterr
    field technm     as char format "x(24)"
    field custno     like arsc.custno
    field custnm     like arsc.lookupnm
    field budgeted   as logical
    field mo-1-sls   as dec format ">>,>>>,>>9-"
    field mo-1-cst   as dec format ">>,>>>,>>9-"
    field mo-2-sls   as dec format ">>,>>>,>>9-"
    field mo-2-cst   as dec format ">>,>>>,>>9-"
    field mo-3-sls   as dec format ">>,>>>,>>9-"
    field mo-3-cst   as dec format ">>,>>>,>>9-"
    field sls-annual as dec format ">>>,>>>,>>9-"
    field cst-annual as dec format ">>>,>>>,>>9-"
    field sls-ytd    as dec format ">>>,>>>,>>9-"
    field sls-key    as dec format ">>>,>>>,>>9-"
    field cst-ytd    as dec format ">>>,>>>,>>9-"
    field sls-lst-yr as dec format ">>>,>>>,>>9-"
    field cst-lst-yr as dec format ">>>,>>>,>>9-"
    field sls-lst-tyr as dec format ">>>,>>>,>>9-"
    field cst-lst-tyr as dec format ">>>,>>>,>>9-"
    field sls-plan   as dec format ">>>,>>>,>>9-"
    field gm-plan    as dec format ">>>.99-"
       index k-lookup is unique primary
        cono
        region
        dist
        slsrep
        custno

       index k-det1
        cono
        region
        dist
        slsrep
        sls-key descending.
        
def temp-table tt-sls-sum no-undo
    field cono       like arsc.cono
    field divno      like arsc.divno
    field region     as char format "x"
    field dist       as int format "999"
    field slsrep     like arsc.slsrepout
    field repnm      as char format "x(20)"
    field tech       like arsc.salesterr
    field technm     as char format "x(24)"
    field custno     like arsc.custno
    field custnm     like arsc.lookupnm
    field mo-1-sls   as dec format ">>,>>>,>>9-"
    field mo-1-cst   as dec format ">>,>>>,>>9-"
    field mo-2-sls   as dec format ">>,>>>,>>9-"
    field mo-2-cst   as dec format ">>,>>>,>>9-"
    field mo-3-sls   as dec format ">>,>>>,>>9-"
    field mo-3-cst   as dec format ">>,>>>,>>9-"
    field sls-annual as dec format ">>>,>>>,>>9-"
    field cst-annual as dec format ">>>,>>>,>>9-"
    field sls-ytd    as dec format ">>>,>>>,>>9-"
    field sls-key    as dec format ">>>,>>>,>>9-"
    field cst-ytd    as dec format ">>>,>>>,>>9-"
    field sls-lst-yr as dec format ">>>,>>>,>>9-"
    field cst-lst-yr as dec format ">>>,>>>,>>9-"
    field sls-lst-tyr as dec format ">>>,>>>,>>9-"
    field cst-lst-tyr as dec format ">>>,>>>,>>9-"
    field sls-plan   as dec format ">>>,>>>,>>9-"
    field gm-plan    as dec format ">>>.99-"
    field count      as int
      index k-slookup is unique primary
        cono
        region
        dist
        slsrep
        custno


      index k-sdet1
        cono
        region
        dist
        sls-key descending.

form 
  header
  "~033~105~033~046~154~061~117~033~046~153~062~123~033~046~154~060~105" at 1
         with frame kheadings1 down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.
form 
         rptdate             at 1 space
         rpt-dow             space
         rpt-time            space(3)
         "Company:"          space
         rpt-cono            space
         "Function: smrzc"   space
         "Operator:"         space
         rpt-user            space
         "page:"              at 169
         x-page              at 174
         "~015"              at 178
         x-title             at 50
         "~015"              at 178
                
         with frame kheadings1a down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.
         


form  
    "Region:"         at 1
    x-region          at 11
    "District:"       at 39
    x-dist            at 50
    "~015"            at 178
    "Sales Rep:"      at 1
    x-slsrep          at 12
    "~015"            at 178
    with  frame f-dettitle down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.


form 
    "Region:"         at 1
    x-region          at 11
    "District:"       at 39
    x-dist            at 50
    "~015"            at 178
    with frame f-sumtitle  down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.

form 
    x-mo-1            at 51
    x-mo-2            at 71
    x-mo-3            at 91
    "Sales"           at 134
    "Annualized"      at 155
    "~015"            at 178
    "Customer"        at 5
    "Name"            at 14
    "Sales Rep"       at 30
    "Sales"           at 56 
    "GM %"            at 65
    "Sales"           at 76
    "GM %"            at 85
    "Sales"           at 96
    "GM %"            at 105
    "Sales YTD"       at 113
    "GM %"            at 126
    "Last Year"       at 134
    "GM %"            at 147
    "Sales"           at 155
    "GM %"            at 168
    "~015"            at 178
    "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "------------------------------------------------" at 131
    "~015" at 178
         with frame f-sumhdr  down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.


form 
    x-mo-1            at 35
    x-mo-2            at 55
    x-mo-3            at 74
    "Sales"           at 117
    "Annualized"      at 135
    "~015"            at 178
    "Customer"        at 5
    "Name"            at 14
    "Tech"            at 30
    "Sales"           at 40 
    "GM %"            at 48
    "Sales"           at 60
    "GM %"            at 68
    "Sales"           at 79
    "GM %"            at 87
    "Sales YTD"       at 96
    "GM %"            at 108
    "Last Year"       at 117
    "GM %"            at 128
    "Sales"           at 135
    "GM %"            at 148
    "Tot Ly Sls"      at 154
    "GM %"            at 168
    "~015"            at 178
    "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "------------------------------------------------" at 131
    "~015" at 178
         with frame f-dethdr  down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.



form                   
    tt-sls-det.custno     at 1
    tt-sls-det.custnm     at 14 
    tt-sls-det.tech       at 30
    tt-sls-det.mo-1-sls   at 35
    gm1                   at 46
    tt-sls-det.mo-2-sls   at 55
    gm2                   at 66
    tt-sls-det.mo-3-sls   at 74
    gm3                   at 85 
    tt-sls-det.sls-ytd    at 94
    gm4                   at 106
    tt-sls-det.sls-lst-yr at 114
    gm5                   at 126
    tt-sls-det.sls-annual at 134
    gm6                   at 146
    tt-sls-det.sls-lst-tyr at 154
    gm7                   at 166
    "~015"                at 178
         with frame f-detdtl  down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.



form                  
    tt-sls-sum.custno     at 1
    tt-sls-sum.custnm     at 14 
    tt-sls-sum.repnm      at 30
    tt-sls-sum.mo-1-sls   at 51
    gm1                   at 63
    tt-sls-sum.mo-2-sls   at 71
    gm2                   at 83
    tt-sls-sum.mo-3-sls   at 91
    gm3                   at 103 
    tt-sls-sum.sls-ytd    at 111
    gm4                   at 124
    tt-sls-sum.sls-lst-yr at 132
    gm5                   at 145
    tt-sls-sum.sls-annual at 153
    gm6                   at 166
    "~015"                at 178
         with  frame f-sumdtl down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.


form 
    "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "-----------------------------------------------" at 131
    "~015"             at 178
    "District Totals:" at 18
    disttot-1-sls      at 35
    gm1                at 46
    disttot-2-sls      at 55
    gm2                at 66
    disttot-3-sls      at 74
    gm3                at 85
    disttot-4-sls      at 94
    gm4                at 106
    disttot-5-sls      at 114
    gm5                at 126
    disttot-6-sls      at 134
    gm6                at 146
    disttot-8-sls      at 154
    gm7                at 166
    "~015"             at 178
         with frame f-dtldisttot down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.



form 
    "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "-----------------------------------------------" at 131
    "~015"        at 178
    with frame f-dtlreptotu down STREAM-IO NO-BOX NO-LABELS no-underline
    WIDTH 178.
    
form
    tt-sls-det.slsrep at 1 format "x(4)"
    tt-sls-det.repnm  at 6 format "x(17)" 
    "Rep Totals:" at 23
    reptot-1-sls  at 35
    gm1           at 46
    reptot-2-sls  at 55
    gm2           at 66
    reptot-3-sls  at 74
    gm3           at 85
    reptot-4-sls  at 94
    gm4           at 106
    reptot-5-sls  at 114
    gm5           at 126
    reptot-6-sls  at 134
    gm6           at 146
    reptot-8-sls  at 154
    gm7           at 166
    "~015"        at 178
         with frame f-dtlreptot down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.


form 
    "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "-----------------------------------------------" at 131
    "~015"             at 178
    "District Totals:" at 18
    disttot-1-sls      at 51
    gm1                at 63
    disttot-2-sls      at 71
    gm2                at 83
    disttot-3-sls      at 91
    gm3                at 103
    disttot-4-sls      at 111
    gm4                at 124
    disttot-5-sls      at 132
    gm5                at 145
    disttot-6-sls      at 153
    gm6                at 166
    "~015"             at 178
         with frame f-sumdistot down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.



form 
    "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "-----------------------------------------------" at 131
    "~015"           at 178
    "Top 25 Totals:" at 36
    top25-1-sls      at 51
    gm1              at 63
    top25-2-sls      at 71
    gm2              at 83
    top25-3-sls      at 91
    gm3              at 103
    top25-4-sls      at 111
    gm4              at 124
    top25-5-sls      at 132
    gm5              at 145
    top25-6-sls      at 153
    gm6              at 166
    "~015"           at 178
         with frame f-sum25 down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.


form 
    "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "-----------------------------------------------" at 131
    "~015"           at 178
    "Top 100 Totals:" at 36
    top25-1-sls      at 51
    gm1              at 63
    top25-2-sls      at 71
    gm2              at 83
    top25-3-sls      at 91
    gm3              at 103
    top25-4-sls      at 111
    gm4              at 124
    top25-5-sls      at 132
    gm5              at 145
    top25-6-sls      at 153
    gm6              at 166
    "~015"           at 178
         with frame f-sum100 down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.


form 
    "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "-----------------------------------------------" at 131
    "~015"           at 178
    "Region Totals:" at 20
    regtot-1-sls     at 35
    gm1              at 46
    regtot-2-sls     at 55
    gm2              at 66
    regtot-3-sls     at 74
    gm3              at 85
    regtot-4-sls     at 94
    gm4              at 106
    regtot-5-sls     at 114
    gm5              at 126
    regtot-6-sls     at 134
    gm6              at 146
    regtot-8-sls     at 154
    gm7              at 166
    "~015"           at 178
         with frame f-dtlregtot down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.


form 
    "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "-----------------------------------------------" at 131
    "~015"
    "Region Totals:" at 20
    regtot-1-sls     at 51
    gm1              at 63
    regtot-2-sls     at 71
    gm2              at 83
    regtot-3-sls     at 91
    gm3              at 103
    regtot-4-sls     at 111
    gm4              at 124
    regtot-5-sls     at 132
    gm5              at 145
    regtot-6-sls     at 153
    gm6              at 166
    "~015"           at 178
         with frame f-sumregtot down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.


form 
    "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "-----------------------------------------------" at 131
    "~015"           at 178
    "Report Totals:" at 20
    tot-1-sls        at 35
    gm1              at 46
    tot-2-sls        at 55
    gm2              at 66
    tot-3-sls        at 74
    gm3              at 85
    tot-4-sls        at 94
    gm4              at 106
    tot-5-sls        at 114
    gm5              at 126
    tot-6-sls        at 134
    gm6              at 146
    tot-8-sls        at 154
    gm7              at 166
    "~015"           at 178
         with frame f-dtltot down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.


form 
    "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "-----------------------------------------------" at 131
    "~015"           at 178
    "Report Totals:" at 36
    tot-1-sls        at 51
    gm1              at 63
    tot-2-sls        at 71
    gm2              at 83
    tot-3-sls        at 91
    gm3              at 103
    tot-4-sls        at 111
    gm4              at 124
    tot-5-sls        at 132
    gm5              at 145
    tot-6-sls        at 153
    gm6              at 166
    "~015"           at 178
         with frame f-sumtot down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.



form 
   "-----------------------------------------------------------------" at 1
    "-----------------------------------------------------------------" at 66
    "-----------------------------------------------" at 131
    "~015"         at 178
    "All Others:"  at 39
    alloth-1-sls   at 51
    gm1            at 63
    alloth-2-sls   at 71
    gm2            at 83
    alloth-3-sls   at 91
    gm3            at 103
    alloth-4-sls   at 111
    gm4            at 124
    alloth-5-sls   at 132
    gm5            at 145
    alloth-6-sls   at 153
    gm6            at 166
    "~015"         at 178
         with frame f-alloth down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.



assign b-rep    = sapb.rangebeg[1]
       e-rep    = sapb.rangeend[1]
       b-slsgrp = sapb.rangebeg[2]
       e-slsgrp = sapb.rangeend[2]
       b-slsmgr = sapb.rangebeg[3]
       e-slsmgr = sapb.rangeend[3]
       b-region = sapb.rangebeg[4] 
       e-region = sapb.rangeend[4]
       b-dist   = dec(sapb.rangebeg[5])
       e-dist   = dec(sapb.rangeend[5])
       b-grpcode = sapb.rangebeg[6]
       e-grpcode = sapb.rangeend[6].
     


if sapb.optvalue[1] = "" then
  do:
  v-perin = "****".
  {p-rptper.i}
  end.
else
    do:
    v-perin = sapb.optvalue[1].
    {p-rptper.i}
    if string(v-perout) = v-lowper then
      do:
      v-perin = "****".
      {p-rptper.i}
      end.
    end.

  rpt-dt =  date(int(substring(string(int(v-perout),"9999"),1,2)),
                 1,
                (if int(substring(string(int(v-perout),"9999"),3,2)) < 50 then
                   int(substring(string(int(v-perout),"9999"),3,2)) +  2000
                 else   
                   int(substring(string(int(v-perout),"9999"),3,2)) + 1900)).
  assign
       x-fileout    = if sapb.optvalue[11] <> "" then
                         sapb.optvalue[11] + ".smrzc"
                      else
                         ""
       p-sortfl = sapb.optvalue[3]
       p-rptfl  = sapb.optvalue[4]
       dhis_v    = sapb.optvalue[5]
       p-arsco   = if sapb.optvalue[6] = "yes" then
                     true
                   else
                     false
       p-obso   = if sapb.optvalue[7] = "yes" then
                     true
                  else
                     false
       p-inside  = if sapb.optvalue[8] = "yes" then
                     true
                   else
                     false
       rpt-mo   = MONTH(rpt-dt)
       rpt-yr   = YEAR(rpt-dt)
       end-dt   = rpt-dt.
      
      
assign dhis = false.
if dhis_v = "yes" then
  dhis = true.

forbegdate = date (01,01,(rpt-yr - 1)).
if rpt-yr <= 2000 then
   do:
   lst-yr = rpt-yr - 1901.
   if rpt-yr < 2000 then
     tyr = rpt-yr - 1900.
   else
     tyr = rpt-yr - 2000.
   end.
else
   do:
   lst-yr = rpt-yr - 2001.
   tyr = rpt-yr - 2000.
   end.
IF rpt-mo = 12 THEN 
    ASSIGN moend-dt = DATE(01,01,rpt-yr + 1) - 1.
ELSE 
    ASSIGN moend-dt = DATE (rpt-mo + 1,01,rpt-yr) - 1.

IF moend-dt = end-dt THEN
   ASSIGN ytd-mos = rpt-mo.
ELSE DO:
   ASSIGN ytd-mos = rpt-mo - 1.
   
   IF ytd-mos = 0 THEN
      ASSIGN rpt-ytd-hdr1 = "(Annualization"
             rpt-ytd-hdr2 = " Not possible)"
             ytd-mos      = 1.
   ELSE
      ASSIGN moend-dt     = DATE(rpt-mo,01,rpt-yr) - 1
             rpt-ytd-hdr1 = "Annualized As "
             rpt-ytd-hdr2 = "of " + STRING(moend-dt,"99/99/9999").
END.             

IF rpt-mo < 3 THEN
   ASSIGN beg-dt = DATE(rpt-mo + 10,01,rpt-yr - 1).
ELSE 
   ASSIGN beg-dt = DATE(rpt-mo - 2,01,rpt-yr).

ASSIGN mo-1 = MONTH(end-dt)
       yr-1 = YEAR(end-dt)
       mo-3 = MONTH(beg-dt)
       yr-3 = YEAR(beg-dt).

IF lst-yr LT 1998 THEN DO:
   
   IF yr-1 = yr-3 THEN
      ASSIGN beg-dt = DATE(01,01,yr-1).

END. /* if lst-yr lt 1998 */
ELSE
   ASSIGN beg-dt = DATE(01,01,lst-yr).

IF mo-1 = 01 THEN 
   ASSIGN mo-2 = 12
          yr-2 = yr-3.
ELSE 
   ASSIGN mo-2 = mo-1 - 1
          yr-2 = yr-1.

if mo-1 = 01 then
   assign
       x-mo-1 = "<-----January---->"
       x-mo-2 = "<----December---->"
       x-mo-3 = "<----November---->".
             
if mo-1 = 02 then
   assign 
       x-mo-1 = "<----Febuary----->"
       x-mo-2 = "<----January----->"
       x-mo-3 = "<----December---->".

if mo-1 = 03 then
   assign 
       x-mo-1 = "<-----March------>"
       x-mo-2 = "<----Febuary----->"
       x-mo-3 = "<----January----->".
             
if mo-1 = 04 then
   assign 
       x-mo-1 = "<-----April------>"
       x-mo-2 = "<------March----->"
       x-mo-3 = "<----Febuary----->".

if mo-1 = 05 then
   assign 
       x-mo-1 = "<-------May------>"
       x-mo-2 = "<------April----->"
       x-mo-3 = "<------March----->".

if mo-1 = 06 then
   assign 
       x-mo-1 = "<------June------>"
       x-mo-2 = "<-------May------>"
       x-mo-3 = "<------April----->".

if mo-1 = 07 then
   assign 
       x-mo-1 = "<------July------>"
       x-mo-2 = "<------June------>"
       x-mo-3 = "<-------May------>".

if mo-1 = 08 then
   assign 
       x-mo-1 = "<-----August----->"
       x-mo-2 = "<------July------>"
       x-mo-3 = "<------June------>".

if mo-1 = 09 then
   assign 
       x-mo-1 = "<----September--->"
       x-mo-2 = "<-----August----->"
       x-mo-3 = "<------July------>".
          
if mo-1 = 10 then
   assign 
       x-mo-1 = "<-----October---->"
       x-mo-2 = "<----September--->"
       x-mo-3 = "<-----August----->".

if mo-1 = 11 then
   assign 
       x-mo-1 = "<----November---->"
       x-mo-2 = "<-----October---->"
       x-mo-3 = "<----September--->".

if mo-1 = 12 then
   assign 
       x-mo-1 = "<----December---->"
       x-mo-2 = "<----November---->"
       x-mo-3 = "<-----October---->".
slshld = "~~~~".

if x-fileout <> "" then do:
  assign x-exportfl = true.
  output stream x-out to value("/usr/tmp/" + x-fileout).

  if mo-1 = 01 then
    assign
       x-fmo-1 = "January"
       x-fmo-2 = "December"
       x-fmo-3 = "November".
             
  if mo-1 = 02 then
    assign 
       x-fmo-1 = "Febuary"
       x-fmo-2 = "January"
       x-fmo-3 = "December".

  if mo-1 = 03 then
    assign 
       x-fmo-1 = "March"
       x-fmo-2 = "Febuary"
       x-fmo-3 = "January".
             
  if mo-1 = 04 then
    assign 
       x-fmo-1 = "April"
       x-fmo-2 = "March"
       x-fmo-3 = "Febuary".

  if mo-1 = 05 then
    assign 
       x-fmo-1 = "May"
       x-fmo-2 = "April"
       x-fmo-3 = "March".

  if mo-1 = 06 then
    assign 
       x-fmo-1 = "June"
       x-fmo-2 = "May"
       x-fmo-3 = "April".

  if mo-1 = 07 then
    assign 
       x-fmo-1 = "July"
       x-fmo-2 = "June"
       x-fmo-3 = "May".

  if mo-1 = 08 then
    assign 
       x-fmo-1 = "August"
       x-fmo-2 = "July"
       x-fmo-3 = "June".

  if mo-1 = 09 then
    assign 
       x-fmo-1 = "September"
       x-fmo-2 = "August"
       x-fmo-3 = "July".
          
  if mo-1 = 10 then
    assign 
       x-fmo-1 = "October"
       x-fmo-2 = "September"
       x-fmo-3 = "August".

  if mo-1 = 11 then
    assign 
       x-fmo-1 = "November"
       x-fmo-2 = "October"
       x-fmo-3 = "September".

  if mo-1 = 12 then
    assign 
       x-fmo-1 = "December"
       x-fmo-2 = "November"
       x-fmo-3 = "October".

  if can-do("y,l",p-sortfl) then do:
    export stream x-out delimiter "~011"
            "Total"
            "Region"
            "District"
            "SalesRep"
            "SalesRepName"
            "CustomerNbr"     
            "CustomerName"     
            "Territory"
            x-fmo-1 + " Sales"   
            x-fmo-1 + " Margin"   
            x-fmo-1 + " Margin%"   
            x-fmo-2 + " Sales"   
            x-fmo-2 + " Margin"   
            x-fmo-2 + " Margin%"   
            x-fmo-3 + " Sales"   
            x-fmo-3 + " Margin"   
            x-fmo-3 + " Margin%"   
            "Ytd Sales"
            "Ytd Margin"
            "Ytd Margin%"
            "LYtd Sales"
            "LYtd Margin"
            "LYtd Margin%"
            "Annualized Sales"
            "Annualized Margin"
            "Annualized Margin%"
            "Last Year Sales"
            "Last Year Margin"
            "Last Year Margin%".
            
  end.
  else 
  if not can-do("y,l",p-sortfl) then do:
    export stream x-out delimiter "~011"
            "Total"
            "Region"
            "District"
            "SalesRep"
            "SalesRepName"
            "CustomerNbr"     
            "CustomerName"     
            "Territory"
            x-fmo-1 + " Sales"   
            x-fmo-1 + " Margin"   
            x-fmo-1 + " Margin%"   
            x-fmo-2 + " Sales"   
            x-fmo-2 + " Margin"   
            x-fmo-2 + " Margin%"   
            x-fmo-3 + " Sales"   
            x-fmo-3 + " Margin"   
            x-fmo-3 + " Margin%"   
            "Ytd Sales"
            "Ytd Margin"
            "Ytd Margin%"
            "LYtd Sales"
            "LYtd Margin"
            "LYtd Margin%"
            "Annualized Sales"
            "Annualized Margin"
            "Annualized Margin%"
            "Last Year Sales"
            "Last Year Margin"
            "Last Year Margin%".
  end.



end. 
      


 
 
 
 /*----------------------------------------------------------------------*/

rpt-user = userid("dictdb").
rptdate = today.
rpt-time = string(time, "HH:MM").
rpt-dow = entry(weekday(today),dow-lst).
rpt-cono = g-cono.
x-page = 0.
x-title = "Customer Performance Report".


/*-----------------------main ------------------------------------------*/
/* --New 
if p-obso = "no " then
  run smrzccost.p.
*/  

run smrzcmorph.p ( input b-region,
                 input e-region,
                 input b-dist,
                 input e-dist,
                 input b-rep,
                 input e-rep,
                 input b-slsmgr,
                 input e-slsmgr,
                 input b-slsgrp,
                 input e-slsgrp,
                 input b-grpcode,
                 input e-grpcode,
                 input p-inside,
                 input p-obso,
                 input p-arsco,
                 input (if tyr < 50 then
                           tyr + 2000
                        else
                           tyr + 1900),
                 input (if lst-yr < 50 then
                           lst-yr + 2000
                        else
                           lst-yr + 1900)).
                 
yrz[1] = lst-yr.
yrz[2] = tyr.
do inz = 1 to 2:
repyr = yrz[inz].
FOR EACH c-smsc
   WHERE c-smsc.cono = g-cono
        AND c-smsc.yr   = repyr no-lock:
     find c-zsmsq
       where      c-zsmsq.cono       = g-cono 
              and c-zsmsq.year       = rpt-yr
              and c-zsmsq.divno      = 0
              and c-zsmsq.quotaty    = "IC"
              and c-zsmsq.primarykey =  string(c-smsc.custno,"999999999999") +
                         c-smsc.shipto
              no-lock no-error.
     
            find l-zsmsq
            where l-zsmsq.cono       = g-cono
              and l-zsmsq.year       = (rpt-yr - 1)
              and l-zsmsq.divno      = 0
              and l-zsmsq.quotaty    = "IC"
              and l-zsmsq.primarykey =   
                string(c-smsc.custno,"999999999999") +
                         c-smsc.shipto
              no-lock no-error.
     


        assign w-salesterr = c-smsc.salesterr
               w-region    = c-smsc.region
               w-dist      = int(c-smsc.district)
               w-slsrep    = c-smsc.slsrep.
                              
       find smsn where smsn.cono = g-cono and
                       smsn.slsrep = w-slsrep no-lock no-error.



        find sasta
         where sasta.cono     = c-smsc.cono
          and sasta.codeiden = "z"
          and sasta.codeval  = w-salesterr
          no-lock no-error.
      
         
         find tt-sls-det use-index  k-lookup
        where tt-sls-det.cono       = c-smsc.cono
          and tt-sls-det.region     = w-region
          and tt-sls-det.dist       = w-dist
          and tt-sls-det.slsrep     = w-slsrep
          and tt-sls-det.custno     = c-smsc.custno
         exclusive-lock no-error.

         if not avail tt-sls-det then do:
            create tt-sls-det.
            assign tt-sls-det.cono   = c-smsc.cono
                   tt-sls-det.divno  = w-divno
                   tt-sls-det.slsrep = w-slsrep
                   tt-sls-det.repnm  = if avail smsn then smsn.name
                                       else "Unknown Rep"
                   tt-sls-det.tech   = w-salesterr
                   tt-sls-det.technm = if avail sasta then sasta.descrip
                                       else "Unknown Technology"
                   tt-sls-det.custno = c-smsc.custno
                   tt-sls-det.custnm = c-smsc.lookupnm
                   tt-sls-det.budgeted = false
                   tt-sls-det.region = w-region
                   tt-sls-det.dist   = w-dist
                   tt-sls-det.mo-1-sls = 0
                   tt-sls-det.mo-1-cst = 0
                   tt-sls-det.mo-2-sls = 0
                   tt-sls-det.mo-2-cst = 0
                   tt-sls-det.mo-3-sls = 0
                   tt-sls-det.mo-3-cst = 0 
                   tt-sls-det.sls-annual = 0
                   tt-sls-det.cst-annual = 0
                   tt-sls-det.sls-ytd  = 0 
                   tt-sls-det.cst-ytd  = 0 
                   tt-sls-det.sls-lst-yr = 0
                   tt-sls-det.cst-lst-yr = 0
                   tt-sls-det.sls-lst-tyr = 0
                   tt-sls-det.cst-lst-tyr = 0
                   tt-sls-det.sls-plan = 0
                   tt-sls-det.gm-plan  = 0.
         end. /* if not avail tt-sls-det */
         find tt-sls-sum  use-index k-slookup
           where tt-sls-sum.cono   = tt-sls-det.cono
           and tt-sls-sum.region = tt-sls-det.region
           and tt-sls-sum.dist   = tt-sls-det.dist
           and tt-sls-sum.slsrep = tt-sls-det.slsrep
           and tt-sls-sum.custno = tt-sls-det.custno
           exclusive-lock no-error.

         if not avail tt-sls-sum then do:
           create tt-sls-sum.
           assign tt-sls-sum.cono   = tt-sls-det.cono
             tt-sls-sum.divno  = tt-sls-det.divno
             tt-sls-sum.slsrep = tt-sls-det.slsrep
             tt-sls-sum.repnm  = tt-sls-det.repnm
             tt-sls-sum.tech   = tt-sls-det.tech
             tt-sls-sum.technm = tt-sls-det.technm
             tt-sls-sum.custno = tt-sls-det.custno
             tt-sls-sum.custnm = tt-sls-det.custnm
             tt-sls-sum.region = tt-sls-det.region
             tt-sls-sum.dist   = tt-sls-det.dist
             tt-sls-sum.mo-1-sls = 0
             tt-sls-sum.mo-1-cst = 0
             tt-sls-sum.mo-2-sls = 0
             tt-sls-sum.mo-2-cst = 0
             tt-sls-sum.mo-3-sls = 0
             tt-sls-sum.mo-3-cst = 0 
             tt-sls-sum.sls-annual = 0
             tt-sls-sum.cst-annual = 0
             tt-sls-sum.sls-ytd  = 0 
             tt-sls-sum.cst-ytd  = 0 
             tt-sls-sum.sls-lst-yr = 0
             tt-sls-sum.cst-lst-yr = 0
             tt-sls-sum.sls-lst-tyr = 0
             tt-sls-sum.cst-lst-tyr = 0
             tt-sls-sum.sls-plan = 0
             tt-sls-sum.gm-plan  = 0.

         end. /* if not avail tt-sls-det */
      
        if repyr = tyr  and tt-sls-det.budgeted = false then
          do:
          tt-sls-det.budgeted = true.
          find zsmsq
            where zsmsq.cono       = tt-sls-det.cono
              and zsmsq.year       = rpt-yr
              and zsmsq.divno      = tt-sls-det.divno
              and zsmsq.quotaty    = "CU"
              and zsmsq.primarykey = string(tt-sls-det.custno,"999999999999")
              no-lock no-error.
      
         if avail zsmsq then do:

            do k = 1 to rpt-mo:
              tt-sls-det.sls-plan = tt-sls-det.sls-plan + zsmsq.quotaamt[k].
              tt-sls-det.gm-plan  = tt-sls-det.gm-plan + zsmsq.quotagp[k].
              tt-sls-sum.sls-plan = tt-sls-sum.sls-plan + zsmsq.quotaamt[k].
              tt-sls-sum.gm-plan  = tt-sls-sum.gm-plan + zsmsq.quotagp[k].
            end. /* do k = 1 to rpt-mo */
           /*
           tt-sls-det.gm-plan = tt-sls-det.gm-plan / rpt-mo.
           */
           end.    /* if avail zsmsq */
         else 
           assign
            tt-sls-det.sls-plan = 0
            tt-sls-det.gm-plan  = 0.00
            tt-sls-sum.sls-plan = 0
            tt-sls-sum.gm-plan  = 0.00.
         end.
         if mo-1 ge 3 and repyr = tyr then
           do:
           ASSIGN tt-sls-det.mo-1-sls = tt-sls-det.mo-1-sls +
                         c-smsc.salesamt[mo-1] -
                          (if avail c-zsmsq then
                            c-zsmsq.quotaamt[mo-1]
                          else 
                            0)
                   tt-sls-det.mo-1-cst = tt-sls-det.mo-1-cst + 
                          c-smsc.cogamt[mo-1] -
                          (if avail c-zsmsq then
                            c-zsmsq.quotagp[mo-1]
                          else 
                            0)
 
                                  
                   tt-sls-det.mo-2-sls = tt-sls-det.mo-2-sls + 
                          c-smsc.salesamt[mo-2] -
                          (if avail c-zsmsq then
                            c-zsmsq.quotaamt[mo-2]
                          else 
                            0)
 
                   tt-sls-det.mo-2-cst = tt-sls-det.mo-2-cst + 
                          c-smsc.cogamt[mo-2] -
                          (if avail c-zsmsq then
                            c-zsmsq.quotagp[mo-2]
                          else 
                            0)
                                  
                   tt-sls-det.mo-3-sls = tt-sls-det.mo-3-sls + 
                          c-smsc.salesamt[mo-3] - 
                          (if avail c-zsmsq then
                            c-zsmsq.quotaamt[mo-3]
                          else 
                            0)
 
                   tt-sls-det.mo-3-cst = tt-sls-det.mo-3-cst + 
                          c-smsc.cogamt[mo-3] -
                          (if avail c-zsmsq then
                            c-zsmsq.quotagp[mo-3]
                          else 
                            0)
 

                   tt-sls-sum.mo-1-sls = tt-sls-sum.mo-1-sls +
                          c-smsc.salesamt[mo-1] -
                          (if avail c-zsmsq then
                            c-zsmsq.quotaamt[mo-1]
                          else 
                            0)
 
                   tt-sls-sum.mo-1-cst = tt-sls-sum.mo-1-cst + 
                          c-smsc.cogamt[mo-1] -
                         (if avail c-zsmsq then
                            c-zsmsq.quotagp[mo-1]
                          else 
                            0)
 
                   tt-sls-sum.mo-2-sls = tt-sls-sum.mo-2-sls + 
                          c-smsc.salesamt[mo-2] -
                          (if avail c-zsmsq then
                            c-zsmsq.quotaamt[mo-2]
                          else 
                            0)
                    tt-sls-sum.mo-2-cst = tt-sls-sum.mo-2-cst + 
                          c-smsc.cogamt[mo-2] -
                          (if avail c-zsmsq then
                            c-zsmsq.quotagp[mo-2]
                          else 
                            0)
         
                   tt-sls-sum.mo-3-sls = tt-sls-sum.mo-3-sls + 
                          c-smsc.salesamt[mo-3] -
                          (if avail c-zsmsq then
                            c-zsmsq.quotaamt[mo-3]
                          else 
                            0)
 
                   tt-sls-sum.mo-3-cst = tt-sls-sum.mo-3-cst + 
                          c-smsc.cogamt[mo-3] -
                         (if avail c-zsmsq then
                            c-zsmsq.quotagp[mo-3]
                          else 
                            0).
           end.                       

         if mo-1 = 2 and repyr = tyr then
           do:
           ASSIGN tt-sls-det.mo-1-sls = tt-sls-det.mo-1-sls +
                          c-smsc.salesamt[mo-1] -
                         (if avail c-zsmsq then
                            c-zsmsq.quotaamt[mo-1]
                          else 
                            0)
                   tt-sls-det.mo-1-cst = tt-sls-det.mo-1-cst + 
                          c-smsc.cogamt[mo-1] -
                         (if avail c-zsmsq then
                            c-zsmsq.quotagp[mo-1]
                          else 
                            0)
                   tt-sls-det.mo-2-sls = tt-sls-det.mo-2-sls + 
                          c-smsc.salesamt[mo-2] -
                         (if avail c-zsmsq then
                            c-zsmsq.quotaamt[mo-2]
                          else 
                            0)
                   tt-sls-det.mo-2-cst = tt-sls-det.mo-2-cst + 
                          c-smsc.cogamt[mo-2] -
                         (if avail c-zsmsq then
                            c-zsmsq.quotagp[mo-2]
                          else 
                            0)
                   tt-sls-sum.mo-1-sls = tt-sls-sum.mo-1-sls +
                          c-smsc.salesamt[mo-1] -
                         (if avail c-zsmsq then
                            c-zsmsq.quotaamt[mo-1]
                          else 
                            0)
                   tt-sls-sum.mo-1-cst = tt-sls-sum.mo-1-cst + 
                          c-smsc.cogamt[mo-1] -
                         (if avail c-zsmsq then
                            c-zsmsq.quotagp[mo-1]
                          else 
                            0)
                   tt-sls-sum.mo-2-sls = tt-sls-sum.mo-2-sls + 
                          c-smsc.salesamt[mo-2] -
                          (if avail c-zsmsq then
                            c-zsmsq.quotaamt[mo-2]
                          else 
                            0)
                   tt-sls-sum.mo-2-cst = tt-sls-sum.mo-2-cst + 
                          c-smsc.cogamt[mo-2] -
                         (if avail c-zsmsq then
                            c-zsmsq.quotagp[mo-2]
                          else 
                            0).
           end.
         if mo-1 = 2 and repyr = lst-yr then
           do:
           ASSIGN tt-sls-det.mo-3-sls = tt-sls-det.mo-3-sls +
                       c-smsc.salesamt[12] -
                      (if avail l-zsmsq then
                            l-zsmsq.quotaamt[12]
                          else 
                            0)
                   tt-sls-det.mo-3-cst = tt-sls-det.mo-3-cst + 
                       c-smsc.cogamt[12] -
                      (if avail l-zsmsq then
                            l-zsmsq.quotagp[12]
                          else 
                            0)
                   tt-sls-sum.mo-3-sls = tt-sls-sum.mo-3-sls +
                       c-smsc.salesamt[12] -
                      (if avail l-zsmsq then
                            l-zsmsq.quotaamt[12]
                          else 
                            0)
                   tt-sls-sum.mo-3-cst = tt-sls-sum.mo-3-cst + 
                       c-smsc.cogamt[12] -
                      (if avail l-zsmsq then
                            l-zsmsq.quotagp[12]
                          else 
                            0).
            end.                       
         
         if mo-1 = 1 and repyr = tyr then
           do:
           ASSIGN tt-sls-det.mo-1-sls = tt-sls-det.mo-1-sls +
                          c-smsc.salesamt[mo-1] -
                        (if avail c-zsmsq then
                            c-zsmsq.quotaamt[mo-1]
                          else 
                            0)
                   tt-sls-det.mo-1-cst = tt-sls-det.mo-1-cst + 
                          c-smsc.cogamt[mo-1] -
                         (if avail c-zsmsq then
                            c-zsmsq.quotagp[mo-1]
                          else 
                            0)
                   tt-sls-sum.mo-1-sls = tt-sls-sum.mo-1-sls +
                          c-smsc.salesamt[mo-1] -
                         (if avail c-zsmsq then
                            c-zsmsq.quotaamt[mo-1]
                          else 
                            0)
                   tt-sls-sum.mo-1-cst = tt-sls-sum.mo-1-cst + 
                          c-smsc.cogamt[mo-1] -
                         (if avail c-zsmsq then
                            c-zsmsq.quotagp[mo-1]
                          else 
                            0).
            end.
         if mo-1 = 1 and repyr = lst-yr  then
             do:
             ASSIGN tt-sls-det.mo-2-sls = tt-sls-det.mo-2-sls + 
                          c-smsc.salesamt[12] -
                         (if avail l-zsmsq then
                            l-zsmsq.quotaamt[12]
                          else 
                            0)
                     tt-sls-det.mo-2-cst = tt-sls-det.mo-2-cst + 
                           c-smsc.cogamt[12] -
                         (if avail l-zsmsq then
                            l-zsmsq.quotagp[12]
                          else 
                            0)
                     tt-sls-det.mo-3-sls = tt-sls-det.mo-3-sls + 
                           c-smsc.salesamt[11] -
                        (if avail l-zsmsq then
                            l-zsmsq.quotaamt[11]
                          else 
                            0)
                     tt-sls-det.mo-3-cst = tt-sls-det.mo-3-cst + 
                           c-smsc.cogamt[11] -
                         (if avail l-zsmsq then
                            l-zsmsq.quotagp[11]
                          else 
                            0)
                     tt-sls-sum.mo-2-sls = tt-sls-sum.mo-2-sls + 
                           c-smsc.salesamt[12] -
                        (if avail l-zsmsq then
                            l-zsmsq.quotaamt[12]
                          else 
                            0)
                     tt-sls-sum.mo-2-cst = tt-sls-sum.mo-2-cst + 
                           c-smsc.cogamt[12] -
                         (if avail l-zsmsq then
                            l-zsmsq.quotagp[12]
                          else 
                            0)
                     tt-sls-sum.mo-3-sls = tt-sls-sum.mo-3-sls + 
                           c-smsc.salesamt[11] -
                         (if avail l-zsmsq then
                            l-zsmsq.quotaamt[11]
                          else 
                            0)
                     tt-sls-sum.mo-3-cst = tt-sls-sum.mo-3-cst + 
                           c-smsc.cogamt[11] -
                         (if avail l-zsmsq then
                            l-zsmsq.quotagp[11]  
                          else 
                            0).
              end. 
           

         if repyr = tyr then
           do:
           do tx = 1 to rpt-mo:
         
             ASSIGN tt-sls-det.sls-ytd = tt-sls-det.sls-ytd + 
                          c-smsc.salesamt[tx] -
                        (if avail c-zsmsq then
                            c-zsmsq.quotaamt[tx]
                          else 
                            0)
                     tt-sls-det.cst-ytd = tt-sls-det.cst-ytd + 
                          c-smsc.cogamt[tx] -
                         (if avail c-zsmsq then
                            c-zsmsq.quotagp[tx]
                          else 
                            0)
                     tt-sls-sum.sls-ytd = tt-sls-sum.sls-ytd + 
                          c-smsc.salesamt[tx] -
                         (if avail c-zsmsq then
                            c-zsmsq.quotaamt[tx]
                          else 
                            0)
                     tt-sls-sum.cst-ytd = tt-sls-sum.cst-ytd + 
                          c-smsc.cogamt[tx] -
                         (if avail c-zsmsq then
                            c-zsmsq.quotagp[tx]
                          else 
                            0)
                     tt-sls-det.sls-annual =
                      tt-sls-det.sls-annual +
                      (((c-smsc.salesamt[tx] -
                           (if avail c-zsmsq then
                            c-zsmsq.quotaamt[tx]
                          else 
                            0)) / rpt-mo) * 12)
                    tt-sls-det.cst-annual =
                      tt-sls-det.cst-annual +
                      (((c-smsc.cogamt[tx] -
                        (if avail c-zsmsq then
                            c-zsmsq.quotagp[tx]
                          else 
                            0))
 
                       / rpt-mo) * 12)
                    tt-sls-sum.sls-annual =
                      tt-sls-sum.sls-annual +
                      (((c-smsc.salesamt[tx] -
                           (if avail c-zsmsq then
                            c-zsmsq.quotaamt[tx]
                           else 
                             0))
                       / rpt-mo) * 12)
                    tt-sls-sum.cst-annual =
                      tt-sls-sum.cst-annual +
                      (((c-smsc.cogamt[tx] -
                          (if avail c-zsmsq then
                            c-zsmsq.quotagp[tx]
                          else 
                            0))
                       / rpt-mo) * 12)
                   tt-sls-sum.sls-key = if can-do("y",p-sortfl) then
                                         tt-sls-sum.sls-ytd
                                       else
                                         tt-sls-sum.sls-key
                   tt-sls-det.sls-key = if can-do("y",p-sortfl) then
                                         tt-sls-det.sls-ytd
                                       else
                                         tt-sls-det.sls-key.

   
              end.
           end.
         if repyr = lst-yr then                 
          do: 
            do k = 1 to rpt-mo:
            assign tt-sls-det.sls-lst-yr = tt-sls-det.sls-lst-yr + 
                     (c-smsc.salesamt[k]) -
                        (if avail l-zsmsq then
                            l-zsmsq.quotaamt[k]
                          else 
                            0)
                   tt-sls-det.cst-lst-yr = tt-sls-det.cst-lst-yr + 
                     (c-smsc.cogamt[k]) -
                         (if avail l-zsmsq then
                            l-zsmsq.quotagp[k]
                          else 
                            0)
                   tt-sls-sum.sls-lst-yr = tt-sls-sum.sls-lst-yr + 
                     (c-smsc.salesamt[k]) -
                         (if avail l-zsmsq then
                            l-zsmsq.quotaamt[k]
                          else 
                            0)
                   tt-sls-sum.cst-lst-yr = tt-sls-sum.cst-lst-yr + 
                     (c-smsc.cogamt[k]) -
                        (if avail l-zsmsq then
                            l-zsmsq.quotagp[k]
                          else 
                            0)
                  tt-sls-sum.sls-key = if can-do("l",p-sortfl) then
                                         tt-sls-sum.sls-lst-yr
                                       else
                                         tt-sls-sum.sls-key
                  tt-sls-det.sls-key = if can-do("l",p-sortfl) then
                                         tt-sls-det.sls-lst-yr
                                       else
                                         tt-sls-det.sls-key.
                       
             end.
          end. /* for k = 1 to rpt-mo */
         if repyr = lst-yr then                 
          do: 
            do k = 1 to 12:
            assign tt-sls-det.sls-lst-tyr = tt-sls-det.sls-lst-tyr + 
                     (c-smsc.salesamt[k]) -
                        (if avail l-zsmsq then
                            l-zsmsq.quotaamt[k]
                          else 
                            0)
 
                  tt-sls-det.cst-lst-tyr = tt-sls-det.cst-lst-tyr + 
                     (c-smsc.cogamt[k]) -
                         (if avail l-zsmsq then
                            l-zsmsq.quotagp[k]
                          else 
                            0)
                   tt-sls-sum.sls-lst-tyr = tt-sls-sum.sls-lst-tyr + 
                     (c-smsc.salesamt[k]) -
                         (if avail l-zsmsq then
                            l-zsmsq.quotaamt[k]
                          else 
                            0)
                   tt-sls-sum.cst-lst-tyr = tt-sls-sum.cst-lst-tyr + 
                     (c-smsc.cogamt[k]) -
                        (if avail l-zsmsq then
                            l-zsmsq.quotagp[k]
                         else 
                            0).
 
            end.
          end. /* for k = 1 to rpt-mo */
       end.
      release tt-sls-det.
      release tt-sls-sum.
end.             /* for each oeel */

 
if can-do("y,l",p-sortfl) then do:
   
   if p-rptfl = "d" then do:
   
      for each tt-sls-det use-index k-det1
      no-lock break by tt-sls-det.region
                    by tt-sls-det.dist
                    by tt-sls-det.slsrep
                    by tt-sls-det.sls-key descending
                    by tt-sls-det.tech: 
   
        
      assign disttot-1-sls = disttot-1-sls + tt-sls-det.mo-1-sls
                disttot-2-sls = disttot-2-sls + tt-sls-det.mo-2-sls
                disttot-3-sls = disttot-3-sls + tt-sls-det.mo-3-sls
                disttot-4-sls = disttot-4-sls + tt-sls-det.sls-ytd
                disttot-5-sls = disttot-5-sls + tt-sls-det.sls-lst-yr
                disttot-6-sls = disttot-6-sls + tt-sls-det.sls-annual
                disttot-7-sls = disttot-7-sls + tt-sls-det.sls-plan
                disttot-8-sls = disttot-8-sls + tt-sls-det.sls-lst-tyr
                disttot-1-cst = disttot-1-cst + tt-sls-det.mo-1-cst
                disttot-2-cst = disttot-2-cst + tt-sls-det.mo-2-cst
                disttot-3-cst = disttot-3-cst + tt-sls-det.mo-3-cst
                disttot-4-cst = disttot-4-cst + tt-sls-det.cst-ytd
                disttot-5-cst = disttot-5-cst + tt-sls-det.cst-lst-yr
                disttot-6-cst = disttot-6-cst + tt-sls-det.cst-annual
                disttot-8-cst = disttot-8-cst + tt-sls-det.cst-lst-tyr
                dist-gm-plan  = dist-gm-plan + tt-sls-det.gm-plan
                dist-cnt      = dist-cnt + 1
                reptot-1-sls = reptot-1-sls + tt-sls-det.mo-1-sls
                reptot-2-sls = reptot-2-sls + tt-sls-det.mo-2-sls
                reptot-3-sls = reptot-3-sls + tt-sls-det.mo-3-sls
                reptot-4-sls = reptot-4-sls + tt-sls-det.sls-ytd
                reptot-5-sls = reptot-5-sls + tt-sls-det.sls-lst-yr
                reptot-6-sls = reptot-6-sls + tt-sls-det.sls-annual
                reptot-7-sls = reptot-7-sls + tt-sls-det.sls-plan
                reptot-8-sls = reptot-8-sls + tt-sls-det.sls-lst-tyr
                reptot-1-cst = reptot-1-cst + tt-sls-det.mo-1-cst
                reptot-2-cst = reptot-2-cst + tt-sls-det.mo-2-cst
                reptot-3-cst = reptot-3-cst + tt-sls-det.mo-3-cst
                reptot-4-cst = reptot-4-cst + tt-sls-det.cst-ytd
                reptot-5-cst = reptot-5-cst + tt-sls-det.cst-lst-yr
                reptot-6-cst = reptot-6-cst + tt-sls-det.cst-annual
                reptot-8-cst = reptot-8-cst + tt-sls-det.cst-lst-tyr
                rep-gm-plan  = rep-gm-plan + tt-sls-det.gm-plan
                rep-cnt      = rep-cnt + 1.

         if first-of (tt-sls-det.region)  or
            first-of (tt-sls-det.dist)    or
            first-of (tt-sls-det.slsrep)  then do:
            if dhis = true and 
               (first-of(tt-sls-det.slsrep) and
                not 
                first-of(tt-sls-det.dist)) then
              dhis = true.
            else
              page.
            assign 
               x-slsrep = string(tt-sls-det.slsrep) + " " + tt-sls-det.repnm
               x-region = tt-sls-det.region
               x-dist   = tt-sls-det.dist.
            if dhis = true and
               (first-of(tt-sls-det.slsrep) and
                not
                first-of(tt-sls-det.dist)) then
              dhis = true.
            else  
              run write-det-hdgs.

         end. /*if first-of (tt-sls-det.divno) or first-of (tt-sls-det.slsrep)*/
             
         if tt-sls-det.mo-1-sls > 0 then
            gm1 = round( (((tt-sls-det.mo-1-sls - tt-sls-det.mo-1-cst) 
                  / tt-sls-det.mo-1-sls) * 100) ,2).
         else
            gm1 = 0.
      
         if tt-sls-det.mo-2-sls > 0 then
            gm2 = round( (((tt-sls-det.mo-2-sls - tt-sls-det.mo-2-cst) 
                  / tt-sls-det.mo-2-sls) * 100) ,2).
         else 
            gm2 = 0.
      
         if tt-sls-det.mo-3-sls > 0 then
            gm3 = round( (((tt-sls-det.mo-3-sls - tt-sls-det.mo-3-cst) 
                  / tt-sls-det.mo-3-sls) * 100) ,2).
         else
            gm3 = 0.
      
         if tt-sls-det.sls-ytd > 0 then
            gm4 = round( (((tt-sls-det.sls-ytd - tt-sls-det.cst-ytd) 
                  / tt-sls-det.sls-ytd) * 100) ,2).
         else
            gm4 = 0.
       
         if tt-sls-det.sls-lst-yr > 0 then
            gm5 = round( (((tt-sls-det.sls-lst-yr - tt-sls-det.cst-lst-yr) 
                  / tt-sls-det.sls-lst-yr) * 100) ,2).
         else
            gm5 = 0.
         
         if tt-sls-det.sls-annual > 0 then
            gm6 = round( (((tt-sls-det.sls-annual - tt-sls-det.cst-annual) 
                 / tt-sls-det.sls-annual) * 100) ,2).
         else
            gm6 = 0.

         if tt-sls-det.sls-lst-tyr > 0 then
            gm7 = round( (((tt-sls-det.sls-lst-tyr -
                            tt-sls-det.cst-lst-tyr)
                 / tt-sls-det.sls-lst-tyr) * 100) ,2).
         else
            gm7 = 0.


         run fix-gm.                           
       
         if line-counter + 2 > page-size then do:
            page.
            run write-det-hdgs.
         end. /* if line-counter + 1 > page-size */

         if dhis = false then
           do:
           if x-exportfl then
           export stream x-out delimiter "~011"
            " "
            tt-sls-det.region
            tt-sls-det.dist
            tt-sls-det.slsrep
            tt-sls-det.repnm
            tt-sls-det.custno     
            tt-sls-det.custnm     
            tt-sls-det.tech
            tt-sls-det.mo-1-sls   
            (tt-sls-det.mo-1-sls - tt-sls-det.mo-1-cst)   
            gm1               
            tt-sls-det.mo-2-sls   
            (tt-sls-det.mo-2-sls - tt-sls-det.mo-2-cst)   
            gm2               
            tt-sls-det.mo-3-sls   
            (tt-sls-det.mo-3-sls - tt-sls-det.mo-3-cst)   
            gm3               
            tt-sls-det.sls-ytd    
            (tt-sls-det.sls-ytd - tt-sls-det.cst-ytd)    
            gm4               
            tt-sls-det.sls-lst-yr
            (tt-sls-det.sls-lst-yr - tt-sls-det.cst-lst-yr)   
            gm5               
            tt-sls-det.sls-annual
            (tt-sls-det.sls-annual - tt-sls-det.cst-annual)
            gm6
            tt-sls-det.sls-lst-tyr
            (tt-sls-det.sls-lst-tyr - tt-sls-det.cst-lst-tyr)
            gm7.
 
           
           disp
            tt-sls-det.custno     
            tt-sls-det.custnm     
            tt-sls-det.tech
            tt-sls-det.mo-1-sls   
            gm1               
            tt-sls-det.mo-2-sls   
            gm2               
            tt-sls-det.mo-3-sls   
            gm3               
            tt-sls-det.sls-ytd    
            gm4               
            tt-sls-det.sls-lst-yr 
            gm5               
            tt-sls-det.sls-annual
            gm6
         /*   tt-sls-det.sls-plan
              tt-sls-det.gm-plan */
            tt-sls-det.sls-lst-tyr
            gm7
           with frame f-detdtl down.
           down with frame f-detdtl.
           end.

         if last-of (tt-sls-det.slsrep) then do:
         
            if reptot-1-sls > 0 then
               gm1 = round( (((reptot-1-sls - reptot-1-cst) / reptot-1-sls) 
                               * 100) ,2).
            else
               gm1 = 0.
      
            if reptot-2-sls > 0 then
               gm2 = round( (((reptot-2-sls - reptot-2-cst) / reptot-2-sls) 
                               * 100) ,2).
            else 
               gm2 = 0.
      
            if reptot-3-sls > 0 then
               gm3 = round( (((reptot-3-sls - reptot-3-cst) / reptot-3-sls) 
                               * 100) ,2).
            else
               gm3 = 0.
      
            if reptot-4-sls > 0 then
               gm4 = round( (((reptot-4-sls - reptot-4-cst) / reptot-4-sls) 
                               * 100) ,2).
            else                        
               gm4 = 0.
    
            if reptot-5-sls > 0 then
               gm5 = round( (((reptot-5-sls - reptot-5-cst) / reptot-5-sls) 
                               * 100) ,2).
            else
               gm5 = 0.
            
            if reptot-6-sls > 0 then
               gm6 = round( (((reptot-6-sls - reptot-6-cst) / reptot-6-sls) 
                               * 100) ,2).
            else
               gm6 = 0.
            
            if reptot-8-sls > 0 then
               gm7 = round( (((reptot-8-sls - reptot-8-cst) / reptot-8-sls) 
                               * 100) ,2).
            else
               gm7 = 0.


            if rep-gm-plan > 0 then
               rep-gm-plan = round( (rep-gm-plan / rep-cnt) ,2).
            else 
               rep-gm-plan = 0.
            
            run fix-gm.

            if line-counter + 2 > page-size then do:
               page.
               run write-det-hdgs.
            end. /* if line-counter + 1 > page-size */

            if dhis = false then do:
              disp with frame f-dtlreptotu.         
              down with frame f-dtlreptotu.
              end.
            if x-exportfl then
            export stream x-out delimiter "~011"
               "SalesRep"
                tt-sls-det.region
                tt-sls-det.dist
                tt-sls-det.slsrep
                tt-sls-det.repnm
                " "     
                " "     
                " "
               reptot-1-sls
               (reptot-1-sls - reptot-1-cst)
               gm1
               reptot-2-sls
               (reptot-2-sls - reptot-2-cst)
               gm2
               reptot-3-sls
               (reptot-3-sls - reptot-3-cst)
               gm3
               reptot-4-sls
               (reptot-4-sls - reptot-4-cst)
               gm4
               reptot-5-sls
               (reptot-5-sls - reptot-5-cst)
               gm5
               reptot-6-sls
               (reptot-6-sls - reptot-6-cst)
               gm6
               reptot-8-sls
               (reptot-8-sls - reptot-8-cst)
               gm7.
             disp
               tt-sls-det.slsrep
               tt-sls-det.repnm
               reptot-1-sls
               gm1
               reptot-2-sls
               gm2
               reptot-3-sls
               gm3
               reptot-4-sls
               gm4
               reptot-5-sls
               gm5
               reptot-6-sls
               gm6
             /*  reptot-7-sls
                 rep-gm-plan   */
               reptot-8-sls
               gm7
            with frame f-dtlreptot down.
            down with frame f-dtlreptot.
            if sapb.optvalue[10] = "yes" then
              page.
            assign reptot-1-sls = 0
                   reptot-2-sls = 0
                   reptot-3-sls = 0
                   reptot-4-sls = 0
                   reptot-5-sls = 0
                   reptot-6-sls = 0
                   reptot-7-sls = 0
                   reptot-8-sls = 0
                   reptot-1-cst = 0
                   reptot-2-cst = 0
                   reptot-3-cst = 0
                   reptot-4-cst = 0
                   reptot-5-cst = 0
                   reptot-6-cst = 0
                   rep-gm-plan  = 0
                   reptot-8-cst = 0
                   rep-cnt      = 0.
       
         end. /* if last-of(tt-sls-det.slsrep) */

         if last-of (tt-sls-det.dist) then do:
           
            if disttot-1-sls > 0 then
               gm1 = round( (((disttot-1-sls - disttot-1-cst) / disttot-1-sls) 
                               * 100) ,2).
            else
               gm1 = 0.
      
            if disttot-2-sls > 0 then
               gm2 = round( (((disttot-2-sls - disttot-2-cst) / disttot-2-sls) 
                               * 100) ,2).
            else 
               gm2 = 0.
      
            if disttot-3-sls > 0 then
               gm3 = round( (((disttot-3-sls - disttot-3-cst) / disttot-3-sls) 
                               * 100) ,2).
            else
               gm3 = 0.
      
            if disttot-4-sls > 0 then
               gm4 = round( (((disttot-4-sls - disttot-4-cst) / disttot-4-sls) 
                               * 100) ,2).
            else
               gm4 = 0.
    
            if disttot-5-sls > 0 then
               gm5 = round( (((disttot-5-sls - disttot-5-cst) / disttot-5-sls) 
                               * 100) ,2).
            else
               gm5 = 0.
            
            if disttot-6-sls > 0 then
               gm6 = round( (((disttot-6-sls - disttot-6-cst) / disttot-6-sls) 
                               * 100) ,2).
            else
               gm6 = 0.

            if dist-gm-plan > 0 then
               dist-gm-plan = round( (dist-gm-plan / dist-cnt) ,2).
            else 
               dist-gm-plan = 0.

            if disttot-8-sls > 0 then
               gm7 = round( (((disttot-8-sls - disttot-8-cst) / disttot-8-sls) 
                               * 100) ,2).
            else
               gm7 = 0.

            
            run fix-gm.

            if line-counter + 2 > page-size then do:
               page.
               run write-det-hdgs.
            end. /* if line-counter + 1 > page-size */

            if x-exportfl then
              export stream x-out delimiter "~011"
               "District"
                tt-sls-det.region
                tt-sls-det.dist
                " "
                " "
                " "     
                " "     
                " "
               disttot-1-sls
               (disttot-1-sls - disttot-1-cst)
               gm1
               disttot-2-sls
               (disttot-2-sls - disttot-2-cst)
               gm2
               disttot-3-sls
               (disttot-3-sls - disttot-3-cst)
               gm3
               disttot-4-sls
               (disttot-4-sls - disttot-4-cst)
               gm4
               disttot-5-sls
               (disttot-5-sls - disttot-5-cst)
               gm5
               disttot-6-sls
               (disttot-6-sls - disttot-6-cst)
               gm6
               disttot-8-sls
               (disttot-8-sls - disttot-8-cst)
               gm7.
             
           
            disp
               disttot-1-sls
               gm1
               disttot-2-sls
               gm2
               disttot-3-sls
               gm3
               disttot-4-sls
               gm4
               disttot-5-sls
               gm5
               disttot-6-sls
               gm6
              /* disttot-7-sls
                 dist-gm-plan   */
               disttot-8-sls
               gm7

            with frame f-dtldisttot down.
            down with frame f-dtldisttot.
      
            assign regtot-1-sls  = regtot-1-sls + disttot-1-sls
                   regtot-2-sls  = regtot-2-sls + disttot-2-sls
                   regtot-3-sls  = regtot-3-sls + disttot-3-sls
                   regtot-4-sls  = regtot-4-sls + disttot-4-sls
                   regtot-5-sls  = regtot-5-sls + disttot-5-sls
                   regtot-6-sls  = regtot-6-sls + disttot-6-sls
                   regtot-7-sls  = regtot-7-sls + disttot-7-sls
                   regtot-8-sls  = regtot-8-sls + disttot-8-sls
                   regtot-1-cst  = regtot-1-cst + disttot-1-cst
                   regtot-2-cst  = regtot-2-cst + disttot-2-cst
                   regtot-3-cst  = regtot-3-cst + disttot-3-cst
                   regtot-4-cst  = regtot-4-cst + disttot-4-cst
                   regtot-5-cst  = regtot-5-cst + disttot-5-cst
                   regtot-6-cst  = regtot-6-cst + disttot-6-cst
                   regtot-8-cst  = regtot-8-cst + disttot-8-cst
                   reg-gm-plan   = reg-gm-plan + dist-gm-plan
                   reg-cnt       = reg-cnt + 1 
                   disttot-1-sls = 0
                   disttot-2-sls = 0
                   disttot-3-sls = 0
                   disttot-4-sls = 0
                   disttot-5-sls = 0
                   disttot-6-sls = 0
                   disttot-7-sls = 0
                   disttot-8-sls = 0
                   disttot-1-cst = 0
                   disttot-2-cst = 0
                   disttot-3-cst = 0
                   disttot-4-cst = 0
                   disttot-5-cst = 0
                   disttot-6-cst = 0
                   dist-gm-plan  = 0
                   disttot-8-cst = 0
                   dist-cnt      = 0.
       
         end. /* if last-of(tt-sls-det.dist) */

         if last-of (tt-sls-det.region) then do:
      
            if regtot-1-sls > 0 then
               gm1 = round( (((regtot-1-sls - regtot-1-cst) / regtot-1-sls) 
                               * 100) ,2).
            else
               gm1 = 0.
    
            if regtot-2-sls > 0 then
               gm2 = round( (((regtot-2-sls - regtot-2-cst) / regtot-2-sls) 
                               * 100) ,2).
            else 
               gm2 = 0.
       
            if regtot-3-sls > 0 then
               gm3 = round( (((regtot-3-sls - regtot-3-cst) / regtot-3-sls) 
                               * 100) ,2).
            else
               gm3 = 0.
      
            if regtot-4-sls > 0 then
               gm4 = round( (((regtot-4-sls - regtot-4-cst) / regtot-4-sls) 
                               * 100) ,2).
            else
               gm4 = 0.
    
            if regtot-5-sls > 0 then
               gm5 = round( (((regtot-5-sls - regtot-5-cst) / regtot-5-sls) 
                               * 100) ,2).
            else
               gm5 = 0.
           
            if regtot-6-sls > 0 then
               gm6 = round( (((regtot-6-sls - regtot-6-cst) / regtot-6-sls) 
                               * 100) ,2).
            else
               gm6 = 0.

            if reg-gm-plan > 0 then
               reg-gm-plan = round( (reg-gm-plan / reg-cnt) ,2).
            else 
               reg-gm-plan = 0.

            if regtot-8-sls > 0 then
               gm7 = round( (((regtot-8-sls - regtot-8-cst) / regtot-8-sls) 
                               * 100) ,0).
            else
               gm7 = 0.
               
          
            run fix-gm.

            if line-counter + 2 > page-size then do:
               page.
               run write-det-hdgs.
            end. /* if line-counter + 1 > page-size */

            if x-exportfl then
              export stream x-out delimiter "~011"
               "Region"
                tt-sls-det.region
                " "
                " "
                " "
                " "     
                " "     
                " "
               regtot-1-sls
               (regtot-1-sls - regtot-1-cst)
               gm1
               regtot-2-sls
               (regtot-2-sls - regtot-2-cst)
               gm2
               regtot-3-sls
               (regtot-3-sls - regtot-3-cst)
               gm3
               regtot-4-sls
               (regtot-4-sls - regtot-4-cst)
               gm4
               regtot-5-sls
               (regtot-5-sls - regtot-5-cst)
               gm5
               regtot-6-sls
               (regtot-6-sls - regtot-6-cst)
               gm6
               regtot-8-sls
               (regtot-8-sls - regtot-8-cst)
               gm7.



            disp
               regtot-1-sls
               gm1
               regtot-2-sls
               gm2
               regtot-3-sls
               gm3
               regtot-4-sls
               gm4
               regtot-5-sls
               gm5
               regtot-6-sls
               gm6
             /*  regtot-7-sls
                 reg-gm-plan   */
               regtot-8-sls
               gm7
            with frame f-dtlregtot down.
            down with frame f-dtlregtot.
      
            assign tot-1-sls    = tot-1-sls + regtot-1-sls
                   tot-2-sls    = tot-2-sls + regtot-2-sls
                   tot-3-sls    = tot-3-sls + regtot-3-sls
                   tot-4-sls    = tot-4-sls + regtot-4-sls
                   tot-5-sls    = tot-5-sls + regtot-5-sls
                   tot-6-sls    = tot-6-sls + regtot-6-sls
                   tot-7-sls    = tot-7-sls + regtot-7-sls
                   tot-8-sls    = tot-8-sls + regtot-8-sls
                   tot-1-cst    = tot-1-cst + regtot-1-cst
                   tot-2-cst    = tot-2-cst + regtot-2-cst
                   tot-3-cst    = tot-3-cst + regtot-3-cst
                   tot-4-cst    = tot-4-cst + regtot-4-cst
                   tot-5-cst    = tot-5-cst + regtot-5-cst
                   tot-6-cst    = tot-6-cst + regtot-6-cst
                   tot-gm-plan  = tot-gm-plan + reg-gm-plan
                   tot-8-cst    = tot-8-cst + regtot-8-cst
                   regtot-1-sls = 0
                   regtot-2-sls = 0
                   regtot-3-sls = 0
                   regtot-4-sls = 0
                   regtot-5-sls = 0
                   regtot-6-sls = 0
                   regtot-7-sls = 0
                   regtot-8-sls = 0
                   regtot-1-cst = 0
                   regtot-2-cst = 0
                   regtot-3-cst = 0
                   regtot-4-cst = 0
                   regtot-5-cst = 0
                   regtot-6-cst = 0
                   reg-gm-plan  = 0
                   regtot-8-cst = 0
                   reg-cnt      = 0.
                
         end. /* if last-of(tt-sls-det.region) */
      end.    /* for each tt-sls-det */
   
      if tot-1-sls > 0 then
         gm1 = round( (((tot-1-sls - tot-1-cst) / tot-1-sls) * 100) ,2).
      else
         gm1 = 0.
      
      if tot-2-sls > 0 then
         gm2 = round( (((tot-2-sls - tot-2-cst) / tot-2-sls) * 100) ,2).
      else 
         gm2 = 0.
      
      if tot-3-sls > 0 then
         gm3 = round( (((tot-3-sls - tot-3-cst) / tot-3-sls) * 100) ,2).
     else
         gm3 = 0.
      
      if tot-4-sls > 0 then
         gm4 = round( (((tot-4-sls - tot-4-cst) / tot-4-sls) * 100) ,2).
      else
         gm4 = 0.
      
      if tot-5-sls > 0 then
         gm5 = round( (((tot-5-sls - tot-5-cst) / tot-5-sls) * 100) ,2).
      else
         gm5 = 0.

      if tot-6-sls > 0 then
         gm6 = round( (((tot-6-sls - tot-6-cst) / tot-6-sls) * 100) ,2).
      else
         gm6 = 0.

      if tot-gm-plan > 0 then
         tot-gm-plan = round( (tot-gm-plan / tot-cnt) ,2).
      else
         tot-gm-plan = 0.

      if tot-8-sls > 0 then
         gm7 = round( (((tot-8-sls - tot-8-cst) / tot-8-sls) * 100) ,2).
      else
         gm7 = 0.

   
      run fix-gm.

      if line-counter + 2 > page-size then do:
         page.
         run write-det-hdgs.
      end.


      if x-exportfl then
         export stream x-out delimiter "~011"
          "Total"
           " "
           " "
           " "
           " "
           " "     
           " "     
           " "
           tot-1-sls
          (tot-1-sls - tot-1-cst)
          gm1
          tot-2-sls
          (tot-2-sls - tot-2-cst)
          gm2
          tot-3-sls
          (tot-3-sls - tot-3-cst)
          gm3
          tot-4-sls
          (tot-4-sls - tot-4-cst)
          gm4
          tot-5-sls
          (tot-5-sls - tot-5-cst)
          gm5
          tot-6-sls
          (tot-6-sls - tot-6-cst)
          gm6
          tot-8-sls
          (tot-8-sls - tot-8-cst)
          gm7.


      
      disp
         tot-1-sls
         gm1
         tot-2-sls
         gm2
         tot-3-sls
         gm3
         tot-4-sls
         gm4
         tot-5-sls
         gm5
         tot-6-sls
         gm6
       /*  tot-7-sls
           tot-gm-plan  */
         tot-8-sls
         gm7
      with frame f-dtltot down.
      down with frame f-dtltot.

   end.       /* if p-rptfl = "d" */
   else do:
      
      cntr = 0.
      run write-sum-hdgs.
      
      for each tt-sls-sum use-index k-sdet1
      no-lock break by tt-sls-sum.sls-key descending
                    by tt-sls-sum.custnm:
                    
         if cntr = 25 then do:
            
            if top25-1-sls > 0 then
               gm1 = round( (((top25-1-sls - top25-1-cst) / top25-1-sls) 
                               * 100) ,2).
            else
               gm1 = 0.
      
            if top25-2-sls > 0 then
               gm2 = round( (((top25-2-sls - top25-2-cst) / top25-2-sls) 
                               * 100) ,2).
            else 
               gm2 = 0.
      
            if top25-3-sls > 0 then
               gm3 = round( (((top25-3-sls - top25-3-cst) / top25-3-sls) 
                               * 100) ,2).
            else
               gm3 = 0.
      
            if top25-4-sls > 0 then
               gm4 = round( (((top25-4-sls - top25-4-cst) / top25-4-sls) 
                               * 100) ,2).
            else
               gm4 = 0.
    
            if top25-5-sls > 0 then
               gm5 = round( (((top25-5-sls - top25-5-cst) / top25-5-sls) 
                               * 100) ,2).
            else
               gm5 = 0.
            
            if top25-6-sls > 0 then
               gm6 = round( (((top25-6-sls - top25-6-cst) / top25-6-sls) 
                               * 100) ,2).
            else
               gm6 = 0.

            if top25-8-sls > 0 then
               gm7 = round( (((top25-8-sls - top25-8-cst) / top25-8-sls) 
                               * 100) ,2).
            else
               gm7 = 0.
             
            if top25-gm-plan > 0 then
               top25-gm-plan = round( (top25-gm-plan / cntr) ,2).
            else
               top25-gm-plan = 0.
            
            run fix-gm.

            if line-counter + 2 > page-size then do:
                page.
                run write-sum-hdgs.
            end. /* if line-counter + 1 > page-size */

         if x-exportfl then
            export stream x-out delimiter "~011"
               "Top25"
               " "
               " "
               " "
               " "     
               " "     
               " "
               " "
               top25-1-sls
               (top25-1-sls - top25-1-cst)
               gm1
               top25-2-sls
               (top25-2-sls - top25-2-cst)
               gm2
               top25-3-sls
               (top25-3-sls - top25-3-cst)
               gm3
               top25-4-sls
               (top25-4-sls - top25-4-cst)
               gm4
               top25-5-sls
               (top25-5-sls - top25-5-cst)
               gm5
               top25-6-sls
               (top25-6-sls - top25-6-cst)
               gm6
               top25-8-sls
               (top25-8-sls - top25-8-cst)
               gm7.               
  
            disp
               top25-1-sls
               gm1
               top25-2-sls
               gm2
               top25-3-sls
               gm3
               top25-4-sls
               gm4
               top25-5-sls
               gm5
               top25-6-sls
               gm6
              /*  top25-7-sls
                  top25-gm-plan     */
            with frame f-sum25 down.
            down with frame f-sum25.
     /*       
            assign top25-1-sls    = 0
                   top25-2-sls    = 0
                   top25-3-sls    = 0
                   top25-4-sls    = 0
                   top25-5-sls    = 0
                   top25-6-sls    = 0
                   top25-7-sls    = 0
                   top25-1-cst    = 0
                   top25-2-cst    = 0
                   top25-3-cst    = 0
                   top25-4-cst    = 0
                   top25-5-cst    = 0
                   top25-6-cst    = 0
                   top25-gm-plan  = 0
                   top25-cnt      = 0.
      */
            page.
            run write-sum-hdgs.

         end. /* if cntr = 25 */
         
         if cntr >= 100 then do:
         
            assign alloth-1-sls   = alloth-1-sls + tt-sls-sum.mo-1-sls
                   alloth-2-sls   = alloth-2-sls + tt-sls-sum.mo-2-sls
                   alloth-3-sls   = alloth-3-sls + tt-sls-sum.mo-3-sls
                   alloth-4-sls   = alloth-4-sls + tt-sls-sum.sls-ytd
                   alloth-5-sls   = alloth-5-sls + tt-sls-sum.sls-lst-yr
                   alloth-6-sls   = alloth-6-sls + tt-sls-sum.sls-annual
                   alloth-7-sls   = alloth-7-sls + tt-sls-sum.sls-plan
                   alloth-8-sls   = alloth-8-sls + tt-sls-sum.sls-lst-tyr
                   alloth-1-cst   = alloth-1-cst + tt-sls-sum.mo-1-cst
                   alloth-2-cst   = alloth-2-cst + tt-sls-sum.mo-2-cst
                   alloth-3-cst   = alloth-3-cst + tt-sls-sum.mo-3-cst
                   alloth-4-cst   = alloth-4-cst + tt-sls-sum.cst-ytd
                   alloth-5-cst   = alloth-5-cst + tt-sls-sum.cst-lst-yr
                   alloth-6-cst   = alloth-6-cst + tt-sls-sum.cst-annual
                   alloth-gm-plan = alloth-gm-plan + tt-sls-sum.gm-plan
                   alloth-8-cst   = alloth-8-cst + tt-sls-sum.cst-lst-tyr
                   alloth-cnt     = alloth-cnt + 1
                   tot-1-sls = tot-1-sls + tt-sls-sum.mo-1-sls
                   tot-2-sls = tot-2-sls + tt-sls-sum.mo-2-sls
                   tot-3-sls = tot-3-sls + tt-sls-sum.mo-3-sls
                   tot-4-sls = tot-4-sls + tt-sls-sum.sls-ytd
                   tot-5-sls = tot-5-sls + tt-sls-sum.sls-lst-yr
                   tot-6-sls = tot-6-sls + tt-sls-sum.sls-annual
                   tot-7-sls = tot-7-sls + tt-sls-sum.sls-plan
                   tot-8-sls = tot-8-sls + tt-sls-sum.sls-lst-tyr
                   tot-1-cst = tot-1-cst + tt-sls-sum.mo-1-cst
                   tot-2-cst = tot-2-cst + tt-sls-sum.mo-2-cst
                   tot-3-cst = tot-3-cst + tt-sls-sum.mo-3-cst
                   tot-4-cst = tot-4-cst + tt-sls-sum.cst-ytd
                   tot-5-cst = tot-5-cst + tt-sls-sum.cst-lst-yr
                   tot-6-cst = tot-6-cst + tt-sls-sum.cst-annual
                   tot-8-cst = tot-8-cst + tt-sls-sum.cst-lst-tyr
                   tot-gm-plan  = tot-gm-plan + tt-sls-sum.gm-plan
                   tot-cnt      = tot-cnt + 1.
         
         end.    /* if cntr >= 100 */
         else do:
            
            if tt-sls-sum.mo-1-sls > 0 then
               gm1 = round( (((tt-sls-sum.mo-1-sls - tt-sls-sum.mo-1-cst) 
                             / tt-sls-sum.mo-1-sls) * 100) ,2).
            else
               gm1 = 0.
      
            if tt-sls-sum.mo-2-sls > 0 then
               gm2 = round( (((tt-sls-sum.mo-2-sls - tt-sls-sum.mo-2-cst) 
                             / tt-sls-sum.mo-2-sls) * 100) ,2).
            else 
               gm2 = 0.
      
            if tt-sls-sum.mo-3-sls > 0 then
               gm3 = round( (((tt-sls-sum.mo-3-sls - tt-sls-sum.mo-3-cst) 
                             / tt-sls-sum.mo-3-sls) * 100) ,2).
            else
               gm3 = 0.
      
            if tt-sls-sum.sls-ytd > 0 then
               gm4 = round( (((tt-sls-sum.sls-ytd - tt-sls-sum.cst-ytd) 
                             / tt-sls-sum.sls-ytd) * 100) ,2).
            else
               gm4 = 0.
        
            if tt-sls-sum.sls-lst-yr > 0 then
               gm5 = round( (((tt-sls-sum.sls-lst-yr - tt-sls-sum.cst-lst-yr) 
                             / tt-sls-sum.sls-lst-yr) * 100) ,2).
            else
               gm5 = 0.
         
            if tt-sls-sum.sls-annual > 0 then
               gm6 = round( (((tt-sls-sum.sls-annual - tt-sls-sum.cst-annual) 
                             / tt-sls-sum.sls-annual) * 100) ,2).
            else
               gm6 = 0.
 
            if tt-sls-sum.sls-lst-tyr > 0 then
               gm7 = round( (((tt-sls-sum.sls-lst-tyr - tt-sls-sum.cst-lst-tyr) 
                             / tt-sls-sum.sls-lst-tyr) * 100) ,2).
            else
               gm7 = 0.
 
            run fix-gm.                           
           
            if line-counter + 2 > page-size then do:
               page.
               run write-sum-hdgs.
            end. /* if line-counter + 1 > page-size */
           if x-exportfl then
           export stream x-out delimiter "~011"
            " "
            tt-sls-sum.region
            tt-sls-sum.dist
            tt-sls-sum.slsrep
            tt-sls-sum.repnm
            tt-sls-sum.custno     
            tt-sls-sum.custnm     
            tt-sls-sum.tech
            tt-sls-sum.mo-1-sls   
            (tt-sls-sum.mo-1-sls - tt-sls-sum.mo-1-cst)   
            gm1               
            tt-sls-sum.mo-2-sls   
            (tt-sls-sum.mo-2-sls - tt-sls-sum.mo-2-cst)   
            gm2               
            tt-sls-sum.mo-3-sls   
            (tt-sls-sum.mo-3-sls - tt-sls-sum.mo-3-cst)   
            gm3               
            tt-sls-sum.sls-ytd    
            (tt-sls-sum.sls-ytd - tt-sls-sum.cst-ytd)    
            gm4               
            tt-sls-sum.sls-lst-yr
            (tt-sls-sum.sls-lst-yr - tt-sls-sum.cst-lst-yr)   
            gm5               
            tt-sls-sum.sls-annual
            (tt-sls-sum.sls-annual - tt-sls-sum.cst-annual)
            gm6
            tt-sls-sum.sls-lst-tyr
            (tt-sls-sum.sls-lst-tyr - tt-sls-sum.cst-lst-tyr)   
            gm7.         
            
            disp
               tt-sls-sum.custno     
               tt-sls-sum.custnm     
               tt-sls-sum.repnm
               tt-sls-sum.mo-1-sls   
               gm1               
               tt-sls-sum.mo-2-sls   
               gm2               
               tt-sls-sum.mo-3-sls   
               gm3               
               tt-sls-sum.sls-ytd    
               gm4               
               tt-sls-sum.sls-lst-yr 
               gm5               
               tt-sls-sum.sls-annual
               gm6
            /*   tt-sls-sum.sls-plan
                 tt-sls-sum.gm-plan  */
            with frame f-sumdtl down.
            down with frame f-sumdtl.

            cntr = cntr + 1.
            if cntr < 101 then
              assign top25-1-sls   = top25-1-sls + tt-sls-sum.mo-1-sls
                   top25-2-sls   = top25-2-sls + tt-sls-sum.mo-2-sls
                   top25-3-sls   = top25-3-sls + tt-sls-sum.mo-3-sls
                   top25-4-sls   = top25-4-sls + tt-sls-sum.sls-ytd
                   top25-5-sls   = top25-5-sls + tt-sls-sum.sls-lst-yr
                   top25-6-sls   = top25-6-sls + tt-sls-sum.sls-annual
                   top25-7-sls   = top25-7-sls + tt-sls-sum.sls-plan
                   top25-8-sls   = top25-8-sls + tt-sls-sum.sls-lst-tyr
                   top25-1-cst   = top25-1-cst + tt-sls-sum.mo-1-cst
                   top25-2-cst   = top25-2-cst + tt-sls-sum.mo-2-cst
                   top25-3-cst   = top25-3-cst + tt-sls-sum.mo-3-cst
                   top25-4-cst   = top25-4-cst + tt-sls-sum.cst-ytd
                   top25-5-cst   = top25-5-cst + tt-sls-sum.cst-lst-yr
                   top25-6-cst   = top25-6-cst + tt-sls-sum.cst-annual
                   top25-8-cst   = top25-8-cst + tt-sls-sum.cst-lst-tyr
                   top25-gm-plan = top25-gm-plan + tt-sls-sum.gm-plan
                   top25-cnt     = top25-cnt + 1.
           assign  disttot-1-sls = disttot-1-sls + tt-sls-sum.mo-1-sls
                   disttot-2-sls = disttot-2-sls + tt-sls-sum.mo-2-sls
                   disttot-3-sls = disttot-3-sls + tt-sls-sum.mo-3-sls
                   disttot-4-sls = disttot-4-sls + tt-sls-sum.sls-ytd
                   disttot-5-sls = disttot-5-sls + tt-sls-sum.sls-lst-yr
                   disttot-6-sls = disttot-6-sls + tt-sls-sum.sls-annual
                   disttot-7-sls = disttot-7-sls + tt-sls-sum.sls-plan
                   disttot-8-sls = disttot-8-sls + tt-sls-sum.sls-lst-tyr

                   disttot-1-cst = disttot-1-cst + tt-sls-sum.mo-1-cst
                   disttot-2-cst = disttot-2-cst + tt-sls-sum.mo-2-cst
                   disttot-3-cst = disttot-3-cst + tt-sls-sum.mo-3-cst
                   disttot-4-cst = disttot-4-cst + tt-sls-sum.cst-ytd
                   disttot-5-cst = disttot-5-cst + tt-sls-sum.cst-lst-yr
                   disttot-6-cst = disttot-6-cst + tt-sls-sum.cst-annual
                   dist-gm-plan  = dist-gm-plan + tt-sls-sum.gm-plan
                   disttot-8-cst = disttot-8-cst + tt-sls-sum.cst-lst-tyr
                   dist-cnt      = dist-cnt + 1
                   regtot-1-sls = regtot-1-sls + tt-sls-sum.mo-1-sls
                   regtot-2-sls = regtot-2-sls + tt-sls-sum.mo-2-sls
                   regtot-3-sls = regtot-3-sls + tt-sls-sum.mo-3-sls
                   regtot-4-sls = regtot-4-sls + tt-sls-sum.sls-ytd
                   regtot-5-sls = regtot-5-sls + tt-sls-sum.sls-lst-yr
                   regtot-8-sls = regtot-8-sls + tt-sls-sum.sls-lst-tyr
                   
                   regtot-6-sls = regtot-6-sls + tt-sls-sum.sls-annual
                   regtot-7-sls = regtot-7-sls + tt-sls-sum.sls-plan
                   regtot-1-cst = regtot-1-cst + tt-sls-sum.mo-1-cst
                   regtot-2-cst = regtot-2-cst + tt-sls-sum.mo-2-cst
                   regtot-3-cst = regtot-3-cst + tt-sls-sum.mo-3-cst
                   regtot-4-cst = regtot-4-cst + tt-sls-sum.cst-ytd
                   regtot-5-cst = regtot-5-cst + tt-sls-sum.cst-lst-yr
                   regtot-6-cst = regtot-6-cst + tt-sls-sum.cst-annual
                   reg-gm-plan  = reg-gm-plan + tt-sls-sum.gm-plan
                   regtot-8-cst = regtot-8-cst + tt-sls-sum.cst-lst-tyr

                   reg-cnt      = reg-cnt + 1
                   tot-1-sls = tot-1-sls + tt-sls-sum.mo-1-sls
                   tot-2-sls = tot-2-sls + tt-sls-sum.mo-2-sls
                   tot-3-sls = tot-3-sls + tt-sls-sum.mo-3-sls
                   tot-4-sls = tot-4-sls + tt-sls-sum.sls-ytd
                   tot-5-sls = tot-5-sls + tt-sls-sum.sls-lst-yr
                   tot-6-sls = tot-6-sls + tt-sls-sum.sls-annual
                   tot-7-sls = tot-7-sls + tt-sls-sum.sls-plan
                   tot-8-sls = tot-8-sls + tt-sls-sum.sls-lst-tyr

                   tot-1-cst = tot-1-cst + tt-sls-sum.mo-1-cst
                   tot-2-cst = tot-2-cst + tt-sls-sum.mo-2-cst
                   tot-3-cst = tot-3-cst + tt-sls-sum.mo-3-cst
                   tot-4-cst = tot-4-cst + tt-sls-sum.cst-ytd
                   tot-5-cst = tot-5-cst + tt-sls-sum.cst-lst-yr
                   tot-6-cst = tot-6-cst + tt-sls-sum.cst-annual
                   tot-gm-plan  = tot-gm-plan + tt-sls-sum.gm-plan
                   tot-8-cst = tot-8-cst + tt-sls-sum.cst-lst-tyr

                   tot-cnt      = tot-cnt + 1.

         end.    /* else do */
      end.       /* for each tt-sls-sum */
            
      if top25-1-sls > 0 then
         gm1 = round( (((top25-1-sls - top25-1-cst) / top25-1-sls) 
                         * 100) ,2).
      else
         gm1 = 0.

      if top25-2-sls > 0 then
         gm2 = round( (((top25-2-sls - top25-2-cst) / top25-2-sls) 
                               * 100) ,2).
      else 
         gm2 = 0.

      if top25-3-sls > 0 then
         gm3 = round( (((top25-3-sls - top25-3-cst) / top25-3-sls) 
                               * 100) ,2).
      else
         gm3 = 0.
      
      if top25-4-sls > 0 then
          gm4 = round( (((top25-4-sls - top25-4-cst) / top25-4-sls) 
                               * 100) ,2).
      else
          gm4 = 0.
    
      if top25-5-sls > 0 then
         gm5 = round( (((top25-5-sls - top25-5-cst) / top25-5-sls) 
                               * 100) ,2).
      else
         gm5 = 0.
            
      if top25-6-sls > 0 then
         gm6 = round( (((top25-6-sls - top25-6-cst) / top25-6-sls) 
                               * 100) ,2).
      else
         gm6 = 0.

      if top25-8-sls > 0 then
         gm7 = round( (((top25-8-sls - top25-8-cst) / top25-8-sls) 
                               * 100) ,2).
      else
         gm7 = 0.


      if top25-gm-plan > 0 then
        top25-gm-plan = round( (top25-gm-plan / cntr) ,2).
      else
        top25-gm-plan = 0.
            
      run fix-gm.

      if line-counter + 2 > page-size then do:
        page.
        run write-sum-hdgs.
        end. /* if line-counter + 1 > page-size */


         if x-exportfl then
            export stream x-out delimiter "~011"
               "Top100"
               " "
               " "
               " "
               " "     
               " "     
               " "
               " "
               top25-1-sls
               (top25-1-sls - top25-1-cst)
               gm1
               top25-2-sls
               (top25-2-sls - top25-2-cst)
               gm2
               top25-3-sls
               (top25-3-sls - top25-3-cst)
               gm3
               top25-4-sls
               (top25-4-sls - top25-4-cst)
               gm4
               top25-5-sls
               (top25-5-sls - top25-5-cst)
               gm5
               top25-6-sls
               (top25-6-sls - top25-6-cst)
               gm6
               top25-8-sls
               (top25-8-sls - top25-8-cst)
               gm7.
 

       disp
               top25-1-sls
               gm1
               top25-2-sls
               gm2
               top25-3-sls
               gm3
               top25-4-sls
               gm4
               top25-5-sls
               gm5
               top25-6-sls
               gm6
           /*    top25-7-sls
                 top25-gm-plan  */
            with frame f-sum100 down.
            down with frame f-sum100.
      
      if alloth-1-sls > 0 then
         gm1 = round( (((alloth-1-sls - alloth-1-cst) / alloth-1-sls) 
                         * 100) ,2).
      else
         gm1 = 0.
      
      if alloth-2-sls > 0 then
         gm2 = round( (((alloth-2-sls - alloth-2-cst) / alloth-2-sls) 
                         * 100) ,2).
      else 
         gm2 = 0.
        
      if alloth-3-sls > 0 then
         gm3 = round( (((alloth-3-sls - alloth-3-cst) / alloth-3-sls) 
                         * 100) ,2).
      else
         gm3 = 0.
     
      if alloth-4-sls > 0 then
         gm4 = round( (((alloth-4-sls - alloth-4-cst) / alloth-4-sls) 
                         * 100) ,2).
      else
         gm4 = 0.
   
      if alloth-5-sls > 0 then
         gm5 = round( (((alloth-5-sls - alloth-5-cst) / alloth-5-sls) 
                         * 100) ,2).
      else
         gm5 = 0.
           
      if alloth-6-sls > 0 then
         gm6 = round( (((alloth-6-sls - alloth-6-cst) / alloth-6-sls) 
                         * 100) ,2).
      else
         gm6 = 0.
           
      if alloth-8-sls > 0 then
         gm7 = round( (((alloth-8-sls - alloth-8-cst) / alloth-8-sls) 
                         * 100) ,2).
      else
         gm7 = 0.


      if alloth-gm-plan > 0 then
         alloth-gm-plan = round( (alloth-gm-plan / alloth-cnt) ,2).
      else
         alloth-gm-plan = 0.
             
      run fix-gm.
                 
      if line-counter + 2 > page-size then do:
         page.
         run write-sum-hdgs.
      end. /* if line-counter + 1 > page-size */
 
      if x-exportfl then
            export stream x-out delimiter "~011"
               "AllOthers"
               " "
               " "
               " "
               " "     
               " "     
               " "
               " "
               alloth-1-sls
               (alloth-1-sls - alloth-1-cst)
               gm1
               alloth-2-sls
               (alloth-2-sls - alloth-2-cst)
               gm2
               alloth-3-sls
               (alloth-3-sls - alloth-3-cst)
               gm3
               alloth-4-sls
               (alloth-4-sls - alloth-4-cst)
               gm4
               alloth-5-sls
               (alloth-5-sls - alloth-5-cst)
               gm5
               alloth-6-sls
               (alloth-6-sls - alloth-6-cst)
               gm6
               alloth-8-sls
               (alloth-8-sls - alloth-8-cst)
               gm7.               
     
      disp
         alloth-1-sls
         gm1
         alloth-2-sls
         gm2
         alloth-3-sls
         gm3
         alloth-4-sls
         gm4
         alloth-5-sls
         gm5
         alloth-6-sls
         gm6
       /*  alloth-7-sls
           alloth-gm-plan   */
      with frame f-alloth down.
      down with frame f-alloth.
           
      assign alloth-1-sls   = 0
             alloth-2-sls   = 0
             alloth-3-sls   = 0
             alloth-4-sls   = 0
             alloth-5-sls   = 0
             alloth-6-sls   = 0
             alloth-7-sls   = 0
             alloth-8-sls   = 0

             alloth-1-cst   = 0
             alloth-2-cst   = 0
             alloth-3-cst   = 0
             alloth-4-cst   = 0
             alloth-5-cst   = 0
             alloth-6-cst   = 0
             alloth-8-cst   = 0

             alloth-gm-plan = 0
             alloth-cnt     = 0.
   
      if tot-1-sls > 0 then
         gm1 = round( (((tot-1-sls - tot-1-cst) / tot-1-sls) * 100) ,2).
      else
         gm1 = 0.
      
      if tot-2-sls > 0 then
         gm2 = round( (((tot-2-sls - tot-2-cst) / tot-2-sls) * 100) ,2).
      else 
         gm2 = 0.
      
      if tot-3-sls > 0 then
         gm3 = round( (((tot-3-sls - tot-3-cst) / tot-3-sls) * 100) ,2).
      else
         gm3 = 0.
      
      if tot-4-sls > 0 then
         gm4 = round( (((tot-4-sls - tot-4-cst) / tot-4-sls) * 100) ,2).
      else
         gm4 = 0.
      
      if tot-5-sls > 0 then
         gm5 = round( (((tot-5-sls - tot-5-cst) / tot-5-sls) * 100) ,2).
      else
         gm5 = 0.

      if tot-6-sls > 0 then
         gm6 = round( (((tot-6-sls - tot-6-cst) / tot-6-sls) * 100) ,2).
      else
         gm6 = 0.

      if tot-8-sls > 0 then
         gm7 = round( (((tot-8-sls - tot-8-cst) / tot-8-sls) * 100) ,2).
      else
         gm7 = 0.


      if tot-gm-plan > 0 then
         tot-gm-plan = round( (tot-gm-plan / tot-cnt) ,2).
      else
         tot-gm-plan = 0.
   
      run fix-gm.

      if line-counter + 2 > page-size then do:
         page.
         run write-det-hdgs.
      end.
      if x-exportfl then
            export stream x-out delimiter "~011"
               "Total"
               " "
               " " 
               " "
               " "     
               " "     
               " "
               " "
               tot-1-sls
               (tot-1-sls - tot-1-cst)
               gm1
               tot-2-sls
               (tot-2-sls - tot-2-cst)
               gm2
               tot-3-sls
               (tot-3-sls - tot-3-cst)
               gm3
               tot-4-sls
               (tot-4-sls - tot-4-cst)
               gm4
               tot-5-sls
               (tot-5-sls - tot-5-cst)
               gm5
               tot-6-sls
               (tot-6-sls - tot-6-cst)
               gm6
               tot-8-sls
               (tot-8-sls - tot-8-cst)
               gm7.               

      disp
         tot-1-sls
         gm1
         tot-2-sls
         gm2
         tot-3-sls
         gm3
         tot-4-sls
         gm4
         tot-5-sls
         gm5
         tot-6-sls
         gm6
       /*  tot-7-sls
           tot-gm-plan  */
      with frame f-sumtot down.
      down with frame f-sumtot.

   end.          /* else do */
end.  
else do:
   
   if p-rptfl = "d" then do:

      for each tt-sls-det use-index k-det1
      no-lock break by tt-sls-det.region
                    by tt-sls-det.dist
                    by tt-sls-det.slsrep
                    by tt-sls-det.sls-annual descending
                    by tt-sls-det.tech: 
         
      assign disttot-1-sls = disttot-1-sls + tt-sls-det.mo-1-sls
                disttot-2-sls = disttot-2-sls + tt-sls-det.mo-2-sls
                disttot-3-sls = disttot-3-sls + tt-sls-det.mo-3-sls
                disttot-4-sls = disttot-4-sls + tt-sls-det.sls-ytd
                disttot-5-sls = disttot-5-sls + tt-sls-det.sls-lst-yr
                disttot-6-sls = disttot-6-sls + tt-sls-det.sls-annual
                disttot-7-sls = disttot-7-sls + tt-sls-det.sls-plan
                disttot-8-sls = disttot-8-sls + tt-sls-det.sls-lst-tyr
                disttot-1-cst = disttot-1-cst + tt-sls-det.mo-1-cst
                disttot-2-cst = disttot-2-cst + tt-sls-det.mo-2-cst
                disttot-3-cst = disttot-3-cst + tt-sls-det.mo-3-cst
                disttot-4-cst = disttot-4-cst + tt-sls-det.cst-ytd
                disttot-5-cst = disttot-5-cst + tt-sls-det.cst-lst-yr
                disttot-6-cst = disttot-6-cst + tt-sls-det.cst-annual
                dist-gm-plan  = dist-gm-plan + tt-sls-det.gm-plan
                disttot-8-cst = disttot-8-cst + tt-sls-det.sls-lst-tyr
                dist-cnt      = dist-cnt + 1
                reptot-1-sls = reptot-1-sls + tt-sls-det.mo-1-sls
                reptot-2-sls = reptot-2-sls + tt-sls-det.mo-2-sls
                reptot-3-sls = reptot-3-sls + tt-sls-det.mo-3-sls
                reptot-4-sls = reptot-4-sls + tt-sls-det.sls-ytd
                reptot-5-sls = reptot-5-sls + tt-sls-det.sls-lst-yr
                reptot-6-sls = reptot-6-sls + tt-sls-det.sls-annual
                reptot-7-sls = reptot-7-sls + tt-sls-det.sls-plan
                reptot-8-sls = reptot-8-sls + tt-sls-det.sls-lst-tyr
                reptot-1-cst = reptot-1-cst + tt-sls-det.mo-1-cst
                reptot-2-cst = reptot-2-cst + tt-sls-det.mo-2-cst
                reptot-3-cst = reptot-3-cst + tt-sls-det.mo-3-cst
                reptot-4-cst = reptot-4-cst + tt-sls-det.cst-ytd
                reptot-5-cst = reptot-5-cst + tt-sls-det.cst-lst-yr
                reptot-6-cst = reptot-6-cst + tt-sls-det.cst-annual
                rep-gm-plan  = rep-gm-plan + tt-sls-det.gm-plan
                reptot-8-cst = reptot-8-cst + tt-sls-det.cst-lst-tyr
                rep-cnt      = rep-cnt + 1.

         if first-of (tt-sls-det.region)  or
            first-of (tt-sls-det.dist)    or
            first-of (tt-sls-det.slsrep)  then do:
            if dhis = true and 
               (first-of(tt-sls-det.slsrep) and
                not
                first-of(tt-sls-det.dist)) then
              dhis = true.
            else  
              page.
            assign 
               x-slsrep = string(tt-sls-det.slsrep) + " " + tt-sls-det.repnm
               x-region = tt-sls-det.region
               x-dist   = tt-sls-det.dist.
            if dhis = true and 
                (first-of(tt-sls-det.slsrep) and
                 not
                 first-of(tt-sls-det.dist)) then
              dhis = true.
            else  
              run write-det-hdgs.

         end. /*if first-of (tt-sls-det.divno) or first-of (tt-sls-det.slsrep)*/
             
         if tt-sls-det.mo-1-sls > 0 then
            gm1 = round( (((tt-sls-det.mo-1-sls - tt-sls-det.mo-1-cst) 
                  / tt-sls-det.mo-1-sls) * 100) ,2).
         else
            gm1 = 0.
      
         if tt-sls-det.mo-2-sls > 0 then
            gm2 = round( (((tt-sls-det.mo-2-sls - tt-sls-det.mo-2-cst) 
                  / tt-sls-det.mo-2-sls) * 100) ,2).
         else 
            gm2 = 0.
      
         if tt-sls-det.mo-3-sls > 0 then
            gm3 = round( (((tt-sls-det.mo-3-sls - tt-sls-det.mo-3-cst) 
                  / tt-sls-det.mo-3-sls) * 100) ,2).
         else
            gm3 = 0.
      
         if tt-sls-det.sls-ytd > 0 then
            gm4 = round( (((tt-sls-det.sls-ytd - tt-sls-det.cst-ytd) 
                  / tt-sls-det.sls-ytd) * 100) ,2).
         else
            gm4 = 0.
       
         if tt-sls-det.sls-lst-yr > 0 then
            gm5 = round( (((tt-sls-det.sls-lst-yr - tt-sls-det.cst-lst-yr) 
                  / tt-sls-det.sls-lst-yr) * 100) ,2).
         else
            gm5 = 0.
         
         if tt-sls-det.sls-annual > 0 then
            gm6 = round( (((tt-sls-det.sls-annual - tt-sls-det.cst-annual) 
                  / tt-sls-det.sls-annual) * 100) ,2).
         else
            gm6 = 0.
         
         if tt-sls-det.sls-lst-tyr > 0 then
            gm7 = round( (((tt-sls-det.sls-lst-tyr - tt-sls-det.cst-lst-tyr) 
                  / tt-sls-det.sls-lst-tyr) * 100) ,2).
         else
            gm7 = 0.


         run fix-gm.                           
       
         if line-counter + 2 > page-size then do:
            page.
            run write-det-hdgs.
         end. /* if line-counter + 1 > page-size */
         if dhis = false then
           do:
           if x-exportfl then 
           export stream x-out delimiter "~011"
            " "
            tt-sls-det.region
            tt-sls-det.dist
            tt-sls-det.slsrep
            tt-sls-det.repnm
            tt-sls-det.custno     
            tt-sls-det.custnm     
            tt-sls-det.tech
            tt-sls-det.mo-1-sls   
            (tt-sls-det.mo-1-sls - tt-sls-det.mo-1-cst)   
            gm1               
            tt-sls-det.mo-2-sls   
            (tt-sls-det.mo-2-sls - tt-sls-det.mo-2-cst)   
            gm2               
            tt-sls-det.mo-3-sls   
            (tt-sls-det.mo-3-sls - tt-sls-det.mo-3-cst)   
            gm3               
            tt-sls-det.sls-ytd    
            (tt-sls-det.sls-ytd - tt-sls-det.cst-ytd)    
            gm4               
            tt-sls-det.sls-lst-yr
            (tt-sls-det.sls-lst-yr - tt-sls-det.cst-lst-yr)   
            gm5               
            tt-sls-det.sls-annual
            (tt-sls-det.sls-annual - tt-sls-det.cst-annual)
            gm6
            tt-sls-det.sls-lst-tyr
            gm7.
           
           disp
            tt-sls-det.custno     
            tt-sls-det.custnm     
            tt-sls-det.tech
            tt-sls-det.mo-1-sls   
            gm1               
            tt-sls-det.mo-2-sls   
            gm2               
            tt-sls-det.mo-3-sls   
            gm3               
            tt-sls-det.sls-ytd    
            gm4               
            tt-sls-det.sls-lst-yr 
            gm5               
            tt-sls-det.sls-annual
            gm6
          /*  tt-sls-det.sls-plan
              tt-sls-det.gm-plan  */
            tt-sls-det.sls-lst-tyr
            gm7
           with frame f-detdtl down.
           down with frame f-detdtl.
           end.

         if last-of (tt-sls-det.slsrep) then do:
         
            if reptot-1-sls > 0 then
               gm1 = round( (((reptot-1-sls - reptot-1-cst) / reptot-1-sls) 
                               * 100) ,2).
            else
               gm1 = 0.
      
            if reptot-2-sls > 0 then
               gm2 = round( (((reptot-2-sls - reptot-2-cst) / reptot-2-sls) 
                               * 100) ,2).
            else 
               gm2 = 0.
      
            if reptot-3-sls > 0 then
               gm3 = round( (((reptot-3-sls - reptot-3-cst) / reptot-3-sls) 
                               * 100) ,2).
            else
               gm3 = 0.
      
            if reptot-4-sls > 0 then
               gm4 = round( (((reptot-4-sls - reptot-4-cst) / reptot-4-sls) 
                               * 100) ,2).
            else
               gm4 = 0.
    
            if reptot-5-sls > 0 then
               gm5 = round( (((reptot-5-sls - reptot-5-cst) / reptot-5-sls) 
                               * 100) ,2).
            else
               gm5 = 0.
            
            if reptot-6-sls > 0 then
               gm6 = round( (((reptot-6-sls - reptot-6-cst) / reptot-6-sls) 
                               * 100) ,2).
            else
               gm6 = 0.

            if rep-gm-plan > 0 then
               rep-gm-plan = round( (rep-gm-plan / rep-cnt) ,2).
            else 
               rep-gm-plan = 0.
            
            if reptot-8-sls > 0 then
               gm7 = round( (((reptot-8-sls - reptot-8-cst) / reptot-8-sls) 
                               * 100) ,2).
            else
               gm7 = 0.

            
            run fix-gm.

            if line-counter + 2 > page-size then do:
               page.
               run write-det-hdgs.
            end. /* if line-counter + 1 > page-size */

            if dhis = false then do:
              disp with frame f-dtlreptotu.
              down with frame f-dtlreptotu.
              end.
         if x-exportfl then
            export stream x-out delimiter "~011"
               "SlsRep"
               tt-sls-det.region
               tt-sls-det.dist
               tt-sls-det.slsrep
               tt-sls-det.repnm
               " "     
               " "     
               " "
               reptot-1-sls
               (reptot-1-sls - reptot-1-cst)
               gm1
               reptot-2-sls
               (reptot-2-sls - reptot-2-cst)
               gm2
               reptot-3-sls
               (reptot-3-sls - reptot-3-cst)
               gm3
               reptot-4-sls
               (reptot-4-sls - reptot-4-cst)
               gm4
               reptot-5-sls
               (reptot-5-sls - reptot-5-cst)
               gm5
               reptot-6-sls
               (reptot-6-sls - reptot-6-cst)
               gm6
               reptot-8-sls
               (reptot-8-sls - reptot-8-cst)
               gm7.
            
            
            disp
               tt-sls-det.slsrep
               tt-sls-det.repnm
               reptot-1-sls
               gm1
               reptot-2-sls
               gm2
               reptot-3-sls
               gm3
               reptot-4-sls
               gm4
               reptot-5-sls
               gm5
               reptot-6-sls
               gm6
            /*   reptot-7-sls
                 rep-gm-plan */
               reptot-8-sls
               gm7
            with frame f-dtlreptot down.
            down with frame f-dtlreptot.
      
            assign reptot-1-sls = 0
                   reptot-2-sls = 0
                   reptot-3-sls = 0
                   reptot-4-sls = 0
                   reptot-5-sls = 0
                   reptot-6-sls = 0
                   reptot-7-sls = 0
                   reptot-8-sls = 0
                   reptot-1-cst = 0
                   reptot-2-cst = 0
                   reptot-3-cst = 0
                   reptot-4-cst = 0
                   reptot-5-cst = 0
                   reptot-6-cst = 0
                   rep-gm-plan  = 0
                   reptot-8-cst = 0
                   rep-cnt      = 0.
       
         end. /* if last-of(tt-sls-det.slsrep) */

         if last-of (tt-sls-det.dist) then do:
           
            if disttot-1-sls > 0 then
               gm1 = round( (((disttot-1-sls - disttot-1-cst) / disttot-1-sls) 
                               * 100) ,2).
            else
               gm1 = 0.
      
            if disttot-2-sls > 0 then
               gm2 = round( (((disttot-2-sls - disttot-2-cst) / disttot-2-sls) 
                               * 100) ,2).
            else 
               gm2 = 0.
      
            if disttot-3-sls > 0 then
               gm3 = round( (((disttot-3-sls - disttot-3-cst) / disttot-3-sls) 
                               * 100) ,2).
            else
               gm3 = 0.
      
            if disttot-4-sls > 0 then
               gm4 = round( (((disttot-4-sls - disttot-4-cst) / disttot-4-sls) 
                               * 100) ,2).
            else
               gm4 = 0.
    
            if disttot-5-sls > 0 then
               gm5 = round( (((disttot-5-sls - disttot-5-cst) / disttot-5-sls) 
                               * 100) ,2).
            else
               gm5 = 0.
            
            if disttot-6-sls > 0 then
               gm6 = round( (((disttot-6-sls - disttot-6-cst) / disttot-6-sls) 
                               * 100) ,2).
            else
               gm6 = 0.

            if dist-gm-plan > 0 then
               dist-gm-plan = round( (dist-gm-plan / dist-cnt) ,2).
            else 
               dist-gm-plan = 0.

            if disttot-8-sls > 0 then
               gm7 = round( (((disttot-8-sls - disttot-8-cst) / disttot-8-sls) 
                               * 100) ,2).
            else
               gm7 = 0.

            
            run fix-gm.

            if line-counter + 2 > page-size then do:
               page.
               run write-det-hdgs.
            end. /* if line-counter + 1 > page-size */
         if x-exportfl then
            export stream x-out delimiter "~011"
               "District"
               tt-sls-sum.region
               tt-sls-sum.dist
               " "
               " "
               " "     
               " "     
               " "
               disttot-1-sls
               (disttot-1-sls - disttot-1-cst)
               gm1
               disttot-2-sls
               (disttot-2-sls - disttot-2-cst)
               gm2
               disttot-3-sls
               (disttot-3-sls - disttot-3-cst)
               gm3
               disttot-4-sls
               (disttot-4-sls - disttot-4-cst)
               gm4
               disttot-5-sls
               (disttot-5-sls - disttot-5-cst)
               gm5
               disttot-6-sls
               (disttot-6-sls - disttot-6-cst)
               gm6
               disttot-8-sls
               (disttot-8-sls - disttot-8-cst)
               gm7.

            disp
               disttot-1-sls
               gm1
               disttot-2-sls
               gm2
               disttot-3-sls
               gm3
               disttot-4-sls
               gm4
               disttot-5-sls
               gm5
               disttot-6-sls
               gm6
              /* disttot-7-sls
                 dist-gm-plan */
               disttot-8-sls
               gm7
            with frame f-dtldisttot down.
            down with frame f-dtldisttot.
      
            assign regtot-1-sls  = regtot-1-sls + disttot-1-sls
                   regtot-2-sls  = regtot-2-sls + disttot-2-sls
                   regtot-3-sls  = regtot-3-sls + disttot-3-sls
                   regtot-4-sls  = regtot-4-sls + disttot-4-sls
                   regtot-5-sls  = regtot-5-sls + disttot-5-sls
                   regtot-6-sls  = regtot-6-sls + disttot-6-sls
                   regtot-7-sls  = regtot-7-sls + disttot-7-sls
                   regtot-8-sls  = regtot-8-sls + disttot-8-sls
                   regtot-1-cst  = regtot-1-cst + disttot-1-cst
                   regtot-2-cst  = regtot-2-cst + disttot-2-cst
                   regtot-3-cst  = regtot-3-cst + disttot-3-cst
                   regtot-4-cst  = regtot-4-cst + disttot-4-cst
                   regtot-5-cst  = regtot-5-cst + disttot-5-cst
                   regtot-6-cst  = regtot-6-cst + disttot-6-cst
                   reg-gm-plan   = reg-gm-plan + dist-gm-plan
                   regtot-8-cst  = regtot-8-cst + disttot-8-cst
                   reg-cnt       = reg-cnt + 1 
                   disttot-1-sls = 0
                   disttot-2-sls = 0
                   disttot-3-sls = 0
                   disttot-4-sls = 0
                   disttot-5-sls = 0
                   disttot-6-sls = 0
                   disttot-7-sls = 0
                   disttot-8-sls = 0
                   disttot-1-cst = 0
                   disttot-2-cst = 0
                   disttot-3-cst = 0
                   disttot-4-cst = 0
                   disttot-5-cst = 0
                   disttot-6-cst = 0
                   dist-gm-plan  = 0
                   disttot-8-cst = 0
                   dist-cnt      = 0.
       
         end. /* if last-of(tt-sls-det.dist) */

         if last-of (tt-sls-det.region) then do:
      
            if regtot-1-sls > 0 then
               gm1 = round( (((regtot-1-sls - regtot-1-cst) / regtot-1-sls) 
                               * 100) ,2).
            else
               gm1 = 0.
    
            if regtot-2-sls > 0 then
               gm2 = round( (((regtot-2-sls - regtot-2-cst) / regtot-2-sls) 
                               * 100) ,2).
            else 
               gm2 = 0.
       
            if regtot-3-sls > 0 then
               gm3 = round( (((regtot-3-sls - regtot-3-cst) / regtot-3-sls) 
                               * 100) ,2).
            else
               gm3 = 0.
      
            if regtot-4-sls > 0 then
               gm4 = round( (((regtot-4-sls - regtot-4-cst) / regtot-4-sls) 
                               * 100) ,2).
            else
               gm4 = 0.
    
            if regtot-5-sls > 0 then
               gm5 = round( (((regtot-5-sls - regtot-5-cst) / regtot-5-sls) 
                               * 100) ,2).
            else
               gm5 = 0.
           
            if regtot-6-sls > 0 then
               gm6 = round( (((regtot-6-sls - regtot-6-cst) / regtot-6-sls) 
                               * 100) ,2).
            else
               gm6 = 0.

            if reg-gm-plan > 0 then
               reg-gm-plan = round( (reg-gm-plan / reg-cnt) ,2).
            else 
               reg-gm-plan = 0.
           
            if regtot-8-sls > 0 then
               gm7 = round( (((regtot-8-sls - regtot-8-cst) / regtot-8-sls) 
                               * 100) ,2).
            else
               gm7 = 0.

           
            run fix-gm.

            if line-counter + 2 > page-size then do:
               page.
               run write-det-hdgs.
            end. /* if line-counter + 1 > page-size */
         if x-exportfl then
            export stream x-out delimiter "~011"
               "Region"
               tt-sls-sum.region
               " "
               " "
               " "
               " "     
               " "     
               " "
               regtot-1-sls
               (regtot-1-sls - regtot-1-cst)
               gm1
               regtot-2-sls
               (regtot-2-sls - regtot-2-cst)
               gm2
               regtot-3-sls
               (regtot-3-sls - regtot-3-cst)
               gm3
               regtot-4-sls
               (regtot-4-sls - regtot-4-cst)
               gm4
               regtot-5-sls
               (regtot-5-sls - regtot-5-cst)
               gm5
               regtot-6-sls
               (regtot-6-sls - regtot-6-cst)
               gm6
               regtot-8-sls
               (regtot-8-sls - regtot-8-cst)
               gm7.

            disp
              
               gm1
               regtot-2-sls
               gm2
               regtot-3-sls
               gm3
               regtot-4-sls
               gm4
               regtot-5-sls
               gm5
               regtot-6-sls
               gm6
             /*  regtot-7-sls
                 reg-gm-plan   */
               regtot-8-sls
               gm7
            with frame f-dtlregtot down.
            down with frame f-dtlregtot.
      
            assign tot-1-sls    = tot-1-sls + regtot-1-sls
                   tot-2-sls    = tot-2-sls + regtot-2-sls
                   tot-3-sls    = tot-3-sls + regtot-3-sls
                   tot-4-sls    = tot-4-sls + regtot-4-sls
                   tot-5-sls    = tot-5-sls + regtot-5-sls
                   tot-6-sls    = tot-6-sls + regtot-6-sls
                   tot-7-sls    = tot-7-sls + regtot-7-sls
                   tot-8-sls    = tot-8-sls + regtot-8-sls
                   tot-1-cst    = tot-1-cst + regtot-1-cst
                   tot-2-cst    = tot-2-cst + regtot-2-cst
                   tot-3-cst    = tot-3-cst + regtot-3-cst
                   tot-4-cst    = tot-4-cst + regtot-4-cst
                   tot-5-cst    = tot-5-cst + regtot-5-cst
                   tot-6-cst    = tot-6-cst + regtot-6-cst
                   tot-gm-plan  = tot-gm-plan + reg-gm-plan
                   tot-8-cst    = tot-8-cst + regtot-8-cst
                   regtot-1-sls = 0
                   regtot-2-sls = 0
                   regtot-3-sls = 0
                   regtot-4-sls = 0
                   regtot-5-sls = 0
                   regtot-6-sls = 0
                   regtot-7-sls = 0
                   regtot-8-sls = 0
                   regtot-1-cst = 0
                   regtot-2-cst = 0
                   regtot-3-cst = 0
                   regtot-4-cst = 0
                   regtot-5-cst = 0
                   regtot-6-cst = 0
                   reg-gm-plan  = 0
                   regtot-8-cst = 0
                   reg-cnt      = 0.
                
         end. /* if last-of(tt-sls-det.region) */
      end.    /* for each tt-sls-det */
   
      if tot-1-sls > 0 then
         gm1 = round( (((tot-1-sls - tot-1-cst) / tot-1-sls) * 100) ,2).
      else
         gm1 = 0.
      
      if tot-2-sls > 0 then
         gm2 = round( (((tot-2-sls - tot-2-cst) / tot-2-sls) * 100) ,2).
      else 
         gm2 = 0.
      
      if tot-3-sls > 0 then
         gm3 = round( (((tot-3-sls - tot-3-cst) / tot-3-sls) * 100) ,2).
     else
         gm3 = 0.
      
      if tot-4-sls > 0 then
         gm4 = round( (((tot-4-sls - tot-4-cst) / tot-4-sls) * 100) ,2).
      else
         gm4 = 0.
      
      if tot-5-sls > 0 then
         gm5 = round( (((tot-5-sls - tot-5-cst) / tot-5-sls) * 100) ,2).
      else
         gm5 = 0.

      if tot-6-sls > 0 then
         gm6 = round( (((tot-6-sls - tot-6-cst) / tot-6-sls) * 100) ,2).
      else
         gm6 = 0.

      if tot-gm-plan > 0 then
         tot-gm-plan = round( (tot-gm-plan / tot-cnt) ,2).
      else
         tot-gm-plan = 0.

      if tot-8-sls > 0 then
         gm7 = round( (((tot-8-sls - tot-8-cst) / tot-8-sls) * 100) ,2).
      else
         gm7 = 0.

   
      run fix-gm.

      if line-counter + 2 > page-size then do:
         page.
         run write-det-hdgs.
      end.
         if x-exportfl then
            export stream x-out delimiter "~011"
               "Total"
               " "
               " "
               " "
               " "
               " "     
               " "     
               " "
               tot-1-sls
               (tot-1-sls - tot-1-cst)
               gm1
               tot-2-sls
               (tot-2-sls - tot-2-cst)
               gm2
               tot-3-sls
               (tot-3-sls - tot-3-cst)
               gm3
               tot-4-sls
               (tot-4-sls - tot-4-cst)
               gm4
               tot-5-sls
               (tot-5-sls - tot-5-cst)
               gm5
               tot-6-sls
               (tot-6-sls - tot-6-cst)
               gm6
               tot-8-sls
               (tot-8-sls - tot-8-cst)
               gm7.
               


      disp
         tot-1-sls
         gm1
         tot-2-sls
         gm2
         tot-3-sls
         gm3
         tot-4-sls
         gm4
         tot-5-sls
         gm5
         tot-6-sls
         gm6
       /*  tot-7-sls
           tot-gm-plan */
         tot-8-sls
         gm7
      with frame f-dtltot down.
      down with frame f-dtltot.

   end. /* if p-rptfl = "d" */
   else do:
   
      cntr = 0.
      run write-sum-hdgs.
      
      for each tt-sls-sum  use-index k-sdet1
      no-lock break by tt-sls-sum.sls-annual descending
                    by tt-sls-sum.custnm:
                    
         if cntr = 25 then do:
            
            if top25-1-sls > 0 then
               gm1 = round( (((top25-1-sls - top25-1-cst) / top25-1-sls) 
                               * 100) ,2).
            else
               gm1 = 0.
      
            if top25-2-sls > 0 then
               gm2 = round( (((top25-2-sls - top25-2-cst) / top25-2-sls) 
                               * 100) ,2).
            else 
               gm2 = 0.
      
            if top25-3-sls > 0 then   
               gm3 = round( (((top25-3-sls - top25-3-cst) / top25-3-sls) 
                               * 100) ,2).
            else
               gm3 = 0.
      
            if top25-4-sls > 0 then
               gm4 = round( (((top25-4-sls - top25-4-cst) / top25-4-sls) 
                               * 100) ,2).
            else
               gm4 = 0.
    
            if top25-5-sls > 0 then
               gm5 = round( (((top25-5-sls - top25-5-cst) / top25-5-sls) 
                               * 100) ,2).
            else
               gm5 = 0.
            
            if top25-6-sls > 0 then
               gm6 = round( (((top25-6-sls - top25-6-cst) / top25-6-sls) 
                               * 100) ,2).
            else
               gm6 = 0.
            
            if top25-8-sls > 0 then
               gm7 = round( (((top25-8-sls - top25-8-cst) / top25-8-sls) 
                               * 100) ,2).
            else
               gm7 = 0.


            if top25-gm-plan > 0 then
               top25-gm-plan = round( (top25-gm-plan / cntr) ,2).
            else
               top25-gm-plan = 0.
            
            run fix-gm.

            if line-counter + 2 > page-size then do:
                page.
                run write-sum-hdgs.
            end. /* if line-counter + 1 > page-size */

         if x-exportfl then
            export stream x-out delimiter "~011"
               "Top25"
               " "
               " "
               " "
               " "     
               " "
               " "     
               " "
               top25-1-sls
               (top25-1-sls - top25-1-cst)
               gm1
               top25-2-sls
               (top25-2-sls - top25-2-cst)
               gm2
               top25-3-sls
               (top25-3-sls - top25-3-cst)
               gm3
               top25-4-sls
               (top25-4-sls - top25-4-cst)
               gm4
               top25-5-sls
               (top25-5-sls - top25-5-cst)
               gm5
               top25-6-sls
               (top25-6-sls - top25-6-cst)
               gm6
               top25-8-sls
               (top25-8-sls - top25-8-cst)
               gm7.               


            
            disp
               top25-1-sls
               gm1
               top25-2-sls
               gm2
               top25-3-sls
               gm3
               top25-4-sls
               gm4
               top25-5-sls
               gm5
               top25-6-sls
               gm6
            /*   top25-7-sls
                 top25-gm-plan    */
            with frame f-sum25 down.
            down with frame f-sum25.
   /*         
            assign top25-1-sls    = 0
                   top25-2-sls    = 0
                   top25-3-sls    = 0
                   top25-4-sls    = 0
                   top25-5-sls    = 0
                   top25-6-sls    = 0
                   top25-7-sls    = 0
                   top25-1-cst    = 0
                   top25-2-cst    = 0
                   top25-3-cst    = 0
                   top25-4-cst    = 0
                   top25-5-cst    = 0
                   top25-6-cst    = 0
                   top25-gm-plan  = 0
                   top25-cnt      = 0.
   */
            page.
            run write-sum-hdgs.

         end. /* if cntr = 25 */
         
         if cntr >= 100 then do:
         
            assign alloth-1-sls   = alloth-1-sls + tt-sls-sum.mo-1-sls
                   alloth-2-sls   = alloth-2-sls + tt-sls-sum.mo-2-sls
                   alloth-3-sls   = alloth-3-sls + tt-sls-sum.mo-3-sls
                   alloth-4-sls   = alloth-4-sls + tt-sls-sum.sls-ytd
                   alloth-5-sls   = alloth-5-sls + tt-sls-sum.sls-lst-yr
                   alloth-6-sls   = alloth-6-sls + tt-sls-sum.sls-annual
                   alloth-7-sls   = alloth-7-sls + tt-sls-sum.sls-plan
                   alloth-8-sls   = alloth-8-sls + tt-sls-sum.sls-lst-tyr

                   alloth-1-cst   = alloth-1-cst + tt-sls-sum.mo-1-cst
                   alloth-2-cst   = alloth-2-cst + tt-sls-sum.mo-2-cst
                   alloth-3-cst   = alloth-3-cst + tt-sls-sum.mo-3-cst
                   alloth-4-cst   = alloth-4-cst + tt-sls-sum.cst-ytd
                   alloth-5-cst   = alloth-5-cst + tt-sls-sum.cst-lst-yr
                   alloth-6-cst   = alloth-6-cst + tt-sls-sum.cst-annual
                   alloth-gm-plan = alloth-gm-plan + tt-sls-sum.gm-plan
                   alloth-8-cst   = alloth-8-cst + tt-sls-sum.cst-lst-tyr

                   alloth-cnt     = alloth-cnt + 1
                   tot-1-sls = tot-1-sls + tt-sls-sum.mo-1-sls
                   tot-2-sls = tot-2-sls + tt-sls-sum.mo-2-sls
                   tot-3-sls = tot-3-sls + tt-sls-sum.mo-3-sls
                   tot-4-sls = tot-4-sls + tt-sls-sum.sls-ytd
                   tot-5-sls = tot-5-sls + tt-sls-sum.sls-lst-yr
                   tot-6-sls = tot-6-sls + tt-sls-sum.sls-annual
                   tot-7-sls = tot-7-sls + tt-sls-sum.sls-plan
                   tot-8-sls = tot-8-sls + tt-sls-sum.sls-lst-tyr

                   tot-1-cst = tot-1-cst + tt-sls-sum.mo-1-cst
                   tot-2-cst = tot-2-cst + tt-sls-sum.mo-2-cst
                   tot-3-cst = tot-3-cst + tt-sls-sum.mo-3-cst
                   tot-4-cst = tot-4-cst + tt-sls-sum.cst-ytd
                   tot-5-cst = tot-5-cst + tt-sls-sum.cst-lst-yr
                   tot-6-cst = tot-6-cst + tt-sls-sum.cst-annual
                   tot-gm-plan  = tot-gm-plan + tt-sls-sum.gm-plan
                   tot-8-cst = tot-8-cst + tt-sls-sum.cst-lst-tyr

                   tot-cnt      = tot-cnt + 1.
         
         end.    /* if cntr >= 100 */
         else do:
            
            if tt-sls-sum.mo-1-sls > 0 then
               gm1 = round( (((tt-sls-sum.mo-1-sls - tt-sls-sum.mo-1-cst) 
                             / tt-sls-sum.mo-1-sls) * 100) ,2).
            else
               gm1 = 0.
      
            if tt-sls-sum.mo-2-sls > 0 then
               gm2 = round( (((tt-sls-sum.mo-2-sls - tt-sls-sum.mo-2-cst) 
                             / tt-sls-sum.mo-2-sls) * 100) ,2).
            else 
               gm2 = 0.
      
            if tt-sls-sum.mo-3-sls > 0 then
               gm3 = round( (((tt-sls-sum.mo-3-sls - tt-sls-sum.mo-3-cst) 
                             / tt-sls-sum.mo-3-sls) * 100) ,2).
            else
               gm3 = 0.
      
            if tt-sls-sum.sls-ytd > 0 then
               gm4 = round( (((tt-sls-sum.sls-ytd - tt-sls-sum.cst-ytd) 
                             / tt-sls-sum.sls-ytd) * 100) ,2).
            else
               gm4 = 0.
        
            if tt-sls-sum.sls-lst-yr > 0 then
               gm5 = round( (((tt-sls-sum.sls-lst-yr - tt-sls-sum.cst-lst-yr) 
                             / tt-sls-sum.sls-lst-yr) * 100) ,2).
            else
               gm5 = 0.
         
            if tt-sls-sum.sls-annual > 0 then
               gm6 = round( (((tt-sls-sum.sls-annual - tt-sls-sum.cst-annual) 
                             / tt-sls-sum.sls-annual) * 100) ,2).
            else
               gm6 = 0.
            if tt-sls-sum.sls-lst-tyr > 0 then
               gm7 = round( (((tt-sls-sum.sls-lst-tyr - tt-sls-sum.cst-lst-tyr) 
                             / tt-sls-sum.sls-lst-tyr) * 100) ,2).
            else
               gm7 = 0.
 
            run fix-gm.                           
           
            if line-counter + 2 > page-size then do:
               page.
               run write-sum-hdgs.
            end. /* if line-counter + 1 > page-size */
           if x-exportfl then
           export stream x-out delimiter "~011"
            " "
            " "
            " "
            tt-sls-sum.slsrep
            tt-sls-sum.repnm
            tt-sls-sum.custno     
            tt-sls-sum.custnm     
            tt-sls-sum.tech
            tt-sls-sum.mo-1-sls   
            (tt-sls-sum.mo-1-sls - tt-sls-sum.mo-1-cst)   
            gm1               
            tt-sls-sum.mo-2-sls   
            (tt-sls-sum.mo-2-sls - tt-sls-sum.mo-2-cst)   
            gm2               
            tt-sls-sum.mo-3-sls   
            (tt-sls-sum.mo-3-sls - tt-sls-sum.mo-3-cst)   
            gm3               
            tt-sls-sum.sls-ytd    
            (tt-sls-sum.sls-ytd - tt-sls-sum.cst-ytd)    
            gm4               
            tt-sls-sum.sls-lst-yr
            (tt-sls-sum.sls-lst-yr - tt-sls-sum.cst-lst-yr)   
            gm5               
            tt-sls-sum.sls-annual
            (tt-sls-sum.sls-annual - tt-sls-sum.cst-annual)
            gm6
            tt-sls-sum.sls-lst-tyr
            (tt-sls-sum.sls-lst-tyr - tt-sls-sum.cst-lst-tyr)   
            gm7.
 
            disp
               tt-sls-sum.custno     
               tt-sls-sum.custnm     
               tt-sls-sum.repnm
               tt-sls-sum.mo-1-sls   
               gm1               
               tt-sls-sum.mo-2-sls   
               gm2               
               tt-sls-sum.mo-3-sls   
               gm3               
               tt-sls-sum.sls-ytd    
               gm4               
               tt-sls-sum.sls-lst-yr 
               gm5               
               tt-sls-sum.sls-annual
               gm6
            /*   tt-sls-sum.sls-plan
                 tt-sls-sum.gm-plan   */
            with frame f-sumdtl down.
            down with frame f-sumdtl.

            cntr = cntr + 1.
            if cntr < 101 then
              assign top25-1-sls   = top25-1-sls + tt-sls-sum.mo-1-sls
                   top25-2-sls   = top25-2-sls + tt-sls-sum.mo-2-sls
                   top25-3-sls   = top25-3-sls + tt-sls-sum.mo-3-sls
                   top25-4-sls   = top25-4-sls + tt-sls-sum.sls-ytd
                   top25-5-sls   = top25-5-sls + tt-sls-sum.sls-lst-yr
                   top25-6-sls   = top25-6-sls + tt-sls-sum.sls-annual
                   top25-7-sls   = top25-7-sls + tt-sls-sum.sls-plan
                   top25-8-sls   = top25-8-sls + tt-sls-sum.sls-lst-tyr

                   top25-1-cst   = top25-1-cst + tt-sls-sum.mo-1-cst
                   top25-2-cst   = top25-2-cst + tt-sls-sum.mo-2-cst
                   top25-3-cst   = top25-3-cst + tt-sls-sum.mo-3-cst
                   top25-4-cst   = top25-4-cst + tt-sls-sum.cst-ytd
                   top25-5-cst   = top25-5-cst + tt-sls-sum.cst-lst-yr
                   top25-6-cst   = top25-6-cst + tt-sls-sum.cst-annual
                   top25-gm-plan = top25-gm-plan + tt-sls-sum.gm-plan
                   top25-8-cst   = top25-8-cst + tt-sls-sum.cst-lst-tyr

                   top25-cnt     = top25-cnt + 1.
            assign disttot-1-sls = disttot-1-sls + tt-sls-sum.mo-1-sls
                   disttot-2-sls = disttot-2-sls + tt-sls-sum.mo-2-sls
                   disttot-3-sls = disttot-3-sls + tt-sls-sum.mo-3-sls
                   disttot-4-sls = disttot-4-sls + tt-sls-sum.sls-ytd
                   disttot-5-sls = disttot-5-sls + tt-sls-sum.sls-lst-yr
                   disttot-6-sls = disttot-6-sls + tt-sls-sum.sls-annual
                   disttot-7-sls = disttot-7-sls + tt-sls-sum.sls-plan
                   disttot-8-sls = disttot-8-sls + tt-sls-sum.sls-lst-tyr

                   disttot-1-cst = disttot-1-cst + tt-sls-sum.mo-1-cst
                   disttot-2-cst = disttot-2-cst + tt-sls-sum.mo-2-cst
                   disttot-3-cst = disttot-3-cst + tt-sls-sum.mo-3-cst
                   disttot-4-cst = disttot-4-cst + tt-sls-sum.cst-ytd
                   disttot-5-cst = disttot-5-cst + tt-sls-sum.cst-lst-yr
                   disttot-6-cst = disttot-6-cst + tt-sls-sum.cst-annual
                   dist-gm-plan  = dist-gm-plan + tt-sls-sum.gm-plan
                   disttot-8-cst = disttot-8-cst + tt-sls-sum.cst-lst-tyr

                   dist-cnt      = dist-cnt + 1
                   regtot-1-sls = regtot-1-sls + tt-sls-sum.mo-1-sls
                   regtot-2-sls = regtot-2-sls + tt-sls-sum.mo-2-sls
                   regtot-3-sls = regtot-3-sls + tt-sls-sum.mo-3-sls
                   regtot-4-sls = regtot-4-sls + tt-sls-sum.sls-ytd
                   regtot-5-sls = regtot-5-sls + tt-sls-sum.sls-lst-yr
                   regtot-6-sls = regtot-6-sls + tt-sls-sum.sls-annual
                   regtot-7-sls = regtot-7-sls + tt-sls-sum.sls-plan
                   regtot-8-sls = regtot-8-sls + tt-sls-sum.sls-lst-tyr

                   regtot-1-cst = regtot-1-cst + tt-sls-sum.mo-1-cst
                   regtot-2-cst = regtot-2-cst + tt-sls-sum.mo-2-cst
                   regtot-3-cst = regtot-3-cst + tt-sls-sum.mo-3-cst
                   regtot-4-cst = regtot-4-cst + tt-sls-sum.cst-ytd
                   regtot-5-cst = regtot-5-cst + tt-sls-sum.cst-lst-yr
                   regtot-6-cst = regtot-6-cst + tt-sls-sum.cst-annual
                   reg-gm-plan  = reg-gm-plan + tt-sls-sum.gm-plan
                   regtot-8-cst = regtot-8-cst + tt-sls-sum.cst-lst-tyr

                   reg-cnt      = reg-cnt + 1
                   tot-1-sls = tot-1-sls + tt-sls-sum.mo-1-sls
                   tot-2-sls = tot-2-sls + tt-sls-sum.mo-2-sls
                   tot-3-sls = tot-3-sls + tt-sls-sum.mo-3-sls
                   tot-4-sls = tot-4-sls + tt-sls-sum.sls-ytd
                   tot-5-sls = tot-5-sls + tt-sls-sum.sls-lst-yr
                   tot-6-sls = tot-6-sls + tt-sls-sum.sls-annual
                   tot-7-sls = tot-7-sls + tt-sls-sum.sls-plan
                   tot-8-sls = tot-8-sls + tt-sls-sum.sls-lst-tyr

                   tot-1-cst = tot-1-cst + tt-sls-sum.mo-1-cst
                   tot-2-cst = tot-2-cst + tt-sls-sum.mo-2-cst
                   tot-3-cst = tot-3-cst + tt-sls-sum.mo-3-cst
                   tot-4-cst = tot-4-cst + tt-sls-sum.cst-ytd
                   tot-5-cst = tot-5-cst + tt-sls-sum.cst-lst-yr
                   tot-6-cst = tot-6-cst + tt-sls-sum.cst-annual
                   tot-gm-plan  = tot-gm-plan + tt-sls-sum.gm-plan
                   tot-8-cst = tot-8-cst + tt-sls-sum.cst-lst-tyr
                   
                   tot-cnt      = tot-cnt + 1.

         end.    /* else do */
      end.       /* for each tt-sls-sum */
            
      if top25-1-sls > 0 then
        gm1 = round( (((top25-1-sls - top25-1-cst) / top25-1-sls) 
                               * 100) ,2).
      else
        gm1 = 0.
      
      if top25-2-sls > 0 then
        gm2 = round( (((top25-2-sls - top25-2-cst) / top25-2-sls) 
                               * 100) ,2).
      else 
        gm2 = 0.
      
      if top25-3-sls > 0 then
         gm3 = round( (((top25-3-sls - top25-3-cst) / top25-3-sls) 
                               * 100) ,2).
      else
         gm3 = 0.
      
      if top25-4-sls > 0 then
         gm4 = round( (((top25-4-sls - top25-4-cst) / top25-4-sls) 
                               * 100) ,2).
      else
          gm4 = 0.
    
      if top25-5-sls > 0 then
          gm5 = round( (((top25-5-sls - top25-5-cst) / top25-5-sls) 
                               * 100) ,2).
       else
          gm5 = 0.
            
       if top25-6-sls > 0 then
          gm6 = round( (((top25-6-sls - top25-6-cst) / top25-6-sls) 
                               * 100) ,2).
        else
           gm6 = 0.
            
       if top25-8-sls > 0 then
          gm7 = round( (((top25-8-sls - top25-8-cst) / top25-8-sls) 
                               * 100) ,2).
        else
           gm7 = 0.


        if top25-gm-plan > 0 then
           top25-gm-plan = round( (top25-gm-plan / cntr) ,2).
         else                                              
           top25-gm-plan = 0.
            
         run fix-gm.

         if line-counter + 2 > page-size then do:
             page.
             run write-sum-hdgs.
         end. /* if line-counter + 1 > page-size */

         if x-exportfl then
            export stream x-out delimiter "~011"
               "Top100"
               " "
               " "
               " "
               " "
               " "     
               " "     
               " "
               top25-1-sls
               (top25-1-sls - top25-1-cst)
               gm1
               top25-2-sls
               (top25-2-sls - top25-2-cst)
               gm2
               top25-3-sls
               (top25-3-sls - top25-3-cst)
               gm3
               top25-4-sls
               (top25-4-sls - top25-4-cst)
               gm4
               top25-5-sls
               (top25-5-sls - top25-5-cst)
               gm5
               top25-6-sls
               (top25-6-sls - top25-6-cst)
               gm6
               top25-8-sls
               (top25-8-sls - top25-8-cst)
               gm7.               


         disp
               top25-1-sls
               gm1
               top25-2-sls
               gm2
               top25-3-sls
               gm3
               top25-4-sls
               gm4
               top25-5-sls
               gm5
               top25-6-sls
               gm6
            /*   top25-7-sls
                 top25-gm-plan  */
            with frame f-sum100 down.
            down with frame f-sum100.
      
      if alloth-1-sls > 0 then
         gm1 = round( (((alloth-1-sls - alloth-1-cst) / alloth-1-sls) 
                         * 100) ,2).
      else
         gm1 = 0.
      
      if alloth-2-sls > 0 then
         gm2 = round( (((alloth-2-sls - alloth-2-cst) / alloth-2-sls) 
                         * 100) ,2).
      else 
         gm2 = 0.
        
      if alloth-3-sls > 0 then
         gm3 = round( (((alloth-3-sls - alloth-3-cst) / alloth-3-sls) 
                         * 100) ,2).
      else
         gm3 = 0.
     
      if alloth-4-sls > 0 then
         gm4 = round( (((alloth-4-sls - alloth-4-cst) / alloth-4-sls) 
                         * 100) ,2).
      else
         gm4 = 0.
   
      if alloth-5-sls > 0 then
         gm5 = round( (((alloth-5-sls - alloth-5-cst) / alloth-5-sls) 
                         * 100) ,2).
      else
         gm5 = 0.
           
      if alloth-6-sls > 0 then
         gm6 = round( (((alloth-6-sls - alloth-6-cst) / alloth-6-sls) 
                         * 100) ,2).
      else
         gm6 = 0.

      if alloth-8-sls > 0 then
         gm7 = round( (((alloth-8-sls - alloth-8-cst) / alloth-8-sls) 
                         * 100) ,2).
      else
         gm7 = 0.


      if alloth-gm-plan > 0 then
         alloth-gm-plan = round( (alloth-gm-plan / alloth-cnt) ,2).
      else
         alloth-gm-plan = 0.
             
      run fix-gm.
                 
      if line-counter + 1 > page-size then do:
         page.
         run write-sum-hdgs.
      end. /* if line-counter + 1 > page-size */
     
         if x-exportfl then
            export stream x-out delimiter "~011"
               "AllOthers"
               " "
               " " 
               " "
               " "
               " "     
               " "     
               " "
               alloth-1-sls
               (alloth-1-sls - alloth-1-cst)
               gm1
               alloth-2-sls
               (alloth-2-sls - alloth-2-cst)
               gm2
               alloth-3-sls
               (alloth-3-sls - alloth-3-cst)
               gm3
               alloth-4-sls
               (alloth-4-sls - alloth-4-cst)
               gm4
               alloth-5-sls
               (alloth-5-sls - alloth-5-cst)
               gm5
               alloth-6-sls
               (alloth-6-sls - alloth-6-cst)
               gm6
               alloth-8-sls
               (alloth-8-sls - alloth-8-cst)
               gm7.               


      disp
         alloth-1-sls
         gm1
         alloth-2-sls
         gm2
         alloth-3-sls
         gm3
         alloth-4-sls
         gm4
         alloth-5-sls
         gm5
         alloth-6-sls
         gm6
       /*  alloth-7-sls
           alloth-gm-plan  */
      with frame f-alloth down.
      down with frame f-alloth.
           
      assign alloth-1-sls   = 0
             alloth-2-sls   = 0
             alloth-3-sls   = 0
             alloth-4-sls   = 0
             alloth-5-sls   = 0
             alloth-6-sls   = 0
             alloth-7-sls   = 0
             alloth-8-sls   = 0

             alloth-1-cst   = 0
             alloth-2-cst   = 0
             alloth-3-cst   = 0
             alloth-4-cst   = 0
             alloth-5-cst   = 0
             alloth-6-cst   = 0
             alloth-8-cst   = 0
             alloth-gm-plan = 0
             alloth-cnt     = 0.
   
      if tot-1-sls > 0 then
         gm1 = round( (((tot-1-sls - tot-1-cst) / tot-1-sls) * 100) ,2).
      else
         gm1 = 0.
      
      if tot-2-sls > 0 then
         gm2 = round( (((tot-2-sls - tot-2-cst) / tot-2-sls) * 100) ,2).
      else 
         gm2 = 0.
      
      if tot-3-sls > 0 then
         gm3 = round( (((tot-3-sls - tot-3-cst) / tot-3-sls) * 100) ,2).
      else
         gm3 = 0.
      
      if tot-4-sls > 0 then
         gm4 = round( (((tot-4-sls - tot-4-cst) / tot-4-sls) * 100) ,2).
      else
         gm4 = 0.
      
      if tot-5-sls > 0 then
         gm5 = round( (((tot-5-sls - tot-5-cst) / tot-5-sls) * 100) ,2).
      else
         gm5 = 0.

      if tot-6-sls > 0 then
         gm6 = round( (((tot-6-sls - tot-6-cst) / tot-6-sls) * 100) ,2).
      else
         gm6 = 0.

      if tot-8-sls > 0 then
         gm7 = round( (((tot-8-sls - tot-8-cst) / tot-8-sls) * 100) ,2).
      else
         gm7 = 0.


      if tot-gm-plan > 0 then
         tot-gm-plan = round( (tot-gm-plan / tot-cnt) ,2).
      else
         tot-gm-plan = 0.
   
      run fix-gm.

      if line-counter + 2 > page-size then do:
         page.
         run write-det-hdgs.
      end.
         if x-exportfl then
            export stream x-out delimiter "~011"
               "Total"
               " "
               " "
               " "
               " "
               " "     
               " "     
               " "
               tot-1-sls
               (tot-1-sls - tot-1-cst)
               gm1
               tot-2-sls
               (tot-2-sls - tot-2-cst)
               gm2
               tot-3-sls
               (tot-3-sls - tot-3-cst)
               gm3
               tot-4-sls
               (tot-4-sls - tot-4-cst)
               gm4
               tot-5-sls
               (tot-5-sls - tot-5-cst)
               gm5
               tot-6-sls
               (tot-6-sls - tot-6-cst)
               gm6
               tot-8-sls
               (tot-8-sls - tot-8-cst)
               gm7.               


      disp
         tot-1-sls
         gm1
         tot-2-sls
         gm2
         tot-3-sls
         gm3
         tot-4-sls
         gm4
         tot-5-sls
         gm5
         tot-6-sls
         gm6
       /*  tot-7-sls
           tot-gm-plan   */
      with frame f-sumtot down.
      down with frame f-sumtot.

   end. /* else do */
end.    /* else do */
PROCEDURE fix-gm.
    IF gm1 LT 9999- THEN
       ASSIGN gm1 = 9999-.
    ELSE          
       IF gm1 GT 9999 THEN
          ASSIGN gm1 = 9999.
    
    IF gm2 LT 9999- THEN
       ASSIGN gm2 = 9999-.
    ELSE 
       IF gm2 GT 9999 THEN
          ASSIGN gm2 = 9999.
    
    IF gm3 LT 9999- THEN
       ASSIGN gm3 = 9999-.
    ELSE 
       IF gm3 GT 9999 THEN
          ASSIGN gm3 = 9999.
    
    IF gm4 LT 9999- THEN
       ASSIGN gm4 = 9999-.
    ELSE 
       IF gm4 GT 9999 THEN
          ASSIGN gm4 = 9999.
    
    IF gm5 LT 9999- THEN
       ASSIGN gm5 = 9999-.
    ELSE 
       IF gm5 GT 9999 THEN
          ASSIGN gm5 = 9999.

    IF gm6 LT 9999- THEN
       ASSIGN gm6 = 9999-.
    ELSE 
       IF gm6 GT 9999 THEN
          ASSIGN gm6 = 9999.

    IF gm7 LT 9999- THEN
       ASSIGN gm7 = 9999-.
    ELSE 
       IF gm7 GT 9999 THEN
          ASSIGN gm7 = 9999.


    RETURN.
END PROCEDURE. /* fix-gm */

procedure write-det-hdgs.
   x-page = x-page + 1.
   disp with frame kheadings1.
   disp
     rptdate
     rpt-dow         
     rpt-time
     rpt-cono           
     rpt-user           
     x-page             
     x-title            
    with frame kheadings1a.
   disp
      x-slsrep
      x-region
      x-dist
   with frame f-dettitle down.
   down with frame f-dettitle.
         
   disp   
      x-mo-1
      x-mo-2
      x-mo-3
   with frame f-dethdr down.
   down with frame f-dethdr.

   return.
end procedure. /* write-det-hdgs */

procedure write-sum-hdgs.
/*
   disp
      x-region
      x-dist
   with frame f-sumtitle down.
   down with frame f-sumtitle.
*/                 
   x-page = x-page + 1.
   disp with frame kheadings1.
   disp
     rptdate
     rpt-dow         
     rpt-time
     rpt-cono           
     rpt-user           
     x-page             
     x-title            
    with frame kheadings1a.
   
   disp
      x-mo-1
      x-mo-2
      x-mo-3
   with frame f-sumhdr down.
   down with frame f-sumhdr.

   return.
end procedure. /* write-sum-hdgs */

