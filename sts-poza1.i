/***************************************************************************
 PROGRAM NAME: sts-poza1.i
  PROJECT NO.: 99-16
  DESCRIPTION: This program file def's for poza... programs
 DATE WRITTEN: 03/10/00
       AUTHOR: Sir Carl E. Graul Esq.
     MODIFIED: 

***************************************************************************/


def {1} var q-prod like poel.shipprod.
def {1} var q-whse like poel.whse.
def {1} var q-open as logical init yes.
def {1} var q-valrec as logical init yes.
def {1} var q-ptb as char format "x(1)" init "B".
def {1} var q-pono like poel.pono.
def {1} var q-posuf like poel.posuf.
def {1} var q-lineno like poel.lineno.

def {1} var to-qtyreservd like icsw.qtyreservd no-undo.
def {1} var to-qtycommit like icsw.qtycommit no-undo.
def {1} var to-whse like icsw.whse extent 3 no-undo.
def {1} var to-qtyoh like icsw.qtyonhand extent 3 no-undo.
def {1} var to-qtyonhand like icsw.qtyonhand no-undo.
def {1} var to-avail like icsw.xxde1 no-undo. /* I need the neg sign */

def {1} var err-msg as char format "x(60)" init "".

/* 1 = po#  2 = prod#  3 = po# + prod# */
def {1} var sts-select as integer init 0. 

def {1} temp-table t-ack1 no-undo
    field tcono      like wleh.cono             label "tcono"
    field ent-dt     like poel.enterdt          label "ent-dt"
    field due-dt     like poel.duedt            label "due-dt"
    field fst-ack-dt like poel.reqshipdt        label "1stExShp"
    field exp-dt     like poel.expshipdt        label "exp-dt"
    field t-type     as char format "x(2)"      label "t-type"
    field ack-no     as char format "x(12)"     label "ack-no"
    field ack-no-x   as char format "x(14)"     label "ack-no-x"
    field vend-no    as char format "x(13)"     label "vend-no"
    field whse       like poel.whse             label "whse"
    field ackcd      as char format "x"         label "ackcd"
    field ackuser    as char format "x(8)"      label "ackuser" 
    field ack-dt     like poel.user9            label "ack-dt"
    field qty        as i format "zzzzzz9"      label "qty"
    field stagecd    like poeh.stagecd          label "stagecd"
    field lineno     like poel.lineno           label "lineno"
    field prod       like poel.shipprod         label "prod"

    /* kind of tie + number */
    field tied-out   as char format "x(36)"     label "tied-out" 
    field trkno      as char format "x(35)"     label "trkno"
    field ackno      as char format "x(35)"     label "ackno"
    field notesfl    as char format "x(1)"      label "notesfl"
    field commfl     as char format "x(1)"      label "commfl"
    field tno        like poeh.pono             label "tno"
    field tsuf       like poeh.posuf            label "tsuf"
    field taltno     like poelo.orderaltno      label "taltno"
    field taltsuf    like poelo.orderaltsuf     label "taltsuf"
    field tied       as char format "x"         label "tied"
    field tiedto     as char format "x"         label "tiedto"
       index k-t-ack2 is primary
            tcono    ascending
            ent-dt   ascending
       index k-t-ack3
            tcono    ascending
            ack-no   ascending
            lineno   ascending.






