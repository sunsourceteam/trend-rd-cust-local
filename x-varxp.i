
/{&user_temptable}*/

define {&sharetype} temp-table xxrepxx no-undo

  field slsrep like smsn.slsrep
  field region as char format "x"
  field district as char format "xxx"
  field sales    as dec  
  field year     as int
  index xx
     slsrep.

/* selection list includes */
{zsapboydef.i}    
{zsapboycheck.i} 


def var p-regval  as char  no-undo.    
  
{g-vastg2.i}
{vastage.gva}

define buffer x-vaspsl for vaspsl.
define buffer x-vasps  for vasps.
define buffer v-vaesl  for vaesl.
define buffer v-vaelo  for vaelo.
define buffer v-vaes   for vaes.
define buffer v-vaeh   for vaeh.
/* zsap switches */
def var sw-s as logical no-undo.
def var sw-b as logical no-undo.
def var sw-c as logical no-undo.
def var sw-p as logical no-undo.
def var sw-t as logical no-undo.

/* ranges */
def var rVaNo        as dec                  extent 2  no-undo.   
def var rWhse        as char                 extent 2  no-undo.    
def var rTaken       as char                 extent 2  no-undo.   
def var custnum      as dec                  extent 2  no-undo.
def var rCust        as dec                  extent 2  no-undo.    
def var rProdCat     as char format "xxxx"   extent 2  no-undo.   
def var rStage       as char format "xxx"    extent 2  no-undo.   
def var rVend        as dec                  extent 2  no-undo.   
def var rSctnPromDt  as date                 extent 2  no-undo.  
def var rSctnStage   as char format "x(8)"   extent 2  no-undo.   
def var rFinProd     as char                 extent 2  no-undo.   
def var rLnProd      as char                 extent 2  no-undo.   
def var rEnterDt     as date                 extent 2  no-undo.  

/* def Options */
def var oFormat      as dec                   no-undo.
def var oPromDtLess  as logical               no-undo.      
def var oNonTieLate  as logical               no-undo.
def var oFutureLate  as logical               no-undo.
def var oList        as logical               no-undo.
def var oExport      as logical               no-undo.

def var v-custno     as char   format "x(12)" no-undo.
def var v-custnm     as char   format "x(30)" no-undo.
def var v-vendno     as char   format "x(12)" no-undo.
def var v-vendnm     as char   format "x(30)" no-undo.
def var v-ackncd     as char   format "x(01)" no-undo.
def var v-ackndt     as char   format "x(08)" no-undo.
def var v-dotype     as char   format "xxx"   no-undo.
def var v-poelpono   as char   format "x(11)" no-undo.
def var v-poellineno as char   format "x(03)" no-undo.
def var v-vaelovano  as char   format "x(11)" no-undo.
def var v-all        as char   format "x(11)" no-undo.
def var v-allline    as char   format "x(10)" no-undo.
def var v-vaesllineno as char   format "x(03)" no-undo.
def var v-vaeslseqno as char   format "x(03)" no-undo.
def var v-wtelwtno   as char   format "x(11)" no-undo.
def var v-wtellineno as char   format "x(03)" no-undo.
/* def var v-exptshipdt as char   format "x(08)" no-undo.  */
def var v-exptshipdt as date no-undo.

def var v-seqno      as char   format "x(6)"  no-undo.
def var v-stage      as char   format "x(3)"  no-undo. 
def var v-sctnstage  as char   format "x(6)"  no-undo. 
def var v-idx        as int                   no-undo. 
def var b-stageidx   as char                  no-undo. 
def var e-stageidx   as char                  no-undo. 
def var b-sctnstgidx as char                   no-undo. 
def var e-sctnstgidx as char                   no-undo. 
def var p-format     as integer               no-undo.
def var p-detailprt  as logical               no-undo.
def var pk_good      as logical.

/{&user_temptable}*/

/{&user_drpttable}*/
/* Use to add fields to the report table */

   field vaehid  as recid
   field vaesid  as recid
   field vaeslid as recid
   field poelid  as recid
   field wtelid  as recid
   
/{&user_drpttable}*/

/{&user_forms}*/
form header
  " " /* "~015 " */ at 1
  with frame fx-trn1 page-top.

form header
  rpt-dt              at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: VARXP"   space
  "Operator:"         space
  rpt-user            space
  "page:"             at 168
  page-number(xpcd)   at 173 format ">>>9"
/*  "~015"              at 177 */
  "Late Products Tied to VA Orders" at 71
   with frame fx-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.

form header
  "Product/"    at 58 
  "ExpShpDt/"   at 145
  "TakenBy"     at 1 
  "Whse"        at 10 
  "Customer"    at 18 
   "VA#"        at 32 
   "Seq#"       at 44 
   "Ln#"        at 51 
   "Vendor"     at 58
   "PromDt"     at 77 
   "DO"         at 97
   "Tie"        at 106 
   "Ln#"        at 125
   "AckCd"      at 135
   "LastAckDt"  at 145
  with frame fx-trn3 NO-BOX NO-LABELS no-underline WIDTH 178 page-top. 

form header
  "Product/"    at 58 
  "ExpShpDt/"   at 145
  "TakenBy"     at 1 
  "Whse"        at 10 
  "Customer"    at 18 
   "VA#"        at 32 
   "Seq#"       at 44 
   "Ln#"        at 51 
   "Vendor"     at 58
   "PromDt"     at 82 
   "DO"         at 97
   "Tie"        at 106 
   "Ln#"        at 125
   "AckCd"      at 135
   "LastAckDt"  at 145
  with frame f-trn3 NO-BOX NO-LABELS no-underline WIDTH 178 page-top. 

form header
  " " /* "~015 " */ at 1
  with frame f-trn1 page-top.

form header
  rpt-dt              at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: VARXP"   space
  "Operator:"         space
  rpt-user            space
  "page:"             at 168
  page-number         at 173 format ">>>9"
  "   Late Products Tied to VA Orders" at 72
   with frame f-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.

form header 
  " " /*  "~015"    */                 at 177
  with frame f-trn3 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.

form 
 " " /* "~015" */          at 1
with frame f-skip width 178 no-box no-labels.  

form
  vaeh.takenby        at 1
  vaeh.whse           at 10
  v-custno            at 17  format "x(12)"
  vaeh.vano           at 31  
  "-"                 at 38 
  vaeh.vasuf          at 39 
  vaes.seqno          at 42  format "zzz9.9" 
/*   ".00"               at 45  */
  vaesl.lineno        at 51  format "zz9" 
  vaesl.shipprod      at 58  format "x(24)" 
  vaes.promisedt      at 82
  v-dotype            at 97
/*
  v-poelpono          at 106
  v-poellineno        at 125
*/
  v-all               at 106
  v-allline           at 125
  v-ackncd            at 135
  v-exptshipdt        at 145
/*
  v-wtelwtno          at 155
  v-wtellineno        at 165
  v-vaelovano         at 155
*/  
/*  "~015"              at 176 */
with frame f-detail-1 width 178 no-box no-labels.  

form
/*   "~015"           at 1  */
  v-custnm         at 18
  v-vendnm         at 59   
  v-ackndt         at 146 no-label 
/*  "~015"           at 176 */
with frame f-detail-2 width 178 no-box no-labels.  

/{&user_forms}*/

/{&user_extractrun}*/
 run sapb_vars.
/*  run extract_data.  */
/{&user_extractrun}*/

/{&user_exportstatheaders}*/
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
/* 
  put stream expt unformatted " " +
      v-del + v-del + v-del + v-del + v-del + v-del + v-del + 
      /*"Product/" */ v-del + v-del +
       v-del
       v-del
       v-del
       v-del
       v-del
      "ExpShpDt/"  + v-del
      chr(13).     
 */
  assign export_rec = "".  
  assign export_rec =
         export_rec   +  
         "TakenBy"    + v-del +
         "Whse"       + v-del +
         "Customer"   + v-del +
         "Cust #"     + v-del +
         "VA#"        + v-del +
         "  Seq#"     + v-del +
         "  Ln#"      + v-del +
         "Product"    + v-del +
         "Vendor"     + v-del +
         " PromDt"    + v-del +
         "DO"         + v-del +
         "PO#"        + v-del +
         "  Ln#"      + v-del +
         "AckCd"      + v-del +
         "ExpShpDt"   + v-del +
         " LastAckDt".
         
/*
  if p-detailprt then
     assign export_rec = export_rec /* + v-del . */ .
*/

/{&user_exportstatheaders}*/

/{&user_exportstatheaders1}*/
  assign export_rec = "". 
/{&user_exportstatheaders1}*/

/{&user_foreach}*/
/* must have an iterating block here - a label is in zsdixrptx.p */
for each vaeh use-index k-active where  
         vaeh.cono       = g-cono     and
         vaeh.statustype = yes        and  
         vaeh.vano >= int(rVaNo[1])   and 
         vaeh.vano <= int(rVaNo[2])   and
         vaeh.whse >= rWhse[1]        and
         vaeh.whse <= rWhse[2]        and 
         vaeh.takenby  >= rTaken[1]   and
         vaeh.takenby  <= rTaken[2]   and 
         vaeh.shipprod >= rFinProd[1] and
         vaeh.shipprod <= rFinProd[2] and 
         vaeh.stagecd  >= int(b-stageidx)  and 
         vaeh.stagecd  <= int(e-stageidx)  and /* 1=ent 2=ord 3=prt */  
         vaeh.enterdt  >= rEnterDt[1] and 
         vaeh.enterdt  <= rEnterDt[2] 
         no-lock, 
    each vaes use-index k-section where  
         vaes.cono      = g-cono          and
         vaes.completefl = no             and   
         vaes.sctntype  = "in"            and   
         vaes.vano      = vaeh.vano       and 
         vaes.vasuf     = vaeh.vasuf      and
         vaes.stagecd   >= int(b-sctnstgidx)   and /* 1=open 2=print 3=ship */  
         vaes.stagecd   <= int(e-sctnstgidx)   and 
         vaes.promisedt >= rSctnPromDt[1] and
         vaes.promisedt <= rSctnPromDt[2]      
         no-lock, 
    each vaesl  use-index k-active where  
         vaesl.cono       = g-cono        and
         vaesl.sctntype   = "in"          and   
         vaesl.completefl = no            and  
         vaesl.vano       = vaeh.vano     and 
         vaesl.vasuf      = vaeh.vasuf    and
         vaesl.seqno      = vaes.seqno    and 
         vaesl.shipprod  >= rLnProd[1]    and 
         vaesl.shipprod  <= rLnProd[2]    and
         vaesl.prodcat   >= rProdCat[1]   and 
         vaesl.prodcat   <= rProdCat[2]   and   
         vaesl.arpvendno >= dec(rVend[1]) and 
         vaesl.arpvendno <= dec(rVend[2]) and       
        (vaesl.qtyship < vaesl.qtyord) 
         no-lock,
     each vaelo use-index k-vaelo where 
          vaelo.cono      = g-cono        and
          vaelo.vano      = vaeh.vano     and
          vaelo.vasuf     = vaeh.vasuf    and
          vaelo.custno    >= custnum[1]   and
          vaelo.custno    <= custnum[2]
          no-lock:

    
/* check list */
            
     if oList then do:
  /*    message "got here".   */
      pk_good = true.
      /*
      zelection_type = "o".              
      zelection_char4 = string(vaeh.vano).
      zelection_cust = 0.                
      run zelectioncheck.             
      if zelection_good = false then do:
        pk_good = false.
        next buildrec.
      end.
      */
      zelection_type = "p".              
      zelection_char4 = vaesl.prodcat. 
      zelection_cust = 0.                
      run zelectioncheck.             
      if zelection_good = false then do:
        pk_good = false.
        next buildrec.
      end.

      zelection_type = "k".              
      zelection_char4 = vaeh.takenby. 
      zelection_cust = 0.                
      run zelectioncheck.             
      if zelection_good = false then do:
        pk_good = false.
        next buildrec. 
      end.
      zelection_type = "w".
      zelection_char4 = vaeh.whse. 
      zelection_cust = 0.                
      run zelectioncheck.             
      if zelection_good = false then do:
        pk_good = false.
        next buildrec.
      end.
      zelection_type = "v".              
      zelection_vend = vaesl.arpvendno. 
      zelection_cust = 0.                
      run zelectioncheck.             
      if zelection_good = false then do:
        pk_good = false.
        next buildrec.
      end.
/*   display zelection_type zelection_char4 zelection_cust pk_good.   */
    end. /* oList */
  
  
  find first poelo use-index k-order where
           poelo.cono = g-cono              and 
           poelo.ordertype   = "f"          and 
           poelo.orderaltno  = vaesl.vano   and 
           poelo.orderaltsuf = vaesl.vasuf  and 
           poelo.seqaltno    = vaesl.seqno  and 
           poelo.linealtno   = vaesl.lineno 
           no-lock no-error.         
        /*   display "found poelo".  */
   if avail poelo and poelo.ordertype = "f" and 
      poelo.seqaltno > 0 then do:    

    for each poel use-index k-poel where
             poel.cono   = g-cono       and               
             poel.pono   = poelo.pono   and 
             poel.posuf  = poelo.posuf  and 
             poel.lineno = poelo.lineno
             no-lock:  
         /*    display poel.pono poel.lineno.  */
   end.
 end.  
  
  
  find first wtelo use-index k-order where 
                       wtelo.wtcono = g-cono              and 
                       wtelo.ordertype   = "f"          and 
                       wtelo.orderaltno  = vaesl.vano   and 
                       wtelo.orderaltsuf = vaesl.vasuf  and 
                       wtelo.seqaltno    = vaesl.seqno  and                                           wtelo.linealtno   = vaesl.lineno 
                       no-lock no-error.          

       if avail wtelo and wtelo.ordertype = "f" and 
                wtelo.seqaltno > 0 then do:    
         /*   display "got wtelo". pause.  */
            for each wtel use-index k-wtel where
                     wtel.cono   = g-cono       and               
                     wtel.wtno   = wtelo.wtno   and 
                     wtel.wtsuf  = wtelo.wtsuf  and 
                     wtel.lineno = wtelo.lineno
                     no-lock:  
                  /*   display wtel.wtno wtel.lineno. pause.  */
            end.
      end.      
      
            
     find first v-vaelo use-index k-order where v-vaelo.cono = g-cono and 
                       v-vaelo.ordertype   = "f"          and 
                       v-vaelo.orderaltno  = vaesl.vano   and 
                       v-vaelo.orderaltsuf = vaesl.vasuf  and 
                       v-vaelo.seqaltno    = vaesl.seqno  and                                           v-vaelo.linealtno   = vaesl.lineno 
                       no-lock no-error.          
       
       
       if avail v-vaelo and v-vaelo.ordertype = "f" and 
                v-vaelo.seqaltno > 0 then do:    

          for each v-vaes use-index k-section where  
                   v-vaes.cono      = v-vaelo.cono and
            /*     v-vaes.completefl = no        and   
                   v-vaes.sctntype  = "in"       and */  
                   v-vaes.vano      = v-vaelo.vano and 
                   v-vaes.vasuf     = v-vaelo.vasuf /*and
                   v-vaes.stagecd   >= 1   and /* 1=open 2=print 3=ship */  
                   v-vaes.stagecd   <= 3  /*  and  */ 
                   v-vaes.promisedt >= 01/01/08 and
                   v-vaes.promisedt <= 03/01/09        */
                   no-lock:
          /*   display v-vaes.vano v-vaes.vasuf v-vaes.seqno. pause.  */
          /*
            for each v-vaesl use-index k-vaesl where
                     v-vaesl.cono   = g-cono       and               
                     v-vaesl.vano   = v-vaes.vano   and 
                     v-vaesl.vasuf  = v-vaes.vasuf  and 
                     v-vaesl.seqno  = v-vaes.seqno
                     no-lock:  
            end. */
         end.      
      end.    
            

   assign v-vendno = string(vaesl.arpvendno). 
   find first apsv where apsv.cono = g-cono and 
              apsv.vendno = vaesl.arpvendno 
              no-lock no-error. 
   if avail apsv then 
    assign v-vendnm = apsv.name. 
   else 
    assign v-vendnm = "".          
/*   
   find first vaelo where 
              vaelo.cono      = g-cono and 
              vaelo.ordertype = "o"    and  
              vaelo.vano      = vaeh.vano 
              no-lock no-error.   */
              
   if avail vaelo and vaelo.ordertype = "o" then do:            
    find oeeh where 
         oeeh.cono     = g-cono           and 
         oeeh.orderno  = vaelo.orderaltno and 
         oeeh.ordersuf = vaelo.orderaltsuf
         no-lock no-error.  
    if avail oeeh then do: 
     find first arsc where arsc.cono = g-cono and
                arsc.custno = oeeh.custno 
        no-lock no-error.
    end. /* avail oeeh */
   end. /* avail vaelo */
            
 

/{&user_foreach}*/

/{&user_drptassign}*/
 /* If you add fields to the dprt table this is where you assign them */

  drpt.vaehid  = recid(vaeh)
  drpt.vaesid  = recid(vaes)
  drpt.vaeslid = recid(vaesl)

/{&user_drptassign}*/

/{&user_viewheadframes}*/
/* Since frames are unique to each program you need to put your
   views and hides here with your names */

if not p-exportl then do:
 if not x-stream then do:
  hide frame f-trn1.
  hide frame f-trn3.
 end.
 else do:
  hide stream xpcd frame fx-trn1.
  hide stream xpcd frame fx-trn3.
 end.
end.

if not p-exportl  then do:
 if not x-stream then do:
  page.
  view frame f-trn1.
/* view frame f-trn2. */
  view frame f-trn3.
 end.
 else do:
  page stream xpcd.
  view stream xpcd frame fx-trn1.
  view stream xpcd frame fx-trn3.
 end.
end.
else do: 
 page.
 view frame f-trn1.
 view frame f-trn3.
end.

/{&user_viewheadframes}*/

/{&user_drptloop}*/

/{&user_drptloop}*/


/{&user_detailprint}*/
   /* Comment this out if you are not using detail 'X' option  */

   if p-detailprt and 
      entry(u-entitys,drpt.sortmystery,v-delim) <> 
      fill("~~",length(entry(u-entitys,drpt.sortmystery,v-delim))) then do: 
    /* this code skips the all other lines for printing detail */
    /* next line would be your detail print procedure */
    p-detailprt = p-detailprt.
    if p-exportl then
     run print-dtl-export(input drpt.vaehid, 
                          input drpt.vaesid, 
                          input drpt.vaeslid). 
    else
     run print-dtl(input drpt.vaehid, 
                   input drpt.vaesid, 
                   input drpt.vaeslid). 
   end.

/{&user_detailprint}*/

/{&user_finalprint}*/
/{&user_finalprint}*/

/{&user_summaryframeprint}*/
/{&user_summaryframeprint}*/

/{&user_summaryputexport}*/
return.
/{&user_summaryputexport}*/

/{&user_procedures}*/

procedure sapb_vars:

/* set Ranges */
rVaNo[1] = decimal(sapb.rangebeg[1]). 
rVaNo[2] = decimal(sapb.rangeend[1]). 
rWhse[1] =  sapb.rangebeg[2].
rWhse[2] =  sapb.rangeend[2].
if rwhse[1] = "" then 
 assign rwhse[1] = "aaaa". 
if rwhse[2] = "" then 
 assign rwhse[2] = "zzzz". 
if rwhse[1] ne "" and 
   rwhse[2]  = "" then 
 assign rwhse[2] = rwhse[1].   
rTaken[1]       = sapb.rangebeg[3].
rTaken[2]       = sapb.rangeend[3].
/* rCust[1]        = decimal(sapb.rangebeg[4]). 
rCust[2]        = decimal(sapb.rangeend[4]). */
rProdCat[1]     = sapb.rangebeg[5].
rProdCat[2]     = sapb.rangeend[5].
/* rStage[1]       = sapb.rangebeg[6].
   rStage[2]       = sapb.rangeend[6].  */
rVend[1]        = decimal(sapb.rangebeg[7]).
rVend[2]        = decimal(sapb.rangeend[7]).
/*rSctnStage[1]   = sapb.rangebeg[9].
  rSctnStage[2]   = sapb.rangeend[9]. */
rFinProd[1]     = sapb.rangebeg[10].
rFinProd[2]     = sapb.rangeend[10].
rLnProd[1]      = sapb.rangebeg[11].
rLnProd[2]      = sapb.rangeend[11].
custnum[1]      = decimal(sapb.rangebeg[4]).
custnum[2]      = decimal(sapb.rangeend[4]).

/** section promised date **/
if sapb.rangebeg[8] ne "" then do: 
 v-datein = sapb.rangebeg[8].
 {p-rptdt.i}
 if string(v-dateout) = v-lowdt then
  rSctnPromDt[1] = 01/01/1900.
 else  
  rSctnPromDt[1] = v-dateout.
end.
else
  rSctnPromDt[1] = 01/01/1900.
if sapb.rangeend[8] ne "" then do: 
 v-datein = sapb.rangeend[8].
 {p-rptdt.i}
 if string(v-dateout) = v-lowdt then
  rSctnPromDt[2] = 12/31/2049.
 else  
  rSctnPromDt[2] = v-dateout.
end.
else
  rSctnPromDt[2] = 12/31/2049.

/** entered date **/
if sapb.rangebeg[12] ne "" then do: 
 v-datein = sapb.rangebeg[12].
 {p-rptdt.i}
 if string(v-dateout) = v-lowdt then
  rEnterDt[1] = 01/01/1900.
 else  
  rEnterDt[1] = v-dateout.
end.
else
  rEnterDt[1] = 01/01/1900.
if sapb.rangeend[12] ne "" then do: 
 v-datein = sapb.rangeend[12].
 {p-rptdt.i}
 if string(v-dateout) = v-lowdt then
  rEnterDt[2] = 12/31/2049.
 else  
  rEnterDt[2] = v-dateout.
end.
else
  rEnterDt[2] = 12/31/2049.


 b-stageidx = sapb.rangebeg[6].
 e-stageidx = sapb.rangeend[6].

 if b-stageidx = " " then
    b-sctnstgidx = "1".
 if e-stageidx begins "~~~~~~" then
    e-stageidx = "3".
 
 
 b-sctnstgidx  = sapb.rangebeg[9]. 
 e-sctnstgidx  = sapb.rangeend[9].

 if b-sctnstgidx = " " then
    b-sctnstgidx = "1".
 if e-sctnstgidx begins "~~~~~~" then
    e-sctnstgidx = "4".
    
assign p-detailprt = yes
       p-detail    = "d"
       p-portrait  = no. 

/* set Options */
oFormat        = dec(sapb.optvalue[1]).    
oPromDtLess    = if sapb.optvalue[2] = "yes" then yes else no. 
oNonTieLate    = if sapb.optvalue[4] = "yes" then yes else no. 
oFutureLate    = if sapb.optvalue[5] = "yes" then yes else no. 
oList          = if sapb.optvalue[6] = "yes" then yes else no. 
/* oExport        = if sapb.optvalue[7] = "yes" then yes else no.  */
p-export       = sapb.optvalue[7]. 

if p-export <> "" then
  assign oExport = true.
else
  assign oExport = false.

 assign  
  zzl_begordno    = rVaNo[1] 
  zzl_endordno    = rVaNo[2] 
  zzl_begcat      = rProdCat[1] 
  zzl_endcat      = rProdCat[2] 
  zzl_begtakenby  = rTaken[1] 
  zzl_endtakenby  = rTaken[2] 
  zzl_begwhse     = rWhse[1]
  zzl_endwhse     = rWhse[2]
  zzl_begvend     = rVend[1]
  zzl_endvend     = rVend[2].
 assign   
  zel_begordno    = rVaNo[1] 
  zel_endordno    = rVaNo[2] 
  zel_begcat      = rProdCat[1] 
  zel_endcat      = rProdCat[2] 
  zel_begtakenby  = rTaken[1] 
  zel_endtakenby  = rTaken[2] 
  zel_begwhse     = rWhse[1]
  zel_endwhse     = rWhse[2]
  zel_begvend     = rVend[1]
  zel_endvend     = rVend[2].


 if oList then do:
/*    message "running list". pause.    */
    {zsapboyload.i}

 
 
  assign   
    rVaNo[1]    = zel_begordno  
    rVaNo[2]    = zel_endordno 
    rProdCat[1] = zel_begcat
    rProdCat[2] = zel_endcat 
    rTaken[1]   = zel_begtakenby
    rTaken[2]   = zel_endtakenby 
    rWhse[1]    = zel_begwhse 
    rWhse[2]    = zel_endwhse  
    rVend[1]    = zel_begvend 
    rVend[2]    = zel_endvend.


 end.

/* end.  */



/* Zelection_matrix has to have an extent for each selection type 
   right now we have. During the load these booleans are set showing what
   options have been selected.
   
   s - sales rep               ....Index 1
   c - customer number         ....Index 2
   p - product cat             ....Index 3
   b - buyer                   ....Index 4
   t - Order types             ....Index 5
   w - Warehouse               ....Index 6
   v - Vendor number           ....Index 7
   N - Vendor Name             ....Index 8
   k - taken by                ....Index 14
 PLEASE KEEP THIS DOCUMENTATION UPDATED WHEN CHANGED 
 
*/ 
  
assign zelection_matrix [1] = (if zelection_matrix[1] > 1 then /* Rep   */
                                 zelection_matrix[1]
                              else   
                                 1)
       zelection_matrix [2] = (if zelection_matrix[2] > 1 then  /* Cust */
                                 zelection_matrix[2]
                              else   
                                 1)
       zelection_matrix [3] = (if zelection_matrix[3] > 1 then  /* Cat  */
                                 zelection_matrix[3]
                              else   
                                 1)
       zelection_matrix [4] = (if zelection_matrix[4] > 1 then  /* Buyer  */
                                 zelection_matrix[4]
                              else   
                                 1)
       zelection_matrix [6] = (if zelection_matrix[6] > 1 then  /* Whse */
                                 zelection_matrix[6]
                              else   
                                 1)
       zelection_matrix [7] = (if zelection_matrix[7] > 1 then  /* Vendor */
                                 zelection_matrix[7]
                              else   
                                 1)
                                 
       zelection_matrix [8] = (if zelection_matrix[8] > 1 then  /* VName */
                                 zelection_matrix[8]
                              else   
                                 1)
                                 
       zelection_matrix [14] = (if zelection_matrix[14] > 1 then  /* TakenBy */
                                 zelection_matrix[14]
                              else   
                                 1).
                     
 
 

 run formatoptions.
/*
  message  "VA" rVaNo[1] 
                rVaNo[2] 
           " Pcat " rProdCat[1] 
           rProdCat[2] 
           " TAken"
           rTaken[1] 
           rTaken[2] 
           " Whse "
           rWhse[1]
           rWhse[2]
           " VEnd "
           rVend[1]
           rVend[2]
           " Stg "
           b-sctnstgidx
           e-sctnstgidx.
           pause.
*/ 

end.


/* ------------------------------------------------------------------------- */
procedure formatoptions:
/* ------------------------------------------------------------------------- */
   
  assign p-portrait = false.
  /* if false then the report will print in LANDSCAPE 178 character */
  
  if sapb.optvalue[1] > "0" and sapb.optvalue[1] ne "99" then do: 
   p-optiontype = " ".

   find notes where notes.cono = g-cono and
                    notes.notestype = "zz" and
                    notes.primarykey = "varxp" and  /* tbxr program */
                    notes.secondarykey = string(p-format) no-lock no-error.
   if not avail notes then do: 
    display "Format is not valid cannot process request".
    assign p-optiontype = "v"
           p-sorttype   = ">,"
           p-totaltype  = "l"
           p-summcounts = "a".
    return.
   end.               
   
   assign p-optiontype = notes.noteln[1]
          p-sorttype   = notes.noteln[2]
          p-totaltype  = notes.noteln[3]
          p-summcounts = notes.noteln[4]
          p-export     = "    "
          p-register   = notes.noteln[5]
          p-registerex = notes.noteln[6].
  end.     
  else
  if sapb.optvalue[1] = "99" then do: 
   assign p-register   = ""
          p-registerex = "".
   run reportopts(input sapb.user5, 
                  input-output p-optiontype,
                  input-output p-sorttype,  
                  input-output p-totaltype,
                  input-output p-summcounts,
                  input-output p-register,
                  input-output p-regval).
  end.

  run print_reportopts(input recid(sapb), 
                       input g-cono).

end. /* procedure */

procedure extract_data:

end. 

/* ------------------------------------------------------------------------- */
procedure print-dtl:
/* ------------------------------------------------------------------------- */
define input parameter zx-recidvaeh  as recid no-undo.
define input parameter zx-recidvaes  as recid no-undo.
define input parameter zx-recidvaesl as recid no-undo.

find vaeh  where recid(vaeh)  = zx-recidvaeh no-lock. 
find vaes  where recid(vaes)  = zx-recidvaes no-lock. 
find vaesl where recid(vaesl) = zx-recidvaesl no-lock. 

if not avail vaeh then leave. 
if not avail vaes then leave. 
if not avail vaesl then leave. 

find first poelo use-index k-order where
           poelo.cono = g-cono              and 
           poelo.ordertype   = "f"          and 
           poelo.orderaltno  = vaesl.vano   and 
           poelo.orderaltsuf = vaesl.vasuf  and 
           poelo.seqaltno    = vaesl.seqno  and 
           poelo.linealtno   = vaesl.lineno 
           no-lock no-error.        

assign v-dotype     = "no" 
       v-poelpono   = ""
       v-poellineno = ""
       v-custno     = "" 
       v-custnm     = ""
       v-vendno     = ""
       v-vendnm     = ""
 /*    v-exptshipdt = ""   */
       v-exptshipdt = ? 
       v-ackncd     = ""
       v-ackndt     = ""
       v-all        = ""
       v-allline    = "".

if avail poelo and poelo.ordertype = "f" and 
   poelo.seqaltno > 0 then do:    
 for each poel use-index k-poel where
            poel.cono   = g-cono      and               
            poel.pono   = poelo.pono  and 
            poel.posuf  = poelo.posuf and 
            poel.lineno = poelo.lineno
            no-lock: 

 if avail poel then do: 
 /*  display poel.pono "2". */
  assign v-ackncd = "" 
         v-ackndt = "". 
  assign v-ackndt = if avail poel and poel.user9 ne ? then 
                     string(poel.user9)
                    else 
                     "". 
  assign v-ackncd = if avail poel and substring(poel.user1,1,1) ne "" then 
                     substring(poel.user1,1,1) 
                    else 
                     "" 
         v-dotype = if avail poel and poel.transtype = "do" then 
                     "yes" 
                    else 
                     "no"
/*       v-exptshipdt = if avail poel and poel.expshipdt ne ? then 
                         string(poel.expshipdt)
                        else 
                         ""    */
         v-exptshipdt = if avail poel and poel.expshipdt ne ? then 
                         poel.expshipdt
                        else 
                        ?  /*  ""   */
         v-all        = if avail poel then 
                         string(poel.pono) + "-" + string(poel.posuf,"99")
                        else 
                         "" 
         v-allline    = if avail poel then 
                         string(poel.lineno)
                        else 
                         "".
                         
  end. 
 end. /* avail poelo */
end. 


  find first wtelo use-index k-order where
              wtelo.wtcono = g-cono              and 
              wtelo.ordertype   = "f"          and 
              wtelo.orderaltno  = vaesl.vano   and 
              wtelo.orderaltsuf = vaesl.vasuf  and 
              wtelo.seqaltno    = vaesl.seqno  and 
              wtelo.linealtno   = vaesl.lineno 
              no-lock no-error.          

      assign v-wtelwtno = ""
             v-wtellineno = "".
             /* v-all = ""
             v-allline = "".  */
             
   if avail wtelo and wtelo.ordertype = "f" and 
      wtelo.seqaltno > 0 then do:  
        
        
    for each wteh use-index k-wteh where 
             wteh.cono = g-cono and 
             wteh.wtno = wtelo.wtno and
             wteh.wtsuf = wtelo.wtsuf
             no-lock:

    for each wtel use-index k-wtel where
               wtel.cono   = g-cono       and               
               wtel.wtno   = wteh.wtno   and 
               wtel.wtsuf  = wteh.wtsuf  and 
               wtel.lineno = wtelo.lineno
               no-lock:  
      /*  display wtel.wtno wtel.lineno.  */
               
        if avail wtel then do:
            assign  v-all = string(wtel.wtno)
                    v-allline = string(wtel.lineno)
                    v-exptshipdt = wteh.reqshipdt.  
        end.                                         
    end.
 end.
end. 


     find first v-vaelo use-index k-order where v-vaelo.cono = g-cono and 
                       v-vaelo.ordertype   = "f"          and 
                       v-vaelo.orderaltno  = vaesl.vano   and 
                       v-vaelo.orderaltsuf = vaesl.vasuf  and 
                       v-vaelo.seqaltno    = vaesl.seqno  and                                           v-vaelo.linealtno   = vaesl.lineno 
                       no-lock no-error.          
              
              assign v-vaelovano = "". 
                  /* v-all       = ""
                     v-allline   = "".  */
                     
       if avail v-vaelo and v-vaelo.ordertype = "f" and 
                v-vaelo.seqaltno > 0 then do:    
          /*  display "got vaelo 2". pause. */

         for each v-vaeh use-index k-vaeh where 
                  v-vaeh.cono = v-vaelo.cono and
                  v-vaeh.vano = v-vaelo.vano and
                  v-vaeh.vasuf = v-vaelo.vasuf
                  no-lock:
            
           for each v-vaes use-index k-section where  
                 v-vaes.cono      = v-vaelo.cono and
            /*   v-vaes.completefl = no        and   
                 v-vaes.sctntype  = "in"       and */  
                 v-vaes.vano      = vaeh.vano and 
                 v-vaes.vasuf     = vaeh.vasuf /*and
                 v-vaes.stagecd   >= 1   and /* 1=open 2=print 3=ship */  
                 v-vaes.stagecd   <= 3  /*  and  */ 
                 v-vaes.promisedt >= 01/01/08 and
                 v-vaes.promisedt <= 03/01/09        */
                 no-lock:
           
              if avail v-vaelo then do:
                 assign  /* v-vaelovano = string(vaelo.vano) */
                         v-all        = string(v-vaelo.vano)
                         v-allline    = ""
                         v-exptshipdt = v-vaeh.reqshipdt.
              end.
 
         /*

            for each v-vaesl use-index k-vaesl where
                     v-vaesl.cono   = g-cono       and               
                     v-vaesl.vano   = v-vaes.vano   and 
                     v-vaesl.vasuf  = v-vaes.vasuf  and 
                     v-vaesl.seqno  = v-vaes.seqno
                     no-lock:  

             /*   display v-vaesl.vano v-vaesl.seqno. pause.  */
             */
              end.      
           end.   
         end.  

/*   message vaeh.vano vaes.promisedt oNonTieLate oFutureLate  
   v-exptshipdt.     
*/

/*  here  */

if oFutureLate then do:

/*   message "got ofuture".  */
   if vaes.promisedt < today then     
     next.
   if v-exptshipdt <= vaes.promisedt then do:     
      next.
/*    message "oFutureLate".   */
   end.    
end.  /* oFutureLate */   
else do:        

/*   message " else do".   */
   if (oPromDtLess and vaes.promisedt >= today)   and
       not oNonTieLate and not oFutureLate  
       then do:
/*     message "1".          */
    next.
   end. 

  /* added this  */
  if oPromDtLess and vaes.promisedt >= today  then do:
     next.
 /*  message "this one".   */
  end.   
  
  
   if (not oPromDtLess and v-exptshipdt >= today) then do: 
   /*  message "2".   */    
       next.
   end.
  
   
   /* Option2 = no only - not late checks  */
   if avail poel and (not oPromDtLess and v-exptshipdt >= today)
    and not oFutureLate and not oNonTieLate then do:
 /* message "3".  */            
    next. 
   end. 

   /* Option #2 and Option #5 must have a poel record */
   if not avail poelo and not oNonTieLate then do: 
    if not avail wtelo and not oNonTieLate then do:
/*     if not avail vaelo and not oNonTieLate then do:  */
  /* message "4".   */ 
      next. 
    end.  
   end. 
/*  end. */
  
  
   /* Option4 = yes - only display non ties */
   if (oNonTieLate and vaes.promisedt > today and
       vaesl.orderalttype = " ") then do:   /* used to be = */
   /*  message "5".      */
       next. 
   end.

/* 
   if (oNonTieLate and vaes.promisedt < today and
       vaesl.orderalttype ne " ") then do:              
   /*  message "6".    */  
       next. 
   end.   
*/   


   /* Option5 = yes */
   if avail poel and (oFutureLate and v-exptshipdt <= today) then do: 
   /* message "7".  */ 
      next.
    end.

/*  here  */ 

 end.  /* oFutureLate */

/*
assign v-vendno = ""
       v-vendnm = "".   */
find first apsv where apsv.cono = g-cono and 
           apsv.vendno = vaesl.arpvendno 
           no-lock no-error. 
if avail apsv then 
 assign v-vendnm = apsv.name
        v-vendno = string(vaesl.arpvendno).
/* assign v-custno = "".  */
find first vaelo where 
           vaelo.cono      = g-cono and 
           vaelo.ordertype = "o"    and  
           vaelo.vano      = vaeh.vano 
           no-lock no-error. 
if avail vaelo and vaelo.ordertype = "o" then do:            
 find oeeh where 
      oeeh.cono     = g-cono           and 
      oeeh.orderno  = vaelo.orderaltno and 
      oeeh.ordersuf = vaelo.orderaltsuf
      no-lock no-error.  
 if avail oeeh then do: 
  find first arsc where arsc.cono = g-cono and
             arsc.custno = oeeh.custno 
     no-lock no-error.
  assign v-custno = string(oeeh.custno)
         v-custnm = if avail arsc then 
                     arsc.name 
                    else 
                     " ". 
 end. /* avail oeeh */
end. /* avail vaelo */

/* detail line 1 */
display  vaeh.takenby    
         vaeh.whse       
         v-custno        
         vaeh.vano       
         vaeh.vasuf      
         vaes.seqno      
         vaesl.lineno    
         vaesl.shipprod        
         vaes.promisedt  
         v-dotype   
/*
         v-poelpono      
         v-poellineno     
*/         
         v-ackncd
         v-exptshipdt
/*
         v-wtelwtno          
         v-wtellineno        
         v-vaelovano
*/         
         v-all
         v-allline
         with frame f-detail-1.  
    down with frame f-detail-1.

/* detail line 2 */ 
  display v-custnm v-vendnm v-ackndt with frame f-detail-2. 
                                          down with frame f-detail-2.

end. /* procedure print-dtl */ 

/* ------------------------------------------------------------------------- */
procedure print-dtl-export:
/* ------------------------------------------------------------------------- */
define input parameter zx-recidvaeh  as recid no-undo.
define input parameter zx-recidvaes  as recid no-undo.
define input parameter zx-recidvaesl as recid no-undo.

find vaeh  where recid(vaeh)  = zx-recidvaeh no-lock. 
find vaes  where recid(vaes)  = zx-recidvaes no-lock. 
find vaesl where recid(vaesl) = zx-recidvaesl no-lock. 

if not avail vaeh then leave. 
if not avail vaes then leave. 
if not avail vaesl then leave. 

find first poelo where
           poelo.cono = g-cono              and 
           poelo.ordertype   = "f"          and 
           poelo.orderaltno  = vaesl.vano   and 
           poelo.orderaltsuf = vaesl.vasuf  and 
           poelo.seqaltno    = vaesl.seqno  and 
           poelo.linealtno   = vaesl.lineno 
           no-lock no-error.        

assign v-dotype     = "no" 
       v-poelpono   = ""
       v-poellineno = ""
       v-custno     = "" 
       v-custnm     = ""
       v-vendno     = ""
       v-vendnm     = ""
       v-exptshipdt = ?
       v-ackncd     = ""
       v-ackndt     = "".

if avail poelo and poelo.ordertype = "f" and 
   poelo.seqaltno > 0 then do:    
 find first poel where
            poel.cono   = g-cono      and               
            poel.pono   = poelo.pono  and 
            poel.posuf  = poelo.posuf and 
            poel.lineno = poelo.lineno
            no-lock no-error. 

 if avail poel then do:  
  assign v-ackncd = "" 
         v-ackndt = "". 
  assign v-ackndt = if avail poel and poel.user9 ne ? then 
                     string(poel.user9)
                    else 
                     "". 
  assign v-ackncd = if avail poel and substring(poel.user1,1,1) ne "" then 
                     substring(poel.user1,1,1) 
                    else 
                     "" 
         v-dotype = if avail poel and poel.transtype = "do" then 
                     "yes" 
                    else 
                     "no"
         v-exptshipdt = if avail poel and poel.expshipdt ne ? then 
                         poel.expshipdt
                        else 
                         ?  /* ""   */
         v-poelpono   = if avail poel then 
                         string(poel.pono) + "-" + string(poel.posuf,"99")
                        else 
                         "" 
         v-poellineno = if avail poel then 
                         string(poel.lineno)
                        else 
                         "".
 end. 
end. /* avail poelo */


   find first wtelo use-index k-order where
              wtelo.wtcono = g-cono              and 
              wtelo.ordertype   = "f"          and 
              wtelo.orderaltno  = vaesl.vano   and 
              wtelo.orderaltsuf = vaesl.vasuf  and 
              wtelo.seqaltno    = vaesl.seqno  and 
              wtelo.linealtno   = vaesl.lineno 
              no-lock no-error.          

      assign v-wtelwtno = ""
             v-wtellineno = "".
             /* v-all = ""
             v-allline = "".  */
             
   if avail wtelo and wtelo.ordertype = "f" and 
      wtelo.seqaltno > 0 then do:  
        
        
    for each wteh use-index k-wteh where 
             wteh.cono = g-cono and 
             wteh.wtno = wtelo.wtno and
             wteh.wtsuf = wtelo.wtsuf
             no-lock:

    for each wtel use-index k-wtel where
               wtel.cono   = g-cono       and               
               wtel.wtno   = wteh.wtno   and 
               wtel.wtsuf  = wteh.wtsuf  and 
               wtel.lineno = wtelo.lineno
               no-lock:  
      /*  display wtel.wtno wtel.lineno.  */
               
        if avail wtel then do:
            assign  v-all = string(wtel.wtno)
                    v-allline = string(wtel.lineno)
                    v-exptshipdt = wteh.reqshipdt.  
        end.                                         
    end.
 end.
end. 


     find first v-vaelo use-index k-order where v-vaelo.cono = g-cono and 
                       v-vaelo.ordertype   = "f"          and 
                       v-vaelo.orderaltno  = vaesl.vano   and 
                       v-vaelo.orderaltsuf = vaesl.vasuf  and 
                       v-vaelo.seqaltno    = vaesl.seqno  and                                           v-vaelo.linealtno   = vaesl.lineno 
                       no-lock no-error.          
              
              assign v-vaelovano = "". 
                  /* v-all       = ""
                     v-allline   = "".  */
                     
       if avail v-vaelo and v-vaelo.ordertype = "f" and 
                v-vaelo.seqaltno > 0 then do:    
          /*  display "got vaelo 2". pause. */

         for each v-vaeh use-index k-vaeh where 
                  v-vaeh.cono = v-vaelo.cono and
                  v-vaeh.vano = v-vaelo.vano and
                  v-vaeh.vasuf = v-vaelo.vasuf
                  no-lock:
            
           for each v-vaes use-index k-section where  
                 v-vaes.cono      = vaelo.cono and
            /*   v-vaes.completefl = no        and   
                 v-vaes.sctntype  = "in"       and */  
                 v-vaes.vano      = vaeh.vano and 
                 v-vaes.vasuf     = vaeh.vasuf /*and
                 v-vaes.stagecd   >= 1   and /* 1=open 2=print 3=ship */  
                 v-vaes.stagecd   <= 3  /*  and  */ 
                 v-vaes.promisedt >= 01/01/08 and
                 v-vaes.promisedt <= 03/01/09        */
                 no-lock:
           
              if avail v-vaelo then do:
                 assign  /* v-vaelovano = string(v-vaelo.vano) */
                         v-all        = string(v-vaelo.vano)
                         v-allline    = ""
                         v-exptshipdt = v-vaeh.reqshipdt.
              end.
 
         /*

            for each v-vaesl use-index k-vaesl where
                     v-vaesl.cono   = g-cono       and               
                     v-vaesl.vano   = v-vaes.vano   and 
                     v-vaesl.vasuf  = v-vaes.vasuf  and 
                     v-vaesl.seqno  = v-vaes.seqno
                     no-lock:  

             /*   display v-vaesl.vano v-vaesl.seqno. pause.  */
             */
              end.      
           end.   
         end.  

/* here */

if oFutureLate then do:

/*   message "got ofuture".  */
   if vaes.promisedt < today then     
     next.
   if v-exptshipdt <= vaes.promisedt then do:     
      next.
/*    message "oFutureLate".   */
   end.    
end.  /* oFutureLate */   
else do:        

/*   message " else do".   */
   if (oPromDtLess and vaes.promisedt >= today)   and
       not oNonTieLate and not oFutureLate  
       then do:
/*     message "1".          */
    next.
   end. 

  /* added this  */
  if oPromDtLess and vaes.promisedt >= today  then do:
     next.
 /*  message "this one".   */
  end.   
  
  
   if (not oPromDtLess and v-exptshipdt >= today) then do: 
   /*  message "2".   */    
       next.
   end.
  
   
   /* Option2 = no only - not late checks  */
   if avail poel and (not oPromDtLess and v-exptshipdt >= today)
    and not oFutureLate and not oNonTieLate then do:
 /* message "3".  */            
    next. 
   end. 

   /* Option #2 and Option #5 must have a poel record */
   if not avail poelo and not oNonTieLate then do: 
    if not avail wtelo and not oNonTieLate then do:
     if not avail vaelo and not oNonTieLate then do:
  /* message "4".   */ 
      next. 
    end.  
   end. 
  end.
  
  
   /* Option4 = yes - only display non ties */
   if (oNonTieLate and vaes.promisedt > today and
       vaesl.orderalttype = " ") then do:   /* used to be = */
   /*  message "5".      */
       next. 
   end.

/* 
   if (oNonTieLate and vaes.promisedt < today and
       vaesl.orderalttype ne " ") then do:              
   /*  message "6".    */  
       next. 
   end.   
*/   


   /* Option5 = yes */
   if avail poel and (oFutureLate and v-exptshipdt <= today) then do: 
   /* message "7".  */ 
      next.
    end.

 end.  /* oFutureLate */

/*
assign v-vendno = ""
       v-vendnm = "".   */
find first apsv where apsv.cono = g-cono and 
           apsv.vendno = vaesl.arpvendno 
           no-lock no-error. 
if avail apsv then 
 assign v-vendnm = apsv.name
        v-vendno = string(vaesl.arpvendno).
/* assign v-custno = "".   */
find first vaelo where 
           vaelo.cono      = g-cono and 
           vaelo.ordertype = "o"    and  
           vaelo.vano      = vaeh.vano 
           no-lock no-error. 
if avail vaelo and vaelo.ordertype = "o" then do:            
 find oeeh where 
      oeeh.cono     = g-cono           and 
      oeeh.orderno  = vaelo.orderaltno and 
      oeeh.ordersuf = vaelo.orderaltsuf
      no-lock no-error.  
 if avail oeeh then do: 
  find first arsc where arsc.cono = g-cono and
             arsc.custno = oeeh.custno 
     no-lock no-error.
  assign v-custno = string(oeeh.custno)
         v-custnm = if avail arsc then 
                     arsc.name 
                    else 
                     " ". 
 end. /* avail oeeh */
end. /* avail vaelo */


assign v-seqno = ""
       v-seqno = string(vaes.seqno,">>9.9").      



/* detail line 1 */
put stream expt unformatted  
 " "
/*  v-del */
 caps(vaeh.takenby)
 v-del
 caps(vaeh.whse)
 v-del 
 v-custnm
 v-del
 v-custno        
 v-del
 string(vaeh.vano) + "-" +      
 string(vaeh.vasuf,"99")      
 v-del
 """"
 v-seqno     
 """"
 v-del
 """"
 left-trim(string(vaesl.lineno))      format "x(3)"
 """"
 v-del
 vaesl.shipprod
 v-del
 v-vendnm
 v-del
 string(vaes.promisedt,"99/99/99")   
 v-del
 v-dotype   
 v-del
 v-poelpono      
 v-del
 """"
 left-trim(v-poellineno)              format "x(3)"
 """"
 v-del
 v-ackncd
 v-del
 v-ackndt
 v-del
 v-exptshipdt 
 chr(13) chr(10).     


/* one line for export 
/* detail line 2 */ 
put stream expt unformatted  
 " "
 v-del
 v-del 
 v-del
 v-custnm   
 v-del 
 v-del 
 v-del 
 v-del
 v-vendnm 
 v-del
 v-del
 v-del
 v-del
 v-del 
 v-del
 v-ackndt
 chr(13) chr(10).     
*/

end. /* procedure print-dtl-export */ 

/{&user_procedures}*/


/* TBXR F6 CodeGen Includes from here down are CodeGen Generated  */ 

/{&user_optiontype}*/
/{&user_optiontype}*/
   
/{&user_registertype}*/
/{&user_registertype}*/
 
/{&user_exportvarheaders1}*/
/{&user_exportvarheaders1}*/

/{&user_exportvarheaders2}*/
/{&user_exportvarheaders2}*/

/{&user_loadtokens}*/
/{&user_loadtokens}*/

/{&user_totaladd}*/
/{&user_totaladd}*/

/{&user_numbertotals}*/
/{&user_numbertotals}*/

/{&user_summaryloadprint}*/
/{&user_summaryloadprint}*/

/{&user_summaryloadexport}*/
/{&user_summaryloadexport}*/


/* TBXR F6 CodeGen End of Includes that are CodeGen Generated  */ 






