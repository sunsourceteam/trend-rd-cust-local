/* SX 55
   f-wtei.i 1.1 01/03/98 */
/* f-wtei.i 1.3 06/15/93 */
/*h*****************************************************************************
  INCLUDE      : f-wtei.i
  DESCRIPTION  : Form for wtei.p
  USED ONCE?   : Yes (wtei.p)
  AUTHOR       :
  DATE WRITTEN : 02/22/93
  CHANGES MADE :
    02/22/93 rhl; TB#  7729 Charge Quantity field in Extended screen
    06/15/93 mms; TB# 11820 Display entire unavail description
    03/25/96 kr;  TB3 12406 Variables defined in form, moved to calling program
    10/18/2007 dkt added 2 hooks &user_frame_vars, &user_fields_f-wteil
*******************************************************************************/

form
    g-wtno                               colon  6    label "WT #"
        {f-help.i}
    "-"                                  at    15
    g-wtsuf                              at    16 no-label
    wteh.notesfl                         at    18 no-label
    wteh.receiptdt                       colon 31    label "Received"
    "From: Co#/Whse  To: Co#/Whse  Type" at    43
    wteh.ignoreltfl                      colon 31    label "Ignore Lead"
    wteh.cono                            at    48 no-label
    wteh.shipfmwhse                      at    53 no-label
    wteh.cono2                           at    62 no-label
    wteh.shiptowhse                      at    67 no-label
    wteh.transtype                       at    74 no-label
with frame f-wtei row 1 width 80 side-labels overlay title g-title.

form
    poei.lineno                          at     2
    poei.nonstockty                      at     6
    poei.shipprod                        at     8
    poei.notesfl                         at    32
    icsp.descrip[1]                      at    34
    poei.qtyrcv                          at    59
    poei.unit                            at    70
    poei.serlottype                      at    76
    poei.speccostty                      at    77
    s-assign                             at    78
    {f-wtei.z99 &user_f-wteil1 = "*"}  

with frame f-wteil row 5 width 80 no-labels overlay 
{f-wtei.z99 &user_f-wteil2 = "*"}  
down no-hide title
" Ln#   Product                                             Quantity  Unit     "
.


form
    s-updatefl                           colon 43    label "Update"

    skip(1)

    s-backfl                             colon 43    label "Background"
        help "Run Backorder Processing in the Background"
    s-printernm                          colon 43    label
        "Printer For Allocation and Receipt Report"

    skip(1)

    s-pckprinter                         colon 43    label
        "Printer For Pick Tickets"
    s-routefl                            colon 43    label
        "Print Order: (R)oute or (O)rder"
with frame f-wteiu overlay row 5 centered side-labels title "Update".

form
    s-updatefl                           colon 43    label "Update"

    skip(1)

    s-backfl                             colon 43    label "Background"
        help "Run Backorder Processing in the Background"
    s-printernm                          colon 43    label
        "Printer For Allocation and Receipt Report"

    skip(1)
    s-printibfl                         colon 43     label
        "Print IBC Labels?"
    s-ibprinter                         colon 43     label
        "Printer for IBC Labels"
    s-prnlabelmode                      colon 43     label
        "Print Based on"
         validate ( can-do("Q,L",s-prnlabelmode), "Must Enter Q or L")
         help "L = Print One per line; Q = Print Based on Qty Received"
    skip(1)

    s-pckprinter                         colon 43    label
        "Printer For Pick Tickets"
    s-routefl                            colon 43    label
        "Print Order: (R)oute or (O)rder"
with frame f-wteiuib overlay row 5 centered side-labels title "Update".

form
    "Substitute Product"                 at     1
    s-subprod                            at     1
        {f-help.i}
with frame f-poeis overlay row 10 column 8 no-labels.

form
    s-nextno                             colon 15    label "Next Line #"

    skip(1)

    s-nextprod                           colon 15    label "Product Search"
with frame f-wtein overlay row 10 centered side-labels.

form
    s-desc                               at     2 no-label

    skip(1)

    wtel.qtyord                          colon 22    label "Ordered"
    wtel.unit                            at    36 no-label
    wtel.stkqtyord                       colon 22    label "Stocking Ordered"
    poei.stkqtyrcv format "zzzzzz9.99"   colon 22    label "Stocking Quantity"
    icsp.unitstock                       at    36 no-label

    skip(1)

    poei.qtyunavail format "zzzzzz9.99"  colon 22    label
        "Unavailable Quantity"
    s-unitstock                          at    36 no-label
    poei.reasunavty                      colon 22    label "Unavailable Reason"
    s-unavdesc                           at    27 no-label
    poei.qtyassign                       colon 22    label "Assigned to Orders"
    icsw.binloc1                         colon 22    label "Bin Loc"
    icsw.binloc2                         at    38 no-label
with frame f-wteie overlay row 7 centered side-labels title "Extend".

form
    s-fprintfl                                       label "Print to File?"
with frame x row 13 column 27 side-labels overlay.
