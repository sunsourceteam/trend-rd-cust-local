/* *****************************************************************************
  INCLUDE      : zsdixli.i
  DESCRIPTION  : Scrolling Line Object Code
  USED ONCE?   : no
  AUTHOR       :
  DATE WRITTEN :
*******************************************************************************/

    clear frame {&frame} all.
    v-keys = "".
    pause 0.
    input clear.
    do i = 1 to v-length:
        confirm = true.
        if i = 1 then do:
          &if "{&direction}" = "" &then
              {{&find} first}.
            do while not confirm and avail {&file}:
              {{&find} next "/*"}.
            end.
          &else
             if {&direction} then do:
                {{&find} first}.
             end.
             else do:
                 {{&find} last}.
             end.

             if {&direction} then
               do while not confirm and avail {&file}:
                 {{&find} next "/*"}.
             end.
             else do while not confirm and avail {&file}:
                {{&find} prev "/*"}.
             end.

          &endif

        end.  /* if i = 1 */
        else do:
            &if "{&direction}" = "" &then
              {{&find} next "/*"}.
              do while not confirm and avail {&file}:
                 {{&find} next "/*"}.
              end.
            &else
               if {&direction} then do:
                 {{&find} next "/*"}.
               end.
               else do:
                 {{&find} prev "/*"}.
               end.
               if {&direction} then
                 do while not confirm and avail {&file}:
                   {{&find} next "/*"}.
                 end.

               else do while not confirm and avail {&file}:
                 {{&find} prev "/*"}.
               end.

            &endif

        end.
        if not avail {&file} then do:
            run err.p(9013).
            leave.
        end.
        else do:
            {{&display}}.
            down with frame {&frame}.
            readkey pause 0.
            if {k-cancel.i} then leave.
        end.
    end.

    up (i - 1) with frame {&frame}.
    dsply:
    repeat with frame {&frame} on endkey undo main, next main:
        input clear.
        status default
        "Up/Down Arrow,F1 to Select".
        choose row {&field} no-error
            auto-return keys v-keys
            go-on(f12 f19 shift-f9 f20 shift-f10 {&go-on})
            with frame {&frame}.

        if not can-find(first b-t-sassm where 
                          b-t-sassm.screenno = v-keys no-lock) and
                          v-keys <> "" then do:
          message "invalid selection".
          assign v-keys = "".
          next dsply.
        end.  
          
        
        color display normal {&field} with frame {&frame}.





        hide message no-pause.
        if {k-func12.i} then do:
            run err.p(2003).
            next.
        end.
        if frame-value = "" and not ({k-func.i} or {k-func20.i}) then next.
        if frame-value ne o-frame-value then do:
            o-frame-value = frame-value.
            {{&where}}.
        end.
        /{&usecode}*/
        {{&usefile}}
        /{&usecode}*/
        if {k-func20.i} then do:
          {{&where}}.
          if substring(g-ourproc,1,2) = "tb"
            then run tbnoted.p(no,"","","").
          else if substring(g-ourproc,1,2) = "cm"
            then run cmnoted.p(no,"","","").
          else run noted.p(no,"","","").
            next.
        end.
        if {k-return.i} then do:
          if frame-line({&frame}) ne v-length then do:
            down with frame {&frame}.
            next.
          end.
        end.
        if {k-select.i} or ({k-func.i} and not g-lkupfl) then do:
          {&global}.
          hide message no-pause.
          if {k-select.i} then do:
            leave main.
          end.
          else do:
            {k-lkfunc.i}
            if v-fkey[i] = "" then do:
               bell.
               next.
            end.
            assign
              g-modulenm = substring(v-fkey[i],1,2)
              g-menusel  = substring(v-fkey[i],3)
              g-inqfl    = true.
              return.
            end.
        end.
        if {k-prevpg.i} or {k-scrlup.i} then do:
          up (frame-line({&frame}) - 1) with frame {&frame}.
          if frame-value = "" then do:
            run err.p(9014).
            next.
          end.
          if frame-value ne o-frame-value then do:
             o-frame-value = frame-value.
            {{&where}}.
          end.
          input clear.
          do i = 1 to v-length:
            &if "{&direction}" = "" &then
               {{&find} prev "/*"}
               do while not confirm and avail {&file}:
                 {{&find} prev "/*"}.
               end.
            &else
               if {&direction} then do:
                 {{&find} prev "/*"}.
               end.
               else do:
                 {{&find} next "/*"}.
               end.
               if {&direction} then
                 do while not confirm and avail {&file}:
                   {{&find} prev "/*"}.
                 end.
               else do while not confirm and avail {&file}:
                 {{&find} next "/*"}.
               end.
             &endif

             if not avail {&file} then do:
               run err.p(9013).
               if i ne 1 then do:
                 repeat j = i to v-length:
                   clear frame {&frame} no-pause.
                   if j ne v-length then up with frame {&frame}.
                 end.
                 if i ne 1 then down (frame-down({&frame}) - i + 1)
                                   with frame {&frame}.
             end.
             leave.
           end.
           else do:
             if i = 1 then down (v-length - 1) with frame {&frame}.
               {{&display}}
             if i ne v-length then up with frame {&frame}.
               readkey pause 0.
               if {k-cancel.i} then leave.
           end.
         end.
         if frame-line({&frame}) ne frame-down({&frame}) then
            down frame-down({&frame}) - frame-line({&frame})
               with frame {&frame}.
      end.
      else if {k-nextpg.i} or {k-scrldn.i} then do:
        down (frame-down({&frame}) - frame-line({&frame}))
                with frame {&frame}.
        if frame-value = "" then do:
          run err.p(9015).
          next.
        end.
        if frame-value ne o-frame-value then do:
                o-frame-value = frame-value.
          {{&where}}.
        end.
        input clear.
        do i = 1 to v-length:
          &if "{&direction}" = "" &then
             {{&find} next "/*"}
             do while not confirm and avail {&file}:
               {{&find} next "/*"}.
             end.
          &else                                                                               if {&direction} then do:
               {{&find} next "/*"}.
             end.
             else do:
               {{&find} prev "/*"}.
             end.

             if {&direction} then
               do while not confirm and avail {&file}:
                 {{&find} next "/*"}.
               end.
             else do while not confirm and avail {&file}:
               {{&find} prev "/*"}.
             end.
          &endif

          if not avail {&file} then do:
            run err.p(9013).
            if i ne 1 then do:
              repeat j = i to v-length:                                                          clear frame {&frame} no-pause.
                if j ne v-length then down with frame {&frame}.
              end.
              if i ne 1 then up (frame-down({&frame}) - i + 1)
                                with frame {&frame}.
            end.
            leave.
          end.
          else do:
            if i = 1 then up (v-length - 1) with frame {&frame}.
                    {{&display}}.
            if i ne v-length then down with frame {&frame}.
              readkey pause 0.
            if {k-cancel.i} then leave.
          end.
        end.
        if frame-line({&frame}) ne 1 then
                up frame-line({&frame}) - 1 with frame {&frame}.
      end.
    end.
    status default.




