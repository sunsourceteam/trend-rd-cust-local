find com where
     com.cono     = g-cono and
     com.comtype  = {1} and
     com.orderno  = {2} and
     com.ordersuf = {3} and
     com.lineno   = {4} and
     com.printfl  = {5}
     {6} no-error.

  /****
  Un-comment the following code, save and compile WTERRP.P.  
  After a successful compile, re-comment the code and save.
  
  if g-currproc = "wterr" then do:
     find wteral use-index k-wteral where wteral.cono     = g-cono
                                      and wteral.reportno = {2} 
                                      and wteral.lineno   = {4} 
                                      no-lock no-error.
     if avail wteral then do:
        find com where com.cono     = g-cono and
                       com.comtype  = "va" + string(wteral.seqaltno,"999") and
                       com.orderno  = wteral.orderaltno  and
                       com.ordersuf = wteral.orderaltsuf and
                       com.lineno   = wteral.linealtno   and
                       com.printfl  = {5}
                       {6} no-error.
        if not avail com then do:
           find com where com.cono     = g-cono and                             
                       com.comtype  = "kt" + string(wteral.seqaltno,"999") and
                       com.orderno  = wteral.orderaltno  and
                       com.ordersuf = wteral.orderaltsuf and
                       com.lineno   = wteral.linealtno   and
                       com.printfl  = {5}
                       {6} no-error.
           if not avail com then do:
              find com where com.cono     = g-cono             and
                             com.comtype  = "pu"               and
                             com.orderno  = wteral.orderaltno  and
                             com.ordersuf = wteral.orderaltsuf and
                             com.lineno   = wteral.linealtno   and
                             com.printfl  = {5}
                             {6} no-error.
           end.
        end.
     end.
  end.
  ***/
