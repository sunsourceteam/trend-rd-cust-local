if not avail oeehb then
  find oeehb where oeehb.cono = g-cono and
                   oeehb.batchnm = string(w-batchnm,">>>>>>>9") and 
                   oeehb.sourcepros = "Quote"
                   no-error.
for each kpsk where kpsk.cono = g-cono and
                    kpsk.prod = icsp.prod and
                    kpsk.comptype = "c"
                    no-lock:
   find icsw where icsw.cono = g-cono and
                   icsw.prod = kpsk.comprod and
                   icsw.whse = v-whse
                   no-lock no-error.
   if not avail icsw then do:
      find first icsw where icsw.cono = g-cono and
                            icsw.prod = kpsk.comprod and
                            icsw.statustype <> "x"
                            no-lock no-error.
   end.
   if avail icsw then do:
      assign xcount = 0.
      for each icsec where icsec.cono = g-cono and     
                           icsec.rectype = "c" and     
                           icsec.custno  = v-custno and
                           icsec.altprod = icsw.prod
                           no-lock:                    
         assign xcount  = xcount + 1                   
         v-xprod = substr(icsec.prod,1,22).
      end.                                             
      if xcount = 0 then                               
         assign v-xprod = "No Xrefs Exist".            
      if xcount > 1 then                               
         assign v-xprod = "Multiple Xrefs Exist".      
   end.      
   
   find oeelk where oeelk.cono = g-cono and
                    oeelk.ordertype = "b" and
                    oeelk.orderno = v-quoteno and
                    oeelk.ordersuf = 0 and
                    oeelk.lineno  = v-lineno and
                    oeelk.seqno   = kpsk.seqno
                    no-lock no-error.
   if not avail oeelk then do:
      create oeelk.
      assign oeelk.cono       = g-cono
             oeelk.ordertype  = "b"
             oeelk.orderno    = v-quoteno
             oeelk.ordersuf   = 0
             oeelk.lineno     = v-lineno
             oeelk.seqno      = kpsk.seqno
             oeelk.statustype = "a"
             oeelk.transtype  = "QU"

             oeelk.shipprod     = kpsk.comprod
             oeelk.qtyneeded    = kpsk.qtyneeded
             oeelk.qtyord       = kpsk.qtyneeded
             oeelk.unit         = kpsk.unit
             oeelk.custno       = oeehb.custno
             oeelk.whse         = /*if avail oeehb then oeehb.whse else*/ v-whse
             oeelk.arpwhse      = if avail icsw then icsw.whse else v-whse
             oeelk.pricetype    = if avail icsw then icsw.pricetype else " "
             oeelk.comptype     = kpsk.comptype
             oeelk.pricefl      = kpsk.pricefl
          /* oeelk.price        = v-sellprc    */
             oeelk.refer        = kpsk.refer
          /* oeelk.specnstype   = v-specnstype */
          /* oeelk.pdrecno      = zi-pdscrecno */
             oeelk.stkqtyord    = 0
             oeelk.stkqtyship   = 0
             oeelk.proddesc2    = v-xprod
             oeelk.prodcat      = if avail icsp then icsp.prodcat else " "
     overlay(oeelk.user4,1,4)   = v-whse.
        
      {t-all.i oeelk}
   
      create t-lines.
      assign t-lines.lineno     =  oeelk.lineno
             t-lines.seqno      =  oeelk.seqno
             t-lines.prod       =  oeelk.shipprod
             t-lines.descrip    =  oeelk.refer
             t-lines.xprod      =  substr(oeelk.proddesc2,1,22)
             t-lines.whse       =  oeelk.whse
             t-lines.listprc    =  0
             t-lines.sellprc    =  oeelk.price
             t-lines.gp         =  0
             t-lines.discpct    =  0
             t-lines.kqty       =  oeelk.qtyord
             t-lines.shipdt     =  ?
             t-lines.kitfl      =  no
             t-lines.specnstype = oeelk.specnstype
             t-lines.pdscrecno  = oeelk.pdrecno
             t-lines.prodcat    = oeelk.prodcat.

   end. /* not avail oeelk */
end. /* each kpsk */
