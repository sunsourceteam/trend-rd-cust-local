define input parameter z-update as logical.
define input parameter z-cono like icsd.cono. 
define input parameter z-begdate as date.
define input parameter z-enddate as date.
define input parameter z-whse as c format "x(4)".
define input parameter z-prod as c format "x(24)".
define output parameter z-usage as de.
define output parameter z-usageval as de.

define var x-prod as character format "x(24)" no-undo.
define var xenddate as date no-undo.
define var xbegdate as date no-undo.
define var xinx as integer no-undo.
/*
define var begindate as date init 04/30/2001 no-undo.
define var enddate as date init 05/01/2000 no-undo.
*/
assign z-usage = 0
       z-usageval = 0.

find first icsec where icsec.cono = z-cono and icsec.rectype = "p" and
                       icsec.altprod = z-prod no-lock no-error.
if avail icsec then
  do:
  x-prod = icsec.prod.
  run issueaquire.
  run get_issues.
  x-prod = z-prod.
  run issueaquire.
  end.
else
  do:
  x-prod = z-prod.
  run issueaquire.
  end.

run get_issues.



procedure issueaquire:



find zsastz where zsastz.cono = z-cono and
                  zsastz.codeiden = "USAGE" and
                  zsastz.primarykey = x-prod + "~011" + z-whse and
                  zsastz.secondarykey = string(year(today),"9999")
                  exclusive-lock no-error.
if not avail zsastz then
  find first zsastz where zsastz.cono = z-cono and
                    zsastz.codeiden = "USAGE" and
                    zsastz.primarykey = x-prod + "~011" + z-whse and
                    zsastz.secondarykey le string(year(today) - 1,"9999")
                    exclusive-lock no-error.
if avail zsastz then 
  do:
  assign xenddate = date(entry(1,zsastz.codeval[13],"~011")).
  if xenddate = 01/01/1960 then
    assign xenddate = z-enddate - 1. /* because the < was changed to le */
  end.
else
   assign xenddate = z-enddate - 1. /* because the < was changed to le */

/* Never process to the current dat till it's over.  */
if z-begdate = today then
   assign xbegdate = z-begdate - 1. 
else
   assign xbegdate = z-begdate. 
   

icetloop:

for each icet use-index k-postdt
  where icet.cono = z-cono and icet.whse = z-whse
                      and icet.prod = x-prod 
                      and can-do("ri,in",icet.transtype)  no-lock:
  
  if icet.postdt le xenddate then
    leave icetloop.
  
  if icet.postdt > xbegdate then
    next icetloop.
    
  if icet.transtype = "ri" and not can-do ("oe",icet.module) then
    next icetloop.
   

  
   
  if icet.module = "wt" then
    do:
    find wtel where wtel.cono = z-cono and wtel.wtno = icet.orderno and
                    wtel.wtsuf = icet.ordersuf and
                    wtel.lineno = icet.lineno no-lock no-error.
    if not avail wtel then
      do:
      message "wt" icet.prod icet.whse icet.orderno icet.ordersuf
              icet.lineno icet.seqno.
      pause.
      end.
    else
    if wtel.transtype <> "do" then
       do:
       next icetloop.
       end.
   if z-update then
     run create_usage2
        (input icet.postdt,
         input int(icet.stkqtyship),
         input dec(icet.stkqtyship * icet.cost),
         input icet.prod,
         input icet.whse ).
       
   if not z-update then
     assign z-usage = z-usage + icet.stkqtyship  
            z-usageval = z-usageval + 
                         (icet.stkqtyship * icet.cost).  

   end. 
  
  
  
  if icet.module = "oe" then
    do:
    find oeel where oeel.cono = z-cono and oeel.orderno = icet.orderno and
                    oeel.ordersuf = icet.ordersuf and
                    oeel.lineno = icet.lineno no-lock no-error.
    if not avail oeel then
      do:
      message "oe" icet.prod icet.whse icet.orderno icet.ordersuf
              icet.lineno icet.seqno.
      pause.
      end.
    else
    if oeel.botype = "d" or oeel.transtype = "do" or
       (oeel.specnstype = "n" and oeel.kitfl = false) or 
       (icet.transtype = "ri" and oeel.transtype <> "rm") then
       do:
        next icetloop.
        /*
        message "oe" icet.prod icet.whse icet.orderno icet.ordersuf
               icet.lineno icet.seqno.
        pause.
        */
       end.
    if icet.seqno <> 0 then
      do:
      find oeelk where oeelk.cono = z-cono and 
                  oeelk.ordertype = (if icet.module = "oe" then
                                      "o" 
                                     else 
                                      "w") and
                  oeelk.orderno = icet.orderno and
                  oeelk.ordersuf = icet.ordersuf and
                  oeelk.lineno = icet.lineno and
                  oeelk.seqno = icet.seqno no-lock no-error.

      if not avail oeelk then
        do:
        message "oe" icet.prod icet.whse icet.orderno icet.ordersuf
                icet.lineno icet.seqno.
        pause.
        next icetloop.
        end.
       if avail oeelk and oeelk.specnstype = "n" then
        next icetloop.
      end.    
    else
    if (oeel.specnstype = "n" and oeel.kitfl = true) then
      next icetloop.

      
   if z-update then   
     run create_usage2
      (input icet.postdt,
       input int(icet.stkqtyship  
                 * (if icet.transtype = "ri" and oeel.transtype = "rm" then
                       -1
                    else
                       1)),
       input dec((icet.stkqtyship 
                 * (if icet.transtype = "ri" and oeel.transtype = "rm" then
                      -1
                    else
                    1))
             * icet.cost),
       input icet.prod,
       input icet.whse ).
       

     if not z-update then 
      assign z-usage = z-usage + (icet.stkqtyship
          * (if icet.transtype = "ri" and oeel.transtype = "rm" then
               -1
            else
               1))
               
            z-usageval = z-usageval + ((icet.stkqtyship
          * (if icet.transtype = "ri" and oeel.transtype = "rm" then
               -1
            else
               1)) * icet.cost).
               

    
    
    end.
  else
  if icet.module = "kp" then
    do:
    if icet.transtype <> "in" then
      next icetloop.
    
    find first oeelk where oeelk.cono = z-cono and 
                oeelk.ordertype = (if icet.module = "oe" then
                                    "o" 
                                   else 
                                    "w") and
                oeelk.orderno = icet.orderno and
                oeelk.ordersuf = icet.ordersuf and
                oeelk.shipprod = icet.prod
                no-lock no-error.
    if avail oeelk and oeelk.specnstype = "n" then
      next icetloop.


    if z-update then
      run create_usage2
        (input icet.postdt,
         input int(icet.stkqtyship),
         input dec(icet.stkqtyship * icet.cost),
         input icet.prod,
         input icet.whse ).
 
    
    if not z-update then
      assign z-usage = z-usage + icet.stkqtyship  
             z-usageval = z-usageval + 
                       (icet.stkqtyship * icet.cost).  

    end.   
  else
  if icet.seqno <> 0 and icet.module = "va" then
    do:
    if icet.transtype <> "in" then
      next icetloop.

    find vaesl where vaesl.cono = z-cono and 
                     vaesl.vano = icet.orderno and
                     vaesl.vasuf = icet.ordersuf and
                     vaesl.seqno = icet.seqno and
                     vaesl.lineno = icet.lineno no-lock no-error.
    if vaesl.nonstockty = "n" then
      next icetloop.
    
    if not avail vaesl then
      do:   
      message "va" icet.prod icet.orderno icet.ordersuf icet.lineno icet.seqno.
      pause.
      end.   

    if z-update then
      run create_usage2
        (input icet.postdt,
         input int(icet.stkqtyship),
         input dec(icet.stkqtyship * icet.cost),
         input icet.prod,
         input icet.whse ).
    if not z-update then
      assign z-usage = z-usage + icet.stkqtyship  
             z-usageval = z-usageval + 
                       (icet.stkqtyship * icet.cost).  
 
   
   end. 
end.
end.



procedure issuefixback:

assign xenddate = date (month(z-enddate),1,year(z-enddate)).
if z-enddate = xenddate then
  return.
if z-enddate = today then
   assign xbegdate = z-enddate - 1. 
else
   assign xbegdate = z-enddate. 
   

find zsastz where zsastz.cono = z-cono and
                  zsastz.codeiden = "USAGE" and
                  zsastz.primarykey = x-prod + "~011" + z-whse and
                  zsastz.secondarykey = string(year(xenddate),"9999")
                  exclusive-lock no-error.
if not avail zsastz then
  return.   
else
if avail zsastz and 
   date(entry(2,zsastz.codeval[13],"~011")) < xenddate  then
   return.

 


icetloop:

for each icet use-index k-postdt
  where icet.cono = z-cono and icet.whse = z-whse
                      and icet.prod = x-prod 
                      and icet.postdt < xbegdate
                      and can-do("ri,in",icet.transtype)  no-lock:
  if icet.postdt < xenddate then  /* adjusted date to the end of the usage */
    leave icetloop.
  
  if icet.postdt > xbegdate then
    next.

  if icet.transtype = "ri" and not can-do ("oe",icet.module) then
    next icetloop.
   

  
   
  if icet.module = "wt" then
    do:
    find wtel where wtel.cono = z-cono and wtel.wtno = icet.orderno and
                    wtel.wtsuf = icet.ordersuf and
                    wtel.lineno = icet.lineno no-lock no-error.
    if not avail wtel then
      do:
      message "wt" icet.prod icet.whse icet.orderno icet.ordersuf
              icet.lineno icet.seqno.
      pause.
      end.
    else
    if wtel.transtype <> "do" then
       do:
       next icetloop.
       end.
   assign z-usage = z-usage + (icet.stkqtyship * -1) 
            z-usageval = z-usageval + 
                         ((icet.stkqtyship * icet.cost) * -1).  

   end. 
  
  
  
  if icet.module = "oe" then
    do:
    find oeel where oeel.cono = z-cono and oeel.orderno = icet.orderno and
                    oeel.ordersuf = icet.ordersuf and
                    oeel.lineno = icet.lineno no-lock no-error.
    if not avail oeel then
      do:
      message "oe" icet.prod icet.whse icet.orderno icet.ordersuf
              icet.lineno icet.seqno.
      pause.
      end.
    else
    if oeel.botype = "d" or oeel.transtype = "do" or
       (oeel.specnstype = "n" and oeel.kitfl = false) or 
       (icet.transtype = "ri" and oeel.transtype <> "rm") then
       do:
        next icetloop.
        /*
        message "oe" icet.prod icet.whse icet.orderno icet.ordersuf
               icet.lineno icet.seqno.
        pause.
        */
       end.
    if icet.seqno <> 0 then
      do:
      find oeelk where oeelk.cono = z-cono and 
                  oeelk.ordertype = (if icet.module = "oe" then
                                      "o" 
                                     else 
                                      "w") and
                  oeelk.orderno = icet.orderno and
                  oeelk.ordersuf = icet.ordersuf and
                  oeelk.lineno = icet.lineno and
                  oeelk.seqno = icet.seqno no-lock no-error.

      if not avail oeelk then
        do:
        message "oe" icet.prod icet.whse icet.orderno icet.ordersuf
                icet.lineno icet.seqno.
        pause.
        next icetloop.
        end.
       if avail oeelk and oeelk.specnstype = "n" then
        next icetloop.
      end.    
    else
    if (oeel.specnstype = "n" and oeel.kitfl = true) then
      next icetloop.

    assign z-usage = z-usage + (icet.stkqtyship
          * (if icet.transtype = "ri" and oeel.transtype = "rm" then
               1
            else
               -1))
               
            z-usageval = z-usageval + ((icet.stkqtyship
          * (if icet.transtype = "ri" and oeel.transtype = "rm" then
               1
            else
              -1)) * icet.cost).
               

    
    
    end.
  else
  if icet.module = "kp" then
    do:
    if icet.transtype <> "in" then
      next icetloop.
    
    find first oeelk where oeelk.cono = z-cono and 
                oeelk.ordertype = (if icet.module = "oe" then
                                    "o" 
                                   else 
                                    "w") and
                oeelk.orderno = icet.orderno and
                oeelk.ordersuf = icet.ordersuf and
                oeelk.shipprod = icet.prod
                no-lock no-error.
    if avail oeelk and oeelk.specnstype = "n" then
      next icetloop.

    assign z-usage = z-usage + (icet.stkqtyship * -1)
             z-usageval = z-usageval + 
                       ((icet.stkqtyship * icet.cost) * -1).  

    end.   
  else
  if icet.seqno <> 0 and icet.module = "va" then
    do:
    if icet.transtype <> "in" then
      next icetloop.

    find vaesl where vaesl.cono = z-cono and 
                     vaesl.vano = icet.orderno and
                     vaesl.vasuf = icet.ordersuf and
                     vaesl.seqno = icet.seqno and
                     vaesl.lineno = icet.lineno no-lock no-error.
    if vaesl.nonstockty = "n" then
      next icetloop.
    
    if not avail vaesl then
      do:   
      message "va" icet.prod icet.orderno icet.ordersuf icet.lineno icet.seqno.
      pause.
      end.   
    assign z-usage = z-usage + (icet.stkqtyship * -1)
             z-usageval = z-usageval + 
                       ((icet.stkqtyship * icet.cost) * -1).  
 
   
   end. 
end.
end.


procedure issuefixforward:

find zsastz where zsastz.cono = z-cono and
                  zsastz.codeiden = "USAGE" and
                  zsastz.primarykey = z-prod + "~011" + z-whse and
                  zsastz.secondarykey = string(year(today),"9999")
                  exclusive-lock no-error.
if not avail zsastz then
  find first zsastz where zsastz.cono = z-cono and
                          zsastz.codeiden = "USAGE" and
                          zsastz.primarykey = z-prod + "~011" + z-whse and
                          zsastz.secondarykey le string(year(today) - 1,"9999")
                          no-lock no-error.
if avail zsastz then
  assign xbegdate = date(entry(2,zsastz.codeval[13],"~011")).
else
  return.


if xbegdate  < z-begdate then
  return.

xenddate = date (month(z-begdate) + 1,1,
                 (if month(z-begdate) = 12 then
                    year(z-begdate) + 1
                 else
                    year(z-begdate))).
xenddate = xenddate - 1.
if xenddate > today then 
   xenddate = today - 1.
if z-enddate = today then
   assign xbegdate = z-begdate - 1. 
else
   assign xbegdate = z-begdate. 
   
 
icetloop:
for each icet use-index k-postdt
  where icet.cono = z-cono and icet.whse = z-whse
                      and icet.prod = x-prod 
                      and icet.postdt > xbegdate
                      and can-do("ri,in",icet.transtype)  no-lock:
  if icet.postdt le xenddate then  /* adjusted date to the end of the usage */
    leave icetloop.
  
  if icet.postdt > xbegdate then
    next.
    
 
  if icet.transtype = "ri" and not can-do ("oe",icet.module) then
    next icetloop.
   

  
   
  if icet.module = "wt" then
    do:
    find wtel where wtel.cono = z-cono and wtel.wtno = icet.orderno and
                    wtel.wtsuf = icet.ordersuf and
                    wtel.lineno = icet.lineno no-lock no-error.
    if not avail wtel then
      do:
      message "wt" icet.prod icet.whse icet.orderno icet.ordersuf
              icet.lineno icet.seqno.
      pause.
      end.
    else
    if wtel.transtype <> "do" then
       do:
       next icetloop.
       end.
   assign z-usage = z-usage + (icet.stkqtyship * -1) 
            z-usageval = z-usageval + 
                         ((icet.stkqtyship * icet.cost) * -1).  

   end. 
  
  
  
  if icet.module = "oe" then
    do:
    find oeel where oeel.cono = z-cono and oeel.orderno = icet.orderno and
                    oeel.ordersuf = icet.ordersuf and
                    oeel.lineno = icet.lineno no-lock no-error.
    if not avail oeel then
      do:
      message "oe" icet.prod icet.whse icet.orderno icet.ordersuf
              icet.lineno icet.seqno.
      pause.
      end.
    else
    if oeel.botype = "d" or oeel.transtype = "do" or
       (oeel.specnstype = "n" and oeel.kitfl = false) or 
       (icet.transtype = "ri" and oeel.transtype <> "rm") then
       do:
        next icetloop.
        /*
        message "oe" icet.prod icet.whse icet.orderno icet.ordersuf
               icet.lineno icet.seqno.
        pause.
        */
       end.
    if icet.seqno <> 0 then
      do:
      find oeelk where oeelk.cono = z-cono and 
                  oeelk.ordertype = (if icet.module = "oe" then
                                      "o" 
                                     else 
                                      "w") and
                  oeelk.orderno = icet.orderno and
                  oeelk.ordersuf = icet.ordersuf and
                  oeelk.lineno = icet.lineno and
                  oeelk.seqno = icet.seqno no-lock no-error.

      if not avail oeelk then
        do:
        message "oe" icet.prod icet.whse icet.orderno icet.ordersuf
                icet.lineno icet.seqno.
        pause.
        next icetloop.
        end.
       if avail oeelk and oeelk.specnstype = "n" then
        next icetloop.
      end.    
    else
    if (oeel.specnstype = "n" and oeel.kitfl = true) then
      next icetloop.

    assign z-usage = z-usage + (icet.stkqtyship
          * (if icet.transtype = "ri" and oeel.transtype = "rm" then
               1
            else
               -1))
               
            z-usageval = z-usageval + ((icet.stkqtyship
          * (if icet.transtype = "ri" and oeel.transtype = "rm" then
               1
            else
              -1)) * icet.cost).
               

    
    
    end.
  else
  if icet.module = "kp" then
    do:
    if icet.transtype <> "in" then
      next icetloop.
    
    find first oeelk where oeelk.cono = z-cono and 
                oeelk.ordertype = (if icet.module = "oe" then
                                    "o" 
                                   else 
                                    "w") and
                oeelk.orderno = icet.orderno and
                oeelk.ordersuf = icet.ordersuf and
                oeelk.shipprod = icet.prod
                no-lock no-error.
    if avail oeelk and oeelk.specnstype = "n" then
      next icetloop.

    assign z-usage = z-usage + (icet.stkqtyship * -1)
             z-usageval = z-usageval + 
                       ((icet.stkqtyship * icet.cost) * -1).  

    end.   
  else
  if icet.seqno <> 0 and icet.module = "va" then
    do:
    if icet.transtype <> "in" then
      next icetloop.

    find vaesl where vaesl.cono = z-cono and 
                     vaesl.vano = icet.orderno and
                     vaesl.vasuf = icet.ordersuf and
                     vaesl.seqno = icet.seqno and
                     vaesl.lineno = icet.lineno no-lock no-error.
    if vaesl.nonstockty = "n" then
      next icetloop.
    
    if not avail vaesl then
      do:   
      message "va" icet.prod icet.orderno icet.ordersuf icet.lineno icet.seqno.
      pause.
      end.   
    assign z-usage = z-usage + (icet.stkqtyship * -1)
             z-usageval = z-usageval + 
                       ((icet.stkqtyship * icet.cost) * -1).  
 
   
   end. 
end.
end.








procedure create_usage2:
define input parameter zi-date  as date no-undo.
define input parameter zi-usage as integer no-undo.
define input parameter zi-cost  as decimal no-undo.
define input parameter zi-prod  as char no-undo.
define input parameter zi-whse  as char no-undo.

find zsastz where zsastz.cono = z-cono and
                  zsastz.codeiden = "USAGE" and
                  zsastz.primarykey = zi-prod + "~011" + zi-whse and
                  zsastz.secondarykey = string(year(zi-date),"9999") 
                  exclusive-lock no-error.

if not avail zsastz then
  do:
  create zsastz.
  assign 
    zsastz.cono = z-cono 
    zsastz.codeiden = "USAGE" 
    zsastz.primarykey = zi-prod + "~011" + zi-whse
    zsastz.secondarykey = string(year(zi-date),"9999") 
    zsastz.codeval[13] = "01/01/1960" + "~011" + "01/01/1960"
    zsastz.codeval[1] = "0" + "~011" + "0.00" + "~011" + "0"
    zsastz.codeval[2] = "0" + "~011" + "0.00" + "~011" + "0"
    zsastz.codeval[3] = "0" + "~011" + "0.00" + "~011" + "0"
    zsastz.codeval[4] = "0" + "~011" + "0.00" + "~011" + "0"
    zsastz.codeval[5] = "0" + "~011" + "0.00" + "~011" + "0"
    zsastz.codeval[6] = "0" + "~011" + "0.00" + "~011" + "0"
    zsastz.codeval[7] = "0" + "~011" + "0.00" + "~011" + "0"
    zsastz.codeval[8] = "0" + "~011" + "0.00" + "~011" + "0"
    zsastz.codeval[9] = "0" + "~011" + "0.00" + "~011" + "0"
    zsastz.codeval[10] = "0" + "~011" + "0.00" + "~011" + "0"
    zsastz.codeval[11] = "0" + "~011" + "0.00" + "~011" + "0"
    zsastz.codeval[12] = "0" + "~011" + "0.00" + "~011" + "0".
  
  end.  


  
assign zsastz.codeval[13] = 
         if date(entry(1,zsastz.codeval[13],"~011")) > zi-date then
           entry(1,zsastz.codeval[13],"~011") + "~011" + 
           entry(2,zsastz.codeval[13],"~011")         
         else
           string(zi-date,"99/99/9999") + "~011" +
           entry(2,zsastz.codeval[13],"~011") 
       zsastz.codeval[month(zi-date)] = 
         string(int(entry(1,zsastz.codeval[month(zi-date)],"~011")) + 
                zi-usage)  + "~011" +
         string(dec(entry(2,zsastz.codeval[month(zi-date)],"~011")) + 
                zi-cost)   + "~011" +
         entry(3,zsastz.codeval[month(zi-date)],"~011"). 
   
end.
       


procedure get_issues:

define var zxyear as integer no-undo.
define var zxdate as date no-undo.

assign zxyear = year(z-enddate).
do  zxyear = zxyear to year(z-begdate):
  find zsastz where zsastz.cono = z-cono and
                    zsastz.codeiden = "USAGE" and
                    zsastz.primarykey = x-prod + "~011" + z-whse and
                    zsastz.secondarykey = string(zxyear,"9999") 
                    no-lock no-error.
  if avail zsastz then
    do:
    do xinx = 1 to 11:
      if (date (xinx + 1,1,zxyear) - 1) ge z-enddate and 
         (date (xinx,1,zxyear)) le z-begdate then 
         assign z-usageval = z-usageval +
                  dec(entry(2,zsastz.codeval[xinx],"~011"))  
                z-usage = z-usage +
                  int(entry(1,zsastz.codeval[xinx],"~011")).  
    end.
    if date (12,31,zxyear) ge z-enddate and 
       date (12,1,zxyear) le z-begdate then 
       assign z-usageval = z-usageval +
                dec(entry(2,zsastz.codeval[xinx],"~011"))  
              z-usage = z-usage +
                int(entry(1,zsastz.codeval[xinx],"~011")).  
    end.
    end.
end.

