/* zsdivaesu.p
 
   Post processing of the VA after the Infor updates have occured
   
   This is resetting the Used Parts Information after the transition to 
   completed. In case there is more activity ADDING SECTIONS these totals
   have already been moved to WIP 
     
 Created: 08/19/2005
 Author:  Tom Herog

*/   

{g-all.i}
 
def input parameter xp-vano  like vaeh.vano no-undo. 
def input parameter xp-vasuf like vaeh.vasuf no-undo. 
 
 
find vaeh where vaeh.cono = g-cono and
          vaeh.vano = xp-vano and
          vaeh.vasuf  = xp-vasuf 
          exclusive-lock no-error.
if avail vaeh and (vaeh.user6 <> 0 or vaeh.user7 <> 0) then
  do:
  find first vaes where 
             vaes.cono       = g-cono and
             vaes.vano       = vaeh.vano and
             vaes.vasuf      = vaeh.vasuf and
             vaes.completefl = true and
             vaes.user8      <> ?  no-error.
  if avail vaes then
    assign vaeh.user6 = 0 
           vaeh.user7 = 0
           vaes.user8 = ?.
  end.         
  

