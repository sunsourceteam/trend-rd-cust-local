/*****************************************************************************
Creates a file with all the APEC checking information. When APEC runs to print
checks for vendors, it also creates the file ARPvenrp.txt  to send to JP
Morgan Chase (JPC prefix - JPChase)
Dave Sivak
03/03/08
*****************************************************************************/
{p-rptbeg.i}

def stream arp.
def var o_doctype           as c    format "x". 
def var o_file              as c    format "x(30)".

assign o_file = "/usr/tmp/bank/JPPARPWver".

find zidc use-index docinx3 where 
     zidc.cono  = g-cono and
     zidc.docttype   = string(TODAY,"99/99/99") and
                           zidc.doctype    = "JPPARP"
                           no-error.
if avail zidc then
  do:
  if zidc.docdisp = "A" then
    assign o_doctype = "B".
  if zidc.docdisp = "B" then
    assign o_doctype = "C".
  if zidc.docdisp = "C" then
    assign o_doctype = "D".
  if zidc.docdisp = "D" then
    assign o_doctype = "E".
  if zidc.docdisp = "E" then
    assign o_doctype = "F".
  if zidc.docdisp = "F" then
    assign o_doctype = "G".
  if zidc.docdisp = "G" then
    assign o_doctype = "H".
  if zidc.docdisp = "H" then         
    assign o_doctype = "I".
  if zidc.docdisp = "I" then
    assign o_doctype = "J".
  if zidc.docdisp = "J" then
    assign o_doctype = "K".
  if zidc.docdisp = "K" then
    assign o_doctype = "L".
  if zidc.docdisp = "L" then
    assign o_doctype = "M".
  if zidc.docdisp = "M" then
    assign o_doctype = "N".
  if zidc.docdisp = "N" then
    assign o_doctype = "O".
  if zidc.docdisp = "O" then
    assign o_doctype = "P".
  if zidc.docdisp = "P" then
    assign o_doctype = "Q".
  if zidc.docdisp = "Q" then
    assign o_doctype = "R".
  if zidc.docdisp = "R" then
    assign o_doctype = "S".
  if zidc.docdisp = "S" then
    assign o_doctype = "T".
  if zidc.docdisp = "T" then
    assign o_doctype = "U".
  if zidc.docdisp = "U" then
    assign o_doctype = "V".
  if zidc.docdisp = "V" then
    assign o_doctype = "W".
  if zidc.docdisp = "W" then
    assign o_doctype = "X".
  if zidc.docdisp = "X" then
    assign o_doctype = "Y".
  if zidc.docdisp = "Y" then
    assign o_doctype = "Z".
  assign zidc.docdisp = o_doctype.
end. /* if avail zidc */
else
  do:
  assign o_doctype = "A".
  create zidc.
  assign zidc.cono      = g-cono.
  assign zidc.docttype  = string(TODAY,"99/99/99").
  assign zidc.doctype  = "JPPARP".
  assign zidc.docdisp   = o_doctype.
  assign zidc.docdate   = TODAY.
end. 

output stream arp to value(o_file + o_doctype + ".txt").

define variable destfilename as character format "x(40)".
define variable outputfilename as character format "x(40)".
define variable destfilecounter as character format "x(40)".

define variable flsuff  as character.
define variable flsuff2 as character.


substring(flsuff,1,2,"character") =
  substring(string(today),1,2,"character").
substring(flsuff,3,2,"character") = 
  substring(string(today),4,2,"character").

substring(flsuff,5,1,"character") = ".".

substring(flsuff,6,2,"character") = 
  substring(string(time,"HH:MM:SS"),1,2,"character").

substring(flsuff,8,2,"character") =
  substring(string(time,"HH:MM:SS"),4,2,"character").
substring(flsuff,10,2,"character") = 
  substring(string(time,"HH:MM:SS"),7,2,"character").

substring(destfilename,1,16,"character") = "/usr/tmp/JPPARPba".
substring(destfilename,17,11,"character") = flsuff.
substring(destfilecounter,1,19,"character") = "/usr/tmp/JPPARPcounter".

{g-apec.i new}

assign b-vendno = decimal(sapb.rangebeg[1]).
assign e-vendno = decimal(sapb.rangeend[1]).
assign p-jrnlno = integer(sapb.optvalue[1]).
assign p-bankno = integer(sapb.optvalue[6]).


/*DETAIL RECORD*/
def var d_paycode       as c    format "x(1)"           initial "P".
def var d_space1        as c    format "x(1)"           initial " ".
def var d_account       as i    format "99999999999999999999".
def var d_space2        as c    format "x(1)"           initial " ".
def var d_check_nbr     as i    format "999999999999999999".
def var d_space3        as c    format "x(1)"           initial " ".
def var d_check_amt     as de   format "999999999999999999".
def var d_space4        as c    format "x(1)"           initial " ".
def var d_issue_date    as c    format "x(8)".
def var d_space5        as c    format "x(1)"           initial " ".
def var d_paid_date     as c    format "x(8)".
def var d_space6        as c    format "x(1)"           initial " ".
def var d_additnl_data  as c    format "x(15)"          initial " ".
def var d_payee1_name   as c    format "x(50)".
def var d_payee2_name   as c    format "x(50)"          initial " ".
def var d-bankno        as i    format "999999"         initial 0.

/*WORK FIELDS*/
def var w_counter       as i    format "9999999".
def var ed_counter      as i    format "9999999".
def var w_total         as i    format "9999999999".

def buffer c-apet for apet.
def buffer c-apsv for apsv.

{w-crsb.i p-bankno no-lock}
if avail crsb then
  assign d_account = int(substr(crsb.bankacct,10,10)).
else
  return.
        
/*CREATE DETAIL RECORD*/
for each apet use-index k-jrnl where apet.cono      = g-cono and
                                     apet.jrnlno  = p-jrnlno and
                                     apet.vendno >= b-vendno and
                                     apet.vendno <= e-vendno and  
                                     apet.bankno  = p-bankno and
                                     apet.transcd = 07
                                     no-lock:
  if apet.manaddrfl then
    find apemm use-index k-apemm where
         apemm.cono = apet.cono and
         apemm.jrnlno = apet.pidjrnlno and
         apemm.setno = apet.pidsetno
         no-lock no-error.
  find apsv use-index k-apsv where   apsv.cono     = g-cono and
                                     apsv.vendno   = apet.vendno and
                                     apsv.bankno   = p-bankno and
                                    (apsv.vendtype <> "edc" and
                                     apsv.vendtype <> "eds" and
                                     apsv.vendtype <> "ddc" and
                                     apsv.vendtype <> "dds" and
                                     apsv.vendtype <> "sdc" and
                                     apsv.vendtype <> "sds" and
                                     apsv.vendtype <> "xdc" and
                                     apsv.vendtype <> "adc" and
                                     apsv.vendtype <> "udc" and
                                     apsv.vendtype <> "ldc" and
                                     apsv.vendtype <> "lds" and
                                     apsv.vendtype <> "pdc" and
                                     apsv.vendtype <> "sua" and
                                     apsv.vendtype <> "gdc" and
                                     apsv.vendtype <> "gds")
                                     no-lock no-error.
  if avail apsv then 
    do:
    assign d_payee1_name = if avail apemm and apet.manaddrfl = yes then
                             apemm.name else apsv.name.
    assign d_issue_date  = string(YEAR(apet.transdt),"9999") +
                           string(MONTH(apet.transdt),"99")  +
                           string(DAY(apet.transdt),"99").
    assign d_check_nbr   = int(apet.checkno).
    assign d_paid_date   = d_issue_date.
    assign d_check_amt   = apet.amount * 100.
    if d_check_amt > 0 then 
      do:
      put stream arp UNFORMATTED
         d_paycode      format "x(1)"
         d_space1       format "x(1)"
         d_account      format "99999999999999999999"
         d_space2       format "x(1)"
         d_check_nbr    format "999999999999999999"
         d_space3       format "x(1)"
         d_check_amt    format "999999999999999999"
         d_space4       format "x(1)"
         d_issue_date   format "x(8)"
         d_space5       format "x(1)"
         d_paid_date    format "x(8)"
         d_space6       format "x(1)"
         d_additnl_data format "x(15)"
         d_payee1_name  format "x(50)"
         d_payee2_name  format "x(50)"
         d-bankno       format "999999" 
         skip.
    end. /* if d_check_amt > 0 */
  end. /* avail apsv */
end. /* each apet */     
       
output stream arp close.

if opsys = "unix" then
  do:
  os-copy value(o_file + o_doctype + ".txt") value(destfilename).
  
/*
    if ed_counter > 0 then 
      os-delete value(o_file + o_doctype + ".txt").
*/      
end.            
 

            
            
            
/*/**************************Create ZIDC***************************************/
procedure create_zidc.

     create zidc.       
             assign zidc.cono      = g-cono.
             assign zidc.docttype  = string(TODAY,"99/99/99").
             assign zidc.docdate   = today.
             assign zidc.doctype  = "JPPARP".
             assign zidc.docdisp   = o_doctype.
end. 
*/

                        
