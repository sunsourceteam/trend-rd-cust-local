/*h****************************************************************************
  INCLUDE      : pdsc.lpr
  DESCRIPTION  : PDSC - Header and Copy update/editing block
  USED ONCE?   : yes pdsc.p
  AUTHOR       : jl
  DATE WRITTEN : 05/07/97
  CHANGES MADE :
    05/07/97 jl;  TB# 13808 Created - Cut out of pdsc to make this include
        added parameter &frame and put "frame &frame" after input keyword
    07/23/98 sbr; TB# 25126 Allowing blank product price type
    05/11/99 cm;  TB# 5366 Added shipto notes
    06/01/00 twa; TB# e5258 Nonstock Pricing; Allow entry of nonstock products
    10/31/00 kjb; TB# e4280 Add the ability to do customer pricing based upon
        the ICSW Rebate Type
******************************************************************************
    Required Parameters -

        &frame  - Pass in the name of the frame to use in the update
                  statement

******************************************************************************/


/* Set Error Message For Use of s-ctype */
v-ctm = if can-do("1,2,5",string(s-levcd)) then
            "Customer/Ship To Not Set Up in Ship To Setup - ARSS (4304)"
        else
            "Customer Type Not Set Up in System Table - SASTT (4005)".

/*tb 25126 07/23/98 sbr; Added code to set validate to false for s-ptype if
    s-levcd is 4 or 8 and s-ptype is blank. */
/*tb e4280 10/31/00 kjb; Added rebate to when condition for s-ptype */
update
    g-custno validate({v-arsc.i g-custno "/*"},v-cm)
        when can-do("1,2,5",string(s-levcd))
    s-ctype validate(if can-do("1,2,5",string(s-levcd))
                     then ({v-arss.i "input frame {&frame} g-custno" s-ctype})
                     else ({v-sasta.i j s-ctype "/*"}),v-ctm)
        when can-do("3,4,6",string(s-levcd)) or
             (v-pdjobfl and can-do("1,2,5",string(s-levcd)) and disp-pdjobfl)
    s-ptype validate (if can-do("4,8",string(s-levcd)) then
                          (if s-ptype = "" then false
                           else ({v-sasta.i k s-ptype "/*"}))
                      else if v-pdlevelty = "p" and s-levcd = 2 then
                          (if s-ptype = "" then false
                           else ({v-sasta.i k s-ptype "/*"}))
                      else if v-pdlevelty = "c" and s-levcd = 2 then
                          ({v-sasta.i c s-ptype "/*"})
                      else if v-pdlevelty = "l" and s-levcd = 2 then
                          can-find(first icsl use-index k-vend where
                                         icsl.cono       = g-cono      and
                                         icsl.vendno     = v-vendno    and
                                         icsl.prodline   = s-ptype)
                      else false,
                if can-do("4,8",string(s-levcd)) then v-ptm
                    else if v-pdlevelty = "p" and s-levcd = 2 then v-ptm
                    else if v-pdlevelty = "l" and s-levcd = 2 then v-plm
                    else v-pcm)
        when can-do("2,4,8",string(s-levcd)) and v-pdlevelty <> "r"
    g-prod when can-do("1,3,7",string(s-levcd))
    s-unit when can-do("1,3,7",string(s-levcd))
    g-whse validate({v-icsd.i g-whse},v-wm)
         when v-pdwhsefl
    s-startdt validate(s-startdt ne ?,"This is a Required Field (2100)")
    with frame {&frame} edit-block: editing:

    readkey.
    
    if g-errfl then do:

        hide message no-pause.
        g-errfl = false.
    end.

    /*tb e4280 10/31/00 kjb; Modified the prod parameter */
    {e-pddate.i &custno = "(if can-do(""1,2,5"",string(s-levcd)) then
                              input frame {&frame} g-custno else v-empty)"
                &prod   = "(if can-do(""1,3,7"",string(s-levcd)) then
                              input frame {&frame} g-prod
                            else if can-do(""2,4"",string(s-levcd)) and
                                 v-pdlevelty = ""r""
                              then ""r-"" + s-prodrebatety
                            else if s-levcd = 4 then
                              v-pdlevelty + ""-"" +
                                  input frame {&frame} s-ptype
                            else if s-levcd = 8 then
                              input frame {&frame} s-ptype
                            else if s-levcd = 2 then
                              v-pdlevelty + ""-"" +
                                  (if v-pdlevelty = ""l""
                                     then string(v-vendalpha,""999999999999"")
                                   else """") + input frame {&frame} s-ptype
                                   else v-null)"
                &ctype  = "(if  can-do(""3,4,6"",string(s-levcd))
                                or (can-do(""1,2,5"",string(s-levcd))
                                and v-pdjobfl) then
                                input frame {&frame} s-ctype else v-null)"
                &units  = "(if can-do(""1,3,7"",string(s-levcd)) then
                                input frame {&frame} s-unit else v-null)"
                &whse   = "(if v-pdwhsefl then
                                input frame {&frame}  g-whse
                            else v-null)"}

    if {k-after.i} then do:

        /* Prod Check */
        if frame-field = "g-prod"           and
            can-do("1,3,7",string(s-levcd)) and
            o-prod ne input frame {&frame} g-prod then do:

            g-prod = input frame {&frame} g-prod.

            run pdscp.p(g-prod).

            display s-name[2]
                    s-catalog
                    s-notes[2] @ icsp.notesfl
                    with frame {&frame}.

            if not s-ProdOK then do:
                run err.p(4757).
                next-prompt g-prod with frame {&frame}.
                next edit-block.
            end.

            o-prod = input frame {&frame} g-prod.

        end. /* if can-do("1,3,7",string(s-levcd)) */

        if frame-field = "s-unit" and
            input frame {&frame} s-unit ne "" then
        do for icsp:

            confirm = yes.

            /*tb 12743 09/03/93 pjt; Unit Field not edited, icsp find
                 was deleted after 4.2 */

            {w-icsp.i o-prod no-lock}


            /*tb 7241  (7.1) 03/03/97 jl; Added icss.gfi and  speccost.gas */
            /*d Find current special price costing information from
                ICSP record */

            if available icsp then
                {icss.gfi
                    &prod           = icsp.prod
                    &icspecrecno    = icsp.icspecrecno
                    &lock           = "no"}

            {speccost.gas}

            /*tb 7241  (7.1) 03/03/97 jl; Changed references of ICSP
                special price costing fields to standard speccost
                variables */

            if avail icsp then do:

                if v-speccostty ne ""
                    or icsp.unitstock = input frame {&frame} s-unit
                then do:   /*** Unit Check ***/

                    confirm = false.

                    run err.p(if v-speccostty ne "" then 5752 else 5753).

                end. /* if v-speccostty <> "" */

                else do for sasta, icseu:

                    o-unit = input frame {&frame} s-unit.

                    {p-unit.i &prod       = "o-prod"
                              &unitconvfl = "icsp.unitconvfl"
                              &unit       = "o-unit"
                              &unitstock  = "icsp.unitstock"
                              &desc       = "s-null"
                              &conv       = "s-nulln"
                              &confirm    = "confirm"}
                    if not confirm then run err.p(4026).
                end. /* else do for sasta */

            end. /* if available icsp */

            else if avail icsc then do:

                if icsc.speccostty ne ""
                    or icsc.unitstock = input frame {&frame} s-unit
                then do:                        /*** Unit Check ***/

                    confirm = false.

                    run err.p(if icsc.speccostty ne "" then 5752 else 5753).

                end. /* if icsc.speccostty <> "" */

                else do:

                    o-unit = input frame {&frame} s-unit.

                    if not {v-sasta.i "u" o-unit} then do:
                        confirm = false.
                        run err.p(4026).
                    end.

                end. /* else do */

            end. /* else if avail icsc */

            if not confirm then do:

                next-prompt s-unit with frame {&frame}.
                next.
            end.

        end. /* if frame-field = "s-unit" and  */

        /*d Customer Editing */
        if can-do("1,2,5",string(s-levcd))
           then do for arsc:

           {t-custno.i &field   = g-custno
                       &frame   = "{&frame}"
                       &def     = "/*"
                       &code    =
                       "o-ctype = ?.
                        if avail arsc then display arsc.name @ s-name[1]
                            with frame {&frame}.
                        else display v-invalid @ s-name[1] with frame {&frame}."
                                &display = "s-dummy"}

            if frame-field = "s-ctype" then
                assign g-custno = input frame {&frame} g-custno.

            /*tb 5366 05/11/99 cm; Added shipto notes */
            if input frame {&frame} s-ctype <> "" then do for arss:

                {w-arss.i g-custno "input frame {&frame} s-ctype" no-lock}

                if avail arss then display arss.notesfl with frame {&frame}.
                else display "" @ arss.notesfl with frame {&frame}.
            end.
            else display "" @ arss.notesfl with frame {&frame}.

        end. /* if can-do("1,2,5",string(s-levcd)) */

        if can-do("3,4,6",string(s-levcd)) and
            o-ctype ne input frame {&frame} s-ctype
        then do for sasta:                      /*** CType Check ***/

            o-ctype = input frame {&frame} s-ctype.

            {w-sasta.i j "input frame {&frame} s-ctype" no-lock}

            if avail sasta then
                    display sasta.descrip @ s-name[1]
                            with frame {&frame}.

            else display v-invalid @ s-name[1] with frame {&frame}.

        end. /* if can-do("3,4,6 */

        if can-do("1,2,5",string(s-levcd)) and
            o-ctype ne input frame {&frame} s-ctype
        then do:                                /*** ShiptoCheck ***/

            o-ctype = input frame {&frame} s-ctype.

            /*tb 5366 05/11/99 cm; Added shipto notes */
            if o-ctype = "" then
                display
                    "" @ s-name[3]
                    "" @ arss.notesfl
                with frame {&frame}.

            else do for arss:

                {w-arss.i "input frame {&frame} g-custno" o-ctype no-lock}

                if avail arss then do:

                    /*tb 5366 05/11/99 cm; Added shipto notes */
                    display arss.notesfl with frame {&frame}.

                    if arss.jobdesc ne "" then
                        display "Job: " +
                                arss.jobdesc @ s-name[3]
                                with frame {&frame}.
                    else display "Loc: " +
                            arss.city @ s-name[3] with frame {&frame}.

                end. /* if avail arss then do */

                else
                    /*tb 5366 05/11/99 cm; Added shipto notes */
                    display
                        v-invalid @ s-name[3]
                        "" @ arss.notesfl
                    with frame {&frame}.

            end. /* if o-ctype = "" */


        end. /* if can-do("1,2,5",string(s-levcd)) */



        /*d Price Type Check */
        if can-do("2,4,8",string(s-levcd)) and
            o-ptype ne input frame {&frame} s-ptype
        then do for sasta, icsl:                /*** PType Check ***/

            assign o-ptype = input frame {&frame} s-ptype
                   v-ptype = input frame {&frame} s-ptype.

            /*tb e4280 10/31/00 kjb; Added display of rebate type description.
                On next line, added the word "else". */
            if can-do("2,4",string(s-levcd)) and v-pdlevelty = "r"
                then {pdst.gfi pt v-ptype no}

            else if s-levcd = 2 and v-pdlevelty = "c"
                then {w-sasta.i c v-ptype no-lock}

            else if s-levcd = 2 and v-pdlevelty = "l" then do:

                if input frame {&frame} g-whse ne "" and v-pdwhsefl then
                    {w-icsl.i "input frame {&frame} g-whse"
                              v-vendno v-ptype no-lock}

                else find first icsl use-index k-vend where
                                icsl.cono       = g-cono      and
                                icsl.vendno     = v-vendno    and
                                icsl.prodline   = v-ptype     no-lock no-error.

            end. /* else if s-levcd = 2 */

            else if v-ptype ne "" then
                {w-sasta.i k v-ptype no-lock}.

            /*tb e4280 10/31/00 kjb; Added display of rebate descrip */
            s-name[2] = if v-pdlevelty = "r" and avail pdst then pdst.descrip
                        else if v-ptype = "" then v-invalid
                        else if s-levcd = 2 then
                            if v-pdlevelty = "l" and avail icsl then
                                 icsl.descrip
                            else if v-pdlevelty ne "l" and avail sasta then
                                 sasta.descrip
                            else v-invalid
                        else if avail sasta then sasta.descrip

                        else v-invalid.

            display s-name[2] with frame {&frame}.

        end. /* if can-do("2,4,8",string(s-levcd)) and  */

        /* Warehouse edit, if on product line */
        if input frame {&frame} g-whse ne o-whse and v-pdwhsefl
            and s-levcd = 2 and v-pdlevelty = "l"
            and v-ptype ne "" then do:

            o-whse = input frame {&frame} g-whse.

            run pdscd.p(o-whse,v-vendno,v-ptype).

            display s-name[2] with frame {&frame}.

        end. /* if input frame  */

    end. /* if {k-after.i} then do: */

    apply lastkey.

end. /*editing */

