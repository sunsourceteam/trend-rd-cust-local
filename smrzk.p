/*  smrzk.p
    sales report 1b - district or region by pcat,
    compare curr mo to last year same month; also compare curr ytd to 
    last ytd
    26/jan/99 gb
    30/jan/99 gb - add rep,district subtotals 
    31/jan/99 gb - add summary report logic        
    12/feb/99 gb - turn off sassr ability to print headings, print landscape.
    12/mar/99 gb - add chgs per chuck's email

*/


{p-rptbeg.i}


def new shared temp-table zicp no-undo
  field prod    like icsp.prod
index p
  prod.
  



def new shared temp-table xwo no-undo
  field whse   like icsw.whse
  field custno like arsc.custno
  field shipto like arss.shipto
  field pcat   like icsp.prodcat
  field prod   like icsp.prod
  field yr     as integer
  field mnth   as integer
  field qty    as decimal
  field qtyfl  as decimal
  field salesamt as decimal
  field wono   like kpet.wono
  field wosuf  like kpet.wosuf
index
  whse
  custno
  shipto
  prod
  yr
  mnth descending.





def new shared var yrz as integer extent 2 no-undo.
def var inz as integer          no-undo.

def buffer b-icsp for icsp.
def buffer b-oeelk for oeelk.
def var cc as integer.

def var hold-orderno like oeeh.orderno no-undo.
def var hold-ordersuf like oeeh.ordersuf no-undo.
def var hold-lineno like oeel.lineno no-undo.



def var newreport as logical no-undo.
def var newpage as logical no-undo.
def var printcust as logical no-undo.
def var validcustinfo as logical no-undo.
def var zk-cost as decimal no-undo.
def var zk-price as decimal no-undo.
def var zk-type as decimal no-undo.
def var zk-found as logical no-undo.
def var vz-cost as decimal no-undo.
def var vz-price as decimal no-undo.
def var v-rebate as decimal no-undo.
def var vz-prodcat as character format "x(4)" no-undo.
def var z-tild  as character format "x" init "~~" no-undo.
def var z-hash  as character format "x" init "#" no-undo.
def var z-cart  as character format "x" init "^" no-undo.
def var z-atsn  as character format "x" init "@" no-undo.
def var z-pcsn  as character format "x" init "%" no-undo.

def var narr1   as c  format "x(10)" no-undo.
def var narr2   as c  format "x(30)" no-undo.
def var narr3   as c  format "x(4)"  no-undo.
def var distx   as c  format "x(12)" no-undo.
def var i-list  as logical           no-undo.
def var i-kp      as logical         no-undo.
def var i-bodkits as logical         no-undo.
def var i-vastuff as logical         no-undo.
def var i-costonly as logical        no-undo.
def var i-state as char format "xx"  no-undo.
def var i-rgn   as c  format "x"    no-undo.
def var i-dsrt  as c  format "x(3)" no-undo.
def var i-compare as logical no-undo.
def var as-per  as i  format "9999" no-undo.
def var i-sort  as c  format "x"    no-undo.
def var i-type  as c  format "xx"    no-undo.
def var i-cnt   as de format ">>>9" no-undo.
def var x-page  as integer format ">>>9" no-undo.
def var x-yr    as i  format "99"   no-undo.
def var x-state as character format  "xx" no-undo.
def var e-yr    as i  format "99"   no-undo.
def var e-mo    as i  format ">9"   no-undo.
def var bgdt    as date             no-undo.
def var eddt    as date             no-undo.
def var catbg   as c  format "x(4)" no-undo.
def var cated   as c  format "x(4)" no-undo.
def new shared var kcatbg   as c  format "x(4)" no-undo.
def new shared var kcated   as c  format "x(4)" no-undo.
def var smnbg   as c  format "x(4)" no-undo.
def var smned   as c  format "x(4)" no-undo.
def var cstbg   as dec              no-undo.
def var csted   as dec              no-undo.
def var csthi   as dec init 999999999999 no-undo.


def var pk_cat as character format "x(5)" no-undo.
def var pk_custno like arsc.custno no-undo.
def var pk_shipto like arss.shipto no-undo.
def var pk_good   as logical       no-undo.


def var x-slsm  as c  format "x(4)"  no-undo.
def var x-reg   as c  format "x(1)"  no-undo.
def var x-dist  as c  format "x(3)"  no-undo.
def var n-cnt   as i                 no-undo.
def var v-avoid as logical           no-undo.
def var v-sls   as de format ">>>,>>>,>>9-" no-undo.
def var v-val   as de format ">>>,>>>,>>9-" no-undo.
def var v-mgn   as de format ">>>,>>>,>>9-" no-undo.
def var p-desc  as c  format "x(24)"   no-undo.
def var n-desc  as c  format "x(12)"   no-undo. 
def var c-pct   as de format ">>>9.9-" no-undo.
def var l-process as logical no-undo.
def var l-pct   as de format ">>>9.9-" no-undo.
def var m-ytd   as de format ">>>9.9-" no-undo.
def var m-last  as de format ">>>9.9-" no-undo.
def var s-pdif  as de format ">>>9.9-" no-undo.
def var s-diff  as de format ">>>,>>>,>>9-" no-undo.
def var s-mdif  as de format ">>>9.9-" no-undo.
def var p-pdif  as de format ">>>9.9-" no-undo.
def var p-diff  as de format ">>,>>>,>>9-" no-undo.
def var p-mdif  as de format ">>>9.9-" no-undo.
def var p-sls   as de format ">>>,>>>,>>9-" extent 6 no-undo.  /* pcat ttls */
def var p-mgn   as de format ">>>9.9-" extent 8 no-undo.
def var h-sls   as de format ">>>,>>>,>>9-" extent 6 no-undo.  /* pcat ttls */
def var h-mgn   as de format ">>>9.9-" extent 8 no-undo.
def var d-sls   as de format ">>>,>>>,>>9-" extent 6 no-undo. /* district ttl */
def var d-mgn   as de format ">>>9.9-" extent 8 no-undo.
def var r-sls   as de format ">>>,>>>,>>9-" extent 6 no-undo.  /* rep ttls */
def var r-mgn   as de format ">>>9.9-" extent 8 no-undo.
def var t-sls   as de format ">>>,>>>,>>9-" extent 6 no-undo.  /* region ttl */
def var t-mgn   as de format ">>>9.9-" extent 8 no-undo.
def var f-sls   as de format ">>>,>>>,>>9-" extent 6 no-undo.  /* region ttl */
def var f-mgn   as de format ">>>9.9-" extent 8 no-undo.
def var c-sls   as de format ">>>,>>>,>>9-" extent 6 no-undo.  /* region ttl */
def var c-mgn   as de format ">>>9.9-" extent 8 no-undo.
def var m-lbl   as c  format "x(3)" extent 2 no-undo.
def var skipdet as logical no-undo.
def var x-val as c format "x(3)" no-undo. 
def var x-cat as c format "x(4)" no-undo. 
def var z-cat as c format "x(5)" no-undo. 
def var x-today as date format "99/99/9999" no-undo.
def var x-time as char format "x(5)" no-undo.
def var x-lit as cha format "x(5)" init "Sales" no-undo.
def var n-mth as c format "x(3)" extent 12 no-undo
init ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'].
def var i-div as i format ">>99".

{zsapboydef.i}


def var v-peckinx as integer no-undo.
def var zzx as integer no-undo.

def var v-peckinglist as character format "x(4)" extent 10 init
  ["fmec",
   "fgr ",
   "fbir",
   "dhou",
   "dmcp",
   "datl",
   "decp",
   "dscp",
   "dwcp",
   "dros"].


DEF temp-table whses no-undo
  field zwhse like icsw.whse
  field zcnt as integer
  
  index k-zpecking
        zcnt
  index k-zwhse
        zwhse.





def temp-table zsmsep no-undo 
  Field cono        as integer
  Field yr          as integer format "99"
  Field custno      as decimal format "zzzzzzzzzzz9"
  Field shipto      as char format "x(8)"
  Field prodcat     as char format "x(5)"
  Field whse        as char format "x(4)"
  Field salesamt    as decimal format "zzzzzzzz9.99-" extent 12
                       init [0,0,0,0,0,0,0,0,0,0,0,0] 
  Field discamt     as decimal format "zzzzzzzz9.99-" extent 12
                       init [0,0,0,0,0,0,0,0,0,0,0,0]
  Field qtysold     as decimal format "zzzzzzzz9.99-" extent 12
                       init [0,0,0,0,0,0,0,0,0,0,0,0] 
  Field cogamt      as decimal format "zzzzzzzz9.99-" extent 12
                       init [0,0,0,0,0,0,0,0,0,0,0,0] 
  Field prntfl      as logical
  
  index k-prodcat
    cono
    yr
    prodcat
    whse
    custno
    shipto.


define buffer b-zsmsep for zsmsep.


def temp-table tdetail no-undo
    field state     as c  format "xx"
    field region    as c  format "x"
    field district  as c  format "xxx"
    field rep       as c  format "x(4)"
    field cust      as de format ">>>>>>>>>>9"
    field name      as char format "x(15)"
    field ptdsls    as de format ">>>>>>>>>9.99-"
    field ytdsls    as de format ">>>>>>>>>9.99-"
    field ptdmgn    as de format ">9.999"                /* 100 */
    field ytdmgn    as de format ">9.999"
    field lyptdsls  as de format ">>>>>>>>>9.99-"
    field lyytdsls  as de format ">>>>>>>>>9.99-"
    field lyptdmgn  as de format ">9.999"
    field lyytdmgn  as de format ">9.999"
    field vend      as c  format "x(2)"
    field cat       as c  format "x(5)"
    field cnt       as i  format ">>>>>>>9"
    field prntfl    as logical
  index k-dprime as primary
    region
    district
    rep
    cust
    cat
  index k-cater
     cat
  index k-dsales
    region
    district
    rep
    cust
    ytdsls descending
    vend
    cat.
def temp-table tsort no-undo
    field region    as c  format "x"
    field district  as c  format "xxx"
    field rep       as c  format "x(4)"
    field name      as c  format "x(15)"
    field cust      as de format ">>>>>>>>>>9"
    field dytd      as de format ">>>>>>>>>9-"
    field lytd      as de format ">>>>>>>>>9-"
    field ytd       as de format ">>>>>>>>>9-"
    field vend      as c  format "x(2)"
    field cat       as c  format "x(3)"
    field cnt       as i  format ">>>>>>>9"
    field prntfl    as logical
  index k-prime as primary
    region
    district
    rep
    cust
    cat
  index k-sales
    region
    district
    rep
    dytd descending
    lytd descending
    cust
    ytd descending
    vend
    cat.

def buffer b-tdetail for tdetail.
def temp-table tdsort no-undo
    field region    as c  format "x"
    field district  as c  format "xxx"
    field rep       as c  format "x(4)"
    field name      as c  format "x(15)"
    field cust      as de format ">>>>>>>>>>9"
    field dytd      as de format ">>>>>>>>>9-"
    field lytd      as de format ">>>>>>>>>9-"
   index k-dprime as primary
    region
    district
    rep
    cust.
    

/*****   Landscape print sequence *******/
/*
DEFINE FRAME kheadings1
  
  "~033~105~033~046~154~061~117~033~046~153~062~123~033~046~154~060~105" at 1
         with  down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.
     
*/  
 Define frame f-hd 
    "~033~105~033~046~154~061~117~033~046~153~062~123~033~046~154~060~105" at 1
    "Date:" at 1
    x-today format "99/99/99"  at 7
    "at "      at   16
    x-time     at   19
    "~015"
    "Company:" at 1 
    g-cono at 10
    "Operator:" at 130 
    sapb.operinit at 140 
    "Page:" at 160 
    x-page  at 166 format ">>>9"
    "~015"  at 178
    "Function: smrzk" at 1
    "Component Product Category Sales Comparison" at 72
    "~015"  at 178
    narr1   at 69
    narr2   at 80
    narr3   at 170
    "~015"  at 178 
    "Pcat"  at 1
    "Curr " at 41 
    m-lbl[1] at 46
    "Prev " at 62 
    m-lbl[2] at 67                                       /* 200 */
    "Variance" at 86
    "Curr YTD" at 112
    "Prev YTD" at 133
    "Variance" at 154
    "~015"  at 178
    "Sales" at 37
    "Mgn"   at 47
    "Sales" at 58
    "Mgn"   at 68
    "Sales" at 79
    "S %"   at 89
    "Mgn"   at 97
    "Sales" at 108
    "Mgn"   at 118
    "Sales" at 129
    "Mgn"   at 140
    "Sales" at 150
    "S %"   at 160
    "Mgn"   at 168
    "~015"  at 178 
with down stream-io width 178 no-labels no-box no-underline.    
 
Define frame f-shd 
    "~033~105~033~046~154~061~117~033~046~153~062~123~033~046~154~060~105" at 1
    "Date:" at 1
    x-today format "99/99/99"  at 7
    "at "      at   16
    x-time     at   19
    "~015"
    "Company:" at 1 
    g-cono   at 10
    "Operator:" at 130 
    sapb.operinit at 140
    "Page:" at 160 
    x-page at 166 format ">>>9"
    "~015"  at 178
    "Function: smrzk" at 1
    "Component Product Category Sales Comparison" at 72
    "~015"  at 178
    narr1   at 69
    narr2   at 80
    narr3   at 170
    "~015"  at 178 
    "~015"  at 1
    "Pcat"  at 1
    "Curr " at 41 
    m-lbl[1] at 46
    "Prev " at 62 
    m-lbl[2] at 67
    "Variance" at 86
    "Curr YTD" at 112
    "Prev YTD" at 133
    "Variance" at 154
    "~015"  at 178
    "Sales" at 37
    "Mgn"   at 47
    "Sales" at 58
    "Mgn"   at 68
    "Sales" at 79
    "S %"   at 89
    "Mgn"   at 97
    "Sales" at 108
    "Mgn"   at 118
    "Sales" at 129
    "Mgn"   at 140
    "Sales" at 150
    "S %"   at 160
    "Mgn"   at 168
    "~015"  at 178 
with down stream-io width 178 no-labels no-box no-underline.    


  
Define frame f-chd 
    "~033~105~033~046~154~061~117~033~046~153~062~123~033~046~154~060~105" at 1
    "Date:" at 1
    x-today format "99/99/99"  at 7
    "at "      at   16
    x-time     at   19
    "~015"
    "Company:" at 1 
    g-cono at 10
    "Operator:" at 130 
    sapb.operinit at 140 
    "Page:" at 160 
    x-page  at 166 format ">>>9"
    "~015"  at 178
    "Function: smrzk" at 1
    "Component Product Category Sales Comparison" at 72
    "~015"  at 178
    narr1   at 69
    narr2   at 80
    narr3   at 170
    "~015"  at 178 
    "Pcat"  at 1
    "Curr " at 41 
    m-lbl[1] at 46
    "Prev " at 62 
    m-lbl[2] at 67
    "Variance" at 86                                     /* 300 */
    "Curr YTD" at 112
    "Prev YTD" at 133
    "Variance" at 154
    "~015"  at 178
    "Cost " at 37
    "Mgn"   at 47
    "Cost " at 58
    "Mgn"   at 68
    "Cost " at 79
    "S %"   at 89
    "Mgn"   at 97
    "Cost " at 108
    "Mgn"   at 118
    "Cost " at 129
    "Mgn"   at 140
    "Cost " at 150
    "S %"   at 160
    "Mgn"   at 168
    "~015"  at 178 
with down stream-io width 178 no-labels no-box no-underline.    
 
 Define frame f-cshd 
    "~033~105~033~046~154~061~117~033~046~153~062~123~033~046~154~060~105" at 1
    "Date:" at 1
    x-today format "99/99/99"  at 7
    "at "      at   16
    x-time     at   19
    "~015"
    "Company:" at 1 
    g-cono   at 10
    "Operator:" at 130 
    sapb.operinit at 140
    "Page:" at 160 
    x-page at 166 format ">>>9"
    "~015"  at 178
    "Function: smrzk" at 1
    "Component Product Category Sales Comparison" at 72
    "~015"  at 178
    narr1   at 69
    narr2   at 80
    narr3   at 170
    "~015"  at 178 
    "~015"  at 1
    "Pcat"  at 1
    "Curr " at 41 
    m-lbl[1] at 46
    "Prev " at 62 
    m-lbl[2] at 67
    "Variance" at 86
    "Curr YTD" at 112
    "Prev YTD" at 133
    "Variance" at 154
    "~015"  at 178
    "Cost " at 37
    "Mgn"   at 47
    "Cost " at 58
    "Mgn"   at 68
    "Cost " at 79
    "S %"   at 89
    "Mgn"   at 97
    "Cost " at 108
    "Mgn"   at 118
    "Cost " at 129
    "Mgn"   at 140
    "Cost " at 150
    "S %"   at 160
    "Mgn"   at 168
    "~015"  at 178 
with down stream-io width 178 no-labels no-box no-underline.    



define frame f-disp
    x-cat           at 1 format "x(4)"
    x-val           at 5 format "x(3)"
    p-desc          at 9 format "x(22)" 
    tdetail.ptdsls  at 31 format ">>>,>>>,>>9-"
    c-pct           at 44 format ">>>9.9-"
    tdetail.lyptdsls at 52 format ">>>,>>>,>>9-"
    l-pct           at 65 format ">>>9.9-"
    p-diff          at 73 format ">>>,>>>,>>9-"
    p-pdif          at 86 format ">>>9.9-"
    p-mdif          at 94 format ">>>9.9-"
    tdetail.ytdsls  at 102 format ">>>,>>>,>>9-"
    m-ytd           at 115 format ">>>9.9-"
    tdetail.lyytdsls   at 124 format ">>>,>>>,>>9-"
    m-last          at 136 format ">>>9.9-"
    s-diff          at 145 format ">>>,>>>,>>9-"
    s-pdif          at 157 format ">>>9.9-"
    s-mdif          at 165 format ">>>9.9-"
    "~015"          at 178
with down stream-io width 178 no-box no-labels no-underline.
 define frame f-sub
    n-desc   at 8
    p-sls[1] at 31
    h-mgn[1] at 44
    p-sls[2] at 52
    h-mgn[2] at 65
    h-sls[5] at 73
    h-mgn[5] at 86
    h-mgn[7] at 94                                       /* 400 */
    p-sls[3] at 102
    h-mgn[3] at 115
    p-sls[4] at 124
    h-mgn[4] at 136
    h-sls[6] at 145
    h-mgn[6] at 157
    h-mgn[8] at 165
    "~015"   at 178
    
with down stream-io width 178 no-labels no-box no-underline.
define frame f-rep
    "Subtotal Rep" at 2
    tsort.rep at 15
    r-sls[1] at 31
    r-mgn[1] at 44
    r-sls[2] at 52
    r-mgn[2] at 65
    r-sls[5] at 73
    r-mgn[5] at 86
    r-mgn[7] at 94
    r-sls[3] at 102
    r-mgn[3] at 115
    r-sls[4] at 124
    r-mgn[4] at 136
    r-sls[6] at 145
    r-mgn[6] at 157
    r-mgn[8] at 165
    "~015"   at 178
with down stream-io width 178 no-labels no-box no-underline.
define frame f-custh
    "*" at 1
    tsort.cust at 2 format "***********9"
    tsort.name at 15
    "~015"       at 178
with down stream-io width 178 no-labels no-box no-underline.
define frame f-cust
    "*" at 1
    tsort.cust at 2 format "***********9"
    tsort.name at 15
    c-sls[1] at 31
    c-mgn[1] at 44
    c-sls[2] at 52
    c-mgn[2] at 65
    c-sls[5] at 73
    c-mgn[5] at 86
    c-mgn[7] at 94
    c-sls[3] at 102
    c-mgn[3] at 115
    c-sls[4] at 124
    c-mgn[4] at 136
    c-sls[6] at 145
    c-mgn[6] at 157
    c-mgn[8] at 165
    "~015"   at 178
    "~015"   at 1
with down stream-io width 178 no-labels no-box no-underline.

define frame f-dstrct
    distx    at 2
    tsort.district at 15
    d-sls[1] at 31
    d-mgn[1] at 44
    d-sls[2] at 52
    d-mgn[2] at 65
    d-sls[5] at 73
    d-mgn[5] at 86
    d-mgn[7] at 94
    d-sls[3] at 102
    d-mgn[3] at 115
    d-sls[4] at 124
    d-mgn[4] at 136
    d-sls[6] at 145
    d-mgn[6] at 157
    d-mgn[8] at 165
    "~015"   at 178
with down stream-io width 178 no-labels no-box no-underline.
define frame f-tot
    "Region Ttl" at 2
    tsort.region at 15
    t-sls[1] at 31
    t-mgn[1] at 44
    t-sls[2] at 52
    t-mgn[2] at 65
    t-sls[5] at 73
    t-mgn[5] at 86
    t-mgn[7] at 94
    t-sls[3] at 102
    t-mgn[3] at 115
    t-sls[4] at 124
    t-mgn[4] at 136
    t-sls[6] at 145 
    t-mgn[6] at 157
    t-mgn[8] at 165
    "~015"   at 178
with down stream-io width 178 no-labels no-box no-underline.

define frame f-final
    "Final Ttl" at 2
    f-sls[1] at 31
    f-mgn[1] at 44                                       /* 500 */
    f-sls[2] at 52
    f-mgn[2] at 65
    f-sls[5] at 73
    f-mgn[5] at 86
    f-mgn[7] at 94
    f-sls[3] at 102
    f-mgn[3] at 115
    f-sls[4] at 124
    f-mgn[4] at 136
    f-sls[6] at 145 
    f-mgn[6] at 157
    f-mgn[8] at 165
    "~015"   at 178
with down stream-io width 178 no-labels no-box no-underline.



assign i-rgn = sapb.optvalue[1]
       i-dsrt = sapb.optvalue[2]
       i-type = sapb.optvalue[10]   /* District or Region Rpt */
       i-sort = sapb.optvalue[11]   /* Summary or Detail Rpt */
       i-cnt  = dec(sapb.optvalue[12])
       v-perin = sapb.optvalue[13]
       i-list  = if sapb.optvalue[14] = "yes" then
                   true
                 else
                   false
       i-costonly = if sapb.optvalue[15] = "yes" then
                   true
                 else
                   false
       i-compare = if sapb.optvalue[17] <> "N" then
                   true
                 else
                   false
       i-bodkits  = if sapb.optvalue[18] = "yes" then
                   true
                 else
                   false
            
       i-vastuff = if sapb.optvalue[19] = "yes" then
                   true
                 else
                   false
            
       i-kp      = if sapb.optvalue[20] = "yes" then
                   true
                 else
                   false.
            
                    
            
                   
assign i-div = integer(sapb.optvalue[16]).

if sapb.rangebeg[1] = "" then
   catbg = "    ".
else
   catbg = rangebeg[1].
   
if sapb.rangeend[1] = "" then
   cated = "````".
else
   cated = sapb.rangeend[1].

if sapb.rangebeg[2] = "" then
   smnbg = "    ".
else
   smnbg = rangebeg[2].
   
if sapb.rangeend[2] = "" then
   smned = "````".
else
   smned = sapb.rangeend[2].
   

if sapb.rangebeg[3] = "" then
   kcatbg = "    ".
else
   kcatbg = rangebeg[3].
   
if sapb.rangeend[3] = "" then
   kcated = "````".
else
   kcated = sapb.rangeend[3].



if dec(sapb.rangebeg[4]) = 0 then
   cstbg = 0.
else
   cstbg = dec(rangebeg[4]).
   
if dec(sapb.rangeend[4]) = 0 then
   csted = csthi.
else
   csted = dec(sapb.rangeend[4]).
    
 

if i-type = "r" then
  assign narr1 = " "
       narr2 = " "
       narr3 = " ".
else
  assign narr1 = "Salesrep: "
         narr2 = " "
         narr3 = " ".

if i-costonly = true then
   x-lit = "Cost ".
else
   x-lit = "Sales". 
   


{p-rptper.i}       
assign as-per = v-perout.       

assign x-time = string(time,"HH:MM")
       x-today = today.

if i-list then
  do:
  {zsapboyload.i}
  end.



assign zelection_matrix [1] = (if zelection_matrix[1] > 1 then /* Rep   */
                                 zelection_matrix[1]
                              else   
                                 1)
       zelection_matrix [2] = (if zelection_matrix[2] > 1 then  /* Cust */
                                 zelection_matrix[2]
                              else   
                                 1)
       zelection_matrix [3] = (if zelection_matrix[3] > 1 then  /* Cat  */
                                 zelection_matrix[3]
                              else   
                                 1)
                   

       zzl_begcat =  catbg
       zzl_endcat =  cated
       zzl_begcust = cstbg
       zzl_endcust = csted
       zzl_begrep  = smnbg
       zzl_endrep  = smned.



/* calc curr month from input as of date  */
assign e-yr = int(substr(string(as-per,"9999"),3,2))
       e-mo = int(substr(string(as-per,"9999"),1,2)).
                                        
assign m-lbl[1] = n-mth[e-mo]
       m-lbl[2] = m-lbl[1].

yrz[2] = e-yr.

if e-yr > 0 then
 yrz[1] = e-yr - 1.
else
 yrz[1] = 99. 



zzx = 0.

do v-peckinx = 1 to 10:
  find whses use-index k-zwhse
       where whses.zwhse = v-peckinglist[v-peckinx] no-lock no-error.
  if not avail whses then
    do:
    create whses.
    zzx = zzx + 1.
    assign whses.zcnt = zzx
           whses.zwhse = v-peckinglist[v-peckinx].
    end.       
end.

for each icsd where icsd.cono = g-cono no-lock:
 find whses use-index k-zwhse
       where whses.zwhse = icsd.whse no-lock no-error.
  if not avail whses then
    do:
    create whses.
    zzx = zzx + 1.
    assign whses.zcnt = zzx
           whses.zwhse = icsd.whse.
    end.       
end.


do inz = 1 to 2:
x-yr = yrz[inz].

if i-kp  and inz = 1 then
  do:
  run smrzk99.p.
  for each xwo no-lock:
                         
    assign
        zk-type = 0
        zk-cost = 0
        zk-price = 0.
      find kpet where kpet.cono = g-cono
                     and kpet.wono = xwo.wono
                     and kpet.wosuf = xwo.wosuf no-lock no-error.
      if avail kpet then do:
        for each b-oeelk use-index k-oeelk
                  where b-oeelk.cono = g-cono
                    and b-oeelk.ordertype = "w"
                    and b-oeelk.orderno = kpet.wono
                    and b-oeelk.ordersuf = kpet.wosuf
                    and b-oeelk.lineno  = 0
                    and b-oeelk.specnstype <> "l"
                     no-lock:
            assign zk-cost = (b-oeelk.qtyneeded  
                            * b-oeelk.prodcost) + zk-cost.      
         end.
      end.
/*      zk-cost = kpet.prodcost.      */
 
      if zk-cost le 0 then
        next.
      for each oeelk  use-index k-oeelk
                 where oeelk.cono = g-cono and 
                       oeelk.ordertype = "w" and
                       oeelk.orderno = xwo.wono and
                       oeelk.ordersuf = xwo.wosuf  no-lock:
       find icsp where icsp.cono = g-cono and
                       icsp.prod = oeelk.shipprod no-lock no-error.
      
     run create_zsmsep.
     end.
  end.
end.

    

if i-bodkits and inz = 1 then
  do:
  assign bgdt = date(01,01,(if yrz[1] < 50 then (yrz[1] + 2000) else 
                       (yrz[1] + 1900)))
           eddt = date((if e-mo <> 12 then
                        e-mo + 1
                      else
                        1)
                        ,1
                        ,
                       ( if yrz[2] < 50 then 
                           (yrz[2] + 2000 +
                             (if e-mo <> 12 then 0 else 1))
                          else 
                            (yrz[2] +
                            (if e-mo <> 12 then 0 else 1)))). 

  
  assign 
    zk-price = 0
    zk-type = 0
    zk-cost = 0.
  joeelkloop:
  for each oeelk  use-index  /* k-specns */ k-oeelk
                 where oeelk.cono = g-cono and 
                       oeelk.ordertype = "o" and
                       oeelk.statustype = "i" and
                       oeelk.specnstype <> "l"  
                       no-lock:
                       /*
                       break by string(oeelk.orderno,">>>>>>>>9") + 
                                string(oeelk.ordersuf,"99") +
                                string(oeelk.lineno,">>>9") :
      
    if first-of(string(oeelk.orderno,">>>>>>>>9") + 
                string(oeelk.ordersuf,"99") +
                string(oeelk.lineno,">>>9")) then */
    if oeelk.orderno <> hold-orderno or
       oeelk.lineno <> hold-lineno or
       oeelk.ordersuf <> hold-ordersuf then
      do:                        
      l-process = false.
      assign hold-orderno = oeelk.orderno
             hold-lineno = oeelk.lineno 
             hold-ordersuf = oeelk.ordersuf. 
      find oeel use-index k-oeel
              where oeel.cono = g-cono and
                    oeel.orderno = oeelk.orderno and
                    oeel.ordersuf = oeelk.ordersuf and
                    oeel.lineno = oeelk.lineno and
                    oeel.invoicedt ge bgdt and
                    oeel.invoicedt <  eddt and
                    oeel.specnstype <> "l" and
                    oeel.prodcat ge kcatbg and
                    oeel.prodcat le kcated
                    no-lock no-error.
       if not avail oeel or oeel.qtyship = 0 then
         next joeelkloop.
       l-process = true.
       zk-cost = 0.
       for each b-oeelk use-index k-oeelk
                       where b-oeelk.cono = g-cono
                         and b-oeelk.ordertype = oeelk.ordertype
                         and b-oeelk.orderno = oeelk.orderno
                         and b-oeelk.ordersuf = oeelk.ordersuf
                         and b-oeelk.lineno  = oeelk.lineno
                         and b-oeelk.specnstype <> "l"
                         no-lock:
          zk-cost = ((b-oeelk.qtyneeded)   /* this does not use qty ship
                                              because it should be how much the
                                              unit each cost is */
                     * b-oeelk.prodcost) + zk-cost.      
       end.
       
       end.
    
    if avail oeel and l-process and zk-cost <> 0 then 
      run create_zsmsep_bod.
  end.
end.


if i-vastuff and inz = 1 then
  do:
  assign bgdt = date(01,01,(if yrz[1] < 50 then (yrz[1] + 2000) else 
                       (yrz[1] + 1900)))
         eddt = date((if e-mo <> 12 then
                        e-mo + 1
                      else
                        1)
                        ,1
                        ,
                       ( if yrz[2] < 50 then 
                           (yrz[2] + 2000 +
                             (if e-mo <> 12 then 0 else 1))
                          else 
                            (yrz[2] +
                            (if e-mo <> 12 then 0 else 1)))). 

    for each vaeh where vaeh.cono = g-cono and 
                 vaeh.receiptdt <> ? and
                 vaeh.stagecd <> 9   and
                 vaeh.prodcat ge kcatbg and
                 vaeh.prodcat le kcated
                 no-lock:
   if vaeh.prodcost le 0 then
     next.
     
   assign
     zk-cost  = 0
     zk-price = 0
     zk-type  = 0
     zk-found = false.

   {z-smrzk.i zk-price zk-found}.  
   if zk-found then
     if
      oeel.invoicedt ge bgdt and
      oeel.invoicedt < eddt then
     do:
     for each vaesl where
                       vaesl.cono = g-cono and
                       vaesl.vano = vaeh.vano and 
                       vaesl.vasuf = vaeh.vasuf and
                       can-do ("in,it",vaesl.sctntype) no-lock:
                             /* took out II sections aren't selling them
                                They are going in to inventory            */
              
       run create_zsmsep_va.
     end.
     end.   
   end.
 end. 


if i-compare then      
  do:
  if sapb.optvalue[17] = "c" then
    do:
    for each zsmsep where zsmsep.cono = g-cono and zsmsep.yr = x-yr
         and substring (zsmsep.prodcat,5,1) <> "~~"  no-lock break by
             substring (zsmsep.prodcat,1,4):

      if first-of(substring(zsmsep.prodcat,1,4)) then   
        for each smsep where smsep.cono = g-cono and smsep.yr = x-yr
                 and smsep.prodcat = substring(zsmsep.prodcat,1,4)
               USE-INDEX k-prodcat no-lock:
         run create_zsmsep2.
        end. 
    end.
    end.
  else
  if sapb.optvalue[17] <> "e" then
    do:
     for each smsep where smsep.cono = g-cono and smsep.yr = x-yr
              USE-INDEX k-prodcat no-lock:
         run create_zsmsep2.
     end.  
    end.
end.    
end.      

do inz = 1 to 2:
x-yr = yrz[inz].
for each zsmsep where zsmsep.cono = g-cono and zsmsep.yr = x-yr
             USE-INDEX k-prod no-lock:

  zelection_type = "p".
  zelection_char4 = substring (zsmsep.prodcat,1,4).
  zelection_cust = 0.
  run zelectioncheck.
  if zelection_good = false then
    next.

  zelection_type = "x".
  zelection_char4 = substring (zsmsep.prodcat,1,4).
  zelection_cust = 0.
  run zelectioncheck.
  if zelection_good = false then
    next.
  zelection_type = "c".
  zelection_char4 = "".
  zelection_cust = zsmsep.custno.
  run zelectioncheck.
  if zelection_good = false then
    next.

find arsc where arsc.cono = g-cono and
                arsc.custno = zsmsep.custno no-lock no-error.
if avail arsc then do:
  
  if i-div <> 0 then do:
     if i-div = 30 and arsc.divno <> 30 then next.
     if i-div = 40 and arsc.divno <> 40 then next.
  end.

  assign
    x-slsm = arsc.slsrepout
    x-state = arsc.state.
end.


if zsmsep.shipto <> "" then
  do:
  find arss where arss.cono = g-cono and
                arss.custno = zsmsep.custno and
                arss.shipto = zsmsep.shipto no-lock no-error.
  if avail arss then
    do:
    x-slsm = arss.slsrepout.
    x-state = arss.state.
    end.
  end.

  if i-state <> "" then
    if i-state <> x-state then
      next.
  
  
  find smsn where smsn.cono = g-cono and smsn.slsrep = x-slsm
    no-lock no-error.
  if avail smsn then
    do:
    assign
      x-reg  = substring(smsn.mgr,1,1)
      x-dist = substring(smsn.mgr,2,3).
    end.
  else
    assign
      x-reg  = "z"
      x-dist = "999".


  if i-rgn <> "" then
    if x-reg <> i-rgn then
      next.
  if i-dsrt <> "" then
    if x-dist <> i-dsrt then
      next.

  if x-slsm < smnbg or
     x-slsm > smned then
    next.
  if i-list = true then
    do:
    zelection_type = "s".
    zelection_char4 = x-slsm.
    zelection_cust = 0.
    run zelectioncheck.
    if zelection_good = false then
      next.
    end.


  assign v-sls = 0 v-mgn = 0.
  do i = 1 to e-mo:
    v-val = if i-costonly = true then zsmsep.cogamt[i] else zsmsep.salesamt[i].
    assign v-sls = v-sls + v-val
           v-mgn = v-mgn + (v-val - zsmsep.cogamt[i]).
  end.
  j = 0.  /* report creation counter */
        
  /* find & store in report file for later sort & print */
   find first tdetail where  
              tdetail.region   = (if (i-type = "C "
                                   or i-type = "ST" 
                                   or i-type = "CP") then "" else x-reg)
          and tdetail.district = (if (i-type = "R " or i-type = "C "
                                   or i-type = "CP")
                                      then ""
                                  else if i-type = "ST" then
                                      x-state
                                  else x-dist)
          and tdetail.rep      = (if (i-type = "s " or i-type = "sc")
                                  then x-slsm else "")
          and tdetail.cust      = (if (i-type = "cc" or i-type = "sc"
                                      or i-type = "ST" or i-type = "CP")
                                  then zsmsep.custno else 0)
          and tdetail.cat      =  zsmsep.prodcat
        exclusive-lock no-error.
   if not avail tdetail then do:
          create tdetail.
          assign
              tdetail.region   = (if (i-type = "C " 
                                   or i-type = "ST"
                                   or i-type = "CP") then "" else x-reg)
              tdetail.district = (if (i-type = "R " or i-type = "C " or
                                      i-type = "CP")
                                   then ""
                                 else if i-type = "ST" then
                                   x-state
                                 else x-dist)
              tdetail.rep      = if (i-type = "S " or i-type = "sc")
                                 then x-slsm else ""
              tdetail.cust      = (if (i-type = "cc" or i-type = "sc"
                                    or i-type = "ST" or i-type = "CP")
                                   then zsmsep.custno else 0)
              tdetail.name      = (if (i-type = "cc" or i-type = "sc")
                                  then arsc.lookupnm else "")

              tdetail.cat      = zsmsep.prodcat
              tdetail.prntfl   = if substring(zsmsep.prodcat,5,1) <> "~~" then
                                   false
                                 else
                                   true
              j =  1. 
        end.
    if x-yr = e-yr then
        do:
        v-val = if i-costonly = true then zsmsep.cogamt[e-mo]
                else zsmsep.salesamt[e-mo].
        assign                 
          tdetail.ptdsls    = tdetail.ptdsls + v-val
          tdetail.ytdsls    = tdetail.ytdsls + v-sls
          tdetail.ptdmgn    = tdetail.ptdmgn + 
                               (v-val - zsmsep.cogamt[e-mo])
          tdetail.ytdmgn    = tdetail.ytdmgn + v-mgn.
        end.
    else
        do:
        v-val = if i-costonly = true then zsmsep.cogamt[e-mo]
                else zsmsep.salesamt[e-mo].
    
        assign
          tdetail.lyptdsls  = tdetail.lyptdsls + v-val
          tdetail.lyptdmgn  = tdetail.lyptdmgn + 
                              (v-val - zsmsep.cogamt[e-mo])
          tdetail.lyytdsls  = tdetail.lyytdsls + v-sls
          tdetail.lyytdmgn  = tdetail.lyytdmgn + v-mgn.
        end.

    /* update temp sorting table with ytd sales */
    find tsort use-index k-prime where 
           tsort.region = (if (i-type = "C " 
                            or i-type = "ST" 
                            or i-type = "CP") then "" else x-reg)
           and tsort.district = (if (i-type = "C " or i-type = "R "
                                  or i-type = "CP")
                                   then "" 
                                 else if i-type = "ST" then
                                   x-state
                                 else x-dist)
           and tsort.rep = (if (i-type = "S " or i-type = "sc")
                              then x-slsm
                           else "")
           and tsort.cust  = (if (i-type = "cc" or i-type = "sc"
                               or i-type = "ST" or i-type = "CP")
                                   then zsmsep.custno else 0)
           and tsort.cat = substr(zsmsep.prodcat,1,3)
        exclusive-lock no-error.
    if not avail tsort then do:
         create tsort.
         assign
           tsort.region = (if (i-type = "C " 
                            or i-type = "ST"
                            or i-type = "CP") then "" else x-reg)
           tsort.district = if (i-type = "C " or i-type = "R "
                             or i-type = "CP")
                                then ""
                             else if i-type = "ST" then
                                 x-state
                             else x-dist
           tsort.rep = if (i-type = "S " or i-type = "sc")
                           then x-slsm
                        else ""
           tsort.cust      = (if (i-type = "cc" or i-type = "sc"
                               or i-type = "ST" or i-type = "CP")
                                   then zsmsep.custno else 0)
    
           tsort.name      = (if (i-type = "cc" or i-type = "sc"
                                 or i-type = "st" or i-type = "cp")
                                  then arsc.lookupnm else "")
          
           tsort.vend = substr(zsmsep.prodcat,2,2)
           tsort.cat = substr(zsmsep.prodcat,1,3).
        end.
    if x-yr = e-yr then
        assign tsort.ytd = tsort.ytd + v-sls
               tsort.cnt = tsort.cnt + j.
    
    else
        assign tsort.cnt = tsort.cnt + j.


    /* update temp sorting table with ytd sales */
    if (i-type = "cc" or i-type = "sc"
        or i-type = "st" or i-type = "cp") then 
      do:
      find tdsort use-index k-dprime where
           tdsort.region = (if (i-type = "C "
                             or i-type = "CP" 
                             or i-type = "ST") then "" else x-reg)
           and tdsort.district = (if (i-type = "C " or i-type = "R "
                                   or i-type = "CP")
                                     then "" 
                                 else if i-type = "ST" then
                                     x-state
                                 else x-dist)
           and tdsort.rep = (if (i-type = "S " or i-type = "sc")
                              then x-slsm
                           else "")
           and tdsort.cust  = if (i-type = "cc" or i-type = "sc"
                               or i-type = "ST" or i-type = "CP")
                                   then zsmsep.custno else 0
           
        exclusive-lock no-error.
      if not avail tdsort then do:
         create tdsort.
         assign
           tdsort.region = (if (i-type = "C " 
                             or i-type = "CP" 
                             or i-type = "ST") then "" else x-reg)
           tdsort.district = if (i-type = "C " or i-type = "R "
                              or i-type = "CP")
                                 then "" 
                             else if i-type = "ST" then
                                 x-state
                             else x-dist
           tdsort.rep = if (i-type = "S " or i-type = "sc")
                           then x-slsm
                        else ""
           tdsort.cust      = if (i-type = "cc" or i-type = "sc"
                               or i-type = "ST" or i-type = "CP")
                                   then zsmsep.custno else 0.
    
           
        end.
      if x-yr = e-yr then
        assign tdsort.dytd = tdsort.dytd + v-sls.
      else
        assign tdsort.lytd = tdsort.lytd + v-sls.
     end.
  
   end.

end.

if i-type = "cc" or i-type = "sc"  or i-type = "CP" or
   i-type = "ST" then
  do:
  for each tdsort use-index k-dprime:
    for each tsort use-index k-prime where
      tsort.region = tdsort.region    and
      tsort.district = tdsort.district  and
      tsort.rep = tdsort.rep    and
      tsort.cust = tdsort.cust:
    tsort.dytd = tdsort.dytd.
    tsort.lytd = tdsort.lytd.
    end.
  end.
end.

for each tdetail use-index k-dprime where
       substring(tdetail.cat,1,5) <> z-pcsn
       
       and (tdetail.ptdsls = 0 
       and tdetail.lyptdsls = 0
       and tdetail.ytdsls = 0 
       and tdetail.lyytdsls = 0
       and tdetail.ptdmgn = 0 
       and tdetail.lyptdmgn = 0
       and tdetail.ytdmgn = 0 
       and tdetail.lyytdmgn = 0):
   delete tdetail.
end.

if sapb.optvalue [17] = "c" then
  do:
  for each b-tdetail use-index k-cater
    where substring(b-tdetail.cat,5,1) = "~~" break by b-tdetail.cat:
    if last-of(b-tdetail.cat) then
      do:
      if can-find(first tdetail where substring(tdetail.cat,1,4) =
                                    substring(b-tdetail.cat,1,4) and
                                    substring(tdetail.cat,5,1) <> "~~") then
        next.
      else
        delete b-tdetail.
      end.
  end.
end.                                    
         
         
         
 






/*
view frame f-hd.
display with frame kheadings1 down width 178.
*/
newreport = true.    
n-cnt = 0.    
Report-loop:
for each tsort use-index k-sales  
    no-lock
    break by tsort.region by tsort.district by tsort.rep
    by tsort.dytd descending
    by tsort.cust
    by tsort.ytd descending by tsort.vend:
    
    if first-of (tsort.cust) then
      assign validcustinfo = false
             printcust = true.
    else  
      printcust = false.   
        
    assign
      j = 0.
    if i-type <> "ST"  then
      n-cnt = n-cnt + 1.
    v-avoid = true.
    for each tdetail use-index k-dprime where
            tdetail.region   = tsort.region
        and tdetail.district = tsort.district
        and tdetail.rep      = tsort.rep
        and tdetail.cust     = tsort.cust
        and substr(tdetail.cat,1,3) = tsort.cat 
        and substr(tdetail.cat,1,5) <> z-pcsn 
        and (tdetail.ptdsls <> 0 
        or tdetail.lyptdsls <> 0
        or tdetail.ytdsls <> 0 
        or tdetail.lyytdsls <> 0
        or tdetail.ptdmgn <> 0 
        or tdetail.lyptdmgn <> 0
        or tdetail.ytdmgn <> 0 
        or tdetail.lyytdmgn <> 0)  
         
         
      /*  tdetail.prntfl = true   */
        exclusive-lock:
    
      /*
      if substring(tdetail.cat,1,5) <> "~~" then  
        do:
        {z-smrzk2.i z-cat tdetail.cat z-tild}  
        find b-tdetail use-index k-dprime where
            b-tdetail.region   = tdetail.region
        and b-tdetail.district = tdetail.district
        and b-tdetail.rep      = tdetail.rep
        and b-tdetail.cust     = tdetail.cust
        and b-tdetail.cat      =  z-cat
        and (tdetail.ptdsls <> 0 
        or tdetail.lyptdsls <> 0
        or tdetail.ytdsls <> 0 
        or tdetail.lyytdsls <> 0)
        exclusive-lock no-error.
        if avail b-tdetail then
          b-tdetail.prntfl = true.
        end.  
        
      */   
         
        
        
        v-avoid = false.

        find smsn where smsn.cono = g-cono and
                        smsn.slsrep = tsort.rep no-lock no-error.
        narr2 = "".
        if not avail smsn  and i-type = "S " then
           narr2 = "Name not found".
        else
        if i-type = "S " then 
           narr2 = smsn.name.

        find sasta use-index k-sasta where sasta.cono = g-cono
            and sasta.codeiden = "C"
            and sasta.codeval = substring(tdetail.cat,1,4) no-lock no-error.
        assign p-desc = if avail sasta then sasta.descrip
                        else "No SASTA Descrip".

        assign 
            c-pct = if tdetail.ptdsls = 0 then 0
                  else if tdetail.ptdmgn = 0 then 0
                  else (tdetail.ptdmgn / tdetail.ptdsls) * 100
            l-pct = if tdetail.lyptdsls = 0 then 0
                  else if tdetail.lyptdmgn = 0 then 0
                  else (tdetail.lyptdmgn / tdetail.lyptdsls) * 100
            p-diff = tdetail.ptdsls - tdetail.lyptdsls
            p-pdif = if tdetail.lyptdsls = 0 then 100
                     else (p-diff / tdetail.lyptdsls) * 100
            p-mdif = c-pct - l-pct
            m-ytd = if tdetail.ytdsls = 0 then 0
                  else if tdetail.ytdmgn = 0 then 0
                  else (tdetail.ytdmgn / tdetail.ytdsls) * 100
            m-last = if tdetail.lyytdsls = 0 then 0
                  else if tdetail.lyytdmgn = 0 then 0
                  else (tdetail.lyytdmgn / tdetail.lyytdsls) * 100
            s-diff = tdetail.ytdsls - tdetail.lyytdsls
            s-pdif = if tdetail.ytdsls = 0 then 100
                     else (s-diff / tdetail.ytdsls) * 100
            s-mdif = m-ytd - m-last.
            assign
            p-sls[1] = p-sls[1] + tdetail.ptdsls        
            p-sls[2] = p-sls[2] + tdetail.lyptdsls
            p-sls[3] = p-sls[3] + tdetail.ytdsls
            p-sls[4] = p-sls[4] + tdetail.lyytdsls.
            assign
            p-mgn[1] = p-mgn[1] + tdetail.ptdmgn
            p-mgn[2] = p-mgn[2] + tdetail.lyptdmgn
            p-mgn[3] = p-mgn[3] + tdetail.ytdmgn
            p-mgn[4] = p-mgn[4] + tdetail.lyytdmgn.
        

         if (line-counter + 4) > page-size or newreport then
            do:
            run headers.
            end.
        skipdet = false.

     
        if (i-sort = "S" and n-cnt > i-cnt
           or i-type = "ST") then
          do:
          j = 0.
          end.
        else if (tdetail.ptdsls = 0 and tdetail.lyptdsls = 0 and
                 tdetail.ytdsls = 0 and tdetail.lyytdsls = 0 and 
                 tdetail.ptdmgn = 0 and tdetail.lyptdmgn = 0 and
                 tdetail.ytdmgn = 0 and tdetail.lyytdmgn = 0) then 

          do:
          skipdet = true.
          end.      
        else do:
           j = j + 1.
           if printcust  and
             (i-type = "sc" or i-type  = "cc" or
              i-type = "cp") then
              do:
              printcust = false.
              validcustinfo = true.
              display tsort.cust tsort.name with frame f-custh.
              end.
            skipdet = false.
            if c-pct > 999 then
               c-pct = 999.
            else
            if c-pct < -999 then
              c-pct = -999.
            if l-pct > 999 then
               l-pct = 999.
            else
            if l-pct < -999 then
              l-pct = -999.
            if p-pdif > 999 then
               p-pdif = 999.
            else
            if p-pdif < -999 then
              p-pdif = -999.
            if p-mdif > 999 then
               p-mdif = 999.
            else
            if p-mdif < -999 then
              p-mdif = -999.
            if m-ytd > 999 then
               m-ytd = 999.
            else
            if m-ytd < -999 then
              m-ytd = -999.
            if m-last > 999 then
               m-last = 999.
            else
            if m-last < -999 then
              m-last = -999.
            if s-pdif > 999 then
               s-pdif = 999.
            else
            if s-pdif < -999 then
              s-pdif = -999.
            if s-mdif > 999 then
               s-mdif = 999.
            else
            if s-mdif < -999 then
              s-mdif = -999.
            
            x-val = (if substring(tdetail.cat,5,1) = "~~" then
                      "   "
                    else
                    if substring(tdetail.cat,5,1) = "#" then
                      "-KP"
                    else
                    if substring(tdetail.cat,5,1) = "^" then
                      "-VA"
                    else
                    if substring(tdetail.cat,5,1) = "@" then
                      "-BD"
                   else
                      "   ").
            x-cat = substring(tdetail.cat,1,4).
            display x-cat  x-val
                p-desc tdetail.ptdsls c-pct tdetail.lyptdsls l-pct
                p-diff p-pdif p-mdif tdetail.ytdsls m-ytd tdetail.lyytdsls 
                m-last s-diff s-pdif s-mdif with frame f-disp.
        end.    
        
    end.  /* report tdetail */

    
    
    if (i-sort = "S" and n-cnt > i-cnt or i-type = "ST")
        then
        do:
        skipdet = true.
        assign i = i.
        end.
    else do:
        assign 
        n-desc   = if (i-sort = "S" and n-cnt > i-cnt) then "All Other"
                   else "Subtotal"
        h-sls[5] = p-sls[1] - p-sls[2]
        h-mgn[5] = if p-sls[5] = 0 then 0
                   else if p-sls[2] = 0 then 100
                   else (p-sls[5] / p-sls[2]) * 100
        h-sls[6] = p-sls[3] - p-sls[4]
        h-mgn[6] = if p-sls[6] = 0 then 0
                   else if p-sls[4] = 0 then 0
                   else (p-sls[6] / p-sls[4]) * 100
     
        h-mgn[7] = p-mgn[1] - p-mgn[2]
        h-mgn[8] = p-mgn[3] - p-mgn[4].

        do i = 1 to 4:
          assign
             h-mgn[i] = if p-sls[i] = 0 then 0
                        else if p-mgn[i] = 0 then 0
                        else (p-mgn[i] / p-sls[i]) * 100.
        end.

        
        h-mgn[7] = h-mgn[1] - h-mgn[2].
        h-mgn[8] = h-mgn[3] - h-mgn[4].

 
        
        do i = 1 to 8:
          if h-mgn[i] > 999 then
            h-mgn[i] = 999.
          else
          if h-mgn[i] < -999 then
            h-mgn[i] = -999.
        end.
       

        if j > 1 then
            do:
            display n-desc
                p-sls[1] h-mgn[1] p-sls[2] h-mgn[2] h-sls[5] h-mgn[5] h-mgn[7]
                p-sls[3] h-mgn[3] p-sls[4] h-mgn[4] h-sls[6] h-mgn[6] h-mgn[8]
                with frame f-sub.
            display "~015" with frame f-skip2 no-box no-labels width 178.
            end.
        else
        if j <> 0 then
          display "~015" with frame f-skip no-box no-labels width 178.
        
       
        do i = 1 to 4:
          assign r-sls[i] = r-sls[i] + p-sls[i]
               r-mgn[i] = r-mgn[i] + p-mgn[i]
               d-sls[i] = d-sls[i] + p-sls[i]
               d-mgn[i] = d-mgn[i] + p-mgn[i]
               t-sls[i] = t-sls[i] + p-sls[i]
               t-mgn[i] = t-mgn[i] + p-mgn[i]
               f-sls[i] = f-sls[i] + p-sls[i]
               f-mgn[i] = f-mgn[i] + p-mgn[i]
               c-sls[i] = c-sls[i] + p-sls[i]
               c-mgn[i] = c-mgn[i] + p-mgn[i]
               p-mgn[i] = if p-sls[i] = 0 then 0
                          else if p-mgn[i] = 0 then 0
                          else (p-mgn[i] / p-sls[i]) * 100.
        end.

        
        do i = 1 to 6:
            assign p-sls[i] = 0
                   p-mgn[i] = 0.
        end.            
    end.    
 
    if (last-of(tsort.cust) and i-type = "st" and
        n-cnt ge i-cnt) then
        n-cnt = n-cnt + 1.
    
    if (last-of(tsort.cust) and
       (i-type = "SC" or i-type = "cc" or
        i-type = "CP" or 
        (i-type = "ST" and n-cnt < i-cnt))) then do:
        if i-type = "st" or 
          (i-type = "cp" and i-cnt = 0) then
          do:
          if (p-sls[1] <> 0 or p-sls[2] <> 0 or
              p-sls[3] <> 0 or p-sls[4] <> 0 or 
              p-mgn[1] <> 0 or p-mgn[2] <> 0 or
              p-mgn[3] <> 0 or p-mgn[4] <> 0) then

             do:
             validcustinfo = true.  
             n-cnt = n-cnt + 1.
             end.
          end.
        if i-sort = "s" and n-cnt > i-cnt
          and (i-type <> "st" and
               i-type = "cp" and i-cnt > 0) then
        do:
        assign 
        n-desc   = if (i-sort = "S" and n-cnt > i-cnt) then "All Other"
                   else "Subtotal"
        h-sls[5] = p-sls[1] - p-sls[2]
        h-mgn[5] = if p-sls[5] = 0 then 0
                   else if p-sls[2] = 0 then 0
                   else (p-sls[5] / p-sls[2]) * 100
        h-sls[6] = p-sls[3] - p-sls[4]
        h-mgn[6] = if p-sls[6] = 0 then 0
                   else if p-sls[4] = 0 then 0
                   else (p-sls[6] / p-sls[4]) * 100
        h-mgn[7] = p-mgn[1] - p-mgn[2]
        h-mgn[8] = p-mgn[3] - p-mgn[4].

        do i = 1 to 4:
          assign
             h-mgn[i] = if p-sls[i] = 0 then 0
                        else if p-mgn[i] = 0 then 0
                        else (p-mgn[i] / p-sls[i]) * 100.
        end.

        h-mgn[7] = h-mgn[1] - h-mgn[2].
        h-mgn[8] = h-mgn[3] - h-mgn[4].

 
        
        do i = 1 to 8:
          if h-mgn[i] > 999 then
            h-mgn[i] = 999.
          else
          if h-mgn[i] < -999 then
            h-mgn[i] = -999.
        end.
       
        if  p-sls[1] <> 0 or p-sls[2] <> 0 or h-sls[5] <> 0 or p-sls[3] <> 0  
            or p-sls[4] <> 0 or h-sls[6] <> 0 or
            p-mgn[1] <> 0 or p-mgn[2] <> 0 or h-mgn[5] <> 0 or p-mgn[3] <> 0  
            or p-mgn[4] <> 0 or h-mgn[6] <> 0 then

        display n-desc
              p-sls[1] h-mgn[1] p-sls[2] h-mgn[2] h-sls[5] h-mgn[5] h-mgn[7]
              p-sls[3] h-mgn[3] p-sls[4] h-mgn[4] h-sls[6] h-mgn[6] h-mgn[8]
              with frame f-sub.
              
             
        do i = 1 to 4:
          assign r-sls[i] = r-sls[i] + p-sls[i]
               r-mgn[i] = r-mgn[i] + p-mgn[i]
               d-sls[i] = d-sls[i] + p-sls[i]
               d-mgn[i] = d-mgn[i] + p-mgn[i]
               t-sls[i] = t-sls[i] + p-sls[i]
               t-mgn[i] = t-mgn[i] + p-mgn[i]
               f-sls[i] = f-sls[i] + p-sls[i]
               f-mgn[i] = f-mgn[i] + p-mgn[i]
               c-sls[i] = c-sls[i] + p-sls[i]
               c-mgn[i] = c-mgn[i] + p-mgn[i]
               p-mgn[i] = if p-sls[i] = 0 then 0
                          else if p-mgn[i] = 0 then 0
                          else (p-mgn[i] / p-sls[i]) * 100.
        end.

        
        do i = 1 to 6:
            assign p-sls[i] = 0
                   p-mgn[i] = 0.
        end.            
    end.    
   
    if i-type = "st" or
       (i-type = "cp" and i-cnt = 0) then
      do:           
      do i = 1 to 4:
          assign r-sls[i] = r-sls[i] + p-sls[i]
               r-mgn[i] = r-mgn[i] + p-mgn[i]
               d-sls[i] = d-sls[i] + p-sls[i]
               d-mgn[i] = d-mgn[i] + p-mgn[i]
               t-sls[i] = t-sls[i] + p-sls[i]
               t-mgn[i] = t-mgn[i] + p-mgn[i]
               f-sls[i] = f-sls[i] + p-sls[i]
               f-mgn[i] = f-mgn[i] + p-mgn[i]
               c-sls[i] = c-sls[i] + p-sls[i]
               c-mgn[i] = c-mgn[i] + p-mgn[i]
               p-mgn[i] = if p-sls[i] = 0 then 0
                          else if p-mgn[i] = 0 then 0
                          else (p-mgn[i] / p-sls[i]) * 100.
      end.
                                     
        
      do i = 1 to 6:
          assign p-sls[i] = 0
                 p-mgn[i] = 0.
      
      end.      
    end.    
        
        do i = 1 to 4:
            assign 
                c-mgn[i] = if c-sls[i] = 0 then 0
                          else if c-mgn[i] = 0 then 0
                          else (c-mgn[i] / c-sls[i]) * 100.
        end.
        assign 
            c-sls[5] = c-sls[1] - c-sls[2]
            c-mgn[5] = if c-sls[5] = 0 then 0
                       else if c-sls[2] = 0 then 0
                       else (c-sls[5] / c-sls[2]) * 100
            c-sls[6] = c-sls[3] - c-sls[4]
            c-mgn[6] = if c-sls[6] = 0 then 0
                       else if c-sls[4] = 0 then 0
                       else (c-sls[6] / c-sls[4]) * 100
            c-mgn[7] = c-mgn[1] - c-mgn[2]
            r-mgn[8] = r-mgn[3] - r-mgn[4].
            

        do i = 1 to 8:
          if c-mgn[i] > 999 then
            c-mgn[i] = 999.
          else
          if c-mgn[i] < -999 then
            c-mgn[i] = -999.
        end.
      
        if validcustinfo then

        display tsort.cust tsort.name
                c-sls[1] c-mgn[1] c-sls[2] c-mgn[2] c-sls[5] c-mgn[5]
                c-mgn[7] c-sls[3] c-mgn[3] c-sls[4] c-mgn[4] c-sls[6] 
                c-mgn[6] c-mgn[8]
                with frame f-cust.
            
        do i = 1 to 6:
            assign c-sls[i] = 0
                   c-mgn[i] = 0.
        end.
      if i-type <> "st" then  
        n-cnt = 0.
    end.
   
          
    if (last-of(tsort.rep) and
       (i-type = "S " or i-type = "sc")) then do:
        if i-sort = "s" and n-cnt > i-cnt then
        do:
        assign 
        n-desc   = if (i-sort = "S" and n-cnt > i-cnt) then "All Other"
                   else "Subtotal"
        h-sls[5] = p-sls[1] - p-sls[2]
        h-mgn[5] = if p-sls[5] = 0 then 0
                   else if p-sls[2] = 0 then 0
                   else (p-sls[5] / p-sls[2]) * 100
        h-sls[6] = p-sls[3] - p-sls[4]
        h-mgn[6] = if p-sls[6] = 0 then 0
                   else if p-sls[4] = 0 then 0
                   else (p-sls[6] / p-sls[4]) * 100
        h-mgn[7] = p-mgn[1] - p-mgn[2]
        h-mgn[8] = p-mgn[3] - p-mgn[4].

        do i = 1 to 4:
          assign
             h-mgn[i] = if p-sls[i] = 0 then 0
                        else if p-mgn[i] = 0 then 0
                        else (p-mgn[i] / p-sls[i]) * 100.
        end.

        h-mgn[7] = h-mgn[1] - h-mgn[2].
        h-mgn[8] = h-mgn[3] - h-mgn[4].

 
        
        do i = 1 to 8:
          if h-mgn[i] > 999 then
            h-mgn[i] = 999.
          else
          if h-mgn[i] < -999 then
            h-mgn[i] = -999.
        end.
       
        if  p-sls[1] <> 0 or p-sls[2] <> 0 or h-sls[5] <> 0 or p-sls[3] <> 0 or
            p-sls[4] <> 0 or h-sls[6] <> 0 or
            p-mgn[1] <> 0 or p-mgn[2] <> 0 or h-mgn[5] <> 0 or p-mgn[3] <> 0 or
            p-mgn[4] <> 0 or h-mgn[6] <> 0 then
          
          display n-desc
              p-sls[1] h-mgn[1] p-sls[2] h-mgn[2] h-sls[5] h-mgn[5] h-mgn[7]
              p-sls[3] h-mgn[3] p-sls[4] h-mgn[4] h-sls[6] h-mgn[6] h-mgn[8]
              with frame f-sub.
              
             
        do i = 1 to 4:
          assign r-sls[i] = r-sls[i] + p-sls[i]
               r-mgn[i] = r-mgn[i] + p-mgn[i]
               d-sls[i] = d-sls[i] + p-sls[i]
               d-mgn[i] = d-mgn[i] + p-mgn[i]
               t-sls[i] = t-sls[i] + p-sls[i]
               t-mgn[i] = t-mgn[i] + p-mgn[i]
               f-sls[i] = f-sls[i] + p-sls[i]
               f-mgn[i] = f-mgn[i] + p-mgn[i]
               c-sls[i] = c-sls[i] + p-sls[i]
               c-mgn[i] = c-mgn[i] + p-mgn[i]
                    p-mgn[i] = if p-sls[i] = 0 then 0
                          else if p-mgn[i] = 0 then 0
                          else (p-mgn[i] / p-sls[i]) * 100.
        end.

        
        do i = 1 to 6:
            assign p-sls[i] = 0
                   p-mgn[i] = 0.
        end.            
    end.    
        do i = 1 to 4:
            assign 
                r-mgn[i] = if r-sls[i] = 0 then 0
                          else if r-mgn[i] = 0 then 0
                          else (r-mgn[i] / r-sls[i]) * 100.
        end.
        assign 
            r-sls[5] = r-sls[1] - r-sls[2]
            r-mgn[5] = if r-sls[5] = 0 then 0
                       else if r-sls[2] = 0 then 0
                       else (r-sls[5] / r-sls[2]) * 100
            r-sls[6] = r-sls[3] - r-sls[4]
            r-mgn[6] = if r-sls[6] = 0 then 0
                       else if r-sls[4] = 0 then 0
                       else (r-sls[6] / r-sls[4]) * 100
            r-mgn[7] = r-mgn[1] - r-mgn[2]
            r-mgn[8] = r-mgn[3] - r-mgn[4].
            

        do i = 1 to 8:
          if r-mgn[i] > 999 then
            r-mgn[i] = 999.
          else
          if r-mgn[i] < -999 then
            r-mgn[i] = -999.
        end.
      
       if  r-sls[1] <> 0 or r-sls[2] <> 0 or r-sls[5] <> 0 or
           r-sls[3] <> 0 or r-sls[4] <> 0 or r-sls[6] <> 0 or
           r-mgn[1] <> 0 or r-mgn[2] <> 0 or r-mgn[5] <> 0 or
           r-mgn[3] <> 0 or r-mgn[4] <> 0 or r-mgn[6] <> 0 then
              
        
        display tsort.rep
                r-sls[1] r-mgn[1] r-sls[2] r-mgn[2] r-sls[5] r-mgn[5]
                r-mgn[7] r-sls[3] r-mgn[3] r-sls[4] r-mgn[4] r-sls[6] 
                r-mgn[6] r-mgn[8]
                with frame f-rep.
            
        do i = 1 to 6:
            assign r-sls[i] = 0
                   r-mgn[i] = 0.
        end.
        n-cnt = 0.
        newreport = true.
      end.
   
    if last-of(tsort.district)  and (i-type = "d " or i-type = "ST"
       or i-type = "S " or i-type = "cc" or i-type = "sc") then do:
       if i-sort = "s" and n-cnt > i-cnt then do:
        assign 
        n-desc   = if (i-sort = "S" and n-cnt > i-cnt) then "All Other"
                   else "Subtotal"
        h-sls[5] = p-sls[1] - p-sls[2]
        h-mgn[5] = if p-sls[5] = 0 then 0
                   else if p-sls[2] = 0 then 0
                   else (p-sls[5] / p-sls[2]) * 100
        h-sls[6] = p-sls[3] - p-sls[4]
        h-mgn[6] = if p-sls[6] = 0 then 0
                   else if p-sls[4] = 0 then 0
                   else (p-sls[6] / p-sls[4]) * 100
        h-mgn[7] = p-mgn[1] - p-mgn[2]
        h-mgn[8] = p-mgn[3] - p-mgn[4].

        do i = 1 to 4:
          assign
             h-mgn[i] = if p-sls[i] = 0 then 0
                        else if p-mgn[i] = 0 then 0
                        else (p-mgn[i] / p-sls[i]) * 100.
        end.
 
        h-mgn[7] = h-mgn[1] - h-mgn[2].
        h-mgn[8] = h-mgn[3] - h-mgn[4].

         
        do i = 1 to 8:
          if h-mgn[i] > 999 then
            h-mgn[i] = 999.
          else
          if h-mgn[i] < -999 then
            h-mgn[i] = -999.
        end.
       
        if p-sls[1] <> 0 or p-sls[2] <> 0 or h-sls[5] <> 0 or
           p-sls[3] <> 0 or p-sls[4] <> 0 or h-sls[6] <> 0 or 
           p-mgn[1] <> 0 or p-mgn[2] <> 0 or h-mgn[5] <> 0 or
           p-mgn[3] <> 0 or p-mgn[4] <> 0 or h-mgn[6] <> 0 then 
         
 
         display n-desc
              p-sls[1] h-mgn[1] p-sls[2] h-mgn[2] h-sls[5] h-mgn[5] h-mgn[7]
              p-sls[3] h-mgn[3] p-sls[4] h-mgn[4] h-sls[6] h-mgn[6] h-mgn[8]
              with frame f-sub.
              
         
        
       
        do i = 1 to 4:
          assign r-sls[i] = r-sls[i] + p-sls[i]
               r-mgn[i] = r-mgn[i] + p-mgn[i]
               d-sls[i] = d-sls[i] + p-sls[i]
               d-mgn[i] = d-mgn[i] + p-mgn[i]
               t-sls[i] = t-sls[i] + p-sls[i]
               t-mgn[i] = t-mgn[i] + p-mgn[i]
               f-sls[i] = f-sls[i] + p-sls[i]
               f-mgn[i] = f-mgn[i] + p-mgn[i]
               c-sls[i] = c-sls[i] + p-sls[i]
               c-mgn[i] = c-mgn[i] + p-mgn[i]
               p-mgn[i] = if p-sls[i] = 0 then 0
                          else if p-mgn[i] = 0 then 0
                          else (p-mgn[i] / p-sls[i]) * 100.
        end.

        do i = 1 to 6:
            assign p-sls[i] = 0
                   p-mgn[i] = 0.
        end.            
    end.    
 

        do i = 1 to 4:
            assign 
                d-mgn[i] = if d-sls[i] = 0 then 0
                          else if d-mgn[i] = 0 then 0
                          else (d-mgn[i] / d-sls[i]) * 100.
        end.
        assign 
            d-sls[5] = d-sls[1] - d-sls[2]
            d-mgn[5] = if d-sls[5] = 0 then 0
                       else if d-sls[2] = 0 then 0
                       else (d-sls[5] / d-sls[2]) * 100
            d-sls[6] = d-sls[3] - d-sls[4]
            d-mgn[6] = if d-sls[6] = 0 then 0
                       else if d-sls[4] = 0 then 0
                       else (d-sls[6] / d-sls[4]) * 100
            d-mgn[7] = d-mgn[1] - d-mgn[2]
            d-mgn[8] = d-mgn[3] - d-mgn[4].

        do i = 1 to 8:
          if d-mgn[i] > 999 then
            d-mgn[i] = 999.
          else
          if d-mgn[i] < -999 then
            d-mgn[i] = -999.
        end.
       
        if i-type <> "ST" then
          do:
          if (d-sls[1] <> 0 or d-sls[2] <> 0 or d-sls[5] <> 0 or
             d-sls[3] <> 0 or d-sls[4] <> 0 or d-sls[6] <> 0 or
             d-mgn[1] <> 0 or d-mgn[2] <> 0 or d-mgn[5] <> 0 or
             d-mgn[3] <> 0 or d-mgn[4] <> 0 or d-mgn[6] <> 0) then
          display
                "District Ttl" @ distx
                tsort.district
                d-sls[1] d-mgn[1] d-sls[2] d-mgn[2] d-sls[5] d-mgn[5]
                d-mgn[7] d-sls[3] d-mgn[3] d-sls[4] d-mgn[4] d-sls[6] 
                d-mgn[6] d-mgn[8]
                with frame f-dstrct.
          end.
         else
           do:
           if (d-sls[1] <> 0 or d-sls[2] <> 0 or d-sls[5] <> 0 or
               d-sls[3] <> 0 or d-sls[4] <> 0 or d-sls[6] <> 0 or
               d-mgn[1] <> 0 or d-mgn[2] <> 0 or d-mgn[5] <> 0 or
               d-mgn[3] <> 0 or d-mgn[4] <> 0 or d-mgn[6] <> 0) then
            do:
             display "~015" with frame f-skipa no-box no-labels width 178.
             display
                "State Ttl" @ distx
                tsort.district
                d-sls[1] d-mgn[1] d-sls[2] d-mgn[2] d-sls[5] d-mgn[5]
                d-mgn[7] d-sls[3] d-mgn[3] d-sls[4] d-mgn[4] d-sls[6] 
                d-mgn[6] d-mgn[8]
                with frame f-dstrct.
            
             display "~015" with frame f-skipb no-box no-labels width 178.
             end.
           do i = 1 to 6:
            assign c-sls[i] = 0
                   c-mgn[i] = 0.
           end.  
           
           
           end.
  
        do i = 1 to 6:
            assign d-sls[i] = 0
                   d-mgn[i] = 0.
        if i-type <> "ST" then
         newreport = true.
        end.
    n-cnt = 0.
    end.
    /*
    if (last-of(tsort.district) and i-sort = "D") then page.
    */
    if last-of(tsort.region) and i-type <> "C " 
      and i-type <> "ST" and i-type <> "CP" then do:
        if i-sort = "s" and n-cnt > i-cnt then
        do:
        assign 
        n-desc   = if (i-sort = "S" and n-cnt > i-cnt) then "All Other"
                   else "Subtotal"
        h-sls[5] = p-sls[1] - p-sls[2]
        h-mgn[5] = if p-sls[5] = 0 then 0
                   else if p-sls[2] = 0 then 0
                   else (p-sls[5] / p-sls[2]) * 100
        h-sls[6] = p-sls[3] - p-sls[4]
        h-mgn[6] = if p-sls[6] = 0 then 0
                   else if p-sls[4] = 0 then 0
                   else (p-sls[6] / p-sls[4]) * 100
        h-mgn[7] = p-mgn[1] - p-mgn[2]
        h-mgn[8] = p-mgn[3] - p-mgn[4].

        do i = 1 to 4:
          assign
             h-mgn[i] = if p-sls[i] = 0 then 0
                        else if p-mgn[i] = 0 then 0
                        else (p-mgn[i] / p-sls[i]) * 100.
        end.

        h-mgn[7] = h-mgn[1] - h-mgn[2].
        h-mgn[8] = h-mgn[3] - h-mgn[4].

 
        do i = 1 to 8:
          if h-mgn[i] > 999 then
            h-mgn[i] = 999.
          else
          if h-mgn[i] < -999 then
            h-mgn[i] = -999.
        end.
       
        if p-sls[1] <> 0 or p-sls[2] <> 0 or h-sls[5] <> 0 or
           p-sls[3] <> 0 or p-sls[4] <> 0 or h-sls[6] <> 0 or  
           p-mgn[1] <> 0 or p-mgn[2] <> 0 or h-mgn[5] <> 0 or
           p-mgn[3] <> 0 or p-mgn[4] <> 0 or h-mgn[6] <> 0 then
         


         display n-desc
              p-sls[1] h-mgn[1] p-sls[2] h-mgn[2] h-sls[5] h-mgn[5] h-mgn[7]
              p-sls[3] h-mgn[3] p-sls[4] h-mgn[4] h-sls[6] h-mgn[6] h-mgn[8]
              with frame f-sub.
              
              
        do i = 1 to 4:
          assign r-sls[i] = r-sls[i] + p-sls[i]
               r-mgn[i] = r-mgn[i] + p-mgn[i]
               d-sls[i] = d-sls[i] + p-sls[i]
               d-mgn[i] = d-mgn[i] + p-mgn[i]
               t-sls[i] = t-sls[i] + p-sls[i]
               t-mgn[i] = t-mgn[i] + p-mgn[i]
               f-sls[i] = f-sls[i] + p-sls[i]
               f-mgn[i] = f-mgn[i] + p-mgn[i]
               c-sls[i] = c-sls[i] + p-sls[i]
               c-mgn[i] = c-mgn[i] + p-mgn[i]
               p-mgn[i] = if p-sls[i] = 0 then 0
                          else if p-mgn[i] = 0 then 0
                          else (p-mgn[i] / p-sls[i]) * 100.
        end.

        
        do i = 1 to 6:
            assign p-sls[i] = 0
                   p-mgn[i] = 0.
        end.            
    end.    
        do i = 1 to 4:
            assign 
                t-mgn[i] = if t-sls[i] = 0 then 0
                          else if t-mgn[i] = 0 then 0
                          else (t-mgn[i] / t-sls[i]) * 100.
        end.
        assign 
            t-sls[5] = t-sls[1] - t-sls[2]
            t-mgn[5] = if t-sls[5] = 0 then 0
                       else if t-sls[2] = 0 then 0
                       else (t-sls[5] / t-sls[2]) * 100
            t-sls[6] = t-sls[3] - t-sls[4]
            t-mgn[6] = if t-sls[6] = 0 then 0
                       else if t-sls[4] = 0 then 0
                       else (t-sls[6] / t-sls[4]) * 100
            t-mgn[7] = t-mgn[1] - t-mgn[2]
            t-mgn[8] = t-mgn[3] - t-mgn[4].


        do i = 1 to 8:
          if t-mgn[i] > 999 then
            t-mgn[i] = 999.
          else
          if t-mgn[i] < -999 then
            t-mgn[i] = -999.
        end.
       
        if t-sls[1] <> 0 or t-sls[2] <> 0 or t-sls[5] <> 0 or
           t-sls[3] <> 0 or t-sls[4] <> 0 or t-sls[6] <> 0 or
           t-mgn[1] <> 0 or t-mgn[2] <> 0 or t-mgn[5] <> 0 or
           t-mgn[3] <> 0 or t-mgn[4] <> 0 or t-mgn[6] <> 0 then
                          
        display tsort.region 
                t-sls[1] t-mgn[1] t-sls[2] t-mgn[2] t-sls[5] t-mgn[5]
                t-mgn[7] t-sls[3] t-mgn[3] t-sls[4] t-mgn[4] t-sls[6] 
                t-mgn[6] t-mgn[8]
                with frame f-tot.
            
        do i = 1 to 6:
            assign t-sls[i] = 0
                   t-mgn[i] = 0.
        newreport = true.
        end.
     end.
 end.
   do i = 1 to 4:
     assign 
       f-mgn[i] = if f-sls[i] = 0 then 0
                  else if f-mgn[i] = 0 then 0
                  else (f-mgn[i] / f-sls[i]) * 100.
   end.
   assign 
     f-sls[5] = f-sls[1] - f-sls[2]
     f-mgn[5] = if f-sls[5] = 0 then 0
                else if f-sls[2] = 0 then 0
                else (f-sls[5] / f-sls[2]) * 100
     f-sls[6] = f-sls[3] - f-sls[4]
     f-mgn[6] = if f-sls[6] = 0 then 0
                else if f-sls[4] = 0 then 0
                else (f-sls[6] / f-sls[4]) * 100
     f-mgn[7] = f-mgn[1] - f-mgn[2]
     f-mgn[8] = f-mgn[3] - f-mgn[4].


  do i = 1 to 8:
    if f-mgn[i] > 999 then
      f-mgn[i] = 999.
    else
    if f-mgn[i] < -999 then
      f-mgn[i] = -999.
  end.
       
            
  display  
          f-sls[1] f-mgn[1] f-sls[2] f-mgn[2] f-sls[5] f-mgn[5]
          f-mgn[7] f-sls[3] f-mgn[3] f-sls[4] f-mgn[4] f-sls[6] 
          f-mgn[6] f-mgn[8]
          with frame f-final.

 
 
 
PROCEDURE headers.
  newreport = false.
  x-page = x-page + 1.
  page.
  if not i-costonly then
    do:
    if i-type = "d " then                     
      display x-today
            x-time    
            g-cono
            sapb.operinit
            x-page
            with frame f-shd.
    else
      display x-today
            x-time    
            g-cono
            sapb.operinit
            x-page
            with frame f-hd.
    end.
  else
    do:
    if i-type = "d " then                     
      display x-today
            x-time    
            g-cono
            sapb.operinit
            x-page
            with frame f-cshd.
    else
      display x-today
            x-time    
            g-cono
            sapb.operinit
            x-page
            with frame f-chd.
    end.
     
  end.          
{zsapboycheck.i}

procedure create_zsmsep:

 if   (xwo.qtyfl * oeelk.qtyneeded) *
        ((xwo.salesamt / xwo.qtyfl) *
         (oeelk.prodcost / zk-cost)) <> 0 or
          ((xwo.qtyfl * oeelk.qtyneeded) *
                     oeelk.prodcost) <> 0 then
 do:                    
           
   
  {z-smrzk2.i z-cat icsp.prodcat z-hash}

  assign pk_custno = xwo.custno
         pk_shipto = xwo.shipto
         pk_cat    = icsp.prodcat.
  run precheckparams.
  if not pk_good then
    leave.       
  
  find zsmsep where
                    zsmsep.cono = g-cono           and
                    zsmsep.custno = xwo.custno   and
                    zsmsep.shipto = xwo.shipto   and
                    zsmsep.prodcat = z-cat  and
                    zsmsep.yr     = xwo.yr       and
                    zsmsep.whse = xwo.whse no-lock no-error.
  if not avail zsmsep then
    do:
    create zsmsep.
    assign zsmsep.cono = g-cono
           zsmsep.custno = xwo.custno
           zsmsep.shipto = xwo.shipto
           zsmsep.prodcat = z-cat
           zsmsep.yr     = xwo.yr
           zsmsep.whse = xwo.whse.
    end.
    assign
      zsmsep.salesamt[xwo.mnth] = zsmsep.salesamt[xwo.mnth] +
        (xwo.qtyfl * oeelk.qtyneeded) *
        ((xwo.salesamt / xwo.qtyfl) *
         (oeelk.prodcost / zk-cost))
      zsmsep.cogamt[xwo.mnth] = zsmsep.cogamt[xwo.mnth] + 
         ((xwo.qtyfl * oeelk.qtyneeded) *
                     oeelk.prodcost).
    
  
run create_rev_zsmsep.
end.
end.



procedure create_rev_zsmsep:
  if i-compare then
    do:
    if sapb.optvalue[17] = "e" then
      do:
      {z-smrzk2.i z-cat xwo.pcat z-pcsn}
      find zsmsep where
                    zsmsep.cono = g-cono           and
                    zsmsep.custno = xwo.custno   and
                    zsmsep.shipto = xwo.shipto   and
                    zsmsep.prodcat = z-cat  and
                    zsmsep.yr     = xwo.yr       and
                    zsmsep.whse = xwo.whse no-error.
      if not avail zsmsep then
        do:
        create zsmsep.
        assign zsmsep.cono = g-cono
             zsmsep.custno = xwo.custno
             zsmsep.shipto = xwo.shipto
             zsmsep.prodcat = z-cat 
             zsmsep.yr     = xwo.yr
             zsmsep.whse = xwo.whse.
        end.
      end.
    {z-smrzk2.i z-cat xwo.pcat z-tild}
    find zsmsep where
                    zsmsep.cono = g-cono           and
                    zsmsep.custno = xwo.custno   and
                    zsmsep.shipto = xwo.shipto   and
                    zsmsep.prodcat = z-cat  and
                    zsmsep.yr     = xwo.yr       and
                    zsmsep.whse = xwo.whse no-error.
    if not avail zsmsep then
      do:
      create zsmsep.
      assign zsmsep.cono = g-cono
             zsmsep.custno = xwo.custno
             zsmsep.shipto = xwo.shipto
             zsmsep.prodcat = z-cat 
             zsmsep.yr     = xwo.yr
             zsmsep.whse = xwo.whse.
      end.
    assign
      zsmsep.salesamt[xwo.mnth] = zsmsep.salesamt[xwo.mnth] +
        (((xwo.qtyfl * oeelk.qtyneeded) *
        ((xwo.salesamt / xwo.qtyfl) *
         (oeelk.prodcost / zk-cost))) * -1)
      zsmsep.cogamt[xwo.mnth] = zsmsep.cogamt[xwo.mnth] + 
         (((xwo.qtyfl * oeelk.qtyneeded) *
                     oeelk.prodcost) * -1).
    
  
  end.
end.






procedure create_zsmsep2:
  {z-smrzk2.i z-cat smsep.prodcat z-tild}

  assign pk_custno = smsep.custno
         pk_shipto = smsep.shipto
         pk_cat    = smsep.prodcat.
  run precheckparams.
  if not pk_good then
    leave.       
 
  
  
  find zsmsep where 
                    zsmsep.cono = g-cono           and
                    zsmsep.custno = smsep.custno   and
                    zsmsep.shipto = smsep.shipto   and
                    zsmsep.prodcat = z-cat and
                    zsmsep.yr     = smsep.yr       and
                    zsmsep.whse = smsep.whse no-lock no-error.
  if not avail zsmsep then
    do:
    create zsmsep.
    assign zsmsep.cono = g-cono
           zsmsep.custno = smsep.custno
           zsmsep.shipto = smsep.shipto
           zsmsep.prodcat = z-cat
           zsmsep.yr     = smsep.yr
           zsmsep.whse = smsep.whse.
    end.
  do zzx = 1 to 12:
    zsmsep.salesamt[zzx] = zsmsep.salesamt[zzx] +
      (smsep.salesamt[zzx]).
    zsmsep.cogamt[zzx] = zsmsep.cogamt[zzx] + 
      (smsep.cogamt[zzx]).
                          
  end.
end.


procedure create_zsmsep_bod:
  v-rebate = 0.
  for each pder where pder.cono = oeelk.cono and
                      pder.orderno = oeelk.orderno and
                      pder.ordersuf = oeelk.ordersuf and
                      pder.lineno = oeelk.lineno and
                      pder.seqno = oeelk.seqno no-lock:
     v-rebate = v-rebate + (pder.rebateamt * pder.stkqtyship).                
  end.   

  if  ((((oeel.qtyship * oeelk.qtyneeded)  *
      (oeel.price * ((100 - oeel.discpct) / 100) * 
                 oeel.unitconv)) *
       (oeelk.prodcost / zk-cost))         
                           * (if oeel.returnfl then -1 else 1)) <> 0
      or
      (((((oeel.qtyship * oeelk.qtyneeded)  *
       oeel.prodcost) *
       (oeelk.prodcost / zk-cost))         
                           * (if oeel.returnfl then -1 else 1)) 
                           - (v-rebate * oeelk.prodcost / zk-cost)) <> 0 then
  do:                           

     
  
  
  
  
  
  
  
  {z-smrzk2.i z-cat oeelk.prodcat z-atsn}
                 
  assign pk_custno = oeel.custno
         pk_shipto = oeel.shipto
         pk_cat    = oeelk.prodcat.
  run precheckparams.
  if not pk_good then
    leave.       
 
  
  find zsmsep where
                    zsmsep.cono = g-cono           and
                    zsmsep.custno = oeel.custno   and
                    zsmsep.shipto = oeel.shipto   and
                    zsmsep.prodcat = z-cat and
                    zsmsep.yr     = 
                    int(substring(string(year(oeel.invoicedt),"9999"),3,2))  and
                    zsmsep.whse = oeel.whse no-lock no-error.
  if not avail zsmsep then
    do:
    create zsmsep.
    assign zsmsep.cono = g-cono
           zsmsep.custno = oeel.custno
           zsmsep.shipto = oeel.shipto
           zsmsep.prodcat = z-cat
           zsmsep.yr     =
             int(substring(string(year(oeel.invoicedt),"9999"),3,2))  
           zsmsep.whse = oeel.whse.
    end.
   zsmsep.salesamt[month(oeel.invoicedt)] =   
   zsmsep.salesamt[month(oeel.invoicedt)] +
     ((((oeel.qtyship * oeelk.qtyneeded)  *
      (oeel.price * ((100 - oeel.discpct) / 100) * 
                 oeel.unitconv)) *
       (oeelk.prodcost / zk-cost))         
                           * (if oeel.returnfl then -1 else 1)).

    v-rebate = v-rebate * (oeelk.prodcost / zk-cost).

    zsmsep.cogamt[month(oeel.invoicedt)] = 
      zsmsep.cogamt[month(oeel.invoicedt)] + 
      (((((oeel.qtyship * oeelk.qtyneeded)  *
       oeel.prodcost) *
       (oeelk.prodcost / zk-cost))         
                           * (if oeel.returnfl then -1 else 1)) 
                           - v-rebate).

       
      
      
      
      
      
      
/*   
    if oeel.invoicedt ge 01/01/00 and v-rebate <> 0 then
    do:
    message z-cat
            bgdt
            eddt
               ((((oeel.qtyship * oeelk.qtyneeded)  *
      (oeel.price * ((100 - oeel.discpct) / 100) * 
                 oeel.unitconv)) *
       (oeelk.prodcost / zk-cost))         
                           * (if oeel.returnfl then -1 else 1))
          
            
            oeelk.prodcost
            zk-cost
            v-rebate
            zsmsep.salesamt [month(oeel.invoicedt)]
            zsmsep.cogamt [month(oeel.invoicedt)]
            zsmsep.prodcat
            zsmsep.yr
            oeel.invoicedt
            oeelk.seqno
            oeelk.orderno
            oeelk.ordersuf.
    
    pause.        
    end.   
  */  
   run create_rev_zsmsep_bod.
  end.
end.



procedure create_rev_zsmsep_bod:
  if i-compare then do:
  v-rebate = 0.
  for each pder where pder.cono = oeel.cono and
                      pder.orderno = oeel.orderno and
                      pder.ordersuf = oeel.ordersuf and
                      pder.lineno = oeel.lineno 
                      no-lock:
     v-rebate = v-rebate + (pder.rebateamt * pder.stkqtyship).                
  end.                    
  
  if sapb.optvalue[17] = "e" then
    do:
    {z-smrzk2.i z-cat oeel.prodcat z-pcsn}
    find zsmsep where
                    zsmsep.cono = g-cono           and
                    zsmsep.custno = oeel.custno   and
                    zsmsep.shipto = oeel.shipto   and
                    zsmsep.prodcat = z-cat and
                    zsmsep.yr     = 
                    int(substring(string(year(oeel.invoicedt),"9999"),3,2))  and
                    zsmsep.whse = oeel.whse no-error.
    if not avail zsmsep then
      do:
      create zsmsep.
      assign zsmsep.cono = g-cono
           zsmsep.custno = oeel.custno
           zsmsep.shipto = oeel.shipto
           zsmsep.prodcat = z-cat
           zsmsep.yr     =
             int(substring(string(year(oeel.invoicedt),"9999"),3,2))  
           zsmsep.whse = oeel.whse.
      end.
   end.
  {z-smrzk2.i z-cat oeel.prodcat z-tild}
  find zsmsep where
                    zsmsep.cono = g-cono           and
                    zsmsep.custno = oeel.custno   and
                    zsmsep.shipto = oeel.shipto   and
                    zsmsep.prodcat = z-cat and
                    zsmsep.yr     = 
                    int(substring(string(year(oeel.invoicedt),"9999"),3,2))  and
                    zsmsep.whse = oeel.whse no-error.
  if not avail zsmsep then
    do:
    create zsmsep.
    assign zsmsep.cono = g-cono
           zsmsep.custno = oeel.custno
           zsmsep.shipto = oeel.shipto
           zsmsep.prodcat = z-cat
           zsmsep.yr     =
             int(substring(string(year(oeel.invoicedt),"9999"),3,2))  
           zsmsep.whse = oeel.whse.
    end.
       zsmsep.salesamt[month(oeel.invoicedt)] =   
   zsmsep.salesamt[month(oeel.invoicedt)] -
     ((((oeel.qtyship * oeelk.qtyneeded)  *
      (oeel.price * ((100 - oeel.discpct) / 100) * 
                 oeel.unitconv)) *
       (oeelk.prodcost / zk-cost))         
                           * (if oeel.returnfl then -1 else 1)).


    v-rebate = v-rebate * (oeelk.prodcost / zk-cost).
    zsmsep.cogamt[month(oeel.invoicedt)] = 
      zsmsep.cogamt[month(oeel.invoicedt)] - 
      (((((oeel.qtyship * oeelk.qtyneeded)  *
       oeel.prodcost) *
       (oeelk.prodcost / zk-cost))         
                           * (if oeel.returnfl then -1 else 1)) 
                           - v-rebate).

      end. /* if i-compare */
end.




procedure create_zsmsep_va:
  v-rebate = 0.
  
  find icsw where icsw.cono = g-cono and 
                  icsw.prod = vaesl.shipprod and
                  icsw.whse = vaesl.whse  no-lock no-error.
  if avail icsw then                
    v-rebate = icsw.listprice.
  else
    v-rebate = vaesl.prodcost.
  

    
   if vaesl.sctntype = "it" then
     do:
     if vaesl.timeactty <> "" then
       v-rebate = vaesl.timeelapsed / 3600.
     else
       v-rebate = vaesl.qtyship.
     end.
   else
     v-rebate = vaesl.qtyship.
   
   if
     (v-rebate * 
      (zk-price *
      (vaesl.prodcost / vaeh.prodcost))) <> 0 or


      ((v-rebate) * vaesl.prodcost) <> 0 then
    do:   
  
  {z-smrzk2.i z-cat vaesl.prodcat z-cart}

  assign pk_custno = oeel.custno
         pk_shipto = oeel.shipto
         pk_cat    = vaesl.prodcat.
  run precheckparams.
  if not pk_good then
    next.       
 
  
  
  find zsmsep where
                    zsmsep.cono = g-cono           and
                    zsmsep.custno = oeel.custno   and
                    zsmsep.shipto = oeel.shipto   and
                    zsmsep.prodcat = z-cat and
                    zsmsep.yr     = 
                    int(substring(string(year(oeel.invoicedt),"9999"),3,2))  
                    and
                    zsmsep.whse = vaesl.whse no-lock no-error.
  if not avail zsmsep then
    do:
    create zsmsep.
    assign zsmsep.cono = g-cono
           zsmsep.custno = oeel.custno
           zsmsep.shipto = oeel.shipto
           zsmsep.prodcat = z-cat
           zsmsep.yr     = 
           int(substring(string(year(oeel.invoicedt),"9999"),3,2))  
           zsmsep.whse = vaesl.whse.
    end.
  
   if vaesl.sctntype = "it" then
     do:
     if vaesl.timeactty <> "" then
       v-rebate = vaesl.timeelapsed / 3600.
     else
       v-rebate = vaesl.qtyship.
     end.
   else
     v-rebate = vaesl.qtyship.
   
   zsmsep.salesamt[month(oeel.invoicedt)] =
     zsmsep.salesamt[month(oeel.invoicedt)] +
 
     (v-rebate * 
      (zk-price *
      (vaesl.prodcost / vaeh.prodcost))).


   zsmsep.cogamt[month(oeel.invoicedt)] = 
      zsmsep.cogamt[month(oeel.invoicedt)] + 
     ((v-rebate) * vaesl.prodcost). 
     
  run create_rev_zsmsep_va.
  end.
end.




procedure create_rev_zsmsep_va:
  if i-compare then do:
  v-rebate = 0.
  if avail oeel then                
    v-rebate = ((oeel.price * ((100 - oeel.discpct) / 100)) * 
                  oeel.unitconv).
  else
    v-rebate = vaesl.prodcost.
  
  if sapb.optvalue [17] = "e" then
    do:
      {z-smrzk2.i z-cat oeel.prodcat z-pcsn}

  
    find zsmsep where
                    zsmsep.cono = g-cono           and
                    zsmsep.custno = oeel.custno   and
                    zsmsep.shipto = oeel.shipto   and
                    zsmsep.prodcat = z-cat and
                    zsmsep.yr     =
                    int(substring(string(year(oeel.invoicedt),"9999"),3,2))  
                    and
                    zsmsep.whse = oeel.whse no-lock no-error.
    if not avail zsmsep then
      do:
      create zsmsep.
      assign zsmsep.cono = g-cono
           zsmsep.custno = oeel.custno
           zsmsep.shipto = oeel.shipto
           zsmsep.prodcat = z-cat
           zsmsep.yr     = 
            int(substring(string(year(oeel.invoicedt),"9999"),3,2))                         zsmsep.whse = oeel.whse.
      end.
    end.
  
  
  {z-smrzk2.i z-cat oeel.prodcat z-tild}

  
  find zsmsep where
                    zsmsep.cono = g-cono           and
                    zsmsep.custno = oeel.custno   and
                    zsmsep.shipto = oeel.shipto   and
                    zsmsep.prodcat = z-cat and
                    zsmsep.yr     =
                    int(substring(string(year(oeel.invoicedt),"9999"),3,2))  
                    and
                    zsmsep.whse = oeel.whse no-lock no-error.
  if not avail zsmsep then
    do:
    create zsmsep.
    assign zsmsep.cono = g-cono
           zsmsep.custno = oeel.custno
           zsmsep.shipto = oeel.shipto
           zsmsep.prodcat = z-cat
           zsmsep.yr     = 
           int(substring(string(year(oeel.invoicedt),"9999"),3,2))                          zsmsep.whse = oeel.whse.
    end.
  
   if vaesl.sctntype = "it" then
     do:
     if vaesl.timeactty <> "" then
       v-rebate = vaesl.timeelapsed / 3600.
     else
       v-rebate = vaesl.qtyship.
     end.
   else
     v-rebate = vaesl.qtyship.
   
     zsmsep.salesamt[month(oeel.invoicedt)] =
     zsmsep.salesamt[month(oeel.invoicedt)] +
 
    (v-rebate * 
      (zk-price *
      (vaesl.prodcost / vaeh.prodcost)) * -1).


   zsmsep.cogamt[month(oeel.invoicedt)] = 
      zsmsep.cogamt[month(oeel.invoicedt)] + 
     (((v-rebate) * vaesl.prodcost) * -1). 
       
     
     
  end.
end.

procedure precheckparams.

  pk_good = true.
  zelection_type = "p".
  zelection_char4 = substring (pk_cat,1,4).
  zelection_cust = 0.
  run zelectioncheck.
  if zelection_good = false then
    do:
    pk_good = false.
    next.
    end.

  zelection_type = "x".
  zelection_char4 = substring (pk_cat,1,4).
  zelection_cust = 0.
  run zelectioncheck.
  if zelection_good = false then
    do:
    pk_good = false.
    next.
    end.

  zelection_type = "c".
  zelection_char4 = "".
  zelection_cust = pk_custno.
  run zelectioncheck.
  if zelection_good = false then
    do:
    pk_good = false.
    next.
    end.


find arsc where arsc.cono = g-cono and
                arsc.custno = pk_custno no-lock no-error.
if avail arsc then do:
  
  if i-div <> 0 then do:
     if i-div = 30 and arsc.divno <> 30 then
        do:
        pk_good = false.
        next.
        end.

     if i-div = 40 and arsc.divno <> 40 then
       do:
       pk_good = false.
       next.
       end.
  end.

  assign
    x-slsm = arsc.slsrepout
    x-state = arsc.state.
end.


if pk_shipto <> "" then
  do:
  find arss where arss.cono = g-cono and
                arss.custno = pk_custno and
                arss.shipto = pk_shipto no-lock no-error.
  if avail arss then
    do:
    x-slsm = arss.slsrepout.
    x-state = arss.state.
    end.
  end.

  if i-state <> "" then
    if i-state <> x-state then
       do:
       pk_good = false.
       next.
       end.
     
  
  find smsn where smsn.cono = g-cono and smsn.slsrep = x-slsm
    no-lock no-error.
  if avail smsn then
    do:
    assign
      x-reg  = substring(smsn.mgr,1,1)
      x-dist = substring(smsn.mgr,2,3).
    end.
  else
    assign
      x-reg  = "z"
      x-dist = "999".


  if i-rgn <> "" then
    if x-reg <> i-rgn then
      do:
      pk_good = false.
      next.
      end.
  if i-dsrt <> "" then
    if x-dist <> i-dsrt then
      do:
      pk_good = false.
      next.
      end.
     

  if x-slsm < smnbg or
     x-slsm > smned then
    do:
    pk_good = false.
    next.
    end.

  if i-list = true then
    do:
    zelection_type = "s".
    zelection_char4 = x-slsm.
    zelection_cust = 0.
    run zelectioncheck.
    if zelection_good = false then
      do:
      pk_good = false.
      next.
      end.
   end.

end.

