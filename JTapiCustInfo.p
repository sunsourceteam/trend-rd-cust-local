
/* JTapiCustInfor.p
   Purpose:    Paragon interface Returns customer A/R balance and 
               any other customer values kept in SXE

*/

{g-SdiApi.i}    /* Error table */
{g-SdiDebug.i "New"}

Define Var v-cono      like sasc.cono          no-undo.
Define Var v-customer  like arsc.custno        no-undo.

Define Var v-ARbalance as character            no-undo.
Define Var v-ARlimit   as character            no-undo.
Define Var v-ARtermscd as character            no-undo.

Define Var s-totalbal  as dec no-undo.
Define Var g-arcodinbalfl as logical no-undo.
Define Var j as int no-undo.




Define Var v-debugfl as   logical      no-undo init true.    
Define Stream mylog.



Procedure JTCustInfo:
  Define input  parameter ip-cono      as char.
  Define input  parameter ip-customer  as char.
    
  Define output parameter  ip-ARbalance as character.
  Define output parameter  ip-ARlimit   as character.
  Define output parameter  ip-ARtermscd as character.
   
  Define output parameter ip-errcd     as char.
  Define output parameter ip-error     as char.


  run ZsdiDebug ( input ip-cono,
                  input "JTapiCustInfo.p").

  if v-setup-debugfl then do:
    assign v-setup-log-text = 
      "Parameters Cono = " +  
       ip-cono.  

    run ZsdiLogWrite(input "minimum",
                     input v-setup-log-text).
  end.

  int(ip-cono) no-error.
  if not error-status:error then 
    assign v-cono = int(ip-cono).
  else do:
    assign v-errinx = 3
           ip-errcd = string(v-errinx)
           ip-error = v-errorcd[v-errinx].
    return.
  end.          


  if v-setup-debugfl then do:
    assign v-setup-log-text = 
      "Parameters Customer = " +  
       ip-customer.  

    run ZsdiLogWrite(input "minimum",
                     input v-setup-log-text).
  end.

  dec(ip-customer) no-error.
  if not error-status:error then 
    assign v-customer = dec(ip-customer).
  else do:
    assign v-errinx = 3
           ip-errcd = string(v-errinx)
           ip-error = v-errorcd[v-errinx].
    return.
  end.          


  find arsc where 
       arsc.cono   = v-cono and
       arsc.custno = v-customer     no-lock no-error.
  if avail arsc then do:


       find sasc where sasc.cono = v-cono no-lock no-error.

       assign g-arcodinbalfl = sasc.arcodinbalfl
              s-totalbal = 0.

       assign
         s-totalbal  = arsc.futinvbal +
                       arsc.servchgbal +
                       arsc.misccrbal +
                       arsc.uncashbal +
                       if g-arcodinbalfl then arsc.codbal else 0.

      do j = 1 to 5:
        s-totalbal  = s-totalbal  + arsc.periodbal[j].
      end.

      find sasta where
           sasta.cono     = v-cono and
           sasta.codeiden = "t"    and
           sasta.codeval  = arsc.termstype no-lock no-error.
      if avail sasta then 
        assign 
          ip-ARtermscd    = if sasta.user5 <> "" then 
                              sasta.user5 
                            else
                              sasta.codeval.
      else
         ip-ARtermscd      =  sasta.codeval.





    
    assign ip-ARbalance = string(s-totalbal)
           ip-ARlimit   = string(arsc.credlim).
  end.
  
  RETURN.
end.

{p-SdiDebug.i}
