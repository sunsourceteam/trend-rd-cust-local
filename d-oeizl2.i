/* d-oeizl2.i 1.2 06/12/98 */
/*h*****************************************************************************

  SX 55
  INCLUDE      : d-oeizl2.i
  DESCRIPTION  : OE Inquiry Line Item Display
  USED ONCE?   : no (oeizl2.p, oeizl4.p)
  AUTHOR       : rhl
  DATE WRITTEN : 09/08/89
  CHANGES MADE :
    12/23/91 pap; TB# 5266 JIT/Line Due - replace requested ship date with
        promised date
    12/26/91 pap; TB# 5266 JIT/Line Due - added jit flag to display if oeeh
        reqshipdt or promisedt differ from oeel dates.
    01/13/92 pap; TB# 5447 JIT/Line Due - added more conditions to jit flag
    06/04/92 smb; TB# 6867  Operator Choice of Qty/Value
    07/20/92 mwb; TB# 7191 added custno change
    02/01/93 rhl; TB# 9688 Unit conversion displays wrong unit
    04/09/93 rhl; TB# 10848 Negative qty not showing
    01/26/95 dww; TB# 17308 Add User Hook Includes
    04/28/95 smb; TB# 11811 & 10234 Added specnstype display
    05/09/95 mtt; TB# 5180 When looking for a product-need kit ln
    05/29/98 cm;  TB# 24295 Incorrect qty/value on components. Need to consider
        quantity needed per kit.
*******************************************************************************/


if v-xmode = "O" then
  do:
 
 {w-icsp.i oeel.shipprod no-lock}

/*tb 11811 04/28/95 smb; Added specnstype display*/
 assign
    s-suspend = if oeeh.updtype = "s" then "s"
                else if oeeh.fpcustno ne {c-empty.i} then "f"
                else ""
    s-order   = string(oeeh.orderno,"zzzzzzz9") + "-" +
                string(oeeh.ordersuf,"99") +
                  (if oeeh.notesfl = "" then " "
                   else oeeh.notesfl) +
                  (if oeel.specnstype ne "" then oeel.specnstype
                   else " ")
    s-order   = s-order + string(oeel.lineno,"zz9") +
                  if oeel.commentfl = yes then "c"
                  else ""
    s-netamt = if {p-oeordl.i "oeel." "oeeh."} then
                (if s-valuefl then oeel.netord else oeel.stkqtyord)
                else (if s-valuefl then oeel.netamt else oeel.stkqtyship)
    s-netamt  = s-netamt *
                    if can-do("2,4",string(v-mode)) and s-kitty = "k" then
                        v-qtyneeded
                    else 1
    s-linedo  = if oeel.botype = "d" then "d" else ""
    s-rushfl  = if oeel.rushfl then "r" else ""
    s-netamt  = s-netamt * if oeel.returnfl then -1 else 1
    s-value   = if s-valuefl then string(s-netamt,"zzzzzzzz9.99-")
                else string(s-netamt,"zzzzzz9-") + " " +
                      (if avail icsp then icsp.unitstock else oeel.unit)
    s-jitflag = if    oeeh.orderdisp = "j"
                      then  if   oeeh.stagecd <= 1
                            then if  oeeh.reqshipdt = oeel.reqshipdt  and
                                     oeeh.promisedt = oeel.promisedt
                                 then ""
                                 else "j"
                            else if   oeel.jitpickfl = yes
                                 then ""
                                 else  if  oeeh.reqshipdt = oeel.reqshipdt and
                                           oeeh.promisedt = oeel.promisedt
                                       then ""
                                       else "j"
                       else ""
         s-dtranstype = oeeh.transtype
         s-dstagecd   = oeeh.stagecd
         s-dpromisedt = oeeh.promisedt
         s-dinvoicedt = oeeh.invoicedt
         s-dtakenby   = oeeh.takenby
         s-pocust  = if s-custpojobty = "j":u         then oeeh.jobno
                     else if s-custpojobty = "p":u    then oeeh.custpo
                     else {c-asignc.i oeeh.custno} + " " + oeeh.shipto.
   end.
else
if v-xmode = "C" then
  do:
 {w-icsp.i oeelb.shipprod no-lock}

/*tb 11811 04/28/95 smb; Added specnstype display*/
 assign
    s-suspend = ""
    s-order =   substring(string(int(oeehb.batchnm),">>>>>>>9"),1,8)
                  + "c" + "  " + 
                  (if oeehb.notesfl = "" then " "
                   else oeehb.notesfl) +         
                  (if oeelb.specnstype ne "" then oeelb.specnstype
                   else " ")
    s-order   = s-order + string(oeelb.lineno,"zz9") +
                  if oeelb.commentfl = yes then "c"
                  else ""
    s-netamt =  (if s-valuefl then oeelb.netamt else oeelb.qtyord)
    s-netamt  = s-netamt *
                    if can-do("2,4",string(v-mode)) and s-kitty = "k" then
                        v-qtyneeded
                    else 1
    s-linedo  = if oeelb.botype = "d" then "d" else ""
    s-rushfl  = if oeelb.rushfl then "r" else ""
    s-netamt  = s-netamt /* * if oeel.returnfl then -1 else 1 */
    s-value   = if s-valuefl then string(s-netamt,"zzzzzzzz9.99-")
                else string(s-netamt,"zzzzzz9-") + " " +
                      (if avail icsp then icsp.unitstock else oeelb.unit)
    s-jitflag = ""
    
         s-dtranstype = "ZQ"
         s-dstagecd   = if oeehb.stagecd = 9 then 9 else 0
         s-dpromisedt = oeehb.promisedt
         s-dinvoicedt = ?
         s-dtakenby   = oeehb.takenby
         s-pocust  = if s-custpojobty = "j":u         then oeehb.jobno
                     else if s-custpojobty = "p":u    then oeehb.custpo
                     else {c-asignc.i oeehb.custno} + " " + oeehb.shipto.         
 
  end.





display
    s-order
    s-dtranstype @ oeeh.transtype
    s-linedo
    s-dstagecd   @ oeeh.stagecd
    s-suspend
    s-dpromisedt @ oeeh.promisedt
    s-rushfl
    s-jitflag
    s-dinvoicedt  @ oeeh.invoicedt
    s-dtakenby   @ oeeh.takenby
    s-pocust
    s-value
with frame f-oeizl.


/* SX 55
if v-xmode = "O" then
  do:
  if can-do("1,4,5,7",string(v-mode)) then
     if oeeh.jobno ne "" and oeeh.jobno ne "ZWIP" and oeeh.jobno ne "ZBLOCK"
       then display "Job # " + oeeh.jobno @ s-pocust with frame f-oeizl.
       else display oeeh.custpo @ s-pocust with frame f-oeizl.
  else display {c-asignc.i oeeh.custno} + " " + oeeh.shipto @ s-pocust
          with frame f-oeizl.
  end.
else
if v-xmode = "C" then
  do:
  if can-do("1,4,5,7",string(v-mode)) then
      display oeehb.custpo @ s-pocust with frame f-oeizl.
  else display {c-asignc.i oeehb.custno} + " " + oeehb.shipto @ s-pocust
       with frame f-oeizl.
  end.
SX 55 */
if avail oeeh then
  do: 
  /*tb 17308 01/26/95 dww; Add User Hook Includes */
  {d-oeizl2.z99 &user_line_display = "*"}
  end.
