/* p-icib.i 1.2 10/18/93 */
/*h*****************************************************************************
  INCLUDE      : p-icib.i
  DESCRIPTION  : Processes for icib inquiry
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN : 10/18/93
  CHANGES MADE :
    07/27/93 pjt; TB# 12355 Added Product Status
    10/31/95 kr ; TB# 19565 Introduce new ARP Type of "M" (VMI)
    05/19/98 cm;  TB# 21668 Whse surplus not calculated correctly
    03/18/99 sbr; TB# 25356 Order Qty incorrect for Min/Max
    09/14/99 ama; TB# e2761 Tally Project - Need to be able to handle new
                icsp.kittype of "M" - include M whereever kittype referenced
    05/02/00 des; TB# e5061 Inventory Allocation Project - Assign reservation
        type
    06/08/00 maa; TB# e5061 Inventory Allocation Project - Assign reservation
        days
    08/15/01 rgm; TB# e10068 Modify safety allow display for "days" type
    03/23/04 bpa; TB# e19466 If safety allowance is days, use icsw.safealldays
            instead of icsw.safeallpct
            *******************************************************************************/

/*tb 25356 03/18/99 sbr; Use "({p-poavl.i} / v-conv)" instead of "{p-poavl.i}"
    when setting s-ordqty in the assign statement. */
/*tb e5061 05/02/00 des; Inventory Allocation Project - Add assign of
        s-reservety */
/*tb e5061 06/08/00 maa; Inventory Allocation Project - Add assign of
        s-reservedays */
 assign
   s-poduedt       = ?
   s-wtduedt       = ?
   s-qtyonhand     = icsw.qtyonhand / v-conv
   s-qtyreservd    = icsw.qtyreservd / v-conv
   s-qtycommit     = icsw.qtycommit / v-conv
   s-qtyonord      = icsw.qtyonord / v-conv
   s-qtybo         = icsw.qtybo / v-conv
   s-wtdemand      = icsw.qtydemand / v-conv
   s-qtyrcvd       = icsw.qtyrcvd / v-conv
   s-usagerate     = icsw.usagerate / v-conv
   s-ordpt         = icsw.orderpt / v-conv
   s-linept        = icsw.linept / v-conv
   s-availnow      = s-qtyonhand - s-qtyreservd - s-qtycommit
   s-availfut      = s-qtyonord  + s-qtyrcvd    - s-qtybo - s-wtdemand
   s-ordqty        = if icsw.ordcalcty = "m" then
                          maximum(0,s-linept - ({p-poavl.i} / v-conv)) else
                     if icsw.seasbegmm = 0 then
                          icsw.ordqtyin / v-conv
                     else
                     if icsw.seasbegmm < icsw.seasendmm then
                       if month(today) < icsw.seasbegmm or
                         month(today) > icsw.seasendmm
                       then icsw.ordqtyout / v-conv
                       else icsw.ordqtyin / v-conv
                     else
                     if month(today) >= icsw.seasbegmm or
                        month(today) <= icsw.seasendmm
                     then icsw.ordqtyin / v-conv
                     else icsw.ordqtyout / v-conv

   s-source        =      if icsp.kittype = "b" then "Build to Ord Kit"
                          else if icsp.kittype = "p" then "Prebuilt Kit"
                          else if icsp.kittype = "m" then "Mix Kit"
                          else if icsw.arptype = "v" then "Vendor"
                          else if icsw.arptype = "m" then "VMI"
                          else if icsw.arptype = "w" then "Warehouse"
                          else if icsw.arptype = "c" then "Whse Order"
                          else ""
   s-source        = if can-do("w,c",icsw.arptype) and icsw.arppushfl
                      then s-source + " (Push)" else s-source
   s-sourceno      = if can-do("b,p,m",icsp.kittype) then "" else
                     if can-do("w,c",icsw.arptype) then icsw.arpwhse else
                     if can-do("v,m",icsw.arptype) then
                       string(icsw.arpvendno)
                     else "".
assign
  s-status        = if avail icsp and icsp.statustype = "s" then
                      "SUPERSEDED" else
                    if icsw.statustype = "x" then
                      "DO NOT REORDER" else ""
  s-ordpttext     = if icsw.ordcalcty = "m" then " Minimum:" else
                      "Order Pt:"
  s-linepttext    = if icsw.ordcalcty = "m" then " Maximum:" else
                      " Line Pt:"
  s-safeallow     = if icsw.safeallty = "%"  or icsw.safeallty = "q" then
                      string(round(icsw.safeallamt / v-conv,2)) + "/" +
                      string(icsw.safeallpct) + "%"
                    else if icsw.safeallty = "d" then
                      string(icsw.safealldays) + " days"
                    else ""
  s-seastext      = if icsw.seasbegmm ne 0 then
                      "Season: " + v-seas[icsw.seasbegmm] + " to " +
                      (if icsw.seasendmm = 0 then "" else
                      v-seas[icsw.seasendmm])
                    else ""
  s-seasord       = if icsw.seasbegmm ne 0 then
                      if month(today) ge icsw.seasbegmm and
                        month(today) le icsw.seasendmm then
                       "Qty Out of Seas: " +
                        string(round(icsw.ordqtyout / v-conv,2))
                      else "Qty In Season: "   +
                          string(round(icsw.ordqtyin / v-conv,2))
                    else ""
  s-ordcalcty     = if icsw.ordcalcty = "e" then "      (EOQ)" else
                    if icsw.ordcalcty = "c" then "    (Class)" else
                    if icsw.ordcalcty = "m" then "  (Min/Max)" else
                    if icsw.ordcalcty = "b" then "  (Blanket)" else
                    if icsw.ordcalcty = "h" then "    (Human)" else
                    if icsw.ordcalcty = "q" then "(Qty Break)" else ""
  s-days          = if s-usagerate = 0 then "" else
                      if s-availnow le 0 then "  0 Days" else
                        string(
                           minimum((s-availnow * v-conv) /
                          (icsw.usagerate / 28),999),"zz9")
                          + " Days"
  s-alttext       = ""
  s-usagetxt      = "Usage(" + string(icsw.usgmths,"99") + "):"
  v-alt           = 1
  s-prodstat      = if icsw.statustype = "s" then "Stock" else
                    if icsw.statustype = "o" then "Order as Needed" else
                    if icsw.statustype = "d" then "Direct Ship" else
                    if icsw.statustype = "x" then "Do Not Reorder" else
                      ""
  s-reservety     = if icsw.reservety = "d":u
                    then "Delay Reservation"
                    else if icsw.reservety = "r":u
                    then "Against Pending Receipts"
                    else if icsw.reservety = "a":u
                     then "Always"
                    else ""
  s-reservedays   = icsw.reservedays.

/** Set Alt Text Settings **/
/** Surplus Check **/
/*
lbl:
  for each b-icsw use-index k-icsw where
           b-icsw.cono     = g-cono    and
           b-icsw.prod     = icsw.prod
           no-lock:

/*tb 21668 05/19/98 cm; Whse surplus not calculated properly. Use common
        include to determine if surplus exists. */
    {p-srplus.i &sur = v-surplus
                &b   = "b-"}
    if v-surplus > 0 then do:
      assign
        s-alttext[v-alt] = "WHSE SURPLUS EXISTS"
        v-alt = v-alt + 1.
      leave lbl.
    end.
  end.
*/
/** Alt Vendor Check **/
if can-find(first icsec use-index k-icsec where
                  icsec.cono    = g-cono    and
                  icsec.prod    = g-prod    and
                  icsec.rectype = "v")      then
  assign
    s-alttext[v-alt] = "ALT VENDORS EXIST"
                       v-alt = v-alt + 1.

/** Subs/Upgrades **/
if can-find(first icsec use-index k-icsec where
                  icsec.cono    = g-cono    and
                  icsec.prod    = g-prod    and
                  icsec.rectype = "s")
or can-find(first icsec use-index k-icsec where
                  icsec.cono    = g-cono    and
                  icsec.prod    = g-prod    and
                  icsec.rectype = "u")     then
  assign
    s-alttext[v-alt] = "SUBS/UPGRADES EXIST"
    v-alt = v-alt + 1.

/** Set Frozen Information **/
if icsw.frozentype ne "" and s-status = "" then do:
  {w-sasta.i "f" icsw.frozentype no-lock}
  s-status = if avail sasta then "FZ-" + substring(sasta.descrip,1,12)
             else "Frozen-" + icsw.frozentype.
end.

/** Associated Finds **/
{w-apsv.i icsw.arpvendno no-lock}
{w-icsl.i icsw.whse icsw.arpvendno icsw.prodline no-lock}

/*
/** Color Settings **/
do i = 1 to 3:
  if s-alttext[i] ne "" then color display message       s-alttext[i]
    with frame f-icib.
  else color display normal s-alttext[i] with frame f-icib.
end.
*/
/*
if s-status ne "" then color display message s-status with frame f-icib.
else color display normal s-status with frame f-icib.
*/
