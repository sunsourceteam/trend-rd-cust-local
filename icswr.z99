/* icswr.z99 1.1 01/03/98 */
/* icswr.z99 1.1 05/10/95 */
/*h****************************************************************************
  INCLUDE      : icswr.z99
  DESCRIPTION  : User Hook for icswr.p
  USED ONCE?   : yes (icswr.p)
  AUTHOR       : kjb
  DATE WRITTEN : 05/10/95
                 04/17/2006 cleaning up - try to make shorter with loop
                 for bin1 and bin2
                 02/28/2006 dkt Tie wmsb.priority to icsw.binloc# 
  CHANGES MADE :
    05/10/95 kjb; TB# 18428 Add user hook to icswr.p (&end_of_main) for
        All-Phase Electric
    12/10/96 rh ; TB# 22226 Add User Hook in icswr.p (&user_inedit)
    12/13/98 bp ; TB# 25661 Add user hook &user_defvar
    06/03/99 bp ; TB# 26278 Add the user inlcudes &user_aftfind and
        &user_aftedit
    12/18/02 bp ; TB# 15724 Add additional hooks.

    Mode           Description
    ------------   -----------------------------------------------------------
    &user_defvar   Before processing
    &user_topmain  At top of main
    &user_aftfind  In icswr.p, after the find of the ICSW record
    &end_of_main   To be used at the end of the main ICSW processing loop
    &user_inedit   In icswr.p, after end of if k-after.i or k-func.i and
                   befor apply lastkey
    &user_aftedit  In icswr.p, after edit logic
    &user_canceled After delete logic.
    &user_deleted  After final delete logic.
******************************************************************************/

/{&user_defvar}*/
def shared var g-binloc like icsw.binloc1 no-undo.
def buffer zx-icsd for icsd.
def buffer zy-sasos for sasos.
def var zy-securecd as integer no-undo. 
on CTRL-O anywhere do:
  run zsdictrlpicsw.p.
  hide frame f-icswr.
  lastkey = keycode("RETURN").
  apply lastkey.
end.
def            var x-prefix        like icsw.prod               no-undo.
def            var x-pricetype     like icsw.pricetype          no-undo.
def            var x-model         as char format "x(40)"       no-undo.
def            var x-bypassfl      as logical  format "y/n"     no-undo.
def            var v-FlatBypassfl  as logical  format "y/n"     no-undo.

def var v-lp          as   logical             no-undo.    /* listprice  */
def var v-bp          as   logical             no-undo.    /* baseprice  */
def var v-rc          as   logical             no-undo.    /* replcost   */
def var v-sc          as   logical             no-undo.    /* stndcost   */
def var v-pt          as   logical             no-undo.    /* pricetype  */
def var v-st          as   logical             no-undo.    /* statustype */ 
def var hold-ptype    like icsw.pricetype      no-undo.
def var hold-status   like icsw.statustype     no-undo.


/* dkt 01/24/2006 */
def var ostatus  like icsw.statustype no-undo.
def var obinloc1 like icsw.binloc1 no-undo.
def var obinloc2 like icsw.binloc2 no-undo.
def var s-binloc1 like icsw.binloc1 no-undo.
def var s-binloc2 like icsw.binloc2 no-undo.

def var whichLoc as integer no-undo.        
def var thisBin like icsw.binloc1 no-undo.  
def var errFlag as logical no-undo.  
def var ansDel  as logical no-undo.

def var updRight as logical no-undo.
def var inPrior like wmsb.priority no-undo.       
def var dispBin like icsw.binloc1 no-undo.
def var wrongBin like icsw.binloc1 no-undo.

/* try to shorten */
def var oldBinLoc like icsw.binloc1 no-undo.

form                                          
  "Bin"  at 1                                 
  dispBin  at 5                                 
  "Priority" at 1
  inPrior  at 12
with frame f-priorityBin width 22 centered 
no-hide overlay no-labels title "Change Bin Priority".                    
/{&user_defvar}*/


&if defined(user_topmain) = 2 &then
&endif

/{&user_aftfind}*/
if icsw.pricetype <> "" then
  assign hold-ptype = icsw.pricetype.
assign hold-status  = icsw.statustype.

/* dkt 01/2006  when first pull up icsw */
ostatus  = icsw.statustype.
obinloc1 = icsw.binloc1.
obinloc2 = icsw.binloc2.
find first zy-sasos where zy-sasos.cono   = g-cono and
                          zy-sasos.oper2  = g-operinits and
                          zy-sasos.menuproc = "icsz"
                          no-lock no-error.
if not avail zy-sasos then
   zy-securecd  = 0.
else
   zy-securecd = zy-sasos.securcd[1]. 

/{&user_aftfind}*/

/{&end_of_main}*/
g-binloc = icsw.binloc1.
s-binloc1 = icsw.binloc1.
s-binloc2 = icsw.binloc2.
/{&end_of_main}*/

/{&user_inedit}*/
if {k-after.i} or {k-func.i} then do:
  if (frame-field = "pricetype" and input icsw.pricetype = "") or
     (keyfunction(lastkey) = "GO" and input icsw.pricetype = "") then do: 
    message "Please enter a valid Price Type". 
    next-prompt icsw.pricetype with frame f-icswr.
    next.
  end.
  if frame-field = "pricetype" and input icsw.pricetype <> "" and
     length(input icsw.pricetype) = 4 then do:
    {w-sasta.i "K" input icsw.pricetype no-lock}
    if avail sasta and sasta.descrip MATCHES "*NOT USE*" then do:
      message "Please enter a valid Price Type".
      next-prompt icsw.pricetype with frame f-icswr.
      next.
    end.
  end.
 
  if frame-field = "pricetype" and input icsw.pricetype <> "" then do:
    {w-sasta.i "K" input icsw.pricetype no-lock}
    if avail sasta and input icsw.pricetype <> "" then do:
      run vaexq-flatcheck (input g-cono,
                           input g-prod,
                           input "i",
                           output x-prefix,
                           output x-pricetype,
                           output x-model,
                           output x-bypassfl).
      if x-prefix <> " " and 
         input icsw.pricetype <> x-pricetype then do:
        run vaexq-flatbypass (input x-bypassfl,
                              input-output v-FlatBypassfl,
                              input-output x-model,
                              input-output x-prefix).
        if not v-FlatBypassfl then  do:
 
          message "Product is MFG " + x-model + " " +
                  "must be pricetype " + x-pricetype.
          next-prompt icsw.pricetype with frame f-icswr.
          next.
        end.
      end.
    end.
    
    if avail sasta and sasta.descrip MATCHES "*NOT USE*" then do:
      message "Please enter a valid Price Type".
      next-prompt icsw.pricetype with frame f-icswr.
      next.
    end.
  end.
    
  
  
  
  
  
  
  if (frame-field = "arpwhse" or {k-accept.i}) and
     index("wc",input icsw.arptype) > 0  and
     not can-find(b-icsw use-index k-icsw where
                  b-icsw.cono = icsw.cono and
                  b-icsw.prod = icsw.prod and
                  b-icsw.whse = input icsw.arpwhse)
  then do:
    run err.p(5876).
    next-prompt icsw.arpwhse with frame f-icswr.
    next.
  end.
end.

{icsd.gfi &whse = g-whse &lock = "no" &b = "zx-"}             
if ENTRY(1,zx-icsd.user4) = "Y" 
/* TAH 011707 */   
   or ENTRY(1,zx-icsd.user4) = "P" then do:
/* TAH 011707 */   

  if frame-field = "binloc1" then whichLoc = 1.                  
  if frame-field = "binloc2" then whichLoc = 2.                  
  
  if frame-field = "binloc1" or frame-field = "binloc2" then do:
    find first zsdiput where
      zsdiput.cono = g-cono and zsdiput.whse = icsw.whse and
      zsdiput.shipprod = icsw.prod and zsdiput.completefl = no
    no-lock no-error.
    if avail zsdiput then do:
      message zsdiput.shipprod " is currently being received"
      view-as alert-box error.
      release zsdiput.
      /* apply "focus" to icsw.wmfl in frame f-icswr. */
      next-prompt icsw.wmfl with frame f-icswr.
      next.
      /* leave. */
    end.
  end.         
  
  if (frame-field = "binloc1" or frame-field = "binloc2")
     and keylabel(lastkey) = "F12" then do:
    run bin-browse.p(input icsw.whse, input icsw.prod, output thisBin).
    pause 0.
    if whichLoc = 1 then do:
      if thisBin ne "" then icsw.binloc1 = thisBin.                    
      display icsw.binloc1 with frame f-icswr.   
      apply "focus" to icsw.binloc1 in frame f-icswr.
      next-prompt icsw.binloc1 with frame f-icswr.
      next.
    end.
    if whichLoc = 2 then do:
      if thisBin ne "" then icsw.binloc2 = thisBin.
      display icsw.binloc2 with frame f-icswr.
      apply "focus" to icsw.binloc2 in frame f-icswr. 
      next-prompt icsw.binloc2 with frame f-icswr.
      next.
    end.
  end. /* binloc 1 or 2 */
   
  if ({k-accept.i} or {k-after.i} or
    (icsw.binloc1:cursor-offset = 13 AND keylabel(lastkey) = "CURSOR-RIGHT") or
    (icsw.binloc1:cursor-offset = 1 AND keylabel(lastkey) = "CURSOR-LEFT")) and
     input icsw.binloc1 <> obinloc1  then do:

    assign whichLoc = 1  
           thisBin = input icsw.binloc1.
    errFlag = no.
    if thisBin = "New Part" then thisBin = "".
    if /* (thisBin = "" and  */  zy-securecd   <= 2 then do:
      errFlag = yes.
      message "Invalid Security to change Bin information".
      next-prompt icsw.binloc1 with frame f-icswr.
      next.
    end.
    if thisBin = "" and   zy-securecd  le 3 then do:
      errFlag = yes.
      message "Invalid Security to delete a Bin, but you may change Bins".
      next-prompt icsw.binloc1 with frame f-icswr.
      next.
    end.
    if thisBin ne "" and thisBin ne "New Part" then do:
      find first wmsb where wmsb.cono = g-cono + 500 and wmsb.whse = icsw.whse
      and wmsb.binloc = thisBin no-lock no-error.
      if avail wmsb then do:
        find first wmsbp where wmsbp.cono = wmsb.cono and wmsbp.whse = wmsb.whse
        and wmsbp.binloc = wmsb.binloc and wmsbp.prod ne icsw.prod
        no-lock no-error.
        if avail wmsbp then do:
        /* if wmsbp.prod ne icsw.prod then do: */
          errFlag = yes.
          message "Bin " thisBin " is in use for product " wmsbp.prod.
          apply "focus" to icsw.binloc1 in frame f-icswr.      
          next-prompt icsw.binloc1 with frame f-icswr.         
          next.

/*
          if wmsbp.prod = "" then message "NULL product".
          pause.
*/
        end.  /* else update info and delete old info */
        else do:
          /* could be pre-update */
          find first zzbin where zzbin.cono = g-cono + 500 and 
          zzbin.whse = icsw.whse and zzbin.binloc = thisBin no-lock no-error.
          if avail (zzbin) /* and wmsb.statuscode = "T" */ then do:
            if zzbin.prod ne icsw.prod then do:
              message "Bin " thisBin " will be assigned to " zzbin.prod.
              errFlag = yes.
              apply "focus" to icsw.binloc1 in frame f-icswr.      
              next-prompt icsw.binloc1 with frame f-icswr.         
              next.
            end.
          end.
        end.
      end.
      else do:
        if  zy-securecd  < 5 then do:
          errFlag = yes.                                              
          message "Invalid security to Add Bins, bin " + thisbin. 
          apply "focus" to icsw.binloc1 in frame f-icswr.      
          next-prompt icsw.binloc1 with frame f-icswr.         
          next.
        end.
      end.
    end.
    if errFlag then do:                                      
      icsw.binloc1 = obinloc1.                               
      display icsw.binloc1 with frame f-icswr.  
      if whichLoc = 1 then do:                               
        apply "focus" to icsw.binloc1 in frame f-icswr.      
        next-prompt icsw.binloc1 with frame f-icswr.         
      end.                                                   
      if whichLoc = 2 then do:                               
        apply "focus" to icsw.binloc2 in frame f-icswr.      
        next-prompt icsw.binloc2 with frame f-icswr.         
      end.                                                   
    end.                                                     
    /* No errors found, update the WMSBP info */
  end.  
   
  if ({k-accept.i} or {k-after.i} or
    (icsw.binloc2:cursor-offset = 13 AND keylabel(lastkey) = "CURSOR-RIGHT") or
    (icsw.binloc2:cursor-offset = 1 AND keylabel(lastkey) = "CURSOR-LEFT")) and
     input icsw.binloc2 <> obinloc2  then do:

    assign whichLoc = 2  
           thisBin = input icsw.binloc2.
    errFlag = no.
    if thisBin = "New Part" then thisBin = "".
    if /* thisBin = "" and     */ zy-securecd   <= 2 then do:
      errFlag = yes.
      message "Invalid Security to change Bin information".
      next-prompt icsw.binloc2 with frame f-icswr.
      next.
    end.
    if thisBin = "" and   zy-securecd  le 3 then do:
      errFlag = yes.
      message "Invalid Security to delete a Bin, but you may change Bins".
      next-prompt icsw.binloc2 with frame f-icswr.
      next.
    end.
    if thisBin ne "" and thisBin ne "New Part" then do:
      find first wmsb where wmsb.cono = g-cono + 500 and wmsb.whse = icsw.whse
      and wmsb.binloc = thisBin no-lock no-error.
      if avail wmsb then do:
        find first wmsbp where wmsbp.cono = wmsb.cono and wmsbp.whse = wmsb.whse
        and wmsbp.binloc = wmsb.binloc and wmsbp.prod ne icsw.prod
        no-lock no-error.
        if avail wmsbp then do:
        /* if wmsbp.prod ne icsw.prod then do: */
          errFlag = yes.
          message "Bin " thisBin " is in use for product " wmsbp.prod.
/* 
          if wmsbp.prod = "" then message "NULL product".
          pause.
*/
          apply "focus" to icsw.binloc2 in frame f-icswr.      
          next-prompt icsw.binloc2 with frame f-icswr.         
          next.

        end.  /* else update info and delete old info */
        else do:
          /* could be pre-update */
          find first zzbin where zzbin.cono = g-cono + 500 and 
          zzbin.whse = icsw.whse and zzbin.binloc = thisBin no-lock no-error.
          if avail (zzbin) /* and wmsb.statuscode = "T" */ then do:
            if zzbin.prod ne icsw.prod then do:
              message "Bin " thisBin " will be assigned to " zzbin.prod.
              errFlag = yes.
              apply "focus" to icsw.binloc2 in frame f-icswr.      
              next-prompt icsw.binloc2 with frame f-icswr.         
              next.

            end.
          end.
        end.
      end.
      else do:
        if  zy-securecd  < 5 then do:
          errFlag = yes.                                              
          message "Invalid security to Add Bins, bin " + thisbin.
          apply "focus" to icsw.binloc2 in frame f-icswr.      
          next-prompt icsw.binloc2 with frame f-icswr.         
          next.
        end.
      end.
    end.
    if errFlag then do:                                      
      icsw.binloc2 = obinloc2.                               
      display icsw.binloc2 with frame f-icswr.  
      if whichLoc = 1 then do:                               
        apply "focus" to icsw.binloc1 in frame f-icswr.      
        next-prompt icsw.binloc1 with frame f-icswr.         
      end.                                                   
      if whichLoc = 2 then do:                               
        apply "focus" to icsw.binloc2 in frame f-icswr.      
        next-prompt icsw.binloc2 with frame f-icswr.         
      end.                                                   
    end.                                                     
    /* No errors found, update the WMSBP info */
  end.  


end.


/{&user_inedit}*/

/{&user_aftedit}*/
/*das - zicp*/
if icsw.baseprice <> o-baseprice or icsw.listprice <> o-listprice or 
   icsw.pricetype <> hold-ptype or icsw.statustype <> hold-status then do:
  if icsw.listprice <> o-listprice then
    assign v-lp = yes.
  else
    assign v-lp = no.
  if icsw.baseprice <> o-baseprice then
    assign v-bp = yes.
  else
    assign v-bp = no.
  assign v-rc = no
         v-sc = no.
  if icsw.pricetype <> hold-ptype then
    assign v-pt = yes.
  else
    assign v-pt = no.
  if icsw.statustype <> hold-status then
     assign v-st = yes.
  else
    assign v-st = no.
  run zsdizicpupdt.p(input icsw.whse,     
                           icsw.prod,
                           icsw.avgcost,
                           o-baseprice,
                           o-listprice,
                           icsw.replcost, 
                           icsw.stndcost,
                           hold-ptype,
                           hold-status,
                           v-lp,
                           v-bp,
                           v-rc,
                           v-sc,
                           v-pt,
                           v-st).
end.
/*das - zicp*/
/* dkt */
{icsd.gfi &whse = g-whse &lock = "no" &b = "zx-"}
if ENTRY(1, zx-icsd.user4) = "Y"  
/* TAH 011707 */   
 or ENTRY(1, zx-icsd.user4) = "P" then do: 
/* TAH 011707 */   

  
  do whichLoc = 1 to 2:
    if whichLoc = 1 then do:
      oldBinLoc = oBinLoc1.
      thisBin   = icsw.binloc1.
    end.
    else if whichLoc = 2 then do:
      oldBinLoc = oBinLoc2.
      thisBin = icsw.binloc2.
      if thisBin ne "" and thisBin ne "New Part" 
        and thisBin = icsw.binloc1 then do:
        message "This bin is already assigned to this item".
        pause.
        icsw.binloc2 = "".
        next-prompt icsw.binloc2 with frame f-icswr.
        next main.
      end.
    end.

    /* try to consolidate code here */
    if oldBinLoc ne thisBin and oldBinLoc ne ""
      and oldBinLoc ne "New Part" and  zy-securecd  > 2 then do:
      message "Do you wish to remove" icsw.prod " from bin" oldBinLoc "? "
      view-as alert-box question buttons yes-no
      update ansDel.
      if ansDel then do:
        /* Delete the old one */                                              
        find first wmsbp where wmsbp.cono = icsw.cono + 500                   
        and wmsbp.prod = icsw.prod and wmsbp.whse = icsw.whse and             
        wmsbp.binloc = oldBinLoc exclusive-lock no-error.                      
        if avail wmsbp then do:                                               
          delete wmsbp.                                                       
          find first wmsbp where wmsbp.cono = g-cono + 500 and                
          wmsbp.whse = icsw.whse and wmsbp.prod <> icsw.prod                  
          and wmsbp.binloc = oldBinLoc no-lock no-error.                       
          if not avail wmsbp then do:                                         
            find wmsb where wmsb.cono = g-cono + 500 and wmsb.whse = icsw.whse
            and wmsb.binloc = oldBinLoc exclusive-lock no-error.             
            if avail wmsb then do:                                        
              wmsb.statuscode = "A".                                           
              wmsb.priority   = whichLoc.
              wmsb.assigncode = icsw.statustype.    
            end.                                                              
          end. /* not avail with diff product */                              
        end.  /* avail wmsbp */                                           
      end. /* if ansDel */                                                
      else do:
        if whichLoc = 1 then icsw.binloc1 = oldBinLoc.
        if whichLoc = 2 then icsw.binloc2 = oldBinLoc.
        display icsw.binloc1 icsw.binloc2 with frame f-icswr.
        input clear.     
        readkey pause 0. 
        if whichLoc = 1 then next-prompt icsw.binloc1 with frame f-icswr.
        if whichLoc = 2 then next-prompt icsw.binloc2 with frame f-icswr.
        next main.
      end.
      /************ This version, retired 11/07/2006 used priorities DKT
      else do:                                                            
        /* new priority */                                                
        updRight = no.                                                    
        dispBin = oldBinLoc.                                               
        inPrior = 1. 
        for each wmsbp where wmsbp.cono = g-cono + 500 and wmsbp.whse =
        icsw.whse and wmsbp.prod = icsw.prod no-lock:                     
          inPrior = inPrior + 1.                                          
        end.                                                              
        if inPrior > 9 then inPrior = 9.                                  
        do while not updRight:                                            
          display dispBin inPrior with frame f-priorityBin.               
          update inPrior with frame f-priorityBin.                        
          if inPrior = 9 then updRight = yes.                             
          if inPrior <= 2 then do:                                        
            if inPrior = 1 then do:                                      
              if icsw.binloc1 = "" then do:                              
                icsw.binloc1 = dispBin.                                  
                updRight = yes.                                          
              end.                                                       
              else do:                                                   
                message "Binloc1 is already assigned to " icsw.binloc1.  
                updRight = no.                                           
              end.                                                       
            end.                                                         
            else if inPrior = 2 then do:                                
              if icsw.binloc2 = "" then do:                             
                icsw.binloc2 = dispBin.                                 
                updRight = yes.                                         
              end.                                                      
              else do:                                                  
                message "Binloc2 is already assigned to " icsw.binloc2. 
                updRight = no.                                          
              end.                                                      
            end.                                                        
            else do:                                                    
              message "Priority for low-priority bin must be from 3-9". 
              updRight = no.                                            
            end.                                                        
          end.                                                          
          if inPrior >= 3 and inPrior <= 8 then do:                          
            updRight = yes.                                                  
            for each wmsbp where wmsbp.cono = g-cono + 500 and wmsbp.whse =  
            icsw.whse and wmsbp.prod = icsw.prod                             
            no-lock:                                                         
              find first wmsb where wmsb.cono = wmsbp.cono and wmsb.whse =   
              wmsbp.whse and wmsb.binloc = wmsbp.binloc and wmsb.priority =  
              inPrior no-lock no-error.                                      
              if avail wmsb then do:                                         
                updRight = no.                                               
                wrongBin = wmsb.binloc.                                      
              end.                                                           
            end.                                                             
            if not updRight then do:                                         
              message "Priority " inPrior " set for bin " wrongBin.          
            end.                                                             
            else do:                                                         
              find first wmsb where wmsb.cono = g-cono + 500 and wmsb.whse = 
              icsw.whse and wmsb.binloc = dispBin and wmsb.priority = whichLoc
              exclusive-lock no-error.                                       
              wmsb.priority = inPrior.                                       
              updRight = yes.                                                
            end.                                                             
          end.                                                               
          if inPrior > 9 then do:                                            
            message "Priority cannot be set above 9".                        
            updRight = no.                                                   
            inPrior = 9.                                                     
          end.                                                               
          hide frame f-priorityBin.                                          
        end.                                                                 
        if inPrior > 2 then do: 
          /* move the bin and create zzbin */ 
          find first wmsb where wmsb.cono = g-cono + 500 and wmsb.whse =       
          icsw.whse and wmsb.binloc = oldBinLoc exclusive-lock no-error.
          wmsb.priority = inPrior.                                                        find first zzbin where zzbin.cono = g-cono and zzbin.prod = icsw.prod
          and zzbin.whse = wmsb.whse and zzbin.binloc = oldBinLoc
          no-lock no-error.
          if not avail zzbin then do:
            create zzbin.
            zzbin.cono = g-cono.
            zzbin.prod = icsw.prod.
            zzbin.whse = icsw.whse.
            zzbin.binloc = oldBinLoc. 
            zzbin.transproc = "ICSWR".
            zzbin.operinit = g-operinits. 
            zzbin.transdt = TODAY.
            zzbin.transtm = replace(STRING(TIME, "HH:MM:SS"),":","").
          end.                                     
        end.                                       
      end.
      **********/
    end.    /* new != old, OldBinlocs  */          
    /* do for binloc1 */
    if thisBin ne "" and thisBin ne "New Part" then do:
      /* set for wrong product */
      find first wmsbp where wmsbp.cono = g-cono + 500 and wmsbp.whse = 
      icsw.whse and wmsbp.prod <> icsw.prod and wmsbp.binloc = thisBin no-lock 
      no-error.   
      if avail wmsbp then do:
        message "Bin " wmsbp.binloc " in use for " wmsbp.prod.
        pause.
        if whichLoc = 1 then do:
          icsw.binloc1 = oldBinLoc. 
          display icsw.binloc1 with frame f-icswr.
          next-prompt icsw.binloc1 with frame f-icswr.
          next.
        end.
        if whichLoc = 2 then do:
          icsw.binloc2 = oldBinLoc.
          display icsw.binloc2 with frame f-icswr.
          next-prompt icsw.binloc2 with frame f-icswr.
          next.
        end.
      end.  
      /* bin exists or security to create bin */  
      if  zy-securecd  < 5 then do:
        find first wmsb where wmsb.cono = g-cono + 500 and wmsb.whse = icsw.whse
        and wmsb.binloc = thisBin no-lock no-error.                         
        if not avail wmsb then do:
          message "Bin " thisBin " does not exist".                         
          pause.        
          if whichLoc = 1 then icsw.binloc1 = obinloc1.
          if whichLoc = 2 then icsw.binloc2 = obinloc2.
          display icsw.binloc1 icsw.binloc2 with frame f-icswr.
          if whichLoc = 1 then next-prompt icsw.binloc1 with frame f-icswr.
          if whichLoc = 2 then next-prompt icsw.binloc2 with frame f-icswr.
          next.
        end.
      end.
      /* right product or DNE */
      find first wmsbp where wmsbp.cono = g-cono + 500 and wmsbp.whse = 
      icsw.whse and wmsbp.prod = icsw.prod and wmsbp.binloc = thisBin no-lock  
      no-error. 
      /* if avail good, else */ 
      if not avail wmsbp then do:
        create wmsbp.
        wmsbp.cono = g-cono + 500.
        wmsbp.whse = icsw.whse.
        wmsbp.prod = icsw.prod.
        wmsbp.binloc = thisBin.                                             
        {t-all.i wmsbp}
        wmsbp.transproc = "ICSWR".
     
      end.
      find first wmsb where wmsb.cono = g-cono + 500 and wmsb.whse = icsw.whse
      and wmsb.binloc = thisBin exclusive-lock no-error.
      if not avail wmsb then do:
        create wmsb.
        wmsb.cono = g-cono + 500.
        wmsb.whse = icsw.whse.
        wmsb.binloc = thisBin.                                              
        wmsb.priority = whichLoc.
        wmsb.statuscode = "A".
        wmsb.assigncode = icsw.statustype.
      end.
      else do:
        if wmsb.priority > 2 then do:
          /* delete zzbin, bin moved to icsw */
          find first zzbin where zzbin.cono = g-cono and zzbin.prod = icsw.prod
          and zzbin.whse = icsw.whse and zzbin.binloc = wmsb.binloc
          exclusive-lock no-error.
          if avail zzbin then do:
            delete zzbin.
          end.
        end.
        if icsw.prod ne "" then do:
          wmsb.statuscode = "A".
          wmsb.priority = whichLoc.
          wmsb.assigncode = icsw.statustype.
        end.
      end.  /* avail wmsb */
    end. /* binloc1 ne "" */
  end. /* loop whichLoc 1 to 2 */
                    
  if ostatus ne icsw.statustype then do:
    /* zzbins, wmsb1 and 2 should be okay - only wmsb has statustype */
    for each wmsbp where wmsbp.cono = g-cono + 500 and wmsbp.whse = icsw.whse
    and wmsbp.prod = icsw.prod no-lock:
      find wmsb where wmsb.cono = wmsbp.cono and wmsb.whse = wmsbp.whse
      and wmsb.binloc = wmsbp.binloc exclusive-lock no-error.
      if avail wmsb then
        wmsb.assigncode = icsw.statustype.
    end.
  end.                    
end. /* if we are processing a Managed Warehouse */

/{&user_aftedit}*/

&if defined(user_canceled) = 2 &then
  /*das - zicp*/
  assign v-lp = yes
         v-bp = yes
         v-rc = yes
         v-sc = yes
         v-pt = yes
         v-st = yes.
  run zsdizicpupdt.p(input icsw.whse,     
                           icsw.prod,     
                           icsw.avgcost,  
                           icsw.baseprice,
                           icsw.listprice,
                           icsw.replcost, 
                           icsw.stndcost,
                           icsw.pricetype,
                           icsw.statustype,
                           v-lp,
                           v-bp,
                           v-rc,
                           v-sc,
                           v-pt /* ,
                           v-st */).
  /*das - zicp*/
&endif
  
&if defined(user_deleted) = 2 &then
/*das - zicp*/
for each zicp where zicp.cono = g-cono and
                    zicp.whse = g-whse and
                    zicp.prod = g-prod:
  delete zicp.
end.
/*das - zicp*/
/{&comment}* */
{icsd.gfi &whse = g-whse &lock = "no" &b = "zx-"}
if ENTRY(1, zx-icsd.user4) = "Y" 
 /* TAH 011707 */   
   or ENTRY(1, zx-icsd.user4) = "P" then do: 

/* TAH 011707 */   

  for each wmsbp use-index k-wmfifo where
    wmsbp.cono = g-cono + 500 and
    wmsbp.whse = g-whse and
    wmsbp.prod = g-prod 
    and (wmsbp.binloc = s-binloc1 or wmsbp.binloc = s-binloc2)
    exclusive-lock transaction:
    /*tb 14046 03/03/94 mtt;Need t-all processing throughout Trend*/
    find wmsb where wmsb.cono = g-cono + 500 and wmsb.whse = g-whse 
    and wmsb.binloc = wmsbp.binloc exclusive-lock no-error.              
    if avail wmsb then wmsb.statuscode = "".                           
    {delete.gde wmsbp}
  end.
  hide message no-pause.
end. /* if a Managed Warehouse */
&endif
