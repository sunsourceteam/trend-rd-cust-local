/******* kpzp9g.p    ********************************************************
         grove item label - also could be generic item label if needed.
         run from kpzp.p

         12/05/2008 dkt fixed path
****************************************************************************/

define input parameter zp-cpono     as char format "x(24)".
define input parameter zp-custno    like oeeh.custno.
define input parameter zp-custprod  like poel.shipprod.
define input parameter zp-poline    like poel.lineno.
define input parameter zp-qty       like poel.stkqtyord.
define input parameter zp-ordnbr    as char format "x(10)".
define input parameter zp-printernm like sasp.printernm.

{g-all.i}
{g-po.i}
{g-kp.i}
def var x-inx as integer                      no-undo.
def var v-cpono as char format "x(12)"        no-undo.
def var v-posuf like poeh.posuf               no-undo.
def var v-poline like poel.lineno             no-undo.
def var v-qty    like poel.stkqtyord          no-undo.
def var v-filename as c format "x(30)"        no-undo.
def var v-tilda as c format "x" init "~~"     no-undo.
def var v-custprod like  poel.shipprod        no-undo.
def var v-printername as c                    no-undo.
def var v-labelname as c                      no-undo.
def var v-wononbr   as c   format "x(10)"     no-undo.
def var v-ordnbr    as c   format "x(10)"     no-undo.
def var v-serial    as int format "999999999" no-undo.
def var v-custpo    like oeeh.custpo          no-undo.
def var v-custno    like oeeh.custno          no-undo.
def var v-wono      like kpet.wono            no-undo.
def var v-wosuf     like kpet.wosuf           no-undo.
def var v-outputfl  as logical                no-undo.
def var v-quantity  like poel.qtyrcv          no-undo. 
def var x-quantity  like poel.qtyrcv          no-undo. 
def var x-labels    as integer                no-undo.
def var z-labels    as integer                no-undo.
def var newtype     as logical.
/* record layout for new custom-txdata */
def var custom-01   as char format "x(30)"    no-undo. 
def var custom-02   as char format "x(30)"    no-undo. 
def var custom-03   as char format "x(30)"    no-undo. 
def var custom-04   as char format "x(30)"    no-undo. 
def var custom-05   as char format "x(30)"    no-undo. 
def var custom-06   as char format "x(30)"    no-undo. 
def var custom-07   as char format "x(30)"    no-undo. 
def var custom-08   as char format "x(30)"    no-undo. 
def var custom-09   as char format "x(30)"    no-undo. 
def var custom-10   as char format "x(30)"    no-undo. 
def var custom-11   as char format "x(30)"    no-undo. 
def var custom-12   as char format "x(30)"    no-undo. 

def stream bcd.

assign v-cpono     = zp-cpono
       v-custno    = zp-custno
       v-custprod  = zp-custprod
       v-qty       = zp-qty
       v-poline    = zp-poline
       v-ordnbr    = zp-ordnbr.           

find icsp where icsp.cono = g-cono and 
                icsp.prod = v-custprod
                no-lock no-error.

if not avail icsp then do: 
 message "Invalid Trend Item Nbr - pls re-enter " v-custprod.
 pause.
 put screen row 22 col 1 color message 
"    F7-Grove Item                                                             ".
 next.
end.

if avail icsp then do: 
 assign newtype = no. 
 find first zsdibarcd where 
     zsdibarcd.cono    = g-cono  and 
     zsdibarcd.type    = "xyxy"  and 
     zsdibarcd.apptype = "grove" and 
     zsdibarcd.labelnm ne "" 
     no-lock no-error. 
 if avail zsdibarcd and zsdibarcd.style = "new" then do:  
  if zsdibarcd.labelnm = "" then do: 
   message "No label for new style barcode..". 
   pause 2.
   return. 
  end. 
  assign newtype = yes
         v-printername = (if zsdibarcd.printernm[1] ne "" then 
                           zsdibarcd.printernm[1] 
                          else 
                           v-printername)
           v-labelname = zsdibarcd.labelnm.                       
 end.
end.
        
find icsec where icsec.cono = g-cono and 
     icsec.rectype = "c" and
     icsec.custno  = v-custno and 
     icsec.altprod = v-custprod
     no-lock no-error.
      
if avail icsec then
 v-custprod = icsec.prod.
else
if not avail icsec then do: 
  message "Invalid Cross Reference - pls re-enter ".
  message  v-custno " " v-custprod.
  pause.
  put screen row 22 col 1 color message 
"    F7-Grove Item                                                             ".
  v-custprod = icsec.altprod.
  next.
end.

assign v-filename = ""
       v-outputfl = false 
       v-printername = zp-printernm.

 if v-filename = "" then do: 
  nameloop: 
  do while true:
   v-filename = "/usr/tmp/" + string(time) + string(random(1,999)).
   if search(v-filename) = ? then
    leave nameloop.
  end.
 end.
 output stream bcd to value(v-filename).
 
 if not newtype then 
  assign v-labelname = "groveitem.lbl".

 assign v-outputfl = true.

 if not newtype then do:  
  put stream bcd unformatted
  "/L=" 
   v-labelname
  " "
  "/P="
   v-printername  
  chr(13)
  chr(10).
 end. 
 
 /** new zsdibarcd table format **/ 
 if avail zsdibarcd and newtype then do: 
   put stream bcd unformatted
    "/L=" 
    v-labelname
     " "
     "/P="
    v-printername 
   /** bc2000 .ldd file name **/
     " /DR"
     zsdibarcd.txdatanm
     " /C=" 
     zsdibarcd.copies
     " "
    chr(13)
    chr(10).
 end.                       

 put stream bcd unformatted
  v-cpono
   v-tilda
  v-custprod
   v-tilda
  v-poline   
   v-tilda
  v-qty
   v-tilda
  v-ordnbr
   v-tilda
  chr(13)
  chr(10).

 if v-outputfl = true then do: 
  output stream bcd  close.
  unix silent value("sh /rd/unibar/ibprint.sh " + v-filename).
  unix silent value ("rm " + v-filename).
 end.

leave.
