                        
  if z-icsp.prodcat < zzl_begcat or
     z-icsp.prodcat > zzl_endcat then
     next dataloop.

       
  zelection_type = "p".
  zelection_char4 = z-icsp.prodcat.
  zelection_cust = 0.
  run zelectioncheck.
  if zelection_good = false then
    do:
    next dataloop.
    end.


  assign 
    t-whse = z-icsw.whse
    t-prod = z-icsw.prod
    t-buyer = ""
    t-prodline = z-icsw.prodline
    t-vendor = z-icsw.arpvend
    t-search = 0.
  
 
 
  if z-icsp.kittype = "p" and z-icsw.arpvend = 0 and z-icsw.prodline = "" then
    do:
    find first kpsk where kpsk.cono = g-cono and
                          kpsk.prod = z-icsw.prod no-lock no-error.
    if avail kpsk then
      do:                    
      find first w-icsw where w-icsw.cono = g-cono and
                              w-icsw.prod = z-icsw.prod no-lock no-error.
      if avail w-icsw then
        assign 
          t-whse = w-icsw.whse
          t-prod = w-icsw.prod
          t-buyer = ""
          t-prodline = w-icsw.prodline
          t-vendor = w-icsw.arpvend
          t-search = 0.
      end.
    end.
    
  
  {p-zfindbuygne2.i
     t-whse
     t-prod
     t-buyer
     t-vendor
     t-prodline
     t-search
     }
  
  
  if t-buyer = "" then
     t-buyer = substring(z-icsd.user3,15,4).
  
  if t-prodline ge b-pline and
     t-prodline le e-pline then
     t-search = 0.
  else
     next dataloop.

 
  zelection_type = "b".
  zelection_char4 = t-buyer.
  zelection_cust = 0.
  run zelectioncheck.
  if zelection_good = false then
    do:
    next dataloop.
    end.

 
  zelection_type = "v".
  zelection_vend = t-vendor.
  zelection_char4 = " ".
  run zelectioncheck.
  if zelection_good = false then
    do:
    next dataloop.
    end.

  
  run zdmdwhse (input z-icsw.whse,input z-icsw.prod).
  assign w-urate = 0.
  run get-URate (input z-icsw.whse,
                 input z-icsw.prod,
                 input-output w-urate).

 
  
  
  create buyicsw.
  assign
    buyicsw.buyer        = t-buyer
    buyicsw.prod         = z-icsw.prod
    buyicsw.whse         = z-icsw.whse
    buyicsw.statustype   = z-icsw.statustype
    buyicsw.um           = z-icsw.unitbuy
    buyicsw.vendor       = t-vendor
    buyicsw.prodline     = t-prodline
    buyicsw.qtyavbl      = (z-icsw.qtyonhand
                            - z-icsw.qtyreservd 
                            - z-icsw.qtycommit) 
    buyicsw.onhand       = z-icsw.qtyonhand
    buyicsw.qtycommit    = z-icsw.qtycommit
    buyicsw.qtyreserved  = z-icsw.qtyreservd
    buyicsw.oo           = z-icsw.qtyonorder
    buyicsw.co           = z-icsw.qtybo
    buyicsw.ytdissue     = z-icsw.issueunytd
    /*buyicsw.ptdissue     = z-icsw.usagerate*/
    buyicsw.ptdissue     = w-urate
    buyicsw.leadtime     = z-icsw.leadtmavg
    buyicsw.min          = z-icsw.orderpt
    buyicsw.ordqty       = z-icsw.ordqtyin
    buyicsw.max          = z-icsw.linept
    buyicsw.replcst      = z-icsw.replcost
    buyicsw.method       = z-icsw.ordcalcty
    buyicsw.avgcost      = z-icsw.avgcost
    buyicsw.listprc      = z-icsw.listprice
    buyicsw.zdescr       = z-icsp.descrip[1].
    do v-inxv = 1 to 7:
      buyicsw.whsestuff[v-inxv]   = x-whseqty[v-inxv].
    end.
    /*
    v-inxv =0.
    Whse_loop:
    for each z2-icsw where z2-icsw.cono = g-cono and 
                           z2-icsw.prod = z-icsw.prod and
                           z2-icsw.whse <> z-icsw.whse and
                           (z2-icsw.qtyonhand - 
                            z2-icsw.qtyreservd -
                            z2-icsw.qtycommit) > 0   no-lock
       
                       by
                        (z2-icsw.qtyonhand -
                         z2-icsw.qtyreservd - 
                         z2-icsw.qtycommit) descending: 
      
      if (z2-icsw.qtyonhand - z2-icsw.qtyreservd - z2-icsw.qtycommit) > 0 
        then
        do:
        if v-inxv ge 7 then
          leave Whse_loop.
        
        v-inxv = v-inxv + 1.                  
        buyicsw.whsestuff[v-inxv] = string(z2-icsw.whse,"x(4)") 
                                    + " - " 
                                    + string((z2-icsw.qtyonhand - 
                                            z2-icsw.qtyreservd -
                                            z2-icsw.qtycommit),">>>>>9") 
                                           + ",". 
        end.
      if v-inxv <> 0 then
        substring(buyicsw.whsestuff [v-inxv],14,1)  = " ". 
    end.
 */
 