/* d-wtep1.i */
/* Display for wtep1 and wtep1wm */
/* changes

   02/03/09 dt01 findfirstbin.p */


/* This is the common line display code for wmep1 and wtep1wm */

/* comt will be used to comment out parts not used when sorting in binloc
        order
   comb will be used to comment out parts not used when sorting in product or
        line order
   coms will be used to comment out parts not used when sorting in product or
        line order */

v-linecnt = v-linecnt + 1.

/* Skip the print of this line if we are not supposed to print 0
   qty shipped lines */
if v-oelinefl = no and wtel.stkqtyship = 0
then do:

    v-noprint = v-noprint + 1.
    next.
end.

/* If a pick ticket reprint is requested and this line has not been 
   changed since the last pick, skip it */
if v-reprintfl = yes and p-reprintfl = yes and wtel.printfl = no
then do:

    v-noprint = v-noprint + 1.
    next.

end.

/* Load line variables */
if wtel.nonstockty <> "n"
then do:
    find icsw where
         icsw.cono = wtel.cono       and
         icsw.prod = wtel.shipprod   and
         icsw.whse = wtel.shipfmwhse
    no-lock no-error.

    {w-icsp.i wtel.shipprod no-lock}
end.


if wtel.nonstockty = "n"  then do:
  /* dt01 02/03/09
  for each wmsbp where 
           wmsbp.cono = 500 + g-cono and
           wmsbp.whse = wtel.shipfmwhse and 
           wmsbp.prod = wtel.shipprod no-lock,
      each wmsb where 
           wmsb.cono = wmsbp.cono and 
           wmsb.whse = wmsbp.whse and
           wmsb.binloc = wmsbp.binloc no-lock break by wmsb.priority:
        leave.
      end.
      */
      
      define var wmsbid  as recid initial ? no-undo.
      define var wmsbpid as recid initial ? no-undo.
      run findfirstbin.p(input g-cono, input wtel.shipfmwhse,
                         input wtel.shipprod,
                         output wmsbid, output wmsbpid).

      if wmsbid ne ? and wmsbpid ne ? then do:
        find wmsbp where recid(wmsbp) = wmsbpid no-lock no-error.
        find wmsb  where recid(wmsb)  = wmsbid  no-lock no-error.
      end.                                                       
  
{w-icsw.i wtel.shipprod wtel.shipfmwhse no-lock}
{w-icsp.i wtel.shipprod no-lock}
end. 

/* tb 18091 11/03/95;  Find product section of UPC number */
if wtel.nonstockty = "n" or wtel.vendno <> 0 then
    {icsv.gfi "'u'" wtel.shipprod wtel.vendno no}
else if avail icsw then
    {icsv.gfi "'u'" wtel.shipprod icsw.arpvendno no}

/* tb 18091 11/03/95;  Assign product section of UPC Number */
assign
    v-wmfl      = if wtel.nonstockty <> "n" and avail icsw
                      {&coms}
                      and w-binorder.w-wmid <> 0
                      /{&coms}* */
                      then icsw.wmfl
                  else no
    {&comt}
    a-totqtyshp = a-totqtyshp + wtel.qtyship
    /{&comt}* */
    v-conv      = /*tb 13520 11/05/93;  conv to 10 dec */
                {unitconv.gas &decconv = "(wtel.stkqtyord / wtel.qtyord)"}
    s-qtybo     = if wtel.bono > 0 then 0
                  else if wtel.bofl and wtel.qtyord > wtel.qtyship
                      then wtel.qtyord - wtel.qtyship
                  else 0
    s-qtyship   = (wtel.stkqtyship -
                      (if p-reprintfl = yes then wtel.prevqtyshp else 0))
                      / v-conv
    s-qtyship   = if s-qtyship < 0 then 0 else s-qtyship
    v-qtyship   = s-qtyship
    s-qtyord    = wtel.qtyord
    s-lineno    = string(wtel.lineno,"zz9")
/*    s-netamt    = wtel.netamt */
    s-netamt    = if avail icsw and wteh.transtype <> "do" 
                     then icsw.avgcost
                  else 0
    s-binloc    = if wtel.nonstockty = "n" and not avail wmsb then 
                    "Non Stock"
                  else if avail icsw then
                    string(icsw.binloc1,"xx/xx/xxx/xxx")
                  else if avail wmsb then
                    string(wmsb.binloc,"xx/xx/xxx/xxx") 
                  else 
                    "".
    
    s-upcpno    = if avail icsv then
                      /* Assign a Section of a UPC Number */
                      {upcno.gas
                          &section  = "4"
                          &sasc     = "v-"
                          &icsv     = "icsv."
                          &comdelim = "/*"}
                  else "00000".

/*tb 19484 (B2b) 08/08/96; Warehouse Logistics */
/*tb 24141 11/07/97; F10-Print of WT in PMEP causes WL error */
/* Warehouse Logistics processing for lines */
if v-wlwhsefl      = yes and
    wteh.stagecd   < 3   and
    wteh.wletsetno = " " then
do:

    /*tb 19484 10/30/96; (B13) Warehouse Logistics - Shipmentid*/
    /*tb 22490 01/24/97; Download not sent from WTIA - invoutcond */
    /* Create the WLEL records */
    {wlpproc.gpr
        &invoutcond   = "true"
        &wlsetno      = v-wlsetno
        &whse         = wteh.shipfmwhse
        &ordertype    = ""t""
        &orderno      = wtel.wtno
        &ordersuf     = wtel.wtsuf
        &lineno       = wtel.lineno
        &seqno        = 0
        &msdssheetno  = """"
        &updtype      = ""A""
        &shipmentid   = """"}
end.

/* Set the back ordered qty */
if wtel.bono > 0 then do:

    find b-wtel where
         b-wtel.cono   = g-cono       and
         b-wtel.wtno   = wtel.wtno    and
         b-wtel.wtsuf  = wtel.bono    and
         b-wtel.lineno = wtel.lineno
    no-lock no-error.

    if avail b-wtel then s-qtybo = b-wtel.qtyord.

end.

/* PickPack */
{p-zsdipckwt.i
                    &comr       = "/*"
                    &coms       = "/*"
                    &returnfl   = "wtel.returnfl"
                    &cubes      = "wtel.cubes"
                    &weight     = "wtel.weight"
                    &head       = "wteh"
                    &line       = "wtel"
                    &section    = "wteh"
                    &ourproc    = "wtep"
                    &specnstype = "wtel.nonstockty"
                    &altwhse    = "wtel.shipfmwhse"
                    &vendno     = "wtel.vendno"
                    &kitfl      = "x-false"
                    &botype     = "wtel.botype"
                    &orderdisp  = "' '"
                    &prevqtyship= "wtel.prevqtyshp"
                    &netamt     = "wtel.netamt"
                    &prtpricefl = "wteh.pickprtfl"
                    &bono       = "wtel.bono"
                    &orderno    = "wtel.wtno"
                    &ordersuf   = "wtel.wtsuf"
                    &custno     = "wteh.custno"
                    &corechgty  =  "0"
                    &qtybo      =  "0"
                    &ordertype  = "t"
                    &prefix     = "transfer"
                    &seqno      = "0"
                    &stkqtyshp  = "wtel.stkqtyship"
                    &nosnlots   = "wtel.nosnlots"
                    &comtype    = "wt"
                    &comlineno  = "wtel.lineno"
                    &shipto     = "wteh.shipto"
                    &whse       = "wtel.shipfmwhse"
                    &twlstgedit = "  "} .
 



/* Pick Pack */

if icsdAutoXdock = "Y" then do:
  find first zsdipckdtl use-index k-ordix where 
             zsdipckdtl.cono = g-cono          and
             zsdipckdtl.ordertype = "t"        and
             zsdipckdtl.whse = wtel.shipfmwhse and 
             zsdipckdtl.orderno = wtel.wtno    and
             zsdipckdtl.ordersuf = wtel.wtsuf  and
             zsdipckdtl.lineno =wtel.lineno    and
             zsdipckdtl.seqno  = 0             and
             zsdipckdtl.binloc1 = "RECEIVING" no-lock no-error.

    if avail zsdipckdtl then do:
      s-binloc2 = replace(s-binloc,"/","").
      s-binloc = "RECEIVING".
    end.
    else s-binloc2 = "".
end.


/* Display the line if WM has not been purchased or is not used */
/*tb 18091 11/01/95; Change UPC number display */
if not v-wmfl or not v-modwmfl then do:
    if wteh.transtype <> "do" and (substring(wteh.shiptowhse,2,1) =  "x" and
                                   substring(wteh.shipfmwhse,2,1) <> "x") then
    display
        s-lineno
        wtel.shipprod
        wtel.unit
        s-qtyord
        s-qtybo
        s-qtyship
        s-upcpno
        s-binloc
        s-binloc2
        "____________ _____" @ s-underline
        s-netamt
    with frame f-oeel.
   else
    display
            s-lineno
            wtel.shipprod
            wtel.unit
            s-qtyord
            s-qtybo
            s-qtyship
            s-upcpno
            s-binloc
            s-binloc2
            "____________ _____" @ s-underline
    with frame f-oeel.

    if wtel.nonstockty = "n" then do:
    
        if wtel.proddesc <> "" then display wtel.proddesc @ s-descrip
           with frame f-oeel.
           
    end.
    else do:
        
        if (avail icsw and icsw.binloc2 <> "") then
            display
                icsw.binloc2 @ s-binloc2
            with frame f-oeel.
        if not avail icsp or not avail icsw then
            display
                v-invalid @ s-descrip
            with frame f-oeel.
        else
            display
                icsp.descrip[1] @ s-descrip
            with frame f-oeel.
    end.

    down with frame f-oeel.

    if avail icsp and icsp.descrip[2] <> "" and wtel.nonstockty <> "n"
        then
            display 
                icsp.descrip[2] @ s-descrip3
            with frame f-oeel2.

    
/*    &&/   */
    

      
    if wteh.transtype = "do" and wteh.orderaltno <> 0 then do:
      g-cono = wteh.cono2.
/*d Find the associalted WTELO record */
      find first wtelo use-index k-wtelo where
                 wtelo.wtno  = wteh.wtno  and 
                 wtelo.lineno = wtel.lineno and
                 wtelo.ordertype = "o"                 
                 no-lock no-error.
      if avail wtelo then do:
        find first oeel where oeel.cono = g-cono and
                              oeel.orderno = wtelo.orderaltno and
                              oeel.lineno  = wtelo.linealtno and
                              oeel.shipprod = wtel.shipprod no-lock no-error.
      
        if avail oeel then do:
          v-reqtitle = if oeel.xrefprodty = "c" then "   Customer Prod:"
                       else if oeel.xrefprodty = "i" then "Interchange Prod:"
                       else if oeel.xrefprodty = "p" then " Superseded Prod:"
                       else if oeel.xrefprodty = "s" then " Substitute Prod:"
                       else if oeel.xrefprodty = "u" then "    Upgrade Prod:"
                       else "".

          if can-do("c,i,p,s,u",oeel.xrefprodty) and oeel.reqprod <> "" then do:

             s-prod  =   oeel.reqprod.

             display
               s-prod
               v-reqtitle
             with frame f-oeelc.
             down with frame f-oeelc.

             end. /* cross ref prod display */

          if oeel.xrefprodty ne "c" then do:

            s-prod = if oeel.reqprod ne "" then oeel.reqprod
                     else wtel.shipprod.

            {n-icseca.i "first" s-prod ""c"" oeel.custno no-lock}

            if avail icsec then do:

              assign
                s-prod     = icsec.prod
                v-reqtitle = "   Customer Prod:".

              display
                s-prod
                v-reqtitle
              with frame f-oeelc.

              end.

            end. /* xrefprodty display */
         end.
       end.
     end.
/*    TAH 11/20/06  */
    
    else  
    if v-wtcustno > 0 then do:
      assign g-cono = wteh.cono2
             s-prod = wtel.shipprod.
       {n-icseca.i "first" s-prod ""c"" v-wtcustno no-lock}

        if avail icsec then do:
          assign
            s-prod     = icsec.prod
            v-reqtitle = "   Customer Prod:".

          display
            s-prod
            v-reqtitle
           with frame f-oeelc.

        end.
     end.
/*    TAH 11/20/06  */

    
 
    
    
    
    
    s-lastfl = yes.
end. /* if not WM */

/* Process for a WM product */
else do:
    /* Assignments for display if displaying in binloc order */
    {&coms}
    assign
        s-netfl   = w-binorder.w-netfl
        s-lastfl  = w-binorder.w-lastfl
        s-qtyship = if avail wmet then wmet.qtyactual / vconv
                    else 0
        s-qtyship = if s-qtyship < 0 then 0
                    else s-qtyship
        v-qtyship = if avail wmet then wmet.qtyactual / v-conv
                    else 0
        s-binloc  = if avail wmet then
                        string(wmet.binloc,"xx/xx/xxx/xxx")
                    else "".
    /{&coms}* */

    /* Assignments for display if not displaying in binloc order */
    {&comt}
    assign 
        s-netfl   = yes
        s-lastfl  = yes
        s-qtyship = 0 /* So they don't pick it twice */
        v-frameln = 1
        s-binloc  = if wtel.nonstockty = "n" then "Non Stock"
                    else "Whse Mgr".
    /{&comt}* */

    /* Display the line item */
    /*tb 18091 11/01/95; Change UPC number display */
    if wteh.transtype <> "do" and (substring(wteh.shiptowhse,2,1) =  "x" and 
                                   substring(wteh.shipfmwhse,2,1) <> "x") then
    display
        s-lineno
        wtel.shipprod
        s-upcpno
        s-binloc
        s-qtyord
        s-qtybo
        wtel.unit
        "____________ _____"        @ s-underline
        s-netamt
        {&comb}
        s-qtyship
        "Whse Mgr"                  @ s-binloc2
        /{&comb}* */
        {&comt}
        "" when s-qtyord <> s-qtybo @ s-qtyship
        0 when s-qtyord = s-qtybo   @ s-qtyship
        /{&comt}* */
    with frame f-oeel.
    else
    display                                       
        s-lineno                                  
        wtel.shipprod                             
        s-upcpno                                  
        s-binloc                                  
        s-qtyord                                  
        s-qtybo                                   
        wtel.unit                                 
        "____________ _____"        @ s-underline 
        {&comb}                                   
        s-qtyship                                 
        "Whse Mgr"                  @ s-binloc2   
        /{&comb}* */                              
        {&comt}                                   
        "" when s-qtyord <> s-qtybo @ s-qtyship   
        0 when s-qtyord = s-qtybo   @ s-qtyship   
        /{&comt}* */                              
    with frame f-oeel.                            
    

    {&comb}
    assign
        s-wminfo = "(of " + string(wtel.qtyship) +
                      " Total)".
    display
        icsp.descrip[1] @ s-descrip
        s-wminfo        @ s-descrip2
    with frame f-oeel.
    down with frame f-oeel.
    /{&comb}* */

    /* Display the WM bins to pick from */
    {&comt}
    for each wmet use-index k-ord where
             wmet.cono      = wtel.cono   and
             wmet.ordertype = "t"         and
             wmet.orderno   = wtel.wtno   and
             wmet.ordersuf  = wtel.wtsuf  and
             wmet.lineno    = wtel.lineno and
             wmet.seqno     = 0
    no-lock:

        if v-frameln = 1 then do:

            assign 
                s-wmqtyship = wmet.qtyactual / v-conv
                s-wmqtyship = if s-wmqtyship < 0 then 0
                              else s-wmqtyship
                s-descrip2 = "                         " +
                                  string(s-wmqtyship,">>>>>>>>9.99-")
                s-binloc2  = wmet.binloc.

            display
                icsp.descrip[1] @ s-descrip
                s-binloc2
                s-descrip2
            with frame f-oeel.
            down with frame f-oeel.

            v-frameln = v-frameln + 1.
        end. /* if v-frameln = 1 */

        else do:
            assign 
                s-wmqtyship = wmet.qtyactual / v-conv
                s-wmqtyship = if s-wmqtyship < 0 then 0
                              else s-wmqtyship
                s-binlocwm  = wmet.binloc.
            display
                s-binlocwm
                s-wmqtyship
            with frame f-oewm.
            down with frame f-oewm.
        end.
    end. /* for each wmet */
    
    if wtel.stkqtyship = 0 then do:

        display
            wtel.proddesc when wtel.nonstockty = "n" @ s-descrip
            icsp.descrip[1] when wtel.nonstockty = "" @ s-descrip
        with frame f-oeel.
        down with frame f-oeel.

    end. /* if wtel.stkqtyship = 0 */

    /{&comt}* */

end. /* else do WM */

if wteh.transtype = "do" then
  run zsditariffprt (g-cono,wtel.shipprod,wtel.shiptowhse,"wtepp",8).
else
  run zsditariffprt (g-cono,wtel.shipprod,wtel.shipfmwhse,"wtepp",8).


  
  /* PickPack */
    if not v-tovid then do:
       run bar-code-pick (input wtel.shipprod). 
     end.
  /* PickPack */


  
  

/* Print the product notes */
if wtel.nonstockty <> "n" and
    avail icsp            and
    icsp.notesfl <> ""
then
    for each notes where
             notes.cono         = g-cono    and
             notes.notestype    = "p"       and
             notes.primarykey   = icsp.prod and
             notes.secondarykey = ""        and
             notes.printfl      = yes
    no-lock:

        do i = 1 to 16:
            if notes.noteln[i] <> "" then do:
                display notes.noteln[i] with frame f-notes.
                down with frame f-notes.
            end. 
        end.
    end. /* for each notes */

/* Print the serial/lot records */
/*tb 13356 04/27/95 mtt; Serial/Lot # doesn't print on PO/RM */
{p-oeepp1.i
    &type       = "t"
    &line       = "wtel"
    &pref       = "wt"
    &seq        = 0
    &stkqtyship = "wtel.stkqtyship"
    &snlot      = "wtel.nosnlotso"}

/* Print the line item comments */
if wtel.commentfl = yes then do:

    {w-com.i ""wt"" wtel.wtno wtel.wtsuf wtel.lineno yes no-lock}
    
    if avail com then do i = 1 to 16:
        if com.noteln[i] <> "" then do:
            display com.noteln[i] with frame f-com.
            down with frame f-com.
        end.
    end.
end.

/* Reset the line item print flag */
/*tb 21254 06/27/96;  adding the parameter for &seqno, it's a zero for WT */
if wtel.printfl = yes and s-lastfl then
    /* Update during Pick Ticket Print */
    run oeeppu.p("t",
                 {c-empty.i},
                 " ",
                 " ",
                 today,
                 output v-printfl,
                 wtel.wtno,
                 wtel.wtsuf,
                 wtel.lineno,
                 0).
