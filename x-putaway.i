/*******************************************************************************
  PROCEDURE      : x-putaway.i
  DESCRIPTION    : 
  AUTHOR         : Sunsource
  DATE WRITTEN   : 04/01/07
  CHANGES MADE   :


*******************************************************************************/

{g-xxex.i "new shared"}
{g-ic.i  "new"}
{g-po.i  "new"}
{g-conv.i}
{g-jump.i}
{speccost.gva "new shared"}

/* report includes */
{g-sapb.i new}   
{g-rpt.i}        
{g-rptctl.i new} 
def var v-reportid          like sassr.reportid   no-undo. 
def var v-openprfl          like sassr.openprfl  no-undo.   
def var v-openprno          like sassr.openprno   no-undo. 
def var v-scrollmode        as logical     no-undo.
def var v-completescroll    as logical     no-undo.
def var v-position          as integer no-undo.
def var v-offset            as integer no-undo.
def var v-move              as integer no-undo.
def var v-reposition        as logical no-undo.

def var v-reload            as logical no-undo.
def var v-reloadID          as recid   no-undo.

def var v-prompt       as c                  no-undo.   
def var prompt-sw      as l                  no-undo.   
def var prodLoop       as logical initial yes no-undo.
def var binLoop        as logical initial yes no-undo.
def var advance        as logical initial yes no-undo.

def var o-frameval     as c                  no-undo.
def var v-returnfl     as l                  no-undo.
def var v-emptyl       as l                  no-undo.
/* def var v-errfl        as c format "x"       no-undo. */
def var zx-okfl        as logical            no-undo.
def var zx-error       as integer            no-undo.
def var zx-suff        like poeh.posuf       no-undo.
def var v-keys         as c                  no-undo.
def var v-key          as c                  no-undo.
def var v-clearfl      as log                no-undo.
def var z-lastkey      as i                  no-undo.
def var z-loadcharfl   as log                no-undo.
def var v-start        as char format "x(24)"  no-undo.
def var v-editorfl     as logical            no-undo.
def var v-searchfl     as logical            no-undo.
def var v-srchfnd      as logical            no-undo.
def var x-spce         as c    format "x"    no-undo.

define var v-titlelit   as char format "x(26)"          no-undo.

define var h as handle.
define var p-rowid as rowid.             
def var cRcptId as dec no-undo.
def var iter as int no-undo.

def var v-errorno    as i                 no-undo.
def var v-openinit   like poeh.openinit   no-undo.
def var v-bintpeB   as char format "x(6)" no-undo. 
def var v-bintpeE   as char format "x(6)" no-undo. 

def var v-putawayinit like poeh.openinit      no-undo.  
def var v-receiptid   like zsdiput.receiptid  no-undo. 
def var v-whse        like zsdiput.whse       no-undo. 

def var v-shipprod    like poel.shipprod      no-undo.  
def var v-binloc      as c format "xx/xx/xxx/xxx"  no-undo.  
def var h-shipprod    like poel.shipprod      no-undo.  
def var h-binloc      as c format "xx/xx/xxx/xxx"  no-undo.  
def var h-message     as c format "x(26)" no-undo.  


def var v-actioncode  as c no-undo.
def var v-extype      as c format "x(2)" no-undo.
def var v-editnochhoose as logical no-undo init no.
def var loaded-sw    as logical              no-undo. 
def var zx-l         as logical              no-undo. 
def var zx-x         as integer              no-undo. 

def var srch-flg  as logical              no-undo. 
def var v-notsrch as logical              no-undo. 
def var v-okfl as logical                 no-undo. 

def buffer b-zsdiput for zsdiput.

def var  t-shipprod       like zsdiput.shipprod no-undo.
def var  t-proddesc1      like zsdiput.proddesc1 no-undo.
def var  t-proddesc2      like zsdiput.proddesc2 no-undo.
def var  t-stkqtyputaway  like zsdiput.stkqtyputaway no-undo.

def var  t-stkqty         like zsdiput.stkqtyputaway no-undo.
def var  t-binloc1        like zsdiput.binloc1 no-undo.
def var  t-binloc2        like zsdiput.binloc2 no-undo.

def var  w-binloc1        like zsdiput.binloc1 no-undo.
def var  w-binloc2        like zsdiput.binloc2 no-undo.
def var  h-binsort        as character         no-undo.

def var  w-errorlit       as char format "x(24)" no-undo.
def var  v-listlit        as char format "x(10)" no-undo.
define var idcnt as int initial 0 no-undo.
/* def var putno as int initial 0 no-undo. */
def var doneFlag as logical initial no no-undo.
/* used for F6 exception check for generateReport in .led and .lfu */


def var v-direction      as character format "X(1) " init "F" no-undo.
def var who as char extent 10 no-undo.
def var unixCmd as char no-undo.      
def var errFlag as logical initial no no-undo.
def var exceptCode as char format "x(2)" no-undo.

def var nothing as char no-undo.
/* define new shared var v-sapbid as  recid     no-undo. */

/* The list of receipt ids we are looking at */
define temp-table tmp-rcpt no-undo
  field receiptid like zsdiput.receiptid
  index k-rcpt
    receiptid.


/* The product/qty list of current items
   This way can go from summary to detail */
define temp-table tsum-put no-undo                     
  field prod like zsdiput.shipprod     
  field qty  like zsdiput.stkqtyputaway
  field binloc  like zsdiput.binloc1
  field binloc1 like zsdiput.binloc1
  field binloc2 like zsdiput.binloc2
  field zone   like zsdiput.locationid
  field completefl as logical 
  field split      as logical 
  field extype   like zsdiput.extype
/*  field putno as int */
  field binsort as char
  field vendprod like icsw.vendprod
  field vendno   like apsv.vendno
  field qtyrcvd as dec
  index k-tsum
    prod
  index k-bin
    binloc1
  index k-tbin
    zone
    binsort
/*    zone                 */
    binloc1 
    prod
  index k-tbinx
    binsort
    zone
    binloc1 
    completefl 
    split      
    extype     
    prod
   
  index kd-tbin
    zone       descending
    binsort    descending
/*    zone       descending */
    binloc1    descending
    prod       descending.



define temp-table t-binlist no-undo
  field prod    like icsw.prod
  field whse    like icsw.whse
  field binloc  like icsw.binloc1
  field binnum  as integer
  field binused as logical
index ix-prod
      prod
      whse
      binloc
      binnum
index ix-binnum
      prod
      whse
      binnum
      binloc.
  
define buffer f-tsum-put for tsum-put.

define buffer b-tr for tmp-rcpt.
define query q-tr for b-tr scrolling.
define browse b-rcpt query q-tr
display
  b-tr.receiptid
with size-chars 24 by 4 down
centered no-hide no-labels no-box no-row-markers.
form
  b-rcpt at 1                    
with frame f-rcpt row 7 width 26 no-hide no-labels.

define buffer b-trcpt for tmp-rcpt.
define query q-trcpt for b-trcpt scrolling. 
define browse b-idlist query q-trcpt
  display b-trcpt.receiptid
  with size-chars 24 by 8 down                  
centered overlay no-hide no-labels.             
form
  b-idlist     at 1
with frame f-idlist row 3 no-hide overlay title "Receipt Ids" no-labels.


/* an updateable copy of zsdiput */
define temp-table t-put no-undo
  field whse like zsdiput.whse
  field receiptid like zsdiput.receiptid
  field prod like zsdiput.shipprod
  field bin  like zsdiput.putawaybin
  field binloc1 like zsdiput.binloc1
  field binloc2 like zsdiput.binloc2
  field qty  like zsdiput.stkqtyputaway
  field oper like zsdiput.operinit
  field extype   like zsdiput.extype
  field completefl like zsdiput.completefl
  field transdt  like zsdiput.transdt
  field transtm  like zsdiput.transtm
  field zone     like zsdiput.locationid
  field idno     as int
/*  field putno    as int */
  field zput     as recid
  index k-tmpput
    whse zone bin prod idno
  index k-idno
    idno
  index k-prod
    prod binloc1.

define buffer b-tprod for tsum-put.
define query q-tprod for b-tprod scrolling.
define browse b-prodlist query q-tprod
display
   b-tprod.prod
   b-tprod.binloc1 format "x(24)"
with size-chars 26 by 7 down
centered overlay no-hide no-labels no-box . /* no-row-markers. */
def var inSearch as char format "x(24)" no-undo.
def var thisLabel as char format "x(24)" no-undo.
form
  thisLabel at 1 format "x(24)" dcolor 2
  inSearch  at 1
  b-prodlist at 1                    
with frame f-prodlist no-box no-hide overlay no-labels.

/*
def var t-binloc1 as char no-undo.
def var t-binloc2 as char no-undo.
*/

/* dkt March 2008 */
def var partialFlag as log initial no no-undo.
def var p-qtyputaway as dec format "->>>>>>9.99" no-undo.
define frame f-partial
  "Total Qty: "      at 1
  t-stkqtyputaway    at 15
  "Previous Qty: "   at 1
  t-stkqty           at 15
  "Current Qty:"     at 1
  p-qtyputaway       at 14
  skip (1)
  "Bin Location:"    at 1
  t-binloc1          at 15
with title "Partial Putaway" no-labels overlay. 


form 
   " Status:    "        at 1  dcolor 2
   v-listlit             at 14
   "Binloc1:    "        at 1  dcolor 2
   t-binloc1       at 14
   "Binloc2:    "        at 1  dcolor 2
   t-binloc2       at 14
   with frame f-hhepb width 26 row 10 no-box 
   no-hide no-labels overlay.   



define buffer b-tbin for tsum-put.
define query q-tbin for b-tbin scrolling.
define browse b-binlist query q-tbin
display
  b-tbin.binloc1
with size-chars 24 by 10 down
centered overlay no-hide no-labels. /* no-box no-row-markers. */
form
  b-binlist at 1                    
with frame f-binlist row 1 no-hide overlay title "Bins" no-labels.

/* for xx-excustom &file */
define buffer b-t-put for t-put.
define buffer b-tsum-put  for tsum-put.

{{&appname} &temptable = "*" }
{{&appname} &user_vars = "*" }
{{&appname} &B-zz_browse_events = "*" }
{{&appname} &user_ctrlproc = "*"}

assign v-prompt = "R". 

main1: 
/* do while true on endkey undo main1, leave main1:       */
repeat:
  do transaction:
  pause 0 before-hide.        
  {{&appname} &user_display1 = "*" }
  {{&appname} &user_display3 = "*" }

  if {k-jump.i} then do: 
    clear frame f-hhep all no-pause.  
    run unlockZsdi.
    leave main1.  
  end.

  {w-sasoo.i g-operinits no-lock}  
  assign v-putawayinit = g-operinits
         v-whse        = g-whse.
  if v-whse = "" then v-whse = sasoo.whse.       
  /* This loops the whole frame until the flags are set
     and allows jump or cancel to quickly exit the program */
  do while not loaded-sw: 
    if v-prompt = "R" then do:
      view frame f-hhep-h.
      run RcptSelect.
      if loaded-sw then next main1.
    end.
    else
    if v-prompt = "o" then do:
      view frame f-hhep-h.
      run Options_Select.
      assign v-prompt = "R".
    end.

    if {k-jump.i} or {k-cancel.i} then do: 
      run unlockZsdi.
      clear frame f-hhep   all no-pause.  
      clear frame f-hhep-h all no-pause.  
      leave main1.  
    end.
    next main1.
  end.
  end. /*    transaction. */
  assign g-lkupfl  = yes.
  main:
  repeat       on endkey undo main, leave main
               on error undo main, leave main: 

    if {k-cancel.i} or {k-jump.i} then do:
      leave main.
    end.
    if not can-find(first zsdiput no-lock) then do:
      assign loaded-sw = false
             v-prompt = "R".
      next main1.
   end.
    {{&appname} &user_Query = "*"}
    {{&appname} &user_display2 = "*" }
    leave main.
  end. /* main  */

  if {k-cancel.i} then do: 
    run unlockZsdi.
    clear frame f-hhep   all no-pause.  
    hide frame f-hhep.
    assign loaded-sw = false
           v-prompt = "R". 
   {{&appname} &user_display3 = "*" }

  end.
  if {k-jump.i} then do: 
    clear frame f-hhep all no-pause.  
    run unlockZsdi.
  end.

end. /* do while main1 */

if {k-cancel.i} then do: 
  run unlockZsdi.
  clear frame f-hhep   all no-pause.  
  hide frame f-hhep.
  assign loaded-sw = false
         v-prompt = "R". 
end.
if {k-jump.i} then do: 
  clear frame f-hhep all no-pause.  
  run unlockZsdi.
end.


{{&appname} &user_aftmain1 = "*"}

procedure OptionsPage:
find first notes use-index k-notes
  where notes.cono         = g-cono and 
        notes.notestype    = "zz" and
        notes.primarykey   = "hher-" + g-operinits and
        notes.secondarykey = " "
        /* exclusive-lock */ no-error.
{w-sasoo.i g-operinits no-lock}

/*
display v-accum s-pckprinter s-ibprinter v-saso-whse with frame f-opts.
OptLoop:
do while true on endkey undo, leave OptLoop: 
  update v-accum  s-pckprinter s-ibprinter
         v-saso-whse  when sasoo.whse = "" 
         go-on(f7)
  with frame f-opts.
  assign s-pckprinter = input s-pckprinter 
         s-ibprinter  = input s-ibprinter.
  g-whse = v-saso-whse.
  run RFLoop.
  
  if (s-pckprinter ne "") then do:
    {w-sasp.i "s-pckprinter" no-lock}
    if not avail(sasp) then do:
      if zx-l = yes then                                         
        run ibrferrx.p                                           
          (input "Invalid Printer"
             + s-pckprinter, input "yes").                                     
      else do:                                                   
        run err.p(1134).                                         
        bell.                                                    
      end.                                                       
      next-prompt s-pckprinter with frame f-opts.                 
      next optloop.                                              
    end.                                                         
  end.    
  
  if (s-ibprinter ne "") then do:           
    /* run RFLoop. */
    {w-sasp.i "s-ibprinter" no-lock}
    /* if (avail sasp and not sasp.pcommand begins "sh") or */
    if /* (avail sasp and trim(sasp.prodlbl) = "") or  */
      (not avail sasp and s-ibprinter ne "") then
    do:
      if zx-l = yes then
        run ibrferrx.p
        (input "Invalid Printer Type for Label Printing..       "  
               + s-ibprinter,
               input "yes"). 
      else do: 
        run err.p(1134).
        bell.
      end.
      next-prompt s-ibprinter with frame f-opts.
      next optloop.
    end.
  end. /* if frame-field = "s-ibprinter" */
  if ((frame-field = "v-saso-whse") and ({k-accept.i} or {k-return.i})) or
      {k-accept.i} then 
  do:           
    assign v-saso-whse = input v-saso-whse. 
    /* run RFLoop. */
    {w-icsd.i v-saso-whse no-lock}  /* v-saso-whse */ 
           
    if avail icsd then
      assign x-saso-whsegrp = "".
           
    if v-saso-whse begins "&" then do:
      find notes where notes.cono       = g-cono and
                       notes.notestype  = "zz"   and
                       notes.primarykey = "Handheld Whse Group" and 
                       notes.secondarykey = v-saso-whse no-lock no-error.
                       /* v-saso-whse */
      if avail notes then
        assign x-saso-whsegrp = notes.noteln[1].
      else do:
        if zx-l = yes then
          run ibrferrx.p
            (input "No Whse Exist - "   
                   +  v-saso-whse + " Please Enter Valid Whse Group",
             input "yes"). 
        else do: 
          run err.p(4601).
          bell.
        end.
        next-prompt v-saso-whse with frame f-opts.
        next optloop.
      end.
    end.
    else    
    if not avail icsd then do:
      if zx-l = yes then
        run ibrferrx.p
          (input "No Whse Exist - "   
              +  v-saso-whse + " Please Enter Valid Whse",
           input "yes"). 
      else do: 
        run err.p(4601).
        bell.
      end.
      next-prompt v-saso-whse with frame f-opts.    /* v-saso-whse */
      next optloop.
    end.
  end. /* if frame-field = v-saso-whse  */
           
      create notes. 
      assign notes.cono         = g-cono  
             notes.notestype    = "zz" 
             notes.primarykey   = "hher-" + g-operinits               
             notes.secondarykey = " "
             notes.noteln[1]    = s-pckprinter
             notes.noteln[2]    = s-ibprinter.
    end. 
    else 
    if avail notes then  
      assign notes.noteln[1] = s-pckprinter 
             notes.noteln[2] = s-ibprinter.
  end. /* accepted and created note */
  if {k-cancel.i} or {k-accept.i} or {k-func7.i} then do: 
    leave OptLoop.
    release notes.
  end. 
end. /* do while true */ /* end update */
*/

if {k-cancel.i} then 
  readkey pause 0.
hide frame f-opts.
release notes.
hide frame f-opts.
end procedure.

procedure RcptSelect:
{ {&appname} &RcptSelect = "*" }

run setTempRecords. 

{{&appname} &user_display2 = "*" }
end procedure. 

/**
Procedure RFLoop: 
assign zx-l = no
       zx-x = 1.
repeat while program-name(zx-x) <> ?:         
  if program-name(zx-x) = "hher.p" then do:                                    
    zx-l = yes.                              
    leave.                             
  end.                                      
  assign zx-x = zx-x + 1.                     
end.                                          
end procedure.
**/

{ {&appname} &user_procedures = "*" }

{p-hhep.i}
