/* SX 55

   u-csip.i 1.1 01/03/98 */
/* u-csip.i 1.2 2/3/93 */
/* u-csip.i (display for csip.i) */
/*tb 8979 01/28/93 enp; Price per each is price minus discount */
    g-shipto
    g-prod
    s-notesfl
    v-descrip
    s-qty
    s-unit
    s-prcunit
    s-netunit
/*
    s-discunit
*/
    s-avail
    v-leadtime
/*
    s-totprice
    s-totdisc
*/
    s-totnet
    s-alttext
    s-kitrebtxt
/*   
    s-baseprice
    s-listprice
*/
    s-nextqty    when s-nextqty ne 0
    s-nextdisc   when s-nextqty ne 0
    s-nexttext   when s-nextqty ne 0
    s-quotetxt   when s-quotetxt ne ""
    s-jobtxt     when s-jobtxt   ne ""
    s-xreftext
    s-nunit
    s-punit
 /*  
    s-dunit
    s-bunit
    s-lunit
 */
    s-prcpertext
    s-qtypertext
