/* vaetslld.las 1.1 01/03/98 */
/* vaetslld.las 1.1 08/29/96 */
/*h****************************************************************************
  INCLUDE      : vaetslld.las
  DESCRIPTION  : Value Add Entry Transactions - Line Items - Assign Labor
                 Data Field Include
  USED ONCE?   : yes
  AUTHOR       : mtt
  DATE WRITTEN : 08/29/96
  CHANGES MADE :
    08/29/96 mtt; TB# 21254 (2F) Develop Value Add Module
    11/15/96 mtt; TB# 21254 (2F) Develop Value Add Module
******************************************************************************/

/*e

    Variables that must be loaded for this include are:

    Required parameters used in this include are:
     &labordata      - The labor data field to be setup - to be displayed.
     &icspstatustype - The ICSP statustype field.  Ex: "v-icspstatus"
                         "avail icsp and icsp.statustype"
     &sctntype       - The section type field or variable
     &timeworkdt     - The time workdt field (not used in vaspsl)
     &file           - Either "vaesl" or "vaspsl"

    Optional parameters used in this include are:
     &buf            - The buffer for VAESL
     &buficsw        - The buffer for ICSW
*/

s-hours            = if can-do("it,is",{&sctntype}) and
                         ( {&icspstatustype} = "l" )
                     then
                         truncate({&file}.timeelapsed / 3600,0)
                     else 0
s-minutes          = if can-do("it,is",{&sctntype}) and
                         ( {&icspstatustype} = "l" )
                     then
                         ({&file}.timeelapsed mod 3600) / 60
                     else 0

{&labordata}       = if {&sctntype} = "ex"
                     then
                         "Flat Rate: " +
                         string({&file}.laborflatrtfl)
                         + (if {&file}.laborflatrtfl = yes then ""
                            else "    Ty: " + {&file}.labortype)
                     else
                     if can-do("it,is",{&sctntype}) and
                         ( {&icspstatustype} = "l" )
                     then
                         (if {&timeworkdt} ne ?
                          then
                              "Dt: " + string({&timeworkdt})
                          else
                              "            ")
                          +
                          " Time:" + string(s-hours,"999") + ":" +
                                     string(s-minutes,"99") +
                                     (if {&file}.timeactty = "e"
                                      then
                                          " (Est)"
                                      else
                                          " (Act)")
                     else
                     if {&file}.nonstockty ne "n" and
                         avail {&buficsw}icsw
                     then
                         if {&buficsw}icsw.wmfl = yes then
                             if {&buficsw}icsw.serlottype = "s"
                             then
                                 "Ser/WM"
                             else
                             if {&buficsw}icsw.serlottype = "l"
                             then
                                 "Lot/WM"
                             else "WM"
                         else
                             if {&buficsw}icsw.serlottype = "s"
                             then
                                 "Ser"
                             else
                             if {&buficsw}icsw.serlottype = "l"
                             then
                                 "Lot"
                             else ""
                     else ""


