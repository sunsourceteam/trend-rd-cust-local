/* oeetlo.lpr 1.1 02/20/98 */
/*h****************************************************************************
  INCLUDE      : oeetlo.lpr
  DESCRIPTION  : OE Line Items - Optional Products - Temp Table Building
  USED ONCE?   : no
  AUTHOR       : mtt
  DATE WRITTEN : 02/18/98
  CHANGES MADE :
    02/18/98 mtt; TB# 24049 PV: Common code use for GUI interface
    09/09/99 bp ; TB# 26575 Add logic to use special price/cost defaults.
    10/25/99 ama; TB# e2761 Tally Project - added run icavail_m.p
    03/07/02 bp ; TB# e12151 Add logic to display second description in GUI.
        Here, load the required data.
    08/08/03 bp ; TB# e17796 Add user hooks. Per GP, we will use the name
        oeetlopt.z99 for the hooks because there is an oeetlo.z99 in the
        CHUI code base.
******************************************************************************/

{oeetlopt.z99 &top = "*"}

/* for pricing */

def new shared var  pd-whse       like icsw.whse                  no-undo.
def new shared var  pd-prod       like icsw.prod                  no-undo.
def new shared var  pd-cust       like arsc.custno                no-undo.
def new shared var  pd-shipto     like arss.shipto                no-undo.
def new shared var  pd-totnet     as dec format ">>>,>>9.99-"     no-undo.
def new shared var  pd-totcost    as dec format ">>>,>>9.99-"     no-undo.
def new shared var  pd-vendrebamt as dec format ">>>,>>9.99-"     no-undo.
def new shared var  pd-margin     as dec format ">>>,>>9.99-"     no-undo.
def new shared var  pd-margpct    as dec format ">>>,>>9.99-"     no-undo.
def new shared var  pd-pdrecno    like pdsc.pdrecno               no-undo.
def new shared var  pd-netunit    as dec format ">>>,>>9.99-"     no-undo.
def new shared var  pd-levelcd    like pdsc.levelcd               no-undo.
def new shared var  pd-price      as dec format ">>>,>>9.99-"     no-undo.

/* Remove temp table records */
for each t-icsec exclusive-lock:
    delete t-icsec.
end.

/*o**** Load temp-table, t-icsec, from icsec for display of options records ***/


load:

for each icsec where
         icsec.cono    = g-cono and
         icsec.rectype = "o" and
         icsec.prod    = {&optprod}
no-lock:

    {oeetlopt.z99 &top_icsec = "*"}

    /*d Find the product records to perform various edits prior to including
        this product on the screen display list */  
    {w-icsp.i icsec.altprod no-lock}
    {w-icsw.i icsec.altprod {&whse} no-lock}

    if avail icsp and icsp.statustype <> "i" and avail icsw then do:
        {oeetlopt.z99 &top_processing = "*"}

        /*tb 22599 02/20/97 gp;  Progress error when optional product
            is setup more than once in ICSEC */
        /*tb 24143 11/20/97 gp; Add index to find */
        /*d Eliminate the possiblity of displaying records twice based on
            a multiple setup in ICSEC */
        find first t-icsec where t-icsec.prod = icsec.altprod
            use-index k-icsec no-lock no-error.

        if avail t-icsec
        then
            next load.

        /*e A kit product cannot be put on a direct order */
        if icsp.kittype <> "" and
            ({&transtype} = "do" or ({&transtype} = "st" and {&stordty}))
        then
            next load.

        /*e A drop ship product can only be put on a DO type */
        if icsw.statustype = "d"
        then

            if {&transtype} = "do" or
                ({&transtype} = "st" and {&stordty})
            then do:
            end.

            else next load.


        if icsp.kittype = "m" then do:
            run icavail_m.p(g-cono,icsp.prod,icsw.whse,1,output v-tottally).
        end.
        else

         /*e Verify each component of a BOD kit */
        if icsp.kittype = "b" or can-do("b,p",icsp.kitrollty) then do:

            v-errfl = "".

            {&comoeetlki}

            {oeetlopt.z99 &bef_oeetlki = "*"}

            /*tb 26575 09/09/99 bp ; Add the seventh parameter, which
              represents the SPC default type. */
            run oeetlki.p (input icsp.kitrollty,
                           input icsp.kittype,
                           input icsp.prod,
                           input no,
                           input no,
                           input "o",
                           input v-oeicssty,
                           output {&seqno}).

            {oeetlopt.z99 &aft_oeetlki = "*"}

            /{&comoeetlki}* */

            {&kitcomponentcall}

            if v-errfl <> "" then next load.

        end.

        /*tb 22599 02/20/97 gp;  Remove 100 limit logic */

        /*d Load the optional product into the temp-table */
     
        assign pd-prod   = icsec.altprod
               pd-whse   = b-oeeh.whse
               pd-cust   = b-oeeh.custno
               pd-shipto = b-oeeh.shipto.
        assign pd-pdrecno    = 0
               pd-levelcd    = 0
               pd-totcost    = 0
               pd-vendrebamt = 0
               pd-netunit    = 0
               pd-margin     = 0
               pd-margpct    = 0.
     
         run dasoeip.p(input 1,
                        "EACH").
        
        
        
        
        
        
        
        create t-icsec.

        /*tb 10169 03/26/93 kmw; Optional products show stocking unit */
        /*tb 24143 11/20/97 gp; Assign seqno */
        assign
            t-icsec.seqno     = icsec.keyno
            t-icsec.prod      = icsec.altprod
            t-icsec.notesfl   = icsp.notesfl
            t-icsec.proddesc  = icsp.descrip[1]
            t-icsec.proddesc2 = icsp.descrip[2]
            t-icsec.qtyship  = 0
            t-icsec.unit     = /* icsp.unitstock */ icsp.unitsell
            t-icsec.chrg     = "y"
            t-icsec.price    = pd-price
            t-icsec.leadtm   = icsw.leadtmavg.

        {oeetlopt.z99 &bef_netavl = "*"}

        {p-netavl.i "/*"}.

        {oeetlopt.z99 &aft_netavl = "*"}

        /*tb 10169 03/26/93 kmw; Optional products show stocking unit */
        {p-unit.i &prod       = "icsp.prod"
                  &unitconvfl = "icsp.unitconvfl"
                  &unitstock  = "icsp.unitstock"
                  &unit       = "t-icsec.unit"
                  &desc       = "{&unitdesc}"
                  &conv       = "{&unitconv}"
                  &confirm    = "{&unitconfirm}"}.

        if not {&unitconfirm} then
            t-icsec.unit = icsp.unitstock.

        /*tb 10169 03/26/93 kmw; Optional products show stocking unit */
        /*tb 13520 11/08/93 mwb; added round for unitconv round error */
        assign
            t-icsec.qtyavail  = if icsp.kittype = "b" then v-buildqty
                                else if icsp.kittype = "m" then v-tottally
                                else s-netavail
            t-icsec.qtyavaild = t-icsec.qtyavail / {&unitconv}
            t-icsec.qtyavaild = truncate(round(t-icsec.qtyavaild,3),0)
            t-icsec.conv      = {&unitconv}.

        {oeetlopt.z99 &bot_asgn = "*"}

    end.

{oeetlopt.z99 &bottom = "*"}

end.
