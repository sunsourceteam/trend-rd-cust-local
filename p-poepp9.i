/* p-poepp9.i 5/22/97 */
/*si**** Custom ****************************************************************
CLIENT  : 80281 SDI
JOB     : sdi014 Custom PO print
AUTHOR  : msk
VERSION : 7
CHANGES :
    si01 5/22/97 msk; 80281/sdi014; initial changes
    si02 06/11/97 jsb; take out the signature line per Jack
    si03 07/17/97 jsb; TOF changes
*******************************************************************************/

/* p-poepp9.i 1.18 03/21/94 */
/*h*****************************************************************************
  INCLUDE      : p-poepp9.i
  DESCRIPTION  : Purchase Order Entry PO Print format 9 - NEMA
  USED ONCE?   : no
  AUTHOR       : kmw
  DATE WRITTEN : 11/09/90
  CHANGES MADE :
    10/14/91 mwb; TB#  4761 Moved page # and resale # to fit pre-printer forms
    11/13/91 mwb; TB#  2572 If an 'rm' then use the apsv or apss info for ship
        to display
    08/18/92 mwb; TB#  7319 Added unitconv to replace stkqtyord / qtyord.
    08/19/92 mwb; TB#  7398 Don't use v-conv for qty's, use stkqtyord/qtyord
    09/25/92 mjq; TB#  6410 Print the manual address when using misc
        Vendor # 999999999999
    11/11/92 mms; TB#  8669 Rush - Display Rush message in header
    12/21/92 mtt; TB#  9266 Changed ship to address to print
        '*** Do Not Ship ***' for a Blanket PO (BL).
    02/25/93 rs ; TB#  2932  Indicate whether a po has been costed or received
    02/25/93 rs ; TB#  9323  Ship To info not printing right for RM orders
    03/18/93 rs ; TB# 10241 Cust# does not print on POEPP
    03/18/93 rs ; TB# 10264 Price U/M displays ICSP Special Cost
    04/12/93 rs ; TB# 10061 POEPP doesn't print bin locs for rm's
    04/21/93 rhl; TB# 10892 Alt. vend product not printed
    05/15/93 rhl; TB#  9461 FOB Terminology Is Incorrect
    08/23/93 tdd; TB# 12699 Printing "c" status lines
    11/05/93 mwb; TB# 13520 Carry unitconv to 10 deciamals
    02/21/94 dww; TB# 13890 File name not specified causing error
    09/13/94 tdd; TB# 15541 REPRINT printing a line too low
    04/27/95 mtt; TB# 13356 Serial/Lot # doesn't print on PO/RM
    10/11/95 kr ; TB# 19526 Move POEP to new Processing Menu
    10/31/95 gp;  TB# 18091 Expand UPC Number (T23)
    11/06/95 gp;  TB# 18091 Expand UPC Number (T23)
    08/29/97 JH       Remove Sold To INformation From PO header.
    07/22/98 DBF  IT# 1; Add logic to display correct remit to address on 
                         printed PO's.
    02/10/99 tha  STI# 1; Add print designation for Fax and EDI.
    10/08/99 tha  ST2# 2; Take the prining of terms out
*******************************************************************************/
def var s-lit48d as c format "x(10)" no-undo. /*si02*/
def var s-soldcity as c format "x(35)" no-undo.    /*08/29/97 */
/*e If print fields are added or changed, check poeppe1.p for EDI format */

/* DBF IT# 1 */
def buffer z-icsd for icsd.
def buffer v-icsd for icsd.
def buffer z-oimsp for oimsp. 
def var z-type as character format "x(5)" no-undo.
/* DBF IT# 1 */

assign
    v-pageno    = page-number - 1
    v-totinvamt = 0.

/*tb 15541 09/13/94 tdd; REPRINT printing a line too low.  Positioning one
    character off causing s-reprint to rap to next line. */
/*tb 18091 10/31/95 gp;  Changed UPC Number */
form header
    v-tof    format "x(12)"      /*si03*/
    skip(1)
    s-comphd1     as c format "x(50)"           at 43
    s-comphd2     as c format "x(50)"           at 43
    s-lit1a       as c format "x(41)"           at 1
    s-comphd3     as c format "x(50)"           at 43
    /* s-reprint     as c format "x(46)"           at 46    si01 move down */
    s-lit1b       as c format "x(40)"           at 93
    s-lit2a       as c format "x(41)"           at 1
    s-comphd4     as c format "x(50)"           at 43
    /* s-rush        as c format "x(12)"           at 57    si01 move down */
    s-upcvno                                    at 95
    poeh.orderdt                                at 105
    poeh.pono                                   at 116
    "-"                                         at 123
    poeh.posuf                                  at 124
    page-num - v-pageno format "zzz9"           at 129
    s-lit3a       as c format "x(9)"            at 1
    poeh.vendno                                 at 11
    s-currdescrip like sastc.descrip            at 24
    s-custnolbl   as c format "x(7)"            at 105
    apsv.apcustno                               at 113
    s-reprint     as c format "x(46)"           at 20    /* si01 moved here */
    s-rush        as c format "x(12)"           at 70    /* si01 moved here */
    /* skip(1)                                              si01 */
    s-lit6a       as c format "x(7)"            at 3
    s-sellernm    like apsv.name                at 11
    s-lit6b       as c format "x(8)"            at 43
/*   b-sasc.name                                 at 52      08/29/97*/
    s-lit6c       as c format "x(11)"           at 84
    s-invname     like icsd.name                at 96
    s-selleraddr[1]                             at 11
/*    b-sasc.addr[1]                              at 52     08/29/97*/
    s-invaddr[1]                                at 96
    s-selleraddr[2]                             at 11
/*    b-sasc.addr[2]                              at 52     08/29/97*/
    s-invaddr[2]                                at 96
    s-sellercity  as c format "x(35)"           at 11
/*    s-soldcity    as c format "x(35)"           at 52     08/29/97*/
    s-invcity     as c format "x(35)"           at 96
    skip(1)
    s-lit11a      as c format "x(8)"            at 2
    s-shiptonm    like poeh.shiptonm            at 11
    s-lit11b      as c format "x(12)"           at 47
    s-lit11c      as c format "x(8)"            at 79
    s-lit11d      as c format "x(9)"            at 103
    s-shiptoaddr[1]                             at 11
    poeh.shipinstr                              at 47
    poeh.resaleno                               at 79
    poeh.refer                                  at 103
    s-shiptoaddr[2]                             at 11
    s-lit12b      as c format "x(3)"            at 51
    s-lit12c      as c format "x(12)"           at 65
    s-lit12d      as c format "x(13)"           at 83
    s-lit12e      as c format "x(13)"           at 102
    s-lit12f      as c format "x(8)"            at 118
    s-shipcity    as c format "x(35)"           at 11
    s-shipvia     as c format "x(12)"           at 47
    "See Below"                                 at 66
/*    poeh.duedt                                  at 65  */
    s-fob         as c format "x(19)"           at 81
    s-terms       as c format "x(12)"           at 102
    s-schedule    as c format "x(14)"           at 118
    /* skip(1)                                                  si01 */
with frame f-head no-box no-labels width 132 page-top.

form header
    s-lit14a      as c format "x(78)"           at 1
    s-lit14b      as c format "x(50)"           at 79
    s-lit15a      as c format "x(78)"           at 1
    s-lit15b      as c format "x(50)"           at 79
    s-lit16a      as c format "x(78)"           at 1
    s-lit16b      as c format "x(50)"           at 79
with frame f-headl no-box no-labels width 132 page-top.

form
    skip(1)
    v-linecnt          format "zz9"             at 1
    s-lit40a      as c format "x(11)"           at 5
    s-lit40b      as c format "x(18)"           at 42
    s-totqtyord   as c format "x(13)"           at 62
    s-lit40c      as c format "x(18)"           at 76
    s-totinvamt   as c format "x(13)"           at 103
with frame f-tot1 no-box no-labels no-underline width 132.

form
    s-title2      as c format "x(18)"           at 75
    s-amt2        as dec format "zzzzzzzz9.99-" at 102
    s-type2       as c format "x(1)"            at 115
with frame f-tot2 no-box no-labels width 132 no-underline down.

form header
    s-lit48a      as c format "x(9)"            at 1
    s-lit48b      as c format "x(11)"           at 80
    s-confirm     as c format "x(3)"            at 61
    s-buyerph     as c format "x(17)"           at 98
    s-buyerfx     as c format "x(17)"           at 116          
    s-lit48c      as c format "x(6)"            at 80
    s-buyer       as c format "x(24)"           at 98
    /*    s-lit48d      as c format "x(10)"     at 80   si02*/ 
with frame f-tot4 no-box no-labels width 140 page-bottom.

form confirm
    s-lit49       as c format "x(95)"           at 3
    s-lit49a      as c format "x(70)"           at 3
    skip(1)
    s-lit51a      as c format "x(55)"           at 39
    skip(1)
with frame f-confirm no-box no-labels width 132 no-underline.

/* form 
    s-lit51a      as c format "x(55)"           at 39
    skip(1)
with frame f-message no-box no-labels width 132 no-underline. */

form 
    skip(1)
    s-lit52a      as c format "x(90)"           at 13
    s-lit52b      as c format "x(90)"           at 13
    skip(1)
    s-lit52c      as c format "x(90)"           at 13
with frame f-relmsg1 no-box no-labels width 132 no-underline. 
   
form 
    s-relpono     as c format "x(10)"           at 13 
    s-relprod     as c format "x(24)"           at 30
    s-relqty      as i format "zzzzzz9"         at 60
    s-reldate     as date                       at 81
with frame f-reldet1 no-box no-labels width 132 no-underline.

form
    s-spaces      as c format "x"               at 1 
with frame f-spaces no-box no-labels width 132 no-underline.

/*o Assign all of the form labels */
{a-poepp9.i s-}

/* DBF IT #1 beg */
/*
find z-icsd where z-icsd.cono = g-cono
 and z-icsd.whse = poeh.whse
no-lock no-error.

if avail z-icsd then do:

   if z-icsd.user1 = "" then do:

      find v-icsd where v-icsd.cono = g-cono
      and v-icsd.whse = "dros"
      no-lock no-error.
   
      if avail v-icsd then 
   
          assign s-comphd2 = trim(v-icsd.addr[1])
                 s-comphd3 = trim(v-icsd.city) + ", " + trim(v-icsd.state) +
                             " " + trim(v-icsd.zip)
                 s-comphd4 = "TEL:(" + substring(v-icsd.phoneno,1,3) + ")" +
                             substring(v-icsd.phoneno,4,3) + "-" + 
                             substring(v-icsd.phoneno,7,4) + "  FAX:(" +
                             substring(v-icsd.faxphoneno,1,3) + ")" +
                             substring(v-icsd.faxphoneno,4,3) + "-" +
                             substring(v-icsd.faxphoneno,7,4).
   
   end.
   else do:
   
       find v-icsd where v-icsd.cono = g-cono
        and v-icsd.whse = z-icsd.user1
       no-lock no-error.
   
       if avail v-icsd then 
   
          assign s-comphd2 = trim(v-icsd.addr[1])
                 s-comphd3 = trim(v-icsd.city) + ", " + trim(v-icsd.state) +
                             " " + trim(v-icsd.zip)
                 s-comphd4 = "TEL:(" + substring(v-icsd.phoneno,1,3) + ")" +
                             substring(v-icsd.phoneno,4,3) + "-" + 
                             substring(v-icsd.phoneno,7,4) + "  FAX:(" +
                             substring(v-icsd.faxphoneno,1,3) + ")" +
                             substring(v-icsd.faxphoneno,4,3) + "-" +
                             substring(v-icsd.faxphoneno,7,4).
          
   end.

end.
/* DBF IT #1 end */
*/

assign s-comphd2 = "" 
       s-comphd3 = "" 
       s-comphd4 = "".

/*tb 18091 10/26/95 gp;  Find the first ICSV record for this vendor */
find first icsv use-index k-vendno where
    icsv.cono   = g-cono and
    icsv.vendno = poeh.vendno
no-lock no-error.

/*o Set the form variables for the banner/header */
/*tb 18091 10/31/95 gp;  Add UPC number assignment */
assign
    s-custnolbl     = if apsv.apcustno ne "" then "Cust #:" else ""
    s-sellernm      = if poeh.vendno = 999999999999.0 then poeh.manname
                      else if avail apss then apss.name else apsv.name
    s-selleraddr[1] = if poeh.vendno = 999999999999.0
                          then poeh.manaddr[1]
                      else if avail apss then apss.addr[1]
                      else apsv.addr[1]
    s-selleraddr[2] = if poeh.vendno = 999999999999.0
                          then poeh.manaddr[2]
                      else if avail apss then apss.addr[2]
                      else apsv.addr[2]
    s-sellercity    = if poeh.vendno = 999999999999.0 then
                          poeh.mancity + ", " + poeh.manstate + " "
                          + poeh.manzipcd
                      else if avail apss then
                          apss.city + ", " + apss.state + " " + apss.zipcd
                      else
                          apsv.city + ", " + apsv.state + " " + apsv.zipcd
    s-soldcity      = b-sasc.city + ", " + b-sasc.state + " " + b-sasc.zipcd 
    s-invname       = if avail icsd then icsd.name else b-sasc.name
    s-invaddr[1]    = if avail icsd then icsd.addr[1] else b-sasc.addr[1]
    s-invaddr[2]    = if avail icsd then icsd.addr[2] else b-sasc.addr[2]
    s-invcity       = if avail icsd then
                          icsd.city + ", " + icsd.state + " " + icsd.zipcd
                      else
                          s-soldcity
    z-type          = if sapbo.user2 = "fax" then
                         "*FAX*"
                      else
                      if sapbo.user2 begins "edi" then
                         "*EDI*"
                      else ""   
    s-reprint       = if ((poeh.pocnt < 2 and z-type <> "*EDI*") or
                         (poeh.pocnt < 3 and z-type = "*EDI*")) then
                        (z-type + " " +
                          if poeh.stagecd = 5 then "***(Received)***"
                      else if poeh.stagecd = 6 then "***(Costed)***"
                      else if poeh.stagecd = 7 then "***(Closed)***"
                      else "")
                      else 
                          z-type + " " +
                          "*** R E P R I N T " +
                          (if p-changefl = yes then "(Changes Only) "
                           else "") +
                          (if poeh.stagecd = 5 then "(Received)"
                           else if poeh.stagecd = 6 then "(Costed)"
                           else if poeh.stagecd = 7 then "(Closed)"
                           else "")
                           + "***"
 
  
  /*
    s-reprint       = if sapbo.outputty = "f" and
                          poeh.pocnt < 2  then
                            (if poeh.stagecd = 5 then "*F*(Received)*F*"
                        else if poeh.stagecd = 6 then "*F*(Costed)*F*"
                        else if poeh.stagecd = 7 then "*F*(Closed)*F*"
                        else "*** FAXED ***")
                       else 
                        if sapbo.outputty = "f" and
                          "*F* R E P R I N T " +
                            (if p-changefl = yes then "(Changes Only) "
                             else "") +
                            (if poeh.stagecd = 5 then "(Received)"
                             else if poeh.stagecd = 6 then "(Costed)"
                             else if poeh.stagecd = 7 then "(Closed)"
                             else "")
                             + "*F*"
    
    s-reprint       = if sabpo.outputty = "e" and
                         poeh.pocnt < 2 then
                           (if poeh.stagecd = 5 then "*e*(Received)*e*"
                        else if poeh.stagecd = 6 then "*e*(Costed)*e*"
                        else if poeh.stagecd = 7 then "*e*(Closed)*e*"
                        else "")
                        else
                        if sabpo.outputty = "e" and
                          "*e* R E P R I N T " +
                            (if p-changefl = yes then "(Changes Only) "
                             else "") +
                            (if poeh.stagecd = 5 then "(Received)"
                             else if poeh.stagecd = 6 then "(Costed)"
                             else if poeh.stagecd = 7 then "(Closed)"
                             else "")
                             + "*e*"
   */ 
   
    s-rush          = if poeh.rushfl then "*** RUSH ***" else ""
    v-linecnt       = 0
    s-schedule      = if poeh.orderdisp = "s" then "Ship Complete"
                      else if poeh.bofl      = yes then "BO Partial"
                      else if poeh.bofl      = no  then "Cancel Partial"
                      else "Mutually Defn"
    s-confirm       =  "Yes"  /* if poeh.confirmfl then "Yes" else "No" */
    s-shiptonm      = poeh.shiptonm
    s-shiptoaddr[1] = if poeh.transtype = "bl" then " "
                      else poeh.shiptoaddr[1]
    s-shiptoaddr[2] = if poeh.transtype = "bl" then "*** Do Not Ship ***"
                      else poeh.shiptoaddr[2]
    s-shipcity      = if poeh.transtype = "bl" then " "
                      else poeh.shiptocity + ", " +
                          poeh.shiptost +
                          " " + poeh.shiptozip
    s-upcvno        = if avail icsv and avail sasc then
                          {upcno.gas &section  = "3"
                                     &sasc     = "sasc."
                                     &icsv     = "icsv."
                                     &comdelim = "/*"}
                      else "000000".

/*d Find and display the description of table values */
{w-sastc.i poeh.currencyty no-lock}
assign
    s-currdescrip = if avail sastc then sastc.descrip else "".
{w-sasta.i "S" poeh.shipviaty no-lock}
assign
    s-shipvia  = if avail sasta then sasta.descrip else poeh.shipviaty.
if apsv.langcd ne "" then
    {w-sals.i apsv.langcd "t" poeh.termstype no-lock}
{w-sasta.i "t" poeh.termstype no-lock}
assign
    s-terms    = if avail sals then sals.descrip[1] else
                     if avail sasta then sasta.descrip else poeh.termstype.
/* tah SDI2 Termsupdate DO NOT PRINT  */
 if poeh.vendno = 81101539 then
    s-terms = "Net 30".
 else   
    s-terms = "".
/* tah SDI2 Termsupdate DO NOT PRINT  */
if avail apsv and substr(apsv.user11,1,4) <> "    " then
  assign s-terms = if avail sasta then sasta.descrip else poeh.termstype.


find first z-oimsp where
           z-oimsp.person = poeh.buyer
     no-lock no-error.
if avail z-oimsp then
 do:
   assign
   s-buyerph = "TEL:(" + substring(z-oimsp.phoneno,1,3) + ")" +
                         substring(z-oimsp.phoneno,4,3) + "-" + 
                         substring(z-oimsp.phoneno,7,4) 
   s-buyerfx = "FAX:(" + substring(z-oimsp.faxphoneno,1,3) + ")" +
                         substring(z-oimsp.faxphoneno,4,3) + "-" +
                         substring(z-oimsp.faxphoneno,7,4).
 end.
else 
  assign s-buyerph = " "
         s-buyerfx = " ".

{w-sasta.i "b" poeh.buyer no-lock}
assign
    s-buyer    = if avail sasta then sasta.descrip else poeh.buyer.
    s-fob      = if poeh.fobfl then "F.O.B.-Delivered"
                 else "F.O.B.-Ship Point".

view frame f-head.
view frame f-headl.
view frame f-tot4.

/*d Print the vendor notes  */

/* sx32
if apsv.notesfl ne "" then do:
    for each notes where
             notes.cono         = g-cono              and
             notes.notestype    = "v"                 and
             notes.primarykey   = string(poeh.vendno) and
             notes.secondarykey = ""
    no-lock:
        if notes.printfl = yes then do i = 1 to 16:
            /*tb 13890 02/21/94 dww; File name not specified causing error */
            if notes.noteln[i] ne "" then do:
                display notes.noteln[i] with frame f-notes.
                down with frame f-notes.
            end.

        end.

    end.

end.

/*d Print the purchase order notes  */
if poeh.notesfl ne "" then do:
    for each notes where
             notes.cono         = g-cono            and
             notes.notestype    = "x"               and
             notes.primarykey   = string(poeh.pono) and
             notes.secondarykey = ""
    no-lock:
        if notes.printfl = yes then do i = 1 to 16:
            /*tb 13890 02/21/94 dww; File name not specified causing error */
            if notes.noteln[i] ne "" then do:
                display notes.noteln[i] with frame f-notes.
                down with frame f-notes.
            end.

        end.

    end.

end.
SX32 */

/*d Print the vendor notes  */
if apsv.notesfl ne "" then
   run notesprt.p(g-cono,"v",string(poeh.vendno),"","poepp",5,2).
    
    /*d Print the purchase order notes  */
if poeh.notesfl ne "" then
   run notesprt.p(g-cono,"x",string(poeh.pono),string(poeh.posuf),
                   "poepp",5,2).
                             
                             

v-totqtyord = 0.
/*o Find each PO line to display */

/* das */
display s-lit49 s-lit49a s-lit51a with frame f-confirm.    
down with frame f-confirm.
/* das */

for each poel use-index k-poel where
         poel.cono        = g-cono      and
         poel.pono        = poeh.pono   and
         poel.posuf       = poeh.posuf  and
         poel.statustype  = "a"
no-lock:
    v-linecnt = v-linecnt + 1.

    /*d If the PO has been printed once before and changes only are
        requested and no changes have been made to the line since
        it printed, find the next line */
    if sapbo.reprintfl = yes and p-changefl = yes and poel.printfl = no
        then next.

    if poel.nonstockty = "" then do:
        {w-icsw.i poel.shipprod poel.whse no-lock}
        {w-icsp.i poel.shipprod no-lock}
        if apsv.langcd ne "" then
            {w-sals.i apsv.langcd "p" poel.shipprod no-lock}
    end.
     else {w-icsc.i poel.shipprod no-lock}
            
            

    /*tb 18091 10/30/95 gp;  Find Product section of UPC number */
    {icsv.gfi "'u'" poel.shipprod poeh.vendno no}

    
    /* SX30   SunSource  */
     {icss.gfi  &prod     = poel.shipprod
                &icsprecno = poel.icsprecno
                &lock      = "no"}
    /* SX30   SunSource  */



    clear frame f-poel.
    /*d Set the PO line varibles for printing */

    /*tb 18091 10/31/95 gp;  Assign product UPC number */
    assign
        v-conv      = {unitconv.gas &decconv = poel.unitconv}
        v-qtyord    =  if poel.transtype = "br" and poel.qtyord = 0 then 
                          0
                       else
                          (poel.stkqtyord -
                           (if p-changefl = yes then poel.prevqtyord else 0))
                             / (poel.stkqtyord / poel.qtyord)
        s-qtyord    = if substring(string(v-qtyord,"9999999.99-"),9,2)
                          = "00" then string(v-qtyord,"zzzzzz9-")
                      else string(v-qtyord,"zzzzzz9.99-")
        s-lineno    = string(poel.lineno,"zz9")
        s-price     = if substring
                          (string(poel.price,"9999999.99999"),11,3) = "000"
                          then string(poel.price,"zzzzzz9.99-")
                      else string(poel.price,"zzzzzz9.99999-")
            
    /* SX30   SunSource  */

        s-prcunit   = if avail icss and icss.prccostper <> "" then
                          icss.prccostper else poel.unit
        s-spcunit   = if avail icss and index("ht",icss.speccostty) > 0
                          then "(" + icss.speccostty + ")"
                      else ""
     /* SX30   SunSource  */

        s-netamt    = string(poel.netamt,"zzzzzzzz9.99-")
        v-totinvamt = v-totinvamt + poel.netamt
        v-totqtyord = v-totqtyord + v-qtyord
        s-shipprod  = if p-ourprodfl = yes or poel.nonstockty ne "" or
                          avail icsw and icsw.vendprod = ""
                          then poel.shipprod
                      else icsw.vendprod
        s-binloc    =  ""
        s-upcpno    = if avail icsv and avail sasc then
                          {upcno.gas &section  = "4"
                                     &sasc     = "sasc."
                                     &icsv     = "icsv."
                                     &comdelim = "/*"}
                       else "00000".

    if not p-ourprodfl and poel.nonstockty = "" and
        poel.vendno ne icsw.arpvendno then do:
            {w-icsec.i ""v"" poel.shipprod poel.vendno no-lock}
            if avail icsec then s-shipprod = icsec.altprod.
        end.

    /*d Print the PO line */

    /*tb 18091 10/31/95 gp;  Change UPC number display */
    display
        s-lineno
        s-shipprod
        poel.unit
        s-qtyord
        s-upcpno
        s-price
        s-prcunit
        s-spcunit
        s-netamt
        poel.duedt
        s-sign
    with frame f-poel.

    if poel.nonstockty ne "" then do:
        if poel.proddesc ne "" then 
        display poel.proddesc + " " + poel.proddesc2 @ s-descrip
        with frame f-poel.
    end.

    else if not avail icsp or not avail icsw then
        if apsv.langcd = "" or not avail sals then
            display v-invalid @ s-descrip with frame f-poel.
        else
            display sals.descrip[1] + " " + sals.descrip[2]
            @ s-descrip with frame f-poel.
    else if apsv.langcd = "" or not avail sals then do:
        if poeh.transtype = "rm" then do:
            if icsw.wmfl then do:
                s-binloc = "Bin Loc:".
                for each wmet use-index k-prod where
                         wmet.cono = g-cono    and
                         wmet.whse = icsw.whse and
                         wmet.prod = icsw.prod and
                         wmet.qtyactual > 0
                no-lock:
                    s-binloc = s-binloc + " " +
                                   string(wmet.binloc,"xx/xx/xxx/xxx").
                end.

                if length(trim(s-binloc)) = 8 then
                    for each wmsbp use-index k-wmfifo where
                             wmsbp.cono = g-cono    and
                             wmsbp.whse = icsw.whse and
                             wmsbp.prod = icsw.prod
                    no-lock:
                        s-binloc = s-binloc + " " +
                                       string(wmsbp.binloc,"xx/xx/xxx/xxx").
                    end.

                    if length(trim(s-binloc)) = 8 then
                        s-binloc = s-binloc + " No WM bin loc found.".
            end. /* end of if icsw.wmfl */

            else
                s-binloc = "Bin Loc: " + string(icsw.binloc1,"xx/xx/xxx/xxx").
        end. /* end of if poeh.transtype = "rm" */

        display
            icsp.descrip[1] + " " + icsp.descrip[2] @ s-descrip
            s-binloc
        with frame f-poel.
    end.

    else
        display
            sals.descrip[1] + " " + sals.descrip[2] @ s-descrip
        with frame f-poel.
        down with frame f-poel.

    if (poeh.transtype = "bl" or poeh.transtype = "br") and 
        v-blprint then 
        run "print_releases".

/* Tariff mod 08/25/09 */                                               
/* Commented out due to buyers not needing tariff info printed on the PO
   das - 11/15/12 */
/* run zsditariffprt (g-cono,poel.shipprod,poel.whse,"poepp",8).      */
/* Tariff mod 08/25/09 */                                               
     


    if poel.nonstockty = "" and
      avail icsp and icsp.notesfl ne "" then
      run notesprt.p(g-cono,"p",icsp.prod,"","poepp",5,1).
             
 /*d Print the catalog product notes  */
    if poel.nonstockty = "n" and
      avail icsc and icsc.notesfl ne "" then
      run notesprt.p(1,"g",icsc.catalog,"","poepp",5,1).
 

/* SX32 
         /*d Print the product notes  */
        if poel.nonstockty = "" and
        avail icsp and icsp.notesfl ne "" then do:
            for each notes where
                     notes.cono         = g-cono    and
                     notes.notestype    = "p"       and
                     notes.primarykey   = icsp.prod and
                     notes.secondarykey = ""
            no-lock:
                if notes.printfl = yes then do i = 1 to 16:
                    /*tb 13890 02/21/94 dww; File name not specified
                         causing error */
                    if notes.noteln[i] ne "" then do:
                        display notes.noteln[i] with frame f-notes.
                        down with frame f-notes.
                    end.

                end.

            end.

        end.
SX32 */
        /*d Print the PO line item comments  */
        if poel.commentfl = yes then do:
            {w-com.i ""po"" poel.pono poel.posuf poel.lineno yes no-lock}
            if avail com and com.printfl then do i = 1 to 16:
                if com.noteln[i] ne "" then do:
                    display com.noteln[i] with frame f-com.
                    down with frame f-com.
                end.

            end.

            {w-com.i ""po"" poel.pono poel.posuf poel.lineno no no-lock}
            if avail com and com.printfl then do i = 1 to 16:
                if com.noteln[i] ne "" then do:
                    display com.noteln[i] with frame f-com.
                    down with frame f-com.
                end.

            end.

        end.

        /*tb 13356 04/27/95 mtt; Serial/Lot # doesn't print on PO/RM */
        if poel.nonstockty = "" and avail icsw and poeh.transtype = "rm"
        then do:
            assign
                v-nosnlots   = 0
                v-stkqtyord  = poel.stkqtyord.
            /*o Print the serial/lot records */
            {p-oeepp1.i
                &type       = "p"
                &line       = "poel"
                &pref       = "po"
                &seq        = 0
                &stkqtyship = "v-stkqtyord"
                &snlot      = "v-nosnlots"}
        end.

        /*o Reset the line item print flag */
        if poel.printfl = yes then
            run poeppu.p(poel.pono, poel.posuf, poel.lineno).

    end. /* for each oeel */

    /*o******Display the order totals ********************/
    assign
        s-totinvamt = string(v-totinvamt,"zzzzzzzz9.99-")
        s-totqtyord = if substring (string(v-totqtyord,"999999999.99-"),11,2)
                          = "00" then string(v-totqtyord,"zzzzzzzz9-")
                      else string(v-totqtyord,"zzzzzzzz9.99-").
    clear frame f-tot1.
    display
        v-linecnt
        s-lit40a
        s-lit40b
        s-totqtyord
        s-lit40c
        s-totinvamt
    with frame f-tot1.

    if poeh.wodiscamt <> 0 then do:
        clear frame f-tot2.
        assign
            s-title2 = s-lit41a
            s-amt2   = poeh.wodiscamt
            s-type2  = if poeh.wodisctype = yes then "$" else "%".
        display
            s-title2
            s-amt2
            s-type2
        with frame f-tot2.
        down with frame f-tot2.
    end.

    assign
        s-lit48a = "Last Page".
