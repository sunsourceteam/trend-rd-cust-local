/* p-vaepq9.i */
/*si**** Custom ****************************************************************
  CLIENT  : 80281 SDI Operating Partners, Inc
  JOB     : SDI06501 - Change to show Canadian Dollar Amts
  ANCHOR  : p-oeepa1.i
  VERSION : 8.0.003
  PURPOSE : 
  CHANGES :
    SI01 (Client originally developed order acknowledgement)
    SI02 03/04/00; ucv/usi; SDI06501 - Change to show Canadian Dollar Amts
******************************************************************************//* p-oeepa1.i 1.20 02/21/94 */
/*h*****************************************************************************
  INCLUDE      : p-vaepq9.i
  DESCRIPTION  : vaepq9.p  main print logic
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    10/14/91 mwb; TB# 4762  Change page#, order #, shipped date and ship via
        for pre-printed forms
    04/03/92 pap; TB# 6219  Line DO  add DO line indicator adjusted qty
        backordered
    06/06/92 mwb; TB# 6889  Replaced oeeh.payamt with oeeh.tendamt.
    06/16/92 mkb; TB# 5929  Full amount tendered, add write off amount to
        downpayment
    06/20/92 mwb; TB# 5413  If the header as a write off amount set - add it
        to the down payment display (BO rounding problems when down
        payment is applied).
    08/05/92 pap; TB# 7245  Do not print the backordered qty or the shipped
        qty on an acknowledgement
    08/12/92 pap;           REVERSAL of TB# 7245 BY DESIGN MEETING
    11/11/92 mms; TB# 8561  Display product surcharge label from ICAO
    12/07/92 ntl; TB# 9041  Canadian Tax Changes (QST)
    03/18/93 jlc; TB# 7245  Items with a quantity shipped of 0 are printing a
        quantity shipped and a quantity backordered.
    04/07/93 jlc; TB# 8890  Drop Ship orders should print Drop Ship for
        the Ship Point.
    04/08/93 jlc; TB# 10155 Print customer product.
    04/08/93 jlc; TB# 9727  Don't print core chg and restock amt for a
        non-print line.
    04/10/93 jlc; TB# 7405  Print DROP for the qty ship on drop lines.
    06/07/93 jlc;           Changes to tb# 7245 per Design Committee.
    02/21/94 dww; TB# 13890 File name not specified causing error
    06/23/95 ajw; TB# 18575 Nonstock/Special Kit Component Project (G1)
    08/17/95 tdd; TB# 17771 Notes print twice when reference component
    11/01/95 gp;  TB# 18091 Expand UPC Number (T24)
    11/06/95 gp;  TB# 18091 Expand UPC Number (T24)
    02/27/97 jkp; TB#  7241 (5.1) Spec8.0 Special Price Costing.  Added call
        of speccost.gva.  Added find of icss record.  Added call to 
        speccost.gas to determine prccostper.  Replaced icsp.prccostper with 
        v-prccostper.
    04/17/97 JPH; ITB# 1    Remove upc number, qty b/o, qty shpd, upc vendor
                            number, change orderno to invoice number.
    12/01/97 JPH; ITB# 2    Add icsd information to remittance portion.        ******************************************************************************/

form header  /* main title lines  */
    skip(2)
    s-lit1aa as c format "x(10)"         at 2
    s-lit1a  as c format "x(50)"         at 12
    s-cancel as c format "x(25)"         at 64
    s-lit2a  as c format "x(53)"         at 2
    v-tagno                              at 116
    s-lit3a as c format "x(09)"          at 2
    arsc.custno                          at 12
    s-litattn as c format "X(30)"        at 2  
    ""                                   at 94
    ""                                   at 104
    page-num - v-pageno format "zzz9"    at 129
    skip(1)
    s-lit6a as c format "x(8)"           at 2
    s-billname like arsc.name            at 12
    s-lit6b as c format "x(10)"          at 48 
    s-remitnm as c format "x(24)"        at 62  
    s-billaddr1 like arsc.name           at 12
    s-lit6bb as c format "x(6)"          at 48   
    s-remit-addr1 as c format "x(24)"    at 62    
    s-billaddr2 like arsc.name           at 12
    s-remit-addr2 as c format "x(24)"    at 62   
    s-lit8a as c format "x(9)"           at 106
    s-pstlicense as c format "x(11)"     at 116
    s-billcity as c format "x(35)"       at 12
    s-corrcity as c format "x(35)"       at 62
    s-lit9a as c format "x(9)"           at 106
    s-gstregno as c format "x(10)"       at 116
    s-litbfax as c format "X(30)"        at 12
    s-lit11a as c format "x(8)"          at 2
    s-shiptonm like oeeh.shiptonm        at 12
    s-lit11b as c format "x(12)"         at 50
    s-lit1b  as c format "x(22)"         at 62
    s-shiptoaddr[1]                      at 12
    ""                                   at 50
    s-lit3b  as c format "x(22)"         at 62 
    s-shiptoaddr[2]                      at 12
    s-lit12a as c format "x(10)"         at 50
    s-lit12b as c format "x(3)"          at 82
    s-lit12c as c format "x(7)"          at 104
    s-lit12d as c format "x(5)"          at 117
    s-shipcity as c format "x(35)"       at 12
    s-whsedesc as c format "x(30)"       at 50
    s-shipvia as c format "x(12)"        at 82
    s-cod as c format "x(6)"             at 95
    s-shipdt like oeeh.shipdt            at 104
    s-terms as c format "x(12)"          at 117
    s-litsfax as c format "X(30)"        at 12
    s-rpttitle like sapb.rpttitle        at 1 
with frame f-head no-box no-labels width 132 page-top.
 
form header  /* detail line title */
    s-lit14a as c format "x(75)"         at 1
    s-lit14b as c format "x(53)"         at 76
    s-lit15a as c format "x(75)"         at 1
    s-lit15b as c format "x(53)"         at 76
    s-lit16a as c format "x(78)"         at 1
    s-lit16b as c format "x(53)"         at 79
with frame f-headl no-box no-labels width 132 page-top.

form  /* detail line totals  */
    skip(1)
    v-linecnt         format "zz9"       at 1
    s-lit40a     as c format "x(11)"     at 5
    s-lit40c     as c format "x(17)"     at 44
    s-totqtyshp  as c format "x(13)"     at 62
    s-lit40d     as c format "x(18)"     at 82
    s-totlineamt as c format "x(13)"     at 117
with frame f-tot1 no-box no-labels no-underline width 132.

form  /* all extra costs display  */
    s-canlit as c   format "x(60)"         at 15    /*si02*/
    s-title2 as c format "x(33)"           at 82
    s-amt2   as dec format "zzzzzzzz9.99-" at 117
with frame f-tot2 no-box no-labels width 132 no-underline down.

form  /* invoice total*/
    s-currencyty as c format "x(51)"     at 24      /*si02*/
    s-invtitle as c format "x(18)"       at 82
    s-totinvamt like oeeh.totinvamt      at 117
with frame f-tot3 no-box no-labels no-underline width 132.

form header  /* footter at last page */
    " "                                      at 1                          
    s-lit48a as c format "x(9)"              at 1
    s-CSR-title  as c format "x(25)"         at 20
    s-contact    as c format "x(30)"         at 46
    s-contlit-ph as c format "x(4)"          at 82
    s-contact-ph as c format "(xxx)xxx-xxxx" at 87
    s-contlit-fx as c format "x(4)"          at 102
    s-contact-fx as c format "(xxx)xxx-xxxx" at 107
with frame f-tot4 no-box no-labels width 132 page-bottom.

assign
    v-pageno    = page-number - 1
    v-subnetamt = 0.

    {a-vaepv.i s-}

assign
    s-billname   = arsc.name
    s-billaddr1  = arsc.addr[1]
    s-billaddr2  = arsc.addr[2]
    s-billcity   = arsc.city + ", " + arsc.state + " " + arsc.zipcd
    s-litbfax    = ""
    s-cod        = ""
    s-shipdt     = if avail vasp and vasp.user9 = ? then
                      vasp.transdt 
                   else 
                      vasp.user9
    s-shiptonm   = if length(arsc.phoneno) > 1 and 
                      substring(arsc.phone,1,1) = "(" and 
                      substring(arsc.phone,5,1) = ")" then
                      "Phone: " + arsc.phone 
                   else 
                   if length(arsc.phoneno) > 1 and 
                      substring(arsc.phone,1,1) ne "(" then 
                      "Phone: " + string(arsc.phone,"(xxx)xxx-xxxx xxxxx") 
                   else ""   
    s-shiptoaddr[1]
                 = if length(arsc.faxphoneno) > 1 and 
                      substring(arsc.faxphone,1,1) = "(" and 
                      substring(arsc.faxphone,5,1) = ")" then
                      "  Fax: " + arsc.faxphone 
                   else 
                   if length(arsc.faxphoneno) > 1 and 
                      substring(arsc.faxphone,1,1) ne "(" then 
                      "  Fax: " + string(arsc.faxphone,"(xxx)xxx-xxxx xxxxx") 
                   else ""           
 /*
    s-totlineamt = if oeeh.lumpbillfl then
                       string(oeeh.lumpbillamt,"zzzzzzzz9.99-")
                   else if oeeh.transtype = "rm" then
                       string((oeeh.totlineamt * -1),"zzzzzzzz9.99-")
                   else string(v-totlineamt,"zzzzzzzz9.99-")
    s-taxes      = oeeh.taxamt[1] + oeeh.taxamt[2] +
                   oeeh.taxamt[3] + oeeh.taxamt[4]
    s-dwnpmtamt  = if oeeh.transtype = "cs" then oeeh.tendamt
                   else if oeeh.dwnpmttype then oeeh.dwnpmtamt
                   else (oeeh.dwnpmtamt / 100) * oeeh.totinvamt
    s-dwnpmtamt  = s-dwnpmtamt + oeeh.writeoffamt
*/
    v-linecnt    = 0.
/*
    s-shiptonm      = oeeh.shiptonm
    s-shiptoaddr[1] = oeeh.shiptoaddr[1]
    s-shiptoaddr[2] = oeeh.shiptoaddr[2]
    s-shipcity      = oeeh.shiptocity + ", " + oeeh.shiptost +
                      " " + oeeh.shiptozip
    /*SI02*/
    s-litsfax    = if oeeh.divno = 40 and avail arss and 
                   length(arss.faxphoneno) > 1 then
                   "Fax: " + string(arss.faxphoneno,"(xxx)xxx-xxxx xxxxx") else
                   if oeeh.divno = 40 and 
                   not avail arss and 
                   s-billname = s-shiptonm and
                   s-billaddr1 = s-shiptoaddr[1] and
                   s-billaddr2 = s-shiptoaddr[2] and
                   s-billcity = s-shipcity and
                   length(arsc.faxphoneno) > 1 then
                   "Fax: " + string(arsc.faxphone,"(xxx)xxx-xxxx xxxxx") 
                   else ""

    s-gstregno      = if g-country = "ca" then b-sasc.fedtaxid else ""
    s-pstlicense    = if g-country = "ca" then oeeh.pstlicenseno
                         else "".
*/
{w-icsd.i g-whse no-lock}

/* ITB# 2 */
assign s-corrcity = icsd.city + ", " + icsd.state + " " + icsd.zipcd
       s-remitnm = icsd.name
       s-remit-addr1 = icsd.addr[1]
       s-remit-addr2 = icsd.addr[2]
       s-whsedesc    = icsd.name
       s-lit1b       = "Phone: " + string(icsd.phoneno,"(xxx)xxx-xxxx xxxxx")
       s-lit3b       = "  Fax: " + 
                                 string(icsd.faxphoneno,"(xxx)xxx-xxxx xxxxx").

view frame f-head.
view frame f-headl.
view frame f-tot4.

find first oimsp where oimsp.person = g-operinits no-lock no-error.
if avail(oimsp) then
   assign s-CSR-title  = "Customer Service Contact:"
          s-contact    = oimsp.name
          s-contlit-ph = "TEL:"
          s-contact-ph = oimsp.phoneno
          s-contlit-fx = "FAX:"
          s-contact-fx = oimsp.faxphoneno.
else
   assign s-CSR-title  = ""
          s-contact    = ""
          s-contlit-ph = ""
          s-contact-ph = ""
          s-contlit-fx = ""
          s-contact-fx = "".

          
          
