/*h*****************************************************************************
  INCLUDE      : p-oeepp1.i
  DESCRIPTION  : Order Entry Print Pick Tickets - Print Serial/Lot# infor
  USED ONCE?   : no
  AUTHOR       : rhl
  DATE WRITTEN : 04/20/93
  CHANGES MADE :
    tb 9409 04/20/93 rhl; Pick Ticket Serial # in random order
    04/27/95 mtt; TB# 13356 Serial/Lot # doesn't print on PO/RM
    07/02/96 jms; TB# (10A) 21254 Develop Value Add Module
        Added VAEPP1 to a comment
    04/06/99 cm;  TB# 26054 added &comui, &export_files and &usage functionality
    10/22/99 cm;  TB# 26054 Removed OPTIO interface
    01/24/01 mms; TB# e7644 WT BOD Project - Allow kpet assigned serials to
        be printed on fabricated kits.
    11/28/05 blw; TB# L1763 Add serial/lot printing for PO's tied to VA orders.
*******************************************************************************/

/*e This include is used in both OEEPP, OEEPI, WTEP1 and VAEPP1
    When &comforkpout is set to "out" then an alternative display is done for
    a KP Work Order below. */
&if "{&comforkpout}" <> "out":u and
    "{&comforvaout}" <> "out":u
&then

        /*d Serial # print  */
        if avail icsw and icsw.serlottype = "s":u and
            {&snlot} ne {&stkqtyship}
        then do:
            i = 0.
            clear frame f-serial.
            for each icets use-index k-order where
                 icets.cono      = {&line}.cono and
                 icets.ordertype = "{&type}" and
                 icets.orderno   = {&line}.{&pref}no and
                 icets.ordersuf  = {&line}.{&pref}suf and
                 icets.lineno    = {&line}.lineno and
                 icets.seqno     = {&seq}
            no-lock

            /*tb 9409 04/20/93 rhl; Pick Ticket Serial # in random order */
            by icets.serialno:
                assign i             = i + 1
                       s-serialno[i] = icets.serialno.
                display s-serialno[i] with frame f-serial.
                if i = 5 then do:
                    i = 0.
                    down with frame f-serial.
                end.
            end.
        end.

        /*d  Lot print  */
        if avail icsw and icsw.serlottype = "l":u and
            {&snlot} ne {&stkqtyship}
        then do:
            i = 0.
            clear frame f-lot.
            for each icetl use-index k-order where
                 icetl.cono      = {&line}.cono and
                 icetl.ordertype = "{&type}" and
                 icetl.orderno   = {&line}.{&pref}no and
                 icetl.ordersuf  = {&line}.{&pref}suf and
                 icetl.lineno    = {&line}.lineno and
                 icetl.seqno     = {&seq} no-lock
             /*tb 9409 04/20/93 rhl; Pick Ticket Serial # in random order */
             by icetl.lotno:
                i = i + 1.
                if i = 1 then do:
                    assign s-lot1  = icetl.lotno
                           s-qty1  = icetl.quantity
                           s-lot1d = "Lot #:"
                           s-qty1d = "Qty:".
                    display s-lot1 s-qty1 s-lot1d s-qty1d with frame f-lot.
                end.
                else do:
                    assign i       = 0
                           s-lot2  = icetl.lotno
                           s-qty2  = icetl.quantity
                           s-lot2d = "Lot #:"
                           s-qty2d = "Qty:".
                    display s-lot2 s-qty2 s-lot2d s-qty2d with frame f-lot.
                    down with frame f-lot.
                end.
            end.
        end.
&endif

/* Use this processing when coming from a KP WO.  If the component is either
   a serial or a lot product, then always print the serial or lot #. */
/*tb L1763 - Integrate VA serial/lot component print with new frame */
&if "{&comforkpout}" = "out":u or
    "{&comforvaout}" = "out":u
&then

    /*d Serial # print  */
    if avail icsw and icsw.serlottype = "s":u
    then do:

        i = 0.
        &if "{&comforkpout}" = "out":u
        &then
            clear frame f-serial.
        &else
            clear frame f-serial2.
        &endif

        for each icets use-index k-order where
                 icets.cono      = {&line}.cono and
                 icets.ordertype = "{&type}"    and
                 icets.orderno   = {&wono}      and
                 icets.ordersuf  = {&wosuf}     and
                 icets.lineno    = {&lineno}    and
                 icets.seqno     = {&seq}
        no-lock
        by icets.serialno:

            assign
                i             = i + 1
                s-serialno[i] = icets.serialno.

            display s-serialno[i]
            &if "{&comforkpout}" = "out":u
            &then
                with frame f-serial.
            &else
                with frame f-serial2.
            &endif

            if i = 5 then do:

                i = 0.
                &if "{&comforkpout}" = "out":u
                &then
                    down with frame f-serial.
                &else
                    down with frame f-serial2.
                &endif

            end.

        end.

    end.

    /*d  Lot print  */
    if avail icsw and icsw.serlottype = "l":u
    then do:

        i = 0.
        &if "{&comforkpout}" = "out":u
        &then
            clear frame f-lot.
        &else
            clear frame f-lot2.
        &endif

        for each icetl use-index k-order where
            icetl.cono      = {&line}.cono and
            icetl.ordertype = "{&type}"    and
            icetl.orderno   = {&wono}      and
            icetl.ordersuf  = {&wosuf}     and
            icetl.lineno    = {&lineno}    and
            icetl.seqno     = {&seq} no-lock
        by icetl.lotno:

            i = i + 1.

            if i = 1 then do:

                assign
                     s-lot1  = icetl.lotno
                     s-qty1  = icetl.quantity
                     s-lot1d = "Lot #:"
                     s-qty1d = "Qty:".

                display s-lot1 s-qty1 s-lot1d s-qty1d
                &if "{&comforkpout}" = "out":u
                &then
                    with frame f-lot.
                &else
                    with frame f-lot2.
                &endif

            end.
            else do:

                assign
                    i       = 0
                    s-lot2  = icetl.lotno
                    s-qty2  = icetl.quantity
                    s-lot2d = "Lot #:"
                    s-qty2d = "Qty:".

                display s-lot2 s-qty2 s-lot2d s-qty2d
                &if "{&comforkpout}" = "out":u
                &then
                    with frame f-lot.
                    down with frame f-lot.
                &else
                    with frame f-lot2.
                    down with frame f-lot2.
                &endif


            end.

        end.

    end.
&endif
