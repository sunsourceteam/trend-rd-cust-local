/*h*****************************************************************************
  PROCEDURE    : edepi.p
  DESCRIPTION  : EDI-Incoming Invoice
  AUTHOR       : jlc
  DATE WRITTEN : 04/18/95 (Rewritten)
  CHANGES MADE :
    05/05/95 jlc; TB# 15807 New Program.
    06/25/96 jkp; TB# 21222 Change check for module purchased to use
        modcheck.gpr include, so message is displayed.
    09/14/96 jlc; TB# 20139 Allow for soft edi document directories.
    01/10/97 jkp; TB# 22436 Added the explanatory comment of how the report
        fields are assigned.
    02/21/97 jkp; TB#  7241 (5.2) Spec8.0 Special Price Costing.  Added call
        of speccost.gva.
    01/25/02 blr; TB# 11799 Take character ap edi import process from sx 3.0
        and roll up into sx 3.1.004.
    03/15/02 mms; TB# e8796 edepp - did not process batch but renamed file;
        Roll back to 3.1.
    06/26/02 blr; TB# e10498 Modify the Import Process to write out
        Invoice Transactions that cannot be processed into a separate flat
        file, so the user can view this data, print, or change and reprocess.
        Also modified the bottom of the error report to display how many
        invoices errored out and where to find the flat file.
    10/02/02 blr; TB# e10498/11799 Modify for TB Turn where multiple invoices
        with errors didn't always print on the report.
    12/10/01 lcb; TB# e11494 Remove quoter from EDI functions. Added this as
        part of tb turn above (10498/11799).
    07/13/06 gwk; TB# e24080 Clean up terms processing
    02/07/08 gwk; ERP-30464 Handle more than 999 invoices in EDI input file
        by creating a new batch and resetting the setno to 1.
    08/05/09 bp ; MSF 122513 Add user hooks.
    09/02/10 dww; MSF 132444 Remove REPORT table from SX
    06/09/11 mwb; MSF 135260 Removed changes for ERP-30464
*******************************************************************************/

/*e*****************************************************************************
  REPORT FILE ASSIGNS  :

    This list is alphabetical by report field.  If no specific type (based on
        the transcd) is listed, the field is used for all types.  If specific
        types are listed, it is only used for the types listed.  The file
        prefix (b-) is not identified, unless it is critical to understanding
        the assignment.

    Types (stored in the report.outputty field) used are:
        type n - Notes Data
        type u - Item User Data
        type a - Addon Data

    Line data is stored in the temp-table t-lines.

    report.c12      = (type a-addon) substr(g-data,20,8)        Trend Addon
    report.c13      = (type a-addon) substr(g-data,28,8)        User2
    report.c20      = (type n-notes) substr(g-data,55,12)
                      (type u-user)  substr(g-data,7,16)        User1 & User2
                      (type a-addon) substr(g-data,108,6)
    report.c24      = (type u-user)  substr(g-data,23,24)       User3
                      (type a-addon) substr(g-data,36,24)       User3
    report.c24-2    = (type n-notes) substr(g-data,7,24)
                      (type u-user)  substr(g-data,47,24)       User3 Continued
                      (type a-addon) substr(g-data,60,24)
    report.c24-3    = (type n-notes) substr(g-data,31,24)
                      (type u-user)  substr(g-data,71,24)       User3 Continued
                      (type a-addon) substr(g-data,84,24)
    report.c4       = (type a-addon) substr(g-data,17,3)
    report.c6       = (type u-user)  substr(g-data,95,6)        User3 Continued
                                                                (final section)
    report.cono     = g-cono
    report.de12d0   = (type n-notes) v-noteseq
                      (type u-user)  dec(v-lnseqno)             Assoc. Line #
                      (type a-addon) 0 (zero)
    report.de9d2s   = (type u-user)  dec(substr(g-data,257,16)) User6
                      (type a-addon) dec(substr(g-data,8,9))    Addon Amount
    report.de9d2s-2 = (type u-user)  dec(substr(g-data,273,16)) User7
    report.dt       = (type u-user)  date(substr(g-data,289,8))
    report.dt-2     = (type u-user)  date(substr(g-data,297,8))
    report.oper2    = g-operinit
    report.outputty = (type n-notes) "n" for Notes Data
                      (type u-user)  "u" for User Data
                      {type a-addon) "a" for Addon Data
    report.reportnm = v-reportnm
*******************************************************************************/

do for sapb:
    {p-rptbeg.i}
    {edepi.gva "new"}
    
/* SXE 5.5 */
     def new  shared var g-wodiscamt     like poeh.wodiscamt          no-undo.

    {tt-report.gtt &shared = "new shared"}
    {g-post.i}
    {g-gl.i}

    /*tb  7241 (5.2) 02/21/97 jkp; Added call of speccost.gva. */
    {speccost.gva "new shared"}
    def new shared stream importbatch.
    def new shared stream edilog.
    def new shared stream edierrdat.
    def new shared frame f-head.
    def new shared frame f-edilog.
    def new shared frame f-errhead.
    def new shared frame f-err.
    def new shared frame f-errin.

    def            var o-jrnlno      like g-jrnlno                      no-undo.

    /*d Paramaters passed to p-edidir. Only store values here temporarily */
    def            var v-docnm       as c format "x(15)"            no-undo.
    def            var v-docdir      as c format "x(70)"            no-undo.
    def            var v-randomnm    as c format "x(15)"            no-undo.
    def            var v-rdmdir      as c format "x(70)"            no-undo.
    def            var v-fulldir     as c format "x(70)"            no-undo.

    /*tb 20139 09/14/96 jlc; Allow for soft edi document directories */
    def            var v-ediindir    like sasc.ediindir             no-undo.

    /*d Permanent name (including the directory) of the edepi, edi810_i.dat and
         the edi810_i.bak incoming files */
    def            var v-edepidir    as c format "x(70)"                no-undo.
    def            var v-810idatdir  as c format "x(70)"                no-undo.
    def            var v-810ibakdir  as c format "x(70)"                no-undo.

    /*d Used by s-edicat.i to cat the temporary random edi files that are
         created to the end of the permanent flat files */
    def            var v-from        as c format "x(70)"                no-undo.
    def            var v-to          as c format "x(70)"                no-undo.

    /*d Used by s-edirm.i to indicate the file to remove */
    def            var v-edirmfile   as c format "x(70)"                no-undo.

    /*d Used by s-edidir.i to indicate a valid edi directory */
    def            var v-dirok       as c format "x(8)"                 no-undo.

    {edepi.z99 &user_def = "*"}

    {edepi.gfo}

    /*pause 0 before-hide.*/

    /* Check module purchased flag */
    /*tb 21222 06/25/96 jkp; Change to modcheck.gpr include so standard message
         is displayed. Do not need the sasa (&sasacom) read because p-rptbeg.i
         is being used and it already reads sasa.  Do not want pause (&com). */
    {modcheck.gpr
        &module  = "ed"
        &sasacom = "/*"
        &com     = "/*"}

    /* Load Parameters from SAPB */
    assign
        g-edepi99fl     = if search("edepi99.r") ne ? then yes else no
        p-batchnm       = sapb.optvalue[1]
        p-updfl         = if sapb.optvalue[2] = "yes" then yes else no
        p-logfile       = sapb.optvalue[3]   /* printer/file for log */
        p-printfl       = if sapb.optvalue[4] = "yes" then yes else no
        p-invpo         = if sapb.optvalue[5] = "yes" then yes else no
        p-glno          = sapb.optvalue[6]
        p-edidir        = sapb.optvalue[7]
        p-newtermslogic
                        = if sapb.optvalue[8] = "yes":u then yes else no
        p-title         = "EDI - Invoice Log"
        g-headln        =
  /*1234567890123456789012345678901234567890123456789012345678901234567890*/
   "Line #   Product/Description           Quantity      Unit       Price"
        g-uline     = fill("-",132).
end.

{p-center.i p-title 50}

/************ Open Alternate Streams ******************/
if p-logfile ne "" then do:
    {p-rptst1.i
        &prno   = 1
        &title  = "p-title at 15"
        &stream = "edilog"
        &rptst  = "stream edilog"}
end.

/*o Find, copy, and rename the incoming edi file to a filename we
    use for Trend */
/*tb 20139 09/14/96 jlc; Allow for soft edi document directories */
assign
    v-docnm     = "edepi.dat":u
    v-ediindir  = if avail sasc then sasc.ediindir
                  else ""
    v-randomnm  = "excdat":u + "_":u +
                   substring(string(g-today,"99/99/99":u),1,2) +
                   substring(string(g-today,"99/99/99":u),4,2) +
                   substring(string(g-today,"99/99/99":u),7,2) +
                   "_":u +
                   string(time,"hh:mm":u).

/*d Build the location/file name of edepi.dat */

{p-edidir.i
    &docnm          = "v-docnm"
    &edidir         = "p-edidir"
    &docdir         = "v-docdir"
    &rdmdir         = "v-rdmdir"
    &rdmnm          = "v-randomnm"
    &fulldir        = "v-fulldir"
    &edidefault     = "v-ediindir"}

/*d Before reading the edi files, ensure a valid directory has been
    specified as the location of the edi files.  */
{s-edidir.i}
if v-dirok <> "yes" then do:
    g-mess  =     "Invalid Directory: " + v-fulldir.
    display g-mess with frame f-mess.
    return.
end.

assign
    v-edepidir   = v-docdir
    v-docnm      = "edi810_i.dat"
    g-excdir     = v-rdmdir.

/*d Build the location/file name of edi810_i.dat */
/*tb 20139 09/14/96 jlc; Allow for soft edi document directories */
{p-edidir.i
    &docnm      = "v-docnm"
    &edidir     = "p-edidir"
    &docdir     = "v-docdir"
    &rdmdir     = "v-rdmdir"
    &rdmnm      = "v-randomnm"
    &fulldir    = "v-fulldir"
    &edidefault = "v-ediindir"}

assign
    v-810idatdir   = v-docdir
    v-docnm        = "edi810_i.bak".

/*d Build the location/file name of edi810_i.bak */
/*tb 20139 09/14/96 jlc; Allow for soft edi document directories */
{p-edidir.i
    &docnm      = "v-docnm"
    &edidir     = "p-edidir"
    &docdir     = "v-docdir"
    &rdmdir     = "v-rdmdir"
    &rdmnm      = "v-randomnm"
    &fulldir    = "v-fulldir"
    &edidefault = "v-ediindir"}

v-810ibakdir   = v-docdir.

if search(v-edepidir) ne ? then
do:
    v-edirmfile =   v-edepidir.
    {s-edirm.i}
end.

if search(v-810idatdir) ne ? then
do:
    assign
        v-from  = v-810idatdir
        v-to    = v-edepidir.
    {s-ediren.i}
    v-to    =   v-810ibakdir.
    {s-edicp.i}        /* creates the .bak file even if updatefl = no */

    /*tb e8796 03/15/02 mms; Added "and can-find(sabs..." to the if
        condition */
    if p-updfl and
        can-find(sabs use-index k-sabs where
                 sabs.cono     = g-cono    and
                 sabs.batchnm  = p-batchnm and
                 sabs.freqtype = "o") then do:

        v-edirmfile = v-810idatdir.
        {s-edirm.i}  /*only removes the .dat file if p-updfl = yes */

    end.

end.

else do:
    g-mess  =     "File Not Found: " + v-810idatdir.
    display g-mess with frame f-mess.
    return.
end.

/* TB 11494 lcb; Remove quoter from EDI functions. */
    input STREAM importbatch from value(v-edepidir).
view frame f-errhead.
view stream edilog frame f-head.

/*d Remove all records from the report database with this report name */
{tt-report.gde}

main:
do:
    /*d Read a record to identify whether a batch file exists */
    do for sabs:
        {w-sabs.i p-batchnm no-lock}
        if not avail sabs then do:
            run err.p(5376).
            display p-batchnm with frame f-batchnm.
            leave main.
        end.

        else if sabs.freqtype ne "o" then do:
            g-mess  =     "Invalid Batch Type - Must Be One Time Type Batch".
            display g-mess with frame f-mess.
            display p-batchnm with frame f-batchnm.
            leave main.
        end.

        else if sabs.modulenm ne "ap" or
            (sabs.securinit ne "" and sabs.securinit ne g-operinit)  or
            sabs.inuseby ne "" then do:
                if sabs.modulenm ne "ap" then run err.p(5402).
                else if sabs.inuseby ne "" then do:
                    run err.p(5401).
                    g-mess  =   "In Use By " + sabs.inuseby.
                    display g-mess with frame f-mess.
                end. /* else if sabs.inuseby ne "" */

                else run err.p(5403).
        end.

    end.  /* end sapbs */

    /*o Reserve a Journal Number */
    if p-updfl then
    do for sabs transaction:
        find sabs use-index k-sabs where
             sabs.cono    = g-cono     and
             sabs.batchnm = p-batchnm
        exclusive-lock no-error.
        if avail sabs and
            sabs.freqtype = "o" and
            sabs.jrnlno = 0 then do:

            assign
                o-jrnlno        =   g-jrnlno
                sabs.inuseby    =   g-operinit.
            {edepijnl.lcr}

        end. /* if sabs.freqtype = "o" and sabs.jrnlno = 0 */

        g-jrnlno = if avail sabs then sabs.jrnlno
                   else g-jrnlno.

    end. /* do for sabs transaction */

    /*o run program to read through the incoming edi flat file processing
        data from those records */
    run edepia.p.

    /*o Clear out the sabs inuseby field */
    if p-updfl then
    do for sabs transaction:
        find sabs use-index k-sabs where
             sabs.cono    = g-cono     and
             sabs.batchnm = p-batchnm
        exclusive-lock no-error.
        if avail sabs and sabs.freqtype = "o" then
                sabs.inuseby    =   "".
    end. /* do for sabs transaction */

end.  /* end main */

input close.

/************** Close Alternate Stream *******************/
if p-logfile ne "" then do:
    {p-rptst2.i
        &rptst = "stream edilog"}
end.


display
    skip(1)
with frame f-norec.
g-mess  =   "** " + string(t-updcnt) + " Document Sets Processed **".
display
    g-mess
with frame f-mess.
display
    skip(1)
with frame f-norec.
g-mess  =   "** " + string(t-transcnt) + " Document Sets Read **".
display
    g-mess
with frame f-mess.

/* display the flat file location if transactions errored out and

    couldn't be processed, so the user can view/change, etc.,. */

If p-updfl = true and g-okerrfl = true then do:
    g-mess = "** The " + (string(t-transcnt - t-updcnt)) +
    " Invoices with errors can be found in :" + g-excdir.
    display
        skip(1)
    with frame f-norec.
    display
        g-mess
    with frame f-mess.
end. /* if p-updfl = true and g-okerrfl */

{edepi.z99 &bef_reset =  "*"}

/* Restore the global journal number to that which is was upon entering edepi*/
if p-updfl then g-jrnlno = o-jrnlno.

hide all no-pause.

{edepi.z99 &bottom = "*"}
