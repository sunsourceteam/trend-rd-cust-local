/*  icxm.p                      
    measurables report - ss907
    16/feb/99 gb
    27/feb/99 gb - taking to long - use temp report file for sort.
*/
{p-rptbeg.i}

def var v-sapbhold as recid no-undo.
def var v-sasphold as recid no-undo.
def var v-sassrhold as recid no-undo.

assign v-sapbhold = v-sapbid
       v-sassrhold = v-sassrid
       v-sasphold = v-saspid.




{g-sdirank.i}

{zsapboydef.i}



Define Var jrk_optvalue as character format "x(24)" no-undo extent 20. 
Define Var xinx         as integer                 no-undo.
Define Var sum_ary      as character               no-undo.
Define Var x-summarybld       as character format "x(24)" no-undo. 
Define Var x-mevals     as logical                 no-undo.

Define new shared Var p-bgrpbeg  like icsd.buygroup.
Define new shared Var p-bgrpend  like icsd.buygroup.
Define new shared Var p-deptbeg  as c  format "x(4)" no-undo.
Define new shared Var p-deptend  as c  format "x(4)" no-undo.
Define new shared Var p-buyrbeg  as c  format "x(4)" no-undo.
Define new shared Var p-buyrend  as c  format "x(4)" no-undo.
Define new shared Var p-vendbeg  as de format ">>>>>>>>>>>9" no-undo.
Define new shared Var p-vendend  as de format ">>>>>>>>>>>9" no-undo.
Define new shared Var p-linebeg  as c  format "x(6)" no-undo.
Define new shared Var p-lineend  as c  format "x(6)" no-undo.
Define new shared Var b-postdt as date no-undo.
Define new shared Var e-postdt as date no-undo.

def new shared var p-master as character format "x" no-undo.
def new shared var p-stored as character format "x(24)" no-undo.





Define new shared Var p-distbeg  as character format "x(3)" no-undo.
Define new shared Var p-distend  as character format "x(3)" no-undo.
Define new shared Var p-region   as character format "x(1)" no-undo.
Define new shared Var p-regbeg   as character format "x(4)" no-undo.
Define new shared Var p-regend   as character format "x(4)" no-undo.
Define new shared Var p-iregbeg   as character format "x(1)" no-undo.
Define new shared Var p-iregend   as character format "x(1)" no-undo.
Define new shared Var p-slsrep   as character format "x(4)" no-undo.
Define new shared Var p-repbeg   as character format "x(4)" no-undo.
Define new shared Var p-repend   as character format "x(4)" no-undo.
Define new shared Var p-whsebeg  as character format "x(4)" no-undo.
Define new shared Var p-prodbeg  as character format "x(4)" no-undo.
Define new shared Var p-prodend  as character format "x(4)" no-undo.
Define new shared Var p-pcatbeg  as character format "x(4)" no-undo.
Define new shared Var p-pcatend  as character format "x(4)" no-undo.
Define new shared Var p-custbeg  as decimal                 no-undo.
Define new shared Var p-custend  as decimal                 no-undo.


/* Abbrv tech names
   PNEU
   HYDR
   MOBL
   FILT
   AUTO
   SERV
   NONE
   CONN
   FABR
*/ 


Define new shared  Var p-whseend  as character format "x(4)" no-undo.
Define new shared Var p-techbeg  as character format "x(4)" no-undo.
Define new shared Var p-techend  as character format "x(4)" no-undo.
Define new shared Var p-divbeg   as int no-undo.
Define new shared Var p-divend   as int no-undo.
Define new shared Var p-vpbeg   as character format "x(24)" no-undo.
Define new shared Var p-vpend   as character format "x(24)" no-undo.
Define new shared Var p-vidbeg   as character format "x(24)" no-undo.
Define new shared Var p-vidend   as character format "x(24)" no-undo.


Define new shared Var p-list as logical no-undo.
Define new shared Var p-optiontype as character format "x(15)" no-undo.
Define new shared Var p-sorttype as character format "x(15)"   no-undo.
Define new shared Var p-totaltype as character format "x(15)"  no-undo.
Define new shared Var p-summtype as character format "x(15)"   no-undo.
Define new shared Var p-summcounts as character format "x(24)" no-undo.
Define new shared Var p-export as character format "x(24)"     no-undo.
Define new shared Var p-obsolete as logical                    no-undo.
Define new shared Var p-recvmonths as integer                  no-undo.
Define new shared Var p-ohonly as logical                      no-undo.
Define new shared Var p-pastsuff as char format "x(15)"        no-undo.



define var x-breakreg as char format "x(200)" no-undo.
define var x-breakpct as char format "x(200)" no-undo.
define var x-breaklst as char format "x(200)" no-undo.
define var x-cust     as char format "x(200)" no-undo.
define var x-shipto   as char format "x(200)" no-undo.
define var x-range    as int                  no-undo.
define var x-inx      as int                  no-undo.  
define var s-brklit   as char format "x(7)"   no-undo.

define var jj as decimal no-undo.
define var xx-begdate as date                 no-undo.
define var xx-enddate as date                 no-undo.
define var xx-usage   as dec                  no-undo.
define var xx-usageval as dec                 no-undo.
define var xx-toh     like icsw.qtyonhand     no-undo.

define var opendept as logical init false no-undo. 
define var fylename as character format "x(30)" no-undo.

def new shared var i-turnmo as de format ">9" no-undo.





def var za as decimal no-undo.
def var v-totoh as dec no-undo.
def var bgrp as character format "x(4)" no-undo.
def var bbuy as character format "x(4)" no-undo.
def var cur-inv as de format ">>>>>>>9.99-" no-undo.
def var avg-inv as de format ">>>>>>>9.99-" no-undo.
def var n-cgs   as de format ">>>>>>>9.99-" no-undo.
def var n-cog   as de format ">>>>>>>9.99-" no-undo.
def var n-inv   as de format ">>>>>>>9.99-" no-undo.
def var slo-inv as de format ">>>>>>>9.99-" no-undo.
def var obs-inv as de format ">>>>>>>9.99-" no-undo.
def var comp-ln as de format ">>>>>>>9-"    no-undo.
def var comp-qty as de format ">>>>>>>9.99-" no-undo.
def var comp-val as de format ">>>>>>>9.99-" no-undo.
def var tot-ln  as de format ">>>>>>>9-"    no-undo.
def var tot-qty as de format ">>>>>>>9.99-" no-undo.
def var tot-val as de format ">>>>>>>9.99-" no-undo.
def var d-turn  as de format ">>>>9.9-"     no-undo.
def var slo-pct as de format ">>>9.9-"      no-undo.
def var obs-pct as de format ">>>9.9-"      no-undo.
def var d-name  as c  format "x(15)"        no-undo.
def var z-name  as c  format "x(15)"        no-undo.
def var h-icslfound as logical no-undo.
def var h-validparams as logical no-undo.
def var n-val   as dec format ">>>>>>>9.99-" no-undo.
def var h-buyer    like icsl.buyer no-undo.
def var h-prodline like icsl.prodline no-undo.
def var h-vendor   like icsl.vendno no-undo.
def var h-searchlevel as i no-undo.
def var h-lined       as logical no-undo.
def var v-phase2      as logical no-undo init false.
def var forbegdate as date no-undo.
def var forenddate as date no-undo.
def var z-icswrecid  as recid no-undo.
def var z-usage      as decimal format ">>>>>>>9.99-" no-undo.
def var z-avgval     as decimal format ">>>>>>>9.99-" no-undo.
def var d-option     as char format "x(8)" no-undo.
def var h-person     like oimsp.perdept no-undo.

Define Var v-technology  as character format "x(30)"  no-undo.
Define Var v-parentcode as char format "x(24)"  no-undo.
Define Var v-vendorid as char format "x(24)"  no-undo.

Define buffer catmaster for notes.

def var v-peckinx as integer no-undo.
def var zzx as integer no-undo.

def var v-peckinglist as character format "x(4)" extent 9 init
  ["dros",
   "dren",
   "dhou",
   "dmcp",
   "datl",
   "decp",
   "dscp",
   "dwcp",
   "dxcp"].

define buffer x-icsw for icsw.


DEF temp-table whses no-undo
  field zwhse like icsw.whse
  field zcnt as integer
  
  index k-zpecking
        zcnt
  index k-zwhse
        zwhse.



def new shared temp-table obsolete no-undo 
  field o-prod     like icsw.prod
  field o-proddesc as c format "x(30)"
  field o-vendor   like apsv.vendno
  field o-vname    as c format "x(30)"
  field o-ls12month  as integer 
  field o-6month   as integer
  field o-12month  as integer 
  field o-24month  as integer
  field o-48month  as integer
  field o-60month  as integer
  field o-g60month  as integer
  field o-ns7      as integer
  field o-ns12     as integer
  field o-ns24     as integer
  field o-total    as integer
  field o-asofdate as date
 
 
  field o-enterdt  as date
  field o-rcptdt   as date
  field o-invdt    as date
  field o-firstrcpt as date
  field o-rcptohdt as date
  field o-12b4rcpt as integer   
  field o-rcptoh   as integer    
    
    
  index o-inx
        o-prod.


def temp-table zoh no-undo
  field prod like icsp.prod
  field qtyoh like icsw.qtyonhand
  
 index zx-zoh
   prod.



Define temp-table whsqty no-undo
  field prod   like icsw.prod
  field whse   like icsw.whse
  field qtyoh   like icsw.qtyonhand
  field vendor  as char format "x(15)"
  field vcost like icsw.avgcost
  field acost like icsw.avgcost

  index w-inx
        prod
        whse.

define var whsqtyoh like icsw.qtyonhand no-undo.
define var whsqtycost like icsw.avgcost no-undo.
define var v-prodhold like icsp.prod    no-undo init "!!@@##".



def new shared temp-table wuyers  no-undo
  Field wregion as char format "x(1)"
  Field wdistrict as char format "x(3)"
 /*
    Field wxbreakreg as char
    Field wxbreakpct as char
    Field wxbreaklst as char
    Field wxcust as char
    Field wxshipto as char
    Field wxrange as integer
  */
  Field woh like icsw.qtyonhand
  Field wavgcost like icsw.avgcost  
  Field wcustno  like arsc.custno
  Field wshipto  like arss.shipto    
  Field wlstsale  as  integer  
  Field wlsttrans  as  character 
  Field wmaster as logical
  Field wwhse like icsw.whse
  Field wicsdregion like icsd.region
  Field wprod as char format "x(24)"
  Field wprodcat as char format "x(4)"
  Field wgroup as char format "x(4)"
  Field wbuyer as char format "x(4)"
  Field wdept as char format "x(15)"
  Field wname  as char format "x(15)"
  Field wvendor like icsw.arpvendno
  Field wpline like icsl.prodline
  Field wcur as de format ">>>>>>>9.99-"
  Field wncur as de format ">>>>>>>9.99-"
  Field wavg as de format ">>>>>>>9.99-"
  Field wcgs as de format ">>>>>>>9.99-"
  Field wncgs as de format ">>>>>>>9.99-"
  Field wls12 as de format ">>>>>>>9.99-" 
  Field w6 as de format ">>>>>>>9.99-" 
  Field w12 as de format ">>>>>>>9-"    
  Field w24 as de format ">>>>>>>9.99-" 
  Field w48 as de format ">>>>>>>9.99-" 
  Field w60  as de format ">>>>>>>9.99-"    
  Field wg60  as de format ">>>>>>>9.99-" 
  Field wns7 as de format ">>>>>>>9.99-" 
  Field wns12 as de format ">>>>>>>9.99-" 
  Field wns24 as de format ">>>>>>>9.99-"
  Field wqls12 as de format ">>>>>>>9.99-" 
  Field wq6 as de format ">>>>>>>9.99-" 
  Field wq12 as de format ">>>>>>>9.99-" 
  Field wq24 as de format ">>>>>>>9.99-" 
  Field wq48 as de format ">>>>>>>9.99-" 
  Field wq60  as de format ">>>>>>>9.99-" 
  Field wqg60  as de format ">>>>>>>9.99-" 
  Field wqns7 as de format ">>>>>>>9.99-" 
  Field wqns12 as de format ">>>>>>>9.99-" 
  Field wqns24 as de format ">>>>>>>9.99-" 
   
   index wkey
     wmaster
     wwhse
     wprod

   index wkey2
     wmaster
     wprod
     wwhse

index wkey3  
      wmaster
      wregion
      wdistrict
      wicsdregion
      wwhse
      wgroup
      wbuyer
      wdept
      wpline
      wprodcat
      wvendor
      wcustno
      wprod.
      
      
def new shared temp-table r1range  no-undo

Field r1region   as char format "x"
Field r1district as char format "x(3)"
Field r1icsdregion like icsd.region
Field r1whse like icsw.whse
Field r2group as char format "x(4)"
field r2dept as char format "x(15)"
Field r2buyer as char format "x(4)"
Field r2vendor like apsv.vendno
Field r2pline like icsl.prodline
Field r3prodcat as character format "x(4)"

 

index r1key
      r1region
      r1district
      r1icsdregion
      r1whse
      r2group
      r2dept
      r2buyer
      r2vendor
      r2pline
      r3prodcat.

 
define buffer zwuyers for wuyers.
define buffer xwuyers for wuyers.
define buffer b-xpart for xpart.


find sapb where recid(sapb) = v-sapbid no-lock no-error.



 
assign p-bgrpbeg = sapb.rangebeg[1]
       p-bgrpend = sapb.rangeend[1]
       p-regbeg = sapb.rangebeg[2]
       p-regend = sapb.rangeend[2]
       p-whsebeg = sapb.rangebeg[3]
       p-whseend = sapb.rangeend[3]
       p-buyrbeg = sapb.rangebeg[4]
       p-buyrend = sapb.rangeend[4]
       p-vendbeg = dec(sapb.rangebeg[5])
       p-vendend = dec(sapb.rangeend[5])
       p-deptbeg = sapb.rangebeg[7]
       p-deptend = sapb.rangeend[7]
       p-linebeg = sapb.rangebeg[6]
       p-lineend = sapb.rangeend[6]
       p-techbeg = sapb.rangebeg[8]
       p-techend = sapb.rangeend[8]
       p-vpbeg = sapb.rangebeg[9]
       p-vpend = sapb.rangeend[9]
       p-iregbeg = sapb.rangebeg[10]
       p-iregend = sapb.rangeend[10]
       p-prodbeg = sapb.rangebeg[11]
       p-prodend = sapb.rangeend[11]
       p-vidbeg  = sapb.rangebeg[12]
       p-vidend  = sapb.rangeend[12]
       p-custbeg = dec(sapb.rangebeg[13])
       p-custend = dec(sapb.rangeend[13])
       p-pcatbeg  = sapb.rangebeg[14]
       p-pcatend  = sapb.rangeend[14]
       p-repbeg  = sapb.rangebeg[15]
       p-repend  = sapb.rangeend[15].

run formatreference.
assign       
       i-turnmo = int(jrk_optvalue[1])
       p-optiontype = jrk_optvalue[2]
       p-sorttype    = jrk_optvalue[3]
       p-totaltype   = jrk_optvalue[4]
       p-summcounts = jrk_optvalue[5]
       p-master  =  jrk_optvalue[6]
       p-stored =  jrk_optvalue[7]
       p-list = if jrk_optvalue[8] = "yes" then true else false
       p-export = jrk_optvalue[9]
       p-obsolete = if jrk_optvalue[10] = "yes" then true else false
       p-recvmonths = int(jrk_optvalue[11])
       p-ohonly   = if jrk_optvalue[12] = "yes" then true else false
       p-pastsuff = jrk_optvalue[13].



define var v-monitors as logical no-undo.
assign v-monitors = if g-operinits = "tah" then
                      true
                    else
                      false.



assign zzl_begcat =  p-pcatbeg
       zzl_endcat =  p-pcatend
       zzl_begcust = p-custbeg
       zzl_endcust = p-custend
       zzl_begvname  = p-vidbeg
       zzl_endvname  = p-vidend
       zzl_begvend   = p-vendbeg
       zzl_endvend   = p-vendend
       zzl_begbuy    = p-buyrbeg
       zzl_endbuy    = p-buyrend
       zzl_begwhse   = p-whsebeg
       zzl_endwhse   = p-whseend
       zzl_begrep  = p-repbeg
       zzl_endrep  = p-repend.

assign zel_begcat =  p-pcatbeg
       zel_endcat =  p-pcatend
       zel_begcust = p-custbeg
       zel_endcust = p-custend
       zel_begvname  = p-vidbeg
       zel_endvname  = p-vidend
       zel_begvend   = p-vendbeg
       zel_endvend   = p-vendend
       zel_begbuy    = p-buyrbeg
       zel_endbuy    = p-buyrend
       zel_begwhse   = p-whsebeg
       zel_endwhse   = p-whseend
       zel_begrep  = p-repbeg
       zel_endrep  = p-repend.


if p-list then
  do:
  assign zel_begcat =  p-pcatbeg
         zel_endcat =  p-pcatend
         zel_begcust = p-custbeg
         zel_endcust = p-custend
         zel_begvname  = p-vidbeg
         zel_endvname  = p-vidend
         zel_begvend   = p-vendbeg
         zel_endvend   = p-vendend
         zel_begbuy    = p-buyrbeg
         zel_endbuy    = p-buyrend
         zel_begwhse   = p-whsebeg
         zel_endwhse   = p-whseend
         zel_begrep  = p-repbeg
         zel_endrep  = p-repend.
          

    {zsapboyload.i &delcom = "/*"}

    assign zzl_begcat    =  zel_begcat
           zzl_endcat    =  zel_endcat
           zzl_begcust   = zel_begcust
           zzl_endcust   = zel_endcust
           zzl_begvname  = zel_begvname
           zzl_endvname  = zel_endvname
           zzl_begvend   = zel_begvend
           zzl_endvend   = zel_endvend
           zzl_begbuy    = zel_begbuy
           zzl_endbuy    = zel_endbuy
           zzl_begwhse   = zel_begwhse
           zzl_endwhse   = zel_endwhse
           zzl_begrep    = zel_begrep
           zzl_endrep    = zel_endrep.
          


  end.


/* Zelection_matrix has to have an extent for each selection type 
   right now we have. During the load these booleans are set showing what
   options have been selected.
   
   s - sales rep               ....Index 1
   c - customer number         ....Index 2
   p - product cat             ....Index 3
   b - buyer                   ....Index 4
   t - Order types             ....Index 5
   w - Warehouse               ....Index 6
   v - Vendor number           ....Index 7
   N - Vendor Name             ....Index 8
   
 PLEASE KEEP THIS DOCUMENTATION UPDATED WHEN CHANGED 
 
*/ 

  

assign zelection_matrix [1] = (if zelection_matrix[1] > 1 then /* Rep   */
                                 zelection_matrix[1]
                              else   
                                 1)
       zelection_matrix [2] = (if zelection_matrix[2] > 1 then  /* Cust */
                                 zelection_matrix[2]
                              else   
                                 1)
       zelection_matrix [3] = (if zelection_matrix[3] > 1 then  /* Cat  */
                                 zelection_matrix[3]
                              else   
                                 1)
       zelection_matrix [4] = (if zelection_matrix[4] > 1 then  /* Buyer  */
                                 zelection_matrix[4]
                              else   
                                 1)
       zelection_matrix [6] = (if zelection_matrix[6] > 1 then  /* Whse */
                                 zelection_matrix[6]
                              else   
                                 1)
       zelection_matrix [7] = (if zelection_matrix[7] > 1 then  /* Vendor */
                                 zelection_matrix[7]
                              else   
                                 1)
                                 
       zelection_matrix [8] = (if zelection_matrix[8] > 1 then  /* VName */
                                 zelection_matrix[8]
                              else   
                                 1).
                    



if v-testmode then
   v-printdir = "/mod20b/workfiles/".

if p-master <> "r" then
  do:
  if v-monitors then 
    message "before sdi " today "@ "  
                   int(truncate (time / 3600,0))  ":"
                   int(truncate ((time modulo 3600) / 60 ,0)).

  run introduce_stats. /*  in sdi-tools. */

  if v-monitors then 
    message "after sdi " today "@ "  
                     int(truncate (time / 3600,0))  ":"
                     int(truncate ((time modulo 3600) / 60 ,0)).
/*
  xicswloop:
  for each x-icsw where x-icsw.cono = 1 no-lock: 
   if (x-icsw.qtyonhand + x-icsw.qtyunavail + x-icsw.qtyrcvd) = 0 then
     next xicswloop.

   find zoh where zoh.prod = x-icsw.prod no-lock no-error.
   if not avail zoh then
     do:
     create zoh.
     assign zoh.prod = x-icsw.prod.
     end.
   assign zoh.qtyoh = zoh.qtyoh +
                (x-icsw.qtyonhand + x-icsw.qtyunavail + x-icsw.qtyrcvd). 
  end.

  
  if v-monitors then 
    message "after icsw oh " today "@ "  
                     int(truncate (time / 3600,0))  ":"
                     int(truncate ((time modulo 3600) / 60 ,0)).
*/
  end. 
 
if i-turnmo = 0 then
  i-turnmo = month(today) - 1.


if p-master <> "x" then
  do:
  if p-export <> "" and search (v-printdir + p-export + ".dat") <> ? then
    input stream xpcd from value(v-printdir + p-export + ".dat").
  else  
    input stream xpcd from value(v-printdir + "tbzbmnthdet.dat").

  repeat:
  create obsolete.
  import stream xpcd delimiter "~011"
        obsolete.o-prod
        obsolete.o-proddesc
        obsolete.o-vendor
        obsolete.o-vname    
        obsolete.o-ls12month 
        obsolete.o-6month   
        obsolete.o-12month   
        obsolete.o-24month  
        obsolete.o-48month  
        obsolete.o-60month  
        obsolete.o-g60month  
        obsolete.o-ns7
        obsolete.o-ns12     
        obsolete.o-ns24     
        obsolete.o-total    
        obsolete.o-asofdate 
        obsolete.o-enterdt  
        obsolete.o-rcptdt   
        obsolete.o-invdt    
 
        obsolete.o-firstrcpt 
        obsolete.o-rcptohdt 
        obsolete.o-12b4rcpt    
        obsolete.o-rcptoh.       
         
  end.
  input stream xpcd close.

  assign x-mevals = false.
  if p-export <> "" and search (v-printdir + p-export + "woh") <> ? and
   (p-master = "m" or p-master = "p") then
    do:
    assign x-mevals = true.
    input stream xpcd from value(v-printdir + p-export + "woh").
    repeat:
     create whsqty.
     import stream xpcd whsqty.
    end.
    input stream xpcd close.
    end.
end.


if p-master <> "r" then
  do:
  if v-monitors then 
    message "Before OH load " today "@ "  
                     int(truncate (time / 3600,0))  ":"
                     int(truncate ((time modulo 3600) / 60 ,0)).
  if not x-mevals then
    do:
    xicswloop:
    for each x-icsw where x-icsw.cono = 1 no-lock: 
     if (x-icsw.qtyonhand + x-icsw.qtyunavail + x-icsw.qtyrcvd) = 0 then
       next xicswloop.

     find zoh where zoh.prod = x-icsw.prod no-lock no-error.
     if not avail zoh then
       do:
       create zoh.
       assign zoh.prod = x-icsw.prod.
       end.
     assign zoh.qtyoh = zoh.qtyoh +
                  (x-icsw.qtyonhand + x-icsw.qtyunavail + x-icsw.qtyrcvd). 
    end.
    end.
  else
    do:
    xwhsqtyloop:
    for each whsqty no-lock:
      
    if (whsqty.qtyoh) = 0 then
       next xwhsqtyloop.

     find zoh where zoh.prod = whsqty.prod no-lock no-error.
     if not avail zoh then
       do:
       create zoh.
       assign zoh.prod = whsqty.prod.
       end.
     assign zoh.qtyoh = zoh.qtyoh +
                  (whsqty.qtyoh). 
    end.
    end.
 

  
  if v-monitors then 
    message "after icsw oh " today "@ "  
                     int(truncate (time / 3600,0))  ":"
                     int(truncate ((time modulo 3600) / 60 ,0)).

  end. 
 


zzx = 0.
do v-peckinx = 1 to 9:
  find whses use-index k-zwhse
       where whses.zwhse = v-peckinglist[v-peckinx] no-lock no-error.
  if not avail whses then
    do:
    create whses.
    zzx = zzx + 1.
    assign whses.zcnt = zzx
           whses.zwhse = v-peckinglist[v-peckinx].
    end.       
end.

for each icsd where icsd.cono = g-cono no-lock:
 find whses use-index k-zwhse
       where whses.zwhse = icsd.whse no-lock no-error.
  if not avail whses then
    do:
    create whses.
    zzx = zzx + 1.
    assign whses.zcnt = zzx
           whses.zwhse = icsd.whse.
    end.       
end.




assign xx-begdate = (if today = 06/30/2002 then
                       (date(07,1,year(today))) - 1
                       
                    else   
                       (date(month(today),1,year(today))) - 1)

       xx-enddate = xx-begdate.

do x-inx = 1 to i-turnmo :
  assign xx-enddate = (date(month(xx-enddate),1,year(xx-enddate))) - 1.
end.       
     
if v-monitors then 
  message xx-begdate xx-enddate
   "before icsp " today "@ "  
                   int(truncate (time / 3600,0))  ":"
                   int(truncate ((time modulo 3600) / 60 ,0)).


if p-master <> "r" and p-master <> "p" then
for each icsp where icsp.cono = 1 
  
   and icsp.prodcat ge zzl_begcat
   and icsp.prodcat le zzl_endcat  
   and icsp.prod    ge p-prodbeg    
   and icsp.prod    le p-prodend   
      no-lock:
DO TRANSACTION: 


  zelection_type = "p".
  zelection_char4 = icsp.prodcat.
  zelection_cust = 0.
  run zelectioncheck.
  if zelection_good = false then
    do:
    next.
    end.


/* rep, tech, vendor id, vendor parent */ 
  find catmaster where catmaster.cono = 1 and 
                      catmaster.notestype = "zz" and
                      catmaster.primarykey  = "apsva" and
                      catmaster.secondarykey = 
                      icsp.prodcat no-lock
                       no-error. 
  if not avail catmaster then
    do:
    v-parentcode = "".
    v-vendorid = icsp.prodcat.
    if icsp.prodcat  = "a" then 
      v-technology = "Automation".
    else if icsp.prodcat  = "f" then
      v-technology = "Fabrication". 
    else if icsp.prodcat = "h" then 
      v-technology = "Hydraulic".   
    else if icsp.prodcat  = "p" then 
      v-technology =  "Pneumatic".   
    else if icsp.prodcat = "s" then 
      v-technology =  "Service".     
    else if icsp.prodcat = "f" then 
      v-technology =  "Filtration".  
    else if icsp.prodcat  = "c" then 
      v-technology =  "Connectors".  
    else if icsp.prodcat  = "m" then 
      v-technology =  "Mobile".      
    else if icsp.prodcat = "n" then 
      v-technology =  "None".        
    end.
  
  else
    assign
      v-vendorid   = catmaster.noteln[1]
      v-technology = catmaster.noteln[2]
      v-parentcode = catmaster.noteln[3].
    
  if (substring(v-technology,1,4) < p-techbeg or
    substring(v-technology,1,4) > p-techend) or
    (v-parentcode < p-vpbeg or
     v-parentcode > p-vpend) 
                                then

    do:
    next.
    end.

  zelection_type = "n".
  zelection_vname = v-vendorid.
  zelection_cust = 0.
  run zelectioncheck.
  if zelection_good = false then
    do:
    next.
    end.


 if icsp.statustype = "l" then
   next.
  
 h-buyer = "".
 find obsolete where obsolete.o-prod = icsp.prod no-lock no-error.   

  
 assign xx-toh = 0.
 find zoh where zoh.prod = icsp.prod no-lock no-error.
 if avail zoh then
    xx-toh = zoh.qtyoh.
END.  
  Whses:
  for each whses use-index k-zpecking no-lock:
DO TRANSACTION:    
    if whses.zwhse < zzl_begwhse or
       whses.zwhse > zzl_endwhse then
       do:
       assign h-buyer = "".
       next Whses.
       end.
    find icsd where icsd.cono = g-cono    and
         icsd.whse = whses.zwhse no-lock no-error.
    if not avail icsd then
      next Whses.
   
    
    zelection_type = "w".
    zelection_char4 = icsd.whse.
    zelection_cust = 0.
    run zelectioncheck.
    if zelection_good = false then
      do:
      next Whses.
      end.
    
    
    assign h-buyer = "".
  
  
    
/*  Icswloop: */
    find first icsw where icsw.cono = g-cono
                    and icsw.whse = icsd.whse 
                    and icsw.prod = icsp.prod no-lock no-error.
    if not avail icsw then
      next Whses.
      
      if h-buyer = "" then
        do:
        h-searchlevel = 0.
        {p-zfindbuygne.i
             icsw.whse
             icsw.prod
             h-buyer
             h-vendor
             h-prodline
             h-searchlevel}
        end.
      if h-prodline = ""  then
        assign
          h-prodline = "misc".

      if (h-vendor >= p-vendbeg and h-vendor <= p-vendend)
        and (h-prodline >= p-linebeg and h-prodline <= p-lineend)
        and (h-buyer >= zzl_begbuy and h-buyer <= zzl_endbuy) 
        and (icsd.whse >= zzl_begwhse and icsd.whse <= zzl_endwhse)
        and (icsd.buygroup ge p-bgrpbeg and icsd.buygroup le p-bgrpend)
        and (icsd.region >= p-iregbeg and icsd.region <= p-iregend)   then
        assign h-validparams = true.
      else
        next Whses.

/* if we are using the values from monthend then get them 
   Since the file only holds masters with OH at then time of 
   monthend if it is not in there then OH is zero */
      if x-mevals then
        do:
        find whsqty where whsqty.whse = icsw.whse and
                          whsqty.prod = icsw.prod no-lock no-error.
        if avail whsqty then
          assign whsqtyoh = whsqty.qtyoh
                 whsqtycost = whsqty.acost.
        else
          assign whsqtyoh = 0
                 whsqtycost = icsw.avgcost.                       
        end.
      find sasta where sasta.cono = g-cono
                   and sasta.codeiden = "B"
                   and sasta.codeval = h-buyer
           no-lock no-error.

      assign d-name = if avail sasta then sasta.descrip else "BuyerNotFound".
     
      assign z-usage = 0
             z-avgval = 0.
      
      run zsdiusage2.p 
        (xx-begdate,
         xx-enddate,
         icsw.whse,
         icsw.prod,
         output xx-usage,
         output xx-usageval).

      assign z-usage  = xx-usageval.
      if icsp.kittype = "p" or icsp.kittype = "b" or
         icsw.arptype = "f" or icsw.arptype = "k" then
        z-usage = 0.


      cur-inv = if not x-mevals then
                  ((icsw.qtyonhand + icsw.qtyunavail + icsw.qtyrcvd) 
                    * icsw.avgcost)
                else
                  ((whsqtyoh) 
                   * whsqtycost).

      v-totoh = if not x-mevals then
                  (icsw.qtyonhand + icsw.qtyunavail + icsw.qtyrcvd)
                else
                  whsqtyoh. 
      avg-inv =  
               if not x-mevals then
                  ((icsw.qtyonhand + icsw.qtyunavail + icsw.qtyrcvd) 
                    * icsw.avgcost)
                else
                  ((whsqtyoh) 
                   * whsqtycost).

 

     find oimsp where oimsp.person = h-buyer no-lock no-error.
     if avail oimsp then
       h-person = oimsp.responsible.
     else   
       h-person = "NTFND".
    
     if h-person < p-deptbeg or
        h-person > p-deptend then
       next Whses.                  

     if cur-inv <> 0 or
        z-usage <> 0 or
        v-totoh <> 0 or
        avg-inv <> 0 then
       do: 
       create wuyers.
       assign wuyers.wmaster = true
              wuyers.wwhse        = icsd.whse
              wuyers.wprod        = icsp.prod 
              wuyers.woh          = v-totoh
/*                  (icsw.qtyonhand + icsw.qtyunavail + icsw.qtyrcvd)    */
              wuyers.wavgcost     = (if not x-mevals then
                                       icsw.avgcost 
                                    else
                                       whsqtycost)    
    /*       wuyers.wxbreakreg   =  x-breakreg 
             wuyers.wxbreakpct   =  x-breakpct 
             wuyers.wxbreaklst   =  x-breaklst 
             wuyers.wxcust       =  x-cust
             wuyers.wxshipto     =  x-shipto
             wuyers.wxrange      =  x-range    
    */
              wuyers.wprodcat     = icsp.prodcat
              wuyers.wname        = d-name
              wuyers.wvendor      = h-vendor
              wuyers.wpline       = h-prodline
              wuyers.wgroup       = icsd.buygroup
              wuyers.wwhse        = icsd.whse
              wuyers.wicsdregion  =  icsd.region
              wuyers.wdept     =   h-person
              wuyers.wbuyer       = h-buyer
              wuyers.wcur         = cur-inv
              wuyers.wcgs         = z-usage
              wuyers.wavg         = avg-inv 
              wuyers.wls12        =
                     (if avail obsolete then
                        if xx-toh > 0 then 
                          (((v-totoh) /
                            xx-toh) * obsolete.o-ls12month)
                            * icsw.avgcost
                        else
                          0
                      else
                         0)
              wuyers.w6           =
                     (if avail obsolete then
                        if xx-toh > 0 then 
                          (((v-totoh) /
                            xx-toh) * obsolete.o-6month)
                            * icsw.avgcost
                        else
                          0
                      else
                         0)
              
              wuyers.w12          =
                     (if avail obsolete then
                        if xx-toh > 0 then 
                          (((v-totoh) /
                            xx-toh) * obsolete.o-12month)
                            * icsw.avgcost
                        else
                          0
                      else
                         0)
              
              wuyers.w24          =
                     (if avail obsolete then
                        if xx-toh > 0 then 
                          (((v-totoh) /
                            xx-toh) * obsolete.o-24month)
                            * icsw.avgcost
                        else
                          0
                      else
                         0)
              
              wuyers.w48          =
                     (if avail obsolete then
                        if xx-toh > 0 then 
                          (((v-totoh) /
                            xx-toh) * obsolete.o-48month)
                            * icsw.avgcost
                        else
                          0
                      else
                         0)
              
              wuyers.w60          =
                     (if avail obsolete then
                        if xx-toh > 0 then 
                          (((v-totoh) /
                            xx-toh) * obsolete.o-60month)
                            * icsw.avgcost
                        else
                          0
                      else
                         0)
              wuyers.wg60          =
                     (if avail obsolete then
                        if xx-toh > 0 then 
                          (((v-totoh) /
                            xx-toh) * obsolete.o-g60month)
                            * icsw.avgcost
                        else
                          0
                      else
                         0)
     

              wuyers.wns7        =
                     (if avail obsolete then
                        if xx-toh > 0 then 
                          (((v-totoh) /
                            xx-toh) * obsolete.o-ns7)
                            * icsw.avgcost
                        else
                          0
                      else
                         0)
     
 
              
              wuyers.wns12        =
                     (if avail obsolete then
                        if xx-toh > 0 then 
                          (((v-totoh) /
                            xx-toh) * obsolete.o-ns12)
                            * icsw.avgcost
                        else
                          0
                      else
                         0)
     
              wuyers.wns24        =
                     (if avail obsolete then
                        if xx-toh > 0 then 
                          (((v-totoh) /
                            xx-toh) * obsolete.o-ns24)
                            * icsw.avgcost
                        else
                          0
                      else
                         0).
     
     assign   wuyers.wqls12        =
                    (if avail obsolete then
                       if xx-toh > 0 then 
                         (((v-totoh) /
                           xx-toh) * obsolete.o-ls12month)
                       else
                         0
                     else
                        0)
              wuyers.wq6           =
                    (if avail obsolete then
                       if xx-toh > 0 then 
                         (((v-totoh) /
                           xx-toh) * obsolete.o-6month)
                       else
                         0
                     else
                        0)

              wuyers.wq12          =
                    (if avail obsolete then
                       if xx-toh > 0 then 
                         (((v-totoh) /
                           xx-toh) * obsolete.o-12month)
                       else
                         0
                     else
                        0)

              wuyers.wq24          =
                    (if avail obsolete then
                       if xx-toh > 0 then 
                         (((v-totoh) /
                           xx-toh) * obsolete.o-24month)
                       else
                         0
                     else
                        0)

              wuyers.wq48          =
                    (if avail obsolete then
                       if xx-toh > 0 then 
                         (((v-totoh) /
                           xx-toh) * obsolete.o-48month)
                       else
                         0
                     else
                        0)

              wuyers.wq60          =
                    (if avail obsolete then
                       if xx-toh > 0 then 
                         (((v-totoh) /
                           xx-toh) * obsolete.o-60month)
                       else
                         0
                     else
                        0)
              wuyers.wqg60          =
                    (if avail obsolete then
                       if xx-toh > 0 then 
                         (((v-totoh) /
                           xx-toh) * obsolete.o-g60month)
                       else
                         0
                     else
                        0)


              wuyers.wqns7        =
                    (if avail obsolete then
                       if xx-toh > 0 then 
                         (((v-totoh) /
                           xx-toh) * obsolete.o-ns7)
                       else
                         0
                     else
                        0)


              wuyers.wqns12        =
                    (if avail obsolete then
                       if xx-toh > 0 then 
                         (((v-totoh) /
                           xx-toh) * obsolete.o-ns12)
                       else
                         0
                     else
                        0)

              wuyers.wqns24        =
                    (if avail obsolete then
                       if xx-toh > 0 then 
                         (((v-totoh) /
                           xx-toh) * obsolete.o-ns24)
                       else
                         0
                     else
                        0).                        
       
           run createranges.
           end.
END.                                  /* Transaction */
    end. /* end whses */
 end. /* icsp */
if v-monitors then 
  message "after icsp " today "@ "  
                   int(truncate (time / 3600,0))  ":"
                   int(truncate ((time modulo 3600) / 60 ,0)).

if p-master <> "r" then
  do:
DO TRANSACTION:
  if p-master <> "p" then
    run nonstockaquire.   

  if v-monitors then 
    message "after nonstock " today "@ "  
                    int(truncate (time / 3600,0))  ":"
                    int(truncate ((time modulo 3600) / 60 ,0)).
  
  
  run rolloutpartial. 
 END.  

  if v-monitors then 
    message "after rollout partial " today "@ "  
                    int(truncate (time / 3600,0))  ":"
                    int(truncate ((time modulo 3600) / 60 ,0)).
  
 
  /*  ----------------------- 
  run distribute_value.

  */

 if v-monitors then
    output stream xpcd to value(v-printdir + "wuyersmonit.cxt0").
 
 do for xwuyers, b-xpart, zwuyers:
  
  for each xwuyers use-index wkey2 where
                xwuyers.wmaster = true  
                no-lock: 
                /*    break          by wuyers.wprod:  */


    if v-prodhold <> xwuyers.wprod then
      do:
      export stream xpcd delimiter "," "1" xwuyers.wprod.
      assign v-prodhold = xwuyers.wprod.
      
      find zoh where zoh.prod = xwuyers.wprod no-lock no-error.
      if avail zoh then
        xx-toh = zoh.qtyoh.
 
      run weight_item2  /* in sdi-tools */
         (input-output x-range,
         input       xwuyers.wprod,
         input       xwuyers.wprodcat,
         input       g-cono,
         input       xx-toh).
      export stream xpcd delimiter "," "2" xwuyers.wprod.
      end.
    jj = 0.

      
 
    for each b-xpart  no-lock :
      if b-xpart.wxpct = 0 then
        next.  

      jj = jj + b-xpart.wxpct.  
      create zwuyers.
      assign  zwuyers.wmaster = false
              zwuyers.wregion      = 
                 substring(b-xpart.wxreg,1,1)
              zwuyers.wdistrict    = 
                 substring(b-xpart.wxreg,2,3)
              zwuyers.wcustno      = 
                 b-xpart.wxcust
              zwuyers.wshipto      = 
                 b-xpart.wxshipto
              zwuyers.wlstsale     = 
                 int(substring(b-xpart.wxlst,1,6))
              zwuyers.wlsttrans    = 
                 substring(b-xpart.wxlst,7,15)
              zwuyers.wwhse        = xwuyers.wwhse
              zwuyers.wprod        = xwuyers.wprod 
              zwuyers.wprodcat     = xwuyers.wprodcat
              /*
              zwuyers.wxbreakreg   = xwuyers.wxbreakreg
              zwuyers.wxbreakpct   = xwuyers.wxbreakpct
              zwuyers.wxcust       = xwuyers.wxcust
              zwuyers.wxshipto     = xwuyers.wxshipto
              zwuyers.wxrange      = xwuyers.wxrange
              */                     
              zwuyers.wname        = xwuyers.wname 
              zwuyers.wvendor      = xwuyers.wvendor
              zwuyers.wgroup       = xwuyers.wgroup
              zwuyers.wpline       = xwuyers.wpline
              zwuyers.wwhse        = xwuyers.wwhse 
              zwuyers.wicsdregion  = xwuyers.wicsdregion
              zwuyers.wdept     = xwuyers.wdept
              zwuyers.wbuyer       = xwuyers.wbuyer
              zwuyers.woh          = xwuyers.woh
                            * b-xpart.wxpct
              zwuyers.wavgcost     = xwuyers.wavgcost
              zwuyers.wcur         = xwuyers.wcur
                            * b-xpart.wxpct
              zwuyers.wcgs         = xwuyers.wcgs
                            * b-xpart.wxpct
              zwuyers.wavg         = xwuyers.wavg
                            * b-xpart.wxpct
              zwuyers.wls12        = xwuyers.wls12
                            * b-xpart.wxpct
              zwuyers.w6           = xwuyers.w6
                            * b-xpart.wxpct
              zwuyers.w12          = xwuyers.w12
                            * b-xpart.wxpct
              zwuyers.w24          = xwuyers.w24
                            * b-xpart.wxpct
              zwuyers.w48          = xwuyers.w48
                            * b-xpart.wxpct
              zwuyers.w60          = xwuyers.w60
                            * b-xpart.wxpct
              zwuyers.wg60         = xwuyers.wg60
                            * b-xpart.wxpct
              zwuyers.wns7        = xwuyers.wns7
                            * b-xpart.wxpct
              zwuyers.wns12        = xwuyers.wns12
                            * b-xpart.wxpct
              zwuyers.wns24        = xwuyers.wns24
                            * b-xpart.wxpct
              zwuyers.wncur        = xwuyers.wncur
                            * b-xpart.wxpct
              zwuyers.wncgs        = xwuyers.wncgs
                            * b-xpart.wxpct
              zwuyers.wqls12        = xwuyers.wls12
                            * b-xpart.wxpct
              zwuyers.wq6           = xwuyers.wq6
                            * b-xpart.wxpct
              zwuyers.wq12          = xwuyers.wq12
                            * b-xpart.wxpct
              zwuyers.wq24          = xwuyers.wq24
                            * b-xpart.wxpct
              zwuyers.wq48          = xwuyers.wq48
                            * b-xpart.wxpct
              zwuyers.wq60          = xwuyers.wq60
                            * b-xpart.wxpct
              zwuyers.wqg60         = xwuyers.wqg60
                            * b-xpart.wxpct
              zwuyers.wqns7        = xwuyers.wqns7
                            * b-xpart.wxpct
              zwuyers.wqns12        = xwuyers.wqns12
                            * b-xpart.wxpct
              zwuyers.wqns24        = xwuyers.wqns24
                            * b-xpart.wxpct.
    /*
      run createranges2.
    */
      /*  */
            
      find r1range where
 r1range.r1region       = zwuyers.wregion and
 r1range.r1district     = zwuyers.wdistrict and
 r1range.r1icsdregion   = zwuyers.wicsdregion  and
 r1range.r1whse         = zwuyers.wwhse   and
 r1range.r2group = zwuyers.wgroup and
 r1range.r2buyer = zwuyers.wbuyer and
 r1range.r2dept  = zwuyers.wdept and
 r1range.r2vendor = zwuyers.wvendor and
 r1range.r2pline = zwuyers.wpline and
 r1range.r3prodcat = zwuyers.wprodcat no-lock no-error.


if not avail r1range then
  do:
  create r1range.
  assign  r1range.r1region       = zwuyers.wregion
          r1range.r1district     = zwuyers.wdistrict
          r1range.r1icsdregion   = zwuyers.wicsdregion
          r1range.r1whse         = zwuyers.wwhse 
          r1range.r2group = zwuyers.wgroup
          r1range.r2dept  = zwuyers.wdept 
          r1range.r2buyer = zwuyers.wbuyer
          r1range.r2pline = zwuyers.wpline
          r1range.r3prodcat = zwuyers.wprodcat
          r1range.r2vendor = zwuyers.wvendor.
  end.

/* */
      end.
   end.
END. /* TRANSACTION */

  
  
  if v-monitors then 
    message "after distribute " today "@ "  
                     int(truncate (time / 3600,0))  ":"
                     int(truncate ((time modulo 3600) / 60 ,0)).

end. 


 if v-monitors then
    output stream xpcd close.

 
 if p-master = "r" then
  do:
  
  if v-monitors then 
    message "Before repository in " today "@ "  
                    int(truncate (time / 3600,0))  ":"
                    int(truncate ((time modulo 3600) / 60 ,0)).
  

   if p-pastsuff <> " " and
      search(v-printdir + "wuyers.cxt0" + "." + p-pastsuff) <> ? then
     do: 
     input stream xpcd from
                  value(v-printdir + "wuyers.cxt0" + "." + p-pastsuff).
   
     message "TBZN - Check option 1, diverting repository to " p-pastsuff.
     end.   
   
   else                    
     input stream xpcd from value(v-printdir + "wuyers.cxt0").
  repeat:
    create wuyers.
    import stream xpcd wuyers.
    run createranges.
  
  end.
  input stream xpcd close.
 
  if v-monitors then 
    message "after repository in " today "@ "  
                    int(truncate (time / 3600,0))  ":"
                    int(truncate ((time modulo 3600) / 60 ,0)).
  
  end.



if p-master = "m" or p-master = "p" then
  do:

  output stream xpcd to value(v-printdir + "wuyers.cxt0").
  for each wuyers where /* wuyers.wmaster = false   */
           no-lock:
    export stream xpcd wuyers.
  end.  
  output stream xpcd close.
  end.



assign v-sapbid = v-sapbhold
       v-sassrid = v-sassrhold
       v-saspid = v-sasphold.

find sapb where recid(sapb) = v-sapbid no-lock no-error.
find sassr where recid(sassr) = v-sassrid no-lock no-error.
find sasp where recid (sasp) = v-saspid no-lock no-error.
            

run tbrwc0.p (input no).

if p-stored <> " " then 
  do:
 /*  output close.   */
  for each sapb use-index  k-currproc
     where sapb.currproc = "tbzc" and
           sapb.reportnm begins p-stored no-lock:
   

    find sasp where 
                   sasp.printernm = sasp.printernm
                      no-lock no-error.
      if avail sasp then
        output stream xpcd through value(sasp.pcommand)  paged.
      else        
        output stream xpcd to value("/usr/tmp/" + sapb.printernm) paged.
   
  assign v-sapbid = recid(sapb)
         v-sassrid = v-sassrhold
         v-saspid = recid(sasp).
  run zsdirptopt.p
      (yes,45).
 
       assign 
         p-bgrpbeg = sapb.rangebeg[1]
         p-bgrpend = sapb.rangeend[1]
         p-regbeg = sapb.rangebeg[2]
         p-regend = sapb.rangeend[2]
         p-whsebeg = sapb.rangebeg[3]
         p-whseend = sapb.rangeend[3]
         p-buyrbeg = sapb.rangebeg[4]
         p-buyrend = sapb.rangeend[4]
         p-vendbeg = dec(sapb.rangebeg[5])
         p-vendend = dec(sapb.rangeend[5])
         p-deptbeg = sapb.rangebeg[7]
         p-deptend = sapb.rangeend[7]
         p-linebeg = sapb.rangebeg[6]
         p-lineend = sapb.rangeend[6]
         p-techbeg = sapb.rangebeg[8]
         p-techend = sapb.rangeend[8]
         p-vpbeg = sapb.rangebeg[9]
         p-vpend = sapb.rangeend[9]
         p-iregbeg = sapb.rangebeg[10]
         p-iregend = sapb.rangeend[10]
         p-prodbeg = sapb.rangebeg[11]
         p-prodend = sapb.rangeend[11]
         p-vidbeg  = sapb.rangebeg[12]
         p-vidend  = sapb.rangeend[12]
         p-custbeg = dec(sapb.rangebeg[13])
         p-custend = dec(sapb.rangeend[13])
         p-pcatbeg  = sapb.rangebeg[14]
         p-pcatend  = sapb.rangeend[14]
         p-repbeg  = sapb.rangebeg[15]
         p-repend  = sapb.rangeend[15].

      run formatreference.
      assign       
        i-turnmo = int(jrk_optvalue[1])
        p-optiontype = jrk_optvalue[2]
        p-sorttype    = jrk_optvalue[3]
        p-totaltype   = jrk_optvalue[4]
        p-summcounts = jrk_optvalue[5]
        p-master  =  jrk_optvalue[6]
        p-stored =  jrk_optvalue[7]
        p-list = if jrk_optvalue[8] = "yes" then true else false
        p-export = jrk_optvalue[9]
        p-obsolete = if jrk_optvalue[10] = "yes" then true else false.



      status default "Now Processing Report ..." +  sapb.reportnm.
      run tbrwc0.p (input yes).
      output stream xpcd close.
   
     end.

  assign v-sapbid = v-sapbhold
         v-sassrid = v-sassrhold
         v-saspid = v-sasphold.

  find sapb where recid(sapb) = v-sapbid no-lock no-error.
  find sassr where recid(sassr) = v-sassrid no-lock no-error.
  /*
  if sapb.filefl = true then
    output to value("/usr/tmp/" + sapb.reportnm) append.
  else 
    output to value ("/usr/tmp/" + sapb.reportnm + ".000") append.
  */
  
  end.

/* ====================================================================== */
Procedure nonstockaquire.
/* ====================================================================== */

for each icsd where icsd.cono = g-cono and
         icsd.whse ge zzl_begwhse and
         icsd.whse le zzl_endwhse 
         no-lock:


  zelection_type = "w".
  zelection_char4 = icsd.whse.
  zelection_cust = 0.
  run zelectioncheck.
  if zelection_good = false then
    do:
    next.
    end.
 
icnehloop:
for each icenh use-index k-opendt where 
          icenh.cono = g-cono and
          icenh.typecd = "n" and
          icenh.whse = icsd.whse and 
         (icenh.active = true or
          icenh.opendt ge
            (today - (i-turnmo * 30)))  
  
          and icenh.prodcat ge zzl_begcat
          and icenh.prodcat le zzl_endcat  
          and icenh.prod    ge p-prodbeg    
          and icenh.prod    le p-prodend   
             
/*             and icenh.prodcat begins "f"   
         and icenh.prod = "63477-36"     */   

             no-lock:

  
  zelection_type = "p".
  zelection_char4 = icenh.prodcat.
  zelection_cust = 0.
  run zelectioncheck.
  if zelection_good = false then
    do:
    next icnehloop.
    end.


  
  
  assign h-icslfound = false
         h-validparams = false
         h-lined = false
         n-cgs = 0
         n-val = 0. 
  icenlloop:
  for each icenl where icenl.cono = g-cono and
                       icenl.typecd = icenh.typecd and
                       icenl.whse = icenh.whse and
                       icenl.prod = icenh.prod and
                       icenl.prodcat = icenh.prodcat and 
                       icenl.seqnoh = icenh.seqnoh no-lock:
    
    
    if icenl.ordtype = "o" and not h-icslfound and not h-lined then
      do:
      find oeel where oeel.cono = g-cono and
                       oeel.orderno = icenl.orderno and
                       oeel.ordersuf = icenl.ordersuf and
                       oeel.lineno  = icenl.lineno and
                       oeel.shipprod = icenl.prod 
                       no-lock no-error.
     
            
       if avail oeel then
         do:
         if oeel.kitfl = yes or oeel.ordertype = "f" then
           do:
           n-val = n-val + (icenl.amount * (if icenl.quantity < 0 then 
                                              -1
                                            else
                                              1)).
           
           next icenlloop.
           end.
         find icsl where icsl.cono = oeel.cono
                    and  icsl.whse = oeel.whse
                    and  icsl.vendno = oeel.vendno
                    and  icsl.prodline = oeel.prodline no-lock no-error.
    
        if not avail icsl then
          do:
          find first icsl where icsl.cono = g-cono
            and icsl.whse = oeel.whse
            and icsl.prodline = oeel.prodline no-error.
          if not avail icsl then
            do:
            find first icsl where icsl.cono = g-cono
              and icsl.whse = oeel.whse
              and icsl.vendno = oeel.vendno use-index k-icsl no-error.

            if not avail icsl then
              assign h-lined = false.
            else
              assign 
                h-lined = true
                h-vendor = icsl.vendno
                h-prodline = icsl.prodline
                h-buyer = icsl.buyer.
            end.
          else
            assign 
              h-lined = true
              h-vendor = icsl.vendno
              h-prodline = icsl.prodline
              h-buyer = icsl.buyer.
         end.
      else
        assign 
          h-lined = true
          h-vendor = icsl.vendno
          h-prodline = icsl.prodline
          h-buyer = icsl.buyer.
      end.
    else
      do:
      find first oeelk  where oeelk.cono = g-cono and
                       oeelk.ordertype = "o" and
                       oeelk.orderno = icenl.orderno and
                       oeelk.ordersuf = icenl.ordersuf and
                       oeelk.lineno  = icenl.lineno and
                       oeelk.shipprod  = icenl.prod no-lock no-error.
     
            
       if avail oeelk then
         do:
         if oeelk.orderalttype = "f" then
           do:
           n-val = n-val + (icenl.amount * (if icenl.quantity < 0 then 
                                              -1
                                            else
                                              1)).
          
           next icenlloop.
           end.
         find icsl where icsl.cono = oeelk.cono
                    and  icsl.whse = oeelk.whse
                    and  icsl.vendno = oeelk.arpvendno
                    and  icsl.prodline = oeelk.arpprodline no-lock no-error.
    
        if not avail icsl then
          do:
          find first icsl where icsl.cono = g-cono
            and icsl.whse = oeelk.whse
            and icsl.prodline = oeelk.arpprodline  no-error.
          if not avail icsl then
            do:
            find first icsl where icsl.cono = g-cono
              and icsl.whse = oeelk.whse
              and icsl.vendno = oeelk.arpvendno use-index k-icsl no-error.

            if not avail icsl then
              assign
                h-lined = false.
            else
              assign 
                h-lined = true
                h-vendor = icsl.vendno
                h-prodline = icsl.prodline
                h-buyer = icsl.buyer.
            end.
          else
            assign 
              h-lined = true
              h-vendor = icsl.vendno
              h-prodline = icsl.prodline
              h-buyer = icsl.buyer.
         end.
      else
        assign 
          h-lined = true
          h-vendor = icsl.vendno
          h-prodline = icsl.prodline
          h-buyer = icsl.buyer.
      end.
     end.
    end.
   


    if icenl.ordtype = "f"  and not h-icslfound and not h-lined then
       do:
       find vaesl where vaesl.cono = g-cono and
                        vaesl.shipprod = icenl.prod and
                        vaesl.sctntype = "in" and
                        vaesl.vano = icenl.orderno and
                        vaesl.vasuf = icenl.ordersuf and
                        vaesl.lineno  = icenl.lineno no-lock no-error.
       if avail vaesl then
         do:
         find icsl where icsl.cono = vaesl.cono
                    and  icsl.whse = vaesl.whse
                    and  icsl.vendno = vaesl.arpvendno
                    and  icsl.prodline = vaesl.arpprodline no-lock no-error.
    
        if not avail icsl then
          do:
          find first icsl where icsl.cono = g-cono
            and icsl.whse = vaesl.whse
            and icsl.prodline = vaesl.arpprodline use-index k-prodline no-error.
          if not avail icsl then
            do:
            find first icsl where icsl.cono = g-cono
              and icsl.whse = vaesl.whse
              and icsl.vendno = vaesl.arpvendno use-index k-icsl no-error.

            if avail icsl then
              assign 
                h-lined = true
                h-vendor = icsl.vendno
                h-prodline = icsl.prodline
                h-buyer = icsl.buyer.
           end.
          else
            assign 
              h-lined = true
              h-vendor = icsl.vendno
              h-prodline = icsl.prodline
              h-buyer = icsl.buyer.
         end.
      else
        assign 
          h-lined = true
          h-vendor = icsl.vendno
          h-prodline = icsl.prodline
          h-buyer = icsl.buyer.
      end.
    end.

    if icenl.ordtype = "p"  and not h-icslfound and not h-lined then
       do:
       find poel where poel.cono = g-cono and
                       poel.pono = icenl.orderno and
                       poel.posuf = icenl.ordersuf and
                       poel.lineno  = icenl.lineno no-lock no-error.
       if avail poel then
         do:
         find icsl where icsl.cono = poel.cono
                    and  icsl.whse = poel.whse
                    and  icsl.vendno = poel.vendno
                    and  icsl.prodline = poel.prodline no-lock no-error.
    
        if not avail icsl then
          do:
          find first icsl where icsl.cono = g-cono
            and icsl.whse = poel.whse
            and icsl.prodline = poel.prodline use-index k-prodline no-error.
          if not avail icsl then
            do:
            find first icsl where icsl.cono = g-cono
              and icsl.whse = poel.whse
              and icsl.vendno = poel.vendno use-index k-icsl no-error.

            if not avail icsl then
              assign
                h-vendor = 0
                h-prodline = "Nisc"
                h-buyer = "Nisc".
            else
              assign 
                h-vendor = icsl.vendno
                h-prodline = icsl.prodline
                h-buyer = icsl.buyer.
            end.
          else
            assign 
              h-vendor = icsl.vendno
              h-prodline = icsl.prodline
              h-buyer = icsl.buyer.
         end.
      else
        assign 
          h-vendor = icsl.vendno
          h-prodline = icsl.prodline
          h-buyer = icsl.buyer.
    end.
  end.

   
  
  if (h-vendor >= p-vendbeg and h-vendor <= p-vendend)
    and (h-prodline >= p-linebeg and h-prodline <= p-lineend)
    and (h-buyer >= zzl_begbuy and h-buyer <= zzl_endbuy) then
     assign h-validparams = true.
  if ((icenl.ordtype = "o" and icenl.entrytype = false) or 
      (icenl.ordtype = "f" and icenl.entrytype = false))
                            and icenl.postdt ge (today - (i-turnmo * 30))  
                            and icenl.postdt le today  then
                                              /* orders are opposite entries */
    n-cgs = n-cgs + (icenl.amount * (if icenl.quantity < 0 then 1 else -1)).
 n-val = n-val + (icenl.amount * (if icenl.quantity < 0 then -1 else 1)).
 end. /* icnelloop */ 

if h-validparams then
    do:
    
    find sasta where sasta.cono = g-cono
                 and sasta.codeiden = "B"
                 and sasta.codeval = h-buyer
           no-lock no-error.

    assign d-name = if avail sasta then sasta.descrip else "".
     
    find oimsp where oimsp.person = h-buyer no-lock no-error.
    if avail oimsp then
      h-person = oimsp.responsible.
    else   
      h-person = "NTFND".

    if h-person < p-deptbeg  or
       h-person > p-deptend then
      next.                  
       
    find wuyers where
                    wuyers.wmaster = true  
                and wuyers.wwhse = icsd.whse
                and wuyers.wprod = icenh.prod 
                no-error.
    if not avail wuyers and
       (n-val <> 0 or
        n-cgs <> 0) then
      do:
      create wuyers.
      assign  wuyers.wmaster = true
              wuyers.wwhse        = icsd.whse
              wuyers.wprod        = icenh.prod
    /*        wuyers.wxbreakreg   = x-breakreg
              wuyers.wxbreakpct   = x-breakpct
              wuyers.wxbreakpct   = x-breaklst
              wuyers.wxrange      = x-range
              wuyers.wxcust       = x-cust
              wuyers.wxshipto     = x-shipto
    */
              wuyers.wcustno      = 0
              wuyers.wshipto      = " "
              wuyers.wlsttrans   = " "            
              wuyers.wlstsale    = 0
              wuyers.wprodcat     = icenh.prodcat
              wuyers.wname        = d-name
              wuyers.wvendor      = h-vendor
              wuyers.wpline       = h-prodline
              wuyers.wgroup       = icsd.buygroup
              wuyers.wwhse        = icsd.whse
              wuyers.wicsdregion  =  icsd.region
              wuyers.wdept     =   h-person
              wuyers.wbuyer       = h-buyer
              wuyers.wls12        = 0
              wuyers.w6           = 0
              wuyers.w12          = 0
              wuyers.w24          = 0
              wuyers.w48          = 0
              wuyers.w60          = 0
              wuyers.wg60         = 0
              wuyers.wns7         = 0
              wuyers.wns12        = 0
              wuyers.wns24        = 0
              wuyers.wqls12        = 0
              wuyers.wq6           = 0
              wuyers.wq12          = 0
              wuyers.wq24          = 0
              wuyers.wq48          = 0
              wuyers.wq60          = 0
              wuyers.wqg60         = 0
              wuyers.wqns7         = 0
              wuyers.wqns12        = 0
              wuyers.wqns24        = 0
              wuyers.wncur = n-val
              wuyers.wncgs = n-cgs.
      run createranges.
      end.
    else
    if avail wuyers and
       (n-val <> 0 or
        n-cgs <> 0) then
       assign
         wuyers.wncur = n-val +  wuyers.wncur
         wuyers.wncgs = n-cgs +  wuyers.wncgs.
   end.
  end.
 end.
end.                

/* ------------------- for transaction control moved 
procedure distribute_value:

  for each wuyers use-index wkey2 where
                wuyers.wmaster = true  
                no-lock 
                    break
                          by wuyers.wprod
               transaction:
    if first-of(wuyers.wprod) then
      do:
      find zoh where zoh.prod = wuyers.wprod no-lock no-error.
      if avail zoh then
        xx-toh = zoh.qtyoh.
 
      run weight_item2  /* in sdi-tools */
         (input-output x-range,
         input       wuyers.wprod,
         input       wuyers.wprodcat,
         input       g-cono,
         input       xx-toh).
      end.
    jj = 0.
      
    for each xpart:
      if xpart.wxpct = 0 then
        next.  

      jj = jj + xpart.wxpct.  
      create zwuyers.
      assign  zwuyers.wmaster = false
              zwuyers.wregion      = 
                 substring(xpart.wxreg,1,1)
              zwuyers.wdistrict    = 
                 substring(xpart.wxreg,2,3)
              zwuyers.wcustno      = 
                 xpart.wxcust
              zwuyers.wshipto      = 
                 xpart.wxshipto
              zwuyers.wlstsale     = 
                 int(substring(xpart.wxlst,1,6))
              zwuyers.wlsttrans    = 
                 substring(xpart.wxlst,7,15)
              zwuyers.wwhse        = wuyers.wwhse
              zwuyers.wprod        = wuyers.wprod 
              zwuyers.wprodcat     = wuyers.wprodcat
              /*
              zwuyers.wxbreakreg   = wuyers.wxbreakreg
              zwuyers.wxbreakpct   = wuyers.wxbreakpct
              zwuyers.wxcust       = wuyers.wxcust
              zwuyers.wxshipto     = wuyers.wxshipto
              zwuyers.wxrange      = wuyers.wxrange
              */
              zwuyers.wname        = wuyers.wname 
              zwuyers.wvendor      = wuyers.wvendor
              zwuyers.wgroup       = wuyers.wgroup
              zwuyers.wpline       = wuyers.wpline
              zwuyers.wwhse        = wuyers.wwhse 
              zwuyers.wicsdregion  = wuyers.wicsdregion
              zwuyers.wdept     = wuyers.wdept
              zwuyers.wbuyer       = wuyers.wbuyer
              zwuyers.woh          = wuyers.woh
                            * xpart.wxpct
              zwuyers.wavgcost     = wuyers.wavgcost
              zwuyers.wcur         = wuyers.wcur
                            * xpart.wxpct
              zwuyers.wcgs         = wuyers.wcgs
                            * xpart.wxpct
              zwuyers.wavg         = wuyers.wavg
                            * xpart.wxpct
              zwuyers.wls12        = wuyers.wls12
                            * xpart.wxpct
              zwuyers.w6           = wuyers.w6
                            * xpart.wxpct
              zwuyers.w12          = wuyers.w12
                            * xpart.wxpct
              zwuyers.w24          = wuyers.w24
                            * xpart.wxpct
              zwuyers.w48          = wuyers.w48
                            * xpart.wxpct
              zwuyers.w60          = wuyers.w60
                            * xpart.wxpct
              zwuyers.wg60         = wuyers.wg60
                            * xpart.wxpct
              zwuyers.wns7        = wuyers.wns7
                            * xpart.wxpct
              zwuyers.wns12        = wuyers.wns12
                            * xpart.wxpct
              zwuyers.wns24        = wuyers.wns24
                            * xpart.wxpct
              zwuyers.wncur        = wuyers.wncur
                            * xpart.wxpct
              zwuyers.wncgs        = wuyers.wncgs
                            * xpart.wxpct
              zwuyers.wqls12        = wuyers.wls12
                            * xpart.wxpct
              zwuyers.wq6           = wuyers.wq6
                            * xpart.wxpct
              zwuyers.wq12          = wuyers.wq12
                            * xpart.wxpct
              zwuyers.wq24          = wuyers.wq24
                            * xpart.wxpct
              zwuyers.wq48          = wuyers.wq48
                            * xpart.wxpct
              zwuyers.wq60          = wuyers.wq60
                            * xpart.wxpct
              zwuyers.wqg60         = wuyers.wqg60
                            * xpart.wxpct
              zwuyers.wqns7        = wuyers.wqns7
                            * xpart.wxpct
              zwuyers.wqns12        = wuyers.wqns12
                            * xpart.wxpct
              zwuyers.wqns24        = wuyers.wqns24
                            * xpart.wxpct.

      run createranges2.
      end.
/*  
    if jj < .99 or jj = ? or jj > 1.00019 then
      do:
      jj = 0.
      do x-inx = 1 to wuyers.wxrange:
        jj = jj + dec(entry(x-inx,wuyers.wxbreakpct,"|")).  

        message "rs" jj wuyers.wprod x-inx  wuyers.wxrange
                         entry(x-inx,wuyers.wxbreakpct,"|")
                         entry(x-inx,wuyers.wxbreakreg,"|").
       
      end.
      message jj wuyers.wprod wuyers.wprodcat wuyers.wxbreakpct
                                       wuyers.wxbreakreg.
        
      end.
*/  
   end.
 end.
*/


Procedure formatreference:


assign x-summarybld = "".

if g-currproc <> "tbrw" and 
   g-currproc <> "tbzc" and
   g-currproc <> "tbzw" and
   g-currproc <> "tbzn" then
  do:
  find notes where notes.cono = g-cono  and 
                   notes.notestype = "zz" and
                   notes.primarykey = g-currproc and
                   notes.secondarykey = sapb.optvalue[2] no-lock no-error.
  if not avail notes then
    do:
    message "Invalid Format Type for " g-currproc " --> " sapb.optvalue[2].
    return.
    end.
  else
    do:
    sum_ary = "a,a,a,a,a,a,a,a,a,a".
    
    overlay(sum_ary,1,(length(notes.noteln[4]))) = notes.noteln[4].
     
    do xinx = 1 to 10:
      if entry(xinx,sum_ary,",") = "@" then
        do:
        if xinx > 1 then
          assign x-summarybld = x-summarybld + ",".
        assign x-summarybld = x-summarybld +
               (if int(sapb.optvalue[3]) = 0 then 
                  "A"
                else
                   sapb.optvalue[3]).
        end.        
      else
       do:
       if xinx > 1 then
         assign x-summarybld = x-summarybld + ",".
       assign x-summarybld = x-summarybld  + "A".
       end.
    end.
  do xinx = 2 to 4:
     jrk_optvalue[xinx] = notes.noteln[xinx - 1].
  end.
  jrk_optvalue[1] = sapb.optvalue[1].
  jrk_optvalue[5] = x-summarybld.  
  jrk_optvalue[6] = "r".
  jrk_optvalue[7] = " ".
  jrk_optvalue[13] = " ".
  do xinx = 8 to 12:
    jrk_optvalue[xinx] = sapb.optvalue[xinx - 4].
  end.
  end.
  end.
else
  do:                   
  do xinx = 1 to 13:
   jrk_optvalue[xinx] = sapb.optvalue[xinx].
  end.
  end.

end. 


Procedure createranges:
      
find r1range where
 r1range.r1region       = wuyers.wregion and
 r1range.r1district     = wuyers.wdistrict and
 r1range.r1icsdregion   = wuyers.wicsdregion  and
 r1range.r1whse         = wuyers.wwhse   and
 r1range.r2group = wuyers.wgroup and
 r1range.r2buyer = wuyers.wbuyer and
 r1range.r2dept  = wuyers.wdept and
 r1range.r2vendor = wuyers.wvendor and
 r1range.r2pline = wuyers.wpline and
 r1range.r3prodcat = wuyers.wprodcat no-lock no-error.


if not avail r1range then
  do:
  create r1range.
  assign  r1range.r1region       = wuyers.wregion
          r1range.r1district     = wuyers.wdistrict
          r1range.r1icsdregion   = wuyers.wicsdregion
          r1range.r1whse         = wuyers.wwhse 
          r1range.r2group = wuyers.wgroup
          r1range.r2dept  = wuyers.wdept 
          r1range.r2buyer = wuyers.wbuyer
          r1range.r2pline = wuyers.wpline
          r1range.r3prodcat = wuyers.wprodcat
          r1range.r2vendor = wuyers.wvendor.
  end.
/*
find r4range  where
  r4range.r4vendor = wuyers.wvendor no-lock no-error.
if not avail r4range then
  do:
  create r4range.
  assign r4range.r4vendor = wuyers.wvendor.
  end.
*/

end.

/*
Procedure createranges2:
      
find r1range where
 r1range.r1region       = zwuyers.wregion and
 r1range.r1district     = zwuyers.wdistrict and
 r1range.r1icsdregion   = zwuyers.wicsdregion  and
 r1range.r1whse         = zwuyers.wwhse   and
 r1range.r2group = zwuyers.wgroup and
 r1range.r2buyer = zwuyers.wbuyer and
 r1range.r2dept  = zwuyers.wdept and
 r1range.r2vendor = zwuyers.wvendor and
 r1range.r2pline = zwuyers.wpline and
 r1range.r3prodcat = zwuyers.wprodcat no-lock no-error.


if not avail r1range then
  do:
  create r1range.
  assign  r1range.r1region       = zwuyers.wregion
          r1range.r1district     = zwuyers.wdistrict
          r1range.r1icsdregion   = zwuyers.wicsdregion
          r1range.r1whse         = zwuyers.wwhse 
          r1range.r2group = zwuyers.wgroup
          r1range.r2dept  = zwuyers.wdept 
          r1range.r2buyer = zwuyers.wbuyer
          r1range.r2pline = zwuyers.wpline
          r1range.r3prodcat = zwuyers.wprodcat
          r1range.r2vendor = zwuyers.wvendor.
  end.

end.

*/
procedure rolloutpartial:
 
  if p-master = "p" then
    do:
    if v-monitors then 
      message "Before roll out-in  " today "@ "  
                    int(truncate (time / 3600,0))  ":"
                    int(truncate ((time modulo 3600) / 60 ,0)).
  
   input stream xpcd from value(v-printdir + "wuyersroll.cxt0").
   repeat:
     create wuyers.
     import stream xpcd wuyers.
     run createranges.
  
   end.
   input stream xpcd close.
   if v-monitors then 
     message "After roll out-in " today "@ "  
                    int(truncate (time / 3600,0))  ":"
                    int(truncate ((time modulo 3600) / 60 ,0)).
   end.

  if p-master <> "p" then
    do:

    if v-monitors then 
      message "before roll out" today "@ "  
                    int(truncate (time / 3600,0))  ":"
                    int(truncate ((time modulo 3600) / 60 ,0)).

    output stream xpcd to value(v-printdir + "wuyersroll.cxt0").
    for each wuyers where  wuyers.wmaster = true   
           no-lock:
      export stream xpcd wuyers.
    end.  
    output stream xpcd close.
    end.
 end.

 


{zsapboycheck.i}


{p-sdirank2.i}
