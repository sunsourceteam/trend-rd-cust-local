{w-icsp.i kpsk.comprod no-lock "b-"}
{w-icsw.i kpsk.comprod g-whse no-lock "b-"}

if not avail b-icsp then
  next.


if num-entries(b-icsp.user3,"~011") ge 1 then
 c-tarnbr = entry(1,b-icsp.user3,"~011").
else 
 c-tarnbr = "".
if num-entries(b-icsp.user3,"~011") ge 2 then
 u-tarnbr = entry(2,b-icsp.user3,"~011").
else 
 u-tarnbr = "".

if num-entries(b-icsp.user3,"~011") ge 3 then
 v-countrycd = entry(3,b-icsp.user3,"~011").
else 
 v-countrycd = "".

if num-entries(b-icsp.user3,"~011") ge 4 then
 v-lastupdt = date(entry(4,b-icsp.user3,"~011")).
else 
 v-lastupdt = ?.

if num-entries(b-icsp.user3,"~011") ge 5 then
 v-expdt = date(entry(5,b-icsp.user3,"~011")).
else 
 v-expdt = ?.

if avail b-icsw then do:
  assign v-netamt =  kpsk.qtyneeded  * b-icsw.avgcost.
  assign s-netcost = string(v-netamt,"zzzzzz9.99999")
         s-avgcost = string(b-icsw.avgcost,"zzzzzz9.99999").
end.
else do:
  assign s-netcost = "    No ICSW  "
         s-avgcost = "    N/A".

end.
display  kpsk.seqno     
         kpsk.comprod  
         kpsk.qtyneeded 
         s-avgcost   
         v-countrycd      
         v-expdt        
         v-lastupdt
         b-icsp.descrip[1]
         s-netcost
         u-tarnbr
         c-tarnbr
         vp-ScheduleBInfo     
         vp-ScheduleBMaintDt   
with frame f-icizkd.
           
