/***************************************************************************
 PROGRAM NAME: varxi.p
  PROJECT NO.: 
  DESCRIPTION: This program is similar to the vaexq.p service repair   
               order entry screen. The template orders that create         
               Vasp(value add setup product) and vaspsl(item) records 
               can be viewed here.
  DATE WRITTEN: 05/19/06.
  AUTHOR      : SunSource TA.  
***************************************************************************/

{p-rptbeg.i}   
{g-ic.i}
{g-jump.i}
{g-vastg2.i}  

def var v-stgint   as int                             no-undo.
def var v-found    as logical                         no-undo.
def var v-seqno    like oeel.lineno                   no-undo.
def var v-lineno   like oeel.lineno                   no-undo.
def var v-prod     like oeel.shipprod                 no-undo.
def var v-lmode    as character format "x(1)"         no-undo.
def var v-vano     as   character format "x(10)" dcolor 2 no-undo.
def var v-custpo   like oeeh.custpo                   no-undo.
def var v-stage    as c format "xxx"  dcolor 2        no-undo.
def var v-model    as character format "x(39)"        no-undo.
def var v-serial   as character format "x(22)"        no-undo.
def var s-prod     as character format "x(24)"        no-undo.
def var v-esttdn-h as int format "99"                 no-undo.
def var v-esttdn-m as int format "99"                 no-undo.
def var v-estasy-h as int format "99"                 no-undo.
def var v-estasy-m as int format "99"                 no-undo.
def var v-esttst-h as int format "99"                 no-undo.
def var v-esttst-m as int format "99"                 no-undo.

def var v-quoteno  as char format "x(12)"             no-undo.
def var x-quoteno  as char format "x(12)"             no-undo.
def var v-error    as char format "x(78)"             no-undo.
def var v-takenby  like oeeh.takenby                  no-undo.
def var l-takenby  like oeeh.takenby                  no-undo.
def var l-whse     like icsw.whse                     no-undo.  
def var v-custno   like oeeh.custno                   no-undo.
def var v-shipto   like oeeh.shipto                   no-undo.
def var v-custname like arsc.name                     no-undo.
def var v-errfl    as logical     init "no"           no-undo.
def var s-colon    as c format "x" init ":"           no-undo.
def var s-hours    as i format "99"                   no-undo.
def var s-minutes  as i format "99"                   no-undo.
def var s-lnlabordata   as c format "x(18)"           no-undo.
def var s-functions     as c format "x(78)"  dcolor 2 no-undo.
def var v-idx           as integer                    no-undo.
def var v-idx2          as integer                    no-undo.
def var v-comdisplay    as char format "x(60)"        no-undo. 
def var v-comments      as char format "x(60)" extent 32. 
def var v-comments2     as char format "x(60)" extent 16. 
def var v-comments3     as char format "x(60)" extent 32. 

def var p-prce     as logical     init "yes"          no-undo.
def var p-onlydesc as logical     init "no"           no-undo.
def var p-detail   as logical     init "no"           no-undo.
def var p-costfl   as logical     init "no"           no-undo.
def var p-quotefl  as logical     init "no"           no-undo.
def var copy-okfl  as logical     init "no"           no-undo.
def var p-brwsfl   as logical     init "no"           no-undo.
def var v-tagprt   as logical     init "no"           no-undo.
def var funcflag   as logical     init "no"           no-undo.
def var x-cursorup as i format "999" init 501         no-undo.   
def var h-recid    as recid                           no-undo.   


define temp-table t-lines no-undo
  field lineno      like oeel.lineno
  field seqno       as int format 99
  field specnstype  as char format "xx" 
  field prod        like oeel.shipprod
  field prodline    like icsl.prodline
  field descrip     as character format "x(24)"
  field gdesc       as character format "x(24)"
  field prctype     like oeel.pricetype
  field prodcat     like oeel.prodcat 
  field vendno      as int format ">>>>>>999999" 
  field vendnm      like apsv.name
  field whse        like oeel.whse
  field altwhse     like oeel.whse
  field prodcost    like oeel.prodcost 
  field extcost     like oeel.prodcost 
  field gain        like oeel.prodcost 
  field listprc     like oeel.price
  field discpct     as dec format ">>9.99"
  field sellprc     like oeel.price
  field gp          as dec format ">>>9.99-"
  field qty         like oeel.qtyord
  field shipdt      as date format "99/99/99"
  field pdscrecno   like oeel.pdrecno
  field qtybrkty    as char format "x(1)"
  field kitfl       as logical format "yes/no"
  field unit        like oeel.unit 
  field icspstat    like icsp.statustype
  field vaspslid    as recid 
  field leadtm      like icsw.leadtmavg
  index ix1
    lineno
    seqno.

/* Order Lines Query */

define query  q-lines for t-lines scrolling.
define browse b-lines query q-lines

  display (if t-lines.seqno <> 0 then
             "   "
           else
             string(t-lines.lineno,"99")) +  " " +
                     (if t-lines.specnstype <> "" then
                         t-lines.specnstype
                      else
                         "  ") + " " +
           string(t-lines.whse,"x(3)") + " " + 
           (if t-lines.prod ne "" then 
               string(t-lines.prod,"x(24)") 
           else 
               string(t-lines.descrip,"x(24)")) + " " +
          string(t-lines.qty,">>>>>>.99") + " " +
          (if t-lines.seqno  = 0 then
             string((string (t-lines.prodcost,">>>>>>9.99") + 
                             " "),"x(10)") +  
             string((string (t-lines.extcost,">>>>>>>>>9.99") + 
                             " "),"x(13)") + " "    
           else
              "" ) + " " + 
          string(t-lines.leadtm,">>>9")  format "x(74)"
    with 12 down centered no-hide no-labels 
    title 
"Seq# TypLn# Product                       Qty    AvgCost      ExtCost  Leadtm".
assign s-functions = 
"     F1-Continue Inquiry     F6-Comments     F7-Extend                       ".
form
 b-lines
 s-functions at 2      
with frame f-lines no-labels width 80 overlay row 7 no-box.

/* Frame at Top of page */
form
  "Repair:"     at 1
  v-quoteno     at 9   
  "Va OrderNo:" at 23
  v-vano        at 35
  "Stage:"      at 54
  v-stage       at 61
  "Cust# :"     at 1
  v-custno      at 9 format ">>>>>>999999"  {f-help.i}
  "Cust Po:"    at 22
  v-custpo     at 31                    
  "ShipTo:"    at 54
  v-shipto     at 62      {f-help.i} 
  v-custname   at 9
  "Model: "    at 1 
  v-model      at 8 
  "Serial#:"   at 49
  v-serial     at 58
  "Unit :"     at 1 
  s-prod       at 8 
  {f-help.i}
  "Est/Times"  at 34
  "Tdn:"       at 44
  v-esttdn-h   at 49 
  ":"          at 51 
  v-esttdn-m   at 52
  "Asy:"       at 56
  v-estasy-h   at 61 
  ":"          at 63
  v-estasy-m   at 64
  "Tst:"       at 68
  v-esttst-h   at 73
  ":"          at 75
  v-esttst-m   at 76
  with frame f-top1 no-box no-labels overlay row 1.

form 
   "Please Enter Tag No" at 1 
   skip(1)
   v-quoteno             at 2
   with frame f-tagno no-box no-labels overlay row 3 centered.

form 
   t-lines.prodcost at 1  no-label 
   s-lnlabordata    at 18 no-label
   with frame f-labor row 4 width 39 centered overlay
        title "      Cost         Time            ".    

form 
   t-lines.altwhse  at 2  no-label 
   t-lines.prodcost at 7  no-label 
   t-lines.listprc  at 22 no-label 
   "ArpVendor: "    at 2 
   t-lines.vendno   at 13 no-label format ">>>>>>999999"
   t-lines.vendnm   at 2  no-label 
   with frame f-intype row 4 width 39 centered overlay no-labels 
        title " Src/Whse     Cost       ListPrc       ".    

form
  "Customer and Internal Comments"  at 16 dcolor 2  
  v-comments   at 1 no-labels dcolor 3
  with 25 down frame f6-com no-labels no-box overlay row 6 centered.

on any-key of b-lines
do:
   on cursor-up cursor-up. /* back-tab.*/
   on cursor-down  cursor-down. /* tab */.
   if lastkey = 501 or lastkey = 502 or lastkey = 503 or lastkey = 504 then 
   do:  
      hide frame f-labor. 
      hide frame f6-com.
   end. 
   if {k-cancel.i} or {k-jump.i} then 
   do: 
      if frame-name = "f-lines" then 
      do:  
         assign v-found = no
                v-quoteno = g-whse + "-".  
      end.
      hide frame f-labor. 
      hide frame f6-com.
      next.
   end. 
   else 
   if {k-func6.i} then 
   do:
      hide frame f-lines. 
      run cust-comments. 
      hide frame f6-com.
      display b-lines s-functions with frame f-lines.
      leave.
   end. 
   else 
   if {k-func7.i} then 
   do: 
      if t-lines.specnstype = "it" then 
      do: 
         run itlabor-item.
         hide frame f-labor.
      end. 
      else 
      if t-lines.specnstype = "in" then 
      do: 
         run intype-item.
         hide frame f-intype.
      end. 
      display b-lines s-functions with frame f-lines.
      leave.
   end. 
   else 
   if lastkey = 401 or lastkey = 13 then 
   do: 
      hide frame f-labor. 
      hide frame f6-com.
      assign v-quoteno = g-whse + "-" 
             v-found   = no.
      hide frame f-lines.
      apply "window-close" to current-window.
      next. 
   end. 

end.

assign v-found = no 
       v-quoteno = g-whse + "-". 

main: 
do while true on endkey undo main, leave main:
 display with frame f-top1.
 update v-quoteno when v-found = no 
        go-on(f4,f11)
        with frame f-top1.
  apply lastkey. 

  if lastkey = 404 then 
  do: 
     message "jumping here1". pause 2.
     assign v-found = no. 
     hide frame f-labor. 
     hide frame f6-com.
     leave main.
  end. 
   
  if v-quoteno ne ""  and 
      (lastkey = 13 or keyfunction(lastkey) = "go") then 
  do: 
   run makequoteno(input v-quoteno,output x-quoteno).
   if v-quoteno ne "" and g-whse ne entry(1,v-quoteno,"-") then 
      assign g-whse = if entry(2,v-quoteno,"-") ne "" then 
                         entry(1,v-quoteno,"-")
                      else 
                         "".
        find first vasp use-index k-vasp where 
                   vasp.cono     = g-cono    and 
                   vasp.shipprod = x-quoteno and  
                   vasp.whse     = g-whse 
                   no-lock no-error.
        if avail vasp then 
        do:
           assign v-custno   = dec(string(vasp.user5,"999999999999"))
                  v-vano     = vasp.user2
                  s-prod     = vasp.refer 
                  v-model    = vasp.user3
                  v-serial   = vasp.user1 
                  v-custpo   = substring(vasp.user4,1,22).
           find first vaspsl where 
                      vaspsl.cono = g-cono and 
                      vaspsl.shipprod = vasp.shipprod and 
                      vaspsl.compprod = "teardown" 
                      no-lock no-error. 
           if avail vaspsl then 
           do:  
               assign v-esttdn-h =  truncate(vaspsl.timeelapsed / 3600,0)
                      v-esttdn-m =  (vaspsl.timeelapsed mod 3600) / 60.
           end.       
           find first vaspsl where 
                      vaspsl.cono = g-cono and 
                      vaspsl.shipprod = vasp.shipprod and 
                      vaspsl.compprod = "service repair" 
                      no-lock no-error. 
           if avail vaspsl then 
           do:  
               assign v-estasy-h =  truncate(vaspsl.timeelapsed / 3600,0)
                      v-estasy-m =  (vaspsl.timeelapsed mod 3600) / 60.
           end. 
           find first vaspsl where 
                      vaspsl.cono = g-cono and 
                      vaspsl.shipprod = vasp.shipprod and 
                      vaspsl.compprod = "test time" 
                      no-lock no-error. 
           if avail vaspsl then 
           do:  
               assign v-esttst-h =  truncate(vaspsl.timeelapsed / 3600,0)
                      v-esttst-m =  (vaspsl.timeelapsed mod 3600) / 60.
           end.      
           {w-arsc.i v-custno no-lock}
           if avail arsc then assign v-custname = arsc.name.
           display v-custno v-model s-prod  v-custname
                            v-custpo v-custpo
                            v-esttdn-h   
                            v-estasy-h
                            v-esttst-h
                            v-esttdn-m   
                            v-estasy-m
                            v-esttst-m
                   with frame f-top1.
        end. 
        else 
        do: 
           message " Repair quote notfound - please re-enter repair number ". 
           next.
        end. 
    end. 
 
    if v-quoteno ne ""  and 
      (lastkey = 13 or keyfunction(lastkey) = "go") then 
    do: 
        find first vasp use-index k-vasp where 
                   vasp.cono     = g-cono    and 
                   vasp.shipprod = x-quoteno and  
                   vasp.whse     = g-whse 
                   no-lock no-error.
        if avail vasp then 
        do: 
          assign v-lmode  = "c"
                 v-vano   = vasp.user2 
                 v-stage  = if vasp.user7 = 0 then 
                               v-stage2[1]
                            else    
                               v-stage2[int(vasp.user7)]
                 v-found  = yes
                 v-model  = if v-model = "" then
                               vasp.user3
                            else 
                               v-model 
                 v-serial = if v-serial = "" then 
                               vasp.user1 
                            else 
                               v-serial 
                 v-custpo = if v-custpo = "" then 
                               substring(vasp.user4,1,22)
                            else 
                               v-custpo
                 v-esttdn-h = if v-esttdn-h <= 0 then 
                                 dec(substring(vasp.user4,50,2))
                              else 
                                 v-esttdn-h
                 v-esttdn-m = if v-esttdn-m <= 0 then 
                                dec(substring(vasp.user4,52,2))
                              else 
                                 v-esttdn-m
                 v-estasy-h = if v-estasy-h <= 0 then 
                                 dec(substring(vasp.user4,54,2))
                              else 
                                 v-estasy-h
                 v-estasy-m = if v-estasy-m <= 0 then 
                                 dec(substring(vasp.user4,56,2))
                              else 
                                 v-estasy-m
                 v-esttst-h = if v-esttst-h <= 0 then 
                                 dec(substring(vasp.user4,58,2))
                              else 
                                 v-esttst-h
                 v-esttst-m = if v-esttst-m <= 0 then 
                                 dec(substring(vasp.user4,60,2))
                              else 
                                 v-esttst-m
                 v-custno = if v-custno = 0 then 
                               dec(string(vasp.user5,">>>>>>9999999"))
                            else 
                               v-custno. 
          {w-arsc.i v-custno no-lock}
          if avail arsc then assign v-custname = arsc.name.
          display v-custno v-custpo v-vano v-stage  v-custname
                  s-prod   v-model  v-serial v-quoteno  
                  v-esttdn-h
                  v-estasy-h
                  v-esttst-h
                  v-esttdn-m   
                  v-estasy-m
                  v-esttst-m
                  with frame f-top1.
        end. 
    end. 

    if v-quoteno ne ""  and 
      (lastkey = 13 or keyfunction(lastkey) = "go") then 
    do: 
       for each t-lines exclusive-lock: 
           delete t-lines. 
       end.   
       assign v-lmode    = "c".
       run makequoteno(input v-quoteno,output x-quoteno).
       for each vasps where
                vasps.cono     = g-cono    and 
                vasps.shipprod = x-quoteno  
                no-lock,
           each vaspsl use-index k-vaspsl where
                vaspsl.cono     = g-cono    and 
                vaspsl.shipprod = x-quoteno and
                vaspsl.seqno    = vasps.seqno  
                no-lock:
           if substring(vaspsl.user4,1,4) ne "" then 
              assign l-whse = substring(vaspsl.user4,1,4).
           else 
              assign l-whse = vaspsl.whse.

           {w-icsw.i vaspsl.compprod l-whse no-lock}
          
           if avail icsw or vaspsl.arpvendno > 0 then 
              find first apsv where
                         apsv.cono   = g-cono and 
                         apsv.vendno = vaspsl.arpvendno
                         no-lock no-error. 
           {w-icsp.i vaspsl.compprod no-lock}
           create t-lines. 
           assign t-lines.leadtm     = if avail icsw then 
                                          icsw.leadtmavg
                                       else 
                                          int(substring(vaspsl.user5,1,4))
                  t-lines.vendno     = vaspsl.arpvendno 
                  t-lines.vendnm     = if avail apsv then 
                                          apsv.name 
                                       else 
                                          ""
                  t-lines.gain       = vaspsl.xxde1
                  t-lines.vaspslid   = recid(vaspsl)
                  t-lines.prod       = vaspsl.compprod
                  t-lines.prodline   = vaspsl.arpprodline
                  t-lines.prodcat    = vaspsl.prodcat
                  t-lines.prctype    = if vaspsl.nonstockty ne "n" 
                                          and avail icsw  then 
                                          icsw.pricetype
                                       else 
                                          ""
                  t-lines.lineno     = vasps.seqno 
                  t-lines.descrip    = vaspsl.proddesc
                  t-lines.gdesc      = vaspsl.proddesc2  
                  t-lines.prodcost   = vaspsl.prodcost 
                  t-lines.qty        = vaspsl.qtyneeded
                  t-lines.extcost    = t-lines.prodcost * t-lines.qty
                  t-lines.listprc    = if avail icsw then 
                                          icsw.listprice
                                       else 
                                          0
                  t-lines.unit       = vaspsl.unit
                  t-lines.shipdt     = vaspsl.user8
                  t-lines.icspstat   = if avail icsp then 
                                          icsp.statustype 
                                       else 
                                           ""
                  t-lines.specnstype  = vasps.sctntype
                  t-lines.altwhse     = if substring(vaspsl.user4,1,4) ne "" 
                                           then 
                                           substring(vaspsl.user4,1,4)
                                        else 
                                           g-whse 
                  t-lines.whse        = string(vaspsl.lineno,"zz9").
       end. 
    end.  
 
    open query q-lines for each t-lines.
    b-lines:refreshable = false.
    display b-lines s-functions with frame f-lines.
    b-lines:refreshable = true.
    enable b-lines with frame f-lines.
    apply "entry" to b-lines in frame f-lines.
    wait-for "window-close" of current-window. 
    on cursor-up back-tab.
    on cursor-down tab.
    close query q-lines.

    if lastkey = 401 or lastkey = 13 or lastkey = 404 then 
    do: 
       assign v-quoteno = g-whse + "-" 
              v-found   = no. 
       hide frame f-lines.
       next.
    end. 
    if {k-cancel.i} then 
    do: 
       message "cancelled here2". pause 2.
       assign v-found = no 
              v-quoteno = g-whse + "-". 
       leave.
    end. 

end. /* end main */


procedure cust-comments. 
do: 
   assign v-idx2 = 1.
   assign v-comments[v-idx2] = "----------- Global Customer Notes -----------"
                     v-idx2 = v-idx2 + 1.
   for each notes
      where notes.cono         = g-cono     and 
            notes.notestype    = "fa"       and 
            notes.primarykey   = x-quoteno  and 
            notes.secondarykey = g-whse     and 
            notes.requirefl    = yes        and 
            notes.pageno       = 2
            no-lock:
     if avail notes then 
     do:  
        do v-idx = 1 to 16: 
           if notes.noteln[v-idx] ne "" then 
           do: 
              assign v-comments[v-idx2] = notes.noteln[v-idx].
                     v-idx2 = v-idx2 + 1.
           end. 
        end. 
     end.
   end. 
   assign v-comments[v-idx2] = "--------------- Customer Notes --------------"
                     v-idx2 = v-idx2 + 1.
   for each notes
      where notes.cono         = g-cono     and 
            notes.notestype    = "fa"       and 
            notes.primarykey   = x-quoteno  and 
            notes.secondarykey = g-whse     and 
            notes.requirefl    = no         and 
            notes.pageno       = 1
            no-lock:
     if avail notes then 
     do:  
        do v-idx = 1 to 16: 
           if notes.noteln[v-idx] ne "" then 
           do: 
              assign v-comments[v-idx2] = notes.noteln[v-idx]
                     v-idx2 = v-idx2 + 1.
           end. 
        end. 
     end.
   end. 
   assign v-comments[v-idx2] = "--------------- Internal Notes --------------"
                     v-idx2 = v-idx2 + 1.
   for each notes
      where notes.cono         = g-cono     and 
            notes.notestype    = "in"       and 
            notes.primarykey   = x-quoteno  and 
            notes.secondarykey = g-whse      
            no-lock:
     if avail notes then 
     do:  
        do v-idx = 1 to 16: 
           if notes.noteln[v-idx] ne "" then 
           do: 
              assign v-comments[v-idx2] = notes.noteln[v-idx]
                     v-idx2 = v-idx2 + 1.
           end. 
        end. 
     end.
   end. 

   display v-comments with frame f6-com.
   pause.
   assign v-comments = "". 
end. 
end. 

procedure intype-item:
do: 
  if t-lines.specnstype ne "in" then 
     leave.
  def buffer it-vaspsl for vaspsl.
  find it-vaspsl where recid(it-vaspsl) = t-lines.vaspslid
       no-lock no-error.
  display t-lines.altwhse t-lines.prodcost 
          t-lines.listprc t-lines.vendno format ">>>>>>999999" 
          t-lines.vendnm  with frame f-intype.
  release it-vaspsl. 
  pause.
end.
end.

procedure itlabor-item:
do: 
  if t-lines.specnstype ne "it" then 
     leave.
  def buffer it-vaspsl for vaspsl.
  find it-vaspsl where recid(it-vaspsl) = t-lines.vaspslid
       no-lock no-error.

  assign s-hours = truncate(it-vaspsl.timeelapsed / 3600,0)
         s-minutes = (it-vaspsl.timeelapsed mod 3600) / 60
         s-lnlabordata = " Time: " + string(s-hours,"99") + ":" +
                                     string(s-minutes,"99") + " (Act)".
  display t-lines.prodcost s-lnlabordata with frame f-labor.
  release it-vaspsl. 
  pause.
end.
end.


/*-----------------------------------------------------------------------*/
procedure MakeQuoteNo:
/*-----------------------------------------------------------------------*/
define input  parameter ip-quoteno   as character no-undo.
define output parameter ip-DBquoteno as character no-undo.
define var z as char format "x(07)"               no-undo. 
define var y as int                               no-undo.
assign v-errfl = no.

if num-entries(ip-quoteno,"-") <> 2 then
  do:
  assign ip-DBquoteno = ip-quoteno
              v-errfl = yes
              v-error = 
              "Invalid Repair No. --> " + ip-quoteno +   
              " - Please enter format 'Whse-7
              '".
  return.
  end.

 assign z = trim(entry(2,ip-quoteno,"-"),"0123456789").
 assign y = length(z).

 if y ne 0 then 
  do:
  assign ip-DBquoteno = ip-quoteno
              v-errfl = yes
              v-error = 
              "Invalid Repair No. --> " + ip-quoteno +   
              " - Please enter format 'Whse-7'".
  return.
  end.

  assign ip-DBquoteno  = entry (1,ip-quoteno,"-") + "-" +
                        string(int(entry(2,ip-quoteno,"-")),"9999999").
                                                        
end.       

