/***************************************************************************
 PROGRAM NAME: pdis.p
  PROJECT NO.: 
  DESCRIPTION: This program gives the users the capability to maintain their   
               PDZR SPA discount pricing records. Rebates are created
               in the PDZR trend screen and a worksheet is available in PDZP.
  DATE WRITTEN: 02/18/03.
  AUTHOR      : SunSource.  

  03/27/08    das - Replaced hard coded cono 501 with "g-cono + 500"
  03/19/10    tah - Modified f8 function pertaining to Marked "*" records.
***************************************************************************/

{p-rptbeg.i}
{g-ar.i}
{g-ap.i}
{va.gva}
{icsl.gva}

/*d Shared variables defined as new in pdsr.p and pdsc.p. */
{pdzr.lva  new}

def new shared var s-startdt like pdsc.startdt no-undo.
def var os-cmd-string as char format "x(80)" init ""  no-undo.           
def var s-printernm like sapb.printernm               no-undo.
def var vic-rec     as char format "x(252)" init ""   no-undo.
def var v-filename  as char format "x(40)" init ""    no-undo.
def var v-filename2 as char format "x(60)" init ""    no-undo.
def var s-filename2 as char format "x(60)" init ""    no-undo.
def var browse-hdl   as handle                        no-undo.
def var brws-col-hdl as handle                        no-undo.
def var mspaces     as char format "x(78)" init " "   no-undo.
def var p-name      as char format "x(14)" init ""    no-undo.
def var x-function  as char format "x(78)" init "" dcolor 2 no-undo.
def var x-buyer     as char format "x(04)" init ""    no-undo.
def var x-refer     as char format "x(14)"            no-undo.
def var x-vendname  as char format "x(14)"            no-undo.
def var h-asterisk  as char format "x"                no-undo. 
def var h-custno    as dec  format "zzzzzzzzzzz9"     no-undo.
def var h-vendno    as dec  format "zzzzzzzzzzz9"     no-undo.
def var v-ExpBegDt  as date                           no-undo.
def var v-ExpEndDt  as date                           no-undo.
def var s-ExpBegDt  as date                           no-undo.
def var s-ExpEndDt  as date                           no-undo.
def var v-active    as char format "x(01)" init ""    no-undo.
def var i-custno    as dec  format ">>>>>>>>>>>9"     no-undo.
def var i-vendno    as dec  format ">>>>>>>>>>>9"     no-undo.
def var i-enddt     as date                           no-undo.
def var i-fenddt    as date                           no-undo.
def var i-tenddt    as date                           no-undo.
def var i-fcustno   as dec  format ">>>>>>>>>>>9"     no-undo.
def var i-fvendno   as dec  format ">>>>>>>>>>>9"     no-undo.
def var i-begamt    as dec  format "99999.99"         no-undo.
def var i-chgamt    as dec  format "99999.99"         no-undo.
def var h-rebamt    like pdsr.rebateamt               no-undo.
def var g-prod      as char format "x(24)"            no-undo. 
def var v-name      as char format "x(24)" init ""    no-undo.
def var b-name      as char format "x(24)" init ""    no-undo.
def var c-name      as char format "x(24)" init ""    no-undo.
def var buyernm     as char format "x(30)" init ""    no-undo.
def var fhit        as logical init no                no-undo.
def var chghit      as logical init no                no-undo.
def var endsw       as logical init no                no-undo.
def var scrn_lns    as integer                        no-undo.
def var q-recid     as recid                          no-undo.
def var v-status    as logical init no                no-undo.
def var h-rebrecnoh as integer                        no-undo.
def var x           as integer                        no-undo.
def var brwse-nbr   as integer                        no-undo.
def var h-rowid     as rowid                          no-undo.
def var p-rowid     as int                            no-undo.
def var dd          as character                      no-undo.
def var len         as integer                        no-undo.
def var xinx        as integer                        no-undo.
def var lbegin      as logical                        no-undo.
def var cnt         as integer                        no-undo.
def var lncnt       as integer                        no-undo.
def var zp-cnt      as integer                        no-undo.
def var fld1        as char                           no-undo.
def var fld2        as char                           no-undo.
def var fld3        as char                           no-undo.
def var fld4        as char                           no-undo.
def var fld5        as char                           no-undo.
def var fld6        as char                           no-undo.
def var fld7        as char                           no-undo.
def var fld8        as char                           no-undo.
def var fld9        as char                           no-undo.
def var fld10       as char                           no-undo.

def stream ex1. 

define temp-table bpdsr no-undo 
    field rebrecno   as dec format ">>>>>>9"
    field custno     as dec format "zzzzzzz99999"
    field name       as c   format "x(24)"
    field vendno     as dec format "zzzzzzz99999"
    field levelkey   as c   format "x(24)"
    field contractno as c   format "x(30)"
    field rebateamt  as dec format ">>>>9.99"
    field rebcalcty  as c   format "x"
    field dropshipty like   pdsr.dropshipty
    field user1      as c   format "x(4)"
    field refer      as c   format "x(24)"
    field startdt    as date 
    field enddt      as date format "99/99/99"
    field enddt2     as c   format "x(6)" 
    field brecid     as int
    field marked     as c format "x" 
    index k-idx1
          custno
          vendno
          levelkey
          user1
          rebrecno 
    index k-custno
          custno
          vendno
          levelkey
    index k-marked
          custno
          vendno
          levelkey
          marked 
    index k-rebrecno
          rebrecno 
    index k-enddt
          enddt
          vendno
          custno.

def buffer b-bpdsr for bpdsr.
def buffer x-bpdsr for bpdsr.

define query  q-pdsr for bpdsr scrolling.
define browse b-pdsr query q-pdsr
   display (string(bpdsr.custno,">>>>>>>99999") + " " + 
            string(bpdsr.vendno, ">>>>>>999999") + " " +    
            string(bpdsr.levelkey,"x(24)") + " " + 
            string(bpdsr.rebateamt,">>>>9.99") + " " + 
            string(bpdsr.rebcalcty,"x") + " " + 
            string(bpdsr.user1,"xxxx")  + " " +
            (if bpdsr.enddt = ? then 
              "?"  
             else  
              string(bpdsr.enddt,"99/99/99")) ) format "x(75)"  
            h-asterisk
   with 14 down overlay no-hide no-labels no-box. 

form 
   "       Buyer : " at 20 
   g-buyer           at 35 no-label 
   {f-help.i}
   buyernm           at 40 no-label 
   "  Customer No: " at 20 
   g-custno          at 35 no-label 
   {f-help.i}  
     c-name          at 48 no-label
   "    Vendor No: " at 20 
   g-vendno          at 35 no-label  
   {f-help.i}  
     v-name          at 48 no-label
   "      Product: " at 20 
   g-prod            at 35 no-label 
   {f-help.i}  
   "  ExpireBegDt: " at 20
   s-ExpBegDt        at 35 no-label 
   " ExpireEndDt: "  at 43
   s-ExpEndDt        at 57 no-label
   "Active? "        at 67
   v-active          at 75 no-label
   help " Blank, (A)ctive , (I)nactive or (B)oth "
"     Cust No       Vendor   Product                 Amount TypBuyerExpireDt "
   at 3 dcolor 2
   b-pdsr at  row 7 col 1 
   x-function at 2 no-label
 with frame f-pdsr width 81 row 1 col 1 no-hide overlay no-box. 

form 
    i-fcustno       colon 16 label "   Customer  No"         
    i-fvendno       colon 16 label "     Vendor  No"         
    i-fenddt        colon 16 label " From Expire Dt"
    i-tenddt        colon 16 label "   To Expire Dt"
    with frame f-pdsrdt width 32 row 8 col 25
    no-hide overlay title "Change Expire Dates"  side-labels. 

form 
    "Are you sure of marked amt changes? "
    "Only begin amt equivalents changed. "
    i-fcustno       colon 16 label "   Customer  No"         
    i-fvendno       colon 16 label "     Vendor  No"         
    i-begamt        colon 16 label "   Begin Amount"
    i-chgamt        colon 16 label "  Change Amount"
    with frame f-multchgs width 38 row 8 col 22
    no-hide overlay title "Change Marked Amounts "  side-labels. 

form 
    pdsr.rebrecno   colon 12 label "Record No  "       
    pdsr.vendno     colon 12 label "Vendor     "
    b-name          colon 12 label "Name       "
    pdsr.levelkey   colon 12 label "Item No    "
    pdsr.rebcalcty  colon 12 label "Calc Type  "
    pdsr.dropshipty colon 26 label "ShipType "
    pdsr.rebateamt  colon 12 label "Amount     "  format "99999.99" 
    pdsr.enddt      colon 12 label "Expire Date"
    pdsr.startdt    colon 33 label "Start Date"
    pdsr.contractno colon 12 label "Contract No"  format "x(30)" 
    pdsr.refer      colon 12 label "Refer No   "
    x-buyer         colon 12 label "Buyer      "
    {f-help.i}
    with frame f-pdsrchg width 46 row 7 col 18
    no-hide overlay title "Change PDSR Record"  side-labels. 

form header
   "Date: " today "Time: " string(time,"HH:MM:SS")
   "PDZR - SPA Costing Report" at 75
   "Operator: " at 143
   g-operinits  at 153
   "Page " at 162 page-number format ">>9" 
    skip(1)
    "PD #"             at 1
    "Lvl"              at 7
    "Cust No."         at 13
    "Cust Name"        at 24
    "Vendor No."       at 42
    "Vendor Name"      at 54 
    "Contract No."     at 73
    "Product"          at 94
    "Amount"           at 121
    "Typ"              at 130
    "Shp"              at 134
    "Buyer"            at 138
    "Reference"        at 144
    "Start Dt"         at 160
    "Expr Dt"          at 170
with frame f-hd width 180 page-top.

form
    pdsr.rebrecno     at 1 format ">>>>>>>"
    pdsr.levelcd      at 9  format ">"
    pdsr.custno       at 11
    p-name            at 24
    pdsr.vendno       at 40
    x-vendname        at 53
    pdsr.contractno   at 69 format "x(24)" 
    pdsr.levelkey     at 94 
    pdsr.rebateamt    at 119
    pdsr.rebcalcty    at 132 
    pdsr.dropshipty   at 135
    x-buyer           at 138
    x-refer           at 144 
    pdsr.startdt      at 160
    pdsr.enddt        at 170
with frame f-pd  width 200 no-box no-labels.

form 
    s-printernm colon 3        
    {f-help.i}
    with frame f-printer width 20 row 09 col 30
    no-hide overlay title " Enter Printer Name "  no-labels. 

on row-display, value-changed of b-pdsr in frame f-pdsr do:  
 if avail bpdsr then 
  find first pdsr use-index k-rebrecno 
       where pdsr.cono     = g-cono + 500     and
             pdsr.rebrecno = bpdsr.rebrecno  
  no-lock no-error. 
 if avail pdsr and avail bpdsr and 
  ((pdsr.rebateamt ne bpdsr.rebateamt) or 
   (pdsr.enddt ne bpdsr.enddt)) then do:  
  find b-bpdsr where b-bpdsr.rebrecno = bpdsr.rebrecno
       exclusive-lock no-error.
  assign b-bpdsr.rebateamt = pdsr.rebateamt
         b-bpdsr.enddt     = pdsr.enddt. 
 end. 
 if bpdsr.marked = "*" then 
  assign h-asterisk = "*"
         h-asterisk:dcolor in browse b-pdsr = 2.
 else         
  assign h-asterisk = " ". 
end.

on any-key of b-pdsr do:
  do transaction:
   status default
    "  F6, F7, F8, F9 - Apply 'm' to mark/unmark group amt change."
       in window current-window.
   if g-secure <= 2 then do: 
    assign x-function =       
"       F6-Print            F7-Tied-PDZR           F8-Display-PDZR-Info      ".    display x-function with frame f-pdsr.
   end.
   else 
   if g-secure > 2 then do: 
    assign x-function = 
"    F6-Print         F7-Tied-PDZR         F8-Change         F9-Chg-ExpDts   ". 
    display x-function with frame f-pdsr.
   end.
   if not {k-cancel.i} or {k-jump.i} then 
    display g-buyer g-custno g-vendno g-prod c-name v-name buyernm
            x-function with frame f-pdsr.
   if {k-cancel.i} then do: 
    apply "entry" to g-custno.
    run "clear-browse".
   end.
   else 
   if {k-jump.i} then do:  
    run "clear-browse".
   end.
   else
   if keylabel(lastkey) = "m" and g-secure > 2 and  
     frame-name = "f-pdsr" and avail bpdsr then do: 
    if avail bpdsr then do: 
     if bpdsr.marked = "*" then 
      assign bpdsr.marked = " ".
     else 
      assign bpdsr.marked = "*".
     b-pdsr:refresh().  
    end. 
   end.
   else
   if {k-func6.i} and avail bpdsr then do: 
    {w-sasoo.i g-operinits no-lock}
    if avail sasoo then do: 
     assign s-printernm = sasoo.printernm.
     release sasoo.
    end.   
    update s-printernm
           with frame f-printer. 
    if s-printernm = "vid" then do: 
     s-printernm = " ". 
     message "VID - not a valid print option". 
     pause.
    end.
    else
    if s-printernm ne "  " then 
     run "prt-routine".
   end.
   else 
   if {k-func7.i} and avail bpdsr then do:  
    assign v-rebrecnoh = bpdsr.rebrecno
           h-rebrecnoh = bpdsr.rebrecno
           h-custno    = g-custno
           h-vendno    = g-vendno.
    status input off.
    status default " " in window current-window.
    run "pdzrd.p".
    assign g-custno = h-custno
           g-vendno = h-vendno.
    status input off.
    status default
    "  F6, F7, F8, F9 - Apply 'm' to mark/unmark group amt change."
       in window current-window.
    find bpdsr where
         bpdsr.rebrecno = h-rebrecnoh
         exclusive-lock no-error.
    find pdsr use-index k-rebrecno where
         pdsr.cono = g-cono + 500 and 
         pdsr.rebrecno = h-rebrecnoh
         no-lock no-error.
    if avail bpdsr and avail pdsr then do: 
     self:select-focused-row(). 
     assign bpdsr.rebateamt = pdsr.rebateamt 
            bpdsr.rebcalcty = pdsr.rebcalcty
            bpdsr.user1     = substring(pdsr.user1,1,4)
            bpdsr.enddt     = pdsr.enddt.
      b-pdsr:refresh() in frame f-pdsr.     
      display b-pdsr with frame f-pdsr.
    end.
    else 
    if not avail pdsr and avail bpdsr then do: 
     delete bpdsr. 
     b-pdsr:refresh() in frame f-pdsr.     
     display b-pdsr with frame f-pdsr.
    end.  
    v-rebrecnoh = 0.
   end. 
   else
   if {k-func8.i} and avail bpdsr then do: 
     if bpdsr.marked = " " and
        can-find(first b-bpdsr where b-bpdsr.vendno = bpdsr.vendno and
                 b-bpdsr.marked = "*" no-lock) then do:
         run zsdierrx.p
         ( "While marked records exist F8 must be applied to '*' record",yes).          return.
     end.   
       
      
      
      find first pdsr where
                 pdsr.cono     = g-cono + 500 and
                 pdsr.rebrecno = bpdsr.rebrecno and 
                 pdsr.vendno   = bpdsr.vendno   and 
                 pdsr.custno   = bpdsr.custno 
                 exclusive-lock no-error.
       if avail pdsr then do:
        assign x-buyer = substring(pdsr.user1,1,4).
        find first apsv where
                   apsv.cono   = g-cono and 
                   apsv.vendno = bpdsr.vendno
                   no-lock no-error.
        if avail apsv then 
           assign b-name = apsv.name.
        else 
           assign b-name = " " .
        if g-secure > 2 then do:
         display pdsr.rebrecno  pdsr.levelkey pdsr.enddt pdsr.startdt
                 pdsr.rebcalcty pdsr.dropshipty pdsr.vendno b-name
                 with frame f-pdsrchg.
         update pdsr.rebateamt pdsr.enddt pdsr.contractno pdsr.refer 
                x-buyer
                with frame f-pdsrchg.
          status default " " in window current-window.
          self:select-focused-row().
          run f8-updates.
          b-pdsr:refresh() in frame f-pdsr.     
          display b-pdsr with frame f-pdsr.
          apply lastkey.
         end.
        else 
        if g-secure <= 2 then do:
          display pdsr.rebrecno pdsr.levelkey pdsr.dropshipty 
                  pdsr.rebateamt pdsr.contractno b-name pdsr.vendno
                  pdsr.refer pdsr.enddt pdsr.startdt x-buyer pdsr.rebcalcty 
                  with frame f-pdsrchg.
          pause 15.
        end. 
       end.
   end. /** k-func8.i **/
   else    
   if {k-func9.i} and avail bpdsr and g-secure > 2 then do: 
    assign q-recid   = recid(bpdsr) 
           i-fcustno = bpdsr.custno 
           i-fvendno = bpdsr.vendno
           i-fenddt  = bpdsr.enddt. 
    display i-fcustno i-fvendno with frame f-pdsrdt. 
    update i-tenddt i-fenddt
           with frame f-pdsrdt.
    
    if frame-field = "fcustno" then 
     self:select-focused-row().
    if {k-return.i} or {k-accept.i} then do: 
     assign i-fenddt  = input i-fenddt
            i-tenddt  = input i-tenddt         
            chghit    = no.
     if i-tenddt ne ? then 
      run "expire-chg".
    end.
    b-pdsr:refresh() in frame f-pdsr.     
    display b-pdsr with frame f-pdsr.
    apply lastkey.
   end.
 end. /* do transaction */
 if g-secure <= 2 then do: 
  assign x-function =       
"       F6-Print            F7-Tied-PDZR           F8-Display-PDZR-Info      ".  end.
 else 
 if g-secure > 2 then do: 
  assign x-function = 
"    F6-Print         F7-Tied-PDZR         F8-Change         F9-Chg-ExpDts   ". 
 end.
end. /*** big do any-key **/

/****** Main Block ****************/

main:             
do while true on endkey undo, next main: 
 endsw = no.
 fhit = no.
 v-rebrecnoh = 0.
 {p-setup.i f-pdsr}   
 main2: 
 do while true on endkey undo, next main2: 
  update g-buyer
         g-custno 
         g-vendno 
         g-prod 
         s-ExpBegDt
         s-ExpEndDt 
         v-active 
         with frame f-pdsr
    editing:
    readkey.  
      if {k-jump.i} then do: 
      run clear-browse.
      apply "window-close" to current-window.
      leave main2.
     end.
     if {k-cancel.i} then do: 
      run clear-browse.
      apply "window-close" to current-window.
      leave main.
     end.
     if {k-return.i} or {k-accept.i} then do: 
      assign g-buyer    = input g-buyer 
             g-custno   = input g-custno 
             g-vendno   = input g-vendno 
             g-prod     = input g-prod
             s-ExpBegDt = input s-ExpBegDt 
             v-ExpBegDt = input s-ExpBegDt 
             s-ExpEndDt = input s-ExpEndDt 
             v-ExpEndDt = input s-ExpEndDt 
             v-active   = input v-active. 
       
      if v-ExpBegDt ne ? then do:
       v-datein = string(v-ExpBegDt,"99/99/99").
       {p-rptdt.i}
       if string(v-dateout) = v-highdt then
        v-ExpBegDt = 01/01/1950.
       else
        v-ExpBegDt = v-dateout.
      end.
      else
       v-ExpBegDt = 01/01/1950.

      if v-ExpEndDt ne ? then do:
       v-datein = string(v-ExpEndDt,"99/99/99").
       {p-rptdt.i}
       if string(v-dateout) = v-lowdt then
        v-ExpEndDt = 12/31/2049.
       else  
        v-ExpEndDt = v-dateout.
      end.
      else
       v-ExpEndDt = 12/31/2049.
  
      if v-active ne " " then do: 
       if (v-active ne "a" and v-active ne "i" and
           v-active ne "b") then do: 
        run zsdierrx.p
         (" Invalid active selection - blank, a, i or b - please. ","yes"). 
        pause 2.
        next main2. 
       end.    
      end. 
      
      find oimsp where oimsp.person = g-buyer no-lock no-error.
      if avail oimsp then 
       assign buyernm = oimsp.name. 
      else 
       assign buyernm = "".
      find first arsc where
                 arsc.cono   = g-cono and 
                 arsc.custno = g-custno
                 no-lock no-error.
      if avail arsc then 
       assign c-name = arsc.name.
      else 
       assign c-name = " ".
      find first apsv where
                 apsv.cono   = g-cono and 
                 apsv.vendno = g-vendno
                 no-lock no-error.
      if avail apsv then 
       assign v-name = apsv.name.
      else 
       assign v-name = " " .
      display g-custno g-vendno g-prod buyernm 
              c-name v-name
              s-ExpBegDt s-ExpEndDt  
              v-active
              with frame f-pdsr.
      if {k-return.i} and frame-field = "v-active" or {k-accept.i} then  
       leave main2.
     end. /*** end k-after k-return k-accept key check ***/         
     apply lastkey.
   end.  /*** end editing  ****/                 
 end. /* main2 do while */

if lastkey = 311 then do:  
 on cursor-up back-tab.
 on cursor-down tab.
 return. 
end. 
  
if lastkey ne 404 then do: 
 run "extract-recs".
 if fhit = no then do: 
  run err.p(1016).
 end.    
 status input off.
 status default
  "  F6, F7, F8, F9 - Apply 'm' to mark/unmark group amt change."
    in window current-window.
 display g-buyer g-custno g-vendno g-prod with frame f-pdsr. 
 on cursor-up cursor-up.
 on cursor-down cursor-down.
 open query q-pdsr preselect each bpdsr use-index k-idx1.
 enable b-pdsr with frame f-pdsr.
 wait-for window-close of current-window.
 close query q-pdsr.
 on cursor-up back-tab. 
 on cursor-down tab.
end.
end. /****** end do while loop******/

status default. 
 {j-all.i "frame f-pdsr"} 

/***** procedure section *******/

procedure extract-recs:
do:
 if g-secure <= 2 then do: 
  assign x-function =       
"       F6-Print            F7-Tied-PDZR           F8-Display-PDZR-Info      ".
  display x-function with frame f-pdsr.
 end.
 else 
 if g-secure > 2 then do: 
  assign x-function = 
"    F6-Print         F7-Tied-PDZR         F8-Change         F9-Chg-ExpDts   ". 
  display x-function with frame f-pdsr.
 end.
 if g-buyer = "" then do:  
  if g-prod ne "" and g-custno ne 0 and g-vendno ne 0 then do: 
     for each pdsr use-index k-contractno 
        where pdsr.cono       = g-cono + 500  and
              pdsr.vendno     = g-vendno      and 
              pdsr.levelkey   = g-prod        and   
              pdsr.custno     = g-custno      and 
            ((pdsr.enddt     >= v-ExpBegDt    and 
              pdsr.enddt     <= v-ExpEndDt) or pdsr.enddt = ?) 
        no-lock
        break by pdsr.levelkey
              by pdsr.custno
              by pdsr.vendno: 
       if pdsr.enddt = ? and 
         (v-ExpEndDt ne 12/31/2049) then 
        next. 
       if v-active = "a" and pdsr.enddt < today and pdsr.enddt ne ? then 
        next. 
       if v-active = "i" and (pdsr.enddt >= today or pdsr.enddt = ?) then 
        next.
       cnt = cnt + 1.
       create bpdsr.
       assign bpdsr.rebrecno    = pdsr.rebrecno   
              bpdsr.brecid      = cnt
              bpdsr.custno      = pdsr.custno
              bpdsr.name        = p-name 
              bpdsr.vendno      = pdsr.vendno 
              bpdsr.contractno  = pdsr.contractno 
              bpdsr.levelkey    = pdsr.levelkey 
              bpdsr.rebateamt   = pdsr.rebateamt 
              bpdsr.rebcalcty   = pdsr.rebcalcty
              bpdsr.dropshipty  = pdsr.dropshipty
              bpdsr.user1       = substring(pdsr.user1,1,4)
              bpdsr.refer       = pdsr.refer 
              bpdsr.startdt     = pdsr.startdt 
              bpdsr.enddt       = pdsr.enddt
              bpdsr.enddt2      = string(month(pdsr.enddt),"99") +
                                  string(day(pdsr.enddt),"99")   +
                                  substring(string(pdsr.enddt),7,2). 
       fhit = yes.       
     end. /*** for each ****/
  end.
  else 
  if g-prod = "" and g-custno ne 0 and g-vendno ne 0 then do:
     for each pdsr use-index k-contractno 
        where pdsr.cono       = g-cono + 500  and
              pdsr.vendno     = g-vendno      and 
              pdsr.custno     = g-custno      and 
            ((pdsr.enddt     >= v-ExpBegDt    and 
              pdsr.enddt     <= v-ExpEndDt) or pdsr.enddt = ?) 
        no-lock
        break by pdsr.custno
              by pdsr.vendno
              by pdsr.levelkey:
       if pdsr.enddt = ? and 
         (v-ExpEndDt ne 12/31/2049) then 
        next. 
       if v-active = "a" and pdsr.enddt < today and pdsr.enddt ne ? then 
        next. 
       if v-active = "i" and (pdsr.enddt >= today or pdsr.enddt = ?) then 
        next.
       cnt = cnt + 1.
       create bpdsr.
       assign bpdsr.rebrecno    = pdsr.rebrecno   
              bpdsr.brecid      = cnt
              bpdsr.custno      = pdsr.custno
              bpdsr.name        = p-name 
              bpdsr.vendno      = pdsr.vendno 
              bpdsr.contractno  = pdsr.contractno 
              bpdsr.levelkey    = pdsr.levelkey 
              bpdsr.rebateamt   = pdsr.rebateamt
              bpdsr.rebcalcty   = pdsr.rebcalcty
              bpdsr.dropshipty  = pdsr.dropshipty
              bpdsr.user1       = substring(pdsr.user1,1,4)
              bpdsr.refer       = pdsr.refer 
              bpdsr.startdt     = pdsr.startdt 
              bpdsr.enddt       = pdsr.enddt
              bpdsr.enddt2      = string(month(pdsr.enddt),"99") +
                                  string(day(pdsr.enddt),"99")   +
                                  substring(string(pdsr.enddt),7,2). 
       fhit = yes.       
     end. /*** for each ****/
  end.
  else 
  if g-prod ne "" and g-custno = 0 and g-vendno ne 0 then do:
     for each pdsr use-index k-contractno 
        where pdsr.cono       = g-cono + 500 and
              pdsr.levelkey   = g-prod       and 
              pdsr.vendno     = g-vendno     and 
            ((pdsr.enddt     >= v-ExpBegDt   and 
              pdsr.enddt     <= v-ExpEndDt) or pdsr.enddt = ?) 
        no-lock
        break by pdsr.levelkey
              by pdsr.vendno
              by pdsr.custno:
       if pdsr.enddt = ? and 
         (v-ExpEndDt ne 12/31/2049) then 
        next. 
       if v-active = "a" and pdsr.enddt < today and pdsr.enddt ne ? then 
        next. 
       if v-active = "i" and (pdsr.enddt >= today or pdsr.enddt = ?) then 
        next.
       cnt = cnt + 1.
       create bpdsr.
       assign bpdsr.rebrecno    = pdsr.rebrecno   
              bpdsr.brecid      = cnt
              bpdsr.custno      = pdsr.custno
              bpdsr.name        = p-name 
              bpdsr.vendno      = pdsr.vendno 
              bpdsr.contractno  = pdsr.contractno 
              bpdsr.levelkey    = pdsr.levelkey 
              bpdsr.rebateamt   = pdsr.rebateamt 
              bpdsr.rebcalcty   = pdsr.rebcalcty
              bpdsr.dropshipty  = pdsr.dropshipty
              bpdsr.user1       = substring(pdsr.user1,1,4)
              bpdsr.refer       = pdsr.refer 
              bpdsr.startdt     = pdsr.startdt 
              bpdsr.enddt       = pdsr.enddt 
              bpdsr.enddt2      = string(month(pdsr.enddt),"99") +
                                  string(day(pdsr.enddt),"99")   +
                                  substring(string(pdsr.enddt),7,2). 
             fhit = yes.       
     end. /*** for each ****/
  end.
  else  
  if g-prod ne "" and g-custno ne 0 and g-vendno = 0 then do:
     for each pdsr use-index k-pdsr 
        where pdsr.cono       = g-cono + 500 and
              pdsr.levelkey   = g-prod       and 
              pdsr.custno     = g-custno     and 
            ((pdsr.enddt     >= v-ExpBegDt   and 
              pdsr.enddt     <= v-ExpEndDt) or pdsr.enddt = ?) 
        no-lock
        break by pdsr.levelkey
              by pdsr.custno
              by pdsr.vendno:
       if pdsr.enddt = ? and 
         (v-ExpEndDt ne 12/31/2049) then 
        next. 
       if v-active = "a" and pdsr.enddt < today and pdsr.enddt ne ? then 
        next. 
       if v-active = "i" and (pdsr.enddt >= today or pdsr.enddt = ?) then 
        next.
       cnt = cnt + 1.
       create bpdsr.
       assign bpdsr.rebrecno    = pdsr.rebrecno   
              bpdsr.brecid      = cnt
              bpdsr.custno      = pdsr.custno
              bpdsr.name        = p-name 
              bpdsr.vendno      = pdsr.vendno 
              bpdsr.contractno  = pdsr.contractno 
              bpdsr.levelkey    = pdsr.levelkey 
              bpdsr.rebateamt   = pdsr.rebateamt 
              bpdsr.rebcalcty   = pdsr.rebcalcty
              bpdsr.dropshipty  = pdsr.dropshipty
              bpdsr.user1       = substring(pdsr.user1,1,4)
              bpdsr.refer       = pdsr.refer 
              bpdsr.startdt     = pdsr.startdt 
              bpdsr.enddt       = pdsr.enddt
              bpdsr.enddt2      = string(month(pdsr.enddt),"99") +
                                  string(day(pdsr.enddt),"99")   +
                                  substring(string(pdsr.enddt),7,2). 
       fhit = yes.       
     end. /*** for each ****/
  end.
  else 
  if g-prod = "" and g-custno = 0 and g-vendno ne 0 then do:
     for each pdsr use-index k-contractno 
        where pdsr.cono       = g-cono + 500 and
              pdsr.vendno     = g-vendno     and 
            ((pdsr.enddt     >= v-ExpBegDt   and 
              pdsr.enddt     <= v-ExpEndDt) or pdsr.enddt = ?) 
        no-lock
        break by pdsr.vendno 
              by pdsr.levelkey
              by pdsr.custno:
       if pdsr.enddt = ? and 
         (v-ExpEndDt ne 12/31/2049) then 
        next. 
       if v-active = "a" and pdsr.enddt < today and pdsr.enddt ne ? then 
        next. 
       if v-active = "i" and (pdsr.enddt >= today or pdsr.enddt = ?) then 
        next.
       cnt = cnt + 1.
       create bpdsr.
       assign bpdsr.rebrecno    = pdsr.rebrecno   
              bpdsr.brecid      = cnt
              bpdsr.custno      = pdsr.custno
              bpdsr.name        = p-name 
              bpdsr.vendno      = pdsr.vendno 
              bpdsr.contractno  = pdsr.contractno 
              bpdsr.levelkey    = pdsr.levelkey 
              bpdsr.rebateamt   = pdsr.rebateamt 
              bpdsr.rebcalcty   = pdsr.rebcalcty
              bpdsr.dropshipty  = pdsr.dropshipty
              bpdsr.user1       = substring(pdsr.user1,1,4)
              bpdsr.refer       = pdsr.refer 
              bpdsr.startdt     = pdsr.startdt 
              bpdsr.enddt       = pdsr.enddt
              bpdsr.enddt2      = string(month(pdsr.enddt),"99") +
                                  string(day(pdsr.enddt),"99")   +
                                  substring(string(pdsr.enddt),7,2). 
       fhit = yes.       
     end. /*** for each ****/
  end.
  else  
  if g-prod ne "" and g-custno = 0 and g-vendno = 0 then do:  
     for each pdsr use-index k-pdsr 
        where pdsr.cono       = g-cono + 500 and
              pdsr.levelkey   = g-prod       and 
            ((pdsr.enddt     >= v-ExpBegDt   and 
              pdsr.enddt     <= v-ExpEndDt) or pdsr.enddt = ?) 
        no-lock
        break by pdsr.levelkey
              by pdsr.custno
              by pdsr.vendno:
       if pdsr.enddt = ? and 
         (v-ExpEndDt ne 12/31/2049) then 
        next. 
       if v-active = "a" and pdsr.enddt < today and pdsr.enddt ne ? then 
        next. 
       if v-active = "i" and (pdsr.enddt >= today or pdsr.enddt = ?) then 
        next.
       cnt = cnt + 1.
       create bpdsr.
       assign bpdsr.rebrecno    = pdsr.rebrecno   
              bpdsr.brecid      = cnt
              bpdsr.custno      = pdsr.custno
              bpdsr.name        = p-name 
              bpdsr.vendno      = pdsr.vendno 
              bpdsr.contractno  = pdsr.contractno 
              bpdsr.levelkey    = pdsr.levelkey 
              bpdsr.rebateamt   = pdsr.rebateamt 
              bpdsr.rebcalcty   = pdsr.rebcalcty
              bpdsr.dropshipty  = pdsr.dropshipty
              bpdsr.user1       = substring(pdsr.user1,1,4)
              bpdsr.refer       = pdsr.refer 
              bpdsr.startdt     = pdsr.startdt 
              bpdsr.enddt       = pdsr.enddt
              bpdsr.enddt2      = string(month(pdsr.enddt),"99") +
                                  string(day(pdsr.enddt),"99")   +
                                  substring(string(pdsr.enddt),7,2). 
       fhit = yes.       
     end. /*** for each ****/
  end.
  else 
  if g-prod = "" and g-custno ne 0 and g-vendno = 0 then do: 
     for each pdsr use-index k-pdsr 
        where pdsr.cono       = g-cono + 500 and
              pdsr.custno     = g-custno     and 
            ((pdsr.enddt     >= v-ExpBegDt   and 
              pdsr.enddt     <= v-ExpEndDt) or pdsr.enddt = ?) 
        no-lock
        break by pdsr.custno
              by pdsr.vendno:      
       if pdsr.enddt = ? and 
         (v-ExpEndDt ne 12/31/2049) then 
        next. 
       if v-active = "a" and pdsr.enddt < today and pdsr.enddt ne ? then 
        next. 
       if v-active = "i" and (pdsr.enddt >= today or pdsr.enddt = ?) then 
        next.
       cnt = cnt + 1.
       create bpdsr.
       assign bpdsr.rebrecno    = pdsr.rebrecno   
              bpdsr.brecid      = cnt
              bpdsr.custno      = pdsr.custno
              bpdsr.name        = p-name 
              bpdsr.vendno      = pdsr.vendno 
              bpdsr.contractno  = pdsr.contractno 
              bpdsr.levelkey    = pdsr.levelkey 
              bpdsr.rebateamt   = pdsr.rebateamt 
              bpdsr.rebcalcty   = pdsr.rebcalcty
              bpdsr.dropshipty  = pdsr.dropshipty
              bpdsr.user1       = substring(pdsr.user1,1,4)
              bpdsr.refer       = pdsr.refer 
              bpdsr.startdt     = pdsr.startdt 
              bpdsr.enddt       = pdsr.enddt
              bpdsr.enddt2      = string(month(pdsr.enddt),"99") +
                                  string(day(pdsr.enddt),"99")   +
                                  substring(string(pdsr.enddt),7,2). 
       fhit = yes.       
     end. /*** for each ****/
  end.
 end. /* if g-buyer = "" */
 
 if g-buyer ne "" and g-prod = "" and g-custno ne 0 and g-vendno = 0 then do:
     for each pdsr use-index k-pdsr 
        where pdsr.cono       = g-cono + 500 and
              pdsr.custno     = g-custno     and 
              pdsr.user1      = g-buyer      and 
            ((pdsr.enddt     >= v-ExpBegDt   and 
              pdsr.enddt     <= v-ExpEndDt) or pdsr.enddt = ?) 
        no-lock
        break by pdsr.custno
              by pdsr.user1:      
       if pdsr.enddt = ? and 
         (v-ExpEndDt ne 12/31/2049) then 
        next. 
       if v-active = "a" and pdsr.enddt < today and pdsr.enddt ne ? then 
        next. 
       if v-active = "i" and (pdsr.enddt >= today or pdsr.enddt = ?) then 
        next.
       cnt = cnt + 1.
       create bpdsr.
       assign bpdsr.rebrecno    = pdsr.rebrecno   
              bpdsr.brecid      = cnt
              bpdsr.custno      = pdsr.custno
              bpdsr.name        = p-name 
              bpdsr.vendno      = pdsr.vendno 
              bpdsr.contractno  = pdsr.contractno 
              bpdsr.levelkey    = pdsr.levelkey 
              bpdsr.rebateamt   = pdsr.rebateamt 
              bpdsr.rebcalcty   = pdsr.rebcalcty
              bpdsr.dropshipty  = pdsr.dropshipty
              bpdsr.user1       = substring(pdsr.user1,1,4)
              bpdsr.refer       = pdsr.refer 
              bpdsr.startdt     = pdsr.startdt 
              bpdsr.enddt       = pdsr.enddt
              bpdsr.enddt2      = string(month(pdsr.enddt),"99") +
                                  string(day(pdsr.enddt),"99")   +
                                  substring(string(pdsr.enddt),7,2). 
       fhit = yes.       
     end. /*** for each ****/
 end.
 else 
 if g-buyer ne "" and g-prod = "" and g-custno = 0 and g-vendno ne 0 then do:
     for each pdsr use-index k-contractno 
        where pdsr.cono       = g-cono + 500 and
              pdsr.vendno     = g-vendno     and 
              pdsr.user1      = g-buyer      and 
            ((pdsr.enddt     >= v-ExpBegDt   and 
              pdsr.enddt     <= v-ExpEndDt) or pdsr.enddt = ?) 
        no-lock
        break by pdsr.vendno
              by pdsr.user1:      
       if pdsr.enddt = ? and 
         (v-ExpEndDt ne 12/31/2049) then 
        next. 
       if v-active = "a" and pdsr.enddt < today and pdsr.enddt ne ? then 
        next. 
       if v-active = "i" and (pdsr.enddt >= today or pdsr.enddt = ?) then 
        next.
       cnt = cnt + 1.
       create bpdsr.
       assign bpdsr.rebrecno    = pdsr.rebrecno   
              bpdsr.brecid      = cnt
              bpdsr.custno      = pdsr.custno
              bpdsr.name        = p-name 
              bpdsr.vendno      = pdsr.vendno 
              bpdsr.contractno  = pdsr.contractno 
              bpdsr.levelkey    = pdsr.levelkey 
              bpdsr.rebateamt   = pdsr.rebateamt 
              bpdsr.rebcalcty   = pdsr.rebcalcty
              bpdsr.dropshipty  = pdsr.dropshipty
              bpdsr.user1       = substring(pdsr.user1,1,4)
              bpdsr.refer       = pdsr.refer 
              bpdsr.startdt     = pdsr.startdt 
              bpdsr.enddt       = pdsr.enddt
              bpdsr.enddt2      = string(month(pdsr.enddt),"99") +
                                  string(day(pdsr.enddt),"99")   +
                                  substring(string(pdsr.enddt),7,2). 
       fhit = yes.       
     end. /*** for each ****/
 end.
 else 
 if g-buyer ne "" and g-prod ne "" and g-custno = 0 and g-vendno = 0 then do:
     for each pdsr use-index k-pdsr 
        where pdsr.cono       = g-cono + 500 and
              pdsr.levelkey   = g-prod       and 
              pdsr.user1      = g-buyer      and
            ((pdsr.enddt     >= v-ExpBegDt   and 
              pdsr.enddt     <= v-ExpEndDt) or pdsr.enddt = ?) 
        no-lock
        break by pdsr.levelkey
              by pdsr.user1:      
       if pdsr.enddt = ? and 
         (v-ExpEndDt ne 12/31/2049) then 
        next. 
       if v-active = "a" and pdsr.enddt < today and pdsr.enddt ne ? then 
        next. 
       if v-active = "i" and (pdsr.enddt >= today or pdsr.enddt = ?) then 
        next.
       cnt = cnt + 1.
       create bpdsr.
       assign bpdsr.rebrecno    = pdsr.rebrecno   
              bpdsr.brecid      = cnt
              bpdsr.custno      = pdsr.custno
              bpdsr.name        = p-name 
              bpdsr.vendno      = pdsr.vendno 
              bpdsr.contractno  = pdsr.contractno 
              bpdsr.levelkey    = pdsr.levelkey 
              bpdsr.rebateamt   = pdsr.rebateamt 
              bpdsr.rebcalcty   = pdsr.rebcalcty
              bpdsr.dropshipty  = pdsr.dropshipty
              bpdsr.user1       = substring(pdsr.user1,1,4)
              bpdsr.refer       = pdsr.refer 
              bpdsr.startdt     = pdsr.startdt 
              bpdsr.enddt       = pdsr.enddt
              bpdsr.enddt2      = string(month(pdsr.enddt),"99") +
                                  string(day(pdsr.enddt),"99")   +
                                  substring(string(pdsr.enddt),7,2). 
       fhit = yes.       
     end. /*** for each ****/
 end.
 else 
 if g-buyer ne "" and g-prod = "" and g-custno = 0 and g-vendno = 0 then do:
     for each pdsr use-index k-pdsr 
        where pdsr.cono       = g-cono + 500 and
              pdsr.user1      = g-buyer      and 
            ((pdsr.enddt     >= v-ExpBegDt   and 
              pdsr.enddt     <= v-ExpEndDt) or pdsr.enddt = ?) 
        no-lock
        break by pdsr.custno
              by pdsr.vendno:      
       if pdsr.enddt = ? and 
         (v-ExpEndDt ne 12/31/2049) then 
        next. 
       if v-active = "a" and pdsr.enddt < today and pdsr.enddt ne ? then 
        next. 
       if v-active = "i" and (pdsr.enddt >= today or pdsr.enddt = ?) then 
        next.
       cnt = cnt + 1.
       create bpdsr.
       assign bpdsr.rebrecno    = pdsr.rebrecno   
              bpdsr.brecid      = cnt
              bpdsr.custno      = pdsr.custno
              bpdsr.name        = p-name 
              bpdsr.vendno      = pdsr.vendno 
              bpdsr.contractno  = pdsr.contractno 
              bpdsr.levelkey    = pdsr.levelkey 
              bpdsr.rebateamt   = pdsr.rebateamt 
              bpdsr.rebcalcty   = pdsr.rebcalcty
              bpdsr.dropshipty  = pdsr.dropshipty
              bpdsr.user1       = substring(pdsr.user1,1,4)
              bpdsr.refer       = pdsr.refer 
              bpdsr.startdt     = pdsr.startdt 
              bpdsr.enddt       = pdsr.enddt
              bpdsr.enddt2      = string(month(pdsr.enddt),"99") +
                                  string(day(pdsr.enddt),"99")   +
                                  substring(string(pdsr.enddt),7,2). 
       fhit = yes.       
     end. /*** for each ****/
 end.
 else 
 if g-buyer ne "" and g-prod = "" and g-custno ne 0 and g-vendno ne 0 then do:
     for each pdsr use-index k-contractno 
        where pdsr.cono       = g-cono + 500 and
              pdsr.vendno     = g-vendno     and 
              pdsr.custno     = g-custno     and 
              pdsr.user1      = g-buyer      and 
            ((pdsr.enddt     >= v-ExpBegDt   and 
              pdsr.enddt     <= v-ExpEndDt) or pdsr.enddt = ?) 
        no-lock
        break by pdsr.vendno
              by pdsr.custno
              by pdsr.user1:      
       if pdsr.enddt = ? and 
         (v-ExpEndDt ne 12/31/2049) then 
        next. 
       if v-active = "a" and pdsr.enddt < today and pdsr.enddt ne ? then 
        next. 
       if v-active = "i" and (pdsr.enddt >= today or pdsr.enddt = ?) then 
        next.
       cnt = cnt + 1.
       create bpdsr.
       assign bpdsr.rebrecno    = pdsr.rebrecno   
              bpdsr.brecid      = cnt
              bpdsr.custno      = pdsr.custno
              bpdsr.name        = p-name 
              bpdsr.vendno      = pdsr.vendno 
              bpdsr.contractno  = pdsr.contractno 
              bpdsr.levelkey    = pdsr.levelkey 
              bpdsr.rebateamt   = pdsr.rebateamt 
              bpdsr.rebcalcty   = pdsr.rebcalcty
              bpdsr.dropshipty  = pdsr.dropshipty
              bpdsr.user1       = substring(pdsr.user1,1,4)
              bpdsr.refer       = pdsr.refer 
              bpdsr.startdt     = pdsr.startdt 
              bpdsr.enddt       = pdsr.enddt
              bpdsr.enddt2      = string(month(pdsr.enddt),"99") +
                                  string(day(pdsr.enddt),"99")   +
                                  substring(string(pdsr.enddt),7,2). 
       fhit = yes.       
     end. /*** for each ****/
 end.
 else 
 if g-buyer ne "" and g-prod ne "" and g-custno ne 0 and g-vendno = 0 then do:
     for each pdsr use-index k-pdsr 
        where pdsr.cono       = g-cono + 500 and
              pdsr.levelkey   = g-prod       and
              pdsr.custno     = g-custno     and 
              pdsr.user1      = g-buyer      and 
            ((pdsr.enddt     >= v-ExpBegDt   and 
              pdsr.enddt     <= v-ExpEndDt) or pdsr.enddt = ?) 
        no-lock
        break by pdsr.vendno
              by pdsr.custno
              by pdsr.user1:      
       if pdsr.enddt = ? and 
         (v-ExpEndDt ne 12/31/2049) then 
        next. 
       if v-active = "a" and pdsr.enddt < today and pdsr.enddt ne ? then 
        next. 
       if v-active = "i" and (pdsr.enddt >= today or pdsr.enddt = ?) then 
        next.
       cnt = cnt + 1.
       create bpdsr.
       assign bpdsr.rebrecno    = pdsr.rebrecno   
              bpdsr.brecid      = cnt
              bpdsr.custno      = pdsr.custno
              bpdsr.name        = p-name 
              bpdsr.vendno      = pdsr.vendno 
              bpdsr.contractno  = pdsr.contractno 
              bpdsr.levelkey    = pdsr.levelkey 
              bpdsr.rebateamt   = pdsr.rebateamt 
              bpdsr.rebcalcty   = pdsr.rebcalcty
              bpdsr.dropshipty  = pdsr.dropshipty
              bpdsr.user1       = substring(pdsr.user1,1,4)
              bpdsr.refer       = pdsr.refer 
              bpdsr.startdt     = pdsr.startdt 
              bpdsr.enddt       = pdsr.enddt
              bpdsr.enddt2      = string(month(pdsr.enddt),"99") +
                                  string(day(pdsr.enddt),"99")   +
                                  substring(string(pdsr.enddt),7,2). 
       fhit = yes.       
     end. /*** for each ****/
 end.
 if g-buyer ne "" and g-prod ne "" and g-custno ne 0 and g-vendno ne 0 then do:
     for each pdsr use-index k-contractno 
        where pdsr.cono       = g-cono + 500 and
              pdsr.vendno     = g-vendno     and
              pdsr.levelkey   = g-prod       and
              pdsr.custno     = g-custno     and 
              pdsr.user1      = g-buyer      and
            ((pdsr.enddt     >= v-ExpBegDt   and 
              pdsr.enddt     <= v-ExpEndDt) or pdsr.enddt = ?) 
        no-lock
        break by pdsr.vendno
              by pdsr.custno
              by pdsr.user1:      
       if pdsr.enddt = ? and 
        (v-ExpEndDt ne 12/31/2049) then 
        next. 
       if v-active = "a" and pdsr.enddt < today and pdsr.enddt ne ? then 
        next. 
       if v-active = "i" and (pdsr.enddt >= today or pdsr.enddt = ?) then 
        next.
       cnt = cnt + 1.
       create bpdsr.
       assign bpdsr.rebrecno    = pdsr.rebrecno   
              bpdsr.brecid      = cnt
              bpdsr.custno      = pdsr.custno
              bpdsr.name        = p-name 
              bpdsr.vendno      = pdsr.vendno 
              bpdsr.contractno  = pdsr.contractno 
              bpdsr.levelkey    = pdsr.levelkey 
              bpdsr.rebateamt   = pdsr.rebateamt 
              bpdsr.rebcalcty   = pdsr.rebcalcty
              bpdsr.dropshipty  = pdsr.dropshipty
              bpdsr.user1       = substring(pdsr.user1,1,4)
              bpdsr.refer       = pdsr.refer 
              bpdsr.startdt     = pdsr.startdt 
              bpdsr.enddt       = pdsr.enddt
              bpdsr.enddt2      = string(month(pdsr.enddt),"99") +
                                  string(day(pdsr.enddt),"99")   +
                                  substring(string(pdsr.enddt),7,2). 
       fhit = yes.       
     end. /*** for each ****/
 end.
end. /* big do */
end.  /* procedure */

procedure clear-browse:
 if {k-cancel.i} then do:  
  status input off.
  status default "Enter data or press PF11 to end. ". 
 end.
 close query q-pdsr.
 on cursor-up back-tab. 
 on cursor-down tab.
 empty temp-table bpdsr.
 disable b-pdsr with frame f-pdsr.
end. /**** end procedure ***/

procedure prt-routine:
do: 
   lncnt = 0.
   if s-printernm ne " " then do: 
     find sasp where
          sasp.printernm = s-printernm
          no-lock no-error. 
     if avail sasp then 
      do:
        output through value(sasp.pcommand) paged.
        put control "~033~046~1541~117".   /* landscape format */
        put screen row 22 col 2 color message
"Selected File is Printing - Please Check Printer                              ".
        pause 3.
        put screen row 22 col 2 color message
"                                                                              ".
        view frame f-hd.
        for each bpdsr use-index k-custno where
                 bpdsr.rebrecno > 0
            no-lock:
          find pdsr use-index k-rebrecno where 
               pdsr.cono     = g-cono + 500 and
               pdsr.rebrecno = bpdsr.rebrecno
               no-lock no-error.
           if avail pdsr then 
            do:
             find first arsc where 
                        arsc.cono   = g-cono and 
                        arsc.custno = pdsr.custno 
                  no-lock no-error.
             if avail arsc then 
                assign p-name = arsc.name.
             else     
                assign p-name = " No Name available".

             find first apsv use-index k-apsv where 
                        apsv.cono = g-cono and 
                        apsv.vendno = pdsr.vendno
                        no-lock no-error.
             if avail apsv then 
                assign x-vendname = apsv.name.
             else 
                assign x-vendname = " ".         
             if lncnt > 39 then do:
                 page.
                 lncnt = 0.
             end.   
             lncnt = lncnt + 1.
             x-buyer = substring(pdsr.user1,1,4).
             x-refer = substring(pdsr.refer,1,14).
             display  pdsr.rebrecno  
                      pdsr.levelcd   
                      pdsr.custno   
                      p-name     
                      pdsr.vendno    
                      x-vendname
                      pdsr.contractno
                      pdsr.levelkey  
                      pdsr.rebateamt 
                      pdsr.rebcalcty
                      pdsr.dropshipty
                      x-refer     
                      x-buyer
                      pdsr.startdt   
                      pdsr.enddt     
                      with frame f-pd.             
                      down with frame f-pd. 
            end.
        end. /* for each bpdsr */           
        output close.
      end.
   end.     
end.
end. /*** end procedure **/
 
procedure expire-chg: 
if i-tenddt ne ? then do: 
 for each pdsr use-index k-enddt 
    where pdsr.cono       = g-cono + 500 and
          pdsr.enddt      = i-fenddt  and  
          pdsr.vendno     = i-fvendno and 
          pdsr.custno     = i-fcustno  
    exclusive-lock:
    assign chghit        = yes
           pdsr.enddt    = i-tenddt
           pdsr.operinit = g-operinit
           pdsr.transdt  = today 
           pdsr.transtm  = string(time,"hh:mm").
 end.
 for each bpdsr use-index k-enddt 
    where bpdsr.enddt      = i-fenddt  and  
          bpdsr.vendno     = i-fvendno and 
          bpdsr.custno     = i-fcustno  
    exclusive-lock:
    assign bpdsr.enddt    = i-tenddt.
 end.            
end.
end.               

procedure f8-updates: 
def buffer x-bpdsr for bpdsr. 
def buffer x-pdsr  for pdsr.  

if avail bpdsr and bpdsr.marked = "*" then 
 assign h-rebamt = bpdsr.rebateamt. 
if not avail bpdsr then leave.

if avail bpdsr and bpdsr.marked <> "*" then do: 

  for each x-bpdsr use-index k-idx1 where 
           x-bpdsr.vendno   = pdsr.vendno   and 
           x-bpdsr.levelkey = pdsr.levelkey and
           x-bpdsr.custno   = pdsr.custno   and 
           x-bpdsr.user1    = pdsr.user1 exclusive-lock: 
     find x-pdsr use-index k-rebrecno where
          x-pdsr.cono     = g-cono + 500 and 
          x-pdsr.rebrecno = x-bpdsr.rebrecno
          exclusive-lock no-error. 
     if avail x-pdsr then do:  
       assign x-pdsr.rebateamt  = pdsr.rebateamt 
              x-pdsr.enddt      = pdsr.enddt
              x-pdsr.contractno = pdsr.contractno
              x-pdsr.refer      = pdsr.refer
              x-pdsr.user1      = x-buyer            
              x-pdsr.operinit   = g-operinit               
              x-pdsr.transdt    = today                    
              x-pdsr.transtm    = string(time,"hh:mm").    
     end. 
     if avail x-bpdsr and avail x-pdsr then do: 
      assign x-bpdsr.rebateamt  = x-pdsr.rebateamt 
             x-bpdsr.enddt      = x-pdsr.enddt
             x-bpdsr.contractno = x-pdsr.contractno
             x-bpdsr.refer      = x-pdsr.refer
             x-bpdsr.user1      = x-buyer.
     end.               
  end.
end. 

if avail bpdsr and bpdsr.marked = "*" then do: 
  assign q-recid  = recid(bpdsr) 
         i-fcustno = bpdsr.custno 
         i-fvendno = bpdsr.vendno
         i-begamt  = h-rebamt
         i-chgamt  = pdsr.rebateamt. 
  display i-fcustno i-fvendno i-begamt with frame f-multchgs.
  amtupdt: 
  do while true on endkey undo, leave amtupdt: 
    update i-chgamt 
          with frame f-multchgs
   editing: 
   readkey. 
    apply lastkey. 
    assign i-chgamt = input i-chgamt.
    if lastkey = 404 then do:
     readkey pause 0.
     apply 401.
     leave amtupdt. 
    end. 
    if lastkey = 401 then 
     leave. 
   end. 
   /** marked changes are determined by the current browse line that f8 was    
       initiated from - the vendor and amount must match the current browse 
       line in order to be changed **/
   for each x-bpdsr use-index k-marked where 
            x-bpdsr.vendno = bpdsr.vendno   and
            x-bpdsr.marked = "*" exclusive-lock: 
    assign x-bpdsr.marked    = " "
           x-bpdsr.rebateamt = i-chgamt.
    find x-pdsr use-index k-rebrecno where
         x-pdsr.cono     = g-cono + 500 and 
         x-pdsr.rebrecno = x-bpdsr.rebrecno
         exclusive-lock no-error. 
    if avail x-pdsr /* and x-pdsr.rebateamt = h-rebamt  */ then do: 
     assign x-pdsr.rebateamt  = i-chgamt
            x-pdsr.enddt      = pdsr.enddt
            x-pdsr.contractno = pdsr.contractno
            x-pdsr.refer      = pdsr.refer
            x-pdsr.user1      = x-buyer            
            x-pdsr.operinit   = g-operinit               
            x-pdsr.transdt    = today                    
            x-pdsr.transtm    = string(time,"hh:mm").    
    end. 
   end. 
   /* clean up any marked items that did not get changed because they were not       the vendor chosen on the browse line item */ 
   for each x-bpdsr use-index k-marked where 
            x-bpdsr.marked = "*" exclusive-lock: 
    assign x-bpdsr.marked = " ".
   end.
   leave.
  end. 
  if lastkey = 404 then do: 
   readkey pause 0. 
  end. 
end.  
end. 




