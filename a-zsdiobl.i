/*h*****************************************************************************
  INCLUDE      : a-oeiol.i
  DESCRIPTION  : OEIO Line Detail Function keys
  USED ONCE?   : yes
  AUTHOR       : rhl
  DATE WRITTEN : 09/20/89
  CHANGES MADE : dkt 09/15/2006 g-batchnm
**************************************************************************** */
if {k-func.i} then do:
  /*d Set OE Line Global and find the OEEL Line */
  {p-zsdiobl.i}

  if {k-func6.i} and avail oeelb then do:
    put screen row 21 col 4 color message "F6-RETURN".
    run oeiolr.p(s-prod).
  end.
  else if {k-func7.i} and avail oeelb then run zsdioble.p(input v-batchnm).
  else if {k-func8.i} and avail oeelb then do:
    put screen row 21 column 33 color messages "F8-SERIAL/LOTS".
    {w-icsw.i oeelb.shipprod oeehb.whse no-lock}
    if avail icsw then do:
      if icsw.serlottype = "S" then
        run oeiols.p("o",g-orderno,g-ordersuf,g-oelineno,0).
      else if icsw.serlottype = "L" then
        run oeioll.p("o",g-orderno,g-ordersuf,g-oelineno,0).
      else do:
        /*e not serial/lot controlled or not avail icsw */
        run err.p(6451).
        {pause.gsc}
      end.
    end.
    else next main.
  end.
/*
    else if {k-func9.i} and avail oeelb
    then do:
        put screen row 21 col 49 color message "F9-HISTORY".

        run oeiolh.p.
    end.
*/
  else if {k-func10.i} and avail oeelb then do:
    if substring(oeelb.user5,1,1) <> "y" then do:
      run err.p(6461).
      {pause.gsc 2}
    end. /* oeelb.kitfl = no */
    else do:
      assign
        s-prod       = oeelb.shipprod
        g-whse       = oeehb.whse
        v-stkqtyship = 0 /* oeelb.stkqtyship */
        k-orderno    = v-batchnm
        k-ordersuf   = 0
        k-lineno     = g-oelineno
        k-ordtype    = "b".
      run oeiolk.p.
    end. /* oeel.kitfl = yes */
  end.
  put screen row 21 col 3 color message v-fmsg.
  if {k-jump.i} or {k-select.i} then leave main.
  hide message no-pause.
  next.
end.

else if {k-func19.i} then do:
  assign g-quoteno = int(v-batchnm).
  run zsdicomd.p.
end.

