/* SX 55
   d-poeral.i 1.2 10/20/98 */
/*h****************************************************************************
INCLUDE      : d-poeral.i
DESCRIPTION  : PO RRAR line item screen display data (x-xxex.i)
USED ONCE?   :
AUTHOR       : mtt
DATE WRITTEN : 07/30/92
CHANGES MADE :
  07/30/92 mtt; Made major changes to the way the PO RRAR acceptance detail
      screen will work using the new X-XXEX.I include.  The following TBs
      are involved:
                TB#  7335 (Apex) - Ability to add new line items to the PO
      RRAR (all comments will use this tb#).
                TB#  5875 (Hisco) - Down arrow does not adv to next page
      (up arrow also).
                TB#  4411 (Texas Screen) - Want 'next line' function key.
  11/24/92 mms; TB#  8827 Rush enhancement.
  05/25/93 enp; TB# 11029 Comments in RRAR.
  03/08/95 emc; TB# 17945 Display locked status.
  03/09/97 jkp; TB#  7241 (5.2) Spec8.0 Special Price Costing.  Added find
      of icss record.
  10/07/98 kjb; TB# 21027 Add a second nonstock description
  05/23/99 bp ; TB# 26228 Add the user includes &user_befforms (in poeral.p),
      &user_stockasgn (in d-poeral.i), &aft_dispasgn (in d-poeral.i), and
      &user_aftdisp (in d-poeral.i).
******************************************************************************/

/*tb 11029 05/25/93 enp; Comments in RRAR */
/*tb 17945 03/08/95 emc; Display "l" if the line is locked */
/*tb 21027 10/07/98 kjb; Added proddesc2 for second nonstock description */
assign
    s-scrollfield  = if poeral.commentfl then "c" else ""
    s-lineno       = poeral.lineno
    s-nonstockty   = poeral.nonstockty
    s-shipprod     = poeral.shipprod
    s-wtcono       = if poeral.wtcono = g-cono then 0
                     else poeral.wtcono
    s-whse         = if poeral.vendfl then "" else poeral.whse
    s-qtyord       = poeral.qtyord
    s-q            = if poeral.pdsvfl then "q" else ""
    s-unit         = poeral.unit
    s-price        = poeral.price
    s-descrip-1    = poeral.proddesc
    s-descrip-2    = ""   /* poeral.proddesc2  SX 55 */
    s-accepttype   = poeral.accepttype
    s-lock         = if poeral.lockfl then "l" else ""
    s-bo-words     = ""
    s-class        = 0
    s-usagerate    = 0
    /*tb 8827 11/24/92 mms */
.    s-rushfl       = poeral.rushfl.

if poeral.nonstockty <> "n" then do:

    {w-icsp.i poeral.shipprod no-lock}
    /*tb  7241 (5.2) 03/09/97 jkp; Added find of icss record based on
         the poeral record because the prices/costs are being kept in the
         poeral units. */
    {icss.gfi
        &prod        = poeral.shipprod
        &icspecrecno = poeral.icspecrecno
        &lock        = "no"}

    /*tb  7335 07/30/92 mtt; Made det screen have 6 lines not 12(2nd ln desc)
        (was above assign of s-descrip-1). */
    /*tb  7786 09/10/92 mtt; Move v-invalid to description field if no icsp
        (was above second assign fo s-descrip-1 (but also combined these two
         assigns)). */
    /*tb  7241 (5.2) 03/09/97 jkp; Changed the assign of s-sp to use the icss
         record.  Combined two assigns into one. */
    assign
        s-shipprod   = s-shipprod +
                       if avail icsp then
                           icsp.notesfl
                       else ""
        s-sp         = if avail icss and icss.speccostty = "" then
                           ""
                       else "s"

        s-descrip-1  = if avail icsp then
                           icsp.descrip[1]
                       else v-invalid
        s-descrip-2  = if avail icsp then
                           icsp.descrip[2]
                       else "".

    {w-icsw.i poeral.shipprod poeral.whse no-lock}

    if avail icsw then assign
        s-bo-words    = if icsw.qtybo <> 0 then "B/O" else ""
        s-class       = icsw.class
        s-usagerate   = icsw.usagerate.

    
    /*u User Include */
    {poeral.z99 &user_stockasgn = "*"}

end.
else assign
         s-sp          = "".

clear frame f-poeral no-pause.

/*u User Include */
{poeral.z99 &aft_dispasgn = "*"}


/*tb 11029 05/25/93 enp; Comments in RRAR */
display s-scrollfield
        s-lineno  
        s-nonstockty
        s-lock
        s-shipprod
        s-descrip-1
        s-descrip-2
        s-wtcono
        s-whse
        s-qtyord
        s-q
        s-unit
        s-price
        s-sp
        s-accepttype
        /*tb 8827 11/24/92 mms */
        s-rushfl when s-rushfl with frame f-poeral.
      for each oeel use-index k-prod where oeel.cono = g-cono and
                                          oeel.shipprod =  poeral.shipprod and
                                          oeel.statustype = "a" and
                                         (oeel.transtype = "BL" or 
                                          oeel.transtype = "BR") and
                                          oeel.ordertype = "" and
                                          oeel.bono = 0
                                          no-lock:
                                     
       if oeel.transtype = "BL" and oeel.qtyrel >= oeel.qtyord then next.

       find first oeeh where oeeh.cono      = g-cono and
                             oeeh.orderno   = oeel.orderno and
                             oeeh.ordersuf  = oeel.ordersuf and
                            (oeeh.transtype = "BR" or 
                             oeeh.transtype = "BL") and
                            (oeeh.stagecd >= 0 and 
                             oeeh.stagecd <= 1)
                             no-lock no-error.
       if avail oeeh then do:
           color display message s-lineno with frame f-poeral. 
       end.
     end. 







/*  
find first oeeh where oeeh.cono = 1 and
                      oeeh.orderno = poeral.orderaltno and
                      oeeh.ordersuf = poeral.orderaltsuf
                      no-lock no-error. 
     if avail oeeh then do:
        if (oeeh.transtype = "BR" or
            oeeh.transtype = "BL") then do:
           color display message s-lineno with frame f-poeral. 
        end.
     end. 
*/       
                      






if poeral.nonstockty <> "n" and avail icsw then
    display s-bo-words
            s-class
            s-usagerate with frame f-poeral.
                         
                              
                              
/*u User Include */
{poeral.z99 &user_aftdisp = "*"}

 
