/{&user_temptable}*/
/* Put temp-table and field definitions here */
{g-vastg2.i}

def var p-detailprt                 as logical                  no-undo.
def  var p-regval                   as c                        no-undo.
def buffer b-ztmk for ztmk.
def buffer b2-ztmk for ztmk.

def new shared var w-month          as c    format "x(9)"       no-undo.
def new shared var w-date           as c    format "x(10)"      no-undo.
def new shared var work-date        as date format "99/99/9999" no-undo.
def new shared var b1-date          as date format "99/99/9999" no-undo.
def new shared var e1-date          as date format "99/99/9999" no-undo.
def new shared var ze1-date         as date format "99/99/9999" no-undo.
def new shared var b2-date          as date format "99/99/9999" no-undo.
def new shared var e2-date          as date format "99/99/9999" no-undo.
def new shared var beg-date         as date format "99/99/9999" no-undo.
def new shared var leapyr           as de   format "9999.99"    no-undo.
def new shared var leaptst          as c    format "x(7)"       no-undo.
def var p-perend                    as int                      no-undo.
def var p-format                    as int                      no-undo.
def var p-includelaborfl            as logical                  no-undo.
def new shared var b-invdate        as date                     no-undo.
def new shared var e-invdate        as date                     no-undo.
def var b-techid                    as char format "x(4)"       no-undo. 
def var e-techid                    as char format "x(4)"       no-undo. 
def var b-oper                      as char format "x(4)"       no-undo. 
def var e-oper                      as char format "x(4)"       no-undo. 
def var b-am                        as char format "x(4)"       no-undo. 
def var e-am                        as char format "x(4)"       no-undo. 
def var b-tagno                     as char format "x(16)"      no-undo. 
def var e-tagno                     as char format "x(16)"      no-undo.
def var b-stage                     as char format "x(3)"       no-undo.
def var e-stage                     as char format "x(3)"       no-undo.
def var b-rstage                    as char format "x(3)"       no-undo.
def var e-rstage                    as char format "x(3)"       no-undo.


def var b-istage                    as integer                  no-undo.
def var e-istage                    as integer                  no-undo.
def var b-istage2                   as integer                  no-undo.
def var e-istage2                   as integer                  no-undo.
def var b-ristage                   as integer                  no-undo.
def var e-ristage                   as integer                  no-undo.

def var f-bstage                    as integer                  no-undo.
def var f-estage                    as integer                  no-undo.
def var b-wono                      like kpet.wono              no-undo.
def var e-wono                      like kpet.wono              no-undo.
def var b-whse                      like kpet.whse              no-undo.
def var e-whse                      like kpet.whse              no-undo.
def var l-date                      as date                     no-undo.
def var s-stageprt                  as c format "x(3)"          no-undo.
def var s-stageprt2                 as c format "x(3)"          no-undo.
def var d-wononbr                   as c format "x(16)"         no-undo.
def var d-wononbr2                  as c format "x(16)"         no-undo.
def var v-stagetext                 as c extent 10              no-undo initial
["","Ord","Prt","Blt"," "," "," "," "," ","Can"].
def var h-stage                     as int format "99"          no-undo.
def var h-indate                    as date                     no-undo.
def var v-stgint                    as int format "99"          no-undo.
def var m-stgint                    as int format "99"          no-undo.
def var inx                         as integer                  no-undo.
def var inx2                        as integer                  no-undo.
def var t-ordqty                    as decimal                  no-undo.
def var t-sordqty                   as decimal                  no-undo.
def var t-tordqty                   as decimal                  no-undo.
def var t-wordqty                   as decimal                  no-undo.
def var t-gordqty                   as decimal                  no-undo.
def var t-dordqty                   as decimal                  no-undo.
def var t-qty                       as decimal                  no-undo.
def var t-tmout                     as int                      no-undo.
def var t-tmin                      as int                      no-undo.
def var t-time                      as decimal                  no-undo.
def var s-tdn                       as char format "999:99"     no-undo.
def var s-tottime2                  as integer                  no-undo init 0.
def var tot-esthm                   as integer                  no-undo.
def var totalt                      as integer                  no-undo.
def var totalt2                     as integer                  no-undo.
def var tdntot                      as integer                  no-undo.
def var asytot                      as integer                  no-undo.
def var t-hrs                       as decimal                  no-undo. 
def var t-hrs-left                  as decimal                  no-undo. 
def var t-min                       as decimal                  no-undo.
def var ts-lendays                  as integer                  no-undo.
def var ts-lenhr                    as integer                  no-undo.
def var ts-lenmin                   as integer                  no-undo.
def var ts-lensec                   as integer                  no-undo.
def var t-count                     as decimal                  no-undo.
def var prt-time                    as char format "999:99"     no-undo.
def var prt-days                    as int  format "zzz"        no-undo.
def var dtl-tstg-time               as char format "999:99"     no-undo.
def var dtl-tech-time               as char format "999:99"     no-undo.
def var dtl-date-time               as char format "999:99"     no-undo.
def var dtl-twono-time              as char format "999:99"     no-undo.
def var dtl-tgtl-time               as char format "999:99"     no-undo.
def var t-stg-days                  as integer                  no-undo.
def var t-dte-days                  as integer                  no-undo.
def var t-ftl-days                  as integer                  no-undo.
def var t-days                      as decimal                  no-undo.
def var t-xdays                     as decimal                  no-undo.
def var t-days-prt                  as decimal                  no-undo.
def var t-days-tot                  as decimal                  no-undo.
def var dtl-time                    as decimal                  no-undo.
def var tstg-time                   as decimal                  no-undo.
def var tech-time                   as decimal                  no-undo.
def var date-time                   as decimal                  no-undo.
def var twono-time                  as decimal                  no-undo.
def var tgtl-time                   as decimal                  no-undo.
def var s-title                     as char format "x(120)"     no-undo.
def var v-name                      as char format "x(21)"      no-undo.
def var date-flg                    as l                        no-undo.
def var detl-flg                    as l                        no-undo.
def var rcv-flg                     as l                        no-undo.
def var stg-flg                     as l                        no-undo.
def var mlt-stages                  as l                        no-undo.
def var cls-flag                    as l                        no-undo.
def var v-cls                       as l                        no-undo.
def var v-cls2                      as l                        no-undo.
def var v-close                     as l                        no-undo.
def var cls-flag2                   as l                        no-undo.
def var can-flag                    as l                        no-undo.
def var s-tdnflag                   as l                        no-undo.
def var s-asyflag                   as l                        no-undo.
def var s-tstflag                   as l                        no-undo.
def var labor-flg                   as l                        no-undo.
def var s-dateprt                   as date                     no-undo.
def var p-spaces                    as c format "x" init " "    no-undo.
def var v-errfl                     as l                        no-undo.
def var v-error                     as c format "x(78)"         no-undo.
def var v-stgprt                    as c format "x(8)"          no-undo.
def var v-operinit                  as c format "x(4)"          no-undo.
def var p-name                      as char format "x(30)"      no-undo.
def var p-partno                    as char format "x(24)"      no-undo.
def var v-whse                      like kpet.whse.
def var v-shipto                    like arsc.shipto.
def var v-prodcat                   like icsp.prodcat.
def var v-custno                    like arsc.custno.
def var v-slsrep                    like arsc.slsrepout.
def var v-techid                    like arsc.slsrepout.
def var v-esth                      as integer /* format "x(2)"  */  no-undo.
def var v-estm                      as integer /* format "x(2)"  */  no-undo.
def var tdn-esth                    as char format "x(2)"       no-undo.
def var tdn-estm                    as char format "x(2)"       no-undo.
def var tdn-intime                  as char format "999:99"       no-undo.




def var tl-operinit                 as char format "x(10)" initial "Operator".
def var tl-repair                   as char format "x(10)" initial "RepairNo".
def var tl-stage                    as char format "x(6)"  initial "Stage".
def var tl-indate                   as char format "x(10)" initial "InDate".
def var tl-tech                     as char format "x(6)"  initial "TechID".
def var tl-esth                     as char format "x(6)"  initial "Est.Time".
def var tl-estm                     as char format "x(6)".
def var tl-miltime                  as char format "x(8)"  initial "Act.Time". def var tl-days                     as char format "x(4)"  initial "Days".
def var tl-name                     as char format "x(25)" initial "Name". 
def var tl-partno                   as char format "x(28)" initial "PartNo".
def var tl-slsrep                   as char format "x(6)"  initial "AM#".

Define temp-table vasort no-undo
  Field tech      as char format "x(4)" 
  Field operinit  as char format "x(4)"
  Field repair    as char format "x(12)"
  Field stage     as c format "x(3)"
  Field stagecd   as int
  Field indate    as date
  Field intime    as char format "999:99" 
  Field outdate   as date
  Field ttime     as integer format "9999999999" 
  Field outtime   as char format "99:99"
  Field estout    as char format "x(1)"
  Field miltime   as char format "999:99"
  Field days      as decimal
  Field name      as char format "x(30)" 
  Field number    as integer format ">>>>>>999999"
  Field partno    as char format "x(24)" 
  Field whse      as char format "x(4)"
  Field slsrep    as char format "x(4)"
  Field esth      as integer format "99" /* char format "x(3)" */
  Field estm      as integer format "99" /* char format "x(4)" */
  Field tdn       as char format "999:99"
  Field totalt    as integer format ">>>>>9999"
  Field totesthm  as integer format ">>>>>9999"
  Field sortdtl   as char format "x(36)"
  Field ztmkrecid as recid
index k-indate
      indate
      intime
index  k-ztmkrec 
       ztmkrecid 
index  k-repair
       repair.

/* tah tot */
def temp-table t-estctrl
  Field entitylist    as char
  Field repair        as char format "x(12)"
  Field stage         as char format "x(3)"
  Field totesthm      as integer              
index k-ctl
      entitylist
      repair
      stage.
def var v-entitylist  as char no-undo.

/{&user_temptable}*/

/{&user_drpttable}*/
/* Use to add fields to the report table if needed */
  field vasortid as recid
/{&user_drpttable}*/

/{&user_forms}*/
/* Place form definitions here */
form                                           
  "~015"           at 1                        
  with frame f-skip width 178 no-box no-labels.  
form   
   vasort.operinit       at 1   label "Oper"
   vasort.repair         at 6   label "Repair"
   vasort.stage          at 18  label "Stg"
   vasort.indate         at 23  label "InDate"
   vasort.intime         at 33  format "xx:xx" label "InTime"
   vasort.outdate        at 40  label "OutDate"
   vasort.estout         at 48  format "x"      no-label
   vasort.outtime        at 50  format "xx:xx"  label "OutTime"
   vasort.miltime        at 58  format "999:99" label "Hrs:Min"    
   vasort.days           at 67  format "zzz"    label "Days"
   vasort.number         at 73  format ">>>>>>999999" label "CustNo"
   vasort.name           at 87  format "x(25)"  label "Name"
   vasort.partno         at 115  label "PartNo"
   vasort.slsrep         at 141  label "AM#"
   vasort.tech           at 147  label "Tech"
   vasort.esth           at 152  label "Est"
   ":"                   at 155   
   vasort.estm           at 156  label ".Tm"
   vasort.tdn            at 162  format "999:99" label "Act.Tm"    
   with down  frame f-sort-detail 
       width 178 no-box no-labels.
/*
form
   vasort.operinit       at 1   label "Oper"
   vasort.repair         at 6   label "Repair"
   vasort.stage          at 16  label "Stg"
   vasort.indate         at 21  label "InDate"
   vasort.intime         at 31  format "xx:xx" label "InTime"
   vasort.outdate        at 38  label "OutDate"
   vasort.outtime        at 48  format "xx:xx"  label "OutTime"
   vasort.miltime        at 56  format "999:99" label "Hrs:Min"    
   vasort.days           at 64  format "zzz"    label "Days"
   vasort.number         at 69  format ">>>>>>999999" label "CustNo"
   vasort.name           at 84  format "x(25)"  label "Name"
   vasort.partno         at 109  label "PartNo"
   vasort.slsrep         at 134  label "AM#"
   vasort.tech           at 140  label "Tech"
   vasort.esth           at 146  label "Est"
   ":"                   at 149   
   vasort.estm           at 150  label ".Tm"
   with down  frame f-sort-detail2 
        width 178 no-box no-labels.
*/

form                                                               
  v-final                at 1                                      
  v-astrik               at 2   format "x(5)"                      
  v-summary-lit2         at 7   format "x(52)"                     
  
  v-amt1           at 60  format ">>>,>>9-" /*label "Labor total" */     
  v-amt2           at 70  format ">>>,>>9-"  label "Total Count"   
  v-amt3           at 82  format ">>9-"  label "Total Min"   
  v-amt4           at 92  format ">>9-"  label "Total hrs"   
  v-amt6           at 102 format ">>>>>9-" label "Total Days" 
  v-amt13          at 128 format ">>99"  label "Est."  
  ":"              at 132
  v-amt12          at 133 format "99"  label "Tm"  
  v-amt14          at 137 format ">>9"   label "Est Days"  
  
  v-amt9           at 148 format  ">>99" label "Act."   
  ":"              at 152
  v-amt8           at 153 format  "99" label "Tm"   
  v-amt10          at 156 format  ">>>>>9"  label "Act Days"  

  with frame f-tot width 178 no-labels.                                         
  

form header                                          
 "Punch out dates marked by '*' are estimated using current date/time "  at 1
 "because they have not been punched out yet."                           at 69
  with frame f-footer 
     width 178 no-box no-labels overlay page-bottom.   
  
/{&user_forms}*/

/{&user_extractrun}*/
/* This is a place to load SAPB ranges and options and extract temp-table */
 run sapb_vars.
 run extract_data. 
/{&user_extractrun}*/

/{&user_exportstatheaders}*/
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
 
  assign export_rec =
         export_rec   +  " ".
         
  if p-detailprt then
    assign export_rec = export_rec + v-del .

/{&user_exportstatheaders}*/

/{&user_exportstatheaders1}*/

  assign export_rec = "". 
/{&user_exportstatheaders1}*/

/{&user_foreach}*/
/* must have an iterating block here - a label is in zsdixrptx.p */
   for each vasort no-lock: 

/{&user_foreach}*/

/{&user_drptassign}*/
 /* If you add fields to the dprt table this is where you assign them */
      drpt.vasortid = recid(vasort)
/{&user_drptassign}*/


/{&user_B4endloop}*/

if drpt.xval[1] = 0 and
   drpt.xval[2] = 0 and
   drpt.xval[3] = 0 and
   drpt.xval[4] = 0 and
   drpt.xval[5] = 0 and
   drpt.xval[6] = 0 and
   drpt.xval[7] = 0 and
   drpt.xval[8] = 0 and
   drpt.xval[9] = 0 and
   drpt.xval[10] = 0 and
   drpt.xval[11] = 0 and
   drpt.xval[12] = 0 and
   drpt.xval[13] = 0 and
   drpt.xval[14] = 0 and
   drpt.xval[15] = 0 and
   drpt.xval[16] = 0 and
   drpt.xval[17] = 0 and
   drpt.xval[18] = 0 and
   drpt.xval[19] = 0 and
   drpt.xval[20] = 0 and
   drpt.xval[21] = 0 and
   drpt.xval[22] = 0 and
   drpt.xval[23] = 0 and
   drpt.xval[24] = 0 and
   drpt.xval[25] = 0 and
   drpt.xval[26] = 0 and
   drpt.xval[27] = 0 and
   drpt.xval[28] = 0 and
   drpt.xval[29] = 0 and
   drpt.xval[30] = 0 and
   drpt.xval[31] = 0 and
   drpt.xval[32] = 0 and
   drpt.xval[33] = 0 and
   drpt.xval[34] = 0 and
   drpt.xval[35] = 0 and
   drpt.xval[36] = 0 and
   drpt.xval[37] = 0 and
   drpt.xval[38] = 0 and
   drpt.xval[39] = 0 and
   drpt.xval[40] = 0 then
  assign v-entitylist = "".
else do:
  assign v-entitylist  = "".
  find t-estctrl where 
       t-estctrl.entitylist = "FinalTotal#" and
       t-estctrl.stage      = vasort.stage and
       t-estctrl.repair     = vasort.repair no-lock no-error.

  if not avail t-estctrl then do:
    create t-estctrl.
    assign t-estctrl.entitylist = "FinalTotal#" 
           t-estctrl.repair     = vasort.repair
           t-estctrl.stage      = vasort.stage            
           t-estctrl.totesthm   = vasort.totesthm.
  end.          

  do v-inx = 1 to u-entitys - 1:
    assign v-entitylist = v-entitylist + v-delim +
                          entry(v-inx,drpt.mystery,v-delim).
    find t-estctrl where 
         t-estctrl.entitylist = v-entitylist and
         t-estctrl.repair     = vasort.repair and
         t-estctrl.stage      = vasort.stage          
         no-lock no-error.

    if not avail t-estctrl then do:
       create t-estctrl.
       assign t-estctrl.entitylist = v-entitylist 
              t-estctrl.repair     = vasort.repair
              t-estctrl.stage      = vasort.stage      
              t-estctrl.totesthm   = vasort.totesthm.
    end.          
  end.


end.
/{&user_B4endloop}*/


/{&user_viewheadframes}*/
/* Since frames are unique to each program you need to put your
   views and hides here with your names */

if not p-exportl then 
  do:
  if not x-stream then do:
    hide frame f-footer.
  end.
  else do:
    hide stream xpcd frame f-footer.
  end.
end.

if not p-exportl  then do:
  if not x-stream then do:
    page.
    view frame f-footer.
  end.
  else do:
    page stream xpcd.
    view stream xpcd frame f-footer.
  end.
end.
else do: 
  page.
  view frame f-footer.
end.

/{&user_viewheadframes}*/

/{&user_drptloop}*/

/{&user_drptloop}*/


/{&user_detailprint}*/
   /* Comment this out if you are not using detail 'X' option  */

   if p-detailprt and 
      entry(u-entitys,drpt.sortmystery,v-delim) <> 
      fill("~~",length(entry(u-entitys,drpt.sortmystery,v-delim))) then do: 
    /* this code skips the all other lines for printing detail */
    /* next line would be your detail print procedure */
     run print-dtl(input drpt.vasortid). 
   end.

/{&user_detailprint}*/

/{&user_finalprint}*/
                                                                 
if not x-stream then                                            
  do:                                                           
  assign v-amt1 = t-amounts1[u-entitys + 1].
  assign v-amt2 = t-amounts2[u-entitys + 1].
  assign v-amt6 = t-amounts3[u-entitys + 1].
  assign v-amt7 = t-amounts4[u-entitys + 1].
  assign v-amt11 = t-amounts5[u-entitys + 1].

  assign v-amt11 = 0.
  for each t-estctrl where 
           t-estctrl.entitylist = "FinalTotal#"  no-lock:
    assign v-amt11 = v-amt11 + t-estctrl.totesthm.
  end.  
  
  
  
  run formattime(input v-amt1,
                 output v-amt3,
                 output v-amt4,
                 output v-amt5). 
  assign v-amt6 = v-amt5. /* have to take  days from total time */               
  run formattime(input v-amt7,
                 output v-amt8,
                 output v-amt9,
                 output v-amt10). 
                 
  run formattime(input v-amt11,
                 output v-amt12,
                 output v-amt13,
                 output v-amt14). 
                 
                  
                 
                 
  display                                                       
     v-final                                                      
     v-astrik                                                     
     v-summary-lit2                                               
/*     v-amt1 */     
     v-amt2
     v-amt3
     v-amt4
/*     v-amt5  */
     v-amt6
/*     v-amt7     */
     v-amt8
     v-amt9
     v-amt10
     v-amt12
     v-amt13
     v-amt14
  with frame f-tot.     
  down with frame f-tot.
  end.
  else do:
   assign v-amt1 = t-amounts1[u-entitys + 1].
   assign v-amt2 = t-amounts2[u-entitys + 1].
   assign v-amt6 = t-amounts3[u-entitys + 1].
   assign v-amt7 = t-amounts4[u-entitys + 1].
   assign v-amt11 = t-amounts5[u-entitys + 1].
   
   assign v-amt11 = 0.
   for each t-estctrl where 
            t-estctrl.entitylist = "FinalTotal#"  no-lock:
     assign v-amt11 = v-amt11 + t-estctrl.totesthm.
   end.  
  
   
   run formattime(input v-amt1,
                  output v-amt3,
                  output v-amt4,
                  output v-amt5). 
                                                           
  assign v-amt6 = v-amt5. /* have to take  days from total time */                                                                           
  run formattime(input v-amt7,
                 output v-amt8,
                 output v-amt9,
                 output v-amt10). 
                 
  run formattime(input v-amt11,
                 output v-amt12,
                 output v-amt13,
                 output v-amt14). 
                 
  
  display stream xpcd                                                       
     v-final                                                      
     v-astrik                                                     
     v-summary-lit2                                               
/*   v-amt1   */
     v-amt2
     v-amt3
     v-amt4
/*     v-amt5  */
     v-amt6
/*   v-amt7 */
     v-amt8    
     v-amt9
     v-amt10
     v-amt12
     v-amt13
     v-amt14
   with frame f-tot.    
   down with frame f-tot.
end.
page.
/{&user_finalprint}*/

/{&user_summaryframeprint}*/
                                                                 
if not x-stream then                                            
  do:      
   assign v-amt1 = t-amounts1[v-inx2].
   assign v-amt2 = t-amounts2[v-inx2].
   assign v-amt6 = t-amounts3[v-inx2].
   assign v-amt7 = t-amounts4[v-inx2].
   assign v-amt11 = t-amounts5[v-inx2].
  
   assign v-entitylist = "".
   do v-inx3 = 1 to v-inx2:
     assign v-entitylist = v-entitylist + v-delim +
                           entry(v-inx3,v-holdmystery,v-delim).
   end.
   
   assign v-amt11 = 0.

   for each t-estctrl where 
            t-estctrl.entitylist = v-entitylist no-lock:
     assign v-amt11 = v-amt11 + t-estctrl.totesthm.
   end.  
    
   
   run formattime(input v-amt1,
                 output v-amt3,
                 output v-amt4,
                 output v-amt5).  

  assign v-amt6 = v-amt5. /* have to take  days from total time */              
  run formattime(input v-amt7,
                 output v-amt8,
                 output v-amt9,
                 output v-amt10). 

  run formattime(input v-amt11,
                 output v-amt12,
                 output v-amt13,
                 output v-amt14). 
                 
                                                     
   display                                                       
      v-final                                                      
      v-astrik                                                     
      v-summary-lit2                                               
/*    v-amt1    */
      v-amt2
      v-amt3
      v-amt4
/*      v-amt5  */
      v-amt6
/*    v-amt7    */
      v-amt8
      v-amt9
      v-amt10
      v-amt12
      v-amt13
      v-amt14
   with frame f-tot.     
   down with frame f-tot.
  end.
  else 
  do:       
   assign v-amt1 = t-amounts1[v-inx2].
   assign v-amt2 = t-amounts2[v-inx2].
   assign v-amt6 = t-amounts3[v-inx2].
   assign v-amt7 = t-amounts4[v-inx2].
   assign v-amt11 = t-amounts5[v-inx2].
   

  
  
   assign v-entitylist = "".
   do v-inx3 = 1 to v-inx2:
     assign v-entitylist = v-entitylist + v-delim +
                           entry(v-inx3,v-holdmystery,v-delim).
   end.
   
   assign v-amt11 = 0.

   for each t-estctrl where 
            t-estctrl.entitylist = v-entitylist no-lock:
     assign v-amt11 = v-amt11 + t-estctrl.totesthm.
   end.  
     
   run formattime(input v-amt1,
                 output v-amt3,
                 output v-amt4,
                 output v-amt5).

   assign v-amt6 = v-amt5. /* have to take  days from total time */                                                                 
   run formattime(input v-amt7,
                 output v-amt8,
                 output v-amt9,
                 output v-amt10). 

   run formattime(input v-amt11,
                 output v-amt12,
                 output v-amt13,
                 output v-amt14). 
                 
                  
                                                    
   display stream xpcd                                                       
      v-final                                                      
      v-astrik                                                     
      v-summary-lit2                                               
/*    v-amt1    */     
      v-amt2
      v-amt3
      v-amt4
/*    v-amt5  */
      v-amt6
/*    v-amt7    */
      v-amt8
      v-amt9
      v-amt10
      v-amt12
      v-amt13
      v-amt14
   with frame f-tot.     
   down with frame f-tot.
  end.


/{&user_summaryframeprint}*/

/{&user_summaryputexport}*/
/{&user_summaryputexport}*/

/{&user_procedures}*/

procedure sapb_vars:

/* set Ranges */


assign b-oper    = sapb.rangebeg[1] 
       e-oper    = sapb.rangeend[1]
       b-whse    = sapb.rangebeg[2]
       e-whse    = sapb.rangebeg[2]
       b-tagno   = sapb.rangebeg[4]
       e-tagno   = sapb.rangeend[4]
       b-stage   = sapb.rangebeg[5]
       e-stage   = sapb.rangeend[5]
       b-rstage   = sapb.rangebeg[14]
       e-rstage   = sapb.rangeend[14]
       b-am      = sapb.rangebeg[6] 
       e-am      = sapb.rangeend[6]
       b-techid  = sapb.rangebeg[7] 
       e-techid  = sapb.rangeend[7]

       p-format  = int(sapb.optvalue[1])
       detl-flg  = if sapb.optvalue[2] = "yes" then yes 
                   else no
       rcv-flg   = if sapb.optvalue[3] = "yes" then yes 
                   else no
       labor-flg = if sapb.optvalue[4] = "yes" then yes 
                   else no
       p-includelaborfl   = if sapb.optvalue[5] = "yes" then yes 
                   else no

       date-flg = yes.           


if stg-flg  = no and 
   detl-flg = no and
   rcv-flg  = no then 
 assign stg-flg = yes.


if b-stage = " " then 
  b-stage = "Rcv".
if e-stage begins "~~~~~~" then 
  e-stage = "Scp".


if  b-stage = "cls" and
    e-stage = "cls"  then do:
   assign cls-flag = yes.
  end.


if  b-stage = "can" and
    e-stage = "can"  then do:
   assign can-flag = yes.
  end.

do b-istage = 1 to 16:
  if v-stage2[b-istage] = b-stage then
   leave.
end.


do e-istage = 1 to 16:
 if v-stage2[e-istage] = e-stage then do:
   leave.
 end.  

do b-ristage = 1 to 16:
  if v-stage2[b-ristage] = b-rstage then
   leave.
end.


do e-ristage = 1 to 16:
 if v-stage2[e-ristage] = e-rstage then do:
   leave.
 end.  



if can-do("01,02,03,04,05,06,07,08,09,16,13,14,15,16",string(b-istage,"99")) and
   can-do("01,02,03,04,05,06,07,08,09,16,13,14,15,16",string(e-istage,"99")) then
   assign s-tdnflag = yes.

if can-do("10",string(b-istage,"99")) or
   can-do("10",string(e-istage,"99")) then

    assign s-asyflag = yes.


if can-do("11",string(b-istage,"99")) or
   can-do("11",string(e-istage,"99")) then

    assign s-tstflag = yes.


end. /* if not cls-flag */

if e-whse = " " then     
   assign e-whse = "zzzz".

if num-entries(b-tagno,"-") <> 2 then do: 
   assign b-tagno = "aaaa-0000001".
end.
if num-entries(e-tagno,"-") <> 2 then do: 
 assign e-tagno = "zzzz-9999999". 
end.


if sapb.rangebeg[3] ne "" then
 do:
  v-datein = sapb.rangebeg[3].
  {p-rptdt.i}
  if string(v-dateout) = v-lowdt then
    b-invdate = today.
  else  
    b-invdate = v-dateout.
 end.
else
  b-invdate = today.



if sapb.rangeend[3] ne "" then
 do:
  v-datein = sapb.rangeend[3].
  {p-rptdt.i}
  if string(v-dateout) = v-highdt then
    e-invdate = today.
  else
    e-invdate = v-dateout.
  end.
else
  e-invdate = today.


assign p-detailprt = detl-flg 
       p-detail    = "D" /* if detl-flg = yes then "D" else "S". */
       p-portrait  = yes.  


 run formatoptions.

end.


/* ------------------------------------------------------------------------- */
procedure formatoptions:
/* ------------------------------------------------------------------------- */
   
  assign p-portrait = false.
  /* if false then the report will print in LANDSCAPE 178 character */
  
  if sapb.optvalue[1] > "0" and sapb.optvalue[1] ne "99" then do: 
   p-optiontype = " ".

   find notes where notes.cono = g-cono and
                    notes.notestype = "zz" and
                    notes.primarykey = "varxt" and  /* tbxr program */
                    notes.secondarykey = string(p-format) no-lock no-error.
   if not avail notes then do: 
    display "Format is not valid cannot process request".
    assign p-optiontype = "v"
           p-sorttype   = ">,"
           p-totaltype  = "l"
           p-summcounts = "a".
    return.
   end.               
   
   assign p-optiontype = notes.noteln[1]
          p-sorttype   = notes.noteln[2]
          p-totaltype  = notes.noteln[3]
          p-summcounts = notes.noteln[4]
          p-export     = "    "
          p-register   = notes.noteln[5]
          p-registerex = notes.noteln[6].
  end.     
  else
  if sapb.optvalue[1] = "99" then do: 
   assign p-register   = ""
          p-registerex = "".
   run reportopts(input sapb.user5, 
                  input-output p-optiontype,
                  input-output p-sorttype,  
                  input-output p-totaltype,
                  input-output p-summcounts,
                  input-output p-register,
                  input-output p-regval).
  end.

  run print_reportopts(input recid(sapb), 
                       input g-cono).

end. /* procedure */


procedure extract_data:

if rcv-flg then do:  
 run rcv-flg. 
 leave.
end. 
else 
if labor-flg then do: 
 run labor-stages. 
 leave.
end. 

         
   for each ztmk use-index k-ztmk-sc1 where
            ztmk.cono      = g-cono       and 
            ztmk.ordertype = "sc"         and 
       /*     ztmk.statuscd  = 5            and  */  /* shows everything */
           (ztmk.user1     >= entry (1,b-tagno,"-") + "-" +
             string(int(entry(2,b-tagno,"-")),"9999999") and 
            ztmk.user1     <= entry (1,e-tagno,"-") + "-" +
             string(int(entry(2,e-tagno,"-")),"9999999")) and 
           (ztmk.punchindt >= b-invdate    and 
            ztmk.punchindt <= e-invdate)  and 
         /* ztmk.punchoutdt ne ?)         and */
           (ztmk.techid    >= b-techid    and 
            ztmk.techid    <= e-techid)   and    
           (ztmk.operinit >= b-oper       and 
            ztmk.operinit <= e-oper)     
            no-lock
            break by ztmk.cono
                  by ztmk.user1
                  by ztmk.stagecd
                  by ztmk.punchindt
                  by ztmk.punchintm:

       
       find first vasp where 
            vasp.cono     = 01         and 
            vasp.shipprod = ztmk.user1 and 
            vasp.whse     = entry(1,ztmk.user1,"-")          
            no-lock no-error.
       if avail vasp and 
          int(vasp.user7) < b-irstage or
          int(vasp.user7) > e-irstage) then
          next.
       find first icsd where
                  icsd.cono  = ztmk.cono    and 
                  icsd.whse  = entry(1,ztmk.user1,"-")
                  no-lock no-error.
       if avail icsd and 
         (icsd.whse < b-whse or 
          icsd.whse > e-whse) then do:
          
         if last-of (ztmk.user1) and
            p-includelaborfl then 
           run Include_Labor_Transactions.
         next.        
       end.
       if rcv-flg and (ztmk.stagecd ne 1) then do:
        
         if last-of (ztmk.user1) and
            p-includelaborfl then 
           run Include_Labor_Transactions.
 
         next.         
       end.
       
       find first vasp where 
            vasp.cono     = 01         and 
            vasp.shipprod = ztmk.user1 and 
            vasp.whse     = entry(1,ztmk.user1,"-")          
            no-lock no-error.
/*
       assign v-esth = int(substring(vasp.user4,50,2))
              v-estm = int(substring(vasp.user4,52,2)).
*/
       if ztmk.stagecd = 3 then              
         assign v-esth =  int(substring(vasp.user4,50,2))    
                v-estm =  int(substring(vasp.user4,52,2)).    
       else
       if ztmk.stagecd = 10 then              
         assign v-esth =  int(substring(vasp.user4,54,2))  
                v-estm =  int(substring(vasp.user4,56,2)).  
       else
       if ztmk.stagecd = 11 then              
          assign v-esth =  int(substring(vasp.user4,58,2))  
                 v-estm =  int(substring(vasp.user4,60,2)).  
       else       
          assign v-esth = 0
                 v-estm = 0.
       assign tot-esthm = (v-esth * 60) + v-estm.
       
       if ztmk.stagecd   < b-istage or
          ztmk.stagecd   > e-istage then do:
          
         if last-of (ztmk.user1) and
            p-includelaborfl then 
           run Include_Labor_Transactions.
 
         next. 
       end.
       
       find icsp where icsp.cono = 1 and
                       icsp.prod = ztmk.user1
                       no-lock no-error.
                       
         if avail icsp then 
            assign v-prodcat = icsp.prodcat.


/* here */

       if ztmk.punchoutdt = ? then
         assign t-days      = today - ztmk.punchindt  
                t-xdays     = t-days 
                t-tmout     = (dec(substring(string(time,"hh:mm"),1,2)) * 60) +
                               dec(substring(string(time,"hh:mm"),4,2))
                t-tmin      = (dec(substring(ztmk.punchintm,1,2)) * 60) + 
                               dec(substring(ztmk.punchintm,3,2))
                t-time      = if t-days = 0 then 
                                t-tmout - t-tmin
                              else 
                                (t-tmout - t-tmin) + (t-days * (24 * 60)).
 
       
       else       
         assign t-days      = ztmk.punchoutdt - ztmk.punchindt  
                t-xdays     = t-days 
                t-tmout     = (dec(substring(ztmk.punchouttm,1,2)) * 60) +
                               dec(substring(ztmk.punchouttm,3,2))
                t-tmin      = (dec(substring(ztmk.punchintm,1,2)) * 60) + 
                               dec(substring(ztmk.punchintm,3,2))
                t-time      = if t-days = 0 then 
                                t-tmout - t-tmin
                              else 
                                (t-tmout - t-tmin) + (t-days * (24 * 60)).
       assign v-whse      = entry(1,ztmk.user1,"-").
       assign t-ordqty    = t-ordqty  + 1
              t-dordqty   = t-dordqty + 1
              t-tordqty   = t-tordqty + 1
              t-sordqty   = t-sordqty + 1
              t-wordqty   = t-wordqty + 1
              t-gordqty   = t-gordqty + 1.
       assign v-operinit = ztmk.operinit
              v-techid   = ztmk.techid.
              
       find first arsc where
                  arsc.cono = g-cono and 
                  arsc.custno = dec(string(vasp.user5,"999999999999"))
            no-lock no-error.
       if avail arsc then 
        assign p-name = arsc.name
               v-shipto = arsc.shipto
               v-custno = arsc.custno
               v-slsrep = arsc.slsrepout.
       else 
        assign p-name = "". 
       find first vasp where 
                  vasp.cono     = 01     and 
                  vasp.shipprod = ztmk.user1        
         no-lock no-error.
       assign p-partno  = if avail vasp then 
                           vasp.refer 
                          else 
                           "" 
              d-wononbr = entry(1,ztmk.user1,"-") + "-" + 
                left-trim(entry(2,ztmk.user1,"-"),"0")
              v-stgprt  = if ztmk.stagecd = 0 then 
                              v-stage2[1]
                          else    
                          if ztmk.stagecd = 5 or
                             ztmk.stagecd  = 6 then 
                     /*  ask jen "ToBe" + "-" +  */ v-stage2[ztmk.stagecd]
                          else 
                            v-stage2[ztmk.stagecd]
              dtl-time   = t-time.              /* (t-time / 60).  */           
       
       
       if dtl-time > 0 then do:                     
        run formattime(input dtl-time, 
                      output t-min, 
                      output t-hrs, 
                      output t-xdays).
       end.    

       assign tstg-time  = tstg-time + dtl-time
              date-time  = date-time + dtl-time
              tgtl-time  = tgtl-time + dtl-time
              prt-days   = t-xdays
              t-ftl-days = t-ftl-days + t-xdays
              t-stg-days = t-stg-days + t-xdays
              t-dte-days = t-dte-days + t-xdays. 

       assign substring(prt-time,1,3) = string(t-hrs,"zz9")
              substring(prt-time,4,2) = string(t-min,"99")
              dtl-time = 0 
              t-hrs    = 0 
              t-min    = 0.


       run xsdioetlt.p (input-output v-slsrep,
                        input        v-prodcat,
                        input        v-custno,
                        input        v-shipto).

     if b-am <> "" then
       if v-slsrep < b-am or
          v-slsrep > b-am then do:

       if last-of (ztmk.user1) and
          p-includelaborfl then 
         run Include_Labor_Transactions.
           
       next.
     end.
/*
     if s-asyflag and ztmk.stagecd = 10 then do:
        run Accumulate_Asy (/*output s-tdn, */
                            output s-tottime2,
                            buffer ztmk).
           
     end.
       

     if s-tstflag and ztmk.stagecd = 11 then do:
       run Accumulate_tst (/* output  s-tdn, */
                           output s-tottime2,   
                           buffer ztmk).  
     end. 
     
     if s-tdnflag and ztmk.stagecd <> 10 and ztmk.stagecd <> 11 then do:
        run Accumulate_teardown (/* output  s-tdn, */
                                 output s-tottime2,   
                                 buffer ztmk).  
     end. 
       
*/

    if ztmk.stagecd <> 11 and ztmk.stagecd <> 10 and ztmk.stagecd <> 3 then            assign substring(s-tdn,1,3)  = "  0"
                substring(s-tdn,4,2)  = "00".
      else                                
         assign s-tdn = prt-time.
  
     if ztmk.stagecd = 11 or ztmk.stagecd = 10 or ztmk.stagecd = 3 then  
        assign  s-tottime2 = t-time. 
     else 
        assign s-tottime2 = 0.
        
       if cls-flag then 
         run Create_All_Transactions.
       else  
       if can-flag then
         run Create_All_Transactions.
       else do:
       create vasort.                       
       assign vasort.operinit  = v-operinit
              vasort.sortdtl   = string(ztmk.user1,"x(12)") +
                                 string(ztmk.stagecd,"99") +
                                 string(ztmk.punchindt,"99/99/9999") +
                                 string(ztmk.punchintm,"x(8)") +
                                 string(recid(ztmk),"999999999999") 
              vasort.repair    = d-wononbr
              vasort.stage     = v-stgprt
              vasort.stagecd   = ztmk.stagecd
              vasort.indate    = ztmk.punchindt
              vasort.tech      = v-techid
              vasort.esth      = v-esth
              vasort.estm      = v-estm
              vasort.intime    = ztmk.punchintm
              vasort.outdate   = if ztmk.punchoutdt = ? then
                                   today
                                 else
                                   ztmk.punchoutdt
              vasort.outtime   = if ztmk.punchoutdt = ? then
                                   substring(string(time,"hh:mm"),1,2) +
                                   substring(string(time,"hh:mm"),4,2) 
                                 else
                                   ztmk.punchouttm  
              vasort.estout    = if ztmk.punchoutdt = ? then
                                   "*"
                                 else
                                   " " 
              vasort.ttime     = t-time  
              vasort.miltime   = prt-time
              vasort.days      = prt-days
              vasort.number    = v-custno
              vasort.name      = p-name
              vasort.partno    = p-partno
              vasort.slsrep    = v-slsrep
              vasort.whse      = v-whse  
              vasort.tdn       = s-tdn
              vasort.totalt    = s-tottime2
              vasort.totesthm  = tot-esthm
              vasort.ztmkrecid = recid(ztmk).
              
      end. 

      if last-of (ztmk.user1) and
         p-includelaborfl then 
        run Include_Labor_Transactions.
        
       
       assign prt-days = 0.
       
   end. /* for each ztmk  */
   
   
end.  /* big loop */

/*-----------------------------------------------------------------------*/
procedure Create_All_Transactions:
/*-----------------------------------------------------------------------*/


   
for each b2-ztmk use-index k-ztmk-sc2 where
         b2-ztmk.cono      = g-cono       and 
         b2-ztmk.ordertype = "sc"         and 
         b2-ztmk.user1     = ztmk.user1   
       /*  recid(b2-ztmk) <> recid(ztmk) */
         no-lock
         break by b2-ztmk.cono
                  by b2-ztmk.user1
                  by b2-ztmk.stagecd
                  by b2-ztmk.punchindt
                  by b2-ztmk.punchintm:



       
       find first icsd where
                  icsd.cono  = b2-ztmk.cono    and 
                  icsd.whse  = entry(1,b2-ztmk.user1,"-")
                  no-lock no-error.
       find first vasp where 
            vasp.cono     = 01         and 
            vasp.shipprod = b2-ztmk.user1 and 
            vasp.whse     = entry(1,b2-ztmk.user1,"-")          
            no-lock no-error.
       
/*       
       assign v-esth = int(substring(vasp.user4,50,2))
              v-estm = int(substring(vasp.user4,52,2)).
*/
       if b2-ztmk.stagecd = 3 then              
         assign v-esth  = int(substring(vasp.user4,50,2))    
                v-estm  = int(substring(vasp.user4,52,2)).    
       else
       if b2-ztmk.stagecd = 10 then              
         assign v-esth  = int(substring(vasp.user4,54,2))  
                v-estm  = int(substring(vasp.user4,56,2)).  
       else
       if b2-ztmk.stagecd = 11 then              
          assign v-esth  = int(substring(vasp.user4,58,2))  
                 v-estm  = int(substring(vasp.user4,60,2)).  
       else       
          assign v-esth = 0
                 v-estm = 0.
        
       assign tot-esthm = (v-esth * 60) + v-estm.

       
       find icsp where icsp.cono = 1 and
                       icsp.prod = b2-ztmk.user1
                       no-lock no-error.
                       
         if avail icsp then 
            assign v-prodcat = icsp.prodcat.
       if b2-ztmk.punchoutdt = ? then
         assign t-days      = today - b2-ztmk.punchindt  
                t-xdays     = t-days 
                t-tmout     = (dec(substring(string(time,"hh:mm"),1,2)) * 60) +
                               dec(substring(string(time,"hh:mm"),4,2))
                t-tmin      = (dec(substring(b2-ztmk.punchintm,1,2)) * 60) + 
                               dec(substring(b2-ztmk.punchintm,3,2))
                t-time      = if t-days = 0 then 
                                t-tmout - t-tmin
                              else 
                                (t-tmout - t-tmin) + (t-days * (24 * 60)).
       else       
       assign t-days      = b2-ztmk.punchoutdt - b2-ztmk.punchindt  
              t-xdays     = t-days 
              t-tmout     = (dec(substring(b2-ztmk.punchouttm,1,2)) * 60) +
                             dec(substring(b2-ztmk.punchouttm,3,2))
              t-tmin      = (dec(substring(b2-ztmk.punchintm,1,2)) * 60) + 
                             dec(substring(b2-ztmk.punchintm,3,2))
              t-time      = if t-days = 0 then 
                             t-tmout - t-tmin
                            else 
                            (t-tmout - t-tmin) + (t-days * (24 * 60)).
       assign v-whse      = entry(1,b2-ztmk.user1,"-").
       assign t-ordqty    = t-ordqty  + 1
              t-dordqty   = t-dordqty + 1
              t-tordqty   = t-tordqty + 1
              t-sordqty   = t-sordqty + 1
              t-wordqty   = t-wordqty + 1
              t-gordqty   = t-gordqty + 1.
       assign v-operinit = b2-ztmk.operinit
              v-techid   = b2-ztmk.techid.
              
       find first arsc where
                  arsc.cono = g-cono and 
                  arsc.custno = dec(string(vasp.user5,"999999999999"))
            no-lock no-error.
       if avail arsc then 
        assign p-name = arsc.name
               v-shipto = arsc.shipto
               v-custno = arsc.custno
               v-slsrep = arsc.slsrepout.
       else 
        assign p-name = "". 
       find first vasp where 
                  vasp.cono     = 01     and 
                  vasp.shipprod = b2-ztmk.user1        
         no-lock no-error.
       assign p-partno  = if avail vasp then 
                           vasp.refer 
                          else 
                           "" 
              d-wononbr = entry(1,b2-ztmk.user1,"-") + "-" + 
                left-trim(entry(2,b2-ztmk.user1,"-"),"0")
              v-stgprt  = if b2-ztmk.stagecd = 0 then 
                              v-stage2[1]
                          else    
                          if b2-ztmk.stagecd = 5 or
                             b2-ztmk.stagecd  = 6 then 
                     /*  ask jen "ToBe" + "-" +  */ v-stage2[b2-ztmk.stagecd]
                          else 
                            v-stage2[b2-ztmk.stagecd]
              dtl-time   = t-time.              /* (t-time / 60).  */           

              if dtl-time > 0 then do:                     

        run formattime(input dtl-time, 
                      output t-min, 
                      output t-hrs, 
                      output t-xdays).
       end.    

       assign tstg-time  = tstg-time + dtl-time
              date-time  = date-time + dtl-time
              tgtl-time  = tgtl-time + dtl-time
              prt-days   = t-xdays
              t-ftl-days = t-ftl-days + t-xdays
              t-stg-days = t-stg-days + t-xdays
              t-dte-days = t-dte-days + t-xdays. 

       assign substring(prt-time,1,3) = string(t-hrs,"zz9")
              substring(prt-time,4,2) = string(t-min,"99")
              dtl-time = 0 
              t-hrs    = 0 
              t-min    = 0.


       run xsdioetlt.p (input-output v-slsrep,
                        input        v-prodcat,
                        input        v-custno,
                        input        v-shipto).


/*
      
      if s-asyflag and b2-ztmk.stagecd = 10 then do:
         run Accumulate_Asy2 (output s-tdn,
                             output s-tottime2,
                             buffer b2-ztmk).
      end.
       
      if s-tstflag and b2-ztmk.stagecd = 11 then do:
        run Accumulate_tst2 (output  s-tdn,
                            output s-tottime2,   
                            buffer b2-ztmk).  
      end. 
       
      
      if s-tdnflag and b2-ztmk.stagecd <> 10 and  b2-ztmk.stagecd <> 11 then do:
        run Accumulate_teardown2  (output  s-tdn,
                                  output s-tottime2,
                                  buffer b2-ztmk).  
      end. 

*/

     if b2-ztmk.stagecd <> 11 and b2-ztmk.stagecd <> 10 
       and b2-ztmk.stagecd <> 3 then  
        assign substring(s-tdn,1,3)  = "  0"
                substring(s-tdn,4,2)  = "00".
    else                                
        assign s-tdn = prt-time.
 
     if b2-ztmk.stagecd = 11 or b2-ztmk.stagecd = 10 or b2-ztmk.stagecd = 3 
     then  
        assign s-tottime2 = t-time. 
     else 
        assign s-tottime2 = 0.
        

       
       create vasort.
       assign vasort.operinit  = v-operinit
              vasort.repair    = d-wononbr
              vasort.sortdtl   = string(b2-ztmk.user1,"x(12)") +
                                 string(b2-ztmk.stagecd,"99") +
                                 string(b2-ztmk.punchindt,"99/99/9999") +
                                 string(b2-ztmk.punchintm,"x(8)") +
                                 string(recid(b2-ztmk),"999999999999") 

              vasort.stage     = v-stgprt
              vasort.stagecd   = b2-ztmk.stagecd
              vasort.indate    = b2-ztmk.punchindt
              vasort.tech      = v-techid
              vasort.esth      = v-esth
              vasort.estm      = v-estm
              vasort.intime    = b2-ztmk.punchintm
              vasort.outdate   = if b2-ztmk.punchoutdt = ? then
                                   today
                                 else
                                   b2-ztmk.punchoutdt
              vasort.outtime   = if b2-ztmk.punchoutdt = ? then
                                   substring(string(time,"hh:mm"),1,2) +
                                   substring(string(time,"hh:mm"),4,2) 
                                 else
                                   b2-ztmk.punchouttm  
              vasort.estout    = if b2-ztmk.punchoutdt = ? then
                                   "*"
                                 else
                                   " " 
              vasort.ttime     = t-time  
              vasort.miltime   = prt-time
              vasort.days      = prt-days
              vasort.number    = v-custno
              vasort.name      = p-name
              vasort.partno    = p-partno
              vasort.slsrep    = v-slsrep
              vasort.whse      = v-whse  
              vasort.tdn       = s-tdn
              vasort.totalt    = s-tottime2
              vasort.totesthm  = tot-esthm
              vasort.ztmkrecid = recid(b2-ztmk).
  
       assign prt-days = 0.
   end. /* for each ztmk  */
   
   
end.  /* big loop */

/*-----------------------------------------------------------------------*/
procedure Include_Labor_Transactions:
/*-----------------------------------------------------------------------*/


/* Don't perform if there were not any of the repair included
   to add the labor items to.
*/   
   
if not can-find(first vasort  use-index k-repair where 
                      vasort.repair = entry(1,ztmk.user1,"-") + "-" + 
                                      left-trim(entry(2,ztmk.user1,"-"),"0")
                no-lock) then
  leave.          
    
for each b2-ztmk use-index k-ztmk-sc2 where
         b2-ztmk.cono      = g-cono       and 
         b2-ztmk.ordertype = "sc"         and 
         b2-ztmk.user1     = ztmk.user1   and
         (b2-ztmk.stagecd  = 3 or
          b2-ztmk.stagecd = 10 or
          b2-ztmk.stagecd = 11) and   
         recid(b2-ztmk) <> recid(ztmk) 
         no-lock
         break by b2-ztmk.cono
                  by b2-ztmk.user1
                  by b2-ztmk.stagecd
                  by b2-ztmk.punchindt
                  by b2-ztmk.punchintm:

       if can-find(vasort use-index k-ztmkrec where 
                   vasort.ztmkrecid = recid(b2-ztmk) no-lock) then
         next.          

       find first icsd where
                  icsd.cono  = b2-ztmk.cono    and 
                  icsd.whse  = entry(1,b2-ztmk.user1,"-")
                  no-lock no-error.
       find first vasp where 
            vasp.cono     = 01         and 
            vasp.shipprod = b2-ztmk.user1 and 
            vasp.whse     = entry(1,b2-ztmk.user1,"-")          
            no-lock no-error.
       
/*       
       assign v-esth = int(substring(vasp.user4,50,2))
              v-estm = int(substring(vasp.user4,52,2)).
*/
       if b2-ztmk.stagecd = 3 then              
         assign v-esth  = int(substring(vasp.user4,50,2))    
                v-estm  = int(substring(vasp.user4,52,2)).    
       else
       if b2-ztmk.stagecd = 10 then              
         assign v-esth  = int(substring(vasp.user4,54,2))  
                v-estm  = int(substring(vasp.user4,56,2)).  
       else
       if b2-ztmk.stagecd = 11 then              
          assign v-esth  = int(substring(vasp.user4,58,2))  
                 v-estm  = int(substring(vasp.user4,60,2)).  
       else       
          assign v-esth = 0
                 v-estm = 0.
        
       assign tot-esthm = (v-esth * 60) + v-estm.

       
       find icsp where icsp.cono = 1 and
                       icsp.prod = b2-ztmk.user1
                       no-lock no-error.
                       
         if avail icsp then 
            assign v-prodcat = icsp.prodcat.
       if b2-ztmk.punchoutdt = ? then
         assign t-days      = today - b2-ztmk.punchindt  
                t-xdays     = t-days 
                t-tmout     = (dec(substring(string(time,"hh:mm"),1,2)) * 60) +
                               dec(substring(string(time,"hh:mm"),4,2))
                t-tmin      = (dec(substring(b2-ztmk.punchintm,1,2)) * 60) + 
                               dec(substring(b2-ztmk.punchintm,3,2))
                t-time      = if t-days = 0 then 
                                t-tmout - t-tmin
                              else 
                                (t-tmout - t-tmin) + (t-days * (24 * 60)).
       else       
         assign t-days      = b2-ztmk.punchoutdt - b2-ztmk.punchindt  
                t-xdays     = t-days 
                t-tmout     = (dec(substring(b2-ztmk.punchouttm,1,2)) * 60) +
                               dec(substring(b2-ztmk.punchouttm,3,2))
                t-tmin      = (dec(substring(b2-ztmk.punchintm,1,2)) * 60) + 
                               dec(substring(b2-ztmk.punchintm,3,2))
                t-time      = if t-days = 0 then 
                               t-tmout - t-tmin
                              else 
                              (t-tmout - t-tmin) + (t-days * (24 * 60)).
       assign v-whse      = entry(1,b2-ztmk.user1,"-").
       assign t-ordqty    = t-ordqty  + 1
              t-dordqty   = t-dordqty + 1
              t-tordqty   = t-tordqty + 1
              t-sordqty   = t-sordqty + 1
              t-wordqty   = t-wordqty + 1
              t-gordqty   = t-gordqty + 1.
       assign v-operinit = b2-ztmk.operinit
              v-techid   = b2-ztmk.techid.
              
       find first arsc where
                  arsc.cono = g-cono and 
                  arsc.custno = dec(string(vasp.user5,"999999999999"))
            no-lock no-error.
       if avail arsc then 
        assign p-name = arsc.name
               v-shipto = arsc.shipto
               v-custno = arsc.custno
               v-slsrep = arsc.slsrepout.
       else 
        assign p-name = "". 
       find first vasp where 
                  vasp.cono     = 01     and 
                  vasp.shipprod = b2-ztmk.user1        
         no-lock no-error.
       assign p-partno  = if avail vasp then 
                           vasp.refer 
                          else 
                           "" 
              d-wononbr = entry(1,b2-ztmk.user1,"-") + "-" + 
                left-trim(entry(2,b2-ztmk.user1,"-"),"0")
              v-stgprt  = if b2-ztmk.stagecd = 0 then 
                              v-stage2[1]
                          else    
                          if b2-ztmk.stagecd = 5 or
                             b2-ztmk.stagecd  = 6 then 
                     /*  ask jen "ToBe" + "-" +  */ v-stage2[b2-ztmk.stagecd]
                          else 
                            v-stage2[b2-ztmk.stagecd]
              dtl-time   = t-time.              /* (t-time / 60).  */           

              if dtl-time > 0 then do:                     

        run formattime(input dtl-time, 
                      output t-min, 
                      output t-hrs, 
                      output t-xdays).
       end.    

       assign tstg-time  = tstg-time + dtl-time
              date-time  = date-time + dtl-time
              tgtl-time  = tgtl-time + dtl-time
              prt-days   = t-xdays
              t-ftl-days = t-ftl-days + t-xdays
              t-stg-days = t-stg-days + t-xdays
              t-dte-days = t-dte-days + t-xdays. 

       assign substring(prt-time,1,3) = string(t-hrs,"zz9")
              substring(prt-time,4,2) = string(t-min,"99")
              dtl-time = 0 
              t-hrs    = 0 
              t-min    = 0.


       run xsdioetlt.p (input-output v-slsrep,
                        input        v-prodcat,
                        input        v-custno,
                        input        v-shipto).


/*
      
      if s-asyflag and b2-ztmk.stagecd = 10 then do:
         run Accumulate_Asy2 (output s-tdn,
                             output s-tottime2,
                             buffer b2-ztmk).
      end.
       
      if s-tstflag and b2-ztmk.stagecd = 11 then do:
        run Accumulate_tst2 (output  s-tdn,
                            output s-tottime2,   
                            buffer b2-ztmk).  
      end. 
       
      
      if s-tdnflag and b2-ztmk.stagecd <> 10 and  b2-ztmk.stagecd <> 11 then do:
        run Accumulate_teardown2  (output  s-tdn,
                                  output s-tottime2,
                                  buffer b2-ztmk).  
      end. 

*/

     if b2-ztmk.stagecd <> 11 and b2-ztmk.stagecd <> 10 
       and b2-ztmk.stagecd <> 3 then  
        assign substring(s-tdn,1,3)  = "  0"
                substring(s-tdn,4,2)  = "00".
    else                                
        assign s-tdn = prt-time.
 
     if b2-ztmk.stagecd = 11 or b2-ztmk.stagecd = 10 or b2-ztmk.stagecd = 3 
     then  
        assign s-tottime2 = t-time. 
     else 
        assign s-tottime2 = 0.
        

       
       create vasort.
       assign vasort.operinit  = v-operinit
              vasort.repair    = d-wononbr
              vasort.sortdtl   = string(b2-ztmk.user1,"x(12)") +
                                 string(b2-ztmk.stagecd,"99") +
                                 string(b2-ztmk.punchindt,"99/99/9999") +
                                 string(b2-ztmk.punchintm,"x(8)") + 
                                 string(recid(b2-ztmk),"999999999999") 

       
              vasort.stage     = v-stgprt
              vasort.stagecd   = b2-ztmk.stagecd
              vasort.indate    = b2-ztmk.punchindt
              vasort.tech      = v-techid
              vasort.esth      = v-esth
              vasort.estm      = v-estm
              vasort.intime    = b2-ztmk.punchintm
              vasort.outdate   = if b2-ztmk.punchoutdt = ? then
                                   today
                                 else
                                   b2-ztmk.punchoutdt
              vasort.outtime   = if b2-ztmk.punchoutdt = ? then
                                   substring(string(time,"hh:mm"),1,2) +
                                   substring(string(time,"hh:mm"),4,2) 
                                 else
                                   b2-ztmk.punchouttm  
              vasort.estout    = if b2-ztmk.punchoutdt = ? then
                                   "*"
                                 else
                                   " " 
 
              vasort.ttime     = t-time  
              vasort.miltime   = prt-time
              vasort.days      = prt-days
              vasort.number    = v-custno
              vasort.name      = p-name
              vasort.partno    = p-partno
              vasort.slsrep    = v-slsrep
              vasort.whse      = v-whse  
              vasort.tdn       = s-tdn
              vasort.totalt    = s-tottime2
              vasort.totesthm  = tot-esthm
              vasort.ztmkrecid = recid(b2-ztmk).
  
       assign prt-days = 0.
   end. /* for each ztmk  */
   
   
end.  /* big loop */


/*-----------------------------------------------------------------------*/
procedure Accumulate_teardown:
/*-----------------------------------------------------------------------*/

/*def output parameter ip-tdntime as char format "999:99" no-undo.  */
def output parameter ip-tottime as integer  no-undo.
def parameter buffer ib-ztmk for ztmk.

def var lt-days   as integer no-undo.
def var lt-tmout  as integer no-undo.
def var lt-tmin   as integer no-undo.
def var lt-time   as integer no-undo.
def var lt-min    as integer no-undo init 0.
def var lt-hrs    as integer no-undo init 0.
def var lt-xdays  as integer no-undo init 0.
def var lt-totaltime   as integer no-undo init 0.

   
for each b-ztmk use-index k-ztmk-sc2 where
         b-ztmk.cono      = g-cono       and 
         b-ztmk.ordertype = "sc"         and 
         b-ztmk.user1     = ib-ztmk.user1  /* and
         b-ztmk.stagecd = 3 /* Teardown */   */
         no-lock:

  assign lt-hrs    = 0 
         lt-min    = 0.
 
       assign lt-days      =  b-ztmk.punchoutdt - b-ztmk.punchindt  
              lt-tmout     = (dec(substring(b-ztmk.punchouttm,1,2)) * 60) +
                             dec(substring(b-ztmk.punchouttm,3,2))
              lt-tmin      = (dec(substring(b-ztmk.punchintm,1,2)) * 60) + 
                             dec(substring(b-ztmk.punchintm,3,2))
              lt-time      = if lt-days = 0 then 
                               lt-tmout - lt-tmin
                            else 
                              (lt-tmout - lt-tmin) + (lt-days * (24 * 60)).
  assign lt-totaltime = lt-time + lt-totaltime.
end.      /* For each */

if lt-totaltime > 0 then do:                     
   run formattime(input lt-totaltime,
                  output lt-min, 
                  output lt-hrs, 
                  output lt-xdays).
end.      
/* 
assign substring(ip-tdntime,1,3) = string(lt-hrs,"zz9")
       substring(ip-tdntime,4,2) = string(lt-min,"99").
*/
assign ip-tottime = ip-tottime + lt-totaltime.       
 
end. /* Procedure */


/*-----------------------------------------------------------------------*/
procedure Accumulate_Asy:
/*-----------------------------------------------------------------------*/

/* def output parameter ip-tdntime as char format "999:99" no-undo. */
def output parameter ip-tottime as integer no-undo.
def parameter buffer ib-ztmk for ztmk.
def var lt-days   as integer no-undo.
def var lt-tmout  as integer no-undo.
def var lt-tmin   as integer no-undo.
def var lt-time   as integer no-undo.
def var lt-min    as integer no-undo init 0.
def var lt-hrs    as integer no-undo init 0.
def var lt-xdays  as integer no-undo init 0.
def var lt-totaltime   as integer no-undo init 0.

   
for each b-ztmk use-index k-ztmk-sc2 where
         b-ztmk.cono      = g-cono       and 
         b-ztmk.ordertype = "sc"         and 
         b-ztmk.user1     = ib-ztmk.user1   and
         b-ztmk.stagecd = 10 /* Asy */  
         no-lock:
  assign lt-hrs    = 0 
         lt-min    = 0.
         
       assign lt-days      =  b-ztmk.punchoutdt - b-ztmk.punchindt  
              lt-tmout     = (dec(substring(b-ztmk.punchouttm,1,2)) * 60) +
                             dec(substring(b-ztmk.punchouttm,3,2))
              lt-tmin      = (dec(substring(b-ztmk.punchintm,1,2)) * 60) + 
                             dec(substring(b-ztmk.punchintm,3,2))
              lt-time      = if lt-days = 0 then 
                               lt-tmout - lt-tmin
                            else 
                              (lt-tmout - lt-tmin) + (lt-days * (24 * 60)).
  assign lt-totaltime = lt-time + lt-totaltime.
end.      /* For each */

if lt-totaltime > 0 then do:                     
   run formattime(input lt-totaltime, 
                  output lt-min, 
                  output lt-hrs, 
                  output lt-xdays).
end.      
/*
assign substring(ip-tdntime,1,3) = string(lt-hrs,"zz9")
       substring(ip-tdntime,4,2) = string(lt-min,"99").
*/       
assign ip-tottime = ip-tottime + lt-totaltime.       
       
 
end. /* Procedure */



/*-----------------------------------------------------------------------*/
procedure Accumulate_tst:
/*-----------------------------------------------------------------------*/

/* def output parameter ip-tdntime as char format "999:99" no-undo.      */
def output parameter ip-tottime as integer no-undo.
def parameter buffer ib-ztmk for ztmk.
def var lt-days   as integer no-undo.
def var lt-tmout  as integer no-undo.
def var lt-tmin   as integer no-undo.
def var lt-time   as integer no-undo.
def var lt-min    as integer no-undo init 0.
def var lt-hrs    as integer no-undo init 0.
def var lt-xdays  as integer no-undo init 0.
def var lt-totaltime   as integer no-undo init 0.

   
for each b-ztmk use-index k-ztmk-sc2 where
         b-ztmk.cono      = g-cono       and 
         b-ztmk.ordertype = "sc"         and 
         b-ztmk.user1     = ib-ztmk.user1   and
         b-ztmk.stagecd = 11 /* tst */  
         no-lock:

  assign lt-hrs    = 0 
         lt-min    = 0.
         
       assign lt-days      =  b-ztmk.punchoutdt - b-ztmk.punchindt  
              lt-tmout     = (dec(substring(b-ztmk.punchouttm,1,2)) * 60) +
                             dec(substring(b-ztmk.punchouttm,3,2))
              lt-tmin      = (dec(substring(b-ztmk.punchintm,1,2)) * 60) + 
                             dec(substring(b-ztmk.punchintm,3,2))
              lt-time      = if lt-days = 0 then 
                               lt-tmout - lt-tmin
                            else 
                              (lt-tmout - lt-tmin) + (lt-days * (24 * 60)).
  assign lt-totaltime = lt-time + lt-totaltime.
end.      /* For each */

if lt-totaltime > 0 then do:                     
   run formattime(input lt-totaltime, 
                  output lt-min, 
                  output lt-hrs, 
                  output lt-xdays).
end.      
/*
assign substring(ip-tdntime,1,3) = string(lt-hrs,"zz9")
       substring(ip-tdntime,4,2) = string(lt-min,"99").
*/       
assign ip-tottime = ip-tottime + lt-totaltime.       
       
 
end. /* Procedure */


/*-----------------------------------------------------------------------*/
procedure Accumulate_teardown2:
/*-----------------------------------------------------------------------*/

/* def output parameter ip-tdntime as char format "999:99" no-undo. */
def output parameter ip-tottime as integer  no-undo.
def parameter buffer ib-ztmk for ztmk.

def var lt-days   as integer no-undo.
def var lt-tmout  as integer no-undo.
def var lt-tmin   as integer no-undo.
def var lt-time   as integer no-undo.
def var lt-min    as integer no-undo init 0.
def var lt-hrs    as integer no-undo init 0.
def var lt-xdays  as integer no-undo init 0.
def var lt-totaltime   as integer no-undo init 0.

   
for each b-ztmk use-index k-ztmk-sc2 where
         b-ztmk.cono      = g-cono       and 
         b-ztmk.ordertype = "sc"         and 
         b-ztmk.user1     = ib-ztmk.user1   and
         b-ztmk.stagecd = 3 /* Teardown */ 
         no-lock:

  assign lt-hrs    = 0 
         lt-min    = 0.
 
       assign lt-days      =  b-ztmk.punchoutdt - b-ztmk.punchindt  
              lt-tmout     = (dec(substring(b-ztmk.punchouttm,1,2)) * 60) +
                             dec(substring(b-ztmk.punchouttm,3,2))
              lt-tmin      = (dec(substring(b-ztmk.punchintm,1,2)) * 60) + 
                             dec(substring(b-ztmk.punchintm,3,2))
              lt-time      = if lt-days = 0 then 
                               lt-tmout - lt-tmin
                            else 
                              (lt-tmout - lt-tmin) + (lt-days * (24 * 60)).
  assign lt-totaltime = lt-time + lt-totaltime.
end.      /* For each */

if lt-totaltime > 0 then do:                     
   run formattime(input lt-totaltime,
                  output lt-min, 
                  output lt-hrs, 
                  output lt-xdays).
end.      
/*
assign substring(ip-tdntime,1,3) = string(lt-hrs,"zz9")
       substring(ip-tdntime,4,2) = string(lt-min,"99").
*/
assign ip-tottime = ip-tottime + lt-totaltime.       
 
end. /* Procedure */


/*-----------------------------------------------------------------------*/
procedure Accumulate_Asy2:
/*-----------------------------------------------------------------------*/

/* def output parameter ip-tdntime as char format "999:99" no-undo.      */
def output parameter ip-tottime as integer no-undo.
def parameter buffer ib-ztmk for ztmk.
def var lt-days   as integer no-undo.
def var lt-tmout  as integer no-undo.
def var lt-tmin   as integer no-undo.
def var lt-time   as integer no-undo.
def var lt-min    as integer no-undo init 0.
def var lt-hrs    as integer no-undo init 0.
def var lt-xdays  as integer no-undo init 0.
def var lt-totaltime   as integer no-undo init 0.

   
for each b-ztmk use-index k-ztmk-sc2 where
         b-ztmk.cono      = g-cono       and 
         b-ztmk.ordertype = "sc"         and 
         b-ztmk.user1     = ib-ztmk.user1   and
         b-ztmk.stagecd = 10 /* Asy */  
         no-lock:

  assign lt-hrs    = 0 
         lt-min    = 0.
         
       assign lt-days      =  b-ztmk.punchoutdt - b-ztmk.punchindt  
              lt-tmout     = (dec(substring(b-ztmk.punchouttm,1,2)) * 60) +
                             dec(substring(b-ztmk.punchouttm,3,2))
              lt-tmin      = (dec(substring(b-ztmk.punchintm,1,2)) * 60) + 
                             dec(substring(b-ztmk.punchintm,3,2))
              lt-time      = if lt-days = 0 then 
                               lt-tmout - lt-tmin
                            else 
                              (lt-tmout - lt-tmin) + (lt-days * (24 * 60)).
  assign lt-totaltime = lt-time + lt-totaltime.
end.      /* For each */

if lt-totaltime > 0 then do:                     
   run formattime(input lt-totaltime, 
                  output lt-min, 
                  output lt-hrs, 
                  output lt-xdays).
end.      
/*
assign substring(ip-tdntime,1,3) = string(lt-hrs,"zz9")
       substring(ip-tdntime,4,2) = string(lt-min,"99").
*/       
assign ip-tottime = ip-tottime + lt-totaltime.       
       
 
end. /* Procedure */



/*-----------------------------------------------------------------------*/
procedure Accumulate_tst2:
/*-----------------------------------------------------------------------*/

/* def output parameter ip-tdntime as char format "999:99" no-undo. */
def output parameter ip-tottime as integer no-undo.
def parameter buffer ib-ztmk for ztmk.

def var lt-days   as integer no-undo.
def var lt-tmout  as integer no-undo.
def var lt-tmin   as integer no-undo.
def var lt-time   as integer no-undo.
def var lt-min    as integer no-undo init 0.
def var lt-hrs    as integer no-undo init 0.
def var lt-xdays  as integer no-undo init 0.
def var lt-totaltime   as integer no-undo init 0.

   
for each b-ztmk use-index k-ztmk-sc2 where
         b-ztmk.cono      = g-cono       and 
         b-ztmk.ordertype = "sc"         and 
         b-ztmk.user1     = ib-ztmk.user1   and
         b-ztmk.stagecd = 11 /* tst */  
         no-lock:

  assign lt-hrs    = 0 
         lt-min    = 0.
         
       assign lt-days      =  b-ztmk.punchoutdt - b-ztmk.punchindt  
              lt-tmout     = (dec(substring(b-ztmk.punchouttm,1,2)) * 60) +
                             dec(substring(b-ztmk.punchouttm,3,2))
              lt-tmin      = (dec(substring(b-ztmk.punchintm,1,2)) * 60) + 
                             dec(substring(b-ztmk.punchintm,3,2))
              lt-time      = if lt-days = 0 then 
                               lt-tmout - lt-tmin
                            else 
                              (lt-tmout - lt-tmin) + (lt-days * (24 * 60)).
  assign lt-totaltime = lt-time + lt-totaltime.
end.      /* For each */

if lt-totaltime > 0 then do:                     
   run formattime(input lt-totaltime, 
                  output lt-min, 
                  output lt-hrs, 
                  output lt-xdays).
end.      
/*
assign substring(ip-tdntime,1,3) = string(lt-hrs,"zz9")
       substring(ip-tdntime,4,2) = string(lt-min,"99").
*/       
assign ip-tottime = ip-tottime + lt-totaltime.       
       
 
end. /* Procedure */



/*-------------------------------------------------------------------------- */
procedure rcv-flg: 
/*-------------------------------------------------------------------------- */

do l-date = b-invdate to e-invdate: 
   for each ztmk use-index k-ztmk-sc2 where
            ztmk.cono      = g-cono       and 
            ztmk.ordertype = "sc"         and 
            ztmk.statuscd   = 0           and 
           (ztmk.user1     >= entry (1,b-tagno,"-") + "-" +
             string(int(entry(2,b-tagno,"-")),"9999999") and 
            ztmk.user1     <= entry (1,e-tagno,"-") + "-" +
             string(int(entry(2,e-tagno,"-")),"9999999")) and   
            ztmk.stagecd    = 1           and 
           (ztmk.techid    >= b-techid    and 
            ztmk.techid    <= e-techid)   and 
           (ztmk.operinit  >= b-oper      and
            ztmk.operinit  <= e-oper)     and
          ((ztmk.punchindt >= b-invdate   and 
            ztmk.punchindt <= e-invdate)  and 
            ztmk.punchoutdt = ?)           
            no-lock              
            break by ztmk.cono 
                  by ztmk.user1
                  by ztmk.stagecd
                  by ztmk.punchindt
                  by ztmk.punchintm:
       
       find first icsd where
                  icsd.cono  = ztmk.cono    and 
                  icsd.whse  = entry(1,ztmk.user1,"-")
                  no-lock no-error.
       if avail icsd and 
         (icsd.whse < b-whse or 
          icsd.whse > e-whse) then do:
          
         if last-of (ztmk.user1) and
            p-includelaborfl then 
           run Include_Labor_Transactions.
         next.        
       end.
       if rcv-flg and (ztmk.stagecd ne 1) then do:
        
         if last-of (ztmk.user1) and
            p-includelaborfl then 
           run Include_Labor_Transactions.
 
         next.         
       end.
       
       find first vasp where 
            vasp.cono     = 01         and 
            vasp.shipprod = ztmk.user1 and 
            vasp.whse     = entry(1,ztmk.user1,"-")          
            no-lock no-error.
/*
       assign v-esth = int(substring(vasp.user4,50,2))
              v-estm = int(substring(vasp.user4,52,2)).
*/
       if ztmk.stagecd = 3 then              
         assign v-esth =  int(substring(vasp.user4,50,2))    
                v-estm =  int(substring(vasp.user4,52,2)).    
       else
       if ztmk.stagecd = 10 then              
         assign v-esth =  int(substring(vasp.user4,54,2))  
                v-estm =  int(substring(vasp.user4,56,2)).  
       else
       if ztmk.stagecd = 11 then              
          assign v-esth =  int(substring(vasp.user4,58,2))  
                 v-estm =  int(substring(vasp.user4,60,2)).  
       else       
          assign v-esth = 0
                 v-estm = 0.
       assign tot-esthm = (v-esth * 60) + v-estm.
       
       if ztmk.stagecd   < b-istage or
          ztmk.stagecd   > e-istage then do:
          
         if last-of (ztmk.user1) and
            p-includelaborfl then 
           run Include_Labor_Transactions.
 
         next. 
       end.
       
       find icsp where icsp.cono = 1 and
                       icsp.prod = ztmk.user1
                       no-lock no-error.
                       
         if avail icsp then 
            assign v-prodcat = icsp.prodcat.


/* here */

       if ztmk.punchoutdt = ? then
         assign t-days      = today - ztmk.punchindt  
                t-xdays     = t-days 
                t-tmout     = (dec(substring(string(time,"hh:mm"),1,2)) * 60) +
                               dec(substring(string(time,"hh:mm"),4,2))
                t-tmin      = (dec(substring(ztmk.punchintm,1,2)) * 60) + 
                               dec(substring(ztmk.punchintm,3,2))
                t-time      = if t-days = 0 then 
                                t-tmout - t-tmin
                              else 
                                (t-tmout - t-tmin) + (t-days * (24 * 60)).
 
       
       else       
         assign t-days      = ztmk.punchoutdt - ztmk.punchindt  
                t-xdays     = t-days 
                t-tmout     = (dec(substring(ztmk.punchouttm,1,2)) * 60) +
                               dec(substring(ztmk.punchouttm,3,2))
                t-tmin      = (dec(substring(ztmk.punchintm,1,2)) * 60) + 
                               dec(substring(ztmk.punchintm,3,2))
                t-time      = if t-days = 0 then 
                                t-tmout - t-tmin
                              else 
                                (t-tmout - t-tmin) + (t-days * (24 * 60)).
       assign v-whse      = entry(1,ztmk.user1,"-").
       assign t-ordqty    = t-ordqty  + 1
              t-dordqty   = t-dordqty + 1
              t-tordqty   = t-tordqty + 1
              t-sordqty   = t-sordqty + 1
              t-wordqty   = t-wordqty + 1
              t-gordqty   = t-gordqty + 1.
       assign v-operinit = ztmk.operinit
              v-techid   = ztmk.techid.
              
       find first arsc where
                  arsc.cono = g-cono and 
                  arsc.custno = dec(string(vasp.user5,"999999999999"))
            no-lock no-error.
       if avail arsc then 
        assign p-name = arsc.name
               v-shipto = arsc.shipto
               v-custno = arsc.custno
               v-slsrep = arsc.slsrepout.
       else 
        assign p-name = "". 
       find first vasp where 
                  vasp.cono     = 01     and 
                  vasp.shipprod = ztmk.user1        
         no-lock no-error.
       assign p-partno  = if avail vasp then 
                           vasp.refer 
                          else 
                           "" 
              d-wononbr = entry(1,ztmk.user1,"-") + "-" + 
                left-trim(entry(2,ztmk.user1,"-"),"0")
              v-stgprt  = if ztmk.stagecd = 0 then 
                              v-stage2[1]
                          else    
                          if ztmk.stagecd = 5 or
                             ztmk.stagecd  = 6 then 
                     /*  ask jen "ToBe" + "-" +  */ v-stage2[ztmk.stagecd]
                          else 
                            v-stage2[ztmk.stagecd]
              dtl-time   = t-time.              /* (t-time / 60).  */           
       
       
       if dtl-time > 0 then do:                     
        run formattime(input dtl-time, 
                      output t-min, 
                      output t-hrs, 
                      output t-xdays).
       end.    

       assign tstg-time  = tstg-time + dtl-time
              date-time  = date-time + dtl-time
              tgtl-time  = tgtl-time + dtl-time
              prt-days   = t-xdays
              t-ftl-days = t-ftl-days + t-xdays
              t-stg-days = t-stg-days + t-xdays
              t-dte-days = t-dte-days + t-xdays. 

       assign substring(prt-time,1,3) = string(t-hrs,"zz9")
              substring(prt-time,4,2) = string(t-min,"99")
              dtl-time = 0 
              t-hrs    = 0 
              t-min    = 0.


       run xsdioetlt.p (input-output v-slsrep,
                        input        v-prodcat,
                        input        v-custno,
                        input        v-shipto).

     if b-am <> "" then
       if v-slsrep < b-am or
          v-slsrep > b-am then do:

       if last-of (ztmk.user1) and
          p-includelaborfl then 
         run Include_Labor_Transactions.
           
       next.
     end.
/*
     if s-asyflag and ztmk.stagecd = 10 then do:
        run Accumulate_Asy (/*output s-tdn, */
                            output s-tottime2,
                            buffer ztmk).
           
     end.
       

     if s-tstflag and ztmk.stagecd = 11 then do:
       run Accumulate_tst (/* output  s-tdn, */
                           output s-tottime2,   
                           buffer ztmk).  
     end. 
     
     if s-tdnflag and ztmk.stagecd <> 10 and ztmk.stagecd <> 11 then do:
        run Accumulate_teardown (/* output  s-tdn, */
                                 output s-tottime2,   
                                 buffer ztmk).  
     end. 
       
*/

    if ztmk.stagecd <> 11 and ztmk.stagecd <> 10 and ztmk.stagecd <> 3 then    ~        assign substring(s-tdn,1,3)  = "  0"
                substring(s-tdn,4,2)  = "00".
      else                                
         assign s-tdn = prt-time.
  
     if ztmk.stagecd = 11 or ztmk.stagecd = 10 or ztmk.stagecd = 3 then  
        assign  s-tottime2 = t-time. 
     else 
        assign s-tottime2 = 0.
        
       if cls-flag then 
         run Create_All_Transactions.
       else  
       if can-flag then
         run Create_All_Transactions.
       else do:
       create vasort.                       
       assign vasort.operinit  = v-operinit
              vasort.sortdtl   = string(ztmk.user1,"x(12)") +
                                 string(ztmk.stagecd,"99") +
                                 string(ztmk.punchindt,"99/99/9999") +
                                 string(ztmk.punchintm,"x(8)") +
                                 string(recid(ztmk),"999999999999") 
              vasort.repair    = d-wononbr
              vasort.stage     = v-stgprt
              vasort.stagecd   = ztmk.stagecd
              vasort.indate    = ztmk.punchindt
              vasort.tech      = v-techid
              vasort.esth      = v-esth
              vasort.estm      = v-estm
              vasort.intime    = ztmk.punchintm
              vasort.outdate   = if ztmk.punchoutdt = ? then
                                   today
                                 else
                                   ztmk.punchoutdt
              vasort.outtime   = if ztmk.punchoutdt = ? then
                                   substring(string(time,"hh:mm"),1,2) +
                                   substring(string(time,"hh:mm"),4,2) 
                                 else
                                   ztmk.punchouttm  
              vasort.estout    = if ztmk.punchoutdt = ? then
                                   "*"
                                 else
                                   " " 
              vasort.ttime     = t-time  
              vasort.miltime   = prt-time
              vasort.days      = prt-days
              vasort.number    = v-custno
              vasort.name      = p-name
              vasort.partno    = p-partno
              vasort.slsrep    = v-slsrep
              vasort.whse      = v-whse  
              vasort.tdn       = s-tdn
              vasort.totalt    = s-tottime2
              vasort.totesthm  = tot-esthm
              vasort.ztmkrecid = recid(ztmk).
              
      end. 

      if last-of (ztmk.user1) and
         p-includelaborfl then 
        run Include_Labor_Transactions.
        
       
       assign prt-days = 0.
       
   end. /* for each ztmk  */
   
 end.  
end.  /* big loop */


/* ///


       if ztmk.orderno > 0 then do: 
          find first vaeh where
                     vaeh.cono  = ztmk.cono    and 
                     vaeh.vano  = ztmk.orderno and 
                     vaeh.vasuf = ztmk.ordersuf 
                     no-lock no-error.
       if avail vaeh and 
         (vaeh.whse < b-whse or 
          vaeh.whse > e-whse) then 
          next.
       end. 
       else
       if entry(1,ztmk.user1,"-") < b-whse or 
          entry(1,ztmk.user1,"-") > e-whse then 
          next. 
       if rcv-flg and ztmk.stagecd ne 1 then 
          next.         
       find first vasp where 
            vasp.cono     = 01         and 
            vasp.shipprod = ztmk.user1 and 
            vasp.whse     = entry(1,ztmk.user1,"-")          
            no-lock no-error.
       if not avail vasp then 
        next.
       assign t-days      =  ztmk.punchoutdt - ztmk.punchindt  
              t-tmout     = (dec(substring(ztmk.punchouttm,1,2)) * 60) +
                             dec(substring(ztmk.punchouttm,3,2))
              t-tmin      = (dec(substring(ztmk.punchintm,1,2)) * 60) + 
                             dec(substring(ztmk.punchintm,3,2))
              t-time      = if t-days = 0 then 
                               t-tmout - t-tmin
                            else 
                              (t-tmout - t-tmin) + (t-days * (24 * 60)).
       assign t-ordqty    = t-ordqty  + 1
              t-dordqty   = t-dordqty + 1
              t-tordqty   = t-tordqty + 1
              t-sordqty   = t-sordqty + 1
              t-wordqty   = t-wordqty + 1
              t-gordqty   = t-gordqty + 1.
   
   /*
       if dec(vasp.user7) = 14 then do: 
        assign s-stageprt = "can". 
        next. 
       end. 
       else     */
        do inx2  = 1 to 16: 
        if inx2 = ztmk.stagecd then do: 
         assign s-stageprt = v-stage2[inx2].
         leave.
        end.
       end. 
       
      assign v-esth = int(substring(vasp.user4,50,2))
             v-estm = int(substring(vasp.user4,52,2)).
             
      assign tot-esthm = (v-esth * 60) + v-estm.

      
       assign v-operinit = ztmk.operinit
              v-techid   = ztmk.techid.
       
       find icsp where icsp.cono = 1 and
                       icsp.prod = ztmk.user1
                       no-lock no-error.
                       
         if avail icsp then 
            assign v-prodcat = icsp.prodcat.
       
       find first arsc where
                  arsc.cono = g-cono and 
                  arsc.custno = dec(string(vasp.user5,"999999999999"))
                  no-lock no-error.
       if avail arsc then 
        assign p-name = arsc.name
               v-shipto = arsc.shipto
               v-custno = arsc.custno
               v-slsrep = arsc.slsrepout.
       else 
        assign p-name = "". 
       assign p-partno  = if avail vasp then 
                           vasp.refer 
                          else 
                           "" 
              d-wononbr = entry(1,ztmk.user1,"-") + "-" +
                left-trim(entry(2,ztmk.user1,"-"),"0")
                dtl-time  = t-time
                v-stgprt  = if ztmk.stagecd = 0 then 
                              v-stage2[1]
                             else    
                             if ztmk.stagecd = 5 or
                                ztmk.stagecd  = 6 then 
                              "ToBe" + "-" + v-stage2[ztmk.stagecd]
                             else 
                               v-stage2[ztmk.stagecd].
       
       
       if dtl-time > 0 then do:                     
        run formattime(input dtl-time, 
                      output t-min, 
                      output t-hrs, 
                      output t-xdays).
       end.    

       
       assign tstg-time  = tstg-time + dtl-time
              date-time  = date-time + dtl-time
              prt-days   = t-xdays
              t-ftl-days = t-ftl-days + t-xdays
              t-stg-days = t-stg-days + t-xdays
              t-dte-days = t-dte-days + t-xdays. 

       assign substring(prt-time,1,3) = string(t-hrs,"zz9")
              substring(prt-time,4,2) = string(t-min,"99")
              dtl-time = 0 
              t-hrs    = 0 
              t-min    = 0.
       if last-of(ztmk.user1) then do: 
          assign d-wononbr = entry(1,ztmk.user1,"-") + "-" +
                             left-trim(entry(2,ztmk.user1,"-"),"0")
                 v-stgprt  = if ztmk.stagecd = 0 then 
                              v-stage2[1]
                             else    
                             if ztmk.stagecd = 5 or
                                ztmk.stagecd = 6 then 
                              "ToBe" + "-" + v-stage2[ztmk.stagecd]
                             else 
                               v-stage2[ztmk.stagecd].
          if tech-time > 0 then 
             assign t-hrs = t-hrs + truncate(tech-time / 1,0)
                    t-min = t-min + 
                          (60 * (tech-time - truncate(tech-time / 1,0))).
          else 
             assign t-hrs = 0              
                    t-min = 0.
          assign substring(dtl-tech-time,1,3) = string(t-hrs,"zz9")
                 substring(dtl-tech-time,4,2) = string(t-min,"99").
          if ztmk.techid = "admn" then 
           {w-smsn.i ztmk.operinit no-lock}
          else 
           {w-smsn.i ztmk.techid no-lock}.
          if ztmk.techid = "admn" then 
           assign v-operinit = ztmk.operinit. 
          else 
           assign v-operinit = ztmk.techid. 
          assign v-name = if avail smsn then
                           smsn.name
                          else 
                          if avail ztmk and ztmk.techid = "admn" then 
                           "Administrative Stages" 
                          else  
                           "Tech Name Notfound"
                 v-name = "".

        run xsdioetlt.p (input-output v-slsrep,
                         input        v-prodcat,
                         input        v-custno,
                         input        v-shipto).

          if b-am <> "" then               
             if v-slsrep < b-am or
                v-slsrep > e-am then
                next.         


    
       
/*       
       if s-asyflag and ztmk.stagecd = 10 then do:
        run Accumulate_Asy (output s-tdn,
                            output s-tottime2,
                            buffer ztmk).
       end.
       
       if s-tdnflag and ztmk.stagecd <> 10 then do:
        run Accumulate_teardown (output  s-tdn,
                                 output s-tottime2,   
                                 buffer ztmk).  
       end. 
*/
  


    if ztmk.stagecd <> 11 and ztmk.stagecd <> 10 and ztmk.stagecd <> 3 then            assign substring(s-tdn,1,3)  = "  0"
                substring(s-tdn,4,2)  = "00".
    else                                
      assign s-tdn = prt-time.
  
     if ztmk.stagecd = 11 or ztmk.stagecd = 10 or ztmk.stagecd = 3 then  
        assign  s-tottime2 = t-time. 
     else 
        assign s-tottime2 = 0.
        
    
       create vasort.
       assign vasort.operinit  = v-operinit
              vasort.repair    = d-wononbr
              vasort.sortdtl   = string(ztmk.user1,"x(12)") +
                                 string(ztmk.stagecd,"99") +
                                 string(ztmk.punchindt,"99/99/9999") +
                                 string(ztmk.punchintm,"x(8)") + 
                                 string(recid(ztmk),"999999999999") 

       
              vasort.stage     = v-stgprt
              vasort.stagecd   = ztmk.stagecd
              vasort.indate    = ztmk.punchindt
              vasort.tech      = v-techid
              vasort.esth      = v-esth
              vasort.estm      = v-estm
              vasort.intime    = ztmk.punchintm
              vasort.outdate   = ztmk.punchoutdt
              vasort.outtime   = ztmk.punchouttm  
              vasort.ttime     = t-time  
              vasort.miltime   = prt-time
              vasort.days      = prt-days
              vasort.name      = p-name
              vasort.number    = v-custno
              vasort.partno    = p-partno
              vasort.slsrep    = v-slsrep
              vasort.whse      = v-whse
              vasort.tdn       = s-tdn
              vasort.totalt    = s-tottime2
              vasort.totesthm  = tot-esthm
              vasort.ztmkrecid = recid(ztmk).
              
 
                  
       if cls-flag then 
         run Create_All_Transactions.
       if can-flag then
         run Create_All_Transactions.
     
          assign t-tordqty   = 0 
                 tech-time   = 0
                 t-hrs       = 0 
                 t-min       = 0.  
       end. 
       if last-of(ztmk.punchindt) then do: 
         assign tgtl-time = tgtl-time + date-time. 
       end. 
   end.  
 leave.
end.
end.
///////////// */ /*------------------------------------------------------------------------ */
procedure labor-stages: 
/*------------------------------------------------------------------------ */

do l-date = b-invdate to e-invdate: 
 
      for each ztmk use-index k-ztmk-sc1 where
            ztmk.cono      = g-cono       and 
            ztmk.ordertype = "sc"         and 
            ztmk.statuscd  = 5            and 
           (ztmk.stagecd = 3              or
            ztmk.stagecd =10              or
            ztmk.stagecd = 11 ) and
           (ztmk.user1     >= entry (1,b-tagno,"-") + "-" +
             string(int(entry(2,b-tagno,"-")),"9999999") and 
            ztmk.user1     <= entry (1,e-tagno,"-") + "-" +
             string(int(entry(2,e-tagno,"-")),"9999999")) and 
          ((ztmk.punchindt >= b-invdate    and 
            ztmk.punchindt <= e-invdate)  and 
            ztmk.punchoutdt ne ?)         and 
           (ztmk.techid    >= b-techid    and 
            ztmk.techid    <= e-techid)   and
           (ztmk.operinit  >= b-oper      and    
            ztmk.operinit  <= e-oper)     
            no-lock
            break by ztmk.cono 
                  by ztmk.user1
                  by ztmk.stagecd
                  by ztmk.punchindt
                  by ztmk.punchintm:

       
       find first icsd where
                  icsd.cono  = ztmk.cono    and 
                  icsd.whse  = entry(1,ztmk.user1,"-")
                  no-lock no-error.
       if avail icsd and 
         (icsd.whse < b-whse or 
          icsd.whse > e-whse) then do:
          
         if last-of (ztmk.user1) and
            p-includelaborfl then 
           run Include_Labor_Transactions.
         next.        
       end.
       if rcv-flg and (ztmk.stagecd ne 1) then do:
        
         if last-of (ztmk.user1) and
            p-includelaborfl then 
           run Include_Labor_Transactions.
 
         next.         
       end.
       
       find first vasp where 
            vasp.cono     = 01         and 
            vasp.shipprod = ztmk.user1 and 
            vasp.whse     = entry(1,ztmk.user1,"-")          
            no-lock no-error.
/*
       assign v-esth = int(substring(vasp.user4,50,2))
              v-estm = int(substring(vasp.user4,52,2)).
*/
       if ztmk.stagecd = 3 then              
         assign v-esth =  int(substring(vasp.user4,50,2))    
                v-estm =  int(substring(vasp.user4,52,2)).    
       else
       if ztmk.stagecd = 10 then              
         assign v-esth =  int(substring(vasp.user4,54,2))  
                v-estm =  int(substring(vasp.user4,56,2)).  
       else
       if ztmk.stagecd = 11 then              
          assign v-esth =  int(substring(vasp.user4,58,2))  
                 v-estm =  int(substring(vasp.user4,60,2)).  
       else       
          assign v-esth = 0
                 v-estm = 0.
       assign tot-esthm = (v-esth * 60) + v-estm.
       
       if ztmk.stagecd   < b-istage or
          ztmk.stagecd   > e-istage then do:
          
         if last-of (ztmk.user1) and
            p-includelaborfl then 
           run Include_Labor_Transactions.
 
         next. 
       end.
       
       find icsp where icsp.cono = 1 and
                       icsp.prod = ztmk.user1
                       no-lock no-error.
                       
         if avail icsp then 
            assign v-prodcat = icsp.prodcat.


/* here */

       if ztmk.punchoutdt = ? then
         assign t-days      = today - ztmk.punchindt  
                t-xdays     = t-days 
                t-tmout     = (dec(substring(string(time,"hh:mm"),1,2)) * 60) +
                               dec(substring(string(time,"hh:mm"),4,2))
                t-tmin      = (dec(substring(ztmk.punchintm,1,2)) * 60) + 
                               dec(substring(ztmk.punchintm,3,2))
                t-time      = if t-days = 0 then 
                                t-tmout - t-tmin
                              else 
                                (t-tmout - t-tmin) + (t-days * (24 * 60)).
 
       
       else       
         assign t-days      = ztmk.punchoutdt - ztmk.punchindt  
                t-xdays     = t-days 
                t-tmout     = (dec(substring(ztmk.punchouttm,1,2)) * 60) +
                               dec(substring(ztmk.punchouttm,3,2))
                t-tmin      = (dec(substring(ztmk.punchintm,1,2)) * 60) + 
                               dec(substring(ztmk.punchintm,3,2))
                t-time      = if t-days = 0 then 
                                t-tmout - t-tmin
                              else 
                                (t-tmout - t-tmin) + (t-days * (24 * 60)).
       assign v-whse      = entry(1,ztmk.user1,"-").
       assign t-ordqty    = t-ordqty  + 1
              t-dordqty   = t-dordqty + 1
              t-tordqty   = t-tordqty + 1
              t-sordqty   = t-sordqty + 1
              t-wordqty   = t-wordqty + 1
              t-gordqty   = t-gordqty + 1.
       assign v-operinit = ztmk.operinit
              v-techid   = ztmk.techid.
              
       find first arsc where
                  arsc.cono = g-cono and 
                  arsc.custno = dec(string(vasp.user5,"999999999999"))
            no-lock no-error.
       if avail arsc then 
        assign p-name = arsc.name
               v-shipto = arsc.shipto
               v-custno = arsc.custno
               v-slsrep = arsc.slsrepout.
       else 
        assign p-name = "". 
       find first vasp where 
                  vasp.cono     = 01     and 
                  vasp.shipprod = ztmk.user1        
         no-lock no-error.
       assign p-partno  = if avail vasp then 
                           vasp.refer 
                          else 
                           "" 
              d-wononbr = entry(1,ztmk.user1,"-") + "-" + 
                left-trim(entry(2,ztmk.user1,"-"),"0")
              v-stgprt  = if ztmk.stagecd = 0 then 
                              v-stage2[1]
                          else    
                          if ztmk.stagecd = 5 or
                             ztmk.stagecd  = 6 then 
                     /*  ask jen "ToBe" + "-" +  */ v-stage2[ztmk.stagecd]
                          else 
                            v-stage2[ztmk.stagecd]
              dtl-time   = t-time.              /* (t-time / 60).  */           
       
       
       if dtl-time > 0 then do:                     
        run formattime(input dtl-time, 
                      output t-min, 
                      output t-hrs, 
                      output t-xdays).
       end.    

       assign tstg-time  = tstg-time + dtl-time
              date-time  = date-time + dtl-time
              tgtl-time  = tgtl-time + dtl-time
              prt-days   = t-xdays
              t-ftl-days = t-ftl-days + t-xdays
              t-stg-days = t-stg-days + t-xdays
              t-dte-days = t-dte-days + t-xdays. 

       assign substring(prt-time,1,3) = string(t-hrs,"zz9")
              substring(prt-time,4,2) = string(t-min,"99")
              dtl-time = 0 
              t-hrs    = 0 
              t-min    = 0.


       run xsdioetlt.p (input-output v-slsrep,
                        input        v-prodcat,
                        input        v-custno,
                        input        v-shipto).

     if b-am <> "" then
       if v-slsrep < b-am or
          v-slsrep > b-am then do:

       if last-of (ztmk.user1) and
          p-includelaborfl then 
         run Include_Labor_Transactions.
           
       next.
     end.
/*
     if s-asyflag and ztmk.stagecd = 10 then do:
        run Accumulate_Asy (/*output s-tdn, */
                            output s-tottime2,
                            buffer ztmk).
           
     end.
       

     if s-tstflag and ztmk.stagecd = 11 then do:
       run Accumulate_tst (/* output  s-tdn, */
                           output s-tottime2,   
                           buffer ztmk).  
     end. 
     
     if s-tdnflag and ztmk.stagecd <> 10 and ztmk.stagecd <> 11 then do:
        run Accumulate_teardown (/* output  s-tdn, */
                                 output s-tottime2,   
                                 buffer ztmk).  
     end. 
       
*/

    if ztmk.stagecd <> 11 and ztmk.stagecd <> 10 and ztmk.stagecd <> 3 then    ~        assign substring(s-tdn,1,3)  = "  0"
                substring(s-tdn,4,2)  = "00".
      else                                
         assign s-tdn = prt-time.
  
     if ztmk.stagecd = 11 or ztmk.stagecd = 10 or ztmk.stagecd = 3 then  
        assign  s-tottime2 = t-time. 
     else 
        assign s-tottime2 = 0.
        
       if cls-flag then 
         run Create_All_Transactions.
       else  
       if can-flag then
         run Create_All_Transactions.
       else do:
       create vasort.                       
       assign vasort.operinit  = v-operinit
              vasort.sortdtl   = string(ztmk.user1,"x(12)") +
                                 string(ztmk.stagecd,"99") +
                                 string(ztmk.punchindt,"99/99/9999") +
                                 string(ztmk.punchintm,"x(8)") +
                                 string(recid(ztmk),"999999999999") 
              vasort.repair    = d-wononbr
              vasort.stage     = v-stgprt
              vasort.stagecd   = ztmk.stagecd
              vasort.indate    = ztmk.punchindt
              vasort.tech      = v-techid
              vasort.esth      = v-esth
              vasort.estm      = v-estm
              vasort.intime    = ztmk.punchintm
              vasort.outdate   = if ztmk.punchoutdt = ? then
                                   today
                                 else
                                   ztmk.punchoutdt
              vasort.outtime   = if ztmk.punchoutdt = ? then
                                   substring(string(time,"hh:mm"),1,2) +
                                   substring(string(time,"hh:mm"),4,2) 
                                 else
                                   ztmk.punchouttm  
              vasort.estout    = if ztmk.punchoutdt = ? then
                                   "*"
                                 else
                                   " " 
              vasort.ttime     = t-time  
              vasort.miltime   = prt-time
              vasort.days      = prt-days
              vasort.number    = v-custno
              vasort.name      = p-name
              vasort.partno    = p-partno
              vasort.slsrep    = v-slsrep
              vasort.whse      = v-whse  
              vasort.tdn       = s-tdn
              vasort.totalt    = s-tottime2
              vasort.totesthm  = tot-esthm
              vasort.ztmkrecid = recid(ztmk).
              
      end. 

      if last-of (ztmk.user1) and
         p-includelaborfl then 
        run Include_Labor_Transactions.
        
       
       assign prt-days = 0.
       
   end. /* for each ztmk  */
   
 end.  
end.  /* big loop */


/* ////                  
       if ztmk.orderno > 0 then do: 
          find first vaeh where
                     vaeh.cono  = ztmk.cono    and 
                     vaeh.vano  = ztmk.orderno and 
                     vaeh.vasuf = ztmk.ordersuf 
                     no-lock no-error.
       if avail vaeh and 
         (vaeh.whse < b-whse or 
          vaeh.whse > e-whse) then 
          next.
       end. 
       else
       if entry(1,ztmk.user1,"-") < b-whse or 
          entry(1,ztmk.user1,"-") > e-whse then 
          next. 
       if rcv-flg and ztmk.stagecd ne 1 then 
          next.         
       find first vasp where 
            vasp.cono     = 01         and 
            vasp.shipprod = ztmk.user1 and 
            vasp.whse     = entry(1,ztmk.user1,"-")          
            no-lock no-error.
       if not avail vasp then 
        next.
       assign t-days      =  ztmk.punchoutdt - ztmk.punchindt  
              t-tmout     = (dec(substring(ztmk.punchouttm,1,2)) * 60) +
                             dec(substring(ztmk.punchouttm,3,2))
              t-tmin      = (dec(substring(ztmk.punchintm,1,2)) * 60) + 
                             dec(substring(ztmk.punchintm,3,2))
              t-time      = if t-days = 0 then 
                               t-tmout - t-tmin
                            else 
                              (t-tmout - t-tmin) + (t-days * (24 * 60)).
       assign t-ordqty    = t-ordqty  + 1
              t-dordqty   = t-dordqty + 1
              t-tordqty   = t-tordqty + 1
              t-sordqty   = t-sordqty + 1
              t-wordqty   = t-wordqty + 1
              t-gordqty   = t-gordqty + 1.
 /*             
       if dec(vasp.user7) = 14 then do: 
        assign s-stageprt = "can". 
        next. 
       end. 
       else     */
        do inx2  = 1 to 16: 
        if inx2 = ztmk.stagecd then do: 
         assign s-stageprt = v-stage2[inx2].
         leave.
        end.
       end.       
       
       assign v-esth = int(substring(vasp.user4,50,2))
              v-estm = int(substring(vasp.user4,52,2)).

       assign tot-esthm = (v-esth * 60) + v-estm.

       assign v-operinit = ztmk.operinit
              v-techid   = ztmk.techid.
       
       find icsp where icsp.cono = 1 and
                       icsp.prod = ztmk.user1
                       no-lock no-error.
                       
         if avail icsp then 
            assign v-prodcat = icsp.prodcat.
       
       find first arsc where
                  arsc.cono = g-cono and 
                  arsc.custno = dec(string(vasp.user5,"999999999999"))
            no-lock no-error.
       if avail arsc then 
        assign p-name = arsc.name
               v-shipto = arsc.shipto
               v-custno = arsc.custno
               v-slsrep = arsc.slsrepout.
       else 
        assign p-name = "". 
       assign p-partno  = if avail vasp then 
                           vasp.refer 
                          else 
                           "" 
              d-wononbr = entry(1,ztmk.user1,"-") + "-" +
                left-trim(entry(2,ztmk.user1,"-"),"0")
                dtl-time  = t-time
                v-stgprt  = if ztmk.stagecd = 0 then 
                              v-stage2[1]
                             else    
                             if ztmk.stagecd = 5 or
                                ztmk.stagecd  = 6 then 
                              "ToBe" + "-" + v-stage2[ztmk.stagecd]
                             else 
                               v-stage2[ztmk.stagecd].

       if dtl-time > 0 then do:                     
        run formattime(input dtl-time, 
                      output t-min, 
                      output t-hrs, 
                      output t-xdays).
       end.  
       
       
       assign tstg-time  = tstg-time + dtl-time
              date-time  = date-time + dtl-time
              prt-days   = t-xdays
              t-ftl-days = t-ftl-days + t-xdays
              t-stg-days = t-stg-days + t-xdays
              t-dte-days = t-dte-days + t-xdays. 

  
       assign substring(prt-time,1,3) = string(t-hrs,"zz9")
              substring(prt-time,4,2) = string(t-min,"99")
              dtl-time = 0 
              t-hrs    = 0 
              t-min    = 0.
       if last-of(ztmk.user1) then do: 
          assign d-wononbr = entry(1,ztmk.user1,"-") + "-" +
                             left-trim(entry(2,ztmk.user1,"-"),"0")
                 v-stgprt  = if ztmk.stagecd = 0 then 
                              v-stage2[1]
                             else    
                             if ztmk.stagecd = 5 or
                                ztmk.stagecd = 6 then 
                              "ToBe" + "-" + v-stage2[ztmk.stagecd]
                             else 
                               v-stage2[ztmk.stagecd].
          if tech-time > 0 then 
             assign t-hrs = t-hrs + truncate(tech-time / 1,0)
                    t-min = t-min + 
                          (60 * (tech-time - truncate(tech-time / 1,0))).
          else 
             assign t-hrs = 0              
                    t-min = 0.
          assign substring(dtl-tech-time,1,3) = string(t-hrs,"zz9")
                 substring(dtl-tech-time,4,2) = string(t-min,"99").
          if ztmk.techid = "admn" then 
           {w-smsn.i ztmk.operinit no-lock}
          else 
           {w-smsn.i ztmk.techid no-lock}.
        
          assign v-operinit = ztmk.operinit
                 v-techid   = ztmk.techid.
           
          assign v-name = if avail smsn then
                           smsn.name
                          else 
                          if avail ztmk and ztmk.techid = "admn" then 
                           "Administrative Stages" 
                          else  
                           "Tech Name Notfound"
                 v-name = "".

       run xsdioetlt.p (input-output v-slsrep,
                        input        v-prodcat,
                        input        v-custno,
                        input        v-shipto).
            if b-am <> "" then            
              if v-slsrep < b-am or
                 v-slsrep > e-am then
                 next.       
                 
/*                 
                 
       if s-asyflag and ztmk.stagecd = 10 then do:
        run Accumulate_Asy (output s-tdn,
                            output s-tottime2,
                            buffer ztmk).
       end.
       
       if s-tdnflag and ztmk.stagecd <> 10 then do:
        run Accumulate_teardown (output  s-tdn,
                                 output s-tottime2,   
                                 buffer ztmk).  
       end. 

*/
    

    if ztmk.stagecd <> 11 and ztmk.stagecd <> 10 and ztmk.stagecd <> 3 then            assign substring(s-tdn,1,3)  = "  0"
              substring(s-tdn,4,2)  = "00".
    else                                
       assign s-tdn = prt-time.
  
     if ztmk.stagecd = 11 or ztmk.stagecd = 10 or ztmk.stagecd = 3 then  
        assign  s-tottime2 = t-time. 
     else 
        assign s-tottime2 = 0.
        

    
       create vasort.
       assign vasort.operinit  = v-operinit
              vasort.repair    = d-wononbr
              vasort.sortdtl   = string(ztmk.user1,"x(12)") +
                                 string(ztmk.stagecd,"99") +
                                 string(ztmk.punchindt,"99/99/9999") +
                                 string(ztmk.punchintm,"x(8)") + 
                                 string(recid(ztmk),"999999999999") 
                                 
       
              vasort.stage     = v-stgprt
              vasort.stagecd   = ztmk.stagecd
              vasort.indate    = ztmk.punchindt
              vasort.tech      = v-techid
              vasort.esth      = v-esth
              vasort.estm      = v-estm
              vasort.intime    = ztmk.punchintm
              vasort.outdate   = ztmk.punchoutdt
              vasort.outtime   = ztmk.punchouttm  
              vasort.ttime     = t-time  
              vasort.miltime   = prt-time
              vasort.days      = prt-days
              vasort.name      = p-name
              vasort.number    = v-custno
              vasort.partno    = p-partno
              vasort.whse      = v-whse
              vasort.slsrep    = v-slsrep  
              vasort.tdn       = s-tdn
              vasort.totalt    = s-tottime2
              vasort.totesthm  = tot-esthm
              vasort.ztmkrecid = recid(ztmk).
              
 

       if cls-flag then 
         run Create_All_Transactions.
       if can-flag then
         run Create_All_Transactions.
     
          assign t-tordqty   = 0 
                 tech-time   = 0
                 t-hrs       = 0 
                 t-min       = 0.  
       end. 
       if last-of(ztmk.punchindt) then do: 
         assign tgtl-time = tgtl-time + date-time. 
       end. 
   end.  
 leave.
end.
end.   
/////// */

/*-----------------------------------------------------------------------*/
procedure print-dtl:
/*-----------------------------------------------------------------------*/
  define input parameter ip-recid as recid no-undo. 

  find vasort where recid(vasort) = ip-recid no-lock no-error.
  
   display vasort.operinit vasort.repair vasort.stage 
           vasort.indate vasort.intime vasort.outdate vasort.outtime
           vasort.estout
           vasort.tech vasort.esth vasort.estm   
           vasort.miltime vasort.days vasort.name vasort.partno 
           vasort.slsrep vasort.number vasort.tdn 
           with frame f-sort-detail.
           down with frame f-sort-detail.    
end.

/*-----------------------------------------------------------------------*/

procedure MakeQuoteNo:
/*-----------------------------------------------------------------------*/
define input  parameter ip-quoteno   as character no-undo.
define output parameter ip-DBquoteno as character no-undo.
define var z as char format "x(07)"               no-undo. 
define var y as int                               no-undo.
assign v-errfl = no.

if num-entries(ip-quoteno,"-") <> 2 then
  do:
  assign ip-DBquoteno = ip-quoteno
              v-errfl = yes
              v-error = 
              "Invalid Repair No. --> " + ip-quoteno +   
              " - Please enter format 'Whse-7
              '".
  return.
  end.

 assign z = trim(entry(2,ip-quoteno,"-"),"0123456789").
 assign y = length(z).

 if y ne 0 then 
  do:
  assign ip-DBquoteno = ip-quoteno
              v-errfl = yes
              v-error = 
              "Invalid Repair No. --> " + ip-quoteno +   
              " - Please enter format 'Whse-7'".
  return.
  end.

  assign ip-DBquoteno  = entry (1,ip-quoteno,"-") + "-" +
                        string(int(entry(2,ip-quoteno,"-")),"9999999").
                                                        
end.     
  
procedure timespan:    
  define input  parameter ix-indate    as date no-undo.
  define input  parameter ix-intime    as char format "xx:xx:xx" no-undo.
  define input  parameter ix-outdate   as date no-undo.
  define input  parameter ix-outtime   as char format "xx:xx:xx" no-undo.
  define output parameter ix-lendays   as integer no-undo.
  define output parameter ix-lenhr     as integer no-undo.
  define output parameter ix-lenmin    as integer no-undo.
  define output parameter ix-lensec    as integer no-undo.


  def var wx-days as integer no-undo.
  def var wx-tm   as integer no-undo.
  def var wx-itm  as integer no-undo.
  def var wx-otm  as integer no-undo.

  assign wx-days = ix-outdate - ix-indate
         wx-itm  = (dec(substring(ix-intime,1,2)) * 3600) +
                    dec(substring(ix-intime,3,2)) * 60 +       
                    dec(substring(ix-intime,5,2))

         wx-otm  = (dec(substring(ix-outtime,1,2)) * 3600) +
                    dec(substring(ix-outtime,3,2)) * 60 +       
                    dec(substring(ix-outtime,5,2)).
                             
  assign wx-tm   = if wx-days = 0 then                          
                     wx-otm - wx-itm                         
                   else
                     (((wx-days * (24 * 3600)) + wx-otm) - wx-itm).  
  assign ix-lendays = truncate((wx-tm / 86400),0)
         ix-lenhr   = truncate((wx-tm modulo 86400) / 3600 ,0)
         ix-lenmin  = truncate((wx-tm modulo 3600 ) / 60   ,0)
         ix-lensec  = truncate((wx-tm modulo 60)    / 1    ,0).
end.

procedure formatTime:
def input parameter inTime as dec no-undo. /* in minutes */
def output parameter minutes as int no-undo.
def output parameter hours   as int no-undo.
def output parameter days    as int no-undo.
def var holdTime as dec no-undo. 

holdTime = inTime.
minutes = holdTime modulo 60.
holdTime = holdTime - minutes.
hours = (holdTime modulo (60 * 24)) / 60 .
holdTime = holdTime - (hours * 60 ).
days = holdTime / (60 * 24 ).
/* display minutes hours days.   */
end procedure.



/{&user_procedures}*/


