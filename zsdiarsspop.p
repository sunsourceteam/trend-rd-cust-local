
{g-all.i}
{g-ar.i}


define var v-lastfieldname as char no-undo.

define buffer z-arss for arss.
define buffer slsmx for notes.
define buffer x-slsmx for notes.
define buffer z-logo for notes.
define buffer slslb for notes.
define buffer x-sasos for sasos.
define buffer b-sasos for sasos.
define var s-CCgeneric as logical            label "Generic CC".
define var s-complogo   as c format "x(4)"   label "Company Logo".
define var s-custtype    as c format "x(3)"   label "Cust Type".
define var ix-cname     as character no-undo.
define var ix-cphone    as character no-undo.
define var ix-phone     as character no-undo.
define var ix-email     as character no-undo.

define var idx          as i format "99"      no-undo.
define var found-logo   as logical            no-undo.
def var blank-line1     as c format "x(71)"                  no-undo.
def var blank-line2     as c format "x(71)"                  no-undo.
def var blank-line3     as c format "x(71)"                  no-undo.
def var blank-line4     as c format "x(71)"                  no-undo.
def var blank-line5     as c format "x(71)"                  no-undo.
def var blank-line6     as c format "x(71)"                  no-undo.
def var blank-line7     as c format "x(71)"                  no-undo.
def var blank-line8     as c format "x(71)"                  no-undo.
def var blank-line9     as c format "x(71)"                  no-undo.
def var blank-line10    as c format "x(71)"                  no-undo.
def var blank-line11    as c format "x(71)"                  no-undo.
def var blank-line12    as c format "x(71)"                  no-undo.
def var blank-line13    as c format "x(71)"                  no-undo.
def var blank-line14    as c format "x(71)"                  no-undo.
def var blank-line15    as c format "x(71)"                  no-undo.
def var blank-line16    as c format "x(71)"                  no-undo.
def var blank-line17    as c format "x(71)"                  no-undo.
def var blank-line18    as c format "x(71)"                  no-undo.
def var blank-line19    as c format "x(71)"                  no-undo.


def frame blank-screen
  blank-line1   at 2 no-label
  blank-line2   at 2 no-label
  blank-line3   at 2 no-label
  blank-line4   at 2 no-label
  blank-line5   at 2 no-label
  blank-line6   at 2 no-label
  blank-line7   at 2 no-label
  blank-line8   at 2 no-label
  blank-line9   at 2 no-label
  blank-line10  at 2 no-label
  with overlay no-labels no-box width 80 column 1 row 12.

def frame blank-screen2
  blank-line1   at 2 no-label
  blank-line2   at 2 no-label
  blank-line3   at 2 no-label
  blank-line4   at 2 no-label
  blank-line5   at 2 no-label
  blank-line6   at 2 no-label
  blank-line7   at 2 no-label
  blank-line8   at 2 no-label
  blank-line9   at 2 no-label
  blank-line10  at 2 no-label
  blank-line11  at 2 no-label  
  blank-line12  at 2 no-label  
  blank-line13  at 2 no-label  
  blank-line14  at 2 no-label  
  blank-line15  at 2 no-label  
  blank-line16  at 2 no-label  
  blank-line17  at 2 no-label  
  blank-line18  at 2 no-label  
  blank-line19  at 2 no-label  
  with overlay no-labels no-box width 80 column 1 row 4.


define new shared temp-table t-tech no-undo
  field Technology as c format "x(24)"
  field SlsRep     as c format "x(4)" 
  field Tpage      as i format "999"  
  field Tsequence  as i format "99"   
  index k-tech     
        Tpage      
        Tsequence  
        Technology.
                    
def new shared buffer s-tech for t-tech.
def new shared var q-line     as integer         init 1         no-undo.


def frame f-arsszk
z-arss.custno at 3 
z-arss.shipto at 3
z-arss.name at 3
slsmx.printfl3 at 3 label "Exempt Sauer APR?"
slsmx.printfl4 at 40 label "SalesLogics?"
s-CCgeneric at 3
s-complogo     at 40 
s-custtype  at 59            {f-help.i}
skip(1)
skip(1) 
slslb.noteln[1] at 3 format "x(20)" no-label
slsmx.noteln[1] at 30 format "x(4)" no-label
slslb.noteln[2] at 3 format "x(20)" no-label
slsmx.noteln[2] at 30 format "x(4)" no-label
slslb.noteln[3] at 3 format "x(20)" no-label
slsmx.noteln[3] at 30 format "x(4)" no-label
slslb.noteln[4] at 3 format "x(20)" no-label
slsmx.noteln[4] at 30 format "x(4)" no-label
slslb.noteln[5] at 3 format "x(20)" no-label
slsmx.noteln[5] at 30 format "x(4)" no-label
slslb.noteln[6] at 3 format "x(20)" no-label
slsmx.noteln[6] at 30 format "x(4)" no-label
slslb.noteln[7] at 3 format "x(20)" no-label
slsmx.noteln[7] at 30 format "x(4)" no-label
slslb.noteln[8] at 3 format "x(20)" no-label
slsmx.noteln[8] at 30 format "x(4)" no-label
slslb.noteln[9] at 3 format "x(20)" no-label
slsmx.noteln[9] at 30 format "x(4)" no-label

 with overlay side-labels width 78 column 1 row 4 title
    " Custom Extended ARSS Update ".

for each t-tech:
  delete t-tech.
end.

on f8 anywhere
do:
 
  if g-secure ge 2 then
    do:
    
    find x-slsmx      where x-slsmx.cono = g-cono and 
                            x-slsmx.notestype = "zz" and
                            x-slsmx.primarykey  = "slsmx" and
                            x-slsmx.secondarykey = 
                            string(g-custno,"99999999999") +
                                   g-shipto and 
                            x-slsmx.pageno = 0       
                            exclusive-lock
                            no-error. 
    /*
     if not avail x-slsmx /* and g-secure ge 3 */ then
       do:
        message "creating slsmx". pause.   
       create slsmx.
       assign slsmx.cono = g-cono  
              slsmx.notestype = "ZZ" 
              slsmx.primarykey  = "SLSMX"
              slsmx.secondarykey = 
                    (string(g-custno,"99999999999") +
                            g-shipto).
              slsmx.pageno = 0.

           run zsdicontact.p (input "uu",
                         input g-cono,
                         input g-custno,
                         input g-shipto,
                         input-output ix-cname,
                         input-output ix-cphone,
                         input-output ix-phone,
                         input-output ix-email).
     end.
    
    else
    */
    
    if avail x-slsmx then do:
/*     message "avail x-slsmx". pause.          */
      run zsdicontact.p (input "uu",
                         input g-cono,
                         input g-custno,
                         input g-shipto,
                         input-output ix-cname,
                         input-output ix-cphone,
                         input-output ix-phone,
                         input-output ix-email).
    end.  /* added */
   end.

 put screen row 11 column 3 
         color message
  " Technology                 SlsRep     (Salesman Override by Technology)  ".
 
end.

on f9 anywhere
  do:
  on cursor-up   cursor-up.
  on cursor-down cursor-down.
  hide message no-pause.
  run zsditech99.p.
  for each s-tech no-lock:
    find slsmx where slsmx.cono = g-cono and
                     slsmx.notestype = "zz" and
                     slsmx.primarykey = "slsmx" and
                     slsmx.secondarykey = 
                           string(g-custno,"99999999999") + g-shipto and
                     slsmx.pageno = s-tech.Tpage
                     exclusive-lock no-error.
    if avail slsmx then
      do:
      assign slsmx.noteln[s-tech.Tsequence] = s-tech.SlsRep.
    end.
    else
      do:
      create slsmx.
      assign slsmx.cono          = g-cono
             slsmx.notestype     = "zz"
             slsmx.primarykey    = "slsmx"
             slsmx.secondarykey  = string(g-custno,"99999999999") + g-shipto
             slsmx.pageno        = s-tech.Tpage
             slsmx.noteln[s-tech.Tsequence] = s-tech.SlsRep.
    end.
  end.
  find slsmx  where slsmx.cono = g-cono and
                    slsmx.notestype = "zz" and
                    slsmx.primarykey  = "slsmx" and
                    slsmx.secondarykey =
             string(g-custno,"99999999999") + g-shipto and
                    slsmx.pageno = 0
                    /*exclusive-lock*/
                    no-lock
                    no-error.
  
         display 
          slslb.noteln[1] when slslb.noteln[1] <> " "
          slsmx.noteln[1] when avail slsmx and slslb.noteln[1] <> ""
          slslb.noteln[2] when slslb.noteln[2] <> " "
          slsmx.noteln[2] when avail slsmx and slslb.noteln[2] <> ""
          slslb.noteln[3] when slslb.noteln[3] <> " "
          slsmx.noteln[3] when avail slsmx and slslb.noteln[3] <> ""
          slslb.noteln[4] when slslb.noteln[4] <> " "
          slsmx.noteln[4] when avail slsmx and slslb.noteln[4] <> ""
          slslb.noteln[5] when slslb.noteln[5] <> " "
          slsmx.noteln[5] when avail slsmx and slslb.noteln[5] <> ""
          slslb.noteln[6] when slslb.noteln[6] <> " "
          slsmx.noteln[6] when avail slsmx and slslb.noteln[6] <> ""
          slslb.noteln[7] when slslb.noteln[7] <> " "
          slsmx.noteln[7] when avail slsmx and slslb.noteln[7] <> ""
          slslb.noteln[8] when slslb.noteln[8] <> " "
          slsmx.noteln[8] when avail slsmx and slslb.noteln[8] <> ""
          slslb.noteln[9] when slslb.noteln[9] <> " "
          slsmx.noteln[9] when avail slsmx and slslb.noteln[9] <> ""
      with frame f-arsszk.
      put screen row 11 column 3 
        color message
  " Technology                 SlsRep     (Salesman Override by Technology)  ".
  readkey pause 0.
  on cursor-up   back-tab.
  on cursor-down tab.
  put screen row 11 column 3 
     color message
  " Technology                 SlsRep     (Salesman Override by Technology)  ".
  
  
  if lastkey = 401 then leave.

end. /* on F9 */





if g-ourproc begins "arss" and g-custno <> {c-empty.i} then
do:

           
   {w-arss.i g-custno g-shipto "exclusive-lock" " " "z-"}
   assign s-CCgeneric = if substr(z-arss.user10,1,1) = "y" then yes else no.
   assign s-complogo  = substr(z-arss.user19,1,4).
   assign s-custtype  = substr(z-arss.user21,1,3).
   for each slslb where slslb.cono = g-cono and 
                        slslb.notestype = "zz" and
                        slslb.primarykey  = "slsmx" and
                        slslb.secondarykey = "label"
                        no-lock:
     do idx = 1 to 16:
       find s-tech where s-tech.Technology = slslb.noteln[idx] no-error.
       if avail s-tech then
         next.
       if slslb.noteln[idx] = " " then leave.
       create s-tech.
       assign s-tech.Technology = slslb.noteln[idx]
              s-tech.Tpage      = if slslb.pageno >= 1 then slslb.pageno - 1 
                                  else slslb.pageno
              s-tech.Tsequence  = idx.
     end.
   end.
   for each s-tech:
     find notes where notes.cono = g-cono and 
                      notes.notestype = "zz" and
                      notes.primarykey  = "slsmx" and
                      notes.secondarykey = 
                             string(g-custno,"99999999999") + g-shipto and
                      notes.pageno = s-tech.Tpage
                      no-lock no-error. 
     if avail notes then
       assign s-tech.SlsRep = notes.noteln[s-tech.Tsequence].
   end.
   
   find first slslb      where slslb.cono = g-cono and 
                         slslb.notestype = "zz" and
                         slslb.primarykey  = "slsmx" and
                         slslb.secondarykey = 
                           "label"
                            no-lock
                            no-error. 
   find slsmx where slsmx.cono = g-cono and 
                    slsmx.notestype = "zz" and
                    slsmx.primarykey  = "slsmx" and
                    slsmx.secondarykey = 
                       (string(g-custno,"99999999999") + g-shipto) and
                    slsmx.pageno = 0
                    exclusive-lock
                    no-error. 

       display z-arss.custno z-arss.shipto z-arss.name 
             slsmx.printfl3 when avail slsmx
             slsmx.printfl4 when avail slsmx
             s-CCgeneric
             s-complogo
             s-custtype
             slslb.noteln[1] when slslb.noteln[1] <> " "
             slsmx.noteln[1] when avail slsmx and slslb.noteln[1] <> ""
             slslb.noteln[2] when slslb.noteln[2] <> " "
             slsmx.noteln[2] when avail slsmx and slslb.noteln[2] <> ""
             slslb.noteln[3] when slslb.noteln[3] <> " "
             slsmx.noteln[3] when avail slsmx and slslb.noteln[3] <> ""
             slslb.noteln[4] when slslb.noteln[4] <> " "
             slsmx.noteln[4] when avail slsmx and slslb.noteln[4] <> ""
             slslb.noteln[5] when slslb.noteln[5] <> " "
             slsmx.noteln[5] when avail slsmx and slslb.noteln[5] <> ""
             slslb.noteln[6] when slslb.noteln[6] <> " "
             slsmx.noteln[6] when avail slsmx and slslb.noteln[6] <> ""
             slslb.noteln[7] when slslb.noteln[7] <> " "
             slsmx.noteln[7] when avail slsmx and slslb.noteln[7] <> ""
             slslb.noteln[8] when slslb.noteln[8] <> " "
             slsmx.noteln[8] when avail slsmx and slslb.noteln[8] <> ""
             slslb.noteln[9] when slslb.noteln[9] <> " "
             slsmx.noteln[9] when avail slsmx and slslb.noteln[9] <> ""
     with frame f-arsszk.
     put screen row 11 column 3 
       color message
   " Technology                 SlsRep     (Salesman Override by Technology)  ".
     /*  end. */
    
    
if g-secure ge 3 then
do:
     /* REMOVED 
     put screen row 21 col 6 color message   
      "F8-Add Contact Info   F9-Maintain Technology                         ".
     /*   pause 0.  */
     */
     
    
     if not avail slsmx then
       do:
       create slsmx.
       assign slsmx.cono = g-cono  
              slsmx.notestype = "ZZ" 
              slsmx.primarykey  = "SLSMX" 
              slsmx.secondarykey = 
                    (string(g-custno,"99999999999") +
                            g-shipto).
              slsmx.pageno = 0.
     end.
     
     
       find first x-sasos where 
                  x-sasos.cono   = g-cono and      
                  x-sasos.oper2  = g-operinits and
                  x-sasos.menuproc = "zxt2"  no-lock no-error.      

       
       find first b-sasos where b-sasos.cono = g-cono and
                                b-sasos.oper2 = g-operinits and
                                b-sasos.menuproc = "zxar"
                                no-lock no-error.
     
      

        if not ((avail x-sasos and x-sasos.securcd[12] = 5) or 
                (avail b-sasos and b-sasos.securcd[9] >= 3) or
                (avail b-sasos and b-sasos.securcd[7] >= 3) or
                (avail b-sasos and b-sasos.securcd[4] >= 3) or
                (avail b-sasos and b-sasos.securcd[14] >= 3) ) then  do:
        
       /*   message "Press any key to continue".   */

             
             display 
             z-arss.custno z-arss.shipto z-arss.name 
             slsmx.printfl3 when avail slsmx
             slsmx.printfl4 when avail slsmx
             s-CCgeneric
             s-complogo
             s-custtype
             slslb.noteln[1] when slslb.noteln[1] <> " "
             slsmx.noteln[1] when avail slsmx and slslb.noteln[1] <> ""
             slslb.noteln[2] when slslb.noteln[2] <> " "
             slsmx.noteln[2] when avail slsmx and slslb.noteln[2] <> ""
             slslb.noteln[3] when slslb.noteln[3] <> " "
             slsmx.noteln[3] when avail slsmx and slslb.noteln[3] <> ""
             slslb.noteln[4] when slslb.noteln[4] <> " "
             slsmx.noteln[4] when avail slsmx and slslb.noteln[4] <> ""
             slslb.noteln[5] when slslb.noteln[5] <> " "
             slsmx.noteln[5] when avail slsmx and slslb.noteln[5] <> ""
             slslb.noteln[6] when slslb.noteln[6] <> " "
             slsmx.noteln[6] when avail slsmx and slslb.noteln[6] <> ""
             slslb.noteln[7] when slslb.noteln[7] <> " "
             slsmx.noteln[7] when avail slsmx and slslb.noteln[7] <> ""
             slslb.noteln[8] when slslb.noteln[8] <> " "
             slsmx.noteln[8] when avail slsmx and slslb.noteln[8] <> ""
             slslb.noteln[9] when slslb.noteln[9] <> " "
             slsmx.noteln[9] when avail slsmx and slslb.noteln[9] <> ""
     with frame f-arsszk.
     down with frame f-arsszk.
 

      readkey.      
      return.      
      /* leave.   */
      
      hide frame f-arsszk no-pause.
      clear frame f-arsszk all.
      hide all no-pause.

   end. /* not security */

  arsszk-updt:

  do while true with frame f-arsszk on endkey undo arsszk-updt,
                                                leave arsszk-updt:
       
       

        
   put screen row 11 column 3 
       color message
   " Technology                 SlsRep     (Salesman Override by Technology)  ".
     /*  end. */
 
   put screen row 21 col 6 color message   
        "F8-Add Contact Info   F9-Maintain Technology                         ".
      /*  pause 0.  */
   
   
         
         if x-sasos.securcd[12] = 5 then
           assign v-lastfieldname = "printfl3". 
         if b-sasos.securcd[9] >= 3 then
           assign v-lastfieldname = "printfl4".      
         if b-sasos.securcd[7] >= 3 then
           assign v-lastfieldname = "s-CCgeneric".
         if b-sasos.securcd[4] >= 3 then
           assign v-lastfieldname = "s-complogo".
         if b-sasos.securcd[14] >= 3 then
           assign v-lastfieldname = "s-custtype".   
       
       update  

        slsmx.printfl3    when (avail x-sasos and x-sasos.securcd[12] = 5) 
        slsmx.printfl4    when (avail b-sasos and b-sasos.securcd[9] >= 3)
        s-CCgeneric       when (avail b-sasos and b-sasos.securcd[7] >= 3)
        s-complogo        when (avail b-sasos and b-sasos.securcd[4] >= 3)
        s-custtype        when (avail b-sasos and b-sasos.securcd[14] >= 3) 
        with frame f-arsszk
        editorloop:
        editing:
        readkey.


        if {k-cancel.i} or {k-jump.i} then do:
           apply lastkey.
           undo arsszk-updt, leave arsszk-updt.
                             
        end.


        if keylabel(lastkey) = "F8" /*and frame-field = "noteln"*/ then
          do:
          apply lastkey.
          leave arsszk-updt.
        end.

        
        if input s-CCgeneric = yes then          
         overlay(z-arss.user10,1,1) = "y".
        else                               
         overlay(z-arss.user10,1,1) = "n".


        
        
        
        if frame-field = "s-custtype" and
           {k-sdileave.i &fieldname = s-custtype 
                         &completename = s-custtype} then do:
          assign s-custtype = input s-custtype.
          if not {v-sasta.i "CU" s-custtype} then do:              
            run err.p (4162).
            next.
          end.  
          else
            overlay(z-arss.user21,1,3) = input s-custtype.
 
        
        
        
        end.    
          
 
        
        if frame-field = "s-complogo" and
           can-do ("9,13,306,307,308,309,310,401,501,502,503,504",
            string(lastkey)) then
          do:
          assign found-logo = no.
          for each z-logo where z-logo.cono = g-cono and
                                z-logo.notestype  = "zz" and           
                                z-logo.primarykey = "Company Logo"
                                no-lock:
            do idx= 1 to 16:
              if input s-complogo = z-logo.noteln[idx] then
                do:
                assign found-logo = yes.
                leave.
              end.
            end.
          end. /* each z-logo */
          if found-logo = no then
            do:
            message "Invalid Comany Logo".
            next-prompt s-complogo with frame f-arsszk.
            next.
          end.
          overlay(z-arss.user19,1,4) = input s-complogo.
        end. /* frame s-complogo*/

      if {k-accept.i} or {k-cancel.i} then
        do:
        hide frame f-arsszk.
        leave arsszk-updt.
      end.


      if {k-return.i} and frame-field = v-lastfieldname then do: 


         if x-sasos.securcd[12] = 5 then
           apply "entry" to slsmx.printfl3 in frame f-arsszk. 
         else if b-sasos.securcd[9] >= 3 then
           apply "entry" to slsmx.printfl4 in frame f-arsszk.      
         else if b-sasos.securcd[7] >= 3 then
           apply "entry" to s-CCgeneric in frame f-arsszk.
         else if b-sasos.securcd[4] >= 3 then
           apply "entry" to s-complogo in frame f-arsszk.  
         else if b-sasos.securcd[14] >= 3 then
           apply "entry" to s-custtype in frame f-arsszk.
   
          next editorloop.
        end.  

      apply lastkey.
     end. /* editing */
    
    end. /* do while true */
    
   end. /* g-secure ge 3 */

 else
      
 if g-secure ge 2 then
 do:
   
     put screen row 21 col 6 color message   
      "F8-Add Contact Info   F9-Maintain Technology                         ".
       /* pause 0.  */

     
       
       find first x-sasos where 
                  x-sasos.cono   = g-cono and      
                  x-sasos.oper2  = g-operinits and
                  x-sasos.menuproc = "zxt2"  
                  no-lock no-error.      

       
       find first b-sasos where b-sasos.cono = g-cono and
                                b-sasos.oper2 = g-operinits and
                                b-sasos.menuproc = "zxar"
                                no-lock no-error.
     
      

        if not ((avail x-sasos and x-sasos.securcd[12] = 5) or 
                (avail b-sasos and b-sasos.securcd[9] >= 3) or
                (avail b-sasos and b-sasos.securcd[7] >= 3) or
                (avail b-sasos and b-sasos.securcd[4] >= 3) or
                (avail b-sasos and b-sasos.securcd[14] >= 3) ) then  do:
          

          
                /*   message "Press any key to continue".   */
         
         display 
             z-arss.custno z-arss.shipto z-arss.name 
             slsmx.printfl3 when avail slsmx
             slsmx.printfl4 when avail slsmx
             s-CCgeneric
             s-complogo
             s-custtype
             slslb.noteln[1] when slslb.noteln[1] <> " "
             slsmx.noteln[1] when avail slsmx and slslb.noteln[1] <> ""
             slslb.noteln[2] when slslb.noteln[2] <> " "
             slsmx.noteln[2] when avail slsmx and slslb.noteln[2] <> ""
             slslb.noteln[3] when slslb.noteln[3] <> " "
             slsmx.noteln[3] when avail slsmx and slslb.noteln[3] <> ""
             slslb.noteln[4] when slslb.noteln[4] <> " "
             slsmx.noteln[4] when avail slsmx and slslb.noteln[4] <> ""
             slslb.noteln[5] when slslb.noteln[5] <> " "
             slsmx.noteln[5] when avail slsmx and slslb.noteln[5] <> ""
             slslb.noteln[6] when slslb.noteln[6] <> " "
             slsmx.noteln[6] when avail slsmx and slslb.noteln[6] <> ""
             slslb.noteln[7] when slslb.noteln[7] <> " "
             slsmx.noteln[7] when avail slsmx and slslb.noteln[7] <> ""
             slslb.noteln[8] when slslb.noteln[8] <> " "
             slsmx.noteln[8] when avail slsmx and slslb.noteln[8] <> ""
             slslb.noteln[9] when slslb.noteln[9] <> " "
             slsmx.noteln[9] when avail slsmx and slslb.noteln[9] <> ""
     with frame f-arsszk.
     
      readkey.      
      return.      
      /* leave.   */
      
      hide frame f-arsszk no-pause.
      clear frame f-arsszk all.
      hide all no-pause.

end. /* not security */

     
     arsszk-updt:

     do while true with frame f-arsszk on endkey undo arsszk-updt,
                                                leave arsszk-updt:
       

    
    put screen row 11 column 3 
       color message
   " Technology                 SlsRep     (Salesman Override by Technology)  ".
     /*  end. */
 
    put screen row 21 col 6 color message   
      "F8-Add Contact Info   F9-Maintain Technology                         ".
      /*  pause 0.  */
   
   
         
         if x-sasos.securcd[12] = 5 then
           assign v-lastfieldname = "printfl3". 
         if b-sasos.securcd[9] >= 3 then
           assign v-lastfieldname = "printfl4".      
         if b-sasos.securcd[7] >= 3 then
           assign v-lastfieldname = "s-CCgeneric".
         if b-sasos.securcd[4] >= 3 then
           assign v-lastfieldname = "s-complogo".
         if b-sasos.securcd[14] >= 3 then
           assign v-lastfieldname = "s-custtype".   
   
       
       
       update  
        slsmx.printfl3    when (avail x-sasos and x-sasos.securcd[12] = 5) 
        slsmx.printfl4    when (avail b-sasos and b-sasos.securcd[9] >= 3)
        s-CCgeneric       when (avail b-sasos and b-sasos.securcd[7] >= 3)
        s-complogo        when (avail b-sasos and b-sasos.securcd[4] >= 3)
        s-custtype        when (avail b-sasos and b-sasos.securcd[14] >= 3) 
        editorloop:
        editing:
        readkey.


        if {k-cancel.i} or {k-jump.i} then do:
           apply lastkey.
           undo arsszk-updt, leave arsszk-updt.
                             
        end.

        
        if keylabel(lastkey) = "F8" /*and frame-field = "noteln"*/ then
          do:
          apply lastkey.
          leave arsszk-updt.
        end.

        
        if input s-CCgeneric = yes then          
         overlay(z-arss.user10,1,1) = "y".
        else                               
         overlay(z-arss.user10,1,1) = "n".

        
        if frame-field = "s-custtype" and
           {k-sdileave.i &fieldname = s-custtype 
                         &completename = s-custtype} then do:
          assign s-custtype = input s-custtype.
          if not {v-sasta.i "CU" s-custtype} then do:              
            run err.p (4162).
            next.
          end.  
          else
            overlay(z-arss.user21,1,3) = input s-custtype.
 
        end.    
          
        
        if frame-field = "s-complogo" and
           can-do ("9,13,306,307,308,309,310,401,501,502,503,504",
            string(lastkey)) then
          do:
          assign found-logo = no.
          for each z-logo where z-logo.cono = g-cono and
                                z-logo.notestype  = "zz" and           
                                z-logo.primarykey = "Company Logo"
                                no-lock:
            do idx= 1 to 16:
              if input s-complogo = z-logo.noteln[idx] then
                do:
                assign found-logo = yes.
                leave.
              end.
            end.
          end. /* each z-logo */
          if found-logo = no then
            do:
            message "Invalid Comany Logo".
            next-prompt s-complogo with frame f-arsszk.
            next.
          end.
          overlay(z-arss.user19,1,4) = input s-complogo.
        end. /* frame s-complogo*/

      if {k-accept.i} or {k-cancel.i} then
        do:
        hide frame f-arsszk.
        leave arsszk-updt.
      end.

       if {k-return.i} and frame-field = v-lastfieldname then do: 


         if x-sasos.securcd[12] = 5 then
           apply "entry" to slsmx.printfl3 in frame f-arsszk. 
         else if b-sasos.securcd[9] >= 3 then
           apply "entry" to slsmx.printfl4 in frame f-arsszk.      
         else if b-sasos.securcd[7] >= 3 then
           apply "entry" to s-CCgeneric in frame f-arsszk.
         else if b-sasos.securcd[4] >= 3 then
           apply "entry" to s-complogo in frame f-arsszk.  
         else if b-sasos.securcd[14] >= 3 then
           apply "entry" to s-custtype in frame f-arsszk.
   
          next editorloop.
        end.  

      
      apply lastkey.
     end. /* editing */
    
    end. /* do while true */
    

   
   
   end. /* g-secure ge 2 */

display
blank-line1
blank-line2
blank-line3
blank-line4
blank-line5
blank-line6
blank-line7
blank-line8
blank-line9
blank-line10
blank-line11    
blank-line12    
blank-line13    
blank-line14    
blank-line15    
blank-line16    
blank-line17    
blank-line18    
blank-line19    
with frame blank-screen2.


end. /* added */

     
     
     
     
     
