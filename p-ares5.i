/* p-ares5.i 1.12 06/15/94 */
/*h****************************************************************************
  INCLUDE      : p-ares5.i
  DESCRIPTION  : Print logic for ARES - statement format 1
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    07/13/92 pap; TB#  7191 Customer # change Use report.c12, not de12d0 for 
        cust #.
    05/24/93 jrg; TB#  3645 Added seqno so statements will print in the same 
        order everytime.
    06/24/94 jrg; TB# 11968 Added p-arroll.i to get SVs and Credits to go into 
        period balances when Special Transaction flag is set.
    07/12/93 jrg; TB# 12196 Moved p-rollsp.i info to ares1.p and aresf1.p 
        in order to print page footer info.
    08/16/93 jlc; TB# 12424 Running statements for a range of customers is slow.
    12/08/93 bj ; TB# 13761 Unable to print 0 balance accounts.
    06/15/94 fef; TB# 15721 Remove duplicate break by.
    11/09/94 rs ; TB# 17051 Transfer Info To Notification Msg.
    01/03/95 fef; TB# 16836 added display of s-totbal if arsc.statmentty ne  
        'b'.
    06/20/97 jkp; TB# 23081 Removed all occurrences of v-totbal as it is no 
        longer used.  Corrected the code changed by tb 16836.  See F20 notes in
        tb 23081 for examples of the orignal code.
    01/13/99 wpp; SS903 Changed the view of f-top to a display.
*******************************************************************************/

{&fax}

    /*tb 17051 11/09/94 rs; Transfer Info to Notification Msg. */
    {p-faxc2.i &extent = "4"
               &file   = "arsc."
               &out    = "report.outputty"
               &tag    = "'AR Statement'"}
               
/{&fax}* */

if v-bankno ne 10 and     /* BOA CAD$ */
   v-bankno ne 11 then
   do:
     {arbal.lca}
     /* {p-arbal.i}. SX55 */
   end.

/*tb  7191 07/13/92 pap; Customer # chg - use c12 instead of de12d0. */
/*tb 11968 06/24/93 jrg; Remove COD amount from balance (s-totalbal) if
     flag = no. */
/*tb 23081 06/20/97 jkp; Removed the assign of v-totbal (was v-totbal 
     = if arsc.statementty = "b" then v-lastbal else 0).  V-totbal is
     no longer used. */

assign
    v-addr[1]   = report.c24
    v-addr[2]   = report.c24-2
    v-addr[3]   = report.c24-3
    v-addr[4]   = report.c20 + ", " + report.c2 + " " + report.c13
    v-lastbal   = report.de9d2s
    v-lastdt    = report.dt
    v-c12       = report.c12
    v-continued = "Continued On Next Page"
    s-totalbal  = s-totalbal - 
                  (if not p-futinvfl then
                      arsc.futinvbal 
                  else 0)
    s-totalbal  = s-totalbal - 
                  (if g-arcodinbalfl then
                      arsc.codbal 
                  else 0).

/** das  05/21/14 - do only for US Dollars.  Canadian is done in
                    p-can-arroll.i.  We need to remove since misc and
                    credit balances are added above in arbal.lca **/
if x-nocred = yes and
   v-bankno ne 10 and  
   v-bankno ne 11 then  
   s-totalbal = s-totalbal + (arsc.misccrbal * -1) +  
                (arsc.uncashbal * -1).


/* already taken care of from above
/* do not supress credits and unapplied cash - Option 16 is no */
if x-nocred = no then
   s-totalbal = s-totalbal + arsc.misccrbal + arsc.uncashbal.
*/

do j = 1 to 3:

    if v-addr[j] = "" then
        assign
            v-addr[j]       = v-addr[j + 1]
            v-addr[j + 1]   = "".
            
end. /* do j = 1 to 3 */

view frame f-bottom.
if h-caninv = "**Canadian $$**" then
  view frame f-ctop.
else
  view frame f-top.  /* das - added f-top to appear on each page - BillTrust */
/* das - BillTrust */
/* wpp 01/13/99 Changed to a 'display' from a 'view'. */
/*
display 
    x-callmsg
    h-remitto     
    h-custno 
    h-coname   
    report.c12  
    h-coaddr[1]
    h-stmtdt
    h-coaddr[2] 
    p-stmtdt
    h-cocitystzip
    h-totdue   
    s-totalbal 
    v-addr[1]           
    h-amtpaid           
    v-addr[2]           
    h-caninv
    v-addr[3]           
    h-amtpaidln         
    v-addr[4]           
    h-remit1            
    h-remit2            
    v-msg               
    h-stmtdt2           
    h-custno2           
    p-stmtdt2           
    v-c12               
    h-headingln         
    h-uline  
with frame f-top.
*/

if arsc.statementty = "b" then
    display 
        v-lastdt 
        v-lastbal 
    with frame f-fwdbal.

/*tb  7191 07/13/92 pap; Customer # change.  Use c12 instead of de12d0. */
/*tb  3645 05/24/93 jrg; Added i7, i3, i4 (aret.seqno) to print order (break
     by). */
/*tb 12424 08/16/93 jlc; Report is running slow.  Plug report.de12d0 with 
     0 to keep the key. */
if p-invord = "d" then do:  /* wpp 01/16/99 Added the 'do' stmt. */
    /* wpp 01/17/99; View new page-top form */
    /*view frame f-custtop.*/
    if h-caninv = "**Canadian $$**" then
      view frame f-ctop.
    else
      view frame f-top.        /* das - BillTrust */
                                /** SX30 Sunsource **/
    for each b-report use-index k-treport where
             b-report.reportnm = v-reportnm and
             b-report.cono     = v-cono     and
             /** SX30 Sunsource **/
             /* b-report.oper2    = g-operinit and */
             b-report.outputty = "d"        and
             b-report.de12d0   = 0          and
             b-report.c12      = report.c12
    no-lock
        by b-report.dt
        by b-report.i7
        by b-report.i3
        by b-report.i4:
        /*
        if g-operinit = "das1" then
          do:
          message "B4 p-aresd5.i 1" s-totalbal. pause.
        end.
        */
        {p-aresd5.i "/*"}
        /*
        if g-operinit = "das1" then
          do:
          message "AFTER p-aresd5.i 1" s-totalbal. pause.
        end.
        */
            
    end.

    /* wpp 01/17/99; Hide new page-top form */
    /*hide frame f-custtop.*/
    hide frame f-ctop.
    hide frame f-top.  /* das - BillTrust */
end. /* if p-invord = "d" then for each b-report */

/*tb  7191 07/13/92 pap; Customer # change. Use c12 instead of de12d0. */
/*tb  3645 05/24/93 jrg; Added i7, i3, i4 (aret.seqno) to print order (break 
     by). */
/*tb 12424 08/16/93 jlc; Report is running slow. Plug report.de12d0 with 0 
     to keep the key. */
/*tb 15721 06/15/94 fef; Remove duplicate by report.c13. */
else if p-invord = "s" then do:  /* wpp 01/17/99 Added the 'do' stmt. */
    /* wpp 01/17/99 View the new page-top form */
    /*view frame f-custtop.*/
    if h-caninv = "**Canadian $$**" then
      view frame f-ctop.
    else
      view frame f-top.  /* BillTrust */
                                /** SX30 Sunsource **/
    for each b-report use-index k-treport where
             b-report.reportnm = v-reportnm and
             b-report.cono     = v-cono     and
             /** SX30 Sunsource **/
             /* b-report.oper2    = g-operinit and */
             b-report.outputty = "d"        and
             b-report.de12d0   = 0          and
             b-report.c12      = report.c12
    no-lock
    break by b-report.c13 
          by b-report.dt
          by b-report.i7
          by b-report.i3
          by b-report.i4:
        /*
        if g-operinit = "das1" then
          do:
          message "B4 p-aresd5.i 2" s-totalbal. pause.
        end.
        */
        {p-aresd5.i}
        /*
        if g-operinit = "das1" then
          do:
          message "AFTER p-aresd5.i 2" s-totalbal. pause.
        end.
        */
    
    end.
    /* wpp 01/17/99 Hide the new page-top form */
    /*hide frame f-custtop.*/
    hide frame f-ctop.
    hide frame f-top.  /* das - BillTrust */
        
end. /* else if p-invord = "s" then for each b-report */

/*tb  7191 07/13/92 pap; Customer # change. Use c12 instead of de12d0. */
/*tb  3645 05/24/93 jrg; Added i4 (aret.seqno) to print order (break by). */
/*tb 12424 08/16/93 jlc; Report is running slow. Plug report.de12d0 with 0 
     to keep the key. */
/*tb 23081 06/20/97 jkp; Removed the word "break" because no first-of or 
     last-of logic is being used and so this removes Progress overhead. */
else if p-invord = "i" then do:   /* wpp Added the 'do' stmt. */
    /* wpp 01/17/99 View the new page-top form */
    /*view frame f-custtop.*/
    if h-caninv = "**Canadian $$**" then
      view frame f-ctop.
    else
      view frame f-top.  /* das - BillTrust */

                                /** SX30 Sunsource **/
    for each b-report use-index k-treport where
             b-report.reportnm = v-reportnm and
             b-report.cono     = v-cono     and
             /** SX30 Sunsource **/
             /* b-report.oper2    = g-operinit and */
             b-report.outputty = "d"        and
             b-report.de12d0   = 0          and
             b-report.c12      = report.c12
    no-lock
        by b-report.c12
        by b-report.i7 
        by b-report.i3 
        by b-report.i4:
        /*
        if g-operinit = "das1" then
          do:
          message "B4 p-aresd5.i 3" s-totalbal. pause.
        end.
        */
        {p-aresd5.i "/*"}
        /*
        if g-operinit = "das1" then
          do:
          message "AFTER p-aresd5.i 3" s-totalbal. pause.
        end.
        */
    
    end.
    
    /* wpp 01/17/99 Hide the new page-top form */
    /*hide frame f-custtop.*/
    hide frame f-ctop.
    hide frame f-top.  /* das - BillTrust */
end. /* else if p-invord = "i" then for each b-report */

/*tb 16836 01/03/95 fef; Added else display s-totalbal. */
/*tb 23081 06/20/97 jkp; Removed "if arsc.statementty = "b" then 
     display v-totbal with frame f-tot. else".  Removed "@ v-totbal"
     from display of s-totalbal.  The change with tb 16836
     was really to add "display v-totbal with frame f-tot. else". */
/*assign s-totalbal = v-subtot.*/  /* das - 12/15/08   
                                  currently s-totalbal is assigned from
                                  arsc.futinvbal or arsc.codbal */
display s-totalbal with frame f-tot.

if p-chksummfl and p-actonlyfl then
do:

    /*tb  7191 07/13/92 pap; Customer # change.  Use c12 for de12d0. */
    /*tb 12424 08/16/93 jlc; Report is running slow.  Plug report.de12d0 with 
         0 to keep the key. */
    /*tb 15721 06/15/94 fef; Remove duplicate by b-report.i7. */
                                /** SX30 Sunsource **/
    for each b-report use-index k-treport where
             b-report.reportnm = v-reportnm      and
             b-report.cono     = v-cono          and
             /** SX30 Sunsource **/
             /* b-report.oper2    = g-operinit and */
             b-report.outputty = "d"             and
             b-report.de12d0   = 0               and
             b-report.c12      = report.c12      and
             b-report.c1       = "p"        
    no-lock
    break by b-report.i7:

        if first(b-report.i7) then
        do:
        
            display with frame f-chksumm.
                    
        end. /* if first(b-report.i7) */
        
        display
            b-report.dt
            b-report.c20    @ v-invoice
            b-report.de9d2s @ v-credit
        with frame f-detail.
        down with frame f-detail.
        
    end. /* for each b-report */
    
    hide frame f-chksumm.
    
end. /* if p-chksummfl and p-actonlyfl */

/*tb 13761 12/08/93 bj; Unable to print 0 balance accounts. */
if line-counter eq 1 then 
    display with frame f-detail.

v-continued = "".

if not v-faxfl then page.

{&fax}

    display with frame f-bottom.
    down with frame f-bottom.

/* wpp 01/13/99 Don't need this since not a page-top form  hide frame f-top. */
    hide frame f-top.    /* das - Put back for BillTrust */
    hide frame f-ctop.
    hide frame f-bottom.
    
    if not (sasp.pcommand begins "fx") then
        put unformatted "^T".
        
    output close.
    
/{&fax}* */
