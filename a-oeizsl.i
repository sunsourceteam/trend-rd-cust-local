/*h*****************************************************************************
  INCLUDE      : a-oeiol.i
  DESCRIPTION  : OEIO Line Detail Function keys
  USED ONCE?   : yes
  AUTHOR       : rhl
  DATE WRITTEN : 09/20/89
  CHANGES MADE : 
**************************************************************************** */
if {k-func.i} then do:
  /*d Set OE Line Global and find the OEEL Line */
  {p-oeizsl.i}

  if {k-func6.i} and avail oeelb then 
    do:
    put screen row 21 col 4 color message "F6-RETURN".
    run oeiolr.p(s-prod).
  end.
  else if {k-func7.i} and avail oeelb then 
    do:
    find b-oeelb where b-oeelb.cono = g-cono and
                       b-oeelb.batchnm = string(v-batchnm,">>>>>>>9") and
                       b-oeelb.lineno = g-oelineno and
                       b-oeelb.seqno = 2
                       no-lock no-error.
    if avail b-oeelb then
      do:
      assign v-lineno   = b-oeelb.lineno 
             v-prod     = b-oeelb.shipprod
             v-vendno   = b-oeelb.arpvendno
             v-totnet   = b-oeelb.netamt
             v-qty      = b-oeelb.qtyord
             v-prodcost = b-oeelb.prodcost
             v-totcost  = b-oeelb.prodcost * b-oeelb.qtyord.
      run oeexclius.p.
    end.
  end.
  else if {k-func9.i} and avail oeelb then
    do:
    put screen row 21 col 49 color message "F9-HISTORY".
    assign v-lineno = oeelb.lineno.
    run oeexchist.p.   
  end.
  put screen row 21 col 3 color message v-fmsg.
  if {k-jump.i} or {k-select.i} then leave main.
  hide message no-pause.
  next.
end.

else if {k-func19.i} then do:
  assign g-quoteno = int(v-batchnm).
  run zsdicomd.p.
end.

