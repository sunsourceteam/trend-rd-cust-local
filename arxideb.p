def     shared var b-customer          as de   format "999999999999"  no-undo.
def     shared var e-customer          as de   format "999999999999"  no-undo.
def     shared var b-invdate           as date format "99/99/99"      no-undo.
def     shared var e-invdate           as date format "99/99/99"      no-undo.
def     shared var o-rpttype           as c    format "x(1)"          no-undo.
def     shared var o-disprej               as c    format "x(3)"          no-undo.


def     shared var accum-totals        as de   format ">>>,>>9.99-"   no-undo.

def            var customer            as de format "999999999999"    no-undo.
def            var s-name              as c  format "x(30)"           no-undo.
def            var s-report-type       as c  format "x(20)"           no-undo.
def            var s-customer          as i  format ">>>>>>>>>999"    no-undo.
def            var w-customer          as i  format ">>>>>>>>>999"    no-undo.

def            var orig-amount         like aret.amount               no-undo.
def            var paid-amount         like aret.amount               no-undo.
def            var due-amount          like aret.amount               no-undo.
def            var st-orig-amt         as de format "->>>,>>9.99"     no-undo.
def            var st-paid-amt         as de format "->>>,>>9.99"     no-undo.
def            var st-due-amt          as de format "->>>,>>9.99"     no-undo.
def            var st-count            as i  format ">>,>>9"          no-undo.

def            var g-count             as i  format ">>,>>9"          no-undo.
def            var g-orig-amt          as de format "->>>,>>9.99"     no-undo.
def            var g-paid-amt          as de format "->>>,>>9.99"     no-undo.
def            var g-due-amt           as de format "->>>,>>9.99"     no-undo.

def            var net-orig-amt        as de format "->>>,>>9.99"     no-undo.
def            var net-paid-amt        as de format "->>>,>>9.99"     no-undo.
def            var net-due-amt         as de format "->>>,>>9.99"     no-undo.
      

/*
def            var w-refer             like aret.refer                no-undo.
*/
def            var page-count          as i format "99"               no-undo.

def            var count               as i  format ">>,>>9"          no-undo.
def            var tot-amt             as de format ">>>,>>9.99-"     no-undo.
def            var totsales            as de format ">>>,>>9.99-"     no-undo.
def            var w-refer             as c  format "x(7)"            no-undo.
def            var disp-amt            as de format ">>>,>>9.99-"     no-undo.
def            var disp-count          as i  format ">>,>>9"          no-undo.


def buffer z-aret for aret.

def     shared temp-table debits no-undo
   field customer     as  i    format ">>>>>>>>>999"
   field invoice      as  i    format ">>>>>>9"
   field suffx        as  i    format "99"
   field seq          as  i    format "999"
   field transtype    as  c    format "x(2)"
   field stage        as  c    format "x(2)"
   field reference    as  c    format "x(25)"
   field invdate      as  date format "99/99/99"
   field journal      as  i    format ">>>>>>>>"
   field orig-amt     as  de   format "->>>,>>9.99"
   field paid-amt     as  de   format "->>>,>>9.99"
   field due-amt      as  de   format "->>>,>>9.99"
   field doctype      as  c    format "x(4)"
   field e1           as  c    format "x(3)"
   field error1       as  c    format "x(50)"
   field inv-status   as  c    format "x(50)"
   field type         as  c    format "x(10)"
   field takenby      as  c    format "x(4)"
   field po           as  c    format "x(12)"

   index k-debits
       customer
       type
       invoice
       suffx
       seq
       
   index k-amount
       type
       orig-amt descending
         
   index k-credit
       type
       orig-amt ascending
   
   index k-error
       type
       e1 ascending
       orig-amt descending.
         
{g-all.i}

form
"Open Reject Order Report" at 1
skip (1)                      
"Ord"                 at  13
"Invoice"             at  39  
"Invoice"             at  50  
"Doc"                 at  63  
"Order #"             at   2  
"Typ"                 at  13  
"Refer"               at  18
"PO"                  at  26
"Date"                at  39  
"Amount"              at  50  
"Type"                at  63  
"Error 1"             at  68  
"Status"              at 119  
"Tknby"               at 170
"----------"          at   1
"---"                 at  13
"-------"             at  18
"------------"        at  26
"----------"          at  39
"-----------"         at  50
"----"                at  63
"--------------------------------------------------" at  68
"--------------------------------------------------" at 119 
"-----"                                              at 170
with frame f-rej-header no-underline no-box no-labels page-top down width 178.

form                         
debits.invoice         at   1
"-"                    at   8
debits.suffx           at   9
debits.transtype       at  13
w-refer                at  18
debits.po              at  26
debits.invdate         at  39
debits.orig-amt        at  50
debits.doctype         at  63
debits.error1          at  68
debits.inv-status      at 119
debits.takenby         at 170
with frame f-rej-detail no-underline no-box no-labels down width 178.

form                                          
skip (2)                                      
"Total Open Rejected Orders:"             at 9
count                 at 37                   
tot-amt               at 46                   
with frame f-rej-totals no-underline no-box no-labels down width 178.


form                                          
skip (2)                                      
"Total Open Rejected Orders:"             at 9
count                 at 37                   
tot-amt               at 46  
"/"                   at 58
disp-count            at 61
disp-amt              at 70

with frame f-rej-totals_2 no-underline no-box no-labels down width 178.



form
"Open AR Reports"          at   1
skip (1)
"SHORT PAY (Cash) - INVOICES" at   1
skip (1)
"Invoice"                  at  21
"Original"                 at  74
"Paid"                     at  87
"Amount"                   at 100
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21
"Reference"                at  31
"Stage"                    at  57
"Journal#"                 at  64
"Amount"                   at  74
"Amount"                   at  87
"Due"                      at 100
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31
"-----"                    at  57
"--------"                 at  64
"-----------"              at  74
"-----------"              at  87
"-----------"              at 100
"------------------------------------------------------------" at 113
with frame f-ppi-hdr no-underline no-box page-top no-labels  down width 178.

form                             
"Open AR Reports"          at   1
skip (1)                         
"SHORT PAY (Credit)- INVOICES" at   1
skip (1)                         
"Invoice"                  at  21
"Original"                 at  74
"Paid"                     at  87
"Amount"                   at 100
                                 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21
"Reference"                at  31
"Stage"                    at  57
"Journal#"                 at  64
"Amount"                   at  74
"Amount"                   at  87
"Due"                      at 100
"Status"                   at 141
"------------------------------------------------------------" at 113       
with frame f-crppi-hdr no-underline no-box page-top no-labels  down width 178.


form                             
"                                
Open AR Reports"           at   1
skip (1)                         
"REBATES - DEBITS"         at   1
skip (1)
"Invoice"                  at  21
"Original"                 at  74
"Paid"                     at  87
"Amount"                   at 100

"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141

"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31
"-----"                    at  57
"--------"                 at  64
"-----------"              at  74
"-----------"              at  87
"-----------"              at 100
"------------------------------------------------------------" at 113
with frame f-reb-hdr no-underline no-box page-top no-labels  down width 178.

form                             
"                                
Open AR Reports"           at   1
skip (1)                         
"BUYBACKS - DEBITS"        at   1
skip (1)
"Invoice"                  at  21
"Original"                 at  74
"Paid"                     at  87
"Amount"                   at 100
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31
"-----"                    at  57
"--------"                 at  64
"-----------"              at  74
"-----------"              at  87
"-----------"              at 100
"------------------------------------------------------------" at 113
with frame f-buy-hdr no-underline no-box page-top no-labels  down width 178.

form                              
"                                 
Open AR Reports"           at   1 
skip (1)                          
"RETURNS - DEBITS"         at   1 
skip(1)
"Invoice"                  at  21 
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4 
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31 
"-----"                    at  57 
"--------"                 at  64 
"-----------"              at  74 
"-----------"              at  87 
"-----------"              at 100 
"------------------------------------------------------------" at 113
with frame f-ret-hdr no-underline no-box page-top no-labels  down width 178.

form                              
"                                 
Open AR Reports"           at   1 
skip (1)                          
"FREIGHT - DEBITS"         at   1 
skip (1)
"Invoice"                  at  21 
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31
"-----"                    at  57
"--------"                 at  64
"-----------"              at  74
"-----------"              at  87
"-----------"              at 100
"------------------------------------------------------------" at 113
with frame f-fre-hdr no-underline no-box page-top no-labels  down width 178.

form                              
"                                 
Open AR Reports"           at   1 
skip (1)                          
"cat CLAIMS DOUBLE PAID - DEBITS"   at   1  
skip (1)
"Invoice"                  at  21 
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31
"-----"                    at  57
"--------"                 at  64
"-----------"              at  74
"-----------"              at  87
"-----------"              at 100
"------------------------------------------------------------" at 113
with frame f-dou-hdr no-underline no-box page-top no-labels  down width 178.

form                              
"                                 
Open AR Reports"           at   1 
skip (1)                          
"PRICING - DEBITS"         at   1  
skip (1)
"Invoice"                  at  21 
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4 
"---"                      at  16
"--------"                 at  21
"------------------------" at  31 
"-----"                    at  57 
"--------"                 at  64 
"-----------"              at  74 
"-----------"              at  87 
"-----------"              at 100 
"------------------------------------------------------------" at 113
with frame f-pri-hdr no-underline no-box page-top no-labels  down width 178.

form                              
"                                 
Open AR Reports"           at   1 
skip (1)                          
"PROOF OF DELIVERY - DEBITS" at   1  
skip (1)
"Invoice"                  at  21
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21 
"------------------------" at  31 
"-----"                    at  57 
"--------"                 at  64 
"-----------"              at  74
"-----------"              at  87 
"-----------"              at 100 
"------------------------------------------------------------" at 113
with frame f-pod-hdr no-underline no-box page-top no-labels  down width 178.

form                              
"                                 
Open AR Reports"           at   1 
skip (1)                          
"MISCELLANEOUS - DEBITS"   at   1  
skip (1)
"Invoice"                  at  21
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21 
"------------------------" at  31 
"-----"                    at  57 
"--------"                 at  64 
"-----------"              at  74 
"-----------"              at  87 
"-----------"              at 100 
"------------------------------------------------------------" at 113
with frame f-mis-hdr no-underline no-box page-top no-labels  down width 178.


form                             
"                                
Open AR Reports"           at   1
skip (1)                         
"TRANSFER VOUCHERS - DEBITS" at   1
skip (1)
"Invoice"                  at  21
"Original"                 at  74
"Paid"                     at  87
"Amount"                   at 100
"Order"                    at   4
"Seq"                      at  16 
"Date"                     at  21
"Reference"                at  31
"Stage"                    at  57
"Journal#"                 at  64
"Amount"                   at  74
"Amount"                   at  87
"Due"                      at 100
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21 
"------------------------" at  31 
"-----"                    at  57 
"--------"                 at  64 
"-----------"              at  74 
"-----------"              at  87 
"-----------"              at 100 
"------------------------------------------------------------" at 113
with frame f-tra-hdr no-underline no-box page-top no-labels  down width 178.

form                             
"                                
Open AR Reports"           at   1
skip (1)                        
"OTHER - DEBITS"           at   1
skip (1)                         
"Invoice"                  at  21
"Original"                 at  74
"Paid"                     at  87
"Amount"                   at 100
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21
"Reference"                at  31
"Stage"                    at  57
"Journal#"                 at  64
"Amount"                   at  74
"Amount"                   at  87
"Due"                      at 100
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21 
"------------------------" at  31 
"-----"                    at  57 
"--------"                 at  64 
"-----------"              at  74 
"-----------"              at  87 
"-----------"              at 100 
"------------------------------------------------------------" at 113
with frame f-unk-hdr no-underline no-box page-top no-labels  down width 178.


form                             
"                                
Open AR Reports"           at   1
skip (1)                         
"CONSUMPTION - INVOICES"   at   1
skip (1)                         
"Invoice"                  at  21
"Original"                 at  74
"Paid"                     at  87
"Amount"                   at 100
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21
"Reference"                at  31
"Stage"                    at  57
"Journal#"                 at  64
"Amount"                   at  74
"Amount"                   at  87
"Due"                      at 100
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31
"-----"                    at  57
"--------"                 at  64
"-----------"              at  74
"-----------"              at  87
"-----------"              at 100
"------------------------------------------------------------" at 113       
with frame f-cns-hdr no-underline no-box page-top no-labels  down width 178.


form                              
"                                 
Open AR Reports"           at   1 
skip (1)                          
"OTHER - POR - INVOICES"   at   1 
skip (1)
"Invoice"                  at  21
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21 
"------------------------" at  31 
"-----"                    at  57 
"--------"                 at  64 
"-----------"              at  74 
"-----------"              at  87 
"-----------"              at 100 
"------------------------------------------------------------" at 113
with frame f-oth-p-hdr no-underline no-box page-top no-labels  down width 178.

form                              
"                                 
Open AR Reports"           at   1 
skip (1)                          
"OTHER - NON POR - INVOICES" at   1 
skip (1)
"Invoice"                  at  21 
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21 
"------------------------" at  31 
"-----"                    at  57 
"--------"                 at  64 
"-----------"              at  74 
"-----------"              at  87 
"-----------"              at 100 
"------------------------------------------------------------" at 113
with frame f-oth-hdr no-underline no-box page-top no-labels  down width 178.
/***************************************************************************/

form                              
"                                                             Open AR Reports - Miscellaneous Credits"     at   1 
skip (1)                          
"MC - BUYBACKS"            at   1 
skip (1)                          
"Invoice"                  at  21
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21 
"------------------------" at  31 
"-----"                    at  57 
"--------"                 at  64 
"-----------"              at  74
"-----------"              at  87 
"-----------"              at 100 
"------------------------------------------------------------" at 113
with frame f-mc-buy-hdr no-underline no-box page-top no-labels  down width 178.

form
"                                                             Open AR Reports -
Miscellaneous Credits"     at   1 
skip (1)                          
"MC - CUSTOMER ERROR"      at   1 
skip (1)                          
"Invoice"                  at  21
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31
"-----"                    at  57
"--------"                 at  64
"-----------"              at  74
"-----------"              at  87
"-----------"              at 100
"------------------------------------------------------------" at 113
with frame f-mc-cst-hdr no-underline no-box page-top no-labels  down width 178.


form
"                                                             Open AR Reports - 
Miscellaneous Credits"     at   1 
skip (1)                          
"MC - MASS RM PROJECT"     at   1 
skip (1)                          
"Invoice"                  at  21
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31
"-----"                    at  57
"--------"                 at  64
"-----------"              at  74
"-----------"              at  87
"-----------"              at 100
"------------------------------------------------------------" at 113
with frame f-mc-rm-hdr no-underline no-box page-top no-labels  down width 178.


form
"                                                             Open AR Reports - 
Miscellaneous Credits"     at   1 
skip (1)                          
"MC - OE ERROR"            at   1 
skip (1)                          
"Invoice"                  at  21
"Original"                 at  74
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31
"-----"                    at  57
"--------"                 at  64
"-----------"              at  74
"-----------"              at  87
"-----------"              at 100
"------------------------------------------------------------" at 113
with frame f-mc-oe-hdr no-underline no-box page-top no-labels  down width 178.


form
"                                                             Open AR Reports - 
Miscellaneous Credits"     at   1 
skip (1)                          
"MC - PRICING"             at   1 
skip (1)                          
"Invoice"                  at  21
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31
"-----"                    at  57
"--------"                 at  64
"-----------"              at  74
"-----------"              at  87
"-----------"              at 100
"------------------------------------------------------------" at 113
with frame f-mc-pri-hdr no-underline no-box page-top no-labels  down width 178.


form
"                                                             Open AR Reports - 
Miscellaneous Credits"     at   1 
skip (1)                          
"MC - SHIPPING"            at   1 
skip (1)                          
"Invoice"                  at  21
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31
"-----"                    at  57
"--------"                 at  64
"-----------"              at  74
"-----------"              at  87
"-----------"              at 100
"------------------------------------------------------------" at 113
with frame f-mc-shi-hdr no-underline no-box page-top no-labels  down width 178.


form
"                                                             Open AR Reports - Miscellaneous Credits"     at   1 
skip (1)                          
"MC - VENDOR ERROR"        at   1 
skip (1)                          
"Invoice"                  at  21
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21 
"------------------------" at  31 
"-----"                    at  57 
"--------"                 at  64 
"-----------"              at  74 
"-----------"              at  87 
"-----------"              at 100 
"------------------------------------------------------------" at 113
with frame f-mc-ven-hdr no-underline no-box page-top no-labels  down width 178.


form
"                                                             Open AR Reports - 
Miscellaneous Credits"     at   1 
skip (1)                          
"MC - OTHER"               at   1 
skip (1)                          
"Invoice"                  at  21
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31
"-----"                    at  57
"--------"                 at  64
"-----------"              at  74
"-----------"              at  87
"-----------"              at 100
"------------------------------------------------------------" at 113
with frame f-mc-oth-hdr no-underline no-box page-top no-labels  down width 178.
/***************************************************************************/


form
"                                                             Open AR Reports -
Unapplied Cash"            at   1 
skip (1)                          
"UC - DUPLICATE PAYMENT"   at   1 
skip (1)                          
"Invoice"                  at  21
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31
"-----"                    at  57
"--------"                 at  64
"-----------"              at  74
"-----------"              at  87
"-----------"              at 100
"------------------------------------------------------------" at 113
with frame f-uc-dup-hdr no-underline no-box page-top no-labels  down width 178.


form
"                                                             Open AR Reports -
Unapplied Cash"            at   1  
skip (1)                           
"UC - OVER PAY"            at   1  
skip (1)                           
"Invoice"                  at  21
"Original"                 at  74  
"Paid"                     at  87  
"Amount"                   at 100  
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21  
"Reference"                at  31  
"Stage"                    at  57  
"Journal#"                 at  64  
"Amount"                   at  74  
"Amount"                   at  87  
"Due"                      at 100  
"Status"                   at 141 
"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31
"-----"                    at  57
"--------"                 at  64
"-----------"              at  74
"-----------"              at  87
"-----------"              at 100
"------------------------------------------------------------" at 113
with frame f-uc-ovr-hdr no-underline no-box page-top no-labels  down width 178.


form
"                                                             Open AR Reports -
Unapplied Cash"            at   1 
skip (1)                          
"UC - PAID ITEM"           at   1 
skip (1)                          
"Invoice"                  at  21
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31
"-----"                    at  57
"--------"                 at  64
"-----------"              at  74
"-----------"              at  87
"-----------"              at 100
"------------------------------------------------------------" at 113
with frame f-uc-pd-hdr no-underline no-box page-top no-labels  down width 178.


form
"                                                             Open AR Reports -
Unapplied Cash"            at   1  
skip (1)                           
"UC - UNIDENTIFIED"        at   1  
skip (1)                           
"Invoice"                  at  21
"Original"                 at  74  
"Paid"                     at  87  
"Amount"                   at 100  
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21  
"Reference"                at  31  
"Stage"                    at  57  
"Journal#"                 at  64  
"Amount"                   at  74  
"Amount"                   at  87  
"Due"                      at 100  
"Status"                   at 141 
"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31
"-----"                    at  57
"--------"                 at  64
"-----------"              at  74
"-----------"              at  87
"-----------"              at 100
"------------------------------------------------------------" at 113
with frame f-uc-und-hdr no-underline no-box page-top no-labels  down width 178.


form
"                                                             Open AR Reports - Unapplied Cash"            at   1 
skip (1)                          
"UC - NO INVOICE FOUND/NOT INVOICED"    at   1 
skip (1)                          
"Invoice"                  at  21
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31
"-----"                    at  57
"--------"                 at  64
"-----------"              at  74
"-----------"              at  87
"-----------"              at 100
"------------------------------------------------------------" at 113
with frame f-uc-inv-hdr no-underline no-box page-top no-labels  down width 178.


form
"                                                             Open AR Reports -
Unapplied Cash"            at   1 
skip (1)                          
"UC - OTHER"               at   1 
skip (1)                          
"Invoice"                  at  21
"Original"                 at  74 
"Paid"                     at  87 
"Amount"                   at 100 
"Order"                    at   4
"Seq"                      at  16
"Date"                     at  21 
"Reference"                at  31 
"Stage"                    at  57 
"Journal#"                 at  64 
"Amount"                   at  74 
"Amount"                   at  87 
"Due"                      at 100 
"Status"                   at 141
"----------"               at   4
"---"                      at  16
"--------"                 at  21
"------------------------" at  31
"-----"                    at  57
"--------"                 at  64
"-----------"              at  74
"-----------"              at  87
"-----------"              at 100
"------------------------------------------------------------" at 113
with frame f-uc-oth-hdr no-underline no-box page-top no-labels  down width 178.


/***************************************************************************/

form
debits.invoice             at   4
"-"                        at  11
debits.suffx               at  12
debits.seq                 at  16
debits.invdate             at  21
debits.reference           at  31
debits.stage               at  59
debits.journal             at  64
debits.orig-amt            at  74
debits.paid-amt            at  87
debits.due-amt             at 100
debits.inv-status          at 113
with frame f-detail no-underline no-box no-labels down width 178.

form 
skip (1)
"Sub Totals:"              at  53
st-count                   at  65
st-orig-amt                at  74
st-paid-amt                at  87
st-due-amt                 at 100
with frame f-subtotal no-underline no-box no-labels down width 178.

form
skip(2)
"Grand Totals:"            at  21
g-count                    at  65
g-orig-amt                 at  74
g-paid-amt                 at  87
g-due-amt                  at 100
with frame f-grand no-underline no-box no-labels down width 178.

form
"Original"                 at  74
"Paid"                     at  87
"Amount"                   at 100
"Amount"                   at  74
"Amount"                   at  87
"Due"                      at 100
"-----------"              at  74
"-----------"              at  87
"-----------"              at  100
skip (2)
"Net Totals:"              at  51                               
net-orig-amt               at  74                               
net-paid-amt               at  87                               
net-due-amt                at 100                               
with frame f-net no-underline no-box no-labels down width 178.



/* Program Start */

if b-customer = 233915 or b-customer = 233911 or b-customer = 233914 or
   b-customer = 1100025274 then do:
   assign customer = b-customer.
   run "process-oeeh".
   run "process-aret".
   assign customer = 233912.
   run "process-oeeh".
   run "process-aret".
end.
else do:
   assign customer = b-customer.
   run "process-oeeh".
   run "process-aret".
end.
                  


procedure process-oeeh.
for each oeeh use-index k-custno where oeeh.cono = g-cono and
                                       oeeh.custno = customer and
                                       oeeh.stagecd = 4 and
                                      (oeeh.transtype = "so" or
                                       oeeh.transtype = "do" or
                                       oeeh.transtype = "br") and
                                       oeeh.invoicedt >= b-invdate and
                                       oeeh.invoicedt <= e-invdate 
                                       no-lock:
                                       
   if b-customer = 233912 and customer = 233912 and oeeh.shipto  = "E" 
      then next.
   
   if b-customer = 233915 and customer = 233912 and oeeh.shipto <> "E" 
      then next.
      
   if b-customer = 233911 and customer = 233912 and oeeh.shipto <> "E" 
      then next.
      
   if b-customer = 233914 and customer = 233912 and oeeh.shipto <> "E"
      then next.
      
   if b-customer = 1100025274 and customer = 233912 then next.
   
   assign orig-amount = 0.
   assign paid-amount = 0.
   assign due-amount  = 0.
   
   for each aret use-index k-cod where aret.cono = g-cono and
                                       aret.invno = oeeh.orderno and
                                       aret.invsuf = oeeh.ordersuf and
                                       aret.statustype = yes and
                                       aret.transcd = 0
                                       no-lock:
                                       
      assign orig-amount = orig-amount + aret.amount.
      assign paid-amount = paid-amount + 
                          (aret.paymtamt + aret.discamt - aret.pifamt).
   
      assign due-amount = due-amount + (orig-amount - paid-amount).
   
      find first debits use-index k-debits where 
                                        debits.customer  = int(oeeh.custno) and
                                        debits.invoice   = oeeh.orderno and
                                        debits.suffx     = oeeh.ordersuf
                                        no-error.
           
      if avail debits then do:
         if debits.type = "consig" or debits.type = "824" then next.
         assign debits.paid-amt = debits.paid-amt + paid-amount.
         assign debits.due-amt  = debits.due-amt  - due-amt.
      end.
      else do:
         create debits.
         assign debits.customer   = aret.custno
                debits.invoice    = aret.invno
                debits.suffx      = aret.invsuf
                debits.seq        = aret.seqno
                debits.stage      = "IN"
                debits.reference  = oeeh.refer
                debits.invdate    = aret.invdt
                debits.journal    = aret.jrnlno
                debits.orig-amt   = orig-amount
                debits.paid-amt   = paid-amount
                debits.due-amt    = due-amount
                debits.inv-status = substring(aret.user5,10,50)
                debits.takenby    = oeeh.takenby
                debits.po         = oeeh.custpo.
                
         if debits.paid-amt > 0 then do:
            for each z-aret use-index k-cod where 
                                          z-aret.cono = g-cono and
                                          z-aret.invno = oeeh.orderno and
                                          z-aret.invsuf = oeeh.ordersuf and
                                          z-aret.statustype = no and
                                          z-aret.transcd = 11
                                          no-lock:                       
               do i = 1 to 18:
                  if substring(z-aret.refer,i,1)     = "c" and
                     substring(z-aret.refer,i + 1,1) = "r" and
                     substring(z-aret.refer,i + 2,1) = "#" then do:
                     assign debits.type = "crppi"
                            debits.inv-status = substring(oeeh.user5,10,50)
                            debits.reference = z-aret.refer.
                     leave.
                  end.
               end.
            end.
             
            if debits.type <> "crppi" then do:
               assign debits.type       = "ppi".
               assign debits.inv-status = substring(oeeh.user5,10,50).
            end.
         end.
         else do:
            if substring(oeeh.user2,1,3) = "862" then do:
               assign debits.inv-status = substring(oeeh.user5,10,50).
               assign debits.type       = "open por".
            end.
            else
               if substring(oeeh.user2,1,3) = "846" then do:
                  assign debits.inv-status = substring(oeeh.user5,10,50).
                  assign debits.type       = "consume".
               end.
               else do:
                  assign debits.inv-status = substring(oeeh.user5,10,50).
                  assign debits.type       = "open".
               end.
         end.
      end.
   end.  /* for each aret */       
end. /* for each oeeh */
end. /* procedure process-oeeh */


procedure process-aret:
for each aret use-index k-invno where aret.cono = g-cono and
                                      aret.custno = customer and
                                      aret.statustype = yes and
                                      aret.transcd = 0 and
                                      aret.invsuf = 99 and
                                      aret.invdt >= b-invdate and
                                      aret.invdt <= e-invdate
                                      no-lock:
                                      
    find first oeeh use-index k-oeeh where oeeh.cono = g-cono and
                                           oeeh.orderno = aret.invno and
                                           oeeh.ordersuf = 0
                                           no-lock no-error.
    if avail oeeh and b-customer = 233912 and customer = 233912 and
       oeeh.shipto  = "E" then next.
    
    if avail oeeh and b-customer = 233915 and customer = 233912 and
       oeeh.shipto <> "E" then next.
       
    if avail oeeh and b-customer = 233911 and customer = 233912 and
       oeeh.shipto <> "E" then next.
    
    if avail oeeh and b-customer = 233914 and customer = 233912 and
       oeeh.shipto <> "E" then next.
       
    if avail oeeh and b-customer = 1100025274 and customer = 233912 then next.
    
    if not avail oeeh and 
       (b-customer = 233915 or b-customer = 233911 or b-customer = 233914) and 
       customer = 233912 then next.
    
    find first debits use-index k-debits where 
                                         debits.customer = int(aret.custno) and
                                         debits.invoice  = aret.invno and
                                         debits.suffx    = aret.invsuf and
                                         debits.seq      = aret.seq
                                         no-error.
    if avail debits then do:
       if debits.type = "consig" or debits.type = "824" then next.
       assign debits.paid-amt = debits.paid-amt + aret.paymtamt
              debits.due-amt  = debits.due-amt - (aret.amount - aret.paymtamt).
    end.
       
    create debits.
    assign debits.customer   = aret.custno
           debits.invoice    = aret.invno
           debits.suffx      = aret.invsuf
           debits.seq        = aret.seqno
           debits.stage      = "IN"
           debits.reference  = aret.refer
           debits.invdate    = aret.invdt
           debits.journal    = aret.jrnlno
           debits.inv-status = substring(aret.user5,10,50)
           debits.orig-amt   = aret.amount
           debits.paid-amt   = (aret.paymtamt + aret.discamt - aret.pifamt)
           debits.due-amt    = aret.amount - (aret.paymtamt + aret.discamt
                                                           - aret.pifamt).
                                                           
    do i = 1 to 22:
       if substring(aret.refer,i,1)     = "r" and
          substring(aret.refer,i + 1,1) = "e" and
          substring(aret.refer,i + 2,1) = "b" then do:
             assign debits.type = "rebate".
             leave.
       end.
    end.
    if debits.type = "" then do:
       do i = 1 to 22:
          if substring(aret.refer,i,1)     = "b" and 
             substring(aret.refer,i + 1,1) = "u" and 
             substring(aret.refer,i + 2,1) = "y" then do:
             assign debits.type = "buyback".
             leave.
          end.
       end.
    end.
    if debits.type = "" then do:
       do i = 1 to 22:
          if substring(aret.refer,i,1)     = "r" and
             substring(aret.refer,i + 1,1) = "e" and
             substring(aret.refer,i + 2,1) = "t" then do:
             assign debits.type = "return".
             leave.
          end.
       end.
    end.
    if debits.type = "" then do:
       do i = 1 to 21:
          if substring(aret.refer,i,1)     = "f" and
             substring(aret.refer,i + 1,1) = "r" and
             substring(aret.refer,i + 2,1) = "e" and
             substring(aret.refer,i + 3,1) = "i" then do:
             assign debits.type = "freight".
             leave.
          end.
       end.
    end.
    if debits.type = "" then do:
       do i = 1 to 21:
          if substring(aret.refer,i,1)     = "d" and
             substring(aret.refer,i + 1,1) = "o" and
             substring(aret.refer,i + 2,1) = "u" and
             substring(aret.refer,i + 3,1) = "b" then do:
             assign debits.type = "double".
             leave.
          end.
       end.
    end.
    if debits.type = "" then do:
       do i = 1 to 21:
          if substring(aret.refer,i,1)     = "p" and
             substring(aret.refer,i + 1,1) = "r" and
             substring(aret.refer,i + 2,1) = "i" and
             substring(aret.refer,i + 3,1) = "c" then do:
             assign debits.type = "pricing".
             leave.
          end.
       end.
    end.
    if debits.type = "" then do:
       do i = 1 to 22:
          if substring(aret.refer,i,1)     = "p" and
             substring(aret.refer,i + 1,1) = "o" and
             substring(aret.refer,i + 2,1) = "d" then do:
             assign debits.type = "pod".
             leave.
          end.
       end.
    end.
    if debits.type = "" then do:
       do i = 1 to 21:
          if substring(aret.refer,i,1)     = "m" and
             substring(aret.refer,i + 1,1) = "i" and
             substring(aret.refer,i + 2,1) = "s" and
             substring(aret.refer,i + 3,1) = "c" then do:
             assign debits.type = "misc".
             leave.
          end.
       end.
    end.
    if debits.type = "" then do:                         
       do i = 1 to 21:                                   
          if substring(aret.refer,i,1)     = "t" and
             substring(aret.refer,i + 1,1) = "r" and
             substring(aret.refer,i + 2,1) = "a" and
             substring(aret.refer,i + 3,1) = "n" then do:
             assign debits.type = "transfer".            
             leave.
          end.
       end.
    end.
    if debits.type = "" then do:
        assign debits.type = "unknown".
    end.

end.   /* for each aret */
/**********************************************************************/
/* MC's */
for each aret use-index k-invno where aret.cono = g-cono and
                                      aret.custno = customer and
                                      aret.statustype = yes and
                                      aret.transcd = 5 and
                                      aret.invdt >= b-invdate and
                                      aret.invdt <= e-invdate
                                      no-lock:
                                      
    if (b-customer = 233915 or b-customer = 233911 or b-customer = 233914 or
        b-customer = 1100025274) 
       and customer = 233912 then next.
    
    find first debits use-index k-debits where
                                         debits.customer = int(aret.custno) and
                                         debits.invoice  = aret.invno and
                                         debits.suffx    = aret.invsuf and
                                         debits.seq      = aret.seqno
                                         no-error.

    if avail debits then do: 
       if debits.type = "consig" or debits.type = "824" then next.
          assign debits.paid-amt = debits.paid-amt + aret.paymtamt
                 debits.due-amt  = debits.due-amt - 
                                  (aret.amount - aret.paymtamt). 
    end.
    else do:
        create debits.                        
        assign debits.customer   = aret.custno
               debits.invoice    = aret.invno 
               debits.suffx      = aret.invsuf
               debits.seq        = aret.seqno 
               debits.stage      = "MC"
               debits.reference  = aret.refer 
               debits.invdate    = aret.invdt 
               debits.journal    = aret.jrnlno
               debits.inv-status = substring(aret.user5,10,50)
               debits.orig-amt   = aret.amount
               debits.paid-amt   = (aret.paymtamt + aret.discamt - aret.pifamt)
               debits.due-amt    = aret.amount - (aret.paymtamt + aret.discamt
                                                                - aret.pifamt).

        do i = 1 to 19:     
           if substring(aret.refer,i,1)     = "b" and
              substring(aret.refer,i + 1,1) = "u" and
              substring(aret.refer,i + 2,1) = "y" and
              substring(aret.refer,i + 3,1) = "b" and 
              substring(aret.refer,i + 4,1) = "a" and 
              substring(aret.refer,i + 5,1) = "c" then do:
              assign debits.type = "MC buy".
              leave.
           end.
        end.

        if debits.type = "" then do:
           do i = 1 to 14:
              if substring(aret.refer,i,1)      = "c" and
                 substring(aret.refer,i + 1,1)  = "u" and
                 substring(aret.refer,i + 2,1)  = "s" and
                 substring(aret.refer,i + 3,1)  = "t" and
                 substring(aret.refer,i + 4,1)  = "o" and
                 substring(aret.refer,i + 5,1)  = "m" and
                 substring(aret.refer,i + 6,1)  = "e" and
                 substring(aret.refer,i + 7,1)  = "r" and
                 substring(aret.refer,i + 8,1)  = " " and
                 substring(aret.refer,i + 9,1)  = "e" and
                 substring(aret.refer,i + 10,1) = "r" then do:
                 assign debits.type = "MC cst err".
                 leave.
              end.
           end.
        end.

        if debits.type = "" then do:
           do i = 1 to 18:
              if substring(aret.refer,i,1)     = "m" and
                 substring(aret.refer,i + 1,1) = "a" and
                 substring(aret.refer,i + 2,1) = "s" and
                 substring(aret.refer,i + 3,1) = "s" and
                 substring(aret.refer,i + 4,1) = " " and
                 substring(aret.refer,i + 5,1) = "r" and
                 substring(aret.refer,i + 6,1) = "m" then do:
                 assign debits.type = "MC mass rm".
                 leave.
              end.
           end. 
        end.

        if debits.type = "" then do:                         
           do i = 1 to 19:
              if substring(aret.refer,i,1)     = "o" and     
                 substring(aret.refer,i + 1,1) = "e" and     
                 substring(aret.refer,i + 2,1) = " " and     
                 substring(aret.refer,i + 3,1) = "e" and     
                 substring(aret.refer,i + 4,1) = "r" and     
                 substring(aret.refer,i + 5,1) = "r" then do:     
                 assign debits.type = "MC oe err".             
                 leave.                                      
              end.                                           
           end.                                              
        end.                                                 
                  
        if debits.type = "" then do:                         
           do i = 1 to 18:                                   
              if substring(aret.refer,i,1)     = "p" and     
                 substring(aret.refer,i + 1,1) = "r" and     
                 substring(aret.refer,i + 2,1) = "i" and     
                 substring(aret.refer,i + 3,1) = "c" and     
                 substring(aret.refer,i + 4,1) = "i" and     
                 substring(aret.refer,i + 5,1) = "n" and     
                 substring(aret.refer,i + 6,1) = "g" then do:
                 assign debits.type = "MC price".             
                 leave.                                      
              end.                                           
           end.                                              
        end.                                                 

        if debits.type = "" then do:                         
           do i = 1 to 18:                                   
              if substring(aret.refer,i,1)     = "s" and     
                 substring(aret.refer,i + 1,1) = "h" and     
                 substring(aret.refer,i + 2,1) = "i" and
                 substring(aret.refer,i + 3,1) = "p" and     
                 substring(aret.refer,i + 4,1) = "p" and     
                 substring(aret.refer,i + 5,1) = "i" and     
                 substring(aret.refer,i + 6,1) = "n" then do:
                 assign debits.type = "MC ship".             
                 leave.
              end.                                           
           end.                                              
        end. 

        if debits.type = "" then do:                         
           do i = 1 to 16:
              if substring(aret.refer,i,1)     = "v" and     
                 substring(aret.refer,i + 1,1) = "e" and     
                 substring(aret.refer,i + 2,1) = "n" and     
                 substring(aret.refer,i + 3,1) = "d" and     
                 substring(aret.refer,i + 4,1) = "o" and     
                 substring(aret.refer,i + 5,1) = "r" and     
                 substring(aret.refer,i + 6,1) = " " and
                 substring(aret.refer,i + 7,1) = "e" and
                 substring(aret.refer,i + 8,1) = "r" then do:
                 assign debits.type = "MC vnd err".            
                 leave.
              end.                                           
           end.                                              
        end.
       
        if debits.type = "" then do:                         
           assign debits.type = "MC other".
        end.
    end. /* else */
end. /* for each aret */
         
/****************************************************************************/
/* UC's */

for each aret use-index k-invno where aret.cono = g-cono and
                                      aret.custno = customer and
                                      aret.statustype = yes and
                                      aret.transcd = 3 and  /* UC */
                                      aret.invdt >= b-invdate and
                                      aret.invdt <= e-invdate
                                      no-lock:
                                      
    if (b-customer = 233915 or b-customer = 233911 or b-customer = 233914 or
        b-customer = 1100025274) and 
       customer = 233912 then next.
    
    find first debits use-index k-debits where
                                         debits.customer = int(aret.custno) and
                                         debits.invoice  = aret.invno and
                                         debits.suffx    = aret.invsuf and
                                         debits.seq      = aret.seqno
                                         no-error.

    if avail debits then do:
       if debits.type = "consig" or debits.type = "824" then next.
          assign debits.paid-amt = debits.paid-amt + aret.paymtamt
                 debits.due-amt  = debits.due-amt -
                                  (aret.amount - aret.paymtamt).
    end.
    else do:
        create debits.
        assign debits.customer   = aret.custno
               debits.invoice    = aret.invno
               debits.suffx      = aret.invsuf
               debits.seq        = aret.seqno
               debits.stage      = "UC"
               debits.reference  = aret.refer
               debits.invdate    = aret.invdt
               debits.journal    = aret.jrnlno
               debits.inv-status = substring(aret.user5,10,50)
               debits.orig-amt   = aret.amount
               debits.paid-amt   = (aret.paymtamt + aret.discamt - aret.pifamt)
               debits.due-amt    = aret.amount - (aret.paymtamt + aret.discamt
                                                                - aret.pifamt).

        do i = 1 to 17:
           if substring(aret.refer,i,1)     = "d" and     
              substring(aret.refer,i + 1,1) = "u" and     
              substring(aret.refer,i + 2,1) = "p" and     
              substring(aret.refer,i + 3,1) = "l" and     
              substring(aret.refer,i + 4,1) = "i" and     
              substring(aret.refer,i + 5,1) = "c" and     
              substring(aret.refer,i + 6,1) = "a" and
              substring(aret.refer,i + 7,1) = "t" then do:
              assign debits.type = "UC dup".
              leave.                                      
           end.                                           
        end.                                              
        
        if debits.type = "" then do:                         
           do i = 1 to 17:
              if substring(aret.refer,i,1)     = "o" and
                 substring(aret.refer,i + 1,1) = "v" and     
                 substring(aret.refer,i + 2,1) = "e" and     
                 substring(aret.refer,i + 3,1) = "r" and     
                 substring(aret.refer,i + 4,1) = " " and     
                 substring(aret.refer,i + 5,1) = "p" and     
                 substring(aret.refer,i + 6,1) = "a" and
                 substring(aret.refer,i + 7,1) = "y" then do:
                 assign debits.type = "UC ovrpay".             
                 leave.                                      
              end.                                           
           end.                                              
        end.

        if debits.type = "" then do:                         
           do i = 1 to 21:
              if substring(aret.refer,i,1)     = "p" and     
                 substring(aret.refer,i + 1,1) = "a" and     
                 substring(aret.refer,i + 2,1) = "i" and     
                 substring(aret.refer,i + 3,1) = "d" then do:    
                 assign debits.type = "UC pd itm".
                 leave.                                      
              end.                                           
           end.                                              
        end.

        if debits.type = "" then do:                         
           do i = 1 to 18:                                   
              if substring(aret.refer,i,1)     = "u" and     
                 substring(aret.refer,i + 1,1) = "n" and     
                 substring(aret.refer,i + 2,1) = "i" and     
                 substring(aret.refer,i + 3,1) = "d" and     
                 substring(aret.refer,i + 4,1) = "e" and     
                 substring(aret.refer,i + 5,1) = "n" and     
                 substring(aret.refer,i + 6,1) = "t" then do:
                 assign debits.type = "UC unident".             
                 leave.                                      
              end.                                           
           end.                                              
        end.
        
        if debits.type = "" then do:                         
           do i = 1 to 18:                                   
              if substring(aret.refer,i,1)     = "n" and     
                 substring(aret.refer,i + 1,1) = "o" and     
                 substring(aret.refer,i + 2,1) = "t" and     
                 substring(aret.refer,i + 3,1) = " " and     
                 substring(aret.refer,i + 4,1) = "i" and     
                 substring(aret.refer,i + 5,1) = "n" and     
                 substring(aret.refer,i + 6,1) = "v" then do:
                 assign debits.type = "UC not inv".          
                 leave.                                      
              end.                                           
           end.                                              
        end.                                                 
                  
        if debits.type = "" then do:       
            assign debits.type = "UC other".
        end.                               
    end. /* else */   
end. /* for each aret */
end. /* procedure process-aret */

put control "~033~046~1541~117".   /* landscape format */

if o-rpttype = "a" or o-rpttype = "d" then do:

/* BuyBacks */
   view frame f-buy-hdr.
   for each debits use-index k-amount where debits.type = "buyback"
                                            no-lock:
       run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-buy-hdr.
   page.


/* Double Paid Invoices */
   view frame f-dou-hdr.
   for each debits use-index k-amount where debits.type = "double"
                                            no-lock:
       run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-dou-hdr.
   page.


/* Material Returned/rejected */
   view frame f-ret-hdr.
   for each debits use-index k-amount where debits.type = "return"
                                            no-lock:
       run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-ret-hdr.
   page.


/* Proof of Delivery */
   view frame f-pod-hdr.
   for each debits use-index k-amount where debits.type = "pod"
                                            no-lock:
       run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-pod-hdr.
   page.


/* Pricing Issues */                          
   view frame f-pri-hdr.
   for each debits use-index k-amount where debits.type = "pricing"
                                            no-lock:           
       run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-pri-hdr.
   page.


/* Miscellaneous */                          
   view frame f-mis-hdr.
   for each debits use-index k-amount where debits.type = "misc"
                                            no-lock:           
       run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-mis-hdr.
   page.


/* Freight */
   view frame f-fre-hdr.
   for each debits use-index k-amount where debits.type = "freight"
                                            no-lock:
       run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-fre-hdr.
   page.


/* Rebates */                                          
   view frame f-reb-hdr.
   for each debits use-index k-amount where debits.type = "rebate"
                                            no-lock:            
       run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-reb-hdr.
   page.


/* Transfer Vouchers */                                        
   view frame f-tra-hdr.
                        
   for each debits use-index k-amount where debits.type = "transfer"
                                            no-lock:              
       run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-tra-hdr.
   page.


/* Other Debits */        
   view frame f-unk-hdr.
   for each debits use-index k-amount where debits.type = "unknown"
                                            no-lock:
       run "print-detail".
   end.
   run "print-subtotals".
   run "print-grandtots".   /* Debits */
   hide frame f-unk-hdr.
   page.
end. /* if o-rpttype = "a" or "d" */

/**************************************************************************/

if o-rpttype = "a" or o-rpttype = "u" then do:
/* report on MC's */
/* MC Other */
   view frame f-mc-oth-hdr.
   for each debits use-index k-credit where debits.type = "MC other"
                                            no-lock:
      run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-mc-oth-hdr.
   page.


/* MC Pricing */
   view frame f-mc-pri-hdr.
   for each debits use-index k-credit where debits.type = "MC price"
                                            no-lock:
      run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-mc-pri-hdr.
   page.


/* MC OE Error */
   view frame f-mc-oe-hdr.
   for each debits use-index k-credit where debits.type = "MC oe err"
                                            no-lock:
      run "print-detail". 
   end.
   run "print-subtotals".
   hide frame f-mc-oe-hdr.
   page.

/* MC Vendor Error */
   view frame f-mc-ven-hdr.
   for each debits use-index k-credit where debits.type = "MC vnd err"
                                            no-lock:
      run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-mc-ven-hdr.
   page.

/* MC Buybacks */
   view frame f-mc-buy-hdr.
   for each debits use-index k-credit where debits.type = "MC buy"
                                            no-lock:
      run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-mc-buy-hdr.
   page.

/* MC Customer Error */
   view frame f-mc-cst-hdr.
   for each debits use-index k-credit where debits.type = "MC cst err"
                                            no-lock:
      run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-mc-cst-hdr.
   page.

/* MC Mass RM Project */
   view frame f-mc-rm-hdr.
   for each debits use-index k-credit where debits.type = "MC mass rm"
                                            no-lock:
      run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-mc-rm-hdr.
   page.

/* MC Shipping */
   view frame f-mc-shi-hdr.
   for each debits use-index k-credit where debits.type = "MC ship"
                                            no-lock:
      run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-mc-shi-hdr.
   run "print-grandtots".
   page.


/****************************************************************************/

/* UC Unidentified */
   view frame f-uc-und-hdr.
   for each debits use-index k-credit where debits.type = "UC unident"
                                            no-lock:
      run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-uc-und-hdr.
   page.

/* UC Paid Item */
   view frame f-uc-pd-hdr.
   for each debits use-index k-credit where debits.type = "UC pd itm"
                                            no-lock:
      run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-uc-pd-hdr.
   page.

/* UC Not Invoiced */
   view frame f-uc-inv-hdr.
   for each debits use-index k-credit where debits.type = "UC not inv"
                                            no-lock:
      run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-uc-inv-hdr.
   page.

/* UC Duplicate Payments */
   view frame f-uc-dup-hdr.
   for each debits use-index k-credit where debits.type = "UC dup"
                                            no-lock:
      run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-uc-dup-hdr.
   page.


/* UC Over Pay */
   view frame f-uc-ovr-hdr.
   for each debits use-index k-credit where debits.type = "UC ovrpay"
                                            no-lock:
      run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-uc-ovr-hdr.
   page.


/* UC Other */                                    
   view frame f-uc-oth-hdr.                                       
   for each debits use-index k-credit where debits.type = "UC other"
                                            no-lock:
      run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-uc-oth-hdr.
   run "print-grandtots".
   page.
   
end. /* if o-rpttype = "a" or "u" */
/****************************************************************************/

/* Rejects (824) */
if o-rpttype = "a" or o-rpttype = "r" then do:
   view frame f-rej-header.
   for each debits use-index k-error where debits.type = "824"
                                            no-lock:
      
      if o-disprej = "NO" and 
               debits.inv-status begins "pay"
                
                then do:
                  assign count = count + 1.
                  assign tot-amt = tot-amt + debits.orig-amt.
                  assign disp-amt = disp-amt + debits.orig-amt.
                  assign disp-count = disp-count + 1.
                  next.
      end.            
     
     else
       do: 
      assign w-refer = substr(debits.reference,1,7).
      display                      
        debits.invoice             
        debits.suffx               
        debits.transtype           
        /*
        debits.stage               
        */
        w-refer
        debits.po
        debits.invdate             
        debits.orig-amt            
        debits.doctype             
        debits.error1              
        debits.inv-status          
        debits.takenby
        with frame f-rej-detail.     
        down with frame f-rej-detail.
        
      assign count   = count + 1.
      assign tot-amt = tot-amt + debits.orig-amt.
     end. /* Else */
   end.   /* For Each */
   
   if o-disprej = "NO"
      then do: 
        display count tot-amt disp-count disp-amt        
        with frame f-rej-totals_2.
        down with frame f-rej-totals_2.
   end.
        
   else do:   
     display count tot-amt
     with frame f-rej-totals.
     down with frame f-rej-totals_2.
   end.
      
   assign accum-totals = accum-totals + tot-amt.
   hide frame f-rej-header.
   page.
end.
                        
/**************************************************************************/

if o-rpttype = "a" or o-rpttype = "d" then do:
/* Other - Non POR */
   view frame f-oth-hdr.                                                        
   for each debits use-index k-amount where debits.type = "open"                
                                            no-lock:                            
       run "print-detail".                                                      
   end.                                                                         
   run "print-subtotals".                                                       
   hide frame f-oth-hdr.                                                        
   page.                                                                        

/* Consumption */
   view frame f-cns-hdr.
   for each debits use-index k-amount where debits.type = "consume"
                                            no-lock:
       run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-cns-hdr.
   page.
/* Other - POR */
   view frame f-oth-p-hdr.
   for each debits use-index k-amount where debits.type = "open POR"
                                            no-lock:
       run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-oth-p-hdr.
   page.

/* Partial Paid Invoices */
   view frame f-ppi-hdr.
   for each debits use-index k-amount where debits.type = "ppi"
                                            no-lock:
       run "print-detail".
   end.
   run "print-subtotals".
   hide frame f-ppi-hdr.
   page.
   
/* Partial Paid Invoices - Credits */
   view frame f-crppi-hdr.
   for each debits use-index k-amount where debits.type = "crppi"
                                             no-lock:
      run "print-detail".
   end.
   run "print-subtotals".
   run "print-grandtots".
   hide frame f-crppi-hdr.
   page.
        
end. /* o-rpttype = "a" or "d" */

/***************************************************************************/

/* Consignment Orders to be cancelled */
if o-rpttype = "a" or o-rpttype = "c" then do:
   run arxicon.p.
end.
page.

/***************************************************************************/


assign net-orig-amt = net-orig-amt + accum-totals
       net-due-amt  = net-due-amt  + accum-totals.

display net-orig-amt         
        net-paid-amt         
        net-due-amt          
   with frame f-net.     
   down with frame f-net.
   hide frame f-net.





procedure print-detail:

    display debits.invoice      
            debits.suffx
            debits.seq
            debits.invdate      
            debits.reference    
            debits.stage        
            debits.journal      
            debits.orig-amt     
            debits.paid-amt     
            debits.due-amt      
            debits.inv-status   
            with frame f-detail.     
    down with frame f-detail.
    
    run "tally-subtotals".
           
end. /* print-detail */

procedure tally-subtotals:

    assign st-count    = st-count + 1                 
           st-orig-amt = st-orig-amt + debits.orig-amt
           st-paid-amt = st-paid-amt + debits.paid-amt
           st-due-amt  = st-due-amt  + debits.due-amt.

end.  /* procedure tally-subtotals */


procedure print-subtotals:

    display st-count                
            st-orig-amt             
            st-paid-amt             
            st-due-amt              
       with frame f-subtotal.     
       down with frame f-subtotal.
       hide frame f-subtotal.     
       hide frame f-detail.       

    assign g-count    = g-count + st-count      
           g-orig-amt = g-orig-amt + st-orig-amt
           g-paid-amt = g-paid-amt + st-paid-amt
           g-due-amt  = g-due-amt  + st-due-amt.
                                     
    assign  st-count    = 0         
            st-orig-amt = 0         
            st-paid-amt = 0         
            st-due-amt  = 0.        

end. /* procedure print-subtotals */


procedure print-grandtots:

    display g-count                                 
            g-orig-amt                              
            g-paid-amt                              
            g-due-amt                               
       with frame f-grand.                          
       down with frame f-grand.
       hide frame f-grand.
       
    assign  net-orig-amt = net-orig-amt + g-orig-amt
            net-paid-amt = net-paid-amt + g-paid-amt
            net-due-amt  = net-due-amt  + g-due-amt.
     
    assign  g-count    = 0
            g-orig-amt = 0                          
            g-paid-amt = 0                          
            g-due-amt  = 0.                         

end. /* procedure */
