/* zsdifloes.p ibrfloes.p 1.5 02/19/98 */
/*h*****************************************************************************
  PROCEDURE    : ibrfloes.p (Replaces icetloes.p for RF terminals; IB Module)
  DESCRIPTION  : Display, Assign, and Deassign Lot #'s, OE
  AUTHOR       : enp
  DATE WRITTEN :
  CHANGES MADE :
    08/23/91 mwb;           Credit return changes.
    08/29/91 mwb;           Split out the first display screen, select method.
    02/18/93 kmw; TB#  4776 Disallow F12 on Choose
    07/28/93 tdd; TB# 12319 Changes to s-lotno for user hook
    03/19/96 kr;  TB# 12406 Variables defined in form, moved to calling
        programs
    10/15/96 mtt; TB# 21254 (2F) Develop Value Add Module
    03/10/97 mms; TB#  7241 (5.1) Spec8.0 Special Price Costing; added
        speccost.gva as shared.
    02/09/98  aa; TB# 23693 copied from stnd 9.0, adatpted for RF terminals
                      (IB Project)
    02/19/98  aa; TB# 23693 Use ibrfloes.lpr instead of ibrfloes.ldi (not
                            needed any more)
    05/13/99 jbs; TB# 23693 Added g-screenpos for variable positioning.
    12/21/99 bp ; TB# 26804 Added SPC conversion in display and define of the
        b-icss buffer.
******************************************************************************/

define input parameter inIcselRecid as recid no-undo.
/* for icetl */
def var v-whse       like icsd.whse                      no-undo.
def var v-prod       like icsp.prod                      no-undo.
def var v-type       as c                                no-undo.
def var v-ordno      like oeeh.orderno                   no-undo.
def var v-ordsuf     like oeeh.ordersuf                  no-undo.
def var v-lineno     like oeel.lineno                    no-undo.
def var v-seqno      as i                                no-undo.
def var v-returnfl   as logical                          no-undo.
def var v-origqty    as dec format ">>>>>>>>9.99-"       no-undo.
def var v-proofqty   as dec format ">>>>>>>>9.99-"       no-undo.
def var v-ordqty     as dec format ">>>>>>>>9.99-"       no-undo.
def var v-cost       like icsel.prodcost                 no-undo.
def var v-ictype     like icet.transtype                 no-undo.
def var v-outqty     as dec format  ">>>>>>>>9.99-"      no-undo.
def var v-errfl      as c                                no-undo.
def var v-qtyunavail as dec format  ">>>>>>>>9.99-"      no-undo.

/* from g-icetl.i **/
{g-all.i}
{g-ic.i}
{g-oe.i}
{g-po.i}
{g-ar.i}
{g-ap.i}
{g-wt.i}


/*tb 26804 12/21/99 bp ; Add define of buffer.*/
def shared buffer b-icss for icss.

/*tb 23693 05/13/99 jbs; Added var */
def shared var g-screenpos as i no-undo.

{speccost.gva "shared"}
def {1} shared var v-seecostfl      as logical                          no-undo.
def {1} shared var v-icsnpofl       as logical                          no-undo.
def {1} shared var v-method         as c                                no-undo.
def {1} shared var s-title          as c format "x(76)"                 no-undo.
def {1} shared var v-iclotrcptty    as c                                no-undo.
def {1} shared var v-sfl            as logical                          no-undo.
def {1} shared var s-lotno          like icetl.lotno                    no-undo.
def            var s-selectfl       as logical                         no-undo.
def            var v-first          as recid                            no-undo.
def            var v-last           like aret.seqno                     no-undo.
def            var v-recid          as recid extent 13                  no-undo.
def            var v-recid2         as recid extent 13                  no-undo.
def            var s-quantity       as dec format "zzzzzzz9.99"         no-undo.
def     shared var s-qtyunavail     like icetl.quantity                 no-undo.
def     shared var s-reasunavty     like icetl.reasunavty               no-undo.
def            var v-frow           as i                                no-undo.

def            var v-cfl            as logical                          no-undo.
def            var v-firstfl        as logical                          no-undo.
def            var v-oldfl          as logical                          no-undo.
def            var v-quantity       as dec format "zzzzzzz9.99"         no-undo.

/* for returns */
def buffer b-icetl for icetl.
def     shared var s-retorderno     like oeel.retorderno                no-undo.
def     shared var s-retordersuf    like oeel.retordersuf               no-undo.
def     shared var s-retlineno      like oeel.retlineno                 no-undo.
def     shared var s-returnty       like oeel.returnty                  no-undo.
def     shared var v-maint-l        as c format "x"                     no-undo.
def     shared var s-retseqno       like oeelk.retseqno                 no-undo.
def            var o-ordno          like oeel.orderno                   no-undo.
def            var o-ordsuf         like oeel.ordersuf                  no-undo.
def            var o-lineno         like oeel.lineno                    no-undo.
def            var o-seqno          like oeelk.retseqno                 no-undo.
def            var v-count          as integer                          no-undo.
def            var v-created        as logical                          no-undo.

/*tb 12406 03/19/96 kr; Variables defined in form */
def            var s-rettext        as c format "x(7)"                  no-undo.
def            var s-seqno          as c format "x(4)"                  no-undo.
def            var s-kplabel        as c format "x(5)"                  no-undo.
def            var s-wono           as i format "zzzzzz9"               no-undo.
def            var s-kpdash         as c format "x"                     no-undo.
def            var s-wosuf          as i format "99"                    no-undo.
/*tb 21254 10/15/96 mtt; Develop Value Add Module */
def            var v-ordertype      like icets.ordertype                no-undo.


/* dkt */
def var maxRows as int initial 9 no-undo.

/*tb 23693 01/19/97 Repleaces f-icetl.i */
{ibrfetl.lfo} /*tb 23693 Repaced f-icetl.i...*/

assign
    o-ordno       = v-ordno
    o-ordsuf      = v-ordsuf
    o-lineno      = v-lineno
    o-seqno       = v-seqno
    v-ordertype   = if v-type = "oe" then "o" else "f".

/*** MAIN LOGIC ***/
main:
do while true with frame f-icetlout on endkey undo main, leave main:
  clear frame f-icetlout all no-pause.
  find icsel where recid(icsel) = inIcselRecid exclusive-lock no-wait no-error.
  if locked icsel then do:
    run ibrferr.p(4624).
    pause.
    next.
  end. /* end of if locked icsel */

  l-update:
  do on endkey undo, leave l-update:
    assign
      s-qtyunavail = if avail icetl and v-returnfl
                     then icetl.qtyunavail
                     else 0
      s-reasunavty = if avail icetl and v-returnfl
                     and icetl.orderno = o-ordno
                     then icetl.reasunavty
                     else if v-returnfl = yes
                          then s-reasunavty
                          else "".
    /* Update */
    find first b-icss no-lock no-error.
    message "ibrfloes.p .lpr which to find?". pause.
    {icss.gfi &buf = "b-"
              &prod = v-prod
              &lock = "no" }
                              
    {ibrfloes.lpr} /*tb 23693 Replaces u-lnoe.i  */
    if v-returnfl = yes and s-retorderno <> 0 then do:
      assign
        v-ordno  = o-ordno
        v-ordsuf = o-ordsuf
        v-lineno = o-lineno
        v-seqno  = o-seqno
        s-lotno  = icsel.lotno.
      find {p-icetl.i} exclusive-lock no-error.
    end. /* end of if v-returnfl = yes and .... */

    message "pre-assign".
    {p-lnoea.i &oeordertype = "v-ordertype"}
    message "post assign".

    /*tb 23693 Changed frame for RF */
    display
      s-selectfl
    with frame f-icetlout.
    if frame-line <> maxRows and not {k-cancel.i}
    then down with frame f-icetlout.
  end. /* end of l-update */

  hide frame f-rf-icetlout.

  if {k-cancel.i} then do:

    hide message no-pause.
    hide frame f-return no-pause.
    hide frame f-return2 no-pause.

    if avail icetl then
      s-quantity = if icetl.orderno = s-retorderno
                   then 0
                   else icetl.quantity.
    else s-quantity = 0.
  end. /* end of if k-cancel.i */
end. /* end of else if k-return.i */

{p-icetlr.i &oeordertype = "v-ordertype"}

message "end of main zsdifloes.p". pause.

assign
    v-ordno  = o-ordno
    v-ordsuf = o-ordsuf
    v-lineno = o-lineno
    v-seqno  = o-seqno.

