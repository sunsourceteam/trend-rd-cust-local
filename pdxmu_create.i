/* pdxmu_create.i */
   
   find first pdsc where pdsc.cono = 1 and
                         pdsc.statustype = yes and
                         pdsc.levelcd    = 1 and 
                         pdsc.enddt     >= TODAY and
                         pdsc.custno     = dec(i-customer) and
                         pdsc.prod       = i-product
                         no-error.
   if not avail pdsc then do:
      if dec(i-newprice) <> 0 then do:
         if i-pdtype = "$" then
           assign x-dm$    = i-newprice
                  x-pdtype = "$".
         else
         if dec(i-newprice) > dec(i-a-list) then
            assign x-dm$ = 
                     string(ROUND(((dec(i-newprice) / dec(i-a-list)) * 100),5))
                   x-pdtype = "m".
         else
            /* convert to multiplier */
            /*
            assign x-dm$ = 
               string(ROUND(((1 - (dec(i-newprice) / dec(i-a-list))) * 100),2))
                   x-pdtype = "%".
            */
            assign x-dm$ =
            string
             (ROUND(((100 - (1 - (dec(i-newprice) / dec(i-a-list))) * 100)),5))
                   x-pdtype = "m".

      end.
      else do:
         do i = 1 to 10:
            if substring(i-i-dm$,i,1) = "$" or 
               substring(i-i-dm$,i,1) = "%" or
               substring(i-i-dm$,i,1) = "m" then do:
               assign x-pdtype = substring(i-i-dm$,i,1).
               leave.
            end.
            else
              assign substring(x-dm$,i,1) = substring(i-i-dm$,i,1).
         end.
      end.
      
      find last L-pdsc use-index k-pdrecno where L-pdsc.cono = g-cono
                                                 no-lock
                                                 no-error.
      if avail L-pdsc then do:
         assign recno = L-pdsc.pdrecno + 1.
         create pdsc.
         assign pdsc.cono       = g-cono
                pdsc.pdrecno    = recno
                pdsc.statustype = yes
                pdsc.levelcd    = 1
                pdsc.custno     = dec(i-customer)
                pdsc.prod       = i-product
                pdsc.operinit   = "PXMU"
                pdsc.transdt    = TODAY
                pdsc.user9      = TODAY /*Used to tag ChDate on OEIP*/
                pdsc.user8      = TODAY /*used for Last Supplier Incr on OEIP*/
                pdsc.startdt    = TODAY
                pdsc.enddt      = TODAY + 365
                pdsc.prctype    = if x-pdtype = "$" then yes
                                  else no
                pdsc.priceonty  = if i-pdbasedon = "List" then "L"
                                  else
                                     if i-pdbasedon = "Cost" then "C"
                                     else
                                        if i-pdbasedon = "Base" then "B"
                                        else
                                           "L".

         
         if x-pdtype = "$" then
            assign pdsc.prcmult[1] = dec(x-dm$)
                   pdsc.prctype    = yes.
         if x-pdtype = "m" then 
            assign pdsc.prcmult[1] = dec(x-dm$)
                   pdsc.prctype    = no.
         if x-pdtype = "%" then
            assign pdsc.prcdisc[1] = dec(x-dm$)
                   pdsc.prctype    = no.
            
         assign add-count = add-count + 1.
            
         if avail icsw then
           do:
           find first pdsr where pdsr.cono = g-cono + 500 and
                                 pdsr.levelcd = 1 and
                                 pdsr.levelkey = icsw.prod and
                                 pdsr.vendno = icsw.arpvendno and
                                 pdsr.custno = dec(i-customer) and
                                 pdsr.rebatecostty  = "s" and
                                 pdsr.rebcalcty = "n" and
                                 pdsr.startdt <= TODAY and
                                (pdsr.enddt >= TODAY or
                                 pdsr.enddt = ?)
                                 no-lock no-error.
           if avail pdsr then
             assign pdsc.enddt = pdsr.enddt.
         end. /* if avail icsw */
          
            /* create audit report */
         export delimiter ","   
            pdsc.pdrecno
            pdsc.levelcd
            pdsc.statustype
            pdsc.enddt
            pdsc.prctype
            pdsc.priceonty
            pdsc.qtybreakty
            pdsc.prcmult[1]
            pdsc.prcmult[2]
            pdsc.prcmult[3]
            pdsc.prcmult[4]
            pdsc.prcmult[5]
            pdsc.prcmult[6]
            pdsc.prcmult[7]
            pdsc.prcmult[8]
            pdsc.prcmult[9]
            pdsc.prcdisc[1]
            pdsc.prcdisc[2]
            pdsc.prcdisc[3]
            pdsc.prcdisc[4]
            pdsc.prcdisc[5]
            pdsc.prcdisc[6]
            pdsc.prcdisc[7]
            pdsc.prcdisc[8]
            pdsc.prcdisc[9]
            x-dm$
            x-pdtype
            "ADD".
      end. /* last pdsc found */
   end. /* pdsc not avail */    
   
   for each pdsc where pdsc.cono = 1 and
                       pdsc.statustype = yes and
                       pdsc.levelcd    = 1 and 
                       pdsc.custno     = dec(i-customer) and
                       pdsc.prod       = i-product and
                       pdsc.enddt      < TODAY:
      assign pdsc.statustype = no.
   end.

