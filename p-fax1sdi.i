/*h*****************************************************************************
  INCLUDE      : p-fax1.i
  DESCRIPTION  : Set up for Fax Output
  USED ONCE?   : no (ares, oeepi, oeepa)
  AUTHOR       : enp
  DATE WRITTEN : 02/01/92
  CHANGES MADE :
    11/27/92 enp; TB# 8889 Add VisiFax Capability
    07/19/93 pjt; TB# 9599 Getting "sasp record not available" message
    01/19/94 rs; TB# 14024 Frame Error when using Fax
    01/29/94 rs; TB# 14552 From line is hard coded
    09/12/95 dww; TB# 19012 Add g-operinits to the RPTLOG
    07/17/98 jl;  TB# 25144 Add the interface to Vsifax Gold
    05/15/99 jl;  TB# 26204 Added nxtfax script logic. Added v-pcommand
        v-faxto1 and v-faxto2
    05/20/99 jl;  TB# 26190 Rename product to SX.e
    01/06/00 rgm; TB# 27121 Define variables for ODS printer type
    01/13/00 sbr; TB# 26773 Fax invoice printer override is ignored
    01/13/00 sbr; TB# 26774 Ack printer override is ignored
    01/13/00 sbr; TB# 26775 PO fax printer is ignored
    01/27/00 mjg; TB#e2785 Demand printing not working in GUI.
    03/29/00 rgm; TB# 3-2  Add RxServer Interface to Standard
    04/28/00 rgm; TB#e4656 Fix OERD fax when appropriate
    04/28/00 rgm; TB# A9   Fix for RxServer Faxing issue
    09/11/00 sbr; TB#e6416 File print change fax number
    09/27/00 bpa; TB# A50  Needed to add procedure to write comments out to                            file.
    11/05/00 lbr; TB# e7016 Fax where approp. vs entered printer confusion
    06/20/01 lbr; TB# e9095 Added PC User Name and PC password for
        conformation of rxlaser faxing
    12/07/01 lbr; TB# e11405 Moved user name and password to the end and
        added email address
    05/22/02 rcl; TB# e13657 VSIfax extra blank page.
    06/26/02 sbr; TB# t18698 FAX: FROM defaults Purchasing/Accounting
    06/27/02 rgm; TB# e11405 Restore changes lost from prior version
    08/14/02 lcb; TB# e12313 Report Manager overwriting output files
*******************************************************************************/

/*tb 9599 07/19/93 pjt; Getting "sasp record not available" message */
def var v-faxto         as c format "x(30)"     no-undo extent 2.
def var v-faxstring     as c format "x(50)"     no-undo.
def var v-wide          as c format "x"         no-undo.

{&com}
def var v-title         as c format "x(30)"     no-undo. 
def var v-from          as c format "x(40)"     no-undo.
def var z-memofilenm    as char                 no-undo.
def var z-printdir      like sasc.printdir      no-undo.
def var z-sapbrecid     as char                 no-undo.
def var z-pcommand      like sasp.pcommand      no-undo.
def var z-filenm        as char format "x(50)"  no-undo.
def var z-name          like sasc.name          no-undo.

/{&com}* */

def var v-rpttitle      like sapb.rpttitle      no-undo.
def var v-heading       as logical              no-undo.
def var v-faxcmd        as c format "x(99)"     no-undo.
def var v-fax132        as c format "x(99)"     no-undo.
def var v-pcommand      like sasp.pcommand      no-undo.
def var v-faxto1        like sapb.faxto1        no-undo.
def var v-faxto2        like sapb.faxto2        no-undo. 
def var v-faxfrom       like sapb.faxfrom       no-undo.
def var v-faxtag1       like sapb.faxtag1       no-undo.
def var v-faxtag2       like sapb.faxtag2       no-undo.
def var v-faxcom        as char format "x(75)" extent 10  no-undo.
/* TB# 27121 ODS variables for printer language/type */
def var s-charpos       as int                  no-undo.
def var e-charpos       as int                  no-undo.
def var v-charctr       as int                  no-undo.
def var v-printtype     as char                 no-undo.
def var v-memofl        as log      init no     no-undo.

/*tb 3-2 03/29/00 rgm; Rxserver variables - interface for faxing */
def var z-memocnt       as int                 no-undo.
/* def var z-memofilenm    as char                no-undo. 
def var z-printdir      like sasc.printdir     no-undo.
def var z-sapbrecid     as char                no-undo.
def var z-pcommand      like sasp.pcommand     no-undo.
def var z-filenm        as char format "x(50)" no-undo.
def var z-name          like sasc.name         no-undo.
*/
def var z-compress      as char                no-undo.
def var z-addr          like sasc.addr         no-undo.
def var z-phoneno       like sasc.phoneno      no-undo.
def var z-csz           as char format "x(36)" no-undo.
/* tb #9095 */
def var z-pcusername    like sasoo.pcusername                 no-undo.
def var z-pcpassword    like sasoo.pcpassword                 no-undo.
def var z-pcemailaddr   like pv_user.email                    no-undo.

def var v-rptno         as int                 no-undo.

procedure get-printdir:
    def buffer sasc for sasc.

    {sasc.gfi &lock  = "no"}
    assign
        z-printdir = if avail sasc then sasc.printdir
                     else ""
        z-name     = if avail sasc then sasc.name else ""
        z-addr[1]  = if avail sasc then sasc.addr[1] else ""
        z-addr[2]  = if avail sasc then sasc.addr[2] else ""
        z-phoneno  = if avail sasc then sasc.phoneno else ""
        z-csz      = if avail sasc then
                       sasc.city  + ", " + sasc.state + " " + sasc.zipcd
                     else ""
        z-compress = "".
end. /*procedure get-printdir */

/*tb A49&A50 09/27/00 bpa; Needed to move the creation of
   memo file to this procedure; broke 64K segment */
procedure create-memofile:
    def input parameter l-memo-only as char no-undo.

     /*Modified assignment of filename to stop jobs from
       overwriting the output file. */

       v-rptno = 0.

    /* Add a pause of one second to stop jobs from
       overwriting the output file. */

        pause(1) no-message.

        assign z-sapbrecid  = string(time,"999999999999999")
               z-memofilenm = z-printdir + g-operinit +
                              substring(z-sapbrecid,9,4) + "." +
                              substring(z-sapbrecid,13,3).

        /*Verify filename does not already exist before
             sending output to it. */
        do while search(z-memofilenm + "." + string(v-rptno,"999")) ne ?:
            v-rptno = v-rptno + 1.
        end.  /* do while search */

       /*Modify assignment of file name */
        assign z-sapbrecid  = string(time,"999999999999999")
               z-memofilenm = z-printdir + g-operinit +
                               substring(z-sapbrecid,9,4) + "." +
                               substring(z-sapbrecid,13,3) + "." +
                               string(v-rptno,"999").

        /*d Create Coversheet memo comments file */
        output to value(z-memofilenm).
        /*TB# 11405 Moved pcusername and password to the end and added email*/
        if l-memo-only = "" then do:
            put v-faxstring  skip.
            put v-faxto[1]   skip.
            put v-faxto[2]   skip.
            /*tb A18 07/18/00 sbr; Use v-from variable instead of
                "Accounting 1" */
            put v-from       skip.
        end.

        do z-memocnt = 1 to 10:
            if v-faxcom[z-memocnt] <> "" then
                v-memofl = yes.
            put v-faxcom[z-memocnt] format "x(75)" skip.
        end.

/*
        put z-pcusername skip.
        put z-pcpassword skip.
        put z-pcemailaddr skip.
*/        
        put unformatted chr(12).

        output close.

end. /* procedure create memofile */

assign
    v-heading   = v-headingfl
    v-headingfl = true.

form
    skip (15)
    "==================================="
    sasc.name                          colon 10 label "From"
    v-from                                at 12 no-label
    skip (1)
    v-faxto[1]                         colon 10 label "To"
    v-faxto[2]                            at 12 no-label
    skip (1)
    "Date/Time: ^US"                       at 1
    s-faxphoneno                        colon 10 label "Phone #"
    skip (1)
    v-title                             colon 10 label "Document"
    "=================================="    at 1
    "         Transmitted by           "    at 1
    "   FAX BOX and NxTrend Software   "    at 1
    "=================================="    at 1
with frame f-fax centered side-labels no-box.

{w-sasc.i no-lock}

/*tb e6416 09/11/00 sbr; Removed "(g-ourproc = "oeet" or g-ourproc = "poet")
    from the if condition that uses the printer device from the sapb file. */
/*tb#e2785 01/27/00 mjg; Demand printing not working in GUI */

/*tb 26773,26774,26775 01/13/00 sbr; Added code to make use of the printer that
    was entered within oeet or poet instead of always using the OIFA printer */
/*tb e4656 04/28/00 rgm; Remove OERD from sasp lookup to fix the
                         fax when appropraite option */

/*d Try to use the one entered */
if  avail sapb and sapb.printernm <> "" then
    {w-sasp.i sapb.printernm no-lock}

/*d If it's not there or not a fax device or they didn't enter one,
    then get the default */
if not avail sasp or sasp.ptype <> "f" or sapb.printernm = "" then
    {w-sasp.i sasc.oifaxdev[{&extent}] no-lock}

/*tb 14024 01/19/94 rs; Frame Error when using Fax */
if not avail sasp then do:

    output stream log to value(v-log) append unbuffered.

    assign
        v-date   = today
        v-time   = string(time,"HH:MM:SS")
        v-logmsg = "Error: Fax Not Set Up in OIFA".
    {d-log.i}

    output stream log close.

    return.
end.

{w-sassr.i g-ourproc no-lock}

if v-from = "" then do:
    for first pv_user fields(_user-name) where
        pv_user.cono  = g-cono and
        pv_user.oper2 = g-operinit
    no-lock:

        v-from = if pv_user._user-name <> "" and
                    pv_user._user-name <> g-operinit
                 then
                     pv_user._user-name
                 else "".
    end.
end.

if v-from = "" then do:
    for first smsn fields(name) where
        smsn.cono   = g-cono and
        smsn.slsrep = g-operinit
    no-lock:
        v-from = smsn.name.
    end.
end.
if v-from = "" then
    /*tb 14552 01/29/94 rs; From line is hard coded */
    v-from = if sasc.name begins "R&D" then
                 if g-ourproc begins "po" then "Purchasing"
                 else "Accounting"
             else "".

/*tb 8889 enp 11/27/92 Add VisiFax Capability */
/*d Faxbox Only */
/*tb A9 04/28/00 rgm; FIx for Rxserver faxing, add rxserv.lcn */
if avail sasp and
    not ({nxtfax.lcn  &file = "sasp." } or
         {vsifax.lcn  &file = "sasp." } or
         {rxserv.lcn &file = "sasp." &rxstype = "FP"})
then do:

    if avail sassr and v-reportno = 0 then
        v-wide = if sassr.wide then "Z"
                 else "X".
    else if avail sassr then
        v-wide = if sassr.xwide[v-reportno] then "Z"
                 else "X".
    else v-wide = "X".
end.

v-rpttitle = sapb.rpttitle.


