/******************************************************************************
  INCLUDE       : p-oeexcproc.i
  DESCRIPTION   : Custom Quote Entry - Main Entry Include
  USED ONCE?    : yes (oeexc.p)
  AUTHOR        : SunSource
  DATE WRITTEN  : 04/01/15
  CHANGES MADE  : 
                  
******************************************************************************/


/* -------------------------------------------------------------------------  */
Procedure Edit_v-qty:
/* -------------------------------------------------------------------------  */
  
  assign v-prodcost = 0
         w-commission = 0.
  if can-do("501,504",string(lastkey)) then
    assign typeover = yes.
  else
    assign typeover = no.
  if v-lmode = "c" /*and not avail oeelb*/ then
    find oeelb where oeelb.cono = g-cono and
         oeelb.batchnm = string(w-batchnm,">>>>>>>9") and
         oeelb.seqno = 2 and
         oeelb.lineno = v-lineno
         no-lock no-error.
  if v-lmode = "a" and v-slsrepovr = no then
    do:
    if v-slsrepout = "" then
      do:
      assign v-slsrepout = if avail arss then arss.slsrepout
                           else if avail arsc then arsc.slsrepout
                                else v-slsrepout.
      if v-slsrepout = "" then
        assign v-slsrepout = o-slsrepout.
    end. /* v-slsrepout = "" */
    if v-slsrepin = "" then
      do:
      v-slsrepin  = if avail arss then arss.slsrepin
                    else if avail arsc then arsc.slsrepin
                         else v-slsrepin.
    end.
  end. /* v-lmode = "a" and v-slsrepovr = no */
  {w-icsp.i v-prod no-lock}.
  if ((v-lmode = "a" and avail icsp) or
     (v-hqty <> v-qty and avail icsp)) then 
    do:
    assign /*v-qty = input v-qty*/
           v-sellmultfl = if icsp.sellmult > 0 then true else false.
           v-qty = if icsp.sellmult > 0 then
                     icsp.sellmult * (truncate(v-qty / icsp.sellmult,0) +
                     if (v-qty / icsp.sellmult) -
                       truncate(v-qty / icsp.sellmult,0) > 0 then 1        
                     else 0)
                   else v-qty.
    assign v-qtyrem = if v-lmode = "a" then v-qty else v-qtyrem.
                 
    /*display v-qty with frame f-mid1.*/
    if lastkey = 13 or lastkey = 401 or lastkey = 9 then
      do:
      assign x-whse       = v-whse
             x-specnstype = v-specnstype.
      assign v-whse = x-whse
             v-specnstype = x-specnstype.
       
    end. /* if lastkey = 13 or lastkey = 401 or lastkey = 9 */
    display v-prod with frame f-mid1.
          
    run Get-Whse-Info (input v-prod,
                       input v-whse,
                       input v-qty,
                       input-output v-shipdt,
                       input-output v-specnstype,
                       input-output v-ptype,
                       input-output v-foundfl,
                       input-output v-statustype,
                       input-output v-prodline,
                       input-output v-rarrnotesfl).

    display v-specnstype v-shipdt with frame f-mid1.
    assign zi-displaylinefl = true
           h-sellprc = v-sellprc.
    if zi-displaylinefl then
      do:
      assign disp-sellprc = v-sellprc
             v-sellprc = round(v-sellprc,2).
      display   
        v-lineno     
        v-specnstype
        v-prod  
        v-qty     
        v-qtyrem
        v-sellprc    
        v-shipdt
        v-totnet
        v-lcomment
      with frame f-mid1.
      assign v-sellprc = disp-sellprc.
    end.
  end. /* if v-lmode = "a" and avail icsp */
  else
    do:
    if ((v-lmode = "a" and not avail icsp) or
       (v-hqty <> v-qty and not avail icsp)) then
      do:
      assign v-qty = input v-qty
             v-sellmultfl = v-sellmultfl.
      assign v-qtyrem = if v-lmode = "a" then v-qty else v-qtyrem.
                 
      /*display v-qty with frame f-mid1.*/
      if lastkey = 13 or lastkey = 401 or lastkey = 9 then
        do:
        assign x-whse       = v-whse
               x-specnstype = v-specnstype.
        assign v-whse = x-whse
               v-specnstype = x-specnstype.
       
      end. /* if lastkey = 13 or lastkey = 401 or lastkey = 9 */
      display v-prod with frame f-mid1.
      assign zi-displaylinefl = true
             h-sellprc = v-sellprc.
      if zi-displaylinefl then
        do:
        assign disp-sellprc = v-sellprc
               v-sellprc = round(v-sellprc,2).
        display   
          v-lineno     
          v-specnstype
          v-prod  
          v-qty     
          v-qtyrem
          v-sellprc    
          v-shipdt
          v-totnet
          v-lcomment
        with frame f-mid1.
        assign v-sellprc = disp-sellprc.
      end.
    end.
  end.  
    
  if v-lmode = "c" then 
    do:
    if h-sellprc <> v-sellprc and
       h-sellprc <> 0 and
       oeelb.priceoverfl = no then
      do:
      find oeelb where
           oeelb.cono    = g-cono and
           oeelb.batchnm = string(w-batchnm,">>>>>>>9") and
           oeelb.seqno = 2 and
           oeelb.lineno  = v-lineno
           no-error.
      if avail oeelb then
        do:
        run Update_OEELB.
      end.
    end. /* if h-sellprc <> v-sellprc */
    run Quote-Total.
  end. /* v-lmode = "c" */
  
  /**************/
  /*
  if g-operinit begins "das" then
  do:
  */
  for each bpeh where
           bpeh.cono  = g-cono and
           bpeh.bpid  = string(v-quoteno,">>>>>>>9") + "C" 
           no-lock:
    
    find bpel where bpel.cono   = g-cono and
                    bpel.bpid   = bpeh.bpid and
                    bpel.revno  = bpeh.revno and
                    bpel.lineno = v-lineno
                    no-error.
    if avail bpel then
      do:
      assign bpel.item     = if bpel.revno <= 3 then bpeh.slsrepout
                             else bpeh.awardnm.
      assign
           bpel.price      = if bpel.itemid <> " " then v-sellprc else 0
           bpel.extprice   = if bpel.itemid <> " " then (v-sellprc * v-qty)
                             else 0
           bpel.seqid      = bpeh.slsrepout
           bpel.commtype   = if bpel.itemid <> " " then bpeh.commtype
                             else "0.0"
           bpel.termspct   = if bpel.itemid <> " " then v-Lcommpct else 0
           bpel.qtyord     = if bpel.itemid <> " " then v-qty else 0.
      assign
           bpel.awardprice = if bpel.revno <= 3 then
                               ((bpel.extprice * (.01 * bpel.termspct)) *
                                               (.01 * int(bpel.commtype))) /* *
                                               if oeehb.xxde1 > 0 then 
                                                 (1 - (.01 * oeehb.xxde1))
                                               else
                                                 1                         */
                             else
                               (bpel.extprice * (.01 * bpel.termspct)) *
                                                 (.01 * int(bpel.commtype))
           bpel.cost       = if bpel.qtyord <> 0 then
                               (.01 * int(bpel.commtype)) *
                               ((bpel.extprice * (1 - (.01 * bpel.termspct)) /
                                                       bpel.qtyord))
                             else 0
           bpel.transdt    = TODAY
           bpel.transtm    = substr(string(TIME,"HH:MM"),1,2) +
                             substr(string(TIME,"HH:MM"),4,2)
           bpel.operinit   = g-operinit.
      /* determine the unit price */
      assign 
           bpel.listprice  = if bpel.qtyord <> 0 then
                               bpel.awardprice / bpel.qtyord
                             else 0.

    end.
  end.
  /*
  end.
  */
  /**************/
  
  
  for each bpel where bpel.cono   = g-cono and
                      bpel.bpid   = string(v-quoteno,">>>>>>>9") + "C" and
                      bpel.lineno = v-lineno and
                      bpel.itemid <> " "
                      no-lock:
    if bpel.revno <= 3 then
      do:
      if v-lmode = "c" then
        if bpel.qtyord > 0 then
          assign w-commission = w-commission + (bpel.awardprice / bpel.qtyord).
        else
          assign w-commission = w-commission + bpel.awardprice.
      else
        assign w-commission = w-commission + (bpel.awardprice / bpel.qtyord).
    end.
  end. /* for each bpel */
  assign v-prodcost = v-sellprc - w-commission.
  assign v-totnet  = v-sellprc  * input v-qty.
  assign v-totcost = v-prodcost * input v-qty.
  /*
  if g-operinit begins "das" then
    do:
    message "in Edit_v-qty"
            "v-prodcost:" v-prodcost
            "v-sellprc:" v-sellprc
            "input v-qty:" input v-qty
            "v-qty:" v-qty
            "w-commission:" w-commission. pause.
  end.
  */
  
  /* a refresh needs to be done to keep the changes in the browse */
  if v-conversion then
    do:
    b-lines:refresh() in frame f-lines. 
    display b-lines with frame f-lines.
  end.
  
  if lastkey = -1 then
    do:
    readkey pause 0.  /* reset lastkey */
    next-prompt v-sellprc with frame f-mid1.
    next.
  end.
end. /* procedure Edit_v-qty */

/* -------------------------------------------------------------------------  */
Procedure Edit_v-sellprc:
/* -------------------------------------------------------------------------  */
  assign v-prodcost = 0
         w-commission = 0.
  for each bpeh where
           bpeh.cono  = g-cono and
           bpeh.bpid  = string(v-quoteno,">>>>>>>9") + "C" 
           no-lock:
    
    find bpel where bpel.cono   = g-cono and
                    bpel.bpid   = bpeh.bpid and
                    bpel.revno  = bpeh.revno and
                    bpel.lineno = v-lineno
                    no-error.
    if avail bpel then
      do:
      assign bpel.item     = if bpel.revno <= 3 then bpeh.slsrepout
                             else bpeh.awardnm.
      assign
           bpel.price      = if bpel.itemid <> " " then v-sellprc else 0
           bpel.extprice   = if bpel.itemid <> " " then (v-sellprc * v-qty)
                             else 0
           bpel.seqid      = bpeh.slsrepout
           bpel.commtype   = if bpel.itemid <> " " then bpeh.commtype
                             else "0.0"
           bpel.termspct   = if bpel.itemid <> " " then v-Lcommpct else 0
           bpel.qtyord     = if bpel.itemid <> " " then v-qty else 0.
      /*
      if g-operinit begins "das" then
        do:
        message "bpel.price:" bpel.price
                "bpel.extprice:" bpel.extprice
                "bpel.commtype:" bpel.commtype
                "bpel.termspct:" bpel.termspct
                "bpel.qtyord:"   bpel.qtyord. pause.
      end.
      */
      assign
           bpel.awardprice = if bpel.revno <= 3 then
                               ((bpel.extprice * (.01 * bpel.termspct)) *
                                               (.01 * int(bpel.commtype))) /* *
                                               if oeehb.xxde1 > 0 then 
                                                 (1 - (.01 * oeehb.xxde1))
                                               else
                                                 1                         */
                             else
                               (bpel.extprice * (.01 * bpel.termspct)) *
                                                 (.01 * int(bpel.commtype))
           bpel.cost       = if bpel.qtyord <> 0 then
                               (.01 * int(bpel.commtype)) *
                               ((bpel.extprice * (1 - (.01 * bpel.termspct)) /
                                                       bpel.qtyord))
                             else 0
           bpel.transdt    = TODAY
           bpel.transtm    = substr(string(TIME,"HH:MM"),1,2) +
                             substr(string(TIME,"HH:MM"),4,2)
           bpel.operinit   = g-operinit.
      /* determine the unit price */
      assign 
           bpel.listprice  = if bpel.qtyord <> 0 then
                               bpel.awardprice / bpel.qtyord
                             else 0.
      assign w-commission = w-commission + if bpel.revno <= 3 then 
                                             if bpel.qtyord <> 0 then
                                               (bpel.awardprice / bpel.qtyord)
                                             else
                                               bpel.awardprice
                                           else
                                             0.
    end.
    else
      do:
      create bpel.
      assign bpel.cono       = bpeh.cono
             bpel.bpid       = bpeh.bpid
             bpel.revno      = bpeh.revno
             bpel.lineno     = v-lineno.
      assign bpel.itemid     = if bpel.revno <= 3 then bpeh.slsrepout
                               else bpeh.awardnm
             bpel.prod       = v-prod.
      assign bpel.qtyord     = if bpel.itemid <> " " then v-qty else 0
             bpel.unit       = if bpel.itemid <> " " then "EACH" else " "
             bpel.price      = if bpel.itemid <> " " then v-sellprc else 0
             bpel.extprice   = if bpel.itemid <> " " then v-sellprc * v-qty
                                 else 0
             bpel.seqid      = bpeh.slsrepout
             bpel.commtype   = if (bpeh.slsrepout <> " " or
                                   bpeh.awardnm <> " ") then bpeh.commtype
                               else "0.0"
             bpel.termspct   = if bpel.itemid <> " " then v-Lcommpct else 0.
      assign bpel.awardprice = if bpel.revno <= 3 then
                                ((bpel.extprice * (.01 * bpel.termspct)) *
                                               (.01 * int(bpel.commtype))) /* *
                                               if oeehb.xxde1 > 0 then 
                                                 (1 - (.01 * oeehb.xxde1))
                                               else
                                                 1                         */
                               else
                                 (bpel.extprice * (.01 * bpel.termspct)) *
                                                 (.01 * int(bpel.commtype))
             bpel.cost       = if bpel.qtyord <> 0 then
                                (.01 * int(bpel.commtype)) *
                                ((bpel.extprice * (1 - (.01 * bpel.termspct)) /
                                                         bpel.qtyord))
                               else 0
             bpel.transdt    = TODAY
             bpel.transtm    = substr(string(TIME,"HH:MM"),1,2) +
                               substr(string(TIME,"HH:MM"),4,2)
             bpel.operinit   = g-operinit.
        /* determine the unit price */
      assign bpel.listprice  = if bpel.qtyord <> 0 then
                                 bpel.awardprice / bpel.qtyord
                               else 0.
      assign w-commission = w-commission + if bpel.revno <= 3 then 
                                             if bpel.qtyord <> 0 then
                                               (bpel.awardprice / bpel.qtyord)
                                             else
                                               bpel.awardprice
                                           else
                                             0.
    end. /* create */
    
    /* only include slsrep split; not company split in the cost */
    /*
    assign v-prodcost = v-prodcost + if bpel.revno <= 3 then bpel.cost
                                     else 0.
    */
    assign bpel.extcost = if bpel.itemid <> " " then v-prodcost * v-qty
                          else 0.
    assign v-costoverfl = yes.
  end. /* each bpeh */
  /*
  if g-operinit begins "das" then
    do:
    message "v-sellprc:" v-sellprc
            "w-commission:" w-commission. pause.
  end.
  */
  assign v-prodcost = v-sellprc - w-commission.
  assign v-totcost = v-prodcost * v-qty.
  /*
  if g-operinit begins "das" then
    do:
    message "in Edit_v-sellprc"
            "v-prodcost:" v-prodcost
            "v-sellprc:" v-sellprc
            "v-qty:" v-qty
            "w-commission:" w-commission. pause.
  end.
  */
end. /* procedure Edit_v-sellprc */

/* -------------------------------------------------------------------------  */
Procedure Edit_v-Lcommpct:
/* -------------------------------------------------------------------------  */
  assign v-prodcost = 0
         w-commission = 0.
  for each bpeh where
           bpeh.cono  = g-cono and
           bpeh.bpid  = string(v-quoteno,">>>>>>>9") + "C" 
           no-lock:
    
    find bpel where bpel.cono   = g-cono and
                    bpel.bpid   = bpeh.bpid and
                    bpel.revno  = bpeh.revno and
                    bpel.lineno = v-lineno
                    no-error.
    if avail bpel then
      do:
      assign bpel.itemid     = if bpel.revno <= 3 then bpeh.slsrepout
                               else bpeh.awardnm.
      assign
           bpel.price      = if bpel.itemid <> " " then v-sellprc else 0
           bpel.extprice   = if bpel.itemid <> " " then v-sellprc * v-qty
                             else 0
           bpel.seqid      = bpeh.slsrepout
           bpel.commtype   = if bpel.itemid <> " " then bpeh.commtype
                             else "0.0"
           bpel.termspct   = if bpel.itemid <> " " then v-Lcommpct else 0
           bpel.qtyord     = if bpel.itemid <> " " then v-qty else 0.
      assign
           bpel.awardprice = if bpel.revno <= 3 then
                               ((bpel.extprice * (.01 * bpel.termspct)) *
                                               (.01 * int(bpel.commtype))) /* *
                                               if oeehb.xxde1 > 0 then 
                                                 (1 - (.01 * oeehb.xxde1))
                                               else
                                                 1                         */
                             else
                               (bpel.extprice * (.01 * bpel.termspct)) *
                                                 (.01 * int(bpel.commtype))
           bpel.cost       = if bpel.qtyord <> 0 then
                                (.01 * int(bpel.commtype)) *
                                ((bpel.extprice * (1 - (.01 * bpel.termspct)) /
                                                         bpel.qtyord))
                             else 0
           bpel.transdt    = TODAY
           bpel.transtm    = substr(string(TIME,"HH:MM"),1,2) +
                             substr(string(TIME,"HH:MM"),4,2)
           bpel.operinit   = g-operinit.
      /* determine the unit price */
      assign 
           bpel.listprice  = if bpel.qtyord <> 0 then
                               bpel.awardprice / bpel.qtyord
                             else 0.

    end.
    else
      do:
      create bpel.
      assign bpel.cono       = bpeh.cono
             bpel.bpid       = bpeh.bpid
             bpel.revno      = bpeh.revno
             bpel.lineno     = v-lineno.
      assign bpel.itemid     = if bpel.revno <= 3 then bpeh.slsrepout
                               else bpeh.awardnm.
      assign bpel.prod       = if bpel.itemid <> " " then v-prod else " "
             bpel.qtyord     = if bpel.itemid <> " " then v-qty else 0
             bpel.unit       = if bpel.itemid <> " " then "EACH" else " "
             bpel.price      = if bpel.itemid <> " " then v-sellprc else 0
             bpel.extprice   = if bpel.itemid <> " " then v-sellprc * v-qty
                               else 0
             bpel.seqid      = bpeh.slsrepout
             bpel.commtype   = if bpel.itemid <> " " then bpeh.commtype
                               else "0.0"
             bpel.termspct   = if bpel.itemid <> " " then v-Lcommpct else 0.
        assign
             bpel.awardprice = if bpel.revno <= 3 then
                                ((bpel.extprice * (.01 * bpel.termspct)) *
                                                (.01 * int(bpel.commtype)))/* *
                                                if oeehb.xxde1 > 0 then 
                                                  (1 - (.01 * oeehb.xxde1))
                                                else
                                                  1                        */
                               else
                                 (bpel.extprice * (.01 * bpel.termspct)) *
                                                   (.01 * int(bpel.commtype))
             bpel.cost       = if bpel.qtyord <> 0 then
                                (.01 * int(bpel.commtype)) *
                                ((bpel.extprice * (1 - (.01 * bpel.termspct)) /
                                                         bpel.qtyord))
                               else 0
             bpel.transdt    = TODAY
             bpel.transtm    = substr(string(TIME,"HH:MM"),1,2) +
                               substr(string(TIME,"HH:MM"),4,2)
             bpel.operinit   = g-operinit.
      /* determine the unit price */
      assign bpel.listprice  = if bpel.qtyord <> 0 then
                                 bpel.awardprice / bpel.qtyord
                               else 0.
    end. /* create */
    /* only include slsrep split in the cost, not company split */
    /*
    assign v-prodcost = v-prodcost + if bpel.revno <= 3 then bpel.cost
                                     else 0.
    */
    assign w-commission = w-commission + if bpel.revno <= 3 then 
                                           if bpel.qtyord <> 0 then
                                             (bpel.awardprice / bpel.qtyord)
                                           else
                                             bpel.awardprice
                                         else
                                           0.
    assign v-costoverfl = yes.
  end. /* each bpeh */
  assign v-prodcost = v-sellprc - w-commission.
  /* since cost is recalulated if v-Lcommpct is changed, we need to update
     the Extended Cost after all the updates are completed */
  for each bpel where bpel.cono = g-cono and
                      bpel.bpid = string(v-quoteno,">>>>>>>9") + "C" and
                      bpel.lineno = v-lineno:
    assign bpel.extcost = if bpel.itemid <> " " then v-prodcost  * v-qty 
                          else 0.
  end.
  assign v-totcost = v-prodcost * v-qty.
  /*
  if g-operinit begins "das" then
    do:
    message "in Edit_v-Lcommpct"
            "v-prodcost:" v-prodcost
            "v-sellprc:" v-sellprc
            "w-commission:" w-commission
            "v-qty:" v-qty. pause.
  end.
  */
end. /* procedure Edit_v-Lcommpct */



/* -------------------------------------------------------------------------  */
Procedure Continue_Middle1_Process:
/* -------------------------------------------------------------------------  */
  
  assign s-prod       = v-prod
         s-qtyord     = v-qty
         s-chrgqty    = v-qty
         s-price      = /*input*/ v-sellprc
         s-specnstype = v-specnstype
         s-discamt    = 0 /*v-disc*/
         s-disctype   = no
         s-prodcost   = v-prodcost 
         s-returnfl   = no
         s-pricetype  = v-ptype
         s-prodcat    = v-prodcat
         v-rebamt     = v-sparebamt.
  find first icss where icss.cono = g-cono and
                        icss.prod = v-prod and
                        icss.statusfl = yes
                        no-lock no-error.
  if avail icss then
    assign v-speccostty = icss.speccostty
           v-csunperstk = icss.csunperstk.
  else
    assign v-speccostty = " "
           v-csunperstk = 0.
   
  if zsdiconfirm = no or v-sellprc >= 0 then
    do:
    if auth-sellprc <> v-sellprc and v-disc > 0 and manual-change = no then
      assign s-price = if auth-sellprc <> 0 then auth-sellprc else s-price.
    assign s-price = v-sellprc.
  end.
end. /* procedure Continue_Middle1_Process */



/* -------------------------------------------------------------------------  */
Procedure Check_product_ID:
/* -------------------------------------------------------------------------  */
  define input        parameter ix-type  as character                  no-undo.
  define input-output parameter ix-prod  like icsp.prod                no-undo.
  define input-output parameter ix-hprod like icsp.prod                no-undo.
  /*
  v-xprod      = "cpsoiub". /*"cxbpmigt".*/ /*if v-custprodfl then  "cxbpmigt"
                                                 else "xbpmigt". */
  */ 
  on cursor-up cursor-up. /*tab.*/
  on cursor-down cursor-down. /*back-tab.*/

  assign ix-hprod = ix-prod.

  if v-whse = "" then
    do:
    run Find_Whse (input x-prodtype).
  end.

  /* catalog */
  if v-lmode = "a" then
    run crossref.p (v-prod,
                    v-whse,
                    ix-type,
                    v-custno,
                    6,
                    5,
                    output ix-prod,
                    output v-whse,
                    output v-type,
                    output v-cqty,
                    output v-cunit).
  
  if v-whse = "" and x-whse <> "" then
    assign v-whse = x-whse.
  
  if ix-prod = "" then
    ix-prod = ix-hprod.
     
  if ix-prod <> ix-hprod then
  /* das - 07/05/07 - If avail icsp and an xref exists, use the xref
    do:
    if avail icsp and icsp.prod = ix-hprod then
      assign newprod = no
             ix-prod = ix-hprod.
    else
      assign newprod = yes.
  end.
  */
    assign newprod = yes.
    
  /* Catalog */           
  assign g-whse = x-whse
         catalogfl = false
         v-crprod  = ix-prod
         v-crwhse  = v-whse
         v-crtype  = v-type
         v-crqty   = v-cqty
         v-crunit  = v-cunit.
    
  if ix-prod = "" then 
     assign ix-prod   = v-prod.
            /*v-prctype = v-type.*/
  else
  if ix-prod <> "" and v-type = "G" then
    assign  v-prod = ix-prod
            catalogfl = if v-type = "g" then true else false.
  if catalogfl then 
    do:
    assign /*sh-prod = s-prod*/
           s-prod  = v-prod.
    if v-lmode = "a" then
      do:
      run vaexqlx.p ("G",no).  /* catalog cross reference options and access */
      assign v-prod = s-prod.
             /*s-prod = sh-prod.*/
      
      find u-icsp where u-icsp.cono = g-cono and
                        u-icsp.prod = v-prod
                        no-lock no-error.
      run zsdiCatalogTrace.p
         (input "Audit",  /* Audit, Inquire, Delete */
          input g-cono,
          input v-prod,
          input v-whse,
          input "OEEXC",
          input (if avail u-icsp then "s" else "n"),
          input v-quoteno,
          input 0,
          input v-lineno,
          input v-seqno,
          input g-operinits,
          output v-zsdireturncd).
      assign cataddfl = if avail u-icsp then "gs" else "gn".
      /*run Catalog_Audit.*/
    end.       
  end.
/* Catalog */

  on cursor-up cursor-up.
  on cursor-down cursor-down.


end. /* Check_product_ID */
  

/* -------------------------------------------------------------------------  */
Procedure FindNextFrame:
/* -------------------------------------------------------------------------  */
  def input        parameter ix-lastkey          as integer         no-undo.
  def input-output parameter ix-currentposition  as integer         no-undo.
  def input-output parameter ix-newposition      as char            no-undo.
  
  if {k-cancel.i "ix-"} and not v-ManualFrameJump then
    do:
    if avail OptionMoves and v-quoteno > 0 then
      find Opts where Opts.OptionIndex = OptionMoves.OptionIndex + 2
                      no-lock no-error.
    if not avail Opts then
      do:
      assign ix-currentposition = (if ix-currentposition - 2 < 0 then
                                     -1
                                   else
                                     ix-currentposition - 2).
      find first oeelb where oeelb.cono = g-cono and
                             oeelb.batchnm = string(v-quoteno,">>>>>>>9") and
                             oeelb.seqno   = 2
                             no-lock no-error.
      if not avail oeelb then 
        do:
        if ix-currentposition < 0 then
           assign ix-newposition      = "xx"
                  ix-currentposition  = 0.
        if v-quoteno > 0 then 
          do:
          find first oeelb where oeelb.cono = g-cono and
                           oeelb.batchnm = string(v-quoteno,">>>>>>>9") and
                           oeelb.seqno = 2
                           no-lock no-error.
          if not avail oeelb then 
            do:
            find b-oeehb where 
                 b-oeehb.cono = g-cono and  
                 b-oeehb.batchnm = string(v-quoteno,">>>>>>>") and
                 b-oeehb.seqno   = 2
                 exclusive-lock no-error.          
            if avail b-oeehb then 
              do:
              assign v-delfl = yes.
              delmode:
              do on endkey undo delmode, leave delmode:
                update v-delfl label
   "The Quote Just Entered Does Not Have Line Items, Do You Want to Delete It"
                with frame f-del side-labels row 5 centered overlay.
                if v-delfl = yes then 
                  do:
                  for each bpeh where bpeh.cono = g-cono and
                                      bpeh.bpid = b-oeehb.batchnm + "C":
                    for each bpel where bpel.cono = g-cono and
                                        bpel.bpid = bpeh.bpid:
                      delete bpel.
                    end. /* each bpel */
                    delete bpeh.
                  end. /* each bpeh */
                  for each zidc where zidc.cono      = g-cono and
                           zidc.docstatus = 00     and
                           zidc.docttype  = b-oeehb.batchnm + "C" and
                           zidc.doctype   = "ComQu":
                    delete zidc.
                  end.
                  delete b-oeehb.
                  run Clear-Frames.  
                  run Initialize-All.
                  leave.
                end.
              end.
            end. /* avail b-oeehb */
          end.  /* not avail oeehb */
          run Clear-Frames.  
          run Initialize-All.
          leave.
        end. /* v-quoteno > 0 */
        return.                          
      end. /* not avail oeehb */
    end. /* not avail Opts */
    else do:
      if Opts.OptionIndex < 5 then
        readkey pause 0.
    end.
  end. /* cancel and not manual jump */
  if v-ManualFrameJump  then 
    do: /*
          If they jumped frames bring them back into the place they
          jumped from within the operator settings.
        */   

    assign ix-currentposition = (if ix-currentposition - 1 < 0 then
                                  0
                                else
                                  ix-currentposition - 1)
           v-ManualFrameJump = false. 
    if v-framestate = "B2" then
      readkey pause 0. /* due to leaving quote when F4 was pressed on notes */
  end.

  if ix-currentposition = -1 then
    do:
    
    assign ix-newposition      = "xx"
           ix-currentposition  = 0. 
    return.
    end.  


  do v-inx = (ix-currentposition + 1) to 5 by 1:
    find OptionMoves where
         OptionMoves.OptionIndex  = v-inx no-lock no-error.
    if avail OptionMoves then
      leave.
  end.    
  if not avail OptionMoves then 
    do v-inx = 1 to 5 by 1:
      find OptionMoves where
           OptionMoves.OptionIndex  = v-inx no-lock no-error.
      if avail OptionMoves then
        leave.
    end.    
  
  assign ix-newposition      = OptionMoves.Procedure
         ix-currentposition  = OptionMoves.OptionIndex. 
         
  if ix-newposition = "d1" then do:
     assign ix-newposition = "xx"
            ix-currentposition = 0.
     return.
  end.
         
  if ix-newposition = "" then
    assign ix-newposition      = "t1"
           ix-currentposition  = 1. 

end. /* Procedure FindNextFrame */


/* -------------------------------------------------------------------------  */
Procedure LoadQuote:
/* -------------------------------------------------------------------------  */
 def input-output parameter errorfl  as logical      no-undo.

   for each t-lines:
     delete t-lines.
   end.
   assign v-qmode   = "c"
          v-hmode   = "c"
          v-messagelit = " ".
   assign x-quoteno = string(w-batchnm,">>>>>>>9").
   /*{w-oeehb.i x-quoteno 2 no-lock}*/
   find oeehb where oeehb.cono = g-cono and
                    oeehb.batchnm    = x-quoteno and
                    oeehb.seqno      = 2 and
                    oeehb.sourcepros = "ComQu"
                    no-lock no-error.
   if not avail oeehb then 
     do:
     assign errorfl = yes.
     message "Quote "  x-quoteno " is not in Quote Entry.". pause.
     return.
   end.
   assign w-qtyremain = 0
          count = 0.
   if avail oeehb then
     do:
     assign errorfl = no.
     for each oeelb where oeelb.cono = g-cono and
              oeelb.batchnm = oeehb.batchnm and
              oeelb.seqno   = oeehb.seqno
              no-lock:
       assign w-qtyremain = w-qtyremain + (oeelb.qtyord - oeelb.xxde4)
              count = count + 1.
     end. /* each oeelb */
     if w-qtyremain = 0 and count > 0 and g-operinit <> "das" then
       do:
       assign errorfl = yes.
       message "Please view in OEIZS.  Fully Shipped". pause.
       return.
     end. /* no qty remaining */
   end. /* avail oeehb */
   if avail oeehb and oeehb.stagecd = 9 and inquiryfl = no then
     do:
     message 
     "Quote " x-quoteno " has already been converted. View in Inquiry (OEIZS)".
     pause.
     assign errorfl = yes.
     return.
   end.

   {w-arss.i oeehb.custno oeehb.shipto no-lock}
   {w-arsc.i oeehb.custno no-lock}
   if avail arss then
     {w-smsn.i arss.slsrepout no-lock}
   else
     {w-smsn.i arsc.slsrepout no-lock}

   assign v-mode      = "c"
          v-custno    = oeehb.custno
          v-custno    = oeehb.custno
          v-hcustno   = oeehb.custno
          v-shipto    = oeehb.shipto
          v-custlu    = arsc.lookupnm
          v-hcustlu   = arsc.lookupnm
          v-custname  = if avail arss then
                          substr(arss.name,1,25)
                        else
                          substr(arsc.name,1,25)
          v-currency  = if arsc.currencyty = "CN" then
                          "CURRENCY: CN"
                        else ""
          v-terms     = if avail arss then arss.termstype
                        else arsc.termstype
          v-arnotefl  = if avail arss then arss.notesfl else arsc.notesfl
          v-takenby   = oeehb.takenby
          v-canceldt  = oeehb.canceldt
          v-quotetot  = oeehb.xxde2 
          v-quotemgn% = oeehb.user7
          v-contact   = substr(oeehb.user3,1,20)
          v-phone     = substr(oeehb.user3,21,12)
          v-email     = substr(oeehb.user3,33,20)
          v-custpo    = oeehb.custpo
          v-slsrepout = if avail arss then arss.slsrepout else arsc.slsrepout
          v-slsrepin  = if avail arss then arss.slsrepin  else arsc.slsrepin
          v-notefl    = oeehb.notesfl
          v-shiptonm    = oeehb.shiptonm
          v-shiptoaddr1 = oeehb.shiptoaddr[1]
          v-shiptoaddr2 = oeehb.shiptoaddr[2]
          v-shiptocity  = oeehb.shiptocity
          v-shiptost    = oeehb.shiptost
          v-shiptozip   = oeehb.shiptozip
          v-shiptogeocd = oeehb.geocd.
   assign v-commtot = oeehb.totinvamt.
   assign v-Hcommpct = ((v-commtot / v-quotetot) * 100).
   /***       
   find first bpeh where bpeh.cono = g-cono and
                         bpeh.bpid = oeehb.batchnm + "C"
                         no-lock no-error.
   if avail bpeh then
     assign v-Hcommpct = int(bpeh.termstype).
   assign ss-commtot = (v-quotetot * (v-Hcommpct * .01)).
   assign c-commtot  = (ss-commtot * (tot-Ccomm% * .01)).
   assign v-commtot  = ss-commtot - c-commtot.
   ***/
   find zsdiarsc where zsdiarsc.cono = g-cono and
                       zsdiarsc.custno = arsc.custno and
                       zsdiarsc.statustype = yes
                       no-lock no-error.
   if avail zsdiarsc then
     do:
     v-custno:dcolor in frame f-top1 = 2.
   end.
   else
     do:
     v-custno:dcolor in frame f-top1 = 0.
   end.
   
/* %toms% */
   if not v-conversion then
/* %toms% */
    
   display v-quoteno
           v-notefl
           v-custno 
           v-arnotefl
           v-shipto  
           v-custlu  
           v-custname
           v-whse
           v-takenby 
           v-contact 
           v-phone   
         /*  v-email   */
           v-custpo
           v-currency
           v-terms
           v-canceldt
           v-quotetot
           v-commtot
           v-Hcommpct
           with frame f-Top1.
/*   disable v-quoteno with frame f-Top1.*/
   
   for each oeelb where oeelb.cono    = g-cono and
                        oeelb.batchnm = oeehb.batchnm and
                        oeelb.seqno   = oeehb.seqno
                        no-lock:
     find t-lines where t-lines.lineno = oeelb.lineno and
                        t-lines.seqno  = oeelb.seqno  
                        no-lock no-error.
     if avail t-lines then next.
     find first bpel where bpel.cono   = g-cono and
                           bpel.bpid   = oeelb.batchnm + "C" and
                           bpel.lineno = oeelb.lineno
                           no-lock no-error.
     create t-lines.
     assign t-lines.lineno     = oeelb.lineno
            t-lines.seqno      = 0
            t-lines.specnstype = oeelb.specnstype
            t-lines.prod       = oeelb.shipprod
            t-lines.descrip    = oeelb.proddesc
            t-lines.descrip2   = oeelb.proddesc2
            t-lines.xprod      = substr(oeelb.user5,30,24)
            t-lines.sellprc    = oeelb.price
            t-lines.Lcommpct   = if avail bpel then bpel.termspct else 0
            t-lines.listprc    = if oeelb.specnstype = "n" then
                                   oeelb.price
                                 else 0
            t-lines.listfl     = substr(oeelb.user4,25,1)
            t-lines.qty        = oeelb.qtyord
            t-lines.qtyrem     = oeelb.qtyord - oeelb.xxde4
            t-lines.vendno     = oeelb.vendno
            t-lines.slsrepout  = oeelb.slsrepout
            t-lines.slsrepin   = oeelb.slsrepin
            t-lines.prodcat    = oeelb.prodcat
            t-lines.prodcost   = oeelb.prodcost
            t-lines.surcharge  = oeelb.datccost
            t-lines.leadtm     = oeelb.leadtm
            t-lines.pricetype  = oeelb.pricetype
            t-lines.prodline   = oeelb.prodline
/* %toms2% */
            t-lines.convertfl  = if v-conversion then 
                                    if substr(oeelb.user5,2,3) = "yes" then
                                      yes 
                                    else
                                      no
                                 else
                                    no
            t-lines.gp         = dec(substr(oeelb.user4,5,7))
            t-lines.listprc    = dec(substr(oeelb.user4,12,11))
            t-lines.shipdt     = if substr(oeelb.user4,26,8) = "?       " then
                                    ?
                                 else
                                    date(substr(oeelb.user4,26,8))
            t-lines.discpct    = dec(substr(oeelb.user4,34,7))
            t-lines.disctype   = if substr(oeelb.user4,40,1) = "%" then no
                                 else yes
            t-lines.qtybrkty   = substr(oeelb.user4,41,1)
            t-lines.pdscrecno  = int(substr(oeelb.user4,42,8))
            t-lines.pdtype     = int(substr(oeelb.user4,50,1))
            t-lines.pdamt      = dec(substr(oeelb.user4,51,10))
            t-lines.pd$md      = substr(oeelb.user4,61,1)
            t-lines.totnet     = /*dec(substr(oeelb.user4,62,11))*/
                                 oeelb.price * oeelb.qtyord
            t-lines.lcomment   = substr(oeelb.user4,74,1)
            t-lines.totcost    = /*dec(substr(oeelb.user3,44,11))*/
                                 oeelb.prodcost * oeelb.qtyord
            t-lines.rebatefl   = substr(oeelb.user3,55,1)
            t-lines.sparebamt  = oeelb.user6
            t-lines.totmarg    = /*dec(substr(oeelb.user3,56,11))*/
                                 (oeelb.price - oeelb.prodcost) * oeelb.qtyord
            t-lines.margpct    = /*dec(substr(oeelb.user3,67,11)).*/
                                 if oeelb.price > 0 then
                          ((oeelb.price - oeelb.prodcost) / oeelb.price) * 100
                                 else 0.

     find icsp where icsp.cono = 1 and
                     icsp.prod = oeelb.shipprod
                     no-lock no-error.
     if avail icsp then
      assign t-lines.icnotefl = icsp.notesfl.
     assign v-surcharge        = oeelb.datccost 
            v-sparebamt        = oeelb.user6.

   end.
   assign v-quoteloadfl = yes
          g-orderno = v-quoteno
          g-custno  = v-custno
          g-shipto  = v-shipto.

   if v-arnotefl = "!" and (not v-conversion and v-chgfl = no) then
     do:
     run noted.p(yes,"c",string(v-custno),"").
     readkey pause 0.
   end.
end. /* Procedure LoadQuote */



/* ------------------------------------------------------------------------- */
Procedure Update_OEEHB:
/* ------------------------------------------------------------------------- */
  if not avail arss and oeehb.shipto <> ""  then
    find arss where arss.cono   = g-cono and
                    arss.custno = oeehb.custno and
                    arss.shipto = oeehb.shipto
                    no-lock no-error.
    
  if not avail arsc then
    find arsc where arsc.cono   = g-cono and
                    arsc.custno = oeehb.custno 
                    no-lock no-error.
  assign  oeehb.custno        = arsc.custno
          oeehb.takenby       = v-takenby
          oeehb.shipto        = v-shipto
          oeehb.shiptonm      = if v-shiptonm <> "" then v-shiptonm
                                else oeehb.shiptonm
          oeehb.shiptoaddr[1] = if v-shiptoaddr1 <> "" then v-shiptoaddr1
                                else oeehb.shiptoaddr[1]
          oeehb.shiptoaddr[2] = if v-shiptoaddr2 <> "" then v-shiptoaddr2
                                else oeehb.shiptoaddr[2]
          oeehb.shiptocity    = if v-shiptocity <> "" then CAPS(v-shiptocity)
                                else oeehb.shiptocity
          oeehb.shiptost      = if v-shiptost <> "" then CAPS(v-shiptost)
                                else oeehb.shiptost
          oeehb.shiptozip     = if v-shiptozip <> "" then CAPS(v-shiptozip)
                                else oeehb.shiptozip
          oeehb.geocd         = if v-shiptogeocd <> 0 then v-shiptogeocd
                                else oeehb.geocd
          oeehb.statecd       = if avail arss then arss.statecd
                                else arsc.statecd
          oeehb.whse          = if avail arss and v-shipto <> "" 
                                  then arss.whse
                                else arsc.whse
          oeehb.shipviaty     = if avail arss and v-shipto <> ""
                                  then arss.shipviaty
                                else arsc.shipviaty
          oeehb.countrycd     = if oeehb.countrycd <> "" then oeehb.countrycd
                                  else 
                                  if arsc.divno = 40 then "CN"
                                    else arsc.currencyty
          oeehb.custpo        = v-custpo
          oeehb.orderdisp     = if avail arss and v-shipto <> ""
                                  then arss.orderdisp
                                else arsc.orderdisp
          oeehb.termstype     = if avail arss and v-shipto <> ""
                                  then arss.termstype
                                else arsc.termstype
          oeehb.shipinstr     = if avail arss and v-shipto <> "" 
                                  then arss.shipinstr
                                else arsc.shipinstr
          oeehb.slsrepout     = if avail arss and v-shipto <> ""
                                  then arss.slsrepout
                                else if avail arsc then arsc.slsrepout
                                     else v-slsrepout
          oeehb.slsrepin      = if avail arss and v-shipto <> ""
                                  then arss.slsrepin
                                else if avail arsc then arsc.slsrepin
                                     else v-takenby
          oeehb.canceldt      = v-canceldt
          oeehb.user8         = if refreshfl = yes then TODAY else oeehb.user8
          oeehb.placedby      = v-contact
          /*
          oeehb.inbndfrtfl    = if avail arss and v-shipto <> ""
                                  then arss.inbndfrtfl
                                else
                                  if avail arsc then arsc.inbndfrtfl
                                  else oeehb.inbndfrtfl
          oeehb.outbndfrtfl   = if avail arss and v-shipto <> ""
                                  then arss.outbndfrtfl
                                else
                                  if avail arsc then arsc.outbndfrtfl
                                  else oeehb.outbndfrtfl
          */
          oeehb.inbndfrtfl    = no
          oeehb.outbndfrtfl   = no
          /*oeehb.totinvamt     =  v-quotetot*/
          oeehb.xxde1         = tot-Ccomm%
          v-hcustno           = oeehb.custno. 
  overlay(oeehb.user3,1,20)  = string(v-contact,"x(20)").
  overlay(oeehb.user3,21,12) = string(v-phone,"x(12)").
  overlay(oeehb.user3,33,20) = string(v-email,"x(20)").
        
  find first notes use-index k-notes where
                 notes.cono     = g-cono and               
                 notes.notestype = "ba"                 and
                 notes.primarykey   = string(v-quoteno) and
                 notes.secondarykey = string(1) + "c"
                 no-lock no-error.
  if avail notes then do:
    if notes.requirefl = yes then
      assign oeehb.notesfl = "!".
    else
      assign oeehb.notesfl = "*".
  end.
  find sastc where sastc.cono = g-cono and
                   sastc.currencyty = arsc.currencyty
                   no-lock no-error.
  if avail sastc then
    assign oeehb.user6 = if sastc.purchexrate > 0 then
                           1 / sastc.purchexrate
                         else
                           0.
  else
    assign oeehb.user6 = 0.
  assign oeehb.totinvamt = v-commtot.
{t-all.i "oeehb"}
end. /* Procedure Update_OEEHB */

/* ------------------------------------------------------------------------- */
Procedure Update_OEELB:
/* ------------------------------------------------------------------------- */
  
  /* Loosing sales rep in and out and getting operinits every so often
     so I'm going to refresh the buffers here.  TAH
  */   
  if avail oeehb then 
    do:
    find arsc where arsc.cono = oeehb.cono and
                    arsc.custno = oeehb.custno no-lock no-error. 
  
    if oeehb.shipto <> "" then
      find arss where arss.cono = oeehb.cono and
                      arss.custno = oeehb.custno and
                      arss.shipto  = oeehb.shipto no-lock no-error. 
    if avail oeelb then
      do:
      run xsdioetlt.p (input-output v-slsrepout,
                       input v-prodcat,
                       input oeehb.custno,
                       input oeehb.shipto).
    end. /* if avail oeelb */
  end. /* if avail oeehb */
  
  run Get_Tax_Flag (input-output v-taxablefl,
                    input-output v-nontaxtype).
  
   assign  oeelb.cono        = g-cono
           oeelb.batchnm     = oeehb.batchnm
           oeelb.seqno       = oeehb.seqno
           oeelb.user7       = oeehb.custno 
           oeelb.enterdt     = TODAY
           oeelb.reqshipdt   = if v-shipdt <> ? then v-shipdt
                               else oeehb.reqshipdt
           oeelb.promisedt   = if v-shipdt <> ? then v-shipdt
                               else oeehb.reqshipdt
           oeelb.xxda1       = if manualdtfl = yes then v-shipdt 
                               else oeelb.xxda1
           /*oeelb.lineno      = integer(v-lineno)*/
           oeelb.shipprod    = v-prod
           oeelb.proddesc    = if avail icsp then icsp.descrip[1]
                               else 
                                  if v-descrip <> "" then v-descrip
                                  else ""
           oeelb.proddesc2   = if avail icsp and icsp.descrip[2] <> "" 
                                 then icsp.descrip[2]
                               else if v-descrip2 <> "" then v-descrip2
                                    else oeelb.proddesc2
           oeelb.price       = if manual-change = yes then v-sellprc
                               else oeelb.price
           oeelb.datccost    = v-surcharge
           oeelb.priceoverfl = if oeelb.priceoverfl = yes then
                                 oeelb.priceoverfl else
                               if v-sellprc <> orig-price then
                                 yes else no
           oeelb.pricetype   = if v-specnstype = "n" then v-ptype
                               else 
                                  if avail icsw then icsw.pricetype
                                  else oeelb.pricetype
           oeelb.specnstype  = if v-specnstype = "" then " " else
                                  v-specnstype
           oeelb.usagefl     = if v-specnstype = "n" then no 
                               else if avail icsp and icsp.statustype = "l"
                                       then no
                                    else oeelb.usagefl
           oeelb.rushfl      = v-rushfl
           oeelb.costoverfl  = if oeelb.costoverfl = no then v-costoverfl
                               else oeelb.costoverfl
           oeelb.qtyord      = dec(v-qty).
    assign oeelb.reqprod     = v-reqprod
           oeelb.xrefprodty  = if v-xrefty <> "" then
                                 v-xrefty
                               else if v-reqprod <> "" then "c"
                               else oeelb.xrefprodty
           oeelb.unit        = if avail icsp and icsp.unitsell <> "" then
                                  icsp.unitsell 
                               else
                               if avail icsp and icsp.unitstock <> "" then
                                  icsp.unitstock
                               else if v-cunit <> "" then
                                  v-cunit
                               else
                                  "EACH"
           oeelb.leadtm      = v-leadtm
           oeelb.commentfl   = if can-find
                           (first com use-index k-com where
                            com.cono     = g-cono         and
                            com.comtype  = "ob" + string(w-batchnm,">>>>>>>9")
                            and com.lineno = v-lineno
                            no-lock)
                            then yes else no
           oeelb.prodcat     = if v-prodcat <> "" then v-prodcat
                               else if avail icsp then icsp.prodcat
                               else oeelb.prodcat
           oeelb.prodline    = if v-prodline <> " " then v-prodline
                               else if avail icsw then icsw.prodline
                               else oeelb.prodline
           oeelb.taxgroup    = if v-conversion then
                                 oeelb.taxgroup
                               else
                               if avail icsw then icsw.taxgroup else 1
           oeelb.gststatus   = if avail icsw then icsw.gststatus else true
           oeelb.netamt      = v-totnet
           oeelb.user1       = if oeelb.costoverfl = yes and 
                               ((over-cost >= 0 and v-specnstype = "n")
                                 or over-cost > 0) then
                                 "y" + string(over-cost)
                               else
                                 if oeelb.costoverfl = no and v-specnstype = "n"
                                   then "y" + string(over-cost)
                                 else
                                   oeelb.user1
           oeelb.xxc3         = /*if v-conversion then oeelb.xxc3 
                                else oeehb.whse.*/ string(oeelb.arpvendno).
   overlay(oeelb.user4,1,4)   = if v-conversion then substr(oeelb.user4,1,4) 
                                else oeehb.whse.
   overlay(oeelb.user4,5,7)   = if v-gp <> ? then string(v-gp,">>9.99-")
                                else string(0,">>9.99-").
   overlay(oeelb.user4,12,13) = string(v-listprc,">>>>>>9.99999").
   overlay(oeelb.user4,25,1)  = v-listfl.
   overlay(oeelb.user4,26,8)  = if v-shipdt = ? then "?       "
                                else string(v-shipdt,"99/99/99").
   overlay(oeelb.user4,62,11) = string(v-totnet,">>>>>>9.99-").
   overlay(oeelb.user4,74,1)  = v-lcomment.
   overlay(oeelb.user4,75,4)  = v-pdbased.
   overlay(oeelb.user5,30,24) = v-xprod.
   /* was >>>,>>9.99- */
   overlay(oeelb.user3,44,11) = string(v-totcost,">>>>>>9.99-").
   overlay(oeelb.user3,55,1)  = if v-rebatefl <> " " then v-rebatefl
                                else substr(oeelb.user3,55,1).
   /* was >>>,>>9.99- */
   overlay(oeelb.user3,56,11) = string(v-totmarg,">>>>>>9.99-").
   overlay(oeelb.user3,67,11) = string(v-margpct,">>>>>>9.99-").

   assign  oeelb.ordertype    =  if not v-conversion then 
                                   " "
                                 else
                                   oeelb.ordertype.
   assign  oeelb.slsrepin    = if v-conversion then
                                 oeelb.slsrepin
                               else 
                                 if v-slsrepin = "" then oeelb.slsrepin
                                 else v-slsrepin
                               /*
                               if avail arss and v-shipto <> ""
                                 then arss.slsrepin
                               else if avail arsc then arsc.slsrepin
                               else g-operinit
                               */
           oeelb.slsrepout   = if v-conversion then
                                 oeelb.slsrepout
                               else  
                               if v-slsrepout = "" then oeelb.slsrepout
                               else v-slsrepout                    
           oeelb.nontaxtype  = if v-conversion then
                                 oeelb.nontaxtype
                               else  
                                 v-nontaxtype.
   /*tb 24060 09/13/99 kjb; Load additional information if processing
        a catalog product */
   
   assign  oeelb.botype       = if v-conversion then 
                                  oeelb.botype
                                else  
                                if v-botype = "" then
                                  if v-5603fl = yes then "n"
                                  else
                                    if avail arsc and arsc.bofl = yes
                                      then "y"
                                    else "n"
                                else
                                  /* das - 06/23/09 */
                                  if avail arsc and arsc.bofl = yes
                                    then "y"
                                  else
                                    v-botype
           oeelb.user2       = if v-glcost <> 0 then 
                                 string(v-glcost,">>>>>9.99")
                               else if avail icsw then 
                                      string(icsw.avgcost,">>>>>9.99")
                                    else string(0,">>>>>9.99").
   assign  oeelb.termspct    = v-Lcommpct.
   /** since company commissions must be taken into consideration, prodcost is
       caclulated at the end 
   assign  oeelb.prodcost    = if oeelb.qtyord <> 0 then
                                 (oeelb.netamt * (1 - (.01 * oeelb.termspct)) /
                                                             oeelb.qtyord)
                               else 0.
   assign  v-totcost         = oeelb.prodcost * oeelb.qtyord.
   **/
   assign  oeelb.vendno      = if v-conversion then
                                 oeelb.vendno
                               else
                                 if v-vendno <> 0 then v-vendno
                                 else
                                 if avail icsw and 
                                          icsw.prod = oeelb.shipprod then
                                   icsw.arpvendno
                                 else 0
           oeelb.arpvendno   = if oeelb.specnstype <> " " then oeelb.vendno
                                 else oeelb.arpvendno
           oeelb.taxablefl   = if v-conversion then
                                 oeelb.taxablefl
                               else  
                                 v-taxablefl
           oeelb.printpricefl = yes
           oeelb.subtotalfl   = no.
   assign commission-amt = 0.
   if oeelb.specnstype = "n" then
     do:
     if oeelb.reqprod = " " then
       assign oeelb.xrefprodty = " ".
     else
       assign oeelb.xrefprodty = "c".
   end.

   for each bpel where bpel.cono   = g-cono and
                       bpel.bpid   = oeelb.batchnm + "C" and
                       bpel.lineno = oeelb.lineno   /* 071015 */
                       no-lock:
     if bpel.revno = 1 and bpel.itemid <> " " then
       assign oeelb.xxde1 = dec(bpel.commtype).
     if bpel.revno = 2 and bpel.itemid <> " " then
       assign oeelb.xxde2 = dec(bpel.commtype).
     if bpel.revno = 3 and bpel.itemid <> " " then
       assign oeelb.xxde3 = dec(bpel.commtype).
     if bpel.revno <= 3 then
       assign commission-amt = commission-amt + bpel.awardprice.
   end. /* each pbel */
   assign oeelb.prodcost = (oeelb.netamt - commission-amt) / oeelb.qtyord.
   assign v-totcost      = oeelb.prodcost * oeelb.qtyord.

   /* keep track of the qty converted */
   /**
   MOVED TO CONVERSION_LINE_EDIT
   if v-conversion then
     do:
     /*
     if g-operinit begins "das" then
       do:
       message "in the update oeelb. we're updating xxde4 and specnstype".
       pause.
     end.
     */
     find c-oeelb where c-oeelb.cono    = g-cono + 500 and
                        c-oeelb.batchnm = oeelb.batchnm and
                        c-oeelb.seqno   = oeelb.seqno and
                        c-oeelb.lineno  = oeelb.lineno
                        no-error.
     if avail c-oeelb then
       do:
       /*assign c-oeelb.xxde4 = c-oeelb.xxde4 + oeelb.qtyord.*/
      /*if user set qty to 0 so it doesn't convert, we can't send to lost bus*/
       if c-oeelb.xxde4 = c-oeelb.qtyord then
         assign c-oeelb.specnstype = "l"
                c-oeelb.reasunavty = "CO".
     end.           
   end. /* if v-conversion */
   **/    
   {t-all.i "oeelb"}
   /* fix misc issues with xrefprodty */
   if (oeelb.xrefprodty = " " or oeelb.xrefprodty = ""
                              or oeelb.xrefprodty = "g") and
     oeelb.reqprod <> " " then
     assign oeelb.xrefprodty = "c".
  
   
   
   
   for each bpel where bpel.cono   = g-cono and
                       bpel.bpid   = oeelb.batchnm + "C" and
                       bpel.lineno = oeelb.lineno and
                       bpel.itemid <> " ":
     assign bpel.prod       = oeelb.shipprod
            bpel.qtyord     = oeelb.qtyord
            bpel.unit       = "EACH"
            bpel.price      = oeelb.price
            bpel.extprice   = bpel.price * bpel.qtyord
          /*bpel.seqid      = bpeh.slsrepout*/
          /*bpel.commtype   = bpeh.commtype*/
            bpel.termspct   = if bpel.itemid <> " " then v-Lcommpct else 0.
     assign bpel.awardprice = if bpel.revno <= 3 then
                               ((bpel.extprice * (.01 * bpel.termspct)) *
                                               (.01 * int(bpel.commtype))) /* *
                                               if oeehb.xxde1 > 0 then 
                                                 (1 - (.01 * oeehb.xxde1))
                                               else
                                                 1                         */
                              else
                                (bpel.extprice * (.01 * bpel.termspct)) *
                                                 (.01 * int(bpel.commtype)).
   end. /* for each bpel */
  
  /* update t-lines */
  find t-lines where t-lines.lineno = oeelb.lineno  and
                     t-lines.seqno  = 0 no-error.      
  find first bpel where bpel.cono   = g-cono and
                        bpel.bpid   = oeelb.batchnm + "C" and
                        bpel.lineno = oeelb.lineno
                        no-lock no-error.
            
  if not v-conversion then
    do:
    assign  t-lines.vendno     = oeelb.vendno
            t-lines.slsrepout  = oeelb.slsrepout
            t-lines.slsrepin   = oeelb.slsrepin
            t-lines.prodcat    = oeelb.prodcat
            t-lines.prodline   = oeelb.prodline
            t-lines.listprc    = dec(substr(oeelb.user4,12,13))
            t-lines.listfl     = substr(oeelb.user4,25,1)
            t-lines.pricetype  = oeelb.pricetype
            t-lines.sellprc    = oeelb.price
            t-lines.Lcommpct   = if avail bpel then bpel.termspct else 0
            t-lines.prodcost   = oeelb.prodcost
            t-lines.glcost     = dec(oeelb.user2)
            t-lines.surcharge  = oeelb.datccost
            t-lines.sparebamt  = oeelb.user6
            t-lines.rebatefl   = substr(oeelb.user3,55,1)
            t-lines.leadtm     = oeelb.leadtm
            t-lines.gp         = dec(substr(oeelb.user4,5,7))
            t-lines.discpct    = dec(substr(oeelb.user4,34,7))
            t-lines.qty        = oeelb.qtyord
            t-lines.qtyrem     = oeelb.qtyord - oeelb.xxde4
            t-lines.shipdt     = oeelb.reqshipdt
            t-lines.specnstype = oeelb.specnstype
            t-lines.pdscrecno  = int(substr(oeelb.user4,42,8))    
            t-lines.qtybrkty   = substr(oeelb.user4,41,1)
            t-lines.pdtype     = int(substr(oeelb.user4,50,1))
            t-lines.pdamt      = dec(substr(oeelb.user4,51,10))
            t-lines.pd$md      = substr(oeelb.user4,61,1)
            t-lines.pdbased    = substr(oeelb.user4,75,4)
            t-lines.totnet     = dec(substr(oeelb.user4,62,11))
            t-lines.totcost    = dec(substr(oeelb.user3,44,11))
            t-lines.rebatefl   = substr(oeelb.user3,55,1)
            t-lines.totmarg    = dec(substr(oeelb.user3,56,11))
            t-lines.margpct    = dec(substr(oeelb.user3,67,11)).
   end. /* not v-conversion */
   /* update t-lines */
end. /* procedure Update-OEELB */

/* ------------------------------------------------------------------------- */
Procedure Quote-Total:
/* ------------------------------------------------------------------------- */
   def var work-Hcommpct as de format ">>9.99" no-undo.
   def var work-lcount   as  i format ">>9"    no-undo.
   
   assign v-commtot     = 0
          v-Hcommpct    = 0
          work-Hcommpct = 0
          work-lcount   = 0
          v-quotetot    = 0.
   for each t-oeelb where t-oeelb.cono    = g-cono and
                          t-oeelb.batchnm = string(w-batchnm,">>>>>>>9") and
                          t-oeelb.seqno   = oeehb.seqno and
                          t-oeelb.specnstype <> "L"
                          no-lock:
     for each bpel where bpel.cono = g-cono and
                         bpel.bpid = t-oeelb.batchnm + "C" and
                         bpel.lineno = t-oeelb.lineno and
                         bpel.revno <= 3
                         no-lock:
       assign v-commtot = v-commtot + bpel.awardprice.
     end.
     
     assign v-quotetot = v-quotetot + 
                         ((t-oeelb.price * t-oeelb.qtyord) + 
                         (t-oeelb.datccost * t-oeelb.qtyord))
            v-mgn%net  = v-mgn%net + (t-oeelb.price * t-oeelb.qtyord)
            v-mgn%cost = v-mgn%cost + (t-oeelb.prodcost * t-oeelb.qtyord).
     
   end.
   
   /****
   assign work-Hcommpct = ROUND(work-Hcommpct / work-lcount,1).
   assign v-Hcommpct = work-Hcommpct.
   assign ss-commtot = (v-quotetot * (v-Hcommpct * .01)).
   assign c-commtot  = (ss-commtot * (tot-Ccomm% * .01)).
   assign v-commtot  = ss-commtot - c-commtot.
   ****/
   if v-quotetot > 0 then
     assign v-Hcommpct = (v-commtot / v-quotetot) * 100.
   else 
     assign v-Hcommpct = 0.
   
   for each bpeh where bpeh.cono = g-cono and
                       bpeh.bpid  = string(v-quoteno,">>>>>>>9") + "C" and
                       bpeh.revno <= 3 and
                       bpeh.termstype <> "0.0":
     assign bpeh.termstype = string(v-Hcommpct,">>9.99").
   end.

   if v-mgn%net > 0 then
      assign v-quotemgn% = 
             ROUND(((v-mgn%net - v-mgn%cost) / v-mgn%net) * 100,3).
   else
      assign v-quotemgn% = v-mgn%net - v-mgn%cost.
   if v-quotemgn% >  999999.99 then assign v-quotemgn% =  999999.99.
   if v-quotemgn% < -999999.99 then assign v-quotemgn% = -999999.99.
/* %toms% */

   if not v-conversion then
/* %toms% */

   display v-quotetot 
           v-commtot
           v-Hcommpct
           with frame f-top1.
   if v-quotetot <> 0 then do transaction:
     find oeehb where oeehb.cono = g-cono and
                      oeehb.batchnm    = string(w-batchnm,">>>>>>>9") and 
                      oeehb.seqno      = 2 and
                      oeehb.sourcepros = "ComQu"
                      no-error.

     if avail oeehb then
       do:
       if oeehb.whse <> " " then
         do:
          /* %Tom 08/03 make sure oeehb is recent */
         assign  d-whse              = oeehb.whse
                 oeehb.shiptonm      = if v-shiptonm <> "" then
                                        v-shiptonm 
                                      else
                                        oeehb.shiptonm
                 oeehb.shiptoaddr[1] = if v-shiptoaddr1 <> "" then
                                         v-shiptoaddr1
                                       else
                                         oeehb.shiptoaddr[1]
                 oeehb.shiptoaddr[2] = if v-shiptoaddr2 <> "" then
                                         v-shiptoaddr2 
                                       else
                                         oeehb.shiptoaddr[2]
                 oeehb.shiptocity    = if v-shiptocity <> "" then
                                         v-shiptocity
                                       else
                                         oeehb.shiptocity
                 oeehb.shiptozip     = if v-shiptozip <> "" then
                                         v-shiptozip
                                       else
                                         oeehb.shiptozip
                 oeehb.geocd         = if v-shiptogeocd <> 0 then
                                         v-shiptogeocd 
                                       else
                                         oeehb.geocd
                v-shiptonm    = if v-shiptonm    = " " then oeehb.shiptonm
                                  else v-shiptonm
                v-shiptoaddr1 = if v-shiptoaddr1 = " " then 
                                  oeehb.shiptoaddr[1]
                                else v-shiptoaddr1
                v-shiptoaddr2 = if v-shiptoaddr2 = " " then 
                                  oeehb.shiptoaddr[2]
                                else v-shiptoaddr2
                v-shiptocity  = if v-shiptocity = " " then 
                                  CAPS(oeehb.shiptocity)
                                else v-shiptocity
                v-shiptost    = if v-shiptost    = " " then 
                                  CAPS(oeehb.shiptost)
                                else v-shiptost
                v-shiptozip   = if v-shiptozip   = " " then 
                                  oeehb.shiptozip
                                else v-shiptozip
                v-shiptogeocd = if v-shiptogeocd = 0 then
                                  oeehb.geocd
                                 else v-shiptogeocd.
         assign v-idoeeh  = recid(oeehb)
                v-idoeeh0 = ?
                g-secure  = v-secure
                g-custno  = oeehb.custno
                g-orderno = int(w-batchnm).
         run oeebti.p.
       end.

       assign oeehb.xxde2     = v-quotetot
              oeehb.totinvamt = v-commtot
              oeehb.user7     = v-quotemgn%.
     end. /* avail oeehb */
   end. /* v-quotetot <> 0 */
end. /* procedure Quote-Total */
        

/* ------------------------------------------------------------------------- */
Procedure Find_Whse: 
/* ------------------------------------------------------------------------- */
  def input        parameter x-prodtype  as c format "x(4)"      no-undo.
/* once the warehouse is establised it should stick
   so this is defining the buffer to check that out  %TAH */
  def buffer fw-oeehb for oeehb.
  find fw-oeehb where fw-oeehb.cono = g-cono and
                      fw-oeehb.batchnm = string(w-batchnm,">>>>>>>9") and 
                      fw-oeehb.seqno = 2 and
                      fw-oeehb.sourcepros = "ComQu"
                      no-lock no-error.

  if avail fw-oeehb then do:
    find icsd where icsd.cono    = g-cono and
                    icsd.whse    = fw-oeehb.whse and
                    icsd.salesfl = yes
                    no-lock no-error.
    if avail icsd then
      do:
      assign v-whse = if x-prodtype = "comp" then h-whse else fw-oeehb.whse
             g-whse = if x-prodtype = "comp" then h-whse else fw-oeehb.whse
             v-specnstype = if v-specnstype <> "n" then " " else v-specnstype
             v-statustype = " ".
      run Get-Whse-Info (input v-prod,
                         input v-whse,
                         input v-qty,
                         input-output v-shipdt,
                         input-output v-specnstype,
                         input-output v-ptype,
                         input-output v-foundfl,
                         input-output v-statustype,
                         input-output v-prodline,
                         input-output v-rarrnotesfl).
    end. /* if avail icsd */
    
/* once the warehouse is establised it should stick
   so this is defining the buffer to check that out  %TAH */

    if x-prodtype <> "comp" and fw-oeehb.whse <> "" and
       v-whse = "" then
      do:
      assign v-whse  = fw-oeehb.whse
             g-whse  = fw-oeehb.whse
             v-specnstype = if v-specnstype <> "n" then " " else v-specnstype
             v-statustype = " ".
      run Get-Whse-Info (input v-prod,
                         input v-whse,
                         input v-qty,
                         input-output v-shipdt,
                         input-output v-specnstype,
                         input-output v-ptype,
                         input-output v-foundfl,
                         input-output v-statustype,
                         input-output v-prodline,
                         input-output v-rarrnotesfl).

    end. /*if x-prodtype <> "comp" and fw-oeehb.whse <> "" */
    else if v-whse = " " then
      do:
      for each icsw where 
               icsw.cono = g-cono and
               icsw.prod = v-prod and
        substr(icsw.whse,1,1) <> "c" and
               /*icsw.statustype <> "x" and  PER TAMI, IGNORE STATUS */
               icsw.qtyonhand - (icsw.qtycommit + icsw.qtyreservd) > 0
               no-lock:
        find icsd where icsd.cono    = g-cono and
                        icsd.whse    = icsw.whse and
                        icsd.salesfl = yes and
                        icsd.custno  = 0
                        no-lock no-error.
        if avail icsd then 
          do:
          assign v-whse = if x-prodtype = "comp" then h-whse else icsd.whse
                 g-whse = if x-prodtype = "comp" then h-whse else icsd.whse
                 v-specnstype = if icsw.statustype = "o" and 
                                  v-specnstype <> "n" then "s"
                                else v-specnstype
                 x-qtyavail = 
                       icsw.qtyonhand - icsw.qtyreserv - icsw.qtycommit
                 v-vendno = icsw.arpvendno
                 v-ptype  = icsw.pricetype.
          if v-shipdt = ? then
            run Assign_LineDate (input v-lmode,
                              /* input repricefl, */
                                 input v-whse,
                                 input x-qtyavail,
                                 input v-qty,
                                 input icsw.prod,
                                 input icsw.statustype,
                                 input icsw.qtyreserv,
                                 input icsw.qtycommit,
                                 input icsw.avgltdt,
                                 input icsw.leadtmavg,
                                 input icsd.enddaycut,
                                 input-output v-shipdt).
         
          assign hold-linedt = v-shipdt.
          leave. /* for each icsw */
        end. /* avail icsd */
        else
          next.
      end. /* each icsw */
      if v-whse = "" then
        do:
        for each icsw where 
                 icsw.cono        = g-cono and
                 icsw.prod        = v-prod and
          substr(icsw.whse,1,1)  <> "c" 
                 /*and icsw.statustype <> "x"   PER TAMI, IGNORE STATUS */
                 no-lock:
          find icsd where icsd.cono    = g-cono and
                          icsd.whse    = icsw.whse and
                          icsd.salesfl = yes and
                          icsd.custno  = 0
                          no-lock no-error.
          if avail icsd then 
            do:
            assign v-whse = if x-prodtype = "comp" then h-whse else icsd.whse
                   g-whse = if x-prodtype = "comp" then h-whse else icsd.whse
                   v-specnstype = if icsw.statustype = "o" and 
                                    v-specnstype <> "n" then "s"
                                  else v-specnstype
                   x-qtyavail = 
                         icsw.qtyonhand - icsw.qtyreserv - icsw.qtycommit
                   v-vendno = icsw.arpvendno
                   v-ptype  = icsw.pricetype.
            if v-shipdt = ? then
              run Assign_LineDate (input v-lmode,
                                 /*  input repricefl,*/
                                   input v-whse,
                                   input x-qtyavail,
                                   input v-qty,
                                   input icsw.prod,
                                   input icsw.statustype,
                                   input icsw.qtyreserv,
                                   input icsw.qtycommit,
                                   input icsw.avgltdt,
                                   input icsw.leadtmavg,
                                   input icsd.enddaycut,
                                   input-output v-shipdt).
         
            assign hold-linedt = v-shipdt.
            leave. /* for each icsw */
          end. /* avail icsd */
          else
            next.
        end. /* each icsw */
      end. /* if whse = "" */
    end. /* if v-whse = "" */
  end. /* if avail fw-oeehb */
  assign o-whse = v-whse.
  /*display v-whse with frame f-mid1.*/
  if v-statustype = "x" and v-5603fl = no then 
    do:
    find first icsec where icsec.cono = g-cono and     
                           icsec.rectype = "c" and     
                           icsec.custno  = oeehb.custno and
                           icsec.prod = v-prod
                           no-lock
                           no-error.
    if not avail icsec then
      do:
      if v-specnstype = "n" then
        assign v-5603fl = yes.
      else
        do:
        run warning.p(5603).     
        assign v-5603fl = yes.
      end.
    end. /* not avail icsec */
    else
      message "Customer Cross Reference will be Selected". pause 2.
  end. /* if v-statustype = "x" and v-5603fl = no */
end. /* Procedure Find_Whse */


/* ------------------------------------------------------------------------- */
Procedure Assign-Update-Quote:
/* ------------------------------------------------------------------------- */
  assign v-hmode = "c".
  do transaction:
   if v-quoteno = 0 then do:
      if not avail arsc then
         {w-arsc.i v-custno no-lock}
      if v-shipto <> "" then
        {w-arss.i v-custno v-shipto no-lock}
      /*
      /* to bring back data faster, use an enterdt we know is less */
      find last L-oeehb use-index k-oeehb where 
                L-oeehb.cono       = g-cono     and
                L-oeehb.batchnm   >= "       1" and
                L-oeehb.batchnm   <= "   59999" and
                L-oeehb.seqno      = 2          and
                L-oeehb.sourcepros = "ComQu"
                no-lock no-error.
      if avail L-oeehb then do:
         assign w-batchnm = int(L-oeehb.batchnm) + 1.
      end.
      else
         assign w-batchnm = 1.
      */
      for each m-oeehb use-index k-oeehb where m-oeehb.cono = g-cono no-lock:
        if m-oeehb.sourcepros = "ComQu" and m-oeehb.seqno = 2 then
          assign w-batchnm = int(m-oeehb.batchnm).
        if m-oeehb.sourcepros = "Quote" and m-oeehb.seqno = 1 then
          do:
          find L-oeehb use-index k-oeehb where
               L-oeehb.cono = g-cono and
               L-oeehb.batchnm = m-oeehb.batchnm and
               L-oeehb.seqno = 2 and
               L-oeehb.sourcepros = "ComQu"
               no-lock no-error.
          if avail L-oeehb then
            next.
          else
            do:
            assign w-batchnm = w-batchnm + 1.
            leave.
          end.
        end. /* find a matching ComQu */
      end. /* for each m-oeehb */
      assign v-quoteno = w-batchnm.
      find sasc where sasc.cono = g-cono no-lock no-error.
      create oeehb.

      run oeehb_create_rec.
      release L-oeehb no-error.
      if oeehb.whse <> " " then
        do:
        find icsd where icsd.cono = g-cono and
                        icsd.whse = oeehb.whse and
                        icsd.salesfl = yes
                        no-lock no-error.
        if not avail icsd then
          assign oeehb.whse = " ".
      end.
      if oeehb.shipto <> "" then 
         assign v-shipfl = yes.
      else 
         assign v-shipfl = no.
      
      find first notes use-index k-notes where
                 notes.cono     = g-cono and               
                 notes.notestype = "ba"                 and
                 notes.primarykey   = string(v-quoteno) and
                 notes.secondarykey = string(1) + "c"
                 no-lock no-error.
      if avail notes then do:
        if notes.requirefl = yes then
          assign oeehb.notesfl = "!".
        else
          assign oeehb.notesfl = "*".
      end.
      assign v-idoeeh  = recid(oeehb)
             v-idoeeh0 = ?
             g-secure  = v-secure
             g-whse    = oeehb.whse
             g-custno  = oeehb.custno
             g-orderno = int(w-batchnm).
      run oeebti.p.

      assign v-quoteno = int(oeehb.batchnm)
             v-hcustno = oeehb.custno.
      if v-shiptonm <> " " then
         assign oeehb.shiptonm    = v-shiptonm.
      if v-shiptoaddr1 <> " " or (v-shiptonm <> "" and v-shiptoaddr1 = "") then
         assign oeehb.shiptoaddr[1] = v-shiptoaddr1.
      if v-shiptoaddr2 <> " " or (v-shiptonm <> "" and v-shiptoaddr2 = "") then
         assign oeehb.shiptoaddr[2] = v-shiptoaddr2.
      if v-shiptocity <> " " then
         assign oeehb.shiptocity  = CAPS(v-shiptocity).
      if v-shiptost <> " " then
         assign oeehb.shiptost = CAPS(v-shiptost).
      if v-shiptozip <> " " then
         assign oeehb.shiptozip = CAPS(v-shiptozip).
      if v-shiptogeocd <> 0 then
         assign oeehb.geocd = v-shiptogeocd.
   end. /* v-quoteno > 0 */
   else do:  /* update oeehb */
      if v-hcustno = v-custno then do:
         find oeehb where oeehb.cono = g-cono and   
                          oeehb.batchnm = string(w-batchnm,">>>>>>>9") and
                          oeehb.seqno = 2 and
                          oeehb.sourcepros = "ComQu"
                          no-error.
         if avail oeehb then do:
/* Warehouse is getting zapped when there is not a customer change %TAH */
  
           assign d-whse = oeehb.whse.
           run Update_OEEHB.
           if oeehb.whse = "" then
              assign oeehb.whse = d-whse.
         end.
      end.
      else do:
         /* customer changed - reprice lines */
         find oeehb where oeehb.cono = g-cono and   
                          oeehb.batchnm = string(w-batchnm,">>>>>>>9") and
                          oeehb.seqno = 2 and
                          oeehb.sourcepros = "ComQu"
                               no-error.
         if avail oeehb then 
           do:
            assign v-hcustlu = v-custlu
                   v-custlu = arsc.lookupnm 
                   v-custno = arsc.custno
                   g-custno = arsc.custno
                   v-custname = substr(arsc.name,1,25)
                   v-slsrepout = arsc.slsrepout
                   o-slsrepout = arsc.slsrepout
                   v-slsrepin  = arsc.slsrepin
                   v-arnotefl  = arsc.notesfl
                   v-shipto    = ""
                   v-contact   = ""
                   v-phone     = ""
                   v-custpo    = arsc.custpo
                   v-notefl    = ""
                   v-canceldt  = /*today + 360.*/ v-canceldt.
/* %toms% */
           if not v-conversion then
/* %toms% */


            display  v-notefl
                     v-custname
                     v-custno
                     v-arnotefl
                     v-whse
                     v-takenby
                     v-shipto
                     v-contact
                     v-phone
                     v-custpo
                     v-canceldt
                     v-quotetot
                     v-commtot v-Hcommpct
                     v-custlu 
            with frame f-top1.
            run Update_OEEHB.
         end.
         run RePrice.
         run Quote-total.
      end. /* Customer changed. Reprice lines */
    end. /* update OEEHB */
  end. /* transaction */
end. /* Procedure Assign-Update-Quote */


/* ------------------------------------------------------------------------- */
Procedure Get-Whse-Info:
/* ------------------------------------------------------------------------- */
  def input        parameter v-prod        like oeel.shipprod    no-undo.
  def input        parameter v-whse        like oeel.whse        no-undo.
  def input        parameter v-qty         like oeel.qtyord      no-undo.
  def input-output parameter v-shipdt      like oeel.promisedt   no-undo.
  def input-output parameter v-specnstype  like oeel.specnstype  no-undo.
  def input-output parameter v-ptype       like icsw.pricetype   no-undo.
  def input-output parameter v-foundfl     as logical            no-undo.
  def input-output parameter v-statustype  like icsw.statustype  no-undo.
  def input-output parameter v-prodline    like icsw.prodline    no-undo.
  def input-output parameter v-rarrnotesfl as logical            no-undo.

  find t-oeehb where 
       t-oeehb.cono       = g-cono and   
       t-oeehb.batchnm    = string(w-batchnm,">>>>>>>9") and
       t-oeehb.seqno      = 2 and
       t-oeehb.sourcepros = "ComQu"
       no-lock no-error.

  if avail t-oeehb and t-oeehb.whse <> " "  and v-rarrnotesfl <> yes and
     v-whse = t-oeehb.whse and
     (lastkey = 9 or lastkey = 13 or lastkey = 401) then
    do:
    {w-icsw.i v-prod t-oeehb.whse no-lock "t-"}
    if avail t-icsw then
      assign v-specnstype = if t-icsw.statustype = "o" and v-specnstype <> "n"
                              then "s"
                            else 
                              if v-specnstype <> "n" then ""
                              else v-specnstype.
    else do:
      find b-icsp where b-icsp.cono = g-cono and
                        b-icsp.prod = v-prod and
                        b-icsp.kittype = "B"
                        no-lock no-error.
      if not avail b-icsp then
        do:
        run zsdimaincpy.p (input g-cono,
                           input " ",
                           input t-oeehb.whse,
                           input v-prod,
                           input v-lineno,
                           input-output zsdi-confirm).
        if not can-find (first icsw where 
                               icsw.cono = g-cono and
                               icsw.whse = v-whse and
                               icsw.prod = v-prod no-lock) then
          do:
          run oeexqICSW.p.
/* Callmod */                                                                  
          if not can-find (first icsw where
                                 icsw.cono = g-cono and
                                 icsw.whse = v-whse and
                                 icsw.prod = v-prod no-lock) then 
            do:
            message
            "Product/Warehouse Not Set Up in Warehouse Products - ICSW (4602)".
            next-prompt v-whse with frame f-mid1.
            next.
           end.
/* Callmod */
        end.
      end. /* not avail b-icsp - kittype of "B" */
    end. /* icsw not avail */
    {w-icsw.i v-prod t-oeehb.whse no-lock "t-"}
    if avail t-icsw then
      assign v-specnstype = if t-icsw.statustype = "o" and v-specnstype <> "n"
                              then "s"
                            else 
                              if v-specnstype <> "n" then ""
                              else v-specnstype.
  end.
  else
    do:
    if v-rarrnotesfl <> yes then
      do:
      {w-icsw.i v-prod v-whse no-lock "t-"}
      if avail t-icsw then
        assign v-specnstype = if t-icsw.statustype = "o" and 
                                v-specnstype <> "n" then "s"
                              else 
                                if v-specnstype <> "n" then "" 
                                else v-specnstype.
    end.
  end.

  v-foundfl = no.
  {w-icsw.i v-prod v-whse no-lock}
  if avail icsw then 
    do:
    assign v-foundfl    = yes
           x-qtyavail   = icsw.qtyonhand - icsw.qtyreserv - icsw.qtycommit
           v-ptype      = icsw.pricetype
           v-prodline   = icsw.prodline
           v-leadtm     = icsw.leadtmavg
           v-statustype = icsw.statustype
           v-vendno     = icsw.arpvendno.
    find icsd where icsd.cono = g-cono and icsd.whse = v-whse no-lock no-error.
    if v-shipdt = ? or (v-shipdt <> ? and v-whse <> orig-whse and
                        not v-conversion) then
      run Assign_LineDate (input v-lmode,
                        /* input repricefl,*/
                           input v-whse,
                           input x-qtyavail,
                           input v-qty,
                           input icsw.prod,
                           input icsw.statustype,
                           input icsw.qtyreserv,
                           input icsw.qtycommit,
                           input icsw.avgltdt,
                           input icsw.leadtmavg,
                           input icsd.enddaycut,
                           input-output v-shipdt).
    assign hold-linedt = v-shipdt.
    {w-icsp.i v-prod no-lock}
    if avail icsp then
      assign v-prodcat = if v-prodcat = " " then
                           icsp.prodcat
                         else
                           v-prodcat.
  end. /* if avail icsw */
  else
     assign v-foundfl = no.
end. /* procedure Get-Whse-Info */



/* ------------------------------------------------------------------------- */
Procedure Setup-Options:
/* ------------------------------------------------------------------------- */
  assign v-prodlutype = g-prodlutype
         v-custlutype = g-custlutype.

  find first OptionMoves no-lock no-error.
  if not avail OptionMoves then 
    do:
    create OptionMoves.
    assign OptionMoves.OptionIndex  = 2
           OptionMoves.Procedure    = "m1"
           OptionMoves.Procname     = "Middle 1".
    create OptionMoves.
    assign OptionMoves.OptionIndex  = 1
           OptionMoves.Procedure    = "t1"
           OptionMoves.Procname     = "Top 1".
    create OptionMoves.
    assign OptionMoves.OptionIndex  = 5
           OptionMoves.Procedure    = "d1"
           OptionMoves.Procname     = "dummy".
  end.

assign v-linebrowseopen = false.  

if v-jumpfl = yes then  
  assign v-framestate = "m1"
         v-CurrentKeyPlace = 2.
else
  assign v-CurrentKeyPlace = 0.
run FindNextFrame (input v-NormalMove,   /* Don't want lastkey to effect */
                   input-output v-CurrentKeyPlace ,
                   input-output v-framestate).

if v-framestate = "" then
  assign v-framestate = "t1"
         v-CurrentKeyPlace = 1.
  
assign v-takenby    = g-operinit
       v-lmode      = "a"  /* "a" for add mode on line "c" for change */
       v-qmode      = "a".
       
v-custno:dcolor in frame f-top1 = 0.
on cursor-up back-tab.
on cursor-down tab.

end. /* Procedure Setup-Options */


/* ------------------------------------------------------------------------- */
Procedure Clear-Frames:
/* ------------------------------------------------------------------------- */
  hide frame f-del no-pause.       
  hide frame f-top1 no-pause.      
  hide frame f-bot2i no-pause.      
  hide frame f-bot2e no-pause.
  hide frame f-statusline no-pause.
  hide frame f-Shiptolu no-pause.  
  hide frame f-zcustinfo no-pause. 
  hide frame f-prodlu no-pause.    
  hide frame f-lines no-pause.     
  hide frame f-custlu no-pause.    
  hide frame f-mid1 no-pause.
  hide frame f-extend no-pause.
  hide frame f-cnv no-pause.
  hide frame f-statusline1 no-pause.
  hide frame f-statusline2 no-pause.
  hide frame f-statuslinex no-pause.
  hide frame f-refresh no-pause.
  hide frame f-new-refresh no-pause.
  hide message no-pause.
end. /* Procedure Clear-Frames */


/* ------------------------------------------------------------------------- */ 
Procedure Initialize-All: 
/* ------------------------------------------------------------------------- */
  
  /* das - deadly embrace logic */
  find oeehb where 
       oeehb.cono = g-cono and  
       oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
       oeehb.seqno = 2 and
       oeehb.sourcepros = "ComQu"
       no-error.          
  if avail oeehb then
    do:
    assign oeehb.inprocessfl = no
           oeehb.user2       = " ".
    release oeehb.
  end.
  
  assign 
     /*g-orderno     = 0
       g-ordersuf    = 0
       g-custno      = 0
       g-shipto      = ""
       g-whse        = ""*/
       
       g-shipto      = ""            /* das - 061909 */
       v-mode        = ""
       w-batchnm     = 0
       v-quoteno     = 0
       v-notefl      = " "
       v-custno      = 0
       v-hcustlu     = " "
       v-hcustno     = 0
       v-arnotefl    = " "
       v-custname    = " "
   /*  v-takenby   */
       v-compfocus   = " "
       v-custlu      = ""
       v-shipto      = ""
       v-contact     = ""
       v-phone       = ""
       v-email       = ""
       v-lphone      = ""
       v-custpo      = ""
       v-canceldt    = ?
       v-currency    = ""
       v-terms       = ""
       /*v-quotetot    = 0*/
       v-quotemgn%   = 0
       v-quotetot    = 0
       v-commtot     = 0
       v-Hcommpct    = 0
       
       v-specnstype  = " "
       v-prod        = ""
       v-reqprod     = ""
       v-icnotefl    = ""       /**das**/
       v-qty         = 1
       /*v-qtyrem      = 0*/
       v-whse        = ""
       v-sellprc     = 0
       v-matrixed    = " "
       v-shipdt      = ?
       v-gp          = 0
       v-descrip     = ""
       v-descrip2    = ""
       v-xprod       = ""
       v-listprc     = 0
       v-listfl      = "n"
       v-prodcost    = 0
       v-glcost      = 0
       h-prodcost    = 0
       v-stndcost    = 0
       v-disc        = 0
       v-vendno     = 0  
       v-prodcat    = " "
       v-lcomment    = " "
       v-sparebamt  = 0
       v-totcost     = 0
       v-totmarg     = 0
       v-margpct     = 0
       v-pdrecord    = 0
       v-pdtype      = 0
       v-pdamt       = 0
       v-pd$md       = ""
       v-pdbased     = " "
       v-totnet      = 0
       v-ptype       = " "
       v-prodline    = " "
       v-quoteloadfl = no
       v-comments    = " "
       v-shiptonm    = " "
       v-shiptoaddr1 = " "
       v-shiptoaddr2 = " "
       v-shiptocity  = " "
       v-shiptost    = " "
       v-shiptozip   = " "
       v-shiptogeocd = 0
       v-rarrnotesfl = no
       v-5603fl      = no
       v-statustype  = " "
       /*v-slsrepout   = " "*/
       F11-proc      = " "
       cancel-xref   = no
       newprod       = no
       v-xreffl      = yes
       v-xrefty      = ""
       v-loadbrowseline  = no
       v-linebrowsekeyed = no
       v-costoverfl  = no
       v-delmode     = yes
       v-delkmode    = no
       v-lostbusty   = " "
       v-rushfl      = no
       s-mprice      = 0
       s-mcostplus   = 0
       s-mmargin     = 0
       s-mdiscamt    = 0
       s-mdiscpct    = 0
       s-discfl      = no
       h-whse        = ""
       zsdiconfirm   = no
       v-ctrlaovr    = no
       v-hmode       = "a"
       s-mprice      = 0  
       s-mcostplus   = 0  
       s-mmargin     = 0  
       s-mdiscamt    = 0  
       s-mdiscpct    = 0  
       s-discfl      = no
       apr-error     = no
       cataddfl      = "  "
       manual-change = no
       v-slsrep1     = " "
       v-slsrep2     = " "
       v-slsrep3     = " "
       v-company1    = " "
       v-company2    = " "
       v-slsrep1%    = 0
       v-slsrep2%    = 0
       v-slsrep3%    = 0
       v-company1%   = 0
       v-company2%   = 0.
       
  for each t-lines:
    delete t-lines.
  end.
end. /* procedure Initialize-All */

/* ------------------------------------------------------------------------- */ 
Procedure Initialize-Line: 
/* ------------------------------------------------------------------------- */
for each zidc where 
         zidc.cono      = g-cono and
         zidc.docnumber = int(v-custno) and
         zidc.docdesc   = v-prod and
         zidc.docttype  = "Puffing" and
         zidc.docuser1 <> " " and
         zidc.docdate   = today:
  delete zidc.
end.
          
assign v-lineno      = /* %toms% */ if not v-conversion then
                                      0
                                    else
                                      v-lineno 
       v-specnstype  = " "
       v-prod        = ""
       v-reqprod     = ""
       v-icnotefl    = ""
       v-qty         = 1
       /*v-qtyrem      = 0*/
       v-sellprc     = 0
       h-sellprc     = 0
       auth-sellprc  = 0
       manual-change = no
       v-matrixed    = " "
       v-shipdt      = ?
       v-gp          = 0
       v-descrip     = " "
       v-descrip2    = " "
       v-xprod       = " "
       v-listprc     = 0
       v-listfl      = "n"
       v-prodcost    = 0
       h-prodcost    = 0
       v-glcost      = 0
       v-stndcost    = 0
       v-vendno      = 0
       v-disc        = 0
       v-prodcat     = " "
       v-lcomment    = " "
       v-sparebamt   = 0
       v-totcost     = 0
       v-totmarg     = 0
       v-margpct     = 0
       v-pdrecord    = 0
       v-pdtype      = 0
       v-pdamt       = 0
       v-pd$md       = ""
       v-pdbased     = " "
       v-totnet      = 0
       v-ptype       = " "
       v-prodline    = " "
       v-leadtm      = 0
       v-rarrnotesfl = no
       v-5603fl      = no
       v-statustype  = " "
       cancel-xref   = no
       newprod       = no
       v-xreffl      = yes
       v-xrefty      = ""
       v-costoverfl  = no
       v-delmode     = yes
       v-delkmode    = no
       v-lostbusty   = " "
       v-surcharge   = 0
       h-surcharge   = 0
       v-chgsrchgfl  = no
       v-taxablefl   = no
       v-slsrepovr   = no
       s-mprice      = 0  
       s-mcostplus   = 0  
       s-mmargin     = 0  
       s-mdiscamt    = 0  
       s-mdiscpct    = 0  
       s-discfl      = no
       zsdiconfirm   = no
       v-ctrlaovr    = no
       ns-descrip    = ""  
       ns-descrip2   = ""
       ns-vendno     =  0  
       ns-prodline   = ""  
       ns-prodcat    = ""  
       ns-listprc    =  0  
       ns-listfl     = "n" 
       ns-prodcost   =  0  
       ns-leadtm     = 30  
       ns-ptype      = ""  
       orig-whse     = ""
       orig-cost     = 0
       over-cost     = 0
       v-slsrepout   = ""
       v-slsrepin    = ""
       v-rushfl      = no
       s-mprice      = 0  
       s-mcostplus   = 0  
       s-mmargin     = 0  
       s-mdiscamt    = 0  
       s-mdiscpct    = 0  
       s-discfl      = no
       apr-error     = no
       h-rebfl       = " "
       cataddfl      = "  ".

end. /* procedure Initialize-Line */


/* ------------------------------------------------------------------------- */ 
Procedure XRef_Check: 
/* ------------------------------------------------------------------------- */
  /* Check for Customer Part Number, Supercedes and Substitutes */
  def input        parameter ix-whse  like icsw.whse                  no-undo.
  def input        parameter ix-seqno like oeel.lineno                no-undo.
  def input-output parameter ix-qty   like icsw.qtyonhand             no-undo.
  def input-output parameter ix-prod  like icsw.prod                  no-undo.
  /**
  {w-icsp.i ix-prod no-lock}
  if avail icsp and icsp.kittype = "B" then return.
  
  {w-icsw.i ix-prod ix-whse no-lock}

  if avail icsw then
    assign v-avail = (icsw.qtyonhand - icsw.qtyreservd - icsw.qtycommit)
           v-hprod = ix-prod.
  else
    assign v-avail = 0
           v-hprod = ix-prod.
  **/
  find icsp where icsp.cono = g-cono and
                  icsp.prod = ix-prod
                  no-lock no-error.
  assign xtype = "c"
         v-hprod = ix-prod.
         /*v-xreffl = no.*/
  run Check_product_ID(input xtype,
                       input-output ix-prod,
                       input-output v-hprod).
   
  if ix-prod <> v-hprod then
    do:

    assign v-reqprod = v-hprod
           v-xprod   = v-hprod
           v-xrefty  = xtype
           ix-qty = v-cqty
           v-qty = v-cqty.
    {w-icsp.i ix-prod no-lock}
    {w-icsw.i ix-prod ix-whse no-lock}
    if avail icsw then
      assign v-avail = (icsw.qtyonhand - icsw.qtyreservd - icsw.qtycommit).
  end.
  if ix-qty > v-avail and cancel-xref = no and v-xreffl = yes then
    do:
    find first icsec where icsec.cono = g-cono and
               icsec.rectype = "p" and
               icsec.prod = v-prod
               no-lock no-error.
    if avail icsec then
      do:
      if v-avail > 0 then
        do:
        xrefmode:
        do on endkey undo xrefmode, leave xrefmode:
          update v-xreffl label
          "Product Superseded; Do You Want to Choose From Replacement Products"
          with frame f-xref side-labels row 10 centered overlay.
      
          if v-xreffl = yes then 
            do:
            hide frame f-xref no-pause.
            assign xtype = "p"
                   v-hprod = ix-prod.
            run Check_product_ID(input xtype,
                                 input-output ix-prod,
                                 input-output v-hprod).
            if v-hprod <> ix-prod then
              do:
              {w-icsp.i ix-prod no-lock}
              if avail icsp and icsp.statustype <> "I" then
                do:
                  assign v-prod = ix-prod
                         /*ix-qty = 1*/
                         ix-qty = if v-cqty <> 0 then v-cqty else
                         if icsp.sellmult > 0 then
                         icsp.sellmult * (truncate(ix-qty / icsp.sellmult,0) +
                           if (ix-qty / icsp.sellmult) -
                            truncate(ix-qty / icsp.sellmult,0) > 0
                            then 1
                          else 0)
                          else ix-qty
                         v-reqprod = v-hprod
                         v-xrefty  = xtype.
                {w-icsw.i ix-prod ix-whse no-lock}
                if avail icsw then
                  assign v-avail = 
                          (icsw.qtyonhand - icsw.qtyreservd - icsw.qtycommit)
                         v-hprod = ix-prod
                         v-reqprod = v-hprod
                         v-xrefty  = xtype.
                else
                  assign v-avail = 0
                         v-hprod = ix-prod
                         v-reqprod = v-hprod
                         v-xrefty  = xtype.
                if ix-seqno > 0 then
                  display v-prod with frame f-midk.
                else
                  display v-prod with frame f-mid1.
                message "Cross Reference Product Selected For Use".
                pause.
              end. /* avail icsp and not inactive */
              else
                assign ix-prod = v-hprod.
            end. /* cross reference found */
      
            if ix-qty > v-avail then
              do:                   
              assign xtype = "s"
                     v-hprod = ix-prod.
              run Check_product_ID(input xtype,
                                   input-output ix-prod,
                                   input-output v-hprod).
              if v-hprod <> ix-prod then
                do:
                {w-icsp.i ix-prod no-lock}
                if avail icsp then
                  assign /*ix-qty = 1*/
                         ix-qty = if v-cqty <> 0 then v-cqty else
                          if icsp.sellmult > 0 then
                          icsp.sellmult * (truncate(ix-qty / icsp.sellmult,0) +
                          if (ix-qty / icsp.sellmult) -
                           truncate(ix-qty / icsp.sellmult,0) > 0
                           then 1
                          else 0)
                          else ix-qty
                         v-reqprod = v-hprod
                         v-xrefty  = xtype.
              end.
            end.
          end. /* v-xreffl = yes */
        end. /* xrefmode */
      end. /* v-avail 0 - don't display pop-up if qtyavail is <= 0 */
      else
        do:
        assign xtype   = "p"
               v-hprod = ix-prod.
        run Check_product_ID(input xtype,
                             input-output ix-prod,
                             input-output v-hprod).
        if v-hprod <> ix-prod then
          do:
          {w-icsp.i ix-prod no-lock}
          if avail icsp and icsp.statustype <> "I" then
            do:
            assign v-prod = ix-prod
                   /*ix-qty = 1*/   
                   ix-qty = if v-cqty <> 0 then v-cqty else
                       if icsp.sellmult > 0 then
                       icsp.sellmult * (truncate(ix-qty / icsp.sellmult,0) +
                       if (ix-qty / icsp.sellmult) -
                        truncate(ix-qty / icsp.sellmult,0) > 0
                        then 1
                       else 0)
                       else ix-qty
                   v-reqprod = v-hprod
                   v-xrefty  = xtype.

            {w-icsw.i ix-prod ix-whse no-lock}

            if avail icsw then
              assign v-avail = 
                          (icsw.qtyonhand - icsw.qtyreservd - icsw.qtycommit)
                     v-hprod = ix-prod.
            else
              assign v-avail = 0
                     v-hprod = ix-prod.
            if ix-seqno > 0 then
              display v-prod with frame f-midk.
            else
              display v-prod with frame f-mid1.
            message "Cross Reference Product Selected For Use".
            pause.
          end. /* if avail icsp and not inactive */
          else
            assign ix-prod = v-hprod.
        end. /* cross reference found */
        if ix-qty > v-avail then
          do:                   
          assign xtype = "s"
                 v-hprod = ix-prod.
          run Check_product_ID(input xtype,
                               input-output ix-prod,
                               input-output v-hprod).
          if v-hprod <> ix-prod then
            do:
            {w-icsp.i ix-prod no-lock}
            if avail icsp then
               assign /*ix-qty = 1*/
                      ix-qty = if v-cqty <> 0 then v-cqty else
                         if icsp.sellmult > 0 then
                         icsp.sellmult * (truncate(ix-qty / icsp.sellmult,0) +
                         if (ix-qty / icsp.sellmult) -
                          truncate(ix-qty / icsp.sellmult,0) > 0
                          then 1
                         else 0)
                         else ix-qty
                      v-reqprod = v-hprod
                      v-xrefty  = xtype.
          end.
        end.
      end. /* qtyavail is = 0 */
    end. /* Supersede exists */
  end. /* qty > available */

  if ix-qty > v-avail and cancel-xref = no then
    do:
    assign xtype = "s"
           v-hprod = ix-prod.
           /*v-xreffl = yes.*/
    run Check_product_ID(input xtype,
                         input-output ix-prod,
                         input-output v-hprod).
    if v-hprod <> ix-prod then
      do:
      if avail icsp then
        assign /*ix-qty = 1*/
               ix-qty = if v-cqty <> 0 then v-cqty
                          else if icsp.sellmult > 0 then
                          icsp.sellmult * (truncate(ix-qty / icsp.sellmult,0) +
                          if (ix-qty / icsp.sellmult) -
                            truncate(ix-qty / icsp.sellmult,0) > 0
                            then 1
                          else 0)
                          else ix-qty
               v-reqprod = v-hprod
               v-xrefty  = xtype.
    end.
  end.
  if v-hprod = ix-prod and not avail icsp then
    do:
    assign xtype = "m"
           v-hprod = ix-prod.
    run Check_product_ID(input xtype,
                         input-output ix-prod,
                         input-output v-hprod).
    if v-hprod <> ix-prod then
      do:
      message "Crossed By: Vendor Product".
      assign v-reqprod = v-hprod
             v-xrefty  = xtype.
    end.
  end.
  if v-hprod = ix-prod and not avail icsp then
    do:
    assign xtype = "g"
           v-hprod = ix-prod.
    run Check_product_ID(input xtype,
                         input-output ix-prod,
                         input-output v-hprod).
    assign v-xrefty = xtype
           xtype    = " ".
    
    /****
    if v-hprod <> ix-prod then
      do:
      message "Crossed By: Vendor Product".
      assign v-reqprod = v-hprod
             v-xrefty  = xtype.
    end.
    ****/
  end.

  hide frame f-xref no-pause.
  
end. /* procedure XRef_Check */

/* ------------------------------------------------------------------------- */ 
Procedure XRef_Check_for_Options:
/* ------------------------------------------------------------------------- */
  /* Check for Options */
  def input        parameter ix-whse  like icsw.whse                  no-undo.
  def input        parameter ix-seqno like oeel.lineno                no-undo.
  def input-output parameter ix-qty   like icsw.qtyonhand             no-undo.
  def input-output parameter ix-prod  like icsw.prod                  no-undo.
  run Initialize-Line.
  assign xtype = "o"
         v-hprod = ix-prod
         g-ourproc = "oeetl"
         /*v-xreffl = yes.*/
         x-prod = ""
         x-qty  = 0
         x-unit = ""
         v-optprod = ix-prod.
  run oeexqo.p.
  find first b-oeehb where b-oeehb.cono = g-cono and
             b-oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
             b-oeehb.seqno   = 2 and
             b-oeehb.sourcepros = "ComQu"
             no-lock no-error.
  if avail b-oeehb then
    do:
    find last oeelb where oeelb.cono = g-cono and
                          oeelb.batchnm = b-oeehb.batchnm and
                          oeelb.seqno   = b-oeehb.seqno
                          no-lock no-error.
    if avail oeelb then
      assign v-lineno    = oeelb.lineno + 1
             v-slsrepin  = oeelb.slsrepin
             v-slsrepout = oeelb.slsrepout.
    do p-inx = 1 to 7:
      if x-prod[p-inx] <> "" then
        do:
        assign v-prod = x-prod[p-inx]
               v-qty  = x-qty[p-inx]
               v-unit = x-unit[p-inx]
               zi-displaylinefl = false.
        if v-rebatefl <> " " and h-rebfl = " " then
          assign h-rebfl = v-rebatefl.
        if v-shipdt = ? then
          do:
          /* das- did not have icsd when v-whse was changed on the line */
          if not avail icsd then  
            find icsd where icsd.cono = g-cono and
                            icsd.whse = v-whse
                            no-lock no-error.
          run Assign_LineDate (input v-lmode,
                             /*  input repricefl,*/
                               input v-whse,
                               input x-qtyavail,
                               input v-qty,
                               input icsw.prod,
                               input icsw.statustype,
                               input icsw.qtyreserv,
                               input icsw.qtycommit,
                               input icsw.avgltdt,
                               input icsw.leadtmavg,
                               input icsd.enddaycut,
                               input-output v-shipdt).
        end. /* if v-shipdt = ? */
        find t-lines where t-lines.lineno = v-lineno      and
                           t-lines.seqno  = 0 no-error.
        if not avail t-lines then
          do:
          if v-rebatefl = " " and h-rebfl <> " " then
            assign v-rebatefl = h-rebfl.
          create t-lines.
          assign  t-lines.lineno     = v-lineno
                  t-lines.seqno      = 0
                  t-lines.prod       = v-prod
                  t-lines.icnotefl   = v-icnotefl
                  t-lines.descrip    = v-descrip
                  t-lines.descrip2   = v-descrip2
                  t-lines.xprod      = v-xprod
                  t-lines.whse       = v-whse
                  t-lines.vendno     = v-vendno
                  t-lines.slsrepout  = v-slsrepout
                  t-lines.slsrepin   = v-slsrepin
                  t-lines.prodcat    = v-prodcat
                  t-lines.prodline   = v-prodline
                  t-lines.listprc    = v-listprc
                  t-lines.listfl     = v-listfl
/* %toms2% */                                                              
                t-lines.convertfl  = if v-conversion then t-lines.convertfl 
                                     else no
/* %toms2% */
                t-lines.pricetype  = if v-ptype <> " " then v-ptype
                                     else t-lines.pricetype
                t-lines.sellprc    = v-sellprc
                t-lines.Lcommpct   = v-Lcommpct
                t-lines.prodcost   = v-prodcost
                t-lines.glcost     = v-glcost
                t-lines.surcharge  = v-surcharge
                t-lines.sparebamt  = v-sparebamt
                t-lines.leadtm     = v-leadtm
                t-lines.gp         = if v-gp = ? then 0 else v-gp
                t-lines.qty        = v-qty
                t-lines.shipdt     = v-shipdt
                t-lines.specnstype = v-specnstype
                t-lines.pdamt      = v-pdamt
                t-lines.pd$md      = v-pd$md
                t-lines.pdbased    = v-pdbased
                t-lines.totnet     = v-sellprc * v-qty
                t-lines.lcomment   = v-lcomment
                t-lines.totcost    = v-prodcost * v-qty
                t-lines.rebatefl   = if v-rebatefl <> " " then v-rebatefl
                                     else t-lines.rebatefl
                t-lines.totmarg    = (v-sellprc - v-prodcost) * v-qty
                t-lines.margpct    = if v-sellprc <> 0 then
                                   ((v-sellprc - v-prodcost) / v-sellprc) * 100
                                     else 0.
        end. /* not avail t-lines */
        create oeelb.    
        run Create_OEELB.
      end. /* if x-prod[p-inx] <> "" */
    end. /* do p-inx = 1 to 7 */
    run Quote-Total.
  end. /* if avail b-oeehb */
end. /* Procedure XRef_Check_for_Options */


/* ------------------------------------------------------------------------- */ 
Procedure Line_Browse: 
/* ------------------------------------------------------------------------- */
  find first t-lines no-lock no-error.
  if avail t-lines then
    do:
    v-rowid = rowid(t-lines). 
    open query q-lines for each t-lines.
    assign v-linebrowseopen = true.
    display b-lines with frame f-lines.
    b-lines:refreshable = false.
       
    if not v-conversion then
      do:
      enable b-lines with frame f-lines.
      apply "end" to b-lines in frame f-lines.
      b-lines:select-row (b-lines:num-iterations).
    end.
    
    b-lines:refreshable = true.
    b-lines:refresh(). 
  end.
  else
    open query q-lines for each t-lines.
end. /* procedure Line_Browse */


/* ------------------------------------------------------------------------- */ 
Procedure Create_OEELB: 
/* ------------------------------------------------------------------------- */
  if avail oeehb then 
    do:
    find arsc where arsc.cono = oeehb.cono and
                    arsc.custno = oeehb.custno no-lock no-error. 
  
    if oeehb.shipto <> "" then
      find arss where arss.cono = oeehb.cono and
                      arss.custno = oeehb.custno and
                      arss.shipto  = oeehb.shipto no-lock no-error. 
  end.
  
  run Get_Tax_Flag (input-output v-taxablefl,
                    input-output v-nontaxtype).

  assign oeelb.cono        = g-cono
         oeelb.user7       = oeehb.custno 
         oeelb.batchnm     = oeehb.batchnm
         oeelb.seqno       = oeehb.seqno
         oeelb.enterdt     = TODAY
         oeelb.reqshipdt   = if v-shipdt <> ? then v-shipdt
                               else oeehb.reqshipdt
         oeelb.promisedt   = if v-shipdt <> ? then v-shipdt
                                else oeehb.reqshipdt
         oeelb.lineno      = integer(v-lineno)
         oeelb.shipprod    = v-prod
         oeelb.proddesc    = if avail icsp then icsp.descrip[1]
                             else 
                                if v-descrip <> "" then v-descrip
                                else ""
         oeelb.proddesc2   = if avail icsp and icsp.descrip[2] <> ""
                               then icsp.descrip[2]
                             else
                               if v-descrip2 <> "" then v-descrip2
                               else oeelb.proddesc2
         oeelb.price       = v-sellprc
         oeelb.datccost    = 0
         oeelb.priceoverfl = if v-sellprc <> orig-price then yes else no
         oeelb.pricetype   = if v-specnstype = "n" then v-ptype
                             else 
                                if avail icsw then icsw.pricetype
                                else ""
         oeelb.specnstype  = if v-specnstype = "" then 
                               " "
                             else
                               v-specnstype
         oeelb.usagefl     = no
         oeelb.rushfl      = no
         oeelb.costoverfl  = no
         oeelb.termspct    = v-Lcommpct
         oeelb.qtyord      = dec(v-qty)
         oeelb.reqprod     = v-reqprod
         oeelb.xrefprodty  = if v-xrefty <> "" then 
                               v-xrefty 
                             else if v-reqprod <> "" then "c"
                             else oeelb.xrefprodty
         oeelb.unit        = "EACH"
         oeelb.leadtm      = v-leadtm
         oeelb.commentfl   = if can-find
                         (first com use-index k-com where
                         com.cono     = g-cono         and
                         com.comtype  = "ob" + string(w-batchnm,">>>>>>>9") and
                         com.lineno   = v-lineno
                         no-lock)
                         then yes else no
         oeelb.icspecrecno = if zi-icspecrecno = 0 then 1 else zi-icspecrecno
         oeelb.prodcat     = if v-prodcat <> "" then v-prodcat
                             else if avail icsp then icsp.prodcat
                             else " "
         oeelb.prodline    = if avail icsw then icsw.prodline
                             else v-prodline
         oeelb.taxgroup    = if avail icsw then icsw.taxgroup else 1
         oeelb.gststatus   = if avail icsw then icsw.gststatus else true
         oeelb.netamt      = v-totnet
         oeelb.ordertype   = " ".
  assign oeelb.slsrepin    = if v-slsrepin = "" then
                               if avail arss and v-shipto <> ""
                                 then arss.slsrepin
                               else if avail arsc then arsc.slsrepin
                                    else v-slsrepin
                             else
                               v-slsrepin
         oeelb.slsrepout   = v-slsrepout
         oeelb.nontaxtype  = v-nontaxtype.

  assign  oeelb.usagefl = no.
  assign  oeelb.botype       = "n"
          oeelb.vendno       = if avail icsw then icsw.arpvendno
                               else if v-vendno <> 0 then v-vendno
                               else 0
          oeelb.arpvendno    = if oeelb.specnstype <> " " then oeelb.vendno
                               else oeelb.arpvendno
          oeelb.taxablefl    = no
          oeelb.printpricefl = yes
          oeelb.subtotalfl   = no
          oeelb.xxc3         = /*oeehb.whse.*/ string(oeelb.arpvendno).
  
  /** since company commissions must be taken into consideration, prodcost is
      caclulated at the end 

  assign  oeelb.prodcost     = if oeelb.qtyord <> 0 then
                                 (oeelb.netamt * (1 - (.01 * oeelb.termspct)) /
                                                             oeelb.qtyord)
                               else 0.
  **/

  overlay(oeelb.user4,1,4)   = oeehb.whse.
  overlay(oeelb.user4,5,7)   = string(v-gp,">>9.99-").
  overlay(oeelb.user4,12,13) = string(v-listprc,">>>>>>9.99999").
  overlay(oeelb.user4,25,1)  = v-listfl.
  overlay(oeelb.user4,26,8)  = if v-shipdt = ? then "?       "
                               else string(v-shipdt,"99/99/99").
  overlay(oeelb.user4,34,7)  = string(v-disc,">>9.99-").
  /*assign  v-totcost          = oeelb.prodcost * oeelb.qtyord.*/
  if oeelb.specnstype = "n" then
    do:
    if oeelb.reqprod = " " then
      assign oeelb.xrefprodty = " ".
    else
      assign oeelb.xrefprodty = "c".
  end.
  assign commission-amt = 0.
  for each bpel where bpel.cono   = g-cono and
                      bpel.bpid   = oeelb.batchnm + "C" and
                      bpel.lineno = oeelb.lineno    /* 071015 */
                      no-lock:
    if bpel.revno = 1 and bpel.itemid <> " " then
      assign oeelb.xxde1 = dec(bpel.commtype).
    if bpel.revno = 2 and bpel.itemid <> " " then
      assign oeelb.xxde2 = dec(bpel.commtype).
    if bpel.revno = 3 and bpel.itemid <> " " then
      assign oeelb.xxde3 = dec(bpel.commtype).
    if bpel.revno <= 3 then
      assign commission-amt = commission-amt + bpel.awardprice.
  end. /* each pbel */
  assign oeelb.prodcost = (oeelb.netamt - commission-amt) / oeelb.qtyord.
  assign v-totcost      = oeelb.prodcost * oeelb.qtyord.

  {t-all.i "oeelb"}
  /* fix misc issues with xrefprodty */
  if (oeelb.xrefprodty = " " or oeelb.xrefprodty = "" 
                             or oeelb.xrefprodty = "g") and 
    oeelb.reqprod <> " " then
    assign oeelb.xrefprodty = "c".
                                        
  for each bpeh where
           bpeh.cono  = g-cono and
           bpeh.bpid  = string(v-quoteno,">>>>>>>9") + "C" 
           no-lock:
    find bpel where bpel.cono = g-cono and
                    bpel.bpid = bpeh.bpid and
                    bpel.revno = bpeh.revno and
                    bpel.lineno = oeelb.lineno
                    no-error.
    if avail bpel then
      do:
      assign
           bpel.price      = if bpel.itemid <> " " then oeelb.price else 0
           bpel.extprice   = if bpel.itemid <> " " then 
                               (bpel.price * bpel.qtyord)
                             else 0
           bpel.seqid      = bpeh.slsrepout
           bpel.commtype   = if bpel.itemid <> " " then bpeh.commtype
                             else "0.0"
           bpel.termspct   = if bpel.itemid <> " " then v-Lcommpct else 0.
      assign
           bpel.awardprice = if bpel.revno <= 3 then
                               ((bpel.extprice * (.01 * bpel.termspct)) *
                                               (.01 * int(bpel.commtype))) /* *
                                               if oeehb.xxde1 > 0 then 
                                                 (1 - (.01 * oeehb.xxde1))
                                               else
                                                 1                         */
                             else
                               (bpel.extprice * (.01 * bpel.termspct)) *
                                                 (.01 * int(bpel.commtype))
           bpel.transdt    = TODAY
           bpel.transtm    = substr(string(TIME,"HH:MM"),1,2) +
                             substr(string(TIME,"HH:MM"),4,2)
           bpel.operinit   = g-operinit.
    end.
    else
      do:
      create bpel.
      assign bpel.cono       = bpeh.cono
             bpel.bpid       = bpeh.bpid
             bpel.revno      = bpeh.revno
             bpel.lineno     = oeelb.lineno
             bpel.itemid     = bpeh.slsrepout
             bpel.prod       = oeelb.shipprod
             bpel.qtyord     = oeelb.qtyord
             bpel.unit       = "EACH".
      assign bpel.price      = if bpel.itemid <> " " then oeelb.price else 0
             bpel.extprice   = bpel.price * bpel.qtyord
             bpel.seqid      = bpeh.slsrepout
             bpel.commtype   = if bpel.itemid <> " " then bpeh.commtype
                               else "0.0"
             bpel.termspct   = if bpel.itemid <> " " then v-Lcommpct else 0.
        assign
             bpel.awardprice = if bpel.revno <= 3 then
                                ((bpel.extprice * (.01 * bpel.termspct)) *
                                               (.01 * int(bpel.commtype))) /* *
                                               if oeehb.xxde1 > 0 then 
                                                 (1 - (.01 * oeehb.xxde1))
                                               else
                                                 1                         */
                               else
                                 (bpel.extprice * (.01 * bpel.termspct)) *
                                                   (.01 * int(bpel.commtype))
             bpel.transdt    = TODAY
             bpel.transtm    = substr(string(TIME,"HH:MM"),1,2) +
                               substr(string(TIME,"HH:MM"),4,2)
             bpel.operinit   = g-operinit.

    end. /* create */
  end. /* each bpeh */
  
    
end. /* Procedure Create_OEELB */


/* ------------------------------------------------------------------------- */ 
Procedure notes-create: 
/* ------------------------------------------------------------------------- */

  assign n-inx = 1     /* noteln index    */
         c-inx = 1     /* v-comment index */
         p-inx = 1     /* page index      */
         n-IE  = if v-internalfl = yes then 
                   "I"
                 else 
                   "E".

  if v-quoteno > 0 then do:
  find notes where notes.cono = g-cono and
                   notes.notestype = "ba" and
                   notes.primarykey   = string(v-quoteno,">>>>>>>9") and
                   notes.secondarykey = n-IE + "c" and
                   notes.pageno = p-inx
                   no-error.
  if not avail notes then 
    do:
    create notes.
    assign notes.cono = g-cono
           notes.notestype = "ba"
           notes.primarykey   = string(v-quoteno,">>>>>>>9")
           notes.secondarykey = n-IE + "c"
           notes.pageno = p-inx
           notes.printfl  = if n-IE = "I" then no else v-allfl
           notes.printfl2 = if n-IE = "I" then no else v-ackfl
           notes.printfl3 = if n-IE = "I" then no else v-pckfl
           notes.printfl4 = if n-IE = "I" then no else v-advfl
           notes.printfl5 = if n-IE = "I" then no else v-invfl
           notes.operinit = g-operinits
           notes.transdt  = TODAY
           notes.transtm  = string(TIME,"HH:MM").
  end.

  if n-IE = "i" then
    h-chandle =  v-comments:handle in frame f-bot2i. 
  else  
    h-chandle = v-comments:handle in frame f-bot2e.

  assign v-cstartPos  = 1
         j-inx        = h-chandle:num-lines.
  do i-inx = 1 to j-inx:                          /* 99:  %TAH */
    if n-inx > 16 then 
      do:
      assign n-inx = 1
             p-inx = p-inx + 1.
      find notes where notes.cono = g-cono and
                       notes.notestype = "ba" and
                       notes.primarykey   = string(v-quoteno,">>>>>>>9") and
                       notes.secondarykey = n-IE + "c" and
                       notes.pageno = p-inx
                       no-error.
      if not avail notes then 
        do:
        create notes.
        assign notes.cono = g-cono
               notes.notestype = "ba"
               notes.primarykey   = string(v-quoteno,">>>>>>>9")
               notes.secondarykey = n-IE + "c"
               notes.pageno = p-inx
               notes.printfl  = if n-IE = "I" then no else v-allfl
               notes.printfl2 = if n-IE = "I" then no else v-ackfl
               notes.printfl3 = if n-IE = "I" then no else v-pckfl
               notes.printfl4 = if n-IE = "I" then no else v-advfl
               notes.printfl5 = if n-IE = "I" then no else v-invfl
               notes.operinit = g-operinits
               notes.transdt  = TODAY
               notes.transtm  = string(TIME,"HH:MM").
      end.
    end.
  
    if i-inx > j-inx then
      leave.                /* no more lines */
    else 
      do:
   /* Code to read each line how it sits in the EDITOR widget */
      assign v-coffset = if i-inx = j-inx then
                         h-chandle:length
                      else
                         h-chandle:convert-to-offset ( i-inx + 1, 1 )
           
             v-dummyfl = if i-inx = j-inx and j-inx = 1 then
                           h-chandle:set-selection (v-cstartPos, v-coffset )
                         else
                           h-chandle:set-selection (v-cstartPos, v-coffset - 1)
/* TAH 01/17/07                           
            v-dummyfl = h-chandle:set-selection ( v-cstartPos, v-coffset - 1 )
*/
           v-commentline = h-chandle:selection-text
           v-cstartPos = v-coffset
           v-dummyfl = h-chandle:clear-selection(). 
           /* Above line not necessary - hides selection bar */
      assign notes.noteln[n-inx] = replace(v-commentline,chr(10),"")
             c-inx = c-inx + 1
             n-inx = n-inx + 1. 
    end.

    if v-quoteno > 0 then
    run oeexqinqnote.p (input g-cono,
                   input string(v-quoteno,">>>>>>>9")).
  end.

  /* 
  OEEXQ is holding the notes record and locking other users out.  This
  FOR EACH transaction stops this from happening.
  */
  for each notes where notes.cono = g-cono and
                       notes.notestype = "ba" and
                       notes.primarykey   = string(v-quoteno,">>>>>>>9") and
                       notes.secondarykey = n-IE + "c" and
                       notes.pageno = p-inx:
  end.
  end. /* v-quoteno > 0 */
end. /* Procedure notes-create */

/************
/* ------------------------------------------------------------------------- */ 
Procedure Create_ICSW: 
/* ------------------------------------------------------------------------- */
  def input        parameter ix-whse  like icsw.whse                  no-undo.
  def input        parameter ix-prod  like icsw.prod                  no-undo.

  for each icsd where icsd.cono    = g-cono and 
                      icsd.salesfl = yes    and
                      icsd.custno  = 0
                      no-lock:
    find first t-icsw where t-icsw.cono        = g-cono  and
                            t-icsw.prod        = ix-prod and
                            t-icsw.statustype <> "X"
                            no-lock no-error.
    if avail t-icsw then
      do transaction:
      create icsw.
      buffer-copy t-icsw except t-icsw.cono
                                t-icsw.whse
                                t-icsw.qtyonhand
                                t-icsw.qtyreservd
                                t-icsw.qtycommit
                                t-icsw.qtybo
                                t-icsw.qtyintrans
                                t-icsw.qtyunavail
                                t-icsw.qtyonorder
                                t-icsw.qtyrcvd
                                t-icsw.qtyreqrcv
                                t-icsw.qtyreqshp
                                t-icsw.qtydemand
                                t-icsw.lastsodt
                                t-icsw.availsodt
                                t-icsw.nodaysso
                                t-icsw.leadtmprio
                                t-icsw.leadtmlast
                                t-icsw.lastltdt
                                t-icsw.priorltdt
                                t-icsw.lastinvdt
                                t-icsw.lastrcptdt
                                t-icsw.lastcntdt
                                t-icsw.lastpowtdt
                                t-icsw.rpt852dt
                                t-icsw.priceupddt
                                t-icsw.issueunytd
                                t-icsw.retinunytd
                                t-icsw.retouunytd
                                t-icsw.rcptunytd
                                t-icsw.qtydemand
                                t-icsw.enterdt
                                t-icsw.linept
                                t-icsw.orderpt
                                t-icsw.frozentype
                                t-icsw.safeallamt
                                t-icsw.safeallpct
                                t-icsw.usagerate
                                t-icsw.ordqtyin
                                t-icsw.ordqtyout
                                t-icsw.frozenmmyy
                                t-icsw.frozenmos
                                t-icsw.binloc1
                                t-icsw.binloc2
      to icsw
      assign icsw.cono       = g-cono
             icsw.whse       = ix-whse
             icsw.statustype = "O"
             icsw.leadtmavg  = 30
             icsw.frozenmmyy  = string(month(today),"99") +
                                substr(string(year(today),"9999"),3,2)
             icsw.frozenmos   = 6
             icsw.binloc1     =  "New Part"
             icsw.enterdt     = today
             icsw.frozentype  = "N".
                  
      create icswu.                       
      assign icswu.cono        = g-cono
             icswu.prod        = ix-prod
             icswu.whse        = ix-whse
             g-prod = ix-prod
             g-whse = ix-whse.
      {t-all.i icswu}
      release icswu.
      /* Create a Partial Product Record (Product on the Fly) */
      find icsl where icsl.cono = g-cono and
                      icsl.whse = t-icsw.whse and
                      icsl.vendno = t-icsw.arpvendno and
                      icsl.prodline = t-icsw.prodline
                      no-lock no-error.
      if avail icsl then
        do:
        find zidc use-index docinx4 where
             zidc.cono       = g-cono and
             zidc.docstatus  = 02     and
             zidc.docttype   = "OTF" and
             zidc.doctype    = icsl.buyer and
             substr(zidc.docpartner,1,4)  = icsw.whse and
             substr(zidc.docpartner,5,24) = icsw.prod
             no-lock no-error.
        if not avail zidc then
          do:
          find icsp where icsp.cono = g-cono and
                          icsp.prod = v-prod
                          no-lock no-error.
          if not avail icsp then leave.
          assign substr(w-docno,1,7) = string(w-batchnm,">>>>>>>9")
                 substr(w-docno,8,2) = "00".
          create zidc.
          assign zidc.cono       = g-cono
                 zidc.docnumber  = int(w-docno)
                 zidc.docdate    = TODAY
                 substr(zidc.docpartner,1,4)  = icsw.whse
                 substr(zidc.docpartner,5,24) = icsw.prod   
                 substr(zidc.docdesc,1,24) = icsp.descrip[1]
                 substr(zidc.docdesc,27,4) = g-operinit     
                 zidc.docstatus  = 02
                 zidc.docdisp    = ""                                
                 zidc.docstnbr   = 0                                 
                 zidc.docfadate  = if icsp.enterdt = TODAY and
                                      icsp.operinit = g-operinit then
                                                           TODAY
                                   else
                                      ?
                 zidc.doctype    = icsl.buyer
                 zidc.docgsnbr   = v-lineno
                 zidc.docttype   = "OTF"
                 substr(zidc.docuser1,1,6)  = icsl.prodline
                 substr(zidc.docuser1,7,24) = icsp.descrip[1]
                 substr(zidc.docuser2,1,15) = "              "
                 substr(zidc.docuser2,17,4) = icsp.prodcat
                 substr(zidc.docuser3,1,24) = icsw.prod
                 substr(zidc.docuser3,27,4) = icsw.pricetype
                 substr(zidc.docuser4,1,4)  = icsw.whse
                 substr(zidc.docuser4,6,12) = string(icsl.vendno)
                 zidc.docvendno  = icsw.replcost
                 zidc.docstnbr   = icsw.leadtmavg.
          find b-icsl where b-icsl.cono = g-cono and
                            b-icsl.whse = icsw.whse and
                            b-icsl.vendno = icsw.arpvendno and
                            b-icsl.prodline = icsw.prodline
                            no-lock no-error.
          if not avail b-icsl then
            assign zidc.docdisp = "n".  /* icsl is new or missing */
          else
            assign zidc.docdisp = "".
          release zidc.
        end. /* not avail zidc */
      end. /* avail icsl */
      leave.
    end. /* if avail t-icsw */
  end. /* for each icsd */

end. /* Procedure Create_ICSW */
************/

/* ------------------------------------------------------------------------- */ 
Procedure Assign_LineDate:
/* ------------------------------------------------------------------------- */
  def input        parameter  ix-lmode         as   character        no-undo.
/*def input        parameter  ix-repricefl     as   logical          no-undo.*/
  def input        parameter  ix-whse          like icsw.whse        no-undo.
  def input        parameter  ix-qtyavail      like icsw.qtyonhand   no-undo.
  def input        parameter  ix-qty           like icsw.qtyonhand   no-undo.
  def input        parameter  ix-prod          like icsw.prod        no-undo.
  def input        parameter  ix-statustype    like icsw.statustype  no-undo.
  def input        parameter  ix-qtyreserv     like icsw.qtyreserv   no-undo.
  def input        parameter  ix-qtycommit     like icsw.qtycommit   no-undo.
  def input        parameter  ix-avgltdt       like icsw.avgltdt     no-undo.
  def input        parameter  ix-leadtmavg     like icsw.leadtmavg   no-undo.
  def input        parameter  ix-enddaycut     like icsd.enddaycut   no-undo.
  def input-output parameter  ix-shipdt        like oeel.promisedt   no-undo.

  if not avail icsp then
    find icsp where icsp.cono = g-cono and
                    icsp.prod = ix-prod
                    no-lock no-error.
  if ix-shipdt = ? or (ix-shipdt <> ? and ix-whse <> orig-whse) then
    do:
    if ix-qtyavail = 0 and (ix-avgltdt < (TODAY - 183) or ix-avgltdt = ?) 
      then
      assign ix-shipdt = if ix-shipdt = ? then 09/09/99
                        else ix-shipdt.
    else
    if ix-leadtmavg > 0 then
      assign ix-shipdt = TODAY + ix-leadtmavg.
    else
      assign ix-shipdt = if avail icsp and icsp.kittype = "b" then 09/09/99
                         else 
                           if not avail icsp then 09/09/99
                           else TODAY + 30.
  end. 
  
  if ix-shipdt <> 09/09/99 and ix-lmode = "a" then
    do:
    assign ix-shipdt = 
    if ((int(substr(string(time,"hh:mm:ss"),1,2)) * 100) +
         int(substr(string(time,"hh:mm:ss"),4,2))) >
       ((truncate(truncate(ix-enddaycut / 60 ,0) / 60,0) * 100) +
        (truncate(ix-enddaycut / 60 ,0)) mod 60) then
      ix-shipdt + 1
    else
      ix-shipdt.
    run zsdiweekend (input-output ix-shipdt,
                     input ix-whse,
                     input g-cono).
  end.
end. /* Procedure Assign_LineDate */


/* ------------------------------------------------------------------------- */ 
Procedure Check_ARSS:
/* ------------------------------------------------------------------------- */
  def input        parameter  ix-custno        like arss.custno      no-undo.
  def input        parameter  ix-shipto        like arss.shipto      no-undo.
  def input-output parameter  ix-5966fl        as   logical          no-undo.
  
  assign ix-5966fl = no.
  find arss where arss.cono = g-cono and
                  arss.custno = v-custno and
                  arss.shipto = v-shipto
                  no-lock no-error.
  if avail arss and arss.statustype = no then
    assign ix-5966fl = yes.
  
end. /* Check_ARSS */

/* ------------------------------------------------------------------------- */
Procedure OTF_Routine:
/* ------------------------------------------------------------------------- */
  def input        parameter  v-prod          like icsw.prod        no-undo.
  def input        parameter  v-whse          like icsw.whse        no-undo. 
  def input        parameter  v-lineno        like oeel.lineno      no-undo.
  def input        parameter  x-prodtype      as   c format "x(4)"  no-undo.
  
  if v-whse = "" then
    do:
    run Find_Whse (input x-prodtype).
  end.
  assign otf-create = no
         otf-kit    = no.
  assign g-whse     = v-whse
         g-oelineno = v-lineno.
  if g-whse = "" then
    do:
    find oeehb where oeehb.cono = g-cono and
                     oeehb.batchnm = string(w-batchnm,">>>>>>>9") and 
                     oeehb.seqno = 2 and
                     oeehb.sourcepros = "ComQu"
                     no-lock no-error.
    if avail oeehb then
      assign g-whse = oeehb.whse.
  end.

  if ({v-icsw.i v-prod v-whse}) = false then 
    do:
    assign g-prod     = v-prod.
    if ({v-icsp.i v-prod}) = false then 
      do:
      assign g-secure = 4.
      run icsp.p.
      run icspupdt.p(g-prod,v-lineno).
      assign w-loadefaults = no.
    end.
    else
      assign w-loadefaults = yes.
    /* verify icsp was created */
    if ({v-icsp.i v-prod}) = true then
      do:
      assign otf-error = no.
      run otficsp.p(input v-prod,
                    output otf-error).
      if otf-error = yes then 
        do:
        run err.p(4735).
        next-prompt v-prod.
        next.
      end.
      assign otf-create = yes.
      assign w-currproc = g-currproc
             w-ourproc  = g-ourproc
             w-lineno   = g-oelineno
             g-secure = 4
             g-currproc = "icsw"
             g-oelineno = w-lineno.
      run icsw.p.
      assign g-oelineno = w-lineno
             g-currproc = w-currproc
             g-ourproc  = w-ourproc.

      run zsdimaincpy.p (input g-cono,
                         input "x",
                         input v-whse,
                         input v-prod,
                         input v-lineno,
                         input-output zsdi-confirm).           
    end. /* icsp as verified */
  end. /* icsw does not exist */
  run Find_Whse (input x-prodtype).

  display blankline1
          blankline2
          blankline3
          blankline4
          blankline5
          blankline6
          blankline7
          blankline8
          blankline9
          blankline10
          blankline11
          blankline12
          blankline13
          blankline14
          blankline15
          blankline16
          blankline17
  with frame f-blank.
end. /* Procedure OTF_Routine */

/* ------------------------------------------------------------------------- */
Procedure Tag_SASOO:
/* ------------------------------------------------------------------------- */
  def input        parameter w-batchnm    as i format ">>>>>>>9" no-undo.
  def input        parameter ix-location  as c format "x(7)"     no-undo.
  for each b-sasoo where b-sasoo.cono   = g-cono and
                         b-sasoo.oper2  = g-operinit:
    assign b-sasoo.user3 = if w-batchnm <> 0 then 
                             string(w-batchnm,">>>>>>>9")
                           else " ".
           b-sasoo.user2 = ix-location.
  end.
end. /* Procedure Tag_SASOO */


/* ------------------------------------------------------------------------- */
Procedure Handle_Currency:
/* ------------------------------------------------------------------------- */
  def input         parameter v-quoteno as i format ">>>>>>>9"      no-undo.
  def input         parameter v-currencytype as c format "x(2)"     no-undo.
  def input-output  parameter v-currencyty   as c format "x(1)"     no-undo.
  def var                     x-currencyty   as c format "x(1)"     no-undo.
  form 
    x-currencyty colon  29 label "(C)anadian or (U)S Currency?"
    with frame f-CNcurr overlay side-labels row 5 centered.
    
  if v-currencytype = "CN" then
    do:
    display x-currencyty with frame f-CNcurr.
    
    CNcurrency:
    do with frame f-CNcurr on endkey undo, leave CNcurrency:
      update  x-currencyty 
      with frame f-CNcurr
      editing:
        readkey.
      if can-do("13,401,404",string(lastkey)) and 
         input x-currencyty <> "C" and
         input x-currencyty <> "U" then
        do:
        message "Enter C or U".
        next-prompt x-currencyty with frame f-CNcurr.
        next.
      end.
      apply lastkey.
      if (input x-currencyty = "C" or input x-currencyty = "U") and
        can-do("13,401",string(lastkey)) then
        do:
        assign x-currencyty = input x-currencyty.
        find b-oeehb where 
             b-oeehb.cono    = g-cono and
             b-oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
             b-oeehb.seqno   = 2 and
             b-oeehb.sourcepros = "ComQu"
             no-error.
        if avail b-oeehb then
          do:
          assign substr(b-oeehb.user5,1,1) = x-currencyty.
          if x-currencyty = "c" then
            do:
            assign b-oeehb.divno = 40.
            /****** MOVED TO oeehb_create.i
            *find sastc where sastc.cono = g-cono and
            *                 sastc.currencyty = "CN"
            *                 no-lock no-error.
            *if avail sastc then
            *  assign b-oeehb.user6 = if sastc.purchexrate > 0 then
            *                           1 / sastc.purchexrate
            *                         else
            *                           1.
            *else
            *  assign b-oeehb.user6 = 1.
            ******/
          end. /* if x-currencyty = "c" */
          /*****
          *else
          *  assign b-oeehb.user6 = 1.
          *****/
        end. /* if avail b-oeehb */
        leave.
      end. /* x-currencyty = "C" or "U" */
    end. /* editing */
    end. /* do while */
    hide frame f-CNcurr.
  end. /* v-currencytype = "CN" */
  /*******
  else
    do:
    if v-currencytype = "LB" then
      do:
      find b-oeehb where 
           b-oeehb.cono    = g-cono and
           b-oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
           b-oeehb.seqno   = 2 and
           b-oeehb.sourcepros = "ComQu"
           no-error.
      if avail b-oeehb then
        do:
        find sastc where sastc.cono = g-cono and
                         sastc.currencyty = "LB"
                         no-lock no-error.
        if avail sastc then 
          assign b-oeehb.user6 = if sastc.purchexrate > 0 then
                                   1 / sastc.purchexrate
                                 else
                                   1.
        else
          assign b-oeehb.user6 = 1.
      end. /* if avail b-oeehb */
    end. /* if v-currencytype = "LB" */
  end. /* else */
  *******/
end. /* Procedure Handle_Currency */


/* ------------------------------------------------------------------------- */
Procedure Process_Contact:
/* ------------------------------------------------------------------------- */
  def input parameter v-custno like arsc.custno    no-undo.
  def input parameter v-shipto like arss.shipto    no-undo.
  def input parameter v-phone  as c format "x(12)" no-undo.

  find first notes where 
                   notes.cono         = g-cono and 
                   notes.notestype    = "zz" and
                   notes.primarykey   = "slsmx" and
                   notes.secondarykey = 
                      string(v-custno,"99999999999") +
                             v-shipto
                   no-error. 
  if avail notes then 
    do:
    v-inx = num-entries(notes.user3,"^").
    if v-inx > 0 then
      do:
      do i = 1 to v-inx:
        if v-contact = entry(i,notes.user3,"^") then
          do:
          assign entry(i,notes.user5,"^") = v-phone.
          leave.
        end.
      end. /* i = 1 to v-inx */
    end. /* v-inx > 0 */
  end. /* if avail notes */
end. /* procedure Process_Contact */

/* ------------------------------------------------------------------------- */
Procedure Get_Tax_Flag:
/* ------------------------------------------------------------------------- */
  def input-output parameter v-taxablefl  as logical          no-undo.
  def input-output parameter v-nontaxtype as c format "x(2)"  no-undo.
  
  /*if not avail icsw then*/
    find icsw where icsw.cono = g-cono and
                    icsw.whse = v-whse and
                    icsw.prod = v-prod
                    no-lock no-error.
  if avail icsw then
    do:
    if icsw.taxablety = "Y" then
      assign v-taxablefl  = yes
             v-nontaxtype = "".
    if icsw.taxablety = "N" then
      assign v-taxablefl  = no
             v-nontaxtype = icsw.nontaxtype.
    if icsw.taxablety = "V" then
      do:
      if avail oeehb then 
        assign v-taxablefl = oeehb.taxablefl
               v-nontaxtype = oeehb.nontaxtype.
      else         
      if avail arss then
        do:
        if arss.taxablety = "y" then 
          assign v-taxablefl  = yes
                 v-nontaxtype = "".
        else 
          assign v-taxablefl  = no
                 v-nontaxtype = arss.nontaxtype.
      end. /* avail arss */
      else
        do:
        if avail arsc then
          do:
          if arsc.taxablety = "y" then 
            assign v-taxablefl  = yes
                   v-nontaxtype = "".
          else 
            assign v-taxablefl  = no
                   v-nontaxtype = arsc.nontaxtype.
        end. /* avail arsc */
        else
          assign v-taxablefl  = yes
                 v-nontaxtype = "".
      end. /* else */
    end.
    if avail arss then
      do:
      if arss.taxablety = "N" then 
          assign v-taxablefl  = no
                 v-nontaxtype = arss.nontaxtype.
    end. /* avail arss */
    else
      do:
      if avail arsc then
        do:
        if arsc.taxablety = "N" then 
            assign v-taxablefl  = no
                   v-nontaxtype = arsc.nontaxtype.
      end. /* avail arsc */
    end.
  end. /* avail icsw */
  else /* not avail icsw */
    do:

    if avail oeehb then 
      assign v-taxablefl = oeehb.taxablefl
             v-nontaxtype = oeehb.nontaxtype.
    else
    if avail arss then
      do:
      if arss.taxablety = "y" then 
        assign v-taxablefl  = yes
               v-nontaxtype = "".
      else 
        assign v-taxablefl  = no
               v-nontaxtype = arss.nontaxtype.
    end. /* avail arss */
    else
      do:
      if avail arsc then
        do:
        if arsc.taxablety = "y" then 
          assign v-taxablefl  = yes
                 v-nontaxtype = "".
        else 
          assign v-taxablefl  = no 
                 v-nontaxtype = arsc.nontaxtype.
      end. /* avail arsc */
      else
        assign v-taxablefl  = yes
               v-nontaxtype = "".
    end. /* else check arsc */
  end. /* not avail icsw */

  if v-taxablefl = yes then
    do:
    /**
    if avail oeehb then do:
     /* this should be taken care of from the header determination */
    end.
    else
    **/
    if avail arss then
      do:
      if arss.taxablety = "n" then
        assign v-taxablefl  = no
               v-nontaxtype = arss.nontaxtype.
    end.
    else
      if avail arsc and arsc.taxablety = "n" then
        assign v-taxablefl  = no
               v-nontaxtype = arsc.nontaxtype.
  end.      

end. /* procedure Get_Tax_Flag */

/* ------------------------------------------------------------------------- */
Procedure Load_Variables:
/* ------------------------------------------------------------------------- */
  assign v-enterdt      = oeelb.enterdt
         v-shipdt       = oeelb.reqshipdt
         v-xxda1        = oeelb.xxda1
         v-prod         = oeelb.shipprod
         v-sellprc      = oeelb.price
         v-surcharge    = oeelb.datccost
         v-sparebamt    = oeelb.user6
         v-priceoverfl  = oeelb.priceoverfl
         v-ptype        = oeelb.pricetype
         v-specnstype   = oeelb.specnstype
         v-usagefl      = oeelb.usagefl
         v-rushfl       = oeelb.rushfl
         v-costoverfl   = oeelb.costoverfl
         orig-cost      = dec(substr(oeelb.user1,2,7))
         over-cost      = dec(substr(oeelb.user1,2,7))
         v-qty          = oeelb.qtyord 
         v-reqprod      = oeelb.reqprod 
         v-xrefty       = oeelb.xrefprodty
         v-unit         = oeelb.unit
         v-leadtm       = oeelb.leadtm 
         v-commentfl    = oeelb.commentfl
         v-icspecrecno  = oeelb.icspecrecno
         v-prodcat      = oeelb.prodcat
         v-prodline     = oeelb.prodline
         v-taxgroup     = oeelb.taxgroup
         v-gststatus    = oeelb.gststatus
         v-totnet       = oeelb.netamt
         v-gp           = dec(substr(oeelb.user4,5,7))
         v-listprc      = dec(substr(oeelb.user4,12,13))         
         v-listfl       = substr(oeelb.user4,25,1)
       /*v-shipdt       = date(string(substr(oeelb.user4,26,8),"99/99/99"))*/
         v-pdrecord     = int(substr(oeelb.user4,42,8))
         v-pdtype       = int(substr(oeelb.user4,50,1))
         v-pdamt        = dec(substr(oeelb.user4,51,10))
         v-pd$md        = substr(oeelb.user4,61,1)
         v-totnet       = dec(substr(oeelb.user4,62,11))         
         v-lcomment     = substr(oeelb.user4,74,1)
         v-pdbased      = substr(oeelb.user4,75,4)
         v-totcost      = dec(substr(oeelb.user3,44,11))         
         v-rebatefl     = substr(oeelb.user3,55,1)
         v-totmarg      = dec(substr(oeelb.user3,56,11))         
         v-margpct      = dec(substr(oeelb.user3,67,11))         
         v-ordertype    = oeelb.ordertype
         v-slsrepin     = oeelb.slsrepin
         v-slsrepout    = oeelb.slsrepout
         v-nontaxtype   = oeelb.nontaxtype
         v-botype       = oeelb.botype
         v-prodcost     = oeelb.prodcost
         v-glcost       = dec(oeelb.user2)
         v-vendno       = oeelb.vendno
         v-taxablefl    = oeelb.taxablefl
         v-xprod        = substr(oeelb.user5,30,24)
         v-descrip2     = oeelb.proddesc2.
  find icsp where icsp.cono = g-cono and
                  icsp.prod = v-prod
                  no-lock no-error.
  if avail icsp then 
    assign v-icnotefl = icsp.notesfl.
  else
    assign v-icnotefl = "".
end. /* Load_Variables */


/* ------------------------------------------------------------------------- */
Procedure Convert_Option:
/* ------------------------------------------------------------------------- */
/* TAH FG */
  find first b-oeehb where b-oeehb.cono = g-cono and  
             b-oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
             b-oeehb.seqno = 2 and
             b-oeehb.sourcepros = "ComQu"
             no-lock no-error.    
  if avail b-oeehb and b-oeehb.stagecd = 9 then 
    return.

  form
     v-cnvfl  at 1 label
                  "Would you like to convert this quote to an order?"
    
  with frame f-cnv side-labels row 5 centered overlay.

  assign v-checkingfl = true.
/* TAH FG */ 
  
  assign v-nextmainfl = no.
  /* %toms% 3 */
  assign x-backed-quote-up = false.
  /* %toms% 3 */
  do transaction:
    find oeehb where oeehb.cono = g-cono and  
                     oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
                     oeehb.seqno      = 2 and
                     oeehb.sourcepros = "ComQu" and
                     oeehb.stagecd <> 9
                     no-error.
    if avail oeehb then 
      do:
      assign oeehb.promisedt = TODAY
             oeehb.totdatccost = if lastkey = 401 then 0 else oeehb.totdatccost
             g-whse          = oeehb.whse.
      for each t-lines no-lock:
        assign oeehb.totdatccost = if lastkey = 401 then
               oeehb.totdatccost + (t-lines.surcharge * t-lines.qty) 
                                  else oeehb.totdatccost.
        if t-lines.shipdt > oeehb.promisedt then
          assign oeehb.promisedt = t-lines.shipdt.
      end.
    end.
    else
      do:
      run Clear-Frames.
      run Initialize-All.
      run Setup-Options.
      display v-quoteno  v-notefl   v-custno   v-arnotefl v-custname 
              v-takenby  v-custlu   v-shipto   v-contact  v-phone 
              v-custpo   v-currency v-terms    v-compfocus
              v-canceldt v-quotetot v-quotemgn%
      with frame f-top1.
      release oeehb no-error.
      release oeelb no-error.
      release notes no-error.
      /*
      next main.
      */
      assign v-nextmainfl = yes.
    end.
  end. /* transaction */
  cnvquote:
  do while v-checkingfl on endkey undo cnvquote, leave cnvquote:
    
    find first b-oeehb where b-oeehb.cono = g-cono and  
               b-oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
               b-oeehb.seqno = 2 and
               b-oeehb.sourcepros = "ComQu"
               no-lock no-error.    
    if avail b-oeehb and b-oeehb.stagecd = 9 then
      return.
      
    update v-cnvfl label
    "Would you like to convert this quote to an order?"
    
    with frame f-cnv side-labels row 5 centered overlay.
 /* %toms% */

    if v-cnvfl = yes and v-commissionfl = yes then do:
      message "Commission Quotes cannot be converted".
      next cnvquote.
    end.  

    assign v-checkingfl = false.

    if v-cnvfl = yes then 
      do:
      assign F7-pressed = no.
      hide frame f-cnv no-pause.
      find first b-oeehb where b-oeehb.cono = g-cono and  
                 b-oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
                 b-oeehb.seqno = 2 and
                 b-oeehb.sourcepros = "ComQu"
                 no-lock no-error.    
      if avail b-oeehb then 
        do:
        assign ip-update = true.
        run Conversion (input recid(b-oeehb)).
        if ip-update then 
          do:
          run Check_Quote_Restore.
          run Check_Quote_Mortality.
          if can-find(first oeeh where 
                            oeeh.cono      = g-cono and
                            oeeh.orderno   = g-orderno and
                            oeeh.ordersuf  = 0 and
                            oeeh.enterdt   = TODAY and
                            oeeh.approvty <> "y" no-lock) then
            do:
            if not can-find (first oeel where 
                                   oeel.cono = g-cono and
                                   oeel.orderno = g-orderno and
                                   oeel.ordersuf = 0 and
                                   oeel.enterdt = TODAY no-lock) then
              do:
              find oeeh where oeeh.cono = g-cono and
                              oeeh.orderno = g-orderno and
                              oeeh.ordersuf = 0 and
                              oeeh.enterdt = TODAY
                              no-error.
              if avail oeeh then
                do:
                delete oeeh.
                run Restore_Quote_to_Uncanelled.
                message string(v-quoteno,">>>>>>>9") + 
                     " Quote did NOT Convert. Contact IT with Quote Info (y)".
                assign g-orderno = v-quoteno
                       errorfl   = yes.     
                pause.
                /*return.*/
              end.         
            end. /* no-lines available */
            if errorfl = no then
              do:
              find first poelo where poelo.cono = g-cono and
                                     poelo.ordertype = "o" and
                                     poelo.orderaltno = g-orderno and
                                     poelo.orderaltsuf = 0
                                     no-lock no-error.
              if avail poelo then
                assign tied-po = poelo.pono.
              else
                assign tied-po = 0.
              find first wtelo where wtelo.wtcono = g-cono and
                                     wtelo.ordertype = "o" and
                                     wtelo.orderaltno = g-orderno and
                                     wtelo.orderaltsuf = 0
                                     no-lock no-error.
              if avail wtelo then
                assign tied-wt = wtelo.wtno.
              else
                assign tied-wt = 0.
              if tied-po = 0 and tied-wt = 0 then
                message "Customer Order " + string(ip-orderno) + " created". 
              if tied-po > 0 then
                message "Customer Order " + string(ip-orderno) + " created. " +
                        "Purchase Order " + string(tied-po) + " created.".
              if tied-wt > 0 then
                message "Customer Order " + string(ip-orderno) + " created. " +
                        "WHSE Transfer " + string(tied-wt) + " created.".
              run warning.p(8624).
              if s-oelockfl = yes then
                run Set_OE_Lock (input ip-orderno).
            end.
          end. /* if can-find first */
          else 
            do:
            if can-find(first oeeh where
                              oeeh.cono     = g-cono and
                              oeeh.orderno  = g-orderno and
                              oeeh.ordersuf = 0 and
                              oeeh.enterdt  = TODAY no-lock) then
              do:
              if not can-find (first oeel where 
                                     oeel.cono = g-cono and
                                     oeel.orderno = g-orderno and
                                     oeel.ordersuf = 0 and
                                     oeel.enterdt = TODAY no-lock) then
                do:
                find oeeh where oeeh.cono = g-cono and
                                oeeh.orderno = g-orderno and
                                oeeh.ordersuf = 0 and
                                oeeh.enterdt = TODAY
                                no-error.
                if avail oeeh then
                  do:
                  delete oeeh.
                  run Restore_Quote_to_Uncanelled.
                  message string(v-quoteno,">>>>>>>9") + 
                      " Quote did NOT Convert. Contact IT with Quote Info (x)".
                  assign g-orderno = v-quoteno
                         errorfl   = yes.     
                  pause.
                  /*return.*/
                end.         
              end. /* no-lines available */
              if errorfl = no then
                do:
                find first poelo where poelo.cono = g-cono and
                                       poelo.ordertype = "o" and
                                       poelo.orderaltno = g-orderno and
                                       poelo.orderaltsuf = 0
                                       no-lock no-error.
                if avail poelo then
                  assign tied-po = poelo.pono.
                else
                  assign tied-po = 0.
                find first wtelo where wtelo.wtcono = g-cono and
                                       wtelo.ordertype = "o" and
                                       wtelo.orderaltno = g-orderno and
                                       wtelo.orderaltsuf = 0
                                       no-lock no-error.
                if avail wtelo then
                  assign tied-wt = wtelo.wtno.
                else
                  assign tied-wt = 0.
                if tied-po = 0 and tied-wt = 0 then
                  message "Customer Order " + string(ip-orderno) + " created". 
                if tied-po > 0 then
                  message "Customer Order " + string(ip-orderno) + " created. "                         + "Purchase Order " + string(tied-po) + " created.".
                if tied-wt > 0 then
                  message "Customer Order " + string(ip-orderno) + " created. "
                        + "WHSE Transfer " + string(tied-wt) + " created.".
                pause.
                if s-oelockfl = yes then
                  run Set_OE_Lock (input ip-orderno).
              end.
            end. /* if can find first */
          end. 
          run Clear-Frames.
          /*if v-faxpopup and errorfl = no then*/
          if v-faxpopup then
            do:
            if errorfl = yes then
              run oeexcg.p.
            else
              run oeexqgo.p.
          end.
          leave cnvquote.
          hide frame f-cnv no-pause.
        end. /* if ip-update */
        else 
          do:
          run Check_Quote_Restore.
          run Clear-Frames.
          if v-faxpopup and errorfl = no then
            run oeexcg.p.
          hide frame f-cnv no-pause.
          leave cnvquote.
        end.
      end. /* avail oeehb */
      else  
        do:
        run Check_Quote_Restore.
        hide frame f-cnv no-pause.
        assign v-blanks = string(" ","x(76)").         
        put screen row 22 col 3 v-blanks.
        run Clear-Frames.
        /*if v-faxpopup and errorfl = no then*/
        if v-faxpopup then
          do:
          if errorfl = yes then
            run oeexcg.p.
          else
            run oeexqgo.p.
        end.
        hide frame f-cnv no-pause.
        leave cnvquote.
      end. /* not avail oeehb */
    end. /* v-cnvfl = yes */
    else
      do:
      if lastkey = 404 then
        do:
        if v-faxpopup and errorfl = no then
          run oeexcg.p.
      end.
    end.
  end. /* cnvquote */
  run Check_Quote_Restore.
  hide frame f-cnv no-pause.
end. /* Procedure Convert_Option */
 
 
/* ------------------------------------------------------------------------- */
Procedure Set_OE_Lock:
/* ------------------------------------------------------------------------- */
  define input        parameter ip-orderno like oeeh.orderno       no-undo.
  for each oeeh where oeeh.cono = g-cono and
                    oeeh.orderno = ip-orderno and
                    oeeh.ordersuf = 0 and
                    oeeh.transtype = "FO":
    if s-oelockfl = yes then
      assign oeeh.lockfl = yes.
  end.
end. /* Procedure Set_OE_Lock */

/* ------------------------------------------------------------------------- */
Procedure Catalog_Audit:
/* ------------------------------------------------------------------------- */
  if s-yn = yes then
    do:
    if not avail icsp then
      do:
      find icsp where icsp.cono = g-cono and
                      icsp.prod = v-prod
                      no-lock no-error.
      if avail icsp then
        do:
        find icsw where icsw.cono = g-cono and
                        icsw.whse = v-whse and
                        icsw.prod = v-prod no-lock no-error.
        if avail icsw then
          do:
          find first icsl where icsl.cono     = g-cono and
                                icsl.whse     = icsw.whse and
                                icsl.vendno   = icsw.arpvendno and
                                icsl.prodline = icsw.prodline
                                no-lock no-error.
        end. /* avail icsw */
      end. /* avail icsp */
    end. /* not avail icsp */
  end. /* s-yn = yes */

  assign x-catlineno = v-lineno * 1000.
  if x-prodtype = "comp" then assign x-catlineno = x-catlineno + v-seqno.
  
  find first zidc use-index docinx4 where
             zidc.cono       = g-cono and
             zidc.docstatus  = (if s-yn = yes then 50 else 51) and
             zidc.docttype   = "CAT" and
             zidc.doctype    = "oeexc" and
             substring(zidc.docpartner,1,4)  = v-whse and
             substring(zidc.docpartner,5,24) = v-prod and
             zidc.docgsnbr   = x-catlineno
             no-lock no-error.
  if not avail zidc then 
    do:
    create zidc.
    assign zidc.cono       = g-cono
           zidc.docnumber  = v-quoteno
           zidc.docdate    = TODAY
           substring(zidc.docpartner,1,4)  = if avail icsw then icsw.whse
                                             else v-whse
           substring(zidc.docpartner,5,24) = if avail icsw then icsw.prod
                                             else v-prod
           substring(zidc.docdesc,1,24) = if avail icsp then icsp.descrip[1]
                                          else 
                                          if avail icsc then icsc.descrip[1]
                                          else v-descrip
           substring(zidc.docdesc,27,4) = g-operinit
           zidc.docstatus  = if avail icsp then 51 else 50
           zidc.docdisp    = ""
           zidc.docstnbr   = 0
           zidc.docfadate  = ?
           zidc.doctype    = "OEEXC"
           zidc.docgsnbr   = x-catlineno
           zidc.docttype   = "CAT"
           substring(zidc.docuser1,1,6) = if avail icsw then icsw.prodline
                                          else if avail icsc then icsc.prodline
                                          else "      "
           substring(zidc.docuser1,7,24) = if avail icsp then icsp.descrip[1]
                                           else if avail icsc then 
                                             icsc.descrip[1]
                                           else v-descrip
           substring(zidc.docuser2,1,15) = "               "
           substring(zidc.docuser2,17,4) = if avail icsp then icsp.prodcat
                                           else v-prodcat
           substring(zidc.docuser3,1,24) = if avail icsw then icsw.prod
                                           else v-prod
           substring(zidc.docuser3,27,4) = if avail icsw then icsw.pricetype
                                           else v-ptype
           substring(zidc.docuser4,1,4)  = if avail icsw then icsw.whse
                                           else v-whse
           substring(zidc.docuser4,6,12) = if avail icsw then 
                                             string(icsw.arpvendno)
                                           else
                                           if avail icsc then
                                             string(icsc.vendno)
                                           else
                                             string(v-vendno)
           zidc.docvendno  = if avail icsw then icsw.replcost
                             else if avail icsc then icsc.prodcost
                             else v-prodcost
           zidc.docstnbr   = if avail icsw then icsw.leadtmavg
                             else if avail icsc then int(icsc.user6) else 30
           zidc.docdisp = "".
  end. /* not avail zidc */
end. /* Procedure Catalog_Audit */

/* ------------------------------------------------------------------------- */
Procedure Update_ShipTo_Info:
/* ------------------------------------------------------------------------- */
  if avail arss then
    do:
    for each t-oeehb where 
             t-oeehb.cono = g-cono and  
             t-oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
             t-oeehb.seqno      = 2 and
             t-oeehb.sourcepros = "ComQu":
      if v-mode = "C" or v-chgfl = yes then
        do:
        assign t-oeehb.shipto        = arss.shipto
               t-oeehb.orderdisp     = arss.orderdisp
               t-oeehb.shiptonm      = if v-shiptonm <> "" then
                                         v-shiptonm else arss.name
               t-oeehb.shiptoaddr[1] = if v-shiptoaddr1 <> "" then
                                         v-shiptoaddr1 else arss.addr[1]
               t-oeehb.shiptoaddr[2] = if v-shiptoaddr2 <> "" then
                                         v-shiptoaddr2 else arss.addr[2]
               t-oeehb.shiptocity    = if v-shiptocity <> "" then
                                         v-shiptocity else arss.city
               t-oeehb.shiptost      = if v-shiptost <> "" then
                                         v-shiptost else arss.state
               t-oeehb.shiptozip     = if v-shiptozip <> "" then
                                         v-shiptozip else arss.zip
               t-oeehb.geocd         = if v-shiptogeocd <> 0 then
                                         v-shiptogeocd else arss.geocd
               t-oeehb.slsrepout     = arss.slsrepout
               t-oeehb.slsrepin      = arss.slsrepin 
               t-oeehb.codfl         = if arss.termstype = "COD" then 
                                         yes else no
               /*
               t-oeehb.outbndfrtfl   = arss.outbndfrtfl /*das-03/02/09*/
               t-oeehb.inbndfrtfl    = arss.inbndfrtfl  /*das-03/02/09*/
               */
               t-oeehb.outbndfrtfl   = no
               t-oeehb.inbndfrtfl    = no
               t-oeehb.taxablefl     = if arss.taxablety = "n" then no
                                     else yes.
        for each t-oeelb where t-oeelb.cono = g-cono and
                               t-oeelb.batchnm = t-oeehb.batchnm and
                               t-oeelb.seqno   = t-oeehb.seqno:
          assign t-oeelb.slsrepout  = arss.slsrepout
                 t-oeelb.slsrepin     = arss.slsrepin.
        end. /* each oeelb */
        assign v-shiptonm    = if v-shiptonm = "" then arss.name
                               else v-shiptonm
               v-shiptoaddr1 = if v-shiptoaddr1 = "" then arss.addr[1]
                               else v-shiptoaddr1
               v-shiptoaddr2 = if v-shiptoaddr2 = "" then arss.addr[2]
                               else v-shiptoaddr2
               v-shiptocity  = if v-shiptocity = "" then arss.city
                               else v-shiptocity
               v-shiptost    = if v-shiptost = "" then arss.state
                               else v-shiptost
               v-shiptozip   = if v-shiptozip = "" then arss.zip
                               else v-shiptozip
               v-shiptogeocd = if v-shiptogeocd = 0 then arss.geocd
                               else v-shiptogeocd
               g-shipto      = arss.shipto
               v-slsrepout   = arss.slsrepout
               o-slsrepout   = arss.slsrepout
               v-slsrepin    = arss.slsrepin
               v-taxablefl   = if arss.taxablety = "n" 
                               then no else yes.
      end. /* if mode = "c" or chgfl = yes */
    end. /* each oeehb */
  end. /* avail arss */
end. /* Procedure Update_ShipTo_Info */

/* ------------------------------------------------------------------------- */
Procedure oeehb_create_rec:
/* ------------------------------------------------------------------------- */

  assign  
        oeehb.cono          = g-cono
        oeehb.batchnm       = string(w-batchnm,">>>>>>>9")
        oeehb.seqno         = 2
        oeehb.transtype     = "QU"
        oeehb.stagecd       = 0
        oeehb.inprocessfl   = yes 
        oeehb.user2         = g-operinit
        oeehb.custno        = arsc.custno
        oeehb.divno         = arsc.divno
        oeehb.shipto        = v-shipto
        oeehb.takenby       = v-takenby
        oeehb.statecd       = if avail arss then arss.statecd else arsc.statecd
        oeehb.countrycd     = if oeehb.countrycd <> "" then oeehb.countrycd
                                  else 
                                  if arsc.divno = 40 then "CN"
                                    else arsc.currencyty
        oeehb.pricecd       = if avail arss and v-shipto <> " "
                                then arss.pricecd
                              else arsc.pricecd
        oeehb.whse          = if avail arss and v-shipto <> " " 
                                then arss.whse
                              else arsc.whse
        oeehb.shipviaty     = if avail arss and v-shipto <> " "
                                then arss.shipviaty
                              else arsc.shipviaty
        /*
        oeehb.outbndfrtfl   = if avail arss and v-shipto <> " "
                                then arss.outbndfrtfl
                              else
                                arsc.outbndfrtfl
        oeehb.inbndfrtfl    = if avail arss and v-shipto <> " "
                                then arss.inbndfrtfl
                              else
                                arsc.inbndfrtfl
        */
        oeehb.outbndfrtfl   = no
        oeehb.inbndfrtfl    = no
        oeehb.addonno[1]    = 0
        oeehb.addonno[2]    = 0
        oeehb.addonno[3]    = 0
        oeehb.addonno[4]    = 0
        oeehb.orderdisp     = if avail arss and v-shipto <> " "
                                then arss.orderdisp
                              else
                                arsc.orderdisp
        oeehb.bofl          = /*if avail arss and v-shipto <> " "
                                then arss.bofl
                              else
                                arsc.bofl*/ no
        oeehb.subfl         = if avail arss and v-shipto <> " "
                                then arss.subfl
                              else
                                arsc.subfl
        oeehb.slsrepout     = if avail arss and v-shipto <> " "
                                then arss.slsrepout
                              else arsc.slsrepout
        oeehb.slsrepin      = if avail arss and v-shipto <> " "
                                then arss.slsrepin
                              else arsc.slsrepin
        oeehb.custpo        = v-custpo
        oeehb.orderdisp     = if avail arss and v-shipto <> " " 
                                then arss.orderdisp
                              else arsc.orderdisp
        oeehb.termstype     = if avail arss and v-shipto <> " "
                                then arss.termstype
                              else arsc.termstype
        oeehb.approvty      = /*sasc.oeapprty*/ "y"
        oeehb.shipinstr     = if avail arss and v-shipto <> " "
                                then arss.shipinstr
                              else arsc.shipinstr
        oeehb.codfl         = if avail arss and arss.termstype = "COD" 
                                then yes     
                              else
                                if avail arsc and arsc.termstype = "COD" 
                                  then yes     
                                else no                              
        oeehb.placedby      = v-contact
        oeehb.canceldt      = v-canceldt
        oeehb.reqshipdt     = TODAY
        oeehb.poissdt       = ?
        oeehb.enterdt       = g-today
        oeehb.sourcepros    = "ComQu"
        substr(oeehb.user3,1,20) = string(v-contact,"x(20)")
        substr(oeehb.user3,21,12) = string(v-phone,"x(12)")
        substr(oeehb.user3,33,20) = string(v-email,"x(20)")
        substr(oeehb.user5,1,1)   = v-currencyty
        hdr-whse            = oeehb.whse
        oeehb.nontaxtype    = if avail arss then arss.nontaxtype
                              else arsc.nontaxtype
        oeehb.taxablefl     = if avail arss and arss.taxablety = "n" then
                                no
                              else 
                                if avail arss and arss.taxablety = "y" then
                                  yes
                                else
                                  if avail arsc and arsc.taxablety = "n" then
                                    no
                                  else
                                    if avail arss and arsc.taxablety = "y" then
                                      yes
                                    else
                                      yes.
        find sastc where sastc.cono = g-cono and
                         sastc.currencyty = arsc.currencyty
                         no-lock no-error.
        if avail sastc then
          assign oeehb.user6 = if sastc.purchexrate > 0 then
                                 1 / sastc.purchexrate
                               else
                                 0.
        else
          assign oeehb.user6 = 0.
        assign
          oeehb.shiptonm      = if v-shiptonm <> " " then v-shiptonm
                                else oeehb.shiptonm
          oeehb.shiptoaddr[1] = if v-shiptoaddr1 <> " " then v-shiptoaddr1
                                else oeehb.shiptoaddr[1]
          oeehb.shiptoaddr[2] = if v-shiptoaddr2 <> " " then v-shiptoaddr2
                                else oeehb.shiptoaddr[2]
          oeehb.shiptocity    = if v-shiptocity <> " " then CAPS(v-shiptocity)
                                else oeehb.shiptocity
          oeehb.shiptost      = if v-shiptost <> " " then CAPS(v-shiptost)
                                else oeehb.shiptost
          oeehb.shiptozip     = if v-shiptozip <> " " then CAPS(v-shiptozip)
                                else oeehb.shiptozip
          oeehb.geocd         = if v-shiptogeocd <> 0 then v-shiptogeocd
                                else oeehb.geocd
          oeehb.xxl12         = w-shipchgfl.  /*not used but needed since
                                                both OEEXQ and OEEXC share
                                                shiptopopup.p*/
       assign oeehb.totinvamt = v-commtot
              oeehb.xxde2     = v-quotetot.
       assign oeehb.xxde1     = tot-Ccomm%.

  {t-all.i "oeehb"}

end. /* Procedure oeehb_create_rec */


/* ------------------------------------------------------------------------- */
Procedure Conversion_Line_Split:
/* ------------------------------------------------------------------------- */
  /***
  if g-operinit begins "das" then
    do:
    message "here in Conversion_Line_Split"
            "v-continue:" v-continue. pause.
  end.
  ***/
  /*if not avail oeehb then*/
    find oeehb where oeehb.cono = g-cono and
                     oeehb.batchnm    = string(v-quoteno,">>>>>>>9") and
                     oeehb.seqno      = 2 and
                     oeehb.sourcepros = "Comqu"
                     no-error.
  if avail oeehb then
    do:
    assign linecount = 0.
    for each oeelb where
             oeelb.cono    = g-cono and
             oeelb.batchnm = oeehb.batchnm and
             oeelb.seqno   = oeehb.seqno:
      /* keep the qtyrem in line with the backup copy */
      find c-oeelb where c-oeelb.cono = 500 + g-cono and
                         c-oeelb.batchnm = oeelb.batchnm and
                         c-oeelb.seqno   = oeelb.seqno and
                         c-oeelb.lineno  = oeelb.lineno and
                         c-oeelb.specnstype <> "L" no-error.
      /* done in conversion_line_edit 
      if avail c-oeelb then
        assign c-oeelb.xxde4 = oeelb.xxde4 + oeelb.qtyord.
      */
      /* assign the quantity converted here */
      assign x-name = " ".
      find icsw where icsw.cono = g-cono and
                      icsw.whse = h-whse and
                      icsw.prod = oeelb.shipprod
                      no-lock no-error.
      if avail icsw then
        find apsv where apsv.cono = g-cono and
                        apsv.vendno = icsw.arpvendno
                        no-lock no-error.
      else
        find apsv where apsv.cono = g-cono and
                        apsv.vendno = oeelb.arpvendno
                        no-lock no-error.
      if avail apsv then 
        assign x-name = entry(1,apsv.name," ").
      find first icsw where icsw.cono = g-cono and
                            icsw.prod begins x-name and
                            icsw.whse = oeehb.whse and
                            icsw.prod matches "*COMMISSION*"
                            no-lock no-error.
      if avail icsw then
        do:
        assign oeelb.proddesc = oeelb.shipprod.
        assign oeelb.shipprod = icsw.prod.
        assign oeelb.qtyord   = if v-continue = yes then
                                  oeelb.qtyord - oeelb.xxde4
                                else
                                  oeelb.qtyord.
        assign oeelb.cono     = g-cono + 501.  /* put in 502 */
      end. /* avail icsw */
      else
        do:
        assign oeelb.proddesc = oeelb.shipprod.
        assign oeelb.shipprod = "FG VENDOR COMMISSION".
        assign oeelb.qtyord   = if v-continue = yes then
                                  oeelb.qtyord - oeelb.xxde4
                                else
                                  oeelb.qtyord.
        assign oeelb.cono     = g-cono + 501.
      end.
      find last icss where icss.cono = g-cono and
                           icss.prod = oeelb.shipprod
                           no-lock no-error.
      if not avail icss then
        do:
        create icss.
        assign icss.cono        = g-cono
               icss.prod        = oeelb.shipprod
               icss.icspecrecno = 1
               icss.transdt     = TODAY
               icss.transtm     = string(TIME,"HH:MM")
               icss.operinit    = g-operinits
               icss.statusfl    = no
               icss.transproc   = "oeexc"
               zi-icspecrecno   = 1.
      end. /* not avail icss */
      assign linecount = linecount + 1.
      assign v-prod = oeelb.shipprod.
    end. /* each oeelb */
    /* We have linecount. Now, determine how many lines we will create.
       we create a line for every split */
    assign splitcount = 0.
    for each bpeh where bpeh.cono = g-cono and
                        bpeh.bpid = oeehb.batchnm + "C"
                        no-lock:
      if bpeh.slsrepout <> " " /* or bpeh.awardnm <> " " */ then
        assign splitcount = splitcount + 1.
    end.
    if splitcount >= 1 then
      do:
      assign createline = linecount * splitcount.
      assign xx-line = 1.
      /*do bpi = linecount to 1 by -1:*/
      do bpi = 1 to linecount:
        find c-oeelb where c-oeelb.cono    = g-cono + 501 and
                           c-oeelb.batchnm = oeehb.batchnm and
                           c-oeelb.seqno   = oeehb.seqno and
                           c-oeelb.lineno  = bpi and
                           c-oeelb.qtyord  > 0 and
                           c-oeelb.specnstype <> "L"
                           no-error.
        if avail c-oeelb then
          do:
          for each bpel where bpel.cono = g-cono and
                              bpel.bpid = oeehb.batchnm + "C" and
                              bpel.lineno = c-oeelb.lineno and
                              bpel.itemid <> " " and
                              bpel.revno  <= 3    /* don't create for comany */
                              no-lock:
            /* find the original bpel to get the original quantity ordered */
            /****
            find o-bpel where o-bpel.cono   = g-cono + 500 and
                              o-bpel.bpid   = bpel.bpid and
                              o-bpel.lineno = bpel.lineno and
                              o-bpel.revno  = bpel.revno
                              no-lock no-error.
            if avail o-bpel then
              assign hold-qty = o-bpel.qtyord.
            else
              assign hold-qty = 1.
            ****/
            /* 12/08/2015 */
            if v-continue = no then
              assign hold-qty = c-oeelb.qtyord.
            else
              assign hold-qty = bpel.qtyord.
            create oeelb.
            buffer-copy c-oeelb except lineno
                                       slsrepout
                                       price
                                       cono
            to oeelb.
            assign 
              /*oeelb.lineno    = createline - xx-line*/
              oeelb.lineno    = xx-line
              oeelb.slsrepout = bpel.itemid
              oeelb.price     = bpel.awardprice / hold-qty
              oeelb.prodcost  = 0
              oeelb.icspecrecno = 1
              oeelb.xxi5      = c-oeelb.lineno
              oeelb.xxde5     = if bpel.revno = 1 then
                                  if hold-qty > 0 then
                                     bpel.awardprice / hold-qty
                                  else 0
                                else
                                if bpel.revno = 2 then
                                  if hold-qty > 0 then
                                    bpel.awardprice / hold-qty
                                  else 0
                                else
                                if bpel.revno = 3 then
                                  if hold-qty > 0 then
                                    bpel.awardprice / hold-qty
                                  else 0
                                else oeelb.xxde5.
            assign oeelb.cono      = g-cono.
            assign oeelb.specnstype = if oeelb.specnstype = "n" then "s"
                                      else oeelb.specnstype.
            assign xx-line = xx-line + 1. 
          end. /* each bpel */
          delete c-oeelb.
        end. /* avail c-oeelb */
        else
          do:
          for each c-oeelb where c-oeelb.cono    = g-cono + 501 and
                                 c-oeelb.batchnm = oeehb.batchnm and
                                 c-oeelb.seqno   = oeehb.seqno and
                                 c-oeelb.qtyord  = 0:
            delete c-oeelb.
          end.
        end. /* delete the lines with 0 qtyord */
      end. /* bpi linecount to 1 */
    end. /* splitcount > 1 - create multilpe oe lines */
  end.  /* if avail oeehb */
end. /* Conversion_Line_Split */

/* ------------------------------------------------------------------------- */
Procedure Conversion_Line_Edit:
/* ------------------------------------------------------------------------- */
  assign errorfl = no.
  /*
  if not avail oeehb then
    find oeehb where oeehb.cono = g-cono and
                     oeehb.batchnm    = string(v-quoteno,">>>>>>>9") and
                     oeehb.seqno      = 2 and
                     oeehb.sourcepros = "Comqu"
                     no-error.
  if avail oeehb then
    do:
    for each oeelb where
             oeelb.cono    = g-cono and
             oeelb.batchnm = oeehb.batchnm and
             oeelb.seqno   = oeehb.seqno:
      find c-oeelb where c-oeelb.cono    = oeelb.cono + 500 and
                         c-oeelb.batchnm = oeelb.batchnm and
                         c-oeelb.seqno   = oeelb.seqno and
                         c-oeelb.lineno  = oeelb.lineno
                         no-lock no-error.
      if oeelb.qtyord > c-oeelb.qtyord - oeelb.xxde4 then
        do:
        assign v-lineno = oeelb.lineno
               errorfl = yes.
      end.
    end.
  end.
  */
  for each oeelb where oeelb.cono = g-cono and
                       oeelb.batchnm = string(v-quoteno,">>>>>>>9") and
                       oeelb.seqno = 2
                       no-lock:
    find c-oeelb where c-oeelb.cono    = g-cono + 500 and
                       c-oeelb.batchnm = oeelb.batchnm and
                       c-oeelb.seqno   = oeelb.seqno and
                       c-oeelb.lineno  = oeelb.lineno and
                       c-oeelb.specnstype <> "L"
                       no-error.
    if avail c-oeelb then
      do:
      assign c-oeelb.xxde4 = c-oeelb.xxde4 + oeelb.qtyord.
      if c-oeelb.xxde4 = c-oeelb.qtyord then
        assign c-oeelb.specnstype = "l"
               c-oeelb.reasunavty = "CO".
    end.           
  end. /* each oeelb */
end. /* Conversion_Line_Edit */

/* ------------------------------------------------------------------------ */
Procedure Restore_Quote_to_Uncanelled:
/* ------------------------------------------------------------------------ */
/* reverse the cancellation of a quote */
  for each b-oeehb use-index k-oeehb where 
           b-oeehb.cono = g-cono and
           b-oeehb.batchnm = string(v-quoteno,">>>>>>>9") and
           b-oeehb.seqno   = 2 and
           b-oeehb.sourcepros = "ComQu":
    assign b-oeehb.transtype = "QU"
           b-oeehb.stagecd   = 0.
    overlay (b-oeehb.user3,53,4) = "    ".
    for each b-oeelb where b-oeelb.cono = g-cono and
                           b-oeelb.batchnm = b-oeehb.batchnm and
                           b-oeelb.seqno = 2:
      find icsw where icsw.cono = g-cono and
                      icsw.whse = substr(b-oeelb.user4,1,4) and
                      icsw.prod = b-oeelb.shipprod
                      no-lock no-error.
      if avail icsw then
        assign b-oeelb.specnstype = if icsw.statustype = "o" then "s"
                                  else "".
      else
        assign b-oeelb.specnstype = "".
      assign b-oeelb.reasunavty = ""
             b-oeelb.xxde4 = 0.
    end. /* each b-oeelb */
  end. /* each b-oeehb */
end. /* Procedure Restore_Quote_to_Uncanelled */



