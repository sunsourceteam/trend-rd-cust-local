{g-all.i}
{g-oe.i}

def new shared var q-line     as integer    init 1             no-undo.
def     shared var v-quoteno  as integer    format ">>>>>>>9"  no-undo.
def     shared var v-lineno   like oeelb.lineno                no-undo.
def     shared var v-prod     like oeelb.shipprod              no-undo.


define new shared temp-table t-hist no-undo
  field batchnm   like oeelb.batchnm
  field prod      like oeelb.shipprod
  field orderno   like oeeh.orderno
  field ordersuf  like oeeh.ordersuf
  field lineno    like oeelb.lineno
  field stagecd   like oeeh.stagecd
  field dateconv  as date format "99/99/99"
  field qtyconv   like oeelb.qtyord
  field qtyremain as de format ">>>>>9.99-"
  index k-hist
        batchnm
        orderno
        ordersuf
        lineno
        prod.

define new shared buffer s-hist for t-hist.

find oeehb where oeehb.cono      = g-cono and
                 oeehb.batchnm   = string(v-quoteno,">>>>>>>9") and
                 oeehb.seqno     = 2 and
                 oeehb.sourcepros = "ComQu"
                 no-lock no-error.
if not avail oeehb then return.
find oeelb where oeelb.cono = oeehb.cono and
                 oeelb.batchnm  = oeehb.batchnm and
                 oeelb.seqno    = oeehb.seqno and
                 oeelb.lineno   = v-lineno
                 no-lock no-error.
if not avail oeelb then return.
for each oeeh where oeeh.cono = oeehb.cono and
                    oeeh.whse  begins "a" and
                    oeeh.custno = oeehb.xxde5 and
             substr(oeeh.user3,65,8) = oeehb.batchnm
                    no-lock:
  find first oeel where oeel.cono     = oeeh.cono      and
                        oeel.orderno  = oeeh.orderno   and
                        oeel.ordersuf = oeeh.ordersuf  and
                        oeel.proddesc = oeelb.shipprod and
                        oeel.user7    = dec(oeelb.lineno)
                        no-lock no-error.
  if avail oeel then
    do:
    find t-hist where t-hist.batchnm  = oeelb.batchnm and  
                      t-hist.orderno  = oeeh.orderno  and
                      t-hist.ordersuf = oeeh.ordersuf and
                      t-hist.lineno   = oeel.lineno   and
                      t-hist.prod     = oeelb.shipprod
                      no-lock no-error.
    if avail t-hist then next.
    create t-hist.
    assign t-hist.batchnm   = oeehb.batchnm
           t-hist.prod      = oeelb.shipprod
           t-hist.orderno   = oeel.orderno
           t-hist.ordersuf  = oeel.ordersuf
           t-hist.lineno    = oeelb.lineno
           t-hist.stagecd   = oeeh.stagecd
           t-hist.dateconv  = oeel.enterdt
           t-hist.qtyconv   = oeel.qtyord
           t-hist.qtyremain = oeelb.qtyord - oeelb.xxde4.
  end. /* avail oeel */
end. /* each oeeh */

on cursor-up back-tab.
on cursor-down tab.

run oeexchist99.p.

on cursor-up back-tab.
on cursor-down tab.

    
                      
