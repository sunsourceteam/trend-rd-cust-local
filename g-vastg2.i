/*  g-vastg2.i    */

def var v-stage2 as c extent 16 initial
["Rcv","Sch","Tdn","Cst","Qtd","Apr","Por","Pin","Srp",
 "Asy","Tst","Pnt","Inf","Can","Cls","Scp"] no-undo.

def var v-stage3 as c extent 16 initial
["Received","Scheduled","Tear Down","Costing","Quoted",
 "Approved","Parts On Order","Parts In Inventory",
 "Scheduled Repair","Assembly","Test","Paint","Information Needed",
 "Cancelld","Closed","Scrapped"] no-undo.

 
 
