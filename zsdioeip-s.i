/*  Variables used for sharing. Easier than shared vareters 
    but always update this library so all programs compile
*/    
def {1} shared var zi-PDSCfl        as logical initial no           no-undo.
def {1} shared var zi-PDSCrec       like pdsc.pdrecno               no-undo.
def {1} shared var zi-whse          like icsd.whse                  no-undo.
def {1} shared var zi-qty           like oeel.qtyord                no-undo.
def {1} shared var zi-custno        like oeel.custno                no-undo.
def {1} shared var zi-shipto        like arss.shipto                no-undo.
def {1} shared var zi-prod          like oeel.shipprod              no-undo.
def {1} shared var zi-price   as dec                                no-undo.
def {1} shared var zi-gpmarg  as dec                                no-undo.
def {1} shared var zi-level   as int                                no-undo. 
def {1} shared var zi-rebate  as dec                                no-undo. 
def {1} shared var zi-Spa     as dec                                no-undo.
def {1} shared var zi-Surcharge  as dec                             no-undo.

