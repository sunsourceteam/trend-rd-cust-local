/*vazs.las 1.1  05/19/00*/
/*h****************************************************************************
  INCLUDE      : vazs.las
  DESCRIPTION  : VA S.C. - Production Schedule Report
  USED ONCE?   : 
  AUTHOR       : SunSource 
  DATE WRITTEN :   
  CHANGES MADE :
******************************************************************************/
       
assign
    s-seqno       = t-vaeh.seqno
    s-wono        = if t-vaeh.rectyp = 1 then  
                       string(t-vaeh.wono,"9999999") + "-" +
                       string(t-vaeh.wosuf,"99")
                    else 
                       entry(1,t-vaeh.tagno,"-") + "-" + 
                       left-trim(entry(2,t-vaeh.tagno,"-"),"0") 
/*    s-wosuf       = string(t-vaeh.wosuf,"99") */
    s-custname    = t-vaeh.custname
    s-custpo      = t-vaeh.custpo
    s-whse        = t-vaeh.whse
    s-requestdt   = t-vaeh.requestdt
    s-requestdt3  = t-vaeh.requestdt3
    s-scheddt     = t-vaeh.scheddt 
    s-technician  = t-vaeh.technician
    s-rate        = t-vaeh.rate
    s-hours       = t-vaeh.hours
    s-units       = t-vaeh.stkqtyord
    s-qtybld      = t-vaeh.stkqtyship
    s-whselabel   = "Whse " + t-vaeh.whse + " Total:"
    s-techlabel   = if (p-showtechfl and p-srttype = "t") then  
                        "Technician " + t-vaeh.technician + " Total:"
                    else 
                    if (p-showtechfl and p-srttype = "d") then 
                        "Scheduled  " + "Day " + "Total:"
                    else 
                        "                     "
    s-totprodcost = t-vaeh.totprodcost                                      
    s-whsetotcnt  = s-whsetotcnt + 1                   
    s-whsetothrs  = s-whsetothrs + t-vaeh.hours        
    s-whsetotval  = s-whsetotval + t-vaeh.totprodcost  
    s-techtotcnt  = s-techtotcnt + 1                  
    s-techtothrs  = s-techtothrs + t-vaeh.hours       
    s-techtotval  = s-techtotval + t-vaeh.totprodcost  
    s-totalcnt    = s-totalcnt   + 1                   
    s-totalhrs    = s-totalhrs   + t-vaeh.hours        
    s-totalval    = s-totalval   + t-vaeh.totprodcost 
    s-kitprod     = t-vaeh.shipprod
    s-orderaltno  = t-vaeh.orderaltno
    s-orderaltsuf = t-vaeh.orderaltsuf
    s-ordertype   = t-vaeh.ordertype
    s-linealtno   = t-vaeh.linealtno

    s-stage       = if t-vaeh.stagecd = 1 and t-vaeh.rectyp = 1 then
                      "Ord"
                    else
                    if t-vaeh.stagecd = 3 and t-vaeh.rectyp = 1 then
                      "Prt"
                    else
                    if t-vaeh.stagecd = 5 and t-vaeh.rectyp = 1 then 
                      "Rcv" 
                    else
                    if t-vaeh.stagecd = 7 and t-vaeh.rectyp = 1 then
                      "Cls"
                    else
                    if t-vaeh.stagecd = 9 and t-vaeh.rectyp = 1 then
                       "Can"
                    else 
                    if t-vaeh.stagecd = 3 and t-vaeh.rectyp = 2 then
                       "Tdn"
                    else 
                    if t-vaeh.stagecd = 2 and t-vaeh.rectyp = 2 then
                       "Sch" 
                    else 
                    if t-vaeh.stagecd = 1 and t-vaeh.rectyp = 2 then
                       "Rcv" 
                    else    
                       " ".

