/* SX 55
  f-poetg.i 1.1 01/03/98 */
/* f-poetg.i 1.2 01/10/95 */
/*h****************************************************************************
  INCLUDE      : f-poetg.i
  DESCRIPTION  : Forms for F10-Print
  USED ONCE?   : yes (poetg.p)
  AUTHOR       : rhl
  DATE WRITTEN : 12/02/89
  CHANGES MADE :
    01/10/95 dww; TB# 17221 Position of suffix is wrong.
*******************************************************************************/
form
   g-pono                     colon 23
{f-help.i}
   "-"                           at 32
   g-posuf                       at 33 no-label
{f-help.i}
   skip(1)
   s-poprintfl                colon 23 label "Purchase Order"
   s-printernmpo              colon 37 label "Printer"
/*
validate(if input s-poprintfl = yes and (s-printernmpo = "" or
                                         input s-printernmpo = "E-Mail") 
                                         then false
  else {v-sasp.i s-printernmpo},
   {valmsg.gme 4035})
*/
{f-help.i}
    skip(1)
    s-autoprintfl              colon 23 label "Automatic Print"
   s-whereappfl               colon 23 label "Use Where Appropriate" 
   /* SX 55 */
   with frame f-poetg row 5 width 60 side-labels overlay centered
       title " Document Print ".

