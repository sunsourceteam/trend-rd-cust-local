/* n-oeizl2.i 1.1 01/03/98 */
/* n-oeizl2.i 1.6 01/29/94 */
/*h*****************************************************************************
  SX 55
  INCLUDE      : n-oeizl2.i
  DESCRIPTION  : Find Next/Prev for OEIZL when v-mode = 2
  USED ONCE?   : yes
  AUTHOR       : rhl
  DATE WRITTEN : 09/08/89
  CHANGES MADE :
    12/23/91 pap; TB#  5266 JIT/Line Due - reqship to promise dt
    01/29/94 kmw; TB# 11747 Make stage a range
    05/09/95 mtt; TB#  5180 When looking for a product-need kit ln
    03/14/96 tdd; TB# 20713 Inquiry not working properly
    11/04/99 gfk; TB# 26830 Improve performance by including whse in oeel find
       and remove it from the oeeh find. Also removed shipto from find since
       this routine is not used if custno/shipto is entered
    06/28/00 gfk; TB# e5650 Remove use-index clauses and separate find clause
       into different statements to utilize the indexes more effectively.
    08/08/02 lcb; TB# e14281 User Hooks
    10/02/02 emc; TB# e13512 OEIZL Searches are slow
*******************************************************************************/

/*e*****************************************************************************
     Selection Criteria for this procedure:
       v-mode ==> 2:
               Product: entered on the screen
    Kit Component Type: "o" (OE Order Line Items Only)
             Customer#: <blank> (0)
               CustPO#: <blank>
*******************************************************************************/
do:
confirm = yes.
/* If whse and a stagecd range for open orders, it will be much faster to go
    thru k-fill for open lines. */

if ("{1}" = "FIRST" and s-directionfl )  then
  assign v-xmode = "C".
else
if ("{1}" = "LAST" and not s-directionfl )  then
  assign v-xmode = "O".


if v-xmode = "C" then
  do:

if s-whse <> "" and s-stagecdh <= "3" then do:
   if s-transtype = "" or s-transtype = "bo" then
      find {1} oeelb  use-index k-whse where
         oeelb.cono       = g-cono and
         oeelb.xxc3       = s-whse and 
         oeelb.shipprod   = s-prod and
/*       oeel.statustype = "a" and
         oeel.invoicedt  = ?   and */
         (s-doonlyfl   = no   or oeelb.botype    = "d")
         and oeelb.seqno = 2
      no-lock no-error.
   else
      find {1} oeelb  use-index k-whse where
         oeelb.cono      = g-cono and
         oeelb.xxc3      = s-whse and  
         oeelb.shipprod  = s-prod and
/*       oeel.statustype = "a" and
         oeel.invoicedt  = ?   and */
/*       oeelb.transtype  = s-transtype and */
         (s-doonlyfl     = no   or oeelb.botype    = "d")
         and oeelb.seqno = 2
      no-lock no-error.

end.
else if s-whse = "" then do:
   if s-transtype = "" or s-transtype = "bo" then
      find {1} oeelb use-index k-prod where
         oeelb.cono     = g-cono and
         oeelb.shipprod = s-prod and
         (s-doonlyfl   = no   or oeelb.botype    = "d")
         and oeelb.seqno = 2
      no-lock no-error.
   else
      find {1} oeelb  where
         oeelb.cono     = g-cono and
         oeelb.shipprod = s-prod and
/*       oeelb.transtype = s-transtype and */
         (s-doonlyfl   = no   or oeelb.botype    = "d")
         and oeelb.seqno = 2

      no-lock no-error.
end.
else do:
   if s-transtype = "" or s-transtype = "bo" then
      find {1} oeelb use-index k-whse where
         oeelb.cono     = g-cono and
         oeelb.xxc3     = s-whse and 
         oeelb.shipprod = s-prod and
         (s-doonlyfl   = no   or oeelb.botype    = "d")
         and oeelb.seqno = 2

      no-lock no-error.
   else
      find {1} oeelb use-index k-whse  where
         oeelb.cono     = g-cono and
         oeelb.xxc3    = s-whse and 
         oeelb.shipprod = s-prod and
/*       oeelb.transtype = s-transtype and */
         (s-doonlyfl   = no   or oeelb.botype    = "d")
         and oeelb.seqno = 2

      no-lock no-error.
end.

if avail oeelb then do:
    find oeehb use-index k-oeehb where
         oeehb.cono     = g-cono        and
         oeehb.batchnm  = oeelb.batchnm and
         (oeehb.whse     = s-whse or 
          s-whse = "") and
         ("QU" /* oeehb.transtype */ = s-transtype or
          s-transtype = "" ) and
         (s-transtype ne "bo" ) and
         (s-takenby    = ""   or oeehb.takenby   = s-takenby) and
         oeehb.seqno = 2 and

          oeehb.sourcepros = "ComQu" and
          ((oeehb.stagecd <> 1 and oeehb.stagecd >= integer(s-stagecdl)) or
           (oeehb.stagecd = 1 and integer(s-stagecdl) = 0)) and
          (oeehb.stagecd <= integer(s-stagecdh)) and

         (s-promisedt  = ?    or oeehb.promisedt >= s-promisedt) and
         (s-custpo     = ""   or oeehb.custpo    = s-custpo)

         {oeizl.z99 &user_find_oeizl = "*"
                    &file            = "oeehb"}
    no-lock no-error.
    if not avail oeehb then confirm = no.
    else if s-transtype = "bo"  then confirm = no.
  end.
else
  confirm = no. /* no oeelb */
if avail oeelb then
  find zxinq where zxinq.present = true  no-lock no-error.
else
  find zxinq where zxinq.present = false no-lock no-error.

end.


if v-xmode = "C" and (not avail oeelb)  and
   ("{1}" = "first" or "{1}" = "next" ) then
  do:
  assign v-xmode = "O"
         confirm = yes.

  find first oeel where oeel.cono = g-cono and
                        oeel.whse = "" and
                        oeel.shipprod = ""    no-lock no-error.
  end.

if v-xmode = "O" then
do:

if s-whse <> "" and s-stagecdh <= "3" then do:
   if s-transtype = "" or s-transtype = "bo" then
      find {1} oeel  where
         oeel.cono       = g-cono and
         oeel.whse       = s-whse and
         oeel.shipprod   = s-prod and
         oeel.statustype = "a" and
         oeel.invoicedt  = ?   and
         (s-doonlyfl   = no   or oeel.botype    = "d")
      no-lock no-error.
   else
      find {1} oeel  where
         oeel.cono      = g-cono and
         oeel.whse      = s-whse and
         oeel.shipprod  = s-prod and
         oeel.statustype = "a" and
         oeel.invoicedt  = ?   and
         oeel.transtype  = s-transtype and
         (s-doonlyfl     = no   or oeel.botype    = "d")
      no-lock no-error.

end.
else if s-whse = "" then do:
   if s-transtype = "" or s-transtype = "bo" then
      find {1} oeel  where
         oeel.cono     = g-cono and
         oeel.shipprod = s-prod and
         (s-doonlyfl   = no   or oeel.botype    = "d")
      no-lock no-error.
   else
      find {1} oeel  where
         oeel.cono     = g-cono and
         oeel.shipprod = s-prod and
         oeel.transtype = s-transtype and
         (s-doonlyfl   = no   or oeel.botype    = "d")
      no-lock no-error.
end.
else do:
   if s-transtype = "" or s-transtype = "bo" then
      find {1} oeel  where
         oeel.cono     = g-cono and
         oeel.whse     = s-whse and
         oeel.shipprod = s-prod and
         (s-doonlyfl   = no   or oeel.botype    = "d")
      no-lock no-error.
   else
      find {1} oeel  where
         oeel.cono     = g-cono and
         oeel.whse     = s-whse and
         oeel.shipprod = s-prod and
         oeel.transtype = s-transtype and
         (s-doonlyfl   = no   or oeel.botype    = "d")
      no-lock no-error.
end.

if avail oeel then do:
    find oeeh use-index k-oeeh where
         oeeh.cono     = g-cono        and
         oeeh.orderno  = oeel.orderno  and
         oeeh.ordersuf = oeel.ordersuf and
         (s-transtype ne "bo" or (s-transtype = "bo" and oeeh.borelfl)) and
         (s-takenby    = ""   or oeeh.takenby   = s-takenby) and

         (oeeh.stagecd >= integer(s-stagecdl)) and
         (oeeh.stagecd <= integer(s-stagecdh)) and

         (s-promisedt  = ?    or oeeh.promisedt >= s-promisedt) and
         (s-custpo     = ""   or oeeh.custpo    = s-custpo)

         {oeizl.z99 &user_find_oeizl = "*"
                    &file            = "oeeh"}
    no-lock no-error.
    if not avail oeeh then confirm = no.
    else if s-transtype = "bo" and oeeh.borelfl = no then confirm = no.
  end.
else
  confirm = no.

if avail oeel then
  find zxinq where zxinq.present = true  no-lock no-error.
else
  find zxinq where zxinq.present = false no-lock no-error.


end.   

/* message v-xmode avail oeel "{1}". pause. */
if v-xmode = "o" and (not avail oeel) and
   ("{1}" = "Prev" or "{1}" = "Last" ) then
  do:
  assign v-xmode = "C"
         confirm = yes.



if s-whse <> "" and s-stagecdh <= "3" then do:
   if s-transtype = "" or s-transtype = "bo" then
      find last oeelb use-index k-whse  where
         oeelb.cono       = g-cono and
         oeelb.xxc3       = s-whse and 
         oeelb.shipprod   = s-prod and
/*       oeel.statustype = "a" and
         oeel.invoicedt  = ?   and */
         (s-doonlyfl   = no   or oeelb.botype    = "d")
         and oeelb.seqno = 2

      no-lock no-error.
   else
      find last oeelb  use-index k-whse where
         oeelb.cono      = g-cono and
         oeelb.xxc3      = s-whse and  
         oeelb.shipprod  = s-prod and
/*       oeel.statustype = "a" and
         oeel.invoicedt  = ?   and */
/*       oeelb.transtype  = s-transtype and */
         (s-doonlyfl     = no   or oeelb.botype    = "d")
         and oeelb.seqno = 2

      no-lock no-error.

end.
else if s-whse = "" then do:
   if s-transtype = "" or s-transtype = "bo" then
      find last oeelb  use-index k-prod where
         oeelb.cono     = g-cono and
         oeelb.shipprod = s-prod and
         (s-doonlyfl   = no   or oeelb.botype    = "d")
         and oeelb.seqno = 2

      no-lock no-error.
   else
      find last oeelb use-index k-prod where
         oeelb.cono     = g-cono and
         oeelb.shipprod = s-prod and
/*       oeelb.transtype = s-transtype and */
         (s-doonlyfl   = no   or oeelb.botype    = "d")
         and oeelb.seqno = 2

      no-lock no-error.
end.
else do:
   if s-transtype = "" or s-transtype = "bo" then
      find last oeelb  use-index k-whse where
         oeelb.cono     = g-cono and
         oeelb.xxc3     = s-whse and 
         oeelb.shipprod = s-prod and
         (s-doonlyfl   = no   or oeelb.botype    = "d")
         and oeelb.seqno = 2

      no-lock no-error.
   else
      find last oeelb use-index k-prod where
         oeelb.cono     = g-cono and
         oeelb.xxc3     = s-whse and 
         oeelb.shipprod = s-prod and
/*       oeelb.transtype = s-transtype and */
         (s-doonlyfl   = no   or oeelb.botype    = "d")
         and oeelb.seqno = 2

      no-lock no-error.
end.

if avail oeelb then do:
    find oeehb use-index k-oeehb where
         oeehb.cono     = g-cono        and
         oeehb.batchnm  = oeelb.batchnm and
         (oeehb.whse     = s-whse or 
          s-whse = "") and
         ("QU" /* oeehb.transtype */  = s-transtype or
          s-transtype = "" ) and
         (s-transtype ne "bo" ) and
         (s-takenby    = ""   or oeehb.takenby   = s-takenby) and

          ((oeehb.stagecd <> 1 and oeehb.stagecd >= integer(s-stagecdl)) or
           (oeehb.stagecd = 1 and integer(s-stagecdl) = 0)) and
          (oeehb.stagecd <= integer(s-stagecdh)) and


         (s-promisedt  = ?    or oeehb.promisedt >= s-promisedt) and
         (s-custpo     = ""   or oeehb.custpo    = s-custpo) and
         oeehb.seqno = 2  and
         oeehb.sourcepros = "ComQu" 
 

         {oeizl.z99 &user_find_oeizl = "*"
                    &file            = "oeehb"}
    no-lock no-error.
    if not avail oeehb then confirm = no.
    else if s-transtype = "bo"  then confirm = no.
  end.
else
  confirm = no. /* no oeelb */
if avail oeelb then
  find zxinq where zxinq.present = true  no-lock no-error.
else
  find zxinq where zxinq.present = false no-lock no-error.

end.





end.