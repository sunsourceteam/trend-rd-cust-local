/* List, Repl, Stnd and PriceType Update 
This ADHOC will update:
 1) Listprice
 2) Price Type
 3) Replcost
 4) Stndcost
for each ICSW with a specified Vendor
*/
def     shared var b-whse    like icsw.whse             no-undo.
def     shared var e-whse    like icsw.whse             no-undo.
def     shared var i-file      as c format "x(40)"      no-undo.
def     shared var i-aud       as c format "x(40)"      no-undo.
def     shared var i-excp      as c format "x(40)"      no-undo.
def     shared var g-operinit  like icsw.operinit       no-undo.
def     shared var g-cono      like icsw.cono           no-undo.
def     shared var i-dec       as i format "9"          no-undo.
def            var i-prod      like icsw.prod           no-undo.
def            var i-list      like icsw.listprice      no-undo.
def            var i-ptype     like icsw.pricetype      no-undo.
def            var i-repl      like icsw.replcost       no-undo.
def            var i-stnd      like icsw.stndcost       no-undo.
def            var i-vend      like icsw.arpvendno      no-undo.
def            var i-whse      like icsw.whse           no-undo.
def            var i-garb      as c                     no-undo.
def            var b-list      like icsw.listprice      no-undo.
def            var b-base      like icsw.baseprice      no-undo.
def            var b-repl      like icsw.replcost       no-undo.
def            var b-stnd      like icsw.stndcost       no-undo.
def            var b-ptype     like icsw.pricetype      no-undo.
def            var count       as i format ">>>,>>9"    no-undo.
def            var skip-stnd   as logical   init no     no-undo.

form
  "WHSE"                     at   1
  "PROD"                     at   7
  "Bef List"                 at  33
  "Aft List"                 at  48
  "Bef Base"                 at  63
  "Aft Base"                 at  78
  "Bef Repl"                 at  93
  "Aft Repl"                 at 108
  "Bef Stnd"                 at 123
  "Aft Stnd"                 at 138
  "Bef Ptype"                at 153
  "Aft Ptype"                at 164
  "----"                     at   1
  "------------------------" at   7
  "-------------"            at  33
  "-------------"            at  48
  "-------------"            at  63
  "-------------"            at  78
  "-------------"            at  93
  "-------------"            at 108
  "-------------"            at 123
  "-------------"            at 138
  "---------"                at 153
  "---------"                at 164
 with frame f-header no-box page-top no-labels down width 178.
 
form
  icsw.whse                  at   1
  icsw.prod                  at   7
  b-list                     at  33
  icsw.listprice             at  48
  b-base                     at  63
  icsw.baseprice             at  78
  b-repl                     at  93
  icsw.replcost              at 108
  b-stnd                     at 123
  icsw.stndcost              at 138
  b-ptype                    at 155
  icsw.pricetype             at 167
 with frame f-detail no-box no-labels down width 178.

def buffer b-icsw for icsw.

def stream EXCEP.
def stream AUD.
input               from value(i-file).
output stream AUD     to value(i-aud).
output stream EXCEP   to value(i-excp).

export stream EXCEP delimiter ","
  "WHSE" "PROD" "ARP VENDNO" "REQ STNDCOST" "CUR AVGCOST" "CUR STNDCOST".

put control "~033~046~1541~117".   /* landscape format */

view frame f-header.
repeat:
  import delimiter ","
    i-prod i-list i-repl i-stnd i-vend i-ptype i-garb.
  if i-prod = " " then next.
  assign skip-stnd = no.
  for each icsd where icsd.cono    = g-cono and 
                      icsd.salesfl = yes and
                      icsd.whse   >= b-whse and 
                      icsd.whse   <= e-whse 
                      no-lock:
    ICSWLOOP:
    for each icsw where icsw.cono      = g-cono and
                        icsw.whse      = icsd.whse and
                        icsw.prod      = i-prod    and
                        icsw.arpvendno = i-vend:
      
      find zicp where zicp.cono = g-cono and
                      zicp.whse = icsw.whse and
                      zicp.prod = icsw.prod and
                      zicp.changedt = TODAY
                      no-error.
      if avail zicp then
         assign zicp.avgcost   = icsw.avgcost
                zicp.baseprice = icsw.baseprice
                zicp.listprice = icsw.listprice
                zicp.replcost  = icsw.replcost
                zicp.stndcost  = icsw.stndcost
                zicp.transdt   = TODAY
                zicp.transtm   = substr(string(TIME,"HH:MM"),1,2) +
                                 substr(string(TIME,"HH:MM"),4,2)
                zicp.operinit  = g-operinit
                substring(zicp.user2,1,4) = icsw.pricetype.
      if not avail zicp then 
        do:
        create zicp.
        assign zicp.cono      = g-cono
               zicp.changedt  = TODAY
               zicp.avgcost   = icsw.avgcost
               zicp.baseprice = icsw.baseprice
               zicp.listprice = icsw.listprice
               zicp.replcost  = icsw.replcost
               zicp.stndcost  = icsw.stndcost
               zicp.transdt   = TODAY
               zicp.transtm   = substr(string(TIME,"HH:MM"),1,2) +
                                substr(string(TIME,"HH:MM"),4,2)
               zicp.whse      = icsw.whse
               zicp.prod      = icsw.prod
               zicp.operinit  = g-operinit
               substring(zicp.user2,1,4) = icsw.pricetype.
      end. /* not avail zicp */
      
      export stream AUD delimiter ","
         icsw.cono icsw.whse icsw.prod icsw.pricetype
         icsw.replcost icsw.replcostdt icsw.stndcost icsw.stndcostdt
         icsw.listprice icsw.baseprice icsw.priceupddt.
      if icsw.avgcost > i-stnd and (icsw.qtyonhand > 0 or
                                    icsw.qtyonord  > 0) then
        do:
        export stream EXCEP delimiter ","
          icsw.whse icsw.prod icsw.arpvendno i-stnd icsw.avgcost icsw.stndcost.
        assign skip-stnd = yes.
      end.
      assign b-list  = icsw.listprice
             b-base  = icsw.baseprice
             b-repl  = icsw.replcost
             b-stnd  = icsw.stndcost
             b-ptype = icsw.pricetype.
      assign icsw.stndcost = if skip-stnd = no then
                               ROUND(i-stnd,5)
                             else
                               icsw.stndcost
             icsw.stndcostdt = if skip-stnd = no then
                                 TODAY
                               else
                                 icsw.stndcostdt
             icsw.listprice  = if i-list = .001 then 
                                 ROUND(i-list,3)
                               else 
                                 ROUND(i-list,2)
             icsw.baseprice  = if i-list = .001 then 
                                 ROUND(i-list,3)
                               else
                                 ROUND(i-list,2)
             icsw.priceupddt = today
             icsw.pricetype  = i-ptype
             icsw.replcost   = ROUND(i-repl,i-dec)
             icsw.replcostdt = TODAY
             icsw.transdt    = TODAY
             icsw.transtm    = substr(string(TIME,"HH:MM"),1,2) +
                               substr(string(TIME,"HH:MM"),4,2).
      assign count = count + 1.
      display icsw.whse     
              icsw.prod     
              b-list        
              icsw.listprice
              b-base        
              icsw.baseprice
              b-repl        
              icsw.replcost 
              b-stnd        
              icsw.stndcost 
              b-ptype       
              icsw.pricetype
              with frame f-detail.
      down with frame f-detail.
              
      find b-icsw where b-icsw.cono       = 90 and
                        b-icsw.prod       = icsw.prod and
                        b-icsw.whse       = "main" and
                        b-icsw.arpvendno  = icsw.arpvendno  
                        no-error.
      if avail b-icsw then 
        do:
        export stream AUD delimiter ","
        b-icsw.cono b-icsw.whse b-icsw.prod b-icsw.pricetype
        b-icsw.replcost b-icsw.replcostdt b-icsw.stndcost b-icsw.stndcostdt
        b-icsw.listprice b-icsw.baseprice b-icsw.priceupddt.
        
        assign b-icsw.listprice  = icsw.listprice
               b-icsw.baseprice  = icsw.baseprice
               b-icsw.priceupddt = icsw.priceupddt
               b-icsw.pricetype  = icsw.pricetype
               b-icsw.replcost   = icsw.replcost
               b-icsw.replcostdt = icsw.replcostdt
               b-icsw.stndcost   = icsw.stndcost
               b-icsw.stndcostdt = icsw.stndcostdt
               b-icsw.operinit   = g-operinit
               b-icsw.transdt    = TODAY
               b-icsw.transtm    = substr(string(TIME,"HH:MM"),1,2) +
                                   substr(string(TIME,"HH:MM"),4,2).
      end. /* avail b-icsw */
      find icsc where icsc.catalog    = i-prod   and
                      icsc.vendno     = i-vend   and 
                      icsc.pricetype <> i-ptype
                      no-error.                                              
      if avail icsc then                                                     
        do:
        export stream AUD delimiter ","
        "ICSC" "1"  " "  icsc.catalog  icsc.pricetype  
              icsc.vendno.
        assign icsc.pricetype = i-ptype.
      end.
    end. /* each icsw */
  end. /* each icsd */
end. /* repeat */
display count.
input close.
output stream AUD close.
output stream EXCEP close.
hide frame f-header.
hide frame f-detail.
page.

/* DISPLAY THE EXCEPTIONS */
def var ex-whse     like icsw.whse         no-undo. 
def var ex-prod     like icsw.prod         no-undo.
def var ex-arpvend  as c                   no-undo.
def var ex-stnd     as c                   no-undo.
def var ex-cur-avg  as c                   no-undo.
def var ex-cur-stnd as c                   no-undo.
def var ex-count    as i format ">>>,>>9"  no-undo.

form
  "WHSE"                     at   1
  "PROD"                     at   7
  "VENDOR"                   at  33
  "Req StndCost"             at  48
  "Cur AvgCost"              at  63
  "Cur StndCost"             at  78
  "----"                     at   1
  "------------------------" at   7
  "------------"             at  33
  "------------"             at  48
  "------------"             at  63
  "------------"             at  78
 with frame f-ex-header page-top no-labels title "EXCEPTIONS"
 down width 132.
form
  ex-whse                    at   1
  ex-prod                    at   7
  ex-arpvend                 at  38
  ex-stnd                    at  48
  ex-cur-avg                 at  63
  ex-cur-stnd                at  78
 with frame f-ex-detail no-box no-labels down width 132.  

input from value(i-excp).
view frame f-ex-header.
repeat:
  import delimiter ","
    ex-whse ex-prod ex-arpvend ex-stnd ex-cur-avg ex-cur-stnd.
  if ex-prod = "PROD" then next.
  assign ex-count = ex-count + 1.
  display ex-whse
          ex-prod
          ex-arpvend
          ex-stnd
          ex-cur-avg
          ex-cur-stnd
          with frame f-ex-detail.
  down with frame f-ex-detail.
end.
if ex-count = 0 then
  display "NO EXCEPTIONS".
