/* zsdirebfinds.lpr 1.1 01/03/98 */
/* zsdirebfinds.lpr 1.1 08/04/95 */
/*h*****************************************************************************
   INCLUDE : zsdirebfinds.lpr
   DESCRIPTION : Rebate Security
   USED ONCE? : yes
   AUTHOR : gdf
   DATE WRITTEN : 08/04/95
   CHANGES MADE :
07/21/95 gdf; TB# 18812 7.0 Rebates Enhancement (C5a)
04/23/96 gp; TB# 21030 Rebate processing type 5 product category
04/23/96 gp; TB# 21016 Add customer rebate hierarchy
10/24/96 tdd; TB# 22060 Rebate not calculated if PDAO blank
*******************************************************************************/

/*o The following variables are pulled from the PDAO admin options for
   rebates (sasc). Each array represents a hit on the rebate hierarchy.
P = Product Specific
RT = Product Rebate Type
ST = Product Rebate Sub Type
PT = Product Price Type
PL = Product Line
PC = Product Category

C = Customer Specific
J = Shipto/Job Specific
CR = Customer Rebate Specific
W = Whse Specific
****************************************************************************/

/*tb 22060 10/24/96 tdd; Rebate not calculated if PDAO blank */
/*tb 21016 04/23/96 gp; Check PDAO option. Add customer hierarchy. */

if sasc.pdrebhierty = "c" then do:

/* CUSTOMER HIERARCHY */

  assign
/*e Product Customer Specific */
    v-pdrebsec[1] = if sasc.pdreblevlfl1 = yes and
                       sasc.pdrebjobfl = yes and
                       sasc.pdrebwhsefl = yes then yes
                    else no /* P/C/J/W */
    v-pdrebsec[2] = if sasc.pdreblevlfl1 = yes and
                       sasc.pdrebjobfl = yes then yes
                    else no /* P/C/J */
    v-pdrebsec[3] = if sasc.pdreblevlfl1 = yes and
                       sasc.pdrebwhsefl = yes then yes
                    else no /* P/C/W */
    v-pdrebsec[4] = if sasc.pdreblevlfl1 = yes then yes
                    else no /* P/C */

/*e Product Rebate Type Subtype Customer Specific */
    v-pdrebsec[5] = if sasc.pdreblevlfl2 = yes and
                       sasc.pdrebsubtyfl = yes and
                       sasc.pdrebjobfl = yes and
                       sasc.pdrebwhsefl = yes then yes
                    else no /* RT/ST/C/J/W*/
    v-pdrebsec[6] = if sasc.pdreblevlfl2 = yes and
                       sasc.pdrebsubtyfl = yes and
                       sasc.pdrebjobfl = yes then yes
                    else no /* RT/ST/C/J */
    v-pdrebsec[7] = if sasc.pdreblevlfl2 = yes and
                       sasc.pdrebsubtyfl = yes and
                       sasc.pdrebwhsefl = yes then yes
                    else no /* RT/ST/C/W */
    v-pdrebsec[8] = if sasc.pdreblevlfl2 = yes and
                       sasc.pdrebsubtyfl = yes then yes
                    else no /* RT/ST/C */

/*e Product Rebate Type Customer Specific */
    v-pdrebsec[9] = if sasc.pdreblevlfl2 = yes and
                       sasc.pdrebjobfl = yes and
                       sasc.pdrebwhsefl = yes then yes
                    else no /* RT/C/J/W */
    v-pdrebsec[10] = if sasc.pdreblevlfl2 = yes and
                        sasc.pdrebjobfl = yes then yes
                     else no /* RT/C/J */
    v-pdrebsec[11] = if sasc.pdreblevlfl2 = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* RT/C/W */
    v-pdrebsec[12] = if sasc.pdreblevlfl2 = yes then yes
                     else no /* RT/C */

/*e Product Price Type Customer Specific */
    v-pdrebsec[13] = if sasc.pdreblevlfl3 = yes and
                        sasc.pdrebjobfl = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* PT/C/J/W */
    v-pdrebsec[14] = if sasc.pdreblevlfl3 = yes and
                        sasc.pdrebjobfl = yes then yes
                     else no /* PT/C/J */
    v-pdrebsec[15] = if sasc.pdreblevlfl3 = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* PT/C/W */
    v-pdrebsec[16] = if sasc.pdreblevlfl3 = yes then yes
                     else no /* PT/C */

/*e Product Line Customer Specific */
    v-pdrebsec[17] = if sasc.pdreblevlfl4 = yes and
                        sasc.pdrebjobfl = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* PL/C/J/W */
    v-pdrebsec[18] = if sasc.pdreblevlfl4 = yes and
                        sasc.pdrebjobfl = yes then yes
                     else no /* PL/C/J */
    v-pdrebsec[19] = if sasc.pdreblevlfl4 = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* PL/C/W */
    v-pdrebsec[20] = if sasc.pdreblevlfl4 = yes then yes
                     else no /* PL/C */
 
/*e Product Category Customer Specific */
    v-pdrebsec[21] = if sasc.pdreblevlfl5 = yes and
                        sasc.pdrebjobfl = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* PC/C/J/W */
    v-pdrebsec[22] = if sasc.pdreblevlfl5 = yes and
                        sasc.pdrebjobfl = yes then yes
                     else no /* PC/C/J */
    v-pdrebsec[23] = if sasc.pdreblevlfl5 = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* PC/C/W */
    v-pdrebsec[24] = if sasc.pdreblevlfl5 = yes then yes
                     else no. /* PC/C */
 
  assign
/*e Product Customer Rebate Type Specific */
    v-pdrebsec[25] = if sasc.pdreblevlfl1 = yes and
                        sasc.pdcustrebfl = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* P/CR/W */
    v-pdrebsec[26] = if sasc.pdreblevlfl1 = yes and
                        sasc.pdcustrebfl = yes then yes
                     else no /* P/CR */

/*e Product Rebate Type Subtype Customer Rebate Type Specific */
    v-pdrebsec[27] = if sasc.pdreblevlfl2 = yes and
                        sasc.pdrebsubtyfl = yes and
                        sasc.pdcustrebfl = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* RT/ST/CR/W */
    v-pdrebsec[28] = if sasc.pdreblevlfl2 = yes and
                        sasc.pdrebsubtyfl = yes and
                        sasc.pdcustrebfl = yes then yes
                     else no /* RT/ST/CR */

/*e Product Rebate Type Customer Rebate Type Specific */
   v-pdrebsec[29] = if sasc.pdreblevlfl2 = yes and
                       sasc.pdcustrebfl = yes and
                       sasc.pdrebwhsefl = yes then yes
                    else no /* RT/CR/W */
   v-pdrebsec[30] = if sasc.pdreblevlfl2 = yes and
                       sasc.pdcustrebfl = yes then yes
                    else no /* RT/CR */

/*e Product Price Type Customer Rebate Type Specific */
   v-pdrebsec[31] = if sasc.pdreblevlfl3 = yes and
                       sasc.pdcustrebfl = yes and
                       sasc.pdrebwhsefl = yes then yes
                    else no /* PT/CR/W */
   v-pdrebsec[32] = if sasc.pdreblevlfl3 = yes and
                       sasc.pdcustrebfl = yes then yes
                    else no /* PT/CR */

/*e Product Line Customer Rebate Type Specific */
   v-pdrebsec[33] = if sasc.pdreblevlfl4 = yes and
                       sasc.pdcustrebfl = yes and
                       sasc.pdrebwhsefl = yes then yes
                    else no /* PL/CR/W */
   v-pdrebsec[34] = if sasc.pdreblevlfl4 = yes and
                       sasc.pdcustrebfl = yes then yes
                    else no /* PL/CR */

/*e Product Category Customer Rebate Type Specific */
   v-pdrebsec[35] = if sasc.pdreblevlfl5 = yes and
                       sasc.pdcustrebfl = yes and
                       sasc.pdrebwhsefl = yes then yes
                    else no /* PC/CR/W */
   v-pdrebsec[36] = if sasc.pdreblevlfl5 = yes and
                       sasc.pdcustrebfl = yes then yes
                    else no /* PC/CR */

/*e Product Specific */
   v-pdrebsec[37] = v-pdrebsec[3] /* P/W */
   v-pdrebsec[38] = v-pdrebsec[4] /* P */

/*e Product Rebate Type Sub Type Specific */
   v-pdrebsec[39] = v-pdrebsec[7] /* RT/ST/W */
   v-pdrebsec[40] = v-pdrebsec[8] /* RT/ST */

/*e Product Rebate Type Specific */
   v-pdrebsec[41] = v-pdrebsec[11] /* RT/W */
   v-pdrebsec[42] = v-pdrebsec[12] /* RT */

/* Product Price Type Specific */
   v-pdrebsec[43] = v-pdrebsec[15] /* PT/W */
   v-pdrebsec[44] = v-pdrebsec[16] /* PT */

/*e Product Line Specific */
   v-pdrebsec[45] = v-pdrebsec[19] /* PL/W */
   v-pdrebsec[46] = v-pdrebsec[20] /* PL */

/*e Product Category Specific */
   v-pdrebsec[47] = v-pdrebsec[23] /* PC/W */
   v-pdrebsec[48] = v-pdrebsec[24]. /* PC */

 end. /* end if sasc.rebhierty = "c" */

/*tb 22060 10/24/96 tdd; Rebate not calculated if PDAO blank */
/*tb 21016 04/23/96 gp; Check PDAO option */
else do:

/*d LEVEL HIERARCHY */

  assign
/*e Product Specific */
    v-pdrebsec[1] = if sasc.pdreblevlfl1 = yes and
                       sasc.pdrebjobfl = yes and
                       sasc.pdrebwhsefl = yes then yes
                    else no /* P/C/J/W */
    v-pdrebsec[2] = if sasc.pdreblevlfl1 = yes and
                       sasc.pdrebjobfl = yes then yes
                    else no /* P/C/J */
    v-pdrebsec[3] = if sasc.pdreblevlfl1 = yes and
                       sasc.pdrebwhsefl = yes then yes
                    else no /* P/C/W */
    v-pdrebsec[4] = if sasc.pdreblevlfl1 = yes then yes
                    else no /* P/C */
    v-pdrebsec[5] = if sasc.pdreblevlfl1 = yes and
                       sasc.pdcustrebfl = yes and
                       sasc.pdrebwhsefl = yes then yes
                    else no /* P/CR/W */
    v-pdrebsec[6] = if sasc.pdreblevlfl1 = yes and
                       sasc.pdcustrebfl = yes then yes
                    else no /* P/CR */
    v-pdrebsec[7] = v-pdrebsec[3] /* P/W */
    v-pdrebsec[8] = v-pdrebsec[4] /* P */

/*e Product Rebate Type / Sub Type Specific */
    v-pdrebsec[9] = if sasc.pdreblevlfl2 = yes and
                       sasc.pdrebsubtyfl = yes and
                       sasc.pdrebjobfl = yes and
                       sasc.pdrebwhsefl = yes then yes
                     else no /* RT/ST/C/J/W*/
    v-pdrebsec[10] = if sasc.pdreblevlfl2 = yes and
                        sasc.pdrebsubtyfl = yes and
                        sasc.pdrebjobfl = yes then yes
                     else no /* RT/ST/C/J */
    v-pdrebsec[11] = if sasc.pdreblevlfl2 = yes and
                        sasc.pdrebsubtyfl = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* RT/ST/C/W */
    v-pdrebsec[12] = if sasc.pdreblevlfl2 = yes and
                        sasc.pdrebsubtyfl = yes then yes
                     else no /* RT/ST/C */
    v-pdrebsec[13] = if sasc.pdreblevlfl2 = yes and
                        sasc.pdrebsubtyfl = yes and
                        sasc.pdcustrebfl = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* RT/ST/CR/W */
    v-pdrebsec[14] = if sasc.pdreblevlfl2 = yes and
                        sasc.pdrebsubtyfl = yes and
                        sasc.pdcustrebfl = yes then yes
 
                     else no /* RT/ST/CR */
    v-pdrebsec[15] = v-pdrebsec[11] /* RT/ST/W */
    v-pdrebsec[16] = v-pdrebsec[12] /* RT/ST */

/*e Product Rebate Type Specific */
    v-pdrebsec[17] = if sasc.pdreblevlfl2 = yes and
                        sasc.pdrebjobfl = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* RT/C/J/W */
    v-pdrebsec[18] = if sasc.pdreblevlfl2 = yes and
                        sasc.pdrebjobfl = yes then yes
                     else no /* RT/C/J */
    v-pdrebsec[19] = if sasc.pdreblevlfl2 = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* RT/C/W */
    v-pdrebsec[20] = if sasc.pdreblevlfl2 = yes then yes
                     else no /* RT/C */
    v-pdrebsec[21] = if sasc.pdreblevlfl2 = yes and
                        sasc.pdcustrebfl = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* RT/CR/W */
    v-pdrebsec[22] = if sasc.pdreblevlfl2 = yes and
                        sasc.pdcustrebfl = yes then yes
                     else no /* RT/CR */
    v-pdrebsec[23] = v-pdrebsec[19] /* RT/W */
    v-pdrebsec[24] = v-pdrebsec[20]. /* RT */

  assign
/*e Product Price Type Specific */
    v-pdrebsec[25] = if sasc.pdreblevlfl3 = yes and
                        sasc.pdrebjobfl = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* PT/C/J/W */
    v-pdrebsec[26] = if sasc.pdreblevlfl3 = yes and
                        sasc.pdrebjobfl = yes then yes
                     else no /* PT/C/J */
    v-pdrebsec[27] = if sasc.pdreblevlfl3 = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* PT/C/W */
    v-pdrebsec[28] = if sasc.pdreblevlfl3 = yes then yes
                     else no /* PT/C */
    v-pdrebsec[29] = if sasc.pdreblevlfl3 = yes and
                        sasc.pdcustrebfl = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* PT/CR/W */
    v-pdrebsec[30] = if sasc.pdreblevlfl3 = yes and
                        sasc.pdcustrebfl = yes then yes
                     else no /* PT/CR */
    v-pdrebsec[31] = v-pdrebsec[27] /* PT/W */
    v-pdrebsec[32] = v-pdrebsec[28] /* PT */

/*e Product Line Specific */
    v-pdrebsec[33] = if sasc.pdreblevlfl4 = yes and
                        sasc.pdrebjobfl = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* PL/C/J/W */
    v-pdrebsec[34] = if sasc.pdreblevlfl4 = yes and
                        sasc.pdrebjobfl = yes then yes
                     else no /* PL/C/J */
    v-pdrebsec[35] = if sasc.pdreblevlfl4 = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* PL/C/W */
    v-pdrebsec[36] = if sasc.pdreblevlfl4 = yes then yes
                     else no /* PL/C */
    v-pdrebsec[37] = if sasc.pdreblevlfl4 = yes and
                        sasc.pdcustrebfl = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* PL/CR/W */
    v-pdrebsec[38] = if sasc.pdreblevlfl4 = yes and
                        sasc.pdcustrebfl = yes then yes
                     else no /* PL/CR */
    v-pdrebsec[39] = v-pdrebsec[35] /* PL/W */
    v-pdrebsec[40] = v-pdrebsec[36] /* PL */

/*e Product Category Specific */
/*tb 21030 04/23/96 gp; Change level 4 to 5 */
    v-pdrebsec[41] = if sasc.pdreblevlfl5 = yes and
                        sasc.pdrebjobfl = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* PC/C/J/W */
    v-pdrebsec[42] = if sasc.pdreblevlfl5 = yes and
                        sasc.pdrebjobfl = yes then yes
                     else no /* PC/C/J */
    v-pdrebsec[43] = if sasc.pdreblevlfl5 = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* PC/C/W */
    v-pdrebsec[44] = if sasc.pdreblevlfl5 = yes then yes
                     else no /* PC/C */
    v-pdrebsec[45] = if sasc.pdreblevlfl5 = yes and
                        sasc.pdcustrebfl = yes and
                        sasc.pdrebwhsefl = yes then yes
                     else no /* PC/CR/W */
    v-pdrebsec[46] = if sasc.pdreblevlfl5 = yes and
                        sasc.pdcustrebfl = yes then yes
                     else no /* PC/CR */
    v-pdrebsec[47] = v-pdrebsec[43] /* PC/W */
    v-pdrebsec[48] = v-pdrebsec[44]. /* PC */

  end. /* end if sasc.pdrebhierty = "l" */







