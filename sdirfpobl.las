
/* sdirfpobl.las 1.1 01/27/98 */
/*h****************************************************************************
  PROCEDURE      : sdirfpobl.las
  DESCRIPTION    :
  AUTHOR         : aa
  DATE WRITTEN   : 01/22/98
  CHANGES MADE   :
    01/22/98 aa; TB# 23693 IB Module
    05/10/99 jbs; TB# 23693 Moved to std 9.0
******************************************************************************/

/*ibrfpobl.las*/

    {w-poel.i {&buf}poelb.pono {&buf}poelb.posuf {&buf}poelb.lineno no-lock}

    assign
         s-shipprod   = {&buf}poelb.shipprod
         s-nonstockty = if avail poel then poel.nonstockty else "".

    if s-nonstockty = "" then do:
        {w-icsp.i s-shipprod no-lock}
        {w-icsw.i s-shipprod g-whse no-lock}
    end.

    assign
         s-receivety  = if {&buf}poelb.qtyrcv ne 0 then "y" else "n"
         s-commentty  = if avail poel and poel.commentfl = yes then "c"
                        else ""
         s-lineno     = {&buf}poelb.lineno
         s-notesfl    = if not can-do("n,r",s-nonstockty)
                        then
                            if avail icsp then icsp.notesfl
                            else ""
                        else ""
         s-qtyord     = if avail poel then poel.qtyord else 0
         s-unit       = {&buf}poelb.unit
         s-qtyrcv     = {&buf}poelb.qtyrcv
         s-cancelfl   = {&buf}poelb.cancelfl
         s-type       = if s-nonstockty = "" and avail icsw then
                            if icsw.serlottype = "l" then
                                "Lot"
                            else
                            if icsw.serlottype = "s" and v-icsnpofl = yes
                            then
                                "Ser"
                            else ""
                        else ""
         s-type       = if s-nonstockty = "" and avail icsw and icsw.wmfl = yes
                        then
                            if s-type ne "" then s-type + "/WM"
                            else "WM"
                        else s-type
         s-proddesc   = if not avail poel then v-invalid
                        else if can-do("n,r",s-nonstockty)
                            then poel.proddesc
                        else if avail icsp then
                            icsp.descrip[1] + " " + icsp.descrip[2]
                        else v-invalid.




