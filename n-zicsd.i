confirm = true.
find {1} icsd where icsd.cono = g-cono no-lock no-error.  

  
assign s-rissues = 0
       s-nissues = 0
       s-dissues = 0 
       s-avail   = 0
       s-onhand  = 0
       s-lead    = 0
       s-onord   = 0
       s-cost    = 0
       s-lastdt = ?.
 



if avail icsd then
  do:
  find icsw where icsw.cono = g-cono and 
                  icsw.whse = icsd.whse and
                  icsw.prod = g-prod no-lock no-error.
  if avail icsw then                
    do:
    s-onhand = icsw.qtyonhand.
    s-cost  = icsw.avgcost.
    s-lead = icsw.leadtmavg.
    s-onord = icsw.qtyonorder.
    
    s-avail = icsw.qtyonhand - icsw.qtycommit - icsw.qtyreservd.
    s-lastdt = icsw.lastinvdt.
    find icswu where icswu.cono = g-cono  and
                     icswu.whse = icsd.whse and
                     icswu.prod = icsw.prod no-lock no-error.
    if avail icswu then
      do zx = 2 to 13:
        s-rissues = s-rissues + icswu.normusage[zx].
      end.                  

    end.
 
 
  
  for each icenl use-index k-icenl
                 where icenl.cono = g-cono and
                       icenl.typecd = "n" and
                       icenl.whse = icsd.whse and
                       icenl.prod = g-prod and
                       icenl.ordtype = "o" and
                       icenl.entrytype = false and
                       icenl.quantity <> 0 no-lock:
           
  if icenl.postdt > (today - 365) then
    do:
    s-nissues = s-nissues + (icenl.quantity * -1).
    if icenl.postdt > s-lastdt or s-lastdt = ? then
       s-lastdt = icenl.postdt.
    
    end.
         
  end.       

    
  for each icenl use-index k-icenl
                 where icenl.cono = g-cono and
                       icenl.typecd = "d" and
                       icenl.whse = icsd.whse and
                       icenl.prod = g-prod and
                       icenl.ordtype = "o" and
                       icenl.entrytype = false and
                       icenl.quantity <> 0 no-lock:
           
  if icenl.postdt > (today - 365) then
    do:
    s-dissues = s-dissues + (icenl.quantity * -1).
    if icenl.postdt > s-lastdt or s-lastdt = ? then
       s-lastdt = icenl.postdt.
    
    end.
         
  end.       

 if s-rissues = 0 and s-nissues = 0 and s-dissues = 0 and
   s-avail = 0 and s-onhand = 0 and s-onord = 0 then
   confirm = no.
 
 end.
