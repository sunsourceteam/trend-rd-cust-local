/* poebustk.p 1.1 01/02/98 */
/* poebustk.p 1.7 05/23/95 */
/*h*****************************************************************************
  PROCEDURE      : poebustk.p
  DESCRIPTION    : Handheld Receiving interface to POEI work DB
  AUTHOR         : Sunsource
  DATE WRITTEN   : 02/24/04
  CHANGES MADE   : 03/28/2006 dkt Allow new bin choice stored in zzbin

  TAH 10/17/11   : fix to period based on gl fiscal period.
*******************************************************************************/

define input parameter hr-operinit         like oeeh.operinit.
define input parameter hr-updtfl           as logical.

/* INSPECT */
define buffer b-zzbin for zzbin.
/* INSPECT */


def var zx-l as logical                   no-undo. 
def var zx-x as int                       no-undo.

/* TAH MidMonth Close  */
{g-gl.i}  

{poei.lva}
{g-rpt.i}
{g-rptctl.i}
{g-poebustk.i "new shared" "/*" "/*"}
{g-jump.i}
/* SX 55
def new shared var v-pocrctfl     like sasoo.pocrctfl        no-undo.

/*tb e8651 09/29/02 n-jk; Core Tracking and Accounting */
def new shared var v-pocostcorechyes    like sasc.pocostcorechyes   no-undo.
def new shared var v-pocostcorechno     like sasc.pocostcorechno    no-undo.
def new shared var v-icvendcost   like sasc.icvendcost       no-undo.
*/ 
def new shared var v-binloc like wmsb.binloc no-undo.

def var v-ms       as i       init 4        no-undo.
def var v-ds       as i       init 1        no-undo.
def var v-leapyr   as dec format "99.99"    no-undo.

/*tb 23693 11/13/97  aa; Flag to know if user purchased IB module */
def         var     v-modibfl       like sasa.modibfl           no-undo.
def         var     v-putscreen     as   c format "x(78)"       no-undo.
    
def new shared frame f-poei.
def shared  var     v-jumpfl        as logical                  no-undo.
def var o-secure                    as i                        no-undo.
def var v-poruproc                  as c init "poru"            no-undo.
def var v-porusecure                as i                        no-undo.
    
/*tb e8651 01/16/03 n-jk; Core Tracking */
def var lCoreInstalled              as logic                    no-undo.
def var cCoreInstalled              as char                     no-undo.
def var v-hascorefl                 as logic                    no-undo.
    
/* Corrections */
def var lCorrectionFl               as log                      no-undo.
def var dQtyChange                  as dec                      no-undo.
def var dCrctTotQty                 as dec                      no-undo.
def var iCrctSerLot                 as int                      no-undo.
def var cPOCrctReason               as char                     no-undo.
def var lCrctExists                 as log                      no-undo.
def var lCorrectionAllowed          as log                      no-undo.
def var cHoldTitle                  as char                     no-undo.
def var lCrctLineFound              as log                      no-undo.

def buffer          b-poeh          for poeh.
def buffer          b-icsd          for icsd.
def buffer          b-sasa          for sasa.
/* SX 55
def var s-adjbofl as logical                 no-undo.
*/
def new shared var s-flglbl  as logical      no-undo.
def var hld-currproc like g-currproc         no-undo.
def shared var v-saso-whse  like sasoo.whse  no-undo.
def shared var x-saso-whsegrp as character   no-undo.
def shared var x-f10-whse   like sasoo.whse  no-undo.

def var s-rcvwhse    like icsd.whse          no-undo.
def var zx-okfl        as logical            no-undo.
def var zx-error       as integer            no-undo.
def var zx-suff        like poeh.posuf       no-undo.
def new shared var s-ibprinter  like sapb.printernm  no-undo.
def var o-ibprinter  like sapb.printernm     no-undo.

def new shared var s-rcptno as integer initial 1 no-undo.
def buffer b-zsdiputid  for zsdiputid.
def buffer x-icsd for icsd.
form
  s-updatefl                        colon 43 label "Update"
  s-rcvwhse                         colon 43 label "Receiving Whse"
  s-backfl                          colon 43 label "Background"
    help "Run Backorder Processing in the Background"
  s-printernm                       colon 43
    {f-help.i}
      label "Printer For Allocation and Receipt Report"
  s-pckprinter                    colon 43 label 
    "Override Printer For Pick Tickets"
      {f-help.i}
  s-ibprinter                     colon 43 label
    "Override Printer For Barcode Labels"      {f-help.i}
  s-routefl                         colon 43 label
    "Print Order: (R)oute or (O)rder"
  s-flglbl                          colon 43 label
    "Print Barcode Labels Y/N?"
  s-adjbofl                         colon 43 label
    "For Corrections, Adjust Any Back Orders"
  s-rcptno                          colon 43 label
    "Number of Receipts Reports to Print" 
  with frame f-poeiu overlay row 6 centered side-labels title
    " Update Batch Po's By Receiving Whse ".
 
form
  s-fprintfl label "Print to File?"
  with frame x row 13 column 27 side-labels overlay.

assign b-whse     = g-whse 
       p-update   = hr-updtfl 
       g-operinit = hr-operinit. 

if not p-update then do:
  bell.
  message "Updating was not Selected - Please Check Parameters.".
  pause.
  return.
end.

/*o Check for level security for the operator */
/*tb 13155 09/29/93 tdd; Added security check */
/*tb 13379 10/22/93 tdd; Use new security check include */
{securchk.gpr &secure = 3}
pause 0 before-hide.

{sasa.gfi &lock = "no"}
{sasc.gfi &lock = "no"}
{sasoo.gfi &oper = g-operinit
           &lock = "no"}

/* LOAD NON-DATE PARAMETERS FROM SAPB */
assign
  p-postdt = today
  v-perin  = string(month(today),"99") +
             substring(string(year(today),"9999"),3,2).            
{p-rptper.i}
p-period = v-perout.

/* TAH MidMonth Close  */
run period.p.                  
assign p-period = g-gldefper.  
/* TAH MidMonth Close  */
                        


/*tb 19724 05/06/99 gwk; Split SM/GL cost into two options */
assign {wlsasa.gas}   
  v-modwmfl      = if avail sasa and sasa.modwmfl and
                      avail sasc and sasc.wmintfl then yes
                   else no
  v-whse         = ""
  v-divno        = 0
  v-pdvenqtyfl   = sasc.pdvenqtyfl
  v-poallfl      = sasc.poallfl
  v-poqtyrcvfl   = sasc.poqtyrcvfl
  v-pocapaddfl   = sasc.pocapaddfl
  v-pocapdiscfl  = sasc.pocapdiscfl
  v-pocapfl      = sasc.pocapfl
  v-pocostfl     = sasc.pocostfl
  v-powodist     = sasc.powodist
  v-pocostfl     = sasc.pocostfl
  v-porcvnofl    = sasc.porcvnofl
  v-oeautofity   = sasc.oeautofity
  v-oepickfl     = sasc.oepickfl
  v-oeautofity   = sasc.oeautofity
  v-oebofillfl   = sasc.oebofillfl
  v-icrollcostfl = sasc.icrollcostfl
  v-icglcost     = sasc.icglcost
  v-icincaddsm   = sasc.icincaddsm
  v-icincaddgl   = sasc.icincaddgl
  v-icglbsty     = sasc.icglbsty
  v-icfifofl     = sasc.icfifofl
  v-icsnpofl     = sasc.icsnpofl
  v-wmmultfl     = sasc.wmmultfl
  v-wmdelfl      = sasc.wmdelfl
  v-smvendrebfl  = sasc.smvendrebfl
  v-pdwhsefl     = sasc.pdwhsefl
  s-backfl       = sasoo.backfl
  v-updglty      = sasoo.updglpoty
  v-pocrctfl     = sasoo.pocrctfl
  /*tb e8651 09/29/02 n-jk; Core Tracking and Accounting */
  v-pocostcorechyes = sasc.pocostcorechyes
  v-pocostcorechno  = sasc.pocostcorechno
  v-icvendcost      = sasc.icvendcost.
                                     
/*tb 23693 11/13/97  aa; Flag to know if user purchased IB module */
do for b-sasa:
  {sasa.gfi &lock = "no" &b = "b-"}
  v-modibfl = if avail b-sasa then b-sasa.modibfl
              else no.
end. /*e for sasa */

run RFloop.                        

main:
repeat on endkey undo, next main
       on error  undo, next main:

/*d Allow update of the printing parameters */
assign   hld-currproc = g-currproc
         s-printernm  = g-printernm
         s-updatefl   = no
         s-adjbofl    = yes
         s-flglbl     = yes 
         s-rcvwhse    = v-saso-whse. 

if substring(s-rcvwhse,1,1) = "&" then 
  icsdloop:
  do while true with frame f-poeiu
           on endkey undo, leave main
           on error  undo, leave main:
    update
      s-rcvwhse    
    with frame f-poeiu
    editing:
      readkey.
      if {k-cancel.i} or {k-jump.i} then do: 
        assign g-currproc = hld-currproc
               g-ourproc  = hld-currproc
               g-exitproc = hld-currproc.
        hide frame f-poeiu no-pause.
        return.
      end.    
      if {k-after.i} then do:
        if frame-field = "s-rcvwhse" then do:
          do for icsd :
            assign s-rcvwhse = input s-rcvwhse.
            if not can-do(x-saso-whsegrp,s-rcvwhse) then do:
              bell.
              message "Warehouse To Receive For Not in Whse Group - " s-rcvwhse.
              next-prompt s-rcvwhse.
              next.
            end.
            {w-icsd.i s-rcvwhse no-lock} 
            if not avail icsd then do:
              bell.
              message
              "Warehouse To Receive For Not Set Up in Warehou~se Setup - ICSD" 
              s-rcvwhse.
              next-prompt s-rcvwhse.
              next.
            end.
            else do:
              apply lastkey.
              leave icsdloop.
            end.
          end.
        end.
      end. /* if {k-after.i} */
      apply lastkey.
    end. /* editing */
  end.
        
  do for icsd:
    {w-icsd.i g-whse no-lock}
    s-pckprinter = if avail icsd then icsd.printernm[1]
                   else "".
  end.
  display s-rcvwhse with frame f-poeiu.
  /*tb 19803 12/19/95 sbl(CGA); Force background printer if SASSR set
       to print background only. */

  /* setup for notes record specified printers */
  find first notes use-index k-notes
    where notes.cono         = g-cono and 
          notes.notestype    = "zz" and
          notes.primarykey   = "hher-" + g-operinit and
          notes.secondarykey = " "
          no-lock no-error.
  if avail notes then do:
    assign s-pckprinter = notes.noteln[1]
           s-ibprinter  = notes.noteln[2].
  end.
  run sdipickfl (input g-whse,
                 input-output v-oepickfl).
     
  func10loop:
  do for sassr while true with frame f-poeiu
                     on endkey undo, leave main
                     on error  undo, leave main:
    /*d Get sassr to see if force to background */
    {w-sassr.i "'oeepb'" no-lock}
    update
      s-updatefl
      s-backfl     when v-oeautofity ne "n" and sassr.backfl = no
      s-printernm  when v-oeautofity ne "n"
      s-pckprinter when v-oeautofity ne "n" and v-oepickfl 
      s-ibprinter 
      s-routefl    when v-oeautofity ne "n" and v-oepickfl = yes
      s-flglbl 
      s-adjbofl    when lCrctExists = true
      s-rcptno     /** validate( s-rcptno >= 1 and s-rcptno <= 9,"You may print between 1 and 9 copies of the Receipt Report")    **/
    with frame f-poeiu
    editing:
      readkey.
      if {k-cancel.i} or {k-jump.i} then do: 
        assign g-currproc = hld-currproc
               g-ourproc  = hld-currproc
               g-exitproc = hld-currproc.
        hide frame f-poeiu no-pause.
        return.
      end.    
      if {k-after.i} then do:
        /*d Validate the back order fill exception and pick ticket printers */
        if frame-field = "s-rcptno" then do:
          if input s-rcptno > 9 or input s-rcptno < 1 then do:
             message "Print between 1 and 9 Receipt Reports".         
             next-prompt s-rcptno.                       
             next.                                          
          end.
        end.
        
        if frame-field = "s-printernm" then do:
          if input s-printernm = "" then do:
            s-printernm = "vid".
            display s-printernm.
          end.
          else do:
            {w-sasp.i "input s-printernm" no-lock}
            if not avail sasp then do:
              s-fprintfl = yes.
              update s-fprintfl with frame x.
              hide frame x.
              if s-fprintfl = no then do:
                next-prompt s-printernm.
                next.
              end.
            end.
            /*tb 4525 02/23/93 rhl; Printer names for BO fill */
            if avail sasp and sasp.ptype = "f" then do:
              run err.p(1134).
              next.
            end.
          end.
        end. /* if frame-field = "s-printernm" */

        if frame-field = "s-pckprinter" then do:
          if input s-pckprinter = "" then do:
            s-pckprinter = "vid".
            display s-pckprinter.
          end.
          else do:
            {w-sasp.i "input s-pckprinter" no-lock}
            if not avail sasp then do:
              s-fprintfl = yes.
              update s-fprintfl with frame x.
              hide frame x.
              if s-fprintfl = no then do:
                next-prompt s-pckprinter.
                next.
              end.
            end.
            /*tb 4525 02/23/93 rhl; Printer names for BO fill */
            if avail sasp and sasp.ptype = "f" then do:
              run err.p(1134).
              next.
            end.
          end.
        end. /* if frame-field = "s-pckprinter" */
            
        if (input s-updatefl = yes and input s-ibprinter ne "") or 
          (frame-field = "s-ibprinter" and input s-ibprinter ne "")
        then do:           
          {w-sasp.i "input s-ibprinter" no-lock}
          /* if avail sasp and not sasp.pcommand begins "sh" then do: */
          /**
          if avail sasp and not sasp.pcommand begins "sh" then do:     
            if avail sasp and sasp.prodlbl = "" then do: 
            run err.p(1134).
            next-prompt s-ibprinter.
            next.
          end.
          else 
          **/
          if not avail sasp then do:
            run err.p(1134).
            next-prompt s-ibprinter.
            next.
          end.
          if trim(input s-ibprinter) = "vid" then do:
            message "Vid may not used for labels".
            next-prompt s-ibprinter.
            next.
          end.
          else message "".
        end. /* if frame-field = "s-ibprinter" */
        if input s-updatefl = yes then 
        do for icsd :
          {w-icsd.i s-rcvwhse no-lock} 
          if not avail icsd then do:
            bell.
            message
              "Warehouse To Receive For Not Set Up in Warehouse Setup - ICSD" 
              s-rcvwhse.
            next-prompt s-rcvwhse.
            next.
          end.
        end.
        /*d If a journal is already open, give an error & return */
        if g-jrnlno ne 0 and input s-updatefl = yes then do:
          message "A journal is already open - please close"
            "journal#" g-jrnlno "from" g-jrnlproc "process..".
          next.
        end.
      end. /* if {k-after.i} */
      apply lastkey.
    end. /* editing */

    if s-printernm = "" then do:
      run err.p(2100).
      next-prompt s-printernm.
      next.
    end.
            
    if s-pckprinter = "" then do:
      run err.p(2100).
      next-prompt s-pckprinter.
      next.
    end.
    if s-ibprinter = "vid" then do:
      run err.p(2100).
      next-prompt s-ibprinter.
      next.
    end.
    /*d Make sure printer name is not 'vid' when running in background */
    /*tb 15094 10/25/94 jrb; Don't allow 'vid' when run in background */
    if (s-printernm = "vid" or s-pckprinter = "vid") and
       (s-backfl = yes or opsys = "win32":u) then do:
      run err.p(5000).
      if s-printernm = "vid"
        then next-prompt s-printernm.
        else next-prompt s-pckprinter.
      next.
    end.    /* if s-printernm = "vid" and s-backfl = yes */
    leave.
  end. /* do for sassr while true */
  hide frame f-poeiu no-pause.

  {poreceiveupdate.i}

  
procedure set-openinit:
/*o This internal procedures sets the poeh.openinit field with the
    operators inititals.*/
/*e Parameters Name        Description                  Req/Opt
    v-pono      PO number                       Req
    v-posuf     PO suffix                       Req     */
         
def buffer poeh for poeh.
def input param v-pono  like poeh.pono        no-undo.
def input param v-posuf like poeh.posuf       no-undo.
               
{ipbegin.gpr &proc-name = "set-openinit"}
{poeh.gfi &pono     = "v-pono"
          &posuf    = "v-posuf"
          &lock     = "exclusive"}
 
if avail poeh then
  assign poeh.openinit = g-operinit.
{ipend.gpr &proc-name = "set-openinit"}
end procedure.
  
procedure clear-openinit:
/*o This internal procedures clears the poeh.openinit field with the
    operators inititals.*/
/*e Parameters
    Name        Description                  Req/Opt
    v-pono      PO number                       Req
    v-posuf     PO suffix                       Req     */
         
def buffer poeh for poeh.
def input param v-pono  like poeh.pono        no-undo.
def input param v-posuf like poeh.posuf       no-undo.
                     
{ipbegin.gpr &proc-name = "clear-openinit"}
{poeh.gfi &pono     = "v-pono"
          &posuf    = "v-posuf"
          &lock     = "exclusive"}
if avail poeh then
  assign poeh.openinit = "".
{ipend.gpr &proc-name = "clear-openinit"}
end procedure.

procedure sdipickfl:
define input        parameter xip-whse like icsw.whse no-undo.
define input-output parameter xip-oepickfl as logical no-undo.
define var xip-whsestring as character no-undo.
define var xip-x          as integer   no-undo.
define buffer xip-notes for notes.

find first xip-notes where xip-notes.cono = g-cono and
                           xip-notes.notestype = "zz" and 
                           xip-notes.primarykey = "oepickfl override" 
                           no-lock no-error.         
do xip-x = 1 to 16:
  if xip-notes.noteln[xip-x] <> "" then
    xip-whsestring = xip-whsestring + left-trim(xip-notes.noteln[xip-x]).
end.
if index(xip-whsestring,xip-whse,1) <> 0 then
  xip-oepickfl = true.
end.     

/**
Procedure RFLoop: 
assign zx-l = no
       zx-x = 1.
repeat while program-name(zx-x) <> ?:         
  if program-name(zx-x) = "hher.p" then do:                                          zx-l = yes.                              
    leave.                             
  end.                                      
  assign zx-x = zx-x + 1.                     
end.                                          
end procedure.
**/

