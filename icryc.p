/****************************************************************************
ICRYC
Cycle count History based on table zsdiicsep
smaf - 01/12/11
*****************************************************************************/

{p-rptbeg.i}


def var b-lastcntdt like icsw.lastcntdt         no-undo.
def var e-lastcntdt like icsw.lastcntdt         no-undo.
def var b-whse      like icsw.whse              no-undo.
def var e-whse      like icsw.whse              no-undo.
def var b-prod      like icsw.prod              no-undo.
def var e-prod      like icsw.prod              no-undo.
def var v-type      like icsw.statustype        no-undo.


def var v-lastcntdt    like icsw.lastcntdt         no-undo.
def var v-whse         like icsw.whse              no-undo.
def var v-prod         like icsw.prod              no-undo.
def var v-statustype   like icsw.statustype        no-undo.
def var v-binloc1      like icsw.binloc1           no-undo.
def var v-binloc2      like icsw.binloc1           no-undo.
def var v-qtycnt       as dec format "->>>>9.99"   no-undo.
def var v-qtyexp       as dec format "->>>>9.99"   no-undo.
def var v-postdt       like icet.postdt            no-undo.
def var v-qtyoh        as dec format "->>>>9.99"   no-undo.
def var v-lastrcpdt    like icsw.lastrcptdt        no-undo.
def var v-name         as char format "x(25)"      no-undo.

form 
    "LastCntDt"        at 1
    "Product"          at 12
    "Whse"             at 38
    "BinLoc1"          at 43
/*  "BinLoc2"          at 59  */
    "QtyCnt"           at 59  /* 76  */
    "QtyExp"           at 76  /* 87  */
    "Qtyoh"            at 87  /* 97  */
    "LstUsDt"          at 97  /* 108 */
    "LstRcDt"          at 108 /* 117 */
    "Ty"               at 117 /* 129 */
    "Name"             at 120
     with frame f-titles no-box no-labels width 178 /* 132  */.
    


form
     v-lastcntdt        at 1    label "LstCntDt"  
     v-prod             at 12   label "Product" 
     v-whse             at 38   label "Whse"    
     v-binloc1          at 43   label "BinLoc1" 
/*   v-binloc2          at 59   label "BinLoc2"   */
     v-qtycnt           at 59   label "QtyCnt"  
     v-qtyexp           at 76   label "QtyExp"        
     v-qtyoh            at 87   label "Qtyoh"  
     v-postdt           at 97   label "LstUsDt"  
     v-lastrcpdt        at 108  label "LstRcDt"  
     v-statustype       at 117  label "Ty" 
     v-name             at 120  label "Name"  
     with down frame f-cycleh 
     width 178 no-box no-labels.

put control "~033~046~1541~117".   /* landscape format */  


/*

form
     zsdiicsep.icswlstcntdt     at 1    label "LastCntDt"
     zsdiicsep.prod             at 12   label "Product"
     zsdiicsep.whse             at 38   label "Whse"
     zsdiicsep.binloc1          at 43   label "BinLoc1"
     zsdiicsep.binloc2          at 62   label "BinLoc2"
     zsdiicsep.qtycnt           at 79   label "QtyCnt"
     zsdiicsep.qtyexp           at 90   label "QtyExp"        
     zsdiicsep.icetpstdt        at 108  label "PostDt"
     zsdiicsep.statustype       at 122  label "StatusType"
     with frame f-cycleh no-box no-labels width 132.
*/


assign  b-lastcntdt  =   date(sapb.rangebeg[1])
        e-lastcntdt  =   date(sapb.rangeend[1])  
       b-whse       =   sapb.rangebeg[2]
       e-whse       =   sapb.rangeend[2]
       b-prod       =   sapb.rangebeg[3]
       e-prod       =   sapb.rangeend[3]
       v-type       =   sapb.optvalue[1]. 

 /* DON'T USE
 
 assign v-datein  = sapb.rangebeg[1].         
       {p-rptdt.i}                                  
       if string(v-dateout) = v-lowdt then          
          assign b-lastcntdt = TODAY.                    
       else                                         
          assign b-lastcntdt = v-dateout.                
 
 assign v-datein  = sapb.rangeend[1].         
       {p-rptdt.i}                                  
       if string(v-dateout) = v-lowdt then          
          assign e-lastcntdt = TODAY.                    
       else                                         
          assign e-lastcntdt = v-dateout.                
*/
                                 



/*
display
 with frame f-titles.
 down with frame f-titles.
*/         




if  v-type <> "" and v-type <> "b" then do:
       
/* message b-lastcntdt e-lastcntdt. pause.  */

for each zsdiicsep where zsdiicsep.cono = g-cono  and
                         zsdiicsep.icswlstcntdt >= b-lastcntdt and
                         zsdiicsep.icswlstcntdt <= e-lastcntdt  and
                         zsdiicsep.whse >= b-whse and
                         zsdiicsep.whse <= e-whse  and
                         zsdiicsep.prod >= b-prod and
                         zsdiicsep.prod <= e-prod and
                         zsdiicsep.statustype = v-type
                         no-lock:
        
     /* message zsdiicsep.prod. pause.                    */
                         
   find first icet use-index k-postdt where 
                  icet.cono = zsdiicsep.cono and
                  icet.whse = zsdiicsep.whse and
                  icet.prod = zsdiicsep.prod and
                  icet.transtype = "IN"  
                  no-lock no-error.
        
    if avail icet then do:

    assign 
     v-lastcntdt        = zsdiicsep.icswlstcntdt
     v-prod             = zsdiicsep.prod
     v-whse             = zsdiicsep.whse
     v-binloc1          = zsdiicsep.binloc1
     v-binloc2          = zsdiicsep.binloc2
     v-qtycnt           = zsdiicsep.qtycnt
     v-qtyexp           = zsdiicsep.qtyexp                   
     v-statustype       = zsdiicsep.statustype
     v-qtyoh            = zsdiicsep.qtyonhand
     v-postdt           = icet.postdt  /* zsdiicsep.icetpstdt */
     v-lastrcpdt        = zsdiicsep.lastrcptdt
     v-name             = zsdiicsep.user5.

      display
      v-lastcntdt   
      v-prod        
      v-whse        
      v-binloc1     
   /* v-binloc2 */    
      v-qtycnt      
      v-qtyexp      
      v-statustype  
      v-qtyoh
      v-postdt
      v-lastrcpdt
      v-name
      with frame f-cycleh.
      down with frame f-cycleh.
 
    
    
    end. 
    else do:
     
    assign 
     v-lastcntdt        = zsdiicsep.icswlstcntdt
     v-prod             = zsdiicsep.prod
     v-whse             = zsdiicsep.whse
     v-binloc1          = zsdiicsep.binloc1
     v-binloc2          = zsdiicsep.binloc2
     v-qtycnt           = zsdiicsep.qtycnt
     v-qtyexp           = zsdiicsep.qtyexp                   
     v-statustype       = zsdiicsep.statustype
     v-qtyoh            = zsdiicsep.qtyonhand
     v-postdt           = ? /* zsdiicsep.icetpstdt  */
     v-lastrcpdt        = zsdiicsep.lastrcptdt
     v-name             = zsdiicsep.user5.
     
      display
      v-lastcntdt   
      v-prod        
      v-whse        
      v-binloc1     
   /* v-binloc2   */  
      v-qtycnt      
      v-qtyexp      
      v-statustype  
      v-qtyoh
      v-postdt
      v-lastrcpdt
      v-name
      with frame f-cycleh.
      down with frame f-cycleh.
 
    end.  


end. /* for each zsdiicsep */

end. /* if v-type <> "b" */
/* end. /* if icet */        */

else if v-type = "b"  or v-type = "" then do:

/* message "here2"  b-lastcntdt e-lastcntdt. pause.  */


for each zsdiicsep where zsdiicsep.cono = g-cono  and
                         zsdiicsep.icswlstcntdt >= b-lastcntdt and
                         zsdiicsep.icswlstcntdt <= e-lastcntdt  and
                         zsdiicsep.whse >= b-whse and
                         zsdiicsep.whse <= e-whse  and
                         zsdiicsep.prod >= b-prod and
                         zsdiicsep.prod <= e-prod and
                        (zsdiicsep.statustype = "o" or 
                         zsdiicsep.statustype = "s" or
                         zsdiicsep.statustype = "x")
                         no-lock:
 
       /* message zsdiicsep.whse zsdiicsep.prod zsdiicsep.icetpstdt. pause.  */
       
       find first icet use-index k-postdt where 
                  icet.cono = zsdiicsep.cono and
                  icet.whse = zsdiicsep.whse and
                  icet.prod = zsdiicsep.prod and
                  icet.transtype = "IN"  
                  no-lock no-error.
        
      if avail icet then do:
 
/*     message "got icet" icet.prod icet.postdt. pause.  */

     assign 
     v-lastcntdt        = zsdiicsep.icswlstcntdt
     v-prod             = zsdiicsep.prod
     v-whse             = zsdiicsep.whse
     v-binloc1          = zsdiicsep.binloc1
     v-binloc2          = zsdiicsep.binloc2
     v-qtycnt           = zsdiicsep.qtycnt
     v-qtyexp           = zsdiicsep.qtyexp                   
     v-statustype       = zsdiicsep.statustype
     v-qtyoh            = zsdiicsep.qtyonhand
     v-postdt           = icet.postdt  /* zsdiicsep.icetpstdt */
     v-lastrcpdt        = zsdiicsep.lastrcptdt
     v-name             = zsdiicsep.user5.

     display
      v-lastcntdt   
      v-prod        
      v-whse        
      v-binloc1     
  /*  v-binloc2   */  
      v-qtycnt      
      v-qtyexp      
      v-statustype  
      v-qtyoh
      v-postdt
      v-lastrcpdt
      v-name
      with frame f-cycleh.
      down with frame f-cycleh.
     
    
    
    end.
    else do:
    
    assign 
     v-lastcntdt        = zsdiicsep.icswlstcntdt
     v-prod             = zsdiicsep.prod
     v-whse             = zsdiicsep.whse
     v-binloc1          = zsdiicsep.binloc1
     v-binloc2          = zsdiicsep.binloc2
     v-qtycnt           = zsdiicsep.qtycnt
     v-qtyexp           = zsdiicsep.qtyexp                   
     v-statustype       = zsdiicsep.statustype
     v-qtyoh            = zsdiicsep.qtyonhand
     v-postdt           = ? /* zsdiicsep.icetpstdt  */
     v-lastrcpdt        = zsdiicsep.lastrcptdt
     v-name             = zsdiicsep.user5.
 
     display
      v-lastcntdt   
      v-prod        
      v-whse        
      v-binloc1     
 /*   v-binloc2     */
      v-qtycnt      
      v-qtyexp      
      v-statustype  
      v-qtyoh
      v-postdt
      v-lastrcpdt
      v-name
      with frame f-cycleh.
      down with frame f-cycleh.
 
    
    end. 

end. /* for each zsdiicsep */

end. /* if v-type = "b" */
/* end. /* if icet  */  */





