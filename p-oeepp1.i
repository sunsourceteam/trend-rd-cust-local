/*h*****************************************************************************
  INCLUDE      : p-oeepp1.i
  DESCRIPTION  : Order Entry Print Pick Tickets - Print Serial/Lot# infor
  USED ONCE?   : no
  AUTHOR       : rhl
  DATE WRITTEN : 04/20/93
  CHANGES MADE :
    tb 9409 04/20/93 rhl; Pick Ticket Serial # in random order
    04/27/95 mtt; TB# 13356 Serial/Lot # doesn't print on PO/RM
    07/02/96 jms; TB# (10A) 21254 Develop Value Add Module
      Added VAEPP1 to a comment
    04/06/99 cm;  TB# 26054 added &comui, &export_files and &usage functionality
    10/22/99 cm;  TB# 26054 Removed OPTIO interface
    01/24/01 mms; TB# e7644 WT BOD Project - Allow kpet assigned serials to
                be printed on fabricated kits.
    01/11/2007  dkt  oeepp6.z99 print_lots and skip_lots

*******************************************************************************/

/* This include is used in both OEEPP, OEEPI, WTEP1 and VAEPP1
   When &comforkpout is set to "out" then an alternative display is done for
    a KP Work Order below. */

&if "{&comforkpout}" <> "out":u &then
   /*d Serial # print  */
   
   /** assign the oldest icses masters here and create the icets records for 
       service repair product categories that begin with "s". **/
   if avail icsw and icsw.serlottype = "s"            and  
      avail icsp and icsp.prodcat begins "s"          and
     (g-currproc ne "oerd" and g-currproc ne "oeepi") and
      "{&type}" = "o" and "{&specnstype}" ne "n"      and 
      "{&line}" = "oeel" and {&snlot} > 0 and 
      {&stkqtyship} > 0 then do:
      assign i      = 0
             v-whse = if icsw.arpwhse ne "" then 
                       icsw.arpwhse 
                      else 
                       icsw.whse.
      tied-loop:
      for each icses use-index k-avail where
               icses.cono          = g-cono    and
               icses.whse          = icsw.whse and
               icses.prod          = icsw.prod and
               icses.currstatus    = "a"       and
               icses.reservefl     = no
               exclusive-lock:
       if {&snlot} = 0 or v-qtyship = 0 then leave. 
       if icses.orderno = 0 or 
          icses.orderno <> 0 then do: /* New */
        find vaelo where vaelo.cono = g-cono        and                    
             vaelo.ordertype   = "{&type}"          and                    
             vaelo.orderaltno  = {&line}.{&pref}no  and 
             vaelo.orderaltsuf = {&line}.{&pref}suf and 
             vaelo.linealtno   = {&line}.lineno                           
             no-lock no-error.                                       
        if avail vaelo then do:  
         find sn-oeeh where sn-oeeh.cono = g-cono and
              sn-oeeh.orderno  = vaelo.orderaltno and 
              sn-oeeh.ordersuf = vaelo.orderaltsuf 
              no-lock no-error.
         find vaeh where vaeh.cono = g-cono      and                     
                         vaeh.vano  = vaelo.vano and                    
                         vaeh.vasuf = vaelo.vasuf no-lock no-error.            
         if avail vaeh and num-entries(vaeh.user2,"-") > 1 and 
            icses.serialno = entry(1,vaeh.user2,"-") + "-" +  
                    left-trim(entry(2,vaeh.user2,"-"),"0") then do:  
           run serialno-assign(input "yes").
          leave tied-loop. 
         end. 
        end. /* avail vaelo */ 
        else do:
         run serialno-assign(input "no").
         leave tied-loop. 
        end.   

       end. /* icses.orderno = 0 */
      end. 
      
      autofill:
      for each icses use-index k-avail where
               icses.cono          = g-cono    and
               icses.whse          = icsw.whse and
               icses.prod          = icsw.prod and
               icses.currstatus    = "a"       and
               icses.reservefl     = no
               exclusive-lock:
       if {&snlot} = 0 or v-qtyship = 0 then leave. 
       if icses.orderno = 0 or
          icses.orderno <> 0 then do: /* new */
        if num-entries(icses.serialno,"-") > 1 then do: 
         find icsd where
              icsd.cono = g-cono and 
              icsd.whse = entry(1,icses.serialno,"-")
              no-lock no-error.
        end. 
        find vaelo where vaelo.cono = g-cono        and                    
             vaelo.ordertype   = "{&type}"          and                    
             vaelo.orderaltno  = {&line}.{&pref}no  and 
             vaelo.orderaltsuf = {&line}.{&pref}suf and 
             vaelo.linealtno   = {&line}.lineno                           
             no-lock no-error.                                       
        if avail vaelo and avail icsd then next autofill. 
        if not avail icsd then do:                                      
         find sn-oeeh where sn-oeeh.cono = g-cono  and
              sn-oeeh.orderno  = {&line}.{&pref}no and 
              sn-oeeh.ordersuf = {&line}.{&pref}suf 
              no-error.
         find sn-oeel where sn-oeel.cono = g-cono   and
              sn-oeel.orderno  = {&line}.{&pref}no  and 
              sn-oeel.ordersuf = {&line}.{&pref}suf and
              sn-oeel.lineno   = {&line}.lineno                           
              no-error.
         if avail sn-oeeh and avail sn-oeel then 
          assign sn-oeeh.nosnlots = sn-oeeh.nosnlots - 1
                 sn-oeel.nosnlots = sn-oeel.nosnlots - 1
                 v-qtyship        = v-qtyship - 1.
         create icets.
         assign icets.cono        = g-cono
                icets.prod        = s-prod
                icets.whse        = icsw.whse
                icets.serialno    = icses.serialno
                icets.custno      = if avail sn-oeeh then 
                                     sn-oeeh.custno 
                                    else
                                     0               
                icets.shipto      = if avail sn-oeeh then 
                                     sn-oeeh.shipto 
                                    else 
                                     ""
                icets.postdt      = today
                icets.orderno     = {&line}.{&pref}no 
                icets.ordersuf    = {&line}.{&pref}suf
                icets.lineno      = {&line}.lineno 
                icets.seqno       = 0 
                icets.ordertype   = "{&type}"
                icets.prodcost    = icses.cost 
                icets.icspecrecno = if avail icsp then
                                     icsp.icspecrecno
                                    else 
                                     0
                icses.orderno     = {&line}.{&pref}no 
                icses.ordersuf    = {&line}.{&pref}suf
                icses.lineno      = {&line}.lineno 
                icses.custno      = icets.custno
                icses.shipto      = icets.shipto
                icses.seqno       = 0
                icses.ordertype   = "{&type}"
                icses.reservefl   = true.
         if icses.comment = "" then do: 
          assign x-date = today. 
          run weekofyear.
          find first zsastz where
                     zsastz.cono       = g-cono and 
                     zsastz.codeiden   = "serviceserialnumbers" and 
                     zsastz.labelfl    = false  and
                     zsastz.primarykey = icsw.whse 
                     exclusive-lock no-error.
          if avail zsastz then do:
           if int(zsastz.codeval[3]) >= int(zsastz.codeval[2]) then 
            assign zsastz.codeval[3] = zsastz.codeval[1].
           else 
            assign zsastz.codeval[3] =
                   string((int(zsastz.codeval[3]) + 1),"99999").
           assign icses.comment = substring(string(year(today),"9999"),3,2)
                                  + "-" +  string(x-week,"99") + "-"  
                                  + string(zsastz.codeval[3],"99999").
           create zsdisnxref.                                   
           assign zsdisnxref.cono      = g-cono      
                  zsdisnxref.xrefno    = icses.comment   
                  zsdisnxref.serialno  = icses.serialno
                  zsdisnxref.whse      = icses.whse  
                  zsdisnxref.ordertype = icses.ordertype 
                  zsdisnxref.orderno   = icses.orderno     
                  zsdisnxref.ordersuf  = icses.ordersuf
                  zsdisnxref.lineno    = icses.lineno  
                  zsdisnxref.prod      = icses.prod   
                  zsdisnxref.operinit  = g-operinits   
                  zsdisnxref.transdt   = today   
                  zsdisnxref.transtm   = substring(string(time,"hh:mm"),1,2)
                                       + substring(string(time,"hh:mm"),4,2). 
          end. /* avail zsstz */ 
         end. /* comment = "" */ 
        end. /* not avail vaelo */ 
       end. /* icses.orderno = 0 */ 
      end. /* for each */
   end. /* avail icsw and icsw.serlottype = "s" */ 

   /** warehouse transfer side of the va tied order **/
   if avail icsw and icsw.serlottype = "s"            and  
      avail icsp and icsp.prodcat begins "s"          and
     (g-currproc ne "oerd" and g-currproc ne "oeepi") and
      "{&type}" = "t" and "{&specnstype}" ne "n"      and 
      "{&line}" = "wtel" and {&snlot} > 0 and 
      {&stkqtyship} > 0 then do:
      assign i      = 0
             v-whse = if icsw.arpwhse ne "" then 
                       icsw.arpwhse 
                      else 
                       icsw.whse.
      wt-tied-loop:
      for each icses use-index k-avail where
               icses.cono          = g-cono    and
               icses.whse          = icsw.whse and
               icses.prod          = icsw.prod and
               icses.currstatus    = "a"       and
               icses.reservefl     = no
               exclusive-lock:
       if {&snlot} = 0 or v-qtyship = 0 then leave. 
       if icses.orderno = 0 or 
          icses.orderno <> 0 then do:  /* New */
        find vaelo where vaelo.cono = g-cono        and                    
             vaelo.ordertype   = "{&type}"          and                    
             vaelo.orderaltno  = {&line}.{&pref}no  and 
             vaelo.orderaltsuf = {&line}.{&pref}suf and 
             vaelo.linealtno   = {&line}.lineno                           
             no-lock no-error.                                       
        if avail vaelo then do:  
         find sn-wteh where sn-wteh.cono = g-cono and
              sn-wteh.wtno  = vaelo.orderaltno    and 
              sn-wteh.wtsuf = vaelo.orderaltsuf 
              no-lock no-error.
         run wt-serialno-assign(input "yes").
         leave wt-tied-loop. 
        end. /* avail vaelo */  
        else do: 
         run wt-serialno-assign(input "no").
         leave wt-tied-loop. 
        end. 
       end. /* icses.orderno = 0 */
      end. 
      
      wt-autofill:
      for each icses use-index k-avail where
               icses.cono          = g-cono    and
               icses.whse          = icsw.whse and
               icses.prod          = icsw.prod and
               icses.currstatus    = "a"       and
               icses.reservefl     = no
               exclusive-lock:
       if {&snlot} = 0 or v-qtyship = 0 then leave. 
       if icses.orderno = 0 or
          icses.orderno <> 0 then do: 
        if num-entries(icses.serialno,"-") > 1 then do: 
         find icsd where
              icsd.cono = g-cono and 
              icsd.whse = entry(1,icses.serialno,"-")
              no-lock no-error.
        end. 
        find vaelo where vaelo.cono = g-cono        and                    
             vaelo.ordertype   = "{&type}"          and                    
             vaelo.orderaltno  = {&line}.{&pref}no  and 
             vaelo.orderaltsuf = {&line}.{&pref}suf and 
             vaelo.linealtno   = {&line}.lineno                           
             no-lock no-error.                                       
        if avail vaelo and avail icsd then next wt-autofill. 
        if not avail icsd then do:                                      
         find sn-wteh where sn-wteh.cono = g-cono and
              sn-wteh.wtno  = {&line}.{&pref}no   and 
              sn-wteh.wtsuf = {&line}.{&pref}suf 
              no-error.
         find sn-wtel where sn-wtel.cono = g-cono and
              sn-wtel.wtno   = {&line}.{&pref}no  and 
              sn-wtel.wtsuf  = {&line}.{&pref}suf and
              sn-wtel.lineno = {&line}.lineno                           
              no-error.
         if avail sn-wteh and avail sn-wtel then 
          assign sn-wteh.nosnlotso = sn-wteh.nosnlotso - 1
                 sn-wtel.nosnlotso = sn-wtel.nosnlotso - 1
                 v-qtyship        = v-qtyship - 1.
         create icets.
         assign icets.cono        = g-cono
                icets.prod        = icsw.prod
                icets.whse        = icsw.whse
                icets.serialno    = icses.serialno
                icets.custno      = 0 
                icets.shipto      = if avail sn-wteh then 
                                     sn-wteh.shipfmwhse 
                                    else 
                                     ""
                icets.postdt      = today
                icets.orderno     = {&line}.{&pref}no 
                icets.ordersuf    = {&line}.{&pref}suf
                icets.lineno      = {&line}.lineno 
                icets.seqno       = 0 
                icets.ordertype   = "{&type}"
                icets.prodcost    = icses.cost 
                icets.icspecrecno = if avail icsp then
                                     icsp.icspecrecno
                                    else 
                                     0
                icses.orderno     = {&line}.{&pref}no 
                icses.ordersuf    = {&line}.{&pref}suf
                icses.lineno      = {&line}.lineno 
                icses.custno      = icets.custno
                icses.shipto      = icets.shipto
                icses.seqno       = 0
                icses.ordertype   = "{&type}"
                icses.reservefl   = true.
         if icses.comment = "" and 
         (avail sn-wtel and sn-wtel.transtype = "do") then do: 
          assign x-date = today. 
          run weekofyear.
          find first zsastz where
                     zsastz.cono       = g-cono and 
                     zsastz.codeiden   = "serviceserialnumbers" and 
                     zsastz.labelfl    = false  and
                     zsastz.primarykey = icsw.whse 
                     exclusive-lock no-error.
          if avail zsastz then do:
           if int(zsastz.codeval[3]) >= int(zsastz.codeval[2]) then 
            assign zsastz.codeval[3] = zsastz.codeval[1].
           else 
            assign zsastz.codeval[3] =
                   string((int(zsastz.codeval[3]) + 1),"99999").
           assign icses.comment = substring(string(year(today),"9999"),3,2)
                                  + "-" +  string(x-week,"99") + "-"  
                                  + string(zsastz.codeval[3],"99999").
           create zsdisnxref.                                   
           assign zsdisnxref.cono      = g-cono      
                  zsdisnxref.xrefno    = icses.comment   
                  zsdisnxref.serialno  = icses.serialno
                  zsdisnxref.whse      = icses.whse  
                  zsdisnxref.ordertype = icses.ordertype 
                  zsdisnxref.orderno   = icses.orderno     
                  zsdisnxref.ordersuf  = icses.ordersuf
                  zsdisnxref.lineno    = icses.lineno  
                  zsdisnxref.prod      = icses.prod   
                  zsdisnxref.operinit  = g-operinits   
                  zsdisnxref.transdt   = today   
                  zsdisnxref.transtm   = substring(string(time,"hh:mm"),1,2)
                                       + substring(string(time,"hh:mm"),4,2). 
          end. /* avail zsstz */ 
         end. /* comment = "" */ 
        end. /* not avail vaelo */ 
       end. /* icses.orderno = 0 */ 
      end. /* for each */
   end. /* avail icsw and icsw.serlottype = "s" and (t)ransfer type */ 
   if avail icsw and icsw.serlottype = "s" and
     {&snlot} ne {&stkqtyship} then do:
     i = 0.
     clear frame f-serial.
     for each icets use-index k-order where
       icets.cono      = {&line}.cono       and
       icets.ordertype = "{&type}"          and
       icets.orderno   = {&line}.{&pref}no  and
       icets.ordersuf  = {&line}.{&pref}suf and
       icets.lineno    = {&line}.lineno     and
       icets.seqno     = {&seq}          
       no-lock
       /*tb 9409 04/20/93 rhl; Pick Ticket Serial # in random order */
     by icets.serialno:

    /** pre-assign a valid sauer serialno identifier in the comment field **/
       if avail icsw and icsw.serlottype = "s"   and  
          avail icsp and icsp.prodcat begins "s" and
         (g-currproc ne "oerd" and g-currproc ne "oeepi") and
         (icets.ordertype = "f" or icets.ordertype = "t" or 
          icets.ordertype = "o") then do: 
        if num-entries(icets.serialno,"-") > 1 then do: 
         find icsd where
              icsd.cono = g-cono and 
              icsd.whse = entry(1,icets.serialno,"-")
              no-lock no-error.
        end. 
        if "{&type}" = "t" then  
         find sn-wtel where sn-wtel.cono = g-cono and
              sn-wtel.wtno   = {&line}.{&pref}no  and 
              sn-wtel.wtsuf  = {&line}.{&pref}suf and
              sn-wtel.lineno = {&line}.lineno                           
              no-lock no-error.
        find vaelo where vaelo.cono = g-cono    and                    
             vaelo.ordertype   = "{&type}"      and                    
             vaelo.orderaltno  = icets.orderno  and                    
             vaelo.orderaltsuf = icets.ordersuf and                    
             vaelo.linealtno   = icets.lineno                           
             no-lock no-error.                                       
        if avail vaelo and avail icsd then do:                                            find sn-oeeh where sn-oeeh.cono = g-cono   and
              sn-oeeh.orderno  = vaelo.orderaltno   and 
              sn-oeeh.ordersuf = vaelo.orderaltsuf 
              no-lock no-error.
         find vaeh where vaeh.cono = g-cono      and                     
                         vaeh.vano  = vaelo.vano and                    
                         vaeh.vasuf = vaelo.vasuf no-lock no-error.            
         assign x-serialno = " ". 
         if avail vaeh and num-entries(vaeh.user2,"-") > 1 then 
          assign x-serialno = entry(1,vaeh.user2,"-") + "-" +  
                    left-trim(entry(2,vaeh.user2,"-"),"0"). 
          if avail vaeh and icets.serialno = x-serialno then do:                
          find first icses use-index k-icses where 
               icses.cono       = g-cono         and                       
               icses.prod       = vaeh.shipprod  and                       
               icses.whse       = vaeh.whse      and                       
               icses.serialno   = (if x-serialno ne " " then 
                                    x-serialno  
                                   else    
                                    icets.serialno) and   
               icses.currstatus = "a"            and     
               icses.orderno    = icets.orderno  and                    
               icses.ordersuf   = icets.ordersuf and 
               icses.lineno     = icets.lineno   and 
               icses.reservefl  = (if x-serialno ne " " then 
                                              yes  
                                            else    
                                              false)
                       
               exclusive-lock no-error. 
            end. /* New */
          end.  /* new  */
          else do:
            find first icses use-index k-icses where 
                       icses.cono       = g-cono         and                                            icses.prod       = icets.prod     and                                            icses.whse       = icets.whse     and                                            icses.serialno   = icets.serialno and 
                       icses.lineno     = icets.lineno   and
                       
                       (icses.currstatus = "d"            or
                        icses.currstatus = "a" )           and

                       icses.orderno    = icets.orderno  and                    
                       icses.ordersuf   = icets.ordersuf and 
                       icses.lineno     = icets.lineno   and 
                       icses.reservefl  =  yes
              no-error. 
          end.
          
          
          if avail icses and icses.comment = " " and 
           ((icets.ordertype = "t" and avail sn-wtel
             and sn-wtel.transtype = "do") or 
            (icets.ordertype = "f" or icets.ordertype = "o")) then do:  
           assign x-date = today. 
           run weekofyear.
           find first zsastz where
                      zsastz.cono       = g-cono and 
                      zsastz.codeiden   = "serviceserialnumbers" and 
                      zsastz.labelfl    = false  and
                      zsastz.primarykey = icses.whse 
                      exclusive-lock no-error.
           if avail zsastz then do:
            if int(zsastz.codeval[3]) >= int(zsastz.codeval[2]) then 
             assign zsastz.codeval[3] = zsastz.codeval[1].
            else 
             assign zsastz.codeval[3] =
                    string((int(zsastz.codeval[3]) + 1),"99999").
            assign icses.comment = substring(string(year(today),"9999"),3,2)
                                   + "-" +  string(x-week,"99") + "-"  
                                   + string(zsastz.codeval[3],"99999").
            create zsdisnxref.  
            assign zsdisnxref.cono      = g-cono   
                   zsdisnxref.xrefno    = icses.comment   
                   zsdisnxref.serialno  = icses.serialno
                   zsdisnxref.whse      = icses.whse  
                   zsdisnxref.ordertype = icses.ordertype                   
                   zsdisnxref.orderno   = icses.orderno  
                   zsdisnxref.ordersuf  = icses.ordersuf
                   zsdisnxref.lineno    = icses.lineno  
                   zsdisnxref.prod      = icses.prod                        
                   zsdisnxref.operinit  = g-operinits   
                   zsdisnxref.transdt   = today   
                   zsdisnxref.transtm   = substring(string(time,"hh:mm"),1,2)
                                        + substring(string(time,"hh:mm"),4,2). 
           end. /* avail zastz */   
          end. /* avail icses and blank comment */

          if avail icses then 
           assign i = i + 1
                  s-serialno[i] = icets.serialno.
          if length(s-serialno[i]) >= 10 then do: 
            if i = 4 then do:
             assign s-serialno[i + 1] = icses.comment.       
             display s-serialno[i] s-serialno[i + 1] with frame f-serial.
             assign i = 0.
             down with frame f-serial.
            end.
            else do:
             assign s-serialno[i + 1] = icses.comment.       
             display s-serialno[i] s-serialno[i + 1] with frame f-serial.
            end. 
            assign i = i + 1. 
          end. /*  >= 10  */
          else 
          if avail icses and icses.comment ne "" then do:      
           assign s-serialno[i] = s-serialno[i] + " " + icses.comment.       
          end.
/*         end. /* avail vaeh */  */
/*        end. /* avail vaelo */ */
        /* serial item and service prodcat valid - no tie to va order*/ 
        else do: 
         find first icses use-index k-icses where 
              icses.cono       = g-cono         and                       
              icses.prod       = icets.prod     and                       
              icses.whse       = icets.whse     and                       
              icses.serialno   = icets.serialno and 
              icses.lineno     = icets.lineno    and
              icses.currstatus = "a"            
              no-error. 
          if avail icses then do: 
           assign i = i + 1 
                  s-serialno[i] = icets.serialno.
           if length(s-serialno[i]) >= 10 then do: 
            if i = 4 then do:
             assign s-serialno[i + 1] = icses.comment.       
             display s-serialno[i] s-serialno[i + 1] with frame f-serial.
             assign i = 0.
             down with frame f-serial.
            end.
            else do:
             assign s-serialno[i + 1] = icses.comment.       
             display s-serialno[i] s-serialno[i + 1] with frame f-serial.
            end. 
            assign i = i + 1. 
           end. 
           else do:
            assign s-serialno[i] = s-serialno[i] + " " + icses.comment.       
            display s-serialno[i] with frame f-serial.
            if i = 5 then do:
             assign i = 0.
             down with frame f-serial.
            end.
           end. /* avail icses */
          end. 
        end. /* else do */
       end. /* prodcat valid and serialitem and ordertype = "f" or "t" */
       else 
/*        assign i = i + 1
               s-serialno[i] = icets.serialno.
*/ 
       /** print the sauer serialno comment field if applicable 
           on invoices from oeepi or oerd  **/
       if avail icsw and icsw.serlottype = "s"         and  
          avail icsp and icsp.prodcat begins "s"       and
         (g-currproc = "oerd" or g-currproc = "oeepi") and 
          icets.ordertype = "o" then do: 
          find first icses use-index k-icses where 
                     icses.cono       = g-cono         and                      
                     icses.prod       = icets.prod     and                     
                     icses.whse       = icets.whse     and                     
                     icses.serialno   = icets.serialno  
                     no-error. 
          if avail icses then do: 
           if num-entries(icses.serialno,"-") > 1 then  
            find first icsd where
                       icsd.cono = g-cono and 
                       icsd.whse = entry(1,icses.serialno,"-")
                       no-lock no-error.

           if num-entries(icses.serialno,"-") > 1 and 
              avail icsd then 
             assign i = 1 
                   s-serialno[i] = icets.serialno.
           else   
             assign i = i + 1 
                   s-serialno[i] = icets.serialno.
           if length(s-serialno[i]) >= 10 then do: 
           if i = 3 then do:
             assign s-serialno[i + 1] = icses.comment.       
             display s-serialno[i] s-serialno[i + 1] with frame f-serial.
             assign i = -1.
             down with frame f-serial.
            end.
            else do:
             assign s-serialno[i + 1] = icses.comment.       
             display s-serialno[i] s-serialno[i + 1] with frame f-serial.
            end. 
            assign i = i + 1. 
           end. 
           else do:
            assign s-serialno[i] = s-serialno[i] + " " + icses.comment.       
            display s-serialno[i] with frame f-serial.
            if i = 5 then do:
             assign i = 0.
             down with frame f-serial.
            end.
           end. /* avail icses */
          next.
          end. 
       end. /* oerd or oeepi - printing only. */
     
       assign i = i + 1
              s-serialno[i] = icets.serialno.
  
       /* va/oe one for one tie to repair serialno here */         
       display s-serialno[i] with frame f-serial.
       if i = 4 then do:
          i = 0.
          down with frame f-serial.
       end.
     end. /* for each icets */ 
   end.
   /*d  Lot print  */
   {oeepp6.z99 &skip_lots = "*"}
   if avail icsw and icsw.serlottype = "l" 
     and {&snlot} ne {&stkqtyship} 
   then do:
     i = 0.
     clear frame f-lot.
     for each icetl use-index k-order where
       icetl.cono      = {&line}.cono and
       icetl.ordertype = "{&type}" and
       icetl.orderno   = {&line}.{&pref}no and
       icetl.ordersuf  = {&line}.{&pref}suf and
       icetl.lineno    = {&line}.lineno and
       icetl.seqno     = {&seq} no-lock
       /*tb 9409 04/20/93 rhl; Pick Ticket Serial # in random order */
     by icetl.lotno:
       i = i + 1.
       if i = 1 then do:       
         assign s-lot1  = icetl.lotno
                s-qty1  = icetl.quantity
                s-lot1d = "Lot #:"
                s-qty1d = "Qty:".
         display s-lot1 s-qty1 s-lot1d s-qty1d with frame f-lot.
       end.
       else do:
         assign i       = 0
                s-lot2  = icetl.lotno
                s-qty2  = icetl.quantity
                s-lot2d = "Lot #:"
                s-qty2d = "Qty:".
         display s-lot2 s-qty2 s-lot2d s-qty2d with frame f-lot.
         down with frame f-lot.
       end.
     end.
     
     end. /* the nosnlot ne stkqtyship */
     {oeepp6.z99 &print_lots = "*" 
       &line = "{&line}"
       &type = "{&type}"
       &pref = "{&pref}"
       &seq  = "{&seq}"}  
     

     /**** begin dkt 
     /* put in again for rest */
     if v-loopcnt = 1 and icsw.whse ne "CIMT" then do:
     i = 0.
     /** clear frame f-serial. **/
     /*d  Lot print  */
     for each icsel where icsel.cono = icsw.cono and
       icsel.prod = icsw.prod and icsel.whse = icsw.whse and
       icsel.qtyavail > 0 and icsel.statustype = "A" no-lock
       by icsel.opendt by icsel.lotno:
     /**
     for each icetl use-index k-order where
         icetl.cono      = icsw.cono and
         icetl.orderno   <> {&line}.{&pref}no and
         icetl.whse      = icsw.whse and
         icetl.prod      = icsw.prod and
         icetl.lotno     = icsel.lotno
          no-lock:
     **/     
         /*tb 9409 04/20/93 rhl; Pick Ticket Serial # in random order */
         if i = 0 then display with frame f-lotheader. /* "frame header". */
         i = i + 1.
         display icsel.lotno icsel.qtyavail icsel.opendt icsel.binloc[1]
            icsel.binloc[2] icsel.closedt with frame f-lotdetails.
         down with frame f-lotdetails.
       end.
     /** end.  icetl */
     end.  
     /* end of again */
   /* end. */
    end dkt ****/

&endif

/* Use this processing when coming from a KP WO.  If the component is either
   a serial or a lot product, then always print the serial or lot #. */
&if "{&comforkpout}" = "out":u &then
   /*d Serial # print  */
   if avail icsw and icsw.serlottype = "s":u 
   then do:
     i = 0.
     clear frame f-serial.
     for each icets use-index k-order where
     icets.cono      = {&line}.cono and
     icets.ordertype = "{&type}"    and
     icets.orderno   = {&wono}      and
     icets.ordersuf  = {&wosuf}     and
     icets.lineno    = {&lineno}    and
     icets.seqno     = {&seq}
     no-lock
     by icets.serialno:
       assign i             = i + 1
              s-serialno[i] = icets.serialno.
       display s-serialno[i] with frame f-serial.
       if i = 5 then do:
         i = 0.
         down with frame f-serial.
       end.
     end.
   end.
   /*d  Lot print  */
   if avail icsw and icsw.serlottype = "l"
   then do:
     i = 0.
     clear frame f-lot.
     for each icetl use-index k-order where
       icetl.cono      = {&line}.cono and
       icetl.ordertype = "{&type}"    and
       icetl.orderno   = {&wono}      and
       icetl.ordersuf  = {&wosuf}     and
       icetl.lineno    = {&lineno}    and
       icetl.seqno     = {&seq} no-lock
     by icetl.lotno:
       i = i + 1.
       if i = 1 then do:
         assign s-lot1  = icetl.lotno
                s-qty1  = icetl.quantity
                s-lot1d = "Lot #:"
                s-qty1d = "Qty:".
         display s-lot1 s-qty1 s-lot1d s-qty1d with frame f-lot.
       end.
       else do:
         assign i       = 0
                s-lot2  = icetl.lotno
                s-qty2  = icetl.quantity
                s-lot2d = "Lot #:"
                s-qty2d = "Qty:".
         display s-lot2 s-qty2 s-lot2d s-qty2d with frame f-lot.
         down with frame f-lot.
       end.
     end.
   end.
  {oeepp6.z99 &print_lots = "*" 
              &line = "{&line}"
              &type = "{&type}"
              &pref = "{&pref}"
              &seq  = "{&seq}"}  
&endif
