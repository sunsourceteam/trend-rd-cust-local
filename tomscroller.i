
clear frame {&frame} all.
pause 0.

input clear.

do i = 1 to v-length:
  confirm = true.
  if i = 1 then 
  do:  
    {{&find} first}.
    do while not confirm and avail {&file}:
      {{&find} next "/*"}.
    end.
  end.
  else 
    do:  
    {{&find} next "/*"}.
      do while not confirm and avail {&file}:
      {{&find} next "/*"}.
      end.
    end.
  if not avail {&file} then
    do:
    run err.p(9013).
    leave.
    end.
  else
    do:
    {{&display}}.
    down with frame {&frame}.
    readkey pause 0.
    if {k-cancel.i} then
      leave.
    end.
end.


if s-position = 0 then
  s-position = 1.

up (i - s-position) with frame {&frame}.
s-position = 0.


dsply:
repeat with frame {&frame} on endkey undo main, next main:

  input clear.
  status default
  "Up/Down Arrow, Prev/Next Page, Go to Select, or Cancel".
  choose row {&field} no-error
    go-on(f12 shift-f9 f19 shift-f10 f20  {&go-on})
    with frame {&frame}.
  color display  normal {&field} with frame {&frame}.
  hide message no-pause.
  {{&hookname} &user_after_choose = "*"}
  if {k-func12.i} then
    do:
    {{&hookname} &func12 = "*"}
    run err.p(2003).
    next.
    end.
  if frame-value = " " and not ({k-func.i} or {k-func20.i}) then
    next.
  if frame-value ne o-frame-value then
    do:
    o-frame-value = frame-value.
    {{&where}}.
    end.
  /{&usecode}*/
  {{&usefile}}
  /{&usecode}*/

  if {k-func20.i} then
    do:
    {{&where}}.
    next.
    end.
  if {k-return.i} then
    do:
    if frame-line({&frame}) ne v-length then
      do:
      down with frame {&frame}.
      next.
      end.
    end.
  if {k-select.i}  or ({k-func.i} and not g-lkupfl) then
    do:
    {&global}.
    hide message no-pause.
    if {k-select.i} then
      do:
     /* leave main.   */
      next.
      end.
    else
      do:
      {k-lkfunc.i}
      if v-fkey[i] = "" then
        do:
        bell.
        next.
        end.
      assign g-modulenm = substring(v-fkey[i],1,2)
             g-menusel  = substring(v-fkey[i],3)
             g-inqfl    = true.
      return.
      end.
    end.
    if {k-prevpg.i} or {k-scrlup.i} then
      do:
      up (frame-line({&frame}) - 1) with frame {&frame}.
      if frame-value = "" then
        do:
        run err.p(9014).
        next.
        end.
      if frame-value ne o-frame-value  then
        do:
        o-frame-value = frame-value.
        {{&where}}.
        end.
      input clear.
      do i = 1 to v-length :
        {{&find} prev "/*"}.
        do while not confirm and avail {&file}:
          {{&find} prev "/*"}.      
        end.
        if not avail {&file} then
          do:
          run err.p (9013).
          if i ne 1 then
            do:
              repeat j = i to v-length:
              clear frame {&frame} no-pause.
              if j ne v-length then
                up with frame {&frame}.
              end.
            if i ne 1 then
              down (frame-down({&frame}) - i + 1) with frame {&frame}.
            end.
          leave.
          end.
        else
          do:
          if i = 1 then
            down (v-length - 1) with frame {&frame}.
            {{&display}}.
          if i ne v-length then
            up with frame {&frame}.
          readkey pause 0.
          if {k-cancel.i} then
            leave.
          end.
       end.
       if frame-line({&frame}) ne frame-down({&frame}) then
         down frame-down({&frame}) - frame-line({&frame}) with frame {&frame}.
       end.
     else
     if {k-nextpg.i} or {k-scrldn.i} then
       do:
       down (frame-down({&frame}) - frame-line({&frame})) with frame {&frame}.
       if frame-value = "" then
         do:
         run err.p(9015).
         next.
         end.
       if frame-value ne o-frame-value then
         do:
         o-frame-value = frame-value.
         {{&where}}.
         end.
       input clear.
         do i = 1 to v-length:
           {{&find} next "/*"}.
             do while not confirm and avail {&file}:
               {{&find} next "/*"}.
             end.
           if not avail {&file} then
             do:
             run err.p (9013).
             if i ne  1 then
               do:
                 repeat j = i to v-length:
                   clear frame {&frame} no-pause.
                   if j ne v-length then
                     down with frame {&frame}.
                 end.
               if i ne 1 then
                 up (frame-down({&frame}) - i + 1) with frame {&frame}.
               end.
               leave.
             end.
           else
             do:
             if i = 1 then
               up (v-length - 1) with frame {&frame}.
             {{&display}}.
             if i ne v-length then
               down with frame {&frame}.
             readkey pause 0.
             if {k-cancel.i} then
               leave.
             end.
           end.
         if frame-line({&frame}) ne 1 then
           up frame-line({&frame}) - 1 with frame {&frame}.
       end.
     end.
 status default.


  


