/**************************************************************************
    Procedure    : iczo
    Description  : 
    Author       : Jack Hahn
    Date Written :
    Changes Made : dbf; 09/28/98; changed logic to include all warehouses.
***************************************************************************/
def buffer b-icswu for icswu.

Def var x-openstream as logical no-undo.
def var testcount as integer no-undo.
def var z-linecount as integer no-undo.
def var v-desc as character format "x(30)" no-undo.
def var v-pline like icsw.prodline no-undo.
def var v-vendor like icsw.arpvendno no-undo.
def var v-buyer like icsl.buyer no-undo.
def var v-rcptdt as date no-undo.
def var v-invdt as date no-undo.
def var v-entdt as date no-undo.
def var v-basename as character format "x(8)" no-undo.
def var v-avl  like icsw.qtyonhand no-undo.
def var v-co   like icsw.qtyonhand no-undo.
def var v-fiss as integer no-undo.
def var v-future as integer no-undo.
def var v-whseinx as integer no-undo.
def var v-whselist as character format "x(11)" extent 150.

def var v-peckinx as integer no-undo.


def var v-peckinglist as character format "x(4)" extent 9 init
  ["dros",
   "dren",
   "dhou",
   "dmcp",
   "datl",
   "decp",
   "dscp",
   "dwcp",
   "dxcp"].
def var v-alphainx as integer no-undo.
def var v-nameinx as integer no-undo.
def var v-filepart as character format "x(4)" no-undo.
def var v-name as character format "x" extent 4.
def var v-letter as character format "x" extent 26
  init ["a",
        "b",
        "c",
        "d",
        "e",
        "f",
        "g",
        "h",
        "i",
        "j",
        "k",
        "l",
        "m",
        "n",
        "o",
        "p",
        "q",
        "r",
        "s",
        "t",
        "u",
        "v",
        "w",
        "x",
        "y",
        "z"].
DEF var ttloh as integer format ">>>>>>>9-" no-undo.
DEF var ttlusg as integer format ">>>>>>>9-" no-undo.
DEF var ntot as integer format ">>>>>>>9-" no-undo.
DEF var dtot as integer format ">>>>>>>9-" no-undo.
def var ftot as integer format ">>>>>>>9-" no-undo.
def var ttlusg2 as integer format ">>>>>>>9-" no-undo.
def var ttlcost as decimal format ">>>>>>>9.99999-" no-undo.
DEF var mos as decimal format ">>>>>>9.9-" no-undo.
DEF var avgdemd as decimal format ">>>>>>>9.99999-" no-undo.
DEF var oh24 as integer format ">>>>>>>9-" no-undo.
DEF var oh36 as integer format ">>>>>>>9-" no-undo.
DEF var oh60 as integer format ">>>>>>>9-" no-undo.
DEF var wrkoh as integer format ">>>>>>>9-" no-undo.     
DEF var excess as integer format ">>>>>>>9-" no-undo.        
   
DEF var qty24 as integer format ">>>>>>>9-" no-undo.        
DEF var qty36 as integer format ">>>>>>>9-" no-undo.        
DEF var qty60 as integer format ">>>>>>>9-" no-undo.        
DEF var qtyov as integer format ">>>>>>>9-" no-undo.        
DEF var cost24 as decimal format ">>>>>>>9.99-" no-undo.        
DEF var cost36 as decimal format ">>>>>>>9.99-" no-undo.        
DEF var cost60 as decimal format ">>>>>>>9.99-" no-undo.        
DEF var costov as decimal format ">>>>>>>9.99-" no-undo.        
DEF var savecost as decimal format ">>>>>>>9.99999-" no-undo.        

DEF var finalqty24 as integer format ">>>>>>>9-" no-undo.        
DEF var finalqty36 as integer format ">>>>>>>9-" no-undo.        
DEF var finalqty60 as integer format ">>>>>>>9-" no-undo.        
DEF var finalqtyov as integer format ">>>>>>>9-" no-undo.        
DEF var finalcost24 as decimal format ">>>>>>>9.99-" no-undo.        
DEF var finalcost36 as decimal format ">>>>>>>9.99-" no-undo.        
DEF var finalcost60 as decimal format ">>>>>>>9.99-" no-undo.        
DEF var finalcostov as decimal format ">>>>>>>9.99-" no-undo.        

DEF var plineqty24 as integer format ">>>>>>>9-" no-undo.        
DEF var plineqty36 as integer format ">>>>>>>9-" no-undo.        
DEF var plineqty60 as integer format ">>>>>>>9-" no-undo.        
DEF var plineqtyov as integer format ">>>>>>>9-" no-undo.        
DEF var plinecost24 as decimal format ">>>>>>>9.99-" no-undo.        
DEF var plinecost36 as decimal format ">>>>>>>9.99-" no-undo.        
DEF var plinecost60 as decimal format ">>>>>>>9.99-" no-undo.        
DEF var plinecostov as decimal format ">>>>>>>9.99-" no-undo.        
def var begdiv as decimal no-undo.
def var enddiv as decimal no-undo.
def var begved as decimal no-undo.
def var endved as decimal no-undo.
def var begby  like icsl.buyer no-undo.
def var endby  like icsl.buyer no-undo.
def var begcat like icsp.prodcat no-undo.
def var endcat like icsp.prodcat no-undo.

DEF var invvalue as decimal format ">>>>>>>>>>9.99-" no-undo.        
DEF var tinvvalue as decimal format ">>>>>>>>>>9.99-" no-undo.        
DEF var x as integer no-undo.

DEF temp-table whses no-undo
  field zwhse like icsw.whse
  field zcnt as integer
  
  index k-zpecking
        zcnt
  index k-zwhse
        zwhse.

DEF temp-table itemz no-undo 
  field zprod like icsw.prod
  field zdescrip like icsp.descr[1]
  field zvendor like icsw.arpvendno
  field zbuyer like icsl.buyer
  field zusage as integer format ">>>>>>>9-"
  field zusage2 as integer format ">>>>>>>9-"
  field zntot   as integer format ">>>>>>>9-"
  field zdtot   as integer format ">>>>>>>9-"  
  field zftot   as integer format ">>>>>>>9-"
  field zrcptdt as date
  field zinvdt  as date 
  field zentdt  as date
  field zonhand like icsw.qtyonhand
  field zov     like icsw.qtyonhand
  field zqty60  like icsw.qtyonhand
  field zcost like icsw.avgcost
  field zwhse like icsw.whse
  field zpline like icsw.prodline
  
  index i-line
        zpline
        zprod.

  


DEF temp-table itemw no-undo 
  field zprod like icsw.prod
  field zdescrip like icsp.descr[1]
  field zbuyer like icsl.buyer
  field zvendor like icsw.arpvendno
  field zusage as integer format ">>>>>>>9-"
  field zusage2 as integer format ">>>>>>>9-"
  field zntot   as integer format ">>>>>>>9-"
  field zdtot   as integer format ">>>>>>>9-"  
  field zftot   as integer format ">>>>>>>9-"
  field zrcptdt as date
  field zinvdt  as date 
  field zentdt  as date
  field zonhand like icsw.qtyonhand format ">>>>>>>9-" 
  field zov     like icsw.qtyonhand
  field zqty60  like icsw.qtyonhand
  field zthisavl like icsw.qtyonhand
  field zcost like icsw.avgcost
  field zwhse like icsw.whse
  field zpline like icsw.prodline
  
  index w-line is unique primary
        zwhse
        zpline
        zprod
  index w-csv
        zpline
        zprod
        zwhse
  index w-prod
        zprod
        zwhse.

Define stream xcsv.
Define stream xicxm.
Define stream xwhsef.
Define stream xaudit.
Define var xwhsef_name as character format "x(20)" no-undo.


/* ------------------------------------------------------------------------
   Variables Used To Create Trend Like Header
   -----------------------------------------------------------------------*/


DEFINE VARIABLE rpt-dt      as date format "99/99/9999" no-undo.
DEFINE VARIABLE rpt-dow     as char format "x(3)"   no-undo. 
DEFINE VARIABLE rpt-time    as char format "x(5)"  no-undo.
DEFINE VARIABLE rpt-cono    like oeeh.cono         no-undo.
DEFINE VARIABLE rpt-user    as char format "x(08)" no-undo.
DEFINE VARIABLE x-title     as char format "x(177)" no-undo.
DEFINE VARIABLE x-page      as integer format ">>>9" no-undo.
DEFINE VARIABLE dow-lst     as character format "x(30)"
           init "Sun,Mon,Tue,Wed,Thu,Fri,Sat"                 no-undo.

DEFINE VARIABLE monthname   as char format "x(10)" EXTENT 12 initial
["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"].

/* -------------------------------------------------------------------------
   Global Frame Definitions
   ------------------------------------------------------------------------*/

DEFINE FRAME kheadings1
  header
  "~033~105~033~046~154~061~117~033~046~153~062~123~033~046~154~060~105" at 1
         with  down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.
DEFINE FRAME kheadings1a
         rpt-dt           at 1 space
         rpt-dow             space
         rpt-time            space(3)
         "Company:"          space
         rpt-cono            space
         "Function: iczo "   space
         "Operator:"         space
         rpt-user            space
         "page:"              at 169
         x-page              at 174
         "~015"              at 178
         with  down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.
         
DEFINE FRAME kheadings1b
         "Policy 21 Obsolecence Report" at 71
         "~015"              at 178
         with  down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.
         
DEFINE FRAME kheadings1c
         "36-48"        at 117
         "49-60"        at 131
         "Over 60"      at 144
         "24Mth No Act" at 154
         "~015"         at 178
         "Product"      at 1
         "Description"  at 26
         "Rusage"       at 59
         "D/Fusage"     at 69
         "Nusage"       at 79
         "AVL"          at 92
         "Cost"         at 106
         "Qty/Amt"      at 116
         "Qty/Amt"      at 130
         "Qty/Amt"      at 144
         "Qty/Amt"      at 159
         "Lastsale"     at 168
         "~015"         at 178
         with  down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.

DEFINE FRAME detail-l
       itemz.zprod     at 1
       itemz.zdescrip  at 26
       itemz.zusage    at 57
       itemz.zdtot     at 67
       itemz.zntot     at 77
       ttloh           at 87
       savecost        at 97
       qty24           at 115
       qty36           at 129
       qty60           at 143
       qtyov           at 157
       itemz.zinvdt    at 168
       "~015"          at 178
       itemz.zftot     at  67
       cost24          at 112
       cost36          at 126
       cost60          at 140
       costov          at 154
       "~015"          at 178
        with  down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.

           
DEFINE FRAME pline-total-l
       "------------------------------------------------------------" space(0)
       "------------------------------------------------------------" space(0)
       "---------------------------------------------------------"
       "~015"           at 178
       "ProdLine"       at 1
       itemz.zpline     at 10
       "Total"          at 18 
       plineqty24       at 115
       plineqty36       at 129
       plineqty60       at 143
       plineqtyov       at 157
       "~015"           at 178
       plinecost24      at 112
       plinecost36      at 126
       plinecost60      at 140
       plinecostov      at 154
       "~015"           at 178 
       "------------------------------------------------------------" space(0)
       "------------------------------------------------------------" space(0)
       "---------------------------------------------------------"
       "~015"           at 178
 
       with  down STREAM-IO NO-BOX NO-LABELS no-underline
           WIDTH 178.
 
           
           
DEFINE FRAME total-l
       "Totals :"       at 1
       finalqty24       at 115
       finalqty36       at 129
       finalqty60       at 143
       finalqtyov       at 157
       "~015"           at 178
       finalcost24      at 112
       finalcost36      at 126
       finalcost60      at 140
       finalcostov      at 154
       "~015"           at 178
       "     TOTAL INVENTORY VALUE:"    at 1
       tinvvalue                        at 30
       "~015"                           at 178
       "     TOTAL OBSOLESENCE INVENTORY VALUE:" at 1
       invvalue                         at 40
       "~015"                           at 178
 
      with  down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.
 


          
DEFINE FRAME kheadings1c_w
         "Over 60"      at 144
         "24Mth No Act" at 154
         "~015"         at 178
         "Product"      at 1
         "Description"  at 26
         "Vendor"       at 57
         "Vendor Name"  at 65
         "AVL"          at 102
         "Cost"         at 117
         "Whse"         at 125
         "Qty/Amt"      at 144
         "Qty/Amt"      at 159
         "Lastsale"     at 168
         "~015"         at 178
         with  down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.

DEFINE FRAME detail-l_w
       itemw.zprod     at 1
       itemw.zdescrip  at 26
       itemw.zvendor   at 52
       v-desc          at 65
       itemw.zonhand   at 97
       savecost        at 108
       itemw.zwhse     at 125
       qty60           at 143
       qtyov           at 157
       itemw.zinvdt    at 168
       "~015"          at 178
       cost60          at 140
       costov          at 154
       "~015"          at 178
        with  down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.
 
           
DEFINE FRAME pline-total-l_w
       "------------------------------------------------------------" space(0)
       "------------------------------------------------------------" space(0)
       "---------------------------------------------------------"
       "~015"           at 178
       "ProdLine"       at 1
       itemw.zpline     at 10
       "Total"          at 18 
       plineqty60       at 143
       plineqtyov       at 157
       "~015"           at 178
       plinecost60      at 140
       plinecostov      at 154
       "~015"           at 178 
       "------------------------------------------------------------" space(0)
       "------------------------------------------------------------" space(0)
       "---------------------------------------------------------"
       "~015"           at 178
 
       with  down STREAM-IO NO-BOX NO-LABELS no-underline
           WIDTH 178.
 
           
           
DEFINE FRAME total-l_w
       "Totals :"       at 1
       finalqty60       at 143
       finalqtyov       at 157
       "~015"           at 178
       finalcost60      at 140
       finalcostov      at 154
       "~015"           at 178
/*       "     TOTAL INVENTORY VALUE:"    at 1
       tinvvalue                        at 30
       "~015"                           at 178   */
       "     TOTAL OBSOLESENCE INVENTORY VALUE:" at 1
       invvalue                         at 40
       "~015"                           at 178
 
      with  down STREAM-IO NO-BOX NO-LABELS no-underline
            WIDTH 178.
 

{p-rptbeg.i}  


if sapb.optvalue[1] <> "" then
   v-basename = sapb.optvalue[1].
else
   v-basename = "iczorpt".


if sapb.rangebeg[1] ne "" then
   begdiv = decimal(sapb.rangebeg[1]).
else begdiv = 0.   
if sapb.rangeend[1] ne "" then
   enddiv = decimal(sapb.rangeend[1]).
else
   enddiv = 99.
           
if sapb.rangebeg[2] ne "" then
   begved = decimal(sapb.rangebeg[2]).
else begved = 0.   
if sapb.rangeend[2] ne "" then
   endved = decimal(sapb.rangeend[2]).
else
   endved = 999999999.
            
if sapb.rangebeg[3] ne "" then
   begby = sapb.rangebeg[3].
else begby = "    ".   
if sapb.rangeend[3] ne "" then
   endby = sapb.rangeend[3].
else
   endby = "````".
           
if sapb.rangebeg[4] ne "" then
   begcat = sapb.rangebeg[4].
else begcat = "    ".   
if sapb.rangeend[4] ne "" then
   endcat = sapb.rangeend[4].
else
   endcat = "````".

/* -----------------------------------------------------------------------
   Header Variable Initialization
   ----------------------------------------------------------------------*/

rpt-user = userid("dictdb").
rpt-dt = today.
rpt-time = string(time, "HH:MM").
rpt-dow = entry(weekday(today),dow-lst).
rpt-cono = g-cono.
x-page = 0.
x = 0.
do v-peckinx = 1 to 9:
  find whses use-index k-zwhse
       where whses.zwhse = v-peckinglist[v-peckinx] no-lock no-error.
  if not avail whses then
    do:
    create whses.
    x = x + 1.
    assign whses.zcnt = x
           whses.zwhse = v-peckinglist[v-peckinx].
    end.       
end.

for each icsd where icsd.cono = g-cono and
                    (icsd.whse <> "cimt" and
                     icsd.whse <> "ctip") no-lock:
 find whses use-index k-zwhse
       where whses.zwhse = icsd.whse no-lock no-error.
  if not avail whses then
    do:
    create whses.
    x = x + 1.
    assign whses.zcnt = x
           whses.zwhse = icsd.whse.
    end.       
end.


            
for each icsp  use-index k-icsp
   where icsp.cono = g-cono 
/*     and icsp.prod = "mp602514e50" */
     and icsp.statustype <> "l"
     and icsp.enterdt < (today - 730) 
     and (icsp.prodcat ge begcat and
          icsp.prodcat le endcat)
no-lock:

/*
testcount = testcount + 1.

if testcount > 3000 then
  leave.
*/  
  
v-vendor = 0.
v-pline = "".
v-entdt = icsp.enterdt.
v-invdt = ?.
v-rcptdt = 01/01/1965.
v-fiss = 0.
v-co = 0.
v-buyer = "".
v-future = 0.

ttlusg = 0.
ttlcost = 0.
ttloh = 0.
ftot = 0.
ntot = 0.
dtot = 0.

for each whses use-index k-zpecking no-lock:
 find icsw use-index k-whse
  where icsw.cono = g-cono 
    and icsw.whse = whses.zwhse
    and icsw.prod = icsp.prod no-lock no-error. 
/*    and icsw.enterdt < (today - 730)   */
if not avail icsw then
  next.
 
 
 find first icsd 
     where icsd.cono = g-cono 
       and icsd.whse = icsw.whse 
     no-lock no-error.

 if not avail icsd then
   next.
                               
         
 if icsd.divno lt begdiv or
    icsd.divno gt enddiv then next.

 if icsw.arpvendno lt begved or
    icsw.arpvendno gt endved then next.
/* 
 if v-pline = "" then
    do:
    v-pline = icsw.prodline.
    end.
 */   
 if v-buyer = "" and icsw.prodline <> "" then
   do:
   find first icsl where icsl.cono = g-cono and 
                         icsl.whse = icsw.whse and  
                         icsl.prodline = icsw.prodline and
                         icsl.vendno = icsw.arpvendno no-lock no-error.
   if avail icsl then
     do:
     assign v-buyer = icsl.buyer.
      
     if v-pline = "" then
       assign v-vendor = icsw.arpvendno
              v-pline = icsw.prodline.
     end.
   end.
    
   
 if v-invdt = ? then
    v-invdt = icsw.lastinvdt.
 else if v-invdt < icsw.lastinvdt then
    v-invdt = icsw.lastinvdt.
/*   
 if v-rcptdt = ? then
    v-rcptdt = icsw.lastrcptdt.
 else if v-rcptdt < icsw.lastrcptdt then
    v-rcptdt = icsw.lastrcptdt.
 */


 if v-entdt = ? then
    v-entdt = icsw.enterdt.
 else if v-entdt > icsw.enterdt then
    v-entdt = icsw.enterdt.
 find first icswu 
          where icswu.cono = g-cono 
               and icswu.prod = icsw.prod 
               and icswu.whse = icsw.whse 
        no-lock no-error.
         
        if avail icswu then do:
           
           repeat x = 2 to 13: 
           
              ttlusg = ttlusg + icswu.normusage[x].
                         
           end.
        end.
      
/*
 find first icswu 
          where icswu.cono = 98 
               and icswu.prod = icsw.prod 
               and icswu.whse = icsw.whse 
        no-lock no-error.
         
        if avail icswu then do:
           
           repeat x = 2 to 13: 
           
              ttlusg = ttlusg + icswu.normusage[x].
                         
           end.
        end.
*/      

 for each oeel use-index k-fill 
                   where oeel.cono = g-cono
                     and oeel.statustype = "a"
                     and oeel.invoicedt = ?
                     and oeel.shipprod = icsw.prod 
                     and oeel.whse = icsw.whse no-lock, 
  
     oeeh where oeeh.cono = oeel.cono and oeeh.orderno = oeel.orderno and
                            oeeh.ordersuf = oeel.ordersuf no-lock:
   if avail oeeh then
     if oeeh.transtype <> "qu" and
       (oeeh.stagecd = 0  and (oeeh.transtype = "bl" or
                              oeeh.transtype = "fo") or                                 oeeh.stagecd < 4 and oeeh.transtype = "do") and       
       
       oeel.statustype = "a" and 
       not(oeel.specnstype = "l" and oeel.statustype = "c") then
    
     if not (oeeh.transtype = "bl" and ((oeel.qtyord - oeel.qtyrel) le 0)) then
       if oeeh.transtype = "bl" then 
         v-future = v-future + (oeel.qtyord - oeel.qtyrel).
       else
         v-future = v-future + oeel.qtyord.
 end.

 v-co = 0.
 if icsw.qtycommit > 0 then
   v-co = v-co + icsw.qtycommit.
 

 if icsw.qtyreservd > 0 then
   v-co = v-co + icsw.qtyreservd.
  
 
 
 if icsw.qtybo > 0 then
   v-co = v-co + icsw.qtybo.
 
 v-avl = icsw.qtyonhand - (v-co).
                         

 ftot  = ftot + v-future.


 ttloh = (ttloh + v-avl).
        
 ttlcost = ttlcost + (v-avl
               * icsw.avgcost).
 tinvvalue = tinvvalue + (icsw.qtyonhand * icsw.avgcost).
 end.

for each icsd where icsd.cono = 1 no-lock:  
  for each icenl use-index k-icenl
                 where icenl.cono = g-cono and
                       icenl.typecd = "n" and
                       icenl.whse = icsd.whse and
                       icenl.prod = icsp.prod and
                       icenl.ordtype = "o" and
                       icenl.entrytype = false and
                       icenl.quantity <> 0 no-lock:
         
      if v-invdt = ? then
         v-invdt = icenl.postdt.
      else if v-invdt < icenl.postdt then
         v-invdt = icenl.postdt.


      if icenl.postdt > (today - 365) then
        do:                 
     /*   ttlusg = ttlusg + (icenl.quantity * -1).   */
        ntot   = ntot + (icenl.quantity * -1).
        end.
  end.
end.        


for each icsd where icsd.cono = 1 no-lock:  
  for each icenl use-index k-icenl
                 where icenl.cono = g-cono and
                       icenl.typecd = "n" and
                       icenl.whse = icsd.whse and
                       icenl.prod = icsp.prod and
                       icenl.ordtype = "f" and
                       icenl.entrytype = false and
                       icenl.quantity <> 0 no-lock:
         
      if v-invdt = ? then
         v-invdt = icenl.postdt.
      else if v-invdt < icenl.postdt then
         v-invdt = icenl.postdt.


      if icenl.postdt > (today - 365) then
        do:                 
     /*   ttlusg = ttlusg + (icenl.quantity * -1).   */
        ntot   = ntot + (icenl.quantity * -1).
        end.
  end.
end.        



for each icsd where icsd.cono = 1 no-lock:  
  for each icenl use-index k-icenl
                 where icenl.cono = g-cono and
                       icenl.typecd = "d" and
                       icenl.whse = icsd.whse and
                       icenl.prod = icsp.prod and
                       icenl.ordtype = "o" and
                       icenl.entrytype = false and
                       icenl.quantity <> 0 no-lock:

      
             
      if v-invdt = ? then
         v-invdt = icenl.postdt.
      else if v-invdt < icenl.postdt then
         v-invdt = icenl.postdt.
 
      if icenl.postdt > (today - 365) then
        do:                 
   /*     ttlusg = ttlusg + (icenl.quantity * -1).    */
        dtot   = dtot + (icenl.quantity * -1).
          
        end.
  end.
end.  /* end ICSW */      
if ttloh > 0 then
  do:
  for each icet where icet.cono = g-cono and
                      icet.prod = icsp.prod and
                      icet.postdt > (today - 365) no-lock: 
    if (can-do("kp,po,oe",icet.module) and icet.transtype = "re") or
       (can-do("va",icet.module) and can-do("re,in,ri",icet.transtype)) then

       do:
       if v-rcptdt = 01/01/1950 then
          v-rcptdt = icet.postdt.
       else   
       if v-rcptdt < icet.postdt then
          v-rcptdt = icet.postdt.  
       end.
    else   
    if (icet.module = "ic" and icet.transtype = "sa")  and
        icet.stkqtyship < 0 then
      ftot = ftot + (icet.stkqtyship * -1).
  end.
 end.     
 
 if v-rcptdt > (today - 365) then
    v-rcptdt = v-rcptdt.
 else
   do:         
   create itemz.
   assign itemz.zprod = icsp.prod
        itemz.zdescrip = icsp.descr[1]
        itemz.zvendor = v-vendor
        itemz.zusage = ttlusg
        itemz.zbuyer = v-buyer
        itemz.zdtot  = dtot
        itemz.zntot  = ntot
        itemz.zftot   = ftot
        itemz.zonhand = ttloh
        itemz.zcost = ttlcost
        itemz.zentdt = v-entdt
        itemz.zinvdt = v-invdt
        itemz.zrcptdt = v-rcptdt
        itemz.zwhse = " "
        itemz.zpline = v-pline.
    end.  
end.
        

  ttlusg = 0.
  ntot   = 0.
  dtot   = 0.  
  ftot   = 0.
  ttlcost = 0.
/*  tinvvalue = 0.      */
  ttloh = 0.

 run bkheader.

 for each itemz use-index i-line no-lock 
   break by itemz.zpline:

 if itemz.zbuyer > endby or
    itemz.zbuyer < begby then
    next.
    
/*  
 if itemz.zprod = "0811404001" then
 display "found2 " itemz.zentdt with frame y.
*/ 
if itemz.zentdt > (today - 730) then
   next.

/*  
 if itemz.zprod = "0811404001" then
  display "found3 "  with frame z.
*/ 

 ttloh = (ttloh + itemz.zonhand).
 ttlusg = ttlusg + (itemz.zusage + itemz.zdtot + itemz.zntot + itemz.zftot).       
 ttlcost = ttlcost + itemz.zcost.
/* tinvvalue = tinvvalue + itemz.zcost.    */
/* 
if itemz.zprod = "0811404001" then
  display "found " ttloh ttlusg with frame aa.
*/

/* if last-of(itemz.zprod) then do:    */
    savecost = ttlcost / ttloh.
    if ttlusg le 0 then 
      ttlusg = 0. 
    if ttloh > 0 then 
      do:
      avgdemd = ttlusg / 12.
      if avgdemd <> 0 then
        mos = ttloh / avgdemd.
      else
        mos = 9999.
      end.
    else
      do:
      avgdemd = 0. 
      mos = 0.
      ttlcost = 0.
      end.
        
/*                
  if itemz.zprod = "0811404001" then
     display "found " avgdemd mos ttlusg with frame ac.        
*/  
     wrkoh = ttloh.
     qtyov = 0.
     oh24 = avgdemd * 36.
     oh36 = avgdemd * 48.
     oh60 = avgdemd * 60.
     if ((itemz.zinvdt = ? and itemz.zentdt < today - 730) or
        (itemz.zinvdt <> ? and itemz.zinvdt < today - 730)) 
        and ttlcost > 100 and ttlusg = 0 then
       do:
       excess = wrkoh. 
       qtyov  = excess.
       finalqtyov = finalqtyov + excess.
       plineqtyov = plineqtyov + excess.
       costov = excess * savecost.
       finalcostov = finalcostov + costov.
       plinecostov = plinecostov + costov.
       wrkoh = wrkoh - excess.
       mos = 0.
       qty60 = 0.
       itemz.zov = qtyov.
       run scatter_info.
       end.
        
    if mos > 60 and ttlcost > 100 and wrkoh > 0 then do:
        excess = wrkoh - oh60.
        qty60 = excess.
        finalqty60 = finalqty60 + excess.
        plineqty60 = plineqty60 + excess.
        cost60 = excess * savecost.
        finalcost60 = finalcost60 + cost60.
        plinecost60 = plinecost60 + cost60.
        wrkoh = wrkoh - excess.
        itemz.zqty60 = qty60.
        if qtyov = 0 then
          run scatter_info.
     end.
     if mos > 48 and wrkoh > 0 and ttlcost > 100 then do:
        excess = wrkoh - oh36.
        qty36 = excess.
        finalqty36 = finalqty36 + excess.
        plineqty36 = plineqty36 + excess.
        cost36 = excess * savecost.
        finalcost36 = finalcost36 + cost36.
        plinecost36 = plinecost36 + cost36.
        wrkoh = wrkoh - excess.
     end.
     if mos > 36 and wrkoh > 0 and ttlcost > 100 then do:
        excess = wrkoh - oh24.
        qty24 = excess.
        finalqty24 = finalqty24 + excess.
        plineqty24 = plineqty24 + excess.
        cost24 = excess * savecost.
        finalcost24 = finalcost24 + cost24.
        plinecost24 = plinecost24 + cost24.
        wrkoh = wrkoh - excess.
     end.
      if (qty24 ge 1 or qty36 ge 1 or
         qty60 ge 1 or qtyov ge 1) then do:
        if line-counter > 45 then
          run bkheader.
        display 
           itemz.zprod
           itemz.zdescrip 
           itemz.zusage 
           itemz.zdtot
           itemz.zntot
           ttloh 
           savecost
           qty24  
           qty36  
           qty60
           qtyov
           itemz.zinvdt
           itemz.zftot
           cost24
           cost36 
           cost60
           costov
       
        with frame detail-l width 178.
        invvalue = invvalue + (ttloh * savecost).                
     end.        

     assign ttlusg  = 0 
            ttlcost = 0 
            ttloh   = 0 
            qty24   = 0 
            qty36   = 0 
            qty60   = 0
            qtyov   = 0
            cost24  = 0
            cost36  = 0 
            cost60  = 0
            costov  = 0.
/* end. */

 
if last-of(itemz.zpline) then do:
    if plineqty24 ge 1 or plineqty36 ge 1 or plineqty60 ge 1 or
       plineqtyov ge 1 then 
       do:
       if line-counter > 44 then
         run bkheader.
       display
          itemz.zpline 
          plineqty24 
          plineqty36 
          plineqty60
          plineqtyov
          plinecost24
          plinecost36 
          plinecost60
          plinecostov
       with frame pline-total-l width 178.
       end. 
    assign plineqty24 = 0 
           plineqty36 = 0 
           plineqty60 = 0
           plineqtyov  = 0
           plinecost24 = 0 
           plinecost36 = 0 
           plinecost60 = 0
           plinecostov = 0.
 end.


end.   

if line-counter > 44 then
   run bkheader.
     
display 
   finalqty24 
   finalqty36 
   finalqty60
   finalqtyov
   finalcost24
   finalcost36 
   finalcost60
   finalcostov
   tinvvalue 
   invvalue
with frame total-l width 178.

run whse_reporter.

run audit_reporter.

run ssd_reporter.

/* ------------------------------------------------------------------------*/
PROCEDURE bkheader.
/* ------------------------------------------------------------------------*/

  assign x-page = x-page + 1.
  if page-number ge 1 then
    page.
  display with frame kheadings1 down width 178.

  display
       rpt-dt
       rpt-dow
       rpt-time
       rpt-cono
       rpt-user
       x-page
       with frame kheadings1a no-underline no-box no-labels down width 178.

  display with frame kheadings1b down width 178.
  display with frame kheadings1c down width 178.

  end.


/* ------------------------------------------------------------------------*/
PROCEDURE bkheader_w.
/* ------------------------------------------------------------------------*/

  assign x-page = x-page + 1.
  z-linecount = 5.
  if page-number(xwhsef) ge 1 then
    page stream xwhsef.
  display stream xwhsef with frame kheadings1 down width 178.

  display stream xwhsef
       rpt-dt
       rpt-dow
       rpt-time
       rpt-cono
       rpt-user
       x-page
       with frame kheadings1a no-underline no-box no-labels down width 178.

  display stream xwhsef with frame kheadings1b down width 178.
  display stream xwhsef with frame kheadings1c_w down width 178.

  end.



/* ---------------------------------------------------------------------- */
PROCEDURE scatter_info.
/* ---------------------------------------------------------------------- */
  for each icsw use-index k-icsw
          where icsw.cono = g-cono and
                icsw.prod = itemz.zprod and
                icsw.qtyonhand > 0 no-lock:
    
    v-co = 0.
    if icsw.qtycommit > 0 then
       v-co = v-co + icsw.qtycommit.
    if icsw.qtyreservd > 0 then
      v-co = v-co + icsw.qtyreservd.
    if icsw.qtybo > 0 then
      v-co = v-co + icsw.qtybo.
    v-avl = icsw.qtyonhand - (v-co).
    if v-avl le 0 then                    
      next.
    
    create itemw.
    assign itemw.zprod = itemz.zprod 
           itemw.zdescrip = itemz.zdescrip 
           itemw.zvendor = itemz.zvendor
           itemw.zusage = itemz.zusage 
           itemw.zbuyer = itemz.zbuyer 
           itemw.zdtot  = itemz.zdtot 
           itemw.zntot  = itemz.zntot 
           itemw.zftot   = itemz.zftot 
           itemw.zonhand = itemz.zonhand 
           itemw.zthisavl  = v-avl
           itemw.zcost =  itemz.zcost
           itemw.zentdt =  itemz.zentdt 
           itemw.zinvdt =  itemz.zinvdt 
           itemw.zrcptdt =  itemz.zrcptdt
           itemw.zov     = itemz.zov
           itemw.zqty60  = itemz.zqty60
           itemw.zwhse = icsw.whse
           itemw.zpline = itemz.zpline.
           
    end.  
end.

/* ----------------------------------------------------------------------- */ 
PROCEDURE whse_reporter.
/* ----------------------------------------------------------------------- */ 

 for each itemw use-index w-line no-lock 
  where  
  (itemw.zbuyer le endby and
    itemw.zbuyer ge begby) and
    itemw.zentdt le (today - 730) and
  (itemw.zov > 0 or itemw.zqty60 > 0)


   
   break by itemw.zwhse
         by itemw.zpline:

 
 if first-of (itemw.zwhse) then
   do:
   x-page = 0.
   assign
    ttlusg  = 0 
    ttlcost = 0 
    ttloh   = 0 
    qty24   = 0 
    qty36   = 0 
    qty60   = 0
    qtyov   = 0
    cost24  = 0
    cost36  = 0 
    cost60  = 0
    costov  = 0.
   assign
     plineqty24 = 0 
     plineqty36 = 0 
     plineqty60 = 0
     plineqtyov  = 0
     plinecost24 = 0 
     plinecost36 = 0 
     plinecost60 = 0
     plinecostov = 0.
   assign
     finalqty24  = 0
     finalqty36  = 0
     finalqty60  = 0
     finalqtyov  = 0
     finalcost24 = 0
     finalcost36 = 0
     finalcost60 = 0
     finalcostov = 0
     tinvvalue   = 0
     invvalue    = 0.
   run determine_case_name.
   if x-openstream then
     do:
     output stream xwhsef close.
     x-openstream = false.
     end.
   
   output stream xwhsef to value ("/usr/tmp/"
                                + right-trim (v-basename) + "." + v-filepart).
   x-openstream = true.
   run bkheader_w.
   end.

  if itemw.zov > 0 then  
    do:
    savecost = itemw.zcost / itemw.zonhand.
    excess = itemw.zthisavl.
    qtyov  = excess.
    finalqtyov = finalqtyov + excess.
    plineqtyov = plineqtyov + excess.
    costov = excess * savecost.
    finalcostov = finalcostov + costov.
    plinecostov = plinecostov + costov.
    wrkoh = wrkoh - excess.
    mos = 0.
    end.
  else
    qtyov = 0.
/*        
  if mos > 60 and ttlcost > 100 and itemw.zthisavl > 0 
    and qty60 ge 1 and qtyov = 0 then do:
*/
  if itemw.zqty60 > 0 then
     do:
     savecost = itemw.zcost / itemw.zonhand.
     excess = itemw.zthisavl.
     qty60 = excess.
     finalqty60 = finalqty60 + excess.
     plineqty60 = plineqty60 + excess.
     cost60 = excess * savecost.
     finalcost60 = finalcost60 + cost60.
     plinecost60 = plinecost60 + cost60.
     end.
  else
     qty60 = 0.
  if qty60 ge 1 or qtyov ge 1 then do:
    if z-linecount > 45 then
      run bkheader_w.
      
    v-desc = "".
    find apsv where apsv.cono = g-cono and
                    apsv.vendno = itemw.zvendor no-lock no-error.
    if avail apsv then
      v-desc = apsv.name.
      
    display stream xwhsef
           itemw.zprod
           itemw.zdescrip 
           itemw.zvendor
           v-desc
           itemw.zonhand 
           savecost
           itemw.zwhse
           qty60
           qtyov
           itemw.zinvdt
           cost60
           costov
       
        with frame detail-l_w width 178.
    
     down with frame detail-l_w.   
        invvalue = invvalue + (qty60 * savecost) +
                              (qtyov * savecost).                
        z-linecount = z-linecount + 2.
  
  assign ttlusg  = 0 
         ttlcost = 0 
         ttloh   = 0 
         qty24   = 0 
         qty36   = 0 
         qty60   = 0
         qtyov   = 0
         cost24  = 0
         cost36  = 0 
         cost60  = 0
         costov  = 0.
  end.        

  assign ttlusg  = 0 
         ttlcost = 0 
         ttloh   = 0 
         qty24   = 0 
         qty36   = 0 
         qty60   = 0
         qtyov   = 0
         cost24  = 0
         cost36  = 0 
         cost60  = 0
         costov  = 0.
 
  if last-of(itemw.zpline) then do:
    if plineqty60 ge 1 or
       plineqtyov ge 1 then 
       do:
       if z-linecount > 45 then
         run bkheader_w.
       display stream xwhsef
          itemw.zpline 
          plineqty60
          plineqtyov
          plinecost60
          plinecostov
       with frame pline-total-l_w width 178.
       z-linecount = z-linecount + 4.
       end. 
    assign plineqty24 = 0 
           plineqty36 = 0 
           plineqty60 = 0
           plineqtyov  = 0
           plinecost24 = 0 
           plinecost36 = 0 
           plinecost60 = 0
           plinecostov = 0.
  end.


  if last-of(itemw.zwhse) then
    do: 
    if z-linecount > 45 then
       run bkheader_w.
     
    display stream xwhsef
      finalqty60
      finalqtyov
      finalcost60
      finalcostov
      tinvvalue 
      invvalue
    with frame total-l_w width 178.
    end.
  end.

 if x-openstream then
   do:
   output stream xwhsef close.
   x-openstream = false.
   end.
 
end.      


/* ----------------------------------------------------------------------- */ 
PROCEDURE ssd_reporter.
/* ----------------------------------------------------------------------- */ 

 output stream xcsv to value ("/usr/tmp/" + right-trim(v-basename)                           + ".csv"). 

 output stream xicxm to value ("/usr/tmp/" + right-trim(v-basename)                                          + ".icxm"). 
  

 for each itemw use-index w-csv no-lock 
   break by itemw.zprod:

 if itemw.zov le 0 and itemw.zqty60 le 0 then
   next.
 
 
 if itemw.zbuyer > endby or
    itemw.zbuyer < begby then
    next.
    
if itemw.zentdt > (today - 730) then
   next.

 
 v-whseinx = v-whseinx + 1.
 v-whselist[v-whseinx] = itemw.zwhse + " " +
                         string (itemw.zthisavl,">>>>>9").

if last-of (itemw.zprod) then
    do:
    find apsv where apsv.cono = g-cono and
                    apsv.vendno = itemw.zvendor no-lock no-error.
    if avail apsv then
      v-desc = apsv.name.
    savecost = itemw.zcost / itemw.zonhand.  
    export stream xcsv delimiter  ","
         itemw.zprod
         itemw.zdescrip 
         itemw.zvendor
         v-desc
         itemw.zqty60
         itemw.zov 
         savecost
         v-whselist.
    export stream xicxm delimiter  ","
         itemw.zprod
         itemw.zdescrip 
         itemw.zvendor
         v-desc
         itemw.zqty60
         itemw.zov 
         itemw.zonhand
         today.
 
    
    
    do v-whseinx = 1 to 150:
       v-whselist[v-whseinx] = "".
    end.
    v-whseinx = 0.   
    end.
  end.
end.
/* -------------------------------------------------------------------- */
PROCEDURE determine_case_name.
/* -------------------------------------------------------------------- */
  v-name[1] = substring(itemw.zwhse,1,1).
  v-name[2] = substring(itemw.zwhse,2,1).
  v-name[3] = substring(itemw.zwhse,3,1).
  v-name[4] = substring(itemw.zwhse,4,1).
  do v-nameinx = 1 to 4:
    do v-alphainx = 1 to 26:
      if v-name[v-nameinx] = v-letter[v-alphainx] then
        do:
        v-name[v-nameinx] = v-letter[v-alphainx].
        leave.
        end.
    end.
  end.
v-filepart = "    ".

substring (v-filepart,1,1) = v-name[1].
substring (v-filepart,2,1) = v-name[2].
substring (v-filepart,3,1) = v-name[3].
substring (v-filepart,4,1) = v-name[4].

end.   
/* -------------------------------------------------------------------- */
PROCEDURE audit_reporter.
/* -------------------------------------------------------------------- */
  output stream xaudit to value ("/usr/tmp/" + right-trim(v-basename) 
                                 + ".aud").
  for each itemw use-index w-prod no-lock:
      export stream xaudit delimiter ","
         itemw.zprod 
         itemw.zdescrip 
         itemw.zwhse 
         itemw.zvendor
         itemw.zusage
         itemw.zinvdt 
         itemw.zonhand 
         itemw.zpline
         itemw.zcost
         itemw.zrcptdt.
  end.
end.
