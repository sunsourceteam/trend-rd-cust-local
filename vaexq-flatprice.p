define input parameter ip-cono        like icsw.cono      no-undo.
define input parameter ip-model       as character        no-undo.
define input parameter ip-product     like icsw.prod      no-undo.
define input parameter ip-whse        like icsw.whse      no-undo.
define input parameter ip-customer    like arsc.custno    no-undo.
define input parameter ip-shipto      like arss.shipto    no-undo.
define output parameter ip-pricetype  like icsw.pricetype no-undo.
define output parameter ip-prefix     like icsw.prod      no-undo.
 
define var v-len                   as int              no-undo.
define var v-inx                   as int              no-undo.


define temp-table t-flatprice
  field prefix as char
  field dprefix as char
  field length as integer
  field pricetype  like icsw.pricetype
index ix1
  length descending
  prefix.



for each notes where
         notes.cono         = ip-cono  and 
         notes.notestype    = "<pd"    and
         notes.primarykey   = ip-model  no-lock:
  
  create t-flatprice.
  assign 
    t-flatprice.length = 24
    t-flatprice.prefix = notes.secondarykey
    t-flatprice.pricetype = notes.noteln[2]
    t-flatprice.dprefix   = notes.noteln[4].
end.

for each notes where
         notes.cono         = ip-cono  and 
         notes.notestype    = "<p"    and
         notes.primarykey   = ip-model  no-lock:
  
  assign v-len = length(notes.secondarykey).
  create t-flatprice.
  assign 
    t-flatprice.length = v-len
    t-flatprice.prefix = notes.secondarykey
    t-flatprice.pricetype = notes.noteln[2].
end.
         
find first t-flatprice use-index ix1 where 
  ip-product begins t-flatprice.prefix no-lock no-error.

if avail t-flatprice then do:
  assign ip-pricetype = t-flatprice.pricetype
         ip-prefix = if t-flatprice.dprefix <> "" then
                       t-flatprice.dprefix         
                     else
                       t-flatprice.prefix.         



end.         
