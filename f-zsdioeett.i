/*h****************************************************************************
  INCLUDE      : f-oeett.i
  DESCRIPTION  : Tendering display
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    12/21/01 gp  ; TB# e10368 Back order tendering no lines
                ******************************************************************************/


form
  v-shplabel              at 1  no-label
  s-shplineamt            colon 18 label "Line Items"
  v-ordlabel          at 39 no-label
  s-ordlineamt        colon 61 label "Line Items"
  s-shpdiscamt            colon 18 label "Discount"
  s-orddiscamt        colon 61 label "Discount"
  s-shpaddonamt           colon 18 label "Addons"
  s-ordaddonamt       colon 61 label "Addons"
  s-shptaxamt             colon 18 label "Tax"
  s-ordtaxamt         colon 61 label "Tax"
  "-------------"         at 20
  "-------------"         at 63
  s-shptotalamt           colon 18 label "Total"
  s-ordtotalamt       colon 61 label "Total"
  skip(1)
  v-tendqtyfl             colon 18 label "Totals Based On"
    help "Amounts Based Off (O)rdered or (S)hipped Quantities"
  v-paidlabel         at 51 no-label
  v-tottendamt        at 63 no-label
  v-dwnpmtamt             colon 18 label "Down Payment"
  v-dwnpmttype            at 34 no-label
  s-dwnpmtamt             at 36 no-label
  s-paid              colon 61 label "Paid"
  v-codfl                 colon 18 label "COD"
  s-amtdue                colon 61 label "Amount Due"
  v-sdiTenderfl           colon 18 label "Tender ?"
  "Payment Type"          at 2
  "Amount"                at 24
  "Payment #"             at 33
  "Authorize #"           at 58
  "Check #"               at 70
  x-mediacd[1]            at 4 no-label
    {f-help.i}
  s-mediacd[1]            at 6 no-label
  s-payamt[1]             at 19 no-label
  v-media[1]              at 33 no-label
  v-mediaauth[1]          at 60 no-label
  v-checkno               at 70 no-label
  x-mediacd[2]            at  4 no-label
   {f-help.i}
  s-mediacd[2]            at  6 no-label
  s-payamt[2]             at 19 no-label
  v-media[2]              at 33 no-label
  v-mediaauth[2]          at 60 no-label
  x-mediacd[3]            at  4 no-label
   {f-help.i}
  s-mediacd[3]            at  6 no-label
  s-payamt[3]             at 19 no-label
  v-media[3]              at 33 no-label
  v-mediaauth[3]          at 60 no-label
with frame f-oeett row 5 width 80 side-labels overlay
