/***************************************************************************
 PROGRAM NAME: n-zicszlu.i
  PROJECT NO.: 00-44
  DESCRIPTION: This is an include for zicspxlu.p new p/n lookup 
 DATE WRITTEN: 06/12/00
       AUTHOR: Sir Carl E. Graul Esq.
     MODIFIED: 

***************************************************************************/

/* ########################## TREND INCLUDES ######################### */


/* ########################### DEFINITIONS ########################### */


/* ############################## LOGIC ############################## */



find {1} w-prod where
     {2}
     w-prod.prod >= v-start
     /{2}* */
     no-lock no-error.


SkipInactive:

do while true :

  if available w-prod then
    do:
    {w-zicsp.i w-prod.prod no-lock}
    if not avail icsp then
      do:
      if "{1}" = "first" or 
         "{1}" = "next" then
        do:
        find next w-prod where
             {2}
            w-prod.prod >= v-start
             /{2}* */
            no-lock no-error.
        end.    
      else
      if "{1}" = "last" or 
         "{1}" = "prev" then
        do:
        find prev w-prod where
           {2}
          w-prod.prod >= v-start
           /{2}* */
         no-lock no-error.
        end.
      else
        leave SkipInactive.
      next SkipInactive.
      end.
    leave SkipInactive.
    end.
  else
    leave SkipInactive.
  end.

if available icsp  then
  do:
    {w-icsw.i icsp.prod g-whse no-lock}
    if available icsw then
      do:
        {p-netavl.i "/*"}      
      end. /* if available icsw */
    assign
        s-prod    = icsp.prod
        s-notesfl = icsp.notesfl
        s-descrip = icsp.descrip[1] + icsp.descrip[2]
        s-txt = (if can-do("i,s",icsp.statustype) then icsp.statustype
                else "") +
                (if not available icsw then "-" else "")
        s-qty = if icsp.statustype        = "l" then "  Labor"  
                else if icsp.kittype    = "b" then "    Kit"  
                else if not available icsw then    "    N/A"  
                else if s-netavail > 9999999 then  "+++++++"  
                else if s-netavail < 0       then  "      0"  
                else string(truncate(s-netavail,0),"zzzzzz9").
  end. /* if available icsp */
else
  assign s-txt = ""
         s-qty = ""
         s-descrip = v-invalid.
  

/* ############################ EOJ STUFF ############################ */


/* ######################### END OF PROGRAM ########################## */ 
/* ####################### INTERNAL PROCEDURES ####################### */


