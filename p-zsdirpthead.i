def var v-daylist    as c format "x(3)"
    initial "Sun,Mon,Tue,Wed,Thu,Fri,Sat"               no-undo.
def var v-string     as c format "x(78)"     no-undo.
def var v-rpttitle   like sapb.rpttitle      no-undo.
def var v-functproc  like sapb.currproc      no-undo.
def var v-dayofweek  as c format "x(3)"      no-undo.
def var v-stop       as logical              no-undo.
def var v-rptno      as i                    no-undo.
def var v-printdir   like sasc.printdir      no-undo.
def var v-jobnm      like sapb.jobnm         no-undo.
def var v-wide       like sassr.wide         no-undo.
def var v-pagedfl    like sassr.pagedfl      no-undo.
def var v-pglength   like sassr.pglength     no-undo.
def var v-optpgfl    like sassr.optpgfl      no-undo.
def var v-headerprfl like sassr.headerprfl   no-undo.
def var v-titletype  like sassr.titletype    no-undo.
def var v-filefl     like sapb.filefl        no-undo.
def var v-printernm  like sapb.printernm     no-undo.
def var conm         like sasc.conm          no-undo.
def var license-to   like sasa.licenseto     no-undo.
    
form header
    today                                 at 1
    v-dayofweek                           at 10
    string(time,"hh:mm")  format "x(5)"   at 14
    "Co: "                                at 21
    string(g-cono) format "x(4)"          at 25
    conm                                  at 30
    license-to                            at 70
    v-functproc                           at 104
    "Oper: "                              at 111
    g-operinit                            at 117
    "Page: "                              at 122
    string(page-number)  format "x(5)"    at 128
    v-rpttitle                            at 41 skip(1)
    with frame f-head132 no-box page-top width 132.
 find sassr where recid(sassr) = v-sassrid no-lock no-error.
 find sapb  where recid(sapb)  = v-sapbid  no-lock no-error.
 {w-sasa.i no-lock}
 {w-sasc.i no-lock}
 assign
   conm          = sasc.conm
   v-dayofweek   = entry(weekday(today),v-daylist)
   license-to    = sasa.licenseto
   v-rpttitle    = if sassr.titletype = yes then
                     sassr.rpttitle 
                   else 
                     sapb.rpttitle
   v-functproc   = sapb.currproc.
                
           
