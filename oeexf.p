/******************************************************************************
  PROCEDURE     : oeexf.p
  DESCRIPTION   : Custom Quote Follow Up
  AUTHOR        : SunSource
  DATE WRITTEN  : 05/01/07
  CHANGES MADE  : 
  
  
  
******************************************************************************/
{g-all.i}
{g-oe.i}
{g-ic.i}
{g-inq.i}
{g-oestg.i}
{g-oeetk.i "new"}

def buffer flwup for notes.
def new shared var v-quoteno         as integer format ">>>>>>>9"  no-undo.
def new shared var v-lineno          like oeel.lineno              no-undo.

def new shared var x-quoteno         as c format "x(8)"             no-undo.
def            var x-seqno          like oeelb.seqno            no-undo.
def new shared var v-stkqtyship     like oeel.stkqtyship        no-undo.
def new shared var s-prod           like oeel.shipprod          no-undo.
def new shared var s-returnfl       like oeel.returnfl          no-undo.

def var x-key          as integer            no-undo.
def var s-rushdesc     as char format "x(4)" no-undo.
def var v-errfl        as logical            no-undo.
def var o-orderno      like g-orderno        no-undo.
def var s-stagecd      as c format "x(3)"    no-undo.
def var s-botype       like oeel.botype      no-undo.
def var s-lineno        like oeel.lineno        no-undo.
def var s-orderdisp     as c format "x(14)"     no-undo.
def var s-formend       as c format "x"         no-undo.
def var s-com           as c format "x"         no-undo.
def var s-netamt        as de format "zzzzzz9.99999-"       no-undo.
def var s-jitflag       as c format "x"                     no-undo.
def var s-descrip       like oeel.proddesc                  no-undo.
def var s-core          as c format "x(25)"                 no-undo.
def var s-alttxt        as c format "x(12)"                 no-undo.
def var v-putscreen     as c format "x(76)"                 no-undo.
def var w-currproc      like sassm.currproc                 no-undo.
def var w-ourproc       like sassm.currproc                 no-undo.
def var blank-line1     as c format "x(1)"                  no-undo.
def var blank-line3     as c format "x(1)"                  no-undo.
def var blank-line4     as c format "x(1)"                  no-undo.
def var blank-line5     as c format "x(1)"                  no-undo.
def var blank-line6     as c format "x(1)"                  no-undo.
def var blank-line7     as c format "x(1)"                  no-undo.
def var blank-line8     as c format "x(1)"                  no-undo.
def var blank-line9     as c format "x(1)"                  no-undo.
def var blank-line10    as c format "x(1)"                  no-undo.
def var blank-line11    as c format "x(1)"                  no-undo.
def var blank-line12    as c format "x(1)"                  no-undo.
def var blank-line13    as c format "x(1)"                  no-undo.
def var blank-line14    as c format "x(1)"                  no-undo.
def var blank-line15    as c format "x(1)"                  no-undo.
def var blank-line16    as c format "x(1)"                  no-undo.
def var blank-line17    as c format "x(1)"                  no-undo.
def var blank-line18    as c format "x(1)"                  no-undo.
def var blank-line19    as c format "x(1)"                  no-undo.
def var blank-line20    as c format "x(1)"                  no-undo.
def var blank-line21    as c format "x(1)"                  no-undo.
def var blank-line22    as c format "x(1)"                  no-undo.
def var blank-line23    as c format "x(1)"                  no-undo.

/*tb 24775 04/13/98 sbr; Added the variable definition for v-oerebty. */
def     shared var v-oerebty      like sasoo.oerebty                   no-undo.

def var v-fkey         as c format "x(5)" extent 5 initial
    ["    ","    ","     ","","     "] no-undo.

def var v-msg as c format "x(78)" initial
" Ln# N DO Product                  Unit  Ordered    Cost           Net Price "
                                        no-undo.
def var v-fmsg as c format "x(78)" initial
"F6-Create FlwUp   F7-View FlwUp   F8-oeizo   F9-oeexq                       ".

def frame blank-screen                             
  blank-line1   at 2 no-label
  blank-line3   at 2 no-label                      
  blank-line4   at 2 no-label                      
  blank-line5   at 2 no-label                      
  blank-line6   at 2 no-label                      
  blank-line7   at 2 no-label                      
  blank-line8   at 2 no-label                      
  blank-line9   at 2 no-label                      
  blank-line10  at 2 no-label                      
  blank-line11  at 2 no-label                      
  blank-line12  at 2 no-label                      
  blank-line13  at 2 no-label                      
  blank-line14  at 2 no-label
  blank-line15  at 2 no-label                      
  blank-line16  at 2 no-label                      
  blank-line17  at 2 no-label                      
  blank-line18  at 2 no-label                      
  blank-line19  at 2 no-label                      
  blank-line20  at 2 no-label                      
  blank-line21  at 2 no-label                      
  blank-line22  at 2 no-label                      
  blank-line23  at 2 no-label
  with overlay no-labels no-box width 80 column 1 row 1.

def var s-orderno like oeel.orderno no-undo.  
def var s-ordersuf like oeel.ordersuf no-undo.

if g-ourproc = "oeexf" then                    
  assign x-checkfocus = true
         x-quoteno    = right-trim(string(v-quoteno)).

v-length = 6.

form
    "Quote #:"                         at  2
    g-orderno                          at  11 
       {f-help.i}
    "q"                                at  19 
    oeehb.notesfl                      at  22 
    "Type:"                            at  30
    oeehb.transtype                    at  36 
    "Stage:"                           at  42
    s-stagecd                          at  49
    "Enter:"                           at  64
    oeehb.enterdt                      at  71
    "Taken By:"                        at  26
    oeehb.takenby                      at  36
    "BO's:"                            at  43
    oeehb.bofl                         at  49
    "Req:"                             at  66
    oeehb.reqshipdt                    at  71
    "Cust #:"                          at   3
    oeehb.custno                       at  11
    arsc.notesfl                       at  23 
    arsc.lookupnm                      at  25
    "Disp:"                            at  43
    s-orderdisp                        at  49
    "Prom:"                            at  65
    oeehb.promisedt                    at  71
    "Ship To:"                         at   2
    oeehb.shipto                       at  11
    arss.notesfl                       at  19
    "Total Order:"                     at  36
    oeehb.totinvamt                    at  49
    "CanDt:"                           at  64
    oeehb.canceldt                     at  71
 /*   skip(1) */
    v-msg                             at 1  no-label skip(13)
    s-formend                         at 1  no-label
  with frame f-oeexfs no-underline no-labels title g-title width 80 overlay.
                               /*side-labels*/
form
    s-com                                at 1
    oeelb.lineno                         at 2
       help ""
    oeelb.specnstype                     at 6
    s-botype                             at 8
    oeelb.shipprod                       at 11
    icsp.notesfl                         at 35
    oeelb.unit                           at 36
    oeelb.qtyord    format "zzzzzz9.99-" at 40
    oeelb.prodcost  format "zzzzzz9.99-" at 51
    s-netamt                             at 65
    s-rushdesc                           at  1
    s-jitflag                            at 6
    s-descrip                            at 11
    s-core                               at 36
    s-alttxt                             at 67
  with frame f-oeexfd overlay no-labels width 78 column 2 scroll 1
                      no-box row 7 v-length down.



/*** Event Declarations ***/
on f12 of g-orderno
  do:
  run oeexqlkup.p(output x-quoteno).
  assign g-orderno = int(x-quoteno).
  display g-orderno with frame f-oeexfs.
  next-prompt g-orderno with frame f-oeexfs.
  next.
end.


/*** Main Logic ***/
if g-ourproc ne "oeexf" then v-fkey = "".
{w-sasoo.i g-operinits no-lock}

main:
do while true with frame f-oeexfs on endkey undo main, leave main:
    if {k-jump.i} then leave main.
    hide frame f-oeexf.
    view frame f-oeexfs.

    display v-msg with frame f-oeexfs.
    color display input v-msg with frame f-oeexfs.
    /*tb 19900 12/20/95 gp;  Function keys display incorrect */
    {putscr.gsc &color    = "messages"
                &blankout = "*"}

    if v-errfl = no then hide message no-pause.
    v-errfl = no.

    update
        g-orderno
        {f-help.i}
    with frame f-oeexfs editing:
        readkey.
        if {k-after.i} or {k-func.i} then
            assign g-orderno = input g-orderno.
                   x-quoteno = right-trim(string(g-orderno)).
        apply lastkey.
    end.
    
    find oeehb where oeehb.cono = g-cono and
                     oeehb.batchnm = string(g-orderno,">>>>>>>9") and
                     oeehb.sourcepros = "Quote"
                     no-lock no-error.

    if avail oeehb then
      assign x-seqno = oeehb.seqno.
    if int(x-quoteno) <> g-orderno then
      assign x-checkfocus = true.                                  
    
    if x-checkfocus then                                           
      do:                                                          
      run oeexqCustFocusPop.p(input g-cono, 
                              input 0,
                              input 0,
                              input oeehb.custno,
                              input oeehb.shipto).
      assign x-checkfocus = false.                                 
    end.                                                         
    
    /* das - New Customer Mod */
    {zsdiInewcust.i &frame  = f-oeexfs
                    &custno = oeehb.custno}
    
    if not avail oeehb then do:
        run err.p(5685). /*5685 This is Not a Quote Order Type*/
        v-errfl = yes.
        next.
    end.

    if can-do("ra",oeehb.transtype) then do:
        run err.p(5688).
        v-errfl = yes.
        next.
    end.

    if {c-chan.i} and g-oecustno ne oeehb.custno then do:
        run err.p(1100).
        v-errfl = yes.
        next.
    end.
    
    put screen row 21 col 3 color message v-fmsg.

    display oeehb.notesfl with frame f-oeexfs.
    {w-arsc.i oeehb.custno no-lock}
    if not avail arsc then do:
        run err.p(4303).
        v-errfl = yes.
        next.
    end.

    /*tb 5366 05/11/99 cm; Added shipto notes */
    if oeehb.shipto <> "" then
        {w-arss.i oeehb.custno oeehb.shipto no-lock}

    hide message no-pause.
    assign s-orderdisp = if oeehb.orderdisp = "t" then "Tag and Hold"
                         else if oeehb.orderdisp = "s" then "Ship Complete"
                         else if oeehb.orderdisp = "w" then "Will Call"
                         else if oeehb.orderdisp = "j" then "Just In Time"
                         else "".
    s-stagecd   = string(oeehb.stagecd,">>9").
    
    /*tb e7852 09/15/00 lcg;  Multi-Currency. Added currency display  */
    display
        oeehb.custno
        arsc.notesfl
        arsc.lookupnm
        oeehb.shipto
        "" @ arss.notesfl
        arss.notesfl when oeehb.shipto <> "" and avail arss
        oeehb.transtype
        oeehb.takenby
        s-stagecd
        oeehb.bofl
        s-orderdisp
        oeehb.totinvamt
        oeehb.enterdt
        oeehb.reqshipdt
        oeehb.promisedt
        oeehb.canceldt
    with frame f-oeexfs.

    /*tb 24775 04/13/98 sbr; Added w-sasoo.i, and assign statement */
    {w-sasoo.i g-operinit no-lock}
    assign
        v-oerebty     = if avail sasoo then sasoo.oerebty else "n".

    assign s-lineno = 1.
    repeat:
        {xxilx.i &find    = "n-oeexf.i"
                 &frame   = "f-oeexfd"
                 &file    = "oeelb"
                 &field   = "oeelb.lineno"
                 &display = "d-oeexf.i"
                 &usecode = *
                 &usefile = "a-oeexf.i"
                 &where   = "w-oeexf.i"
                 &global  = "g-oelineno = integer(frame-value)"}.
    end.
    leave main.
end.

on cursor-up   back-tab.
on cursor-down tab.


/*tb 21318 01/21/97 jl; Added k-cancel.i to allow for hide of frames */
if ({k-jump.i} or {k-cancel.i}) and substring(program-name(2),1,4) ne "jump" 
  then do:
    hide frame f-oeexf.
    hide frame f-oeexfs.
    return. 
end.

