/*h*****************************************************************************
  INCLUDE         : saag.led
  DESCRIPTION     : Update authsecure with grant/deny status
  USED ONLY ONCE  : yes
  AUTHOR          : mrp
  CHANGES MADE    :
    02/29/00 mrp; TB# 27105 Developed Authtrans update screen.
    03/08/00 bm;  TB# 27105 Revised due to cross locking.
    04/28/00 bm;  TB# 27105 Code review changes.
    
    05/21/09 TAH001; Log cpability back to order .          *******************************************************************************/
/*d assign value to screen variables */
  assign
     s-status  = b-tt-authtrans.authstatus.

    /*d Display line on down frame */
  display s-status with frame f-saagbl.

    /*d update status field */
  upd:
  do while true on endkey undo upd, leave upd on error undo upd, leave upd:

    update s-status with frame f-saagbl.

    if not can-do("A,G,D":u,s-status) then do:
      run err.p(1184).
      next upd.
    end.
    else do for authtrans:
/* Check If authorization transaction status
   is still available for updating.
   If it is not then display appropriate error */
      find authtrans
        where recid(authtrans) = b-tt-authtrans.authtransrecid
        no-lock no-error.
      v-errorno = if available authtrans         and
                    authtrans.authstatus = "c":u
                    then 1177
                  else if available authtrans    and
                    authtrans.authstatus = "g":u
                    then 1178
                  else if available authtrans    and
                    authtrans.authstatus = "d":u
                    then 1179
                  else if available authtrans    and
                    authtrans.authstatus = "u":u
                    then 1180
                  else if not available authtrans
                    then 1182
                  else 0.
      if v-errorno > 0 then do:
        run err.p(v-errorno).
        next upd.
      end.  /* if not avail authtrans */
    end.  /* do for authtrans */

    leave upd.
    end.

/*d change mode updating */
    if not {k-cancel.i} and not {k-jump.i} then do:
      assign b-tt-authtrans.authstatus  = s-status.

      if can-do("A,G,D":u,s-status) then do for authtrans:
        find authtrans
             where recid(authtrans) = b-tt-authtrans.authtransrecid
             no-lock no-error.

/* If authorization transaction status is attempted
   OK to change authsecure to Granted */
      if avail authtrans and authtrans.authstatus = "a":u
        then do for b2-authsecure:
        {authsecure.gfi &cono       = g-cono
                        &oper2      = b-tt-authtrans.oper2
                        &ourproc    = b-tt-authtrans.ourproc
                        &key1       = b-tt-authtrans.key1
                        &key2       = b-tt-authtrans.key2
                        &mode       = b-tt-authtrans.mode
                        &transtype  = b-tt-authtrans.transtype
                        &buf        = "b2-"
                        &lock       = "exclusive"}

/* TAH001 Sunsource Modification for carrying reson information to log file */

        if not avail b2-authsecure then do:
          create b2-authsecure.
          assign
            b2-authsecure.cono    = g-cono
            b2-authsecure.oper2   = b-tt-authtrans.oper2
            b2-authsecure.ourproc = b-tt-authtrans.ourproc
            b2-authsecure.key1    = b-tt-authtrans.key1
            b2-authsecure.key2    = b-tt-authtrans.key2
            b2-authsecure.mode    = b-tt-authtrans.mode
            b2-authsecure.transtype = b-tt-authtrans.transtype
            b2-authsecure.securcd   = 1.
        end.
        assign
          b2-authsecure.user1       = if b2-authsecure.authtransid = 
                                         int(recid(authtrans)) then
                                        b2-authsecure.user1   
                                      else
                                        ""
          b2-authsecure.user2       = if b2-authsecure.authtransid = 
                                         int(recid(authtrans)) then
                                        b2-authsecure.user2   
                                      else
                                        ""
          b2-authsecure.authtransid = int(recid(authtrans))
          b2-authsecure.authstatus  = b-tt-authtrans.authstatus
          b2-authsecure.actionby    = g-operinit
          b2-authsecure.actiondt    = today
          b2-authsecure.actiontm    =
            substring(string(time,"hh:mm:ss":U),1,2) +
            substring(string(time,"hh:mm:ss":U),4,2) +
            substring(string(time,"hh:mm:ss":U),7,2).
        {t-all.i b2-authsecure /*}

        if s-status = "G" and 
          b2-authsecure.key2 begins "ChgCheck" and
          b2-authsecure.ourproc = "oeet" then do:
          run zsdiauthcd.p (input-output b2-authsecure.user1,
                            input-output b2-authsecure.user2). 
          enable s-status with frame f-saagbl.

        end.



        end.  /* if avail authtrans and authtrans.authstatus = "a"  */
      end.  /*if can-do("A,G,D",s-status) */
    end.  /* change mode updating. Did not cancel or jump */
    /* cancel or jump */
    else do:
      if not v-scrollfl then 
        clear frame f-saagbl.
      assign s-status   = b-tt-authtrans.authstatus
             v-scrollfl = yes.
      display s-status with frame f-saagbl.
      next dsply.
    end. /* cancel or jump */
