/******************************************************************************
 ar_defvars_lookup.i
 Include for customer lookups. to fill in the bottom of the screen with 
  additional customer information.
  
*****************************************************************************/

/* SDI custom loookup variables */
def buffer z-arsc for arsc.
def buffer z-arss for arss.
def {1} shared var zu-shipto   as char format "x(04)"            no-undo.
def {1} shared var zu-custno   like arss.custno                  no-undo.
def {1} shared var lu-custno        like arsc.custno  initial 0  no-undo.
def {1} shared var lu-name          as c format "x(30)" dcolor 3 no-undo.
def {1} shared var lu-addr1         as c format "x(30)"          no-undo.
def {1} shared var lu-restaddr      as c format "x(60)"          no-undo.
def {1} shared var lu-city          as c format "x(20)"          no-undo.
def {1} shared var lu-state         like arsc.state              no-undo.
def {1} shared var lu-repout        like arsc.slsrepout          no-undo.
def {1} shared var lu-repnm1        as c format "x(60)"          no-undo.
def {1} shared var lu-repnm2        as c format "x(30)"          no-undo.
def {1} shared var lu-repin         like arsc.slsrepout          no-undo.
def {1} shared var lu-crmgr         as c format "x(04)"          no-undo.
def {1} shared var lu-crmgrnm       as c format "x(21)"          no-undo.
def {1} shared var lu-slsytd      like arsc.salesytd dcolor 3    no-undo.
def {1} shared var lu-slslstyr    like arsc.salesytd dcolor 3    no-undo.
def {1} shared var lu-shiptos     as   char format "x(14)"      no-undo.
 

/* NEW */
def {1} shared var lu-row         as integer   init 11           no-undo.
/* NEW */



def {1} shared frame f-zcustinfo
    "Name:"         at 1
    lu-name         at 6
    "SlsYtd/Lstyr:" at 40
    lu-slsytd       at 53
    lu-slslstyr     at 67
    lu-addr1        at 6                           
    lu-shiptos      at 66    
    lu-restaddr     at 6 format "x(55)"
    

    "Rep Out:"      at 1 
    lu-repout       at 10
    lu-repnm1       at 16 
    "Rep In :"      at 1 
    lu-repin        at 10
    lu-repnm2       at 16  
    "CrMgr:"        at 47
    lu-crmgr        at 54
    lu-crmgrnm      at 59 

    
    with width 80 row lu-row centered no-box overlay no-labels.

