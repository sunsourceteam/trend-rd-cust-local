/* p-oerr5.i 1.2 08/25/97 */
/*h*****************************************************************************
  INCLUDE      : p-oerr5.i
  DESCRIPTION  : Displays line totals by order type for OERR.P
  USED ONCE?   : No
  AUTHOR       : kjb
  DATE WRITTEN : 04/10/95
  CHANGES MADE :
    04/10/95 kjb; TB# 16383 Display the totalling information based on the lines
    08/25/97 kjb; TB# 22385 OERR totals do not include rebates
*******************************************************************************/
/*o If changes are made in this routine, please make sure to review p-oerr4.i
    and p-oere4.i to determine if the changes are required there also. */

page.
display with frame f-oerrf.
v-count = 0.

do i = 1 to 10:

    /*tb 22385 08/25/97 kjb; Set the rebate indicator if the margin for the 
        current order type was calculated with a rebate.  Do not change the
        margin and margin % calculation because the total sales amount and the
        total cost of goods already are adjusted by the customer and vendor
        rebate amounts. */
    assign
        v-sales    = lt-sales[i]
        v-cstgds   = lt-cstgds[i]
        v-gross    = lt-sales[i] - lt-cstgds[i]
        v-totord   = t-totord[i]
        v-count    = lt-count[i]
        v-marg     = if v-oecostsale then
                         if v-sales <> 0
                             then ((v-sales - v-cstgds) / v-sales) * 100
                         else 0
                     else if v-cstgds <> 0
                         then ((v-sales - v-cstgds) / v-cstgds) * 100
                     else 100
        v-ordname  = t-ordtype[i]
        v-rebatefl = lt-rebfl[i]
        v-lines    = t-lines[i].

    /*tb 22385 08/25/97 kjb; Added t-torebatefl which will be set if any of
        the margins were calculated with a rebate */
    if t-ordtype[i] ne "Blanket Order" then
        assign
            t-tosales    = t-tosales  + lt-sales[i]
            t-tocstgds   = t-tocstgds + lt-cstgds[i]
            t-togross    = t-togross  + v-gross
            t-tocount    = t-tocount  + lt-count[i]
            t-tototord   = t-tototord + t-totord[i]
            t-tolines    = t-tolines  + t-lines[i]
            t-torebatefl = if v-rebatefl = "r" then "r"
                           else t-torebatefl.

    /*tb 22385 08/25/97 kjb; Added v-rebatefl to the display */
    display
        v-ordname
        v-count
        v-lines
        v-sales
        v-cstgds       when g-seecostfl = yes
        v-gross
        v-marg
        v-rebatefl
        "           -" @ v-charge
        "           -" @ v-down
        v-totord
    with frame f-oerrf1.
    down with frame f-oerrf1.

    /*tb 22385 08/25/97 kjb; Reset the rebate indicator for the next order 
        type. */
    assign
        v-ordname  = ""
        v-sales    = 0
        v-cstgds   = 0
        v-gross    = 0
        v-marg     = 0
        v-charge   = 0
        v-down     = 0
        v-count    = 0
        v-totord   = 0
        v-rebatefl = ""
        v-lines    = 0.

end. /* do i = 1 to 10 */

t-tomarg = if v-oecostsale then
               if t-tosales <> 0 then
                   ((t-tosales - t-tocstgds) / t-tosales) * 100 else 0
               else if t-tocstgds <> 0 then
                   ((t-tosales - t-tocstgds) / t-tocstgds) * 100 else 100.

/*tb 22385 08/25/97 kjb; Added t-torebatefl to the display */
display
    t-tosales
    t-tocount
    t-tolines
    t-tocstgds      when g-seecostfl = yes
    t-togross
    t-tomarg
    t-torebatefl
    "            -" @ t-tocharge
    "            -" @ t-todown
    t-tototord
    t-lostcnt
    t-lostnet
    t-lostord
    
    s-csrfield
    s-drilldown
    s-ddvalue
with frame f-oerrf2.

/*tb 22385 08/25/97 kjb; Removed the hide of frame f-legend so that the user
    will know what the 'r' indicates on the totals page */

view frame f-totleg.
page.
