/* p-oerr2.i 1.1 01/03/98 */
/* p-oerr2.i 1.5 08/25/97 */
/*h*****************************************************************************
  INCLUDE      : p-oerr2.i
  DESCRIPTION  : Calculate header variables
  USED ONCE?   : no
  AUTHOR       : mwb
  DATE WRITTEN : 09/12/90
  CHANGES MADE :
    06/27/91 mwb; TB# 3670 No return flag on the net sales
    01/15/92 pap; TB# 5225 JIT - add jit flag on display for salesrep & prod
    07/22/92 jlc; TB# 7191 Alpha customer number change
    10/11/95 mcd; TB# 18812-E5a 7.0 Rebates Enhancement (E5a) -
        Display figures in consideration of rebates on the Exception Report
        Added v-totrebamt.
    08/25/97 kjb; TB# 22385 OERR totals do not include rebates
*******************************************************************************/

/*d If can see costs */
if g-seecostfl then do:

    /*d Find v-totlineamt */
    {p-oeord.i "oeeh."}
    
    /*d Find v-totcost */
    {p-oeordc.i "oeeh."}

    /*tb 22385 08/25/97 kjb; Initialize the variables that will hold the
        customer and vendor rebate values.  Also added run of eval-order-lines
        to calculate the total rebate amounts. */
    assign
        v-totcustrebamt = 0
        v-totvendrebamt = 0.
        
    /*d To be able to apply rebates in this report, the user must have SASO
        security to (i)nclude rebates, SMAO must be set to use either customer
        or vendor rebates, and there must be a rebate record associated with
        this order. */
    if v-oerebty      = "i"           and 
       (v-smvendrebfl = yes or 
        v-smcustrebfl = yes)          and
       can-find (first pder use-index k-pder where
                     pder.cono     = g-cono       and
                     pder.orderno  = oeeh.orderno and
                     pder.ordersuf = oeeh.ordersuf)
    then do:
    
        run eval-order-lines in this-procedure (buffer oeeh,
                                                input-output v-totcustrebamt,
                                                input-output v-totvendrebamt).
                         
    end. /* v-oerebty = i ... */
    
    /*tb 18812 10/06/95 mcd; Rebate Enhancement 7.0 (E5-a)*/
    /*  Variable v-totrebamt is the total rebate amount that will impact the
        header provided that user's security is set to view rebates and
        the sasc fields to subtract rebates is set to yes.*/
    /*tb 22385 08/25/97 kjb; Removed all references to v-totrebamt and 
        replaced with v-totcustrebamt and v-totvendrebamt where appropriate.
        v-totrebamt only contained vendor rebates and ignored customer rebates
        (s-margin, s-margpct, s-cost, and v-rebatety).  When rebate values are
        returned from the internal procedure, they have been adjusted for 
        a return but none of the other values have yet, so multiply by -1 
        again to correctly calculate the margin on an RM order. */
    assign
        v-totcustrebamt = v-totcustrebamt * if oeeh.transtype = "rm" then -1 
                                            else 1
        v-totvendrebamt = v-totvendrebamt * if oeeh.transtype = "rm" then -1 
                                            else 1
        v-totlineamt    = if oeeh.lumpbillfl then oeeh.lumpbillamt
                          else v-totlineamt

        s-margin        = v-totlineamt - oeeh.wodiscamt - oeeh.specdiscamt -
                          v-totcustrebamt - v-totcost + v-totvendrebamt

        s-margpct       = if v-oecostsale then
                              if (v-totlineamt    - oeeh.wodiscamt - 
                                  v-totcustrebamt - oeeh.specdiscamt) ne 0
                                  then (s-margin /
                                   (v-totlineamt    - oeeh.wodiscamt -
                                    v-totcustrebamt - oeeh.specdiscamt)) * 100
                              else 0
                          else if (v-totcost - v-totvendrebamt) ne 0 then
                             (s-margin / (v-totcost - v-totvendrebamt)) * 100
                          else 100

        s-cost          = if v-oecostsale then
                              v-totlineamt    - oeeh.wodiscamt - 
                              v-totcustrebamt - oeeh.specdiscamt
                          else if v-oecostsale = no then 
                              v-totcost - v-totvendrebamt
                          else s-cost

        v-rebatety      = if v-totcustrebamt ne 0 or v-totvendrebamt ne 0 
                              then "r"
                          else "".


    assign
        s-margin  = s-margin  * if oeeh.transtype = "rm" then -1 else 1
        s-margpct = s-margpct * if oeeh.transtype = "rm" then -1 else 1
        s-margpct = if s-margpct < 9999.99- then 9999.99-
                    else if s-margpct > 9999.99 then 9999.99
                    else s-margpct.
end.

v-totlineamt = if oeeh.lumpbillfl then oeeh.lumpbillamt
                   else if g-seecostfl then v-totlineamt
                   else oeeh.totlineamt.

/*tb 22385 08/25/97 kjb; Load the totaling arrays with the margin data that
    now takes rebates into account, as well as the customer and vendor rebate
    totals which are needed for the grand total margin calculation.  This code
    will be commented out when totaling has already occurred in OERE.  If the
    order is an RM, multiply by -1. */
{&com_asgn}

assign
    i = 0
    i = lookup(oeeh.transtype,"so,do,cs,rm,cr,bl,br,fo,qu,st").
    
if (i < 11) and (i <> 0) then
    assign 
        t-custreb[i] = t-custreb[i] + (v-totcustrebamt * if i = 4 then -1 
                                                         else 1)
        t-vendreb[i] = t-vendreb[i] + (v-totvendrebamt * if i = 4 then -1
                                                         else 1)
        t-rebfl[i]   = if v-rebatety = "r" then v-rebatety 
                       else t-rebfl[i].

/{&com_asgn}* */

do i = 1 to 4:
    assign
        s-addon = s-addon + oeeh.addonnet[i]
        s-tax   = s-tax   + oeeh.taxamt[i].
end.

{&arsc}
{w-arsc.i oeeh.custno no-lock}
/{&arsc}* */

/*tb 3670 06/27/91 mwb; No return flag on the net sales */
assign s-addon    = s-addon + oeeh.totdatccost
       s-netsale  = (v-totlineamt - oeeh.wodiscamt - oeeh.specdiscamt)
       v-cust     = string(oeeh.custno) + if avail arsc then arsc.notesfl
                    else ""
       s-lookupnm = if avail arsc then arsc.lookupnm
                    else if oeeh.custno = {c-empty.i} then ""
                    else v-invalid
       s-jitflag  = if oeeh.orderdisp = "j" then "jit" else ""
       s-stagecd  = v-stage[oeeh.stagecd + 1].

