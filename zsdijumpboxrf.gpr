/* zsdijumpboxrf.gpr 1.2 07/31/98 */
/*h*****************************************************************************
  INCLUDE      : jumpbox.gpr
  DESCRIPTION  : Standard Jump Logic (derived from j-all.i)
  USED ONCE?   : no
  AUTHOR       : dea
  DATE WRITTEN : 10/11/94
  CHANGES MADE :
    03/09/95 dea; TB 17928; F13 did not work from jump box.
    07/22/98 cm;  TB# 23289 GO from right side of jumpbox causing error
    07/22/98 cm;  TB# 20219 Cancel after lookup runs wrong function
*******************************************************************************/

/*e This is the standard jump box behavior for Trend 6.0 and onwards. It should
    be included only in jump.p and menu*.p programs. There is no longer any
    need to have the jump box resident in each function. This saves considerable
    space on the .r file (approx. 5K per).
    This code should NOT be modified without senior level Trend design
    approval.
******************************************************************************/

/*d Jump box frame layout */
form
    g-modulenm at 2 auto-return
        help "Module Name                                                 [L]"
    space (2)
    g-menusel
        help "Menu Selection                                              [L]"
with frame f-jump row 1 column 16 title "Selection" no-labels overlay.


/*d Reset Mode for Setups and Inquiries */
assign g-add        = no
       g-secondsel  = "".

/*d Make sure no pause messages are displayed before frames are hidden */
pause 0 before-hide.

/*d Clear any status messages before displaying the jump box */
status default.

/*o*********************** Jump Processing ***********************************/
j-proc:
do:
    /*d Last Posting Key */
    if {k-func13.i} then leave j-proc.

    /*d If cancel from function, return to parent */
    /*tb 20219 07/22/98 cm; Cancel after lookup runs wrong function.
        If returning to pv leave as is to avoid su looping. */
    if ({k-cancel.i} or g-callproc = "sapb") then do:
        assign
            g-modulenm = if g-modulenm = "pv" and g-menusel  = ""
                         then "pv"
                         else "pa"
            g-menusel  = "".
        input clear.
        readkey pause 0.
        leave j-proc.
    end. /* k-cancel.i */

    /*d Clear jump variables */
    /*e Only clear vars if real jump, leave as is if they were reset in the
        last function. The assumption is that if the jump variables were set
        in the previous function, then there was specific intent to jump to
        the set values at the end of the function */

    if g-modulenm = o-modulenm and g-menusel = o-menusel then do:
        assign
            g-modulenm = ""
            g-menusel  = "".
    end. /* clear variables */
    else do:
        leave j-proc.
    end.

    /*d Otherwise, show the jump box to the user */
    clear frame f-jump.
    view frame f-jump.

    j-updt:
    do while true on endkey undo, leave:

        /*d Prompt on right half of box if not on main menu */
 /*       if g-callproc = "menu" and g-ourproc ne "tr"
            then
 */            
         next-prompt g-menusel with frame f-jump.

        /*d Check for superuser */
        if g-operinit ne g-operinits then do:
            view frame f-jump.
            put screen row 3 col 18 color message " SUPER ".
        end. /* superuser */

        /*d Accept the input from the user */
        update g-modulenm auto-return g-menusel go-on(f12)
        with frame f-jump editing:
            readkey.
            /*d Edting allows user to 'type off the end' of jump box */
            /*tb 23289 07/22/98 cm; GO from right side of jumpbox causing error.
                Add check for 'GO' key (k-accept.i) to avoid appending
                chr(lastkey). In certain levels of Progress this was causing an
                invalid menu selection. */
            if frame-field = "g-menusel" then
                g-secondsel = if {k-backsp.i} then
                                  substring(g-secondsel,1,
                                            length(g-secondsel) - 1)
                              else if not ({k-return.i} or {k-accept.i}) then
                                  g-secondsel + chr(lastkey)
                              else g-secondsel.
            /*tb 17928 03/09/95 dea; Use k-func11.i instead of k-jump.i
                                     for f13 from jump box */
            if {k-func11.i} then do:
                bell.
                next-prompt g-modulenm with frame f-jump.
                next.
            end. /* k-func11.i */
            apply lastkey.
        end. /* editing */

        /*d If the user has typed off the end, and no characters were
            entered in the module, then shift to the left */
        if length(g-secondsel) > 3 and length(g-modulenm) = 0 then
            assign
                g-modulenm = substring(trim(g-secondsel),1,2)
                g-menusel  = substring(trim(g-secondsel),3,3).

        /*d Lookup from the jump box */
        /*e dea, 10/12/94, This really shouldn't need to be here,
            check applhelp.p */
        else if {k-func12.i} then do:
            run sassmlu.p(g-modulenm + g-menusel,true).
            next j-updt.
        end. /* F12 */

        leave j-updt.

    end. /* do while true */

    /*tb 20219 07/22/98 cm; Cancel after lookup runs wrong function.
        If cancel hit, clear the jumpbox variables that were set by
        the lookup function. */
    if {k-cancel.i} then
        assign
            g-modulenm = ""
            g-menusel  = "".

    hide all no-pause.

end. /* j-proc */

hide frame f-jump.



