/* kpes_timer.p - 02/16/2005 */
/*****************************************************************************
  PROCEDURE      : kpes_timer.p
  DESCRIPTION    : Kit Production - Work Order Activity Maintenance Time      
                   Updates - run from hhks.p and kpes.p. 
  AUTHOR         : Sunsource
  DATE WRITTEN   : 02/16/05
  CHANGES MADE   :
*******************************************************************************/

{g-all.i} 

def shared var t-time as decimal                   no-undo.
def shared var t-days as decimal                   no-undo.  

define shared temp-table ltimes no-undo 
    field techid    like swsst.technician
    field orderno   like kpet.wono 
    field ordersuf  like kpet.wosuf
    field whse      like swsst.whse
    field ltm       as dec
    index k-timesidx
          orderno  
          ordersuf.

main:
do transaction:
for each ltimes 
  no-lock:
  find first  kpet where 
              kpet.cono  = g-cono         and 
              kpet.wono  = ltimes.orderno and 
              kpet.wosuf = ltimes.ordersuf no-lock no-error.
      if avail kpet then 
      do:
        /******* Component Labor Line lookup  *******/
        find first oeelk use-index k-oeelk where
                   oeelk.cono      = g-cono     and
                   oeelk.ordertype = "w"        and
                   oeelk.orderno   = kpet.wono  and
                   oeelk.ordersuf  = kpet.wosuf and
                   oeelk.lineno    >= 0         and
                   oeelk.shipprod  = "mecoh"    and
                   oeelk.comptype  <> "r"
                   exclusive-lock no-error.
          if avail oeelk then 
          do: 
/*          
           message "oeelkuser7 updt"
                    ltimes.orderno ltimes.ordersuf 
                    "now" oeelk.user7 "+time" t-time. pause.  
*/          
              assign oeelk.user7 = if t-time > 0 then 
                                      oeelk.user7 + t-time
                                   else 
                                      oeelk.user7.
              release oeelk.
          end.  
      end.
end. /* for each ltimes */
release oeelk.
end. /* do transaction */
