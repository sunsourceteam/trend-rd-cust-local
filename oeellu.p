/* oeellu.p 1.1 01/02/98 */
/* oeellu.p 1.4 10/21/95 */
/* ****************************************************************************
  PROCEDURE      : oeellu
  DESCRIPTION    : Lookup OE Line
  AUTHOR         : enp
  DATE WRITTEN   : 09/18/89
  CHANGES MADE   :
    03/18/93 rhl; TB# 9793 Lookups in oe line#-negatives don't show minus sign
    10/21/95 dww; TB# 16065 Add First Description to OE Line lookup
    03/09/99 abe; TB# 25961 Add new user hook {&user_after_frame}
    10/19/01 mt;  TB# e10078 OE Usability project phase 1.
    02/11/03 mt;  TB# e16226 Issues with line # lookup.
**************************************************************************** */

{g-lu.i}

def            var k-exchange       as   dec                            no-undo.
def buffer zxt3-sasos for sasos.

def var v-nxtviewln as i                  no-undo.


find first zxt3-sasos where
           zxt3-sasos.cono  = g-cono and
           zxt3-sasos.oper2 = g-operinits and
           zxt3-sasos.menuproc = "zxt3"  no-lock no-error.
 if avail zxt3-sasos and zxt3-sasos.securcd[3] > 1 then do:
   if g-currproc = "oeexq" then do:
     find   oeehb  where
            oeehb.cono     = g-cono and
            oeehb.batchnm  = string(g-orderno,">>>>>>>9")  and
           (oeehb.countrycd = "CN" or
            oeehb.countrycd = "LB") and
            oeehb.seqno    = 1 no-lock no-error.
     if avail oeehb then do:
       run oeelluX.p.
       return.
     end.
   end.   
   else do:
     find   oeeh  where
            oeeh.cono     = g-cono     and
            oeeh.orderno  = g-orderno  and
            oeeh.ordersuf = g-ordersuf and
           (oeeh.currencyty = "CN" or
            oeeh.currencyty = "LB") no-lock no-error.
     if avail oeeh then do:
       run oeelluX.p.
       return.
     end.
   end.   
 end.
v-length = 10.

{oeellu.lva}
{oeellu.ltt}
{oeellu.lfo}

/*u User Hook*/
{oeellu.z99 &user_after_frame = "*"}

pause 0 before-hide.
on f11 bell.

assign
    s-orderno   = g-orderno
    s-ordersuf  = g-ordersuf
    confirm     = false.

{oeellu.lpr} /*tb 16226 02/10/03 mt; move oeellu.lpr outside of main*/

main:
repeat for oeel with frame f-lookup on endkey undo main, leave main
                                    on error  undo main, leave main:

    if confirm = no and g-ourproc ne "oeetl" and  
       g-currproc ne "oeexq" then do:
        update
            s-orderno
            s-ordersuf
        with frame f-x.
        hide frame f-x no-pause.
        confirm = true.
    end.

/*  {oeellu.lpr} tb 16226 02/10/03 mt; move oeellu.lpr outside of main*/

    {xxxxlu.i &find    = "oeellu.lfi"
              &file    = "t-lu"
              &alpha   = "no"
              &chfield = "t-lu.key"
              &display = "t-lu.rushfl
                          t-lu.key
                          t-lu.shipprod
                          t-lu.proddesc
                          t-lu.qtyord when
                                      not t-lu.returnfl and
                                      not can-do('e,i',substring(t-lu.key,5,1))
                          (t-lu.qtyord * -1) when
                                      t-lu.returnfl and
                                      not can-do('e,i',substring(t-lu.key,5,1))
                                      @ t-lu.qtyord
                          t-lu.unit when
                                      not can-do('e,i',substring(t-lu.key,5,1))
                          t-lu.netamt when
                                      not t-lu.returnfl and
                                      not can-do('e,i',substring(t-lu.key,5,1))
                          (t-lu.netamt * -1) when
                                      t-lu.returnfl and
                                      not can-do('e,i',substring(t-lu.key,5,1))
                                      @ t-lu.netamt"
              
              &global  = "g-oelineno   = integer(substring(frame-value,1,3)).
                          g-oeelclty   = substring(frame-value,5,1).
                          g-oeelcseqno = integer(substring(frame-value,7,2)).
                          frame-value  = string(integer
                                            (substring(frame-value,1,3)),'zz9')"
              &dsfield = "t-lu.shipprod"}.
end.

on f11 endkey.
pause 0 before-hide.

hide frame f-lookup no-pause.
hide frame f-x no-pause.

status default.

