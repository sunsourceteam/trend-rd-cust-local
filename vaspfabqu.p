
/* vaspfabqu.p */
/*****************************************************************************
PROCEDURE      : vaspfabqu.p
DESCRIPTION    : Fabrication - power unit Production - when a power unit
quote is desired it will be built into a quote template.
The vasp will store a temporary template of needed parts
until the customer decides to pursue the proposed quoted
temporary order. Information in the vasp will be kept in
company 501.
AUTHOR         : Sunsource
DATE WRITTEN   : 7/21/08
CHANGES MADE   :
*******************************************************************************/

DEF INPUT param v-whseid     LIKE icsw.whse              NO-UNDO.
DEF INPUT param v-custid     LIKE arsc.custno            NO-UNDO.
DEF INPUT param v-proctyp    AS CHAR FORMAT "x(03)"      NO-UNDO.
DEF INPUT param o-model      AS CHAR FORMAT "x(24)"      NO-UNDO.
DEF INPUT-OUTPUT param v-tag AS CHAR FORMAT "x(12)"      NO-UNDO.
DEF OUTPUT param v-tag2      AS CHAR FORMAT "x(12)"      NO-UNDO.

{g-all.i}
{g-ic.i}
{va.gva}
{g-fabstg.i}

DEF VAR v-idvaeh          AS RECID                      NO-UNDO.
DEF VAR v-idvaesl         AS RECID                      NO-UNDO.
DEF VAR v-yes             AS LOGICAL                    NO-UNDO.
DEF VAR copy-okfl         AS LOGICAL                    NO-UNDO.
DEF SHARED VAR v-stage    AS c FORMAT "x(8)"  DCOLOR 2  NO-UNDO.
DEF SHARED VAR v-revno    AS c FORMAT "x(3)"            NO-UNDO.
DEF SHARED VAR s-prod     AS CHARACTER FORMAT "x(24)"   NO-UNDO.
DEF SHARED VAR v-custpo   LIKE oeeh.custpo              NO-UNDO.
DEF SHARED VAR v-model    AS CHARACTER FORMAT "x(39)"   NO-UNDO.
DEF SHARED VAR v-takenby  LIKE oeeh.takenby             NO-UNDO.
DEF SHARED VAR v-shipto   LIKE oeeh.shipto              NO-UNDO.
DEF SHARED VAR v-binloc   AS CHAR    FORMAT "x(6)"      NO-UNDO.
DEF VAR r-prod            AS CHARACTER FORMAT "x(24)"   NO-UNDO.
DEF VAR x-tagno           AS CHAR FORMAT "x(12)"        NO-UNDO.
DEF VAR v-emailmsg        AS CHAR FORMAT "x(30)"        NO-UNDO.
DEF VAR v-whse            LIKE vaeh.whse                NO-UNDO.
DEF VAR v-stgint          AS INT                        NO-UNDO.
DEF VAR g-technician      LIKE swsst.technician         NO-UNDO.
DEF VAR v-costgain        LIKE oeel.prodcost            NO-UNDO.
DEF VAR v-fabcost         LIKE oeel.prodcost            NO-UNDO.
DEF VAR v-labormins       AS INT                        NO-UNDO.
DEF VAR v-jobseqno        AS INT                        NO-UNDO.
DEF VAR l-prod1a          AS CHARACTER FORMAT "x(24)"   NO-UNDO.
DEF VAR l-prod1b          AS CHARACTER FORMAT "x(24)"   NO-UNDO.
DEF VAR l-prod1c          AS CHARACTER FORMAT "x(24)"   NO-UNDO.
DEF VAR l-prod2           AS CHARACTER FORMAT "x(24)"   NO-UNDO.
DEF VAR l-prod3           AS CHARACTER FORMAT "x(24)"   NO-UNDO.
DEF VAR l-prod4           AS CHARACTER FORMAT "x(24)"   NO-UNDO.
DEF VAR l-prod5           AS CHARACTER FORMAT "x(24)"   NO-UNDO.
DEF VAR l-prod6           AS CHARACTER FORMAT "x(24)"   NO-UNDO.
DEF VAR l-prod7           AS CHARACTER FORMAT "x(24)"   NO-UNDO.
DEF VAR test-prod         AS CHARACTER FORMAT "x(24)"   NO-UNDO.
DEF VAR fillchar          AS CHAR INIT " "              NO-UNDO.
DEF VAR s-listprc         AS DEC                        NO-UNDO.
DEF VAR s-hours           AS i FORMAT "99"              NO-UNDO.
DEF VAR s-colon           AS c FORMAT "x" INIT ":"      NO-UNDO.
DEF VAR s-minutes         AS i FORMAT "99"              NO-UNDO.

DEF BUFFER pn-ztmk FOR ztmk.
DEF BUFFER b-vasp FOR vasp.
DEF BUFFER b-zsdivasp FOR zsdivasp.
DEF BUFFER bo-zsdivasp for zsdivasp.
DEF BUFFER bo-vasp     for vasp.

DEFINE NEW SHARED TEMP-TABLE  t-vaspmap no-undo like zsdivaspmap 
  Field qtyord      like vaspsl.qtyneeded

index k-add
      cono
      rectype
      seqno
      lineno
      prod.



DEFINE SHARED TEMP-TABLE t-lines NO-UNDO
    FIELD lineno      LIKE oeel.lineno
    FIELD seqno       LIKE oeelk.seqno
    FIELD specnstype  LIKE oeel.specnstype
    FIELD prod        LIKE oeel.shipprod
    FIELD astr1       AS CHAR FORMAT "x"
    FIELD prodline    LIKE icsl.prodline
    FIELD descrip     AS CHARACTER FORMAT "x(24)"
    FIELD icspdesc    AS CHARACTER FORMAT "x(24)"
    FIELD gdesc       AS CHARACTER FORMAT "x(50)"
    FIELD keyno       AS INT
    FIELD prctype     LIKE oeel.pricetype
    FIELD prodcat     LIKE oeel.prodcat
    FIELD vendno      LIKE apsv.vendno
    FIELD vendnm      LIKE apsv.NAME
    FIELD whse        LIKE oeel.whse
    FIELD altwhse     LIKE oeel.whse
    FIELD prodcost    LIKE oeel.prodcost
    FIELD extcost     LIKE oeel.prodcost
    FIELD gain        LIKE oeel.prodcost
    FIELD listprc     LIKE oeel.price
    FIELD discpct     AS DEC FORMAT ">>9.99"
    FIELD sellprc     LIKE oeel.price
    FIELD gp          AS DEC FORMAT ">>>9.99-"
    FIELD qty         LIKE oeel.qtyord
    FIELD shipdt      AS DATE FORMAT "99/99/99"
    FIELD pdscrecno   LIKE oeel.pdrecno
    FIELD qtybrkty    AS CHAR FORMAT "x(1)"
    FIELD kitfl       AS LOGICAL FORMAT "yes/no"
    FIELD unit        LIKE oeel.unit
    FIELD icspstat    LIKE icsp.statustype
    FIELD vaspslid    AS RECID
    FIELD leadtm      AS CHAR FORMAT "xxxx"
    FIELD faxfl       AS CHAR FORMAT "x"
    FIELD attrfl      as logical
    FIELD ZsdiXrefType as char format "xx"

INDEX ix1
      lineno
      seqno
INDEX ix2-lead
      leadtm descending
      specnstype.


{p-fabstg.i}
main:
DO WHILE TRUE:
  ASSIGN /* next 3 in-items(seq3) ------ last 3 it-items(seq2) */
    l-prod1a = "fabrication fittings"
    l-prod1b = "fab frt"
    l-prod1c = "fab shop"
    l-prod2  = "fab"
    l-prod3  = "documentation charge"
    l-prod4  = "fab cad".
  
  {w-icsw.i l-prod2 v-whseid NO-LOCK}.
  IF avail icsw THEN
    ASSIGN v-fabcost = icsw.avgcost.
  ELSE
    ASSIGN v-fabcost = 1.
  
  
  IF v-proctyp = "cpy" THEN DO:
  
  
    FIND FIRST bo-zsdivasp USE-INDEX  k-repairno  WHERE
               bo-zsdivasp.cono     = g-cono  AND
               bo-zsdivasp.rectype  = "fb"    AND
               bo-zsdivasp.repairno = v-tag
    EXCLUSIVE-LOCK NO-ERROR.
    FIND FIRST bo-vasp USE-INDEX k-vasp WHERE
               bo-vasp.cono     = g-cono  AND
               bo-vasp.shipprod = v-tag   
    EXCLUSIVE-LOCK NO-ERROR.

    if not avail bo-zsdivasp or not avail bo-vasp then do:
      message "Internal error during Copy " + v-tag.
      pause 4.
      return.
    end.
  
    FIND LAST b-zsdivasp USE-INDEX  k-rectype  WHERE
              b-zsdivasp.cono = g-cono and
              b-zsdivasp.rectype = "fb" 
    NO-LOCK NO-ERROR.
    find first t-stages no-lock no-error.
    
    CREATE vasp.
    buffer-copy bo-vasp 
      except bo-vasp.shipprod
    to vasp  
      assign 
        vasp.shipprod  = substring(STRING(YEAR(TODAY),"9999"),3,2) +
                         STRING(MONTH(TODAY),"99") + "-" +
                         STRING(IF avail b-zsdivasp THEN
                                  b-zsdivasp.seqno + 1
                                ELSE
                                  1,"9999999")
        vasp.refer     =    ENTRY(1,vasp.shipprod,"-") + "-" +
                            STRING(INT(ENTRY(2,vasp.shipprod,"-")))
        vasp.user4     = FILL(fillchar,180)
        vasp.user2     = ""
        vasp.user6     = if avail b-zsdivasp then
                           b-zsdivasp.seqno + 1
                         else
                           1
        vasp.user7     = 10
        vasp.user9     = ?
        vasp.transdt   = TODAY
        vasp.operinit  = g-operinits
        vasp.transtm   = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                         SUBSTRING(STRING(TIME,"hh:mm"),4,2)
        vasp.transproc = g-currproc.
  
    
    
    CREATE zsdivasp.
    
    buffer-copy bo-zsdivasp 
    except
        bo-zsdivasp.repairno
    to zsdivasp
      ASSIGN 
        zsdivasp.cono       = vasp.cono
        zsdivasp.repairno   = vasp.shipprod
        zsdivasp.rectype    = "fb"
        zsdivasp.partno     = vasp.refer
        zsdivasp.outincd    = " "  /* substring(vasp.user4,90,2)  */
        zsdivasp.hotcd      = " "  /* substring(vasp.user4,105,3) */
        zsdivasp.binloc     = " "
        zsdivasp.inuseby    = " "
        zsdivasp.seqno      = IF avail b-zsdivasp THEN
                                b-zsdivasp.seqno + 1
                              ELSE
                                1
        zsdivasp.stagecd    = vasp.user7
        zsdivasp.enterdt    = TODAY
        zsdivasp.entertm    = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                              SUBSTRING(STRING(TIME,"hh:mm"),4,2)
        zsdivasp.transdt    = vasp.transdt
        zsdivasp.operinit   = vasp.operinit
        zsdivasp.transtm    = vasp.transtm
        zsdivasp.xvano      = 0
        zsdivasp.xuser4     = ?
        zsdivasp.xuser3     = ""
        zsdivasp.user4      = today
        zsdivasp.user5      = today.
    
    {w-icsw.i s-prod vasp.whse NO-LOCK}.
    {w-icsp.i s-prod NO-LOCK}.
    IF avail icsp AND NOT avail icsw THEN DO:
      /** copy from main whse - if not at desired **/
      RUN zsdimaincpy.p(INPUT g-cono,
                        INPUT ""    ,
                        INPUT v-whseid,
                        INPUT s-prod,
                        INPUT 1,
                        INPUT-OUTPUT copy-okfl).
        if not copy-okfl then
          run Create_ICSW (input v-whseid,
                           INPUT s-prod,
                           INPUT-OUTPUT copy-okfl).
        {w-icsw.i s-prod v-whseid NO-LOCK}.
    
    END.
    
    
    RUN  CopyVASPStructure (INPUT RECID(vasp)).
    
    FIND FIRST ztmk USE-INDEX k-ztmk-sc2 WHERE
               ztmk.cono      = g-cono       AND
               ztmk.ordertype = "fb"          AND
               ztmk.user1     = vasp.shipprod AND
               ztmk.statuscd  = 0             AND
               ztmk.stagecd   = vasp.user7
    NO-LOCK NO-ERROR.
    IF NOT avail ztmk THEN DO:
      CREATE ztmk.
      ASSIGN 
        ztmk.jobseqno  = 0
        ztmk.cono      = g-cono
        ztmk.ordertype = "fb"
        ztmk.user1     = vasp.shipprod
        ztmk.user2     = string(zsdivasp.custno,"99999999999")
        ztmk.stagecd   = vasp.user7
        ztmk.statuscd  = 0
        ztmk.techid    = "admn"
        ztmk.punchindt = TODAY
        ztmk.punchintm = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                         SUBSTRING(STRING(TIME,"hh:mm"),4,2) +
                         SUBSTRING(STRING(TIME,"hh:mm:ss"),7,2)
        ztmk.operinit  = g-operinits
        ztmk.transtm   = IF ztmk.transtm = "" THEN
                           SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                           SUBSTRING(STRING(TIME,"hh:mm"),4,2)
                         ELSE
                           ztmk.transtm
        ztmk.transdt   = IF ztmk.transdt = ? THEN
                           TODAY
                         ELSE
                           ztmk.transdt.
    END.
  ASSIGN v-tag2 = vasp.shipprod.
  END. /* if cpy */
  ELSE
  IF v-proctyp = "add" THEN DO:
    FIND LAST b-zsdivasp USE-INDEX  k-rectype  WHERE
              b-zsdivasp.cono = g-cono and
              b-zsdivasp.rectype = "fb" 
    NO-LOCK NO-ERROR.
    find first t-stages no-lock no-error.
    
    CREATE vasp.
    ASSIGN 
      vasp.cono      = g-cono
      vasp.whse      = v-whseid
      vasp.shipprod  = substring(STRING(YEAR(TODAY),"9999"),3,2) +
                       STRING(MONTH(TODAY),"99") + "-" +
                       STRING(IF avail b-zsdivasp THEN
                                b-zsdivasp.seqno + 1
                              ELSE
                                1,"9999999")
      vasp.refer     = if s-prod = "" then
                          ENTRY(1,vasp.shipprod,"-") + "-" +
                          STRING(INT(ENTRY(2,vasp.shipprod,"-")))
                       else
                         s-prod
      vasp.user4     = FILL(fillchar,180)
      vasp.user6     = if avail b-zsdivasp then
                         b-zsdivasp.seqno + 1
                       else
                         1
      vasp.user7     = t-stages.stage
      vasp.transdt   = TODAY
      vasp.operinit  = g-operinits
      vasp.transtm   = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                       SUBSTRING(STRING(TIME,"hh:mm"),4,2)
      vasp.transproc = g-currproc.
    CREATE zsdivasp.
    ASSIGN 
      zsdivasp.cono       = vasp.cono
      zsdivasp.xuser1     = v-revno
      zsdivasp.whse       = vasp.whse
      zsdivasp.repairno   = vasp.shipprod
      zsdivasp.rectype    = "fb"
      zsdivasp.partno     = vasp.refer
      zsdivasp.notesfl    = vasp.notesfl
      zsdivasp.mfgname    = STRING(v-model,"x(39)")
      zsdivasp.serial     = " "
      zsdivasp.custpo     = STRING(v-custpo,"x(22)")
      zsdivasp.shipto     = STRING(v-shipto,"x(8)")
      /** lstusr defaults to takenby here **/
      zsdivasp.lstusr     = STRING(v-takenby,"x(4)")
      zsdivasp.takenby    = STRING(v-takenby,"x(4)")
      zsdivasp.outincd    = " "  /* substring(vasp.user4,90,2)  */
      zsdivasp.hotcd      = " "  /* substring(vasp.user4,105,3) */
      zsdivasp.binloc     = " "
      zsdivasp.inuseby    = vasp.xxc4
      zsdivasp.custno     = v-custid
      zsdivasp.seqno      = IF avail b-zsdivasp THEN
                              b-zsdivasp.seqno + 1
                            ELSE
                              1
      zsdivasp.stagecd    = vasp.user7
      zsdivasp.enterdt    = TODAY
      zsdivasp.entertm    = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                            SUBSTRING(STRING(TIME,"hh:mm"),4,2)
      zsdivasp.transdt    = vasp.transdt
      zsdivasp.operinit   = vasp.operinit
      zsdivasp.transtm    = vasp.transtm
      zsdivasp.leadtm1    = " "
      zsdivasp.margpct    = 35.00
      zsdivasp.leadtm2    = " "
      zsdivasp.OVERRIDE   = " "
      zsdivasp.listprc    = 0
      zsdivasp.sellprc    = 0
      zsdivasp.invcost    = 0
      zsdivasp.vendno     = 0
      zsdivasp.cost       = 0
      zsdivasp.Labortotl    = 0
      zsdivasp.repairtotl = 0
      zsdivasp.xvano      = INT(SUBSTRING(vasp.user2,1,7))
      zsdivasp.costingdt  = TODAY
      zsdivasp.xuser4     = ?
      zsdivasp.user4      = today
      zsdivasp.user5      = today.
    
    {w-icsw.i s-prod vasp.whse NO-LOCK}.
    {w-icsp.i s-prod NO-LOCK}.
    IF avail icsp AND NOT avail icsw THEN DO:
      /** copy from main whse - if not at desired **/
      RUN zsdimaincpy.p(INPUT g-cono,
                        INPUT ""    ,
                        INPUT v-whseid,
                        INPUT s-prod,
                        INPUT 1,
                        INPUT-OUTPUT copy-okfl).
    END.
    
    
    RUN  BuildVASPStructure (INPUT RECID(vasp)).
    
    FIND FIRST ztmk USE-INDEX k-ztmk-sc2 WHERE
               ztmk.cono      = g-cono       AND
               ztmk.ordertype = "fb"          AND
               ztmk.user1     = vasp.shipprod AND
               ztmk.statuscd  = 0             AND
               ztmk.stagecd   = vasp.user7
    NO-LOCK NO-ERROR.
    IF NOT avail ztmk THEN DO:
      CREATE ztmk.
      ASSIGN 
        ztmk.jobseqno  = 0
        ztmk.cono      = g-cono
        ztmk.ordertype = "fb"
        ztmk.user1     = vasp.shipprod
        ztmk.user2     = string(zsdivasp.custno,"99999999999")
        ztmk.stagecd   = vasp.user7
        ztmk.statuscd  = 0
        ztmk.techid    = "admn"
        ztmk.punchindt = TODAY
        ztmk.punchintm = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                         SUBSTRING(STRING(TIME,"hh:mm"),4,2) +
                         SUBSTRING(STRING(TIME,"hh:mm:ss"),7,2)
        ztmk.operinit  = g-operinits
        ztmk.transtm   = IF ztmk.transtm = "" THEN
                           SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                           SUBSTRING(STRING(TIME,"hh:mm"),4,2)
                         ELSE
                           ztmk.transtm
        ztmk.transdt   = IF ztmk.transdt = ? THEN
                           TODAY
                         ELSE
                           ztmk.transdt.
    END.
  END. /* if add */
  ELSE
  IF v-proctyp = "chg" THEN DO:
    run vaexfa.p (input g-cono,
                  input v-tag,
                  input v-whseid).
 

    FIND FIRST zsdivasp USE-INDEX  k-repairno  WHERE
               zsdivasp.cono     = g-cono  AND
               zsdivasp.rectype  = "fb"    AND
               zsdivasp.repairno = v-tag
    EXCLUSIVE-LOCK NO-ERROR.
    FIND FIRST vasp USE-INDEX k-vasp WHERE
               vasp.cono     = g-cono  AND
               vasp.shipprod = v-tag   
    EXCLUSIVE-LOCK NO-ERROR.
    IF avail vasp THEN DO:
      ASSIGN
        zsdivasp.xuser1     = v-revno
        zsdivasp.whse       = v-whseid
        zsdivasp.repairno   = vasp.shipprod
        zsdivasp.partno     = vasp.refer
        zsdivasp.mfgname    = STRING(v-model,"x(39)")
        zsdivasp.serial     = " "
        zsdivasp.custpo     = STRING(v-custpo,"x(22)")
        zsdivasp.shipto     = STRING(v-shipto,"x(8)")
        /** lstusr defaults to takenby here **/
        zsdivasp.lstusr     = STRING(v-takenby,"x(4)")
        zsdivasp.takenby    = STRING(v-takenby,"x(4)")
        zsdivasp.inuseby    = vasp.xxc4
        zsdivasp.custno     = v-custid
        zsdivasp.transdt    = vasp.transdt
        zsdivasp.operinit   = vasp.operinit
        zsdivasp.transtm    = vasp.transtm
        zsdivasp.xvano      = INT(SUBSTRING(vasp.user2,1,7)).
        
        
      ASSIGN 
        vasp.whse     = v-whseid
        vasp.refer    = s-prod
        vasp.transdt  = TODAY
        vasp.operinit = g-operinit
        vasp.transtm  = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                        SUBSTRING(STRING(TIME,"hh:mm"),4,2).
    END.
  END.
  
  IF avail vasp THEN do:
    find t-stages where t-stages.stage = int(vasp.user7) no-lock no-error.
    if not avail t-stages then
      find first t-stages no-lock no-error.
    
    ASSIGN
      v-tag   = vasp.shipprod
      v-tag2  = ENTRY(1,vasp.shipprod,"-") + "-" +
                LEFT-TRIM(ENTRY(2,vasp.shipprod,"-"),"0")
      v-stage = t-stages.shortname.
   end.
  LEAVE.
  
END.  /* do while true */

PROCEDURE product_fill:
  DEFINE INPUT PARAMETER ic-desc AS CHAR FORMAT "x(30)".
  DEFINE INPUT-OUTPUT PARAMETER ic-prod LIKE s-prod.
  
  DEFINE BUFFER l-icsec FOR icsec.
  DEFINE VAR v-prodcat LIKE icsp.prodcat NO-UNDO.

END. /* procedure */


PROCEDURE BuildVASPStructure:
  DEFINE INPUT PARAMETER ip-vasprecid AS RECID NO-UNDO.
  
  DEFINE BUFFER xp-vasp FOR vasp.
  DEFINE BUFFER xp-vasps FOR vasps.
  DEFINE BUFFER xp-vaspsl FOR vaspsl.
  DEFINE BUFFER xp-zsdivaspmap FOR zsdivaspmap.

  
  FIND xp-vasp WHERE RECID(xp-vasp) = ip-vasprecid NO-LOCK NO-ERROR.
  if not avail xp-vasp then do:
    message "no record". pause.
  end.  

/*      
        
    find zsdivaspmap where
         zsdivaspmap.cono = g-cono and
         zsdivaspmap.rectype = "H" and        /* VAES */
         zsdivaspmap.prod    = "Fabrication Template" and
         zsdivaspmap.seqno = 0 and
         zsdivaspmap.lineno = 0            no-lock no-error.
 
 
    CREATE xp-zsdivaspmap.
    BUFFER-COPY zsdivaspmap
    EXCEPT
      zsdivaspmap.prod
      zsdivaspmap.transtm
      zsdivaspmap.transdt
      zsdivaspmap.operinit
    TO xp-zsdivaspmap
      ASSIGN
      xp-zsdivaspmap.prod         = xp-vasp.shipprod
      xp-zsdivaspmap.transdt      = TODAY
      xp-zsdivaspmap.operinit     = g-operinit
      xp-zsdivaspmap.transtm      = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                                    SUBSTRING(STRING(TIME,"hh:mm"),4,2)
      xp-zsdivaspmap.transproc    = g-currproc.
*/      
      
        
    FOR EACH zsdivaspmap where
             zsdivaspmap.cono    = g-cono and
             zsdivaspmap.rectype = "GO" and        /* GLobal Options */
             zsdivaspmap.prod    = "Fabrication Template" and
             zsdivaspmap.seqno   = 0 and
             zsdivaspmap.lineno  = 0  no-lock:
 
 
      CREATE xp-zsdivaspmap.
      BUFFER-COPY zsdivaspmap
      EXCEPT
        zsdivaspmap.prod
        zsdivaspmap.transtm
        zsdivaspmap.transdt
            zsdivaspmap.operinit
      TO xp-zsdivaspmap
        ASSIGN
        xp-zsdivaspmap.prod         = xp-vasp.shipprod
        xp-zsdivaspmap.transdt      = TODAY
        xp-zsdivaspmap.operinit     = g-operinit
        xp-zsdivaspmap.transtm      = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                                      SUBSTRING(STRING(TIME,"hh:mm"),4,2)
        xp-zsdivaspmap.transproc    = g-currproc.
      
    END.
  
  
  
  FOR EACH vasps WHERE
           vasps.cono    = g-cono AND
           vasps.shipprod = "Fabrication Template" AND
           vasps.whse    = "DSTH"  NO-LOCK:

      if not avail xp-vasp then do:
        message "Template Section". pause.
      end.  
 
    CREATE xp-vasps.
    BUFFER-COPY vasps
    EXCEPT
      vasps.shipprod
      vasps.whse
      vasps.intrwhse
/*       vasps.desttype */
      vasps.sctncode
      vasps.transdt
      vasps.operinit
      vasps.transtm
    TO xp-vasps
    ASSIGN
      xp-vasps.shipprod     = xp-vasp.shipprod
      xp-vasps.whse         = xp-vasp.whse
      xp-vasps.intrwhse     = xp-vasp.whse
      xp-vasps.destwhse     = xp-vasp.whse
      xp-vasps.sctncode     = if xp-vasps.sctntype = "sp" then
                                "spec"
                              else
                                xp-vasp.whse
      xp-vasps.transdt      = TODAY
      xp-vasps.operinit     = g-operinit
      xp-vasps.transtm      = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                              SUBSTRING(STRING(TIME,"hh:mm"),4,2)
      xp-vasps.transproc    = g-currproc.
    

    
        
    find zsdivaspmap where
         zsdivaspmap.cono = g-cono and
         zsdivaspmap.rectype = "S" and        /* VAES */
         zsdivaspmap.prod    = "Fabrication Template" and
         zsdivaspmap.seqno = vasps.seqno and
         zsdivaspmap.lineno = 0            no-lock no-error.
 
 
    CREATE xp-zsdivaspmap.
    BUFFER-COPY zsdivaspmap
    EXCEPT
      zsdivaspmap.prod
      zsdivaspmap.transtm
      zsdivaspmap.transdt
      zsdivaspmap.operinit
    TO xp-zsdivaspmap
      ASSIGN
      xp-zsdivaspmap.prod         = xp-vasp.shipprod
      xp-zsdivaspmap.transdt      = TODAY
      xp-zsdivaspmap.operinit     = g-operinit
      xp-zsdivaspmap.transtm      = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                                    SUBSTRING(STRING(TIME,"hh:mm"),4,2)
      xp-zsdivaspmap.transproc    = g-currproc.
      
    
   
    
    FOR EACH vaspsl WHERE
             vaspsl.cono = g-cono AND
             vaspsl.shipprod = "Fabrication Template" AND
             vaspsl.whse    = "DSTH" /* whse for template */ AND
             vaspsl.seqno   = xp-vasps.seqno NO-LOCK:
      
      {w-icsw.i vaspsl.compprod xp-vasp.whse NO-LOCK}.
      {w-icsp.i vaspsl.compprod NO-LOCK}.
      IF NOT avail icsw THEN DO:
        /** copy from main whse - if not at desired **/

        RUN zsdimaincpy.p(INPUT g-cono,
                          INPUT ""    ,
                          INPUT xp-vasps.whse,
                          INPUT vaspsl.compprod,
                          INPUT 1,
                          INPUT-OUTPUT copy-okfl).
        if not copy-okfl then
          run Create_ICSW (input xp-vasps.whse,
                           INPUT vaspsl.compprod,
                           INPUT-OUTPUT copy-okfl).
        
        {w-icsw.i vaspsl.compprod xp-vasp.whse NO-LOCK}.
      END.
      
      CREATE xp-vaspsl.
      BUFFER-COPY vaspsl
      EXCEPT
        vaspsl.shipprod
        vaspsl.whse
        vaspsl.transdt
        vaspsl.operinit
        vaspsl.transtm
      TO xp-vaspsl
      ASSIGN
        xp-vaspsl.shipprod   = xp-vasp.shipprod
        xp-vaspsl.whse       = xp-vasp.whse
        xp-vaspsl.transdt    = TODAY
        xp-vaspsl.operinit   = g-operinit
        xp-vaspsl.prodcost   = if avail icsw then 
                                 icsw.avgcost 
                               else
                                 xp-vaspsl.prodcost                
        xp-vaspsl.transtm    = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                               SUBSTRING(STRING(TIME,"hh:mm"),4,2)
        xp-vaspsl.transproc   = g-currproc.
      if xp-vasps.sctntype = "it" then
        assign xp-vaspsl.timeelapsed = 0.
      else
        assign xp-vaspsl.qtyneeded = 0.

              
             
      find zsdivaspmap where
           zsdivaspmap.cono = g-cono and
           zsdivaspmap.rectype = "l" and        /* VAESL */
           zsdivaspmap.prod    = "Fabrication Template" and
           zsdivaspmap.seqno = vasps.seqno and
           zsdivaspmap.lineno = vaspsl.lineno  no-lock no-error.
 
 
      CREATE xp-zsdivaspmap.
      BUFFER-COPY zsdivaspmap
      EXCEPT
        zsdivaspmap.prod
        zsdivaspmap.transtm
        zsdivaspmap.transdt
        zsdivaspmap.operinit
      TO xp-zsdivaspmap
        ASSIGN
        xp-zsdivaspmap.prod         = xp-vasp.shipprod
        xp-zsdivaspmap.transdt      = TODAY
        xp-zsdivaspmap.operinit     = g-operinit
        xp-zsdivaspmap.transtm      = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                                      SUBSTRING(STRING(TIME,"hh:mm"),4,2)
        xp-zsdivaspmap.transproc    = g-currproc.
      
      
      
    END. /* VASPSL */
  END.   /* VASPS */





END. /* Procedure */



PROCEDURE CopyVASPStructure:
  DEFINE INPUT PARAMETER ip-vasprecid AS RECID NO-UNDO.
  
  DEFINE BUFFER xp-vasp FOR vasp.
  DEFINE BUFFER xcp-vasp FOR vasp.
  DEFINE BUFFER xp-vasps FOR vasps.
  DEFINE BUFFER xp-vaspsl FOR vaspsl.
  DEFINE BUFFER xp-zsdivaspmap FOR zsdivaspmap.
  DEFINE BUFFER xiv-zsdivaspmap FOR zsdivaspmap.

  DEFINE BUFFER rarr-com FOR com.
  DEFINE BUFFER xp-rarr-com for com.
  
  FIND xp-vasp WHERE RECID(xp-vasp) = ip-vasprecid NO-LOCK NO-ERROR.
  if not avail xp-vasp then do:
    message "no record". pause.
  end.  

   find xcp-vasp where 
        xcp-vasp.cono     = g-cono and
        xcp-vasp.shipprod = v-tag no-lock no-error.  

   find xiv-zsdivaspmap where
        xiv-zsdivaspmap.cono = g-cono and
        xiv-zsdivaspmap.rectype  = "S" and        /* VAES */
        xiv-zsdivaspmap.prod     = v-tag  and
        xiv-zsdivaspmap.typename = "INVENTORY" and
        xiv-zsdivaspmap.lineno = 0  no-lock  no-error.
/* REMINDER INDEX NEEDED */         
   if not avail xiv-zsdivaspmap then do:
     message "program error no zsdivaspmap".
     pause 10.
     return.       
   end.
      
        
        
    FOR EACH zsdivaspmap where
             zsdivaspmap.cono    = g-cono and
             zsdivaspmap.rectype = "GO" and        /* GLobal Options */
             zsdivaspmap.prod    = v-tag and
             zsdivaspmap.seqno   = 0 and
             zsdivaspmap.lineno  = 0  no-lock:
 
 
      CREATE xp-zsdivaspmap.
      BUFFER-COPY zsdivaspmap
      EXCEPT
        zsdivaspmap.prod
        zsdivaspmap.transtm
        zsdivaspmap.transdt
            zsdivaspmap.operinit
      TO xp-zsdivaspmap
        ASSIGN
        xp-zsdivaspmap.prod         = xp-vasp.shipprod
        xp-zsdivaspmap.transdt      = TODAY
        xp-zsdivaspmap.operinit     = g-operinit
        xp-zsdivaspmap.transtm      = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                                      SUBSTRING(STRING(TIME,"hh:mm"),4,2)
        xp-zsdivaspmap.transproc    = g-currproc.
      
    END.
  
  
  
  FOR EACH vasps WHERE
           vasps.cono    = g-cono AND
           vasps.shipprod = xcp-vasp.shipprod AND
           vasps.whse    = xcp-vasp.whse NO-LOCK:

      if not avail xp-vasp then do:
        message "Template Section". pause.
      end.  
 
    CREATE xp-vasps.
    BUFFER-COPY vasps
    EXCEPT
      vasps.shipprod
      vasps.whse
      vasps.intrwhse
/*       vasps.desttype */
      vasps.sctncode
      vasps.transdt
      vasps.operinit
      vasps.transtm
    TO xp-vasps
    ASSIGN
      xp-vasps.shipprod     = xp-vasp.shipprod
      xp-vasps.whse         = xp-vasp.whse
      xp-vasps.intrwhse     = xp-vasp.whse
      xp-vasps.destwhse     = xp-vasp.whse
      xp-vasps.sctncode     = if xp-vasps.sctntype = "sp" then
                                "spec"
                              else
                                xp-vasp.whse
      xp-vasps.transdt      = TODAY
      xp-vasps.operinit     = g-operinit
      xp-vasps.transtm      = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                              SUBSTRING(STRING(TIME,"hh:mm"),4,2)
      xp-vasps.transproc    = g-currproc.
    

    
        
    For EACH zsdivaspmap where
         zsdivaspmap.cono = g-cono and
         zsdivaspmap.rectype = "S" and        /* VAES */
         zsdivaspmap.prod    = xcp-vasp.shipprod and
         zsdivaspmap.seqno = vasps.seqno and
         zsdivaspmap.lineno = 0            no-lock:
 
    CREATE xp-zsdivaspmap.
    BUFFER-COPY zsdivaspmap
    EXCEPT
      zsdivaspmap.prod
      zsdivaspmap.transtm
      zsdivaspmap.transdt
      zsdivaspmap.operinit
    TO xp-zsdivaspmap
      ASSIGN
      xp-zsdivaspmap.prod         = xp-vasp.shipprod
      xp-zsdivaspmap.transdt      = TODAY
      xp-zsdivaspmap.operinit     = g-operinit
      xp-zsdivaspmap.transtm      = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                                    SUBSTRING(STRING(TIME,"hh:mm"),4,2)
      xp-zsdivaspmap.transproc    = g-currproc.
    END.
      
    For Each zsdivaspmap where
         zsdivaspmap.cono = g-cono and
         zsdivaspmap.rectype = "SQ" and        /* VAES */
         zsdivaspmap.prod    = xcp-vasp.shipprod and
         zsdivaspmap.seqno = vasps.seqno and
         zsdivaspmap.lineno = 0            no-lock:
 
    if avail zsdivaspmap then do:
      CREATE xp-zsdivaspmap.
      BUFFER-COPY zsdivaspmap
      EXCEPT
        zsdivaspmap.prod
        zsdivaspmap.transtm
        zsdivaspmap.transdt
        zsdivaspmap.operinit
      TO xp-zsdivaspmap
        ASSIGN
        xp-zsdivaspmap.prod         = xp-vasp.shipprod
        xp-zsdivaspmap.transdt      = TODAY
        xp-zsdivaspmap.operinit     = g-operinit
        xp-zsdivaspmap.transtm      = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                                      SUBSTRING(STRING(TIME,"hh:mm"),4,2)
        xp-zsdivaspmap.transproc    = g-currproc.
        
    end. /* If avail zsdivaspmap. */
    END.
    
    FOR EACH vaspsl WHERE
             vaspsl.cono = g-cono AND
             vaspsl.shipprod = xcp-vasp.shipprod AND
             vaspsl.whse    = xcp-vasp.whse /* whse for template */ AND
             vaspsl.seqno   = xp-vasps.seqno NO-LOCK:
      
      {w-icsw.i vaspsl.compprod xp-vasp.whse NO-LOCK}.
      {w-icsp.i vaspsl.compprod NO-LOCK}.
      IF NOT avail icsw THEN DO:
        /** copy from main whse - if not at desired **/
        RUN zsdimaincpy.p(INPUT g-cono,
                          INPUT ""    ,
                          INPUT xp-vasps.whse,
                          INPUT vaspsl.compprod,
                          INPUT 1,
                          INPUT-OUTPUT copy-okfl).
        if not copy-okfl then
          run Create_ICSW (input xp-vasps.whse,
                           INPUT vaspsl.compprod,
                           INPUT-OUTPUT copy-okfl).
 
        
        {w-icsw.i vaspsl.compprod xp-vasp.whse NO-LOCK}.
      END.
      
      CREATE xp-vaspsl.
      BUFFER-COPY vaspsl
      EXCEPT
        vaspsl.shipprod
        vaspsl.whse
        vaspsl.transdt
        vaspsl.operinit
        vaspsl.transtm
      TO xp-vaspsl
      ASSIGN
        xp-vaspsl.shipprod   = xp-vasp.shipprod
        xp-vaspsl.whse       = xp-vasp.whse
        xp-vaspsl.transdt    = TODAY
        xp-vaspsl.operinit   = g-operinit
/*
        xp-vaspsl.prodcost   = if vaspsl.nonstockty = "n" then
                                  vaspsl.prodcost 
                               else
                               if avail icsw then 
                                 icsw.avgcost 
   
                               else
                                  xp-vaspsl.prodcost               
*/
        xp-vaspsl.transtm    = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                               SUBSTRING(STRING(TIME,"hh:mm"),4,2)
        xp-vaspsl.transproc   = g-currproc.
      /*
      if xp-vasps.sctntype = "it" then
        assign xp-vaspsl.timeelapsed = 0.
      else
        assign xp-vaspsl.qtyneeded = 0.
      */
      if xiv-zsdivaspmap.seqno = vaspsl.seqno then do:
        FIND rarr-com USE-INDEX k-com WHERE
             rarr-com.cono       = g-cono      AND
             rarr-com.comtype    = xcp-vasp.shipprod  AND
             rarr-com.orderno    = 0           AND
             rarr-com.ordersuf   = 0           AND
             rarr-com.lineno     = vaspsl.lineno
        NO-LOCK NO-ERROR.
        if avail rarr-com then do:
          create xp-rarr-com.
          buffer-copy rarr-com
          EXCEPT rarr-com.comtype
                 rarr-com.lineno
          to xp-rarr-com
          ASSIGN xp-rarr-com.comtype = xp-vaspsl.shipprod
                 xp-rarr-com.lineno  = xp-vaspsl.lineno.
        end.              
      end.       
      For Each zsdivaspmap where
           zsdivaspmap.cono = g-cono and
           zsdivaspmap.rectype = "l" and        /* VAESL */
           zsdivaspmap.prod    = xcp-vasp.shipprod and
           zsdivaspmap.seqno = vasps.seqno and
           zsdivaspmap.lineno = vaspsl.lineno  no-lock:
      if avail zsdivaspmap then do:
 
        CREATE xp-zsdivaspmap.
        BUFFER-COPY zsdivaspmap
        EXCEPT
          zsdivaspmap.prod
          zsdivaspmap.transtm
          zsdivaspmap.transdt
          zsdivaspmap.operinit
        TO xp-zsdivaspmap
          ASSIGN
          xp-zsdivaspmap.prod         = xp-vasp.shipprod
          xp-zsdivaspmap.transdt      = TODAY
          xp-zsdivaspmap.operinit     = g-operinit
          xp-zsdivaspmap.transtm      = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                                        SUBSTRING(STRING(TIME,"hh:mm"),4,2)
          xp-zsdivaspmap.transproc    = g-currproc.
      end. /* avail zsdivaspmap */ 
      end.       
      For Each zsdivaspmap where
           zsdivaspmap.cono = g-cono and
           zsdivaspmap.rectype = "LQ" and        /* VAESL */
           zsdivaspmap.prod    = xcp-vasp.shipprod and
           zsdivaspmap.seqno = vasps.seqno and
           zsdivaspmap.lineno = vaspsl.lineno  no-lock:
      if avail zsdivaspmap then do:
        CREATE xp-zsdivaspmap.
        BUFFER-COPY zsdivaspmap
        EXCEPT
          zsdivaspmap.prod
          zsdivaspmap.transtm
          zsdivaspmap.transdt
          zsdivaspmap.operinit
        TO xp-zsdivaspmap
          ASSIGN
          xp-zsdivaspmap.prod         = xp-vasp.shipprod
          xp-zsdivaspmap.transdt      = TODAY
          xp-zsdivaspmap.operinit     = g-operinit
          xp-zsdivaspmap.transtm      = SUBSTRING(STRING(TIME,"hh:mm"),1,2) +
                                        SUBSTRING(STRING(TIME,"hh:mm"),4,2)
          xp-zsdivaspmap.transproc    = g-currproc.
       end. /* avail zsdivaspmap. */ 
      end.
      
      
    END. /* VASPSL */
  END.   /* VASPS */





END. /* Procedure */


/* ------------------------------------------------------------------------- */
PROCEDURE Create_ICSW:
/* -------------------------------------------------------------------------*/
  DEF INPUT        PARAMETER ix-whse  LIKE icsw.whse                  NO-UNDO.
  DEF INPUT        PARAMETER ix-prod  LIKE icsw.prod                  NO-UNDO.
  DEF INPUT-OUTPUT PARAMETER cpyflg   AS LOGICAL                      NO-UNDO.
  
  DEF BUFFER t-icsw FOR icsw.
  DEF BUFFER n-icsw FOR icsw.
  DEF BUFFER x-icsl for icsl.
  DEF BUFFER x-icsd for icsd.
  DEF BUFFER x-icsp for icsp.
  
  def var    ix-vendno like apsv.vendno no-undo.
  def var    ix-prodcat  like icsp.prodcat no-undo.
  ASSIGN cpyflg = NO.
  FOR EACH icsd WHERE icsd.cono    = g-cono AND
           icsd.salesfl = YES    AND
           icsd.custno  = 0
    NO-LOCK:
    FIND FIRST t-icsw WHERE
               t-icsw.cono        = g-cono  AND
               t-icsw.prod        = ix-prod AND
               t-icsw.statustype <> "X"
    NO-LOCK NO-ERROR.
    IF NOT avail t-icsw THEN
    FIND FIRST t-icsw WHERE 
               t-icsw.cono        = g-cono  AND
               t-icsw.prod        = ix-prod AND
               t-icsw.statustype  = "X"
    NO-LOCK NO-ERROR.
    
    IF avail t-icsw THEN
      do:
      ASSIGN cpyflg  = YES.
      CREATE n-icsw.
      BUFFER-COPY t-icsw 
      EXCEPT 
        t-icsw.cono
        t-icsw.whse
        t-icsw.qtyonhand
        t-icsw.qtyreservd
        t-icsw.qtycommit
        t-icsw.qtybo
        t-icsw.qtyintrans
        t-icsw.qtyunavail
        t-icsw.qtyonorder
        t-icsw.qtyrcvd
        t-icsw.qtyreqrcv
        t-icsw.qtyreqshp
        t-icsw.qtydemand
        t-icsw.lastsodt
        t-icsw.availsodt
        t-icsw.nodaysso
        t-icsw.leadtmprio
        t-icsw.leadtmlast
        t-icsw.lastltdt
        t-icsw.priorltdt
        t-icsw.lastinvdt
        t-icsw.lastrcptdt
        t-icsw.lastcntdt
        t-icsw.lastpowtdt
        t-icsw.rpt852dt
        t-icsw.priceupddt
        t-icsw.issueunytd
        t-icsw.retinunytd
        t-icsw.retouunytd
        t-icsw.rcptunytd
        t-icsw.qtydemand
        t-icsw.enterdt
        t-icsw.linept
        t-icsw.orderpt
        t-icsw.frozentype
        t-icsw.safeallamt
        t-icsw.safeallpct
        t-icsw.usagerate
        t-icsw.ordqtyin
        t-icsw.ordqtyout
        t-icsw.frozenmmyy
        t-icsw.frozenmos
        t-icsw.binloc1
        t-icsw.binloc2
      TO n-icsw
      ASSIGN 
        n-icsw.cono       = g-cono
        n-icsw.whse       = ix-whse
        n-icsw.statustype = "O"
        n-icsw.leadtmavg  = 30
        n-icsw.frozenmmyy  = STRING(MONTH(TODAY),"99") +
                           SUBSTRING(STRING(YEAR(TODAY),"9999"),3,2)
        n-icsw.frozenmos   = 6
        n-icsw.binloc1     =  "New Part"
        n-icsw.enterdt     = TODAY
        n-icsw.frozentype  = "N".


      assign ix-vendno = n-icsw.arpvendno. 
    /* assign proper vendor and discounts */
      if n-icsw.arpvendno = 70500019  or 
         n-icsw.arpvendno = 222000019 or
         n-icsw.arpvendno = 9853210   or
         n-icsw.arpvendno = 222095000 then do:
      
        assign n-icsw.stndcost = n-icsw.listprice * .40.
        find x-icsd where x-icsd.cono = g-cono and
                          x-icsd.whse = ix-whse
                          no-lock no-error.
        if avail x-icsd and (x-icsd.branchmgr = "Pab" or
                            (x-icsd.branchmgr = "Sun" and
                            (x-icsd.whse = "DROS" or
                             x-icsd.whse = "SROS"))) then 
          do:
          assign n-icsw.replcost = n-icsw.listprice * .39.
          if n-icsw.arpvendno = 70500019 then
            assign n-icsw.arpvendno = 222000019.
          if n-icsw.arpvendno = 9853210 then
            assign n-icsw.arpvendno = 222095000.
        end.
        else 
          do:
          assign n-icsw.replcost = n-icsw.listprice * .41.
          if n-icsw.arpvendno = 222000019 then
            assign n-icsw.arpvendno = 70500019.
          if n-icsw.arpvendno = 222095000 then
            assign n-icsw.arpvendno = 9853210.
        end.
        assign ix-vendno = n-icsw.arpvendno. 
        find x-icsp where x-icsp.cono = g-cono and
                          x-icsp.prod = n-icsw.prod
                          no-lock no-error.
        if avail x-icsp then
          assign ix-prodcat = x-icsp.prodcat.
        else
          assign ix-prodcat = "".
          
        find notes use-index k-notes where
             notes.cono         = g-cono and
             notes.notestype    = "zz" and
             notes.primarykey   = "standard cost markup" and
             notes.secondarykey = ix-prodcat and
             notes.pageno       = 1                           
             no-lock no-error.
        if not avail notes then
          find notes use-index k-notes where
               notes.cono         = g-cono and
               notes.notestype    = "zz" and
               notes.primarykey   = "standard cost markup" and
               notes.secondarykey = "" and
               notes.pageno       = 1
               no-lock no-error.
        if avail notes and dec(notes.noteln[1]) > 0 then
          assign n-icsw.stndcost   = n-icsw.replcost *
                                   (1 + (dec(notes.noteln[1]) / 100)).
        else
          assign n-icsw.stndcost   = n-icsw.replcost * 1.03.
      end.
 
      
      find icsl where icsl.cono   = g-cono and
                      icsl.whse     = t-icsw.whse and 
                      icsl.vendno   = t-icsw.arpvendno and
                      icsl.prodline = t-icsw.prodline no-lock no-error.
     
      if avail icsl then do:
      
        find x-icsl where x-icsl.cono     = g-cono and
                          x-icsl.whse     = n-icsw.whse and 
                          x-icsl.vendno   = ix-vendno and
                          x-icsl.prodline = n-icsw.prodline no-lock no-error.
        if not avail x-icsl then do:
          create x-icsl.
          buffer-copy icsl except icsl.cono
                                  icsl.whse
          to x-icsl
           assign x-icsl.cono = g-cono
                  x-icsl.whse = n-icsw.whse.
                
        /* assign proper vendor */
          if t-icsw.arpvendno = 70500019  or 
             t-icsw.arpvendno = 222000019 or
             t-icsw.arpvendno = 9853210   or
             t-icsw.arpvendno = 222095000 then do:
             find x-icsd where x-icsd.cono = g-cono and 
                               x-icsd.whse = n-icsw.whse 
                               no-lock no-error.
            if avail x-icsd and (x-icsd.branchmgr = "Pab" or
                                (x-icsd.branchmgr = "Sun" and
                                (x-icsd.whse = "DROS" or
                                 x-icsd.whse = "SROS"))) then do:
              if t-icsw.arpvendno = 70500019 then
                assign x-icsl.vendno = 222000019.
              if t-icsw.arpvendno = 9853210 then
                assign x-icsl.vendno = 222095000.
            end.
            else do:
              if t-icsw.arpvendno = 222000019 then
                assign x-icsl.vendno = 70500019.
              if t-icsw.arpvendno = 222095000 then
                assign x-icsl.vendno = 9853210.
            end.
          end.      
        end.
      end.
      find icswu where 
           icswu.cono = g-cono and
           icswu.prod = ix-prod and
           icswu.whse = ix-whse no-lock no-error.
      if not avail icswu then do:
        CREATE icswu.
        ASSIGN 
          icswu.cono        = g-cono
          icswu.prod        = ix-prod
          icswu.whse        = ix-whse.
        {t-all.i icswu}
        RELEASE icswu.
      end.
      LEAVE.
    END. /* if avail t-icsw */
  END. /* for each icsd */
  
END. /* Procedure Create_ICSW */




