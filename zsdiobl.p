/*h*****************************************************************************
  PROCEDURE    : zsdiobl
  DESCRIPTION  : Batch line item display
  AUTHOR       : tah
  DATE WRITTEN : 04/17/06
  CHANGES MADE : dkt 10/03/2006
                 v-batchnm as input parameter
*******************************************************************************/
define input parameter v-batchnm like oeehb.orderno.
{g-all.i}
{g-oe.i}
{g-inq.i}
{g-margin.i}
{g-oeetkp.i "new" "/*"}
{g-ic.i}
{g-conv.i}
{g-oeetk.i "new"}
{speccost.gva}
{g-bp.i}
{arexchg.gva "new"}

def            var v-fkey         as c format "x(5)" extent 5 initial
    ["    ","     ","    ","    ","     "]                             no-undo.
def            var v-msg          as c format "x(78)" initial
" Ln# N Product                   Quantity    Unit     Price      Discount   "
                                                                       no-undo.
def            var v-fmsg         as c format "x(78)" initial
/*
 " F6-Return      F7-Extended   F8-Serial/Lots  F9-History     F10-Kit        "
*/
 "                F7-Extended                                  F10-Kit        "
                                                                        no-undo.
def     shared var v-oeio99fl     as logical                           no-undo.
def new shared var s-commentfl    as c format "x"                      no-undo.
def new shared var s-disctype     like oeel.disctype                   no-undo.
def new shared var s-discamt      like oeel.discamt                    no-undo.
def new shared var s-discpct      like oeel.discpct                    no-undo.
def new shared var s-chrgqty      like oeel.chrgqty                    no-undo.


def new shared var s-jitflag      as c format "x(1)"                   no-undo.
def new shared var s-lineno       like oeel.lineno                     no-undo.
def new shared var s-netamt       like oeel.netamt                     no-undo.
def new shared var s-netord       like oeel.netord                     no-undo.
def new shared var s-speccost     as c format "x(4)"                   no-undo.
def new shared var s-notesfl      like icsp.notesfl                    no-undo.
def new shared var s-price        like oeel.price                      no-undo.
def new shared var s-prod         like oeel.shipprod                   no-undo.
def new shared var s-proddesc     as c format "x(49)"                  no-undo.
def new shared var s-qtybreakty   like pdsc.qtybreakty                 no-undo.
def new shared var s-returnfl     like oeel.returnfl                   no-undo.
def new shared var s-return       as c format "x"                      no-undo.
def new shared var s-qtyord       like oeel.qtyord                     no-undo.
def new shared var s-rushdesc     as c format "x(4)"                   no-undo.
def new shared var s-specnstype   like oeel.specnstype                 no-undo.
def new shared var s-URL          as c format "x(1)"                   no-undo.
def new shared var s-unit         like oeel.unit                       no-undo.

{g-secure.i}

def            var v-oecostsale   like sasc.oecostsale                 no-undo.
def            var s-botype       like oeel.botype initial "n"         no-undo.
def            var s-commtype     like oeel.commtype                   no-undo.
def            var s-corecharge   like oeel.corecharge                 no-undo.
def            var s-coretitle    as c format "x(12)"                  no-undo.
def            var s-disccd       like oeel.disccd                     no-undo.
def            var s-gststatus    like oeel.gststatus                  no-undo.
def            var s-jobno        like oeel.jobno                      no-undo.
def            var s-kitfl        as logical initial "no"              no-undo.
def            var s-kitrollty    like oeel.kitrollty initial "no"     no-undo.
def            var s-marginamt    as dec format "zzzzz9.99999"         no-undo.
def            var s-marginpct    as dec format "zzz9.99-"             no-undo.
def            var s-netavail     as dec format "zzzzzzzz9.99-"        no-undo.
def            var s-nontaxtype   like oeel.nontaxtype                 no-undo.
def            var s-ordertype    like oeel.ordertype                  no-undo.
def            var s-pdcost       like oeel.prodcost                   no-undo.
def            var s-priceclty    like oeel.priceclty                  no-undo.
def            var s-pricecd      like oeel.pricecd                    no-undo.
def            var s-pricelevel   like arsc.pricecd                    no-undo.
def            var s-printpricefl like oeel.printpricefl               no-undo.
def            var s-subtotalfl   like oeel.subtotalfl                 no-undo.
def            var s-prodcat      like oeel.prodcat                    no-undo.
def            var s-prodcost     like oeel.prodcost                   no-undo.
def            var s-prodline     like oeel.prodline                   no-undo.
def            var s-qtyship      like oeel.qtyship                    no-undo.
def            var s-qtyunavail   like oeel.qtyunavail                 no-undo.
def            var s-reasunavty   like oeel.reasunavty                 no-undo.
def            var s-slsrepin     like oeel.slsrepin                   no-undo.
def            var s-slsrepout    like oeel.slsrepout                  no-undo.
def            var s-taxablefl    like oeel.taxablefl                  no-undo.
def            var s-taxgroup     like oeel.taxgroup                   no-undo.
def            var s-termspct     like oeel.termspct                   no-undo.
def            var s-totcorechg   like oeeh.totcorechg                 no-undo.
def            var s-totlineamt   like oeeh.totlineamt                 no-undo.
def            var s-usagefl      like oeel.usagefl initial "yes"      no-undo.
def            var s-vendno       like oeel.vendno                     no-undo.
def            var s-whse         like icsw.arpwhse                    no-undo.
def            var s-linedo       as char format "x"                   no-undo.
def            var v-smcustrebfl  like sasc.smcustrebfl                no-undo.
def            var v-smvendrebfl  like sasc.smvendrebfl                no-undo.
def     shared var v-oerebty      like sasoo.oerebty                   no-undo.
def            var v-custrebamt   like pder.rebateamt                  no-undo.
def            var v-vendrebamt   like pder.rebateamt                  no-undo.
def var s-position as integer                init 0   no-undo.
def var s-zeroamt  like     oeelb.price      init 0   no-undo.
def buffer ic-notes for notes.
{zsdiobl.z99 &user_define = "*"}


v-length = 7.

form
    v-msg                             at  1  no-label skip(13)
    {f-frmend.i}
with frame f-oeiols overlay no-labels row 5 centered
title "Line Items For " + string(v-batchnm) + "q".

form
    s-commentfl                       at  1
    s-lineno                          at  2
    s-linedo                          at  5
    s-specnstype                      at  6
    s-prod                            at  8
    s-notesfl                         at 32
    s-qtyord                          at 33
    s-returnfl                        at 43
    s-return                          at 44
    s-unit                            at 46
    s-price                           at 51
    s-qtybreakty                      at 64
    s-discamt format "zzzzz9.99999"   at 66
    s-disctype                        at 78
    s-rushdesc                        at  1
    s-jitflag                         at  6
    s-proddesc                        at  8
    s-speccost                        at 59
    s-netamt                          at 66
with frame f-oeiol overlay no-labels centered scroll 1
    no-box row 7 v-length down.

pause 0 before-hide.

/*tb 20100 02/02/96 gdf; Added find for sasoo and assign of v-oerebty. */
do for sasc, sasoo:

    {w-sasc.i no-lock}
    {w-sasoo.i g-operinit no-lock}

    assign
        v-oecostsale  = if avail sasc then sasc.oecostsale   else no
        v-smcustrebfl = if avail sasc then sasc.smcustrebfl  else no
        v-smvendrebfl = if avail sasc then sasc.smvendrebfl  else no
        v-oerebty     = if avail sasoo then sasoo.oerebty else "n".

end. /* end of do for sasc, sasoo */

main:
do while true with frame f-oeiol on endkey undo main, leave main:
    if {k-cancel.i} or {k-jump.i} or {k-func13.i} then leave main.

    view frame f-oeiols.
    hide frame f-oeiol.

    display v-msg with frame f-oeiols.
    color display input v-msg with frame f-oeiols.


    find oeehb where oeehb.cono = g-cono and
                     oeehb.batchnm = string(v-batchnm,">>>>>>>9")  and
                     oeehb.seqno = 1 no-lock no-error.

    put screen row 21 col 3 color message v-fmsg.

    repeat:
 
     {tomscroller.i
          &find      = "n-zsdiobl.i"
          &go-on     = " 'CTRL-K'"
          &frame     = "f-oeiol"
          &file      = "oeelb"
          &field     = "s-lineno"
          &display   = "d-zsdiobl.i"
          &where     = "p-zsdiobl.i"
          &usecode   = *
          &usefile   = "a-zsdiobl.i"
          &hookname  = "zsdioblscroll.z99"  
          &global    = "g-oelineno = integer(frame-value)"}.

    end. /* end of repeat block */

    leave main.

end. /* end of main block */

if {k-func13.i} and not g-lkupfl then return.
hide frame f-oeiols no-pause.
hide frame f-oeiol
no-pause.
