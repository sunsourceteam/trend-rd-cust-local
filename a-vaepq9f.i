/* a-oeepa9.i 1.3 11/11/92 */
/*h*****************************************************************************
  INCLUDE      : a-oeepa9.i
  DESCRIPTION  :
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    04/03/92 pap; TB# 6219 Line DO - add direct order handling
    11/11/92 mms; TB# 8561 Display product surcharge label from ICAO
    04/17/97 JH; ITB# 1     Center "acknowledgement", remove "upc Vendor",
                            change "order #" to "Invoce #", change
                            "correspondence to" to "remit to", change 
                            "shipped date" to "expected ship date",
                            remove "upc item #.
*******************************************************************************/

if v-headingfl = yes then assign
    {1}cancel = if oeeh.stagecd = 9 then "*** C A N C E L L E D ***"
                else ""
/*  {1}lit1b  = "UPC Vendor   Invoice Date     Order #" */
    {1}lit1b  = 
    (if oeeh.transtype = "qu" then
     "                              Quote #  "  /* (ITB# 1) */
    else
     "                              Order #  ")  /* (ITB# 1) */
        
    {1}lit2a  = if can-do("ra,rm",oeeh.transtype) then
                    "===================================================="
                else if oeeh.transtype = "cr" then
                    "============================================="
                else ""
    {1}lit3a  = "  Cust #:"
    {1}lit3b  = "PO Date   PO #                   Page #"
    {1}lit6a  = "Bill To:"
    {1}lit6b  = "Correspondence To  "
    {1}lit6bb = "(ONLY)"          
    {1}lit8a  = if g-country = "ca" then "PST Lic#:" else ""
    {1}lit9a  = if g-country = "ca" then "GST Reg#:" else ""
    {1}lit11a = "Ship To:"
    {1}lit11b = "Instructions"
    {1}lit12a = "Ship Point"
    {1}lit12b = "Via"
/*  {1}lit12c = "Shipped"        (ITB# 1)  */
    {1}lit12c = "To Ship By"    /*  (ITB# 1)  */
    {1}lit12d = "Terms"
    {1}lit14a =
"    Product                            Quantity     Quantity     Quantity     "
    {1}lit14b =
"Qty.     Unit       Price                      Amount    "
    {1}lit15a =
"Ln# And Description                    Ordered        B.O.       Shipped      "
    {1}lit15b =
" UM      Price       UM                        (Net)     "
    {1}lit16a =
"------------------------------------------------------------------------------"
    {1}lit16b =
"-------------------------------------------------------".
   
assign
    {1}lit1aa = if v-headingfl = yes then "Document: " else ""
    {1}lit1a  = "Value Add - Order Quote"

              /*
                if oeeh.transtype = "rm" then
                    "Order Quote  - Return Merchandise"
                else if oeeh.transtype = "cr" then
                    "Order Acknowledgment - Correction"
                else if oeeh.transtype = "ra" then
                    "Order Acknowledgment - Received On Account"
                else if oeeh.transtype = "qu" then
                    "Order Acknowledgment - Quote Order"
                else "Order Acknowledgment"
         */
     {1}lit1a = "Value Add - Order Quote"
         /*       
                if oeeh.transtype = "bl" then
                    "Order Acknowledgment - Blanket Order"
                else if oeeh.transtype = "do" then
                    "Order Acknowledgment - Direct Order"
                else if oeeh.transtype = "fo" then
                    "Order Acknowledgment - Future Order"
                else if oeeh.transtype = "st" then
                    "Order Acknowledgment - Standing Order"
                else if oeeh.transtype = "br" and oeeh.stagecd <> 0 then
                    "Order Acknowledgment - Blanket Release"
                else if oeeh.transtype = "br" and oeeh.stagecd = 0 then
                    "Order Acknowledgment - Blanket Release (Entered)"
                else {1}lit1a
        */
    {1}lit40a = "Lines      "
    {1}lit40c = "Qty Shipped Total"   
    {1}lit40d = if can-do("bl,br",oeeh.transtype) and
                    oeeh.lumpbillfl = yes then
                "Amount Billed     " else
                "Total             "
    {1}lit41a = "Order Discount    "
    {1}lit42a = "Other Discount    "
    {1}lit43a = "Core Charge       "
    {1}lit44a = v-icdatclabel
    {1}lit45a = "Taxes             "
    {1}lit46a = "Downpayment       "
    {1}lit46b = "Payment           "
    {1}lit47a = "Order Total       "
    {1}lit47b = "Payment           "
    {1}lit48a = "Continued"
    {1}lit49a = "G.S.T.            "
    {1}lit50a = "P.S.T.            "
    {1}lit53a = "Restock Amount    ".
