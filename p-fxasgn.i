/*h*****************************************************************************
  INCLUDE      : p-fxasgn.i
  DESCRIPTION  : Assign fax info to sapb
  USED ONCE?   : no (oeetg.p and poetg.p)
  AUTHOR       : rhl
  DATE WRITTEN : 10/28/93
  CHANGES MADE :
    04/22//2005   dkt stopped assigning faxpriority (boolean)
    09/15/95 dww; TB# 19314 Remove Imbedded spaces from the Fax Phone Number
    05/05/99 jl;  TB# 17865 Expanded tag updated for faxing. Added assign
            of faxtag1 and faxtag2
    01/10/00 sbr; TB# 26770 F10 Fax - Invoice Print, Fax # not used
                *******************************************************************************/
if v-faxphoneno ne "" then do:
  assign
    sapb.faxphoneno     = v-faxphoneno
    /* sapb.faxpriority    = v-faxpriority */
    sapb.faxfrom        = v-faxfrom
    sapb.faxto1         = v-faxto1
    sapb.faxto2         = v-faxto2
    sapb.faxtag1        = {&faxtag1}
    sapb.faxtag2        = {&faxtag2}.

  do i = 1 to 10:
    sapb.faxcom[i] = v-faxcom[i].
  end.

  /*tb 26770 01/10/00 sbr; Removed code that turned off fax if appropriate */
  /* turn off fax if appropriate */
  /* sapb.optvalue[{&option}] = "no". */

  if sapb.faxphoneno ne "" then do:
    /*tb 19314 09/15/95 dww; Remove Imbedded Space from Fax Phone Number */
    {fixphone.gca sapb.faxphoneno sapb.faxphoneno {&comvar}}
  end.

end.
