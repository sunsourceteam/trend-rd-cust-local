procedure regionalize:

/* -----------------------------------------------------------------------
   Header Variable Initialization
   ----------------------------------------------------------------------*/
   assign dobuyinhead = false.
   assign
        v-phase2 = true
        buyg-cur = 0
        buyg-ncgs = 0
        buyg-ncur = 0
        buyg-cgs = 0
        buyg-avg = 0
        buyg-slo = 0
        buyg-obs = 0
        buyg-cln = 0
        buyg-tln = 0
        buyg-cqty = 0
        buyg-tqty = 0
        buyg-cval = 0
        buyg-tval =0.
    assign
        buyt-cur = 0
        buyt-ncgs = 0
        buyt-ncur = 0
        buyt-cgs = 0
        buyt-avg = 0
        buyt-slo = 0
        buyt-obs = 0
        buyt-cln = 0
        buyt-tln = 0
        buyt-cqty = 0
        buyt-tqty = 0
        buyt-cval = 0
        buyt-tval = 0.
    assign
        buy-cur = 0
        buy-cgs = 0
        buy-ncur = 0
        buy-ncgs = 0
        buy-avg = 0
        buy-slo = 0
        buy-obs = 0
        buy-cln = 0
        buy-tln = 0
        buy-cqty = 0
        buy-tqty = 0
        buy-cval = 0
        buy-tval = 0.


message "regionalizer".

for each buyers:
  delete buyers.

end.
page.

rpt-user = userid("dictdb").
rpt-dt = today.
rpt-time = string(time, "HH:MM").
rpt-dow = entry(weekday(today),dow-lst).
rpt-cono = g-cono.
x-page = 0.

dofirstheaders = true.
dobuyinhead = false.
d-option = "Region".
/*
for each xuyers no-lock:
  export stream jxx xuyers.
end.  
*/


for each xuyers exclusive-lock
     break  
      by  xuyers.wuys-group 
      by  xuyers.wuys-buyer 
      by  xuyers.wuys-vendor
      by  xuyers.wuys-cur descending 
      by  xuyers.wuys-prod with frame f-vend: 
    if first-of(xuyers.wuys-group) and o-master
       and o-sort <> "w" then
      do:
      if opendept = true then
        output stream dept close.
      if o-master then
        fylename = "/usr/tmp/icxrd" + LC(xuyers.wuys-group).
      else
        fylename = "/usr/tmp/icxrz" + LC(xuyers.wuys-group).

      output stream dept to value(fylename).
      os-command silent value ("chmod 666 " + fylename).
      opendept = true.
      put stream dept control compress-string.  
      end.    
    if first-of(xuyers.wuys-group) and o-master and o-sort = "w" then
      do:
      if opendept = true then
        output stream dept close.
      if o-master then
        fylename = "/usr/tmp/icxrreg" + LC(xuyers.wuys-group).
      else
        fylename = "/usr/tmp/icxrreg" + LC(xuyers.wuys-group).

      output stream dept to value(fylename).
      os-command silent value ("chmod 666 " + fylename).
      opendept = true.
      put stream dept control compress-string.  
      end.    





    assign cur-inv = xuyers.wuys-cur + cur-inv
           avg-inv = xuyers.wuys-avg + avg-inv
           n-inv   = xuyers.wuys-ncur + n-inv
           n-cog   = xuyers.wuys-ncgs + n-cog
           slo-inv = xuyers.wuys-slo + slo-inv
           tot-ln  = xuyers.wuys-tln + tot-ln
           tot-qty = xuyers.wuys-tqty + tot-qty
           tot-val = xuyers.wuys-tval + tot-val
           comp-ln = xuyers.wuys-cln + comp-ln
           comp-qty = xuyers.wuys-cqty + comp-qty
           comp-val = xuyers.wuys-cval + comp-val
           n-cgs = xuyers.wuys-cgs + n-cgs
           obs-inv = xuyers.wuys-obs + obs-inv.
           
 if x-line-counter > 42 and o-rsummary <> "s" then
   run headers.

  
 if o-rsummary = "p" then
    do:

    assign xcur-inv =  xuyers.wuys-cur 
           xavg-inv =  xuyers.wuys-avg 
           xn-inv   =  xuyers.wuys-ncur 
           xn-cog   =  xuyers.wuys-ncgs 
           xslo-inv =  xuyers.wuys-slo 
           xtot-ln  =  xuyers.wuys-tln 
           xtot-qty =  xuyers.wuys-tqty
           xtot-val =  xuyers.wuys-tval
           xcomp-ln =  xuyers.wuys-cln
           xcomp-qty = xuyers.wuys-cqty
           xcomp-val = xuyers.wuys-cval
           xn-cgs =  xuyers.wuys-cgs 
           xobs-inv = xuyers.wuys-obs.

     assign
       d-turn  = 0
       slo-pct = 0
       obs-pct = 0.

                

     if i-turnmo <> 0 then
       do:
       xn-inv = xn-inv.
       xavg-inv = xavg-inv.    /*  / i-turnmo.             avg annual inv */
       end.
     if i-turnmo <> 0 then
       do:
       xn-cgs   = (xn-cgs / i-turnmo) * 12.         /* annualized cgs */
       xn-cog   = (xn-cog / i-turnmo) * 12.
       end.
     if (xavg-inv + xn-inv) <> 0 then
       d-turn  = (xn-cgs + xn-cog) / (xavg-inv + xn-inv).   /* turnover */
     if xcur-inv <> 0 then
       slo-pct = (xslo-inv / xcur-inv) * 100.       /* slow moving % */
     if xcur-inv <> 0 then
       obs-pct = (xobs-inv / xcur-inv) * 100.       /* obsolete % */
     if xtot-ln <> 0 then
       xcomp-ln = (xcomp-ln / xtot-ln) * 100.        /* line fill rate */
     if xtot-qty <> 0 then
       xcomp-qty = (xcomp-qty / xtot-qty) * 100.    /* qty fill rate */
     if xtot-val <> 0 then
       xcomp-val = (xcomp-val / xtot-val) * 100.    /* value fill rate */
     if d-turn > 9999 then
        d-turn = 9999.99.
     else
     if d-turn < -9999 then
       d-turn = -9999.99.
     if slo-pct > 9999 then
       slo-pct = 9999.99.
     else
     if slo-pct < -9999 then
       slo-pct = -9999.99.
     if obs-pct > 9999 then
       obs-pct = 9999.99.
     else
     if obs-pct < -9999 then
       obs-pct = -9999.99.
     if xcomp-ln > 9999 then
        xcomp-ln = 9999.9.
     else
     if xcomp-ln < -9999 then
       xcomp-ln = -9999.9.

     if xcomp-qty > 9999 then
        xcomp-qty = 9999.9.
     else
     if xcomp-qty < -9999 then
       xcomp-qty = -9999.9.

     if xcomp-val > 9999 then
        xcomp-val = 9999.9.
     else
     if xcomp-val < -9999 then
       xcomp-val = -9999.9.

     if o-rsummary <> "s" and
       (xcur-inv ge 1 or
        xavg-inv ge 1 or
        xn-inv   ge 1 or
        xn-cog   ge 1 or
        xslo-inv ge 1 or
        xobs-inv ge 1 or
        xcur-inv le -1 or
        xavg-inv le -1 or
        xn-inv   le -1 or
        xn-cog   le -1 or
        xslo-inv le -1 or
        xobs-inv le -1 or
        xtot-ln  <> 0 or
        xtot-qty <> 0 or
        xtot-val <> 0 or
        xcomp-ln <> 0 or
        xcomp-qty <> 0 or
        xcomp-val <> 0 or
        xn-cgs ge 1 or
        xn-cgs le -1)
                      then
        do:
             
        if dofirstheaders and o-rsummary <> "s" then
          do:
          run headers.
          dofirstheaders = false.
          end.
 
         display xuyers.wuys-prod
                 round ((xcur-inv / 1),0) @ cur-inv 
                 round ((xn-cgs / 1),0) @ n-cgs 
             /*    truncate ((xavg-inv / 1),0) @ avg-inv      */
                 round ((xn-inv / 1),0) @ n-inv 
                 round ((xn-cog / 1),0) @ n-cog 
                 d-turn
                 round ((xslo-inv / 1),0) @ slo-inv 
                 slo-pct 
                 round ((xobs-inv / 1),0) @ obs-inv 
                 obs-pct 
                 xcomp-ln @ comp-ln 
                 xcomp-qty @ comp-qty
                 xcomp-val @ comp-val
                 xuyers.wuys-brk 
            with frame f-xdetail.
          down with frame f-xdetail.
          x-line-counter = x-line-counter + 1.
       end.    
    end.
   
    if last-of(xuyers.wuys-vendor) then do:
    
      assign
        buy-cur = buy-cur + cur-inv
        buy-cgs = buy-cgs + n-cgs
        buy-ncur = buy-ncur + n-inv
        buy-ncgs = buy-ncgs + n-cog
        buy-avg = buy-avg + avg-inv
        buy-slo = buy-slo + slo-inv
        buy-obs = buy-obs + obs-inv
        buy-cln = buy-cln + comp-ln
        buy-tln = buy-tln + tot-ln
        buy-cqty = buy-cqty + comp-qty
        buy-tqty = buy-tqty + tot-qty
        buy-cval = buy-cval + comp-val
        buy-tval = buy-tval + tot-val
        buyg-cur = buyg-cur + cur-inv
        buyg-cgs = buyg-cgs + n-cgs
        buyg-ncur = buyg-ncur + n-inv
        buyg-ncgs = buyg-ncgs + n-cog
        buyg-avg = buyg-avg + avg-inv
        buyg-slo = buyg-slo + slo-inv
        buyg-obs = buyg-obs + obs-inv
        buyg-cln = buyg-cln + comp-ln
        buyg-tln = buyg-tln + tot-ln
        buyg-cqty = buyg-cqty + comp-qty
        buyg-tqty = buyg-tqty + tot-qty
        buyg-cval = buyg-cval + comp-val
        buyg-tval = buyg-tval + tot-val
        buyt-cur = buyt-cur + cur-inv
        buyt-cgs = buyt-cgs + n-cgs
        buyt-ncur = buyt-ncur + n-inv
        buyt-ncgs = buyt-ncgs + n-cog
        buyt-avg = buyt-avg + avg-inv
        buyt-slo = buyt-slo + slo-inv
        buyt-obs = buyt-obs + obs-inv
        buyt-cln = buyt-cln + comp-ln
        buyt-tln = buyt-tln + tot-ln
        buyt-cqty = buyt-cqty + comp-qty
        buyt-tqty = buyt-tqty + tot-qty
        buyt-cval = buyt-cval + comp-val
        buyt-tval = buyt-tval + tot-val.

        assign
         d-turn  = 0
         slo-pct = 0
         obs-pct = 0.

                

        if i-turnmo <> 0 then
          do:
          n-inv = n-inv.
          avg-inv = avg-inv.    /*  / i-turnmo.             avg annual inv */
          end.
        if i-turnmo <> 0 then
          do:
          n-cgs   = (n-cgs / i-turnmo) * 12.         /* annualized cgs */
          n-cog   = (n-cog / i-turnmo) * 12.
          end.
        if (avg-inv + n-inv) <> 0 then
          d-turn  = (n-cgs + n-cog) / (avg-inv + n-inv).   /* turnover */
        if cur-inv <> 0 then
          slo-pct = (slo-inv / cur-inv) * 100.       /* slow moving % */
        if cur-inv <> 0 then
          obs-pct = (obs-inv / cur-inv) * 100.       /* obsolete % */
        if tot-ln <> 0 then
          comp-ln = (comp-ln / tot-ln) * 100.        /* line fill rate */
        if tot-qty <> 0 then
          comp-qty = (comp-qty / tot-qty) * 100.    /* qty fill rate */
        if tot-val <> 0 then
          comp-val = (comp-val / tot-val) * 100.    /* value fill rate */

        find apsv where apsv.cono = g-cono
                    and apsv.vendno = xuyers.wuys-vendor
                    no-lock no-error.

        assign d-name = if avail apsv then apsv.lookupnm else "".
        
        

       if d-turn > 9999 then
          d-turn = 9999.99.
       else
       if d-turn < -9999 then
         d-turn = -9999.99.

       if slo-pct > 9999 then
          slo-pct = 9999.99.
       else
       if slo-pct < -9999 then
         slo-pct = -9999.99.

       if obs-pct > 9999 then
          obs-pct = 9999.99.
       else
       if obs-pct < -9999 then
         obs-pct = -9999.99.

       if comp-ln > 9999 then
          comp-ln = 9999.9.
       else
       if comp-ln < -9999 then
         comp-ln = -9999.9.

       if comp-qty > 9999 then
          comp-qty = 9999.9.
       else
       if comp-qty < -9999 then
         comp-qty = -9999.9.

       if comp-val > 9999 then
          comp-val = 9999.9.
       else
       if comp-val < -9999 then
         comp-val = -9999.9.

       if o-rsummary <> "x" and
         (cur-inv ge 1 or
         avg-inv ge 1 or
         n-inv   ge 1 or
         n-cog   ge 1 or
         slo-inv ge 1 or
         obs-inv ge 1 or
         cur-inv le -1 or
         avg-inv le -1 or
         n-inv   le -1 or
         n-cog   le -1 or
         slo-inv le -1 or
         obs-inv le -1 or
         tot-ln  <> 0 or
         tot-qty <> 0 or
         tot-val <> 0 or
         comp-ln <> 0 or
         comp-qty <> 0 or
         comp-val <> 0 or
         n-cgs ge 1 or
         n-cgs le -1)
                       then
         do:
             
        if dofirstheaders and o-rsummary <> "x" then
          do:
          run headers.
          dofirstheaders = false.
          end.
 
         display xuyers.wuys-vendor @ wuyers.wuys-vendor
                 d-name
                 round ((cur-inv / 1),0) @ cur-inv 
                 round ((n-cgs / 1),0) @ n-cgs 
             /*    truncate ((avg-inv / 1),0) @ avg-inv      */
                 round ((n-inv / 1),0) @ n-inv 
                 round ((n-cog / 1),0) @ n-cog 
                 d-turn
                 round ((slo-inv / 1),0) @ slo-inv 
                 slo-pct 
                 round ((obs-inv / 1),0) @ obs-inv 
                 obs-pct 
                 comp-ln 
                 comp-qty 
                 comp-val
            with frame f-vend down.
          x-line-counter = x-line-counter + 1.
          if o-master then
          display stream dept 
                 xuyers.wuys-vendor @ wuyers.wuys-vendor
                 d-name
                 round ((cur-inv / 1),0) @ cur-inv 
                 round ((n-cgs / 1),0) @ n-cgs 
             /*    truncate ((avg-inv / 1),0) @ avg-inv      */
                 round ((n-inv / 1),0) @ n-inv 
                 round ((n-cog / 1),0) @ n-cog 
                 d-turn
                 round ((slo-inv / 1),0) @ slo-inv 
                 slo-pct 
                 round ((obs-inv / 1),0) @ obs-inv 
                 obs-pct 
                 comp-ln 
                 comp-qty 
                 comp-val
            with frame f-vend down.
  
          end.
        

    assign cur-inv = 0
           avg-inv = 0
           n-inv   = 0
           n-cog   = 0
           slo-inv = 0
           tot-ln  = 0
           tot-qty = 0
           tot-val = 0
           comp-ln = 0
           comp-qty = 0
           comp-val = 0
           n-cgs = 0
           obs-inv = 0.
 
    end.  /* last of vendor */
    
    if last-of(xuyers.wuys-buyer) then do:

        find buyers where buyers.buys-group =
                          xuyers.wuys-group and      
                          buyers.buys-buyer =
                          xuyers.wuys-buyer no-error.
        if not avail buyers then
          do:
          create buyers.
          assign
              buyers.buys-group =
                     xuyers.wuys-group        
               buyers.buys-buyer =
                    xuyers.wuys-buyer 
               buyers.buys-name = 
                     xuyers.wuys-name
               buyers.buys-cur = buy-cur
               buyers.buys-cgs = buy-cgs
               buyers.buys-avg = buy-avg 
               buyers.buys-ncgs = buy-ncgs
               buyers.buys-ncur = buy-ncur 
               buyers.buys-slo = buy-slo 
               buyers.buys-obs = buy-obs 
               buyers.buys-cln = buy-cln 
               buyers.buys-tln = buy-tln
               buyers.buys-cqty = buy-cqty
               buyers.buys-tqty = buy-tqty 
               buyers.buys-cval = buy-cval
               buyers.buys-tval = buy-tval. 
          end.
        else
          assign
               buyers.buys-cur = buy-cur +  buyers.buys-cur
               buyers.buys-cgs = buy-cgs +  buyers.buys-cgs
               buyers.buys-ncur = buy-ncur +  buyers.buys-ncur
               buyers.buys-ncgs = buy-ncgs +  buyers.buys-ncgs
               buyers.buys-avg = buy-avg +  buyers.buys-avg
               buyers.buys-slo = buy-slo +  buyers.buys-slo
               buyers.buys-obs = buy-obs +  buyers.buys-obs
               buyers.buys-cln = buy-cln +  buyers.buys-cln
               buyers.buys-tln = buy-tln  + buyers.buys-tln
               buyers.buys-cqty = buy-cqty + buyers.buys-cqty
               buyers.buys-tqty = buy-tqty + buyers.buys-tqty
               buyers.buys-cval = buy-cval + buyers.buys-cval
               buyers.buys-tval = buy-tval + buyers.buys-tval.
    

        assign
          d-turn  = 0
          slo-pct = 0
          obs-pct = 0.
        
        

        if i-turnmo <> 0 then
          do:
          buy-avg= buy-avg.    /*  / i-turnmo.             avg annual inv */
          buy-ncur = buy-ncur.
          end.
        if i-turnmo <> 0 then
          do:
          buy-cgs   = (buy-cgs / i-turnmo) * 12.         /* annualized cgs */
          buy-ncgs  = (buy-ncgs / i-turnmo) * 12.
          end.
        if (buy-avg + buy-ncur) <> 0 then
          d-turn  = (buy-cgs + buy-ncgs) / (buy-avg + buy-ncur).
            /* turnover */
        if buy-cur <> 0 then
          slo-pct = (buy-slo / buy-cur) * 100.       /* slow moving % */
        if buy-cur <> 0 then
          obs-pct = (buy-obs / buy-cur) * 100.       /* obsolete % */
        if buy-tln <> 0 then
          buy-cln = (buy-cln / buy-tln) * 100.        /* line fill rate */
        if buy-tqty <> 0 then
          buy-cqty = (buy-cqty / buy-tqty) * 100.    /* qty fill rate */
        if buy-tval <> 0 then
          buy-cval = (buy-cval / buy-tval) * 100.    /* value fill rate */
 
      
 
        if d-turn > 9999 then
           d-turn = 9999.99.
        else
        if d-turn < -9999 then
          d-turn = -9999.99.

        if slo-pct > 9999 then
           slo-pct = 9999.99.
        else
        if slo-pct < -9999 then
          slo-pct = -9999.99.

        if obs-pct > 9999 then
           obs-pct = 9999.99.
        else
        if obs-pct < -9999 then
          obs-pct = -9999.99.

        if buy-cln > 9999 then
           buy-cln = 9999.9.
        else
        if buy-cln < -9999 then
          buy-cln = -9999.9.

        if buy-cqty > 9999 then
           buy-cqty = 9999.9.
        else
        if buy-cqty < -9999 then
          buy-cqty = -9999.9.

        if buy-cval > 9999 then
           buy-cval = 9999.9.
        else
        if buy-cval < -9999 then
          buy-cval = -9999.9.
        bbuy = xuyers.wuys-buyer.
        z-name = xuyers.wuys-name.
        if o-rsummary <> "x" and o-sort <> "v" and
        (buy-cur  ge 1 or
          buy-cgs  ge 1 or
          buy-avg  ge 1 or
          buy-ncur ge 1 or
          buy-ncgs ge 1 or
          buy-slo  ge 1 or
          buy-obs  ge 1 or
          buy-cur  le -1 or
          buy-cgs  le -1 or
          buy-avg  le -1 or
          buy-ncur le -1 or
          buy-ncgs le -1 or
          buy-slo  le -1 or
          buy-obs  le -1 or
          buy-cln  <> 0 or
          buy-tln  <> 0 or
          buy-cqty <> 0 or
          buy-tqty <> 0 or
          buy-cval <> 0 or
          buy-tval <> 0)
          then
           do:
           display bbuy
                   z-name
                   round ((buy-cur / 1),0) @ buy-cur 
                   round ((buy-cgs / 1),0) @ buy-cgs 
               /*    truncate ((buy-avg / 1),0) @ buy-avg    */
                   round ((buy-ncur / 1),0) @ buy-ncur 
                   round ((buy-ncgs / 1),0) @ buy-ncgs 
                   d-turn 
                   round ((buy-slo / 1),0) @ buy-slo 
                   slo-pct
                   round ((buy-obs / 1),0) @ buy-obs 
                   obs-pct 
                   buy-cln 
                   buy-cqty 
                   buy-cval
            with frame f-buyer.
          x-line-counter = x-line-counter + 2.
          if o-master then
          display stream dept bbuy
                   z-name
                   round ((buy-cur / 1),0) @ buy-cur 
                   round ((buy-cgs / 1),0) @ buy-cgs 
               /*    truncate ((buy-avg / 1),0) @ buy-avg    */
                   round ((buy-ncur / 1),0) @ buy-ncur 
                   round ((buy-ncgs / 1),0) @ buy-ncgs 
                   d-turn 
                   round ((buy-slo / 1),0) @ buy-slo 
                   slo-pct
                   round ((buy-obs / 1),0) @ buy-obs 
                   obs-pct 
                   buy-cln 
                   buy-cqty 
                   buy-cval
            with frame f-buyer.
        end.    
  
        assign buy-cur = 0 buy-cgs = 0 buy-avg = 0 buy-slo = 0
            buy-obs = 0 buy-cln = 0 buy-cqty = 0 buy-cval = 0
            buy-tln = 0 buy-tqty = 0 buy-tval = 0
            buy-ncur = 0 buy-ncgs = 0.
       if not last-of(xuyers.wuys-group) and o-rsummary <> "s" then do:
         dofirstheaders = true.
    end.  /* last of buyer */
    if last-of(xuyers.wuys-group) then do:
        assign 
        d-turn  = 0
        slo-pct = 0
        obs-pct = 0.
                

        if i-turnmo <> 0 then
          do:
          buyg-avg = buyg-avg.    /* / i-turnmo.             avg annual inv */
          buyg-ncur = buyg-ncur.
          end.
        if i-turnmo <> 0 then
          do:
          buyg-cgs   = (buyg-cgs / i-turnmo) * 12.         /* annualized cgs */
          buyg-ncgs  = (buyg-ncgs / i-turnmo) * 12.
          end.        
        if (buyg-avg + buyg-ncur) <> 0 then
          d-turn  = (buyg-cgs + buyg-ncgs) / ( buyg-avg + buyg-ncur).
           /* turnover */
        if buyg-cur <> 0 then
          slo-pct = (buyg-slo / buyg-cur) * 100.       /* slow moving % */
        if buyg-cur <> 0 then
          obs-pct = (buyg-obs / buyg-cur) * 100.       /* obsolete % */
        if buyg-tln <> 0 then
          buyg-cln = (buyg-cln / buyg-tln) * 100.        /* line fill rate */
        if buyg-tqty <> 0 then
          buyg-cqty = (buyg-cqty / buyg-tqty) * 100.    /* qty fill rate */
        if buyg-tval <> 0 then
          buyg-cval = (buyg-cval / buyg-tval) * 100.    /* value fill rate */
 
       
        if d-turn > 9999 then
           d-turn = 9999.99.
        else
        if d-turn < -9999 then
          d-turn = -9999.99.

        if slo-pct > 9999 then
           slo-pct = 9999.99.
        else
        if slo-pct < -9999 then
          slo-pct = -9999.99.

        if obs-pct > 9999 then
           obs-pct = 9999.99.
        else
        if obs-pct < -9999 then
          obs-pct = -9999.99.

        if buyg-cln > 9999 then
           buyg-cln = 9999.99.
        else
        if buyg-cln < -9999 then
          buyg-cln = -9999.99.

        if buyg-cqty > 9999 then
           buyg-cqty = 9999.99.
        else
        if buyg-cqty < -9999 then
          buyg-cqty = -9999.99.

        if buyg-cval > 9999 then
           buyg-cval = 9999.99.
        else
        if buyg-cval < -9999 then
          buyg-cval = -9999.99.
       
       bgrp = xuyers.wuys-group.
       z-name = "".
       if o-rsummary <> "x" and
        (buyg-cur ge 1 or
          buyg-cgs  ge 1 or
          buyg-avg ge 1  or
          buyg-ncur ge 1 or
          buyg-ncgs ge 1 or
          buyg-slo  ge 1 or
          buyg-obs  ge 1 or
          buyg-cur le -1 or
          buyg-cgs  le -1 or
          buyg-avg le -1  or
          buyg-ncur le -1 or
          buyg-ncgs le -1 or
          buyg-slo  le -1 or
          buyg-obs  le -1 or
          buyg-cln  <> 0 or
          buyg-tln  <> 0 or
          buyg-cqty <> 0 or
          buyg-tqty <> 0 or
          buyg-cval <> 0 or
          buyg-tval <> 0)
          then
          do:
          display 
                 d-option
                 bgrp
                 z-name
                 round ((buyg-cur / 1),0) @ buyg-cur 
                 round ((buyg-cgs / 1),0) @ buyg-cgs 
              /*   truncate ((buyg-avg / 1),0) @ buyg-avg   */
                 round ((buyg-ncur / 1),0) @ buyg-ncur 
                 round ((buyg-ncgs / 1),0) @ buyg-ncgs 
                 d-turn 
                 round ((buyg-slo / 1),0) @ buyg-slo 
                 slo-pct
                 round ((buyg-obs / 1),0) @ buyg-obs 
                 obs-pct 
                 buyg-cln 
                 buyg-cqty 
                 buyg-cval
            with frame f-buyerg.
         x-line-counter = x-line-counter + 2.
         if o-master then
         display stream dept
                 d-option
                 bgrp
                 z-name
                 round ((buyg-cur / 1),0) @ buyg-cur 
                 round ((buyg-cgs / 1),0) @ buyg-cgs 
              /*   truncate ((buyg-avg / 1),0) @ buyg-avg   */
                 round ((buyg-ncur / 1),0) @ buyg-ncur 
                 round ((buyg-ncgs / 1),0) @ buyg-ncgs 
                 d-turn 
                 round ((buyg-slo / 1),0) @ buyg-slo 
                 slo-pct
                 round ((buyg-obs / 1),0) @ buyg-obs 
                 obs-pct 
                 buyg-cln 
                 buyg-cqty 
                 buyg-cval
            with frame f-buyerg.
        end.      
        assign buyg-cur = 0 buyg-cgs = 0 buyg-avg = 0 buyg-slo = 0
            buyg-obs = 0 buyg-cln = 0 buyg-cqty = 0 buyg-cval = 0
            buyg-tln = 0 buyg-tqty = 0 buyg-tval = 0
            buyg-ncur = 0 buyg-ncgs = 0.
        if o-rsummary <> "x" then
        dofirstheaders = true.
    end.  /* last of buyer group */
    
  end.
end.

/* total line */
     assign 
        d-turn  = 0
        slo-pct = 0
        obs-pct = 0.
        

        if i-turnmo <> 0 then
          do:
          buyt-avg = buyt-avg.    /*  / i-turnmo.           avg annual inv */
          buyt-ncur = buyt-ncur.
          end.        
        if i-turnmo <> 0 then
          do:          
          buyt-cgs   = (buyt-cgs / i-turnmo) * 12.         /* annualized cgs */
          buyt-ncgs   = (buyt-ncgs / i-turnmo) * 12.
          end.        
        if (buyt-avg + buyt-ncur) <> 0 then
          d-turn  = (buyt-cgs + buyt-ncgs) / (buyt-avg + buyt-ncur).
           /* turnover */
        if buyt-cur <> 0 then
          slo-pct = (buyt-slo / buyt-cur) * 100.       /* slow moving % */
        if buyt-cur <> 0 then
          obs-pct = (buyt-obs / buyt-cur) * 100.       /* obsolete % */
        if buyt-tln <> 0 then
          buyt-cln = (buyt-cln / buyt-tln) * 100.        /* line fill rate */
        if buyt-tqty <> 0 then
          buyt-cqty = (buyt-cqty / buyt-tqty) * 100.    /* qty fill rate */
        if buyt-tval <> 0 then
          buyt-cval = (buyt-cval / buyt-tval) * 100.    /* value fill rate */
 
        if d-turn > 9999 then
           d-turn = 9999.99.
        else
        if d-turn < -9999 then
          d-turn = -9999.99.

        if slo-pct > 9999 then
           slo-pct = 9999.99.
        else
        if slo-pct < -9999 then
          slo-pct = -9999.99.

        if obs-pct > 9999 then
           obs-pct = 9999.99.
        else
        if obs-pct < -9999 then
          obs-pct = -9999.99.

        if buyt-cln > 9999 then
           buyt-cln = 9999.99.
        else
        if buyt-cln < -9999 then
          buyt-cln = -9999.99.

        if buyt-cqty > 9999 then
           buyt-cqty = 9999.99.
        else
        if buyt-cqty < -9999 then
          buyt-cqty = -9999.99.

        if buyt-cval > 9999 then
           buyt-cval = 9999.99.
        else
        if buyt-cval < -9999 then
          buyt-cval = -9999.99.
       
       if (round((buyt-cur  / 1),0) <> 0 or
          round((buyt-cgs / 1),0)  <> 0 or
          round((buyt-avg / 1),0)  <> 0 or
          round((buyt-ncur / 1),0) <> 0 or
          round((buyt-ncgs / 1),0) <> 0 or
          round((buyt-slo / 1),0)  <> 0 or
          round((buyt-obs / 1),0)  <> 0 or
          buyt-cln  <> 0 or
          buyt-tln  <> 0 or
          buyt-cqty <> 0 or
          buyt-tqty <> 0 or
          buyt-cval <> 0 or
          buyt-tval <> 0)
          then
          do:
 
        display round ((buyt-cur / 1),0) @ buyt-cur 
                round ((buyt-cgs / 1),0) @ buyt-cgs 
         /*       truncate ((buyt-avg / 1),0) @ buyt-avg  */
                round ((buyt-ncur / 1),0) @ buyt-ncur 
                round ((buyt-ncgs / 1),0) @ buyt-ncgs 
                d-turn 
                round ((buyt-slo / 1),0) @ buyt-slo 
                slo-pct
                round ((buyt-obs / 1),0) @ buyt-obs 
                obs-pct 
                buyt-cln 
                buyt-cqty 
                buyt-cval
            with frame f-buyerts.
        x-line-counter  = x-line-counter + 2.
        if o-master  then
        
        display stream  dept 
                round ((buyt-cur / 1),0) @ buyt-cur 
                round ((buyt-cgs / 1),0) @ buyt-cgs 
         /*       truncate ((buyt-avg / 1),0) @ buyt-avg  */
                round ((buyt-ncur / 1),0) @ buyt-ncur 
                round ((buyt-ncgs / 1),0) @ buyt-ncgs 
                d-turn 
                round ((buyt-slo / 1),0) @ buyt-slo 
                slo-pct
                round ((buyt-obs / 1),0) @ buyt-obs 
                obs-pct 
                buyt-cln 
                buyt-cqty 
                buyt-cval
            with frame f-buyerts.
         end.
        assign
          buyt-cur = 0
          buyt-ncgs = 0
          buyt-ncur = 0
          buyt-cgs = 0
          buyt-avg = 0
          buyt-slo = 0
          buyt-obs = 0
          buyt-cln = 0
          buyt-tln = 0
          buyt-cqty = 0
          buyt-tqty = 0
          buyt-cval = 0
          buyt-tval = 0.
 
        
        dofirstheaders = true.  
/* total line end */

   s-brklit = "PctAllc".
   assign
        buyg-cur = 0
        buyg-ncgs = 0
        buyg-ncur = 0
        buyg-cgs = 0
        buyg-avg = 0
        buyg-slo = 0
        buyg-obs = 0
        buyg-cln = 0
        buyg-tln = 0
        buyg-cqty = 0
        buyg-tqty = 0
        buyg-cval = 0
        buyg-tval =0.
    assign
        buyt-cur = 0
        buyt-ncgs = 0
        buyt-ncur = 0
        buyt-cgs = 0
        buyt-avg = 0
        buyt-slo = 0
        buyt-obs = 0
        buyt-cln = 0
        buyt-tln = 0
        buyt-cqty = 0
        buyt-tqty = 0
        buyt-cval = 0
        buyt-tval = 0.
    assign
        buy-cur = 0
        buy-cgs = 0
        buy-ncur = 0
        buy-ncgs = 0
        buy-avg = 0
        buy-slo = 0
        buy-obs = 0
        buy-cln = 0
        buy-tln = 0
        buy-cqty = 0
        buy-tqty = 0
        buy-cval = 0
        buy-tval = 0.

if o-sort <> "v" and o-sort <> "w" then
 do:              
 if opendept = true then
    output stream dept close.
 if o-master then
    fylename = "/usr/tmp/icxmrbuy".
 else
    fylename = "/usr/tmp/icxmrbuyx".

 output stream dept to value(fylename).
 os-command silent value ("chmod 666 " + fylename).

 opendept = true.
 put stream dept control compress-string.  
                           
 dobuyinhead = false.
 dofirstheaders = false.
 run headers.
 
 for each buyers break by buyers.buys-group
                       by buyers.buys-cur descending
                       by buyers.buys-buyer:
   assign
        buyg-cur = buyg-cur + buyers.buys-cur
        buyg-ncgs = buyg-ncgs + buyers.buys-ncgs
        buyg-ncur = buyg-ncur + buyers.buys-ncur
        buyg-cgs = buyg-cgs + buyers.buys-cgs
        buyg-avg = buyg-avg + buyers.buys-avg
        buyg-slo = buyg-slo + buyers.buys-slo
        buyg-obs = buyg-obs + buyers.buys-obs
        buyg-cln = buyg-cln + buyers.buys-cln
        buyg-tln = buyg-tln + buyers.buys-tln
        buyg-cqty = buyg-cqty + buyers.buys-cqty
        buyg-tqty = buyg-tqty + buyers.buys-tqty
        buyg-cval = buyg-cval + buyers.buys-cval
        buyg-tval = buyg-tval + buyers.buys-tval.
    assign
        buyt-cur = buyt-cur + buyers.buys-cur
        buyt-ncgs = buyt-ncgs + buyers.buys-ncgs
        buyt-ncur = buyt-ncur + buyers.buys-ncur
        buyt-cgs = buyt-cgs + buyers.buys-cgs
        buyt-avg = buyt-avg + buyers.buys-avg
        buyt-slo = buyt-slo + buyers.buys-slo
        buyt-obs = buyt-obs + buyers.buys-obs
        buyt-cln = buyt-cln + buyers.buys-cln
        buyt-tln = buyt-tln + buyers.buys-tln
        buyt-cqty = buyt-cqty + buyers.buys-cqty
        buyt-tqty = buyt-tqty + buyers.buys-tqty
        buyt-cval = buyt-cval + buyers.buys-cval
        buyt-tval = buyt-tval + buyers.buys-tval.
    assign
        buy-cur = buyers.buys-cur
        buy-cgs = buyers.buys-cgs
        buy-ncur = buyers.buys-ncur
        buy-ncgs = buyers.buys-ncgs
        buy-avg = buyers.buys-avg
        buy-slo = buyers.buys-slo
        buy-obs = buyers.buys-obs
        buy-cln = buyers.buys-cln
        buy-tln = buyers.buys-tln
        buy-cqty = buyers.buys-cqty
        buy-tqty = buyers.buys-tqty
        buy-cval = buyers.buys-cval
        buy-tval = buyers.buys-tval.

        assign 
        d-turn  = 0
        slo-pct = 0
        obs-pct = 0.
        

        if i-turnmo <> 0 then
          do:
          buy-avg = buy-avg.       /*  / i-turnmo.      avg annual inv */
          buy-ncur = buy-ncur.
          end.        
        if i-turnmo <> 0 then
          do:
          buy-cgs   = (buy-cgs / i-turnmo) * 12.         /* annualized cgs */
          buy-ncgs  = (buy-ncgs / i-turnmo) * 12.
          end.
        
        if (buy-avg + buy-ncur) <> 0 then
          d-turn  = (buy-cgs + buy-ncgs) / (buy-avg + buy-ncur).  /* turnover */
        if buy-cur <> 0 then
          slo-pct = (buy-slo / buy-cur) * 100.       /* slow moving % */
        if buy-cur <> 0 then
          obs-pct = (buy-obs / buy-cur) * 100.       /* obsolete % */
        if buy-tln <> 0 then
          buy-cln = (buy-cln / buy-tln) * 100.        /* line fill rate */
        if buy-tqty <> 0 then
          buy-cqty = (buy-cqty / buy-tqty) * 100.    /* qty fill rate */
        if buy-tval <> 0 then
          buy-cval = (buy-cval / buy-tval) * 100.    /* value fill rate */
 
      
        if d-turn > 9999 then
           d-turn = 9999.99.
        else
        if d-turn < -9999 then
          d-turn = -9999.99.

        if slo-pct > 9999 then
           slo-pct = 9999.99.
        else
        if slo-pct < -9999 then
          slo-pct = -9999.99.

        if obs-pct > 9999 then
           obs-pct = 9999.99.
        else
        if obs-pct < -9999 then
          obs-pct = -9999.99.

        if buy-cln > 9999 then
           buy-cln = 9999.99.
        else
        if buy-cln < -9999 then
          buy-cln = -9999.99.

        if buy-cqty > 9999 then
           buy-cqty = 9999.99.
        else
        if buy-cqty < -9999 then
          buy-cqty = -9999.99.
                       
        if buy-cval > 9999 then
           buy-cval = 9999.99.
        else
        if buy-cval < -9999 then
          buy-cval = -9999.99.
   if x-line-counter > 42 or dofirstheaders then
     do:
     dofirstheaders = false.
     run headers.   
     end.
        
    
   if    (round((buy-cur / 1),0)  <> 0 or
          round((buy-cgs / 1),0)   <> 0 or
          round((buy-avg / 1),0)   <> 0 or
          round((buy-ncur / 1),0)  <> 0 or
          round((buy-ncgs / 1),0)  <> 0 or
          round((buy-slo  / 1),0)  <> 0 or
          round((buy-obs / 1),0)   <> 0 or
          buy-cln  <> 0 or
          buy-tln  <> 0 or
          buy-cqty <> 0 or
          buy-tqty <> 0 or
          buy-cval <> 0 or
          buy-tval <> 0)
          then
          do:
    display buyers.buys-buyer
            buyers.buys-name
           round ((buy-cur / 1),0) @ buy-cur 
           round ((buy-cgs / 1),0) @ buy-cgs 
        /*   truncate ((buy-avg / 1),0) @ buy-avg       */
           round ((buy-ncur / 1),0) @ buy-ncur 
           round ((buy-ncgs / 1),0) @ buy-ncgs
           d-turn 
           round ((buy-slo / 1),0) @ buy-slo 
           slo-pct
           round ((buy-obs / 1),0) @ buy-obs 
           obs-pct 
           buy-cln 
           buy-cqty 
           buy-cval
           with frame f-buyers down.
        down with frame f-buyers.
        x-line-counter = x-line-counter + 1.
        if o-master then
        do:   
         display stream dept buyers.buys-buyer
            buyers.buys-name
           round ((buy-cur / 1),0) @ buy-cur 
           round ((buy-cgs / 1),0) @ buy-cgs 
        /*   truncate ((buy-avg / 1),0) @ buy-avg       */
           round ((buy-ncur / 1),0) @ buy-ncur 
           round ((buy-ncgs / 1),0) @ buy-ncgs
           d-turn 
           round ((buy-slo / 1),0) @ buy-slo 
           slo-pct
           round ((buy-obs / 1),0) @ buy-obs 
           obs-pct 
           buy-cln 
           buy-cqty 
           buy-cval
           with frame f-buyers down.
        down stream dept with frame f-buyers.
        end.
      end.
      if last-of(buyers.buys-group) then do:
        assign 
        d-turn  = 0
        slo-pct = 0
        obs-pct = 0.
        

        if i-turnmo <> 0 then
          do:
          buyg-avg = buyg-avg.    /*  / i-turnmo.           avg annual inv */
          buyg-ncur = buyg-ncur.
          end.        
        if i-turnmo <> 0 then
          do:          
          buyg-cgs   = (buyg-cgs / i-turnmo) * 12.         /* annualized cgs */
          buyg-ncgs   = (buyg-ncgs / i-turnmo) * 12.
          end.        
        if (buyg-avg + buyg-ncur) <> 0 then
          d-turn  = (buyg-cgs + buyg-ncgs) / (buyg-avg + buyg-ncur).
           /* turnover */
        if buyg-cur <> 0 then
          slo-pct = (buyg-slo / buyg-cur) * 100.       /* slow moving % */
        if buyg-cur <> 0 then
          obs-pct = (buyg-obs / buyg-cur) * 100.       /* obsolete % */
        if buyg-tln <> 0 then
          buyg-cln = (buyg-cln / buyg-tln) * 100.        /* line fill rate */
        if buyg-tqty <> 0 then
          buyg-cqty = (buyg-cqty / buyg-tqty) * 100.    /* qty fill rate */
        if buyg-tval <> 0 then
          buyg-cval = (buyg-cval / buyg-tval) * 100.    /* value fill rate */
 
        if d-turn > 9999 then
           d-turn = 9999.99.
        else
        if d-turn < -9999 then
          d-turn = -9999.99.

        if slo-pct > 9999 then
           slo-pct = 9999.99.
        else
        if slo-pct < -9999 then
          slo-pct = -9999.99.

        if obs-pct > 9999 then
           obs-pct = 9999.99.
        else
        if obs-pct < -9999 then
          obs-pct = -9999.99.

        if buyg-cln > 9999 then
           buyg-cln = 9999.99.
        else
        if buyg-cln < -9999 then
          buyg-cln = -9999.99.

        if buyg-cqty > 9999 then
           buyg-cqty = 9999.99.
        else
        if buyg-cqty < -9999 then
          buyg-cqty = -9999.99.

        if buyg-cval > 9999 then
           buyg-cval = 9999.99.
        else
        if buyg-cval < -9999 then
          buyg-cval = -9999.99.
       
       if (round((buyg-cur / 1),0)  <> 0 or
          round((buyg-cgs / 1),0)  <> 0 or
          round((buyg-avg / 1),0)  <> 0 or
          round((buyg-ncur / 1),0) <> 0 or
          round((buyg-ncgs / 1),0) <> 0 or
          round((buyg-slo / 1),0)  <> 0 or
          round((buyg-obs / 1),0)  <> 0 or
          buyg-cln  <> 0 or
          buyg-tln  <> 0 or
          buyg-cqty <> 0 or
          buyg-tqty <> 0 or
          buyg-cval <> 0 or
          buyg-tval <> 0)
          then
          do:
 
        display d-option
                buyers.buys-group
                round ((buyg-cur / 1),0) @ buyg-cur 
                round ((buyg-cgs / 1),0) @ buyg-cgs 
       /*         truncate ((buyg-avg / 1),0) @ buyg-avg   */
                round ((buyg-ncur / 1),0) @ buyg-ncur 
                round ((buyg-ncgs / 1),0) @ buyg-ncgs 
                d-turn 
                round ((buyg-slo / 1),0) @ buyg-slo 
                slo-pct
                round ((buyg-obs / 1),0) @ buyg-obs 
                obs-pct 
                buyg-cln 
                buyg-cqty 
                buyg-cval
            with frame f-buyergs.
         x-line-counter = x-line-counter + 1.
         if o-master then
         display stream dept d-option
                buyers.buys-group
                round ((buyg-cur / 1),0) @ buyg-cur 
                round ((buyg-cgs / 1),0) @ buyg-cgs 
       /*         truncate ((buyg-avg / 1),0) @ buyg-avg   */
                round ((buyg-ncur / 1),0) @ buyg-ncur 
                round ((buyg-ncgs / 1),0) @ buyg-ncgs 
                d-turn 
                round ((buyg-slo / 1),0) @ buyg-slo 
                slo-pct
                round ((buyg-obs / 1),0) @ buyg-obs 
                obs-pct 
                buyg-cln 
                buyg-cqty 
                buyg-cval
            with frame f-buyergs.
        end.
        assign
          buyg-cur = 0
          buyg-ncgs = 0
          buyg-ncur = 0
          buyg-cgs = 0
          buyg-avg = 0
          buyg-slo = 0
          buyg-obs = 0
          buyg-cln = 0
          buyg-tln = 0
          buyg-cqty = 0
          buyg-tqty = 0
          buyg-cval = 0
          buyg-tval = 0.
 
        
        dofirstheaders = true.    
     end.  /* last of buyer */
end.
     assign 
        d-turn  = 0
        slo-pct = 0
        obs-pct = 0.
        

        if i-turnmo <> 0 then
          do:
          buyt-avg = buyt-avg.    /*  / i-turnmo.           avg annual inv */
          buyt-ncur = buyt-ncur.
          end.        
        if i-turnmo <> 0 then
          do:          
          buyt-cgs   = (buyt-cgs / i-turnmo) * 12.         /* annualized cgs */
          buyt-ncgs   = (buyt-ncgs / i-turnmo) * 12.
          end.        
        if (buyt-avg + buyt-ncur) <> 0 then
          d-turn  = (buyt-cgs + buyt-ncgs) / (buyt-avg + buyt-ncur).
           /* turnover */
        if buyt-cur <> 0 then
          slo-pct = (buyt-slo / buyt-cur) * 100.       /* slow moving % */
        if buyt-cur <> 0 then
          obs-pct = (buyt-obs / buyt-cur) * 100.       /* obsolete % */
        if buyt-tln <> 0 then
          buyt-cln = (buyt-cln / buyt-tln) * 100.        /* line fill rate */
        if buyt-tqty <> 0 then
          buyt-cqty = (buyt-cqty / buyt-tqty) * 100.    /* qty fill rate */
        if buyt-tval <> 0 then
          buyt-cval = (buyt-cval / buyt-tval) * 100.    /* value fill rate */
 
        if d-turn > 9999 then
           d-turn = 9999.99.
        else
        if d-turn < -9999 then
          d-turn = -9999.99.

        if slo-pct > 9999 then
           slo-pct = 9999.99.
        else
        if slo-pct < -9999 then
          slo-pct = -9999.99.

        if obs-pct > 9999 then
           obs-pct = 9999.99.
        else
        if obs-pct < -9999 then
          obs-pct = -9999.99.

        if buyt-cln > 9999 then
           buyt-cln = 9999.99.
        else
        if buyt-cln < -9999 then
          buyt-cln = -9999.99.

        if buyt-cqty > 9999 then
           buyt-cqty = 9999.99.
        else
        if buyt-cqty < -9999 then
          buyt-cqty = -9999.99.

        if buyt-cval > 9999 then
           buyt-cval = 9999.99.
        else
        if buyt-cval < -9999 then
          buyt-cval = -9999.99.
       
       if (round((buyt-cur  / 1),0) <> 0 or
          round((buyt-cgs / 1),0)  <> 0 or
          round((buyt-avg / 1),0)  <> 0 or
          round((buyt-ncur / 1),0) <> 0 or
          round((buyt-ncgs / 1),0) <> 0 or
          round((buyt-slo / 1),0)  <> 0 or
          round((buyt-obs / 1),0)  <> 0 or
          buyt-cln  <> 0 or
          buyt-tln  <> 0 or
          buyt-cqty <> 0 or
          buyt-tqty <> 0 or
          buyt-cval <> 0 or
          buyt-tval <> 0)
          then
          do:
 
        display round ((buyt-cur / 1),0) @ buyt-cur 
                round ((buyt-cgs / 1),0) @ buyt-cgs 
         /*       truncate ((buyt-avg / 1),0) @ buyt-avg  */
                round ((buyt-ncur / 1),0) @ buyt-ncur 
                round ((buyt-ncgs / 1),0) @ buyt-ncgs 
                d-turn 
                round ((buyt-slo / 1),0) @ buyt-slo 
                slo-pct
                round ((buyt-obs / 1),0) @ buyt-obs 
                obs-pct 
                buyt-cln 
                buyt-cqty 
                buyt-cval
            with frame f-buyerts.
        x-line-counter  = x-line-counter + 2.
        if o-master  then
        
        display stream  dept 
                round ((buyt-cur / 1),0) @ buyt-cur 
                round ((buyt-cgs / 1),0) @ buyt-cgs 
         /*       truncate ((buyt-avg / 1),0) @ buyt-avg  */
                round ((buyt-ncur / 1),0) @ buyt-ncur 
                round ((buyt-ncgs / 1),0) @ buyt-ncgs 
                d-turn 
                round ((buyt-slo / 1),0) @ buyt-slo 
                slo-pct
                round ((buyt-obs / 1),0) @ buyt-obs 
                obs-pct 
                buyt-cln 
                buyt-cqty 
                buyt-cval
            with frame f-buyerts.
         end.
        assign
          buyt-cur = 0
          buyt-ncgs = 0
          buyt-ncur = 0
          buyt-cgs = 0
          buyt-avg = 0
          buyt-slo = 0
          buyt-obs = 0
          buyt-cln = 0
          buyt-tln = 0
          buyt-cqty = 0
          buyt-tqty = 0
          buyt-cval = 0
          buyt-tval = 0.
 
        
        dofirstheaders = true.    
  end.
end. 