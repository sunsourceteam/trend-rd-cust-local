/* SX 55
   d-wtei.i 1.1 01/03/98 */
/* d-wtei.i 1.2 6/30/93 */
/* d-poei.i */      
/* dkt 10/18/2007 added 2 hooks */
/*tb 06/29/93 mms; Added scoping logic for wteh and wtel as last wteh and wtel
  record touched were being held through bofill as shared lock */

do for wtel:
    s-assign = if poei.qtyassign ne 0 then "o" else "".
    if poei.nonstockty ne "n" then {w-icsp.i poei.shipprod no-lock}
    else {w-wtel.i poei.pono poei.posuf poei.lineno no-lock}

    /* dkt */
    /* {wtei.z99 &user_d_setbins = "*"} */
    {d-wtei.z99 &before_display = "*"}
    
    display
      poei.lineno
      poei.nonstockty
      poei.shipprod
      poei.notesfl
      icsp.descrip[1] when poei.nonstockty ne "n"
      wtel.proddesc when poei.nonstock = "n" @ icsp.descrip[1]
      poei.qtyrcv
      poei.unit
      poei.serlottype
      poei.speccostty
      s-assign
      /* {wtei.z99 &user_d_displaybins = "*"} */
    with frame f-wteil.
    {1} with frame f-wteil.
end.
