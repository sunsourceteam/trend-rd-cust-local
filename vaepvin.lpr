/* vaepvin.lpr 1.1 01/03/98 */
/* vaepvin.lpr 1.1 08/21/96 */
/*h****************************************************************************
    INCLUDE      : vaepvin.lpr
    DESCRIPTION  : Print Inventory Compnts & Inventory IN on Internal Processes
    USED ONCE?   : yes
    AUTHOR       : smn
    DATE WRITTEN : 08/21/96
    CHANGES MADE :
        08/21/96 smn; TB# 21254 (11A) Develop Value Add Module
        12/03/96 bao; TB# 21254 (21B) Add underline to separate 'in'& 'ii'
            headers from 'in'&'ii' data; add 'N'on-stock field; Make changes so
            this include can be used for both inventory components and
            inventory in vaes records
******************************************************************************/
/*

    Variables that must be loaded for this include are:

    Required parameters used in this include are:
     &stitle    - Used to display appropriate section header
     &qthead    - The Returned Quantity heading
     &xdash1    - Extra dashes required to underscore &qthead
     &xdash2    - Extra dashes to mark a space for write-in of returned qty

    Optional parameters used in this include are:
*/

if v-vafstinvfl = yes then do:
    assign
        v-vafstinvfl   = no
        s-vadata       = {&stitle}.
/*  
    if p-detail then
      do:
      display s-vadata with frame f-vadata.
      down with frame f-vadata.

      assign
          s-vadata                  = ""
          substring(s-vadata,1,35)  = "-----------------------------------"
          substring(s-vadata,36,35) = "-----------------------------------"
          substring(s-vadata,71,56) = 
               "--------------------------------------------------------"
          substring(s-vadata,127,1) = {&xdash1}. 
      display s-vadata with frame f-vadata.
      down with frame f-vadata.
      end.
*/
end. /* if v-vafstinvfl = yes */

/******used only for vaesl********************************************
assign va-rebamt = 0.
if b-vaes.sctntype  = "in" or b-vaes.sctntype  = "ii" then 
do:
find first pder use-index k-pder where 
           pder.cono     = g-cono          and 
           pder.orderno  = oeeh.orderno    and 
           pder.ordersuf = oeeh.ordersuf   and  
           pder.lineno   = vaelo.linealtno and 
           pder.shipprod = vaesl.shipprod
           no-lock no-error.
   if avail pder then do:       
    assign va-rebamt = pder.rebateamt.
   end.    
   else 
    assign va-rebamt = 0.
end.
       
/*d Print the notes for the section */
if v-seqhld ne b-vaes.seqno and b-vaes.sctntype = "in" then do: 
  if avail b-vaes then 
   if b-vaes.notesfl = "" then do:
    assign v-seqhld  = b-vaes.seqno
           s-comdata = "Section " + b-vaes.sctntype +
                       " - " +  "Seq# " + string(vaesl.seqno,"zz9"). 
/*
      if p-detail then
        do:
        display s-comdata no-label with frame f-com3.
        down with frame f-com3.
        assign s-comdata = "--------------------------------".
        display s-comdata no-label with frame f-com3.
        down with frame f-com3.
        end.
*/
     end.
    if b-vaes.notesfl ne "" then do: 
     assign v-seqhld  = b-vaes.seqno
            s-comdata = "Section " + b-vaes.sctntype +
                        " - " +  "Seq# " + string(vaesl.seqno,"zz9") +
                        " Note(s): ".
     if p-detail then do: 
      display s-comdata no-label with frame f-com3.
      down with frame f-com3.
      assign s-comdata = "--------------------------------".
      display s-comdata no-label with frame f-com3.
      down with frame f-com3.
       {vaepi1nt.lpr &notestype    = ""fl""
                     &primarykey   = "string(b-vaes.vano) + '-' +
                                      string(b-vaes.vasuf)"
                     &secondarykey = "string(b-vaes.seqno)"}
     end.
    end.  /* if b-vaes.notesfl ne "" for section */
end.
*******************************************************************/

 assign qa-netamt = 0
        qa-cost   = 0
        va-rebamt = 0. 

if vaspsl.nonstockty ne "n" then
   {w-icsp.i vaspsl.compprod no-lock}

if vaspsl.prodcat ne "" then do:                                                 find notes use-index k-notes where                              
      notes.cono         = g-cono and                            
      notes.notestype    = "zz" and                              
      notes.primarykey   = "standard cost markup" and            
      notes.secondarykey = vaspsl.prodcat and                         
      notes.pageno       = 1                                     
      no-lock no-error.                                             
end.
if not avail notes then do: 
 find notes use-index k-notes where                          
      notes.cono         = g-cono and                        
      notes.notestype    = "zz" and                          
      notes.primarykey   = "standard cost markup" and        
      notes.secondarykey = "" and                            
      notes.pageno       = 1                                 
      no-lock no-error.                                         
end.

if p-margpct > 0 and vasps.sctntype ne "ex" then do: 
   qa-netamt = 
     ((((vaspsl.prodcost * 1.04) - va-rebamt) * vaspsl.qtyneeded)
         / p-margpct) * 100. 
   qa-cost   = (((vaspsl.prodcost * 1.04) - va-rebamt) / p-margpct) * 100.
end.
else 
if vasps.sctntype ne "ex" then do: 
  assign qa-netamt = (((vaspsl.prodcost * 1.04) - va-rebamt) *     
                        vaspsl.qtyneeded)
         qa-cost   =  ((vaspsl.prodcost  * 1.04) - va-rebamt). 
end.
else do: 
 assign qa-netamt = ((vaspsl.prodcost * 1.04) * vaspsl.qtyneeded) 
        qa-cost   =  (vaspsl.prodcost * 1.04). 
end.

assign qa-netamt = truncate(qa-netamt,2). 

assign s-descrip = "". 
if qa-netamt < 0 then 
   s-sign = "-".
else    
   s-sign = "".

if vaspsl.compprod = "margin improvement" then do:
 def var m-gain as dec no-undo. 
 def buffer g-vaspsl for vaspsl. 
 assign m-gain = 0. 
 for each g-vaspsl where 
          g-vaspsl.cono = g-cono              and 
          g-vaspsl.shipprod = vaspsl.shipprod and 
          g-vaspsl.nonstockty ne "l" 
          no-lock: 
     assign m-gain = m-gain + (g-vaspsl.xxde1 * 1.04).     
 end.
 assign m-gain        = truncate(((m-gain / p-margpct) * 100),2)
        qa-misctotamt =  m-gain
        qa-miscamt    =  m-gain.
end.
else 
if substring(vaspsl.user5,25,1) = "n" then do:  
 assign qa-miscamt    = qa-miscamt    + (qa-cost * 1.04)
        qa-misctotamt = qa-misctotamt + (qa-netamt * 1.04).
end. 
else
if substring(vaspsl.user5,25,1) ne "n" then do:  
 assign s-descrip  = "" 
        qa-netamt  = if qa-netamt > 9999999 then 
                      9999999
                     else 
                      qa-netamt 
        qa-cost    = if qa-cost > 9999999 then 
                      9999999
                     else 
                      qa-cost
        s-netamt   = if p-prtprc then 
                       string(qa-netamt,"zzzzzz9.99") 
                     else "" 
        s-lineno   = string(vaspsl.lineno,"zz9")
        s-qtyord   = string((vaspsl.qtyneeded),"zzzzzz9")  
        v-unit     = vaspsl.unit
        s-prcunit  = vaspsl.unit
        s-price    = if p-prtprc then                 
                      string(qa-cost,"zzzzzz9.99") 
                     else ""                          
        s-lnshipdt = if string(vaspsl.user8,"99/99/99") ne ? then 
                      string(vaspsl.user8,"99/99/99") 
                     else 
                      ""
        v-shipprod = if p-desconly and vaspsl.nonstockty = "n" then 
                      vaspsl.proddesc + vaspsl.proddesc2                      
                     else 
                     if p-desconly and avail icsp then 
                      icsp.descrip[1] + icsp.descrip[2]
                     else  
                      vaspsl.compprod
        s-descrip  = if p-desconly then 
                      " "
                     else    
                     if vaspsl.nonstockty = "n" and 
                        vaspsl.proddesc ne vaspsl.proddesc2 then 
                      vaspsl.proddesc + vaspsl.proddesc2                      
                     else
                     if vaspsl.nonstockty = "n" and 
                        vaspsl.proddesc = vaspsl.proddesc2 then 
                        vaspsl.proddesc 
                     else                         
                     if avail icsp then 
                      icsp.descrip[1] + icsp.descrip[2]
                     else "".

  if vasps.sctntype = "ii" then 
    assign s-netamt = if not p-prtprc then
                       " "
                      else     
                       string(dec(s-netamt) * -1,"zzzzzz9.99-"). 
  if p-detail and 
     vaspsl.compprod ne "Margin Improvement" then do: 
   display  s-netamt "   " @ s-lineno  s-qtyord v-shipprod  
            v-unit   s-prcunit s-price s-lnshipdt s-descrip  
   with frame f-vaesl.
   down with frame f-vaesl.
  end.
end.

