/*h*****************************************************************************
  INCLUDE      : p-zsdiob.i
  DESCRIPTION  : Calculate the Display Values
  USED ONCE?   : zsdiob.p)
  AUTHOR       : tah
  DATE WRITTEN : 04/17/06
  CHANGES MADE :
*******************************************************************************/

/* Get Order Disposition Description */
/*
{p-oedisp.i}
*/
assign
    s-shipviaty  = ""
    s-cancelcd   = ""
    s-termstype  = ""
    s-finthru    = ""
    s-cod        = if oeehb.codfl then "C.O.D." else ""
    s-addon      = ""
    s-addonamt   = 0
    s-addondesc  = ""
    s-otheraddon = 0
    s-salestax   = 0
/*    s-payamt     = oeehb.tendamt  */
    v-addontemp  = v-totlineamt - oeehb.specdiscamt - oeehb.wodiscamt
    s-stagecd    = if oeehb.stagecd <> 9 then v-stage[1]
                      else v-stage[10].
                                  /* v-stage[oeehb.stagecd + 1]. */
                   
assign s-cancelcd = substr(oeehb.user3,53,4).
if oeehb.shipviaty ne "" then do:
    {sasta.gfi S oeehb.shipviaty no}
    s-shipviaty = if avail sasta then sasta.descrip
                  else v-invalid.
end.

if oeehb.termstype ne "" then do:
    {sasta.gfi T oeehb.termstype no}
    s-termstype = if avail sasta then sasta.descrip
                  else v-invalid.
end.

do i = 1 to 4:
    if oeehb.addonno[i] ne 0 then do:
        {w-sastn.i A oeehb.addonno[i] no-lock}
        assign
            s-addon[i]    = if avail sastn then sastn.descrip
                            else v-invalid
            s-addonamt[i] = 0.  /* oeehb.addonnet[i]. */
    end.

    /*tb 15542 04/29/94 dww; Display of addonamt[1] needs description */
    s-salestax = s-salestax + oeehb.taxamt[i].

end.
/*tb 15195 03/16/95 mtt; RM total shows as positive vs negative */
assign
     s-salestax   = if oeehb.transtype = "rm":u then s-salestax * -1 else
                    s-salestax
/*
     s-totlineord = if oeehb.transtype = "rm":u then oeehb.totlineord * -1 else
                    oeehb.totlineord
*/    
     s-totlineamt = if oeehb.transtype = "rm":U then oeehb.totlineamt * -1 else
                    oeehb.totlineamt
     s-totinvamt  = if oeehb.transtype = "rm":u then oeehb.totinvamt * -1 else
                    oeehb.totinvamt.
     s-payamt     = if oeehb.transtype = "rm":u then s-payamt * -1 else
                    s-payamt.

/*tb 15542 04/29/94 dww; Display of addonamt[1].  If there is no
    description for the charge, put in some kind of text and allow the amount to
    print. Otherwise the DATC, Core Charge, and/or Restocking Amount will not
    appear on the screen. */
/* Find a place to put the DATC Cost */
if oeehb.totdatccost ne 0 then do:
    /* Set flag to test if the Total DATC Cost could be put into an empty
        bucket */
    confirm = no.

    datcloop:
    do i = 1 to 4.
        /* If bucket isn't empty, try next bucket */
        if s-addonamt[i] ne 0 then next datcloop.

        /* Put it into the empty bucket, set the flag indicating the bucket has
            been filled, and exit the loop */
        assign
            s-addonamt[i] = oeehb.totdatccost
            s-addon[i]    = v-icdatclabel
            confirm       = yes.

        leave datcloop.
    end. /* do i = 1 to 4 */

    /* If the amount couldn't be put in any one bucket, then force it into
        a bucket */
    if confirm = no then
        assign
            s-addondesc  = v-icdatclabel
            s-otheraddon = s-otheraddon + oeehb.totdatccost.

end. /* if oeeh.totdatccost ne 0 */

if oeehb.totcorechg ne 0 then do:
    /* Set flag to test if the Total Core Charge could be put into an empty
        bucket */
    confirm = no.

    coreloop:
    do i = 1 to 4.
        /* If bucket isn't empty, try next bucket */
        if s-addonamt[i] ne 0 then next coreloop.

        /* Put it into the empty bucket, set the flag indicating the bucket has
            been filled, and exit the loop */
        assign
            s-addonamt[i] = oeehb.totcorechg
            s-addon[i]    = "Core"
            confirm       = yes.

        leave coreloop.
    end. /* do i = 1 to 4 */

    /* If the amount couldn't be put in any one bucket, then force it into
        a bucket */
    if confirm = no then
        assign
            s-addondesc  = if s-otheraddon = 0 then "Core"
                           else s-addondesc + ",Core"
            s-otheraddon = s-otheraddon + oeehb.totcorechg.

end. /* if oeeh.totcorechg ne 0 */

if oeehb.totrestkamt ne 0 then do:
    /* Set flag to test if the Total Restock Amount could be put into an empty
        bucket */
    confirm = no.

    restkloop:
    do i = 1 to 4.
        /* If bucket isn't empty, try next bucket */
        if s-addonamt[i] ne 0 then next restkloop.

        /* Put it into the empty bucket, set the flag indicating the bucket has
            been filled, and exit the loop */
        assign
            s-addonamt[i] = oeehb.totrestkamt
            s-addon[i]    = "Restock"
            confirm       = yes.

        leave restkloop.
    end. /* do i = 1 to 4 */

    /* If the amount couldn't be put in any one bucket, then force it into
        a bucket */
    if confirm = no then
        assign
            s-addondesc  = if s-otheraddon = 0 then "Restock"
                           else s-addondesc + ",Rstk"
            s-otheraddon = s-otheraddon + oeehb.totrestkamt.

end. /* if oeeh.totrestkamt ne 0 */
/*
if oeehb.fpcustno ne {c-empty.i} then do for b-arsc:
    {w-arsc.i oeehb.fpcustno no-lock b-}

    s-finthru = "Financed Thru " +
                 (if avail b-arsc then b-arsc.name else v-invalid) +
                " (" + string(oeehb.fpcustno) + ")".
end. /* if oeehb.fpcustno ne ... */
*/



