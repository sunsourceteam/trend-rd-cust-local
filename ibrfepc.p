
/* icepec.p 1.6 5/3/93 */
/*h****************************************************************************
  PROCEDURE      : icepec
  DESCRIPTION    : Add to Count List From ICEPC
  AUTHOR         : enp
  DATE WRITTEN   :
  CHANGES MADE   :
    03/18/96 mwb; TB# 19381 Added New Edit to see if the Product Exists on
        Another run.  Was Causing an Out Of Balance Condition for GL.
    04/16/96 tdd; TB# 19660 Added lot item does not adjust ICSEL
    03/02/99 sbr; TB# 25908 Expected quantity field length
    10/06/00 kjb; TB# t5317 Enhance count reconciliation capability; added
        icepp.gip to define internal procedure and added definitions for
        v-cost, v-icglcost and v-icincaddgl.
    03/23/01 kjb; TB# e5054 Correctly apply user hook
    02/25/02 bpa; TB# e9553 Don't allow user to add new ticket number for
        product that already exists on run
    02/26/02 kjb; TB# e10132 Can enter a duplicate primary bin; added
            v-pribinfl (rolled by bpa)
    11/11/03 ns;  TB# e17893 override ao serial setting at the icsw level
    03/02/04 kjb; TB# t8355 Include unavailable stock on the physical count
*******************************************************************************/

  {g-ibrfepe.i}    

/*
def output param v-confirm      as l                    no-undo.
def output param v-ret-ticketno like icset.ticketno     no-undo.
*/

/*tb 25908 03/02/99 sbr; Added "format "zzzzzzzz9.99-"" to s-qtyexp. */
def var s-qtyexp       like icsep.qtyexp   format "zzzzzzzz9.99-"   no-undo.
def var s-ncount       like icsep.qtyexp   format "zzzzzzzz9.99-"   no-undo.
def var s-unavailfl    as l                                         no-undo.
def var v-pcreate      as l                                         no-undo.
def var s-descrip      as c                format "x(22)"           no-undo.
def var v-cost         as dec              format "zzzzzz9.99-"     no-undo.
def var v-icglcost     like sasc.icglcost                           no-undo.
def var v-icincaddgl   like sasc.icincaddgl                         no-undo.
def var v-pribinfl     as l                                         no-undo.
{icepp.gip}

form
    s-ticketno      at 1 label "Ticket/Seq #"
        help "'0' to Assign Next Available"
        skip
    "Prod:"         at 1 
    skip
    s-prod          at 1 no-label  {f-help.i}
        validate({v-icsw.i s-prod g-whse "/*"},
        "Prod/Whse Not Set Up")
        skip
    icsw.wmfl          at 1 format "WHSE MGR/ "    no-label
    skip
    s-descrip          at 1 no-label
    skip
    s-binloc           at 1 label "Location"   {f-help.i}
    skip
    s-qtyexp           at 1 label "Expected"
        help "Enter Quantity to Compare Against Counted (Usually 0)"
        skip
    s-unit             at 1      no-label
    skip
    s-unavailfl        at 1  label "Unavailable"
        help "Is this Unavailable Inventory"
    s-qtycnt           at 1 label "Count"
    skip    
with frame f-1 width 26 title " Add Product " side-labels overlay.

form
    confirm     colon 12 label "Create Bin"
with frame f-c overlay centered side-labels title " New Bin Record ".

/*o Initialize variables */
/*tb t5317 10/06/00 kjb; Load costing variables from SASC */
do for sasc:

    {sasc.gfi &lock = "no"}

    assign
        s-prod       = ""
        s-binloc     = ""
        s-qtyexp     = 0
        s-qtycnt     = 0   /* added this */
/*        v-confirm    = false    */
        v-pcreate    = true
        v-icglcost   = if avail sasc then sasc.icglcost   else "a":u
        v-icincaddgl = if avail sasc then sasc.icincaddgl else no.

end.

main:
do for icset, icsep, wmsbp, icsw
while true transaction on endkey undo, leave main:

    /*o Allow entry of the ticket number */
    do while s-ticketno = 0 or can-find(icset where
        icset.cono      = g-cono        and
        icset.whse      = g-whse        and
        icset.runno     = s-runno       and
        icset.ticketno  = s-ticketno):

        update s-ticketno with frame f-1.

        if s-ticketno = 0 then do:

            find last icset use-index k-icset where
                icset.cono      = g-cono        and
                icset.whse      = g-whse        and
                icset.runno     = s-runno
            no-lock no-error.

            s-ticketno = if avail icset then icset.ticketno + 1 else 1.

            display s-ticketno with frame f-1.
        end.

        /* 6051  Ticket/Seq# Already Exists, Enter New Number or '0' for
            Next Available */
        else run err.p(6051).
    end.

    display s-ticketno with frame f-1.
    put screen row 13 column 1 color messages "F10-Prod Lookup           ". 
    
  /*   v-ret-ticketno = s-ticketno.      */

    /*o Allow entry of the product/bin counted */
    do on endkey undo main, leave main:

    update s-prod s-binloc with frame f-1 editing:
        readkey.
    
     if {k-func10.i} and frame-field = "s-prod" then do:
      put screen row 13 column 1 color messages "                            ". 
        assign s-prod = input s-prod. 
        run zsdiprodbwsrf.p(input-output s-prod, input g-whse).
        display s-prod s-binloc
        with frame f-1. 
        next-prompt s-prod.   
        next.
     end.   
          
         if {k-after.i} then do:

            /*d Validate the product */
            if frame-field = "s-prod":u then do:

                {w-icsw.i "input s-prod" g-whse exclusive-lock}
                {w-icsp.i "input s-prod" no-lock}

                if not avail icsw or not avail icsp then do:

                    display v-invalid @ s-descrip with frame f-1.

                    /* 4602  Product/Warehouse Not Set Up in Warehouse
                        Products - ICSW */
                    run err.p(4602).
                    next-prompt s-prod with frame f-1.
                    next.
                end.

                else do:
                    assign
                        s-unit    = if icsp.unitcnt ne "" then icsp.unitcnt
                                    else icsp.unitstock
                        s-descrip = icsp.descrip[1] + " " + icsp.descrip[2]
                        s-binloc  = if icsw.wmfl = false
                                    then icsw.binloc1 else s-binloc.

                    display
                        s-descrip
                        s-binloc when s-binloc ne ""
                        icsw.wmfl
                    with frame f-1.
                end.

            end. /* field = s-prod */

            /*d Validate the bin location */
            else if frame-field = "s-binloc":u then do:

                /*d* Duplicate tickets are not allowed for the bin */
                if icsw.wmfl and can-find(first icset where
                    icset.cono      = g-cono        and
                    icset.whse      = g-whse        and
                    icset.runno     = s-runno       and
                    icset.prod      = input s-prod  and
                    icset.binloc    = input s-binloc) then do:

                    /* 6052  This WM Controlled Product and Bin Is Already
                        On The Count List */
                    run err.p(6052).
                    next-prompt s-binloc with frame f-1.
                    next.
                end.

                /*d**** Create the product/bin combination in WMSBP if it
                        does not already exist */
                if icsw.wmfl and
                not ({v-wmsbp.i g-whse "input s-binloc" "input s-prod" "/*"})
                then do:

                    /* 6002  Product Not Set Up in Bin - WMSB */
                    run err.p(6002).

                    if can-find(wmsb where
                        wmsb.cono   = g-cono    and
                        wmsb.whse   = g-whse    and
                        wmsb.binloc = input s-binloc) then
                    do on endkey undo, leave:
                        confirm = true.

                        update confirm with frame f-c.

                        if confirm then do for wmsb:

                            /*tb 5798 enp 06/13/92
                            Update WMSB & Handle First Store Dt in WMSB */
                            {w-wmsb.i g-whse "input s-binloc" exclusive-lock}
                            v-pribinfl = no.
                                                                                                            /*tb e10132 09/14/01 kjb; Check for the existence
                                of a primary bin location */
                            if avail wmsb and wmsb.assigncode = "p":u then do:

                                for each wmsbp where
                                    wmsbp.cono = g-cono and
                                    wmsbp.whse = g-whse and
                                    wmsbp.prod = input s-prod
                                no-lock:
                                                                                                                    if can-find (first wmsb where
                                        wmsb.cono       = wmsbp.cono   and
                                        wmsb.whse       = wmsbp.whse   and
                                        wmsb.binloc     = wmsbp.binloc and
                                        wmsb.assigncode = "p":u)  and
                                        wmsbp.binloc <> input s-binloc
                                    then v-pribinfl = yes.
                                end.
                            end.
                            if v-pribinfl = yes then do:

                                /*d This Product Has Another Primary Location
                                    Assigned */
                                run err.p(4892).
                            end.
                            else do:
                                create wmsbp.
                                assign
                                    wmsb.statuscode =
                                        if can-do("u,x,i":u,wmsb.statuscode)
                                            then wmsb.statuscode
                                        else "s":u
                                    wmsb.fstoredt   = if wmsb.fstoredt = ? then
                                                          today
                                                      else wmsb.fstoredt
                                    wmsbp.cono      = g-cono
                                    wmsbp.prod      = input s-prod
                                    wmsbp.binloc    = input s-binloc
                                    wmsbp.whse      = g-whse
                                    wmsbp.fstoredt  = today.
                                {t-all.i wmsbp}
                            end. /* no other primary bin found */
                        end. /* if confirm */
                    end. /* can-find wmsb */

                    hide frame f-c no-pause.
                    next-prompt s-binloc with frame f-1.
                    next.
                end. /* if icsw.wmfl */
            end. /* if field = s-binloc */
        end. /* if k-after */

        apply lastkey.
    end. /* Update  */

    /*tb 19381 03/18/96 mwb;  Added Product Search on Another Run */
    if can-find(first icset use-index k-product where
        icset.cono   = g-cono   and
        icset.prod   = s-prod   and
        icset.whse   = g-whse   and
        icset.binloc = s-binloc and
        icset.runno ne s-runno)
    then do:
        /*d Require go to continue */
        g-errfl = yes.

        /* 8729  WARNING: This Product Exists On Another Physical/Cycle
            Count Run */
        run warning.p(8729).

        /*d If not go, then reset entry line - don't enter */
        if g-errfl = yes then next.
    end.

    end. /* Do Loop */

    /*o********* Check for Duplicate Tickets ******/
    if can-find(first icset where
        icset.cono      = g-cono        and
        icset.whse      = g-whse        and
        icset.runno     = s-runno       and
        icset.prod      = s-prod        and
        icset.binloc    = s-binloc)
    then do on endkey undo main, next main:

        /*tb e9553 02/25/02 bpa; If ticket already exists for product, error
            out and send back ticket number for existing product */
        find first icset where
            icset.cono      = g-cono        and
            icset.whse      = g-whse        and
            icset.runno     = s-runno       and
            icset.prod      = s-prod        and
            icset.binloc    = s-binloc
        no-lock no-error.
      /*  v-ret-ticketno = icset.ticketno. */

        /* 8006  WARNING: This Product & Bin is Already On The Count List */
        run err.p(8006).
        pause.
        leave main.
    end.

    else if can-find(first icset where
        icset.cono      = g-cono        and
        icset.whse      = g-whse        and
        icset.runno     = s-runno       and
        icset.prod      = s-prod)
    then do on endkey undo main, next main:

        /* 8007  WARNING: This Product Already On The Count List Under a
            Different Bin Loc */
        run err.p(8007).
        pause.
    end.

    /*o Update the count expected */
    v-binloc = if icsw.wmfl then s-binloc else icsw.binloc1.

    {w-icsep.i g-whse s-runno s-prod v-binloc no-lock}

    if not avail icsep then do:

        /* 6095  Product Not Stored for Counting, Please Enter Quantity
            Expected */
        run err.p(6095).

        display s-unit with frame f-1.

        /*tb e5054 03/23/01 kjb; Add user hook */
        {icepec.z99 &user_before_qtyexp = "*"}

        /*tb t8355 03/02/04 kjb; Allow the user to indicate the new entry on
            the count is for unavailable inventory.  This should only be
            accessible when working with a physical count and the product is
            not warehouse managed. */
        update
            s-qtyexp
            s-unavailfl     when v-phyfl = yes and icsw.wmfl = no
            s-qtycnt
        with frame f-1.

        /*tb t5317 10/06/00 kjb; Call the calc-current-cost internal procedure
            to calculate the cost of the product to be stored on the ICSEP
            record for use during reconciliation. */
        run calc-current-cost in this-procedure (buffer icsp,
                                                 buffer icsw,
                                                 input-output v-cost).

        /*o Create a Count Record */
        create icsep.

        /*tb 19660 04/16/96 tdd; Need to set the Serial/Lot type */
        /*tb t5317 10/06/00 kjb; Load the new fields cost and icspecrecno with
            the values as they stand at this point in time.  They will be used
            during reconciliation. */
        /*tb t8355 03/02/04 kjb; Set the rectype to 'u' for unavailable
            inventory if the user have set the flag */
      /*  message "creating icsep". pause.  */
        assign
            icsep.cono        = g-cono

            icsep.whse        = g-whse
            icsep.runno       = s-runno
            icsep.prod        = s-prod
            icsep.binloc      = s-binloc       /* v-binloc  */
            icsep.rectype     = if s-unavailfl = yes then "u":u
                                else if icsw.wmfl then "w":u
                                else "1":u
            icsep.qtyexp      = s-qtyexp
            icsep.qtycnt      = s-qtycnt  /* added this */
            icsep.seqno       = 9999999
            icsep.wmfl        = icsw.wmfl
            icsep.phyfl       = v-phyfl
            icsep.unit        = s-unit
            icsep.createfl    = false /* true  */
            icsep.createdt    = g-today
            icsep.lastcntdt   = g-today /* icsw.lastcntdt  */
            icsep.mustcntfl   = icsw.countfl
            icsep.serlotty    = if {icsnpo.gcn &file=icsw
                                               &aosnpofl=v-serfl
                                               &cond="no"}
                                    then ""
                                else icsw.serlotty
            icsep.cost        = v-cost
            icsep.icspecrecno = icsp.icspecrecno
            icsw.countfl      = false
            icsw.lastcntdt    = g-today.

        {t-all.i icsw}
        {t-all.i icsep}
    end.

    /*o Create a ticket record */
    create icset.

    /*tb t8355 03/02/04 kjb; Set the rectype to 'u' for unavailable inventory
        if the user have set the flag */
  /*   message "creating icset" icset.ticketno icset.prod. pause.  */
    assign
        icset.cono     = g-cono
        icset.whse     = g-whse
        icset.runno    = s-runno
        icset.ticketno = s-ticketno
        icset.uticketno  = s-ticketno
        icset.prod     = s-prod
        icset.binloc   = s-binloc
        icset.qtycnt   = s-qtycnt    /* added this */
        icset.unit     = s-unit
        icset.wmfl     = icsw.wmfl
        icset.rectype  = if s-unavailfl = yes then "u":u
                         else if icset.wmfl then "w":u
                         else if icset.binloc = icsw.binloc1 then "1":u
                         else "2":u
        icset.entfl    = true   /* false */
        icset.ibcntupdtfl    = no     
        icset.createfl = true.
/*        v-confirm      = true.    */

/*    message "after update" icset.ticketno icset.prod. pause.  */
    {t-all.i icset}

    leave main.
end.

hide frame f-1 no-pause.


