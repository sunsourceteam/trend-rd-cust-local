/* oeepp1bo.las 1.1 01/03/98 */
/* oeepp1bo.las 1.1 06/17/96 */
/*h****************************************************************************
  INCLUDE      : oeepp1bo.las
  DESCRIPTION  : Quantity back order assignment statement
  USED ONCE?   : no
  AUTHOR       : jms
  DATE WRITTEN : 06/17/96
  CHANGES MADE :
   06/17/96 jms; TB# 21254 (10A) Develop Value Add Module
                ******************************************************************************/

 if oeel.bono > 0 and oeel.botype <> "d" then 
   0
 else if oeel.botype = "y" and oeel.qtyord > oeel.qtyship then
   oeel.qtyord - oeel.qtyship
 else if oeel.botype = "d" and x-doboqty <> 0 then
   x-doboqty
 else 0

