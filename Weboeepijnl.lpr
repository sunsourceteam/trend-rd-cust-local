/* oeepijrl.lpr 1.1 06/07/99 */
/*h*****************************************************************************
  INCLUDE      : oeepijrl.lpr
  DESCRIPTION  : OE Invoice Processing - Open Journal
  USED ONCE?   : no
  AUTHOR       : mwb
  DATE WRITTEN : 06/07/99
  CHANGES MADE :
    06/07/99 mwb; TB# 26253 Added Tendering to Batch Processing.  Made the
        Invoicing Open Journal logic a common include to be used in
        Batch (OEEBU) and Invoice Processin (OEEPI).
*******************************************************************************/

    if (not sasc.gl13perfl and substring(string(p-period),1,2) = "13")
    then do:

        v-pererrfl = yes.
        {&error}

    end. /* end of if not sasc.gl13perfl and .... */

    /*o Scope sasa due to exclusive-lock for wt create later in the process */
    do for sasa:

        {p-Webentry.i
            &modulefl = "modoefl"
            &updglty  = "updgloety"
            &chrecfl  = "sasc.croefl"
            &batchfl  = "yes"
            &postdt   = "p-postdt"
            &period   = "p-period"}

        /*tb 19484 09/23/96 mwb; loading of the WL module flag for WL */
        {wlsasa.gas}.

    end. /* end of do for sasa */

    /*d If the journal is not opened, set an error flag and get out */
    if g-jrnlno = 0 then do:

        v-pererrfl = yes.
        {&error}

    end. /* end of if g-jrnlno = 0 */

    /*d If the journal period is not valid, set an error flag and get out */
    if {y2kper6.gca p-period} < g-glbegper                  or
       {y2kper6.gca p-period} > g-glendper                  or
       (g-gl13perfl = no  and {y2kperex.gca p-period} > 12) or
       (g-gl13perfl = yes and {y2kperex.gca p-period} > 13)
    then do for sasj
    {&comtrans} transaction
    /{&comtrans}* */:

        {w-sasj.i g-jrnlno exclusive-lock}

        {delete.gde sasj validate(true,'')}

        assign
            g-jrnlno   = 0
            v-pererrfl = yes.
        {&error}

    end. /* end of if .... */
