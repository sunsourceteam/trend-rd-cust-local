/*p-oerr.i 05/20/99 */
/*si**** Custom ****************************************************************
  CLIENT  : 80281 Sunsource
  JOB     : sdi071
  AUTHOR  : dki
  DATE    : 05/20/99
  VERSION : 8.0.003
   ANCHOR : p-oerr.i
  PURPOSE : Order Register Report
  CHANGES :
    SI01 05/20/99 dki; sdi071; add sales mgr and salesrep group ranges.
******************************************************************************/
/* p-oerr.i 1.17 08/25/97 */
/*h****************************************************************************
  INCLUDE      : p-oerr.i
  DESCRIPTION  : OE Order Register Report
  USED ONCE?   : no
  AUTHOR       : mwb
  DATE WRITTEN : 09/12/90
  CHANGES MADE :
    02/06/91                Lock table overflow errors for the report file,
        added the outputty of "r" - for report, and moved the report delete to
        the end.
    05/02/91 mwb;           Made prod and slsrep print own program (OERRP.p)
        because oversized.
    06/27/91 mwb;           Added the invoice date range, and return flag to
        the qtyship, qtyord on lines - off the net sales in the header - make 
        like OE entry.
    11/18/91 mms; TB#  4923 JIT
    12/19/91 pap; TB#  5225 JIT/Line Due modifications
    01/07/92 pap; TB#  5225 JIT - rearrange displays
    05/08/92 dea; TB#  6472 Line DO; Option 3
    07/21/92 mwb; TB#  7191 Added custno changes.
    12/07/92 ntl; TB#  9041 Canadian Tax Changes (QST)
    12/09/92 mwb; TB#  4535 Added the legend display for rebates used in the
        margin calculation.
    01/13/92 mwb; TB#  5972 Removed page edit after each line.
    03/11/93 rs;  TB#  5869 Total Order Value is zero
    04/14/93 rs;  TB# 10909 Need to have closed status for BL type
    10/19/93 mwb; TB# 10909 Removed BL - closed edit.
    12/16/93 mwb; TB# 13974 Floor plan customers were printing when totals
        only is yes.
    06/26/96 dww; TB# 21391 High Character used in Ranges is incorrect.
        Replaced 24 "z" characters with an include {highchar.gas} that should
        always be higher in the ASCII chart than anything an operator would
        have typed manually.
    08/25/97 kjb; TB# 22385 OERR totals do not include rebates
******************************************************************************/

    /*o************ check if n-s, spec, DO is in the line items *************/
    /*o******** check if slsrepin and slsrepout are witin ranges *******/
    /*o********* check the product range for line items **************/
    if can-do("n,s,d",p-printnonst) or p-printline = yes or
       (b-prod <> v-lowchar or e-prod <> {highchar.gas &length = "24"}) 
    then do:
        find first oeel use-index k-oeel where
            oeel.cono       = g-cono        and
            oeel.orderno    = oeeh.orderno  and
            oeel.ordersuf   = oeeh.ordersuf and
            (p-printnonst = "a"                             or
             (p-printnonst = "n" and oeel.specnstype = "n") or
             (p-printnonst = "s" and oeel.specnstype = "s") or
             (p-printnonst = "d" and oeel.botype = "d"))       and
            (p-printline  = no  or
            (p-printline  = yes and
                oeel.slsrepin  >= b-slsrepin    and 
                oeel.slsrepin  <= e-slsrepin    and
                oeel.slsrepout >= b-slsrepout   and 
                oeel.slsrepout <= e-slsrepout   and
                
                /*si01 check for mgr and slstype ranges*/
                ((p-printsls = "o" and
                    can-find(smsn where 
                        smsn.cono        = g-cono           and 
                        smsn.slsrep      = oeel.slsrepout   and 
                        smsn.mgr        >= b-mgr            and 
                        smsn.mgr        <= e-mgr            and 
                        smsn.slstype    >= b-slstype        and 
                        smsn.slstype    <= e-slstype))      or
                (p-printsls = "i" and 
                    (can-find(smsn where 
                        smsn.cono        = g-cono           and 
                        smsn.slsrep      = oeel.slsrepin    and 
                        smsn.mgr        >= b-mgr            and 
                        smsn.mgr        <= e-mgr            and 
                        smsn.slstype    >= b-slstype        and 
                        smsn.slstype    <= e-slstype)       or
                    v-wideopen = true)))))                  and
            oeel.shipprod >= b-prod    and oeel.shipprod  <= e-prod
        no-lock no-error.
        if not avail oeel then next main.
    end.

/* das - begin */
    assign s-slstype = "   ".
    assign foundrep = "n".
    if p-printsls = "o" then do:
       find first smsn use-index k-smsn where 
           smsn.cono   = g-cono     and
           smsn.slsrep = oeeh.slsrepout no-lock no-error.
       if avail (smsn) then do:
          assign s-slstype = smsn.slstype.
          assign foundrep = "y".
       end.
    end.
    if p-printsls = "i" then do:
       find first smsn use-index k-smsn where
           smsn.cono   = g-cono     and
           smsn.slsrep = oeeh.slsrepin no-lock no-error.
       if avail (smsn) then do:
          assign s-slstype = smsn.slstype.
          assign foundrep = "y".
       end.
       if b-mgr ne "" then do:
          assign foundrep = "n".
          find first smsn use-index k-smsn where
             smsn.cono = g-cono     and
             smsn.slsrep = oeeh.slsrepout no-lock no-error.
             if avail (smsn) then do:
                if smsn.mgr >= b-mgr and smsn.mgr <= e-mgr then
                   assign foundrep = "y".
             end.
       end.   
    end.
/* das - end */
    
    /*si01 check for mgr and slstype ranges*/
       if p-printline = no and
          (foundrep = "n" or 
         ((p-printsls = "o" and  
                can-find(smsn where 
                    smsn.cono        = g-cono           and 
                    smsn.slsrep      = oeeh.slsrepout   and 
                    (smsn.mgr        < b-mgr            or 
                     smsn.mgr        > e-mgr            or 
                     smsn.slstype    < b-slstype        or 
                     smsn.slstype    > e-slstype)))     or
             (p-printsls = "i" and 
                can-find(smsn where 
                    smsn.cono        = g-cono           and 
                    smsn.slsrep      = oeeh.slsrepin    and 
/*                    (smsn.mgr        < b-mgr            or
                     smsn.mgr        > e-mgr            or   */
                    (smsn.slstype    < b-slstype        or 
                     smsn.slstype    > e-slstype))      and
                v-wideopen = false)))                    
    then next.


    /********* calc variables for header info **********/
    {p-oerr2.i &arsc = {&arsc}}
    if p-printord <> "p" and ((p-printord = "s" and p-printline = no) or
       p-printord <> "s") and p-printtot = no
    then do:
        assign w-cust = v-cust.
        assign w-cust = replace(w-cust,"*","").
        assign w-cust = replace(w-cust,"!","").
        assign zsdi-cust = dec(w-cust).
        /* das - New Cust Mod */
        find zsdiarsc where zsdiarsc.cono = g-cono and
                            zsdiarsc.custno = zsdi-cust and
                            zsdiarsc.statustype = yes
                            no-lock no-error.
        if avail zsdiarsc then
          do:
          if zsdi-toprinter = yes then
            do:
            put control "~033(10U~033(s1p8v0s3b16901T".  
            display with frame f-NewCust.
            put control "~033(10U~033(s0p16.67h8.5v0s0b0T".
          end.
          else
            display with frame f-NewCustV.
        end.
          
        /** 12/19/91 pap  TB# 5225 JIT - add promised date      **/
        /**  1/7/92  pap  TB# 5225 JIT - rearrange display      **/
        display 
              oeeh.orderno        oeeh.ordersuf       oeeh.notesfl
              v-cust              oeeh.transtype      s-stagecd
              oeeh.approvty
              oeeh.whse           oeeh.enterdt        oeeh.reqshipdt
              oeeh.promisedt      s-addon           /*s-tax*/     /*tb#9041*/
              oeeh.taxamt[4] when g-country =  "ca" @ s-tax       /*tb#9041*/
              s-tax          when g-country ne "ca"               /*tb#9041*/
              s-netsale           oeeh.takenby        s-slstype
              s-lookupnm          oeeh.whse           oeeh.slsrepin
              oeeh.slsrepout
          oeeh.taxamt[1] when g-country = "ca" @ oeeh.psttaxamt   /*tb#9041*/
              oeeh.totlineord
        with frame f-oerrb.
        if g-seecostfl then
          display s-margin s-margpct v-rebatety with frame f-oerrb.
        if oeeh.wtauth <> 0 then
          display v-auth oeeh.wtauth with frame f-oerrb.
        down with frame f-oerrb.

    end.

    /* Print Invoice To if Applicable */
    /*tb 13974 12/16/93 mwb; added totals only edit */
    if oeeh.fpcustno ne {c-empty.i} and p-printtot = no and
       (p-printord = "o" or p-printord = "c") 
    then do:

        {&arsc}
        {w-arsc.i oeeh.fpcustno no-lock}
        /{&arsc}* */

        if avail arsc then do:
            assign
                v-fpcustno = string(arsc.custno) + arsc.notesfl
                s-fplookup = arsc.lookupnm.
            display v-fpcustno s-fplookup with frame f-oerrfp.
            down with frame f-oerrfp.
        end.
    end.

    /*o********* if print the line items is yes ************/
    /*tb 5869 03/11/93 rs; Total Order Value is zero using salesrep */
    /* if p-printline or p-printord = "s" then */
       run oerrl.p(recid(oeeh)).

    /*o****** calc totals for order type ***************/
    if (p-printord <> "p" and ((p-printord = "s" and p-printline = no) or
        p-printord <> "s")) or v-count <> 0 
    then do:
        
        /*tb 22385 08/25/97 kjb; Pass in a new parameter to comment out the
            totalling of rebate values because that has already been done in
            p-oerr2.i. */
        {p-oerr1.i &com_asgn = "/*"}
    
    end.

    if p-printord <> "p" and ((p-printord = "s" and p-printline = no) or
       p-printord <> "s") and p-printtot = no then do:
        /*tb 5972 11/14/92 dea; replace with f-bar...
        display skip with frame f-blank.
        down with frame f-blank.
        */
        display with frame f-bar.
    end.

    assign v-totcost    = 0   s-margin  = 0   s-margpct    = 0
           v-cust       = ""  s-netsale = 0   v-count      = 0
           s-addon      = 0   s-tax     = 0   v-totlineamt = 0
           s-stagecd    = ""
           t-transcnt   = t-transcnt + 1.
