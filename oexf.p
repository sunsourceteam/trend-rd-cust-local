{p-rptbeg.i}


define buffer catmaster for notes.

Define Var v-technology as character format "x(30)"  no-undo.
Define Var v-parentcode as char format "x(24)"       no-undo.
Define Var v-vendorid   as char format "x(24)"       no-undo.
Define Var x-cat        as char format "x(4)"        no-undo.


define var zx-cono    like oeel.cono         no-undo.       
define var zx-qty      like oeel.qtyship      no-undo.      
define var zx-orderno  like oeel.orderno      no-undo.      
define var zx-ordersuf like oeel.ordersuf     no-undo.      
define var zx-lineno   like oeel.lineno       no-undo.      
define var zx-cost like oeel.prodcost no-undo.       
define var zx-glcost like oeel.prodcost no-undo.     
define var zx-sell like oeel.price    no-undo.       
                                                                        
define buffer a-vaesl for vaesl.                                               
define buffer b-vaesl for vaesl.                                               define buffer c-vaesl for vaesl.                                               
define buffer d-vaesl for vaesl.                                                
define buffer e-vaesl for vaesl.                                               define buffer f-vaesl for vaesl.                                               
define buffer g-vaesl for vaesl.                                                
define buffer h-vaesl for vaesl.                                                
define buffer i-vaesl for vaesl.                                                
define buffer j-vaesl for vaesl.                                                
define buffer k-vaesl for vaesl.                                               define buffer l-vaesl for vaesl.                                                define buffer m-vaesl for vaesl.                                                define buffer n-vaesl for vaesl.                                                define buffer o-vaesl for vaesl.                                               
define buffer p-vaesl for vaesl.                                                 


define buffer a-oeelk for oeelk.                                               define buffer b-oeelk for oeelk.                                               define buffer c-oeelk for oeelk.                                               
define buffer d-oeelk for oeelk.                                               
define buffer e-oeelk for oeelk.                                               define buffer f-oeelk for oeelk.                                               
define buffer g-oeelk for oeelk.                                                
define buffer h-oeelk for oeelk.                                               define buffer i-oeelk for oeelk.                                               define buffer j-oeelk for oeelk.                                              define buffer k-oeelk for oeelk.                                               
define buffer l-oeelk for oeelk.                                                
define buffer m-oeelk for oeelk.                                               define buffer n-oeelk for oeelk.                                               define buffer o-oeelk for oeelk.                                              
define buffer p-oeelk for oeelk.                                              

define var x-datefrom as date init 02/01/2003.
define var x-dateto   as date init 02/28/2003.
define var x-dateit   as date no-undo.
define var x-name     as char no-undo.

define stream xc. 
 


if sapb.rangebeg[1] ne "" then
    do:
    v-datein = sapb.rangebeg[1].
    {p-rptdt.i}
    if string(v-dateout) = v-lowdt then
      x-datefrom = 01/01/1900.
    else  
      x-datefrom = v-dateout.
    end.
else
  x-datefrom = 01/01/1900.



if sapb.rangeend[1] ne "" then
    do:
    v-datein = sapb.rangeend[1].
    {p-rptdt.i}
    if string(v-dateout) = v-lowdt then
      x-dateto = 01/01/1900.
    else  
      x-dateto = v-dateout.
    end.
else
  x-dateto = 01/01/1900.

x-name = if sapb.optvalue[1] = "" then 
           "forders" 
         else
           sapb.optvalue[1].


output stream xc to value("/usr/tmp/" + x-name + ".lxg").

export stream xc delimiter "~011"
  "Whse"
  "OrderNo"
  "OrderSuf"
  "LineNo"
  "TransType"
  "Kit?"
  "VaNo"
  "InvoiceDt"
  "ProdCat"
  "Technology"
  "Product"
  "QtyShip"
  "TotOrdVal"
  "ProdCost"
  "GLCost"
  "GLPeriod"
  "Fab"
  "Control 1 Labor 1 Min"
  "DL Consultant Labor"
  "Labor 1 Min"
  "Labor Fab Tech"
  "Labor - R.I.S."
  "Labor Field Service"
  "FAB VA LITE LABOR - WHSE"
  "FAB VA LITE LABOR -FAB"
  "FAB VA LITE LABOR -SERV"
  "FAB HSI"
  "FAB SCHRUPP"
  "HSI EXTERNAL"
  "SCHRUPP EX LABOR & PARTS"
  "FAB CAD"
  "FAB OT"
  "Region"
  "District"
  "SalesRep Name"
  "SalesRep Number"
  "CustomerNbr"
  "Customer Name".
  
for each icsd where icsd.cono = g-cono and
                    icsd.salesfl = true no-lock:
  
  do x-dateit = x-datefrom to x-dateto:
  for each oeeh use-index k-invproc where oeeh.cono = g-cono    and 
                      oeeh.whse = icsd.whse and
                      oeeh.invoicedt = x-dateit and
                      (oeeh.updtype = "m" or
                       oeeh.updtype = "i") no-lock :
    for each oeel use-index k-oeel where oeel.cono = oeeh.cono and 
                        oeel.orderno = oeeh.orderno and
                        oeel.ordersuf = oeeh.ordersuf and
                        oeel.specnstype <> "l" and
                       (oeel.ordertype = "f"  or
                        oeel.kitfl = true ) no-lock:
      if oeel.ordertype = "f" then   
        do:
        find first a-vaesl use-index k-vaesl
                       where a-vaesl.cono     = g-cono and
                             a-vaesl.vano     = oeel.orderaltno and
                             a-vaesl.shipprod = "Fab" 
                             no-lock no-error.
        find first b-vaesl use-index k-vaesl
                       where b-vaesl.cono     = g-cono and
                             b-vaesl.vano     = oeel.orderaltno and
                             b-vaesl.shipprod = "DL CONSULTANT Labor"
                             no-lock no-error.
        find first c-vaesl use-index k-vaesl
                       where c-vaesl.cono     = g-cono and
                             c-vaesl.vano     = oeel.orderaltno and
                             c-vaesl.shipprod = "Labor 1 min"
                             no-lock no-error.
        find first d-vaesl use-index k-vaesl
                       where d-vaesl.cono     = g-cono and
                             d-vaesl.vano     = oeel.orderaltno and
                             d-vaesl.shipprod = "control one ex labor"
                             no-lock no-error.
        find first e-vaesl use-index k-vaesl
                       where e-vaesl.cono     = g-cono and
                             e-vaesl.vano     = oeel.orderaltno and
                             e-vaesl.shipprod = "Labor Fab Tech     "
                             no-lock no-error.
        find first f-vaesl use-index k-vaesl
                       where f-vaesl.cono     = g-cono and
                             f-vaesl.vano     = oeel.orderaltno and
                             f-vaesl.shipprod = "Labor - R.I.S."
                             no-lock no-error.
        find first g-vaesl use-index k-vaesl
                       where g-vaesl.cono     = g-cono and
                             g-vaesl.vano     = oeel.orderaltno and
                             g-vaesl.shipprod = "Labor Field Service"
                             no-lock no-error.
        find first h-vaesl use-index k-vaesl
                       where h-vaesl.cono     = g-cono and
                             h-vaesl.vano     = oeel.orderaltno and
                             h-vaesl.shipprod = "FAB VA LITE LABOR - WHSE"
                             no-lock no-error.
        find first i-vaesl use-index k-vaesl
                       where i-vaesl.cono     = g-cono and
                             i-vaesl.vano     = oeel.orderaltno and
                             i-vaesl.shipprod = "FAB VA LITE LABOR -FAB"
                             no-lock no-error.
        find first j-vaesl use-index k-vaesl
                       where j-vaesl.cono     = g-cono and
                             j-vaesl.vano     = oeel.orderaltno and
                             j-vaesl.shipprod = "FAB VA LITE LABOR -SERV"
                             no-lock no-error.
                               

        find first k-vaesl use-index k-vaesl
                       where k-vaesl.cono     = g-cono and
                             k-vaesl.vano     = oeel.orderaltno and
                             k-vaesl.shipprod = "FAB HSI"
                             no-lock no-error.
                               
         find first l-vaesl use-index k-vaesl
                       where l-vaesl.cono     = g-cono and
                             l-vaesl.vano     = oeel.orderaltno and
                             l-vaesl.shipprod = "FAB SCHRUPP"
                             no-lock no-error.
                               
         find first m-vaesl use-index k-vaesl
                       where m-vaesl.cono     = g-cono and
                             m-vaesl.vano     = oeel.orderaltno and
                             m-vaesl.shipprod = "HSI EXTERNAL LABOR/PARTS"
                             no-lock no-error.
                               
         find first n-vaesl use-index k-vaesl
                       where n-vaesl.cono     = g-cono and
                             n-vaesl.vano     = oeel.orderaltno and
                             n-vaesl.shipprod = "SCHRUPP EX LABOR & PARTS"
                             no-lock no-error.
                               
         find first o-vaesl use-index k-vaesl
                       where o-vaesl.cono     = g-cono and
                             o-vaesl.vano     = oeel.orderaltno and
                             o-vaesl.shipprod = "FAB CAD"
                             no-lock no-error.
                               
         find first p-vaesl use-index k-vaesl
                       where p-vaesl.cono     = g-cono and
                             p-vaesl.vano     = oeel.orderaltno and
                             p-vaesl.shipprod = "FAB OT"
                             no-lock no-error.
                                
                             
                             
        end.                     
      else
        do:
        find first a-oeelk use-index k-oeelk
                         where a-oeelk.cono     = g-cono and
                               a-oeelk.ordertype = "o" and
                               a-oeelk.orderno    = oeel.orderno and
                               a-oeelk.shipprod = "control one ex labor" 
                               no-lock no-error.
        find first b-oeelk use-index k-oeelk
                         where b-oeelk.cono     = g-cono and
                               b-oeelk.ordertype = "o" and
                               b-oeelk.orderno    = oeel.orderno and
                               b-oeelk.shipprod = "DL CONSULTANT Labor" 
                               no-lock no-error.

        find first c-oeelk use-index k-oeelk
                         where c-oeelk.cono     = g-cono and
                               c-oeelk.ordertype = "o" and
                               c-oeelk.orderno    = oeel.orderno and
                               c-oeelk.shipprod = "Labor 1 min"
                               no-lock no-error.
        find first d-oeelk use-index k-oeelk
                         where d-oeelk.cono     = g-cono and
                               d-oeelk.ordertype = "o" and
                               d-oeelk.orderno    = oeel.orderno and
                               d-oeelk.shipprod = "Fab"
                               no-lock no-error.

        find first e-oeelk use-index k-oeelk
                         where e-oeelk.cono     = g-cono and
                               e-oeelk.ordertype = "o" and
                               e-oeelk.orderno    = oeel.orderno and
                               e-oeelk.shipprod = "Labor Fab Tech" 
                               no-lock no-error.

        find first f-oeelk use-index k-oeelk
                         where f-oeelk.cono     = g-cono and
                               f-oeelk.ordertype = "o" and
                               f-oeelk.orderno    = oeel.orderno and
                               f-oeelk.shipprod = "Labor - R.I.S."
                               no-lock no-error.
        find first g-oeelk use-index k-oeelk
                         where g-oeelk.cono     = g-cono and
                               g-oeelk.ordertype = "o" and
                               g-oeelk.orderno    = oeel.orderno and
                               g-oeelk.shipprod = "Labor Field Service"
                               no-lock no-error.
        find first h-oeelk use-index k-oeelk
                         where h-oeelk.cono     = g-cono and
                               h-oeelk.ordertype = "o" and
                               h-oeelk.orderno    = oeel.orderno and
                               h-oeelk.shipprod = "FAB VA LITE LABOR - WHSE"
                               no-lock no-error.

        find first i-oeelk use-index k-oeelk
                         where i-oeelk.cono     = g-cono and
                               i-oeelk.ordertype = "o" and
                               i-oeelk.orderno    = oeel.orderno and
                               i-oeelk.shipprod = "FAB VA LITE LABOR -FAB"
                               no-lock no-error.

        find first j-oeelk use-index k-oeelk
                         where j-oeelk.cono     = g-cono and
                               j-oeelk.ordertype = "o" and
                               j-oeelk.orderno    = oeel.orderno and
                               j-oeelk.shipprod = "FAB VA LITE LABOR -SERV"
                               no-lock no-error.


        find first k-oeelk use-index k-oeelk
                         where k-oeelk.cono     = g-cono and
                               k-oeelk.ordertype = "o" and
                               k-oeelk.orderno    = oeel.orderno and
                               k-oeelk.shipprod = "FAB HSI"
                               no-lock no-error.



        find first l-oeelk use-index k-oeelk
                         where l-oeelk.cono     = g-cono and
                               l-oeelk.ordertype = "o" and
                               l-oeelk.orderno    = oeel.orderno and
                               l-oeelk.shipprod = "FAB SCHRUPP"
                               no-lock no-error.



        find first m-oeelk use-index k-oeelk
                         where m-oeelk.cono     = g-cono and
                               m-oeelk.ordertype = "o" and
                               m-oeelk.orderno    = oeel.orderno and
                               m-oeelk.shipprod = "HSI EXTERNAL LABOR/PARTS"
                               no-lock no-error.



        find first n-oeelk use-index k-oeelk
                         where n-oeelk.cono     = g-cono and
                               n-oeelk.ordertype = "o" and
                               n-oeelk.orderno    = oeel.orderno and
                               n-oeelk.shipprod = "SCHRUPP EX LABOR & PARTS"
                               no-lock no-error.



        find first o-oeelk use-index k-oeelk
                         where o-oeelk.cono     = g-cono and
                               o-oeelk.ordertype = "o" and
                               o-oeelk.orderno    = oeel.orderno and
                               o-oeelk.shipprod = "FAB CAD"
                               no-lock no-error.



        find first p-oeelk use-index k-oeelk
                         where p-oeelk.cono     = g-cono and
                               p-oeelk.ordertype = "o" and
                               p-oeelk.orderno    = oeel.orderno and
                               p-oeelk.shipprod = "FAB OT"
                               no-lock no-error.






        end.
                             
      
          
      run zsdisellcost2.p (input oeel.cono,                    
                           input oeel.qtyship,            
                           input oeel.orderno,            
                           input oeel.ordersuf,           
                           input oeel.lineno,             
                           input-output  zx-cost,          
                           input-output  zx-GLcost,         
                           input-output  zx-sell).         

      find smsn where smsn.cono = g-cono and 
                      smsn.slsrep = oeel.slsrepout 
                      no-lock no-error.     

      find arsc where arsc.cono = g-cono and
                      arsc.custno = oeel.custno no-lock no-error.
                     
   
     
      if   (oeel.ordertype = "f" and avail a-vaesl) or
           (oeel.kitfl = true and avail d-oeelk) or
            
           (oeel.ordertype = "f" and avail d-vaesl) or
           (oeel.kitfl = true and avail a-oeelk) or
               
           (oeel.ordertype = "f" and avail b-vaesl) or
           (oeel.kitfl = true and avail b-oeelk) or
            
           (oeel.ordertype = "f" and avail c-vaesl) or
           (oeel.kitfl = true and avail c-oeelk) or

           (oeel.ordertype = "f" and avail e-vaesl) or
           (oeel.kitfl = true and avail e-oeelk) or
            
           (oeel.ordertype = "f" and avail f-vaesl) or
           (oeel.kitfl = true and avail f-oeelk) or
        
           (oeel.ordertype = "f" and avail g-vaesl) or
           (oeel.kitfl = true and avail g-oeelk) or 
 
           (oeel.ordertype = "f" and avail h-vaesl) or
           (oeel.kitfl = true and avail h-oeelk) or 
           
           (oeel.ordertype = "f" and avail i-vaesl) or
           (oeel.kitfl = true and avail i-oeelk) or 
           
           (oeel.ordertype = "f" and avail j-vaesl) or
           (oeel.kitfl = true and avail j-oeelk)  or      
           
           (oeel.ordertype = "f" and avail k-vaesl) or
           (oeel.kitfl = true and avail k-oeelk)  or

           (oeel.ordertype = "f" and avail l-vaesl) or
           (oeel.kitfl = true and avail l-oeelk)  or
           
           (oeel.ordertype = "f" and avail m-vaesl) or
           (oeel.kitfl = true and avail m-oeelk)  or
           
           (oeel.ordertype = "f" and avail n-vaesl) or
           (oeel.kitfl = true and avail n-oeelk)  or
           
           (oeel.ordertype = "f" and avail o-vaesl) or
           (oeel.kitfl = true and avail o-oeelk)  or
           
           (oeel.ordertype = "f" and avail p-vaesl) or
           (oeel.kitfl = true and avail p-oeelk)  then


      
      do: 
      find icsp where icsp.cono = g-cono and 
                      icsp.prod = oeel.shipprod no-lock no-error.
      if oeel.specnstype = "n" then
        x-cat = oeel.prodcat.
      else
      if not avail icsp then
        x-cat = oeel.prodcat.
      else
        x-cat = icsp.prodcat.                         
      
      find catmaster where 
           catmaster.cono         = g-cono and 
           catmaster.notestype    = "zz" and
           catmaster.primarykey   = "apsva" and
           catmaster.secondarykey = x-cat no-lock no-error. 
      if not avail catmaster then
        do:
        v-parentcode = "".
        v-vendorid = x-cat.
        if x-cat  = "a" then 
          v-technology = "Automation".
        else if x-cat  = "f" then
          v-technology = "Fabrication". 
        else if x-cat = "h" then 
          v-technology = "Hydraulic".   
        else if x-cat  = "p" then 
          v-technology =  "Pneumatic".   
        else if x-cat = "s" then 
          v-technology =  "Service".     
        else if x-cat = "f" then 
          v-technology =  "Filtration".  
        else if x-cat  = "c" then 
          v-technology =  "Connectors".  
        else if x-cat  = "m" then 
          v-technology =  "Mobile".      
        else if x-cat = "n" then 
          v-technology =  "None".        
        end.
  
      else
        assign
          v-vendorid   = catmaster.noteln[1]
          v-technology = catmaster.noteln[2]
          v-parentcode = catmaster.noteln[3].
       
      export stream xc delimiter "~011"
            icsd.whse
            oeel.orderno oeel.ordersuf oeel.lineno oeel.ordertype
            oeel.kitfl
            oeel.orderaltno 
            oeel.invoicedt 
            x-cat
            v-technology
            oeel.shipprod 
            (if oeel.returnfl = false then
                oeel.qtyship
             else
                oeel.qtyship * -1)
             (if oeel.returnfl = false then
                zx-sell 
              else
                zx-sell * -1)
             (if oeel.returnfl = false then
                zx-cost 
              else
                zx-cost * -1)
            (if oeel.returnfl = false then
               zx-glcost 
             else
               zx-glcost * -1)
            (string(month(oeel.invoicedt),"99") +
             substring (string(year(oeel.invoicedt),"9999"),3,2))
            
            (if  (oeel.ordertype = "f" and avail a-vaesl) or
                 (oeel.kitfl = true and avail d-oeelk) then
               true
             else
               false)
            (if  (oeel.ordertype = "f" and avail d-vaesl) or
                 (oeel.kitfl = true and avail a-oeelk) then
               true
             else
               false)
               
            (if  (oeel.ordertype = "f" and avail b-vaesl) or
                 (oeel.kitfl = true and avail b-oeelk) then
               true
             else
               false)
            (if  (oeel.ordertype = "f" and avail c-vaesl) or
                 (oeel.kitfl = true and avail c-oeelk) then
               true
             else
               false)

            (if  (oeel.ordertype = "f" and avail e-vaesl) or
                 (oeel.kitfl = true and avail e-oeelk) then
               true
             else
               false)
            (if  (oeel.ordertype = "f" and avail f-vaesl) or
                 (oeel.kitfl = true and avail f-oeelk) then
               true
             else
               false)
             (if  (oeel.ordertype = "f" and avail g-vaesl) or
                 (oeel.kitfl = true and avail g-oeelk) then
               true
             else
               false)
             (if  (oeel.ordertype = "f" and avail h-vaesl) or
                 (oeel.kitfl = true and avail h-oeelk) then
               true
             else
               false)
              (if  (oeel.ordertype = "f" and avail i-vaesl) or
                 (oeel.kitfl = true and avail i-oeelk) then
               true
             else
               false)
              (if  (oeel.ordertype = "f" and avail j-vaesl) or
                 (oeel.kitfl = true and avail j-oeelk) then
               true
             else
               false)

             (if  (oeel.ordertype = "f" and avail k-vaesl) or
                 (oeel.kitfl = true and avail k-oeelk) then
               true
             else
               false)
 
             (if  (oeel.ordertype = "f" and avail l-vaesl) or
                 (oeel.kitfl = true and avail l-oeelk) then
               true
             else
               false)
        
             (if  (oeel.ordertype = "f" and avail m-vaesl) or
                 (oeel.kitfl = true and avail m-oeelk) then
               true
             else
               false)
      
             (if  (oeel.ordertype = "f" and avail n-vaesl) or
                 (oeel.kitfl = true and avail n-oeelk) then
               true
             else
               false)
 
            (if  (oeel.ordertype = "f" and avail o-vaesl) or
                (oeel.kitfl = true and avail o-oeelk) then
               true
             else
               false)
 
            (if  (oeel.ordertype = "f" and avail p-vaesl) or
                (oeel.kitfl = true and avail p-oeelk) then
               true
             else
               false)
                    
                  
            substring(smsn.mgr,1,1)
            substring(smsn.mgr,2,3)
            smsn.name 
            smsn.slsrep 
            oeel.custno 
            arsc.name. 
       end. /* Export */
     end.   /* OEEL */
    end.      /* OEEH */
  end.      /* Date Do */
end.        /* ICSD */

                 
