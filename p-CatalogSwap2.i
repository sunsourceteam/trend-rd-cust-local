
/* if icsc was used with SLZC, original info is stored in user4. Put back */ 
find icsc where icsc.catalog = {&prodX} and
                substr(icsc.user4,43,12) <> " "  /* key off of Vendor # */
                no-lock no-error.    
if avail icsc then
  do:
  for each u-icsc where u-icsc.catalog = {&prodX} and
                 substr(u-icsc.user4,43,12) <> " "  /* key off of Vendor # */
                        exclusive-lock:
    /*********************** 
    if U-ICSC exists, put back
       icsc.prodcat
       icsc.pricetype
       icsc.prodcost
       icsc.stndcost
       icsc.vendno
       icsc.prodline
       icsc.rebatety
    ************************/
    assign u-icsc.ecbatchnm = "". 
    /* keep everything in synch in case user pressed F4 Cancel */
    if substr(u-icsc.user4,1,4)   <> substr(u-icsc.xxc14,1,4)   or
       substr(u-icsc.user4,7,4)   <> substr(u-icsc.xxc14,7,4)   or
       substr(u-icsc.user4,13,13) <> substr(u-icsc.xxc14,13,13) or  
       substr(u-icsc.user4,28,13) <> substr(u-icsc.xxc14,28,13) or
       substr(u-icsc.user4,43,12) <> substr(u-icsc.xxc14,43,12) or
       substr(u-icsc.user4,57,6)  <> substr(u-icsc.xxc14,57,6)  or
       substr(u-icsc.user4,67,8)  <> substr(u-icsc.xxc14,67,8) then
      assign substr(u-icsc.user4,1,4)   = substr(u-icsc.xxc14,1,4)  
             substr(u-icsc.user4,7,4)   = substr(u-icsc.xxc14,7,4)  
             substr(u-icsc.user4,13,13) = substr(u-icsc.xxc14,13,13)
             substr(u-icsc.user4,28,13) = substr(u-icsc.xxc14,28,13)
             substr(u-icsc.user4,43,12) = substr(u-icsc.xxc14,43,12)
             substr(u-icsc.user4,57,6)  = substr(u-icsc.xxc14,57,6) 
             substr(u-icsc.user4,67,8)  = substr(u-icsc.xxc14,67,8).
    assign u-icsc.xxc14 = " ".
    assign u-icsc.prodcat   = substr(u-icsc.user4,1,4).
    assign u-icsc.pricetype = substr(u-icsc.user4,7,4).
    assign u-icsc.prodcost  = dec(substr(u-icsc.user4,13,13)).
    assign u-icsc.stndcost  = dec(substr(u-icsc.user4,28,13)).
    assign u-icsc.vendno    = dec(substr(u-icsc.user4,43,12)).
    assign u-icsc.prodline  = substr(u-icsc.user4,57,6).
    assign u-icsc.rebatety  = substr(u-icsc.user4,67,8).
    assign u-icsc.user4     = "".
  end. /* for each u-icsc */    
end. /* avail icsc */
