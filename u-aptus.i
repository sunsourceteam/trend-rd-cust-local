
/* u-aptus.i 1.1 01/03/98 */
/* u-aptus.i 1.2 10/22/93 */
/*h*****************************************************************************
  INCLUDE      : u-aptus.i
  DESCRIPTION  : AP Vendor Tax Update
  USED ONCE?   : yes
  AUTHOR       : enp
  DATE WRITTEN :
  CHANGES MADE : 10/22/93 kmw; TB# 12955 1099 Name Needed in APSV
*******************************************************************************/

apsv.fed1099no
apsv.fed1099box
apsv.fedtaxid
/*tb 12955 10/22/93 kmw; 1099 Name Needed in APSV */
apsv.ap1099nm
apsv.user24
