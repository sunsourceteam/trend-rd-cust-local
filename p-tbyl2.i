define var  ip-whse like icsd.whse no-undo.
define var  ip-prod like icsw.prod no-undo.

Define new shared temp-table dps no-undo
  Field whse        like icsw.whse
  Field nonstockty  as char  format "x(1)"
  Field prod        like icsw.prod
  Field sprod        like icsw.prod
  Field sdescrip    as char format "x(60)"
  field custpo      like oeeh.custpo
  field custno      like oeeh.custno
  field custshipto  like oeeh.shipto
  Field custname    like arsc.name
  Field seqno       as integer format ">>>9"
  Field ind1        as character format "x"
  Field transtype   like oeel.transtype    
  Field ordersign   as character format "xxx"    
  Field stagecd     as integer format "9"
  Field holdcd      as character format "x"
  Field ordno       like oeeh.orderno        
  Field ordsuf      like oeeh.ordersuf      
  Field lineno      like oeel.lineno       
  Field tieord      like oeeh.orderno
  Field dash        as character format "x"       
  Field tiesuf      as integer format "99"       
  Field tietype     as character format "x"
  Field tielineno   like oeel.lineno    
  Field tieseq      like oeelk.seqno
  Field zdesc       as character format "x(15)"        
  Field szdesc       as character format "x(15)"        
  Field reqdt       as date    format "99/99/99"
  Field promdt      as date    format "99/99/99"
  Field ackncode       as   character format "x(1)"
  Field ackndate       as   date format "99/99/99"
  Field acknnbr        as   character format "x(35)"
  Field ackntrack      as   character format "x(35)"
  Field sacknnbr        as   character format "x(35)"
  Field sackntrack      as   character format "x(35)"
  Field qtyord      as decimal format ">>>>>-"       
  Field qtyresv     as decimal format ">>>>>-"   
  Field qtyunfill   as decimal format ">>>>>-"            
  Field povendno     like apsv.vendno
  Field povendshipfm like apss.shipfmno   
  Field povendname   like apsv.name
  Field poqtyord    as decimal format ">>>>>-"     
  Field poqtyrcvd   as decimal format ">>>>>-"    
  field unitcost    as decimal format ">>>>>>9.9999-"
  field unitprice   as decimal format ">>>>>>9.9999-"
  Field inhouse     as decimal format ">>>>>-"
  Field ind2        as character format "x"  
  Field blcode         as   integer
  Field ordertype      as   character
  field leaddate    as date
  
index reqinx
      prod
      blcode
      reqdt
      ordno
      ordsuf
      lineno
      seqno

index whse
      whse
      prod
      blcode
      promdt
      ordno
      ordsuf
      lineno
      seqno
index finx  
      ordertype 
      ordno   
      ordsuf  
      seqno.

