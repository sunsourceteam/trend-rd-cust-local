
define {1} shared var o-wtno like wteh.wtno             no-undo. 
define {1} shared var o-wtsuf like wteh.wtsuf           no-undo.

define {1} shared var z-wtnoline as character format "x(18)"    no-undo.
define {1} shared var z-wtno  like poeh.pono             no-undo.
define {1} shared var z-wtsuf like poeh.posuf            no-undo.
define {1} shared var z-orderno like oeeh.orderno        no-undo.
define {1} shared var z-ordersuf like oeeh.ordersuf      no-undo.
define {1} shared var z-shipprod like poel.shipprod      no-undo.
define {1} shared var z-lineno like oeel.lineno          no-undo.
define {1} shared var z-seqno  like oeelk.seqno          no-undo.
define {1} shared var z-line like poel.lineno            no-undo.
define {1} shared var z-answer as logical                no-undo.
define {1} shared var v-wtlineno like wtel.lineno        no-undo.
define {1} shared var v-seqno    like poelo.seqno        no-undo.

define {1} shared var v-fkey as character format "x(5)" extent 5 initial
   ["WTIO ","POIO ","","",""]             no-undo.

define {1} shared frame f-wttie.
define {1} shared frame f-wttiel.
define {1} shared frame f-wttoed.

form 
  g-wtno    at 1 no-labels
    {f-help.i}
  "-"       at 8 
  g-wtsuf   at 9 no-labels
    {f-help.i}

  g-wtlineno at  13 no-labels
    
   with frame f-wttie  width 80 row 2 centered side-labels 
   overlay title  g-title.
form 
   z-wtnoline    at 1    
   z-shipprod    at 22
   z-orderno     at 48   
   "-"           at 55   
   z-ordersuf    at 56   
   z-lineno      at 60   
   z-seqno       at 66   
   
   with frame f-wttiel overlay no-labels no-box width 78 column 2 row 10
     scroll 1 v-length down.

form 
  
   g-wtno        at 1    
   "-"           at 8
   g-wtsuf       at 9
   g-wtlineno    at 12
   v-seqno       at 16
   z-shipprod    at 22
   z-orderno     at 48   
   "-"           at 55   
   z-ordersuf    at 56   
   z-lineno      at 60   
   z-seqno       at 66 skip  
   "DELETE ? "   at 1
   z-answer      at 3 
   
   with frame f-wttied overlay no-labels width 78 column 2 row 13
  title      
  " P.O.    Line Seq  Item                         Tied       Tied  Tied Kit"
      .


assign v-length = 10.
assign v-fkey[1] = "OEIO"
       v-fkey[2] = "WTIO".
