/* p-zsdipckwt.i  */
/*  **************************************************************************
 
   Pick Pack Detail Creation 

   Created:        06/27/07
   Author:         Tah
   Modifications:   dkt 04/17/2008 commented RECEIVING out - this is
                    the wrong program! WTEI oeepb99.p should do it, not wtep.
                    dkt RECEIVING 04/14/2008 
                   This might be exactly the same as p-zsdipck.i, plz check

   Parameters the same as d-oeepp1.i to accomodate all modules:

       &returnfl   = "oeel.returnfl"
       &cubes      = "oeel.cubes"
       &weight     = "oeel.weight"
       &head       = "oeeh"
       &line       = "oeel"
       &section    = "oeeh"
       &ourproc    = "oeepp"
       &specnstype = "oeel.specnstype"
       &altwhse    = "oeel.altwhse"
       &vendno     = "oeel.vendno"
       &kitfl      = "x-false"
       &botype     = "oeel.botype"
       &orderdisp  = "oeeh.orderdisp"
       &prevqtyship= "oeel.prevqtyshp"
       &netamt     = "oeel.netamt"
       &prtpricefl = "oeeh.pickprtfl"
       &bono       = "oeel.bono"
       &orderno    = "oeel.orderno"
       &ordersuf   = "oeel.ordersuf"
       &custno     = "oeeh.custno"
       &corechgty  = "oeel.corechgty"
       &qtybo      = "~{oeepp1bo.las~}" 
       &ordertype  = "o"
       &prefix     = "order"
       &seqno      = "0"
       &stkqtyshp  = "oeel.stkqtyship"
       &nosnlots   = "oeel.nosnlots"
       &comtype    = ""oe""
       &comlineno  = "oeel.lineno"
       &shipto     = "oeeh.shipto"
       &twlstgedit = " and oeeh.stagecd < 3 "} 
 


Inits   Date      Description
-----   --------  ----------------------------------------------------------


*************************************************************************    */

if icsdAutoPck = "Y" and v-loopcnt = 1 and
   x-pckdetailfl and 
   ("{&line}" = "oeel" and s-qtyship > 0 or
    "{&line}" = "wtel" and s-qtyship > 0 or

     "{&line}" = "oeelk" and {&stkqtyshp} > 0 or 
    ("{&line}" <> "oeel"  and "{&line}" <> "oeelk" 
  and {&stkqtyshp} > 0)) then do:

    /* RECEIVING */
    curroefillqty = 0.                                            
     
/* INSPECT */  
   {w-icsw.i {&line}.shipprod
      {&line}.shipfmwhse no-lock "zx-"}
  
   if icsdAutoXdock = "y" and
    (not avail zx-icsw or 
      (avail zx-icsw and zx-icsw.wmrestrict <> "insp") ) then do:
/* INSPECT */
     for each oefill use-index k-order where 
              oefill.cono = g-cono and
              oefill.oper2     = g-operinit and
              oefill.reportnm  = sapb.reportnm and
              oefill.processty <> "" and
              oefill.ordertype = "{&ordertype}" and
              oefill.orderno   = {&orderno} and
              oefill.ordersuf  = {&ordersuf} and
              oefill.lineno    = {&line}.lineno and
              oefill.compseqno = if "{&line}" = "oeelk" then
                                   {&seqno}
                                 else
                                   0 : 

        run updateWmsbp.p(input p-whse, "RECEIVING", oefill.qtyalloc, 
                                    oefill.prod, 500 + g-cono, "RCV").
        curroefillqty = curroefillqty + oefill.qtyalloc.              
      end.                                                           
    end.
/*   RECEIVING */   
     
  {w-icsw.i {&line}.shipprod
      {&line}.shipfmwhse no-lock "zx-"}
  {w-icsp.i {&line}.shipprod no-lock "zx-"}

  if avail zx-icsp and zx-icsp.statustype = "L" then do:
    assign x-pckdetailfl = x-pckdetailfl. 
  end.
  else do:
  
    create zsdipckdtl.

    if avail zx-icsp then do:
      if avail zx-icsw then do:
        assign
          zsdipckdtl.binloc1       =  if zx-icsw.binloc1 = "new part" then
                                         " "
                                      else
                                        zx-icsw.binloc1 
          zsdipckdtl.binloc2       =  if zx-icsw.binloc2 = "new part" then
                                         " "
                                      else
                                        zx-icsw.binloc2. 
      end.
    end.
      else do:
     /* Non Stock? */
    find first wmsbp where 
               wmsbp.cono = g-cono + 500 and
               wmsbp.whse = {&line}.shipfmwhse and 
               wmsbp.prod = {&line}.shipprod no-lock no-error.
    if avail wmsbp then do:
      assign zsdipckdtl.binloc1       =  if wmsbp.binloc = "new part" then
                                           " "
                                         else
                                           wmsbp.binloc. 
      find next wmsbp where 
                wmsbp.cono = g-cono + 500 and
                wmsbp.whse = {&line}.shipfmwhse and 
                wmsbp.prod = {&line}.shipprod no-lock no-error.
      if avail wmsbp then 
        assign zsdipckdtl.binloc2       =  if wmsbp.binloc = "new part" then
                                             " "
                                           else
                                             wmsbp.binloc. 
      end.  
    end.
    for each   wmsbp where 
               wmsbp.cono = g-cono + 500 and
               wmsbp.whse = {&line}.shipfmwhse and 
               wmsbp.prod = {&line}.shipprod no-lock:
      find first wmsb where 
                 wmsb.cono = wmsbp.cono and
                 wmsb.whse = wmsbp.whse and 
                 wmsb.binloc = wmsbp.binloc no-lock no-error.
      if avail wmsb then do:
        if wmsb.priority = 9 then    
          zsdipckdtl.zzbinfl      = yes.
      end.  
    end.
  
    find first wmsb where 
               wmsb.cono = g-cono + 500 and
               wmsb.whse = {&line}.shipfmwhse and 
               wmsb.binloc = zsdipckdtl.binloc1 no-lock no-error.
    if avail wmsb then
      assign    zsdipckdtl.locationid     = wmsb.building.
  
  
    assign
       zsdipckdtl.cono           = g-cono
       zsdipckdtl.ordertype      = if "{&line}" = "oeel" or
                                      "{&line}" = "oeelk" then
                                     "o"
                                   else
                                   if "{&line}" = "wtel" then
                                     "t"
                                   else  
                                   if "{&line}" begins "va" then
                                     "f"
                                   else
                                   if "{&line}" begins "kp" then
                                     "w"
                                   else
                                     "o"
    
       zsdipckdtl.orderno        =  {&orderno} 
       zsdipckdtl.ordersuf       =  {&ordersuf} 
       zsdipckdtl.lineno         =  {&line}.lineno
       zsdipckdtl.seqno          =  {&seqno}
       zsdipckdtl.whse           =  {&line}.shipfmwhse
                                      
       zsdipckdtl.pickid         =  b-zsdipckid.pickid 
       zsdipckdtl.pickseq        =  b-zsdipcksq.seqid
                                     
       zsdipckdtl.shipprod       =  {&line}.shipprod   
       zsdipckdtl.stkqtyord      =  {&line}.stkqtyord   
       
       zsdipckdtl.stkqtyship     =  (if "{&line}" = "oeel" then
                                       s-qtyship
                                     else if "{&line}" = "oeelk" then
                                       {&stkqtyshp}
                                     else
                                       {&stkqtyshp})
       
       zsdipckdtl.stkqtybo       =  (if "{&line}" = "oeel" then
                                       s-qtybo
                                     else if "{&line}" = "oeelk" then
                                      x-qtybo
                                    else
                                      0)
       
       zsdipckdtl.stkqtypicked   =  (if "{&line}" = "oeel" then
                                       s-qtyship
                                    else if "{&line}" = "oeelk" then
                                      {&stkqtyshp}
                                    else
                                      {&stkqtyshp})
       
       zsdipckdtl.realqtypicked  = 0
       /*
       zsdipckdtl.locationid     = ""
       zsdipckdtl.binloc1        = ""
       zsdipckdtl.binloc2        = ""
       zsdipckdtl.zzbinfl        = no
       */
       zsdipckdtl.Pickbin        = ""
       zsdipckdtl.extype         = ""
       zsdipckdtl.exstatustype   = ""
       zsdipckdtl.statustype     = "a"
       zsdipckdtl.completefl     = no
       zsdipckdtl.transproc      = g-ourproc
       zsdipckdtl.recordtype     = if "{&line}" = "oeelk" then
                                     "C"
                                   else if x-false = true then
                                     "K"
                                   else
                                     " ".
       if zsdipckdtl.recordtype = "K" then
           zsdipckdtl.completefl = true.
       {t-all.i zsdipckdtl}
       /* RECEIVING  */
              
       /* dkt */
       if curroefillqty > 0 then do:
         if zsdipckdtl.stkqtypicked <= curroefillqty and
            zsdipckdtl.stkqtypicked <> 0 then do:
           assign zsdipckdtl.binloc2 = zsdipckdtl.binloc1
                  zsdipckdtl.binloc1 = "RECEIVING"
                  zsdipckdtl.locationid = "".
         end.
         else if zsdipckdtl.stkqtypicked > curroefillqty then do:
/* make one for qty curroefillqty and this is for qty - curroefill        */
           buffer-copy zsdipckdtl
              except
                zsdipckdtl.locationid
                zsdipckdtl.user2
                zsdipckdtl.binloc1                      
                zsdipckdtl.binloc2             
                zsdipckdtl.stkqtyord
                zsdipckdtl.stkqtyship
                zsdipckdtl.stkqtybo
                zsdipckdtl.stkqtypicked
              to bf-zsdipckdtl
              assign
                bf-zsdipckdtl.locationid =  ""
                bf-zsdipckdtl.user2   =  ""
                bf-zsdipckdtl.user1   = "Copy to receiving"
                bf-zsdipckdtl.binloc1 = "RECEIVING"
                bf-zsdipckdtl.binloc2 = zsdipckdtl.binloc1
                bf-zsdipckdtl.stkqtyord  = 0
                bf-zsdipckdtl.stkqtyship = 0
                bf-zsdipckdtl.stkqtybo   = 0
                bf-zsdipckdtl.stkqtypicked = curroefillqty.
                
           assign zsdipckdtl.stkqtypicked = 
                  zsdipckdtl.stkqtypicked - curroefillqty
                  zsdipckdtl.user1   = "Copy of original".
   
         end.
       end.
    /*    RECEIVING */
  end. /* else not labor */    
  
end.   /* icsdAutoPck = "Y" */ 
