/* oeetltpo.c99 */
/*si**** Custom ****************************************************************
  CLIENT  : 80281 SDI Operating Partners
  JOB     : sdi113 OE - Do Not Update SM Cost On Tied Lines
  ANCHOR  : oeetltpo.z99
  VERSION : 8.0.003
  PURPOSE : User Hook include for oeetltpo.p

            *** NXTREND SHOULD NOT MAKE ANY CHANGES TO THIS MODULE
                FOR CLIENT USE ONLY

  CHANGES :
    SI01 12/15/01 bdt; sdi113; Created
         03/09/09 das - CTRL-A notes in OEEXQ causing direct lines to carry 
                        over wrong cost.
*******************************************************************************/
/* oeetltpo.z99 1.2 09/25/94 */
/*h*****************************************************************************
  INCLUDE      : oeetltpo.z99
  DESCRIPTION  : User Hook include for oeetltpo.p
  USED ONCE?   : yes
  AUTHOR       : tdd
  DATE WRITTEN : 07/15/94
  CHANGES MADE :
    07/15/94 tdd; TB# 16058 - Created for &user_pr
    08/12/97 jms; TB# 23667 Added user include &user_def.
    09/17/97 emc; TB# 23842 Add user hook user_availpo

    Mode          Description
    ------------  ----------------------------------------------------------
    &user_pr      In oeetltpo.p, before updating PO Header (a-poetlt.i)
    &user_def     To allow addition of variables.
    &user_availpo  In oeetltpo.p, after if not avail poeh then return
*******************************************************************************/

/{&user_def}*/
  /* das - when OE is changed after a PO has been acknowledged, don't allow
           OEET to change duedt, reqshipdt and expshipdt.                    */
  def buffer z-poel for poel.
  def buffer q-note for notes.
  def var q-idx        as   i format "99"     no-undo.
  def var h-duedt      like poel.duedt        no-undo.
  def var h-reqshipdt  like poel.reqshipdt    no-undo.
  def var h-expshipdt  like poel.expshipdt    no-undo.
  find last z-poel where z-poel.cono      = g-cono and
                         z-poel.pono      = b-oeel.orderaltno and
                         z-poel.lineno    = b-oeel.linealtno  and
                         z-poel.transtype = "DO"
                         no-lock no-error.
  if avail z-poel then
     assign h-duedt     = z-poel.duedt
            h-reqshipdt = z-poel.reqshipdt
            h-expshipdt = z-poel.expshipdt.
  else
     assign h-duedt     = s-vduedt
            h-reqshipdt = s-vduedt
            h-expshipdt = s-vduedt.
/{&user_def}*/

/{&user_pr}*/

  /* das - 5/6/11 - */
  if b-oeel.specnstype = "n" and 
     b-oeel.prodcost  <> poel.price and
     poel.price = 0 then
    do:
    assign
          poel.invcost = if poel.priceoverfl = yes then poel.invcost
                         else if b-oeel.specnstype = "n" or
                                (b-oeel.costoverfl = yes and
                                 v-diffspecfl = no)
                              then b-oeel.prodcost / v-exchgrate
                              else s-price
          poel.price   = poel.invcost
          poel.rcvcost = poel.invcost
          poel.netamt  = if avail b-icss and b-icss.speccostty <> "" then
                           (poel.price * {speceach.gco &file = "b-icss"}) *
                            v-conv * poel.qtyord
                         else poel.price * poel.qtyord
          o-unitconv   = poel.unitconv.

    assign poel.netrcv  = poel.netamt.
  end. /* poel.price not assigned on NS */
  
  /* remove the standard cost markup from the PO Price */
  if b-oeel.prodcost = poel.price then
    do:
    find first notes where
               notes.cono         = g-cono and
               notes.notestype    = "ZZ" and
               notes.primarykey   = "standard cost markup" and
               notes.secondarykey = b-oeel.prodcat and
               notes.pageno       = 1
               no-lock no-error.
    if not avail notes then                               
      find notes use-index k-notes where                  
           notes.cono         = g-cono and                
           notes.notestype    = "zz" and                  
           notes.primarykey   = "standard cost markup" and
           notes.secondarykey = "" and                    
           notes.pageno       = 1                         
           no-lock no-error.                              
    if avail notes then                                   
      assign 
        poel.invcost = poel.invcost / (1 + (dec(notes.noteln[1]) / 100))
        poel.netamt  = poel.netamt  / (1 + (dec(notes.noteln[1]) / 100))
        poel.netrcv  = poel.netrcv  / (1 + (dec(notes.noteln[1]) / 100))
        poel.price   = poel.price   / (1 + (dec(notes.noteln[1]) / 100))
        poel.rcvcost = poel.rcvcost / (1 + (dec(notes.noteln[1]) / 100)).
  end. /* if b-oeel.prodcost = poel.price */

  /* oeexq carrying over the wrong cost after CTRL-A cost */
  if g-currproc = "oeexq" and poel.price <> v-prodcost and
     poel.priceoverfl = no and b-oeel.costoverfl = no and
     b-oeel.specnstype <> "n" then
    do:
    assign poel.invcost = v-prodcost
           poel.price   = poel.invcost
           poel.rcvcost = poel.invcost
           poel.netamt  = if avail b-icss and b-icss.speccostty <> "" then
                          (poel.price * {speceach.gco &file = "b-icss"}) *
                               v-conv * poel.qtyord
                          else poel.price * poel.qtyord
           poel.netrcv  = poel.netamt.
  end.
  
  find first pder where pder.cono = g-cono + 500 and
                        pder.orderno = g-orderno and
                        pder.ordersuf = g-ordersuf and
                        pder.lineno = b-oeel.lineno no-lock no-error.
  /*das pder on backorders if not found */
  if not avail pder then
    find first pder where pder.cono   = g-cono + 500 and
                        pder.orderno  = g-orderno and
                        pder.ordersuf = 00 and
                        pder.lineno   = b-oeel.lineno no-lock no-error.

/* tah 011007 
  if avail pder and
    (pder.user2 <> "") and
     avail apsv then
     do: 
     for each q-note where q-note.cono = g-cono and
                           q-note.notestype = "zz" and
                           q-note.primarykey = "DOPO Quote"
                           no-lock:
       do q-idx = 1 to 16:
         if q-note.noteln[q-idx] = " " then leave.
         if q-note.noteln[q-idx] = apsv.edipartner then
           do:
           assign poeh.resaleno = pder.user2.
           leave.
         end.
       end. /* q-idx = 1 to 16 */
     end. /* each q-note */
  end. /* avail pder */
   tah 011007 */
/*   tah 011007 */
   
  if avail pder and
    (pder.user2 <> "") then 
    do:
    if poeh.shipfmno <> 0 then do:
      {w-apss.i poeh.vendno poeh.shipfmno no-lock}
    end.  
    if avail apss and poeh.shipfmno <> 0 then
      do:
      for each q-note where q-note.cono = g-cono and
                            q-note.notestype = "zz" and
                            q-note.primarykey = "DOPO Quote"
                            no-lock:
        do q-idx = 1 to 16:
          if q-note.noteln[q-idx] = " " then leave.
          if q-note.noteln[q-idx] = apss.edipartner then
            do:
            assign poeh.resaleno = pder.user2.
            leave.
          end.
        end. /* q-idx = 1 to 16 */
      end. /* each q-note */
    end. /* avail apss */
      
    else if avail apsv then
      do:
      for each q-note where q-note.cono = g-cono and
                            q-note.notestype = "zz" and
                            q-note.primarykey = "DOPO Quote"
                            no-lock:
        do q-idx = 1 to 16:
          if q-note.noteln[q-idx] = " " then leave.
          if q-note.noteln[q-idx] = apsv.edipartner then
            do:
            assign poeh.resaleno = pder.user2.
            leave.
          end.
        end. /* q-idx = 1 to 16 */
      end. /* each q-note */
    end. /* else */
  end. /* avail pder */
/*    tah 011007 */


  /* das - when OE is changed after a PO has been acknowledged, don't allow
           OEET to change duedt, reqshipdt and expshipdt.                    */
  if h-duedt     <> ? and
     h-reqshipdt <> ? and
     h-expshipdt <> ? then do:
     find last z-poel where z-poel.cono      = g-cono and
                            z-poel.pono      = b-oeel.orderaltno and
                            z-poel.lineno    = b-oeel.linealtno and
                            z-poel.transtype = "DO"
                            no-error.
     if avail z-poel then
        assign z-poel.duedt     = h-duedt
               z-poel.reqshipdt = h-reqshipdt
               z-poel.expshipdt = h-expshipdt.
  end.
  else do:
     /* ontime */
     find last z-poel where z-poel.cono      = g-cono and
                            z-poel.pono      = b-oeel.orderaltno and
                            z-poel.lineno    = b-oeel.linealtno and
                            z-poel.transtype = "DO"
                            no-error.
     if avail z-poel then
        assign z-poel.duedt     = poeh.duedt
               z-poel.reqshipdt = poeh.duedt
               z-poel.expshipdt = poeh.duedt.
  end.
  
  /*das - 8/26/19
   Default Sauer/Sundstrand Discount Contract on EDI PO's (shipto 2)
   to get long-lead discount    
  */
  if poeh.resaleno = " " and poeh.transtype = "do" and
                             ((poeh.vendno = 72200014 and 
                               poeh.shipfmno = 2)     or 
                              (poeh.vendno = 70400588 and 
                               poeh.shipfmno = 7)     or 
                              (poeh.vendno = 70400588 and 
                               poeh.shipfmno = 8)     or 
                              (poeh.vendno = 10009856016 and 
                               poeh.shipfmno = 2))      
                             then
    assign poeh.resaleno = "135891".
/{&user_pr}*/


/{&user_availpo}*/
/{&user_availpo}*/

/{&user_aftpdsvcs}*/
/{&user_aftpdsvcs}*/


