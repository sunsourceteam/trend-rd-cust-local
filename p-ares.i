/*h*****************************************************************************
  INCLUDE      : p-ares.i
  DESCRIPTION  : A/R Statement Print - Create report detail records
  USED ONCE?   : no
  AUTHOR       : r&d
  DATE WRITTEN :
  CHANGES MADE : 07/13/92 pap; TB#  7191 Customer # change Use c12 instead of
                   de12d0 for cust #
                 04/14/93 mrw; TB# 10499 change the transcd for the pace
                   retrofit allowing the py record now to be an 11 and 3
                   will become an unapplied cash record
                 05/24/93 jrg; TB#  3645 Added seqno so statements will print in
                   the same order everytime.
                 04/27/94  bj; TB# 15279 Discount amount is wrong in report file
                 01/03/95 fef; TB# 16836 Amount on type 7 (payment) needs to be
                   negative.
                 03/29/00 rgm; TB# 3-2  Add RxServer Interface to Standard
                 02/27/01 gwk; TB# e8019 Use ares hook instead of ares1
                 03/02/01 gfk; TB# e7925 Use separate for each ARET loops for
                   detail records to improve performance using the index for
                   statustype where possible putting more common logic here
                 03/29/01 gfk; TB# e7925 Change report file to a temp table and                    add shared variable for temp table recid (s-reportid)
                 06/08/01 bm;  TB# e9014 Remove Balance Forward Option.
           *******************************************************************************/


         /* General Qualifications         */
           if  not p-futinvfl      and
               aret.transcd = 11   and
               aret.statustype     and
               aret.period = 0 then next.

           if not aret.statustype then
              if (aret.transcd = 11 and can-do("0,9",string(aret.paymtcd))) or
                  (aret.transcd = 5 and aret.paymtcd = 9)
                   then next.
 
         /* Inactive but reversal or no payment or credit */

            create report.
            assign report.reportnm = sapb.reportnm
                   report.custno   = aret.custno
                   report.c12      = {c-c12.i &custno = "aret.custno"}
                   /** SX30 Sunsource **/
                   report.cono     = aret.cono
         /*tb 16836 01/03/95 fef - added if transcd = 7 set amount to
                negative.  payments were getting added into the balance due
                amount on 'balance forward' statement types. */

                   report.de9d2s   = if aret.transcd = 11 and not 
                                        aret.statustype then 
                                          aret.paymtamt
                                     else if aret.transcd = 7 then
                                          aret.amount * -1
                                     else aret.amount
                   report.de9d2s-2 = if aret.statustype = true
                                        then aret.origdisc
                                     else aret.discamt
                   report.c1       = if aret.transcd = 7 then "p"
                                     else
                                     if aret.transcd = 3 then "u"
                                     else
                                     if aret.transcd = 5 then "m"
                                     else
                                     if aret.transcd = 6 then "c"
                                     else
                                        "i"
                   report.i7       = if can-do("3,7",string(aret.transcd)) then
                                        9999999
                                     else aret.invno
                   report.i3       = aret.invsuf

       /*tb 3645 05/24/93 jrg; Added seqno to print order. */
                   report.i4       = aret.seqno
                   report.dt       = aret.invdt
                   report.dt-2     = aret.duedt
                   report.dt-3     = aret.discdt
                   report.dt-4     = aret.paymtdt
                   report.c24      = aret.refer
                   report.c20      = "Chk # " + string(aret.checkno)
                   report.c2       = if aret.transcd = 11 then
                                        v-type[aret.sourcecd + 1]
                                     else
                                        v-type[aret.transcd + 1]
                   report.c6       = if aret.transcd = 11 and aret.statustype                                      then
                                      (if aret.period = 0 then "FUTURE"
                                       else
                                          "DUE")
                                     else
                                     if can-do("3,5",string(aret.transcd)) and
                                        aret.statustype then
                                        "ACTIVE"
                                     else
                                     if can-do("3,5",string(aret.transcd)) and
                                        not aret.statustype then "APPLD" 
                                     else
                                     if aret.transcd = 11 and aret.paymtcd = 8
                                        then "FLRPLN"
                                     else
                                     if aret.transcd = 11 and not                                         aret.statustype
                                        then "PAID"
                                     else
                                     if aret.transcd = 7 then "PAID" 
                                     else
                                       "INACT"
                   report.c13      = aret.shipto
                   report.l        = false
                   report.rid      = recid(aret)
                   report.outputty = "d".

                /*tb 3-2 03/29/00 rgm; Rxserver interface hook for extra vars */
                /*tb 8019 02/26/01 gwk; Use ares hook instead of ares1 */
                  {ares.z99 &after_reportassign = "*"}

                /*tb 25756 03/04/99 sbr; Added "and (aret.transcd <> 3 or
                arsc.statementty = "b")" to the if condition. */
               /*tb# e9014 06/08/01 bm;  Remove Balance Forward check.*/
            if not p-demandfl and aret.transcd <> 3
               then do:
               find b-aret where recid(b-aret) = recid(aret)
                    exclusive-lock no-error.
               assign b-aret.stmtfl[1] = true
                      b-aret.stmtfl[2] = if b-aret.statustype = false then true
                                         else b-aret.stmtfl[2].
               {t-all.i b-aret}
               end.


