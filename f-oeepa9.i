/* f-oeepa1.i */
/* f-oeepa1.i 1.1 01/03/98 */
/* f-oeepa1.i 1.6 04/16/93 */
/*h*****************************************************************************
  INCLUDE      : f-oeepa1.i
  DESCRIPTION  : Forms & defines for oeepa1 and oeepaf1
  USED ONCE?   : No (oeepa1.p & oeepaf1.p)
  AUTHOR       :
  DATE WRITTEN : 04/03/92
  CHANGES MADE :
    04/03/92 pap; TB#  6219 Line DO - add DO line indicator
    11/11/92 mms; TB#  8561 Display product surcharge label from ICAO
    04/08/93 jlc; TB# 10155 Display customer products.
    04/13/93 jlc; TB#  7245 Incorrect backorder qty.
    11/01/95 gp;  TB# 18091 Expand UPC Number (T24).  Add s-upcpno.
    03/20/96 kr;  TB# 12406 Variables defined in form, moved to calling programs
    03/29/00 rgm; TB# 3-2  Add RxServer Interface to Standard
    11/05/01 ajw; TB# e10078 OE Usability - Print OEELC Comments.
    02/08/02 dls; TB# e11973 OE Esability - New Subtotal Logic
    10/10/02 jbt; TB# e11863 Remove frame f-notes.  Frame is now defined in
                             generic notes printing program - notesprt.p.
    06/10/09 das; NonReturn/NonCancelable
*******************************************************************************/

{f-blank.i}

/*tb 3-2 03/29/00 rgm; Rxserver interface display vars for rxserver */
/* detail line and prod descrip */
form
    s-lineno                             at   1
    oeel.shipprod                        at   5
    /* SX55 
    s-upcpno                             at  30
    */
    s-qtyord                             at  38
    s-qtybo                              at  51
    s-qtyship                            at  64
    oeel.unit                            at  76
    /* SX55 */
    s-price                              at  80
    s-prcunit                            at  96
/*     s-discpct                            at 102 */
    x-shipdt                             at 108
    s-netamt                             at 117
    s-sign                               at 130
    s-lasterisk                          at 131       /* das - NonReturn */
    &IF DEFINED(rxserver) <> 0 &THEN
        s-rxline1         at 132
    &ENDIF
    skip

    s-descrip                            at   5
with frame f-oeel no-box no-labels no-underline down
           &IF DEFINED(rxserv_width) = 0 &THEN
               width 132.
           &ELSE
               width {&rxserv_width}.
           &ENDIF

/*Form to display additional Comments*/
form
    s-descrip            at 8
with frame f-comments no-box down no-labels no-underline.

/* form to display core charge */
form
    oeel.shipprod                        at   5
    s-price                              at  81
    s-netamt                             at 117
with frame f-oeel1 no-box no-labels no-underline down width 132.

form
    v-reqtitle                           at   5
    s-prod                               at  23
with frame f-oeelc no-box no-labels width 132.

form
    "** CORE RETURN **"                  at   8
with frame f-oeelr no-box side-labels width 132.

form
    "** DIRECT ORDER **"                 at   8
with frame f-oeeldo no-box side-labels width 132.

form
    "Kit"                                at   1
    oeelk.instructions                   at   5
with frame f-refer no-box no-labels width 132.

form
with frame f-com no-box no-labels col 8 width 80.

/* SX55 */
form
with frame f-notes no-box no-labels col 8 width 80.

/* TB# 11973 02/11/02 new subtotal logic */
form
    "============="                      at 117
/* SX55 */
    "Subtotal:"                          at 107
/*    s-subtotdesc                         at 93 */
    v-subnetdisp                         at 117

    skip(1)
with frame f-oeelsub no-box no-labels no-underline width 132.



