/* SX 55
   d-oeic.i */
{w-oeeh.i oeel.orderno oeel.ordersuf no-lock}

assign 
    s-descrip  = ""
    s-core     = if oeel.restockamt <> 0 then
                     "Restock Amt " + string(oeel.restockamt,">>>>>>>9.99-")
                     + (if oeel.restockfl = yes then "$" else "%")
                 else if oeel.corechgty = "y" then
                     "Core Charge " + string(oeel.corecharge,">>>>>>>>9.99-")
                 else if oeel.corechgty = "r" then
                     "Core Return " + string(oeel.corecharge,">>>>>>>>9.99-")
                 else if oeel.kitfl then "Built on Demand Kit"
                 else ""
    s-alttxt   = ""
    s-com      = if oeel.commentfl then "c" else ""
    s-jitflag  = if oeeh.orderdisp = "j" then
                     if oeeh.stagecd <= 1 then
                         if oeeh.reqshipdt = oeel.reqshipdt  and
                             oeeh.promisedt = oeel.promisedt 
                             then ""
                         else "j"
                     else if oeel.jitpickfl = yes then ""
                     else if oeeh.reqshipdt = oeel.reqshipdt and
                         oeeh.promisedt = oeel.promisedt then ""
                     else "j"
                 else ""
    s-rushdesc = if oeel.rushfl then "Rush" else ""
    s-alttxt   = if oeel.altwhse ne "" then "(From: " + oeel.altwhse + "}"
                 else if oeel.xrefprodty = "s" then "(Substitute)"
                 else if oeel.xrefprodty = "u" then "(Upgrade)"
                 else if oeel.xrefprodty = "p" then "(Supersede)"
                 else ""
    s-netamt   = oeel.price - oeel.discamt - oeel.discamtoth *
                     (if oeel.returnfl = yes then -1 else 1)
    s-botype   = if oeel.botype = "d" then "d" else "".

display "" @ icsp.notesfl with frame f-oeic.

if oeel.specnstype ne "n" then do:
    {w-icsp.i oeel.shipprod no-lock}

    if avail icsp then do:
        s-descrip = icsp.descrip[1].

        display icsp.notesfl with frame f-oeic.
    end.

    else s-descrip = v-invalid.
end.

else s-descrip = oeel.proddesc.
/* SX 55 */
/* display customer product xref in OEIC */
find icsec use-index k-altprod where icsec.cono     = g-cono         and
                                     icsec.altprod  = oeel.shipprod  and
                                     icsec.rectype  = "c"            and
                                     icsec.custno   = oeeh.custno
                                     no-lock no-error.
if avail icsec then do:
   assign substring(s-core,1,2)  = "x:".
   assign substring(s-core,3,23) = icsec.prod.
end.
/* display customer product xref */ 
/* SX 55 */
display
    s-com
    oeel.lineno
    oeel.specnstype
    s-botype
    oeel.shipprod
    oeel.unit
    s-core
    s-alttxt
    s-rushdesc
    s-jitflag
    s-descrip
    s-netamt when sasoo.oepricefl ne "n"
with frame f-oeic.

if oeel.bono ne 0 then
   display oeel.bono with frame f-oeic.
else
   display "  " @ oeel.bono with frame f-oeic.

if oeel.returnfl = yes then
    display
        (oeel.qtyord * -1) @ oeel.qtyord
        (oeel.qtyship * -1) @ oeel.qtyship
    with frame f-oeic.

else
    display
        oeel.qtyord
        oeel.qtyship
     with frame f-oeic.