define var dd         as integer format "9".
define var zx         as integer format "99".
define var counter2   as integer format "9999".
define var file_cat   as character format "x(17)".
define var file_other as character format "x(13)".
define var full_cat   as character  format "x(24)".
define var full_other as character  format "x(20)".
define var asterisk   as character  format "x(1)" init "*".
define var destfile   as character format "x(70)".  
define var destfile2  as character format "x(70)".  
define var d-filenm   as char format "x(13)". 
define var namechar   as char format "x(13)".


define frame f-menu 
       skip(3) 
       "This Program will only Delete files called edi???_o.app#" at 13
       "The # will designate the 0-100 or 1-200 or 3-365 number " at 13
       "of days to delete. Please be careful. Once deleted, they" at 13
       "are gone for good!!"                                      at 13
       skip(2)
       "Filename(s) to delete?? " at 16
       d-filenm                at 40  
       skip(3) 
       with no-labels width 80
            title "EDI sentitems directory cleanup - Delete with Caution!".
        
main:
   do while true: 

    update d-filenm 
      with frame f-menu.

    do zx = 1 to 13:
       assign namechar = substring(d-filenm,zx,1).
       if namechar = "*" then 
         do: 
           message "Filename cannot include asterisk as wildcard." d-filenm.
           message "Pls re-transmit...".
           pause.
           next main.
         end.
    end.
    
    if length(d-filenm) ne 13 then 
      do: 
           message "Filename cannot be less than 13 characters." d-filenm.
           message "Pls re-transmit...".
           pause.
           next main.
         end.
    
    if d-filenm ne "" then 
      do:
        assign full_other = d-filenm  /* string(dd,"9") */ + asterisk.
        substring (destfile2,1,41,"character") =
             "/usr/sps/spedi/sentitems/" +  full_other.
        message "Delete these files ????? "  + destfile2 
                 view-as alert-box question buttons yes-no
                 update q as logical.
        if q then 
           message "Your sure now !! Delete these files ????? " + destfile2 
                 view-as alert-box  question buttons yes-no
                 update m as logical.
           if m then 
             do: 
                put screen row 21 col 3 color message
 "Selected files are being deleted - pls wait.....                            ".
                pause 5.
                put screen row 21 col 3 color message
 "                                                                            ".
                  os-command cd /usr/sps/spedi/sentitems.
                  os-command ( "rm"  value(destfile2)).
  
             end.
      end.
    else 
        do: 
           message "Please enter file name and re-transmit...".
           pause.
        end.
assign d-filenm = " ".
leave.
end.

