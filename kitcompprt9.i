/*kitcompprt9 based on 1.i 07/11/00*/
/*h*****************************************************************************
PROCEDURE   : kitcompprt9.i
DESCRIPTION : Print components from Work Order for WT BOD kit
AUTHOR      : smi
DATE WRITTEN: 07/19/00
CHANGES MADE:
    07/19/00 smi; TB# e5537 WT BOD Project - created new
    07/25/00 des; TB# e5061 Inventory Allocation - Adjust qtyfmrcvs on a
        reprint.
    01/22/01 mms; TB# e7644 WT BOD Proj Missing param and misc code rework.
    02/22/02 tmg: TB# e10010 RBC Back Order project roll from 3.0.006
    04/01/02 mwb; TB# e3207 Added logic for Catalog Notes print. OE and WT
        Pick tickets.
    10/10/02 jbt; TB# e11863 Call generic notes printing program.
    12/10/02 gkd; TB# L963 Send all lines to TWL when printing changes only
    12/11/03 blr; TB# e18723 NS component description line two was not printing
    03/07/05 blw; TB# L1692 - Add sequence for printing cartons on kits.
    03/18/05 blw; TB# L1678 - Add truck pallet printing support.
    07/06/05 mwb; TB# L1685 - removed 'cs' edit to create wlelk records.
        The 'cs' edit was removed at line level via L1379 - missed comps.
    12/16/05 blw; TB# L1873 - Add optional component types to WLIT creation.

    11/21/2008 dkt change some &line to oeelk.
    01/29/2009 dkt comment out f-cartonline2.
    02/02/09   dt01 findfirstbin
    02/12/09   dt01 labor does not have a bin
    03/17/09   tah  kit kit component prints******************************************************************************/

if oeelk.comptype = "r" then do:
  display oeelk.instructions with frame f-refer.
  down with frame f-refer.
  next.
end.
else do:
  if not v-wlupdtype = "WL-Change":u then do:
    assign v-wmfl = no.
    /*tb 18575 06/22/95 ajw; Nonstock/Special Kit Component.*/
    /* if oeelk.specnstype <> "n" then do:  dkt 12/16/2008 */
        {w-icsw.i oeelk.shipprod v-whse no-lock}
        {w-icsp.i oeelk.shipprod no-lock}
    /* end. I need this to fail to find if not there 12/16/2008 */

    /*tb 18091 11/01/95 gp;  Assign product section of UPC number */
    if oeelk.specnstype = "n" or oeelk.arpvendno <> 0 then
        {icsv.gfi "'u'" oeelk.shipprod oeelk.arpvendno no}
    else if avail icsw then
        {icsv.gfi "'u'" oeelk.shipprod icsw.arpvendno no}

    assign
        s-upcpno    = if avail icsv then
                          {upcno.gas &section  = "4"
                                     &sasc     = "v-"
                                     &icsv     = "icsv."
                                     &comdelim = "/*"}
                      else "00000".


    /* Comment out the WM specific logic. */
    &if "{&comwmout}" <> "out":u &then
    if (oeelk.specnstype <> "n") and
       (avail icsw and icsw.wmfl) then do:

        find first wmet use-index k-ord where
            wmet.cono = oeelk.cono          and
            wmet.ordertype = "o"            and
            wmet.orderno   = oeelk.orderno  and
            wmet.ordersuf  = oeelk.ordersuf and
            wmet.lineno    = oeelk.lineno   and
            wmet.seqno     = oeelk.seqno no-lock no-error.

        v-wmfl = if avail wmet then yes else no.

    end. /* if avail icsw and wm */

    if not v-wmfl or oeelk.specnstype = "n" then do:
    &endif /* &comwmout */

      /* dt01 02/02/09 findfirstbin      */

      
      run findfirstbin.p(input g-cono, input {&whse}, 
                     /* input {&line}.shipprod, 03-24-09 */
                     input oeelk.shipprod,
                     output wmsbid, output wmsbpid).
      
      if wmsbid ne ? and wmsbpid ne ? then do:
        find wmsbp where recid(wmsbp) = wmsbpid no-lock no-error.
        find wmsb  where recid(wmsb)  = wmsbid  no-lock no-error.
      end.

      if not avail (wmsb) or not avail (wmsbp) then do:
        release wmsb.
        release wmsbp.
      end.

      /*d Print the component  */
      /*tb 18575 06/22/95 ajw; Nonstock/Special Kit Component.*/
      assign
                   /* dt01 02/12/09 labor */
        s-binloc = if (avail (icsp) and icsp.statustype = "L") then ""
                   else if /* dkt 12/16/2008 */
                   avail wmsbp and wmsbpid <> ? then wmsbp.binloc
                    /* end */
                   else if {&altwhse} ne "" then "Whse: " + {&altwhse}
                   else if avail icsw then string(icsw.binloc1,"xx/xx/xxx/xxx")
                   else if avail wmsb and wmsbid <> ? then 
                       string(wmsb.binloc,"xx/xx/xxx/xxx")   
                   else if oeelk.specnstype = "n" then "Non Stock"
                   else ""
        v-kitconv = {unitconv.gas &decconv =
                      "({&line}.stkqtyord / {&line}.qtyord)"}.

      clear frame f-oeel.

      assign x-qtybo =  ((oeelk.qtyord * 
                           (if {&returnfl} = yes then -1
                            else 1)) -
                         (oeelk.qtyneeded * v-qtyship * v-kitconv)).

      {oeepp9.z99 &before_complinedisplay = "*"
            &comr        = "{&comr}"
            &coms        = "{&coms}"
            &returnfl    = "{&returnfl}"
            &cubes       = "{&cubes}"
            &weight      = "{&weight}"
            &head        = "{&head}"
            &line        = "oeelk"
            &section     = "{&section}"
            &ourproc     = "{&ourproc}"
            &specnstype  = "oeelk.specnstype"
            &altwhse     = "{&altwhse}"
            &vendno      = "oeelk.arpvendno"
            &kitfl       = "{&kitfl}"
            &botype      = "{&botype}"
            &orderdisp   = "{&orderdisp}"
            &prevqtyship = "oeelk.prevqtyship"
            &netamt      = "{&netamt}"
            &prtpricefl  = "{&prtpricefl}"
            &bono        = "{&bono}"
            &orderno     = "{&orderno}"
            &ordersuf    = "{&ordersuf}"
            &custno      = "{&custno}"
            &corechgty   = "{&corechgty}"
            &qtybo       = "x-qtybo"
            &ordertype   = "{&ordertype}"
            &prefix      = "{&prefix}"
            &seqno       = "oeelk.seqno"
            &stkqtyshp   =  "oeelk.qtyneeded * v-qtyship * v-kitconv" 
            &nosnlots    = "{&nosnlots}"
            &comtype     = "{&comtype}"
            &comlineno   = "{&comlineno}"
            &shipto      = "{&shipto}"
            &twlstgedit  = "{&twlstgedit}" }

      /* PickPack */

      /* Comment out the OE specific logic when coming from areas such as
           VA that might cause compile issues or where the fields are module
           specific. */
      &if "{&comoeout}" <> "out":u &then
        if oeelk.qtyfmrcvs <> 0 then
            s-qtyfmrcvs = "  Qty From Receivers:     " +
                            (if (oeelk.qtyfmrcvs >
                                (oeelk.qtyneeded * v-qtyship *
                                 v-kitconv))
                             then
                                string((oeelk.qtyneeded *
                                        v-qtyship * v-kitconv),
                                       ">>>>>>9.99")
                             else
                                string(oeelk.qtyfmrcvs,">>>>>>9.99")).
        else
            s-qtyfmrcvs = "".
      &endif /* &comoeout */

      if icsdAutoPck = "y" and       
         icsdAutoXdock = "Y" then do:
         
        find first zsdipckdtl use-index k-ordix where 
               zsdipckdtl.cono = g-cono              and
               zsdipckdtl.ordertype = "{&ordertype}" and
               zsdipckdtl.whse = {&whse}             and 
               zsdipckdtl.orderno = {&orderno}       and
               zsdipckdtl.ordersuf = {&ordersuf}     and
               zsdipckdtl.lineno = {&line}.lineno    and
               zsdipckdtl.seqno  = integer(oeelk.seqno)          and
               zsdipckdtl.binloc1 = "RECEIVING" no-lock no-error.
        if not avail zsdipckdtl then
          find first zsdipckdtl use-index k-ordix where 
               zsdipckdtl.cono = g-cono              and
               zsdipckdtl.ordertype = "{&ordertype}" and
               zsdipckdtl.whse = {&whse}             and 
               zsdipckdtl.orderno = {&orderno}       and
               zsdipckdtl.ordersuf = {&ordersuf}     and
               zsdipckdtl.lineno = {&line}.lineno    and
               zsdipckdtl.seqno  = integer(oeelk.seqno)        
               no-lock no-error.
      
        if avail zsdipckdtl then do:
          s-binloc2 = zsdipckdtl.binloc2.
          if zsdipckdtl.binloc1 = "RECEIVING" then
            s-binloc = zsdipckdtl.binloc1.
          else
            s-binloc = string(zsdipckdtl.binloc1,"xx/xx/xxx/xxx").
        end.
      end. /* autoFlags */

      {w-icsw.i oeelk.shipprod oeelk.whse no-lock}
      {w-icsp.i oeelk.shipprod no-lock}                                

      run findfirstbin.p(input g-cono, input {&whse}, 
                        /* input {&line}.shipprod, 03-24-09 */
                         input oeelk.shipprod,
                         output wmsbid, output wmsbpid).
    
      if wmsbid ne ? and wmsbpid ne ? then do:
         find wmsbp where recid(wmsbp) = wmsbpid no-lock no-error.
         find wmsb  where recid(wmsb)  = wmsbid  no-lock no-error.
      end.

      if not avail (wmsb) or not avail (wmsbp) then do:
        release wmsb.
        release wmsbp.
      end.

/*   tah 031709 */
 


        s-binloc2 = "".
        if s-binloc ne "RECEIVING" then      /* dt01 02/12/09 labor */
        s-binloc = if (avail (icsp) and icsp.statustype = "L") then ""
                   else if /* dkt 12/16/2008 */
                   avail wmsbp and wmsbpid <> ? then                       
                       string(wmsbp.binloc,"xx/xx/xxx/xxx")
                    /* end */
                   else if {&altwhse} ne "" then "Whse: " + {&altwhse}
                   else if avail icsw then string(icsw.binloc1,"xx/xx/xxx/xxx")
                   else if avail wmsb and wmsbid <> ? then
                      string(wmsb.binloc,"xx/xx/xxx/xxx")   
                   else if oeelk.specnstype = "n" then "Non Stock"
                   else "".
        else
        s-binloc2 =  if (avail (icsp) and icsp.statustype = "L") then ""
                   else if /* dkt 12/16/2008 */
                   avail wmsbp and wmsbpid <> ? then 
                   string(wmsbp.binloc,"xx/xx/xxx/xxx")
                    /* end */
                   else if {&altwhse} ne "" then "Whse: " + {&altwhse}
                   else if avail icsw then string(icsw.binloc1,"xx/xx/xxx/xxx")
                   else if avail wmsb and wmsbid <> ? then
                      string(wmsb.binloc,"xx/xx/xxx/xxx")   
                   else if oeelk.specnstype = "n" then "Non Stock"
                   else "".
                    
       display
            oeelk.shipprod   @ {&line}.shipprod
            (oeelk.qtyord * if {&returnfl} = yes then -1
                            else 1) @ s-qtyord
            (if oeelk.ovshipfl then oeelk.qtyship else
                oeelk.qtyneeded * v-qtyship * v-kitconv) @ s-qtyship
            oeelk.unit       @ {&line}.unit
            s-upcpno
            s-binloc
            s-binloc2
            "Kit"            @ s-lineno
            "____________ _____" @ s-underline
            &if "{&comoeout}" <> "out":u &then
            s-qtyfmrcvs @ s-descrip2
            &endif
      with frame f-oeel.

      /*tb 18575 06/22/95 ajw; Nonstock/Special Kit Component.*/
      /**   dkt 12/17/2008
      if (oeelk.specnstype <> "n") and
         (avail icsw and icsw.binloc2 ne "") then
            display icsw.binloc2 @ s-binloc2 with frame f-oeel.
      **/

      /*tb 18575 06/22/95 ajw; Nonstock/Special Kit Component.*/
      /*tb 9731 10/21/98 jgc; Expand non-stock description.
            Added second display line for proddesc2. */
      if oeelk.specnstype = "n" then do:
            display oeelk.proddesc @ s-descrip with frame f-oeel.
            if oeelk.proddesc2 ne "" then do:
                display oeelk.proddesc2 @ s-descrip3
                    with frame f-oeel2.
                down with frame f-oeel2.
            end.
      end.
      else
        if not avail icsp or not avail icsw then
          display v-invalid @ s-descrip with frame f-oeel.
        else
          display icsp.descrip[1] @ s-descrip with frame f-oeel.

      down with frame f-oeel.

      /*tb 18575 06/22/95 ajw; Nonstock/Special Kit Component.*/
      /*tb 19484 09/16/96 mwb; added the down with frame line and
            the 'do' and 'end' to go with it. */
      if oeelk.specnstype <> "n" and
        avail icsp and icsp.descrip[2] ne "" then do:
        display icsp.descrip[2] @ s-descrip3 with frame f-oeel2.
        down with frame f-oeel2.
      end.

      /*tb 19484 09/16/96 mwb; added displaying of the counter bin
             location for WL.  Can setup a product with a primary
             counter bin location.  Needs to display with the other
             bins for Primary Picking locations from WL */
      if v-wlwhsefl = yes and {&head}.transtype = "cs"
        then do for wlicsw:

        {wlicsw.gfi &prod = "oeelk.shipprod"
                    &whse = "{&line}.whse"
                    &lock = "no"}

        if avail wlicsw and wlicsw.bincntr ne "" then
          display "            Counter Bin:" @ s-descrip3
            wlicsw.bincntr @ s-binloc3 with frame f-oeel2.

        /*tb 22380 12/26/96 mwb; added blank cs bin location */
        else
          display "            Counter Bin:" @ s-descrip3
                  "_____________" @ s-binloc3
                  with frame f-oeel2.

        down with frame f-oeel2.
      end.
      /* end 19484 - CS Bin Location Print */

      /* Comment out WM specific logic.*/
      &if "{&comwmout}" <> "out":u &then
    end. /* not wm display */

    /*d WM logic for the component */
    else do:
      assign
        v-frameln = 1
        s-binloc  = "Whse Mgr"
        v-conv    = 1
        v-kitconv = {unitconv.gas &decconv =
                            "({&line}.stkqtyord / {&line}.qtyord)"}.

      if avail icsp then do:
            {p-unit.i &prod       = "oeelk.shipprod"
                      &unitconvfl = "icsp.unitconvfl"
                      &unitstock  = "icsp.unitstock"
                      &unit       = "oeelk.unit"
                      &desc       = "v-desc"
                      &conv       = "v-conv"
                      &confirm    = "confirm"}.
      end.

      /* clean finds */
      /* SX_55 dkt 11/20/2008 */
      if oeelk.specnstype = "n" then do:                                      
        for each wmsbp where wmsbp.cono = 500 + g-cono and                 
        wmsbp.whse = {&whse} and wmsbp.prod = oeelk.shipprod no-lock,    
        each wmsb where wmsb.cono = wmsbp.cono and wmsb.whse = wmsbp.whse  
        and wmsb.binloc = wmsbp.binloc no-lock break by wmsb.priority:
          leave.
        end.
  
        {w-icsw.i oeelk.shipprod oeelk.whse" no-lock}
        {w-icsp.i oeelk.shipprod no-lock}                                

      end. 
      else do:
        find icsw where icsw.cono = g-cono and
        icsw.whse = oeelk.whse and icsw.prod = oeelk.shipprod no-lock no-error.
  
        find icsp where icsp.cono = g-cono and
        icsp.prod = oeelk.shipprod no-lock no-error.
      end.                                                                

      /*tb 18091 11/01/95 gp;  Change UPC number display */
      display
            oeelk.shipprod   @ {&line}.shipprod
            (oeelk.qtyord *
                if {&returnfl} = yes then -1 else 1) @ s-qtyord
            (if oeelk.ovshipfl then oeelk.qtyship else
                oeelk.qtyneeded * v-qtyship * v-kitconv) @ s-qtyship
            oeelk.unit       @ {&line}.unit
            s-upcpno
            s-binloc
            "Kit"            @ s-lineno
            "____________ _____" @ s-underline
      with frame f-oeel.

      for each wmet use-index k-ord where
            wmet.cono      = oeelk.cono     and
            wmet.ordertype = "o"            and
            wmet.orderno   = oeelk.orderno  and
            wmet.ordersuf  = oeelk.ordersuf and
            wmet.lineno    = oeelk.lineno   and
            wmet.seqno     = oeelk.seqno
      no-lock:

        if v-frameln = 1 then do:

                assign
                    s-wmqtyship = wmet.qtyactual / v-conv
                    s-wmqtyship = if {&returnfl} = yes or
                                      oeelk.qtyneeded < 0 then
                                      s-wmqtyship * -1
                                  else if s-wmqtyship < 0 then 0
                                  else s-wmqtyship
                    s-descrip2  = "                        " +
                                     string(s-wmqtyship,">>>>>>>>9.99-")
                    s-binloc2   = wmet.binloc.

                display
                    icsp.descrip[1] @ s-descrip
                    s-binloc2
                    s-descrip2
                with frame f-oeel.
                down with frame f-oeel.

                v-frameln = v-frameln + 1.
        end. /* frameln 1 */

        else do:
                assign
                    s-wmqtyship = wmet.qtyactual / v-conv
                    s-wmqtyship = if {&returnfl} = yes or
                                      oeelk.qtyneeded < 0 then
                                      s-wmqtyship * -1
                                  else if s-wmqtyship < 0 then 0
                                  else s-wmqtyship
                    s-binlocwm  = wmet.binloc.

                display
                    s-binlocwm
                    s-wmqtyship
                with frame f-oewm.
                down with frame f-oewm.

        end. /* else do */

      end. /* for each wmet */

    end. /* wm logic for the component */
    &endif /* &comwmout */


    {oeepp9.z99 &before_comp_prodnotes = "*"
            &comr        = "{&comr}"
            &coms        = "{&coms}"
            &returnfl    = "{&returnfl}"
            &cubes       = "{&cubes}"
            &weight      = "{&weight}"
            &head        = "{&head}"
            &line        = "{&line}"
            &section     = "{&section}"
            &ourproc     = "{&ourproc}"
            &specnstype  = "{&specnstype}"
            &altwhse     = "{&altwhse}"
            &vendno      = "{&vendno}"
            &kitfl       = "{&kitfl}"
            &botype      = "{&botype}"
            &orderdisp   = "{&orderdisp}"
            &prevqtyship = "{&prevqtyship}"
            &netamt      = "{&netamt}"
            &prtpricefl  = "{&prtpricefl}"
            &bono        = "{&bono}"
            &orderno     = "{&orderno}"
            &ordersuf    = "{&ordersuf}"
            &custno      = "{&custno}"
            &corechgty   = "{&corechgty}"
            &qtybo       = "{&qtybo}"
            &ordertype   = "{&ordertype}"
            &prefix      = "{&prefix}"
            &seqno       = "{&seqno}"
            &stkqtyshp   = "{&stkqtyshp}"
            &nosnlots    = "{&nosnlots}"
            &comtype     = "{&comtype}"
            &comlineno   = "{&comlineno}"
            &shipto      = "{&shipto}"
            &twlstgedit  = "{&twlstgedit}" }

    /*d Print the component product notes  */
    /*tb 18575 06/22/95 ajw; Nonstock/Special Kit Component. */
    /*tb e11863 kitcompprt1.i is called from oeepp, wtep and vaepp.  Since
                they all print notes in column 8 and they all check notes
                print flag 1, we will not pass a function name to notesprt.p.
                This will force notesprt.p to use notes.printfl. */
    if oeelk.specnstype <> "n" and
      avail icsp and icsp.notesfl ne "" then
      run notesprt.p(g-cono,"p",icsp.prod,"","",8,1).

    /*tb e3207 Added Catalog notes display for non-stock. */
    else if oeelk.specnstype = "n":u
    then do:
        {icsc.gfi &prod = "oeelk.shipprod"
                  &lock = "no"}
        if avail icsc and icsc.notesfl ne "" then
            run notesprt.p(1,"g",icsc.catalog,"","",8,1).
    end.  /* Non-stock Catolog edit */

    /*d Print the component serial/lot records */
    /*tb 18575 06/22/95 ajw; Nonstock/Special Kit Component.*/
    /*tb 13356 04/27/95 mtt; Serial/Lot # doesn't print on PO/RM */
    if oeelk.specnstype <> "n" then do:
      {p-oeepp1.i &type       = "{&ordertype}"
                  &line       = "{&line}"
                  &pref       = "{&prefix}"
                  &seq        = oeelk.seqno
                  &stkqtyship = "{&line}.stkqtyship"
                  &snlot      = "{&nosnlots}"
                  &wono       = "{&wono}"
                  &wosuf      = "{&wosuf}"
                  &lineno     = "{&lineno}"}

      assign
            s-prod     = if oeelk.reqprod = "" then oeelk.shipprod
                         else oeelk.reqprod
            v-reqtitle = if oeelk.reqprod <> "" then "  Requested Prod:"
                         else     "   Customer Prod:".

      {n-icseca.i "first" s-prod ""c"" {&custno} no-lock}

      if avail icsec then do:
        if oeelk.reqprod <> "" then do:
          display
                    s-prod
                    v-reqtitle
          with frame f-oeelc.
          down with frame f-oeelc.
        end.

        assign
          s-prod     = icsec.prod
          v-reqtitle = "   Customer Prod:".

        display
          s-prod
          v-reqtitle
        with frame f-oeelc.

      end. /* avail icsec */
    end. /* End of Specnstype <> "n" */

  end. /* if not v-wlupdtype = "WL-Change":u then do: */

  /*tb 19484 08/20/96 gp; Create a WLELK record for each component
        except reference components */
  /*tb 19484 09/16/96 mwb; Added the CS edit per line.  No need to
        write the data to WL for a Counter Sale.  The interface moved
        to oeepi for a Stk Move out of oh-hand in WL. */
  /*tb 19484 10/03/96 des; Warehouse Logistics Only allow type "c"
        components to create a wlelk record transaction */
  /*tb 23455 07/17/97 mwb; Added stage edit for oeeh.stagecd < 3 */
  /*tb 24253 12/01/97 mwb; Added the tag and hold edit */
  /*tb 25120 09/10/98 gkd; Removed tag and hold edit */
  /*tb L1685 07/06/05 mwb; Removed 'cs' edit - was not creating wlelk
        records for cs order - L1379 removed 'cs' edit on line - missed
        the component level. */

  if v-wlwhsefl = yes and
        can-do("c,o":u,oeelk.comptype)
        {&twlstgedit}
  then do:

    /*d Create WLELK records for each component */
    {wlpproc.gpr &invoutcond    = "{&head}.transtype <> ""rm"" and
                                   {&returnfl} = no"
                     &wlsetno       = v-wlsetno
                     &whse          = oeelk.whse
                     &ordertype     = ""o""

                     &orderno       = oeelk.orderno
                     &ordersuf      = oeelk.ordersuf
                     &lineno        = oeelk.lineno
                     &seqno         = oeelk.seqno
                     &msdssheetno   = """"
                     &updtype       = ""A""
                     &shipmentid    = """"}

  end. /* end if v-wlwhsefl... */
end. /* else do */

/*tb L1211 04/22/03 blw; Added si mod wl009 & wl010 to standard */
&if "{&ordertype}":u = "o":u &then
  if v-modwlfl and v-wlwhsefl and p-cartonfl and
      can-find(first t-cartons where
                  t-cartons.orderno  = oeelk.orderno    and
                  t-cartons.ordersuf = oeelk.ordersuf   and
                  t-cartons.vaordseq = 0                and
                  t-cartons.lineno   = oeelk.lineno     and
                  t-cartons.lineseq  = oeelk.seqno)
  then do:

      /* Clear the display vars */
    do v-numprint = 1 to 2:
          assign
              s-cartons[v-numprint]   = ""
              s-cartonqty[v-numprint] = ""
              s-lineuom[v-numprint]   = ""
              s-cartonlbl[v-numprint] = ""
              s-qtylbl[v-numprint]    = "".
    end. /* do v-printnum = 1 to 2 */

    v-numprint = 1.

    for each t-cartons where
          t-cartons.orderno   = oeelk.orderno   and
          t-cartons.ordersuf  = oeelk.ordersuf  and
          t-cartons.vaordseq  = 0               and
          t-cartons.lineno    = oeelk.lineno    and
          t-cartons.lineseq   = oeelk.seqno
    no-lock
    break by t-cartons.orderno
            by t-cartons.ordersuf
            by t-cartons.vaordseq
            by t-cartons.lineno
            by t-cartons.lineseq
    by t-cartons.cartonid:

      if last-of(t-cartons.cartonid)
      then do:
              assign
                  s-cartons[v-numprint]   = t-cartons.cartonid
                  s-cartonqty[v-numprint] =
                      string(t-cartons.cartonqty,"zzzzzz9.99")
                  s-lineuom[v-numprint]   = t-cartons.lineuom
                  s-cartonlbl[v-numprint] =
                      (if t-cartons.cartonid begins "C":U then
                           "Carton #:"
                       else
                           "Pallet #:")
                  s-qtylbl[v-numprint]    = "Qty:"
                  v-numprint              = v-numprint + 1.
      end. /* last of t-cartons.cartonid */


      if (v-numprint > 2 or last-of(t-cartons.lineno))
      then do:
         /* dt01 01/29/2009  do with frame f-cartonline2:      


              display
                  s-cartonlbl[1]
                  s-cartons[1]
                  s-qtylbl[1]
                  s-cartonqty[1]
                  s-lineuom[1]
                  s-cartonlbl[2]
                  s-cartons[2]
                  s-qtylbl[2]
                  s-cartonqty[2]
                  s-lineuom[2]               
              with frame f-cartonline2.
              down with frame f-cartonline2. 
         end. 
         dt01 */
            
                          
              /* Reset the vars */
              do v-numprint = 1 to 2:
                  assign
                      s-cartons[v-numprint]   = ""
                      s-cartonqty[v-numprint] = ""
                      s-lineuom[v-numprint]   = ""
                      s-cartonlbl[v-numprint] = ""
                      s-qtylbl[v-numprint]    = "".
              end. /* do v-numprint = 1 to 2 */

              v-numprint = 1.

      end. /*  v-numprint = 2 or (last-of(t-cartons.orderno) and ... */
    end. /* for each t-cartons */
  end. /* v-modwlfl, v-wlwhsefl, p-cartonfl and can-find(first t-cartons) */
&endif
