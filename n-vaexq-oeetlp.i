/* n-oeetlp.i 1.2 05/07/98 */
/*h*****************************************************************************
  INCLUDE     : n-vaexq-oeetlp.i
  DESCRIPTION : PDSC lookup of pricing records for OE line item pricing
                modification for Service pricing
  USED ONCE?  : no
  AUTHOR      : tah
  DATE WRITTEN: 06/17/14
  CHANGES MADE:
    05/18/93 kmw; TB# 11450 Enhance Pricing Performance - combined
        n-oeetlp,c,t.i into 1 include and reduced size.
        Added & index and & where
    10/27/93 kmw; TB# 13481 Use g-today in pricing rather than today
    06/18/96 bmf; TB# 21321 Added user hook to n-oeetlp.i
        (&user_before_price_calc)
    05/08/98 bp ; TB# 24911 Add user include &user_aft_price_calc to n-oeetlp.i
    06/05/00 twa; TB# e5258 Support pricing for nonstocks
    01/01/06 bp ; TB# e 25778 Add user hooks.
*******************************************************************************/

{n-oeetlp.z99 &user_bef_noeetlp = "*"}

/*d Find a pricing record using the appropriate key */
v-badrec = yes.

pdsc-loop:
for each b-pdsc use-index {&index} where
         b-pdsc.cono       = g-cono      and
         b-pdsc.statustype = yes         and
         b-pdsc.levelcd    = {&levelcd}  and
         {&where}
         b-pdsc.prod       = {&prod}     and
         /*tb 2311 12-10-91 kmw; Job Pricing- specific whse then "" */
         b-pdsc.whse       = {&whse}     and
         b-pdsc.units      = {&unit}     and
         b-pdsc.startdt    <= g-today    and
         /*tb 11450 05/18/93 kmw; Enhance Pricing Performance - add enddt
                                  and ignore quote checks */
         /*d If the end date is entered and past, disqualify the record */
        (b-pdsc.enddt      >= g-today    or
         b-pdsc.enddt       = ?)         and
         /*d If "ignore quote" is selected on the F12 price screen, don't accept
             any quote records */
       ((    can-do("i,il",s-priceclty) and b-pdsc.quoteno = "") or
        (not can-do("i,il",s-priceclty))) and

         /*d We Don't use base or list to price non-catalog nonstock items */
         not (
             b-pdsc.prctype = no               and
             can-do("b,l":u, b-pdsc.priceonty) and
             s-specnstype   = "n":u            and
             not avail b-icsc)

         no-lock:

    /*d Qualify that the record found is valid */
    {q-oeetlp.i "b-"}

    {n-oeetlp.z99 &user_aft_valid_check = "*"}

    if v-badrec then next  pdsc-loop.
    else             leave pdsc-loop.
end.   /* for each pdsc */

if not v-badrec then do:

    /*u User Hook */
    {n-oeetlp.z99 &user_before_price_calc = "*"}

    /*d Calculate the price/discount */
    run vaexq-oeetlpa.p.

    /*u User Hook */
    {n-oeetlp.z99 &user_aft_price_calc = "*"}

    /*d If this record is a promotional record, save into promo variables */
    if v-promofl then
        assign v-promofound = yes
               v-promoprice = v-price
               v-promodisc  = s-discamt
               v-pridpdsc   = recid(b-pdsc)
               v-hit        = 6.
    else leave pricing.
end.
