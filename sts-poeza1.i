/***************************************************************************
 PROGRAM NAME: sts-poeza1.i
  PROJECT NO.: 99-16
  DESCRIPTION: Screens & includes for poeza
 DATE WRITTEN:
       AUTHOR: Sir Carl E. Graul Esq.
     MODIFIED: 

***************************************************************************/
                         
def {1} var s-lineno     like poel.lineno.
def {1} var s-prod       like poel.shipprod .
def {1} var s-ackcd      as char format "x(1)".
def {1} var s-du~edt      like poel.duedt.
def {1} var s-1stackdt   like poel.reqshipdt.
def {1} var s-expshpdt   like poel.expshipdt.
def {1} var s-ackdt      as date.
def {1} var s-ackno      as char format "x(35)".
def {1} var s-trkno      as char format "x(35)".
def {1} var s-ackuser    as char format "x(4)".
def {1} var s-indt       as char format "99/99/99".        
                         
def {1} var oc-lineno    like poel.lineno.
def {1} var oc-prod      like poel.shipprod. 
def {1} var oc-ackcd     as char format "x(1)".
def {1} var oc-duedt     like poel.duedt.
def {1} var oc-1stackdt  like poel.reqshipdt.
def {1} var oc-expshpdt  like poel.expshipdt.
def {1} var oc-ackdt     as date.
def {1} var oc-ackno     as char format "x(35)".
def {1} var oc-trkno     as char format "x(35)". 
def {1} var oc-ackuser   as char format "x(4)".

def {1} var err-msg      as char format "x(70)" init "".

def {1} var wn-msg1b     as char format "x(58)" init ""  no-undo.
def {1} var wn-msg1c     as char format "x(30)" init ""  no-undo.
def {1} var wn-msg1d     as char format "x(30)" init ""  no-undo.
def {1} var wn-msg1e     as char format "x(15)" init ""  no-undo.
def {1} var wn-msg1      as char format "x(58)" init ""  no-undo.
def {1} var wn-msg2      as char format "x(16)" init ""  no-undo.
def {1} var wn-msg3      as char format "x(50)" init ""  no-undo.

def {1} var wn-ok        as logical init yes no-undo.

def {1} var wn-cnt       as integer format ">>9" init 0  no-undo.

def {1} var wn-ack-nbr   as char format "x(35)" init ""  no-undo.
def {1} var wn-due-date  as date no-undo. 
def {1} var wn-msg-ackdt as char format "x(42)" init ""  no-undo.
def {1} var wn-1st-ackdt as date                         no-undo.
def {1} var wn-msg-expdt as char format "x(42)" init ""  no-undo.
def {1} var wn-expdt     as date                         no-undo.



def {1} frame f-poeza1
    "Acknowledgment Update" at row 1 col 29
    "PO #"                  at row 2 col  5
    "Whse"                  at row 2 col 15
    "Vendor"                at row 2 col 25
    g-pono                  at row 3 col  1
    "-"                     at row 3 col  8
    g-posuf                 at row 3 col  9
    g-whse                  at row 3 col 15
    g-vendno                at row 3 col 25
/*    with no-labels no-box.
    
def {1} frame f-poeza2   */

   "Ln#"                    at row 4 col  1
   "AckCd"                  at row 4 col  5
   "Due Dt"                 at row 4 col 11
   "1stExShp"               at row 4 col 20
   "ExpShpDt"               at row 4 col 29
   "User"                   at row 4 col 38
   "AckgedDt"               at row 4 col 43
   "Prod"                   at row 4 col 52
   "Tracking No"            at row 5 col 5        
   "Acknowledgment No"      at row 5 col 42
   with width 80 no-labels overlay.
    
def {1} frame f-poeza3
    s-lineno                at 1 format "zzz"
    s-ackcd                 at 5
    s-duedt                 at 11
    s-1stackdt              at 20
    s-expshpdt              at 29
    s-ackuser               at 38
    s-ackdt                 at 43
    s-prod                  at 52
    skip
    s-trkno                 at 5
    s-ackno                 at 42
    with row 8 width 80 no-underline overlay no-labels
        scroll 1 12 down.

def {1} frame f-poeza4
    skip
    wn-msg1
    skip
    wn-msg1c
    wn-ack-nbr
    /*
    skip
    wn-msg1d
    wn-due-date
    */
    skip
    wn-msg-ackdt
    wn-1st-ackdt
    skip
    wn-msg-expdt
    wn-expdt
    skip
    /*
    wn-msg1e     
    wn-ok
    */
    skip
    with row 09 centered overlay no-labels title "Acknowledgement".

def {1} frame f-poeza4-b
    skip
    wn-msg1
    skip
    wn-msg1e     
    
    wn-ok
    
    skip
    with row 10 centered overlay no-labels title "Warning".
    
def new shared frame f-poeza5
    skip
    wn-cnt
    wn-msg2 
    skip(1)
    wn-msg3
    skip
    with 
    row 10 centered overlay no-labels title "No Change".
    
     
    
    
