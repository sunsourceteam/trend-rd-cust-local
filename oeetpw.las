/* oeetpw.las 1.1 10/27/98 */
/*h****************************************************************************
  INCLUDE      : oeetpw.las
  DESCRIPTION  : OE PO/WT Interface Window Variable Assignment
  USED ONCE?   : yes
  AUTHOR       : mtt
  DATE WRITTEN : 10/20/98
  CHANGES MADE :
    10/20/98 mtt; TB# 24049 PV: Common code use for GUI interface
    07/01/99 sbr; TB# 26023 Cancel at authorization box process hung
    07/30/99 abe; TB# 26488 Add user hook &user_b4_auth
    03/07/00 bm;  TB# 27105 Roll GUI authorization system to character
    06/19/06 emc; TB# e24161 Creating separate PO's for NS/catalog
******************************************************************************/

    /*d Load defaults for header call (from OEET banner) */
    if v-callmode = "h" then
        assign
            {&ordertype}  = if v-maintmode = "a" then "p" else {&ordertype}
            {&vvendno}    = if v-maintmode = "a" then 0   else {&vvendno}
            {&vshipfmno}  = if v-maintmode = "a" then 0   else {&vshipfmno}
            {&vduedt}     = ?
            {&vorderdt}   = ?
            {&vshipviaty} = if {&ordertype} = "p" and g-oetype <> "do"
                               then v-dshipviaty
                            else ""
            {&vfobfl}     = no
            {&vconfirmfl} = no
            {&wcono}      = g-cono
            {&wwhse}      = if v-maintmode = "a" then ""  else {&wwhse}
            {&wduedt}     = ?
            {&wshipviaty} = if {&ordertype} = "t" and g-oetype <> "do"
                            then
                                v-dshipviaty
                            else
                                "".

    /*d Load Interface information from ARP info/header info
        - don't do if already called or header call (from oeet banner) */
    if v-powtintfl = no and v-callmode <> "h":u and v-maintmode <> "c":u then
        assign
            v-arptype   = if g-oetype = "do" then v-dotype
                          else
                          if {&specnstype} = "n" and {&scrnvendno} > 0
                              then "p"
                          else
                          if {&specnstype} = "n" and {&arpwhse} <> ""
                              then "t"
                          else "a"
            /* cross company transfers will be handled on a later release
            {&wcono}     = if g-oetype = "do" and v-docono <> 0
                              then v-docono
                           else g-cono                                  */
            {&wcono}     = g-cono
            {&wwhse}     = if g-oetype = "do" and v-dotype = "t"
                              then v-dowhse
                           else
                           if {&specnstype} = "n" and {&arpwhse} <> ""
                              then {&arpwhse}
                           else
                           if {&specnstype} = "n" and v-darptype = "w"
                              then v-darpvendno
                           else
                           if {&specnstype} = "n" or
                              can-do("v,m",v-warptype)
                              then ""
                           else v-warpwhse
            {&vshipfmno} = if g-oetype = "do":u and v-dotype = "p":u and
                                ({&specnstype} <> "n":u or
                                 {&scrnvendno} = 0    or
                                 {&scrnvendno} = v-dovendno)
                              then v-doshipfmno
                           else
                           if {&specnstype} = "n":u and {&vshipfmno} > 0
                              then {&vshipfmno}
                           else 0
            {&vvendno}   = if g-oetype = "do":u and v-dotype = "p":u and
                                ({&specnstype} <> "n":u or {&scrnvendno} = 0)
                              then v-dovendno
                           else
                           if {&specnstype} = "n":u and {&scrnvendno} > 0
                              then {&scrnvendno}
                           else
                           if {&specnstype} = "n":u and
                              can-do("v,m",v-darptype)
                              then decimal(v-darpvendno)
                           else
                           if {&specnstype} = "n":u or v-warptype = "w":u
                              then 0
                           else v-warpvendno.
    else
    if v-maintmode = "c" and v-callmode <> "h" then
        assign
            {&vvendno}   = {&oeel-arpvendno}
            {&vshipfmno} = {&oeel-shipfmno}
            {&wwhse}     = if {&oeel-arpvendno} = 0 then {&oeel-arpprodline}
                           else "".

    /*d Load default Ordertype & update it unless a valid ordertype is already
        entered (from oeetle) */
    if not can-do("p,t",{&ordertype}) or v-callmode = "h" or
       ({&specnstype} = "n" and v-maint-l = "a")
    then do:

        if v-callmode <> "h" then
        do:

            if g-oetype = "do"
            then
                v-arptype = v-dotype.

            else if {&specnstype} = "n" then
            do:

            end. /* end of else if {&specnstype} = n */

            else
            if can-do("p,t",v-oetietype)
            then
                v-arptype = v-oetietype.
            else
                v-arptype = "a".

            /*tb 19565 10/20/95 kr; Introduce new ARP Type of "M" (VMI) */
            assign
                {&ordertype} = if (v-arptype = "a" and {&specnstype} = "n"
                                   and can-do("v,m",v-darptype))
                               then
                                   "p"
                               else
                               if (v-arptype = "a" and {&specnstype} = "n")
                               then
                                   "t"
                               else
                               if v-arptype = "a" and
                                   can-do("v,m",v-warptype)
                               then
                                  "p"
                               else
                               if v-arptype = "a"
                               then
                                   "t"
                               else
                                   v-arptype.

        end. /* end of if v-callmode <> h */

        ordertype:
        do while true
        {&comui}
        on endkey undo main, leave main
        /{&comui}* */
        :

            {&comui}
            if v-callmode <> "d" or
                v-loaderr or
                v-authreq
            then
                display s-typelabel.
            /{&comui}* */

            if v-callmode =  "h" or
               v-callmode <> "d" or
              (v-callmode =  "d" and v-loaderr and not v-first) or
               v-authreq
            then do:

                v-updttie = yes.

                /*tb 2103 02/09/96 kr; Create a PO RM from an OE order, added
                    the else if s-createpofl block to set the ordertype field
                    so the user cannot, they should not have access to this
                    field */
                if v-callmode = "h" then do:
                    {&comui}
                    update {&ordertype}
                        help "(A)RP, Vendor (P)O, or Whse (T)ransfer"
                        validate(can-do("a,p,t",{&ordertype}),
                                "Must be A, P, or T (3234)")
                    with frame typeover.
                    /{&comui}* */
                end.
                else
                if {&returnfl} = yes then do:

                    {&ordertype} = "p".

                end. /* end of else if {&returnfl} = yes */

                else do:
                    {&comui}
                    update
                        {&ordertype}
                            help "Vendor (P)O or Whse (T)ransfer"
                            validate(can-do("p,t",{&ordertype}),
                            "Must be P or T (3235)").
                    /{&comui}* */
                end.
            end. /* end of if v-callmode = h or .... */

            /*u User Hook*/
            {p-oeetpw.z99 &user_b4_auth = "*"}

            {&comauth}
            if not (v-doauth        or v-poauth)            and
                (v-oetietype = "n"  or
                (v-oetietype = "p" and {&ordertype} = "t")    or
                (v-oetietype = "t" and {&ordertype} = "p")) then
            auth:
            do while true:

                v-authreq = yes.
                /*tb 26023 07/01/99 sbr; Added "pause 0." in order to allow the
                    error message from "run err.p" to display consistently. */
                pause 0.

                run err.p(if v-callmode <> "d" or not v-authfirst
                            then 1152   /* tie auth req on this ord type */
                            else 1153). /* or tie auth req for product arp */

                if v-callmode = "d" and v-authfirst then pause.
                v-authfirst = no.
                /*tb 27105 03/07/00 bm; Roll GUI auth system to char
                     Replaced saauth.p with authchk.p */
                 run authchk.p(g-cono,
                               g-operinit,
                               "oeet":U,
                               "ties":U,
                               "type":U,
                               "":U,
                               "":U,
                               output v-poauth).

                if v-poauth then
                do:

                    if v-callmode = "h" then v-doauth = yes.
                    leave auth.

                end. /* end of if v-poauth */

                /*tb 26023 07/01/99 sbr; Added "if v-callmode <> "h" and
                    {&ordertype} = "p" and {&returnfl} then leave auth." to the
                    block of code that executes when the cancel key is hit. */

                if v-callmode <> "h" and {&ordertype} = "p" and {&returnfl}
                then
                    leave auth.
                else
                do:
                    next-prompt {&ordertype}.
                    next ordertype.
                end.

            end. /* end of auth: */

            if v-callmode = "h" then hide frame typeover.
            /{&comauth}* */

            leave ordertype.

        end. /* end of ordertype: */

    end. /* end of if not can-do("p,t",{&ordertype}) */