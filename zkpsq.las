/* zkpsq.las 1.1 05/16/00 */
/*h****************************************************************************
  INCLUDE      : zkpsq.las
  DESCRIPTION  : Kit Production Schedule Setup Sequence # Screen
                 Assign Include
  USED ONCE?   : yes 
  AUTHOR       : sd
  DATE WRITTEN : 05/16/00
  CHANGES MADE :
******************************************************************************/

assign
     s-seqn          = {&buf}t-kpsq.seqn      
     s-stagecd       = {&buf}t-kpsq.stagecd
     s-whse          = {&buf}t-kpsq.whse           /*klt*/
     s-wono          = {&buf}t-kpsq.wono   
     s-wosuf         = {&buf}t-kpsq.wosuf
     s-shipprod      = {&buf}t-kpsq.shipprod
     s-reqshipdt     = {&buf}t-kpsq.reqshipdt
     s-tech          = {&buf}t-kpsq.tech.
         
