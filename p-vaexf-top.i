/* *************************************************************************
PROGRAM NAME: p-vaexf-top.i
PROJECT NO.:
DESCRIPTION:  Header routines for VAEXF
DATE WRITTEN: 06/24/10
AUTHOR      : based on VAEXQ written by Tony Abbate
              Tom Herzog


modifications:

tahtrain - Training bug fixes.

***************************************************************************/


/* -------------------------------------------------------------------------  */
PROCEDURE Top1:
/* -------------------------------------------------------------------------  */
  ASSIGN v-vano   = "".
  ASSIGN fs-1 =
"  F6-TOP  F7-Lines  F8-Totals  F9-SPcNotes  F10-Copy   Ctrl-R -Refesh            ".
  DISPLAY fs-1 WITH FRAME f-statusline.
  Top1loop:
  DO WHILE TRUE ON ENDKEY UNDO, LEAVE Top1Loop:
    HIDE FRAME f-lines.
    RUN DisplayTop1.

    IF fldpos1 <= 1 AND
    (v-error = "" OR v-error MATCHES ("*created")) THEN
      NEXT-PROMPT g-fabquoteno WITH FRAME f-top1.
    ELSE
    /** prompt to customer # **/
    IF fldpos1 = 2 AND
    (v-error = "" OR v-error MATCHES ("*created")) THEN DO:
      NEXT-PROMPT v-custnoa WITH FRAME f-top1.
      APPLY "entry" TO v-custnoa IN FRAME f-top1.
    END.
    IF promptpos = 1 THEN
      NEXT-PROMPT g-fabquoteno WITH FRAME f-top1.
    ELSE
    IF promptpos = 2 THEN
      NEXT-PROMPT x-whse  WITH FRAME f-top1.
    ELSE
    IF promptpos = 3 THEN
      NEXT-PROMPT v-takenby WITH FRAME f-top1.
    ELSE
    IF promptpos = 4 THEN DO:
      NEXT-PROMPT v-custnoa WITH FRAME f-top1.
      APPLY "entry" TO v-custnoa IN FRAME f-top1.
    END.
    ELSE
    IF promptpos = 5 THEN DO:
      NEXT-PROMPT v-shipto  WITH FRAME f-top1.
      APPLY "entry" TO v-shipto IN FRAME f-top1.
    END.
    ELSE
    IF promptpos = 6 THEN
      NEXT-PROMPT v-custpo WITH FRAME f-top1.
    ELSE
    IF promptpos = 7 THEN
      NEXT-PROMPT v-model  WITH FRAME f-top1.
    ELSE
    IF promptpos = 8 THEN DO:
      NEXT-PROMPT s-prod   WITH FRAME f-top1.
      APPLY "entry" TO s-prod IN FRAME f-top1.
    END.
    ASSIGN promptpos = 0.

 /* If there is a quoteno then the warehouse is implied and cannot be changed */
    IF g-fabquoteno <> "" THEN DO:
      RUN makequoteno(g-fabquoteno,OUTPUT x-quoteno).
      FIND FIRST vasp WHERE
                 vasp.cono     = g-cono AND
                 vasp.shipprod = x-quoteno
      NO-LOCK NO-ERROR.
      IF avail vasp THEN DO:
        ASSIGN x-whse = vasp.whse.
        ASSIGN v-found = IF x-whse NE "" THEN
                           YES
                         ELSE
                           NO.
      END.
      ELSE
        v-found = FALSE.
    END.
    ELSE
      v-found = FALSE.
    UPDATE
      g-fabquoteno
      v-revno
      x-whse
      v-takenby
      v-custnoa
      v-shipto
      v-custpo
      v-model
      s-prod when s-prod <> ""
    GO-ON ({k-vaexqon.i} ctrl-d ctrl-o f8 f10 ctrl-r)
    WITH FRAME f-top1
    EDITING: 
      IF v-error = "" THEN
        PUT SCREEN  ROW 23 COL 1
      "                                                                      ".
      ELSE DO:
        PUT SCREEN  ROW 23 COL 1
        "                                                                       ".
          PUT SCREEN  ROW 23 COL 1 v-error.
        ASSIGN v-error = "".
      END.
  
      READKEY.
      
      IF INPUT g-fabquoteno = "" THEN DO:
        ASSIGN v-found = NO.
        IF CAN-DO('ctrl-o',KEYLABEL(LASTKEY)) THEN DO:
          ASSIGN
            v-error      = "Operator options set."
            v-framestate = "t1"
            g-fabquoteno  = INPUT g-fabquoteno
            o-vaquoteno  = TRIM(g-fabquoteno," ").
          RUN WhatPrompt.
          RUN operatorsettings.
          RETURN.
        END. /* can-do ctrl-o */
      END.

      if {k-cancel.i} and frame-field = "v-model" and
          (v-mfgbrowsekeyed = true or 
           v-mfgbrowseopen = true) THEN DO:
        readkey pause 0.
        APPLY "entry" TO v-model IN FRAME f-top1.
        assign v-mfgbrowseopen = FALSE
               v-mfgbrowsekeyed = FALSE
               v-modelsbuildfl = FALSE.
        CLOSE QUERY q-models.
        NEXT-PROMPT v-model  WITH FRAME f-top1.
        
        hide frame f-models.
        next.
      end.
      
      IF {k-cancel.i} THEN DO:
        APPLY LASTKEY.
        LEAVE top1loop.
      END.
      
      IF CAN-DO('f7',KEYLABEL(LASTKEY))     OR
         CAN-DO('f8',KEYLABEL(LASTKEY))     OR
         CAN-DO('f9',KEYLABEL(LASTKEY))     OR
         CAN-DO('f10',KEYLABEL(LASTKEY))    OR
         CAN-DO('ctrl-r',KEYLABEL(LASTKEY)) OR
         CAN-DO('ctrl-d',KEYLABEL(LASTKEY)) OR
         CAN-DO('ctrl-o',KEYLABEL(LASTKEY)) THEN
      ASSIGN
        promptpos   = 0
        v-takenby   = INPUT v-takenby
        v-revno     = INPUT v-revno
        v-model     = INPUT v-model
        v-custnoa   = INPUT v-custnoa
        v-shipto    = INPUT v-shipto
        v-custpo    = INPUT v-custpo
        s-prod      = INPUT s-prod
        v-tagprt    = NO.
      
      IF {k-sdileave.i
          &functions  = " can-do('PF1',keylabel(lastkey))    or
                          can-do('f7',keylabel(lastkey))     or
                          can-do('f8',keylabel(lastkey))     or
                          can-do('f9',keylabel(lastkey))     or
                          can-do('f10',keylabel(lastkey))    or
                          CAN-DO('ctrl-r',KEYLABEL(LASTKEY)) OR
                          can-do('ctrl-o',keylabel(lastkey)) or
                          can-do('ctrl-d',keylabel(lastkey)) or "
          &fieldname     = "g-fabquoteno"
          &completename  = "g-fabquoteno"} THEN DO:
        IF CAN-DO('f10',KEYLABEL(LASTKEY)) THEN DO:
          IF (o-vaquoteno NE "" AND INPUT g-fabquoteno = "") THEN
            ASSIGN g-fabquoteno = o-vaquoteno.
          ELSE
          IF o-vaquoteno = "" AND INPUT g-fabquoteno NE "" THEN
            ASSIGN g-fabquoteno = INPUT g-fabquoteno.
        END.
        ELSE
          ASSIGN g-fabquoteno = INPUT g-fabquoteno.
/*        
        IF CAN-DO('f10',KEYLABEL(LASTKEY)) THEN DO:
          ASSIGN g-fabquoteno = g-fabquoteno.
          RUN makequoteno(g-fabquoteno,OUTPUT x-quoteno).
          FIND FIRST vasp WHERE
                     vasp.cono     = g-cono AND
                     vasp.shipprod = x-quoteno
          NO-LOCK NO-ERROR.
        END.
        ELSE DO:
*/       /** very first find for vasp for editing-g-fabquoteno established **/
          RUN makequoteno(g-fabquoteno,OUTPUT x-quoteno).
          FIND FIRST vasp WHERE
                     vasp.cono     = g-cono AND
                     vasp.shipprod = x-quoteno
          NO-LOCK NO-ERROR.
          IF avail vasp THEN DO:
            IF (avail vasp AND vasp.user7 = v-cancelled) and
               keylabel(lastkey) <> "f10" THEN DO:
              PUT SCREEN ROW 23 COL 1
                "Invalid FabQu Stage Cancelled --> " + g-fabquoteno.
              ASSIGN v-error =
                "Invalid FabQu Stage Cancelled --> " + g-fabquoteno.
              BELL.
              ASSIGN v-found = NO.
              NEXT-PROMPT g-fabquoteno WITH FRAME f-top1.
              NEXT.
            END.
            IF vasp.user2 NE "" AND
            DEC(SUBSTRING(vasp.user2,1,7)) > 0 
            and keylabel(lastkey) <> "f10" THEN DO:
              HIDE FRAME f-bot2.
              PUT SCREEN ROW 23 COL 1
                "Va order exists->" + SUBSTRING(vasp.user2,1,7) +
              STRING(SUBSTRING(vasp.user2,8,2),"99") + " FabQu update invalid.".
              ASSIGN v-error =
                "Va order exists->" + SUBSTRING(vasp.user2,1,7) +
              STRING(SUBSTRING(vasp.user2,8,2),"99") + 
                     " FabQu update invalid.".
              BELL.
              ASSIGN v-found   = NO.
              DISPLAY g-fabquoteno WITH FRAME f-top1.
              NEXT-PROMPT g-fabquoteno WITH FRAME f-top1.
              NEXT.
            END.
          END.
          ELSE /* if avail vasp */
          IF (NOT avail vasp AND g-fabquoteno NE "" AND
          NUM-ENTRIES(g-fabquoteno,"-") = 2) OR v-errfl THEN DO:
            IF v-errfl THEN DO:
              PUT SCREEN ROW 23 COL 1
                "Invalid FabQuNo --> " + g-fabquoteno +
                " - please enter format 'YYMM-7      '".
              ASSIGN v-error =
                "Invalid FabQuNo --> " + g-fabquoteno +
                " - please enter format 'YYMM-7      '".
            END.
            ELSE DO:
              PUT SCREEN ROW 23 COL 1
                "Invalid FabQuNo --> " + g-fabquoteno.
              ASSIGN v-error =
                "Invalid FabQuNo --> " + g-fabquoteno.
            END.
            BELL.
            ASSIGN v-found    = NO
              v-custnoa  = ""
              v-custno   = 0
              v-vano     = ""
              s-prod     = ""
              v-astr-top = ""
              v-model    = ""
              v-custpo   = ""
              v-shipto   = ""
              v-binloc   = ""
              v-custname = ""
              v-stage    = ""
              v-tagdt    = ?.
            RUN DisplayTop1.
            NEXT-PROMPT g-fabquoteno WITH FRAME f-top1.
            NEXT.
          END.  /* if v-errfl - repair notfound  */
          
          ASSIGN x-whse = IF avail vasp THEN
                            vasp.whse
                          ELSE IF x-whse <> "" then
                            x-whse
                          ELSE
                            "".
          IF x-whse = "" AND d-whse NE "" THEN
            ASSIGN x-whse = d-whse.
          IF x-whse = "" THEN DO:
            {w-sasoo.i g-operinits NO-LOCK}
            ASSIGN x-whse  = IF avail sasoo AND
                               sasoo.whse NE "" THEN
                               sasoo.whse
                             ELSE
                               INPUT x-whse.
            DISPLAY x-whse WITH FRAME f-top1.
          END.
          {w-icsd.i x-whse NO-LOCK}
          IF (x-whse = "" OR NOT avail icsd) THEN DO:
            PUT SCREEN ROW 23 COL 1
              "Valid Whse required field - please enter. " + x-whse.
            ASSIGN v-error = "Valid Whse required field - please enter. "
              + x-whse.
            BELL.
            ASSIGN 
              v-framestate = "t1"
              promptpos    =  2
              v-found      = NO
              o-vaquoteno  = TRIM(g-fabquoteno," ")
              x-whse       = "".
            RETURN.
          END.
          ELSE
            DISPLAY x-whse WITH FRAME f-top1.
          
          DISPLAY x-whse WITH FRAME f-top1.
          ASSIGN v-found = IF x-whse NE "" THEN
                             YES
                           ELSE
                             NO.
          IF CAN-DO('ctrl-d',KEYLABEL(LASTKEY)) THEN
            ASSIGN g-fabquoteno = g-fabquoteno.
          ELSE
          /* header-chg will initialize and lock a new repair */
          IF TRIM(g-fabquoteno," ") NE TRIM(o-vaquoteno," ") or
            (TRIM(g-fabquoteno," ") = TRIM(o-vaquoteno," ") and
             g-fabquoteno = "") OR
          CAN-DO("ctrl-o",KEYLABEL(LASTKEY)) THEN DO:
            ASSIGN 
              v-notes1tm = NO
              leavefl    = NO
              promptpos  = 0.
          
            IF KEYFUNCTION(LASTKEY) = "go" AND v-error = "" AND
              g-fabquoteno <> "" THEN DO:
              ASSIGN v-CurrentKeyPlace = 1.
              RUN FindNextFrame (INPUT LASTKEY,
                                 INPUT-OUTPUT v-CurrentKeyPlace ,
                                 INPUT-OUTPUT v-framestate).
            END.
            ELSE
              ASSIGN v-framestate = "t1".
            if g-fabquoteno <> "" then 
              assign s-LastFabNo = "Editing: " + 
                      ENTRY(1,g-fabquoteno,"-") +  "-" +
                      STRING(INT(ENTRY(2,g-fabquoteno,"-"))).

            RUN header-chg.

            IF g-fabquoteno = "" THEN DO:
              ASSIGN 
                v-framestate = "t1"
                promptpos    = IF v-error <> "" THEN
                                 1
                               ELSE IF LASTKEY = 502 THEN
                                 4
                               ELSE if  CAN-DO('f10',KEYLABEL(LASTKEY))   OR
                                       CAN-DO('ctrl-r',KEYLABEL(LASTKEY)) then
                                 1
                               ELSE
                                 2
                v-found      = NO
                o-vaquoteno  = TRIM(g-fabquoteno," ").
/*                x-whse       = "". */
/* Jen wanted the fields cleared */
              ASSIGN
                v-custnoa  = ""
                v-custno   = 0
                v-vano     = ""
                s-prod     = ""
                v-astr-top = ""
                v-model    = ""
                v-custpo   = ""
                v-shipto   = ""
                v-binloc   = ""
                v-custname = ""
                v-stage    = ""
                v-tagdt    = ?.
/*
              APPLY LASTKEY.
              RUN DisplayTop1.
              RETURN.
*/
            END.
            
            IF v-errfl THEN DO:
              BELL.
              DISPLAY fs-1 WITH FRAME f-statusline.
              NEXT-PROMPT g-fabquoteno WITH FRAME f-top1.
              ASSIGN 
                v-framestate = "t1"
                promptpos    = 1
                v-found      = NO.
              RELEASE vasp.
              RETURN.
            END.
            
            ASSIGN v-framestate = IF v-error <> "" OR
                                  CAN-DO("ctrl-o",KEYLABEL(LASTKEY)) THEN
                                   "t1"
                                  ELSE
                                    v-framestate
              promptpos    = IF v-error <> "" OR
                             CAN-DO("ctrl-o",KEYLABEL(LASTKEY)) THEN
                               1
                             ELSE IF LASTKEY = 502 THEN
                               4
                             ELSE if  CAN-DO('f10',KEYLABEL(LASTKEY))   OR
                                      CAN-DO('ctrl-r',KEYLABEL(LASTKEY)) then
                                 1
                              ELSE
                               2
              v-found      = YES
              o-vaquoteno  = TRIM(g-fabquoteno," ").

            APPLY LASTKEY.

            DISPLAY fs-1 WITH FRAME f-statusline.
            RELEASE vasp.
            IF v-framestate = "t1" THEN DO:
              IF v-takenby = "" 
/* tahtrain */
                and {k-accept.i}
/* tahtrain */              
              THEN DO:
                ASSIGN v-error = " Required TknBy - please enter.".
                PUT SCREEN ROW 23 COL 1 v-error.
                ASSIGN 
                  promptpos = 2
                  v-framestate = "t1".
              END.
              ELSE
              IF v-custnoa = "" 
/* tahtrain */
                 and {k-accept.i}  
/* tahtrain */
                 THEN DO:
                ASSIGN v-error = " Required Customer - please enter.".
                PUT SCREEN ROW 23 COL 1 v-error.
                ASSIGN 
                  promptpos = 4
                  v-framestate = "t1".
              END.
              ELSE
              IF v-model = ""
/* tahtrain */
                 and {k-accept.i}  
/* tahtrain */
               
               THEN DO:
                ASSIGN v-error = " Required Unit type - please enter.".
                PUT SCREEN ROW 23 COL 1 v-error.
                ASSIGN 
                  promptpos = 7
                  v-framestate = "t1".
              END.
     
              IF v-error = "" AND g-fabquoteno <> "" AND
                 NOT CAN-DO("ctrl-o",KEYLABEL(LASTKEY)) THEN
                RUN Create_browse_lines("yes").
            END.
            ELSE
            IF v-error NE "" AND
            NOT CAN-DO("ctrl-o",KEYLABEL(LASTKEY))and
             g-fabquoteno <> "" THEN DO:
               RUN Create_browse_lines("yes").
              RETURN.
            END.
/* tahtrain */
            IF g-fabquoteno <> "" AND
               NOT CAN-DO("ctrl-o",KEYLABEL(LASTKEY))and
               (NOT {k-accept.i}) and v-error = "" THEN DO:
              ASSIGN promptpos = 2.
              NEXT Top1Loop.
            END.
/* tahtrain */ 
          END.
          ELSE
          IF (avail vasp AND TRIM(g-fabquoteno," ") = TRIM(o-vaquoteno," "))
          THEN DO:
            ASSIGN 
              v-notes1tm = NO
              leavefl    = NO
              promptpos  = 0.
            IF (CAN-DO("PF1",KEYLABEL(LASTKEY))     OR
               (CAN-DO("RETURN",KEYLABEL(LASTKEY))  AND
                 FRAME-FIELD = "v-model" and
                  s-prod = "") or 
               (CAN-DO("RETURN",KEYLABEL(LASTKEY))  AND
                 FRAME-FIELD = "s-prod")) THEN DO:

            
            
              IF v-takenby = "" THEN DO:
                ASSIGN v-error = " Required TknBy - please enter.".
                PUT SCREEN ROW 23 COL 1 v-error.
                ASSIGN 
                  promptpos = 3
                  v-errfl   = YES
                  v-framestate = "t1".
              END.
              ELSE
              IF v-custnoa = "" THEN DO:
                ASSIGN v-error = " Required Customer - please enter.".
                PUT SCREEN ROW 23 COL 1 v-error.
                ASSIGN 
                  promptpos = 4
                  v-errfl = yes
                  v-framestate = "t1".
              END.
              ELSE
              IF v-model = "" THEN DO:
                ASSIGN v-error = " Required Unit type - please enter.".
                PUT SCREEN ROW 23 COL 1 v-error.
                ASSIGN 
                  promptpos = 7
                  v-errfl   = YES
                  v-framestate = "t1".
              END.
            END. /* go or return */
            IF (KEYFUNCTION(LASTKEY) = "go" or
                KEYLABEL(LASTKEY) = "F7")
             AND v-error = "" AND
            input g-fabquoteno <> "" THEN DO:
              ASSIGN v-CurrentKeyPlace = 1
                     v-ManualFrameJump = false.
              RUN FindNextFrame (INPUT LASTKEY,
                                 INPUT-OUTPUT v-CurrentKeyPlace ,
                                 INPUT-OUTPUT v-framestate).
            END.
            ELSE
              ASSIGN v-framestate = "t1".
            RUN header-chg.
            ASSIGN v-framestate = IF v-error <> "" THEN
                                    "t1"
                                  ELSE
                                    v-framestate
              promptpos    = IF  CAN-DO('f10',KEYLABEL(LASTKEY))   OR
                                 CAN-DO('ctrl-r',KEYLABEL(LASTKEY)) then
                                 1
                             else
                                 2
/*                             IF v-error <> "" THEN
                                1
                              ELSE IF LASTKEY = 502 THEN
                                4
                              ELSE
                                2 */
              v-found      = YES
              o-vaquoteno  = TRIM(g-fabquoteno," ").
            RELEASE vasp.

            APPLY LASTKEY.
      
            IF v-framestate = "m1" THEN DO:
              IF v-takenby = "" THEN DO:
                ASSIGN v-error = " Required TknBy - please enter.".
                PUT SCREEN ROW 23 COL 1 v-error.

                
                ASSIGN 
                  promptpos = 3
                  v-framestate = "t1".
              END.
              IF v-error = "" THEN
                RUN Create_browse_lines("yes").
            END.
            
            if v-error <> "" then
             /*  was not going to the appropriate function */
              RETURN.
            
          END.
          ELSE DO:
            RUN vaexfinuse.p(INPUT x-quoteno,
                             INPUT g-operinits,
                             INPUT "x",
                             INPUT-OUTPUT v-inuse).
            IF v-inuse THEN DO:
              ASSIGN v-error = "FabQuNo is in-use by operator: " +
                STRING(vasp.xxc4,"x(20)").
              PUT SCREEN ROW 23 COL 1 "FabQuNo is in-use by operator: " +
                STRING(vasp.xxc4,"x(20)").
              BELL.
              NEXT-PROMPT g-fabquoteno WITH FRAME f-top1.
              ASSIGN 
                v-framestate = "t1"
                promptpos    = 1
                v-found      = NO.
              RELEASE vasp.
              RETURN.
            END.
            ELSE DO:
              ASSIGN 
                v-framestate = "t1"
                promptpos    = if  CAN-DO('f10',KEYLABEL(LASTKEY))   OR
                                   CAN-DO('ctrl-r',KEYLABEL(LASTKEY)) then
                                 1
                               else 
                                 2 
/*                            IF LASTKEY = 502 THEN
                                 4
                               ELSE
                                 2  */
                v-found      = YES
                o-vaquoteno  = TRIM(g-fabquoteno," ").
            
              RUN makequoteno(g-fabquoteno,OUTPUT x-quoteno).
              assign s-LastFabNo = "Editing: " +
                      ENTRY(1,x-quoteno,"-") +  "-" +
                      STRING(INT(ENTRY(2,x-quoteno,"-"))).

              display s-LastFabNo with frame f-top1.
              APPLY LASTKEY.
              DISPLAY fs-1 WITH FRAME f-statusline.
              
              RELEASE vasp.
                          
               RETURN.
            END. /* else do */
          
          END.
/*        END. /* else do - no f10 */      */
      END. /** big - if k-sdileave.i on g-vaquotno **/
      
      IF CAN-DO("PF1",KEYLABEL(LASTKEY)) AND INPUT s-prod NE "" THEN DO:
        ASSIGN 
          v-model     = INPUT v-model
          s-prod      = INPUT s-prod
          g-fabquoteno = INPUT g-fabquoteno.
        RUN makequoteno(g-fabquoteno,OUTPUT x-quoteno).
        
        FIND FIRST vasp WHERE
                   vasp.cono     = g-cono AND
                   vasp.shipprod = x-quoteno
        NO-LOCK NO-ERROR.
      END. /* PF1   */
      
      ASSIGN 
        v-takenby   = INPUT v-takenby
        v-model     = INPUT v-model
        v-revno     = INPUT v-revno
        v-custnoa   = INPUT v-custnoa
        v-shipto    = INPUT v-shipto
/*        x-whse      = INPUT x-whse   */
        v-custpo    = INPUT v-custpo
        s-prod      = INPUT s-prod
        v-tagprt    = NO.
      
      IF FRAME-FIELD = "g-fabquoteno" AND KEYLABEL(LASTKEY) = "F12" THEN DO:
        APPLY LASTKEY.
        ASSIGN 
          l-whse    = x-whse
          l-stage   = "".
        updtloop:
        DO WHILE TRUE ON ENDKEY UNDO, LEAVE updtloop:
          UPDATE 
            g-fabquoteno
            l-whse
            g-custno
            l-custpo
            l-takenby
            l-stage
          WITH FRAME f-qlookup
          EDITING: 
            READKEY.
            PUT SCREEN ROW 23 COL 1
              "                                             ".
            IF {k-cancel.i} OR {k-jump.i} THEN DO:
              HIDE FRAME f-qlookup.
              LEAVE updtloop.
            END.
            IF LASTKEY = 401 OR
            (LASTKEY = 13 AND FRAME-FIELD = "l-stage") THEN DO:
              IF INPUT l-whse = "" THEN DO:
                PUT SCREEN ROW 23 COL 1 "  Whse required - please enter.".
                ASSIGN v-error = "  Whse required - please enter.".
                NEXT updtloop.
              END.
              LEAVE updtloop.
            END.
            APPLY LASTKEY.
          END. /* Editing */
        END. /* do while update loop */
        HIDE FRAME f-qlookup.
        IF {k-cancel.i} THEN DO:
          READKEY PAUSE 0.
          NEXT top1loop.
        END.
        ASSIGN g-fabquoteno l-whse l-takenby v-stage l-custpo.
        IF (KEYLABEL(LASTKEY) = "RETURN" AND FRAME-FIELD = "l-stage") OR
        KEYLABEL(LASTKEY) = "PF1"    THEN DO:
          RUN zsdiquotefab.p(INPUT g-fabquoteno,
                             INPUT l-whse,
                             input g-custno,
                             INPUT l-takenby,
                             INPUT l-stage,                    
                             INPUT l-custpo,
                             INPUT-OUTPUT g-fabquoteno,
                             INPUT-OUTPUT v-errfl).
          IF v-errfl THEN DO:
            ASSIGN v-error = "  No records found for F12 lookup..".
              PUT SCREEN ROW 23 COL 1 v-error.
              ASSIGN 
                promptpos = 1
                v-framestate = "t1".
              BELL.
          END.
          IF LASTKEY = 404 THEN DO:
            ASSIGN 
              v-tagprt  = YES
              g-fabquoteno = "".
            DISPLAY g-fabquoteno WITH FRAME f-top1.
            NEXT top1loop.
          END.
          ELSE DO:
            DISPLAY g-fabquoteno WITH FRAME f-top1.
            ASSIGN promptpos    = 1.
            NEXT-PROMPT g-fabquoteno WITH FRAME f-top1.
            NEXT top1loop.
          END.
        END.
      END. /* frame-field = g-fabquoteno */
      
      IF FRAME-FIELD = "v-custnoa" AND
      (CAN-DO('f7',KEYLABEL(LASTKEY))      OR
       CAN-DO('f8',KEYLABEL(LASTKEY))      OR
       CAN-DO('f9',KEYLABEL(LASTKEY))      OR
       CAN-DO('f10',KEYLABEL(LASTKEY))     OR
       CAN-DO('ctrl-r',KEYLABEL(LASTKEY)) OR
       CAN-DO('ctrl-d',KEYLABEL(LASTKEY))  OR
       CAN-DO('ctrl-o',KEYLABEL(LASTKEY))) THEN
        ASSIGN v-custnoa = INPUT v-custnoa.
      ELSE
      IF FRAME-FIELD = "v-custnoa" THEN DO:
        HIDE FRAME f-bot2.
        IF NOT CAN-DO("9,13,401,404,501,502,503,504,508,507",STRING(LASTKEY))
        THEN
          ASSIGN v-custbrowsekeyed = FALSE.
        IF (NOT CAN-DO("9,21,13,501,502,503,504,507,508",STRING(LASTKEY)) AND
            KEYFUNCTION(LASTKEY) NE "go" AND
            KEYFUNCTION(LASTKEY) NE "end-error") THEN DO:
          /* user has keyed in non-movement key */
          APPLY LASTKEY.
          IF FRAME-FIELD <> "v-custnoa" THEN
            NEXT.
          ASSIGN v-custnoa = INPUT v-custnoa.
          RUN DoCustLu (INPUT v-custnoa,
                        INPUT "normal",
                        INPUT "f-top1" ).
          HIDE FRAME f-mid1.
          HIDE FRAME f-lines.
          ASSIGN v-custbrowsekeyed = FALSE.
          NEXT.
        END.
        ELSE
        IF (KEYLABEL(LASTKEY) = "cursor-left" OR
            LASTKEY = 501)  AND
            v-custnoa:CURSOR-OFFSET = 1 AND
            v-custbrowsekeyed = FALSE THEN DO:
          ASSIGN v-custbrowseopen = FALSE.
          HIDE FRAME f-custlu.
          HIDE FRAME f-zcustinfo.
          ON cursor-up back-tab.
          ON cursor-down tab.
          NEXT-PROMPT v-takenby WITH FRAME f-top1.
          NEXT.
        END.
        ELSE IF (LASTKEY = 501 OR
                 LASTKEY = 502 OR
                 LASTKEY = 507 OR
                 LASTKEY = 508) AND v-custbrowseopen = TRUE THEN DO:
          RUN MoveCustLu (INPUT LASTKEY,
                          INPUT "f-top1").
          ASSIGN v-custbrowsekeyed = TRUE.
          NEXT.
        END.
        ELSE
        IF KEYFUNCTION(LASTKEY) = "end-error" AND
        v-custbrowsekeyed    = TRUE THEN DO:
          ON cursor-up back-tab.
          ON cursor-down tab.
          ASSIGN v-custbrowsekeyed = FALSE.
          HIDE FRAME f-custlu.
          HIDE FRAME f-zcustinfo.
          NEXT.
        END.
        ELSE
        IF (LASTKEY = 13 OR KEYFUNCTION(LASTKEY) = "go") THEN DO:
          IF v-custbrowsekeyed = FALSE THEN DO:
            DEC(v-custnoa) NO-ERROR.
            IF ERROR-STATUS:ERROR THEN DO:
              ASSIGN v-error = " Invalid Customer - please enter.".
              PUT SCREEN ROW 23 COL 1 v-error.
              NEXT-PROMPT v-custnoa WITH FRAME f-top1.
              NEXT.
            END.
          END.
          IF v-custbrowsekeyed = FALSE THEN DO:
            FIND b-arsc WHERE
                 b-arsc.cono = g-cono AND
                 b-arsc.custno = DEC(INPUT v-custnoa) NO-LOCK NO-ERROR.
            IF NOT avail b-arsc AND INPUT v-custnoa <> "" THEN DO:
              ASSIGN v-error = " Invalid Customer - please enter.".
              PUT SCREEN ROW 23 COL 1 v-error.
              NEXT-PROMPT v-custnoa WITH FRAME f-top1.
              NEXT.
            END.
            IF v-custbrowsekeyed = FALSE AND avail b-arsc THEN DO:
              ASSIGN v-custnoa   = STRING(b-arsc.custno).
              ASSIGN 
                v-custno    = DEC(v-custnoa)
                v-Shipto    = INPUT v-Shipto
                v-revno     = INPUT v-revno
                v-takenby   = INPUT v-takenby
                v-custname  = IF avail b-arsc THEN
                                b-arsc.NAME
                              ELSE
                                v-custname
                v-custpo    = /* if (o-custno = 0 or
                                v-custno <> o-custno) and
                                 avail b-arsc and v-custpo = "" then
                                b-arsc.custpo
                              else */
                                v-custpo.
                               
                                
              IF (o-custno NE v-custno AND o-custno > 0) AND
              g-fabquoteno NE "" THEN DO:
                ASSIGN v-chgfl = YES.
                RUN CustomerChange (INPUT-OUTPUT v-chgfl).
                ASSIGN v-custbrowseopen = FALSE.
                HIDE FRAME f-custlu.
                HIDE FRAME f-zcustinfo.
                ON cursor-up back-tab.
                ON cursor-down tab.
                NEXT  Top1loop.
              END.
              RUN DisplayTop1.
            END.
          END. /* if v- custbrowsekeyed */
          ELSE
          IF v-custbrowsekeyed AND avail arsc THEN DO:
            ASSIGN 
              v-custnoa  = STRING(arsc.custno)
              v-custno   = arsc.custno
              v-custname = arsc.NAME
              v-custpo    = /* if (o-custno = 0 or
                               arsc.custno <> o-custno) and
                               avail arsc and v-custpo = "" then
                                arsc.custpo
                              else */
                                v-custpo.
 
            IF (o-custno NE v-custno AND o-custno > 0) AND
            g-fabquoteno NE "" THEN DO:
              ASSIGN v-chgfl = YES.
              RUN CustomerChange (INPUT-OUTPUT v-chgfl).
              ASSIGN v-custbrowseopen = FALSE.
              HIDE FRAME f-custlu.
              HIDE FRAME f-zcustinfo.
              ON cursor-up back-tab.
              ON cursor-down tab.
              NEXT  Top1loop.
            END.
            
            DISPLAY 
              v-custnoa
              v-custname
              v-custpo
            WITH FRAME f-top1.
            ASSIGN v-custbrowsekeyed = FALSE.
            HIDE FRAME f-custlu.
            HIDE FRAME f-zcustinfo.
          END.
          ELSE
          ASSIGN v-Custbrowsekeyed = FALSE.
        END. /* lastkey = 13 and go */
        ASSIGN v-custbrowsekeyed = FALSE.
        HIDE FRAME f-Custlu.
        HIDE FRAME f-zcustinfo.
      END. /* frame field = v-custnoa */
      
      IF {k-sdileave.i
          &functions  = " can-do('f7',keylabel(lastkey))     or
                          can-do('f8',keylabel(lastkey))     or
                          can-do('f9',keylabel(lastkey))     or
                          can-do('f10',keylabel(lastkey))    or
                          CAN-DO('ctrl-r',KEYLABEL(LASTKEY)) OR
                          can-do('ctrl-d',keylabel(lastkey)) or
                          can-do('ctrl-o',keylabel(lastkey)) or "
          &fieldname     = "v-takenby"
          &completename  = "v-takenby"} THEN DO:
        ASSIGN v-takenby = INPUT v-takenby.
      END.
      
      IF {k-sdileave.i
          &functions  = " can-do('f7',keylabel(lastkey))     or
                          can-do('f8',keylabel(lastkey))     or
                          can-do('f9',keylabel(lastkey))     or
                          can-do('f10',keylabel(lastkey))    or
                          CAN-DO('ctrl-r',KEYLABEL(LASTKEY)) OR
                          can-do('ctrl-d',keylabel(lastkey)) or
                          can-do('ctrl-o',keylabel(lastkey)) or "
          &fieldname     = "x-whse"
          &completename  = "x-whse"} THEN DO:
        ASSIGN x-whse = INPUT x-whse.
      END.
/*       
      IF FRAME-FIELD = "x-whse" THEN
        IF (NOT CAN-DO("9,21,13,501,502,507,508",STRING(LASTKEY)) AND
            KEYFUNCTION(LASTKEY) NE "go" AND
            KEYFUNCTION(LASTKEY) NE "end-error") THEN 
          ASSIGN x-whse = INPUT x-whse.
*/      
      IF {k-sdileave.i
          &functions  = " can-do('f7',keylabel(lastkey))     or
                          can-do('f8',keylabel(lastkey))     or
                          can-do('f9',keylabel(lastkey))     or
                          can-do('f10',keylabel(lastkey))    or
                          CAN-DO('ctrl-r',KEYLABEL(LASTKEY)) OR
                          can-do('ctrl-d',keylabel(lastkey)) or
                          can-do('ctrl-o',keylabel(lastkey)) or "
          &fieldname     = "v-model"
          &completename  = "v-model"} THEN DO:
        ASSIGN v-model = INPUT v-model.
        HIDE FRAME f-models.
        if ( v-model:cursor-offset = v-model:width-chars
              and  keylabel(lastkey) = "CURSOR-RIGHT") then do:        
           NEXT-PROMPT v-model WITH FRAME f-top1.
          NEXT.
        end.
        if  (v-model:cursor-offset = 1 AND 
              keylabel(lastkey) = "CURSOR-LEFT") then do:         
          NEXT-PROMPT v-custpo WITH FRAME f-top1.
          NEXT.
        END.

        IF (LASTKEY = 501) AND v-model:CURSOR-OFFSET = 1 AND
             v-mfgbrowsekeyed = FALSE THEN DO:
          ON cursor-up back-tab.
          ON cursor-down tab.
          NEXT-PROMPT v-custpo WITH FRAME f-top1.
          NEXT.
        END.

        IF (LASTKEY = 502) AND v-model:CURSOR-OFFSET = 1 AND
             v-mfgbrowseopen = FALSE THEN DO:
          ON cursor-up back-tab.
          ON cursor-down tab.
          /* hhh
          NEXT-PROMPT s-prod WITH FRAME f-top1.
          NEXT.
          */
        END.
      

      
      
      END.
      
      IF FRAME-FIELD = "v-model" AND
        (CAN-DO('f7',KEYLABEL(LASTKEY))     OR
         CAN-DO('f8',KEYLABEL(LASTKEY))     OR
         CAN-DO('f9',KEYLABEL(LASTKEY))     OR
         CAN-DO('f10',KEYLABEL(LASTKEY))    OR
         CAN-DO('ctrl-r',KEYLABEL(LASTKEY)) OR
         CAN-DO('ctrl-d',KEYLABEL(LASTKEY)) OR
         CAN-DO('ctrl-o',KEYLABEL(LASTKEY))) THEN DO:
        ASSIGN v-model = INPUT v-model.
      END.
      ELSE
      IF FRAME-FIELD = "v-model" THEN DO:
        HIDE FRAME f-models.
        IF NOT CAN-DO("9,13,401,404,501,502,503,504,508,507",STRING(LASTKEY))
        THEN
          ASSIGN v-mfgbrowsekeyed = FALSE.
        IF (NOT CAN-DO("9,21,13,501,502,503,504,507,508",STRING(LASTKEY)) AND
            KEYFUNCTION(LASTKEY) NE "go" AND
            KEYFUNCTION(LASTKEY) NE "end-error") THEN DO:
          /* user has keyed in non-movement key */
          APPLY LASTKEY.
          IF FRAME-FIELD <> "v-model" THEN
            NEXT.
          ASSIGN v-model = INPUT v-model.
          RUN DoMfgLu (INPUT v-model,
                       INPUT "normal").
          HIDE FRAME f-mid1.
          HIDE FRAME f-lines.
          ASSIGN v-mfgbrowsekeyed = FALSE.
          NEXT.
        END.
        ELSE
        IF ((KEYLABEL(LASTKEY) = "cursor-left" OR
             LASTKEY = 501)   AND
             v-model:CURSOR-OFFSET = 1) AND
             v-mfgbrowsekeyed = FALSE THEN DO:
          ON cursor-up back-tab.
          ON cursor-down tab.
          ASSIGN v-mfgbrowseopen = FALSE.
          HIDE FRAME f-models.
          NEXT-PROMPT s-prod WITH FRAME f-top1.
          APPLY "entry" TO s-prod IN FRAME f-top1.
          NEXT.
        END.
        ELSE IF (LASTKEY = 501 OR
                 LASTKEY = 502 OR
                 LASTKEY = 507 OR
                 LASTKEY = 508) AND v-mfgbrowseopen = TRUE THEN DO:
          
          RUN MoveMfgLu (INPUT LASTKEY).
          ASSIGN v-mfgbrowsekeyed = TRUE.
          NEXT.
        END.
        ELSE
        IF KEYFUNCTION(LASTKEY) = "end-error" AND
           v-mfgbrowsekeyed    = TRUE THEN DO:
          ON cursor-up back-tab.
          ON cursor-down tab.
          HIDE FRAME f-models.
          NEXT.
        END.
        ELSE
        IF (LASTKEY = 13 OR KEYFUNCTION(LASTKEY) = "go")
           AND NOT v-mfgbrowsekeyed  THEN DO:
          ASSIGN v-model = INPUT v-model.
          RUN DisplayTop1.
          ASSIGN v-mfgbrowsekeyed = FALSE.
          CLOSE QUERY q-models.
          ON cursor-up back-tab.
          ON cursor-down tab.
          HIDE FRAME f-models.
        END.
        ELSE
        IF (LASTKEY = 13 OR KEYFUNCTION(LASTKEY) = "go")
          AND v-mfgbrowsekeyed AND avail zsdirefty THEN DO:
          ASSIGN v-model = zsdirefty.reference.
          DISPLAY v-model WITH FRAME f-top1.
          ASSIGN v-mfgbrowsekeyed = FALSE.
          CLOSE QUERY q-models.
          ON cursor-up back-tab.    /**&&**/
          ON cursor-down tab.
          HIDE FRAME f-models.
        END.
        ELSE DO:
          ASSIGN v-mfgbrowsekeyed = FALSE.
          HIDE FRAME f-models.
        END.
      END.
      IF FRAME-FIELD = "v-custpo" THEN DO:
        IF (NOT CAN-DO("9,21,13,501,502,503,503,507,508",STRING(LASTKEY)) AND
            KEYFUNCTION(LASTKEY) NE "go" AND
            KEYFUNCTION(LASTKEY) NE "end-error") THEN DO:
          ASSIGN v-custpo = INPUT v-custpo.
        END.
      END.
      
      IF FRAME-FIELD = "s-prod" AND
      (CAN-DO('f7',KEYLABEL(LASTKEY))     OR
       CAN-DO('f8',KEYLABEL(LASTKEY))     OR
       CAN-DO('f9',KEYLABEL(LASTKEY))     OR
       CAN-DO('f10',KEYLABEL(LASTKEY))    OR
       CAN-DO('ctrl-r',KEYLABEL(LASTKEY)) OR
       CAN-DO('ctrl-d',KEYLABEL(LASTKEY)) OR
       CAN-DO('ctrl-o',KEYLABEL(LASTKEY))) THEN
        ASSIGN s-prod = INPUT s-prod.
      ELSE
      IF FRAME-FIELD = "s-prod" THEN DO:
        IF NOT CAN-DO("9,13,401,404,501,502,503,504,508,507",STRING(LASTKEY))
        THEN
          ASSIGN v-prodbrowsekeyedt = FALSE.
        IF (NOT CAN-DO("9,21,13,501,502,503,504,507,508",STRING(LASTKEY)) AND
            KEYFUNCTION(LASTKEY) NE "go" AND
            KEYFUNCTION(LASTKEY) NE "end-error") THEN DO:
          APPLY LASTKEY.
          IF FRAME-FIELD <> "s-prod" THEN
            NEXT.
          ASSIGN s-prod = INPUT s-prod.
          /** Product lookup by keystroke logic **/
          IF NOT funcflag THEN
            RUN DoProdLus (INPUT s-prod,
                           INPUT "normal").
          ELSE
            ASSIGN funcflag = NO.
          NEXT.
        END.
        IF (((KEYLABEL(LASTKEY) = "cursor-left" OR
              LASTKEY = 501)  AND
              s-prod:CURSOR-OFFSET = 1) OR
             (s-prod:CURSOR-OFFSET = s-prod:WIDTH-CHARS AND
              KEYLABEL(LASTKEY) = "CURSOR-RIGHT")) AND
              v-prodbrowsekeyedt = FALSE THEN DO:
          /* %%tom */
          IF (o-partno NE INPUT s-prod AND o-partno <> "") AND
            g-fabquoteno NE "" THEN DO:
            ASSIGN v-chgfl = YES.
            RUN ProductChange (INPUT-OUTPUT v-chgfl).
            ASSIGN v-prodbrowseopent = FALSE.
            HIDE FRAME f-prodlu.
            HIDE FRAME f-whses.
            ON cursor-up back-tab.
            ON cursor-down tab.
            IF v-chgfl then DO:
              ASSIGN o-partno = input s-prod.
              ASSIGN promptpos = 8
                     v-error = "Part# has been modified".
            END.
            NEXT  Top1loop.
          END.
          ELSE
          IF (o-partno NE INPUT s-prod AND o-partno <> "")  THEN
            RUN sProductCheck.
          IF INPUT s-prod = "" THEN DO:
            ASSIGN v-astr-top = "".
            DISPLAY v-astr-top WITH FRAME f-top1.
          END.
          /* %%tom */
          ON cursor-up back-tab.
          ON cursor-down tab.
          ASSIGN v-prodbrowseopent = FALSE.
          HIDE FRAME f-prodlu.
          HIDE FRAME f-whses.
          IF KEYLABEL(LASTKEY) = "CURSOR-RIGHT" THEN
            NEXT-PROMPT g-fabquoteno WITH FRAME f-top1.
          ELSE DO:
            NEXT-PROMPT v-model WITH FRAME f-top1.
            APPLY "entry" to v-model in FRAME f-top1. 
          END.  
          NEXT.
        END.
        ELSE IF (LASTKEY = 501 OR
                 LASTKEY = 502 OR
                 LASTKEY = 507 OR
                 LASTKEY = 508) AND v-prodbrowseopent = TRUE THEN DO:
          ON cursor-up cursor-up.
          ON cursor-down cursor-down.
          RUN MoveProdLus (INPUT LASTKEY).
          ASSIGN v-prodbrowsekeyedt = TRUE.
          NEXT.
        END.
        ELSE
        IF (LASTKEY = 13 OR KEYFUNCTION(LASTKEY) = "go") THEN DO:
          IF v-prodbrowsekeyedt = FALSE AND
            (avail vasp AND vasp.refer NE INPUT s-prod) THEN DO:
            FIND icsp WHERE icsp.cono = g-cono AND
                            icsp.prod = INPUT s-prod NO-LOCK NO-ERROR.
            IF NOT avail icsp THEN DO:
              ASSIGN v-astr-top = "".
              DISPLAY v-astr-top WITH FRAME f-top1.
            END.
              ASSIGN s-prod  = IF avail icsp THEN
                                 icsp.prod
                               ELSE
                                 s-prod.
            ASSIGN v-astr-top = "".
            RUN NoteDisplay3(INPUT s-prod).
            ASSIGN v-astr-top = v-astr3.
            DISPLAY v-astr-top WITH FRAME f-top1.
            IF (o-partno NE s-prod AND o-partno <> "") AND
                g-fabquoteno NE "" THEN DO:
              ASSIGN v-chgfl = YES.
              RUN ProductChange (INPUT-OUTPUT v-chgfl).
              ASSIGN v-prodbrowseopent = FALSE.
              HIDE FRAME f-prodlu.
              HIDE FRAME f-whses.
              ON cursor-up back-tab.
              ON cursor-down tab.
              if v-chgfl then DO:
                ASSIGN o-partno =  s-prod.
                ASSIGN promptpos = 8
                       v-error = "Part# has been modified".
              END.
              NEXT  Top1loop.
            END.
            ELSE
            IF (o-partno NE INPUT s-prod AND o-partno <> "")  THEN
              RUN sProductCheck.
            IF o-partno = "" THEN
              o-partno = s-prod.
            DISPLAY s-prod WITH FRAME f-top1.
          END.
          ELSE DO:
            ASSIGN s-prod    = IF avail icsp THEN
                                 icsp.prod
                               ELSE
                                 s-prod.
            IF (o-partno NE s-prod AND o-partno <> "") AND
                g-fabquoteno NE "" THEN DO:
              ASSIGN v-chgfl = YES.
              RUN ProductChange (INPUT-OUTPUT v-chgfl).
              ASSIGN v-prodbrowseopent = FALSE.
              HIDE FRAME f-prodlu.
              HIDE FRAME f-whses.
              ON cursor-up back-tab.
              ON cursor-down tab.
              IF v-chgfl then DO:
                ASSIGN o-partno = input s-prod.
                ASSIGN promptpos = 8
                       v-error = "Part# has been modified".
              END.
              NEXT  Top1loop.
            END.
            ELSE
            IF (o-partno NE INPUT s-prod AND o-partno <> "")  THEN
              RUN sProductCheck.
            IF o-partno = "" THEN
              o-partno = s-prod.
            DISPLAY s-prod WITH FRAME f-top1.
          END.
          ASSIGN v-prodbrowsekeyedt = FALSE.
          HIDE FRAME f-prodlu.
          HIDE FRAME f-whses.
          APPLY LASTKEY.
        END. /* last key 13 or go or v-applygo */
      END.
      
      IF FRAME-FIELD = "v-Shipto" AND
        (CAN-DO('f7',KEYLABEL(LASTKEY))     OR
         CAN-DO('f8',KEYLABEL(LASTKEY))     OR
         CAN-DO('f9',KEYLABEL(LASTKEY))     OR
         CAN-DO('f10',KEYLABEL(LASTKEY))    OR
         CAN-DO('ctrl-r',KEYLABEL(LASTKEY)) OR
         CAN-DO('ctrl-d',KEYLABEL(LASTKEY)) OR
         CAN-DO('ctrl-o',KEYLABEL(LASTKEY))) THEN DO:
        ASSIGN v-shipto = INPUT v-shipto.
      END.
      ELSE
      IF FRAME-FIELD = "v-Shipto" THEN DO:
        HIDE FRAME f-bot2.
        IF NOT CAN-DO("9,13,401,404,501,502,503,504,508,507",STRING(LASTKEY))
        THEN
          ASSIGN v-Shiptobrowsekeyed = FALSE.
        IF (NOT CAN-DO("9,21,13,501,502,503,504,507,508",STRING(LASTKEY)) AND
            KEYFUNCTION(LASTKEY) NE "go" AND
            KEYFUNCTION(LASTKEY) NE "end-error") THEN DO:
          /* user has keyed in non-movement key */
          APPLY LASTKEY.
          IF FRAME-FIELD <> "v-Shipto" THEN
            NEXT.
          ASSIGN v-Shipto = INPUT v-Shipto.
          RUN DoShiptoLu (INPUT v-Shipto,
                          INPUT "normal",
                          INPUT "f-top1" ).
          HIDE FRAME f-mid1.
          HIDE FRAME f-lines.
          ASSIGN v-Shiptobrowsekeyed = FALSE.
          NEXT.
        END.
        ELSE
        IF (KEYLABEL(LASTKEY) = "cursor-left" OR
            LASTKEY = 501)  AND
            v-shipto:CURSOR-OFFSET = 1 AND
            v-shiptobrowsekeyed = FALSE THEN DO:
          ON cursor-up back-tab.
          ON cursor-down tab.
          ASSIGN v-shiptobrowseopen = FALSE.
          HIDE FRAME f-shiptolu.
          HIDE FRAME f-zcustinfo.
          NEXT-PROMPT v-custnoa WITH FRAME f-top1.
          APPLY "entry" TO v-custnoa IN FRAME f-top1.
          NEXT.
        END.
        ELSE IF (LASTKEY = 501 OR
                 LASTKEY = 502 OR
                 LASTKEY = 507 OR
                 LASTKEY = 508) AND v-Shiptobrowseopen = TRUE THEN DO:
          RUN MoveShiptoLu (INPUT LASTKEY,
          INPUT "f-top1").
          ASSIGN v-Shiptobrowsekeyed = TRUE.
          NEXT.
        END.
        ELSE
        IF KEYFUNCTION(LASTKEY) = "end-error" AND
           v-shiptobrowsekeyed    = TRUE THEN DO:
          ON cursor-up back-tab.
          ON cursor-down tab.
          ASSIGN v-Shiptobrowsekeyed = FALSE.
          HIDE FRAME f-Shiptolu.
          HIDE FRAME f-zcustinfo.
          NEXT.
        END.
        ELSE
        IF (LASTKEY = 13 OR KEYFUNCTION(LASTKEY) = "go")
        AND NOT v-Shiptobrowsekeyed  THEN DO:
          ASSIGN v-shipto = INPUT v-shipto.
          {w-arss.i v-custno v-shipto "no-lock"}
          IF v-shipto NE "" AND NOT avail arss THEN DO:
            ASSIGN v-error = " Invalid shipto - pls enter.".
            NEXT-PROMPT v-shipto WITH FRAME f-top1.
            NEXT.
          END.
          IF NOT avail arss THEN
            {w-arsc.i v-custno "no-lock"}
          ASSIGN v-Shipto   = INPUT v-Shipto
          /*   v-binloc   = input v-binloc */
            v-custname = IF avail arss THEN
                           arss.NAME
                         ELSE
                         IF avail arsc THEN
                           arsc.NAME
                         ELSE
                          v-custname.
          RUN DisplayTop1.
          ASSIGN v-Shiptobrowsekeyed = FALSE.
          CLOSE QUERY q-Shiptolu.
          ON cursor-up back-tab.
          ON cursor-down tab.
          HIDE FRAME f-Shiptolu.
          HIDE FRAME f-zcustinfo.
        END.
        ELSE
        IF (LASTKEY = 13 OR KEYFUNCTION(LASTKEY) = "go")
           AND v-Shiptobrowsekeyed AND avail arss THEN DO:
          ASSIGN v-Shipto = arss.Shipto.
          DISPLAY 
            v-Shipto
            v-custname
          WITH FRAME f-top1.
          ASSIGN v-Shiptobrowsekeyed = FALSE.
          CLOSE QUERY q-Shiptolu.
          ON cursor-up back-tab.    /**&&**/
          ON cursor-down tab.
          HIDE FRAME f-Shiptolu.
          HIDE FRAME f-zcustinfo.
        END.
        ELSE
          ASSIGN v-Shiptobrowsekeyed = FALSE.
      END.
      
      
      IF  CAN-DO('ctrl-d',KEYLABEL(LASTKEY)) OR
          CAN-DO('ctrl-r',KEYLABEL(LASTKEY)) OR
          CAN-DO('f8',KEYLABEL(LASTKEY))     OR
          CAN-DO('f10',KEYLABEL(LASTKEY)) THEN DO:
        RUN WhatPrompt.
      END. /* can-do ctrl-d f8 */
      

      IF g-fabquoteno = "" AND
            
        ({k-sdileave.i
            &functions  = " can-do('f7',keylabel(lastkey))     or
                            can-do('f8',keylabel(lastkey))     or
                            can-do('f9',keylabel(lastkey))     or
                            can-do('f10',keylabel(lastkey))    or
                            CAN-DO('ctrl-r',KEYLABEL(LASTKEY)) OR
                            can-do('ctrl-d',keylabel(lastkey)) or
                            can-do('ctrl-o',keylabel(lastkey)) or "
            &fieldname     = "v-model"
            &completename  = "v-model"})  THEN DO:
        ASSIGN v-model = INPUT v-model.
               v-lastkey = lastkey.
        FIND zsdirefty WHERE
             zsdirefty.cono       = g-cono   AND
             zsdirefty.rectype    = "va"     AND
             zsdirefty.reference  = TRIM(v-model)
        NO-LOCK NO-ERROR.
        IF NOT avail zsdirefty THEN DO:
          ASSIGN mx = YES.
          MESSAGE " This Unit Type does not exist would"
            SKIP
            " you like to add it to the list?   "
            SKIP
            " Enter-Adds F4-Skips              "
            SKIP
          VIEW-AS ALERT-BOX QUESTION BUTTONS OK
          UPDATE mx.
          IF mx THEN DO:
            RUN makenew(INPUT "p").
            DISPLAY v-model WITH FRAME f-top1.
            /*
            apply v-lastkey.
tahtrain    */
            assign promptpos = 7.
            next top1loop.
            /*
            next.
            */
          END.
          ELSE DO:
            DISPLAY v-model WITH FRAME f-top1.
            apply v-lastkey.
            next.
          END.
        END.
      END.

      
      
      
      
      
      
      APPLY LASTKEY.
    END. /* ^^^^^^  end editing loop  ^^^^^ */
  
    IF {k-after.i} OR
       (CAN-DO("RETURN",KEYLABEL(LASTKEY)) AND
                FRAME-FIELD = "s-prod")  OR
        (CAN-DO("RETURN",KEYLABEL(LASTKEY))  AND
          FRAME-FIELD = "v-model" and
          s-prod = "") or 
                
        CAN-DO('f7',KEYLABEL(LASTKEY))     OR
        CAN-DO('f8',KEYLABEL(LASTKEY))     OR
        CAN-DO('f9',KEYLABEL(LASTKEY))     OR
        CAN-DO('ctrl-r',KEYLABEL(LASTKEY)) OR
        CAN-DO('f10',KEYLABEL(LASTKEY))    OR
        CAN-DO('ctrl-o',KEYLABEL(LASTKEY)) THEN DO:
      RUN makequoteno(g-fabquoteno,OUTPUT x-quoteno).
      FIND FIRST vasp WHERE
                 vasp.cono     = g-cono AND
                 vasp.shipprod = x-quoteno
      NO-LOCK NO-ERROR.
      
      IF v-takenby = "" THEN DO:
        ASSIGN v-error = " Required TknBy - please enter.".
        PUT SCREEN ROW 23 COL 1 v-error.
        ASSIGN 
          promptpos = 3
          v-framestate = "t1".
        NEXT.
      END.
      
      IF x-whse = "" THEN DO:
        {w-sasoo.i g-operinits NO-LOCK}
        ASSIGN x-whse  = IF avail sasoo AND
                            sasoo.whse NE "" THEN
                           sasoo.whse
                         ELSE
                           INPUT x-whse.
        DISPLAY x-whse WITH FRAME f-top1.
      END.
      /*** need an edit on vast  info ****/
      {w-icsd.i x-whse NO-LOCK}
      IF (x-whse = "" OR NOT avail icsd) THEN DO:
        PUT SCREEN ROW 23 COL 1
          "Valid Whse required field - please enter. " + x-whse.
        ASSIGN v-error = "Valid Whse required field - please enter. "
          + x-whse.
        BELL.
        NEXT-PROMPT x-whse WITH FRAME f-top1.
        ASSIGN promptpos = 2. 
        NEXT.
      END.
      ELSE
        DISPLAY x-whse WITH FRAME f-top1.
      DISPLAY x-whse WITH FRAME f-top1.
      ASSIGN v-found = IF x-whse NE "" THEN
                         YES
                       ELSE
                          NO.
      IF NOT CAN-DO('f10',KEYLABEL(LASTKEY)) THEN DO:
        IF (v-custno <= 0 OR
        NOT CAN-FIND(arsc WHERE arsc.cono   = g-cono AND
                                 arsc.custno = v-custno NO-LOCK)) THEN DO:
          ASSIGN v-error = " Customer# required - please enter.".
          PUT SCREEN ROW 23 COL 1 " Customer# required - please enter.".
          NEXT-PROMPT v-custnoa WITH FRAME f-top1.
          ASSIGN promptpos = 4. 
          NEXT.
        END.
        
        IF v-model = "" THEN DO:
          ASSIGN v-error = " Required Unit Type - please enter.".
          PUT SCREEN ROW 23 COL 1 v-error.
          NEXT-PROMPT v-model WITH FRAME f-top1.
          ASSIGN promptpos = 7. 
          NEXT.
        END.
      END.
    END. /* End of k-after If - editing checks  */

    IF (CAN-DO('f7',KEYLABEL(LASTKEY))     OR
        CAN-DO('f8',KEYLABEL(LASTKEY))     OR
        CAN-DO('f9',KEYLABEL(LASTKEY))     OR
        CAN-DO('ctrl-r',KEYLABEL(LASTKEY)) OR
        CAN-DO('ctrl-o',KEYLABEL(LASTKEY))) AND g-fabquoteno NE "" THEN DO:
      RUN makequoteno(g-fabquoteno,OUTPUT x-quoteno).

      FIND FIRST vasp USE-INDEX k-vasp WHERE
                 vasp.cono     = g-cono    AND
                 vasp.shipprod = x-quoteno 
      NO-LOCK NO-ERROR.
      FIND zsdivasp  WHERE
           zsdivasp.cono     = g-cono    AND
           zsdivasp.rectype    = "fb"     AND
           zsdivasp.repairno = x-quoteno  NO-LOCK NO-ERROR.
      
      IF v-model NE zsdivasp.mfgname THEN DO:
        ASSIGN mx = YES.
        MESSAGE " Changing Unit Type please review  "
          SKIP
          " all description built line items."
          SKIP
          "    Continue with change - Ok?    "
          SKIP
        VIEW-AS ALERT-BOX QUESTION BUTTONS OK
        UPDATE mx.
        IF mx THEN DO:
          DISPLAY v-model WITH FRAME f-top1.
        END.
        ELSE DO:
          ASSIGN o-model = zsdivasp.mfgname
          v-model = o-model.
          DISPLAY v-model WITH FRAME f-top1.
          ASSIGN 
            v-lmode = "c"
            v-framestate = "t1".
          ASSIGN promptpos = 7.
          NEXT.
        END.
      END. /* if v-model */
      
      IF v-model NE zsdivasp.mfgname THEN DO:
        FIND zsdirefty WHERE
             zsdirefty.cono       = g-cono   AND
             zsdirefty.rectype    = "va"     AND
             zsdirefty.reference  = TRIM(v-model)
        NO-LOCK NO-ERROR.
        IF NOT avail zsdirefty THEN DO:
          ASSIGN mx = YES.
          MESSAGE " This Unit Type does not exist would"
            SKIP
            " you like to add it to the list?   "
            SKIP
            " Enter-Adds F4-Skips              "
            SKIP
          VIEW-AS ALERT-BOX QUESTION BUTTONS OK
          UPDATE mx.
          IF mx THEN DO:
            RUN makenew(INPUT "p").
            DISPLAY v-model WITH FRAME f-top1.
          END.
          ELSE DO:
            DISPLAY v-model WITH FRAME f-top1.
          END.
        END. /* if v-model */
      END.
      
      IF s-prod     NE vasp.refer            OR
         v-takenby  NE zsdivasp.takenby      OR
         v-revno    ne zsdivasp.xuser1       OR
         x-whse     ne zsdivasp.whse         OR
         v-model    NE zsdivasp.mfgname      OR
         v-custpo   NE zsdivasp.custpo       OR
         v-shipto   NE zsdivasp.shipto       OR
         v-custno NE zsdivasp.custno  THEN DO:
       
        RUN vaspfabqu.p(INPUT x-whse,
                        INPUT v-custno,
                        INPUT "chg",
                        INPUT v-model,
                        INPUT-OUTPUT x-quoteno,
                        OUTPUT v-tag2).
        RUN makequoteno(INPUT v-tag2,OUTPUT x-quoteno).

        RUN Total_TagValue.
        ASSIGN  
          o-model = v-model
          o-partno = s-prod
          v-found = YES
          g-fabquoteno = TRIM(v-tag2," ").
      END.
    END.
    
    ASSIGN  v-astr-top = "".
    DISPLAY v-astr-top WITH FRAME f-top1.
    FIND FIRST notes USE-INDEX k-notes WHERE
               notes.cono         = g-cono  AND
               notes.notestype    = "p"     AND
               notes.primarykey   = INPUT s-prod AND
               notes.secondarykey = ""
    NO-LOCK NO-ERROR.
    IF avail notes AND notes.requirefl = YES THEN DO:
      ASSIGN v-astr-top = "!".
      DISPLAY v-astr-top WITH FRAME f-top1.
    END.
    ELSE
    IF avail notes AND notes.requirefl = NO THEN DO:
      ASSIGN v-astr-top = "*".
      DISPLAY v-astr-top WITH FRAME f-top1.
    END.
    
    IF g-fabquoteno NE "" AND CAN-DO('ctrl-d',KEYLABEL(LASTKEY)) THEN DO:
      
      assign v-delmode = false.
      display v-delmode colon 30 
                       label "Send Quote to Lost Business" with frame del.
      update
        v-delmode label "Send Quote to Lost Business?"
        v-lostbusty colon 30 label "Lost Business Reason" validate
        (if not input v-delmode then true else {v-sasta.i E v-lostbusty "/*"},
        "Lost Business Reason Not Setup in System Table - SASTT (4009)")
        {f-help.i}
      with frame del side-labels row 5 centered overlay.
      hide frame del.
 
     /* 
      MESSAGE " Update quote to (Can)cel stage y/n? " + g-fabquoteno
        SKIP
      VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO
      UPDATE dx AS LOGICAL.
     */
      IF v-delmode THEN DO:
        FIND FIRST vasp USE-INDEX k-vasp WHERE
                   vasp.cono     = g-cono    AND
                   vasp.shipprod = x-quoteno 
        NO-ERROR.
        
        FIND FIRST zsdivasp WHERE
                   zsdivasp.cono = g-cono         AND
                   zsdivasp.rectype = "fb"        AND
                   zsdivasp.repairno = x-quoteno  
        NO-ERROR.
        
        IF avail vasp AND avail zsdivasp THEN DO:
          ASSIGN 
            zsdivasp.stagecd   = v-cancelled
            zsdivasp.costingdt = TODAY
            zsdivasp.xuser3    = v-lostbusty 
            zsdivasp.xuser4    = today
            vasp.user7 = v-cancelled
            vasp.user9 = TODAY.
          RUN create-cancel.

          FOR EACH t-lines use-index ix1 EXCLUSIVE-LOCK:
            DELETE t-lines.
          END.

          HIDE ALL.
          ASSIGN 
            g-fabquoteno  = ""
            v-found      = NO
            v-framestate = "t1".
          /* clear the fields the header is not the same as it was */
          ASSIGN 
            v-gp        = 0
            v-custnoa   = ""
            v-custno    = 0
            g-fabquoteno   = ""
            v-shipto    = ""
            v-revno     = ""
            v-model     = ""
            v-custname  = ""
            v-prod      = ""
            v-descrip   = ""
            v-descrip2  = ""
            v-custpo    = ""
            s-prod      = ""
            v-prodcat   = ""
            v-prodcost  = 0
            v-disc      = 0
            v-Intangables   = 0
            v-prodcost  = 0
            v-costgain  = 0
            v-leadtm    = ""
            v-prctype   = ""
            v-framestate = "ex".
          return.
         /* LEAVE Top1loop. */
        END.
      END.
    END.
    
    
    IF CAN-DO('ctrl-o',KEYLABEL(LASTKEY)) THEN DO:
      ASSIGN 
        v-error      = "Operator options set."
        v-framestate = "t1"
        g-fabquoteno  = g-fabquoteno
        o-custno     = v-custno
        o-vaquoteno  = TRIM(g-fabquoteno," ").
      RUN WhatPrompt.
      RUN operatorsettings.
      RETURN.
    END. /* can-do ctrl-o */
    
    IF CAN-DO('f8',KEYLABEL(LASTKEY)) THEN DO:
      FIND FIRST t-lines use-index ix1 NO-LOCK NO-ERROR.
      IF NOT avail t-lines THEN DO:
        RUN Create_browse_lines("yes").
        ASSIGN v-framestate = "m1".
      END.
      run WhatPrompt.
      RUN f8totals.
      ASSIGN v-framestate = "t1".
      FIND first OptionMoves WHERE
           OptionMoves.procedure  = v-framestate  NO-LOCK NO-ERROR.
      if avail OptionMoves then
        assign v-framestate = OptionMoves.PROCEDURE
               v-currentkeyplace = optionmoves.optionindex.
      
      IF LASTKEY = 404 THEN DO:
        PUT SCREEN  ROW 23 COL 1
          "  F4-cancel - from F8-Totals - pls check updates. ".
        PAUSE.
        ASSIGN v-framestate = "t1".
        readkey pause 0.    
        FIND first OptionMoves WHERE
             OptionMoves.procedure  = v-framestate  NO-LOCK NO-ERROR.
        if avail OptionMoves then
          assign v-framestate = OptionMoves.PROCEDURE
                 v-currentkeyplace = optionmoves.optionindex.

        /* LEAVE top1loop.  */
        return.
      END.
       /* LEAVE top1loop.  */
       return.
    END.

    IF CAN-DO('ctrl-r',KEYLABEL(LASTKEY)) THEN DO:
      ASSIGN
        v-framestate = "t1".
      assign s-verify         = yes
             refreshfl        = no
             v-linebrowseopen = no.
      run Refresh_Quote.
      assign s-pctrefresh = 0.
      readkey pause 0.
      ASSIGN
        v-framestate = "t1".
      DISPLAY b-lines WITH FRAME f-lines.

      return.

    end. /* if ctrl-r */

    
    if can-do('f10',keylabel(lastkey)) then do:
      if h-vaquoteno ne ""  and g-fabquoteno = "" then
        assign g-fabquoteno = h-vaquoteno.
      run vaexf-copy.p (input g-fabquoteno,
                        output s-lastkey).
      RUN makequoteno(INPUT g-fabquoteno,OUTPUT x-quoteno).

      FIND vasp  WHERE
           vasp.cono     = g-cono    AND
           vasp.shipprod = x-quoteno
      NO-LOCK NO-ERROR.
  
      FIND zsdivasp  WHERE
           zsdivasp.cono     = g-cono    AND
           zsdivasp.rectype  = "fb"      AND
           zsdivasp.repairno = x-quoteno
      NO-LOCK NO-ERROR.
      
      IF NOT avail vasp THEN DO:
        ASSIGN v-tagdt = TODAY.
        DISPLAY v-tagdt WITH FRAME f-top1.
      END.
      IF avail vasp  AND avail zsdivasp THEN DO:
        if vasp.user7 = 0 then
          find first t-stages no-lock no-error.
        else do:
          find first t-stages where 
                     t-stages.stage = int(vasp.user7)
          no-lock no-error.
          if not avail t-stages then
            find first t-stages where 
                       t-stages.stage >= int(vasp.user7)
            no-lock no-error.
        end.    
        ASSIGN 
          s-maxlead = INT(zsdivasp.leadtm1)
          p-pct     = zsdivasp.margpct
          v-lmode   = "c"
          v-vano    = vasp.user2
          v-revno   = if v-revno NE zsdivasp.xuser1 then
                        v-revno 
                      else
                        zsdivasp.xuser1
          v-stage   = t-stages.shortname
          v-tagdt   = zsdivasp.enterdt
          v-found   = YES
          v-takenby = IF v-takenby NE "" THEN
                        v-takenby
                      ELSE
                      IF v-takenby = "" AND
                         zsdivasp.takenby NE "" THEN
                        zsdivasp.takenby
                      ELSE
                        g-operinits
        
          v-custpo  = IF v-custpo NE zsdivasp.custpo THEN
                        v-custpo
                      ELSE
                        zsdivasp.custpo
          v-shipto  = IF v-shipto NE zsdivasp.shipto THEN
                        v-shipto
                      ELSE
                        zsdivasp.shipto
        
          v-custnoa = IF v-custnoa = "" THEN
                        STRING(zsdivasp.custno)
                      ELSE
                        v-custnoa
        
          v-custno = IF v-custno = 0 or v-custno = ? THEN
                       zsdivasp.custno
                     ELSE
                       v-custno.
        
        
        {w-arsc.i v-custno NO-LOCK}
        IF avail arsc THEN
          ASSIGN v-custname = arsc.NAME.
      end.
      RUN DisplayTop1.
      NEXT-PROMPT g-fabquoteno WITH FRAME f-top1.
  
      if {k-cancel.i "s-"} then do:
        v-framestate = "t1".
        next.
      end.
      v-framestate   = "t1".
      return.
    end.  
    
    IF CAN-DO('f7',KEYLABEL(LASTKEY)) OR
       CAN-DO('f9',KEYLABEL(LASTKEY)) THEN DO:
      RUN WhatPrompt.
      IF CAN-DO('f7',KEYLABEL(LASTKEY)) THEN DO:
        RUN Create_browse_lines("yes").
        ASSIGN v-framestate = "m1".
      END.
      ELSE
        ASSIGN v-framestate = "t1".
     
      LEAVE top1loop. 
    END.
    
    IF {k-after.i} AND v-custno = 0 THEN DO:
      FIND FIRST vasp USE-INDEX k-vasp WHERE
                 vasp.cono     = g-cono    AND
                 vasp.shipprod = x-quoteno 
      NO-LOCK NO-ERROR.
      FIND zsdivasp  WHERE
           zsdivasp.cono     = g-cono    AND
           zsdivasp.rectype  = "fb"      AND
           zsdivasp.repairno = x-quoteno
      NO-LOCK NO-ERROR.
      IF NOT avail vasp THEN DO:
        ASSIGN v-tagdt = TODAY.
        DISPLAY v-tagdt WITH FRAME f-top1.
      END.
      IF avail vasp AND avail zsdivasp THEN DO:
        ASSIGN 
          v-custno   = zsdivasp.custno
          v-custnoa  = STRING(zsdivasp.custno)
          v-takenby  = IF v-takenby NE "" THEN
                         v-takenby
                       ELSE
                       IF zsdivasp.takenby NE "" THEN
                         zsdivasp.takenby
                       ELSE
                         g-operinits
          v-revno    = if v-revno <> "" then
                         v-revno
                       else
                       if avail zsdivasp then
                         zsdivasp.xuser1
                       else
                         " "
          v-tagdt    = zsdivasp.enterdt
          v-vano     = vasp.user2
          s-prod     = zsdivasp.partno
          v-model    = zsdivasp.mfgname
          o-model    = v-model
          v-custpo   = zsdivasp.custpo
          v-shipto   = zsdivasp.shipto.
        {w-arsc.i v-custno NO-LOCK}
        IF avail arsc THEN 
          ASSIGN v-custname = arsc.NAME.
        RUN DisplayTop1.
        
      END. /* avail vasp */
      
    END. /* if k-after and custno = 0 */
    {w-arsc.i v-custno NO-LOCK}
    IF (NOT avail arsc AND v-custno > 0) AND {k-after.i} THEN DO:
      PUT SCREEN ROW 23 COL 1
        "Valid Customer required field - please enter.".
      ASSIGN v-error =  "Valid Customer required field - please enter.".
      NEXT-PROMPT v-custnoa WITH FRAME f-top1.
      NEXT.
    END.
    ELSE
    IF {k-after.i} AND v-custno = 0 THEN DO:
      PUT SCREEN ROW 23 COL 1
        "Valid Customer required field - please enter.".
      ASSIGN v-error =  "Valid Customer required field - please enter.".
      NEXT-PROMPT v-custnoa WITH FRAME f-top1.
      NEXT.
    END.
    
    IF {k-after.i} AND v-model = "" THEN DO:
      HIDE FRAME f-Shiptolu.
      HIDE FRAME f-zcustinfo.
      PUT SCREEN ROW 23 COL 1 "Required Unit Type - please enter.".
      ASSIGN v-error = "Required Unit Type - please enter.".
      NEXT-PROMPT v-model WITH FRAME f-top1.
      NEXT.
    END.
/* raised to edit loop   
    IF g-fabquoteno = "" AND
            
      ({k-sdileave.i
          &functions  = " can-do('f7',keylabel(lastkey))     or
                          can-do('f8',keylabel(lastkey))     or
                          can-do('f9',keylabel(lastkey))     or
                          can-do('f10',keylabel(lastkey))    or
                          can-do('ctrl-d',keylabel(lastkey)) or
                          can-do('ctrl-o',keylabel(lastkey)) or "
          &fieldname     = "v-model"
          &completename  = "v-model"}) or
       ({k-after.i} OR 
        (CAN-DO("RETURN",KEYLABEL(LASTKEY)) AND
           FRAME-FIELD = "s-prod") or
         (CAN-DO("RETURN",KEYLABEL(LASTKEY))  AND
            FRAME-FIELD = "v-model" and
            s-prod = "")) THEN  DO:
      ASSIGN v-model = INPUT v-model.
      FIND zsdirefty WHERE
           zsdirefty.cono       = g-cono   AND
           zsdirefty.rectype    = "va"     AND
           zsdirefty.reference  = TRIM(v-model)
      NO-LOCK NO-ERROR.
      IF NOT avail zsdirefty THEN DO:
        ASSIGN mx = YES.
        MESSAGE " This Unit Type does not exist would"
          SKIP
          " you like to add it to the list?   "
          SKIP
          " Enter-Adds F4-Skips              "
          SKIP
        VIEW-AS ALERT-BOX QUESTION BUTTONS OK
        UPDATE mx.
        IF mx THEN DO:
          RUN makenew(INPUT "p").
          DISPLAY v-model WITH FRAME f-top1.
        END.
        ELSE DO:
          DISPLAY v-model WITH FRAME f-top1.
        END.
      END.
    END.

*/    
    IF g-fabquoteno NE "" AND
       ({k-after.i} OR 
       (CAN-DO("RETURN",KEYLABEL(LASTKEY)) AND
        FRAME-FIELD = "s-prod") or
       (CAN-DO("RETURN",KEYLABEL(LASTKEY))  AND
         FRAME-FIELD = "v-model" and
        s-prod = "")) THEN DO:
      FIND FIRST vasp USE-INDEX k-vasp WHERE
                 vasp.cono     = g-cono    AND
                 vasp.shipprod = x-quoteno 
      NO-LOCK NO-ERROR.
      FIND zsdivasp  WHERE
           zsdivasp.cono     = g-cono    AND
           zsdivasp.rectype  = "fb"      AND
           zsdivasp.repairno = x-quoteno
      NO-LOCK NO-ERROR.
      
      IF NOT avail vasp THEN DO:
        ASSIGN v-tagdt = TODAY.
        DISPLAY v-tagdt WITH FRAME f-top1.
      END.
      IF avail vasp  AND avail zsdivasp THEN DO:
        if vasp.user7 = 0 then
          find first t-stages no-lock no-error.
        else do:
          find first t-stages where 
                     t-stages.stage = int(vasp.user7)
          no-lock no-error.
          if not avail t-stages then
            find first t-stages where 
                       t-stages.stage >= int(vasp.user7)
            no-lock no-error.
        end.    
        ASSIGN 
          s-maxlead = INT(zsdivasp.leadtm1)
          p-pct     = zsdivasp.margpct
          v-lmode   = "c"
          v-vano    = vasp.user2
          v-revno   = if v-revno NE zsdivasp.xuser1 then
                        v-revno 
                      else
                        zsdivasp.xuser1
          v-stage   = t-stages.shortname
          v-tagdt   = zsdivasp.enterdt
          v-found   = YES
          v-takenby = IF v-takenby NE "" THEN
                        v-takenby
                      ELSE
                      IF v-takenby = "" AND
                         zsdivasp.takenby NE "" THEN
                        zsdivasp.takenby
                      ELSE
                        g-operinits
        
          v-custpo  = IF v-custpo NE zsdivasp.custpo THEN
                        v-custpo
                      ELSE
                        zsdivasp.custpo
          v-shipto  = IF v-shipto NE zsdivasp.shipto THEN
                        v-shipto
                      ELSE
                        zsdivasp.shipto
        
          v-custnoa = IF v-custnoa = "" THEN
                        STRING(zsdivasp.custno)
                      ELSE
                        v-custnoa
        
          v-custno = IF v-custno = 0 or v-custno = ? THEN
                       zsdivasp.custno
                     ELSE
                       v-custno.
        
        
        {w-arsc.i v-custno NO-LOCK}
        IF avail arsc THEN
          ASSIGN v-custname = arsc.NAME.
        RUN DisplayTop1.
        IF v-model NE zsdivasp.mfgname THEN DO:
          ASSIGN mx = YES.
          MESSAGE " Changing Unit Type please review  "
            SKIP
            " all description built line items."
            SKIP
            "    Continue with change - Ok?    "
            SKIP
          VIEW-AS ALERT-BOX QUESTION BUTTONS OK
          UPDATE mx.
          IF mx THEN DO:
            DISPLAY v-model WITH FRAME f-top1.
            ASSIGN 
              v-lmode = "c"
              v-framestate = "m1".
          END.
          ELSE DO:
            ASSIGN 
              o-model = zsdivasp.mfgname
              v-model = o-model.
            DISPLAY v-model WITH FRAME f-top1.
            ASSIGN
              v-lmode = "c"
              v-framestate = "t1".
            NEXT-PROMPT v-model WITH FRAME f-top1.
            NEXT.
          END.
          
          FIND zsdirefty WHERE
               zsdirefty.cono       = g-cono   AND
               zsdirefty.rectype    = "va"     AND
               zsdirefty.reference  = TRIM(v-model)
          NO-LOCK NO-ERROR.
          IF NOT avail zsdirefty THEN DO:
            ASSIGN mx = YES.
            MESSAGE " This Unit Type does not exist would"
              SKIP
              " you like to add it to the list?   "
              SKIP
              " Enter-Adds F4-Skips              "
              SKIP
            VIEW-AS ALERT-BOX QUESTION BUTTONS OK
            UPDATE mx.
            IF mx THEN DO:
              RUN makenew(INPUT "p").
              DISPLAY v-model WITH FRAME f-top1.
              ASSIGN 
                v-lmode = "c"
                v-framestate = "m1".
            END.
            ELSE DO:
              DISPLAY v-model WITH FRAME f-top1.
              ASSIGN 
                v-lmode = "c"
                v-framestate = "m1".
            END.
          END.
        END. /* if v-model */
        
        
        IF s-prod     NE vasp.refer     OR
           v-revno    NE zsdivasp.xuser1     OR
           v-takenby  NE zsdivasp.takenby    OR
           v-model    NE zsdivasp.mfgname    OR
           x-whse     ne zsdivasp.whse       or
           v-custpo   NE zsdivasp.custpo     OR
           v-shipto   NE zsdivasp.shipto     OR
           v-custno   NE zsdivasp.custno THEN DO:
          RUN vaspfabqu.p(INPUT x-whse,
                          INPUT v-custno,
                          INPUT "chg",
                          INPUT v-model,
                          INPUT-OUTPUT x-quoteno,
                          OUTPUT v-tag2).
          RUN makequoteno(INPUT v-tag2,OUTPUT x-quoteno).
          RUN Total_TagValue.
          ASSIGN  
            o-model = v-model
            v-found = YES
            g-fabquoteno = TRIM(v-tag2," ").
        END.
        ELSE
          ASSIGN 
            v-lmode = "c"
            v-framestate = "m1".
      END. /* if avail vasp */
      ELSE DO:
        PUT SCREEN ROW 23 COL 1
        "Whse Tag Notfound - Fill in header info and F1 for new tag please.".
          ASSIGN 
            v-lmode   = "a"
            g-fabquoteno = ""
            v-found     = NO
            v-vano      = ""
            v-notes1tm  = YES.
          NEXT-PROMPT g-fabquoteno WITH FRAME f-top1.
          NEXT Top1loop.
      END.
    END.
    ELSE
    IF {k-after.i} AND g-fabquoteno = "" THEN DO:
      ASSIGN v-idx = 0.
      ASSIGN v-lmode    = "a".
      RUN vaspfabqu.p(INPUT x-whse,
                      INPUT v-custno,
                      INPUT "add",
                      INPUT v-model,
                      INPUT-OUTPUT x-quoteno,
                      OUTPUT v-tag2).
      ASSIGN 
        v-notes1tm  = YES
        g-fabquoteno = TRIM(v-tag2," ").
      PUT SCREEN ROW 23 COL 1
        " FabQuNo " + g-fabquoteno + " - F1 to continue. ".
      RUN makequoteno(INPUT v-tag2,OUTPUT x-quoteno).
      assign s-prod = ENTRY(1,x-quoteno,"-") +  "-" +
                      STRING(INT(ENTRY(2,x-quoteno,"-"))).
      assign s-LastFabNo = "Adding:  " + s-prod.
      
      DISPLAY g-fabquoteno s-LastFabNo WITH FRAME f-top1.
      RUN DisplayTop1.
      FIND FIRST vaspsl WHERE
                 vaspsl.cono      = g-cono    AND
                 vaspsl.shipprod  = x-quoteno AND
                 vaspsl.whse      = x-whse    and
                 (vaspsl.compprod  NE "fab"                   AND
                  vaspsl.compprod  NE "documentation charge"  AND
                  vaspsl.compprod  NE "Mechanical Charge"  AND
                  vaspsl.compprod  NE "Electrical charge"  AND
                  vaspsl.compprod  NE "fab cad"               AND
                  vaspsl.compprod  NE "fabrication fittings"  AND
                  vaspsl.compprod  NE "fab frt"               AND
                  vaspsl.compprod  NE "fab shop")      /*     and
                  substring(vaspsl.compprod,1,5) ne "core-"      */
      NO-LOCK NO-ERROR.
      IF NOT avail vaspsl THEN DO:
        IF v-custno NE 0 THEN DO:
          {w-arsc.i v-custno NO-LOCK}
          IF avail arsc THEN
            ASSIGN v-custname = arsc.NAME.
        END.
        IF NUM-ENTRIES(g-fabquoteno,"-") = 2 THEN DO:
          RUN bottom2.
        END.
        RUN Create_Browse_Lines(INPUT NO).
        IF NUM-ENTRIES(g-fabquoteno,"-") = 2 THEN DO:
          DEFINE BUFFER q-vasp FOR vasp.
          FIND FIRST q-vasp WHERE
                     q-vasp.cono     = g-cono AND
                     q-vasp.shipprod = x-quoteno
          EXCLUSIVE-LOCK NO-ERROR.
          FIND q-zsdivasp WHERE
               q-zsdivasp.cono     = g-cono AND
               q-zsdivasp.rectype  = "fb"   AND
               q-zsdivasp.repairno = x-quoteno
          EXCLUSIVE-LOCK NO-ERROR.
          IF avail q-zsdivasp AND q-zsdivasp.margpct = 0 THEN
          /* default the margin pct to 50 - here so vaepv reflects it
          also set the user field for inuse editing */
            ASSIGN q-zsdivasp.margpct = 035.00.
          RUN Total_TagValue.
/*
          {w-sasp.i q-printernm NO-LOCK}
          IF q-printernm NE "" AND avail sasp THEN DO:
            ASSIGN sendit = NO.
            RUN prt-routine2(INPUT q-printernm,INPUT YES).
  
            IF avail q-vasp AND q-vasp.notesfl = "!" THEN
            ASSIGN q-vasp.notesfl = "".
          END. /* avail q-vasp */
*/
            ASSIGN v-error = " FabQuNo " + g-fabquoteno + " Created".
          RUN zsdi_vaerrx.p(v-error,"yes").
/*          RUN prt-routine. */
          HIDE FRAME f-printer.
        END. /* if num-entries */
        
        ASSIGN v-currentKeyPlace = 1.
        RUN FindNextFrame (INPUT 401,
                           INPUT-OUTPUT v-CurrentKeyPlace,
                           INPUT-OUTPUT v-framestate).
        /* *** comment out line below if Kevin changes things. ***
        
        assign v-framestate = "t1".
        */
        IF v-framestate = "m1" THEN DO:
          ON cursor-up back-tab.
          ON cursor-down tab.
          DISPLAY fs-1 WITH FRAME f-statusline.
          ASSIGN 
            o-custno = v-custno
            o-vaquoteno = TRIM(g-fabquoteno," ").
          RUN DisplayTop1.
          RETURN.
        END.
        ELSE
        /* clear the fields the header is not the same as it was */
        ASSIGN 
          v-tagprt    = YES
          promptpos   = 0
          v-binloc    = ""
          v-model     = ""
          v-serial    = ""
          v-astr1     = ""
          v-astr2     = ""
          v-gp        = 0
          v-prod      = ""
          v-descrip   = ""
          v-descrip2  = ""
          s-prod      = ""
          v-prodcat   = ""
          v-prodcost  = 0
          v-disc      = 0
          v-Intangables   = 0
          v-prodcost  = 0
          v-costgain  = 0
          v-leadtm    = ""
          v-prctype   = ""
          g-fabquoteno = "".
        ON cursor-up back-tab.
        ON cursor-down tab.
        DISPLAY fs-1 WITH FRAME f-statusline.
        ASSIGN 
          o-custno = v-custno
          o-vaquoteno = TRIM(g-fabquoteno," ").
        RUN DisplayTop1.
        RUN makequoteno(o-vaquoteno,OUTPUT x-quoteno).
        RUN vaexfinuse.p(INPUT x-quoteno,
                         INPUT g-operinits,
                         INPUT "c",
                         INPUT-OUTPUT v-inuse).
        RETURN.
      END.  /* not avail vaspsl */
    END. /* go and g-fabquoteno = space */
    
    ASSIGN 
      o-custno = v-custno
      o-vaquoteno = TRIM(g-fabquoteno," ").
    {w-arsc.i v-custno NO-LOCK}
    IF avail arsc THEN
      ASSIGN v-custname = arsc.NAME.
    RUN DisplayTop1.
    RUN makequoteno(INPUT g-fabquoteno,OUTPUT x-quoteno).
    IF v-found THEN
    /* standard procedure to run line creation for tag and sealkit */
      RUN Create_Browse_Lines(INPUT v-found).
    IF {k-after.i} AND v-found THEN DO:
      ASSIGN 
        v-lmode = "c"
        v-framestate = "m1"
        v-CurrentKeyPlace = 1.
      DISPLAY b-lines WITH FRAME f-lines.
    END.
    
    {k-sdinavigation.i &jmpcode = "return."}
    IF ({k-accept.i} OR 
       (CAN-DO("RETURN",KEYLABEL(LASTKEY)) AND
         FRAME-FIELD = "s-prod") or
       (CAN-DO("RETURN",KEYLABEL(LASTKEY))  AND
         FRAME-FIELD = "v-model" and
         substring(s-LastFabNo,1,7) = "Adding:"))       
          
          AND v-custno NE 0 THEN DO:
      IF v-notes1tm = NO THEN DO:
        HIDE FRAME f-lines.
        IF KEYFUNCTION(LASTKEY) = "go" THEN DO:
          /*
          RUN cust-comments.
          */
          ASSIGN v-notes1tm = YES.
        END.
        ELSE DO:
          /*
          RUN cust-comments.
          */
          ASSIGN v-notes1tm = YES.
/*          ASSIGN v-framestate = "m1". */
          leave.
        END.
      END.
/*      ASSIGN v-framestate = "m1".     */
      leave.
    END.
  END. /* end of Top1Loop */
  
  {k-sdinavigation.i &jmpcode = "return." }
  IF {k-jump.i} OR {k-cancel.i} THEN DO:
    RUN FindNextFrame (INPUT LASTKEY,
                       INPUT-OUTPUT v-CurrentKeyPlace,
                       INPUT-OUTPUT v-framestate).
    ASSIGN v-framestate = "xx".
    LEAVE.
  END.
  ELSE
  IF (CAN-DO('f7',KEYLABEL(LASTKEY))      OR
      CAN-DO('f8',KEYLABEL(LASTKEY))      OR
      CAN-DO('f9',KEYLABEL(LASTKEY))      OR
      CAN-DO('ctrl-o',KEYLABEL(LASTKEY))) THEN DO:
    RUN FindNextFrame (INPUT LASTKEY,
                       INPUT-OUTPUT v-CurrentKeyPlace,
                       INPUT-OUTPUT v-framestate).
  END.
  ELSE do:
    RUN FindNextFrame (INPUT 401,
                       INPUT-OUTPUT v-CurrentKeyPlace,
                       INPUT-OUTPUT v-framestate).
  end.
END.  /* end Top1 procedure */


