/*
 
 Standard Navigation for all Frame sections                          

 {&prefix}    = Lastkey variable if lastkey was not used
 {&jmpcode}   = Statement(s) of the action(s) taken after framestate is set
 {&staycode}  = Statement(s) of the action(s) taken after null action is       
                determined                                                     
*/

if keylabel({&prefix}lastkey) = "F6" and v-framestate <> "T1" then
   do:
   assign v-framestate = "t1"
          v-ManualFrameJump  = true.
   {&jmpcode}
   end.
else 
if keylabel({&prefix}lastkey) = "F7" and v-framestate <> "M1" then
   do:
   assign v-framestate = "m1"
          v-ManualFrameJump  = true.
   {&jmpcode}
   end.
else 
if keylabel({&prefix}lastkey) = "F9" and v-framestate <> "B2" then
   do:
   assign v-framestate = "B2"
          v-ManualFrameJump  = true.
   {&jmpcode}
   end.
else 
if keylabel({&prefix}lastkey) = "F6" and v-framestate = "T1" or
   keylabel({&prefix}lastkey) = "F7" and v-framestate = "M1" or
   keylabel({&prefix}lastkey) = "F9" and v-framestate = "B2" then
   do:
   {&staycode}
   next.
   end.
