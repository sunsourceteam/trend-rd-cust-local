/* p-zicsd.i   */

assign g-whse = substring (o-frame-value,1,4).   

find icsw use-index k-prodco
     where      icsw.prod = g-prod and
                icsw.cono = g-cono and
                icsw.whse = g-whse  no-lock no-error.  
find icsd where icsd.cono = g-cono and
                icsd.whse = g-whse no-lock no-error.