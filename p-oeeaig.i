/* p-oeeaig.i 1.1 01/03/98 */
/* p-oeeaig.i 1.1 01/03/98 */
/*h*****************************************************************************  INCLUDE      : p-oeepaig.i
  DESCRIPTION  : Fields used in the "Terms" record in the edi 810, 843, and 855
  USED ONCE?   : no
  AUTHOR       : jlc
  DATE WRITTEN : 02/23/94
  CHANGES MADE :
*******************************************************************************/
"Terms " +
    caps(s-termstpcd) +
    caps(s-termsdtcd) +
    caps(s-termsdiscpct) +
    caps(s-discduedt) +
    caps(s-discdaysdue) +
    caps(s-netduedt) +
    caps(s-netdaysdue) +
    caps(s-terms) +
    caps(s-discproxday)
