/* p-oeeaie.i 1.1 01/03/98 */
/* p-oeeaie.i 1.1 01/03/98 */
/*h*****************************************************************************  INCLUDE      : p-oeepaie.i
  DESCRIPTION  : Fields used in the "Billto" record in the edi 810, 843, and 855
  USED ONCE?   : no
  AUTHOR       : jlc
  DATE WRITTEN : 02/23/94
  CHANGES MADE :
    TB# e17245 lbr; Added billto dunsno
    TB# e18165 08/10/03 jlc; Add additional identifiers for eBill
*******************************************************************************/
"Billto" +
     caps(s-invname) +
     caps(s-invaddr[1]) +
     caps(s-invaddr[2]) +
     caps(s-invcity) +
     caps(s-invstate) +
     caps(s-invzipcd) +
     caps(s-invphone) +
     caps(s-invuser1) +
     caps(s-invuser2)  /* +
     caps(s-billtodunsno) +
     caps(s-billtoerpid)  +
     caps(s-billtotpid) */