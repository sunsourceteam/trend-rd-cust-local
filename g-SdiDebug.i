/* --------------------------------------------------------------------------
   Include Name : g-SdiDebug.i
   Description  : This include contains all variables that are associated
                  with debug logging. You need a database connection to 
                  process the p-SdiDebug.i Procedures.
   -------------------------------------------------------------------------- */ 
/* Debug variables are internal storage for debug processes */

Define {1} Shared Var v-debug-logfilesize as integer               no-undo.
Define {1} Shared Var v-debug-logfilename as char                  no-undo.
Define {1} Shared Var v-debug-loglevel as char                     no-undo.

/* Setup Variables are storage for database setting for the appropriate
   API / Program */
   
Define {1} Shared Var v-setup-logmaxfilesize as integer             no-undo.
Define {1} Shared Var v-setup-logfilename    as char                no-undo.
Define {1} Shared Var v-setup-logfilefull    as char                no-undo.
Define {1} Shared Var v-setup-objectname     as char                no-undo.
Define {1} Shared Var v-setup-loglevel       as char                no-undo.
Define {1} Shared Var v-setup-log-dir        as char                no-undo.
Define {1} Shared Var v-setup-log-text       as char                no-undo.
Define {1} Shared Var v-setup-debugfl        as logical             no-undo
                                             init true. 
Define {1} Shared Stream Debugmylog.

