&if defined(user_temptable) = 2 &then
 
Define Var b-buyer      like icsl.buyer     no-undo.
Define Var e-buyer      like icsl.buyer     no-undo.
Define Var b-buygrp     like icsl.buyer     no-undo.
Define Var e-buygrp     like icsl.buyer     no-undo.
Define Var b-buydept    like oimsp.perdept  no-undo.
Define Var e-buydept    like oimsp.perdept  no-undo.
Define Var b-whse       like icsd.whse      no-undo.
Define Var e-whse       like icsd.whse      no-undo.
Define Var b-product    like icsp.prod      no-undo.
Define Var e-product    like icsp.prod      no-undo.
Define Var b-prodline   like icsl.prodline  no-undo.
Define Var e-prodline   like icsl.prodline  no-undo.
Define Var b-vendno     like apsv.vendno    no-undo.
Define Var e-vendno     like apsv.vendno    no-undo.
Define Var b-prodcat    like icsp.prodcat   no-undo.
Define Var e-prodcat    like icsp.prodcat   no-undo.
Define Var b-vp         as character format "x(24)" no-undo.
Define Var e-vp         as character format "x(24)" no-undo.
Define Var b-vid        as character format "x(24)" no-undo.
Define Var e-vid        as character format "x(24)" no-undo.
Define Var b-tech       as character format "x(24)" no-undo.
Define Var e-tech       as character format "x(24)" no-undo.
Define Var p-format     as  character       no-undo.
Define Var p-period     as  integer         no-undo.
Define Var p-list       as  logical         no-undo.
Define Var p-year       as  integer         no-undo.
Define Var p-mth        as  integer         no-undo.
Define Var p-regval     as  character       no-undo.
Define Var p-minmax     as  logical         no-undo. 
Define Var y-inx        as  integer         no-undo.
Define Var y-yr         as  integer         no-undo.
Define buffer catmaster for notes.
Define var v-vendorid   as c format "x(24)" no-undo.
Define var v-parentcode as c format "x(24)" no-undo.
Define var v-technology as c format "x(24)" no-undo.
Define var        w-buygrp     like icsl.buyer     no-undo.
Define var        w-buyer      like icsl.buyer     no-undo.
Define var        w-vendno     like icsw.arpvendno no-undo.
Define var        w-prodcat    like icsp.prodcat   no-undo.
Define var        w-prodline   like icsl.prodline  no-undo.
def var h-person     like oimsp.perdept no-undo.
Define var  x-qtyord   as decimal extent 4         no-undo.
Define var  x-qtybo    as decimal extent 4         no-undo.
Define var  x-ordcnt   as decimal extent 4         no-undo.
Define var  x-fillcnt  as decimal extent 4         no-undo.
 
{zsapboydef.i}
define {&sharetype} temp-table xfill no-undo
  field module   as character  format "x(3)"
  field buygrp   as character  format "x(4)"
  field buydept  as character  format "x(4)"
  field buyer    as character format "x(4)"
  field vendno   as dec       format ">>>>>>>>>9"  
  field prodcat  as character format "x(4)"
  field tech     as character format "x(4)"
  field vendid   as character format "x(24)"
  field vendpnt  as character format "x(24)"
  field prodline like icsl.prodline 
  field whse     like icsd.whse
  field prod     like icsw.prod
  field descr    like icsp.descrip
  field qtyord   as decimal extent 4
  field qtybo    as decimal extent 4
  field ordcnt   as decimal extent 4
  field fillcnt  as decimal extent 4
  
  index xx
     module
     whse
     prod.
 
 
 
 
 
 
 
&endif
 
&if defined(user_drpttable) = 2 &then
/* Use to add fields to the report table */
  field xrecid   as recid
 
 
 
 
 
 
 
&endif
 
&if defined(user_forms) = 2 &then
form header
  "~015 " at 1
  with frame fx-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: TBXP"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 168
  page-number(xpcd)    at 173 format ">>>9"
  "~015"               at 177
         "Stock Item Fill Rate Report" at 71
         "~015"              at 177
   with frame fx-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header
  "~015 " at 1
  with frame f-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: TBXP"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 168
  page-number          at 173 format ">>>9"
  "~015"              at 177
  "Stock Item Fill Rate Report" at 71
  "~015"              at 177
   with frame f-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header 
   "~015"                    at 177
  with frame f-trn3 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
            
form header 
   "Product"                    at 7
   "Description"                at 32
   "----------MTD------------"  at 59
   "----------YTD-----------"   at 85
   "--------LAST MTD--------"   at 111
   "--------LAST YTD--------"   at 137
   "~015"                       at 177
  with frame f-trn4p down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header 
   "  Order     Fill     Fill"  at 59
   "  Order     Fill    Fill"   at 85
   " Order     Fill     Fill"   at 111
   "  Order     Fill    Fill"   at 137
   "~015"                       at 177
  with frame f-trn5 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
form header 
   " Ln Cnt   Ln Cnt     Rate"  at 59
   " Ln Cnt   Ln Cnt    Rate"   at 85
   "Ln Cnt   Ln Cnt     Rate"   at 111
   " Ln Cnt   Ln Cnt    Rate"   at 137
   
   "~015"                    at 177
  with frame f-trn6 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
            
form header 
   "~015"                       at 177
  with frame f-trn7 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.
            
            
            
            
            
            
form
  v-final                at 1
  v-astrik               at 2   format "x(5)"
  v-summary-lit2         at 7   format "x(52)"
  v-amt1           at 59  format ">>>,>>9-"  /* count  mtd    */
  v-amt2           at 68  format ">>>,>>9-"  /* filled mtd    */
  v-amt3           at 78  format ">>9.99"    /* %      mtd    */
  v-amt4           at 85  format ">>>,>>9-"  /* count  ytd    */
  v-amt5           at 94  format ">>>,>>9-"  /* filled ytd    */
  v-amt6           at 103 format ">>9.99"    /* %      ytd    */
  v-amt7           at 110 format ">>>,>>9-"  /* count  lmtd   */
  v-amt8           at 119 format ">>>,>>9-"  /* filled lmtd   */
  v-amt9           at 129 format ">>9.99"    /* %      lmtd   */
  v-amt10          at 137 format ">>>,>>9-"  /* count  lytd   */
  v-amt11          at 146 format ">>>,>>9-"  /* filled lytd   */
  v-amt12          at 155 format ">>9.99"    /* %      lytd   */
  "~015"           at 178
with frame f-tot width 178
 no-box no-labels.  
form 
  "~015"           at 1
with frame f-skip width 178 no-box no-labels.  
 
 
 
 
 
 
&endif
 
&if defined(user_extractrun) = 2 &then
 run sapb_vars.
 run extract_data.
 
 
 
 
 
 
&endif
 
&if defined(user_exportstatDWheaders) = 2 &then
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  
  assign export_rec = export_rec + v-del +
                      "MTDOrdCnt"  + v-del +
                      "MTDFillCnt"   + v-del +
                      "MTDFillPct"     + v-del +
                      "YTDOrdCnt"  + v-del +
                      "YTDFillCnt"   + v-del +
                      "YTDFilPct"     + v-del +
                      "LMTDOrdCnt" + v-del +
                      "LMTDFillCnt"  + v-del +
                      "LMTDFilPct"    + v-del +
                      "LYTDOrdCnt" + v-del +
                      "LYTDFillCnt"  + v-del +
                      "LYTDFilPct"    + v-del +
                      (if p-exportDWl = true then 
                         "PMonth"
                        else
                          "Head1 ")  + v-del +
                      (if p-exportDWl = true then 
                         "Pyear"
                        else
                          "Head2 ").
                          
 
 
 
 
 
  
 
 
 
&endif
 
&if defined(user_exportstatheaders) = 2 &then
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  
  assign export_rec = export_rec + v-del +
                      "MTD Order Count"  + v-del +
                      "MTD Fill Count"   + v-del +
                      "MTD % Filled"     + v-del +
                      "YTD Order Count"  + v-del +
                      "YTD Fill Count"   + v-del +
                      "YTD % Filled"     + v-del +
                      "LMTD Order Count" + v-del +
                      "LMTD Fill Count"  + v-del +
                      "LMTD % Filled"    + v-del +
                      "LYTD Order Count" + v-del +
                      "LYTD Fill Count"  + v-del +
                      "LYTD % Filled"    + v-del +
                      " " + v-del +
                      " ".
 
 
 
 
 
 
 
&endif
 
&if defined(user_foreach) = 2 &then
for each xfill no-lock:
 
 
 
 
 
 
 
&endif
 
&if defined(user_drptassign) = 2 &then
 /* If you add fields to the dprt table this is where you assign them */
   drpt.xrecid = recid(xfill)
 
 
 
 
 
 
 
&endif
 
&if defined(user_viewheadframes) = 2 &then
/* Since frames are unique to each program you need to put your
   views and hides here with your names */
if not p-exportl then
  do:
  if not x-stream then
    do:
/*  hide frame f-trn1.  */
    hide frame f-trn2.
    hide frame f-trn3.
    hide frame f-trn4p.
    hide frame f-trn5.
    hide frame f-trn6.
    hide frame f-trn7.  
    
    end.
  else
   do:
/*  hide stream xpcd frame f-trn1.  */
    hide stream xpcd frame fx-trn2.
    hide stream xpcd frame f-trn3.
    hide stream xpcd frame f-trn4p.
    hide stream xpcd frame f-trn5.
    hide stream xpcd frame f-trn6.
    hide stream xpcd frame f-trn7.  
    end.
  
  end.
if not p-exportl  then
  do:
  if not x-stream then
    do:
    page.
/*  view frame f-trn1.   */
    view frame f-trn2.
    view frame f-trn3.
    view frame f-trn4p.
    view frame f-trn5.
    view frame f-trn6.
    view frame f-trn7. 
    
    end.
  else  
    do:
    page stream xpcd.
/*  view stream xpcd frame f-trn1. */
    view stream xpcd frame fx-trn2.
    view stream xpcd frame f-trn3.
    view stream xpcd frame f-trn4p.
    view stream xpcd frame f-trn5.
    view stream xpcd frame f-trn6.
    view stream xpcd frame f-trn7. 
    
    end.
   
  end.
 
 
 
 
 
 
 
&endif
 
&if defined(user_drptloop) = 2 &then
 
 
 
&endif
 
&if defined(user_detailprint) = 2 &then
   /* Comment this out if you are not using detail 'X' option  */
   if p-detail = "d"  and 
      entry(u-entitys,drpt.sortmystery,v-delim) <> 
      fill("~~",length(entry(u-entitys,drpt.sortmystery,v-delim))) then 
     do:
     /* this code skips the all other lines for printing detail */
     
     /* next line would be your detail print procedure */
     p-detail = p-detail.
     end.
 
 
 
 
 
 
 
&endif
 
&if defined(user_finalprint) = 2 &then
if not p-exportl then
 do:
 
  assign v-per1 = 
                 if t-amounts1[u-entitys + 1] = 0 then                     
                   0                                                
                 else                                               
                  (t-amounts2[u-entitys + 1] / 
                    t-amounts1[u-entitys + 1]) *       
                    100                       
         v-per2 = 
                 if t-amounts4[u-entitys + 1] = 0 then                     
                   0                                                
                 else                                               
                  (t-amounts5[u-entitys + 1] / 
                    t-amounts4[u-entitys + 1]) *       
                    100                       
         v-per3 = 
                 if t-amounts7[u-entitys + 1] = 0 then                     
                   0                                                
                 else                                               
                  (t-amounts8[u-entitys + 1] / 
                    t-amounts7[u-entitys + 1]) *       
                    100                       
         v-per4 =
                 if t-amounts10[u-entitys + 1] = 0 then                     
                   0                                                
                 else                                               
                  (t-amounts11[u-entitys + 1] /
                    t-amounts10[u-entitys + 1]) *       
                    100.                       
       
 
 
 
 if not x-stream then 
   do:
   display 
    v-final
    v-astrik
    v-summary-lit2
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts2[u-entitys + 1]   @ v-amt2
    v-per1                      @ v-amt3
    t-amounts4[u-entitys + 1]   @ v-amt4
    t-amounts5[u-entitys + 1]   @ v-amt5
    v-per2                      @ v-amt6
    t-amounts7[u-entitys + 1]   @ v-amt7
    t-amounts8[u-entitys + 1]   @ v-amt8
    v-per3                      @ v-amt9
    t-amounts10[u-entitys + 1]  @ v-amt10
    t-amounts11[u-entitys + 1]  @ v-amt11
    v-per4                      @ v-amt12
    with frame f-tot.
  
   down with frame f-tot.
   end.
 else
   do:
   display stream xpcd
    v-final
    v-astrik
    v-summary-lit2
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts2[u-entitys + 1]   @ v-amt2
    v-per1                      @ v-amt3
    t-amounts4[u-entitys + 1]   @ v-amt4
    t-amounts5[u-entitys + 1]   @ v-amt5
    v-per2                      @ v-amt6
    t-amounts7[u-entitys + 1]   @ v-amt7
    t-amounts8[u-entitys + 1]   @ v-amt8
    v-per3                      @ v-amt9
    t-amounts10[u-entitys + 1]  @ v-amt10
    t-amounts11[u-entitys + 1]  @ v-amt11
    v-per4                      @ v-amt12
    with frame f-tot.
  
   down stream xpcd with frame f-tot.
   end.
      
 end.
else
  do:
  assign export_rec =  v-astrik.
  do v-inx4 = 1 to u-entitys:
    if v-totalup[v-inx4] = "n" then
       next.
    if v-inx4 = 1 then
      v-summary-lit2 = "Final".
    else
      v-summary-lit2 = "".
    if v-cells[v-inx4]= "2" then
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec + v-summary-lit2 + v-del + " ".
     
      end.
    else
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec +  v-summary-lit2.
      end.
  end.
 
    
  assign v-per1 = 
                 if t-amounts1[u-entitys + 1] = 0 then                     
                   0                                                
                 else                                               
                  (t-amounts2[u-entitys + 1] / 
                    t-amounts1[u-entitys + 1]) *       
                    100                       
         v-per2 = 
                 if t-amounts4[u-entitys + 1] = 0 then                     
                   0                                                
                 else                                               
                  (t-amounts5[u-entitys + 1] / 
                    t-amounts4[u-entitys + 1]) *       
                    100                       
         v-per3 = 
                 if t-amounts7[u-entitys + 1] = 0 then                     
                   0                                                
                 else                                               
                  (t-amounts8[u-entitys + 1] / 
                    t-amounts7[u-entitys + 1]) *       
                    100                       
         v-per4 =
                 if t-amounts10[u-entitys + 1] = 0 then                     
                   0                                                
                 else                                               
                  (t-amounts11[u-entitys + 1] /
                    t-amounts10[u-entitys + 1]) *       
                    100.                       
  
  
  assign export_rec = export_rec + v-del +
      string(t-amounts1[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts2[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(v-per1,"->>>>9.999") + v-del +
      string(t-amounts4[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts5[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(v-per2,"->>>>9.999") + v-del +
      string(t-amounts7[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts8[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(v-per3,"->>>>9.999") + v-del +
      string(t-amounts10[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts11[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(v-per4,"->>>>9.999")  + v-del +
      (if p-exportDWl = true then 
         string(p-mth,"99")
       else
          " ") 
          + v-del +
      (if p-exportDWl = true then 
         string ((if p-year < 50 then
                    p-year + 2000
                  else
                    p-year + 1900),"9999")
       else
         "  ").
      
    
  assign export_rec = export_rec + chr(13).    
  put stream expt unformatted export_rec.
  end.
 
 
 
 
 
 
 
&endif
 
&if defined(user_summaryframeprint) = 2 &then
  
  assign v-per1 = 
                 if t-amounts1[v-inx2] = 0 then                     
                   0                                                
                 else                                               
                  (t-amounts2[v-inx2] / t-amounts1[v-inx2]) *       
                    100                       
         v-per2 = 
                 if t-amounts4[v-inx2] = 0 then                     
                   0                                                
                 else                                               
                  (t-amounts5[v-inx2] / t-amounts4[v-inx2]) *       
                    100                       
         v-per3 = 
                 if t-amounts7[v-inx2] = 0 then                     
                   0                                                
                 else                                               
                  (t-amounts8[v-inx2] / t-amounts7[v-inx2]) *       
                    100                       
         v-per4 =
                 if t-amounts10[v-inx2] = 0 then                     
                   0                                                
                 else                                               
                  (t-amounts11[v-inx2] / t-amounts10[v-inx2]) *       
                    100.                       
       
  
  
  
  
    if not x-stream then
      do:
      display 
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        v-per1               @ v-amt3
        t-amounts4[v-inx2]   @ v-amt4
        t-amounts5[v-inx2]   @ v-amt5
        v-per2               @ v-amt6
        t-amounts7[v-inx2]   @ v-amt7
        t-amounts8[v-inx2]   @ v-amt8
        v-per3               @ v-amt9
        t-amounts10[v-inx2]  @ v-amt10
        t-amounts11[v-inx2]  @ v-amt11
        v-per4               @ v-amt12
        with frame f-tot.
      down with frame f-tot.
      end.
    else
      do:
      display stream xpcd
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        v-per1               @ v-amt3
        t-amounts4[v-inx2]   @ v-amt4
        t-amounts5[v-inx2]   @ v-amt5
        v-per2               @ v-amt6
        t-amounts7[v-inx2]   @ v-amt7
        t-amounts8[v-inx2]   @ v-amt8
        v-per3               @ v-amt9
        t-amounts10[v-inx2]  @ v-amt10
        t-amounts11[v-inx2]  @ v-amt11
        v-per4               @ v-amt12
        with frame f-tot.
      down stream xpcd with frame f-tot.
      end.
 
 
 
 
 
 
 
&endif
 
&if defined(user_summaryputexport) = 2 &then
 
  
  assign v-per1 = 
                 if t-amounts1[v-inx2] = 0 then                     
                   0                                                
                 else                                               
                  (t-amounts2[v-inx2] / t-amounts1[v-inx2]) *       
                    100                       
         v-per2 = 
                 if t-amounts4[v-inx2] = 0 then                     
                   0                                                
                 else                                               
                  (t-amounts5[v-inx2] / t-amounts4[v-inx2]) *       
                    100                       
         v-per3 = 
                 if t-amounts7[v-inx2] = 0 then                     
                   0                                                
                 else                                               
                  (t-amounts8[v-inx2] / t-amounts7[v-inx2]) *       
                    100                       
         v-per4 =
                 if t-amounts10[v-inx2] = 0 then                     
                   0                                                
                 else                                               
                  (t-amounts11[v-inx2] / t-amounts10[v-inx2]) *       
                    100.                       
       
 
    assign export_rec = v-astrik + v-del + export_rec.
    assign export_rec = export_rec + v-del +
      string(t-amounts1[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts2[v-inx2],"->>>>>>>>9") + v-del +
      string(v-per1,"->>>>9.999") + v-del +
      string(t-amounts4[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts5[v-inx2],"->>>>>>>>9") + v-del +
      string(v-per2,"->>>>9.999") + v-del +
      string(t-amounts7[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts8[v-inx2],"->>>>>>>>9") + v-del +
      string(v-per3,"->>>>9.999") + v-del +
      string(t-amounts10[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts11[v-inx2],"->>>>>>>>9") + v-del +
      string(v-per4,"->>>>9.999") + v-del +
      (if p-exportDWl = true then 
         string(p-mth,"99")
       else
          " ") 
          + v-del +
      (if p-exportDWl = true then 
         string ((if p-year < 50 then
                    p-year + 2000
                  else
                    p-year + 1900),"9999")
       else
         "  ").
       
      
 
 
 
 
 
 
 
&endif
 
&if defined(user_procedures) = 2 &then
procedure sapb_vars:
  assign
    b-buygrp     = sapb.rangebeg[1]
    e-buygrp     = sapb.rangeend[1]
    b-buyer      = sapb.rangebeg[2]
    e-buyer      = sapb.rangeend[2]
    b-whse       = sapb.rangebeg[3]
    e-whse       = sapb.rangeend[3]
    b-prodline   = sapb.rangebeg[4]
    e-prodline   = sapb.rangeend[4]
    b-vendno     = dec(sapb.rangebeg[5])
    e-vendno     = dec(sapb.rangeend[5])
    b-prodcat    = sapb.rangebeg[6]
    e-prodcat    = sapb.rangeend[6]
    b-vid        = sapb.rangebeg[7]
    e-vid        = sapb.rangeend[7]
    b-vp         = sapb.rangebeg[8]
    e-vp         = sapb.rangeend[8]
    b-tech       = sapb.rangebeg[9]
    e-tech       = sapb.rangeend[9]
    b-buydept    = sapb.rangebeg[10]
    e-buydept    = sapb.rangeend[10]
    b-product    = sapb.rangebeg[11]
    e-product    = sapb.rangeend[11]
     
    
    v-perin      = sapb.optvalue[1]
    
    
    
    p-detail  = "s" /* if sapb.optvalue[3] = "d" then "d" else "s" */
    p-list    = if sapb.optvalue[3] = "yes" then true else false
    p-export  = sapb.optvalue[4]
    p-minmax  = if sapb.optvalue[5] = "yes" then true else false.
 {p-rptper.i}
 
 assign p-year = int(substring(string(v-perout,"9999"),3,2))   
        p-mth  = int(substring(string(v-perout,"9999"),1,2)).   
  run formatoptions.
assign zzl_begcat =  b-prodcat
       zzl_endcat =  e-prodcat
       zzl_begvname  = b-vid
       zzl_endvname  = e-vid
       zzl_Prodbeg   = b-product
       zzl_Prodend   = e-product
       zzl_begvpid  = b-vp
       zzl_endvpid  = e-vp
       zzl_begbuy   = b-buyer
       zzl_endbuy   = e-buyer
       zzl_begvend  = b-vendno
       zzl_endvend  = e-vendno
       zzl_begwhse  = b-whse
       zzl_endwhse  = e-whse.
assign zel_begcat =  b-prodcat
       zel_endcat =  e-prodcat
       zel_Prodbeg   = b-product
       zel_Prodend   = e-product
       zel_begvname  = b-vid
       zel_endvname  = e-vid
       zel_begvpid  = b-vp
       zel_endvpid  = e-vp
       zel_begbuy   = b-buyer
       zel_endbuy   = e-buyer
       zel_begvend  = b-vendno
       zel_endvend  = e-vendno
       zel_begwhse  = b-whse
       zel_endwhse  = e-whse.
if p-list then
  do:
  assign zel_begcat =  b-prodcat
       zel_endcat =  e-prodcat
       zel_begvname  = b-vid
       zel_endvname  = e-vid
       zel_Prodbeg   = b-product
       zel_Prodend   = e-product
       zel_begvpid  = b-vp
       zel_endvpid  = e-vp
       zel_begbuy   = b-buyer
       zel_endbuy   = e-buyer
       zel_begvend  = b-vendno
       zel_endvend  = e-vendno
       zel_begwhse  = b-whse
       zel_endwhse  = e-whse.
  {zsapboyload.i}
/*
  assign zzl_begcat   = zel_begcat
       zzl_endcat   = zel_endcat
       zzl_begvname = zel_begvname
       zzl_endvname = zel_endvname
       zzl_begvpid  = zel_begvpid
       zzl_endvpid  = zel_endvpid
       zzl_begbuy   = zel_begbuy
       zzl_endbuy   = zel_endbuy
       zzl_begvend  = zel_begvend
       zzl_endvend  = zel_endvend
       zzl_begwhse  = zel_begwhse
       zzl_endwhse  = zel_endwhse.
 */
  end.
  
  assign zelection_matrix [4] = (if zelection_matrix[4] > 1 then /* Buyer */
                                  zelection_matrix[4]
                                 else   
                                   1)
         zelection_matrix [7] = (if zelection_matrix[7] > 1 then  /* Vend */
                                  zelection_matrix[7]
                                 else   
                                  1)
         zelection_matrix [6] = (if zelection_matrix[6] > 1 then  /* Whse */
                                  zelection_matrix[6]
                                 else   
                                  1)
         zelection_matrix [3] = (if zelection_matrix[3] > 1 then  /* Cat  */
                                 zelection_matrix[3]
                              else   
                                 1)
         zelection_matrix [8] = (if zelection_matrix[8] > 1 then  /* VName */
                                 zelection_matrix[8]
                              else   
                                 1)
         zelection_matrix [9] = (if zelection_matrix[9] > 1 then  /* VPArent */
                                 zelection_matrix[9]
                              else   
                                 1)
         zelection_matrix [11] = (if zelection_matrix[11] > 1 then /* Product */
                                 zelection_matrix[11]
                              else   
                                 1).
                                 
                                 
 
end.
/* ------------------------------------------------------------------------- */
procedure formatoptions:
/* ------------------------------------------------------------------------- */
   
   
  assign p-portrait = false.
  /* if false then the report will print in LANDSCAPE 178 character */
  
  if sapb.optvalue[2] > "0" and sapb.optvalue[2] ne "99" then 
    do:
    p-optiontype = " ".
    p-format = string(int(sapb.optvalue[2])).
    find notes where notes.cono = g-cono and
                     notes.notestype = "zz" and
                     notes.primarykey = "icxf" and  /* tbxr program */
                     notes.secondarykey = string(p-format) no-lock no-error.
    if not avail notes then
      do:
      display "Format is not valid cannot process request".
      assign p-optiontype = "r"
             p-sorttype   = ">,"
             p-totaltype  = "l"
             p-summcounts = "a".
      return.
      end.               
   
    assign p-optiontype = notes.noteln[1]
           p-sorttype   = notes.noteln[2]
           p-totaltype  = notes.noteln[3]
           p-summcounts = notes.noteln[4]
           p-register   = notes.noteln[5]
           p-registerex = notes.noteln[6].
    
    find x-sapb where recid(x-sapb) = recid(sapb) exclusive-lock.
    
    assign x-sapb.user5 = 
             p-optiontype + "~011" +
             p-sorttype   + "~011" +
             p-totaltype  + "~011" +
             p-summcounts + "~011" +
             p-register   + "~011" +
             p-registerex.     
    
    
    end.     
  else
  if sapb.optvalue[2] = "99" then 
    do:
    assign p-register   = ""
           p-registerex = "".
    run reportopts(input sapb.user5, 
                   input-output p-optiontype,
                   input-output p-sorttype,  
                   input-output p-totaltype,
                   input-output p-summcounts,
                   input-output p-register,
                   input-output p-registerex).
    end.
 run Print_reportopts (input recid (sapb),
                       input g-cono).
 end.
   
 
procedure extract_data:
  do y-yr = p-year to (p-year - 1) by -1:
  
  
  for each zicfill where zicfill.cono = g-cono  and
                       zicfill.yr   = y-yr    and
                       zicfill.whse ge zel_begwhse and
                       zicfill.whse le zel_endwhse and
                       zicfill.prod ge zel_Prodbeg and
                       zicfill.prod le zel_Prodend 
                       
                        no-lock:
    find icsd where icsd.cono = g-cono and
                    icsd.whse = zicfill.whse no-lock no-error.
    if not avail icsd or
       icsd.buygroup < b-buygrp or
       icsd.buygroup > e-buygrp then
      next.                  
        
    assign zelection_type = "u"
           zelection_char4 = zicfill.prod
           zelection_cust = 0.
    run zelectioncheck.
    if zelection_good = false then
      next.
    
    
    do y-inx = 1 to 4:
      assign x-qtyord [y-inx] = 0
             x-qtybo  [y-inx] = 0
             x-ordcnt [y-inx] = 0
             x-fillcnt[y-inx] = 0.
    end.
    
    if y-yr = p-year then
    do:
    assign x-qtyord[1]  = x-qtyord[1]   + zicfill.qtyord[p-mth]
           x-qtybo [1]  = x-qtybo [1]   + zicfill.qtybo [p-mth]
           x-ordcnt[1]  = x-ordcnt[1]   + zicfill.ordcnt[p-mth]
           x-fillcnt[1] = x-fillcnt[1]  + zicfill.fillcnt[p-mth].
    do y-inx = 1 to p-mth:
      assign x-qtyord[2]  = x-qtyord[2]   + zicfill.qtyord[y-inx]
             x-qtybo [2]  = x-qtybo [2]   + zicfill.qtybo [y-inx]
             x-ordcnt[2]  = x-ordcnt[2]   + zicfill.ordcnt[y-inx]
             x-fillcnt[2] = x-fillcnt[2]  + zicfill.fillcnt[y-inx].
    end.             
    end.                      
  if y-yr = (p-year - 1) then
    do:
    assign x-qtyord[3]  = x-qtyord[3]   + zicfill.qtyord[p-mth]
           x-qtybo [3]  = x-qtybo [3]   + zicfill.qtybo [p-mth]
           x-ordcnt[3]  = x-ordcnt[3]   + zicfill.ordcnt[p-mth]
           x-fillcnt[3] = x-fillcnt[3]  + zicfill.fillcnt[p-mth].
    do y-inx = 1 to p-mth:
      assign x-qtyord[4]  = x-qtyord[4]   + zicfill.qtyord[y-inx]
             x-qtybo [4]  = x-qtybo [4]   + zicfill.qtybo [y-inx]
             x-ordcnt[4]  = x-ordcnt[4]   + zicfill.ordcnt[y-inx]
             x-fillcnt[4] = x-fillcnt[4]  + zicfill.fillcnt[y-inx].
    end.             
    end.
 
        
    
    if x-qtyord[1] = 0  and  x-qtyord[2] = 0  and   
       x-qtybo [1] = 0  and  x-qtybo [2] = 0  and  
       x-ordcnt[1] = 0  and  x-ordcnt[2] = 0  and
       x-fillcnt[1] = 0 and x-fillcnt[2] = 0  and
       x-qtyord[3] = 0  and  x-qtyord[4] = 0  and   
       x-qtybo [3] = 0  and  x-qtybo [4] = 0  and  
       x-ordcnt[3] = 0  and  x-ordcnt[4] = 0  and
       x-fillcnt[3] = 0 and x-fillcnt[4] = 0  then
      next.  
  
  
  
  
  
  
  
  
  
  
  
  find xfill where xfill.module = zicfill.module and
                   xfill.whse   = zicfill.whse and
                   xfill.prod   = zicfill.prod no-lock no-error.
  if not avail xfill then
    do:
    find icsp where icsp.cono = g-cono and 
                    icsp.prod = zicfill.prod no-lock no-error.
    find icsw where icsw.cono = g-cono and 
                    icsw.whse = zicfill.whse and
                    icsw.prod = zicfill.prod no-lock no-error.
    if avail icsw then
      do:
      find icsl where icsl.cono = g-cono and
                      icsl.whse = icsw.whse and
                      icsl.vendno = icsw.arpvendno and
                      icsl.prodline = icsw.prodline no-lock no-error.
      if not avail icsl then
        find first icsl where icsl.cono = g-cono and
                              icsl.whse = icsw.whse and
                              icsl.vendno = icsw.arpvendno 
                              no-lock no-error.
      if not avail icsl then
        find first icsl where icsl.cono = g-cono and
                              icsl.whse = icsw.whse and
                              icsl.prodline = icsw.prodline no-lock no-error.
                      
      if not avail icsl then
        find first icsl where icsl.cono = g-cono and
                              icsl.vendno = icsw.arpvendno and
                              icsl.prodline = icsw.prodline no-lock no-error.
      
      if not avail icsl then                
       find first icsl where icsl.cono = g-cono and
                             icsl.vendno = icsw.arpvendno no-lock no-error.
   
      end.
    assign
       w-buyer      = (if avail icsw and avail icsl then
                         icsl.buyer
                       else
                         " ")
       w-vendno     = (if avail icsw then
                         icsw.arpvendno
                       else
                         0)
       w-prodcat    = (if avail icsp then 
                         icsp.prodcat 
                       else
                         "")
       w-prodline   = (if avail icsw then 
                         icsw.prodline
                       else
                         "").
    
     find oimsp where oimsp.person = w-buyer no-lock no-error.
     if avail oimsp then
       h-person = oimsp.responsible.
     else   
       h-person = "NTFND".
     if not p-minmax then
       if avail icsw then
         do:
         if icsw.statustype = "s" and
            icsw.orderpt = 0 and
            icsw.linept = 0 then
           next. 
         end.
    
     if h-person < b-buydept or
        h-person > e-buydept then
       next.                   
    
    
    
    if w-prodline < b-prodline or
       w-prodline > e-prodline then
       next.
    
    
    assign zelection_type = "p"
           zelection_char4 = w-prodcat
           zelection_cust = 0.
    run zelectioncheck.
    if zelection_good = false then
      next.
    assign zelection_type = "b"
           zelection_char4 = w-buyer
           zelection_cust = 0.
    run zelectioncheck.
    if zelection_good = false then
      next.
    assign zelection_type = "w"
           zelection_char4 =  zicfill.whse
           zelection_cust = 0.
    run zelectioncheck.
    if zelection_good = false then
      next.
    
    assign
      v-vendorid   =  " "
      v-technology =  " "
      v-parentcode =  " ".
     
    
    if avail icsp then
      do:
      find catmaster where catmaster.cono = 1 and 
                         catmaster.notestype = "zz" and
                         catmaster.primarykey  = "apsva" and
                         catmaster.secondarykey = 
                         icsp.prodcat no-lock
                         no-error. 
      if not avail catmaster then
        do:
        v-parentcode = "".
        v-vendorid = substring(w-prodcat,1,3).
        if substring(icsp.prodcat,1,1) = "a" then 
          v-technology = "Automation".
        else if substring(w-prodcat,1,1) = "f" then
          v-technology = "Fabrication". 
        else if substring(w-prodcat,1,1) = "h" then 
          v-technology = "Hydraulic".   
        else if substring(w-prodcat,1,1) = "p" then 
          v-technology =  "Pneumatic".   
        else if substring(w-prodcat,1,1) = "s" then 
          v-technology =  "Service".     
        else if substring(w-prodcat,1,1) = "f" then 
          v-technology =  "Filtration".  
        else if substring(w-prodcat,1,1) = "c" then 
          v-technology =  "Connectors".  
        else if substring(w-prodcat,1,1) = "m" then 
          v-technology =  "Mobile".      
        else if substring(w-prodcat,1,1) = "n" then 
          v-technology =  "None".        
        end.
  
      else
        assign
          v-vendorid   = catmaster.noteln[1]
          v-technology = catmaster.noteln[2]
          v-parentcode = catmaster.noteln[3].
      
      end.
    if p-list = true then
      do:
      zelection_type = "n".
      zelection_char4 = v-vendorid.
      zelection_cust = 0.
      run zelectioncheck.
      if zelection_good = false then
        next.
      end.
    if p-list = true then
      do:
      zelection_type = "e".
      zelection_char4 = v-parentcode.
      zelection_cust = 0.
      run zelectioncheck.
      if zelection_good = false then
        next.
      end.
    if (substring(v-technology,1,4) < b-tech or
        substring(v-technology,1,4) > e-tech) or
       (v-parentcode < zel_begvpid or
        v-parentcode > zel_endvpid) or
       (v-vendorid < zel_begvname or
        v-vendorid > zel_endvname)  then
      next. 
   
    assign zelection_type = "v"
           zelection_vend = w-vendno
           zelection_char4 = " ".
    run zelectioncheck.
    if zelection_good = false then
      next.
  
    
    create xfill.
    assign
      xfill.tech       = caps(v-technology)
      xfill.vendid     = caps(v-vendorid )
      xfill.vendpnt    = caps(v-parentcode)
      xfill.module     = caps(zicfill.module)
      xfill.buygrp     = caps(icsd.buygroup)
      xfill.buydept    = caps(h-person)
      xfill.buyer      = caps(w-buyer)
      xfill.vendno     = w-vendno
      xfill.prodcat    = caps(w-prodcat)
      xfill.prodline   = caps(w-prodline)
      xfill.whse       = caps(zicfill.whse)
      xfill.prod       = caps(zicfill.prod)
      xfill.descr      = caps(if avail icsp then
                                icsp.descrip[1]
                              else
                                "")
      xfill.qtyord [1]  = 0
      xfill.qtybo  [1]  = 0
      xfill.ordcnt [1]  = 0
      xfill.fillcnt[1]  = 0
      xfill.qtyord [2]  = 0
      xfill.qtybo  [2]  = 0
      xfill.ordcnt [2]  = 0
      xfill.fillcnt[2]  = 0
      xfill.qtyord [3]  = 0
      xfill.qtybo  [3]  = 0
      xfill.ordcnt [3]  = 0
      xfill.fillcnt[3]  = 0
      xfill.qtyord [4]  = 0
      xfill.qtybo  [4]  = 0
      xfill.ordcnt [4]  = 0
      xfill.fillcnt[4]  = 0.
      end.
  if y-yr = p-year then
    do:
    assign xfill.qtyord[1]  = xfill.qtyord[1]   + zicfill.qtyord[p-mth]
           xfill.qtybo [1]  = xfill.qtybo [1]   + zicfill.qtybo [p-mth]
           xfill.ordcnt[1]  = xfill.ordcnt[1]   + zicfill.ordcnt[p-mth]
           xfill.fillcnt[1] = xfill.fillcnt[1]  + zicfill.fillcnt[p-mth].
    do y-inx = 1 to p-mth:
      assign xfill.qtyord[2]  = xfill.qtyord[2]   + zicfill.qtyord[y-inx]
             xfill.qtybo [2]  = xfill.qtybo [2]   + zicfill.qtybo [y-inx]
             xfill.ordcnt[2]  = xfill.ordcnt[2]   + zicfill.ordcnt[y-inx]
             xfill.fillcnt[2] = xfill.fillcnt[2]  + zicfill.fillcnt[y-inx].
    end.             
    end.                      
  if y-yr = (p-year - 1) then
    do:
    assign xfill.qtyord[3]  = xfill.qtyord[3]   + zicfill.qtyord[p-mth]
           xfill.qtybo [3]  = xfill.qtybo [3]   + zicfill.qtybo [p-mth]
           xfill.ordcnt[3]  = xfill.ordcnt[3]   + zicfill.ordcnt[p-mth]
           xfill.fillcnt[3] = xfill.fillcnt[3]  + zicfill.fillcnt[p-mth].
    do y-inx = 1 to p-mth:
      assign xfill.qtyord[4]  = xfill.qtyord[4]   + zicfill.qtyord[y-inx]
             xfill.qtybo [4]  = xfill.qtybo [4]   + zicfill.qtybo [y-inx]
             xfill.ordcnt[4]  = xfill.ordcnt[4]   + zicfill.ordcnt[y-inx]
             xfill.fillcnt[4] = xfill.fillcnt[4]  + zicfill.fillcnt[y-inx].
    end.             
    
    
    
    end.                      
                           
                          
  end.
  end. /* end Do for p-year */ 
end.
  
{zsapboycheck.i}
 
 
 
 
 
 
 
&endif
 
/* TBXR F6 CodeGen Includes from here down are CodeGen Generated  */ 
&if defined(user_optiontype) = 2 &then
if not
can-do("B,C,D,G,I,L,N,P,T,U,V,W",substring(p-optiontype,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries B,C,D,G,I,L,N,P,T,U,V,W "
     substring(p-optiontype,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries B,C,D,G,I,L,N,P,T,U,V,W "
     substring(p-optiontype,v-inx,1).
&endif
 
&if defined(user_registertype) = 2 &then
if not
can-do("B,C,D,G,I,L,N,P,T,U,V,W",substring(p-register,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries B,C,D,G,I,L,N,P,T,U,V,W "
     substring(p-register,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries B,C,D,G,I,L,N,P,T,U,V,W "
     substring(p-register,v-inx,1).
&endif
 
&if defined(user_DWvarheaders1) = 2 &then
if v-lookup[v-inx4] = "B" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Buyer".
  assign export_rec = export_rec + v-del + "BuyerName".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Pcat".
  assign export_rec = export_rec + v-del + "PcatDescrip".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Department".
  end. 
else  
if v-lookup[v-inx4] = "G" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "BuyerGroup".
  end. 
else  
if v-lookup[v-inx4] = "I" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "VendId".
  end. 
else  
if v-lookup[v-inx4] = "L" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "ProdLine".
  end. 
else  
if v-lookup[v-inx4] = "N" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "VendParentNm".
  end. 
else  
if v-lookup[v-inx4] = "P" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Prod".
  assign export_rec = export_rec + v-del + "ProdDescrip".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Technology".
  end. 
else  
if v-lookup[v-inx4] = "U" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Module".
  end. 
else  
if v-lookup[v-inx4] = "V" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "VendNbr".
  assign export_rec = export_rec + v-del + "VendName".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Warehouse".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_exportvarheaders1) = 2 &then
if v-lookup[v-inx4] = "B" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Buyer".
  assign export_rec = export_rec + v-del + "Buyer".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Product".
  assign export_rec = export_rec + v-del + "Product Category".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "BuyerDept".
  end. 
else  
if v-lookup[v-inx4] = "G" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Buyer Group".
  end. 
else  
if v-lookup[v-inx4] = "I" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Vendor ID".
  end. 
else  
if v-lookup[v-inx4] = "L" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Product".
  end. 
else  
if v-lookup[v-inx4] = "N" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Vendor".
  end. 
else  
if v-lookup[v-inx4] = "P" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Product".
  assign export_rec = export_rec + v-del + "Product".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Technology".
  end. 
else  
if v-lookup[v-inx4] = "U" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Trend".
  end. 
else  
if v-lookup[v-inx4] = "V" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Vendor".
  assign export_rec = export_rec + v-del + "Vendor".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Warehouse".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_exportvarheaders2) = 2 &then
if v-lookup[v-inx4] = "B" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  assign export_rec = export_rec + v-del + "Name".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Category".
  assign export_rec = export_rec + v-del + "Name".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "G" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "I" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "L" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Line".
  end. 
else  
if v-lookup[v-inx4] = "N" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Parent".
  end. 
else  
if v-lookup[v-inx4] = "P" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  assign export_rec = export_rec + v-del + "Description".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "U" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Module".
  end. 
else  
if v-lookup[v-inx4] = "V" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Number".
  assign export_rec = export_rec + v-del + "Number".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_loadtokens) = 2 &then
if v-lookup[v-inx] = "B" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(xfill.buyer,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "C" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(xfill.prodcat,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "D" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(xfill.buydept,"x(6)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(6)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "G" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(xfill.buygrp,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "I" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(xfill.vendid,"x(15)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(15)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "L" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(xfill.prodline,"x(6)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(6)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "N" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(xfill.vendpnt,"x(15)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(15)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "P" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(xfill.prod,"x(24)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(24)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "T" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(xfill.tech,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "U" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(xfill.module,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "V" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(xfill.vendno,"999999999999")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"999999999999").   
  end.                                              
else                                              
if v-lookup[v-inx] = "W" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(xfill.whse,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "X" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(recid(xfill),"999999999999")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"999999999999").   
  end.                                              
else                                              
  assign v-token = " ".
&endif
 
&if defined(user_totaladd) = 2 &then
  assign t-amounts[1] = xfill.ordcnt[1].
  assign t-amounts[2] = xfill.fillcnt[1].
  assign t-amounts[3] = 0 /* This is a percent needs formula */.
  assign t-amounts[4] = xfill.ordcnt[2].
  assign t-amounts[5] = xfill.fillcnt[2].
  assign t-amounts[6] = 0 /* This is a percent needs formula */.
  assign t-amounts[7] = xfill.ordcnt[3].
  assign t-amounts[8] = xfill.fillcnt[3].
  assign t-amounts[9] = 0 /* This is a percent needs formula */.
  assign t-amounts[10] = xfill.ordcnt[4].
  assign t-amounts[11] = xfill.fillcnt[4].
  assign t-amounts[12] = 0 /* This is a percent needs formula */.
&endif
 
&if defined(user_numbertotals) = 2 &then
  if v-subtotalup[v-inx4] = 1 then
    v-val = t-amounts1[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 2 then
    v-val = t-amounts2[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 3 then
    v-val = 
      if t-amounts1[v-inx4] = 0 then
        0
      else
      (t-amounts2[v-inx4] / t-amounts1[v-inx4]) *
        100.
  else 
  if v-subtotalup[v-inx4] = 4 then
    v-val = t-amounts4[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 5 then
    v-val = t-amounts5[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 6 then
    v-val = 
      if t-amounts4[v-inx4] = 0 then
        0
      else
      (t-amounts5[v-inx4] / t-amounts4[v-inx4]) *
       100.
  else 
  if v-subtotalup[v-inx4] = 7 then
    v-val = t-amounts7[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 8 then
    v-val = t-amounts8[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 9 then
    v-val = 
      if t-amounts7[v-inx4] = 0 then
        0
      else
      (t-amounts8[v-inx4] - t-amounts7[v-inx4]) *
       100.
  else 
  if v-subtotalup[v-inx4] = 10 then
    v-val = t-amounts10[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 11 then
    v-val = t-amounts11[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 12 then
    v-val = 
      if t-amounts10[v-inx4] = 0 then
        0
      else
      (t-amounts10[v-inx4] / t-amounts11[v-inx4]) *
       100.
  else 
    v-val = t-amounts1[v-inx4].
  do inz = 1 to 12:
  if inz = 1 then
    v-regval[inz] = t-amounts1[v-inx4].
  else 
  if inz = 2 then
    v-regval[inz] = t-amounts2[v-inx4].
  else 
  if inz = 3 then
    v-regval[inz] = 
      if t-amounts1[v-inx4] = 0 then
        0
      else
      (t-amounts2[v-inx4] / t-amounts1[v-inx4]) *
        100.
  else 
  if inz = 4 then
    v-regval[inz] = t-amounts4[v-inx4].
  else 
  if inz = 5 then
    v-regval[inz] = t-amounts5[v-inx4].
  else 
  if inz = 6 then
    v-regval[inz] = 
      if t-amounts4[v-inx4] = 0 then
        0
      else
      (t-amounts5[v-inx4] / t-amounts4[v-inx4]) *
       100.
  else 
  if inz = 7 then
    v-regval[inz] = t-amounts7[v-inx4].
  else 
  if inz = 8 then
    v-regval[inz] = t-amounts8[v-inx4].
  else 
  if inz = 9 then
    v-regval[inz] = 
      if t-amounts7[v-inx4] = 0 then
        0
      else
      (t-amounts8[v-inx4] - t-amounts7[v-inx4]) *
       100.
  else 
  if inz = 10 then
    v-regval[inz] = t-amounts10[v-inx4].
  else 
  if inz = 11 then
    v-regval[inz] = t-amounts11[v-inx4].
  else 
  if inz = 12 then
    v-regval[inz] = 
      if t-amounts10[v-inx4] = 0 then
        0
      else
      (t-amounts10[v-inx4] / t-amounts11[v-inx4]) *
       100.
  else 
    assign inz = inz.
  end.
&endif
 
&if defined(user_summaryloadprint) = 2 &then
  if v-lookup[v-inx2] = "B" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-buyer.i}
      end. 
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Buyer - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "C" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-prodcat.i}
      end. 
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Product Category - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "D" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Department - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "G" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Buyer Group - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "I" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Vendor Name (ID) - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "L" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Product Line - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "N" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Vendor Parent Name - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "P" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-product.i}
      end. 
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Product - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "T" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Technology - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "U" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Module - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "V" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-vendor.i}
      end. 
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Vendor - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "W" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Warehouse - " + h-lookup + " " + h-genlit.
    end.
  else
      v-summary-lit = v-lookup[v-inx2].
&endif
 
&if defined(user_summaryloadexport) = 2 &then
  if v-inx4 > v-inx2  then 
    do:
    if v-inx4 > 1 then          
      export_rec = export_rec + v-del. 
    assign export_rec = export_rec +  " ".
    if v-cells[v-inx4] = "2" then
      assign export_rec = export_rec +  v-del + " ".
    end. 
  else 
  if v-lookup[v-inx4] = "B" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-buyer.i}
      end. 
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "C" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-prodcat.i}
      end. 
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "D" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(6)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(6)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "G" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "I" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(15)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(15)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "L" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(6)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(6)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "N" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(15)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(15)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "P" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-product.i}
      end. 
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(24)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(24)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "T" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "U" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "V" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-vendor.i}
      end. 
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "999999999999" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "999999999999" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "W" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  "~~~~~~~~~~~~" then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
    do:
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    assign export_rec = export_rec + v-lookup[v-inx4].
    end.
&endif
 
/* TBXR F6 CodeGen End of Includes that are CodeGen Generated  */ 
