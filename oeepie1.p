/*se5.5*/
/* oeepie1.p 1.1 01/02/98 */
/* oeepie1.p 1.1 01/02/98 */
/* oeepie1.p 1.17 9/28/93 */
/*h*****************************************************************************
  PROCEDURE    : oeepie1.p
  DESCRIPTION  : EDI write outgoing Invoice to flat file
  AUTHOR       : mjm
  DATE WRITTEN : 03-06-92
  CHANGES MADE :
    06-06-92 mwb; TB# 6889; removed oeeh.payamt and
                               replaced with oeeh.tendamt.
    06/23/92 dkk tb 7123;Renamed g-oeepie1.i to g-oeepie.i
    07/20/92 mkb; TB# 7191 Alphanumeric changes for custno
    12/10/92 ntl; tb# 9041 Canadian Taxtation Changes
    01/27/93 jlc; tb# 9813 Rewrite for EDI flat file interface.
    04/20/93 jlc; tb# 10998 Adjust location of header user call.
    04/22/93 jlc; tb# 11037 Orders paid in full with a bo are
                                         causing rounding issues.
    04/24/93 jlc; tb# 11064 Added the 843 document.  The 843 and
                                         810 are both generated from a common
                                         include.
    09/07/93 rs; TB# 12932 Trend Doc Mgr for Rel 5.2
    10/21/94 jlc; tb# 15205 Define new shared stream for 855.
    10/21/94 jlc; tb# 15187 EDI Data Collision
    09/22/96 jlc;TB# 20139 Allow for soft edi document directories.
*******************************************************************************/
{p-rptbeg.i}
{g-oeepie.i new}  /*tb 7123 06/23/92 dkk; g-oeepie1.i was renamed to g-oeepie.i
                     for length purposes.*/

def new shared var p-minorder   as de  initial 0     no-undo.
def new shared stream    edi810.
def new shared stream    edi843.
def new shared stream    edi855.
/*tb 15187 10/21/94 jlc; EDI data collision. */
def shared var p-edidir   as c format "x(24)"        no-undo.
def var v-taxes          as de                       no-undo.
def var v-downpmtamt     as de                       no-undo.
def var v-minordamt      as de                       no-undo.
/*tb 15187 10/21/94 jlc; EDI data collision. */
/*d Paramaters passed to p-edidir. Only store values here temporarily */
def var v-docnm           as c format "x(15)"        no-undo.
def var v-docdir          as c format "x(70)"        no-undo.
def var v-randomnm        as c format "x(15)"        no-undo.
def var v-rdmdir          as c format "x(70)"        no-undo.
def var v-fulldir         as c format "x(70)"        no-undo.

/*tb 20139 09/22/96 jlc; Allow for soft edi document directories */
def var v-edioutdir       like sasc.edioutdir        no-undo.

/*d Used by s-edicat.i to cat the temporary random edi files that are
     created to the end of the permanent flat files */
def var v-from            as c format "x(70)"        no-undo.
def var v-to              as c format "x(70)"        no-undo.
/*d Used by s-edidir.i to indicate a valid edi directory */
def var v-dirok           as c format "x(8)"         no-undo.


/*tb 12932 09/07/93 rs; Trend Doc Mgr for Rel 5.2 */
def var v-coldfl  like sassr.coldfl           no-undo.

def buffer b-arsc  for arsc.
def buffer b-oeeh  for oeeh.
def buffer b-sapbo for sapbo.


/* tb 15187 10/21/94 jlc; EDI Data Collision *****************************
output stream edi810 to /usr/sps/dat2_6/mac/edi810_o.dat append.         *
*************************************************************************/

/*o create random name for the temporary edi file for the 810 */

/*tb 20139 09/22/96 jlc; Allow for soft edi document directories */
assign
    v-randomnm  = string(time) + string(random(1,9999)) + "810"
    v-docnm     = "edi810_o.dat"
    v-edioutdir = if avail sasc then
                     sasc.edioutdir
                  else "".

/*d Build the random file name with the directory and the 810 file name
    with the directory */
/*tb 20139 09/22/96 jlc; Allow for soft edi document directories */
{p-edidir.i
    &docnm      = "v-docnm"
    &edidir     = "p-edidir"
    &docdir     = "v-docdir"
    &rdmdir     = "v-rdmdir"
    &rdmnm      = "v-randomnm"
    &fulldir    = "v-fulldir"
    &edidefault = "v-edioutdir"}.


/*d Before opening the random files for output, ensure a valid directory has
    been specified as the location of the edi files.  */
{s-edidir.i}
if v-dirok <> "yes" then
    return.

output stream edi810 to value(v-rdmdir).

v-oeepie99fl = if search("oeepie99.r") ne ? then yes else no.

/*tb 11064 04/24/93 jlc; Replaced all code from here down with the common
                         include p-oeepia.i.  */
{p-oeepai.i}.

output stream edi810 close.

/*d cat the temporary 810 edi file to the end of the permanent 810 file */
assign
    v-from  = v-rdmdir
    v-to    = v-docdir.
{s-edicat.i}