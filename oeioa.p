/* SX 55 */
/*h*****************************************************************************
  PROCEDURE     : oeioa
  DESCRIPTION   : Additional Information
  AUTHOR        : dkk
  DATE WRITTEN  : 05/27/93
  CHANGES MADE  :
    05/27/93 dkk; TB# 11089,4953,6738,7218,9721,9752,10394, 11181.
    05/29/93 dkk; TB# 11089 Added different display for Canadian countries.
    10/18/93 mwb; TB# 11491 Added oeeh.nontaxtype display, was using arss, arsc
        nontaxtype.
    04/20/94 mwb; TB# 15410 Don't display arsc/arss non tax reason if the order
        is taxable.
    01/10/95 dww; TB# 17474 Forms misalignment in Progress V7.
    02/20/95 dww; TB# 17779 Canadian Tax changes per Groupe Laurier CIM and
        Systemetrix.
    08/13/95  bj; TB# 19071 New Sales Tax Inquiry
    09/22/95 gdf; TB# 18812 7.0 Rebates Enhancement (C1a). Added v-oerebty
        to variable definitions.
    02/02/96 gdf; TB# 20100 The 'r' is not being displayed on rebate.
        Removed v-oerebty as assign of this variable should be in oeiol.p
    07/23/97 mtt; TB# 23133 Develop Credit Control Center Feature
    09/30/97 tdd; TB# 23837 Can view costs when SASO view cost is no
    09/03/98 jgc; TB# 24567 UPS OnLine Compatible certification (Clipper Ship)
    12/31/98 lbr; TB# 24473 Displaying wrong addional cod ammount, needed
        to check if codcollamt was <> 0 also.
    05/11/99 cm;  TB# 5366 Added shipto notes
    01/11/01 gwk; TB# e6494 Show projected cost when items have not been filled
    04/03/01 bpa; TB# e7312 Show Rebated Cost
    11/29/01 kjb; TB# e5979 Need to display both Other tax amounts as unique
        entries in the browser - added s-other1amt and s-other2amt
    01/08/02 mtt; TB# e10559 Package tab
    05/27/02 jsb; TB# e12432 Direct Rotue Interface
    07/10/02 gwk; TB# e12723 Require tax override reason code for changes to
        detail (add params for run of oeetju.p)
    11/25/02 dls; TB# e13523 Additonal Addons in OE
    03/31/03 mms; TB# e14913 Tax amount #4 is not displaying correct when
        running TaxWare.
    05/14/03 ds;  TB# e16559 Fixed Spelling error.
    06/06/03 kjb; TB# e16616 Increase the format of the Package ID field from
        15 to 18 to accommodate a UPS tracking number.  Adjusted other fields
        to provide the necessary room on the screen.
    01/12/04 bm;  TB# e17572 Canadian Tax Authority description blank
    02/18/04 sbr; TB# e17571 Tax Override Reason not displayed
    11/04/05 bp ; TB# e23505 Add user hooks
*******************************************************************************/
{g-all.i}
{g-inq.i}
{idtaxirn.lva}

def    shared  var g-orderno     like oeeh.orderno       no-undo.
def    shared  var g-ordersuf    like oeeh.ordersuf      no-undo.
/* def    shared  var g-whse        like oeeh.whse          no-undo.  */
def            var s-phoneno     like arsc.phoneno       no-undo.
def            var s-pophoneno   like arsc.pophoneno     no-undo.
def            var s-seecostfl   like sasoo.seecostfl    no-undo.
def            var s-nontaxtype  like arsc.nontaxtype    no-undo.
def            var s-addcod      like pmep.addcod        no-undo.
def            var s-nontax      like sasta.descrip      no-undo.
def            var s-descript    as c format "x(15)"     no-undo.
def            var s-taxgroupnm  as c format "x(9)"      no-undo extent 5.
def            var s-taxoverdesc like sasta.descrip      no-undo.
def            var s-taxauth     like sasgl.descrip      no-undo.
def            var s-rebatedcost like oeeh.vendrebord    no-undo.

/*tb e12723 07/10/02 gwk; Require tax override reason code for changes to
    detail (new params for running oeetju.p)*/
def var s-taxoverfl              like oeeh.taxoverfl     no-undo.
def var s-taxovercd              like oeeh.taxovercd     no-undo.

def var s-statecd                like sasgm.statecd      no-undo.
def var s-countycd               like sasgm.countycd     no-undo.
def var s-citycd                 like sasgm.citycd       no-undo.
def var s-other1cd               like sasgm.othercd      no-undo.
def var s-other2cd               like sasgm.othercd      no-undo.
def var s-other1amt              like arett.taxsaleamt   no-undo.
def var s-other2amt              like arett.taxsaleamt   no-undo.
def var s-statedesc              as c format "x(12)"     no-undo.
def var s-countydesc             as c format "x(12)"     no-undo.
def var s-citydesc               as c format "x(12)"     no-undo.
def var s-other1desc             as c format "x(12)"     no-undo.
def var s-other2desc             as c format "x(12)"     no-undo.
def new shared var v-putscreen   as c format "x(78)"     no-undo.

{oeioa.z99 &user_defvar = "*"}

{addon-tt.i &shared = "shared"}

/*tb 5366 05/11/99 cm; Added shipto notes */
/*tb e6494 01/11/01 gwk; Added totcostord */
/*tb e5979 Need to display both Other tax amounts as unique entries. Replaced
    oeeh.taxamt[4] with s-other1amt and added s-other2amt */
/*tb e16616 06/06/03 kjb; Adjust fields on the US frame to accommodate the
    change in length of the package id field from 15 to 18 */



form
    oeeh.shipto        colon 16 label "Ship To"
    arss.notesfl       at    26 no-label
    oeeh.totcostord    colon 55 label "Ordered Cost"
    s-phoneno          colon 16 label "Phone"
    oeeh.totcost       colon 55 label "Actual Cost"
    s-pophoneno        colon 16 label "Agent Phone"
    s-rebatedcost      colon 55 label "Rebated Cost"
    oeeh.route         colon 16 label "Route/Day/Stop"
    s-addcod           colon 55 label "Additional COD"
    oeeh.drdeldt       colon 16 label "Delivery Date"
    oeeh.drdeltm       colon 30 label "Tm"
    oeeh.drexpfl       colon 52 label "Sent to DRoute"
    oeeh.drholdfl      colon 68 label "Route Hold"
    s-statecd          colon 10 label "State"
    s-statedesc        at    24 no-label
    oeeh.taxamt[1]     at    37 no-label
    s-taxgroupnm[1]    to    58 no-label
    oeeh.taxsaleamt[1] at    60 no-label
    s-countycd         colon 10 label "County"
    s-countydesc       at    24 no-label
    oeeh.taxamt[2]     at    37 no-label
    s-taxgroupnm[2]    to    58 no-label
    oeeh.taxsaleamt[2] at    60 no-label
    s-citycd           colon 10 label "City"
    s-citydesc         at    24 no-label
    oeeh.taxamt[3]     at    37 no-label
    s-taxgroupnm[3]    to    58 no-label
    oeeh.taxsaleamt[3] at    60 no-label
    s-other1cd         colon 10 label "Other 1"
    s-other1desc       at    24 no-label
    s-other1amt        at    37 no-label
    s-taxgroupnm[4]    to    58 no-label
    oeeh.taxsaleamt[4] at    60 no-label
    s-other2cd         colon 10 label "Other 2"
    s-other2desc       at    24 no-label
    s-other2amt        at    37 no-label
    s-taxgroupnm[5]    to    58 no-label
    oeeh.taxsaleamt[5] at    60 no-label
    s-frtinout         colon 10 label "FrtIn/Out"
    v-frtflgdsp        colon 19 label "FO"
    oeeh.taxablefl     colon 38 label "Taxable"
    oeeh.pkgid         colon 10 label "Pkg ID"
    s-nontaxtype       colon 38 label "Reason"
    s-nontax           at 48    no-label
    oeeh.nopackages    colon 10 label "# Pkgs"
    oeeh.taxoverfl     colon 38 label "Override"
    oeeh.taxovercd     colon 38 label "Reason"
    s-taxoverdesc      at 48    no-label
    oeeh.taxdefltty    colon 38 label "Default Type"
    s-descript         at 48    no-label
    /* SX 55 */
    v-crmgr            colon 10 label "CrMgr"                
/*  v-crmgrnm          at 12 no-label  */                    
    s-margin           at 54 no-label                        
    s-margpct          at 68 no-label                        
    /* SX 55 */     
with frame f-oeioa overlay row 4 centered width 80 side-labels
    title "Additional Information For " + string(g-orderno) +
    "-" + string(g-ordersuf, "99").

/*tb 17779 02/20/95 dww; Canadian Tax changes per Groupe Laurier CIM and
    Systemetrix.  Groupe Laurier's job code was "pt1294". Systemetrix's
    Job Code was "CST5FX".  Change "Other" to "  GST". */
/*tb 5366 05/11/99 cm; Added shipto notes */
/*tb e6494 01/11/01 gwk; Added totcostord */
/*tb e16616 06/06/03 kjb; Adjust fields on the Canadian frame to accommodate
    the change in length of the package id field from 15 to 18 */
form
    oeeh.shipto        colon 16 label "Ship To"
    arss.notesfl       at    26 no-label
    oeeh.taxamt[1]     colon 55 label "Tax Amount - State"
    s-phoneno          colon 16 label "Phone"
    oeeh.taxamt[2]     colon 55 label "County"
    s-pophoneno        colon 16 label "Agent Phone"
    oeeh.taxamt[3]     colon 55 label "City"
    oeeh.route         colon 16 label "Route/Day/Stop"
    oeeh.taxamt[4]     colon 55 label "  GST"
    oeeh.totcostord    colon 16 label "Ordered Cost"
    oeeh.totcost       colon 16 label "Actual Cost"
    "Tax Sale Amount - "  at    37
    s-rebatedcost      colon 16 label "Rebated Cost"
    s-addcod           colon 16 label "Additional COD"
    s-taxgroupnm[1]    to    55 no-label
    oeeh.taxsaleamt[1] at    57 no-label
    s-taxgroupnm[2]    to    55 no-label
    oeeh.taxsaleamt[2] at    57 no-label
    oeeh.taxablefl     colon 16 label "GST/PST Taxable"
    s-taxgroupnm[3]    to    55 no-label
    oeeh.taxsaleamt[3] at    57 no-label
    oeeh.statecd       colon 16 label "Taxing State"
    s-statedesc        at    27 no-label
    s-taxgroupnm[4]    to    55 no-label
    oeeh.taxsaleamt[4] at    57 no-label
    oeeh.taxauth       colon 16 label "Tax Authority"
    s-taxauth          at    27 no-label
    s-taxgroupnm[5]    to    55 no-label
    oeeh.taxsaleamt[5] at    57 no-label
    oeeh.pstlicenseno  colon 16 label "PST License"
    oeeh.pkgid         colon 52 label "Pkg ID"
    oeeh.taxdefltty    colon 16 label "Default Type"
    s-descript         at 27    no-label
    oeeh.nopackages    colon 52 label "# Pkgs"
    /* SX 55 */
    s-grmarglbl        at 57 no-label                            
    v-crmgr            colon 10 label "CrMgr"                    
/*  v-crmgrnm          at 12 no-label */                       
    s-margin           at 54 no-label                            
    s-margpct          at 68 no-label                            
    "%"                at 76                                     
    /* SX 55 */           

with frame f-oeioac overlay row 4 centered width 80 side-labels
    title "Additional Information For " + string(g-orderno) +
    "-" + string(g-ordersuf, "99").

run idtaxi.p(
    input  g-cono,
    output v-modvtfl,
    output v-geocdfl,
    output v-geotxrgfl,
    output v-geoindxty).

pause 0 before-hide.
main:
do while true with frame f-oeioa on endkey undo main, leave main
                                 on error  undo main, leave main:

    {w-oeeh.i g-orderno g-ordersuf no-lock}
    if avail oeeh then do:
        {w-sasoo.i g-operinit no-lock}
        /*tb 11491 10/18/93 mwb; load off the oeeh record */
        /*tb 15410 04/20/94 mwb; load the non tax reason if not taxable, or
             if something in oeeh.nontaxtype. */
        /*tb 23133 07/24/97 mtt; Develop Credit Control Center Feature */

        {w-sasc.i no-lock}

        {oeioa.lpr}

        /*tb 24473 12/31/98 lbr; displaying wrong additional cod amt.
             added check codcollamt <> 0 for s-addcode */

        if avail arsc then
        assign
            s-phoneno       = if oeeh.shipto ne "" and avail arss and
                                arss.phoneno ne "" then arss.phoneno
                              else arsc.phoneno
            s-pophoneno     = if oeeh.shipto ne "" and avail arss and
                                arss.pophoneno ne "" then arss.pophoneno
                              else arsc.pophoneno
            s-rebatedcost   = if
                                (can-do("fo,qu,st,bl",oeeh.transtype)  or
                                 can-do("s,t",oeeh.orderdisp)          or
                                (oeeh.stagecd    < 2                  and
                                 oeeh.boexistsfl = no                 and
                                 oeeh.totqtyshp  = 0                  and
                                 oeeh.totqtyret  = 0))
                              then
                                  oeeh.totcostord - oeeh.vendrebord
                              else
                                  oeeh.totcost - oeeh.vendrebamt
            s-addcod        = if oeeh.codfl and oeeh.codcollamt <> 0 then
                                    oeeh.codcollamt - oeeh.totinvamt
                              else 0

            /*tb 11491 10/18/93 mwb; loaded up higher ***************
            s-nontaxtype    = if oeeh.shipto ne "" and avail arss   *
                                then arss.nontaxtype                *
                              else arsc.nontaxtype                  *
            *********************************************************/
            s-seecostfl     = if avail sasoo then sasoo.seecostfl
                              else no
            s-nontax        = if avail sasta and not oeeh.taxablefl
                                 then sasta.descrip
                              else if not oeeh.taxablefl then v-invalid
                              else ""
            s-descript      = if oeeh.taxdefltty = "c" then "Customer"
                              else if oeeh.taxdefltty = "s" then "Ship To"
                              else if oeeh.taxdefltty = "w" then "Warehouse"
                              else if oeeh.taxdefltty = "m" then
                                                         "Manual Override"
                              else ""
            s-taxgroupnm[1] = if avail sasc
                                 then fill(" ",8 - length(sasc.taxgroupnm[1])) +
                                    sasc.taxgroupnm[1] + ":"
                              else ""
            s-taxgroupnm[2] = if avail sasc
                                 then fill(" ",8 - length(sasc.taxgroupnm[2])) +
                                    sasc.taxgroupnm[2] + ":"
                              else ""
            s-taxgroupnm[3] = if avail sasc
                                 then fill(" ",8 - length(sasc.taxgroupnm[3])) +
                                    sasc.taxgroupnm[3] + ":"
                              else ""
            s-taxgroupnm[4] = if avail sasc
                                 then fill(" ",8 - length(sasc.taxgroup[4])) +
                                 sasc.taxgroupnm[4] + ":"
                              else ""
            s-taxgroupnm[5] = if avail sasc
                                 then fill(" ",8 - length(sasc.taxgroup[5])) +
                                 sasc.taxgroupnm[5] + ":"
                              else "".

        if oeeh.taxovercd <> "" then do:
            {sasta.gfi "aa" "oeeh.taxovercd" "no"}
            s-taxoverdesc = if avail sasta then sasta.descrip
                            else v-invalid.
        end.

        {oeioa.z99 &user_befdisp = "*"}

        /*tb 5366 05/11/99 cm; Added shipto notes */
        /*tb e6494 01/11/01 gwk; Added totcostord */
        /*tb e5979 11/29/01 kjb; Added s-other1amt and s-other2amt */

        if avail sasc and sasc.country = "us" then
        display
            oeeh.shipto
            "" @ arss.notesfl
            arss.notesfl when oeeh.shipto <> "" and avail arss
            s-phoneno
            oeeh.totcostord when s-seecostfl = yes
            oeeh.totcost when s-seecostfl = yes
            s-pophoneno
            s-rebatedcost when s-seecostfl = yes
            s-addcod
            oeeh.route
            oeeh.drdeldt
            oeeh.drdeltm
            oeeh.drexpfl
            oeeh.drholdfl
            s-statecd
            s-statedesc
            oeeh.taxamt[1]
            s-taxgroupnm[1]
            oeeh.taxsaleamt[1]
            s-countycd
            s-countydesc
            oeeh.taxamt[2]
            s-taxgroupnm[2]
            oeeh.taxsaleamt[2]
            s-citycd
            s-citydesc
            oeeh.taxamt[3]
            s-taxgroupnm[3]
            oeeh.taxsaleamt[3]
            s-other1cd
            s-other1desc
            s-other1amt
            s-taxgroupnm[4]
            oeeh.taxsaleamt[4]
            s-other2cd
            s-other2desc
            s-other2amt
            s-taxgroupnm[5]
            oeeh.taxsaleamt[5]
            oeeh.taxablefl
            s-nontaxtype
            s-nontax
            oeeh.taxoverfl
            oeeh.taxovercd
            s-taxoverdesc
            oeeh.taxdefltty
            s-descript
            oeeh.pkgid
            oeeh.nopackages
            /* SX 55 */
            s-frtinout
            v-frtflgdsp
            v-crmgr                         
/*          v-crmgrnm */                  
            s-grmarglbl                     
            s-margin                        
            s-margpct  
            /* SX 55 */
                                 
        with frame f-oeioa.
        else if avail sasc and sasc.country = "ca" then
        display oeeh.route
                s-phoneno
                s-pophoneno
                oeeh.shipto
                "" @ arss.notesfl
                arss.notesfl when oeeh.shipto <> "" and avail arss
                oeeh.taxauth
                oeeh.statecd
                oeeh.taxablefl
                oeeh.taxsaleamt[1]
                s-descript
                oeeh.taxsaleamt[2]
                oeeh.taxsaleamt[3]
                oeeh.taxsaleamt[4]
                oeeh.taxsaleamt[5]
                oeeh.taxamt[1]
                oeeh.taxamt[2]
                oeeh.taxamt[3]
                oeeh.taxamt[4]
                s-rebatedcost when s-seecostfl = yes
                s-addcod
                oeeh.taxdefltty
                s-statedesc
                s-taxauth
                s-taxgroupnm[2]
                s-taxgroupnm[3]
                s-taxgroupnm[4]
                s-taxgroupnm[5]
                s-taxgroupnm[1]
                oeeh.totcostord when s-seecostfl
                oeeh.totcost when s-seecostfl
                oeeh.pstlicenseno
                oeeh.pkgid
                oeeh.nopackages
            /* SX 55 */
                s-frtinout
                v-frtflgdsp
                v-crmgr                     
/*              v-crmgrnm */              
                s-grmarglbl                 
                s-margin                    
                s-margpct                   
            /* SX 55 */
        
        with frame f-oeioac.

        {oeioa.z99 &user_aftdisp = "*"}

        /*tb 24567 jgc, UPS OnLine Compatible certification (Clipper Ship) */
        /*d  Added menu to bottom of screen with options F8 - Packages and
             F10 - Tax Detail */
        /*d Removed help message "F10-Tax Detail..." and added new procedure
            call to oeioap.p if F8 is hit for pakage detail.  */
        loop:
        do while true on endkey undo main, leave main:
            {putscr.gsc &color  = "messages"
                        &f6     = "*"
                        &f6text = "Addons"
                        &f7     = "*"
                        &f7text = "ScheduleB"
                        &f8     = "*"
                        &f8text = "Packages"
                        &f10    = "*"
                        &f10text = "Tax Detail" }

            readkey.
            if {k-func6.i} then do:

                hide frame f-oeioa.
               put screen row 21 column 1 color message "".
                 run oe-addons.p
                      (input oeeh.orderno,
                       input oeeh.ordersuf,
                       input "oe",
                       input oeeh.totlineamt,
                       input "",
                       input oeeh.wodiscamt,
                       input oeeh.specdiscamt,
                       input "inquiry",
                       input-output table t-addon).
            end.
            
            if {k-func7.i} then
               run ScheduleBdisplay.p(g-orderno, g-ordersuf).
               

            
            if {k-func8.i} then
               run oeioap.p(g-orderno, g-ordersuf, "O" ).

            if {k-func10.i} then do:
               assign
                   s-taxoverfl  = oeeh.taxoverfl
                   s-taxovercd  = oeeh.taxovercd.
               run oeetju.p (input-output s-taxoverfl,
                             input-output s-taxovercd,
                             input-output s-taxoverdesc).
            end.
            if {k-jump.i} or {k-cancel.i} then
               leave.
            else
               next.
        end. /* do on endkey */
        leave.
    end. /*if avail oeeh*/
end. /*main*/
hide frame f-oeioa.