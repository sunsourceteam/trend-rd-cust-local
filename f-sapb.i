/* SX 55

f-sapb.i 1.1 01/03/98 */
/* f-sapb.i 1.1 01/03/98 */
/*h*****************************************************************************

  INCLUDE      : f-sapb.i
  DESCRIPTION  : Form for SAPB
  USED ONCE?   : No
  AUTHOR       : enp
  DATE WRITTEN :
  CHANGES MADE :
    07/25/94  rs; TB# 3491 Rpt Manager Schedule Enhancement
    08/23/94  rs; TB# 12464 Schedule jobs only in future
    05/02/07 des; TB# e26319 Allow 30 character option entry
*******************************************************************************/

form
    b-sassr.currproc                        at 4    {f-help.i}
    "Title: "                               at 21
    b-sapb.rpttitle                         at 28
    s-heading1                              at 2
    s-heading2                              at 31
    s-reportnm     like sapb.reportnm       at 2    {f-help.i}
    s-back                                  at 12
       help "(Y)es to Print in Background Mode"
    b-sapb.printernm                        at 17 help
       "Leave Blank to Print to Video or Enter Printer or File Name [L]"
    b-sapb.printoptfl                       at 28
        help "(Y)es to Print Options Page"
/*tb 3491 07/25/94 rs; Rpt Manager Schedule Enhancement */
    b-sapb.starttype                        at 32
       {f-help.i}
        validate({v-sasta.i "h" "b-sapb.starttype"},{valmsg.gme 4109})
/*tb 12464 08/23/94 rs; Schedule jobs only in future */
    b-sapb.startdt                          at 37
        help "Enter Starting Date"
    s-hr                                    at 46   auto-return
        help "Report Will Run AFTER This Time"
        validate(integer(s-hr) ge 1 and integer(s-hr) le 24,{valmsg.gme 2114})
    v-colon                                 at 48
    s-mn                                    at 49   auto-return
        help "Report Will Run AFTER This Time"
        validate(integer(s-hr) ge 0 and integer(s-hr) le 59,{valmsg.gme 2115})
    s-ampm                                  at 52
    b-sapb.batchnm                          at 55
    b-sapb.runnowfl                         at 65
    b-sapb.delfl                            at 73
        help "(Y)es to Remove Stored Report After Running"
  with frame f-sapb1 no-labels width 80 row 1.
                        
form
   s-req           as logical format "*/ " at 1
   i               format "z9"             at 2
   s-rangenm       as c format "x(24)"     at 5
   s-rangebeg      as c format "x(24)"     at 30
   s-rangeend      as c format "x(24)"     at 55
with frame f-sapb2 v-rdown down no-labels width 80 row 6 title
"Rq  Range  (" + string(b-sassr.rangecnt,"99") 
+ ")                              Value                            ".


form
   s-req           format "*/ "            at 1
   i               format "z9"             at 2
   s-optnm         as c format "x(40)"     at 5
   s-opttype       as c format "x(30)"     at 46
     help ""
with frame f-sapb3 v-odown down no-labels
   width 80 row v-orow title
"Rq  Option (" + string(b-sassr.optioncnt,"99") 
+ ")                              Value                            ".
           

on f10 anywhere
 do:
 run zsdirpthlp.p.
 end.
