/* z-poparc.i 1.1 6/12/92 */
/*h*****************************************************************************
  INCLUDE              : z-poparc.i
  DESCRIPTION          : Custom Assignments for Pop-Up Window on Setups
  AUTHOR               : enp
  DATE LAST MODIFIED   : 04/22/92
  CHANGES MADE AND DATE:
*******************************************************************************/
define buffer z-arsc for arsc.


define var v-technology as character format "x(30)" no-undo.
define buffer catmaster for notes.
define buffer slsmx     for notes.

define var j-slsinx as integer no-undo.


form

z-arsc.custno at 5 
z-arsc.name at 5
z-arsc.xxl2 at 5   label "Exclude Margin Check"
z-arsc.xxl13 at 5   label "Duplicate POs are Allowed"
z-arsc.xxi1 at 5   label "Tax Code"
 with frame f-arsczk overlay side-labels width 45 column 20 row 8 title
    " Custom Extended ARSC Update ".
  


On CTRL-O  anywhere  
  do:
  if g-ourproc begins "arsc" then
   do:
   run zsdiarscpop.p.
   end.
  end. 
