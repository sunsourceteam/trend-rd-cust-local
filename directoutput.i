/**********************
**  DIRECTOUTPUT.i   **
**********************/
/*{1} represents warehouse */

if {1} = "ddes" then
   output thru "lp -dwdesshplj5 -s -c".
if {1} = "dndn" then
   output thru "lp -dwdencntlj4v -s -c".
if {1} = "dspk" then
   output thru "lp -dwrencntlj5 -s -c".
if {1} = "dslc" then
   output thru "lp -dwslcofflj4v -s -c".
if {1} = "dprc" then
   output thru "lp -dwprcpcklj5n -s -c".
if {1} = "dpho" then
   output thru "lp -dwphoofflj5n -s -c".
if {1} = "dtuc" then
   output thru "lp -dwtucofflj5n -s -c".
if {1} = "dalb" then
   output thru "lp -dwalbcntlj5n -s -c".
if {1} = "dlv" then
   output thru "lp -dwlvgcntlj5n -s -c".
if {1} = "dros" then
   output thru "lp -dwrospcklj5n -s -c".
end.   
   
