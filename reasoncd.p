/* reasoncd.p Select Handheld ReasonCode */   
/* choose an exception code */
define input parameter inCono as int no-undo.
define input parameter inKey  as char no-undo.
define input parameter inTitle as char format "x(26)" no-undo.
define input parameter hhFlag as logical no-undo. /* Handheld? */
define output parameter outCode as char no-undo.

define query q-reason for zsastz scrolling.
define query q-fullreason for zsastz scrolling.

define browse b-reason query q-reason
  display zsastz.primarykey format "x(2)"
          zsastz.secondarykey format "x(20)"
  with 12 down width 26 overlay no-hide no-labels no-box.
define browse b-fullreason query q-fullreason
  display zsastz.primarykey format "x(2)"
          zsastz.secondarykey format "x(35)"
  with 15 down width 40 overlay no-hide no-labels no-box.

form
  inTitle  at 1 dcolor 2 format "x(26)"
  b-reason at 1
with frame f-reason width 26 no-labels overlay no-box.

form
  inTitle at 6 dcolor 2 format "x(30)"
  skip(1)
  b-fullreason at 1
with frame f-fullreason width 42 centered overlay no-labels.

on any-key of b-reason in frame f-reason do:
  if {k-accept.i} or {k-return.i} then do:
    if avail zsastz then
      outCode = zsastz.primarykey.
    else
      outCode = "".
    hide frame f-reason.
    apply "window-close" to frame f-reason. 
  end.
  if {k-cancel.i} then do:
    outCode = "".
    hide frame f-reason.
    apply "window-close" to frame f-reason.
  end.
end.


on any-key of b-fullreason in frame f-fullreason do:
  if {k-accept.i} or {k-return.i} then do:
    if avail zsastz then
      outCode = zsastz.primarykey.
    else
      outCode = "".
    hide frame f-fullreason.
    apply "window-close" to frame f-fullreason. 
  end.
  if {k-cancel.i} then do:
    outCode = "".
    hide frame f-fullreason.
    apply "window-close" to frame f-fullreason.
  end.
end.

on cursor-up cursor-up.
on cursor-down cursor-down.
if hhFlag then do:
  open query q-reason for each zsastz where
  cono = inCono and codeiden = inKey and labelfl = no no-lock
  by codeiden by primarykey by secondarykey.
  clear frame f-reason.  
  enable b-reason with frame f-reason.
  apply "ENTRY" to b-reason in frame f-reason.
  display inTitle with frame f-reason.
  put screen row 13 column 1 "                          ".
  wait-for window-close of frame f-reason. 
  close query q-reason.
end.
else do:
  open query q-fullreason for each zsastz where
  cono = inCono and codeiden = inKey and labelfl = no no-lock
  by codeiden by primarykey by secondarykey.
  clear frame f-fullreason.  
  enable b-fullreason with frame f-fullreason.
  apply "ENTRY" to b-fullreason in frame f-fullreason.
  display inTitle with frame f-fullreason.
  wait-for window-close of frame f-fullreason. 
  close query q-fullreason.
end.
on cursor-up back-tab.
on cursor-down tab.

