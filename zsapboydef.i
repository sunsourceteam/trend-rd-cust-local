/* z-sapbozdef.i         
 
   Definitions for Custom SAPBO reocrds

*/


define temp-table zsapbo no-undo

  field selection_type as character format "xx"
  field selection_mode as character format "x"
  field selection_char4 as character format "x(4)"
  field selection_cust like arsc.custno

  index zsapbo_inx
        selection_type
        selection_mode
        selection_char4
        selection_cust.

define var zelection_good      as logical              no-undo.

/* Zelection_matrix has to have an extent for each selection type 
   right now we have. During the load these booleans are set showing what
   options have been selected.
   
   s - sales rep               ....Index 1
   c - customer number         ....Index 2
   p - product cat             ....Index 3
   b - buyer                   ....Index 4
   t - Order types             ....Index 5
   w - Warehouse               ....Index 6
   v - Vendor number           ....Index 7
   N - Vendor  Name            ....Index 8
   e - Vendor parent Id        ....Index 9
   g - Technology              ....Index 10
   U - Product                 ....Index 11
   h - Customer Ship To        ....Index 12
   L - Lostbusiness reason     ....Index 13
   k - Takenby                 ....Index 14
   bt - Batch                            15
   ex - Exception                        16
   o - order no                          17
   J - Inside Sales Rep                  18 
 PLEASE KEEP THIS DOCUMENTATION UPDATED WHEN CHANGED 
 
*/ 
define var zelection_matrix    as integer extent 18  
 init
 [0, 0, 0, 0, 0,
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0,
  0, 0]    no-undo.


define var zel_begcust as decimal init 0 no-undo.
define var zel_endcust as decimal init 999999999999 no-undo.
define var zel_hicust  as decimal init 999999999999 no-undo.
define var zel_begvend as decimal init 0 no-undo.
define var zel_endvend as decimal init 999999999999 no-undo.
define var zel_hivend  as decimal init 999999999999 no-undo.
define var zel_begrep  as character format "x(4)" init "    " no-undo.
define var zel_endrep  as character format "x(4)" init "~~~~~~~~" no-undo.
define var zel_Ibegrep as character format "x(4)" init "    " no-undo.
define var zel_Iendrep as character format "x(4)" init "~~~~~~~~" no-undo.
define var zel_begcat  as character format "x(4)" init "    " no-undo.
define var zel_endcat  as character format "x(4)" init "~~~~~~~~" no-undo.
define var zel_begbuy  as character format "x(4)" init "    " no-undo.
define var zel_endbuy  as character format "x(4)" init "~~~~~~~~" no-undo.
define var zel_begtype as character format "x(4)" init "    " no-undo.
define var zel_endtype as character format "x(4)" init "~~~~~~~~" no-undo.
define var zel_begwhse as character format "x(4)" init "    " no-undo.
define var zel_endwhse as character format "x(4)" init "~~~~~~~~"   no-undo.
define var zel_begLbus as character format "x(2)" init "    " no-undo.
define var zel_endLbus as character format "x(2)" init "~~~~"   no-undo.
define var zel_begtakenby as character format "x(4)" init "    " no-undo.
define var zel_endtakenby as character format "x(4)" init "~~~~~~~~"   no-undo.
define var zel_begvname as character format "x(4)" init "    "      no-undo.
define var zel_endvname as character format "x(4)" init "~~~~~~~~"  no-undo.
define var zel_begvpid   as character format "x(4)" init "    "     no-undo.
define var zel_endvpid   as character format "x(4)" init "~~~~~~~~" no-undo.
Define Var zel_techbeg   as character format "x(4)" init "    "     no-undo.
Define Var zel_techend   as character format "x(4)" init "~~~~~~~~" no-undo.
Define Var zel_Prodbeg   as character format "x(24)" 
         init "                        "     no-undo.
Define Var zel_Prodend   as character format "x(24)" 
         init "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" no-undo.
define var zel_begshipto  as character format "x(8)" init "    " no-undo.      define var zel_endshipto  as character format "x(8)" init "~~~~~~~~" no-undo.  
define var zel_begbatch   as dec init 0 no-undo.
define var zel_endbatch   as dec init 999999999999 no-undo.
define var zel_begexcept  as char format "x(2)" init "  " no-undo.
def var    zel_endexcept  as char format "x(2)" init "~~" no-undo.

define var zel_begordno as decimal init 0 no-undo.
define var zel_endordno as decimal init 999999999999 no-undo.


/* The appropriate ranges will need to be filled in here for the includes
   to automatically compute 'i' type records. Or you can do it manually 
   yourself
   
*/

define var zzl_begcust as decimal init 0 no-undo.
define var zzl_endcust as decimal init 999999999999 no-undo.
define var zzl_hicust  as decimal init 999999999999 no-undo.
define var zzl_begvend as decimal init 0 no-undo.
define var zzl_endvend as decimal init 999999999999 no-undo.
define var zzl_hivend  as decimal init 999999999999 no-undo.
define var zzl_begrep  as character format "x(4)" init "    " no-undo.
define var zzl_endrep  as character format "x(4)" init "~~~~~~~~" no-undo.
define var zzl_Ibegrep  as character format "x(4)" init "    " no-undo.
define var zzl_Iendrep  as character format "x(4)" init "~~~~~~~~" no-undo.
define var zzl_begcat  as character format "x(4)" init "    " no-undo.
define var zzl_endcat  as character format "x(4)" init "~~~~~~~~" no-undo.
define var zzl_begbuy  as character format "x(4)" init "    " no-undo.
define var zzl_endbuy  as character format "x(4)" init "~~~~~~~~" no-undo.
define var zzl_begtype as character format "x(4)" init "    " no-undo.
define var zzl_endtype as character format "x(4)" init "~~~~~~~~" no-undo.
define var zzl_begwhse as character format "x(4)" init "    " no-undo.
define var zzl_endwhse as character format "x(4)" init "~~~~~~~~" no-undo.
define var zzl_begLbus as character format "x(2)" init "    " no-undo.
define var zzl_endLbus as character format "x(2)" init "~~~~"   no-undo.
define var zzl_begtakenby as character format "x(4)" init "    " no-undo.
define var zzl_endtakenby as character format "x(4)" init "~~~~~~~~"   no-undo.
define var zzl_begvname as character format "x(4)" init "    " no-undo.
define var zzl_endvname as character format "x(4)" init "~~~~~~~~" no-undo.
define var zzl_begvpid as character format "x(4)" init "    " no-undo.
define var zzl_endvpid as character format "x(4)" init "~~~~~~~~" no-undo.
Define Var zzl_techbeg   as character format "x(4)" init "    "     no-undo.
Define Var zzl_techend   as character format "x(4)" init "~~~~~~~~" no-undo.
Define Var zzl_Prodbeg   as character format "x(24)" 
         init "                        "     no-undo.
Define Var zzl_Prodend   as character format "x(24)" 
         init "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" no-undo.
define var zzl_begshipto  as character format "x(8)" init "    " no-undo.       define var zzl_endshipto  as character format "x(8)" init "~~~~~~~~" no-undo. 
define var zzl_begbatch   as dec init 0 no-undo.
define var zzl_endbatch   as dec init 999999999999 no-undo.
define var zzl_begexcept  as char format "x(2)" init "  " no-undo.
def var    zzl_endexcept  as char format "x(2)" init "~~" no-undo.
define var zzl_begordno as decimal init 0 no-undo.
define var zzl_endordno as decimal init 999999999999 no-undo.


define var zelection_type      as character format "x"     no-undo. 
define var zelection_mode      as character format "x"     no-undo.
define var zelection_slsrep  as character format "x(4)"    no-undo.
define var zelection_prodcat   as character format "x(4)"  no-undo.
define var zelection_user3    as character format "x(15)"  no-undo.
define var zelection_char4     as character format "x(4)"  no-undo.
define var zelection_cust      like arsc.custno            no-undo.
define var zelection_vend      like arsc.custno            no-undo.
define var zelection_vname     like arsc.lookupnm          no-undo.
define var zelection_vpid       like arsc.lookupnm          no-undo.
define var zelection_prod      like icsp.prod              no-undo.


form "Selection Criteria List"  at 1 with frame zygot1.
form  " " at 1 with frame zygot2.

form "Type - " at 1
       zsapbo.selection_type at 9 format "x(2)"
     "Mode - " at 12
       zsapbo.selection_mode at 19
     "Selection - "   at 21
       zsapbo.selection_char4 at 33 format "x(25)" with frame zygot4
      no-box no-underline no-labels .


form "Type - " at 1
       zsapbo.selection_type at 9 format "x(2)"
      "Mode - " at 12
       zsapbo.selection_mode at 19
      "Selection - "   at 21
       zsapbo.selection_cust at 33 with frame zygot3
        no-box no-underline no-labels .
