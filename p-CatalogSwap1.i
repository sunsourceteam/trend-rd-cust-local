/* Always Clear out sl-notes */  
find first sl-notes where 
           sl-notes.cono         = g-cono             and
           sl-notes.notestype    = "vxref"            and
           sl-notes.primarykey   = "Dummy"    and
           sl-notes.secondarykey = "Garbage"   
           no-lock no-error.
/**
if g-operinit = "das" then
  do:
  message "prod:" {&prodX}
          "g-whse:" g-whse. pause.  
end.
**/  
find icsc where icsc.catalog = {&prodX} and  /*don't swap if already swapped*/
         substr(icsc.user4,43,12) = " "
                no-lock no-error.
if avail icsc then
  do:
  /* Check if SLZC exists */
  find first sl-notes where 
             sl-notes.cono       = g-cono                and  
             sl-notes.notestype  = "vxref"               and
             sl-notes.primarykey = icsc.user1        and
             sl-notes.secondarykey begins icsc.user2 and
             sl-notes.noteln[1]  = g-whse        
             no-lock no-error.
  if not avail sl-notes then
    find first sl-notes where 
               sl-notes.cono       = g-cono                and
               sl-notes.notestype  = "vxref"               and
               sl-notes.primarykey = icsc.user1        and  
               sl-notes.secondarykey begins icsc.user2 and
               sl-notes.noteln[1]  = "*"
               no-lock no-error.
  /*********************** 
  if SLZC exists, swap out
     icsc.prodcat
     icsc.pricetype
     icsc.prodcost
     icsc.stndcost
     icsc.vendno
     icsc.prodline
     icsc.rebatety
  ************************/
  /**
  if g-operinit = "das" then
    do:
    if avail sl-notes then
      do:
      message "found sl-notes". pause.
    end.
    else
      do:
      message "sl-notes not found". pause.
    end.
  end.
  **/
  if avail sl-notes then
    do:
    for each u-icsc where u-icsc.catalog = {&prodX} exclusive-lock:    
      assign u-icsc.ecbatchnm     = g-operinit.
      overlay(u-icsc.user4,1,4)   = u-icsc.prodcat.  
      overlay(u-icsc.user4,7,4)   = u-icsc.pricetype.
      overlay(u-icsc.user4,13,13) = string(u-icsc.prodcost,"9999999.99999").
      overlay(u-icsc.user4,28,13) = string(u-icsc.stndcost,"9999999.99999").
      overlay(u-icsc.user4,43,12) = string(u-icsc.vendno,"99999999999").   
      overlay(u-icsc.user4,57,6)  = u-icsc.prodline.
      overlay(u-icsc.user4,67,8)  = u-icsc.rebatety.
      /*xxc14 used as holding area for original values in case user cancels*/
      if u-icsc.xxc14 = " " then
        do:
        overlay(u-icsc.xxc14,1,4)   = substr(u-icsc.user4,1,4).  
        overlay(u-icsc.xxc14,7,4)   = substr(u-icsc.user4,7,4).
        overlay(u-icsc.xxc14,13,13) = substr(u-icsc.user4,13,13).
        overlay(u-icsc.xxc14,28,13) = substr(u-icsc.user4,28,13).
        overlay(u-icsc.xxc14,43,12) = substr(u-icsc.user4,43,12).
        overlay(u-icsc.xxc14,57,6)  = substr(u-icsc.user4,57,6).
        overlay(u-icsc.xxc14,67,8)  = substr(u-icsc.user4,67,8).
      end.
    
      assign u-icsc.prodcat   = sl-notes.noteln[2]
             u-icsc.pricetype = sl-notes.noteln[3]
             u-icsc.prodcost  = ROUND(u-icsc.listprice * 
                                      dec(sl-notes.noteln[4]),2).
      /**
      if g-operinit begins "das" then
        do:
        message "u-icsc.listprice:" u-icsc.listprice 
                "u-icsc.prodcost:"  u-icsc.prodcost. pause. 
      end.
      **/
      find first sc-notes where
                 sc-notes.cono         = g-cono and
                 sc-notes.notestype    = "ZZ" and
                 sc-notes.primarykey   = "standard cost markup" and
                 sc-notes.secondarykey = u-icsc.prodcat and
                 sc-notes.pageno       = 1
                 no-lock no-error.
      if not avail sc-notes then
        find sc-notes use-index k-notes where
             sc-notes.cono         = g-cono and
             sc-notes.notestype    = "zz" and
             sc-notes.primarykey   = "standard cost markup" and
             sc-notes.secondarykey = "" and
             sc-notes.pageno       = 1
             no-lock no-error.
      if avail sc-notes then
        assign u-icsc.stndcost = ROUND(u-icsc.prodcost *
                                 (1 + (dec(sc-notes.noteln[1]) / 100)),5).
      else
        assign u-icsc.stndcost = ROUND((u-icsc.prodcost * 1.03),5).
      /**
      if g-operinit begins "das" then
        do:
        message "u-icsc.stndcost:" u-icsc.stndcost. pause.
      end.
      **/
      assign u-icsc.vendno   = dec(sl-notes.noteln[5])
             u-icsc.prodline = sl-notes.noteln[6]
             u-icsc.rebatety = sl-notes.noteln[7].
    end. /* for each u-icsc */ 
  end. /* avail sl-notes */
end. /* avail icsc */

