/*h****************************************************************************
  INCLUDE      : vaiiicer.lpr
  DESCRIPTION  : Value Add completion create icer for ii section lines
  USED ONCE?   : yes
  AUTHOR       : des
  DATE WRITTEN : 06/19/03
  CHANGES MADE :
    06/19/03 des; TB# e7291 Created New
                ******************************************************************************/
/* Required Parameters:
    &vano   - The VA number for the ii section to be processed
    &vasuf  - The VA suffix for the ii section to be processed
    &seqno  - The sequence number for the ii section to be processed

    Optional Parameters:
      &lbuf   - Buffer prefix for vaesl table
      &rbuf   - Buffer prefix for icer table
      &pbuf   - Buffer prefix for icsp table
                          ******************************************************************************/

for each {&lbuf}vaesl where
    {&lbuf}vaesl.cono  = g-cono   and
    {&lbuf}vaesl.vano  = {&vano}  and
    {&lbuf}vaesl.vasuf = {&vasuf} and
    {&lbuf}vaesl.seqno = {&seqno}
     no-lock:

  {w-icsp.i {&lbuf}vaesl.shipprod no-lock {&pbuf} }

  if (v-oebofillfl = yes                  or
      v-oeautofity <> "n":u)              and
     ({&lbuf}vaesl.stkqtyship -
      {&lbuf}vaesl.qtyunavail ) >= 0      and
      {&lbuf}vaesl.stkqtyship > 0          and
     ({&lbuf}vaesl.nonstockty <> ""       or
     ({&lbuf}vaesl.nonstockty = ""       and
     avail {&pbuf}icsp                  and
     {&pbuf}icsp.statustype <> "l":u))
     then do:

    v-order = if {&lbuf}vaesl.nonstockty = "n":u then "n":u
              else "".

    if v-order = "n":u                  or
       (v-order = ""                    and
       (not {icer.gvl 
               {&lbuf}vaesl.whse
               {&lbuf}vaesl.shipprod
               """"
               """"}                   and
         not {icer.gvl
                {&lbuf}vaesl.whse
                {&lbuf}vaesl.shipprod
                """"
                g-operinit}))  then do:

      create {&rbuf}icer.
      assign
        {&rbuf}icer.cono        = g-cono
        {&rbuf}icer.whse        = {&lbuf}vaesl.whse
        {&rbuf}icer.prod        = {&lbuf}vaesl.shipprod
        {&rbuf}icer.ordertype   = if {&lbuf}vaesl.stkqtyship -
                                     {&lbuf}vaesl.qtyunavail = 0 then "z":u
                                  else v-order
        {&rbuf}icer.pono        = 0
        {&rbuf}icer.posuf       = 0
        {&rbuf}icer.lineno      = 0
        {&rbuf}icer.qtyrcvd     = {&lbuf}vaesl.stkqtyship
        {&rbuf}icer.proddesc    = if v-order = "n":u then
                                    {&lbuf}vaesl.proddesc
                                  else ""
        {&rbuf}icer.proddesc2   = if v-order = "n":u then
                                    {&lbuf}vaesl.proddesc2
                                  else ""
        {&rbuf}icer.botype      = "y":u
        {&rbuf}icer.processinit = if v-oeautofity <> "n":u then
                                    g-operinit
                                  else "".
      {vaiiicer.z99 &user_after_assign = "*"
                    &xlbuf  = {&lbuf}vaesl
                    &xrbuf  = {&rbuf}icer}

      {t-all.i {&rbuf}icer}

 /* Override the transproc here so that oeepb will process
    correctly.  Without this, the icer will get out of sync with
    it's quanity and will not appear or appear incorrectly on the
    receipts report.  Since the icer is in essence a temporary
    record, this overridding of the transproc is OK */
      assign
        {&rbuf}icer.transproc  = "vaii":u.
    end. /* if can-do */

  end. /* if v-oebofill = yes or... */

end. /* for each vaesl */
