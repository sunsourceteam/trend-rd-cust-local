/* poepp1vp.lpr 1.1 01/03/98 */
/* poepp1vp.lpr 1.1 06/27/96 */
/*h****************************************************************************
  INCLUDE      : poepp1vp.lpr
  DESCRIPTION  : Print Purchase Order Created by a Value Add Transaction
  USED ONCE?   : no
  AUTHOR       : smn
  DATE WRITTEN : 06/27/96
  CHANGES MADE :
    06/27/96 smn; TB# 21254 (18G) Develop Value Add Module
    12/13/96 mtt; TB# 21254 (18G) Develop Value Add Module
    11/28/05 blw; TB# L1763 Add serial/lot printing for PO's tied to VA orders.
******************************************************************************/
do with frame f-vadata:

&scoped-define comforvaout out

if substring(poeh.createdby,5) = "-va" then do:
    do for poelo:
        find first poelo use-index k-poelo where
            poelo.cono      = poel.cono    and
            poelo.pono      = poel.pono    and
            poelo.posuf     = 0            and
            poelo.lineno    = poel.lineno  and
            poelo.ordertype = "f"
        no-lock no-error.
        if avail poelo then do:
            find t-vadata use-index k-vadata where
                 t-vadata.vano  = poelo.orderaltno and
                 t-vadata.vasuf = poelo.orderaltsuf
            exclusive-lock no-error.
            if avail t-vadata and t-vadata.printfl = no then do:
                assign
                    v-vafstspecfl  = yes
                    v-vafstinvfl   = yes
                    v-vafstlinefl  = yes
                    t-vadata.print = yes.
                for each vaes use-index k-vaes where
                         vaes.cono         = g-cono         and
                         vaes.vano         = t-vadata.vano  and
                         vaes.vasuf        = t-vadata.vasuf and
                         vaes.specdata     ne ""            and
                         ( (vaes.sctntype  = "sp"           and
                            can-do("e,b,",vaes.specprtty))  or
                           (vaes.sctntype  = "ex"           and
                            vaes.seqno     = poelo.seqaltno and
                            vaes.specprtfl = yes) )
                no-lock:
                    if v-vafstspecfl = yes then do:
                        assign
                            v-vafstspecfl = no
                            s-vadata      = "Specifications/Instructions:".
                        display
                            s-vadata with frame f-vadata.
                        down with frame f-vadata.
                    end. /* if v-vafstspecfl = yes */
                    display vaes.specdata with frame f-vaspecdata.
                    down with frame f-vaspecdata.
                end.  /* for each vaes with sectiontype = sp */

                for each vaes use-index k-vaes where
                    vaes.cono         = g-cono          and
                    vaes.vano         = t-vadata.vano   and
                    vaes.vasuf        = t-vadata.vasuf  and
                    vaes.destvendno   = poeh.vendno     and
                    vaes.destshipfmno = poeh.shipfmno   and
                    vaes.sctntype     = "in"
                no-lock:
                    for each vaesl use-index k-vaesl where
                             vaesl.cono       = g-cono          and
                             vaesl.vano       = t-vadata.vano   and
                             vaesl.vasuf      = t-vadata.vasuf  and
                             vaesl.seqno      = vaes.seqno      and
                             vaesl.qtyship    ne 0
                    no-lock:
                        if v-vafstinvfl = yes then do:
                            assign
                                v-vafstinvfl = no
                                s-vadata     =
                                  "Inventory components to be sent " +
                                  "for fabrication - Reference Value " +
                                  "Add Order# " +
                                  string(vaes.vano,"zzzzzz9") + "-" +
                                  string(vaes.vasuf,"99").
                            display
                                s-vadata with frame f-vadata.
                            down with frame f-vadata.

                            assign
                                s-vadata                      = ""
                                substring(s-vadata,3,7)       = "Product"
                                substring(s-vadata,26,11)     = "Description"
                                substring(s-vadata,52,8)      = "Quantity"
                                substring(s-vadata,62,4)      = "Unit"
                                substring(s-vadata,67,12)     = "Stk Quantity".
                            display s-vadata with frame f-vadata.
                            down with frame f-vadata.
                        end. /*if v-vafstinvfl = yes then do */

                        if vaesl.nonstockty ne "n":u
                        then do:
                            {w-icsp.i vaesl.shipprod no-lock}
                            {w-icsw.i vaesl.shipprod vaesl.whse no-lock}
                        end.

                        assign
                             s-vadata                      = ""
                             substring(s-vadata,3,22)      = vaesl.shipprod
                             substring(s-vadata,26,24)     =
                                                   if vaesl.nonstockty = "n"   
                                                    then vaesl.proddesc
                                                   else
                                                   if avail icsp then          
                                                    icsp.descrip[1]
                                                   else v-invalid
                             substring(s-vadata,51,10)     =
                                             string(vaesl.qtyship,"zzzzzz9.99")
                             substring(s-vadata,62,4)      = vaesl.unit
                             substring(s-vadata,70,5)      =
                                         string(vaesl.stkqtyship,"zzzzzz9.99").
                        display s-vadata with frame f-vadata.
                        down with frame f-vadata.
                        if available icsw and
                            icsw.serlottype <> ""
                        then do:

                            {p-oeepp1.i
                                &type = "f"
                                &line = "vaesl"
                                &wono = "vaes.vano"
                                &wosuf = "vaes.vasuf"
                                &lineno = "vaesl.lineno"
                                &pref   = "va"
                                &seq = "vaes.seqno"
                                &stkqtyship = "vaesl.qtyship"
                                &snlot      = "v-nosnlots"}

                        end. /* serial / lot data */
                    end. /* for each vaesl */
                end. /* for each vaes */
            end. /* if avail t-vadata */
        end. /* if avail poelo */
    end. /* do for poelo */

    if poel.vafakeprodfl = yes then do:
        assign
            s-vadata     = ""
            s-vadata     = "**** The following line item represents the" +
                           " fabricated product ****".
        display s-vadata with frame f-vadata.
        down with frame f-vadata.
    end. /* if poel.vafakeprodfl = yes */
end. /* if substring ... = "-va" */
end. /* do with frame f-vadata */


