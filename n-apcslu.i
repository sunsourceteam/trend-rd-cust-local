/*n-apcslu.i  1.1 01/03/98 */
/*n-apcslu.i  1.4 7/30/93  */
/******************* n-apcslu.i **********************************/
/* (city/state lookup for vendors                                */
/*****************************************************************/
/*tb 7191 07/20/92 mwb; added vendno change */
/*tb 10582 05/05/93 jrg; Copied from AR lookup */
/* 07/30/93 jtc/f&w; Alpha Custno change       */

if s-type then
  do:
  find {1} apsv use-index k-addr where apsv.cono = g-cono and
   apsv.state = s-state
   {2}
   and apsv.city >= v-start
/{2}* */ and
  apsv.city begins s-city
  and 
        ((g-ourproc begins "oe" and
           not can-do("`,',|",substring(apsv.lookupnm,1,1))) or
         (not g-ourproc begins "oe"))
  no-lock no-error.

 if avail apsv then
    do:
    assign
      s-select       = string(apsv.vendno,"zzzzzzzzzzz9")
      s-vendno       = apsv.vendno
      s-name         = apsv.lookupnm +
                       if apsv.lookupnm = substring(apsv.name,1,15) then
                         ""
                       else
                         "/" + apsv.name
      s-dispcity     = apsv.city.
    end.
  end.
else
  do:
  find {1} apss use-index k-addr where apss.cono = g-cono and
  apss.state = s-state
  {2}
  and apss.city >= v-start
/{2}* */ 
  and apss.city begins s-city and
  (v-vendno = 0 or apss.vendno  = v-vendno) no-lock no-error.
  if avail apss then
    do:
    assign
      s-select       = string(apss.vendno,"zzzzzzzzzzz9")
                     + " " + string(apss.shipfmno)   
      s-vendno       = apss.vendno
      s-name         = apss.name
      s-dispcity     = apss.city.
    end.
  end.
