confirm = true.

find {1} kpz  use-index izsg where
         kpz.kitprod = g-prod  /* and
         kpz.whse    = g-whse  */
         no-lock no-error.


if not avail kpz then do:
   confirm =  false.
end.

if avail kpz then do:

   
   assign var-prod = kpz.compprod.  
   assign var-total = var-total + kpz.stndcost.
/* assign var-extcost = kpz.stndcost * kpz.qtyneeded.  */

   
   {w-icsw.i var-prod g-whse no-lock}
   
      if avail icsw then do:
      
       find b-CallCost where                                          
            b-CallCost.Cono = g-cono and                              
            b-CallCost.NotesType = "ZZ" and                           
           b-CallCost.Primarykey = "SdiCallCost" no-lock no-error.   
      if avail b-CallCost then                                          
          v-SdiCallCost = dec(b-CallCost.noteln[1]).                      
          else                                                            
           v-SdiCallCost = -9871234.987653.                              
                
         
         if icsw.stndcost <> v-SdiCallCost then
            assign var-extcost = kpz.stndcost * kpz.qtyneeded.  
         else
            assign var-extcost =  v-SdiCallCost.  
         
         
         assign var-qtyavail = (icsw.qtyonhand - icsw.qtyreservd  -            
                                icsw.qtycommit).
      end.                          
  
end.  
