/* q-oeepp1.i 1.1 01/03/98 */
/* q-oeepp1.i 1.4 10/29/93 */
/*h*****************************************************************************
  INCLUDE      : q-oeepp1.i
  DESCRIPTION  : variables and assignments needed in both WT and OE picking
  USED ONCE?   : no
  AUTHOR       : rhl
  DATE WRITTEN : 11/03/89
  CHANGES MADE :
    11/11/92 mms; TB# 8669  Rush; Sort rush order to top
    10/28/93 kmw; TB# 11758 Improve Unit Conversion Precision
    11/20/94 mms; TB# 14261 Added Ship Complete packing slip logic; code cleanup
    11/01/95 gp;  TB# 18091 Expand UPC Number (T20).  Add v-icupclength4.
*******************************************************************************/

{p-rptbeg.i}
{g-conv.i}

{g-oeepp1.i "new"}

/* BUFFERS */
def buffer b-{&line} for {&line}.

/* LOCAL VARIABLES */
def var p-sortty        as c format "x"                no-undo.
def var v-pageno        as i                           no-undo. 
def var v-invtofl       as l                           no-undo.
def var v-line          as i initial 46                no-undo.
def var v-whse          like icsw.whse                 no-undo.
def var v-qtyship       like oeel.qtyship              no-undo.
def var v-type          as c format "x(1)" initial "o" no-undo.
def var s-serialno      as c format "x(22)" extent 6   no-undo.
def var s-shiptoaddr    like oeeh.shiptoaddr           no-undo.

/*d Form define for pick ticket print */
{m-oeepp1.i &line = "{&line}"}

hide message no-pause.
pause 0 before-hide.

/*d Load range and option parameters from SAPB as they are not carried global
  to oeepp1.p */
assign
    p-listfl       = if sapb.optvalue[2] = "yes" then yes else no
    p-reprintfl    = if sapb.optvalue[6] = "yes" then yes else no
    p-sortty       = substring(sapb.optvalue[7],1,1)
    v-oepickordty  = if avail sasc then sasc.oepickordty else "l"
    v-oelinefl     = if avail sasc then sasc.oelinefl else no
    v-icmsdsprt    = if avail sasc then sasc.icmsdsprt else no
    v-icupclength4 = if avail sasc then sasc.icupclength4 else 0.
