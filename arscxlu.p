/* arscxlu.p 1.2 04/08/98 */
/*h*****************************************************************************
  PROCEDURE     : arscxlu
  DESCRIPTION   : Find ARSC by Keywords
  AUTHOR        : enp
  DATE WRITTEN  : ??/??/??
  CHANGES MADE  :
    01/31/94 dww; TB# 13927 Keyboard buffering Keyword lookups
    04/11/95 fef; TB# 18104 Progress error on numeric input.  Added &alpha
        to xxxxlu.i.
    12/27/95 dww; TB# 17501 Keyword Lookup frames do not hide when cancel is
        pressed.
    07/08/97 ajw; TB# 23410 Added User Hook &define_vars
    04/03/98 bm;  TB# 24767 Customer can display multiple times. Only do once.
*******************************************************************************/

{g-lu.i}
{g-help.i}

def input param       v-notused as logical  no-undo.

def var v-setup       as c initial ["arsc"] no-undo.
def var v-start       like arsc.lookupnm    no-undo.
def var s-words       as c extent 5         no-undo.
def var v-parse2      as i                  no-undo.
def var s-wordtxt     as c format "x(54)"   no-undo.
def var s-custno      like arsc.custno      no-undo.
def var s-name        like arsc.name        no-undo.
def var s-statustype  as c format "x(3)"    no-undo.
def var s-notesfl     like arsc.notesfl     no-undo.
def var s-city        as c format "x(24)"   no-undo.
def var v-dial        as c format "x(12)"   no-undo initial "ardial.p".
def var v-bp          as c format "x(2)" initial "bp"     no-undo.
def var v-skipfl      as l                  no-undo.

{ar_defvars_lookup.i "new"}

def buffer b-saindex for saindex.

def workfile w-cust no-undo
    field custno like arsc.custno.

v-length = 10.

/*u User Hook*/
{arscxlu.z99 &define_vars = "*"}

/*u User Hook*/
{arscllu.z99 &define_vars = "*"}


form /* Assign Words */
    s-words[1]  colon 9  label "Look For"
        validate(if ({k-func.i}) then true else s-words[1] ne "",
        "This is a Required Field (2100)")
        help "F6-Lkup Nm F7-Phone F9-City/St F10-Zip"
    s-words[2]  no-label
        help "F6-Lkup Nm F7-Phone F9-City/St F10-Zip"
    s-words[3]  no-label
        help "F6-Lkup Nm F7-Phone F9-City/St F10-Zip"
    s-words[4]  no-label
        help "F6-Lkup Nm F7-Phone F9-City/St F10-Zip"
    s-words[5]  no-label
        help "F6-Lkup Nm F7-Phone F9-City/St F10-Zip"
with frame f-words row 3 col 3 side-labels overlay
    title " Find by Keywords ".

form
    s-custno            at 1  label "Customer #"
    s-notesfl           at 13 no-label
    s-name              at 15 label "Name"
    s-city                    label "City, State"
    s-statustype              no-label
with frame f-lookup overlay column 3 scroll 1
    row 2 v-length down title s-wordtxt.

{c-lkup.i}

on entry of s-custno in frame f-lookup  
 do:          
   find z-arsc where z-arsc.cono = g-cono and
                     z-arsc.custno = input s-custno
   no-lock no-error.
   if avail z-arsc then
    do:
      run bottom_cust(input z-arsc.custno, 
                      input " ").
    end.
 end.

main:
do while true on endkey undo, leave:

    hide frame f-lookup no-pause.
    hide frame f-zcustinfo no-pause.
    if s-words[1] ne "" then next-prompt s-words[2] with frame f-words.
    if s-words[2] ne "" then next-prompt s-words[3] with frame f-words.
    if s-words[3] ne "" then next-prompt s-words[4] with frame f-words.
    if s-words[4] ne "" then next-prompt s-words[5] with frame f-words.

    if v-skipfl = false then do:
        update s-words go-on(f6 f7 f8 f9 f10) with frame f-words.
        hide frame f-words no-pause.
    end.
    else v-skipfl = false.

    if {k-func.i} then leave.

    for each w-cust:
        delete w-cust.
    end.

    do j = 1 to 3:
        do i = 2 to 4:
            if s-words[i] = "" and s-words[i + 1] ne "" then
                assign
                    s-words[i]     = s-words[i + 1]
                    s-words[i + 1] = "".
        end.
    end.

    /*d set k = to highest filled in word */
    assign
        k = if s-words[5] ne "" then 5 else
            if s-words[4] ne "" then 4 else
            if s-words[3] ne "" then 3 else
            if s-words[2] ne "" then 2 else 1
        j = 0.

    /*tb 13927 01/31/94 dww; Keyboard buffering Keyword lookups-ICSW */
    /* input clear. */
    cr:
    for each saindex use-index k-saindex where
        saindex.cono     = g-cono and
        saindex.xtype    = "ar"   and
        saindex.crossref begins s-words[k] and
       (s-words[1] = "" or
        can-find(first b-saindex where
        b-saindex.cono      = g-cono            and
        b-saindex.xtype     = "ar"              and
        b-saindex.crossref  begins s-words[1]   and
        b-saindex.prod      = saindex.prod))    and
       (s-words[2] = "" or
        can-find(first b-saindex where
        b-saindex.cono      = g-cono            and
        b-saindex.xtype     = "ar"              and
        b-saindex.crossref  begins s-words[2]   and
        b-saindex.prod      = saindex.prod))    and
       (s-words[3] = "" or
        can-find(first b-saindex where
        b-saindex.cono      = g-cono            and
        b-saindex.xtype     = "ar"              and
        b-saindex.crossref  begins s-words[3]   and
        b-saindex.prod      = saindex.prod))    and
       (s-words[4] = "" or
        can-find(first b-saindex where
        b-saindex.cono      = g-cono            and
        b-saindex.xtype     = "ar"              and
        b-saindex.crossref  begins s-words[4]   and
        b-saindex.prod      = saindex.prod))
    no-lock:
        /*tb 13927 01/31/94 dww; Keyboard buffering Keyword lookups-ICSW */
        /* readkey pause 0. */
        if {k-cancel.i} then do:
            run err.p(9012).
            {pause.gsc}
            leave cr.
        end.

       /*tb 24767 04/03/98 bm; Customer can display multiple times.
 Only do once*/
        find first w-cust no-lock where
            w-cust.custno = {c-asignd.i "saindex.prod" "1" "12"} no-error.
        if avail w-cust then next cr.

        create w-cust.
        assign
            w-cust.custno = {c-asignd.i "saindex.prod" "1" "12"}
            j = j + 1.

        if j > 200 then do:
            run err.p(9008).
            leave cr.
        end.

        if j / 10 = integer(j / 10) then
            status default string(j,">>9") + " Records Matched ...".
    end.

    status default.

    clear frame f-lookup all. 
    clear frame f-zcustinfo all.
  
    pause 0 before-hide.

    assign
        s-wordtxt = " Matching " + s-words[1] +
                (if s-words[2] ne "" then ", " + s-words[2] else "") +
                (if s-words[3] ne "" then ", " + s-words[3] else "") +
                (if s-words[4] ne "" then ", " + s-words[4] else "") +
                (if s-words[5] ne "" then ", " + s-words[5] else "") + " ".
        s-wordtxt = substring(s-wordtxt, 1, 51).

    pause 0 before-hide.

    main:
    repeat:

        view frame f-lookup.

        put screen row v-length + 5 col 16 color message
            " F6-Lkup Nm F7-Phone F8-KEYWORDS F9-City/St F10-Zip ".

        /*tb 18104 04/11/95 fef; added &alpha to not allow character input
            into v-start */
        {xxxxlu.i &find    = "n-arcxlu.i"
                  &prog    = "arscx"  
                  &file    = "w-cust"
                  &chfield = "s-custno"
                  &alpha   = "no"
                  &display = "s-custno s-notesfl s-name s-city
                              s-statustype"
                  &where   = "w-arsxlu.i decimal(o-frame-value) no-lock"
                  &global  = "if substring(g-ourproc,1,2) = v-bp
                              then frame-value =
                              substring(frame-value,1,12).
                              else frame-value =
                              {c-asignd.i frame-value "1" "12"}"
                  &func    = *
                  &dial    = *
                  &dsfield = "s-name"}.
    end.

    if {k-accept.i} or {k-return.i} then leave.
end.

hide frame f-lookup no-pause.
hide frame f-words no-pause.
hide frame f-zcustinfo no-pause.


