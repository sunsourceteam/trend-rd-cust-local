/* 
   This procedure will go through all the customers to find the contracts that
   do not have any sales for the year
*/

procedure get-0-sales-info:

 for each arsc use-index k-arsc where arsc.cono = g-cono and
                                      arsc.custno     >= b-customer and
                                      arsc.custno     <= e-customer and
                                      arsc.statustype  = yes and
                                      arsc.slsrepout  >= b-salesrep and
                                      arsc.slsrepout  <= e-salesrep 
                                      no-lock:
     if arsc.pricetype < b-cprice-type or
        arsc.pricetype > e-cprice-type then
        next.

   find smsn use-index k-smsn where smsn.cono = 1 and
                                    smsn.slsrep = arsc.slsrepout
                                    no-lock no-error.
   if not avail smsn then next.
   if avail smsn then do:
      if smsn.mgr < b-region or
         smsn.mgr > e-region then
         next.
   end.
   
   for each pdsc use-index k-custprod where pdsc.cono       = g-cono and
                                            pdsc.statustype = yes and
                                            pdsc.levelcd    = 1 and
                                            pdsc.custno     = arsc.custno and
                                            pdsc.startdt   <= p-date and
                                           (pdsc.enddt     >= p-date or
                                            pdsc.enddt      = ?)
                                            no-lock:
      if pdsc.custtype <> "" then do:
         find arss where arss.cono   = g-cono        and
                         arss.custno = pdsc.custno   and
                         arss.shipto = pdsc.custtype 
                         no-lock
                         no-error.
         if not avail arss then next.
         assign w-slsrep    = arss.slsrepout
                w-pricetype = arss.pricetype.
      end.
      else
         assign w-slsrep    = arsc.slsrepout
                w-pricetype = arsc.pricetype.
                
      find first matrix use-index k-product where 
                                  matrix.customer = pdsc.custno and
                                  matrix.product  = pdsc.prod
                                  no-lock
                                  no-error.
      if avail matrix then next.
      else do:
         assign w-ytdsales = 0.
         assign w-lastyr = string(YEAR(p-date)).
         assign wk-lastyr = substring(w-lastyr,3,2).
         assign z-lastyr = int(wk-lastyr).
         assign z-lastyr = z-lastyr - 1.
      
         for each smsc use-index k-smsc where 
                              smsc.cono = g-cono and
                              smsc.yr   = z-lastyr and
                              smsc.custno = pdsc.custno and
                              smsc.shipto = pdsc.custtype
                              no-lock:
            do i = 1 to 12:
               assign w-ytdsales = w-ytdsales + smsc.salesamt[i].
            end.
         end.
         
         assign w-pricetype   = ""
                w-description = "".
         find first icsw use-index k-icsw where 
                                 icsw.cono        = g-cono
                             and icsw.prod        = pdsc.prod
                             /*
                             and icsw.statustype <> "X" 
                             */
                                  no-lock no-error.
         if avail icsw then do:
            
            if icsw.pricetype < b-pprice-type or
               icsw.pricetype > e-pprice-type then
               next.
            
            assign w-pricetype = icsw.pricetype.
            find icsp use-index k-icsp where 
                                    icsp.cono = g-cono and
                                    icsp.prod = icsw.prod
                                    no-lock no-error.
            if avail icsp then
               assign w-description = icsp.descrip[1].
            else
               assign w-description = "".
         end.
         
         assign w-qtybrk = "".
         if pdsc.prctype = no then do:
            if pdsc.prcmult[1] <> 0 then
               assign w-qtybrk = "p".
            else
               if pdsc.prcdisc[1] <> 0 then
                  assign w-qtybrk = "d".
               else
                  assign w-qtybrk = "d".
         end.

         assign w-disctp = "".
         if pdsc.qtybrk[1] > 0 then do:
            assign substring(w-disctp,1,1) = "Q".
            if pdsc.prctype = yes then
               assign substring(w-disctp,2,3) = "Prc".
            if pdsc.prctype = no then do:
               if pdsc.qtybreakty = "D" or w-qtybrk = "d" then
                  assign substring(w-disctp,2,2) = "D%".
               if pdsc.qtybreakty = "P" or w-qtybrk = "p" then
                  assign substring(w-disctp,2,2) = "P%".
            end.
            if pdsc.priceonty = "L" then do:
               if substring(w-disctp,4,1) = "" then
                  assign substring(w-disctp,4,1) = "L".
               else
                  assign substring(w-disctp,5,1) = "L".
            end.
            if pdsc.priceonty = "B" then do:
               if substring(w-disctp,4,1) = "" then
                  assign substring(w-disctp,4,1) = "B".
               else
                  assign substring(w-disctp,5,1) = "B".
            end.
         end.   
         else do:
            if pdsc.prctype = yes then                  
               assign substring(w-disctp,1,5) = "Price".  
            if pdsc.prctype = no then do:               
               if pdsc.qtybreakty = "D" or w-qtybrk = "d" then
                  assign substring(w-disctp,1,2) = "D%".
               if pdsc.qtybreakty = "P" or w-qtybrk = "p" then
                  assign substring(w-disctp,1,2) = "P%".
               if pdsc.priceonty = "L" then
                  assign substring(w-disctp,3,1) = "L".
               if pdsc.priceonty = "B" then
                  assign substring(w-disctp,3,1) = "B".
            end.
         end.
         
         create matrix.
         assign matrix.region        = substring(smsn.mgr,1,1)
             matrix.district      = if p-distbrk = no then
                                       substring(smsn.mgr,2,3)
                                    else
                                       "999"
             matrix.salesrep      = w-slsrep
             matrix.customer      = pdsc.custno
             matrix.ship-to       = pdsc.custtype
             matrix.name          = substring(arsc.name,1,9)
             matrix.ytdsales      = arsc.salesytd
             matrix.last-ytdsales = w-ytdsales
             matrix.product       = pdsc.prod
             matrix.proddesc      = pdsc.prod
             matrix.cprice-type   = arsc.pricetype
             matrix.pprice-type   = w-pricetype
             matrix.pd-recno      = pdsc.pdrecno
             matrix.pd-level      = pdsc.levelcd
             matrix.pd-enddt      = pdsc.enddt
             matrix.pd-trandt     = pdsc.transdt
             matrix.pct-mult      = if pdsc.prcmult[1] <> 0 then
                                       pdsc.prcmult[1]
                                    else
                                       if pdsc.prcdisc[1] <> 0 then
                                          pdsc.prcdisc[1]
                                       else
                                          0
             matrix.type          = if pdsc.prctype = yes then 
                                       "$"
                                    else 
                                       "%"
             matrix.pd-disctp     = w-disctp.
      end.
    end.
    
    for each pdsc use-index k-custprod where pdsc.cono   = g-cono and
                                             pdsc.statustype = yes and
                                             pdsc.levelcd    = 2 and
                                             pdsc.custno     = arsc.custno and
                                             pdsc.startdt   <= p-date and
                                            (pdsc.enddt     >= p-date or
                                             pdsc.enddt      = ?)
                                             no-lock:
      if pdsc.custtype <> "" then do:
         find arss where arss.cono   = g-cono        and
                         arss.custno = pdsc.custno   and
                         arss.shipto = pdsc.custtype 
                         no-lock
                         no-error.
         if not avail arss then next.
         assign w-slsrep    = arss.slsrepout
                w-pricetype = arss.pricetype.
      end.
      else
         assign w-slsrep    = arsc.slsrepout
                w-pricetype = arsc.pricetype.

      if substring(pdsc.prod,1,1) = "p" then do:

      
         if substring(pdsc.prod,3,4) < b-pprice-type or
            substring(pdsc.prod,3,4) > e-pprice-type then
            next.
      
         find sasta where sasta.cono = g-cono and
                          sasta.codeiden = "k" and
                          sasta.codeval  = substring(pdsc.prod,3,4)
                          no-lock no-error.
         if avail sasta then
            assign w-description = sasta.descrip.
         else
            assign w-description = "".
         assign w-product = substring(pdsc.prod,3,4).
         run "check-type-2".
      end.

      if b-pprice-type <> "" then next.

      if substring(pdsc.prod,1,1) = "l" then do:
         find first icsl use-index k-prodline where
                                   icsl.cono     = g-cono and
                                   icsl.whse     = arsc.whse and
                                   icsl.prodline = substring(pdsc.prod,15,6)
                                   no-lock no-error.
         if avail icsl then 
            assign w-description = icsl.descrip.
         else
            assign w-description = "".
         assign w-product = substring(pdsc.prod,15,6).
         run "check-type-2".
      end.

      if substring(pdsc.prod,1,1) = "c" then do:
         find sasta where sasta.cono = g-cono and
                          sasta.codeiden = "c" and
                          sasta.codeval  = substring(pdsc.prod,3,4)
                          no-lock no-error.
         if avail sasta then
            assign w-description = sasta.descrip.
         else
            assign w-description = "".
         assign w-product = substring(pdsc.prod,3,4).
         run "check-type-2".
      end.
    end.  
 end.    
end. /* procedure */

procedure check-type-2:
  find first matrix use-index k-product where 
                              matrix.customer = pdsc.custno and
                              matrix.product  = w-product
                              no-lock
                              no-error.
  if avail matrix then next.
  else do:
     assign w-ytdsales  = 0 
            w-lytdsales = 0.
     assign w-lastyr = string(YEAR(p-date)).
     assign wk-lastyr = substring(w-lastyr,3,2).
     assign z-lastyr = int(wk-lastyr).
     for each smsc use-index k-smsc where 
                          smsc.cono = g-cono and
                          smsc.yr   = z-lastyr and
                          smsc.custno = arsc.custno 
                          no-lock:
        do i = 1 to 12:
           assign w-ytdsales = w-ytdsales + smsc.salesamt[i].
        end.
     end.

     assign z-lastyr = z-lastyr - 1.
      
     for each smsc use-index k-smsc where 
                          smsc.cono = g-cono and
                          smsc.yr   = z-lastyr and
                          smsc.custno = arsc.custno 
                          no-lock:
        do i = 1 to 12:
           assign w-lytdsales = w-lytdsales + smsc.salesamt[i].
        end.
     end.
     
     assign w-qtybrk = "".
     if pdsc.prctype = no then do:
        if pdsc.prcmult[1] <> 0 then
           assign w-qtybrk = "p".
        else
           if pdsc.prcdisc[1] <> 0 then
              assign w-qtybrk = "d".
           else
              assign w-qtybrk = "d".
     end.

     assign w-disctp = "".
     if pdsc.qtybrk[1] > 0 then do:
        assign substring(w-disctp,1,1) = "Q".
        if pdsc.prctype = yes then
           assign substring(w-disctp,2,3) = "Prc".
        if pdsc.prctype = no then do:
           if pdsc.qtybreakty = "D" or w-qtybrk = "d" then
              assign substring(w-disctp,2,2) = "D%".
           if pdsc.qtybreakty = "P" or w-qtybrk = "p" then
              assign substring(w-disctp,2,2) = "P%".
        end.
        if pdsc.priceonty = "L" then do:
           if substring(w-disctp,4,1) = "" then
              assign substring(w-disctp,4,1) = "L".
           else
              assign substring(w-disctp,5,1) = "L".
        end.
        if pdsc.priceonty = "B" then do:
           if substring(w-disctp,4,1) = "" then
              assign substring(w-disctp,4,1) = "B".
           else
              assign substring(w-disctp,5,1) = "B".
        end.
     end.   
     else do:
        if pdsc.prctype = yes then                  
           assign substring(w-disctp,1,5) = "Price".  
        if pdsc.prctype = no then do:               
           if pdsc.qtybreakty = "D" or w-qtybrk = "d" then
              assign substring(w-disctp,1,2) = "D%".
           if pdsc.qtybreakty = "P" or w-qtybrk = "p" then
              assign substring(w-disctp,1,2) = "P%".
           if pdsc.priceonty = "L" then
              assign substring(w-disctp,3,1) = "L".
           if pdsc.priceonty = "B" then
              assign substring(w-disctp,3,1) = "B".
        end.
     end.
         
     create matrix.
     assign matrix.region        = substring(smsn.mgr,1,1)
            matrix.district      = if p-distbrk = no then
                                      substring(smsn.mgr,2,3)
                                   else
                                      "999"
            matrix.salesrep      = w-slsrep
            matrix.customer      = pdsc.custno
            matrix.ship-to       = pdsc.custtype
            matrix.name          = substring(arsc.name,1,9)
            matrix.ytdsales      = w-ytdsales
            matrix.last-ytdsales = w-lytdsales
            matrix.product       = w-product
            matrix.proddesc      = w-description
            matrix.cprice-type   = w-pricetype
            matrix.pprice-type   = w-product
            matrix.pd-recno      = pdsc.pdrecno
            matrix.pd-level      = pdsc.levelcd
            matrix.pd-enddt      = pdsc.enddt
            matrix.pd-trandt     = pdsc.transdt
            matrix.pct-mult      = if pdsc.prcmult[1] <> 0 then
                                      pdsc.prcmult[1]
                                   else
                                      if pdsc.prcdisc[1] <> 0 then
                                         pdsc.prcdisc[1]
                                      else
                                         0
            matrix.type          = if pdsc.prctype = yes then 
                                      "$"
                                   else 
                                      "%"
            matrix.pd-disctp     = w-disctp.
    end.
end.
