/* f-ares5.i 1.9 11/19/93 */
/*h****************************************************************************
  INCLUDE      : f-ares5.i
  DESCRIPTION  : Forms for standard AR statements, format 5
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    07/13/92 pap; TB#  7191 Customer # change.  Use report.c12 instead of 
        report.de12d0 for customer #.
    06/24/93 jrg; TB# 11968 Changed variables in f-bottom to change period 
        balances to contain SVs and Credits when the Special Transaction
        flag is set.
    11/17/93 bj ; TB# 13551 Check summary last 30 days incorrect.
    06/20/97 jkp; TB# 23081 Changed form f-tot to display s-totalbal instead
        of v-totbal.  V-totbal is no longer used.
    01/13/99 wpp; SS903 Changed f-top to NOT be a page-top form.
    10/27/00 - ceg - 00-84 - Stuff to increase x-ponum size
******************************************************************************/

/*tb 7191 07/13/92 pap; Customer # change. Use c12 instead of de12d0 (for
     report. and v-). */
form header /* wpp 01/13/99 No longer a form 'header'. */
    skip (2)
    x-callmsg           at 4
    h-remitto           at 4 
    h-custno            at 66
    h-coname            at 11
    report.c12          at 68
    h-coaddr[1]         at 11
    h-stmtdt            at 66
    h-coaddr[2]         at 11
    p-stmtdt            at 69
    h-cocitystzip       at 11
    h-totdue            at 66
    s-totalbal          at 68
/* wpp 02/16/99; Move the address up 5 lines.    skip (5)    */
    v-addr[1]           at 11
    h-caninv            at 65
    v-addr[2]           at 11
    v-addr[3]           at 11
    v-addr[4]           at 11
    skip (1)  
    h-amtpaid           at 66
    skip (1)
    h-amtpaidln         at 62
    h-remit1            at 55
    h-remit2            at 55
    v-msg               at 1
    skip (1)
    h-stmtdt2           at 1
    h-custno2           at 19
    p-stmtdt2           at 4
    v-c12               at 16
    skip (1)
    h-headingln         at 1
    h-uline             at 1
with frame f-top width 80 no-labels no-box overlay page-top no-underline.
/* wpp 01/13/99 No longer 'page-top'. */  /* das - put page-top back in */
                                          /*       for BillTrust        */
form header /* wpp 01/13/99 No longer a form 'header'. */
    skip (2)
    x-callmsg           at 4
    h-remitto           at 4 
    h-custno            at 66
    h-coname            at 11
    report.c12          at 68
    h-coaddr[1]         at 11
    h-stmtdt            at 66
    h-coaddr[2]         at 11
    p-stmtdt            at 69
    h-cocitystzip       at 11
 /* h-totdue            at 66   Removed for Canadian Statements - replaced
    s-totalbal          at 68   with skip (2) */
    skip (2)
/* wpp 02/16/99; Move the address up 5 lines.    skip (5)    */
    v-addr[1]           at 11
    h-caninv            at 65
    v-addr[2]           at 11
    v-addr[3]           at 11
    v-addr[4]           at 11
    skip (1)  
    h-amtpaid           at 66
    skip (1)
    h-amtpaidln         at 62
    h-remit1            at 55
    h-remit2            at 55
    v-msg               at 1
    skip (1)
    h-stmtdt2           at 1
    h-custno2           at 19
    p-stmtdt2           at 4
    v-c12               at 16
    skip (1)
    h-headingln         at 1
    h-uline             at 1
with frame f-ctop width 80 no-labels no-box overlay page-top no-underline.
/* wpp 01/13/99 No longer 'page-top'. */  /* das - put page-top back in */
                                          /*       for BillTrust        */


/*
/* wpp 01/16/99 Add new page-top frame for subsequent pages. */
form header
    h-headingln   at 1
    h-uline       at 1
with frame f-custtop width 80 no-labels no-box overlay page-top no-underline.
*/

form
    b-report.dt           at 1
/* wpp   b-report.dt-2         at 10    */
    b-report.c2           at 11
/* wpp   b-report.c6           at 24    */
    v-invoice             at 14 format "x(11)"
    x-ponum               at 26
    v-credit              at 45 format "zzzzzz9.99-"
    x-partial             at 57
    v-charge              at 70 format "zzzzzz9.99-"
with frame f-detail width 80 no-labels no-box overlay.

form
    "Last Statement -"    at 1
    v-lastdt           colon 45 label "Date"
    v-lastbal          colon 66 format "zzzzzzzz9.99-" label "Balance"
    skip (1)
with frame f-fwdbal width 80 side-labels no-box.

form
    report.c24          at 10
with frame f-refer width 80 no-labels no-box overlay.

form
    skip (1)
    s-totalbal         colon 66 format "zzzzzzzz9.99-" label "Balance Due"
with frame f-tot width 80 side-labels no-box overlay.

/*tb 13551 11/17/93 bj; Check summary last 30 days incorrect. */
form header
    skip (1)
    "Check Summary Since Last Statement:"      at 1
    "Chk Date"            at 1
    "Check #"             at 31
    "  Amount"            at 68
    "--------"            at 1
    "--------"            at 31
    "--------"            at 68
with frame f-chksumm width 80 no-labels no-box overlay page-top no-underline.

/*tb 11968 06/24/93 jrg; Changed variables in f-bottom to change period
     balances to contain SVs and Credits when the Special Transaction flag 
     is set. */
form header
    v-continued         at 10
    skip (1)
    h-footerln1         at 1
    s-periodbal[1]      at 3
    s-periodbal[2]      at 18
    s-periodbal[3]      at 33
    s-periodbal[4]      at 48
    s-periodbal[5]      at 63
/* wpp 02/09/99
    skip (1)
    h-footerln2         at 1
    arsc.servchgbal     at 3
/* wpp 02/05/99 Remove 'Credits'   report.de9d2s-3     at 18  */
    s-futbal            at 18
    arsc.ordbal         at 33
    arsc.servchgytd     at 48     /* wpp - was 63 */
************************************************/
with frame f-bottom width 80 no-labels no-box overlay page-bottom no-underline.

form
    b-report.c13       colon 11 label "** Ship To"
    v-subtot           colon 66 format "zzzzzzzz9.99-"
                                label "Outstanding Charges"
with frame f-subbot width 80 side-labels no-box overlay.

form
    skip (1)
    b-report.c13       colon 11 label "** Ship To"
with frame f-subtop width 80 side-labels no-box overlay.

/********* original f-bottom frame *********
form header
    v-continued         at 10
    skip (1)
    h-footerln1         at 1
    arsc.periodbal[1]   at 3
    arsc.periodbal[2]   at 18
    arsc.periodbal[3]   at 33
    arsc.periodbal[4]   at 48
    arsc.periodbal[5]   at 63
    skip (1)
    h-footerln2         at 1
    arsc.servchgbal     at 3
    report.de9d2s-3     at 18       /* arsc.misccrbal      at 18 */
    arsc.futinvbal      at 33
    arsc.ordbal         at 48
    arsc.servchgytd     at 63
with frame f-bottom width 80 no-labels no-box overlay page-bottom no-underline.
********************************************/
