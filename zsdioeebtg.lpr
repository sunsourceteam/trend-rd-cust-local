/*****************************************************************************
  INCLUDE      : zsdioeetg.lpr from oeetg.lpr
  DESCRIPTION  : OE Print Parameters - Processing Include
  USED ONCE?   : no
  AUTHOR       : mtt
  DATE WRITTEN : 03/12/98
  CHANGES MADE :
    03/12/98 mtt; TB# 24049 PV: Common code use for GUI interface
    04/15/98 cm;  TB# 22068 SAPB record not always deleted
    03/04/99 jba; PVTB#1730 skip delete SAPB if no SASP and (sapb.backfl=yes),
                            to allow print to file/email in GUI,
    05/05/99 jl;  TB# 17865 Expanded tag updated for faxing
    07/20/99 sbr; TB# 25541 F10 - F10 Fax# defaulting from ARSC
    01/10/00 sbr; TB# 26770 F10 Fax - Invoice Print, Fax # not used
    01/11/00 sbr; TB# 26767 F10 - Fax # not defaulting using ARSE
    01/27/00 mjg; TB#e2785  Demand print not working correctly in GUI.
    11/06/01 lsl; TB# e11093 Moved the ck of sapb.filefl to allow F10
                  demand email to pass through the email where appropriate
                  code if doc format = 3.
     02/04/02 rgm; TB# e11699 Correction to Email Demand RxServer print.
     06/24/02 rgm; TB# e13550 modify to work with F10 prints and custom code
     09/20/02 bp ; TB# e14704 Add user hooks.
     01/25/03 bp ; TB# e16009 Add user hooks.
     02/02/03 bp ; TB# e16009 Add user hooks.
     10/09/03 bp ; TB# e18547 Add additional hooks.
******************************************************************************/

def            var v-adoctypefl      as l                            no-undo.
def            var v-arxserverfl     as l                            no-undo.
def            var v-idoctypefl      as l                            no-undo.
def            var v-irxserverfl     as l                            no-undo.

/*o Print documents for entered order # or auto print the document */
/*o*****************  Pick Ticket Print  *******************************/
if {&pickprintfl} and
    not ({&runtype} = "a" and {&runpick} = "n")
then do:
    /*tb 14928 02/23/94 mwb; added clearing of the id's, moved transaction to
         not to include the actual oeepp.p run (scoping locks) */
    assign
        v-sapbid  = ?
        v-sassrid = ?.

    pickblock:
    /* dkt */
    do /* for oeeh, sapb, b-sapb, icsd, sasp, sassr, sapbo, sasc */ transaction:

        /*d Re-read order header, even though it was read earlier and
            validated */
        {w-oeehb.i {&orderno} {&ordersuf} no-lock}

        /*tb 10245 03/03/93 sdc*/
        {&comdnet}
        {e-dnctrl.i &file = "oeehb"
                    &next = "pause.
                             return"}
        /{&comdnet}* */

        /*d Don't allow pick ticket printing of an unapproved order */
        /*tb 6307/6516 5/05/92 pap; Check if order is on hold before printing */
        if oeehb.approvty ne "y" then do:
            run err{&errorno}.p(5838).
            {&comui}
            next-prompt {&orderno}.
            next.
            /{&comui}* */
            {&errorexit}
        end.

        /*tb 17133 11/18/94 jrb;  Added user include */
        {oeebtg.z99 &user_001 = "*"}

        g-whse = oeehb.whse.

        /*tb 10718 08/12/93 rhl; Allow *whse* in printer device */
        if {&printernmpck} = "*whse*" then do:
            {w-icsd.i g-whse no-lock}
            {&whseprinter} = if avail icsd then icsd.printernm[1] else "".
        end.

        /*d Only allow picking of proper order types */
        if can-do("cr,qu,bl,st,fo,do",oeehb.transtype)
        then
            leave pickblock.

        /*d Find the stored report that contains the defaults for options */
        {w-sapb.i g-cono "'@pck' + g-whse" no-lock b-}

        /*tb#e2785 01/27/00 mjg; Demand print not working correctly in GUI */
        {p-rptnm.i v-reportnm {&report-prefix}}

        /*d Find SASSR Record */
        {w-sassr.i ""oeepp"" no-lock}
        if not avail sassr then do:
            run err{&errorno}.p(4004).
            {&comui}
            {pause.gsc}
            /{&comui}* */
            {&errorexit}
            return.
        end. /*if not avail sassr */

        /*d Create the SAPB record for processing */
        /*tb 10718 08/12/93 rhl; Allow *whse* in printer device */
        /*tb 13720 11/18/93 rs; Pick Ticket Print problem */
        create sapb.

        /*tb 19803 12/19/95 sbl(CGA); Force background printer if SASSR set
                   to print background only. */
        assign
            sapb.backfl      = if sassr.backfl = yes then yes else no
            sapb.currproc    = "oeepp"
            sapb.cono        = g-cono
            sapb.reportnm    = v-reportnm
            sapb.printoptfl  = no
            sapb.priority    = 9
            sapb.inusecd     = "y"
            sapb.printernm   = if {&printernmpck} = "*whse*"
                               then
                                   {&whseprinter}
                               else
                                   {&printernmpck}.

        /*tb 8669 11/13/92 mms */
        /*tb 5157 02/04/93 rhl; Print uses oeaob commit inventory opt */
        /*tb 14261 11/23/94 mms; Added Ship complete packing slip logic */
        if not avail b-sapb then
            assign
                sapb.optvalue[1]  = g-whse
                sapb.optvalue[3]  = ""
                sapb.optvalue[4]  = "0"
                sapb.optvalue[6]  = "no"
                sapb.optvalue[7]  = "O"
                sapb.optvalue[8]  = "no"
                sapb.optvalue[10] = "b".
        /*tb 5467 02/04/93 rhl; Promo msg not printing on @stored Rpts. */
        else do:
            sapb.rpttitle  = b-sapb.rpttitle.
            do i = 1 to 20:
                sapb.optvalue[i] = b-sapb.optvalue[i].
            end.

        end.

        assign  sapb.optvalue[5] = "no"
                sapb.optvalue[2] = "yes".

        {&pcksapbassigns}

        {oeebtg.z99 &user_sapbpick = "*"}

        {t-all.i sapb}

        /*d Check for existence of all necessary records */
        {w-sapb.i g-cono v-reportnm exclusive-lock}
        if not avail sapb then do:
            run err{&errorno}.p(4036).
            {&comui}
            {pause.gsc}
            /{&comui}* */
            {&errorexit}
            return.
        end.

        /*tb 10718 08/12/93 rhl; Allow *whse* in printer device */
        if {&printernmpck} = "*whse*" then
            {w-sasp.i {&whseprinter} no-lock}
        else
            {w-sasp.i {&printernmpck} no-lock}

        /*tb 22068 04/15/98 cm; Delete temporary SAPB record before return */
        /*tb  1730 03/04/99 jba; no SASP necessary if GUI (background) */
        if ( not avail sasp )
        and sapb.backfl<>yes         /* TB 1730 */
        then do:
            {delete.gde sapb}
            run err{&errorno}.p(4035).
            {&comui}
            {pause.gsc}
            /{&comui}* */
            {&errorexit}
            return.
        end.  /* if not avail sasp */

        /*d Create the SAPBO list for the order to be processed */
        create sapbo.
        assign sapbo.orderno   = g-orderno
               sapbo.ordersuf  = {&ordersuf}
               sapbo.reportnm  = v-reportnm
               sapbo.cono      = g-cono
               sapbo.outputty  = if avail sasp      /* TB# 1730 */
                                 and sasp.ptype = "f" then "f" else "p"
               /*
               sapbo.reprintfl = if (oeeh.printpckfl = yes and oeeh.stagecd > 1)
                                 then yes else no
               */
               sapbo.route     = "".

        {t-all.i sapbo}
        assign  v-sapbid  = recid(sapb)
                v-sassrid = recid(sassr).

        /*tb 19803 01/16/96  vlp(CGA); Report Scheduler.  Create RSES record
             and skip pick ticket print if running in background. */
        if sapb.backfl = yes then do:
            assign
                v-sapbid     = ?
                sapb.delfl   = yes
                sapb.startdt = today
                sapb.starttm = time.

            {&comrssched}
            {rssched.gpr}
            /{&comrssched}* */
            {&reportsched}

        end. /* sapb.backfl = yes */

    end. /* do for oeeh transaction */

    /*d Run the pick ticket print */
    /*tb 14928 02/23/94 mwb; added v-sapbid edit */
    {&comoeepp}
    if v-sapbid ne ? then do:
        run oeepp.p.
                /* dkt */
        do /* for sapb, sapbo */ transaction:
            {w-sapb.i g-cono v-reportnm exclusive-lock}
            if avail sapb then delete sapb.

            {w-sapbo.i g-cono v-reportnm g-orderno {&ordersuf}
                    exclusive-lock}
            if avail sapbo then delete sapbo.
        end. /* do for sapb, sapbo transaction */

    end. /* v-sapbid ne ? */
    /{&comoeepp}* */
end.    /* Pick Ticket Print */

status default.
pause 0 before-hide.

if {&runtype} = "p"
then
    return.

/*o*****************  Invoice Print  *******************************/
/*tb 14928 02/23/94 mwb; change scoping to mirror picking changes */
if {&invprintfl} then do:
    assign
        v-sapbid  = ?
        v-sassrid = ?.

    invblock:
    /* dkt */
    do /* for b-sapb, sapb, sasp, sassr, sasc, arsc, arss, sapbo */
    transaction:
        /*tb 15297 04/07/94 mwb; moved oeeh read to here to load the g-whse
             for the @inv on sapb read, also loaded custno and shipto for fax */
        /* dkt do for oeeh: */
            {w-oeehb.i {&orderno} {&ordersuf} no-lock}
            /*tb 21053 04/25/96 tdd; Assign c-empty.i, not 0, on custno */
            assign
                g-whse   = if avail oeehb then oeehb.whse   else g-whse
                {&custno} = if avail oeehb then oeehb.custno else {c-empty.i}
                {&shipto} = if avail oeehb then oeehb.shipto else "".
        /* end.  */

        /*d Find the stored rpt with default options for the invoice print */
        {w-sapb.i g-cono "'@inv' + g-whse" no-lock b-}

        /*tb#e2785 01/27/00 mjg; Demand print not working correctly in GUI */
        /*d Create the SAPB record for processing */
        {p-rptnm.i v-reportnm {&report-prefix}}

        /*d Find SASSR Record */
        {w-sassr.i ""oerd"" no-lock}
        if not avail sassr then do:
            run err{&errorno}.p(4004).
            {&comui}
            {pause.gsc}
            /{&comui}* */
            {&errorexit}
            return.
        end. /* if not avail sassr */

        create sapb.

        /*tb 19803 12/19/95 sbl(CGA); Force background printer if SASSR set
                   to print background only. */
        assign
            sapb.backfl      = if sassr.backfl = yes then yes else no
            sapb.currproc    = "oerd"
            sapb.cono        = g-cono
            sapb.reportnm    = v-reportnm
            sapb.printoptfl  = no
            sapb.priority    = 9
            sapb.printernm   = {&printernminv}
            sapb.rpttitle    = {&promomsg}.

        /* jlc-sapb.optvalue[8] for edi was left at "yes" even though you cannot
           print edi records to the flat file from the direct print.
           sapb.optvalue[9] used to be the same.  Roger has made some changes
           to allow printing to the fax device so sapb.optvalue[9] is 'yes' */
        /*tb 17702 04/18/95 kjb; Add Service Warranty options in OERD.  Changed
           option 12 to option 13 and added new option 12 for SW. */
        if not avail b-sapb then
            assign
               sapb.optvalue[1]  = "o"  /* (O)rder/(Z)ip/(E)ntry/(R)oute order*/
               sapb.optvalue[2]  = "b"  /* (O)nly Billing/(F)loorplan/(B)oth  */
               sapb.optvalue[3]  = "a"  /* Print (Q)uotes only/(A)ll orders   */
               sapb.optvalue[4]  = "yes" /* Enter a list of order#s?          */
               sapb.optvalue[5]  = "no"  /*   if no, backorders incuded?      */
               sapb.optvalue[6]  = "no"  /*   if no, unprinted invoices only? */
               sapb.optvalue[8]  = "yes" /* Print to EDI where appropriate    */
               sapb.optvalue[9]  = "yes" /* Print to Fax where appropriate    */
               sapb.optvalue[10] = "no"  /* Suppress duplicate messages       */
               sapb.optvalue[11] = "no"  /* Honor Customer # of copies        */
               sapb.optvalue[12] = "s"   /* Ser(V)ice, (S)ales Order, (B)oth  */
               sapb.optvalue[13] = "".   /* EDI Flat File Directory           */
        else do:

            /*tb 9863 02/04/93 rhl; Not Honoring Invoice Option 11 (#Copies) */
            do i = 1 to 20:
                sapb.optvalue[i] = b-sapb.optvalue[i].
            end.

            /*tb 15314 02/23/95 mtt; Promotional msg not printing(stored rpt) */
            /*e Only move the @inv stored report rpttitle field if in fact the
                option#7 = "yes", otherwise it will be the default sassr message
                "Order Entry Demand Invoices" which will be meaningless for
                a promotional message.  Also, the screen value will override
                the oerd stored report value. */
            if b-sapb.optvalue[7] = "yes" and {&promomsg} = "" then
                sapb.rpttitle = b-sapb.rpttitle.
        end. /* avail b-sapb */

        /*tb 2812 6/15/92 mkb; print only unprinted invoices */
        /*tb 5467 02/04/93 rhl; Promo msg not printing on @stored Rpts. */
        /*tb 15314 02/23/95 mtt; Promotional msg not printing(stored rpt) */
        assign
            sapb.optvalue[4] = "yes"  /* Enter a list of order#s */
            sapb.optvalue[6] = "no"   /*  If no, unprinted invoices only */
            sapb.optvalue[7] = /*d If the @inv stored report exists and
                                   option#7 = yes, then the promo msg will
                                   come from this stored report (above) and
                                   need to set the option#7 to be yes. */
                               if avail b-sapb and b-sapb.optvalue[7]
                                   = "yes" and {&promomsg} = ""
                                      then "yes"
                               else
                               /*d If there is not a @inv stored report or
                                   there is one, but the option#7 = no,
                                   then check to see if the screen promo
                                   msg is not blank.  If so, set option#7

                                   to be yes. Otherwise, set option#7 to no. */
                               if {&promomsg} ne "" then "yes"
                               else "no".

        {&invsapbassigns}

        /* tb e11093 11/09/01 lsl; begin - added a check for format 3 docs
        (RxServer) and the email where appropriate doc type.  This sets the
        sapb.filefl = no, so that email where appropriate output is used
        instead of the email-report code in rptctl.p */
        /* TB e11699 02/04/01 rgm; We should not require "email where
        appropriate" flag when doing a demand print */
        if {&printernminv} = 'E-MAIL':u then
        do:
            {w-arsc.i {&custno} no-lock}

            if not avail arsc then do:
                run err{&errorno}.p(4303).
                {&comui}
                {pause.gsc}
                /{&comui}* */
                {&errorexit}
                return.
            end.

            v-idoctypefl = yes.

            /* set the format type */
            run rxformck.p (input "inv":u,
                            output v-irxserverfl).
            assign
               sapb.filefl   = if v-idoctypefl and v-irxserverfl then no
                               else yes.
        end.

        {oeebtg.z99 &aft_invasgn = "*"}

        {t-all.i sapb}

        {oeebtg.z99 &user_aft_invsapb = "*"}

        {w-sasp.i {&printernminv} no-lock}

        {oeebtg.z99 &aft_invsasp = "*"}

        /*tb 22068 04/15/98 cm; Delete temporary SAPB record before return */
        /*tb  1730 03/04/99 jba; no SASP necessary if GUI (background) */
        if ( not avail sasp )
        and sapb.backfl<>yes         /* TB 1730 */
        then do:
            {delete.gde sapb}
            run err{&errorno}.p(4035).
            {&comui}
            {pause.gsc}
            /{&comui}* */
            {&errorexit}
            return.
        end. /* not avail sasp */

        /*tb 13497 10/27/93 rhl; F10 print to fax: Pipe to Subprocess */
        /*  Add long distance code  */
        if avail sasp and sasp.ptype = "f" then do:
            if v-faxphoneno = "" then do:
                {w-sasc.i no-lock}

                /*tb 15297 04/07/94 mwb; moved OEEH find to top of invoice prt
                    edit, also made oeeh custno and shipto v- variables */
                {w-arsc.i {&custno} no-lock}
                if not avail arsc then leave invblock.

                if {&shipto} ne " " then
                    {w-arss.i {&custno} {&shipto} no-lock}

                /*d Popup Fax Information screen automatically if the Fax
                    phone# in arsc/arss is blank */
                if {&shipto} ne " " and avail arss and arss.faxphoneno = "" or
                   {&shipto} =  " " and avail arsc and arsc.faxphoneno = ""
                then do:

                    /* tb 20968 02/03/98 sbr; check arsso "To:" field when
                       faxing */
                    /*tb 26767 01/11/00 sbr; Use "(arss.invtofl or
                        (arss.einvtype ne "" and arss.einvto = false))"
                        instead of "arss.invtofl" within the if condition. */
                    if {&shipto} ne " " and avail arss and (arss.invtofl or
                        (arss.einvtype ne "" and arss.einvto = false))
                    then do:
                        {p-fxardf.i &file   = "arss."
                                    &extent = 3}
                    end.

                    else do:
                        {p-fxardf.i &file   = "arsc."
                                    &extent = 3}
                    end.

                    /*u User Hook just prior to p-fxparm.i */
                    {oeebtg.z99 &user_002 = "*"}

                    /*tb 16974 11/08/94 rs; Need > 2 comment lines */
                    /*tb 17288 06/16/95 dww; Unix Error in On-Line Doc. */

                    {&comfax}
                    /*
                    {p-fxparm.i &proc = "oeetg"
                                &pre  = "v-"}
                    */
                    if {k-endkey.i} then do:
                        delete sapb.
                        g-errfl = no.
                        run warning.p(6339).
                        return.
                    end.
                    /{&comfax}* */

                    {&invfaxprocessing}

                end. /* if {&shipto} ne " " ... */

            end. /* if v-faxphoneno = "" */

            /*tb 17865 05/05/99 jl; Added &faxtag1 and &faxtag2 */
            {p-fxasgn.i &option  = 9
                        &faxtag1 = "sapb.faxto1"
                        &faxtag2 = "'OE: ' + string({&orderno}) + '-' +
                                      string({&ordersuf},'99')"}

        end. /* if avail sasp */

        /*d Ensure existance of the necessary records */
        {w-sapb.i g-cono v-reportnm exclusive-lock}
        if not avail sapb then do:
            run err{&errorno}.p(4036).
            {&comui}
            {pause.gsc}
            /{&comui}* */
            {&errorexit}
            return.
        end. /* not avail sapb */

        /*d Create the SAPBO record for the order to process */
        create sapbo.

        /*tb 26770 01/10/00 sbr; Removed " and sapb.faxphoneno = "" " from the
            if clause within the assign statement for sapbo.outputty. */
        assign sapbo.orderno   = g-orderno 
               sapbo.ordersuf  = {&ordersuf}
               sapbo.reportnm  = v-reportnm
               sapbo.cono      = g-cono
               sapbo.reprintfl = no
               sapbo.route     = ""
               sapbo.outputty  = if avail sasp      /* TB#1730 */
                                 and sasp.ptype = "f"
                                 then "f" else "p".

        {oeebtg.z99 &aft_invoasgn = "*"}

        {t-all.i sapbo}

        assign  v-sapbid  = recid(sapb)
                v-sassrid = recid(sassr).

        /*tb 19803 01/16/96  vlp(CGA); Report Scheduler.  Create RSES record
             and skip run if running in background. */
        if sapb.backfl = yes then do:
            assign
                v-sapbid     = ?
                sapb.delfl   = yes
                sapb.startdt = today
                sapb.starttm = time.

            {&comrssched}
            {rssched.gpr}
            /{&comrssched}* */
            {&reportsched}

        end. /* sapb.backfl = yes */

    end. /* invblock: */

    /*d Print the invoice */
    /*tb 14928 02/23/94 mwb; added v-sapbid edit and scoping */
    {&comoerd}
    if v-sapbid ne ? then do:
        run oerd.p.

        do /* for sapb, sapbo */ transaction:

            /*tb 14928 02/23/94 mwb; OEEH find not needed as exclusive */
            {w-sapb.i g-cono v-reportnm exclusive-lock}
            if avail sapb then delete sapb.

            {w-sapbo.i g-cono v-reportnm g-orderno {&ordersuf}
                exclusive-lock}
            if avail sapbo then delete sapbo.
        end. /* do for sapb, sapbo transaction */

    end. /* v-sapbid ne ? */
    /{&comoerd}* */

end. /* Invoice Print */

/*o*****************  Acknowledgement Print  *******************************/
if {&ackprintfl} then do:
    assign
        v-sapbid  = ?
        v-sassrid = ?.
  
    def var okMessage as character format "x(20)" 
            initial "Creating Acknowledgement".
    put screen row 21 col 8 color message okMessage.
    ackblock:
    do /* for b-sapb, sapb, sasp, sassr, sasc, arsc, arss, sapbo */
    transaction:
        /*tb 15298 04/07/94 mwb; moved oeeh read to top to set the g-whse
             corectly for the @ack sapb read */
        /* do for oeeh: */
            {w-oeehb.i {&orderno} {&ordersuf} no-lock}
            /*tb 21053 04/25/96 tdd; Assign c-empty.i, not 0, on custno */
            
            assign
                g-whse   = if avail oeehb then oeehb.whse   else g-whse
                {&custno} = if avail oeehb then oeehb.custno else {c-empty.i}
                {&shipto} = if avail oeehb then oeehb.shipto else "".
        /* end. */

        /*d Find the stored report that contains the default options for the
            acknowledgement print */
        find b-sapb use-index k-sapb where
            b-sapb.cono     = g-cono and
            b-sapb.reportnm = ("@ack" + g-whse)
        no-lock no-error.

        /*tb#e2785 01/27/00 mjg; Demand print not working correctly in GUI */
        /*d Create the SAPB record for processing */
        {p-rptnm.i v-reportnm {&report-prefix}}

        /*d Find SASSR Record */
        {w-sassr.i ""oeepa"" no-lock}
        if not avail sassr then do:
            run err{&errorno}.p(4004).
            {&comui}
            {pause.gsc}
            /{&comui}* */
            {&errorexit}
            return.
        end. /* if not avail sassr */

        okMessage = okMessage + ".".
        put screen row 21 col 8 color message okMessage.
        create sapb.

        /*tb 19803 12/19/95 sbl(CGA); Force background printer if SASSR set
                   to print background only. */
        assign
            sapb.backfl      = if sassr.backfl = yes then yes else no
            sapb.currproc    = "oeepa"
            sapb.cono        = g-cono
            sapb.reportnm    = v-reportnm
            sapb.printoptfl  = no
            sapb.priority    = 9
            sapb.printernm   = {&printernmack}
            sapb.rpttitle    = {&promomsg}.
        if not avail b-sapb
        then
            assign
               sapb.optvalue[1] = "0"
               sapb.optvalue[2] = "o"
               sapb.optvalue[4] = "yes"
               sapb.optvalue[5] = if {&promomsg} = "" then "no" else "yes"
               sapb.optvalue[6] = "yes"
               sapb.optvalue[7] = "yes".
        else do:
            do i = 1 to 20:
                sapb.optvalue[i] = b-sapb.optvalue[i].
            end.

            sapb.rpttitle = b-sapb.rpttitle.
        end. /* else do */

        sapb.optvalue[3] = "yes".

        okMessage = okMessage + ".".
        put screen row 21 col 8 color message okMessage.

        {&acksapbassigns}

        /* tb e11093 11/09/01 lsl; begin - added a check for format 3 docs
        (RxServer) and the email where appropriate doc type.  This sets the
        sapb.filefl = no, so that email where appropriate output is used
        instead of the email-report code in rptctl.p */
        /* TB 311699 02/04/01 rgm; We should not require the "e-mail where
        appropriate" flag be set on a demand print */
        if {&printernmack} = 'E-MAIL':u then
        do:
            {w-arsc.i {&custno} no-lock}

            if not avail arsc then do:
                run err{&errorno}.p(4303).
                {&comui}
                {pause.gsc}
                /{&comui}* */
                {&errorexit}
                return.
            end.

            v-adoctypefl = yes.

            /* set the format type */
            run rxformck.p (input "ack":u,
                            output v-arxserverfl).
            assign
               sapb.filefl   = if v-adoctypefl and v-arxserverfl then no
                               else yes.
        end.

        {oeebtg.z99 &aft_ackasgn = "*"}

        {t-all.i sapb}

        {w-sasp.i {&printernmack} no-lock}

        {oeebtg.z99 &aft_acksasp = "*"}

        /*tb 22068 04/15/98 cm; Delete temporary SAPB record before return */
        /*tb  1730 03/04/99 jba; no SASP necessary if GUI (background) */
        if ( not avail sasp )
        and sapb.backfl<>yes         /* TB 1730 */
        then do:
            {delete.gde sapb}
            run err{&errorno}.p(4035).
            {&comui}
            {pause.gsc}
            /{&comui}* */
            {&errorexit}
            return.
        end. /* not avail sasp */

        /*tb 13497 10/27/93 rhl; F10 print to fax: Pipe to Subprocess */
        /*  Add long distance code  */
        if avail sasp and sasp.ptype = "f" then do:
            if v-faxphoneno = "" then do:
                {w-sasc.i no-lock}

                /*tb 15298 04/07/94 mwb; moved OEEH find to top for g-whse and
                    loaded oeeh custno, shipto as v-'s */
                {w-arsc.i {&custno} no-lock}
                if not avail arsc then leave ackblock.

                if {&shipto} ne " " then
                    {w-arss.i {&custno} {&shipto} no-lock}

                if ({&shipto} ne " " and avail arss and arss.faxphoneno = "") or
                   ({&shipto} =  " " and avail arsc and arsc.faxphoneno = "")
                then do:

                    /* tb 20968 02/03/98 sbr; check arsso "To:" field when
                       faxing */
                    /*tb 25541 07/20/99 sbr; Removed "and arss.invtofl" from
                        if condition when faxing acknowledgement. */
                    /*tb 26767 01/11/00 sbr; Added "and arss.eackto = false"
                        to the if condition. */
                    if {&shipto} ne " " and avail arss and arss.eackto = false
                    then do:
                        {p-fxardf.i &file   = "arss."
                                    &extent = 2}
                    end. /* {&shipto} ne "" */

                    else do:
                        {p-fxardf.i &file   = "arsc."
                                    &extent = 2}
                    end. /* else */

                    /*u User Hook just prior to p-fxparm.i */
                    {oeebtg.z99 &user_002 = "*"}

                    /*tb 16974 11/08/94 rs; Need > 2 comment lines */
                    /*tb 17288 06/16/95 dww; Unix Error in On-Line Doc. */
                    {&comfax}
                    {p-fxparm.i &proc = "oeetg"
                                &pre  = "v-"}
                    if {k-endkey.i} then do:
                        delete sapb.
                        g-errfl = no.
                        run warning.p(6339).
                        return.
                    end. /* k-endkey.i */
                    /{&comfax}* */

                    {&ackfaxprocessing}

                end. /* {&shipto} ne "" */

            end. /* v-faxphoneno = "" */

            /*tb 19314 09/15/95 dww; Remove Imbedded Space from Fax Phone
                Number */
            /*tb 17865 05/05/99 jl; Added &faxtag1 and &faxtag2 */
            {p-fxasgn.i &option = 7
                        &faxtag1 = "sapb.faxto1"
                        &faxtag2 = "'OE: ' + string(g-orderno) + '-' +

                                      string(g-ordersuf,'99')"
                        &comvar = "/*"}

        end. /* avail sasp */

        /*d Ensure the existance of the necessary records */
        {w-sapb.i g-cono v-reportnm exclusive-lock}
        if not avail sapb then do:
            run err{&errorno}.p(4036).
            {&comui}
            {pause.gsc}
            /{&comui}* */
            {&errorexit}
            return.
        end. /* not avail sapb */

        okMessage = okMessage + ".".
        put screen row 21 col 8 color message okMessage.
        /*d Create the SAPBO record for the order to print */
        create sapbo.

        /*tb 26770 01/10/00 sbr; Removed " and sapb.faxphoneno = "" " from the
            if clause within the assign statement for sapbo.outputty. */
        assign sapbo.orderno   = g-orderno
               sapbo.ordersuf  = {&ordersuf}
               sapbo.reportnm  = v-reportnm
               sapbo.cono      = g-cono
               sapbo.reprintfl = no
               sapbo.route     = ""
               sapbo.outputty  = if available sasp      /* TB#1730 */
                                 and sasp.ptype = "f"
                                 then "f" else "p".

        {oeebtg.z99 &aft_ackoasgn = "*"}

        {t-all.i sapbo}

        assign  v-sapbid  = recid(sapb)
                v-sassrid = recid(sassr).

        /*tb 19803 01/16/96  vlp(CGA); Report Scheduler. Create RSES record
             and skip run if running in background */
        if sapb.backfl = yes then do:
            assign
                v-sapbid     = ?
                sapb.delfl   = yes
                sapb.startdt = today
                sapb.starttm = time.

            {&comrssched}
            {rssched.gpr}
            /{&comrssched}* */
            {&reportsched}

        end. /* sapb.backfl = yes */

    end. /* ackblock */

    okMessage = okMessage + ".".
    put screen row 21 col 8 color message okMessage.

    /*d Print the acknowledgement */
    {&comoeepa}
    if v-sapbid ne ? then do:
        run oeepa.p.

        do /* for sapb, sapbo */ transaction:
            {w-sapb.i g-cono v-reportnm exclusive-lock}
            if avail sapb then delete sapb.

            {w-sapbo.i g-cono v-reportnm g-orderno {&ordersuf}
                exclusive-lock}
            if avail sapbo then delete sapbo.
        end. /* do for sapb, sapbo transaction */

    end. /* v-sapbid ne ? */
    /{&comoeepa}* */

   put screen row 21 col 8 "                                         ".

end. /* Acknowledgement Print */

/*o*****************  Receipt Print  *******************************/
if {&rcptprintfl} and {&runtype} = "d" then do /* for oeeh */ :

    {w-oeehb.i {&orderno} {&ordersuf} no-lock}

    if avail oeehb then do:
        /*tb 10457 03/24/93 kmw; Add BR and DO */
        if not can-do("cr,cs,so,rm,ra,br,do",oeehb.transtype) then leave.
        v-idoeehb = recid(oeehb).

        run value("oeetrp" + string(v-oercptfrmt,"9") + ".p")(no).
    end.

end.    /* Receipt Print */

{oeebtg.z99 &Bottom = "*"}


