&if defined(user_temptable) = 2 &then
/*   Variables from screen */

def  var rEnterdt         as date extent 2                 no-undo.
def  var b-region         as c    format "x(1)"            no-undo.
def  var e-region         as c    format "x(1)"            no-undo.
def  var b-district       as c    format "x(3)"            no-undo.
def  var e-district       as c    format "x(3)"            no-undo.
def  var b-takenby        as c    format "x(4)"            no-undo.
def  var e-takenby        as c    format "x(4)"            no-undo.
def  var b-slsrep         as c    format "x(4)"            no-undo.
def  var e-slsrep         as c    format "x(4)"            no-undo.
def  var b-custno         as dec                           no-undo.
def  var e-custno         as dec                           no-undo.
def  var p-format         as integer                       no-undo.
def  var p-type        as c    format "x(1)"            no-undo.
def var p-regval    as c                             no-undo.
def var s-regdist   as c    format "x(4)"            no-undo.
def var s-name      as c    format "x(20)"           no-undo.
def var s-slsrep    as c    format "x(4)"            no-undo.
def var s-takenby   as c    format "x(25)"           no-undo.
def var s-order     as c    format "x(11)"           no-undo.
def var s-transtype as c    format "x(2)"            no-undo.
def var s-pline     as c    format "x(6)"            no-undo.
def var s-prod      as c    format "x(24)"           no-undo.
def var s-proddesc  as c    format "x(24)"           no-undo.
def var s-qtyord    as i    format ">>>>>>>>9"       no-undo.
def var w-name      as c    format "x(20)"           no-undo.
def var w-custno    like    arsc.custno              no-undo.
def var w-shipto    like    arss.shipto              no-undo.
def var com-cnt     as i    format "99"              no-undo.
def var s-space     as c    format "x(1)"            no-undo.
def var x-name      as c    format "x(20)"           no-undo.
def var s-custno    like    arsc.custno              no-undo.
def var s-shipto    like    arss.shipto              no-undo.
def temp-table t-zsdioeellog no-undo
  field trecid as recid.
  

def temp-table expdt no-undo
  FIELD regdist     as c    format "x(4)"
  FIELD custno      like    arsc.custno     /**/
  FIELD shipto      like    arss.shipto     /**/
  FIELD name        as c    format "x(20)"
  FIELD slsrep      as c    format "x(4)"
  FIELD takenby     as c    format "x(4)"
  FIELD orderno     like    oeeh.orderno
  FIELD ordersuf    like    oeeh.ordersuf
  FIELD ordernoL    as c    format "x(11)"
  FIELD lineno      like    oeel.lineno
  FIELD transtype   as c    format "x(2)"
  FIELD prod        as c    format "x(24)"
  FIELD proddesc    as c    format "x(24)"
  FIELD POexpdt     as date
  FIELD OEexpdt     as date
  FIELD expDTcode   as char format "x(1)"
  FIELD transdt     as date
  FIELD cnt         as int
INDEX
  orderno
  ordersuf
  lineno.         
 
 
 
 
 
&endif
 
&if defined(user_drpttable) = 2 &then
/* Use to add fields to the report table */
    FIELD ovrrecid        AS recid   
 
 
 
 
 
&endif
 
&if defined(user_forms) = 2 &then
form
  "Customer Nbr"                   at 1
  "Customer Name"                  at 15
  "TakenBy"                        at 48
  "OrderNo"                        at 62
  "Lineno"                         at 75
  "Product"                        at 84
  "Description"                    at 110
  "Po ExpDt"                       at 139
  "Oe ExpDt"                       at 150
  "TransDt"                        at 160
  "------------"                   at 1
  "-----------------------------"  at 15
  "-------"                        at 48
  "-----------"                    at 60
  "------"                         at 75
  "------------------------"       at 84
  "------------------------"       at 109
  "--------"                       at 138
  "--------"                       at 149
  "--------"                       at 160
  
  with frame f-header no-underline no-box no-labels page-top down width 178.
form
  expdt.custno                   at 1
  expdt.name                     at 15
  expdt.TakenBy                  at 50
  expdt.OrderNoL                 at 60
  expdt.Lineno                   at 75
  expdt.Prod                     at 84
  expdt.Proddesc                 at 109
  expdt.PoExpDt                  at 138
  expdt.OeExpDt                  at 149
  expdt.ExpDtcode                at 157
  expdt.transdt                  at 160
  
 with frame f-detail no-underline no-box no-labels down width 178.
 
form
  s-space             at 1
 with frame f-space no-underline no-box no-labels down width 178.
 
 
 
 
&endif
 
&if defined(user_extractrun) = 2 &then

if sapb.rangebeg[5] ne "" then do: 
 v-datein = sapb.rangebeg[5].
 {p-rptdt.i}
 if string(v-dateout) = v-lowdt then
  rEnterDt[1] = 01/01/1900.
 else  
  rEnterDt[1] = v-dateout.
end.
else
  rEnterDt[1] = 01/01/1900.

if sapb.rangeend[5] ne "" then do: 
 v-datein = sapb.rangeend[5].
 {p-rptdt.i}
 if string(v-dateout) = v-lowdt then
  rEnterDt[2] = 01/01/2049.
 else  
  rEnterDt[2] = v-dateout.
end.
else
  rEnterDt[2] = 01/01/2049.

                    
assign b-custno   = dec(sapb.rangebeg[1])
       e-custno   = dec(sapb.rangeend[1])
       b-region   = substr(sapb.rangebeg[4],1,1)
       e-region   = substr(sapb.rangeend[4],1,1)
       b-district = substr(sapb.rangebeg[4],2,3)
       e-district = substr(sapb.rangeend[4],2,3)
       b-takenby  = sapb.rangebeg[2]
       e-takenby  = sapb.rangeend[2]
       b-slsrep   = sapb.rangebeg[3]
       e-slsrep   = sapb.rangeend[3]
       
       p-format   = int(sapb.optvalue[1])
       p-type  = sapb.optvalue[2]
       p-export = sapb.optvalue[3]. 
assign p-detail   = "D".
run formatoptions.

for each zsdioeellog 
         fields (orderno
                 ordersuf
                 lineno
                 cono
                 user1
                 user10
                 user11
                 transdt)
                 where 
         zsdioeellog.cono = g-cono and
         (zsdioeellog.user1 = "ackn"  or
           date (zsdioeellog.user10) <>
           date (zsdioeellog.user11)) and
           zsdioeellog.transdt >=   rEnterDt[1] and 
           zsdioeellog.transdt <= rEnterDt[2]  
           no-lock:
    create t-zsdioeellog.
    assign t-zsdioeellog.trecid = recid(zsdioeellog).
end.

for each t-zsdioeellog  no-lock,
  first zsdioeellog  where
        recid(zsdioeellog) = t-zsdioeellog.trecid  no-lock,
  first oeel where 
       oeel.cono     = g-cono and
       oeel.orderno  = zsdioeellog.orderno and
       oeel.ordersuf = zsdioeellog.ordersuf and
       oeel.lineno   = zsdioeellog.lineno and
       oeel.custno >= b-custno and
       oeel.custno <= e-custno and
       oeel.slsrepout >= b-slsrep and
       oeel.slsrepout <= e-slsrep no-lock
 
       break by oeel.orderno
             by oeel.ordersuf
             by oeel.lineno
             by zsdioeellog.transdt
             by zsdioeellog.transtm:
 
  if not avail oeel then next.     
  if not last-of(oeel.lineno) then
    next.
   
  if  oeel.statustype <> "C" and
      oeel.specnstype <> "L" and
      oeel.transtype <> "RM" then do:
    find oeeh where oeeh.cono     = oeel.cono and
                    oeeh.orderno  = oeel.orderno and
                    oeeh.ordersuf = oeel.ordersuf
                    no-lock no-error.
    if not avail oeeh then next.
    if oeeh.shipto = "" then do:
       find arsc use-index k-arsc where arsc.cono = g-cono and
                                        arsc.custno = oeeh.custno                                                        no-lock no-error.
       if not avail arsc then next.
       assign w-name      = substring(arsc.name,1,20)
              w-custno    = arsc.custno
              w-shipto    = "".
    end.
    else do:
       find arss use-index k-arss where arss.cono = g-cono and
                                        arss.custno = oeeh.custno and
                                        arss.shipto = oeeh.shipto 
                                        no-lock no-error.
       if not avail arss then next.
       assign w-name      = substring(arss.name,1,20)
              w-custno    = arss.custno
              w-shipto    = arss.shipto.
       find arsc use-index k-arsc where arsc.cono = g-cono and
                                        arsc.custno = arss.custno
                                        no-lock
                                        no-error.
       if not avail arsc then next.
    end.
    find icsp use-index k-icsp where icsp.cono = g-cono and
                                     icsp.prod = oeel.shipprod 
                                     no-lock no-error.
         
    find smsn use-index k-smsn where smsn.cono      = g-cono and
                                     smsn.slsrep    = oeel.slsrepout and
                              substr(smsn.mgr,1,1) >= b-region       and
                              substr(smsn.mgr,1,1) <= e-region       and
                              substr(smsn.mgr,2,3) >= b-district     and
                              substr(smsn.mgr,2,3) <= e-district
                                     no-lock no-error.
    if not avail smsn then next.
    find first poelo  where
               poelo.cono        = oeel.cono       and
               poelo.ordertype   = "o"             and
               poelo.orderaltno  = oeel.orderno    and
               poelo.orderaltsuf = oeel.ordersuf   and
               poelo.linealtno   = oeel.lineno no-lock no-error.

     
    if avail poelo then
      find last poel where 
                poel.cono = poelo.cono and
                poel.pono   = poelo.pono and 
  /*            poel.posuf  = poelo.posuf and         */
                poel.lineno    = poelo.lineno  no-lock no-error.
 
    create expdt.
    assign expdt.regdist   = CAPS(smsn.mgr)
           expdt.custno    = w-custno
           expdt.shipto    = w-shipto
           expdt.name      = w-name
           expdt.slsrep    = smsn.slsrep
           expdt.takenby   = CAPS(oeeh.takenby) + " " + CAPS(x-name)
           expdt.ordernol  = string(oeeh.orderno,">9999999") + "-" + 
                           string(oeeh.ordersuf,"99")
           expdt.orderno   =  oeeh.orderno
           expdt.ordersuf  = oeeh.ordersuf
           expdt.lineno    = oeel.lineno
           expdt.transtype = oeeh.transtype
           expdt.POexpdt    = if avail poelo and avail poel then
                                poel.expshipdt
                              else
                                ?
           expdt.OEexpdt    = date(zsdioeellog.user11)
 
           expdt.cnt       = 1
           expdt.prod      = replace(oeel.shipprod,"""","'")
           expdt.proddesc  = if avail icsp then
                               icsp.descrip[1]
                             else
                               oeel.proddesc
           expdt.expDTcode = if zsdioeellog.user17 <> "" then
                               zsdioeellog.user17
                             else
                             if zsdioeellog.user1 = "ACKN" then
                               "P"
                             else 
                               "O"
           expdt.transdt   = zsdioeellog.transdt.
    assign expdt.prod      = CAPS(expdt.prod).
 
  
  end. /* each oeeh */
end. /* each oeel */
 
 
 
&endif
 
&if defined(user_exportstatDWheaders) = 2 &then
  assign export_rec = "".
  assign export_rec = "RegDist"      + v-del +
                      "CustNo"        + v-del +
                      "CustName"      + v-del +
                      "AM"            + v-del +
                      "TknBy"         + v-del +
                      "OrderNo"       + v-del +
                      "Lineno"        + v-del +
                      "Product"       + v-del +
                      "ProdDescript"  + v-del +
                      "PoExpDt"       + v-del +
                      "OeExpDt"       + v-del +
                      "OeExpDtCode"       + v-del +
                      "Transdt".
  
 
 
 
 
 
&endif
 
&if defined(user_exportstatheaders) = 2 &then
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  assign export_rec = " ".

  
  assign export_rec = "RegDist"      + v-del +
                      "CustNo"        + v-del +
                      "CustName"      + v-del +
                      "AM"            + v-del +
                      "TknBy"         + v-del +
                      "OrderNo"       + v-del +
                      "Lineno"        + v-del +
                      "Product"       + v-del +
                      "ProdDescript"  + v-del +
                      "PoExpDt"       + v-del +
                      "OeExpDt"       + v-del +
                      "OeExpDtCode"       + v-del +
 
                      "Transdt".
  
 
 
 
 
&endif
 
&if defined(user_foreach) = 2 &then
for each expdt no-lock:
   if p-type = "P" and expdt.expDTcode  <> "P" then next.
   if p-type = "O" and expdt.expDTcode  <> "O" then next.
 
 
&endif
 
&if defined(user_drptassign) = 2 &then
 /* If you add fields to the dprt table this is where you assign them */
   drpt.ovrrecid = recid(expdt) 
 
 
 
 
&endif
 
&if defined(user_B4endloop) = 2 &then
 
 
 
 
 
&endif
 
&if defined(user_viewheadframes) = 2 &then
/* Since frames are unique to each program you need to put your
   views and hides here with your names */
if not p-exportl then do:
  if not x-stream then do:
    hide frame f-header.
  end.
  else do:
    hide stream xpcd frame f-header.
  end.
end.
if not p-exportl then do:
  if not x-stream then do:
    page.
    view frame f-header.
  end.
  else do:
    page stream xpcd.
    view stream xpcd frame f-header.
  end.
end.
  
 
 
 
 
 
&endif
 
&if defined(user_drptloop) = 2 &then
 
 
 
 
 
&endif
 
&if defined(user_detailprint) = 2 &then
   /* Comment this out if you are not using detail 'X' option  */
  find expdt where recid(expdt) = drpt.ovrrecid no-lock no-error.
  assign export_rec = "".
  if p-exportDWl then do:

    assign export_rec =
       expDT.regdist              + v-del +
 
       string(expdt.custno)       + v-del + 
       expdt.name                 + v-del +
       expdt.slsrep               + v-del +
       expdt.TakenBy              + v-del +
       expdt.OrderNoL             + v-del +
       string(expdt.Lineno)       + v-del +
       expdt.Prod                 + v-del +
       expdt.Proddesc             + v-del +
       if expdt.Poexpdt = ? then " "
       else string(expdt.PoExpDt,"99/99/9999")      + v-del +
       if expdt.oeexpdt = ? then " " else
       string(expdt.OeExpDt,"99/99/9999")      + v-del +
     
       expdt.ExpDtcode    + v-del +
       if expdt.transdt = ? then  " " else
       string(expdt.transdt,"99/99/9999").
/* 
      assign export_rec = substr(ovr.regdist,1,1)           + v-del +
                          substr(ovr.regdist,2,3)           + v-del +
                          string(ovr.custno,"999999999999") + v-del +
                          ovr.shipto                        + v-del +
                          ovr.name                          + v-del +
                          ovr.slsrep                        + v-del +
                          substr(ovr.takenby,1,4)           + v-del +
                          substr(ovr.takenby,6,20)          + v-del +
                          ovr.order                         + v-del +
                          ovr.transtype                     + v-del +
                          ovr.pline                         + v-del +
                          ovr.prod                          + v-del +
                          ovr.proddesc                      + v-del +
                          string(ovr.qtyord,">>>>>>>9")     + v-del +
                          string(ovr.stndcost,"->>>>>9.99") + v-del +
                          string(ovr.overcost,"->>>>>9.99") + v-del +
                          string(ovr.enterdt,"99/99/99").
*/          
      assign export_rec = export_rec + chr(13) + chr(10).
      put stream expt unformatted export_rec.
      assign export_rec = "".
   end.
   else 
   if p-exportl then do:

    assign export_rec =
       expDT.regdist              + v-del +
       string(expdt.custno)       + v-del + 
       expdt.name                 + v-del +
       expdt.slsrep               + v-del +
       expdt.TakenBy              + v-del +
       expdt.OrderNoL             + v-del +
       string(expdt.Lineno)       + v-del +
       expdt.Prod                 + v-del +
       expdt.Proddesc             + v-del +
       if expdt.Poexpdt = ? then " "
       else string(expdt.PoExpDt,"99/99/9999")      + v-del +
       if expdt.oeexpdt = ? then " " else
       string(expdt.OeExpDt,"99/99/9999")      + v-del +
     
       expdt.ExpDtcode    + v-del +
       if expdt.transdt = ? then  " " else
       string(expdt.transdt,"99/99/9999").


/*
         assign export_rec = ovr.regdist           + v-del +
                             ovr.name                          + v-del +
                             ovr.slsrep                        + v-del +
                             substr(ovr.takenby,1,4)           + v-del +
                             substr(ovr.takenby,6,20)          + v-del +
                             ovr.order                         + v-del +
                             ovr.transtype                     + v-del +
                             ovr.pline                         + v-del +
                             ovr.prod                          + v-del +
                             ovr.proddesc                      + v-del +
                             string(ovr.qtyord,"->>>>>>>>9")   + v-del +
                             string(ovr.stndcost,"->>>>>9.99") + v-del +
                             string(ovr.overcost,"->>>>>9.99") + v-del +
                             string(ovr.enterdt,"99/99/99")    + v-del +
                             x-comment1                        + v-del +
                             x-comment2.
*/          
         assign export_rec = export_rec + chr(13) + chr(10).
         put stream expt unformatted export_rec.
         assign export_rec = "".
   end.
   else do:
     assign s-regdist   = expDT.regdist  
            s-name      = expDT.name
            s-slsrep    = expDT.slsrep
            s-takenby   = expDT.takenby
            s-order     = expDT.ordernoL
            s-transtype = expDT.transtype
            s-prod      = expDT.prod
            s-proddesc  = expDT.proddesc.
     if LINE-COUNTER + com-cnt > PAGE-SIZE then do:
        page.
     end.
      
     
     display
       expdt.custno  
       expdt.name         
       expdt.TakenBy      
       expdt.OrderNoL 
       expdt.Lineno    
       expdt.Prod   
       expdt.Proddesc  
       expdt.PoExpDt  
       expdt.OeExpDt  
       expdt.ExpDtcode 
       expdt.transdt
     with frame f-detail.
     down with frame f-detail.
   
  end.
 
 
 
 
 
&endif
 
&if defined(user_finalprint) = 2 &then
 
 
 
 
 
&endif
 
&if defined(user_summaryframeprint) = 2 &then
 
 
 
 
 
&endif
 
&if defined(user_summaryputexport) = 2 &then
 
 
 
 
 
&endif
 
&if defined(user_detailputexport) = 2 &then
 
 
 
 
&endif
 
&if defined(user_procedures) = 2 &then
 procedure formatoptions:
    assign p-portrait = false.
    /* if false then the report will print in LANDSCAPE 178 character */
  
    if sapb.optvalue[1] > "0" and sapb.optvalue[1] ne "99" then do:
       p-optiontype = " ".
       find notes where notes.cono = g-cono and
                        notes.notestype = "zz" and
                        notes.primarykey = "oerxu" and  /* tbxr program */
                        notes.secondarykey = string(p-format) no-lock no-error.
       if not avail notes then do:
         display "Format is not valid cannot process request".
         assign p-optiontype = "R"
                p-sorttype   = ">,"
                p-totaltype  = "N"
                p-summcounts = "A".
         return.
       end.               
   
       assign p-optiontype = notes.noteln[1]
              p-sorttype   = notes.noteln[2]
              p-totaltype  = notes.noteln[3]
              p-summcounts = notes.noteln[4]
        /*      
              p-export     = "    "
        */      
              p-register   = notes.noteln[5]
              p-registerex = notes.noteln[6].
    end.     
    else
       if sapb.optvalue[1] = "99" then do:
          assign p-register   = ""
                 p-registerex = "".
          run reportopts(input sapb.user5, 
                         input-output p-optiontype,
                         input-output p-sorttype,  
                         input-output p-totaltype,
                         input-output p-summcounts,
                         input-output p-register,
                         input-output p-regval).
       end.
 end.
 
 
 
 
 
&endif
 
