{g-all.i}
{g-oeet.i}

def shared buffer b-oeeh for oeeh.

def buffer z-oeeh for oeeh.
def buffer AC-oeel for oeel.
def var s-reasoncd  as char format "x(2)" no-undo.
def var s-descrip   as char format "x(40)" no-undo.


def temp-table restbl
  field reasoncd    as character format "x(2)"
  field description as character format "x(40)"
index vv
  reasoncd.


define query  q-restbl for restbl scrolling.
define browse b-restbl query q-restbl
      display restbl.reasoncd
              restbl.description
      with 6 down width 44 centered overlay no-hide no-labels
title 
"       Select Reason Code           ". 




form
s-reasoncd   at 1 format "xx" label "All Cost Order ReasonCD"
{f-help.i}
s-descrip    at 1            label "Reason Description"
with frame f-oeetrfx row 10  overlay side-labels
title "     Cost Only Reason Code Entry " centered.





on f12 anywhere
 do:
 run zoeet95pop.p (input-output s-reasoncd). 
 
 find restbl where restbl.reasoncd = s-reasoncd no-lock no-error.
  
 if avail restbl then
   assign s-descrip = restbl.description.
 else
   assign s-descrip = " ".

 display s-reasoncd 
         s-descrip
   with frame  f-oeetrfx.
      
 end.

find z-oeeh where z-oeeh.cono = g-cono and
                  z-oeeh.orderno  = g-orderno and
                  z-oeeh.ordersuf = g-ordersuf no-error.

if avail z-oeeh then
  find first AC-oeel where AC-oeel.cono       = z-oeeh.cono and
                           AC-oeel.orderno    = z-oeeh.orderno and
                           AC-oeel.ordersuf   = z-oeeh.ordersuf and
                           AC-oeel.price      = 0 and
                           AC-oeel.specnstype <> "l" and
                           AC-oeel.shipprod   <> "INSTALLMENT PAYMENT"
                           no-lock no-error.
    

/*
message z-oeeh.orderno z-oeeh.ordersuf
   z-oeeh.totlineord    z-oeeh.nextlineno 
   z-oeeh.totcostord.
    
pause .
*/
if avail z-oeeh and
  (z-oeeh.totlineord = 0 and
   z-oeeh.nextlineno <> 1 and
   z-oeeh.totcostord <> 0) or
   avail AC-oeel
   then
  do:
  run load-reasoncd.

  assign s-reasoncd =  substring(z-oeeh.user2,99,2). 

  find restbl where restbl.reasoncd = 
      left-trim(s-reasoncd) no-lock no-error.
  
  if avail restbl then
    assign s-descrip = restbl.description.
  else
    assign s-descrip = " ".

  
  display  s-reasoncd 
           s-descrip
    with frame f-oeetrfx.
    updt:
    do while true on endkey undo updt, leave updt: 
      update s-reasoncd 
         with frame 
            f-oeetrfx
        editing:
          readkey.
          assign s-reasoncd.
          if {k-cancel.i} and s-reasoncd = "" then
            do:
            message "Cost Only Reason Code Required ".
            next.
            end.
        apply lastkey.
        end. 
      if {k-cancel.i} then
        leave updt.

      if {k-after.i} or {k-accept.i} or {k-return.i} then
        do:
        if s-reasoncd = "" then
          do:
          message "Cost Only Reason Code Required ".
          next updt.
          end.
        if z-oeeh.user2 = "" then
          assign z-oeeh.user2 = " ".

         
        find restbl where restbl.reasoncd = 
          left-trim(s-reasoncd) no-lock no-error.
  
        if avail restbl then
          assign s-descrip = restbl.description.
        else
          do:
          message "Invalid Reason Code, Reason Code Required ".
          next updt.
          assign s-descrip = " ".
          end.
        display s-reasoncd 
          s-descrip
          with frame  f-oeetrfx.
 
        
        assign substring(z-oeeh.user2,99,2) = s-reasoncd. 

        end.
      if {k-after.i} or {k-accept.i} or {k-return.i} then

        leave updt.
       
   end.
 end.
 else
 if avail z-oeeh then
  do:
  assign substring(z-oeeh.user2,99,2) = "  ". 
  end.
 hide frame  f-oeetrfx.
 
 
 
 procedure load-reasoncd:
 


 for each zsastz where
          zsastz.cono = g-cono and
          zsastz.labelfl = false and
          zsastz.codeiden = "AllCostReasons":
 
   create restbl.
   assign restbl.reasoncd = substring(zsastz.primarykey,1,2)
          restbl.description =  zsastz.secondarykey.
 end.          

 END.  
 
/* 
 procedure load-reasoncd:
 
 create restbl.
 assign restbl.reasoncd = "A"
        restbl.description = "Sales Samples".

 create restbl.
 assign restbl.reasoncd = "B"
        restbl.description = "SunSource Warranty - Service Repair".
 create restbl.
 assign restbl.reasoncd = "C"
        restbl.description = "SunSource Warranty - Fabrication".
 create restbl.
 assign restbl.reasoncd = "D"
        restbl.description = "Surcharge Offset".
 create restbl.
 assign restbl.reasoncd = "E"
        restbl.description = "SunSource Warranty - Customer Goodwill".
 create restbl.
 assign restbl.reasoncd = "F"
        restbl.description = "Dedicated Customer Inventory Write-off".
 create restbl.
 assign restbl.reasoncd = "G"
        restbl.description = "Purchasing Error".
 create restbl.
 assign restbl.reasoncd = "H"
        restbl.description = "Customer Service Dept Error".
 create restbl.
 assign restbl.reasoncd = "I"
        restbl.description = "Account Manager Error".

 create restbl.
 assign restbl.reasoncd = "J"
        restbl.description = "Material Inadvertently Shp on Orig Order".
END.
*/   
   
