/******************************************************************************
  VERSION 5.5  : called from lansdcape(poepp6a.p) , portrait(poepp9a.p) ,
               : fax(poeppf9.p) , e-mail(poeppm1.p) SDI specific programs
  INCLUDE      : p-poepp6.i
  DESCRIPTION  : Purchase Order Entry PO Print format 1 - NEMA
  USED ONCE?   : no
  AUTHOR       : kmw
  DATE WRITTEN : 11/09/90
  CHANGES MADE :
    10/14/91 mwb; TB#  4761 Moved page # and resale # to fit pre-printer forms
    11/13/91 mwb; TB#  2572 If an 'rm' then use the apsv or apss info for ship
                  to display
    08/18/92 mwb; TB#  7319 Added unitconv to replace stkqtyord / qtyord.
    08/19/92 mwb; TB#  7398 Don't use v-conv for qty's, use stkqtyord/qtyord
    09/25/92 mjq; TB#  6410 Print the manual address when using misc
                       Vendor # 999999999999
    11/11/92 mms; TB#  8669 Rush - Display Rush message in header
    12/21/92 mtt; TB#  9266 Changed ship to address to print
                       '*** Do Not Ship ***' for a Blanket PO (BL).
    02/25/93 rs ; TB#  2932  Indicate whether a po has been costed or received
    02/25/93 rs ; TB#  9323  Ship To info not printing right for RM orders
    03/18/93 rs ; TB# 10241 Cust# does not print on POEPP
    03/18/93 rs ; TB# 10264 Price U/M displays ICSP Special Cost
    04/12/93 rs ; TB# 10061 POEPP doesn't print bin locs for rm's
    04/21/93 rhl; TB# 10892 Alt. vend product not printed
    05/15/93 rhl; TB#  9461 FOB Terminology Is Incorrect
    08/23/93 tdd; TB# 12699 Printing "c" status lines
    11/05/93 mwb; TB# 13520 Carry unitconv to 10 deciamals
    02/21/94 dww; TB# 13890 File name not specified causing error
    09/13/94 tdd; TB# 15541 REPRINT printing a line too low
    04/27/95 mtt; TB# 13356 Serial/Lot # doesn't print on PO/RM
    10/11/95 kr ; TB# 19526 Move POEP to new Processing Menu
    10/31/95 gp;  TB# 18091 Expand UPC Number (T23)
    11/06/95 gp;  TB# 18091 Expand UPC Number (T23)
    04/10/96 kjb; TB# 20919 Printing a BR in Entered stage can result in a
                      divide by zero situation
    06/25/96 des; TB# 19484 (B1a) Warehouse Logistics Send data to WL using
        WLET, WLEH and WLEL records.
    08/02/96 mms; TB# 19484 (B1a) Warehouse Logistics-Distribution acceptance
    08/06/96 smn; TB# 21254 (18G) Develop Value Add Module-Add logic to print
        Value Add Transaction information.
    10/03/96 des; TB# 19484 (B1a) Warehouse Logistics Add return logic.
    10/04/96 gp;  TB# 19484 (B1a) Warehouse Logistics. Create WL records
                       only for PO, BR and RM transaction types.
    10/30/96 ajw; TB# 19484 (B13) Warehouse Logistics - Added ShipmentID
    03/02/97 jkp; TB#  7241 (5.2) Spec8.0 Special Price Costing.  Added find
                       of icss record.
    06/04/98 cm;  TB# 21865 Ship from our cust# not displayed
    10/07/98 kjb; TB# 21027 Add a second nonstock description
    12/23/98 cm;  TB# 14835 Allow notes specific by suffix
    11/18/99 cm;  TB# e2761 Tallies Project; Added tally product explosion
                      Corrected some poor indentation also
    03/29/00 rgm; TB# 3-2  Add RxServer Interface to Standard
    05/08/00 rgm; TB# A9   Fix for RxServer Faxing issue
    01/30/01 mwb; TB# e7440 Added the display of the Job ID if loaded
    11/26/01 sbr; TB# e8887 POET F10 on WM RM/PO Progress error 444
    01/16/02 lbr; TB# e3207 Added printing of catalog notes
    08/14/02 ap;  TB# e13477 Added logic for printing closed PO's
    08/20/02 des; TB# e11863 Specify where notes print by page
    10/02/02 gkd; TB# L956 Send all lines to TWL even if printing changes only
    10/10/02 jbt; TB# e11863 Call generic notes printing program.
    09/29/02 n-jk; TB# e8651 Core - Show Core Allocation Detail
    10/17/02  al; TB# x74 Change PO 'Invoice To' information - PTX
    11/12/02 kjb; TB# e15321 Modify the Tally print logic - Replace the
                      existing tally print logic with new logic found in 
                      tallyprt.ldi
    04/15/03 lcb; TB# t14244 8Digit Ord No Project
    07/01/03 mms; TB# e17778 8 Digit OrdNo.
    08/11/03 rgm; TB# e18072 add Language code to RxServer output as needed
    05/01/06 ejk; TB# e24390 don't send PORD PO reprint to TWL
    07/07/06 gkds;TB# e20597 Vendor Chained Discounts
    07/24/06 ejk; TB# L1969 PRE wasn't created when print to email
    07/25/06 mtt; TB# e24980 Change $ to soft label currency symbol
    09/08/06 bp ; TB# e25265 Add user hooks.
    10/05/06 bp ; TB# e25382 Add user hooks.
    04/10/07 ejk; TB# L2037 Skip create of WLEL for not-for-resale PO lines
    04/23/07 mms; TB# e20597 Backout changes for-Add PO Chained Discounts
Modifications:
 TAH 03/29/07 FPT Eaton Mod          
*******************************************************************************/
/* SX55 */
def var s-lit48d as c format "x(10)" no-undo. /*si02*/
def var s-soldcity as c format "x(35)" no-undo.    /*08/29/97 */
def     var v-reqtitle    as c format "x(30)" initial "". 
/*e If print fields are added or changed, check poeppe1.p for EDI format */

/* DBF IT# 1 */
def buffer z-icsd for icsd.
def buffer v-icsd for icsd.
def buffer z-oimsp for oimsp.
def buffer b-poelo for poelo.
def var z-type as character format "x(5)" no-undo.
/* DBF IT# 1 */
/* TAH 03/29/07 FPT Eaton Mod  */         
def var s-hvendno as character format "x(12)" no-undo.
/* TAH 03/29/07 FPT Eaton Mod  */         
/* TAH 01/24/17 FOB DJ */
define buffer zz-notes for notes.
/* TAH 01/24/17 FOB DJ */



/* SX55 */

/*e If print fields are added or changed, check poeppe1.p for EDI format */

assign
    v-pageno    = page-number - 1
    v-totinvamt = 0.

def var v-jobid as char format "x(45)"  no-undo.

/*tb 3-2 03/29/00 rgm; Rxserver interface display vars for rxserver */
/*e these have to be defined outside frame because they
    get assigned in a-poepp1.i */
def var s-lit14a      as c format "x(78)"                  no-undo.
def var s-lit14b      as c format "x(50)"                  no-undo.
def var s-lit15a      as c format "x(78)"                  no-undo.
def var s-lit15b      as c format "x(50)"                  no-undo.
def var s-lit16a      as c format "x(78)"                  no-undo.
def var s-lit16b      as c format "x(50)"                  no-undo.

/*tb 15541 09/13/94 tdd; REPRINT printing a line too low.  Positioning one
    character off causing s-reprint to rap to next line. */
/*tb 18091 10/31/95 gp;  Changed UPC Number */
/*tb 21865 06/04/98 cm; Ship from our cust# not displayed; Replaced
    apsv.apcustno with s-apcustno. */
form header
    /* SX55 */
    v-tof                                       at 1     /*si03*/
    " "                                         at 43     
    s-comphd1     as c format "x(50)"           at 43   
    s-comphd2     as c format "x(50)"           at 43
    s-lit1a       as c format "x(41)"           at 1
    s-comphd3     as c format "x(50)"           at 43
    /* s-reprint     as c format "x(46)"           at 46    si01 move down */
    s-lit1b       as c format "x(40)"           at 93
    s-lit2a       as c format "x(41)"           at 1
    s-comphd4     as c format "x(50)"           at 43
    /* s-rush        as c format "x(12)"           at 57    si01 move down */
    /* SX55 */
    s-upcvno                                    at 95
    poeh.orderdt                                at 105
    poeh.pono                                   at 116
    "-"                                         at 123
    poeh.posuf                                  at 124
    page-num - v-pageno format "zzz9"           at 129
    s-lit3a       as c format "x(9)"            at 1
    /* SX55 */
    /* TAH 03/29/07 FPT Eaton Mod  */         
    s-hvendno                                   at 11
    /* TAH 03/29/07 FPT Eaton Mod           
    poeh.vendno                                 at 11
    TAH 03/29/07 FPT Eaton Mod  */         
    s-currdescrip like sastc.descrip            at 24
    s-custnolbl   as c format "x(7)"            at 105
    /* SX55 */ 
    s-apcustno  /*  like apss.apcustno    */    at 113
    /* SX55 */
    s-reprint     as c format "x(46)"           at 20    /* si01 moved here */
    s-rush        as c format "x(12)"           at 70    /* si01 moved here */
    /* skip(1)                                              si01 */
    s-lit6a       as c format "x(7)"            at 3
    s-sellernm    like apsv.name                at 11
    s-lit6b       as c format "x(8)"            at 43
    /* SX55 */
/*   b-sasc.name                                 at 52      08/29/97*/
    s-lit6c       as c format "x(11)"           at 84
    s-invname     like icsd.name                at 96
    s-selleraddr[1]                             at 11
    /* SX55 */
/*    b-sasc.addr[1]                              at 52     08/29/97*/
    s-invaddr[1]                                at 96
    s-selleraddr[2]                             at 11
    /* SX55 */
/*    b-sasc.addr[2]                              at 52     08/29/97*/
    s-invaddr[2]                                at 96
    s-sellercity  as c format "x(35)"           at 11
    /* SX55 */ 
/*    s-soldcity    as c format "x(35)"           at 52     08/29/97*/
    s-invcity     as c format "x(35)"           at 96
    skip(1)
    s-lit11a      as c format "x(8)"            at 2
    s-shiptonm    like poeh.shiptonm            at 11
    s-lit11b      as c format "x(12)"           at 47
    s-lit11c      as c format "x(8)"            at 79
    s-lit11d      as c format "x(9)"            at 103
    s-shiptoaddr[1]                             at 11
    poeh.shipinstr                              at 47
    poeh.resaleno                               at 79
    poeh.refer                                  at 103
    s-shiptoaddr[2]                             at 11
    s-lit12b      as c format "x(3)"            at 51
    s-lit12c      as c format "x(12)"           at 65
    s-lit12d      as c format "x(13)"           at 83
    s-lit12e      as c format "x(13)"           at 102
    s-lit12f      as c format "x(8)"            at 118
    s-shipcity    as c format "x(35)"           at 11
    s-shipvia     as c format "x(12)"           at 47
    /* SX55 */
    "See Below"                                 at 66
/*    poeh.duedt                                  at 65 */
    s-fob         as c format "x(19)"           at 81
    s-terms       as c format "x(12)"           at 102
    s-schedule    as c format "x(14)"           at 118
    /* SX55
    /* skip(1)                                                  si01 */
    /*tb A9 05/08/00 rgm; Rxserver interface change when appropriate */
    &IF DEFINED(rxserver) = 0 &THEN
        skip(1)
    &ELSE
        s-rxtext1                               at 1
    &ENDIF
    */
with frame f-head no-box no-labels
           &IF DEFINED(rxserv_width) = 0 &THEN
               width 132
           &ELSE
               width {&rxserv_width}
           &ENDIF
           page-top.

/*tb 3-2 03/29/00 rgm; Rxserver interface display vars for rxserver */
form header
    &IF DEFINED(rxserver) = 0 &THEN
        s-lit14a                            at 1
        s-lit14b                            at 79
        s-lit15a                            at 1
        s-lit15b                            at 79
        s-lit16a                            at 1
        s-lit16b                            at 79
    &ELSE
        s-rxhead1                           at  1
        s-rxhead2                           at  1
        s-rxhead3                           at  1
    &ENDIF
with frame f-headl no-box no-labels
           &IF DEFINED(rxserv_width) = 0 &THEN
               width 132
           &ELSE
               width {&rxserv_width}
           &ENDIF
           page-top.

form
    skip(1)
    v-linecnt          format "zz9"             at 1
    s-lit40a      as c format "x(11)"           at 5
    s-lit40b      as c format "x(18)"           at 42
    s-totqtyord   as c format "x(13)"           at 62
    s-lit40c      as c format "x(18)"           at 76
    s-totinvamt   as c format "x(13)"           at 103
with frame f-tot1 no-box no-labels no-underline width 132.

form
    s-title2      as c format "x(18)"           at 75
    s-amt2        as dec format "zzzzzzzz9.99-" at 102
    s-type2       as c format "x(1)"            at 115
with frame f-tot2 no-box no-labels width 132 no-underline down.

/*tb 3-2 03/29/00 rgm; Rxserver interface display vars for rxserver */
form header
    s-lit48a      as c format "x(9)"            at 1
    s-lit48b      as c format "x(11)"           at 80
    /* SX55 */
    s-confirm     as c format "x(3)"            at 92
    s-buyerph     as c format "x(17)"           at 99
    s-buyerfx     as c format "x(17)"           at 117          
    s-lit48c      as c format "x(6)"            at 80
    s-buyer       as c format "x(24)"           at 99
    /* SX55 */
/*    s-lit48d      as c format "x(10)"           at 80   si02*/
with frame f-tot4 no-box no-labels width 140 page-bottom.         
    /* SX55
    &IF DEFINED(rxserver) <> 0 &THEN
        s-rxfoot1                        at 132
    &ENDIF
with frame f-tot4 no-box no-labels
           &IF DEFINED(rxserv_width) = 0 &THEN
               width 132
           &ELSE
               width {&rxserv_width}
           &ENDIF
           page-bottom.
    */

/*tb e8651 09/29/02 n-jk; Core - Show Core Allocation Detail */
define frame f-alloc
    "Core Allocated against PO"     at 8
    iceaa.orderno                   at 34
    "-"                             at 42
    iceaa.ordersuf                  at 43
    "Line"                          at 46
    iceaa.lineno                    at 51
    "Qty"                           at 55
    iceaa.qty                       at 59
with no-box no-labels width 132 down.

/* SX55 **********************************************/
form confirm
    s-lit49       as c format "x(95)"           at 3
    s-lit49a      as c format "x(70)"           at 3
    skip(1)
    s-lit51a      as c format "x(55)"           at 39
    skip(1)
with frame f-confirm no-box no-labels width 132 no-underline.

/* TAH GOV */

form 
    skip(1)
    s-lita49      as c format "x(125)"           at 3
    s-lita49a     as c format "x(125)"           at 3

with frame f-govstuff no-box no-labels width 132 no-underline.


/* form 
    s-lit51a      as c format "x(55)"           at 39
    skip(1)
with frame f-message no-box no-labels width 132 no-underline. */

form 
    skip(1)
    s-lit52a      as c format "x(90)"           at 13
    s-lit52b      as c format "x(90)"           at 13
    skip(1)
    s-lit52c      as c format "x(90)"           at 13
with frame f-relmsg1 no-box no-labels width 132 no-underline. 
   
form 
    s-relpono     as c format "x(10)"           at 12 
    s-relprod     as c format "x(24)"           at 30
    s-relqty      as i format "zzzzzz9"         at 60
    s-reldate     as date                       at 81
with frame f-reldet1 no-box no-labels width 132 no-underline.

form
    s-spaces      as c format "x"               at 1 
with frame f-spaces no-box no-labels width 132 no-underline.

form
    skip(1)
    s-lit53      as c format "x(130)"           at 3
    s-lit53a     as c format "x(130)"           at 3
    s-lit53b     as c format "x(130)"           at 3
    s-lit53c     as c format "x(130)"           at 3
    s-lit53d     as c format "x(130)"           at 3
with frame f-termclause no-box no-labels width 132 no-underline.

/* SX55 *******************************************/

/*o Assign all of the form labels */
{a-poepp6.i s-}

/*tb A9 05/08/00 rgm; Rxserver interface change when appropriate */
&IF DEFINED(rxserver) <> 0 &THEN
    assign
        s-rxtext1               = s-lit14a
        overlay(s-rxtext1,79)   = s-lit14b
        overlay(s-rxtext1,133)  = s-lit15a
        overlay(s-rxtext1,211)  = s-lit15b.
    if avail apsv then
    assign
        overlay(s-rxhead1,1,2)  = apsv.langcd.
&ENDIF

/* SX55 */
assign s-comphd2 = "" 
       s-comphd3 = "" 
       s-comphd4 = "".

/*tb 18091 10/26/95 gp;  Find the first ICSV record for this vendor */
find first icsv use-index k-vendno where
    icsv.cono   = g-cono and
    icsv.vendno = poeh.vendno
no-lock no-error.

/*o Set the form variables for the banner/header */
/*tb 18091 10/31/95 gp;  Add UPC number assignment */
/*tb 19484 10/04/96 gp;  Assign v-wlwhsefl to no because WL records
    are not created for every PO transaction type */
/*tb 21865 06/04/98 cm; Ship from our cust# not displayed. Use APSS if entered
    otherwise APSV. */

/* SX55 */
/* TAH 03/29/07 FPT Eaton Mod  */         
assign s-hvendno = string(poeh.vendno,">>>>>>>>>>>9").
/* TAH 03/29/07 FPT Eaton Mod  */         

if (poeh.vendno = 222095000 or
    poeh.vendno = 222000019 ) then do:
    if poeh.whse = "dnvr" then
      assign s-hvendno = "F.P.T.".
end.

assign
    v-wlwhsefl      = no
    s-apcustno      = if avail apss and apss.apcustno <> "" then apss.apcustno
                      else apsv.apcustno
    s-custnolbl     = if s-apcustno <> "" then "Cust #:" else ""
    s-sellernm      = if poeh.vendno = 999999999999.0 then poeh.manname
                      else if avail apss then apss.name else apsv.name
    s-selleraddr[1] = if poeh.vendno = 999999999999.0
                          then poeh.manaddr[1]
                      else if avail apss then apss.addr[1]
                      else apsv.addr[1]
    s-selleraddr[2] = if poeh.vendno = 999999999999.0
                          then poeh.manaddr[2]
                      else if avail apss then apss.addr[2]
                      else apsv.addr[2]
    s-sellercity    = if poeh.vendno = 999999999999.0 then
                          poeh.mancity + ", " + poeh.manstate + " "
                          + poeh.manzipcd
                      else if avail apss then
                          apss.city + ", " + apss.state + " " + apss.zipcd
                      else
                          apsv.city + ", " + apsv.state + " " + apsv.zipcd
    s-soldcity      = b-sasc.city + ", " + b-sasc.state + " " + b-sasc.zipcd.

/* tb# x74 - PTX - change PO 'Invoice To' information */
&IF "{&NXTREND-APP}" = "PTX" &THEN
  {po-ptx-invoiceto.i &b = "poeh"}
  if s-billtoname <> "" then
      assign
        s-invname    = s-billtoname
        s-invaddr[1] = s-billtoaddr1
        s-invaddr[2] = s-billtoaddr2
        s-invcity    = s-billtocity + ", " + s-billtost + " " + s-billtozip.
  else
&ENDIF
assign
    s-invname       = if avail icsd then icsd.name else b-sasc.name
    s-invaddr[1]    = if avail icsd then icsd.addr[1] else b-sasc.addr[1]
    s-invaddr[2]    = if avail icsd then icsd.addr[2] else b-sasc.addr[2]
    s-invcity       = if avail icsd then

                          icsd.city + ", " + icsd.state + " " + icsd.zipcd
                      else
                           s-soldcity.


assign
        /* SX55 */
        z-type      = if sapbo.user2 = "fax" then
                         "*FAX*"
                      else
                      if sapbo.user2 = "email" then
                         "*E-Mail*"
                      else
                      if sapbo.user2 begins "edi" then
                         "*EDI*"
                      else ""   
    s-reprint       = if ((poeh.pocnt < 2 and z-type <> "*EDI*") or
                         (poeh.pocnt < 3 and z-type = "*EDI*")) then
                        (z-type + " " +
                          if poeh.stagecd = 5 then "***(Received)***"
                          else if poeh.stagecd = 6 then "***(Costed)***"
                          else if poeh.stagecd = 7 then "***(Closed)***"
                          else "")
                      else 
                          z-type + " " +
                          "*** R E P R I N T " +
                          (if p-changefl = yes then "(Changes Only) "
                           else "") +
                          (if poeh.stagecd = 5 then "(Received)"
                           else if poeh.stagecd = 6 then "(Costed)"
                           else if poeh.stagecd = 7 then "(Closed)"
                           else "")
                           + "***"
/*
    s-reprint       = if poeh.pocnt < 2 then
                          (if poeh.stagecd = 5 then "***(Received)***"
                      else if poeh.stagecd = 6 then "***(Costed)***"
                      else if poeh.stagecd = 7 then "***(Closed)***"
                      else "")
                      else "*** R E P R I N T " +
                          (if p-changefl = yes then "(Changes Only) "
                           else "") +
                          (if poeh.stagecd = 5 then "(Received)"
                           else if poeh.stagecd = 6 then "(Costed)"
                           else if poeh.stagecd = 7 then "(Closed)"
                           else "")
                           + "***"
*/
    s-rush          = if poeh.rushfl then "*** RUSH ***" else ""
    v-linecnt       = 0
    s-schedule      = if poeh.orderdisp = "s" then "Ship Complete"
                      else if poeh.bofl      = yes then "BO Partial"
                      else if poeh.bofl      = no  then "Cancel Partial"
                      else "Mutually Defn"
/* SX55 
    s-confirm       = if poeh.confirmfl then "Yes" else "No"
*/
    s-confirm       = "Yes"  /* if poeh.confirmfl then "Yes" else "No" */
    s-shiptonm      = poeh.shiptonm
    s-shiptoaddr[1] = if poeh.transtype = "bl" then " "
                      else poeh.shiptoaddr[1]
    s-shiptoaddr[2] = if poeh.transtype = "bl" then "*** Do Not Ship ***"
                      else poeh.shiptoaddr[2]
    s-shipcity      = if poeh.transtype = "bl" then " "
                      else poeh.shiptocity + ", " +
                          poeh.shiptost +
                          " " + poeh.shiptozip
    s-upcvno        = if avail icsv and avail sasc then
                          {upcno.gas &section  = "3"
                                     &sasc     = "sasc."
                                     &icsv     = "icsv."
                                     &comdelim = "/*"}
                      else "000000".

/*d Find and display the description of table values */
{w-sastc.i poeh.currencyty no-lock}
assign
    s-currdescrip = if avail sastc then sastc.descrip else "".
{w-sasta.i "S" poeh.shipviaty no-lock}
assign
    s-shipvia  = if avail sasta then sasta.descrip else poeh.shipviaty.
if apsv.langcd ne "" then
    {w-sals.i apsv.langcd "t" poeh.termstype no-lock}
{w-sasta.i "t" poeh.termstype no-lock}
assign
    s-terms    = if avail sals then sals.descrip[1] else
                     if avail sasta then sasta.descrip else poeh.termstype.
/* SX55 *******************************/
/* tah SDI2 Termsupdate DO NOT PRINT  */
 if poeh.vendno = 81101539 then
    s-terms = "Net 30".
 else   
    s-terms = "".
/* tah SDI2 Termsupdate DO NOT PRINT  */

if avail apsv and substr(apsv.user11,1,4) <> "    " then
  assign s-terms = if avail sasta then sasta.descrip else poeh.termstype.

find first z-oimsp where
           z-oimsp.person = poeh.buyer
     no-lock no-error.
if avail z-oimsp then
 do:
   assign 
   s-buyerph = "TEL:(" + substring(z-oimsp.phoneno,1,3) + ")" +
                         substring(z-oimsp.phoneno,4,3) + "-" + 
                         substring(z-oimsp.phoneno,7,4)
   s-buyerfx = "FAX:(" + substring(z-oimsp.faxphoneno,1,3) + ")" +
                         substring(z-oimsp.faxphoneno,4,3) + "-" +
                         substring(z-oimsp.faxphoneno,7,4).
 end.
else 
  assign s-buyerph = " "
         s-buyerfx = " ".
/* SX55 *******************************/

{w-sasta.i "b" poeh.buyer no-lock}
assign
    s-buyer    = if avail sasta then sasta.descrip else poeh.buyer.
    s-fob      = if poeh.fobfl then "F.O.B.-Delivered"
                 else "F.O.B.-Ship Point".
/* TAH 01/24/17 FOB DJ */
find zz-notes where 
     zz-notes.cono = g-cono and
     zz-notes.notestype = "zz" and
     zz-notes.primarykey = "FOB labels" and
     zz-notes.secondarykey = string (poeh.vendno) no-lock no-error.
if avail zz-notes then do:
  assign s-fob = if zz-notes.noteln[1] <> " " and poeh.fobfl then
                   zz-notes.noteln[1]
                 else
                 if zz-notes.noteln[2] <> " " and not poeh.fobfl then
                   zz-notes.noteln[2]
                 else
                   s-fob.
end.
/* TAH 01/24/17 FOB DJ */


/* FG update tah */
if avail z-oimsp and
/*
   NOT  (z-oimsp.responsible begins "mob" or
         z-oimsp.responsible begins "serv" or
         z-oimsp.responsible begins "ind1" ) then do:
*/ 
     z-oimsp.responsible = "indXXXXX" then do:
                            
   if (num-entries (poeh.createdby,"-") ge 2 and 
     (entry(2,poeh.createdby,"-") = "cv" or    
      entry(2,poeh.createdby,"-") = "oe") ) or   
     (num-entries (poeh.createdby,"-") = 1) then do:                               find first z-oimsp where
               z-oimsp.person = entry(1,poeh.createdby,"-")
         no-lock no-error.
    if avail z-oimsp then do:
       assign 
         s-buyerph = "TEL:(" + substring(z-oimsp.phoneno,1,3) + ")" +
                               substring(z-oimsp.phoneno,4,3) + "-" + 
                               substring(z-oimsp.phoneno,7,4)
         s-buyerfx = "FAX:(" + substring(z-oimsp.faxphoneno,1,3) + ")" +
                               substring(z-oimsp.faxphoneno,4,3) + "-" +
                               substring(z-oimsp.faxphoneno,7,4).
         s-buyer    = if z-oimsp.name <> ""  then 
                        z-oimsp.name 
                      else 
                        entry(1,poeh.createdby,"-").

    end.
  end.
end. 
/* FG update tah */


/*tb 19484 06/25/96 des; Warehouse Logistics logic added */
/*tb 19484 10/04/96 gp;  Only create WL records for PO, BR and RM types */
/*o Warehouse Logistics processing for temp tables and WL transaction and
    header */
/*d If WL is purchased, this is the first copy (if multiples are done only need
    one WL record create) and if this is a fax run and they print the hard copy
    only create the data once (on the "p" outputty run) */

if v-modwlfl = yes                   and
   can-do("po,br,rm":u,poeh.transtype) and
   v-index = 1                       and
   ((sapbo.outputty = "f":u and sasc.oifaxhardfl[1] = false) or
     sapbo.outputty = "m":u  or
     sapbo.outputty = "p":u)           and
   poeh.stagecd < 5
then do:

    /*tb 19484 10/03/96 des; Warehouse Logistics return logic */
    v-processty = if poeh.transtype = "RM":u then "PCK":u else "PRE":u.

    /*d Create a temp table entry if one does not exist and create the wlet as
        well, if necessary, otherwise just assign the setno and wlwhsefl based
        on the temp table. */
    {wlpcrt.gpr &whse       = poeh.whse
                &b          = "b-"
                &priority   = 5
                &transtype  = ""S""
                &process    = v-processty }

    /*tb 19484 10/30/96 ajw; (B13) Warehouse Logistics - Shipmentid*/
    /*d Create the WLEH record.  Since lineno and seqno are zero only header
        record will be created.  Inventory out condition is when the
        transaction is an RM. */
    {wlpproc.gpr &invoutcond    = "poeh.transtype = ""rm"""
                 &wlsetno       = v-wlsetno
                 &whse          = poeh.whse
                 &ordertype     = ""p""
                 &orderno       = poeh.pono
                 &ordersuf      = poeh.posuf
                 &lineno        = 0
                 &seqno         = 0
                 &msdssheetno   = """"
                 &updtype       = ""A""
                 &shipmentid    = """"}

end. /* v-modwlfl and v-index = 1 */

/*tb 3-2 03/29/00 rgm; Rxserver interface hook for extra vars */
{poepp1.z99 &before_headview = "*"}

view frame f-head.
view frame f-headl.
view frame f-tot4.

if poeh.jmjobid ne "" then do:

    display with frame f-blank.
    down with frame f-blank.
    v-jobid = "          Job ID: " + poeh.jmjobid + "-" +
              string(poeh.jmjobrevno,"999").
    display v-jobid no-label
            with down frame f-jobid no-box.
    down with frame f-jobid.
    display with frame f-blank.
    down with frame f-blank.
end.

{poepp1.z99 &before_notes = "*"}

/*d Print the vendor notes  */
if apsv.notesfl ne "" then
    run notesprt.p(g-cono,"v",string(poeh.vendno),"","poepp",5,2).

{poepp1.z99 &after_vendor_notes = "*"}

/*d Print the purchase order notes  */
if poeh.notesfl ne "" then
    run notesprt.p(g-cono,"x",string(poeh.pono),string(poeh.posuf),
                     "poepp",5,2).

{poepp1.z99 &after_notes = "*"}

/*d Collect tied VA orders-place in temp table */
{poepp1vc.lpr}

v-totqtyord = 0.
/*o Find each PO line to display */
/*tb e8651 09/29/02 n-jk; Core - Show Core Allocation Detail */

/* SX55 */
/* das */
display s-lit49 s-lit49a s-lit51a with frame f-confirm.    
down with frame f-confirm.
/* das */

poloop:
for each poel use-index k-poel where
         poel.cono        = g-cono      and
         poel.pono        = poeh.pono   and
         poel.posuf       = poeh.posuf  and
        (poeh.stagecd     >= 5          or
         poel.statustype  = "a")
no-lock:

    /*tb e8651 09/29/02 n-jk; Core - Show Core Allocation Detail */
    /*e Skip the implied line if combine cost or no core print is selected */
    if can-do("c,n",apsv.coreprice) and
       {icspimpl.gvl &prod = poel.shipprod} then next poloop.

    v-linecnt = v-linecnt + 1.

    {w-icsp.i poel.shipprod no-lock}

    /*o Warehouse Logistics processing for lines */
    /*d If this is the first print (we don't want multiple WLEH records for
        multiple copies) Don't create the WLEL records for labor products */
    /*d All lines need to go to TWL - even if printing changes only */
    /*d L2037; don't send not-for-resale lines to TWL */
    if v-wlwhsefl = yes and
         v-index  = 1   and
        ((sapbo.outputty = "f":u and sasc.oifaxhardfl[1] = false) or
          sapbo.outputty = "m":u  or
          sapbo.outputty = "p":u)  and
        (not avail icsp or icsp.statustype ne "l":u) and
        poeh.stagecd < 5 and
        poel.nonstockty ne 'r'
    then do:

        /*tb 19484 10/30/96 ajw; (B13) Warehouse Logistics - Shipmentid*/
        /*d Create the WLEL records.  Inventory out condition is when the
            transaction is an RM.  */
        {wlpproc.gpr &invoutcond    = "poel.transtype = ""rm"""
                     &wlsetno       = v-wlsetno
                     &whse          = poeh.whse
                     &ordertype     = ""P""
                     &orderno       = poel.pono
                     &ordersuf      = poel.posuf
                     &lineno        = poel.lineno
                     &seqno         = 0
                     &msdssheetno   = """"
                     &updtype       = ""A""
                     &shipmentid    = """"}

    end. /* v-index ... */

    /*d If the PO has been printed once before and changes only are
        requested and no changes have been made to the line since
        it printed, find the next line */
    if sapbo.reprintfl = yes and p-changefl = yes and poel.printfl = no
        then next.

    /*e Print specifications/instruction/line item data from VA order if
        the purchase order came from the Value Add Module */
/*
    {poepp1vp.lpr}
*/
    /*tb #e3207 Added read for catalog */
    if poel.nonstockty = "" then do:
        {w-icsw.i poel.shipprod poel.whse no-lock}
        {w-icsp.i poel.shipprod no-lock}
        if apsv.langcd ne "" then
            {w-sals.i apsv.langcd "p" poel.shipprod no-lock}
    end.
         else {w-icsc.i poel.shipprod no-lock}

    /*tb 18091 10/30/95 gp;  Find Product section of UPC number */
    {icsv.gfi "'u'" poel.shipprod poeh.vendno no}

    /*tb  7241 (5.2) 03/02/97 jkp; Added find of icss record based on
         the poel record because the prices/costs are not being recalculated. */
    {icss.gfi
        &prod        = poel.shipprod
        &icspecrecno = poel.icspecrecno
        &lock        = "no"}

    clear frame f-poel.

    /*d Set the PO line varibles for printing */

    /*tb 18091 10/31/95 gp;  Assign product UPC number */
    /*tb 20919 04/10/96 kjb; Trap for a divide by zero situation in the
         calculation of v-qtyord */
    /*tb  7241 (5.1) 03/02/97 jkp; Changed the assign of s-prcunit and
         s-spcunit to use icss instead of icsp. */
    assign
        v-conv      = {unitconv.gas &decconv = poel.unitconv}
        /* SX55 
        v-qtyord    = if poel.stkqtyord = 0 or poel.qtyord = 0 then 0
        */
        v-qtyord    =  if poel.transtype = "br" and poel.qtyord = 0 then 0
                      else
                          (poel.stkqtyord -
                             (if p-changefl = yes then poel.prevqtyord else 0))
                          / (poel.stkqtyord / poel.qtyord)
        s-qtyord    = if substring(string(v-qtyord,"9999999.99-"),9,2)
                          = "00" then string(v-qtyord,"zzzzzz9-")
                      else string(v-qtyord,"zzzzzz9.99-")
        s-lineno    = string(poel.lineno,"zz9")
        s-price     = if substring
                          (string(poel.price,"9999999.99999"),11,3) = "000"
                          then string(poel.price,"zzzzzz9.99-")
                      else string(poel.price,"zzzzzz9.99999-")
        s-prcunit   = if avail icss and icss.prccostper <> "" then
                          icss.prccostper else poel.unit
        s-spcunit   = if avail icss and index("ht",icss.speccostty) > 0
                          then "(" + icss.speccostty + ")"
                      else ""
        s-netamt    = string(poel.netamt,"zzzzzzzz9.99-")
        v-totinvamt = v-totinvamt + poel.netamt
        v-totqtyord = v-totqtyord + v-qtyord
        s-shipprod  = if p-ourprodfl = yes or poel.nonstockty ne "" or
                          avail icsw and icsw.vendprod = ""
                          then poel.shipprod
                      else icsw.vendprod
        s-binloc    =  ""
        s-upcpno    = if avail icsv and avail sasc then
                          {upcno.gas &section  = "4"
                                     &sasc     = "sasc."
                                     &icsv     = "icsv."
                                     &comdelim = "/*"}
                       else "00000".

    /*tb e8651 09/29/02 n-jk; Core - Show Core Allocation Detail */
    /*e If this is a reman line and combine price is selected, add in the
        implied line */
    if apsv.coreprice = "c" and {icspremn.gvl &prod = poel.shipprod} then do:

        {w-poel.i poel.pono poel.posuf "poel.lineno + 1" no-lock b-}

        if avail b-poel then
        assign
            v-price  = poel.price + b-poel.price
            s-price  = if substring
                          (string(v-price,"9999999.99999"),11,3) = "000"
                       then string(v-price,"zzzzzz9.99-")
                       else string(v-price,"zzzzzz9.99999-")
            s-netamt = string(poel.netamt + b-poel.netamt,"zzzzzzzz9.99-")
            v-totinvamt = poel.netamt + b-poel.netamt.

    end. /* combine price on implied core */

    if not p-ourprodfl and poel.nonstockty = "" and
        poel.vendno ne icsw.arpvendno then do:
            {w-icsec.i ""v"" poel.shipprod poel.vendno no-lock}
            if avail icsec then s-shipprod = icsec.altprod.
        end.

    /*d Print the PO line */

    /*tb 3-2 03/29/00 rgm; Rxserver interface hook for extra vars */
    {poepp1.z99 &before_linedisplay = "*"}

    /*tb 18091 10/31/95 gp;  Change UPC number display */
    /*tb 3-2 03/29/00 rgm; Rxserver interface display vars for rxserver */
    display
        s-lineno
        s-shipprod
        poel.unit
        s-qtyord
        s-upcpno
        s-price
        s-prcunit
        s-spcunit
        s-netamt
        poel.duedt
        s-sign
        &IF DEFINED(rxserver) &THEN
            s-rxline1
        &ENDIF
    with frame f-poel.

    if poel.nonstockty ne "" then do:

        /*tb 21027 10/07/98 kjb; Added proddesc2 for second nonstock
            description */
        if poel.proddesc ne "" then
            display
                poel.proddesc + " " + poel.proddesc2 @ s-descrip
            with frame f-poel.

    end.

    else if not avail icsp or not avail icsw then
        if apsv.langcd = "" or not avail sals then
            display v-invalid @ s-descrip with frame f-poel.
        else
            display sals.descrip[1] + " " + sals.descrip[2]
            @ s-descrip with frame f-poel.
    else if apsv.langcd = "" or not avail sals then do:
        if poeh.transtype = "rm" then do:
            if icsw.wmfl then do:
                s-binloc = "Bin Loc:".
                for each wmsbp use-index k-wmfifo where
                         wmsbp.cono = g-cono    and
                         wmsbp.whse = icsw.whse and
                         wmsbp.prod = icsw.prod
                no-lock:
                    if wmsbp.qtyonhand <> 0 or
                       wmsbp.qtycommitted <> 0 or
                       wmsbp.qtyreceived <> 0
                    then
                        s-binloc = s-binloc + " " +
                                       string(wmsbp.binloc,"xx/xx/xxx/xxx").
                end.

                if length(trim(s-binloc)) = 8 then
                    s-binloc = s-binloc + " No WM bin loc found.".

            end. /* end of if icsw.wmfl */

            else
                s-binloc = "Bin Loc: " + string(icsw.binloc1,"xx/xx/xxx/xxx").
        end. /* end of if poeh.transtype = "rm" */

        display
            icsp.descrip[1] + " " + icsp.descrip[2] @ s-descrip
            s-binloc
        with frame f-poel.
    end.

    else
        display
            sals.descrip[1] + " " + sals.descrip[2] @ s-descrip
        with frame f-poel.
       down with frame f-poel.

    /* SX55 */
    if (poeh.transtype = "bl" or poeh.transtype = "br") and 
     v-blprint then do:
      run "print_releases".
    end.
     
    /* smaf */
    if  poeh.transtype = "do" /* and poeh.orderaltno <> 0 */ then do:
     find first b-poelo use-index k-poelo where
                b-poelo.cono = g-cono and
                b-poelo.pono = poeh.pono and
                b-poelo.posuf = poeh.posuf and
                b-poelo.lineno = poel.lineno and
                b-poelo.ordertype = "o"     
                no-lock no-error.
          
         if avail b-poelo then do:
     
            find first oeel where oeel.cono = g-cono and
                                  oeel.orderno  = b-poelo.orderaltno and
                                  oeel.lineno   = b-poelo.linealtno and
                                  oeel.shipprod = poel.shipprod
                                  no-lock no-error.
            
     if avail oeel then do:
      v-reqtitle = if oeel.xrefprodty = "c" then " SunSource Customer Prod:"                         else if oeel.xrefprodty = "i" then "Interchange Prod:"
                    else if oeel.xrefprodty = "p" then " Superseded Prod:"
                    else if oeel.xrefprodty = "s" then " Substitute Prod:"
                    else if oeel.xrefprodty = "u" then "    Upgrade Prod:"
                    else "".                                              
          
      if can-do("c,i,p,s,u",oeel.xrefprodty) and oeel.reqprod <> "" then do:
          
            s-prod =  oeel.reqprod.
             
            display
            v-reqtitle
            s-prod
            with frame f-oeelc no-box no-labels.
            down with frame f-oeelc.
        
      end.  /* smaf - cross ref display */
            
    if oeel.xrefprodty ne "c" then do:                            
         
       s-prod = if oeel.reqprod ne "" then oeel.reqprod            
                else poel.shipprod.                                
                                                                         
       {n-icseca.i "first" s-prod ""c"" oeel.custno no-lock}       
                                                              
       if avail icsec then do:                                     
                                                 
            assign                                                    
            s-prod     = icsec.prod                                 
            v-reqtitle = "   SunSource Customer Prod:".                       
            
            display
            v-reqtitle
            s-prod
            with frame f-oeelc no-box no-labels.
            down with frame f-oeelc.
       end.  

    end.
  
   end.
  end.          
  end.                                                                    
                                                                      
/* smaf -  end */

    

    /*d Explode tally/mix components */
    /*tb e15321 11/12/02 kjb; Replaced old tally print logic with the new
        logic contained in tallyprt.ldi */
    {tallyprt.ldi &orderno   = poel.pono
                  &ordersuf  = poel.posuf
                  &lineno    = poel.lineno
                  &ordertype = "'P'"
                  &bundleid  = "''"}

    /*tb 3-2 03/29/00 rgm; Rxserver interface hook for extra vars */
    {poepp1.z99 &before_prodnotes = "*"}
 
 /* Tariff mod 08/25/09 */
 /* Commented out due to buyers not needing tariff info printed on the PO
    das - 11/15/12 */
 /* run zsditariffprt (g-cono,poel.shipprod,poel.whse,"poepp",8).  */
 /* Tariff mod 08/25/09 */
    
    /*d Print the product notes  */
    if poel.nonstockty = "" and
    avail icsp and icsp.notesfl ne "" then
        run notesprt.p(g-cono,"p",icsp.prod,"","poepp",5,1).

    /*d Print the catalog product notes  */
    if poel.nonstockty = "n" and
    avail icsc and icsc.notesfl ne "" then
        run notesprt.p(1,"g",icsc.catalog,"","poepp",5,1).

    /*d Print the PO line item comments  */
    if poel.commentfl = yes then do:
        {w-com.i ""po"" poel.pono poel.posuf poel.lineno yes no-lock}
        if avail com and com.printfl then do i = 1 to 16:
            if com.noteln[i] ne "" then do:
                display com.noteln[i] with frame f-com.
                down with frame f-com.
            end.

        end.

        {w-com.i ""po"" poel.pono poel.posuf poel.lineno no no-lock}
        if avail com and com.printfl then do i = 1 to 16:
            if com.noteln[i] ne "" then do:
                display com.noteln[i] with frame f-com.
                down with frame f-com.
            end.

        end.

    end.

    /*tb e8651 09/29/02 n-jk; Core - Show Core Allocation Detail */
    /*e Display allocation of dirty cores on RM PO's */
    if poeh.transtype = "rm" and
       {icspcore.gvl &prod = poel.shipprod} then do:

        for each iceaa use-index k-return where
            iceaa.cono        = g-cono     and
            iceaa.transty     = "P"        and
            iceaa.retorderno  = poel.pono  and
            iceaa.retordersuf = poel.posuf and
            iceaa.retlineno   = poel.lineno
        no-lock:
            display
                iceaa.orderno
                iceaa.ordersuf
                iceaa.lineno
                iceaa.qty
            with frame f-alloc.
            down with frame f-alloc.

        end. /* for each iceaa */

    end. /* RM PO and dirty core */

    /*tb 13356 04/27/95 mtt; Serial/Lot # doesn't print on PO/RM */
    if poel.nonstockty = "" and avail icsw and poeh.transtype = "rm"
        {poepp1.z99 &before_lot_print = "*"}
    then do:
        assign
            v-nosnlots   = 0
            v-stkqtyord  = poel.stkqtyord.
        /*o Print the serial/lot records */
        {p-oeepp1.i
            &type       = "p"
            &line       = "poel"
            &pref       = "po"
            &seq        = 0
            &stkqtyship = "v-stkqtyord"
            &snlot      = "v-nosnlots"}
         
    end.

    /*o Reset the line item print flag */
    if poel.printfl = yes then
        run poeppu.p(poel.pono, poel.posuf, poel.lineno).

end. /* for each poel */

/*o******Display the order totals ********************/
assign
    s-totinvamt = string(v-totinvamt,"zzzzzzzz9.99-")
    s-totqtyord = if substring (string(v-totqtyord,"999999999.99-"),11,2)
                      = "00" then string(v-totqtyord,"zzzzzzzz9-")
                  else string(v-totqtyord,"zzzzzzzz9.99-").
clear frame f-tot1.
display
    v-linecnt
    s-lit40a
    s-lit40b
    s-totqtyord
    s-lit40c
    s-totinvamt
with frame f-tot1.

if poeh.wodiscamt <> 0 then do:
    clear frame f-tot2.
    assign
        s-title2 = s-lit41a
        s-amt2   = poeh.wodiscamt
        /* SX55 *
        s-type2  = if poeh.wodisctype = yes then g-currsymbol else "%".
        */
        s-type2  = if poeh.wodisctype = yes then "$" else "%".
    display
        s-title2
        s-amt2
        s-type2
    with frame f-tot2.
    down with frame f-tot2.
end.
/* TAH GOV */
assign 
    s-lita49  = 
/*    123456789012345678901234567890123456789012345678901234567890 */
      "SunSource is an EEO employer and federal contractor. The parties" +
      " agree that, if applicable, they will comply with E.O. 11246,"
    s-lita49a = 
     "VEVRAA and the Vocational Rehabilitation Act, and that these laws " +
      "are incorporated herein by reference.".  

        
display s-lita49 s-lita49a with frame f-govstuff.


assign s-lit53 =
   "Buyer may at any time terminate all or any part of undelivered quantiti" +
   "es on any outstanding purchase order and Seller shall ".
assign s-lit53a =
   "immediately stop all work hereunder and cause its suppliers or"  +
   " subcontractors to cease such work as well.  ".
assign s-lit53b =
   "Seller agrees that any termination charges made in consequence shall b" +
   "e limited to direct costs of material and labor ".
assign s-lit53c =
   "incurred (which shall not include lost profits) on items cancelled p" +
   "rior to knowledge of their cancellations.  ".
assign s-lit53d =
   "Seller further agrees to take all steps reasonably possible to mitigate" +
   " such cancellation charges.".
display s-lit53 s-lit53a s-lit53b s-lit53c s-lit53d with frame f-termclause.


assign
    s-lit48a = "Last Page".


