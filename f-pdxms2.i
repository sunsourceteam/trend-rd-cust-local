/****************************************************************************
*                                                                           *
* f-pdxs2.i - Sales Matrix Margin Report Forms (North Format)               *
*                                                                           *
****************************************************************************/

/* Form 2 (North Version) with General Matrix % */
form 
"Cust"                     at  27
"Prod"                     at  39
"Part Number/"             at  45
"Product/"                 at  71
"Contract"                 at  83
"Matrix"                   at 115
"Margin %"                 at 128

"Ship"                     at  14
"Cust"                     at  19
"Prc"                      at  27
"Sls"                      at  33
"Prc"                      at  39
"Product Price Type"       at  45
"Prc Type"                 at  71
"Discount/"                at  83
"Invoiced"                 at 101
"Contract"                 at 115
"Based on"                 at 128
"PD"                       at 142
"PD"                       at 151

"Customer #"               at   1
"To"                       at  14
"Name"                     at  19
"Type"                     at  27
"Rep"                      at  33
"Type"                     at  39
"Description"              at  45
"YTD Sales"                at  71
"Net Price"                at  83
"Margin %"                 at 101
"Margin %"                 at 115
"Gen'l Matrx"              at 128
"Record#"                  at 142
"Level"                    at 151
"Exp Date"                 at 158

"------------"             at   1
"----"                     at  14
"-------"                  at  19
"----"                     at  27
"----"                     at  33
"----"                     at  39
"------------------------" at  45
"----------"               at  71
"---------"                at  83
"------------"             at 101
"------------"             at 115
"------------"             at 128
"-------"                  at 142
"-----"                    at 151
"--------"                 at 158
with frame f-2g-hdr no-underline no-box no-labels page-top down width 178.


form
matrix.customer             at   1
matrix.ship-to              at  14
matrix.name                 at  19
matrix.cprice-type          at  27
matrix.salesrep             at  33
matrix.pprice-type          at  39
matrix.proddesc             at  45
matrix.i-ytd-amt            at  72
matrix.pct-mult             at  83
matrix.type                 at  91
matrix.pd-disctp            at  94
s-imarg                     at 103
s-mmarg                     at 117
s-gmarg                     at 130
matrix.pd-recno             at 142
matrix.pd-level             at 151
matrix.pd-enddt             at 158
with frame f-2g-detail  no-underline no-box no-labels down width 178.

form
"Cust"                     at  27
"Prod"                     at  39
"Part Number/"             at  45
"Product/"                 at  71
"Contract"                 at  83
"Matrix"                   at 115

"Ship"                     at  14
"Cust"                     at  19
"Prc"                      at  27
"Sls"                      at  33
"Prc"                      at  39
"Product Price Type"       at  45
"Prc Type"                 at  71
"Discount/"                at  83
"Invoiced"                 at 101
"Contract"                 at 115
"PD"                       at 128
"PD"                       at 137

"Customer #"               at   1
"To"                       at  14
"Name"                     at  19
"Type"                     at  27
"Rep"                      at  33
"Type"                     at  39
"Description"              at  45
"YTD Sales"                at  71
"Net Price"                at  83
"Margin %"                 at 101
"Margin %"                 at 115
"Record#"                  at 128
"Level"                    at 137
"Exp Date"                 at 144

"------------"             at   1
"----"                     at  14
"-------"                  at  19
"----"                     at  27
"----"                     at  33
"----"                     at  39
"------------------------" at  45
"----------"               at  71
"---------"                at  83
"------------"             at 101
"------------"             at 115
"-------"                  at 128
"-----"                    at 137
"--------"                 at 144
with frame f-2-hdr no-underline no-box no-labels page-top down width 178.


form
matrix.customer             at   1
matrix.ship-to              at  14
matrix.name                 at  19
matrix.cprice-type          at  27
matrix.salesrep             at  33
matrix.pprice-type          at  39
matrix.proddesc             at  45
matrix.i-ytd-amt            at  72
matrix.pct-mult             at  83
matrix.type                 at  91
matrix.pd-disctp            at  94
s-imarg                     at 103
s-mmarg                     at 117
matrix.pd-recno             at 128
matrix.pd-level             at 137
matrix.pd-enddt             at 144
with frame f-2-detail  no-underline no-box no-labels down width 178.
                            
form                                                               
skip (1)
"Customer Totals:"          at  45
s-ytd-isales                at  70
s-ytd-imarg                 at 103
s-ytd-mmarg                 at 117
with frame f-2-subtot no-underline no-box no-labels down width 178.

form                                                               
skip (1)
"Customer Totals:"          at  45
s-ytd-isales                at  70
s-ytd-imarg                 at 103                                 
s-ytd-mmarg                 at 117                                 
s-ytd-gmarg                 at 130                                 
with frame f-2g-subtot no-underline no-box no-labels down width 178.

form                                                                 
skip (1)
"Salesrep Totals:"           at  45
s-ytd-tisales                at  70
s-ytd-timarg                 at 103                                   
s-ytd-tmmarg                 at 117                                   
with frame f-2-salesrep no-underline no-box no-labels down width 178. 

form                                                                 
skip(1)
"Salesrep Totals:"           at  45
s-ytd-tisales                at  70
s-ytd-timarg                 at 103                                   
s-ytd-tmmarg                 at 117                                   
s-ytd-tgmarg                 at 130                                   
with frame f-2g-salesrep no-underline no-box no-labels down width 178.

form                                                                 
skip (1)
"District Totals:"           at  45
s-ytd-tisales                at  70
s-ytd-timarg                 at 103                                   
s-ytd-tmmarg                 at 117                                   
with frame f-2-district no-underline no-box no-labels down width 178. 

form                                                                 
skip(1)
"District Totals:"           at  45
s-ytd-tisales                at  70
s-ytd-timarg                 at 103                                   
s-ytd-tmmarg                 at 117                                   
s-ytd-tgmarg                 at 130                                   
with frame f-2g-district no-underline no-box no-labels down width 178.

form                                                                   
skip (1)
"Region Totals:"             at  45                                    
r-ytd-tisales                at  70                                    
r-ytd-timarg                 at 103                                    
r-ytd-tmmarg                 at 117                                    
with frame f-2-region no-underline no-box no-labels down width 178. 
                                                                       
form    
skip (1)                                       
"Region Totals:"             at  45
r-ytd-tisales                at  70                                    
r-ytd-timarg                 at 103                                    
r-ytd-tmmarg                 at 117                                    
r-ytd-tgmarg                 at 130                                    
with frame f-2g-region no-underline no-box no-labels down width 178.

form                                                                 
"Grand Totals:"              at  45                                  
g-ytd-tisales                at  70                                  
g-ytd-timarg                 at 103                                  
g-ytd-tmmarg                 at 117                                  
with frame f-2-grand no-underline no-box no-labels down width 178. 
                                                                     
form
"Grand Totals:"              at  45
g-ytd-tisales                at  70                                  
g-ytd-timarg                 at 103                                  
g-ytd-tmmarg                 at 117                                  
g-ytd-tgmarg                 at 130                                  
with frame f-2g-grand no-underline no-box no-labels down width 178.

/*
form
skip(1)
"Legend:"                                                at 5
"Price - Value in preceding field is customer price"     at 15
"D%L - Value in preceding field is discount off of list" at 70
"P%L - Price = Value in preceding field * List"          at 130
with frame f-dd-trailer no-box no-labels page-bottom
     down width 178.
*/     
