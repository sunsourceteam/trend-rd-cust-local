/* p-entry.i 1.1 01/03/98 */
/* p-entry.i 1.4 8/11/93 */
/*h*****************************************************************************
  INCLUDE      : p-entry.i
  DESCRIPTION  : Checks for module purchased and journal open, opens the journal
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    12/19/91 kmw; TB# 5039   Hide all no-pause
    08/11/93 rhl; TB# 12558  Batch a keyword in V7
    02/04/02 emc; TB# e9552  Allow multiple journals per operator
    06/02/05 gwk; TB# e18460 Add foreign currency to APEMA
*******************************************************************************/
{&com2}
def var v-glupfl as logical no-undo.
/{&com2}* */

{w-sasa.i no-lock}
{w-sasc.i no-lock}

if not sasa.{&modulefl} then do:

    if {&batchfl} then do:
        {&com}
        {p-rptend.i}
        /{&com}* */
    end.

    if not {&batchfl} then do:
        run err.p(1124).
        pause.
    end.

    assign g-modulenm = "tr" g-menusel = "".
    return.
end.

v-chrecfl = {&chrecfl}.

{w-sasoo.i g-operinits no-lock}

v-updglty = sasoo.{&updglty}.

if not {&batchfl} and (g-jrnlno ne 0 or
can-find(first sasj use-index k-close where
    sasj.cono       = g-cono        and
    sasj.oper2      = g-operinit    and
    sasj.batchfl    = false         and
    sasj.closefl    = no))
then do:

    run err.p(5010).

    if g-jrnlno = 0 then message "Opened by Report Manager".
    else message "Journal # - " g-jrnlno " Process - " g-jrnlproc.

    if g-jrnlproc ne g-currproc or g-jrnlno = 0 then do:
        assign
            g-modulenm = if g-jrnlno = 0 then "pv" else
                         substring(g-jrnlproc,1,2)
            g-menusel  = if g-jrnlno = 0 then "" else
                         substring(g-jrnlproc,3).
        pause.
        return.
    end.

    else if g-perfisc = 0 or g-percal = 0 or g-period = 0 then do:
        run err.p(5009).
        pause.
        run WebJrnl.p(no,yes,{&batchfl},{&postdt},{&period}).
        assign g-modulenm = "pa" g-menusel = "".
        return.
    end.

end.

else if substring(g-ourproc,1,4) ne "oeet" then do:
    run WebJrnl.p(yes,no,{&batchfl},{&postdt},{&period}).

    if g-jrnlno = 0 then do:

        if {&batchfl} then do:
            {&com}
            {p-rptend.i}
            /{&com}* */
        end.

        assign g-modulenm = "pa" g-menusel = "".
        return.
    end.

end.
