define input parameter p-casety as c format "x"    no-undo.
define input parameter p-suffix as c format "x(6)" no-undo.
define input parameter b-postdt as date            no-undo.
define input parameter e-postdt as date            no-undo.
define input parameter b-cono   as int             no-undo.
define input parameter e-cono   as int             no-undo.

{p-rptbeg.i} 
 define buffer b-oeeh for oeeh. 
 define shared temp-table zxhd no-undo
   field cono like oeeh.cono
   field ordno like oeeh.orderno
   field ordsuf like oeeh.ordersuf
   field transtype like oeeh.transtype
   field whse like oeeh.whse
   field invoicedt like oeeh.invoicedt
   field enterdt like oeeh.enterdt
   field reqshipdt like oeeh.reqshipdt
   field promisedt like oeeh.promisedt
   field shipdt like oeeh.shipdt
   field entity as c format "x(10)"
   field pmfl        as logical
   field actfrt      as de
   field frtin       as de 
   field frtout      as de
   field handle      as de
   field exped       as de
   field restk       as de
   field fuelsur     as de
   field pst         as de
   field gst         as de
   field tax         as de
   field countrycd like oeeh.countrycd   
   field invamt      as de
   field ordamt      as de
   field ordcst      as de
   field tendamt     as de
   field disc        as de
   field nolines as int
   field takenby like oeeh.takenby
   field slsrepin like oeeh.slsrepin
   field slsrepout like oeeh.slsrepout
   field custno like oeeh.custno
   field shipto like oeeh.shipto
   field name   like arsc.name
   field shiptoaddr1 as c format "x(30)"
   field shiptoaddr2 as c format "x(30)"
   field shiptocity  like oeeh.shiptocity
   field shiptost    like oeeh.shiptost
   field shiptozip   like oeeh.shiptozip
   field shipvia like oeeh.shipvia
   field refer   like oeeh.refer
   field termstype like oeeh.termstype 
   field custpo like oeeh.custpo
   field completefl as c format "x"
 index izxhd 
       cono
       ordno
       ordsuf
       slsrepout.   

 define var v-txtfile as c format "x(30)" no-undo.
 define var v-message as c format "x(30)" no-undo. /* SX 55 */

 define var v-newfile as c format "x(30)" no-undo.
 define var v-del     as c format "x" no-undo.
 define var v-d       as c format "x" no-undo.

 define var v-null    as c format "x" init " " no-undo.
 define var v-zero    as i format "9" init 0   no-undo. 
 define var v-fyear   as i format "9999"       no-undo.
 define var v-fper    as i format "99"         no-undo.
 define buffer zz-icsd for icsd.
 
 define stream strminvhead.

 {z-fiscperiods.i "new"}

 v-del = chr(09).
 v-d = chr(09).

 v-txtfile = if avail sasc then
               sasc.printdir + "sbinvhd" + p-suffix + ".exp"
             else  
               "sbinvhd" + p-suffix + ".exp".
 {p-infile.i}
 
 
 output stream strminvhead to value(v-txtfile).

 for each sasc use-index k-sasc 
    where sasc.cono >= b-cono and
          sasc.cono <= e-cono no-lock:
          
    for each zz-icsd where zz-icsd.cono = sasc.cono no-lock:
    for each b-oeeh use-index k-invproc
                  where b-oeeh.cono = sasc.cono and
                        b-oeeh.whse = zz-icsd.whse and
                        b-oeeh.invoicedt >= b-postdt and
                        b-oeeh.invoicedt <= e-postdt and
                        b-oeeh.stagecd <> 9 no-lock:
      find zxhd where zxhd.cono = b-oeeh.cono and
                      zxhd.ordno  = b-oeeh.orderno and
                      zxhd.ordsuf = b-oeeh.ordersuf and
                      zxhd.slsrepout = b-oeeh.slsrepout no-lock no-error.
      if not avail zxhd then
        do:
        find arsc where arsc.cono = b-oeeh.cono and
                        arsc.custno = b-oeeh.custno no-lock no-error.
        if b-oeeh.shipto <> "" then
          find arss where arss.cono = b-oeeh.cono and
                          arss.custno = b-oeeh.custno and
                          arss.shipto = b-oeeh.shipto no-lock no-error.


       create zxhd.
       assign zxhd.cono = b-oeeh.cono
              zxhd.ordno = b-oeeh.orderno
              zxhd.ordsuf = b-oeeh.ordersuf
              zxhd.transtype = b-oeeh.transtype
              zxhd.whse = b-oeeh.whse
              zxhd.enterdt   = b-oeeh.enterdt
              zxhd.invoicedt = b-oeeh.invoicedt
              zxhd.reqshipdt = b-oeeh.reqshipdt
              zxhd.promisedt = b-oeeh.promisedt
              zxhd.shipdt = b-oeeh.shipdt
              zxhd.entity = "x"
             zxhd.invamt = (b-oeeh.totlineamt *
                        (if b-oeeh.transtype = "rm" then -1 else 1)
                            - (b-oeeh.wodiscamt + b-oeeh.specdiscamt))
             zxhd.ordamt = (b-oeeh.totordamt *
                        (if b-oeeh.transtype = "rm" then -1 else 1))
             zxhd.ordcst = ((if b-oeeh.transtype = "cr" then
                               0
                            else
                              b-oeeh.totcost) *
                          (if b-oeeh.transtype = "rm" then -1 else 1))
             zxhd.tendamt = (b-oeeh.tottendamt *
                        (if b-oeeh.transtype = "rm" then -1 else 1))
             zxhd.nolines = 0
             zxhd.takenby = b-oeeh.takenby
             zxhd.slsrepin = b-oeeh.slsrepin
             zxhd.slsrepout = b-oeeh.slsrepout
             zxhd.custno = b-oeeh.custno
             zxhd.shipto = b-oeeh.shipto
             zxhd.shipvia = b-oeeh.shipviaty
             zxhd.termstype = b-oeeh.termstype
             zxhd.custpo    = b-oeeh.custpo
             zxhd.completefl = "x"
             zxhd.name   = if avail arss and zxhd.shipto <> ""then
                              arss.name
                           else
                           if avail arsc then
                             arsc.name
                           else
                             " "
             zxhd.shiptoaddr1 = b-oeeh.shiptoaddr[1]
             zxhd.shiptoaddr2 = b-oeeh.shiptoaddr[2]
             zxhd.shiptocity  = b-oeeh.shiptocity
             zxhd.shiptost    = b-oeeh.shiptost
             zxhd.shiptozip   = b-oeeh.shiptozip
             zxhd.countrycd   = if arsc.divno = 40 then
                                 "CA"
                                else
                                if b-oeeh.countrycd <> " " then
                                   b-oeeh.countrycd
                                else
                                 "US"
             zxhd.refer       = b-oeeh.refer.
        assign     
             zxhd.frtin = ((if b-oeeh.addonno[1] = 1 then
                           b-oeeh.addonnet[1] 
                         else
                           0)
                        + 
                        (if b-oeeh.addonno[2] = 1 then
                           b-oeeh.addonnet[2] 
                         else
                           0)
                        +
                        (if b-oeeh.addonno[3] = 1 then
                           b-oeeh.addonnet[3] 
                         else
                           0)
                        +
                        (if b-oeeh.addonno[4] = 1 then
                           b-oeeh.addonnet[4] 
                         else
                           0))
           zxhd.frtout = ((if b-oeeh.addonno[1] = 2 then
                           b-oeeh.addonnet[1] 
                         else
                           0)
                         + 
                        (if b-oeeh.addonno[2] = 2 then
                           b-oeeh.addonnet[2] 
                         else
                           0)
                        +
                        (if b-oeeh.addonno[3] = 2 then
                           b-oeeh.addonnet[3] 
                         else
                           0)
                        +
                        (if b-oeeh.addonno[4] = 2 then
                           b-oeeh.addonnet[4] 
                          else
                          0))
          zxhd.exped = ((if b-oeeh.addonno[1] = 4 then
                           b-oeeh.addonnet[1] 
                         else
                           0)
                        + 
                        (if b-oeeh.addonno[2] = 4 then
                           b-oeeh.addonnet[2] 
                         else
                           0)
                        +
                        (if b-oeeh.addonno[3] = 4 then
                           b-oeeh.addonnet[3] 
                         else
                           0)
                        +
                        (if b-oeeh.addonno[4] = 4 then
                           b-oeeh.addonnet[4] 
                         else
                           0))
        zxhd.handle = ((if can-do("3,5,6,7",string(b-oeeh.addonno[1],"9")) then                           b-oeeh.addonnet[1] 
                       else
                         0)
                      + 
                      (if can-do("3,5,6,7",string(b-oeeh.addonno[2],"9")) then
                         b-oeeh.addonnet[2] 
                       else
                         0)
                      +
                      (if can-do("3,5,6,7",string(b-oeeh.addonno[3],"9")) then
                         b-oeeh.addonnet[3] 
                       else
                         0)
                      +
                      (if can-do("3,5,6,7",string(b-oeeh.addonno[4],"9")) then
                         b-oeeh.addonnet[4] 
                       else
                         0))

        zxhd.restk = ((if b-oeeh.addonno[1] = 8 then
                         b-oeeh.addonnet[1] 
                       else
                         0)
                      + 
                      (if b-oeeh.addonno[2] = 8 then
                         b-oeeh.addonnet[2] 
                       else
                         0)
                      +
                      (if b-oeeh.addonno[3] = 8 then
                         b-oeeh.addonnet[3] 
                       else
                         0)
                      +
                      (if b-oeeh.addonno[4] = 8 then
                         b-oeeh.addonnet[4] 
                       else
                         0))
        zxhd.fuelsur = ((if b-oeeh.addonno[1] = 9 then
                         b-oeeh.addonnet[1] 
                       else
                         0)
                      + 
                      (if b-oeeh.addonno[2] = 9 then
                         b-oeeh.addonnet[2] 
                       else
                         0)
                      +
                      (if b-oeeh.addonno[3] = 9 then
                         b-oeeh.addonnet[3] 
                       else
                         0)
                      +
                      (if b-oeeh.addonno[4] = 9 then
                         b-oeeh.addonnet[4] 
                       else
                         0)).

         if zxhd.countrycd <> "ca" then
           zxhd.tax = b-oeeh.taxamt[1] + 
                      b-oeeh.taxamt[2] +
                      b-oeeh.taxamt[3] +
                      b-oeeh.taxamt[4].
         else
           assign zxhd.pst = b-oeeh.taxamt[1]
                  zxhd.gst = b-oeeh.taxamt[4].                 
         if b-oeeh.transtype = "rm" then
            assign zxhd.frtin =  zxhd.frtin * -1                                
                   zxhd.frtout = zxhd.frtout * -1
                   zxhd.exped   = zxhd.exped * -1
                   zxhd.handle  = zxhd.handle * -1
                   zxhd.restk    = zxhd.restk * -1              
                   zxhd.fuelsur = zxhd.fuelsur * -1
                   zxhd.tax     = zxhd.tax * -1
                   zxhd.gst     = zxhd.gst * -1
                   zxhd.pst    = zxhd.pst * -1.
        assign zxhd.restk = zxhd.restk + b-oeeh.totrestkamt.                   



                                          
      end.                
    end.
  end.
  end.
 
 
 
 
 
 
 
 for each zxhd no-lock:
  assign z-paramdate = zxhd.invoicedt.
  run getfiscfmdate.
  if z-paramfisc <> 9999 then
    assign v-fyear = year(zfisc.zfiscdt)
           v-fper  = month(zfisc.zfiscdt).
 
 
 
  if p-casety = "u" then
    do:
    put stream strminvhead unformatted
      {zsdiinsaex3.lpr
         &type = "b"
         &buffoeel = "b-"
         &buffoeeh = "b-"
         &case     = "caps"}.
    end.
  else       
  if p-casety = "l" then
    do:
    put stream strminvhead unformatted
      {zsdiinsaex3.lpr
         &type = "b"
         &buffoeel = "b-"
         &buffoeeh = "b-"
         &case     = "lc"}.
    end.
  else
    do:
    put stream strminvhead unformatted
      {zsdiinsaex3.lpr
         &type = "b"
         &buffoeel = "b-"
         &buffoeeh = "b-"
         &case     = " "}.
    end.
          

end.

