/* apaf.lva  9.0 12/16/98 */
/*h*****************************************************************************
  INCLUDE      : apaf.lva 
  DESCRIPTION  : Shared Variables used by 1099 Magnetic Media
  USED ONCE?   : no
  AUTHOR       : gfk
  DATE WRITTEN :
  CHANGES MADE : 
    12/16/98 gfk; TB# 24465 1099 Magnetic Media Processing Added
    08/31/01 lbr; TB# e9792 Added no-undo to temp-table definitions
    07/03/01 gwk; TB# e8953 New boxes on 1099 Misc form
    12/15/02 gwk; TB# e15761 Changes for mag media per IRS Pub 1220     
    09/30/03 gwk; TB# e15762 Changes for mag media for 2003 per IRS Bulletin
        2003-30 which contains revised Pub 1220 
*******************************************************************************/

/*Shared Variables*/
def {1} shared var v-sascmagname    as c format "x(20)"        no-undo.
def {1} shared var v-tcc            as c format "x(05)"        no-undo.
def {1} shared var v-tname          as c format "x(40)"        no-undo.
def {1} shared var v-tcomp          as c format "x(40)"        no-undo.
def {1} shared var v-tcomp2         as c format "x(40)"        no-undo.
def {1} shared var v-taddress       as c format "x(40)"        no-undo.
def {1} shared var v-tcity          as c format "x(40)"        no-undo.
def {1} shared var v-tstate         as c format "x(02)"        no-undo.
def {1} shared var v-tzip           as c format "x(09)"        no-undo.
def {1} shared var v-tcontact       as c format "x(40)"        no-undo.
def {1} shared var v-tphone         as c format "x(15)"        no-undo.
def {1} shared var v-testfile       as c format "x(01)"        no-undo.
def {1} shared var v-foreign        as c format "x(01)"        no-undo.
def {1} shared var v-original       as c format "x(01)"        no-undo.
def {1} shared var v-correction     as c format "x(01)"        no-undo.
def {1} shared var v-replacement    as c format "x(01)"        no-undo.
def {1} shared var v-combined       as c format "x(01)"        no-undo.
def {1} shared var v-tapefile       as c format "x(02)"        no-undo.
def {1} shared var v-taxyear        as c format "x(04)"        no-undo.
def {1} shared var v-fedtaxid-t     like sasc.fedtaxid         no-undo.
def {1} shared var v-email          as c format "x(35)"        no-undo.
def {1} shared var v-replacementfilename
                                    as c format "x(15)"        no-undo.
def {1} shared var v-mediano        as c format "x(6)"         no-undo.

def var formtypes                   as c format "x(20)"
                init "4,B,1,F,6,A,D,7,9,S"                     no-undo.
def var formcodes                   as c format "x(40)"
                init  "A,B,DIV,G,INT,MISC,OID,PATR,R,S"        no-undo.
/*tb e8953 7/11/01 gwk; boxes 11 and 12 are no longer used on the 1099-misc 
    form.  Box 13 is now used for Excess golden parachute payments.  This
    gets amount code "B".  Box 14 is no used for Attorney compensation.  This
    gets amount code "C". */
def var amountcodes                 as c format "x(27)"
                init "1,2,3,4,5,6,7,8,9,A,X,X,B,C"                 no-undo.
def var v-valid-num                 as char init "1234567890"  no-undo.
def var v-valid-chr                 as char
                init "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-&"  no-undo.
def var v-name-exclude              as char format "x(60)"
      init "mr ,mr. ,& ,mrs ,mrs. ,ms ,ms. ,dr ,dr. ,estate ,of ".

def var v-reportid                  like sassr.reportid        no-undo.
def var v-openprfl                  like sassr.openprfl        no-undo.
def var v-openprno                  like sassr.openprno        no-undo.
def var v-continue                  as log                     no-undo.
def var v-magname                   as c format "x(7)"         no-undo.
def var v-firsttime                 as log                     no-undo.
def var v-form-type                 as c format "x(01)"        no-undo.
def var v-count-c                   as i format "99999999"     no-undo.
def var v-count-b                   as i format "99999999"     no-undo.
def var v-count-a                   as i format "99999999"     no-undo.
def var v-fedtaxid-a                like sasc.fedtaxid         no-undo.
def var v-fedtaxid                  like sasc.fedtaxid         no-undo.
def var v-tmp-zip                   as c format "x(09)"        no-undo.
def var v-tin-type                  as c format "x(01)"        no-undo.
def var v-tmp-str                   as c format "x(04)"        no-undo.
def var v-tmp-chr                   as c format "x(01)"        no-undo.
def var v-name-ctrl                 as c format "x(04)"        no-undo.
def var v-amount                    as dec format ">>>>>>>>9.99-"
                                       initial 0               no-undo.
def var v-amount-code               as c format "x(14)"        no-undo. 
def var v-read-ln                   as c format "x(750)"       no-undo.
def var v-tot-amt                   as de format "999999999999999999"
                                                extent 14      no-undo.
def var v-loop                      as int                     no-undo.
def var v-string                    as c format "x(40)"        no-undo.

def stream 1099M.

def {1} shared temp-table magmedia no-undo
    field  payer-id     as c format "x(09)"
    field  rec-type     as c format "x"
    field  form-type    like v-form-type
    field  amount-code  like v-amount-code 
    field  name-ctrl    like v-name-ctrl   
    field  name         as c format "x(40)"
    field  name2        as c format "x(40)"
    field  address      as c format "x(40)"
    field  city         as c format "x(40)"
    field  state        as c format "x(02)"
    field  zip          as c format "x(09)"
    field  phone        as c format "x(15)"
    field  foreignfl    as c  format "x"        
    field  tin-type     like v-tin-type 
    field  payee-id     as c format "x(09)"
    field  account      as c format "x(20)"
    field  fed1099box   as c format "x(02)"
    field  cdate        as c format "x(08)"
    field  amount       as de format "999999999999" extent 14
  
    index  k-magmedia is primary
           payer-id
           rec-type
           form-type 
           account.

form                                                                 
    skip(1)                                                           
    " A file already exists with the name: " v-sascmagname 
    skip(1)                                                 
    " Do you want to continue to add records to this file?    "
    v-continue                                                        
    skip(1)                                                           
    " If you answer Y:  This file will be APPENDED with selected   "  
    skip                                                              
    "                   1099 records and Transmitter Record Rewritten. "
    skip(1)                                                           
    " If you answer N:  The OLD 1099 file will be REMOVED, and a new "
    skip                                                                
    "                   file with a Transmitter Record will be opened"  
    skip                                                                
    with frame dup-name-form attr-space centered row 5                
       no-labels title " DUPLICATE MAG MEDIA FILE NAME ".                

form /* 1099 Magnetic Media*/
    magmedia.name          at 1          label "Name"
    magmedia.address       at 42         label "Address"
    magmedia.phone    format "x(12)"     label "Phone"
    magmedia.payer-id                    label "Payer-Id"
    magmedia.name-ctrl                   label "Control"
    v-amount               at 116        label "  Amount "
    skip
    magmedia.name2         at 1          no-label
    magmedia.city          at 42         label "City   "
    magmedia.account  format "x(12)"     label "Account"
    magmedia.payee-id                    label "Payee-Id"
    magmedia.form-type format "x(10)"    label "Form "
    magmedia.fed1099box    at 120        label "1099 Box"
with frame f-apaf1099 no-box down width 132.

form /* 1099 Magnetic Media*/
    "TOTAL COUNT FOR THIS PAYER & FORM " at 42
    v-count-c    format "zzzzzzz9"
    v-string     format "x(10)"          at 90
    v-amount                             at 116
with frame t-apaf1099 no-box no-labels down width 132.
