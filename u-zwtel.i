if {k-func6.i} then
  do:
  assign g-modulenm = ""
         g-title = "Order Entry Inquiry - Orders".
  run oeio.p.
   put screen row 21 col 3   
    color message
  " F6-Order Detail   F7-W.T. Detail   F8-Delete Tie                        ".     put screen row 8 col 1  color message
  " P.O.      PO   Seq   Item                       Tied      Tied  Tied Kit".
   put screen row 9 col 1 color message
  " Number    Line       Number                     Order     Line     Seq  ".
  if {k-jump.i} or {k-select.i} or not
     (g-modulenm = "" or g-modulenm = "pa") then 
    leave main.
  hide message no-pause.
  next.
  end.
else
if {k-func7.i} then
  do:
  assign g-modulenm = ""
         g-title = "Warehouse Transfer Entry Inquiry - Orders".
   run wtio.p.
   put screen row 21 col 3   
     color message
  " F6-Order Detail   F7-W.T. Detail   F8-Delete Tie                        ".      put screen row 8 col 1  color message
  " W.T.      PO   Seq   Item                       Tied      Tied  Tied Kit".
    put screen row 9 col 1 color message
  " Number    Line       Number                     Order     Line     Seq  ".
    if {k-jump.i} or {k-select.i} or not
     (g-modulenm = "" or g-modulenm = "pa") then 
    leave main.
  hide message no-pause.
  next.

  end.
else
if {k-func8.i} then
  do:
  assign g-wtno = dec(substring(o-frame-value,1,7))
         g-wtsuf = int(substring(o-frame-value,9,2))
         g-wtlineno = int(substring(o-frame-value,12,3))
         v-seqno = int(substring(o-frame-value,16,3)).

        
  find wtelo where wtelo.cono = g-cono and
                   wtelo.wtno = g-wtno and
                   wtelo.wtsuf = g-wtsuf and
                   wtelo.lineno = g-wtlineno and
                   wtelo.seqno = v-seqno no-lock no-error.


  
if avail wtelo then
  do:
  find wtel where wtel.cono = g-cono and
                  wtel.wtno = g-wtno and
                  wtel.wtsuf = g-wtsuf  and 
                  wtel.lineno = wtelo.lineno no-lock no-error.
  if avail wtel then 
     assign z-shipprod = wtel.shipprod.
  else
     assign z-shipprod = "". 
   assign z-orderno =  wtelo.orderaltno
          z-ordersuf = wtelo.orderaltsuf
          z-lineno   = wtelo.linealtno
          z-seqno    = wtelo.seqaltno.
  end.       
else
  do:
  assign z-orderno =  0
         z-ordersuf = 0
         z-lineno   = 0
         z-shipprod = ""
         z-seqno    = 0.
  end.       

  z-answer = no.
  display 
    g-wtno
    g-wtsuf
    g-wtlineno
    v-seqno
    z-shipprod
    z-orderno
    z-ordersuf
    z-lineno
    z-seqno
    with frame f-wttied.


  do on endkey undo, leave:
    update z-answer with frame f-wttied.
  end.
  hide frame f-wttied no-pause.
  
  if z-answer = yes then
    do:
            
    find wtelo where wtelo.cono = g-cono and
                  wtelo.wtno = g-wtno and
                  wtelo.wtsuf = g-wtsuf and
                  wtelo.lineno = g-wtlineno and
                  wtelo.seqno = v-seqno no-error.

    find wtel  where wtel.cono = g-cono and
                  wtel.wtno = g-wtno and
                  wtel.wtsuf = g-wtsuf and
                  wtel.lineno = g-wtlineno 
                   no-error.


   
    if avail wtelo and avail wtel then
      do:
      find oeeh where oeeh.cono = g-cono  and
                      oeeh.orderno = wtelo.orderaltno and
                      oeeh.ordersuf = wtelo.orderaltsuf no-error.
      if avail oeeh then
        do:              
        if oeeh.stagecd = 9 or 
           oeeh.stagecd ge 5 then
          do:
          run err.p (5687).
          next.
          end. 
          
        
        
        
        if wtelo.seqaltno = 0 then
          do:
          find oeel where oeel.cono = g-cono  and
                        oeel.orderno = wtelo.orderaltno and
                        oeel.ordersuf = wtelo.orderaltsuf and
                        oeel.lineno = wtelo.linealtno  no-error.
          if avail oeel then
            do:              
            assign oeel.ordertype = " "
                   oeel.orderaltno = 0
                   oeel.linealtno = 0.
            delete wtelo.
            end.       

        else if wtelo.seqaltno ne 0 then
          do:
          find oeelk where oeelk.cono = g-cono  and
                        oeelk.orderno = wtelo.orderaltno and
                        oeelk.ordersuf = wtelo.orderaltsuf and
                        oeelk.lineno = wtelo.linealtno and
                        oeelk.seqno  = wtelo.seqaltno  no-error.
          if avail oeel then
            do:              
            assign oeelk.ordertype = " "
                   oeelk.orderaltno = 0
                   oeelk.linealtno = 0.
            delete wtelo.
            end.       
          end.
        end.
      end.
    end.
  end.
end.


