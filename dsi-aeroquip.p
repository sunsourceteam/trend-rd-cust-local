/***************************************************************************
 PROGRAM NAME: dsi-aeroquip.p
  DESCRIPTION: This program will create the DSI file to upload to MNet's
                Aeroquip network. (AEROQUIP DSI EXTRACT)
 DATE WRITTEN:
       AUTHOR: Dave Sivak
     MODIFIED: 10/14/99 - Carl Graul - changed the formatting in the output
                    file
             : 11/12/99 - Carl Graul added "," to .csv records
             : 11/22/99 - Carl Graul - default distributor to SUNSRCE"
                          until mnet is fixed
***************************************************************************/

def shared var fl-done as logical.
def var wprod as c format "x(16)" no-undo.
def var quantity as i format "9999" no-undo.
def var distributor as c format "x(8)" no-undo.
def var discount as i format "99" no-undo.
def var price as c format "x(1)" no-undo.
def var todays_date as date format "99/99/99" no-undo.
def var wusagerate as de format "99999.99" no-undo.
def var count as i format "999" no-undo.
def var vendornum as i format "999999999999" no-undo.
def var warehouse as c format "x(4)" no-undo.
def var distribcode as c format "x(8)" no-undo.

def var out-csv as char format "x(60)" no-undo.
def var out-aer as char format "x(60)" no-undo.
assign out-csv = os-getenv("HOME") + "/aeroquip.csv".
assign out-aer = os-getenv("HOME") + "/sunsrce.aer".

def stream str1.

output stream str1 to value(out-csv).
run "120-load-csv".
output stream str1 close.

input from value(out-csv).
output stream str1 to value(out-aer).

assign todays_date = TODAY.

repeat:
  import delimiter ","
     vendornum warehouse distribcode.
   for each icsw use-index k-vendor where icsw.cono = 1 and 
                                          icsw.whse = warehouse and
                                          icsw.arpvendno = vendornum no-lock.
                     
      quantity = (qtyonhand - qtyreservd - qtycommit).  /* net available */
      if quantity > 0 then do:
         distributor = distribcode.
/* until distributor address is fixed at mnet */
         distributor = "SUNSRCE".         
         
         run "100-discount".
         wprod = substring(icsw.prod,1,16).
         run "110-write".
       end. /* if quantity > 0  */
   end. /* for each icsw */
end. /* repeat */
input close.
output  stream str1 close.
assign fl-done = yes.
                                           
procedure 100-discount:
/*    DETERMINE DISCOUNT     */
wusagerate = 0.
if icsw.usagerate > 0 then
    wusagerate = quantity / icsw.usagerate.
if wusagerate > 8.00 or icsw.usagerate = 0 then
    discount = 77.
else
    discount = 74.
end procedure. /* 100-discount */

procedure 110-write:
put stream str1 UNFORMATTED 
    trim(wprod) "|"
    trim(distributor) "|" 
    quantity "|"
    discount "|"
    trim(price) "|"
    todays_date 
    skip.
end procedure. /* 110-write */

procedure 120-load-csv:
put stream str1 UNFORMATTED 
    "9035000,scr,SUNDENV,"
    skip
    "9035000,dnvr,SUNDENV,"
    skip
    "9035000,dclv,SUNDENV,"
    skip
    "9035000,ddel,SUNDENV,"
    skip
    "9035000,dsth,SUNDENV,"
    skip
    "9035000,dslc,SUNDENV,".
end procedure. /* 120-load-csv */


