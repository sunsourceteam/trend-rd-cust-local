/* SX55 */
/* p-infile.i 1.1 01/03/98 */
/* p-infile.i 1.1 12/13/94 */
/*h****************************************************************************
  INCLUDE      : p-infile.i
  DESCRIPTION  : Check to see if the file exists and rename it if it does
  USED ONCE?   : rhl
  AUTHOR       : 12/13/94
  DATE WRITTEN :
  CHANGES MADE :
    06/24/96 tdd; TB# 20839 Established baseline
    01/16/03 lsl; TB# 15997 change so that file rename works for both the
             WIN32 and UNIX operating systems.
                 *******************************************************************************/
v-message = "*** " + v-txtfile + " was renamed".

/* check to see if the file exists */
if search (v-txtfile) <> ? then do:
  v-newfile = if index(v-txtfile,".exp":u) > 0 then
                substring(v-txtfile,1,(index(v-txtfile,".exp":u) - 1))
                           + "old.exp":u
              else v-txtfile + ".old":u.
  if opsys = "UNIX":u then
    unix silent mv value(v-txtfile) value( v-newfile).
/*      must use dos copy followed by del as the dos rename
        does not work across directories    */
  else if opsys = "WIN32":u then do:
    dos silent copy value(v-txtfile) value(v-newfile).
    dos silent del value(v-txtfile).
  end.
  else v-message = "Unsupported OS Error!".

  display v-message format "x(60)" no-label
    with frame f-duplicate.
  down with frame f-duplicate.
 end.

