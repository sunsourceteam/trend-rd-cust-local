

/{&user_temptable}*/

define {&sharetype} temp-table xxrepxx no-undo

  field slsrep like smsn.slsrep
  field region as char format "x"
  field district as char format "xxx"
  field sales    as dec extent 12
  field year     as int

  index xx
     slsrep.

/{&user_temptable}*/

/{&user_drpttable}*/
/* Use to add fields to the report table */

/{&user_drpttable}*/



/{&user_forms}*/
form header
  "~015 " at 1
  with frame fx-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: PORZX"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 168
  page-number(xpcd)    at 173 format ">>>9"
  "~015"               at 177
         "Po Exepdite Report" at 71
         "~015"              at 177
   with frame fx-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.

form header
  "~015 " at 1
  with frame f-trn1 page-top.
form header
  rpt-dt           at 1 space
  rpt-dow             space
  rpt-time            space(3)
  "Company:"          space
  rpt-cono            space
  "Function: PORZX"   space
  "Operator:"         space
  rpt-user            space
  "page:"              at 168
  page-number          at 173 format ">>>9"
  "~015"              at 177
  "Po Expedite Report" at 72
  "~015"              at 177
   with frame f-trn2 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.

form header 
   "~015"                    at 177
  with frame f-trn3 down NO-BOX NO-LABELS no-underline
            WIDTH 178 page-top.

form
  v-final                at 1
  v-astrik               at 2   format "x(5)"
  v-summary-lit2         at 7   format "x(27)"
  v-amt1           at 34  format ">>>,>>>,>>9-"
  v-amt2           at 47  format ">>>,>>>,>>9-"
  v-amt3           at 60  format ">>>,>>>,>>9-"
  v-amt4           at 73  format ">>>,>>>,>>9-" 
  v-amt5           at 86  format ">>>,>>>,>>9-"
  v-amt6           at 99  format ">>>,>>>,>>9-"
  "~015"           at 178
with frame f-tot width 178
 no-box no-labels.  

form 
  "~015"           at 1
with frame f-skip width 178 no-box no-labels.  
/{&user_forms}*/


/{&user_extractrun}*/

 run sapb_vars.
 run extract_data.

/{&user_extractrun}*/

/{&user_exportstatDWheaders}*/
/{&user_exportstatDWheaders}*/

/{&user_exportstatheaders}*/
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  
  assign export_rec = export_rec + v-del +
                      "Period 1" + v-del +
                      "Period 2" + v-del   +
                      "Period 3" + v-del   +
                      "Period 4" + v-del   +
                      "Period 5" + v-del   +
                      "Period 6 " + v-del   +
                      " " + v-del +
                      " ".
/{&user_exportstatheaders}*/


/{&user_foreach}*/
for each xxrepxx no-lock:
/{&user_foreach}*/


/{&user_drptassign}*/
 /* If you add fields to the dprt table this is where you assign them */

/{&user_drptassign}*/

/{&user_B4endloop}*/
  /* this is before the end of the for each loop to feed the data BUILDREC */

/{&user_B4endloop}*/





/{&user_viewheadframes}*/
/* Since frames are unique to each program you need to put your
   views and hides here with your names */

if not p-exportl then
  do:
  if not x-stream then
    do:
    hide frame f-trn1.
    hide frame f-trn2.
    hide frame f-trn3.
    hide frame f-trn4.
    hide frame f-trn5.
    end.
  else
   do:
    hide stream xpcd frame f-trn1.
    hide stream xpcd frame fx-trn2.
    hide stream xpcd frame f-trn3.
    hide stream xpcd frame f-trn4.
    hide stream xpcd frame f-trn5.
    end.
  
  end.


if not p-exportl  then
  do:
  if not x-stream then
    do:
    page.
    view frame f-trn1.
    view frame f-trn2.
    view frame f-trn3.
    end.
  else  
    do:
    page stream xpcd.
    view stream xpcd frame f-trn1.
    view stream xpcd frame fx-trn2.
    view stream xpcd frame f-trn3.
    end.
   
  end.

/{&user_viewheadframes}*/

/{&user_drptloop}*/
/{&user_drptloop}*/


/{&user_detailprint}*/
   /* Comment this out if you are not using detail 'X' option  */
   if p-detailprt and 
      entry(u-entitys,drpt.sortmystery,v-delim) <> 
      fill("~~",length(entry(u-entitys,drpt.sortmystery,v-delim))) then 
     do:
     /* this code skips the all other lines for printing detail */
     
     /* next line would be your detail print procedure */
     p-detailprt = p-detailprt.
     end.


/{&user_detailprint}*/

/{&user_finalprint}*/
if not p-exportl then
 do:
 if not x-stream then 
   do:
   display 
    v-final
    v-astrik
    v-summary-lit2
/* PTD */
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts3[u-entitys + 1]   @ v-amt2
    with frame f-tot.
  
   down with frame f-tot.
   end.
 else
   do:
   display stream xpcd
    v-final
    v-astrik
    v-summary-lit2
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts3[u-entitys + 1]   @ v-amt2
    with frame f-tot.
  
   down stream xpcd with frame f-tot.
   end.
      
 end.
else
  do:
  assign export_rec =  v-astrik.
  do v-inx4 = 1 to u-entitys:
    if v-totalup[v-inx4] = "n" then
       next.
    if v-inx4 = 1 then
      v-summary-lit2 = "Final".
    else
      v-summary-lit2 = "".

    if v-cells[v-inx4]= "2" then
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.

      assign export_rec = export_rec + v-summary-lit2 + v-del + " ".
     
      end.
    else
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec +  v-summary-lit2.
      end.
  end.
  assign export_rec = export_rec + v-del +
      string(t-amounts1[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts2[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts3[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts4[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts5[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts6[u-entitys + 1],"->>>>>>>>9").
    
  assign export_rec = export_rec + chr(13).    
  put stream expt unformatted export_rec.
  end.
/{&user_finalprint}*/


/{&user_summaryframeprint}*/
  
    if not x-stream then
      do:
      display 
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        t-amounts3[v-inx2]   @ v-amt3
        t-amounts4[v-inx2]   @ v-amt4
        t-amounts5[v-inx2]   @ v-amt5
        t-amounts6[v-inx2]   @ v-amt6
        with frame f-tot.
      down with frame f-tot.
      end.
    else
      do:
      display stream xpcd
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        t-amounts3[v-inx2]   @ v-amt3
        t-amounts4[v-inx2]   @ v-amt4
        t-amounts5[v-inx2]   @ v-amt5
        t-amounts6[v-inx2]   @ v-amt6
        with frame f-tot.
      down stream xpcd with frame f-tot.
      end.
/{&user_summaryframeprint}*/



/{&user_summaryputexport}*/

 
/* PTD */
    assign export_rec = v-astrik + v-del + export_rec.
    assign export_rec = export_rec + v-del +
      string(t-amounts1[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts2[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts3[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts4[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts5[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts6[v-inx2],"->>>>>>>>9").
      
/{&user_summaryputexport}*/

/{&user_detailputexport}*/
/{&user_detailputexport}*/



/{&user_procedures}*/



procedure sapb_vars:

  assign
    b-slsrep     = sapb.rangebeg[1]
    e-slsrep     = sapb.rangeend[1]
    p-detailprt  = if sapb.optvalue[2] = "Yes" then yes else no
    p-detail     = if p-detailprt then "d" else "s".
    
  run formatoptions.
end.


/* ------------------------------------------------------------------------- */
procedure formatoptions:
/* ------------------------------------------------------------------------- */

   
   
  assign p-portrait = true.
  /* if false then the report will print in LANDSCAPE 178 character */
  
  if sapb.optvalue[1] > "0" and sapb.optvalue[1] ne "99" then 
    do:
    p-optiontype = " ".

    find notes where notes.cono = g-cono and
                     notes.notestype = "zz" and
                     notes.primarykey = "arrg" and  /* tbxr program */
                     notes.secondarykey = string(p-format) no-lock no-error.
    if not avail notes then
      do:
      display "Format is not valid cannot process request".
      assign p-optiontype = "r"
             p-sorttype   = ">,"
             p-totaltype  = "l"
             p-summcounts = "a".
      return.
      end.               
   
    assign p-optiontype = notes.noteln[1]
           p-sorttype   = notes.noteln[2]
           p-totaltype  = notes.noteln[3]
           p-summcounts = notes.noteln[4]
           p-export     = "    "
           p-register   = notes.noteln[5]
           p-registerex = notes.noteln[6].
    end.     
  else
  if sapb.optvalue[1] = "99" then 
    do:
    assign p-register   = ""
           p-registerex = "".
    run reportopts(input sapb.user5, 
                   input-output p-optiontype,
                   input-output p-sorttype,  
                   input-output p-totaltype,
                   input-output p-summcounts,
                   input-output p-register,
                   input-output p-regval).

    end.

 end.
   
 

procedure extract_data:
  for each smss where
           smss.cono = g-cono and
           smss.slsreptype = true and
           smss.yr = 03 no-lock:
  
    find smsn where smsn.cono = g-cono and
                    smsn.slsrep = smss.slsrep no-lock no-error.
    
    create xxrepxx.
  
    assign xxrepxx.slsrep      = smss.slsrep
           xxrepxx.region      = substring(smsn.mgr,1,1)
           xxrepxx.district    = substring(smsn.mgr,2,3)
           xxrepxx.year        = smss.yr.
    do v-inx = 1 to 12:
      assign xxrepxx.sales[v-inx]   = smss.sales[v-inx].
    end.
  end.
end.
  
/{&user_procedures}*/



/* TBXR F6 CodeGen Includes from here down are CodeGen Generated  */ 



/{&user_optiontype}*/
/{&user_optiontype}*/
   

/{&user_registertype}*/
/{&user_registertype}*/
 

/{&user_exportvarheaders1}*/
/{&user_exportvarheaders1}*/


/{&user_exportvarheaders2}*/
/{&user_exportvarheaders2}*/


/{&user_loadtokens}*/
/{&user_loadtokens}*/


/{&user_totaladd}*/
/{&user_totaladd}*/


/{&user_numbertotals}*/
/{&user_numbertotals}*/



/{&user_summaryloadprint}*/
/{&user_summaryloadprint}*/



/{&user_summaryloadexport}*/
/{&user_summaryloadexport}*/


/* TBXR F6 CodeGen End of Includes that are CodeGen Generated  */ 






