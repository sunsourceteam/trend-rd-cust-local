assign
  {&NewValue} = replace({&NewValue}, "~~":u ," ")
  {&NewValue} = replace({&Newvalue}, "`":u , " ")
  {&NewValue} = replace({&NewValue}, "@":u , " ")
  {&NewValue} = replace({&NewValue}, "#":u , " ")
  {&NewValue} = replace({&NewValue}, "$":u , " ")
  {&NewValue} = replace({&NewValue}, "%":u , "_")     
  {&NewValue} = replace({&NewValue}, "(":u , " ")
  {&NewValue} = replace({&NewValue}, ")":u , " ")
  {&NewValue} = replace({&NewValue}, "-":u , " ")
  {&NewValue} = replace({&NewValue}, "+":u , " ")
  {&NewValue} = replace({&NewValue}, "=":u , " ")
  {&NewValue} = replace({&NewValue}, "~{":u ," ")
  {&NewValue} = replace({&NewValue}, "}":u , " ")
  {&NewValue} = replace({&NewValue}, "[":u , " ")
  {&NewValue} = replace({&NewValue}, "]":u , " ")
  {&NewValue} = replace({&NewValue}, "~\":u , " ")
  {&NewValue} = replace({&NewValue}, ":":u , " ").
assign
  {&NewValue} = replace({&NewValue}, ";":u , " ")
  {&NewValue} = replace({&NewValue}, '"':u , "_")   
  {&NewValue} = replace({&NewValue}, "'":u , "_")   
  {&NewValue} = replace({&NewValue}, "<":u , " ")
  {&NewValue} = replace({&NewValue}, ",":u , " ")
  {&NewValue} = replace({&NewValue}, ">":u , " ")
  {&NewValue} = replace({&NewValue}, ".":u , "_")   
  {&NewValue} = replace({&NewValue}, "?":u , " ")
  {&NewValue} = replace({&NewValue}, "/":u , "_")  .

if length({&NewValue}) > 0 then do:
    /* Remove the "*" at the end of each word; it's used as a wildcard */
  assign  {&NewValue} = replace({&NewValue},"* "," ").
  if substring({&NewValue},length({&NewValue}),1) = "*" then do:
    {&NewValue} = substring({&NewValue},1,length({&NewValue}) - 1).
 end.
end.
    

{&NewValue} = replace({&NewValue},"*"," ").
    


assign
  {&NewValue} = replace({&NewValue}, "&":u , " ")
  {&NewValue} = replace({&NewValue}, "!":u , " ")
  {&NewValue} = replace({&NewValue}, "^":u , " ")
  {&NewValue} = replace({&NewValue}, "|":u , " ").
