/* SX 55
   u-arscc.i 1.1 01/03/98 */
/* u-arscc.i 1.5 1/12/94 */
/*h*****************************************************************************
  INCLUDE      : u-arscc.i
  DESCRIPTION  : update for arsc credit screen
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE : 05/12/93 jrg; TB# 10577 - Add high balance
                 01/05/94 tdd; TB# 14112 - Added PM Cash Only flag
*******************************************************************************/

arsc.creditmgr  arsc.crestdt    arsc.lastrev
arsc.nextrev  arsc.lbxpostty
arsc.credlim   {arscc.z99 &user_updateinclude = "*"} 
 arsc.highbal    arsc.holdpercd   {arscc.z99 &user_updateinclude2 = "*"} 
arsc.selltype
/*tb 14112 01/05/94 tdd; Add PM Cash Only flag */
arsc.pmcashfl
arsc.statusdt
arsc.apmgr      arsc.apphoneno  arsc.banknm
arsc.bankmgr    arsc.bankphoneno
arsc.bankacct   arsc.securfl
arsc.lastpayamt arsc.lastpaydt  arsc.pastduedt
arsc.nopastdue
arsc.avgpaydays arsc.nopay      arsc.noinv
arsc.enterdt
arsc.dunsno     arsc.lastrtg[1] arsc.lastrtgdt[1]
arsc.crsname    arsc.lastrtg[2] arsc.lastrtgdt[2]
arsc.crref
