Def var Parker-Inx      as integer  no-undo. 
Def var Parker-Max      as integer  no-undo init 50.
Def var Parker-Id       as character no-undo.
 
def temp-table Parker-xref
  field ParkerRef as character
  field ParkerWhse as character
index
  ix-Parker
    ParkerWhse.

Def var Parker-whsexref as char no-undo extent 50
  init
 ["B79311,DPEO",       /* 1 */
  "B79311,SROS",       /* 2 */
  "B79311,DROS",       /* 3 */
  "868489,SBIR",       /* 4 */
  "868489,FBIR",       /* 5 */
  "017414,DBER",       /* 6 */
  "017414,DPHX",       /* 7 */
  "951902,DNDN",       /* 8 */
  "868204,DATL",       /* 9 */
  "312435,SCR",        /* 10 */
  "868198,DNOR",       /* 11 */
  "868198,SNOR",       /* 12 */
  "312481,FGR",        /* 13 */
  "312481,DGR",        /* 14 */
  "866924,SGF",        /* 15 */
  "866950,SMIN",       /* 16 */
  "866950,FMEC",       /* 17 */
  "866950,DMCP",       /* 18 */
  "867774,SOM",        /* 19 */
  "312470,DSCP",       /* 20 */
  "312470,DCIN",       /* 21 */
  "837306,DDAL",       /* 22 */
  "837306,SDAL",       /* 23 */
  "837304,DELP",       /* 24 */
  "017470,DHOU",       /* 25 */
  "017470,SHOU",       /* 26 */
  "951928,SSLC",       /* 27 */
  "951928,DSLC",       /* 28 */
  "951917,DXCP",       /* 29 */
  "951917,DXTR",       /* 30 */
  "017411,SPHX",       /* 31 */
  "867799,SBTM",       /* 32 */
  "680051,DSTH",       /* 33 */
  "680051,SDET",       /* 34 */
  "312418,SKC",        /* 35 */
  "868396,SSL",        /* 36 */
  "866949,SFAR",       /* 37 */
  "678847,SCOL",       /* 38 */
  "678847,DCLM",       /* 39 */
  "866927,SMIL",       /* 40 */
  "866927,DMIL",       /* 41 */
  "678788,DDEL",       /* 42 */
  "678788,SDEL",       /* 43 */
  "680932,ACLV",       /* 44 */
  "680932,SCLV",       /* 45 */
  "680932,DCAS",       /* 46 */
  "680932,DCLV",       /* 47 */
  "678795,DDAY",       /* 48 */
  "D40582,DPIT",       /* 49 */
  "866959,WARDEN"].    /* 50 */


Procedure ParkerLoad:
 do Parker-inx = 1 to Parker-Max:
   create Parker-xref.
   assign Parker-xref.ParkerRef  = entry(1,Parker-whsexref[Parker-inx],",")
          Parker-xref.ParkerWhse = entry(2,Parker-whsexref[Parker-inx],",").
 end.

end.

Procedure ParkerRef:
  define input  parameter ip-whse as character format "x(4)" no-undo.
  define output parameter ip-Ref  as character               no-undo.
  
  find Parker-xref where Parker-xref.ParkerWhse = ip-whse no-lock no-error.
  
  if not avail Parker-xref then
    assign ip-Ref = ip-whse.
  else
    assign ip-Ref = Parker-xref.ParkerRef.
end.    



















