/*  SX 55

    u-icswb.i 1.3 08/13/98 */ 
/******************************************************************************   
  INCLUDE         : u-icswb.i 
  DESCRIPTION     : inventory control setup warehouse balances 
  USED ONLY ONCE? : no 
  AUTHOR          : jlc 
  DATE WRITTEN    : 06/11/92 
  CHANGES MADE    : 
    06/12/92 jlc; TB#  6945 added parameter 1. 
    11/03/92 mms; TB#  8561 Remove seecost security off datc add update when 
        using product surcharge logic 
    07/06/95 emc; TB# 18812 7.0 Rebates Enhancement (a3b) - Add Rebate Cost 
    05/20/96 jkp; TB# 21136 Added rpt852dt. 
    07/26/96 gdf; TB# 21580 Added security to not change GL cost depending on         the new sasos f
lag chgglcostfl. 
    03/17/98 sbr; TB# 24615 Level 2 security blocks cost display. 
    07/30/98 sbr; TB# 25161 Cannot see addon when security = 2. 
******************************************************************************/ 
/*e The g-chgbalfl needs to be utilized during icswb.p (setup) update mode.  Th    flag is to be ignored when
 this include is used in display mode. */ 
 
/*tb 24615 03/17/98 sbr; Added {1} parameter the when condition for costs. */ 
icsw.avgcost     when g-seecostfl = yes {1} and 
                      (v-icglcost ne "a" or v-chgglcostfl = yes) /{1}* */ 
icsw.lastcost    when g-seecostfl = yes {1} and 
                      (v-icglcost ne "l" or v-chgglcostfl = yes) /{1}* */  
icsw.replcost    when g-seecostfl = yes {1} and 
                      (v-icglcost ne "r" or v-chgglcostfl = yes) /{1}* */  
icsw.replcostdt  when g-seecostfl = yes {1} and 
                     (v-icglcost ne "r" or v-chgglcostfl = yes)  /{1}* */ 
icsw.stndcost    when g-seecostfl = yes {1} and 
                      (v-icglcost ne "a" or v-chgglcostfl = yes or
                      (avail x-sasos and x-sasos.securcd[15] >= 2) or
                      (avail x-sasos and x-sasos.securcd[15] > 0 and
                       icsw.stndcost = v-SdiCallCost))               /{1}* */  
icsw.stndcostdt  when g-seecostfl = yes {1} and 
                      (v-icglcost ne "a" or v-chgglcostfl = yes or
                      (avail x-sasos and x-sasos.securcd[15] >= 2) or 
                      (avail x-sasos and x-sasos.securcd[15] > 0 and
                       icsw.stndcost = v-SdiCallCost))               /{1}* */  
icsw.rebatecost  when g-seecostfl = yes 
 
/*tb 25161 07/30/98 sbr; Added {1} to the when condition for icsw.addoncost */ 
icsw.addoncost   when g-seecostfl = yes {1} and                               
                      (v-icincaddgl = no or v-icglcost = "f" or 
                      v-chgglcostfl = yes)   /{1}* */
icsw.datccost    when s-title1 <> "" 
icsw.baseyrcost  when g-seecostfl = yes 
icsw.lastcostfor when g-seecostfl = yes 
 
icsw.qtyonhand      {1} when g-chgbalfl /{1}* */ 
icsw.qtyreservd     {1} when g-chgbalfl /{1}* */ 
icsw.qtycommit      {1} when g-chgbalfl /{1}* */ 
icsw.qtybo          {1} when g-chgbalfl /{1}* */ 
icsw.qtyonorder     {1} when g-chgbalfl /{1}* */ 
icsw.qtyrcvd        {1} when g-chgbalfl /{1}* */ 
icsw.qtyunavail     {1} when g-chgbalfl /{1}* */ 
icsw.qtyintrans     {1} when g-chgbalfl /{1}* */ 
icsw.qtyreqshp      {1} when g-chgbalfl /{1}* */ 
icsw.qtyreqrcv      {1} when g-chgbalfl /{1}* */ 
icsw.qtydemand      {1} when g-chgbalfl /{1}* */ 
 
icsw.issueunytd     {1} when g-chgbalfl /{1}* */ 
icsw.rcptunytd      {1} when g-chgbalfl /{1}* */ 
icsw.retinunytd     {1} when g-chgbalfl /{1}* */ 
icsw.retouunytd     {1} when g-chgbalfl /{1}* */ 
 
icsw.rpt852dt       {1} when g-chgbalfl /{1}* */ 
icsw.lastinvdt      {1} when g-chgbalfl /{1}* */ 
icsw.lastrcptdt     {1} when g-chgbalfl /{1}* */ 
icsw.slchgdt        {1} when g-chgbalfl /{1}* */     /* SX 55 */
icsw.lastpowtdt     {1} when g-chgbalfl /{1}* */ 
icsw.priceupddt     {1} when g-chgbalfl /{1}* */ 
icsw.lastcntdt      {1} when g-chgbalfl /{1}* */ 
        

