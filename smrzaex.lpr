/* insaex3.lpr 1.2 09/15/98 */
/*h****************************************************************************
  INCLUDE      : insaex3.lpr
  DESCRIPTION  : Fields to extract during insaex3.p
******************************************************************************/
/*e Record Status */
{&case}(v-linetype)
v-del
(if "{&type}" = "i" then 
    if {&buffoeel}oeel.invoicedt <> ? then 
        string({&buffoeel}oeel.invoicedt) 
    else string(today)
 else if "{&type}" = "b" then
    if {&buffoeel}oeel.enterdt <> ? then
        string({&buffoeel}oeel.enterdt)
    else string(today)
 else 
    string(today))
v-del
/*e Entity ID */
{&case}(v-entity)
v-del

/*e Sales */

if zx-net <> ? then
  string(zx-net,"->>>>>>>>>9.99")
else
  string(v-zero)
v-del

/*tb 23938 10/29/97 jkp; Added check for question mark. */
/*e Cost */
if zx-cost <> ? then
    string(zx-cost,"->>>>>>>>>9.99")
else string(v-zero)
v-del

/*e Discount */
string(v-disc,"->>>>9.99")
v-del

/*tb 23938 10/29/97 jkp; Added check for question mark. */
/*tb 24271 12/03/97 jkp; Changed the quantity to oeel.stkqtyord for the 
     backlog gateway only. */
/*tb 25288 09/05/98 jkp; Changed it to check type a since the rest can fall
     into the else condition. */
/*e Invoices = Stock Quantity Shipped
    Sales    = Stock Quantity Shipped
    Bookings = Stock Quantity Shipped
    Backlog  = Stock Quantity Ordered */
(if v-linetype = "o" then                                
    string(zx-orderqty)
 else 
    string(zx-cancelqty))
v-del

/*e Number of Billing Lines (counter) */
string(v-nolines)
v-del

/*tb 23938 10/29/97 jkp; Added check for question mark. */
/*tb 23985 10/29/97 jkp; Correct rebate amount to be negative for returns. */
/*e Vendor Rebate Amount */
if v-vendrebamt <> ? then
    if {&buffoeel}oeel.returnfl = yes then
        string((zx-vendrebamt * (-1)), "->>>>9.99")
    else        
        string(zx-vendrebamt, "->>>>9.99")
else string(v-zero)
v-del

/*tb 23938 10/29/97 jkp; Added check for question mark. */
/*tb 23985 10/29/97 jkp; Correct rebate amount to be negative for returns. */
/*e Customer Rebate Amount */
if v-custrebamt <> ? then
    if {&buffoeel}oeel.returnfl = yes then
        string((zx-custrebamt * (-1)), "->>>>9.99")
    else
        string(zx-custrebamt, "->>>>9.99")
else string(v-zero)
v-del

/*tb 23938 10/29/97 jkp; Added check for question mark. */
/*e Order Number */
if ({&buffoeel}oeel.orderno <> ? and {&buffoeel}oeel.ordersuf <> ?) then
    string({&buffoeel}oeel.orderno,">>>>>>>9") +
        "-" +  
        string({&buffoeel}oeel.ordersuf,"99")
else v-null
v-del

/*tb 23938 10/29/97 jkp; Added check for question mark. */
/*e Outside Sales Rep */

if {&buffoeel}oeel.slsrepout <> "" then
   {&case}({&buffoeel}oeel.slsrepout)
else {&case}({&buffoeeh}oeeh.slsrepout)
v-del

/*tb 23938 10/29/97 jkp; Added check for question mark. */
/*e Inside Sales Rep */
if substr({&buffoeel}oeel.slsrepin,1,4) <> "" then
    {&case}({&buffoeel}oeel.slsrepin)
else {&case}({&buffoeeh}oeeh.slsrepin)
v-del

/*tb 23938 10/29/97 jkp; Added check for question mark. */
/*e Taken By */
if {&buffoeeh}oeeh.takenby <> "" then
    {&case}({&buffoeeh}oeeh.takenby)
else {&case}({&buffoeeh}oeeh.slsrepin)
v-del

/*tb 23938 10/29/97 jkp; Added check for question mark. */
/*e Customer ID */
if {&buffoeel}oeel.custno <> ? then
    string({&buffoeel}oeel.custno)
else string(v-zero)    
v-del

/*tb 23938 10/29/97 jkp; Added check for question mark. */
/*e Ship To ID */
if {&buffoeel}oeel.shipto <> ? then
    {&case}({&buffoeel}oeel.shipto)
else v-null
v-del

/*e Product ID */
if {&buffoeel}oeel.shipprod <> ? then
    {&case}({&buffoeel}oeel.shipprod)
else v-null
v-del

/*e Vendor ID */
if {&buffoeel}oeel.vendno <> ? then
    string({&buffoeel}oeel.vendno)    
else string(v-zero)
v-del

/*e Product Line */
if {&buffoeel}oeel.prodline <> ? then
    {&case}(SubStr({&buffoeel}oeel.prodline,1,6))
else v-null
v-del

/*e Buyer */
{&case}(v-buyer)
v-del

/*e Price Type */
if {&buffoeel}oeel.pricetype <> ? then
    {&case}(substr({&buffoeel}oeel.pricetype,1,4))
else v-null
v-del

/*e Product Family Group */
{&case}(v-famgrp)
v-del

/*e Product Class */
string(v-class)
v-del

/*e Order Fill Type */
{&case}(v-ordertype)
v-del

/*e Ship Via */
if {&buffoeeh}oeeh.shipviaty <> ? then
    {&case}({&buffoeeh}oeeh.shipviaty)
else v-null
v-del

/*e Payment Type */
{&case}(v-paytype)
v-del

/*e Terms Type */
if {&buffoeeh}oeeh.termstype <> ? then
    {&case}({&buffoeeh}oeeh.termstype)
else v-null
v-del

/*e Delivery Code */
{&case}(v-null)  
v-del

/*e Order Source Type */
if {&buffoeeh}oeeh.sourcepros <> ? then
    {&case}(substr({&buffoeeh}oeeh.sourcepros,1,4))
else v-null
v-del

/*e Kit Code */
{&case}(v-kittype)
v-del

/*e Warehouse ID */
if {&buffoeel}oeel.whse <> ? then
    {&case}({&buffoeel}oeel.whse)
else v-null
v-del

/*e Header User 1 */
if {&buffoeeh}oeeh.user1 <> ? then
    {&case}(substr({&buffoeeh}oeeh.approvty,1,1))
else v-null
v-del

/*e Header User 2 */
if {&buffoeeh}oeeh.custpo <> ? then
    {&case}(substr({&buffoeeh}oeeh.custpo,1,22))
else v-null    
v-del

/*e Line User 1 */
{&case}(string (zx-age,"9"))

v-del

/*e Line User 2 */
{&case}({&buffoeel}oeel.specnstype)

v-del

/*e Error ID */
v-zero
v-del

/*e Product Category ID */
if {&buffoeel}oeel.prodcat <> ? then
    {&case}({&buffoeel}oeel.prodcat)
else v-null
v-del 

/*e Invoices Only = Order Date */
(if "{&type}" = "i" then 
    (if {&buffoeel}oeel.enterdt <> ? then 
       string({&buffoeel}oeel.enterdt)
     else string(today))
 
 else "")
(if "{&type}" = "i" then 
    v-del 
 else "")
 
/*e Requested Date */ 
if {&buffoeel}oeel.reqshipdt <> ? then
    string({&buffoeel}oeel.reqshipdt)
else if {&buffoeeh}oeeh.reqshipdt <> ? then
    string({&buffoeeh}oeeh.reqshipdt)
else string(today)
v-del

/*e Promised Date */
if {&buffoeel}oeel.promisedt <> ? then
    string({&buffoeel}oeel.promisedt)
else if {&buffoeeh}oeeh.promisedt <> ? then
    string({&buffoeeh}oeeh.promisedt)
else string(today)
v-del

(if "{&type}" = "i" then 
    (if {&buffoeeh}oeeh.shipdt <> ? then 
        string({&buffoeeh}oeeh.shipdt)
     else string(today))
 else /* if "{&type}" = "a" then  */
    (if {&buffoeeh}oeeh.enterdt <> ? then 
        string({&buffoeeh}oeeh.enterdt)
     else string(today)))
/*  else "")   */

v-del 

/*e SX User 1 */

{&case}(string (v-GlCost,"-999999.9999"))

v-del

/*e SX User 2 */

{&case}(v-stagecd)
        
        

v-del

/*e SX User 3 */

{&case}(v-null)

v-del

/*e SX User 4 */

{&case}(string({&buffoeeh}oeeh.divno,">>9")) 

v-del

/*e SX User 5 */
 v-lpromdt

v-del
/*e SX User 6 */
 v-lexpdt
  
CHR(13) CHR(10).


