/*
If any changes to parameters are made, look in the following programs/includes:
g-zsdiicspuser.i
zsdiicscuser.p
zsdiicspuser.p
icsp-tariff2.p
zsditariffprt.p
icrzb.p
n-icizk.i
n-scheduleb.i
n-schedulebc.i
n-schedulebk.i
*/

confirm = true.
find {1} oeelk use-index k-oeelk where 
         oeelk.cono = g-cono and
         oeelk.ordertype = "o" and
         oeelk.orderno = g-orderno and
         oeelk.ordersuf = g-ordersuf and
         oeelk.lineno = ip-lineno
         no-lock no-error.
         
if not avail oeelk then do:
   confirm = false.
end.
else   
if avail oeelk then do:     
      assign var-prod = oeelk.shipprod
             var-whse = oeelk.whse.
             
             
      {w-icsp.i var-prod no-lock}
      {w-icsw.i oeelk.shipprod var-whse no-lock}

            if avail icsp then do:
              run zsdiicspuser.p  (INPUT "I", /* Inquiry */
                         INPUT recid(icsp),
                       /* Tariff Parameters */
                        INPUT-OUTPUT  vp-CanadianTariffNbr,  
                        INPUT-OUTPUT  vp-USATariffNbr,   
                        INPUT-OUTPUT  vp-CountryOfOrigin, 
                        INPUT-OUTPUT  vp-MaintDt,  
                        INPUT-OUTPUT  vp-CustomsExpireDt,
                        INPUT-OUTPUT  vp-NAFTAexpiredt,  

                       /* Schedule B Parameter */
                        INPUT-OUTPUT  vp-ScheduleBInfo,  
                        INPUT-OUTPUT  vp-ScheduleBMaintDt, 
                        
                       /* RAF */
                        INPUT-OUTPUT  vp-source,
                        INPUT-OUTPUT  vp-sourcedt,
                    

                       /* MISC Fields */
                        INPUT-OUTPUT vp-SurChargePct,  
                        INPUT-OUTPUT vp-NR-NC,  
                        INPUT-OUTPUT vp-UNSPSCcode,
                        INPUT-OUTPUT vp-flagtr,     
                        INPUT-OUTPUT vp-Protected,  
                        INPUT-OUTPUT vp-gasandoil,  
                        INPUT-OUTPUT vp-core,       
                        INPUT-OUTPUT vp-sensitivity,
                             

                       /* Fabrication Labor Information */
                        INPUT-OUTPUT vp-Labor,              
                        INPUT-OUTPUT vp-Documentation,     
                        INPUT-OUTPUT vp-DrawingFee,      
                        INPUT-OUTPUT vp-ElectricalLabor,    
                        INPUT-OUTPUT vp-MEchanicalLabor).   
 
       end. /* avail icsp */
           if avail icsw then do:
             assign var-netamt = oeelk.qtyord * icsw.avgcost.
           end.  /* if avail icsw */
           else
             assign var-netamt = 0.
    end. /* avail oeel */
 

