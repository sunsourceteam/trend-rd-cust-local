define stream zd.
/*
define {1} shared temp-table zsdivaspmap  no-undo 
 field cono         like vaesl.cono
 field rectype     as char format "x(1)"
 field seqno        like vaes.seqno
 field lineno       like vael.lineno
 field prod         as char format "x(24)"
 field typename     as char format "x(15)"
 field addontype    as logical format "$/%"
 field addonamt     as dec  format ">>>9.99"  
 field description  as char format "x(24)"
 field override     as logical
 field overrideinit as char format "x(4)"
 
 index tyinx
    cono
    rectype
    prod
    seqno
    lineno
    addontype.
*/    
