{p-rptbeg.i}
{g-gl.i}
{g-secure.i}
{p-glbalx.i &def = "/*"}
{icrt.gva}

def var s-date     as  date    format "99/99/99"  no-undo.
def var s-pcat     as  c       format "x(4)"      no-undo.
def var print-hdr  as  logical                    no-undo.

form
    icenh.descrip[1]   at 10
with frame f-desc no-box no-labels down width 132.

/* The first four forms are for detail level only */
/* Detail Header */
form
    s-lbl1             at 1
    v-key1             at 7
    s-lbl2             at 20
    v-key2             at 26
    s-glno             at 40
    s-glname           at 40
    v-dupfl            format "(Dup)/ "
    v-sasoofl          format "(SASOO)/ "
with frame f-dethdr no-labels no-box page-top width 132.

/* Detail Page Top */

form
    s-printasof        at 5
    skip (1)
    "Product"          at 3
    "PCAT"             at 29
    "Whse"             at 35
    "Trn Date"         at 41
    v-ordernotit       at 53
    "InvQty"           at 67
    "Unit"             at 77
    "Cost"             at 88
    v-title1           at 95
/*    v-title2           at 115 */
/*    v-title3           at 126 */
with frame f-dettop no-labels no-box page-top width 132.

/*
form
    s-printasof        at 5
    skip (1)
    "Product"          at 3
    s-lbl3             at 29
    v-ordernotit       at 38
    "Inv Qty"          at 52
    "Unit"             at 62
    "Cost"             at 73
    v-title1           at 80
    v-title2           at 95  
/*    v-title3           at 115 */
with frame f-dettop no-labels no-box page-top width 132.
*/

/* Detail Line */
form
    v-prod             at 3
    s-pcat             at 29
    v-whse             at 35
    s-date             at 41
    v-ordernodata      at 50
    s-quantity         at 63
    icsp.unitstock     at 77
    s-cost             at 82
    s-invval1          at 96
    s-invval2          at 114 
/*    s-extdiff          at 122 */
with frame f-detail no-box no-labels down width 132.

/* Detail Footer */
form
    "--------------"   at 95
    "G/L Bal:"         to 74
    s-glbal            at 76
    t-catval1          at 96
    skip (1)
with frame f-detftr no-box no-labels width 132.

/* These Forms are for GL Level Print - Currently, not used */
/* Page Top Header */
form
    s-printasof        at 1
    skip (1)
    s-lbl1             at 6
    s-lbl2             at 13
    v-title1           at 53
    v-title2           at 69
    v-title3           at 87
    v-title4           at 104
with frame f-gltop no-labels no-box page-top width 132.

form
    "G/L Acct: "       at 1
    s-glno
    s-glname
    v-sasoofl          format "(SASOO)/ "
with frame f-glhdr no-labels no-box page-top width 132.

/* Detail Line */
form
    v-key1             at 6
    v-key2             at 13
    s-invval1          at 53
    s-invval2          at 69
    s-extdiff          at 85
    s-perdiff          at 102
with frame f-gldet no-labels no-box width 132.

/* Totals */
form
    v-title7           at 53                                     /* 100 */
    v-title5           at 69
    v-title6           at 85
    s-offby            at 1
    "G/L:"             at 33
    s-glbal            at 38
    s-invval1          at 53
    s-invval2          at 69
    s-extdiff          at 85
    s-perdiff          at 102
    skip (1)
with frame f-gltot no-labels no-box width 132.

/* Totals */
form
    skip (1)
    "Grand Totals:"
with frame f-totals no-labels no-box.

/* These Frames Used to Display ICSEG Records Not Used */
form
    "The following ICSEG records were in the ranges specified"
                       at 1
    "but had no products assigned to them:"
                       at 1
    skip (1)
    s-lbl1             at 5
    s-lbl2             at 12
    s-lbl3             at 19
    "G/L Account"      at 25
    "Title"            at 50
    "Balance"          at 92
    skip (1)
with frame f-not1 no-box no-labels width 132.

form
    v-key1             at 5
    v-key2             at 12
    g-divno            at 19
    s-glno             at 25
    s-glname           at 48
    s-glbal            at 86
with frame f-not2 no-box no-labels down width 132.

/* Balance Change Form */
form
    sasb.chgbal[1]     at 1
    sasb.chgbal[2]     at 19
    sasb.chgbal[3]     at 37
    sasb.chgbal[4]     at 55
    sasb.chgbal[9]     at 73                                    /* 150 */
    sasb.chgbal[10]    at 91
    sasb.operinit2     at 110
    sasb.enterdt       at 115
    v-beforefl         at 123 format "(b)/(a)"
with frame f-chgbal2 down no-box no-labels width 132.

/* Title for balance changes */
form
    "Manual Balance Changes: "                                 at 6
    " Average Cost         Last Cost        Addon Cost"        at 6
    "  Qty On Hand       Qty Unavail      Qty Received"        at 60
    "Enter Dt Oper"                                            at 116 skip
    "------------------------------------------------------"   at 6
    "------------------------------------------------------"   at 60
    "---------------"                                          at 115
with frame f-chgbal1 no-box page-top no-labels width 132.

/* These Forms Are For SASOO Records */
form
    skip (1)
    "The following SASO records have Inventory G/L Accounts."  at 1
    "This will often cause out of balance problems:"           at 1
    skip(1)
    "Oper"             at 5
    s-lbl3             at 10
    "G/L Account"      at 25
    "Title"            at 50
    "Balance"          at 92
    skip (1)
with frame f-oper1 no-box no-labels width 132.

form
    sasoo.oper2        at 5
    g-divno            at 10
    s-glno             at 25
    s-glname           at 48
    s-glbal            at 86
with frame f-oper2 no-box no-labels down width 132.

/* Generic Warning Frame */
form 
    v-warning    colon 15 label "WARNING"
with frame f-warn no-box side-labels down width 132.

{f-under.i}

{jrnlopn.gpr}

assign
    b-prod       = sapb.rangebeg[1]                            /* 200 */
    e-prod       = sapb.rangeend[1]
    b-whse       = sapb.rangebeg[2]
    e-whse       = sapb.rangeend[2]
    b-prodcat    = sapb.rangebeg[3]
    e-prodcat    = sapb.rangeend[3]
    b-divno      = integer(substring(sapb.rangebeg[4],1,4))
    e-divno      = integer(substring(sapb.rangeend[4],1,4))
    p-asoffl     = if sapb.optvalue[1] = "yes" then true else false
    v-datein     = sapb.optvalue[2].

{p-rptdt.i}

assign
    p-datecut    = v-dateout
    p-compare    = ""
    p-variance   = 0
    p-totals     = sapb.optvalue[3]
    p-qtyfl      = if sapb.optvalue[4] = "yes" then yes else no
    p-descfl     = if sapb.optvalue[5] = "yes" then yes else no
    p-unusedfl   = if sapb.optvalue[6] = "yes" then yes else no
  /* SX30 SunSource */

    v-title1     = "IC Value, " +
                   if      sasc.icglcost = "a" then "Avg"
                   else if sasc.icglcost = "s" then "Stnd"
                   else if sasc.icglcost = "r" then "Repl"
                   else if sasc.icglcost = "l" then "Last"
                   else if sasc.icglcost = "f" then "FIFO"
                   else ""
/* SX30 SunSource */

    v-title2     = ""
    v-title3     = ""
    v-title4     = ""
    v-title5     = ""
    v-title6     = ""
    v-title7     = fill("-",14)
    v-ordernotit = "Order#"
    v-keytype    = sasc.icglbsty
    s-lbl1       = if p-totals = "t" then ""
                   else if v-keytype = 1 then
                       if p-totals = "d" then "PCAT:"
                       else "PCAT"
                   else if p-totals = "d" then "Whse:"
                   else "Whse"
    s-lbl2       = if p-totals = "t" then ""
                   else if v-keytype = 3 then
                       if p-totals = "d" then "PCAT:"
                       else "PCAT"
                   else ""
    s-lbl3       = if v-keytype = 1 then "Whse"
                   else if v-keytype = 2 then "PCAT"
                   else ""
    v-cutoffdt   = p-datecut                                      /* 250 */
    v-asoffl     = p-asoffl
    s-printasof  = if p-asoffl then
                       "Printed on " + string(today) + ", Postings Through " +
                       string(p-datecut)
                   else "Printed on " + string(today) + ", All Postings".
                   
/* BEGIN REPORT LOGIC */

{report.gde}

if g-seecostfl = no then
    display "*** Operator Not Set Up To View Costs ***" with frame f-a.
else if sasc.icnsdofl and p-asoffl then
    display "*** Cannot Provide As Of Balance When ICAOC Purge Flag is Yes ***"
            with frame f-a.
else
main:
do:
    if p-totals = "d" then do:
        display
            v-title1
            s-lbl3
            s-printasof
            v-ordernotit
        with frame f-dettop.
        display with frame f-under.
    end.
    
    {w-sasoo.i ""r&d"" no-lock}
    
    if avail sasoo then
        if "{&typecd}" = "N" then
            assign
                v-gldivno   =  sasoo.gldivno2[4]
                v-gldeptno  =  sasoo.gldeptno2[4]
                v-glacctno  =  sasoo.glacctno2[4]
                v-glsubno   =  sasoo.glsubno2[4].
        else
            assign 
                v-gldivno   =  sasoo.gldivno[14]
                v-gldeptno  =  sasoo.gldeptno[14]
                v-glacctno  =  sasoo.glacctno[14]
                v-glsubno   =  sasoo.glsubno[14].
    
    for each icsd use-index k-icsd no-lock where
             icsd.cono  =  g-cono    and
             icsd.whse  ge b-whse    and                           /* 300 */
             icsd.whse  le e-whse    and
             icsd.divno ge b-divno   and
             icsd.divno le e-divno,
    
    each icenh use-index k-icenh no-lock where
         icenh.cono     = g-cono      and
         icenh.typecd   = "{&typecd}" and
         icenh.whse     = icsd.whse   and
         icenh.prodcat >= b-prodcat   and
         icenh.prodcat <= e-prodcat   and
         icenh.prod    >= b-prod      and
         icenh.prod    <= e-prod
/*    break by (if v-keytype = 1 and g-gldivfl = no then "" else icenh.whse) */
    break by icenh.whse
          by icenh.prodcat
/*          by (if v-keytype = 2 then "" else icenh.prodcat)  */
          by icenh.prod:
          if first-of(icenh.whse) 
          
/*          if first-of(if v-keytype = 1 and g-gldivfl = no then ""
                      else icenh.whse)
          or first-of(if v-keytype = 2 then "" else icenh.prodcat) */
/*          or first-of(if v-keytype = 2 then "" else icenh.prodcat) */
          then do for report transaction:
              assign
                  print-hdr  = yes
                  v-key1     = if v-keytype = 1 then "" else icsd.whse
                  v-key2     = if v-keytype = 2 then "" else icenh.prodcat
                  t-catval1  = 0
                  t-catval2  = 0.
              find first icseg use-index k-icseg where
                         icseg.cono      = g-cono and
                         icseg.accttype  = yes    and
                         icseg.disttype  = sasc.icglbsty and
                         icseg.key1      = v-key1 /* and
                         icseg.key2      = v-key2 */
                         no-lock no-error.
/*              {w-icseg.i &type = "yes"
                         &dist = "sasc.icglbsty" 
                         &key1 = "v-key1"
                         &key2 = "v-key2"
                         &lock = "no-lock"}. */
                         
              assign
                  i          = if "{&typecd}" = "n" then 15 else 9
                  v-sasoofl  = if avail icseg and
                                   icseg.gldivno[i] + icseg.gldeptno[i] +
                                   icseg.glacctno[i] + icseg.glsubno[i] ne 0
                                  then false
                               else true
                  s-gldivno  = if v-sasoofl then v-gldivno
                               else icseg.gldivno[i]
                  s-gldeptno = if v-sasoofl then v-gldeptno
                               else icseg.gldeptno[i]
                  s-glacctno = if v-sasoofl then v-glacctno
                               else icseg.glacctno[i]
                  s-glsubno  = if v-sasoofl then v-glsubno
                               else icseg.glsubno[i]
                  g-divno    = icsd.divno.
                  
              {p-glbalx.i &main = "/*"}
                                                                 /* 350 */
              if p-totals = "d" then do:
                  display
                      s-lbl1
                      s-glno
                      s-glname
                      v-dupfl
                      v-sasoofl
                  with frame f-dethdr.
                  
                  if v-keytype = 1 then
                      display
                          v-key2 @ v-key1
                      with frame f-dethdr.
                  else
                      display
                          v-key1
                          v-key2
                      with frame f-dethdr.
                  if v-glerrcd ne "" then do:
                      if v-glerrcd = "nf" then
                          display
                              "G/L Account Not Found" @ v-warning
                          with frame f-warn.
                      else if v-glerrcd = "tm" then
                          display "G/L Postings Occured During Calculation"
                              @ v-warning with frame f-warn.
                  end.
              end.
          end.
          
          assign
              s-quantity  = 0
              s-cost      = 0.
              
          if (p-asoffl = yes and icenh.opendt <= p-datecut) or
             (p-asoffl = no and icenh.activefl = yes) then
          for each icenl use-index k-icenl where
                   icenl.cono      = g-cono        and
                   icenl.typecd    = icenh.typecd  and
                   icenl.whse      = icenh.whse    and
                   icenl.prodcat   = icenh.prodcat and
                   icenl.prod      = icenh.prod    and
                   icenl.seqnoh    = icenh.seqnoh  and
                   (p-asoffl = no or icenl.postdt le p-datecut)
          no-lock:
              assign
                  s-quantity = s-quantity + icenl.quantity
                  s-cost     = s-cost + icenl.amount *
                               (if icenl.quantity < 0 then -1 else 1).
          end.
          
          assign
              s-invval1 = s-cost
              v-qtyfl   = no
              s-cost    = if s-quantity = 0 then s-cost
                          else s-cost / s-quantity.
                          
          if p-totals = "d" then do:
              find first icenl use-index k-icenl where
                         icenl.cono      = g-cono        and
                         icenl.typecd    = icenh.typecd  and
                         icenl.whse      = icenh.whse    and
                         icenl.prod      = icenh.prod    and
                         icenl.prodcat   = icenh.prodcat and
                         icenl.seqnoh    = icenh.seqnoh
              no-lock no-error.
              
              assign
                  v-prod        = icenh.prod
                  s-pcat        = icenh.prodcat
                  v-whse        = icenh.whse
                  s-date        = icenl.transdt
                  v-ordernodata = if avail icenl then
                                      if icenl.ordtype = "p" then
                                          "PO-" +
                                          string(icenl.orderno,"zzzzzz9") +
                                          "-" + string(icenl.ordersuf,"99")
                                      else if icenl.ordtype = "o" then
                                          "OE-" +
                                          string(icenl.orderno,"zzzzzz9") +
                                          "-" + string(icenl.ordersuf,"99")
                                      else if icenl.ordtype = "t" then
                                          "WT-" +
                                          string(icenl.orderno,"zzzzzz9") +
                                          "-" + string(icenl.ordersuf,"99")
                                      else if icenl.ordtype = "f" then
                                          "VA-" +
                                          string(icenl.orderno,"zzzzzz9") +
                                          "-" + string(icenl.ordersuf,"99")
                                      else ""
                                  else "".
              
              if p-qtyfl = no or s-quantity <> 0 or s-cost <> 0 then do:
/*                if print-hdr = yes then do:
                      display
                          s-lbl1
                          s-glno
                          s-glname
                          v-dupfl
                          v-sasoofl
                      with frame f-dethdr.
                      
                      if v-keytype = 1 then 
                          display
                              v-key2 @ v-key1
                          with frame f-dethdr.
                      else 
                          display
                              v-key1
                          with frame f-dethdr.
                      if v-glerrcd ne "" then do:
                          if v-glerrcd = "nf" then
                              display
                                  "G/L Account Not Found" @ v-warning 
                              with frame f-warn.
                          else if v-glerrcd = "tm" then
                              display "G/L Postings Occured During Calculation"
                                  @ v-warning with frame f-warn.
                      end.
                      assign print-hdr = no.
                  end.  */
                  display v-prod
                          v-whse
                          s-pcat
                          s-date
                          s-cost
                          s-quantity
                          icenh.unit @ icsp.unitstock
                          s-invval1
                          v-ordernodata
                  with frame f-detail.
                  
                  if p-descfl then
                      display
                          icenh.descrip[1]
                      with frame f-desc.
                  down with frame f-detail.
              end.
          end.
          assign
              t-catval1   = t-catval1 + s-invval1
              t-catval2   = t-catval2 + s-invval2.
              
          if last-of(icenh.whse)
/*          if last-of(if v-keytype = 1 and g-gldivfl = no then ""
                     else icenh.whse)
          or last-of(if v-keytype = 2 then "" else icenh.whse) */
/*          or last-of(if v-keytype = 2 then "" else icenh.prodcat)  */
          then do for report transaction:

              /* Create Report record */
              {icrt.gcr} 
              
              /* Print Detail Subtotals */
              if p-totals = "d" and t-catval1 <> 0 then do:
                  display
                      s-glbal
                      t-catval1
                  with frame f-detftr.
                  down with frame f-detftr.
              end.
          end.
      end.
      
      /* Hide Header Frames */
      hide frame f-under   no-pause.
      hide frame f-dettop  no-pause.
      hide frame f-dethdr  no-pause.
      
      {icrt2.gpr}
      
      {icrt3.gpr &typecd = "{&typecd}"}
  
  end.

  /* Delete Report Records */
  {report.gde}

  hide all no-pause.