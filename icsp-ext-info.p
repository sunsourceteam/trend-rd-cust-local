{g-lu.i}
/*
{g-all.i}
*/

def input parameter e-prod like icsp.prod          no-undo.

def var z-desc1    as c  format "x(60)"             no-undo.
def var z-desc2    as c  format "x(60)"             no-undo.
def var z-desc3    as c  format "x(60)"             no-undo.
def var z-desc4    as c  format "x(60)"             no-undo.
def var z-desc5    as c  format "x(60)"             no-undo.    
def var z-desc6    as c  format "x(60)"             no-undo.
def var z-URL1     as c  format "x(60)"             no-undo.    
def var z-URL2     as c  format "x(60)"             no-undo.
def var z-URL3     as c  format "x(60)"             no-undo.    
def var z-URL4     as c  format "x(60)"             no-undo.
def var z-URL5     as c  format "x(60)"             no-undo.
def var z-longprod as c  format "x(60)"             no-undo.    
def var z-LT       like  icsw.leadtmavg             no-undo.
def var z-space    as c  format "x(1)"              no-undo.
def var z-lastkey  as i                             no-undo.    
def var z-transdt  as date format "99/99/99"        no-undo.    
def var z-dispfl   as logical          init no      no-undo.
  
/*tb 19427 08/12/98 cm; Add multi language capability. Add error 4150. */
{valmsg.gas &string = "9042"}



form
   z-desc1        colon  9 label "Ext Desc"
   z-desc2        at 11 no-label
   z-desc3        at 11 no-label
   z-desc4        at 11 no-label
   z-desc5        at 11 no-label
   z-desc6        at 11 no-label
   z-URL1         colon  9 label "SpecsURL"
   z-URL2         at 11  no-label
   z-URL3         at 11  no-label
   z-URL4         at 11  no-label
   z-URL5         at 11  no-label
   z-longprod     colon  9 label "ExtVProd"
   /*
   z-LT           colon 13 label "Published LT"      
   "Days"         at 19
   z-transdt      at 52    label "Last Upd"     
   */
with frame f-extdesc side-labels title "Extended Product Information"   
     row 2 column 4 width 74 no-hide overlay.



hide message no-pause.

find first sasos where sasos.cono     = g-cono and
                       sasos.oper2    = g-operinits and
                       sasos.menuproc = "ics" 
                       no-lock no-error.
if avail sasos and sasos.securcd[2] >= 4 then
  assign z-dispfl = yes.
else
  assign z-dispfl = no.

if g-callproc begins "OE" or g-callproc begins "zsdio" then
  assign z-dispfl = no.
/*  
find first icsw where icsw.cono = g-cono and
                      icsw.prod = e-prod and
                      icsw.whse > "AZZZ" and
                      icsw.statustype <> "X"
                      no-lock no-error.
if avail icsw then
  assign z-LT = icsw.xxi4.  
*/
find notes where notes.cono         = g-cono and
                 notes.notestype    = "ZZ" and
                 notes.primarykey   = "EXTENDED ICSP INFO" and
                 notes.secondarykey = e-prod and
                 notes.pageno       = 1
                 no-lock
                 no-error.
if avail notes then
   assign z-desc1    = notes.noteln[1]
          z-desc2    = notes.noteln[2]
          z-desc3    = notes.noteln[3]
          z-desc4    = notes.noteln[4]
          z-desc5    = notes.noteln[5] 
          z-desc6    = notes.noteln[6]
          z-URL1     = notes.noteln[7]  
          z-URL2     = notes.noteln[8]
          z-URL3     = notes.noteln[9]
          z-URL4     = notes.noteln[10]
          z-URL5     = notes.noteln[11]
          z-longprod = notes.noteln[12].
        /*  z-transdt  = notes.transdt.   */
else
   assign z-desc1    = ""
          z-desc2    = ""
          z-desc3    = ""
          z-desc4    = ""
          z-desc5    = ""
          z-desc6    = ""
          z-URL1     = ""
          z-URL2     = ""   
          z-URL3     = ""
          z-URL4     = ""    
          z-URL5     = ""
          z-longprod = "".
        /*  z-transdt  = ?. */

extd:
do while true with frame f-extdesc on endkey undo extd, leave extd:
  if z-dispfl = no then
    do:
    display z-desc1
            z-desc2
            z-desc3
            z-desc4
            z-desc5
            z-desc6
            z-URL1
            z-URL2
            z-URL3
            z-URL4
            z-URL5
            z-longprod
          /*  z-LT 
            z-transdt */ with frame f-extdesc. 
    /*
    {pause.gsc} 
    */
    /* 
    fool progress when pausing so progress doesn't know the user pressed a
    key
    */   
    UNIX SILENT VALUE("sh /rd/cust/local/UnixPause.sh").
    readkey pause 0.
    leave extd.
  end.
  
  else 
    do: 
    display z-URL1
            z-URL2
            z-URL3
            z-URL4
            z-URL5
          /*  z-LT
            z-transdt */ with frame f-extdesc. 
    update  z-desc1       when z-dispfl = yes
            z-desc2       when z-dispfl = yes
            z-desc3       when z-dispfl = yes
            z-desc4       when z-dispfl = yes
            z-desc5       when z-dispfl = yes
            z-desc6       when z-dispfl = yes
            z-longprod    when z-dispfl = yes
  editing:
  readkey.
  if {k-cancel.i} then 
    do:
    readkey pause 0.
    leave extd. 
  end.
  apply lastkey.
  if {k-after.i} or {k-func.i} then 
    do:    
    find notes where notes.cono = g-cono and
                     notes.notestype   = "ZZ" and
                     notes.primarykey  = "EXTENDED ICSP INFO" and
                     notes.secondarykey = e-prod and
                     notes.pageno      = 1
                     no-error.
    if not avail notes then 
      do:
      create notes.
      assign notes.cono = g-cono
             notes.notestype = "ZZ"
             notes.primarykey = "EXTENDED ICSP INFO"
             notes.secondarykey = e-prod
             notes.pageno     = 1
             notes.noteln[1]  = z-desc1
             notes.noteln[2]  = z-desc2
             notes.noteln[3]  = z-desc3
             notes.noteln[4]  = z-desc4
             notes.noteln[5]  = z-desc5 
             notes.noteln[6]  = z-desc6
             notes.noteln[7]  = z-URL1
             notes.noteln[8]  = z-URL2
             notes.noteln[9]  = z-URL3
             notes.noteln[10] = z-URL4
             notes.noteln[11] = z-URL5
             notes.noteln[12] = z-longprod
             notes.transdt    = TODAY. 
    end. /* not avail notes */
    else 
      do:
      assign notes.cono = g-cono
             notes.notestype = "ZZ"
             notes.primarykey = "EXTENDED ICSP INFO"
             notes.secondarykey = e-prod
             notes.pageno     = 1
             notes.noteln[1]  = z-desc1
             notes.noteln[2]  = z-desc2
             notes.noteln[3]  = z-desc3
             notes.noteln[4]  = z-desc4
             notes.noteln[5]  = z-desc5 
             notes.noteln[6]  = z-desc6
             notes.noteln[7]  = z-URL1
             notes.noteln[8]  = z-URL2
             notes.noteln[9]  = z-URL3
             notes.noteln[10] = z-URL4
             notes.noteln[11] = z-URL5
             notes.noteln[12] = z-longprod
             notes.transdt    = TODAY.
    end. /* update notes */
  end. /* k-after or k-func */
  if {k-accept.i} then 
    do: 
    hide frame f-extdesc.
    hide message no-pause.
    leave extd.
  end.
  end. /* editing */
  hide frame f-extdesc.
  hide message no-pause.
  end.
end. /* do while */     
hide frame f-extdesc no-pause.
hide message no-pause.
readkey pause 0.
