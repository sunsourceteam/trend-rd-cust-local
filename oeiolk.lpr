/* SX 55 */
/* oeiolk.lpr 1.1 01/03/98 */
/* oeiolk.lpr 1.0 5/22/95*/
/*h****************************************************************************

  INCLUDE      : oeiolk.lpr
  DESCRIPTION  : Function Key Processing in OEIO
  USED ONCE?   : yes
  AUTHOR       : ajw
  DATE WRITTEN : 05/22/95
  CHANGES MADE :
    05/22/95 ajw; TB# 12769 Added F6-Detail to OEIO Kit Component Screen
    04/24/01 sbr; TB# e7433 Does not display component serial number
******************************************************************************/

{oeiolk.z99 &user_beginlpr = "*"}

if {k-func6.i} then do:
  find oeelk where recid(oeelk) = v-recid[frame-line(f-oeetk)]
            no-lock no-error.
  if avail oeelk then do:
    run oeiolkd.p(recid(oeelk)).
    next dsply.
    end.
  else do:
    run err.p(1013).
    next dsply.
    end.
end.

/*tb e7433 04/24/01 sbr; Added code for F7-Serial/Lots inquiry screen display */
else if {k-func7.i} then 
  do:
  put screen row 21 col 20 color messages "F7-SERIAL/LOTS  F8-POIZS".
  find oeelk where recid(oeelk) = v-recid[frame-line(f-oeetk)]
                no-lock no-error.
  if avail oeelk then 
    do:
    {w-icsw.i oeelk.shipprod "if oeelk.altwhse ne '' then oeelk.altwhse
                               else oeelk.whse" no-lock}
    if avail icsw then 
      do:
      if icsw.serlottype = "s" then run oeiols.p("o",
                                                 oeelk.orderno,
                                                 oeelk.ordersuf,
                                                 oeelk.lineno,
                                                 oeelk.seqno).
      else if icsw.serlottype = "l" then run oeioll.p("o",
                                                      oeelk.orderno,
                                                      oeelk.ordersuf,
                                                      oeelk.lineno,
                                                      oeelk.seqno).
      else 
        do:
 /*e not serial/lot controlled */
        run err.p(6451).
        {pause.gsc}
        end.
      end. /* if avail icsw */
    end. /* if avail oeelk */
   put screen row 21 col 20 color messages "F7-Serial/Lots      F8-POIZS".
   end.
 else
 if {k-func8.i} then
   do:
   find oeelk where recid(oeelk) = v-recid[frame-line(f-oeetk)]
    no-lock no-error.
  if avail oeelk then
    do:
    assign o-prod = oeelk.shipprod.
    assign g-whse = oeelk.whse
           g-prod = oeelk.shipprod.
    run poizs.p.
    assign g-prod = o-prod.
     put screen row 21 column 4 color message
" F6-Detail   F7-Serial/Lots       F8-POIZS                                  ".      
    lastkey = 32.
    next dsply.
    end.
  else
   do:
   run err.p (1013).
   next dsply.
   end.
 end.

 find com use-index k-com where com.cono     = g-cono      and
                                com.comtype  = "ktq" +
                                               string(oeelk.seqno,"999") and
                                com.orderno  = oeelk.orderno  and
                                com.ordersuf = oeelk.ordersuf and
                                com.lineno   = oeelk.lineno   and
                                com.printfl  = yes
                                no-lock no-error.
 if avail com then do:
    if s-notesfl = "!" or s-notesfl = "*" or s-notesfl = "x" then
       assign s-notesfl = "x".
    else
       assign s-notesfl = "p".
    display s-notesfl with frame f-oeetk.
 end.
 
 if LASTKEY = 1 then do:   /* LASTKEY = 1 is the same as CTRL-A anywhere */
    find oeelk where recid(oeelk) = v-recid[frame-line(f-oeetk)].
    if avail oeelk then do:
       {kpioctrl_a.i}
       /*view frame f-blank.
       display x-blank with frame f-blank.*/
    end.
 end.                                              
          
{oeiolk.z99 &user_endlpr = "*"}
