/* f-pdxmm.i:   Form definitions for pdxmm.p - Matrix Modal Report */

form 
"Margin Breakdown"          at  81
"0 - 5"                     at  34
"6 -10"                     at  41
"11-15"                     at  48
"16-20"                     at  55
"21-25"                     at  62
"26-30"                     at  69
"31-35"                     at  76
"36-40"                     at  83
"41-45"                     at  90
"46-50"                     at  97
"51-55"                     at 104
"56-60"                     at 111
"61-65"                     at 118
"66-70"                     at 125
"71-75"                     at 132
"76-80"                     at 139
"81-85"                     at 146
"86-90"                     at 153
"91-95"                     at 160
"96-100"                    at 167

"-----"                     at  34
"-----"                     at  41
"-----"                     at  48
"-----"                     at  55
"-----"                     at  62
"-----"                     at  69
"-----"                     at  76
"-----"                     at  83
"-----"                     at  90
"-----"                     at  97
"-----"                     at 104
"-----"                     at 111
"-----"                     at 118
"-----"                     at 125
"-----"                     at 132
"-----"                     at 139
"-----"                     at 146
"-----"                     at 153
"-----"                     at 160
"------"                    at 167
with frame f-header no-underline no-box no-labels page-top down width 178.


form
"Region:"                   at   1
s-region                    at   9
s-ptype                     at   1
s-ptype-desc                at   8
s-ctype                     at   1
s-ctype-desc                at   8
s-pline                     at   1
s-pline-desc                at   8
with frame f-subhdr no-underline no-box no-labels down width 178.

form
s-description               at   1
s-line-amt                  at  23
s-0-5                       at  33
s-6-10                      at  40
s-11-15                     at  47
s-16-20                     at  54
s-21-25                     at  61
s-26-30                     at  68
s-31-35                     at  75
s-36-40                     at  82
s-41-45                     at  89
s-46-50                     at  96
s-51-55                     at 103
s-56-60                     at 110
s-61-65                     at 117
s-66-70                     at 124
s-71-75                     at 131
s-76-80                     at 138
s-81-85                     at 145
s-86-90                     at 152
s-91-95                     at 159
s-96-100                    at 167
with frame f-detail no-underline no-box no-labels down width 178.

form
s-description               at   1
s-line-amt                  at  23
sd-0-5                      at  33
sd-6-10                     at  40
sd-11-15                    at  47
sd-16-20                    at  54
sd-21-25                    at  61
sd-26-30                    at  68
sd-31-35                    at  75
sd-36-40                    at  82
sd-41-45                    at  89
sd-46-50                    at  96
sd-51-55                    at 103
sd-56-60                    at 110
sd-61-65                    at 117
sd-66-70                    at 124
sd-71-75                    at 131
sd-76-80                    at 138
sd-81-85                    at 145
sd-86-90                    at 152
sd-91-95                    at 159
sd-96-100                   at 167
with frame f-detaildot no-underline no-box no-labels down width 178.
