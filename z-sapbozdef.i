/* z-sapbozdef.i         
 
   Definitions for Custom SAPBO reocrds

*/

define temp-table zsapbo no-undo

  field selection_type as character format "x"
  field selection_char4 as character format "x(4)"
  field selection_cust like arsc.custno

  index zsapbo_inx
        selection_type
        selection_char4
        selection_cust.

define var zelection_good      as logical              no-undo.
define var zelection_matrix    as logical extent 6  
 init
 [false,
  false,
  false,
  false,
  false,
  false]    no-undo.

define var zelection_type      as character format "x"     no-undo. 
define var zelection_slsrep  as character format "x(4)"    no-undo.
define var zelection_prodcat   as character format "x(4)"  no-undo.
define var zelection_char4     as character format "x(4)"  no-undo.
define var zelection_cust      like arsc.custno            no-undo.