/*****************************************************************************
 This program will interact with the arsc?lu.p series of programs that display the customer lookup frames. It will fill in a frame at the bottom the screen with additional customer information that can not fit in the lookup browses.
 Date: 10/0103. 
 SunSource 
*****************************************************************************/

{g-all.i}

def input parameter in-custno like arsc.custno.
def input parameter in-shipto like arss.shipto.
def var currdate as date format "99/99/99"  no-undo.
def var curryr   as c    format "x(4)"      no-undo.
def var lastyr   as c    format "x(4)"      no-undo.
def var procyr   as i    format "99"        no-undo.
def var prolyr   as i    format "99"        no-undo.
def var yr       as c    format "x(2)"      no-undo.

assign currdate = TODAY.
assign curryr = string(YEAR(currdate)).
assign yr  = substring(curryr,3,2).
assign procyr = integer(yr).
assign lastyr = string(YEAR(currdate - 365)).
assign yr = substring(lastyr,3,2).
assign prolyr = integer(yr).

{ar_defvars_lookupoe.i}

run sales_totaler. 
run fillin_custinfo.

procedure sales_totaler:
do:
  assign lu-slslstyr = 0
         lu-slsytd   = 0.
  if in-shipto = " " then 
  do:
      for each smsc where
               smsc.cono   = g-cono    and 
               smsc.yr     = procyr    and 
               smsc.custno = in-custno  
               no-lock:
        assign lu-slsytd = lu-slsytd +
                           smsc.salesamt[1]  + smsc.salesamt[2] + 
                           smsc.salesamt[3]  + smsc.salesamt[4] + 
                           smsc.salesamt[5]  + smsc.salesamt[6] + 
                           smsc.salesamt[7]  + smsc.salesamt[8] + 
                           smsc.salesamt[9]  + smsc.salesamt[10] + 
                           smsc.salesamt[11] + smsc.salesamt[12].
      end.
      for each smsc where
               smsc.cono   = g-cono    and 
               smsc.yr     = prolyr    and 
               smsc.custno = in-custno  
               no-lock:
        assign lu-slslstyr = lu-slslstyr +
                           smsc.salesamt[1]  + smsc.salesamt[2] + 
                           smsc.salesamt[3]  + smsc.salesamt[4] + 
                           smsc.salesamt[5]  + smsc.salesamt[6] + 
                           smsc.salesamt[7]  + smsc.salesamt[8] + 
                           smsc.salesamt[9]  + smsc.salesamt[10] + 
                           smsc.salesamt[11] + smsc.salesamt[12]. 

      end.
  end. 
  else 
  if in-shipto ne " " then 
  do:
      for each smsc where
               smsc.cono   = g-cono    and 
               smsc.yr     = procyr    and 
               smsc.custno = in-custno and 
               smsc.shipto = in-shipto 
               no-lock:
        assign lu-slsytd = lu-slsytd +
                           smsc.salesamt[1]  + smsc.salesamt[2] + 
                           smsc.salesamt[3]  + smsc.salesamt[4] + 
                           smsc.salesamt[5]  + smsc.salesamt[6] + 
                           smsc.salesamt[7]  + smsc.salesamt[8] + 
                           smsc.salesamt[9]  + smsc.salesamt[10] + 
                           smsc.salesamt[11] + smsc.salesamt[12].
      end.
      for each smsc where
               smsc.cono   = g-cono    and 
               smsc.yr     = prolyr    and 
               smsc.custno = in-custno and 
               smsc.shipto = in-shipto  
               no-lock:
        assign lu-slslstyr = lu-slslstyr +
                           smsc.salesamt[1]  + smsc.salesamt[2] + 
                           smsc.salesamt[3]  + smsc.salesamt[4] + 
                           smsc.salesamt[5]  + smsc.salesamt[6] + 
                           smsc.salesamt[7]  + smsc.salesamt[8] + 
                           smsc.salesamt[9]  + smsc.salesamt[10] + 
                           smsc.salesamt[11] + smsc.salesamt[12]. 

      end.
  end.   
end.
end procedure.  

procedure fillin_custinfo: 
do: 
  find first arsc where
             arsc.cono = g-cono and 
             arsc.custno = in-custno
   no-lock no-error.
 
   find first arss where
              arss.cono = g-cono and 
              arss.custno = in-custno  and
              arss.statustype = true 
    no-lock no-error.
 
   if avail arss then do:
     assign lu-shiptos  = "Shipto's Exist".
     lu-shiptos:dcolor in frame f-zcustinfo = 2.
   end.
   else do:
     assign lu-shiptos  = " ". 
     lu-shiptos:dcolor in frame f-zcustinfo = 0.
    end.     
   
    
   
   
   
   if avail arsc and in-shipto = " " then 
    do: 
      assign lu-custno   = arsc.custno  
             lu-name     = arsc.name  
             lu-addr1    = arsc.addr[1] 
             lu-restaddr = string(arsc.addr[2],"x(30)") +
                           " " + arsc.city + ", " + arsc.state 
             lu-city     = arsc.city   
             lu-state    = arsc.state  
             lu-repout   = arsc.slsrepout  
             lu-repnm1   = " "  
             lu-repin    = arsc.slsrepin  
             lu-repnm2   = " "  
             lu-crmgr    = arsc.creditmgr 
             lu-crmgrnm  = " ". 
      end.
    else  
    do:
      find first arss where
                 arss.cono = g-cono and 
                 arss.custno = in-custno and 
                 arss.shipto = in-shipto
                 no-lock no-error.
      if avail arss then 
       do:
          assign lu-custno   = arss.custno  
                 lu-name     = arss.name  
                 lu-addr1    = arss.addr[1] 
                 lu-restaddr = string(arss.addr[2],"x(30)") +
                               "      " + arss.city + ", " + arss.state 
                 lu-city     = arss.city   
                 lu-state    = arss.state  
                 lu-repout   = arss.slsrepout  
                 lu-repnm1   = " "  
                 lu-repin    = arss.slsrepin  
                 lu-repnm2   = " "  
                 lu-crmgr    = arsc.creditmgr 
                 lu-crmgrnm  = " ". 
       end.
    end.
      lu-restaddr = left-trim(lu-restaddr).
      find first smsn where
                 smsn.cono   = g-cono and 
                 smsn.slsrep = (if in-shipto = " " then 
                                   arsc.slsrepout
                                else 
                                   arss.slsrepout)
                 no-lock no-error.
       if avail smsn then 
          assign lu-repnm1 = smsn.name. 
       find first smsn where
                  smsn.cono   = g-cono and 
                  smsn.slsrep = (if in-shipto = " " then 
                                    arsc.slsrepin
                                 else 
                                    arss.slsrepin)
                  no-lock no-error.
       if avail smsn then 
          assign lu-repnm2 = smsn.name. 
       find oimsp where oimsp.person = arsc.creditmgr no-lock no-error.
       if avail oimsp then
          assign lu-crmgrnm = oimsp.name.
    run zsdisplitrep.p (input g-cono,
                        input in-custno,
                        input in-shipto,
                        input-output lu-repnm1).
 
    
    display lu-name      
            lu-addr1     
            lu-shiptos
            lu-restaddr     
            lu-repout    
            lu-repnm1     
            lu-repin
            lu-repnm2
            lu-crmgr     
            lu-crmgrnm   
            lu-slsytd 
            lu-slslstyr 
            with frame f-zcustinfo. 
end.           
end procedure.           

