define input parameter z-begdate as date.
define input parameter z-enddate as date.
define input parameter z-whse as c format "x(4)".
define input parameter z-prod as c format "x(24)".
define output parameter z-usage as de.
define output parameter z-usageval as de.

define var x-prod as character format "x(24)" no-undo.
/*
define var begindate as date init 04/30/2001 no-undo.
define var enddate as date init 05/01/2000 no-undo.
*/
assign z-usage = 0
       z-usageval = 0.

find first icsec where icsec.cono = 1 and icsec.rectype = "p" and
                      icsec.altprod = z-prod no-lock no-error.
if avail icsec then
  do:
  x-prod = icsec.prod.
  run issueaquire.
  x-prod = z-prod.
  run issueaquire.
  end.
else
  do:
  x-prod = z-prod.
  run issueaquire.
  end.






procedure issueaquire:

icetloop:

for each icet use-index k-postdt
  where icet.cono = 1 and icet.whse = z-whse
                      and icet.prod = x-prod 
                      and can-do("ri,in",icet.transtype)  no-lock:
  
  if icet.postdt < z-enddate then
    leave icetloop.
  
  if icet.postdt > z-begdate then
    next icetloop.
    
  if icet.transtype = "ri" and not can-do ("oe",icet.module) then
    next icetloop.
   

  
   
  if icet.module = "wt" then
    do:
    find wtel where wtel.cono = 1 and wtel.wtno = icet.orderno and
                    wtel.wtsuf = icet.ordersuf and
                    wtel.lineno = icet.lineno no-lock no-error.
    if not avail wtel then
      do:
      message "wt" icet.prod icet.whse icet.orderno icet.ordersuf
              icet.lineno icet.seqno.
      pause.
      end.
    else
    if wtel.transtype <> "do" then
       do:
       next icetloop.
       end.
   assign z-usage = z-usage + icet.stkqtyship.  
   assign z-usageval = z-usageval + 
                       (icet.stkqtyship * icet.cost).  

   end. 
  
  
  
  if icet.module = "oe" then
    do:
    find oeel where oeel.cono = 1 and oeel.orderno = icet.orderno and
                    oeel.ordersuf = icet.ordersuf and
                    oeel.lineno = icet.lineno no-lock no-error.
    if not avail oeel then
      do:
      message "oe" icet.prod icet.whse icet.orderno icet.ordersuf
              icet.lineno icet.seqno.
      pause.
      end.
    else
    if oeel.botype = "d" or oeel.transtype = "do" or
       (oeel.specnstype = "n" and oeel.kitfl = false) or 
       (icet.transtype = "ri" and oeel.transtype <> "rm") then
       do:
        next icetloop.
        /*
        message "oe" icet.prod icet.whse icet.orderno icet.ordersuf
               icet.lineno icet.seqno.
        pause.
        */
       end.
    if icet.seqno <> 0 then
      do:
      find oeelk where oeelk.cono = 1 and 
                  oeelk.ordertype = (if icet.module = "oe" then
                                      "o" 
                                     else 
                                      "w") and
                  oeelk.orderno = icet.orderno and
                  oeelk.ordersuf = icet.ordersuf and
                  oeelk.lineno = icet.lineno and
                  oeelk.seqno = icet.seqno no-lock no-error.

      if not avail oeelk then
        do:
        message "oe" icet.prod icet.whse icet.orderno icet.ordersuf
                icet.lineno icet.seqno.
        pause.
        next icetloop.
        end.
       if avail oeelk and oeelk.specnstype = "n" then
        next icetloop.
      end.    
    else
    if (oeel.specnstype = "n" and oeel.kitfl = true) then
      next icetloop.
      
      assign z-usage = z-usage + (icet.stkqtyship
          * (if icet.transtype = "ri" and oeel.transtype = "rm" then
               -1
            else
               1)).
               
      assign z-usageval = z-usageval + ((icet.stkqtyship
          * (if icet.transtype = "ri" and oeel.transtype = "rm" then
               -1
            else
               1)) * icet.cost).
               

    
    
    end.
  else
  if icet.module = "kp" then
    do:
    if icet.transtype <> "in" then
      next icetloop.
    
    find first oeelk where oeelk.cono = 1 and 
                oeelk.ordertype = (if icet.module = "oe" then
                                    "o" 
                                   else 
                                    "w") and
                oeelk.orderno = icet.orderno and
                oeelk.ordersuf = icet.ordersuf and
                oeelk.shipprod = icet.prod
                no-lock no-error.
    if avail oeelk and oeelk.specnstype = "n" then
      next icetloop.

    assign z-usage = z-usage + icet.stkqtyship.  
    assign z-usageval = z-usageval + 
                       (icet.stkqtyship * icet.cost).  

    end.   
  else
  if icet.seqno <> 0 and icet.module = "va" then
    do:
    if icet.transtype <> "in" then
      next icetloop.

    find vaesl where vaesl.cono = 1 and 
                     vaesl.vano = icet.orderno and
                     vaesl.vasuf = icet.ordersuf and
                     vaesl.seqno = icet.seqno and
                     vaesl.lineno = icet.lineno no-lock no-error.
    if vaesl.nonstockty = "n" then
      next icetloop.
    
    if not avail vaesl then
      do:   
      message "va" icet.prod icet.orderno icet.ordersuf icet.lineno icet.seqno.
      pause.
      end.   

   assign z-usage = z-usage + icet.stkqtyship.  
   assign z-usageval = z-usageval + 
                       (icet.stkqtyship * icet.cost).  

   
   end. 
end.
end.
