/***************************************************************************
 PROGRAM NAME: oipz.p
  PROJECT NO.: 
  DESCRIPTION: This program gives user print and download capabilities
               for EDI PO's or any file in the /usr/tmp directory.
***************************************************************************/

def var s-file      as c format "x(40)"           no-undo.
def shared var s-printer   like sasp.printernm    no-undo.
def var o-printer   like sasp.printernm           no-undo.
def var v-printdir  like sasc.printdir            no-undo.
def shared var s-directory as char format "x(40)" no-undo.
def var v-string    as c format "x(80)"           no-undo.
def var v-foundfile as c format "x(40)"           no-undo.
def shared var z-filename  as c format "x(70)"    no-undo.
def shared var s-filename  as c format "x(70)"    no-undo.
def var z-shellcmd  as c format "x(70)"           no-undo.
def var z-accessfl  as log.

def shared var v-sapbjobnm like sapb.jobnm       no-undo.

{g-all.i}

main:
repeat:

/* do for sasc:
   {w-sasc.i no-lock}
   v-printdir = sasc.printdir.
end.   
*/
/*
message "s-directory"  s-directory 
        "s-filename" s-filename. pause.
*/        

   assign v-printdir = s-directory.
/*   message "s-filename" s-filename. pause.  */
/*   assign s-file = s-directory + s-filename.   */
    assign s-file = s-directory + s-filename.  
/*  message "s-file"    s-file
           "s-printer" s-printer
           "s-directory" s-directory
           "v-printdir" v-printdir. pause.     */

{w-sasp.i s-printer no-lock}

/* message "sasp" sasp.ptype. pause.  */
if sasp.ptype = "v" then 
   do:
      if sasp.pinit[1] ne 0 then 
         do:
           v-string = "". 
           do i = 1 to 10: 
              if sasp.pinit[i] ne 0 then
                 v-string = v-string + chr(sasp.pinit[i]).
           end.
           do:
            put screen v-string.
            leave main.
           end. 
         end.
      
      z-filename = s-file.
      /* message "z-filename"  z-filename. pause.   */
      {s-rpctlv.i &filename = "set"
                  &skipdelete = "/*"} 
      v-string = "".
      
      if sasp.pnormal[1] ne 0 then              
         do i = 1 to 10:
            if sasp.pnormal[i] ne 0 then
                 v-string = v-string + chr(sasp.pnormal[i]).
         end.
      if v-string ne "" then 
         do:
          put screen v-string.
          leave main.
         end. 
   end.
end. /**** end of do while  ******/

    /* remove file */
    os-command silent
    value( "rm " + s-file).
        
 
