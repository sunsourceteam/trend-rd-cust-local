/*h****************************************************************************
  INCLUDE      : vaetsled.lpr
  DESCRIPTION  : Value Add Entry Transactions - Line Items - Edit Program
  USED ONCE?   : no
  AUTHOR       : cm
  DATE WRITTEN : 11/28/05
  CHANGES MADE :
    11/28/05 cm; TB# e23236 Split out code for common usage
******************************************************************************/

g-prod = if s-lnshipprod <> "" then s-lnshipprod
         else g-prod.

if s-lnnonstockty <> "n":u and s-lnshipprod <> "" then do:

    {w-icsp.i s-lnshipprod no-lock}

    /* There must be an ICSP record on file */
    if not avail icsp then do:
        run err{&errorno}.p(4600).

        {&comui}
        next-prompt s-lnshipprod with frame f-vaetsll.
        next upd.
        /{&comui}* */

        {&errorlogic}
    end. /* if not avail icsp */

    v-icspecrecno = if v-scrollfl = no then
                        icsp.icspecrecno
                    else if o-lnshipprod ne s-lnshipprod then
                        icsp.icspecrecno
                    else
                        b-vaesl.icspecrecno.

    {icss.gfi
        &prod        = g-prod
        &icspecrecno = v-icspecrecno
        &lock        = "no"}

    /* The product (ICSP) cannot be inactive */
    if icsp.statustype = "i":u then do:
        run err{&errorno}.p(4735).

        {&comui}
        next-prompt s-lnshipprod with frame f-vaetsll.
        next upd.
        /{&comui}* */

        {&errorlogic}
    end. /* if icsp.statustype = "i" */

    /* For External/Inspection section - only allow labor products. */
    if can-do("ex,is":u,s-sctntype) and
        icsp.statustype <> "l":u
    then do:
        run err{&errorno}.p(6511).

        {&comui}
        next-prompt s-lnshipprod with frame f-vaetsll.
        next upd.
        /{&comui}* */

        {&errorlogic}
    end. /* if can-do */

    /* Product cannot be a BOD Kit (ICSP) */
    if icsp.kittype = "b":u then do:
        run err{&errorno}.p(5624).

        {&comui}
        next-prompt s-lnshipprod with frame f-vaetsll.
        next upd.
        /{&comui}* */

        {&errorlogic}
    end. /* if icsp.kittype = "b" */

    /* Display any required product notes */
    {&comui}
    if icsp.notesfl = "!":u then
        run noted.p (true,"P":u,s-lnshipprod,"").
    /{&comui}* */

    {w-icsw.i s-lnshipprod s-lnwhse no-lock}

    /* There must a valid ICSW record on file */
    if not avail icsw then do:
        run err{&errorno}.p(4602).

        {&comui}
        next-prompt s-lnshipprod with frame f-vaetsll.
        next upd.
        /{&comui}* */

        {&errorlogic}
    end. /* if not avail icsw */

    /* If the section type = "in" (Inventory Components) and the
       product is an order as needed product, then make it a
       special. */
    assign
         {speccost.gas
             &com = "/*"}
         s-lnnonstockty  = if icsw.statustype = "o":u and
                               s-sctntype = "in":u and
                               g-ourproc <> "vaes":u
                           then
                               "s":u
                           else
                               s-lnnonstockty
         s-lnproddesc    = icsp.descrip[1]
         s-lnproddesc2   = icsp.descrip[2]
         s-lnprodnotesfl = icsp.notesfl
         s-lnunit        = if v-scrollfl = yes then s-lnunit
                           else if icsw.unitbuy = "" then icsp.unitstock
                           else icsw.unitbuy
         v-unitconvfl    = icsp.unitconvfl
         v-unitstock     = icsp.unitstock
         v-icspstatus    = icsp.statustype
         v-icswstatus    = icsw.statustype
         v-wmfl          = icsw.wmfl
         v-serlottype    = icsw.serlottype
         v-boshortfl     = icsw.boshortfl
         s-lnprodcat     = icsp.prodcat
         s-lncubes       = icsp.cubes
         s-lnweight      = icsp.weight
         s-lnusagefl     = if icsp.statustype = "l":u then no
                           else if s-lnnonstockty = "s":u and
                               icsw.statustype <> "o":u and
                               s-lnnonstockty <> o-lnnonstockty
                           then
                               no
                           else if s-lnnonstockty = "" and
                               s-lnnonstockty <> o-lnnonstockty
                           then
                               yes
                           else if s-lndirectfl = yes then no
                           else s-lnusagefl.


/* // User Hook // */
  assign    s-lnunit     = if v-scrollfl = yes then s-lnunit
                           /*
                           else if icsw.unitbuy = "" then icsp.unitstock
                           else icsw.unitbuy
                           */
                           else if icsp.unitsell = "" then
                              icsp.unitstock 
                           else icsp.unitsell.


    {p-unit.i &prod       = "s-lnshipprod"
              &unitconvfl = "v-unitconvfl"
              &unitstock  = "v-unitstock"
              &unit       = "s-lnunit"
              &desc       = "v-desc"
              &conv       = "s-lnunitconv"
              &confirm    = "confirm"}.

    if not confirm then s-lnunitconv = 1.

    /* Establish the cost for the VA line item */
    {valncost.las &sasc           = "v-"
                  &sctntype       = "s-sctntype"
                  &oldshipprod    = "o-lnshipprod"
                  &shipprod       = "s-lnshipprod"
                  &unitconv       = "s-lnunitconv"
                  &icspstatustype = "v-icspstatus"}

    /* If the VA line item is an Inventory In section, check to
       see if the same product exists on another IN section.
       Need to use the same cost that went out. */
    if s-sctntype = "ii":u and
        s-lnnonstockty ne "n":u and
        s-lncostoverfl = no
    then do for b2-vaesl:
        find first b2-vaesl use-index k-prod where
                   b2-vaesl.cono       = g-cono       and
                   b2-vaesl.shipprod   = s-lnshipprod and
                   b2-vaesl.whse       = s-lnwhse     and
                   b2-vaesl.sctntype   = "in":u       and
                   b2-vaesl.completefl = yes          and
                   b2-vaesl.vano       = g-vano       and
                   b2-vaesl.vasuf      = g-vasuf
        no-lock no-error.

        if avail b2-vaesl then
            assign
                 s-lnprodcost   = b2-vaesl.prodcost
                 s-lncostoverfl = yes.
    end. /* if s-sctntype = "ii" */

    o-prodcost = s-lnprodcost.

    /* Set the ARP fields (variables) */
    {valnarpf.las &vaeslprefix = "s-ln"}

    /* Calculate ICSW net available */
    {p-netavl.i "/*"}

    assign
        s-qtyonorder = icsw.qtyonorder
        s-netavail   = if s-vatranstype ne "qu":u and
                           o-lnnonstockty = s-lnnonstockty and
                           o-lnshipprod = s-lnshipprod
                       then
                           s-netavail + s-lnstkqtyship
                       else
                           s-netavail
        s-netavail   = s-netavail / s-lnunitconv
        s-netavail   = truncate(round(s-netavail,3),2)
        s-qtyonorder = icsw.qtyonorder / s-lnunitconv.

    if avail icsp and icsp.statustype = "l":u then s-netavail = 0.

end. /* if s-lnnonstockty <> "n" */

else if s-lnshipprod ne "" then do:
    assign
        s-lnunitconv = 1.

        {icss.gfi &prod        = s-lnshipprod
                  &type        = "last"
                  &lock        = "no"}

    assign
        v-icspecrecno = if avail icss then
                            icss.icspecrecno
                        else 0
        v-speccostty  = ""
        v-csunperstk  = 1
        v-prccostper  = ""
        v-specconv    = 1.

    {icsscrt.gpr
        &cond        = "not avail icss or"
        &speccostty  = "' '"
        &csunperstk  = "1"
        &prccostper  = "' '"
        &assign      = "v-icspecrecno =
                          v-icspecrecno + 1."
        &file        = "icss."
        &cono        = g-cono
        &prod        = s-lnshipprod
        &getrecno    = no
        &icspecrecno = v-icspecrecno
        &statusfl    = no}

end. /* else do */

assign
    v-prevunit     = s-lnunit
    v-prevunitconv = s-lnunitconv
    s-lnqtyord     = if s-lnqtybasetotfl = yes then
                         s-vastkqtyship * s-lnqtyneeded
                     else s-lnqtyneeded
    s-lnstkqtyord  = s-lnqtyord * s-lnunitconv.

/* Establish the quantity shipped / stocking qty shipped */
/* Only do this for a new line added or when the product/nonstockty
   field was changed.  Don't want to change the qty shipped once
   it's established. */
if s-lnshipprod ne o-lnshipprod or
    s-lnnonstockty ne o-lnnonstockty
then do:
    {valnqtys.las}
end.

assign
    s-lnextcubes   = s-lncubes  * s-lnstkqtyship
    s-lnextweight  = s-lnweight * s-lnstkqtyship.
