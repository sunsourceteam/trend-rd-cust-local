/* icin.lpr 1.1 01/03/98 */
/* icin.lpr 1.1 11/1/93 */
/*h*****************************************************************************
  INCLUDE      : icin.lpr
  DESCRIPTION  : Detail description
  USED ONCE?   : Yes
  AUTHOR       : RS
  DATE WRITTEN : 10/14/93
  CHANGES MADE : 09/22/94 kjb; TB# 16648; F7-Summary not capitalized
*******************************************************************************/

find b-icenh where recid(b-icenh) = v-recid[frame-line(f-icind)]
    no-lock no-error.
if not avail b-icenh then do:
    run err.p(2007).
    next dsply.
end.

if {k-func6.i} then do:
    run icinl.p.
    put screen row 21 col 3 color message
 " F6-Detail      F7-Summary      F8-All Trans.     F9-Active Only            ".
end.
/*tb 16648 09/22/94 kjb; F7-Summary not capitalized */
else if {k-func7.i} then do:
    put screen row 21 col 3 color message
 " F6-Detail      F7-SUMMARY      F8-All Trans.     F9-Active Only            ".

    run icins.p.
    put screen row 21 col 3 color message
 " F6-Detail      F7-Summary      F8-All Trans.     F9-Active Only            ".
end.
else if {k-func8.i} then do:
     run icinlz.p (input s-typendcd).
     put screen row 21 col 3 color message
 " F6-Detail      F7-Summary      F8-All Trans.     F9-Active Only            ".
end.    
else if {k-func9.i} then do:
     run icinla.p (input s-typendcd).
     put screen row 21 col 3 color message
 " F6-Detail      F7-Summary      F8-All Trans.     F9-Active Only            ".
end.    




if {k-cancel.i} then next dsply.
else if {k-jump.i} then leave main.
