/****************************************************************************
*                                                                           *
* PDXMI.P  -  Sales Analysis Matrix Report                                  *
*                                                                           *
****************************************************************************/


{p-rptbeg.i} 
def new shared var b-region        as c    format "x(4)"             no-undo.
def new shared var e-region        as c    format "x(4)"             no-undo.
def new shared var b-customer      as de   format "999999999999"     no-undo.
def new shared var e-customer      as de   format "999999999999"     no-undo.
def new shared var b-shipto        as c    format "x(8)"             no-undo.
def new shared var e-shipto        as c    format "x(8)"             no-undo.
def new shared var b-salesrep      as c    format "x(4)"             no-undo.
def new shared var e-salesrep      as c    format "x(4)"             no-undo.
def new shared var b-ptype         as c    format "x(4)"             no-undo.
def new shared var e-ptype         as c    format "x(4)"             no-undo.

def new shared var p-date          as date format "99/99/9999"       no-undo.
def new shared var p-custfile      as c    format "x(24)"            no-undo.
def new shared var p-infile        as c    format "x(24)"            no-undo.
def new shared var p-update        as logical                        no-undo.
def new shared var p-print         as logical                        no-undo.
def new shared var p-contracts     as logical                        no-undo.
def new shared var p-calcfutr      as c    format "x(1)"             no-undo.
def new shared var p-increase      as de   format "99.9"             no-undo.
def new shared var p-0sales        as logical                        no-undo.

def            var w-customer      as de   format "999999999999"     no-undo.
def            var w-shipto        as c    format "x(8)"             no-undo.
def            var w-cpricetype    as c    format "x(4)"             no-undo.
def            var w-name          as c    format "x(8)"             no-undo.
def            var r-year          as i    format "99"               no-undo.
def            var z-year          as c    format "x(4)"             no-undo.
def            var s-date          as c    format "x(8)"             no-undo.
def            var s-order         as c    format "x(10)"            no-undo.
def            var s-line          as c    format "x(3)"             no-undo.
def            var s-isales        as de   format ">,>>>,>>9-"       no-undo.
def            var s-csales        as de   format ">,>>>,>>9-"       no-undo.
def            var s-fsales        as de   format ">,>>>,>>9-"       no-undo.
def            var s-bsales        as de   format ">,>>>,>>9-"       no-undo.
def            var s-asales        as de   format ">,>>>,>>9-"       no-undo.
def            var s-dbsales       as de   format ">,>>>,>>9-"       no-undo.
def            var s-dasales       as de   format ">,>>>,>>9-"       no-undo.
def            var s-imarg         as de   format ">>>>9.99-"        no-undo.
def            var s-cmarg         as de   format ">>>>9.99-"        no-undo.
def            var s-fmarg         as de   format ">>>>9.99-"        no-undo.
def            var s-bmarg         as de   format ">>>>9.99-"        no-undo.
def            var s-amarg         as de   format ">>>>9.99-"        no-undo.
def            var s-dbmarg        as de   format ">>>>9.99-"        no-undo.
def            var s-damarg        as de   format ">>>>9.99-"        no-undo.
def            var s-vs1           as de   format ">,>>9.99"         no-undo.
def            var s-vs2           as de   format ">,>>9.99"         no-undo.

def            var w-margin        as de   format ">>>>>>>9.99-"     no-undo.
def            var matrix-price    as de   format ">>>>>>9.999"      no-undo.
def            var f-matrix-price  as de   format ">>>>>>9.999"      no-undo.
def            var matrix-pct      as de   format ">>>>9.999"        no-undo.
def            var w-errmsg        as c    format "x(65)"            no-undo.

def            var w-recno         as i    format "9999999"          no-undo.
def            var w-level         as i    format "9"                no-undo.
def            var x-level         as i    format "9"                no-undo.
def            var w-enddt         as date format "99/99/99"         no-undo.
def            var w-trndt         as date format "99/99/99"         no-undo.
def            var w-disctp        as c    format "x(5)"             no-undo.
def            var w-qtybrk        as c    format "x(1)"             no-undo.
def            var wk-sales        as de   format "->>,>>9.99"       no-undo.
def            var wk-cost         as de   format "->>,>>9.99"       no-undo.
def            var wk-disc         as de   format "->>,>>9.99"       no-undo.
def            var w-description   as c    format "x(24)"            no-undo.
def            var w-pricetype     as c    format "x(4)"             no-undo.
def            var w-stndcost      as de   format ">>>>>>>9.99-"     no-undo.
def            var w-listprice     as de   format ">>>>>>>9.99-"     no-undo.
def            var w-product       as c    format "x(24)"            no-undo.
def            var w-ytdsales      as de   format "->>,>>>,>>9.99"   no-undo.
def            var w-lastyr        as c    format "x(4)"             no-undo.
def            var wk-lastyr       as c    format "x(2)"             no-undo.
def            var z-lastyr        as i    format "99"               no-undo.
def            var w-slsrepout     like    arsc.slsrepout            no-undo.

/* Input Fields */
def            var i-slsrep        as c    format "x(4)"             no-undo.
def            var i-cust          as de   format "999999999999"     no-undo.
def            var i-shipto        as c    format "x(8)"             no-undo.
def            var i-name          as c    format "x(12)"            no-undo.
def            var i-ctype         as c    format "x(4)"             no-undo.
def            var i-pptype        as c    format "x(4)"             no-undo.
def            var i-prod          as c    format "x(24)"            no-undo.
def            var i-prodsales     as c    format "x(12)"            no-undo.
def            var i-lastsell      as de   format ">>,>>9.99-"       no-undo.
def            var i-asell         as c    format "x(10)"            no-undo.
def            var i-adiscmult     as c    format "x(11)"            no-undo.
def            var i-amrg          as c    format "x(10)"            no-undo.
def            var i-csell         as c    format "x(10)"            no-undo.
def            var i-cmrg          as c    format "x(10)"            no-undo.
def            var i-cpdlvl        as i    format "9"                no-undo.
def            var i-fsell         as c    format "x(10)"            no-undo.
def            var i-fdiscmult     as c    format "x(11)"            no-undo.
def            var i-fmrg          as c    format "x(10)"            no-undo.
def            var i-fpdlvl        as i    format "9"                no-undo.
def            var i-pdrecno       as i    format ">>>>>>9"          no-undo.
def            var i-change        as c    format "x(1)"             no-undo.
def            var i-factor        as de   format ">>,>>9.99-"       no-undo.

def  var s-imrgsales      as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-cmrgsales      as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-fmrgsales      as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-bmrgsales      as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-amrgsales      as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-dbmrgsales     as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-damrgsales     as de format ">>>,>>>,>>9-"  init 0   no-undo.


def new shared var cur-mo           as i    format "99"       no-undo.
def new shared var cur-yr           as i    format "9999"     no-undo.
def new shared var last-mo          as i    format "99"       no-undo.
def new shared var last-yr          as i    format "99"       no-undo.
def new shared var w-year           as c    format "x(4)"     no-undo.

def new shared var w-month          as c    format "x(9)"       no-undo.
def new shared var w-date           as c    format "x(10)"      no-undo.
def new shared var work-date        as date format "99/99/9999" no-undo.
def new shared var b1-date          as date format "99/99/9999" no-undo.
def new shared var e1-date          as date format "99/99/9999" no-undo.
def new shared var b2-date          as date format "99/99/9999" no-undo.
def new shared var e2-date          as date format "99/99/9999" no-undo.
def new shared var beg-date         as date format "99/99/9999" no-undo.
def new shared var leapyr           as de   format "9999.99"    no-undo.
def new shared var leaptst          as c    format "x(7)"       no-undo.

def new shared var  pd-whse       like icsw.whse                no-undo.
def new shared var  pd-prod       like icsw.prod                no-undo.
def new shared var  pd-cust       like arsc.custno              no-undo.
def new shared var  pd-shipto     like arss.shipto              no-undo.
def new shared var  pd-totnet     as dec format ">>>,>>9.99-"   no-undo.
def new shared var  pd-totcost    as dec format ">>>,>>9.99-"   no-undo.
def new shared var  pd-vendrebamt as dec format ">>>,>>9.99-"   no-undo.
def new shared var  pd-margin     as dec format ">>>,>>9.99-"   no-undo.
def new shared var  pd-margpct    as dec format ">>>,>>9.99-"   no-undo.
def new shared var  pd-netunit    as dec format ">>>,>>9.99-"   no-undo.
def new shared var  pd-pdrecno    like pdsc.pdrecno             no-undo.
def new shared var  pd-levelcd    like pdsc.levelcd             no-undo.
def new shared var  pd-price      as dec format ">>>,>>9.99-"   no-undo.

def var v-qtyord      like    oeel.qtyord           no-undo.
def var v-unitbuy     like    icsw.unitbuy          no-undo.

def            var w-price          like icsw.listprice        no-undo.
def            var w-qtyship        like oeel.qtyship          no-undo.
def            var t-quantity       like oeel.qtyord           no-undo.

def            var w-prcincr        as de   format "9.999"     no-undo.
def            var w-pctincr        as de   format "99.999"    no-undo.
def            var w-costincr       as de   format "99.999"    no-undo.
def            var w-actualprc      as de   format "99999.99"  no-undo.

def var y                as i    format "99"                  no-undo.
def var b                as i    format "99"                  no-undo.
def var z                as i    format "99"                  no-undo.

def var calyr            as logical                           no-undo.

def var b-order          like oeeh.orderno                    no-undo.
def var e-order          like oeeh.orderno                    no-undo.
def var b-lastyr         as date format "99/99/9999"          no-undo.
def var e-lastyr         as date format "99/99/9999"          no-undo.

def var ex-order         as i    format "9999999"             no-undo.
def var ex-suffix        as i    format "99"                  no-undo.
def var contract-found   as logical                           no-undo.

def var nextfl           as logical                           no-undo.
def var sdi-cost         like oeel.prodcost                   no-undo.
def var cf-cost          like oeel.prodcost                   no-undo.
def var zx-valid         as logical                           no-undo.
def var zx-pct           as decimal                           no-undo.
def var zx-rebate        as decimal                           no-undo.



def stream exclude_oeeh.
/*
def stream customer.
*/

/* the following was added for rebate cost */
def new shared buffer b-oeel for oeel.
def new shared buffer b2-oeel for oeel.
def new shared buffer b-oeeh for oeeh.
def new shared buffer b2-oeeh for oeeh.

def new shared var v-smwodiscfl    like sasc.smwodiscfl  initial no no-undo.
def new shared var v-oecostsale    like sasc.oecostsale  initial no no-undo.
def new shared var v-smcustrebfl   like sasc.smcustrebfl initial no no-undo.
def new shared var v-smvendrebfl   like sasc.smvendrebfl initial no no-undo.

def new shared var v-custrebamt    as dec format ">>>>9.99-"       no-undo.
def new shared var v-vendrebamt    as dec format ">>>>9.99-"       no-undo.
def new shared var v-totvendrebamt as dec format ">>>>9.99-"       no-undo.
def new shared var v-totcustrebamt as dec format ">>>>9.99-"       no-undo.

def new shared var v-csunperstk    like icss.csunperstk  initial 1  no-undo.
def new shared var v-specconv      as integer            initial 1  no-undo.
def new shared var v-prccostper    like icss.prccostper             no-undo.
def new shared var v-speccostty    like icss.speccostty             no-undo.
def new shared var v-icspecrecno   like icss.icspecrecno            no-undo.
def new shared var vc-csunperstk   like icss.csunperstk  initial 1  no-undo.
def new shared var vc-specconv     as integer            initial 1  no-undo.
def new shared var vc-prccostper   like icss.prccostper             no-undo.
def new shared var vc-speccostty   like icss.speccostty             no-undo.
def new shared var vc-icspecrecno  like icss.icspecrecno            no-undo.
def new shared var v-spcconvertfl  as logical            initial no no-undo.

def            var p-cost          as c   format "x(1)"  initial "g"  no-undo.
def            var v-cost          as de  format ">>>>>>>9.999".
/* rebate cost end */

def buffer bk-oeel for oeel.
def buffer br-oeel for oeel.

def stream i-pdscfile.
def stream o-pdscfile.
def stream dryrun.

def new shared temp-table matrix 
    field region        as c    format "x(1)"
    field district      as c    format "x(3)"
    field salesrep      as c    format "x(4)"
    field customer      as de   format ">>>>>>>>>>>9"
    field ship-to       as c    format "x(8)"
    field name          as c    format "x(8)"
    field ytdsales      as de   format "->>,>>>,>>9"
    field i-ytd-amt     as de   format ">>>>>>9"
    field cprice-type   as c    format "x(4)"
    field pprice-type   as c    format "x(4)"
    field pd-recno      as i    format "9999999"
    field pd-level      as i    format "9"
    field f-level       as i    format "9"
    field product       as c    format "x(24)"
    field i-netprc      as de   format ">>,>>9.99"
    field i-listprc     as de   format ">>,>>9.99"
    field i-amount      as de   format ">>>>>>>>>9.99-"
    field i-cost        as de   format ">>>>>>>9.99-"
    field i-discount    as de   format ">>>>>>>>>9.99-"
    field i-discmult    as de   format ">>,>>9.999"
    field c-netprc      as de   format ">>,>>9.99"
    field c-amount      as de   format ">>>>>>>>>9.99-" 
    field c-cost        as de   format ">>>>>>>9.99-"   
    field c-discount    as de   format ">>>>>>>>>9.99-" 
    field f-netprc      as de   format ">>,>>9.99"
    field f-discmult    as de   format ">>,>>9.999"
    field f-amount      as de   format ">>>>>>>>>9.99-"
    field f-cost        as de   format ">>>>>>>9.99-"  
    field f-discount    as de   format ">>>>>>>>>9.99-"
    field change        as c    format "x(1)"
    field change-amt    as de   format ">,>>>.99"
    field i-pdtype      as c    format "x(1)"
    field f-pdtype      as c    format "x(1)"
    field usage00       as i    format "99999"
    field usage01       as i    format "99999"
    field usage02       as i    format "99999"
    field p-cat         as c    format "x(4)"
    field pd-enddt      as date format "99/99/99"
    field pd-trndt      as date format "99/99/99"
    field inact-amount  as de   format ">>>>>>>>9.99-"
    field inact-cost    as de   format ">>>>>>>9.99-"
    
                            
  index k-salesrep       
    region
    district
    salesrep
    customer
    ship-to
    product
    i-ytd-amt descending.
    
/* forms */
{f-pdxmi.i}


output stream o-pdscfile to "/usr/tmp/pdscfile".
output stream o-pdscfile close.

/*
output stream dryrun to value(p-infile + "_dr").
output stream dryrun close.
*/
    
assign b-region        = sapb.rangebeg[1]
       e-region        = sapb.rangeend[1]
       b-salesrep      = sapb.rangebeg[3]
       e-salesrep      = sapb.rangeend[3]
       b-ptype         = sapb.rangebeg[4]
       e-ptype         = sapb.rangeend[4].

if e-region = "" then assign e-region = "~~~~".
if e-salesrep = "" then assign e-salesrep = "~~~~".
if e-ptype = "" then assign e-ptype = "~~~~".

assign p-custfile      = sapb.optvalue[1]
       p-infile        = sapb.optvalue[3]
       p-update        = if sapb.optvalue[4] = "yes" then yes
                         else no
       p-print         = if sapb.optvalue[5] = "yes" then yes
                         else no
       p-contracts     = if sapb.optvalue[6] = "yes" then yes
                         else no
       p-calcfutr      = sapb.optvalue[7]
       p-increase      = dec(sapb.optvalue[9])
       p-0sales        = if sapb.optvalue[10] = "yes" then yes
                         else no.



if substring(b-region,2,3) = "" 
   then assign substring(b-region,2,3) = "000".
   
if substring(e-region,2,3) = ""
   then assign substring(e-region,2,3) = "999".
   
if sapb.optvalue[2] = "" then        
  p-date  = today.                   
else                                 
  do:                                
    v-datein = sapb.optvalue[2].
    {p-rptdt.i}
    if string(v-dateout) = v-lowdt then
       p-date  = today.
    else
       p-date  = v-dateout.
  end.
  
/*
/* Error Conditions */
  assign w-errmsg = "".
  if p-infile = "" and p-update = yes then
    assign w-errmsg = 
    "Update cannot be chosen if an update file is not entered".

  if w-errmsg ne "" then do:
     display w-errmsg with frame f-error.
     leave.
  end.
*/

if p-update = yes then
  do:
   display "UPDATE capabilities have been DISABLED".
   leave.
end.

/* begin processing */

put control "~033~046~1541~117".   /* landscape format */

assign cur-mo = MONTH(p-date)
       cur-yr = YEAR(p-date)
       w-date = string(p-date,"99/99/9999").
       
assign substring(w-date,1,2) = "01"
       substring(w-date,4,2) = "01".

if cur-yr = 2003 then do:
   assign calyr = yes.
   run "get-last-year".
   assign calyr = no.
end.
else do:
   find sasc use-index k-sasc where sasc.cono = g-cono
                                    no-lock no-error.
   if avail sasc then do:
      do i = 1 to 12:
         if sasc.glenddt[i] >= p-date then do:
            assign e1-date = sasc.glenddt[i]
                   beg-date = date(w-date).
            leave.
         end.
      end.
   end.
end.

if p-infile = "" then do:
   view frame f-header.
   view frame f-trailer.
end.
else do:
   if p-update = yes then do:
      view frame f-header-a.
      view frame f-trailer.
   end.
   else do:
      output stream dryrun to value(p-infile + "_dr").
      output stream dryrun close.
   end.
end.


if p-custfile <> "" then do:

   input from value(p-custfile).
   repeat:
      import delimiter ","
         i-slsrep i-cust i-shipto.
      assign b-customer = i-cust
             e-customer = i-cust
             b-shipto   = i-shipto
             e-shipto   = i-shipto.
      assign w-prcincr  = 1 + (p-increase / 100)
             w-pctincr  = p-increase
             w-costincr = 1.000.
      run "process-customer".
   end.
end.
else do:
   if p-infile <> "" then do:
      input from value(p-infile).
      repeat:
         import delimiter ","
            i-slsrep    i-cust      i-shipto i-name   i-ctype   i-pptype
            i-prod      i-prodsales i-lastsell
            i-asell     i-adiscmult i-amrg
            i-csell                 i-cmrg   i-cpdlvl
            i-fsell     i-fdiscmult i-fmrg   i-fpdlvl
            i-pdrecno   i-change    i-factor.
         
         assign i-change = substring(i-change,1,1).
   /**/
         if (i-change = "Y" or i-change = " ") and (i-factor = 0 or
                                                    i-factor = 0.1) then
            assign i-factor = p-increase
                   i-change = "Y".
            
         assign b-customer = i-cust
                e-customer = i-cust
                b-shipto   = i-shipto
                e-shipto   = i-shipto.
         
         assign w-prcincr  = if (i-change = "Y" or i-change = " ") and 
                                (i-factor = 0 or i-factor = 0.1)
                                 then (1 + (p-increase / 100))

                             else 
                                if (i-change = "N" or i-change = "H" or
                                    i-change = "I")
                                   and i-factor = 0 then 1
                                else
                                   if (i-change = "N" or i-change = "H" or
                                       i-change = "I") and 
                                       i-factor > 0 then
                                       1
                                       /*
                                      (i-factor / 100) + 1
                                      */
                                   else (i-factor / 100) + 1 
                w-pctincr  = if (i-change = "Y" or i-change = " ") and 
                                (i-factor = 0 or i-factor = 0.1)
                                 then p-increase
                             else 
                                if i-change = "Y" and i-factor = 0.9
                                   then 1
                                else
                                   if (i-change = "N" or i-change = "H" or
                                       i-change = "I") and 
                                      i-factor <> 0
                                      then i-factor
                                   else
                                      i-factor.
                w-costincr = 1.000.
         run "analyze-customer".
      end.
   end.
   else do:
      assign b-customer      = dec(sapb.rangebeg[2])
             e-customer      = dec(sapb.rangeend[2])
             b-shipto        = ""
             e-shipto        = "~~~~~~~~"
             w-prcincr  = 1 + (p-increase / 100)
             w-pctincr  = p-increase
             w-costincr = 1.000.
      run "process-customer".
   end.
end.

if p-0sales = yes then do:
   run "get-0-sales-info".
end.

run "sort-by-slsrep".

if s-dbsales <> 0 then
   assign s-dbmarg = (s-dbmrgsales / s-dbsales) * 100.
if s-dasales <> 0 then
   assign s-damarg = (s-damrgsales / s-dasales) * 100.
   
if p-infile <> "" then do:
   if p-update = no then do:
      output stream dryrun to value(p-infile + "_dr") append.
      export stream dryrun delimiter ","
         i-slsrep
         ""  /* customer #  */
         ""  /* shipto      */
         ""  /* cust name   */
         ""  /* cust type   */
         "GRAND TOTAL MARGINS:"  /* part number */
         ""  /* last sell   */
         ""  /* list price  */
         ""  /* disc mult   */
         ""  /* old inv mrg */
         ""  /* last sell   */
         ""  /* list price  */
         ""  /* disc mult   */
         s-dbmarg
         ""  /* net sell    */
         ""  /* disc mult   */
         ""  /* old fut mrg */
         ""  /* net sell    */
         ""  /* disc mult   */
         ""  /* pd level    */
         ""  /* pd recno    */
         s-damarg.
      output stream dryrun close.
   end.
   else do:
      assign s-bmarg = s-dbmarg
             s-amarg = s-damarg.
                    
      display s-bmarg
              s-amarg
      with frame f-margtot.
      down with frame f-margtot.
   end.
end.
 


/*************************
       PROCEDURES
*************************/

procedure process-customer:
   
   for each oeeh use-index k-custno  where oeeh.cono       = g-cono 
                                       and oeeh.custno    >= b-customer
                                       and oeeh.custno    <= e-customer
                                       and oeeh.shipto    >= b-shipto
                                       and oeeh.shipto    <= e-shipto
                                       and oeeh.stagecd   >= 4
                                       and oeeh.stagecd   <= 5
                                       and oeeh.invoicedt >= beg-date
                                       and oeeh.invoicedt <= e1-date
                                       and oeeh.transtype <> "cr"
                                       and oeeh.transtype <> "rm"
                                       no-lock
                                       by oeeh.invoicedt:
                                    
      assign nextfl = no.
      input  stream exclude_oeeh from "/usr/tmp/pdxmg_exclude.dat".
      repeat:
         import stream exclude_oeeh delimiter ","
                ex-order ex-suffix.
              
         if oeeh.orderno = ex-order and oeeh.ordersuf = ex-suffix then do:
            assign nextfl = yes.
            leave.
         end.
      end.
    
      if nextfl = yes then next.

      if oeeh.shipto = "" then do:
         find arsc use-index k-arsc where arsc.cono   = g-cono
                                      and arsc.custno = oeeh.custno 
                                      and arsc.statustype = yes
                                      no-lock no-error.
         if not avail arsc then next.
         if avail arsc then do:
            find smsn use-index k-smsn where smsn.cono   = g-cono
                                         and smsn.slsrep = arsc.slsrepout
                                         no-lock no-error.
            if not avail smsn then next.
            if avail smsn then do:
               if smsn.mgr < b-region or
                  smsn.mgr > e-region then
                  next.
               if smsn.slsrep < b-salesrep or
                  smsn.slsrep > e-salesrep then
                  next.
            end.
            assign w-customer  = arsc.custno
                   w-shipto    = ""
                   w-slsrepout = arsc.slsrepout
                   w-cpricetype = arsc.pricetype
                   w-name      = substring(arsc.name,1,8)
                   w-ytdsales  = arsc.salesytd.

         end. /* arsc is avail */
      end. /* ship-to = "" */
      if oeeh.shipto <> "" then do:
         find arss use-index k-arss where arss.cono   = g-cono
                                      and arss.custno = oeeh.custno 
                                      and arss.shipto = oeeh.shipto
                                      and arss.statustype = yes
                                      no-lock no-error.
         if not avail arss then next.
         if avail arss then do:
            find smsn use-index k-smsn where smsn.cono   = g-cono
                                         and smsn.slsrep = arss.slsrepout
                                         no-lock no-error.
            if not avail smsn then next.
            if avail smsn then do:
               if smsn.mgr < b-region or
                  smsn.mgr > e-region then
                  next.
               if smsn.slsrep < b-salesrep or
                  smsn.slsrep > e-salesrep then
                  next.
            end.
            assign w-customer  = arss.custno
                   w-shipto    = arss.shipto
                   w-slsrepout = arss.slsrepout
                   w-cpricetype = arss.pricetype
                   w-name      = substring(arss.name,1,8)
                   w-ytdsales  = 0.
         end. /* arss is avail */
      end. /* ship-to <> "" */

      
      
      for each oeel use-index k-oeel where oeel.cono       = g-cono
                                       and oeel.orderno    = oeeh.orderno
                                       and oeel.ordersuf   = oeeh.ordersuf
                                       and oeel.specnstype <> "l" 
                                       and oeel.qtyship    > 0
                                       no-lock:
         if
            oeel.slsrepout = "0982" or
            oeel.slsrepout = "0983" or
            oeel.slsrepout = "0984" or
            oeel.slsrepout = "0985" or
            oeel.slsrepout = "0986" or
            oeel.slsrepout = "0987" or
            oeel.slsrepout = "0988" or
            oeel.slsrepout = "0990" or
          
            oeel.slsrepout = "0991" or
            oeel.slsrepout = "0992" or
            oeel.slsrepout = "0993" or
            oeel.slsrepout = "0994" or
            oeel.slsrepout = "0995" or
            oeel.slsrepout = "0996" or
            oeel.slsrepout = "0997" or
            oeel.slsrepout = "0998" or
            oeel.slsrepout = "0999"   
            then next.                
                                       
         find icsw use-index k-icsw where icsw.cono  = g-cono
                                      and icsw.prod  = oeel.shipprod
                                      and icsw.whse  = oeeh.whse 
                                      and icsw.listprice > 0.001
                                      and icsw.pricetype >= b-ptype 
                                      and icsw.pricetype <= e-ptype
                                      no-lock no-error.
         if avail icsw then do:
            if substring(smsn.mgr,1,1) = "S" and    /* don't include consign */
               substring(icsw.whse,1,1) = "C"       /* warehouses for SOUTH  */
               then next.
            find icsp use-index k-icsp where 
                                       icsp.cono = g-cono and
                                       icsp.prod = icsw.prod and
                                       icsp.statustype <> "L"  /* labor */
                                       no-lock no-error.
            if not avail icsp then next.
         end.
         else next.
         assign w-product = icsp.prod.
         assign w-description = "".
         assign w-level = 0
                x-level = 0.
                
         if p-contracts = yes then do:
            assign contract-found = no.
            run "contracts-only".
            if contract-found = no then
               next.
         end.
         
         run "find-pdsc-record".       
                
      end. /* oeel */
   end. /* oeeh */
end. /* procedure process-customer */

                           
procedure analyze-customer:
   
   for each oeel use-index k-prod where oeel.cono       = g-cono
                                    and oeel.shipprod   = i-prod
                                    and oeel.custno     = i-cust
                                    and oeel.shipto     = i-shipto
                                    and oeel.invoicedt >= beg-date
                                    and oeel.invoicedt <= e1-date
                                    and oeel.specnstype <> "l" 
                                    and oeel.qtyship    > 0
                                        no-lock:
      if 
         oeel.slsrepout = "0982" or
         oeel.slsrepout = "0983" or
         oeel.slsrepout = "0984" or
         oeel.slsrepout = "0985" or
         oeel.slsrepout = "0986" or
         oeel.slsrepout = "0987" or
         oeel.slsrepout = "0988" or
         oeel.slsrepout = "0990" or
         
         oeel.slsrepout = "0991" or
         oeel.slsrepout = "0992" or
         oeel.slsrepout = "0993" or
         oeel.slsrepout = "0994" or
         oeel.slsrepout = "0995" or
         oeel.slsrepout = "0996" or
         oeel.slsrepout = "0997" or
         oeel.slsrepout = "0998" or
         oeel.slsrepout = "0999"   
         then next.                
                                       
      find icsw use-index k-icsw where icsw.cono  = g-cono
                                   and icsw.prod  = oeel.shipprod
                                   and icsw.whse  = oeel.whse 
                                   and icsw.listprice > 0.001 
                                   no-lock no-error.
      if not avail icsw then next.
      find icsp use-index k-icsp where icsp.cono = g-cono and
                                       icsp.prod = icsw.prod and
                                       icsp.statustype <> "L"  /* labor */
                                       no-lock no-error.
      if not avail icsp then next.
      find oeeh use-index k-oeeh  where oeeh.cono           = g-cono 
                                        and oeeh.orderno    = oeel.orderno 
                                        and oeeh.ordersuf   = oeel.ordersuf
                                        and oeeh.stagecd   >= 4
                                        and oeeh.stagecd   <= 5
                                        and oeeh.invoicedt >= beg-date
                                        and oeeh.invoicedt <= e1-date
                                        and oeeh.transtype <> "cr"
                                        and oeeh.transtype <> "rm"
                                        no-lock no-error.
      if not avail oeeh then next.
      assign nextfl = no.
      input  stream exclude_oeeh from "/usr/tmp/pdxmg_exclude.dat".
      repeat:
         import stream exclude_oeeh delimiter ","
                ex-order ex-suffix.
              
         if oeeh.orderno = ex-order and oeeh.ordersuf = ex-suffix then do:
            assign nextfl = yes.
            leave.
         end.
      end.
    
      if nextfl = yes then next.

      if oeeh.shipto = "" then do:
         find arsc use-index k-arsc where arsc.cono   = g-cono
                                      and arsc.custno = oeeh.custno 
                                      and arsc.statustype = yes
                                      no-lock no-error.
         if not avail arsc then next.
         find smsn use-index k-smsn where smsn.cono   = g-cono
                                   and smsn.slsrep = arsc.slsrepout
                                   no-lock no-error.
         if not avail smsn then next.
         assign w-customer  = arsc.custno
                w-shipto    = ""
                w-slsrepout = arsc.slsrepout
                w-cpricetype = arsc.pricetype
                w-name      = substring(arsc.name,1,8)
                w-ytdsales  = arsc.salesytd.
      end.
      else do:
         find arss use-index k-arss where arss.cono   = g-cono
                                      and arss.custno = oeeh.custno 
                                      and arss.shipto = oeeh.shipto
                                      and arss.statustype = yes
                                      no-lock no-error.
         if not avail arss then next.
         find smsn use-index k-smsn where smsn.cono   = g-cono
                                   and smsn.slsrep = arss.slsrepout
                                   no-lock no-error.
         if not avail smsn then next.
         assign w-customer  = arss.custno
                w-shipto    = ""
                w-slsrepout = arss.slsrepout
                w-cpricetype = arss.pricetype
                w-name      = substring(arss.name,1,8)
                w-ytdsales  = 0.
      end.
         
      assign w-product = icsp.prod.
      assign w-description = "".
      assign w-level = 0
             x-level = 0.
             
      if p-contracts = yes then do:
         assign contract-found = no.
         run "contracts-only".
         if contract-found = no then
            next.
      end.
      
      if i-change = "I" then
         run "find-pdsc-2-4-rec".
      else
         run "find-pdsc-record".
      
   end. /* oeel */
end. /* procedure analyze-customer */


procedure find-pdsc-record:

   assign pd-prod   = icsw.prod     
          pd-whse   = icsw.whse     
          pd-cust   = pdsc.custno   
          pd-shipto = pdsc.custtype.
   assign pd-totnet     = 0         
          pd-totcost    = 0         
          pd-vendrebamt = 0         
          pd-margin     = 0         
          pd-margpct    = 0         
          pd-netunit    = 0         
          pd-pdrecno    = 0         
          pd-levelcd    = 0.        

   assign t-quantity = 0.   
   run "get-total-quantity".
   assign v-qtyord  = t-quantity
          v-unitbuy = icsw.unitbuy.
   
   run dasoeip.p(input v-qtyord,    
                      v-unitbuy).  
                                        

/*
/*                                 
   Go through the pecking order of PDSC Records.
   If a Type 1 or Type 2 PDSC record is found, a Matrix Record is not created 
*/
   find first     /* Type 1 PDSC Record */
           pdsc use-index k-custprod where 
                                     pdsc.cono       = g-cono
                                 and pdsc.statustype = yes
                                 and pdsc.levelcd    = 1
                                 and pdsc.custno     = oeeh.custno
                                 and pdsc.prod       = oeel.shipprod
                                 and pdsc.whse       = ""
                                 /*
                                 and (pdsc.startdt  <= if e1-date > TODAY
                                                          then TODAY
                                                       else e1-date)
                                 */
                                 and (pdsc.enddt     = ?
                                 or  pdsc.enddt     >= if e1-date > TODAY 
                                                          then TODAY
                                                       else e1-date) 
                                 no-lock no-error.
   if avail pdsc then do and (:
      assign w-description = icsp.descrip[1].
      run "get-pdsc-info".
      run "find-matrix-table".
   end.
   else do:
      find first   /* Type 2 PDSC Record */
            pdsc use-index k-custprod where 
                                       pdsc.cono       = g-cono
                                   and pdsc.statustype = yes
                                   and pdsc.levelcd    = 2
                                   and pdsc.custno     = oeeh.custno
                         and substring(pdsc.prod,1,1)  = "p"
                         and substring(pdsc.prod,3,4)  = icsw.pricetype
                                   /*
                                   and (pdsc.startdt   <= if e1-date > TODAY
                                                            then TODAY
                                                         else e1-date)
                                   */
                                   and (pdsc.enddt     = ?
                                   or  pdsc.enddt     >= if e1-date > TODAY
                                                            then TODAY
                                                         else e1-date)
                                       no-lock no-error.
                                      
      if avail pdsc then do:
         find sasta where sasta.cono = g-cono and
                          sasta.codeiden = "k" and
                          sasta.codeval  = substring(pdsc.prod,3,4)
                          no-lock no-error.
         if avail sasta then
            assign w-description = sasta.descrip.
         else
            assign w-description = "".
         run "get-pdsc-info".
         run "find-matrix-table".
      end.
      else do:
         find first   /* Type 2 PDSC Record */
             pdsc use-index k-custprod where 
                                       pdsc.cono       = g-cono
                                   and pdsc.statustype = yes
                                   and pdsc.levelcd    = 2
                                   and pdsc.custno     = oeeh.custno
                         and substring(pdsc.prod,1,1)  = "l"
                         and substring(pdsc.prod,15,6) = icsw.prodline
                                   /*
                                   and (pdsc.startdt   <= if e1-date > TODAY
                                                            then TODAY
                                                         else e1-date)
                                   */
                                   and (pdsc.enddt     = ?
                                   or  pdsc.enddt     >= if e1-date > TODAY
                                                            then TODAY
                                                         else e1-date)
                                       no-lock no-error.
         if avail pdsc then do:
            find first icsl use-index k-prodline where
                             icsl.cono     = g-cono and
                             icsl.whse     = oeeh.whse and
                             icsl.prodline = substring(pdsc.prod,15,6)
                             no-lock no-error.
            if avail icsl then 
               assign w-description = icsl.descrip.
            else
               assign w-description = "".
            run "get-pdsc-info".
            run "find-matrix-table".
         end.
         else do:
            find icsp use-index k-icsp where 
                                 icsp.cono = g-cono and  
                                 icsp.prod = oeel.shipprod
                                 no-lock no-error.
            if avail icsp then do:
               find first pdsc use-index k-custprod where 
                                pdsc.cono = g-cono
                            and pdsc.statustype = yes
                            and pdsc.levelcd    = 2
                            and pdsc.custno     = oeeh.custno
                            and substring(pdsc.prod,1,1) = "c"
                            and substring(pdsc.prod,3,4) = icsp.prodcat
                            /*
                            and (pdsc.startdt   <= if e1-date > TODAY 
                                                     then TODAY
                                                  else e1-date)
                           */
                           and (pdsc.enddt     = ?
                            or  pdsc.enddt    >= if e1-date > TODAY
                                                    then TODAY
                                                 else e1-date)
                            no-lock no-error.
               if avail pdsc then do:
                  find sasta where sasta.cono = g-cono and
                             sasta.codeiden = "c" and
                             sasta.codeval  = substring(pdsc.prod,3,4)
                             no-lock no-error.
                  if avail sasta then
                     assign w-description = sasta.descrip.
                  else
                     assign w-description = "".
                  run "get-pdsc-info".
                  run "find-matrix-table".
               end.
               else do:
                 /*
                 next.     - used for 16+ customers.
               end.
               */
                  find first pdsc use-index k-ctprod
                               where pdsc.cono       = g-cono
                                 and pdsc.statustype = yes
                                 and pdsc.levelcd    = 4
                                 and pdsc.custtype   = w-cpricetype
                       and substring(pdsc.prod,1,1)  = "P"
                       and substring(pdsc.prod,3,4)  = icsw.pricetype
                                 and pdsc.whse       = ""
                                 /*
                                 and (pdsc.whse      = oeel.whse
                                  or  pdsc.whse      = "")
                                 */
                                 /*
                                 and (pdsc.startdt   <= if e1-date > TODAY
                                                          then TODAY
                                                       else e1-date)
                                 */
                                 and (pdsc.enddt     = ?
                                  or  pdsc.enddt    >= if e1-date > TODAY
                                                          then TODAY
                                                       else e1-date)
                                      no-lock no-error.
                  if avail pdsc then do:
                     find sasta where sasta.cono = g-cono and
                                         sasta.codeiden = "k" and
                                         sasta.codeval  = pdsc.prod
                                         no-lock no-error.
                     if avail sasta then
                        assign w-description = sasta.descrip.
                     else
                        assign w-description = "". 
                     run "get-pdsc-info".
                     run "find-matrix-table".
                  end.
                  else do:   /* no matrix record found */
                     assign w-level = 0
                            x-level = 1
                            w-recno = 0
                            matrix-price = icsw.listprice
                            w-actualprc  = icsw.listprice.
                            run "no-pd-found-update-matrix".
                  end.
               end. /* else find pdsc 4 (general) */
            end. /* else find pdsc 2 (prodcat) */
         end. /* else find pdsc 2 (prodline) */
      end. /* else find pdsc 2 (pricetype) */
   end. /* else no pdsc 1 found */
*/
end. /* procedure find-pdsc-record */

procedure find-pdsc-2-4-rec:
/*                                 
   Go through the pecking order of PDSC Records.  This procedure will check
   what the margin will be if the pdsc record is inactivated.
*/
   
/*
   find first   /* Type 2 PDSC Record */
         pdsc use-index k-custprod where 
                                    pdsc.cono       = g-cono
                                and pdsc.statustype = yes
                                and pdsc.levelcd    = 2
                                and pdsc.custno     = oeeh.custno
                      and substring(pdsc.prod,1,1)  = "p"
                      and substring(pdsc.prod,3,4)  = icsw.pricetype
                                /*
                                and (pdsc.startdt   <= if e1-date > TODAY
                                                         then TODAY
                                                      else e1-date)
                                */
                                and (pdsc.enddt     = ?
                                or  pdsc.enddt     >= if e1-date > TODAY
                                                         then TODAY
                                                      else e1-date)
                                    no-lock no-error.
                                   
   if avail pdsc then do:
      find sasta where sasta.cono = g-cono and
                       sasta.codeiden = "k" and
                       sasta.codeval  = substring(pdsc.prod,3,4)
                       no-lock no-error.
      if avail sasta then
         assign w-description = sasta.descrip.
      else
         assign w-description = "".
      run "get-pdsc-info".
      run "find-matrix-table".
   end.
   else do:
      find first   /* Type 2 PDSC Record */
          pdsc use-index k-custprod where 
                                    pdsc.cono       = g-cono
                                and pdsc.statustype = yes
                                and pdsc.levelcd    = 2
                                and pdsc.custno     = oeeh.custno
                      and substring(pdsc.prod,1,1)  = "l"
                      and substring(pdsc.prod,15,6) = icsw.prodline
                                /*
                                and (pdsc.startdt   <= if e1-date > TODAY
                                                         then TODAY
                                                      else e1-date)
                                */
                                and (pdsc.enddt     = ?
                                or  pdsc.enddt     >= if e1-date > TODAY
                                                         then TODAY
                                                      else e1-date)
                                    no-lock no-error.
      if avail pdsc then do:
         find first icsl use-index k-prodline where
                          icsl.cono     = g-cono and
                          icsl.whse     = oeeh.whse and
                          icsl.prodline = substring(pdsc.prod,15,6)
                          no-lock no-error.
         if avail icsl then 
            assign w-description = icsl.descrip.
         else
            assign w-description = "".
         run "get-pdsc-info".
         run "find-matrix-table".
      end.
      else do:
         find icsp use-index k-icsp where 
                              icsp.cono = g-cono and  
                              icsp.prod = oeel.shipprod
                              no-lock no-error.
         if avail icsp then do:
            find first pdsc use-index k-custprod where 
                             pdsc.cono = g-cono
                         and pdsc.statustype = yes
                         and pdsc.levelcd    = 2
                         and pdsc.custno     = oeeh.custno
                         and substring(pdsc.prod,1,1) = "c"
                         and substring(pdsc.prod,3,4) = icsp.prodcat
                         /*
                         and (pdsc.startdt   <= if e1-date > TODAY 
                                                  then TODAY
                                               else e1-date)
                        */
                        and (pdsc.enddt     = ?
                         or  pdsc.enddt    >= if e1-date > TODAY
                                                 then TODAY
                                              else e1-date)
                         no-lock no-error.
            if avail pdsc then do:
               find sasta where sasta.cono = g-cono and
                          sasta.codeiden = "c" and
                          sasta.codeval  = substring(pdsc.prod,3,4)
                          no-lock no-error.
               if avail sasta then
                  assign w-description = sasta.descrip.
               else
                  assign w-description = "".
               run "get-pdsc-info".
               run "find-matrix-table".
            end.
            else do:
              /*
              next.     - used for 16+ customers.
            end.
            */
               find first pdsc use-index k-ctprod
                            where pdsc.cono       = g-cono
                              and pdsc.statustype = yes
                              and pdsc.levelcd    = 4
                              and pdsc.custtype   = w-cpricetype
                    and substring(pdsc.prod,1,1)  = "P"
                    and substring(pdsc.prod,3,4)  = icsw.pricetype
                              and pdsc.whse       = ""
                              /*
                              and (pdsc.whse      = oeel.whse
                               or  pdsc.whse      = "")
                              */
                              /*
                              and (pdsc.startdt   <= if e1-date > TODAY
                                                       then TODAY
                                                    else e1-date)
                              */
                              and (pdsc.enddt     = ?
                               or  pdsc.enddt    >= if e1-date > TODAY
                                                       then TODAY
                                                    else e1-date)
                                   no-lock no-error.
               if avail pdsc then do:
                  find sasta where sasta.cono = g-cono and
                                      sasta.codeiden = "k" and
                                      sasta.codeval  = pdsc.prod
                                      no-lock no-error.
                  if avail sasta then
                     assign w-description = sasta.descrip.
                  else
                     assign w-description = "". 
                  run "get-pdsc-info".
                  run "find-matrix-table".
               end.
               else do:
                  find first pdsc use-index k-ctprod
                               where pdsc.cono       = g-cono
                                 and pdsc.statustype = yes
                                 and pdsc.levelcd    = 6
                                 and pdsc.custtype   = w-cpricetype
                                 and pdsc.whse       = ""
                                 and (pdsc.enddt     = ?
                                 or  pdsc.enddt    >= if e1-date > TODAY
                                                       then TODAY
                                                    else e1-date)
                                   no-lock no-error.
                  if avail pdsc then do:
                     assign w-description = "". 
                     run "get-pdsc-info".
                     run "find-matrix-table".
                  end.
               end. /* else find pdsc 6 */
               else do:   /* no matrix record found */
                  assign w-level = 0
                         x-level = 1
                         w-recno = 0
                         matrix-price = icsw.listprice
                         w-actualprc  = icsw.listprice.
                         run "no-pd-found-update-matrix".
               end.
            end. /* else find pdsc 4 (general) */
         end. /* else find pdsc 2 (prodcat) */
      end. /* else find pdsc 2 (prodline) */
   end. /* else find pdsc 2 (pricetype) */
*/

end. /* procedure find-pdsc-2-4-rec */




procedure get-pdsc-info:

   assign t-quantity = 0.   
   run "get-total-quantity".
   assign matrix-price = 0
          w-level      = 0
          x-level      = 0
          w-enddt      = ?
          w-trndt      = ?.
   
   assign w-recno = pdsc.pdrecno
          w-level = pdsc.levelcd
          x-level = if pdsc.levelcd = 4 then 1
                    else pdsc.levelcd
          w-enddt = pdsc.enddt
          w-trndt = pdsc.transdt.
          
   if pdsc.qtybrk[1] > 0 then do:
      do y = 1 to 8:
         if t-quantity < pdsc.qtybrk[y] then do:
            if pdsc.prctype = yes then             /* $ */
                assign matrix-price = pdsc.prcmult[y].
            if pdsc.prctype = no and               /* % */
               pdsc.qtybreakty = "d" then          /* %discount from price */
                assign matrix-price = pdsc.prcdisc[y].
            if pdsc.prctype = no and               /* % */
               pdsc.qtybreakty = "p" then          /* %discount of price */
                assign matrix-price = pdsc.prcmult[y].
            if pdsc.prctype = no and
               pdsc.qtybreakty = "" then do:
               if pdsc.prcmult[y] <> 0 then
                  assign matrix-price = pdsc.prcmult[y]
                         w-qtybrk     = "p".
               if pdsc.prcdisc[y] <> 0 then
                  assign matrix-price = pdsc.prcdisc[y]
                         w-qtybrk     = "d".
            end.
            leave.
         end.
         if pdsc.qtybrk[y] = 0 and y > 1 and matrix-price = 0
            then do:
            if pdsc.prctype = yes then
               assign matrix-price = pdsc.prcmult[y].
            if pdsc.prctype = no and pdsc.qtybreakty = "d" then
               assign matrix-price = pdsc.prcdisc[y].
            if pdsc.prctype = no and pdsc.qtybreakty = "p" then
               assign matrix-price = pdsc.prcmult[y].
            if pdsc.prctype = no and pdsc.qtybreakty = "" then do:           
               if pdsc.prcmult[y] <> 0 then
                  assign matrix-price = pdsc.prcmult[y]
                         w-qtybrk     = "p".
               if pdsc.prcdisc[y] <> 0 then
                  assign matrix-price = pdsc.prcdisc[y]
                         w-qtybrk     = "d".
            end.
            leave.
         end.
      end.
      if w-qtybrk = "" and pdsc.qtybreakty = "" then assign w-qtybrk = "d".
   end.
   else do:
      assign matrix-price = 0.
      
      if pdsc.prctype = yes then
         matrix-price = pdsc.prcmult[1].
      if pdsc.prctype = no and pdsc.qtybreakty = "d" then
         matrix-price = pdsc.prcdisc[1].
      if pdsc.prctype = no and pdsc.qtybreakty = "p" then
         matrix-price = pdsc.prcmult[1].
      if pdsc.prctype = no and pdsc.qtybreakty = "" then do:
         if pdsc.prcmult[1] <> 0 then
            assign matrix-price = pdsc.prcmult[1]
                   w-qtybrk = "p".
         if pdsc.prcdisc[1] <> 0 then
            assign matrix-price = pdsc.prcdisc[1]
                   w-qtybrk = "d".
      end.
      if w-qtybrk = "" and pdsc.qtybreakty = "" then assign w-qtybrk = "d".
   end.
   
   assign w-price = 0.                                        
   if pdsc.priceonty = "l" then   /* discount on list price */
      assign w-price   = icsw.listprice
             w-actualprc = icsw.listprice.
   if pdsc.priceonty = "b" then   /* discount on base price */
      assign w-price   = icsw.baseprice
             w-actualprc = icsw.baseprice.
   if pdsc.priceonty = "c" then   /* based on cost */
      assign w-price   = icsw.stndcost
             w-actualprc = icsw.stndcost.
   
end. /* procedure */   


procedure find-matrix-table:

   find matrix use-index k-salesrep where matrix.salesrep = w-slsrepout and
                                          matrix.customer = oeeh.custno and
                                          matrix.ship-to  = oeeh.shipto and
                                          matrix.product  = w-product
                                          no-error.
                                         
   if avail matrix then do:
      run "update-matrix". 
   end.
   else do:
      create matrix.
      assign matrix.region        = substring(smsn.mgr,1,1)
             matrix.district      = substring(smsn.mgr,2,3)
             matrix.salesrep      = w-slsrepout
                                    /*
                                    if p-infile = "" then oeel.slsrepout
                                    else i-slsrep
                                    */
             matrix.customer      = oeeh.custno
             matrix.ship-to       = oeeh.shipto
             matrix.name          = w-name
             matrix.ytdsales      = w-ytdsales
             matrix.product       = w-product
             matrix.cprice-type   = w-cpricetype
             matrix.pprice-type   = icsw.pricetype
             matrix.i-listprc     = w-actualprc
             matrix.pd-recno      = w-recno
             matrix.pd-level      = w-level
             matrix.pd-enddt      = w-enddt
             matrix.pd-trndt      = w-trndt
             matrix.f-level       = if p-infile <> "" then i-fpdlvl
                                    else if x-level = 0 then 1
                                         else x-level
             matrix.change        = if p-infile <> "" then i-change
                                    else ""
             matrix.change-amt    = if p-infile <> "" then i-factor
                                    else 0
             matrix.p-cat         = if avail icsp then icsp.prodcat
                                    else "".
                                    
      assign z-year = string(YEAR(e1-date),"9999").
      assign r-year = int(substring(z-year,3,2)).
      do i = 1 to 3:                                
         for each smsew use-index k-smsew where
                                    smsew.cono        = g-cono
                                and smsew.yr          = r-year
                                and smsew.custno      = oeeh.custno
                                and smsew.shipto      = oeeh.shipto
                                and smsew.prod        = w-product
                                and smsew.componentfl = no
                                    no-lock:
            do z = 1 to 12:
               if i = 3 then
                  assign matrix.usage00 = matrix.usage00 + smsew.qtysold[z].
               if i = 2 then
                  assign matrix.usage01 = matrix.usage01 + smsew.qtysold[z].
               if i = 1 then
                  assign matrix.usage02 = matrix.usage02 + smsew.qtysold[z].
            end.
         end.
         assign r-year = r-year - 1.
      end.
      run "update-matrix".
   end.                                                                     
end. /* procedure */

  
procedure update-matrix:

   if (oeel.returnfl = yes) then             
      assign w-qtyship = (oeel.qtyship * -1).
   else                                      
      assign w-qtyship = oeel.qtyship.
      
   assign sdi-cost = oeel.prodcost.
   
   /* load buffers for NxTrend's incost.p */
   /*
   find b-oeeh use-index k-oeeh where b-oeeh.cono     = oeeh.cono and
                                      b-oeeh.orderno  = oeeh.orderno and
                                      b-oeeh.ordersuf = oeeh.ordersuf
                                      no-lock no-error.
   if avail b-oeeh then do:
      find b-oeel use-index k-oeel where b-oeel.cono     = oeel.cono and
                                         b-oeel.orderno  = oeel.orderno and
                                         b-oeel.ordersuf = oeel.ordersuf and
                                         b-oeel.lineno   = oeel.lineno
                                         no-lock no-error.
      if avail b-oeel then do:
         assign v-smvendrebfl = yes.
         assign p-cost = " ".
         run incost.p (no,
                       p-cost,
                       b-oeel.cono,
                       output v-cost).
         assign sdi-cost = v-cost / w-qtyship.
      end.
      else
         assign sdi-cost = oeel.prodcost.
   end.
   else
      assign sdi-cost = oeel.prodcost.
   */
   /* find rebate if applicable */
   find pder where pder.cono     = g-cono and
                   pder.orderno  = oeel.orderno and
                   pder.ordersuf = oeel.ordersuf and
                   pder.lineno   = oeel.lineno
                   no-lock
                   no-error.
   if avail pder then
      assign sdi-cost = sdi-cost - pder.rebateamt.
   /* find spa if applicable */
   find pder where pder.cono     = 501 and
                   pder.orderno  = oeel.orderno and
                   pder.ordersuf = oeel.ordersuf and
                   pder.lineno   = oeel.lineno
                   no-lock
                   no-error.
   if avail pder then
      assign sdi-cost = sdi-cost - pder.rebateamt.
      
   /*
   if p-infile <> "" then  /* if Dry Run */
      assign cf-cost = sdi-cost.
   else do:
   */
   
   /* find current and future cost */
   if icsw.rebatety = "C" or icsw.rebatety = "V" then do:
      assign zx-valid = no.
      run zsdirebinquire.p (input icsw.cono,
                            input icsw.arpvendno,
                            input oeeh.custno,
                            input oeeh.shipto,
                            input oeel.shipprod,
                            input icsw.rebatety,
                            input-output zx-pct,
                            input-output zx-rebate,
                            input-output zx-valid).
      if zx-valid = yes then
         assign cf-cost = icsw.stndcost - (icsw.listprice * (zx-rebate / 100)).
      else do:
         assign cf-cost = icsw.stndcost.
         find pder where pder.cono     = g-cono and
                         pder.orderno  = oeel.orderno and
                         pder.ordersuf = oeel.ordersuf and
                         pder.lineno   = oeel.lineno
                         no-lock
                         no-error.
         if avail pder then
            assign cf-cost = cf-cost - pder.rebateamt.
         /* find spa if applicable */
         find pder where pder.cono     = 501 and
                      pder.orderno  = oeel.orderno and
                      pder.ordersuf = oeel.ordersuf and
                      pder.lineno   = oeel.lineno
                      no-lock
                      no-error.
         if avail pder then
            assign cf-cost = cf-cost - pder.rebateamt.
      end.
   end.
   else do:
      assign cf-cost = icsw.stndcost.
      find pder where pder.cono     = g-cono and
                      pder.orderno  = oeel.orderno and
                      pder.ordersuf = oeel.ordersuf and
                      pder.lineno   = oeel.lineno
                      no-lock
                      no-error.
      if avail pder then
         assign cf-cost = cf-cost - pder.rebateamt.
      /* find spa if applicable */
      find pder where pder.cono     = 501 and
                   pder.orderno  = oeel.orderno and
                   pder.ordersuf = oeel.ordersuf and
                   pder.lineno   = oeel.lineno
                   no-lock
                   no-error.
      if avail pder then
         assign cf-cost = cf-cost - pder.rebateamt.
   end.
   /*
   end.  /* if Dry Run */
   */
   
   assign matrix.i-netprc = oeel.price - oeel.discamt
          matrix.i-amount = matrix.i-amount + ((oeel.price * w-qtyship) -
                                               (oeel.discamt * w-qtyship))
          matrix.i-cost   = matrix.i-cost + 
                            ((sdi-cost * w-costincr) * w-qtyship)
          matrix.i-discount = matrix.i-discount + (oeel.discamt * w-qtyship).

   if (oeel.price * w-qtyship) <> 0 then do:
      assign matrix.i-ytd-amt = matrix.i-ytd-amt + ((oeel.price * w-qtyship) -
                                                   (oeel.discamt * w-qtyship)).

      /*
      if g-operinit = "das" and oeel.shipprod = "45802-01F-30SP" and
         oeeh.custno = 106358 then do:
         display pdsc.prctype pdsc.qtybreakty w-qtybrk matrix-price w-price.
      end.
      */
  
      if pdsc.prctype = yes then do: 
         if matrix-price > 0 then do:
            assign matrix.c-netprc = matrix-price
                   matrix.c-amount = matrix.c-amount + 
                                                (matrix-price * w-qtyship)
                   matrix.c-cost   = matrix.c-cost + 
                                      ((cf-cost * w-costincr) * w-qtyship)   
                                   /* ((sdi-cost * w-costincr) * w-qtyship) */
                                      
                   matrix.c-discount = matrix.c-discount + 
                                      (oeel.discamt * w-qtyship).

            if p-calcfutr = "c" then do:
               if p-infile = "" then
                  assign matrix.f-netprc = matrix.c-netprc * w-prcincr
                         matrix.f-amount = matrix.f-amount +
                                           ((matrix.c-netprc * w-prcincr)
                                                             * w-qtyship).
               else do:
                  if i-change = "N" then
                     assign matrix.f-netprc = matrix.c-netprc
                            matrix.f-amount = matrix.c-amount.
                  else do:
                     if i-factor = 0.1 then
                        assign matrix.f-netprc = matrix.i-lastsell * w-prcincr
                               matrix.f-amount = matrix.f-amount +
                                          ((matrix.i-lastsell * w-prcincr)
                                                              * w-qtyship).
                     else
                        if i-factor = 0.9 then
                           assign matrix.f-netprice = matrix.i-lastsell
                                  matrix.f-amount   = matrix.f-amount + 
                                              ((matrix.i-lastsell * w-qtyship).
                        else
                           assign matrix.f-netprc = matrix.c-netprc * w-prcincr
                                  matrix.f-amount = matrix.f-amount + 
                                                ((matrix.c-netprc * w-prcincr)
                                                                  * w-qtyship).

                  end.
               end. /* dry-run */
            end.
            else
               assign matrix.f-netprc = (oeel.price - oeel.discamt) * w-prcincr
                      matrix.f-amount = matrix.f-amount + 
                                  (((oeel.price - oeel.discamt) * w-prcincr)
                                                                * w-qtyship).
            assign matrix.f-cost     = matrix.f-cost +
                                  /* ((sdi-cost * w-costincr) * w-qtyship) */
                                     ((cf-cost * w-costincr) * w-qtyship)   
                                     
                   matrix.f-discount = matrix.f-discount +
                                      (oeel.discamt * w-qtyship).
      
            if matrix.i-netprc >= matrix.i-listprc then 
               assign matrix.i-discmult = 1 +
                                      (1 - matrix.i-listprc / matrix.i-netprc)
                      matrix.f-discmult = if matrix.f-level = 1 then
                                             matrix.f-netprc
                                          else
                                             if p-calcfutr = "c" then
                                              matrix.f-netprc / matrix.c-netprc
                                             else
                                              matrix.f-netprc / matrix.i-netprc
                      matrix.i-pdtype   = "m"
                      matrix.f-pdtype   = if matrix.f-level = 1 then 
                                             "$"
                                          else 
                                             "m".
            else
               assign matrix.i-discmult = 100 -
                                  ((matrix.i-netprc / matrix.i-listprc) * 100)
                      matrix.f-discmult = if matrix.f-level = 1 then
                                             matrix.f-netprc
                                          else
                                             100 -
                                  ((matrix.f-netprc / matrix.i-listprc) * 100)
                      matrix.i-pdtype   = "d"
                      matrix.f-pdtype   = if matrix.f-level = 1 then
                                             "$"
                                          else
                                             "d".
         end.
         else do:
            assign matrix.c-netprc   = w-price
                   matrix.c-amount   = matrix.c-amount + (w-price * w-qtyship)
                   matrix.c-cost     = matrix.c-cost   +
                                  /* ((sdi-cost * w-costincr) * w-qtyship) */
                                     ((cf-cost * w-costincr) * w-qtyship) 
                                     
                   matrix.c-discount = matrix.c-discount + 
                                        (oeel.discamt * w-qtyship).
            if p-calcfutr = "c" then do:
               if p-infile = "" then
                  assign matrix.f-netprc = matrix.c-netprc * w-prcincr
                         matrix.f-amount = matrix.f-amount + 
                                           ((matrix.c-netprc * w-prcincr)
                                                             * w-qtyship).
               else do:
                  if i-change = "N" then
                     assign matrix.f-netprc = matrix.c-netprc
                            matrix.f-amount = matrix.c-amount.
                  else do:
                     if i-factor = 0.1 then
                        assign matrix.f-netprc = matrix.i-lastsell * w-prcincr
                               matrix.f-amount = matrix.f-amount +
                                          ((matrix.i-lastsell * w-prcincr)
                                                              * w-qtyship).
                     else
                        if i-factor = 0.9 then
                           assign matrix.f-netprice = matrix.i-lastsell
                                  matrix.f-amount   = matrix.f-amount + 
                                              ((matrix.i-lastsell * w-qtyship).
                        else
                           assign matrix.f-netprc = matrix.c-netprc * w-prcincr
                                  matrix.f-amount = matrix.f-amount + 
                                                ((matrix.c-netprc * w-prcincr)
                                                                  * w-qtyship).
                  end.
               end.
            end.
            else do:
               assign matrix.f-netprc = (oeel.price - oeel.discamt) * w-prcincr
                      matrix.f-amount = matrix.f-amount + 
                                   ((( oeel.price - oeel.discamt) * w-prcincr)
                                                                  * w-qtyship).
            end.
            assign matrix.f-cost     = matrix.f-cost     + 
                                  /* ((sdi-cost * w-costincr) * w-qtyship) */
                                     ((cf-cost * w-costincr) * w-qtyship) 
                                     
                   matrix.f-discount = matrix.f-discount + 
                                      (oeel.discamt  * w-qtyship).
            if matrix.i-netprc >= matrix.i-listprc then
               assign matrix.i-discmult = 1 +
                                      (1 - matrix.i-listprc / matrix.i-netprc)
                      matrix.f-discmult = if matrix.f-level = 1 then
                                             matrix.f-netprc
                                          else
                                             matrix.f-netprc / matrix.i-netprc
                      matrix.i-pdtype   = "m"
                      matrix.f-pdtype   = if matrix.f-level = 1 then
                                             "$"
                                          else
                                             "m".
            else
               assign matrix.i-discmult = 100 -
                                  ((matrix.i-netprc / matrix.i-listprc) * 100)
                      matrix.f-discmult = if matrix.f-level = 1 then
                                             matrix.f-netprc
                                          else
                                             100 -
                                  ((matrix.f-netprc / matrix.i-listprc) * 100)
                      matrix.i-pdtype   = "d"
                      matrix.f-pdtype   = if matrix.f-level = 1 then
                                             "$"
                                          else
                                             "d".
         end.                                                                   
      end.

      if pdsc.prctype = no and (pdsc.qtybreakty = "d" or w-qtybrk = "d")
        then do:
         if matrix-price > 0 then do:
            assign f-matrix-price = 100 - ((100 - matrix-price) * w-prcincr).
            assign matrix.c-netprc   = (w-price * (100 - matrix-price)) / 100
                   matrix.c-amount   = matrix.c-amount +
                         ((w-price * (100 - matrix-price)) * w-qtyship) / 100
                   matrix.c-cost     = matrix.c-cost     +
                                  /* ((sdi-cost * w-costincr) * w-qtyship) */
                                     ((cf-cost * w-costincr) * w-qtyship) 

                   matrix.c-discount = matrix.c-discount +
                                        (oeel.discamt  * w-qtyship).

            if p-calcfutr = "c" then do:
               if p-infile = "" then
                   assign matrix.f-netprc = matrix.c-netprc * w-prcincr
                          matrix.f-amount = matrix.f-amount +
                                            ((matrix.c-netprc * w-prcincr)
                                                              * w-qtyship).
               else do:
                  if i-change = "N" then
                     assign matrix.f-netprc = matrix.c-netprc
                            matrix.f-amount = matrix.c-amount.
                  else do:
                     if i-factor = 0.1 then
                        assign matrix.f-netprc = matrix.i-lastsell * w-prcincr
                               matrix.f-amount = matrix.f-amount +
                                          ((matrix.i-lastsell * w-prcincr)
                                                              * w-qtyship).
                     else
                        if i-factor = 0.9 then
                           assign matrix.f-netprice = matrix.i-lastsell
                                  matrix.f-amount   = matrix.f-amount + 
                                              ((matrix.i-lastsell * w-qtyship).
                        else
                           assign matrix.f-netprc = matrix.c-netprc * w-prcincr
                                  matrix.f-amount = matrix.f-amount + 
                                                ((matrix.c-netprc * w-prcincr)
                                                                  * w-qtyship).
                  end.
               end.
            end.
            else
               assign matrix.f-netprc = ((oeel.price - oeel.discamt) 
                                        * w-prcincr)
                      matrix.f-amount = matrix.f-amount +
                                   (((oeel.price - oeel.discamt) * w-prcincr)
                                                                 * w-qtyship).
            assign matrix.f-cost     = matrix.f-cost     +  
                                  /* ((sdi-cost * w-costincr) * w-qtyship) */
                                     ((cf-cost * w-costincr) * w-qtyship)   
                                     
                   matrix.f-discount = matrix.f-discount + 
                                      (oeel.discamt  * w-qtyship).
            if matrix.i-netprc >= matrix.i-listprc then
               assign matrix.i-discmult = 1 +
                                      (1 - matrix.i-listprc / matrix.i-netprc)
                      matrix.f-discmult = if matrix.f-netprc < matrix.i-listprc
                                             then
                                            if p-calcfutr = "c" then
                                              matrix.f-netprc / matrix.c-netprc
                                            else
                                              matrix.f-netprc / matrix.i-netprc
                                          else
                                     100 / (matrix.i-listprc / matrix.f-netprc)
                      matrix.i-pdtype   = "m"
                      matrix.f-pdtype   = "m".
            else do:
               assign matrix.i-discmult = 100 -
                                  ((matrix.i-netprc / matrix.i-listprc) * 100)
                      matrix.i-pdtype   = "d".
               if matrix.f-netprc >= matrix.i-listprc then
                  assign matrix.f-discmult = 100 / 
                                   (matrix.i-listprc / matrix.f-netprc)
                         matrix.f-pdtype = "m".
               else
                  assign matrix.f-discmult = 100 -
                                  ((matrix.f-netprc / matrix.i-listprc) * 100)
                         matrix.f-pdtype   = "d".
            end.
         end.
         else do:
            assign matrix.c-netprc   = w-price 
                   matrix.c-amount   = matrix.c-amount + (w-price * w-qtyship)
                   matrix.c-cost     = matrix.c-cost   +
                                  /* ((sdi-cost * w-costincr) * w-qtyship) */
                                     ((cf-cost * w-costincr) * w-qtyship)   
                                     
                   matrix.c-discount = matrix.c-discount +
                                        (oeel.discamt  * w-qtyship).
            if p-calcfutr = "c" then do:
               if p-infile = "" then
                  assign matrix.f-netprc = matrix.c-netprc * w-prcincr
                         matrix.f-amount = matrix.f-amount +
                                           ((matrix.c-netprc * w-prcincr)
                                                             * w-qtyship).
               else do:
                  if i-change = "N" then
                     assign matrix.f-netprc = matrix.c-netprc
                            matrix.f-amount = matrix.c-amount.
                  else do:
                     if i-factor = 0.1 then
                        assign matrix.f-netprc = matrix.i-lastsell * w-prcincr
                               matrix.f-amount = matrix.f-amount +
                                          ((matrix.i-lastsell * w-prcincr)
                                                              * w-qtyship).
                     else
                        if i-factor = 0.9 then
                           assign matrix.f-netprice = matrix.i-lastsell
                                  matrix.f-amount   = matrix.f-amount + 
                                              ((matrix.i-lastsell * w-qtyship).
                        else
                           assign matrix.f-netprc = matrix.c-netprc * w-prcincr
                                  matrix.f-amount = matrix.f-amount + 
                                                ((matrix.c-netprc * w-prcincr)
                                                                  * w-qtyship).
                  end.
               end.
            end.
            else
               assign matrix.f-netprc = (oeel.price - oeel.discamt) * w-prcincr
                      matrix.f-amount =  matrix.f-amount + 
                                     (((oeel.price - oeel.discamt) * w-prcincr)
                                                             * w-qtyship).
            assign matrix.f-cost     = matrix.f-cost     +  
                                  /* ((sdi-cost * w-costincr) * w-qtyship) */
                                     ((cf-cost * w-costincr) * w-qtyship)   
                                     
                   matrix.f-discount = matrix.f-discount + 
                                      (oeel.discamt  * w-qtyship).
            if matrix.i-netprc >= matrix.i-listprc then
               assign matrix.i-discmult = 1 +
                                      (1 - matrix.i-listprc / matrix.i-netprc)
                      matrix.f-discmult = if matrix.f-netprc < matrix.i-listprc
                                             then
                                     matrix.f-netprc / matrix.i-netprc
                                          else
                                     100 / (matrix.i-listprc / matrix.f-netprc)
                      matrix.i-pdtype   = "m"
                      matrix.f-pdtype   = "m".
            else do:
               assign matrix.i-discmult = 100 -
                                  ((matrix.i-netprc / matrix.i-listprc) * 100)
                      matrix.i-pdtype   = "d".
               if matrix.f-netprc >= matrix.i-listprc then
                  assign matrix.f-discmult = 100 /
                                   (matrix.i-listprc / matrix.f-netprc)
                         matrix.f-pdtype = "m".
               else
                  assign matrix.f-discmult = 100 -
                                  ((matrix.f-netprc / matrix.i-listprc) * 100)
                         matrix.f-pdtype   = "d".
            end.
         end.
      end.

      if pdsc.prctype = no and (pdsc.qtybreakty = "p" or w-qtybrk = "p") 
         then do:
         if matrix-price > 0 then do:
            assign f-matrix-price = matrix-price * w-prcincr.
            assign matrix.c-netprc    = (matrix-price / 100) * w-price
                   matrix.c-amount    = matrix.c-amount +
                                 ((matrix-price / 100) * w-price) * w-qtyship
                   matrix.c-cost     = matrix.c-cost     +
                                /* ((sdi-cost * w-costincr) * w-qtyship) */
                                   ((cf-cost * w-costincr) * w-qtyship)   
                                   
                   matrix.c-discount = matrix.c-discount +
                                      (oeel.discamt  * w-qtyship).

            if p-calcfutr = "c" then do:
               if p-infile = "" then
                  assign matrix.f-netprc = matrix.c-netprc * w-prcincr
                         matrix.f-amount = matrix.f-amount + 
                                           ((matrix.c-netprc * w-prcincr)
                                                             * w-qtyship).
               else do:
                  if i-change = "N" then
                     assign matrix.f-netprc = matrix.c-netprc
                            matrix.f-amount = matrix.c-amount.
                  else
                     if i-factor = 0.1 then
                        assign matrix.f-netprc = matrix.i-lastsell * w-prcincr
                               matrix.f-amount = matrix.f-amount +
                                          ((matrix.i-lastsell * w-prcincr)
                                                              * w-qtyship).
                     else
                        if i-factor = 0.9 then
                           assign matrix.f-netprice = matrix.i-lastsell
                                  matrix.f-amount   = matrix.f-amount + 
                                              ((matrix.i-lastsell * w-qtyship).
                        else
                           assign matrix.f-netprc = matrix.c-netprc * w-prcincr
                                  matrix.f-amount = matrix.f-amount + 
                                                ((matrix.c-netprc * w-prcincr)
                                                                  * w-qtyship).
                  end.
               end.
            end.
            else
               assign matrix.f-netprc = (oeel.price - oeel.discamt) * w-prcincr
                      matrix.f-amount = matrix.f-amount + 
                                    (((oeel.price - oeel.discamt) * w-prcincr)
                                                                  * w-qtyship).
            assign matrix.f-cost     = matrix.f-cost     +  
                                  /* ((sdi-cost * w-costincr) * w-qtyship) */
                                     ((cf-cost * w-costincr) * w-qtyship)   
                                     
                   matrix.f-discount = matrix.f-discount + 
                                      (oeel.discamt  * w-qtyship).
                                      
            if matrix.i-netprc >= matrix.i-listprc then 
               assign matrix.i-discmult = 1 +
                                      (1 - matrix.i-listprc / matrix.i-netprc)
                      matrix.f-discmult = if matrix.f-netprc < matrix.i-listprc
                                             then
                                             if p-calcfutr = "c" then
                                              matrix.f-netprc / matrix.c-netprc
                                             else
                                              matrix.f-netprc / matrix.i-netprc
                                          else
                                               100 / 
                                           (matrix.i-listprc / matrix.f-netprc)
                      matrix.i-pdtype   = "m"
                      matrix.f-pdtype   = "m".
            else 
               assign matrix.i-discmult = 
                                  ((matrix.i-netprc / matrix.i-listprc) * 100)
                      matrix.i-pdtype   = "m"
                      matrix.f-discmult = if matrix.f-netprc < matrix.i-listprc
                                             then
                                  ((matrix.f-netprc / matrix.i-listprc) * 100)
                                             else
                                  100 / (matrix.i-listprc / matrix.f-netprc)
                      matrix.f-pdtype = "m".
         end.
         else do:
            assign matrix.c-netprc   = w-price 
                   matrix.c-amount   = matrix.c-amount + (w-price * w-qtyship)
                   matrix.c-cost     = matrix.c-cost   +
                                  /* ((sdi-cost * w-costincr) * w-qtyship) */
                                     ((cf-cost * w-costincr) * w-qtyship)   
                                     
                   matrix.c-discount = matrix.c-discount +
                                        (oeel.discamt  * w-qtyship).
            if p-calcfutr = "c" then do:
               if p-infile = "" then
                  assign matrix.f-netprc = matrix.c-netprc * w-prcincr
                         matrix.f-amount = matrix.f-amount + 
                                           ((matrix.c-netprc * w-prcincr)
                                                             * w-qtyship).
               else do:
                  if i-change = "N" then
                     assign matrix.f-netprc = matrix.c-netprc
                            matrix.f-amount = matrix.c-amount.
                  else
                     if i-factor = 0.1 then
                        assign matrix.f-netprc = matrix.i-lastsell * w-prcincr
                               matrix.f-amount = matrix.f-amount +
                                          ((matrix.i-lastsell * w-prcincr)
                                                              * w-qtyship).
                     else
                        if i-factor = 0.9 then
                           assign matrix.f-netprice = matrix.i-lastsell
                                  matrix.f-amount   = matrix.f-amount + 
                                              ((matrix.i-lastsell * w-qtyship).
                        else
                           assign matrix.f-netprc = matrix.c-netprc * w-prcincr
                                  matrix.f-amount = matrix.f-amount + 
                                                ((matrix.c-netprc * w-prcincr)
                                                                  * w-qtyship).
                  end.
               end.
            end.
            else
               assign matrix.f-netprc = (oeel.price - oeel.discamt) * w-prcincr
                      matrix.f-amount = matrix.f-amount +
                                   (((oeel.price - oeel.discamt) * w-prcincr)
                                                                 * w-qtyship).
            assign matrix.f-cost     = matrix.f-cost     +  
                                  /* ((sdi-cost * w-costincr) * w-qtyship) */
                                     ((cf-cost * w-costincr) * w-qtyship)   
                                     
                   matrix.f-discount = matrix.f-discount + 
                                      (oeel.discamt  * w-qtyship).
            if matrix.i-netprc >= matrix.i-listprc then
               assign matrix.i-discmult = 1 +
                                      (1 - matrix.i-listprc / matrix.i-netprc)
                      matrix.f-discmult = if matrix.f-netprc < matrix.i-listprc
                                             then
                                             if p-calcfutr = "c" then
                                              matrix.f-netprc / matrix.c-netprc
                                             else
                                              matrix.f-netprc / matrix.i-netprc
                                           else
                                     100 / (matrix.i-listprc / matrix.f-netprc)
                      matrix.i-pdtype   = "m"
                      matrix.f-pdtype   = "m".
            else 
               assign matrix.i-discmult = 
                                  ((matrix.i-netprc / matrix.i-listprc) * 100)
                      matrix.f-discmult = if matrix.f-netprc < matrix.i-listprc
                                           then
                                   ((matrix.f-netprc / matrix.i-listprc) * 100)
                                           else
                                   100 / (matrix.i-listprc / matrix.f-netprc)
                         matrix.i-pdtype   = "m"
                         matrix.f-pdtype   = "m".

         end.
      end.
   end.
   assign w-qtybrk = "".
end. /* procedure */

procedure no-pd-found-update-matrix:

   if (oeel.returnfl = yes) then
      assign w-qtyship = (oeel.qtyship * -1).
   else                                      
      assign w-qtyship = oeel.qtyship.

   assign sdi-cost = oeel.prodcost.
   /*
   /* load buffers for NxTrend's incost.p */
   find b-oeeh use-index k-oeeh where b-oeeh.cono     = oeeh.cono and
                                      b-oeeh.orderno  = oeeh.orderno and
                                      b-oeeh.ordersuf = oeeh.ordersuf
                                      no-lock no-error.
   if avail b-oeeh then do:
      find b-oeel use-index k-oeel where b-oeel.cono     = oeel.cono and
                                         b-oeel.orderno  = oeel.orderno and
                                         b-oeel.ordersuf = oeel.ordersuf and
                                         b-oeel.lineno   = oeel.lineno
                                         no-lock no-error.
      if avail b-oeel then do:
         assign v-smvendrebfl = yes.
         assign p-cost = " ".
         run incost.p (no,
                       p-cost,
                       b-oeel.cono,
                       output v-cost).
         assign sdi-cost = v-cost / w-qtyship.
      end.
      else
         assign sdi-cost = oeel.prodcost.
   end.
   else
      assign sdi-cost = oeel.prodcost.
   */
   /* find rebate if applicable */
   find pder where pder.cono     = g-cono and
                   pder.orderno  = oeel.orderno and
                   pder.ordersuf = oeel.ordersuf and
                   pder.lineno   = oeel.lineno
                   no-lock
                   no-error.
   if avail pder then
      assign sdi-cost = sdi-cost - pder.rebateamt.
   /* find spa if applicable */
   find pder where pder.cono     = 501 and
                   pder.orderno  = oeel.orderno and
                   pder.ordersuf = oeel.ordersuf and
                   pder.lineno   = oeel.lineno
                   no-lock
                   no-error.
   if avail pder then
      assign sdi-cost = sdi-cost - pder.rebateamt.
      
   /* find current and future cost */
   if icsw.rebatety = "C" or icsw.rebatety = "V" then do:
      assign zx-valid = no.
      run zsdirebinquire.p (input icsw.cono,
                            input icsw.arpvendno,
                            input oeeh.custno,
                            input oeeh.shipto,
                            input oeel.shipprod,
                            input icsw.rebatety,
                            input-output zx-pct,
                            input-output zx-rebate,
                            input-output zx-valid).
      if zx-valid = yes then
         assign cf-cost = icsw.stndcost - (icsw.listprice * (zx-rebate / 100)).
      else do:
         assign cf-cost = icsw.stndcost.
         find pder where pder.cono     = g-cono and
                         pder.orderno  = oeel.orderno and
                         pder.ordersuf = oeel.ordersuf and
                         pder.lineno   = oeel.lineno
                         no-lock
                         no-error.
         if avail pder then
            assign cf-cost = cf-cost - pder.rebateamt.
         /* find spa if applicable */
         find pder where pder.cono     = 501 and
                      pder.orderno  = oeel.orderno and
                      pder.ordersuf = oeel.ordersuf and
                      pder.lineno   = oeel.lineno
                      no-lock
                      no-error.
         if avail pder then
            assign cf-cost = cf-cost - pder.rebateamt.
      end.
   end.
   else do:
      assign cf-cost = icsw.stndcost.
      find pder where pder.cono     = g-cono and
                      pder.orderno  = oeel.orderno and
                      pder.ordersuf = oeel.ordersuf and
                      pder.lineno   = oeel.lineno
                      no-lock
                      no-error.
      if avail pder then
         assign cf-cost = cf-cost - pder.rebateamt.
      /* find spa if applicable */
      find pder where pder.cono     = 501 and
                   pder.orderno  = oeel.orderno and
                   pder.ordersuf = oeel.ordersuf and
                   pder.lineno   = oeel.lineno
                   no-lock
                   no-error.
      if avail pder then
         assign cf-cost = cf-cost - pder.rebateamt.
   end.
   
   find matrix use-index k-salesrep where matrix.salesrep = w-slsrepout and
                                          matrix.customer = oeeh.custno and
                                          matrix.ship-to  = oeeh.shipto and
                                          matrix.product = w-product
                                          no-error.
   if avail matrix then do:
      
      assign matrix.i-ytd-amt = matrix.i-ytd-amt + ((oeel.price * w-qtyship) -
                                                   (oeel.discamt * w-qtyship))
             matrix.i-netprc  = oeel.price - oeel.discamt
             matrix.c-netprc  = matrix-price
             matrix.f-netprc  = /* if p-infile <> "" and 
                                   i-fpdlvl  = 1  and
                                   i-cpdlvl <> i-fpdlvl then 
                                   i-factor
                                else */
                                   (oeel.price - oeel.discamt) * w-prcincr
             matrix.i-amount  = matrix.i-amount + ((oeel.price   * w-qtyship) -
                                                  (oeel.discamt * w-qtyship))
             matrix.c-amount  = matrix.c-amount + ((matrix-price * w-qtyship) -
                                                   (oeel.discamt * w-qtyship))
             matrix.f-amount  = /* if p-infile <> "" and
                                   i-fpdlvl  = 1  and
                                   i-cpdlvl <> i-fpdlvl then
                                   matrix.f-amount + 
                                   ((i-factor * w-qtyship) - 
                                    (oeel.discamt * w-qtyship))
                                else */
                                   matrix.f-amount + 
                                   (((oeel.price * w-prcincr) * w-qtyship) -
                                     (oeel.discamt * w-qtyship))
             matrix.i-cost    = matrix.i-cost +
                                ((sdi-cost * w-costincr) * w-qtyship)
             matrix.c-cost    = matrix.c-cost + 
                             /* ((sdi-cost * w-costincr) * w-qtyship) */
                                ((cf-cost * w-costincr) * w-qtyship)   
                                
             matrix.f-cost    = matrix.f-cost +
                             /* ((sdi-cost * w-costincr) * w-qtyship). */
                                ((cf-cost * w-costincr) * w-qtyship). 
                                
      if matrix.i-netprc >= matrix.i-listprc then
         assign matrix.i-discmult = 1 +
                                      (1 - matrix.i-listprc / matrix.i-netprc)
                matrix.f-discmult = if matrix.f-netprc < matrix.i-listprc
                                       then
                                    matrix.f-netprc / matrix.i-netprc
                                       else
                                    100 / (matrix.i-listprc / matrix.f-netprc)
                matrix.i-pdtype   = "m"
                matrix.f-pdtype   = "m".
      else do:
         assign matrix.i-discmult = 100 -
                                  ((matrix.i-netprc / matrix.i-listprc) * 100)
                matrix.i-pdtype   = "d".
         if matrix.f-netprc >= matrix.i-listprc then
            assign matrix.f-discmult = 100 /
                                   (matrix.i-listprc / matrix.f-netprc)
                   matrix.f-pdtype   = "m".
         else
            assign matrix.f-discmult = 100 -
                                  ((matrix.f-netprc / matrix.i-listprc) * 100)
                   matrix.f-pdtype   = "d".
      end.
   end.
   else do:
      create matrix.
      assign matrix.region        = substring(smsn.mgr,1,1)
             matrix.district      = substring(smsn.mgr,2,3)
             matrix.salesrep      = w-slsrepout
                                    /*
                                    if p-infile = "" then arsc.slsrepout
                                    else i-slsrep
                                    */
             matrix.customer      = oeeh.custno
             matrix.ship-to       = oeeh.shipto
             matrix.name          = w-name
             matrix.ytdsales      = w-ytdsales
             matrix.product       = w-product
             matrix.cprice-type   = w-cpricetype
             matrix.pprice-type   = icsw.pricetype
             matrix.i-listprc     = w-actualprc
             matrix.pd-recno      = w-recno
             matrix.pd-level      = w-level
             matrix.pd-enddt      = w-enddt
             matrix.pd-trndt      = w-trndt
             matrix.f-level       = if p-infile <> "" then i-fpdlvl
                                    else if x-level = 0 then 1
                                         else x-level
             matrix.change        = if p-infile <> "" then i-change
                                    else ""
             matrix.change-amt    = if p-infile <> "" then i-factor
                                    else 0
             matrix.i-ytd-amt = ((oeel.price * w-qtyship) -
                                                   (oeel.discamt * w-qtyship))
             matrix.i-netprc  = oeel.price - oeel.discamt
             matrix.c-netprc  = matrix-price
             matrix.f-netprc  = /* if p-infile <> "" and       
                                   i-fpdlvl  = 1  and       
                                   i-cpdlvl <> i-fpdlvl then
                                   i-factor      
                                else */
                                  (oeel.price - oeel.discamt) * w-prcincr
             matrix.i-amount  = ((oeel.price * w-qtyship) -
                                                   (oeel.discamt * w-qtyship))
             matrix.c-amount  = ((matrix-price * w-qtyship) -
                                                   (oeel.discamt * w-qtyship))
             matrix.f-amount  = /* if p-infile <> "" and          
                                   i-fpdlvl  = 1  and          
                                   i-cpdlvl <> i-fpdlvl then   
                                   ((i-factor * w-qtyship) -   
                                    (oeel.discamt * w-qtyship))
                                else */
                                    (((oeel.price * w-prcincr) * w-qtyship) -
                                    (oeel.discamt * w-qtyship))
             matrix.i-cost    = ((sdi-cost * w-costincr) * w-qtyship)
             matrix.c-cost    = /* ((sdi-cost * w-costincr) * w-qtyship) */
                                 ((cf-cost * w-costincr) * w-qtyship) 
             matrix.f-cost    = /* ((sdi-cost * w-costincr) * w-qtyship) */
                                   ((cf-cost * w-costincr) * w-qtyship) 
             matrix.p-cat     = if avail icsp then icsp.prodcat
                                else "".
      if matrix.i-netprc >= matrix.i-listprc then
         assign matrix.i-discmult = 1 +
                                      (1 - matrix.i-listprc / matrix.i-netprc)
                matrix.f-discmult = if f-netprc < matrix.i-listprc 
                                       then
                                   matrix.f-netprc / matrix.i-netprc
                                       else
                                   100 / (matrix.i-listprc / matrix.f-netprc)
                matrix.i-pdtype   = "m"
                matrix.f-pdtype   = "m".
      else do:
         assign matrix.i-discmult = 100 -
                                  ((matrix.i-netprc / matrix.i-listprc) * 100)
                matrix.i-pdtype   = "d".
         if matrix.f-netprc >= matrix.i-listprc then
            assign matrix.f-discmult = 100 /
                                   (matrix.i-listprc / matrix.f-netprc)
                   matrix.f-pdtype   = "m".
         else
            assign matrix.f-discmult = 100 -
                                  ((matrix.f-netprc / matrix.i-listprc) * 100)
                   matrix.f-pdtype   = "d".
      end.

      assign z-year = string(YEAR(e1-date),"9999").
      assign r-year = int(substring(z-year,3,2)).
      do i = 1 to 3:
         for each smsew use-index k-smsew where       
                                    smsew.cono      = g-cono     
                                and smsew.yr        = r-year       
                                and smsew.custno    = oeeh.custno   
                                and smsew.prod      = w-product    
                                    no-lock:                   
            do z = 1 to 12:                                     
               if i = 1 then                                       
                  assign matrix.usage00 = matrix.usage00 + smsew.qtysold[z].
               if i = 2 then                                  
                  assign matrix.usage01 = matrix.usage01 + smsew.qtysold[z].
               if i = 3 then                             
                  assign matrix.usage02 = matrix.usage02 + smsew.qtysold[z].
            end.                                         
         end.
         assign r-year = r-year - 1.
      end.
   end.
end. /* no-pd-found-update-matrix */


procedure get-total-quantity:

if oeel.transtype = "BR" then do:
   for each br-oeel use-index k-oeel where br-oeel.cono      = g-cono
                                       and br-oeel.orderno   = oeel.orderno
                                       and br-oeel.ordersuf  = 00
                                       and br-oeel.shipprod  = oeel.shipprod
                                       and br-oeel.transtype = "BL"
                                           no-lock:
       assign t-quantity = t-quantity + br-oeel.qtyord.
   end.
end.
else do:
   if oeel.ordersuf > 00 then do:
      for each bk-oeel use-index k-oeel where bk-oeel.cono     = g-cono
                                          and bk-oeel.orderno  = oeel.orderno
                                          and bk-oeel.ordersuf = 00
                                          and bk-oeel.shipprod = oeel.shipprod
                                          and bk-oeel.specnstype <> "l"
                                              no-lock:
                                              
          assign t-quantity = t-quantity + bk-oeel.qtyord.
      end.
   end.
   else do:
      if oeeh.totlineord > 1 then do:
         for each b-oeel use-index k-oeel where b-oeel.cono     = g-cono
                                            and b-oeel.orderno  = oeel.orderno
                                            and b-oeel.ordersuf = oeel.ordersuf
                                            and b-oeel.shipprod = oeel.shipprod
                                            and b-oeel.specnstype <> "l"
                                                no-lock:
                                                
             assign t-quantity = t-quantity + b-oeel.qtyord.
         end.
      end.
   end.
end. /* else */
end. /* procedure */

procedure get-last-year:

  input from "/usr/tmp/lastyr.dat".

  if calyr = yes then
     assign w-date                = string(p-date,"99/99/9999")
            work-date             = date(w-date).              
  else                  
     assign w-date                = string(p-date,"99/99/9999")
            substring(w-date,7,4) = string((cur-yr - 1),"9999")
            work-date             = date(w-date).
         
  assign substring(w-date,1,2) = "01"
         substring(w-date,4,2) = "01"
         beg-date = date(w-date).
         
  repeat:
     import delimiter ","
        w-month b-lastyr e-lastyr.
     if work-date >= b-lastyr and work-date <= e-lastyr then
         assign e1-date   = e-lastyr.
  end.
         
end. /* procedure get-last-year */

procedure contracts-only:

   find first     /* Type 1 PDSC Record */
           pdsc use-index k-custprod where 
                                     pdsc.cono       = g-cono
                                 and pdsc.statustype = yes
                                 and pdsc.levelcd    = 1
                                 and pdsc.custno     = oeeh.custno
                                 and pdsc.prod       = oeel.shipprod
                                 and pdsc.whse       = ""
                                 and (pdsc.enddt     = ?
                                 or  pdsc.enddt     >= if e1-date > TODAY 
                                                          then TODAY
                                                       else e1-date) 
                                 no-lock no-error.
   if avail pdsc and (p-infile <> "" and i-change <> "I") then do:
      assign contract-found = yes.
      return.
   end.
   else do:
      find first   /* Type 2 PDSC Record */
            pdsc use-index k-custprod where 
                                       pdsc.cono       = g-cono
                                   and pdsc.statustype = yes
                                   and pdsc.levelcd    = 2
                                   and pdsc.custno     = oeeh.custno
                         and substring(pdsc.prod,1,1)  = "p"
                         and substring(pdsc.prod,3,4)  = icsw.pricetype
                                   and (pdsc.enddt     = ?
                                   or  pdsc.enddt     >= if e1-date > TODAY
                                                            then TODAY
                                                         else e1-date)
                                       no-lock no-error.
                                      
      if avail pdsc then do:
         assign contract-found = yes.
         return.
      end.
      else do:
         find first   /* Type 2 PDSC Record */
             pdsc use-index k-custprod where 
                                       pdsc.cono       = g-cono
                                   and pdsc.statustype = yes
                                   and pdsc.levelcd    = 2
                                   and pdsc.custno     = oeeh.custno
                         and substring(pdsc.prod,1,1)  = "l"
                         and substring(pdsc.prod,15,6) = icsw.prodline
                                   and (pdsc.enddt     = ?
                                   or  pdsc.enddt     >= if e1-date > TODAY
                                                            then TODAY
                                                         else e1-date)
                                       no-lock no-error.
         if avail pdsc then do:
            assign contract-found = yes.
            return.
         end.
         else do:
            find icsp use-index k-icsp where 
                                 icsp.cono = g-cono and  
                                 icsp.prod = oeel.shipprod
                                 no-lock no-error.
            if avail icsp then do:
               find first pdsc use-index k-custprod where 
                                pdsc.cono = g-cono
                            and pdsc.statustype = yes
                            and pdsc.levelcd    = 2
                            and pdsc.custno     = oeeh.custno
                            and substring(pdsc.prod,1,1) = "c"
                            and substring(pdsc.prod,3,4) = icsp.prodcat
                            and (pdsc.enddt     = ?
                            or  pdsc.enddt    >= if e1-date > TODAY
                                                    then TODAY
                                                 else e1-date)
                            no-lock no-error.
               if avail pdsc then do:
                  assign contract-found = yes.
                  return.
               end.
            end. /* else find pdsc 2 (prodcat) */
         end. /* else find pdsc 2 (prodline) */
      end. /* else find pdsc 2 (pricetype) */
   end. /* else no pdsc 1 found */

end. /* procedure contract-only */

{pdxmi0sls.i}


{pdxmislsr.i}

/*
if g-operinit = "das" then do: 
for each matrix use-index k-salesrep no-lock:
   display matrix.
end.
end.
*/









