/*******************************************************************************
  PROCEDURE      : x-receive.i
  DESCRIPTION    : PO Receiving interface to build poehb & poelb records,
                   for non EDI po's. If a shipping notice(856) is not sent 
                   F6 will build screen. F7 for no EDI types. POEI batch 
                   receiving can then be run by whse by poehb.openinit.
                   F10 will facilitate batch receiving like a poei(whse) run.
  AUTHOR         : Sunsource
  DATE WRITTEN   : 02/25/04
  CHANGES MADE   :
                   dkt 09/23/2008          RECEIVING
                   dkt 10/10/2007          printlabel = y if autoWhse
                   tah1206 /12/06/2006     change back nonstock bin for non
                                           mananged whses
                   dkt 06/06/2006          Changing qtyPrintLabel to
                                           poelb.user7
                   dkt 04/07/2006          Non stock descrip
                   dkt 03/28/2006          zzbin delete
                   SDI DANFOSS 11/14/05    Make vendor check to be on trading
                                           partner ID.
                   tah 08/28/07            Resale product fix

*******************************************************************************/

{g-xxex.i "new shared"}
{g-ic.i  "new"}
{g-po.i  "new"}
{g-conv.i}
{g-jump.i}
{poebtl.lva new}
{speccost.gva "new shared"}

/* INSPECT */
def var v-inspectitem as logical no-undo.
def var v-inspectbin  like wmsb.binloc no-undo. 
/* INSPECT */

/* tah1206 */
def buffer zw-icsd for icsd.
def var v-f as char init "f" no-undo. 

/* tah1206 */

/* dkt */
def var qtyLabel like poelb.user7 no-undo.
def var cQty as char no-undo.      
def var extraData as char no-undo. 
{p-poehb.i}

def var updtWmsb as logical initial no no-undo.
def var err-sw as logical init no  no-undo.
def var appliedFlag   as logical initial no no-undo.
def var BinSoftstopbin like icsw.binloc1    extent 2 no-undo.

def var var-noleave as logical initial no no-undo.


/* have to possibly hold values if xxxcustom is used twice */
def  var zh-msg           as c format "x(78)"             no-undo.
def  var zh-status        as c format "x(63)"             no-undo.
def  var zh-scrollfl      as l                            no-undo.
def  var zh-nextfl        as l                            no-undo.
def  var zh-recid         as i extent 20                  no-undo.
def  var zh-curpos        as i                            no-undo.
def  var zh-holdline      as i format ">>>9"              no-undo.
def  var zh-length        as i                            no-undo.
def  var zh-newdisplay    as l                            no-undo.
def  var zh-nextpage      as l                            no-undo.
def  var zh-prevpage      as l                            no-undo.
def new shared var s-qtyunavail   like poelb.qtyunavail no-undo.
def new shared var s-reasunavty   like icetl.reasunavty no-undo.
def var v-alpha        as char
  init "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z" no-undo.
def var o-frameval     as c                  no-undo.
def var v-noassn       like poei.nosnlots    no-undo.
def var v-eachprice    like poel.rcvcost     no-undo.
def var v-allprice     like poel.netamt      no-undo.
def var v-returnfl     as l                  no-undo.
def var v-emptyl       as l                  no-undo.
def var v-errfl        as c format "x"       no-undo.
def var zx-okfl        as logical            no-undo.
def var zx-error       as integer            no-undo.
def var zx-suff        like poeh.posuf       no-undo.
def var v-keys         as c                  no-undo.
def var v-key          as c                  no-undo.
def var v-clearfl      as log                no-undo.
def var z-lastkey      as i                  no-undo.
def var z-loadcharfl   as log                no-undo.
def var v-start        as char format "x(24)"  no-undo.
def var v-editorfl     as logical            no-undo.
def var v-searchfl     as logical            no-undo.
def var v-srchfnd      as logical            no-undo.

/* dkt */
/**
def var fndZzbinFlag  as logical extent 99 no-undo.
def var saveZzbinFlag as logical extent 99 no-undo.
def var zzIndex as integer initial 1 no-undo.
**/
def var v-printLblFlag as logical label "Print" no-undo.

/* Fix over receiving problem with ASN's */
define buffer rq-poelb for poelb.
define var rq-totrcvqty as decimal no-undo.

/* Append */

define var o-Accum      as logical                      no-undo init no.
define var v-Accum      as logical                      no-undo init no.
define new shared var v-vend-accum like apsv.vendno     no-undo init 0.
define new shared var v-vend-accum-edi like apsv.edipartner no-undo init " ".
define new shared var h-vend-accum-edi like apsv.edipartner no-undo init " ".


define var v-titlelit   as char format "x(26)"          no-undo.
define var s-ibprinter  like sapb.printernm             no-undo.
define var o-ibprinter  like sapb.printernm             no-undo.
define var s-pckprinter like sapb.printernm             no-undo.
define var o-pckprinter like sapb.printernm             no-undo.

define var e-flglbl    as logical format "y/n"          no-undo.
define var e-binloc    like icsw.binloc1                no-undo.
define var e-qtyrcv    like poel.qtyrcv format ">>>>>>9.99" no-undo.
define var e-qtyrcv2   like poel.qtyrcv format ">>>>>>9.99" no-undo.
define var e-cancelfl  as logical format "y/n"          no-undo.
define var o-cancelfl  as logical format "y/n"          no-undo.
define var v-prodsrcha as character format "x(24)"      no-undo.
define var v-prodsrchb as character format "x(24)"      no-undo.

define buffer e-poelb for poelb.
define buffer e-poehb for poehb.
define var e-poeditl  as logical                        no-undo.
/* Append */

{poebtl.lfo}

define var h as handle.
define var p-rowid as rowid.             
def var v-pono like poeh.pono             no-undo.
def var z-pono like poeh.pono             no-undo.
def var z-recid as recid                  no-undo.
def var v-shipid as char format "x(24)"   no-undo.
def var v-msg2   as char format "x(78)"   no-undo.
def var wk-binloc like icsw.binloc1       no-undo.
def var bd-lineno like poel.lineno        no-undo.
def var bd-prod   like poel.shipprod      no-undo.  
def var bd-unit   like poel.unit          no-undo.     
def var bd-qtyord like poel.qtyord        no-undo.  
def var bd-qtyrcv like poel.qtyrcv        no-undo.  
def var bd-binloc like icsw.binloc1       no-undo.  
def var o-qtyrcv7 like poel.qtyord        no-undo.  
def var sr-prod   like poel.shipprod      no-undo.  
def var v-lineno like poel.lineno         no-undo. 
def var v-errorno    as i                 no-undo.
def var v-openinit   like poeh.openinit   no-undo.
def new shared var v-saso-whse     like sasoo.whse no-undo. 
def new shared var x-saso-whsegrp  as character    no-undo.
def new shared var x-f10-whse      like sasoo.whse   no-undo. 

def new shared var toggle-sw as logical   no-undo. 
def var loaded-sw as logical              no-undo. 
def var prompt-sw  as char format "x(1)"  no-undo init "o".
def var prompt-swh as char format "x(1)"  no-undo init "p".
def var accum-sw  as logical              no-undo init "no".
def var zx-l as logical                   no-undo. 
def var zx-x as int                       no-undo.
def var srch-flg  as logical              no-undo. 
def var v-notsrch as logical              no-undo. 
def var v-okfl as logical                 no-undo. 
def var v-multifl as logical              no-undo. 
def var v-procfl as logical               no-undo.
def var v-ctr    as int                   no-undo.
def var s-function as char                no-undo.
def var s-itemnbr  like poel.shipprod     no-undo.
def var sc-recid   as recid               no-undo.
def var o-tpoehb-qtyrcv like poelb.qtyrcv no-undo.    

def buffer b-poehb for poehb. 

define new shared temp-table tpoehb no-undo 
  field pono       like poeh.pono 
  field posuf      like poeh.posuf
  field prod       like poel.shipprod
  field lineno     like poel.lineno format ">>>"
  field linecnt    as integer
  field qtyord     like poel.qtyord format ">>>>>>9.99"
  field qtyrcv     like poel.qtyrcv format ">>>>>>9.99"
  field pqtyrcv    like poel.qtyord format ">>>>>>9.99"
  field pstkqtyrcv like poel.qtyrcv format ">>>>>>9.99"
  field binloc     like icsw.binloc1
  field statustype like icsw.statustype 
  field whse       like poeh.whse
  field unit       like poel.unit
  field type       as integer format "9"    
  field lncd       as integer format "9"    
  field descrip    like icsp.descrip[1] 
  field vname      as char format "x(25)"
  field vendno     like icsw.arpvendno
  field vprod      like icsw.prod 
  field leadtm     like icsw.leadtmavg
  field nonstockty like poel.nonstockty
  field serlottype like icsw.serlottype
  field price      like poel.price
  field nosnlots   like poel.nosnlots
  field qtyunavail like poel.qtyunavail
  field reasunavty like poel.reasunavty
  field poelbid    as recid 
  field botyp      as char format "x"
  field slotty     as char format "x"
  field potyp      as char format "xx"
  field shipmentid like poehb.shipmentid
  field flglbl     as logical format "y/n"
  field cancelfl   as logical format "y/n"
  field notesfl1   as char format "x"
  field comfl      as l    format "c/ "
  field prodsrch   as character format "x(24)"
  field expshpdt   like poeh.reqshipdt
/* SDI DANFOSS 11/14/05    */
  field vendedi    like apsv.edipartner
/* SDI DANFOSS 11/14/05    */    
    index k-temp1
          pono
          posuf
          lineno
          prod
    index k-temp2
          pono 
          posuf
          vprod
          lineno
    index k-temp3
          pono 
          posuf
          qtyrcv descending
    index k-temp4
          pono 
          posuf
          prodsrch
          lineno.

def buffer b-tpoehb for tpoehb. 
def buffer x-tpoehb for tpoehb. 

define temp-table sid no-undo 
  field pono     as char format "x(10)" 
  field posuf    like poeh.posuf
  field prod     like poel.shipprod
  field lineno   like poel.lineno format ">>>"
  field whse     like poeh.whse
  field unit     like poel.unit
  field type     as integer format "9"    
  field lncd     as integer format "9"    
  field poelbid  as recid 
  field botyp    as char format "x"
  field shipid   as char format "x(20)"
  field openinit like poehb.openinit
  field marked   as char format "x" 
  field tpoehbid as recid 
  field vendno   like icsw.arpvendno
/* SDI DANFOSS 11/14/05    */
  field vendedi  like apsv.edipartner
/* SDI DANFOSS 11/14/05    */
    index k-sid1
          pono
          posuf
          lineno
          prod
    index k-sid2
          marked
          pono
          posuf
          lineno
          prod.

/* SDI DANFOSS 11/14/05    */

define buffer z1-apsv for apsv.

/* SDI DANFOSS 11/14/05    */
define buffer b-sid  for sid.
define buffer b2-sid for sid.
def var sid-recno  as recid no-undo. 
def var sid-recrow as rowid no-undo. 

define temp-table plist no-undo 
  field pono     as char format "x(10)" 
  field posuf    like poeh.posuf
  field prod     like poel.shipprod
  field lineno   like poel.lineno format ">>>"
  field whse     like poeh.whse
  field unit     like poel.unit
  field type     as char format "xx"    
  field botyp    as char format "x"
  field shipid   as char format "x(20)"
  field openinit like poehb.openinit
  field marked   as character format "x"
    index k-plist
          pono
          whse
    index k-plist2
          marked
          pono
          whse.

def var selBin like wmsbp.binloc no-undo.

{po_defvars.i "new"} 
{ {&appname} &temptable = "*" }
{ {&appname} &user_vars = "*" }

on any-key of b-blbr do:
  if {k-cancel.i} or {k-jump.i} then do: 
    hide frame f-blbr.
    apply "window-close" to current-window.
    leave.
  end.   
  if {k-accept.i} or {k-return.i} then do: 
    assign zx-brblnbr = dec(blbr.posuf)
           g-posuf    = zx-brblnbr.
    find first poeh where 
               poeh.cono  = g-cono and 
               poeh.pono  = g-pono and 
               poeh.posuf = zx-brblnbr
               no-lock no-error.
    if avail poeh and 
      (poeh.transtype = "bl" or poeh.transtype = "br") then 
    do:  
      run blanket-suffix.
      find poeh where recid(poeh) = bl-recid no-lock no-error.
      assign g-pono  = poeh.pono 
             g-posuf = poeh.posuf.
    end.
    hide frame f-blbr.
    apply "window-close" to current-window.
    leave.
  end.   
end. /* any-key */

on any-key of b-xx do:
  if {k-cancel.i} or {k-jump.i} then do: 
    run clear-brwse2.
    hide frame f-xx.
    hide frame f-zpoinfo.
    leave.
  end.   
  if {k-delete.i} then do: 
    /* Code from p-delete.i is brought in whole for this application */     
    if g-secure < 5 then 
      if g-currproc = "hher" then do: 
        run ibrferrx.p (input "Security Violation - Delete Not Allowed..",
                        input "yes"). 
        hide message no-pause.
        assign v-key = "".
        next.
      end.
      else do:
        /* Security Violation - Delete Not Allowed */
        run err.p(1109).
        {pause.gsc}
        hide message no-pause.
        assign v-key = "".
        next.
      end.
    if sid.openinit ne g-operinit then
      if g-currproc = "hher" then do: 
        run ibrferrx.p (input "Not Owner of Shipment - Can't Delete...",
                        input "yes"). 
        hide message no-pause.
        assign v-key = "".
        next.
      end.
      else do:
        message "Not Owner of Shipment - Can't Delete...". 
        bell.
        hide message no-pause.
        assign v-key = "".
        next.
      end.

    confirm = no.
    pause 0 before-hide.
    if g-currproc = "hher" then 
      do on endkey undo, leave:
        update confirm label "Delete?" auto-return
          with frame hher-del side-labels row 1 column 12 overlay.
      end.
      else
      if g-currproc = "poezb" then 
        do on endkey undo, leave:
          update confirm label "Delete?" auto-return
            with frame poezb-del side-labels row 1 column 67 overlay.
        end.
    readkey pause 0.
    if confirm then do:
      find first poehb where poehb.cono = (g-cono + 500) and 
                 poehb.pono       = int(substring(sid.pono,1,7)) and 
                 poehb.posuf      = int(substring(sid.pono,9,2)) and 
                 poehb.shipmentid = sid.shipid                   and 
                 poehb.openinit   = g-operinit
                 exclusive-lock no-prefetch.
      if avail poehb then do:
        for each poelb where poelb.cono = (g-cono + 500) and 
                 poelb.pono       = poehb.pono           and 
                 poelb.posuf      = poehb.posuf          and 
                 poelb.shipmentid = poehb.shipmentid
                 exclusive-lock no-prefetch:
          find first zzbin                                        
            where zzbin.cono = poelb.cono /* 501 */                
            and zzbin.whse = poehb.whse                            
            and zzbin.prod = poelb.shipprod                        
            and zzbin.transproc = "PO"+ STRING(poehb.pono)
            /* + "-" + STRING(poehb.posuf) */
            and zzbin.binloc ne ""  exclusive-lock no-error.                
          if avail zzbin then do:                                
            /* wmsb.statuscode */
            find wmsb where wmsb.cono = g-cono + 500 and
            wmsb.whse = zzbin.whse and wmsb.binloc = zzbin.binloc
            and wmsb.statuscode = "T" exclusive-lock no-error.
            if avail wmsb then wmsb.statuscode = "".
            delete zzbin.                                        
          end.                                                   
           
          if substring(poelb.user5,1,6) = "inb856" then 
            assign substring(poelb.user5,7,1) = "n"
                   poelb.qtyrcv = poelb.user6. 
          else          
            delete poelb.
        end.
        for each tpoehb where  
                 tpoehb.pono       = poehb.pono          and 
                 tpoehb.posuf      = poehb.posuf         and 
                 tpoehb.shipmentid = poehb.shipmentid    and 
                 poehb.openinit    = g-operinit 
                 exclusive-lock:
          /***
          for each qtyLabelPrint where qtyLabelPrint.pono = poehb.pono
          and qtyLabelPrint.posuf = poehb.posuf exclusive-lock:
            delete qtyLabelPrint.
          end.
          ***/
          delete tpoehb.
        end.
        if substring(poehb.user5,1,6) ne "inb856" then       
          delete poehb.
        else do:
          assign poehb.openinit = "".
        end.
      end.  /* if avail poehb */
      self:refreshable = false. 
      assign sid-recno  = recid(sid)
             sid-recrow = rowid(sid).
      find b-sid where recid(b-sid) = sid-recno 
        exclusive-lock no-error.
      delete b-sid.
      find first b2-sid where b2-sid.openinit = g-operinit
        no-lock no-error.
      if avail b2-sid then do:
        self:delete-selected-rows().              
        self:refreshable = true. 
        get current q-xx. 
        self:refresh().
      end. 
      else do:
        self:refreshable = true. 
        self:refresh().
      end.
    end.  /* if confirm */
    if g-currproc = "hher" then 
      hide frame hher-del.
    else 
      if g-currproc = "poezb" then 
        hide frame poezb-del.
  end.  /* if k-delete.i */

  run RFLoop.
  if {k-ctrlo.i} and zx-l = no then do: 
    assign g-pono  = int(substring(sid.pono,1,7)) 
           g-posuf = int(substring(sid.pono,9,2)).
    run poio.p.
    if toggle-sw = no then 
      put screen row 21 column 1 color messages
"  F6-Options     F7-Load Type    F8-VendProd Toggle     F9-PoList   F10-Update~  ".
    else 
      put screen row 21 column 1 color messages
"  F6-Options     F7-Load type    F8-VndProd Toggle(on)  F9-PoList   F10-Update~  ".
  end. 
  if lastkey = 32 then do:
    if not v-accum = true then do:
      find first b-sid where b-sid.marked = "X" no-lock no-error.
      if avail b-sid then do:
        display v-accum with frame f-accum.
        update v-accum with frame f-accum.
        hide frame f-accum.
        if not v-accum then do:            
          run RFLoop.
          if zx-l = yes then
            run ibrferrx.p (input "Append option not set..", input "yes"). 
          else do:
            message "Append option not set ".
            bell.
          end.  
          return.
        end.
      end.
    end.
    /* Include this if you want to keep them to a single vendor */
    find first b-sid where recid(b-sid) <> recid(sid) and
      b-sid.marked <> " " no-lock no-error.
    if avail b-sid then do:
      assign v-vend-accum = b-sid.vendno
             v-vend-accum-edi = b-sid.vendedi.
    end.
    else do:
      assign v-vend-accum = 0
             v-vend-accum-edi = "".
    end.  

/* If different Vendors are not allowed uncomment */
    
    if avail sid and sid.marked = " " and v-accum = true and 
       v-vend-accum <> 0  and
      ((sid.vendno <> v-vend-accum  and
       (v-vend-accum-edi = "" or 
        v-vend-accum-edi = string(sid.vendno))) or
     
      (sid.vendedi <> v-vend-accum-edi  and
       v-vend-accum-edi <> "" and
       v-vend-accum-edi <> string(sid.vendno))) then do:
        run RFLoop.
        if zx-l = yes then
          run ibrferrx.p (input "VndNbrs differ in Accum.",
                          input "yes"). 
        else do:
          message "VndNbrs differ in Accum.".
          bell.
        end.  
        return.
      end. 

/*                                                             */
      if avail sid and sid.marked = "X"  then
        assign sid.marked = " ".
      else  
      if avail sid and sid.marked = " " then
        assign sid.marked = "X".
      if avail sid then 
        display sid.marked with browse b-xx.
    end.              
    else
    if {k-accept.i} or {k-return.i} then do:
      def buffer b-poehb for poehb.
      def buffer bb-poehb for poehb. 
      def buffer bb-poeh  for poeh.
      hide frame l-f6.
      hide frame l-f7.
      clear frame f-poehb all no-pause.
      for each b-sid  where b-sid.marked = "X" no-lock:
        do for b-poehb:
          find first b-poehb where 
                     b-poehb.cono        = (g-cono + 500)     and 
                     b-poehb.shipmentid  =  b-sid.shipid      and 
                     b-poehb.pono   = int(substring(b-sid.pono,1,7))
                      /* and  
                     b-poehb.posuf  = int(substring(b-sid.pono,9,2)) 
                      */
                     no-lock no-error. 
          
          if avail b-poehb and b-poehb.openinit ne "" and 
             b-poehb.openinit ne g-operinit then 
          do:
            run RFLoop.
            if zx-l = yes then
              run ibrferrx.p (input "Order InUse by " + b-poehb.openinit,
                              input "yes"). 
            else do:
              message "Order InUse by " + b-poehb.openinit + " (5608)".
              bell.
            end.
            return.
          end.                    
        end. /* for b-poehb */
        assign g-pono  = int(substring(b-sid.pono,1,7))  
               g-posuf = int(substring(b-sid.pono,9,2)).  
        do for bb-poehb , bb-poeh:
          {w-poeh.i g-pono g-posuf no-lock "bb-"}
          find first bb-poehb use-index k-vendno where
                     bb-poehb.cono       = g-cono                       and
                     bb-poehb.vendno     = bb-poeh.vendno               and
                     bb-poehb.pono       = int(substring(b-sid.pono,1,7)) and  
                     /*
                     bb-poehb.posuf      = int(substring(b-sid.pono,9,2)) and
                     */
                     bb-poehb.shipmentid = b-sid.shipid
                     no-lock no-error.
          if avail bb-poehb then do:   
            run RFLoop.
            if zx-l = yes then
              run ibrferrx.p
              (input "Warning: PO Is In The PO Batch Receiving System - POEBT",
               input "yes"). 
            else do:
              {{&appname} &user_errormsg = "*" &errnbr = 8723}
              bell.
            end.
            return.                              
          end.
        end.
        assign z-pono = dec(substring(b-sid.pono,1,7)). 
        run bldtemp2(input b-sid.shipid,
                     input z-pono). 
      end. /* b-sid */
      hide frame f-zpoinfo.
      hide frame f-xx.
      assign loaded-sw = true.
      apply "window-close" to current-window. 
    end. /* if k-accept.i */
    else
    if {k-func6.i} then do:
      self:refreshable = false. 
      run clear-brwse ("all").
    
      for each b-sid:
        assign b-sid.marked = " ".
      end.
      self:refreshable = true. 
      self:refresh().
    end.

end. /* on any-key b-xx */

on any-key of b-yy do:
  if {k-cancel.i} or {k-jump.i} then do: 
    run clear-brwse2.
    hide frame f-yy.
    hide frame f-zpoinfo.
    leave.
  end.   
  if {k-accept.i} or {k-return.i} then do:
    def buffer b-poehb for poehb.
    def buffer bb-poehb for poehb. 
    def buffer bb-poeh  for poeh.
    assign z-pono = dec(substring(sid.pono,1,7)).
    if sid.shipid = "New Receipt" then do:
      run poehb_stocker.p (input z-pono, 
                           input-output v-okfl,
                           input-output z-recid).
      find poehb where recid(poehb) = z-recid no-lock no-error.
      if not avail poehb or v-okfl = false then do: 
        run clear-brwse(" ").
        hide frame f-yy.
        apply "window-close" to current-window. 
      end.
      else
        assign v-shipid = poehb.shipmentid.
    end.
    else
      assign v-shipid = sid.shipid.
    hide frame l-f6.
    hide frame l-f7.
    clear frame f-poehb all no-pause.
    do for b-poehb:
      find first b-poehb where 
                 b-poehb.cono        = (g-cono + 500)     and 
                 b-poehb.shipmentid  =  v-shipid          and 
                 b-poehb.pono   = int(substring(sid.pono,1,7)) and  
                 b-poehb.posuf  = int(substring(sid.pono,9,2)) 
                 no-lock no-error. 
      if avail b-poehb then 
        if b-poehb.openinit ne "" and 
           b-poehb.openinit ne g-operinit then 
        do:
          run RFLoop.
          if zx-l = yes then
            run ibrferrx.p (input "Order InUse by " + b-poehb.openinit,
                            input "yes"). 
          else do:  
            message "Order InUse by " + b-poehb.openinit + " (5608)".
            bell.
          end.
          return.
        end.                    
    end. /* for b-poehb */
     
    assign g-pono  = int(substring(sid.pono,1,7))  
           g-posuf = int(substring(sid.pono,9,2)).  
    do for bb-poehb , bb-poeh:
      {w-poeh.i g-pono g-posuf no-lock "bb-"}
      find first bb-poehb use-index k-vendno where
                 bb-poehb.cono       = g-cono                       and
                 bb-poehb.vendno     = bb-poeh.vendno               and
                 bb-poehb.pono       = int(substring(sid.pono,1,7)) and  
                 bb-poehb.posuf      = int(substring(sid.pono,9,2)) and
                 bb-poehb.shipmentid = v-shipid
                 no-lock no-error.
      if avail bb-poehb then do:   
        run RFLoop.
        if zx-l = yes then
          run ibrferrx.p
          (input "Warning: PO Is In The PO Batch Receiving System - POEBT",
           input "yes"). 
        else do:
          {{&appname} &user_errormsg = "*" &errnbr = 8723}
          bell.
        end.   
        return.
      end.
    end.
    run bldtemp2(input v-shipid,
                 input z-pono). 
    assign loaded-sw = true.
    hide frame f-yy.
    hide frame f-zpoinfo.
    apply "window-close" to current-window. 
  end.
end.

on any-key of b-ll do:
  if {k-cancel.i} or {k-jump.i} then do: 
    hide frame f-ll.
    leave.
  end.   
  if {k-delete.i} then do: 
    {p-delete.i}
  end.
end.

{ {&appname} &B-zz_browse_events = "*" }

do transaction :
for each poehb use-index k-openinit where
                   poehb.cono        = g-cono + 500 and
                   poehb.receivefl   = yes          and
                   poehb.openinit    = g-operinit   
                   no-lock:
  assign zx-suff = poehb.posuf
         zx-okfl = true.

  run poehb_suffix (input poehb.pono,
                    input-output zx-suff,
                    input-output zx-okfl,
                    input-output zx-error).
  run RFLoop.
/* TAH - 01-29-14 display better error message 
  if not zx-okfl then do:
    if zx-l = yes then
      run ibrferrx.p
        (input "P.O." + string(poehb.pono) + string(poehb.posuf)
                      + "Error " + string(zx-error),
         input "yes"). 
    else do:
      message "P.O." + string(poehb.pono) + string(poehb.posuf)
                     + "Error " + string(zx-error).
      bell.
    end.
  end.
   TAH - 01-29-14 display better error message */
/* TAH - 01-29-14 display better error message */
  if not zx-okfl then do:
    if zx-l = yes then
      run ibrferrx.p
        (input {poehb_suffix.gcr  
                  zx-error
                  zx-l                
                  poehb.pono
                  poehb.posuf},
         input "yes"). 
    else do:
      message  {poehb_suffix.gcr  
                      zx-error
                      zx-l                
                      poehb.pono
                      poehb.posuf}.
      bell.
    end.
  end.
/*   TAH - 01-29-14 display better error message */
  

end.                    
end. /* transaction */

{{&appname} &user_ctrlproc = "*"}

main1: 
do while true on endkey undo main1, leave main1:      
  pause 0 before-hide.        
  { {&appname} &user_display1 = "*" }
  tpoehb.qtyrcv:auto-return in frame f-poehb = true.
  tpoehb.binloc:auto-return in frame f-poehb = true.
  { {&appname} &user_display2 = "*" }
  if {k-jump.i} then do: 
    clear frame f-poehb all no-pause.  
    leave main1.  
  end.
  {w-sasoo.i g-operinit no-lock}  
  if (avail sasoo and sasoo.whse ne "") then 
/* TAH 1204 
    if v-saso-whse = "" then assign v-saso-whse = sasoo.whse. 
    else do:
      /** prompt-sw = "o". **/  /* dkt */
      prompt-sw = "p".
      /* message "zapped order?". pause 3. */
    end.
   TAH 1204 */
    assign v-saso-whse = sasoo.whse. 
  else 
  if v-saso-whse = "" then do:
    assign v-saso-whse = ""
           x-saso-whsegrp = "".
    prompt-sw = "o".
  end.
  if not can-find(first tpoehb) then do:
    assign loaded-sw = false.
    do while not loaded-sw: 
      if prompt-sw = "p" then do:
        view frame f-poehb-hdr.
        run PoSelect.
      end.
      else
      if prompt-sw = "s" then do:
        view frame f-poehb-hdr.
        run ShipToId_Select.
      end.
      else
      if prompt-sw = "o" then
        run OptionsPage.
    end.
    if {k-jump.i} or {k-cancel.i} then do: 
      clear frame f-poehb all no-pause.  
      clear frame f-poehb-hdr all no-pause.  
      leave main1.  
    end.
    next main1.
  end.
  else do:
    find first x-tpoehb no-lock no-error.
    if avail x-tpoehb then do:
      assign v-vend-accum = x-tpoehb.vendno
             v-vend-accum-edi = x-tpoehb.vendedi .
      if  v-saso-whse = "" and
        x-saso-whsegrp = "" then
        assign  v-saso-whse  = x-tpoehb.whse.
/* TAH 1204 
    if g-whse ne v-saso-whse then do:
      message "dkt g-whse v-saso" g-whse v-saso-whse.
      pause.
    end.  
   TAH 1204 */

  end.
end. 

assign g-lkupfl  = yes.
main:
do while true on endkey undo main, leave main
               on error undo main, leave main: 
  if {k-cancel.i} or {k-jump.i} then do:
    leave main.
  end.
  if not can-find(first tpoehb) then do:
    assign loaded-sw = false.
    do while not loaded-sw: 
      if prompt-sw = "p" then do:
        view frame f-poehb-hdr.
        run PoSelect.
      end.
      else
      if prompt-sw = "s" then do:
        view frame f-poehb-hdr.
        run ShipToId_Select.
      end.
      else
      if prompt-sw = "o" then
        run OptionsPage.
    end.
    if {k-cancel.i} or {k-jump.i} then do:
      leave main.
    end.
  end.
  {{&appname} &user_Query = "*"}
  
     for each poehb use-index k-openinit where                        
           poehb.cono        = g-cono + 500 and    
           poehb.receivefl   = yes          and    
           poehb.whse        = g-whse       and    
           poehb.openinit    = g-operinit   and
  /*       poehb.whse        = s-rcvwhse    and    smaf */
           poehb.updtfl      = yes          and
           substring(poehb.user5,1,6) = "inb856" 
   no-lock:           

   if substring(poehb.user5,1,6) = "inb856" then
      find first poelb where poelb.cono = poehb.cono and
                 poelb.shipmentid = poehb.shipmentid and
                 poelb.pono  = poehb.pono and
                 poelb.posuf = poehb.posuf and
                 substring(poelb.user5,7,1) <> "y" and 
                 (poelb.qtyrcv <> 0  and poelb.user6 <> 0)
      no-lock no-error.
    
     if avail poelb then do:
      /* message "yes at the receive". pause.  */
      run zsdierrx.p 
        (input "UnAck ASN" + " " + string(poelb.pono) + " " + "Ln" + " " 
        + string(poelb.lineno),
         input "yes").
         assign var-noleave = yes.

         /* message "var-noleave" var-noleave. pause.   */
     end.
     else 
     assign var-noleave = no. 
   end.   
  /* smaf */
  

  if toggle-sw = no then 
    put screen row 21 column 1 color messages
"  F6-Options     F7-Load Type    F8-VendProd Toggle     F9-PoList   F10-Update  ".
  else 
    put screen row 21 column 1 color messages
"  F6-Options     F7-Load type    F8-VndProd Toggle(on)  F9-PoList   F10-Update  ".

 if var-noleave = no then  
    leave main.
end. /* main  */

assign v-emptyl = true.
       v-clearl = false.

for each tpoehb use-index k-temp3 no-lock 
  where tpoehb.qtyrcv <> 0
        break by tpoehb.pono
              by tpoehb.posuf
              by tpoehb.shipmentid
              by tpoehb.qtyrcv descending:
  if tpoehb.qtyrcv <> 0 then do: 
    find rq-poelb where recid(rq-poelb) = tpoehb.poelbid no-lock no-error.
    if avail rq-poelb and substring(rq-poelb.user5,1,6) = "inb856" and
      substring(rq-poelb.user5,7,1) = "y" then do:
      assign v-emptyl = false.  
      leave.
    end.
    else                                                      
    if avail rq-poelb and substring(rq-poelb.user5,1,6) <> "inb856" then do:
      assign v-emptyl = false.  
      leave.
    end.
  end.
end.
  
if v-emptyl = true then do:
  display v-clearl with frame zx-ql.        
  update v-clearl with frame zx-ql.                                  
  if v-clearl then do:                                                
    run clear-brwse ("all").  
    assign v-pono = 0
           v-shipid = "".
    hide frame f-poehb no-pause.                    
    clear frame f-poehb all no-pause.                
    hide frame zx-ql.
  end.
  else do:
    hide frame zx-ql.
    readkey pause 0.
  end.
end.
else do:                                                
  run clear-brwse ("all").  
  assign v-pono = 0
         v-shipid = "".
  hide frame f-poehb no-pause.                    
  clear frame f-poehb all no-pause.                
  hide frame zx-ql.
end.

end. /* do while main1 */


{{&appname} &user_aftmain1 = "*"}
procedure OptionsPage:
find first notes use-index k-notes
  where notes.cono         = g-cono and 
        notes.notestype    = "zz" and
        notes.primarykey   = "hher-" + g-operinit and
        notes.secondarykey = " "
        no-error.
if avail notes then do:
  assign s-pckprinter = notes.noteln[1]
         s-ibprinter  = notes.noteln[2].
end.
else  
  assign s-pckprinter = "" 
         s-ibprinter  = "".
assign o-accum      = v-accum
       o-pckprinter = s-pckprinter
       o-ibprinter  = s-ibprinter.
{w-sasoo.i g-operinit no-lock}
/* TAH 1204
if g-whse ne "" then v-saso-whse = g-whse.
   TAH 1204 */
display v-accum s-pckprinter s-ibprinter v-saso-whse with frame f-opts.
OptLoop:
do while true on endkey undo, leave OptLoop: 
  /* message prompt-sw prompt-swh s-ibprinter. */
  update v-accum  s-pckprinter s-ibprinter
         v-saso-whse  when sasoo.whse = "" or
                           sasoo.homewhsefl = no 
         go-on(f7)
  with frame f-opts.
  assign s-pckprinter = input s-pckprinter 
         s-ibprinter  = input s-ibprinter.
  g-whse = v-saso-whse.
  run RFLoop.
  
  if (s-pckprinter ne "") then do:
    {w-sasp.i "s-pckprinter" no-lock}
    if not avail(sasp) then do:
      if zx-l = yes then                                         
        run ibrferrx.p                                           
          (input "Invalid Printer"
             + s-pckprinter, input "yes").                                     
      else do:                                                   
        run err.p(1134).                                         
        bell.                                                    
      end.                                                       
      next-prompt s-pckprinter with frame f-opts.                 
      next optloop.                                              
    end.                                                         
  end.    
  
  if (s-ibprinter ne "") then do:           
    /* run RFLoop. */
    {w-sasp.i "s-ibprinter" no-lock}
    /* if (avail sasp and not sasp.pcommand begins "sh") or */
    if /* (avail sasp and trim(sasp.prodlbl) = "") or  */
      (not avail sasp and s-ibprinter ne "") then
    do:
      if zx-l = yes then
        run ibrferrx.p
        (input "Invalid Printer Type for Label Printing..       "  
               + s-ibprinter,
               input "yes"). 
      else do: 
        run err.p(1134).
        bell.
      end.
      next-prompt s-ibprinter with frame f-opts.
      next optloop.
    end.
  end. /* if frame-field = "s-ibprinter" */
  if ((frame-field = "v-saso-whse") and ({k-accept.i} or {k-return.i})) or
      {k-accept.i} then 
  do:           
    assign v-saso-whse = input v-saso-whse. 
    /* run RFLoop. */
    {w-icsd.i v-saso-whse no-lock}  /* v-saso-whse */ 
           
    if avail icsd then
      assign x-saso-whsegrp = "".
           
    if v-saso-whse begins "&" then do:
      find notes where notes.cono       = g-cono and
                       notes.notestype  = "zz"   and
                       notes.primarykey = "Handheld Whse Group" and 
                       notes.secondarykey = v-saso-whse no-lock no-error.
                       /* v-saso-whse */
      if avail notes then
        assign x-saso-whsegrp = notes.noteln[1].
      else do:
        if zx-l = yes then
          run ibrferrx.p
            (input "No Whse Exist - "   
                   +  v-saso-whse + " Please Enter Valid Whse Group",
             input "yes"). 
        else do: 
          run err.p(4601).
          bell.
        end.
        next-prompt v-saso-whse with frame f-opts.
        next optloop.
      end.
    end.
    else    
    if not avail icsd then do:
      if zx-l = yes then
        run ibrferrx.p
          (input "No Whse Exist - "   
              +  v-saso-whse + " Please Enter Valid Whse",
           input "yes"). 
      else do: 
        run err.p(4601).
        bell.
      end.
      next-prompt v-saso-whse with frame f-opts.    /* v-saso-whse */
      next optloop.
    end.
  end. /* if frame-field = v-saso-whse  */
           
  if v-accum <> o-accum then do:
    if o-accum = true then 
      run clear-brwse ("all").
    if v-accum = true then do:
      find first x-tpoehb no-lock no-error.
      if avail x-tpoehb then
        assign v-vend-accum = x-tpoehb.vendno
               v-vend-accum-edi = x-tpoehb.vendedi.
    end.
  end.  /* v-accum != o-accum */
  if ((o-pckprinter ne s-pckprinter) or 
     (o-ibprinter  ne s-ibprinter))  and {k-accept.i} then 
  do: 
    if not avail notes then do:  
      create notes. 
      assign notes.cono         = g-cono  
             notes.notestype    = "zz" 
             notes.primarykey   = "hher-" + g-operinit               
             notes.secondarykey = " "
             notes.noteln[1]    = s-pckprinter
             notes.noteln[2]    = s-ibprinter.
    end. 
    else 
    if avail notes then  
      assign notes.noteln[1] = s-pckprinter 
             notes.noteln[2] = s-ibprinter.
  end. /* accepted and created note */
  if {k-cancel.i} or {k-accept.i} or {k-func7.i} then do: 
    leave OptLoop.
    release notes.
  end. 
end. /* do while true */ /* end update */
if {k-cancel.i} then 
  readkey pause 0.
hide frame f-opts.
assign prompt-sw = prompt-swh.
release notes.
hide frame f-opts.
end procedure.

procedure PoSelect:    
def buffer b-poehb for poehb.
def buffer bb-poehb for poehb. 
def buffer bb-poeh  for poeh.
assign e-poeditl = false.


find first tpoehb no-lock no-error.
if not avail tpoehb then
  assign v-vend-accum = 0
         v-vend-accum-edi = "".
 

/**
if s-ibprinter = "" then do:
  /* apply "f6" to set s-ibprinter */
  hide frame l-f7.                 
  assign  prompt-swh = prompt-sw   
  prompt-sw = "o".                 
  return.                          
end.        
**/

v-printLblFlag = no.
find icsd where icsd.cono = g-cono and icsd.whse = v-saso-whse    
no-lock no-error.                                                
if avail icsd then                                               
  if entry( 1, icsd.user4) = "Y" or entry(1, icsd.user4) = "P" then                 v-printLblFlag = /* no. */ yes.

display 
  v-pono 
  /* v-printLblFlag when s-ibprinter ne "" */
with frame l-f7 side-labels.
update v-pono go-on(f6,f7,f9,f10)
  with frame l-f7
xxeditloop: 
editing: 
  readkey.
  /* SDI001 Taged scan handling */
  if frame-field = "v-pono" and 
    not can-do("0,1,2,3,4,5,6,7,8,9,-", keylabel(lastkey)) and
    can-do (v-alpha,keylabel(lastkey)) then
      next xxeditloop. /* skip any alpha tags before pono */
  /* SDI001 Taged scan handling */
  if frame-field = "v-pono" and keylabel(lastkey) = "-" then do:
    assign e-poeditl = true.
    next xxeditloop.
  end.
  else if frame-field = "v-pono" and e-poeditl = true and
  can-do("0,1,2,3,4,5,6,7,8,9", keylabel(lastkey)) then
  do:
    next xxeditloop. 
    /* found a dash "-" and now numbers get passed by until something
       other than a number is passed */
  end.
  else
    assign e-poeditl = false.
     
  if {k-func7.i} then do:
    hide frame l-f7.
    assign prompt-sw = "s".
    return. 
  end.
  if {k-func6.i} then do:
    hide frame l-f7.
    assign  prompt-swh = prompt-sw
            prompt-sw = "o".
    return. 
  end.
  if {k-cancel.i} or {k-jump.i} then do:
    hide frame l-f7. 
    loaded-sw = true.
    apply lastkey.
    return. 
  end.
  apply lastkey.
end.
if keylabel(lastkey) ne "F9" and keylabel(lastkey) ne "F10"
  and not {k-cancel.i} then do:

find last poeh where                                               
          poeh.cono = g-cono and                                   
          poeh.pono =  v-pono                                      
          no-lock no-error.                                        
if avail poeh then                                                 
  find z1-apsv where z1-apsv.cono = g-cono and                     
  z1-apsv.vendno = poeh.vendno no-lock no-error.

/**  dkt printlable = y
if avail z1-apsv and v-accum = no then              
  v-printLblFlag = if substring(z1-apsv.user1,12,1) = "y"                 
                   then yes                                             
                   else no.                                             
/*  display v-printLblFlag with frame l-f7.  */
else v-printLblFlag = no.
if s-ibprinter = "" then v-printLblFlag = no.
else do:
  display v-printLblFlag with frame l-f7.   
  update v-printLblFlag 
        help "Must be Yes or No"
        go-on(f6,f7,f9,f10)
  with frame l-f7.
end. /* s-ibprinter ne "" */
**/

end.  
if {k-func9.i} then do:
  hide frame l-f7. 
  {{&appname} &user_f9proc = "*"}
  leave. 
end.
if {k-func10.i} then do:
  hide frame l-f7. 
  {{&appname} &user_f10proc = "*"}
  run clear-brwse(" ").
  leave. 
end.
if {k-cancel.i} or {k-jump.i} then do:
  hide frame l-f7. 
  loaded-sw = true.
  leave. 
end.
if {k-func6.i} then do:         
  hide frame l-f7.              
  assign  prompt-swh = prompt-sw
  prompt-sw = "o".
  return.                       
end.                            
if not {k-func6.i} then do:
  find last poeh where 
            poeh.cono = g-cono and 
            poeh.pono =  v-pono
            no-lock no-error.

  if avail poeh then
    find z1-apsv where z1-apsv.cono = g-cono and
                       z1-apsv.vendno = poeh.vendno no-lock no-error.

    
    
    
  if not avail poeh then do: 
    run RFLoop.
    if zx-l = yes then
      run ibrferrx.p (input "No Po Exists", input "yes"). 
    else do:
      message "No Po Exists...". 
      bell.
    end.
    run clear-brwse(" ").
    return.
  end. 
  else if poeh.transtype = "do" then do:
    run RFLoop.
    if zx-l = yes then
      run ibrferrx.p (input "Direct Po's Invalid", input "yes"). 
    else do: 
      message "Direct PO's Invalid..  ". 
      bell.
    end.  
    run clear-brwse(" ").
    return.
  end.
  else if poeh.transtype = "rm" then do:
    run RFLoop.
    if zx-l = yes then
      run ibrferrx.p (input "Return Po's Invalid", input "yes"). 
    else do: 
      message "Return PO's Invalid..  ". 
      bell.
    end.  
    run clear-brwse(" ").
    return.
  end.
  if substring(poeh.createdby,5) = "-va":u then do:                                 find first poel use-index k-poel where                                                     poel.cono         = g-cono      and                                             poel.pono         = poeh.pono   and                                             poel.posuf        = poeh.posuf  and                                             poel.vafakeprodfl = yes                                                         no-lock no-error.                                                    if avail poel then do:                                                            run RFLoop.                                                                     if zx-l = yes then                                                                run ibrferrx.p (input "Tied to Ex VA Use POEI", input "yes").                 else do:                                                                          message "PO Tied to VA Ex section use POEI to receive. ".                       bell.                                                                         end.                                                                            run clear-brwse(" ").                                                           return.                                                                       end.
  end.
  else if ((poeh.whse ne v-saso-whse) and v-saso-whse ne "" and
          x-saso-whsegrp = "") or 
        /*   (poeh.whse ne g-whse and g-whse ne "") or  */
          ( not can-do(x-saso-whsegrp,poeh.whse) and
          x-saso-whsegrp ne "") then 
  do:
    run RFLoop.
    if zx-l = yes then
      run ibrferrx.p (input "Default Whse Mismatch..",
                      input "yes"). 
    else do: 
      message "Default Whse Mismatch.. ". 
      bell.
    end.  
    run clear-brwse(" ").
    return.
  end.
  /* commenting out so the F9 list can load up regardless of vendno */
  else if v-accum = true and
       v-vend-accum <> 0 and
       ((v-vend-accum <> poeh.vendno and 
        (v-vend-accum-edi =  string(poeh.vendno) or
         v-vend-accum-edi = "")) or
        (v-vend-accum-edi <> z1-apsv.edipartner and
         v-vend-accum-edi <> ""
         and v-vend-accum-edi <>  string(poeh.vendno))) then
  do:
    run RFLoop.
    if zx-l = yes then
      run ibrferrx.p (input "VndNbrs differ in Append.", input "yes"). 
    else do:
      message "Vendor Numbers differ cannot Append.  ". 
      bell.
    end.
    run clear-brwse(" ").
    return.
  end.
  else      
    assign g-pono  = poeh.pono 
           g-posuf = poeh.posuf.
end.

{ {&appname} &user_popup_proc = "*" }

if v-saso-whse = ""  and x-saso-whsegrp = "" then do: /* v-saso-whse */
/* TAH 1204
  if g-whse ne "" then v-saso-whse = g-whse.
  else if poeh.whse = "" then do:
   TAH 1204 */
  find first poehb where poehb.cono = (g-cono + 500) and 
             poehb.openinit   = g-operinit
             no-lock no-error.
  if avail poehb then 
    assign v-saso-whse = poehb.whse 
           x-saso-whsegrp = "".
  else 
    assign v-saso-whse = poeh.whse
           x-saso-whsegrp = "".
  end.

/* Tah 1204 
end.
*/
/* TAH 1204 x-saso-whsegrp = "".       */

if zx-brblnbr > 0 then do:
  find poeh where recid(poeh) = bl-recid no-lock no-error.
end.

run preview_poship(input  g-pono,
                   input  g-posuf,
                   output z-pono).
{ {&appname} &user_display2 = "*" }

end procedure.  

procedure ShipToId_Select:
def buffer b-poehb  for poehb.
def buffer bb-poehb for poehb. 
def buffer bb-poeh  for poeh.


find first tpoehb no-lock no-error.
if not avail tpoehb then
  assign v-vend-accum = 0
         v-vend-accum-edi = "".
 

update v-shipid  go-on(f6,f7,f9,f10)
  with frame l-f6
editing: 
  readkey.
  if {k-func7.i} then do:
    hide frame l-f6.
    assign prompt-sw = "p".
    return. 
  end.
  if {k-func6.i} then do:
    hide frame l-f6.
    assign prompt-swh = prompt-sw
           prompt-sw = "o".
    return. 
  end.
  if {k-cancel.i} or {k-jump.i} then do:
    hide frame l-f6. 
    loaded-sw = true.
    apply lastkey.
    return. 
  end.
  apply lastkey.
end.
if {k-func9.i} then do:
  hide frame l-f6.
  {{&appname} &user_f9proc = "*"} 
  leave. 
end.
if {k-func10.i} then do:
  hide frame l-f6. 
  {{&appname} &user_f10proc = "*" }
  leave. 
end.
if {k-cancel.i} or {k-jump.i} then do:
  hide frame l-f6. 
  loaded-sw = true.
  leave. 
end.
assign z-pono   = 0
       v-shipid = input v-shipid.
pause 0. 
run preview_shipid(input-output v-shipid,
                   output z-pono).
if v-ctr = 1 then do: 
  find first sid no-lock no-error.
  if avail sid then do:
    assign g-pono  = int(substring(sid.pono,1,7))  
           g-posuf = int(substring(sid.pono,9,2)).  
    do for bb-poehb , bb-poeh:
      {w-poeh.i g-pono g-posuf no-lock "bb-"}
      find first bb-poehb use-index k-vendno where
                bb-poehb.cono       = g-cono                       and
                bb-poehb.vendno     = bb-poeh.vendno               and
                bb-poehb.pono       = int(substring(sid.pono,1,7)) and  
                bb-poehb.posuf      = int(substring(sid.pono,9,2)) and
                bb-poehb.shipmentid = v-shipid
                no-lock no-error.
      if avail bb-poehb then do:   
        run RFLoop.
        if zx-l = yes then
          run ibrferrx.p
            (input "Warning: PO Is In The PO Batch Receiving System - POEBT",                input "yes"). 
        else do:
          {{&appname} &user_errormsg = "*" &errnbr = 8723}
          bell.
        end.
        run clear-brwse(" ").
        next.
      end.
    end.
  end.
end.
if v-ctr = 1 then do:
  find first sid no-lock no-error.
  if avail sid then do:
    assign z-pono = dec(substring(sid.pono,1,7)).
    do for b-poehb:
      find first b-poehb where 
                 b-poehb.cono        = (g-cono + 500)          and 
                 b-poehb.shipmentid  =  v-shipid               and 
                 b-poehb.pono   = int(substring(sid.pono,1,7)) and  
                 b-poehb.posuf  = int(substring(sid.pono,9,2)) 
                 no-lock no-error. 
      if b-poehb.openinit ne "" and avail b-poehb and 
         b-poehb.openinit ne g-operinit then 
      do:
        run RFLoop.
        if zx-l = yes then
          run ibrferrx.p (input "Order InUse by " + b-poehb.openinit,
                          input "yes"). 
        else 
        do:
          message "Order InUse by " + b-poehb.openinit + " (5608)".
          bell.
        end.
        run clear-brwse(" ").
        next.
      end.                    
    end. /* for b-poehb */
    run bldtemp2(input v-shipid,
                 input z-pono). 
    assign loaded-sw = true.
  end.
end.
else 
if v-ctr = 0 then do:                       
  run RFLoop.
  if zx-l = yes then
    run ibrferrx.p (input "ShipId Recs Notfound..",
                    input "yes"). 
  else do:
    message " ShipId Recs Notfound... ". 
    bell.
  end.
  assign prompt-sw = "p". /* switch to PO search after error */
  hide frame l-f6.
  run clear-brwse(" ").
end.
hide frame l-f7.
{ {&appname} &user_display2 = "*" }
end procedure. 

procedure preview_poship:
def input parameter shippo  like poeh.pono. 
def input parameter shipposuf  like poeh.posuf. 
def output parameter v-pono like poeh.pono. 
assign v-ctr = 0.
run clear-brwse(" "). 
run clear-brwse2. 
hide frame f-poehb.

if not can-find (first poehb where 
                 poehb.cono  = (g-cono + 500) and 
                 poehb.pono  = shippo and
                 poehb.posuf =
                 (if can-do("BR,BL",poeh.transtype) then
                    shipposuf
                  else
                    poehb.posuf) and
                 poehb.shipmentid begins "stk" no-lock) then 


 if ((poeh.whse = v-saso-whse) and v-saso-whse ne "" and
              x-saso-whsegrp = "") or
    ( can-do(x-saso-whsegrp,poeh.whse) and
      x-saso-whsegrp ne "") then 
  do:
    v-ctr = v-ctr + 1. 
    
/* SDI DANFOSS 11/14/05    */
    find z1-apsv where z1-apsv.cono = g-cono and
                       z1-apsv.vendno = poeh.vendno no-lock no-error.             /* SDI DANFOSS 11/14/05    */
        
    
    
    
    create sid. 
    assign sid.shipid   =  "New Receipt"
           sid.pono     =  string(dec(poeh.pono),"9999999") + "-" + 
                           string(dec(poeh.posuf),"99")  
           sid.openinit =  g-operinit
           sid.whse     =  poeh.whse
           sid.vendno   =  poeh.vendno
/* SDI DANFOSS 11/14/05    */

           sid.vendedi  =  if not avail z1-apsv or z1-apsv.edipartner = "" then
                             string(poeh.vendno)
                           else
                             z1-apsv.edipartner.
/* SDI DANFOSS 11/14/05    */

    
    assign sid.marked   =  if can-find(first tpoehb where 
                                             tpoehb.pono = poeh.pono and
                                        tpoehb.posuf =
                                        (if can-do("BR,BL",poeh.transtype) then
                                           shipposuf
                                         else
                                           tpoehb.posuf) and
                                         tpoehb.shipmentid = sid.shipid)   
                           then
                             "L"
                           else
                             " ".
  end.

do transaction:
  for each poehb where 
           poehb.cono  = (g-cono + 500) and 
           poehb.pono  = shippo         and
           poehb.posuf = 
          (if can-do("BR,BL",poeh.transtype) then
             shipposuf
           else
             poehb.posuf) 
           no-lock: 

    if ((poehb.whse ne v-saso-whse) and v-saso-whse ne "" and
         x-saso-whsegrp = "") or
       ( not can-do(x-saso-whsegrp,poehb.whse) and
          x-saso-whsegrp ne "") then 
      next.
    
      assign zx-suff = poehb.posuf
             zx-okfl = true.
      run poehb_suffix (input poehb.pono,
                        input-output zx-suff,
                        input-output zx-okfl,
                        input-output zx-error).
      if not zx-okfl then do:
        run RFLoop.
        if zx-l = yes then
          run ibrferrx.p 
            (input "P.O." + string(poehb.pono) + string(poehb.posuf)
                          + "Error " + string(zx-error),
             input "yes"). 
        else do: 
          message "P.O." + string(poehb.pono) + string(poehb.posuf)
                         + "Error " + string(zx-error).
          bell.
        end.
        next.
      end.
      v-ctr = v-ctr + 1. 

/* SDI DANFOSS 11/14/05    */
    
      find z1-apsv where z1-apsv.cono = g-cono and
                         z1-apsv.vendno = poehb.vendno no-lock no-error.
/* SDI DANFOSS 11/14/05    */
    
    create sid. 
    assign sid.shipid   =  poehb.shipmentid
           sid.pono     =  string(dec(poehb.pono),"9999999") + "-" + 
                           string(dec(zx-suff),"99")  
           sid.openinit =  poehb.openinit
           sid.whse     =  poehb.whse
           sid.vendno   =  poehb.vendno
/* SDI DANFOSS 11/14/05    */

           sid.vendedi  =  if not avail z1-apsv or z1-apsv.edipartner = "" then
                             string(poeh.vendno)
                           else
                             z1-apsv.edipartner.
/* SDI DANFOSS 11/14/05    */

    assign sid.marked   =  if can-find(first tpoehb where 
                                             tpoehb.pono = poehb.pono and
                                        tpoehb.posuf =
                                        (if can-do("BR,BL",poeh.transtype) then                                            shipposuf
                                         else
                                           poehb.posuf) and                                                                  tpoehb.shipmentid = sid.shipid)  
                           then
                             "L"
                           else
                             " ".
   end. /* for each poehb */
end. /* transaction */

if v-ctr = 0 then do:
   hide frame f-lookprod.
   leave.
end.
else if v-ctr = 1 and avail sid and sid.openinit = g-operinit then do:
  run poehb_stocker.p (input shippo, 
                       input-output v-okfl,
                       input-output z-recid).
  find b-poehb where recid(b-poehb) = z-recid
       no-lock no-error.
  if not avail b-poehb or v-okfl = false then do: 
    run clear-brwse(" ").
    leave.
  end.
  else do:
    run poship_load.
    leave.
  end.
end.
else if v-ctr = 1 and avail sid and sid.openinit ne g-operinit then do: 
  run RFLoop.
  if zx-l = yes then
    run ibrferrx.p (input "Order InUse by " + sid.openinit,
                    input "yes"). 
  else do:                   
    message "Order InUse by " + sid.openinit + " (5608)".
    bell.
  end.    
  run clear-brwse (" ").
  leave.
end.  

open query q-yy for each sid.
enable  b-yy with frame f-yy.
display b-yy with frame f-yy.
run bottom_vend(input dec(substring(sid.pono,1,7)), 
                input dec(substring(sid.pono,9,2)),
                input sid.shipid).     
on cursor-up cursor-up.
on cursor-down cursor-down.
wait-for window-close of current-window. 
on cursor-up back-tab.
on cursor-down tab.
close query q-yy.
end procedure. 

procedure poship_load:
def buffer bb-poehb for poehb. 
def buffer bb-poeh  for poeh.
def buffer b-poehb for poehb.
def var v-shipid like poehb.shipmentid.
if v-ctr = 1 then do: 
  find first sid no-lock no-error.
  if avail sid then do:
    {w-poeh.i g-pono g-posuf no-lock "bb-"}
    assign g-pono  = int(substring(sid.pono,1,7))  
           g-posuf = int(substring(sid.pono,9,2))
           v-shipid = if sid.shipid = "New Receipt" and 
                        can-do("BL,BR",bb-poeh.transtype) then
                          "stk" + string(g-pono,"9999999") 
                                + string(g-posuf,"99")
                      else
                      if sid.shipid = "New Receipt" then 
                        "stk" + string(g-pono,"9999999")
                      else
                        sid.shipid. 
    {w-poeh.i g-pono g-posuf no-lock "bb-"}
    find first bb-poehb use-index k-vendno where
               bb-poehb.cono       = g-cono                       and
               bb-poehb.vendno     = bb-poeh.vendno               and
               bb-poehb.pono       = int(substring(sid.pono,1,7)) and  
               bb-poehb.posuf      = int(substring(sid.pono,9,2)) and
               bb-poehb.shipmentid = v-shipid
               no-lock no-error.
    if avail bb-poehb then do:   
      run RFLoop.
      if zx-l = yes then
        run ibrferrx.p
          (input "Warning: PO Is In The PO Batch Receiving System - POEBT",  
           input "yes"). 
      else do:
        {{&appname} &user_errormsg = "*" &errnbr = 8723}
        bell.
      end.  
      run clear-brwse(" ").
      next.
    end.
  end.
end.
if v-ctr = 1 then do:
  find first sid no-lock no-error.
  if avail sid then do:
    assign z-pono = dec(substring(sid.pono,1,7)).
    find first b-poehb where 
               b-poehb.cono        = (g-cono + 500)          and 
               b-poehb.shipmentid  =  v-shipid               and 
               b-poehb.pono   = int(substring(sid.pono,1,7)) and  
               b-poehb.posuf  = int(substring(sid.pono,9,2)) 
               no-lock no-error. 
    if b-poehb.openinit ne "" and avail b-poehb and 
       b-poehb.openinit ne g-operinit then 
    do:
      run RFLoop.
      if zx-l = yes then
        run ibrferrx.p (input "Order InUse by " + b-poehb.openinit,
                        input "yes"). 
      else do:
        message "Order InUse by " + b-poehb.openinit + " (5608)".
        bell.
      end.    
      run clear-brwse (" ").
      next.
    end.                    
    run bldtemp2(input v-shipid,
                 input z-pono). 
    assign loaded-sw = true.
  end.
end.
else if v-ctr = 0 then do:
  run RFLoop.
  if zx-l = yes then
    run ibrferrx.p (input "ShipId Recs Notfound..",
                    input "yes"). 
  else do:
    message " ShipId Recs Notfound... ". 
    bell.
  end.  
  run clear-brwse(" ").
end.
hide frame l-f7.
{ {&appname} &user_display2 = "*" }
end procedure.

procedure preview_shipid:
def input-output parameter shipid as char format "x(24)". 
def output parameter v-pono like poeh.pono. 
assign v-titlelit = " ShipId PO List     F6-Clr".
{{&appname} &bXX_title = "*" }

assign v-ctr = 0.
run clear-brwse (" ").
run clear-brwse2.
hide frame f-poehb.

if not(can-find (first poehb where 
                 poehb.cono       = (g-cono + 500) and 
                 poehb.shipmentid = shipid no-lock)) then        
do: 
  for each zsastz where zsastz.cono = g-cono and
                        zsastz.labelfl = false and
                        zsastz.codeiden = "VendorChars" no-lock:
    if length(zsastz.codeval[5]) <> 0 then do:
      if can-find (first poehb where 
                   poehb.cono       = (g-cono + 500) and 
                   poehb.shipmentid = 
                    substring(shipid,length(zsastz.codeval[5]) + 1,
                     (length(shipid) - length(zsastz.codeval[5]))) and       
                    substring(shipid,1,length(zsastz.codeval[5])) =
                     zsastz.codeval[5] and
                   string(poehb.vendno) = zsastz.primarykey no-lock) then      
         assign shipid = substring(shipid,length(zsastz.codeval[5]) + 1,
                     (length(shipid) - length(zsastz.codeval[5]))).                end.
  end.
end.
for each poehb where 
         poehb.cono       = (g-cono + 500) and 
         poehb.shipmentid = shipid         
         no-lock: 

  if ((poehb.whse ne v-saso-whse) and v-saso-whse ne "" and
       x-saso-whsegrp = "") or
     ( not can-do(x-saso-whsegrp,poehb.whse) and
       x-saso-whsegrp ne "") then 
    next.
    
  /**
  if (g-whse ne "" and poehb.whse ne g-whse) or
     ( not can-do(x-saso-whsegrp, poehb.whse) and x-saso-whsegrp ne "") then
     next.    **/
     
  assign zx-suff = poehb.posuf
         zx-okfl = true.
  run poehb_suffix (input poehb.pono,
                    input-output zx-suff,
                    input-output zx-okfl,
                    input-output zx-error).
  if not zx-okfl then do:
    run RFLoop.
    if zx-l = yes then
      run ibrferrx.p 
        (input "P.O." + string(poehb.pono) + string(poehb.posuf)
                      + "Error " + string(zx-error),
         input "yes"). 
    else do:
      message "P.O." + string(poehb.pono) + string(poehb.posuf)
                     + "Error " + string(zx-error).
      bell.
    end.
  end.
end.            
         
/************************************************************
 if no tpoehb records exist - we have to conclude the first poehb record 
 with a matching shipmentid will determine the accum vendor  
*************************************************************/   
find first x-tpoehb where
           x-tpoehb.shipmentid = shipid   and 
           ((x-tpoehb.whse       = v-saso-whse and
             v-saso-whse ne "" and
             x-saso-whsegrp = "") or
           (can-do(x-saso-whsegrp,x-tpoehb.whse) and
             x-saso-whsegrp <> ""))
     no-lock no-error.
if avail x-tpoehb then do:
   assign v-vend-accum = x-tpoehb.vendno
          v-vend-accum-edi = x-tpoehb.vendedi.
end.
else do:
  find first poehb where 
             poehb.cono       = (g-cono + 500) and 
             poehb.shipmentid = shipid         and
           ((poehb.whse       = v-saso-whse and
             v-saso-whse ne "" and
             x-saso-whsegrp = "") or
            (can-do(x-saso-whsegrp,poehb.whse) and
             x-saso-whsegrp <> ""))
             no-lock no-error.
  if avail poehb then do:
    
/* SDI DANFOSS 08/24/10    */   
  find z1-apsv where z1-apsv.cono = g-cono and
                     z1-apsv.vendno = poehb.vendno no-lock no-error.
/* SDI DANFOSS 08/24/10    */

    
    assign v-vend-accum = poehb.vendno
           v-vend-accum-edi = if not avail z1-apsv or z1-apsv.edipartner = ""
                              then
                                 string(poehb.vendno)
                              else
                                 z1-apsv.edipartner.
  end.                               
end.               

for each poehb where 
         poehb.cono       = (g-cono + 500) and 
         poehb.shipmentid = shipid         and 
         ((poehb.whse       = v-saso-whse and
           v-saso-whse ne "" and
           x-saso-whsegrp = "") or
          (can-do(x-saso-whsegrp,poehb.whse) and
           x-saso-whsegrp <> ""))
         no-lock: 
        
/* SDI DANFOSS 08/24/10    */   
     find z1-apsv where z1-apsv.cono = g-cono and
                        z1-apsv.vendno = poehb.vendno no-lock no-error.

          h-vend-accum-edi = if not avail z1-apsv or z1-apsv.edipartner = ""
                              then
                                 string(poehb.vendno)
                              else
                                 z1-apsv.edipartner.
 
/* SDI DANFOSS 08/24/10    */


    if  v-vend-accum <> 0  and
      ((poehb.vendno <> v-vend-accum  and
       (v-vend-accum-edi = "" or 
        v-vend-accum-edi = string(poehb.vendno))) or
     
      (h-vend-accum-edi <> v-vend-accum-edi  and
       v-vend-accum-edi <> "" and
       v-vend-accum-edi <> string(poehb.vendno))) then do:
/* SDI DANFOSS 08/24/10    */
 
/* SDI DANFOSS 08/24/10    
  if v-vend-accum <> 0 and v-vend-accum <> poehb.vendno then do:
   SDI DANFOSS 08/24/10    */

    run RFLoop.
    if zx-l = yes then do:
      run ibrferrx.p (input "VndNbrs differ in Append. "   +
        string(poehb.pono) + "-" +  string(poehb.posuf) +  "  "  +
        string(poehb.vendno) + "    " + poehb.shipmentid,
        input "yes"). 
      run clear-brwse(" ").
      pause 0.
      next.
    end.
    else do: 
      message "VndNbrs differ cannot Append.  " + 
        string(poehb.pono) + "-" +  string(poehb.posuf)
        string(poehb.vendno) + " ShipId = " + poehb.shipmentid. 
      bell.
      run clear-brwse(" ").
      next.
    end.
  end.

/* SDI DANFOSS 11/14/05    */   
  find z1-apsv where z1-apsv.cono = g-cono and
                     z1-apsv.vendno = poehb.vendno no-lock no-error.
/* SDI DANFOSS 11/14/05    */

  v-ctr = v-ctr + 1. 
  create sid. 
  assign sid.pono     =  string(dec(poehb.pono),"9999999") + "-" + 
                         string(dec(zx-suff),"99")  
         sid.openinit =  poehb.openinit
         sid.shipid   =  poehb.shipmentid
         sid.whse     =  poehb.whse
         sid.vendno   =  poehb.vendno
/* SDI DANFOSS 11/14/05    */
         sid.vendedi  =  if not avail z1-apsv or z1-apsv.edipartner = "" then
                           string(poeh.vendno)
                         else
                           z1-apsv.edipartner.
/* SDI DANFOSS 11/14/05    */
           
  assign sid.marked   =  if can-find(first tpoehb where 
                                           tpoehb.pono = poehb.pono and
                                           tpoehb.shipmentid = sid.shipid) 
                         then
                           "L"
                          else
                            " ".
end. /* for each poehb */

if v-ctr = 1 or v-ctr = 0 then do:
  hide frame f-lookprod.
  leave.
end.
open query q-xx for each sid.
enable  b-xx with frame f-xx.
display b-xx with frame f-xx.
run bottom_vend(input dec(substring(sid.pono,1,7)), 
                input dec(substring(sid.pono,9,2)),
                input sid.shipid).     
on cursor-up cursor-up.
on cursor-down cursor-down.
wait-for window-close of current-window. 
on cursor-up back-tab.
on cursor-down tab.
close query q-xx.
end procedure.

procedure List_POs:
assign v-ctr = 0.
assign v-titlelit = " Open OperID PO's  F6-Clr".
{{&appname} &bXX_title = "*" }
run clear-brwse (" ").
run clear-brwse2.
hide frame f-poehb.
for each poehb where 
         poehb.cono       = (g-cono + 500) and 
         poehb.openinit   = g-operinit     and 
         ((poehb.whse       = v-saso-whse and
           v-saso-whse ne "" and
           x-saso-whsegrp = "") or
          (can-do(x-saso-whsegrp,poehb.whse) and
           x-saso-whsegrp <> ""))
         no-lock: 

/* SDI DANFOSS 11/14/05    */
  find z1-apsv where z1-apsv.cono = g-cono and
                     z1-apsv.vendno = poehb.vendno no-lock no-error.
/* SDI DANFOSS 11/14/05    */
  v-ctr = v-ctr + 1. 
  create sid. 
  assign sid.pono     =  string(dec(poehb.pono),"9999999") + "-" + 
                         string(dec(poehb.posuf),"99")  
         sid.openinit =  poehb.openinit
         sid.shipid   =  poehb.shipmentid
         sid.whse     =  poehb.whse
         sid.vendno   =  poehb.vendno
/* SDI DANFOSS 11/14/05    */
         sid.vendedi  =  if not avail z1-apsv or z1-apsv.edipartner = "" then
                           string(poehb.vendno) /* poeh -> poehb */
                         else
                           z1-apsv.edipartner.
/* SDI DANFOSS 11/14/05    */
  assign sid.marked   =  if can-find(first tpoehb where 
                                           tpoehb.pono = poehb.pono and
                                           tpoehb.shipmentid = sid.shipid)
                         then
                           "L"
                         else
                               " ".
end. /* for each poehb */
if v-ctr = 0 then do:
  hide frame f-lookprod.
  leave.
end.
open query q-xx for each sid.
enable  b-xx with frame f-xx.
display b-xx with frame f-xx.
run bottom_vend(input dec(substring(sid.pono,1,7)), 
                input dec(substring(sid.pono,9,2)),
                input sid.shipid).    
on cursor-up cursor-up.
on cursor-down cursor-down.
wait-for window-close of current-window. 
on cursor-up back-tab.
on cursor-down tab.
close query q-xx.
end procedure.

procedure srch-brws: 
define input-output parameter sc-recid as recid no-undo.

assign v-ctr     = 0
       v-notsrch = false.
run clear-brwse3.
if toggle-sw = no then 
for each tpoehb where 
         tpoehb.prod begins sr-prod 
         no-lock: 
  find e-poelb where recid(e-poelb) = tpoehb.poelbid no-lock no-error.
  v-ctr = v-ctr + 1.
  create srch. 
  assign srch.pono     = string(tpoehb.pono,">>>>>>9") + "-" + 
                         string(tpoehb.posuf,"99")
         srch.prod     = tpoehb.prod
         srch.vprod    = tpoehb.vprod
         srch.ctype    = if substring(e-poelb.user5,1,6) = "inb856" and
                            substring(e-poelb.user5,7,1) = "n" then
                              "!"
                         else
                         if substring(e-poelb.user5,1,6) = "inb856" and
                            substring(e-poelb.user5,7,1) = "y" then
                              "#"
                         else
                         if substring(e-poelb.user5,1,6) <> "inb856" and
                           e-poelb.qtyrcv <> 0 then
                             "#"
                         else
                         if substring(e-poelb.user5,1,6) <> "inb856" and
                           e-poelb.qtyrcv = 0 then
                             " "
                         else
                           " "
         srch.qtyord   = tpoehb.qtyord
         srch.qtyrcv   = tpoehb.qtyrcv
         srch.lineno   = tpoehb.lineno
         srch.srecid   = recid(tpoehb)
         srch.expshpdt = tpoehb.expshpdt 
         srch.shipid   = tpoehb.shipmentid.
end. /* for each poehb */
else 
for each tpoehb where 
         tpoehb.vprod begins sr-prod 
         no-lock: 

  find e-poelb where recid(e-poelb) = tpoehb.poelbid no-lock no-error.
  v-ctr = v-ctr + 1.
  create srch. 
  assign srch.pono     = string(tpoehb.pono,">>>>>>9") + "-" + 
                         string(tpoehb.posuf,"99")
         srch.prod     = if tpoehb.vprod ne "" then 
                           tpoehb.vprod
                         else 
                           tpoehb.prod
         srch.vprod    = if tpoehb.vprod ne "" then 
                           tpoehb.vprod
                         else 
                           tpoehb.prod
         srch.ctype    = if substring(e-poelb.user5,1,6) = "inb856" and
                           substring(e-poelb.user5,7,1) = "n" then
                             "!"
                         else
                         if substring(e-poelb.user5,1,6) = "inb856" and
                            substring(e-poelb.user5,7,1) = "y" then
                              "#"
                         else
                         if substring(e-poelb.user5,1,6) <> "inb856" and
                           e-poelb.qtyrcv <> 0 then
                             "#"
                         else
                         if substring(e-poelb.user5,1,6) <> "inb856" and
                           e-poelb.qtyrcv = 0 then
                             " "
                         else
                           " "
         srch.qtyord   = tpoehb.qtyord
         srch.qtyrcv   = tpoehb.qtyrcv
         srch.lineno   = tpoehb.lineno
         srch.srecid   = recid(tpoehb)
         srch.expshpdt = tpoehb.expshpdt 
         srch.shipid   = tpoehb.shipmentid.
end. /* for each poehb */

/* take a gaze on the other side to make sure */
if v-ctr = 0 then do:
  if toggle-sw = no then 
    for each tpoehb where tpoehb.prod begins sr-prod 
    no-lock:  
      find e-poelb where recid(e-poelb) = tpoehb.poelbid
      no-lock no-error.

      v-ctr = v-ctr + 1.
      create srch. 
      assign srch.pono     = string(tpoehb.pono,">>>>>>9") + "-" + 
                             string(tpoehb.posuf,"99")
             srch.prod     = tpoehb.prod
             srch.vprod    = tpoehb.vprod
             srch.ctype    = if substring(e-poelb.user5,1,6) = "inb856" and
                              substring(e-poelb.user5,7,1) = "n" then
                              "!"
                           else
                           if substring(e-poelb.user5,1,6) = "inb856" and
                              substring(e-poelb.user5,7,1) = "y" then
                              "#"
                           else
                           if substring(e-poelb.user5,1,6) <> "inb856" and
                              e-poelb.qtyrcv <> 0 then
                              "#"
                           else
                           if substring(e-poelb.user5,1,6) <> "inb856" and
                              e-poelb.qtyrcv = 0 then
                              " "
                           else
                              " "
             srch.qtyord   = tpoehb.qtyord
             srch.qtyrcv   = tpoehb.qtyrcv
             srch.lineno   = tpoehb.lineno
             srch.srecid   = recid(tpoehb)
             srch.expshpdt = tpoehb.expshpdt 
             srch.shipid   = tpoehb.shipmentid.

    end. /* for each poehb */
  else 
    for each tpoehb where 
             tpoehb.vprod begins sr-prod 
            no-lock: 
      find e-poelb where recid(e-poelb) = tpoehb.poelbid
           no-lock no-error.
      v-ctr = v-ctr + 1.
      create srch. 
      assign srch.pono     = string(tpoehb.pono,">>>>>>9") + "-" + 
                             string(tpoehb.posuf,"99")
             srch.prod     = if tpoehb.vprod ne "" then 
                              tpoehb.vprod
                           else 
                              tpoehb.prod
             srch.vprod   = if tpoehb.vprod ne "" then 
                              tpoehb.vprod
                           else 
                              tpoehb.prod
             srch.ctype    = if substring(e-poelb.user5,1,6) = "inb856" and
                              substring(e-poelb.user5,7,1) = "n" then
                              "!"
                             else
                             if substring(e-poelb.user5,1,6) = "inb856" and
                                substring(e-poelb.user5,7,1) = "y" then
                                "#"
                             else
                             if substring(e-poelb.user5,1,6) <> "inb856" and
                                e-poelb.qtyrcv <> 0 then
                                "#"
                             else
                             if substring(e-poelb.user5,1,6) <> "inb856" and
                                e-poelb.qtyrcv = 0 then
                                " "
                             else
                                " "
             srch.qtyord   = tpoehb.qtyord
             srch.qtyrcv   = tpoehb.qtyrcv
             srch.lineno   = tpoehb.lineno
             srch.srecid   = recid(tpoehb)
             srch.expshpdt = tpoehb.expshpdt 
             srch.shipid   = tpoehb.shipmentid.
    end. /* for each poehb */
  end.

/* take a gaze at the srch prod  */
if v-ctr = 0 then do:
  assign v-prodsrcha = sr-prod.
  run improvise_product (input poeh.vendno,
                         input "vendedi",
                         input "product",   
                         input-output v-prodsrcha).

  for each tpoehb where 
       tpoehb.prodsrch begins v-prodsrcha
       no-lock: 
    
    find e-poelb where recid(e-poelb) = tpoehb.poelbid
        no-lock no-error.

    v-ctr = v-ctr + 1.
    create srch. 
    assign srch.pono     = string(tpoehb.pono,">>>>>>9") + "-" + 
                           string(tpoehb.posuf,"99")
           srch.prod     = tpoehb.prod
           srch.vprod    = if tpoehb.vprod ne "" then 
                              tpoehb.vprod
                           else 
                              tpoehb.prod
           srch.ctype    = if substring(e-poelb.user5,1,6) = "inb856" and
                              substring(e-poelb.user5,7,1) = "n" then
                              "!"
                           else
                           if substring(e-poelb.user5,1,6) = "inb856" and
                              substring(e-poelb.user5,7,1) = "y" then
                              "#"
                           else
                           if substring(e-poelb.user5,1,6) <> "inb856" and
                              e-poelb.qtyrcv <> 0 then
                              "#"
                           else
                           if substring(e-poelb.user5,1,6) <> "inb856" and
                              e-poelb.qtyrcv = 0 then
                              " "
                           else
                              " "
           srch.qtyord   = tpoehb.qtyord
           srch.qtyrcv   = tpoehb.qtyrcv
           srch.lineno   = tpoehb.lineno
           srch.srecid   = recid(tpoehb)
           srch.expshpdt = tpoehb.expshpdt 
           srch.shipid   = tpoehb.shipmentid.
   end. /* for each poehb */
  end.

if v-ctr = 0 then do:
  if g-currproc = "hher" then do: 
    message "g-currproc"  "v-ctr" v-ctr. pause.
    run ibrferrx.p (input "Unsuccessful search -   " + sr-prod,
                    input "yes"). 
    hide message no-pause.
   
    /* pop up a new search */
    assign v-start = "". 
    do on endkey undo, leave:
      update
        v-start 
      with frame f-lookprod2
      xxeditloop: 
      editing:
        assign z-loadcharfl = false.
        readkey.
        if {k-cancel.i} or {k-jump.i} then do:
          hide frame f-lookprod2.
          apply lastkey.
          {x-hher.i &user_display2 = *}
          leave xxeditloop.
        end.
        /* If the Alpha flag is "no", then only numeric characters
           and edit characters are allowed. */
        apply lastkey.
      end. /* editing */
    end.       
    if not {k-cancel.i} and not {k-jump.i} then do:
      assign v-bufferedsearch = true.
      hide frame f-lookprod2.
    end.
  end.
  else
    message "Can't Find->" sr-prod.
  hide frame f-lookprod.
  {{&appname} &user_display2 = "*" }
  if avail srch then
    sc-recid = recid(srch).
  leave.
end.
else if v-ctr = 1 then do:
  find first srch no-lock no-error.
  sc-recid = recid(srch).
  leave.
end.
{{&appname} &b-zz_Browse = "*" }
if sc-recid = ? then
  if g-currproc = "hher" then do: 
    /* pop up a new search */
    assign v-start = "". 
    do on endkey undo, leave:
      update
        v-start 
      with frame f-lookprod2
      xxeditloop: 
      editing:
        assign z-loadcharfl = false.
        readkey.
        if {k-cancel.i} or {k-jump.i} then do:
          hide frame f-lookprod2.
          apply lastkey.
          {x-hher.i &user_display2 = *}
          leave xxeditloop.
        end.
        /* If the Alpha flag is "no", then only numeric characters
           and edit characters are allowed. */
        apply lastkey.
      end. /* editing */
    end.       
    if not {k-cancel.i} and not {k-jump.i} then do:
      assign v-bufferedsearch = true.
      hide frame f-lookprod2.
    end.
  end.
end procedure.

Procedure RFLoop: 
assign zx-l = no
       zx-x = 1.
repeat while program-name(zx-x) <> ?:         
  if program-name(zx-x) = "hher.p" then do:                                        zx-l = yes.                              
    leave.                             
  end.                                      
  assign zx-x = zx-x + 1.                     
end.                                          
end procedure.

procedure clear-brwse:
define input parameter ip-type as character no-undo.

/* message "clear-brwse" ip-type. pause. */
if v-accum and ip-type = " " then
  return.
else
if can-find(icsd where icsd.cono = g-cono and
                       icsd.whse = ip-type no-lock) then
do:
  for each tpoehb use-index k-temp3 no-lock 
        where tpoehb.whse = ip-type
        break by (string(tpoehb.pono,"9999999") + "-" +
                  string(tpoehb.posuf,"99")):
    
    if first-of (string(tpoehb.pono,"9999999") + "-" +
                  string(tpoehb.posuf,"99")) then
    do:     
      if avail tpoehb and (tpoehb.qtyrcv = 0 or
       can-find (first poehb where poehb.cono = (g-cono + 500) and
                 poehb.pono       = tpoehb.pono          and
                 poehb.posuf      = tpoehb.posuf         and   
                 poehb.shipmentid = tpoehb.shipmentid    and   
                 poehb.user5    = "inb856"  no-lock) and
       not can-find (first poelb where poelb.cono = (g-cono + 500) and 
                     poelb.pono       = tpoehb.pono          and 
                     poelb.posuf      = tpoehb.posuf         and 
                     poelb.shipmentid = tpoehb.shipmentid and
                     substring(poelb.user5,7,1) = "y" 
                     no-lock)) then
      do: 
        /** inb856 **/
        for each poehb where poehb.cono = (g-cono + 500) and
                 poehb.pono       = tpoehb.pono          and
                 poehb.posuf      = tpoehb.posuf         and   
                 poehb.shipmentid = tpoehb.shipmentid    and   
                 poehb.user5    = "inb856"                  
                 exclusive-lock no-prefetch:                   
          for each poelb where poelb.cono = (g-cono + 500) and 
                   poelb.pono       = tpoehb.pono          and 
                   poelb.posuf      = tpoehb.posuf         and 
                   poelb.shipmentid = tpoehb.shipmentid        
                   exclusive-lock no-prefetch: 
            assign substring(poelb.user5,7,1) = "n" 
                   poelb.qtyrcv = poelb.user6              
                   poelb.stkqtyrcv = poelb.qtyrcv *             
                     (if poelb.unitconv = 0 then
                        1                       
                      else                      
                        poelb.unitconv).        

          

/* TAH 022807  clear inputed bin when clearing */
 
            run initialize_bin( input poelb.pono, 
                                input poehb.whse, 
                                input poelb.shipprod, 
                                input poelb.cono).       
          
          end.
           assign poehb.openinit = "".
        end.
            
        /* NOT inb856 */
        for each poehb where poehb.cono = (g-cono + 500) and 
                 poehb.pono       = tpoehb.pono          and 
                 poehb.posuf      = tpoehb.posuf         and 
                 poehb.shipmentid = tpoehb.shipmentid    and
                 poehb.user5      <> "inb856"
                 exclusive-lock no-prefetch: 
          for each poelb where poelb.cono = (g-cono + 500) and 
                   poelb.pono       = tpoehb.pono          and 
                   poelb.posuf      = tpoehb.posuf         and 
                   poelb.shipmentid = tpoehb.shipmentid
                   exclusive-lock no-prefetch: 

             
/* TAH 022807  clear inputed bin when clearing */
 
             run initialize_bin( input poelb.pono, 
                                 input poehb.whse, 
                                 input poelb.shipprod, 
                                 input poelb.cono).       
 
             
             delete poelb.
          end.    
          delete poehb.
        end.
      end.
    end.
  end.

  for each tpoehb 
     where tpoehb.whse = ip-type
     exclusive-lock:
    delete tpoehb.
  end.

  find first tpoehb no-lock no-error.
  if not avail tpoehb then
      assign v-vend-accum = 0
             v-vend-accum-edi = "".
 


end.
else do:                         
  
  for each tpoehb use-index k-temp3 no-lock 
     break by (string(tpoehb.pono,"9999999") + "-" +
                  string(tpoehb.posuf,"99")):
    if first-of (string(tpoehb.pono,"9999999") + "-" +
                  string(tpoehb.posuf,"99")) then
    do:     
      if avail tpoehb and (tpoehb.qtyrcv = 0 or
       can-find (first poehb where poehb.cono = (g-cono + 500) and
                 poehb.pono       = tpoehb.pono          and
                 poehb.posuf      = tpoehb.posuf         and   
                 poehb.shipmentid = tpoehb.shipmentid    and   
                 poehb.user5    = "inb856"  no-lock) and
       not can-find (first poelb where poelb.cono = (g-cono + 500) and 
                     poelb.pono       = tpoehb.pono          and 
                     poelb.posuf      = tpoehb.posuf         and 
                     poelb.shipmentid = tpoehb.shipmentid and
                     substring(poelb.user5,7,1) = "y" 
                     no-lock)) then do:
 
/*      
      if avail tpoehb and tpoehb.qtyrcv = 0 then do: 
*/
        /** inb856 **/
        
        for each poehb where poehb.cono = (g-cono + 500) and
                 poehb.pono       = tpoehb.pono          and
                 poehb.posuf      = tpoehb.posuf         and   
                 poehb.shipmentid = tpoehb.shipmentid    and   
                 poehb.user5    = "inb856"                  
                 exclusive-lock no-prefetch:                   
          for each poelb where poelb.cono = (g-cono + 500) and 
                   poelb.pono       = tpoehb.pono          and 
                   poelb.posuf      = tpoehb.posuf         and 
                   poelb.shipmentid = tpoehb.shipmentid        
                   exclusive-lock no-prefetch: 
            assign substring(poelb.user5,7,1) = "n" 
                   poelb.qtyrcv = poelb.user6              
                   poelb.stkqtyrcv = poelb.qtyrcv *             
                     (if poelb.unitconv = 0 then
                        1                       
                      else                      
                        poelb.unitconv).        

 
            run initialize_bin( input poelb.pono, 
                                input poehb.whse, 
                                input poelb.shipprod, 
                                input poelb.cono).       
           
          end.
           assign poehb.openinit = "".
        end.
         
        
        /* NOT inb856 */
        for each poehb where poehb.cono = (g-cono + 500) and 
               poehb.pono       = tpoehb.pono          and 
               poehb.posuf      = tpoehb.posuf         and 
               poehb.shipmentid = tpoehb.shipmentid    and
               poehb.user5      <> "inb856"
                exclusive-lock no-prefetch: 
                
          for each poelb where poelb.cono = (g-cono + 500) and 
                 poelb.pono       = tpoehb.pono          and 
                 poelb.posuf      = tpoehb.posuf         and 
                 poelb.shipmentid = tpoehb.shipmentid
                 exclusive-lock no-prefetch: 
 
            run initialize_bin( input poelb.pono, 
                                input poehb.whse, 
                                input poelb.shipprod, 
                                input poelb.cono).       
        
            delete poelb.
          end.    
          delete poehb.
        end.
      end.
    end.
  end.
  for each tpoehb exclusive-lock:
    delete tpoehb.
  end.    

  assign v-vend-accum = 0
         v-vend-accum-edi = "".
 end.
{{&appname} &user_clearmsg = "*"}
hide frame f-poehb no-pause.

/* message "end clear-brwse". pause. */
end procedure.
 
procedure clear-brwse2:
for each sid 
   exclusive-lock:
  delete sid.
end.    
end procedure. 

procedure clear-brwse3:
for each srch 
   exclusive-lock:
  delete srch.
end.    
end procedure. 

/* TAH 022807 */
 
procedure initialize_bin:
  define input parameter ix-pono like poelb.pono     no-undo. 
  define input parameter ix-whse like poehb.whse     no-undo. 
  define input parameter ix-prod like poelb.shipprod no-undo. 
  define input parameter ix-cono like poelb.cono     no-undo. 
          
  find first zzbin                                        
       where zzbin.cono = ix-cono /* 501 */                
         and zzbin.whse = ix-whse                            
         and zzbin.prod = ix-prod                        
         and zzbin.transproc = "PO"+ STRING(ix-pono)
          /* + "-" + STRING(poehb.posuf) */
         and zzbin.binloc ne ""  exclusive-lock no-error.                
  if avail zzbin then do:                                
         /* wmsb.statuscode */
    find wmsb where wmsb.cono = g-cono + 500 and
         wmsb.whse = zzbin.whse and 
         wmsb.binloc = zzbin.binloc and
         wmsb.statuscode = "T" exclusive-lock no-error.
    if avail wmsb then 
      assign wmsb.statuscode = "".
    delete zzbin.                                        
  end. /* Avail zzbin */                                                  
end. /* Procedure initialize_bin */

/* TAH 022807 */



procedure bldtemp2: 
def input parameter shipid as char format "x(24)".
def input parameter po-in like poeh.pono. 
define buffer zy-poehb for poehb.
run clear-brwse (" ").

find first zy-poehb where 
           zy-poehb.cono       = (g-cono + 500) and
           zy-poehb.pono       = po-in          and 
           zy-poehb.shipmentid = shipid 
           no-lock no-error. 
if avail zy-poehb then 
  find poehb where recid(poehb) = recid(zy-poehb)
  exclusive-lock no-error.
find last poeh where
          poeh.cono  = g-cono     and 
          poeh.pono  = poehb.pono and 
          poeh.posuf = poehb.posuf
          no-lock no-error.
if avail poeh and poeh.transtype = "do" then 
  leave. 

if avail poeh and can-find(first tpoehb where 
                            tpoehb.pono = poeh.pono and
                            tpoehb.shipmentid = shipid) then
  leave.   

if avail poeh and avail poehb then do: 
  assign poehb.openinit = g-operinit
         g-pono         = poeh.pono 
         g-posuf        = poeh.posuf
         poehb.user6    = 0.
  for each poelb where 
           poelb.cono       = poehb.cono  and 
           poelb.pono       = poehb.pono  and 
           poelb.posuf      = poehb.posuf and 
           poelb.shipmentid = poehb.shipmentid
           no-lock: 
    find first poel where 
               poel.cono   = poeh.cono  and 
               poel.pono   = poeh.pono  and 
               poel.posuf  = poeh.posuf and 
               poel.lineno = poelb.lineno 
               no-lock no-error. 
    find first poelo where
               poelo.cono  = g-cono and 
               poelo.pono  = poeh.pono and 
               poelo.posuf = poeh.posuf  and
               poelo.lineno = poel.lineno
               no-lock no-error.
    /* SDI DANFOSS 11/14/05    */
    find z1-apsv where z1-apsv.cono = g-cono and
                       z1-apsv.vendno = poehb.vendno no-lock no-error.
    /* SDI DANFOSS 11/14/05    */
    find first tpoehb where 
               tpoehb.pono       = poelb.pono     and
               tpoehb.posuf      = poelb.posuf    and
               tpoehb.potyp      = poeh.transtype and
               tpoehb.lineno     = poelb.lineno   and
               tpoehb.prod       = poelb.shipprod and
               tpoehb.unit       = poel.unit                   
               no-lock no-error.
    if not avail tpoehb then
      create tpoehb.
    assign tpoehb.pono       = poelb.pono
           tpoehb.posuf      = poelb.posuf
           tpoehb.potyp      = poeh.transtype
           tpoehb.lineno     = poelb.lineno 
           tpoehb.prod       = poelb.shipprod
           tpoehb.unit       = poel.unit
           tpoehb.flglbl     = poelb.xxl3
           tpoehb.qtyord     = poel.qtyord
           tpoehb.qtyrcv     = poelb.qtyrcv
           tpoehb.pqtyrcv    = poel.qtyord
           tpoehb.pstkqtyrcv = poel.stkqtyord
           tpoehb.nonstockty = poel.nonstockty
           tpoehb.whse       = poeh.whse
           tpoehb.reasunavty = poel.reasunavty
           tpoehb.price      = poel.price
           tpoehb.qtyunavail = poel.qtyunavail  
           tpoehb.poelbid    = recid(poelb)
           tpoehb.shipmentid = shipid
           tpoehb.notesfl1   = poeh.notesfl
           tpoehb.cancelfl   = poelb.cancelfl
           tpoehb.comfl      = poel.commentfl
           tpoehb.expshpdt   = poel.expshipdt
           poehb.user6       = poehb.user6 + 1
           /* SDI DANFOSS 11/14/05    */
           poehb.updtfl      = if substring(poehb.user5,1,6) = "inb856" then
                                 true
                               else
                                 poehb.updtfl  
           tpoehb.vendedi  = if not avail z1-apsv or z1-apsv.edipartner = "" 
                             then
                               string(poeh.vendno)
                             else
                               z1-apsv.edipartner.

    /* SDI DANFOSS 11/14/05    */
    assign v-prodsrcha = poelb.shipprod.
   
    run improvise_product ( input poeh.vendno,
                            input tpoehb.vendedi,
                            input "product",  
                            input-output v-prodsrcha).

    assign tpoehb.prodsrch = v-prodsrcha.
   
    find first icsw where 
               icsw.cono = poeh.cono     and 
               icsw.prod = poel.shipprod and 
               icsw.whse = poeh.whse
               no-lock no-error.
/* INSPECT */
    assign v-inspectitem = false
           v-inspectbin  = "INSP999".
    if avail icsw and icsw.wmrestrict = "insp" then do:
      assign v-inspectitem = true.
      find first wmsbp where 
                 wmsbp.cono = g-cono + 500 and
                 wmsbp.whse = poeh.whse and
                 wmsbp.prod = poel.shipprod and
                 wmsbp.binloc begins "INSP" 
                 no-lock no-error.
       if avail wmsbp then
         assign v-inspectbin = wmsbp.binloc.
       else do:
         find first zzbin where
                    zzbin.cono = g-cono + 500 and
                    zzbin.whse = tpoehb.whse and 
                    zzbin.binloc begins "INSP" and
                    zzbin.prod = poel.shipprod no-lock no-error.
         if avail zzbin then 
           assign v-inspectbin = zzbin.binloc.
         else do:
           for each wmsb where 
                    wmsb.cono = g-cono + 500 and
                    wmsb.whse = poeh.whse and
                    wmsb.binloc begins "INSP"  and
                    wmsb.statuscode = ""  and 
                    not (can-find (first zzbin where
                                         zzbin.cono = g-cono + 500 and
                                         zzbin.whse = tpoehb.whse and 
                                         zzbin.binloc = wmsb.binloc 
                                         no-lock ))
                    no-lock:
             assign v-inspectbin = wmsb.binloc.
             leave.
           end.
         end.   
      end.
    end.
/* INSPECT */    


     
    if  v-inspectitem and v-inspectbin <> "INSP999" and
       not (can-find (first zzbin where
                            zzbin.cono = g-cono + 500 and
                            zzbin.whse = tpoehb.whse and 
                            zzbin.binloc = v-inspectbin and
                            zzbin.prod = poel.shipprod no-lock)) then do:
      create zzbin.
      assign  zzbin.cono  = g-cono + 500 
              zzbin.whse  = tpoehb.whse  
              zzbin.binloc = v-inspectbin 
              zzbin.prod  = poel.shipprod
              zzbin.operinit = g-operinits                                
              zzbin.transdt = TODAY   
              zzbin.transtm = replace(STRING(TIME, "HH:MM:SS"),":","") 
              zzbin.transproc = "PO" + string(tpoehb.pono).
      
    
    end.
    
 
     
    if avail icsw then do:
      assign tpoehb.serlottype = icsw.serlottype 
             tpoehb.binloc     = 
/* INSPECT */
                                 if v-inspectitem then
                                   v-inspectbin
                                 else  
/* INSPECT */
                                 if poel.nonstockty = "N" and icsw.binloc1 = ""
                                 then
                                  "NonStock"
                                 else
                                   icsw.binloc1
/*  tah 08/28/07 */
             tpoehb.statustype = if poel.nonstockty = "N" then
                                  "N"
                                 else
                                 if poel.nonstockty = "R" then
                                  "R"
                                  else
                                   caps(icsw.statustype)
             tpoehb.vendno     = poehb.vendno
             tpoehb.vprod      = icsw.vendprod
             tpoehb.leadtm     = icsw.leadtmavg 
/* tah 08/28/07  */
             tpoehb.slotty     = if poel.nonstockty = "N" or
                                    poel.nonstockty = "R" then
                                   ""
                                 else
                                   caps(icsw.serlottype)
             tpoehb.botyp      = if avail poelo then 
                                  "T"
                                 else 
                                 if icsw.qtybo > 0 then 
                                  "B"
                                 else 
                                  " ".
      find first icsp where
                 icsp.cono = g-cono and 
                 icsp.prod = icsw.prod no-lock no-error.
      if avail icsp then 
        assign tpoehb.descrip = icsp.descrip[1].
/* tah 08/28/07 */
      if (poel.nonstockty = "N" and poel.proddesc <> "") or
         (poel.nonstockty = "R" and poel.proddesc <> "") then
         assign tpoehb.descrip = poel.proddesc.
        
    end.
    else do:
  
      assign tpoehb.descrip = poel.proddesc.
      find first wmsbp where 
                 wmsbp.cono = g-cono + 500  and
                 wmsbp.whse = poeh.whse     and 
                 wmsbp.prod = poel.shipprod and
                 wmsbp.binloc ne "RECEIVING"  /* dkt Sept 2008 */
      no-lock no-error.
      if avail wmsbp then 
        assign tpoehb.binloc = wmsbp.binloc.
      else 
        assign tpoehb.binloc = "NonStock".
  
/* TAH1206 */
      /*   DKT 01 2007 
      find zw-icsd where 
           zw-icsd.cono = g-cono and 
           zw-icsd.whse = tpoehb.whse no-lock no-error.
      if entry( 1, zw-icsd.user4) <> "Y" then do: 
        if tpoehb.binloc = "" then
          assign tpoehb.binloc = "nonstock".
      end.    
      */
/* TAH1206 */

      
      assign tpoehb.serlottype = "" 
/* tah 08/28/07 */
             tpoehb.statustype = if poel.nonstockty = "R" then "R" else "N"
             tpoehb.vendno     = poeh.vendno
             tpoehb.vprod      = ""
             tpoehb.leadtm     = 0 
             tpoehb.slotty     = ""
             tpoehb.botyp      = if avail poelo then 
                                  "T"
                                 else 
                                  " ".
    
    end.
    
 
    
    find apsv where 
         apsv.cono = g-cono and 
         apsv.vendno = poeh.vendno no-lock no-error.

    if avail apsv then 
      assign tpoehb.vname = apsv.name. 
 
    
    assign tpoehb.nosnlots = 
            if avail poel and can-do("l,s",tpoehb.serlottype) then 
              poel.nosnlots
            else 
              0.

      find first zzbin where 
                 zzbin.cono = g-cono + 500 and
                 zzbin.whse = poeh.whse    and 
                 zzbin.prod = poel.shipprod no-lock no-error.
      if avail zzbin /* INSPECT */ and not v-inspectitem then do:
        assign tpoehb.binloc = zzbin.binloc.

      end.
  end. /* for each poelb */         

 
  {{&appname} &user_headdisplay = "*"}

  display tpoehb.whse tpoehb.vendno tpoehb.vname
    with frame f-poehb-hdr. 
   
  {{&appname} &user_reqnotes = "*"} 

end.  /* avail poeh and poehb */
else 
if not avail poeh or not avail poelb then do:
  run RFLoop.
  if zx-l = yes then
    run ibrferrx.p (input "Po or ShipRec notfound",input "yes"). 
  else do:
    message " PO or ShipRec Notfound ". 
    bell.
  end.   
end.
find first poelb where 
           poelb.cono       = poehb.cono  and 
           poelb.pono       = poehb.pono  and 
           poelb.posuf      = poehb.posuf and
           poelb.shipmentid = poehb.shipmentid
           no-lock no-error. 
if avail poehb and not avail poelb then do:
  run RFLoop.
  if zx-l = yes then
    run ibrferrx.p (input "ShipId Recs Notfound..",
                    input "yes"). 
  else do:
    message " ShipId Recs Notfound... ". 
    bell.
  end.  
end.
v-newdisplay = true.
release poehb.

end procedure.




/* UPC added code */
procedure improvise_product:
define input         parameter ipx-vendor  like apsv.vendno no-undo.
define input         parameter ipx-vendedi like apsv.edipartner no-undo.
define input         parameter ipx-field  as character     no-undo. 
define input-output parameter v-prodsrch as char format "x(24)" no-undo.

/* UPC MOD */
def buffer sv-apsv for apsv.

if ipx-vendor <> 0 then do:
  find sv-apsv where 
       sv-apsv.cono = g-cono and
       sv-apsv.vendno = ipx-vendor no-lock no-error.
end.
else do:
  find sv-apsv where 
       sv-apsv.cono = g-cono and
       sv-apsv.edipartner = ipx-vendedi no-lock no-error.
end.

if avail sv-apsv then do:
  dec(v-prodsrch) no-error.
  if error-status:error then do: 
    /* only integers can be upc numbers */
  end.
  else do:
    find icsv use-index k-produpc where 
         icsv.cono = g-cono and 
         icsv.vendno = sv-apsv.vendno and
         icsv.section3 = sv-apsv.vendno and
         icsv.section4 = dec(v-prodsrch) no-lock no-error.
    if avail icsv then do:
      assign v-prodsrch = icsv.prod.   
      return.
    end.
  end.
end.
/* ipx-field = product */
/* UPC MOD */


  v-prodsrch = replace (v-prodsrch," ","").
/*  v-prodsrch = replace (v-prodsrch,"s","5").
    v-prodsrch = replace (v-prodsrch,"o","0").
    v-prodsrch = replace (v-prodsrch,"i","1").
    v-prodsrch = replace (v-prodsrch,"l","1").
    v-prodsrch = replace (v-prodsrch,"z","2").
    v-prodsrch = replace (v-prodsrch,"v","u"). 
*/
  v-prodsrch = replace (v-prodsrch,"-","").
  v-prodsrch = replace (v-prodsrch,"/","").
  v-prodsrch = replace (v-prodsrch,"*","").
  v-prodsrch = replace (v-prodsrch,'"',"").
  v-prodsrch = replace (v-prodsrch,"x","").
  v-prodsrch = replace (v-prodsrch,"~~","").
  v-prodsrch = replace (v-prodsrch,"'","").
  v-prodsrch = replace (v-prodsrch,"(","").
  v-prodsrch = replace (v-prodsrch,")","").
  v-prodsrch = replace (v-prodsrch,"`","").
  v-prodsrch = replace (v-prodsrch,".","").
  v-prodsrch = replace (v-prodsrch,"#","").
  v-prodsrch = replace (v-prodsrch,"<","").
  v-prodsrch = replace (v-prodsrch,">","").
  v-prodsrch = replace (v-prodsrch,"~\","").
end procedure. 

/* UPC added code */


/* UPC added code */


procedure vendor_special_information:
define input         parameter ipx-vendor  like apsv.vendno no-undo.
define input         parameter ipx-vendedi like apsv.edipartner no-undo.
define input         parameter ipx-field  as character     no-undo. 
define input-output  parameter ipx-fieldaval as character  no-undo.
/* UPC MOD */
def buffer sv-apsv for apsv.
def var o-ipx-fieldaval as character no-undo.

if ipx-vendor <> 0 then do:
  find sv-apsv where 
       sv-apsv.cono = g-cono and
       sv-apsv.vendno = ipx-vendor no-lock no-error.
end.
else do:
  find sv-apsv where 
       sv-apsv.cono = g-cono and
       sv-apsv.edipartner = ipx-vendedi no-lock no-error.
end.

if avail sv-apsv then do:
  
  dec(ipx-fieldaval) no-error.
  if error-status:error then do: 
    /* only integers can be upc numbers */
  end.
  else do:
    find icsv use-index k-produpc where 
         icsv.cono = g-cono and 
         icsv.vendno = sv-apsv.vendno and
         icsv.section3 = sv-apsv.vendno and
         icsv.section4 = dec(ipx-fieldaval) no-lock no-error.
    if avail icsv then do:
      assign ipx-fieldaval = icsv.prod.   
      return.
    end.
  end.
end.
/* ipx-field = product */
/* UPC MOD */

find zsastz where zsastz.cono = g-cono and
                  zsastz.labelfl = false and
                  zsastz.codeiden = "VendorChars" and
                  zsastz.primarykey = string(ipx-vendor)
                  no-lock no-error.
                  
  if avail zsastz then do:     

   def var new-fieldaval like ipx-fieldaval. 
  
     assign new-fieldaval = ipx-fieldaval. 
        /*  message "new-fieldaval" new-fieldaval.   */
  
  
    if ipx-field = "Product" and 
       substring(ipx-fieldaval,1,1) = substring(zsastz.codeval[7],1,1) then do:

      /*  message "found 7". pause.   */
    
      if ipx-fieldaval begins zsastz.codeval[7] then do:
        ipx-fieldaval = substring(ipx-fieldaval, length(zsastz.codeval[7]) + 1,
          length(ipx-fieldaval) - length(zsastz.codeval[7])).       
      
           /* message "the7" ipx-fieldaval. pause. */ 
      end.           
  end.
  
  else
  
  /*
  message "checkign"  substring(ipx-fieldaval,1,1)
                      substring(zsastz.codeval[8],1,2). pause.
  message "length" length(zsastz.codeval[8]) + 1   
          "senlen" length(ipx-fieldaval) - length(zsastz.codeval[8]). pause.
  */                    
                      
  if substring(ipx-fieldaval,1,2) = substring(zsastz.codeval[8],1,2) then 

/*  if ipx-fieldaval begins substring(zsastz.codeval[8],1,2) then  */

    assign 
    ipx-fieldaval = substring(ipx-fieldaval, length(zsastz.codeval[8]) + 1,
                    length(ipx-fieldaval) - length(zsastz.codeval[8])).       
 
        /*   message "ipx-fieldaval #8" ipx-fieldaval. pause.    */
        
        find first icsp where icsp.cono = g-cono and
                              icsp.prod = ipx-fieldaval
                              no-lock no-error.
                              
        if avail icsp then 
            message "". 
            /*
            message "found it". pause.   
            */
                              
        if not avail icsp then do:
            /*  message "notfound". pause.   */
           assign ipx-fieldaval =  new-fieldaval. 
            /*  message "new" new-fieldaval. pause.  */ 
           
          /* message "running3" ipx-field. pause.     */
        assign ipx-fieldaval = new-fieldaval. 
         /* message "after assign" ipx-fieldaval. pause.  */

        
  if substring(ipx-fieldaval,1,1) = substring(zsastz.codeval[3],1,1) then
     /* message "at 3??". pause.   */
     if ipx-fieldaval begins zsastz.codeval[3] then 
        ipx-fieldaval = substring(ipx-fieldaval, length(zsastz.codeval[3]) + 1,
          length(ipx-fieldaval) - length(zsastz.codeval[3])).       
 
        /*   message "ipx-fieldaval #3" ipx-fieldaval. pause.       */
         
        
 end. /* else do */
        
                
  end.               
                 
                 
if ipx-field = "PackSlip" and zsastz.codeval[5] <> "" then do:
  if substring(ipx-fieldaval,1,1) = substring(zsastz.codeval[5],1,1) then
    assign ipx-fieldaval = 
      substring(ipx-fieldaval,2,length(ipx-fieldaval) - 1).
  if substring(ipx-fieldaval,1,1) = substring(zsastz.codeval[5],2,1) and
    substring(zsastz.codeval[5],2,1) <> "" then
    assign ipx-fieldaval = 
      substring(ipx-fieldaval,2,length(ipx-fieldaval) - 1).
end.      
end procedure. /* vendor_special_information */



{ {&appname} &user_procedures = "*" }




