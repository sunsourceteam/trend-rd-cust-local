&if defined(user_temptable) = 2 &then
/* oerzq.p */
/* Custom Report for looking at the Quotes entered using OEEXQ.p 
09/06/2006   dkt
*/
define new shared buffer b-oeel for oeel.
define new shared buffer b2-oeel for oeel.
define new shared buffer b-oeeh for oeeh.
define new shared buffer b2-oeeh for oeeh.
def new shared var v-smwodiscfl  like sasc.smwodiscfl initial no no-undo.
def new shared var v-oecostsale  like sasc.oecostsale initial no no-undo.
def new shared var v-smcustrebfl like sasc.smcustrebfl initial yes no-undo.
def new shared var v-smvendrebfl like sasc.smvendrebfl initial yes no-undo.
def var v-cost as dec no-undo.
{speccost.gva "new shared"}
{rebnet.gva "new shared"}
{zsapboydef.i}    
{zsapboycheck.i} 
def var p-regval  as char  no-undo.    
def var v-addvaluesfl as logical no-undo.
 
/* zsap switches */
def var sw-s as logical no-undo.
def var sw-b as logical no-undo.
def var sw-c as logical no-undo.
def var sw-p as logical no-undo.
def var sw-t as logical no-undo.
/* def Ranges */
def var rQuote    as char extent 2 no-undo.    
def var rCust     as dec extent 2 no-undo.    
def var rShipTo   as char extent 2 no-undo.   
def var rVend     as dec extent 2 no-undo.   
def var rVendId   as char extent 2 no-undo.   
def var rRegion   as char extent 2 no-undo.   
def var rDistrict as char extent 2 no-undo.   
def var rProd   as char extent 2 no-undo.   
def var rEnterDt as date extent 2 no-undo.  
def var rStage  as int  extent 2 no-undo.   
def var rSlsIn  as char extent 2 no-undo.   
def var rSlsOut as char extent 2 no-undo.   
def var rTaken  as char extent 2 no-undo.   
def var rCancelDt as date extent 2 no-undo. 
def var rLostBus  as char extent 2 no-undo. 
/* def Options */
def var oSort as char no-undo.
def var oDetail as char no-undo.
def var oList   as logical no-undo.
def var oNotes  as char no-undo.
def var oConv   as logical no-undo.
def var oCancelled as logical no-undo.
def var oOpenStage as logical no-undo.
def var oLostBusiness as logical no-undo.
def var oOEETdata  as logical no-undo.
def var oExportName as character no-undo.
def var oOBC        as logical no-undo.
def var lenSort as integer no-undo.
def var iter    as integer no-undo.
def var sortfield as char extent 4 no-undo.
def var salescode as char no-undo.
def var insidecode as char no-undo.
def var currSlsrep as char no-undo.
def var slssort as char no-undo.
def var w-vendno like icsw.arpvendno.
/* notes */
def var showNotes     as char no-undo.  
def var showNoteType  as char no-undo.   
def var printNoteln   as char no-undo.  
Define buffer catmaster for notes.
Define var v-vendorid   as c format "x(24)" no-undo.
Define var v-parentcode as c format "x(24)" no-undo.
Define var v-technology as c format "x(24)" no-undo.
/* user3 vars */
def var v-contact as char no-undo.
def var v-phone   as char no-undo.
def var v-email   as char no-undo.
def var v-obc     as char format "x(3)" no-undo.
def var totCost   as dec  no-undo.
def var v-rebFlag as log  no-undo.
def var totMargin as dec  no-undo.
def var lostbuscode as char no-undo.
def var lastoeehb as recid no-undo.
def var dataRegion as char no-undo.
def var dataDistrict as char no-undo.
def var dataCustName as char no-undo.
def var lostbusreason as char no-undo.
def var v-margin as dec no-undo.
def var v-batchnm as char format "x(10)" no-undo.
def var h-transtype as char format "x(2)" no-undo.
def var specnsl as char no-undo.
def var sumPrice  as dec no-undo.
def var sumCost   as dec  no-undo.
def var HPrice  as dec no-undo.
def var HCost   as dec  no-undo.
/*das - OBC*/
def var cntOBCquote  as int  no-undo.
def var cntNOBCquote as int  no-undo.
def var OBCsumPrice   as dec  no-undo.
def var OBCsumCost    as dec  no-undo.
def var NOBCsumPrice  as dec  no-undo.
def var NOBCsumCost   as dec  no-undo.
def var v-pct1lit as char format "x(1)" no-undo.
def var v-pct2lit as char format "x(1)" no-undo.
def var v-pct3lit as char format "x(1)" no-undo.
def var v-pct4lit as char format "x(1)" no-undo.
def var v-pct5lit as char format "x(1)" no-undo.
def var linePrice like oeelb.price    no-undo.
def var lineCost  like oeelb.prodcost no-undo.
def var lineMarg  like oeelb.prodcost no-undo.
def var gtotPrice like oeelb.price no-undo.
def var gtotCost  like oeelb.prodcost  no-undo.
def var subtPrice like oeelb.price extent 5 no-undo.
def var subtCost  like oeelb.prodcost  extent 5 no-undo.
def var stPrice   like oeelb.price no-undo.
def var stCost    like oeelb.prodcost no-undo.
def var mrgPerc   as decimal format "99999.9999" no-undo.
def var cntLines  as int extent 5 no-undo.
def var cntOrders as int extent 5 no-undo.
def var cntConvert as int no-undo.
def var cntCancel  as int no-undo.
def var cntLost    as int no-undo.
def var cntOpen    as int no-undo.
def var cntOpen2   as int no-undo.
def var cntOrderOEET    as int no-undo.
def var cntOrder   as int no-undo.
def var cntLine    as int no-undo.
def var cntConvertOrders as int extent 5 no-undo.
def var cntCancelOrders  as int extent 5 no-undo.
def var unasgndtFlag as logical no-undo initial yes.
do iter = 1 to 5:
  subtPrice[iter] = 0.
  subtCost[iter] = 0.
  cntLines[iter] = 0.
  cntOrders[iter] = 0.
  cntConvertOrders[iter] = 0.
  cntCancelOrders[iter]  = 0.
end.
def temp-table repTable
  field batchnm as char
  field orderno like oeeh.orderno
  field ordersuf like oeel.ordersuf
  field oeehbid as recid
  field oeelbid as recid
  field sortfld as char 
  field sortfld2 as char
  field sortfld3 as char
  field sortfld4 as char
  field salescd as char
  field region  as char format "x"
  field district as char format "x(4)"
  field takenby  as char format "x(4)"
  field Custno   like arsc.custno
  field slsrepin like smsn.slsrep
  field slsrepout   like smsn.slsrep
  field slsrepinst  like smsn.slsrep
  field slsrepoutst like smsn.slsrep
  field lostbusinessst as char format "x(2)"
  field lostbusinesscd as char format "x(2)"
  field enterdt   like oeeh.enterdt
  field canceldt  like oeeh.enterdt
  field custname as char
  field sumPrice as dec
  field sumCost  as dec
  field sumPriceOEET as dec
  field sumCostOEET  as dec 
  field sumPriceQUOEET as dec
  field sumCostQUOEET  as dec 
  field QSumPrice as dec
  field QSumCost  as dec
  field CvtSumPrice as dec
  field CvtSumCost  as dec
  field CanSumPrice as dec
  field CanSumCost  as dec
  field OpnSumPrice as dec
  field OpnSumCost  as dec
  field cntConvert as int 
  field cntCancel  as int 
  field cntLost    as int 
  field cntOpen    as int 
  field cntOrder   as int 
  field cntOrderOEET  as int 
  field cntOrderQUOEET  as int 
  field cntOpen2   as int 
  field cntLine    as int 
  field transtype  like oeeh.transtype
  field specnstype as char format "x"
  
  field cntOBCquote  as int
  field OBCSumPrice   as dec
  field OBCSumCost    as dec
  field cntNOBCquote as int
  field NOBCSumPrice  as dec
  field NOBCSumCost   as dec
  
  index k-repTable batchnm oeehbid oeelbid
  index k-repTable2 batchnm 
                    slsrepinst  
                    slsrepoutst
                    lostbusinessst
  index k-repTable3 batchnm oeehbid 
                    slsrepinst  
                    slsrepoutst 
                    lostbusinessst
                    oeelbid 
  index k-OrepTable orderno ordersuf oeehbid oeelbid
  index k-OrepTable2 orderno ordersuf 
                    slsrepinst  
                    slsrepoutst
                    lostbusinessst
  index k-OrepTable3 orderno ordersuf oeehbid 
                    slsrepinst  
                    slsrepoutst 
                    lostbusinessst
                    oeelbid. 
def var v-slsrepoutsort as logical no-undo.
def var v-slsrepinsort  as logical no-undo.
def var v-lostbusiness  as logical no-undo.
def var v-OeetLabel1    as character format "x(9)" init
  " OEET Ord" no-undo.
def var v-OeetLabel2    as character format "x(9)" init
  "---------" no-undo.
  
def buffer b-repTable for repTable. 
def buffer h-repTable for repTable. 
 
&endif
 
&if defined(user_drpttable) = 2 &then
/* Use to add fields to the report table */
    FIELD rptrecid        AS recid   
 
&endif
 
&if defined(user_forms) = 2 &then
form
  "R Dst"       at 1
  "EnterDt"     at 8
  "Stg"         at 17
  "Tkn By"      at 26
  "Cust#"       at 39
  "Name"        at 45
  "Quote#"      at 63
  "Contact"     at 74
  "Phone#"      at 88
  "OBC"         at 102
  "TotalSales"  at 131
  "TotalCost"   at 148
  "Mgn% "       at 160
  "Slsm"        at 1
  "CancelDt"    at 8
  "Lost Bus"    at 17
  "Ship To"     at 39
  "Order#/Type" at 62
  "------"                   at 1   
  "--------"                 at 8   
  "------"                   at 17  
  "----"                     at 26  
  "----------"               at 34  
  "---------------"          at 45  
  "--------"                 at 61  
  "-------"                  at 74  
  "------"                   at 88
  "---"                      at 102
  "---------------"          at 126
  "---------------"          at 142
  "-------"                  at 158
 
with frame f-header no-labels 
     stream-io no-box no-underline width 178 page-top.  
             
/** FIX001 - TA  **/  
form header
  "Line#"           at 5
  "Product"         at 12
  "Prcty"           at 38
  "Qty"             at 49
  "Price"           at 56
  "Cost"            at 68
  "Mgn%"            at 76
  "Line PromDt"     at 82
  "SlsRep"          at 94
  "Lost Bus Reason" at 102
with frame f-lineheader no-labels
     stream-io no-box no-underline width 178 page-top.     
/** FIX001 - TA  **/  
form             
  specnsl          at 5   format "x(3)"
  oeelb.lineno     at 8   format ">>9"  
  oeelb.shipprod   at 12  format "x(24)"
  oeelb.pricetype  at 38  format "x(4)" 
  oeelb.qtyord     at 43  format "->>>,>>9"    
  linePrice        at 52  format "->>>,>>>.99"    
  lineCost         at 64  format "->>>,>>>.99"    
  v-margin         at 75  format "->>9.99"      
  oeelb.promisedt  at 84
  currSlsrep       at 96  format "x(4)"
  oeelb.reasunavty at 101 format "x(4)"
  lostbusreason    at 106 format "x(24)"
with frame f-linedtl no-labels 
     stream-io no-box no-underline width 178.
form
  " "               at 1 
  repTable.region   at 1 format "x(1)"
  repTable.district at 3 format "x(3)"
  oeehb.EnterDt     at 8
  oeehb.Stagecd     at 17
  oeehb.TakenBy     at 26
  oeehb.CustNo      at 34 format ">>>>>>>>>9"
  repTable.custname at 45 format "x(15)"
  v-batchnm         at 61 format "x(11)"
  v-contact         at 74 format "x(13)"
  v-phone           at 88  format "x(12)"
  v-obc             at 102
  repTable.sumPrice at 126 format ">>>,>>>,>>>.99-"
  repTable.sumCost  at 142 format ">>>,>>>,>>>.99-"
  v-margin          at 158 format ">>9.99-"
  repTable.salescd at 1  format "x(4)"
  oeehb.CancelDt   at 8  format  "99/99/99" 
  lostbuscode      at 17 format "x(2)"
  oeehb.ShipTo     at 40 format "x(8)"
  oeehb.orderno    at 59 format ">>>>>>>>9"
  repTable.transtype  at 70 format "x(2)"
  "=================" at 126
  "=============="    at 144
  "======="           at 159
with frame f-oeehb no-labels /* stream-io */ 
     no-box no-underline width 178 down.
form
  showNotes     at 5  format "x(6)"
  showNoteType  at 13 format "x(3)"
  printNoteln   at 17 format "x(80)"
with frame f-notes no-labels 
     stream-io no-box no-underline width 178 down.
form
  sortfield[1]  at 1   format "x(10)"
  sortfield[2]  at 12  format "x(10)"
  sortfield[3]  at 22  format "x(10)"
  sortfield[4]  at 32  format "x(10)"
  cntConvert    at 45  format "->>>9" label "Conv:"
  cntCancel     at 55  format "->>>9" label "Canc:"
  cntOrder      at 65  format "->>>9" label "Ord:"
  cntLine       at 75  format "->>,>>9" label "Lines:"
  stPrice       at 126 format "->>>,>>>,>>>.99"   
  stCost        at 142 format "->>>,>>>,>>>.99"   
  mrgPerc       at 158 format "->>9.99"   
with frame f-subtotal no-labels 
     stream-io no-box no-underline width 178 down.
form
  cntConvertOrders[5]   at 1   format "->>>,>>>,>>9" label "Total Converted:"
  cntCancelOrders[5]    at 20  format "->>>,>>>,>>9" label "Total Cancelled:"
  cntOrders[5]          at 40  format "->>>,>>>,>>9" label "Total Orders:"
  cntLines[5]           at 60  format "->>>,>>>,>>9" label "Total Lines:"
  gtotPrice             at 126 format "->>>,>>>,>>9.99" label "TotalSales:"   
  gtotCost              at 142 format "->>>,>>>,>>9.99" label "TotalCost:"  
  mrgPerc               at 158 format "->>9.99"         label "Margin:"
with frame f-gtotal 
     stream-io no-box no-underline width 178.  
/*
form
  v-final                at 1
  v-astrik               at 2   format "x(5)"
  v-summary-lit2         at 7   format "x(27)"
  v-amt7   at 35  format ">,>>>,>>9" label "Tot Orders"
  v-amt6   at 49  format ">,>>>,>>9" label "Tot Lines"
  v-amt3   at 63  format ">,>>>,>>9" label "Tot Conv Ord."
  v-amt4   at 74  format ">,>>>,>>9" label "Tot Can Ord. "
  v-amt1   at 126 format ">>>,>>>,>>9.99-"  label "TotalSales"  
  v-amt2   at 142 format ">>>,>>>,>>9.99-"  label "TotalCost"  
  v-amt5   at 158 format ">>9.99-"          label "Margin%"
  "~015"   at 178
with frame f-tot width 178
 no-box no-labels.  
*/
form
  "  Orders"              at 30
  "   Lines"              at 39
  " CVt Ord"              at 48
  " Can Ord"              at 57
  " Opn Ord"              at 66
  " OBC Ord"              at 75
  "NOBC Ord"              at 84
  v-OeetLabel1            at 93
  "Ratio"                 at 118
  "     TotalSales"       at 126
  "      TotalCost"       at 142
  "  Mgn% "               at 158
  "--------"              at 30
  "--------"              at 39
  "--------"              at 48
  "--------"              at 57
  "--------"              at 66
  "--------"              at 75
  "--------"              at 84
  v-OeetLabel2            at 93
  "-------"               at 117
  "---------------"       at 126
  "---------------"       at 142
  "-------"               at 158
  v-final                at 1
  v-astrik               at 2   format "x(5)"
  v-summary-lit2         at 7   format "x(27)"
  v-amt7   at 30  format ">>>,>>9"             no-label 
  v-amt6   at 39  format ">>>,>>9"             no-label
  v-amt3   at 48  format ">>>,>>9"             no-label
  v-amt4   at 57  format ">>>,>>9"             no-label 
  v-amt25  at 66  format ">>>,>>9"             no-label
  v-amt35  at 75  format ">>>,>>9"             no-label   /*das*/
  v-amt40  at 84  format ">>>,>>9"             no-label   /*das*/
  v-amt26  at 93  format ">>>,>>>"             no-label  
  "Quoted   :"   at 106
  v-amt8   at 126 format ">>>,>>>,>>9.99-"     no-label   
  v-amt9   at 142 format ">>>,>>>,>>9.99-"     no-label   
  v-amt10   at 158 format ">>9.99-"            no-label 
  "Converted:"   at 106
  v-amt22   at 117 format ">>9.99-"             no-label 
  v-pct1lit at 124                              no-label
  v-amt11   at 126 format ">>>,>>>,>>9.99-"     no-label   
  v-amt12   at 142 format ">>>,>>>,>>9.99-"     no-label   
  v-amt13   at 158 format ">>9.99-"             no-label 
  "LBusiness:"   at 106
  v-amt23   at 117 format ">>9.99-"             no-label 
  v-pct2lit at 124                              no-label
  v-amt14   at 126 format ">>>,>>>,>>9.99-"     no-label   
  v-amt15   at 142 format ">>>,>>>,>>9.99-"     no-label  
  v-amt16   at 158 format ">>9.99-"             no-label 
  "Open     :"   at 106
  v-amt24   at 117 format ">>9.99-"             no-label 
  v-pct3lit at 124                              no-label
  v-amt17   at 126 format ">>>,>>>,>>9.99-"     no-label   
  v-amt18   at 142 format ">>>,>>>,>>9.99-"     no-label   
  v-amt19   at 158 format ">>9.99-"             no-label 
  
  "OBC      :"   at 106
  v-amt36   at 117 format ">>9.99-"             no-label
  v-pct4lit at 124                              no-label
  v-amt37   at 126 format ">>>,>>>,>>9.99-"     no-label
  v-amt38   at 142 format ">>>,>>>,>>9.99-"     no-label
  v-amt39   at 158 format ">>9.99-"             no-label
  "Non OBC :"   at 106
  v-amt41   at 117 format ">>9.99-"             no-label
  v-pct5lit at 124                              no-label
  v-amt42   at 126 format ">>>,>>>,>>9.99-"     no-label
  v-amt43   at 142 format ">>>,>>>,>>9.99-"     no-label
  v-amt44   at 158 format ">>9.99-"             no-label
  
  "~015"   at 178
with frame f-tot width 178 no-underline
 no-box no-labels.  
def var wd2-amt1  as dec no-undo.
def var wd2-amt2  as dec no-undo.
def var vd2-qty1  as dec no-undo.
def var vd2-qty2  as dec no-undo.
def var vd2-qty3  as dec no-undo.
def var vd2-qty4  as dec no-undo.
def var vd2-amt1  as dec no-undo.
def var vd2-amt2  as dec no-undo.
def var vd2-amt3  as dec no-undo.
def var vd2-amt4  as dec no-undo.
def var vd2-amt5  as dec no-undo.
def var vd2-amt6  as dec no-undo.
def var vd2-amt7  as dec no-undo.
def var vd2-amt8  as dec no-undo.
def var vd2-amt9  as dec no-undo.
def var vd2-amt10 as dec no-undo.
def var vd2-amt11 as dec no-undo.
def var vd2-amt12 as dec no-undo.
def var vd2-amt13 as dec no-undo.
def var vd2-amt14 as dec no-undo.
def var vd2-amt15 as dec no-undo.
def var vd2-amt16 as dec no-undo.
def var vd2-pctlit1 as char format "x(1)" no-undo.
def var vd2-pctlit2 as char format "x(1)" no-undo.
def var vd2-pctlit3 as char format "x(1)" no-undo.
def var vd2-pctlit4 as char format "x(1)" no-undo.
def var vd2-strpct1 as char format "x(7)" no-undo.
def var vd2-strpct2 as char format "x(7)" no-undo.
def var vd2-strpct3 as char format "x(7)" no-undo.
def var vd2-strpct4 as char format "x(7)" no-undo.
form
  "COMPLIANCE DATA    "   at 70                   
  "                   "   at 70                   
  "OEEXQ Entered     :"   at 70
  vd2-qty1   at 90  format ">,>>>,>>>"            no-label
  vd2-strpct1  at 117                             no-label 
  vd2-pctlit1 at 124                              no-label
  vd2-amt2   at 126 format ">>>,>>>,>>9.99-"      no-label   
  vd2-amt3   at 142 format ">>>,>>>,>>9.99-"      no-label   
  vd2-amt4   at 158 format ">>9.99-"              no-label 
  "OEEXQ Converted   :"   at 70
  vd2-qty2     at 90  format ">,>>>,>>>"         no-label
  vd2-strpct2  at 117                               no-label 
  vd2-pctlit2 at 124                              no-label
  vd2-amt6   at 126 format ">>>,>>>,>>9.99-"      no-label   
  vd2-amt7   at 142 format ">>>,>>>,>>9.99-"      no-label   
  vd2-amt8   at 158 format ">>9.99-"              no-label 
  "OEET&OEEXQ Entered:"   at 70
  vd2-qty3   at 90  format ">,>>>,>>>"            no-label
  vd2-strpct3 at 117                              no-label 
  vd2-pctlit2 at 124                              no-label
  vd2-amt10   at 126 format ">>>,>>>,>>9.99-"     no-label   
  vd2-amt11   at 142 format ">>>,>>>,>>9.99-"     no-label   
  vd2-amt12   at 158 format ">>9.99-"             no-label 
  "OEET QU Entered:"   at 70
  vd2-qty4   at 90  format ">,>>>,>>>"            no-label
  vd2-strpct4 at 117                              no-label 
  vd2-pctlit2 at 124                              no-label
  vd2-amt14   at 126 format ">>>,>>>,>>9.99-"     no-label   
  vd2-amt15   at 142 format ">>>,>>>,>>9.99-"     no-label   
  vd2-amt16   at 158 format ">>9.99-"             no-label 
  "~015"    at 178
with frame f-tot2 width 178 no-underline
 no-box no-labels.  
form 
  " "              at 1
  "~015"           at 178
with frame f-skip width 178 no-box no-labels.  
 
&endif
 
&if defined(user_extractrun) = 2 &then
 run sapb_vars.
 run extract_data.
 if oOEETdata then
   run extractOrders_data.
 else
   assign v-OeetLabel1  = ""
          v-OeetLabel2  = "".
 
&endif
 
&if defined(user_exportstatDWheaders) = 2 &then
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  
  assign export_rec = export_rec + v-del +
                      "OeexqEnteredOrders" + v-del +
                      "OeexqEnteredLines" + v-del +
                      "OeexqConvertedOrders" + v-del +
                      "OeexqCanceledOrders" + v-del +
                      "OeexqOpenOrders".
  if oOEETdata then
    assign export_rec = export_rec + v-del +
                         "OeetEnteredOrders".
  assign export_rec = export_rec + v-del +
                      "OeexqQuotedSell" + v-del +
                      "OeexqQuotedCost" + v-del +
                      "OeexqQuotedMgn" + v-del +
                      "OeexqConvertedRatio" + v-del +
                      "OeexqConvertedSell" + v-del +
                      "OeexqConvertedCost" + v-del +
                      "OeexqConvertedMgn" + v-del +
                      "OeexqLbusinessRatio" + v-del +
                      "OeexqLbusinessSell" + v-del +
                      "OeexqLbusinessCost" + v-del +
                      "OeexqLbusinessMgn" + v-del +
                      "OeexqOpenRatio" + v-del +
                      "OeexqOpenSell" + v-del +
                      "OeexqOpenCost" + v-del +
                      "OeexqOpenMgn".
                       
/* Compliance */              
               if oOEETdata  then 
                 assign export_rec = export_rec + v-del +
                                                 
                      "OeexqEnteredOrders"  + v-del +
                      "OeexqEnteredRatio" + v-del +
                      "OeexqEnteredSell" + v-del +
                      "OeexqEnteredCost" + v-del +
                      "OeexqEnteredMgn" + v-del +
                                                 
                      "OeexqConvertedOrders"  + v-del +
                      "OeexqConvertedRatio" + v-del +
                      "OeexqConvertedSell" + v-del +
                      "OeexqConvertedCost" + v-del +
                      "OeexqConvertedMgn" + v-del +
                                                  
                      "OeetOeexqEnteredOrders"  + v-del +
                      "OeetOeexqEnteredRatio" + v-del +
                      "OeetOeexqEnteredSell" + v-del +
                      "OeetOeexqEnteredCost" + v-del +
                      "OeetOeexqEnteredMgn" + v-del +
                                                  
                      "OeetQUEnteredOrders"  + v-del +
                      "OeetQUEnteredRatio" + v-del +
                      "OeetQUEnteredSell" + v-del +
                      "OeetQUEnteredCost" + v-del +
                      "OeetQUEnteredMgn".
  assign export_rec = export_rec + v-del +
                      " " + v-del +
                      " ".
 
&endif
 
&if defined(user_exportstatheaders) = 2 &then
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  
  
  assign export_rec = export_rec + v-del +
                      "OeexqEnteredOrders" + v-del +
                      "OeexqEnteredLines" + v-del +
                      "OeexqConvertedOrders" + v-del +
                      "OeexqCanceledOrders" + v-del +
                      "OeexqOpenOrders".
  if oOEETdata then
    assign export_rec = export_rec + v-del +
                         "OeetEnteredOrders".
  assign export_rec = export_rec + v-del +
                      "OeexqQuotedSell" + v-del +
                      "OeexqQuotedCost" + v-del +
                      "OeexqQuotedMgn" + v-del +
                      "OeexqConvertedRatio" + v-del +
                      "OeexqConvertedSell" + v-del +
                      "OeexqConvertedCost" + v-del +
                      "OeexqConvertedMgn" + v-del +
                      "OeexqLbusinessRatio" + v-del +
                      "OeexqLbusinessSell" + v-del +
                      "OeexqLbusinessCost" + v-del +
                      "OeexqLbusinessMgn" + v-del +
                      "OeexqOpenRatio" + v-del +
                      "OeexqOpenSell" + v-del +
                      "OeexqOpenCost" + v-del +
                      "OeexqOpenMgn".
                       
/* Compliance */              
               if oOEETdata  then 
                 assign export_rec = export_rec + v-del +
                                                 
                      "OeexqEnteredOrders"  + v-del +
                      "OeexqEnteredRatio" + v-del +
                      "OeexqEnteredSell" + v-del +
                      "OeexqEnteredCost" + v-del +
                      "OeexqEnteredMgn" + v-del +
                                                 
                      "OeexqConvertedOrders"  + v-del +
                      "OeexqConvertedRatio" + v-del +
                      "OeexqConvertedSell" + v-del +
                      "OeexqConvertedCost" + v-del +
                      "OeexqConvertedMgn" + v-del +
                                                  
                      "OeetOeexqEnteredOrders"  + v-del +
                      "OeetOeexqEnteredRatio" + v-del +
                      "OeetOeexqEnteredSell" + v-del +
                      "OeetOeexqEnteredCost" + v-del +
                      "OeetOeexqEnteredMgn" + v-del +
                                                  
                      "OeetQUEnteredOrders"  + v-del +
                      "OeetQUEnteredRatio" + v-del +
                      "OeetQUEnteredSell" + v-del +
                      "OeetQUEnteredCost" + v-del +
                      "OeetQUEnteredMgn".
  assign export_rec = export_rec + v-del +
                      " " + v-del +
                      " ".
 
&endif
 
&if defined(user_foreach) = 2 &then
for each repTable where repTable.oeelbid = 0 no-lock:
 
&endif
 
&if defined(user_drptassign) = 2 &then
 /* If you add fields to the dprt table this is where you assign them */
   drpt.rptrecid = recid(repTable)
 
&endif
 
&if defined(user_B4endloop) = 2 &then
  /* this is before the end of the for each loop to feed the data BUILDREC */
 
&endif
 
&if defined(user_viewheadframes) = 2 &then
/* Since frames are unique to each program you need to put your
   views and hides here with your names */
if not p-exportl then
  do:
  if not x-stream then
    do:
    hide frame f-header.
    end.
  else
   do:
    hide stream xpcd frame f-header.
    end.
  
  end.
if not p-exportl  then
  do:
  if not x-stream then
    do:
    page.
    if oDetail <> "s" then
      view frame f-header.
    end.
  else  
    do:
    page stream xpcd.
    if oDetail > "s" then
      view stream xpcd frame f-header.
    end.
   
  end.
 
&endif
 
&if defined(user_drptloop) = 2 &then
 
&endif
 
&if defined(user_detailprint) = 2 &then
   /* Comment this out if you are not using detail 'X' option  */
   if p-detail = "d"   and oDetail <> "s" and
      entry(u-entitys,drpt.sortmystery,v-delim) <> 
      fill("~~",length(entry(u-entitys,drpt.sortmystery,v-delim))) then 
     do:
     /* this code skips the all other lines for printing detail */
     
     /* next line would be your detail print procedure */
     run DetailPrint (input  drpt.rptrecid ).
     end.
 
&endif
 
&if defined(user_finalprint) = 2 &then
 assign t-amounts5[u-entitys + 1] =
        (if t-amounts1[u-entitys + 1] = 0 then 0
         else if ((t-amounts1[u-entitys + 1] - t-amounts2[u-entitys + 1]) /
                   t-amounts1[u-entitys + 1]) * 100 < -999 then -999
         else if ((t-amounts1[u-entitys + 1] - t-amounts2[u-entitys + 1]) /
                   t-amounts1[u-entitys + 1]) * 100 > 999 then 999
         else ((t-amounts1[u-entitys + 1] - t-amounts2[u-entitys + 1]) /
                t-amounts1[u-entitys + 1]) * 100).
 assign t-amounts10[u-entitys + 1] =
        (if t-amounts8[u-entitys + 1] = 0 then 0
         else if ((t-amounts8[u-entitys + 1] - t-amounts9[u-entitys + 1]) /
                   t-amounts8[u-entitys + 1]) * 100 < -999 then -999
         else if ((t-amounts8[u-entitys + 1] - t-amounts9[u-entitys + 1]) /
                   t-amounts8[u-entitys + 1]) * 100 > 999 then 999
         else ((t-amounts8[u-entitys + 1] - t-amounts9[u-entitys + 1]) /
                t-amounts8[u-entitys + 1]) * 100).
 assign t-amounts13[u-entitys + 1] =
        (if t-amounts11[u-entitys + 1] = 0 then 0
         else if ((t-amounts11[u-entitys + 1] - t-amounts12[u-entitys + 1]) /
                   t-amounts11[u-entitys + 1]) * 100 < -999 then -999
         else if ((t-amounts11[u-entitys + 1] - t-amounts12[u-entitys + 1]) /
                   t-amounts11[u-entitys + 1]) * 100 > 999 then 999
         else ((t-amounts11[u-entitys + 1] - t-amounts12[u-entitys + 1]) /
                t-amounts11[u-entitys + 1]) * 100).
 assign t-amounts16[u-entitys + 1] =
        (if t-amounts14[u-entitys + 1] = 0 then 0
         else if ((t-amounts14[u-entitys + 1] - t-amounts15[u-entitys + 1]) /
                   t-amounts14[u-entitys + 1]) * 100 < -999 then -999
         else if ((t-amounts14[u-entitys + 1] - t-amounts15[u-entitys + 1]) /
                   t-amounts14[u-entitys + 1]) * 100 > 999 then 999
         else ((t-amounts14[u-entitys + 1] - t-amounts15[u-entitys + 1]) /
                t-amounts14[u-entitys + 1]) * 100).
 assign t-amounts19[u-entitys + 1] =
        (if t-amounts17[u-entitys + 1] = 0 then 0
         else if ((t-amounts17[u-entitys + 1] - t-amounts18[u-entitys + 1]) /
                   t-amounts17[u-entitys + 1]) * 100 < -999 then -999
         else if ((t-amounts17[u-entitys + 1] - t-amounts18[u-entitys + 1]) /
                   t-amounts17[u-entitys + 1]) * 100 > 999 then 999
         else ((t-amounts17[u-entitys + 1] - t-amounts18[u-entitys + 1]) /
                t-amounts17[u-entitys + 1]) * 100).
 
 assign t-amounts39[u-entitys + 1] =
        (if t-amounts37[u-entitys + 1] = 0 then 0
         else if ((t-amounts37[u-entitys + 1] - t-amounts38[u-entitys + 1]) /
                   t-amounts37[u-entitys + 1]) * 100 < -999 then -999
         else if ((t-amounts37[u-entitys + 1] - t-amounts38[u-entitys + 1]) /
                   t-amounts37[u-entitys + 1]) * 100 > 999 then 999
         else ((t-amounts37[u-entitys + 1] - t-amounts38[u-entitys + 1]) /
                t-amounts37[u-entitys + 1]) * 100).
 assign t-amounts44[u-entitys + 1] =
        (if t-amounts42[u-entitys + 1] = 0 then 0
         else if ((t-amounts42[u-entitys + 1] - t-amounts43[u-entitys + 1]) /
                   t-amounts42[u-entitys + 1]) * 100 < -999 then -999
         else if ((t-amounts42[u-entitys + 1] - t-amounts43[u-entitys + 1]) /
                   t-amounts42[u-entitys + 1]) * 100 > 999 then 999
         else ((t-amounts42[u-entitys + 1] - t-amounts43[u-entitys + 1]) /
                t-amounts42[u-entitys + 1]) * 100).
 
 assign t-amounts22[u-entitys + 1] =
        (if t-amounts7[u-entitys + 1] = 0 then 0
         else if ((t-amounts3[u-entitys + 1]) /
                   t-amounts7[u-entitys + 1]) * 100 < -999 then -999
         else if ((t-amounts3[u-entitys + 1]) /
                   t-amounts7[u-entitys + 1]) * 100 > 999 then 999
         else ((t-amounts3[u-entitys + 1]) /
                t-amounts7[u-entitys + 1]) * 100).
  assign t-amounts23[u-entitys + 1] =
        (if t-amounts7[u-entitys + 1] = 0 then 0
         else if ((t-amounts20[u-entitys + 1]) /
                   t-amounts7[u-entitys + 1]) * 100 < -999 then -999
         else if ((t-amounts20[u-entitys + 1]) /
                   t-amounts7[u-entitys + 1]) * 100 > 999 then 999
         else ((t-amounts20[u-entitys + 1]) /
                t-amounts7[u-entitys + 1]) * 100).
  assign t-amounts24[u-entitys + 1] =
        (if t-amounts7[u-entitys + 1] = 0 then 0
         else if ((t-amounts25[u-entitys + 1]) /
                   t-amounts7[u-entitys + 1]) * 100 < -999 then -999
         else if ((t-amounts25[u-entitys + 1]) /
                   t-amounts7[u-entitys + 1]) * 100 > 999 then 999
         else ((t-amounts25[u-entitys + 1]) /
                t-amounts7[u-entitys + 1]) * 100).
  
  assign t-amounts36[u-entitys + 1] =
        (if t-amounts7[u-entitys + 1] = 0 then 0
         else if ((t-amounts35[u-entitys + 1]) /
                   t-amounts7[u-entitys + 1]) * 100 < -999 then -999
         else if ((t-amounts35[u-entitys + 1]) /
                   t-amounts7[u-entitys + 1]) * 100 > 999 then 999
         else ((t-amounts35[u-entitys + 1]) /
                t-amounts7[u-entitys + 1]) * 100).
  assign t-amounts41[u-entitys + 1] =
        (if t-amounts7[u-entitys + 1] = 0 then 0
         else if ((t-amounts40[u-entitys + 1]) /
                   t-amounts7[u-entitys + 1]) * 100 < -999 then -999
         else if ((t-amounts40[u-entitys + 1]) /
                   t-amounts7[u-entitys + 1]) * 100 > 999 then 999
         else ((t-amounts40[u-entitys + 1]) /
                t-amounts7[u-entitys + 1]) * 100).
  if t-amounts36[u-entitys + 1] <> 0 then
    v-pct4lit = "%".
  else
    v-pct4lit = " ".
  if t-amounts41[u-entitys + 1] <> 0 then
    v-pct5lit = "%".
  else
    v-pct5lit = " ".
  
  if t-amounts22[u-entitys + 1] <> 0 then
     v-pct1lit = "%".          
  else
     v-pct1lit = " ".          
   if t-amounts23[u-entitys + 1] <> 0 then
     v-pct2lit = "%".          
  else
     v-pct2lit = " ".          
 
  if t-amounts24[u-entitys + 1] <> 0 then
     v-pct3lit = "%".          
  else
     v-pct3lit = " ". 
     
     
  assign v-pct1lit = "%"
         v-pct2lit = "%"
         v-pct3lit = "%".
     
/* Line 2 total information Conformity */
  if oOEETdata  then do:
    /* Entered */
    assign wd2-amt1 = t-amounts7[u-entitys + 1]
           wd2-amt2 = t-amounts7[u-entitys + 1] +   
                      t-amounts26[u-entitys + 1].                      
                      /* 08/19/10 tah
                      ( t-amounts26[u-entitys + 1]  - 
                        t-amounts3[u-entitys + 1]  ).
                      */  
    assign vd2-amt1 = (if wd2-amt2 = 0 then 0
                       else if (wd2-amt1 / wd2-amt2 ) * 100 < -999 then -999
                       else if (wd2-amt1 / wd2-amt2 ) * 100 >  999 then  999
                       else (wd2-amt1 / wd2-amt2 * 100 )).
    assign vd2-amt2 = t-amounts8[u-entitys + 1]
           vd2-amt3 = t-amounts9[u-entitys + 1]
           vd2-amt4 = (if vd2-amt2 = 0 then 0
                       else if ((vd2-amt2 - vd2-amt3) / vd2-amt2 )
                                 * 100 < -999 then -999
                       else if ((vd2-amt2 - vd2-amt3) / vd2-amt2 ) 
                                 * 100 >  999 then  999
                       else ((vd2-amt2 - vd2-amt3) / vd2-amt2 * 100) ).
    /* Converted */
    assign wd2-amt1 = t-amounts3[u-entitys + 1]
           wd2-amt2 = ( t-amounts26[u-entitys + 1] + 
                        t-amounts3[u-entitys + 1] ). /* 08/19/10 tah */
    assign vd2-amt5 = (if wd2-amt2 = 0 then 0
                       else if (wd2-amt1 / wd2-amt2 ) * 100 < -999 then -999
                       else if (wd2-amt1 / wd2-amt2 ) * 100 >  999 then  999
                       else (wd2-amt1 / wd2-amt2 * 100 )).
    assign vd2-amt6 = t-amounts11[u-entitys + 1]
           vd2-amt7 = t-amounts12[u-entitys + 1]
           vd2-amt8 = (if vd2-amt6 = 0 then 0
                       else if ((vd2-amt6 - vd2-amt7) / vd2-amt6 )
                                 * 100 < -999 then -999
                       else if ((vd2-amt6 - vd2-amt7) / vd2-amt6 ) 
                                 * 100 >  999 then  999
                       else ((vd2-amt6 - vd2-amt7) / vd2-amt6 * 100 )).
    /* OEET OEEXQ Entered */
    assign wd2-amt1 = t-amounts3[u-entitys + 1]
           wd2-amt2 = t-amounts26[u-entitys + 1].
    assign vd2-amt9 = 0. /*
                      (if wd2-amt2 = 0 then 0
                       else if (wd2-amt1 / wd2-amt2 ) * 100 < -999 then -999
                       else if (wd2-amt1 / wd2-amt2 ) * 100 >  999 then  999
                       else (wd2-amt1 / wd2-amt2 * 100 ).
                         */
                       
    assign vd2-amt10 = t-amounts8[u-entitys + 1] +
                       t-amounts27[u-entitys + 1]
           vd2-amt11 = t-amounts9[u-entitys + 1] +
                      t-amounts28[u-entitys + 1]
           vd2-amt12 = (if vd2-amt10 = 0 then 0
                       else if ((vd2-amt10 - vd2-amt11) / vd2-amt10 )
                                 * 100 < -999 then -999
                       else if ((vd2-amt10 - vd2-amt11) / vd2-amt10 ) 
                                 * 100 >  999 then  999
                       else ((vd2-amt10 - vd2-amt11) / vd2-amt10 * 100) ).
    /* OEET QU OEEXQ Entered */
    assign wd2-amt1 = t-amounts34[u-entitys + 1]
           wd2-amt2 = t-amounts26[u-entitys + 1].
    assign vd2-amt13 = (if wd2-amt2 = 0 then 0
                        else if (wd2-amt1 / wd2-amt2 ) * 100 < -999 then -999
                        else if (wd2-amt1 / wd2-amt2 ) * 100 >  999 then  999
                        else (wd2-amt1 / wd2-amt2 * 100 )).
                                               
    assign vd2-amt14 = t-amounts31[u-entitys + 1]
           vd2-amt15 = t-amounts32[u-entitys + 1]
           vd2-amt16 = (if vd2-amt14 = 0 then 0
                       else if ((vd2-amt14 - vd2-amt15) / vd2-amt14 )
                                 * 100 < -999 then -999
                       else if ((vd2-amt14 - vd2-amt15) / vd2-amt14 ) 
                                 * 100 >  999 then  999
                       else ((vd2-amt14 - vd2-amt15) / vd2-amt15 * 100) ).
   
   
   assign vd2-pctlit1 = "%"
          vd2-pctlit2 = "%"
          vd2-pctlit3 = "%".
   assign vd2-qty1 = t-amounts7[u-entitys + 1] 
          vd2-qty2 = t-amounts3[u-entitys + 1] 
          vd2-qty3 = t-amounts26[u-entitys + 1] +
          
                     /* 08/19/10 tah
                        (t-amounts26[u-entitys + 1]  -
                        t-amounts3[u-entitys + 1]  ) */                                              t-amounts7[u-entitys + 1]
          vd2-qty4 = (t-amounts34[u-entitys + 1]).
  assign vd2-strpct1 = string(vd2-amt1,">>9.99-")
         vd2-strpct2 = string(vd2-amt5,">>9.99-")
         vd2-strpct3 = if vd2-amt9 = 0 then "       "
                       else string(vd2-amt9,">>9.99-")
         vd2-strpct4 = string(vd2-amt13,">>9.99-").
 
  
  end.
          
          
          
if not p-exportl then
 do:
 if not x-stream then 
   do:
   display 
    v-final
    v-astrik
    v-summary-lit2
    t-amounts3[u-entitys + 1]   @ v-amt3
    t-amounts4[u-entitys + 1]   @ v-amt4
    t-amounts6[u-entitys + 1]   @ v-amt6
    t-amounts7[u-entitys + 1]   @ v-amt7
    t-amounts8[u-entitys + 1]   @ v-amt8
    t-amounts9[u-entitys + 1]   @ v-amt9
    t-amounts10[u-entitys + 1]   @ v-amt10
    t-amounts11[u-entitys + 1]   @ v-amt11
    t-amounts12[u-entitys + 1]   @ v-amt12
    t-amounts13[u-entitys + 1]   @ v-amt13
    t-amounts14[u-entitys + 1]   @ v-amt14
    t-amounts15[u-entitys + 1]   @ v-amt15
    t-amounts16[u-entitys + 1]   @ v-amt16
    t-amounts17[u-entitys + 1]   @ v-amt17
    t-amounts18[u-entitys + 1]   @ v-amt18
    t-amounts19[u-entitys + 1]   @ v-amt19
    t-amounts22[u-entitys + 1]   @ v-amt22
    t-amounts23[u-entitys + 1]   @ v-amt23
    t-amounts24[u-entitys + 1]   @ v-amt24
    t-amounts25[u-entitys + 1]   @ v-amt25
    (if oOEETdata then
       t-amounts26[u-entitys + 1]
     else
        0) @ v-amt26
      /* 08/19/10 tah
         if (t-amounts26[u-entitys + 1] -
           t-amounts3[u-entitys + 1]) ge 0 then
          (t-amounts26[u-entitys + 1] -
           t-amounts3[u-entitys + 1])            
       else
         0
     else
       0)  
         @ v-amt26 */
    /*das - OBC*/
    t-amounts35[u-entitys + 1]   @ v-amt35
    t-amounts40[u-entitys + 1]   @ v-amt40
    t-amounts36[u-entitys + 1]   @ v-amt36
    t-amounts37[u-entitys + 1]   @ v-amt37
    t-amounts38[u-entitys + 1]   @ v-amt38
    t-amounts39[u-entitys + 1]   @ v-amt39
    t-amounts41[u-entitys + 1]   @ v-amt41
    t-amounts42[u-entitys + 1]   @ v-amt42
    t-amounts43[u-entitys + 1]   @ v-amt43
    t-amounts44[u-entitys + 1]   @ v-amt44
    v-pct1lit  v-pct2lit  v-pct3lit v-pct4lit v-pct5lit    
    v-OeetLabel1 v-OeetLabel2 
    with frame f-tot.
  
   down with frame f-tot.
   
   if oOEETdata  then do:
     display 
          vd2-strpct1 
          vd2-pctlit1
          vd2-amt2
          vd2-amt3
          vd2-amt4
          vd2-strpct2
          vd2-pctlit2
          vd2-amt6
          vd2-amt7
          vd2-amt8
          vd2-strpct3 
          vd2-pctlit2
          vd2-amt10
          vd2-amt11
          vd2-amt12
          vd2-strpct4 
          vd2-pctlit2
          vd2-amt14
          vd2-amt15
          vd2-amt16
          vd2-qty1
          vd2-qty2
          vd2-qty3
          vd2-qty4
          
          
       with frame f-tot2.
  
     down with frame f-tot2.
   end.
   end.
 else
   do:
   display stream xpcd
    v-final
    v-astrik
    v-summary-lit2
    t-amounts3[u-entitys + 1]   @ v-amt3
    t-amounts4[u-entitys + 1]   @ v-amt4
    t-amounts6[u-entitys + 1]   @ v-amt6
    t-amounts7[u-entitys + 1]   @ v-amt7
    t-amounts8[u-entitys + 1]   @ v-amt8
    t-amounts9[u-entitys + 1]   @ v-amt9
    t-amounts10[u-entitys + 1]   @ v-amt10
    t-amounts11[u-entitys + 1]   @ v-amt11
    t-amounts12[u-entitys + 1]   @ v-amt12
    t-amounts13[u-entitys + 1]   @ v-amt13
    t-amounts14[u-entitys + 1]   @ v-amt14
    t-amounts15[u-entitys + 1]   @ v-amt15
    t-amounts16[u-entitys + 1]   @ v-amt16
    t-amounts17[u-entitys + 1]   @ v-amt17
    t-amounts18[u-entitys + 1]   @ v-amt18
    t-amounts19[u-entitys + 1]   @ v-amt19
    t-amounts22[u-entitys + 1]   @ v-amt22
    t-amounts23[u-entitys + 1]   @ v-amt23
    t-amounts24[u-entitys + 1]   @ v-amt24
    t-amounts25[u-entitys + 1]   @ v-amt25
    (if oOEETdata then
       t-amounts26[u-entitys + 1]  
     else 0  )
       
 /* 08/19/10 tah
       if (t-amounts26[u-entitys + 1] -
           t-amounts3[u-entitys + 1]) ge 0 then
          (t-amounts26[u-entitys + 1] -
           t-amounts3[u-entitys + 1])
       else
         0
     else
       0) */  @ v-amt26
   
    /*das - OBC*/
    t-amounts35[u-entitys + 1]   @ v-amt35
    t-amounts40[u-entitys + 1]   @ v-amt40
    t-amounts36[u-entitys + 1]   @ v-amt36
    t-amounts37[u-entitys + 1]   @ v-amt37
    t-amounts38[u-entitys + 1]   @ v-amt38
    t-amounts39[u-entitys + 1]   @ v-amt39
    t-amounts41[u-entitys + 1]   @ v-amt41
    t-amounts42[u-entitys + 1]   @ v-amt42
    t-amounts43[u-entitys + 1]   @ v-amt43
    t-amounts44[u-entitys + 1]   @ v-amt44
    v-pct1lit  v-pct2lit  v-pct3lit v-pct4lit v-pct5lit
    v-OeetLabel1 v-OeetLabel2 
    with frame f-tot.
  
   down stream xpcd with frame f-tot.
      
   if oOEETdata  then do:
     display  stream xpcd
          vd2-strpct1
          vd2-pctlit1
          vd2-amt2
          vd2-amt3
          vd2-amt4
          vd2-strpct2
          vd2-pctlit2
          vd2-amt6
          vd2-amt7
          vd2-amt8
          vd2-strpct3 
          vd2-pctlit2
          vd2-amt10
          vd2-amt11
          vd2-amt12
          vd2-strpct4 
          vd2-pctlit2
          vd2-amt14
          vd2-amt15
          vd2-amt16
          vd2-qty1
          vd2-qty2
          vd2-qty3
          vd2-qty4
          
       with frame f-tot2.
  
     down stream xpcd with frame f-tot2.
   end.
   end.
      
 end.
else
  do:
  assign export_rec =  v-astrik.
  do v-inx4 = 1 to u-entitys:
    if v-totalup[v-inx4] = "n" then
       next.
    if v-inx4 = 1 then
      v-summary-lit2 = "Final".
    else
      v-summary-lit2 = "".
    if v-cells[v-inx4]= "2" then
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec + v-summary-lit2 + v-del + " ".
     
      end.
    else
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec +  v-summary-lit2.
      end.
  end.
  assign export_rec = export_rec + v-del +
      string(t-amounts7[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts6[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts3[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts4[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts25[u-entitys + 1],"->>>>>>>>9").
  if oOEETdata then
    assign export_rec = export_rec + v-del +
/*   08/19/10 tah
          string( if (t-amounts26[u-entitys + 1] -
                      t-amounts3[u-entitys + 1]) ge 0  then
                    (t-amounts26[u-entitys + 1] -
                      t-amounts3[u-entitys + 1]) 
                  else
                     0  ,"->>>>>>>>9").   */
           string(t-amounts26[u-entitys + 1],"->>>>>>>>9").
  assign export_rec = export_rec + v-del +
      string(t-amounts8[u-entitys + 1],"->>>>>>>>9.99") + v-del +
      string(t-amounts9[u-entitys + 1],"->>>>>>>>9.99") + v-del +
      string(t-amounts10[u-entitys + 1],"->>>>>>>>9.99") + v-del +
      string(t-amounts22[u-entitys + 1],"->>>>>>>>9.99") + v-del +
      string(t-amounts11[u-entitys + 1],"->>>>>>>>9.99") + v-del +
      string(t-amounts12[u-entitys + 1],"->>>>>>>>9.99") + v-del +
      string(t-amounts13[u-entitys + 1],"->>>>>>>>9.99") + v-del +
      string(t-amounts23[u-entitys + 1],"->>>>>>>>9.99") + v-del +
      string(t-amounts14[u-entitys + 1],"->>>>>>>>9.99") + v-del +
      string(t-amounts15[u-entitys + 1],"->>>>>>>>9.99") + v-del +
      string(t-amounts16[u-entitys + 1],"->>>>>>>>9.99") + v-del +
      string(t-amounts24[u-entitys + 1],"->>>>>>>>9.99") + v-del +
      string(t-amounts17[u-entitys + 1],"->>>>>>>>9.99") + v-del +
      string(t-amounts18[u-entitys + 1],"->>>>>>>>9.99") + v-del +
      string(t-amounts19[u-entitys + 1],"->>>>>>>>9.99").
  if oOEETdata then
    assign export_rec = export_rec + v-del +
      string(vd2-qty1,"->>>>>>>>9.99") + v-del +
      vd2-strpct1 + v-del +
      string(vd2-amt2,"->>>>>>>>9.99") + v-del +
      string(vd2-amt3,"->>>>>>>>9.99") + v-del +
      string(vd2-amt4,"->>>>>>>>9.99") + v-del +
 
      string(vd2-qty2,"->>>>>>>>9.99") + v-del +
      vd2-strpct2 + v-del +
      string(vd2-amt6,"->>>>>>>>9.99") + v-del +
      string(vd2-amt7,"->>>>>>>>9.99") + v-del +
      string(vd2-amt8,"->>>>>>>>9.99") + v-del +
 
      string(vd2-qty3,"->>>>>>>>9.99") + v-del +
      vd2-strpct3 + v-del +
      string(vd2-amt10,"->>>>>>>>9.99") + v-del +
      string(vd2-amt11,"->>>>>>>>9.99") + v-del +
      string(vd2-amt12,"->>>>>>>>9.99") + v-del +
 
      string(vd2-qty4,"->>>>>>>>9.99") + v-del +
      vd2-strpct4 + v-del +
      string(vd2-amt14,"->>>>>>>>9.99") + v-del +
      string(vd2-amt15,"->>>>>>>>9.99") + v-del +
      string(vd2-amt16,"->>>>>>>>9.99").
      
    
  assign export_rec = export_rec + chr(13).    
  put stream expt unformatted export_rec.
  end.
 
 
 
&endif
 
&if defined(user_summaryframeprint) = 2 &then
    assign t-amounts5[v-inx2] =
       (if t-amounts1[v-inx2] = 0 then 0
        else if ((t-amounts1[v-inx2] - t-amounts2[v-inx2]) /
                  t-amounts1[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts1[v-inx2] - t-amounts2[v-inx2]) /
                  t-amounts1[v-inx2]) * 100 > 999 then 999
        else ((t-amounts1[v-inx2] - t-amounts2[v-inx2]) /
               t-amounts1[v-inx2]) * 100).
    assign t-amounts10[v-inx2] =
       (if t-amounts8[v-inx2] = 0 then 0
        else if ((t-amounts8[v-inx2] - t-amounts9[v-inx2]) /
                  t-amounts8[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts8[v-inx2] - t-amounts9[v-inx2]) /
                  t-amounts8[v-inx2]) * 100 > 999 then 999
        else ((t-amounts8[v-inx2] - t-amounts9[v-inx2]) /
               t-amounts8[v-inx2]) * 100).
               
    assign t-amounts13[v-inx2] =
       (if t-amounts11[v-inx2] = 0 then 0
        else if ((t-amounts11[v-inx2] - t-amounts12[v-inx2]) /
                  t-amounts11[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts11[v-inx2] - t-amounts12[v-inx2]) /
                  t-amounts11[v-inx2]) * 100 > 999 then 999
        else ((t-amounts11[v-inx2] - t-amounts12[v-inx2]) /
               t-amounts11[v-inx2]) * 100).
    assign t-amounts16[v-inx2] =
       (if t-amounts14[v-inx2] = 0 then 0
        else if ((t-amounts14[v-inx2] - t-amounts15[v-inx2]) /
                  t-amounts14[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts14[v-inx2] - t-amounts15[v-inx2]) /
                  t-amounts14[v-inx2]) * 100 > 999 then 999
        else ((t-amounts14[v-inx2] - t-amounts15[v-inx2]) /
               t-amounts14[v-inx2]) * 100).
    assign t-amounts19[v-inx2] =
       (if t-amounts17[v-inx2] = 0 then 0
        else if ((t-amounts17[v-inx2] - t-amounts18[v-inx2]) /
                  t-amounts17[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts17[v-inx2] - t-amounts18[v-inx2]) /
                  t-amounts17[v-inx2]) * 100 > 999 then 999
        else ((t-amounts17[v-inx2] - t-amounts18[v-inx2]) /
               t-amounts17[v-inx2]) * 100).
    assign t-amounts39[v-inx2] =
       (if t-amounts37[v-inx2] = 0 then 0
        else if ((t-amounts37[v-inx2] - t-amounts38[v-inx2]) /
                  t-amounts37[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts37[v-inx2] - t-amounts38[v-inx2]) /
                  t-amounts37[v-inx2]) * 100 > 999 then 999
        else ((t-amounts37[v-inx2] - t-amounts38[v-inx2]) /
               t-amounts37[v-inx2]) * 100).
    assign t-amounts44[v-inx2] =
       (if t-amounts42[v-inx2] = 0 then 0
        else if ((t-amounts42[v-inx2] - t-amounts43[v-inx2]) /
                  t-amounts42[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts42[v-inx2] - t-amounts43[v-inx2]) /
                  t-amounts42[v-inx2]) * 100 > 999 then 999
        else ((t-amounts42[v-inx2] - t-amounts43[v-inx2]) /
               t-amounts42[v-inx2]) * 100).
    
    assign t-amounts22[v-inx2] =
       (if t-amounts7[v-inx2] = 0 then 0
        else if ((t-amounts3[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts3[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 > 999 then 999
        else ((t-amounts3[v-inx2]) /
               t-amounts7[v-inx2]) * 100).
     assign t-amounts23[v-inx2] =
       (if t-amounts7[v-inx2] = 0 then 0
        else if ((t-amounts20[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts20[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 > 999 then 999
        else ((t-amounts20[v-inx2]) /
               t-amounts7[v-inx2]) * 100).
     assign t-amounts24[v-inx2] =
       (if t-amounts7[v-inx2] = 0 then 0
        else if ((t-amounts25[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts25[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 > 999 then 999
        else ((t-amounts25[v-inx2]) /
               t-amounts7[v-inx2]) * 100).
                
     assign t-amounts36[v-inx2] =
       (if t-amounts7[v-inx2] = 0 then 0
        else if ((t-amounts35[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts35[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 > 999 then 999
        else ((t-amounts35[v-inx2]) /
               t-amounts7[v-inx2]) * 100).
     assign t-amounts41[v-inx2] =
       (if t-amounts7[v-inx2] = 0 then 0
        else if ((t-amounts40[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts40[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 > 999 then 999
        else ((t-amounts40[v-inx2]) /
               t-amounts7[v-inx2]) * 100).
     if t-amounts36[v-inx2] <> 0 then
       v-pct4lit = "%".
     else
       v-pct4lit = " ".
     if t-amounts41[v-inx2] <> 0 then
       v-pct5lit = "%".
     else
       v-pct5lit = " ".
    
    if t-amounts22[v-inx2] <> 0 then 
      v-pct1lit  = "%".
    else
      v-pct1lit  = " ".                    
 
    if t-amounts23[v-inx2] <> 0 then 
      v-pct2lit  = "%".
    else
      v-pct2lit  = " ".                    
   
     if t-amounts24[v-inx2] <> 0 then 
      v-pct3lit  = "%".
    else
      v-pct3lit  = " ".                    
  assign v-pct1lit = "%"
         v-pct2lit = "%"
         v-pct3lit = "%".
 
     
/* Line 2 total information Conformity */
    if oOEETdata  then do:
    /* Entered */
      assign wd2-amt1 = t-amounts7[v-inx2]
             wd2-amt2 = t-amounts7[v-inx2] +
                        t-amounts26[v-inx2]. 
                        /* 08/19/10 tah
                        (t-amounts26[v-inx2]  -
                         t-amounts3[v-inx2]  ).
                         */
      assign vd2-amt1 = (if wd2-amt2 = 0 then 0
                         else if (wd2-amt1 / wd2-amt2 ) * 100 < -999 then -999
                         else if (wd2-amt1 / wd2-amt2 ) * 100 >  999 then  999
                         else (wd2-amt1 / wd2-amt2 * 100 ) ).
      assign vd2-amt2 = t-amounts8[v-inx2]
             vd2-amt3 = t-amounts9[v-inx2]
             vd2-amt4 = (if vd2-amt2 = 0 then 0
                         else if ((vd2-amt2 - vd2-amt3) / vd2-amt2 )
                                   * 100 < -999 then -999
                         else if ((vd2-amt2 - vd2-amt3) / vd2-amt2 ) 
                                   * 100 >  999 then  999
                         else ((vd2-amt2 - vd2-amt3) / vd2-amt2 * 100 )).
    /* Converted */
      assign wd2-amt1 = t-amounts3[v-inx2]
             wd2-amt2 = t-amounts26[v-inx2] +
                        t-amounts3[v-inx2]. /* 08/19/10 tah */
      assign vd2-amt5 = (if wd2-amt2 = 0 then 0
                         else if (wd2-amt1 / wd2-amt2 ) * 100 < -999 then -999
                         else if (wd2-amt1 / wd2-amt2 ) * 100 >  999 then  999
                         else (wd2-amt1 / wd2-amt2 * 100) ).
      assign vd2-amt6 = t-amounts11[v-inx2]
             vd2-amt7 = t-amounts12[v-inx2]
             vd2-amt8 = (if vd2-amt6 = 0 then 0
                         else if ((vd2-amt6 - vd2-amt7) / vd2-amt6 )
                                   * 100 < -999 then -999
                         else if ((vd2-amt6 - vd2-amt7) / vd2-amt6 ) 
                                   * 100 >  999 then  999
                         else ((vd2-amt6 - vd2-amt7) / vd2-amt6 * 100) ).
    /* OEET OEEXQ Entered */
      assign wd2-amt1 = t-amounts3[v-inx2]
             wd2-amt2 = t-amounts26[v-inx2].
      assign vd2-amt9 = 0. /*
                        (if wd2-amt2 = 0 then 0
                         else if (wd2-amt1 / wd2-amt2 ) * 100 < -999 then -999
                         else if (wd2-amt1 / wd2-amt2 ) * 100 >  999 then  999
                         else (wd2-amt1 / wd2-amt2 * 100) ).
                           */
                       
      assign vd2-amt10 = t-amounts8[v-inx2] +
                         t-amounts27[v-inx2]
             vd2-amt11 = t-amounts9[v-inx2] +
                         t-amounts28[v-inx2]
             vd2-amt12 = (if vd2-amt10 = 0 then 0
                          else if ((vd2-amt10 - vd2-amt11) / vd2-amt10 )
                                   * 100 < -999 then -999
                          else if ((vd2-amt10 - vd2-amt11) / vd2-amt10 ) 
                                   * 100 >  999 then  999
                          else ((vd2-amt10 - vd2-amt11) / vd2-amt10 * 100) ).
 
 
    /* OEET QU OEEXQ Entered */             
    assign wd2-amt1 = t-amounts34[v-inx2]
           wd2-amt2 = t-amounts26[v-inx2].
    assign vd2-amt13 = (if wd2-amt2 = 0 then 0
                        else if (wd2-amt1 / wd2-amt2 ) * 100 < -999 then -999
                        else if (wd2-amt1 / wd2-amt2 ) * 100 >  999 then  999
                        else (wd2-amt1 / wd2-amt2 * 100 )).
                                               
    assign vd2-amt14 = t-amounts31[v-inx2]
           vd2-amt15 = t-amounts32[v-inx2]
           vd2-amt16 = (if vd2-amt14 = 0 then 0
                       else if ((vd2-amt14 - vd2-amt15) / vd2-amt14 )
                                 * 100 < -999 then -999
                       else if ((vd2-amt14 - vd2-amt15) / vd2-amt14 ) 
                                 * 100 >  999 then  999
                       else ((vd2-amt14 - vd2-amt15) / vd2-amt15 * 100) ).
   
 
     assign vd2-pctlit1 = "%"
            vd2-pctlit2 = "%"
            vd2-pctlit3 = "%".
      
    assign vd2-qty1 = t-amounts7[v-inx2] 
           vd2-qty2 = t-amounts3[v-inx2] 
           vd2-qty3 = t-amounts26[v-inx2] 
                      /* 08/19/10 tah
                      (t-amounts26[v-inx2] -
                       t-amounts3[v-inx2] )   */  +
                      t-amounts7[v-inx2]
           vd2-qty4 = t-amounts34[v-inx2].
    assign vd2-strpct1 = string(vd2-amt1,">>9.99-")
           vd2-strpct2 = string(vd2-amt5,">>9.99-")
           vd2-strpct3 = if vd2-amt9 = 0 then "       "
                         else string(vd2-amt9,">>9.99-")
           vd2-strpct4 = string(vd2-amt13,">>9.99-").
 
    
    end.
                
    if not x-stream then
      do:
      display 
        v-final
        v-astrik
        v-summary-lit2
        t-amounts3[v-inx2]   @ v-amt3
        t-amounts4[v-inx2]   @ v-amt4
        t-amounts6[v-inx2]   @ v-amt6
        t-amounts7[v-inx2]   @ v-amt7
        t-amounts8[v-inx2]   @ v-amt8
        t-amounts9[v-inx2]   @ v-amt9
        t-amounts10[v-inx2]   @ v-amt10
        t-amounts11[v-inx2]   @ v-amt11
        t-amounts12[v-inx2]   @ v-amt12
        t-amounts13[v-inx2]   @ v-amt13
        t-amounts14[v-inx2]   @ v-amt14
        t-amounts15[v-inx2]   @ v-amt15
        t-amounts16[v-inx2]   @ v-amt16
        t-amounts17[v-inx2]   @ v-amt17
        t-amounts18[v-inx2]   @ v-amt18
        t-amounts19[v-inx2]   @ v-amt19
        t-amounts22[v-inx2]   @ v-amt22
        t-amounts23[v-inx2]   @ v-amt23
        t-amounts24[v-inx2]   @ v-amt24
        t-amounts25[v-inx2]   @ v-amt25
        (if oOEETdata  then
           t-amounts26[v-inx2]  
         else 
            0)  @ v-amt26
        /* 08/19/10 tah
           if (t-amounts26[v-inx2] -
               t-amounts3[v-inx2]) ge 0 then
             (t-amounts26[v-inx2] -
              t-amounts3[v-inx2])
           else
             0
         else
           0)  @ v-amt26   */
           
        /*das - OBC*/
        t-amounts35[v-inx2]   @ v-amt35
        t-amounts40[v-inx2]   @ v-amt40
        t-amounts36[v-inx2]   @ v-amt36
        t-amounts37[v-inx2]   @ v-amt37
        t-amounts38[v-inx2]   @ v-amt38
        t-amounts39[v-inx2]   @ v-amt39
        t-amounts41[v-inx2]   @ v-amt41
        t-amounts42[v-inx2]   @ v-amt42
        t-amounts43[v-inx2]   @ v-amt43
        t-amounts44[v-inx2]   @ v-amt44
   
        v-pct1lit  v-pct2lit  v-pct3lit v-pct4lit v-pct5lit
        v-OeetLabel1 v-OeetLabel2 
               
        with frame f-tot.
      down with frame f-tot.
      
      if oOEETdata  then do:
         display 
           vd2-strpct1
           vd2-pctlit1
           vd2-amt2
           vd2-amt3
           vd2-amt4
           vd2-strpct2
           vd2-pctlit2
           vd2-amt6
           vd2-amt7
           vd2-amt8
           vd2-strpct3 
           vd2-pctlit2
           vd2-amt10
           vd2-amt11
           vd2-amt12
           vd2-strpct4 
           vd2-pctlit2
           vd2-amt14
           vd2-amt15
           vd2-amt16
           vd2-qty4
           vd2-qty1
           vd2-qty2
           vd2-qty3
           
        with frame f-tot2.
  
      down with frame f-tot2.
      end.
 
      
      end.
    else
      do:
      display stream xpcd
        v-final
        v-astrik
        v-summary-lit2
        t-amounts3[v-inx2]   @ v-amt3
        t-amounts4[v-inx2]   @ v-amt4
        t-amounts6[v-inx2]   @ v-amt6
        t-amounts7[v-inx2]   @ v-amt7
        t-amounts8[v-inx2]   @ v-amt8
        t-amounts9[v-inx2]   @ v-amt9
        t-amounts10[v-inx2]   @ v-amt10
        t-amounts11[v-inx2]   @ v-amt11
        t-amounts12[v-inx2]   @ v-amt12
        t-amounts13[v-inx2]   @ v-amt13
        t-amounts14[v-inx2]   @ v-amt14
        t-amounts15[v-inx2]   @ v-amt15
        t-amounts16[v-inx2]   @ v-amt16
        t-amounts17[v-inx2]   @ v-amt17
        t-amounts18[v-inx2]   @ v-amt18
        t-amounts19[v-inx2]   @ v-amt19
        t-amounts22[v-inx2]   @ v-amt22
        t-amounts23[v-inx2]   @ v-amt23
        t-amounts24[v-inx2]   @ v-amt24
        t-amounts25[v-inx2]   @ v-amt25
        (if oOEETdata  then
           t-amounts26[v-inx2]
         else
           0)
       /* 08/19/10 tah
          if (t-amounts26[v-inx2] -
              t-amounts3[v-inx2]) ge 0 then
            (t-amounts26[v-inx2] -
             t-amounts3[v-inx2])
          else
            0
         else
           0) */   @ v-amt26
        /*das - OBC*/
        t-amounts35[v-inx2]   @ v-amt35
        t-amounts40[v-inx2]   @ v-amt40
        t-amounts36[v-inx2]   @ v-amt36
        t-amounts37[v-inx2]   @ v-amt37
        t-amounts38[v-inx2]   @ v-amt38
        t-amounts39[v-inx2]   @ v-amt39
        t-amounts41[v-inx2]   @ v-amt41
        t-amounts42[v-inx2]   @ v-amt42
        t-amounts43[v-inx2]   @ v-amt43
        t-amounts44[v-inx2]   @ v-amt44
        
        v-pct1lit  v-pct2lit  v-pct3lit v-pct4lit v-pct5lit
        v-OeetLabel1 v-OeetLabel2 
        with frame f-tot.
      down stream xpcd with frame f-tot.
     
   
      if oOEETdata  then do:
        display  stream xpcd
          vd2-strpct1
          vd2-pctlit1
          vd2-amt2
          vd2-amt3
          vd2-amt4
          vd2-strpct2
          vd2-pctlit2
          vd2-amt6
          vd2-amt7
          vd2-amt8
          vd2-strpct3 
          vd2-pctlit2
          vd2-amt10
          vd2-amt11
          vd2-amt12
          vd2-strpct4 
          vd2-pctlit2
          vd2-amt14
          vd2-amt15
          vd2-amt16
          vd2-qty4
          vd2-qty1
          vd2-qty2
          vd2-qty3
          
        with frame f-tot2.
  
       down   stream xpcd with frame f-tot2.
       end.
      end.
 
&endif
 
&if defined(user_summaryputexport) = 2 &then
 /* Same as print logic above */
    assign t-amounts5[v-inx2] =
       (if t-amounts1[v-inx2] = 0 then 0
        else if ((t-amounts1[v-inx2] - t-amounts2[v-inx2]) /
                  t-amounts1[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts1[v-inx2] - t-amounts2[v-inx2]) /
                  t-amounts1[v-inx2]) * 100 > 999 then 999
        else ((t-amounts1[v-inx2] - t-amounts2[v-inx2]) /
               t-amounts1[v-inx2]) * 100).
    assign t-amounts10[v-inx2] =
       (if t-amounts8[v-inx2] = 0 then 0
        else if ((t-amounts8[v-inx2] - t-amounts9[v-inx2]) /
                  t-amounts8[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts8[v-inx2] - t-amounts9[v-inx2]) /
                  t-amounts8[v-inx2]) * 100 > 999 then 999
        else ((t-amounts8[v-inx2] - t-amounts9[v-inx2]) /
               t-amounts8[v-inx2]) * 100).
               
    assign t-amounts13[v-inx2] =
       (if t-amounts11[v-inx2] = 0 then 0
        else if ((t-amounts11[v-inx2] - t-amounts12[v-inx2]) /
                  t-amounts11[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts11[v-inx2] - t-amounts12[v-inx2]) /
                  t-amounts11[v-inx2]) * 100 > 999 then 999
        else ((t-amounts11[v-inx2] - t-amounts12[v-inx2]) /
               t-amounts11[v-inx2]) * 100).
    assign t-amounts16[v-inx2] =
       (if t-amounts14[v-inx2] = 0 then 0
        else if ((t-amounts14[v-inx2] - t-amounts15[v-inx2]) /
                  t-amounts14[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts14[v-inx2] - t-amounts15[v-inx2]) /
                  t-amounts14[v-inx2]) * 100 > 999 then 999
        else ((t-amounts14[v-inx2] - t-amounts15[v-inx2]) /
               t-amounts14[v-inx2]) * 100).
    assign t-amounts19[v-inx2] =
       (if t-amounts17[v-inx2] = 0 then 0
        else if ((t-amounts17[v-inx2] - t-amounts18[v-inx2]) /
                  t-amounts17[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts17[v-inx2] - t-amounts18[v-inx2]) /
                  t-amounts17[v-inx2]) * 100 > 999 then 999
        else ((t-amounts17[v-inx2] - t-amounts18[v-inx2]) /
               t-amounts17[v-inx2]) * 100).
    assign t-amounts39[v-inx2] =
       (if t-amounts37[v-inx2] = 0 then 0
        else if ((t-amounts37[v-inx2] - t-amounts38[v-inx2]) /
                  t-amounts37[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts37[v-inx2] - t-amounts38[v-inx2]) /
                  t-amounts37[v-inx2]) * 100 > 999 then 999
        else ((t-amounts37[v-inx2] - t-amounts38[v-inx2]) /
               t-amounts37[v-inx2]) * 100).
    assign t-amounts44[v-inx2] =
       (if t-amounts42[v-inx2] = 0 then 0
        else if ((t-amounts42[v-inx2] - t-amounts43[v-inx2]) /
                  t-amounts42[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts42[v-inx2] - t-amounts43[v-inx2]) /
                  t-amounts42[v-inx2]) * 100 > 999 then 999
        else ((t-amounts42[v-inx2] - t-amounts43[v-inx2]) /
               t-amounts42[v-inx2]) * 100).
    
    assign t-amounts22[v-inx2] =
       (if t-amounts7[v-inx2] = 0 then 0
        else if ((t-amounts3[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts3[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 > 999 then 999
        else ((t-amounts3[v-inx2]) /
               t-amounts7[v-inx2]) * 100).
     assign t-amounts23[v-inx2] =
       (if t-amounts7[v-inx2] = 0 then 0
        else if ((t-amounts20[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts20[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 > 999 then 999
        else ((t-amounts20[v-inx2]) /
               t-amounts7[v-inx2]) * 100).
     assign t-amounts24[v-inx2] =
       (if t-amounts7[v-inx2] = 0 then 0
        else if ((t-amounts25[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts25[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 > 999 then 999
        else ((t-amounts25[v-inx2]) /
               t-amounts7[v-inx2]) * 100).
     assign t-amounts36[v-inx2] =
       (if t-amounts7[v-inx2] = 0 then 0
        else if ((t-amounts35[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts35[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 > 999 then 999
        else ((t-amounts35[v-inx2]) /
               t-amounts7[v-inx2]) * 100).
     assign t-amounts41[v-inx2] =
       (if t-amounts7[v-inx2] = 0 then 0
        else if ((t-amounts40[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 < -999 then -999
        else if ((t-amounts40[v-inx2]) /
                  t-amounts7[v-inx2]) * 100 > 999 then 999
        else ((t-amounts40[v-inx2]) /
               t-amounts7[v-inx2]) * 100).
     if t-amounts36[v-inx2] <> 0 then
       v-pct4lit = "%".
     else
       v-pct4lit = " ".
     if t-amounts41[v-inx2] <> 0 then
       v-pct5lit = "%".
     else
       v-pct5lit = " ".
                
    if t-amounts22[v-inx2] <> 0 then 
      v-pct1lit  = "%".
    else
      v-pct1lit  = " ".                    
 
    if t-amounts23[v-inx2] <> 0 then 
      v-pct2lit  = "%".
    else
      v-pct2lit  = " ".                    
   
     if t-amounts24[v-inx2] <> 0 then 
      v-pct3lit  = "%".
    else
      v-pct3lit  = " ".                    
  assign v-pct1lit = "%"
         v-pct2lit = "%"
         v-pct3lit = "%".
 
     
/* Line 2 total information Conformity */
    if oOEETdata  then do:
    /* Entered */
      assign wd2-amt1 = t-amounts7[v-inx2]
             wd2-amt2 = t-amounts7[v-inx2] +
                        t-amounts26[v-inx2] .
                        /* 08/19/10 tah 
                        (t-amounts26[v-inx2]  -
                         t-amounts3[v-inx2]  ). */
      assign vd2-amt1 = (if wd2-amt2 = 0 then 0
                         else if (wd2-amt1 / wd2-amt2 ) * 100 < -999 then -999
                         else if (wd2-amt1 / wd2-amt2 ) * 100 >  999 then  999
                         else (wd2-amt1 / wd2-amt2 * 100 ) ).
      assign vd2-amt2 = t-amounts8[v-inx2]
             vd2-amt3 = t-amounts9[v-inx2]
             vd2-amt4 = (if vd2-amt3 = 0 then 0
                         else if ((vd2-amt2 - vd2-amt3) / vd2-amt2 )
                                   * 100 < -999 then -999
                         else if ((vd2-amt2 - vd2-amt3) / vd2-amt2 ) 
                                   * 100 >  999 then  999
                         else ((vd2-amt2 - vd2-amt3) / vd2-amt2 * 100 )).
    /* Converted */
      assign wd2-amt1 = t-amounts3[v-inx2]
             wd2-amt2 = t-amounts26[v-inx2] +
                        t-amounts3[v-inx2].
             
                        /* 08/19/10 tah
                          t-amounts26[v-inx2].
                        */  
      assign vd2-amt5 = (if wd2-amt2 = 0 then 0
                         else if (wd2-amt1 / wd2-amt2 ) * 100 < -999 then -999
                         else if (wd2-amt1 / wd2-amt2 ) * 100 >  999 then  999
                         else (wd2-amt1 / wd2-amt2 * 100) ).
      assign vd2-amt6 = t-amounts11[v-inx2]
             vd2-amt7 = t-amounts12[v-inx2]
             vd2-amt8 = (if vd2-amt3 = 0 then 0
                         else if ((vd2-amt2 - vd2-amt3) / vd2-amt2 )
                                   * 100 < -999 then -999
                         else if ((vd2-amt2 - vd2-amt3) / vd2-amt2 ) 
                                   * 100 >  999 then  999
                         else ((vd2-amt2 - vd2-amt3) / vd2-amt2 * 100) ).
    /* OEET OEEXQ Entered */
      assign wd2-amt1 = t-amounts3[v-inx2]
             wd2-amt2 = t-amounts26[v-inx2].
      assign vd2-amt9 = 0. /*
                        (if wd2-amt2 = 0 then 0
                         else if (wd2-amt1 / wd2-amt2 ) * 100 < -999 then -999
                         else if (wd2-amt1 / wd2-amt2 ) * 100 >  999 then  999
                         else (wd2-amt1 / wd2-amt2 * 100) ).
                           */
                       
      assign vd2-amt10 = t-amounts8[v-inx2] +
                         t-amounts27[v-inx2]
             vd2-amt11 = t-amounts9[v-inx2] +
                         t-amounts28[v-inx2]
             vd2-amt12 = (if vd2-amt10 = 0 then 0
                          else if ((vd2-amt10 - vd2-amt11) / vd2-amt10 )
                                   * 100 < -999 then -999
                          else if ((vd2-amt10 - vd2-amt11) / vd2-amt10 ) 
                                   * 100 >  999 then  999
                          else ((vd2-amt10 - vd2-amt11) / vd2-amt10 * 100) ).
 
 
    /* OEET QU OEEXQ Entered */             
    assign wd2-amt1 = t-amounts34[v-inx2]
           wd2-amt2 = t-amounts26[v-inx2].
    assign vd2-amt13 = (if wd2-amt2 = 0 then 0
                        else if (wd2-amt1 / wd2-amt2 ) * 100 < -999 then -999
                        else if (wd2-amt1 / wd2-amt2 ) * 100 >  999 then  999
                        else (wd2-amt1 / wd2-amt2 * 100 )).
                                               
    assign vd2-amt14 = t-amounts31[v-inx2]
           vd2-amt15 = t-amounts32[v-inx2]
           vd2-amt16 = (if vd2-amt14 = 0 then 0
                       else if ((vd2-amt14 - vd2-amt15) / vd2-amt14 )
                                 * 100 < -999 then -999
                       else if ((vd2-amt14 - vd2-amt15) / vd2-amt14 ) 
                                 * 100 >  999 then  999
                       else ((vd2-amt14 - vd2-amt15) / vd2-amt15 * 100) ).
   
 
     assign vd2-pctlit1 = "%"
            vd2-pctlit2 = "%"
            vd2-pctlit3 = "%".
      
    assign vd2-qty1 = t-amounts7[v-inx2] 
           vd2-qty2 = t-amounts3[v-inx2] 
           vd2-qty3 = t-amounts26[v-inx2] +
                     /* 08/19/10 tah 
                      (t-amounts26[v-inx2] -
                       t-amounts3[v-inx2] ) +
                     */
                      t-amounts7[v-inx2]
           vd2-qty4 = t-amounts34[v-inx2].
    assign vd2-strpct1 = string(vd2-amt1,">>9.99-")
           vd2-strpct2 = string(vd2-amt5,">>9.99-")
           vd2-strpct3 = if vd2-amt9 = 0 then "       "
                         else string(vd2-amt9,">>9.99-")
           vd2-strpct4 = string(vd2-amt13,">>9.99-").
 
    
    end.
  
    assign export_rec = v-astrik + v-del + export_rec.
    assign export_rec = export_rec + v-del +
      string(t-amounts7[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts6[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts3[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts4[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts25[v-inx2],"->>>>>>>>9").
  if oOEETdata then
    assign export_rec = export_rec + v-del +
      /* 08/19/10 tah
      string( if (t-amounts26[v-inx2] -
                  t-amounts3[v-inx2]) ge 0 then
                (t-amounts26[v-inx2] -
                 t-amounts3[v-inx2]) 
              else
                0,"->>>>>>>>9").
      */
       string(t-amounts26[v-inx2],"->>>>>>>>9").
  assign export_rec = export_rec + v-del +
      string(t-amounts8[v-inx2],"->>>>>>>>9.99") + v-del +
      string(t-amounts9[v-inx2],"->>>>>>>>9.99") + v-del +
      string(t-amounts10[v-inx2],"->>>>>>>>9.99") + v-del +
      string(t-amounts22[v-inx2],"->>>>>>>>9.99") + v-del +
      string(t-amounts11[v-inx2],"->>>>>>>>9.99") + v-del +
      string(t-amounts12[v-inx2],"->>>>>>>>9.99") + v-del +
      string(t-amounts13[v-inx2],"->>>>>>>>9.99") + v-del +
      string(t-amounts23[v-inx2],"->>>>>>>>9.99") + v-del +
      string(t-amounts14[v-inx2],"->>>>>>>>9.99") + v-del +
      string(t-amounts15[v-inx2],"->>>>>>>>9.99") + v-del +
      string(t-amounts16[v-inx2],"->>>>>>>>9.99") + v-del +
      string(t-amounts24[v-inx2],"->>>>>>>>9.99") + v-del +
      string(t-amounts17[v-inx2],"->>>>>>>>9.99") + v-del +
      string(t-amounts18[v-inx2],"->>>>>>>>9.99") + v-del +
      string(t-amounts19[v-inx2],"->>>>>>>>9.99").
  if oOEETdata then
    assign export_rec = export_rec + v-del +
      string(vd2-qty1,"->>>>>>>>9.99") + v-del +
      vd2-strpct1 + v-del +
      string(vd2-amt2,"->>>>>>>>9.99") + v-del +
      string(vd2-amt3,"->>>>>>>>9.99") + v-del +
      string(vd2-amt4,"->>>>>>>>9.99") + v-del +
 
      string(vd2-qty2,"->>>>>>>>9.99") + v-del +
      vd2-strpct2 + v-del +
      string(vd2-amt6,"->>>>>>>>9.99") + v-del +
      string(vd2-amt7,"->>>>>>>>9.99") + v-del +
      string(vd2-amt8,"->>>>>>>>9.99") + v-del +
 
      string(vd2-qty3,"->>>>>>>>9.99") + v-del +
      vd2-strpct3 + v-del +
      string(vd2-amt10,"->>>>>>>>9.99") + v-del +
      string(vd2-amt11,"->>>>>>>>9.99") + v-del +
      string(vd2-amt12,"->>>>>>>>9.99") + v-del +
 
      string(vd2-qty4,"->>>>>>>>9.99") + v-del +
      vd2-strpct4 + v-del +
      string(vd2-amt14,"->>>>>>>>9.99") + v-del +
      string(vd2-amt15,"->>>>>>>>9.99") + v-del +
      string(vd2-amt16,"->>>>>>>>9.99").
      
 
&endif
 
&if defined(user_detailputexport) = 2 &then
 
&endif
 
&if defined(user_procedures) = 2 &then
procedure sapb_vars:
/* set Ranges */
rQuote[1]       = string(decimal(sapb.rangebeg[1]),">>>>>>>9").
rQuote[2]       = if dec(sapb.rangeend[1]) < 100000000 then
                    trim(string(decimal(sapb.rangeend[1]),">>>>>>>9"))
                  else
                    "99999999".
rCust[1]        = decimal(sapb.rangebeg[2]). 
rCust[2]        = decimal(sapb.rangeend[2]).
rShipTo[1]      = sapb.rangebeg[3].
rShipTo[2]      = sapb.rangeend[3].
rVend[1]        = decimal(sapb.rangebeg[4]).
rVend[2]        = decimal(sapb.rangeend[4]).
rVendId[1]      = sapb.rangebeg[5].
rVendId[2]      = sapb.rangeend[5].
rRegion[1]      = substring(sapb.rangebeg[6],1,1).
rRegion[2]      = substring(sapb.rangeend[6],1,1).
rDistrict[1]    = substring(sapb.rangebeg[6],2,3).
rDistrict[2]    = substring(sapb.rangeend[6],2,3).
rProd[1]        = sapb.rangebeg[7].
rProd[2]        = sapb.rangeend[7].
/*
rEnterDt[1]     = date(sapb.rangebeg[8]).
rEnterDt[2]     = date(sapb.rangeend[8]).
*/
rStage[1]       = int(substring(sapb.rangebeg[9],1,2)).
rStage[2]       = int(substring(sapb.rangeend[9],1,2)).
rSlsIn[1]       = sapb.rangebeg[10].
rSlsIn[2]       = sapb.rangeend[10].
rSlsOut[1]      = sapb.rangebeg[11].
rSlsOut[2]      = sapb.rangeend[11].
rTaken[1]       = sapb.rangebeg[12].
rTaken[2]       = sapb.rangeend[12].
/*
rCancelDt[1]    = date(sapb.rangebeg[13]).
rCancelDt[2]    = date(sapb.rangeend[13]).
*/
rLostBus[1]     = sapb.rangebeg[14].
rLostBus[2]     = sapb.rangeend[14].
/** FIX001 - TA  **/
/** entered date **/
if sapb.rangebeg[8] ne "" then do: 
 v-datein = sapb.rangebeg[8].
 {p-rptdt.i}
 if string(v-dateout) = v-lowdt then
  rEnterDt[1] = 01/01/1900.
 else  
  rEnterDt[1] = v-dateout.
end.
else
  rEnterDt[1] = 01/01/1900.
if sapb.rangeend[8] ne "" then do: 
 v-datein = sapb.rangeend[8].
 {p-rptdt.i}
 if string(v-dateout) = v-lowdt then
  rEnterDt[2] = 12/31/2049.
 else  
  rEnterDt[2] = v-dateout.
end.
else
  rEnterDt[2] = 12/31/2049.
/** cancel date **/
if sapb.rangebeg[13] ne "" then do: 
 v-datein = sapb.rangebeg[13].
 {p-rptdt.i}
 if string(v-dateout) = v-lowdt then
  rCancelDt[1] = 01/01/1900.
 else  
  rCancelDt[1] = v-dateout.
end.
else
  rCancelDt[1] = 01/01/1900.
if sapb.rangeend[13] ne "" then do: 
 v-datein = sapb.rangeend[13].
 {p-rptdt.i}
 if string(v-dateout) = v-lowdt then
  rCancelDt[2] = 12/31/2049.
 else  
  rCancelDt[2] = v-dateout.
end.
else
  rCancelDt[2] = 12/31/2049.
/** FIX001 - TA  **/  
if rCancelDt[1] = 12/31/49 and rCancelDt[2] = 12/31/49 then do:
  unasgndtFlag = yes.
  rCancelDt[1] = 01/01/1950.
end.  
else unasgndtFlag = no.
/* set Options */
oSort      = sapb.optvalue[1].    
oDetail    = sapb.optvalue[2].
oList      = if sapb.optvalue[3] = "yes" then yes else no. 
oNotes     = sapb.optvalue[4].    
oConv      = if sapb.optvalue[5] = "yes" then yes else no. 
oCancelled = if sapb.optvalue[6] = "yes" then yes else no. 
oOpenStage = if sapb.optvalue[7] = "yes" then yes else no. 
p-detail = /* if oDetail = "s" then
             "s"
           else */
             "d". 
oLostBusiness = if sapb.optvalue[8] = "yes" then yes else no. 
oOEETdata = if sapb.optvalue[9] = "yes" then yes else no. 
oExportName = sapb.optvalue[10].
oOBC = if sapb.optvalue[11] = "yes" then yes else no.
assign p-export = oexportName.
if p-export <> "" then
  assign oDetail = "S"
         p-detail = "S". /* export function is only valid Summary */ 
lenSort = length(oSort).
if oList then do:
  /* stuff for zsapboycheck.i */
  /* do I have to set zel_ too? */
  assign 
  zzl_begcust = rCust[1]
  zzl_endcust = rCust[2]
  zzl_begvend = rVend[1]
  zzl_endvend = rVend[2]
  zzl_begrep  = rSlsOut[1]
  zzl_endrep  = rSlsOut[2] 
  zzl_begcat  = rProd[1]
  zzl_endcat  = rProd[2]
  zzl_begvname  = rVendId[1]
  zzl_endvname  = rVendId[2]
  zzl_begshipto = rShipTo[1]
  zzl_endshipto = rShipTo[2].
  assign                     
  zel_begcust = rCust[1]     
  zel_endcust = rCust[2]     
  zel_begvend = rVend[1]     
  zel_endvend = rVend[2]     
  zel_begrep  = rSlsOut[1]   
  zel_endrep  = rSlsOut[2]   
  zel_begcat  = rProd[1]     
  zel_endcat  = rProd[2]     
  zel_begvname  = rVendId[1] 
  zel_endvname  = rVendId[2] 
  zel_begshipto = rShipTo[1] 
  zel_endshipto = rShipTo[2]
  zel_beglbus   = rlostbus[1] 
  zel_endlbus  = rlostbus[2] 
  zel_begtakenby = rtaken[1] 
  zel_endtakenby = rtaken[2].
  
  {zsapboyload.i}                                 
  assign                             
   rCust[1]    = zel_begcust
   rCust[2]    = zel_endcust
   rVend[1]    = zel_begvend
   rVend[2]    = zel_endvend
   rSlsOut[1]  = zel_begrep
   rSlsOut[2]  = zel_endrep
   rProd[1]    = zel_begcat
   rProd[2]    = zel_endcat
   rVendId[1]   = zel_begvname
   rVendId[2]   = zel_endvname
   rShipTo[1]   = zel_begshipto
   rShipTo[2]   = zel_endshipto
   rlostbus[1]  = zel_beglbus  
   rlostbus[2]  = zel_endlbus   
   rtaken[1]  = zel_begtakenby 
   rtaken[2]  = zel_endtakenby.
/*   
  /* set zelection switches */                    
  assign    sw-s = no    sw-b = no    sw-c = no    sw-p = no    sw-t = no. 
  for each zsapbo no-lock:                                    
    case zsapbo.selection_type:                 
      when "s" then assign sw-s = yes.        
      when "b" then assign sw-b = yes.        
      when "c" then assign sw-c = yes.        
      when "p" then assign sw-p = yes.        
      when "t" then assign sw-t = yes.        
    end case. /*case zspabo.selection_type */   
  end. /* for each zsapbo */                      
*/
end. /* if oList */  
    
  run formatoptions.
end.
/* ------------------------------------------------------------------------- */
procedure formatoptions:
/* ------------------------------------------------------------------------- */
   
/** FIX001 - TA  **/  
  assign p-portrait = false.
  /* if false then the report will print in LANDSCAPE 178 character */
  
  if sapb.optvalue[1] > "0" and sapb.optvalue[1] ne "99" then 
    do:
    p-optiontype = " ".
    find notes where notes.cono = g-cono and
                     notes.notestype = "zz" and
                     notes.primarykey = "oerzq" and  /* tbxr program */
                     notes.secondarykey = 
                     sapb.optvalue[1]  no-lock no-error.
    if not avail notes then
      do:
      display "Format is not valid cannot process request".
      assign p-optiontype = "c"
             p-sorttype   = ">,"
             p-totaltype  = "S"
             p-summcounts = "a".
      end.               
    else do:
      assign p-optiontype = notes.noteln[1]
             p-sorttype   = notes.noteln[2]
             p-totaltype  = notes.noteln[3]
             p-summcounts = notes.noteln[4]
             p-export     = "    "
             p-register   = notes.noteln[5]
             p-registerex = notes.noteln[6].
      end.
    end.     
  else
  if sapb.optvalue[1] = "99" then 
    do:
    assign p-register   = ""
           p-registerex = "".
    run reportopts(input sapb.user5, 
                   input-output p-optiontype,
                   input-output p-sorttype,  
                   input-output p-totaltype,
                   input-output p-summcounts,
                   input-output p-register,
                   input-output p-regval).
    end.
 if p-optiontype = "" then
   assign p-optiontype = "c"
          p-sorttype   = ">,"
          p-totaltype  = "S"
          p-summcounts = "a".
 if index(p-optiontype,"R") <> 0 then
   assign v-slsrepoutsort = true. 
 else
 if index(p-optiontype,"D") <> 0 then
   assign v-slsrepoutsort = true. 
 else
 if index(p-optiontype,"O") <> 0 then
   assign v-slsrepoutsort = true. 
 else
   assign v-slsrepoutsort = false. 
 if index(p-optiontype,"I") <> 0 then
   assign v-slsrepinsort = true. 
 else
   assign v-slsrepinsort = false. 
 if index(p-optiontype,"L") <> 0 then
   assign v-lostbusiness = true. 
 else
   assign v-lostbusiness = false. 
   
 run Print_reportopts (input recid (sapb),
                       input g-cono).
 end.
   
procedure extract_data:
assign sumPrice     = 0
       sumCost      = 0
       OBCsumPrice  = 0
       OBCsumCost   = 0
       NOBCsumPrice = 0
       NOBCsumCost  = 0.
if oOpenStage then
  assign rStage[1] = 0
         rStage[2] = 1.
/* Main, select 1 ranges */
for each oeehb where                                       
  oeehb.cono = g-cono and
  oeehb.batchnm >=  string(dec(rQuote[1]),">>>>>>>9") and
  oeehb.batchnm <= string(dec(rQuote[2]),">>>>>>>9") and
  oeehb.custno  >= rCust[1]  and oeehb.custno  <= rCust[2]  and
  oeehb.shipto  >= rShipTo[1] and oeehb.shipto <= rShipTo[2] and  
  oeehb.enterDt >= rEnterDt[1] and oeehb.enterDt <= rEnterDt[2] and
  oeehb.stagecd >= rStage[1] and oeehb.stagecd <= rStage[2] and
  oeehb.takenby >= rTaken[1] and oeehb.takenby <= rTaken[2] and
 ((oeehb.canceldt >= rCancelDt[1] and oeehb.canceldt <= rCancelDt[2]
   and not unasgndtFlag) or
 (unasgndtFlag and 
   (oeehb.canceldt >= rCancelDt[1] and oeehb.canceldt <= rCancelDt[2])
    or oeehb.canceldt = ?))
   and oeehb.sourcepros = "Quote" and
      oeehb.seqno = 1
no-lock:
assign cntConvert = 0
       cntCancel  = 0
       cntOrder   = 0
       cntLost    = 0
       cntOpen    = 0
       cntOpen2   = 0
       cntLine    = 0
       cntOBCquote = 0
       cntNOBCquote = 0.
  /* Converted orders */
/* if oConv = "N" and oeehb.orderno > 0 then next. */
  if oOBC = yes and oeehb.xxl1 = no then next.
  if oConv and oeehb.orderno = 0 then next.
  /* Cancelled only */
  if oCancelled then if oeehb.stagecd ne 9 or oeehb.orderno > 0 then next.
  if oList then do:
    zelection_type = "c".              
    zelection_char4 = "". 
    zelection_cust = oeehb.custno.                
    run zelectioncheck.             
    if not zelection_good then next.
  end.
  if oList then do:
    zelection_type = "k".              
    zelection_char4 = oeehb.takenby. 
    zelection_cust = 0.                
    run zelectioncheck.             
    if not zelection_good then next.
  end.
   
  find first arsc where arsc.cono = g-cono and arsc.custno = oeehb.custno
  no-lock no-error.
  if avail arsc then dataCustName = arsc.name.
  else dataCustName = "".
  release arsc.
  if not oCancelled then do:
    find first oeelb where oeelb.cono = g-cono and oeelb.batchnm = oeehb.batchnm
    and oeelb.prodcat >= rProd[1] and oeelb.prodcat <= rProd[2] and          
    oeelb.vendno >= rVend[1] and oeelb.vendno <= rVend[2] and            
    oeelb.reasunavty >= rLostBus[1] and oeelb.reasunavty <= rLostBus[2]  
    and
    oeelb.reasunavty >= (if oLostBusiness then " !" else oeelb.reasunavty) 
    and
    oeelb.reasunavty <= (if oLostBusiness then "}}" else oeelb.reasunavty) 
    and
    oeelb.reasunavty <> (if oLostBusiness then "CQ" else "}}") 
    and
    oeelb.reasunavty <> (if oLostBusiness then "CR" else "}}") 
    no-lock no-error.                                              
    if not avail oeelb then  next.
  end.
  assign h-transtype = "QU".
  if oeehb.xxl1 = yes then
    assign cntOBCquote = cntOBCquote + 1.
  else
    assign cntNOBCquote = cntNOBCquote + 1.
  
  if oeehb.orderno > 0 then do:
/*****das
/* tah jen     cntConvert = cntConvert + 1.   */
     cntConvert = 0.
*****/
     cntConvert = /*if oOEETdata = yes then 0 else*/ cntConvert + 1.  /*das*/
     find first oeeh where oeeh.cono = g-cono and
                           oeeh.orderno = oeehb.orderno no-lock no-error.
     if avail oeeh then 
       assign h-transtype = oeeh.transtype.
  
  
  end.                         
  else
  if oeehb.stagecd = 9 and oeehb.orderno = 0 then
    assign cntCancel = cntCancel + 1
           cntLost   = cntLost + 1.
  else
    cntOpen   = cntOpen + 1.
 if oeehb.stagecd <> 9  then
   cntOpen2   = cntOpen2 + 1.
  
  
 assign sumPrice = 0
        sumCost  = 0
        OBCsumPrice  = 0
        OBCsumCost   = 0
        NOBCsumPrice = 0
        NOBCsumCost  = 0.
  for each oeelb where 
    oeelb.cono = g-cono and
    oeelb.batchnm = oeehb.batchnm and
    oeelb.prodcat >= rProd[1] and oeelb.prodcat <= rProd[2] and
/*    oeelb.vendno >= rVend[1] and oeelb.vendno <= rVend[2] and */
    oeelb.reasunavty >= rLostBus[1] and oeelb.reasunavty <= rLostBus[2]
        and
    oeelb.reasunavty >= (if oLostBusiness then " !" else oeelb.reasunavty) 
    and
    oeelb.reasunavty <= (if oLostBusiness then "}}" else oeelb.reasunavty)
    and
    oeelb.reasunavty <> (if oLostBusiness then "CQ" else "}}") 
    and
    oeelb.reasunavty <> (if oLostBusiness then "CR" else "}}") 
  no-lock:
  
    if oList then do:                   
      zelection_type = "p".             
      zelection_char4 = oeelb.prodcat.
      zelection_cust = 0.               
      run zelectioncheck.               
      if not zelection_good then next.  
    end. 
      
    if oList then do:                   
      zelection_type = "l".             
      zelection_char4 = oeelb.reasunavty.
      zelection_cust = 0.               
      run zelectioncheck.               
      if not zelection_good then next.  
    end. 
    /** FIX001 - TA  **/  
    if oeelb.slsrepout ne "" then 
     assign salescode = oeelb.slsrepout. 
    else 
     assign salescode = oeehb.slsrepout.
    if oeelb.slsrepin ne "" then 
     assign insidecode = oeelb.slsrepin. 
    else 
     assign insidecode = oeehb.slsrepin.
    find first smsn where smsn.cono = g-cono and
               smsn.slsrep = salescode no-lock no-error.
     if avail smsn then do:
      dataRegion   =  substring(smsn.mgr,1,1).
      dataDistrict =  substring(smsn.mgr,2,3).
      release smsn.
     end.
     else do:
      dataRegion = "".
      dataDistrict = "".
     end.
    if (dataRegion < rRegion[1] or dataRegion > rRegion[2]) then  
     next.
    if (dataDistrict < rDistrict[1] or dataDistrict > rDistrict[2]) then 
     next.
    if (salescode < rSlsOut[1] or  salescode > rSlsOut[2]) or
       (insidecode < rSlsin[1] or insidecode > rSlsin[2]) then
      next.   
   
    if oList then do:
     zelection_type = "s".
     zelection_char4 = salescode.
     zelection_cust = 0.
     run zelectioncheck.
     if not zelection_good then next.
    end. 
    
    assign w-vendno     = 0
           v-vendorid   =  " "
           v-technology =  " "
           v-parentcode =  " ".
    
    find icsp where icsp.cono = g-cono and 
                    icsp.prod = oeelb.shipprod no-lock no-error.
    find icsw where icsw.cono = g-cono and 
                    icsw.whse = substring(oeelb.user4,1,4) and 
                    icsw.prod = oeelb.shipprod no-lock no-error.
    assign w-vendno = (if avail icsw then
                        icsw.arpvendno
                       else
                        oeelb.arpvendno).
     
    if avail icsp then do: 
      find catmaster where catmaster.cono = 1 and 
           catmaster.notestype    = "zz" and
           catmaster.primarykey   = "apsva" and
           catmaster.secondarykey = icsp.prodcat
      no-lock no-error. 
      if not avail catmaster then do:
       v-parentcode = "".
       v-vendorid = substring(icsp.prodcat,1,3).
      end.
      else
       assign v-vendorid   = catmaster.noteln[1].
      end.
    if (v-vendorid < rVendid[1] or v-vendorid > rVendid[2]) or
       (w-vendno < rVend[1] or w-vendno > rVend[2]) then  
       next.
    if oList then do: 
     zelection_type = "n".
     zelection_char4 = v-vendorid.
     zelection_cust = 0.
     run zelectioncheck.
     if zelection_good = false then
       next.
     assign zelection_type = "v"
            zelection_vend = w-vendno
            zelection_char4 = " ".
     run zelectioncheck.
     if zelection_good = false then
      next.
    end. 
    /** FIX001 - TA  **/  
    assign cntLine    = cntLine + 1.
    create repTable.                 
    repTable.slsrepout = salescode.
    repTable.slsrepin  = oeelb.slsrepin.
    repTable.enterdt   = oeehb.enterdt.
    repTable.canceldt  = oeehb.canceldt.
    repTable.custno    = oeehb.custno.
    
    repTable.oeehbid = recid(oeehb). 
    repTable.batchnm = string(int(oeehb.batchnm),">>>>>>>9") no-error.
    if error-status:error then message "error batchnm not numeric".
    repTable.oeelbid = recid(oeelb).
    repTable.salescd = salescode.
    repTable.region  = dataRegion.
    repTable.district = dataRegion + dataDistrict.
    repTable.custname = dataCustName.
    repTable.cntConvert = 0.
    repTable.cntLost    = 0.
    repTable.cntOpen    = 0.
    repTable.cntOpen2   = 0.
    repTable.cntCancel  = 0.
    repTable.cntOrder   = 0.
    repTable.cntLine    = 0.
    repTable.cntOBCquote = 0.
    repTable.cntNOBCquote = 0.
    repTable.lostbusinesscd = if oeelb.specnstype = "l" then 
                                oeelb.reasunavty
                              else 
                                "  ".             
    repTable.lostbusinessst = if v-lostbusiness then
                                if oeelb.specnstype = "l" then 
                                  oeelb.reasunavty             
                                else
                                  "  "
                              else
                                  " ".
    repTable.slsrepinst = if v-slsrepinsort then 
                             oeelb.slsrepin
                          else
                               "     ".                                            repTable.slsrepoutst = if v-slsrepoutsort then 
                             salescode
                          else
                              "    ".   
     
    
    /** FIX001 - TA  **/  
    repTable.specnstype = if oeelb.specnstype = "l" then 
                        "l" 
                       else
                        " ".           
    /** FIX001 - TA  **/  
    
    sumPrice = if oeelb.specnstype = "l" and oeehb.stagecd <> 9 then 
                0
               else  
                 oeelb.price * oeelb.qtyord. 
    sumCost  = if oeelb.specnstype = "l" and oeehb.stagecd <> 9 then 
                0
               else  
                oeelb.prodcost * oeelb.qtyord.
    if oeehb.xxl1 = yes then
      assign OBCsumPrice = if oeelb.specnstype = "l" and
                              oeehb.stagecd <> 9 then
                             0
                           else
                              oeelb.price * oeelb.qtyord
             OBCsumCost  = if oeelb.specnstype = "l" and 
                              oeehb.stagecd <> 9 then
                             0
                           else
                              oeelb.prodcost * oeelb.qtyord.
    else
      assign NOBCsumPrice = if oeelb.specnstype = "l" and
                              oeehb.stagecd <> 9 then
                              0
                            else
                              oeelb.price * oeelb.qtyord
             NOBCsumCost  = if oeelb.specnstype = "l" and 
                              oeehb.stagecd <> 9 then
                              0
                            else
                              oeelb.prodcost * oeelb.qtyord.
    
    
    HPrice = oeelb.price * oeelb.qtyord. 
    HCost  = oeelb.prodcost * oeelb.qtyord.  
     find b-repTable use-index k-repTable2
                where b-repTable.batchnm = string(int(oeehb.batchnm),">>>>>>>9")
                  and b-repTable.slsrepinst = (if v-slsrepinsort then 
                                                 oeelb.slsrepin
                                               else
                                                 "    ")
                  and b-repTable.slsrepoutst = (if v-slsrepoutsort then 
                                                  salescode
                                                else
                                                  "    ") 
                  and b-repTable.lostbusinessst = 
                                              (if v-lostbusiness then 
                                                 if oeelb.specnstype = "l" then 
                                                   oeelb.reasunavty
                                                 else
                                                    "  "
                                               else
                                                  "  ") 
                  and b-repTable.oeelbid     = 0                                        no-lock no-error.                                              
    if avail b-repTable then
      do:
      assign b-repTable.cntLine  = b-repTable.cntLine + 1
             b-repTable.sumPrice = b-repTable.sumPrice + sumPrice           
             b-repTable.sumCost  = b-repTable.sumCost + sumCost
      
             b-repTable.QsumPrice   = b-repTable.QsumPrice + hPrice           
             b-repTable.QsumCost    = b-repTable.QsumCost + hCost
             b-repTable.CvtsumPrice = b-repTable.CvtsumPrice +
                                      (if oeehb.orderno > 0 and
                                         (oeelb.reasunavty = " "  or
                                          oeelb.reasunavty = "CQ" or
                                          oeelb.reasunavty = "CR") then
                                         hPrice
                                       else
                                        0)
             b-repTable.CvtsumCost  = b-repTable.CvtsumCost +
                                     (if oeehb.orderno > 0 and
                                         (oeelb.reasunavty = " "  or
                                          oeelb.reasunavty = "CQ" or
                                          oeelb.reasunavty = "CR") then
                                         hCost
                                       else
                                         0)
             b-repTable.OpnSumPrice = b-repTable.OpnSumPrice +
                                      (if oeehb.stagecd <> 9 and
                                         (oeelb.reasunavty = " "  or
                                          oeelb.reasunavty = "CQ" or
                                          oeelb.reasunavty = "CR") then
                                         hPrice
                                       else
                                        0)           
             b-repTable.OpnSumCost  = b-repTable.OpnSumCost +
                                     (if oeehb.stagecd <> 9 and
                                         (oeelb.reasunavty = " "  or
                                          oeelb.reasunavty = "CQ" or
                                          oeelb.reasunavty = "CR") then
                                         hCost
                                       else
                                         0)           
                                         
             b-repTable.CansumPrice = b-repTable.CansumPrice +
                                      (if (oeelb.reasunavty <> " "  and
                                           oeelb.reasunavty <> "CQ" and
                                           oeelb.reasunavty <> "CR") then
                                         hPrice
                                       else
                                         0)           
             b-repTable.CansumCost  = b-repTable.CansumCost +
                                      (if (oeelb.reasunavty <> " "  and
                                           oeelb.reasunavty <> "CQ" and
                                           oeelb.reasunavty <> "CR") then
                                         hCost
                                       else
                                          0)
             b-repTable.OBCsumPrice = b-repTable.OBCsumPrice  + OBCsumPrice
             b-repTable.OBCsumCost  = b-repTable.OBCsumCost   + OBCsumCost
             b-repTable.NOBCsumPrice = b-repTable.NOBCsumPrice + NOBCsumPrice
             b-repTable.NOBCsumCost  = b-repTable.NOBCsumCost  + NOBCsumCost.
      end.
    else do:
    
      assign cntOrder   = 1.
  /* Header with totals */
      create b-repTable.                 
      b-repTable.batchnm = string(int(oeehb.batchnm),">>>>>>>9") no-error.
      if error-status:error then message "error, batchnm not integer".
       
      b-repTable.oeehbid   = recid(oeehb). 
      b-repTable.oeelbid   = 0.            
      b-repTable.transtype = h-transtype.
      b-repTable.salescd   = salescode.    
      b-repTable.region    = dataRegion.   
      b-repTable.district  = dataRegion + dataDistrict.
      b-repTable.custname  = dataCustName.
      b-repTable.slsrepout = salescode.
      b-repTable.takenby   = oeehb.takenby.
      b-repTable.slsrepinst = if v-slsrepinsort then 
                              oeelb.slsrepin
                            else
                               " ".                                            
      b-repTable.slsrepoutst = if v-slsrepoutsort then 
                               salescode
                             else
                               " ".   
      
      b-repTable.lostbusinesscd = if oeelb.specnstype = "l" then 
                                    oeelb.reasunavty
                                  else
                                     "  ".             
      b-repTable.lostbusinessst = if v-lostbusiness then
                                    if oeelb.specnstype = "l" then 
                                      oeelb.reasunavty             
                                    else
                                       " "
                                  else
                                     "  ".
       
      
      b-repTable.slsrepin   = oeelb.slsrepin.
      b-repTable.enterdt    = oeehb.enterdt.
      b-repTable.canceldt   = oeehb.canceldt.
      b-repTable.custno     = oeehb.custno.
      b-repTable.cntConvert = cntConvert.
      b-repTable.cntCancel  = cntCancel.
      b-repTable.cntOrder   = CntOrder.
      b-repTable.cntLost    = cntLost.
      b-repTable.cntOpen    = cntOpen.
      b-repTable.cntOpen2   = cntOpen2.
      b-repTable.cntLine    = 1.
      b-repTable.sumPrice   = sumPrice.           
      b-repTable.sumCost    = sumCost.
      b-repTable.QsumPrice   = hPrice.           
      b-repTable.QsumCost    = hCost.
      b-repTable.cntOBCquote  = cntOBCquote.
      b-reptable.OBCsumPrice  = OBCsumPrice.
      b-reptable.OBCSumCost   = OBCSumCost.
      b-repTable.cntNOBCquote = cntNOBCquote.
      b-reptable.NOBCsumPrice = NOBCsumPrice.
      b-reptable.NOBCSumCost  = NOBCSumCost.
  
      b-repTable.CvtsumPrice = if oeehb.orderno > 0 and
                                 (oeelb.reasunavty = " "  or
                                  oeelb.reasunavty = "CQ" or
                                  oeelb.reasunavty = "CR") then
                                 hPrice
                               else
                                 0.
      b-repTable.CvtsumCost  = if oeehb.orderno > 0 and
                                 (oeelb.reasunavty = " "  or
                                  oeelb.reasunavty = "CQ" or
                                  oeelb.reasunavty = "CR") then
                                 hCost
                               else
                                 0.
      b-repTable.OpnSumPrice = if oeehb.stagecd <> 9 and
                                 (oeelb.reasunavty = " "  or
                                  oeelb.reasunavty = "CQ" or
                                  oeelb.reasunavty = "CR") then
                                 hPrice
                               else
                                 0.           
      b-repTable.OpnSumCost  = if oeehb.stagecd <> 9 and
                                 (oeelb.reasunavty = " "  or
                                  oeelb.reasunavty = "CQ" or
                                  oeelb.reasunavty = "CR") then
                                 hCost
                               else
                                 0.           
      
      b-repTable.CansumPrice = if (oeelb.reasunavty <> " "  and
                                   oeelb.reasunavty <> "CQ" and
                                   oeelb.reasunavty <> "CR") then
                                 hPrice
                               else
                                 0.           
      b-repTable.CansumCost     = if (oeelb.reasunavty <> " "  and
                                      oeelb.reasunavty <> "CQ" and
                                      oeelb.reasunavty <> "CR") then
                                 hCost
                               else
                                 0.
      assign b-repTable.OBCsumPrice  =  OBCsumPrice
             b-repTable.OBCsumCost   =  OBCsumCost
             b-repTable.NOBCsumPrice =  NOBCsumPrice
             b-repTable.NOBCsumCost  =  NOBCsumCost.
    end.
  end. /* oeelb */   
end. /* oeehb pass 1 */  
end. /* procedure extract_data */
   
procedure extractOrders_data:
define buffer eb-oeehb for oeehb.
sumPrice = 0.
sumCost  = 0.
if oOpenStage then
  assign rStage[1] = 0
         rStage[2] = 1.
/* Main, select 1 ranges */
for each oeeh where                                       
  oeeh.cono = g-cono and
/*
  oeeh.orderno >=  string(dec(rQuote[1]),">>>>>>>9") 
  and oeeh.orderno <= string(dec(rQuote[2]),">>>>>>>9") and
*/
  oeeh.ordersuf = 0 and
  oeeh.custno  >= rCust[1]  and oeeh.custno  <= rCust[2]  and
  oeeh.shipto  >= rShipTo[1] and oeeh.shipto <= rShipTo[2] and  
  oeeh.enterDt >= rEnterDt[1] and oeeh.enterDt <= rEnterDt[2] and
  oeeh.takenby >= rTaken[1] and oeeh.takenby <= rTaken[2] and
 ((oeeh.canceldt >= rCancelDt[1] and oeeh.canceldt <= rCancelDt[2]
   and not unasgndtFlag) or
 (unasgndtFlag and 
 (oeeh.canceldt >= rCancelDt[1] and oeeh.canceldt <= rCancelDt[2])
    or oeeh.canceldt = ?))
  no-lock:
if can-do("CR,RM,BL,BR",oeeh.transtype) or oeeh.stagecd = 9 then
  next.
  
find eb-oeehb where
     eb-oeehb.cono        = g-cono and
     eb-oeehb.batchnm     = substr(oeeh.user3,53,8) and
     eb-oeehb.seqno       = 1 no-lock no-error.
if avail eb-oeehb and oOBC = yes and eb-oeehb.xxl1 = no then next.
assign cntConvert = 0
       cntCancel  = 0
       cntOrder   = 0
       cntLost    = 0
       cntOpen    = 0
       cntOpen2   = 0
       cntOrderOEET    = 0
       cntLine    = 0.
  /* Converted orders */
  
  if oList then do:
    zelection_type = "c".              
    zelection_char4 = "". 
    zelection_cust = oeeh.custno.                
    run zelectioncheck.             
    if not zelection_good then next.
  end.
  
  if oList then do:
    zelection_type = "k".              
    zelection_char4 = oeeh.takenby. 
    zelection_cust = 0.                
    run zelectioncheck.             
    if not zelection_good then next.
  end.
 
  find first arsc where arsc.cono = g-cono and arsc.custno = oeeh.custno
  no-lock no-error.
  if avail arsc then dataCustName = arsc.name.
  else dataCustName = "".
  release arsc.
 
  if not oCancelled then do:
    find first oeel where 
         oeel.cono = g-cono and 
         oeel.orderno = oeeh.orderno   and
         oeel.ordersuf = oeeh.ordersuf and
         oeel.prodcat >= rProd[1] and oeel.prodcat <= rProd[2] and          
         oeel.vendno >= rVend[1] and oeel.vendno <= rVend[2] and            
         oeel.reasunavty >= rLostBus[1] and oeel.reasunavty <= rLostBus[2] and 
         oeel.reasunavty >= (if oLostBusiness then " !" else oeel.reasunavty) 
       and
        oeel.reasunavty <= (if oLostBusiness then "}}" else oeel.reasunavty) 
       and
        oeel.reasunavty <> (if oLostBusiness then "CQ" else "}}") 
       and
        oeel.reasunavty <> (if oLostBusiness then "CR" else "}}") 
       no-lock no-error.                                              
    if not avail oeel then  next.
  end.
/* tah jen  */  
  assign            v-addvaluesfl = true.
  if substring(oeeh.user3,53,8) <> "" and
     oeeh.ordersuf = 0 then do:
    /*assign cntConvert = cntConvert + 1.*/
   
    find eb-oeehb where 
         eb-oeehb.cono        = g-cono and                     
         eb-oeehb.batchnm     = substr(oeeh.user3,53,8) and  
         eb-oeehb.seqno       = 1 no-lock no-error.              
                      
    if avail eb-oeehb /* Jen 06/28/10 and
       
       (eb-oeehb.enterDt < rEnterDt[1] or 
        eb-oeehb.enterDt > rEnterDt[2])) or
        not avail eb-oeehb */ then
      v-addvaluesfl = false.
  end.     
        
  sumPrice = 0.
  sumCost  = 0.
  for each oeel where 
    oeel.cono = g-cono and
    oeel.orderno = oeeh.orderno and
    oeel.ordersuf  = oeeh.ordersuf and
    oeel.prodcat >= rProd[1] and oeel.prodcat <= rProd[2] and
    oeel.reasunavty >= rLostBus[1] and oeel.reasunavty <= rLostBus[2]
        and
    oeel.reasunavty >= (if oLostBusiness then " !" else oeel.reasunavty) 
    and
    oeel.reasunavty <= (if oLostBusiness then "}}" else oeel.reasunavty)
    and
    oeel.reasunavty <> (if oLostBusiness then "CQ" else "}}") 
    and
    oeel.reasunavty <> (if oLostBusiness then "CR" else "}}") 
  no-lock:
  
  
  if oList then do:                   
    zelection_type = "p".             
    zelection_char4 = oeel.prodcat.
    zelection_cust = 0.               
    run zelectioncheck.               
    if not zelection_good then next.  
    end. 
  
  if oList then do:                   
    zelection_type = "l".             
    zelection_char4 = oeel.reasunavty.
    zelection_cust = 0.               
    run zelectioncheck.               
    if not zelection_good then next.  
    end. 
    
    /** FIX001 - TA  **/  
 
    if oeel.slsrepout ne "" then 
     assign salescode = oeel.slsrepout. 
    else 
     assign salescode = oeeh.slsrepout.
    if oeel.slsrepin ne "" then 
     assign insidecode = oeel.slsrepin. 
    else 
     assign insidecode = oeeh.slsrepin.
    find first smsn where smsn.cono = g-cono and
               smsn.slsrep = salescode no-lock no-error.
     if avail smsn then do:
      dataRegion   =  substring(smsn.mgr,1,1).
      dataDistrict =  substring(smsn.mgr,2,3).
      release smsn.
     end.
     else do:
      dataRegion = "".
      dataDistrict = "".
     end.
 
    if (dataRegion < rRegion[1] or dataRegion > rRegion[2]) then  
     next.
    
    if (dataDistrict < rDistrict[1] or dataDistrict > rDistrict[2]) then 
     next.
    if (salescode < rSlsOut[1] or  salescode > rSlsOut[2]) or
       (insidecode < rSlsin[1] or insidecode > rSlsin[2]) then
      next.   
   
    if oList then do:
     zelection_type = "s".
     zelection_char4 = salescode.
     zelection_cust = 0.
     run zelectioncheck.
     if not zelection_good then next.
    end. 
    
    assign w-vendno     = 0
           v-vendorid   =  " "
           v-technology =  " "
           v-parentcode =  " ".
    
    find icsp where icsp.cono = g-cono and 
                    icsp.prod = oeel.shipprod no-lock no-error.
    find icsw where icsw.cono = g-cono and 
                    icsw.whse = oeel.whse  and 
                    icsw.prod = oeel.shipprod no-lock no-error.
    assign w-vendno = (if avail icsw then
                        icsw.arpvendno
                       else
                        oeel.vendno).
    if avail icsp then do: 
      find catmaster where catmaster.cono = 1 and 
           catmaster.notestype    = "zz" and
           catmaster.primarykey   = "apsva" and
           catmaster.secondarykey = icsp.prodcat
      no-lock no-error. 
      if not avail catmaster then do:
       v-parentcode = "".
       v-vendorid = substring(icsp.prodcat,1,3).
      end.
      else
       assign v-vendorid   = catmaster.noteln[1].
      end.
    if (v-vendorid < rVendid[1] or v-vendorid > rVendid[2]) or
       (w-vendno < rVend[1] or w-vendno > rVend[2]) then  
       next.
    if oList then do: 
     zelection_type = "n".
     zelection_char4 = v-vendorid.
     zelection_cust = 0.
     run zelectioncheck.
     if zelection_good = false then
       next.
     assign zelection_type = "v"
            zelection_vend = w-vendno
            zelection_char4 = " ".
     run zelectioncheck.
     if zelection_good = false then                                
      next.
    end. 
    /** FIX001 - TA  **/
      
      
/* --------------------------- no line info 
    assign cntLine    = cntLine + 1.
    create repTable.                 
    repTable.slsrepout = salescode.
    repTable.slsrepin  = oeel.slsrepin.
    repTable.enterdt   = oeeh.enterdt.
    repTable.canceldt  = oeeh.canceldt.
    repTable.custno    = oeeh.custno.
       
    repTable.oeehbid = recid(oeeh). 
    repTable.batchnm = string(0,">>>>>>>9") no-error.
    repTable.orderno = oeel.orderno.
    repTable.ordersuf = oeel.ordersuf.
    if error-status:error then message "error batchnm not numeric".
    repTable.oeelbid = recid(oeel).
    repTable.salescd = salescode.
    repTable.region  = dataRegion.
    repTable.district = dataRegion + dataDistrict.
    repTable.custname = dataCustName.
    repTable.cntConvert = 0.
    repTable.cntLost    = 0.
    repTable.cntOpen    = 0.
    repTable.cntOpen2   = 0.
    repTable.cntCancel  = 0.
    repTable.cntOrder   = 0.
    repTable.cntLine    = 0.
    repTable.lostbusinesscd = if oeel.specnstype = "l" then 
                                oeel.reasunavty
                              else 
                                "  ".             
    repTable.lostbusinessst = if v-lostbusiness then
                                if oeel.specnstype = "l" then 
                                  oeel.reasunavty             
                                else
                                  "  "
                              else
                                  " ".
    repTable.slsrepinst = if v-slsrepinsort then 
                             oeel.slsrepin
                          else
                               "     ".                                        
    repTable.slsrepoutst = if v-slsrepoutsort then 
                             salescode
                          else
                              "    ".   
    
    
    /** FIX001 - TA  **/  
    repTable.specnstype = if oeel.specnstype = "l" then 
                        "l" 
                       else
                        " ".           
    /** FIX001 - TA  **/  
 -----   no line info */ 
    
       
    sumPrice = if oeel.specnstype = "l" and oeeh.stagecd <> 9 then 
                0
               else  
                 oeel.price * oeel.qtyord.
  
    sumCost  = if oeel.specnstype = "l" and oeeh.stagecd <> 9 then 
                0
               else  
                oeel.prodcost * oeel.qtyord.  
                
    find b-oeeh where recid(b-oeeh) = recid(oeeh) no-lock no-error.
    find b-oeel where recid(b-oeel) = recid(oeel) no-lock no-error.
    run incost.p (no,
                  " ",
                  b-oeel.cono,
                  output v-cost). 
                   
    if oeeh.transtype = "cr" then
      v-cost = 0.
   
    sumCost  = if oeel.specnstype = "l" and oeeh.stagecd <> 9 then 
                0
               else  
                v-cost.  
 
    
    HPrice = oeel.price * oeel.qtyord. 
/*    HCost  = oeel.prodcost * oeel.qtyord.   */
    find b-repTable use-index k-OrepTable2
                where b-repTable.batchnm = 
                        string(0,">>>>>>>9")
                  and b-repTable.orderno = oeel.orderno
                  and b-repTable.ordersuf = oeel.ordersuf
                  and b-repTable.slsrepinst = (if v-slsrepinsort then 
                                                 oeel.slsrepin
                                               else
                                                 "    ")
                  and b-repTable.slsrepoutst = (if v-slsrepoutsort then 
                                                 salescode
                                                else
                                                  "    ") 
                  and b-repTable.lostbusinessst = 
                                              (if v-lostbusiness then 
                                                 if oeel.specnstype = "l" then 
                                                   oeel.reasunavty
                                                 else
                                                    "  "
                                               else
                                                  "  ") 
                 and b-repTable.oeelbid     = 0                                
                 no-lock no-error.                                              
    if avail b-repTable then
      do:
      assign b-repTable.cntLine  = 0 /* b-repTable.cntLine + 1 */
             b-repTable.sumPriceOEET = b-repTable.sumPriceOEET + 
                                        (if v-addvaluesfl then 
                                            sumPrice
                                         else
                                            0 )
             b-repTable.sumCostOEET  = b-repTable.sumCostOEET + 
                                       (if v-addvaluesfl then 
                                          sumCost
                                        else
                                          0 )
             b-repTable.sumPriceQUOEET = if oeeh.transtype = "QU" then         
                                           b-repTable.sumPriceQUOEET +
                                            sumPrice          
                                         else
                                           b-repTable.sumPriceQUOEET 
            
             b-repTable.sumCostQUOEET  = if oeeh.transtype = "QU" then  
                                            b-repTable.sumCostQUOEET + sumCost
                                         else
                                            b-repTable.sumCostQUOEET 
                                            
             b-repTable.QsumPrice   = 0
             b-repTable.QsumCost    = 0
             b-repTable.CvtsumPrice = if substring(oeeh.user3,53,8) <> "" and                                         oeeh.ordersuf = 0     then
                                        sumPrice
                                      else
                                        0
             b-repTable.CvtsumCost  = if substring(oeeh.user3,53,8) <> "" and                                          oeeh.ordersuf = 0 then
                                        sumCost
                                      else
                                        0
             b-repTable.OpnSumPrice = 0
             b-repTable.OpnSumCost  = 0
             b-repTable.CansumPrice = 0
             b-repTable.CansumCost  = 0.
             
 
      end.
    else do:
    
      assign cntOrderOEET   = 1.
 
  /* Header with totals */
      create b-repTable.                 
      b-repTable.batchnm = string(0,">>>>>>>9") no-error.
      if error-status:error then message "error, batchnm not integer".
      b-repTable.orderno = oeel.orderno.
      b-repTable.ordersuf = oeel.ordersuf.
      b-repTable.oeehbid   = recid(oeeh). 
      b-repTable.oeelbid   = 0.            
      b-repTable.transtype = h-transtype.
      b-repTable.salescd   = salescode.    
      b-repTable.region    = dataRegion.   
      b-repTable.district  = dataRegion + dataDistrict.
      b-repTable.custname  = dataCustName.
      b-repTable.slsrepout = salescode.
      b-repTable.takenby   = oeeh.takenby.
      b-repTable.slsrepinst = if v-slsrepinsort then 
                                oeel.slsrepin
                              else
                                " ".                                            
      b-repTable.slsrepoutst = if v-slsrepoutsort then 
                                 salescode
                               else
                                 " ".   
      
      b-repTable.lostbusinesscd = if oeel.specnstype = "l" then 
                                    oeel.reasunavty
                                  else
                                     "  ".             
      b-repTable.lostbusinessst = if v-lostbusiness then
                                    if oeel.specnstype = "l" then 
                                      oeel.reasunavty             
                                    else
                                      " "
                                  else
                                     "  ".
      b-repTable.slsrepin   = oeel.slsrepin.
      b-repTable.enterdt    = oeeh.enterdt.
      b-repTable.canceldt   = oeeh.canceldt.
      b-repTable.custno     = oeeh.custno.
      b-repTable.cntConvert = cntConvert.
      b-repTable.cntCancel  = 0.
      b-repTable.cntOrder   = 0.
      b-repTable.cntOrderOEET   = /* Jen 06/28/10 */
                                  if substring(oeeh.user3,53,8) <> "" and                                              oeeh.ordersuf = 0     then
                                    0
                                  else if oeeh.ordersuf = 0 then
                                    CntOrderOEET
                                  else
                                    0.
      b-repTable.cntOrderQUOEET = if oeeh.transtype = "QU" then  
                                    CntOrderOEET
                                  else
                                    b-repTable.cntOrderQUOEET. 
      b-repTable.cntLost    = 0.
      b-repTable.cntOpen    = 0.
      b-repTable.cntOpen2   = 0.
      b-repTable.cntLine    = 0.
      b-repTable.sumPriceQUOEET  = if oeeh.transtype = "QU" then  
                                     sumPrice
                                   else
                                     b-repTable.sumPriceQUOEET.           
      b-repTable.sumCostQUOEET  = if oeeh.transtype = "QU" then  
                                    sumCost
                                  else
                                    b-repTable.sumCostQUOEET.
      b-repTable.sumPriceOEET =  (if v-addvaluesfl then 
                                     sumPrice
                                  else
                                     0 ).
      b-repTable.sumCostOEET  = (if v-addvaluesfl then 
                                   sumCost
                                 else
                                    0 ).
/*      
      b-repTable.sumPriceOEET   = sumPrice.           
      b-repTable.sumCostOEET    = sumCost.
*/
      b-repTable.QsumPrice   = 0.
      b-repTable.QsumCost    = 0.
      b-repTable.CvtsumPrice = if substring(oeeh.user3,53,8) <> "" and 
                                  oeeh.ordersuf = 0 then
                                 sumPrice
                               else
                                 0.
      b-repTable.CvtsumCost  = if substring(oeeh.user3,53,8) <> "" and
                                  oeeh.ordersuf = 0 then
                                 sumCost
                               else
                                 0.
      b-repTable.CvtsumPrice = 0.
      b-repTable.CvtsumCost  = 0.
      b-repTable.OpnSumPrice = 0.
      b-repTable.OpnSumCost  = 0.
      b-repTable.CansumPrice = 0.
      b-repTable.CansumCost  = 0.
    end.
  end. /* oeel */   
end. /* oeeh pass 1 */  
end. /* procedure extractOrders_data */
  
  
Procedure DetailPrint:
  define input parameter x-recid as recid no-undo.
  
  find first repTable where recid(repTable) = x-recid no-lock no-error.
  
  if repTable.batchnm = string(0,">>>>>>>9") then
    return.
  
  find first oeehb where recid(oeehb) = repTable.oeehbid no-lock no-error.
  
  if not avail oeehb then return.
  
  v-contact = trim(substring(oeehb.user3,1,20)).
  v-phone   = trim(substring(oeehb.user3,21,12)).
  v-obc     = if oeehb.xxl1 = yes then "yes" else " no".
  v-email   = trim(substring(oeehb.user3,33,20)).
  /* totCost   = dec(substring(oeehb.user3,44,11)). */
  v-rebFlag = if substring(oeehb.user3,55,1) = "y" then yes else no.
  /* totMargin = dec(substring(oeehb.user3,56,11)). */
  /*  v-margin = dec(substring(oeehb.user3,67,11)). */
  if repTable.sumPrice ne 0 then
    v-margin = (repTable.sumPrice - repTable.sumCost) * 100 / repTable.sumPrice.
  else v-margin = 0.
  lostbuscode = substring(oeehb.user3,53,2).
  lostbusreason = "".
  if trim(lostbuscode) ne "" then do:
    find first sasta where sasta.cono = g-cono and
    sasta.codeiden = "e" and sasta.codeval = lostbuscode
    no-lock no-error.
    if avail sasta then lostbusreason = sasta.descr.
    release sasta.
  end.  
  /* show header */
  if oDetail ne "s" then do:
    if /* recid(oeehb) ne lastoeehb */ repTable.oeelbid = 0 then do: 
      assign v-batchnm = trim(oeehb.Batchnm," ") + "q"
             v-margin  = if v-margin < (999.99 * -1) then 
                          (999.99 * -1)
                         else   
                           v-margin.
       display  
        repTable.region
        substring(repTable.district,2,3) @ reptable.district
        oeehb.EnterDt   
        oeehb.Stagecd   
        oeehb.TakenBy   
        oeehb.CustNo    
        repTable.custname
        v-batchnm   
        v-contact       
        v-phone 
        v-obc
        repTable.sumPrice
        repTable.sumCost         
        v-margin 
        repTable.salescd 
        oeehb.CancelDt   
        oeehb.ShipTo     
        lostbuscode    
        oeehb.orderno    
        repTable.transtype  
      with frame f-oeehb.
      down with frame f-oeehb.
    end.
    /* show notes */
    showNotes    = "Notes:".
    showNoteType = "".
    printNoteln  = "".
    
    if oNotes ne "n" and repTable.oeelbid = 0 then do:
      if oNotes = "I" or oNotes = "B" then do:
        /* show internal notes */
        for each notes where notes.cono = g-cono and                         
          notes.notestype = "ba" and notes.primarykey = oeehb.batchnm and  
          notes.secondarykey = "I" no-lock by notes.pageno:                    
          showNoteType = "(I)".  
          do iter = 1 to 16:
             if iter = 1 then 
              display with frame f-skip.
             if trim(notes.noteln[iter]) ne "" then do:
              printNoteln = replace(trim(notes.noteln[iter]),chr(10)," ").
              display showNotes showNoteType printNoteln with frame f-notes.
              down with frame f-notes.
            end.
            showNotes    = "".
            showNoteType = "".  
          end.
          display with frame f-skip.
         end.
      end.
      if oNotes = "E" or oNotes = "B" then do:
        /* show external notes */
        for each notes where notes.cono = g-cono and                    
          notes.notestype = "ba" and notes.primarykey = oeehb.batchnm and
          notes.secondarykey = "E" no-lock by notes.pageno:              
          showNoteType = "(E)".                                             
          do iter = 1 to 16:            
            if iter = 1 then 
              display with frame f-skip.
            if trim(notes.noteln[iter]) ne "" then do:                      
              printNoteln = replace(trim(notes.noteln[iter]),chr(10)," ").
              display showNotes showNoteType printNoteln with frame f-notes.
              down with frame f-notes.
            end.                               
            showNotes    = "".
            showNoteType = "". 
          end.                                                              
          display with frame f-skip.
         end.                                                             
      end.
    end.
    /* show line item detail */
    if  oDetail ne "s" and oDetail ne "h" then do:
      display with frame f-lineheader.
      for each b-repTable use-index k-repTable3 where 
               b-repTable.oeehbid = repTable.oeehbid and
               b-repTable.slsrepoutst = repTable.slsrepoutst and
               b-repTable.slsrepinst  = repTable.slsrepinst and
               b-repTable.lostbusinessst  = repTable.lostbusinessst and
               b-repTable.oeelbid <> 0  no-lock:
/*
      message  b-repTable.oeehbid   repTable.oeehbid 
               b-repTable.slsrepoutst   repTable.slsrepoutst 
               b-repTable.slsrepinst   repTable.slsrepinst.
                                              pause.
*/         
        find first oeelb where recid(oeelb) = b-repTable.oeelbid 
         no-lock no-error.
        /** FIX001 - TA  **/  
        if oeelb.slsrepout ne "" then 
         assign currSlsrep = oeelb.slsrepout. 
        else 
        if oeehb.slsrepout ne "" then 
         assign currSlsrep = oeehb.slsrepout. 
         
        if oeelb.price ne 0 then
         v-margin = (oeelb.price - oeelb.prodcost) * 100 / oeelb.price.
        else
         v-margin = 0.
        assign v-margin  = if v-margin < (999.99 * -1) then 
                            (999.99 * -1)
                           else   
                            v-margin.
      /**
      if error-status:error then message "error u3:v-margin".
      lineCost = dec(substring(oeelb.user3, 44, 11)) no-error.
      if error-status:error then message "error u3:lineCost".
      lineMarg = dec(substring(oeelb.user3, 56, 11)) no-error.
      if error-status:error then message "error u3:lineMarg".
      **/
        lineCost = oeelb.prodcost * oeelb.qtyord.
        linePrice = oeelb.price * oeelb.qtyord.
        if oeelb.specnstype = "L" then specnsl = "(l)".
        else do:
          specnsl = "".
          if oeehb.stagecd = 9 and not (oeehb.orderno > 0) then specnsl = "(l)".
        end.
        if oDetail = "L" then do:
          lostbuscode = oeelb.reasunavty.             
          lostbusreason = "".                                    
          if trim(lostbuscode) ne "" then do:                    
              find first sasta where sasta.cono = g-cono and       
              sasta.codeiden = "e" and sasta.codeval = lostbuscode 
              no-lock no-error.                                    
              if avail sasta then lostbusreason = sasta.descr.     
              release sasta.                                       
          end.                                                   
        end.
        display specnsl
                 oeelb.lineno
                 oeelb.shipprod  
                 oeelb.pricetype          
                 oeelb.qtyord 
                 oeelb.reasunavty
                 lostbusreason
                 oeelb.promisedt
                 currSlsrep
                 linePrice        
                 lineCost     
                 v-margin           
                 with frame f-linedtl.
                 down with frame f-linedtl.
      end. /* for each */
      hide frame f-lineheader.
    end. /** if odetail = **/
  end.
  lastoeehb = recid(oeehb).
  release oeehb.
  release oeelb.
end.
 
&endif
 
/* TBXR F6 CodeGen Includes from here down are CodeGen Generated  */ 
&if defined(user_optiontype) = 2 &then
if not
can-do("B,C,D,E,I,L,N,O,R,T",substring(p-optiontype,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries B,C,D,E,I,L,N,O,R,T "
     substring(p-optiontype,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries B,C,D,E,I,L,N,O,R,T "
     substring(p-optiontype,v-inx,1).
&endif
 
&if defined(user_registertype) = 2 &then
if not
can-do("B,C,D,E,I,L,N,O,R,T",substring(p-register,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries B,C,D,E,I,L,N,O,R,T "
     substring(p-register,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries B,C,D,E,I,L,N,O,R,T "
     substring(p-register,v-inx,1).
&endif
 
&if defined(user_exportvarheaders1) = 2 &then
if v-lookup[v-inx4] = "B" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Branch".
  end. 
else  
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Customer".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "District".
  end. 
else  
if v-lookup[v-inx4] = "E" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Enter".
  end. 
else  
if v-lookup[v-inx4] = "I" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "SlsRepIn".
  assign export_rec = export_rec + v-del + "SlsRepIn".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "L" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "LostBusiness".
  end. 
else  
if v-lookup[v-inx4] = "N" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Cancel".
  end. 
else  
if v-lookup[v-inx4] = "O" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "SlsRepOut".
  assign export_rec = export_rec + v-del + "SlsRepOut".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Region".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "TakenBy".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_exportvarheaders2) = 2 &then
if v-lookup[v-inx4] = "B" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Name".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "E" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Date".
  end. 
else  
if v-lookup[v-inx4] = "I" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  assign export_rec = export_rec + v-del + "Name".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "L" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "ReasonCode".
  end. 
else  
if v-lookup[v-inx4] = "N" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Date".
  end. 
else  
if v-lookup[v-inx4] = "O" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  assign export_rec = export_rec + v-del + "Name".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_DWvarheaders1) = 2 &then
if v-lookup[v-inx4] = "B" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Branch".
  end. 
else  
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "CustName".
  end. 
else  
if v-lookup[v-inx4] = "D" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "District".
  end. 
else  
if v-lookup[v-inx4] = "E" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "EnterDt".
  end. 
else  
if v-lookup[v-inx4] = "I" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "SlsRepIn".
  assign export_rec = export_rec + v-del + "SlsRepInName".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "L" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "LostbusinessCd".
  end. 
else  
if v-lookup[v-inx4] = "N" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "CancelDt".
  end. 
else  
if v-lookup[v-inx4] = "O" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "SlsRepOut".
  assign export_rec = export_rec + v-del + "SlsRepOutName".
  assign v-cells[v-inx4] = "2".
  end. 
else  
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Region".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "TakenBy".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_loadtokens) = 2 &then
if v-lookup[v-inx] = "B" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.Branch,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "C" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.custname,"x(15)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(15)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "D" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.district,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "E" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.enterdt,"99/99/99")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"99/99/99").   
  end.                                              
else                                              
if v-lookup[v-inx] = "I" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.slsrepin,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "L" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.lostbusinesscd,"x(2)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(2)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "N" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.Canceldt,"99/99/99")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"99/99/99").   
  end.                                              
else                                              
if v-lookup[v-inx] = "O" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.slsrepout,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "R" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.region,"x(1)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(1)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "T" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.Takenby,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "X" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.batchnm,"x(8)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(8)").   
  end.                                              
else                                              
  assign v-token = " ".
&endif
 
&if defined(user_totaladd) = 2 &then
  assign t-amounts[1] = repTable.sumPrice.
  assign t-amounts[2] = repTable.sumCost.
  assign t-amounts[3] = repTable.cntConvert.
  assign t-amounts[4] = repTable.cntCancel.
  assign t-amounts[5] = 0.
  assign t-amounts[6] = repTable.cntLine.
  assign t-amounts[7] = repTable.cntOrder.
  assign t-amounts[8] = repTable.QsumPrice.
  assign t-amounts[9] = repTable.QsumCost.
  assign t-amounts[10] = 0.
  assign t-amounts[11] = repTable.CvtSumPrice.
  assign t-amounts[12] = repTable.CvtSumCost.
  assign t-amounts[13] = 0.
  assign t-amounts[14] = repTable.CanSumPrice.
  assign t-amounts[15] = repTable.CanSumCost.
  assign t-amounts[16] = 0.
  assign t-amounts[17] = repTable.OpnSumPrice.
  assign t-amounts[18] = repTable.OpnSumCost.
  assign t-amounts[19] = 0.
  assign t-amounts[20] = reptable.cntLost.
  assign t-amounts[21] = reptable.cntOpen.
  assign t-amounts[22] = 0.
  assign t-amounts[23] = 0.
  assign t-amounts[24] = 0.
  assign t-amounts[25] = repTable.cntopen2.
  assign t-amounts[26] = repTable.cntOrderOEET.
  assign t-amounts[27] = repTable.sumpriceOEET.
  assign t-amounts[28] = reptable.sumCostOEET.
  assign t-amounts[29] = 0.
  assign t-amounts[30] = 0.
  assign t-amounts[31] = repTable.sumpriceQUOEET.
  assign t-amounts[32] = reptable.sumCostQUOEET.
  assign t-amounts[33] = 0.
  assign t-amounts[34] = repTable.cntOrderQUOEET.
  assign t-amounts[35] = repTable.cntOBCquote.
  assign t-amounts[36] = 0.
  assign t-amounts[37] = repTable.OBCSumPrice.
  assign t-amounts[38] = repTable.OBCSumCost.
  assign t-amounts[39] = 0.
  assign t-amounts[40] = repTable.cntNOBCquote.
  assign t-amounts[41] = 0.
  assign t-amounts[42] = repTable.NOBCSumPrice.
  assign t-amounts[43] = repTable.NOBCSumCost.
  assign t-amounts[44] = 0.
&endif
 
&if defined(user_numbertotals) = 2 &then
  if v-subtotalup[v-inx4] = 1 then
    v-val = t-amounts1[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 2 then
    v-val = t-amounts2[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 3 then
    v-val = t-amounts3[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 4 then
    v-val = t-amounts4[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 5 then
    v-val = 
      {x-oerzq-fmt5.i "1" "2"}
  else 
  if v-subtotalup[v-inx4] = 6 then
    v-val = t-amounts6[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 7 then
    v-val = t-amounts7[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 8 then
    v-val = t-amounts8[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 9 then
    v-val = t-amounts9[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 10 then
    v-val = 
      {x-oerzq-fmt5.i "8" "9"}
  else 
  if v-subtotalup[v-inx4] = 11 then
    v-val = t-amounts11[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 12 then
    v-val = t-amounts12[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 13 then
    v-val = 
      {x-oerzq-fmt5.i "11" "12"}
  else 
  if v-subtotalup[v-inx4] = 14 then
    v-val = t-amounts14[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 15 then
    v-val = t-amounts15[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 16 then
    v-val = 
      {x-oerzq-fmt5.i "14" "15"}
  else 
  if v-subtotalup[v-inx4] = 17 then
    v-val = t-amounts17[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 18 then
    v-val = t-amounts18[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 19 then
    v-val = 
      {x-oerzq-fmt5.i "17" "18"}
  else 
  if v-subtotalup[v-inx4] = 20 then
    v-val = t-amounts20[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 21 then
    v-val = t-amounts21[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 22 then
    v-val = 
      {x-oerzq-fmt20.i "7" "3"}
  else 
  if v-subtotalup[v-inx4] = 23 then
    v-val = 
      {x-oerzq-fmt20.i "7" "20"}
  else 
  if v-subtotalup[v-inx4] = 24 then
    v-val = 
      {x-oerzq-fmt20.i "7" "25"}
  else 
  if v-subtotalup[v-inx4] = 25 then
    v-val = t-amounts25[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 26 then
    v-val = t-amounts26[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 27 then
    v-val = t-amounts27[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 28 then
    v-val = t-amounts28[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 29 then
    v-val = 
      {x-oerzq-fmt5.i "27" "28"}
  else 
  if v-subtotalup[v-inx4] = 30 then
    v-val = 
      {x-oerzq-fmt20.i "7" "28"}
  else 
  if v-subtotalup[v-inx4] = 31 then
    v-val = t-amounts31[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 32 then
    v-val = t-amounts32[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 33 then
    v-val = 
      {x-oerzq-fmt5.i "31" "32"}
  else 
  if v-subtotalup[v-inx4] = 34 then
    v-val = t-amounts34[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 35 then
    v-val = t-amounts35[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 36 then
    v-val = 
      {x-oerzq-fmt20.i "7" "35"}
  else 
  if v-subtotalup[v-inx4] = 37 then
    v-val = t-amounts37[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 38 then
    v-val = t-amounts38[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 39 then
    v-val = 
      {x-oerzq-fmt5.i "37" "38"}
  else 
  if v-subtotalup[v-inx4] = 40 then
    v-val = t-amounts40[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 41 then
    v-val = 
      {x-oerzq-fmt20.i "7" "40"}
  else 
  if v-subtotalup[v-inx4] = 42 then
    v-val = t-amounts42[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 43 then
    v-val = t-amounts43[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 44 then
    v-val = 
      {x-oerzq-fmt5.i "42" "43"}
  else 
    v-val = t-amounts1[v-inx4].
  do inz = 1 to 44:
  if inz = 1 then
    v-regval[inz] = t-amounts1[v-inx4].
  else 
  if inz = 2 then
    v-regval[inz] = t-amounts2[v-inx4].
  else 
  if inz = 3 then
    v-regval[inz] = t-amounts3[v-inx4].
  else 
  if inz = 4 then
    v-regval[inz] = t-amounts4[v-inx4].
  else 
  if inz = 5 then
    v-regval[inz] = 
      {x-oerzq-fmt5.i "1" "2"}
  else 
  if inz = 6 then
    v-regval[inz] = t-amounts6[v-inx4].
  else 
  if inz = 7 then
    v-regval[inz] = t-amounts7[v-inx4].
  else 
  if inz = 8 then
    v-regval[inz] = t-amounts8[v-inx4].
  else 
  if inz = 9 then
    v-regval[inz] = t-amounts9[v-inx4].
  else 
  if inz = 10 then
    v-regval[inz] = 
      {x-oerzq-fmt5.i "8" "9"}
  else 
  if inz = 11 then
    v-regval[inz] = t-amounts11[v-inx4].
  else 
  if inz = 12 then
    v-regval[inz] = t-amounts12[v-inx4].
  else 
  if inz = 13 then
    v-regval[inz] = 
      {x-oerzq-fmt5.i "11" "12"}
  else 
  if inz = 14 then
    v-regval[inz] = t-amounts14[v-inx4].
  else 
  if inz = 15 then
    v-regval[inz] = t-amounts15[v-inx4].
  else 
  if inz = 16 then
    v-regval[inz] = 
      {x-oerzq-fmt5.i "14" "15"}
  else 
  if inz = 17 then
    v-regval[inz] = t-amounts17[v-inx4].
  else 
  if inz = 18 then
    v-regval[inz] = t-amounts18[v-inx4].
  else 
  if inz = 19 then
    v-regval[inz] = 
      {x-oerzq-fmt5.i "17" "18"}
  else 
  if inz = 20 then
    v-regval[inz] = t-amounts20[v-inx4].
  else 
  if inz = 21 then
    v-regval[inz] = t-amounts21[v-inx4].
  else 
  if inz = 22 then
    v-regval[inz] = 
      {x-oerzq-fmt20.i "7" "3"}
  else 
  if inz = 23 then
    v-regval[inz] = 
      {x-oerzq-fmt20.i "7" "20"}
  else 
  if inz = 24 then
    v-regval[inz] = 
      {x-oerzq-fmt20.i "7" "25"}
  else 
  if inz = 25 then
    v-regval[inz] = t-amounts25[v-inx4].
  else 
  if inz = 26 then
    v-regval[inz] = t-amounts26[v-inx4].
  else 
  if inz = 27 then
    v-regval[inz] = t-amounts27[v-inx4].
  else 
  if inz = 28 then
    v-regval[inz] = t-amounts28[v-inx4].
  else 
  if inz = 29 then
    v-regval[inz] = 
      {x-oerzq-fmt5.i "27" "28"}
  else 
  if inz = 30 then
    v-regval[inz] = 
      {x-oerzq-fmt20.i "7" "28"}
  else 
  if inz = 31 then
    v-regval[inz] = t-amounts31[v-inx4].
  else 
  if inz = 32 then
    v-regval[inz] = t-amounts32[v-inx4].
  else 
  if inz = 33 then
    v-regval[inz] = 
      {x-oerzq-fmt5.i "31" "32"}
  else 
  if inz = 34 then
    v-regval[inz] = t-amounts34[v-inx4].
  else 
  if inz = 35 then
    v-regval[inz] = t-amounts35[v-inx4].
  else 
  if inz = 36 then
    v-regval[inz] = 
      {x-oerzq-fmt20.i "7" "35"}
  else 
  if inz = 37 then
    v-regval[inz] = t-amounts37[v-inx4].
  else 
  if inz = 38 then
    v-regval[inz] = t-amounts38[v-inx4].
  else 
  if inz = 39 then
    v-regval[inz] = 
      {x-oerzq-fmt5.i "37" "38"}
  else 
  if inz = 40 then
    v-regval[inz] = t-amounts40[v-inx4].
  else 
  if inz = 41 then
    v-regval[inz] = 
      {x-oerzq-fmt20.i "7" "40"}
  else 
  if inz = 42 then
    v-regval[inz] = t-amounts42[v-inx4].
  else 
  if inz = 43 then
    v-regval[inz] = t-amounts43[v-inx4].
  else 
  if inz = 44 then
    v-regval[inz] = 
      {x-oerzq-fmt5.i "42" "43"}
  else 
    assign inz = inz.
  end.
&endif
 
&if defined(user_summaryloadprint) = 2 &then
  if v-lookup[v-inx2] = "B" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Branch - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "C" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Customer Name - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "D" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "District - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "E" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Entered Date - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "I" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-smsn.i}
      end. 
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Sales Rep In - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "L" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Lost Business Reason - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "N" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Canceled Date - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "O" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-smsn.i}
      end. 
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Sales Rep Out - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "R" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Region - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "T" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Taken By - " + h-lookup + " " + h-genlit.
    end.
  else
      v-summary-lit = v-lookup[v-inx2].
&endif
 
&if defined(user_summaryloadexport) = 2 &then
  if v-inx4 > v-inx2  then 
    do:
    if v-inx4 > 1 then          
      export_rec = export_rec + v-del. 
    assign export_rec = export_rec +  " ".
    if v-cells[v-inx4] = "2" then
      assign export_rec = export_rec +  v-del + " ".
    end. 
  else 
  if v-lookup[v-inx4] = "B" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "C" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(15)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(15)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "D" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "E" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "99/99/99" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "99/99/99" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "I" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-smsn.i}
      end. 
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "L" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(2)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(2)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "N" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "99/99/99" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "99/99/99" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "O" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    else 
      do: 
      {x-xrptx-smsn.i}
      end. 
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "R" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(1)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(1)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "T" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
    do:
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    assign export_rec = export_rec + v-lookup[v-inx4].
    end.
&endif
 
/* TBXR F6 CodeGen End of Includes that are CodeGen Generated  */ 
