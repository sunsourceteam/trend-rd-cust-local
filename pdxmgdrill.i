/****************************************************************************
*                                                                           *
* pdxmgdrill.i - General Matrix Margin - Order Detail                       *
*                                                                           *
****************************************************************************/

def new shared var x-mo-1          as c    format "x(9)"             no-undo.
def new shared var x-mo-1ytd       as c    format "x(9)"             no-undo.
def new shared var x-mo-2          as c    format "x(9)"             no-undo.
 
def new shared var b-region        as c    format "x(4)"             no-undo.
def new shared var e-region        as c    format "x(4)"             no-undo.
def new shared var b-salesrep      as c    format "x(4)"             no-undo.
def new shared var e-salesrep      as c    format "x(4)"             no-undo.
def new shared var b-customer      as de   format "999999999999"     no-undo.
def new shared var e-customer      as de   format "999999999999"     no-undo.
def new shared var b-cprice-type   as c    format "x(4)"             no-undo.
def new shared var e-cprice-type   as c    format "x(4)"             no-undo.
def new shared var b-pprice-type   as c    format "x(4)"             no-undo.
def new shared var e-pprice-type   as c    format "x(4)"             no-undo.

def new shared var p-date          as date format "99/99/9999"       no-undo.
def new shared var p-rptfl         as c    format "x(1)"             no-undo.
def new shared var p-sort          as c    format "x(1)"             no-undo.
def new shared var p-drill         as logical                        no-undo.
def new shared var p-count         as i    format "9999999"          no-undo.
def new shared var p-nonmatrix     as logical                        no-undo.
def new shared var p-policy21      as logical                        no-undo.
def new shared var p-kits          as logical                        no-undo.
def new shared var p-replcst       as logical                        no-undo.

def var firstfl         as logical                            no-undo.
def var print-count     as i    format "9999999"              no-undo.

def            var s-cprice        as c    format "x(4)"             no-undo.
def            var s-pprice        as c    format "x(4)"             no-undo.
def            var s-rgn           as c    format "x(4)"             no-undo.
def            var r-rgn           as c    format "x(5)"             no-undo.
def            var s-custno        as de   format ">>>>>>>>>99"      no-undo.
def            var s-name          as c    format "x(17)"            no-undo.
def            var s-price-desc    as c    format "x(15)"            no-undo.

def            var s-date          as c    format "x(8)"             no-undo.
def            var s-order         as c    format "x(10)"            no-undo.
def            var s-line          as c    format "x(3)"             no-undo.
def            var s-isales        as de   format ">,>>>,>>9-"       no-undo.
def            var s-mmarg         as de   format ">>>>9.99-"        no-undo.
def            var s-imarg         as de   format ">>>>9.99-"        no-undo.

def            var w-margin        as de   format ">>>>>>>9.99-"     no-undo.
def            var matrix-price    as de   format ">>>>>>9.999"      no-undo.
def            var w-errmsg        as c    format "x(65)"            no-undo.

def  var ao-mo-isales     as de format ">>>>>>>>9.99-" init 0   no-undo.
def  var ao-mo-imrgsales  as de format ">>>>>>>>9.99-" init 0   no-undo.
def  var ao-lmo-isales    as de format ">>>>>>>>9.99-" init 0   no-undo.
def  var ao-lmo-imrgsales as de format ">>>>>>>>9.99-" init 0   no-undo.
def  var ao-ytd-isales    as de format ">>>>>>>>9.99-" init 0   no-undo.
def  var ao-ytd-imrgsales as de format ">>>>>>>>9.99-" init 0   no-undo.
def  var ao-mo-msales     as de format ">>>>>>>>9.99-" init 0   no-undo.
def  var ao-mo-mmrgsales  as de format ">>>>>>>>9.99-" init 0   no-undo.
def  var ao-lmo-msales    as de format ">>>>>>>>9.99-" init 0   no-undo.
def  var ao-lmo-mmrgsales as de format ">>>>>>>>9.99-" init 0   no-undo.
def  var ao-ytd-msales    as de format ">>>>>>>>9.99-" init 0   no-undo.
def  var ao-ytd-mmrgsales as de format ">>>>>>>>9.99-" init 0   no-undo.

def  var s-mo-msales      as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-mo-isales      as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-mo-mmrgsales   as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-mo-imrgsales   as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-mo-mmarg       as de format ">>,>>9.99-"    init 0   no-undo.
def  var s-mo-imarg       as de format ">>,>>9.99-"    init 0   no-undo.
def  var s-lmo-msales     as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-lmo-isales     as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-lmo-mmrgsales  as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-lmo-imrgsales  as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-lmo-mmarg      as de format ">>,>>9.99-"    init 0   no-undo.
def  var s-lmo-imarg      as de format ">>,>>9.99-"    init 0   no-undo.
def  var s-ytd-msales     as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-ytd-isales     as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-ytd-mmrgsales  as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-ytd-imrgsales  as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-ytd-mmarg      as de format ">>,>>9.99-"    init 0   no-undo. 
def  var s-ytd-imarg      as de format ">>,>>9.99-"    init 0   no-undo.

def  var s-mo-tmsales     as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-mo-tisales     as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-mo-tmmrgsales  as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-mo-timrgsales  as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-mo-tmmarg      as de format ">>,>>9.99-"    init 0   no-undo.
def  var s-mo-timarg      as de format ">>,>>9.99-"    init 0   no-undo.
def  var s-lmo-tmsales    as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-lmo-tisales    as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-lmo-tmmrgsales as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-lmo-timrgsales as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-lmo-tmmarg     as de format ">>,>>9.99-"    init 0   no-undo.
def  var s-lmo-timarg     as de format ">>,>>9.99-"    init 0   no-undo.
def  var s-ytd-tmsales    as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-ytd-tisales    as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-ytd-tmmrgsales as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-ytd-timrgsales as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var s-ytd-tmmarg     as de format ">>,>>9.99-"    init 0   no-undo.
def  var s-ytd-timarg     as de format ">>,>>9.99-"    init 0   no-undo.

def  var r-mo-tmsales     as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var r-mo-tisales     as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var r-mo-tmmrgsales  as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var r-mo-timrgsales  as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var r-mo-tmmarg      as de format ">>,>>9.99-"    init 0   no-undo.
def  var r-mo-timarg      as de format ">>,>>9.99-"    init 0   no-undo.
def  var r-lmo-tmsales    as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var r-lmo-tisales    as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var r-lmo-tmmrgsales as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var r-lmo-timrgsales as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var r-lmo-tmmarg     as de format ">>,>>9.99-"    init 0   no-undo.
def  var r-lmo-timarg     as de format ">>,>>9.99-"    init 0   no-undo.
def  var r-ytd-tmsales    as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var r-ytd-tisales    as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var r-ytd-tmmrgsales as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var r-ytd-timrgsales as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var r-ytd-tmmarg     as de format ">>,>>9.99-"    init 0   no-undo.
def  var r-ytd-timarg     as de format ">>,>>9.99-"    init 0   no-undo.

def  var g-mo-tmsales     as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var g-mo-tisales     as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var g-mo-tmmrgsales  as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var g-mo-timrgsales  as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var g-mo-tmmarg      as de format ">>,>>9.99-"    init 0   no-undo.
def  var g-mo-timarg      as de format ">>,>>9.99-"    init 0   no-undo.
def  var g-lmo-tmsales    as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var g-lmo-tisales    as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var g-lmo-tmmrgsales as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var g-lmo-timrgsales as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var g-lmo-tmmarg     as de format ">>,>>9.99-"    init 0   no-undo. 
def  var g-lmo-timarg     as de format ">>,>>9.99-"    init 0   no-undo.
def  var g-ytd-tmsales    as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var g-ytd-tisales    as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var g-ytd-tmmrgsales as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var g-ytd-timrgsales as de format ">>>,>>>,>>9-"  init 0   no-undo.
def  var g-ytd-tmmarg     as de format ">>,>>9.99-"    init 0   no-undo.
def  var g-ytd-timarg     as de format ">>,>>9.99-"    init 0   no-undo.

def new shared var cur-mo           as i    format "99"       no-undo.
def new shared var cur-yr           as i    format "99"       no-undo.
def new shared var last-mo          as i    format "99"       no-undo.
def new shared var last-yr          as i    format "99"       no-undo.
def new shared var w-year           as c    format "x(4)"     no-undo.

def new shared var w-date           as c    format "x(10)"      no-undo.
def new shared var b1-date          as date format "99/99/9999" no-undo.
def new shared var e1-date          as date format "99/99/9999" no-undo.
def new shared var b2-date          as date format "99/99/9999" no-undo.
def new shared var e2-date          as date format "99/99/9999" no-undo.
def new shared var beg-date         as date format "99/99/9999" no-undo.
def new shared var leapyr           as de   format "9999.99"    no-undo.
def new shared var leaptst          as c    format "x(7)"       no-undo.

def            var type2            as logical                 no-undo.
def            var w-price          like icsw.listprice        no-undo.
def            var w-qtyship        like oeel.qtyship          no-undo.
def            var t-quantity       like oeel.qtyord           no-undo.
def            var w-cost           like oeel.prodcost         no-undo.

def var counter          as i    format "9999"                no-undo.
   
def var ex-order         as i    format "9999999"             no-undo.
def var ex-suffix        as i    format "99"                  no-undo. 

def var nextfl           as logical.

def stream exclude_oeeh.

def buffer b-icsp for icsp.
def buffer b-oeel  for oeel.
def buffer bk-oeel for oeel.
def buffer br-oeel for oeel.


assign firstfl = yes.

display with frame f-dd-header.

do b = 1 to 2:

   if b = 2 then do:
      assign w-date                = string(e1-date,"99/99/9999")
             substring(w-date,7,4) = string((cur-yr - 1),"9999")
             e1-date               = date(w-date)
             w-date                = string(beg-date,"99/99/9999")
             substring(w-date,7,4) = string((cur-yr - 1),"9999")
             beg-date              = date(w-date).
   end.
   
if b = 2 then do:
   assign w-year = string((cur-yr),"9999").
   if s-ytd-msales <> 0 then
      assign s-ytd-mmarg = (s-ytd-mmrgsales / s-ytd-msales) * 100.
   if s-ytd-isales <> 0 then
      assign s-ytd-imarg = (s-ytd-imrgsales / s-ytd-isales) * 100.
   display w-year                 
           s-ytd-isales               
           s-ytd-mmarg                
           s-ytd-imarg                
   with frame f-dd-total.
   down with frame f-dd-total.   
   
   assign s-ytd-isales    = 0
          s-ytd-mmarg     = 0
          s-ytd-imarg     = 0
          s-ytd-msales    = 0
          s-ytd-mmrgsales = 0
          s-ytd-imrgsales = 0.
end.
                 

for each oeeh use-index k-custno  where oeeh.cono      = 1 
                                    and oeeh.custno    = b-customer
                                    and oeeh.stagecd   >= 4
                                    and oeeh.stagecd   <= 5
                                    and oeeh.invoicedt >= beg-date
                                    and oeeh.invoicedt <= e1-date
                                    and oeeh.updtype    = "m"  /* invoiced or
                                                                  paid */
                                    no-lock:


    if p-policy21 then
       if 
          oeel.slsrepout = "0982" or
          oeel.slsrepout = "0983" or
          oeel.slsrepout = "0984" or
          oeel.slsrepout = "0985" or
          oeel.slsrepout = "0986" or
          oeel.slsrepout = "0987" or
          oeel.slsrepout = "0988" or
          oeel.slsrepout = "0990" or

          oeeh.slsrepout = "0991" or
          oeeh.slsrepout = "0992" or
          oeeh.slsrepout = "0993" or
          oeeh.slsrepout = "0994" or
          oeeh.slsrepout = "0995" or
          oeeh.slsrepout = "0996" or
          oeeh.slsrepout = "0997" or
          oeeh.slsrepout = "0998" or
          oeeh.slsrepout = "0999"
          then next.
                   
    /*
    if (oeeh.orderno = 437288 and oeeh.ordersuf = 03) or
       (oeeh.orderno = 571129 and oeeh.ordersuf = 00) or
       (oeeh.orderno = 182161 and oeeh.ordersuf = 25) or
       (oeeh.orderno = 182161 and oeeh.ordersuf = 26) or
       (oeeh.orderno = 450315 and oeeh.ordersuf = 00) or
       (oeeh.orderno = 450275 and oeeh.ordersuf = 00) or
       (oeeh.orderno = 402693 and oeeh.ordersuf = 00) or
       (oeeh.orderno = 495770 and oeeh.ordersuf = 00) or
       (oeeh.orderno = 413275 and oeeh.ordersuf = 01) or
       (oeeh.orderno = 569549 and oeeh.ordersuf = 00) or
       (oeeh.orderno = 393728 and oeeh.ordersuf = 00) or
       (oeeh.orderno = 460882 and oeeh.ordersuf = 02) or
       (oeeh.orderno = 377739 and oeeh.ordersuf = 00) or
       (oeeh.orderno = 467374 and oeeh.ordersuf = 00) or
       (oeeh.orderno = 373940 and oeeh.ordersuf = 00) or
       (oeeh.orderno = 355215 and oeeh.ordersuf = 00) or
       (oeeh.orderno = 419175 and oeeh.ordersuf = 00) or
       (oeeh.orderno = 463793 and oeeh.ordersuf = 00) or
       (oeeh.orderno = 343882 and oeeh.ordersuf = 01) or
       (oeeh.orderno = 343882 and oeeh.ordersuf = 04) or
       (oeeh.orderno = 373882 and oeeh.ordersuf = 05) or
       (oeeh.orderno = 350949 and oeeh.ordersuf = 01) or
       (oeeh.orderno = 355215 and oeeh.ordersuf = 00) or
       (oeeh.orderno = 585318 and oeeh.ordersuf = 00) or  
       (oeeh.orderno = 585462 and oeeh.ordersuf = 00) or  
       (oeeh.orderno = 367502 and oeeh.ordersuf = 00) or  
       (oeeh.orderno = 460495 and oeeh.ordersuf = 00) or  
       (oeeh.orderno = 509926 and oeeh.ordersuf = 00) or  
       (oeeh.orderno = 410166 and oeeh.ordersuf = 00) or  
       (oeeh.orderno = 486673 and oeeh.ordersuf = 00) or  
       (oeeh.orderno = 450305 and oeeh.ordersuf = 00) or  
       (oeeh.orderno = 526976 and oeeh.ordersuf = 00) or  
       (oeeh.orderno = 418357 and oeeh.ordersuf = 01) or  
       (oeeh.orderno = 418357 and oeeh.ordersuf = 02) or  
       (oeeh.orderno = 418357 and oeeh.ordersuf = 03) or  
       (oeeh.orderno = 569549 and oeeh.ordersuf = 00) or  
       (oeeh.orderno = 476423 and oeeh.ordersuf = 00) then
       next.                                              
    */   

    assign nextfl = no.
    input  stream exclude_oeeh from "/home/das/pdxmg_exclude.dat".
    repeat:
       import stream exclude_oeeh delimiter ","
              ex-order ex-suffix.
/*       display ex-order ex-suffix. */
       if oeeh.orderno = ex-order and oeeh.ordersuf = ex-suffix then do:
          assign nextfl = yes.
          leave.
       end.
    end.
    input close. 
    
    if nextfl = yes then next.


    find arsc use-index k-arsc where arsc.cono   = 1 
                                 and arsc.custno = oeeh.custno
                                 no-lock no-error.
    if avail arsc then do:
       if arsc.pricetype < b-cprice-type or
          arsc.pricetype > e-cprice-type then
          next.
       find smsn use-index k-smsn where smsn.cono   = 1
                                    and smsn.slsrep = arsc.slsrepout
                                    no-lock no-error.
       if avail smsn then do:
          if smsn.mgr < b-region or
             smsn.mgr > e-region then
             next.
       end.
       else next.
    end.
    for each oeel use-index k-oeel where oeel.cono       = oeeh.cono
                                     and oeel.orderno    = oeeh.orderno
                                     and oeel.ordersuf   = oeeh.ordersuf
                                     and oeel.specnstype <> "l"
                                     and oeel.qtyship    > 0
                                     no-lock:
       find icsw use-index k-icsw where icsw.cono  = 1 
                                    and icsw.prod  = oeel.shipprod
                                    and icsw.whse  = oeeh.whse
                                    no-lock no-error.
       if avail icsw then do:
          if icsw.pricetype < b-pprice-type or
             icsw.pricetype > e-pprice-type then
             next.
          if p-kits = yes and oeel.kitfl = yes then
             next.
          if p-kits = yes and icsw.arptype = "f" then
             next.
          find b-icsp use-index k-icsp where b-icsp.cono = g-cono and
                                             b-icsp.prod = icsw.prod
                                             no-lock no-error.
          if avail b-icsp then
             if b-icsp.statustype = "l"    /* labor */
                then next.
               
          
       end.
       else next.
/*
   Go through the pecking order of PDSC Records.
   If a Type 1 or Type 2 PDSC record is found, a Matrix Record is not created 
*/
       find first     /* Type 1 PDSC Record */
            pdsc use-index k-custprod where pdsc.cono       = oeeh.cono
                                        and pdsc.statustype = yes
                                        and pdsc.levelcd    = 1
                                        and pdsc.custno     = oeeh.custno
                                        and pdsc.prod       = oeel.shipprod
                                        and (pdsc.whse      = oeel.whse
                                         or  pdsc.whse      = "") 
                                        and (pdsc.enddt     = ?
                                         or  pdsc.enddt    >= oeeh.invoicedt) 
                                        no-lock no-error.
       if not avail pdsc then do:
          assign type2 = no.
          for each   /* Type 2 PDSC Record */
            pdsc use-index k-custprod where pdsc.cono       = oeeh.cono
                                        and pdsc.statustype = yes
                                        and pdsc.levelcd    = 2
                                        and pdsc.custno     = oeeh.custno
                                        and (pdsc.enddt     = ?
                                         or  pdsc.enddt    >= oeeh.invoicedt)
                                        no-lock:
             if substring(pdsc.prod,1,1) = "p" and        /* price type */
                substring(pdsc.prod,3,4) = icsw.pricetype then do:
                assign type2 = yes.
                leave.
             end.
             else do:
                if substring(pdsc.prod,1,1)  = "l" and    /* product line */
                   substring(pdsc.prod,15,6) = icsw.prodline then do:
                   assign type2 = yes.
                   leave.
                end.
                else do:
                   if substring(pdsc.prod,1,1) = "c" then do: /* product cat */
                      find icsp use-index k-icsp where icsp.cono = oeeh.cono
                                                   and icsp.prod = icsw.prod
                                                   no-lock no-error.
                      if avail icsp and substring(pdsc.prod,3,4) = icsp.prodcat
                         then do:
                           assign type2 = yes.
                           leave.
                      end.
                   end.
                end.
             end.
          end.
          if type2 then next.
          else do:
             find first
                  pdsc use-index k-ctprod where pdsc.cono       = oeeh.cono
                                          and pdsc.statustype = yes
                                          and pdsc.levelcd    = 4
                                          and pdsc.custtype   = arsc.pricetype
                                          and pdsc.prod       = icsw.pricetype
                                          and (pdsc.whse      = oeel.whse 
                                           or  pdsc.whse      = "")
                                          and (pdsc.enddt     = ?
                                           or  pdsc.enddt    >= oeeh.invoicedt)
                                          no-lock no-error.
             if avail pdsc and p-nonmatrix = no then do:

                assign t-quantity = 0.
                run "get-total-quantity".
                
                matrix-price = 0.
                if pdsc.qtybrk[1] > 0 then do:
                   do y = 1 to 8:
                      if t-quantity < pdsc.qtybrk[y] then do:
                         if pdsc.prctype = yes
                            then matrix-price = pdsc.prcmult[y].
                         if pdsc.prctype = no and pdsc.qtybreakty = "d"
                            then matrix-price = pdsc.prcdisc[y].
                         if pdsc.prctype = no and pdsc.qtybreakty = "p"
                            then matrix-price = pdsc.prcmult[y].
                         leave.
                      end.
                      if pdsc.qtybrk[y] = 0 and y > 1 and matrix-price = 0
                         then do:
                         if pdsc.prctype = yes 
                            then matrix-price = pdsc.prcmult[y].
                         if pdsc.prctype = no and pdsc.qtybreakty = "d"
                            then matrix-price = pdsc.prcdisc[y].
                         if pdsc.prctype = no and pdsc.qtybreakty = "p"
                            then matrix-price = pdsc.prcmult[y].
                         leave.
                      end.
                   end.
                end.
                else do:
                   if pdsc.prctype = yes
                       then matrix-price = pdsc.prcmult[1].
                   if pdsc.prctype = no and pdsc.qtybreakty = "d"
                      then matrix-price = pdsc.prcdisc[1].
                   if pdsc.prctype = no and pdsc.qtybreakty = "p"
                      then matrix-price = pdsc.prcmult[1].
                end.                                             

                if pdsc.priceonty = "l" then   /* discount on list price */
                   assign w-price = icsw.listprice.
                if pdsc.priceonty = "b" then   /* discount on base price */
                   assign w-price = icsw.baseprice.
                   
                if (oeel.returnfl = yes or oeel.transtype = "cr") then
                   assign w-qtyship = (oeel.qtyship * -1).
                else
                   assign w-qtyship = oeel.qtyship.
                   
                if p-replcst = yes then
                   assign w-cost = icsw.replcost.
                else
                   assign w-cost = oeel.prodcost.
                
                if (oeel.price * w-qtyship) <> 0 then do:
                    assign s-isales = (oeel.price * w-qtyship)
                           s-ytd-isales    = s-ytd-isales + 
                                                  (oeel.price * w-qtyship)
                           w-margin        = (oeel.price * w-qtyship) - 
                                                  (w-cost * w-qtyship)
                           s-imarg         = (w-margin / s-isales) * 100
                           s-ytd-imrgsales = s-ytd-imrgsales + w-margin.
                    
                    if pdsc.prctype = yes then do:
                       if matrix-price > 0 then
                          assign w-margin = (matrix-price * w-qtyship) -
                                            (w-cost * w-qtyship).
                       else
                          assign w-margin = (oeel.price * w-qtyship) -
                                            (w-cost * w-qtyship).
                       assign s-mmarg  = 
                                  (w-margin / (pdsc.prcmult[1] * w-qtyship))
                                                                        * 100
                              s-ytd-mmrgsales = s-ytd-mmrgsales + w-margin
                              s-ytd-msales =  s-ytd-msales +
                                              (pdsc.prcmult[1] * w-qtyship).
                    end.
                    
                    if pdsc.prctype = no and
                       pdsc.qtybreakty = "d" and w-price > 0.01 then do:
                       if matrix-price > 0 then do:
                          assign w-margin = 
                                         (((w-price * (100 - matrix-price)) 
                                                * w-qtyship) / 100) -
                                         (w-cost * w-qtyship)
                                 s-mmarg = (w-margin /
                                           (((w-price * (100 - matrix-price))
                                                * w-qtyship) / 100)) * 100
                                 s-ytd-mmrgsales = s-ytd-mmrgsales + w-margin
                                 s-ytd-msales    = s-ytd-msales +
                                          (((w-price * (100 - matrix-price)) *
                                             w-qtyship) / 100).
                       end.
                       else do:
                          assign w-margin        = (w-price *  w-qtyship) -
                                                   (w-cost * w-qtyship)
                                 s-mmarg         = (w-margin /
                                                   (w-price * w-qtyship)) * 100
                                 s-ytd-mmrgsales = s-ytd-mmrgsales + w-margin
                                 s-ytd-msales    = s-ytd-msales +
                                                   (w-price * w-qtyship).
                       end.
                    end.
                    
                    if pdsc.prctype = no and
                       pdsc.qtybreakty = "p" and w-price > 0.01 then do:
                       if matrix-price > 0 then do:
                          assign w-margin =   (((matrix-price / 100) 
                                                 * w-price) * w-qtyship)
                                              - (w-cost * oeel.qtyship)
                                 s-mmarg  = (w-margin /
                                            (((matrix-price / 100) * w-price) 
                                                          * w-qtyship)) * 100
                                 s-ytd-mmrgsales = s-ytd-mmrgsales + w-margin
                                 s-ytd-msales    = s-ytd-msales +
                                          (((matrix-price / 100) * w-price) *
                                            w-qtyship).
                       end.
                       else do:
                          assign w-margin = (w-price *  w-qtyship) - 
                                            (w-cost * w-qtyship)
                                 s-mmarg = (w-margin /
                                           (w-price * w-qtyship)) * 100
                                 s-ytd-mmrgsales = s-ytd-mmrgsales + w-margin
                                 s-ytd-msales    = s-ytd-msales + 
                                                   (w-price * w-qtyship).
                       end.

                    end.
                    
                    assign s-cprice = arsc.pricetype
                           s-pprice = icsw.pricetype
                           s-rgn    = smsn.mgr
                           s-custno = oeeh.custno
                           s-name   = arsc.name
                           s-order  = string(oeeh.orderno,">>>>>99") + "-" +
                                      string(oeeh.ordersuf,"99")
                           s-line   = string(oeel.lineno,">>9")
                           s-date   = string(oeeh.invoicedt,"99/99/99").
                           
                    if firstfl = yes then do:
                       display s-cprice
                               s-pprice
                               s-rgn
                               s-custno
                               s-name
                               s-order
                               s-line
                               s-date
                               s-isales
                               s-mmarg
                               s-imarg
                               with frame f-dd-detail.
                        down with frame f-dd-detail.
                        assign firstfl = no.
                     end.
                     else do:
                        display s-pprice
                                s-order
                                s-line
                                s-date
                                s-isales
                                s-mmarg
                                s-imarg
                                with frame f-dd-detail.
                        down with frame f-dd-detail.
                     end.
                           
                end.
             end.      /* pdsc type 4 record found */

             else do:

             if not avail pdsc and p-nonmatrix then do: 

                if (oeel.returnfl = yes or oeel.transtype = "cr") then
                   assign w-qtyship = (oeel.qtyship * -1).
                else
                   assign w-qtyship = oeel.qtyship.
                   
                if p-replcst = yes then
                   assign w-cost = icsw.replcost.
                else 
                   assign w-cost = oeel.prodcost.

                if (oeel.price * w-qtyship) <> 0 then do:
                    assign s-isales = oeel.price * w-qtyship
                           w-margin = (oeel.price * w-qtyship) -
                                      (w-cost * w-qtyship)
                           s-imarg  = (w-margin / s-isales) * 100
                    
                           s-ytd-isales    = s-ytd-isales +
                                             (w-cost * w-qtyship)
                           s-ytd-imrgsales = s-ytd-imrgsales + w-margin.

                    assign s-cprice = arsc.pricetype
                           s-pprice = icsw.pricetype
                           s-rgn    = smsn.mgr
                           s-custno = oeeh.custno
                           s-name   = arsc.name
                           s-order  = string(oeeh.orderno,">>>>>99") + "-" +
                                      string(oeeh.ordersuf,"99")
                           s-line   = string(oeel.lineno,">>9") 
                           s-date   = string(oeeh.invoicedt,"99/99/99").

                    if firstfl = yes then do:
                       display s-cprice
                               s-pprice
                               s-rgn
                               s-custno
                               s-name
                               s-order
                               s-line
                               s-date
                               s-isales
                               s-mmarg
                               s-imarg
                               with frame f-dd-detail.
                       down with frame f-dd-detail.
                       assign firstfl = no.
                    end.
                    else do:
                        display s-pprice
                                s-order
                                s-line
                                s-date
                                s-isales
                                s-mmarg
                                s-imarg
                        with frame f-dd-detail.
                        down with frame f-dd-detail.
                    end.
                end.
                end.
             end.      /* no pdsc record is found */
          end.
       end.
    end.
end.     /* oeeh */

end.     /* b = 1 to 2 */

assign w-year = string((cur-yr - 1),"9999").
if s-ytd-msales <> 0 then
   assign s-ytd-mmarg = (s-ytd-mmrgsales / s-ytd-msales) * 100.
if s-ytd-isales <> 0 then
   assign s-ytd-imarg = (s-ytd-imrgsales / s-ytd-isales) * 100.
   
display w-year
        s-ytd-isales
        s-ytd-mmarg
        s-ytd-imarg
       with frame f-dd-total.
       down with frame f-dd-total.

   
procedure get-total-quantity:

if oeel.transtype = "BR" then do:
   for each br-oeel use-index k-oeel where br-oeel.cono      = g-cono
                                       and br-oeel.orderno   = oeel.orderno
                                       and br-oeel.ordersuf  = 00
                                       and br-oeel.shipprod  = oeel.shipprod
                                       and br-oeel.transtype = "BL"
                                           no-lock:
       assign t-quantity = t-quantity + br-oeel.qtyord.
   end.
end.
else do:
   if oeel.ordersuf > 00 then do:
      for each bk-oeel use-index k-oeel where bk-oeel.cono     = g-cono
                                          and bk-oeel.orderno  = oeel.orderno
                                          and bk-oeel.ordersuf = 00
                                          and bk-oeel.shipprod = oeel.shipprod
                                          and bk-oeel.specnstype <> "l"
                                              no-lock:
                                              
          assign t-quantity = t-quantity + bk-oeel.qtyord.
      end.
   end.
   else do:
      if oeeh.totlineord > 1 then do:
         for each b-oeel use-index k-oeel where b-oeel.cono     = g-cono
                                            and b-oeel.orderno  = oeel.orderno
                                            and b-oeel.ordersuf = oeel.ordersuf
                                            and b-oeel.shipprod = oeel.shipprod
                                            and b-oeel.specnstype <> "l"
                                                no-lock:
                                                
             assign t-quantity = t-quantity + b-oeel.qtyord.
         end.
      end.
   end.
end.
end. /* procedure */


