/* vaexd.p */
/*******************************************************************************
  PROCEDURE    : vaexd.p
  DESCRIPTION  : Maintenance program to handle the generic/MFG description 
                 records used in the Vaexq.p entry program. Generic model
                 descriptions can be associated with a valid icsp part nbr. 
                 Data is stored in the icsec table with a type of "zx" and "zz".
  AUTHOR       : SunSource TA
  DATE WRITTEN : 06/15/06
  CHANGES MADE : ta001 - fixed the mfg/part# find's on zx recs - zx are only
                 for master mfg records going forward - part# recs have no 
                 master
*******************************************************************************/

{g-all.i}
{g-po.i}

def var h-title     like g-title                    no-undo.
def var v-model     as char   format "x(39)"        no-undo.
def var h-model     as char   format "x(39)"        no-undo.
def var s-prod      as char   format "x(24)"        no-undo.
def var h-unit      as char   format "x(24)"        no-undo.
def var h-chgdt     as date                         no-undo.
def var v-desc      as char format "x(50)"          no-undo.
def var set-desc    as char format "x(24)"          no-undo.
def var hld-desc    as char format "x(24)"          no-undo.
def var v-prod      like oeel.shipprod              no-undo.
def var v-vendno    like apss.vendno                no-undo.
def var v-shipfm    like apss.shipfmno              no-undo.
def var v-addr1     like  apss.name                 no-undo.
def var v-addr2     like  apss.name                 no-undo.
def var v-city      like  apss.city                 no-undo.
def var v-state     like  apss.state                no-undo.
def var v-zipcd     like  apss.zipcd                no-undo.
def var v-phone     like apss.phoneno               no-undo.
def var v-recid     as recid                        no-undo.
def var v-row       as int                          no-undo.
def var v-inq       as int                          no-undo.
def var v-rowid     as rowid                        no-undo.
def var q-rowid     as rowid                        no-undo.
def var lk-prod     like oeel.shipprod              no-undo. 
def new shared var v-copies  as int format "999"  init 1 no-undo.
def new shared var v-vendor  as logical             no-undo.
def var v-filename  as c format "x(30)"             no-undo.
def var v-tilda     as c format "x" init "~~"       no-undo.
def var s-printernm like sasp.printernm             no-undo.
def var v-reprint   as c format "x"  init "R"       no-undo.
def var firstcall   as logical                      no-undo.
def var callflag    as logical                      no-undo.
def var returnfl    as logical                      no-undo.
def var ctrlo-flag  as logical                      no-undo.
def var new-flag    as logical                      no-undo.
def var f8-flag     as logical                      no-undo.
def var fakemodel   as char                         no-undo.   
def var v-error     as char format "x(75)"          no-undo.   
def var h-type      as char format "x(9)"  dcolor 2 no-undo.    
def var x-literal   as char format "x(59)" dcolor 2 no-undo.   

define new shared temp-table models no-undo 
    field name      as char format "x(39)"
    field unitnm    as char format "x(20)" 
    field maker     as char format "xxxx"
    field transdt   as date
    field chgdt     as date 
    field descrip   as char format "x(30)"
    field whse      like swsst.whse 
    index k-model 
          name 
          unitnm.

define query q-models for models scrolling.
define browse b-models query q-models 
       display models.name   
           /*    models.unitnm  */
               models.maker
               models.transdt 
 with 13 down centered overlay no-hide no-labels
 title
"     MFG Specific Descriptions(f12-search) Oper  TransDt   ". 
       
assign x-literal =
"                   Up/down arrow to scroll                 ".
define frame f-models
  b-models                                                
  x-literal at 1
  with width 80 col 12 row 6 overlay no-hide no-labels no-box.   

define buffer g-icsec  for icsec.
define buffer bq-icsec  for icsec.
define buffer bz-icsec  for icsec.
define query  q-lookdesc for bq-icsec, bz-icsec scrolling.
define browse b-lookdesc query q-lookdesc
  display if not avail bz-icsec then 
             string(bq-icsec.altprod,"x(45)")
          else 
            (string(bq-icsec.altprod,"x(45)") + " " +  
             string(bz-icsec.prod,"x(24)"))
  format "x(74)" 
  with 12 down centered no-hide no-labels 
  title " " + h-type + 
        " -  Description                      Item Number                    ".

def var x-function as char format "x(78)"  dcolor 2 no-undo. 
assign x-function = 
"            F6-Add             F7- Change             F8-Delete            ".

form
 b-lookdesc
 x-function at 2 
with frame f-lookdesc no-labels overlay row 6 width 80 no-box.

form
   skip(1)
   v-model  at 12 label "  MFG Master "
   s-prod   at 12 label "    Part Nbr " 
   "      Ctrl-O : Generic Description Updates." at 12   
   with frame f-vaexd width 80 overlay side-labels title g-title.   

form
  "Search For: " at 1  
   lk-prod at 13
  with frame f-search  no-labels overlay row 7 column 22.

form
   skip(1)
   v-model  at 12 label "MFG Master  "
   s-prod   at 12 label "Part Nbr.   "      
   v-desc   at 12 label "Description "
   with frame f-vaexd-add width 80 side-labels overlay 
        title "   Add Description Master Record   ".   
    
form
   skip(1)
   v-model  at 12 label "MFG Master  "
   s-prod   at 12 label "Part Nbr.   "  
   v-desc   at 12 label "Description "
   v-prod   at 12 label "Item Nbr.   " 
   {f-help.i}
   skip(1) 
   with frame f-vaexd-chg width 80 side-labels overlay 
        title  " MFG Description Maintenance ".   

def buffer zxt2-sasos for sasos. 
find first zxt2-sasos where
           zxt2-sasos.cono  = g-cono and
           zxt2-sasos.oper2 = g-operinits and
           zxt2-sasos.menuproc = "zxt2"
           no-lock no-error.

on any-key of b-models 
do: 
   if {k-cancel.i} or {k-jump.i} then 
   do: 
      hide frame f-models.
      hide frame f-search. 
      apply "window-close" to current-window.  
      on cursor-up back-tab. 
      on cursor-up tab. 
      leave. 
   end. 
   if lastkey = 401 or lastkey = 13 then 
   do:  
      assign v-model = if avail models then 
                          trim(models.name)
                       else 
                          v-model
             s-prod  = if avail models then 
                          trim(models.unitnm)
                       else 
                          s-prod. 
      hide frame f-models.
      hide frame f-search.
      apply "window-close" to current-window.  
      on cursor-up back-tab.
      on cursor-down tab.
   end.
end.

on f12 of v-model in frame f-vaexd                    
do:
  def buffer zz-models for models. 
  def var h-model as char format "x(24)" no-undo. 
  for each models exclusive-lock: 
    delete models. 
  end.  
  /*** &&& see if cross ref master descrip is available ***/
  for each icsec use-index k-icsec where 
           icsec.cono    = g-cono   and 
           icsec.rectype = "zm"     and 
           icsec.prod ne ""        
           no-lock: 
    find first zz-models where 
               zz-models.name = icsec.prod  
               no-lock no-error. 
    if not avail zz-models and avail icsec then do:            
    /*  run getchgdt.  */
     if icsec.prod = "" then 
      next.
     create models. 
     assign models.name    = icsec.prod 
            models.maker   = icsec.operinit
            models.transdt = icsec.transdt 
            models.descrip = icsec.altprod
            models.transdt = if icsec.transdt ne ? then 
                              icsec.transdt
                             else 
                              ?
            h-model        = models.name.
    end.
  end. 
  open query q-models preselect each models.
  enable b-models with frame f-models.
  apply "entry" to b-models in frame f-models.
  apply "focus" to b-models in frame f-models.
  display b-models x-literal with frame f-models. 
  assign h-model = ""
         h-unit  = "".
  on cursor-up cursor-up.
  on cursor-down cursor-down.
  wait-for "window-close" of current-window. 
  on cursor-up back-tab.
  on cursor-down tab.
  close query q-models.
  display v-model s-prod with frame f-vaexd.
  on cursor-up back-tab. 
  on cursor-up tab. 
end. 

on f12 of b-models in frame f-models  
do:
   if frame-name   = "f-models" then     
   do:  
    enable b-models with frame f-models.
    apply "focus" to b-models in frame f-models. 
    assign lk-prod = "".
    update lk-prod with frame f-search.
    find first models where 
         models.name begins lk-prod
         no-lock no-error.
    if avail models then do: 
      assign q-rowid = rowid(models).
     /*
      if q-rowid ne ? then 
       reposition q-models to rowid(q-rowid).
     */
      get current q-models. 
      display b-models with frame f-models.
    end.
   hide frame f-search.
   end. 
end.  

procedure getchgdt: 
do: 
   define buffer z-icsec for icsec.  
   define var h-chgdt as date. 
   assign h-chgdt = ?.
   for each z-icsec where  
            z-icsec.cono = g-cono  and 
            z-icsec.rectype = "zm" and 
            z-icsec.prod  ne ""    and 
            z-icsec.prod = h-model
            exclusive-lock: 
    if z-icsec.lastchgdt ne ? then 
     if h-chgdt = ? then 
        assign h-chgdt = z-icsec.lastchgdt. 
     else 
     if z-icsec.lastchgdt > h-chgdt then 
        assign h-chgdt = z-icsec.lastchgdt. 
   end.
   find models where models.name = h-model no-lock no-error.
   if avail models and h-chgdt ne ? then 
      assign models.chgdt = h-chgdt. 
end.                      
end. 

on any-key of b-lookdesc 
do:
   if {k-cancel.i} or {k-jump.i} then do:
     hide frame f-vaexd.     
     hide frame f-lookdesc.  
     hide frame f-vaexd-add. 
     hide frame f-vaexd-chg.
     hide frame f-search. 
     if not ctrlo-flag then
      apply "window-close" to current-window. 
     on cursor-up back-tab. 
     on cursor-up tab. 
     assign h-type = "  Generic".
   end.
   if lastkey = 13 then 
    next.
   assign v-model = if h-title ne "" and g-callproc = "vaexq" then             
                       trim(substring(h-title,1,39)) 
                    else 
                       trim(v-model)
          s-prod  = if h-title ne "" and g-callproc = "vaexq" then             
                       trim(substring(h-title,40,24)) 
                    else if avail bz-icsec then 
                       trim(bz-icsec.user3) 
                    else    
                       s-prod
          v-prod  = if avail bz-icsec then 
                       bz-icsec.prod
                    else 
                        ""
          v-desc  = if avail bq-icsec then 
                       bq-icsec.altprod
                    else 
                       ""
          hld-desc = v-desc
          returnfl = no
          f8-flag  = no 
          v-row    = b-lookdesc:focused-row
          v-rowid  = if avail bq-icsec then 
                        rowid(bq-icsec)
                     else 
                        ?.
  if lastkey = 13 or lastkey = 401 and callflag then do: 
   apply "window-close" to current-window. 
   assign g-title = "" 
          returnfl = yes            
          substring(g-title,1,39) = if avail bz-icsec and
                                      entry(2,bz-icsec.altprod,"^") ne " " then 
                                      entry(2,bz-icsec.altprod,"^")
                                    else 
                                    if avail bq-icsec then 
                                      bq-icsec.altprod
                                    else
                                      "".  
   hide frame f-lookdesc.
   leave.
  end.
  else 
   assign returnfl = no.
  assign x-function = 
 "            F6-Add             F7-Change              F8-Delete            ".
  display x-function with frame f-lookdesc. 
  if {k-cancel.i} or {k-jump.i} then do: 
    hide frame f-vaexd. 
    hide frame f-lookdesc.    
    hide frame f-vaexd-add. 
    hide frame f-vaexd-chg.
    if not ctrlo-flag then
     apply "window-close" to current-window. 
    on cursor-up back-tab. 
    on cursor-up tab. 
    assign h-type = "  Generic".
   end.
   else
   if {k-func6.i} and not ctrlo-flag then do: 
    if not avail zxt2-sasos  or 
      (avail zxt2-sasos and zxt2-sasos.securcd[5] < 3) then do:
     assign v-error = "--> Invalid security - Please contact supervisor". 
     run zsdierrx.p(v-error,"yes"). 
    end.
    assign x-function = 
"            F6-ADD             F7-Change              F8-Delete            ".
    display x-function with frame  f-lookdesc. 
    message "  Create new MFG description?  "
    skip  
            "  Please enter a description.  "
    skip 
    view-as alert-box question buttons yes-no
    update ru as logical.
    if ru then do: 
     /*** &&& see if cross ref master descrip is available ***/
     find first icsec use-index k-prod where 
                icsec.cono    = g-cono  and 
                icsec.prod    = trim(v-model) + "^" and  
                icsec.rectype = "zx"    and 
                icsec.altprod ne ""      
                no-lock no-error.
     assign v-desc   = ""
            new-flag = if avail icsec then 
                          no 
                       else 
                          yes. 
     hide frame f-vaexd-chg.
     display v-model s-prod with frame f-vaexd-add.
     update v-desc 
            with frame f-vaexd-add.
     run add-item. 
     if v-desc = "" then 
      return.
     hide frame f-vaexd-add.
     close query q-lookdesc. 
     run openinquirym.
     assign x-function = 
"            F6-Add             F7-Change              F8-Delete            ".
     display x-function with frame f-lookdesc. 
     next.
    end.
   end. 
   else 
   if {k-func6.i} and ctrlo-flag then do: 
    if not avail zxt2-sasos  or 
      (avail zxt2-sasos and zxt2-sasos.securcd[5] < 3) then do:
     assign v-error = "--> Invalid security - Please contact supervisor". 
     run zsdierrx.p(v-error,"yes"). 
    end.
    assign x-function = 
"            F6-ADD             F7-Change              F8-Delete            ".
    display x-function with frame  f-lookdesc. 
    message "  Create generic description?  "
    skip  
            "  Do you have valid security?  "
    skip 
    view-as alert-box question buttons yes-no
    update jj as logical.
    if jj then do: 
     assign v-desc  = ""
            s-prod  = "" 
            v-model = "" 
            h-type  = "  Generic".
     hide frame f-vaexd-chg.
     display v-model s-prod v-desc with frame f-vaexd-add.
     update v-desc 
            with frame f-vaexd-add.
     run add-item. 
     if v-desc = "" then 
      return.
     hide frame f-vaexd-add.
     close query q-lookdesc. 
     run openinquiryg.
     find icsec use-index k-prod where 
          icsec.cono    = g-cono and 
          icsec.prod    = " "    and  
          icsec.rectype = "zx"   and 
          icsec.altprod = v-desc       
          no-lock no-error.
     if avail icsec then do: 
      assign v-rowid = rowid(icsec).
      if v-rowid ne ? then do: 
       b-lookdesc:refresh() in frame f-lookdesc. 
       reposition q-lookdesc to rowid(v-rowid). 
      end.
     end.  
     assign x-function = 
"            F6-Add             F7-Change              F8-Delete            ".
     display x-function with frame f-lookdesc. 
    end.
    else do: 
     assign x-function = 
"            F6-Add             F7-Change              F8-Delete            ".
     display x-function with frame f-lookdesc. 
     next. 
    end.
   end. 
   else
   if {k-func7.i} then do: 
    if not avail zxt2-sasos  or 
      (avail zxt2-sasos and zxt2-sasos.securcd[5] < 3) then do:
     assign v-error = "--> Invalid security - Please contact supervisor". 
     run zsdierrx.p(v-error,"yes"). 
    end.
    hide frame f-vaexd-add. 
    assign x-function = 
 "            F6-Add             F7-CHANGE              F8-Delete            ".
    display x-function with frame  f-lookdesc. 
    run change-item. 
    assign x-function = 
 "            F6-Add             F7-Change              F8-Delete            ".
    display x-function with frame f-lookdesc. 
    hide frame f-vaexd-chg.
    if not avail bz-icsec then do: 
     close query q-lookdesc.  
     if not ctrlo-flag then  
      run openinquirym.   
     else 
      run openinquiryg.
     if v-rowid ne ? then do:  
      b-lookdesc:refresh() in frame f-lookdesc. 
      reposition q-lookdesc to rowid(v-rowid).
     end.
    end.
   end.   
   else
   if {k-func8.i} then do: 
    def var mfgtodelete as char format "x(39)" no-undo. 
    if not avail zxt2-sasos  or 
      (avail zxt2-sasos and zxt2-sasos.securcd[5] < 4) then do:
     assign v-error = "--> Invalid security - Please contact supervisor". 
     run zsdierrx.p(v-error,"yes"). 
    end.
    define buffer dz-icsec for icsec. 
    define buffer nn-icsec for icsec. 
    define buffer zm-icsec for icsec. 
    assign x-function = 
"            F6-Add             F7-Change              F8-DELETE            ".
    display x-function with frame  f-lookdesc. 
    message "  Delete master description  "
    skip  
            "  and associated products?   "
    skip 
    view-as alert-box question buttons yes-no
    update rd as logical.
    if not rd then 
     assign rd = rd.
    else  
    if rd then do: 
     find first dz-icsec where recid(dz-icsec) = recid(bz-icsec)
          exclusive-lock no-error.
     /** delete part association first **/
     if avail dz-icsec then  
      delete dz-icsec.
     define buffer dx-icsec for icsec. 
     find first dx-icsec where recid(dx-icsec) = recid(bq-icsec)
          exclusive-lock no-error.
     if avail dx-icsec then do: 
      assign mfgtodelete = dx-icsec.prod. 
      run deletemstr(input recid(dx-icsec)).
      b-lookdesc:delete-current-row().
      if avail dx-icsec then do: 
       delete dx-icsec.
       find first nn-icsec use-index k-prod where 
                  nn-icsec.cono     = g-cono      and 
                  nn-icsec.prod     = mfgtodelete and   
                  nn-icsec.rectype  = "zx"           
       no-lock no-error. 
       if not avail nn-icsec then do:              
        find first zm-icsec use-index k-icsec where 
                   zm-icsec.cono     = g-cono        and 
                   zm-icsec.rectype  = "zm"          and 
                   zm-icsec.prod     = mfgtodelete  
        exclusive-lock no-error. 
        delete zm-icsec.
       end. 
      end.
     end.
     assign x-function = 
"            F6-Add             F7-Change              F8-Delete            ".
     display b-lookdesc x-function with frame f-lookdesc.
     next.
    end. /* if f8 */
    /*** &&& see if cross ref master descrip is available ***/
    find first icsec use-index k-prod where 
               icsec.cono    = g-cono  and 
               icsec.prod    = trim(v-model) + "^" and  
               icsec.rectype = "zx"    and 
               icsec.altprod ne ""      
               no-lock no-error.
    if avail icsec or {k-func8.i} then do: 
     display b-lookdesc x-function with frame f-lookdesc.
    end.
    else do: 
     assign g-title = "".
     close query q-lookdesc. 
     display v-model s-prod with frame f-vaexd.
    end. 
   end. 
 end.

on ctrl-o of v-model in frame f-vaexd
do:
   message "  Create Generic description?  "
   skip  
           "  Do you have valid security?  "
   skip 
   view-as alert-box question buttons yes-no
   update rr as logical.
   if rr then do: 
    if not avail zxt2-sasos  or 
      (avail zxt2-sasos and zxt2-sasos.securcd[5] < 3) then do:
     assign v-error = "--> Invalid security - Please contact supervisor". 
     run zsdierrx.p(v-error,"yes"). 
     next.
    end.
    else do: 
    find first bq-icsec use-index k-prod where
               bq-icsec.cono = g-cono  and
               bq-icsec.prod = " "     and 
               bq-icsec.rectype = "zx" and 
               bq-icsec.altprod ne ""  
               no-lock no-error.
     assign v-model = ""
            s-prod  = ""   
            v-desc  = ""
            ctrlo-flag = yes
            h-type  = "  Generic"
            v-rowid = if avail bq-icsec then 
                       rowid(bq-icsec) 
                    else 
                       ?.
     display v-model s-prod with frame f-vaexd.
     apply "window-close" to current-window. 
     assign  h-type  = "  Generic".
     run openinquiryg. 
    end. 
   end.
end.   


if g-callproc = "vaexq" then do: 
 assign substring(h-title,1,39)  = substring(g-title,1,39)
        substring(h-title,40,24) = substring(g-title,40,24)
        v-model   = trim(substring(g-title,1,39))
        s-prod    = trim(substring(g-title,40,24))
        set-desc  = trim(substring(g-title,70,24)) 
        callflag  = yes
        firstcall = yes.
end.             
else 
 assign callflag  = no
        firstcall = no.         

mainzx:
do while not returnfl on endkey undo, leave mainzx: 
  if not callflag then do: 
   display v-model s-prod with frame f-vaexd.
   update v-model 
          s-prod
          with frame f-vaexd.
   if s-prod ne " " then do: 
    assign v-error =
    " Note: Items will be asscoiated with repair Part# and not MFG master. ". 
    run zsdi_vaerrx.p(v-error,"yes"). 
   end.
  end.
  else do: 
    assign v-model = trim(substring(h-title,1,39))
           s-prod  = trim(substring(h-title,40,24)).
    if s-prod ne " " then do: 
     assign v-error =
     " Note: Items will be asscoiated with repair Part# and not MFG master. ". 
     run zsdi_vaerrx.p(v-error,"yes"). 
    end.
  end. 
  do while true on endkey undo, leave:
   if callflag and lastkey = 404 then do: 
    assign returnfl = yes.
    leave. 
   end.
   else 
   if callflag and
    (lastkey = 401 or lastkey = 13 or
     lastkey = 311 or lastkey = 404) then do: 
    assign returnfl = yes
           substring(g-title,70,24) = if hld-desc ne " " then 
                                       hld-desc
                                      else  
                                       substring(g-title,70,24).
    leave.
   end. 
   assign x-function = 
"            F6-Add             F7-Change              F8-Delete            ".
   display x-function with frame f-lookdesc. 
   if ctrlo-flag and (lastkey = 401 or lastkey = 13) then do: 
    assign ctrlo-flag = no.
    readkey pause 0.
    leave.
   end.
   assign ctrlo-flag = no.
   /*** &&& is cross ref model specific master descrip available ***/
   find first icsec use-index k-prod where 
              icsec.cono    = g-cono  and 
              icsec.prod    = trim(v-model) + "^" and 
              icsec.rectype = "zx"    and 
              icsec.altprod ne ""     
              no-lock no-error.
   if (not avail icsec and callflag = yes) then do: 
     assign v-error = 
" Cannot create description masters from Vaexq(Ctrl-j)-Pls contact supervisor".      run zsdierrx.p(v-error,"yes"). 
     pause 3.
    assign returnfl = yes.
    leave.
   end.
   if not avail icsec and not ctrlo-flag then do:
    hide frame f-lookdesc.
    if ((lastkey = 13 or lastkey = 401) and not ctrlo-flag) or 
        (not avail icsec and not {k-cancel.i}) then do: 
      if not avail zxt2-sasos  or 
        (avail zxt2-sasos and zxt2-sasos.securcd[5] < 3) then do:
       assign v-error = "--> Invalid security - Please contact supervisor". 
       run zsdierrx.p(v-error,"yes"). 
       next.
      end.
      message "  Create new MFG description?  "
      skip  
              "  Please enter a description.  "
      skip 
      view-as alert-box question buttons yes-no
      update rr as logical.
      if rr then do: 
       assign v-desc  = ""
              v-model = if callflag then
                         trim(substring(h-title,1,39))
                        else 
                         v-model 
              s-prod  = if callflag then 
                         " "
                        else 
                         trim(s-prod).
       if v-model = " " then do: 
        assign v-error =
         " Cannot create blank masters descriptions - Pls enter a MFG master".
         run zsdierrx.p(v-error,"yes"). 
        leave.
       end. 
       display v-model s-prod with frame f-vaexd-add.
       updt1:
       do while true on endkey undo, leave updt1:
       update v-desc 
              with frame f-vaexd-add.
        if v-desc = "" then  
         next mainzx.
        else 
         leave updt1.
       end.   
       if avail icsec then 
        assign v-row = b-lookdesc:focused-row.
       else 
        assign v-row   = 1.
       run add-item. 
       /*** &&& first create cross ref model specific master  ***/
       find first icsec use-index k-prod where 
                  icsec.cono    = g-cono  and 
                  icsec.prod    = trim(v-model) + "^" and  
                  icsec.rectype = "zx"    and 
                  icsec.altprod ne ""     
                  no-lock no-error.
       hide frame f-vaexd-add.
       assign x-function = 
 "            F6-Add             F7-Change              F8-Delete            ".
       display x-function with frame f-lookdesc. 
      end.
      else /* if rr  */
       leave.
   end. /* if go or return  */
   if (lastkey = 13 or lastkey = 401) and ctrlo-flag then do: 
      message " Create new generic description? "
      skip  
              "  Please enter a description.    "
      skip 
      view-as alert-box question buttons yes-no                           
      update jr as logical.
      if jr then do: 
       assign v-desc  = ""
              s-prod  = ""
              v-model = "" .
       display v-model s-prod v-desc with frame f-vaexd-add.
       update v-desc 
              with frame f-vaexd-add.
       assign v-row = b-lookdesc:focused-row.
       assign v-rowid  = rowid(bq-icsec). 
       run add-item. 
       hide frame f-vaexd-add.
       assign x-function = 
 "            F6-Add             F7-Change              F8-Delete            ".
        display x-function with frame f-lookdesc. 
       close query q-lookdesc. 
       run openinquiryg.
       if v-rowid ne ? then do:  
        b-lookdesc:refresh() in frame f-lookdesc. 
        reposition q-lookdesc to rowid(v-rowid).
       end.
      end.
      else 
       leave.
    end.
   end.
   if avail icsec then 
    assign fakemodel = right-trim(v-model). 
   else 
    assign fakemodel = "".
   run openinquirym. 
   wait-for window-close of current-window. 
   close query q-lookdesc. 
  end. /* do while true */
  on cursor-down back-tab. 
  on cursor-up tab. 
  hide frame f-vaexd.
  hide frame f-lookdesc.
  hide frame f-vaexd-add. 
  hide frame f-vaexd-chg. 
end. /* do while true */

hide frame f-vaexd.
on cursor-up back-tab. 
on cursor-up tab. 
apply "window-close" to current-window.
return.

procedure add-item: 
define buffer z-icsec for icsec.
define buffer l-icsec for icsec.
define buffer x-icsec for icsec. 
define buffer n-icsec for icsec.
define buffer om-icsec for icsec. 

define var h-user3 as char format "x(24)" no-undo. 

/* if mfg master descrip already exists - do not make another */
find first x-icsec use-index k-prod where 
           x-icsec.cono    = g-cono           and 
           x-icsec.prod    = trim(v-model) + "^" and  
           x-icsec.rectype = "zx"             and 
           x-icsec.altprod = trim(v-desc," ")    
           no-lock no-error.
if avail x-icsec then do: 
  leave.
end. 
else do:
 /*** &&& is cross ref model specific master descrip available ***/
 find first om-icsec use-index k-prod where 
            om-icsec.cono    = g-cono  and 
            om-icsec.prod    = trim(v-model) + "^" and 
            om-icsec.rectype = "zx"    and 
            om-icsec.altprod ne ""      
            no-lock no-error.
 if not avail om-icsec then do:
  assign v-rowid = ?
         v-row   = 1.
 end.
end.  

find last l-icsec use-index k-keyno where
          l-icsec.cono = g-cono and 
          l-icsec.rectype = "zx"
          no-lock no-error.
create z-icsec.
assign z-icsec.cono = g-cono
       z-icsec.altprod = trim(v-desc," ")  
       /* ta001  still need to create the master mfg record here */ 
       z-icsec.prod    = if ctrlo-flag then 
                          " " 
                          else 
                         trim(v-model) + "^"  
       z-icsec.user3   =  " "   
       z-icsec.rectype = "zx"
       z-icsec.keyno   = if avail l-icsec then 
                            l-icsec.keyno + 1
                         else 
                            1
       z-icsec.operinit  = g-operinits
       z-icsec.transdt   = today
       z-icsec.lastchgdt = today.
 
run make-zm-record. 
 
b-lookdesc:refresh() in frame f-lookdesc.
assign v-rowid  = rowid(z-icsec).

if ctrlo-flag then
 leave.

/** If a new descrip is added for part specific - all other part specific need 
    to have added, so the vaexq browse will reflect master descrips **/ 

/** for each not needed anymore - mfg/unit zx type recs discontinued **
for each n-icsec use-index k-icsec where 
         n-icsec.cono     = g-cono and 
         n-icsec.rectype  = "zx"   and 
         n-icsec.prod     begins (trim(v-model) + "^") and  
         n-icsec.prod     ne z-icsec.prod  
         no-lock:

 /* if descrip already exists - do not make another */
 find first x-icsec use-index k-icsec where 
            x-icsec.cono    = g-cono            and 
            x-icsec.rectype = "zx"              and 
            x-icsec.prod    = n-icsec.prod      and 
            x-icsec.altprod = z-icsec.altprod
            no-lock no-error.
 if avail x-icsec then 
  next.

 /*** create a master descrip record if a part specific descrip is added ***/
 find last l-icsec use-index k-keyno where
           l-icsec.cono = g-cono and 
           l-icsec.rectype = "zx"
           no-lock no-error.
 create icsec.
 assign icsec.cono = g-cono
        icsec.altprod = trim(v-desc," ")  
        icsec.prod    = trim(v-model) + "^" + trim(n-icsec.user3)
        icsec.user3   = trim(n-icsec.user3) 
        icsec.rectype = "zx"
        icsec.keyno   = if avail l-icsec then 
                             l-icsec.keyno + 1
                          else 
                             1
        icsec.operinit  = g-operinits
        icsec.transdt   = today
        icsec.lastchgdt = today.
end. /* for each */
***** for each not needed anymore - mfg/unit zx type recs discontinued **/

if avail z-icsec then 
 release z-icsec. 
if avail x-icsec then 
 release x-icsec. 
if avail n-icsec then 
 release n-icsec. 

if new-flag then 
 apply "window-close" to current-window.

leave.
end.

procedure change-item: 
  define buffer x-icsec for icsec.
  define buffer z-icsec for icsec.
  define buffer l-icsec for icsec.
  /* ta001 */
  /* this zx type rec is discontinued 07/09/08 **************
  find first icsec use-index k-icsec where 
             icsec.cono    = g-cono           and 
             icsec.rectype = "zx"             and 
             icsec.altprod = bq-icsec.altprod and 
             icsec.prod    = right-trim(v-model) + "^" + 
                             right-trim(s-prod) 
             no-lock no-error.
  if not avail icsec then
     assign v-model = "".
  ***********************************************************/
  
  find x-icsec use-index k-prod where 
       x-icsec.cono    = g-cono           and 
       x-icsec.prod    = bq-icsec.prod    and  
       x-icsec.rectype = "zx"             and 
       x-icsec.altprod = bq-icsec.altprod  
       exclusive-lock no-error.
  if not avail x-icsec then
   leave.
  else 
   assign v-rowid = rowid(x-icsec).
  if avail bz-icsec then do: 
   find first z-icsec where recid(z-icsec) = recid(bz-icsec)
        no-lock no-error.
   assign v-prod  = z-icsec.prod.
          v-rowid = rowid(z-icsec).  
  end.
  else 
   assign v-prod = "". 
  assign v-desc = bq-icsec.altprod.
  display v-model s-prod v-prod v-desc with frame f-vaexd-chg.         
  updtloop:
  repeat:
  update v-prod
         with frame f-vaexd-chg.
  if {k-cancel.i} then do: 
   hide frame f-vaexd-chg. 
   return.
  end.
  if {k-accept.i} and v-prod ne "" then do:  
   {w-icsp.i v-prod}
   if not avail icsp then do: 
    assign v-error = "--> " + v-prod + 
                     " Invalid product - Please enter valid icsp.". 
    run zsdierrx.p(v-error,"yes"). 
    next-prompt v-prod with frame f-vaexd-chg.
    next. 
   end.
  end.   
  if {k-accept.i} then do:  
   assign fakemodel = v-model.
   find first z-icsec where recid(z-icsec) = recid(bz-icsec)
        exclusive-lock no-error.
   if avail bz-icsec and 
   (v-prod ne bz-icsec.prod) then do:
      if avail z-icsec and v-prod ne ""  then do: 
       assign z-icsec.prod = v-prod
              z-icsec.operinit   = g-operinits
              z-icsec.lastchgdt  = today.
       if avail x-icsec then 
        assign x-icsec.lastchgdt = today
               x-icsec.operinit  = g-operinits.
       end.
      else 
      if avail z-icsec and v-prod = "" then do:
       /** need to assign v-rowid of master rec here **/
       assign v-rowid = if avail bq-icsec then 
                         rowid(bq-icsec) 
                        else 
                         v-rowid. 
       delete z-icsec.
       if avail x-icsec then 
         assign x-icsec.lastchgdt = today
                x-icsec.operinit  = g-operinits.
      end.
   end.
   else 
   if not avail bz-icsec and v-prod ne "" then do:
    /* if prod was assigned to another desc rec-change or add */
    find last l-icsec use-index k-keyno where
              l-icsec.cono = g-cono and 
              l-icsec.rectype = "zz"
              no-lock no-error.
    create z-icsec.
    assign z-icsec.cono = g-cono
           z-icsec.altprod = if v-model ne "" then  
                              v-model + "^" + trim(v-desc," ")  
                             else 
                              "^" + trim(v-desc," ")  
           z-icsec.prod    = v-prod 
           z-icsec.user3   = s-prod
           z-icsec.rectype = "zz"
           z-icsec.keyno   = if avail l-icsec then 
                                l-icsec.keyno + 1
                              else 
                                1
           z-icsec.operinit  = g-operinits
           z-icsec.lastchgdt = today.
    if avail x-icsec then 
      assign x-icsec.lastchgdt = today
             x-icsec.operinit  = g-operinits.
   end. 
   if v-desc ne bq-icsec.altprod then do: 
      assign x-icsec.altprod = v-desc.
      if avail z-icsec then
         assign z-icsec.altprod   =  v-model + "^" + trim(v-desc," ")
                z-icsec.operinit  = g-operinits
                z-icsec.lastchgdt = today.
   end. 
   hide frame f-vaexd-chg.
   on cursor-up cursor-up.   
   on cursor-down cursor-down.
   leave.
  end. 
  end.    
end. 

procedure deletemstr:
 define input parameter d-recid as recid no-undo.  
 define buffer d-icsec for icsec.
 define buffer n-icsec for icsec.
 define buffer z-icsec for icsec.
 define buffer nn-icsec for icsec.
 define buffer zm-icsec for icsec.

 find first d-icsec where recid(d-icsec) = d-recid
   no-lock no-error.

 if not avail d-icsec then 
  leave.
/** If a new descrip is added for part specific - all other part specific need 
    to have added, so the vaexq browse will reflect master descrips **/ 
for each n-icsec use-index k-icsec where 
         n-icsec.cono     = g-cono          and 
         n-icsec.rectype  = "zx"            and 
         n-icsec.prod     = d-icsec.prod    and  
         n-icsec.altprod  = d-icsec.altprod   
         exclusive-lock:

   find first z-icsec where 
              z-icsec.cono    = g-cono and 
              z-icsec.rectype = "zz"   and 
              z-icsec.altprod = right-trim(v-model) + "^" + 
                                right-trim(d-icsec.altprod) and 
              z-icsec.user3 = s-prod
      exclusive-lock no-error. 
   if avail z-icsec then 
    delete z-icsec. 
   delete n-icsec.  
end. /* for each */

end.

procedure openinquirym: 
do: 
    assign x-function = 
"            F6-Add             F7-Change              F8-Delete            ".
    display x-function with frame f-lookdesc. 
    define buffer om-icsec for icsec. 
    assign h-type = if ctrlo-flag then 
                     "  Generic" 
                    else  
                     "MfgMaster".
    b-lookdesc:title in frame f-lookdesc = " " + h-type +   
      " -  Description                      Item Number                    ". 
    /*** mfg master ne " " specific query  ***/
    open query q-lookdesc 
     for each bq-icsec use-index k-prod where
              bq-icsec.cono = g-cono       and 
              /* ta001  */ 
              bq-icsec.prod    = trim(fakemodel) + "^" and 
              bq-icsec.rectype = "zx"      and 
              bq-icsec.altprod ne ""        
              no-lock,
        first bz-icsec outer-join where 
              bz-icsec.cono    = g-cono           and 
              bz-icsec.rectype = "zz"             and 
              bz-icsec.altprod = trim(fakemodel) + "^" +
                                  bq-icsec.altprod  and 
              bz-icsec.user3   = trim(s-prod)
         no-lock
         by bq-icsec.altprod.
    on cursor-up cursor-up.   
    on cursor-down cursor-down.
    enable b-lookdesc  with frame f-lookdesc.
    apply "focus" to b-lookdesc in frame f-lookdesc.
    apply "entry" to b-lookdesc in frame f-lookdesc.
    b-lookdesc:refreshable = false.
    if firstcall = yes and callflag = yes then do: 
     find bq-icsec use-index k-prod where 
          bq-icsec.cono    = g-cono           and 
          bq-icsec.prod    = right-trim(v-model) + "^" and   
          bq-icsec.rectype = "zx"             and 
          bq-icsec.altprod = set-desc 
          no-lock no-error.
      if avail bq-icsec and bq-icsec.altprod = set-desc then do:  
       assign v-rowid   = rowid(bq-icsec) 
              firstcall = no.
      end. 
    end.
    b-lookdesc:refreshable = true.
    display b-lookdesc x-function with frame f-lookdesc.
end.  
end.

procedure openinquiryg: 
do: 
    close query q-lookdesc. 
    assign x-function = 
"            F6-Add             F7-Change              F8-Delete            ".
    display x-function with frame f-lookdesc. 
    assign h-type = "  Generic".
    b-lookdesc:title in frame f-lookdesc = " " + h-type +   
    " -  Description                      Item Number                    ". 
    /*** generic description query  ***/
    open query
     q-lookdesc for each bq-icsec use-index k-prod where
                         bq-icsec.cono = g-cono  and
                         bq-icsec.prod    = ""   and 
                         bq-icsec.rectype = "zx" and 
                         bq-icsec.altprod ne ""  
                         no-lock,
                   first bz-icsec outer-join where 
                         bz-icsec.cono    = g-cono     and 
                         bz-icsec.rectype = "zz"       and 
                         bz-icsec.altprod  =  "^" + bq-icsec.altprod          
                    no-lock
                    by bq-icsec.altprod.
    on cursor-up cursor-up.   
    on cursor-down cursor-down.
    enable b-lookdesc  with frame f-lookdesc.
    apply "focus" to b-lookdesc in frame f-lookdesc.
    apply "entry" to b-lookdesc in frame f-lookdesc.
    b-lookdesc:refreshable = false.
    if v-rowid ne ? then do: 
     b-lookdesc:refresh() in frame f-lookdesc. 
     reposition q-lookdesc to rowid(v-rowid). 
    end.
    b-lookdesc:refreshable = true.
    display b-lookdesc x-function with frame f-lookdesc.
    assign v-model = "".
end.  
end.

procedure make-zm-record: 
do:  

define buffer zz-icsec for icsec. 
define buffer nn-icsec for icsec.
define buffer zm-icsec for icsec.
define buffer mm-icsec for icsec. 

 /*** &&& is cross ref model specific master descrip available ***/
 find first zm-icsec use-index k-icsec where 
            zm-icsec.cono    = g-cono  and 
            zm-icsec.rectype = "zm"    and 
            zm-icsec.prod    = v-model  
            no-lock no-error.
 if not avail zm-icsec then do:
  find last nn-icsec use-index k-keyno where
            nn-icsec.cono = g-cono and 
            nn-icsec.rectype = "zm"
            no-lock no-error.
  create zz-icsec.
  assign zz-icsec.cono    = g-cono
         zz-icsec.prod    = trim(v-model," ")
         zz-icsec.rectype = "zm"
         zz-icsec.keyno   = if avail nn-icsec then 
                             nn-icsec.keyno + 1
                            else 
                             1
         zz-icsec.operinit  = g-operinits
         zz-icsec.transdt   = today
         zz-icsec.lastchgdt = today.
 
 end. 
end.  
end. 

