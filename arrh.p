/***************************************************************************
 PROGRAM NAME: arrh.p old arrg for testing
  PROJECT NO.: 00-35
  DESCRIPTION: This program is the arrg control program that passes stuff
                to arrg1.p & arrg2.p
 DATE WRITTEN: 04/11/00
       AUTHOR: Sir Carl E. Graul Esq.
     MODIFIED: 

***************************************************************************/

{sts-arrg1.i "new shared"}
{p-rptbeg.i}

/* the following loads parameters from sapb */

assign
    b-creditmgr  = sapb.rangebeg[1]
    e-creditmgr  = sapb.rangeend[1]
    b-region     = sapb.rangebeg[2]
    e-region     = sapb.rangeend[2]
    b-district   = sapb.rangebeg[3]
    e-district   = sapb.rangeend[3]
    b-slsrep     = sapb.rangebeg[4]
    e-slsrep     = sapb.rangeend[4]
    b-divno      = if sapb.rangebeg[5] = "" then 0 else int(sapb.rangebeg[5])
    e-divno      = if sapb.rangeend[5] = "" then 999 else 
                      min (999,dec(sapb.rangeend[5]))
    p-agecredit  = if sapb.optvalue[1] = "Yes" then "Y" else "N"
    p-minbal     = dec(sapb.optvalue[3])  
    p-mindays    = int(sapb.optvalue[5])   
    p-detail     = if sapb.optvalue[6] = "Yes" then "Y" else "N".
    if sapb.optvalue[7] = "yes" then
        assign p-div = yes.
    else
        assign p-div = no.

/*   Header Variable Initialization */
assign rpt-user = userid("dictdb")
       rpt-dt = today
       rpt-time = string(time, "HH:MM")
       rpt-dow = entry(weekday(today),dow-lst)
       rpt-cono = g-cono
       x-page = 0.


if p-div = yes then
    run "arrh2.p".
else 
    run "arrh1.p".

/* ***** eoj stuff ***** */


/* ############################ end of program ######################### */ 

