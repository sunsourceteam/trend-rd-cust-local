/* vaexqlx.lpr 1.1 03/09/98 */
/*h****************************************************************************
INCLUDE      : vaexqlx.lpr
DESCRIPTION  : VAEXQ Line Item Entry - Cross Reference Options - ICSP Create
USED ONCE?   : yes
AUTHOR       : tah
DATE WRITTEN : 07/25/11
CHANGES MADE :
******************************************************************************/

{oeetlx.z99 &user_top = "*"
            &parmwhse     = "{&whse}"
            &parmprod     = "{&prod}"}

/*d Allow icsp/icsw create if sasoo security permits for a
    catalog product **/


if v-catconvertfl then
cat:
do on endkey undo, leave:
    pause 0 before-hide.

    {w-sasc.i no-lock}
    {w-icsc.i {&prod} no-lock}
    {w-icsp.i {&prod} no-lock}

    if avail icsc and avail sasc then do:

        /*d Find the ICSL record for the product defaults */
        {w-icsl.i {&whse} icsc.vendno icsc.prodline no-lock}


        
        
        /*u User Hook */
        {oeetlx.z99 &user_prodline = "*"
                    &parmwhse     = "{&whse}"
                    &parmprod     = "{&prod}"}

        /*tb 18575 08/07/95 ajw; Nonstock/Special Kit Component.*/
        if substring(g-ourproc,1,2) ne "bp" and
           not avail icsl
        then
        do on endkey undo, leave cat:
            run err{&errprog}.p(9022).
            {&errorlogic}

            {&compause}
            {pause.gsc}
            /{&compause}* */

            {&catcreatefl} = no.

            leave cat.
        end. /* if substring(g-ourproc,1,2) ne "bp" */

        if substring(g-ourproc,1,2) = "bp" then
            {&catcreatefl} = false.
        else
            {&catcreatefl} = v-iccatstockfl.

        /*tb 21380 07/15/96 sah; Don't display if called from
            Service Warranty.  Changed to can-do clause. */
        /*d Ask the operator if they want to create the catalog
            product in inventory */
        {&comui}

        /*tb e11740 05/16/02 kjb; Added 'do while true' so that
            the user can be prompted over again to create the
            product if necessary */
        catloop:
        do while true on endkey undo, retry:

            /*d If the user presses Cancel or Jump, then execute
                the statement passed in. */
            if {k-cancel.i} or {k-jump.i} then do:

                {&cancelact}

            end.

            if substring(g-ourproc,1,2) ne "bp" then
                update {&catcreatefl}
                    label "Create Catalog Product in Inventory"
                with frame f-yncat overlay
                centered row 11 side-labels.

            /*tb e11740 05/16/02 kjb; If you do not want to create
                the catalog as a stocked product, then make sure
                you can enter a nonstock OE line.  If not, get out
                of here. */
            if {&catcreatefl} = no and
               can-find (sasoo use-index k-sasoo where
                         sasoo.cono       = g-cono      and
                         sasoo.oper2      = g-operinits and
                         sasoo.nonstockfl = no)
            then do:

                /*d "Operator Security Prohibits Non-Stock Product
                    Entry (1122)" */
                run err{&errprog}.p(1122).
                {pause.gsc}

                next catloop.

            end.
            leave catloop.

        end. /* do while true */
        /{&comui}* */

        {&resetcatcreatefl}

        if {&catcreatefl} then do:
            {&comui}
            hide frame f-yncat.
            /{&comui}* */

            {w-icsp.i {&prod} no-lock}

            /*u User Hook */
            {oeetlx.z99 &user_beficspcrt = "*"
                        &parmwhse     = "{&whse}"
                        &parmprod     = "{&prod}"}

            {&comcreatemess}
            run err.p(9021).
            /{&comcreatemess}* */

            &if "{&NXTREND-SYS}" = "POWERVUE" &then

            /* Set product and grace period defaults for remanufactured prod */
            if pv-prodtype = "R" then do:
                if avail icsp then
                    assign
                        v-dirtycore     = icsp.dirtycoreprod
                        v-impliedcore   = icsp.impliedcoreprod
                        v-vendgraceper  = icsp.vendgraceper
                        v-vendcoregrcfl = icsp.vendcoregrcfl
                        v-custgraceper  = icsp.custgraceper
                        v-custcoregrcfl = icsp.custcoregrcfl.

                else
                  assign
                      v-dirtycore     
                        = if sasc.icdirtycorepre = "" then ""
                          else if sasc.iccoredflt = "a":u then
                          if can-do("0,1,2,3,4,5,6,7,8,9":u,subst({prod},1,1))
                          then
                            sasc.icdirtycorepre +
                            subst({&prod},1,24 - length(sasc.icdirtycorepre))
                          else
                            sasc.icdirtycorepre +
                            subst({&prod},2,23 - length(sasc.icdirtycorepre))
                          else if sasc.iccoredflt = "p":u then
                            sasc.icdirtycorepre +
                            subst({&prod},1,24 - length(sasc.icdirtycorepre))
                          else if sasc.iccoredflt = "s":u then
                         trim(subst({&prod},1,24 - length(sasc.icdirtycorepre)))                          +
                          sasc.icdirtycorepre
                          else ""
                      v-impliedcore   = 
                        if sasc.icimplcorepre = "" then ""
                        else if sasc.iccoredflt = "a" then
                        if can-do("0,1,2,3,4,5,6,7,8,9":u, subst({&prod},1,1))                         then   
                          sasc.icimplcorepre +
                          subst({&prod},1,24 - length(sasc.icimplcorepre))
                        else
                          sasc.icimplcorepre +
                          subst({&prod},2,23 - length(sasc.icimplcorepre))
                        else if sasc.iccoredflt = "p":u then
                          sasc.icimplcorepre +
                          subst({&prod},1,24 - length(sasc.icimplcorepre))
                        else if sasc.iccoredflt = "s":u then
                        trim(subst({&prod},1,24 - length(sasc.icimplcorepre))) +
                        sasc.icimplcorepre
                        else ""
                      v-vendgraceper  = sasc.apvendgraceper
                      v-vendcoregrcfl = if sasc.apvendcoregrcfl then yes
                                        else no
                      v-custgraceper  = sasc.arcustgraceper
                      v-custcoregrcfl = if sasc.arcustcoregrcfl then yes
                                        else no.

                /* AO Defaults are not correct for core items */
                if v-impliedcore = "" or v-dirtycore = "" then do:
                    pv-retnerrormess = "Defaults Not Setup For " +
                                       "Implied/Dirty Core Items".
                    leave cat.
                end.


                /* Implied core validation */
                /* Check for Implied core product in ICSP */
                if not {v-icsp.i v-impliedcore} then do:

                    /* Check for implied core product in catalog */
                    if not {v-icsc.i v-impliedcore} then do:
                        run err{&errprog}.p(7103).
                        {&errorlogic}
                        leave cat.
                    end. /* if not avail icsc */

                end. /* if not avail icsp */
                else do:
                    /* Verify that this is a implied core product */
                    if not {icspimpl.gvl &prod = v-impliedcore}
                    then do:
                        run err{&errprog}.p(7107).
                        {&errorlogic}
                        leave cat.
                    end. /* not an implied core product */

                end. /* else do - icsp avail */

                /* Dirty core validation */
                /* Check for dirty core product in ICSP */
                if not {v-icsp.i v-dirtycore} then do:

                    /* Check for dirty core product in catalog */
                    if not {v-icsc.i v-dirtycore} then do:
                        run err{&errprog}.p(7104).
                        {&errorlogic}
                        leave cat.
                    end. /* if not avail icsc */

                end. /* if not avail icsp */
                else do:
                    /* Verify that this is a dirty core product */
                    if not {icspcore.gvl &prod = v-dirtycore} then do:
                        run err{&errprog}.p(7108).
                        {&errorlogic}
                        leave cat.
                    end. /* not an implied core product */

                end. /* else do - icsp avail */

                /* Validations all passed, records exist */
                /* Create Implied core */
                {w-icsp.i v-impliedcore exclusive-lock b-}

                if not avail b-icsp then do:

                    {w-icsc.i v-impliedcore no-lock}

                    if avail icsc then do:
                        {w-icsl.i g-whse icsc.vendno icsc.prodline no-lock}

                        /* Check implied core catalog product line */
                        if not avail icsl
                        then do:

                            run err{&errprog}.p(7105).
                            {&errorlogic}
                            leave cat.
                        end. /* icsl not avail */

                    end. /* if not avail icsc */

                    if not can-find(first icsw where
                            icsw.cono = g-cono        and
                            icsw.prod = v-impliedcore and
                            icsw.whse = g-whse)
                    then do:

                        {p-iccat.i &buficsp = "b-"}

                    end. /* no icsw */

                    assign
                        b-icsp.statustype = "L"
                        b-icsp.prodtype   = "I".

                    {t-all.i b-icsp}

                end. /* if not avail b-icsp */


                /* Create Dirty core */
                {w-icsp.i v-dirtycore exclusive-lock b1-}
                if not avail b1-icsp then do:

                    {w-icsc.i v-dirtycore no-lock}
                    if avail icsc then do:

                        {w-icsl.i g-whse icsc.vendno icsc.prodline no-lock}

                        if not avail icsl
                        then do:
                            run err{&errprog}.p(7106).
                            {&errorlogic}
                            undo cat.  /* Need to undo implied */
                            leave cat.
                        end. /* not avail icsl */

                    end. /* avail icsc */

                    if not can-find(first icsw where
                            icsw.cono = g-cono      and
                            icsw.prod = v-dirtycore and
                            icsw.whse = g-whse)
                    then do:

                        {p-iccat.i &varcom = "&varcom = /*"
                                   &buficsp = "b1-"}

                    end. /* no icsw */

                end. /* if not avail b1-icsp */

                assign
                    b1-icsp.prodtype = "C".
                {t-all.i b1-icsp}

                {w-icsp.i {&prod} no-lock}
                {w-icsc.i {&prod} no-lock}

            end. /* Remanufactured product */
            &endif

            /*d Create icsp/icsw records for catalog product **/
            do:

                &if "{&NXTREND-SYS}" = "POWERVUE" &then

                /* Create the ICSP/ICSW record */
                {p-iccat.i &varcom = "&varcom = /*"}

                /* Set remanufactured part defaults */
                if pv-prodtype = "R":u and avail icsp
                then do:

                    assign
                        icsp.prodtype        = pv-prodtype
                        icsp.impliedcoreprod = v-impliedcore
                        icsp.dirtycoreprod   = v-dirtycore
                        icsp.vendgraceper    = v-vendgraceper
                        icsp.vendcoregrcfl   = v-vendcoregrcfl
                        icsp.custgraceper    = v-custgraceper
                        icsp.custcoregrcfl   = v-custcoregrcfl.
                    {t-all.i icsp}

                end. /* if pv-prodtype = "R" */

                &else

                    {p-iccat.i}

                &endif

            end. /* do */

            {oeetlx.z99 &user_afticspcrt = "*"
                        &parmwhse        = "{&whse}"
                        &parmprod        = "{&prod}"}

            /*u Custom user call */
            if v-oeinterfl then run oeetlu99.p("ec").

            /*tb 19484 10/03/96 jl; WL - Added Create of WL when
                      adding catalog product */
            /*tb 19484 10/29/96 gp;  Change wlmprocb.gpr to
                wlmproca.gpr */
            {wlmproca.gpr
                &whse      = "{&whse}"
                &prod      = "{&prod}"
                &livecd    = "yes"
                &priority  = 5
                &processty = "'MST'"
                &transtype = "'s'"
                &function  = "'icsw'"
                &updtype   = "'a'"
                &sastype   = "' '"
                &b-        = "b-"
                &recid     = ?        }

            {oeetlx.z99 &user_beficswupdt = "*"
                        &parmwhse     = "{&whse}"
                        &parmprod     = "{&prod}"}

            /*d If arp for icsw created is a whse, create arp
                whse record also if it doesn't exist **/
            /*tb 19565 10/20/95 kr; Introduce new ARP Type of "M"
                (VMI) */
            if not can-do("v,m",icsw.arptype) then do:

                /*d if arp whse record not already set up... */
                {wb-icsw.i g-cono icsw.prod icsw.arpwhse no-lock}

                if not avail b-icsw then do:
                    {w-icsd.i icsw.arpwhse no-lock}

                    {w-icsl.i icsw.arpwhse icsc.vendno
                              icsc.prodline no-lock}

                    /*tb 20858 04/04/96 tdd; Replace s-null.i with
                        reccopy.gpr */
                    if avail icsd and avail icsl then do:

                        {reccopy.gpr &file  = "icsw"
                                     &field = "cono whse"
                                     &assign = "assign
                                                    icsw.cono =
                                                        g-cono
                                                    icsw.whse =
                                                        icsd.whse."
                                     &stream = "stream s-out"}

                        {oeetlx.z99 &user_icsw-crt-arp = "*"
                                    &parmwhse          = "{&whse}"
                                    &parmprod          = "{&prod}"}

                        {p-iccat.i &com = "/*"}

                        {oeetlx.z99 &user_arp-p-iccat = "*"
                                    &parmwhse     = "{&whse}"
                                    &parmprod     = "{&prod}"}

                        {w-icswu.i icsw.prod icsd.whse
                            exclusive-lock}

                        if not avail icswu then create icswu.

                        /*tb 17637 10/04/95 jtk;
                          Default from icsc.serlottype  */
                        assign
                            icswu.prod      = icsw.prod
                            icswu.whse      = icsd.whse
                            icswu.cono      = g-cono
                            icsw.arptype    = "v"
                            icsw.arpvendno  = icsc.vendno
                            icsw.prodline   = icsc.prodline
                            icsw.arppushfl  = no
                            icsw.arpwhse    = ""
                            icsw.serlottype = icsc.serlottype.

                        {t-all.i icsw}
                        {t-all.i icswu}

                    end. /* if avail icsd and avail icsl */

                    else if not avail icsd
                    /*tb 18575 08/07/95 ajw; Nonstock/Special
                        Comps*/
                    then do on endkey undo, leave cat:
                        run err{&warnprog}.p(9616).
                        {&warninglogic}

                        {&compause}
                        {pause.gsc}
                        /{&compause}* */

                    end. /* else if not avail icsd */

                    else if not avail icsl
                    then do on endkey undo, leave cat:
                        run err{&warnprog}.p(9024).
                        {&warninglogic}

                        {&compause}
                        {pause.gsc}
                        /{&compause}* */

                    end. /* else if not avail icsl */

                end. /* if not avail b-icsw */

                {oeetlx.z99 &user_aftarpicsw = "*"
                            &parmwhse     = "{&whse}"
                            &parmprod     = "{&prod}"}

            end. /* if not can-do("v,m",icsw.arptype) */

            {oeetlx.z99 &aft_candoarptype = "*"
                        &parmwhse     = "{&whse}"
                        &parmprod     = "{&prod}"}

            hide message no-pause.

            assign
                {&cataddfl} = yes
                v-errflchar     = "p".
        end. /* if {&catcreatefl} */

    end. /* if avail icsc and avail sasc */

    {oeetlx.z99 &user_befprodcat = "*"
                &parmwhse        = "{&whse}"
                &parmprod     = "{&prod}"}

    {&prodcat} = if avail icsp then icsp.prodcat
                 else if avail icsc then icsc.prodcat
                 else "".

end. /* if v-catconvertfl then cat: */

{oeetlx.z99 &user_bottom = "*"
            &parmwhse    = "{&whse}"
            &parmprod    = "{&prod}"}
