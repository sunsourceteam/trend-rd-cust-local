/* p-ares1.i 1.12 06/15/94 */
/*h*****************************************************************************
  PROCEDURE     : p-ares9.i
  DESCRIPTION   : Print logic for ARES - statement format 1
  AUTHOR        :
  DATE WRITTEN  :
  CHANGES MADE  : 07/13/92 pap; TB#  7191 Customer # change Use report.c12,
                                not de12d0 for cust #
                  05/24/93 jrg; TB#  3645 Added seqno so statements will print
                                in the same order everytime.
                  06/24/94 jrg; TB# 11968 Added p-arroll.i to get SVs and
                                Credits to go into period balances when Special
                                Transaction flag is set.
                  07/12/93 jrg; TB# 12196 Moved p-rollsp.i info to ares1.p and
                                aresf1.p i norder to print page footer info.
                  08/16/93 jlc; TB# 12424 Running statements for a range of
                                customers is slow.
                  12/08/93  bj; TB# 13761 Unable to print 0 balance accounts
                  06/15/94 fef; TB# 15721 Remove duplicate break by
                  11/09/94  rs; TB# 17051 Transfer Info To Notification Msg
                  01/03/95 fef; TB# 16836 added display of s-totbal if
                                arsc.statmentty ne 'b'.
*******************************************************************************/

        {&fax}
        /*tb 17051 11/09/94 rs; Transfer Info to Notification Msg */
        {p-faxc2.i &extent = "4"
                   &file   = "arsc."
                   &out    = "report.outputty"
                   &tag    = "'AR Statement'"}
        /{&fax}* */

        {arbal.lca}
        /* {p-arbal.i} SX55 */

        assign
            v-addr[1]   = report.c24
            v-addr[2]   = report.c24-2
            v-addr[3]   = report.c24-3
            v-addr[4]   = report.c20 + ", " + report.c2 + " " + report.c13
            v-lastbal   = report.de9d2s
            v-lastdt    = report.dt
            v-totbal    = if arsc.statementty = "b"
                          then v-lastbal else 0
            /*tb 7191 07/13/92 pap; Customer # chg - use c12 instead of de12d0
            v-de12d0    = report.de12d0                                       */
            v-c12       = report.c12

            v-continued = "Continued On Next Page"
            s-totalbal  = s-totalbal - (if not p-futinvfl
                                        then arsc.futinvbal else 0)
/*tb 11968 06/24/93 jrg; remove COD amount from balance if flag = no. */
            s-totalbal  = s-totalbal - if g-arcodinbalfl
                                        then arsc.codbal else 0.

        do j = 1 to 3:
            if v-addr[j] = ""
            then assign
                    v-addr[j]       = v-addr[j + 1]
                    v-addr[j + 1]   = "".
        end.
        view frame f-bottom.
        view frame f-top.

        if arsc.statementty = "b"
        then
            display v-lastdt v-lastbal with frame f-fwdbal.

        if p-invord = "d"
        then for each b-report use-index k-cust where
                      b-report.reportnm = v-reportnm and
                      b-report.cono     = v-cono     and
                      b-report.oper2    = g-operinit and
                      b-report.outputty = "d"        and
                      /*tb 7191 07/13/92 pap; Customer # chg
                                              Use c12 instead of de12d0       */
                      /*tb 12424 08/16/93 jlc; Report is running slow.  Plug
                                  report.de12d0 with 0 to keep the key        */
                      b-report.de12d0   = 0          and
                      b-report.c12      = report.c12
                      no-lock
                by b-report.dt
                by b-report.i7
                by b-report.i3
                by b-report.i4:
/*tb 3645 05/24/93 jrg; Added i7, i3, i4 (aret.seqno) to print order. */
            {p-aresd9.i "/*"}
        end.
        else if p-invord = "s"
             then for each b-report use-index k-cust where
                           b-report.reportnm = v-reportnm and
                           b-report.cono     = v-cono     and
                           b-report.oper2    = g-operinit and
                           b-report.outputty = "d"        and
                           /*tb 7191 07/13/92 pap; Customer # change
                                                   Use c12 instead of de12d0  */
                           /*tb 12424 08/16/93 jlc; Report is running slow. Plug
                                  report.de12d0 with 0 to keep the key        */
                           b-report.de12d0   = 0          and
                           b-report.c12      = report.c12
                           no-lock
                    /*tb 15721 06/15/94 fef - remove duplicate by report.c13*/
                    break by b-report.c13 /*by b-report.c13*/
                          by b-report.dt
                          by b-report.i7
                          by b-report.i3
                          by b-report.i4:
/*tb 3645 05/24/93 jrg; Added i7, i3, i4 (aret.seqno) to print order. */

                    {p-aresd9.i}
                end.

        else if p-invord = "i"
             then for each b-report use-index k-cust where
                           b-report.reportnm = v-reportnm and
                           b-report.cono     = v-cono     and
                           b-report.oper2    = g-operinit and
                           b-report.outputty = "d"        and
                           /*tb 7191 07/13/92 pap; Customer # change
                                                   Use c12 instead of de12d0  */
                           /*tb 12424 08/16/93 jlc; Report is running slow. Plug
                                  report.de12d0 with 0 to keep the key        */
                           b-report.de12d0   = 0          and
                           b-report.c12      = report.c12
                           no-lock
                    break by b-report.c12
                        by b-report.i7 by b-report.i3 by b-report.i4:
/*tb 3645 05/24/93 jrg; Added i4 (aret.seqno) to print order. */

                    {p-aresd9.i "/*"}
                  end.

       /*tb 16836 01/03/95 fef -  added else display s-totalbal */
        if arsc.statementty = "b"
        then display  v-totbal with frame f-tot.
        else display s-totalbal @ v-totbal with frame f-tot.

        if p-chksummfl and p-actonlyfl
        then do:
            for each b-report use-index k-cust where
                     b-report.reportnm = v-reportnm      and
                     b-report.cono     = v-cono          and
                     b-report.oper2    = g-operinit      and
                     b-report.outputty = "d"             and
                     /*tb 7191 07/13/92 pap; Customer # change - c12
                                                            for de12d0        */
                     /*tb 12424 08/16/93 jlc; Report is running slow.  Plug
                                  report.de12d0 with 0 to keep the key        */
                     b-report.de12d0   = 0               and
                     b-report.c12      = report.c12      and
                     b-report.c1       = "p"        no-lock
                /*tb 15721 06/15/94 fef - remove duplicate by b-report.i7*/
                break by b-report.i7: /* by b-report.i7*/

                if first(b-report.i7)
                then do:
                    display with frame f-chksumm.
                end.
                display
                        b-report.dt
                        b-report.c20    @ v-invoice
                        b-report.de9d2s @ v-credit
                    with frame f-detail.
                down with frame f-detail.
            end.
            hide frame f-chksumm.
        end.

        /*tb 13761 12/08/93 bj; Unable to print 0 balance accounts */
        if line-counter eq 1 then display with frame f-detail.

        v-continued = "".
        if not v-faxfl then page.
        {&fax}

        display with frame f-bottom.
        down with frame f-bottom.

        hide frame f-top.
        hide frame f-bottom.
        if not (sasp.pcommand begins "fx") then
            put unformatted "^T".
        output close.
        /{&fax}* */
