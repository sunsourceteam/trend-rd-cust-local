define input parameter zx-cono   like apsv.cono   no-undo.
define input parameter zx-vendno like apsv.vendno no-undo.
define input parameter zx-custno like arsc.custno no-undo.
define input parameter zx-shipto like arss.shipto no-undo.
define input parameter zx-prod   like icsp.prod   no-undo.

define var z-valid  as logical no-undo.
define var c-prod as character format "x(24)" no-undo.
define var c-hash as character format "x(60)" no-undo.
define var z-prod as character format "x(24)" no-undo.
define var z-hash as character format "x(24)" no-undo.
define var v-recno like pdsc.pdrecno          no-undo.
define var z-prefix as char format "x(20)"    no-undo.
define var z-inx    as integer                no-undo.
define var z-shipto like arss.shipto          no-undo.

/* Hash builder Variables */

define var x-hashlen    as integer                              no-undo.
define var x-prodlen    as integer                              no-undo.
define var x-prod       as character format "x(24)"             no-undo.
define var x-hash       as character format "x(24)"             no-undo.
define var x-deadend    as character format "x(2)"              no-undo.
define var x-anchor     as logical                              no-undo.


define var x-normalstring as character  format "x(300)" init "" no-undo.
define var x-deadstring   as character  format "x(300)" init "" no-undo.


/*  */



define buffer l-pdsc for pdsc.
define buffer x-notes for notes.

find sasc use-index k-sasc where
     sasc.cono = zx-cono
     no-lock no-error.
     
     


find first pdsc where pdsc.cono = zx-cono and
                      pdsc.statustype = true and
                      pdsc.levelcd = 1 and
                      pdsc.custno = zx-custno and
                      pdsc.custtype = (if sasc.pdjobfl = true then
                                         zx-shipto
                                       else
                                         " ") and
                      pdsc.prod = zx-prod and
                      pdsc.whse = "" and
                      (pdsc.enddt ge today or pdsc.enddt = ?) and
                      (pdsc.startdt le today or pdsc.startdt = ?)
                      no-lock no-error.
                      
if not avail pdsc then
  do:
  c-prod = zx-prod.
  z-prod = replace (c-prod," ","").
  z-prod = replace (z-prod,"-","").
  z-prod = replace (z-prod,"/","").
  z-prod = replace (z-prod,",","").
  z-prod = replace (z-prod,".","").
  z-prod = replace (z-prod,"(","").
  z-prod = replace (z-prod,")","").

  z-valid =  false.
  assign z-shipto = "".
  if zx-shipto <> "" then
    do:
    assign z-shipto = zx-shipto.
    find first notes where notes.cono = zx-cono and
                          notes.notestype = "zz" and
                          notes.primarykey = "pdsc" +
                             string(zx-vendno,"999999999999") +
                             string(zx-custno,"999999999999") +
                             string(z-shipto,"x(8)")
                             no-lock no-error.
    
    if not avail notes then
      do:
      assign z-shipto = "".
      
      find first pdsc where pdsc.cono = zx-cono and
                            pdsc.statustype = true and
                            pdsc.levelcd = 1 and
                            pdsc.custno = zx-custno and
                            pdsc.custtype = (if sasc.pdjobfl = true then
                                               z-shipto
                                             else
                                               "") and
                            pdsc.prod = zx-prod and
                            pdsc.whse = "" and
                           (pdsc.enddt ge today or pdsc.enddt = ?) and
                           (pdsc.startdt le today or pdsc.startdt = ?)
                            no-lock no-error.
                      
      if avail pdsc then
        return.
      end.
    end.
  Hashloop:
  for each notes where notes.cono = zx-cono and
                       notes.notestype = "zz" and
                       notes.primarykey = "pdsc" +
                             string(zx-vendno,"999999999999") +
                             string(zx-custno,"999999999999") +
                             string(z-shipto,"x(8)")
                              no-lock
                              break by notes.origpageno descending:

    
    Prefixloop:
    for each x-notes where x-notes.cono = zx-cono and
                           x-notes.notestype = "zz" and
                           x-notes.primarykey = "Vickers Prefixes"                                         no-lock:
      do z-inx = 1 to 16:
        if x-notes.noteln[z-inx] = " " then
          next.
                                        
        assign z-prefix = substring (x-notes.noteln[z-inx],1,20).
      
      /* Take out the commas from the command they don't belong */
        z-hash = "".
        z-hash = replace (notes.secondarykey,",","").
        z-hash = z-prefix + z-hash.
        z-hash = replace (z-hash," ","").


        run build_hash  (input z-hash,
                         input-output x-deadstring,
                         input-output x-normalstring).
      /* We've done the job in the Build_Hash because of an overflow string */
      
        if z-valid then 
          do:
          run intpdscbuild.
          leave HashLoop.
          end.              
      end.
    end.
  

  if z-valid = false then
    do:
    if length(x-normalstring) > 0 then
      do:
      run grepofitem2.p (input z-prod,
                         input x-normalstring,
                         input-output z-valid).
      if z-valid then 
        do:
        run intpdscbuild.
        leave HashLoop.
        end.
     assign x-normalstring = "".
     end.

    if length(x-deadstring) > 0 and z-valid = false then
      do:
      x-deadstring = " -x " + x-deadstring. 
      run grepofitem2.p (input z-prod,
                         input x-deadstring,
                         input-output z-valid).
      
      if z-valid then 
        do:
        run intpdscbuild.
        leave HashLoop.
        end.
      assign x-deadstring = "".
      end.
    end.    

  
  end.



/*

if z-valid = false then
  do:
  if length(x-normalstring) > 0 then
    do:
    run grepofitem2.p (input z-prod,
                       input x-normalstring,
                       input-output z-valid).
    if z-valid then 
      do:
      run intpdscbuild.
      end.
    end.

  if length(x-deadstring) > 0 and z-valid = false then
    do:
    x-deadstring = " -x " + x-deadstring. 
    run grepofitem2.p (input z-prod,
                       input x-deadstring,
                       input-output z-valid).
      
    if z-valid then 
      do:
      run intpdscbuild.
      end.
    end.
  end.    
*/
end.

/*
run dump_string(input x-deadstring,"deadstring").
run dump_string(input x-normalstring,"normalstring").
*/
procedure dump_string:
  define input parameter gx-string as character no-undo.
  define input parameter gx-name   as character no-undo.
 
  define var gx-inx as integer no-undo.
  output to value("/usr/tmp/" + gx-name).


  gx-inx = length(gx-string).
  
  do gx-inx = 1 to length(gx-string) by 60:
    put substring(gx-string, gx-inx,60) format "x(60)" skip.
  end.
end.    




/* ----------------------------------------------------------------*/
procedure intpdscbuild:
/* ----------------------------------------------------------------*/


  find first pdsc where pdsc.cono = zx-cono and
                        pdsc.statustype = false and
                        pdsc.levelcd = 1 and
                        pdsc.custno = zx-custno and
                        pdsc.custtype = (if sasc.pdjobfl = true then
                                           z-shipto 
                                         else 
                                           "") and
                        pdsc.prod = zx-prod and
                        pdsc.whse = "" and
                        pdsc.startdt        = today
                        no-error.
  
  if avail pdsc then
    do:
    assign 
         pdsc.enddt          = if notes.noteln[3] = "  /  /  " then 
                                  12/31/2009
                                 else    
                                 if notes.noteln[3] <> "" then
                                    date(substring(notes.noteln[3],1,8))
                                 else
                                   12/31/2009
         pdsc.statustype     = true
         pdsc.operinit       = "tah"
         pdsc.transdt        = TODAY
         pdsc.qtybrk[1]      = 0
/* SDIMULT */
         pdsc.prcmult[1]     = 100 - dec(notes.noteln[1])
         pdsc.prcdisc[1]     = 0
         pdsc.qtybreakty     = " "
         pdsc.prctype        = false
         pdsc.priceonty      = "L"
         pdsc.user5          = "PDSCautoPDXP" + 
                                substring(notes.primarykey,5,12)
                               /* 12 char marker 12 char vendor */
         pdsc.user9          = TODAY. 

    end.
  else
    do:
    find last l-pdsc use-index k-pdrecno where l-pdsc.cono = zx-cono
                                               no-lock no-error.
    assign v-recno = (l-pdsc.pdrecno + 1).

    create pdsc.
    assign pdsc.cono           = zx-cono
           pdsc.levelcd        = 1
           pdsc.custno         = zx-custno
           pdsc.custtype       = (if sasc.pdjobfl = true then 
                                    z-shipto
                                  else
                                    " ")
           pdsc.startdt        = today
           pdsc.enddt          = if notes.noteln[3] = "  /  /  " then 
                                    12/31/2009
                                 else    
                                 if notes.noteln[3] <> "" then
                                    date(substring(notes.noteln[3],1,8))
                                 else
                                    12/31/2009
           pdsc.statustype     = true
           pdsc.operinit       = "tah"
           pdsc.transdt        = TODAY
           pdsc.prod           = zx-prod
     /*      pdsc.custtype       = " "            */
           pdsc.pdrecno        = v-recno
           pdsc.qtybrk[1]      = 0
/* SDIMULT */
           pdsc.prcmult[1]     = 100 - dec(notes.noteln[1])
           pdsc.prcdisc[1]     = 0
           pdsc.qtybreakty     = " "
           pdsc.prctype        = false
           pdsc.priceonty      = "L"
           pdsc.user5          = "PDSCautoPDXP" + 
                                  substring(notes.primarykey,5,12)
                                 /* 12 char marker 12 char vendor */
           pdsc.user9          = TODAY.
   end.
end.



/* ----------------------------------------------------------------*/
procedure build_hash:
/* ----------------------------------------------------------------*/


define input parameter zx-hash as  character format "x(60)" no-undo.
define input-output parameter zx-deadendstring as char format "x(300)" no-undo.
define input-output parameter zx-normalstring  as char format "x(300)" no-undo.


assign x-hashlen = length(zx-hash)
       x-anchor = true.

if x-hashlen > 1 and substring(zx-hash,x-hashlen,1) = "%" then
  assign x-hash = substring(zx-hash,1,x-hashlen - 1)  
         x-deadend = "  ". 
else
if x-hashlen > 3 and substring(zx-hash,x-hashlen - 2,3) = "[*]" then
  assign x-hash = substring(zx-hash,1,x-hashlen - 3)  
         x-deadend = "  ". 
else
if x-hashlen > 2 and substring(zx-hash,x-hashlen,1) = "$" then
  assign x-hash = substring(zx-hash,1,x-hashlen)  
         x-anchor = false
         x-deadend = "  ". 
         
else
  assign x-hash = zx-hash
         x-prod = zx-prod
         x-deadend = "-x".

if  x-deadend = "-x" then
  assign zx-deadendstring = zx-deadendstring + 
                            ' -e' + "'" +
                            (if x-anchor then
                              "^"
                             else
                               "")
                           + x-hash + "'".
else
if  x-deadend <> "-x" then
  assign zx-normalstring = zx-normalstring + 
                            ' -e' + "'" +
                            (if x-anchor then
                              "^"
                             else
                               "")
                           + x-hash + "'".

/* We've done the job in the Build_Hash because of an overflow string */
                            
                           
if length(zx-normalstring) > 1000  and z-valid = false then
  do:
  run grepofitem2.p (input z-prod,
                     input zx-normalstring,
                     input-output z-valid).
  assign zx-normalstring = "".
  end.

                           
if length(zx-deadendstring) > 1000  and z-valid = false then
  do:

  zx-deadendstring = " -x " + zx-deadendstring. 
  run grepofitem2.p (input z-prod,
                     input zx-deadendstring,
                     input-output z-valid).
  assign zx-deadendstring = "".
  end.
  
 
end.


