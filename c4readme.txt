c42pdf version 0.12 2001-12-23: 
convert CCITT4 multipage 6.0 TIFF to A4 PDF 1.1
name: *C*CITT *4* TIFF images *2* (=to) *PDF*

"Images in general can be quite revolting."
	Ian Stewart, Foundations of Mathematics

Purpose: To now, this program - evolved from some amateur experimentation with Thomas Merz PDFlib v 0.6 library and code - has mainly been used for the fast conversion of medium-sized (1-200 pages) scans e.g. of journal articles or other scientific documents (sometimes including sice reduction to allow for headers to be added in a second step). As the "ecological niche" it inhabits is so tiny, it should be one of the rather fast programs for CCITT->TIFF conversion; please respond if you find faster programs.

Features:

- runs from command line
- fast processing, because CCITT4 data is directly dumped to output, it is not uncompressed into memory
- scaling option (-s), free vertical placement of reduced-format images on the page (-b option), cropping option (-c)
- can unite list of single TIFF files (-l option)
- can create long sample files from single TIFF images (-r switch)
- distributable under Aladdin Free Public License (see file "copyright")
- no log files, command line options can be viewed via Acrobat Reader's file/document info/general info or by searching raw PDF in a text editor for "Creator:"

Limitations:

- generated PDF files may not be longer than 1,000 pages; see http://c42pdf.ffii.org/docs/FAQ.txt on how to do conversions with other programs
- output PDF has a flat page tree ("linked list")
- only part of TIFF 6.0 specification is used
- only CCITT4 TIFFs are handled (see http://c42pdf.ffii.org/docs/FAQ.txt on how to command-line convert from other formats)
- some functions I rarely use (e.g. nostretch) have not been tested extensively

Usage: 

"c42pdf" display summary of options
"c42pdf mytif.tif" make mytif.pdf from mytif.tif
"c42pdf mytif1.tif mytif2.tif" make mytif1.pdf from mytif1.tif and mytif2.tif.
Input files can be (mixed) single page or multipage TIFFs.

Options: 

-h print this documentation

-o output file other than *.pdf, e.g. ("c42pdf -o newname.pdf mytif.tif" makes
newname.pdf instead of default output mytif.pdf)

-p paper format other than A4 (default), options are "-p B5" , "-p l" (US Letter) "-p o" (original size as defined by TIFF image data) or "-p 1190x1684" (this width 1190 times height 1684 points choice will e.g. give you A2)

-s scale other than 1.0 (original), e.g. "-s 0.7" reduces area covered by image to 0.7*0.7, centered unless "-b" option used

-b bottom in points, combine with the "-s" option, e.g. "-s 0.8 -b 0" places 0.8*0.8-shrunken image in the page bottom center. Hint: Use "-s" for adding headers and footers in a second step (e.g. by the commercial Compose toolkit, http://www.ambia.com), use combined "-s -b" to add headers only.

-l read a list delimited by whitespaces (blanks,tabs,linefeeds etc.) of file names as input, e.g. "c42pdf -o all.pdf -l mylist.txt" merges files named in mylist.txt to all.pdf. The "-l" switch must be preceeded by the "-o" switch. Hint: make such lists under Windows/DOS by "dir /b *.* > mylist.txt"; under Linux "ls * > mylist.txt"

-c <cropbox>: (leftxbottomxrightxtop): defines a crop box, e.g. "-c 45x35x45x35" crops thirty-five pixels at page bottom on top and forty-five pixels at page left and right. The information you hide by cropping (such as scanner margins) is not really lost, it can always be recovered by resetting the crop box to lower values in a PDF manipulation program like Acrobat Exchange.

-r <repetitions>: x times, default: 1: May be useful for creating huge sample files, e.g. "-r 500" repeats each image 500 times.

-R <rotate>: rotate by x degrees 
 
-t <Group (three)>: in addition to G4 allow raw conversion of Group 3 images

--noflip: override automatic flipping (landscape/portait) of page orientations

--nostretch: override automatic stretching of images when samples are smaller than paper size

--lockstretch: allow automatic stretching of images without changing the aspect ratio


Acknowledgements: This program uses PDFlib v 0.6, http://www.pdflib.com/, (C) 1997-98 Thomas Merz, Aladdin Free Public License applies. Also, major parts of the enclosed code are directly modified from the PDFlib library code (and elegance has been sacrificed for having a ready-to-run application). Development was with the gcc compiler, for details of compilation see the readme file in the source code. I also want to express thanks to Richard Urquhart, Thomas Merz, Hideki Watanabe and Rolf Macht for support and encouragement as well as Rick Lightbody (4 June 1999, bug in nostretch option --fixed) for useful bug reports (all errors are mine).

Currently c42pdf inherits Aladdin Free Public License from PDFlib, it also has to be distributed with that license (enclosed document "copyright.txt" or http://www.cs.wisc.edu/~ghost/aladdin/doc/Public.html). Basically, you can distribute the program, and modify the source code, but commercial distribution, e.g. selling the program, is limited. Although as of today, PDFlib is the most liberally distributed PDF C library, AFPL is more restrictive than other public licenses (GPL, Perl Artistic). For the ideas and code alterations for c42pdf the author disclaims any copyright restrictions, so in case the underlying PDFlib shall at any time be submitted to a more free license (such e.g. GPL, Debian, GPLL, Perl Artistic) it is also distributable under that license by default. From my side, you are also explicitly welcome to recode this program under a more free license, if you do so without loss of functionality you may inherit its name and URL.

License: c42pdf.c is distributed under Aladdin Free Public License 

DISCLAIMER: No legal warranties are given for the usability of this software or any arising damages from its use. Especially, only part of TIFF specification has been used and tested, but I would like to hear if important parts for you are missing. 

Contact: Further documentation, source code and problem reports or registration (optional), contact Holger Blasum via http://c42pdf.ffii.org, c42pdf@ffii.org. I had no time to check all the options in combination with various formats (except than what I had in everyday usage), so bug reports (best: including TIFF images) or improvement for documentation are welcome !
