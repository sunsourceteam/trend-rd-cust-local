/* p-oeeaif.i 1.1 01/03/98 */
/* p-oeeaif.i 1.1 01/03/98 */
/*h*****************************************************************************  INCLUDE      : p-oeepaif.i
  DESCRIPTION  : Fields used in the "Remit" record in the edi 810, 843, and 855
  USED ONCE?   : no
  AUTHOR       : jlc
  DATE WRITTEN : 02/23/94
  CHANGES MADE :
    TB# e18165 08/10/03 jlc; Add additional identifiers for eBill
*******************************************************************************/
"Remit " +
     caps(s-remitnm) +
     caps(s-remitaddr[1]) +
     caps(s-remitaddr[2]) +
     caps(s-remitcity) +
     caps(s-remitstate) +
     caps(s-remitzipcd) +
     caps(s-remitphone) /* +
     caps(s-remitdunsno) +
     caps(s-remiterpid) */
