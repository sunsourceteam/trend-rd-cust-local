/**************************************************************************    
    Procedure    : icza
    Description  : 
    Author       : Tom Herzog  re-write of ICZO
    Date Written :
    Changes Made : 
 ***************************************************************************/

{g-sdirank.i &new = "new shared"
             &com = "/*"}


define stream bfile.
define stream cfile.
define stream logfile.
define stream regfile.


define buffer catmaster for notes.

define var sdi-tools as handle no-undo.
define var sdi-next  as handle no-undo.
define var x-breakreg as char format "x(200)" no-undo.
define var x-breakpct as char format "x(200)" no-undo.
define var x-breaklst as char format "x(200)" no-undo.
define var x-range    as int                  no-undo.
define var x-inx      as int                  no-undo.  
define var x-mult     as de                   no-undo.
define var s-brklit   as char format "x(7)"   no-undo.
define var jj         as integer              no-undo.

define var typevall   as decimal format "-zzz,zzz,zz9.99" no-undo.
define var typevala   as decimal format "-zzz,zzz,zz9.99" no-undo.
define var finalvala   as decimal format "-zzz,zzz,zz9.99" no-undo.
define var finalvall   as decimal format "-zzz,zzz,zz9.99" no-undo.


def var v-desc as character format "x(30)"   no-undo.
def var v-vendor as character format "x(15)" no-undo.
def var v-vendorid  as character format "x(15)" no-undo.
def var v-rcptdt as date no-undo.
def var v-invdt as date no-undo.
def var v-entdt as date no-undo.
def var v-avl  like icsw.qtyonhand no-undo.
def var v-co   like icsw.qtyonhand no-undo.
def var v-fiss as integer no-undo.
def var v-future as integer no-undo.
def var v-cost like icsw.avgcost no-undo.
def var v-printdir like sasc.printdir no-undo.
def var v-testmode as logical no-undo.

DEF var ttloh as decimal format ">>>>>>>9-" no-undo.
DEF var ttlusg as integer format ">>>>>>>9-" no-undo.
DEF var ntot as integer format ">>>>>>>9-" no-undo.
DEF var dtot as integer format ">>>>>>>9-" no-undo.
def var ftot as integer format ">>>>>>>9-" no-undo.
Def var savecost like icsw.avgcost         no-undo.
def var zttlusg as integer format ">>>>>>>9-" no-undo.
def var ttlcost as decimal format ">>>>>>>9.99999-" no-undo.
DEF var mos as decimal format ">>>>>>9.9-" no-undo.
DEF var avgdemd as decimal format ">>>>>>>9.99999-"  no-undo.
DEF var ohls12  as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var oh6    as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var oh12    as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var oh24    as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var oh36    as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var oh48    as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var oh60    as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var ohg60   as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var ohns12  as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var ohns24  as decimal format ">>>,>>>,>>9-" extent 5 no-undo.

DEF var qtyoh    as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var qtyusg   as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var qtyls12  as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var qty6     as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var qty12    as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var qty24    as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var qty36    as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var qty48    as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var qty60    as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var qtyg60   as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var qtyns12  as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var qtyns24  as decimal format ">>>,>>>,>>9-" extent 5 no-undo.
                                                          

DEF var costoh    as dec format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var costusg   as dec format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var costls12  as dec format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var cost6     as dec format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var cost12    as dec format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var cost24    as dec format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var cost36    as dec format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var cost48    as dec format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var cost60    as dec format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var costg60   as dec format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var costns12  as dec format ">>>,>>>,>>9-" extent 5 no-undo.
DEF var costns24  as dec format ">>>,>>>,>>9-" extent 5 no-undo.



DEF var aprod     as char format "x(24)"            no-undo.


DEF var aqtyoh    as decimal format ">>>,>>>,>>9-"  no-undo.
DEF var aqtyusg   as decimal format ">>>,>>>,>>9-"  no-undo.
DEF var aqtyls12  as decimal format ">>>,>>>,>>9-"  no-undo.
DEF var aqty6     as decimal format ">>>,>>>,>>9-"  no-undo.
DEF var aqty12    as decimal format ">>>,>>>,>>9-"  no-undo.
DEF var aqty24    as decimal format ">>>,>>>,>>9-"  no-undo.
DEF var aqty36    as decimal format ">>>,>>>,>>9-"  no-undo.
DEF var aqty48    as decimal format ">>>,>>>,>>9-"  no-undo.
DEF var aqty60    as decimal format ">>>,>>>,>>9-"  no-undo.
DEF var aqtyg60   as decimal format ">>>,>>>,>>9-"  no-undo.
DEF var aqtyns12  as decimal format ">>>,>>>,>>9-"  no-undo.
DEF var aqtyns24  as decimal format ">>>,>>>,>>9-"  no-undo.
DEF var a12iss    as decimal format ">>>,>>>,>>9-"  no-undo.                                                          

DEF var adesc      as char format "x(24)"        no-undo.
DEF var acostoh    as dec format ">>>,>>>,>>9-"  no-undo.
DEF var acostusg   as dec format ">>>,>>>,>>9-"  no-undo.
DEF var acostls12  as dec format ">>>,>>>,>>9-"  no-undo.
DEF var acost6     as dec format ">>>,>>>,>>9-"  no-undo.
DEF var acost12    as dec format ">>>,>>>,>>9-"  no-undo.
DEF var acost24    as dec format ">>>,>>>,>>9-"  no-undo.
DEF var acost36    as dec format ">>>,>>>,>>9-"  no-undo.
DEF var acost48    as dec format ">>>,>>>,>>9-"  no-undo.
DEF var acost60    as dec format ">>>,>>>,>>9-"  no-undo.
DEF var acostg60   as dec format ">>>,>>>,>>9-"  no-undo.
DEF var acostns12  as dec format ">>>,>>>,>>9-"  no-undo.
DEF var acostns24  as dec format ">>>,>>>,>>9-"  no-undo.




DEF var wrkoh as decimal format ">>>>>>>9-" no-undo.     
DEF var excess as decimal format ">>>>>>>9-" no-undo.        
   
DEF var p-cost as character format "x(1)" no-undo.
DEF var p-detail as character format "x(1)" no-undo.
DEF var p-filename as character format "x(24)" no-undo.
DEF var p-blankets  as logical                 no-undo.
DEF var p-layer     as logical                 no-undo.
DEF var p-reserve   as logical                 no-undo.
DEF var p-consign   as logical                 no-undo.
Def var p-region    as logical                 no-undo.
DEF var p-breg     as character format "x(1)" no-undo.
DEF var p-ereg     as character format "x(1)" no-undo.
DEF var p-bdist    as character format "x(3)" no-undo.
DEF var p-edist    as character format "x(3)" no-undo.
DEF var p-bvend    as character format "x(24)" no-undo.
DEF var p-evend    as character format "x(24)" no-undo.
DEF var p-bwhse    as character format "x(4)"  no-undo.
DEF var p-ewhse    as character format "x(4)"  no-undo.



DEF var invvalue as decimal format ">>>>>>>>>>9.99-" no-undo.        
DEF var tinvvalue as decimal format ">>>>>>>>>>9.99-" no-undo.        
DEF var x as integer no-undo.


DEF temp-table itemz no-undo 
  field zprod like icsw.prod
  field zdescrip like icsp.descr[1]
  field zpcat like icsp.prodcat
  field zvendor as character format "x(15)"
  field zusage as decimal format ">>>>>>>9-"
  field zrcptdt as date
  field zinvdt  as date 
  field zentdt  as date
  field zonhand as decimal         
  field zcost like icsw.avgcost
  
  index i-line is unique primary
        zprod
   
  index i-vend
        zvendor
        zprod.

  


DEF temp-table itemw no-undo 
  field zregion as character format "x(1)"
  field zdistrict as character format "x(3)"
  field zprod like icsw.prod
  field zdescrip like icsp.descr[1]
  field zpcat like icsp.prodcat
  field zbrk as de
  field zvendor as character format "x(15)"
  field zusage as decimal format ">>>>>>>9-"
  field zrcptdt as date
  field zinvdt  as date 
  field zentdt  as date
  field zonhand as decimal 
  field zcost like icsw.avgcost
  
  index w-line is unique primary
        zregion
        zdistrict
        zprod
  index w-vendor 
        zregion
        zdistrict
        zvendor
        zprod
  index w-prod 
        zvendor
        zprod.



def temp-table obsolete no-undo 
  field o-prod     like icsw.prod
  field o-proddesc as c format "x(30)"
  field o-vendor   like apsv.vendno
  field o-vname    as c format "x(30)"
  field o-ls12month  as integer 
  field o-6month   as integer
  field o-12month  as integer 
  field o-24month  as integer
  field o-48month  as integer
  field o-60month  as integer
  field o-g60month  as integer
  field o-ns12     as integer
  field o-ns24     as integer
  field o-total    as integer
  field o-asofdate as date
 
 
  field o-enterdt  as date
  field o-rcptdt   as date
  field o-invdt    as date
    
   
    
    
    
  index o-inx
        o-prod.


Define temp-table whsqty
  field prod   like icsw.prod
  field whse   like icsw.whse
  field qtyoh   like icsw.qtyonhand
  field vendor  as char format "x(15)"
  field vcost like icsw.avgcost
  field acost like icsw.avgcost

  index w-inx
        prod
        whse.

Define temp-table whsval
  field type   as logical
  field whse   like icsd.whse
  field name   like icsd.name
  field divno  like icsd.divno
  field addr1  as char format "x(30)" 
  field addr2  as char format "x(30)"
  field city   like icsd.city
  field state  like icsd.state
  field zip    like icsd.zipcd
  field lvalue as de format "-zzz,zzz,zz9.99"
  field avalue  as de format "-zzz,zzz,zz9.99"
  
  index whseix
        whse
  index typeix
        type
        whse.




/* ------------------------------------------------------------------------
   Variables Used To Create Trend Like Header
   -----------------------------------------------------------------------*/


DEFINE VARIABLE rpt-dt      as date format "99/99/9999" no-undo.
DEFINE VARIABLE rpt-dow     as char format "x(3)"   no-undo. 
DEFINE VARIABLE rpt-time    as char format "x(5)"  no-undo.
DEFINE VARIABLE rpt-cono    like oeeh.cono         no-undo.
DEFINE VARIABLE rpt-user    as char format "x(08)" no-undo.
DEFINE VARIABLE x-title     as char format "x(177)" no-undo.
DEFINE VARIABLE x-page      as integer format ">>>9" no-undo.
DEFINE VARIABLE dow-lst     as character format "x(30)"
           init "Sun,Mon,Tue,Wed,Thu,Fri,Sat"                 no-undo.

DEFINE VARIABLE monthname   as char format "x(10)" EXTENT 12 initial
["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"].

{p-rptbeg.i}

def var compress-string as character format "x(50)" no-undo 
 init
  "~033~105~033~046~154~061~117~033~046~153~062~123~033~046~154~060~105".
put control compress-string.  


form  header /* Header line */
/* SDI001    */
     "~015"              at 178
     rpt-dt              at 1 space
     rpt-dow             space
     rpt-time            space(3)
     "Company: "         space
     rpt-cono            space
     "Function: ICZA "   space
     "Operator: "        space
     rpt-user            space
     "page:"             at 168
     page-number         at 174 format "zzz9"
     "~015"              at 178
with frame f-headx no-box no-labels page-top down width 180.


form  header /* Header line */
/* SDI001    */
     "~015"              at 178
     rpt-dt              at 1 space
     rpt-dow             space
     rpt-time            space(3)
     "Company: "         space
     rpt-cono            space
     "Function: ICZA "   space
     "Operator: "        space
     rpt-user            space
     "page:"             at 168
     page-number(logfile)         at 174 format "zzz9"
     "~015"              at 178
with frame f-heady no-box no-labels page-top down width 180.


form  header /* Header line */
/* SDI001    */
     "~015"              at 178
     rpt-dt              at 1 space
     rpt-dow             space
     rpt-time            space(3)
     "Company: "         space
     rpt-cono            space
     "Function: ICZA "   space
     "Operator: "        space
     rpt-user            space
     "page:"             at 168
     page-number(regfile)         at 174 format "zzz9"
     "~015"              at 178
with frame f-headz no-box no-labels page-top down width 180.



form
"I N V E N T O R Y   E V A L U A T I O N   R E P O R T"
                 at 61
"~015"           at 178                 
with frame f-head1 no-box no-labels page-top down width 180.

form 
"~015"          at 178
with frame f-head2 no-box no-labels page-top down width 180.

form
"New"           at 42
"NoSale"        at 134
"NoSale"        at 147
"Last"          at 162
"~015"          at 178
with frame f-head3 no-box no-labels page-top down width 180.

form
"Product /"     at 1
"On Hand"       at 30
"Items"         at 41
"0 - 6 Mth"     at 54
"7 - 12 Mth"    at 67
"1 - 2 Yr"      at 81
"2 - 3 Yr"      at 94
"3 - 5 Yr"      at 107
"> 5 Yr"        at 121
"1-2 Yr"        at 134
"> 2 Yr"        at 147
"12 Mth"        at 161
"~015"          at 178
with frame f-head4 no-box no-labels page-top down width 180.


form
"Product /"     at 1
"On Hand"       at 30
"Items"         at 41
"0 - 6 Mth"     at 54
"7 - 12 Mth"    at 67
"1 - 2 Yr"      at 81
"2 - 3 Yr"      at 94
"3 - 5 Yr"      at 107
"> 5 Yr"        at 121
"1-2 Yr"        at 134
"> 2 Yr"        at 147
"12 Mth"        at 161
"Whse"          at 170 
"~015"          at 178
with frame f-head4w no-box no-labels page-top down width 180.




form
"Description"   at 1
"Qty/Cost"      at 29
"Qty/Cost"      at 41
"Qty/Cost"      at 55
"Qty/Cost"      at 68
"Qty/Cost"      at 81
"Qty/Cost"      at 94
"Qty/Cost"      at 108
"Qty/Cost"      at 121
"Qty/Cost"      at 133
"Qty/Cost"      at 146
"Usage"         at 162
"~015"          at 178
with frame f-head5 no-box no-labels page-top down width 180.

form 
"~015"          at 178
with frame f-head6 no-box no-labels page-top down width 180.



form 

 aprod          at 1
 aqtyoh         at 26    
 aqtyls12       at 39 
 aqty6          at 52
 aqty12         at 65
 aqty24         at 78
 aqty36         at 91
 aqty60         at 104
 aqtyg60        at 117
 aqtyns12       at 130
 aqtyns24       at 143
 a12iss         at 157                                                    
 "~015"         at 178  
with frame f-det1 no-box no-labels down width 180.



form 

 aprod          at 1
 aqtyoh         at 26    
 aqtyls12       at 39 
 aqty6          at 52
 aqty12         at 65
 aqty24         at 78
 aqty36         at 91
 aqty60         at 104
 aqtyg60        at 117
 aqtyns12       at 130
 aqtyns24       at 143
 a12iss         at 157
 whsqty.whse    at 170                                                     
 "~015"         at 178  
with frame f-det3 no-box no-labels down width 180.



form            
 adesc          at 2
 acostoh        at 27
 acostls12      at 40
 acost6         at 53
 acost12        at 66
 acost24        at 79
 acost36        at 92
 acost60        at 105
 acostg60       at 118
 acostns12      at 131
 acostns24      at 144
 "~015"         at 178
with frame f-det2 no-box no-labels down width 180.

form 
"~015" at 178
with frame f-space no-box no-labels down width 180.

form
 "~015"          at 1
 "~015"          at 1
 "Whse"          at 1
 "Name"          at 6
 "Div"           at 37
 "Address"       at 43
/* whsval.addr2    at 74 */
 "City"          at 105
 "St"            at 126
 "ZipCd"         at 129
 "     Val Avg"  at 142
 "   Val Lower"  at 158
 "~015"          at 178
with frame f-whsehead no-box no-labels page-top down width 180.




form
 whsval.whse     at 1
 whsval.name     at 6
 whsval.divno    at 37
 whsval.addr1    at 43
/* whsval.addr2    at 74 */
 whsval.city     at 105
 whsval.state    at 126
 whsval.zip      at 129
 whsval.avalue   at 139
 whsval.lvalue   at 155
 "~015"          at 178
with frame f-whseval no-box no-labels down width 180.

form
 typevala   at 139
 typevall   at 155
 "~015"          at 178
with frame f-whsetot no-box no-labels down width 180.

form
 finalvala   at 139
 finalvall   at 155
 "~015"          at 178
with frame f-whsefinal no-box no-labels down width 180.



assign p-cost = sapb.optvalue[1]
       p-detail = sapb.optvalue[2]
       p-filename = sapb.optvalue[3]
       p-blankets = if sapb.optvalue[4] = "yes" then true else false
       p-layer = if sapb.optvalue[5] = "yes" then true else false
       p-reserve = if sapb.optvalue[6] = "yes" then true else false
       p-consign = if sapb.optvalue[7] = "yes" then true else false
       p-region = if sapb.optvalue[8] = "yes" then true else false
       
       p-breg = sapb.rangebeg[1]
       p-ereg = sapb.rangeend[1]
       p-bdist = sapb.rangebeg[2]
       p-edist = sapb.rangeend[2]
       p-bvend = sapb.rangebeg[3]
       p-evend = sapb.rangeend[3]
       p-bwhse = sapb.rangebeg[4]
       p-ewhse = sapb.rangeend[4].



/* -----------------------------------------------------------------------
   Header Variable Initialization
   ----------------------------------------------------------------------*/

rpt-user = userid("dictdb").
rpt-dt = today.
rpt-time = string(time, "HH:MM").
rpt-dow = entry(weekday(today),dow-lst).
rpt-cono = g-cono.
x-page = 0.
x = 0.


/*

run sdirankzc.p persistent set sdi-tools.

run introduce_stats in sdi-tools.
             
*/



if p-filename = " " then
   p-filename = "TBRV".
assign v-printdir = sasc.printdir.

assign v-testmode = true.

if v-testmode then
  assign v-printdir = "/usr/tmp/workfiles/".  
   
output stream bfile to value(v-printdir + p-filename + "xlr").
output stream cfile to value(v-printdir + p-filename + "xlc").
output stream logfile to value(v-printdir + p-filename + "log").
output stream regfile to value(v-printdir + p-filename + "rpt") 
       paged page-size 47.
put stream regfile control compress-string.  


unix silent  value("chmod 666 ") value(v-printdir + p-filename + "xlr").
unix silent  value("chmod 666 ") value(v-printdir + p-filename + "xlc").
unix silent  value("chmod 666 ") value(v-printdir + p-filename + "log").
unix silent  value("chmod 666 ") value(v-printdir + p-filename + "rpt").
if sapb.filefl then
 unix silent  value("chmod 666 ") value(sasc.printdir + sapb.printernm).




itemzloop:
            
for each icsp  use-index k-icsp
   where icsp.cono = g-cono 
/*   and icsp.prod le "/888"     */
   and icsp.statustype <> "l"
   and icsp.kittype <> "b"
no-lock:

  
v-vendor = " ".
v-entdt = icsp.enterdt.
v-invdt = ?.
v-rcptdt = ?.
v-fiss = 0.
v-co = 0.

ttlusg = 0.
ttlcost = 0.
ttloh = 0.
ftot = 0.

icswloop:
for each icsw 
   where icsw.cono = g-cono 
     and icsw.prod = icsp.prod no-lock: 


if (icsw.whse < p-bwhse or
    icsw.whse > p-ewhse) or
    icsw.whse = "sla" or icsw.whse = "ssac" then
   next icswloop.
/*
if  icsw.whse = "dsth"       or
    icsw.whse = "ddel"       or
    icsw.whse = "dclv"       or
    icsw.whse = "dclm"       or
    icsw.whse = "dday"       or
    icsw.whse = "dpit"       or
    icsw.whse = "dsgn"       or
    icsw.whse = "cjeg"       or
    icsw.whse = "ccir"       or
    icsw.whse = "ccol"       or
    icsw.whse = "cpr"        or
    icsw.whse = "cpsn"       then

   next icswloop.
   
  */
  v-cost = (if p-cost = "a" then
             icsw.avgcost
           else
           if p-cost = "r" then
             icsw.replcost 
           else
           if p-cost = "l" and 
              (icsw.avgcost < icsw.replcost or
              icsw.replcost = 0) then
             icsw.avgcost 
           else
           if p-cost = "l" and 
              icsw.avgcost > icsw.replcost then
             icsw.replcost
           else
             icsw.avgcost). 
  
  if p-consign = true then
    do:

    find icsd where icsd.cono = g-cono and
                    icsd.whse = icsw.whse no-lock no-error.
    
    if (can-do("c,a,v,z,k",substring(icsw.whse,1,1)) and 
       icsw.whse <> "csei" and
       icsw.whse <> "chry" and
       icsw.whse <> "aclv" and
       icsw.whse <> "ctip" and
       icsw.whse <> "cimt") or
       (avail icsd and (substring(icsd.city,1,10) = "Do Not Use" or
                        icsd.whse = "****") 
                   and icsw.whse <> "dnwg")  then
    
      do:
      
      v-avl = (icsw.qtyonhand + icsw.qtyunavail + icsw.qtyrcvd).

      find whsval where whsval.whse = icsw.whse no-lock no-error.
      if not avail whsval then
        do:
        create whsval.
        assign
         whsval.type   = true 
         whsval.whse   = icsd.whse
         whsval.name   = icsd.name
         whsval.divno  = icsd.divno
         whsval.addr1  = icsd.addr[1]
         whsval.addr2  = icsd.addr[2]
         whsval.city   = icsd.city
         whsval.state  = icsd.state
         whsval.zip    = icsd.zipcd
         whsval.avalue  = v-avl * icsw.avgcost
         whsval.lvalue  = v-avl * v-cost.
        end.  
      else 
        assign
          whsval.avalue  = whsval.avalue + (v-avl * icsw.avgcost)
          whsval.lvalue  = whsval.lvalue + (v-avl * v-cost).
      next icswloop.
      end.
    end.


  if v-invdt = ? then
     v-invdt = icsw.lastinvdt.
  else if v-invdt < icsw.lastinvdt then
     v-invdt = icsw.lastinvdt.

  if v-entdt = ? then
     v-entdt = icsw.enterdt.
  else if v-entdt > icsw.enterdt then
     v-entdt = icsw.enterdt.

  zttlusg = 0.
  run zsdiusage.p  (input today,
                    input (today - 365),
                    input icsw.whse,
                    input icsw.prod,
                    output zttlusg).

  ttlusg = ttlusg + zttlusg.

  v-future = 0.
  if not p-blankets then
    do:
    for each oeel use-index k-fill 
                       where oeel.cono = g-cono
                         and oeel.statustype = "a"
                         and oeel.invoicedt = ?
                         and oeel.shipprod = icsw.prod 
                         and oeel.whse = icsw.whse no-lock. 
    find
     oeeh where oeeh.cono = oeel.cono and oeeh.orderno = oeel.orderno and
                            oeeh.ordersuf = oeel.ordersuf no-lock no-error.
    if avail oeeh then
      if oeeh.transtype <> "qu" and
        (oeeh.stagecd = 0  and (oeeh.transtype = "bl" or
                              oeeh.transtype = "fo") or                                 oeeh.stagecd < 4 and oeeh.transtype = "do") and       
       
         oeel.statustype = "a" and 
         not(oeel.specnstype = "l" and oeel.statustype = "c") then
    
       if not (oeeh.transtype = "bl" and ((oeel.qtyord - oeel.qtyrel) le 0)) 
         then
         if oeeh.transtype = "bl" then 
           v-future = v-future + (oeel.qtyord - oeel.qtyrel).
         else
           v-future = v-future + oeel.qtyord.
    end.
  end.

 if not p-blankets then
  do:
  for each icenl use-index k-icenl
                 where icenl.cono = g-cono and
                       icenl.typecd = "d" and
                       icenl.whse = icsw.whse and
                       icenl.prod = icsp.prod and
                       icenl.ordtype = "o" and
                       icenl.entrytype = false and
                       icenl.quantity <> 0 no-lock:

      
             
      if v-invdt = ? then
         v-invdt = icenl.postdt.
      else if v-invdt < icenl.postdt then
         v-invdt = icenl.postdt.
 
      if icenl.postdt > (today - 365) then
        do:                 
        v-future   = v-future + (icenl.quantity * -1).
        end.
    end.
  end.





v-co = 0.
if icsw.qtycommit > 0 then
  v-co = v-co + icsw.qtycommit.
if icsw.qtyreservd > 0 then
  v-co = v-co + icsw.qtyreservd.
/*
if icsw.qtybo > 0 then
  v-co = v-co + icsw.qtybo.
*/

if p-reserve = true then
   ttlusg = ttlusg + v-co.

v-avl = (icsw.qtyonhand + icsw.qtyunavail + icsw.qtyrcvd).


if v-avl <> 0 then
  do:
  find whsqty where whsqty.prod = icsw.prod and
                    whsqty.whse = icsw.whse no-lock no-error.
  if not avail whsqty then
    do:
    
    find catmaster where catmaster.cono = g-cono and 
                         catmaster.notestype = "zz" and
                         catmaster.primarykey  = "apsva" and
                         catmaster.secondarykey = 
                         icsp.prodcat no-lock  no-error. 
    if not avail catmaster then
       do:
       v-vendorid = icsp.prodcat.
       end.
    else
      assign
         v-vendorid   = catmaster.noteln[1].

    
    
    create whsqty.                              
    assign                     
      whsqty.prod   = icsw.prod
      whsqty.whse    = icsw.whse 
      whsqty.vendor  = v-vendorid
      whsqty.vcost   = v-cost
      whsqty.qtyoh   = v-avl
      whsqty.acost   = icsw.avgcost.
  
    end.
  else
    assign whsqty.qtyoh = whsqty.qtyoh + v-avl.
  end.
find whsval where whsval.whse = icsw.whse no-lock no-error.
if not avail whsval then
  do:
  find icsd where icsd.cono = g-cono and
                  icsd.whse = icsw.whse no-lock no-error.
  create whsval.
  assign
   whsval.type   = false
   whsval.whse   = icsd.whse
   whsval.name   = icsd.name
   whsval.divno  = icsd.divno
   whsval.addr1  = icsd.addr[1]
   whsval.addr2  = icsd.addr[2]
   whsval.city   = icsd.city
   whsval.state  = icsd.state
   whsval.zip    = icsd.zipcd
   whsval.avalue  = v-avl * icsw.avgcost
   whsval.lvalue  = v-avl * v-cost.
  end.  
else 
  assign
   whsval.avalue  = whsval.avalue + (v-avl * icsw.avgcost)
   whsval.lvalue  = whsval.lvalue + (v-avl * v-cost).


if icsw.avgcost > icsw.replcost and icsw.replcost > 0 then
  export stream logfile delimiter "~011"
    icsp.prod
    icsp.descrip[1]
    icsw.avgcost
    icsw.replcost
    v-avl.

ftot  = ftot + v-future.

ttloh = ttloh + v-avl.
ttlcost = ttlcost + (v-avl
              * v-cost).

for each icet use-index k-postdt where icet.cono = 1 and
                                       icet.prod = icsw.prod and
                                       icet.whse = icsw.whse no-lock:
                      
 if (can-do("kp,po,oe",icet.module) and icet.transtype = "re") or
    (can-do("va",icet.module) and can-do("re,in,ri",icet.transtype)) then
   do:
   if v-rcptdt = ? then
      v-rcptdt = icet.postdt.
   else   
   if v-rcptdt < icet.postdt then
      v-rcptdt = icet.postdt.  
   leave.
   end.
end.

for each icet use-index k-postdt where icet.cono = 1 and
                                       icet.whse = icsw.whse and
                                       icet.prod = icsw.prod no-lock:                                             
  if (can-do("kp,po,oe",icet.module) and icet.transtype = "in") or
     (can-do("va",icet.module) and can-do("re,in,ri",icet.transtype)) then
    do:
    if v-invdt = ? then
       v-invdt = icet.postdt.
    else   
    if v-invdt < icet.postdt then
       v-invdt = icet.postdt.  
    leave.
    end.
  end.
end. /* ICSW */ 


  find catmaster where catmaster.cono = g-cono and 
                       catmaster.notestype = "zz" and
                       catmaster.primarykey  = "apsva" and
                       catmaster.secondarykey = 
                       icsp.prodcat no-lock  no-error. 
  if not avail catmaster then
     do:
     v-vendorid = icsp.prodcat.
     end.
  else
    assign
       v-vendorid   = catmaster.noteln[1].

if ttloh le 0 then
  next itemzloop.
create itemz.
assign 
    itemz.zprod = icsp.prod
    itemz.zdescrip = icsp.descr[1]
    itemz.zpcat = icsp.prodcat
    itemz.zvendor = v-vendorid
    itemz.zusage = ttlusg + ftot
    itemz.zonhand = ttloh
    itemz.zcost = ttlcost
    itemz.zentdt = v-entdt
    itemz.zinvdt = v-invdt
    itemz.zrcptdt = v-rcptdt.


if p-region then
  do:
  /*
  run weight_item2 in sdi-tools
        (input-output x-breakreg,
         input-output x-breakpct,
         input-output x-breaklst,
         input-output x-range,
         input       icsp.prod,
         input       icsp.prodcat,
         input       g-cono,
         input       ttloh).
  */
  run distribute_value.
  end.
  

end.
        
if not p-region then
  do:
  run companyrpt.

  hide frame f-headx.
  hide frame f-head1.
  hide frame f-head2.
  hide frame f-head3.
  hide frame f-head4.
  hide frame f-head5.
  hide frame f-head6.
  end.
else  
  do:
  run companyregrpt.

  hide frame f-headx.
  hide frame f-head1.
  hide frame f-head2.
  hide frame f-head3.
  hide frame f-head4.
  hide frame f-head5.
  hide frame f-head6.
  end.


if p-region then
  run regionrpt.


hide stream regfile frame f-headz.
hide stream regfile frame f-head1.
hide stream regfile frame f-head2.
hide stream regfile frame f-head3.
hide stream regfile frame f-head4.
hide stream regfile frame f-head5.
hide stream regfile frame f-head6.

output stream logfile close.
output stream logfile to value(v-printdir + p-filename + "sum") paged
                                             page-size 47.
put stream logfile control compress-string.  

unix silent  value("chmod 666 ") value(v-printdir + p-filename + "sum").

run whsesummary.


output stream logfile close.
output stream logfile to value(v-printdir + p-filename + "xlcsum").           unix silent  value("chmod 666 ") value(v-printdir + p-filename + "xlcsum").

run whsexlcsummary.


output stream logfile close.
output stream logfile to value(v-printdir + p-filename + ".dat").        
unix silent  value("chmod 666 ") value(v-printdir + p-filename + ".dat").

run icxmxtract.





hide stream regfile frame f-headz.
hide stream regfile frame f-head1.
hide stream regfile frame f-head2.
hide stream regfile frame f-head3.
hide stream regfile frame f-head4.
hide stream regfile frame f-head5.
hide stream regfile frame f-head6.

output stream regfile close.
output stream regfile to value(v-printdir + p-filename + "rpw") paged
                                             page-size 47.
put stream regfile control compress-string.  

unix silent  value("chmod 666 ") value(v-printdir + p-filename + "rpw").

output stream bfile close.
output stream bfile to value(v-printdir + p-filename + "xlw").
unix silent  value("chmod 666 ") value(v-printdir + p-filename + "xlw").


run warehouserpt.











do while valid-handle(sdi-tools):
  sdi-next = sdi-tools:next-sibling.
  delete procedure sdi-tools.
  sdi-tools = sdi-next.
end.




/* ------------------------------------------------------------------ */

procedure regionrpt:

/* ------------------------------------------------------------------ */


view stream regfile frame f-headz.
view stream regfile frame f-head1.
view stream regfile frame f-head2.
view stream regfile frame f-head3.
view stream regfile frame f-head4.
view stream regfile frame f-head5.
view stream regfile frame f-head6.


export stream bfile delimiter "~011"
     " "
     " "
     " "
     " "
     " "
     " "
     " "
     " "
     " "
     "New"
     "New"
     "0 - 6"
     "0 - 6"
     "'7 - 12"
     "'7 - 12"
     " "
     " "
     " "         
     " "          
     " "
     " "
     " "
     " "
     "1 - 2 Year"  
     "1 - 2 Year" 
     "Gtr 2 Year"  
     "Gtr 2 Year" 
     " "
     " "
     " ".


export stream bfile delimiter "~011"
     " "
     " "
     "Region"
     "District"
     "Vendor"
     "Product"
     "Description"
     "OnHand"
     "OnHand"
     "Item"
     "Item"
     "Months"
     "Months"
     "Months"
     "Months"
     "1 - 2 Year"
     "1 - 2 Year"
     "2 - 3 Year"         
     "2 - 3 Year"          
     "3 - 5 Year"
     "3 - 5 Year"
     "Gtr 5 Year"
     "Gtr 5 Year"
     "NoSales"  
     "NoSales" 
     "NoSales"  
     "NoSales" 
     "Usage"
     "SetUp"
     "Lst Sale".

export stream bfile delimiter "~011"
     " "
     " "
     " "
     " "
     " "
     " "
     " "
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"
     "Value" 
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"  
     "Value" 
     "Qty"  
     "Value" 
     "Qty"
     "Date"
     "Date".



do x = 1 to 5:  
  assign    
    qtyoh[x]     = 0
    costoh[x]    = 0 
    qtyls12[x]   = 0
    costls12[x]  = 0
    qty6[x]      = 0
    cost6[x]     = 0
    qty12[x]     = 0
    cost12[x]    = 0
    qty24[x]     = 0
    cost24[x]    = 0
    qty36[x]     = 0
    cost36[x]    = 0
    qty48[x]     = 0
    cost48[x]    = 0
    qty60[x]     = 0
    cost60[x]    = 0
    qtyg60[x]    = 0
    costg60[x]   = 0
    qtyns12[x]   = 0
    costns12[x]  = 0
    qtyns24[x]   = 0
    costns24[x]  = 0
    qtyusg[x]    = 0.
end. 

ttlusg = 0.
ttlcost = 0.
ttloh = 0.

for each itemw no-lock 
  break by itemw.zregion
        by   itemw.zdistrict
        by   itemw.zvendor:
          
  
 ttloh =  itemw.zonhand.
 ttlusg = itemw.zusage.     
 ttlcost = itemw.zcost.
 savecost = ttlcost / ttloh.


 
 
 if ttlusg le 0 then 
    ttlusg = 0. 
 
 if ttloh > 0 then 
   do:
   avgdemd = ttlusg / 12.
   if avgdemd <> 0 then
     mos = ttloh / avgdemd.
   else
     mos = 9999.
   end.
 else
   do:
   avgdemd = 0. 
   mos = 0.
   ttlcost = 0.
   end.
        

 

wrkoh = ttloh.
do x = 1 to 5:
  assign qtyusg[x] = qtyusg[x] + itemw.zusage
         qtyoh[x] = qtyoh[x] + itemw.zonhand
         costoh[x] = costoh[x] + itemw.zcost.
end.

oh6[1]  = round((avgdemd * 6),0).
oh12[1] = round((avgdemd * 12),0).
oh24[1] = round((avgdemd * 24),0).
oh36[1] = round((avgdemd * 36),0).
oh48[1] = round((avgdemd * 48),0).
oh60[1] = round((avgdemd * 60),0).
 

if ((itemw.zentdt > today - 365) and itemw.zusage le 0) and 
    (itemw.zrcptdt = ? or itemw.zrcptdt > (today - 180)) then
  
  do:
  excess = wrkoh.
  do x = 1 to 5:
    assign 
           qtyls12 [x] = qtyls12[x] + excess
           costls12 [x] = costls12[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  mos = 0.
  qty60 = 0.
  end.

if ((itemw.zinvdt = ? and itemw.zentdt < today - 730) or
    (itemw.zinvdt <> ? and itemw.zinvdt < today - 730)) 
       and ttlusg = 0 then
  do:
  excess = wrkoh. 
  do x = 1 to 5:

    assign 
           qtyns24 [x] = qtyns24[x] + excess
           costns24 [x] = costns24[x] + (excess * savecost).
  end.
  wrkoh = wrkoh - excess.
  mos = 0.
  end.


if ((itemw.zinvdt = ? and itemw.zentdt < today - 365) or
    (itemw.zinvdt <> ? and itemw.zinvdt < today - 365)) 
       and ttlusg = 0 then
  do:
  excess = wrkoh. 
  do x = 1 to 5:

    assign 
           qtyns12 [x] = qtyns12[x] + excess
           costns12 [x] = costns12[x] + (excess * savecost).
  end.
  wrkoh = wrkoh - excess.
  mos = 0.
  end.
 
        
        
if mos > 60 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh60[1]
             else
               0).
  do x = 1 to 5:

    assign 
           qtyg60 [x] = qtyg60[x] + excess
           costg60 [x] = costg60[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.

        
if mos > 36 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh48[1]
             else
               0).

  do x = 1 to 5:

    assign 
           qty60 [x] = qty60[x] + excess
           cost60 [x] = cost60[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
   
        
if mos > 36 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh36[1]
             else
               0).

  do x = 1 to 5:

    assign 
           qty48 [x] = qty48[x] + excess
           cost48 [x] = cost48[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
 


        
if mos > 24 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh24[1]
             else
               0).

  do x = 1 to 5:

    assign 
           qty36 [x] = qty36[x] + excess
           cost36 [x] = cost36[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
 

        
if mos > 12 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh12[1]
             else
               0).

  do x = 1 to 5:

    assign 
           qty24 [x] = qty24[x] + excess
           cost24 [x] = cost24[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
 
        
if mos > 6 and wrkoh > 0 then 
  do:
  excess = wrkoh -
            (if p-layer then 
               oh6[1]
             else
               0).

  do x = 1 to 5:

    assign 
           qty12 [x] = qty12[x] + excess
           cost12 [x] = cost12[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.

 
        
if mos > 0 and wrkoh > 0 then 
  do:
  excess = wrkoh.
  do x = 1 to 5:

    assign 
           qty6 [x] = qty6[x] + excess
           cost6 [x] = cost6[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
  
 
assign x = 1.
if p-detail = "d" then
  do:
    display stream regfile 
     itemw.zprod               @ aprod
     qtyoh[x]                  @ aqtyoh
     qtyls12[x]                @ aqtyls12
     qty6[x]                   @ aqty6
     qty12[x]                  @ aqty12
     qty24[x]                  @ aqty24
     qty36[x]                  @ aqty36
     (qty48[x] + qty60[x])     @ aqty60
     qtyg60[x]                 @ aqtyg60
     qtyns12[x]                @ aqtyns12
     qtyns24[x]                @ aqtyns24
     qtyusg[x]                 @ a12iss
   with frame f-det1.
  
  display stream regfile 
     itemw.zdescrip            @ adesc
     costoh[x]                 @ acostoh
     costls12[x]               @ acostls12
     cost6[x]                  @ acost6
     cost12[x]                 @ acost12
     cost24[x]                 @ acost24
     cost36[x]                 @ acost36
     (cost48[x] + cost60[x])   @ acost60
     costg60[x]                @ acostg60
     costns12[x]               @ acostns12
     costns24[x]               @ acostns24
  with frame f-det2.
  
  display stream regfile with frame f-space.



  export stream bfile delimiter "~011"
     " "
     " "
     itemw.zregion
     itemw.zdistrict
     itemw.zvendor
     itemw.zprod
     itemw.zdescrip
     qtyoh[x]
     costoh[x]
     qtyls12[x]
     costls12[x]
     qty6[x]
     cost6[x]
     qty12[x]
     cost12[x]
     qty24[x]
     cost24[x]
     qty36[x] 
     cost36[x]
/*    
      qty48[x]
      cost48[x]
*/
     (qty48[x] + qty60[x])
     (cost48[x] + cost60[x])
     qtyg60[x]
     costg60[x]
     qtyns12[x]
     costns12[x]
     qtyns24[x]
     costns24[x]
     qtyusg[x]
     itemw.zentdt
     itemw.zinvdt.
  end.
assign    
     qtyoh[x]     = 0 
     costoh[x]    = 0
     qtyls12[x]   = 0
     costls12[x]  = 0
     qty6[x]      = 0
     cost6[x]     = 0
     qty12[x]     = 0
     cost12[x]    = 0
     qty24[x]     = 0
     cost24[x]    = 0
     qty36[x]     = 0
     cost36[x]    = 0
     qty48[x]     = 0
     cost48[x]    = 0
     qty60[x]     = 0
     cost60[x]    = 0
     qtyg60[x]    = 0
     costg60[x]   = 0
     qtyns12[x]   = 0
     costns12[x]  = 0
     qtyns24[x]   = 0
     costns24[x]  = 0
     qtyusg[x]    = 0.
                    
if last-of (itemw.zvendor) then
  do:
  assign x = 2.
  display stream regfile 
     (if p-detail = "s" then itemw.zvendor else
        "*Vendor")                 @ aprod
     qtyoh[x]                  @ aqtyoh
     qtyls12[x]                @ aqtyls12
     qty6[x]                   @ aqty6
     qty12[x]                  @ aqty12
     qty24[x]                  @ aqty24
     qty36[x]                  @ aqty36
     (qty48[x] + qty60[x])     @ aqty60
     qtyg60[x]                 @ aqtyg60
     qtyns12[x]                @ aqtyns12
     qtyns24[x]                @ aqtyns24
     qtyusg[x]                 @ a12iss
   with frame f-det1.
    display stream regfile 
     (if p-detail = "s" then " " else
     itemw.zvendor)   @ adesc
     costoh[x]                 @ acostoh
     costls12[x]               @ acostls12
     cost6[x]                  @ acost6
     cost12[x]                 @ acost12
     cost24[x]                 @ acost24
     cost36[x]                 @ acost36
     (cost48[x] + cost60[x])   @ acost60
     costg60[x]                @ acostg60
     costns12[x]               @ acostns12
     costns24[x]               @ acostns24
  with frame f-det2.

  display stream regfile with frame f-space.

  export stream bfile delimiter "~011"
      "*    "
      "Vendor"
      itemw.zregion
      itemw.zdistrict
      itemw.zvendor
      " "
      " "
      qtyoh[x]
      costoh[x]
      qtyls12[x]
      costls12[x]
      qty6[x]
      cost6[x]
      qty12[x]
      cost12[x]
      qty24[x]
      cost24[x]
      qty36[x] 
      cost36[x]
   /*  
      qty48[x]
      cost48[x]
   */
      (qty48[x] + qty60[x])
      (cost48[x] + cost60[x])
      qtyg60[x]
      costg60[x]
      qtyns12[x]
      costns12[x]
      qtyns24[x]
      costns24[x]
      qtyusg[x]
      " "
      " ".
   
   assign    
      qtyoh[x]     = 0
      costoh[x]    = 0 
      qtyls12[x]   = 0
      costls12[x]  = 0
      qty6[x]      = 0
      cost6[x]     = 0
      qty12[x]     = 0
      cost12[x]    = 0
      qty24[x]     = 0
      cost24[x]    = 0
      qty36[x]     = 0
      cost36[x]    = 0
      qty48[x]     = 0
      cost48[x]    = 0
      qty60[x]     = 0
      cost60[x]    = 0
      qtyg60[x]    = 0
      costg60[x]   = 0
      qtyns12[x]   = 0
      costns12[x]  = 0
      qtyns24[x]   = 0
      costns24[x]  = 0
      qtyusg[x]    = 0.
  end.      

                    
if last-of (itemw.zdistrict) then
  do:
  assign x = 3.
  display stream regfile 
     "**District"               @ aprod
     qtyoh[x]                  @ aqtyoh
     qtyls12[x]                @ aqtyls12
     qty6[x]                   @ aqty6
     qty12[x]                  @ aqty12
     qty24[x]                  @ aqty24
     qty36[x]                  @ aqty36
     (qty48[x] + qty60[x])     @ aqty60
     qtyg60[x]                 @ aqtyg60
     qtyns12[x]                @ aqtyns12
     qtyns24[x]                @ aqtyns24
     qtyusg[x]                 @ a12iss
   with frame f-det1.
    display stream regfile 
     itemw.zregion + itemw.zdistrict   @ adesc
     costoh[x]                 @ acostoh
     costls12[x]               @ acostls12
     cost6[x]                  @ acost6
     cost12[x]                 @ acost12
     cost24[x]                 @ acost24
     cost36[x]                 @ acost36
     (cost48[x] + cost60[x])   @ acost60
     costg60[x]                @ acostg60
     costns12[x]               @ acostns12
     costns24[x]               @ acostns24
  with frame f-det2.

  display stream regfile with frame f-space.

  export stream bfile delimiter "~011"
      "**   "
      "District"
      itemw.zregion
      itemw.zdistrict
      " "
      " "
      " "
      qtyoh[x]
      costoh[x]
      qtyls12[x]
      costls12[x]
      qty6[x]
      cost6[x]
      qty12[x]
      cost12[x]
      qty24[x]
      cost24[x]
      qty36[x] 
      cost36[x]
/*
      qty48[x]
      cost48[x]
*/
      (qty48[x] + qty60[x])
      (cost48[x] + cost60[x])
      qtyg60[x]
      costg60[x]
      qtyns12[x]
      costns12[x]
      qtyns24[x]
      costns24[x]
      qtyusg[x]
      " "
      " ".
   
   assign    
      qtyoh[x]     = 0
      costoh[x]    = 0 
      qtyls12[x]   = 0
      costls12[x]  = 0
      qty6[x]      = 0
      cost6[x]     = 0
      qty12[x]     = 0
      cost12[x]    = 0
      qty24[x]     = 0
      cost24[x]    = 0
      qty36[x]     = 0
      cost36[x]    = 0
      qty48[x]     = 0
      cost48[x]    = 0
      qty60[x]     = 0
      cost60[x]    = 0
      qtyg60[x]    = 0
      costg60[x]   = 0
      qtyns12[x]   = 0
      costns12[x]  = 0
      qtyns24[x]   = 0
      costns24[x]  = 0
      qtyusg[x]    = 0.
  end.      
   
                    
if last-of (itemw.zregion) then
  do:
  assign x = 4.
  display stream regfile 
     "***Region "              @ aprod
     qtyoh[x]                  @ aqtyoh
     qtyls12[x]                @ aqtyls12
     qty6[x]                   @ aqty6
     qty12[x]                  @ aqty12
     qty24[x]                  @ aqty24
     qty36[x]                  @ aqty36
     (qty48[x] + qty60[x])     @ aqty60
     qtyg60[x]                 @ aqtyg60
     qtyns12[x]                @ aqtyns12
     qtyns24[x]                @ aqtyns24
     qtyusg[x]                 @ a12iss
   with frame f-det1.
  display stream regfile 
     itemw.zregion   @ adesc
     costoh[x]                 @ acostoh
     costls12[x]               @ acostls12
     cost6[x]                  @ acost6
     cost12[x]                 @ acost12
     cost24[x]                 @ acost24
     cost36[x]                 @ acost36
     (cost48[x] + cost60[x])   @ acost60
     costg60[x]                @ acostg60
     costns12[x]               @ acostns12
     costns24[x]               @ acostns24
  with frame f-det2.

  display stream regfile with frame f-space.
 
  export stream bfile delimiter "~011"
      "***  "
      "Region"  
      itemw.zregion
      " "
      " "
      " "
      " "
      qtyoh[x]
      costoh[x]
      qtyls12[x]
      costls12[x]
      qty6[x]
      cost6[x]
      qty12[x]
      cost12[x]
      qty24[x]
      cost24[x]
      qty36[x] 
      cost36[x] 
/*      
      qty48[x]
      cost48[x]
*/ 
      (qty48[x] + qty60[x])
      (cost48[x] + cost60[x])
      qtyg60[x]
      costg60[x]
      qtyns12[x]
      costns12[x]
      qtyns24[x]
      costns24[x]
      qtyusg[x]
      " "
      " ".
   
   assign    
      qtyoh[x]     = 0
      costoh[x]    = 0 
      qtyls12[x]   = 0
      costls12[x]  = 0
      qty6[x]      = 0
      cost6[x]     = 0      
      qty12[x]     = 0
      cost12[x]    = 0
      qty24[x]     = 0
      cost24[x]    = 0
      qty36[x]     = 0
      cost36[x]    = 0
      qty48[x]     = 0
      cost48[x]    = 0
      qty60[x]     = 0
      cost60[x]    = 0
      qtyg60[x]    = 0
      costg60[x]   = 0
      qtyns12[x]   = 0
      costns12[x]  = 0
      qtyns24[x]   = 0
      costns24[x]  = 0
      qtyusg[x]    = 0.
  end.      
   
    
end.

assign x = 5.
display stream regfile  
     "**** Final Total"        @ aprod
     qtyoh[x]                  @ aqtyoh
     qtyls12[x]                @ aqtyls12
     qty6[x]                   @ aqty6
     qty12[x]                  @ aqty12
     qty24[x]                  @ aqty24
     qty36[x]                  @ aqty36
     (qty48[x] + qty60[x])     @ aqty60
     qtyg60[x]                 @ aqtyg60
     qtyns12[x]                @ aqtyns12
     qtyns24[x]                @ aqtyns24
     qtyusg[x]                 @ a12iss
   with frame f-det1.

display stream regfile 
     " "                       @ adesc
     costoh[x]                 @ acostoh
     costls12[x]               @ acostls12
     cost6[x]                  @ acost6
     cost12[x]                 @ acost12
     cost24[x]                 @ acost24
     cost36[x]                 @ acost36
     (cost48[x] + cost60[x])   @ acost60
     costg60[x]                @ acostg60
     costns12[x]               @ acostns12
     costns24[x]               @ acostns24
  with frame f-det2.

display stream regfile with frame f-space.

export stream bfile delimiter "~011"
    "**** "
    "Final Total"
    " "
    " "
    " "
    " "
    " "
    qtyoh[x]
    costoh[x]
    qtyls12[x]
    costls12[x]
    qty6[x]
    cost6[x]
    qty12[x]
    cost12[x]
    qty24[x]
    cost24[x]
    qty36[x] 
    cost36[x]
/*
    qty48[x]
    cost48[x]
*/
    (qty48[x] + qty60[x]) 
    (cost48[x] + cost60[x])
    qtyg60[x]
    costg60[x]
    qtyns12[x]
    costns12[x]
    qtyns24[x]
    costns24[x]
    qtyusg[x]
    " "
    " ".
 
assign    
    qtyoh[x]     = 0
    costoh[x]    = 0 
    qtyls12[x]   = 0
    costls12[x]  = 0
    qty6[x]      = 0
    cost6[x]     = 0
    qty12[x]     = 0
    cost12[x]    = 0
    qty24[x]     = 0
    cost24[x]    = 0
    qty36[x]     = 0
    cost36[x]    = 0
    qty48[x]     = 0
    cost48[x]    = 0
    qty60[x]     = 0
    cost60[x]    = 0
    qtyg60[x]    = 0
    costg60[x]   = 0
    qtyns12[x]   = 0
    costns12[x]  = 0
    qtyns24[x]   = 0
    costns24[x]  = 0
    qtyusg[x]    = 0.
 
end.




/* ------------------------------------------------------------------ */

procedure companyrpt:

/* ------------------------------------------------------------------ */

           

view frame f-headx.
view frame f-head1.
view frame f-head2.
view frame f-head3.
view frame f-head4.
view frame f-head5.
view frame f-head6.


export stream cfile delimiter "~011"
     " "
     " "
     " "
     " "
     " "
     " "
     " "
     "New"
     "New"
     "0 - 6"
     "0 - 6"
     "'7 - 12"
     "'7 - 12"
     " "
     " "
     " "         
     " "          
     " "
     " "
     " "
     " "
     "1 - 2 Year"  
     "1 - 2 Year" 
     "Gtr 2 Year"  
     "Gtr 2 Year" 
     " "
     " "
     " ".


export stream cfile delimiter "~011"
     " "
     " "
     "Vendor"
     "Product"
     "Description"
     "OnHand"
     "OnHand"
     "Item"
     "Item"
     "Months"
     "Months"
     "Months"
     "Months"
     "1 - 2 Year"
     "1 - 2 Year"
     "2 - 3 Year"         
     "2 - 3 Year"          
     "3 - 5 Year"
     "3 - 5 Year"
     "Gtr 5 Year"
     "Gtr 5 Year"
     "NoSales"  
     "NoSales" 
     "NoSales"  
     "NoSales" 
     "Usage"
     "SetUp"
     "Lst Sale".

export stream cfile delimiter "~011"
     " "
     " "
     " "
     " "
     " "
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"
     "Value" 
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"  
     "Value" 
     "Qty"  
     "Value" 
     "Qty"
     "Date"
     "Date".


           

do x = 1 to 5:  
  assign    
    qtyoh[x]     = 0
    costoh[x]    = 0 
    qtyls12[x]   = 0
    costls12[x]  = 0
    qty6[x]      = 0
    cost6[x]     = 0
    qty12[x]     = 0
    cost12[x]    = 0
    qty24[x]     = 0
    cost24[x]    = 0
    qty36[x]     = 0
    cost36[x]    = 0
    qty48[x]     = 0
    cost48[x]    = 0
    qty60[x]     = 0
    cost60[x]    = 0
    qtyg60[x]    = 0
    costg60[x]   = 0
    qtyns12[x]   = 0
    costns12[x]  = 0
    qtyns24[x]   = 0
    costns24[x]  = 0
    qtyusg[x]    = 0.
end.    
ttlusg = 0.
ttlcost = 0.
ttloh = 0.

for each itemz no-lock 
  break by   itemz.zvendor:
          
  
 ttloh =  itemz.zonhand.
 ttlusg = itemz.zusage.     
 ttlcost = itemz.zcost.
 savecost = ttlcost / ttloh.


 
 
 if ttlusg le 0 then 
    ttlusg = 0. 
 
 if ttloh > 0 then 
   do:
   avgdemd = ttlusg / 12.
   if avgdemd <> 0 then
     mos = ttloh / avgdemd.
   else
     mos = 9999.
   end.
 else
   do:
   avgdemd = 0. 
   mos = 0.
   ttlcost = 0.
   end.
        

 

wrkoh = ttloh.
do x = 1 to 5:
  assign qtyusg[x] = qtyusg[x] + itemz.zusage
         qtyoh[x] = qtyoh[x] + itemz.zonhand
         costoh[x] = costoh[x] + itemz.zcost.
end.


oh6[1]  = round((avgdemd * 6),0).
oh12[1] = round((avgdemd * 12),0).
oh24[1] = round((avgdemd * 24),0).
oh36[1] = round((avgdemd * 36),0).
oh48[1] = round((avgdemd * 48),0).
oh60[1] = round((avgdemd * 60),0).
 


if ((itemz.zentdt > today - 365) and itemz.zusage le 0) and 
    (itemz.zrcptdt = ? or itemz.zrcptdt > (today - 180))  then
 
  do:
  excess = wrkoh.
  do x = 1 to 5:
    assign 
           qtyls12 [x] = qtyls12[x] + excess
           costls12 [x] = costls12[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  mos = 0.
  qty60 = 0.
  end.

if ((itemz.zinvdt = ? and itemz.zentdt < today - 730) or
    (itemz.zinvdt <> ? and itemz.zinvdt < today - 730)) 
       and ttlusg = 0 then
  do:
  excess = wrkoh. 
  do x = 1 to 5:

    assign 
           qtyns24 [x] = qtyns24[x] + excess
           costns24 [x] = costns24[x] + (excess * savecost).
  end.
  wrkoh = wrkoh - excess.
  mos = 0.
  end.


if ((itemz.zinvdt = ? and itemz.zentdt < today - 365) or
    (itemz.zinvdt <> ? and itemz.zinvdt < today - 365)) 
       and ttlusg = 0 then
  do:
  excess = wrkoh. 
  do x = 1 to 5:

    assign 
           qtyns12 [x] = qtyns12[x] + excess
           costns12 [x] = costns12[x] + (excess * savecost).
  end.
  wrkoh = wrkoh - excess.
  mos = 0.
  end.
 
        
        
if mos > 60 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh60[1]
             else
               0).
do x = 1 to 5:

    assign 
           qtyg60 [x] = qtyg60[x] + excess
           costg60 [x] = costg60[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.

        
if mos > 48 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh48[1]
             else
               0).

  do x = 1 to 5:

    assign 
           qty60 [x] = qty60[x] + excess
           cost60 [x] = cost60[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
   
        
if mos > 36 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh36[1]
             else
               0).
  do x = 1 to 5:

    assign 
           qty48 [x] = qty48[x] + excess
           cost48 [x] = cost48[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
 


        
if mos > 24 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh24[1]
             else
               0).

  do x = 1 to 5:

    assign 
           qty36 [x] = qty36[x] + excess
           cost36 [x] = cost36[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
 

        
if mos > 12 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh12[1]
             else
               0).
  do x = 1 to 5:

    assign 
           qty24 [x] = qty24[x] + excess
           cost24 [x] = cost24[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
 
        
if mos > 6 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh6[1]
             else
               0).
  do x = 1 to 5:

    assign 
           qty12 [x] = qty12[x] + excess
           cost12 [x] = cost12[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.

        
if mos > 0 and wrkoh > 0 then 
  do:
  excess = wrkoh.
  do x = 1 to 5:

    assign 
           qty6 [x] = qty6[x] + excess
           cost6 [x] = cost6[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
  
 
assign x = 1.
if p-detail = "d" then
  do:
  display 
     itemz.zprod               @ aprod
     qtyoh[x]                  @ aqtyoh
     qtyls12[x]                @ aqtyls12
     qty6[x]                   @ aqty6
     qty12[x]                  @ aqty12
     qty24[x]                  @ aqty24
     qty36[x]                  @ aqty36
     (qty48[x] + qty60[x])     @ aqty60
     qtyg60[x]                 @ aqtyg60
     qtyns12[x]                @ aqtyns12
     qtyns24[x]                @ aqtyns24
     qtyusg[x]                 @ a12iss
   with frame f-det1.
  display
     itemz.zdescrip            @ adesc
     costoh[x]                 @ acostoh
     costls12[x]               @ acostls12
     cost6[x]                  @ acost6
     cost12[x]                 @ acost12
     cost24[x]                 @ acost24
     cost36[x]                 @ acost36
     (cost48[x] + cost60[x])   @ acost60
     costg60[x]                @ acostg60
     costns12[x]               @ acostns12
     costns24[x]               @ acostns24
  with frame f-det2.

  display with frame f-space.

  export stream cfile delimiter "~011"
     " "
     " "
     itemz.zvendor
     itemz.zprod
     itemz.zdescrip
     qtyoh[x]
     costoh[x]
     qtyls12[x]
     costls12[x]
     qty6[x]
     cost6[x]
     qty12[x]
     cost12[x]
     qty24[x]
     cost24[x]
     qty36[x] 
     cost36[x]
/*     
     qty48[x]
     cost48[x]
*/
     (qty48[x] + qty60[x])
     (cost48[x] + cost60[x])
     qtyg60[x]
     costg60[x]
     qtyns12[x]
     costns12[x]
     qtyns24[x]
     costns24[x]
     qtyusg[x]
     itemz.zentdt
     itemz.zinvdt.
  end.   

assign    
     qtyoh[x]     = 0 
     costoh[x]    = 0
     qtyls12[x]   = 0
     costls12[x]  = 0
     qty6[x]      = 0
     cost6[x]     = 0
     qty12[x]     = 0
     cost12[x]    = 0
     qty24[x]     = 0
     cost24[x]    = 0
     qty36[x]     = 0
     cost36[x]    = 0
     qty48[x]     = 0
     cost48[x]    = 0
     qty60[x]     = 0
     cost60[x]    = 0
     qtyg60[x]    = 0
     costg60[x]   = 0
     qtyns12[x]   = 0
     costns12[x]  = 0
     qtyns24[x]   = 0
     costns24[x]  = 0
     qtyusg[x]    = 0.
                    
if last-of (itemz.zvendor) then
  do:
  assign x = 2.
    display 
    (if p-detail = "s" then itemz.zvendor else
     "*Vendor")                @ aprod
     qtyoh[x]                  @ aqtyoh
     qtyls12[x]                @ aqtyls12
     qty6[x]                   @ aqty6
     qty12[x]                  @ aqty12
     qty24[x]                  @ aqty24
     qty36[x]                  @ aqty36
     (qty48[x] + qty60[x])     @ aqty60
     qtyg60[x]                 @ aqtyg60
     qtyns12[x]                @ aqtyns12
     qtyns24[x]                @ aqtyns24
     qtyusg[x]                 @ a12iss
   with frame f-det1.
  display            
     (if p-detail = "s" then " "
       else itemz.zvendor)     @ adesc
     costoh[x]                 @ acostoh
     costls12[x]               @ acostls12
     cost6[x]                  @ acost6
     cost12[x]                 @ acost12
     cost24[x]                 @ acost24
     cost36[x]                 @ acost36
     (cost48[x] + cost60[x])   @ acost60
     costg60[x]                @ acostg60
     costns12[x]               @ acostns12
     costns24[x]               @ acostns24
  with frame f-det2.

  display with frame f-space.
   
  export stream cfile delimiter "~011"
      "*    " 
      "Vendor"
      itemz.zvendor
      " "
      " "
      qtyoh[x]
      costoh[x]
      qtyls12[x]
      costls12[x]
      qty6[x]
      cost6[x]
      qty12[x]
      cost12[x]
      qty24[x]
      cost24[x]
      qty36[x] 
      cost36[x]
/*      
      qty48[x]
      cost48[x]
*/
      (qty48[x] + qty60[x])
      (cost48[x] + cost60[x])
      qtyg60[x]
      costg60[x]
      qtyns12[x]
      costns12[x]
      qtyns24[x]
      costns24[x]
      qtyusg[x]
      " "
      " ".
   
   assign    
      qtyoh[x]     = 0
      costoh[x]    = 0 
      qtyls12[x]   = 0
      costls12[x]  = 0
      qty6[x]      = 0
      cost6[x]     = 0 
      qty12[x]     = 0
      cost12[x]    = 0
      qty24[x]     = 0
      cost24[x]    = 0
      qty36[x]     = 0
      cost36[x]    = 0
      qty48[x]     = 0
      cost48[x]    = 0
      qty60[x]     = 0
      cost60[x]    = 0
      qtyg60[x]    = 0
      costg60[x]   = 0
      qtyns12[x]   = 0
      costns12[x]  = 0
      qtyns24[x]   = 0
      costns24[x]  = 0
      qtyusg[x]    = 0.
  end.      
    
end.

assign x = 5.
  display 
     "**** Final Total"        @ aprod
     qtyoh[x]                  @ aqtyoh
     qtyls12[x]                @ aqtyls12
     qty6[x]                   @ aqty6
     qty12[x]                  @ aqty12
     qty24[x]                  @ aqty24
     qty36[x]                  @ aqty36
     (qty48[x] + qty60[x])     @ aqty60
     qtyg60[x]                 @ aqtyg60
     qtyns12[x]                @ aqtyns12
     qtyns24[x]                @ aqtyns24
     qtyusg[x]                 @ a12iss
   with frame f-det1.
 
  display
     " "                       @ adesc
     costoh[x]                 @ acostoh
     costls12[x]               @ acostls12
     cost6[x]                  @ acost6
     cost12[x]                 @ acost12
     cost24[x]                 @ acost24
     cost36[x]                 @ acost36
     (cost48[x] + cost60[x])   @ acost60
     costg60[x]                @ acostg60
     costns12[x]               @ acostns12
     costns24[x]               @ acostns24
  with frame f-det2.

display with frame f-space.

export stream cfile delimiter "~011"
    "**** " 
    "Final Total"
    " "
    " "
    " "
    qtyoh[x]
    costoh[x]
    qtyls12[x]
    costls12[x]
    qty6[x]
    cost6[x]
    qty12[x]
    cost12[x]
    qty24[x]
    cost24[x]
    qty36[x] 
    cost36[x] 
/*
    qty48[x]
    cost48[x]
*/
    (qty48[x] + qty60[x])
    (cost48[x] + cost60[x])
    qtyg60[x]
    costg60[x]
    qtyns12[x]
    costns12[x]
    qtyns24[x]
    costns24[x]
    qtyusg[x]
    " "
    " ".
 
assign    
    qtyoh[x]     = 0
    costoh[x]    = 0 
    qtyls12[x]   = 0
    costls12[x]  = 0
    qty6[x]      = 0
    cost6[x]     = 0
    qty12[x]     = 0
    cost12[x]    = 0
    qty24[x]     = 0
    cost24[x]    = 0
    qty36[x]     = 0
    cost36[x]    = 0
    qty48[x]     = 0
    cost48[x]    = 0
    qty60[x]     = 0
    cost60[x]    = 0
    qtyg60[x]    = 0
    costg60[x]   = 0
    qtyns12[x]   = 0
    costns12[x]  = 0
    qtyns24[x]   = 0
    costns24[x]  = 0
    qtyusg[x]    = 0.
 
end.




/* ------------------------------------------------------------------ */

procedure companyregrpt:

/* ------------------------------------------------------------------ */

           

view frame f-headx.
view frame f-head1.
view frame f-head2.
view frame f-head3.
view frame f-head4.
view frame f-head5.
view frame f-head6.


export stream cfile delimiter "~011"
     " "
     " "
     " "
     " "
     " "
     " "
     " "
     "New"
     "New"
     "0 - 6"
     "0 - 6"
     "'7 - 12"
     "'7 - 12"
     " "
     " "
     " "         
     " "          
     " "
     " "
     " "
     " "
     "1 - 2 Year"  
     "1 - 2 Year" 
     "Gtr 2 Year"  
     "Gtr 2 Year" 
     " "
     " "
     " ".


export stream cfile delimiter "~011"
     " "
     " "
     "Vendor"
     "Product"
     "Description"
     "OnHand"
     "OnHand"
     "Item"
     "Item"
     "Months"
     "Months"
     "Months"
     "Months"
     "1 - 2 Year"
     "1 - 2 Year"
     "2 - 3 Year"         
     "2 - 3 Year"          
     "3 - 5 Year"
     "3 - 5 Year"
     "Gtr 5 Year"
     "Gtr 5 Year"
     "NoSales"  
     "NoSales" 
     "NoSales"  
     "NoSales" 
     "Usage"
     "SetUp"
     "Lst Sale".

export stream cfile delimiter "~011"
     " "
     " "
     " "
     " "
     " "
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"
     "Value" 
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"  
     "Value" 
     "Qty"  
     "Value" 
     "Qty"
     "Date"
     "Date".


           

do x = 1 to 5:  
  assign    
    qtyoh[x]     = 0
    costoh[x]    = 0 
    qtyls12[x]   = 0
    costls12[x]  = 0
    qty6[x]      = 0
    cost6[x]     = 0
    qty12[x]     = 0
    cost12[x]    = 0
    qty24[x]     = 0
    cost24[x]    = 0
    qty36[x]     = 0
    cost36[x]    = 0
    qty48[x]     = 0
    cost48[x]    = 0
    qty60[x]     = 0
    cost60[x]    = 0
    qtyg60[x]    = 0
    costg60[x]   = 0
    qtyns12[x]   = 0
    costns12[x]  = 0
    qtyns24[x]   = 0
    costns24[x]  = 0
    qtyusg[x]    = 0.
end.    
ttlusg = 0.
ttlcost = 0.
ttloh = 0.

for each itemw use-index w-prod no-lock 
  break by   itemw.zvendor
        by   itemw.zprod:
          
  
 ttloh =  itemw.zonhand.
 ttlusg = itemw.zusage.     
 ttlcost = itemw.zcost.
 savecost = ttlcost / ttloh.


 
 
 if ttlusg le 0 then 
    ttlusg = 0. 
 
 if ttloh > 0 then 
   do:
   avgdemd = ttlusg / 12.
   if avgdemd <> 0 then
     mos = ttloh / avgdemd.
   else
     mos = 9999.
   end.
 else
   do:
   avgdemd = 0. 
   mos = 0.
   ttlcost = 0.
   end.
        

 

wrkoh = ttloh.
do x = 1 to 5:
  assign qtyusg[x] = qtyusg[x] + itemw.zusage
         qtyoh[x] = qtyoh[x] + itemw.zonhand
         costoh[x] = costoh[x] + itemw.zcost.
end.


oh6[1]  = round((avgdemd * 6),0).
oh12[1] = round((avgdemd * 12),0).
oh24[1] = round((avgdemd * 24),0).
oh36[1] = round((avgdemd * 36),0).
oh48[1] = round((avgdemd * 48),0).
oh60[1] = round((avgdemd * 60),0).
 


if ((itemw.zentdt > today - 365) and itemw.zusage le 0) and 
    (itemw.zrcptdt = ? or itemw.zrcptdt > (today - 180))  then
 
  do:
  excess = wrkoh.
  do x = 1 to 5:
    assign 
           qtyls12 [x] = qtyls12[x] + excess
           costls12 [x] = costls12[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  mos = 0.
  qty60 = 0.
  end.

if ((itemw.zinvdt = ? and itemw.zentdt < today - 730) or
    (itemw.zinvdt <> ? and itemw.zinvdt < today - 730)) 
       and ttlusg = 0 then
  do:
  excess = wrkoh. 
  do x = 1 to 5:

    assign 
           qtyns24 [x] = qtyns24[x] + excess
           costns24 [x] = costns24[x] + (excess * savecost).
  end.
  wrkoh = wrkoh - excess.
  mos = 0.
  end.


if ((itemw.zinvdt = ? and itemw.zentdt < today - 365) or
    (itemw.zinvdt <> ? and itemw.zinvdt < today - 365)) 
       and ttlusg = 0 then
  do:
  excess = wrkoh. 
  do x = 1 to 5:

    assign 
           qtyns12 [x] = qtyns12[x] + excess
           costns12 [x] = costns12[x] + (excess * savecost).
  end.
  wrkoh = wrkoh - excess.
  mos = 0.
  end.
 
        
        
if mos > 60 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh60[1]
             else
               0).
do x = 1 to 5:

    assign 
           qtyg60 [x] = qtyg60[x] + excess
           costg60 [x] = costg60[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.

        
if mos > 48 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh48[1]
             else
               0).

  do x = 1 to 5:

    assign 
           qty60 [x] = qty60[x] + excess
           cost60 [x] = cost60[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
   
        
if mos > 36 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh36[1]
             else
               0).
  do x = 1 to 5:

    assign 
           qty48 [x] = qty48[x] + excess
           cost48 [x] = cost48[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
 


        
if mos > 24 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh24[1]
             else
               0).

  do x = 1 to 5:

    assign 
           qty36 [x] = qty36[x] + excess
           cost36 [x] = cost36[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
 

        
if mos > 12 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh12[1]
             else
               0).
  do x = 1 to 5:

    assign 
           qty24 [x] = qty24[x] + excess
           cost24 [x] = cost24[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
 
        
if mos > 6 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh6[1]
             else
               0).
  do x = 1 to 5:

    assign 
           qty12 [x] = qty12[x] + excess
           cost12 [x] = cost12[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.

        
if mos > 0 and wrkoh > 0 then 
  do:
  excess = wrkoh.
  do x = 1 to 5:

    assign 
           qty6 [x] = qty6[x] + excess
           cost6 [x] = cost6[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
  
if last-of(itemw.zprod) then 
  do:
  assign x = 1.
  if p-detail = "d" then
    do:
    display 
       itemw.zprod               @ aprod
       qtyoh[x]                  @ aqtyoh
       qtyls12[x]                @ aqtyls12
       qty6[x]                   @ aqty6
       qty12[x]                  @ aqty12
       qty24[x]                  @ aqty24
       qty36[x]                  @ aqty36
       (qty48[x] + qty60[x])     @ aqty60
       qtyg60[x]                 @ aqtyg60
       qtyns12[x]                @ aqtyns12
       qtyns24[x]                @ aqtyns24
       qtyusg[x]                 @ a12iss
     with frame f-det1.
    display
       itemw.zdescrip            @ adesc
       costoh[x]                 @ acostoh
       costls12[x]               @ acostls12
       cost6[x]                  @ acost6
       cost12[x]                 @ acost12
       cost24[x]                 @ acost24
       cost36[x]                 @ acost36
       (cost48[x] + cost60[x])   @ acost60
       costg60[x]                @ acostg60
       costns12[x]               @ acostns12
       costns24[x]               @ acostns24
    with frame f-det2.

    display with frame f-space.

    export stream cfile delimiter "~011"
       " "
       " "
       itemw.zvendor
       itemw.zprod
       itemw.zdescrip
       qtyoh[x]
       costoh[x]
       qtyls12[x]
       costls12[x]
       qty6[x]
       cost6[x]
       qty12[x]
       cost12[x]
       qty24[x]
       cost24[x]
       qty36[x] 
       cost36[x] 
/*     
       qty48[x]
       cost48[x]
*/
       (qty48[x] + qty60[x])
       (cost48[x] + cost60[x])
       qtyg60[x]
       costg60[x]
       qtyns12[x]
       costns12[x]
       qtyns24[x]
       costns24[x]
       qtyusg[x]
       itemw.zentdt
       itemw.zinvdt.
    end.   

  assign    
     qtyoh[x]     = 0 
     costoh[x]    = 0
     qtyls12[x]   = 0
     costls12[x]  = 0
     qty6[x]      = 0
     cost6[x]     = 0
     qty12[x]     = 0
     cost12[x]    = 0
     qty24[x]     = 0
     cost24[x]    = 0
     qty36[x]     = 0
     cost36[x]    = 0
     qty48[x]     = 0
     cost48[x]    = 0
     qty60[x]     = 0
     cost60[x]    = 0
     qtyg60[x]    = 0
     costg60[x]   = 0
     qtyns12[x]   = 0
     costns12[x]  = 0
     qtyns24[x]   = 0
     costns24[x]  = 0
     qtyusg[x]    = 0.
  end.           
         
if last-of (itemw.zvendor) then
  do:
  assign x = 2.
    display 
    (if p-detail = "s" then itemw.zvendor else
     "*Vendor")                @ aprod
     qtyoh[x]                  @ aqtyoh
     qtyls12[x]                @ aqtyls12
     qty6[x]                   @ aqty6
     qty12[x]                  @ aqty12
     qty24[x]                  @ aqty24
     qty36[x]                  @ aqty36
     (qty48[x] + qty60[x])     @ aqty60
     qtyg60[x]                 @ aqtyg60
     qtyns12[x]                @ aqtyns12
     qtyns24[x]                @ aqtyns24
     qtyusg[x]                 @ a12iss
   with frame f-det1.
  display            
     (if p-detail = "s" then " "
       else itemw.zvendor)     @ adesc
     costoh[x]                 @ acostoh
     costls12[x]               @ acostls12
     cost6[x]                  @ acost6
     cost12[x]                 @ acost12
     cost24[x]                 @ acost24
     cost36[x]                 @ acost36
     (cost48[x] + cost60[x])   @ acost60
     costg60[x]                @ acostg60
     costns12[x]               @ acostns12
     costns24[x]               @ acostns24
  with frame f-det2.

  display with frame f-space.
   
  export stream cfile delimiter "~011"
      "*    " 
      "Vendor"
      itemw.zvendor
      " "
      " "
      qtyoh[x]
      costoh[x]
      qtyls12[x]
      costls12[x]
      qty6[x]
      cost6[x]
      qty12[x]
      cost12[x]
      qty24[x]
      cost24[x]
      qty36[x] 
      cost36[x]
/*      
      qty48[x]
      cost48[x]
*/
      (qty48[x] + qty60[x])
      (cost48[x] + cost60[x])
      qtyg60[x]
      costg60[x]
      qtyns12[x]
      costns12[x]
      qtyns24[x]
      costns24[x]
      qtyusg[x]
      " "
      " ".
   
   assign    
      qtyoh[x]     = 0
      costoh[x]    = 0 
      qtyls12[x]   = 0
      costls12[x]  = 0
      qty6[x]      = 0
      cost6[x]     = 0 
      qty12[x]     = 0
      cost12[x]    = 0
      qty24[x]     = 0
      cost24[x]    = 0
      qty36[x]     = 0
      cost36[x]    = 0
      qty48[x]     = 0
      cost48[x]    = 0
      qty60[x]     = 0
      cost60[x]    = 0
      qtyg60[x]    = 0
      costg60[x]   = 0
      qtyns12[x]   = 0
      costns12[x]  = 0
      qtyns24[x]   = 0
      costns24[x]  = 0
      qtyusg[x]    = 0.
  end.      
    
end.

assign x = 5.
  display 
     "**** Final Total"        @ aprod
     qtyoh[x]                  @ aqtyoh
     qtyls12[x]                @ aqtyls12
     qty6[x]                   @ aqty6
     qty12[x]                  @ aqty12
     qty24[x]                  @ aqty24
     qty36[x]                  @ aqty36
     (qty48[x] + qty60[x])     @ aqty60
     qtyg60[x]                 @ aqtyg60
     qtyns12[x]                @ aqtyns12
     qtyns24[x]                @ aqtyns24
     qtyusg[x]                 @ a12iss
   with frame f-det1.
 
  display
     " "                       @ adesc
     costoh[x]                 @ acostoh
     costls12[x]               @ acostls12
     cost6[x]                  @ acost6
     cost12[x]                 @ acost12
     cost24[x]                 @ acost24
     cost36[x]                 @ acost36
     (cost48[x] + cost60[x])   @ acost60
     costg60[x]                @ acostg60
     costns12[x]               @ acostns12
     costns24[x]               @ acostns24
  with frame f-det2.

display with frame f-space.

export stream cfile delimiter "~011"
    "**** " 
    "Final Total"
    " "
    " "
    " "
    qtyoh[x]
    costoh[x]
    qtyls12[x]
    costls12[x]
    qty6[x]
    cost6[x]
    qty12[x]
    cost12[x]
    qty24[x]
    cost24[x]
    qty36[x] 
    cost36[x] 
/*
    qty48[x]
    cost48[x]
*/
    (qty48[x] + qty60[x])
    (cost48[x] + cost60[x])
    qtyg60[x]
    costg60[x]
    qtyns12[x]
    costns12[x]
    qtyns24[x]
    costns24[x]
    qtyusg[x]
    " "
    " ".
 
assign    
    qtyoh[x]     = 0
    costoh[x]    = 0 
    qtyls12[x]   = 0
    costls12[x]  = 0
    qty6[x]      = 0
    cost6[x]     = 0
    qty12[x]     = 0
    cost12[x]    = 0
    qty24[x]     = 0
    cost24[x]    = 0
    qty36[x]     = 0
    cost36[x]    = 0
    qty48[x]     = 0
    cost48[x]    = 0
    qty60[x]     = 0
    cost60[x]    = 0
    qtyg60[x]    = 0
    costg60[x]   = 0
    qtyns12[x]   = 0
    costns12[x]  = 0
    qtyns24[x]   = 0
    costns24[x]  = 0
    qtyusg[x]    = 0.
 
end.




/* --------------------------------------------------------------------- */

procedure distribute_value:

/* --------------------------------------------------------------------- */


  if ttloh = 0 then
    next.
 
  jj = 0.

  run weight_item2   in sdi-tools 
     (input-output x-range,
      input       itemz.zprod,
      input       itemz.zpcat,
      input       g-cono,
      input       ttloh).
      
    jj = 0.
  for each xpart:
    if xpart.wxpct = 0 then
      next.  

    jj = jj + xpart.wxpct.  
    find itemw where itemw.zregion = 
                          substring(xpart.wxreg,1,1) and
                     itemw.zdistrict =                                      
                          substring(xpart.wxreg,2,3) and
                    itemw.zprod = icsp.prod
                    no-error.
    if not avail itemw then
       do:
       create itemw.
       assign
              itemw.zregion = 
                      substring(xpart.wxreg,1,1)               
              itemw.zdistrict =                                      
                      substring(xpart.wxreg,2,3)               
              itemw.zprod  =  icsp.prod 
              
              itemw.zbrk      =  xpart.wxpct
              itemw.zusage    = itemz.zusage
                              * xpart.wxpct
              itemw.zonhand   = itemz.zonhand
                              * xpart.wxpct
              itemw.zcost     = itemz.zcost
                              * xpart.wxpct
              itemw.zentdt    = itemz.zentdt
              itemw.zrcptdt    = itemz.zrcptdt
              itemw.zpcat     = itemz.zpcat
              itemw.zdescrip  =  itemz.zdescrip
              itemw.zinvdt    =  itemz.zinvdt              
              
              itemw.zvendor   = itemz.zvendor.
         end.
       else
         assign
              itemw.zusage   = (itemz.zusage  
                              * xpart.wxpct)
                              + itemw.zusage
              itemw.zonhand  = (itemz.zonhand 
                              * xpart.wxpct)
                              +  itemw.zonhand
              itemw.zcost     = (itemz.zcost 
                              * xpart.wxpct)
                              +  itemw.zcost.
       
 
  
    end.
 end.
 
 
procedure whsesummary:
 
 put stream logfile control compress-string.  

 view stream logfile frame f-heady.
 view stream logfile frame f-head1.
 view stream logfile frame f-head2.   
 view stream logfile frame f-whsehead.
 
 for each whsval use-index typeix  no-lock break by whsval.type:
  
   display stream logfile 
     whsval.whse
     whsval.name   
     whsval.divno  
     whsval.addr1  
  /*   whsval.addr2  */
     whsval.city   
     whsval.state  
     whsval.zip    
     whsval.avalue  
     whsval.lvalue
     with frame f-whseval.
  down with frame f-whseval.
  assign
    typevall = typevall + whsval.lvalue    
    typevala = typevala + whsval.avalue    
    finalvall = finalvall + whsval.lvalue    
    finalvala = finalvala + whsval.avalue.    
  if last-of(whsval.type) then
    do:
    display stream logfile
     typevala
     typevall
    with frame f-whsetot.
    assign
      typevall = 0
      typevala = 0.
    end.   
 end.

    display stream logfile
     finalvala
     finalvall
    with frame f-whsefinal.

end. 

 
procedure whsexlcsummary:
 

 hide stream logfile frame f-heady.
 hide stream logfile frame f-head1.
 hide stream logfile frame f-head2.   
 hide stream logfile frame f-whsehead.
 
 for each whsval use-index typeix  no-lock break by whsval.type:
  
   export  stream logfile delimiter "~011"
     whsval.whse
     whsval.name   
     whsval.divno  
     whsval.addr1  
  /*   whsval.addr2  */
     whsval.city   
     whsval.state  
     whsval.zip    
     whsval.avalue  
     whsval.lvalue.
  assign
    typevall = typevall + whsval.lvalue    
    typevala = typevala + whsval.avalue    
    finalvall = finalvall + whsval.lvalue    
    finalvala = finalvala + whsval.avalue.    
  if last-of(whsval.type) then
    do:
    export stream logfile delimiter "~011"
     (if whsval.type = false then
       "Whse Total"
      else  
       "Consignment Total")
     " "
     " "
     " "
     " "
     " "
     " "
     typevala
     typevall.
    assign
      typevall = 0
      typevala = 0.
    end.   
 end.

    export stream logfile delimiter "~011"
     "Final Total"
     " "
     " "
     " "
     " "
     " "
     " "
     finalvala
     finalvall.

end. 



/* ------------------------------------------------------------------ */

procedure icxmxtract:

/* ------------------------------------------------------------------ */

           

           

do x = 1 to 5:  
  assign    
    qtyoh[x]     = 0
    costoh[x]    = 0 
    qtyls12[x]   = 0
    costls12[x]  = 0
    qty6[x]      = 0
    cost6[x]     = 0
    qty12[x]     = 0
    cost12[x]    = 0
    qty24[x]     = 0
    cost24[x]    = 0
    qty36[x]     = 0
    cost36[x]    = 0
    qty48[x]     = 0
    cost48[x]    = 0
    qty60[x]     = 0
    cost60[x]    = 0
    qtyg60[x]    = 0
    costg60[x]   = 0
    qtyns12[x]   = 0
    costns12[x]  = 0
    qtyns24[x]   = 0
    costns24[x]  = 0
    qtyusg[x]    = 0.
end.    
ttlusg = 0.
ttlcost = 0.
ttloh = 0.

for each itemz no-lock 
  break by   itemz.zvendor:
          
  
 ttloh =  itemz.zonhand.
 ttlusg = itemz.zusage.     
 ttlcost = itemz.zcost.
 savecost = ttlcost / ttloh.


 
 
 if ttlusg le 0 then 
    ttlusg = 0. 
 
 if ttloh > 0 then 
   do:
   avgdemd = ttlusg / 12.
   if avgdemd <> 0 then
     mos = ttloh / avgdemd.
   else
     mos = 9999.
   end.
 else
   do:
   avgdemd = 0. 
   mos = 0.
   ttlcost = 0.
   end.
        

 

wrkoh = ttloh.
do x = 1 to 5:
  assign qtyusg[x] = qtyusg[x] + itemz.zusage
         qtyoh[x] = qtyoh[x] + itemz.zonhand
         costoh[x] = costoh[x] + itemz.zcost.
end.


oh6[1]  = round((avgdemd * 6),0).
oh12[1] = round((avgdemd * 12),0).
oh24[1] = round((avgdemd * 24),0).
oh36[1] = round((avgdemd * 36),0).
oh48[1] = round((avgdemd * 48),0).
oh60[1] = round((avgdemd * 60),0).
 


if ((itemz.zentdt > today - 365) and itemz.zusage le 0) and 
    (itemz.zrcptdt = ? or itemz.zrcptdt > (today - 180))  then
 
  do:
  excess = wrkoh.
  do x = 1 to 5:
    assign 
           qtyls12 [x] = qtyls12[x] + excess
           costls12 [x] = costls12[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  mos = 0.
  qty60 = 0.
  end.

if ((itemz.zinvdt = ? and itemz.zentdt < today - 730) or
    (itemz.zinvdt <> ? and itemz.zinvdt < today - 730)) 
       and ttlusg = 0 then
  do:
  excess = wrkoh. 
  do x = 1 to 5:

    assign 
           qtyns24 [x] = qtyns24[x] + excess
           costns24 [x] = costns24[x] + (excess * savecost).
  end.
  wrkoh = wrkoh - excess.
  mos = 0.
  end.


if ((itemz.zinvdt = ? and itemz.zentdt < today - 365) or
    (itemz.zinvdt <> ? and itemz.zinvdt < today - 365)) 
       and ttlusg = 0 then
  do:
  excess = wrkoh. 
  do x = 1 to 5:

    assign 
           qtyns12 [x] = qtyns12[x] + excess
           costns12 [x] = costns12[x] + (excess * savecost).
  end.
  wrkoh = wrkoh - excess.
  mos = 0.
  end.
 
        
        
if mos > 60 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh60[1]
             else
               0).
do x = 1 to 5:

    assign 
           qtyg60 [x] = qtyg60[x] + excess
           costg60 [x] = costg60[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.

        
if mos > 48 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh48[1]
             else
               0).

  do x = 1 to 5:

    assign 
           qty60 [x] = qty60[x] + excess
           cost60 [x] = cost60[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
   
        
if mos > 36 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh36[1]
             else
               0).
  do x = 1 to 5:

    assign 
           qty48 [x] = qty48[x] + excess
           cost48 [x] = cost48[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
 


        
if mos > 24 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh24[1]
             else
               0).

  do x = 1 to 5:

    assign 
           qty36 [x] = qty36[x] + excess
           cost36 [x] = cost36[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
 

        
if mos > 12 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh12[1]
             else
               0).
  do x = 1 to 5:

    assign 
           qty24 [x] = qty24[x] + excess
           cost24 [x] = cost24[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
 
        
if mos > 6 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh6[1]
             else
               0).
  do x = 1 to 5:

    assign 
           qty12 [x] = qty12[x] + excess
           cost12 [x] = cost12[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.

        
if mos > 0 and wrkoh > 0 then 
  do:
  excess = wrkoh.
  do x = 1 to 5:

    assign 
           qty6 [x] = qty6[x] + excess
           cost6 [x] = cost6[x] + (excess * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
  
 
assign x = 1.
if p-detail = "d" then
  do:
  if (qtyls12[x] > 0 or
      qty6[x] > 0 or
      qty12[x] > 0 or
      qty24[x] > 0 or
      qty36[x] > 0 or
      qty48[x] > 0 or
      qty60[x] > 0 or
      qtyg60[x] > 0 or
      qtyns12[x] > 0 or
      qtyns24[x] > 0) then
  export stream logfile delimiter "~011"
     itemz.zprod              
     itemz.zdescrip           
     " "
     " "
     qtyls12[x]
     qty6 [x]
     qty12[x]
     qty24[x]
     qty36[x] 
     (qty48[x] + qty60[x])
     qtyg60[x]
     qtyns12[x]
     qtyns24[x]                
     itemz.zonhand
     today
     itemz.zentdt
     itemz.zrcptdt
     itemz.zinvdt.

  end.
assign    
     qtyoh[x]     = 0 
     costoh[x]    = 0
     qtyls12[x]   = 0
     costls12[x]  = 0
     qty6[x]      = 0
     cost6[x]     = 0
     qty12[x]     = 0
     cost12[x]    = 0
     qty24[x]     = 0
     cost24[x]    = 0
     qty36[x]     = 0
     cost36[x]    = 0
     qty48[x]     = 0
     cost48[x]    = 0
     qty60[x]     = 0
     cost60[x]    = 0
     qtyg60[x]    = 0
     costg60[x]   = 0
     qtyns12[x]   = 0
     costns12[x]  = 0
     qtyns24[x]   = 0
     costns24[x]  = 0
     qtyusg[x]    = 0.
    
end.
assign    
    qtyoh[x]     = 0
    costoh[x]    = 0 
    qtyls12[x]   = 0
    costls12[x]  = 0
    qty6[x]      = 0
    cost6[x]     = 0
    qty12[x]     = 0
    cost12[x]    = 0
    qty24[x]     = 0
    cost24[x]    = 0
    qty36[x]     = 0
    cost36[x]    = 0
    qty48[x]     = 0
    cost48[x]    = 0
    qty60[x]     = 0
    cost60[x]    = 0
    qtyg60[x]    = 0
    costg60[x]   = 0
    qtyns12[x]   = 0
    costns12[x]  = 0
    qtyns24[x]   = 0
    costns24[x]  = 0
    qtyusg[x]    = 0.
 
end.




/* ------------------------------------------------------------------ */

procedure warehouserpt:

/* ------------------------------------------------------------------ */

           

view stream regfile frame f-headz.
view stream regfile frame f-head1.
view stream regfile frame f-head2.
view stream regfile frame f-head3.
view stream regfile frame f-head4w.
view stream regfile frame f-head5.
view stream regfile frame f-head6.


export stream bfile delimiter "~011"
     " "
     " "
     " "
     " "
     " "
     " "
     " "
     "New"
     "New"
     "0 - 6"
     "0 - 6"
     "'7 - 12"
     "'7 - 12"
     " "
     " "
     " "         
     " "          
     " "
     " "
     " "
     " "
     "1 - 2 Year"  
     "1 - 2 Year" 
     "Gtr 2 Year"  
     "Gtr 2 Year" 
     " "
     " "
     " ".
     /*
     " "
     " ".
     */

export stream bfile delimiter "~011"
     " "
     "Whse"
     "Vendor"
     "Product"
     "Description"
     "OnHand"
     "OnHand"
     "Item"
     "Item"
     "Months"
     "Months"
     "Months"
     "Months"
     "1 - 2 Year"
     "1 - 2 Year"
     "2 - 3 Year"         
     "2 - 3 Year"          
     "3 - 5 Year"
     "3 - 5 Year"
     "Gtr 5 Year"
     "Gtr 5 Year"
     "NoSales"  
     "NoSales" 
     "NoSales"  
     "NoSales" 
     "Usage"
     "Lower "
     "Average".
     /*
     "SetUp"
     "Lst Sale".
     */
export stream bfile delimiter "~011"
     " "
     " "
     " "
     " "
     " "
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"
     "Value" 
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"
     "Value"
     "Qty"  
     "Value" 
     "Qty"  
     "Value" 
     "Qty"
     "Cost"
     "Cost".
     /*
     "Date"
     "Date".
     */

           

do x = 1 to 5:  
  assign    
    qtyoh[x]     = 0
    costoh[x]    = 0 
    qtyls12[x]   = 0
    costls12[x]  = 0
    qty6[x]      = 0
    cost6[x]     = 0
    qty12[x]     = 0
    cost12[x]    = 0
    qty24[x]     = 0
    cost24[x]    = 0
    qty36[x]     = 0
    cost36[x]    = 0
    qty48[x]     = 0
    cost48[x]    = 0
    qty60[x]     = 0
    cost60[x]    = 0
    qtyg60[x]    = 0
    costg60[x]   = 0
    qtyns12[x]   = 0
    costns12[x]  = 0
    qtyns24[x]   = 0
    costns24[x]  = 0
    qtyusg[x]    = 0.
end.    
ttlusg = 0.
ttlcost = 0.
ttloh = 0.
for each whsqty no-lock
         break by  whsqty.whse
               by  whsqty.vendor:
  find itemz where itemz.zprod = whsqty.prod no-lock no-error. 
  if not avail itemz then 
    next.      

 find icsd where icsd.cono = g-cono and
                 icsd.whse = whsqty.whse no-lock no-error.
    
  
 ttloh =  itemz.zonhand.
 ttlusg = itemz.zusage.     
 ttlcost = itemz.zcost.
 /*
 savecost = ttlcost / ttloh.
 */
 
 savecost = whsqty.vcost.
 x-mult = whsqty.qtyoh / itemz.zonhand.
 
 
 if ttlusg le 0 then 
    ttlusg = 0. 
 
 if ttloh > 0 then 
   do:
   avgdemd = ttlusg / 12.
   if avgdemd <> 0 then
     mos = ttloh / avgdemd.
   else
     mos = 9999.
   end.
 else
   do:
   avgdemd = 0. 
   mos = 0.
   ttlcost = 0.
   end.
        

 

wrkoh = ttloh.
do x = 1 to 5:
  assign qtyusg[x] = qtyusg[x] + (itemz.zusage * x-mult)
         qtyoh[x] = qtyoh[x] + (whsqty.qtyoh)
         costoh[x] = costoh[x] + (savecost * whsqty.qtyoh).
end.


oh6[1]  = round((avgdemd * 6),0).
oh12[1] = round((avgdemd * 12),0).
oh24[1] = round((avgdemd * 24),0).
oh36[1] = round((avgdemd * 36),0).
oh48[1] = round((avgdemd * 48),0).
oh60[1] = round((avgdemd * 60),0).
 


if ((itemz.zentdt > today - 365) and itemz.zusage le 0) and 
    (itemz.zrcptdt = ? or itemz.zrcptdt > (today - 180))  then
 
  do:
  excess = wrkoh.
  do x = 1 to 5:
    assign 
           qtyls12 [x] = qtyls12[x] + (excess * x-mult)
           costls12 [x] = costls12[x] + ((excess * x-mult) * savecost).
  end.

  wrkoh = wrkoh - excess.
  mos = 0.
  qty60 = 0.
  end.

if ((itemz.zinvdt = ? and itemz.zentdt < today - 730) or
    (itemz.zinvdt <> ? and itemz.zinvdt < today - 730)) 
       and ttlusg = 0 then
  do:
  excess = wrkoh. 
  do x = 1 to 5:

    assign 
           qtyns24 [x] = qtyns24[x] + (excess * x-mult)
           costns24 [x] = costns24[x] + ((excess * x-mult) * savecost).
  end.
  wrkoh = wrkoh - excess.
  mos = 0.
  end.


if ((itemz.zinvdt = ? and itemz.zentdt < today - 365) or
    (itemz.zinvdt <> ? and itemz.zinvdt < today - 365)) 
       and ttlusg = 0 then
  do:
  excess = wrkoh. 
  do x = 1 to 5:

    assign 
           qtyns12 [x] = qtyns12[x] + (excess * x-mult)
           costns12 [x] = costns12[x] + ((excess * x-mult) * savecost).
  end.
  wrkoh = wrkoh - excess.
  mos = 0.
  end.
 
        
        
if mos > 60 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh60[1]
             else
               0).
do x = 1 to 5:

    assign 
           qtyg60 [x] = qtyg60[x] + (excess * x-mult)
           costg60 [x] = costg60[x] + ((excess * x-mult) * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.

        
if mos > 48 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh48[1]
             else
               0).

  do x = 1 to 5:

    assign 
           qty60 [x] = qty60[x] + (excess * x-mult)
           cost60 [x] = cost60[x] + ((excess * x-mult) * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
   
        
if mos > 36 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh36[1]
             else
               0).
  do x = 1 to 5:

    assign 
           qty48 [x] = qty48[x] + (excess * x-mult)
           cost48 [x] = cost48[x] + ((excess * x-mult) * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
 


        
if mos > 24 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh24[1]
             else
               0).

  do x = 1 to 5:

    assign 
           qty36 [x] = qty36[x] + (excess * x-mult)
           cost36 [x] = cost36[x] + ((excess * x-mult) * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
 

        
if mos > 12 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh12[1]
             else
               0).
  do x = 1 to 5:

    assign 
           qty24 [x] = qty24[x] + (excess * x-mult)
           cost24 [x] = cost24[x] + ((excess * x-mult) * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
 
        
if mos > 6 and wrkoh > 0 then 
  do:
  excess = wrkoh - 
            (if p-layer then 
               oh6[1]
             else
               0).
  do x = 1 to 5:

    assign 
           qty12 [x] = qty12[x] + (excess * x-mult)
           cost12 [x] = cost12[x] + ((excess * x-mult) * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.

        
if mos > 0 and wrkoh > 0 then 
  do:
  excess = wrkoh.
  do x = 1 to 5:

    assign 
           qty6 [x] = qty6[x] + (excess * x-mult)
           cost6 [x] = cost6[x] + ((excess * x-mult) * savecost).
  end.

  wrkoh = wrkoh - excess.
  end.
  
 
assign x = 1.
if p-detail = "d" then
  do:
  display stream regfile 
     itemz.zprod               @ aprod
     qtyoh[x]                  @ aqtyoh
     qtyls12[x]                @ aqtyls12
     qty6[x]                   @ aqty6
     qty12[x]                  @ aqty12
     qty24[x]                  @ aqty24
     qty36[x]                  @ aqty36
     (qty48[x] + qty60[x])     @ aqty60
     qtyg60[x]                 @ aqtyg60
     qtyns12[x]                @ aqtyns12
     qtyns24[x]                @ aqtyns24
     qtyusg[x]                 @ a12iss
     whsqty.whse
   with frame f-det3.
  display stream regfile 
     itemz.zdescrip            @ adesc
     costoh[x]                 @ acostoh
     costls12[x]               @ acostls12
     cost6[x]                  @ acost6
     cost12[x]                 @ acost12
     cost24[x]                 @ acost24
     cost36[x]                 @ acost36
     (cost48[x] + cost60[x])   @ acost60
     costg60[x]                @ acostg60
     costns12[x]               @ acostns12
     costns24[x]               @ acostns24
  with frame f-det2.

  display stream regfile with frame f-space.

  export stream bfile delimiter "~011"
     " "
     whsqty.whse
     itemz.zvendor
     itemz.zprod
     itemz.zdescrip
     qtyoh[x]
     costoh[x]
     qtyls12[x]
     costls12[x]
     qty6[x]
     cost6[x]
     qty12[x]
     cost12[x]
     qty24[x]
     cost24[x]
     qty36[x] 
     cost36[x]
/*     
     qty48[x]
     cost48[x]
*/
     (qty48[x] + qty60[x])
     (cost48[x] + cost60[x])
     qtyg60[x]
     costg60[x]
     qtyns12[x]
     costns12[x]
     qtyns24[x]
     costns24[x]
     qtyusg[x]
     whsqty.vcost
     whsqty.acost
     (if (can-do("c,a,v,z,k",substring(whsqty.whse,1,1)) and 
         whsqty.whse <> "csei" and
         whsqty.whse <> "chry" and
         whsqty.whse <> "ctip" and
         whsqty.whse <> "cimt") or
         (avail icsd and (substring(icsd.city,1,10) = "Do Not Use" or
                        whsqty.whse = "****") 
                   and whsqty.whse <> "dnwg")  then
                   
       "Yes"
      else
       "No").
     /*
     itemz.zentdt
     itemz.zinvdt.
     */
  end.   

assign    
     qtyoh[x]     = 0 
     costoh[x]    = 0
     qtyls12[x]   = 0
     costls12[x]  = 0
     qty6[x]      = 0
     cost6[x]     = 0
     qty12[x]     = 0
     cost12[x]    = 0
     qty24[x]     = 0
     cost24[x]    = 0
     qty36[x]     = 0
     cost36[x]    = 0
     qty48[x]     = 0
     cost48[x]    = 0
     qty60[x]     = 0
     cost60[x]    = 0
     qtyg60[x]    = 0
     costg60[x]   = 0
     qtyns12[x]   = 0
     costns12[x]  = 0
     qtyns24[x]   = 0
     costns24[x]  = 0
     qtyusg[x]    = 0.
                    
                    
if last-of (whsqty.vendor) then
  do:
  assign x = 2.
    display stream regfile 
    (if p-detail = "s" then itemz.zvendor else
     "*Vendor")                @ aprod
     qtyoh[x]                  @ aqtyoh
     qtyls12[x]                @ aqtyls12
     qty6[x]                   @ aqty6
     qty12[x]                  @ aqty12
     qty24[x]                  @ aqty24
     qty36[x]                  @ aqty36
     (qty48[x] + qty60[x])     @ aqty60
     qtyg60[x]                 @ aqtyg60
     qtyns12[x]                @ aqtyns12
     qtyns24[x]                @ aqtyns24
     qtyusg[x]                 @ a12iss
     whsqty.whse               
   with frame f-det3.
  display stream regfile            
     (if p-detail = "s" then " "
       else itemz.zvendor)     @ adesc
     costoh[x]                 @ acostoh
     costls12[x]               @ acostls12
     cost6[x]                  @ acost6
     cost12[x]                 @ acost12
     cost24[x]                 @ acost24
     cost36[x]                 @ acost36
     (cost48[x] + cost60[x])   @ acost60
     costg60[x]                @ acostg60
     costns12[x]               @ acostns12
     costns24[x]               @ acostns24
  with frame f-det2.

  display stream regfile with frame f-space.
   
  export stream bfile delimiter "~011"
      "*    " 
      "Vendor"
      itemz.zvendor
      " "
      " "
      qtyoh[x]
      costoh[x]
      qtyls12[x]
      costls12[x]
      qty6[x]
      cost6[x]
      qty12[x]
      cost12[x]
      qty24[x]
      cost24[x]
      qty36[x] 
      cost36[x]
/*      
      qty48[x]
      cost48[x]
*/
      (qty48[x] + qty60[x])
      (cost48[x] + cost60[x])
      qtyg60[x]
      costg60[x]
      qtyns12[x]
      costns12[x]
      qtyns24[x]
      costns24[x]
      qtyusg[x]
      " "
      " ".
   
   assign    
      qtyoh[x]     = 0
      costoh[x]    = 0 
      qtyls12[x]   = 0
      costls12[x]  = 0
      qty6[x]      = 0
      cost6[x]     = 0 
      qty12[x]     = 0
      cost12[x]    = 0
      qty24[x]     = 0
      cost24[x]    = 0
      qty36[x]     = 0
      cost36[x]    = 0
      qty48[x]     = 0
      cost48[x]    = 0
      qty60[x]     = 0
      cost60[x]    = 0
      qtyg60[x]    = 0
      costg60[x]   = 0
      qtyns12[x]   = 0
      costns12[x]  = 0
      qtyns24[x]   = 0
      costns24[x]  = 0
      qtyusg[x]    = 0.
  end.      
    

   
if last-of (whsqty.whse) then
  do:
  assign x = 3.
    display stream regfile 
    (if p-detail = "s" then whsqty.whse else
     "**Whse")                 @ aprod
     qtyoh[x]                  @ aqtyoh
     qtyls12[x]                @ aqtyls12
     qty6[x]                   @ aqty6
     qty12[x]                  @ aqty12
     qty24[x]                  @ aqty24
     qty36[x]                  @ aqty36
     (qty48[x] + qty60[x])     @ aqty60
     qtyg60[x]                 @ aqtyg60
     qtyns12[x]                @ aqtyns12
     qtyns24[x]                @ aqtyns24
     qtyusg[x]                 @ a12iss
     whsqty.whse
   with frame f-det3.
  display stream regfile            
     (if p-detail = "s" then " "
       else whsqty.whse)     @ adesc
     costoh[x]                 @ acostoh
     costls12[x]               @ acostls12
     cost6[x]                  @ acost6
     cost12[x]                 @ acost12
     cost24[x]                 @ acost24
     cost36[x]                 @ acost36
     (cost48[x] + cost60[x])   @ acost60
     costg60[x]                @ acostg60
     costns12[x]               @ acostns12
     costns24[x]               @ acostns24
  with frame f-det2.

  display stream regfile with frame f-space.
   
  export stream bfile delimiter "~011"
      "*    " 
      "Whse"
      whsqty.whse
      " "
      " "
      qtyoh[x]
      costoh[x]
      qtyls12[x]
      costls12[x]
      qty6[x]
      cost6[x]
      qty12[x]
      cost12[x]
      qty24[x]
      cost24[x]
      qty36[x] 
      cost36[x]
/*      
      qty48[x]
      cost48[x]
*/
      (qty48[x] + qty60[x])
      (cost48[x] + cost60[x])
      qtyg60[x]
      costg60[x]
      qtyns12[x]
      costns12[x]
      qtyns24[x]
      costns24[x]
      qtyusg[x]
      " "
      " ".
   
   assign    
      qtyoh[x]     = 0
      costoh[x]    = 0 
      qtyls12[x]   = 0
      costls12[x]  = 0
      qty6[x]      = 0
      cost6[x]     = 0 
      qty12[x]     = 0
      cost12[x]    = 0
      qty24[x]     = 0
      cost24[x]    = 0
      qty36[x]     = 0
      cost36[x]    = 0
      qty48[x]     = 0
      cost48[x]    = 0
      qty60[x]     = 0
      cost60[x]    = 0
      qtyg60[x]    = 0
      costg60[x]   = 0
      qtyns12[x]   = 0
      costns12[x]  = 0
      qtyns24[x]   = 0
      costns24[x]  = 0
      qtyusg[x]    = 0.
  end.      

    
   
   
end.

assign x = 5.
  display stream regfile 
     "**** Final Total"        @ aprod
     qtyoh[x]                  @ aqtyoh
     qtyls12[x]                @ aqtyls12
     qty6[x]                   @ aqty6
     qty12[x]                  @ aqty12
     qty24[x]                  @ aqty24
     qty36[x]                  @ aqty36
     (qty48[x] + qty60[x])     @ aqty60
     qtyg60[x]                 @ aqtyg60
     qtyns12[x]                @ aqtyns12
     qtyns24[x]                @ aqtyns24
     qtyusg[x]                 @ a12iss
   with frame f-det1.
 
  display stream regfile 
     " "                       @ adesc
     costoh[x]                 @ acostoh
     costls12[x]               @ acostls12
     cost6[x]                  @ acost6
     cost12[x]                 @ acost12
     cost24[x]                 @ acost24
     cost36[x]                 @ acost36
     (cost48[x] + cost60[x])   @ acost60
     costg60[x]                @ acostg60
     costns12[x]               @ acostns12
     costns24[x]               @ acostns24
  with frame f-det2.

display stream regfile with frame f-space.

export stream bfile delimiter "~011"
    "**** " 
    "Final Total"
    " "
    " "
    " "
    qtyoh[x]
    costoh[x]
    qtyls12[x]
    costls12[x]
    qty6[x]
    cost6[x]
    qty12[x]
    cost12[x]
    qty24[x]
    cost24[x]
    qty36[x] 
    cost36[x] 
/*
    qty48[x]
    cost48[x]
*/
    (qty48[x] + qty60[x])
    (cost48[x] + cost60[x])
    qtyg60[x]
    costg60[x]
    qtyns12[x]
    costns12[x]
    qtyns24[x]
    costns24[x]
    qtyusg[x]
    " "
    " ".
 
assign    
    qtyoh[x]     = 0
    costoh[x]    = 0 
    qtyls12[x]   = 0
    costls12[x]  = 0
    qty6[x]      = 0
    cost6[x]     = 0
    qty12[x]     = 0
    cost12[x]    = 0
    qty24[x]     = 0
    cost24[x]    = 0
    qty36[x]     = 0
    cost36[x]    = 0
    qty48[x]     = 0
    cost48[x]    = 0
    qty60[x]     = 0
    cost60[x]    = 0
    qtyg60[x]    = 0
    costg60[x]   = 0
    qtyns12[x]   = 0
    costns12[x]  = 0
    qtyns24[x]   = 0
    costns24[x]  = 0
    qtyusg[x]    = 0.
 
end.






