  

/* poezblsl.lpr 1.1 01/03/98 */
/* poezblsl.lpr 1.1 10/25/95 */
/*h*****************************************************************************
  INCLUDE      : poezblsl.lpr
  DESCRIPTION  : Purchase Order Batch Entry - Line Items - Serial/Lot Processing
  USED ONCE?   : no
  AUTHOR       : 
  DATE WRITTEN : 04/23/95
  CHANGES MADE :
    10/25/95 mtt; TB# 19528 Develop PO Batch Receiving Function
    01/08/01 lbr; TB# e5104 Added var passed to icets.p.
*******************************************************************************/

assign
     s-lineno     = b-poelb.lineno
     s-qtyunavail = b-poelb.qtyunavail
     s-reasunavty = b-poelb.reasunavty.

if icsw.serlottype = "s" then do:
 /* serialno auto create for service repair - 04/09/08 */
 {w-icsp.i b-poelb.shipprod no-lock}
 if avail icsp and icsp.prodcat begins "s" then do:    
  def new shared var v-poelbid as recid            no-undo.
  def new shared var v-poeiid  as recid            no-undo.
  assign v-poelbid = recid(b-poelb).
  if avail poel and poel.transtype = "do" then     
   run zsdiserialno_updt.p(input "poezb",           
                            input "do").            
  else                                             
   run zsdiserialno_updt.p(input "poezb",           
                           input " ").             
 end.

 {p-nosn.i &ordertype = ""p""
           &orderno   = g-pono
           &ordersuf  = g-posuf
           &com       = "/*"}
end.
else if icsw.serlottype = "l" then do:
 {p-nolots.i &seqno     = 0
             &ordertype = ""p""
             &orderno   = g-pono
             &ordersuf  = g-posuf
             &com       = "/*"}
end.

assign
     v-eachprice = poel.price /  {unitconv.gas &decconv = poel.unitconv}
     v-allprice  = (poel.price / {unitconv.gas &decconv = poel.unitconv}) *
                   s-stkqtyrcv
     v-nosnlots  = round(s-stkqtyrcv - v-noassn,2).

if avail icsp and icsp.prodcat begins "s" and icsw.serlottype = "s" then    
 assign i = 1. 
else  
if icsw.serlottype = "s" then
    /*tb e5104 Added "" param before output param */
    run icets.p(g-whse,
                b-poelb.shipprod,
                "po",
                g-pono,
                g-posuf,
                b-poelb.lineno,
                0,
                v-returnfl,
                s-stkqtyrcv,
                v-nosnlots,
                poel.stkqtyord,
                v-eachprice,
                g-potype,
                "",
                output s-stkqtyrcv,
                output v-errfl,
                input-output s-qtyunavail).
else if icsw.serlottype = "l" then
    run icetl.p(g-whse,
                b-poelb.shipprod,
                "po",
                g-pono,
                g-posuf,
                b-poelb.lineno,
                0,
                v-returnfl,
                s-stkqtyrcv,
                v-nosnlots,
                poel.stkqtyord,
                v-eachprice,
                g-potype,
                output s-stkqtyrcv,
                output v-errfl,
                input-output s-qtyunavail).

if toggle-sw = no then 
  put screen row 21 column 1 color messages
"  F6-Options     F7-Load Type    F8-VendProd Toggle     F9-Search   F10-Update  ".
else 
  put screen row 21 column 1 color messages
"  F6-Options     F7-Load Type    F8-VndProd Toggle(on)  F9-Search   F10-Update  ".

if v-errfl = "s" then do:
    assign
        s-qtyrcv       = round(s-stkqtyrcv /
                         {unitconv.gas &decconv = b-poelb.unitconv},2).
    display s-qtyrcv @ tpoehb.qtyrcv with frame f-poehb.

    {&comupdt}
    assign
         b-poelb.qtyrcv    = s-qtyrcv
         b-poelb.stkqtyrcv = s-stkqtyrcv.
    /{&compupdt}* */
end.

assign
      b-poelb.qtyunavail = s-qtyunavail.


