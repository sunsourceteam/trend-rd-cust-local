/* n-oeizl5.i 1.1 01/03/98 */
/* n-oeizl5.i 1.5 01/29/94 */
/*h*****************************************************************************
  SX 55
  INCLUDE      : n-oeizl5.i
  DESCRIPTION  : Find next/prev for OEIZL where v-mode = 5
  USED ONCE?   : yes
  AUTHOR       : rhl
  DATE WRITTEN :
  CHANGES MADE :
    12/23/91 pap; TB#  5266 JIT/Line Due - reqship to promise dt
    01/29/94 kmw; TB# 11747 Make stage a range
    03/14/96 tdd; TB# 20713 Inquiry not working properly
    08/08/02 lcb; TB# e14281 User Hooks
*******************************************************************************/

/*e*****************************************************************************
     Selection Criteria for this procedure:
       v-mode ==> 5:
               Product: <blank>
             Customer#: entered on the screen
               CustPO#: entered on the screen
*******************************************************************************/
do:
confirm = yes.

if ("{1}" = "FIRST" and s-directionfl )  then
  assign v-xmode = "C".
else
if ("{1}" = "LAST" and not s-directionfl )  then
  assign v-xmode = "O".



if v-xmode = "C" then
  do:


find {1} oeehb use-index k-custpo where
         oeehb.cono   = g-cono and
         oeehb.custpo begins s-custpo and
         oeehb.custno = s-custno and
         (s-shipto     = ""   or oeehb.shipto    = s-shipto)  and
         (s-takenby    = ""   or oeehb.takenby   = s-takenby) and
         (s-transtype  = ""   or s-transtype    = "QU" /* oeehb.transtype */) 
         and
         (s-transtype  <> "bo" ) and

          
         ((oeehb.stagecd <> 1 and oeehb.stagecd >= integer(s-stagecdl)) or
           (oeehb.stagecd = 1 and integer(s-stagecdl) = 0)) and
          (oeehb.stagecd <= integer(s-stagecdh)) and

         (s-promisedt  = ?    or oeehb.promisedt >= s-promisedt) and

         (s-whse = "" or oeehb.whse = s-whse) and
         oeehb.sourcepros = "ComQu" and
         oeehb.seqno = 2



         {oeizl.z99 &user_find_oeizl = "*"
                    &file            = "oeehb"}
no-lock no-error.

/*tb 6507 05/07/92 dea; added check for line DO orders */
if avail oeehb and s-doonlyfl = yes then do:
    confirm = no.
    find first oeelb use-index k-oeelb where
               oeelb.cono     = g-cono and
               oeelb.batchnm  = oeehb.batchnm and
               oeelb.seqno    = oeehb.seqno   and
               oeelb.botype   = "d"
    no-lock no-error.
    if avail oeelb then confirm = yes.
end.




 
if avail oeehb then
  find zxinq where zxinq.present = true  no-lock no-error.
else
  find zxinq where zxinq.present = false no-lock no-error.


end.   


if v-xmode = "C" and (not avail oeehb) and
   ("{1}" = "first" or "{1}" = "next" ) then
  do:
  assign v-xmode = "O"
         confirm = yes.
  find first oeeh use-index k-custpo where
             oeeh.cono   = g-cono and
             oeeh.custpo = "" and
             oeeh.custno = 0 no-lock no-error.

  end.

if v-xmode = "O" then
do:


find {1} oeeh use-index k-custpo where
         oeeh.cono   = g-cono and
         oeeh.custpo begins s-custpo and
         oeeh.custno = s-custno and
         (s-shipto     = ""   or oeeh.shipto    = s-shipto)  and
         (s-takenby    = ""   or oeeh.takenby   = s-takenby) and
         (s-transtype  = ""   or s-transtype    = oeeh.transtype or
         (s-transtype  = "bo" and oeeh.borelfl)) and

         (oeeh.stagecd >= integer(s-stagecdl)) and
         (oeeh.stagecd <= integer(s-stagecdh)) and

         (s-promisedt  = ?    or oeeh.promisedt >= s-promisedt) and

         (s-whse = "" or oeeh.whse = s-whse)

         {oeizl.z99 &user_find_oeizl = "*"
                    &file            = "oeeh"}
no-lock no-error.

/*tb 6507 05/07/92 dea; added check for line DO orders */
if avail oeeh and s-doonlyfl = yes then do:
    confirm = no.
    find first oeel use-index k-oeel where
               oeel.cono     = g-cono and
               oeel.orderno  = oeeh.orderno and
               oeel.ordersuf = oeeh.ordersuf and
               oeel.botype   = "d"
    no-lock no-error.
    if avail oeel then confirm = yes.
end.



if avail oeeh then
  find zxinq where zxinq.present = true  no-lock no-error.
else
  find zxinq where zxinq.present = false no-lock no-error.
end.

if v-xmode = "o" and (not avail oeeh) and
   ("{1}" = "Prev" or "{1}" = "Last" ) then
  do:
  assign v-xmode = "C"
         confirm = yes.

find last oeehb use-index k-custpo  where
         oeehb.cono   = g-cono and
         oeehb.custpo begins s-custpo and
         oeehb.custno = s-custno and
         (s-shipto     = ""   or oeehb.shipto    = s-shipto)  and
         (s-takenby    = ""   or oeehb.takenby   = s-takenby) and
         (s-transtype  = ""   or s-transtype    = "QU" /* oeehb.transtype */) 
         and
         (s-transtype  <> "bo" ) and
          
         ((oeehb.stagecd <> 1 and oeehb.stagecd >= integer(s-stagecdl)) or
           (oeehb.stagecd = 1 and integer(s-stagecdl) = 0)) and
          (oeehb.stagecd <= integer(s-stagecdh)) and

         (s-promisedt  = ?    or oeehb.promisedt >= s-promisedt) and

         (s-whse = "" or oeehb.whse = s-whse) and
         oeehb.sourcepros = "ComQu" and
         oeehb.seqno = 2



         {oeizl.z99 &user_find_oeizl = "*"
                    &file            = "oeehb"}
no-lock no-error.

/*tb 6507 05/07/92 dea; added check for line DO orders */
if avail oeehb and s-doonlyfl = yes then do:
    confirm = no.
    find first oeelb use-index k-oeelb where
               oeelb.cono     = g-cono and
               oeelb.batchnm  = oeehb.batchnm and
               oeelb.seqno    = oeehb.seqno   and
               oeelb.botype   = "d"
    no-lock no-error.
    if avail oeelb then confirm = yes.
end.




 
if avail oeehb then
  find zxinq where zxinq.present = true  no-lock no-error.
else
  find zxinq where zxinq.present = false no-lock no-error.


end.   


end.
