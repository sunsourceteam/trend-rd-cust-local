
/************ main block ***********/

/* run chk_4_update.  */

/******* procedure block **********/

procedure load_temp_tbl:

  repeat: 
    inx = inx + 1.
    import stream custin delimiter "," c_cust c_shipto c_slsmn.
     find cst where cst_cono = 1 and cst_shipto = c_shipto and 
                   cst_cust = dec(c_cust) no-lock no-error.

     /* This is just ignoring dups if they are really the same */
     if avail cst then /* and cst_slsrepin = c_slsmn then */
        do:
        next.
        end.
     
    create cst.
    assign cst_cono = 1.
    assign cst_cust = dec(c_cust).
    assign cst_shipto = c_shipto.
    assign cst_slsrepin  = c_slsmn.
/*    display cst_cust c_slsmn.       */
  end.

end.

procedure chk_4_entry:
customer_lookup:
  do:
  find first cst where cst_cono = 1  and 
                       cst_cust = dec(h_cust) and 
                       cst_shipto = h_shipto no-lock no-error.
    if avail cst then 
       if cst_slsrepin ne h_slsrepin then                   
          assign h_slsrepin = cst_slsrepin.             
       else
         assign h_slsrepin = h_slsrepin.
  /*
    else
      do:
      find first cst where cst_cono = 1  and 
                           cst_cust = dec(h_cust)  
                           no-lock no-error.
      if avail cst then 
        if cst_slsrepin ne h_slsrepin then                   
          assign h_slsrepin = cst_slsrepin.
      end.                  
  */
  end.
end.
    