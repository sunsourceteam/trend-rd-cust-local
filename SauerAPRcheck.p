/* Check APR information for Sauer Pcats */

/* dt01 01/16/2009 added Cuno */

define input        parameter ip-prodcat like icsp.prodcat no-undo.
define input        parameter ip-state   as character format "x(2)" no-undo.
define input        parameter ip-zipcd   as character format "x(10)" no-undo.
define input-output parameter ip-error   as logical no-undo.
define input-output parameter ip-msg     as char    no-undo.

{g-all.i} 

assign ip-error = false
       ip-msg   = "".

define var v-SauerPcat  as logical no-undo. 
define buffer b-State   for zsastz.
define buffer b-FProdcat for zsastz.
define buffer b-WProdcat for zsastz.
def var v-stateinx  as integer no-undo.
def var v-countrySuf as character format "x(15)" no-undo.
def var v-countrycd as character no-undo init "".
def var v-CAcityname   as c   format "x(26)"           no-undo. 
def var v-CAstatecode  as c   format "x(2)"            no-undo. 

def var v-statename as char extent 62 initial
    ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL",
     "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME",
     "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH",
     "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI",
     "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI",
     "WY", "CN", "IT", "PR", "VI", "MP", "GU", "AS", "PW", "AA",
     "AE", "AP"].
          
          
if ip-state <> "" then
state:
do v-stateinx = 1 to 51:
  if v-statename[v-stateinx] = ip-state then do:
    v-countrycd = "US".
    leave state.
  end.
end. /* US state loop */

    
if v-countrycd = "" then do:
    
    /*d If the state code was not a US state then check if the zip code passed
        in is a valid US zip code */
    if substring(ip-zipcd,1,1) >= '0':U      and
       substring(ip-zipcd,1,1) <= '9':U      and
       substring(ip-zipcd,1,5) <> "00000":U then
        v-countrycd = "US".
    else do:
      run ticachk.p (input  caps(ip-state),
                     input  substring(ip-zipcd, 1, 5),
                     output v-CAcityname,
                     output v-CAstatecode).
      if v-CAcityname  <> "" or 
         v-CAstatecode <> ""  then
         v-countrycd = "CA".
      else
         v-countrycd = "IT".
    end. /* Canadian address check */
end. /* if v-countrycd = "" */
 
assign v-countrySuf = if v-countrycd = "us" then
                        ""
                      else
                      if v-countrycd = "ca" then
                        "Canada"
                      else
                        "IT".

find b-FProdcat where
     b-FProdcat.cono       = g-cono and
     b-FProdcat.labelfl    = false and
     b-FProdcat.codeiden   = "SauerFullLine" and
     b-FProdcat.primarykey = ip-prodcat no-lock no-error.

if avail b-FProdcat then do:   /* This is a sauer product */
  find b-State where
       b-State.cono       = g-cono and
       b-State.labelfl    = false and
       b-State.codeiden   = "SauerFullLineSt" + v-countrySuf and
       b-State.primarykey = ip-state no-lock no-error.

  if avail b-state then do:
    assign ip-error = false.
    return.
  end.

  find b-State where
       b-State.cono       = g-cono and
       b-State.labelfl    = false and
       b-State.codeiden   = "SauerWorkLineSt" + v-countrySuf  and
       b-State.primarykey = ip-state no-lock no-error.
  if not avail b-state then do:
    assign ip-error = true
           ip-msg   = "(APR:1) Not Authorized for Sauer Products in state " +
              caps(ip-state).
    return.
  end.  


  
  find b-WProdcat where
       b-WProdcat.cono       = g-cono and
       b-WProdcat.labelfl    = false and
       b-WProdcat.codeiden   = "SauerWorkLine" and
       b-WProdcat.primarykey = ip-prodcat no-lock no-error.
  if not avail b-WProdcat then do:     
    assign ip-error = true
           ip-msg   = "(APR:2) Not Authorized for Sauer Full Line in state " +
              caps(ip-state).
    return.          
  end.
  else do:
    find b-State where
         b-State.cono       = g-cono and
         b-State.labelfl    = false and
         b-State.codeiden   = "SauerWorkLineSt" + v-countrySuf and
         b-State.primarykey = ip-state no-lock no-error.

    
    if avail b-state then do:
      assign ip-error = false.
      return.
    end.
    else
      assign ip-error = true
             ip-msg   = "(APR:3) Not Authorized for Sauer Work Line in state" +
                caps(ip-state).
  end.
end. 

/* add Cuno dt01 01/16/2009 */

find b-FProdcat where
     b-FProdcat.cono       = g-cono and
     b-FProdcat.labelfl    = false and
     b-FProdcat.codeiden   = "CunoFullLine" and
     b-FProdcat.primarykey = ip-prodcat no-lock no-error.

if avail b-FProdcat then do:   /* This is a sauer product */
  find b-State where
       b-State.cono       = g-cono and
       b-State.labelfl    = false and
       b-State.codeiden   = "CunoFullLineSt" + v-countrySuf and
       b-State.primarykey = ip-state no-lock no-error.

  if avail b-state then do:
    assign ip-error = false.
    return.
  end.
  else do:
    assign ip-error = true
           ip-msg   = /* "(APR:1) Not Authorized for Cuno Products in state " +
              caps(ip-state). */
   "We are auth. for these products nationwide, contact Jan Emert to release". 
    
    
    return.
  end.  
end.

/* add Mitsubishi tah 05/23/2012 */

find b-FProdcat where
     b-FProdcat.cono       = g-cono and
     b-FProdcat.labelfl    = false and
     b-FProdcat.codeiden   = "MitsuFullLine" and
     b-FProdcat.primarykey = ip-prodcat no-lock no-error.

if avail b-FProdcat then do:   /* This is a sauer product */
  find b-State where
       b-State.cono       = g-cono and
       b-State.labelfl    = false and
       b-State.codeiden   = "MitsuFullLineSt" + v-countrySuf and
       b-State.primarykey = ip-state no-lock no-error.

  if not avail b-state then do:
    assign ip-error = false.
    return.
  end.
  else do:
    assign ip-error = true
           ip-msg   = "(APR:1) Not Authorized for Mitsubishi " +
                      "Products in state " +
              caps(ip-state).
    return.
  end.  
end.


/* Add ReThink 9/25/17 */


find b-FProdcat where
     b-FProdcat.cono       = g-cono and
     b-FProdcat.labelfl    = false and
     b-FProdcat.codeiden   = "ReThinkFullLine" and
     b-FProdcat.primarykey = ip-prodcat no-lock no-error.

if avail b-FProdcat then do:   /* This is a ReThink category */
  find b-State where
       b-State.cono       = g-cono and
       b-State.labelfl    = false and
       b-State.codeiden   = "ReThinkFullLineSt" and
       b-State.primarykey = ip-state no-lock no-error.
    
  if avail b-state then do:
    assign ip-error = false.
    return.
  end.

  else do:  
  if not avail b-state then do:
    assign ip-error = true
           ip-msg   = "(APR:1) Not Authorized for ReThink Robotics Products in                        state " +
              caps(ip-state).
    return.
  end.  
  end. 
end.

/* Add Festo 11/28/17 */


find b-FProdcat where
     b-FProdcat.cono       = g-cono and
     b-FProdcat.labelfl    = false and
     b-FProdcat.codeiden   = "FestoFullLine" and
     b-FProdcat.primarykey = ip-prodcat no-lock no-error.

if avail b-FProdcat then do:   /* This is a Festo category */
  find b-State where
       b-State.cono       = g-cono and
       b-State.labelfl    = false and
       b-State.codeiden   = "FestoFullLineSt" and
       b-State.primarykey = ip-state no-lock no-error.
    
  if avail b-state then do:
    assign ip-error = false.
    return.
  end.
  else do:  
  if not avail b-state then do:
    assign ip-error = true  
           ip-msg   = "(APR:1) Not Authorized for Festo Products in state " +
              caps(ip-state).
    return.
  end.  
  end. 
end.


/* Add Kawasaki 04-14-14 */

find b-FProdcat where
     b-FProdcat.cono       = g-cono and
     b-FProdcat.labelfl    = false and
     b-FProdcat.codeiden   = "KawFullLine" and
     b-FProdcat.primarykey = ip-prodcat no-lock no-error.

if avail b-FProdcat then do:   /* This is a Kawasaki product */

  find b-State where
       b-State.cono       = g-cono and
       b-State.labelfl    = false and
       b-State.codeiden   = "KawFullLineSt" + v-countrySuf and
       b-State.primarykey = ip-state no-lock no-error.

  if not avail b-state then do:   
    find first b-State where
         b-State.cono       = g-cono and
         b-State.labelfl    = false and
         b-State.codeiden   = "KawFullLineStZip" + v-countrySuf and
         b-State.primarykey = ip-state and
         ip-zipcd matches b-State.secondarykey no-lock no-error.
  end.

  if avail b-state then do:
    assign ip-error = false.
    return.
  end.
  else do:  
    assign ip-error = true
           ip-msg   = "(APR:1) Not Authorized for Kawasaki " +
                      "Products in state/zip " +
              caps(ip-state) + " / " + ip-zipcd.

    return.
  end.  
end.



