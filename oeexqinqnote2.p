/* 
 PROGRAM NAME: oeexqinqnote2.p
 PURPOSE:      Update the OEEXQ internal and external notes when maintenance
               is done in OEIZO.
               oeexqinqnote2.p is called from zsdinoted.p
 AUTHOR:       das - 10/17/2007
*/

{g-all.i}
def input param v-notestype     like notes.notestype    no-undo.
def input param v-primarykey    like notes.primarykey   no-undo.
def input param v-secondarykey  like notes.secondarykey no-undo.

def buffer b-notes for notes.

for each notes where notes.cono         = g-cono and
                     notes.notestype    = v-notestype  and
                     notes.primarykey   = v-primarykey and
                     notes.secondarykey = v-secondarykey
                     no-lock:
  find b-notes where b-notes.cono         = notes.cono and
                     b-notes.notestype    = "ba"       and
                     b-notes.primarykey   = notes.primarykey and
                     b-notes.secondarykey = if notes.printfl2 = yes then "E"
                                            else "I"
                     no-error.
  
  if avail b-notes then
    do:
    do i = 1 to 16:
      assign b-notes.noteln[i] = notes.noteln[i].
    end.
  end.
  else
    do:
    create b-notes.
    buffer-copy notes except notes.notestype
                             notes.secondarykey
          to b-notes
          assign b-notes.notestype = "ba"
                 b-notes.secondarykey = if notes.printfl2 = yes then "E"
                                        else "I".
  end.
end.
