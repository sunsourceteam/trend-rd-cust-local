/* g-icepe.i 1.2 04/02/98 */
/*h****************************************************************************
  INCLUDE      : g-icepe.i
  DESCRIPTION  : All globals for ICEPE
  USED ONCE?   :
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    04/01/98 cm;  TB# 24226 Increase quantity counted field size
    06/06/00 gfk; TB# 15060 Setup scroll length parameter for performance
    03/02/04 kjb; TB# t8355 Include unavailable stock on the physical count -
        added s-filter and v-unavailfl
******************************************************************************/

{g-all.i}
{g-inq.i}
{g-ic.i}
def buffer b-icset for icset.

/* Frame 1 */
def {1} shared var s-runno     like icsep.runno            no-undo.

/* Frame 2 */
def {1} shared var s-prod      like icsep.prod             no-undo.
def {1} shared var s-qtycnt    like icsep.qtycnt           no-undo.
def {1} shared var s-ticketno  like icset.ticketno         no-undo.
def {1} shared var s-binloc    like icsep.binloc           no-undo.
def {1} shared var s-qtyonhand like icsw.qtyonhand         no-undo
    format "zzzzzzzz9.99-".
def {1} shared var s-unit      like icsep.unit             no-undo.
def {1} shared var s-type      like icset.rectype          no-undo.
def {1} shared var s-filter    as c format "x"             no-undo.
def {1} shared var v-mode      as c format "x(16)"         no-undo.
def {1} shared var v-msg       as c format "x(78)"         no-undo.
def {1} shared var s-uticketno like icset.uticketno        no-undo.
/* Other */
def {1} shared var v-serfl     as l                        no-undo.
def {1} shared var v-showfl    as l                        no-undo.
def {1} shared var v-autofl    as l                        no-undo.
def {1} shared var v-tickfl    as l                        no-undo.
def {1} shared var v-modecd    as c                        no-undo.
def {1} shared var v-tprint    as l                        no-undo.
def {1} shared var v-null      as c                        no-undo.
def {1} shared var v-ticketno  as i                        no-undo.
def {1} shared var v-seqno     as i                        no-undo.
def {1} shared var v-first     as l                        no-undo.
def {1} shared var v-binloc    like icsep.binloc           no-undo.
def {1} shared var v-phyfl     as l                        no-undo.
def {1} shared var v-wsize     as i initial 99999999   no-undo.
def {1} shared var o-modecd    as c                        no-undo.
def {1} shared var v-cnt       as i                        no-undo.
def {1} shared var v-uticketno like icset.uticketno        no-undo.
def {1} shared var v-fkey      as c format "x(5)" extent 5 initial
["","","","",""] no-undo.
def {1} shared var v-selectfl  as l                         no-undo.
def {1} shared var v-unavailfl as l                         no-undo.

/*TB15060 06/06/00 gfk; Add v-flength, v-frow */
def            var v-row       as i                         no-undo.
def {1} shared var v-flength   as i                         no-undo.
def {1} shared var v-frow      as i                         no-undo.
def            var v-zerofl    as l                         no-undo.

def {1} shared frame f-icepe1.
def {1} shared frame f-icepe2.

/********************************* f-icepe1.i *******************************/
/*TB15060 06/06/00 gfk; Add f-top frame for title and frame box */

form
    skip(19)
with frame f-top width 80 no-labels overlay  no-hide title g-title.

/*TB15060 06/06/00 gfk; Add v-flength to parameters, no-box, col 2 */
/*tb t8355 01/14/03 kjb; Add s-filter to the frame to allow the user to display
    or update onhand, unavailable or both */
form
    g-whse      colon 6     label "Whse"    {f-help.i}
        validate({v-icsd.i g-whse "/*"},
        "Warehouse Not Set Up in Warehouse Setup - ICSD (4601)")
    s-runno     colon 18    label "Run #"
        validate(can-find(first icsep where
        icsep.cono  = g-cono         and
        icsep.whse  = input g-whse   and
        icsep.runno = s-runno),
        "No Records Exist For This Warehouse and Run Number (4741)")
    s-filter     at 26       label "Display"
        help "Display (O)n Hand Only, (U)navailable Only or (B)oth"
        validate(can-do("o,u,b":u, input s-filter),
                 "Must be O, U or B")
    v-flength    at 39       label "Length"
        help "Enter number of entry lines to display in scrolling area "
        validate(input v-flength > 1 and
                 input v-flength <= 16,
        "Length of entry area must be > 1 and < 17")
    v-mode      at 63       no-label
    skip(1)
    v-msg       at 1        no-label
    skip
with frame f-icepe1 width 78 side-labels overlay row 2 col 2 no-box no-hide.

/******************** f-icepe2 *********************************/
/*TB15060 06/06/00 gfk; Add v-flength down for scrolling */
/*tb 24226 04/01/98 cm; Increase size of s-qtycnt */
form
    s-ticketno                            at 1
        validate(if {k-func.i} then true else s-ticketno ne 0,
        "This is a Required Field (2100)")
    s-prod                                at 8
    s-binloc                              at 33
    s-type          format "(x)"          at 46
    s-qtycnt        format "zzzzzzzz9.99" at 49
    s-qtyonhand                           at 62
    s-unit                                at 75
with frame f-icepe2 width 78 centered no-labels overlay row 5 v-flength
down no-box.

/******************** f-zero ***********************/
form
    v-zerofl    colon 21 label "Confirm Zero Counted"
with frame f-zero side-labels overlay row v-row col 40 title " Zero Counted ".

/******************** f-tick ************************/
form
    "Ticket #"      at 6
    s-uticketno     at 8
        validate(if v-uticketno ne 0 and v-uticketno = s-uticketno then true
        else not can-find(first icset where
        icset.cono  = g-cono    and
        icset.whse  = g-whse    and
        icset.runno = s-runno   and
        icset.uticketno = s-uticketno),
        "Duplicate And Zero Ticket Numbers Not Allowed (6105)")
with frame f-tick no-labels overlay row v-row + 1 col 60 width 20 no-box.
