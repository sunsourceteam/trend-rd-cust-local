/*  SX 55
    e-poeral.i 1.1 8/17/92  */
/*h*****************************************************************************
  INCLUDE              : e-poeral.i
  DESCRIPTION          : PO RRAR line item screen editing include (changing
                         a line item/adding a new line item (x-xxex.i)
  USED ONLY ONCE?      : yes
  AUTHOR               : mtt
  DATE WRITTEN         : 07/30/92
  CHANGES MADE AND DATE: 07/30/92 mtt; made major changes to the way the
                                  PO RRAR acceptance detail screen will
                                  work using the new X-XXEX.I include.  The
                                  following TBs are involved:
                                   tb#7335 (Apex) - Ability to add new line
                                     items to the Po RRAR (all comments will
                                     use this tb#).
                                   tb#5875 (Hisco) - Down arrow does not adv
                                     to next page (up arrow also).
                                   tb#4411 (Texas Screen) - Want 'next line'
                                     function key.
*******************************************************************************/

run poeralc.p.
        /*
        if g-operinit = "das" then do:
           message "1" v-msg "2" v-status "3" v-scrollfl "4" v-nextfl 
             "5" v-recid[2] "6" v-curpos "7" v-holdline "8" v-length
             "9" v-newdisplay "10" v-nextpage "11" v-prevpage
             "12" confirm "after edit".
           pause.
        end.
        */

if avail b-poeral then do:
   find spa-poeral where recid(spa-poeral) = recid(b-poeral).
   if avail spa-poeral then do:
      if substring(spa-poeral.user5,78,1) <> "" then do:
         assign s-scrollfield = substring(spa-poeral.user5,78,1).
         display s-scrollfield 
                 with frame f-poeral.
      end.
   end.
 end.
  
