/******************************************************************************
 ap_defvars_lookup.i based on
 ar_defvars_lookup.i
 Include for vendor lookups. to fill in the bottom of the screen with 
  additional vendor information.
  
*****************************************************************************/
/* SDI custom loookup variables */
def buffer z-apsv for apsv.
def buffer z-apss for apss.
def {1} shared var zu-shipfrom   as char format "x(04)"            no-undo.
def {1} shared var zu-vendno   like apsv.vendno                  no-undo.
def {1} shared var lu-vendno        like apsv.vendno  initial 0  no-undo.
def {1} shared var lu-name          as c format "x(30)" dcolor 3 no-undo.
def {1} shared var lu-addr1         as c format "x(30)"          no-undo.
def {1} shared var lu-restaddr      as c format "x(60)"          no-undo.
def {1} shared var lu-city          as c format "x(20)"          no-undo.
def {1} shared var lu-state         like apsv.state              no-undo.
def {1} shared var lu-invytd      like apsv.invytd dcolor 3  no-undo.
def {1} shared var lu-invlstyr    like apsv.invly dcolor 3  no-undo.

def {1} shared var lu-currbal like apsv.currbal dcolor 3 no-undo.
def {1} shared var lu-type    like apsv.vendtype dcolor 3 no-undo.
def {1} shared var lu-terms   like apsv.termstype dcolor 3 no-undo.
def {1} shared var lu-shiptos     as   char format "x(14)"      no-undo.  

def {1} shared frame f-zvendinfo
    /* "  Name:" */ lu-name at 1
    "InvYtd/Lstyr:" at 38
    lu-invytd   at 52
    lu-invlstyr at 66 
    /* "       " at 1 lu-addr1  */
    lu-addr1     at 1
    
    "Current Balance:" at 35
    lu-currbal   at 52
    
    lu-restaddr  at 1   
    lu-shiptos   at 66                       
    
    "Type:" at 35
    lu-type at 42
    "Terms:" at 48
    lu-terms at 56
    
    /* "       " at 1 lu-restaddr  */
    "       " at 1
    "       " at 1  /* fill the space */   
    with width 80 row 17 centered no-box overlay no-labels.

