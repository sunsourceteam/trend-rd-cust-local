/* SX 55
   d-smic.i 1.1 01/03/98 */
/* d-smic.i 1.3 7/23/92 */
/*h****************************************************************************
  INCLUDE      : d-smic
  DESCRIPTION  : Display for smic
  USED ONCE?   :
  AUTHOR       : enp
  DATE WRITTEN : 12/27/90
  CHANGES MADE :
    07/20/92 mwb; TB#  7191 Added custno change
    09/12/97 mms; TB# 22023 Year 2000 compliance; added y2k logic.
    02/08/02 mb;  TB# e9331 Inquiries wrong if no curr year data
******************************************************************************/

{w-arsc.i tt-smsc.custno no-lock}
assign
    m               = if frame-line(f-smic) = 0 then 1 else frame-line(f-smic)
    v-slsytd[m]     = 0
    v-slslytd[m]    = 0
    v-qtyytd[m]     = 0
    v-qtylytd[m]    = 0
    v-mgnytd[m]     = 0
    v-mgnlytd[m]    = 0
    v-cogytd[m]     = 0
    v-coglytd[m]    = 0
    v-custno[m]     = {c-empty.i}
    s-descrip       = if tt-smsc.shipto ne "" then "Ship To: " + tt-smsc.shipto
                      else if avail arsc then arsc.lookupnm else v-invalid.

find b-tt-smsc use-index k-smsc where
     b-tt-smsc.cono    = g-cono       and
     b-tt-smsc.custno  = tt-smsc.custno and
     b-tt-smsc.shipto  = tt-smsc.shipto and
     b-tt-smsc.yr      = ({y2ktwo.gca "~{y2kfour.gca tt-smsc.yr~} - 1"})
     no-lock no-error.

do j = s-month to s-month2:
  assign
    v-qtyytd[m]    = v-qtyytd[m] + tt-smsc.qtysold[j]
    v-slsytd[m]    = v-slsytd[m] + tt-smsc.salesamt[j]
    v-cogytd[m]    = v-cogytd[m] + tt-smsc.cogamt[j].
  if avail b-tt-smsc then 
    assign
      v-qtylytd[m]   = v-qtylytd[m] + b-tt-smsc.qtysold[j]
      v-slslytd[m]   = v-slslytd[m] + b-tt-smsc.salesamt[j]
      v-coglytd[m]   = v-coglytd[m] + b-tt-smsc.cogamt[j].
end.
assign
  v-mgnytd[m]    =   v-slsytd[m]  - v-cogytd[m]
  v-mgnlytd[m]   =   v-slslytd[m] - v-coglytd[m]
  s-qtyytd       =   if s-display = "Q" then v-qtyytd[m]  else
                     if s-display = "D" then v-slsytd[m]  else
                     if s-display = "M" then v-mgnytd[m]  else 0
  s-qtylytd      =   if s-display = "Q" then v-qtylytd[m] else
                     if s-display = "D" then v-slslytd[m] else
                     if s-display = "M" then v-mgnlytd[m] else 0
  s-varamt        =  s-qtyytd - s-qtylytd
  s-varpct        =  if s-qtylytd = 0 then 100  else
                     if s-qtyytd  = 0 then -100 else
                       round(((s-qtyytd - s-qtylytd) / s-qtylytd) * 100,2)
  s-varpct        =  if s-varpct > 0 then minimum(9999.99,s-varpct)
                     else maximum(-9999.99,s-varpct)
  v-custno[m]     =  tt-smsc.custno.

/* SX 55 */
   assign x-checkfocus = false.
   run CustFocusCheck.p(g-cono, 
                        0, 
                        0,
                        tt-smsc.custno,
                        tt-smsc.shipto,
                        input-output x-checkfocus).
   if x-checkfocus then
     do:
     s-descrip:dcolor in frame f-smic = 2.
     end.
   else
     do:
     s-descrip:dcolor in frame f-smic = 0.
     end.
 
/* SX 55 */

display
  tt-smsc.custno
  s-descrip
  s-qtyytd
  s-qtylytd
  s-varpct
  s-varamt
 with frame f-smic.


