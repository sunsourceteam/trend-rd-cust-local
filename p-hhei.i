/* update needs to loop like the .lds does for qty */

/* p-hhei.i  */
procedure unlockZsdi:
for each tmp-rcpt:
  if v-picktype = "o" then
    for each zsdipckdtl where zsdipckdtl.cono = g-cono and
    zsdipckdtl.whse = g-whse and 
    zsdipckdtl.orderno = tmp-rcpt.receiptid and
    zsdipckdtl.user2 = g-operinits:
      zsdipckdtl.user2 = "".
    end.
  else
    /* v-picktype = b for batch */
    for each zsdipckdtl where zsdipckdtl.cono = g-cono and
    zsdipckdtl.whse = g-whse and 
    zsdipckdtl.pickid = tmp-rcpt.receiptid and
    zsdipckdtl.user2 = g-operinits:
      zsdipckdtl.user2 = "".
  end.
end.
/* end. */
end procedure.

procedure generateReport:
define input parameter whichPrt like sapb.printernm no-undo.
/* def var rptnm as c no-undo. */
def var minRcpt as dec initial 2147483647 no-undo.
def var maxRcpt as dec initial 0 no-undo.
def var minInt as int initial 0 no-undo.
def var maxInt as int initial 2147483647 no-undo.
def var minChar as char initial "" no-undo.
def var maxChar as char initial "~~~~~~~~~~~~~~~~~~~~~~~~" no-undo.
def var minDate as date initial "01/01/50" no-undo.
def var maxDate as date initial "12/31/49" no-undo.

/**
{g-sapb.i new}   
{g-rpt.i}        
{g-rptctl.i new} 
**/
{p-rptnm.i v-reportnm " "}

create sapb.
assign
  sapb.cono = g-cono
  sapb.backfl      = no
  sapb.currproc    = "packreport"  /* porzp "rrcv"  */
  sapb.printoptfl  = no                                    
  sapb.priority    = 9    
  sapb.filefl      = no                                 
  sapb.inusecd     = "y"
  sapb.statustype  = ""
  sapb.demandfl    = no
  sapb.storefl     = no
  sapb.delfl       = yes.                                   

v-sapbid = recid(sapb).

find first sassr where sassr.currproc = sapb.currproc     
  no-lock no-error.                                         
{t-all.i sapb}                                                 
if avail sassr then sapb.rpttitle = sassr.rpttitle.

assign                                               
  v-reportid    = sassr.reportid                     
  v-openprfl    = sassr.openprfl                     
  v-openprno    = sassr.openprno                     
 /*  v-reportnm    = sapb.reportnm  */
  v-batfl       = false.                             
                                                                     
assign v-sapbid  = recid(sapb)                       
       v-sassrid = recid(sassr).                    
       v-saspid  = 0.        
sapb.reportnm = v-reportnm.
/* for each t-put /* tmp-rcpt */ no-lock:  break by tmp-rcpt.receiptid: */
find first t-put no-lock no-error.
  create sapbv.
  sapbv.selecttype       = v-picktype.
  sapbv.vendno = t-put.orderno.
  sapbv.cono   = g-cono.
  sapbv.reportnm = v-reportnm.
  sapbv.transproc = sapb.currproc.
  sapbv.operinit  = g-operinit.
  {t-all.i sapbv}
  
  maxRcpt = t-put.orderno.
  minRcpt = t-put.orderno.
  
/* end. */ 
sapb.printernm = whichPrt.

/* ranges 1 through 8 */
if v-picktype = "b" then do:
  sapb.rangebeg[1] = string(minInt).
  sapb.rangeend[1] = string(maxInt).
  sapb.rangebeg[2] = string(minRcpt).
  sapb.rangeend[2] = string(maxRcpt).
end.
else do: /* o */
  sapb.rangebeg[1] = string(minRcpt).
  sapb.rangeend[1] = string(maxRcpt).
  sapb.rangebeg[2] = string(minInt).
  sapb.rangeend[2] = string(maxInt).
end.
/*
sapb.rangebeg[1] = string(minInt).
sapb.rangeend[1] = string(maxInt).
sapb.rangebeg[2] = string(minInt).
sapb.rangeend[2] = string(maxInt). */
/*sapb.rangebeg[3] = string(minRcpt).
  sapb.rangeend[3] = string(maxRcpt). */
sapb.rangebeg[4] = string(minDate).
sapb.rangeend[4] = string(maxDate).
sapb.rangebeg[5] = string(minInt).
sapb.rangeend[5] = string(1000000000000).
sapb.rangebeg[6] = string(minChar).
sapb.rangeend[6] = string(maxChar).
sapb.rangebeg[7] = string(minDate).
sapb.rangeend[7] = string(maxDate).
sapb.rangebeg[8] = string(minChar).
sapb.rangeend[8] = string(maxChar).
sapb.optvalue[1] = g-whse.
sapb.optvalue[5] = g-operinits.
sapb.optvalue[6] = "E".
sapb.optvalue[7] = "D".
sapb.optvalue[8] = "yes".
sapb.optvalue[9] = "yes".
run rptctl2.p("packreport.p", yes, 0, yes, 0, yes). /* was rrcv */
/* for some reason not deleting */
find sapb where sapb.cono = g-cono and sapb.reportnm = v-reportnm
   no-error.
if avail sapb then 
  delete sapb.
message "". /* Clear Printing... message */
run unlockZsdi.
end procedure. /* generate report */

/* new F9 */
procedure displayprodbin:
if avail b-tprod then do:
  t-binloc1 = b-tprod.binloc1.
  t-binloc2 = b-tprod.binloc2.
  assign v-listlit = (if b-tprod.packcomplfl then "Completed"
                      else if b-tprod.extype <> "" then "Exception"
                      else "Open").
end.
else do:
  t-binloc1 = "".
  t-binloc2 = "".
  v-listlit = "".
end.                    
display t-binloc1 t-binloc2 v-listlit with frame f-hhepb.
apply "focus" to inSearch in frame f-prodlist.
end procedure.

/* f9 new showprodlist */
procedure showprodlist:
define input  parameter inKitLine as int no-undo.
define output parameter outProduct like icsp.prod no-undo.
define output parameter outBin like icsw.binloc1 no-undo.
outProduct = "".
outBin = "".
inSearch = "".

on cursor-up cursor-up.        
on cursor-down cursor-down.    
put screen row 13 column 1 color messages "Search F6 PROD   F7 Bins  ".

on entry of b-prodlist in frame f-prodlist do:
  run displayprodbin.
end.
on value-changed of b-prodlist in frame f-prodlist do:
  run displayprodbin.
end.

close query q-tprod.
if inKitLine = 0 then
open query q-tprod
  for each b-tprod 
  where b-tprod.recordtype ne "c"
  no-lock.
else
open query q-tprod
  for each b-tprod
  where b-tprod.lineno = inKitLine
  and b-tprod.recordtype = "c"
  no-lock.
  
thisLabel = "Product".
enable b-prodlist with frame f-prodlist.
display thisLabel b-prodlist with frame f-prodlist.
apply "entry" to b-prodlist. 
put screen row 13 column 1 color messages "Search F6 PROD   F7 Bins  ". 
update inSearch with frame f-prodlist
editing:
  readkey.
      
  if (lastkey = 501 or
      lastkey = 502 or
      lastkey = 503 or
      lastkey = 504 or
      lastkey = 507 or 
      lastkey = 508) then do:
    enable b-prodlist with frame f-prodlist. 
    apply lastkey to b-prodlist in frame f-prodlist.            
    apply "focus" to inSearch in frame f-prodlist.                
  end.         
  else do:
    if {k-after.i} or {k-accept.i} then do:
      if avail b-tprod then do:
        outProduct = b-tprod.prod.
        outBin = b-tprod.binloc1.
      end.  
      else do: 
        outProduct = input inSearch.
        outBin = "".
      end.  
    end.
    
    if not {k-cancel.i} then apply lastkey. 
    inSearch = input inSearch.
    /*
    if {k-func6.i} and thisLabel ne "Product" then do:
      thisLabel = "Product".
      put screen row 13 column 1 color messages "Search F6 PROD   F7 Bins  ".
    end.
    if {k-func7.i} and thisLabel ne "Bin" then do:
      thisLabel = "Bin".
      put screen row 13 column 1 color messages "Search F6 Prod   F7 BINS  ".
    end.
    close query q-tprod.
    if thisLabel = "Bin" then
      open query q-tprod 
      for each b-tprod where b-tprod.binloc1 begins input inSearch no-lock.
    else
      open query q-tprod 
      for each b-tprod where b-tprod.prod begins input inSearch no-lock.
    */
    display thisLabel inSearch b-prodlist with frame f-prodlist.
    run displayprodbin.
  end.
 
  if lastkey = 13 or keyfunction(lastkey) = "go" or {k-cancel.i} then do:
    on cursor-up back-tab.
    on cursor-down tab.
    hide frame f-prodlist.
    hide frame f-hhepb.
    apply "window-close" to frame f-prodlist.
    close query q-tprod.
    release b-tprod.
    /* put screen row 13 column 1 color messages "F6Ex F7 Bin2 F8Ids F9Srch       ". */

    return.
  end.
end.

on cursor-up back-tab.
on cursor-down tab.

end procedure.
/* f9 new showprodlist end */

/* new F9 */

/**
/* F9 show product list */
procedure showprodlist:
def var inSearch as char format "x(24)" no-undo.
def var thisLabel as char format "x(24)" no-undo.
form
  thisLabel at 1
  inSearch  at 1
with frame f-updSearch overlay no-labels row 4.

on cursor-up cursor-up.        
on cursor-down cursor-down.    

put screen row 13 column 1 color messages "Search F6 PROD   F7 Bins  ".

/* build table */
on entry of b-prodlist in frame f-prodlist do:
  t-binloc1 = b-tprod.binloc1.
  t-binloc2 = b-tprod.binloc2.
  assign v-listlit =  (if b-tprod.packcomplfl = true then
                         "Completed"
                       else
                       if b-tprod.extype <> "" then
                         "Exception"
                       else
                         "Open").

  display t-binloc1
          t-binloc2
          v-listlit
  with frame f-hhetb.
  put screen row 13 column 1 color messages "Search F6 PROD   F7 Bins  ".
  /* put screen row 13 column 1 color messages "F6ex F7 bin2 F8ids F9SRCH ". */
end.

on value-changed of b-prodlist in frame f-prodlist do:
  t-binloc1 = b-tprod.binloc1.
  t-binloc2 = b-tprod.binloc2.
  assign v-listlit =  (if b-tprod.packcomplfl = true then
                         "Completed"
                       else
                       if b-tprod.extype <> "" then
                         "Exception"
                       else
                         "Open").
  display t-binloc1
          t-binloc2
          v-listlit 
  with frame f-hhetb.
end.

on any-key of b-prodlist in frame f-prodlist do:
  inSearch = "".
  if {k-func6.i} then do:
    thisLabel = "Product".
    put screen row 13 column 1 color messages "Search F6 PROD   F7 Bins  ". 
    display thisLabel with frame f-updSearch.
    update inSearch with frame f-updSearch
    /* . */
    editing:
      readkey.
      apply lastkey.
      close query q-tprod.
      open query q-tprod 
        for each b-tprod where b-tprod.prod begins input inSearch
        use-index k-prod no-lock.
      display b-prodlist with frame f-prodlist.
      apply "focus" to inSearch in frame f-updSearch.
    end.
    hide frame f-updSearch.
    if {k-cancel.i} then
      put screen row 13 column 1 color messages "F6Ex F7 Bin2 F8Ids F9Srch ". 
    close query q-tprod.
    open query q-tprod 
    for each b-tprod where b-tprod.prod begins inSearch
      /* use-index k-tsum */ no-lock.
  end.
  else
  if {k-func7.i} then do:
    thisLabel = "Bin".
    put screen row 13 column 1 color messages "Search F6 Prod   F7 BINS  ".
    display thisLabel with frame f-updSearch.
    update inSearch with frame f-updSearch.
    hide frame f-updSearch.
    if {k-cancel.i} then
      put screen row 13 column 1 color messages "F6Ex F7 Bin2 F8Ids F9Srch ". 
    close query q-tprod.

    find first b-tprod where b-tprod.binloc1 begins inSearch no-lock no-error.
    if not avail b-tprod and substring(inSearch,1,1) <> " " then do:
      find first b-tprod where  b-tprod.binloc1 begins 
                                " " + inSearch no-lock no-error.
      if avail b-tprod then 
        assign insearch = " " + insearch.                  
    end.
    
    open query q-tprod 
    for each b-tprod where b-tprod.binloc1 begins inSearch
      /* use-index k-tsum */ no-lock.
    if avail b-tprod then do:
    assign v-listlit =  (if b-tprod.packcomplfl = true then
                         "Completed"
                       else
                       if b-tprod.extype <> "" then
                         "Exception"
                       else
                         "Open").
    t-binloc1 = b-tprod.binloc1.
    t-binloc2 = b-tprod.binloc2.
    display t-binloc1
            t-binloc2
            v-listlit
    with frame f-hhetb.
    end.
  end. 
  else
  if {k-cancel.i} or {k-jump.i} then do:
    hide frame f-prodlist.
    /* reset to what is on the current frame */
    t-binloc1 = t-put.binloc1.
    t-binloc2 = t-put.binloc2.
    apply "window-close" to frame f-prodlist.
    close query q-tprod.
    {hhei.lds} 
  end.
  else
  if keylabel(lastkey) = "RETURN" or keylabel(lastkey) = "PF1" then do:
    if avail b-tprod then do:
      find t-put where recid(t-put) =                    
        recid(b-tprod) no-lock no-error.
      /**
      assign
        t-shipprod = b-tprod.prod   
        t-proddesc1 = t-put.proddesc1 
        t-proddesc2 = t-put.proddesc2 
        t-stkqtyputaway = t-put.qtytopick  
        t-orderno       = t-put.receiptid
        t-lineno        = t-put.lineno
        t-binloc1   = b-tprod.binloc1   
        t-binloc2   = b-tprod.binloc2.  
      **/
      assign v-recid[1] = recid(b-tprod ).
      find t-put where recid(t-put) = recid(b-tprod) no-lock no-error.
      if avail t-put then
        assign h-binsort = t-put.binsort.
    end.
    hide frame f-prodlist.
    v-shipprod = "".
    v-binloc   = "".
    v-qtypicked = 0.
    apply "window-close" to frame f-prodlist.
    close query q-tprod.
    {hhei.lds} 
  end.
end. /* any-key */

close query q-tprod.
open query q-tprod
  for each b-tprod /* use-index k-tsum */ no-lock.

enable b-prodlist with frame f-prodlist.
apply "entry" to b-prodlist. 
display b-prodlist with frame f-prodlist.
wait-for window-close of frame f-prodlist.
on cursor-up back-tab.
on cursor-down tab.
close query q-tprod.
release b-tprod.                  
end procedure. /* showprodlist */
/* F9 show binloc1 list */
**/

procedure showbinlist:

on cursor-up cursor-up.        
on cursor-down cursor-down.    

put screen row 13 column 1 color messages "Search F6 Prod F7 BINS".

close query q-tbin.
open query q-tbin
  for each b-tbin /* use-index k-bin */ no-lock.
on any-key of b-binlist in frame f-binlist do:
  if {k-func6.i} then do:
    hide frame f-binlist.
    apply "window-close" to frame f-binlist.
    close query q-tbin.
    run showprodlist.
  end. 
  if {k-cancel.i} or {k-jump.i} then do:
    hide frame f-binlist.
    apply "window-close" to frame f-binlist.
  end.
  else
  if keylabel(lastkey) = "RETURN" or keylabel(lastkey) = "PF1" then do:
    find t-put where recid(t-put) =                    
      recid(b-tbin) no-lock no-error.
    /**
    assign
      t-shipprod      = b-tbin.prod   
      t-proddesc1     = t-put.proddesc1 
      t-proddesc2     = t-put.proddesc2 
      t-orderno       = t-put.receiptid
      t-lineno        = t-put.lineno
      t-stkqtyputaway = b-tbin.qtytopick  
      t-binloc1       = b-tbin.binloc1   
      t-binloc2       = b-tbin.binloc2.  
    **/      
    hide frame f-binlist.
    apply "window-close" to frame f-binlist.
    {hhei.lds}
  end.
end. /* any-key */

enable b-binlist with frame f-binlist.
apply "entry" to b-binlist. 
display b-binlist with frame f-binlist.
wait-for window-close of frame f-binlist.
on cursor-up back-tab.
on cursor-down tab.
close query q-tbin.
release b-tbin.                  
end procedure. /* showprodlist */

/* end of F9 */

procedure updateZsdiput:
define input parameter i-recid as recid.
define input parameter i-qty   as decimal no-undo.
define input parameter i-exstat as char no-undo.

define buffer b-zsd for zsdipckdtl.
find b-zsd where recid(b-zsd) = i-recid no-lock no-error.
if not avail b-zsd then do:
  message i-recid "record not found to update". pause.
  return.
end.

if b-zsd.recordtype = "" then do:   /* not kit or component */
  for each sum-tput where sum-tput.orderno = b-zsd.orderno and
  sum-tput.suf = b-zsd.ordersuf /* and sum-tput.lineno = b-zsd.lineno */
  and /* quality check */ sum-tput.prod = b-zsd.shipprod:
    find first zsdipckdtl where recid(zsdipckdtl) = sum-tput.zput.
    
    zsdipckdtl.realqtypack = if i-qty ne 0 then zsdipckdtl.realqtypicked
                             else 0. 
    zsdipckdtl.operinit = g-operinit. 
    zsdipckdtl.packstatusty = i-exstat.
    zsdipckdtl.packcomplfl = yes.
    /* zsdipckdtl.recordtype = "PACK". "", k, c */
    zsdipckdtl.xuser2 = g-operinit.
    zsdipckdtl.xuser9 = today.
    {t-all.i zsdipckdtl}

    sum-tput.packcomplfl = yes.
    sum-tput.extype     = i-exstat.
    sum-tput.qtypacked  = zsdipckdtl.realqtypack. 
  end. 
end.
else do:    /* kit or component */
  for each sum-tput where sum-tput.orderno = b-zsd.orderno and
  sum-tput.suf = b-zsd.ordersuf and sum-tput.lineno = b-zsd.lineno 
  and /* quality check */ sum-tput.prod = b-zsd.shipprod:
    find first zsdipckdtl where recid(zsdipckdtl) = sum-tput.zput.
    
    zsdipckdtl.realqtypack = if i-qty ne 0 then zsdipckdtl.realqtypicked
                             else 0. 
    zsdipckdtl.operinit = g-operinit. 
    zsdipckdtl.packstatusty = i-exstat.
    zsdipckdtl.packcomplfl = yes.
    zsdipckdtl.xuser2 = g-operinit.
    zsdipckdtl.xuser9 = today.
    {t-all.i zsdipckdtl}

    sum-tput.packcomplfl = yes.
    sum-tput.extype     = i-exstat.
    sum-tput.qtypacked  = zsdipckdtl.realqtypack. 
  end. 
  if b-zsd.recordtype = "k" then do: /* update all components */
    for each sum-tput where sum-tput.orderno = b-zsd.orderno and
    sum-tput.suf = b-zsd.ordersuf and sum-tput.lineno = b-zsd.lineno: 
      find first zsdipckdtl where recid(zsdipckdtl) = sum-tput.zput.
      zsdipckdtl.realqtypack = if i-qty ne 0 then zsdipckdtl.realqtypicked
                               else 0. 
      zsdipckdtl.operinit = g-operinit. 
      zsdipckdtl.packstatusty = i-exstat.
      zsdipckdtl.packcomplfl = yes.
      zsdipckdtl.xuser2 = g-operinit.
      zsdipckdtl.xuser9 = today.
      {t-all.i zsdipckdtl}

      sum-tput.packcomplfl = yes.
      sum-tput.extype     = i-exstat.
      sum-tput.qtypacked  = zsdipckdtl.realqtypack. 
    end. 
  end.
end.   
end procedure.

procedure checkReport:
define input  parameter inKitFlag as int       no-undo.
define output parameter o-rptFlag as logical   no-undo.   
def var answer as logical format "yes/no" no-undo. 
def var whichPrinter like sapb.printernm no-undo.
def buffer bf-t-put for t-put.

form
  whichPrinter  at 4
with frame f-prt title "Exception Printer"
row 4 column 4 overlay. 

form answer label  "Batch Completed ?" at 2   
with frame f-complete side-labels row 4 overlay. 
 
assign o-rptFlag = no.
       answer    = no.
if inKitFlag = 0 then 
  find first bf-t-put where                                   
           bf-t-put.packcomplfl = no and            
           bf-t-put.recordtype ne "c"            
           no-lock no-error.    /* no exceptions! */
           
if not avail bf-t-put and inKitFlag = 0 then do:
  /* all complete */
  /* ask if they want to finsh/print report */
  display answer with frame f-complete.
  status input "Arrow up/dn change answer ".
  completeloop:
  do on endkey undo completeloop, leave completeloop:                         
    update answer                 
    with frame f-complete
    editing:
      readkey.
      if lastkey = 501 or lastkey = 502 then do:
        if input answer = true then do:
          assign answer = false.
          display answer with frame f-complete.
          next.
        end.
        else
        if input answer = false then do:
          assign answer = true.
          display answer with frame f-complete.
          next.
        end.
      end.
      apply lastkey.
    end.    
    status input "                          ".
         
    if keyfunction(lastkey) = "GO" or                                 
      keyfunction(lastkey) = "RETURN" then do:                   
      hide frame f-complete no-pause.                                 
      leave completeloop.                            
    end.      
  end. /* completeloop */                                                
  hide frame f-complete.
  if answer = yes then do:
    find first bf-t-put where bf-t-put.extype ne "" no-lock no-error.
    if not avail bf-t-put then do:
      o-rptFlag = yes. /* report would be blank */
      return.
    end.
    {w-sasoo.i g-operinit no-lock}
    if avail sasoo then
      whichPrinter = sasoo.printernm. 
    if whichPrinter = "" or whichPrinter = "vid" then do:
      {w-icsd.i g-whse no-lock}
      if avail icsd then
        whichPrinter = icsd.printernm[1].
    end.
    input clear.
    readkey pause 0.
    display whichPrinter with frame f-prt.
    update  whichPrinter with frame f-prt
    editing:
      readkey.
      if {k-cancel.i} then do:
        whichPrinter = "".
        hide frame f-prt.
        assign  o-rptFlag = no.
        return.
      end.

      if {k-sdileave.i &fieldname     = "whichPrinter"
                       &completename  = "whichPrinter"} then do:
        find first sasp where sasp.printernm = input whichPrinter
                       no-lock no-error.
        if not avail sasp then do:
          message "Invalid Printer".
          apply lastkey.
          next-prompt whichPrinter with frame f-prt.
          next.
        end.
      end.
      apply lastkey.
    end.
    hide frame f-prt.
    message "Printing Report...". 
    run generateReport(input whichPrinter).
    message "".                     
    o-rptFlag = yes.
  end.
end.

clear frame f-hhet.
hide frame f-hhet.
view frame f-hhet-h.
/*
message "end of p-hhep checkRep". pause.
apply "focus" to v-ordernum in frame f-hhet-h. */
apply "entry" to v-ordernum in frame f-hhet-h.
end procedure.

procedure validateException:
define input  parameter i-tRecid as recid no-undo.
define output parameter o-doneFlag  as logical initial no no-undo.

def buffer bf-t-put for t-put.
def var canswer as logical format "yes/no" no-undo. 
def var cquestion as char format "x(17)" no-undo.
form
 cquestion at 2
 canswer at 20 no-label    
with frame f-clear no-labels row 4 overlay. 

find first bf-t-put where recid(bf-t-put) = i-tRecid no-error. 

if avail bf-t-put then do:
  if bf-t-put.extype = "" then        
    assign cquestion = "Clear from Batch?".
  else
    assign cquestion = "Clear Exception ?".
end.    

if avail bf-t-put then do:
  display cquestion
          canswer with frame f-clear.
  status input "Arrow up/dn change answer ".

  clearloop:
  do on endkey undo clearloop, leave clearloop:                         
    update canswer 
      with frame f-clear
      editing:
        readkey.
        if lastkey = 501 or lastkey = 502 then do:
          if input canswer = true then do:
             assign canswer = false.
             display cquestion canswer with frame f-clear.
             next.
          end.
          else
          if input canswer = false then do:
             assign canswer = true.
             display cquestion canswer with frame f-clear.
             next.
          end.
        end.
      apply lastkey.
      end.    
    
      status input "                          ".
        
      if keyfunction(lastkey) = "GO" or                                 
         keyfunction(lastkey) = "RETURN" then do:                   
        hide frame f-clear no-pause.                                 
        leave clearloop.                            
      end.      
   end. /* clearloop */                                                

  hide frame f-clear.
end.
if canswer = yes then do:
  /*
  bf-t-put.packstatusty = "D1".
  bf-t-put.packcomplfl = yes.
  */
  run updateZsdiput         
    (input bf-t-put.zput, 0, "D1").
end.
end procedure.  /* validateException */

procedure setTempRecords:        
define input parameter i-type as char no-undo. /* batch or order */
define input parameter i-stat as char no-undo. /* All, Kit, Loose */
def var sumqty like zsdiput.stkqtyputaway initial 0 no-undo.
def var fndCnt as int initial 0 no-undo. 


fndCnt = 0.
for each tmp-rcpt no-lock:
  find first t-put where 
             t-put.receiptid = tmp-rcpt.receiptid no-lock no-error.
  if not avail t-put then do:
    find first zsdipckdtl where zsdipckdtl.cono = g-cono and
             zsdipckdtl.whse = g-whse and 
             zsdipckdtl.orderno =  tmp-rcpt.receiptid and
             zsdipckdtl.ordersuf = tmp-rcpt.receiptsuf and
             (zsdipckdtl.completefl = no or zsdipckdtl.extype ne "")
    no-lock no-error.
    if avail zsdipckdtl then do:
      loaded-sw = no.
      if zsdipckdtl.completefl = no then
        message "Order not fully picked".
      else
        message "Exception in Order".
      return.  
    end.

    for each zsdipckdtl where zsdipckdtl.cono = g-cono and
             zsdipckdtl.whse = g-whse and 
             zsdipckdtl.orderno =  tmp-rcpt.receiptid and
             zsdipckdtl.ordersuf = tmp-rcpt.receiptsuf and
             zsdipckdtl.completefl = yes :
      if i-stat ne "A" then do:
        if i-stat = "k" and zsdipckdtl.statustype ne "c" then next.
        if i-stat = "l" and zsdipckdtl.statustype = "c" then next.
      end.                             
      /* new */
      if locked zsdipckdtl then do:
        message "Order locked".
        next.
      end.
      if zsdipckdtl.user2 ne "" and zsdipckdtl.user2 ne g-operinits then do:
        message "Being Picked by" zsdipckdtl.user2. 
        next.
      end.  
      fndcnt = fndcnt + 1.
      unixCmd = "who -mu".                                          
      input through value(unixCmd).                                 
      import who[1] who[2] who[3] who[4] who[5] who[6] who[7] who[8].
      input close.                                                  
      
      zsdipckdtl.user2 = g-operinits.
      zsdipckdtl.user3 = who[2]. 
      zsdipckdtl.user4 = who[8]. 
      zsdipckdtl.user5 = v-picktype.

      idcnt = idcnt + 1. 
      create t-put.
      assign
        t-put.whse = v-whse
        t-put.receiptid = zsdipckdtl.orderno
        t-put.orderno   = zsdipckdtl.orderno
        t-put.suf       = zsdipckdtl.ordersuf
        t-put.lineno    = zsdipckdtl.lineno
        t-put.seqno     = zsdipckdtl.seqno
        t-put.prod = zsdipckdtl.shipprod
        t-put.bin  = zsdipckdtl.pickbin /* "" */
        t-put.binloc1 = zsdipckdtl.binloc1
        t-put.binloc2 = zsdipckdtl.binloc2
        t-put.qtytopick  = zsdipckdtl.stkqtypicked  
        t-put.qtypicked  = zsdipckdtl.realqtypicked /* 0 */
        t-put.oper = g-operinit /* zsdiput.operinit */
        t-put.extype = "" /* zsdipckdtl.exstatustype packstatusty */
        t-put.complete = zsdipckdtl.completefl
        t-put.packcomplfl = zsdipckdtl.packcomplfl
        t-put.transdt  = zsdipckdtl.transdt
        t-put.transtm  = zsdipckdtl.transtm
        t-put.zone     = if zsdipckdtl.binloc1 = "" or
                            zsdipckdtl.binloc1 = "New Part" then
                           "````"
                         else
                            zsdipckdtl.locationid
        t-put.zput     = recid(zsdipckdtl)
        t-put.idno     = idcnt
        t-put.recordtype = zsdipckdtl.recordtype
        t-put.batchno  = zsdipckdtl.pickid
        t-put.batchseq = zsdipckdtl.pickseq
        t-put.binsort = if (t-put.binloc1 <> "" and                   
                            t-put.binloc1 <> "New Part")              
                        then substring(t-put.binloc1,1,2) + "````````"
                        else                                             
                          if (t-put.binloc1 = "" or                   
                              t-put.binloc1 = "New Part") and         
                             zsdipckdtl.xuser1 <> ""                    
                          then zsdipckdtl.xuser1                         
                          else "``````````".                             

        find first oeel where oeel.cono = g-cono and
        oeel.orderno = zsdipckdtl.orderno and 
        oeel.ordersuf = zsdipckdtl.ordersuf and 
        oeel.lineno = zsdipckdtl.lineno
        no-lock no-error.
        find first icsp where icsp.cono = g-cono and
        icsp.prod = zsdipckdtl.shipprod no-lock no-error.

        if avail oeel then do:
          if oeel.proddesc ne "" then
            t-put.desc1 = oeel.proddesc.
          if oeel.proddesc2 ne "" then
            t-put.desc2 = oeel.proddesc2.
        end.
        if avail icsp then do:
          if t-put.desc1 = "" and icsp.descrip[1] ne "" then
            t-put.desc1 = icsp.descrip[1].
          if t-put.desc2 = "" and icsp.descrip[2] ne "" then
            t-put.desc2 = icsp.descrip[2].
          find first icsw where icsw.cono = g-cono and
          icsw.prod = icsp.prod and icsw.whse = v-whse no-lock no-error.
          if avail icsw then do:
            t-put.vendprod = icsw.vendprod.
            t-put.vendno   = icsw.arpvendno.
          end.
        end.    
            
     end. /* for each zsdipckdtl */
  end.
end.
if fndCnt > 0 then loaded-sw = yes.
else do:
  loaded-sw = no.
  message "No valid unopen records found".
end.  
end procedure. /* setTempRecs */

procedure putawaybrowse:
define input parameter i-whse like icer.whse.
define input parameter i-receipt as dec.
define input parameter i-jrnlno  as dec.
define input parameter i-style   as char.
define output parameter o-out    as char.

/* define buffer b-zsdiput for zsdiput. */
define buffer b-zsdipck for t-put.
define query q-pck for b-zsdipck scrolling. 
                                               
define browse b-pck query q-pck          
display /* if i-style = "Prod" then */
        if num-results("q-pck") >= 1 then
          STRING(b-zsdipck.prod,"x(24)")
        else
          STRING("No items found", "x(24)")
        /*
        else if i-style = "bin" then
          STRING(b-zsdipck.locationid, "x(24)")   
        else if i-style = "receipt" then
          STRING(b-zsdipck.receiptid, "x(24)")
        else
        STRING(b-zsdipck.whse + " " + STRING(b-zsdipck.putrecno,"x(19)"), "x(24)")     */
        format "x(24)"                          
with size-chars 26 by 9 down                  
centered overlay no-hide no-labels.             
              
form
  b-pck
with frame f-pck row 1 no-hide no-box overlay no-labels.

on cursor-up cursor-up.        
on cursor-down cursor-down.    

/* should match the criteria in the build t-put */
/*
if integer(i-jrnlno) > 0 then  
  open query q-put
  for each b-zsdiput where b-zsdiput.cono = g-cono and b-zsdiput.whse = i-whse
  and b-zsdiput.jrnlno = integer(i-jrnlno)
  and b-zsdiput.completefl = no and zsdiput.extype = "" no-lock.
else if integer(i-receipt) > 0 then
  open query q-put                                                      
  for each b-zsdiput where b-zsdiput.cono = g-cono and b-zsdiput.whse = i-whse
  and b-zsdiput.receiptid = integer(i-receipt) 
  and b-zsdiput.completefl = no and zsdiput.extype = "" no-lock.               */
open query q-pck for each b-zsdipck no-lock.        

on any-key of b-pck do:
  if {k-cancel.i} or {k-jump.i} then do:
    hide frame f-pck.
    o-out = "".
    apply "window-close" to frame f-pck.
  end.
  else
  if keylabel(lastkey) = "RETURN" or keylabel(lastkey) = "PF1" then do:
    if avail b-zsdipck then do:
      case i-style:
        when "Prod" then o-out = b-zsdipck.prod.
      end case.
    end.
    else o-out = "".
    hide frame f-pck.
    apply "window-close" to frame f-pck.
  end.
end. /* any-key */

enable b-pck with frame f-pck.
apply "entry" to b-pck.
display b-pck with frame f-pck.
wait-for window-close of frame f-pck.
on cursor-up back-tab.
on cursor-down tab.
close query q-pck.
release b-zsdipck.                  
end procedure. /* putawaybrowse */

/************** TOMS *************************/

procedure LoadBinList:
  define input parameter ip-cono like icsw.cono no-undo.
  define input parameter ip-prod like icsw.prod no-undo.
  define input parameter ip-whse like icsw.prod no-undo.

  define buffer ix-icsw     for icsw.
  define buffer ix-wmsb     for wmsb.
  define buffer ix-wmsbp    for wmsbp.
  define buffer ix-t-put for t-put.
  define var    ix-cnt   as integer no-undo.

  find first t-binlist where 
             t-binlist.prod = ip-prod and
             t-binlist.whse = ip-whse no-lock no-error.
  if not avail t-binlist then do:
    for each t-binlist :
      delete t-binlist.
    end.  
    find ix-icsw where 
         ix-icsw.cono = ip-cono and
         ix-icsw.prod = ip-prod and
         ix-icsw.whse = ip-whse no-lock no-error.
    if avail ix-icsw then do:
      /* Bin Location 1 */
      if ix-icsw.binloc1 <> "" and 
         ix-icsw.binloc1 <> "New Part" then do:
        create t-binlist.
        assign t-binlist.whse   = ip-whse
               t-binlist.prod   = ip-prod
               t-binlist.binloc = ix-icsw.binloc1
               t-binlist.binnum = 1
               ix-cnt = 1.
      end.          

      /* Bin Location 2 */
      if ix-icsw.binloc2 <> "" and 
         ix-icsw.binloc2 <> "New Part" then do:
        create t-binlist.
        assign t-binlist.whse   = ip-whse
               t-binlist.prod   = ip-prod
               t-binlist.binloc = ix-icsw.binloc2
               t-binlist.binnum = 2
               ix-cnt = 2.
      end.          
    end. 
   
    for each ix-wmsbp where 
               ix-wmsbp.cono = ip-cono + 500 and
               ix-wmsbp.whse = ip-whse and
               ix-wmsbp.prod = ip-prod no-lock:
      find   ix-wmsb where 
             ix-wmsb.cono = ip-cono + 500 and
             ix-wmsb.whse = ip-whse and
             ix-wmsb.binloc = ix-wmsbp.binloc and
             ix-wmsb.priority = 9 no-lock no-error.
      if avail ix-wmsb then do:
        create t-binlist.
        assign t-binlist.whse   = ip-whse
               t-binlist.prod   = ip-prod
               t-binlist.binloc = ix-wmsbp.binloc
               t-binlist.binnum = ix-cnt + 1
               ix-cnt = ix-cnt + 1.
      end.
    end. /* for each IX-WMSBP  */                            
  end.   /*   if not avail t-binlist  */
  else
    for each t-binlist:
      assign t-binlist.binused = false.
    end.
      
  for each t-binlist:
    find first ix-t-put where
               ix-t-put.prod = ip-prod and
               ix-t-put.binloc1 = t-binlist.binloc no-lock no-error.
    if avail ix-t-put then
      assign t-binlist.binused  = true.
  end.                  
end. /* procedure LoadBinList */
   
procedure F6PopUp:
/* choose an exception code */
define output parameter outExceptCode as char no-undo.

def var exReason as char format "x(2)" no-undo.
def var exError as logical no-undo.
def var cancelFlag as logical no-undo.

form
  exReason label "Exception Code" dcolor 2 
with frame f-reasoncode at row 6 col 1 side-labels overlay width 24.

cancelFlag = no.
exError = yes.
do while exError:
  update exReason with frame f-reasoncode
  editing:
    readkey.
    if {k-accept.i} or {k-return.i} then do:
      find first zsastz where zsastz.cono = g-cono and
      zsastz.codeiden = "PackExceptions" and
      zsastz.primarykey = input exReason    and
      zsastz.labelfl = no no-lock no-error.
      if avail zsastz then
        exError = no.
      else
        exError = yes.
      apply lastkey.
    end.
    else if ({k-cancel.i} or {k-jump.i}) then do:
      cancelFlag = yes.
      /* exReason = "". */
      exError = no.
      leave.
    end.
    else if ({k-func10.i} or {k-func12.i}) then do:
      run reasoncd.p(input g-cono, input "PackExceptions",
      input "  Select Exception Code  ", input yes,
      output exReason). /* choose exception code */
      display exReason with frame f-reasoncode.
    end.
    else apply lastkey.
  end. /* edit */
  if exError then message "Invalid Reason".
end.
hide frame f-reasoncode.
message "".
if cancelFlag then exReason = "".
outExceptCode = exReason.
/* message outExceptCode. pause 3. */
end procedure.


procedure F6Exception:

  define input parameter ip-zput as recid no-undo.
  define input parameter ip-exstat as char no-undo.
  define buffer xi-zsdipckdtl    for zsdipckdtl. 
  
  find first xi-zsdipckdtl where recid(xi-zsdipckdtl) = ip-zput no-error.
  
  if avail xi-zsdipckdtl then do:
    xi-zsdipckdtl.packstatusty  = ip-exstat.
    xi-zsdipckdtl.operinit      = g-operinit.
    xi-zsdipckdtl.realqtypack   = 0.
    xi-zsdipckdtl.packcomplfl   = no.
    t-put.extype             = ip-exstat.
    t-put.qtypacked          = 0.
    t-put.packcomplfl        = no.
  end.
  readkey pause 0.
end.  /* procedure F6Exception */

/*
procedure F7BinSplit:
  define input  parameter ip-cono like icsw.cono no-undo.
  define input  parameter ip-shipprod like icsw.prod   no-undo.
  define input  parameter ip-whse like icsw.prod       no-undo.
  define input  parameter ip-binloc  as character      no-undo.
  define output parameter ip-binloc1 like icsw.binloc1 no-undo.
  define output parameter ip-binloc2 like icsw.binloc1 no-undo.
  define output parameter ip-errorlit as character format "x(24)" no-undo.

  define buffer xn-zsdipck    for zsdipckdtl.
  
  define buffer xi-zsdipck    for zsdipckdtl.
  define buffer xi-t-put      for t-put.
  define buffer xi-wmsb       for wmsb.

  define buffer x2-zsdipck    for zsdipckdtl.
  define buffer x2-t-put      for t-put.

  define var    ix-cnt   as integer no-undo.
  define var    ix-currentbinno   as integer no-undo.

  run LoadBinList (input ip-cono,     
                   input ip-shipprod, 
                   input ip-whse).    

  assign ix-cnt = 0
         ip-binloc1 = ""    
         ip-binloc2 = ""
         ip-errorlit = "".    

  for each t-binlist use-index ix-binnum where 
           t-binlist.binused = false:
    if ix-cnt = 0 then
      assign ip-binloc1 = t-binlist.binloc
             ix-currentbinno = ix-cnt + 1
             t-binlist.binused = true.
    else
      assign ip-binloc2 = t-binlist.binloc.
    ix-cnt = ix-cnt + 1.
    if ix-cnt ge 2 then
      leave.
  end.    

  find first zsdipckdtl where recid(zsdipckdtl) = t-put.zput no-lock
    no-error.
    
  run updateZsdiput( input t-put.zput, t-put.prod, t-put.qtypicked, t-put.bin).
  if not avail zsdipckdtl then do:
    message "couldn't find this to copy".
    pause.
    return.
  end.
  else
  find first xi-zsdipck where xi-zsdipck.cono = zsdipckdtl.cono and
  xi-zsdipck.orderno  = zsdipckdtl.orderno and
  xi-zsdipck.ordersuf = zsdipckdtl.ordersuf and
  xi-zsdipck.lineno   = zsdipckdtl.lineno and
  xi-zsdipck.seqno    = zsdipckdtl.seqno no-error.
  
  find first xi-wmsb where 
             xi-wmsb.cono = ip-cono and
             xi-wmsb.whse = ip-whse and 
             xi-wmsb.binloc = ip-binloc1 no-lock no-error.
      
  find last xn-zsdipck /* use-index putin */ no-lock no-error.
  
  create x2-zsdipck.
  buffer-copy xi-zsdipck  
        except xi-zsdipck.binloc1
               xi-zsdipck.binloc2
               xi-zsdipck.recordtype
               /* dkt 05/23/07 doubled quantities */
               xi-zsdipck.stkqtypicked
               xi-zsdipck.realqtypicked
               xi-zsdipck.completefl
  to x2-zsdipck 
        assign x2-zsdipck.binloc1      = ip-binloc1
               x2-zsdipck.binloc2      = ip-binloc2
               x2-zsdipck.packcomplfl   = no
               x2-zsdipck.stkqtypicked  = t-put.qtytopick - t-put.qtypicked                    x2-zsdipck.locationid   = if avail xi-wmsb and 
                                            ip-binloc1 <> "" and
                                            ip-binloc1 ne "New Part" then
                                           xi-wmsb.building
                                         else if ip-binloc1 = "" or
                                            ip-binloc1 = "New Part" then
                                           "````" 
                                         else
                                           "    "  
               x2-zsdipck.xuser1       = if xi-zsdipck.binloc1 <> "" and
                                            xi-zsdipck.binloc1 <> "New Part"                                                  and ip-binloc1 = "" then
                                           substring(xi-zsdipck.binloc1,1,2) +
                                               "````````"
                                         else
                                           ""
                                         
               x2-zsdipck.user1 = "Copy" + string(ix-currentbinno, ">>9")
               x2-zsdipck.user2        = g-operinits.
      
  find last x2-t-put use-index k-bin /* k-idno */ no-lock no-error.

  create xi-t-put.
      assign
        xi-t-put.whse         = v-whse
        xi-t-put.receiptid    = x2-zsdipck.orderno
        xi-t-put.orderno      = x2-zsdipck.orderno 
        xi-t-put.suf          = x2-zsdipck.ordersuf 
        xi-t-put.lineno       = x2-zsdipck.lineno
        xi-t-put.prod         = x2-zsdipck.shipprod
        xi-t-put.bin          = x2-zsdipck.binloc1
        xi-t-put.binloc1      = x2-zsdipck.binloc1
        xi-t-put.binloc2      = x2-zsdipck.binloc2
        xi-t-put.qtypicked    = 0 /* x2-zsdiput.stkqtyputaway  */
        xi-t-put.qtytopick    = x2-zsdipck.stkqtypicked /* 0  ?? */
        xi-t-put.oper         = g-operinit /* zsdiput.operinit */
        xi-t-put.extype       = x2-zsdipck.extype
        xi-t-put.complete     = x2-zsdipck.completefl
        xi-t-put.packcomplfl  = x2-zsdipck.packcomplfl
        xi-t-put.transdt      = x2-zsdipck.transdt
        xi-t-put.transtm      = x2-zsdipck.transtm
        xi-t-put.zput         = recid(x2-zsdipck)
        xi-t-put.zone         = x2-zsdipck.locationid
        
        xi-t-put.binsort = if (xi-t-put.binloc1 <> "" and         
                               xi-t-put.binloc1 <> "New Part")
                           then substring(xi-t-put.binloc1,1,2) + "````````"                                else                                      
                             if (xi-t-put.binloc1 = "" or           
                                 xi-t-put.binloc1 = "New Part") and 
                                 x2-zsdipck.xuser1 <> ""
                             then x2-zsdipck.xuser1                       
                             else "``````````".                            
end.  /* procedure F7BinSplit */
*/

procedure GetBinLocs:
  define input  parameter ip-cono       like icsw.cono    no-undo.
  define input  parameter ip-shipprod   like icsw.prod    no-undo.
  define input  parameter ip-whse like  icsw.prod         no-undo.
  define input  parameter ip-recordcode as character      no-undo.
  define output parameter ip-binloc1    like icsw.binloc1 no-undo.
  define output parameter ip-binloc2    like icsw.binloc1 no-undo.

  define var    ix-currentbinno   as integer no-undo.
  define var    ix-cnt   as integer no-undo.


  run LoadBinList (input ip-cono,
                   input ip-shipprod,
                   input ip-whse).

  if ip-recordcode  = "" then
    assign ix-currentbinno = 1.
  else if ip-recordcode begins "Bin" then
    assign ix-currentbinno = int(substr(ip-recordcode,4,3)).    

  assign ix-cnt = 0
         ip-binloc1 = ""    
         ip-binloc2 = "".    

  for each t-binlist: 
    ix-cnt = ix-cnt + 1.
    if ix-cnt < ix-currentbinno then 
      next.
    if ix-cnt = ix-currentbinno  then
      assign ip-binloc1 = t-binlist.binloc
             t-binlist.binused = true.
    else
      assign ip-binloc2 = t-binlist.binloc.
    if ix-cnt ge (ix-currentbinno + 1) then
      leave.
  end.    
end. 

procedure DetermineProductAction:
define input  parameter ip-shipprod   like icsp.prod no-undo.
define output parameter ip-actioncode as character   no-undo.

def var doFind as logical no-undo.
def buffer ix-t-put for t-put.

assign ip-actioncode = "".

if ip-shipprod = "" then do:                                   
  confirm = no.                            
  assign ip-actioncode = "ProdErr".
  message "p-1 Re-scan part number".
  return.
end.    

find ix-t-put where recid(ix-t-put) = recid(t-put) no-lock no-error.

if ip-shipprod ne "" then do:
  doFind = no.
  if avail ix-t-put then do:
    if ix-t-put.prod ne ip-shipprod then 
      assign doFind = yes.
  end.
  else doFind = yes.
end.
else 
  doFind = no.

if doFind then do:
  /* entered a product */
  find first ix-t-put where ix-t-put.prod = ip-shipprod and
                               ix-t-put.packcomplfl = no and
                               ix-t-put.extype     = "" 
                               no-lock no-error.
  if avail ix-t-put then
     assign ip-actioncode = "Prodjump".
  
  /* entered a bin */
  if not avail ix-t-put then do:
    find first ix-t-put where 
               ix-t-put.binloc1 = ip-shipprod and
               ix-t-put.packcomplfl = no and
               ix-t-put.extype     = "" no-lock no-error.
    if avail ix-t-put then
      assign ip-actioncode = "Prodjump".
  end. 
    if not avail ix-t-put then do:
    find first ix-t-put where 
               ix-t-put.binloc1 = " " + ip-shipprod and
               ix-t-put.packcomplfl = no and
               ix-t-put.extype     = "" no-lock no-error.
    if avail ix-t-put then
      assign ip-actioncode = "Prodjump".
  end. 

  if not avail ix-t-put then do:
    find first ix-t-put where 
               ix-t-put.binloc2 = ip-shipprod and
               ix-t-put.packcomplfl = no and
               ix-t-put.extype     = "" no-lock no-error.
    if avail ix-t-put then
      assign ip-actioncode = "Prodjump".
  end.    
  if not avail ix-t-put then do:
    find first ix-t-put where 
               ix-t-put.binloc2 = " " + ip-shipprod and
               ix-t-put.packcomplfl = no and
               ix-t-put.extype     = "" no-lock no-error.
    if avail ix-t-put then
      assign ip-actioncode = "Prodjump".
  end.    
                                              
  if avail ix-t-put then do: 
    /* dkt */
    find t-put where recid(t-put) = recid(ix-t-put) no-lock
    no-error.
    
    t-shipprod = ix-t-put.prod.
    assign                                       
      v-recid    = 0                             
      v-recid[1] = recid(ix-t-put)                  
      v-curpos   = 2 
      confirm    = no                            
      v-nextfl   = yes.                          
    v-shipprod = ip-shipprod.
    {hhei.lds}
    if ip-actioncode = "" then
      assign ip-actioncode = "Bin".
  end. 
  else do:                                   
    confirm = no.                            
    assign ip-actioncode = "ProdErr".
    message "p-2 Re-scan part number".
  end.      
end.
end procedure.


procedure DetermineBinAction:
define input-output  parameter ip-binloc    like icsw.binloc1 no-undo.
define input  parameter ip-bin1       like t-put.binloc1 no-undo.
define input  parameter ip-shipprod   like icsp.prod no-undo.
define output parameter ip-actioncode as character   no-undo.

def buffer ix-t-put for t-put.

if keylabel(lastkey) = "cursor-up" /* and ip-binloc = "" */ then do:
  ip-actioncode = "upKey".
  return.
end.

if ip-binloc = "" and (ip-bin1 = "" or ip-bin1 = "New Part") then do:
  ip-actioncode = "good".
  return.
end.

find ix-t-put where recid(ix-t-put) = recid(t-put) no-lock no-error.
assign ip-actioncode = "".
if not avail ix-t-put then do:
  ip-actioncode = "binerr".
  message "Bin not found".
  return.
end.

/* CANNOT run updateZsdi inside of determineBinLocation!!!
   Must move it so that it only happens on cursor-down or F1
   and all info right - that means HHEP.LFU? */

if ip-binloc = ix-t-put.binloc1 and 
  ip-binloc ne "" and 
  ip-binloc ne "New Part" then do:
  ip-actioncode = "good1".
end.
else
if " " + ip-binloc = ix-t-put.binloc1 and 
   ip-binloc ne "" and 
   ip-binloc ne "New Part" then do:
  assign ip-binloc = " " + ip-binloc. 
         ip-actioncode = "good2".
end.

else if ip-binloc ne "" and ix-t-put.binloc1 ne "" and
  ix-t-put.binloc1 ne "New Part"
  then do: 
    /* give message that this is not the normal bin? */
    find first wmsbp where wmsbp.cono = g-cono + 500 and
             wmsbp.whse = g-whse and
             wmsbp.prod = ip-shipprod and 
             wmsbp.binloc = ip-binloc no-lock no-error.
    if not avail wmsbp then
      find first wmsbp where wmsbp.cono = g-cono + 500 and
                 wmsbp.whse = g-whse and
                 wmsbp.prod = ip-shipprod and 
                 wmsbp.binloc = " " + ip-binloc no-lock no-error.
             
    /* if not the first bin - then what? */
    if avail wmsbp then do:
      /* message "|" + ip-binloc + "|". pause. */
      if wmsbp.prod ne ip-shipprod then
        message "Bin Error - Product".
      else
        message "Bin Error - Check Bin".
       ip-actioncode = "BinErr".
    end.
    else do:
      /* message "|" + ip-binloc + "|". pause. */
      message "Bin Error - Invalid".
      ip-actioncode = "BinErr".
    end.
end.
else do:
  if (ix-t-put.binloc1 = "" or ix-t-put.binloc1 = "New Part") then do:
    /* assign new bin if valid bin */
    if can-find(first wmsb where wmsb.cono = g-cono + 500 and
                        wmsb.whse = v-whse and 
                        wmsb.binloc = ip-binloc and
                        wmsb.priority = 0 and 
                        wmsb.statuscode = "" no-lock)
    or can-find(first wmsbp where wmsbp.cono = g-cono + 500 and
                        wmsbp.whse = v-whse and
                        wmsbp.binloc = ip-binloc and
                        wmsbp.prod = t-put.prod /*ip-shipprod*/ no-lock)
    then do:
      updBinFlag = yes. /* message "uBFlag = yes". pause. */
      ip-actioncode = "good3".
    end.
    else
    if can-find(first wmsb where wmsb.cono = g-cono + 500 and
                        wmsb.whse = v-whse and 
                        wmsb.binloc = " " + ip-binloc and
                        wmsb.priority = 0 and 
                        wmsb.statuscode = "" no-lock)
    or can-find(first wmsbp where wmsbp.cono = g-cono + 500 and
                        wmsbp.whse = v-whse and
                        wmsbp.binloc = " " + ip-binloc and
                        wmsbp.prod = t-put.prod /* ip-shipprod */ no-lock)
    then do:
      updBinFlag = yes. /* message "uBFlag=yes". pause. */
      ip-binloc = " " + ip-binloc.
      ip-actioncode = "good4".
    end.
    else do:
      assign ip-actioncode = "BinErr".
      message "err ix" ix-t-put.binloc1 "ip" ip-binloc. pause.
      
      if can-find(first wmsb where wmsb.cono = g-cono + 500 and
                        wmsb.whse = v-whse and 
                        wmsb.binloc = " " + ip-binloc  no-lock) or
         can-find(first wmsb where wmsb.cono = g-cono + 500 and
                        wmsb.whse = v-whse and 
                        wmsb.binloc = ip-binloc no-lock) then do:
        
        if can-find(first wmsb where wmsb.cono = g-cono + 500 and
                        wmsb.whse = v-whse and 
                        wmsb.binloc = " " + ip-binloc  no-lock) then
         assign ip-binloc = " " + ip-binloc.
       
        message "Invalid Empty Bin" wmsb.binloc.
        bell.
      end.
      else do:                      
        message "Invalid Bin".
        bell.
      end.
    end.
  end.
  else do:
    if ip-binloc = "PROD" then do:
      ip-actioncode = "Product".
    end.
    else do:
      /* this is not right - why didn't you hit F7 or scan another bin */
      assign ip-actioncode = "BinErr".
      message "Invalid Bin-Rescan".
      bell.
    end.
  end.
end.
/* if rptFlag then ip-actioncode = "PrintReport". */
end procedure.

procedure vendor_special_information:
define input parameter inVendNo like apsv.vendno no-undo.
define input parameter inField as char no-undo.   /* Product */
define input-output parameter ioScanProd as char no-undo.

def var ediPartner like apsv.edipartner no-undo.
def buffer sv-apsv for apsv.

find first apsv where apsv.cono = g-cono and apsv.vendno = inVendNo no-lock 
no-error.

if avail apsv then do:
  ediPartner = apsv.edipartner.
  
if inVendNo <> 0 then do:
  find sv-apsv where 
       sv-apsv.cono = g-cono and
       sv-apsv.vendno = inVendNo no-lock no-error.
end.
else do:
  find sv-apsv where 
       sv-apsv.cono = g-cono and
       sv-apsv.edipartner = ediPartner no-lock no-error.
end.

if avail sv-apsv then do:

  dec(ioScanProd) no-error.
  if error-status:error then do: 
    /* only integers can be upc numbers */
  end.
else do:
    find icsv use-index k-produpc where 
         icsv.cono = g-cono and 
         icsv.vendno = sv-apsv.vendno 
         icsv.section3 = sv-apsv.vendno and
         icsv.section4 = dec(ioScanProd) and
         no-lock no-error.
         
    if avail icsv then do:
      message "found icsv". pause.
      assign ioScanProd = icsv.prod.   
      return.
    end.
   end.  
end.
/* ipx-field = product */
/* UPC MOD */


  find first zsastz where zsastz.cono = g-cono and zsastz.labelfl = false and
    zsastz.codeiden = "VendorChars" and
    zsastz.primarykey = string(inVendNo) no-lock no-error.
  
  if avail zsastz then do:     

   def var new-fieldaval like ioScanProd. 
  
     assign new-fieldaval = ioScanProd. 
        /*  message "new-fieldaval" new-fieldaval.   */
  
  
    if inField = "Product" and 
       substring(ioScanProd,1,1) = substring(zsastz.codeval[7],1,1) then do:

      /*  message "found 7". pause.   */
    
      if ioScanProd begins zsastz.codeval[7] then do:
        ioScanProd = substring(ioScanProd, length(zsastz.codeval[7]) + 1,
          length(ioScanProd) - length(zsastz.codeval[7])).       
      
           /* message "the7" ioScanProd. pause. */ 
      end.           
  end.
  
  else
  
  /*
  message "checkign"  substring(ioScanProd,1,1)
                      substring(zsastz.codeval[8],1,2). pause.
  message "length" length(zsastz.codeval[8]) + 1   
          "senlen" length(ioScanProd) - length(zsastz.codeval[8]). pause.
  */                    
                      
  if substring(ioScanProd,1,2) = substring(zsastz.codeval[8],1,2) then 

/*  if ioScanProd begins substring(zsastz.codeval[8],1,2) then  */

    assign 
    ioScanProd = substring(ioScanProd, length(zsastz.codeval[8]) + 1,
                    length(ioScanProd) - length(zsastz.codeval[8])).       
 
        /*   message "ioScanProd #8" ioScanProd. pause.    */
        
        find first icsp where icsp.cono = g-cono and
                              icsp.prod = ioScanProd
                              no-lock no-error.
                              
        if avail icsp then 
            message "". 
            /*
            message "found it". pause.   
            */
                              
        if not avail icsp then do:
            /*  message "notfound". pause.   */
           assign ioScanProd =  new-fieldaval. 
            /*  message "new" new-fieldaval. pause.  */ 
           
          /* message "running3" inField. pause.     */
        assign ioScanProd = new-fieldaval. 
         /* message "after assign" ioScanProd. pause.  */

        
  if substring(ioScanProd,1,1) = substring(zsastz.codeval[3],1,1) then
     /* message "at 3??". pause.   */
     if ioScanProd begins zsastz.codeval[3] then 
        ioScanProd = substring(ioScanProd, length(zsastz.codeval[3]) + 1,
          length(ioScanProd) - length(zsastz.codeval[3])).       
 
        /*   message "ioScanProd #3" ioScanProd. pause.       */
         
        
 end. /* else do */
        
                
 end.               
                 
 else
 if not avail(zsastz) and inField = "Product" then return. 
 
end. 
end procedure.
  

Procedure RFLoop: 
def var zx-x as int intial 1 no-undo.
assign zx-l = no
       zx-x = 1.
repeat while program-name(zx-x) <> ?:         
  if program-name(zx-x) = "hher.p" then do:                                    
    zx-l = yes.                              
    leave.                             
  end.                                      
  assign zx-x = zx-x + 1.                     
end.                                          
end procedure.


