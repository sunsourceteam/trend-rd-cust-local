z-wtnoline = string(g-wtno,">>>>>>9") + "-" + string(g-wtsuf,"99") + " "
           + string(wtelo.lineno,">>9") + " " 
           + string(wtelo.seqno,">>9").

if avail wtelo then
  do:
  find wtel where wtel.cono = g-cono and
                  wtel.wtno = g-wtno and
                  wtel.wtsuf = g-wtsuf  and
                  wtel.lineno = wtelo.lineno no-lock no-error.
  if avail wtel then 
     assign z-shipprod = wtel.shipprod.
  else
     assign z-shipprod = "". 
  
  assign z-orderno =  wtelo.orderaltno
         z-ordersuf = wtelo.orderaltsuf
         z-lineno   = wtelo.linealtno
         z-seqno    = wtelo.seqaltno.
  end.       
else
  do:
  assign z-orderno =  0
         z-ordersuf = 0
         z-lineno   = 0
         z-seqno    = 0
         z-shipprod = "".
  end.       



display 
  z-wtnoline
  z-shipprod
  z-orderno
  z-ordersuf
  z-lineno
  z-seqno
  with frame f-wttiel.

