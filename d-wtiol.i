/* d-wtiol.i 1.2 10/15/98 */
/*h*****************************************************************************
  INCLUDE      : d-wtiol.i
  DESCRIPTION  : Display line for wtiol.p
  USED ONCE?   : yes
  AUTHOR       :
  DATE WRITTEN :
  CHANGES MADE :
    04/27/93 rs;  TB#  8737 Don't show unapproved shipped info
    05/24/93 mms; TB#  7305 Show lost business on canceled line
    07/06/93 mms; TB# 12016 Move canceled line display to right of nonstockty
    03/07/97 frb; TB#  7241 Special price costing
    10/13/98 kjb; TB#  9731 Add a second nonstock description.
*******************************************************************************/

/*tb 7241 03/07/97 frb; Find special cost variables */
{icss.gfi   &prod           = wtel.shipprod
            &icspecrecno    = wtel.icspecrecno
            &lock           = "no"}

if wtel.nonstockty ne  "n" then
    {w-icsp.i wtel.shipprod no-lock}

/*tb 10307 03/08/93 rhl; Show that line cancelled */
/*tb 7305 05/24/93 mms; Show cancel line as lost business */
/*tb 7241 03/07/97 frb; Assign and use special cost variables */
/*tb 9731 10/13/98 kjb; Added proddesc2 for second nonstock description */

   find first wtelo use-index k-wtelo where wtelo.cono = g-cono and
                                            wtelo.wtno = wtel.wtno and
                                            wtelo.wtsuf = wtel.wtsuf and
                                            wtelo.lineno = wtel.lineno
                                            no-lock no-error.
        if avail wtelo and wtelo.ordertype = "o" then do:
           find first oeeh use-index k-oeeh where oeeh.cono = g-cono and
                                            oeeh.orderno = wtelo.orderaltno and
                                            oeeh.ordersuf = wtelo.orderaltsuf
                                            no-lock no-error.
                                                  
                 if avail oeeh then do:
                    /* message "avail oeeh" oeeh.orderno. pause.  */
                    assign v-tieappr = oeeh.approvty.                         
                 end.
        end.  /* type "o"  */
                
        if avail wtelo and wtelo.ordertype = "f" then do:
           find first vaeh use-index k-vaeh where vaeh.cono = g-cono and
                                            vaeh.vano = wtelo.orderaltno and
                                            vaeh.vasuf = wtelo.orderaltsuf
                                            no-lock no-error.

           find vaes use-index k-vaes where vaes.cono = g-cono and
                                            vaes.vano = wtelo.orderaltno and
                                            vaes.vasuf = wtelo.orderaltsuf and
                                            vaes.seqno = wtelo.seqaltno
                                            no-lock no-error.
                                            
                if avail vaes then do:
                   /* message "avail vaes" vaes.vano. pause.  */
                   assign v-tieappr = vaeh.approvty.
                end.
         end.   /* type "f" */
    

assign
    {speccost.gas   &com            = "/*"
                    &prccostperlbl  = "s-speccost"}
    s-commentfl   = if wtel.commentfl = yes then "c" else ""
    s-lineno      = wtel.lineno
    s-nonstockty  = wtel.nonstockty
    s-cancelty    = if wtel.statustype = "c" and wtel.transtype = "do"
                        then "l"
                    else ""
    s-prodinrcvfl = wtel.prodinrcvfl
    s-prod        = wtel.shipprod
    s-notesfl     = if wtel.nonstockty <> "n" then
                        (if avail icsp then icsp.notesfl else "")
                    else ""
    s-qtyord      = wtel.qtyord
    s-unit        = wtel.unit
    s-approvety   = wtel.approvety
    s-prodcost    = wtel.prodcost
    s-netamt      = wtel.netamt
    s-proddesc    = if wtel.nonstockty = "n" then
                         wtel.proddesc   + " " + wtel.proddesc2
                    else (if avail icsp then
                             icsp.descrip[1] + " " + icsp.descrip[2]
                          else "").

display
    s-commentfl
    s-lineno
    s-nonstockty
    s-cancelty
    s-prod
    s-prodinrcvfl
    s-notesfl
    s-qtyord
    s-unit
    s-approvety
    v-tieappr
    s-proddesc
    s-netamt   when g-seecostfl
    s-prodcost when g-seecostfl
    s-speccost when g-seecostfl
with frame f-wtiol.

/*tb 11038 04/27/93 rs; Product change to do not reorder */
if s-approvety = "n" then
   display "Not Approved" @ s-netamt
     with frame f-wtiol.

