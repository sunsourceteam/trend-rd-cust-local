/* SX 55 */
/* poeiupd.lpr 1.1 11/25/98 */
/*h****************************************************************************
  INCLUDE      : poeiupd.lpr
  DESCRIPTION  : PO Receiving - F10 Update - Check For Exceptions
  USED ONCE?   : no
  AUTHOR       : mtt
  DATE WRITTEN : 11/25/98
  CHANGES 1MADE :
    11/27/98 mtt; TB# 24049 PV: Common code use for GUI interface
    12/21/99 bpa; TB# e3278 Checking Serial assign flag (v-icsnpofl) even for
        lots
    11/12/01 des; TB# e10655 Allow auto receiving from JM.  Need additional
        parameter to allow for search on specific PO from JM - add &criteria
    10/01/02 kjb; TB# e14641 Add the ability to have more than four addons on
        a PO.  Need to distribute additional addons across the PO lines.
    10/24/02 kjb; TB# e14641 Changes to fix code review issues
    10/25/02 mwb; TB# e8651 Cores Project; Added po/rm allocation edits. Forces
        them to the line detail screen if not fully allocated.
    01/14/03 mwb; TB# e8651 Cores Project: Rolled code to 3.2.040.
    02/20/02 des; TB# e9918 Correct PO Receiving Errors
    04/22/03 cm;  TB# e16878 Cores - Allow implied core ratio
    05/09/03 mwb; TB# e8651 Code clean-up for app server call and state-less
        app server - require error include.
    06/19/03 mwb; TB# e17414 Fixed 'no icsp' error for RM Cores when processing
        a 'dirty core' return. There is no implied line. Rolled to 3.2.100.
******************************************************************************/
{nxtrend.gpp}

assign
     g-pono      = 0
     g-posuf     = 0
     {&confirm}  = no
     v-nosnlots  = 0
     dQtyChange  = 0
     dCrctTotQty = 0
     iCrctSerLot = 0.

/*d Read through each POEI record attached to this journal */

/* Need the records "broken" to allow for correction edits */
for each poei use-index k-lineno where
         poei.cono   = g-cono and
         poei.jrnlno = {&jrnlno}
        {&criteria}
no-lock
break by poei.cono
      by poei.jrnlno
      by poei.pono
      by poei.posuf:

    {&confirm} = yes.

    /*d Check for exceptions that prevent the PO from being processed */
    if first-of(poei.pono) or first-of(poei.posuf) then do for poeh:

        {w-poeh.i poei.pono poei.posuf no-lock}

        assign
             g-pono        = poei.pono
             g-posuf       = poei.posuf

             &IF "{&NXTREND-SYS}" = "POWERVUE":u &THEN
             cTransType    = if avail poeh then poeh.transtype
                             else ""
             dVendNo       = if avail poeh then poeh.vendno
                             else 0
             &ENDIF

             v-nosnlots    = v-nosnlots + poeh.nosnlots
             iCrctSerLot   = 0
             dCrctTotQty   = 0
             lCorrectionFl = if avail poeh and poeh.jrnlno ne poei.jrnlno
                                then true
                             else false.

        /* Separate edits for corrections */
        if lCorrectionFl = false then do:

            /*d SN/Lots must be fully allocated based on ICAO setting */
            if v-nosnlots > 0 then do:

                {&message} ("Serial #/Lots Not Fully Assigned for PO# " +
                            string(g-pono) + "-" + string(g-posuf), true).
                {&comui}

                bell.
                /{&comui}* */

                {&next}

            end.

            /*d Manually capitalized discounts must be fully capitalized */
            else
            if v-pocapfl = yes and v-pocapdiscfl = yes and
                v-powodist = "m" and
                (poeh.wodiscnet ne poeh.wodiscdist)
            then do:

                {&message} ("Whole Order Discount Not Balanced for PO# " +
                            string(g-pono) + "-" + string(g-posuf), true).
                {&comui}

                bell.
                /{&comui}* */

                {&next}

            end.

            /*d Manually capitalized addons must be fully capitalized */
            /*tb e14641 10/01/02 kjb; Restructure the check for unapplied manual
                addons to include additional addons */
            else
            if v-pocapfl = yes and v-pocapaddfl = yes and
            ( (s-poadddist[1] = "m":u and poeh.addonnet[1] ne poeh.addondist[1])
                       or
              (s-poadddist[2] = "m":u and poeh.addonnet[2] ne poeh.addondist[2])
                or
              (s-poadddist[3] = "m":u and poeh.addonnet[3] ne poeh.addondist[3])
                or
              (s-poadddist[4] = "m":u and poeh.addonnet[4] ne poeh.addondist[4])
                or
              (can-find (first addon where
                               addon.cono       = poeh.cono  and
                               addon.ordertype  = "po":u     and
                               addon.orderno    = poeh.pono  and
                               addon.ordersuf   = poeh.posuf and
                               addon.seqno      = 0          and
                               addon.addondistr = "m":u      and
                               addon.addonnet  <> addon.addondist)))
            then do:

                {&message} ("Addons Not Balanced for PO# " +
                            string(g-pono) + "-" + string(g-posuf), true).
                {&comui}

                bell.
                /{&comui}* */

                {&next}

            end.

            /*d Make sure whole order discount is < order total */
            /*tb 9943 03/18/93 rhl; Allowed to post negative cost amount */
            /*tb 13254 10/06/93 tdd; Added wodisctype check */
            else
            if poeh.wodisctype        and
                poeh.wodiscnet > (poeh.totrcvamt +
                                  poeh.addonnet[1] + poeh.addonnet[2] +
                                  poeh.addonnet[3] + poeh.addonnet[4])
            then do:

                {&message} ("Whole Order Discount Greater Than Received" +
                 " for PO# " + string(g-pono) + "-" + string(g-posuf), true).
                {&comui}

                bell.
                /{&comui}* */

                {&next}

            end.

            /*tb 20914 07/17/96 tdd; Do not allow F10 update if the
                total quantity received on a PO is 0 */
            else
            if poeh.totqtyrcv = 0 then do:

                {&message} ("Total Quantity Received is Zero for PO# " +
                            string(g-pono) + "-" + string(g-posuf), true).
                {&comui}

                bell.
                /{&comui}* */

                {&next}

            end. /*if poeh.totqtyrcv = 0 */

        end. /* lCorrectionFl = false */

    end. /* if first-of(poei.pono) or first-of (poei.posuf) */


    /*d Edit for bundle tally lines with no OEELM records.  This situation
        can arise when we have copied a bundle tally line without the OEELM
        records and have not updated the detail in POET.*/
    if lCorrectionFl    =  false        and
        poei.nonstockty ne "n":u        and
        can-find(first icsp where
                    icsp.cono           = g-cono        and
                    icsp.prod           = poei.shipprod and
                    icsp.kittype        = "m":u         and
                    icsp.reqbundleidfl  = yes no-lock)  and
        not can-find(first oeelm where
                    oeelm.cono          = g-cono        and
                    oeelm.ordertype     = "p":u         and
                    oeelm.orderno       = poei.pono     and
                    oeelm.ordersuf      = poei.posuf    and
                    oeelm.lineno        = poei.lineno   and
                    oeelm.statustype    ne "d":u no-lock)
    then do:

        {&message} "No Bundle Tally Detail for " +
                       string(g-pono) + "-" + string(g-posuf,"99") +
                       " Ln# " + string(poei.lineno).

        {&next}

    end. /*if lCorrectionFl */



    /* Edit for Fully Allocated Cores - Per Line - Not Per Order */
    &IF "{&NXTREND-SYS}" = "POWERVUE":u &then
    if  cTransType = "rm":u and
        ({icspcore.gvl &prod = poei.shipprod} or
         {icspremn.gvl &prod = poei.shipprod}) and
        lCorrectionFl = false
    then do:
        assign
            cCoreMode    = if {icspcore.gvl &prod = poei.shipprod}
                                then "c":u
                           else "r":u
            dNoCoreAlloc = 0
            iImplyQty    = 1.

        /* Get implied core quantity ratio */
        if cCoreMode = "r":u then
            run coreimplyqty.p(input  g-cono,
                               input  "p":u,
                               input  poei.pono,
                               input  poei.posuf,
                               input  poei.lineno,
                               input  poei.shipprod,
                               output iImplyQty).

        run Validate-PO-Core-Allocation in business-object("PO Entry Trans":u)
            (input poei.pono,
             input poei.posuf,
             input poei.lineno,
             input dVendno,
             input "p":u,
             input cCoreMode,
             input poei.shipprod,
             input poei.whse,
             output dNoCoreAlloc,
             input-output cCustomParam)
        no-error.

        {server/appservererror.i &client = "no"
                                 &comvar = "/*"
                                 &call   = "Validate PO Core Allocation"}

        /* Convert allocated quantity back to line quantity */
        dNoCoreAlloc = dNoCoreAlloc / iImplyQty.

        if poei.stkqtyrcv - dNoCoreAlloc ne 0
        then do:

            {&message} ("Return Core Not Fully Allocated for PO# " +
                        string(g-pono) + "-" + string(g-posuf,"99") +
                        " Ln# " + string(poei.lineno) , true).
            {&next}

        end.  /* proof edit */

    end. /* Return */

    else

    &ENDIF

    /* Calc serials and lots and total lines for the PO's that are corrections.
        Then check these values for each PO */
    if lCorrectionFl = true then do:
        /* Get the quantity change for calculations */
        do for poel:
            {w-poel.i poei.pono poei.posuf poei.lineno no-lock}

            assign
                dQtyChange = if avail poel then
                               poei.qtyrcv - poel.qtyrcv
                             else 0
                dCrctTotQty = dCrctTotQty + absolute(dQtyChange)
                {&crctconfirm}
                .
        end. /* do for poel */

        /* If the quantity received decreases, and thus requires an RM, make
                sure that the corrections return reason is set up */
        if dQtyChange < 0 and
           {&pocrctreason}
        then do:

            {&message} ("Correction Return Reason Not Setup for Qty Decrease " +
                   "For PO# " + string(g-pono) + "-" + string(g-posuf), true).
            {&comui}
            bell.
            /{&comui}* */

            {&next}

        end. /* dQtyChange < 0 and (not avail sasc or (avail sasc and ... */

        /* Calculate if any serials or lots need allocation or de-allocation */
        iCrctSerLot = iCrctSerLot + poei.nosnlots.

        /* For corrections, once we have evaluated all lines being corrected,
            process any errors */
        if last-of(poei.pono) or last-of(poei.posuf) then do:
            if dCrctTotQty = 0 then do:
                {&message} ("Total Quantity Received is Zero for PO# " +
                            string(g-pono) + "-" + string(g-posuf), true).
                {&comui}
                bell.
                /{&comui}* */

                {&next}

            end. /* dQtyChange = 0 */

            if iCrctSerLot ne 0 then do:
                {&message} ("Crct Serial #/Lots Not Fully Assigned for PO# " +
                          string(g-pono) + "-" + string(g-posuf,"99":u), true).
                {&comui}
                bell.
                /{&comui}* */

                {&next}

            end. /* iCrctSerLot ne 0 */

        end. /* last-of (poei.pono) or last-of(poei.posuf) */
          
    end. /* lCorrection = true */

end. /* for each poei */







