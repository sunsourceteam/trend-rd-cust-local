/* zkpsqsq.i 1.1 02/06/98 */
/*h****************************************************************************
  PROCEDURE    : zkpsqsq.i                        
  DESCRIPTION  : KIT PRODUCTION SCHEDULE  - Resequence orders
  AUTHOR       : sd
  DATE WRITTEN : 05/16/00 (#206890)
  CHANGES MADE :
                  kpet.user6 contains seq# for production schedule
******************************************************************************/
/*IF CHANGES ARE MADE TO THIS PROGRAM YOU MUST ALSO CHANGE zkpsqsq.i used
  in zkprp */

/****************************************************************************
Put in kprp.gva 06-30-00 klt

def shared var g-cono         like vaeh.cono                          no-undo.
def shared var p-reseqfl      as l                                    no-undo.
def shared var p-reseqby      as i                                    no-undo.
def        var v-nextseq      as i                                    no-undo.
def temp-table t-kp no-undo
   field neworold    as char format "x(1)"
   field schedseq    as int
   field promisedt   like kpet.enterdt
   field whse        like icsd.whse           /*klt*/
   field wono        like kpet.wono
   field wosuf       like kpet.wosuf
   field stagecd     like kpet.stagecd
index k-new as primary unique
    whse        ascending   /*klt*/
    stagecd     ascending
    neworold    ascending
    schedseq    ascending
    promisedt   ascending
    wono        ascending
    wosuf       ascending
index k-seq 
    whse        ascending   /*klt*/
    stagecd     ascending
    schedseq    ascending.
    
Put in kprp.gva 06-30-00 klt    
***************************************************************************/

put screen row 22 column 2
"Resequencing kit production orders for the production schedule ... ".

sequence:
for each kpet use-index k-kpet no-lock where
   kpet.cono       = g-cono     and
   kpet.stagecd    < 3:

      if kpet.ordertype = "o" then
      {w-oeeh.i kpet.orderaltno kpet.orderaltsuf no-lock}
      else if kpet.ordertype = "t" then
      {w-wteh.i kpet.orderaltno kpet.orderaltsuf no-lock}        

      {zkprpdt.gsa}

      create t-kp.

                   /* 1=already has seq#, reassign 1st
                      2=new va order not yet sequenced, reassign 2nd */
      assign
         t-kp.neworold  = if ((int(kpet.user6)) = 0 or 
                             (substring(kpet.user1,1,4) = "" and 
                              kpet.stagecd le 1 ))                             then "2" else "1"
         t-kp.schedseq  = if  (substring(kpet.user1,1,4) = "" and 
                              kpet.stagecd le 1 ) then
                            0
                          else  
                            int(kpet.user6)
         t-kp.whse      = kpet.whse               /*klt*/
         t-kp.stagecd   = kpet.stagecd
         t-kp.promisedt = s-promisedth
            /*
            if avail wteh then wteh.duedt else     
                          if avail oeeh then oeeh.promisedt else 
                          kpet.enterdt                           
            */
         t-kp.wono      = kpet.wono
         t-kp.wosuf     = kpet.wosuf.
 
end.  /* for each vaeh */

/* KLT ADDED TO SELECT WAREHOUSE *** WORKING ON *****/
whse-sequence:
for each icsd use-index k-icsd where
    icsd.cono  = g-cono and
    icsd.whse >= b-whse and
    icsd.whse <= e-whse
  no-lock:
  
    v-nextseq = 0.                      /*initialize for each warehouse*/
    
    if  p-whselstfl then
    do:
        find first t-whselist use-index k-whselist where
            t-whselist.whse = icsd.whse
           no-lock no-error.
                               
            if not avail t-whselist then next whse-sequence.
    end.

    find last t-kp use-index k-seq where
        t-kp.whse    =  icsd.whse and
        t-kp.stagecd = 2
       no-lock no-error.
       
        assign 
            v-nextseq = if avail t-kp then t-kp.schedseq + 1
                        else p-reseqby.
   /*
    message t-kp.wono t-kp.wosuf t-kp.stagecd t-kp.neworold t-kp.schedseq
            t-kp.whse.
    pause.
   */

    resequence:
    for each t-kp use-index k-new where
        t-kp.whse    = icsd.whse and
        t-kp.stagecd = 1
       no-lock:

  /*
        put screen row 23 column 2
        "Processing KP Order # " +
        string(t-kp.wono,"zzzzzz9") + "-" +
        string(t-kp.wosuf,"99").
  */      
        {kpet.gfi t-kp.wono t-kp.wosuf "exclusive"}
          
        assign
            kpet.user6 = v-nextseq
            v-nextseq  = v-nextseq + p-reseqby.
                      
    end. /* for each temp-table record */
       
    
end. /*whse-sequence*/       


put screen row 22 column 2  
"                                                                      ".
put screen row 23 column 2
"                                                                      ".


