/* SX55 */
/****************************************************************************
  PROCEDURE      : insa.p
  DESCRIPTION    : Daily transaction extract for SX4.0
  AUTHOR         : lsl
  DATE WRITTEN   : 06/11/01
  CHANGES MADE   :
    06/13/01 lsl; fix SX.i 4.0 extracts to include older data streams
    10/15/02 lsl; TB# e14200 added run insaex41.p for option15 = "41".
    08/11/03 lsl; TB# e18409 Added insaex-oe41.p to extract oeel record line
            counts.
    01/19/04 mm;  TB# e19316 Add SX.i 4.2 version option to report parameters.
        Remove old code for SX.i version prior to 3.2
    03/15/04 mm;  TB# e19809 Complete rewrite of DWhs logic
    12/29/05 ds;  TB# e23575 Fixed problem with kits doubling amounts
*****************************************************************************/
{in.gva "new shared"}
{insa.lva "new shared"}
{p-rptbeg.i}

def                    var c-errormsg  as c format "x(80)"             no-undo.

/******************* LOAD PARAMETERS FROM SAPB ******************************/
assign
    v-datein    = sapb.rangebeg[1].
{p-rptdt.i}
assign
     b-postdt    = v-dateout
     v-datein    = sapb.rangeend[1].
{p-rptdt.i}

assign
    e-postdt     = v-dateout
    b-cono       = integer(substring(sapb.rangebeg[2],1,4))
    e-cono       = integer(substring(sapb.rangeend[2],1,4))
    p-dwver     = if sapb.optvalue[1] = ? then v-dwdefver
                  else                                      /* SX55 */
                  if sapb.optvalue[1] = "30" then "32"      /* SX55 */
                  else sapb.optvalue[1]
    p-extractty  = sapb.optvalue[2]
    p-suffix     = sapb.optvalue[3]
    p-casety     = sapb.optvalue[4]
    p-book       = if sapb.optvalue[5] = "yes":u then yes else no
    p-backlog    = if sapb.optvalue[6] = "yes":u then yes else no
    p-ordertype  = if sapb.optvalue[7] = "yes":u then yes else no
    p-cost       = sapb.optvalue[8]
    p-oeentity   = sapb.optvalue[9]
    p-invoice    = if sapb.optvalue[10] = "yes":u then yes else no
    p-sales      = if sapb.optvalue[11] = "yes":u then yes else no
    p-dimgrp1    = sapb.optvalue[12]
    p-dimgrp2    = sapb.optvalue[13]
    p-dimgrp3    = sapb.optvalue[14]
    p-kitline    = sapb.optvalue[15]
    p-headroll   = if sapb.optvalue[15] = "b":u then sapb.optvalue[16] else ""
    o-cono       = g-cono
    c-errormsg   = ?
    v-1start     = 1
    v-2start     = 1
    v-3start     = 1.

{inverchk.lcn &extract = "insa"}

if p-dwver <> "" then do:
  /* This is only here to support DWhs level 3.2 extracts and should be
     removed once this level is no longer supported */
  if v-dwlevel = 1 then
    run insaex3.p.
  else
    run insaex.p.
end.

if c-errormsg <> ? then
   message "Option Entry Errors:" c-errormsg view-as alert-box.

/*d Reassign g-cono to get it back to the original
     company the process is being run from. */
g-cono = o-cono.