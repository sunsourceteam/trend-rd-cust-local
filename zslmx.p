
/* zslmx.p 1.1 05/15/00 */
/*h*****************************************************************************
  PROCEDURE      : zslmx.p
  DESCRIPTION    : Kit Production Orders Sequencing Screen
  AUTHOR         : sd
  DATE WRITTEN   : 05/15/00
  CHANGES MADE   :
*******************************************************************************/
{g-xxex.i "new shared"}
{zslmx.lva &new = "new"}
{valmsg.gas}

/*d Set title for line frame, and number of iterations in line frame */
assign
    v-msg   =
"  Item Number              Item Description            Price     Cost    Type"
    v-length =   14
    v-scrollfl = yes.

/*d Define display forms */
{zslmx.lfo}

/* Create temp table */
/* {zslmx.lcr}       */             


/************************* Main Processing *********************************/
begin:
do while true on endkey undo begin, leave begin:
   update g-imptype 
          g-slupdtno
   with frame f-zslmxh.
   if {k-cancel.i} or {k-jump.i} then 
    do:
       leave begin.
    end.
  do:
   find slsi where slsi.imptype = input g-imptype
             no-lock no-error.
        if avail slsi then 
           do:
             assign  s-impdesc = slsi.impdescrip.
             display s-impdesc with frame f-zslmxh.
           end.
        else    
           do: 
             run err.p(4790).
             apply "entry" to g-imptype.
             next begin. 
           end.
   find sleh where sleh.cono = g-cono and 
                   sleh.statustype = yes and 
                   sleh.imptype    = g-imptype and 
                   sleh.slupdtno   = g-slupdtno
                   no-lock no-error.
        if not avail sleh then            
           do:
           run err.p(4074).
                   apply lastkey.
                   apply "entry" to g-slupdtno.  
                   next begin.
           end.
  assign o-imptype  = input g-imptype
         o-slupdtno = input g-slupdtno.
  end.

main:
 do while true on endkey undo main, leave main:      
    pause 0 before-hide.                            
    color display input v-msg with frame f-zslmxb.   
    display v-msg with frame f-zslmxb.               
    s-prctype:auto-return in frame f-zslmxl = true.
  if {k-jump.i} or {k-cancel.i} then
       do: 
          clear frame f-zslmxl all no-pause.  
          leave main.  
       end.
    hide frame f-zslmxl no-pause.                    
    clear frame f-zslmxl all no-pause.               
    assign g-lkupfl = yes.
  repeat: 
    /*o Process XXEX screen */  
       {x-xxex.i    &file         = "sled"              
                    &frame        = "f-zslmxl"             
                    &find         = "zslmx.lfi"             
                    &field        = "s-itemnbr"  
              /*      &go-on        = "f1 "      */
                    &display      = "zslmx.ldi"            
                    &edit         = "zslmx.led"}          
  end. /* repeat */
  leave main.                                        
 end.  /*e dwt (main) */                                
end. /*  end begin */

hide message no-pause.                                 
hide frame f-zslmxl no-pause.                           
hide frame f-zslmxb no-pause.                           

/*  {j-all.i "frame f-zslmxb" "frame f-zslmxh"}  */
