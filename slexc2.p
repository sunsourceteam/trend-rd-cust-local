/***************************************************************************
 PROGRAM NAME: slexc2.p
  PROJECT NO.: 
  DESCRIPTION: This program will create a file for mass price updates and 
               maintenance. Slexx.p is the maintenance program that will 
               give the capability to change the file created from slexc.

  DATE WRITTEN: 03/30/01.
  AUTHOR      : SunSource
  MODIFIED: 
***************************************************************************/
def var v-error as char format "x(70)"  no-undo.
def var v-error2 as char format "x(25)" no-undo.
def var v-errcnt  as i no-undo.
def var v-errno   as i extent 10 no-undo.

{p-rptbeg.i}
{sl.gva}
{slafb.lva new}
{slafb.lfo}   

assign v-imptype    = sapb.optvalue[1]
       g-slupdtno   = sapb.optvalue[3].

main:
do for slsi:
 {slsi.gfi v-imptype no}
 if not avail slsi then 
    do: 
      run err.p(4790).
    end.
 else       
    v-impdescrip = slsi.impdescrip.
end.
      
{slcheck.lpr &com = "/*"}      

for each sled where sled.cono       = g-cono and 
                    sled.statustype = yes    and 
                    sled.imptype    = v-imptype and  
                    sled.slupdtno   = g-slupdtno:
  assign sled.whse = " ".
  find icsp where icsp.cono = g-cono and
                  icsp.prod = sled.prod and
                  icsp.statustype ne "i" and
                  icsp.statustype ne "s"
                  no-lock no-error.
  if not avail icsp then
    do:
    find icsc where icsc.catalog = sled.prod and
                    icsc.vendno  = sled.vendno
                    no-lock no-error.
    if avail icsc then
      assign sled.pricetype = icsc.pricetype.
    next.
  end.
  find first icsw where 
             icsw.cono        = g-cono      and 
             icsw.prod        = icsp.prod   and
             icsw.whse        > "AZZZ"      and
      substr(icsw.whse,1,1)   <> "C"   and
             icsw.arpvendno   = sled.vendno and
             icsw.arptype     = "V"  and
             icsw.statustype  <> "X"
             no-lock no-error.
 if avail icsw then
   do:
   assign sled.pricetype = icsw.pricetype.  
   next.
 end.
 else
   do:
   find icsc where icsc.catalog = sled.prod and
                   icsc.vendno  = sled.vendno
                   no-lock no-error.
   if avail icsc then
     assign sled.pricetype = icsc.pricetype. 
   next.
 end.
end. /* each sled */                


