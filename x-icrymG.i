/* TBXR F6 CodeGen End of Includes that are CodeGen Generated  */ 
&if defined(user_optiontype) = 2 &then
if not
can-do("R,W",substring(p-optiontype,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries R,W "
     substring(p-optiontype,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries R,W "
     substring(p-optiontype,v-inx,1).
&endif
 
&if defined(user_registertype) = 2 &then
if not
can-do("R,W",substring(p-register,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries R,W "
     substring(p-register,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries R,W "
     substring(p-register,v-inx,1).
&endif
 
&if defined(user_DWvarheaders1) = 2 &then
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_exportvarheaders1) = 2 &then
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_exportvarheaders2) = 2 &then
if v-lookup[v-inx4] = "R" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_loadtokens) = 2 &then
if v-lookup[v-inx] = "R" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(whsetemp.region,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "W" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(whsetemp.whse,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "X" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(recid(whsetemp),"999999999999")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"999999999999").   
  end.                                              
else                                              
  assign v-token = " ".
&endif
 
&if defined(user_totaladd) = 2 &then
  assign t-amounts[1] = whsetemp.ordprt.
  assign t-amounts[2] = whsetemp.ordnotsd.
  assign t-amounts[3] = whsetemp.shiperr.
  assign t-amounts[4] = whsetemp.invvalue.
  assign t-amounts[5] = whsetemp.gainloss.
  assign t-amounts[6] = whsetemp.invcnt.
  assign t-amounts[7] = whsetemp.invcorr.
  assign t-amounts[8] = 1.
  assign t-amounts[9] = 0.
  assign t-amounts[10] = 0.
  assign t-amounts[11] = 0.
  assign t-amounts[12] = whsetemp.goal2.
  assign t-amounts[13] = 0.
  assign t-amounts[14] = whsetemp.goal3.
  assign t-amounts[15] = 0.
  assign t-amounts[16] = whsetemp.goal4.
  assign t-amounts[17] = whsetemp.stg2amt.
  assign t-amounts[18] = whsetemp.stg2cnt.
&endif
 
&if defined(user_numbertotals) = 2 &then
  if v-subtotalup[v-inx4] = 1 then
    v-val = t-amounts1[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 2 then
    v-val = t-amounts2[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 3 then
    v-val = t-amounts3[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 4 then
    v-val = t-amounts4[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 5 then
    v-val = t-amounts5[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 6 then
    v-val = t-amounts6[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 7 then
    v-val = t-amounts7[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 8 then
    v-val = t-amounts8[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 9 then
    v-val = 
      if t-amounts1[v-inx4] = 0 then
        0
      else
      ((t-amounts1[v-inx4] - t-amounts2[v-inx4])
       / t-amounts1[v-inx4]) * 100.
  else 
  if v-subtotalup[v-inx4] = 10 then
    v-val = 
      if t-amounts8[v-inx4] = 0 then
      0
      else
      t-amounts10[v-inx4] / t-amounts8[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 11 then
    v-val = 
      if t-amounts1[v-inx4] = 0 then
       0
      else
       ((t-amounts1[v-inx4] - t-amounts3[v-inx4])
       / t-amounts1[v-inx4]) * 100.
  else 
  if v-subtotalup[v-inx4] = 12 then
    v-val = t-amounts12[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 13 then
    v-val = 
      if t-amounts6[v-inx4] = 0 then
        0
      else
        ((t-amounts6[v-inx4] - t-amounts7[v-inx4])
       / t-amounts6[v-inx4]) * 100.
  else 
  if v-subtotalup[v-inx4] = 14 then
    v-val = t-amounts14[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 15 then
    v-val = 
      if t-amounts4[v-inx4] = 0 then
        0
      else
        ((t-amounts4[v-inx4] + t-amounts5[v-inx4])
       / t-amounts4[v-inx4]) * 100.
  else 
  if v-subtotalup[v-inx4] = 16 then
    v-val = t-amounts16[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 17 then
    v-val = t-amounts17[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 18 then
    v-val = t-amounts18[v-inx4].
  else 
    v-val = t-amounts1[v-inx4].
  do inz = 1 to 18:
  if inz = 1 then
    v-regval[inz] = t-amounts1[v-inx4].
  else 
  if inz = 2 then
    v-regval[inz] = t-amounts2[v-inx4].
  else 
  if inz = 3 then
    v-regval[inz] = t-amounts3[v-inx4].
  else 
  if inz = 4 then
    v-regval[inz] = t-amounts4[v-inx4].
  else 
  if inz = 5 then
    v-regval[inz] = t-amounts5[v-inx4].
  else 
  if inz = 6 then
    v-regval[inz] = t-amounts6[v-inx4].
  else 
  if inz = 7 then
    v-regval[inz] = t-amounts7[v-inx4].
  else 
  if inz = 8 then
    v-regval[inz] = t-amounts8[v-inx4].
  else 
  if inz = 9 then
    v-regval[inz] = 
      if t-amounts1[v-inx4] = 0 then
        0
      else
      ((t-amounts1[v-inx4] - t-amounts2[v-inx4])
       / t-amounts1[v-inx4]) * 100.
  else 
  if inz = 10 then
    v-regval[inz] = 
      if t-amounts8[v-inx4] = 0 then
      0
      else
      t-amounts10[v-inx4] / t-amounts8[v-inx4].
  else 
  if inz = 11 then
    v-regval[inz] = 
      if t-amounts1[v-inx4] = 0 then
       0
      else
       ((t-amounts1[v-inx4] - t-amounts3[v-inx4])
       / t-amounts1[v-inx4]) * 100.
  else 
  if inz = 12 then
    v-regval[inz] = t-amounts12[v-inx4].
  else 
  if inz = 13 then
    v-regval[inz] = 
      if t-amounts6[v-inx4] = 0 then
        0
      else
        ((t-amounts6[v-inx4] - t-amounts7[v-inx4])
       / t-amounts6[v-inx4]) * 100.
  else 
  if inz = 14 then
    v-regval[inz] = t-amounts14[v-inx4].
  else 
  if inz = 15 then
    v-regval[inz] = 
      if t-amounts4[v-inx4] = 0 then
        0
      else
        ((t-amounts4[v-inx4] + t-amounts5[v-inx4])
       / t-amounts4[v-inx4]) * 100.
  else 
  if inz = 16 then
    v-regval[inz] = t-amounts16[v-inx4].
  else 
  if inz = 17 then
    v-regval[inz] = t-amounts17[v-inx4].
  else 
  if inz = 18 then
    v-regval[inz] = t-amounts18[v-inx4].
  else 
    assign inz = inz.
  end.
&endif
 
&if defined(user_summaryloadprint) = 2 &then
  if v-lookup[v-inx2] = "R" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Region - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "W" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Warehouse - " + h-lookup + " " + h-genlit.
    end.
  else
      v-summary-lit = v-lookup[v-inx2].
&endif
 
&if defined(user_summaryloadexport) = 2 &then
  if v-inx4 > v-inx2  then 
    do:
    if v-inx4 > 1 then          
      export_rec = export_rec + v-del. 
    assign export_rec = export_rec +  " ".
    if v-cells[v-inx4] = "2" then
      assign export_rec = export_rec +  v-del + " ".
    end. 
  else 
  if v-lookup[v-inx4] = "R" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "W" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
    do:
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    assign export_rec = export_rec + v-lookup[v-inx4].
    end.
&endif
 
/* TBXR F6 CodeGen End of Includes that are CodeGen Generated  */ 
