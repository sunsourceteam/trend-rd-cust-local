&if defined(user_temptable) = 2 &then
def var p-detailprt     as logical          no-undo.
/* RANGES */
def     var b-buyer         as c      format "x(4)"           no-undo.
def     var e-buyer         as c      format "x(4)"           no-undo.
def     var b-vendno        as dec    format "999999999999"   no-undo.
def     var e-vendno        as dec    format "999999999999"   no-undo.
def     var b-region        as c      format "x(4)"           no-undo.
def     var e-region        as c      format "x(4)"           no-undo.
def     var b-enterdt       as date   format "99/99/99"       no-undo.
def     var e-enterdt       as date   format "99/99/99"       no-undo.
def     var p-format        as int                            no-undo.
def     var p-regval        as c                              no-undo.
def     var o-sort          as c      format "x(1)"           no-undo.
def     var v-weekdays      as  int                           no-undo.     
def     var v-idate         as date   format "99/99/99"       no-undo.
define {&sharetype} temp-table porzatemp no-undo
field buyer       as c   format     "x(4)"
field name        as c   format     "x(20)"
field vend        as dec format     ">>>>>>>>>>>9"
field region      as c   format     "x(4)"
field pono        like   poeh.pono
field tot-po      as i   format     ">>>9"        initial 0
field tot-line    as i   format     ">>>9"        initial 0  
field tot-ack     as i   format     ">>>9"        initial 0
field tot-per     as dec format     ">99.99"      initial 0
field un3         as i   format     ">>>9"        initial 0
field un4-10      as i   format     ">>>9"        initial 0
field un11-20     as i   format     ">>>9"        initial 0
field ov21        as i   format     ">>>9"        initial 0
index k-buyer
      buyer
index k-region
      region.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_drpttable) = 2 &then
/* Use to add fields to the report table */
   field porzatempid as recid
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_forms) = 2 &then
/* FORM HEADER */
form 
  "Buyer"                 at 1
  "Name"                  at 7
  "#PO's"                 at 28
  "#Lines"                at 34
  "#Ack'ed"               at 41 
  "Percent"               at 49
/*  "#Unack'ed <=3 Days"      at 68     */
  "#Unack'ed 4-10 Days"     at 88
  "#Unack'ed 11-20 Days"    at 109
  "#Unack'ed 21+ Days"      at 132  
  "-----"                 at 1
  "--------------------"  at 7
  "-----"                 at 28
  "------"                at 34
  "-------"               at 41
  "------------------"    at 49
  "-------------------"   at 68
  "--------------------"  at 88                       
  "------------------"    at 109
  "------------------"    at 128
  with frame porzatemp-hdr no-underline no-box no-labels page-top down width 178. 
form 
  porzatemp.buyer        at 1 
  porzatemp.name         at 7
  porzatemp.tot-po       at 28
  porzatemp.tot-line     at 34
  porzatemp.tot-ack      at 41
  porzatemp.tot-per      at 49
  "%"                    at 57
/*  porzatemp.un3          at 68    */
  porzatemp.un4-10       at 88
  porzatemp.un11-20      at 109
  porzatemp.ov21         at 132
  with frame porzatemp-dtl no-underline no-box no-labels down width 178.
/* 
form 
  skip(1)
  skip(1)
  "Total All Regions" at 1
  var-total-po        at 28
  var-total-line      at 34
  var-total-ack       at 41
  var-total-per       at 48 
  "%"                 at 55
  with frame porzatemp-totals no-underline no-box no-labels down width 178. 
*/
/*   
form
  "Subtotals"         at 1
  subcountpo          at 28
  subcountline        at 34
  subcountack         at 41
  subcountper         at 48
  with frame f-subtotals no-underline no-box no-labels down width 178.  
*/  
  
form
  skip(1)
  "Region: "         at 1
  porzatemp.region       at 9
  with frame f-region no-underline no-box no-labels down width 178. 
  
 
form
v-final                 at 1
v-astrik                at 2        format "x(2)"
v-summary-lit2          at 4        format "x(27)"
"~015"
v-amt1                  at 28       format     ">>>9"        
v-amt2                  at 34       format     ">>>9"        
v-amt3                  at 41       format     ">>>9"         
v-amt4                  at 49       format     ">99.99"
"%"                     at 57
"~015"                  at 178
skip(1)
with frame f-tot width 178  no-box no-labels.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_extractrun) = 2 &then
 run sapb_vars.
 run extract_data.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_exportstatheaders) = 2 &then
  /* This is the headers for the information you want to export
     the control break headers are taken care of this is just for 
     the totals you want printed */
  
 
     put stream expt unformatted " " +
     v-del + v-del + v-del + v-del + v-del + v-del + v-del +
     v-del
     v-del
     v-del
     v-del
     /*  v-del  */
     chr(13).
 
 
     assign export_rec = 
                         export_rec         +
                         "Buyer"                 + v-del +
                         "Name"                  + v-del +
                         "#PO's"                 + v-del +
                         "#Lines"                + v-del +
                         "#Ack'ed"               + v-del +
                         "Percent"               + v-del +
                         "#Unack'ed > 3 Days"    + v-del +
                         "#Unack'ed 4-10 Days"   + v-del +
                         "#Unack'ed 11-20 Days"  + v-del +
                         "#Unack'ed 21+ Days".
                          
           if p-detailprt then 
              assign export_rec = export_rec + v-del.            
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_foreach) = 2 &then
for each porzatemp no-lock:
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_drptassign) = 2 &then
 /* If you add fields to the dprt table this is where you assign them */
  drpt.porzatempid = recid(porzatemp)
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_viewheadframes) = 2 &then
/* Since frames are unique to each program you need to put your
   views and hides here with your names */
if not p-exportl then
  do:
  if not x-stream then
    do:
    hide frame porzatemp-hdr.
    end.
  else
   do:
    hide stream xpcd frame porzatemp-hdr.
    end.
  
  end.
if not p-exportl  then
  do:
  if not x-stream then
    do:
    page.
    view frame porzatemp-hdr.
    end.
  else  
    do:
    page stream xpcd.
    view stream xpcd frame porzatemp-hdr.
    end.
   
  end.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_drptloop) = 2 &then
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_detailprint) = 2 &then
   /* Comment this out if you are not using detail 'X' option  */
   if p-detailprt and 
      entry(u-entitys,drpt.sortmystery,v-delim) <> 
      fill("~~",length(entry(u-entitys,drpt.sortmystery,v-delim))) then 
     do:
     /* this code skips the all other lines for printing detail */
     /* next line would be your detail print procedure */
     if p-exportl then 
      run print-dtl-export(input drpt.porzatempid).
     else 
      run print-dtl(input drpt.porzatempid).    
     end.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_finalprint) = 2 &then
if not p-exportl then
 do:
  assign v-per1 = 
       if t-amounts3[u-entitys + 1] = 0 then
         0
       else
       (t-amounts3[u-entitys + 1] / t-amounts2[u-entitys + 1]) * 100.
       
 if not x-stream then 
   do:
   display 
    v-final
    v-astrik
    v-summary-lit2
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts2[u-entitys + 1]   @ v-amt2    
    t-amounts3[u-entitys + 1]   @ v-amt3
    v-per1                      @ v-amt4
    with frame f-tot.
    down with frame f-tot.
   end.
 else
   do:
   display stream xpcd
    v-final
    v-astrik
    v-summary-lit2
    t-amounts1[u-entitys + 1]   @ v-amt1
    t-amounts2[u-entitys + 1]   @ v-amt2    
    t-amounts3[u-entitys + 1]   @ v-amt3 
    v-per1                      @ v-amt4   
    with frame f-tot.
    down stream xpcd with frame f-tot.
   end.
      
      
 end.
else
  do:
  assign export_rec =  v-astrik.
  do v-inx4 = 1 to u-entitys:
    if v-totalup[v-inx4] = "n" then
       next.
    if v-inx4 = 1 then
      v-summary-lit2 = "Final".
    else
      v-summary-lit2 = "".
    if v-cells[v-inx4]= "2" then
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec + v-summary-lit2 + v-del + " ".
     
      end.
    else
      do:
      if v-inx4 > 0 then 
        export_rec = export_rec + v-del.
      assign export_rec = export_rec +  v-summary-lit2.
      end.
  end.
   
    assign v-per1 = 
       if t-amounts3[u-entitys + 1] = 0 then
         0
       else
       (t-amounts3[u-entitys + 1] / t-amounts2[u-entitys + 1]) * 100.
     
    assign export_rec = export_rec + v-del +
      string(t-amounts1[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts2[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts3[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(t-amounts4[u-entitys + 1],"->>>>>>>>9") + v-del +
      string(v-per1, "->99.99"). 
    
  assign export_rec = export_rec + chr(13).    
  put stream expt unformatted export_rec.
  end.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_summaryframeprint) = 2 &then
   assign v-per1 = 
       if t-amounts3[v-inx2] = 0 then
         0
       else
       (t-amounts3[v-inx2] / t-amounts2[v-inx2]) * 100.
  
    if not x-stream then
      do:
      display 
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        t-amounts3[v-inx2]   @ v-amt3
        v-per1               @ v-amt4   
        with frame f-tot.
      down with frame f-tot.
      end.
    else
      do:
      display stream xpcd
        v-final
        v-astrik
        v-summary-lit2
        t-amounts1[v-inx2]   @ v-amt1
        t-amounts2[v-inx2]   @ v-amt2
        t-amounts3[v-inx2]   @ v-amt3
        v-per1               @ v-amt4
        with frame f-tot.
      down stream xpcd with frame f-tot.
      end.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_summaryputexport) = 2 &then
   assign v-per1 = 
       if t-amounts3[u-entitys + 1] = 0 then
         0
       else
       (t-amounts3[u-entitys + 1] / t-amounts2[u-entitys + 1]) * 100.
 
 
 
    assign export_rec = v-astrik + v-del + export_rec.
    assign export_rec = export_rec + v-del +
      string(t-amounts1[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts2[v-inx2],"->>>>>>>>9") + v-del +
      string(t-amounts3[v-inx2],"->>>>>>>>9") + v-del +
      string(v-per1,"->99.99").
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
&if defined(user_procedures) = 2 &then
procedure sapb_vars:
  assign b-buyer  = sapb.rangebeg[1]
         e-buyer  = sapb.rangeend[1]
         b-vendno = dec(sapb.rangebeg[2])
         e-vendno = dec(sapb.rangeend[2])
         b-region = sapb.rangebeg[3]
         e-region = sapb.rangeend[3]
         b-enterdt = date(sapb.rangebeg[4])
         e-enterdt = date(sapb.rangeend[4])
       /*  o-sort  = sapb.optvalue[1].  */
         p-format  = int(sapb.optvalue[1])
         p-export  = sapb.optvalue[2].
      assign p-detailprt = yes
             p-detail    = "D"
             p-portrait  = no.
             
             
  run formatoptions.
end.
/* ------------------------------------------------------------------------- */
procedure formatoptions:
/* ------------------------------------------------------------------------- */
   
   
  assign p-portrait = false.
  /* if false then the report will print in LANDSCAPE 178 character */
  
  if sapb.optvalue[1] > "0" and sapb.optvalue[1] ne "99" then 
    do:
    p-optiontype = " ".
    find notes where notes.cono = g-cono and
                     notes.notestype = "zz" and
                     notes.primarykey = "porzatemp" and  /* tbxr program */
                     notes.secondarykey = string(p-format) no-lock no-error.
    if not avail notes then
      do:
      display "Format is not valid cannot process request".
      assign p-optiontype = "r"
             p-sorttype   = ">,"
             p-totaltype  = "l"
             p-summcounts = "a".
      return.
      end.               
   
    assign p-optiontype = notes.noteln[1]
           p-sorttype   = notes.noteln[2]
           p-totaltype  = notes.noteln[3]
           p-summcounts = notes.noteln[4]
           p-export     = "    "
           p-register   = notes.noteln[5]
           p-registerex = notes.noteln[6].
    end.     
  else
  if sapb.optvalue[1] = "99" then 
    do:
    assign p-register   = ""
           p-registerex = "".
    run reportopts(input sapb.user5, 
                   input-output p-optiontype,
                   input-output p-sorttype,  
                   input-output p-totaltype,
                   input-output p-summcounts,
                   input-output p-register,
                   input-output p-regval).
    end.
 end.
   
 
procedure extract_data: 
for each poeh where poeh.cono       = g-cono and    
                    poeh.transtype <> "rm" and
                    poeh.stagecd    = 2 and
                   (poeh.buyer     >= b-buyer and
                    poeh.buyer     <= e-buyer) and
                   (poeh.vendno    >= b-vendno and
                    poeh.vendno    <= e-vendno)
                    no-lock:
  find oimsp where oimsp.person = poeh.buyer and
                  (oimsp.responsible >= b-region and
                   oimsp.responsible <= e-region)
                   no-lock no-error.
                         
    if not avail oimsp then next.
           
    for each poel where poel.cono        = poeh.cono and 
                        poel.pono        = poeh.pono and
                        poel.posuf       = poeh.posuf and
                        poel.nonstockty <> "R" and
                        substr(poel.user1,1,1) <> "x" and
                        poel.enterdt <= e-enterdt
                        no-lock:
    
     
     
find porzatemp where porzatemp.buyer = poeh.buyer no-error. 
    
 if avail porzatemp then do:
    /* message "avail porzatemp" porzatemp.buyer.   */
    /* message "poel.pono" poel.pono.   */
     if (poel.enterdt >= b-enterdt and poel.enterdt <= e-enterdt) then
     do:
       if porzatemp.pono <> poeh.pono then
          assign porzatemp.tot-po = porzatemp.tot-po + 1
                 porzatemp.pono   = poeh.pono.
        
            
          run countdays.p (input poel.user9,    
                           input poel.enterdt,
                           output v-weekdays).
         
         if v-weekdays <= 3 then do:
   
          if substr(poel.user1,1,1) = "a" or substr(poel.user1,1,1) = "s" then                assign porzatemp.tot-ack = porzatemp.tot-ack + 1.  
             /* message "ackcount " poel.pono.  */
           end. /* <= 3 */
            
            
            assign porzatemp.tot-line = porzatemp.tot-line + 1     
                   porzatemp.tot-per  = (porzatemp.tot-ack /                   
                   porzatemp.tot-line) * 100.    
                   porzatemp.un3      = porzatemp.tot-line - porzatemp.tot-ack.
       end. /* between dates */
       
       if (substr(poel.user1,1,1) = " " or 
           substr(poel.user1,1,1) = "n" or
           substr(poel.user1,1,1) = "r") then 
       do:   
           
              
           run countdays.p (input today, 
                            input poel.enterdt,                            output v-weekdays).  
             
             if v-weekdays >= 21 then do:
                assign porzatemp.ov21 = porzatemp.ov21 + 1.         
                /* message " >=21 " poel.pono.      */
             end.
               
               
             if v-weekdays >= 11 and v-weekdays <= 20 then do:  
                assign porzatemp.un11-20 = porzatemp.un11-20 + 1.   
                /* message " >11<20" poel.pono.     */
             end.
             if v-weekdays >= 4 and v-weekdays <= 10 then do:   
                assign porzatemp.un4-10 = porzatemp.un4-10 + 1. 
                  /* message ">4<10" poel.pono.         */
             end.
             
            end. /* if substr(poel.user1,1,1) */
 
 
 end.     /* if avail porzatemp  */         
        
 else if not avail porzatemp then do:   
 
        create porzatemp.
          assign porzatemp.buyer       = poeh.buyer
                 porzatemp.name        = oimsp.name
                 porzatemp.region      = oimsp.responsible
                 porzatemp.pono        = poeh.pono
                 porzatemp.tot-po      = 0
                 porzatemp.tot-line    = 0
                 porzatemp.tot-ack     = 0
                 porzatemp.tot-per     = 0.
         
         run countdays.p (input poel.user9,    
                          input poel.enterdt,
                          output v-weekdays).
         
       
       if (poel.enterdt >= b-enterdt and poel.enterdt <= e-enterdt) then
       do:
         
         if v-weekdays <= 3 then do:
   
          if substr(poel.user1,1,1) = "a" or substr(poel.user1,1,1) = "s" then                 assign porzatemp.tot-ack = porzatemp.tot-ack + 1.     
              /* message "ackcount2" poel.pono. */
            
         end. /* <= 3 */
            
            
            assign porzatemp.tot-line = porzatemp.tot-line + 1     
                   porzatemp.tot-per  = (porzatemp.tot-ack /                   
                   porzatemp.tot-line) * 100.    
                   porzatemp.un3      = porzatemp.tot-line - porzatemp.tot-ack.
      
       end. /* between dates */
       
       if (substr(poel.user1,1,1) = " " or 
           substr(poel.user1,1,1) = "n" or
           substr(poel.user1,1,1) = "r") then 
       do:   
           
              
           run countdays.p (input today, 
                            input poel.enterdt,
                            output v-weekdays).  
             
             if v-weekdays >= 21 then do:
                assign porzatemp.ov21 = porzatemp.ov21 + 1.         
                /* message " >=21 " poel.pono.  */
             end.
               
               
             if v-weekdays >= 11 and v-weekdays <= 20 then do:  
                assign porzatemp.un11-20 = porzatemp.un11-20 + 1.   
                /* message " >11<20" poel.pono.     */
             end.
             if v-weekdays >= 4 and v-weekdays <= 10 then do:  
                assign porzatemp.un4-10 = porzatemp.un4-10 + 1. 
                  /* message ">4<10" poel.pono.         */
             end.
             
       end. /* if substr(poel.user1,1,1) */
        end.     /* else create               */
    end.         /* for each poel             */
end.             /* for each poeh             */    
end.  /* extract data */        
        
/* ------------------------------------------------------------------------- */
procedure print-dtl:
/* ------------------------------------------------------------------------- */
define input parameter ip-recid as recid no-undo.
find porzatemp where recid(porzatemp) = ip-recid no-lock no-error.
    display porzatemp.buyer
            porzatemp.name 
            porzatemp.tot-po
            porzatemp.tot-line
            porzatemp.tot-ack 
            porzatemp.tot-per
        /*  porzatemp.un3   */
            porzatemp.un4-10 
            porzatemp.un11-20 
            porzatemp.ov21
            with frame porzatemp-dtl.
    down with frame porzatemp-dtl.
end. /* procedure print-dtl */  
  
 
 
/* ------------------------------------------------------------------------- */
procedure print-dtl-export:
/* ------------------------------------------------------------------------- */
define input parameter ip-recid as recid no-undo.
find porzatemp where recid(porzatemp) = ip-recid no-lock no-error.
            
  put stream expt unformatted
            "   "
            v-del
            porzatemp.buyer 
            v-del
            porzatemp.name  
            v-del
            porzatemp.tot-po    
            v-del
            porzatemp.tot-line  
            v-del
            porzatemp.tot-ack   
            v-del
            porzatemp.tot-per   
            v-del
            porzatemp.un3   
            v-del
            porzatemp.un4-10    
            v-del
            porzatemp.un11-20   
            v-del
            porzatemp.ov21  
            chr(13).
end. /* procedure print-dtl */  
  
 
 
  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
&endif
 
