/****************************************************************************
*                                                                           *
* f-pdxm.i - General slsrec Margin Report Forms                             *
*                                                                           *
****************************************************************************/


form 
"Reg/Dist"              at 1
"Slsrep#"               at 11
"Name"                  at 20
"Sales"                 at 7      
"Oemarg $"              at 16 
"Oemarg %"              at 25
"Glmarg $"              at 36
"Glmarg %"              at 45
"Sales"                 at 60
"Oemarg $"              at 70 
"Oemarg %"              at 79
"Glmarg $"              at 90
"Glmarg %"              at 99
"Sales"                 at 114
"Oemarg $"              at 122 
"Oemarg %"              at 131
"Glmarg $"              at 143
"Glmarg %"              at 152
"<-------------------"  at 1
x-mo-1                  at 22
"------------------->"  at 33
"<-------------------"  at 57
x-mo-1ytd               at 78
"------------------>"   at 88
"<---------------"      at 108
x-mo-2                  at 126
"To Date "              at 133
"---------------->"     at 143
with frame f-header no-underline no-box no-labels page-top down width 220.

form
/*
"-----------------------------------------------------------------------------------------------------------------------------------------------------------" 
                        at 5
*/
skip(1)
s-rgn                   at 2
s-custno                at 11
s-name                  at 20
with frame f-ss-head no-underline no-box no-labels down width 220.

form
skip(1)
s-rgn                   at 2
s-custno                at 11
s-name                  at 20
s-mo-isales             at 2
s-mo-imarg              at 15
s-mo-ipct               at 27
s-mo-glmarg             at 35
s-mo-glpct              at 47
s-b4-isales             at 55
s-b4-imarg              at 68
s-b4-ipct               at 80
s-b4-glmarg             at 88
s-b4-glpct              at 100
s-ytd-isales            at 109
s-ytd-imarg             at 121
s-ytd-ipct              at 133
s-ytd-glmarg            at 142
s-ytd-glpct             at 154
with frame f-detail no-underline no-box no-labels down width 220.

form
skip (1)
"Totals for District:"  at 1
r-rgn                   at 22
d-mo-tisales            at 2
d-mo-timarg             at 15
d-mo-tipct              at 27
d-mo-tglmarg            at 35
d-mo-tglpct             at 47
d-b4-tisales            at 55
d-b4-timarg             at 68
d-b4-tipct              at 80
d-b4-tglmarg            at 88
d-b4-tglpct             at 100
d-ytd-tisales           at 109
d-ytd-timarg            at 121
d-ytd-tipct             at 133
d-ytd-tglmarg           at 142
d-ytd-tglpct            at 154
with frame f-district no-underline no-box no-labels down width 220.

form                           
skip (1)                       
"Totals for Region: "   at 1   
r-lit                   at 21 
r-rgn                   at 33  
r-mo-tisales            at 2
r-mo-timarg             at 15
r-mo-tipct              at 27
r-mo-tglmarg            at 35
r-mo-tglpct             at 47
r-b4-tisales            at 55
r-b4-timarg             at 68
r-b4-tipct              at 80
r-b4-tglmarg            at 88
r-b4-tglpct             at 100
r-ytd-tisales           at 109
r-ytd-timarg            at 121
r-ytd-tipct             at 133
r-ytd-tglmarg           at 142
r-ytd-tglpct            at 154
with frame f-region no-underline no-box no-labels down width 220.

form
 skip (1)
"Grand Totals:"         at 1 
g-mo-tisales            at 2
g-mo-timarg             at 15
g-mo-tipct              at 27
g-mo-tglmarg            at 35
g-mo-tglpct             at 47
g-b4-tisales            at 55
g-b4-timarg             at 68
g-b4-tipct              at 80
g-b4-tglmarg            at 88
g-b4-tglpct             at 100
g-ytd-tisales           at 108
g-ytd-timarg            at 121
g-ytd-tipct             at 133
g-ytd-tglmarg           at 142
g-ytd-tglpct            at 154
with frame f-grand no-underline no-box no-labels down width 220.

form
"Cust Nbr"              at 5   
"Name"                  at 20 
"----------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
                        at 5
with frame f-cd-header no-underline no-box no-labels page-top width 220.

form
"Order Nbr"             at 10
"Invoice Date"          at 21
"Total Line Amt"         at 34
with frame f-od-header no-underline no-box no-labels page-top width 220.

form
"Line No"               at 7
"Product"               at 17
"Qtyship"               at 47
"Oecost"                at 61
"GLcost"                at 76
"Cst%Var"               at 83
"Lnprice"               at 96
"Oemarg"                at 113
"Glmarg"                at 128   
"Oemarg %"              at 136
"Glmarg %"              at 146
"Shipfm"                at 155
"Tiedord"               at 162
"Typ"                   at 170
"Flg"                   at 174
with frame f-ld-header no-underline no-box no-labels page-top width 220.

form 
s-lineno                at 11
custord.blanksort       at 14
s-prod                  at 16
s-qtyship               at 43 
s-stndcost              at 54
s-glcost                at 69
glhld-var               at 83   format "zz9.99-"
s-lnprice               at 91
s-oemarg                at 106
s-gllnmarg              at 121
glhld-1pct              at 138  format "zz9.99-"
glhld-2pct              at 147  format "zz9.99-"
oeel.whse               at 155
s-orderaltno            at 162
oeel.ordertype          at 170
s-user5                 at 174
with frame f-lndetail no-underline no-box no-labels down width 220.

form
cs-custno               at 2 
cs-name                 at 20
with frame f-cc-head no-underline no-box no-labels width 220.

form 
cs-order                at 10
cs-date                 at 21
cs-total                at 36
with frame f-oo-head no-underline no-box no-labels width 220.

form
cs-mo-isales            at 2
cs-mo-imarg             at 15
cs-mo-ipct              at 27
cs-mo-glmarg            at 35
cs-mo-glpct             at 47
cs-b4-isales            at 55
cs-b4-imarg             at 68
cs-b4-ipct              at 80
cs-b4-glmarg            at 88
cs-b4-glpct             at 100
cs-ytd-isales           at 109
cs-ytd-imarg            at 121
cs-ytd-ipct             at 133
cs-ytd-glmarg           at 142
cs-ytd-glpct            at 154
with frame f-dd-detail no-underline no-box no-labels down width 220.

form
"---------------------------------------------------------" at 55
with frame f-asterisk no-box no-labels no-underline width 220.

form 
w-errmsg                at 5 
with frame f-error no-labels no-box.
 
form 
 skip (1)
with frame f-spaces no-box no-labels no-underline width 132.
       
