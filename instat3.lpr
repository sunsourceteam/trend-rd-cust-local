/* instat3.lpr 1.1 01/03/98 */
/* instat3.lpr 1.3 05/12/94 */
/*h****************************************************************************
  INCLUDE      : instat3.lpr
  DESCRIPTION  : Static Extract - Customer/ShipTo Gateway
  USED ONCE?   :
  AUTHOR       : drm
  DATE WRITTEN : 04/24/97
  CHANGES MADE :
    04/24/97 drm; TB# 23488 Created new from p-instat.i for SX3.0.
    07/18/97 jkp; TB# 23488 Changed name of p-instat3.i to instat3.lpr for MS 
        DOS version. Did a Rename in Aide-de-camp.
    08/16/97 jkp; TB# 23605 Added parameter &case to pass the case selected.
    11/06/97 jkp; TB# 23938 Do not allow question marks to be output.
    07/09/99 jkp; TB# 26421 Changed customer extract to extract state instead
        of statecd.  The statecd field is the taxing state, while the state
        field is the customers state.
    08/19/99 jkp; TB# 24005 Round/Trim all fields larger than the data
        warehouse field back to the data warehouse size.  This is due to SQL 
        7.0 tight typing.
******************************************************************************/

/*o All fields must be put out as character fields.  So if a field is numeric
    or is a date, it must use the "string" parameter to make it a character 
    field.  For those fields that truly are a character field, i.e., they 
    don't need the "string" parameter, they must have parameter 4 in front 
    of them to get them in the correct case.  In addition, all signed fields
    must have the sign on the left.  It is imperative that no question marks
    are output, so every field must be checked to make sure it is not a
    question mark, even though that would be corrupt data except for a date
    field.  This is because the analyzer process cannot handle question marks
    and it is SQL code, so it cannot even give a graceful error - it just
    aborts.  Some of these checks are made in instatx3.p and some are made 
    here. */

/* Passing parameters definition *********************************************
   {&type} Type of gateway being produced:
           i = invoices
           b = bookings
           a = backlog
   {&buffoeel} Buffer prefix for oeel can be either b- or b2-
   {&buffoeeh} Buffer prefix for oeeh can be either b- or b2-
   {&case}     Selection of the case sensitivity.  This is passed by a group
               of if then else statements and is set to either "caps", "lc", 
               or " " depending on how the user answered the p-casety option 
               on the SASSR.
                    
   Sample include calls might be:
       {insaex3.lpr 
           &type     = "b" 
           &buffoeel = "b-" 
           &buffoeeh = "b-" 
           &case     = "caps"}     
           (Will create a bookings gateway, using b-oeel and b-oeeh and will
            output all character fields in all upper case letters)
       {insaex3.lpr 
           &type     = "i" 
           &buffoeel = "b2-" 
           &buffoeeh = "b2-" 
           &case     = "lc"}
           (Will create an invoices gateway, using b2-oeel and b2-oeeh and 
            will output all character fields in all lower case letters)
******************************************************************************/

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Customer ID */
if {&file}.custno <> ? then
    string({&file}.custno)
else string(v-zero)
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Customer Parent ID (for later use) */
if {&file}.custno <> ? then
    string({&file}.custno) 
else string(v-zero)
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Ship To ID */
if v-shipto <> ? then
    {&case}(v-shipto)
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Customer Name */
if {&file}.name <> ? then
    {&case}({&file}.name)
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*tb 24005 08/19/99 jkp; Trimmed addr[2] field to 29 characters to match data
     warehouse. */
/*e Address */
if {&file}.addr[1] <> ? and {&file}.addr[2] <> ? then
    {&case}({&file}.addr[1]) + " " + {&case}(substring({&file}.addr[2],1,29))
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e City */
if {&file}.city <> ? then
    {&case}({&file}.city)
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*tb 26421 07/09/99 jkp; Changed to extract state, not statecd.  The state
     field is the customers state, the statecd field is the taxing state. */
/*e State */
if {&file}.state <> ? then
    {&case}({&file}.state)
else v-null    
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Zip Code */
if {&file}.zipcd <> ? then
    {&case}({&file}.zipcd)
else v-null
v-del

/*e Country */
if "{&file}" = "arss" and {&file}.countrycd = " " and arsc.countrycd <> "" then
  {&case}(arsc.countrycd)
else
if "{&file}" = "arss" and {&file}.countrycd <> " " then
  {&case}({&file}.countrycd)
else
if "{&file}" = "arsc" and arsc.countrycd <> " " then
  {&case}(arsc.countrycd)
else  
{&case}("US")
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Phone Number */
if {&file}.phoneno <> ? then
    {&case}({&file}.phoneno)
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Fax */
if {&file}.faxphoneno <> ? then
    {&case}({&file}.faxphoneno)
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Sales Contact */
if arsc.pocontctnm <> ? then
    {&case}(arsc.pocontctnm)
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Sales Contact Phone */
if arsc.pophoneno <> ? then
    {&case}(arsc.pophoneno)
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Credit Contact */
if arsc.apmgr <> ? then
    {&case}(arsc.apmgr)
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Credit Contact Phone */
if arsc.apphoneno <> ? then
    {&case}(arsc.apphoneno)
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e SIC Code */
if arsc.siccd[1] <> ? then
    string(arsc.siccd[1])
else string(v-zero)
v-del

/*e Map Location */
{&case}(v-null) 
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Territory */
if {&file}.salesterr <> ? then
    {&case}({&file}.salesterr)
else v-null
v-del

/*e Customer Group */
{&case}(v-null) 
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Outside Slsrep */
if {&file}.slsrepout <> ? then
    {&case}({&file}.slsrepout)
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Inside Slsrep */
if {&file}.slsrepin <> ? then
    {&case}({&file}.slsrepin)
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Credit Manager */
if arsc.creditmgr <> ? then
    {&case}(arsc.creditmgr)
else v-null
v-del

/*e Last Sale Date */
string(v-date,"99/99/9999")
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Period 1 Balance */
if {&bal1} <> ? then
    string({&bal1})
else string(v-zero)
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Period 2 Balance */
if {&bal2} <> ? then
    string({&bal2})
else string(v-zero)
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Period 3 Balance */
if {&bal3} <> ? then
    string({&bal3})
else string(v-zero)
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Period 4 Balance */
if {&bal4} <> ? then
    string({&bal4})
else string(v-zero)
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Period 5 Balance */
if {&bal5} <> ? then
    string({&bal5})
else string(v-zero)
v-del

/*e Total Balance 1 to 5 */
string(v-zero)
v-del
        
/*e Total Balance 2 to 5 */
string(v-zero)  
v-del

/*e Total Balance 3 to 5 */
string(v-zero)
v-del

/*e Total Balance 4 to 5 */
string(v-zero) 
v-del
        
/*e Total Balance 5 */
string(v-zero)  
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e User Defined A */
if arsc.user4 <> ? then
    {&case}(substring(arsc.user4,40,30))
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e User Defined B */
if {&file}.user2 <> ? then
    {&case}(substring({&file}.user2,1,30))
else v-null
v-del

/*e Number User Defined A */
string(v-zero)
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Customer Class */
if arsc.class <> ? then
    string(arsc.class,">>>>>>>>>9")
else string(v-zero)
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Customer Type */
if arsc.custtype <> ? then
    {&case}(substring(arsc.custtype,1,4))
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Price Type */
if arsc.pricetype <> ? then
    {&case}(substring(arsc.pricetype,1,4))
else v-null
v-del

/*tb 23938 11/06/97 jkp; Added check for question mark. */
/*e Customer Warehouse ID */
if {&file}.whse <> ? then
    {&case}({&file}.whse)
else v-null
v-del

/*e Security Flag */ 
string(v-zero) 
v-del

/*e SX User 1 */
string({&file}.custno)
v-del

/*e SX User 2 */
{&case}(string(arsc.ediyouraddr,"x(30)"))

v-del

/*e SX User 3 */
(if {&file}.statustype = true then
    "Y "
 else
    "N ")  
v-del

/*e SX User 4 */
{&case}(string(arsc.divno,"999"))
v-del

/*e SX User 5 */

{&case}(x-frtcode)

v-del

/*e SX User 6 */
{&case} (string (x-naisc,"x(6)"))
v-del

/*e SX User 7 */
(if {&file}.enterdt <> ? then
   string({&file}.enterdt,"99/99/9999")
 else
   v-null)  

v-del

/*e SX User 8 */
{&case} (string (x-slsgroup,"x(4)"))
v-del

/*e SX User 9 */
string(v-zero)
v-del

/*e SX User 10 */
string(v-zero)
v-del

/*e Customer E-Mail */
{&case}(v-null)
v-del

/*e Sales Contact E-Mail */
{&case}(v-null) 
v-del

/*e Credit Contact E-Mail */
{&case}(v-null) 

CHR(13) CHR(10).


