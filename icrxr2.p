/***************************************************************************
 PROGRAM NAME: icrxr2.p
  PROJECT NO.: 00-67
  DESCRIPTION: This program produces a  whse receipt performance report
                option "W" from rep00111.p
 DATE WRITTEN: 09/13/00
       AUTHOR: Sir Carl E. Graul Esq.
     MODIFIED: 

***************************************************************************/

/* ########################## TREND INCLUDES ######################### */


/* ########################### DEFINITIONS ########################### */

/* link to trend */
def shared var ln-whse-f        like icsw.whse          no-undo.
def shared var ln-whse-t        like icsw.whse          no-undo.
def shared var ln-receiptdt-f   like wteh.receiptdt     no-undo.
def shared var ln-receiptdt-t   like wteh.receiptdt     no-undo.
def shared var ln-go-to         as char format "x(1)"   no-undo.

DEFINE VARIABLE qbf-i AS INTEGER NO-UNDO.
DEFINE VARIABLE qbf-t AS INTEGER NO-UNDO.
DEFINE VARIABLE qbf-total AS INTEGER NO-UNDO.

/* ############################## LOGIC ############################## */
run "10-initialize".

DO FOR wteh,wtel:
  FORM HEADER
    SPACE(42) "Received WT's" SKIP
    TODAY FORMAT "99/99/99" SKIP
    STRING(qbf-t,"HH:MM:SS") FORMAT "xxxxxxxx" SKIP
    PAGE-NUMBER FORMAT ">>>>9" SKIP(4) 
    WITH FRAME qbf-header PAGE-TOP COLUMN 1 WIDTH 88 
        NO-ATTR-SPACE NO-LABELS NO-BOX.
  VIEW FRAME qbf-header.

END.

FOR EACH wteh WHERE 
        wteh.cono        = 1 
    AND wteh.shiptowhse  >= ln-whse-f 
    AND wteh.shiptowhse  <= ln-whse-t
    and wteh.receiptdt   >= ln-receiptdt-f
    and wteh.receiptdt   <= ln-receiptdt-t
    AND wteh.transtype   = "wt" 
    AND (wteh.stagecd = 5 OR wteh.stagecd = 6) 
    NO-LOCK,
  EACH wtel WHERE 
        (wteh.cono   = wtel.cono 
    and wteh.wtno    = wtel.wtno 
    and wteh.wtsuf   = wtel.wtsuf) 
    AND (wtel.cono  = 1 
    AND wtel.qtyrcv <> 0) 
  NO-LOCK
  BREAK 
    BY wteh.rcvoperinit 
    BY wteh.wtno 
    BY wteh.wtsuf 
    BY wtel.lineno
  qbf-total = 1 TO qbf-total + 1:
  
  FORM
    wteh.shiptowhse COLUMN-LABEL "Whse" 
            FORMAT "x(4)" VIEW-AS FILL-IN
    wteh.wtno COLUMN-LABEL "WT #" 
            FORMAT "zzzzzz9" VIEW-AS FILL-IN
    wteh.wtsuf COLUMN-LABEL "" 
            FORMAT "99" VIEW-AS FILL-IN
    wtel.lineno COLUMN-LABEL "Ln#" 
            FORMAT ">>>" VIEW-AS FILL-IN
    wtel.shipprod COLUMN-LABEL "Product" 
            FORMAT "x(24)" VIEW-AS FILL-IN
    wtel.qtyrcv COLUMN-LABEL "Rcv Qty" 
            FORMAT ">>>>>>9.99" VIEW-AS FILL-IN
    wteh.rcvoperinit COLUMN-LABEL "Rcv Init" 
            FORMAT "x(4)" VIEW-AS FILL-IN
    wteh.receiptdt COLUMN-LABEL "Receipt" 
            FORMAT "99/99/99" VIEW-AS FILL-IN
    wteh.user1 COLUMN-LABEL "Count" 
            FORMAT "x(8)" VIEW-AS FILL-IN
    WITH NO-ATTR-SPACE COLUMN 1 WIDTH 88 NO-BOX NO-VALIDATE.
  DISPLAY
    wteh.shiptowhse
    wteh.wtno
    wteh.wtsuf
    wtel.lineno
    wtel.shipprod
    wtel.qtyrcv
    wteh.rcvoperinit
    wteh.receiptdt
    wteh.user1 (SUB-COUNT BY wteh.rcvoperinit).
END.
PAGE.

/* ############################ EOJ STUFF ############################ */


/* ######################### END OF PROGRAM ########################## */ 
/* ####################### INTERNAL PROCEDURES ####################### */

procedure 10-initialize:
ASSIGN qbf-total = 0
       qbf-t     = TIME.
end procedure. /* 10-initialize */

