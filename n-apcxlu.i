/*n-apcxlu.i  1.1 01/03/98 */
/*n-apcxlu.i  1.2 7/29/93  */
/*n-azczlu.i */
/*tb 7191 07/20/92 mwb; added vendno change */
/*tb 10582 05/05/93 jrg; Copied from AR lookup */

 

if g-ourproc begins "oe" then
  do:
  confirm = true.  
  find {1} w-vend where
      {2}
      w-vend.vendno >= decimal(substring(v-start,1,12))
      /{2}* */
      no-lock no-error.
  
  if avail w-vend then
    do:
    {w-apsv.i w-vend.vendno no-lock}
    if ((avail apsv and can-do("`,',~~",substring(apsv.lookupnm,1,1)))
     or not avail apsv) and ("{1}" = "first" or "{1}" = "next") then
      do:
      scamsearch:
      do while confirm on endkey undo main, leave main
                        on error  undo main, leave main:
        find next w-vend where
         no-lock no-error.
        if not avail w-vend then
          do:
          confirm = false.
          next scamsearch.
          end.
        {w-apsv.i w-vend.vendno no-lock}
        if (avail apsv and can-do("`,',|",substring(apsv.lookupnm,1,1)))
         or not avail apsv then
          next scamsearch.
        else
          confirm = false.
      end. 
      end. 
    else
     if ((avail apsv and can-do("`,',|",substring(apsv.lookupnm,1,1)))
     or not avail apsv) and ("{1}" = "prev" ) then
      do:
      scamsearch:
      do while confirm on endkey undo main, leave main
                        on error  undo main, leave main:
        find prev w-vend where
         no-lock no-error.
        if not avail w-vend then
          do:
          confirm = false.
          next scamsearch.
          end.
        {w-apsv.i w-vend.vendno no-lock}
        if (avail apsv and can-do("`,',|",substring(apsv.lookupnm,1,1)))
         or not avail apsv then
          next scamsearch.
        else
          confirm = false.
      end. 
      end. 
    end.
  if avail w-vend then
    do:
    assign
      s-name         = if avail apsv then apsv.name else v-invalid
      s-notesfl      = if avail apsv then apsv.notesfl else ""
      s-vendno       = w-vend.vendno
      s-statustype   = if avail apsv then
                         if not apsv.statustype then "(I)"
                         else ""
                       else ""
      s-city         = if avail apsv then apsv.city + ", " + apsv.state
                       else "".
    end.
  end.
else
  do: 
  find {1} w-vend where
   {2}
   w-vend.vendno >= decimal(substring(v-start,1,12))
   /{2}* */
   no-lock no-error.

if avail w-vend then do:
  {w-apsv.i w-vend.vendno no-lock}
  assign
    s-name         = if avail apsv then apsv.name else v-invalid
    s-notesfl      = if avail apsv then apsv.notesfl else ""
    s-vendno       = w-vend.vendno
    s-statustype   = if avail apsv then
                       if not apsv.statustype then "(I)"
                       else ""
                     else ""
    s-city         = if avail apsv then apsv.city + ", " + apsv.state
                     else "".
  end.
end.
confirm = false.                     
