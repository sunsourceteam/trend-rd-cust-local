/* TBXR F6 CodeGen End of Includes that are CodeGen Generated  */ 
&if defined(user_optiontype) = 2 &then
if not
can-do("C,E,S,T,W",substring(p-optiontype,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries C,E,S,T,W "
     substring(p-optiontype,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries C,E,S,T,W "
     substring(p-optiontype,v-inx,1).
&endif
 
&if defined(user_registertype) = 2 &then
if not
can-do("C,E,S,T,W",substring(p-register,v-inx,1))
 then 
   if not x-stream then
    display 
   "Invalid option selection valid entries C,E,S,T,W "
     substring(p-register,v-inx,1).
   else 
     display stream xpcd 
    "Invalid option selection valid entries C,E,S,T,W "
     substring(p-register,v-inx,1).
&endif
 
&if defined(user_DWvarheaders1) = 2 &then
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "E" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "EnterDt".
  end. 
else  
if v-lookup[v-inx4] = "S" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "StageCd".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "TakenBy".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Whse".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_exportvarheaders1) = 2 &then
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Cust Name".
  end. 
else  
if v-lookup[v-inx4] = "E" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "EnterDt".
  end. 
else  
if v-lookup[v-inx4] = "S" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "StageCd".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "TakenBy".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "Whse".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_exportvarheaders2) = 2 &then
if v-lookup[v-inx4] = "C" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "E" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "S" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "T" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
if v-lookup[v-inx4] = "W" then
  do:
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + "".
  end. 
else  
  do: 
  if v-inx4 > 0 then 
    export_rec = export_rec + v-del.  
  assign export_rec = export_rec + " ".
  end. 
&endif
 
&if defined(user_loadtokens) = 2 &then
if v-lookup[v-inx] = "C" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.custname,"x(30)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(30)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "E" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.senterdt,"x(12)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(12)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "S" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.stage,"x(3)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(3)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "T" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.Takenby,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "W" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.whse,"x(4)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(4)").   
  end.                                              
else                                              
if v-lookup[v-inx] = "X" then
  do: 
  if v-inx > 1 then 
    assign v-mystery = v-mystery + v-delim  
           v-sortmystery = v-sortmystery + v-delim. 
                                                    
  assign v-token =                                  
    caps(string(repTable.quoteno,"x(12)")). 
  assign v-mystery = v-mystery + v-token.
  if v-sortup[v-inx] = "<" then   
    run ascii_make_it_not (input-output v-token).   
  assign  v-sortmystery = v-sortmystery +           
                          string(v-token,"x(12)").   
  end.                                              
else                                              
  assign v-token = " ".
&endif
 
&if defined(user_totaladd) = 2 &then
  assign t-amounts[1] = repTable.sumPrice.
  assign t-amounts[2] = repTable.sumCost.
  assign t-amounts[3] = repTable.cntConvert.
  assign t-amounts[4] = repTable.cntCancel.
  assign t-amounts[5] = 0.
  assign t-amounts[6] = repTable.cntLine.
  assign t-amounts[7] = repTable.cntOrder.
  assign t-amounts[8] = repTable.QsumPrice.
  assign t-amounts[9] = repTable.QsumCost.
  assign t-amounts[10] = 0.
  assign t-amounts[11] = repTable.CvtSumPrice.
  assign t-amounts[12] = repTable.CvtSumCost.
  assign t-amounts[13] = 0.
  assign t-amounts[14] = repTable.CanSumPrice.
  assign t-amounts[15] = repTable.CanSumCost.
  assign t-amounts[16] = 0.
  assign t-amounts[17] = repTable.OpnSumPrice.
  assign t-amounts[18] = repTable.Opnsumcost.
  assign t-amounts[19] = 0.
&endif
 
&if defined(user_numbertotals) = 2 &then
  if v-subtotalup[v-inx4] = 1 then
    v-val = t-amounts1[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 2 then
    v-val = t-amounts2[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 3 then
    v-val = t-amounts3[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 4 then
    v-val = t-amounts4[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 5 then
    v-val = 
      {x-varxq-fmt1.i}
  else 
  if v-subtotalup[v-inx4] = 6 then
    v-val = t-amounts6[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 7 then
    v-val = t-amounts7[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 8 then
    v-val = t-amounts8[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 9 then
    v-val = t-amounts9[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 10 then
    v-val = 
      {x-varxq-fmt1.i "8" "9"}
  else 
  if v-subtotalup[v-inx4] = 11 then
    v-val = t-amounts11[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 12 then
    v-val = t-amounts12[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 13 then
    v-val = 
      {x-varxq-fmt1.i "11" "12"}
  else 
  if v-subtotalup[v-inx4] = 14 then
    v-val = t-amounts14[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 15 then
    v-val = t-amounts15[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 16 then
    v-val = 
      {x-varxq-fmt1.i "14" "15"}
  else 
  if v-subtotalup[v-inx4] = 17 then
    v-val = t-amounts17[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 18 then
    v-val = t-amounts18[v-inx4].
  else 
  if v-subtotalup[v-inx4] = 19 then
    v-val = 
      {x-varxq-fmt1.i "17" "18"}
  else 
    v-val = t-amounts1[v-inx4].
  do inz = 1 to 19:
  if inz = 1 then
    v-regval[inz] = t-amounts1[v-inx4].
  else 
  if inz = 2 then
    v-regval[inz] = t-amounts2[v-inx4].
  else 
  if inz = 3 then
    v-regval[inz] = t-amounts3[v-inx4].
  else 
  if inz = 4 then
    v-regval[inz] = t-amounts4[v-inx4].
  else 
  if inz = 5 then
    v-regval[inz] = 
      {x-varxq-fmt1.i}
  else 
  if inz = 6 then
    v-regval[inz] = t-amounts6[v-inx4].
  else 
  if inz = 7 then
    v-regval[inz] = t-amounts7[v-inx4].
  else 
  if inz = 8 then
    v-regval[inz] = t-amounts8[v-inx4].
  else 
  if inz = 9 then
    v-regval[inz] = t-amounts9[v-inx4].
  else 
  if inz = 10 then
    v-regval[inz] = 
      {x-varxq-fmt1.i "8" "9"}
  else 
  if inz = 11 then
    v-regval[inz] = t-amounts11[v-inx4].
  else 
  if inz = 12 then
    v-regval[inz] = t-amounts12[v-inx4].
  else 
  if inz = 13 then
    v-regval[inz] = 
      {x-varxq-fmt1.i "11" "12"}
  else 
  if inz = 14 then
    v-regval[inz] = t-amounts14[v-inx4].
  else 
  if inz = 15 then
    v-regval[inz] = t-amounts15[v-inx4].
  else 
  if inz = 16 then
    v-regval[inz] = 
      {x-varxq-fmt1.i "14" "15"}
  else 
  if inz = 17 then
    v-regval[inz] = t-amounts17[v-inx4].
  else 
  if inz = 18 then
    v-regval[inz] = t-amounts18[v-inx4].
  else 
  if inz = 19 then
    v-regval[inz] = 
      {x-varxq-fmt1.i "17" "18"}
  else 
    assign inz = inz.
  end.
&endif
 
&if defined(user_summaryloadprint) = 2 &then
  if v-lookup[v-inx2] = "C" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "CustName - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "E" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Entered Date - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "S" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "StageCd - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "T" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Taken By - " + h-lookup + " " + h-genlit.
    end.
  else
  if v-lookup[v-inx2] = "W" then
    do:
    if entry(v-inx2,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx2 = u-entitys then 
      assign v-summary-lit2 = h-lookup + " " + h-genlit.
    else
      assign v-summary-lit2 = "Warehouse - " + h-lookup + " " + h-genlit.
    end.
  else
      v-summary-lit = v-lookup[v-inx2].
&endif
 
&if defined(user_summaryloadexport) = 2 &then
  if v-inx4 > v-inx2  then 
    do:
    if v-inx4 > 1 then          
      export_rec = export_rec + v-del. 
    assign export_rec = export_rec +  " ".
    if v-cells[v-inx4] = "2" then
      assign export_rec = export_rec +  v-del + " ".
    end. 
  else 
  if v-lookup[v-inx4] = "C" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(30)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(30)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "E" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(12)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(12)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "S" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(3)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(3)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "T" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
  if v-lookup[v-inx4] = "W" then
    do:
    assign h-genlit = " ".
    if entry(v-inx4,v-holdsortmystery,v-delim) begins  v-6tildas then
      assign h-lookup = "All Others".
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    
    if v-cells[v-inx4] = "2" then
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign x-str1 = h-genlit.
      run String_Ready (input x-str1,
                         input-output h-genlit).
      assign export_rec = export_rec  
         + h-lookup + v-del + h-genlit.
      end.
    else
      do:
      if "x(4)" begins "X" Then 
        do:
        assign x-str1 = h-lookup.
        run String_Ready (input x-str1,
                           input-output h-lookup).
        end.
      assign export_rec = export_rec + h-lookup.
      end.
    end.
  else
    do:
    if v-inx4 > 1 then 
      export_rec = export_rec + v-del.
    assign export_rec = export_rec + v-lookup[v-inx4].
    end.
&endif
 
/* TBXR F6 CodeGen End of Includes that are CodeGen Generated  */ 
