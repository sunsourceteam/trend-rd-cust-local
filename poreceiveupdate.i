/* poreceiveupdate.i */
/* update the po receiving info */

 {po_defvars.i }   
 {p-poehb.i}
 
 
if s-updatefl then do:
  release poelb.

  for each poehb use-index k-openinit where                        
           poehb.cono        = g-cono + 500 and    
           poehb.receivefl   = yes          and    
           poehb.whse        = g-whse       and    
           poehb.openinit    = g-operinit   and
           poehb.whse        = s-rcvwhse    and     
           poehb.updtfl      = yes          and
           substring(poehb.user5,1,6) = "inb856" 
  no-lock:           
    /* change to for-each! Move non-stock bin check to here! dkt */     

    if substring(poehb.user5,1,6) = "inb856" then
      find first poelb where poelb.cono = poehb.cono and
                 poelb.shipmentid = poehb.shipmentid and
                 poelb.pono  = poehb.pono and
                 poelb.posuf = poehb.posuf and
                 substring(poelb.user5,7,1) <> "y" and 
                 (poelb.qtyrcv <> 0  and poelb.user6 <> 0)
      no-lock no-error.
    
     if avail poelb then do:
      assign s-updatefl = no.
      run zsdierrx.p 
        (input "UnAck ASN" + " " + string(poelb.pono) + " " + "Ln" + " " 
        + string(poelb.lineno),
         input "yes").
 
/*

/* variables need to run xx again */
def var z-loadcharfl   as log                no-undo.
def var v-start        as c no-undo.
def var v-status       as c format "x(63)"             no-undo.
def var v-newdisplay   as l                            no-undo.
def var v-nextpage     as l                            no-undo.
def var v-prevpage     as l                            no-undo.
def var v-scrollfl     as l                            no-undo.
def var v-editorfl     as logical            no-undo.
def var v-searchfl     as logical            no-undo.
def var v-srchfnd      as logical            no-undo.
def var v-recid       as i extent 20                  no-undo.
def var v-curpos        as i                            no-undo.
def var v-holdline      as i format ">>>9"              no-undo.
def new shared var toggle-sw as logical   no-undo.
def var loaded-sw as logical              no-undo.
def var v-nextfl        as l                            no-undo.
def var qtyLabel like poelb.user7 no-undo.
def var cQty as char no-undo.
def var extraData as char no-undo.
def var v-key          as c                  no-undo.
def var v-clearfl      as log                no-undo.
def var sr-prod   like poel.shipprod      no-undo.
define var o-Accum      as logical                      no-undo init no.
define var v-Accum      as logical                      no-undo init no.
define new shared var v-vend-accum like apsv.vendno     no-undo init 0.
define new shared var v-vend-accum-edi like apsv.edipartner no-undo init " ".
define new shared var h-vend-accum-edi like apsv.edipartner no-undo init " ".
define var v-prodsrcha as character format "x(24)"      no-undo.
define var v-prodsrchb as character format "x(24)"      no-undo.
def var sc-recid   as recid               no-undo.



define new shared temp-table tpoehb no-undo 
  field pono       like poeh.pono 
  field posuf      like poeh.posuf
  field prod       like poel.shipprod
  field lineno     like poel.lineno format ">>>"
  field linecnt    as integer
  field qtyord     like poel.qtyord format ">>>>>>9.99"
  field qtyrcv     like poel.qtyrcv format ">>>>>>9.99"
  field pqtyrcv    like poel.qtyord format ">>>>>>9.99"
  field pstkqtyrcv like poel.qtyrcv format ">>>>>>9.99"
  field binloc     like icsw.binloc1
  field statustype like icsw.statustype 
  field whse       like poeh.whse
  field unit       like poel.unit
  field type       as integer format "9"    
  field lncd       as integer format "9"    
  field descrip    like icsp.descrip[1] 
  field vname      as char format "x(25)"
  field vendno     like icsw.arpvendno
  field vprod      like icsw.prod 
  field leadtm     like icsw.leadtmavg
  field nonstockty like poel.nonstockty
  field serlottype like icsw.serlottype
  field price      like poel.price
  field nosnlots   like poel.nosnlots
  field qtyunavail like poel.qtyunavail
  field reasunavty like poel.reasunavty
  field poelbid    as recid 
  field botyp      as char format "x"
  field slotty     as char format "x"
  field potyp      as char format "xx"
  field shipmentid like poehb.shipmentid
  field flglbl     as logical format "y/n"
  field cancelfl   as logical format "y/n"
  field notesfl1   as char format "x"
  field comfl      as l    format "c/ "
  field prodsrch   as character format "x(24)"
  field expshpdt   like poeh.reqshipdt
/* SDI DANFOSS 11/14/05    */
  field vendedi    like apsv.edipartner
/* SDI DANFOSS 11/14/05    */    
    index k-temp1
          pono
          posuf
          lineno
          prod
    index k-temp2
          pono 
          posuf
          vprod
          lineno
    index k-temp3
          pono 
          posuf
          qtyrcv descending
    index k-temp4
          pono 
          posuf
          prodsrch
          lineno.

form 
  tpoehb.comfl    at 1
  tpoehb.lineno   format ">9"    at 2 
  tpoehb.prod     format "x(24)" at 5  /* was 6 */
  tpoehb.unit     format "x(4)"  at 30 /* was 33 */
  tpoehb.qtyord   format ">>>>9.99" at 35 /* was 38 */
/*   tpoehb.flglbl   at 49 */
  tpoehb.binloc   format "xx/xx/xxx/xxx" at 44 /* was at 51 */
  tpoehb.qtyrcv   format ">>>>>9.99"  at 59 /* was at 65 */
  /* qtyLabel        format ">>9"       at 70  */
  tpoehb.cancelfl at 76
  tpoehb.slotty   at 78
  tpoehb.botyp    at 79
  tpoehb.descrip  at 6 
  tpoehb.vprod    at 34  
  tpoehb.statustype at 59
  tpoehb.pono     at 64 format ">>>>>>9" 
  "-"             at 71
  tpoehb.posuf    at 72 
  tpoehb.notesfl1 at 74
  tpoehb.potyp    at 76
  with frame f-poehb row 6  scroll 1 no-box
  no-hide no-labels overlay v-length down.   
/*
  assign z-loadcharfl = false.
  assign v-start = "".
*/
   
                                    
                                    
  /*o Process XXEX screen */  
  {x-xxexcustom3.i &file         = "tpoehb"              
                   &frame        = "f-poehb"             
                   &find         = "poehb.lfi"              
                   &snchoosecond = "and not v-searchfl"
                   &field        = "tpoehb.lineno"
                   &internalfield  = "lineno"
                   &sneditsecond = "or v-searchfl"
                   &keys         = "keys v-key"
                   &searcher     = "poehb.sch"  
                   &searchat     = "at 1"
                   &go-on        = "ctrl-o"    
                   &usefile      = "poehb.lfu" 
                   &display      = "poehb.lds"            
                   &delete       = "poehb.del"
                   &delfl        = "*" 
                   &keymove      = "poehb.key"
                   &edit         = "poehb.led"
                   &gofile       = "poehb.gof"  
                   }

*/                
                
                
        /*
        (input "ASNed Line exists without verification " +
                string(poelb.pono) + "-" + string(poelb.posuf,"99")
                + " Line " + string(poelb.lineno), 
         input "yes"). 
        */ 

      pause.
    end.
  end.
end.
    
if s-updatefl then do:      
  for each poehb use-index k-openinit where        
         poehb.cono        = g-cono + 500 and   
         poehb.receivefl   = yes          and   
         poehb.whse        = g-whse       and   
         poehb.openinit    = g-operinit   and   
         poehb.whse        = s-rcvwhse    and   
         poehb.updtfl      = yes                
         no-lock:                                        
                  
  for each poelb where poelb.cono = poehb.cono and       
            poelb.shipmentid = poehb.shipmentid and       
            poelb.pono  = poehb.pono and                  
            poelb.posuf = poehb.posuf and                 
            poelb.qtyrcv <> 0                           
   no-lock:                                                 
      /* part moved up, autoWhse = yes */
      /* dkt update if match, else ignore it */                        
     find icsd where icsd.cono = g-cono and icsd.whse = poehb.whse    
     no-lock no-error.                                                

     if avail icsd then                                               
        if entry( 1, icsd.user4) = "Y" then do:                          
          find first zzbin                                             
            where zzbin.cono = poelb.cono /* 501 */                      
            and zzbin.whse = poehb.whse                                  
            and zzbin.prod = poelb.shipprod                              
            and zzbin.transproc = "PO"+ STRING(poehb.pono)
            /*  + "-" + STRING(poehb.posuf) use shipmentid?? */
            and zzbin.binloc ne ""  exclusive-lock no-error.             
            
          if avail zzbin then do:                                      
            /* update */                                               
            find icsw where 
                 icsw.cono = g-cono and                       
                 icsw.prod = poelb.shipprod and
                 icsw.whse = poehb.whse no-error.            
            find first wmsb where 
                       wmsb.cono = g-cono + 500 and
                       wmsb.whse = poehb.whse and 
                       wmsb.binloc = zzbin.binloc no-error.                                       
            if avail wmsb then do:                                          
              assign wmsb.statuscode = "A"                                                            wmsb.assigncode = "N"                                   
                     wmsb.priority   = 1.                                                    if avail icsw then do:                                        
                assign wmsb.assigncode = icsw.statustype.                                     if icsw.binloc1 = "" or icsw.binloc1 = "New Part" then do:   
                  icsw.binloc1 = zzbin.binloc.                           
                end.                                                        
                else do:                                                    
                    /* error out if mismatch per Tami */                      
                    /* if = then probably set through earlier zzbin record */ 
                if icsw.binloc1 ne zzbin.binloc and
/* INSPECT */
                   icsw.wmrestrict <> "insp" and
                   not(zzbin.binloc begins "insp" ) then do:                
/* INSPECT */    
                   message zzbin.prod " for bin " zzbin.binloc           
                       " should be in " icsw.binloc1.                       
                   s-updatefl = no.                                      
                   bell.                                                 
                   pause.                                                
                end.                                                    
/* INSPECT */
                else if avail icsw and icsw.wmrestrict = "insp" and
                   zzbin.binloc begins "insp"  and                 
                   icsw.binloc1 ne zzbin.binloc and
                   icsw.binloc2 ne zzbin.binloc and
               
                   not can-find(first b-zzbin where                            
                                      b-zzbin.cono = g-cono and 
                                      b-zzbin.whse = poehb.whse and
                                      b-zzbin.prod = poelb.shipprod and 
                                      b-zzbin.binloc  = zzbin.binloc) then do:
                   create b-zzbin.
                   assign wmsb.statuscode = "A"
                          wmsb.assigncode = if avail icsw then 
                                              icsw.statustype
                                            else
                                              "N"
                          wmsb.priority   = 9.
                   assign  b-zzbin.cono  = g-cono  
                           b-zzbin.whse  = zzbin.whse  
                           b-zzbin.binloc = zzbin.binloc 
                           b-zzbin.prod   = zzbin.prod
                           b-zzbin.operinit = g-operinits                                                 b-zzbin.transdt = TODAY                                                          b-zzbin.transtm = 
                             replace(STRING(TIME,"HH:MM:SS"),":","") 
                           b-zzbin.transproc = "PO" + string(poehb.pono).
                end.

/* INSPECT */    
              
              end.                                                      
            end. /* icsw */                                                
          find first wmsbp where 
                     wmsbp.cono = g-cono + 500 and         
                     wmsbp.whse = poehb.whse and 
                     wmsbp.binloc = zzbin.binloc and    
                     wmsbp.prod = poelb.shipprod no-lock no-error.           
          if not avail wmsbp then do:                                 
            create wmsbp.                                              
            assign wmsbp.cono = g-cono + 500                                 
                   wmsbp.whse = poehb.whse.                                   
            {t-all.i wmsbp}
            assign
                   wmsbp.transproc = "poreceiveupdate.i"
                   wmsbp.binloc = zzbin.binloc                               
                   wmsbp.prod = poelb.shipprod.                               
           end.
         end. /* avail wmsb */                                            
         if not avail wmsb then do:                                     
           message "Bin "  zzbin.binloc " not found".                   
           message "PO" poelb.pono " caused update to stop".            
           s-updatefl = no.                                             
           bell.                                                        
           pause.                                                       
         end.                                                           
         /* end update */                                               
            /* delete found zzbin - it did its job */                      
          delete zzbin.                                                  
        end. /* if avail zzbin */                                        
      end. /* if autoWhse */        
        /* end insert autoWhse */
    end. /* new for each poelb */
  end.
end.

if s-updatefl = no then do:
  assign g-currproc = hld-currproc
         g-ourproc  = hld-currproc
         g-exitproc = hld-currproc.
  return.
end.
else
if s-updatefl then do:
  assign s-rcvwhse = input s-rcvwhse.
  {w-icsd.i s-rcvwhse "no-lock" "x-"} 
  if not avail x-icsd then do:
    bell.
    message "Warehouse To Receive For Not Set Up in Warehouse Setup - ICSD"
      s-rcvwhse.
    pause.
  end.
  else 
    assign g-whse = s-rcvwhse
           b-whse = s-rcvwhse.
  
  assign g-currproc   = "poei"
         g-ourproc    = "poei".
  
  run poezbco.p (input sasc.crarfl,
                 input p-postdt,
                 input p-period).  
  g-exitproc = "poei".
end. 
     
find b-sasc where b-sasc.cono = g-cono no-lock no-error.
cPOCrctReason = if avail b-sasc then b-sasc.pocrctreason else "".

/*d If the journal was not opened or the journal procedure is not for POEI,
    get out of the Receiving */
if g-jrnlno = 0 or g-jrnlproc ne g-currproc then leave.

/*d If a journal is already open, set the whse to the whse on the first POEI
    record */
if g-jrnlno > 0 then do:
  find first poei use-index k-lineno where
             poei.cono   = g-cono and
             poei.jrnlno = g-jrnlno
             no-lock no-error.
          
  if avail poei then 
    do for poeh, poel:
      {w-poeh.i poei.pono poei.posuf no-lock}
      {w-poel.i poei.pono poei.posuf poei.lineno no-lock}
      if avail poel then
        assign v-divno = poeh.divno
               v-whse  = poel.whse
               v-firstfl = yes.
    end.
end. /* g-jrnlno > 0 */
     
if avail poei then
  {w-poeh.i poei.pono poei.posuf no-lock b-}
else
  {w-poeh.i g-pono g-posuf no-lock b-}

if v-firstfl = yes and v-pocapfl = yes and
  (v-pocapaddfl = yes or v-pocapdiscfl = yes) and
  g-potype ne "rm" then
  do i = 1 to 4:
    if b-poeh.addonno[i] > 0 then
      {w-sastn.i "x" b-poeh.addonno[i] no-lock}
    assign
      s-poadddist[i]  = if v-vapofl = yes
                          then "m"
                          else if b-poeh.addonno[i] > 0
                                  and avail sastn
                               then sastn.state
                               else ""
      s-addondesc[i]  = if b-poeh.addonno[i] > 0 and avail sastn
                          then sastn.descrip
                          else "".
  end. /* do 1 to 4 */
assign v-poadddist = if s-poadddist[1] = "m" or
                        s-poadddist[2] = "m" or
                        s-poadddist[3] = "m" or
                        s-poadddist[4] = "m" then "m" else "a"
       lCrctExists = false.
do transaction:
if s-updatefl then do:
    
    /* Update Zsdiputid here so we can capture copies */

    /* This is temporary in company 501 and is used in 
       oeepbpr.z99 once and then deleted       SX 55 
    */   
    do for b-zsdiputid:
    find   b-zsdiputid use-index jrnlix  where
           b-zsdiputid.cono = g-cono + 500 and
           b-zsdiputid.whse = g-whse and
           b-zsdiputid.jrnlno = g-jrnlno no-error.
    if not avail b-zsdiputid then do:
      create b-zsdiputid.
      assign b-zsdiputid.cono      = g-cono + 500 
               b-zsdiputid.whse      = g-whse
               b-zsdiputid.jrnlno    = g-jrnlno 
               b-zsdiputid.receiptid = 1.
      end.
    
    assign b-zsdiputid.receiptid = s-rcptno. 
    end.
    /* Update Zsdiputid here so we can capture copies */
end.
end.

if s-updatefl then 
  do transaction:
    /*e Actual Processing of the Batch Files */
    /* fix up suffixes */
    for each poehb use-index k-openinit where                        
             poehb.cono        = g-cono + 500 and    
             poehb.receivefl   = yes          and    
             poehb.whse        = g-whse       and    
             poehb.openinit    = g-operinit   and
             poehb.whse        = s-rcvwhse /* v-saso-whse */  and     
             poehb.updtfl      = yes no-lock:                
       
      assign zx-suff = poehb.posuf
             zx-okfl = true.
      run poehb_suffix (input poehb.pono,
                        input-output zx-suff,
                        input-output zx-okfl,
                        input-output zx-error).
      if not zx-okfl then do:
        message {poehb_suffix.gcr  
                   zx-error
                   zx-l                
                   poehb.pono
                   poehb.posuf}.
        pause 5.
      end.
    end. /* for each poehb */
  end.

if s-updatefl then do:
   
  /* print and update, then delete batch */
  run poeibwpstk.p(input g-whse, input g-operinit).
  run RFLoop.
  if not zx-l then do:
    /* print report */
    {zsdipoeiupd.lpr &message = "run zsdierrx.p "
                     &confirm = "confirm"
                     &jrnlno  = "g-jrnlno"
                     &next    =
          " assign g-currproc = 'poezb' g-exitproc = g-currproc. undo main ,
                   next main."
                     &crctconfirm   = "lCrctExists = true"
                     &pocrctreason  = "cPOCrctReason = ''"}
  end.
  else do:
    /* print report, handheld */
    {zsdipoeiupd.lpr &message = "run ibrferrx.p "                            
                     &confirm = "confirm"
                     &jrnlno  = "g-jrnlno"   
                     &next    =                                                            " assign g-currproc = 'hher' g-exitproc = g-currproc. undo main , 
                         next main."                                                                &crctconfirm   = "lCrctExists = true"                                           &pocrctreason  = "cPOCrctReason = ''"}                       end.      
  assign v-addondist  = 0
         v-wodiscdist = 0.
  assign x-f10-whse = s-rcvwhse.

  /*d Perform the automatic allocation of addons and the
      whole order discount */
  if v-pocapfl = yes and
    (v-pocapaddfl = yes or
    (v-pocapdiscfl = yes and can-do("w,c,d,u",v-powodist)))
    then run poeia.p.
       
  /* Adjust and create correction PO's if necessary */
  if lCrctExists = true then
    run poeicrct.p(input g-jrnlno,
                   input g-setno,
                   input s-adjbofl).
    
  /*d Perform the update of other related/tied tables */
  run poeiu.p.
  assign g-currproc = hld-currproc
         g-ourproc  = hld-currproc
         g-exitproc = hld-currproc.
  return. 
end. /* s-updatefl */

assign g-currproc = hld-currproc
       g-ourproc  = hld-currproc
       g-exitproc = hld-currproc.
end. /* main */

release sasj. /* hanging up on journal assignment */
hide all no-pause.

Procedure RFLoop:                          
assign zx-l = no                           
       zx-x = 1.                           
repeat while program-name(zx-x) <> ?:      
  if program-name(zx-x) = "hher.p" then do:
    zx-l = yes.                          
    leave.                                 
  end.                                     
  assign zx-x = zx-x + 1.                  
end.                                       
end procedure.                             
