{g-all.i}

/* */


define shared var p-begdt as date no-undo.
define shared var p-enddt as date no-undo.

define shared var p-begcust as de no-undo.
define shared var p-endcust as de no-undo.
define shared var p-attn    as c format "x(30)" no-undo.
define shared var p-fname    as c format "x(30)" no-undo.
define shared var p-detfl   as logical no-undo.
def var p-ord7typ as logical           no-undo.


def var customerpart as c format "x(24)" no-undo.
def var pagehold as integer no-undo.
def var paged    as logical no-undo.
def var flout3 as c format "x(165)" no-undo.
def var x-price as de no-undo.
def var x-ext as de no-undo.
def var x-desc as c format "x(24)" no-undo.




define stream zdetails.
define stream zsummary.
if not p-detfl then
  do:
  output stream zdetails to  value("/usr/tmp/" + p-fname + "detail").
  output stream zsummary to  value("/usr/tmp/" + p-fname + "summary") paged.
  end.

define var v-total as de no-undo.
define var v-total2 as de no-undo.

                                                    

define temp-table smbal no-undo

  field zcust   as de
  field zqty    as de 
  field zoesale as de 
  field zsurchg as de 
  field zum     as c
  field zprice  as de
  field zprod   like icsp.prod
  field zordno  like oeeh.orderno
  field zordnox as de
  field zordsuf like oeeh.ordersuf
  field zpono   like oeeh.custpo
  field zinvdt  like oeeh.invoicedt
  field zdesc   as c format "x(30)"
  field zrefer  like oeeh.refer


  index reinx
        zcust
        zprod
        
  index detinx
        zordno
        zordsuf
        zprod.


/*

form 

skip(2)
"Bill To:"              at 1
arsc.name               at 1
p-attn                  at 1
arsc.addr[1]            at 1
arsc.addr[2]            at 1
arsc.city               at 1
arsc.state
arsc.zipcd
 
with frame f-header2 no-labels page-top.

                                                    

form 

"SUMMARY INVOICE"       at 37
"Page:"                 at 70
skip (2)
"Summary Invoice No:"   at 46
"     1"                at 68
"Account No:"           at 54
"    65"                at 68
 
with frame f-header1 page-top.
                                                     
form

skip (5)
"Qty"                   at 39
"Price"                 at 49
"Disc"                  at 59
"Extension"             at 67
skip(1)
 
with frame f-header3 page-top.

*/                                                    

form
smbal.zprod             at 1
smbal.zqty              at 36
smbal.zprice            at 46
smbal.zum               at 56
smbal.zoesale           at 65
with frame f-detail down no-labels.
/*

form
smbal.zprod             at 1
smbal.zqty              at 36
smbal.zprice            at 46
smbal.zum               at 57
smbal.zoesale           at 65
smbal.zdesc             at 1
with frame f-detailx down no-labels.
*/                                                     
 
form 

skip(1)
"Total Due:"    at 52
v-total         at 64 format "->>>>>>>>.99"

with frame f-total down with no-labels.





define var xyr as integer no-undo.
define var xinx as integer no-undo.




/* */

define var zslsrep like smss.slsrep no-undo.
define var p-cost as logical init yes.
define var v-cost as de no-undo.
define var v-firsthead as logical no-undo.

define new shared buffer b-oeel for oeel.
define new shared buffer b2-oeel for oeel.
define new shared buffer b-oeeh for oeeh.
define new shared buffer b2-oeeh for oeeh.

define buffer b-icsd for icsd.


def new shared var v-smwodiscfl  like sasc.smwodiscfl initial no no-undo.
def new shared var v-oecostsale  like sasc.oecostsale initial no no-undo.
def new shared var v-smcustrebfl like sasc.smcustrebfl initial no no-undo.
def new shared var v-smvendrebfl like sasc.smvendrebfl initial no no-undo.

{speccost.gva "new shared"}
{rebnet.gva "new shared"}
/* g-cono = 1. */
v-smvendrebfl = true.
v-smcustrebfl = true.


p-ord7typ = no.
for each b-icsd where b-icsd.cono = 1 no-lock:

for each b-oeeh use-index k-invproc
                where b-oeeh.cono = 1 and
                      b-oeeh.whse = b-icsd.whse and
                      b-oeeh.invoicedt ge p-begdt and
                      b-oeeh.invoicedt le p-enddt and
                      b-oeeh.custno    ge p-begcust and
                      b-oeeh.custno    le p-endcust and
                      
                      b-oeeh.stagecd <> 9 and
                      b-oeeh.stagecd > 3 no-lock:
                                             

  for each b-oeel where b-oeel.cono = 1 and 
                        b-oeel.orderno = b-oeeh.orderno and
                        b-oeel.ordersuf = b-oeeh.ordersuf and
                        b-oeel.statustype = "i" and
                        b-oeel.invoicedt ge p-begdt and
                        b-oeel.invoicedt le p-enddt and 
                        b-oeel.specnstype <> "l" no-lock:

/* Tipper does not want Zero lines */    
    if (b-oeeh.custno = 151 and b-oeel.qtyship = 0) or
       (b-oeeh.custno = 151 and b-oeeh.whse <> "ctip") then
      next.

    if b-oeeh.custno = 151 then 
       p-ord7typ   = yes.
    else
       p-ord7typ   = no.

    run incost.p (no,
                  p-cost,
                  b-oeel.cono,
                  output v-cost). 
                   
    if b-oeeh.transtype = "cr" then
      v-cost = 0.
    
    
    zslsrep = b-oeel.slsrepout.
    if b-oeel.slsrepout = "" then
       zslsrep  = b-oeeh.slsrepout.

  if b-oeel.specnstype = "n" then
     x-desc = b-oeel.proddesc.
  else
    do:
    find icsp where icsp.cono = g-cono and
                    icsp.prod = b-oeel.shipprod no-lock no-error.
    if avail icsp then
      x-desc = icsp.descrip[1] .
    end.               
 
  find icsec where icsec.cono = g-cono and 
                   icsec.rectype = "c" and
                   icsec.custno = b-oeel.custno and
                   icsec.altprod = b-oeel.shipprod no-lock no-error.
   
  if not p-detfl then
    do:
    find smbal use-index reinx
             where smbal.zprod = (if avail icsec then
                                    icsec.prod
                                  else  
                                  if b-oeel.reqprod <> "" then
                                    b-oeel.reqprod
                                 else
                                    b-oeel.shipprod)  
                    no-error.
    end.
 else
    do:
    find smbal use-index detinx
             where smbal.zordno = b-oeel.orderno and
                   smbal.zordsuf  = b-oeel.ordersuf and
             
             
                  smbal.zprod = (if avail icsec then
                                   icsec.prod
                                 else  
                                 if b-oeel.reqprod <> "" then
                                    b-oeel.reqprod
                                 else
                                    b-oeel.shipprod)  
                    no-error.
  
    end.
  if avail smbal then
    do:

      x-price  =
        round ((b-oeel.price * ((100 - b-oeel.discpct) / 100)),4).
 
      x-ext =
        round ((((b-oeel.price * ((100 - b-oeel.discpct) / 100)))   
             * b-oeel.qtyship)
             /* * (if b-oeel.returnfl = true then -1 else 1) */ ,2).
 
    
    smbal.zoesale =
      smbal.zoesale
          +
      round ((((b-oeel.price * ((100 - b-oeel.discpct) / 100)))   
             * b-oeel.qtyship)
             * (if b-oeel.returnfl = true then -1 else 1),2).
             
    
    smbal.zsurchg =
      smbal.zsurchg
          +
      round ((b-oeel.datccost   
             * b-oeel.qtyship)
             * (if b-oeel.returnfl = true then -1 else 1),2).
             
             
      smbal.zqty = 
        smbal.zqty + 
         (b-oeel.qtyship 
         * (if b-oeel.returnfl = true then -1 else 1)).
          

     
    end.              
  else
    do:
          x-price  =
        round ((b-oeel.price * ((100 - b-oeel.discpct) / 100)),4).
 
      x-ext =
        round ((((b-oeel.price * ((100 - b-oeel.discpct) / 100)))   
             * b-oeel.qtyship)
             /* * (if b-oeel.returnfl = true then -1 else 1) */,2).
    
    create smbal.
   
    if p-detfl then
      do:
      assign smbal.zordno  = b-oeel.orderno
             smbal.zordsuf = b-oeel.ordersuf
             smbal.zpono    = b-oeeh.custpo
             smbal.zinvdt   = b-oeeh.shipdt
             smbal.zrefer   = b-oeeh.refer
             smbal.zdesc    = x-desc
             smbal.zordnox   = (smbal.zordno * 100) + smbal.zordsuf.
      end.
    
    
    assign   smbal.zprod  = (if avail icsec then
                               icsec.prod
                             else  
                             if b-oeel.reqprod <> "" then 
                               b-oeel.reqprod 
                            else
                               b-oeel.shipprod)
             smbal.zum   = b-oeel.unit
             smbal.zcust = b-oeeh.custno
             smbal.zprice =
             (b-oeel.price * ((100 - b-oeel.discpct) / 100))   
             smbal.zqty = 
                       (b-oeel.qtyship 
                        * (if b-oeel.returnfl = true then -1 else 1)).
 
    
    smbal.zsurchg =
      smbal.zsurchg
          +
      round ((b-oeel.datccost   
             * b-oeel.qtyship)
             * (if b-oeel.returnfl = true then -1 else 1),2).
             
    
    smbal.zoesale =
      smbal.zoesale
          +
     round ((((b-oeel.price * ((100 - b-oeel.discpct) / 100)))   
             * b-oeel.qtyship)
             * (if b-oeel.returnfl = true then -1 else 1),2).
    

    end.     
  if not p-detfl then                                               
    if b-oeeh.custno = 65 then    /* IMT */ 
       run fyleoutput3.
    else 
       run fyleoutput2. 
  end.                                                    
end.
end. /* icsd */
if not p-detfl then
  do:
  do for smbal,arsc:
    form header

    skip(2)
    "Bill To:"              at 1
    arsc.name               at 1
    p-attn                  at 1
    arsc.addr[1]            at 1
    arsc.addr[2]            at 1
    arsc.city               at 1
    arsc.state
    arsc.zipcd
 
  with frame f-header2 no-labels page-top.

                                                    

  form header

  "SUMMARY INVOICE"       at 37
  "Page:"                 at 69
  page-number(zsummary)   at 75 format ">>>9"
  skip (2)
  "Summary Invoice No:"   at 51
  "     1"                at 73
  "Account No:"           at 59
  arsc.custno             at 73 format ">>>>>9"
 
  with frame f-header1 page-top.
                                                     
  form header
  
  skip (5)
  "Qty"                   at 39
  "Price"                 at 49
  "Disc"                  at 59
  "Extension"             at 67
  skip(1)
 
  with frame f-header3 page-top.

                              
view stream zsummary frame f-header1.
view stream zsummary frame f-header2.
view stream zsummary frame f-header3.
 
                             
                             
for each smbal use-index reinx no-lock,
 each arsc where arsc.cono = g-cono and
                 arsc.custno = smbal.zcust no-lock 
  
   break by smbal.zcust:

   v-total = v-total + smbal.zoesale + smbal.zsurchg.
 
 
   if first-of(smbal.zcust) then
      do:
    
      hide stream zsummary frame f-header1.
      hide stream zsummary frame f-header2.
      hide stream zsummary frame f-header3.
      page-number (zsummary)  = 1.                          
      view stream zsummary frame f-header1.
      view stream zsummary frame f-header2.
      view stream zsummary frame f-header3.
     
      page.
      end.
  
   
   if smbal.zoesale <> 0 or
      smbal.zqty <> 0 then
     do:       
     display stream zsummary
        smbal.zprod
        smbal.zqty
        (smbal.zoesale / (if smbal.zqty <> 0 then smbal.zqty else 1)) 
                        @ smbal.zprice
        smbal.zum
        smbal.zoesale
        with frame f-detail.
     down stream zsummary with frame f-detail.
     hide stream zsummary frame f-header2.
     end.
   
   if smbal.zsurchg <> 0 then
     do:       
     display stream zsummary
        "Material Surcharge" @ smbal.zprod
        smbal.zqty
        (smbal.zsurchg / (if smbal.zqty <> 0 then smbal.zqty else 1)) 
                        @ smbal.zprice
        smbal.zum
        smbal.zsurchg  @ smbal.zoesale
        with frame f-detail.
     down stream zsummary with frame f-detail.
     hide stream zsummary frame f-header2.
     end.

 
   
   if last-of(smbal.zcust) then
     do:
     display stream zsummary v-total with frame f-total.
     v-total = 0.
     end.
 
   
  end.                               
end.
end.
else
if p-detfl then
  do:
  do for smbal,arsc:

    form header
     "Invoice Period From"
     space(1)
     p-begdt
     space(1)
     "Through"
     space(1)
     p-enddt
     
 with frame f-headerx1 no-labels page-top.

     
    
    
    form header

    skip(1)
    "Bill To:"              at 1
    arsc.name               at 1
    p-attn                  at 1
    arsc.addr[1]            at 1
    arsc.city               at 1
    arsc.state
    arsc.zipcd
 
  with frame f-headerx2 no-labels page-top.

                                                    
                                                     
form header
  
  "Invoice"             at 1
  "Purchase order"      at 13
  "Date Ship"           at 40
  smbal.zordno  at 1   format ">>>>>>9"
  "-"           at 8
  smbal.zordsuf at 9   format "99"
  smbal.zpono   at 13
  smbal.zinvdt  at 40
  smbal.zrefer  at 50 
  
/*  
  with frame f-headerx2a.
  
  
form header
*/
  "Qty Shp"               at 39
  "Price"                 at 49
  "UM"                    at 57  
  "Extension"             at 67
  with frame f-headerx2a.
  

form
smbal.zprod             at 1
smbal.zqty              at 36
smbal.zprice            at 46
smbal.zum               at 57
smbal.zoesale           at 65
smbal.zdesc             at 1
with frame f-detailx down no-labels.
 

form 

"**** Total Charges For Invoice " space(0)
smbal.zordno format ">>>>>>9" space(0)
"-"
space(0)
smbal.zordsuf
space(0)
"******" 
space(0)
v-total         at 64 format "->>>>>>>>.99"

with frame f-totalx down with no-labels.



form 

skip(1)
"Final Total:"    at 52
v-total2        at 64 format "->>>>>>>>.99"

with frame f-total2 down with no-labels.


                              
                              
                              
view  frame f-headerx1.
view  frame f-headerx2.
 
                             
                             
for each smbal use-index detinx  no-lock,
 each arsc 
      where arsc.cono = g-cono and
                 arsc.custno = smbal.zcust no-lock 
                 with frame f-detailx
   break by smbal.zordnox : 

if first-of(smbal.zordnox) and
   line-counter > (58 - 5) then
  page.  


if pagehold <> page-number then
   assign paged = true
          pagehold = page-number.

   v-total = v-total + smbal.zoesale + smbal.zsurchg.
   v-total2 = v-total2 + smbal.zoesale + smbal.zsurchg.


 
   if first-of(smbal.zordnox)  or paged then
      do:
      paged = false.
      hide frame f-headerx2a.
      view frame f-headerx2a.
      end.
  
   
   if smbal.zoesale <> 0 or
      smbal.zqty <> 0 then
     do:       
     display 
        smbal.zprod
        smbal.zqty
        (smbal.zoesale / (if smbal.zqty <> 0 then smbal.zqty else 1)) 
                        @ smbal.zprice
        smbal.zum
        smbal.zoesale
        smbal.zdesc
        with down frame f-detailx.
     down  with frame f-detailx.
     
     end.
   
   if smbal.zsurchg <> 0 then
     do:       
     display 
        "Material Surcharge" @ smbal.zprod
        smbal.zqty
        (smbal.zsurchg / (if smbal.zqty <> 0 then smbal.zqty else 1)) 
                        @ smbal.zprice
        smbal.zum
        smbal.zsurchg @ smbal.zoesale
        " " @ smbal.zdesc
        with down frame f-detailx.
     down  with frame f-detailx.
     
     end.


   
   if last-of(smbal.zordnox) then
     do:
     display 
     smbal.zordno
     smbal.zordsuf
     v-total with frame f-totalx.
     v-total = 0.
     end.
   if last(smbal.zordnox) then
     do:
     display v-total2 with frame f-total2.
     v-total = 0.
     end.
  
   
  end.                               
end.
end.   


                          


/* ------------------------------------------------------------------------*/
PROCEDURE fyleoutput.
/* ------------------------------------------------------------------------*/
   customerpart = ( if avail icsec then 
                      icsec.prod
                    else
                    if b-oeel.reqprod <> "" then
                      b-oeel.reqprod
                    else
                      b-oeel.shipprod).
    assign
       overlay(flout3,1,22)   = string(b-oeeh.custpo,"x(22)")
       overlay(flout3,23,12)  = "            "
       overlay(flout3,35,1)   = "0"
       overlay(flout3,36,24)  = string(customerpart,"x(24)") 
       overlay(flout3,60,6)   = "    "
       overlay(flout3,66,6)   = string(b-oeel.orderno,"999999")
       overlay(flout3,72,1)   = "-"
       overlay(flout3,73,2)   = string(b-oeel.ordersuf,"99")
       overlay(flout3,75,2)   = substring(
                                  string(year(b-oeeh.invoicedt),"9999"),3,2)
       overlay(flout3,77,2)   = string(month(b-oeeh.invoicedt),"99")
       overlay(flout3,79,2)   = string(day(b-oeeh.invoicedt),"99")
       overlay(flout3,81,1)   = if b-oeel.returnfl = true then "-" else "+"
       overlay(flout3,82,7)   = string(b-oeel.qtyship,"9999999")
       overlay(flout3,89,11)  = string(x-price,"999999.9999")
       overlay(flout3,100,2)  = string(substring(b-oeel.unit,1,2))
       overlay(flout3,102,1)   = if b-oeel.returnfl = true then "-" else "+"
       overlay(flout3,103,11) = string(x-ext,"99999999.99")
       overlay(flout3,114,24) = string(x-desc,"x(24)")
       overlay(flout3,138,26) = "                          ".
                           
                                      
       put stream zdetails flout3 format "x(164)" at 1 skip. 
 if b-oeel.datccost <> 0 then
   do:
    customerpart = "Material Surcharge" .
    x-price  =
        round (b-oeel.datccost,4).
 
      x-ext =
        round ((b-oeel.datccost   
             * b-oeel.qtyship)
             ,2).
 
    
    assign
       overlay(flout3,1,22)   = string(b-oeeh.custpo,"x(22)")
       overlay(flout3,23,12)  = "            "
       overlay(flout3,35,1)   = "0"
       overlay(flout3,36,24)  = string(customerpart,"x(24)") 
       overlay(flout3,60,6)   = "    "
       overlay(flout3,66,6)   = string(b-oeel.orderno,"999999")
       overlay(flout3,72,1)   = "-"
       overlay(flout3,73,2)   = string(b-oeel.ordersuf,"99")
       overlay(flout3,75,2)   = substring(
                                  string(year(b-oeeh.invoicedt),"9999"),3,2)
       overlay(flout3,77,2)   = string(month(b-oeeh.invoicedt),"99")
       overlay(flout3,79,2)   = string(day(b-oeeh.invoicedt),"99")
       overlay(flout3,81,1)   = if b-oeel.returnfl = true then "-" else "+"
       overlay(flout3,82,7)   = string(b-oeel.qtyship,"9999999")
       overlay(flout3,89,11)  = string(x-price,"999999.9999")
       overlay(flout3,100,2)  = string(substring(b-oeel.unit,1,2))
       overlay(flout3,102,1)   = if b-oeel.returnfl = true then "-" else "+"
       overlay(flout3,103,11) = string(x-ext,"99999999.99")
       overlay(flout3,114,24) = string(x-desc,"x(24)")
       overlay(flout3,138,26) = "                          ".
                           
                                      
       put stream zdetails flout3 format "x(164)" at 1 skip. 

   end.
 
 end.                  


/* ------------------------------------------------------------------------*/
PROCEDURE fyleoutput2.
/* ------------------------------------------------------------------------*/
   customerpart = ( if avail icsec then 
                      icsec.prod
                    else
                    if b-oeel.reqprod <> "" then
                      b-oeel.reqprod
                    else
                      b-oeel.shipprod).
    assign
       overlay(flout3,1,22)   = string(b-oeeh.custpo,"x(22)")
       overlay(flout3,23,12)  = "            "
       overlay(flout3,35,1)   = "0"
       overlay(flout3,36,24)  = string(customerpart,"x(24)") 
       overlay(flout3,60,6)   = "    "
       overlay(flout3,66,7)   = string(b-oeel.orderno,"9999999")
       overlay(flout3,73,1)   = "-"
       overlay(flout3,74,2)   = string(b-oeel.ordersuf,"99")
       overlay(flout3,76,2)   = substring(
                                  string(year(b-oeeh.invoicedt),"9999"),3,2)
       overlay(flout3,78,2)   = string(month(b-oeeh.invoicedt),"99")
       overlay(flout3,80,2)   = string(day(b-oeeh.invoicedt),"99")
       overlay(flout3,82,1)   = if b-oeel.returnfl = true then "-" else "+"
       overlay(flout3,83,7)   = string(b-oeel.qtyship,"9999999")
       overlay(flout3,90,11)  = string(x-price,"999999.9999")
       overlay(flout3,101,2)  = string(substring(b-oeel.unit,1,2))
       overlay(flout3,103,1)   = if b-oeel.returnfl = true then "-" else "+"
       overlay(flout3,104,11) = string(x-ext,"99999999.99")
       overlay(flout3,115,24) = string(x-desc,"x(24)")
       overlay(flout3,139,24) = string(b-oeeh.refer,"x(24)")
       overlay(flout3,162,2)  = "  ".
                           
       put stream zdetails flout3 format "x(165)" at 1 skip. 
       p-ord7typ = no.
 if b-oeel.datccost <> 0 then 
   do:
    customerpart = "Material Surcharge". 
    x-price  =
        round ((b-oeel.datccost),4).
 
      x-ext =
        round ((b-oeel.datccost   
             * b-oeel.qtyship)
             ,2).
 
    
    assign
       overlay(flout3,1,22)   = string(b-oeeh.custpo,"x(22)")
       overlay(flout3,23,12)  = "            "
       overlay(flout3,35,1)   = "0"
       overlay(flout3,36,24)  = string(customerpart,"x(24)") 
       overlay(flout3,60,6)   = "    "
       overlay(flout3,66,7)   = string(b-oeel.orderno,"9999999")
       overlay(flout3,73,1)   = "-"
       overlay(flout3,74,2)   = string(b-oeel.ordersuf,"99")
       overlay(flout3,76,2)   = substring(
                                  string(year(b-oeeh.invoicedt),"9999"),3,2)
       overlay(flout3,78,2)   = string(month(b-oeeh.invoicedt),"99")
       overlay(flout3,80,2)   = string(day(b-oeeh.invoicedt),"99")
       overlay(flout3,82,1)   = if b-oeel.returnfl = true then "-" else "+"
       overlay(flout3,83,7)   = string(b-oeel.qtyship,"9999999")
       overlay(flout3,90,11)  = string(x-price,"999999.9999")
       overlay(flout3,101,2)  = string(substring(b-oeel.unit,1,2))
       overlay(flout3,103,1)   = if b-oeel.returnfl = true then "-" else "+"
       overlay(flout3,104,11) = string(x-ext,"99999999.99")
       overlay(flout3,115,24) = string(x-desc,"x(24)")
       overlay(flout3,139,24) = string(b-oeeh.refer,"x(24)")
       overlay(flout3,162,2)  = "  ".
                           
       put stream zdetails flout3 format "x(165)" at 1 skip. 
       p-ord7typ = no.
   end.  
 end.                  

/* ------------------------------------------------------------------------*/
PROCEDURE fyleoutput3.
/* ------------------------------------------------------------------------*/
   if not(b-oeeh.custpo begins "SS") and 
      not(b-oeeh.custpo begins "ST") then do:
     p-ord7typ = no.
     return.
   end.
   customerpart = ( if avail icsec then 
                      icsec.prod
                    else
                    if b-oeel.reqprod <> "" then
                      b-oeel.reqprod
                    else
                      b-oeel.shipprod).
    assign
       overlay(flout3,1,5)    = "O-110" 
       overlay(flout3,6,30)   = "                              " 
       overlay(flout3,36,30)  = left-trim(string(customerpart,"x(30)"))
       overlay(flout3,66,17)  = 
                                 string(b-oeel.orderno,"9999999") +
                                  "-" +
                                 string(b-oeel.ordersuf,"99") + " " +
                                 string(b-oeeh.custpo,"x(6)")
       overlay(flout3,83,7)   = string(b-oeel.qtyship,"9999999")
       overlay(flout3,90,11)  = string(x-price,"999999.9999")
       overlay(flout3,101,2)  = string(substring(b-oeel.unit,1,2))
       overlay(flout3,103,12) = string(x-ext,"999999999.99")
       overlay(flout3,115,40) = string(x-desc,"x(40)").
                           
       put stream zdetails flout3 format "x(154)" at 1 skip. 
       p-ord7typ = no.

 if b-oeel.datccost <> 0 then 
   do:
    customerpart = "Material Surcharge". 
    x-price  =
        round ((b-oeel.datccost),4).
 
      x-ext =
        round ((b-oeel.datccost   
             * b-oeel.qtyship)
             ,2).
 
    
    assign
       overlay(flout3,1,5)    = "O-110" 
       overlay(flout3,6,30)   = "                              " 
       overlay(flout3,36,30)  = left-trim(string(customerpart,"x(30)"))
       overlay(flout3,66,17)  = string(b-oeeh.custpo,"x(17)")
       overlay(flout3,83,7)   = string(b-oeel.qtyship,"9999999")
       overlay(flout3,90,11)  = string(x-price,"999999.9999")
       overlay(flout3,101,2)  = string(substring(b-oeel.unit,1,2))
       overlay(flout3,103,12) = string(x-ext,"999999999.99")
       overlay(flout3,115,40) = string(x-desc,"x(40)").
                            
       put stream zdetails flout3 format "x(154)" at 1 skip. 
       p-ord7typ = no.
   end.  
 end.                  




