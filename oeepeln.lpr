/*oeepeln.lpr*/
/*h****************************************************************************
  INCLUDE      : oeepeln.lpr
  DESCRIPTION  : Processing logic used by oeepe (Advanced Shipping Notice)
                 to prepare line variables for producing the following
                 documents.  It is used in both CHUI and GUI:
                 1) 856 - EDI Advanced Shipping Notice (ASN)
                 2) XML Enabled documents for the ASN
  USED ONCE?   : yes
  AUTHOR       : ajw
  DATE WRITTEN : 06/27/00
  CHANGES MADE :
    06/27/00 ajw; TB# e5636 Buildnet Enable; export ASN as an XML Document.
        NOTE:  We're not currently using it in the XML document, but this logic
        is packaged up so we could be positioned in the future to use it.
    05/21/01 rcl; TB# e6873 Increase ICSP weight and cube fields to 5 decimals.
    12/02/02 ns;  TB# e12955 Add EDI Line #
    04/26/04 emc; TB# e17944 EDI Map Confict w/ EDI Line Number Field
******************************************************************************/
    /*NOTE:  The references to the EDIH and EDIL EDI files are only used for
             EDI Processing.  If this include is called for the XML format,
             this logic is commented out throughout this include file.
             Additionally, all places where the data is exported is also
             commented out.  We handle the exporting of the XML data in
             a different place in XML... This include is just used to prepare
             variables consistently between EDI and XML.*/

        /* read the EDIL record associated with this line */
        /* edil must be read by EDI and XML in order to set the edilineno */
        {w-edil.i """" oeel.orderno oeel.lineno no-lock}

        if oeel.specnstype <> "n" then do:
            {w-icsw.i oeel.shipprod oeel.whse no-lock}
            {w-icsp.i oeel.shipprod no-lock}
        end.

        assign
            v-oeelid    = recid(oeel)
            v-linecnt   = v-linecnt + 1
            s-linecnt   = string(v-linecnt,"zzzzz9")

            s-lineno    = string(oeel.lineno,"999")
            s-edilineno = if available edil then string(edil.edilineno, "x(11)")
                          else fill(" ",11)
            s-shipprod  = string(oeel.shipprod,"x(24)").

        /*tb 9731 10/21/98 jgc; Expand Non-stock description line.
            Added oeel.proddesc2 to s-descrip2 assignment. */
        {n-icseca.i "first" oeel.shipprod ""c"" oeeh.custno no-lock}
        assign
            s-custprod  = if available edil and edil.custprod > '' then
                              string(edil.custprod, "x(24)")
                          else if avail icsec then string(icsec.prod,"x(24)")
                          else v-24spaces
            s-descrip1  = if oeel.specnstype = "n" then
                              string(oeel.proddesc,"x(24)")
                          else if avail icsp then
                              string(icsp.descrip[1],"x(24)")
                          else v-24spaces
            s-descrip2  = if oeel.specnstype = "n" then
                              string(oeel.proddesc2,"x(24)")
                          else if avail icsp then
                              string(icsp.descrip[2],"x(24)")
                          else v-24spaces
            s-weight    = string(oeel.weight,"zzzzzz9.99999")
            s-cubes     = string(oeel.cubes,"zzzzzz9.99999")
            s-qtyord    = string(oeel.qtyord * if oeel.returnfl then -1 else 1,
                                 "zzzzz9.99-")
            s-qtyship   = string(oeel.qtyship * if oeel.returnfl then -1 else 1,
                                 "zzzzz9.99-").

        /*tb e5214 05/07/00 jlc; Add UPC Number */
        /*e Build the 24 character UPC Number */
        do for icsv:

            {icsv.gfi "'U'" oeel.shipprod oeel.vendno no}

            if avail icsv then do:
                assign
                    {upcno.gco &sectionupc  = "*"
                               &sasc        = "v-"
                               &icsv        = "icsv."
                               &upc         = "s-upcno"}.
            end.
            else
                s-upcno = " ".

            s-upcno = string(s-upcno,"x(24)").

        end.  /* do for icsv */

        /*tb 21744 08/16/96 jkp; Corrected s-ediunit to output "zz" if the
             record is found but the unitediuom field is blank.  Previous to
             this change the string portion of the icseu assign was missing,
             so if the field was blank the record would be positioned
             improperly.  While this problem was being fixed, we went ahead
             and changed it to output "zz" if the field was blank as this is
             what really should be happening. */
        {w-icseu.i oeel.shipprod oeel.unit no-lock}
        if avail icseu then
            assign
                s-ediunit = if icseu.unitediuom <> "" then
                                string(icseu.unitediuom,"xx")
                            else string("zz","xx").
        else do:
            {w-sasta.i "u" oeel.unit no-lock}
            assign
                s-ediunit = if avail sasta and sasta.unitediuom <> "" then
                                string(sasta.unitediuom,"xx")
                            else string("zz","xx").
        end. /* not avail icseu */

        assign
            s-oeeluser1   = string(oeel.user1,"x(78)")
            s-oeeluser2   = string(oeel.user2,"x(78)")
            s-oeeluser3   = string(oeel.user3,"x(78)")
            s-oeeluser4   = string(oeel.user4,"x(78)")
            s-oeeluser5   = string(oeel.user5,"x(78)")
            s-oeeluser6   = string(oeel.user6,"zzzzzzzz9.99999-")
            s-oeeluser7   = string(oeel.user7,"zzzzzzzz9.99999-")
            s-oeeluser8   = if oeel.user8 <> ? then
                                string(oeel.user8,"99/99/99")
                            else v-8spaces
            s-oeeluser9   = if oeel.user9 <> ? then
                                string(oeel.user9,"99/99/99")
                            else v-8spaces.
        /*das - If Vermeer, put PO line in user1 to send back correct line # */
        if oeeh.custno >= 13307161 and oeeh.custno <= 13307174 then
          do:
          for each V-edih where 
                   V-edih.cono    = g-cono + 500 and
                   V-edih.batchnm = "VERMEER" and
                   V-edih.custpo  = oeeh.custpo and
                  (V-edih.refer   = "ACTIVE" or
                   V-edih.refer   = "ACCEPT" or
                   V-edih.refer   = "SENT")
                   no-lock:
            if s-oeeluser1 = " " then
              do:
              for each V-edil where 
                       V-edil.cono    = V-edih.cono and
                       V-edil.batchnm = V-edih.batchnm and
                       V-edil.seqno   = V-edih.seqno and
                      (V-edil.reqprod = oeel.reqprod or
                substr(V-edil.user2,63,24) = oeel.shipprod) /*and
                       V-edil.price   = oeel.price and
                       V-edil.xxda1   = date(substr(oeel.user3,1,10))  and
                       V-edil.xxda2   = date(substr(oeel.user3,11,10))*/
                       no-lock:
                assign s-oeeluser1 = "LINE " + string(V-edil.lineno,"9999") +
                                     " on Customer PO" + v-54spaces.
                leave.
              end. /* each V-edil */
            end. /* if s-oeeluser1 = " " */
            if s-oeeluser1 = " " then
              assign s-oeeluser1 = "LINE " + string(oeel.lineno,"9999") + 
                                   " on Cust Order " + v-54spaces.
          end. /* each V-edih */
          if s-oeeluser1 = " " then
            assign s-oeeluser1 = "LINE " + string(oeel.lineno,"9999") +
                                 " on Cust Order " + v-54spaces.
        end. /* if Vermeer */
        {&edicom}
        assign
            s-oeeluser10  = if avail edil then
                              string(edil.user1, "x(8)")
                           else
                              v-8spaces
            s-oeeluser11  = if avail edil then
                              string(edil.user2, "x(8)")
                           else
                              v-8spaces
            s-oeeluser12  = if avail edil then
                              string(edil.user3, "x(8)")
                           else
                              v-8spaces.
        /{&edicom}* */

        /*NOTE:  If these become set from the edil file, move the
                 assignment up into the above assign statement that
                 is commented out when called from the XML output.*/
        assign
            s-oeeluser13  = v-8spaces
            s-oeeluser14  = v-8spaces.


        /* ADVANCED SHIPPING NOTICE LINE ITEM RECORDS */

        /*d User hook before ITEM record write */
        {oeepe.z99 &item_before = "*"}

        /*tb e5214 05/07/00 jlc; Add UPC Number */
        {&displaycom}
        put stream edi856 unformatted "Item  " +
                        caps(s-lineno) +
                        caps(s-shipprod) +
                        caps(s-custprod) +
                        caps(s-descrip1) +
                        caps(s-descrip2) +
                        caps(s-weight) +
                        caps(s-cubes) +
                        caps(s-qtyord) +
                        caps(s-qtyship) +
                        caps(s-ediunit) +
                        caps(s-oeeluser1) +
                        caps(s-oeeluser2) +
                        caps(s-oeeluser3) +
                        caps(s-oeeluser4) +
                        caps(s-upcno)     +
                             v-29spaces   +
                        caps(s-oeeluser5) +
                        caps(s-oeeluser6) +
                        caps(s-oeeluser7) +
                        caps(s-oeeluser8) +
                        caps(s-oeeluser9) +
                        caps(s-oeeluser10) +
                        caps(s-oeeluser11) +
                        caps(s-oeeluser12) +
                        caps(s-oeeluser13) +
                        caps(s-oeeluser14). /* +
                             v-6spaces.        +
                        caps(s-edilineno).  */
        /{&displaycom}* */

        /*d User hook after ITEM record write */
        {oeepe.z99 &item_after = "*"}

        {&displaycom}
        /*d use the skip to finish this record */
        put stream edi856 unformatted skip.
        /{&displaycom}* */