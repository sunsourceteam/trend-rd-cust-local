/*h****************************************************************************
  PROCEDURE      : tirate.p
  DESCRIPTION    : This program will allow you to pass in a location and it
                     will return the tax rates and any error code
  AUTHOR         : kjb
  DATE WRITTEN   : 01/04/00
  CHANGES MADE   :
    01/04/00 kjb; TB# 27114 Need to be able to quickly find the Taxware
        calculated tax rates
    11/27/00 tas; TB# 7173 Move from CHUI to Common for GUI
    01/03/02 gwk; TB# e11788 User hooks
    07/23/02 kjb; TB# e14109 Enhance the address resolution capability to
        function similar to that in titax.p.  Increased the formatting of the
        zip code parameter so that the complete value can be used.
    04/24/03 kjb; TB# e16723 Tighten the address resolution capability to
        prevent addresses entered without city, state and zip code from always
        being processed as an international address.
    10/07/03 emc; TB# e18116 International Customer Setup Error
    02/09/06 rgm; TB# e22770 Taxware for Windows integration
    03/08/06 kjb; TB# e24201 Add new C programs to open and close Taxware data
        files in an attempt to improve performance
    04/01/08 rgm; ERP-36836 Taxware Issue with zipcodes with extensions
******************************************************************************/
/*e
    WARNING:                    WARNING:                        WARNING:

    country is now a parameter passed into tirate.p.

    titax.p and tirate.p now assign the state, zip code and geo code
    interface variables the same way based on the country code.  However, they
    get the country code value in different ways if the country code is
    passed in as blank.

    * titax.p will set the country code base on the ARSC country code, the ARSS
      country code or the ICSD address.
    * tirate.p will set the country code based on the state and zip code passed
      into this program.

    That means it is possible that this program will calculate a country code
    that will tax correctly in Taxware while the Taxware calculation may fail
    in titax.p.
    An example of this would be an order entered for a customer or ship to with
    a blank country code (blank means US) where the ship to address on the OE
    header is changed to a valid Canadian address.  tirate would not produce an
    error because it would calculate the country code as Canada.  However,
    titax would set the country code to US based on the customer or ship to
    record and would not calculate taxes on the Canadian address (you would get
    a TI-95 error).
    To get around this situation, the following would have to occur:
    1) the country code on the OE header would have to be made an enterable
       field  (TB# t22338)
    2) a new parameter into tirate.p would be needed so the country code could
       be passed in instead of calculated
    3) titax.p would have to be changed to use the country code from the OE
       order instead of ARSC/ARSS/ICSD
*/

/*e*******************   Parameters   ****************************************

  INPUT
    v-city      - The city in which the tax rate will be found
    v-countycd  - The county code in which the tax rate will be found
    v-state     - The state in which the tax rate will be found
    v-zipcd     - The zip code in which the tax rate will be found
    v-geocd     - The geo code in which the tax rate will be found
    v-countrycd - The country code, if known, otherwise blank and the
                   country will be determined using city, state, zip
    v-juristy   - The jurisdiction type for which the tax rate will be found
                    A - Point of order acceptance
                    O - Point of order origination
                    F - Ship from location
                    T - Ship to location
    v-amount    - The dollar amount on which the tax rate will be found

  OUTPUT
    v-federalrt - The computed federal tax rate
    v-statert   - The computed state tax rate
    v-countyrt  - The computed county tax rate
    v-cityrt    - The computed city tax rate
    v-scountyrt - The computed secondary county tax rate
    v-scityrt   - The computed secondary city tax rate
    v-errorcd   - Any error code generated from Taxware

******************************************************************************/

def input  param v-city         as c format "x(26)"             no-undo.
def input  param v-countycd     as c format "x(3)"              no-undo.
def input  param v-state        as c format "x(2)"              no-undo.
def input  param v-zipcd        as c format "x(10)"             no-undo.
def input  param v-geocd        as c format "x(2)"              no-undo.
def input  param v-countrycd    like oeeh.countrycd             no-undo.
def input  param v-juristy      as c format "x(1)"              no-undo.
def input  param v-amount       as dec format "zzzzzzzz9.99-"   no-undo.
def output param v-federalrt    as dec format "zzzzzzzz9.99-"   no-undo.
def output param v-statert      as dec format "zzzzzzzz9.99-"   no-undo.
def output param v-countyrt     as dec format "zzzzzzzz9.99-"   no-undo.
def output param v-cityrt       as dec format "zzzzzzzz9.99-"   no-undo.
def output param v-scountyrt    as dec format "zzzzzzzz9.99-"   no-undo.
def output param v-scityrt      as dec format "zzzzzzzz9.99-"   no-undo.
def output param v-errorcode    as i                            no-undo.

{titax.sva "new"}
{tistnm.lva}
{titaxwin.lip}

def        var   v-errorfl      as log format "yes/no"          no-undo.
def        var   v-CAcityname   as c   format "x(26)"           no-undo.
def        var   v-CAstatecode  as c   format "x(2)"            no-undo.
def        var   v-starttm      as int                          no-undo.
def        var   v-retryfl      as log format "yes/no"          no-undo.
def        var   v-openfl       as log format "yes/no"          no-undo.

/*tb e14109 07/23/02 kjb; Need to determine if the address passed in to this
    program is US, Canadian or International.

    If the country is passed in - use it - otherwise, go thru the heirarchy.

    Country will be blank if:
        1) run from tiziplu.p (CHUI) or LookupGeoCode (tiziptmp.i - compile
            server\ar\t-are.w).  Only US or Canada.
        2) order is Counter Sale or Will Call so using Whse info (which has
           no country - only US or Canada)

    If either the state or the zip code passed in is blank, then we assume it
    is an international address.  US and Canadian addresses must have both.

    First, check to see if the state passed in is a US state or if the zip code
    is a US zip code.  If either is true, assume that it is a US address.
    If it is not a US address, call the program ticachk.p to determine if the
    address is Canadian. If so, set the country code to CA.
    If it is not US or Canadian, assume it is an international address. */
/*tb e16723 04/24/03 kjb; Restructuring the address validation logic.  It will
    now follow the following hierarchy:
    0. Is the country code passed in - if so, use it!  Its either US, CA or
         international
    1. Is the state code entered a US state; if so the address is assumed to be
         a US address no matter what else was entered in the zip code
    2. Is the zip code entered a US zip; if so the address is assumed to be a
         US address
    3. Is the state code entered a Canadian province; if so the address is
         assumed to be a Canadian address
    4. Is the zip code entered a Canadian zip; if so the address is assumed to
         be a Canadian address
    5. If a state code was entered but it is not a US state or a Canadian
         province, the address is assumed to be international
    6. If a zip code was entered but is not a US or Canadian zip code, the
         address is assumed to be international
    7. If no state code and zip code were entered but a city was entered, will
         need to prompt the user to find out if the address is international or
         an incorrectly entered US or Canadian address
    8. If no state code, zip code and city were entered, this will be an error
         because a US, Canadian or international address must have some
         information
*/

v-countrycd = "".  /* SX 55 */

/*d If the state passed in is not blank, check if it is a valid US state. */
if v-countrycd = "" and v-state <> "" then
state:
do i = 1 to 51:

    if v-statename[i] = v-state then do:

        v-countrycd = "US":u.
        leave state.

    end.

end. /* US state loop */

/*d If the state code was not a US state or was blank then check if the zip
    code passed in is not blank and is a valid US zip code */
if v-countrycd = "" and v-zipcd <> "" then do:

    if substring(v-zipcd,1,5) <> "00000":U then
    zip:
    do i = 1 to 5:

        /*d A US zip code must have integers in the first five positions */
        if substring(v-zipcd,i,1) >= '0':U and
           substring(v-zipcd,i,1) <= '9':U
        then
            v-countrycd = "US":u.
        else do:
            v-countrycd = "".
            leave zip.
        end.

    end. /* check 1st five positions of zip code */

end. /* US zip loop */

/*d If the address is not a US address, check if it is Canadian */
if v-countrycd = "" then do:

    run ticachk.p (input  caps(v-state),
                   input  substring(v-zipcd, 1, 5),
                   output v-CAcityname,
                   output v-CAstatecode).

    if v-CAcityname  <> "" or
       v-CAstatecode <> ""
    then
        v-countrycd = "CA":u.

end. /* if v-countrycd = "" - Canada check */

/*d If not a US or Candian address, then is it an international address or an
    incorrectly entered address */
if v-countrycd = "" then do:

    /*d If the state code is IT or the zip code is all zeroes, then the
        address is assumed to be international */
    if v-state = "IT":U or substring(v-zipcd,1,5) = "00000":U
        then v-countrycd = "IT":u.

    /*d If something was entered in either the state or the zip code, then the
        address is assumed to be international */
    else if v-state <> "" or v-zipcd <> ""
        then v-countrycd = "IT":u.

    /*d If nothing was entered in the city, state or zip, then it is an error.
        pass back an error code of 95 which will stop processing in the calling
        routing and force the user to change the address information. */
    else if v-city = "" and v-state = "" and v-zipcd = "" then do:

        v-errorcode = 95.
        return.

    end.

    /*d If only the city field has something in it, then we do not know what
        must be done.  Send back an error code of 9999 and let the calling
        routine handle accordingly. */
    else do:

        v-errorcode = 9999.
        return.

    end.

end. /* v-countrycd = "" - International check */

/*d Load the interface variables needed to get a tax rate */
/*  NOTE: The Taxware interface variables are defined in rdtax010.sva and
          the Verazip interface variables are defined in rdzip020.sva. */
/*tb e14109 07/23/02 kjb; Load the state code, zip code and geo code interface
    variables based on the country code. */
assign
    i-taxselparm  = "2":u
    i-loclname    = v-city
    i-cntycode    = v-countycd
    i-statecode   = if      v-countrycd = "US":u then v-state
                    else if v-countrycd = "CA":u then "CN":u
                    else "IT":u
    i-prizip      = if      v-countrycd = "US":u then substring(v-zipcd,1,5)
                    else if v-countrycd = "CA":u then
                         caps(substring(v-zipcd,1,5))
                    else "00000":u
    i-prizipext   = if v-countrycd <> "US":u or length(v-zipcd) < 9 then ""
                    else substring(v-zipcd,length(v-zipcd) - 3,4)
    i-prigeo      = if v-countrycd = "US":u then v-geocd
                    else caps(substring(v-zipcd,6,4))
    i-jurloctp    = v-juristy
    i-invoicedate = string(year(today))
                  + string(month(today), "99")
                  + string(day(today), "99")
    i-exemptamt   = 0
    i-grossamt    = v-amount
    i-servind     = " "
    i-fedslsuse   = i-servind
    i-staslsuse   = i-servind
    i-cnslsuse    = i-servind
    i-loslsuse    = i-servind.

/*tb e11788 01/03/02 gwk; User hooks */
{tirate.z99 &b4_tax010 = "*"}

/*d Run Taxware */
if OPSYS = "WIN32":U then do:


    assign
        i-shiptostate   = i-statecode
        i-shiptocnty    = i-cntycode
        i-shiptocity    = i-loclname
        i-shiptozip     = i-prizip
        i-shiptozipext  = i-prizipext
        i-shiptogeo     = i-prigeo.

    {titaxwin.i}

    run rdtxer.p(output v-errorfl, output v-errorcode).

end. /* Taxware on Windows */

else IF OPSYS = "UNIX":U THEN do:

    /*tb e24201 03/08/06 kjb; Broke out the Taxware processing into 3
        calls: open, process and close.  Run each of the three to determine if
        Taxware is working. */
    {twproc.lpr &type  = "Open"
                &time  = 1
                &errfl = v-errorfl
                &errcd = v-errorcode}

    /* If no error occurred trying to open the Taxware data files, then
       we will continue calculating the taxes in Taxware */
    if v-openfl = yes then do:

        {twproc.lpr &type  = "Tax"
                    &time  = 1
                    &errfl = v-errorfl
                    &errcd = v-errorcode}

        {twproc.lpr &type  = "Close"
                    &time  = 1
                    &errfl = v-errorfl
                    &errcd = v-errorcode}

    end. /* v-errorfl = no */

end. /* Taxware on Unix */

/*tb e11788 01/03/02 gwk; User hooks */
{tirate.z99 &aft_tax010 = "*"}

/*d If the amount is $100.00 then the tax amount is the same as the tax rate.
    Otherwise, need to mulitply the rate by 100 to get the percentage. */
if v-amount <> 100.00 then
    assign
        v-federalrt = i-fedtxrate  * 100
        v-statert   = i-statxrate  * 100
        v-countyrt  = i-cntxrate   * 100
        v-cityrt    = i-lotxrate   * 100
        v-scountyrt = i-sccntxrate * 100
        v-scityrt   = i-sclotxrate * 100.
else
    assign
        v-federalrt = i-fedtxamt

        v-statert   = i-statxamt
        v-countyrt  = i-cntxamt
        v-cityrt    = i-lotxamt
        v-scountyrt = i-sccntxamt
        v-scityrt   = i-sclotxamt.
