/*zkprpcomp.i 1.1  05/19/00*/
/*h****************************************************************************
  INCLUDE      : zkprpcomp.i
  DESCRIPTION  : Kit Production - Production Schedule Report
  USED ONCE?   : yes (4 times in zkprp.lpr)
  AUTHOR       : SD
  DATE WRITTEN : 06/12/00  
  CHANGES MADE :
   06/12/00  SD: 206890 Original development of new custom report - find and                  display unreceived components.
******************************************************************************/

/* KLT 07-07-00 Changed from bofl to boexistsfl*/
 if  t-kprp.boexistsfl = no and              
     t-kprp.stkqtyord > t-kprp.stkqtyship  then do:

     view frame f-kprpmh.
      
     for each b-oeelk use-index k-oeelk where                          
                     b-oeelk.cono            = t-kprp.cono        and
                     b-oeelk.ordertype       = "w"                and 
                     b-oeelk.orderno         = t-kprp.wono        and                                 b-oeelk.ordersuf        = t-kprp.wosuf       and
                     b-oeelk.comptype        = "c"                and                         /* Only get records in Kit Component Range unless           
                  Kit Component range is blank - then get all records */   
                     b-oeelk.shipprod        >= b-comprod         and 
                     b-oeelk.shipprod        <= e-comprod 
                   no-lock:                                          
                                 

          /* 06/29/00 yl */
          {w-icsp.i b-oeelk.shipprod no-lock}
          {w-icsw.i b-oeelk.shipprod t-kprp.whse no-lock}
          /* Tah 07/11/00  */
          if avail icsp and icsp.statustype = "l" then
            next.
          
          assign
                /*KLT 07-07-00*/
                /**** s-matlqty  = b-oeelk.stkqtyord - 
                              (if b-oeelk.stkqtyship <> 0 then
                                b-oeelk.stkqtyship 
                              else
                                b-oeelk.qtyreservd) ******/
                 s-matlqty  = b-oeelk.stkqtyord - b-oeelk.qtyreservd
                 s-matlprod = b-oeelk.shipprod
                 s-matldesc = if b-oeelk.proddesc ne "" then
                                  b-oeelk.proddesc
                              else if avail icsp then icsp.descrip[1]
                                   else ""
                s-onhand = icsw.qtyonhand
                s-buyer  = "".
          if s-matlqty le 0 then
            next.
              
          display
              s-matlqty
              s-matlprod
              s-matldesc
              s-onhand
              with frame f-kprpm.                  
                                                         

          if p-showtiefl then do:
              /* Get earliest due PO date */
              assign s-matlprom = ""
                     s-matltype = ""
                     v-eduedt   = 1/1/9999.
             
              po-loop:   
              for each poel use-index k-prod where
                                     poel.cono     = t-kprp.cono      and
                                     poel.shipprod = b-oeelk.shipprod and                                             poel.whse     = t-kprp.whse
              no-lock:
                  find first poeh use-index k-poeh where 
                                          poeh.cono  = poel.cono and
                                          poeh.pono  = poel.pono and
                                          poeh.posuf = poel.posuf
                  no-lock no-error.

                  /* 06/29/00; only check unreceived po which stagecd < 5 */
                  if not avail poeh or poeh.stagecd >= 5 then next po-loop. 
                                        
                  if poel.duedt < v-eduedt then
                      assign v-eduedt   = poel.duedt
                             s-matlpono = string(poel.pono,"9999999") + "-"
                                          + string(poel.posuf,"99").       

                  assign s-matltype = "PO"
                         s-matlprom = string(v-eduedt)
                         s-buyer = caps(poeh.buyer).    
              end. /* for each poel */
         
              if s-matltype = "PO" then do:
                 display
                     s-matltype                                                                     s-matlprom
                     s-matlpono
                     s-buyer
                 with frame f-kprpm.
                 down with frame f-kprpm.
               end. /* if po */

             /* Get earliest due WT date */
         
             assign s-matlprom = ""
                    s-matltype = ""
                    s-buyer = ""
                    v-eduedt   = 1/1/9999.
         
             wt-loop:        
             for each wtel use-index k-prodi where
                                        wtel.cono2      = t-kprp.cono      and
                                        wtel.shipprod   = b-oeelk.shipprod and
                                        wtel.shiptowhse = t-kprp.whse
             no-lock:
                 find first wteh use-index k-wteh where
                                         wteh.wtno  = wtel.wtno   and
                                         wteh.wtsuf = wtel.wtsuf
                 no-lock no-error.

                  /* 06/29/00; only check unreceived WT which stagecd < 5 */
                 if not avail wteh or wteh.stagecd >= 5 then next wt-loop.      
                    
                 if wtel.duedt < v-eduedt then
                     assign v-eduedt = wtel.duedt
                            s-matlpono = string(wtel.wtno,"9999999") + "-"
                                         + string(wtel.wtsuf,"99").
                  
                 assign s-matltype = "WT"
                        s-matlprom = string(v-eduedt).     
             end. /* for each wtel */    
             
             if s-matltype = "WT" then do:
                 display
                     s-matltype
                     s-matlprom
                     s-matlpono
                 with frame f-kprpm.
                 down with frame f-kprpm.
              end. /* WT */                                   

             /* Get earliest entered WO date */
             assign s-matlprom = ""
                    s-matltype = ""
                    s-buyer = ""
                    v-eduedt   = 1/1/9999.
                                             
             for each kpet use-index k-prod where 
                                      kpet.cono     = t-kprp.cono      and
                                      kpet.shipprod = b-oeelk.shipprod and
                                      kpet.whse     = t-kprp.whse      and
                                      kpet.stagecd  < 3
             no-lock:

                 if kpet.enterdt < v-eduedt then
                     assign v-eduedt = kpet.enterdt
                            s-matlpono = string(kpet.wono,"9999999") + "-"
                                         + string(kpet.wosuf,"99").
                                         
                 assign s-matltype = "WO"
                        s-matlprom = string(v-eduedt).    
            
             end. /* for each kpet */                                         
                  
         
             if s-matltype = "WO" then do:
                 display
                     s-matltype
                     s-matlprom
                     s-matlpono
                 with frame f-kprpm.
                 down with frame f-kprpm.
              end. /* WO */
         end. /* if p-showtiefl */        
     else
       down with frame f-kprpm.
     end. /* for each b-oeelk */         

     display skip with frame f-blank.
end. /* t-kprp.bofl = no and t-kprp.stkqtyord > t-kprp.stkqtyship */

