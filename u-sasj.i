/* u-sasj.i 1.1 01/03/98 */
/* u-sasj.i 1.2 5/28/93 */
/*h****************************************************************************
  INCLUDE      : u-sasj.i
  DESCRIPTION  : SA Setup Journal - update
  USED ONCE?   : yes
  AUTHOR       : rhl
  DATE WRITTEN : 07/06/89
  CHANGES MADE :
    07/30/97 mms; TB# 22023 Year 2000 compliance; Added 6 digit period logic.

    TAH      13 periods not handled properly in edit.

******************************************************************************/

/*d Period validation must be based on 2 digit periods with a format of pp. */

sasj.closefl    sasj.opendt      sasj.opentm
sasj.closedt    sasj.closetm     sasj.oper2
sasj.currproc   sasj.ourproc     sasj.batchnm

sasj.period
    validate (if {y2kperex.gca sasj.period} < 1 then false
              else if g-gl13perfl and
                {y2kperex.gca sasj.period} > 13 then false
              else if not g-gl13perfl and
                   {y2kperex.gca sasj.period} > 12 then false
              else true, "Invalid Period (2002)")
sasj.perfisc
    validate (if {y2kperex.gca sasj.perfisc} < 1 then false
              else if g-gl13perfl and
                {y2kperex.gca sasj.perfisc} > 13 then false
              else if not g-gl13perfl and
                   {y2kperex.gca sasj.perfisc} > 12
                               then false
              else true, "Invalid Period (2002)")

sasj.postdt     sasj.printfl

sasj.balancefl
    help "Used Only for OE Journals that Require Drawer Balancing"

sasj.smmergedfl sasj.proofdr     sasj.proofcr
sasj.nopostings sasj.totdr       sasj.totcr
sasj.tothash
