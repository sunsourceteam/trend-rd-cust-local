find xzicsw2 where xzicsw2.cono = g-cono and
                   xzicsw2.whse = {1} and
                   xzicsw2.prod = xzicsw.prod no-lock no-error.
if avail xzicsw2 then                   
  do:
  find first xzicsl where xzicsl.cono     = g-cono
                      and xzicsl.whse     = xzicsw2.whse
                      and xzicsl.vendno   = xzicsw2.arpvendno
                      and xzicsl.prodline  = xzicsw2.prodline no-lock
                                                            no-error. 
  if avail xzicsl then
    do:
    assign xzbuyer = xzicsl.buyer
           xzvend     = xzicsl.vendno
           xzprodline = xzicsl.prodline
           z-searchlevel = 4.
    end.
  else
    do:
    find first xzicsl where xzicsl.cono     = g-cono
                        and xzicsl.whse     = xzicsw2.whse
                        and xzicsl.prodline = xzicsw2.prodline no-lock
                                                               no-error.
    if avail xzicsl then  
      assign xzbuyer = xzicsl.buyer
             xzvend     = xzicsl.vendno
             xzprodline = xzicsl.prodline
             z-searchlevel = 5.
    else
      do:
      find first xzicsl where xzicsl.cono     = g-cono
                          and xzicsl.whse     = xzicsw2.whse
                          and xzicsl.vendno   = xzicsw2.arpvendno no-lock
                                                                  no-error.
      if avail xzicsl then  
        assign xzbuyer = xzicsl.buyer
               xzvend     = xzicsl.vendno
               xzprodline = xzicsl.prodline
               z-searchlevel = 6.
      end.   
   end.      
end.         
         