/* --------------------------------------------------------------------
   Include :     z-buyer.i
   Description:  Determine buyer to be displayed
                 Parameter {1} is pono
                 Parameter {2} is posuf
                 parameter {3} is buyer returned
   Date Written: 4/20/99
   Changes Made:


----------------------------------------------------------------------- */
define var xzbuyer like poeh.buyer.
define buffer xzpoeh for poeh.
define buffer xzpoel for poel.
define buffer xzicsl for icsl.

find xzpoeh where xzpoeh.cono = g-cono
              and xzpoeh.pono = {1}
              and xzpoeh.posuf = {2} no-lock no-error.
if avail xzpoeh then
  do:
  find first xzpoel where xzpoel.cono = g-cono
                      and xzpoel.pono = {1}
                      and xzpoel.posuf = {2} no-lock no-error.
  if avail xzpoel then
    do:
     find first xzicsl where xzicsl.cono = g-cono
                         and xzicsl.whse = xzpoeh.whse
                         and xzicsl.vendno = xzpoeh.vendno
                         and xzicsl.prodline.
  if avail xzpoel then
    do:
            


