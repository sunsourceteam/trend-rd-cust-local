/* g-oeepi.i 1.4 10/14/98 */
/*h*****************************************************************************
  INCLUDE      : g-oeepi.i
  DESCRIPTION  : Global Defines for OE Invoice Processing
  USED ONCE?   : no
  AUTHOR       : kmw
  DATE WRITTEN : 12/20/89
  CHANGES MADE :
    05/25/93 kmw; TB# 11576 SASJ updated with 0 DR/CR
    10/28/93 kmw; TB# 11758 Improve Unit Conversion Precision
    12/13/93 bj;  TB# 13881 Option to post in detail - Suspense only
    01/26/94 kmw; TB# 13827 2 hooks are needed in OEEPI.P
    06/06/94 dww; TB# 15382 Process OE w/bad terms creates no ARET
    06/07/94 dww; TB# 15845 If any ? found on order don't invoice
    06/24/94 dww; TB# 15045 Move hook for oeepis99 back to li.p
    11/14/94 djp; TB# 14243 Add create stock adjustments? no to option 19 with
        error handling
    06/14/95 kjb; TB# 18584 No SW exception messages provided.  Increased the
        extent of v-errlist from 18 to 22.
    08/04/95 gdf; TB# 18812 7.0 Rebates Enhancement (C5a).  Replaced def like
        oeel.vendrebamt and oeel.custrebamt with pder.rebateamt.
    09/05/95 kjb; TB# 18412 Claim generation report needed from invoice
        processing.  Removed t-nowarclaim and placed in oeepisw.gva.
    10/23/95 gdf; TB# 18812 7.0 Rebates Enhancement (C1a)  Removed define of
        v-vendrebamt and v-custrebamt. Will now use rebnet.gva.
    05/29/96 kmj; TB# 20022 Taxware Interface Project.  Removed
        vars v-geoindxty and v-modvtfl.
    11/11/96 tdd; TB# 22109 Allow NS rebating off ICSC record.  Add b-icsc.
    03/19/97 mms; TB#  7241 (5.1) Spec8.0 Special Price Costing; added b-icss
    12/03/97 kjb; TB# 23539 Stop invoice processing if Taxware returns an error
        Increased the extent of v-errlist from 22 to 23
    09/17/98 jgc, TB# 20381 OEWhse: Allow Order audit from Invoive processing.          Increased the extent of v-errlist from 23 to 24
    09/24/98 mtt; TB# 24049 PV: Common code use for GUI interface
    09/25/98 cm;  TB# 23507 Use new include to define w-div
    05/06/99 jbs; TB# 19724 Changes for sm/gl cost split.
    10/11/99 sah; TB# e2761 Tally Project.  Added b-oeelm buffer.
    02/12/01 lcg; TB# e7852 Multicurrency. Added v-exchrate.
    09/29/02 n-jk; TB# e8651 Add var and temp table for Core Account Processing
    10/10/02  al; TB# x75 Add PTX Invoice Processing Exceptions
    02/28/03 mms; TB# e15767 OAN last unit invoiced causes oob when tied.
    05/11/04 bp ; TB# e19471 Add user hooks.

*******************************************************************************/

{g-oeepi.z99 &user_top = "*"
             &new1     = {1}
             &new2     = {2}
             &com      = {3}}

def {2} shared buffer             b-arsc    for arsc.
def {2} shared buffer             b-icsp    for icsp.
def {2} shared buffer             b-icsw    for icsw.
def {2} shared buffer             b-icsc    for icsc.
def {2} shared buffer             b-icss    for icss.
def {2} shared buffer             b-oeel    for oeel.
def {2} shared buffer             b-oeelk   for oeelk.
def {2} shared buffer             b-oeelm   for oeelm.
def {2} shared buffer             b-wtel    for wtel.
def {2} shared buffer             b-icseg   for icseg.
def {2} shared buffer             b2-icseg  for icseg.
def {2} shared buffer             b-oeeh    for oeeh.
def {1} shared buffer             b-sasc    for sasc.

def {1} shared var p-sort         as c format "x(1)"             no-undo.
def {1} shared var p-postdt       as date                        no-undo.
def {1} shared var p-period       like sasj.period               no-undo.
def {1} shared var p-promofl      as logical initial no          no-undo.
def {1} shared var p-selectfl     as logical initial no          no-undo.
def {1} shared var p-quote        as c format "x(1)" initial "a" no-undo.
def {1} shared var p-edifl        as logical initial no          no-undo.
def {1} shared var p-faxfl        as logical initial no          no-undo.
def {1} shared var p-catalogty    as c format "x(1)"             no-undo.
def {1} shared var p-smfl         as logical initial no          no-undo.
def {1} shared var p-printto      as c format "x(1)" initial "b" no-undo.
def {1} shared var p-reprintmsg   as logical initial no          no-undo.
def {1} shared var p-honorfl      as l init yes                  no-undo.
def {1} shared var p-warranty     as c format "x(1)"             no-undo.
/* def     shared var v-modswfl      like sasa.modswfl           no-undo. */
def {1} shared var v-diff         as dec                         no-undo.

def {1} shared var o-stagecd      like oeeh.stagecd              no-undo.
def {1} shared var s-netavail     as dec format "zzzzzzzz9.99-"  no-undo.
def {1} shared var t-noinvproc    as i format "zzzz9" initial 0  no-undo.
def {1} shared var t-totdr        like sasj.totdr                no-undo.
def {1} shared var t-totcr        like sasj.totcr                no-undo.

def {1} shared var v-fifocost     as dec format ">>>>>>9.999999" no-undo.
def {1} shared var v-fifoadd      like icsef.addoncost           no-undo.
/* def {1} shared var v-conv         as de                       no-undo. */
def {1} shared var v-delete       as logical                     no-undo.
def {1} shared var v-errfl        as c format "x"                no-undo.
def {1} shared var v-gldivnodr    like glsa.gldivno              no-undo.
def {1} shared var v-gldeptnodr   like glsa.gldeptno             no-undo.
def {1} shared var v-glacctnodr   like glsa.glacctno             no-undo.
def {1} shared var v-glsubnodr    like glsa.glsubno              no-undo.
def {1} shared var v-gldivnocr    like glsa.gldivno              no-undo.
def {1} shared var v-gldeptnocr   like glsa.gldeptno             no-undo.
def {1} shared var v-glacctnocr   like glsa.glacctno             no-undo.
def {1} shared var v-glsubnocr    like glsa.glsubno              no-undo.
def {1} shared var v-iccommcost   like sasc.iccommcost           no-undo.
def {1} shared var v-icrmfifofl   like sasc.icrmfifofl           no-undo.
def {1} shared var v-icrollcostfl like sasc.icrollcostfl         no-undo.
def {1} shared var v-icsnpofl     like sasc.icsnpofl             no-undo.
def {1} shared var v-iccatkeyfl   like sasc.iccatkeyfl           no-undo.
def {1} shared var v-icincaddcm   like sasc.icincaddcm           no-undo.
def {1} shared var v-invno        like oeeh.orderno              no-undo.
def {1} shared var v-invsuf       like oeeh.ordersuf             no-undo.
def {1} shared var v-linealtno    like oeel.linealtno            no-undo.
def {1} shared var v-linefl       like oeeh.linefl               no-undo.
def {1} shared var v-lockedfl     as logical init no             no-undo.
def {1} shared var v-maint-l      as c format "x"                no-undo.
def {1} shared var v-mo           as i format "99"               no-undo.
def {1} shared var v-mocal        as i format "99"               no-undo.
def {1} shared var v-nopost       as i format ">>>>9"            no-undo.
def {1} shared var v-oerefer      like oeeh.refer                no-undo.
def {1} shared var v-oeepig99fl   as l init no                   no-undo.
def {1} shared var v-cogsadjamt   like glet.amount               no-undo.

/* Already Defined Wherever p-rptbeg.i Is Used */
{3}

/*tb 15045 06/24/94 dww; Move hook for oeepis99 back to li.p */
def     shared var v-oeepis99fl   as log                         no-undo.

def {1} shared var v-reportnm     like sapb.reportnm             no-undo.
/{3}* */
def {1} shared var v-undofl       as logical init no             no-undo.
def {1} shared var v-userfile     as c                           no-undo.
def {1} shared var v-yr           like smsc.yr                   no-undo.
def {1} shared var v-yrcal        like smsc.yr                   no-undo.

def {1} shared var v-fifocreated  as logical initial no          no-undo.

def     shared var v-reqshipdt    like oeeh.reqshipdt            no-undo.
def     shared var v-promisedt    like oeeh.promisedt            no-undo.
def     shared var v-updglty      like sasoo.updgloety           no-undo.
def     shared var v-approvety    like icsd.approvety            no-undo.
def     shared var v-chrecfl      as logical initial no          no-undo.
def     shared var v-wtapprwhse   like sasoo.wtapprwhse          no-undo.
def     shared var v-oelineno     like sasoo.oelineno            no-undo.
def     shared var v-icglcost   like sasc.icglcost           no-undo.
def     shared var v-icincaddsm   like sasc.icincaddsm           no-undo.
def     shared var v-icincaddgl   like sasc.icincaddgl           no-undo.
def     shared var v-icsmcost     like sasc.icsmcost             no-undo.

def     shared var v-icfifofl     like sasc.icfifofl             no-undo.
def     shared var v-idoeeh       as recid                       no-undo.
def     shared var v-pdspecialfl  as logical                     no-undo.
def     shared var v-modwmfl      like sasa.modwmfl              no-undo.
def {1} shared var v-icvendcost   like sasc.icvendcost           no-undo.

def {1} shared frame f-excpt.

form
    v-excpt         as c format "x(76)"         at 5
with frame f-excpt width 80 no-box no-labels.

def {1} shared var p-printunp     as logical initial no          no-undo.

/*tb 24049 09/21/98 mtt; PV: Common code use for GUI interface */
{nxtrend.gpp}

&IF "{&NXTREND-SYS}" = "TREND" &THEN
    /*tb 7581 08/24/92 dea; Workfile to keep divisional balances for Div Acct */
    /*tb 23507 09/25/98 cm; Use new include to define w-div */
    {gldiv.gtt &new = "{1} shared"}
&ENDIF
&IF "{&NXTREND-SYS}" = "POWERVUE" &THEN
def {1} shared temp-table w-div         no-undo           like pt_div.
&ENDIF

def {1} shared var p-sumpostfl    as logical  initial "yes"      no-undo.
def {1} shared var p-stockadjfl   as logical  initial "yes"      no-undo.

/*tb e7852 lcg; Multicurrency. Added v-exchrate. */
def {1} shared var v-exchrate   like sastc.vouchexrate           no-undo.

/* tb x75 new exception for PTX */
&IF "{&NXTREND-APP}" = "PTX" &THEN
def {1} shared var v-errlist      as char format "x(3)" extent 25
                                                      init "xxx" no-undo.
&ELSE
def {1} shared var v-errlist      as char format "x(3)" extent 24
                                                      init "xxx" no-undo.
&ENDIF


/*tb e8651 09/29/02 n-jk;
     Add var and temp table for Core Account Processing */
def {1} shared temp-table t-schpay no-undo
    field orderno   like oeeh.orderno
    field ordersuf  like oeeh.ordersuf
    field lineno    like oeel.lineno
    field amount    like aret.amount
    field duedt     as date format "99/99/99"
    index k-schpay is primary unique
          orderno
          ordersuf
          lineno.

{g-oeepi.z99 &user_bottom = "*"
             &new1        = {1}
             &new2        = {2}
             &com         = {3}}
