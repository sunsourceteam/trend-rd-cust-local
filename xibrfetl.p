/* ibrfetl.p 1.3 02/18/98 */
/*h****************************************************************************
  PROCEDURE      : ibrfetl.p (Based on icetl.p; same business logic,
                              different frames, adapted for RF equipment)
                   IB Module
  DESCRIPTION    : Display, Assign, and Deassign Lot #'s
  AUTHOR         : aa (after enp)
  DATE WRITTEN   :
  CHANGES MADE   :
    02/25/93 rs;  TB# 10150 Not checking supervisor initials
    07/13/95 gp;  TB# 18611 Make changes for KP Work Order Demand (G8)
        Add s-wosuf and s-kpdash.
    09/28/96 mtt; TB# 21254 (2F) Develop Value Add Module
    10/18/96 jl;  TB# 21920 Added user hook (&before_run)
    10/21/96 mtt; TB# 21254 (14A) Develop Value Add Module
    03/17/97 jl;  TB# 7241 (7.2) Special Price Costing. Added find of icss with
        buffer to pass current information to other programs
    01/16/98  aa; TB# 23693 Copied from icetl.p; adjustments for RF screens
            Calls ibrferr.p, ibrfwarn.p instead of err.p, warning.p;
            changed frames for RF terminals; preserved frame names when
            possible.
    12/08/98 jsb; TB# 25648 add flexible screen position
    05/10/99 jbs; TB# 23693 Moved to std 9.0
*******************************************************************************/

def shared var g-screenpos as i no-undo.
/*tb# 26693 Created include for variables, forms to replace {g-icetl.i new}*/
{ibrfetl.lva new}

def var v-type1  as c        no-undo.   /**** kp ****/
/*tb 21254 09/28/96 mtt; Develop Value Add Module */
/*tb 21254 10/21/96 mtt; Develop Value Add Module */
/*tb 23693 01/16/98  aa; Develop IB Module; changed title headers */
assign
   s-seqno    = if v-seqno ne 0 then
                    "-" + string(v-seqno,"999")
                else ""
   s-rettext  = if (v-type = "ic" and can-do("ro,ri",v-ictype)) or
                    (v-returnfl and v-type ne "wt")
                then
                    "RETURN"
                else ""
   s-title    = if can-do("oe,wt,va",v-type) and v-lineno ne 0
                then
                    if v-type = "wt" and v-returnfl
                    then
" Lot #                 Quantity     Open                    Cost    Expires"
                    else
" Lot #       "
       /*tb 23693 Quantity     Open     Qty Avail      Cost    Expires" */
                else
                if v-type = "ic" or v-type = "kp"
                then  /*** kp ***/
                    if v-ictype = "ro" or (v-ictype = "un" and v-returnfl)
                    then
" Lot #                 Quantity     Open     Qty Unavail    Cost    Expires"
                    else
" Lot #                 Quantity     Open     Qty Avail      Cost    Expires"
                else
" Lot # "
    s-kplabel  = if v-type = "kp" then
                     "WO#: "
                 else ""
    s-wono     = g-wono
    s-kpdash   = if v-type = "kp" then
                     "-"
                 else ""
    s-wosuf    = g-wosuf.

{w-icsp.i v-prod no-lock}

if not avail icsp then do:
    run err.p(4602).
    return.
end.

{icss.gfi
    &prod        = icsp.prod
    &icspecrecno = icsp.icspecrecno
    &lock        = no
    &buf         = "b-"}

{w-sasc.i no-lock}
/*tb 10150 rs; Not checking supervisor initials */
{w-sasoo.i g-operinits no-lock}

/*tb 21254 10/21/96 mtt; Develop Value Add Module */
assign
       v-seecostfl = sasoo.seecostfl
       v-method    = if can-do("oe,va",v-type) and v-lineno ne 0
                     then
                         (if v-ictype = "do" then
                              "e"
                          else
                              sasc.oeslentryty[2])
                     else
                     if v-type = "wt" then
                         (if v-returnfl then
                              "e"
                          else
                              sasc.wtslentryty[2])
                     else "e"
       v-iclotrcptty = if avail sasc then sasc.iclotrcptty else "a".

color display message s-title with frame f-icetl.
view frame f-border.

/*tb# 23693  aa; Modified Frame */
display
        s-title
        v-origqty
        v-proofqty
        v-ordqty
    with frame f-icetl.

/*u User hook before run of icetl??.p */
{icetl.z99 &before_run = "*"}

/*tb 21254 09/28/96 mtt; Develop Value Add Module */
/*tb 21254 10/21/96 mtt; Develop Value Add Module */
/*tb 23693 01/16/98  aa; Develop IB Module; Run ibrfetl?.p instead of icetl?.p*/
run value("xibrfetl" + (if v-type = "oe" then "o"       /*tb 23693 */
                       else if v-type = "po" then "p"   /*tb 23693 */
                       else                             /*tb 23693 */
                       if v-type = "kp" then "ic"
                       else
                       if v-type = "va" then
                           if v-lineno ne 0 then "oe"
                           else "po"
                       else v-type)
          + ".p")
         (v-whse,
          v-prod,
          v-type,
          v-ordno,
          v-ordsuf,
          v-lineno,
          v-seqno,
          v-returnfl,
          v-origqty,
          v-proofqty,
          v-ordqty,
          v-cost,
          v-ictype,
          output v-outqty,
          output v-errfl,
          input-output v-qtyunavail).

hide frame f-icetl  no-pause.
hide frame f-border no-pause.
return.

