/* edepaalp.p 1.1 01/02/98 */
/* edepaalp.p 1.0 10/30/95 */
/*h*****************************************************************************
  PROCEDURE      : edepaalp.p
  DESCRIPTION    : Product Validation, except for the Subs/Supers portion.
  AUTHOR         : jkp
  DATE WRITTEN   : 10/30/95
  CHANGES MADE   :
    10/30/95 jkp; TB# 17679 New Program.
    02/21/97 jkp; TB#  7241 (5.2) Spec8.0 Special Price Costing.  Added call
        of speccost.gva.
*******************************************************************************/

{g-all.i}
{edepa.gva}
{g-rptctl.i}
{edepa.gfo}
{f-blank.i}

/*tb  7241 (5.2) 02/21/97 jkp; Added call of speccost.gva. */
{speccost.gva "shared"}

/* Begin: Verify the Product */
do for icsp, icsw:

    /*d Get the Product using the Trend product */
    if g-shipprod = "" and s-trendprod <> "" then
    do:
        {edepaale.lpr s-trendprod}

        if avail icsp then
        do:
            {edepaalw.lpr}
        end. /* if avail icsp */

        if avail icsw then
        do:
            {edepaali.lpr}
        end. /* if avail icsw */

    end. /* g-shipprod = "" and s-trendprod <> "" */

    /*d Get the Product using the Vendor Product */
    if g-shipprod = "" and g-trvendno <> 0 and s-vendprod <> "" then
    do:
        find first icsw use-index k-manprod where
             icsw.cono       = g-cono          and
             icsw.whse       = g-whse          and
             icsw.vendprod   = s-vendprod      and
             icsw.arpvendno  = g-trvendno
        no-lock no-error.

        if avail icsw then
        do:
            {edepaale.lpr icsw.prod}
            {edepaali.lpr}
        end. /* if avail icsw */

    end. /* Get the Product using the Vendor Product */

    /* Get the Product using the BarCode Product */
    if g-shipprod = "" and s-barcode <> "" then
    do for icsec:
        find icsec use-index k-icsec where
             icsec.cono      = g-cono          and
             icsec.rectype   = "b"             and
             icsec.prod      = s-barcode
        no-lock no-error.

        if avail icsec then
        do:
            {edepaale.lpr icsec.altprod}

            if avail icsp then
            do:
                {edepaalw.lpr}
            end. /* if avail icsp */

            if avail icsw then
            do:
               {edepaali.lpr}
            end. /* if avail icsw */

        end. /* if avail icsec */

    end. /* do for icsec */

    /*d Get the Product using the UPC# */
    if g-shipprod = "" and g-trvendno <> 0 and s-upcno3 <> 0
        and s-upcno4 <> 0 then
    do for icsv:
        find first icsv use-index k-produpc where
             icsv.cono     = g-cono     and
             icsv.section4 = s-upcno4   and
             icsv.section3 = s-upcno3   and
             icsv.vendno   = g-trvendno
        no-lock no-error.

        if avail icsv then
        do:
            {edepaale.lpr icsv.prod}

            {w-icsw.i icsv.prod g-whse no-lock}
            if avail icsw then
            do:
                {edepaali.lpr}
            end. /* avail icsw */

        end. /* if avail icsv */

    end. /* do for icsv */

end. /* do for icsp, icsw */



